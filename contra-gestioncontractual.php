<?php

    header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=utf8");
	require 'comun.inc';
	require 'funciones.inc';
	session_start();
    if(empty($_SESSION)){
        header("location: index.php");
    }
	$linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");
	cargarcodigopag($_GET['codpag'],$_SESSION["nivel"]);
	date_default_timezone_set("America/Bogota");
?>
<!DOCTYPE >
<html lang=es>
	<head>
    <meta charset="utf-8"/>
		<meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1"/>
		<meta name="viewport" content="user-scalable=no">
		<title>:: IDEAL 10 - Contratación</title>
		<link href="favicon.ico" rel="shortcut icon"/>
        <link href="css/css2.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="css/programas.js"></script>

    </head>

	<style>

	</style>
    <body>
        <IFRAME src="alertas.php" name="alertas" id="alertas" style="display:none"></IFRAME>
        <span id="todastablas2"></span>
        <table>
            <tr><script>barra_imagenes("contra");</script><?php cuadro_titulos();?></tr>
            <tr><?php menu_desplegable("contra");?></tr>
        	<tr>
          		<td colspan="3" class="cinta">
					<a class="mgbt1"><img src="imagenes/add2.png" /></a>
					<a class="mgbt1"><img src="imagenes/guardad.png" style="width:24px;"/></a>
					<a class="mgbt1"><img src="imagenes/buscad.png"/></a>
					<a onClick="mypop=window.open('plan-agenda.php','','');mypop.focus()" class="mgbt"><img src="imagenes/agenda1.png" title="Agenda" /></a>
					<a onClick="<?php echo paginasnuevas("contra");?>" class="mgbt"><img src="imagenes/nv.png" title="Nueva Ventana"></a>
				</td>
        	</tr>
        </table>
 		<form name="form2" method="post" action="">
    		<table class="inicio">
      			<tr>
        			<td class="titulos" colspan="2">.: Gesti&oacute;n Contractual</td>
                    <td class="cerrar" style="width:7%"><a onClick="location.href='contra-principal.php'">Cerrar</a></td>
      			</tr>


				<tr>
				<td class='saludo1' width='70%'>
				<ol id="lista2">
						<li onClick="location.href='contra-gestioncontratos.php'" style="cursor:pointer">Proceso contractual</li>
						<li onClick="location.href='#'" style="cursor:pointer">Adiciones contractuales</li>
                        <li onClick="location.href='#'" style="cursor:pointer">Liquidacion contractual</li>

				</ol>

				</tr>
    		</table>
		</form>
	</body>
</html>
