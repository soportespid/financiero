<?php
	header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=uft8");
	require "comun.inc";
	require "funciones.inc";
    require "funcionesSP.inc.php";
	ini_set('max_execution_time',99999999);
	session_start();
	$linkbd=conectar_v7();
	$linkbd -> set_charset("utf8");
	cargarcodigopag($_GET['codpag'],$_SESSION["nivel"]);
	date_default_timezone_set("America/Bogota");
?>
<!DOCTYPE >
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="es">
	<head>
		<meta http-equiv="Content-Type" content="text/html" charset="utf8"/>
		<meta http-equiv="X-UA-Compatible" content="IE=9"/>
		<title>:: Reporte Subsidios</title>
		<link href="css/css2.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<link href="css/css4.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
        <link href="css/cssSP.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<script type="text/javascript" src="css/programas.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
        <script type="text/javascript" src="jquery-1.11.0.min.js"></script> 
        
		<script>
			$(window).load(function () {
				$('#cargando').hide();
			});

			function generarInforme()
			{
				var corte = parseInt(document.getElementById('corte').value);

				if((corte != '-1'))
				{
					document.form2.action="serv-excel-reporteIGAC-aseo.php";
					document.form2.target="_BLANK";
					document.form2.submit();
				}
			}

			function callprogress(vValor)
			{
				document.getElementById("getprogress").innerHTML = vValor;
				document.getElementById("getProgressBarFill").innerHTML = '<div class="ProgressBarFill" style="width: '+vValor+'%;"></div>';
				document.getElementById("titulog1").style.display='block';
				document.getElementById("progreso").style.display='block';
				document.getElementById("getProgressBarFill").style.display='block';
			}  

			function excell()
			{
				document.form2.action="serv-excel-reporteIGAC-aseo.php";
				document.form2.target="_BLANK";
				document.form2.submit();
			}
		</script> 
		<?php titlepag();?>
	</head>
	<body>
        <IFRAME src="alertas.php" name="alertas" id="alertas" style="display:none"></IFRAME>
		<span id="todastablas2"></span>
		<table>
			<tr><script>barra_imagenes("serv");</script><?php cuadro_titulos();?></tr>	 
			<tr><?php menu_desplegable("serv");?></tr>
			<tr>
				<td colspan="3" class="cinta">
					<a href="#" class="mgbt"><img src="imagenes/add2.png"/></a>
					<a href="#" class="mgbt"><img src="imagenes/guardad.png" title="Guardar"/></a>
					<a href="serv-reporteIGAC-alcantarillado.php" class="mgbt"><img src="imagenes/buscad.png" title="Buscar"/></a>
					<a onclick="mypop=window.open('plan-agenda.php','','');mypop.focus()" class="mgbt"><img src="imagenes/agenda1.png" title="Agenda" /></a>
					<a onclick="mypop=window.open('serv-principal.php','','');mypop.focus();" class="mgbt"><img src="imagenes/nv.png" title="Nueva ventana"></a>
					<a onclick="mypop=window.open('<?php echo $url2; ?>','','');mypop.focus();" class="mgbt"><img src="imagenes/duplicar_pantalla.png" title="Duplicar pesta&ntilde;a"></a>
					<a href=""><img src="imagenes/excel.png" title="Excel" onClick="excell()" class="mgbt"></a>
					<a href="serv-menuSUI"><img src="imagenes/iratras.png" class="mgbt" alt="Atrás"></a>
                </td>
			</tr>
		</table>

		<form name="form2" method="post">
			<table class="inicio ancho" style="width:99.5%">
				<tr>
					<td class="titulos" colspan="5">Reporte IGAC Aseo Servicios Públicos</td>
					<td class="cerrar" style="width:7%" onClick="location.href='serv-principal.php'">Cerrar</td>
				</tr>
				
				<tr>
					<td class="tamano01" style="width: 3cm;">Corte Liquidado:</td>
                    <td style="width: 20%;">
                        <select name="corte" id="corte" class="centrarSelect" style="width: 100%;" onchange="actualizar();">
                            <option value="-1" class="aumentarTamaño">SELECCIONE CORTE</option>
                            <?php
                                $sql = "SET lc_time_names = 'es_ES'";
                                mysqli_query($linkbd,$sql);

								$sqlr = "SELECT numero_corte, UPPER(MONTHNAME(fecha_inicial)), UPPER(MONTHNAME(fecha_final)), YEAR(fecha_inicial), YEAR(fecha_final) FROM srvcortes WHERE numero_corte > 0 ORDER BY numero_corte DESC";
								$resp = mysqli_query($linkbd,$sqlr);
								while ($row = mysqli_fetch_row($resp))
								{
									if(@ $_POST['corte'] == $row[0])
									{
										echo "<option class='aumentarTamaño' value='$row[0]' SELECTED>$row[1] $row[3] - $row[2] $row[4]</option>";
									}
									else{echo "<option class='aumentarTamaño' value='$row[0]'>$row[1] $row[3] - $row[2] $row[4]</option>";}
								}
							?>
                        </select>
                    </td>

					<td style="padding-bottom:0px;height:35px;"><em class="botonflecha" onclick="generarInforme()">Generar Reporte</em></td>
					
					<?php
						echo"
							<td>
								<div id='titulog1' style='display:none; float:left'></div>
								<div id='progreso' class='ProgressBar' style='display:none; float:left'>
									<div class='ProgressBarText'><span id='getprogress'></span>&nbsp;% </div>
									<div id='getProgressBarFill'></div>
								</div>
							</td>";
					?>
				</tr>
			</table>

            <div id="cargando" style=" position:absolute;left: 46%; bottom: 45%">
				<img src="imagenes/loading.gif" style=" width: 80px; height: 80px"/>
			</div>
			<input type="hidden" name="oculto" id="oculto" value="1"/>
		</form>
	</body>
</html>
