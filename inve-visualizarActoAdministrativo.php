<?php
	header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=utf8");
	require 'comun.inc';
	require 'funciones.inc';
	session_start();
	$linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");
	cargarcodigopag($_GET['codpag'],$_SESSION["nivel"]);
	date_default_timezone_set("America/Bogota");
?>
<!DOCTYPE >
<html lang=es>
	<head>
        <meta charset="utf-8"/>
		<meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1"/>
		<meta name="viewport" content="user-scalable=no">
		<title>:: IDEAL 10 - Almacen</title>
		<link href="favicon.ico" rel="shortcut icon"/>
		<link href="css/css2z.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
		<link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
		<link href="css/css4.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
        <link href="css/tabs.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css"/>
        <link rel="stylesheet" href="sweetalert2/dist/sweetalert2.min.css">
        <script type="text/javascript" src="css/programas.js"></script>
        <script src="sweetalert2/dist/sweetalert2.min.js"></script>
		<script src="vue/vue.min.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script src="css/programas.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script src="css/calendario.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
        <script>
            function pdf(){
				document.form2.action = "inve-actoadministrativopdf.php";
				document.form2.target = "_BLANK";
				document.form2.submit(); 
				document.form2.action = "";
				document.form2.target = "";
			}
        </script>
    </head>
	<body>
		<header>
			<table>
				<tr><script>barra_imagenes("inve");</script><?php cuadro_titulos();?></tr>
			</table>
		</header>

		<form name="form2" method="post" action="">
			<section id="myapp" v-cloak >
				<nav>
					<table>
						<tr><?php menu_desplegable("inve");?></tr>
					</table>
                    <div class="bg-white group-btn p-1" id="newNavStyle">
                        <button type="button" class="btn btn-white btn-primary-hover d-flex justify-between align-items-center" @click="location.href='inve-actoAdministrativo'">
                            <span>Nuevo</span>
                            <svg viewBox="0 -960 960 960" ><path d="M440-440H200v-80h240v-240h80v240h240v80H520v240h-80v-240Z"/></svg>
                        </button>
                        <button type="button" class="btn btn-white btn-primary-hover d-flex justify-between align-items-center" @click="saveCreate()">
                            <span>Guardar</span>
                            <svg viewBox="0 -960 960 960" ><path d="M840-680v480q0 33-23.5 56.5T760-120H200q-33 0-56.5-23.5T120-200v-560q0-33 23.5-56.5T200-840h480l160 160Zm-80 34L646-760H200v560h560v-446ZM480-240q50 0 85-35t35-85q0-50-35-85t-85-35q-50 0-85 35t-35 85q0 50 35 85t85 35ZM240-560h360v-160H240v160Zm-40-86v446-560 114Z"/></svg>
                        </button>
                        <button type="button" class="btn btn-white btn-primary-hover d-flex justify-between align-items-center" @click="location.href='inve-buscaActoAdministrativo'">
                            <span>Buscar</span>
                            <svg class="fill-black group-hover:fill-white w-5 h-5" viewBox="0 -960 960 960" ><path d="M784-120 532-372q-30 24-69 38t-83 14q-109 0-184.5-75.5T120-580q0-109 75.5-184.5T380-840q109 0 184.5 75.5T640-580q0 44-14 83t-38 69l252 252-56 56ZM380-400q75 0 127.5-52.5T560-580q0-75-52.5-127.5T380-760q-75 0-127.5 52.5T200-580q0 75 52.5 127.5T380-400Z"/></svg>
                        </button>
                        <button type="button" class="btn btn-white btn-primary-hover d-flex justify-between align-items-center" @click="mypop=window.open('inve-principal.php','',''); mypop.focus();">
                            <span>Nueva ventana</span>
                            <svg class="fill-black group-hover:fill-white w-5 h-5" viewBox="0 -960 960 960" ><path d="M320-120v-80h80v-80H160q-33 0-56.5-23.5T80-360v-400q0-33 23.5-56.5T160-840h640q33 0 56.5 23.5T880-760v400q0 33-23.5 56.5T800-280H560v80h80v80H320ZM160-360h640v-400H160v400Zm0 0v-400 400Z"/></svg>
                        </button>
                        <button type="button" class="btn btn-danger btn-danger-hover d-flex justify-between align-items-center" @click="pdf()">
                            <span>Exportar PDF</span>
                            <svg viewBox="0 0 512 512"><path d="M64 464l48 0 0 48-48 0c-35.3 0-64-28.7-64-64L0 64C0 28.7 28.7 0 64 0L229.5 0c17 0 33.3 6.7 45.3 18.7l90.5 90.5c12 12 18.7 28.3 18.7 45.3L384 304l-48 0 0-144-80 0c-17.7 0-32-14.3-32-32l0-80L64 48c-8.8 0-16 7.2-16 16l0 384c0 8.8 7.2 16 16 16zM176 352l32 0c30.9 0 56 25.1 56 56s-25.1 56-56 56l-16 0 0 32c0 8.8-7.2 16-16 16s-16-7.2-16-16l0-48 0-80c0-8.8 7.2-16 16-16zm32 80c13.3 0 24-10.7 24-24s-10.7-24-24-24l-16 0 0 48 16 0zm96-80l32 0c26.5 0 48 21.5 48 48l0 64c0 26.5-21.5 48-48 48l-32 0c-8.8 0-16-7.2-16-16l0-128c0-8.8 7.2-16 16-16zm32 128c8.8 0 16-7.2 16-16l0-64c0-8.8-7.2-16-16-16l-16 0 0 96 16 0zm80-112c0-8.8 7.2-16 16-16l48 0c8.8 0 16 7.2 16 16s-7.2 16-16 16l-32 0 0 32 32 0c8.8 0 16 7.2 16 16s-7.2 16-16 16l-32 0 0 48c0 8.8-7.2 16-16 16s-16-7.2-16-16l0-64 0-64z"/></svg>
                        </button>
                    </div>
				</nav>

				<article>
					<div>
						<table class="inicio">
							<tr>
								<td class="titulos" colspan="8">.: Acto administrativo</td>
                            	<td class="cerrar" style="width:4%" onClick="location.href='inve-principal.php'">Cerrar</td>
							</tr>

                            <tr>
								<td class="textonew01" style="width:3.5cm;">.: Tipo de acto:</td>
                            	<td>
                                    <input type="text" name="movimiento" v-model="movimiento" style="width:20%;" readonly>
                                </td>
							</tr>
						</table>
					</div>

                    <div>
                        <table class="inicio">
                            <tr>
                                <td class="titulos" colspan="12">.: Cabecera de acto administrativo</td>
                            </tr>
                            
                            <tr>
                                <td class="textonew01" style="width:3.5cm;">.: Consecutivo:</td>
                                <td style="width: 11%;">
                                    <input type="text" name="consecutivo" v-model="consecutivo" style="text-align: center;" readonly>
                                </td>

                                <td class="textonew01" style="width:2.5cm;">.: Fecha:</td>
                                <td style="width: 11%;">
                                    <input type="text" name="fecha" v-model="fechaRegistro" style="text-align: center;" name="fechaRegistro" readonly>
                                </td>

                                <td class="textonew01" style="width:3.5cm;">.: Motivo:</td>
                                <td colspan="2">
                                    <textarea v-model="motivo" style="width:100%" readonly></textarea>
                                </td>
                            </tr>

                            <tr>
                                <td class="textonew01" style="width:3.5cm;">.: Ciudad:</td>
                                <td style="width: 11%;">
                                    <input type="text" name="ciudad" v-model="ciudad" readonly>
                                </td>

                                <td class="textonew01" style="width:3.5cm;">.: Lugar fisico:</td>
                                <td style="width: 11%;">
                                    <input type="text" name="lugar" v-model="lugar" readonly>
                                </td>

                                <td class="textonew01" style="width:3.5cm;">.: Tercero:</td>
                                <td style="width: 11%;">
                                    <input type="text" name="tercero" v-model="tercero" style="text-align: center;" readonly>
                                </td>

                                <td>
                                    <input type="text" name="nomTercero" v-model="nomTercero" style="width:100%" readonly>
                                </td>
                            </tr>

                            <tr>
                                <td class="textonew01" style="width:3.5cm;">.: Descripción:</td>
                                <td colspan="6">
                                    <textarea name="descripcion" v-model="descripcion" style="width:100%" readonly></textarea>
                                    <input type="hidden" name="totalValores" v-model="totalValores">
                                </td>
                            </tr>
                        </table>

                        <div class="subpantalla" style='height:33vh; width:100%; overflow-x:hidden; resize: vertical;'>
                            <table>        
                                <thead>
                                    <tr>
                                        <th width="10%" class="titulosnew00">Articulo</th>
                                        <th width="25%" class="titulosnew00">Nombre articulo</th>
                                        <th width="10%" class="titulosnew00">Unidad medida</th>
                                        <th width="5%" class="titulosnew00">Estado</th>
                                        <th width="10%"class="titulosnew00">Cantidad</th>
                                        <th width="10%" class="titulosnew00">Valor unitario</th>
                                        <th width="10%" class="titulosnew00">Valor total</th>
                                    </tr>
                                </thead>

                                <tbody>
                                    <tr v-for="(articulo,index) in listaArticulos" v-bind:class="(index % 2 ? 'saludo1a' : 'saludo2')" style="font: 100% sans-serif;">
                                        <td style="text-align:center; width:10%;">{{ articulo[0] }}</td>
                                        <td style="text-align:center; width:25%;">{{ articulo[1] }}</td>
                                        <td style="text-align:center; width:10%;">{{ articulo[2] }}</td>
                                        <td style="text-align:center; width:5%;">{{ articulo[3] }}</td>
                                        <td style="text-align:center; width:10%;">{{ articulo[4] }}</td>
                                        <td style="text-align:center; width:10%;">{{ formatonumero(articulo[5]) }}</td>
                                        <td style="text-align:center; width:10%;">{{ formatonumero(articulo[6]) }}</td>

                                        <input type='hidden' name='articulo[]' v-model="articulo[0]">	
                                        <input type='hidden' name='nomArticulo[]' v-model="articulo[1]">
                                        <input type='hidden' name='unidad[]' v-model="articulo[2]">
                                        <input type='hidden' name='estado[]' v-model="articulo[3]">
                                        <input type='hidden' name='cantidad[]' v-model="articulo[4]">
                                        <input type='hidden' name='valorUnitario[]' v-model="articulo[5]">
                                        <input type='hidden' name='valorTotal[]' v-model="articulo[6]">
                                    </tr>
                                    <tr>
                                        <td class="titulosnew00" style="text-align:right;" colspan="6">Total: </td>
                                        <td style="text-align:center;">{{ formatonumero(totalValores) }}</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>

					<div id="cargando" v-if="loading" class="loading">
						<span>Cargando...</span>
					</div>
				</article>
			</section>
		</form>

		<script src="node_modules/read-excel-file/bundle/read-excel-file.min.js"></script>
		<script src="node_modules/xlsx/dist/xlsx.full.min.js"></script>
        <script src="Librerias/vue/vue.min.js"></script>
		<script src="Librerias/vue/axios.min.js"></script>
		<script type="module" src="almacen/actoAdministrativo/visualizar/inve-visualizarActoAdministrativo.js"></script>
        
	</body>
</html>