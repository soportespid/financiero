<?php

    header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=utf8");
	require 'comun.inc';
	require 'funciones.inc';
	session_start();
    if(empty($_SESSION)){
        header("location: index.php");
    }
	$linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");
	cargarcodigopag($_GET['codpag'],$_SESSION["nivel"]);
	date_default_timezone_set("America/Bogota");
?>
<!DOCTYPE >
<html lang=es>
	<head>
    <meta charset="utf-8"/>
		<meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1"/>
		<meta name="viewport" content="user-scalable=no">
		<title>:: IDEAL 10 - Gestión humana</title>
		<link href="favicon.ico" rel="shortcut icon"/>
		<link href="css/css2z.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
		<link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
		<link href="css/css4.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
        <link href="css/tabs.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css"/>
        <link href="css/style.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css"/>
        <link rel="stylesheet" href="sweetalert2/dist/sweetalert2.min.css">
        <script type="text/javascript" src="css/programas.js"></script>
        <script src="sweetalert2/dist/sweetalert2.min.js"></script>
		<script src="vue/vue.min.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script src="css/programas.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script src="css/calendario.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
	</head>
	<body>
		<header>
			<table>
				<tr><script>barra_imagenes("hum");</script><?php cuadro_titulos();?></tr>
			</table>
		</header>

        <main id="myapp" v-cloak>
            <input type="hidden" value = "2" ref="pageType">
            <div class="loading-container" v-show="isLoading" >
                <p class="text-loading"data-loading="IDEAL 10">IDEAL 10</p>
            </div>
            <nav>
                <table>
                    <tr><?php menu_desplegable("hum");?></tr>
                </table>
                <div class="bg-white group-btn p-1" id="newNavStyle">
                    <button type="button" @click="window.location.href='hum-nomina-retenciones-crear'" class="btn btn-white btn-primary-hover d-flex justify-between align-items-center">
                        <span>Nuevo</span>
                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 -960 960 960"><path d="M440-440H200v-80h240v-240h80v240h240v80H520v240h-80v-240Z"></path></svg>
                    </button>
                    <button v-if="txtNom == 0" type="button" @click="save()" v-if="arrData.length > 0" class="btn btn-white btn-primary-hover d-flex justify-between align-items-center">
                        <span>Guardar</span>
                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 -960 960 960">
                            <path d="M840-680v480q0 33-23.5 56.5T760-120H200q-33 0-56.5-23.5T120-200v-560q0-33 23.5-56.5T200-840h480l160 160Zm-80 34L646-760H200v560h560v-446ZM480-240q50 0 85-35t35-85q0-50-35-85t-85-35q-50 0-85 35t-35 85q0 50 35 85t85 35ZM240-560h360v-160H240v160Zm-40-86v446-560 114Z">
                            </path>
                        </svg>
                    </button>
                    <button type="button" @click="window.location.href='hum-nomina-retenciones-buscar'" class="btn btn-white btn-primary-hover d-flex justify-between align-items-center">
                        <span>Buscar</span>
                        <svg class="fill-black group-hover:fill-white w-5 h-5" xmlns="http://www.w3.org/2000/svg" viewBox="0 -960 960 960"><path d="M784-120 532-372q-30 24-69 38t-83 14q-109 0-184.5-75.5T120-580q0-109 75.5-184.5T380-840q109 0 184.5 75.5T640-580q0 44-14 83t-38 69l252 252-56 56ZM380-400q75 0 127.5-52.5T560-580q0-75-52.5-127.5T380-760q-75 0-127.5 52.5T200-580q0 75 52.5 127.5T380-400Z"></path></svg>
                    </button>
                    <button type="button" class="btn btn-white btn-primary-hover d-flex justify-between align-items-center" @click="mypop=window.open('hum-principal.php','',''); mypop.focus();">
                        <span>Nueva ventana</span>
                        <svg class="fill-black group-hover:fill-white w-5 h-5" xmlns="http://www.w3.org/2000/svg"  viewBox="0 -960 960 960" ><path d="M320-120v-80h80v-80H160q-33 0-56.5-23.5T80-360v-400q0-33 23.5-56.5T160-840h640q33 0 56.5 23.5T880-760v400q0 33-23.5 56.5T800-280H560v80h80v80H320ZM160-360h640v-400H160v400Zm0 0v-400 400Z"/></svg>
                    </button>
                    <button type="button" @click="mypop=window.open('hum-nomina-retenciones-editar','','');mypop.focus();" class="btn btn-white btn-primary-hover d-flex justify-between align-items-center">
                        <span class="group-hover:text-white">Duplicar pantalla</span>
                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 -960 960 960"><path d="M640-160v-360H160v360h480Zm80-200v-80h80v-360H320v200h-80v-200q0-33 23.5-56.5T320-880h480q33 0 56.5 23.5T880-800v360q0 33-23.5 56.5T800-360h-80ZM160-80q-33 0-56.5-23.5T80-160v-360q0-33 23.5-56.5T160-600h480q33 0 56.5 23.5T720-520v360q0 33-23.5 56.5T640-80H160Zm400-603ZM400-340Z"></path></svg>
                    </button>
                    <button type="button" @click="window.location.href='hum-nomina-retenciones-buscar'" class="btn btn-success btn-success-hover d-flex justify-between align-items-center">
                        <span>Atras</span>
                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 -960 960 960"><path d="M760-200v-160q0-50-35-85t-85-35H273l144 144-57 56-240-240 240-240 57 56-144 144h367q83 0 141.5 58.5T840-360v160h-80Z"></path></svg>
                    </button>
                </div>
            </nav>
            <section class="bg-white">
                <div>
                    <h2 class="titulos m-0">Editar retenciones de nómina</h2>
                    <div class="d-flex">
                        <div class="form-control w-50">
                            <label class="form-label" for="">Código:</label>
                            <div class="d-flex">
                                <button type="button" class="btn btn-primary" @click="nextItem('prev')"><</button>
                                <input type="text"  style="text-align:center;" v-model="txtConsecutivo" @change="nextItem()">
                                <button type="button" class="btn btn-primary" @click="nextItem('next')">></button>
                            </div>
                        </div>
                        <div class="form-control w-25">
                            <label class="form-label">Estado:</label>
                            <span :class="[txtEstado == 'S' && txtNom == 0  ? 'badge-success' : txtEstado=='P' ? 'badge-primary' : txtNom > 0 && txtEstado == 'S' ? 'badge-warning' : 'badge-danger']"class="badge">{{ txtEstado == 'S' && txtNom==0 ? 'Activo' : txtEstado=='P' ? 'Pagado' : txtNom > 0 && txtEstado == 'S' ? 'Liquidado' : 'Inactivo' }}</span>
                        </div>
                        <div class="form-control w-25">
                            <label class="form-label">Fecha <span class="text-danger fw-bolder">*</span>:</label>
                            <input type="date" v-model="txtFecha">
                        </div>
                        <div class="form-control" v-if="txtNom == 0">
                            <label class="form-label">Preliquidaciones <span class="text-danger fw-bolder">*</span>:</label>
                            <select v-model="selectNovedad" disabled>
                                <option value="">Seleccione</option>
                                <option v-for="(data,index) in arrNovedades" :value="data.id">{{data.id+" - "+data.nombre+" - "+data.vigencia+" - "+data.descripcion}}</option>
                            </select>
                        </div>
                        <div class="form-control" v-if="txtNom != 0">
                            <label class="form-label">Preliquidación <span class="text-danger fw-bolder">*</span>:</label>
                            <input type="text" disabled v-model="selectNovedad">
                        </div>
                    </div>
                    <div class="overflow-auto" style="height:60vh">
                        <table class="table fw-normal">
                            <thead class="text-center">
                                <tr>
                                    <th rowspan="2">Empleado</th>
                                    <th rowspan="2">CC/NIT</th>
                                    <th rowspan="2">Total devengado</th>
                                    <th rowspan="2">Factor salarial</th>
                                    <th colspan="3">Ingresos no constitutivos de renta</th>
                                    <th rowspan="2">Total ingresos no constitutivos</th>
                                    <th rowspan="2">Ingresos netos</th>
                                    <th rowspan="2">Deducciones</th>
                                    <th rowspan="2">Ingreso neto - deducciones</th>
                                    <th rowspan="2">25% renta exenta</th>
                                    <th rowspan="2">subtotal</th>
                                    <th rowspan="2">Base de retención</th>
                                    <th rowspan="2">UVT</th>
                                    <th rowspan="2">Total UVT</th>
                                    <th rowspan="2">Total retención mensual</th>
                                    <th rowspan="2">Retención manual</th>
                                    <th rowspan="2">Tipo pago</th>
                                </tr>
                                <tr>
                                    <th>Salud</th>
                                    <th>Pensión</th>
                                    <th>FSP</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr v-for="(data,index) in arrData" v-if="arrData.length > 0">
                                    <td>{{data.nombre}}</td>
                                    <td>{{data.documento}}</td>
                                    <td class="text-right">{{formatMoney(data.devengado)}}</td>
                                    <td class="text-right">{{formatMoney(data.factor)}}</td>
                                    <td class="text-right">{{formatMoney(data.salud)}}</td>
                                    <td class="text-right">{{formatMoney(data.pension)}}</td>
                                    <td class="text-right">{{formatMoney(data.fsp)}}</td>
                                    <td class="text-right">{{formatMoney(data.total_no_constitutivo)}}</td>
                                    <td class="text-right">{{formatMoney(data.total_ingreso_neto)}}</td>
                                    <td class="text-right">{{formatMoney(data.deducciones)}}</td>
                                    <td class="text-right">{{formatMoney(data.total_neto)}}</td>
                                    <td class="text-right">{{formatMoney(data.renta_exenta)}}</td>
                                    <td class="text-right">{{formatMoney(data.subtotal)}}</td>
                                    <td class="text-right">{{formatMoney(data.base_retencion)}}</td>
                                    <td class="text-center">{{data.base_uvt}}</td>
                                    <td class="text-center">{{data.total_uvt_retenido}}</td>
                                    <td class="text-right">{{formatMoney(data.total_retenido)}}</td>
                                    <td><div class="d-flex"><div class="form-control"><input type="text" class="text-right" v-model="data.retencion_formato" @keyup="updateRetencion()"></div></div></td>
                                    <td>
                                        <div class="d-flex">
                                            <div class="form-control">
                                                <select  v-model="data.tipo_pago">
                                                    <option v-for="(variable,i) in data.variables" :value="variable.codigo" :key="i">{{variable.codigo+"-"+variable.nombre}}</option>
                                                </select>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </section>
        </main>
		<script src="Librerias/vue/vue.min.js"></script>
		<script type="module" src="gestion_humana/nomina/js/functions_retenciones.js?<?= date('d_m_Y_h_i_s');?>"></script>
	</body>
</html>
