<?php

    header("Cache-control: no-cache, no-store, must-revalidate");
    header("Content-Type: text/html;charset=utf8");

    require 'comun.inc';
    require 'funciones.inc';

    session_start();
    date_default_timezone_set("America/Bogota");

?>
<html lang="es">
    <head>
        <meta charset="utf-8"/>
		<meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1"/>
		<meta name="viewport" content="user-scalable=no">
        <title>:: IDEAL 10 - Presupuesto</title>
        <link href="favicon.ico" rel="shortcut icon"/>
        <link href="css/css2.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
        <link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
        <link href="css/css4.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
        <script type="text/javascript" src="css/programas.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
        <script type="text/javascript" src="css/calendario.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script type="text/javascript" src="JQuery/jquery-2.1.4.min.js"></script>
        <link href="css/style.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />

        <script>
            function ocultarTabla(){
                app.mostrarEjecucion = false;
            }
        </script>
        <style>
            [v-cloak]{
                display : none;
            }

            label{
                font-size:13px;
            }

            input{
                height: calc(1em + 0.6rem + 0.5px) !important;
                font-size: 14px !important;
                margin-top: 4px !important;
            }


        </style>
    </head>
    <body>
        <IFRAME src="alertas.php" name="alertas" id="alertas" style="display:none"></IFRAME>
        <span id="todastablas2"></span>
        <header>
			<table>
				<tr><script>barra_imagenes("ccpet");</script><?php cuadro_titulos();?></tr>
			</table>
		</header>
        <form name="form2" method="post" action="">
            <section id="myapp" v-cloak >
                <nav>
                    <table>
                        <tr><?php menu_desplegable("ccpet");?></tr>
                    </table>
                    <div class="bg-white group-btn p-1" id="newNavStyle">
                        <button type="button" class="btn btn-white btn-primary-hover d-flex justify-between align-items-center" onclick="mypop=window.open('ccp-principal.php','',''); mypop.focus();">
                            <span>Nueva ventana</span>
                            <svg class="fill-black group-hover:fill-white w-5 h-5" xmlns="http://www.w3.org/2000/svg"  viewBox="0 -960 960 960" ><path d="M320-120v-80h80v-80H160q-33 0-56.5-23.5T80-360v-400q0-33 23.5-56.5T160-840h640q33 0 56.5 23.5T880-760v400q0 33-23.5 56.5T800-280H560v80h80v80H320ZM160-360h640v-400H160v400Zm0 0v-400 400Z"/></svg>
                        </button>
                        <button type="button" @click="downloadExl" class="btn btn-success btn-success-hover d-flex justify-between align-items-center">
                            <span>Exportar Excel</span>
                            <svg class="fill-black group-hover:fill-white w-4 h-4" xmlns="http://www.w3.org/2000/svg"  viewBox="0 0 384 512"><!--!Font Awesome Free 6.5.1 by @fontawesome - https://fontawesome.com License - https://fontawesome.com/license/free Copyright 2024 Fonticons, Inc. --><path d="M48 448V64c0-8.8 7.2-16 16-16H224v80c0 17.7 14.3 32 32 32h80V448c0 8.8-7.2 16-16 16H64c-8.8 0-16-7.2-16-16zM64 0C28.7 0 0 28.7 0 64V448c0 35.3 28.7 64 64 64H320c35.3 0 64-28.7 64-64V154.5c0-17-6.7-33.3-18.7-45.3L274.7 18.7C262.7 6.7 246.5 0 229.5 0H64zm90.9 233.3c-8.1-10.5-23.2-12.3-33.7-4.2s-12.3 23.2-4.2 33.7L161.6 320l-44.5 57.3c-8.1 10.5-6.3 25.5 4.2 33.7s25.5 6.3 33.7-4.2L192 359.1l37.1 47.6c8.1 10.5 23.2 12.3 33.7 4.2s12.3-23.2 4.2-33.7L222.4 320l44.5-57.3c8.1-10.5 6.3-25.5-4.2-33.7s-25.5-6.3-33.7 4.2L192 280.9l-37.1-47.6z"/></svg>
                        </button>
                        <button type="button" onclick="window.location.href='ccp-ejecucionpresupuestal.php'" class="btn btn-success btn-success-hover d-flex justify-between align-items-center">
                            <span>Atras</span>
                            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 -960 960 960"><path d="M760-200v-160q0-50-35-85t-85-35H273l144 144-57 56-240-240 240-240 57 56-144 144h367q83 0 141.5 58.5T840-360v160h-80Z"/></svg>
                        </button>
                    </div>
                </nav>
                <article>
                    <table class="inicio ancho">
                        <tr>
                            <td class="titulos" colspan="10" style="text-align:center; font-weight: 600;">.: Ejecuci&oacute;n presupuestal de gastos - General</td>
                            <td class="cerrar" style="width:7%" onClick="location.href='ccp-principal.php'">Cerrar</td>
                        </tr>
                        <tr>
                            <td style = "width: 10%;">
                                <label class="labelR">Sección</label>
                            </td>
                            <td style = "width: 10%;">
                                <select style = "width: 90%;" v-model="selectUnidad" v-on:change="seccionPresupuestal">
                                    <option value="-1">Todas</option>
                                    <option v-for="unidad in unidadesEjecutoras" v-bind:value="unidad[0]">
                                        {{ unidad[1] }} - {{ unidad[3] }}
                                    </option>
                                </select>
                            </td>
                            <td style = "width: 10%;">
                                <label class="labelR">Dependencia:</label>
                            </td>
                            <td style = "width: 10%;">
                                <select style = "width: 90%;" v-model="selectSeccion" v-on:change="cambiaCriteriosBusqueda">
                                    <option value="-1">Todas</option>
                                    <option v-for="seccion in seccionesPresupuestales" v-bind:value="seccion[0]">
                                        {{ seccion[0] }} - {{ seccion[1] }}
                                    </option>
                                </select>
                            </td>
                            <td style = "width: 10%;">
                                <label class="labelR">Situción/Fondo:</label>
                            </td>
                            <td style = "width: 10%;">
                                <select style = "width: 90%;" v-model="selectMedioPago" v-on:change="cambiaCriteriosBusqueda">
                                    <option v-for="option in optionsMediosPagos" v-bind:value="option.value">
                                        {{ option.text }}
                                    </option>
                                </select>
                            </td>

                            <td style = "width: 8%;">
                                <label class="labelR" for="">Fuente:</label>
                            </td>
                            <td colspan="4">
                                <div>
                                    <multiselect
                                        v-model="valueFuentes"
                                        placeholder="Seleccione una o varias fuentes"
                                        label="fuente" track-by="fuente"
                                        :custom-label="fuenteConNombre"
                                        :options="optionsFuentes"
                                        :multiple="true"
                                        :taggable="false"
                                        @input = "cambiaCriteriosBusqueda"
                                    ></multiselect>
                                </div>
                                <!-- <select style="width:90%" v-model="vigencia" v-on:Change="cambiaCriteriosBusqueda">
                                    <option v-for="year in years" :value="year[0]">{{ year[0] }}</option>
                                </select> -->
                            </td>

                        </tr>
                        <tr>
                            <td>
                                <label class="labelR">
                                    Fecha inicial:
                                </label>
                            </td>
                            <td>
                                <input type="text" style = "width: 90%;" name="fechaini" value="<?php echo $_POST['fechaini']?>" onKeyUp="return tabular(event,this)" id="fc_1198971545" title="DD/MM/YYYY" onDblClick="displayCalendarFor('fc_1198971545');" class="colordobleclik" autocomplete="off" onChange="" readonly>
                            </td>
                            <td>
                                <label class="labelR">
                                    Fecha final:
                                </label>
                            </td>
                            <td>
                                <input type="text" style = "width: 90%;" name="fechafin" value="<?php echo $_POST['fechafin']?>" onKeyUp="return tabular(event,this)" id="fc_1198971546" title="DD/MM/YYYY" onDblClick="displayCalendarFor('fc_1198971546');" class="colordobleclik" autocomplete="off" onChange="" readonly>
                            </td>

                            <td>
                                <label class="labelR">Vig. gasto:</label>
                            </td>
                            <td>
                                <select style = "width: 90%;" v-model="selectVigenciaGasto" v-on:change="cambiaCriteriosBusqueda">
                                    <option value="-1">Todas</option>
                                    <option v-for="vigenciaDeGasto in vigenciasdelgasto" v-bind:value="vigenciaDeGasto[0]">
                                        {{ vigenciaDeGasto[1] }} - {{ vigenciaDeGasto[2] }}
                                    </option>
                                </select>
                            </td>

                            <td>
                                <label class="labelR">Clasificador:</label>
                            </td>
                            <td>
                                <select style = "width: 90%;" v-model="selectClasificador" v-on:change="cuentasDelClasificador">
                                    <option value="-1">Ninguno...</option>
                                    <option v-for="clasificador in clasificadores" v-bind:value="clasificador[0]">
                                        {{ clasificador[1] }}
                                    </option>
                                </select>
                            </td>

                            <td>
                                <label class="labelR">Tipo:</label>
                            </td>
                            <td>
                                <select style = "width: 90%;" v-model="tipoGasto" v-on:change="cambiaCriteriosBusqueda">
                                    <option value="-1">Todos</option>
                                    <option v-for="tipoDeGasto in tiposDeGasto" v-bind:value="tipoDeGasto[2]">
                                        {{ tipoDeGasto[1] }}
                                    </option>
                                </select>
                            </td>

                            <td>
                                <button type="button" class="botonflechaverde" v-on:click="generarEjecucion">Generar</button>
                            </td>
                        </tr>
                    </table>
                    <div class="overflow-auto" style="max-height:55vh" v-show="mostrarEjecucion" id="tableId">

                    </div>
                    <div id="cargando" v-if="loading" class="loading">
                        <span>Cargando...</span>
                    </div>
                </article>
                <!-- <button @click = "downloadExl"> Exportar </button> -->
            </section>
        </form>

        <script src="https://cdn.jsdelivr.net/npm/vue@2.5.16/dist/vue.js"></script>
        <script src="https://unpkg.com/vue-multiselect@2.1.0"></script>
        <!-- <link rel="stylesheet" href="https://unpkg.com/vue-multiselect@2.1.0/dist/vue-multiselect.min.css"> -->
        <link rel="stylesheet" href="multiselect.css">

        <!-- <script src="Librerias/vue/vue.min.js"></script> -->
        <script src="Librerias/vue/axios.min.js"></script>
		<script src="xlsx/dist/xlsx.min.js"></script>
		<script src="file-saver/dist/FileSaver.min.js"></script>
		<script src="vue/presupuesto_ccp/ccp-ejecucionpresupuestal-nuevo.js"></script>
        <script src="sweetalert2/dist/sweetalert2.min.js"></script>
        <link rel="stylesheet" href="sweetalert2/dist/sweetalert2.min.css">
    </body>
</html>
