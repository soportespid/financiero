<?php
		require 'comun.inc';
		require 'funciones.inc';
		session_start();
		cargarcodigopag($_GET['codpag'],$_SESSION['nivel']);
		header('Cache-control: private'); // Arregla IE 6
		header("Cache-control: no-cache, no-store, must-revalidate");
		header("Content-Type: text/html;charset=utf8");
		date_default_timezone_set('America/Bogota');
		titlepag();
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="es">
	<head>
		<meta http-equiv="Content-type" content="text/html; charset=utf8"/>
		<meta http-equiv="X-UA-Compatible" content="IE=9"/>
		<title>:: IDEAL 10 - Servicios P&uacute;blicos</title>
		<link href="css/css2.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<link href="css/css4.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<link href="css/cssSP.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<link href="css/tabs.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css"/>
        <script type="text/javascript" src="css/calendario.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script src="vue/vue.js"></script>
		<script type="text/javascript" src="css/programas.js"></script>        

		<!-- sweetalert2 -->
		<script src="sweetalert2/dist/sweetalert2.min.js"></script>
		<link rel="stylesheet" href="sweetalert2/dist/sweetalert2.min.css">

		<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">

		<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
		<script type="text/javascript" src="bootstrap/fontawesome.5.11.2/js/all.js"></script>

		<style>
			.modal-mask {
			position: fixed;
			z-index: 9998;
			top: 0;
			left: 0;
			width: 100%;
			height: 100%;
			background-color: rgba(0, 0, 0, .5);
			display: table;
			transition: opacity .3s ease;
			}

			.modal-wrapper {
			display: table-cell;
			vertical-align: middle;
			}
			.modal-body{
				max-height: 500px;
				overflow-y: scroll;
			}
			.modal-intermetio{
				margin: 0 15px;
				font-family: helvética !important;
				font-size: 26px !important;
				padding: 10px 0;
			}
			.modal-intermedio-agregar{
				text-align:right;
				padding: 4px;
				margin-top: 6px;
				margin-right: 20px
			}
			.modal-body_1{
				padding-top: 15px;
				height: 40px;
			}
			.loader-table{
				/* background-color: #dff9fb;
				opacity: .5; */
				display: flex;
				align-items: center;
				justify-content: center;
				height: 75%;
			}
			.spinner{
				border: 4px solid rgba(0, 0, 0, 0.2);
				border-left-color: #39C;
				border-radius: 50%;
				width: 50px;
				height: 50px;
				animation: spin .9s linear infinite;
			}
			@keyframes spin {
				to { transform: rotate(360deg); }
			}
			.modal-container1
			{
				width: 50%;
				margin: 0px auto;
				padding: 20px 30px;
				text-align: left;
				background:linear-gradient(#99bbcc, #B6CEDA);
				border-radius: 10px;
				box-shadow: 0 2px 8px rgba(0, 0, 0, .33);
				transition: all .3s ease;
			}

            [v-cloak]{
                display : none;
            }

            label{
                font-size:13px;
            }

            

        </style>

		
	</head>
	<body>
		<div class="subpantalla" style="height:98%; width:99.6%; overflow:hidden;">
			<div id="myapp" style="height:inherit;" v-cloak>
				<div>
					<table>
						<tr>
							<script>barra_imagenes("serv");</script>
							<?php cuadro_titulos();?>
						</tr>

						<tr>
							<?php menu_desplegable("serv");?>
						</tr>

						<tr>
							<td colspan="3" class="cinta">
								<a href="serv-cierreDiarioDOCE.php" class="mgbt"><img src="imagenes/add.png"/></a>
								<a v-on:click="guardar();" class="mgbt"><img src="imagenes/guarda.png" title="Guardar"/></a>
								<a href="serv-buscar-cierresDoce.php" class="mgbt"><img src="imagenes/busca.png" title="Buscar"/></a>
								<a onclick="mypop=window.open('plan-agenda.php','','');mypop.focus()" class="mgbt"><img src="imagenes/agenda1.png" title="Agenda" /></a>
								<a onclick="mypop=window.open('serv-principal.php','','');mypop.focus();" class="mgbt"><img src="imagenes/nv.png" title="Nueva ventana"></a>
								<a onclick="mypop=window.open('<?php echo $url2; ?>','','');mypop.focus();" class="mgbt"><img src="imagenes/duplicar_pantalla.png" title="Duplicar pesta&ntilde;a"></a>
							</td>
						</tr>
					</table>
				</div>

				<table class="inicio grande">
					<tr>
						<td class="titulos" colspan="9">.: Cierre diario</td>
						<td class="cerrar" style="width:7%" onClick="location.href='serv-principal.php'">Cerrar</td>
					</tr>

					<tr>
						<td class="tamano01" style="width: 15%;">N° de cierre:</td>
						<td style="width: 15%;">
							<input type="text" name='numCierre' id='numCierre' v-model='numCierre' style="width: 100%; height: 30px; text-align:center;" readonly>
						</td>

						<td class="tamano01" style="width: 15%;">Fecha:</td>
						<td style="width:15%;">
							<input type="text" name="fecha" id="fecha" onchange="" value="<?php echo @ $_POST['fecha']?>" maxlength="10" onKeyUp="return tabular(event,this)"  id="fecha" onKeyDown="mascara(this,'/',patron,true)" title="DD/MM/YYYY" style="height:30px;text-align:center;width:85%;" readonly/>
							<a href="#" onClick="displayCalendarFor('fecha');" title="Calendario">
								<img src="imagenes/calendario04.png" style="width:25px;">
							</a>
						</td>

						<td class="tamano01" style="width: 15%;">Día de cierre:</td>
						<td style="width:15%;">
							<input type="text" name="diaCierre" id="diaCierre" onchange="" value="<?php echo @ $_POST['diaCierre']?>" maxlength="10" onKeyUp="return tabular(event,this)"  id="diaCierre" onKeyDown="mascara(this,'/',patron,true)" title="DD/MM/YYYY" style="height:30px;text-align:center;width:85%;" readonly/>
							<a href="#" onClick="displayCalendarFor('diaCierre');" title="Calendario">
								<img src="imagenes/calendario04.png" style="width:25px;">
							</a>
						</td>
					</tr>

					<tr>
						<td class="tamano01" style="width: 15%;">Conteo billetes:</td>
						<td style="width: 15%;">
							<input type="number" name='numBilletes' id='numBilletes' v-model='numBilletes' v-on:keyup="operacion();" style="width: 100%; height: 30px; text-align:center;">
						</td>

						<td class="tamano01" style="width: 15%;">Conteo monedas:</td>
						<td style="width: 15%;">
							<input type="number" name='numMonedas' id='numMonedas' v-model='numMonedas' v-on:keyup="operacion();" style="width: 100%; height: 30px; text-align:center;">
						</td>

						<td class="tamano01" style="width: 15%;">Conteo consignaciones:</td>
						<td style="width: 15%;">
							<input type="number" name='numConsignaciones' id='numConsignaciones' v-model='numConsignaciones' v-on:keyup="operacion();" style="width: 100%; height: 30px; text-align:center;">
						</td>
					</tr>

					<tr>
						<td class="tamano01" style="width: 15%;">Conteo cheques:</td>
						<td style="width: 15%;">
							<input type="number" name='numCheques' id='numCheques' v-model='numCheques' v-on:keyup="operacion();" style="width: 100%; height: 30px; text-align:center;">
						</td>

						<td class="tamano01" style="width: 15%;">Total conteo:</td>
						<td style="width: 15%;">
							<input type="number" name='totalConteo' id='totalConteo' v-model='totalConteo' style="width: 100%; height: 30px; text-align:center;" readonly>
						</td>
					
						<td style="padding-bottom:0px; text-align:center;">
							<em class="botonflechaverde" id="filtro" v-on:click="traerRecaudos();">Generar resumen</em>
						</td>
					</tr>
				</table>

				<div id="cargando" v-if="loading" class="loading">
					<span>Cargando...</span>
				</div>

                <div class="subpantalla" style="height: 45%;">
					<table>
						<thead>
							<tr>
								<td class="titulos" colspan="10" height="25">Detalles cierre de caja</td>
							</tr>

							<tr class="titulos2" style="text-align: center;">
								<td width="10%">Consecutivo recaudo</td>
								<td width="50%">Detalle</td>
								<td width="10%">Valor</td>
								<td width="20%">Tipo</td>
								<td width="10%">Forma de pago</td>
							</tr>
						</thead>
						
						<tbody>
							<?php
								$co ='zebra1';
								$co2='zebra2';
								$x = 0;
							?>
							<tr v-for="(ingreso,index) in ingresos" class='<?php echo $co; ?>' style='text-rendering: optimizeLegibility; text-align:center;'>
								<td style="font: 110% sans-serif; padding-left:10px">{{ ingreso[0] }}</td>
								<td style="font: 110% sans-serif; padding-left:10px; text-align:left;">{{ ingreso[1] }}</td>
								<td style="font: 110% sans-serif; padding-left:10px">{{ ingreso[2] }}</td>
								<td style="font: 110% sans-serif; padding-left:10px">{{ ingreso[3] }}</td>
								<td style="font: 110% sans-serif; padding-left:10px">{{ ingreso[4] }}</td>
								<?php
									$aux=$co;
									$co=$co2;
									$co2=$aux;
								?>
							</tr>
						</tbody>
					</table>
				</div>

				<table class="inicio grande">
					<tr>
						<td class="titulos" colspan="9">.: Resumen</td>
					</tr>

					<tr>
						<td class="tamano01" style="width: 15%;">Total recaudado:</td>
						<td style="width: 15%;">
							<input type="text" name='totalRecaudo' id='totalRecaudo' v-model='totalRecaudo' style="width: 100%; height: 30px; text-align:center;" readonly>
						</td>

						<td class="tamano01" style="width: 15%;">Total caja:</td>
						<td style="width: 15%;">
							<input type="text" name='totalCaja' id='totalCaja' v-model='totalCaja' style="width: 100%; height: 30px; text-align:center;" readonly>
						</td>

						<td class="tamano01" style="width: 15%;">Total banco:</td>
						<td style="width: 15%;">
							<input type="text" name='totalBanco' id='totalBanco' v-model='totalBanco' style="width: 100%; height: 30px; text-align:center;" readonly>
						</td>	
					</tr>
				</table>

			</div>	
		</div>
		
		
		<script src="Librerias/vue/vue.min.js"></script>
		<script src="Librerias/vue/axios.min.js"></script>
		<script src="vue/servicios_publicos/serv-cierreDiarioDOCE.js?<?php echo date('d_m_Y_h_i_s');?>"></script>	
	</body>
</html>