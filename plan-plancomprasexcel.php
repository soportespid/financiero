<?php 

    require_once 'PHPExcel/Classes/PHPExcel.php';
    require "comun.inc";
    require "funciones.inc";
    session_start();
	$linkbd = conectar_v7();
    $objPHPExcel = new PHPExcel();
	$objPHPExcel->getActiveSheet()->getStyle('A:J')->applyFromArray(
		array(
			'alignment' => array(
				'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
				'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER
			)
		)
	);
    $objPHPExcel->getProperties()
	->setCreator("IDEAL 10")
	->setLastModifiedBy("IDEAL 10")
	->setTitle("Exportar Excel con PHP")
	->setSubject("Documento de prueba")
	->setDescription("Documento generado con PHPExcel")
	->setKeywords("usuarios phpexcel")
	->setCategory("reportes");

	//----Cuerpo de Documento----
	$objPHPExcel->setActiveSheetIndex(0)
	->mergeCells('A1:j1')
	->mergeCells('A2:j2')
	->setCellValue('A1', 'PLANEACIÓN ESTRATÉGICA')
	->setCellValue('A2', 'REPORTE PLAN ANUAL DE ADQUISICIONES');
	$objPHPExcel-> getActiveSheet ()
	-> getStyle ("A1")
	-> getFill ()
	-> setFillType (PHPExcel_Style_Fill :: FILL_SOLID)
	-> getStartColor ()
	-> setRGB ('C8C8C8');
	$objPHPExcel-> getActiveSheet ()
	-> getStyle ("A1:A2")
	-> getFont ()
	-> setBold ( true ) 
	-> setName ( 'Verdana' ) 
	-> setSize ( 10 ) 
	-> getColor ()
	-> setRGB ('000000');
	$objPHPExcel-> getActiveSheet ()
	-> getStyle ('A1:A2')
	-> getAlignment ()
	-> applyFromArray (array ( 'horizontal'  =>  PHPExcel_Style_Alignment :: HORIZONTAL_CENTER ,) );
	$objPHPExcel-> getActiveSheet ()
	-> getStyle ('A3:j3')
	-> getAlignment ()
	-> applyFromArray (array ( 'horizontal'  =>  PHPExcel_Style_Alignment :: VERTICAL_JUSTIFY ,) ); 
	$objPHPExcel-> getActiveSheet ()
	-> getStyle ("A2")
	-> getFill ()
	-> setFillType (PHPExcel_Style_Fill :: FILL_SOLID)
	-> getStartColor ()
	-> setRGB ('A6E5F3');
	$objPHPExcel-> getActiveSheet ()
	-> getStyle ("A3:j3")
	-> getFill ()
	-> setFillType (PHPExcel_Style_Fill :: FILL_SOLID)
	-> getStartColor ()
	-> setRGB ('22C6CB');
	$borders = array(
		'borders' => array(
			'allborders' => array(
				'style' => PHPExcel_Style_Border::BORDER_THIN,
			'color' => array('argb' => 'FF000000'),
			)
		),
	);
	$objPHPExcel->getActiveSheet()->getStyle('A1:j1')->applyFromArray($borders);
	$objPHPExcel->getActiveSheet()->getStyle('A2:j2')->applyFromArray($borders);
	$objPHPExcel->getActiveSheet()->getStyle('A3:j3')->applyFromArray($borders);
	$objWorksheet = $objPHPExcel->getActiveSheet();

	$objPHPExcel->setActiveSheetIndex(0)
	->setCellValue('A3', 'Codigo')
	->setCellValue('B3', 'Codigos UNSPSC')
	->setCellValue('C3', 'Clasificadores')
	->setCellValue('D3', 'Descripción')
	->setCellValue('E3', 'Fecha estimada')
	->setCellValue('F3', 'Duración estimada')
	->setCellValue('G3', 'Modalidad selección')
    ->setCellValue('H3', 'Fuente')
    ->setCellValue('I3', 'Valor estimado')
    ->setCellValue('J3', 'Valor estimado vigente actual');

	$sql="SELECT * FROM contraplancompras WHERE vigencia='".vigencia_usuarios($_SESSION['cedulausu'])."' $crit1 $crit2  ORDER BY id ASC";
	$request = mysqli_query($linkbd,$sql);
	$cont = 0;
	$rowExcel = 4;

	while ($row = mysqli_fetch_row(($request))) {
		$modalidad = mysqli_fetch_row(mysqli_query($linkbd,"SELECT modalidad FROM contramodalidad WHERE id='$row[8]'"))[0];
		$arrDuracion = explode("/",$row[7]);
        $arrDuracion[0] = intval($arrDuracion[0]);
        $arrDuracion[1] = intval($arrDuracion[1]);
        
        if($arrDuracion[0]>0)$duracion = $arrDuracion[0]." dias";
        if($arrDuracion[1]>0)$duracion = $arrDuracion[1]." meses";
        if($arrDuracion[1]>0 && $arrDuracion[0]>0)$duracion = $arrDuracion[0]." dias ".$arrDuracion[1]." meses";

		$objPHPExcel->setActiveSheetIndex(0)
		->setCellValueExplicit ("A$rowExcel", $row[0], PHPExcel_Cell_DataType :: TYPE_STRING)
		->setCellValueExplicit ("B$rowExcel", str_replace('-',"\n",$row[4]), PHPExcel_Cell_DataType :: TYPE_STRING)
		->setCellValueExplicit ("C$rowExcel", str_replace('-',"\n",$row[16]), PHPExcel_Cell_DataType :: TYPE_STRING)
		->setCellValueExplicit ("D$rowExcel", $row[5], PHPExcel_Cell_DataType :: TYPE_STRING)
		->setCellValueExplicit ("E$rowExcel", $row[6], PHPExcel_Cell_DataType :: TYPE_STRING)
		->setCellValueExplicit ("F$rowExcel", $duracion, PHPExcel_Cell_DataType :: TYPE_STRING)
		->setCellValueExplicit ("G$rowExcel", $modalidad, PHPExcel_Cell_DataType :: TYPE_STRING)
		->setCellValueExplicit ("H$rowExcel", str_replace('-',"\n",$row[9]), PHPExcel_Cell_DataType :: TYPE_STRING)
		->setCellValueExplicit ("I$rowExcel", $row[10], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
		->setCellValueExplicit ("J$rowExcel", $row[11], PHPExcel_Cell_DataType :: TYPE_NUMERIC);
		
		$objPHPExcel->getActiveSheet()->getStyle("A$rowExcel:j$rowExcel")->applyFromArray($borders);
		$cont++;
		$rowExcel++;
	}


    //----Propiedades de la hoja
	$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true); 
	$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(20);
	$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(20);
	$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(40);
	$objPHPExcel->getActiveSheet()->getStyle('B4:B'.$objPHPExcel->getActiveSheet()->getHighestRow())->getAlignment()->setWrapText(true); 
	$objPHPExcel->getActiveSheet()->getStyle('C4:C'.$objPHPExcel->getActiveSheet()->getHighestRow())->getAlignment()->setWrapText(true); 
	$objPHPExcel->getActiveSheet()->getStyle('D4:D'.$objPHPExcel->getActiveSheet()->getHighestRow())->getAlignment()->setWrapText(true); 
	$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true); 
	$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setAutoSize(true); 
    $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setAutoSize(true);
    $objPHPExcel->getActiveSheet()->getColumnDimension('I')->setAutoSize(true);
    $objPHPExcel->getActiveSheet()->getColumnDimension('J')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->setTitle('Plan anual');
	$objPHPExcel->setActiveSheetIndex(0);

    //----Guardar documento----
    header('Content-Type: application/vnd.ms-excel');
    header('Content-Disposition: attachment;filename="reporte-plan-anual.xlsx"');
    header('Cache-Control: max-age=0');
    $objWriter=PHPExcel_IOFactory::createWriter($objPHPExcel,'Excel2007');
    $objWriter->save('php://output');
    die();

?>