<?php
	header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=utf8");
	require 'comun.inc';
	require 'funciones.inc';
	session_start();
	$linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");
	cargarcodigopag($_GET['codpag'],$_SESSION["nivel"]);
	date_default_timezone_set("America/Bogota");

?>
<html lang="es">
    <head>
        <meta charset="utf-8"/>
		<meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1"/>
		<meta name="viewport" content="user-scalable=no">
        <title>:: IDEAL 10 - Presupuesto</title>
        <link href="favicon.ico" rel="shortcut icon"/>
        <link href="css/css2z.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
		<link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
		<link href="css/css4.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
        <link href="css/tabs.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css"/>
        <link href="css/style.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
        <link rel="stylesheet" href="sweetalert2/dist/sweetalert2.min.css">
        <script src="sweetalert2/dist/sweetalert2.min.js"></script>
		<script src="vue/vue.min.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script src="css/programas.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script src="css/calendario.js?<?php echo date('d_m_Y_h_i_s');?>"></script>

        <!-- <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">

        <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
        <script type="text/javascript" src="bootstrap/fontawesome.5.11.2/js/all.js"></script> -->

        <script>
            function ocultarTabla(){
                app.mostrarEjecucion = false;
            }
        </script>
        <style>
            [v-cloak]{
                display : none;
            }

            /* label{
                font-size:13px;
            }

            input{
                height: calc(1em + 0.6rem + 0.5px) !important;
                font-size: 14px !important;
                margin-top: 4px !important;
            }



            .ancho{
                width: 60px;
            }

             */

            label{
                font-size:13px;
            }

            input{
                height: calc(1em + 0.6rem + 0.5px) !important;
                font-size: 14px !important;
                margin-top: 4px !important;
            }

            .captura{
                font-weight: normal;
                cursor: pointer !important;
            }

            .captura:hover{
                background: linear-gradient(#40f3ff , #40b3ff 70%, #40f3ff );
            }

            .colorFila{
                background-color: #F0FFFD !important;
            }

            .colorFila1{
                background-color: #FFF02A !important;
                font-family: -apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, "Helvetica Neue", Arial, "Noto Sans", sans-serif, "Apple Color Emoji", "Segoe UI Emoji", "Segoe UI Symbol", "Noto Color Emoji";
                font-size:10px;
                font-weight:normal;
                border:#eeeeee 1px solid;
                padding-left:3px;
                padding-right:3px;
                margin-bottom:1px;
                margin-top:1px;
                height:23.5px;
                border-radius: 1px;
            }

            .colorFila1:hover{
                background: linear-gradient(#40f3ff , #40b3ff 70%, #40f3ff );
            }

            .agregado{
                font-weight: 700;
            }

            .dobleclickCelda:hover{
                color: brown;
            }

            .sumTotal{
                /* background-color: #8bd1a9; */
                background-color: #31ada1;
                font-weight: 700;
                color: black;
            }
            .sumTotalFuncionamiento{
                background-color: #8bd1a9;
                /* background-color: #59d999; */
                font-weight: 700;
                color:black;
            }

        </style>
    </head>
    <body>
        <IFRAME src="alertas.php" name="alertas" id="alertas" style="display:none"></IFRAME>
        <span id="todastablas2"></span>
        <header>
			<table>
				<tr><script>barra_imagenes("ccpet");</script><?php cuadro_titulos();?></tr>
			</table>
		</header>
        <form name="form2" method="post" action="">
            <section id="myapp" v-cloak >
                <nav>
                    <table>
                        <tr><?php menu_desplegable("ccpet");?></tr>
                        <tr>
                            <td colspan="3" class="cinta">
                                <img src="imagenes/add.png"  class="mgbt1" onClick="location.href='ccp-ejecucionpresupuestal-inv-graf.php'" title="Nuevo">
                                <img src="imagenes/guardad.png" title="Guardar"  class="mgbt1">
                                <img src="imagenes/buscad.png" class="mgbt1" title="Buscar">
                                <img src="imagenes/nv.png" onClick="mypop=window.open('ccp-principal.php','',''); mypop.focus();" class="mgbt" title="Nueva Ventana">
                                <img src="imagenes/excel.png" title="Excel" v-on:click="downloadExl" class="mgbt">
                                <img src="imagenes/iratras.png" title="Atr&aacute;s" onClick="location.href='ccp-ejecucionpresupuestal.php'" class="mgbt">
                            </td>
                        </tr>
                    </table>
                </nav>
                <article>
                    <table class="inicio ancho">
                        <tr>
                            <td class="titulos" colspan="10" style="text-align:center; font-weight: 600;">.: Ejecuci&oacute;n presupuestal de inversi&oacute;n</td>
                            <td class="cerrar" style="width:7%" onClick="location.href='ccp-principal.php'">Cerrar</td>
                        </tr>
                        <tr>
                            <td style = "width: 10%;">
                                <label class="form-label">Secci&oacute;n:</label>
                            </td>
                            <td style = "width: 12%;">
                                <select style = "width: 98.5%;" v-model="selectSeccion" v-on:change="cambiaCriteriosBusqueda">
                                    <option value="-1">Todas</option>
                                    <option v-for="seccion in seccionesPresupuestales" v-bind:value="seccion[0]">
                                        {{ seccion[0] }} - {{ seccion[1] }}
                                    </option>
                                </select>
                            </td>
                            <td style = "width: 10%;">
                                <label class="form-label">CSF/SSF:</label>
                            </td>
                            <td style = "width: 12%;">
                                <select style = "width: 98.5%;" v-model="selectMedioPago" v-on:change="cambiaCriteriosBusqueda">
                                    <option v-for="option in optionsMediosPagos" v-bind:value="option.value">
                                        {{ option.text }}
                                    </option>
                                </select>
                            </td>
                            <td style = "width: 8%;">
                                <label class="form-label">Vig. gasto:</label>
                            </td>
                            <td style = "width: 12%;">
                                <select style = "width: 98.5%;" v-model="selectVigenciaGasto" v-on:change="cambiaCriteriosBusqueda">
                                    <option value="-1">Todas</option>
                                    <option v-for="vigenciaDeGasto in vigenciasdelgasto" v-bind:value="vigenciaDeGasto[0]">
                                        {{ vigenciaDeGasto[1] }} - {{ vigenciaDeGasto[2] }}
                                    </option>
                                </select>
                            </td>
                            <td style = "width: 8%;">
                                <label class="form-label" for="">Fuente:</label>
                            </td>
                            <td colspan="2">
                                <div>
                                    <multiselect
                                        v-model="valueFuentes"
                                        placeholder="Seleccione una o varias fuentes"
                                        label="fuente" track-by="fuente"
                                        :custom-label="fuenteConNombre"
                                        :options="optionsFuentes"
                                        :multiple="true"
                                        :taggable="false"
                                        @input = "cambiaCriteriosBusqueda"
                                    ></multiselect>
                                </div>
                                <!-- <select style="width:90%" v-model="vigencia" v-on:Change="cambiaCriteriosBusqueda">
                                    <option v-for="year in years" :value="year[0]">{{ year[0] }}</option>
                                </select> -->
                            </td>

                        </tr>


                        <tr>
                            <td>
                                <label class="form-label" for="">Sector:</label>
                            </td>
                            <td>
                                <div>
                                    <multiselect
                                        v-model="valueSectores"
                                        placeholder="Sel sector"
                                        label="sector" track-by="sector"
                                        :custom-label="sectorConNombre"
                                        :options="optionsSectores"
                                        :multiple="true"
                                        :taggable="false"
                                        @input = "changeSector"
                                    ></multiselect>
                                </div>
                            </td>
                            <td>
                                <label class="form-label" for="">Programa:</label>
                            </td>
                            <td>
                                <div>
                                    <multiselect
                                        v-model="valueProgramas"
                                        placeholder="Sel programa"
                                        label="programa" track-by="programa"
                                        :custom-label="programaConNombre"
                                        :options="optionsProgramas"
                                        :multiple="true"
                                        :taggable="false"
                                        @input = "changePrograma"
                                    ></multiselect>
                                </div>
                            </td>
                            <td>
                                <label class="form-label" for="">Subprograma:</label>
                            </td>
                            <td>
                                <div>
                                    <multiselect
                                        v-model="valueSubProgramas"
                                        placeholder="Sel subprogramas"
                                        label="codigo" track-by="codigo"
                                        :custom-label="subProgramaConNombre"
                                        :options="optionsSubProgramas"
                                        :multiple="true"
                                        :taggable="false"
                                        @input = "changeSubPrograma"
                                    ></multiselect>
                                </div>
                            </td>
                            <td>
                                <label class="form-label" for="">Producto:</label>
                            </td>
                            <td>
                                <div>
                                    <multiselect
                                        v-model="valueProductos"
                                        placeholder="Sel productos"
                                        label="producto" track-by="producto"
                                        :custom-label="productoConNombre"
                                        :options="optionsProductos"
                                        :multiple="true"
                                        :taggable="false"
                                        @input = "changeProducto"
                                    ></multiselect>
                                </div>
                            </td>

                        </tr>


                        <tr>
                            <td>
                                <label class="form-label">
                                    Fecha inicial:
                                </label>
                            </td>
                            <td>
                                <input type="text" style = "width: 98.5%;" name="fechaini" value="<?php echo $_POST['fechaini']?>" onKeyUp="return tabular(event,this)" id="fc_1198971545" title="DD/MM/YYYY" onDblClick="displayCalendarFor('fc_1198971545');" class="colordobleclik" autocomplete="off" onChange="" readonly>
                            </td>
                            <td>
                                <label class="form-label">
                                    Fecha final:
                                </label>
                            </td>
                            <td>
                                <input type="text" style = "width: 98.5%;" name="fechafin" value="<?php echo $_POST['fechafin']?>" onKeyUp="return tabular(event,this)" id="fc_1198971546" title="DD/MM/YYYY" onDblClick="displayCalendarFor('fc_1198971546');" class="colordobleclik" autocomplete="off" onChange="" readonly>
                            </td>

                            <td>
                                <label class="form-label">Nivel:</label>
                            </td>
                            <td>
                                <select style = "width: 98.5%;" v-model="selectNivel" v-on:change="cambiaCriteriosBusqueda">
                                    <option v-for="nivel in niveles" v-bind:value="nivel.value">
                                        {{ nivel.text }}
                                    </option>
                                </select>
                            </td>

                            <!-- <td>
                                <button type="button" class="botonflechaverde" v-on:click="generarEjecucion">Generar</button>
                            </td> -->
                        </tr>
                    </table>
                    <div class='subpantalla estilos-scroll' style='height:58vh; margin-top:0px; resize: vertical;'>

                        <div class="d-flex w-100 justify-center">
                            <!-- <div class="w-25 cursor-pointer card mb-3 ">
                                <div class="d-flex align-items-center p-4">
                                    <div class="me-4 p-2">
                                        <svg xmlns="http://www.w3.org/2000/svg" height="35px" viewBox="0 0 384 512" fill="#00FF9C"><path d="M64 0C28.7 0 0 28.7 0 64L0 448c0 35.3 28.7 64 64 64l256 0c35.3 0 64-28.7 64-64l0-288-128 0c-17.7 0-32-14.3-32-32L224 0 64 0zM256 0l0 128 128 0L256 0zM80 64l64 0c8.8 0 16 7.2 16 16s-7.2 16-16 16L80 96c-8.8 0-16-7.2-16-16s7.2-16 16-16zm0 64l64 0c8.8 0 16 7.2 16 16s-7.2 16-16 16l-64 0c-8.8 0-16-7.2-16-16s7.2-16 16-16zm54.2 253.8c-6.1 20.3-24.8 34.2-46 34.2L80 416c-8.8 0-16-7.2-16-16s7.2-16 16-16l8.2 0c7.1 0 13.3-4.6 15.3-11.4l14.9-49.5c3.4-11.3 13.8-19.1 25.6-19.1s22.2 7.7 25.6 19.1l11.6 38.6c7.4-6.2 16.8-9.7 26.8-9.7c15.9 0 30.4 9 37.5 23.2l4.4 8.8 54.1 0c8.8 0 16 7.2 16 16s-7.2 16-16 16l-64 0c-6.1 0-11.6-3.4-14.3-8.8l-8.8-17.7c-1.7-3.4-5.1-5.5-8.8-5.5s-7.2 2.1-8.8 5.5l-8.8 17.7c-2.9 5.9-9.2 9.4-15.7 8.8s-12.1-5.1-13.9-11.3L144 349l-9.8 32.8z"/></svg>
                                    </div>
                                    <div>
                                        <h2 class="fs-3 fw-bold m-0">Ingreso programado</h2>
                                        <h1 class="">{{ formatonumero(0) }}</h1>
                                        <p>Total programado para el período.</p>
                                    </div>
                                </div>
                            </div>

                            <div class="w-25 cursor-pointer card mb-3 ">
                                <div class="d-flex align-items-center p-4">
                                    <div class="me-4 p-2">
                                        <svg xmlns="http://www.w3.org/2000/svg" height="35px" viewBox="0 0 512 512" fill="#00FF9C"><path d="M240.1 4.2c9.8-5.6 21.9-5.6 31.8 0l171.8 98.1L448 104l0 .9 47.9 27.4c12.6 7.2 18.8 22 15.1 36s-16.4 23.8-30.9 23.8L32 192c-14.5 0-27.2-9.8-30.9-23.8s2.5-28.8 15.1-36L64 104.9l0-.9 4.4-1.6L240.1 4.2zM64 224l64 0 0 192 40 0 0-192 64 0 0 192 48 0 0-192 64 0 0 192 40 0 0-192 64 0 0 196.3c.6 .3 1.2 .7 1.8 1.1l48 32c11.7 7.8 17 22.4 12.9 35.9S494.1 512 480 512L32 512c-14.1 0-26.5-9.2-30.6-22.7s1.1-28.1 12.9-35.9l48-32c.6-.4 1.2-.7 1.8-1.1L64 224z"/></svg>
                                    </div>
                                    <div>
                                        <h2 class="fs-3 fw-bold m-0">Ingreso Recaudado</h2>
                                        <h1 class="">{{ formatonumero(0) }}</h1>
                                        <p>Total recaudado para el período.</p>
                                    </div>
                                </div>
                            </div>

                            <div class="w-25 cursor-pointer card mb-3 ">
                                <div class="d-flex align-items-center p-4">
                                    <div class="me-4 p-2">
                                        <svg xmlns="http://www.w3.org/2000/svg" height="35px" viewBox="0 0 384 512" fill="#00FF9C"><path d="M374.6 118.6c12.5-12.5 12.5-32.8 0-45.3s-32.8-12.5-45.3 0l-320 320c-12.5 12.5-12.5 32.8 0 45.3s32.8 12.5 45.3 0l320-320zM128 128A64 64 0 1 0 0 128a64 64 0 1 0 128 0zM384 384a64 64 0 1 0 -128 0 64 64 0 1 0 128 0z"/></svg>
                                    </div>
                                    <div>
                                        <h2 class="fs-3 fw-bold m-0">Porcentaje ejecutado</h2>
                                        <h1 class="">{{ 0 }} %</h1>
                                        <p>Porcentaje ejecutado para el período.</p>
                                    </div>
                                </div>
                            </div> -->
                        </div>

                        <div class="d-flex w-100 justify-center flex-column">

                            <div class="d-flex w-100 justify-center m-4">
                                <h2>Gráfico de Barras: Comparación por serctores</h2>
                            </div>
                            <div class="d-flex w-100 justify-center">

                                <div class="w-75">
                                    <div class="w-100">
                                        <line-chart3></line-chart3>
                                    </div>
                                </div>
                            </div>

                            <div class="d-flex w-100 justify-center m-4">
                                <h2>Gráfico de Pastel: Proporción de sectores</h2>
                            </div>
                            <div class="d-flex w-100 justify-center">

                                <div class="w-75">
                                    <div class="w-100">
                                        <line-chart4></line-chart4>
                                    </div>
                                </div>
                            </div>

                            <div class="d-flex w-100 justify-center m-4">
                                <h2>Gráfico de Líneas: Tendencia Mensual</h2>
                            </div>
                            <div class="d-flex w-100 justify-center">

                                <div class="w-75">
                                    <div class="w-100">
                                        <line-chart2></line-chart2>
                                    </div>
                                </div>
                            </div>




                        </div>
                        <!-- <table class='tabla-ejecucion' id="tableId">
                            <thead>
                                <tr style="text-align:Center;">
                                    <th class="titulosnew00" style="width:2%;">Vig. gasto</th>
                                    <th class="titulosnew00" style="width:2%;">Sec. Presupuestal</th>
                                    <th class="titulosnew00" style="width:5%;">Rubro</th>
                                    <th class="titulosnew00">Nombre</th>
                                    <th class="titulosnew00" style="width:2%;">Tipo</th>
                                    <th class="titulosnew00" style="width:4%;">Fuente</th>
                                    <th class="titulosnew00" style="width:8%;">Nombre Fuente</th>
                                    <th class="titulosnew00" style="width:3%;">CSF</th>
                                    <th class="titulosnew00" style="width:6%;">Inicial</th>
                                    <th class="titulosnew00" style="width:6%;">Adici&oacute;n</th>
                                    <th class="titulosnew00" style="width:6%;">Reducci&oacute;n</th>
                                    <th class="titulosnew00" style="width:6%;">Credito</th>
                                    <th class="titulosnew00" style="width:6%;">Contracredito</th>
                                    <th class="titulosnew00" style="width:6%;">Definitivo</th>
                                    <th class="titulosnew00" style="width:6%;">Disponibilidad</th>
                                    <th class="titulosnew00" style="width:6%;">Compromisos</th>
                                    <th class="titulosnew00" style="width:6%;">Oblicaci&oacute;n</th>
                                    <th class="titulosnew00" style="width:6%;">Egreso</th>
                                    <th class="titulosnew00" style="width:6%;">Saldo</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr v-for="(row,index) in sumaTotalGastosInversion" :key="index" v-bind:class="[ 'contenidonew01', 'sumTotalFuncionamiento', 'agregado']">
                                    <td style="width:2%;">{{ row[0] }}</td>
                                    <td style="width:2%;">{{ row[1] }}</td>
                                    <td style="width:5%;">{{ row[2] }}</td>
                                    <td>{{ row[3] }}</td>
                                    <td style="width:2%;">{{ row[4] }}</td>
                                    <td style="width:4%;">{{ row[5] }}</td>
                                    <td style="width:8%;"></td>
                                    <td style="width:3%;">{{ row[6] }}</td>
                                    <td style="width:6%; text-align: right;">{{ formatonumero(row[7]) }}</td>
                                    <td style="width:6%; text-align: right;">{{ formatonumero(row[8]) }}</td>
                                    <td style="width:6%; text-align: right;">{{ formatonumero(row[9]) }}</td>
                                    <td style="width:6%; text-align: right;">{{ formatonumero(row[10]) }}</td>
                                    <td style="width:6%; text-align: right;">{{ formatonumero(row[11]) }}</td>
                                    <td style="width:6%; text-align: right;">{{ formatonumero(row[12]) }}</td>
                                    <td style="width:6%; text-align: right;">{{ formatonumero(row[13]) }}</td>
                                    <td style="width:6%; text-align: right;">{{ formatonumero(row[14]) }}</td>
                                    <td style="width:6%; text-align: right;">{{ formatonumero(row[15]) }}</td>
                                    <td style="width:6%; text-align: right;">{{ formatonumero(row[16]) }}</td>
                                    <td style="width:6%; text-align: right;">{{ formatonumero(row[17]) }}</td>

                                    <input type='hidden' name='vigencia_gastoInv[]' v-model="row[0]">
                                    <input type='hidden' name='sec_presupuestalInv[]' v-model="row[1]">
                                    <input type='hidden' name='rubroInv[]' v-model="row[2]">
                                    <input type='hidden' name='nombreRubroInv[]' v-model="row[3]">
                                    <input type='hidden' name='tipoInv[]' v-model="row[4]">
                                    <input type='hidden' name='fuenteInv[]' v-model="row[5]">

                                    <input type='hidden' name='csfInv[]' v-model="row[6]">
                                    <input type='hidden' name='inicialInv[]' v-model="row[7]">
                                    <input type='hidden' name='adicionInv[]' v-model="row[8]">
                                    <input type='hidden' name='reduccionInv[]' v-model="row[9]">
                                    <input type='hidden' name='creditoInv[]' v-model="row[10]">
                                    <input type='hidden' name='contraCreditoInv[]' v-model="row[11]">
                                    <input type='hidden' name='definitivoInv[]' v-model="row[12]">
                                    <input type='hidden' name='disponibilidadInv[]' v-model="row[13]">
                                    <input type='hidden' name='compromisoInv[]' v-model="row[14]">
                                    <input type='hidden' name='obligacionInv[]' v-model="row[15]">
                                    <input type='hidden' name='egresoInv[]' v-model="row[16]">
                                    <input type='hidden' name='saldoInv[]' v-model="row[17]">

                                </tr>

                                <tr v-for="(row,index) in arbol" :key="index" v-bind:class="[row[2] === 'C' ? 'captura' : 'agregado' , Math.round(row[17]) < 0 ? 'colorFila1' : (Math.ceil(row[13]) < Math.ceil(row[14]) ? 'colorFila1' : (Math.ceil(row[14]) < Math.ceil(row[15]) ? 'colorFila1' : (Math.ceil(row[15]) < Math.ceil(row[16]) ? 'colorFila1' : (index % 2 ? 'contenidonew00' : 'contenidonew01'))))]">
                                    <td style="width:2%;">{{ row[5] }}</td>
                                    <td style="width:2%;">{{ row[4] }}</td>
                                    <td style="width:5%;">{{ row[0] }}</td>
                                    <td>{{ row[1] }}</td>
                                    <td style="width:2%;">{{ row[2] }}</td>
                                    <td style="width:4%;">{{ row[3] }}</td>
                                    <td style="width:8%;">{{ buscarFuente(row[3]) }}</td>
                                    <td style="width:3%;">{{ row[6] }}</td>
                                    <td style="width:6%; text-align: right;">{{ formatonumero(row[7]) }}</td>
                                    <td style="width:6%; text-align: right;">{{ formatonumero(row[8]) }}</td>
                                    <td style="width:6%; text-align: right;">{{ formatonumero(row[9]) }}</td>
                                    <td style="width:6%; text-align: right;">{{ formatonumero(row[10]) }}</td>
                                    <td style="width:6%; text-align: right;">{{ formatonumero(row[11]) }}</td>
                                    <td style="width:6%; text-align: right;">{{ formatonumero(row[12]) }}</td>
                                    <td style="width:6%; text-align: right;" v-on:dblclick="auxiliarPorComprobante('disponibilidad', row[3], row[0])" v-bind:class="row[2] === 'C' ? 'dobleclickCelda' : ''" title="Doble click, genera auxiliar">{{ formatonumero(row[13]) }}</td>
                                    <td style="width:6%; text-align: right;" v-on:dblclick="auxiliarPorComprobante('compromiso', row[3], row[0])" v-bind:class="row[2] === 'C' ? 'dobleclickCelda' : ''" title="Doble click, genera auxiliar">{{ formatonumero(row[14]) }}</td>
                                    <td style="width:6%; text-align: right;" v-on:dblclick="auxiliarPorComprobante('obligacion', row[3], row[0])" v-bind:class="row[2] === 'C' ? 'dobleclickCelda' : ''" title="Doble click, genera auxiliar">{{ formatonumero(row[15]) }}</td>
                                    <td style="width:6%; text-align: right;" v-on:dblclick="auxiliarPorComprobante('egreso', row[3], row[0])" v-bind:class="row[2] === 'C' ? 'dobleclickCelda' : ''" title="Doble click, genera auxiliar">{{ formatonumero(row[16]) }}</td>
                                    <td style="width:6%; text-align: right;">{{ formatonumero(row[17]) }}</td>

                                    <input type='hidden' name='vigencia_gasto[]' v-model="row[5]">
                                    <input type='hidden' name='sec_presupuestal[]' v-model="row[4]">
                                    <input type='hidden' name='rubro[]' v-model="row[0]">
                                    <input type='hidden' name='nombreRubro[]' v-model="row[1]">
                                    <input type='hidden' name='tipo[]' v-model="row[2]">
                                    <input type='hidden' name='fuente[]' v-model="row[3]">
                                    <input type='hidden' name='nombreFuente[]' v-model="buscarFuente(row[3])">
                                    <input type='hidden' name='csf[]' v-model="row[6]">
                                    <input type='hidden' name='inicial[]' v-model="row[7]">
                                    <input type='hidden' name='adicion[]' v-model="row[8]">
                                    <input type='hidden' name='reduccion[]' v-model="row[9]">
                                    <input type='hidden' name='credito[]' v-model="row[10]">
                                    <input type='hidden' name='contraCredito[]' v-model="row[11]">
                                    <input type='hidden' name='definitivo[]' v-model="row[12]">
                                    <input type='hidden' name='disponibilidad[]' v-model="row[13]">
                                    <input type='hidden' name='compromiso[]' v-model="row[14]">
                                    <input type='hidden' name='obligacion[]' v-model="row[15]">
                                    <input type='hidden' name='egreso[]' v-model="row[16]">
                                    <input type='hidden' name='saldo[]' v-model="row[17]">
                                </tr>
                            </tbody>
                        </table> -->
                    </div>
                    <div id="cargando" v-if="loading" class="loading">
                        <span>Cargando...</span>
                    </div>
                </article>
                <!-- <button @click = "downloadExl"> Exportar </button> -->
            </section>
        </form>


        <script src="node_modules/read-excel-file/bundle/read-excel-file.min.js"></script>
		<script src="node_modules/xlsx/dist/xlsx.full.min.js"></script>
		<script src="https://cdn.jsdelivr.net/npm/vue@2.5.16/dist/vue.js"></script>

        <script src="https://unpkg.com/vue-multiselect@2.1.0"></script>
        <link rel="stylesheet" href="multiselect.css">

        <!-- CDN de Chart.js -->
        <script src="https://cdn.jsdelivr.net/npm/chart.js@2.9.4/dist/Chart.min.js"></script>

		<script src="Librerias/vue/axios.min.js"></script>
		<script type="module" src="presupuesto_ccpet/grafIng/ccp-ejecucionpresupuestal-inv-graf.js?<?= date('d_m_Y_h_i_s');?>"></script>
        <!-- CDN de chartjs-plugin-datalabels -->
        <script src="https://cdn.jsdelivr.net/npm/chartjs-plugin-datalabels@0.7.0"></script>
    </body>
</html>
