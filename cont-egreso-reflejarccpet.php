<?php
header("Cache-control: no-cache, no-store, must-revalidate");
header("Content-Type: text/html;charset=utf8");

require "comun.inc";
require "funciones.inc";
require "conversor.php";
require "validaciones.inc";

$linkbd_V7 = conectar_v7();
$linkbd_V7->set_charset("utf8");

session_start();
date_default_timezone_set("America/Bogota");
?>
<!DOCTYPE>
<html lang="es">

<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1" />
    <meta name="viewport" content="user-scalable=no">
    <title>:: IDEAL 10 - Contabilidad</title>
    <link href="favicon.ico" rel="shortcut icon" />
    <link href="css/css2.css?<?php echo date('d_m_Y_h_i_s'); ?>" rel="stylesheet" type="text/css" />
    <link href="css/css3.css?<?php echo date('d_m_Y_h_i_s'); ?>" rel="stylesheet" type="text/css" />
    <link href="css/css4.css?<?php echo date('d_m_Y_h_i_s'); ?>" rel="stylesheet" type="text/css" />
    <link href="css/tabs2.css?<?php echo date('d_m_Y_h_i_s'); ?>" rel="stylesheet" type="text/css" />
    <link href="css/style.css?<?php echo date('d_m_Y_h_i_s'); ?>" rel="stylesheet" type="text/css" />
    <link href="sweetalert2/dist/sweetalert2.min.css" rel="stylesheet">
    <script src="sweetalert2/dist/sweetalert2.min.js"></script>
    <script type="text/javascript" src="css/programas.js?<?php echo date('d_m_Y_h_i_s'); ?>"></script>
    <script type="text/javascript" src="css/calendario.js?<?php echo date('d_m_Y_h_i_s'); ?>"></script>
    <script type="text/javascript" src="JQuery/jquery-2.1.4.min.js?<?php echo date('d_m_Y_h_i_s'); ?>"></script>
    <script>
        function validar() { document.form2.submit(); }
        function guardar() {
            var cuentasDeb = document.form2.elements['cuentaDebito[]'];
            var tieneCuenta = true;
            var cantCuentaDeb = cuentasDeb.length;
            if (cantCuentaDeb > 1) {
                for (var x = 0; x < cuentasDeb.length; x++) {
                    if (cuentasDeb[x].value == 'Sin Cuenta' || cuentasDeb[x].value == '') {
                        tieneCuenta = false;
                        break;
                    }
                }
            } else {
                if (cuentasDeb.value == 'Sin Cuenta' || cuentasDeb.value == '') {
                    tieneCuenta = false;
                }
            }
            if (tieneCuenta) {
                if (document.form2.fecha.value != '') {
                    Swal.fire({
                        icon: 'question',
                        title: '¿Seguro que quieres reflejar la información?',
                        showDenyButton: true,
                        confirmButtonText: 'Guardar',
                        confirmButtonColor: '#01CC42',
                        denyButtonText: 'Cancelar',
                        denyButtonColor: '#FF121A',
                    }).then(
                        (result) => {
                            if (result.isConfirmed) {
                                document.form2.oculto.value = 2;
                                document.form2.vari.value = '3';
                                document.form2.submit();
                            }
                            else if (result.isDenied) {
                                Swal.fire({
                                    icon: 'info',
                                    title: 'No se reflejo la información',
                                    confirmButtonText: 'Continuar',
                                    confirmButtonColor: '#FF121A',
                                    timer: 2500
                                });
                            }
                        }
                    )
                } else {
                    Swal.fire({
                        icon: 'error',
                        title: 'Error!',
                        text: 'Faltan datos para poder reflejar',
                        confirmButtonText: 'Continuar',
                        confirmButtonColor: '#FF121A',
                        timer: 2500
                    });
                    document.form2.fecha.focus();
                    document.form2.fecha.select();
                }
            } else {
                Swal.fire({
                    icon: 'error',
                    title: 'Error!',
                    text: 'Falta atributo contable en la pestaña de contabilidad.',
                    confirmButtonText: 'Continuar',
                    confirmButtonColor: '#FF121A',
                    timer: 2500
                });
            }
        }
        function despliegamodalm(_valor, _tip, mensa, pregunta) {
            document.getElementById("bgventanamodalm").style.visibility = _valor;
            if (_valor == "hidden") {
                document.getElementById('ventanam').src = "";
            } else {
                switch (_tip) {
                    case "1": document.getElementById('ventanam').src = "ventana-mensaje1.php?titulos=" + mensa; break;
                    case "2": document.getElementById('ventanam').src = "ventana-mensaje3.php?titulos=" + mensa; break;
                    case "3": document.getElementById('ventanam').src = "ventana-mensaje2.php?titulos=" + mensa; break;
                    case "4": document.getElementById('ventanam').src = "ventana-consulta1.php?titulos=" + mensa + "&idresp=" + pregunta; break;
                }
            }
        }
        function calcularpago() {
            valorp = document.form2.valor.value;
            descuentos = document.form2.totaldes.value;
            valorc = valorp - descuentos;
            document.form2.valorcheque.value = valorc;
            document.form2.valoregreso.value = valorp;
            document.form2.valorretencion.value = descuentos;
            document.form2.vari.value = '3';
            document.form2.submit();
        }
        function pdf() {
            document.form2.action = "pdfcxp.php";
            document.form2.target = "_BLANK";
            document.form2.vari.value = '3';
            document.form2.submit();
            document.form2.action = "";
            document.form2.target = "";
        }
        function adelante() {
            if (parseFloat(document.form2.ncomp.value) < parseFloat(document.form2.maximo.value)) {
                document.form2.oculto.value = 1;
                document.form2.ncomp.value = parseFloat(document.form2.ncomp.value) + 1;
                document.form2.idcomp.value = parseFloat(document.form2.idcomp.value) + 1;
                document.form2.action = "cont-egreso-reflejarccpet.php";
                //document.form2.vari.value='3';
                document.form2.submit();
            }
        }
        function atrasc() {
            if (document.form2.ncomp.value > 1) {
                document.form2.oculto.value = 1;
                document.form2.ncomp.value = document.form2.ncomp.value - 1;
                document.form2.idcomp.value = document.form2.idcomp.value - 1;
                document.form2.action = "cont-egreso-reflejarccpet.php";
                //document.form2.vari.value='3';
                document.form2.submit();
            }
        }
        function checkinicios() {
            cali = document.getElementsByName('inicios[]');
            for (var i = 0; i < cali.length; i++) {
                if (document.getElementById("inicia").checked == true) {
                    cali.item(i).checked = true;
                    document.getElementById("inicia").value = 1;
                } else {
                    cali.item(i).checked = false;
                    document.getElementById("inicia").value = 0;
                    document.getElementById("inicia").value = 0;
                }
            }
            //document.form2.vari.value='3';
            document.form2.submit();
        }
        function checker() {
            document.form2.vari.value = '3';
            document.form2.submit();
        }
        function validar2() {
            document.form2.oculto.value = 1;
            document.form2.ncomp.value = document.form2.idcomp.value;
            document.form2.action = "cont-egreso-reflejarccpet.php";
            //document.form2.vari.value='3';
            document.form2.submit();
        }
        function respuestaconsulta(pregunta) {
            switch (pregunta) {
                case "1":
                    document.form2.oculto.value = '2';
                    document.form2.submit(); break;
            }
        }
        function despliegamodal2(_valor, _nomve, _vaux, pos = '', sector = '') {
            document.getElementById("bgventanamodal2").style.visibility = _valor;
            if (_valor == "hidden") {
                document.getElementById('ventana2').src = "";
            } else {
                switch (_nomve) {
                    case "1": document.getElementById('ventana2').src = "registroccpet-ventana01.php?vigencia=" + _vaux; break;
                    case "2": document.getElementById('ventana2').src = "tercerosgral-ventana01.php?objeto=tercero&nobjeto=ntercero&tnfoco=detallegreso"; break;
                    case "3": document.getElementById('ventana2').src = "reversar-cxp.php?vigencia=" + _vaux; break;
                    case "4": document.getElementById('ventana2').src = "ventana-servicio.php?vigencia=" + _vaux; break;
                    case "5": document.getElementById('ventana2').src = "parametrizarCuentaccpet-ventana01.php?cuenta=" + _vaux + "&posicion=" + pos + "&sector=" + sector; break;
                }
            }
        }
        function direccionaCuentaGastos(row) { window.open("cont-editarcuentagastos.php?idcta=" + row); }
    </script>
</head>

<body>
    <IFRAME src="alertas.php" name="alertas" id="alertas" style="display:none"></IFRAME>
    <span id="todastablas2"></span>
    <table>
        <tr>
            <script>barra_imagenes("cont");</script><?php cuadro_titulos(); ?>
        </tr>
        <tr><?php menu_desplegable("cont"); ?></tr>
    </table>
    <div class="bg-white group-btn p-1"><button type="button" onclick="window.location.href='cont-buscaegreso-reflejar.php'" class="btn btn-white btn-primary-hover d-flex justify-between align-items-center">
            <span>Buscar</span>
            <svg class="fill-black group-hover:fill-white w-5 h-5" xmlns="http://www.w3.org/2000/svg" viewBox="0 -960 960 960"><path d="M784-120 532-372q-30 24-69 38t-83 14q-109 0-184.5-75.5T120-580q0-109 75.5-184.5T380-840q109 0 184.5 75.5T640-580q0 44-14 83t-38 69l252 252-56 56ZM380-400q75 0 127.5-52.5T560-580q0-75-52.5-127.5T380-760q-75 0-127.5 52.5T200-580q0 75 52.5 127.5T380-400Z"></path></svg>
        </button><button type="button" onclick="mypop=window.open('plan-agenda.php','','');mypop.focus()" class="btn btn-white btn-primary-hover d-flex justify-between align-items-center">
            <span class="group-hover:text-white">Agenda</span>
            <svg class="fill-black group-hover:fill-white w-5 h-5" xmlns="http://www.w3.org/2000/svg" viewBox="0 -960 960 960"><path d="M560-564v-68q33-14 67.5-21t72.5-7q26 0 51 4t49 10v64q-24-9-48.5-13.5T700-600q-38 0-73 9.5T560-564Zm0 220v-68q33-14 67.5-21t72.5-7q26 0 51 4t49 10v64q-24-9-48.5-13.5T700-380q-38 0-73 9t-67 27Zm0-110v-68q33-14 67.5-21t72.5-7q26 0 51 4t49 10v64q-24-9-48.5-13.5T700-490q-38 0-73 9.5T560-454ZM260-320q47 0 91.5 10.5T440-278v-394q-41-24-87-36t-93-12q-36 0-71.5 7T120-692v396q35-12 69.5-18t70.5-6Zm260 42q44-21 88.5-31.5T700-320q36 0 70.5 6t69.5 18v-396q-33-14-68.5-21t-71.5-7q-47 0-93 12t-87 36v394Zm-40 118q-48-38-104-59t-116-21q-42 0-82.5 11T100-198q-21 11-40.5-1T40-234v-482q0-11 5.5-21T62-752q46-24 96-36t102-12q58 0 113.5 15T480-740q51-30 106.5-45T700-800q52 0 102 12t96 36q11 5 16.5 15t5.5 21v482q0 23-19.5 35t-40.5 1q-37-20-77.5-31T700-240q-60 0-116 21t-104 59ZM280-494Z"></path></svg>
        </button><button type="button" onclick="mypop=window.open('cont-principal.php','','');mypop.focus();" class="btn btn-white btn-primary-hover d-flex justify-between align-items-center">
            <span>Nueva ventana</span>
            <svg class="fill-black group-hover:fill-white w-5 h-5" xmlns="http://www.w3.org/2000/svg" viewBox="0 -960 960 960"><path d="M320-120v-80h80v-80H160q-33 0-56.5-23.5T80-360v-400q0-33 23.5-56.5T160-840h640q33 0 56.5 23.5T880-760v400q0 33-23.5 56.5T800-280H560v80h80v80H320ZM160-360h640v-400H160v400Zm0 0v-400 400Z"></path></svg>
        </button><button type="button" onclick="mypop=window.open('/financiero/cont-egreso-reflejarccpet.php','','');mypop.focus();" class="btn btn-white btn-primary-hover d-flex justify-between align-items-center">
            <span class="group-hover:text-white">Duplicar pantalla</span>
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 -960 960 960"><path d="M640-160v-360H160v360h480Zm80-200v-80h80v-360H320v200h-80v-200q0-33 23.5-56.5T320-880h480q33 0 56.5 23.5T880-800v360q0 33-23.5 56.5T800-360h-80ZM160-80q-33 0-56.5-23.5T80-160v-360q0-33 23.5-56.5T160-600h480q33 0 56.5 23.5T720-520v360q0 33-23.5 56.5T640-80H160Zm400-603ZM400-340Z"></path></svg>
        </button><button type="button" onclick="guardar()" class="btn btn-white btn-primary-hover d-flex justify-between align-items-center">
            <span>Reflejar</span>
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 -960 960 960"><path d="M400-280h160v-80H400v80Zm0-160h280v-80H400v80ZM280-600h400v-80H280v80Zm200 120ZM265-80q-79 0-134.5-55.5T75-270q0-57 29.5-102t77.5-68H80v-80h240v240h-80v-97q-37 8-61 38t-24 69q0 46 32.5 78t77.5 32v80Zm135-40v-80h360v-560H200v160h-80v-160q0-33 23.5-56.5T200-840h560q33 0 56.5 23.5T840-760v560q0 33-23.5 56.5T760-120H400Z"></path></svg>
        </button><button type="button" onclick="pdf()" class="btn btn-danger btn-danger-hover d-flex justify-between align-items-center">
            <span>Exportar PDF</span>
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><!-- !Font Awesome Free 6.5.1 by @fontawesome - https://fontawesome.com License - https://fontawesome.com/license/free Copyright 2024 Fonticons, Inc. --><path d="M64 464l48 0 0 48-48 0c-35.3 0-64-28.7-64-64L0 64C0 28.7 28.7 0 64 0L229.5 0c17 0 33.3 6.7 45.3 18.7l90.5 90.5c12 12 18.7 28.3 18.7 45.3L384 304l-48 0 0-144-80 0c-17.7 0-32-14.3-32-32l0-80L64 48c-8.8 0-16 7.2-16 16l0 384c0 8.8 7.2 16 16 16zM176 352l32 0c30.9 0 56 25.1 56 56s-25.1 56-56 56l-16 0 0 32c0 8.8-7.2 16-16 16s-16-7.2-16-16l0-48 0-80c0-8.8 7.2-16 16-16zm32 80c13.3 0 24-10.7 24-24s-10.7-24-24-24l-16 0 0 48 16 0zm96-80l32 0c26.5 0 48 21.5 48 48l0 64c0 26.5-21.5 48-48 48l-32 0c-8.8 0-16-7.2-16-16l0-128c0-8.8 7.2-16 16-16zm32 128c8.8 0 16-7.2 16-16l0-64c0-8.8-7.2-16-16-16l-16 0 0 96 16 0zm80-112c0-8.8 7.2-16 16-16l48 0c8.8 0 16 7.2 16 16s-7.2 16-16 16l-32 0 0 32 32 0c8.8 0 16 7.2 16 16s-7.2 16-16 16l-32 0 0 48c0 8.8-7.2 16-16 16s-16-7.2-16-16l0-64 0-64z"></path></svg>
        </button><button type="button" onclick="window.location.href='cont-reflejardocsccpet.php'" class="btn btn-success btn-success-hover d-flex justify-between align-items-center">
            <span>Atras</span>
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 -960 960 960"><path d="M760-200v-160q0-50-35-85t-85-35H273l144 144-57 56-240-240 240-240 57 56-144 144h367q83 0 141.5 58.5T840-360v160h-80Z"></path></svg>
        </button></div>
        <div id="bgventanamodalm" class="bgventanamodalm">
            <div id="ventanamodalm" class="ventanamodalm">
                <IFRAME src="" name="ventanam" marginWidth=0 marginHeight=0 frameBorder=0 id="ventanam" frameSpacing=0 style=" width:700px; height:130px; top:200; ">
                </IFRAME>
            </div>
        </div>
        <form name="form2" method="post" action="">
            <?php
            $maxVersion = ultimaVersionGastosCCPET();
            function generaRetenciones($orden, $valor)
            {
                $linkbd_V7 = conectar_v7();
                $linkbd_V7->set_charset("utf8");
                $total = 0;
                $sql = "SELECT porcentaje FROM tesoordenpago_retenciones WHERE id_orden=$orden AND estado='S' ";
                $result = mysqli_query($linkbd_V7, $sql);
                $num = mysqli_num_rows($result);
                if ($num == 0) {
                    $total = $valor;
                } else {
                    while ($row = mysqli_fetch_row($result)) {
                        $total += ($valor * $row[0]) / 100;
                    }
                }
                return $total;
            }
            $vigusu = vigencia_usuarios($_SESSION['cedulausu']);

            //*********** cuenta origen va al credito y la destino al debito
            if ($_GET['consecutivo'] != "") {
                echo "<script>document.getElementById('codrec').value=$_GET[consecutivo];</script>";
            }

            if (!$_POST['oculto']) {
                $_POST['sinConta'] = array();
                $_POST['cuentaDebito'] = array();
                $_POST['cuentaCredito'] = array();
                $_POST['escogerCuenta'] = array();

                $sqlr = "select *from cuentapagar where estado='S' ";
                $res = mysqli_query($linkbd_V7, $sqlr);
                while ($row = mysqli_fetch_row($res)) {
                    $_POST['cuentapagar'] = $row[1];
                }
                $sqlr = "select * from tesoordenpago WHERE tipo_mov='201' ORDER BY id_orden DESC";
                $res = mysqli_query($linkbd_V7, $sqlr);
                $r = mysqli_fetch_row($res);
                $_POST['maximo'] = $r[0];
                if ($_POST['codrec'] != "" || $_GET['consecutivo'] != "") {
                    if ($_POST['codrec'] != "") {
                        $sqlr = "select * from tesoordenpago WHERE id_orden='" . $_POST['codrec'] . "' AND tipo_mov='201' ";
                    } else {
                        $sqlr = "select * from tesoordenpago WHERE id_orden='" . $_GET['consecutivo'] . "' AND tipo_mov='201'";
                    }
                } else {
                    $sqlr = "select * from tesoordenpago WHERE tipo_mov='201' ORDER BY id_orden DESC";
                }
                $res = mysqli_query($linkbd_V7, $sqlr);
                $r = mysqli_fetch_row($res);
                $_POST['ncomp'] = $r[0];
                $check4 = "checked";
                $fec = date("d/m/Y");
                if ($_GET['idop'] != "") {
                    $_POST['ncomp'] = $_GET['idop'];
                }
                $vigusu = $vigusu;
                $sqlr = "select * from tesoordenpago where tipo_mov='201' AND id_orden='" . $_POST['ncomp'] . "'";
                $res = mysqli_query($linkbd_V7, $sqlr);
                $consec = 0;
                while ($r = mysqli_fetch_row($res)) {
                    $consec = $r[0];
                    preg_match("/([0-9]{4})\-([0-9]{2})\-([0-9]{2})/", $r[2], $fecha);
                    $_POST['fecha'] = "$fecha[3]/$fecha[2]/$fecha[1]";
                    $_POST['compcont'] = $r[1];
                    $_POST['rp'] = $r[4];
                    $_POST['estado'] = $r[13];
                    $_POST['estadoc'] = $r[13];
                    $_POST['medioDePago'] = $r[19];
                    if ($_POST['medioDePago'] == '')
                        $_POST['medioDePago'] = '-1';
                }
                $_POST['idcomp'] = $consec;
                $_POST['dcuentas'] = array();
                $_POST['dvalores'] = array();
                $_POST['dncuentas'] = array();
                $_POST['sector'] = array();
                $_POST['nombreSector'] = array();
                $_POST['productoServicio'] = array();
                $_POST['nombreProductoServicio'] = array();
                $indicadorProducto = '';

                $sqlr = "select * from tesoordenpago_det where id_orden=$_POST[idcomp] and tipo_mov='201' ";
                $res = mysqli_query($linkbd_V7, $sqlr);
                while ($r = mysqli_fetch_row($res)) {
                    $indicadorProducto = substr($r[10], 0, 2);
                    $_POST['dcuentas'][] = $r[2];
                    $_POST['dvalores'][] = $r[4];
                    $_POST['dncuentas'][] = buscacuentaccpetgastos($r[2], $maxVersion);
                    $_POST['sector'][] = $indicadorProducto;
                    $_POST['nombreSector'][] = buscaNombreSector($indicadorProducto);
                    $_POST['productoServicio'][] = $r[9];
                    $tamProductoServicio = strlen($r[9]);
                    if ($r[9] != '') {
                        if ($tamProductoServicio == 5) {
                            $_POST['nombreProductoServicio'][] = buscaservicioccpetgastos($r[9]);
                        } else {
                            $_POST['nombreProductoServicio'][] = buscaproductoccpetgastos($r[9]);
                        }
                    }
                }
                $sqlr = " select cuenta_deb, cuenta_cred from tesoordenpago_cuenta where id_orden=$_POST[idcomp] and tipo_mov='201' and estado='S'";
                $res = mysqli_query($linkbd_V7, $sqlr);
                while ($r = mysqli_fetch_row($res)) {
                    $_POST['cuentaDebito'][] = $r[0];
                    $_POST['cuentaCredito'][] = $r[1];
                }
            }

            if ($_POST['oculto'] == '1') {
                $_POST['sinConta'] = array();
                $_POST['cuentaDebito'] = array();
                $_POST['cuentaCredito'] = array();
                $_POST['escogerCuenta'] = array();

                $sqlr = " select cuenta_deb, cuenta_cred, rubro from tesoordenpago_cuenta where id_orden=$_POST[idcomp] and tipo_mov='201' and estado='S'";
                $res = mysqli_query($linkbd_V7, $sqlr);
                while ($r = mysqli_fetch_row($res)) {
                    $_POST['cuentaDebito'][] = $r[0];
                    $_POST['cuentaCredito'][] = $r[1];
                    if (substr($r[2], 0, 5) == '2.1.1') {
                        $_POST['escogerCuenta'][] = "N";
                    } else {
                        $_POST['escogerCuenta'][] = "S";
                    }
                }

                $_POST['dcuentas'] = array();
                $_POST['dvalores'] = array();
                $_POST['dncuentas'] = array();
                $_POST['sector'] = array();
                $_POST['nombreSector'] = array();
                $_POST['productoServicio'] = array();
                $_POST['nombreProductoServicio'] = array();
                $indicadorProducto = '';
                $sqlr = "select *from tesoordenpago_det where id_orden=$_POST[idcomp] and tipo_mov='201' ";
                $res = mysqli_query($linkbd_V7, $sqlr);
                while ($r = mysqli_fetch_row($res)) {
                    $indicadorProducto = substr($r[10], 0, 2);
                    $_POST['dcuentas'][] = $r[2];
                    $_POST['dvalores'][] = $r[4];
                    $_POST['dncuentas'][] = buscacuentaccpetgastos($r[2], $maxVersion);
                    $_POST['sector'][] = $indicadorProducto;
                    $_POST['nombreSector'][] = buscaNombreSector($indicadorProducto);
                    $_POST['productoServicio'][] = $r[9];
                    $tamProductoServicio = strlen($r[9]);
                    if ($r[9] != '') {
                        if ($tamProductoServicio == 5) {
                            $_POST['nombreProductoServicio'][] = buscaservicioccpetgastos($r[9]);
                        } else {
                            $_POST['nombreProductoServicio'][] = buscaproductoccpetgastos($r[9]);
                        }
                    }
                }

                $_POST['causacion'] = 1;
            }

            if ($_POST['oculto'] == '2') {
                $_POST['dcuentas'] = array();
                $_POST['dvalores'] = array();
                $_POST['dncuentas'] = array();
                $_POST['sector'] = array();
                $_POST['nombreSector'] = array();
                $_POST['productoServicio'] = array();
                $_POST['nombreProductoServicio'] = array();
                $indicadorProducto = '';
                $sqlr = "select *from tesoordenpago_det where id_orden=$_POST[idcomp] and tipo_mov='201' ";
                $res = mysqli_query($linkbd_V7, $sqlr);
                while ($r = mysqli_fetch_row($res)) {
                    $indicadorProducto = substr($r[10], 0, 2);
                    $_POST['dcuentas'][] = $r[2];
                    $_POST['dvalores'][] = $r[4];
                    $_POST['dncuentas'][] = buscacuentaccpetgastos($r[2], $maxVersion);
                    $_POST['sector'][] = $indicadorProducto;
                    $_POST['nombreSector'][] = buscaNombreSector($indicadorProducto);
                    $_POST['productoServicio'][] = $r[9];
                    $tamProductoServicio = strlen($r[9]);
                    if ($r[9] != '') {
                        if ($tamProductoServicio == 5) {
                            $_POST['nombreProductoServicio'][] = buscaservicioccpetgastos($r[9]);
                        } else {
                            $_POST['nombreProductoServicio'][] = buscaproductoccpetgastos($r[9]);
                        }
                    }
                }
            }
            switch ($_POST['tabgroup1']) {
                case 1:
                    $check1 = 'checked';
                    break;
                case 2:
                    $check2 = 'checked';
                    break;
                case 3:
                    $check3 = 'checked';
                    break;
                case 4:
                    $check4 = 'checked';
            }
            $check6 = 'checked';
            $sqlr = "select * from tesoordenpago where id_orden=$_POST[idcomp] AND tipo_mov='201' ";
            $res = mysqli_query($linkbd_V7, $sqlr);
            while ($r = mysqli_fetch_row($res)) {
                preg_match("/([0-9]{4})\-([0-9]{2})\-([0-9]{2})/", $r[2], $fecha);
                $_POST['fecha'] = "$fecha[3]/$fecha[2]/$fecha[1]";
                $_POST['vigencia'] = $fecha[1];
                $_POST['rp'] = $r[4];
                $sql = "SELECT destino FROM tesoordenpago_almacen WHERE id_orden='$_POST[idcomp]' ";
                $res = mysqli_query($linkbd_V7, $sql);
                $fila = mysqli_fetch_row($res);
                $_POST['destino'] = $fila[0];
                $sqlr1 = "select * from tesoordenpago where id_orden=$_POST[idcomp] AND tipo_mov='401' ";
                $res1 = mysqli_query($linkbd_V7, $sqlr1);
                $cont = mysqli_num_rows($res1);
                if ($cont > 0) {
                    $_POST['estado'] = "R";
                } else {
                    $_POST['estado'] = $r[13];
                }
                $_POST['estadoc'] = $r[13];
                $_POST['medioDePago'] = $r[19];
                if ($_POST['medioDePago'] == '')
                    $_POST['medioDePago'] = '-1';
            }
            $nresul = buscaregistroccpet($_POST['rp'], $vigusu);
            $_POST['cdp'] = $nresul;
            //*** busca detalle cdp
            $sqlr = "select ccpetrp.vigencia,ccpetrp.consvigencia,ccpetcdp.objeto,ccpetrp.estado,ccpetcdp.consvigencia,ccpetrp.valor,ccpetrp.saldo, ccpetrp.tercero,ccpetcdp.objeto from ccpetrp,ccpetcdp where ccpetrp.estado='S' and ccpetcdp.consvigencia=$_POST[cdp] and ccpetrp.idcdp=ccpetcdp.consvigencia and ccpetrp.consvigencia='$_POST[rp]' and ccpetrp.vigencia=$vigusu and ccpetrp.idcdp=ccpetcdp.consvigencia and ccpetcdp.vigencia=$_POST[vigencia] and ccpetrp.tipo_mov='201' AND ccpetcdp.tipo_mov='201' order by ccpetrp.vigencia,ccpetrp.consvigencia,ccpetcdp.objeto,ccpetrp.estado";
            $resp = mysqli_query($linkbd_V7, $sqlr);
            $row = mysqli_fetch_row($resp);
            $_POST['detallecdp'] = $row[2];
            $_POST['cdp'] = $row[4];

            $sqlr = "Select *from tesoordenpago where tipo_mov='201' and id_orden=" . $_POST['idcomp'];
            $resp = mysqli_query($linkbd_V7, $sqlr);
            $row = mysqli_fetch_row($resp);
            $_POST['tercero'] = $row[6];
            $_POST['ntercero'] = buscatercero($_POST['tercero']);
            $_POST['valorrp'] = $row[8];
            $_POST['saldorp'] = $row[9];

            $_POST['valor'] = $row[10];
            $_POST['cc'] = $row[5];
            $_POST['detallegreso'] = $row[7];
            $_POST['valoregreso'] = $_POST['valor'];
            $_POST['valorretencion'] = $row[12];
            $_POST['base'] = $row[14];
            $_POST['iva'] = $row[15];
            $_POST['valorcheque'] = $_POST['valoregreso'] - $_POST['valorretencion'];
            ?>
            <div class="tabsic">
                <div class="tab">
                    <input type="radio" id="tab-6" name="tabgroup2" value="6" <?php echo $check6; ?> >
                    <label id="clabel" for="tab-6">Liquidacion CxP</label>
                    <div class="content" style="overflow-x:hidden;">
                        <table class="inicio" align="center" >
                            <tr >
                                <td class="titulos" colspan="10">Liquidacion CxP</td>
                                <td class="cerrar" style="width:7%" onClick="location.href='cont-principal.php'">Cerrar</td>
                            </tr>
                            <tr>
                                <td style="width:10%" class="saludo1" >Numero CxP:</td>
                                <td style="width:10%">
                                    <img src="imagenes/back.png" title="anterior" onClick="atrasc()"  class="icobut"/>&nbsp;<input name="idcomp" type="text" style="width:50%" value="<?php echo $_POST['idcomp'] ?>" onBlur="validar2()" onKeyUp="return tabular(event,this)"/>&nbsp;<img src="imagenes/next.png" title="siguiente" onClick="adelante()"  class="icobut"/>
                                </td>
                                <input name="ncomp" type="hidden" value="<?php echo $_POST['ncomp'] ?>">
                                <input name="compcont" type="hidden" value="<?php echo $_POST['compcont'] ?>">
                                <input type="hidden" value="a" name="atras" >
                                <input type="hidden" value="s" name="siguiente" >
                                <input type="hidden" value="<?php echo $_POST['maximo'] ?>" name="maximo">
                                <input type="hidden" value="<?php echo $_POST['codrec'] ?>" name="codrec" id="codrec">
                                <td style="width:2.5cm" class="saludo1">Fecha: </td>
                                <td style="width:6%"><input name="fecha" type="text" value="<?php echo $_POST['fecha'] ?>" style="width:100%" onKeyUp="return tabular(event,this)" readonly/></td>
                                <td style="width:2.5cm" class="saludo1">Vigencia: </td>
                                <td style="width:10%"><input name="vigencia" type="text" value="<?php echo $_POST['vigencia'] ?>"  style="width:40%" onKeyUp="return tabular(event,this)" readonly/>&nbsp;<input name="estadoc" type="text" value="<?php echo $_POST['estadoc'] ?>" style="width:40%" readonly/></td>
                                <input name="estado" type="hidden" value="<?php echo $_POST['estado'] ?>">
                                <td style="width:3.5cm" class="saludo1">Causacion Contable:</td>
                                <td style="width:5%">
                                    <select name="causacion" id="causacion" onKeyUp="return tabular(event,this)" disabled>
                                        <option value="1" <?php if ($_POST['causacion'] == '1')
                                            echo "selected" ?> >Si</option>
                                            <option value="2" <?php if ($_POST['causacion'] == '2')
                                            echo "selected" ?> >No</option>
                                        </select>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="saludo1">Registro:</td>
                                    <td><input name="rp" type="text" value="<?php echo $_POST['rp'] ?>" style="width:100%" onKeyUp="return tabular(event,this)" readonly/></td>
                                <input type="hidden" value="0" name="brp">
                                <td class="saludo1">CDP:</td>
                                <td><input type="text" id="cdp" name="cdp" value="<?php echo $_POST['cdp'] ?>" style="width:100%" readonly/></td>
                                <td class="saludo1">Detalle RP:</td>
                                <td colspan="5"><input type="text" id="detallecdp" name="detallecdp" value="<?php echo $_POST['detallecdp'] ?>" style="width:100%" readonly/></td>
                            </tr>
                            <tr>

                                <td class="saludo1">Tercero:</td>
                                <td><input id="tercero" type="text" name="tercero" style="width:100%" onKeyUp="return tabular(event,this)" value="<?php echo $_POST['tercero'] ?>" readonly/></td>
                                <td colspan="4"><input type="text" id="ntercero" name="ntercero" value="<?php echo $_POST['ntercero'] ?>" style="width:100%" readonly/></td>
                                <td class="saludo1" style="width:3cm;">Medio de pago: </td>
                                <td style="width:17%;">
                                    <select name="medioDePago" id="medioDePago" onKeyUp="return tabular(event,this)" disabled style="width:80%">
                                        <option value="-1" <?php if (($_POST['medioDePago'] == '-1'))
                                            echo "SELECTED"; ?>>Seleccione...</option>
                                        <option value="1" <?php if (($_POST['medioDePago'] == '1'))
                                            echo "SELECTED"; ?>>Con SF</option>
                                        <option value="2" <?php if ($_POST['medioDePago'] == '2')
                                            echo "SELECTED"; ?>>Sin SF</option>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td class="saludo1">Detalle Orden:</td>
                                <td colspan="9"><input type="text" id="detallegreso" name="detallegreso" value="<?php echo $_POST['detallegreso'] ?>" style="width:100%"  readonly ></td>
                            </tr>
                            <tr>
                                <td class="saludo1">Valor RP:</td>
                                <td><input type="text" id="valorrp" name="valorrp" value="<?php echo number_format($_POST['valorrp'], 2, ',', '.') ?>" style="width:100%; text-align:right;" onKeyUp="return tabular(event,this)" readonly/></td>
                                <td class="saludo1">Saldo RP:</td>
                                <td><input type="text" id="saldorp" name="saldorp"  value="<?php echo number_format($_POST['saldorp'], 2, ',', '.') ?>" style="width:100%; text-align:right;" onKeyUp="return tabular(event,this)" readonly/></td>
                                <td class="saludo1" >Valor a Pagar:</td>
                                <td><input type="text" id="valor" name="valor" value="<?php echo number_format($_POST['valor'], 2, ',', '.') ?>" style="width:100%; text-align:right;" onKeyUp="return tabular(event,this)" readonly/></td>
                                <input type="hidden" value="1" name="oculto"/>
                                <input type="hidden" value="1" name="vari"/>
                                <td class="saludo1" >Base:</td>
                                <td><input type="text" id="base" name="base" value="<?php echo number_format($_POST['base'], 2, ',', '.') ?>" style="width:100%; text-align:right;" onKeyUp="return tabular(event,this)"  readonly/></td>
                                <td class="saludo1" >Iva:</td>
                                <td><input type="text" id="iva" name="iva" value="<?php echo number_format($_POST['iva'], 2, ',', '.') ?>" style="width:100%; text-align:right;" onKeyUp="return tabular(event,this)"  readonly/></td>
                            </tr>
                        </table>
                    </div>
                </div>

            </div>
            <div class="tabsic">
                <div class="tab">
                    <input type="radio" id="tab-1" name="tabgroup1" value="1" <?php echo $check1; ?> >
                    <label id="clabel" for="tab-1">Detalle</label>

                    <div class="content" style="overflow-x:hidden; height:250px" >
                            <?php
                            //*** busca contenido del rp
                            $_POST['dcuentas'] = array();
                            $_POST['dvaloresoc'] = array();
                            $_POST['dncuentas'] = array();
                            $_POST['dvigenciaGasto'] = array();
                            $_POST['dseccion_presupuestal'] = array();
                            $_POST['dtipo_gasto'] = array();
                            $_POST['ddivipola'] = array();
                            $_POST['dchip'] = array();
                            $_POST['rubros'] = array();
                            $_POST['inicios'] = array();
                            $_POST['drecursos'] = array();
                            $_POST['productoServicio'] = array();
                            $_POST['indicador_producto'] = array();
                            $_POST['medio_pago'] = array();
                            $_POST['bpim'] = array();
                            $_POST['dnrecursos'] = array();
                            $_POST['sector'] = array();
                            $_POST['dcuentasCheck'] = array();
                            $_POST['nombreSector'] = array();

                            if ($_POST['vari'] != '3') {
                                $_POST['inicios'] = array();
                            }
                            $sqlr = "select *from tesoordenpago_det where id_orden=$_POST[idcomp] and tipo_mov='201' ";
                            $res = mysqli_query($linkbd_V7, $sqlr);
                            while ($r = mysqli_fetch_row($res)) {
                                //echo "hola".$_POST[vari];
                                $consec = $r[0];
                                $_POST['dcuentas'][] = $r[2];
                                $_POST['dvaloresoc'][] = $r[4];
                                $_POST['dncuentas'][] = buscacuentaccpetgastos($r[2], $maxVersion);
                                $_POST['dvigenciaGasto'][] = $r[12];
                                $_POST['dseccion_presupuestal'][] = $r[15];
                                $_POST['dtipo_gasto'][] = $r[16];
                                $_POST['ddivipola'][] = $r[17];
                                $_POST['dchip'][] = $r[18];
                                $_POST['rubros'][] = $r[2];
                                $_POST['inicios'][] = $r[2];
                                $nfuente = buscafuenteccpet($r[8]);
                                $cdfuente = substr($nfuente, 0, strpos($nfuente, "_"));
                                $_POST['drecursos'][] = $r[8];

                                $_POST['productoServicio'][] = $r[9];
                                $_POST['indicador_producto'][] = $r[10];
                                $_POST['medio_pago'][] = $r[11];
                                $_POST['bpim'][] = $r[14];
                                $indicadorProducto = substr($r[10], 0, 2);
                                $_POST['dnrecursos'][] = $nfuente;
                                $_POST['sector'][] = $indicadorProducto;
                                $_POST['nombreSector'][] = buscaNombreSector($indicadorProducto);


                                /* $_POST['dcuentasCheck'][]=$r[2].''.$r[8].''.$r[9].''.$r[10];
                                                           $_POST['drecursos'][]=$r[8];
                                                           $_POST['indicador_producto'][]=$r[10];
                                                           $_POST['productoservicio'][]=$r[9];

                                                           $_POST['rubros'][]=$r[2];
                                                           if($_POST['vari']!='3')
                                                           {
                                                               $_POST['inicios'][$r[2].''.$r[8].''.$r[9].''.$r[10]]=$r[2].''.$r[8].''.$r[9].''.$r[10];
                                                           }
                                                           $_POST['dvalores'][]=$r[4];
                                                           $_POST['dncuentas'][]=buscacuentaccpetgastos($r[2], $maxVersion); */
                            }
                            ?>
                            <table class="inicio">
                                <tr><td colspan="11" class="titulos">Detalle Orden de Pago</td></tr>
                                <?php
                                if ($_POST['inicia'] == 1) {
                                    $checkint = 'checked';
                                } else {
                                    $checkint = '';
                                }
                                ?>
                                <tr>

                                    <td class='titulos2' style='text-align:center;'>Vig. Gasto</td>
                                    <td class='titulos2' style='text-align:center;'>Sec. presupuestal</td>
                                    <td class='titulos2' style='text-align:center;'>Medio pago</td>
                                    <td class="titulos2">Cuenta</td>
                                    <td class="titulos2">Nombre Cuenta</td>
                                    <td class="titulos2">Fuente</td>
                                    <td class='titulos2' style='text-align:center;'>Producto/Servicio</td>
                                    <td class='titulos2' style='width:10%;text-align:center;'>Programatico MGA</td>
                                    <td class='titulos2' style='text-align:center;'>BPIM</td>
                                    <td class="titulos2">Valor</td>
                                </tr>
                                <?php
                                $_POST['totalc'] = 0;
                                $iter = 'saludo1a';
                                $iter2 = 'saludo2';
                                for ($x = 0; $x < count($_POST['dcuentas']); $x++) {
                                    /* $chs='';
                                                                  $ck = esta_en_array($_POST['dcuentasCheck'],$_POST['inicios'][$_POST['dcuentas'][$x].''.$_POST['drecursos'][$x].''.$_POST['productoservicio'][$x].''.$_POST['indicador_producto'][$x]]);
                                                                  if($ck=='1')
                                                                  {
                                                                      $chs="checked";
                                                                      $_POST['token']=$_POST['token']+$_POST['dvalores'][$x];
                                                                  }
                                                                  $sql1="SELECT regalias FROM pptocuentas WHERE cuenta='".$_POST['dcuentas'][$x]."' AND (vigencia='$vigusu' or vigenciaf='$vigusu')";
                                                                  $rst1=mysql_query($sql1,$linkbd);
                                                                  $ar=mysql_fetch_row($rst1);
                                                                  echo "
                                                                  <tr class='$iter' style=\"cursor: hand \" onMouseOver=\"anterior=this.style.backgroundColor; this.style.backgroundColor='#40b3ff';\" onMouseOut=\"this.style.backgroundColor=anterior\">
                                                                      <td style='width:3%;'>
                                                                          <input type='checkbox'

                                                                              name='inicios[".$_POST['dcuentas'][$x]."".$_POST['drecursos'][$x]."".$_POST['productoservicio'][$x]."".$_POST['indicador_producto'][$x]."]'
                                                                              value='".$_POST['dcuentas'][$x]."".$_POST['drecursos'][$x]."".$_POST['productoservicio'][$x]."".$_POST['indicador_producto'][$x]."'

                                                                              onClick='checker()' $chs>
                                                                      </td>
                                                                      <td style='width:20%'><input name='dcuentas[]' value='".$_POST['dcuentas'][$x]."' type='hidden'><input name='dregalias[]' value='".$ar[0]."' type='hidden'><input name='rubros[]' value='".$_POST['rubros'][$x]."' type='hidden'>".$_POST['dcuentas'][$x]."</td>
                                                                      <td style='width:60%'><input name='dncuentas[]' value='".$_POST['dncuentas'][$x]."' type='hidden'>".$_POST['dncuentas'][$x]."</td>
                                                                      <td style='width:10%'><input name='drecursos[]' value='".$_POST['drecursos'][$x]."' type='hidden'>".$_POST['drecursos'][$x]."</td>
                                                                      <td align='right'><input name='dvalores[]' value='".$_POST['dvalores'][$x]."' type='hidden'>".number_format($_POST['dvalores'][$x],2,',','.')."</td>
                                                                  </tr>";
                                                                  $_POST['totalc']=$_POST['totalc']+$_POST['dvalores'][$x];
                                                                  $_POST['totalcf']=number_format($_POST['totalc'],2,".",","); */

                                    echo "
										<input type='hidden' name='dcuentas[]' value='" . $_POST['dcuentas'][$x] . "'/>
										<input type='hidden' name='dregalias[]' value='" . $ar[0] . "'/>
										<input type='hidden' name='dncuentas[]' value='" . $_POST['dncuentas'][$x] . "'/>
										<input type='hidden' name='dvigenciaGasto[]' value='" . $_POST['dvigenciaGasto'][$x] . "'/>
										<input type='hidden' name='dseccion_presupuestal[]' value='" . $_POST['dseccion_presupuestal'][$x] . "'/>
										<input type='hidden' name='dtipo_gasto[]' value='" . $_POST['dtipo_gasto'][$x] . "'/>
										<input type='hidden' name='ddivipola[]' value='" . $_POST['ddivipola'][$x] . "'/>
										<input type='hidden' name='dchip[]' value='" . $_POST['dchip'][$x] . "'/>
										<input type='hidden' name='drecursos[]' value='" . $_POST['drecursos'][$x] . "'/>
										<input type='hidden' name='dnrecursos[]' value='" . $_POST['dnrecursos'][$x] . "'/>
										<input type='hidden' name='productoServicio[]' value='" . $_POST['productoServicio'][$x] . "'/>
										<input type='hidden' name='nombreProductoServicio[]' value='" . $_POST['nombreProductoServicio'][$x] . "'/>
										<input type='hidden' name='indicador_producto[]' value='" . $_POST['indicador_producto'][$x] . "'/>
										<input type='hidden' name='bpim[]' value='" . $_POST['bpim'][$x] . "'/>
										<input type='hidden' name='medio_pago[]' value='" . $_POST['medio_pago'][$x] . "'/>
										<input type='hidden' name='sector[]' value='" . $_POST['sector'][$x] . "'/>
										<input type='hidden' name='nombreSector[]' value='" . $_POST['nombreSector'][$x] . "'/>
										<input type='hidden' name='dvalores[]' value='" . $_POST['dvalores'][$x] . "'/>
										<input type='hidden' name='dvaloresoc[]' value='" . $_POST['dvaloresoc'][$x] . "'/>
										<tr  class='$iter' onMouseOver=\"anterior=this.style.backgroundColor;this.style.backgroundColor='#40b3ff';\"
					onMouseOut=\"this.style.backgroundColor=anterior\" style='text-transform:uppercase'>

											<td>" . $_POST['dvigenciaGasto'][$x] . "</td>
											<td>" . $_POST['dseccion_presupuestal'][$x] . "</td>
											<td>" . $_POST['medio_pago'][$x] . "</td>
											<td style='width:10%;'>" . $_POST['dcuentas'][$x] . "</td>
											<td style='width:30%;'>" . $_POST['dncuentas'][$x] . "</td>
											<td style='width:15%;'>" . $_POST['drecursos'][$x] . " - " . $_POST['dnrecursos'][$x] . "</td>
											<td style='width:15%;'>" . $_POST['productoServicio'][$x] . " - " . $_POST['nombreProductoServicio'][$x] . "</td>
											<td style='width:6%;'>" . $_POST['indicador_producto'][$x] . "</td>
											<td style='width:8%;'>" . $_POST['bpim'][$x] . "</td>
											<td style='text-align:right;width:15%;' ";
                                    if ($ch == '1') {
                                        echo "onDblClick='llamarventanaegre(this,$x);'";
                                    }
                                    echo " >$ " . number_format($_POST['dvalores'][$x], 2, '.', ',') . "</td>
										</tr>";

                                    $_POST['totalc'] = $_POST['totalc'] + $_POST['dvalores'][$x];
                                    $_POST['totalcf'] = number_format($_POST['totalc'], 2, ".", ",");

                                    $aux = $iter;
                                    $iter = $iter2;
                                    $iter2 = $aux;


                                }


                                $resultado = convertir($_POST['totalc']);
                                $_POST['letras'] = $resultado . " PESOS M/CTE";
                                echo "
									<input name='totalcf' type='hidden' value='$_POST[totalcf]' readonly>
									<input name='totalc' type='hidden' value='$_POST[totalc]'>
									<tr class='titulos2'>
										<td colspan='8'></td>
										<td>Total</td>
										<td align='right'>" . number_format($_POST['totalc'], 2, ',', '.') . "</td>
									</tr>
									<tr class='titulos2'>
										<td>Son:</td>
										<td colspan='9'><input name='letras' type='hidden' value='$_POST[letras]'>$_POST[letras]</td>
									</tr>";
                                ?>
                            </table>
                    </div>
                </div>
                <div class="tab">
                    <input type="radio" id="tab-2" name="tabgroup1" value="2" <?php echo $check2; ?>>
                    <label id="clabel" for="tab-2">Retenciones</label>
                    <div class="content" style="overflow-x:hidden; height:250px">
                        <table class="inicio" style="overflow:scroll">
                            <tr>
                                <td class="titulos">Descuento</td>
                                <td class="titulos">%</td>
                                <td class="titulos">Valor</td>
                            </tr>
                            <?php
                            $totaldes = 0;
                            $sqlr = "select *from tesoordenpago_retenciones where id_orden='$_POST[idcomp]'";
                            $res = mysqli_query($linkbd_V7, $sqlr);
                            while ($row = mysqli_fetch_row($res)) {
                                $sqlr = "select *from tesoretenciones where id='$row[0]'";
                                $res2 = mysqli_query($linkbd_V7, $sqlr);
                                $row2 = mysqli_fetch_row($res2);
                                echo "
								<tr>
									<td class='saludo2'>
										<input name='dndescuentos[]' value='" . $row2[2] . "' type='hidden'>
										<input name='ddescuentos[]' value='" . $row[0] . "' type='hidden'>" . $row2[2] . "
									</td>
									<td class='saludo2'>
										<input name='dporcentajes[]' value='" . $row[2] . "' type='hidden'>" . $row[2] . "
									</td>
									<td class='saludo2'>
										<input name='ddesvalores[]' value='" . $row[3] . "' type='hidden'>" . $row[3] . "
									</td>
								</tr>";
                                $totaldes = $totaldes + $row[3];
                            }
                            ?>
                        <script>document.form2.totaldes.value=<?php echo $totaldes; ?>;</script>
                    </table>
                </div>
            </div>
                <div class="tab">
                    <input type="radio" id="tab-3" name="tabgroup1" value="3" <?php echo $check3; ?>>
                     <label id="clabel" for="tab-3">Cuenta por Pagar</label>
                    <div class="content" style="overflow-x:hidden; height:250px">
                        <table class="inicio" align="center">
                            <tr>
                                <td colspan="6" class="titulos">Cheque</td>
                                <td class="cerrar" style="width:7%" onClick="location.href='cont-principal.php'">Cerrar</td>
                            </tr>
                            <tr>
                                <td style="width:2.5cm" class="saludo1">Cuenta Contable:</td>
                                <td style="width:20%"><input name="cuentapagar" type="text" value="<?php echo $_POST['cuentapagar'] ?>" style="width:100%; text-align:center" readonly/></td>
                                <input name="cb" type="hidden" value="<?php echo $_POST['cb'] ?>"/>
                                <input type="hidden" id="ter" name="ter" value="<?php echo $_POST['ter'] ?>"/>
                                <td style="width:50%" colspan="4"></td>
                            </tr>
                            <tr>
                                <td class="saludo1">Valor Orden de Pago:</td>
                                <td><input type="text" id="valoregreso" name="valoregreso" value="<?php echo number_format($_POST['valoregreso'], 2, ',', '.') ?>" style="width:100%; text-align:right" readonly/></td>
                                <td class="saludo1">Valor Retenciones:</td>
                                <td align="right"><input type="text" id="valorretencion" name="valorretencion" value="<?php echo number_format($_POST['valorretencion'], 2, ',', '.') ?>" style="width:100%; text-align:right" readonly/></td>
                                <td class="saludo1">Valor Cta Pagar:</td>
                                <td align="right"><input type="text" id="valorcheque" name="valorcheque" value="<?php echo number_format($_POST['valorcheque'], 2, ',', '.') ?>" style="width:100%; text-align:right" readonly/></td>
                            </tr>
                        </table>
                    </div>
                </div>

                <div class="tab">

                    <input type="radio" id="tab-4" name="tabgroup1" value="4" <?php echo $check4; ?>>
                    <label id="clabel" for="tab-4" >Contabilidad</label>
                    <div class="content" style="overflow-x:hidden; height:250px">
                        <table class="inicio">
                            <tr><td colspan="13" class="titulos">Parametrizar Progranacion Contable</td></tr>
                            <tr>

                                <td class='titulos2' style='text-align:center;'>Vig. Gasto</td>
                                <td class='titulos2' style='text-align:center;'>Sec. presupuestal</td>
                                <td class='titulos2' style='text-align:center;'>Cuenta</td>
                                <td class='titulos2' style='text-align:center;'>Nombre Cuenta</td>
                                <td class='titulos2' style='text-align:center;'>Fuente</td>
                                <td class='titulos2' style='text-align:center;'>Sector</td>
                                <td class='titulos2' style='text-align:center;'>Producto/Servicio</td>
                                <td class='titulos2' style='text-align:center;'>Programativo MGA</td>
                                <td class='titulos2' style='text-align:center;'>Medio de Pago</td>
                                <td class='titulos2' style='text-align:center;'>Valor</td>
                                <td class='titulos2' style='text-align:center;'>Cuenta debito</td>
                                <td class='titulos2' style='text-align:center;'>Cuenta Credito</td>
                                <td class='titulos2' style='text-align:center;'>Contabiliza</td>

                            </tr>
                            <?php
                            $iter = 'saludo1a';
                            $iter2 = 'saludo2';
                            for ($x = 0; $x < count($_POST['dcuentas']); $x++) {
                                $sqlrCuentaCont = "SELECT DCD.cuenta_debito, DCD.cuenta_credito, DCD.contabiliza FROM ccpetdc_detalle AS DCD, ccpetdc AS DC WHERE DC.vigencia = '$_POST[vigencia]' AND DC.consvigencia = '$_POST[rp]' AND DC.id = DCD.consvigencia AND DCD.cuenta = '" . $_POST['dcuentas'][$x] . "' AND DCD.productoservicio = '" . $_POST['productoServicio'][$x] . "' AND DCD.fuente = '" . $_POST['drecursos'][$x] . "' AND DCD.indicador_producto = '" . $_POST['indicador_producto'][$x] . "' AND DCD.cod_vigenciag = '" . $_POST['dvigenciaGasto'][$x] . "' AND DCD.medio_pago = '" . $_POST['medio_pago'][$x] . "' AND DCD.seccion_presupuestal = '" . $_POST['dseccion_presupuestal'][$x] . "' AND DCD.bpim = '" . $_POST['bpim'][$x] . "'";//echo $sqlrCuentaCont."<br>";
                                $resCuentaCont = mysqli_query($linkbd_V7, $sqlrCuentaCont);
                                $rowCuentaCont = mysqli_fetch_row($resCuentaCont);

                                if ($rowCuentaCont[2] == 'S') {
                                    $_POST["idswsino$x"] = 'checked';
                                }
                                echo "
									<tr  class='$iter'  style='text-transform:uppercase'>
										<td style='width:10%;'>" . $_POST['dvigenciaGasto'][$x] . "</td>
										<td style='width:10%;'>" . $_POST['dseccion_presupuestal'][$x] . "</td>
										<td style='width:10%;'>" . $_POST['dcuentas'][$x] . "</td>
										<td style='width:20%;'>" . $_POST['dncuentas'][$x] . "</td>
										<td style='width:20%;'>" . $_POST['drecursos'][$x] . " - " . $_POST['dnrecursos'][$x] . "</td>
										<td style='width:15%;'>" . $_POST['sector'][$x] . " - " . $_POST['nombreSector'][$x] . "</td>
										<td style='width:15%;'>" . $_POST['productoServicio'][$x] . " - " . $_POST['nombreProductoServicio'][$x] . "</td>
										<td style='width:8%;'>" . $_POST['indicador_producto'][$x] . "</td>
										<td style='width:4%;'>" . $_POST['medio_pago'][$x] . "</td>
										<td style='width:10%;'>" . number_format($_POST['dvalores'][$x], 2, ',', '.') . "</td>
										<td style='width:10%;'><input name='cuentaDebito[]' class='inpnovisibles' value='" . $rowCuentaCont[0] . "' readonly></td>
										<td style='width:10%;'>
											<input name='cuentaCredito[]' class='inpnovisibles' value='" . $rowCuentaCont[1] . "' readonly>
											<input name='contabiliza[]' type='hidden' value='" . $rowCuentaCont[2] . "' readonly>
										</td>
										<td style='text-align:center;'>
											<div class='swsino'>
												<input type='checkbox' disabled name='idswsino$x' class='swsino-checkbox' id='idswsino$x' value='" . $_POST["idswsino$x"] . "' " . $_POST["idswsino$x"] . " onChange=\"cambiocheck('$x');\">
												<label class='swsino-label' for='idswsino$x'>
													<span class='swsino-inner'></span>
													<span class='swsino-switch'></span>
												</label>
											</div>
										</td>";

                                echo "
									</tr>";
                                $aux = $iter;
                                $iter = $iter2;
                                $iter2 = $aux;
                            }
                            ?>
                        </table>
                    </div>
                </div>
            </div>
            <?php
            if ($_POST['oculto'] == '2') {
                preg_match("/([0-9]{2})\/([0-9]{2})\/([0-9]{4})/", $_POST['fecha'], $fecha);
                $fechaf = "$fecha[3]-$fecha[2]-$fecha[1]";
                $bloq = bloqueos($_SESSION['cedulausu'], $fechaf);
                if ($bloq >= 1) {
                    $query = "SELECT conta_pago FROM tesoparametros";
                    $resultado = mysqli_query($linkbd_V7, $query);
                    $arreglo = mysqli_fetch_row($resultado);
                    $opcion = $arreglo[0];
                    $vigencia = $_POST['vigencia'];
                    if ($_POST['causacion'] == '2') {
                        $_POST['detallegreso'] = "ESTE DOCUMENTO NO REQUIERE CAUSACION CONTABLE - " . $_POST['detallegreso'];
                    }
                    $consec = $_POST['idcomp'];
                    if ($_POST['estado'] == 'N') {
                        $sqlr = "update comprobante_cab  set estado=0 where numerotipo=$_POST[idcomp] and tipo_comp=11";
                        mysqli_query($linkbd_V7, $sqlr);
                    } else if ($_POST['estado'] == 'R') {

                    } else {
                        $sqlr = "delete from comprobante_cab  where numerotipo='$_POST[idcomp]' and tipo_comp=11";
                        mysqli_query($linkbd_V7, $sqlr);
                        $sqlr = "delete from comprobante_det where id_comp='11 $_POST[idcomp]'";
                        mysqli_query($linkbd_V7, $sqlr);
                        echo "<table class='inicio'><tr><td class='saludo1'><center>Se ha Actualizado la Orden de Pago con Exito <img src='imagenes\confirm.png'></center></td></tr></table>";
                        //***cabecera comprobante CXP LIQUIDADA
                        $sqlr = "insert into comprobante_cab (numerotipo,tipo_comp,fecha,concepto,total,total_debito,total_credito,diferencia,estado) values ( $consec,11,'$fechaf','$_POST[detallegreso]',0,0,0,0,'1')";
                        mysqli_query($linkbd_V7, $sqlr);
                        $idcomp = $_POST['idcomp'];
                        //******************* DETALLE DEL COMPROBANTE CONTABLE  *********************
                        $arreglocuenta = array();
                        for ($j = 0; $j < count($_POST['dcuentas']); $j++) {
                            if ($opcion == "1") {
                                $cantDescuentos = count($_POST['dndescuentos']);
                                for ($x = 0; $x < $cantDescuentos; $x++) {
                                    $dd = $_POST['ddescuentos'][$x];

                                    $sqlr = "select * from tesoretenciones, tesoretenciones_det where tesoretenciones_det.codigo=tesoretenciones.id and tesoretenciones.id='" . $dd . "' ORDER BY porcentaje DESC";

                                    $resdes = mysqli_query($linkbd_V7, $sqlr);
                                    $valordes = 0;
                                    $valorProcentaje = 0;
                                    $idRetencion = '';
                                    while ($rowdes = mysqli_fetch_assoc($resdes)) {
                                        $valordes = 0;
                                        if ($idRetencion == $rowdes['codigo']) {
                                            $valorProcentaje = $val2 + $rowdes['porcentaje'];
                                        } else {
                                            $valorProcentaje = $rowdes['porcentaje'];
                                            $idRetencion = $rowdes['codigo'];
                                        }

                                        $val2 = 0;
                                        $val2 = $rowdes['porcentaje'];
                                        if ($_POST['iva'] > 0 && $rowdes['terceros'] == 1) {
                                            $val1 = $_POST['dvalores'][$j];
                                            $val3 = $_POST['ddesvalores'][$x];
                                            $valordes = round(($val1 / $_POST['valor']) * ($val2 / 100) * $val3, 0);
                                        } else {
                                            $val1 = $_POST['dvalores'][$j];
                                            $val3 = $_POST['ddesvalores'][$x];
                                            $valordes = round(($val1 / $_POST['valor']) * ($val2 / 100) * $val3, 0);
                                        }
                                        $nomDescuento = $_POST['dndescuentos'][$x];

                                        $sec_presu = intval($_POST['dseccion_presupuestal'][$j]);
                                        $sqlrCc = "SELECT id_cc FROM centrocostos_seccionpresupuestal WHERE (id_sp = $sec_presu OR id_sp = '" . $_POST['dseccion_presupuestal'][$j] . "')";
                                        $resCc = mysqli_query($linkbd_V7, $sqlrCc);
                                        $rowCc = mysqli_fetch_row($resCc);
                                        $cc = $rowCc[0];

                                        $codigoRetencion = 0;
                                        $rest = 0;
                                        $codigoCausa = 0;
                                        if ($_POST['medioDePago'] != '1') {
                                            //concepto contable //********************************************* */
                                            $rest = substr($rowdes['tipoconce'], 0, 2);
                                            $sq = "select fechainicial from conceptoscontables_det where codigo='" . $rowdes['conceptocausa'] . "' and modulo='" . $rowdes['modulo'] . "' and tipo='$rest' and fechainicial<'$fechaf' and cuenta!='' order by fechainicial asc";
                                            $re = mysqli_query($linkbd_V7, $sq);
                                            while ($ro = mysqli_fetch_assoc($re)) {
                                                $_POST['fechacausa'] = $ro["fechainicial"];
                                            }

                                            $sqlr = "select * from conceptoscontables_det where codigo='" . $rowdes['conceptocausa'] . "' and modulo='" . $rowdes['modulo'] . "' and cc='" . $cc . "' and tipo='$rest' and fechainicial='" . $_POST['fechacausa'] . "'";
                                            //echo $sqlr." hoal <br>";
                                            $rst = mysqli_query($linkbd_V7, $sqlr);
                                            $row1 = mysqli_fetch_assoc($rst);
                                            if ($row1['cuenta'] != '' && $valordes > 0) {
                                                //echo "Hola 5  ";
                                                $sqlr = "insert into comprobante_det (id_comp,cuenta,tercero,centrocosto,detalle, cheque,valdebito, valcredito, estado, vigencia) values ('11 $consec','" . $row1['cuenta'] . "','" . $_POST['tercero'] . "' ,'" . $cc . "' , 'Descuento " . $nomDescuento . "',''," . $valordes . ",0,'1' ,'" . $vigencia . "')";
                                                mysqli_query($linkbd_V7, $sqlr);
                                            }

                                            $rest = substr($rowdes['tipoconce'], -2);
                                            $sq = "select fechainicial from conceptoscontables_det where codigo='" . $rowdes['conceptoingreso'] . "' and modulo='" . $rowdes['modulo'] . "' and tipo='$rest' and fechainicial<'$fechaf' and cuenta!='' order by fechainicial asc";
                                            $re = mysqli_query($linkbd_V7, $sq);
                                            while ($ro = mysqli_fetch_assoc($re)) {
                                                $_POST['fechacausa'] = $ro["fechainicial"];
                                            }

                                            $sqlr = "select * from conceptoscontables_det where codigo='" . $rowdes['conceptoingreso'] . "' and modulo='" . $rowdes['modulo'] . "' and cc='" . $cc . "' and tipo='$rest' and fechainicial='" . $_POST['fechacausa'] . "'";
                                            $rst = mysqli_query($linkbd_V7, $sqlr);
                                            $row1 = mysqli_fetch_assoc($rst);
                                            if ($row1['cuenta'] != '' && $valordes > 0) {
                                                $sqlr = "insert into comprobante_det (id_comp,cuenta,tercero,centrocosto,detalle, cheque,valdebito, valcredito, estado, vigencia) values ('11 $consec','" . $row1['cuenta'] . "','" . $_POST['tercero'] . "' ,'" . $cc . "' , 'Descuento " . $nomDescuento . "','',0," . $valordes . ",'1' ,'" . $vigencia . "')";
                                                mysqli_query($linkbd_V7, $sqlr);

                                                if ($valorProcentaje <= 100) {
                                                    $sqlr = "insert into comprobante_det (id_comp,cuenta,tercero,centrocosto,detalle,cheque,valdebito, valcredito,estado,vigencia) values ('11 $consec','" . $_POST['cuentaCredito'][$j] . "','$_POST[tercero]','" . $cc . "','Causacion " . $_POST['dncuentas'][$j] . "','','" . $valordes . "','0','1', '$_POST[vigencia]')";
                                                    mysqli_query($linkbd_V7, $sqlr);
                                                }
                                            }

                                            $rest = "SR";
                                            $sq = "select fechainicial from conceptoscontables_det where codigo='" . $rowdes['conceptosgr'] . "' and modulo='" . $rowdes['modulo'] . "' and tipo='$rest' and fechainicial<'$fechaf' and cuenta!='' order by fechainicial asc";
                                            $re = mysqli_query($linkbd_V7, $sq);
                                            while ($ro = mysqli_fetch_assoc($re)) {
                                                $_POST['fechacausa'] = $ro["fechainicial"];
                                            }
                                            $sqlr = "select * from conceptoscontables_det where codigo='" . $rowdes['conceptosgr'] . "' and modulo='" . $rowdes['modulo'] . "' and cc='" . $cc . "' and tipo='$rest' and fechainicial='" . $_POST['fechacausa'] . "'";
                                            $rst = mysqli_query($linkbd_V7, $sqlr);
                                            $row1 = mysqli_fetch_assoc($rst);
                                            if ($row1['cuenta'] != '' && $valordes > 0) {
                                                $sqlr = "insert into comprobante_det (id_comp,cuenta,tercero,centrocosto,detalle, cheque,valdebito, valcredito, estado, vigencia) values ('11 $consec','" . $row1['cuenta'] . "','" . $_POST['tercero'] . "' ,'" . $cc . "' , 'Descuento " . $nomDescuento . "','',0," . $valordes . ",'1' ,'" . $vigencia . "')";
                                                mysqli_query($linkbd_V7, $sqlr);
                                            }
                                            continue;
                                        } else {
                                            $codigoIngreso = $rowdes['conceptoingreso'];
                                            if ($codigoIngreso != "-1") {
                                                $codigoRetencion = $rowdes['conceptoingreso'];
                                                $rest = substr($rowdes['tipoconce'], -2);
                                                $val2 = $rowdes['porcentaje'];
                                            }
                                        }
                                        $sq = "select fechainicial from conceptoscontables_det where codigo='$codigoRetencion' and modulo='" . $rowdes['modulo'] . "' and tipo='$rest' and fechainicial<'$fechaf' and cuenta!='' order by fechainicial asc";
                                        $re = mysqli_query($linkbd_V7, $sq);
                                        while ($ro = mysqli_fetch_assoc($re)) {
                                            $_POST['fechacausa'] = $ro["fechainicial"];
                                        }
                                        $sqlr = "select * from conceptoscontables_det where codigo='$codigoRetencion' and modulo='" . $rowdes['modulo'] . "' and cc='" . $cc . "' and tipo='$rest' and fechainicial='" . $_POST['fechacausa'] . "'";
                                        $rst = mysqli_query($linkbd_V7, $sqlr);
                                        $row1 = mysqli_fetch_assoc($rst);
                                        if ($row1['cuenta'] != '' && $valordes > 0) {
                                            $sqlr = "insert into comprobante_det (id_comp,cuenta,tercero,centrocosto,detalle, cheque,valdebito, valcredito, estado, vigencia) values ('11 $consec','" . $row1['cuenta'] . "','" . $_POST['tercero'] . "' ,'" . $cc . "' , 'Descuento " . $nomDescuento . "','',0," . $valordes . ",'1' ,'" . $vigencia . "')";
                                            mysqli_query($linkbd_V7, $sqlr);
                                            if ($valorProcentaje <= 100) {
                                                $sqlr = "insert into comprobante_det (id_comp,cuenta,tercero,centrocosto,detalle,cheque,valdebito, valcredito,estado,vigencia) values ('11 $consec','" . $_POST['cuentaCredito'][$j] . "','$_POST[tercero]','" . $cc . "','Causacion " . $_POST['dncuentas'][$j] . "','','" . $valordes . "','0','1', '$_POST[vigencia]')";
                                                mysqli_query($linkbd_V7, $sqlr);
                                            }
                                        }
                                        $realizaCausacion = 0;
                                        if ($rowdes['terceros'] != 1 || $rowdes['tipo'] == 'C') {
                                            $realizaCausacion = 1;
                                            if ($rowdes['destino'] != 'M' && $rowdes['tipo'] == 'C') {
                                                $realizaCausacion = 0;
                                            }
                                        }
                                        if ($rowdes['conceptocausa'] != '-1' && $_POST['medioDePago'] == '1' && $realizaCausacion == 1) {
                                            //concepto contable //********************************************* */
                                            $rest = substr($rowdes['tipoconce'], 0, 2);
                                            $sq = "select fechainicial from conceptoscontables_det where codigo='" . $rowdes['conceptocausa'] . "' and modulo='" . $rowdes['modulo'] . "' and tipo='$rest' and fechainicial<'$fechaf' and cuenta!='' order by fechainicial asc";
                                            $re = mysqli_query($linkbd_V7, $sq);
                                            while ($ro = mysqli_fetch_assoc($re)) {
                                                $_POST['fechacausa'] = $ro["fechainicial"];
                                            }
                                            $sqlr = "select * from conceptoscontables_det where codigo='" . $rowdes['conceptocausa'] . "' and modulo='" . $rowdes['modulo'] . "' and cc='" . $cc . "' and tipo='$rest' and fechainicial='" . $_POST['fechacausa'] . "'";
                                            $rst = mysqli_query($linkbd_V7, $sqlr);
                                            $row1 = mysqli_fetch_assoc($rst);
                                            if ($row1['cuenta'] != '' && $valordes > 0) {
                                                $sqlr = "insert into comprobante_det (id_comp,cuenta,tercero,centrocosto,detalle, cheque,valdebito, valcredito, estado, vigencia) values ('11 $consec','" . $row1['cuenta'] . "','" . $_POST['tercero'] . "' ,'" . $cc . "' , 'Descuento " . $nomDescuento . "',''," . $valordes . ",0,'1' ,'" . $vigencia . "')";
                                                mysqli_query($linkbd_V7, $sqlr);
                                                if ($valorProcentaje <= 100) {
                                                    $sqlr = "insert into comprobante_det (id_comp,cuenta,tercero,centrocosto,detalle, cheque,valdebito, valcredito, estado, vigencia) values ('11 $consec','" . $row1['cuenta'] . "','" . $_POST['tercero'] . "' ,'" . $cc . "' , 'Descuento " . $nomDescuento . "','',0," . $valordes . ",'1' ,'" . $vigencia . "')";
                                                    mysqli_query($linkbd_V7, $sqlr);
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        $cantCuentas = count($_POST['dcuentas']);
                        $cuentaResta = 0;
                        $vigusu = $_POST['vigencia'];
                        $sqlrSelect = "DELETE FROM tesoordenpago_cuenta WHERE id_orden='$consec'";
                        mysqli_query($linkbd_V7, $sqlrSelect);
                        for ($x = 0; $x < $cantCuentas; $x++) {
                            $sqlrCuetas = "INSERT INTO tesoordenpago_cuenta (id_orden, cuenta_deb, cuenta_cred, rubro, fuente, productoservicio, indicador_producto, tipo_mov, usuario, estado, bpim, seccion_presupuestal, tipo_gasto, codigo_vigenciag, medio_pago) VALUES ($consec, '" . $_POST['cuentaDebito'][$x] . "', '" . $_POST['cuentaCredito'][$x] . "', '" . $_POST['dcuentas'][$x] . "', '" . $_POST['drecursos'][$x] . "', '" . $_POST['productoServicio'][$x] . "', '" . $_POST['indicador_producto'][$x] . "', '$_POST[tipomovimiento]', '" . $_SESSION['nickusu'] . "', 'S', '" . $_POST['bpim'][$x] . "', '" . $_POST['dseccion_presupuestal'][$x] . "', '" . $_POST['dtipo_gasto'][$x] . "', '" . $_POST['dvigenciaGasto'][$x] . "', '" . $_POST['medio_pago'][$x] . "')";
                            mysqli_query($linkbd_V7, $sqlrCuetas);
                            echo "
								<script>
									Swal.fire({
										icon: 'warning',
										title: 'Recuerde reflejar el egreso',
										confirmButtonText: 'Continuar',
										confirmButtonColor: '#FF121A',
										timer: 3000
									});
								</script>";
                        }
                        $cantCuentas = count($_POST['dcuentas']);
                        $valorDesc = 0;
                        $valorDeb = 0;

                        $sqlrIva = "SELECT valor FROM tesoordenpagoivadescontable WhERE id_orden = '" . $_POST['idcomp'] . "'";
                        $resIva = mysqli_query($linkbd_V7, $sqlrIva);
                        $rowIva = mysqli_fetch_row($resIva);
                        $cuentaIvaDesc = $rowIva[0];
                        /*$cuentaIvaDesc = '';
                                              if($_POST['ivaDescontable'] > 0){
                                                 $sqlr = "DELETE FROM tesoordenpagoivadescontable WHERE id_orden = $consec";
                                                 mysqli_query($linkbd_V7, $sqlr);

                                                 $sqlr = "INSERT INTO tesoordenpagoivadescontable (id_orden, valor) VALUES ($consec, $_POST[ivaDescontable])";
                                                 mysqli_query($linkbd_V7, $sqlr);
                                                 $sq = "select fechainicial from conceptoscontables_det where codigo='01' and modulo='4' and tipo='AN' and fechainicial<'$fechaf' and cuenta!='' order by fechainicial asc";
                                                 $re = mysqli_query($linkbd_V7, $sq);
                                                 while($ro = mysqli_fetch_assoc($re))
                                                 {
                                                     $_POST['fechacausa'] = $ro["fechainicial"];
                                                 }
                                                 $sqlr = "select * from conceptoscontables_det where codigo='01' and modulo='4' and tipo='AN' and fechainicial='".$_POST['fechacausa']."'";

                                                 $rst = mysqli_query($linkbd_V7, $sqlr);
                                                 $row1 = mysqli_fetch_assoc($rst);
                                                 $cuentaIvaDesc = $row1['cuenta'];
                                             } */
                        for ($x = 0; $x < $cantCuentas; $x++) {
                            $valorDesc = 0;
                            if ($_POST['contabiliza'][$x] == 'S') {
                                $sec_presu = intval($_POST['dseccion_presupuestal'][$x]);
                                $sqlrCc = "SELECT id_cc FROM centrocostos_seccionpresupuestal WHERE (id_sp = $sec_presu OR id_sp = '" . $_POST['dseccion_presupuestal'][$x] . "')";
                                $resCc = mysqli_query($linkbd_V7, $sqlrCc);
                                $rowCc = mysqli_fetch_row($resCc);
                                $cc = $rowCc[0];
                                if ($_POST['dvalores'][$x] > 0 && $_POST['cuentaCredito'][$x] != 'N/A') {

                                    if ($cuentaIvaDesc != '') {
                                        $valorDesc = round(($_POST['dvalores'][$x] / $_POST['valor']) * $_POST['iva'], 2);
                                    }

                                    $valorCred = $_POST['dvalores'][$x] - $valorDesc;
                                    $sqlr = "insert into comprobante_det (id_comp,cuenta,tercero,centrocosto,detalle,cheque, valdebito,valcredito,estado,vigencia) values ('11 $consec','" . $_POST['cuentaCredito'][$x] . "','" . $_POST['tercero'] . "','" . $cc . "','Causacion " . $_POST['dncuentas'][$x] . "','',0,'" . $valorCred . "','1','" . $vigusu . "')";
                                    mysqli_query($linkbd_V7, $sqlr);

                                    $valorDeb = $_POST['dvalores'][$x] - $valorDesc;
                                    $sqlr = "insert into comprobante_det (id_comp,cuenta,tercero,centrocosto,detalle,cheque, valdebito,valcredito,estado,vigencia) values ('11 $consec','" . $_POST['cuentaDebito'][$x] . "','" . $_POST['tercero'] . "','" . $cc . "','Causacion " . $_POST['dncuentas'][$x] . "',''," . $valorDeb . ",0,'1','" . $vigusu . "')";
                                    mysqli_query($linkbd_V7, $sqlr);
                                    if ($valorDesc > 0) {
                                        $sqlr = "insert into comprobante_det (id_comp,cuenta,tercero,centrocosto,detalle,cheque, valdebito,valcredito,estado,vigencia) values ('11 $consec','" . $_POST['cuentaCredito'][$x] . "','" . $_POST['tercero'] . "','" . $cc . "','Iva descontable " . $_POST['dncuentas'][$x] . "','',0,'" . $valorDesc . "','1','" . $vigusu . "')";
                                        mysqli_query($linkbd_V7, $sqlr);

                                        $sqlr = "insert into comprobante_det (id_comp,cuenta,tercero,centrocosto,detalle,cheque, valdebito,valcredito,estado,vigencia) values ('11 $consec','" . $cuentaIvaDesc . "','" . $_POST['tercero'] . "','" . $cc . "','Iva descontable " . $_POST['dncuentas'][$x] . "',''," . $valorDesc . ",0,'1','" . $vigusu . "')";
                                        mysqli_query($linkbd_V7, $sqlr);
                                    }
                                }
                            }
                        }
                        echo "
							<script>
								Swal.fire({
									icon: 'success',
									title: 'Se reflejo con exito la liquidación de cuenta por pagar N°: $consec',
									confirmButtonText: 'Continuar',
									confirmButtonColor: '#FF121A',
									timer: 3000
								});
							</script>";
                    }
                } else {
                    echo "
						<script>
							Swal.fire({
								icon: 'error',
								title: 'Error!',
								text: 'No Tiene los Permisos para Modificar este Documento',
								confirmButtonText: 'Continuar',
								confirmButtonColor: '#FF121A',
								timer: 3000
							});
						</script>";
                }
            }
            ?>
            <div id="bgventanamodal2">
                <div id="ventanamodal2">
                    <IFRAME  src="" name="buster" marginWidth=0 marginHeight=0 frameBorder=0 id="ventana2" frameSpacing=0 style="left:500px; width:900px; height:500px; top:200;">
                    </IFRAME>
                </div>
            </div>
        </form>
    </body>
</html>
