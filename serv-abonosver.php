<?php
	require "comun.inc";
	require "funciones.inc";
	require "validaciones.inc";
	require "conversor.php";
	session_start();
	$linkbd=conectar_bd();	
	header("Cache-control: private"); // Arregla IE 6
	date_default_timezone_set("America/Bogota");
?>
<!DOCTYPE >
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="es">
	<head>
	 	<meta http-equiv="Content-Type" content="text/html" charset="iso-8859-1"/>
        <meta http-equiv="X-UA-Compatible" content="IE=9"/>
		<title>:: SPID- Servicios Publicos</title>
        <link href="css/css2.css" rel="stylesheet" type="text/css" />
        <link href="css/css3.css" rel="stylesheet" type="text/css" />
        <link href="css/tabs.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="css/programas.js"></script>
		<script type="text/javascript" src="css/calendario.js"></script>
		<script>
			function validar(){document.form2.submit();}
			function pdf()
			{
				document.form2.action="serv-pdfrecaja.php";
				document.form2.target="_BLANK";
				document.form2.submit(); 
				document.form2.action="";
				document.form2.target="";
			}
			function adelante()
			{
				if(parseInt(document.form2.basep.value)>0)
 				{
					document.form2.oculto.value=1;
					document.form2.basep.value=parseInt(document.form2.basep.value)-1;
					document.form2.action="serv-abonosver.php?idr="+document.form2.nmenor.value;
					document.form2.submit();
					document.form2.action="";
					document.form2.target="";
				}
				else{alert();}
			}
			function atrasc()
			{
				if(parseInt(document.form2.basep.value) < parseInt(document.form2.maximo.value))
 				{
					document.form2.oculto.value=1;
					document.form2.basep.value=parseInt(document.form2.basep.value)+1;
					document.form2.action="serv-abonosver.php?idr="+document.form2.nmayor.value;
					document.form2.submit();
					document.form2.action="";
					document.form2.target="";
 				}
				else{alert();}
			}
			function validar2Z()
			{
				//   alert("Balance Descuadrado");
				document.form2.oculto.value=1;
				document.form2.ncomp.value=document.form2.idcomp.value;
				document.form2.action="serv-recaudo_regrabar.php";
				document.form2.submit();
			}
		</script>
		<?php titlepag();?>
	</head>
	<body>
		<IFRAME src="alertas.php" name="alertas" id="alertas" style="display:none"></IFRAME>
		<span id="todastablas2"></span>
		<table>
			<tr><script>barra_imagenes("serv");</script><?php cuadro_titulos();?></tr>	 
			<tr><?php menu_desplegable("serv");?></tr>
			<tr>
  				<td colspan="3" class="cinta"><a onClick="location.href='serv-abonos.php'" class='mgbt'><img src="imagenes/add.png" title="Nuevo"/></a><a class='mgbt'><img src="imagenes/guardad.png"/></a><a onClick="location.href='serv-buscarabonos.php'" class='mgbt'><img src="imagenes/busca.png"  Buscar="Buscar" /></a><a  class='mgbt' onClick="mypop=window.open('serv-principal.php','','');mypop.focus();"><img src="imagenes/nv.png" title="Nueva Ventana"></a><a onClick="pdf()" class='mgbt'><img src="imagenes/print.png" title="Imprimir" /></a></td>
			</tr>		  
		</table>
        <form name="form2" method="post" > 
			<?php
				$vigusu=vigencia_usuarios($_SESSION[cedulausu]);
				$vigencia=$vigusu;
				$sqlr="select valor_inicial from dominios where nombre_dominio='CUENTA_CAJA'";
				$res=mysql_query($sqlr,$linkbd);
				while ($row =mysql_fetch_row($res)){$_POST[cuentacaja]=$row[0];}
				$_POST[tipovista]=1;	
	  			//*********** 11050501	CAJA PRINCIPAL esta es la cuenta que va a credito en todas las consignacones
				//if(!$_POST[oculto])
				{   
					$sqlr="SELECT TB1.*,TB2.codusuario,concat_ws(' ',TB4.nombre1,TB4.nombre2,TB4.apellido1,TB4.apellido2,TB4.razonsocial) FROM servreciboscaja TB1,servfacturas TB2,servclientes TB3,terceros TB4 WHERE TB1.id_recaudo=TB2.id_factura AND TB1.tipo='5' AND TB2.codusuario=TB3.codigo AND TB3.terceroactual=TB4.cedulanit  ORDER BY TB1.id_recibos DESC ";
					unset($_POST[baset]);
					$resp = mysql_query($sqlr,$linkbd);
					while($row=mysql_fetch_row($resp)){$_POST[baset][]=$row;}
					$fec=date("d/m/Y");
					$_POST[vigencia]=$vigencia;
					$sqlr="select valor_inicial from dominios where nombre_dominio='CUENTA_CAJA'";
					$res=mysql_query($sqlr,$linkbd);
					while ($row =mysql_fetch_row($res)){$_POST[cuentacaja]=$row[0];}	
					$sqlr="select max(id_recibos) from servreciboscaja ";
					$res=mysql_query($sqlr,$linkbd);
					$consec=0;
					$_POST[maximo]=count($_POST[baset]);
	 				$_POST[idcomp]=$_GET[idr];
	 				$_POST[ncomp]=$_POST[idcomp]; 
					$_POST[basep]=array_search($_POST[idcomp], array_column($_POST[baset], 0));
					if ($_POST[basep]>0){$_POST[nmenor]=$_POST[baset][$_POST[basep]-1][0];}
					if ($_POST[basep]<$_POST[maximo]){$_POST[nmayor]=$_POST[baset][$_POST[basep]+1][0];}
				}
				$_POST[idcomp]=$_POST[baset][$_POST[basep]][0];
				$sqlr="select * from servreciboscaja where id_recibos='$_POST[idcomp]'";
				$res=mysql_query($sqlr,$linkbd);
				$consec=0;
				while($r=mysql_fetch_row($res))
	 			{
	  				$_POST[idrecaudo]=$r[4];	 
	  				$_POST[fecha]=$r[2];
	  				$_POST[modorec]=$r[5];
	  				$_POST[banco]=$r[7];	   
      				$_POST[estado]=$r[9];
	  				$_POST[tiporec]=$r[10];
	  				if($_POST[estado]=='S'){$_POST[estadoc]="ACTIVO";}
	 				else {$_POST[estadoc]="ANULADO";}
	 			}
			?>
            <input type="hidden" name="baset" id="baset" value="<?php echo $_POST[baset];?>"/>
            <input type="hidden" name="basep" id="basep" value="<?php echo $_POST[basep];?>"/>
            <input type="hidden" name="nmayor" id="nmayor" value="<?php echo $_POST[nmayor];?>"/>
            <input type="hidden" name="nmenor" id="nmenor" value="<?php echo $_POST[nmenor];?>"/>
            <input type="hidden" name="encontro" value="<?php echo $_POST[encontro]?>" >
            <input name="cobrorecibo" type="hidden" value="<?php echo $_POST[cobrorecibo]?>" >
            <input name="vcobrorecibo" type="hidden" value="<?php echo $_POST[vcobrorecibo]?>" >
            <input name="tcobrorecibo" type="hidden" value="<?php echo $_POST[tcobrorecibo]?>" > 
            <input name="codcatastral" type="hidden" value="<?php echo $_POST[codcatastral]?>" >
			<?php 
                if($_POST[oculto])
                {
                 	$sqlr="select *from  servfacturas where  servfacturas.id_factura=$_POST[idrecaudo]  and 5=$_POST[tiporec]";
					$_POST[encontro]="";
					$res=mysql_query($sqlr,$linkbd);
					while ($row =mysql_fetch_row($res)) 
					{
						$nliqui=$row[0];
						$tercero=$row[0];
					}
					$sqlr="select servliquidaciones_det.servicio, servliquidaciones_det.valorliquidacion, servliquidaciones_det.estrato, servliquidaciones.saldo, servliquidaciones.codusuario, servliquidaciones.tercero, servliquidaciones.id_liquidacion from servliquidaciones, servliquidaciones_det where servliquidaciones.factura=$_POST[idrecaudo] and servliquidaciones.id_liquidacion=servliquidaciones_det.id_liquidacion ";
					$res=mysql_query($sqlr,$linkbd);
					while ($row =mysql_fetch_row($res)) 
					{	
						$_POST[intereses]=$row[3];
						$_POST[codcatastral]=$row[1];		
						$_POST[concepto]=$row[17].'USUARIO '.$row[4];	
						$_POST[valorecaudo]=$row[8];		
						$_POST[totalc]=$row[8];	
						$_POST[tercero]=$row[5];	
						$_POST[liquidacion]=$row[6];
						$_POST[codigousuario]=$row[4];
						$_POST[ntercero]=buscatercero($row[5]);		
						$_POST[encontro]=1;
					}
                }
            ?>
    		<table class="inicio" align="center" >
                <tr>
                    <td class="titulos" colspan="9">Recibo de Caja</td>
                     <td class="cerrar" style="width:7%"><a onClick="location.href='serv-principal.php';" >&nbsp;Cerrar</a></td>
                </tr>
                <tr>
                    <td  class="saludo1" >N&deg; Recibo:</td>
                     <td>
                        <input type="hidden" name="codigousuario" value="<?php echo $_POST[codigousuario]?>"/>
                        <input type="hidden" name="liquidacion" value="<?php echo $_POST[liquidacion]?>"/>
                        <input type="hidden" name="intereses" value="<?php echo $_POST[intereses]?>"/>
                        <input type="hidden" name="tipovista" value="<?php echo $_POST[tipovista]?>"/>
                        <input type="hidden" name="cuentacaja" value="<?php echo $_POST[cuentacaja]?>"/>
                        <input type="hidden" name="ncomp" value="<?php echo $_POST[ncomp]?>"/>
                        <input type="hidden" name="siguiente" value="s"/>
                        <input type="hidden" name="maximo" value="<?php echo $_POST[maximo]?>"/>
                        <input type="hidden" name="atras" value="a"/>
                        <a href="#" onClick="atrasc()"><img src="imagenes/back.png" title="anterior"></a>&nbsp;<input type="text" name="idcomp" size="5" value="<?php echo $_POST[idcomp]?>" onKeyUp="return tabular(event,this) "  onBlur="validar2()"/>&nbsp;<a href="#" onClick="adelante()"><img src="imagenes/next.png" alt="siguiente" align="absmiddle"></a>
                    </td>
                    <td  class="saludo1">Fecha:</td>
                    <td><input name="fecha" type="date" value="<?php echo $_POST[fecha]?>" maxlength="10" size="10" onKeyPress="javascript:return solonumeros(event)" onKeyUp="return tabular(event,this)" readonly/>  </td>
                    <td class="saludo1">Vigencia:</td>
                    <td>
                        <input type="text" id="vigencia" name="vigencia" size="8" onKeyPress="javascript:return solonumeros(event)" 
              onKeyUp="return tabular(event,this)"  value="<?php echo $_POST[vigencia]?>" onClick="document.getElementById('tipocta').focus();document.getElementById('tipocta').select();" readonly/>  
                        <input type="hidden" name="estado" value="<?php echo $_POST[estado]?>"/>    
                        <input type="text" name="estadoc" value="<?php echo $_POST[estadoc]?>" readonly/>    
                    </td>		  
                </tr>
                <tr>
                    <td class="saludo1"> Recaudo:</td>
                    <td> 
                        <select name="tiporec" id="tiporec" onKeyUp="return tabular(event,this)" onChange="validar()"/>
                            <?php 
                                if($_POST[tiporec]=='4'){echo"<option value='4' SELECTED>Factura Servicios</option>";}
                                else {echo"<option value='5' SELECTED>Abono Factura Servicios</option>";}
                            ?>
                        </select>
                    </td>
                    <td class="saludo1">N&deg; Factura:</td>
                    <td><input type="text" id="idrecaudo" name="idrecaudo" value="<?php echo $_POST[idrecaudo]?>" onKeyUp="return tabular(event,this)" onBlur="validar()" readonly/></td>
                    <td class="saludo1">Recaudado en:</td>
                    <td> 
                        <select name="modorec" id="modorec" onKeyUp="return tabular(event,this)" onChange="validar()" >
                            <?php
                                if($_POST[modorec]=='caja'){echo"<option value='caja' SELECTED>Caja</option>";}
                                else{"<option value='banco' SELECTED>Banco</option>";}
                            ?>
                        </select>
                        <?php
                            if ($_POST[modorec]=='banco')
                            {
                                echo"
                                    <select id='banco' name='banco' onChange='validar()' onKeyUp='return tabular(event,this)'>
                                        <option value=''>Seleccione....</option>";
                                $sqlr="select tesobancosctas.estado,tesobancosctas.cuenta,tesobancosctas.ncuentaban,tesobancosctas.tipo, terceros.razonsocial,tesobancosctas.tercero from tesobancosctas,terceros where tesobancosctas.tercero=terceros.cedulanit and tesobancosctas.estado='S' ";
                                $res=mysql_query($sqlr,$linkbd);
                                while ($row =mysql_fetch_row($res)) 
                                {
                                    if($row[1]==$_POST[banco])
                                    {
                                        echo "<option value='$row[1]' SELECTED>$row[2] - Cuenta $row[3]</option>";
                                        $_POST[nbanco]=$row[4];
                                        $_POST[ter]=$row[5];
                                        $_POST[cb]=$row[2];
                                    }
                                }	 	
                                echo" 
                                    </select>
                                    <input name='cb' type='hidden' value='$_POST[cb]'/>
                                    <input type='hidden' id='ter' name='ter' value='$_POST[ter]'/>           
                                </td>
                                <td><input type='text' id='nbanco' name='nbanco' value='$_POST[nbanco]' size='40' readonly>";
                            }
                        ?>
                    </td>
                </tr>
                <tr>
                    <td class="saludo1" width="71">Concepto:</td>
                    <td colspan="3"><input type="text" name="concepto"   value="<?php echo $_POST[concepto] ?>" onKeyUp="return tabular(event,this)" style="width:100%;" readonly></td>
                    <?php
                        if($_POST[tiporec]==2)
                        {
                            echo"  
                            <td class='saludo1'>No Cuota:</td>
                            <td><input name='cuotas' size='1' type='text' value='$_POST[cuotas]' readonly>/<input type='text' id='tcuotas' name='tcuotas' value='$_POST[tcuotas]' size='1' readonly ></td>";
                        }
                     ?>
                </tr>
				<tr>
                    <td class="saludo1" width="71">Valor:</td>
                    <td><input type="text" id="valorecaudo" name="valorecaudo" value="<?php echo $_POST[valorecaudo]?>" size="30" onKeyUp="return tabular(event,this)" readonly ></td>
                    <td  class="saludo1">Documento: </td>
                    <td ><input name="tercero" type="text" value="<?php echo $_POST[tercero]?>" size="20" onKeyUp="return tabular(event,this)" readonly></td>
                    <td class="saludo1">Contribuyente:</td>
                    <td><input type="text" id="ntercero" name="ntercero" value="<?php echo $_POST[ntercero]?>" size="50" onKeyUp="return tabular(event,this) "  readonly><input type="hidden" id="cb" name="cb" value="<?php echo $_POST[cb]?>" ><input type="hidden" id="ct" name="ct" value="<?php echo $_POST[ct]?>" >
                    </td>
                	<td></td>
       			</tr>
                <input type="hidden" value="1" name="oculto">
                <input type="hidden" value="<?php echo $_POST[trec]?>"  name="trec">
                <input type="hidden" value="0" name="agregadet">
      		</table>
     		<div class="subpantallac7">
      			<?php 
  					switch($_POST[tiporec]) 
					{
						//*****************otros recaudos *******************
	   				 	case 4:	$_POST[trec]='Factura Servicios';	 
								// echo $_POST[tcobrorecibo];
								$_POST[dcoding]= array(); 		 
								$_POST[dncoding]= array(); 		 
								$_POST[dvalores]= array(); 
								$_POST[destratos]= array(); 
								$_POST[dcargosfijos]= array(); 		 
								$_POST[dtarifas]= array(); 		 
								$_POST[dsubsidios]= array(); 
								$_POST[ddescuentos]= array(); 
								$_POST[dcontribuciones]= array(); 		 
								$_POST[dsaldos]= array(); 		 
								$_POST[dinteres]= array(); 
								$_POST[dtotales]= array(); 		 
		 						$sqlr="select * from servreciboscaja_det where id_recibos=$_POST[idcomp] order by ingreso";
		  						$res=mysql_query($sqlr,$linkbd);
		  						while ($row =mysql_fetch_row($res)) 
								{
									$_POST[dcoding][]=$row[2];	
									$_POST[dncoding][]=buscar_servicio($row[2]);			 		
									$_POST[dcargofijos][]=$row[3];	
									$_POST[dtarifas][]=$row[4];	
									$_POST[dsubsidios][]=$row[5];	
									$_POST[ddescuentos][]=$row[6];							
									$_POST[dcontribuciones][]=$row[7];	
									$_POST[dsaldos][]=$row[8];	
									$_POST[dvalores][]=$row[10];	
									$_POST[dtotales][]=$row[11];	
									$_POST[dinteres][]=$row[9];	
									//$tam=count($_POST[dcoding]);
									//$valorint=round($_POST[intereses]/$tam,2);					
									//$_POST[dinteres][]=$valorint;	
									//echo 	$valorint; 	
								}
								break;
						///*****************otros recaudos *******************
						case 5:	$_POST[trec]='Abono Factura Servicios';	 
								// echo $_POST[tcobrorecibo];
								$_POST[dcoding]= array(); 		 
								$_POST[dncoding]= array(); 		 
								$_POST[dvalores]= array(); 
								$_POST[destratos]= array(); 
								$_POST[dcargosfijos]= array(); 		 
								$_POST[dtarifas]= array(); 		 
								$_POST[dsubsidios]= array(); 
								$_POST[ddescuentos]= array(); 
								$_POST[dcontribuciones]= array(); 		 
								$_POST[dsaldos]= array(); 		 
								$_POST[dinteres]= array(); 
								$_POST[dtotales]= array(); 		 
		 						$sqlr="select * from servreciboscaja_det where id_recibos=$_POST[idcomp] order by ingreso";
		  						$res=mysql_query($sqlr,$linkbd);
		  						while ($row =mysql_fetch_row($res)) 
								{
									$_POST[dcoding][]=$row[2];	
									$_POST[dncoding][]=buscar_servicio($row[2]);			 		
									$_POST[dcargofijos][]=$row[3];	
									$_POST[dtarifas][]=$row[4];	
									$_POST[dsubsidios][]=$row[5];	
									$_POST[ddescuentos][]=$row[6];							
									$_POST[dcontribuciones][]=$row[7];	
									$_POST[dsaldos][]=$row[8];	
									$_POST[dvalores][]=$row[10];	
									$_POST[dtotales][]=$row[11];	
									$_POST[dinteres][]=$row[9];	
									//$tam=count($_POST[dcoding]);
									//$valorint=round($_POST[intereses]/$tam,2);					
									//$_POST[dinteres][]=$valorint;	
									//echo 	$valorint; 	
								}
								break;	
					}
 				?>
	   			<table class="inicio">
	   	   			<tr><td colspan="11" class="titulos">Detalle Factura</td></tr>                  
					<tr>
                    	<td class="titulos2">Codigo</td>
                        <td class="titulos2">Servicio</td>
                        <td class="titulos2">Cargo Fijo</td>
                        <td class="titulos2">Tarifa</td>
                        <td class="titulos2">Subsidio</td>
                        <td class="titulos2">Descuento</td>
                        <td class="titulos2">Contribucion</td>
                        <td class="titulos2">Saldo Anterior</td>
                        <td class="titulos2">Valor</td>
                        <td class="titulos2">Intereses</td>
                        <td class="titulos2">Total</td>
                	</tr>
		  			<?php
		 				$_POST[totalc]=0;
						$iter1="zebra1";
						$iter2="zebra2";
						$tam=count($_POST[dcoding]);
						$valorint=round($_POST[intereses]/$tam,2);
		 				for ($x=0;$x<count($_POST[dcoding]);$x++)
		 				{		 
		 					//$_POST[dinteres][]=$valorint;	
							// 	$total=$valorint+$_POST[dvalores][$x];
		 					echo "
							<tr class='$iter1'>
								<td ><input name='dcoding[]' value='".$_POST[dcoding][$x]."' type='text' size='2' readonly></td>
								<td><input name='dncoding[]' value='".$_POST[dncoding][$x]."' type='text' size='40' readonly></td>
								<td><input name='dcargofijos[]' value='".$_POST[dcargofijos][$x]."' type='text' size='12' readonly></td>
								<td><input name='dtarifas[]' value='".$_POST[dtarifas][$x]."' type='text' size='12' readonly></td>
								<td><input name='dsubsidios[]' value='".$_POST[dsubsidios][$x]."' type='text' size='15' readonly></td>
								<td><input name='ddescuentos[]' value='".$_POST[ddescuentos][$x]."' type='text' size='12' readonly></td>
								<td><input name='dcontribuciones[]' value='".$_POST[dcontribuciones][$x]."' type='text' size='12' readonly></td>
								<td><input name='dsaldos[]' value='".$_POST[dsaldos][$x]."' type='text' size='12' readonly></td>
								<td><input name='dvalores[]' value='".$_POST[dvalores][$x]."' type='text' size='12' readonly></td>
								<td><input name='dinteres[]' value='".$_POST[dinteres][$x]."' type='text' size='12' readonly></td>
								<td><input name='dtotales[]' value='".$_POST[dtotales][$x]."' type='text' size='12' readonly></td>
							</tr>";
							$_POST[totalc]=$_POST[totalc]+$_POST[dtotales][$x];
							$_POST[totalcf]=number_format($_POST[totalc],0);
							$aux=$iter1;
							$iter1=$iter2;
							$iter2=$aux;
		 				}
 						$resultado = convertir($_POST[totalc],"PESOS");
						$_POST[letras]=$resultado." PESOS M/CTE";
		 				echo "<tr class='$iter1'><td colspan='9'></td><td >Total</td><td ><input name='totalcf' type='text' value='$_POST[totalcf]' size='12' readonly><input name='totalc' type='hidden' value='$_POST[totalc]'></td></tr><tr><td class='saludo1'>Son:</td><td colspan='10' ><input name='letras' type='text' value='$_POST[letras]' size='200'></td></tr>";
					?> 
	   			</table>
         	</div>
		</form>
	</body>
</html>