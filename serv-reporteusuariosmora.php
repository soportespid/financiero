<?php
	header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=iso-8859-1");
	require 'comun.inc';
	require 'funciones.inc';
	session_start();
	$linkbd=conectar_v7();
	cargarcodigopag($_GET[codpag],$_SESSION["nivel"]);
	date_default_timezone_set("America/Bogota");
	//error_reporting(E_ALL);
	//ini_set('display_errors', '1');
?>
<!DOCTYPE >
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="es">
	<head>
		<meta http-equiv="Content-type" content="text/html; charset=iso-8859-1"/>
		<meta http-equiv="X-UA-Compatible" content="IE=9"/>
		<title>:: Spid - Servicios P&uacute;blicos</title>
		<link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<link href="css/css2.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<link href="css/css4.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<script type="text/javascript" src="css/programas.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script type="text/javascript" src="css/calendario.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script>
			function fexportar ()
			{
				document.form2.action="serv-reporteusuariosmoraexcel.php";
				document.form2.target="_BLANK";
				document.form2.submit(); 
				document.form2.action="";
				document.form2.target="";
			}
			function actualizar()
			{
				document.form2.oculto.value='2';
				document.form2.submit();
			}
		</script>
		<?php titlepag();?>
	</head>
	<body>
		<IFRAME src="alertas.php" name="alertas" id="alertas" style="display:none"></IFRAME>
		<span id="todastablas2"></span>
		<table>
			<tr><script>barra_imagenes("serv");</script><?php cuadro_titulos();?></tr>	 
			<tr><?php menu_desplegable("serv");?></tr>
			<tr>
				<td colspan="3" class="cinta"><img src="imagenes/add.png" title="Nuevo" onClick="location.href=' serv-reportegeneralusuarios.php'" class="mgbt"/><img src="imagenes/guardad.png" class="mgbt1"/><img src="imagenes/busca.png" class="mgbt" onClick="document.form2.submit();"/><img src="imagenes/nv.png" title="Nueva Ventana" onClick="mypop=window.open('serv-principal.php','','');mypop.focus();" class="mgbt"/><img src="imagenes/excel.png" title="Exportar" onClick="fexportar()" class="mgbt"/><img src='imagenes/iratras.png' title="Men&uacute; Nomina" onClick="location.href=' serv-menureportes.php'" class='mgbt'></td>
			</tr>
		</table>
		<form name="form2" method="post" action="">
			<div class="loading" id="divcarga"><span>Cargando...</span></div>
			<?php if(@ $_POST['oculto']=="11"){echo"<script>document.getElementById('divcarga').style.display='none';</script>";}?>
			
			<input type="hidden" name="numinc" id="numinc" value="<?php echo @ $_POST['numinc'];?>"/>
			<div class="subpantalla" style="height:68.5%; width:99.6%; overflow-x:hidden;" id="divdet">
				<table class="inicio" align="center">
					<tr>
						<td class="titulos" colspan="9">Reporte General de Usuarios Morosos</td>
						<td class="cerrar" style="width:10%" onClick="actualizar();">&nbsp;Actualizar</td>
						<td class="cerrar" style='width:7%'><a onClick="location.href='serv-principal.php'">&nbsp;Cerrar</a></td>
					</tr>
					<tr >
						<td width="70" class="saludo1">Servicio:</td>
						<td><select name="select_servicio" id"select_servicio"> 
							<option value="" <?php if($_POST['select_servicio']==''){echo 'selected';}?>>::: Seleccione Servicio ::: </option>
							<option value="01" <?php if($_POST['select_servicio']=='01'){echo 'selected';}?>>SP - SERVICIO DE ACUEDUCTO </option>
							<option value="02" <?php if($_POST['select_servicio']=='02'){echo 'selected';}?>>SP - SERVICIO DE ALCANTARILLADO </option>
							<option value="03" <?php if($_POST['select_servicio']=='03'){echo 'selected';}?>>SP - SERVICIO DE ASEO </option>
							</select>
						</td>
						<td><input type="button" name="buscar" id="buscar" value="  Buscar  " onClick="document.form2.submit();"></td>
					</tr>
				</table>
				<table class="inicio" >
					<?php
					{
						if(@ $_POST['oculto']=='2')
						{
							ini_set('max_execution_time', 7200);
							$sqlr="TRUNCATE TABLE servtempmorosos";
							$res=mysqli_query($linkbd,$sqlr);
							$sqlr="ALTER TABLE servtempmorosos AUTO_INCREMENT = 1";
							$res=mysqli_query($linkbd,$sqlr);
							$sqlr="SELECT codigo,codcatastral,terceroactual,nombretercero,direccion,barrio,estrato,zona,lado FROM servclientes ORDER BY codigo ASC";
							$res=mysqli_query($linkbd,$sqlr);
							while ($row =mysqli_fetch_row($res))
							{
								flush();
								usleep(1);
								$sqlsv="SELECT codigo,nombre FROM servservicios ORDER BY codigo ASC";
								$ressv=mysqli_query($linkbd,$sqlsv);
								while ($rowsv =mysqli_fetch_row($ressv))
								{
									$auxliqui=$totalfac=$auxservi='';
									$auxsalli=$cont=$ultval=0;
									$sqlli="SELECT id_liquidacion,saldo,servicio FROM servliquidaciones_det WHERE codusuario='$row[0]' AND servicio='$rowsv[0]' ORDER BY servicio ASC,id_liquidacion DESC";
									$resli=mysqli_query($linkbd,$sqlli);
									while ($rowli =mysqli_fetch_row($resli))
									{
										if( $auxliqui=='')
										{
											$auxliqui=$rowli[0];
											$ultval=$auxsalli=$rowli[1];
										}
										else if($auxliqui==$rowli[0])
										{
											$auxsalli=$auxsalli+$rowli[1];
										}
										else
										{
											$cont++;
											if($totalfac==''){$totalfac=$auxliqui;}
											else{$totalfac="$totalfac - $auxliqui";}
											
											if($auxsalli==0){break;}
											else
											{
												$auxliqui=$rowli[0];
												$auxsalli=$rowli[1];
											}
										}
									}
									$sqlgt="INSERT INTO servtempmorosos (codcliente,codcatastral,doccliente,nomcliente,direccion,barrio, estrato,zona,lado,valor,totalfacturas,numfacturas,servicio) VALUES ('$row[0]','$row[1]','$row[2]','$row[3]', '$row[4]','$row[5]','$row[6]','$row[7]','$row[8]','$ultval','$cont','$totalfac','$rowsv[0]')";
									$resgt=mysqli_query($linkbd,$sqlgt);
								}
							}
						}
						$iter='saludo1a';
						$iter2='saludo2';
						if($_POST['select_servicio']!=''){$condi="AND servicio='".$_POST['select_servicio']."'";}
						else {$condi='';}
						$sqlr="SELECT *FROM servtempmorosos WHERE  totalfacturas > 1 $condi ORDER BY totalfacturas DESC";
						$res=mysqli_query($linkbd,$sqlr);
						$totalcli=mysqli_affected_rows ($linkbd);
						echo "
							<tr><td colspan='11'>Clientes Morosos Encontrados: $totalcli</td></tr>
							<tr class='titulos2'>
								<td style='width:7%'>C&oacute;digo</td>
								<td style='width:7%'>Documento</td>
								<td style='width:25%'>Tercero</td>
								<td >Direcci&oacute;n</td>
								<td style='width:12%'>Barrio</td>
								<td style='width:8%'>Estrato</td>
								<td style='width:8%'>Total Facturas</td>
								<td style='width:8%'>Valor Mora</td>
							</tr>";
						while ($row =mysqli_fetch_row($res))
						{
							$barrio=$estrato="";
							$sqlr01="SELECT nombre FROM servbarrios WHERE id='$row[6]'";
							$res01=mysqli_query($linkbd,$sqlr01);
							$row01 =mysqli_fetch_row($res01);
							$barrio=$row01[0];
 							$sqlr01="SELECT tipo, descripcion FROM servestratos WHERE id='$row[7]'";
							$res01=mysqli_query($linkbd,$sqlr01);
							$row01 =mysqli_fetch_row($res01);
							$estrato="$row01[0] - $row01[1]";
  							echo "
							<tr class='$iter'>
								<td class='icoop'>$row[1]</td>
								<td style='text-align:right;'class='icoop'>".number_format($row[3],0,',','.')."&nbsp;</td>
								<td class='icoop' style='padding-left:8;'>$row[4]</td>
								<td class='icoop'>$row[5]</td>
								<td class='icoop'>$barrio</td>
								<td class='icoop'>$estrato</td>
								<td style='text-align:right;' class='icoop'>$row[11]</td>
								<td style='text-align:right;' class='icoop'>$".number_format($row[10],0,',','.')."</td>
								";
							$aux=$iter;
							$iter=$iter2;
							$iter2=$aux;
						
						}
					}
					
					echo"<script>document.getElementById('divcarga').style.display='none';</script>";
					?>
					<input type="hidden" name="oculto" id="oculto" value="1"/>
				</table>
			</div>
		</form>
	</body>
</html>