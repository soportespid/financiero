<!-- Javascript -->
<script>
    var ejemplovariable = "";
    var numerica = 0;

    //Elimina espacios en blanco en ambos extremos del string.
    ejemplovariable.trim(); 

    //en un input al presionar enter ejecuta un boton con un id llamadod filtro, se usa en los filtros de buscar
    onkeydown = "if (event.keyCode == 13){document.getElementById('filtro').click()}";

    //Ejemplo de redondear variable numerica en pesos COP ejemplo: 75 --> 100
    $numerica = ($numerica / 100); 
    $numerica = ceil($numerica);
    $numerica = $numerica * 100;	

    //colocar ceros a la izquierda
    $number = 98765;
    $length = 10;
    $string = substr(str_repeat(0, $length).$number, - $length);
</script>

<script>
    //funcion asincrona
    async function traerPais()
    {
        const respuesta = await fetch("https://api.natioanalize.io/?name=sebastian");

        //validar errores
        if (!respuesta.OK) 
        {
            let oops = "404 No encontre nada";
            alert(oops);
            throw new Error(oops);
        }
    }
</script>
    

    //sweetalert
    <script type="text/javascript" src="css/sweetalert.js"></script>
    <script type="text/javascript" src="css/sweetalert.min.js"></script>
    <link href="css/sweetalert.css" rel="stylesheet" type="text/css" />

<script>
    //Mensaje de alerta dato erroneo o no seleccionado
    swal("Error", "No has seleccionado centro de costos!", "error");

    //Mensaje de confirmacion de borrar
    swal({
        title: "Esta seguro de eliminar?",
        icon: "warning",
        buttons: ["No", "Ok"]
        
        
    }).then((response) => {
        if (response == true) {
            console.log("hola");
        }
    });
</script>
    


<!-- PHP -->
<?php
    //Los datos se guardan y se muestran con formato uft8
    header("Content-Type: text/html;charset=utf8");
    $linkbd -> set_charset("utf8"); 
?>