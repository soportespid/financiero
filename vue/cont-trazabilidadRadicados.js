var app = new Vue({
    el: '#myapp',
	data:{

        error: '',
        loading: false,
        destinosCompra: [],
        ordenPagos: [],
        entradas: [],
        almacen: [],
        datosDestinoCompra: [],
    },

    mounted: function(){

    },

    methods: 
    {
        buscarInformacion: async function() {

            var fechaIni = document.getElementById('fechaIni').value;
            var fechaFin = document.getElementById('fechaFin').value;

            if (fechaIni != '' && fechaFin != '') {

                await axios.post('vue/cont-trazabilidadRadicados.php?action=informeTrazabilidad&fechaIni='+fechaIni+'&fechaFin='+fechaFin)
                .then((response) => {
                    console.log(response.data);
                    app.destinosCompra = response.data.destinoCompra;
                    app.ordenPagos = response.data.ordenPagos;
                    app.entradas = response.data.entradas;
                }).catch((error) => {
                    this.error = true;
                    console.log(error)
                }).finally(() => {
                    
                });    
            }
            else {

                Swal.fire("Error", "Ingrese las fechas", "error");
            }
        },

        buscarEntradas: async function() {

            var fechaIni = document.getElementById('fechaIni').value;
            var fechaFin = document.getElementById('fechaFin').value;

            if (fechaIni != '' && fechaFin != '') {

                await axios.post('vue/cont-trazabilidadRadicados.php?action=buscarEntradas&fechaIni='+fechaIni+'&fechaFin='+fechaFin)
                .then((response) => {
                    console.log(response.data);
                    app.almacen = response.data.almacen;
                    app.datosDestinoCompra = response.data.datosDestinoCompra;
                }).catch((error) => {
                    this.error = true;
                    console.log(error)
                }).finally(() => {
                    
                });    
            }
            else {

                Swal.fire("Error", "Ingrese las fechas", "error");
            }
        }
    }
});