var app = new Vue({
    el: '#myapp',
    data:{
        acuerdos: [],
        ingresos: [],
        mostrarIngresos: [],
        acuerdo: '',
        colortitulosmensaje: '',
        titulomensaje: '',
        contenidomensaje: '',
        colortitulosmensaje:'',
        opcionmensaje: '',
        showMensaje: false,
        showModal_Solo_Presupuesto: false,
        valor: '',
        valores: [],
        valorSolo: '',
        guardarValorSolo: '',
        validacionValores: [],
        posicion: '',
        concepto: '',
        vigencia: '',
        years: [],
        numero: '',
        cabecera: [],
        valorTotal: 0,
    },
    mounted:
        async function(){
            await this.cargarFecha();
            this.cargaAcuerdos();
            this.cargayears();
            this.consecutivo();
    },
    methods:{

        cargarFecha: function(){
            
            const fechaAct = new Date().toJSON().slice(0,10).replace(/-/g,'/');
            const fechaArr = fechaAct.split('/');
            const fechaV = fechaArr[2]+'/'+fechaArr[1]+'/'+fechaArr[0];
            document.getElementById('fecha').value = fechaV;
            this.vigencia = fechaArr[0];
        },

        cargaAcuerdos: async function()
		{
            const fechat = document.getElementById('fecha').value;
            const fechatAr = fechat.split('/');
            this.vigencia = fechatAr[2];
			await axios.post('vue/presupuesto_ccp/teso-reservas-ccpet.php?action=show&vig=' + this.vigencia)
			.then(
				(response)=>
				{
					app.acuerdos = response.data.acuerdos;
				}
			);
		},

        consecutivo: async function()
		{
			await axios.post('vue/presupuesto_ccp/teso-reservas-ccpet.php?action=show')
			.then(
				(response)=>
				{
                    console.log(response.data);
					app.numero = response.data.consecutivo;
				}
			);
		},

        cargayears: async function()
		{
			await axios.post('vue/presupuesto_ccp/ccp-bancoproyectos.php?buscar=years')
			.then(
				(response)=>
				{
					app.years=response.data.anio;
				}
			);
		},

        muestraIngresos: async function()
        {
            if(this.vigencia != '')
            {
                if(this.acuerdo != '')
                {
                    await axios.post('vue/presupuesto_ccp/teso-reservas-ccpet.php?buscar=buscarAcuerdos&acuerdo='+this.acuerdo+'&vigencia='+this.vigencia)
                    .then(
                        (response)=>
                        {
                            app.ingresos = response.data.codigos;
                            
                        }
                    );
                    this.mostrarIngresos = [];

                    for(let i = 0; i < this.ingresos.length; i++)
                    {
                        var rubro = this.ingresos[i][0];
                        var fuente = this.ingresos[i][1];
                        var vigenciaGasto = this.ingresos[i][2];
                        var medioPago = this.ingresos[i][3];
                        var valorIngreso = this.ingresos[i][4];


                        var mostrar = [rubro,fuente,vigenciaGasto,medioPago,valorIngreso];
                        this.mostrarIngresos.push(mostrar);
                        this.validacionValores.push(valorIngreso);
                    } 
                }
                else
                {
                    alert('No ha seleccionado el acuerdo');
                }
            }
            else
            {
                alert('No ha seleccionado vigencia');
            }
        },

        toggleModal: function(posicion)
        {   
            this.posicion = '',
            this.posicion = posicion;
            this.showModal_Solo_Presupuesto = !this.showModal_Solo_Presupuesto;
        },
        
        cambiarValor: function()
        {
            valorOriginal = parseInt(this.validacionValores[this.posicion]);
            valorNuevo = parseInt(this.valorSolo);

            if(valorNuevo >= 0)
            {
                if(valorOriginal >= valorNuevo)
                {
                    this.mostrarIngresos[this.posicion][4] = this.valorSolo;
                    this.valorSolo = '';
                    this.showModal_Solo_Presupuesto = !this.showModal_Solo_Presupuesto;
                }
                else
                {
                    alert("Valor digitado mayor al original, error digitacion");
                    this.valorSolo = '';
                }
            }
            else
            {
                alert('Valor digitado menor a 0, error digitacion');
                this.valorSolo = '';
            }
        },

        guardar: function()
        {
            if(this.concepto != '' && fecha != '' && this.vigencia != '' && this.acuerdo != '')
            {
                Swal.fire({
                    title: 'Esta seguro de guardar?',
                    text: "Guardar la adici&oacute;n en la base de datos, confirmar campos!",
                    icon: 'question',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Si, guardar!'
                }).then((result) => {
                    if (result.isConfirmed) {

                        var fecha = document.getElementById('fecha').value;

                        for (let i = 0; i < this.mostrarIngresos.length; i++) 
                        {
                            var valorUnitario = parseInt(this.mostrarIngresos[i][4]);
                            this.valorTotal = this.valorTotal + valorUnitario;
                        }

                        var informacionCabecera = [this.numero,this.acuerdo,fecha,this.vigencia,this.concepto,this.valorTotal];

                        this.cabecera.push(informacionCabecera);

                        
                        var formData = new FormData();
                        for(var x = 0; x < this.cabecera.length; x++){
                            formData.append("cabecera[]", this.cabecera[x]);
                        }
                        
                        for(var x = 0; x < this.mostrarIngresos.length; x++){
                            formData.append("detalles[]", this.mostrarIngresos[x]);
                        }

                        axios.post('vue/presupuesto_ccp/teso-reservas-ccpet.php?action=guardar',
                            formData
                            )
                            .then(function(response){
                                Swal.fire({
                                    position: 'top-end',
                                    icon: 'success',
                                    title: 'El cdp se guard&oacute; con Exito',
                                    showConfirmButton: false,
                                    timer: 1500
                                }).then((response) => {
                                    app.redireccionar();
                                });
                        });   
                    
                    }
                });
            }else{
                Swal.fire(
                    'Falta informaci&oacute;n para guardar el cdp.',
                    'Verifique que todos los campos esten diligenciados.',
                    'warning'
                );
            }
        },
        
        redireccionar: function(){

            location.href ="teso-reservas-ccpet.php";
        },
        
    },

    

    
});
