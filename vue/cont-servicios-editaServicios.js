var app = new Vue({
    el: '#myapp',
    data:{
        idcodigo: '',
        codigoMostrar: '',
        numero:0,
        nombre: '',
        tipo: 'Gasto',
        tipogf: 'S1',
        correlacion: '',
        conceptos: [],
        conceptoContableHermano: '',
        fecha: '',
        cuenta: '',
        cuentaDebito: '',
        debcred: '',
        nombreCuenta: '',
        nombreCuentaDebito: '',
        cuentasContablesDebito: [],
        showModal_cuentas: false,
        showModal_cuentas_debito: false,
        searchCuenta : {keywordCuenta: '', keywordCuenta2: ''},
        cuentasContables: [],
        debcredOpciones: ['DEBITO', 'CREDITO'],
        centroCostos: [],
        cc2: '',
        destinoCompras: [],
        dc: '',
        detalles: [],
        detalles2: [],
        detallesFuncionamiento: [],
        detallesFuncionamiento2: [],
        nombreMostrar: '',
        fechaMostrar: '',
        tipoPago1: '',
        tipoPago1: '',
        mostrarTipoPago: '',
        tipoCentroCosto: '',
        mostrarCentroCosto: '',
        detalleEliminar: [],
        detalleEliminar2: [],
        numeroMax: 0,
        paginaActual: 0,
        page: 0,
        tieneHijos: false,
        changedCorrelacion: false,
        isActiveCred: true,
        isActiveDeb: false,
        cuentasCredito: [],
        cuentaAsociada: '',
        debcred2: '',
        nombreInicial: '',
        nombreCuentaCreditoDetalles2: '',
        cuenta2: '',
        nombreCuenta2: '',


    },

    mounted: function(){
        this.inicializaCodigo();
        this.precargacuentas();
    },

    methods: {

        inicializaCodigo: async function(){
            
            this.idcodigo = this.zfill(this.idcodigo, 2);
            await axios.post('vue/cont-servicios-editaServicios.php?tipogf=' + this.tipogf + '&codigo=' + this.idcodigo)
            .then((response) => {


                app.nombreMostrar           = response.data.nombreMostrar;
                app.nombreInicial           = response.data.nombreMostrar;
                app.numero                  = response.data.codigo;
                app.conceptos               = response.data.conceptos;
                app.centroCostos            = response.data.centroCostos;
                app.fechaMostrar            = response.data.fecha;
                app.tipoPago1               = response.data.tipoPago1;
                app.tipoPago2               = response.data.tipoPago2;
                app.tipoCentroCosto         = response.data.centroCosto;
                app.tieneHijos              = response.data.tieneHijos;
                app.detallesFuncionamiento  = response.data.datosCredito;
                app.detallesFuncionamiento2 = response.data.datosDebito;
                app.cuentasCredito          = response.data.cuentaAsociada;
                app.destinoCompras          = response.data.destinoCompras;
                
                app.paginaActual = this.idcodigo;
                app.numeroMax = response.data.numeroMax.length;
                this.codigoMostrar = this.zfill(this.idcodigo, 2);

                //Sacar el tipo de centro de costo
                if(this.tipoCentroCosto == '01')
                {
                    this.mostrarCentroCosto = "01 - ACUEDUCTO OPERACIONALES";
                }
                else if(this.tipoCentroCosto == '02')
                {
                    this.mostrarCentroCosto = "02 - ASEO OPERACIONAL";
                }
                else if(this.tipoCentroCosto == '03')
                {
                    this.mostrarCentroCosto = "03 - ADMINISTRACIÓN";
                }  
            });
        },
        precargacuentas: function(){
            
            
            axios.post('vue/cont-servicios-editaServicios.php?action=buscarCuentas')
            .then((response) => {
                
                app.cuentasContables = response.data.cuentas;
                
            });
        },

        precargacuentasdebito: function(){

            var fecha = document.getElementById('fecha2').value;

            axios.post('vue/cont-servicios-editaServicios.php?action=buscarCuentasDebito&destinoCompra='+this.dc+'&fecha='+fecha)
            .then((response) => {
                app.cuentasContablesDebito = response.data.cuentas;
            });
        },

        ejecutaBusquedaCuentasDebito:function()
        {
            var fecha = document.getElementById('fecha2').value;

            if(fecha != '')
            {
                this.precargacuentasdebito();   
            }
            else
            {
                alert('Primero seleccione la fecha.');
                this.dc = '';
            }
        },

        // Funcion para colocar ceros delante de un numero con un ancho que uno desee
        zfill: function(number, width) {
            var numberOutput = Math.abs(number); /* Valor absoluto del número */
            var length = number.toString().length; /* Largo del número */ 
            var zero = "0"; /* String de cero */  
            
            if (width <= length) {
                if (number < 0) {
                     return ("-" + numberOutput.toString()); 
                } else {
                     return numberOutput.toString(); 
                }
            } else {
                if (number < 0) {
                    return ("-" + (zero.repeat(width - length)) + numberOutput.toString()); 
                } else {
                    return ((zero.repeat(width - length)) + numberOutput.toString()); 
                }
            }
        
        }, 

        despliegamodal2: function()
        {
            app.showModal_cuentas = true

            axios.post('vue/cont-servicios-editaServicios.php?action=buscarCuentas')
            .then((response) => {
                
                
            });
        },

        despliegamodalDebito: function()
        {
            app.showModal_cuentas_debito = true;
        },

        buscarCta: function(){

            if(this.cuenta != '' && this.showModal_cuentas == false){

                if(this.cuenta.length == 9){

                    axios.post('vue/cont-servicios-editaServicios.php?action=buscaCta&cuenta=' + this.cuenta)
                    .then((response) => {

                        this.nombreCuenta = response.data.nombreCuenta;
                        this.debcred = response.data.naturalezaCuenta;

                        if(response.data.nombreCuenta == '' || response.data.nombreCuenta == null){
                            alert('Esta cuenta no esta creada en el catalogo.');
                            document.getElementById("cuenta").focus();
                        }

                    });

                }else{

                    alert('Cuenta incorrecta.');
                    document.getElementById("cuenta").focus();
                }

            }else{
                this.nombreCuenta = '';
            }
        },
        
        buscarCta2: function(){

            if(this.cuentaDebito != '' && this.showModal_cuentas_debito == false){

                if(this.cuentaDebito.length == 9){

                    var fecha = document.getElementById('fecha2').value;

                    axios.post('vue/cont-servicios-editaServicios.php?action=buscaCta2&cuenta=' + this.cuentaDebito+'&destinoCompra='+this.dc+'&fecha='+fecha)
                    .then((response) => {

                        if(response.data.nombreCuenta == '' || response.data.nombreCuenta == null)
                        {
                            alert('Esta cuenta no esta en el rango de cuenta o no esta creada en el catalogo.');
                            this.cuentaDebito = '';
                            this.nombreCuentaDebito = '';
                            this.debcred2 = '';
                        }
                        else
                        {
                            this.nombreCuentaDebito = response.data.nombreCuenta;
                            this.debcred2 = response.data.naturalezaCuenta;
                        }

                    });

                }else{

                    alert('Cuenta incorrecta.');
                    this.cuentaDebito = '';
                    this.nombreCuentaDebito = '';
                    this.debcred2 = '';
                }

            }else{
                this.nombreCuentaDebito = '';
            }
        },

        agregarDetalle: function(){

            var fec = document.getElementById('fecha').value;
            var cuentaCredito = [];
            this.cuentasCredito = [];

            if(this.cuenta != '' && fec != '' && this.debcred != '' && this.nombreCuenta != ''){
                var detallesAgr = [];
                detallesAgr.push(fec);
                detallesAgr.push(this.cuenta);
                detallesAgr.push(this.nombreCuenta);
                detallesAgr.push(this.debcred);
                
                cuentaCredito.push(this.cuenta);
                cuentaCredito.push(this.nombreCuenta);

                this.cuentasCredito.push(cuentaCredito);
                this.detallesFuncionamiento.push(detallesAgr);
                this.detalles.push(detallesAgr);
                
                this.nombreCuenta = '';
                this.cuenta = '';
                this.searchCuenta.keywordCuenta = '';

                
                
            }else{
                alert('Falta informacion para agregar');
            }
        },

        agregarDetalle2: function()
        {
            var fec2 = document.getElementById('fecha2').value;
            

            if(this.cuentaAsociada != '' && this.cuentaDebito != '' && fec2 != '' && this.debcred2 != '' && this.cc2 != '' && this.nombreCuentaDebito != '' && this.dc != ''){
                var detallesAgr = [];
                
                detallesAgr.push(fec2);
                detallesAgr.push(this.cuentaDebito);
                detallesAgr.push(this.nombreCuentaDebito);
                detallesAgr.push(this.cc2);
                detallesAgr.push(this.debcred2);
                detallesAgr.push(this.cuentaAsociada);
                detallesAgr.push(this.dc);

                var i = 0;
                var nombre = '';

                while(i < this.detallesFuncionamiento.length)
                {
                    if(this.cuentaAsociada == this.detallesFuncionamiento[i][1])
                    {
                        nombre = this.detallesFuncionamiento[i][2];
                    }
                    i++;
                }
                detallesAgr.push(nombre);

                this.detallesFuncionamiento2.push(detallesAgr);
                this.detalles2.push(detallesAgr);
                console.log(this.detalles2);
                
                
                this.nombreCuentaDebito = '';
                this.cuentaDebito = '';
                this.debcred2 = '';
                this.searchCuenta.keywordCuenta = '';
                this.cuentaAsociada = '';
               
            }else{
                alert('Falta informacion para agregar');
            }
        },

        searchMonitorCuenta: function(){
            var keywordCuenta = app.toFormData(app.searchCuenta);
            axios.post('vue/cont-servicios-editaServicios.php?action=filtrarCuentas', keywordCuenta)
            .then((response) => {
                app.cuentasContables = response.data.cuentas;
                
                
            });
        },

        searchMonitorCuentaDebito: function(){
            
        },

        toFormData: function(obj){
            var form_data = new FormData();
            for(var key in obj){
                form_data.append(key, obj[key]);
            }
            return form_data;
        },

        eliminarCuentaCredito: function (item) 
        {
            for(var i = 0; i < this.cuentasCredito.length-1; i++) {
                for(var j = 0; j < this.cuentasCredito[i].length-1; j++) {
                    var x = this.cuentasCredito[i][j].indexOf( item );
                    if ( x !== -1 ) {
                        this.cuentasCredito.splice( x, 1 );
                    }
                }
            }    
        },

        eliminarDetalle: function(detalleEliminar){
            for(var i = 0; i < this.detallesFuncionamiento2.length; i++)
            {
                if(detalleEliminar[1] == this.detallesFuncionamiento2[i][5])
                {
                    var valorEncontradoIgual = 'S';
                    
                    
                }
                
            }
            if(valorEncontradoIgual != 'S')
            {
                this.removeItemFromArr(detalleEliminar);
                this.removeItemFromArrEditar(detalleEliminar);
                this.eliminarCuentaCredito(detalleEliminar[1]);
                this.detalleEliminar.push(detalleEliminar);
            }
            else
            {
                alert('NO se puede eliminar una cuenta CREDITO que ya tenga cuenta/s DEBITO asociadas.');
            }  
        },

        removeItemFromArr: function(item){
            var i = this.detallesFuncionamiento.indexOf( item );
            if ( i !== -1 ) {
                this.detallesFuncionamiento.splice( i, 1 );
            }
        },

        removeItemFromArrEditar: function(item){
            var i = this.detalles.indexOf( item );
            if ( i !== -1 ) {
                this.detalles.splice( i, 1 );
            }
        },

        eliminarDetalle2: function(detalleEliminar){
            this.removeItemFromArr2(detalleEliminar);
            this.removeItemFromArrEditar2(detalleEliminar);
            this.detalleEliminar2.push(detalleEliminar);
        },

        removeItemFromArr2: function(item){
            var i = this.detallesFuncionamiento2.indexOf( item );
            if ( i !== -1 ) {
                this.detallesFuncionamiento2.splice( i, 1 );
            }
        },

        removeItemFromArrEditar2: function(item){
            var i = this.detalles2.indexOf( item );
            if ( i !== -1 ) {
                this.detalles2.splice( i, 1 );
            }
        },

        seleccionarCuenta: function(cuentaSelec){
            console.log(cuentaSelec);
            if(cuentaSelec[5] == 'AUXILIAR' || cuentaSelec[5] == 'Auxiliar' || cuentaSelec[5] == 'auxiliar'){
                this.cuenta = cuentaSelec [0];
                this.nombreCuenta = cuentaSelec [1];
                this.debcred = cuentaSelec [2];
                this.showModal_cuentas = false;
            }else{
                alert('Debe seleccionar una cuenta de tipo Auxiliar');
            }
            
        },

        seleccionarCuentaDebito: function(cuentaSelec){
            if(cuentaSelec[5] == 'AUXILIAR' || cuentaSelec[5] == 'Auxiliar' || cuentaSelec[5] == 'auxiliar'){
                this.cuentaDebito = cuentaSelec [0];
                this.nombreCuentaDebito = cuentaSelec [1];
                this.debcred2 = cuentaSelec [2];
                this.showModal_cuentas_debito = false;
                
            }else{
                alert('Debe seleccionar una cuenta de tipo Auxiliar');
            }
            
        },

        guardar: function(){
            
            if(this.nombreMostrar != '' || (this.nombreMostrar != this.nombreInicial) &&(this.detalles.length > 0 || this.detalleEliminar.length > 0)){
    

                var formData = new FormData();
    
                
                for(var x = 0; x < this.detalles.length; x++){
                    formData.append("detallesAgr[]", this.detalles[x]);
                }

                for(var x = 0; x < this.detalles2.length; x++){
                    formData.append("detallesAgr2[]", this.detalles2[x]);
                }

                if(this.detalleEliminar.length > 0)
                {
                    for(var x = 0; x < this.detalleEliminar.length; x++){
                        formData.append("detalleEliminar[]", this.detalleEliminar[x]);
                    }
                }

                if(this.detalleEliminar2.length > 0)
                {
                    for(var x = 0; x < this.detalleEliminar2.length; x++){
                        formData.append("detalleEliminar2[]", this.detalleEliminar2[x]);
                    }
                }
                
                formData.append("nombre", this.nombreMostrar);
                formData.append("tipo", this.tipogf);
                formData.append("codigo", this.codigoMostrar);

                axios.post('vue/cont-servicios-editaServicios.php?action=guardar',
                    formData)
                    .then(function(response){
                        
                        if(response.data.insertaBien){
                            location.href = "cont-servicios-editaServicios.php?is=" + app.codigoMostrar;
                        }
                });
    
            }
            else if(this.changedCorrelacion)
            {
                var formData = new FormData();

                formData.append("tipo", this.tipogf);
                formData.append("codigo", this.codigoMostrar);

                formData.append("correlacion", this.correlacion);
                formData.append("conceptoContableHermano", this.conceptoContableHermano);


                axios.post('vue/cont-servicios-editaServicios.php?action=guardar', formData)
                    .then(function(response){
                        
                        if(response.data.insertaBien){
                            location.href = "cont-servicios-editaServicios.php?is=" + app.codigoMostrar;
                        }
                    });
            }
            else
            {
                alert('Falta informacion para guardar.');
            }
        },

        changePage: function(page){
            this.page = page;
            if(this.page > 0 && this.page <= this.numeroMax){
                this.page = this.zfill(this.page, 2);
                location.href = "cont-servicios-editaServicios.php?is=" + this.page;
            }
        },

        checkCorrelacion: function(event){
            
            if(this.correlacion == 'principal' && this.tieneHijos === true)
            {
                alert('Tiene conceptos contables asociados');
                event.preventDefault();
            }

            this.changedCorrelacion = true;

            return;
        },

        mostarCuentaCred: function(){
            this.isActiveCred = true;
            this.isActiveDeb = false;
        },

        mostarCuentaDeb: function(){
            this.isActiveCred = false;
            this.isActiveDeb = true;
        },

    },
})
