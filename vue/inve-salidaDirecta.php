<?php
    require '../comun.inc';
    require '../funciones.inc';
    header('Cache-control: private'); // Arregla IE 6
    header("Cache-control: no-cache, no-store, must-revalidate");
    header("Content-Type: text/html;charset=utf8");
    date_default_timezone_set('America/Bogota');
    session_start();
    $linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");

    $out = array('error' => false);

    $action = "show";

    if(isset($_GET['action'])){
        $action = $_GET['action'];
    }

    if($action == 'show')
    {
        $bodegas = array();
        $centroCostos = array();
        $destinoSalida = array();
        $sectores = array();

        $sqlBodegas = "SELECT id_cc, nombre FROM almbodegas WHERE estado = 'S'";
        $resBodegas = mysqli_query($linkbd, $sqlBodegas);
        while ($rowBodegas = mysqli_fetch_row($resBodegas)) {
            
            array_push($bodegas, $rowBodegas);
        }

        $sqlCentroCosto = "SELECT id_cc, nombre FROM centrocosto WHERE estado='S' AND entidad='S' ORDER BY id_cc";
        $resCentroCosto = mysqli_query($linkbd, $sqlCentroCosto);
        while ($rowCentroCosto = mysqli_fetch_row($resCentroCosto)) {
            
            array_push($centroCostos, $rowCentroCosto);
        }

        $sqlSectores = "SELECT S.nombre, CS.id_cuenta FROM ccpet_cuentasectores AS CS INNER JOIN ccpetsectores AS S ON CS.id_sector = S.codigo";
        $resSectores = mysqli_query($linkbd, $sqlSectores);
        while ($rowSectores = mysqli_fetch_row($resSectores)) {

            array_push($sectores, $rowSectores);
        }
    
        $out['bodegas'] = $bodegas;
        $out['centroCostos'] = $centroCostos;
        $out['sectores'] = $sectores;
    }

    if ($action == 'consecutivo') {

        $movimiento = $_GET['movimiento'];
        $sqlConsecutivo = "SELECT consec FROM almginventario WHERE tipomov = '$movimiento' AND tiporeg = '06' ORDER BY consec DESC";
        $rowConsecutivo = mysqli_fetch_row(mysqli_query($linkbd, $sqlConsecutivo));

        $consecutivo = $rowConsecutivo[0] + 1;

        $out['consecutivo'] = $consecutivo;
    }

    if ($action == 'listaArticulos') {

        //Inicializa variables
        $bodega = $_GET['bodega'];
        $cc = $_GET['cc'];
        $modulo = 3;
        $listadoArticulos = array();

        $codigosEntrada = array();
        $codigosSalida = array();

        $articulosEntrada = array();
        $articulosSalida = array();

        //Tipo de comprobantes
        
        //Entrada
        $sqlComprobanteEntrada = "SELECT codigo FROM tipo_comprobante WHERE id_cat = $modulo AND tipo_movimiento = 1";
        $resComprobanteEntrada = mysqli_query($linkbd, $sqlComprobanteEntrada);
        while ($rowComprobanteEntrada = mysqli_fetch_row($resComprobanteEntrada)) {
            $codigosEntrada[] = $rowComprobanteEntrada[0];
        }

        //En grupa todos los tipos de comprobantes de entrada
        $entrada = implode(",", $codigosEntrada);

        //Busca todos los movimientos de entrada con la cantidad de articulos y su valor total
        $sqlComprobantesEntrada = "SELECT numacti, SUM(cantarticulo), SUM(valdebito) FROM comprobante_det WHERE tipo_comp IN ($entrada) AND centrocosto = '$cc' AND bodega_ubicacion = '$bodega' AND valdebito != 0 AND numacti != '' GROUP BY (numacti)";
        $resComprobantesEntrada = mysqli_query($linkbd, $sqlComprobantesEntrada);
        while ($rowComprobantesEntrada = mysqli_fetch_row($resComprobantesEntrada)) {
            
            array_push($articulosEntrada, $rowComprobantesEntrada);
        }

        //Salida
        $sqlComprobanteSalida = "SELECT codigo FROM tipo_comprobante WHERE id_cat = $modulo AND tipo_movimiento = 2";
        $resComprobanteSalida = mysqli_query($linkbd, $sqlComprobanteSalida);
        while ($rowComprobanteSalida = mysqli_fetch_row($resComprobanteSalida)) {
            $codigosSalida[] = $rowComprobanteSalida[0];
        }

        //En grupa todos los tipos de comprobantes de salida
        $salida = implode(",", $codigosSalida);

        $sqlComprobantesSalida = "SELECT numacti, SUM(cantarticulo), SUM(valcredito) FROM comprobante_det WHERE tipo_comp IN ($salida) AND centrocosto = '$cc' AND bodega_ubicacion = '$bodega' AND valcredito != 0 AND numacti != '' GROUP BY (numacti)";
        $resComprobantesSalida = mysqli_query($linkbd, $sqlComprobantesSalida);
        while ($rowComprobantesSalida = mysqli_fetch_row($resComprobantesSalida)) {
            
            array_push($articulosSalida, $rowComprobantesSalida);
        }
        
        //Recorre el array de articulos de entrada
        foreach ($articulosEntrada as $key => $articulo) {

            $cantidadSalida = 0;
            $valorSalida = 0;
            $saldoArticulo = 0;
            $codigoArticulo2 = "";
            $codigoArticulo2 = $articulo[0];
            $posicion = "";
            
            //Busca el codigo de articulo en el array de salidas en la columna codigo y obtiene su  posicion
            $posicion = array_search($codigoArticulo2, array_column($articulosSalida, 0));

            //Si encuentra la posicion va y busca en el array de salidas y toma la cantidad de articulos de salida y el valor total de esas salidas

            $a = gettype($posicion);
            //echo $codigoArticulo2 . " - " . $a . "\n"; 

            if ($a != 'boolean') { 
                
                $cantidadSalida = $articulosSalida[$posicion][1];
                $valorSalida = $articulosSalida[$posicion][2];
                //echo "Encontro " . $codigoArticulo2 . " - " . $posicion . "\n";
            }   
            else {
                //echo "No encontro " . $codigoArticulo2 . " - " . $posicion .  "\n";
            }
            
            $saldoArticulo = $articulo[1] - $cantidadSalida;
            
            if ($saldoArticulo > 0) {
                
                $datos = array();
                $codigoarticulo = "";
                $codigo = "";
                $saldoValorTotal = 0;
                $valorUnitario = 0;
                $grupo = "";

                $saldoValorTotal = $articulo[2] - $valorSalida;
                $valorUnitario = $saldoValorTotal / $saldoArticulo;

                $codigoarticulo = $articulo[0];
                $grupo = substr($codigoarticulo, 0, -5);
                $codigo = substr($codigoarticulo, 4);
                
                $sqlGrupoInventario = "SELECT nombre, concepent FROM almgrupoinv WHERE codigo = '$grupo'";
                $rowGrupoInventario = mysqli_fetch_row(mysqli_query($linkbd, $sqlGrupoInventario));

                $sqlCuentaContableArticulo = "SELECT cuenta FROM conceptoscontables_det WHERE codigo = '$rowGrupoInventario[1]' AND tipo = 'AE' AND debito = 'S' ";
                $rowCuentaContableArticulo = mysqli_fetch_row(mysqli_query($linkbd, $sqlCuentaContableArticulo));

                $sqlArticulos = "SELECT nombre, codunspsc FROM almarticulos WHERE codigo = '$codigo' AND grupoinven = '$grupo'";
                $rowArticulos = mysqli_fetch_row(mysqli_query($linkbd, $sqlArticulos));

                $sqlArticulosDet = "SELECT unidad FROM almarticulos_det WHERE articulo = '$codigoarticulo'";
                $rowArticulosDet = mysqli_fetch_row(mysqli_query($linkbd, $sqlArticulosDet));

                array_push($datos, $codigoarticulo);
                array_push($datos, $rowArticulos[0]);
                array_push($datos, $rowGrupoInventario[0]);
                array_push($datos, $saldoArticulo);
                array_push($datos, $rowArticulosDet[0]);
                array_push($datos, $bodega);
                array_push($datos, $cc);
                array_push($datos, round($valorUnitario,2));
                array_push($datos, $rowArticulos[1]);
                array_push($datos, $rowCuentaContableArticulo[0]);
                array_push($listadoArticulos, $datos);
            }
        }

        $out['articulos'] = $listadoArticulos;
    }

    if ($action == 'filtrarArticulos') {

        $keywordArticulo = $_POST['keywordArticulo'];
        $condicion = '';

        if ($keywordArticulo != '') {
            $condicion = "AND concat_ws('', grupoinven, codigo) LIKE '%$keywordArticulo%' ";
        }
        
        $bodega = $_POST['bodega'];
        $cc = $_POST['cc'];
        $modulo = 3;
        $codigosEntrada = array();
        $codigosSalida = array();
        $listadoArticulos = array();
        //Tipo de comprobantes
        
        //Entrada
        $sqlComprobanteEntrada = "SELECT codigo FROM tipo_comprobante WHERE id_cat = $modulo AND tipo_movimiento = 1";
        $resComprobanteEntrada = mysqli_query($linkbd, $sqlComprobanteEntrada);
        while ($rowComprobanteEntrada = mysqli_fetch_row($resComprobanteEntrada)) {
            $codigosEntrada[] = $rowComprobanteEntrada[0];
        }

        //Salida
        $sqlComprobanteSalida = "SELECT codigo FROM tipo_comprobante WHERE id_cat = $modulo AND tipo_movimiento = 2";
        $resComprobanteSalida = mysqli_query($linkbd, $sqlComprobanteSalida);
        while ($rowComprobanteSalida = mysqli_fetch_row($resComprobanteSalida)) {
            $codigosSalida[] = $rowComprobanteSalida[0];
        }

        $sqlArticulos = "SELECT codigo, grupoinven, nombre, codunspsc FROM almarticulos WHERE estado = 'S' $condicion";
        $resArticulos = mysqli_query($linkbd, $sqlArticulos);
        while ($rowArticulos = mysqli_fetch_assoc($resArticulos)) {
            $codArticulo = $rowArticulos['grupoinven'].$rowArticulos['codigo'];
            $valorEntradas = 0;
            $valorSalidas = 0;
            $saldoArticulo = 0;
            $valorTotalEntradas = 0;
            $valorTotalSalidas = 0;
            $datos = array();

            for ($i=0; $i < count($codigosEntrada); $i++) { 
                $sqlComprobantesEntrada = "SELECT SUM(cantarticulo), SUM(valdebito) FROM comprobante_det WHERE tipo_comp = $codigosEntrada[$i] AND numacti = '$codArticulo' AND centrocosto = '$cc' AND bodega_ubicacion = '$bodega' AND valdebito != 0";
                $rowComprobantesEntrada = mysqli_fetch_row(mysqli_query($linkbd, $sqlComprobantesEntrada));

                if ($rowComprobantesEntrada[0] != NULL) {
                    $valorEntradas += $rowComprobantesEntrada[0];
                    $valorTotalEntradas += $rowComprobantesEntrada[1];
                }
            }

            for ($i=0; $i < count($codigosSalida); $i++) { 
                $sqlComprobantesSalida = "SELECT SUM(cantarticulo), SUM(valcredito) FROM comprobante_det WHERE tipo_comp = $codigosSalida[$i] AND numacti = '$codArticulo' AND centrocosto = '$cc' AND bodega_ubicacion = '$bodega' AND valcredito != 0";
                $rowComprobantesSalida = mysqli_fetch_row(mysqli_query($linkbd, $sqlComprobantesSalida));

                if ($rowComprobantesSalida[0] != NULL) {
                    $valorSalidas += $rowComprobantesSalida[0]; 
                    $valorTotalSalidas += $rowComprobantesSalida[1];
                }
            }

            $saldoArticulo = $valorEntradas - $valorSalidas;

            if($saldoArticulo > 0) {
    
                $saldoValorTotal = 0;
                $valorUnitario = 0;

                $sqlGrupoInventario = "SELECT nombre, concepent FROM almgrupoinv WHERE codigo = '$rowArticulos[grupoinven]'";
                $rowGrupoInventario = mysqli_fetch_row(mysqli_query($linkbd, $sqlGrupoInventario));

                $sqlCuentaContableArticulo = "SELECT cuenta FROM conceptoscontables_det WHERE codigo = '$rowGrupoInventario[1]' AND tipo = 'AE' AND debito = 'S' ";
                $rowCuentaContableArticulo = mysqli_fetch_row(mysqli_query($linkbd, $sqlCuentaContableArticulo));

                $sqlArticulosDet = "SELECT unidad FROM almarticulos_det WHERE articulo = '$codArticulo'";
                $rowArticulosDet = mysqli_fetch_row(mysqli_query($linkbd, $sqlArticulosDet));

                $saldoValorTotal = $valorTotalEntradas - $valorTotalSalidas;
                $valorUnitario = $saldoValorTotal / $saldoArticulo;

                array_push($datos, $codArticulo);
                array_push($datos, $rowArticulos['nombre']);
                array_push($datos, $rowGrupoInventario[0]);
                array_push($datos, $saldoArticulo);
                array_push($datos, $rowArticulosDet[0]);
                array_push($datos, $bodega);
                array_push($datos, $cc);
                array_push($datos, round($valorUnitario,2));
                array_push($datos, $rowArticulos['codunspsc']);
                array_push($datos, $rowCuentaContableArticulo[0]);
                array_push($listadoArticulos, $datos);
            }
        }

        $out['articulos'] = $listadoArticulos;
    }

    if ($action == 'buscarArticulos') {

        $bodega = $_GET['bodega'];
        $cc = $_GET['cc'];
        $articulo = $_GET['articulo'];
        $modulo = 3;
        $codigosEntrada = array();
        $codigosSalida = array();
        $listadoArticulos = array();
        //Tipo de comprobantes
        
        //Entrada
        $sqlComprobanteEntrada = "SELECT codigo FROM tipo_comprobante WHERE id_cat = $modulo AND tipo_movimiento = 1";
        $resComprobanteEntrada = mysqli_query($linkbd, $sqlComprobanteEntrada);
        while ($rowComprobanteEntrada = mysqli_fetch_row($resComprobanteEntrada)) {
            $codigosEntrada[] = $rowComprobanteEntrada[0];
        }

        //Salida
        $sqlComprobanteSalida = "SELECT codigo FROM tipo_comprobante WHERE id_cat = $modulo AND tipo_movimiento = 2";
        $resComprobanteSalida = mysqli_query($linkbd, $sqlComprobanteSalida);
        while ($rowComprobanteSalida = mysqli_fetch_row($resComprobanteSalida)) {
            $codigosSalida[] = $rowComprobanteSalida[0];
        }

        $sqlArticulos = "SELECT codigo, grupoinven, nombre, codunspsc FROM almarticulos WHERE estado = 'S' AND concat_ws('', grupoinven, codigo) LIKE '%$articulo%'";
        $resArticulos = mysqli_query($linkbd, $sqlArticulos);
        while ($rowArticulos = mysqli_fetch_assoc($resArticulos)) {
            $codArticulo = $rowArticulos['grupoinven'].$rowArticulos['codigo'];
            $valorEntradas = 0;
            $valorSalidas = 0;
            $saldoArticulo = 0;
            $valorTotalEntradas = 0;
            $valorTotalSalidas = 0;
            $datos = array();

            for ($i=0; $i < count($codigosEntrada); $i++) { 
                $sqlComprobantesEntrada = "SELECT SUM(cantarticulo), SUM(valdebito) FROM comprobante_det WHERE tipo_comp = $codigosEntrada[$i] AND numacti = '$codArticulo' AND centrocosto = '$cc' AND bodega_ubicacion = '$bodega' AND valdebito != 0";
                $rowComprobantesEntrada = mysqli_fetch_row(mysqli_query($linkbd, $sqlComprobantesEntrada));

                if ($rowComprobantesEntrada[0] != NULL) {
                    $valorEntradas += $rowComprobantesEntrada[0];
                    $valorTotalEntradas += $rowComprobantesEntrada[1];
                }
            }

            for ($i=0; $i < count($codigosSalida); $i++) { 
                $sqlComprobantesSalida = "SELECT SUM(cantarticulo), SUM(valcredito) FROM comprobante_det WHERE tipo_comp = $codigosSalida[$i] AND numacti = '$codArticulo' AND centrocosto = '$cc' AND bodega_ubicacion = '$bodega' AND valcredito != 0";
                $rowComprobantesSalida = mysqli_fetch_row(mysqli_query($linkbd, $sqlComprobantesSalida));

                if ($rowComprobantesSalida[0] != NULL) {
                    $valorSalidas += $rowComprobantesSalida[0]; 
                    $valorTotalSalidas += $rowComprobantesSalida[1];
                }
            }

            $saldoArticulo = $valorEntradas - $valorSalidas;

            if($saldoArticulo > 0) {

                $saldoValorTotal = 0;
                $valorUnitario = 0;

                $sqlGrupoInventario = "SELECT nombre, concepent FROM almgrupoinv WHERE codigo = '$rowArticulos[grupoinven]'";
                $rowGrupoInventario = mysqli_fetch_row(mysqli_query($linkbd, $sqlGrupoInventario));

                $sqlCuentaContableArticulo = "SELECT cuenta FROM conceptoscontables_det WHERE codigo = '$rowGrupoInventario[1]' AND tipo = 'AE' AND debito = 'S' ";
                $rowCuentaContableArticulo = mysqli_fetch_row(mysqli_query($linkbd, $sqlCuentaContableArticulo));

                $sqlArticulosDet = "SELECT unidad FROM almarticulos_det WHERE articulo = '$codArticulo'";
                $rowArticulosDet = mysqli_fetch_row(mysqli_query($linkbd, $sqlArticulosDet));

                $saldoValorTotal = $valorTotalEntradas - $valorTotalSalidas;
                $valorUnitario = $saldoValorTotal / $saldoArticulo;

                
            }
            else {

            }
        }

    }

    if ($action == 'buscarContrucciones') {
        
        $contrucciones = array();

        $sqlContruccion = "SELECT orden, fecha, descripcion, cuenta FROM acti_construccion WHERE estado = 'S' ORDER BY orden ASC";
        $resContruccion = mysqli_query($linkbd, $sqlContruccion);
        while ($rowContruccion = mysqli_fetch_row($resContruccion)) {
            array_push($contrucciones, $rowContruccion);
        }

        $out['contrucciones'] = $contrucciones;
    }

    if ($action == 'buscarMontajes') {
        
        $montajes = array();

        $sqlMontajes = "SELECT orden, fecha, descripcion, supervisor, cuenta FROM acti_montajes WHERE estado = 'S' ORDER BY orden ASC";
        $resMontajes = mysqli_query($linkbd, $sqlMontajes);
        while ($rowMontajes = mysqli_fetch_row($resMontajes)) {

            array_push($montajes, $rowMontajes);
        }

        $out['montajes'] = $montajes;
    }

    if($action == 'buscarCuentasDebito') {

        $modulo = "5";
        $destino = $_GET['destino'];
        
        if ($destino == 'AG' || $destino == 'AC' || $destino == 'AP') {

            $cuentas = array();
            $cc = $_GET['cc'];

            $sqlConceptosContables = "SELECT CC.codigo, CC.nombre, CCD.cuenta FROM conceptoscontables AS CC INNER JOIN conceptoscontables_det AS CCD ON CCD.codigo = CC.codigo WHERE CC.modulo = $modulo AND CC.tipo = '$destino' AND CCD.tipo = '$destino' AND CCD.cc = '$cc' ORDER BY CCD.fechainicial DESC";
            $resConceptosContables = mysqli_query($linkbd, $sqlConceptosContables);
            while ($rowConceptosContables = mysqli_fetch_row($resConceptosContables)) {
                
                array_push($cuentas, $rowConceptosContables);
            }
            
            $out['cuentas'] = $cuentas;
        }

        if ($destino == 'AI') {

            $cuentas = array();
            $sector = $_GET['sector'];
            $cc = $_GET['cc'];

            $sqlConceptosContables = "SELECT CC.codigo, CC.nombre, CCD.cuenta FROM conceptoscontables AS CC INNER JOIN conceptoscontables_det AS CCD ON CCD.codigo = CC.codigo WHERE CC.modulo = $modulo AND CC.tipo = '$destino' AND CCD.tipo = '$destino' AND CCD.cc = '$cc' AND CCD.cuenta LIKE '$sector%' ORDER BY CCD.fechainicial DESC";
            $resConceptosContables = mysqli_query($linkbd, $sqlConceptosContables);
            while ($rowConceptosContables = mysqli_fetch_row($resConceptosContables)) {
                
                array_push($cuentas, $rowConceptosContables);
            }
            
            $out['cuentas'] = $cuentas;
        }
    }

    if ($action == 'guardar') {

       
        preg_match("/([0-9]{2})\/([0-9]{2})\/([0-9]{4})/", $_POST["fecha"], $f);
        $fecha = $f[3]."-".$f[2]."-".$f[1];

        $sqlr = "SELECT count(*) From dominios dom  where dom.nombre_dominio = 'PERMISO_MODIFICA_DOC'  and dom.valor_final <= '$fecha'  AND dom.valor_inicial =  '$_SESSION[cedulausu]' ";
        $res = mysqli_query($linkbd,$sqlr);
        $row = mysqli_fetch_row($res);

        if ($row[0] > 0) {

            $out['bloqueo'] = true;

            $sqlEntidad = "SELECT nit FROM configbasica";
            $rowEntidad = mysqli_fetch_row(mysqli_query($linkbd, $sqlEntidad));

            $nitEntidad = $rowEntidad[0];
            $tipoComprobante = '65';

            $usuario = $_SESSION['cedulausu'];
            $tipoMovimiento = $_POST['tipoMovimiento'];
            $consecutivo = $_POST['consecutivo'];
            $vigencia = $_POST['vigencia'];
            $descripcion = $_POST['descripcion'];
            $totalSalida = $_POST['totalSalida'];
            $bodega = $_POST['bodega'];

            //var_dump($_POST['detalles']);
            $sqlInventarioCabecera = "INSERT INTO almginventario (consec, fecha, tipomov, tiporeg, valortotal, usuario, estado, nombre, bodega1, bodega2, vigenciadoc) VALUES ('$consecutivo', '$fecha', '$tipoMovimiento', '06', $totalSalida, '$usuario', 'S', '$descripcion', '$bodega', '$bodega', '$vigencia')";
            mysqli_query($linkbd, $sqlInventarioCabecera);        

            $sqlCabeceraContabilidad = "INSERT INTO comprobante_cab (numerotipo, tipo_comp, fecha, concepto, total, total_debito, total_credito, diferencia, estado) VALUES ('$consecutivo', '$tipoComprobante', '$fecha', '$descripcion', $totalSalida, $totalSalida, $totalSalida, 0, '1')";
            mysqli_query($linkbd, $sqlCabeceraContabilidad);
            
            for($x = 0; $x < count($_POST["detalles"]); $x++) {
                
                //Inicializa variables
                $codigoArticulo = '';
                $unspsc = '';
                $cantidadSalida = '';
                $valorUnitario = '';
                $valorTotal = '';
                $unidad = '';
                $bodega = '';
                $codigoCuentaContable = '';
                $centroCostosArticulo = '';
                $cuentaContableArticulo = '';

                //Rellena variable
                $codigoArticulo = $_POST['detalles'][$x][0];
                $unspsc = $_POST['detalles'][$x][12];
                $cantidadSalida = $_POST['detalles'][$x][3];
                $valorUnitario = $_POST['detalles'][$x][10];
                $valorTotal = $_POST['detalles'][$x][11];
                $unidad = $_POST['detalles'][$x][4];
                $bodega = $_POST['detalles'][$x][5];
                $codigoCuentaContable = $_POST['detalles'][$x][7];
                $centroCostosArticulo = $_POST['detalles'][$x][6];
                $cuentaContableArticulo = $_POST['detalles'][$x][13];

                $sqlInventarioDetalles = "INSERT INTO almginventario_det (codigo, unspsc, codart, cantidad_salida, valorunit, valortotal, unidad, tipomov, tiporeg, bodega, codcuentacre, cc, concepto) VALUES ('$consecutivo', '$unspsc', '$codigoArticulo', $cantidadSalida, $valorUnitario, $valorTotal, '$unidad', '$tipoMovimiento', '06', '$bodega', '$codigoCuentaContable', '$centroCostosArticulo', '$cuentaContableArticulo')";
                mysqli_query($linkbd, $sqlInventarioDetalles);


                //ComprobanteDebito
                $sqlDetallesContabilidad = "INSERT INTO comprobante_det (id_comp, cuenta, tercero, centrocosto, detalle, valdebito, valcredito, estado, vigencia, tipo_comp, numerotipo, numacti, cantarticulo, bodega_ubicacion) VALUES ('$tipoComprobante $consecutivo', '$codigoCuentaContable', '$nitEntidad', '$centroCostosArticulo', '$descripcion', $valorTotal, 0, '1', '$vigencia', '$tipoComprobante', '$consecutivo', '$codigoArticulo', '$cantidadSalida', '$bodega')";
                mysqli_query($linkbd, $sqlDetallesContabilidad);

                //ComprobanteCredito
                $sqlDetallesContabilidad = "INSERT INTO comprobante_det (id_comp, cuenta, tercero, centrocosto, detalle, valdebito, valcredito, estado, vigencia, tipo_comp, numerotipo, numacti, cantarticulo, bodega_ubicacion) VALUES ('$tipoComprobante $consecutivo', '$cuentaContableArticulo', '$nitEntidad', '$centroCostosArticulo', '$descripcion', 0, $valorTotal, '1', '$vigencia', '$tipoComprobante', '$consecutivo', '$codigoArticulo', '$cantidadSalida', '$bodega')";
                mysqli_query($linkbd, $sqlDetallesContabilidad);
            }

            $out['insertaBien'] = true;
        }
        else {

        }
    }

    if ($action == "buscaSalidas") {
        
        preg_match("/([0-9]{2})\/([0-9]{2})\/([0-9]{4})/", $_POST["fechaRev"], $f);
        $fechaRev = $f[3]."-".$f[2]."-".$f[1];

        $salidas = [];
        
        $sqlSalidas = "SELECT consec, fecha, nombre, valortotal FROM almginventario WHERE tipomov = '2' AND tiporeg = '06' AND fecha <= '$fechaRev' AND estado = 'S' ORDER BY consec DESC";
        $resSalidas = mysqli_query($linkbd, $sqlSalidas);
        while ($rowSalidas = mysqli_fetch_row($resSalidas)) {
            
            array_push($salidas, $rowSalidas);
        }

        $out['salidas'] = $salidas;
    }

    if ($action == "buscaDatosSalida") {
        
        $consecutivo = $_POST['consecutivo'];
        $tipoMov = "2";
        $tipoReg = "06";
        $listadoArticulos = [];
        $total = 0;

        $sqlAlmacen = "SELECT codart, cantidad_salida, valorunit, valortotal, unidad, bodega, cc, codcuentacre, concepto FROM almginventario_det WHERE codigo = '$consecutivo' AND tipomov = '$tipoMov' AND tiporeg = '$tipoReg'";
        $resAlmacen = mysqli_query($linkbd, $sqlAlmacen);
        while ($rowAlmacen = mysqli_fetch_assoc($resAlmacen)) {
            
            $datosTemp = [];
            $codArticulo = $grupo = $codigo = $bodega = $unidad = $cc = "";
            $cantidad = $valorUni = $subTotal;

            $codArticulo = $rowAlmacen['codart'];
            $grupo = substr($codArticulo, 0, -5);
            $codigo = substr($codArticulo, 4);
            $bodega = $rowAlmacen['bodega'];
            $unidad = $rowAlmacen['unidad'];
            $cc = $rowAlmacen['cc'];
            $cantidad = $rowAlmacen['cantidad_salida'];
            $valorUni = $rowAlmacen['valorunit'];
            $subTotal = $rowAlmacen['valortotal'];
            $cuentaCredito = $rowAlmacen['codcuentacre'];
            $cuentaDebito = $rowAlmacen['concepto'];
            $total += $rowAlmacen['valortotal'];

            $sqlName = "SELECT nombre FROM almarticulos WHERE codigo = '$codigo' AND grupoinven = '$grupo'";
            $resName = mysqli_query($linkbd, $sqlName);
            $rowName = mysqli_fetch_assoc($resName);

            $codName = $rowName['nombre'];

            $datosTemp[] = $bodega;
            $datosTemp[] = $cc;
            $datosTemp[] = $codArticulo;
            $datosTemp[] = $codName;
            $datosTemp[] = $unidad;
            $datosTemp[] = $cantidad;
            $datosTemp[] = $valorUni;
            $datosTemp[] = $subTotal;
            $datosTemp[] = $cuentaCredito;
            $datosTemp[] = $cuentaDebito;
            array_push($listadoArticulos, $datosTemp);
        }

        $out['listadoArticulos'] = $listadoArticulos;
        $out['total'] = $total;
    }

    if ($action == "guardarRev") {

        $fecha = $consecutivo = $documento = "";
        $total = 0;
        $listaArticulos = $valuesInve = $valuesCont = [];

        $sqlEntidad = "SELECT nit FROM configbasica";
        $rowEntidad = mysqli_fetch_row(mysqli_query($linkbd, $sqlEntidad));

        $nitEntidad = $rowEntidad[0];

        preg_match("/([0-9]{2})\/([0-9]{2})\/([0-9]{4})/", $_POST["fechaRev"], $f);
        $fecha = $f[3]."-".$f[2]."-".$f[1];
        $vigencia = $f[3];
        $consecutivo = $_POST['consecutivo'];
        $documento = $_POST['documento'];
        $total = $_POST['valorDocumento'];
        $listaArticulos = $_POST['listaArticulos'];
        $usuario = $_SESSION['cedulausu'];
        $descripcion = "REVERSIÓN DE LA SALIDA DIRECTA NÚMERO $documento";
        $tipoComprobante = "2065";

        $sqlr = "SELECT count(*) From dominios dom  where dom.nombre_dominio = 'PERMISO_MODIFICA_DOC'  and dom.valor_final <= '$fecha'  AND dom.valor_inicial =  '$usuario' ";
        $res = mysqli_query($linkbd,$sqlr);
        $row = mysqli_fetch_row($res);

        if ($row[0] >= 1) {

            $sqlInveCab = "INSERT INTO almginventario (fecha, tipomov, tiporeg, valortotal, usuario, estado, nombre, consec, vigenciadoc) VALUES ('$fecha', '4', '06', $total, '$usuario', 'S', '$descripcion', $consecutivo, '$vigencia')";
            
            $sqlContCab = "INSERT INTO comprobante_cab (numerotipo, tipo_comp, fecha, concepto, total, estado) VALUES ($consecutivo, '$tipoComprobante', '$fecha', '$descripcion', $total, '1')";

            $sql_update = "UPDATE almginventario SET estado = 'R' WHERE tipomov = 2 AND tiporeg = '06' AND consec = $documento";

            foreach ($listaArticulos as $i => $articulos) {
                
                $bodega = $cc = $codArticulo = $unidadMedidad = "";
                $cantidad = $valorUnitario = $valorTotal = 0;

                $bodega = $articulos[0];
                $cc = $articulos[1];
                $codArticulo = $articulos[2];
                $unidadMedida = $articulos[4];
                $cantidad = $articulos[5];
                $valorUnitario = $articulos[6];
                $valorTotal = $articulos[7];
                $cuentaCredito = $articulos[8];
                $cuentaDebito = $articulos[9];

                $valuesInve[] = "($consecutivo, '$codArticulo', $cantidad, $valorUnitario, $valorTotal, '$unidadMedida', '4', '06', '$bodega', '$cuentaCredito', '$cc', '$cuentaDebito')";
                $valuesCont[] = "('$tipoComprobante $consecutivo', '$cuentaCredito', '$nitEntidad', '$cc', '$descripcion', 0, $valorTotal, '1', '$vigencia', '$codArticulo', $cantidad, '$bodega')";
                $valuesCont[] = "('$tipoComprobante $consecutivo', '$cuentaDebito', '$nitEntidad', '$cc', '$descripcion', $valorTotal, 0, '1', '$vigencia', '$codArticulo', $cantidad, '$bodega')";
            }

            $sqlInve = "INSERT INTO almginventario_det (codigo, codart, cantidad_entrada, valorunit, valortotal, unidad, tipomov, tiporeg, bodega, codcuentacre, cc, concepto) VALUES \n" . implode(",\n", $valuesInve);

            $sqlCont = "INSERT INTO comprobante_det (id_comp, cuenta, tercero, centrocosto, detalle, valdebito, valcredito, estado, vigencia, numacti, cantarticulo, bodega_ubicacion) VALUES \n" . implode(",\n", $valuesCont);

            mysqli_query($linkbd, $sqlInveCab);
            mysqli_query($linkbd, $sqlContCab);
            mysqli_query($linkbd, $sql_update);
            mysqli_query($linkbd, $sqlInve);
            mysqli_query($linkbd, $sqlCont);

            $out['respuesta'] = "0";
        }
        else {
            //Fecha bloqueada
            $out['respuesta'] = "1";
        }
    }

    header("Content-type: application/json");
    echo json_encode($out);
    die();