var app = new Vue({ 
	el: '#myapp',
	data:{
		loading: false,
		infobasico1:[],
	
	},
	mounted:
	function(){
		this.loading = false;
	},
	methods:{
		buscarinformacion: async function(fecha1,fecha2,treporte){
			var formData = new FormData();
			let dirreporte;
			formData.append('fecha1', fecha1);
			formData.append('fecha2', fecha2);
			switch (treporte){
				case '1': dirreporte = 'vue/contabilidad/cont-compararotrosegresos.php?accion=sel1';break;
				case '2': dirreporte = 'vue/contabilidad/cont-compararotrosegresos.php?accion=sel2';break;
				case '3': dirreporte = 'vue/contabilidad/cont-compararotrosegresos.php?accion=sel3';break;
			}
			await axios.post(dirreporte, formData)
			.then(
				(response)=>{
					this.infobasico1 = response.data.datosproceso1;
				}
			).catch((error) => {
				this.error = true;
			});
			this.loading = false;
		},
		iniciabuscar: function(tipopago){
			let fecha1 = document.getElementById('fc_1198971545').value;
			let fecha2 = document.getElementById('fc_1198971546').value;
			if(fecha1 != ''&& fecha2 != ''){
				let divfecha1 = fecha1.split('/');
				let divfecha2 = fecha2.split('/');
				let f1 = new Date(divfecha1[2], divfecha1[1], divfecha1[0]);
				let f2 = new Date(divfecha2[2], divfecha2[1], divfecha2[0]);
				let fecha1b = divfecha1[2]+"-"+divfecha1[1]+"-"+divfecha1[0];
				let fecha2b = divfecha2[2]+"-"+divfecha2[1]+"-"+divfecha2[0];
				if(f1 <= f2){
					switch (tipopago){
						case 1:
							this.loading = true;
							this.buscarinformacion(fecha1b,fecha2b,'1');
							break;
						case 2:
							this.loading = true;
							this.buscarinformacion(fecha1b,fecha2b,'2');
							break;
						case 3:
							this.loading = true;
							this.buscarinformacion(fecha1b,fecha2b,'3');
							break;
					}
				}
				else{
					Swal.fire(
						'Error!',
						'La Fecha Inicial no puede ser mayor a la Fecha Final',
						'error'
					);
				}
			}
			else{
				Swal.fire(
					'Error!',
					'Se debe ingresar Fecha Inicial y Fecha Final',
					'error'
				);
			}
		},
		formatonumero: function(valor){
			return new Intl.NumberFormat("en-US", {style: "currency", currency: "USD"}).format(valor);
		},
		genera_excell: function(){
			alert();
		},
		direccionaComprobante: function(idCat,tipo_compro,num_compro){
			window.open("cont-buscacomprobantes.php?idCat="+idCat+"&tipo_compro="+tipo_compro+"&num_compro="+num_compro);
		},
	},
});