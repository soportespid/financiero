<?php
	require_once '../../comun.inc';
	require '../../funciones.inc';
	$linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");
	if(isset($_GET['accion'])){
		$accion = $_GET['accion'];
		switch ($accion){
			case 'sel1':{
				$sqltt = "SELECT codigo FROM redglobal WHERE tipo = 'IN'";
				$restt = mysqli_query($linkbd,$sqltt);
				$rowtt = mysqli_fetch_row($restt);
				$unidadlocal = $rowtt[0];
				$datosproceso1 = array();
				$sql = "SELECT T1.id_trasladocab, T1.fecha, T1.ncuentaban1, T1.tipo_traslado, SUM(T1.valor), T2.concepto, T2.origen, T2.estado  FROM tesotraslados AS T1 INNER JOIN tesotraslados_cab AS T2 ON T1.id_trasladocab = T2.id_consignacion WHERE T1.fecha BETWEEN '".$_POST['fecha1']."' AND '".$_POST['fecha2']."'GROUP BY T1.id_trasladocab ORDER BY T1.id_trasladocab DESC";
				$res = mysqli_query($linkbd,$sql);
				while($row = mysqli_fetch_row($res)){
					if($row[6] != ''){
						$tipomoviento = $diferencia = $varcuenta = $vardebito = $varcredito = '';
						$datos = array();
						$fechaorden = date('d/m/Y',strtotime($row[1]));
						$varcuenta = '1110';
						$sqlcab = "SELECT estado FROM comprobante_cab WHERE tipo_comp = '10' AND numerotipo = '$row[0]'";
						$rescab = mysqli_query($linkbd,$sqlcab);
						$rowcab = mysqli_fetch_row($rescab);
						if($rowcab[0] == 0){$varestadocab = 'N';}
						else{$varestadocab = 'S';}
						$sqlcomp = "SELECT SUM(valdebito), SUM(valcredito) FROM comprobante_det WHERE tipo_comp = '10' AND numerotipo = '$row[0]' AND cuenta LIKE '$varcuenta%'";
						$rescomp = mysqli_query($linkbd,$sqlcomp);
						$rowcomp = mysqli_fetch_row($rescomp);
						if($rowcomp[0] != ''){$vardebito = $rowcomp[0];}
						else{$vardebito = 0;}
						if($rowcomp[1] != ''){$varcredito = $rowcomp[1];}
						else{$varcredito = 0;}
						if($row[6] == $unidadlocal){
							if($row[3] == 'INT'){$diferencia = ((float)$row[4] * 2) - ((float)$varcredito + (float)$vardebito);}
							else{$diferencia = (float)$row[4] - ((float)$varcredito - (float)$vardebito);}
							$tipomoviento = 'SALIDA';
						}
						else{
							$diferencia = (float)$row[4] - ((float)$vardebito - (float)$varcredito);
							$tipomoviento = 'ENTRADA';
						}
						array_push($datos, $row[0]);//0 id traslado
						array_push($datos, $fechaorden);//1 fecha traslado
						array_push($datos, $row[2]);//2 cuanta banco
						array_push($datos, $row[3]);//3 tipo traslado
						array_push($datos, $row[4]);//4 valor
						array_push($datos, $row[5]);//5 concepto
						array_push($datos, $row[7]);//6 estado egreso
						array_push($datos, $varcuenta);//7 cuenta contable
						array_push($datos, $varestadocab);//8 estado contabilidad
						array_push($datos, $vardebito);//9 debito
						array_push($datos, $varcredito);//10 credito
						array_push($datos, $diferencia);//11 diferencia
						array_push($datos, $tipomoviento);//12 
						array_push($datosproceso1, $datos);
					}
				}
				$out['datosproceso1'] = $datosproceso1;
			}break;
			case 'sel2':{
				$datosproceso1 = array();
				$sql = "SELECT id_pago, fecha, banco, cc, valor, concepto, estado, ajuste FROM tesopagoterceros WHERE fecha BETWEEN '".$_POST['fecha1']."' AND '".$_POST['fecha2']."' ORDER BY id_pago DESC";
				$res = mysqli_query($linkbd,$sql);
				while($row = mysqli_fetch_row($res)){
					$diferencia = $vardebito = $varcredito = '';
					$datos = array();
					$fechaorden = date('d/m/Y',strtotime($row[1]));
					$sqlcab = "SELECT estado FROM comprobante_cab WHERE tipo_comp = '12' AND numerotipo = '$row[0]'";
					$rescab = mysqli_query($linkbd,$sqlcab);
					$rowcab = mysqli_fetch_row($rescab);
					if($rowcab[0] == 0){$varestadocab = 'N';}
					else{$varestadocab = 'S';}
					$sqlcomp = "SELECT SUM(valdebito), SUM(valcredito) FROM comprobante_det WHERE tipo_comp = '12' AND numerotipo = '$row[0]' AND cuenta = '$row[2]'";
					$rescomp = mysqli_query($linkbd,$sqlcomp);
					$rowcomp = mysqli_fetch_row($rescomp);
					if($row[7] == ''){
						$valortercero = $row[4];
					}else{//$valortercero = $row[4];
						$valortercero = round($row[4]/1000)*1000;
					}
					if($rowcomp[0] != ''){$vardebito = $rowcomp[0];}
					else{$vardebito = 0;}
					if($rowcomp[1] != ''){$varcredito = $rowcomp[1];}
					else{$varcredito = 0;}
					$diferencia = (float) $valortercero - ((float)$varcredito - (float)$vardebito);
					array_push($datos, $row[0]);//0 id traslado
					array_push($datos, $fechaorden);//1 fecha traslado
					array_push($datos, $row[2]);//2 cuanta banco
					array_push($datos, $row[3]);//3 cc
					array_push($datos, $valortercero);//4 valor
					array_push($datos, $row[5]);//5 concepto
					array_push($datos, $row[6]);//6 estado egreso
					array_push($datos, $row[2]);//7 cuenta contable
					array_push($datos, $varestadocab);//8 estado contabilidad
					array_push($datos, $vardebito);//9 debito
					array_push($datos, $varcredito);//10 credito
					array_push($datos, $diferencia);//11 diferencia
					array_push($datosproceso1, $datos);
				}
				$out['datosproceso1'] = $datosproceso1;
			}break;
			case 'sel3':{
				$datosproceso1 = array();
				$sql = "SELECT id_pago, fecha, banco, cc, valor, concepto, estado FROM tesopagotercerosvigant WHERE fecha BETWEEN '".$_POST['fecha1']."' AND '".$_POST['fecha2']."' ORDER BY id_pago DESC";
				$res = mysqli_query($linkbd,$sql);
				while($row = mysqli_fetch_row($res)){
					$diferencia = $vardebito = $varcredito = '';
					$datos = array();
					$fechaorden = date('d/m/Y',strtotime($row[1]));
					$sqlcab = "SELECT estado FROM comprobante_cab WHERE tipo_comp = '15' AND numerotipo = '$row[0]'";
					$rescab = mysqli_query($linkbd,$sqlcab);
					$rowcab = mysqli_fetch_row($rescab);
					if($rowcab[0] == 0){$varestadocab = 'N';}
					else{$varestadocab = 'S';}
					$sqlcomp = "SELECT SUM(valdebito), SUM(valcredito) FROM comprobante_det WHERE tipo_comp = '15' AND numerotipo = '$row[0]' AND cuenta = '$row[2]'";
					$rescomp = mysqli_query($linkbd,$sqlcomp);
					$rowcomp = mysqli_fetch_row($rescomp);
					if($rowcomp[0] != ''){$vardebito = $rowcomp[0];}
					else{$vardebito = 0;}
					if($rowcomp[1] != ''){$varcredito = $rowcomp[1];}
					else{$varcredito = 0;}
					$diferencia = (float) $row[4] - ((float)$varcredito - (float)$vardebito);
					array_push($datos, $row[0]);//0 id traslado
					array_push($datos, $fechaorden);//1 fecha traslado
					array_push($datos, $row[2]);//2 cuanta banco
					array_push($datos, $row[3]);//3 cc
					array_push($datos, $row[4]);//4 valor
					array_push($datos, $row[5]);//5 concepto
					array_push($datos, $row[6]);//6 estado egreso
					array_push($datos, $row[2]);//7 cuenta contable
					array_push($datos, $varestadocab);//8 estado contabilidad
					array_push($datos, $vardebito);//9 debito
					array_push($datos, $varcredito);//10 credito
					array_push($datos, $diferencia);//11 diferencia
					array_push($datosproceso1, $datos);
				}
				$out['datosproceso1'] = $datosproceso1;
			}break;
		}
	}

	header("Content-type: application/json");
	echo json_encode($out);
	die();
?>