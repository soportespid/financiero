<?php 
    require '../comun.inc';
    $linkbd = conectar_v7();
    $linkbd->set_charset("utf8");

    $out = array('error' => false);

    $action = "show";

    if($_GET['action'] != ''){
        $action = $_GET['action'];
    }

    if($action == 'show'){

        $codigoBuscar = $_GET['codigo'];

        $numeroMax = array();

        $sqlr = 'SELECT nombre FROM conceptoscontables WHERE tipo=\'S1\' ';

        $res = mysqli_query($linkbd, $sqlr);
        
        while ($row = mysqli_fetch_row($res))
        {
            array_push($numeroMax, $row);
        }

        $out['numeroMax'] = $numeroMax;

        $sqlr = 'SELECT nombre FROM conceptoscontables WHERE modulo=3 AND codigo='.$codigoBuscar.' AND tipo=\'S1\'';
		
		$res = mysqli_query($linkbd, $sqlr);

        $row = mysqli_fetch_assoc($res);
        
        $nombreMostrar = '';

        $nombreMostrar = $row['nombre'];

        $out['nombreMostrar'] = $nombreMostrar;

        $tipo = $_GET['tipogf'];

        $conceptos = array();
		
		$sqlr = 'SELECT fechainicial, debito, credito, cc FROM conceptoscontables_det WHERE modulo=3 AND codigo='.$codigoBuscar.' AND tipo=\'S1\'';
		
		$res = mysqli_query($linkbd, $sqlr);

		$row = mysqli_fetch_assoc($res);

		$fecha = array();
		$tipoPago1 = array();
		$tipoPago2 = array();
		$centroCosto = array();

		$fec = explode("-",  $row['fechainicial']);
        $fechaExplode = $fec[2].'/'.$fec[1].'/'.$fec[0];

		array_push($fecha, $fechaExplode);
		array_push($tipoPago1, $row['debito']);
		array_push($tipoPago2, $row['credito']);
        array_push($centroCosto, $row['cc']);
        array_push($ids_det, $row['id_det']);

	
		$out['tipoPago1'] = $tipoPago1;
		$out['tipoPago2'] = $tipoPago2;
		$out['fecha'] = $fecha;
        $out['centroCosto'] = $centroCosto;


        $sqlrS = 'SELECT id_cc, nombre FROM centrocosto WHERE estado=\'S\'';

        $resS = mysqli_query($linkbd, $sqlrS);

        $centroCostos = array();

        while($rowS = mysqli_fetch_row($resS))
        {
            array_push($centroCostos, $rowS);
        }

        $destinoCompras = array();

        $sqlDestinoCompra = "SELECT codigo, nombre FROM almdestinocompra WHERE estado = 'S' ORDER BY codigo"; 
        $respDestinoCompra = mysqli_query($linkbd,$sqlDestinoCompra);
        while($rowDestinoCompra = mysqli_fetch_row($respDestinoCompra))
        {
            array_push($destinoCompras, $rowDestinoCompra);
        }

        $out['destinoCompras'] = $destinoCompras;
        
		$vigencia = date('Y');

        // $out['codigo'] = $codigo;
        $out['vigencia'] = $vigencia;
        $out['conceptos'] = $conceptos;
        $out['centroCostos'] = $centroCostos;
        
		
		
		$sqlr = 'SELECT fechainicial, cuenta, credito, id_det FROM conceptoscontables_det WHERE modulo=3 AND codigo='.$codigoBuscar.' AND tipo=\'S1\' AND credito=\'S\' ';
		
        $res = mysqli_query($linkbd, $sqlr);

		$datosFuncionamiento = array();
        $datosCredito = array();
        $cont = 0;
        
		while($row = mysqli_fetch_assoc($res))
        {
            $datosFuncionamiento[$cont][] = $row['fechainicial'];
			$datosFuncionamiento[$cont][]  = $row['cuenta'];

            $sql = 'SELECT nombre FROM cuentasnicsp WHERE cuenta='.$row['cuenta'].' ';
            
            $res2 = mysqli_query($linkbd, $sql);
            
            $row2 = mysqli_fetch_assoc($res2);

            $datosFuncionamiento[$cont][] = $row2['nombre'];
            
            $nataruleza = '';
            
			if($row['credito'] == 'S') $nataruleza = "CREDITO";
            
            $datosFuncionamiento[$cont][] = $nataruleza;
            
            $datosFuncionamiento[$cont][] = $row['id_det'];

            $cuentaAsociada[$cont][] = $row['cuenta'];
            $cuentaAsociada[$cont][] = $row2['nombre'];
			
			$cont += 1;
		}
		
		
        //var_dump($cuentas);
        $out['datosCredito'] = $datosFuncionamiento;
        $out['cuentaAsociada'] = $cuentaAsociada;
		

        $sqlr = 'SELECT fechainicial, cuenta, cc, debito, asociado_a, id_det, destino_compra FROM conceptoscontables_det WHERE modulo=3 AND codigo='.$codigoBuscar.' AND tipo=\'S1\' AND debito=\'S\' ';
		
        $res = mysqli_query($linkbd, $sqlr);

		$datosFuncionamiento2 = array();
        $cont = 0;
        
		while($row = mysqli_fetch_assoc($res))
        {
            $datosFuncionamiento2[$cont][] = $row['fechainicial'];
			$datosFuncionamiento2[$cont][]  = $row['cuenta'];

            $sql = 'SELECT nombre FROM cuentasnicsp WHERE cuenta='.$row['cuenta'].' ';
            
            $res2 = mysqli_query($linkbd, $sql);
            
            $row2 = mysqli_fetch_assoc($res2);

            $sqlr3 = 'SELECT nombre FROM cuentasnicsp WHERE cuenta = '.$row['asociado_a'].' ';
            $res3 = mysqli_query($linkbd, $sqlr3);
            $row3 = mysqli_fetch_assoc($res3);

            $datosFuncionamiento2[$cont][] = $row2['nombre'];

            $datosFuncionamiento2[$cont][] = $row['cc'];
            
            $nataruleza2 = '';
            
			if($row['debito'] == 'S') $nataruleza2 = "DEBITO";
            
            $datosFuncionamiento2[$cont][] = $nataruleza2;
            
            $datosFuncionamiento2[$cont][] = $row['asociado_a'];

            $datosFuncionamiento2[$cont][] = $row['destino_compra'];

            $datosFuncionamiento2[$cont][] = $row3['nombre'];

            $datosFuncionamiento2[$cont][] = $row['id_det'];
			
			$cont += 1;
		}
		
		
        //var_dump($cuentas);
        $out['datosDebito'] = $datosFuncionamiento2;

	}

    if($action == 'buscarCuentas'){

        $sqlr = 'SELECT * FROM (SELECT cn1.cuenta,cn1.nombre,cn1.naturaleza,cn1.centrocosto,cn1.tercero,cn1.tipo,cn1.estado FROM cuentasnicsp AS cn1 INNER JOIN cuentasnicsp AS cn2 ON cn2.tipo=\'Auxiliar\' AND cn2.naturaleza=\'CREDITO\' AND cn2.cuenta LIKE CONCAT( cn1.cuenta,  \'%\' ) WHERE cn1.tipo=\'Mayor\' AND cn1.naturaleza=\'CREDITO\' GROUP BY cn1.cuenta UNION SELECT cuenta,nombre,naturaleza,centrocosto,tercero,tipo,estado FROM cuentasnicsp WHERE tipo=\'Auxiliar\' AND naturaleza=\'CREDITO\') AS tabla ORDER BY 1';
        
        $res = mysqli_query($linkbd, $sqlr);

        $cuentas = array();
        while($row = mysqli_fetch_row($res))
        {
            array_push($cuentas, $row);
        }
        //var_dump($cuentas);
        $out['cuentas'] = $cuentas;
    }

    if($action == 'buscarCuentasDebito')
    {
        $destinoCompra = $_GET['destinoCompra'];

        $fec = $_GET['fecha'];

        preg_match("/([0-9]{2})\/([0-9]{2})\/([0-9]{4})/", $fec,$fech);

        $fecha = "$fech[3]-$fech[2]-$fech[1]";

        $cuentas = array();

        $sqlAlmacen = "SELECT cuenta_inicial, cuenta_final FROM `almdestinocompra_det` WHERE codigo = '$destinoCompra'";
        $resAlmacen = mysqli_query($linkbd,$sqlAlmacen);
        while ($rowAlmacen = mysqli_fetch_assoc($resAlmacen)) {

            $sqlr = "SELECT cuenta, nombre, naturaleza, centrocosto, tercero, tipo, estado FROM cuentasnicsp WHERE estado = 'S' AND (cuenta BETWEEN '$rowAlmacen[cuenta_inicial]' AND '$rowAlmacen[cuenta_final]') OR cuenta LIKE '$rowAlmacen[cuenta_final]%' ORDER BY cuenta ASC";
        
            $res = mysqli_query($linkbd, $sqlr);
            
            while($row = mysqli_fetch_row($res))
            {
                array_push($cuentas, $row);
            }
        }
        
        $out['cuentas'] = $cuentas;
    }

    if($action == 'filtrarCuentas'){

        $keywordCuenta = $_POST['keywordCuenta'];

        $sqlr = 'SELECT * FROM (SELECT cn1.cuenta,cn1.nombre,cn1.naturaleza,cn1.centrocosto,cn1.tercero,cn1.tipo,cn1.estado FROM cuentasnicsp AS cn1 INNER JOIN cuentasnicsp AS cn2 ON cn2.tipo=\'Auxiliar\'  AND cn2.cuenta LIKE CONCAT( cn1.cuenta,  \'%\' ) WHERE cn1.tipo=\'Mayor\' AND concat_ws(" ", cn1.cuenta, cn1.nombre) LIKE \'%'.$keywordCuenta.'%\' GROUP BY cn1.cuenta UNION SELECT cuenta,nombre,naturaleza,centrocosto,tercero,tipo,estado FROM cuentasnicsp WHERE tipo=\'Auxiliar\' AND concat_ws(" ", cuenta, nombre) LIKE \'%'.$keywordCuenta.'%\')  AS tabla ORDER BY 1';
        
        $res = mysqli_query($linkbd, $sqlr);

        $cuentas = array();
        while($row = mysqli_fetch_row($res))
        {
            array_push($cuentas, $row);
        }
        //var_dump($cuentas);
        $out['cuentas'] = $cuentas;
    }

    if($action == 'filtrarCuentasDebito'){

        $keywordCuenta = $_POST['keywordCuenta2'];

        $sqlr = 'SELECT * FROM (SELECT cn1.cuenta,cn1.nombre,cn1.naturaleza,cn1.centrocosto,cn1.tercero,cn1.tipo,cn1.estado FROM cuentasnicsp AS cn1 INNER JOIN cuentasnicsp AS cn2 ON cn2.tipo=\'Auxiliar\'  AND cn2.cuenta LIKE CONCAT( cn1.cuenta,  \'%\' ) WHERE cn1.tipo=\'Mayor\' AND concat_ws(" ", cn1.cuenta, cn1.nombre) LIKE \'%'.$keywordCuenta.'%\' GROUP BY cn1.cuenta UNION SELECT cuenta,nombre,naturaleza,centrocosto,tercero,tipo,estado FROM cuentasnicsp WHERE tipo=\'Auxiliar\' AND concat_ws(" ", cuenta, nombre) LIKE \'%'.$keywordCuenta.'%\')  AS tabla ORDER BY 1';
        
        $res = mysqli_query($linkbd, $sqlr);

        $cuentas = array();
        while($row = mysqli_fetch_row($res))
        {
            array_push($cuentas, $row);
        }
        //var_dump($cuentas);
        $out['cuentas'] = $cuentas;
    }


    if($action == "buscaCta"){

        $cuenta = $_GET["cuenta"];

        $sqlr = "SELECT nombre, naturaleza FROM cuentasnicsp WHERE cuenta = $cuenta AND naturaleza = 'CREDITO' ";

        $res = mysqli_query($linkbd, $sqlr);

        $row = mysqli_fetch_row($res);

        $out['nombreCuenta'] = $row[0];
        $out['naturalezaCuenta'] = $row[1];

    }

    if($action == "buscaCta2"){

        $fec = $_GET['fecha'];

        preg_match("/([0-9]{2})\/([0-9]{2})\/([0-9]{4})/", $fec,$fech);

		$fecha = "$fech[3]-$fech[2]-$fech[1]";

        $destinoCompra = $_GET['destinoCompra'];
    
        $sqlAlmacen = "SELECT cuenta_inicial, cuenta_final FROM `almdestinocompra_det` WHERE codigo = '$destinoCompra'";
        $resAlmacen = mysqli_query($linkbd,$sqlAlmacen);
        $rowAlmacen = mysqli_fetch_assoc($resAlmacen);

        $cuenta = $_GET["cuenta"];

        $sqlr = "SELECT nombre, naturaleza FROM cuentasnicsp WHERE cuenta BETWEEN '$rowAlmacen[cuenta_inicial]' AND '$rowAlmacen[cuenta_final]' AND cuenta = $cuenta AND naturaleza = 'DEBITO' ";
        $res = mysqli_query($linkbd, $sqlr);
        $row = mysqli_fetch_row($res);

        $out['nombreCuenta'] = $row[0];
        $out['naturalezaCuenta'] = $row[1];

    }

    if($action == "guardar"){

        if(isset($_POST['nombre']))
        {
            $sqlr = 'UPDATE conceptoscontables SET nombre=\''.$_POST['nombre'].'\' WHERE codigo=\''.$_POST['codigo'].'\' AND tipo=\'S1\' ';
            
            mysqli_query($linkbd, $sqlr);

            $out['codigo'] = [];

            array_push($out['codigo'], 'detalleEliminar');  
        
            if(isset($_POST['detalleEliminar']))
            {

                array_push($out['codigo'], $_POST['detalleEliminar']);

                for($x = 0; $x < count($_POST['detalleEliminar']); $x++){

                    $detalleEliminar = explode(",", $_POST['detalleEliminar'][$x]);

                    $cred = 'S';
                    $deb = 'N';
                    $fecha = '';

                    $fec = explode("/", $detalleEliminar[0]);

                    $fecha = $fec[2].'-'.$fec[1].'-'.$fec[0];

                    $sql = 'DELETE FROM conceptoscontables_det WHERE id_det = \''.$detalleEliminar[4].'\'';
                    
                    $out['insertaBien'] = mysqli_query($linkbd, $sql);
                }
            }

            $out['codigo'] = [];

            array_push($out['codigo'], 'detalleEliminar2');  
        
            if(isset($_POST['detalleEliminar2']))
            {

                array_push($out['codigo'], $_POST['detalleEliminar2']);

                for($x = 0; $x < count($_POST['detalleEliminar2']); $x++){

                    $detalleEliminar2 = explode(",", $_POST['detalleEliminar2'][$x]);

                    $cred = '';
                    $deb = '';
                    $fecha = '';

                    if($detalleEliminar2[4] == 'DEBITO'){
                        $deb = 'S';
                        $cred = 'N';
                    }

                    $fec = explode("/", $detalleEliminar2[0]);

                    $fecha = $fec[2].'-'.$fec[1].'-'.$fec[0];

                    $sql = 'DELETE FROM conceptoscontables_det WHERE id_det = \''.$detalleEliminar2[8].'\'';
                    
                    $out['insertaBien'] = mysqli_query($linkbd, $sql);
                }
            }

            array_push($out['codigo'], 'detallesAgr');    

            if(isset($_POST['detallesAgr']))
            {

                array_push($out['codigo'], $_POST['detallesAgr']);

                for($x = 0; $x < count($_POST['detallesAgr']); $x++){

                    $conceptos_det = explode(",", $_POST['detallesAgr'][$x]);

                    $cred = 'S';
                    $deb = 'N';
                    $fecha = '';

                    $fec = explode("/", $conceptos_det[0]);

                    $fecha = $fec[2].'-'.$fec[1].'-'.$fec[0];

                    $sql = 'INSERT INTO conceptoscontables_det(codigo, tipo, tipocuenta, cuenta, debito, credito, estado, modulo, fechainicial) VALUES ("'.$_POST['codigo'].'", "'.$_POST['tipo'].'", \'N\', "'.$conceptos_det[1].'", "'.$deb.'", "'.$cred.'", \'S\', \'3\', "'.$fecha.'")';
                    
                    mysqli_query($linkbd, $sql);

                    

                }
            }

            if(isset($_POST['detallesAgr2']))
            {

                array_push($out['codigo2'], $_POST['detallesAgr2']);

                for($x = 0; $x < count($_POST['detallesAgr2']); $x++){

                    $conceptos_det = explode(",", $_POST['detallesAgr2'][$x]);

                    $cred = '';
                    $deb = '';
                    $fecha = '';

                    if($conceptos_det[4] == 'DEBITO'){
                        $deb = 'S';
                        $cred = 'N';
                    }

                    $fec = explode("/", $conceptos_det[0]);

                    $fecha = $fec[2].'-'.$fec[1].'-'.$fec[0];

                    $sql = 'INSERT INTO conceptoscontables_det(codigo, tipo, tipocuenta, cuenta, cc, debito, credito, estado, modulo, fechainicial, asociado_a, destino_compra) VALUES ("'.$_POST['codigo'].'", "'.$_POST['tipo'].'", \'N\', "'.$conceptos_det[1].'", "'.$conceptos_det[3].'", "'.$deb.'", "'.$cred.'", \'S\', \'3\', "'.$fecha.'", "'.$conceptos_det[5].'","'.$conceptos_det[6].'")';
                    
                    mysqli_query($linkbd, $sql);

                    

                }
            }

        }
        
        $out['insertaBien'] = true;
    }

    header("Content-type: application/json");
    echo json_encode($out);
    die();
