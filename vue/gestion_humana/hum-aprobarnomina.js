var app = new Vue({ 
	el: '#myapp',
	data:{
        loading: false,
        error:'',
		idcomp:'',
        idliq:'',
		idcdp:'',
		idrp:'',
		desrp:'',
		vtercero:'',
		vntercero:'',
		infotercero:'',
		bloqueo:'',
		errorlt:'',
		idbuscar:'',
		valnomina:0,
		valcdp:0,
		valrp:0,
		fecha:'',
		fechagt1:'',
		fechagt2:'',
		scrtop:'',
		idr:'',
		idmayor:'',
		idmenor:'',
		showModalTerceros: false,
        infonomina:[],
		infoterceros:[],
		infoaprobada:[],
		infover:[],
		searchTercero: {keywordTercero: ''},

	},
	mounted:
	function(){
		this.loading = true;
		switch (this.idbuscar){
			case 1:{
				this.carga_id();
				this.carga_nominas();
			}break;
			case 2:{
				this.carga_ver();
			}break;
			case 3:{
				this.loading = false;
				
				if(this.fechagt1 != '' && this.fechagt2 != ''){
					document.getElementById('fc_1198971545').value = this.fechagt1;
					document.getElementById('fc_1198971546').value = this.fechagt2;
					this.iniciabuscar();
				}
				
			}break;
		}
	},
	methods:{
		carga_id: async function(){
			let date = new Date();
			document.getElementById('fc_1198971545').value = String(date.getDate()).padStart(2, '0') + '/' + String(date.getMonth() + 1).padStart(2, '0') + '/' + date.getFullYear();
            await axios.post('vue/gestion_humana/hum-aprobarnomina.php?accion=id_inicial')
			.then(
				(response)=>{
					this.idcomp = response.data.inicialaprobado;
				}
			).catch((error) => {
				this.error = true;
			});
            this.loading = false;
        },
		carga_nominas: async function(){
			await axios.post('vue/gestion_humana/hum-aprobarnomina.php?accion=nominas')
			.then(
				(response)=>{
					this.infonomina = response.data.datosnomina;
				}
			).catch((error) => {
				this.error = true;
			});
		},
		cargainfo: function(){
			this.idcdp = this.infonomina[this.idliq][1];
			this.idrp = this.infonomina[this.idliq][2];
			this.desrp = this.infonomina[this.idliq][4];
			this.valrp = Intl.NumberFormat("en-US", {style: "currency", currency: "USD",minimumFractionDigits: 0, maximumFractionDigits: 0,}).format(this.infonomina[this.idliq][5]);
			this.valcdp = Intl.NumberFormat("en-US", {style: "currency", currency: "USD",minimumFractionDigits: 0, maximumFractionDigits: 0,}).format(this.infonomina[this.idliq][6]);
			this.valnomina = Intl.NumberFormat("en-US", {style: "currency", currency: "USD",minimumFractionDigits: 0, maximumFractionDigits: 0,}).format(this.infonomina[this.idliq][7]);
		},
		toggleModalTerceros: function(){
			this.showModalTerceros = !this.showModalTerceros;
			if(this.showModalTerceros == true){
				this.buscarTerceros();
			}
		},
		buscarTerceros: async function(){
			this.loading = true;
			var keywordTercero = this.toFormData(this.searchTercero);
			await axios.post('vue/tesoreria/teso-mediosdepagonew.php?accion=sel2', keywordTercero)
			.then(
				(response)=>{
					this.infoterceros = response.data.infoterceros;
				}
			);
			this.loading = false;
		},
		ingresarTercero: function(razonsocial, apellido1, apellido2, nombre1, nombre2, documento){			
			this.vtercero = documento;
			if(razonsocial != ''){
				this.vntercero = razonsocial;
			}else{
				this.vntercero = apellido1 + ' ' + apellido2 + ' ' + nombre1 + ' ' + nombre2;
			}
			this.showModalTerceros = false;
		},
		verificatercero: async function(){
			this.loading = true;
			var keyword = this.vtercero;
			await axios.post('vue/tesoreria/teso-mediosdepagonew.php?accion=sel4&keywordTercero='+keyword)
			.then(
				(response)=>{
					if (response.data.infoterceros.length > 0){
						if(response.data.infoterceros[0][0] != ''){
							this.vntercero = response.data.infoterceros[0][0];
						}else{
							this.vntercero = response.data.infoterceros[0][1] + ' ' + response.data.infoterceros[0][2] + ' ' + response.data.infoterceros[0][3] + ' ' + response.data.infoterceros[0][4];
						}
					}else{
						this.vntercero='';
						Swal.fire({
							icon: 'error',
							title: 'Error!',
							text: 'No exite un tercero con el numero de documento: '+ keyword,
							confirmButtonText: 'Continuar',
							confirmButtonColor: '#FF121A',
							timer: 3500
						});
					}
				}
			);
			this.loading = false;
		},
		validabloqueo: async function(fecha1){
			let formData = new FormData();
			formData.append('fecha1', fecha1);
			await axios.post('vue/gestion_humana/hum-aprobarnomina.php?accion=valbloqueo', formData)
			.then(
				(response)=>{
					this.bloqueo = response.data.infobloqueo;
				}
			);
		},
		guardarinformacion: async function(fecha1){
			let formData = new FormData();
			formData.append('fecha1', fecha1);
			formData.append('idcomp', this.idcomp);
			formData.append('idcdp', this.idcdp);
			formData.append('idrp', this.idrp);
			formData.append('idliq', this.infonomina[this.idliq][0]);
			formData.append('vtercero', this.vtercero);
			formData.append('vntercero', this.vntercero);
			await axios.post('vue/gestion_humana/hum-aprobarnomina.php?accion=guardar', formData)
			.then(
				(response)=>{
					this.errorlt = response.data.errorlt;
				}
			);
			this.loading = false;
			if(this.errorlt == 0){
				Swal.fire({
					icon: 'success',
					title: 'Se Aprobo con exito la nomina: '+ this.infonomina[this.idliq][0],
					showConfirmButton: true,
					confirmButtonText: 'Continuar',
					confirmButtonColor: '#01CC42',
					timer: 3500
				}).then((response) => {
					this.moveravisualizar(this.idcomp);
				});
			}else{
				Swal.fire({
					icon: 'error',
					title: 'Error!',
					text: 'No se pudo guardar la aprobación de nómina', 
					confirmButtonText: 'Continuar',
					confirmButtonColor: '#FF121A',
					timer: 3500
				});
			}
		},
		validarguardar: async function(){
			let fecha1 = document.getElementById('fc_1198971545').value;
			if(fecha1 !=''){
				await this.validabloqueo(fecha1);
				if(this.bloqueo >= 1 || this.bloqueo == -1){
					if(this.idcomp != '' ){
						if(this.idrp != 0){
							if(this.vntercero != '' ){
								Swal.fire({
									icon: 'question',
									title: '¿Seguro que quiere aprobar esta nómina?',
									showDenyButton: true,
									confirmButtonText: 'Guardar',
									confirmButtonColor: '#01CC42',
									denyButtonText: 'Cancelar',
									denyButtonColor: '#FF121A',
								}).then(
									(result) => {
										if (result.isConfirmed){
											this.loading = true;
											this.guardarinformacion(fecha1);
										}
										else if (result.isDenied){
											Swal.fire({
												icon: 'info',
												title: 'No se aprobo la nóminan',
												confirmButtonText: 'Continuar',
												confirmButtonColor: '#FF121A',
												timer: 2500
											});
										}
									}
								)
							}else{
								Swal.fire({
									icon: 'error',
									title: 'Error!',
									text: 'Se debe seleccionar un tercero', 
									confirmButtonText: 'Continuar',
									confirmButtonColor: '#FF121A',
									timer: 3500
								});
							}
						}else{
							Swal.fire({
								icon: 'error',
								title: 'Error!',
								text: 'No se ha generado un RP a la nómina para aprobar', 
								confirmButtonText: 'Continuar',
								confirmButtonColor: '#FF121A',
								timer: 3500
							});
						}
					}else{
						Swal.fire({
							icon: 'error',
							title: 'Error!',
							text: 'No se ha generado un codigo de aprobación, volver a iniciar', 
							confirmButtonText: 'Continuar',
							confirmButtonColor: '#FF121A',
							timer: 3500
						});
					}
				}
				/*else if(this.bloqueo == -1){
					Swal.fire({
						icon: 'error',
						title: 'Error!',
						text: 'ya existe una nomina aprobada con una fecha mayor a la seleccionada', 
						confirmButtonText: 'Continuar',
						confirmButtonColor: '#FF121A',
						timer: 3500
					});
				}*/
				else{
					Swal.fire({
						icon: 'error',
						title: 'Error!',
						text: 'No Tiene los Permisos para aprobar la nómina en la fecha seleccionada', 
						confirmButtonText: 'Continuar',
						confirmButtonColor: '#FF121A',
						timer: 3500
					});
				}
			}else{
				Swal.fire({
					icon: 'error',
					title: 'Error!',
					text: 'Se debe ingresar una fecha de aprobación', 
					confirmButtonText: 'Continuar',
					confirmButtonColor: '#FF121A',
					timer: 3500
				});
			}
			
		},
		vernomina: function(){
			if(this.valnomina != 0){
				let nomina;
				if(this.idbuscar == 1){
					nomina = this.infonomina[this.idliq][0];
				}else{
					nomina = this.infover[0];
				}
				mypop=window.open('hum-liquidarnominamirar.php?idnomi=' + nomina,'','');mypop.focus();
			}else{
				Swal.fire({
					icon: 'error',
					title: 'Error!',
					text: 'Se debe seleccionar una nómina', 
					confirmButtonText: 'Continuar',
					confirmButtonColor: '#FF121A',
					timer: 3500
				});
			}
			
		},
		vercdp: function(){
			if(this.valcdp != 0){
				let idcdp = this.idcdp;
				let vigencia;
				if(this.idbuscar == 1){
					vigencia = this.infonomina[this.idliq][8];
				}else{
					vigencia = this.infover[12];
				}
				mypop=window.open('ccp-cdpVisualizar.php?is=' + idcdp + '&vig='+ vigencia,'','');mypop.focus();
			}else{
				Swal.fire({
					icon: 'error',
					title: 'Error!',
					text: 'No se ha vinculado un CDP',
					confirmButtonText: 'Continuar',
					confirmButtonColor: '#FF121A',
					timer: 3500
				});
			}
			
		},
		verrp: function(){
			if(this.valrp != 0){
				let idrp = this.idrp;
				let vigencia;
				if(this.idbuscar == 1){
					vigencia = this.infonomina[this.idliq][8];
				}else{
					vigencia = this.infover[12];
				}
				mypop=window.open('ccp-rpVisualizar.php?is=' + idrp + '&vig='+ vigencia,'','');mypop.focus();
			}else{
				Swal.fire({
					icon: 'error',
					title: 'Error!',
					text: 'No se ha vinculado un RP',
					confirmButtonText: 'Continuar',
					confirmButtonColor: '#FF121A',
					timer: 3500
				});
			}
			
		},
		moveravisualizar: function(codigo){
			let datos;
			if(this.idbuscar != 1){
				let scrtop = $('#divdet').scrollTop();
				let fechaini = document.getElementById('fc_1198971545').value;
				let fechafin = document.getElementById('fc_1198971546').value;
				datos = "idr=" + codigo + "&scrtop=" + scrtop + "&fechaini=" + fechaini + "&fechafin=" + fechafin;	
			}else{
				datos = "idr=" + codigo;
			}
			location.href="hum-aprobarnominaver.php?" + datos;
		},
		carga_ver: async function(){
			let formData = new FormData();
			formData.append('idcomp', this.idcomp);
			await axios.post('vue/gestion_humana/hum-aprobarnomina.php?accion=carga_ver', formData)
			.then(
				(response)=>{
					this.fecha = response.data.datosnomina[1];
					this.idliq = response.data.datosnomina[0] + ' - ' +  response.data.datosnomina[8];
					this.idcdp = response.data.datosnomina[7];
					this.idrp = response.data.datosnomina[2];
					this.desrp = response.data.datosnomina[6];
					this.valrp = Intl.NumberFormat("en-US", {style: "currency", currency: "USD",minimumFractionDigits: 0, maximumFractionDigits: 0,}).format(response.data.datosnomina[9]);
					this.valnomina = Intl.NumberFormat("en-US", {style: "currency", currency: "USD",minimumFractionDigits: 0, maximumFractionDigits: 0,}).format(response.data.datosnomina[10]);
					this.valcdp = Intl.NumberFormat("en-US", {style: "currency", currency: "USD",minimumFractionDigits: 0, maximumFractionDigits: 0,}).format(response.data.datosnomina[11]);
					this.vtercero = response.data.datosnomina[13];
					this.vntercero = response.data.datosnomina[14];
					this.idmenor = response.data.datosnomina[15];
					this.idmayor = response.data.datosnomina[16];
					this.infover = response.data.datosnomina;
				}
			).catch((error) => {
				this.error = true;
			});
            this.loading = false;
		},
		carga_aprobadas: async function(fechaini,fechafin){
			let formData = new FormData();
			if(this.scrtop == ""){
				this.scrtop = 0;
			}
			formData.append('fechaini', fechaini);
			formData.append('fechafin', fechafin);
			formData.append('scrtop',this.scrtop);
			formData.append('idr',this.idr);
			await axios.post('vue/gestion_humana/hum-aprobarnomina.php?accion=carga_buscar', formData)
			.then(
				(response)=>{
					if (response.data.infoaprobada.length > 0){
						this.infoaprobada = response.data.infoaprobada;
						
						//this.tprueba.scrollTop = this.scrtop;
						//window.onload =  function(){ $('#divdet').scrollTop(this.scrtop);}
					}
				}
			);
			this.loading = false;
		},
		fundeshace: async function(numapro,numnomi){
			this.loading = true;
			let formData = new FormData();
			formData.append('idcomp', numapro);
			formData.append('idnomi', numnomi);
			await axios.post('vue/gestion_humana/hum-aprobarnomina.php?accion=deshacer', formData)
			.then(
				(response)=>{
					this.errorlt = response.data.errorlt;
				}
			);
			if(this.errorlt == 0){
				Swal.fire({
					icon: 'success',
					title: 'Se deshizo con exito la aprobación de nómina',
					showConfirmButton: true,
					confirmButtonText: 'Continuar',
					confirmButtonColor: '#01CC42',
					timer: 3500
				});
				this.loading = true;
				let fechaini = document.getElementById('fc_1198971545').value;
				let fechafin = document.getElementById('fc_1198971546').value;
				this.carga_aprobadas(fechaini,fechafin);
			}else{
				
			}
			this.loading = false;
		},
		validardeshacer: async function(numapro,numnomi,fecha1){
			if(numapro != 'NO'){
				await this.validabloqueo(fecha1);
				if(this.bloqueo >= 1){
					Swal.fire({
						icon: 'question',
						title: '¿Seguro que quiere deshacer la aprobación de nómina?',
						showDenyButton: true,
						confirmButtonText: 'Deshacer',
						confirmButtonColor: '#01CC42',
						denyButtonText: 'Cancelar',
						denyButtonColor: '#FF121A',
					}).then(
						(result) => {
							if (result.isConfirmed){
								this.fundeshace(numapro,numnomi);
							}
						}
					)
				}else{
					Swal.fire({
						icon: 'error',
						title: 'Error!',
						text: 'No Tiene los Permisos para aprobar la nómina en la fecha seleccionada', 
						confirmButtonText: 'Continuar',
						confirmButtonColor: '#FF121A',
						timer: 3500
					});
				}
			}else{
				Swal.fire({
					icon: 'error',
					title: 'Error!',
					text: 'No se puede deshacer, ya se generaron egresos',
					confirmButtonText: 'Continuar',
					confirmButtonColor: '#FF121A',
					timer: 3500
				});
			}
		},
		iniciabuscar: async function(){
			let fechaini = document.getElementById('fc_1198971545').value;
			let fechafin = document.getElementById('fc_1198971546').value;
			if(fechaini != '' && fechafin != ''){
				this.loading = true;
				this.carga_aprobadas(fechaini,fechafin);
			}else{
				Swal.fire({
					icon: 'error',
					title: 'Error!',
					text: 'Se debe ingresar fecha inicial y fecha final para buscar', 
					confirmButtonText: 'Continuar',
					confirmButtonColor: '#FF121A',
					timer: 3500
				});
			}
		},
		retornabuscar: function(){
			let datos = "idr=" + this.idcomp + "&scrtop=" + this.scrtop + "&fechaini=" + this.fechagt1 + "&fechafin=" + this.fechagt2;
			location.href='hum-aprobarnominabuscar.php?' + datos;
		},
		atrasc: function(){
			let idres = this.idcomp - 1;
			if(idres >= this.idmenor){
				let scrtop = this.scrtop;
				let fechaini = this.fechagt1;
				let fechafin = this.fechagt2;
				let datos = "idr=" + idres + "&scrtop=" + scrtop + "&fechaini=" + fechaini + "&fechafin=" + fechafin;
				location.href="hum-aprobarnominaver.php?" + datos;
			}
		},
		adelante: function(){
			let idmax = this.idcomp + 1;
			if(idmax <= this.idmayor){
				let scrtop = this.scrtop;
				let fechaini = this.fechagt1;
				let fechafin = this.fechagt2;
				let datos = "idr=" + idmax + "&scrtop=" + scrtop + "&fechaini=" + fechaini + "&fechafin=" + fechafin;
				location.href="hum-aprobarnominaver.php?" + datos;
			}
		},
		toFormData: function(obj)
		{
			var form_data = new FormData();
			for(var key in obj)
			{
				form_data.append(key, obj[key]);
			}
			return form_data;
		},
	},
});