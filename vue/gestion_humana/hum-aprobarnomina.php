<?php
	require_once '../../comun.inc';
	require '../../funciones.inc';
	require '../../funcionesnomima.inc';

	$linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");
	session_start();

	if(isset($_GET['accion'])){
		$accion = $_GET['accion'];
		switch ($accion){
			case 'id_inicial':{
				$sql = "SELECT MAX(id_aprob) FROM humnomina_aprobado";
				$res = mysqli_query($linkbd,$sql);
				$row = mysqli_fetch_row($res);
				$id_aprobado = (INT)$row[0] + 1;
				$out['inicialaprobado'] = $id_aprobado;
			}break;
			case 'nominas':{
				$datosnomina = array();
				$sql = "SELECT T1.id_nom, T2.cdp, T2.rp, T2.vigencia FROM humnomina AS T1 INNER JOIN hum_nom_cdp_rp AS T2 ON T2.nomina = T1.id_nom WHERE T1.estado = 'S' AND T2.estado = 'S' ORDER BY T1.id_nom DESC";
				$res = mysqli_query($linkbd,$sql);
				while($row = mysqli_fetch_row($res)){
					$sqldes = "SELECT T1.descripcion FROM hum_novedadespagos_cab AS T1 INNER JOIN hum_prenomina AS T2 ON T2.codigo = T1.prenomina WHERE T2.num_liq = '$row[0]'";
					$resdes = mysqli_query($linkbd,$sqldes);
					$rowdes = mysqli_fetch_row($resdes);
					if($row[2] != 0){
						$sqlrp = "SELECT detalle,valor FROM ccpetrp WHERE vigencia = '$row[3]' AND consvigencia = '$row[2]' AND idcdp = '$row[1]'";
						$resrp = mysqli_query($linkbd,$sqlrp);
						$rowrp = mysqli_fetch_row($resrp);
						$desrp = $rowrp[0];
						$valrp = $rowrp[1];
						$sqlcdp = "SELECT valor FROM ccpetcdp WHERE vigencia = '$row[3]' AND consvigencia = '$row[1]'";
						$rescdp = mysqli_query($linkbd,$sqlcdp);
						$rowcdp = mysqli_fetch_row($rescdp);
						$valcdp = $rowcdp[0];
						$sqlnom = "SELECT SUM(valor) FROM humnom_presupuestal WHERE id_nom = '$row[0]'";
						$resnom = mysqli_query($linkbd,$sqlnom);
						$rownom = mysqli_fetch_row($resnom);
						$valnom = $rownom[0];
					}else{
						$desrp = 'No posee un RP asignado';
						$valrp = 0;
						$valcdp = 0;
						$valnom = 0;
					}
					$vigenc = $row[3];
					$datos = array();
					array_push($datos, $row[0]);//id nomina
					array_push($datos, $row[1]);//id cdp
					array_push($datos, $row[2]);//id rp
					array_push($datos, $rowdes[0]);// descripción nomina
					array_push($datos, $desrp);// descripción rp
					array_push($datos, $valrp);//valor rp
					array_push($datos, $valcdp);//valor cdp
					array_push($datos, $valnom);//valor nomina
					array_push($datos, $vigenc);//vigencia
					array_push($datosnomina, $datos);
				}
				$out['datosnomina'] = $datosnomina;
			}break;
			case 'valbloqueo':{
				preg_match("/([0-9]{2})\/([0-9]{2})\/([0-9]{4})/", $_POST['fecha1'],$fecha);
				$fechaf = "$fecha[3]-$fecha[2]-$fecha[1]";
				$fechamax = "$fecha[3]$fecha[2]$fecha[1]";
				$sqlblfecha = "SELECT fecha FROM humnomina_aprobado WHERE tipo_mov = '201' AND estado = 'S' ORDER BY id_aprob DESC LIMIT 1";
				$resblfecha = mysqli_query($linkbd,$sqlblfecha);
				$rowblfecha = mysqli_fetch_row($resblfecha);
				$fechamin = date('Ymd',strtotime($rowblfecha[0]));
				if ((int)$fechamax >= (int)$fechamin){
					$bloq = bloqueos($_SESSION['cedulausu'],$fechaf);
				}else{
					$bloq = -1;
				}
				$out['infobloqueo'] = $bloq;
			}break;
			case 'guardar':{
				$errorlt = 0;
				preg_match("/([0-9]{2})\/([0-9]{2})\/([0-9]{4})/", $_POST['fecha1'],$fecha);
				$fechaf = "$fecha[3]-$fecha[2]-$fecha[1]";
				$sqlnom = "SELECT mes, vigencia FROM humnomina WHERE id_nom ='".$_POST['idliq']."'";
				$resnom = mysqli_query($linkbd,$sqlnom);
				$rownom = mysqli_fetch_row($resnom);
				$vigenomina = $rownom[1];
				$mesnnomina = $rownom[0];
				$meslnomina = mesletras($mesnnomina);
				$id = $_POST['idliq'];
				$idcomp = $_POST['idcomp'];
				$idcdp = $_POST['idcdp'];
				$idrp = $_POST['idrp'];
				$vtercero = $_POST['vtercero'];
				$vntercero = $_POST['vntercero'];
				$sqlr="INSERT INTO humnomina_aprobado (id_aprob, id_nom, fecha, id_rp, persoaprobo, estado, tipo_mov, id_cdp, tercero, ntercero, vigencia) VALUES ('$idcomp', '$id', '$fechaf', '$idrp', '".$_SESSION['usuario']."', 'S', '201', '$idcdp', '$vtercero', '$vntercero', '$vigenomina')";
				if (!mysqli_query($linkbd,$sqlr)){
					$errorlt = 1;
				}else{
					$sqlr="UPDATE humnomina SET estado = 'P' WHERE id_nom = '$id'"; 
					mysqli_query($linkbd,$sqlr);
					$sqlr="UPDATE humnom_presupuestal SET estado = 'P' WHERE id_nom = '$id'";
					mysqli_query($linkbd,$sqlr);
					$sqlr="UPDATE humnominaretenemp SET estado = 'A' WHERE id_nom = '$id'";
					mysqli_query($linkbd,$sqlr);
					$sqlr="SELECT cajas,icbf,sena,esap,iti FROM admfiscales WHERE vigencia =  (SELECT MAX(vigencia) FROM admfiscales)";
					$resp = mysqli_query($linkbd,$sqlr);
					while ($row = mysqli_fetch_row($resp))
					{
						$cajacomp = $row[0];
						$icbf = $row[1];
						$sena = $row[2];
						$esap = $row[3];
						$iti = $row[4];
					}
					$sqlr="SELECT cajacompensacion, icbf, sena, iti, esap, arp, salud_empleador, salud_empleado, pension_empleador, pension_empleado FROM humparametrosliquida";
					$resp = mysqli_query($linkbd,$sqlr);
					while ($row = mysqli_fetch_row($resp)){
						$pcajacomp = $row[0];
						$picbf = $row[1];
						$psena = $row[2];
						$piti = $row[3];
						$pesap = $row[4];
						$parp = $row[5];
						$psalud_empleador = $row[6];
						$psalud_empleado = $row[7];
						$ppension_empleador = $row[8];
						$ppension_empleado = $pbfsol = $row[9];
					}
					$listacuentas = array();
					$listanombrecuentas = array();
					$listaterceros = array();	
					$listaterceros = array();	
					$listaterceros = array();	
					$listanombreterceros = array();
					$listaccs = array();
					$listadetalles = array();
					$listadebitos = array();
					$listacreditos = array();
					$listacajacf[] = array();
					$listasena[] = array();
					$listaicbf[] = array();
					$listainstecnicos[] = array();
					$listaesap[] = array();
					$listatipo[] = array();
					$sqlr="SELECT cedulanit, SUM(devendias), SUM(auxalim), SUM(auxtran), SUM(salud), SUM(saludemp), SUM(pension), SUM(pensionemp), SUM(fondosolid), SUM(otrasdeduc), SUM(arp), SUM(cajacf), SUM(sena), SUM(icbf), SUM(instecnicos), SUM(esap), tipofondopension, prima_navi, cc, tipopago, SUM(retefte), idfuncionario FROM humnomina_det WHERE id_nom = '$id' GROUP BY idfuncionario, tipopago ORDER BY tipopago, cc"; 
					$resp = mysqli_query($linkbd,$sqlr);
					while ($row = mysqli_fetch_row($resp)){
						$ccosto = $row[18];
						$empleado = buscatercero($row[0]);
						//tipo de pago (Salarios, Subsidios, primas .....)
						if($row[1] != 0){
							$ctaconcepto = $ctacont = '';
							//Cuenta debito salario empleado
							$ctacont = cuentascontablesccpet::cuentadebito_tipomovccpet($row[19], $row[21], $row[18], $fechaf, 0, '', $id, $row[19]);
							$nresul = buscacuenta($ctacont);
							$nomceunta = nombrevariblespagonomina($row[19]);
							$listacuentas[] = $ctacont;
							$listanombrecuentas[] = $nresul;
							$listaterceros[] = $row[0];
							$listanombreterceros[] = $empleado;
							$listaccs[] = $row[18];
							$listadetalles[] = "$nomceunta Mes $meslnomina";
							$listadebitos[] = $row[1];
							$listacreditos[] = 0;
							$listatipo[] = "$row[19]<->DB";
							//Cuenta credito salario empleado
							$ctaconcepto = cuentascontablesccpet::cuentacredito_tipomovccpet($row[19], $row[21], $row[18], $fechaf, 0, '', $id, $row[19]);
							$nresul = buscacuenta($ctaconcepto);
							$listacuentas[] = $ctaconcepto;
							$listanombrecuentas[] = $nresul;
							$listaterceros[] = $row[0];
							$listanombreterceros[] = $empleado;
							$listaccs[] = $row[18];
							$listadetalles[] = "$nomceunta Mes $meslnomina";
							$listadebitos[] = 0;
							$listacreditos[] = $row[1];
							$listatipo[] = "$row[19]<->CR";
						}
						//Salud Empleado
						if($row[4] != 0){
							$ctaconcepto = $ctacont = '';
							//Cuenta debito salud empleado
							$ctacont = cuentascontablesccpet::cuentacredito_tipomovccpet($row[19], $row[21], $row[18], $fechaf, 0, '', $id, $row[19]);
							$nresul = buscacuenta($ctacont);
							$listacuentas[] = $ctacont;
							$listanombrecuentas[] = $nresul;
							$listaterceros[] = $row[0];
							$listanombreterceros[] = $empleado;
							$listaccs[] = $row[18];
							$listadetalles[] = "Aporte Salud Empleado Mes $meslnomina Debito";
							$listadebitos[] = $row[4];
							$listacreditos[] = 0;
							$listatipo[] = "SE<->DB";
							//Cuenta credito salud empleado
							$ctaconcepto = cuentascontablesccpet::cuentacredito_tipomovccpet($psalud_empleado, $row[21], $row[18], $fechaf, 1, 'N/A', $id, $row[19]);
							$nresul = buscacuenta($ctaconcepto);
							$epsnit = itemfuncionarios($row[21],'14');
							$epsnom = itemfuncionarios($row[21],'15');
							$listacuentas[] = $ctaconcepto;
							$listanombrecuentas[] = $nresul;
							$listaterceros[] = $epsnit;
							$listanombreterceros[] = $epsnom;
							$listaccs[] = $row[18];
							$listadetalles[] = "Aporte Salud Empleado Mes $meslnomina Credito";
							$listadebitos[] = 0;
							$listacreditos[] = $row[4];
							$listatipo[] = "SE<->CR";
						}
						//Pension Empleado
						if($row[6] != 0){
							$ctaconcepto = $ctacont = '';
							//Cuenta debito pension empleado
							$ctacont = cuentascontablesccpet::cuentacredito_tipomovccpet($row[19], $row[21], $row[18], $fechaf, 0, '', $id, $row[19]);
							$nresul = buscacuenta($ctacont);
							$listacuentas[] = $ctacont;
							$listanombrecuentas[] = $nresul;
							$listaterceros[] = $row[0];
							$listanombreterceros[] = $empleado;
							$listaccs[] = $row[18];
							$listadetalles[] = "Aporte pensión empleado mes $meslnomina";
							$listadebitos[] = $row[6];
							$listacreditos[] = 0;
							$listatipo[] = "PE<->DB";
							//Cuenta credito pension empleado
							$ctaconcepto = cuentascontablesccpet::cuentacredito_tipomovccpet($ppension_empleado, $row[21], $row[18], $fechaf, 1,'N/A', $id, $row[19]);
							$nresul = buscacuenta($ctaconcepto);
							$epsnit = itemfuncionarios($row[21],'18');
							$epsnom = itemfuncionarios($row[21],'19');
							$listacuentas[] = $ctaconcepto;
							$listanombrecuentas[] = $nresul;
							$listaterceros[] = $epsnit;
							$listanombreterceros[] = $epsnom;
							$listaccs[] = $row[18];
							$listadetalles[] = "Aporte pension empleado mes $meslnomina";
							$listadebitos[] = 0;
							$listacreditos[] = $row[6];
							$listatipo[] = "PE<->CR";
						}
						//Fondo Solidaridad
						if($row[8] != 0){
							$ctaconcepto = $ctacont = '';
							//Cuenta debito fondo solidaridad
							$ctacont = cuentascontablesccpet::cuentacredito_tipomovccpet($row[19], $row[21], $row[18], $fechaf, 0, '', $id, $row[19]);
							$nresul = buscacuenta($ctacont);
							$listacuentas[] = $ctacont;
							$listanombrecuentas[] = $nresul;
							$listaterceros[] = $row[0];
							$listanombreterceros[] = $empleado;
							$listaccs[] = $row[18];
							$listadetalles[] = "Aporte fondo solidaridad empleado mes $meslnomina";
							$listadebitos[] = $row[8];
							$listacreditos[] = 0;
							$listatipo[] = "FS<->DB";
							//Cuenta credito fondo solidaridad
							$ctaconcepto = cuentascontablesccpet::cuentacredito_tipomovccpet($pbfsol, $row[21], $row[18], $fechaf, 1,'N/A', $id, $row[19]);
							$nresul = buscacuenta($ctaconcepto);
							$epsnit = itemfuncionarios($row[21],'18');
							$epsnom = itemfuncionarios($row[21],'19');
							$listacuentas[] = $ctaconcepto;
							$listanombrecuentas[] = $nresul;
							$listaterceros[] = $epsnit;
							$listanombreterceros[] = $epsnom;
							$listaccs[] = $row[18];
							$listadetalles[] = "Aporte fondo solidaridado mes $meslnomina";
							$listadebitos[] = 0;
							$listacreditos[] = $row[8];
							$listatipo[] = "FS<->CR";
						}
						//Otras Deducciones
						if($row[9] != 0){
							$ctaconcepto=$ctacont='';
							$sqlrd1="SELECT T1.valor,T2.id_retencion,T2.tipopago FROM humnominaretenemp AS T1 INNER JOIN humretenempleados AS T2 ON T1.id=T2.id WHERE T1.id_nom='$id' AND T1.cedulanit='$row[0]' AND T1.tipo_des='DS' AND T2.tipopago='$row[19]' AND T2.idfuncionario='$row[21]'";
							$sqlrd1 =
						"SELECT T1.valor, T2.id_retencion, T2.tipopago, T2.modo_pago 
						FROM humnominaretenemp AS T1 
						INNER JOIN humretenempleados AS T2 
						ON T1.id = T2.id 
						WHERE T1.id_nom='$id' AND T1.cedulanit='$row[0]' AND T1.tipo_des='DS' AND T1.tipopago='".$row[19]."' AND T2.T2.idfuncionario='$row[21]'";
							$respd1=mysqli_query($linkbd,$sqlrd1);
							while ($rowd1=mysqli_fetch_row($respd1)){
								//debito
								$ctaconcepto = $ctacont = '';
								$ctacont = cuentascontablesccpet::cuentacredito_tipomovccpet($rowd1[2], $row[21], $row[18], $fechaf, '0', '',  $id, $row[19]);
								$nresul = buscacuenta($ctacont);
								$sqlrdes = "SELECT nombre FROM humvariables WHERE estado='S' AND codigo='$row[19]'";
								$resdes = mysqli_query($linkbd,$sqlrdes);
								$rowdes = mysqli_fetch_row($resdes);
								$nomceunta = ucwords(strtolower($rowdes[0]));
								$listacuentas[] = $ctacont;
								$listanombrecuentas[] = $nresul;
								$listaterceros[] = $row[0];
								$listanombreterceros[] = $empleado;
								$listaccs[] = $row[18];
								$listadetalles[] = "Decuento $nomdescu Mes $meslnomina";
								$listadebitos[] = $rowd1[0];
								$listacreditos[] = 0;
								$listatipo[] = "DS<->DB";
								//credito
								$sqlrcu = "
								SELECT DISTINCT T1.nombre,T1.beneficiario,T2.cuenta 
								FROM humvariablesretenciones AS T1
								INNER JOIN humvariablesretenciones_det AS T2
								ON T1.codigo = T2.codigo
								WHERE T1.codigo = '$rowd1[1]' AND T2.credito = 'S' AND fechainicial = (SELECT MAX(T3.fechainicial) FROM humvariablesretenciones_det T3 WHERE T3.codigo=T2.codigo AND T3.credito = 'S' AND T3.fechainicial<='$fechaf')";
								$respcu = mysqli_query($linkbd,$sqlrcu);
								while ($rowcu = mysqli_fetch_row($respcu)){
									$ctaconcepto = $rowcu[2];
									$docbenefi = $rowcu[1];
									$nomdescu = ucwords(strtolower($rowcu[0]));
								}
								//Cuenta credito otras deducciones
								$nresul = buscacuenta($ctaconcepto);
								$nombenefi = buscatercero($docbenefi);
								$listacuentas[] = $ctaconcepto;
								$listanombrecuentas[] = $nresul;
								$listaterceros[] = $docbenefi;
								$listanombreterceros[] = $nombenefi;
								$listaccs[] = $row[18];
								$listadetalles[] = "Decuento $nomdescu Mes $meslnomina";
								$listadebitos[] = 0;
								$listacreditos[] = $rowd1[0];
								$listatipo[] = "DS<->CR";
							}
						}
						//Salud Empleador
						if($row[5] != 0){
							$ctaconcepto = $ctacont = '';
							$epsnit = itemfuncionarios($row[21],'14');
							$epsnom = itemfuncionarios($row[21],'15');
							//Cuenta debito salud empleador
							$ctacont = cuentascontablesccpet::cuentadebito_tipomovccpet($psalud_empleador, $row[21], $row[18], $fechaf, 1, 'N/A', $id, $row[19]);
							$nresul = buscacuenta($ctacont);
							$listacuentas[] = $ctacont;
							$listanombrecuentas[] = $nresul;
							$listaterceros[] = $epsnit;
							$listanombreterceros[] = $epsnom;
							$listaccs[] = $row[18];
							$listadetalles[] = "Aporte salud empleador mes $meslnomina";
							$listadebitos[] = $row[5];
							$listacreditos[] = 0;
							$listatipo[] = "SR<->DB";
							$aux = $iter;
							$iter = $iter2;
							$iter2 = $aux;
							$con += 1;
							//Cuenta credito salud empleador
							$ctaconcepto = cuentascontablesccpet::cuentacredito_tipomovccpet($psalud_empleador, $row[21], $row[18], $fechaf, 1, 'N/A', $id, $row[19]);
							$nresul = buscacuenta($ctaconcepto);
							$listacuentas[] = $ctaconcepto;
							$listanombrecuentas[] = $nresul;
							$listaterceros[] = $epsnit;
							$listanombreterceros[] = $epsnom;
							$listaccs[] = $row[18];
							$listadetalles[] = "Aporte salud empleador mes $meslnomina";
							$listadebitos[] = 0;
							$listacreditos[] = $row[5];
							$listatipo[] = "SR<->CR";
							$aux = $iter;
						}
						//Pension Empleador
						if($row[7] != 0){
							$ctaconcepto=$ctacont='';
							$epsnit = itemfuncionarios($row[21],'18');
							$epsnom = itemfuncionarios($row[21],'19');
							//Cuenta debito pension empleador
							$ctacont = cuentascontablesccpet::cuentadebito_tipomovccpet($ppension_empleador, $row[21], $row[18], $fechaf, 1, $row[16], $id, $row[19]);
							$nresul = buscacuenta($ctacont);
							$listacuentas[] = $ctacont;
							$listanombrecuentas[] = $nresul;
							$listaterceros[] = $epsnit;
							$listanombreterceros[] = $epsnom;
							$listaccs[] = $row[18];
							$listadetalles[] = "Aporte pensión empleador mes $meslnomina";
							$listadebitos[] = $row[7];
							$listacreditos[] = 0;
							$listatipo[] = "PR<->DB";
							//Cuenta credito pension empleador
							$ctaconcepto = cuentascontablesccpet::cuentacredito_tipomovccpet($ppension_empleador, $row[21], $row[18] ,$fechaf, 1,$row[16], $id, $row[19]);
							$nresul=buscacuenta($ctaconcepto);
							$listacuentas[] = $ctaconcepto;
							$listanombrecuentas[] = $nresul;
							$listaterceros[] = $epsnit;
							$listanombreterceros[] = $epsnom;
							$listaccs[] = $row[18];
							$listadetalles[] = "Aporte Pensión empleador mes $meslnomina";
							$listadebitos[] = 0;
							$listacreditos[] = $row[7];
							$listatipo[] = "PR<->CR";
						}
						//ARL
						if($row[10] != 0){
							$ctaconcepto = $ctacont = '';
							$epsnit = itemfuncionarios($row[21],'16');
							$epsnom = itemfuncionarios($row[21],'17');
							//Cuenta debito ARL empleador
							$ctacont = cuentascontablesccpet::cuentadebito_tipomovccpet($parp, $row[21], $row[18], $fechaf, 1, 'N/A', $id, $row[19]);
							$nresul = buscacuenta($ctacont);
							$listacuentas[] = $ctacont;
							$listanombrecuentas[] = $nresul;
							$listaterceros[] = $epsnit;
							$listanombreterceros[] = $epsnom;
							$listaccs[] = $row[18];
							$listadetalles[] = "Aportes ARL mes $meslnomina";
							$listadebitos[] = $row[10];
							$listacreditos[] = 0;
							$listatipo[] = "P6<->DB";
							//Cuenta credito ARL empleado
							$ctaconcepto = cuentascontablesccpet::cuentacredito_tipomovccpet($parp, $row[21], $row[18], $fechaf, 1, 'N/A', $id, $row[19]);
							$nresul = buscacuenta($ctaconcepto);
							$listacuentas[] = $ctaconcepto;
							$listanombrecuentas[] = $nresul;
							$listaterceros[] = $epsnit;
							$listanombreterceros[] = $epsnom;
							$listaccs[] = $row[18];
							$listadetalles[] = "Aportes ARL mes $meslnomina";
							$listadebitos[]= 0;
							$listacreditos[] = $row[10];
							$listatipo[] = "P6<->CR";
						}
						//COFREM
						if($row[11] != 0){
							$ctaconcepto = $ctacont = '';
							$parafiscal = $pcajacomp;
							$nomparafiscal = buscatercero($cajacomp);
							$nitparafiscal = $cajacomp;
							$valparafiscal = $row[11];
							//Cuenta debito Caja de compensación familiar
							$ctacont = cuentascontablesccpet::cuentadebito_tipomovccpet($parafiscal, $row[21], $row[18], $fechaf, 1, 'N/A', $id, $row[19]);
							$nresul = buscacuenta($ctacont);
							$listacuentas[] = $ctacont;
							$listanombrecuentas[] = $nresul;
							$listaterceros[] = $nitparafiscal;
							$listanombreterceros[] = $nomparafiscal;
							$listaccs[] = $row[18];
							$listadetalles[] = "Aportes caja compensación mes $meslnomina";
							$listadebitos[] = $row[11];
							$listacreditos[] = 0;
							$listatipo[] = "P1<->DB";
							//Cuenta credito Caja de compensación familiar
							$ctaconcepto = cuentascontablesccpet::cuentacredito_tipomovccpet($parafiscal, $row[21], $row[18], $fechaf, 1, 'N/A',$id, $row[19]);
							$nresul = buscacuenta($ctaconcepto);
							$listacuentas[] = $ctaconcepto;
							$listanombrecuentas[] = $nresul;
							$listaterceros[] = $nitparafiscal;
							$listanombreterceros[] = $nomparafiscal;
							$listaccs[] = $row[18];
							$listadetalles[] = "Aportes caja compensación mes $meslnomina";
							$listadebitos[] = 0;
							$listacreditos[] = $row[11];
							$listatipo[] = "P1<->CR";
						}
						//SENA
						if($row[12] != 0){
							$ctaconcepto = $ctacont = '';
							$parafiscal = $psena;
							$nitparafiscal = $sena;
							$nomparafiscal = buscatercero($sena);
							//Cuenta debito SENA
							$ctacont = cuentascontablesccpet::cuentadebito_tipomovccpet($parafiscal, $row[21], $row[18], $fechaf, 1,'N/A',$id, $row[19]);
							$nresul = buscacuenta($ctacont);
							$listacuentas[] = $ctacont;
							$listanombrecuentas[] = $nresul;
							$listaterceros[] = $nitparafiscal;
							$listanombreterceros[] = $nomparafiscal;
							$listaccs[] = $row[18];
							$listadetalles[] = "Aportes SENA mes $meslnomina";
							$listadebitos[] = $row[12];
							$listacreditos[] = 0;
							$listatipo[] = "P3<->DB";
							//Cuenta credito ICBF
							$ctaconcepto = cuentascontablesccpet::cuentacredito_tipomovccpet($parafiscal, $row[21], $row[18], $fechaf, 1, 'N/A', $id, $row[19]);
							$nresul = buscacuenta($ctaconcepto);
							$listacuentas[] = $ctaconcepto;
							$listanombrecuentas[] = $nresul;
							$listaterceros[] = $nitparafiscal;
							$listanombreterceros[] = $nomparafiscal;
							$listaccs[] = $row[18];
							$listadetalles[] = "Aportes SENA mes $meslnomina";
							$listadebitos[] = 0;
							$listacreditos[] = $row[12];
							$listatipo[] = "P3<->CR";
						}
						//ICBF
						if($row[13] != 0){
							$ctaconcepto = $ctacont = '';
							$parafiscal = $picbf;
							$nomparafiscal = buscatercero($icbf);
							$nitparafiscal = $icbf;
							//Cuenta debito ICBF
							$ctacont = cuentascontablesccpet::cuentadebito_tipomovccpet($parafiscal, $row[21], $row[18], $fechaf, 1, 'N/A', $id, $row[19]);
							$nresul = buscacuenta($ctacont);
							$listacuentas[] = $ctacont;
							$listanombrecuentas[] = $nresul;
							$listaterceros[] = $nitparafiscal;
							$listanombreterceros[] = $nomparafiscal;
							$listaccs[] = $row[18];
							$listadetalles[] = "Aportes ICBF mes $meslnomina";
							$listadebitos[] = $row[13];
							$listacreditos[] = 0;
							$listatipo[] = "P2<->DB";
							//Cuenta credito ICBF
							$ctaconcepto = cuentascontablesccpet::cuentacredito_tipomovccpet($parafiscal, $row[21], $row[18], $fechaf, 1, 'N/A', $id, $row[19]);
							$nresul = buscacuenta($ctaconcepto);
							$listacuentas[] = $ctaconcepto;
							$listanombrecuentas[] = $nresul;
							$listaterceros[] = $nitparafiscal;
							$listanombreterceros[] = $nomparafiscal;
							$listaccs[] = $row[18];
							$listadetalles[] = "Aportes ICBF mes $meslnomina";
							$listadebitos[] = 0;
							$listacreditos[] = $row[13];
							$listatipo[] = "P2<->CR";
						}
						//INSTITUTOS TEC
						if($row[14] != 0){
							$ctaconcepto = $ctacont = '';
							$parafiscal = $piti;
							$nitparafiscal = $iti;
							$nomparafiscal = buscatercero($iti);
							//Cuenta debito Inst tecnicos 
							$ctacont = cuentascontablesccpet::cuentadebito_tipomovccpet($parafiscal, $row[21], $row[18], $fechaf, 1, 'N/A', $id, $row[19]);
							$nresul = buscacuenta($ctacont);
							$listacuentas[] = $ctacont;
							$listanombrecuentas[] = $nresul;
							$listaterceros[] = $nitparafiscal;
							$listanombreterceros[] = $nomparafiscal;
							$listaccs[] = $row[18];
							$listadetalles[] = "Aportes Inst técnicos Mes $meslnomina";
							$listadebitos[] = $row[14];
							$listacreditos[] = 0;
							$listatipo[] = "P4<->DB";
							//Cuenta credito Inst tecnicos 
							$ctaconcepto = cuentascontablesccpet::cuentacredito_tipomovccpet($parafiscal, $row[21], $row[18], $fechaf, 1, 'N/A', $id, $row[19]);
							$nresul = buscacuenta($ctaconcepto);
							$listacuentas[] = $ctaconcepto;
							$listanombrecuentas[] = $nresul;
							$listaterceros[] = $nitparafiscal;
							$listanombreterceros[] = $nomparafiscal;
							$listaccs[] = $row[18];
							$listadetalles[] = "Aportes Inst técnicos Mes $meslnomina";
							$listadebitos[] = 0;
							$listacreditos[] = $row[14];
							$listatipo[] = "P4<->CR";
						}
						//ESAP
						if($row[15] != 0){
							$ctaconcepto = $ctacont = '';
							$parafiscal = $pesap;
							$nitparafiscal = $esap;
							$nomparafiscal = buscatercero($esap);
							//Cuenta debito ESAP
							$ctacont = cuentascontablesccpet::cuentadebito_tipomovccpet($parafiscal, $row[21], $row[18], $fechaf, 1, 'N/A', $id, $row[19]);
							$nresul = buscacuenta($ctacont);
							$listacuentas[] = $ctacont;
							$listanombrecuentas[] = $nresul;
							$listaterceros[] = $nitparafiscal;
							$listanombreterceros[] = $nomparafiscal;
							$listaccs[] = $row[18];
							$listadetalles[] = "Aportes ESAP mes $meslnomina";
							$listadebitos[] = $row[15];
							$listacreditos[] = 0;
							$listatipo[] = "P5<->DB";
							//Cuenta credito ESAP 
							$ctaconcepto = cuentascontablesccpet::cuentacredito_tipomovccpet($parafiscal, $row[21], $row[18], $fechaf, 1, 'N/A', $id, $row[19]);
							$nresul = buscacuenta($ctaconcepto);
							$listacuentas[] = $ctaconcepto;
							$listanombrecuentas[] = $nresul;
							$listaterceros[] = $nitparafiscal;
							$listanombreterceros[] = $nomparafiscal;
							$listaccs[] = $row[18];
							$listadetalles[] = "Aportes ESAP mes $meslnomina";
							$listadebitos[] = 0;
							$listacreditos[] = $row[15];
							$listatipo[] = "P5<->CR";
							$aux = $iter;
							$iter = $iter2;
							$iter2 = $aux;
							$con += 1;
						}
						//Retenciones
						if($row[20] != 0){
							$ctaconcepto = $ctacont = '';
							$sqlrd1 = "SELECT valorretencion, tiporetencion FROM hum_retencionesfun WHERE id_nom = '$id' AND docfuncionario = '$row[0]'";
							$respd1 = mysqli_query($linkbd,$sqlrd1);
							while ($rowd1=mysqli_fetch_row($respd1)){
								$sqlcodi = "SELECT T2.conceptoingreso, T1.nombre FROM tesoretenciones T1, tesoretenciones_det T2 WHERE T1.id = T2.codigo AND T1.id = '$rowd1[1]'";
								$rescodi = mysqli_query($linkbd,$sqlcodi);
								$rowcodi = mysqli_fetch_row($rescodi);
								$sqlrcu="SELECT DISTINCT cuenta, debito, credito FROM conceptoscontables_det WHERE modulo = '4' AND tipo = 'RI' AND cc = '$row[18]' AND tipocuenta = 'N' AND codigo = '$rowcodi[0]' AND fechainicial = (SELECT MAX(fechainicial) FROM conceptoscontables_det WHERE fechainicial <= '$fechaf' AND modulo = '4' AND tipo = 'RI' AND cc = '$row[18]' AND tipocuenta = 'N' AND codigo = '$rowcodi[0]')  ORDER BY credito";
								
								$respcu = mysqli_query($linkbd,$sqlrcu);
								while ($rowcu =mysqli_fetch_row($respcu)){
									$ctaconcepto = $rowcu[0];
								}
								//Cuenta debito otras deducciones
								$ctacont = cuentascontablesccpet::cuentacredito_tipomovccpet($rowd1[2], $row[21], $row[18], $fechaf, 0, '', $id, $row[19]);
								$nresul = buscacuenta($ctacont);
								$listacuentas[] = $ctacont;
								$listanombrecuentas[] = $nresul;
								$listaterceros[] = $row[0];
								$listanombreterceros[] = $empleado;
								$listaccs[] = $row[18];
								$listadetalles[] = "Retención $rowcodi[1] mes $meslnomina";
								$listadebitos[] = $rowd1[0];
								$listacreditos[] = 0;
								$listatipo[] = "RE<->DB";
								//Cuenta credito otras deducciones
								$nresul = buscacuenta($ctaconcepto);
								$listacuentas[] = $ctaconcepto;
								$listanombrecuentas[] = $nresul;
								$listaterceros[] = $row[0];
								$listanombreterceros[] = $empleado;
								$listaccs[] = $row[18];
								$listadetalles[] = "Retención $rowcodi[1] mes $meslnomina";
								$listadebitos[] = 0;
								$listacreditos[] = $rowd1[0];
								$listatipo[] = "RE<->CR";
							}
						}
					}
					$descripgen = "CAUSACION $id MES $meslnomina";
					$sqlcc="INSERT INTO comprobante_cab (numerotipo, tipo_comp, fecha, concepto, total, total_debito, total_credito, diferencia, estado) VALUES ($id, 4, '$fechaf', '$descripgen', 0, 0, 0, 0, '1')";
					mysqli_query($linkbd,$sqlcc);
					for ($x=0; $x<count($listacuentas); $x++){
						$sqlcd = "INSERT INTO comprobante_det (id_comp, cuenta, tercero, centrocosto, detalle, cheque, valdebito, valcredito, estado, vigencia) VALUES ('4 $id', '$listacuentas[$x]', '$listaterceros[$x]', '$listaccs[$x]', '".$listadetalles[$x]."', '', '$listadebitos[$x]', '$listacreditos[$x]', '1', '".$_POST['vigencia']."')";
						mysqli_query($linkbd,$sqlcd);
					}
				}
				$out['errorlt'] = $errorlt;
			}break;
			case 'carga_ver':{
				
				$datosnomina = array();
				//tabla aprobación nomina
				$sqlmm = "SELECT MIN(id_aprob), MAX(id_aprob) FROM humnomina_aprobado";
				$resmm = mysqli_query($linkbd,$sqlmm);
				$rowmm = mysqli_fetch_row($resmm);
				$apmin = $rowmm[0];
				$apmax = $rowmm[1];
				//tabla aprobación nomina
				$sql = "SELECT * FROM humnomina_aprobado WHERE id_aprob = '".$_POST['idcomp']."'";
				$res = mysqli_query($linkbd,$sql);
				$row = mysqli_fetch_row($res);
				preg_match("/([0-9]{4})\-([0-9]{2})\-([0-9]{2})/", $row[2],$fecha);
				$fechaf = "$fecha[3]/$fecha[2]/$fecha[1]";
				$vigencia = $row[10];
				//tabla rp
				$sqlrp = "SELECT detalle, idcdp, valor  FROM ccpetrp WHERE vigencia = '$vigencia' AND consvigencia = '$row[3]'";
				$resrp = mysqli_query($linkbd,$sqlrp);
				$rowrp = mysqli_fetch_row($resrp);
				$desrp = $rowrp[0];
				$idcdp = $rowrp[1];
				$valorrp = $rowrp[2];
				//tablas de union novedades y nomina
				$sqldes = "SELECT T1.descripcion FROM hum_novedadespagos_cab AS T1 INNER JOIN hum_prenomina AS T2 ON T2.codigo = T1.prenomina WHERE T2.num_liq = '$row[1]'";
				$resdes = mysqli_query($linkbd,$sqldes);
				$rowdes = mysqli_fetch_row($resdes);
				$desnomi = $rowdes[0];
				//total nomina
				$sqlnom = "SELECT SUM(valor) FROM humnom_presupuestal WHERE id_nom = '$row[1]'";
				$resnom = mysqli_query($linkbd,$sqlnom);
				$rownom = mysqli_fetch_row($resnom);
				$valnom = $rownom[0];
				//total cdp
				$sqlcdp = "SELECT valor FROM ccpetcdp WHERE vigencia = '$vigencia' AND consvigencia = '$row[7]'";
				$rescdp = mysqli_query($linkbd,$sqlcdp);
				$rowcdp = mysqli_fetch_row($rescdp);
				$valcdp = $rowcdp[0];

				//cargar datos
				array_push($datosnomina, $row[1]);//id nomina
				array_push($datosnomina, $fechaf);//fecha
				array_push($datosnomina, $row[3]);//RP
				array_push($datosnomina, $row[4]);//aprobo
				array_push($datosnomina, $row[5]);//estado
				array_push($datosnomina, $row[6]);//tipo movimento
				array_push($datosnomina, $desrp);//descripción RP
				array_push($datosnomina, $row[7]);//CDP
				array_push($datosnomina, $desnomi);//descripción nómina
				array_push($datosnomina, $valorrp);//Valor RP
				array_push($datosnomina, $valnom);//Valor nomina
				array_push($datosnomina, $valcdp);//Valor cdp
				array_push($datosnomina, $vigencia);//Vigencia
				array_push($datosnomina, $row[8]);//doc tercero
				array_push($datosnomina, $row[9]);//nom tercero
				array_push($datosnomina, $apmin);//id menor
				array_push($datosnomina, $apmax);//id mayor
				
				$out['datosnomina'] = $datosnomina;
			}break;
			case 'carga_buscar':{
				$infoaprobada = array();
				$band1 = 0;
				preg_match("/([0-9]{2})\/([0-9]{2})\/([0-9]{4})/", $_POST['fechaini'],$fecha);
				$fechaini = "$fecha[3]-$fecha[2]-$fecha[1]";
				preg_match("/([0-9]{2})\/([0-9]{2})\/([0-9]{4})/", $_POST['fechafin'],$fecha);
				$fechafin = "$fecha[3]-$fecha[2]-$fecha[1]";
				$sql = "SELECT * FROM humnomina_aprobado WHERE fecha BETWEEN '$fechaini' AND '$fechafin' ORDER BY id_aprob DESC";
				$res = mysqli_query($linkbd,$sql);
				while($row = mysqli_fetch_row($res)){
					$datos = array();
					$vigencia = date('Y',strtotime($row[2]));
					//tablas de union novedades y nomina
					$sqldes = "SELECT T1.descripcion FROM hum_novedadespagos_cab AS T1 INNER JOIN hum_prenomina AS T2 ON T2.codigo = T1.prenomina WHERE T2.num_liq = '$row[1]'";
					$resdes = mysqli_query($linkbd,$sqldes);
					$rowdes = mysqli_fetch_row($resdes);
					$desnomi = $rowdes[0];
					//tablas de valor RP
					$sqlrp = "SELECT idcdp, valor FROM ccpetrp WHERE consvigencia = '$row[3]' AND vigencia = '$vigencia'";
					$resrp = mysqli_query($linkbd,$sqlrp);
					$rowrp = mysqli_fetch_row($resrp);
					//verifica si tiene egresos la nomina
					$sqlreg = "SELECT id_egreso FROM tesoegresosnomina WHERE id_orden = '$row[1]' AND estado='S'";
					$respeg = mysqli_query($linkbd,$sqlreg);
					$numegr = mysqli_num_rows($respeg);
					if ($numegr == 0 ){ 
						if($band1 == 0){
							$fundes = $row[0];
							$imgdes = "imagenes/flechades.png";
						}else{
							$fundes = 'NO';
							$imgdes = "imagenes/flechadesd.png";
						}
						$imgsem = "imagenes/sema_amarilloON.jpg";
						$titsem = "Sin Orden de Pago";
						$band1 = 1;
					}else{
						$fundes = 'NO';
						$imgdes = "imagenes/flechadesd.png";
						if($row[5] == 'S'){
							$imgsem = "imagenes/sema_verdeON.jpg";
							$titsem = "Con Orden de Pago";
						}
					}
					if($_POST['idr'] != ''){
						if($_POST['idr']== $row[0]){
							$resaltar= 'S';
						}else{
							$resaltar = 'N';
						}
					}else{
						$resaltar = 'N';
					}
					preg_match("/([0-9]{4})\-([0-9]{2})\-([0-9]{2})/", $row[2],$fecha);
					$fechaf = "$fecha[3]/$fecha[2]/$fecha[1]";
					array_push($datos, $row[0]);//código
					array_push($datos, $row[1]);//id nomina
					array_push($datos, $fechaf);//fecha
					array_push($datos, $row[3]);//RP
					array_push($datos, $row[4]);//aprobo
					array_push($datos, $row[5]);//estado
					array_push($datos, $row[6]);//tipo movimento
					array_push($datos, $desnomi);//descripción nómina
					array_push($datos, $titsem);//texto semaforo
					array_push($datos, $imgsem);//imagen semaforo
					array_push($datos, $imgdes);//imagen deshacer
					array_push($datos, $fundes);//funcion deshacer
					array_push($datos, $row[7]);//cdp
					array_push($datos, $rowrp[1]);//valor rp
					array_push($datos, $resaltar);//si es amarillo
					array_push($infoaprobada, $datos);
				}
				$out['infoaprobada'] = $infoaprobada;
			}break;
			case 'deshacer':{
				$errorlt = 0;
				$sqlr ="DELETE FROM humnomina_aprobado WHERE id_aprob = '".$_POST['idcomp']."'";
				if (!mysqli_query($linkbd,$sqlr)){
					$errorlt = 1;
				}else{
					$sqlr ="DELETE FROM pptocomprobante_cab WHERE numerotipo = '".$_POST['idnomi']."' AND tipo_comp = '9'";
					mysqli_query($linkbd,$sqlr);
					$sqlr ="DELETE FROM pptocomprobante_det WHERE numerotipo = '".$_POST['idnomi']."' AND tipo_comp = '9'";
					mysqli_query($linkbd,$sqlr);
					$sqlr="UPDATE humnomina SET estado = 'S' WHERE id_nom = '".$_POST['idnomi']."'"; 
					mysqli_query($linkbd,$sqlr);
					$sqlr="UPDATE humnom_presupuestal SET estado = 'S' WHERE id_nom = '".$_POST['idnomi']."'";
					mysqli_query($linkbd,$sqlr);
					$sqlr ="DELETE FROM comprobante_cab WHERE numerotipo = '".$_POST['idnomi']."' AND tipo_comp = '4'";
					mysqli_query($linkbd,$sqlr);
					$sqlr ="DELETE FROM comprobante_det WHERE numerotipo = '".$_POST['idnomi']."' AND tipo_comp = '4'";
					mysqli_query($linkbd,$sqlr);
				}
				$out['errorlt'] = $errorlt;
			}break;
		}
	}
	header("Content-type: application/json");
	echo json_encode($out);
	die();
?>