<?php
	require_once '../../comun.inc';
	require '../../funciones.inc';
	require '../../funcionesnomima.inc';

	$linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");

	if(isset($_GET['accion'])){
		$accion = $_GET['accion'];
		switch ($accion){
			case 'datosiniciales':{
				$numnomina = $_GET['numnomina'];
				$datosfuncionarios = array();
				$sql = "SELECT tipopago, detalle, cedulanit, idfuncionario, salud, saludemp, totalsalud, pension, pensionemp, totalpension, fondosolid, arp, cajacf, sena, icbf, instecnicos, esap, retefte, totaldeduc, netopagar, id FROM humnomina_det WHERE id_nom = '$numnomina' ORDER BY idfuncionario, tipopago";
				$res = mysqli_query($linkbd,$sql);
				while($row = mysqli_fetch_assoc($res)){
					$datos = array();
					$nombrefun = itemfuncionarios($row['idfuncionario'],'7');
					array_push($datos, $row['tipopago']);
					array_push($datos, $row['detalle']);
					array_push($datos, $row['cedulanit']);
					array_push($datos, $nombrefun);
					array_push($datos, $row['salud']);
					array_push($datos, $row['saludemp']);
					array_push($datos, $row['totalsalud']);
					array_push($datos, $row['pension']);
					array_push($datos, $row['pensionemp']);
					array_push($datos, $row['totalpension']);
					array_push($datos, $row['fondosolid']);
					array_push($datos, $row['arp']);
					array_push($datos, $row['cajacf']);
					array_push($datos, $row['sena']);
					array_push($datos, $row['icbf']);
					array_push($datos, $row['instecnicos']);
					array_push($datos, $row['esap']);
					array_push($datos, $row['retefte']);
					array_push($datos, $row['totaldeduc']);
					array_push($datos, $row['netopagar']);
					array_push($datos, $row['id']);
					array_push($datosfuncionarios, $datos);
				}
				$out['datosfuncionarios'] = $datosfuncionarios;
			}break;
			case 'guardar':{
				//Almacenar informacion corregida
				for ($i=0; $i < count($_POST['vid']); $i++){
					$sql = "UPDATE humnomina_det SET salud = '".$_POST['vsaludfun'][$i]."', saludemp = '".$_POST['vsaludemp'][$i]."', totalsalud = '".$_POST['vsaludtotal'][$i]."', pension = '".$_POST['vpensionfun'][$i]."', pensionemp = '".$_POST['vpensionemp'][$i]."', totalpension = '".$_POST['vpensiontotal'][$i]."', fondosolid = '".$_POST['vfondosoli'][$i]."', arp = '".$_POST['varl'][$i]."', cajacf = '".$_POST['vccf'][$i]."', sena = '".$_POST['vsena'][$i]."', icbf = '".$_POST['vicbf'][$i]."', instecnicos = '".$_POST['vinte'][$i]."', esap = '".$_POST['vesap'][$i]."', totaldeduc = '".$_POST['vtotaldeduc'][$i]."', netopagar = '".$_POST['vnetopagar'][$i]."'  WHERE id = '".$_POST['vid'][$i]."'";
					mysqli_query($linkbd, $sql);
				}
				//Calcular nuevamente presupuesto
				$numnomina = $_GET['numnomina'];
				$pfcp = array();
				$sqlrtp = "SELECT * FROM humparametrosliquida";
				$resptp = mysqli_query($linkbd,$sqlrtp);
				$rowtp = mysqli_fetch_row($resptp);
				$tcajacomp = $rowtp[17];
				$ticbf = $rowtp[18];
				$tsena = $rowtp[19];
				$titi = $rowtp[20];
				$tesap = $rowtp[21];
				$tarp = $rowtp[22];
				$tsaludemr = $rowtp[23];
				$tsaludemp = $rowtp[24];
				$tpensionemr = $rowtp[25];
				$tpensionemp = $rowtp[26];
				
				//******Borrar tabla Salud y Pension
				$sqld = "DELETE FROM humnomina_saludpension WHERE id_nom = '$numnomina'";
				$resd = mysqli_query($linkbd,$sqld);
				//******Borrar tabla parafiscales
				$sqld = "DELETE FROM humnomina_parafiscales WHERE id_nom='$numnomina'";
				$resd = mysqli_query($linkbd,$sqld);
				
				$sqlrg = "SELECT * FROM humnomina WHERE id_nom = '$numnomina'";
				$respg = mysqli_query($linkbd,$sqlrg);
				$rowg = mysqli_fetch_row($respg);
				$fecha = $rowg[1];
				$vigencia = $rowg[7];
				//Organizar información
				$sqlrt = "SELECT * FROM humnomina_det WHERE id_nom='$numnomina'";
				$respt = mysqli_query($linkbd,$sqlrt);
				while ($rowt = mysqli_fetch_row($respt)){
					$tipopresupuesto = buscartipovinculacion($rowt[0], $rowt[36], $rowt[38]);
					$cuantapresu = $porcentaje = $secpresu =  $programaticopara = $proyectopara = $bpimpara = '';
					$secpresu = itemfuncionarios($rowt[38],'42');
					if($tipopresupuesto[1] == 'IN' || $tipopresupuesto[1] == 'IT'){
						$fuentepara = itemfuncionarios($rowt[38],'44');
						$programaticopara = itemfuncionarios($rowt[38],'43');
						$proyectopara = itemfuncionarios($rowt[38],'31');
						$bpimpara = nombrebpim($proyectopara);
					}
					else{
						$bpimpara = '';
						$programaticopara ='';
						$proyectopara = '';
					}
					//********SALUD EMPLEADO *****
					if($rowt[10] > 0){
						$idsalud = selconsecutivo('humnomina_saludpension','id');
						$cuantapresu = buscarcuentanuevocatalogo1($rowt[36],$tipopresupuesto[1]);
						$doceps = itemfuncionarios($rowt[38],'14');
						if($tipopresupuesto[1] != 'IN' && $tipopresupuesto[1] != 'IT'){
							$fuentepara = '';
							$sql = "SELECT fuente FROM ccpetinicialgastosfun WHERE cuenta = '$cuantapresu' AND seccion_presupuestal = '$secpresu' AND vigencia = '$vigencia'";
							$res = mysqli_query($linkbd,$sql);
							$row = mysqli_fetch_row($res);
							$fuentepara = $row[0];
						}
						$sqlrins="INSERT INTO humnomina_saludpension (id_nom, tipo, empleado, tercero, cc, valor, estado, sector, id, cod_fun, cuenta_presu, tipopago, fuente, indicador_producto, bpin, productoservicio, medio_pago, codigo_politicap, tipo_mov, proyecto, seccion_presupuestal) VALUES ('$numnomina', 'SE', '$rowt[1]', '$doceps', '$rowt[34]', '$rowt[10]', 'S', '', '$idsalud', '$rowt[38]', '$cuantapresu', '$rowt[36]', '$fuentepara', '$programaticopara', '$bpimpara', '', 'CSF', '1', '201', '$proyectopara', '$secpresu')";
						mysqli_query($linkbd,$sqlrins);
					}
					//********PENSION EMPLEADO *****
					if($rowt[12]>0){
						$idsalud=selconsecutivo('humnomina_saludpension','id');
						$cuantapresu = buscarcuentanuevocatalogo1($rowt[36],$tipopresupuesto[1]);
						$docfp = itemfuncionarios($rowt[38],'18');
						if($tipopresupuesto[1] != 'IN' && $tipopresupuesto[1] != 'IT'){
							$fuentepara = '';
							$sql = "SELECT fuente FROM ccpetinicialgastosfun WHERE cuenta = '$cuantapresu' AND seccion_presupuestal = '$secpresu' AND vigencia = '$vigencia'";
							$res = mysqli_query($linkbd,$sql);
							$row = mysqli_fetch_row($res);
							$fuentepara = $row[0];
						}
						$sqlrins="INSERT INTO humnomina_saludpension (id_nom, tipo, empleado, tercero, cc, valor, estado, sector, id, cod_fun, cuenta_presu, tipopago, fuente, indicador_producto, bpin, productoservicio, medio_pago, codigo_politicap, tipo_mov, proyecto, seccion_presupuestal) VALUES ('$numnomina', 'PE', '$rowt[1]', '$docfp', '$rowt[34]', '$rowt[12]', 'S', 'PR', '$idsalud', '$rowt[38]', '$cuantapresu', '$rowt[36]', '$fuentepara', '$programaticopara', '$bpimpara', '', 'CSF', '1', '201', '$proyectopara', '$secpresu')";
						mysqli_query($linkbd,$sqlrins);
					}
					//********FONDO SOLIDARIDAD EMPLEADO *****
					if($rowt[14]>0){
						$idsalud = selconsecutivo('humnomina_saludpension','id');
						$cuantapresu = buscarcuentanuevocatalogo1($rowt[36],$tipopresupuesto[1]);
						$docfp = itemfuncionarios($rowt[38],'18');
						if($tipopresupuesto[1] != 'IN' && $tipopresupuesto[1] != 'IT'){
							$fuentepara = '';
							$sql = "SELECT fuente FROM ccpetinicialgastosfun WHERE cuenta = '$cuantapresu' AND seccion_presupuestal = '$secpresu' AND vigencia = '$vigencia'";
							$res = mysqli_query($linkbd,$sql);
							$row = mysqli_fetch_row($res);
							$fuentepara = $row[0];
						}
						$sqlrins ="INSERT INTO  humnomina_saludpension (id_nom,tipo,empleado,tercero,cc,valor,estado,sector,id,cod_fun,cuenta_presu, tipopago, fuente, indicador_producto, bpin, productoservicio, medio_pago, codigo_politicap, tipo_mov, proyecto, seccion_presupuestal) VALUES ('$numnomina','FS','$rowt[1]','$docfp','$rowt[34]','$rowt[14]','S','PR','$idsalud','$rowt[38]', '$cuantapresu', '$rowt[36]', '$fuentepara', '$programaticopara', '$bpimpara', '', 'CSF', '1', '201', '$proyectopara', '$secpresu')";
						mysqli_query($linkbd,$sqlrins);
					}
					//******** SALUD EMPLEADOR *******
					if($rowt[11]>0){
						$idsalud = selconsecutivo('humnomina_saludpension','id');
						$cuantapresu = buscarcuentanuevocatalogo2($tsaludemr,$tipopresupuesto[1],"N/A");
						$doceps = itemfuncionarios($rowt[38],'14');
						if($tipopresupuesto[1] != 'IN' && $tipopresupuesto[1] != 'IT'){
							$fuentepara = '';
							$sql = "SELECT fuente FROM ccpetinicialgastosfun WHERE cuenta = '$cuantapresu' AND seccion_presupuestal = '$secpresu' AND vigencia = '$vigencia'";
							$res = mysqli_query($linkbd,$sql);
							$row = mysqli_fetch_row($res);
							$fuentepara = $row[0];
						}
						$cuentafuente = $secpresu.'<_>'.$cuantapresu.'<_>'.$fuentepara;
						$pfcp[$cuentafuente] += $rowt[11];
						$sqlrins="INSERT INTO  humnomina_saludpension (id_nom, tipo, empleado, tercero, cc, valor, estado, sector, id, cod_fun, cuenta_presu, tipopago, fuente, indicador_producto, bpin, productoservicio, medio_pago, codigo_politicap, tipo_mov, proyecto, seccion_presupuestal) VALUES ('$numnomina', 'SR','$rowt[1]', '$doceps', '$rowt[34]', '$rowt[11]', 'S', '', '$idsalud', '$rowt[38]', '$cuantapresu', '$rowt[36]', '$fuentepara', '$programaticopara', '$bpimpara', '', 'CSF', '1', '201', '$proyectopara', '$secpresu')";
						mysqli_query($linkbd,$sqlrins);
					}
					//******** PENSIONES EMPLEADOR *******
					if($rowt[13]>0){
						$idsalud=selconsecutivo('humnomina_saludpension','id');
						$cuantapresu = buscarcuentanuevocatalogo2($tpensionemr,$tipopresupuesto[1],"PR");
						$docfp = itemfuncionarios($rowt[38],'18');
						if($tipopresupuesto[1] != 'IN' && $tipopresupuesto[1] != 'IT'){
							$fuentepara = '';
							$sql = "SELECT fuente FROM ccpetinicialgastosfun WHERE cuenta = '$cuantapresu' AND seccion_presupuestal = '$secpresu' AND vigencia = '$vigencia'";
							$res = mysqli_query($linkbd,$sql);
							$row = mysqli_fetch_row($res);
							$fuentepara = $row[0];
						}
						$cuentafuente = $secpresu.'<_>'.$cuantapresu.'<_>'.$fuentepara;
						$pfcp[$cuentafuente] += $rowt[13];
						$sqlrins="INSERT INTO  humnomina_saludpension (id_nom, tipo, empleado, tercero, cc, valor ,estado, sector, id, cod_fun, cuenta_presu, tipopago, fuente, indicador_producto, bpin, productoservicio, medio_pago, codigo_politicap, tipo_mov, proyecto, seccion_presupuestal) VALUES ('$numnomina','PR','$rowt[1]','$docfp','$rowt[34]','$rowt[13]','S','PR','$idsalud','$rowt[38]', '$cuantapresu', '$rowt[36]', '$fuentepara', '$programaticopara', '$bpimpara', '', 'CSF', '1', '201', '$proyectopara', '$secpresu')";
						mysqli_query($linkbd,$sqlrins);
					}
					//CAJAS DE COMPENSACION
					if($rowt[22] > 0){
						$cuantapresu = buscarcuentanuevocatalogo2($tcajacomp,$tipopresupuesto[1],"N/A");
						$porcentaje = itemfuncionarios($rowt[38],'36');
						if($tipopresupuesto[1] != 'IN' && $tipopresupuesto[1] != 'IT'){
							$fuentepara = 0;
							$sql = "SELECT fuente FROM ccpetinicialgastosfun WHERE cuenta = '$cuantapresu' AND seccion_presupuestal = '$secpresu' AND vigencia = '$vigencia'";
							$res = mysqli_query($linkbd,$sql);
							$row = mysqli_fetch_row($res);
							$fuentepara = $row[0];
						}
						$cuentafuente = $secpresu.'<_>'.$cuantapresu.'<_>'.$fuentepara;
						$pfcp[$cuentafuente] += $rowt[22];
						$nuevoid = selconsecutivo('humnomina_parafiscales','id');
						$sqlr="INSERT INTO humnomina_parafiscales (id_nom, id_parafiscal, porcentaje, valor, cc, estado, cuentapresu, id, codfun, tipopago, fuente, indicador_producto, bpin, productoservicio, medio_pago, codigo_politicap, tipo_mov, proyecto, seccion_presupuestal) VALUES ('$numnomina', '$tcajacomp', '$porcentaje', '$rowt[22]', '$rowt[34]', 'S', '$cuantapresu', '$nuevoid', '$rowt[38]', '$rowt[36]', '$fuentepara', '$programaticopara', '$bpimpara', '', 'CSF', '1', '201', '$proyectopara', '$secpresu')";
						mysqli_query($linkbd,$sqlr);
					}
					//ICBF
					if($rowt[24] > 0){
						$cuantapresu = buscarcuentanuevocatalogo2($ticbf,$tipopresupuesto[1],"N/A");
						$porcentaje = itemfuncionarios($rowt[38],'37');
						if($tipopresupuesto[1] != 'IN' && $tipopresupuesto[1] != 'IT'){
							$fuentepara = 0;
							$sql = "SELECT fuente FROM ccpetinicialgastosfun WHERE cuenta = '$cuantapresu' AND seccion_presupuestal = '$secpresu' AND vigencia = '$vigencia'";
							$res = mysqli_query($linkbd,$sql);
							$row = mysqli_fetch_row($res);
							$fuentepara = $row[0];
						}
						$cuentafuente = $secpresu.'<_>'.$cuantapresu.'<_>'.$fuentepara;
						$pfcp[$cuentafuente] += $rowt[24];
						$nuevoid = selconsecutivo('humnomina_parafiscales','id');
						$sqlr = "INSERT INTO humnomina_parafiscales (id_nom, id_parafiscal, porcentaje, valor, cc, estado, cuentapresu, id, codfun, tipopago, fuente, indicador_producto, bpin, productoservicio, medio_pago, codigo_politicap, tipo_mov, proyecto, seccion_presupuestal) VALUES ('$numnomina', '$ticbf', '$porcentaje', '$rowt[24]', '$rowt[34]', 'S', '$cuantapresu', '$nuevoid', '$rowt[38]', '$rowt[36]', '$fuentepara', '$programaticopara', '$bpimpara', '', 'CSF', '1', '201', '$proyectopara', '$secpresu')";
						mysqli_query($linkbd,$sqlr);
					}
					//SENA
					if($rowt[23] > 0){
						$cuantapresu = buscarcuentanuevocatalogo2($tsena,$tipopresupuesto[1],"N/A");
						$porcentaje = itemfuncionarios($rowt[38],'38');
						if($tipopresupuesto[1] != 'IN' && $tipopresupuesto[1] != 'IT'){
							$fuentepara = 0;
							$sql = "SELECT fuente FROM ccpetinicialgastosfun WHERE cuenta = '$cuantapresu' AND seccion_presupuestal = '$secpresu' AND vigencia = '$vigencia'";
							$res = mysqli_query($linkbd,$sql);
							$row = mysqli_fetch_row($res);
							$fuentepara = $row[0];
						}
						$cuentafuente = $secpresu.'<_>'.$cuantapresu.'<_>'.$fuentepara;
						$pfcp[$cuentafuente] += $rowt[23];
						$nuevoid = selconsecutivo('humnomina_parafiscales','id');
						$sqlr = "INSERT INTO humnomina_parafiscales (id_nom, id_parafiscal, porcentaje, valor, cc, estado, cuentapresu, id, codfun, tipopago, fuente, indicador_producto, bpin, productoservicio, medio_pago, codigo_politicap, tipo_mov, proyecto, seccion_presupuestal) VALUES ('$numnomina', '$tsena', '$porcentaje', '$rowt[23]', '$rowt[34]', 'S', '$cuantapresu','$nuevoid','$rowt[38]', '$rowt[36]', '$fuentepara', '$programaticopara', '$bpimpara', '', 'CSF', '1', '201', '$proyectopara', '$secpresu')";
						mysqli_query($linkbd,$sqlr);
					}
					//ITI
					if($rowt[25] > 0){
						$cuantapresu = buscarcuentanuevocatalogo2($titi,$tipopresupuesto[1],"N/A");
						$porcentaje = itemfuncionarios($rowt[38],'39');
						if($tipopresupuesto[1] != 'IN' && $tipopresupuesto[1] != 'IT'){
							$fuentepara = 0;
							$sql = "SELECT fuente FROM ccpetinicialgastosfun WHERE cuenta = '$cuantapresu' AND seccion_presupuestal = '$secpresu' AND vigencia = '$vigencia'";
							$res = mysqli_query($linkbd,$sql);
							$row = mysqli_fetch_row($res);
							$fuentepara = $row[0];
						}
						$cuentafuente = $secpresu.'<_>'.$cuantapresu.'<_>'.$fuentepara;
						$pfcp[$cuentafuente] += $rowt[25];
						$nuevoid = selconsecutivo('humnomina_parafiscales','id');
						$sqlr = "INSERT INTO humnomina_parafiscales (id_nom, id_parafiscal, porcentaje, valor, cc, estado, cuentapresu, id, codfun, tipopago, fuente, indicador_producto, bpin, productoservicio, medio_pago, codigo_politicap, tipo_mov, proyecto, seccion_presupuestal) VALUES ('$numnomina', '$titi', '$porcentaje', '$rowt[25]', '$rowt[34]', 'S', '$cuantapresu','$nuevoid','$rowt[38]', '$rowt[36]', '$fuentepara', '$programaticopara', '$bpimpara', '', 'CSF', '1', '201', '$proyectopara', '$secpresu')";
						mysqli_query($linkbd,$sqlr);
					}
					//ESAP
					if($rowt[26] > 0){
						$cuantapresu = buscarcuentanuevocatalogo2($tesap,$tipopresupuesto[1],"N/A");
						$porcentaje = itemfuncionarios($rowt[38],'40');
						if($tipopresupuesto[1] != 'IN' && $tipopresupuesto[1] != 'IT'){
							$fuentepara = 0;
							$sql = "SELECT fuente FROM ccpetinicialgastosfun WHERE cuenta = '$cuantapresu' AND seccion_presupuestal = '$secpresu' AND vigencia = '$vigencia'";
							$res = mysqli_query($linkbd,$sql);
							$row = mysqli_fetch_row($res);
							$fuentepara = $row[0];
						}
						$cuentafuente = $secpresu.'<_>'.$cuantapresu.'<_>'.$fuentepara;
						$pfcp[$cuentafuente] += $rowt[26];
						$nuevoid = selconsecutivo('humnomina_parafiscales','id');
						$sqlr = "INSERT INTO humnomina_parafiscales (id_nom, id_parafiscal, porcentaje, valor, cc, estado, cuentapresu, id, codfun, tipopago, fuente, indicador_producto, bpin, productoservicio, medio_pago, codigo_politicap, tipo_mov, proyecto, seccion_presupuestal) VALUES ('$numnomina', '$tesap', '$porcentaje', '$rowt[26]', '$rowt[34]', 'S','$cuantapresu','$nuevoid','$rowt[38]', '$rowt[36]', '$fuentepara', '$programaticopara', '$bpimpara', '', 'CSF', '1', '201', '$proyectopara', '$secpresu')";
						mysqli_query($linkbd,$sqlr);
					}
					//ARL
					if($rowt[30] > 0){
						$cuantapresu = buscarcuentanuevocatalogo2($tarp,$tipopresupuesto[1],"N/A");
						$porcentaje = porcentajearl($rowt[38]);
						if($tipopresupuesto[1] != 'IN' && $tipopresupuesto[1] != 'IT'){
							$fuentepara = 0;
							$sql = "SELECT fuente FROM ccpetinicialgastosfun WHERE cuenta = '$cuantapresu' AND seccion_presupuestal = '$secpresu' AND vigencia = '$vigencia'";
							$res = mysqli_query($linkbd,$sql);
							$row = mysqli_fetch_row($res);
							$fuentepara = $row[0];
						}
						$cuentafuente = $secpresu.'<_>'.$cuantapresu.'<_>'.$fuentepara;
						$pfcp[$cuentafuente] += $rowt[30];
						$nuevoid = selconsecutivo('humnomina_parafiscales','id');
						$sqlr="INSERT INTO humnomina_parafiscales (id_nom, id_parafiscal, porcentaje, valor, cc, estado, cuentapresu, id, codfun, tipopago, fuente, indicador_producto, bpin, productoservicio, medio_pago, codigo_politicap, tipo_mov, proyecto, seccion_presupuestal) VALUES ('$numnomina', '$tarp', '$porcentaje', '$rowt[30]', '$rowt[34]', 'S', '$cuantapresu','$nuevoid','$rowt[38]', '$rowt[36]', '$fuentepara', '$programaticopara', '$bpimpara', '', 'CSF', '1', '201', '$proyectopara', '$secpresu')";
						mysqli_query($linkbd,$sqlr);
					}
					//NETO A PAGAR TIPO PAGO
					if($rowt[9] > 0){
						$cuantapresu = buscarcuentanuevocatalogo1($rowt[36],$tipopresupuesto[0]);
						if($tipopresupuesto[0] != 'IN' && $tipopresupuesto[0] != 'IT'){
							$fuentepara = 0;
							$sql = "SELECT fuente FROM ccpetinicialgastosfun WHERE cuenta = '$cuantapresu' AND seccion_presupuestal = '$secpresu' AND vigencia = '$vigencia'";
							$res = mysqli_query($linkbd,$sql);
							$row = mysqli_fetch_row($res);
							$fuentepara = $row[0];
							$bpimpara = '';
							$programaticopara ='';
							$proyectopara = '';
						}
						else{
							$fuentepara = itemfuncionarios($rowt[38],'44');
							$programaticopara = itemfuncionarios($rowt[38],'43');
							$proyectopara = itemfuncionarios($rowt[38],'31');
							$bpimpara = nombrebpim($proyectopre);
						}
						$cuentafuente = $secpresu.'<_>'.$cuantapresu.'<_>'.$fuentepara;
						$pfcp[$cuentafuente]+=$rowt[9];
					}
				}
				$valindicadorc = array();
				$valbpinc = array();
				$secpresu = array();
				$rubrosp = array();
				$nrubrosp = array();
				$fuente = array();
				$nomproyecto = array();
				$numproyecto = array();
				$vrubrosp = array();
				$vsaldo = array();
				$nproyecto = array();
				foreach($pfcp as $k => $valrubros){
					$datoscuenta = explode('<_>', $k);
					$ncta = nombrecuentapresu($datoscuenta[1]);
					if($valrubros>0){
						$saldo = "";
						if($datoscuenta[4] != ''){
							$nomproyec = nombreproyecto($datoscuenta[4]);
							$numbpim = nombrebpim($datoscuenta[4]);
							$numindicador = $datoscuenta[3];
						}
						else{
							$nomproyec = '';
							$numbpim = '';
							$numindicador = '';
						}
						$valfuente = $valrubros;
						$tipogasto = substr($datoscuenta[1],0,3);
						$mediopago = 'CSF';
						$viggasto = '1';
						$valindicadorc[] = $numindicador;
						$valbpinc[] = $numbpim;
						$secpresu[] = $datoscuenta[0];
						$rubrosp[] = $datoscuenta[1];
						$nrubrosp[] = strtoupper($ncta);
						$fuente[] = $datoscuenta[2];
						$nomproyecto[] = $nomproyec;
						$numproyecto[] = $datoscuenta[4];
						$vrubrosp[] = $valfuente;
						$vsaldo[] = $saldo;
						$nproyecto[] = $saldo;
					}
				}
				$sqld="DELETE FROM humnom_presupuestal WHERE id_nom = '$numnomina'";
				mysqli_query($linkbd,$sqld);
				$conta = count($rubrosp);
				$numnomi = $numnomina;
				for($x = 0; $x < $conta; $x++){
					$idnompresu = selconsecutivo('humnom_presupuestal','id');
					$incuentap = $rubrosp[$x];
					$valorrubros = $vrubrosp[$x];
					$infuentes = $fuente[$x];
					$numproy = $numproyecto[$x];
					$valbpin = $valbpinc[$x];
					$valindicaro = $valindicadorc[$x];
					$valtipogasto = substr($rubrosp[$x],0,3);
					$valsecpresu = $_POST['secpresu'][$x];
					$sqlhum_presu = "INSERT INTO humnom_presupuestal (id_nom, cuenta, valor, estado, fuente, proyecto, medio_pago, id, producto, indicador, bpin, tipo_gasto, seccion_presupuestal, vigencia_gasto, divipola, chip) VALUES ('$numnomi', '$incuentap', '$valorrubros', 'S', '$infuentes', '$numproy', 'CSF', '$idnompresu', '', '$valindicaro', '$valbpin', '$valtipogasto', '$valsecpresu','1','','')";
					mysqli_query($linkbd,$sqlhum_presu);
				}

				$out['insertaBien'] = true;
			}break;
		}
	}

	header("Content-type: application/json");
	echo json_encode($out);
	die();
?>