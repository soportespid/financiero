var app = new Vue({ 
	el: '#myapp',
	data:{
		numnomina:'',
		vcnomina:'',
		loading: false,
		error:'',
		vcambios:0,
		infonomina:[],
		valsaludtotal:[],
		valpensiontotal:[],
		valfondosoli:[],
		valarl:[],
		valccf:[],
		valsena:[],
		valicbf:[],
		valinte:[],
		valesap:[],
	},
	mounted:
	function(){
		this.loading = true;
		this.cargarInformacionFuncionarios();
	},
	methods:{
		cargarInformacionFuncionarios: async function(){
			await axios.post('vue/gestion_humana/hum-ajusteredondeos.php?accion=datosiniciales&numnomina='+this.numnomina)
			.then(
				(response)=>{
					this.infonomina = response.data.datosfuncionarios;
					this.ingresardatosiniciales();
				}
			).catch((error) => {
				this.error = true;
			});
		},
		ingresardatosiniciales: function(){
			for (let i = 0; i < app.infonomina.length; i++){				
				this.valsaludtotal[i] = app.infonomina[i][6];
				this.valpensiontotal[i] = app.infonomina[i][9];
				this.valfondosoli[i] = app.infonomina[i][10];
				this.valarl[i] = app.infonomina[i][11];
				this.valccf[i] = app.infonomina[i][12];
				this.valsena[i] = app.infonomina[i][13];
				this.valicbf[i] = app.infonomina[i][14];
				this.valinte[i] = app.infonomina[i][15];
				this.valesap[i] = app.infonomina[i][16];
			}
			this.loading = false;
		},
		validaringreso: async function(tipo,index){
			switch (tipo){
				case '6':{
					if(this.valsaludtotal[index] != ''){
						const saldo = Math.abs(this.valsaludtotal[index]  - app.infonomina[index][6]);
						if(saldo > 22222200){
							this.valsaludtotal[index] = app.infonomina[index][6];
							this.valsaludtotal = Object.values(this.valsaludtotal);
							Swal.fire(
								'Error!',
								'No se puede ajustar un redondeo mayor a 200',
								'error'
							);
						}
						else{
							const ultdos = this.valsaludtotal[index].substr(-2) ;
							if(ultdos != '00'){
								this.valsaludtotal[index] = app.infonomina[index][6];
								this.valsaludtotal = Object.values(this.valsaludtotal);
								Swal.fire(
									'Error!',
									'El valor debe ser redondeado a centena (debe terminar en 00)',
									'error'
								);
							}
						}
					}
					else{
						this.valsaludtotal[index] = app.infonomina[index][6];
						this.valsaludtotal = Object.values(this.valsaludtotal);
						Swal.fire(
							'Error!',
							'No se puede dejar en blanco el campo, como minimo dejar en 0',
							'error'
						);
					}
				}break;
				case '9':{
					if(this.valpensiontotal[index] != ''){
						const saldo = Math.abs(this.valpensiontotal[index]  - app.infonomina[index][9]);
						if(saldo > 22222200){
							this.valpensiontotal[index] = app.infonomina[index][9];
							this.valpensiontotal = Object.values(this.valpensiontotal);
							Swal.fire(
								'Error!',
								'No se puede ajustar un redondeo mayor a 200',
								'error'
							);
						}
						else{
							const ultdos = this.valpensiontotal[index].substr(-2) ;
							if(ultdos != '00'){
								this.valpensiontotal[index] = app.infonomina[index][9];
								this.valpensiontotal = Object.values(this.valpensiontotal);
								Swal.fire(
									'Error!',
									'El valor debe ser redondeado a centena (debe terminar en 00)',
									'error'
								);
							}
						}
					}
					else{
						this.valpensiontotal[index] = app.infonomina[index][9];
						this.valpensiontotal = Object.values(this.valpensiontotal);
						Swal.fire(
							'Error!',
							'No se puede dejar en blanco el campo, como minimo dejar en 0',
							'error'
						);
					}
				}break;
				case '10':{
					if(this.valfondosoli[index] != ''){
						const saldo = Math.abs(this.valfondosoli[index]  - app.infonomina[index][10]);
						if(saldo > 22222200){
							this.valfondosoli[index] = app.infonomina[index][10];
							this.valfondosoli = Object.values(this.valfondosoli);
							Swal.fire(
								'Error!',
								'No se puede ajustar un redondeo mayor a 200',
								'error'
							);
						}
						else{
							const ultdos = this.valfondosoli[index].substr(-2) ;
							if(ultdos != '00'){
								this.valfondosoli[index] = app.infonomina[index][10];
								this.valfondosoli = Object.values(this.valfondosoli);
								Swal.fire(
									'Error!',
									'El valor debe ser redondeado a centena (debe terminar en 00)',
									'error'
								);
							}
						}
					}
					else{
						this.valfondosoli[index] = app.infonomina[index][10];
						this.valfondosoli = Object.values(this.valfondosoli);
						Swal.fire(
							'Error!',
							'No se puede dejar en blanco el campo, como minimo dejar en 0',
							'error'
						);
					}
				}break;
				case '11':{
					if(this.valarl[index] != ''){
						const saldo = Math.abs(this.valarl[index]  - app.infonomina[index][11]);
						if(saldo > 22222200){
							this.valarl[index] = app.infonomina[index][11];
							this.valarl = Object.values(this.valarl);
							Swal.fire(
								'Error!',
								'No se puede ajustar un redondeo mayor a 200',
								'error'
							);
						}
						else{
							const ultdos = this.valarl[index].substr(-2) ;
							if(ultdos != '00'){
								this.valarl[index] = app.infonomina[index][11];
								this.valarl = Object.values(this.valarl);
								Swal.fire(
									'Error!',
									'El valor debe ser redondeado a centena (debe terminar en 00)',
									'error'
								);
							}
						}
					}
					else{
						this.valarl[index] = app.infonomina[index][11];
						this.valarl = Object.values(this.valarl);
						Swal.fire(
							'Error!',
							'No se puede dejar en blanco el campo, como minimo dejar en 0',
							'error'
						);
					}
				}break;
				case '12':{
					if(this.valccf[index] != ''){
						const saldo = Math.abs(this.valccf[index]  - app.infonomina[index][12]);
						if(saldo > 22222200){
							this.valccf[index] = app.infonomina[index][12];
							this.valccf = Object.values(this.valccf);
							Swal.fire(
								'Error!',
								'No se puede ajustar un redondeo mayor a 200',
								'error'
							);
						}
						else{
							const ultdos = this.valccf[index].substr(-2) ;
							if(ultdos != '00'){
								this.valccf[index] = app.infonomina[index][12];
								this.valccf = Object.values(this.valccf);
								Swal.fire(
									'Error!',
									'El valor debe ser redondeado a centena (debe terminar en 00)',
									'error'
								);
							}
						}
					}
					else{
						this.valccf[index] = app.infonomina[index][12];
						this.valccf = Object.values(this.valccf);
						Swal.fire(
							'Error!',
							'No se puede dejar en blanco el campo, como minimo dejar en 0',
							'error'
						);
					}
				}break;
				case '13':{
					if(this.valsena[index] != ''){
						const saldo = Math.abs(this.valsena[index]  - app.infonomina[index][13]);
						if(saldo > 22222200){
							this.valsena[index] = app.infonomina[index][13];
							this.valsena = Object.values(this.valsena);
							Swal.fire(
								'Error!',
								'No se puede ajustar un redondeo mayor a 200',
								'error'
							);
						}
						else{
							const ultdos = this.valsena[index].substr(-2) ;
							if(ultdos != '00'){
								this.valsena[index] = app.infonomina[index][13];
								this.valsena = Object.values(this.valsena);
								Swal.fire(
									'Error!',
									'El valor debe ser redondeado a centena (debe terminar en 00)',
									'error'
								);
							}
						}
					}
					else{
						this.valsena[index] = app.infonomina[index][13];
						this.valsena = Object.values(this.valsena);
						Swal.fire(
							'Error!',
							'No se puede dejar en blanco el campo, como minimo dejar en 0',
							'error'
						);
					}
				}break;
				case '14':{
					if(this.valicbf[index] != ''){
						const saldo = Math.abs(this.valicbf[index]  - app.infonomina[index][14]);
						if(saldo > 22222200){
							this.valicbf[index] = app.infonomina[index][14];
							this.valicbf = Object.values(this.valicbf);
							Swal.fire(
								'Error!',
								'No se puede ajustar un redondeo mayor a 200',
								'error'
							);
						}
						else{
							const ultdos = this.valicbf[index].substr(-2) ;
							if(ultdos != '00'){
								this.valicbf[index] = app.infonomina[index][14];
								this.valicbf = Object.values(this.valicbf);
								Swal.fire(
									'Error!',
									'El valor debe ser redondeado a centena (debe terminar en 00)',
									'error'
								);
							}
						}
					}
					else{
						this.valicbf[index] = app.infonomina[index][14];
						this.valicbf = Object.values(this.valicbf);
						Swal.fire(
							'Error!',
							'No se puede dejar en blanco el campo, como minimo dejar en 0',
							'error'
						);
					}
				}break;
				case '15':{
					if(this.valinte[index] != ''){
						const saldo = Math.abs(this.valinte[index]  - app.infonomina[index][15]);
						if(saldo > 22222200){
							this.valinte[index] = app.infonomina[index][15];
							this.valinte = Object.values(this.valinte);
							Swal.fire(
								'Error!',
								'No se puede ajustar un redondeo mayor a 200',
								'error'
							);
						}
						else{
							const ultdos = this.valinte[index].substr(-2) ;
							if(ultdos != '00'){
								this.valinte[index] = app.infonomina[index][15];
								this.valinte = Object.values(this.valinte);
								Swal.fire(
									'Error!',
									'El valor debe ser redondeado a centena (debe terminar en 00)',
									'error'
								);
							}
						}
					}
					else{
						this.valinte[index] = app.infonomina[index][15];
						this.valinte = Object.values(this.valinte);
						Swal.fire(
							'Error!',
							'No se puede dejar en blanco el campo, como minimo dejar en 0',
							'error'
						);
					}
				}break;
				case '16':{
					if(this.valesap[index] != ''){
						const saldo = Math.abs(this.valesap[index]  - app.infonomina[index][16]);
						if(saldo > 22222200){
							this.valesap[index] = app.infonomina[index][16];
							this.valesap = Object.values(this.valesap);
							Swal.fire(
								'Error!',
								'No se puede ajustar un redondeo mayor a 200',
								'error'
							);
						}
						else{
							const ultdos = this.valesap[index].substr(-2) ;
							if(ultdos != '00'){
								this.valesap[index] = app.infonomina[index][16];
								this.valesap = Object.values(this.valesap);
								Swal.fire(
									'Error!',
									'El valor debe ser redondeado a centena (debe terminar en 00)',
									'error'
								);
							}
						}
					}
					else{
						this.valesap[index] = app.infonomina[index][16];
						this.valesap = Object.values(this.valesap);
						Swal.fire(
							'Error!',
							'No se puede dejar en blanco el campo, como minimo dejar en 0',
							'error'
						);
					}
				}break;
			}
		},
		validacambios: async function(){
			this.vcambios = 0;
			var xsaldo = 0;
			var formData = new FormData();
			for (let i = 0; i < app.infonomina.length; i++){
				var valsaludfun = app.infonomina[i][4];
				var valsaludemp = app.infonomina[i][5];
				var valpensionfun = app.infonomina[i][7];
				var valpensionemp = app.infonomina[i][8];
				var valtotaldeduc = app.infonomina[i][18];
				var valnetopagar = app.infonomina[i][19];
				var valid = app.infonomina[i][20];
				var concambios = 0;
				if(this.valsaludtotal[i] != app.infonomina[i][6]){
					xsaldo = 0;
					concambios++;
					xsaldo = parseInt(this.valsaludtotal[i]) - parseInt(app.infonomina[i][6]);
					if(app.infonomina[i][5] != 0){valsaludemp = parseInt(valsaludemp) + parseInt(xsaldo);}
					else{
						valsaludfun = parseInt(valsaludfun) + parseInt(xsaldo);
						valtotaldeduc = parseInt(valtotaldeduc) + parseInt(xsaldo);
						valnetopagar = parseInt(valnetopagar) - parseInt(xsaldo);
					}	
				}
				if(this.valpensiontotal[i] != app.infonomina[i][9]){
					xsaldo = 0;
					concambios++;
					xsaldo = parseInt(this.valpensiontotal[i]) - parseInt(app.infonomina[i][9]);
					if(app.infonomina[i][8] != 0){valpensionemp = parseInt(valpensionemp) + parseInt(xsaldo);}
					else{
						valpensionfun = parseInt(valpensionfun) + parseInt(xsaldo);
						valtotaldeduc = parseInt(valtotaldeduc) + parseInt(xsaldo);
						valnetopagar = parseInt(valnetopagar) - parseInt(xsaldo);
					}
				}
				if(this.valfondosoli[i] != app.infonomina[i][10]){
					xsaldo = 0;
					concambios++;
					xsaldo = parseInt(this.valfondosoli[i]) - parseInt(app.infonomina[i][10]);
					valtotaldeduc = parseInt(valtotaldeduc) + parseInt(xsaldo);
					valnetopagar = parseInt(valnetopagar) - parseInt(xsaldo);
				}
				if(this.valarl[i] != app.infonomina[i][11]){concambios++;}
				if(this.valccf[i] != app.infonomina[i][12]){concambios++;}
				if(this.valsena[i] != app.infonomina[i][13]){concambios++;}
				if(this.valicbf[i] != app.infonomina[i][14]){concambios++;}
				if(this.valinte[i] != app.infonomina[i][15]){concambios++;}
				if(this.valesap[i] != app.infonomina[i][16]){concambios++;}
				if(concambios > 0){
					formData.append('vsaludfun[]', valsaludfun);
					formData.append('vsaludemp[]', valsaludemp);
					formData.append('vsaludtotal[]', this.valsaludtotal[i]);
					formData.append('vpensionfun[]', valpensionfun);
					formData.append('vpensionemp[]', valpensionemp);
					formData.append('vpensiontotal[]', this.valpensiontotal[i]);
					formData.append('vfondosoli[]', this.valfondosoli[i]);
					formData.append('varl[]', this.valarl[i]);
					formData.append('vccf[]', this.valccf[i]);
					formData.append('vsena[]', this.valsena[i]);
					formData.append('vicbf[]', this.valicbf[i]);
					formData.append('vinte[]', this.valinte[i]);
					formData.append('vesap[]', this.valesap[i]);
					formData.append('vtotaldeduc[]', valtotaldeduc);
					formData.append('vnetopagar[]', valnetopagar);
					formData.append('vid[]', valid);
					this.vcambios = this.vcambios + concambios;
				}
			}
			if(this.vcambios > 0){
				await axios.post('vue/gestion_humana/hum-ajusteredondeos.php?accion=guardar&numnomina='+this.numnomina, formData)
				.then(
					(response) => {
						if(response.data.insertaBien){
							this.loading = false;
							Swal.fire({
								icon: 'success',
								title: 'Se ha modificado con exito',
								showConfirmButton: false,
								timer: 1500
							})
							this.loading = false;
						}
						else {
							Swal.fire(
								'Error!',
								'No se pudo modificar.',
								'error'
							);
						}
					}
				);
			}
			
		},
		guardar: function(){
			Swal.fire({
				icon: 'question',
				title: 'Seguro que quieres modificar la información?',
				showDenyButton: true,
				confirmButtonText: 'Guardar',
				denyButtonText: 'Cancelar',
			}).then(
				(result) => {
					if (result.isConfirmed) 
					{
						this.loading = true;
						this.validacambios();
					}
					else if (result.isDenied) 
					{
						Swal.fire('No se modifico la información', '', 'info');
					}
				}
			)
        },
		formatonumero: function(valor)
		{
			return new Intl.NumberFormat("en-US", {style: "currency", currency: "USD"}).format(valor);
		},
	},
});