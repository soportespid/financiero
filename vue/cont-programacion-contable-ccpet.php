<?php 
require '../comun.inc';
require '../funciones.inc';


$linkbd = conectar_v7();
$linkbd->set_charset("utf8");
$maxVersion = ultimaVersionGastosCCPET();

$out = array('error' => false);

$action = "show";

if(isset($_GET['action'])){
	$action = $_GET['action'];
}

if($action == 'show'){
    $vigencia = $_GET['vigencia'];
    $unidadEjecutora = $_GET['unidadEjecutora'];
    $medioPago = $_GET['medioPago'];
    $sql = "SELECT codigo, nombre, tipo FROM cuentasccpet WHERE version='$maxVersion' AND municipio=1 ORDER BY id ASC";
    $res = mysqli_query($linkbd,$sql);
    $gastos = array();
    $valorPorCuenta = array();
    $valorPorCuentaNom = array();

    while($row=mysqli_fetch_row($res))
    {
        $sql_cuenta = "SELECT concepto_cont, concepto_nom FROM ccpetcuentas_concepto_contable WHERE estado='S' AND cuenta_p LIKE '%$row[0]%'";
        $res_cuenta = mysqli_query($linkbd, $sql_cuenta);
        $row_cuenta = mysqli_fetch_row($res_cuenta);
        $valorPorCuenta[$row[0]] = $row_cuenta[0];
        $valorPorCuentaNom[$row[0]] = $row_cuenta[1];

        array_push($gastos, $row);
    }
    $out['gastos'] = $gastos;
    $out['valorPorCuenta'] = $valorPorCuenta;
    $out['valorPorCuentaNom'] = $valorPorCuentaNom;
}

if($action == 'buscarClasificadores'){
    $cuenta = $_GET['cuentaIngreso'];
    $cuenta_tipo = substr($cuenta, 0, 3);
    $cuenta_tipo2 = substr($cuenta,0, 5);
    $cuenta_tipo3 = substr($cuenta,0, 8);
    $tipo="";
    if ($cuenta_tipo2=='2.2.1'){
        $tipo='S1';
    }
    elseif($cuenta_tipo2=='2.2.2'){
        $tipo='S2';
    }
    elseif ($cuenta_tipo3=='2.1.1.01') {
        $tipo='F';
    }
    elseif ($cuenta_tipo3=='2.1.1.02') {
        $tipo='';
    }
    elseif($cuenta_tipo2=='2.1.2'){
        $tipo='F2'; 
    }
    elseif($cuenta_tipo2=='2.1.3'){
        $tipo='F3'; 
    }
    elseif($cuenta_tipo2=='2.1.4'){
        $tipo='F4'; 
    }
    elseif($cuenta_tipo2=='2.1.5'){
        $tipo='F5'; 
    }
    elseif($cuenta_tipo2=='2.1.6'){
        $tipo='F6'; 
    }
    elseif($cuenta_tipo2=='2.1.7'){
        $tipo='F7'; 
    }
    elseif($cuenta_tipo2=='2.1.8'){
        $tipo='F8'; 
    }
    elseif($cuenta_tipo3=='2.3.1.01'){
        $tipo='IN';
    }
    elseif($cuenta_tipo3=='2.3.1.02'){
        $tipo='';
    }
    elseif($cuenta_tipo2=='2.3.2'){
        $tipo='I2';
    }
    elseif($cuenta_tipo2=='2.3.3'){
        $tipo='I3';
    }
    elseif($cuenta_tipo2=='2.3.4'){
        $tipo='I4';
    }
    elseif($cuenta_tipo2=='2.3.5'){
        $tipo='I5';
    }
    elseif($cuenta_tipo2=='2.3.6'){
        $tipo='I6';
    }
    elseif($cuenta_tipo2=='2.3.7'){
        $tipo='I7';
    }
    elseif($cuenta_tipo2=='2.3.8'){
        $tipo='I8';
    }
    elseif($cuenta_tipo3=='2.4.1.01')
    {
        $tipo='';
    }
    elseif($cuenta_tipo3=='2.4.1.02')
    {
        $tipo='';
    }
    elseif($cuenta_tipo2=='2.4.5')
    {
        $tipo='G1';
    }
    else{
        $tipo='404';
        array_push($out['insertaBien'], "404 TYPE NOT FOUND", " ");
        array_push($out['insertaBienDebug'], "[guardarTotal] 404 TYPE NOT FOUND");
    }

    $sql1 = "SELECT codigo, nombre, tipo FROM conceptoscontables WHERE tipo = '$tipo' ORDER BY codigo ASC";
    $res1 = mysqli_query($linkbd, $sql1); 
    //echo $sql1;
    //var_dump($sql);
    $codigosDet = array();


    while ($row1 = mysqli_fetch_row($res1)) 
    {
        array_push($codigosDet, $row1);
    }

    $out['codigo'] = $codigosDet;
    
}

if($action == 'buscarClasificadoresNom'){
    $cuenta = $_GET['cuentaIngreso'];
    $cuenta_tipo = substr($cuenta, 0, 3);
    $cuenta_tipo2 = substr($cuenta,0, 5);
    $cuenta_tipo3 = substr($cuenta,0, 8);
    $tipo="";
    if ($cuenta_tipo2=='2.2.1'){
        $tipo='';
    }
    elseif($cuenta_tipo2=='2.2.2'){
        $tipo='';
    }
    elseif ($cuenta_tipo3=='2.1.1.01') {
        $tipo='F';
    }
    elseif ($cuenta_tipo3=='2.1.1.02') {
        $tipo='FT';
    }
    elseif($cuenta_tipo2=='2.1.2'){
        $tipo=''; 
    }
    elseif($cuenta_tipo2=='2.1.3'){
        $tipo='F'; 
    }
    elseif($cuenta_tipo2=='2.1.4'){
        $tipo=''; 
    }
    elseif($cuenta_tipo2=='2.1.5'){
        $tipo=''; 
    }
    elseif($cuenta_tipo2=='2.1.6'){
        $tipo=''; 
    }
    elseif($cuenta_tipo2=='2.1.7'){
        $tipo='F'; 
    }
    elseif($cuenta_tipo2=='2.1.8'){
        $tipo=''; 
    }
    elseif($cuenta_tipo3=='2.3.1.01'){
        $tipo='IN';
    }
    elseif($cuenta_tipo3=='2.3.1.02'){
        $tipo='IT';
    }
    elseif($cuenta_tipo2=='2.3.2'){
        $tipo='';
    }
    elseif($cuenta_tipo2=='2.3.3'){
        $tipo='';
    }
    elseif($cuenta_tipo2=='2.3.4'){
        $tipo='';
    }
    elseif($cuenta_tipo2=='2.3.5'){
        $tipo='';
    }
    elseif($cuenta_tipo2=='2.3.6'){
        $tipo='';
    }
    elseif($cuenta_tipo2=='2.3.7'){
        $tipo='IN';
    }
    elseif($cuenta_tipo2=='2.3.8'){
        $tipo='';
    }
    elseif($cuenta_tipo3=='2.4.1.01')
    {
        $tipo='CP';
    }
    elseif($cuenta_tipo3=='2.4.1.02')
    {
        $tipo='CT';
    }
    elseif($cuenta_tipo2=='2.4.5')
    {
        $tipo='';
    }
    else{
        $tipo='404';
        array_push($out['insertaBien'], "404 TYPE NOT FOUND", " ");
        array_push($out['insertaBienDebug'], "[guardarTotal] 404 TYPE NOT FOUND");
    }

    $sql1 = "SELECT codigo, nombre, tipo FROM conceptoscontables WHERE tipo = '$tipo' ORDER BY codigo ASC";
    $res1 = mysqli_query($linkbd, $sql1); 
    //echo $sql1;
    //var_dump($sql);
    $codigosDet = array();


    while ($row1 = mysqli_fetch_row($res1)) 
    {
        array_push($codigosDet, $row1);
    }

    $out['codigo'] = $codigosDet;
    
}

if($action=='searchFuente'){ 
    $tipoCuenta = $_GET['tipoCuenta'];
    $cuentaTipo1 = substr($tipoCuenta,0, 3);
    $cuentaTipo2 = substr($tipoCuenta,0, 5);
    $cuentaTipo3 = substr($tipoCuenta,0, 8);
    $tipo2="";
    if ($cuentaTipo1=='2.2') {
        $tipo2="SD";
    }
    elseif ($cuentaTipo3=='2.1.1.01') {
        $tipo2="F";
    }
    elseif ($cuentaTipo3=='2.1.1.02') {
        $tipo2="FT";
    }
    elseif($cuentaTipo2=='2.1.2'){
        $tipo2="F2";
    }
    elseif($cuentaTipo2=='2.1.3'){
        $tipo2="F3";
    }
    elseif($cuentaTipo2=='2.1.4'){
        $tipo2="F4";
    }
    elseif($cuentaTipo2=='2.1.5'){
        $tipo2="F5";
    }
    elseif($cuentaTipo2=='2.1.6'){
        $tipo2="F6";
    }
    elseif($cuentaTipo2=='2.1.7'){
        $tipo2="F7";
    }
    elseif($cuentaTipo2=='2.1.8'){
        $tipo2="F8";
    }
    elseif($cuentaTipo3=='2.3.1.01'){
        $tipo2="IN";
    }
    elseif($cuentaTipo3=='2.3.1.02'){
        $tipo2="IT";
    }
    elseif($cuentaTipo2=='2.3.2'){
        $tipo2='I2';
    }
    elseif($cuentaTipo2=='2.3.3'){
        $tipo2='I3';
    }
    elseif($cuentaTipo2=='2.3.4'){
        $tipo2='I4';
    }
    elseif($cuentaTipo2=='2.3.5'){
        $tipo2='I5';
    }
    elseif($cuentaTipo2=='2.3.6'){
        $tipo2='I6';
    }
    elseif($cuentaTipo2=='2.3.7'){
        $tipo2='I7';
    }
    elseif($cuentaTipo2=='2.3.8'){
        $tipo2='I8';
    } 
    else{
        $tipo2="404";
        array_push($out['insertaBien'], "404 TYPE NOT FOUND", " ");
        array_push($out['insertaBienDebug'], "[guardarTotal] 404 TYPE NOT FOUND");
    }

    $keywordFuente=$_POST['keywordFuente'];
    $sql="SELECT * FROM conceptoscontables WHERE nombre LIKE '%$keywordFuente%' AND tipo = '$tipo2' ";
    $res=mysqli_query($linkbd,$sql);//echo $sql;
    $codigosDet = array();

	while($row=mysqli_fetch_row($res))
    {
        //echo $row;
        array_push($codigosDet, $row);
    }

	$out['codigos'] = $codigosDet;
}


if($action == 'guardarValorSolo'){

    $out['insertaBien'] = [];
    $out['insertaBienDebug'] = [];

    $sqlr = "UPDATE ccpetcuentas_concepto_contable SET estado = 'N' WHERE cuenta_p='".$_POST['cuentaPresupuestal']."'";
    mysqli_query($linkbd, $sqlr);//echo $sqlr;

    $now = date("Y-m-d");

    $sql = "INSERT INTO ccpetcuentas_concepto_contable (cuenta_p, concepto_cont, tipo, estado, fecha, concepto_nom) VALUE ('".$_POST['cuentaPresupuestal']."', '".$_POST['conceptoContable']."', '".$_POST['tipo']."', 'S', '".$now."', '".$_POST['nomina']."')";
    if(mysqli_query($linkbd, $sql))
    {
        array_push($out['insertaBien'], "200 OK", " ");
        array_push($out['insertaBienDebug'], "[guardarValorSolo] 200 OK");
    }
    else
    {
        array_push($out['insertaBienDebug'], "[guardarValorSolo] 400 INSERT ERROR");
    }
}

if($action == 'guardarValorSoloNom'){

    $out['insertaBien'] = [];
    $out['insertaBienDebug'] = [];

    $sqlr = "UPDATE ccpetcuentas_concepto_contable SET estado = 'N' WHERE cuenta_p='".$_POST['cuentaPresupuestal']."'";
    mysqli_query($linkbd, $sqlr);//echo $sqlr;

    $now = date("Y-m-d");

    $sql = "INSERT INTO ccpetcuentas_concepto_contable (cuenta_p, concepto_nom, tipo, estado, fecha, concepto_cont) VALUE ('".$_POST['cuentaPresupuestal']."', '".$_POST['conceptoContable']."', '".$_POST['tipo']."', 'S', '".$now."', '".$_POST['general']."')";
    if(mysqli_query($linkbd, $sql))
    {
        array_push($out['insertaBien'], "200 OK", " ");
        array_push($out['insertaBienDebug'], "[guardarValorSolo] 200 OK");
    }
    else
    {
        array_push($out['insertaBienDebug'], "[guardarValorSolo] 400 INSERT ERROR");
    }
}

if($action=='guardarTotal')
{ 

    $conceptoContable = [];
    $out['insertaBien'] = [];
    $out['insertaBienDebug'] = [];

    for ($i = 0; $i < count($_POST['conceptoContable']); $i++)
    {
        $conceptoContable[$i] = preg_split('/(,)/', $_POST['conceptoContable'][$i]);
    }

    if(is_array($conceptoContable))
    {
       for ($i = 0; $i < count($conceptoContable); $i++)
       {
            if(is_array($conceptoContable[$i]))
            {
                $cuentaTipoA = substr($conceptoContable[$i][0], 0, 3);
                $cuentaTipoB = substr($conceptoContable[$i][0], 0, 5);
                $cuentaTipoC = substr($conceptoContable[$i][0], 0, 8);

                $tipoCuentaPresupuestal;

                if ($cuentaTipoA=='2.2') {
                    $tipoCuentaPresupuestal="SD";
                }
                elseif ($cuentaTipoC=='2.1.1.01') {
                    $tipoCuentaPresupuestal="F";
                }
                elseif ($cuentaTipoC=='2.1.1.02') {
                    $tipoCuentaPresupuestal="FT";
                }
                elseif($cuentaTipoB=='2.1.2'){
                    $tipoCuentaPresupuestal="F2";
                }
                elseif($cuentaTipoB=='2.1.3'){
                    $tipoCuentaPresupuestal="F3";
                }
                elseif($cuentaTipoB=='2.1.4'){
                    $tipoCuentaPresupuestal="F4";
                }
                elseif($cuentaTipoB=='2.1.5'){
                    $tipoCuentaPresupuestal="F5";
                }
                elseif($cuentaTipoB=='2.1.6'){
                    $tipoCuentaPresupuestal="F6";
                }
                elseif($cuentaTipoB=='2.1.7'){
                    $tipoCuentaPresupuestal="F7";
                }
                elseif($cuentaTipoB=='2.1.8'){
                    $tipoCuentaPresupuestal="F8";
                }
                elseif($cuentaTipoC=='2.3.1.01'){
                    $tipoCuentaPresupuestal="IN";
                }
                elseif($cuentaTipoC=='2.3.1.02'){
                    $tipoCuentaPresupuestal="IT";
                }
                elseif($cuentaTipoB=='2.3.2'){
                    $tipoCuentaPresupuestal='I2';
                }
                elseif($cuentaTipoB=='2.3.3'){
                    $tipoCuentaPresupuestal='I3';
                }
                elseif($cuentaTipoB=='2.3.4'){
                    $tipoCuentaPresupuestal='I4';
                }
                elseif($cuentaTipoB=='2.3.5'){
                    $tipoCuentaPresupuestal='I5';
                }
                elseif($cuentaTipoB=='2.3.6'){
                    $tipoCuentaPresupuestal='I6';
                }
                elseif($cuentaTipoB=='2.3.7'){
                    $tipoCuentaPresupuestal='I7';
                }
                elseif($cuentaTipoB=='2.3.8'){
                    $tipoCuentaPresupuestal='I8';
                } 
                else{
                    $tipoCuentaPresupuestal="404";
                    array_push($out['insertaBien'], "404 TYPE NOT FOUND", " ");
                    array_push($out['insertaBienDebug'], "[guardarTotal][".$i."] 404 TYPE NOT FOUND");
                }

                if($conceptoContable[$i][2] == 'C')
                {
                    $sqlr = "SELECT concepto_cont FROM ccpetcuentas_concepto_contable WHERE cuenta_p='".$conceptoContable[$i][0]."' AND tipo='".$tipoCuentaPresupuestal."' AND estado='S'";
                    $res = mysqli_query($linkbd, $sqlr);

                    $conceptoContableRow = mysqli_fetch_row($res);

                    $sqlr = "SELECT codigo FROM conceptoscontables WHERE codigo = '".$conceptoContable[$i][1]."' AND tipo = '".$tipoCuentaPresupuestal."'";
                    $res = mysqli_query($linkbd, $sqlr);

                    $codigoRow = mysqli_fetch_row($res);

                    if($codigoRow[0] != null || $codigoRow[0] != "" || is_array($codigoRow) || $conceptoContable[$i][1] == null || $conceptoContable[$i][1] == "")
                    {
                        if($conceptoContableRow[0] != $conceptoContable[$i][1])
                        {
                            $sqlr = "UPDATE ccpetcuentas_concepto_contable SET estado = 'N' WHERE cuenta_p='".$conceptoContable[$i][0]."' AND tipo = '$tipoCuentaPresupuestal'";
                            mysqli_query($linkbd, $sqlr);
                
                            $now = date("Y-m-d");
                
                            $sql = "INSERT INTO ccpetcuentas_concepto_contable (cuenta_p, concepto_cont, tipo, estado, fecha) VALUE ('".$conceptoContable[$i][0]."', '".$conceptoContable[$i][1]."', '".$tipoCuentaPresupuestal."', 'S', '".$now."')";
                            if(mysqli_query($linkbd, $sql))
                            {
                                array_push($out['insertaBien'], "200 OK", " ");
                                array_push($out['insertaBienDebug'], "[guardarTotal][".$i."] 200 OK");
                            } 
                            else
                            {
                                array_push($out['insertaBien'], "400 SQL QUERY ERROR", " ");
                                array_push($out['insertaBienDebug'], "[guardarTotal][".$i."] 400 SQL QUERY ERROR");
                            }
                        }
                        else
                        {
                            array_push($out['insertaBien'], "300 IGNORED", " ");
                            array_push($out['insertaBienDebug'], "[guardarTotal][".$i."] 300 IGNORED");
                        }
                    }
                    else
                    {
                        //array_push($out['insertaBien'], "401 INVALID DATA VALUE ERROR", "El codigo insertado (".$conceptoContable[$i][1].") en la cuenta ".$conceptoContable[$i][0]." no es valido!");
                        array_push($out['insertaBienDebug'], "[guardarTotal][".$i."] 401 INVALID DATA VALUE ERROR");
                    }
                }
                else
                {
                    array_push($out['insertaBien'], "302 INVALID DATA TYPE", " ");
                    array_push($out['insertaBienDebug'], "[guardarTotal][".$i."] 302 INVALID DATA TYPE");
                }
            }
            else
            {
                array_push($out['insertaBien'], "403 DATA COUNT MISMATCH ERROR", " ");
                array_push($out['insertaBienDebug'], "[guardarTotal][".$i."] 403 DATA COUNT MISMATCH ERROR");
            }
        }
    }
    else
    {
        array_push($out['insertarBien'], "402 INVALID DATA STREAM ERROR", " ");
        array_push($out['insertaBienDebug'], "[guardarTotal] 402 INVALID DATA STREAM ERROR");
    }
}



header("Content-type: application/json");
echo json_encode($out);
die();