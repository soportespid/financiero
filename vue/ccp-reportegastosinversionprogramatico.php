<?php 
require '../comun.inc';
$linkbd = conectar_v7();
$linkbd -> set_charset("utf8");

$out = array('error' => false);

$action = "show";

if(isset($_GET['action'])){
	$action=$_GET['action'];
}

function buscarNombreSector($proyecto){
    $linkbd = conectar_v7();
    $sql = "SELECT sector FROM ccpproyectospresupuesto_productos WHERE codproyecto = '$proyecto' GROUP BY codproyecto";
    $res = mysqli_query($linkbd, $sql);
    $row = mysqli_fetch_row($res);
    
    $sqlSector = "SELECT nombre FROM ccpetsectores WHERE codigo='$row[0]'";
    $resSector = mysqli_query($linkbd, $sqlSector);
    $rowSector = mysqli_fetch_row($resSector);

    return $row[0]." - ".$rowSector[0];

}

function buscarNombrePrograma($proyecto){
    $linkbd = conectar_v7();
    $sql = "SELECT programa FROM ccpproyectospresupuesto_productos WHERE codproyecto = '$proyecto' GROUP BY codproyecto";
    $res = mysqli_query($linkbd, $sql);
    $row = mysqli_fetch_row($res);
    
    $sqlPrograma = "SELECT nombre FROM ccpetprogramas WHERE codigo='$row[0]'";
    $resPrograma = mysqli_query($linkbd, $sqlPrograma);
    $rowPrograma = mysqli_fetch_row($resPrograma);

    return $row[0]." - ".$rowPrograma[0];

}

function buscarNombreSubPrograma($proyecto){
    $linkbd = conectar_v7();
    $sql = "SELECT subprograma FROM ccpproyectospresupuesto_productos WHERE codproyecto = '$proyecto' GROUP BY codproyecto";
    $res = mysqli_query($linkbd, $sql);
    $row = mysqli_fetch_row($res);
    
    $sqlSubPrograma = "SELECT nombre_subprograma FROM ccpetprogramas WHERE codigo_subprograma='$row[0]'";
    $resSubPrograma = mysqli_query($linkbd, $sqlSubPrograma);
    $rowSubPrograma = mysqli_fetch_row($resSubPrograma);

    return $row[0]." - ".$rowSubPrograma[0];

}

function buscarNombreProducto($indicadorProducto){

    $linkbd = conectar_v7();

    $sql = "SELECT indicador_producto FROM ccpetproductos WHERE codigo_indicador = '$indicadorProducto'";
    $res = mysqli_query($linkbd, $sql);
    $row = mysqli_fetch_row($res);
    
    return $row[0];

}

function buscarNombreMeta($meta){

    $linkbd = conectar_v7();

    $sql = "SELECT meta_pdm FROM ccpet_metapdm WHERE id_meta = '$meta'";
    $res = mysqli_query($linkbd, $sql);
    $row = mysqli_fetch_row($res);
    
    return $row[0];

}

function buscarSaldoCdp($cuenta, $vigencia = '', $producto = '', $servicio = '', $fuente, $indicador_proucto = '', $medio_pago){

    $linkbd = conectar_v7();

    $productoServicio = '';
    if($servicio != '')
    {
        $productoServicio = $servicio;
    }
    else 
    {
        $productoServicio = $producto;
    }

    if($vigencia == '')
    {
        $vigencia = '2021';
    }

    $sqlr_cdp = "SELECT sum(valor) FROM ccpetcdp_detalle WHERE cuenta='$cuenta' AND productoservicio = ".$productoServicio." AND fuente = '".$fuente."' AND indicador_producto = '".$indicador_proucto."' AND medio_pago='".$medio_pago."'";
    $res_cdp = mysqli_query($linkbd, $sqlr_cdp);
    $row_cdp = mysqli_fetch_array($res_cdp);

    if($row_cdp[0] == NULL)
    {
        $row_cdp[0] = 0;
    }
    
    return round($row_cdp[0],2);

}

function buscarSaldoRp($cuenta, $vigencia = '', $producto = '', $servicio = '', $fuente, $indicador_proucto = '', $medio_pago){

    $linkbd = conectar_v7();

    $productoServicio = '';
    if($servicio != '')
    {
        $productoServicio = $servicio;
    }
    else 
    {
        $productoServicio = $producto;
    }

    if($vigencia == '')
    {
        $vigencia = '2021';
    }

    $sqlr_rp = "SELECT sum(valor) FROM ccpetrp_detalle WHERE cuenta='$cuenta' AND productoservicio = ".$productoServicio." AND fuente = '".$fuente."' AND indicador_producto = '".$indicador_proucto."' AND medio_pago='".$medio_pago."'";
    $res_rp = mysqli_query($linkbd, $sqlr_rp);
    $row_rp = mysqli_fetch_array($res_rp);
    
    if($row_rp[0] == NULL)
    {
        $row_rp[0] = 0;
    }

    return round($row_rp[0],2);

}

function buscarSaldoCxp($cuenta, $vigencia = '', $producto = '', $servicio = '', $fuente, $indicador_proucto = '', $medio_pago){

    $linkbd = conectar_v7();

    $productoServicio = '';
    if($servicio != '')
    {
        $productoServicio = $servicio;
    }
    else 
    {
        $productoServicio = $producto;
    }

    if($vigencia == '')
    {
        $vigencia = '2021';
    }

    $sqlr_cxp = "SELECT sum(valor) FROM tesoordenpago_det WHERE cuentap='$cuenta' AND productoservicio = ".$productoServicio." AND fuente = '".$fuente."' AND vigencia='$vigencia' AND indicador_producto = '".$indicador_proucto."' AND medio_pago='".$medio_pago."'";
    $res_cxp = mysqli_query($linkbd, $sqlr_cxp);
    $row_cxp = mysqli_fetch_array($res_cxp);
    
    if($row_cxp[0] == NULL)
    {
        $row_cxp[0] = 0;
    }

    return round($row_cxp[0],2);

}

function buscarSaldoEgreso($cuenta, $vigencia = '', $producto = '', $servicio = '', $fuente, $indicador_proucto = '', $medio_pago){

    $linkbd = conectar_v7();

    $productoServicio = '';
    if($servicio != '')
    {
        $productoServicio = $servicio;
    }
    else 
    {
        $productoServicio = $producto;
    }

    if($vigencia == '')
    {
        $vigencia = '2021';
    }

    $sqlr_egreso = "SELECT sum(valor) FROM tesoordenpago T, tesoordenpago_det TD, tesoegresos TE WHERE TE.id_orden=T.id_orden AND T.id_orden=TD.id_orden AND TD.cuentap = '$cuenta' AND TE.vigencia='$vigencia' AND TD.vigencia='$vigencia' AND  NOT(TE.estado='N') AND TD.valor >0 AND TE.tipo_mov='201' AND TD.productoservicio = ".$productoServicio." AND TD.fuente = '".$fuente."' AND TD.indicador_producto = '".$indicador_proucto."' AND TD.medio_pago='".$medio_pago."'";
    $res_egreso = mysqli_query($linkbd, $sqlr_egreso);
    $row_egreso = mysqli_fetch_row($res_egreso);
    
    if($row_egreso[0] == NULL)
    {
        $row_egreso[0] = 0;
    }

    return round($row_egreso[0],2);

}

if($action=='show'){

    $sql = "SELECT id, nombre FROM ccpproyectospresupuesto WHERE estado='S'";
    $res = mysqli_query($linkbd, $sql);
    $proyectos = array();
    
    while($row=mysqli_fetch_row($res))
    {
        $nombreSector = buscarNombreSector($row[0]);
        $nombrePrograma = buscarNombrePrograma($row[0]);
        $nombreSubPrograma = buscarNombreSubPrograma($row[0]);
        array_push($row, $nombreSector);
        array_push($row, $nombrePrograma);
        array_push($row, $nombreSubPrograma);

        array_push($proyectos, $row);
    }

	$out['proyectos'] = $proyectos;
}

if($action == 'proyectoProgramatico'){

    //var_dump($_POST);
    $cont = count($_POST["proyectos"]);
    
    $proyectos_det_indicador = array();
    for($i = 0; $i < $cont; $i++){

        $cuentas_separadas = explode(",", $_POST["proyectos"][$i]);

        $sql = "SELECT codproyecto, id_fuente, medio_pago, rubro, subclase, subproducto, valorcsf, valorssf, indicador_producto, metas FROM ccpproyectospresupuesto_presupuesto WHERE codproyecto='".$cuentas_separadas[0]."'";
        $res = mysqli_query($linkbd, $sql);
        $proyectos_det = array();

        while($row=mysqli_fetch_row($res))
        {
            $producto = substr($row[8], 0, 7);

            $nombreProducto = buscarNombreProducto($row[8]);

            $nombreMeta = buscarNombreMeta($row[9]);
            

            array_push($row, $producto);

            array_push($row, $nombreProducto);

            array_push($row, $nombreMeta);

            

            $medioPago = '';
            if($row[0] > 0)
            {
                $medioPago = 'CSF';
            }
            else
            {
                $medioPago = 'SSF';
            }

            $saldoCdp = buscarSaldoCdp($row[3],'', $row[4], $row[5], $row[1], $row[8], $medioPago);

            array_push($row, $saldoCdp);

            $saldoRp = buscarSaldoRp($row[3],'', $row[4], $row[5], $row[1], $row[8], $medioPago);

            array_push($row, $saldoRp);

            $saldoCxp = buscarSaldoCxp($row[3],'', $row[4], $row[5], $row[1], $row[8], $medioPago);

            array_push($row, $saldoCxp);

            $compromisos = $saldoRp - $saldoCxp;

            array_push($row, round($compromisos, 2));

            $saldoEgreso = buscarSaldoEgreso($row[3],'', $row[4], $row[5], $row[1], $row[8], $medioPago);

            array_push($row, $saldoEgreso);

            $cxp = $saldoCxp - $saldoEgreso;

            array_push($row, $cxp);

            $saldo = $row[6] + $row[7] - $saldoCdp;

            array_push($row, round($saldo, 2));

            array_push($proyectos_det, $row);
        }

        array_push($proyectos_det_indicador, $proyectos_det);
    }

	$out['proyectos_det_indicador'] = $proyectos_det_indicador;

}

if($action=='buscarSector'){

    $rest = substr($_GET['cod_sector'], 0, 2);
    
    $sql = "SELECT nombre FROM ccpetsectores WHERE codigo='$rest'";
    $res = mysqli_query($linkbd, $sql);
    $row=mysqli_fetch_row($res);

	$out['nombre_sector'] = $row[0];

}

header("Content-type: application/json");
echo json_encode($out);
die();