<?php 
require '../comun.inc';
require '../funciones.inc';

$linkbd = conectar_v7();
$linkbd -> set_charset("utf8");
$maxVersion = ultimaVersionIngresosCCPET();

$out = array('error' => false);

$action = "show";

if(isset($_GET['action'])){
	$action = $_GET['action'];
}

if($action == 'show'){
    

    $vigencia = $_GET['vigencia'];
    $seccionPresupuestal = $_GET['seccionPresupuestal'];
    $vigenciaDelGasto = $_GET['vigenciaDelGasto'];
    $medioPago = $_GET['medioPago'];

    $sql = "WITH RECURSIVE jerarquia AS (
                SELECT 
                    IF(t1.codigo_auxiliar IS NULL OR t1.codigo_auxiliar = '', t1.codigo, CONCAT(t1.codigo, '-', t1.codigo_auxiliar)) AS codigoP, 
                    IF(t1.codigo_auxiliar IS NULL OR t1.codigo_auxiliar = '', t1.nombre, CONCAT(t1.nombre, '-', t1.nombre_auxiliar)) AS nombreP, 
                    t1.tipo, t1.padre, t1.nivel, t1.version, t1.municipio, t1.departamento, t1.codigo_auxiliar, t1.nombre_auxiliar
                FROM cuentasingresosccpet t1
                INNER JOIN cuentasingresosccpetseleccionadas t2 ON t1.codigo = t2.codigo
                WHERE t1.version = $maxVersion

                UNION ALL

                SELECT 
                    IF(t1.codigo_auxiliar IS NULL OR t1.codigo_auxiliar = '', t1.codigo, CONCAT(t1.codigo, '-', t1.codigo_auxiliar)) AS codigoP, 
                    IF(t1.codigo_auxiliar IS NULL OR t1.codigo_auxiliar = '', t1.nombre, CONCAT(t1.nombre, '-', t1.nombre_auxiliar)) AS nombreP,
                    t1.tipo, t1.padre, t1.nivel, t1.version, t1.municipio, t1.departamento, t1.codigo_auxiliar, t1.nombre_auxiliar
                FROM cuentasingresosccpet t1
                INNER JOIN jerarquia j ON t1.codigo = j.padre
                WHERE t1.version = $maxVersion
            )
            SELECT DISTINCT *
            FROM jerarquia
            ORDER BY codigoP";
    //echo $sql;
    //$sql = "SELECT codigo, nombre, tipo FROM cuentasingresosccpet WHERE municipio=1 AND version = '$maxVersion' ORDER BY id ASC";
    $res=mysqli_query($linkbd,$sql);
    $ingresos = array();
    $valorPorCuenta = array();
    while($row=mysqli_fetch_row($res))
    {
        $sql_cuenta = "SELECT sum(valor) FROM ccpetinicialing WHERE cuenta LIKE '$row[0]%' AND vigencia = '$vigencia' AND  seccion_presupuestal = '$seccionPresupuestal' AND vigencia_gasto = '$vigenciaDelGasto' AND medio_pago = '$medioPago'";

        $res_cuenta = mysqli_query($linkbd, $sql_cuenta);
        $row_cuneta = mysqli_fetch_row($res_cuenta);
        $valorPorCuenta[$row[0]] = $row_cuneta[0];

        array_push($ingresos, $row);
    }
    $out['ingresos'] = $ingresos;
    $out['valorPorCuenta'] = $valorPorCuenta;
}

if($action == 'search'){
    $keyword = $_POST['keyword'];
    $sql = "SELECT * FROM cuentasingresosccpet_cab WHERE concat_ws(' ', nombre) LIKE '%$keyword%'";
    $res = mysqli_query($linkbd, $sql);
    $ingresosBuscados = array();

    while ($row = mysqli_fetch_row($res)) {
        array_push($ingresosBuscados, $row);
    }

    $out['ingresosBuscados'] = $ingresosBuscados;
}

if($action == 'buscarClasificadores'){
    $cuenta = $_GET['cuentaIngreso'];
    $sql = "SELECT clasificadores FROM ccpetprogramarclasificadores WHERE cuenta = '$cuenta'";
    $res = mysqli_query($linkbd, $sql);
    $clasificadores = array();

    while ($row = mysqli_fetch_row($res)) {
        array_push($clasificadores, $row);
    }

    $out['clasificadores'] = $clasificadores;
}

if($action=='searchValorSolo'){
    
    $cuentaAgr = $_GET['cuentaAgr'];
    $unidadEjecutora = $_GET['unidadEjecutora'];
    $fuente = $_GET['fuente'];
    $medioPago = $_GET['medioPago'];
    $vigencia = $_GET['vigencia'];
    $vigenciaDelGasto = $_GET['vigenciaDelGasto'];
        
    $sql = "SELECT valor FROM ccpetinicialing WHERE cuenta = '$cuentaAgr' AND seccion_presupuestal = '$unidadEjecutora' AND fuente='$fuente' AND vigencia_gasto = '$vigenciaDelGasto' AND medio_pago = '$medioPago' AND vigencia = '$vigencia' ";

   	// $sql="SELECT * FROM ccpet_cuin WHERE (nit like '%$keyword%' || nombre like '%$keyword%') AND version=(SELECT MAX(version) FROM ccpet_cuin )";
	/* $sql = "SELECT valor FROM ccpetinicialvaloringresos WHERE cuenta = '$cuentaAgr' AND unidad = '$unidadEjecutora' AND fuente='$fuente' AND medioPago = '$medioPago' AND vigencia = '$vigencia'"; */ //echo $sql;
    $res = mysqli_query($linkbd,$sql);
    $row = mysqli_fetch_row($res);
	$out['valor'] = $row[0];
}

if($action=='cuin'){
    $sql_count = "SELECT count(id) FROM ccpet_cuin WHERE version=(SELECT MAX(version) FROM ccpet_cuin)";
    $res_count = mysqli_query($linkbd,$sql_count);
    $row_count = mysqli_fetch_row($res_count);

    $sql="SELECT * FROM ccpet_cuin WHERE version=(SELECT MAX(version) FROM ccpet_cuin) LIMIT 0, 1000";
    $res=mysqli_query($linkbd,$sql);
    $codigos = array();

    while($row=mysqli_fetch_row($res))
    {
        array_push($codigos, $row);
    }

    $out['codigos'] = $codigos;
    $out['codigos_count'] = $row_count[0];
    
}

if($action=='clasificador'){

    $clasificador = $_GET['clas'];
    $sql="SELECT PR.codigo, PR.nombre, PR.tipo FROM cuentasingresosccpet AS PR, cuentasingresosccpet_det AS DET WHERE PR.id = DET.id_cuentasingreso AND DET.id_clasificador = $clasificador AND PR.version = '$maxVersion' ORDER BY PR.codigo ASC";
    $res=mysqli_query($linkbd,$sql);
    $codigos = array();

    while($row=mysqli_fetch_row($res))
    {
        array_push($codigos, $row);
    }

    $out['codigos'] = $codigos;    
}

if($action=='guardarValorSolo'){

    $destEspecifica = $_POST['destEspecifica'];
    $tiponorma = $_POST['tiponorma'];
    $numeroNorma = $_POST['numeroNorma'];
    $txtFecha = $_POST['txtFecha'];

    

    if($destEspecifica == 'SI'){
        $fechaPartes = explode("/", $txtFecha);
        $fechaDE = $fechaPartes[2]."-".$fechaPartes[1]."-".$fechaPartes[0];
        $sqlrDE = "DELETE FROM ccpet_destinoespecifico WHERE cuenta = '".$_POST['cuentaPresupuestal']."' AND fuente = '".$_POST['fuente']."' AND seccion = '".$_POST['unidadEjecutora']."'";
        mysqli_query($linkbd, $sqlrDE);
        if($_POST['valorSolo'] >= 0){
            $sqlrDeI = "INSERT INTO ccpet_destinoespecifico(cuenta, fuente, seccion, tiponorma, numeronorma,fechanorma) VALUES ('".$_POST['cuentaPresupuestal']."', '".$_POST['fuente']."', '".$_POST['unidadEjecutora']."', '$tiponorma', '$numeroNorma', '$txtFecha')";
            mysqli_query($linkbd, $sqlrDeI);
        }

    }


    $detalSectorial = $_POST['detSectorial'];
    $sqlrDS = "DELETE FROM ccpet_detallesectorial WHERE cuenta = '".$_POST['cuentaPresupuestal']."' AND fuente = '".$_POST['fuente']."' AND seccion = '".$_POST['unidadEjecutora']."'";
    mysqli_query($linkbd, $sqlrDS);

    $producto_serv =  $_POST['producto_servicio'] == 'undefined' ? '' : " AND producto_servicio = '".$_POST['producto_servicio']."'";

    $producto_serv_in =  $_POST['producto_servicio'] == 'undefined' ? "''" : "'".$_POST['producto_servicio']."'";

    $sql = "DELETE FROM ccpetinicialing WHERE cuenta = '".$_POST['cuentaPresupuestal']."' AND seccion_presupuestal = '".$_POST['unidadEjecutora']."' AND fuente='".$_POST['fuente']."' AND medio_pago = '".$_POST['medioPago']."' AND vigencia_gasto = '".$_POST['vigenciaDelGasto']."' AND vigencia = '".$_POST['vigencia']."' $producto_serv ";
        mysqli_query($linkbd, $sql);
    
        if($_POST['valorSolo'] >= 0){

            $sqlDsI = "INSERT INTO ccpet_detallesectorial(cuenta, fuente, seccion, detalle_sectorial) VALUES ('".$_POST['cuentaPresupuestal']."', '".$_POST['fuente']."', '".$_POST['unidadEjecutora']."', '$detalSectorial')";
            mysqli_query($linkbd, $sqlDsI);
            
            $sql = "INSERT INTO ccpetinicialing (cuenta, fuente, seccion_presupuestal, vigencia_gasto, medio_pago, vigencia, valor, finalizar, producto_servicio, cuenta_cuin) VALUES ('".$_POST['cuentaPresupuestal']."', '".$_POST['fuente']."', '".$_POST['unidadEjecutora']."', '".$_POST['vigenciaDelGasto']."', '".$_POST['medioPago']."', '".$_POST['vigencia']."', '".$_POST['valorSolo']."', 'N', $producto_serv_in, '".$_POST['cuenta_cuin']."')";
    
            //$sql="INSERT INTO  ccpetinicialvalorgastos (cuenta, valor, unidad, fuente, medioPago, vigencia) VALUES ('".$_POST['cuentaPresupuestal']."', '".$_POST['valorSolo']."', '".$_POST['unidadEjecutora']."', '".$_POST['fuente']."', '".$_POST['medioPago']."', '".$_POST['vigencia']."')";
            if(mysqli_query($linkbd, $sql))
            {
                $out['insertaBien'] = true; 
            }
            else
            {
                $out['insertaBien'] = false; 
            }
        }else{
            $out['insertaBien'] = true; 
        }


    /* $sql = "DELETE FROM ccpetinicialingresos WHERE cuenta = '".$_POST['cuentaPresupuestal']."' AND unidad = '".$_POST['unidadEjecutora']."' AND fuente='".$_POST['fuente']."' AND medioPago = '".$_POST['medioPago']."' AND vigencia = '".$_POST['vigencia']."'";
    mysqli_query($linkbd, $sql);

    
    $sql = "DELETE FROM ccpetinicialvaloringresos WHERE cuenta = '".$_POST['cuentaPresupuestal']."' AND unidad = '".$_POST['unidadEjecutora']."' AND fuente='".$_POST['fuente']."' AND medioPago = '".$_POST['medioPago']."' AND vigencia = '".$_POST['vigencia']."'";
    mysqli_query($linkbd, $sql);

    if($_POST['valorSolo'] > 0){
        $sql="INSERT INTO  ccpetinicialingresos (cuenta, valor_total, unidad, fuente, medioPago, vigencia) VALUES ('".$_POST['cuentaPresupuestal']."', '".$_POST['valorSolo']."', '".$_POST['unidadEjecutora']."', '".$_POST['fuente']."', '".$_POST['medioPago']."', '".$_POST['vigencia']."')";
        mysqli_query($linkbd, $sql);


        $sql="INSERT INTO  ccpetinicialvaloringresos (cuenta, valor, unidad, fuente, medioPago, vigencia) VALUES ('".$_POST['cuentaPresupuestal']."', '".$_POST['valorSolo']."', '".$_POST['unidadEjecutora']."', '".$_POST['fuente']."', '".$_POST['medioPago']."', '".$_POST['vigencia']."')";
        if(mysqli_query($linkbd,$sql))
        {
            $out['insertaBien'] = true; 
        }
        else
        {
            $out['insertaBien'] = false; 
        }
    }else{
        $out['insertaBien'] = true; 
    } */
    
    
}

if($action=='guardarCuin'){ 

    //var_dump($_POST);
    

    $sql = "DELETE FROM ccpetinicialingresos WHERE cuenta = '".$_POST['cuentaPresupuestal']."' AND unidad = '".$_POST['unidadEjecutora']."' AND fuente='".$_POST['fuente']."' AND medioPago = '".$_POST['medioPago']."' AND vigencia = '".$_POST['vigencia']."'";
    mysqli_query($linkbd, $sql);

    $sql = "DELETE FROM ccpetinicialingresoscuin WHERE cuenta = '".$_POST['cuentaPresupuestal']."' AND unidad = '".$_POST['unidadEjecutora']."' AND fuente='".$_POST['fuente']."' AND medioPago = '".$_POST['medioPago']."'";
    mysqli_query($linkbd, $sql);

    if($_POST['valorTotal'] > 0){
        $sql="INSERT INTO  ccpetinicialingresos (cuenta, valor_total, unidad, fuente, medioPago, vigencia) VALUES ('".$_POST['cuentaPresupuestal']."', '".$_POST['valorTotal']."', '".$_POST['unidadEjecutora']."', '".$_POST['fuente']."', '".$_POST['medioPago']."', '".$_POST['vigencia']."')";
        if(mysqli_query($linkbd, $sql)){
            //var_dump($_POST['cuentasCuin']);
            for($x = 0; $x < count($_POST['cuentasCuin']); $x++){

                $cuentas_separadas = explode(",", $_POST['cuentasCuin'][$x]);
                //var_dump($cuentas_separadas);
                
                //echo $x;
                //echo $_POST['cuentasCuin'][$x];
                $sql = "INSERT INTO  ccpetinicialingresoscuin (cuenta, cuin, valor, unidad, fuente, medioPago, vigencia) VALUES ('".$_POST['cuentaPresupuestal']."', '".$cuentas_separadas[13]."', '".$cuentas_separadas[15]."', '".$_POST['unidadEjecutora']."', '".$_POST['fuente']."', '".$_POST['medioPago']."', '".$_POST['vigencia']."')";

                if(mysqli_query($linkbd,$sql))
                {
                    $out['insertaBien'] = true; 
                }
                else
                {
                    $out['insertaBien'] = false; 
                }
            }
        }
    }else{
        $out['insertaBien'] = true; 
    }
    

    /*$sql="SELECT * FROM ccpet_cuin WHERE version=(SELECT MAX(version) FROM ccpet_cuin) LIMIT 0, 1000";
    $res=mysqli_query($linkbd,$sql);
    $codigos = array();

    while($row=mysqli_fetch_row($res))
    {
        array_push($codigos, $row);
    }

    $out['codigos'] = $codigos;
    $out['codigos_count'] = $row_count[0];*/
    
}

if($action=='guardarClasificador'){

    $sql = "DELETE FROM ccpetinicialingresos WHERE cuenta = '".$_POST['cuentaPresupuestal']."' AND unidad = '".$_POST['unidadEjecutora']."' AND fuente='".$_POST['fuente']."' AND medioPago = '".$_POST['medioPago']."' AND vigencia = '".$_POST['vigencia']."'";
    mysqli_query($linkbd, $sql);

    $sql = "DELETE FROM ccpetinicialingresosclasificador WHERE cuenta = '".$_POST['cuentaPresupuestal']."' AND unidad = '".$_POST['unidadEjecutora']."' AND fuente='".$_POST['fuente']."' AND medioPago = '".$_POST['medioPago']."' AND id_clasificador = '".$_POST['id_clasificador']."'";
    mysqli_query($linkbd, $sql);

    if($_POST['valorTotalClasificador'] > 0){
        $sql="INSERT INTO  ccpetinicialingresos (cuenta, valor_total, unidad, fuente, medioPago, vigencia) VALUES ('".$_POST['cuentaPresupuestal']."', '".$_POST['valorTotalClasificador']."', '".$_POST['unidadEjecutora']."', '".$_POST['fuente']."', '".$_POST['medioPago']."', '".$_POST['vigencia']."')";
        if(mysqli_query($linkbd, $sql)){
            //var_dump($_POST['cuentasCuin']);
            for($x = 0; $x < count($_POST['cuentasClasificador']); $x++){

                $cuentas_separadas = explode(",", $_POST['cuentasClasificador'][$x]);
                //var_dump($cuentas_separadas);
                
                //echo $x;
                //echo $_POST['cuentasCuin'][$x];
                $sql = "INSERT INTO  ccpetinicialingresosclasificador (cuenta, cuenta_clasificador, valor, unidad, fuente, medioPago, vigencia, id_clasificador) VALUES ('".$_POST['cuentaPresupuestal']."', '".$cuentas_separadas[0]."', '".$cuentas_separadas[3]."', '".$_POST['unidadEjecutora']."', '".$_POST['fuente']."', '".$_POST['medioPago']."', '".$_POST['vigencia']."', '".$_POST['id_clasificador']."')";

                if(mysqli_query($linkbd,$sql))
                {
                    $out['insertaBien'] = true; 
                }
                else
                {
                    $out['insertaBien'] = false; 
                }
            }
        }
    }else{
        $out['insertaBien'] = true; 
    }
    
}

if($action == "searchCuentaAgr"){
    $cuentaAgr = $_GET['cuentaAgr'];
    $unidadEjecutora = $_GET['unidadEjecutora'];
    $fuente = $_GET['fuente'];
    $medioPago = $_GET['medioPago'];
    $vigencia = $_GET['vigencia'];
    $sql = "SELECT subclase, valor FROM ccpetinicialingresosbienestransportables WHERE cuenta = '$cuentaAgr' AND unidad = '$unidadEjecutora' AND fuente = '$fuente' AND medioPago = '$medioPago' AND vigencia = '$vigencia'";
    $res = mysqli_query($linkbd, $sql);//echo $sql;
    $codigos = array();
    $codigosEliminar = array();
    $valorTotal = 0;

    while($row = mysqli_fetch_row($res))
    {
        $sql_bienes = "SELECT grupo, titulo, ciiu, sistema_armonizado, cpc FROM ccpetbienestransportables WHERE grupo = '$row[0]'";
        $res_bienes = mysqli_query($linkbd, $sql_bienes);
        $row_bienes = mysqli_fetch_row($res_bienes);

        //array_push($codigosEliminar, $row_bienes);

        array_push($row_bienes, $row[1]);

        array_push($codigos, $row_bienes);

        $valorTotal += $row[1];
    }
    $out['codigos'] = $codigos;
    //$out['codigosEliminar'] = $codigosEliminar;
    $out['valorTotal'] = $valorTotal;
}

if($action == "searchCuentaServiciosAgr"){
    $cuentaAgr = $_GET['cuentaAgr'];
    $unidadEjecutora = $_GET['unidadEjecutora'];
    $fuente = $_GET['fuente'];
    $medioPago = $_GET['medioPago'];
    $vigencia = $_GET['vigencia'];
    $sql = "SELECT subclase, valor FROM ccpetinicialserviciosingresos WHERE cuenta = '$cuentaAgr' AND unidad = '$unidadEjecutora' AND fuente = '$fuente' AND medioPago = '$medioPago' AND vigencia = '$vigencia'";
    $res = mysqli_query($linkbd, $sql);
    $codigos = array();
    $codigosEliminar = array();
    $valorTotalServicios = 0;

    while($row = mysqli_fetch_row($res))
    {
        $sql_bienes = "SELECT grupo, titulo, ciiu, cpc FROM ccpetservicios WHERE grupo = '$row[0]'";
        $res_bienes = mysqli_query($linkbd, $sql_bienes);
        $row_bienes = mysqli_fetch_row($res_bienes);

        //array_push($codigosEliminar, $row_bienes);

        array_push($row_bienes, $row[1]);

        array_push($codigos, $row_bienes);

        $valorTotalServicios += $row[1];
    }
    $out['codigos'] = $codigos;
    //$out['codigosEliminar'] = $codigosEliminar;
    $out['valorTotalServicios'] = $valorTotalServicios;
}

if($action=='searchFuente'){ 
    $keywordFuente=$_POST['keywordFuente'];
   	// $sql="SELECT * FROM ccpet_cuin WHERE (nit like '%$keyword%' || nombre like '%$keyword%') AND version=(SELECT MAX(version) FROM ccpet_cuin )";
	$sql="SELECT * FROM ccpet_fuentes_cuipo WHERE concat_ws(' ', codigo_fuente, nombre) LIKE '%$keywordFuente%' AND version=(SELECT MAX(version) FROM ccpet_fuentes_cuipo )";
    $res=mysqli_query($linkbd,$sql);
    $codigos = array();

	while($row=mysqli_fetch_row($res))
    {
        array_push($codigos, $row);
    }

	$out['codigos'] = $codigos;
}

if($action=='varlorFuente'){
    $cuentaAgr = $_POST['cuentaPresupuestal'];
    $unidadEjecutora = $_POST['unidadEjecutora'];
    $vigenciaDelGasto = $_POST['vigenciaDelGasto'];
    $medioPago = $_POST['medioPago'];
    $vigencia = $_POST['vigencia'];
    //$fuente = $_GET['fuente'];
    $valor = array();
    
    $sql = "SELECT SUM(valor), fuente FROM ccpetinicialing WHERE cuenta = '$cuentaAgr' AND seccion_presupuestal = '$unidadEjecutora' AND vigencia_gasto = '$vigenciaDelGasto' AND medio_pago = '$medioPago' AND vigencia = '$vigencia' GROUP BY fuente";
    $res = mysqli_query($linkbd,$sql);
    while($row = mysqli_fetch_row($res)){
        $valor[$row[1]] = $row[0];
    }

    $out['valor'] = $valor;
}

if($action=='productoServicio'){
    $cuentaAgr = $_POST['cuentaPresupuestal'];
    $unidadEjecutora = $_POST['unidadEjecutora'];
    $vigenciaDelGasto = $_POST['vigenciaDelGasto'];
    $medioPago = $_POST['medioPago'];
    $vigencia = $_POST['vigencia'];
    $fuente = $_POST['fuente'];
    $valor = array();
    
    $sql = "SELECT SUM(valor), producto_servicio, fuente FROM ccpetinicialing WHERE cuenta = '$cuentaAgr' AND seccion_presupuestal = '$unidadEjecutora' AND vigencia_gasto = '$vigenciaDelGasto' AND medio_pago = '$medioPago' AND vigencia = '$vigencia' AND fuente = '$fuente' GROUP BY producto_servicio";
    $res = mysqli_query($linkbd,$sql);
    while($row = mysqli_fetch_row($res)){
        $valor[$row[1]."".$row[2]] = $row[0];
    }
    $out['valor'] = $valor;

}

if($action == 'buscarDetalleSectorial'){
    $rubro = $_GET['rubro'];
    $fuente = $_GET['fuente'];
    $seccion = $_GET['seccion'];

    $sql1 = "SELECT detalle_sectorial FROM ccpet_detallesectorial WHERE cuenta = '$rubro' AND fuente = '$fuente' AND seccion = '$seccion'";
    $res1 = mysqli_query($linkbd, $sql1);
    $banNuevo1 = mysqli_num_rows($res1);
    $row1 = mysqli_fetch_row($res1);
    if($banNuevo1 > 0){
        $detSectorial = $row1[0];
    }else{
        $detSectorial = 0;
    }

    $destinacionesEspecificas = [];
    $sql2 = "SELECT tiponorma, numeronorma,fechanorma FROM ccpet_destinoespecifico WHERE cuenta = '$rubro' AND fuente = '$fuente' AND seccion = '$seccion'";
    $res2 = mysqli_query($linkbd, $sql2);
    $banNuevo2 = mysqli_num_rows($res2);
    while($row2 = mysqli_fetch_row($res2)){
        
        array_push($destinacionesEspecificas, $row2);
    }
    $out['destinacionesEspecificas'] = $destinacionesEspecificas;
    $out['detSectorial'] = $detSectorial;
}

if($action=='varlorFuenteSSF'){ 
    $cuentaAgr = $_POST['cuentaPresupuestal'];
    $unidadEjecutora = $_POST['unidadEjecutora'];
    $vigencia = $_POST['vigencia'];
    //$fuente = $_GET['fuente'];
    $medioPago = 'SSF';
    $valor = array();
   	// $sql="SELECT * FROM ccpet_cuin WHERE (nit like '%$keyword%' || nombre like '%$keyword%') AND version=(SELECT MAX(version) FROM ccpet_cuin )";
	$sql = "SELECT SUM(valor), fuente FROM ccpetinicialvaloringresos WHERE cuenta = '$cuentaAgr' AND unidad = '$unidadEjecutora' AND medioPago = '$medioPago' AND vigencia = '$vigencia' GROUP BY fuente"; //echo $sql;
    $res = mysqli_query($linkbd,$sql);
    while($row = mysqli_fetch_row($res)){
        $valor[$row[1]] = $row[0];
    }
    
    
    $sql = "SELECT SUM(valor), fuente FROM ccpetinicialserviciosingresos WHERE cuenta = '$cuentaAgr' AND unidad = '$unidadEjecutora' AND medioPago = '$medioPago' AND vigencia = '$vigencia' GROUP BY fuente"; //echo $sql;
    $res = mysqli_query($linkbd,$sql);
    while($row = mysqli_fetch_row($res)){
        $valor[$row[1]] += $row[0];
    }
    
    
    $sql = "SELECT SUM(valor), fuente FROM ccpetinicialingresosbienestransportables WHERE cuenta = '$cuentaAgr' AND unidad = '$unidadEjecutora'  AND medioPago = '$medioPago' AND vigencia = '$vigencia' GROUP BY fuente"; //echo $sql;
    $res = mysqli_query($linkbd,$sql);
    while($row = mysqli_fetch_row($res)){
        $valor[$row[1]] += $row[0];
    }

    $sql = "SELECT SUM(valor), fuente FROM ccpetinicialingresoscuin WHERE cuenta = '$cuentaAgr' AND unidad = '$unidadEjecutora'  AND medioPago = '$medioPago' AND vigencia = '$vigencia' GROUP BY fuente"; //echo $sql;
    $res = mysqli_query($linkbd,$sql);
    while($row = mysqli_fetch_row($res)){
        $valor[$row[1]] += $row[0];
    }

    $sql = "SELECT SUM(valor), fuente FROM ccpetinicialingresosclasificador WHERE cuenta = '$cuentaAgr' AND unidad = '$unidadEjecutora'  AND medioPago = '$medioPago' AND vigencia = '$vigencia' GROUP BY fuente"; //echo $sql;
    $res = mysqli_query($linkbd,$sql);
    while($row = mysqli_fetch_row($res)){
        $valor[$row[1]] += $row[0];
    }

	$out['valor'] = $valor;
}

if($action=='guardarBienes'){ 

    //var_dump($_POST);
    
    
    $sql = "DELETE FROM ccpetinicialingresos WHERE cuenta = '".$_POST['cuentaPresupuestal']."' AND unidad = '".$_POST['unidadEjecutora']."' AND fuente = '".$_POST['fuente']."' AND medioPago = '".$_POST['medioPago']."' AND vigencia='".$_POST['vigencia']."'";
    mysqli_query($linkbd, $sql);

    $sql = "DELETE FROM ccpetinicialingresosbienestransportables WHERE cuenta = '".$_POST['cuentaPresupuestal']."' AND unidad = '".$_POST['unidadEjecutora']."' AND fuente = '".$_POST['fuente']."' AND medioPago = '".$_POST['medioPago']."' AND vigencia='".$_POST['vigencia']."'";
    mysqli_query($linkbd, $sql);

    if($_POST['valorTotal'] > 0){
        $sql="INSERT INTO  ccpetinicialingresos (cuenta, valor_total, unidad, fuente, medioPago, vigencia) VALUES ('".$_POST['cuentaPresupuestal']."', '".$_POST['valorTotal']."', '".$_POST['unidadEjecutora']."', '".$_POST['fuente']."', '".$_POST['medioPago']."', '".$_POST['vigencia']."')";
        if(mysqli_query($linkbd, $sql)){
            //var_dump($_POST['cuentasCuin']);
            for($x = 0; $x < count($_POST['cuentasSubclase']); $x++){

                $cuentas_separadas = explode(",", $_POST['cuentasSubclase'][$x]);
                //var_dump($cuentas_separadas);
                
                //echo $x;
                //echo $_POST['cuentasCuin'][$x];
                $sql = "INSERT INTO  ccpetinicialingresosbienestransportables (cuenta, subclase, valor, unidad, fuente, medioPago, vigencia) VALUES ('".$_POST['cuentaPresupuestal']."', '".$cuentas_separadas[0]."', '".$cuentas_separadas[1]."', '".$_POST['unidadEjecutora']."', '".$_POST['fuente']."', '".$_POST['medioPago']."', '".$_POST['vigencia']."')";

                if(mysqli_query($linkbd,$sql))
                {
                    $out['insertaBien'] = true; 
                }
                else
                {
                    $out['insertaBien'] = false; 
                }
            }
        }
    }else{
        $out['insertaBien'] = true; 
    }
    
}

if($action=='guardarServicios'){ 

    //var_dump($_POST);
    $sql = "DELETE FROM ccpetinicialingresos WHERE cuenta = '".$_POST['cuentaPresupuestal']."' AND unidad = '".$_POST['unidadEjecutora']."' AND fuente='".$_POST['fuente']."' AND medioPago = '".$_POST['medioPago']."'  AND vigencia = '".$_POST['vigencia']."'";
    mysqli_query($linkbd, $sql);

    $sql = "DELETE FROM ccpetinicialserviciosingresos WHERE cuenta = '".$_POST['cuentaPresupuestal']."' AND unidad = '".$_POST['unidadEjecutora']."' AND fuente='".$_POST['fuente']."' AND medioPago = '".$_POST['medioPago']."' AND vigencia = '".$_POST['vigencia']."'";
    mysqli_query($linkbd, $sql);

    if($_POST['valorTotalServicios'] > 0){
        $sql="INSERT INTO  ccpetinicialingresos (cuenta, valor_total, unidad, fuente, medioPago, vigencia) VALUES ('".$_POST['cuentaPresupuestal']."', '".$_POST['valorTotalServicios']."', '".$_POST['unidadEjecutora']."', '".$_POST['fuente']."', '".$_POST['medioPago']."', '".$_POST['vigencia']."')";
        if(mysqli_query($linkbd, $sql)){
            //var_dump($_POST['cuentasCuin']);
            for($x = 0; $x < count($_POST['cuentasSubclaseServicios']); $x++){

                $cuentas_separadas = explode(",", $_POST['cuentasSubclaseServicios'][$x]);
                //var_dump($cuentas_separadas);
                
                //echo $x;
                //echo $_POST['cuentasCuin'][$x];
                $sql = "INSERT INTO  ccpetinicialserviciosingresos (cuenta, subclase, valor, unidad, fuente, medioPago, vigencia) VALUES ('".$_POST['cuentaPresupuestal']."', '".$cuentas_separadas[0]."', '".$cuentas_separadas[1]."', '".$_POST['unidadEjecutora']."', '".$_POST['fuente']."', '".$_POST['medioPago']."', '".$_POST['vigencia']."')";

                if(mysqli_query($linkbd,$sql))
                {
                    $out['insertaBien'] = true; 
                }
                else
                {
                    $out['insertaBien'] = false; 
                }
            }
        }
    }else{
        $out['insertaBien'] = true; 
    }
    
}

if($action == "searchCuentaAgr"){
    $cuentaAgr = $_GET['cuentaAgr'];
    $unidadEjecutora = $_GET['unidadEjecutora'];
    $fuente = $_GET['fuente'];
    $medioPago = $_GET['medioPago'];
    $vigencia = $_GET['vigencia'];
    $sql = "SELECT subclase, valor FROM ccpetinicialingresosbienestransportables WHERE cuenta = '$cuentaAgr' AND unidad = '$unidadEjecutora' AND fuente = '$fuente' AND medioPago = '$medioPago' AND vigencia = '$vigencia'";
    $res = mysqli_query($linkbd, $sql);//echo $sql;
    $codigos = array();
    $codigosEliminar = array();
    $valorTotal = 0;

    while($row = mysqli_fetch_row($res))
    {
        $sql_bienes = "SELECT grupo, titulo, ciiu, sistema_armonizado, cpc FROM ccpetbienestransportables WHERE grupo = '$row[0]'";
        $res_bienes = mysqli_query($linkbd, $sql_bienes);
        $row_bienes = mysqli_fetch_row($res_bienes);

        //array_push($codigosEliminar, $row_bienes);

        array_push($row_bienes, $row[1]);

        array_push($codigos, $row_bienes);

        $valorTotal += $row[1];
    }
    $out['codigos'] = $codigos;
    //$out['codigosEliminar'] = $codigosEliminar;
    $out['valorTotal'] = $valorTotal;
}

if($action == "searchCuentaAgrCuin"){
    $cuentaAgr = $_GET['cuentaAgr'];
    $unidadEjecutora = $_GET['unidadEjecutora'];
    $fuente = $_GET['fuente'];
    $medioPago = $_GET['medioPago'];
    $vigencia = $_GET['vigencia'];
    $sql = "SELECT cuin, valor FROM ccpetinicialingresoscuin WHERE cuenta = '$cuentaAgr' AND unidad = '$unidadEjecutora' AND fuente = '$fuente' AND medioPago = '$medioPago' AND vigencia = '$vigencia'";
    $res = mysqli_query($linkbd,$sql);
    $codigos = array();
    $codigosEliminar = array();
    $valorTotal = 0;

    while($row = mysqli_fetch_row($res))
    {
        $sql_cuin = "SELECT * FROM ccpet_cuin WHERE codigo_cuin = '$row[0]'";
        $res_cuin = mysqli_query($linkbd, $sql_cuin);
        $row_cuin = mysqli_fetch_row($res_cuin);

        array_push($codigosEliminar, $row_cuin);

        array_push($row_cuin, $row[1]);

        array_push($codigos, $row_cuin);

        $valorTotal += $row[1];
    }
    $out['codigos'] = $codigos;
    $out['codigosEliminar'] = $codigosEliminar;
    $out['valorTotal'] = $valorTotal;
}

if($action == "searchCuentaAgrClasificador"){
    $cuentaAgr = $_GET['cuentaAgr'];
    $unidadEjecutora = $_GET['unidadEjecutora'];
    $fuente = $_GET['fuente'];
    $medioPago = $_GET['medioPago'];
    $vigencia = $_GET['vigencia'];
    $sql = "SELECT cuenta_clasificador, valor FROM ccpetinicialingresosclasificador WHERE cuenta = '$cuentaAgr' AND unidad = '$unidadEjecutora' AND fuente = '$fuente' AND medioPago = '$medioPago' AND vigencia = '$vigencia'";
    $res = mysqli_query($linkbd,$sql);
    $codigos = array();
    $valorTotal = 0;

    while($row = mysqli_fetch_row($res))
    {
        $sql_cuenta = "SELECT codigo, nombre, tipo FROM cuentasingresosccpet WHERE codigo = '$row[0]'";
        $res_cuenta = mysqli_query($linkbd, $sql_cuenta);
        $row_cuenta = mysqli_fetch_row($res_cuenta);

        array_push($row_cuenta, $row[1]);

        array_push($codigos, $row_cuenta);

        $valorTotal += $row[1];
    }
    $out['codigos'] = $codigos;
    $out['valorTotal'] = $valorTotal;
}

header("Content-type: application/json");
echo json_encode($out);
die();