const URL ='vue/herramientas_mipg/dependencias/editar/mipg-dependencias_editar.php';

var app = new Vue({
	el:"#myapp",
	data() {
		return {
			isLoading:false,
			txtNombre:"",
			txtConsecutivo:0,
			selectDependencias:0,
			arrDependencias:[],
			arrConsecutivos:[]
		}
	},
	mounted() {
		this.getData();
	},
	methods: {
		getData: async function(){
			const codigo = new URLSearchParams(window.location.search).get('id');
			const formData = new FormData();
			formData.append("action","get");
			formData.append("codigo",codigo);
			this.isLoading = true;
			const response = await fetch(URL,{method:"POST",body:formData});
			const objData = await response.json();
			if(objData.status){
				const data = objData.data.data;
				this.txtConsecutivo = data.codarea;
				this.txtNombre = data.nombrearea;
				this.selectDependencias = data.codpadrearea,
				this.arrDependencias = objData.dependencias;
				this.arrConsecutivos = objData.data.consecutivos;
			}else{
				codigo = objData.consecutivo;
				window.location.href="mipg-dependencias_editar?id="+codigo
			}
			this.isLoading = false;
		},
		save:function(){
			const vueContext = this;
			if(this.txtNombre =="" || this.txtCodigo ==""){
				Swal.fire("Error","Todos los campos con (*) son obligatorios","error");
				return false;
			}
			Swal.fire({
				title:"¿Estás segur@ de guardar?",
				text:"",
				icon: 'warning',
				showCancelButton:true,
				confirmButtonColor: '#3085d6',
				cancelButtonColor: '#d33',
				confirmButtonText:"Sí, guardar",
				cancelButtonText:"No, cancelar"
			}).then(async function(result){
				if(result.isConfirmed){
					const formData = new FormData();
					formData.append("action","save");
					formData.append("nombre",vueContext.txtNombre);
					formData.append("codigo",vueContext.txtConsecutivo);
					formData.append("codpadre",vueContext.selectDependencias);
					vueContext.isLoading = true;
					const response = await fetch(URL,{method:"POST",body:formData});
					const objData = await response.json();
					if(objData.status){
						Swal.fire("Guardado",objData.msg,"success");
					}else{
						Swal.fire("Error",objData.msg,"error");
					}
					vueContext.isLoading = false;
				}
			});
		},
		editItem:function(type){
			let vueContext = this;
			let id = this.txtConsecutivo;
			let index = this.arrConsecutivos.findIndex(function(e){return e.codarea == id});
			if(type=="next" && vueContext.arrConsecutivos[++index]){
				id = this.arrConsecutivos[index++].codarea;
			}else if(type=="prev" && vueContext.arrConsecutivos[--index]){
				id = this.arrConsecutivos[index--].codarea;
			}
			window.location.href="mipg-dependencias_editar?id="+id;
		},
		iratras:function(type){
			let id = this.txtConsecutivo;
			window.location.href="mipg-dependencias_buscar.php?id="+id;
		},
	},
	computed:{

	}
})
