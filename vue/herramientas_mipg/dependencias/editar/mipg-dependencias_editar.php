<?php
	require_once '../../../../comun.inc';
	require '../../../../funciones.inc';
	require '../../../../funcionesSP.inc.php';
	/*ini_set('display_errors', '1');
	ini_set('display_startup_errors', '1');
	error_reporting(E_ALL);*/
	session_start();
	//dep($_POST);exit;
	if($_POST){
		$obj = new Plantilla();
		if($_POST['action']=="get"){
			$obj->getData();
		}else if($_POST['action']=="save"){
			$obj->save();
		}
	}

	class Plantilla{
		private $linkbd;
		private $arrData;
		private $intId;
		public function __construct() {
			$this->linkbd = conectar_v7();
			$this->linkbd->set_charset("utf8");
		}
		public function save(){
			if(!empty($_SESSION)){
				if($_POST){
					if(empty($_POST['codigo']) || empty($_POST['nombre'])){
						$arrResponse = array("status"=>false,"msg"=>"Error de datos");
					}else{
						$intCodigo = intval($_POST['codigo']);
						$strNombre = ucwords(replaceChar(strClean($_POST['nombre'])));
						$intCodPadre = $_POST['codpadre'];
						$request = $this->updateData(array("codigo"=>$intCodigo,"nombre"=>$strNombre,"codpadrearea"=>$intCodPadre));
						if(is_numeric($request) && $request> 0 ){
							$arrResponse = array("status"=>true,"msg"=>"Datos guardados.");
						}else if($request =="existe"){
							$arrResponse = array("status"=>false,"msg"=>"El nombre del ya existe, pruebe con otro.");
						}else{
							$arrResponse = array("status"=>false,"msg"=>"Ha ocurrido un error, intente de nuevo.");
						}
					}
				}
				echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
			}
			die();
		}
		public function getData(){
			if(!empty($_SESSION)){
				//$id = intval(strClean($_POST['codigo']));
				$id = strClean($_POST['codigo']);
				$request = $this->selectData($id);
				if(!empty($request['data'])){
					$arrData = array(
						"dependencias"=>$this->selectDependencias(),
						"consecutivo"=>$this->selectConsecutivo(),
						"status"=>true,
						"data"=>$request
					);
				}else{
					$arrData = array("status"=>false,"consecutivo"=>$this->selectConsecutivo()-1);
				}
				echo json_encode($arrData,JSON_UNESCAPED_UNICODE);
			}
			die();
		}
		public function updateData(array $data){
			$this->arrData = $data;
			$sql = "SELECT * FROM planacareas WHERE nombrearea = '{$this->arrData['nombre']}' AND codarea!={$this->arrData['codigo']}";
			$request = mysqli_fetch_all(mysqli_query($this->linkbd,$sql),MYSQLI_ASSOC);
			if(empty($request)){
				$sql = "UPDATE planacareas SET nombrearea = '{$this->arrData['nombre']}', codpadrearea = '{$this->arrData['codpadrearea']}' WHERE codarea = {$this->arrData['codigo']}";
				$request = intval(mysqli_query($this->linkbd,$sql));
			}else{
				$request="existe";
			}
			return $request;
		}
		public function selectData($id){
			$this->intId = $id;
			$sql = "SELECT * FROM planacareas ORDER BY codarea ASC";
			$request = mysqli_fetch_all(mysqli_query($this->linkbd,$sql),MYSQLI_ASSOC);
			$data = array_values(array_filter($request,function($e){return $e['codarea'] == $this->intId;}))[0];
			return array("consecutivos"=>$request,"data"=>$data);
		}
		public function selectConsecutivo(){
			$sql = "SELECT MAX(codarea) as max FROM planacareas";
			$request = mysqli_query($this->linkbd,$sql)->fetch_assoc()['max']+1;
			return $request;		
		}
		public function selectDependencias(){
			$sql = "SELECT * FROM planacareas ORDER BY codarea ASC";
			$request = mysqli_fetch_all(mysqli_query($this->linkbd,$sql),MYSQLI_ASSOC);
			$arrData = array_values(array_filter($request,function($e){return $e['estado'] == "S";}));
			return $arrData;
        }
	}
?>
