<?php
	require_once '../../../../comun.inc';
	require '../../../../funciones.inc';
	require '../../../../funcionesSP.inc.php';
	session_start();
	if($_POST){
		$obj = new Plantilla();
		if($_POST['action']=="get"){
			$obj->getData();
		}else if($_POST['action']=="change"){
			$obj->changeData();
		}
	}

	class Plantilla{
		private $linkbd;
		private $arrData;
		public function __construct() {
			$this->linkbd = conectar_v7();
			$this->linkbd->set_charset("utf8");
		}

		public function getData(){
			if(!empty($_SESSION)){
				echo json_encode($this->selectData(),JSON_UNESCAPED_UNICODE);
			}
			die();
		}
		public function changeData(){
			if(!empty($_SESSION)){
				$id = intval($_POST['id']);
				$estado = $_POST['estado'];
				$sql = "UPDATE planacareas SET estado = '$estado' WHERE codarea = $id";
				$request = mysqli_query($this->linkbd,$sql);
				if($request == 1){
					$arrResponse = array("status"=>true);
				}else{
					$arrResponse = array("status"=>false,"msg"=>"Error");
				}
				echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
			}
			die();
		}
		public function selectData(){
			$linkbd = conectar_v7();
			$linkbd -> set_charset("utf8");
			$sql = "SELECT codarea, nombrearea, codpadrearea, estado FROM planacareas ORDER BY codarea ASC";
			$request = mysqli_fetch_all(mysqli_query($this->linkbd,$sql),MYSQLI_ASSOC);
			$total = count($request);
			for ($i=0; $i < $total; $i++) {
				$request[$i]['is_status'] = $request[$i]['estado'] =="S"? 1 : 0;
				$request[$i]['is_yellow'] = $request[$i]['codarea'] == $_POST['id_gia']? 1 : 0;
				$sqlt = "SELECT nombrearea FROM planacareas WHERE codarea = '".$request[$i]['codpadrearea']."'";
				$rest = mysqli_query($linkbd,$sqlt);
				$rowt = mysqli_fetch_row($rest);
				if($rowt[0] != ""){
					$request[$i]['is_padre'] = $request[$i]['codpadrearea']." - $rowt[0]";
				}else{
					$request[$i]['is_padre'] = "0 - NINGUNO";
				}
				
			}
			return $request;
		}

	}
?>
