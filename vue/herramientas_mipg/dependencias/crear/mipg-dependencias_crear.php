<?php 
	require_once '../../../../comun.inc';
	require '../../../../funciones.inc';
	require '../../../../funcionesSP.inc.php';
	/*ini_set('display_errors', '1');
	ini_set('display_startup_errors', '1');
	error_reporting(E_ALL);*/
	session_start();
	if($_POST){
		$obj = new Plantilla();
		if($_POST['action']=="get"){
			$obj->getData();
		}else if($_POST['action']=="save"){
			$obj->save();
		}
	}

	class Plantilla{
		private $linkbd;
		private $arrData;
		public function __construct() {
			$this->linkbd = conectar_v7();
			$this->linkbd->set_charset("utf8");
		}
		public function save(){
			if(!empty($_SESSION)){
				if($_POST){
					if(empty($_POST['codigo']) || empty($_POST['nombre'])){
						$arrResponse = array("status"=>false,"msg"=>"Error de datos");
					}else{
						$intCodigo = $_POST['codigo'];
						$strNombre = ucwords(replaceChar(strClean($_POST['nombre'])));
						$intCodPadre = $_POST['codpadre'];
						$request = $this->insertData(array("codarea"=>$intCodigo,"nombrearea"=>$strNombre,"codpadrearea"=>$intCodPadre));
						if(is_numeric($request) && $request> 0 ){
							$arrResponse = array("status"=>true,"msg"=>"Datos guardados.");
						}else if($request =="existe"){
							$arrResponse = array("status"=>false,"msg"=>"El nombre del ya existe, pruebe con otro.");
						}else{
							$arrResponse = array("status"=>false,"msg"=>"Ha ocurrido un error, intente de nuevo.");
						}
					}
				}
				echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
			}
			die();
		}
		public function getData(){
			if(!empty($_SESSION)){
				$arrData = array(
					"dependencias"=>$this->selectDependencias(),
					"consecutivo"=>$this->selectConsecutivo(),
				);
				echo json_encode($arrData,JSON_UNESCAPED_UNICODE);
			}
			die();
		}
		public function insertData(array $data){
			$this->arrData = $data;
			$request = mysqli_fetch_all(mysqli_query($this->linkbd,"SELECT * FROM planacareas WHERE nombrearea = '{$this->arrData['nombrearea']}'"),MYSQLI_ASSOC);
			if(empty($request)){
				$sql = "INSERT INTO planacareas(codarea, nombrearea, codpadrearea, estado) VALUES(
					'{$this->arrData['codarea']}',
					'{$this->arrData['nombrearea']}',
					'{$this->arrData['codpadrearea']}',
					'S'
				)";
				$request = intval(mysqli_query($this->linkbd,$sql));
			}else{
				$request="existe";
			}
			return $request;
		}
		public function selectConsecutivo(){
			$sql = "SELECT MAX(codarea) as max FROM planacareas";
			$request = mysqli_query($this->linkbd,$sql)->fetch_assoc()['max']+1;
			return $request;
		}
		public function selectDependencias(){
			$sql = "SELECT * FROM planacareas ORDER BY codarea ASC";
			$request = mysqli_fetch_all(mysqli_query($this->linkbd,$sql),MYSQLI_ASSOC);
			$arrData = array_values(array_filter($request,function($e){return $e['estado'] == "S";}));
			return $arrData;
        }
	}
?>
