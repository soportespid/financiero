const URL ='vue/herramientas_mipg/dependencias/crear/mipg-dependencias_crear.php';

var app = new Vue({
	el:"#myapp",
	data() {
		return {
			isLoading:false,
			txtNombre:"",
			txtConsecutivo:'00',
			selectDependencias:0,
			arrDependencias:[]
		}
	},
	mounted() {
		this.getData();
	},
	methods: {
		getData: async function(){
			const formData = new FormData();
			formData.append("action","get");
			this.isLoading = true;
			const response = await fetch(URL,{method:"POST",body:formData});
			const objData = await response.json();
			this.txtConsecutivo = objData.consecutivo;
			this.arrDependencias = objData.dependencias;
			this.isLoading = false;
		},
		save:function(){
			const vueContext = this;
			if(this.txtNombre == "" || this.txtConsecutivo == ""){
				Swal.fire("Error","Todos los campos con (*) son obligatorios","error");
				return false;
			}
			Swal.fire({
				title:"¿Estás segur@ de guardar?",
				text:"",
				icon: 'warning',
				showCancelButton:true,
				confirmButtonColor: '#3085d6',
				cancelButtonColor: '#d33',
				confirmButtonText:"Sí, guardar",
				cancelButtonText:"No, cancelar"
			}).then(async function(result){
				if(result.isConfirmed){
					const formData = new FormData();
					formData.append("action","save");
					formData.append("nombre",vueContext.txtNombre);
					formData.append("codigo",vueContext.txtConsecutivo);
					formData.append("codpadre",vueContext.selectDependencias);
					vueContext.isLoading = true;
					const response = await fetch(URL,{method:"POST",body:formData});
					const objData = await response.json();
					if(objData.status){
						vueContext.txtConsecutivo = "";
						vueContext.txtNombre = "";
						vueContext.selectDependencias = 0;
						vueContext.getData();
						Swal.fire("Guardado",objData.msg,"success");
					}else{
						Swal.fire("Error",objData.msg,"error");
					}
					vueContext.isLoading = false;
				}
			});
		}
	},
	computed:{

	}
})
