<?php

require __DIR__ . '/../../../vendor/autoload.php';

use Laravel\Route;
use Laravel\DB;

	require_once '../../../comun.inc';
	require '../../../funciones.inc';
	session_start();
	if($_POST){
		$base = isset($_POST['base']) ?  base64_decode($_POST['base']) : null;
		$usuario = isset($_POST['usuario']) ? base64_decode($_POST['usuario']) : null;
		$obj = new Plantilla($base, $usuario);
		switch ($_POST['action']){
			case 'iniDepartamento':
				$obj->iniDepartamento();
			break;
			case 'iniMunicipio':
				$obj->iniMunicipio();
			break;
			case 'infoInicial':
				$obj->infoInicial();
			break;
			case 'iniTerceros':
				$obj->iniTerceros();
			break;

            case 'getOperadorPlanillas':
                $obj->getOperadorPlanillas();
                break;

			case 'infoSave':
				$obj->infoSave();
			break;

		}
	}
	class Plantilla{
		private $linkbd;
		public function __construct($base = null, $usuario = null) {
			if (!empty($base) && !empty($usuario)) {
				$this->linkbd = conectar_Multi($base, $usuario);
			} else {
				$this->linkbd = conectar_v7();
			}
			$this->linkbd->set_charset("utf8");
		}
		public function iniDepartamento(){
			if(!empty($_SESSION)){
				echo json_encode($this->selectDepartamento(),JSON_UNESCAPED_UNICODE);
			}
			die();
		}
		public function selectDepartamento(){
			$sql = "SELECT danedpto, nombredpto FROM danedpto ORDER BY nombredpto ASC";
			$request = mysqli_fetch_all(mysqli_query($this->linkbd,$sql),MYSQLI_ASSOC);
			return $request;
		}
		public function iniMunicipio(){
			if(!empty($_SESSION)){
				echo json_encode($this->selectMunicipio(),JSON_UNESCAPED_UNICODE);
			}
			die();
		}
		public function selectMunicipio(){
			$dpto = $_POST['selectDpto'];
			$sql = "SELECT danemnpio, nom_mnpio FROM danemnpio WHERE danedpto = '$dpto' ORDER BY nom_mnpio ASC";
			$request = mysqli_fetch_all(mysqli_query($this->linkbd,$sql),MYSQLI_ASSOC);
			return $request;
		}
		public function infoInicial(){
			if(!empty($_SESSION)){
				echo json_encode($this->selectInicial(),JSON_UNESCAPED_UNICODE);
			}
			die();
		}
		public function selectInicial(){
			$sql = "SELECT * FROM configbasica";
			$request = mysqli_fetch_all(mysqli_query($this->linkbd,$sql),MYSQLI_ASSOC);
			return $request;
		}
		public function iniTerceros(){
			if(!empty($_SESSION)){
				$arrData = array(
					"terceros" =>$this->selectTerceros()
				);
				echo json_encode($arrData,JSON_UNESCAPED_UNICODE);
			}
			die();
		}
		public function selectTerceros(){
			$sql = "SELECT CONCAT(razonsocial,' ',nombre1,' ',nombre2,' ',apellido1,' ',apellido2) as nombre, cedulanit as documento
			FROM terceros WHERE estado = 'S'";
			$request = mysqli_fetch_all(mysqli_query($this->linkbd,$sql),MYSQLI_ASSOC);
			return $request;
		}
		public function infoSave(){
            $this->updateOperadorPlanilla();

			$inf_Nit = $_POST['varNit'];
			$inf_Razon = $_POST['varRazonsocial'];
			$inf_Direc = $_POST['varDireccion'];
			$inf_Telef = $_POST['varTelefono'];
			$inf_Web = $_POST['varWeb'];
			$inf_Email = $_POST['varEmail'];
			$inf_Reprenom = $_POST['varRepresentante'];
			$inf_Codcontra = $_POST['varCodcontaduria'];
			$inf_Igac = $_POST['varIgac'];
			$inf_Sigla = $_POST['varSigla'];
			$inf_Liquida = $_POST['varLiquidacion'];
			$inf_Orden = $_POST['varOrden'];
			$inf_Repredoc = $_POST['varCedulareplegal'];
			$inf_Depto = $_POST['varDepto'];
			$inf_Mpio = $_POST['varMpio'];
			$inf_Tipo = $_POST['varTipo'];
			$inf_Arldoc = $_POST['varArldoc'];
			$inf_Arlnom = $_POST['varArlnombre'];
			$inf_Icbfdoc = $_POST['varIcbfdoc'];
			$inf_Icbfnom = $_POST['varIcbfnombre'];
			$inf_Senadoc = $_POST['varSenadoc'];
			$inf_Senanom = $_POST['varSenanombre'];
			$inf_Instecdoc = $_POST['varInstecdoc'];
			$inf_Instecnom = $_POST['varInstecnombre'];
			$inf_Ccfdoc = $_POST['varCcfdoc'];
			$inf_Ccfnom = $_POST['varCcfnombre'];
			$inf_Esapdoc = $_POST['varEsapdoc'];
			$inf_Esapnom = $_POST['varEsapnombre'];
			$sql = "DELETE FROM configbasica";
			if (mysqli_query($this->linkbd, $sql)) {
				$sql = "INSERT INTO configbasica (nit, razonsocial, direccion, telefono, web, email, representante, estado, codcontaduria, igac, sigla, liquidacion, orden, cedulareplegal, depto, mnpio, tipo_entidad, arl_doc, arl_nombre, icbf_doc, icbf_nombre, sena_doc, sena_nombre, istec_doc, istec_nombre, cajac_doc, cajac_nombre, esap_cod, esap_nombre) VALUES ('$inf_Nit', '$inf_Razon', '$inf_Direc', '$inf_Telef', '$inf_Web', '$inf_Email', '$inf_Reprenom', 'S', '$inf_Codcontra', '$inf_Igac', '$inf_Sigla', '$inf_Liquida', '$inf_Orden', '$inf_Repredoc', '$inf_Depto', '$inf_Mpio', '$inf_Tipo', '$inf_Arldoc', '$inf_Arlnom', '$inf_Icbfdoc', '$inf_Icbfnom', '$inf_Senadoc', '$inf_Senanom', '$inf_Instecdoc', '$inf_Instecnom', '$inf_Ccfdoc', '$inf_Ccfnom', '$inf_Esapdoc', '$inf_Esapnom')";
				if (mysqli_query($this->linkbd, $sql)) {
					echo json_encode(['status' => 'success']);
				} else {
					echo json_encode(['status' => 'error', 'message' => 'Error al insertar los datos.']);
				}
			} else {
				echo json_encode(['status' => 'error', 'message' => 'Error al eliminar los datos anteriores.']);
			}
			die();
		}

        /**
         * TODO: Implementar endpoint para leer los operadores disponibles en el sistema
         */
        public function getOperadorPlanillas()
        {
            if (empty($_SESSION)) {
                die;
            }

            echo json_encode(['operadorPlanillas' => [
                'SOI', 'Asopagos', 'Aportes En Linea'
            ]], JSON_UNESCAPED_UNICODE);

            die;
		}

        public function updateOperadorPlanilla()
        {
            if (empty($operadorPlanilla = $_POST['operadorPlanilla'])) {
                return;
            }

            $central = DB::connectCentral();
            $domain = Route::currentDomain();

            $stmt = $central->prepare('SELECT `tenant_id` FROM `domains` WHERE `domain` = ? LIMIT 1');
            $stmt->bind_param('s', $domain);
            $stmt->execute();
            $result = $stmt->get_result();

            if ((! $result instanceof mysqli_result) || $result->num_rows == 0) {
                echo json_encode(['status' => 'error', 'message' => 'Error al actualizar el operador de planillas']);
                die;
            }

            $tenant_id = $result->fetch_array(MYSQLI_NUM)[0];

            $stmt = $central->prepare('SELECT JSON_SET(`data`, \'$.operador_planilla\', ?) FROM `tenants` WHERE `id` = ?');
            $stmt->bind_param('ss', $operadorPlanilla, $tenant_id);
            $stmt->execute();
            $result = $stmt->get_result();

            if ((! $result instanceof mysqli_result) || $result->num_rows == 0) {
                echo json_encode(['status' => 'error', 'message' => 'Error al actualizar el operador de planillas']);
                die;
            }

            $data = $result->fetch_array(MYSQLI_NUM)[0];

            if (empty($data)) {
                echo json_encode(['status' => 'error', 'message' => 'Error al actualizar el operador de planillas']);
                die;
            }

            $stmt = $central->prepare('UPDATE `tenants` SET `data` = ? WHERE `id` = ?');
            $stmt->bind_param('ss', $data, $tenant_id);
            $stmt->execute();

            $central->close();
        }
	}
