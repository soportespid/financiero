const infoEntidadURL ='vue/herramientas_mipg/entidad/mipg-infoEntidad.php';
var app = new Vue({
    el: '#myapp',
    data() {
        return {
            isLoading:false,
            isModal:false,
            txtRazon: '',
            txtNit: '',
            txtSigla: '',
            txtDireccion: '',
            selectDpto: '0',
            arrDpto: [],
            selectMpio: '0',
            arrMpio: [],
            txtTelefono: '',
            txtEmail: '',
            txtWeb: '',
            txtIgac: '',
            txtCcgr: '',
            selectOrden: '',
            objTercero:{nombre:"",documento:""},
            arrTerceros:[],
            arrTercerosCopy:[],
            txtBuscar:"",
            txtResultados:0,
            selectTEntidad: '',
            selectLiquidacion: '',
            objArl:{nombre:"",documento:""},
            objIcbf:{nombre:"",documento:""},
            objSena:{nombre:"",documento:""},
            objInstec:{nombre:"",documento:""},
            objCcf:{nombre:"",documento:""},
            objEsap:{nombre:"",documento:""},
            auxTercero: '',
            operadorPlanilla: '',
            operadorPlanillas: [],
        };
    },
    mounted(){
        this.iniDepartamento();
        this.iniTercero();
        this.infoInicial();
        this.getOperadorPlanillas();
    },
    methods: {
        iniDepartamento: async function(){
            const formData = new FormData();
            formData.append("action","iniDepartamento");
            this.isLoading = true;
            const response = await fetch(infoEntidadURL,{method:"POST",body:formData});
            const objData = await response.json();
            this.arrDpto = objData;
            this.isLoading = false;
        },
        iniMunicipio: async function(){
            const formData = new FormData();
            formData.append("action","iniMunicipio");
            formData.append("selectDpto",this.selectDpto);
            this.isLoading = true;
            const response = await fetch(infoEntidadURL,{method:"POST",body:formData});
            const objData = await response.json();
            this.arrMpio = objData;
            this.isLoading = false;
        },
        iniTercero: async function() {
            const formData = new FormData();
            formData.append("action","iniTerceros");
            this.isLoading = true;
            const response = await fetch(infoEntidadURL,{method:"POST",body:formData});
            const objData = await response.json();
            this.arrTerceros = objData.terceros;
            this.arrTercerosCopy = objData.terceros;
            this.txtResultados = this.arrTerceros.length;
            if(objData.data.tercero != null){
                this.objTercero = objData.data.tercero;
            }
        },

        getOperadorPlanillas: async function() {
            this.isLoading = true;

            let body = new FormData();
            body.append('action', 'getOperadorPlanillas');

            let { data } = await axios.post(infoEntidadURL, body);

            this.operadorPlanillas = data.operadorPlanillas;

            this.isLoading = false;
        },

        search: function(type = "", objeto = "objArl") {
            let obj = null;
            if (type === "documento") {
                obj = this.arrTerceros.find(e => e.documento == this[objeto].documento);
                if (obj) {
                    this[objeto].nombre = obj.nombre;
                } else {
                    this[objeto].documento = ""; // Borra el valor del documento
                    Swal.fire(
                        'No encontrado',
                        'No se encuentra almacenado ese tercero.',
                        'warning'
                    );
                }
            } else {
                let search = this.txtBuscar.toLowerCase();
                this.arrTercerosCopy = this.arrTerceros.filter(e => e.nombre.toLowerCase().includes(search) || e.documento.toLowerCase().includes(search) );
                this.txtResultados = this.arrTercerosCopy.length;
            }
        },
        selectItem: function({...item}){
            switch (this.auxTercero){
                case 1:
                    this.objTercero = item;
                    this.isModal = false;
                break;
                case 2:
                    this.objArl = item;
                    this.isModal = false;
                break;
                case 3:
                    this.objIcbf = item;
                    this.isModal = false;
                break;
                case 4:
                    this.objSena = item;
                    this.isModal = false;
                break;
                case 5:
                    this.objInstec = item;
                    this.isModal = false;
                break;
                case 6:
                    this.objCcf = item;
                    this.isModal = false;
                break;
                case 7:
                    this.objEsap = item;
                    this.isModal = false;
                break;
            }
        },
        infoInicial: async function() {
            const formData = new FormData();
            formData.append("action","infoInicial");
            this.isLoading = true;
            const response = await fetch(infoEntidadURL,{method:"POST",body:formData});
            const objData = await response.json();
            this.txtRazon = objData[0].razonsocial;
            this.txtNit = objData[0].nit;
            this.txtSigla = objData[0].sigla;
            this.txtDireccion = objData[0].direccion;
            this.selectDpto = objData[0].depto;
            this.iniMunicipio();
            this.selectMpio = objData[0].mnpio;
            this.txtEmail = objData[0].email;
            this.txtWeb = objData[0].web;
            this.txtIgac = objData[0].igac;
            this.txtCcgr = objData[0].codcontaduria;
            this.selectOrden = objData[0].orden;
            this.txtTelefono = objData[0].telefono;
            this.selectLiquidacion = objData[0].liquidacion;
            this.objTercero.nombre = objData[0].representante;
            this.objTercero.documento = objData[0].cedulareplegal;
            this.selectTEntidad = objData[0].tipo_entidad;
            this.objArl.nombre = objData[0].arl_nombre;
            this.objArl.documento = objData[0].arl_doc;
            this.objIcbf.nombre = objData[0].icbf_nombre;
            this.objIcbf.documento = objData[0].icbf_doc;
            this.objSena.nombre = objData[0].sena_nombre;
            this.objSena.documento = objData[0].sena_doc;
            this.objInstec.nombre = objData[0].istec_nombre;
            this.objInstec.documento = objData[0].istec_doc;
            this.objCcf.nombre = objData[0].cajac_nombre;
            this.objCcf.documento = objData[0].cajac_doc;
            this.objEsap.nombre = objData[0].esap_nombre;
            this.objEsap.documento = objData[0].esap_cod;
        },
        showTab:function(tab){
            let tabs = this.$refs.rTabs.children;
            let tabsContent = this.$refs.rTabsContent.children;
            for (let i = 0; i < tabs.length; i++) {
                tabs[i].classList.remove("active");
                tabsContent[i].classList.remove("active")
            }
            tabs[tab-1].classList.add("active");
            tabsContent[tab-1].classList.add("active")
        },
        confirmSave: function(){

            if(this.txtRazon != ''){
                if(this.txtNit != ''){
                    if(this.txtSigla != ''){
                        if(this.txtDireccion != ''){
                            if(this.selectDpto != '0'){
                                if(this.selectMpio != '0'){
                                    this.save();
                                }else{
                                    Swal.fire(
                                        'Cancelado',
                                        'debes ingresar el Municipio de la entidad.',
                                        'error'
                                        )
                                }
                            }else{
                                Swal.fire(
                                    'Cancelado',
                                    'debes ingresar el Departamento de la entidad.',
                                    'error'
                                    )
                            }
                        }else{
                            Swal.fire(
                                'Cancelado',
                                'debes ingresar la Dirección de la entidad.',
                                'error'
                                )
                        }
                    }else{
                        Swal.fire(
                            'Cancelado',
                            'debes ingresar la Sigla de la entidad.',
                            'error'
                            )
                    }
                }else{
                    Swal.fire(
                        'Cancelado',
                        'debes ingresar el Nit de la entidad.',
                        'error'
                        )
                }
            }else{
                Swal.fire(
                    'Cancelado',
                    'debes ingresar la Razón Social de la entidad.',
                    'error'
                    )
            }
            Swal.fire({
                title: '¿Estás seguro?',
                text: "¡Vas a guardar los cambios!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Sí, guardar'
            }).then((result) => {
                if (result.isConfirmed) {
                    this.save();
                    Swal.fire(
                        '¡Guardado!',
                        'Tus cambios han sido guardados.',
                        'success'
                    )
                }
            })
        },
        save: async function(){
            const formData = new FormData();
            formData.append("action", "infoSave");
            formData.append("varNit", this.txtNit);
            formData.append("varRazonsocial", this.txtRazon);
            formData.append("varDireccion", this.txtDireccion);
            formData.append("varTelefono", this.txtTelefono);
            formData.append("varWeb", this.txtWeb);
            formData.append("varEmail", this.txtEmail);
            formData.append("varRepresentante", this.objTercero.nombre);
            formData.append("varCodcontaduria", this.txtCcgr);
            formData.append("varIgac", this.txtIgac);
            formData.append("varSigla", this.txtSigla);
            formData.append("varLiquidacion", this.selectLiquidacion);
            formData.append("varOrden", this.selectOrden);
            formData.append("varCedulareplegal", this.objTercero.documento);
            formData.append("varDepto", this.selectDpto);
            formData.append("varMpio", this.selectMpio);
            formData.append("varTipo", this.selectTEntidad);
            formData.append("varArldoc", this.objArl.documento);
            formData.append("varArlnombre", this.objArl.nombre);
            formData.append("varIcbfdoc", this.objIcbf.documento);
            formData.append("varIcbfnombre", this.objIcbf.nombre);
            formData.append("varSenadoc", this.objSena.documento);
            formData.append("varSenanombre", this.objSena.nombre);
            formData.append("varInstecdoc", this.objInstec.documento);
            formData.append("varInstecnombre", this.objInstec.nombre);
            formData.append("varCcfdoc", this.objCcf.documento);
            formData.append("varCcfnombre", this.objCcf.nombre);
            formData.append("varEsapdoc", this.objEsap.documento);
            formData.append("varEsapnombre", this.objEsap.nombre);
            formData.append('operadorPlanilla', this.operadorPlanilla);

            this.isLoading = true;
            try {
                const response = await fetch(infoEntidadURL, { method: "POST", body: formData });
                const result = await response.json();
                if (result.status === 'success') {
                    Swal.fire(
                        '¡Guardado!',
                        'Tus cambios han sido guardados.',
                        'success'
                    );
                } else {
                    Swal.fire(
                        'Error',
                        result.message || 'No se pudo guardar la información.',
                        'error'
                    );
                }
            } catch (error) {
                Swal.fire(
                    'Error',
                    'Ocurrió un error al guardar la información.',
                    'error'
                );
            } finally {
                this.isLoading = false;
            }

        },
    }
});
