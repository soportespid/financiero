<?php
	require_once '../../../../comun.inc';
	require '../../../../funciones.inc';
	require '../../../../funcionesSP.inc.php';
	session_start();
	if($_POST){
		$base = isset($_POST['base']) ?  base64_decode($_POST['base']) : null;
		$usuario = isset($_POST['usuario']) ? base64_decode($_POST['usuario']) : null;
		$obj = new Plantilla($base, $usuario);
		switch ($_POST['action']){
			case 'getData':
				$obj->getData();
			break;
			case 'save':
				$obj->save();
			break;

		}
	}
	class Plantilla{
		private $linkbd;
		public function __construct($base = null, $usuario = null) {
			if (!empty($base) && !empty($usuario)) {
				$this->linkbd = conectar_Multi($base, $usuario);
			} else {
				$this->linkbd = conectar_v7();
			}
			$this->linkbd->set_charset("utf8");
		}
		public function getData(){
			if(!empty($_SESSION)){
				$arrData = array(
					"consecutivo"=>$this->selectConsecutivo(),
					"iniJefe"=>$this->selectJefe(),
					"iniDependencia"=>$this->selectDependencia(),
					"iniNSalarial"=>$this->selectNSalarial(),
                    "requisitos"=>$this->selectRequisitos()
				);
				echo json_encode($arrData,JSON_UNESCAPED_UNICODE);
			}
			die();
		}
        public function selectRequisitos(){
            $sql = "SELECT * FROM hum_caracterizacion_funciones ORDER BY orden";
            $request = mysqli_fetch_all(mysqli_query($this->linkbd,$sql),MYSQLI_ASSOC);
            $arrData = [];
            if(!empty($request)){
                $total = count($request);
                for ($i=0; $i < $total ; $i++) {
                    $e = $request[$i];
                    $idFuncion = $e['id'];
                    $sqlTipos = "SELECT * FROM hum_caracterizacion_tipo WHERE funcion_id = $idFuncion ORDER BY orden ";
                    $arrTipos = mysqli_fetch_all(mysqli_query($this->linkbd,$sqlTipos),MYSQLI_ASSOC);
                    if(!empty($arrTipos)){
                        $totalTipos = count($arrTipos);
                        for ($j=0; $j < $totalTipos ; $j++) {

                            $idTipo = $arrTipos[$j]['id'];
                            $sqlRequisitos = "SELECT * FROM hum_caracterizacion_requisito WHERE tipo_id = $idTipo";
                            $arrRequisitos = mysqli_fetch_all(mysqli_query($this->linkbd,$sqlRequisitos),MYSQLI_ASSOC);
                            $totalRequisitos = count($arrRequisitos);
                            for ($k=0; $k < $totalRequisitos ; $k++) {
                                $arrRequisitos[$k]['is_checked'] = 0;
                            }
                            $arrTipos[$j]['requisitos'] = $arrRequisitos;
                            $arrTipos[$j]['is_checked'] = 0;
                        }
                        $arrTipos = array_values(array_filter($arrTipos,function($e){return !empty($e['requisitos']);}));
                        $request[$i]['tipos'] = $arrTipos;
                        $request[$i]['is_checked'] = 0;
                        array_push($arrData,$request[$i]);
                    }
                }
            }
            return $arrData;
        }
		public function selectConsecutivo(){
			$sql = "SELECT MAX(codcargo) AS max FROM planaccargos";
			$request = mysqli_query($this->linkbd,$sql)->fetch_assoc()['max']+1;
			return $request;
		}
		public function selectJefe(){
			$sql = "SELECT codcargo, nombrecargo FROM planaccargos ORDER BY codcargo ASC";
			$request = mysqli_fetch_all(mysqli_query($this->linkbd,$sql),MYSQLI_ASSOC);
			return $request;
		}
		public function selectDependencia(){
			$sql = "SELECT codarea, nombrearea FROM planacareas ORDER BY codarea ASC";
			$request = mysqli_fetch_all(mysqli_query($this->linkbd,$sql),MYSQLI_ASSOC);
			return $request;
		}
		public function selectNSalarial(){
			$sql = "SELECT id_nivel, nombre FROM humnivelsalarial ORDER BY id_nivel ASC";
			$request = mysqli_fetch_all(mysqli_query($this->linkbd,$sql),MYSQLI_ASSOC);
			return $request;
		}
		public function save(){
			if(!empty($_SESSION)){
				$varCodigo = $_POST['codigo'];
				$varNombre = replaceChar(strClean($_POST['nombre']));
				$varJefe = $_POST['jefe'];
				$varDependencia = $_POST['dependencia'];
				$varNivel = $_POST['nsalarial'];
                $arrRequisitos = json_decode($_POST['requisitos'],true);
				$request = $this->insertData(array("codigo"=>$varCodigo,"nombre"=>$varNombre, "jefe"=>$varJefe, "dependencia"=>$varDependencia, "nivel"=>$varNivel),$arrRequisitos);
				if(is_numeric($request) && $request> 0 ){
                    insertAuditoria("mipg_auditoria","mipg_funciones_id",4,"Crear",$varCodigo);
					$arrResponse = array("status"=>true,"msg"=>"Datos guardados.");
				}else if($request =="existe"){
					$arrResponse = array("status"=>false,"msg"=>"El nombre del ya existe, pruebe con otro.");
				}else{
					$arrResponse = array("status"=>false,"msg"=>"Ha ocurrido un error, intente de nuevo.");
				}
				echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
			}
			die();
		}
		public function insertData(array $data,array $requisitos){
			$this->arrData = $data;
			$request = mysqli_fetch_all(mysqli_query($this->linkbd,"SELECT * FROM planaccargos WHERE nombrecargo = '{$this->arrData['nombre']}'"),MYSQLI_ASSOC);
			if(empty($request)){
				$sql = "INSERT INTO planaccargos(codcargo, codpadre, nombrecargo, dependencia, clasificacion, estado) VALUES (
					'{$this->arrData['codigo']}',
					'{$this->arrData['jefe']}',
					'{$this->arrData['nombre']}',
					'{$this->arrData['dependencia']}',
					'{$this->arrData['nivel']}',
					'S'
				)";
				$request = intval(mysqli_query($this->linkbd,$sql));
                if(!empty($requisitos)){
                    foreach ($requisitos as $e) {
                        $tipos = $e['tipos'];
                        foreach ($tipos as $f) {
                            $req = $f['requisitos'];
                            foreach ($req as $g) {
                                if($g['is_checked'] && $f['is_checked'] && $e['is_checked']){
                                    $sqlReq = "INSERT INTO planaccargos_requisitos(cargo_id,requisito_id) VALUES({$this->arrData['codigo']},{$g['id']})";
                                    mysqli_query($this->linkbd,$sqlReq);
                                }
                            }
                        }
                    }
                }
			}else{
				$request = "existe";
			}
			return $request;
		}
	}
?>
