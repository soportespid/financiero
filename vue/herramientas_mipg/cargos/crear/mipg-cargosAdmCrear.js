const URL ='vue/herramientas_mipg/cargos/crear/mipg-cargosAdmCrear.php';
var app = new Vue({
	el: '#myapp',
	data() {
		return {
			isLoading: false,
			txtConsecutivo: '',
			txtNombreCargo: '',
			selectJefe: 0,
			arrJefe: [],
			selectDependencia: 0,
			arrDependencia: [],
			selectNSalarial: 0,
			arrNSalarial: [],
            arrRequisitos:[],
		};
	},
	mounted(){
		this.getData();
	},
	methods: {
		getData: async function(){
			const formData = new FormData();
			formData.append("action","getData");
			this.isLoading = true;
			const response = await fetch(URL,{method:"POST",body:formData});
			const objData = await response.json();
			this.txtConsecutivo = objData.consecutivo;
			this.arrJefe = objData.iniJefe;
			this.arrDependencia = objData.iniDependencia;
			this.arrNSalarial = objData.iniNSalarial;
			this.isLoading = false;
            this.arrRequisitos = objData.requisitos;
		},
		save:function(){
			const vueContext = this;
			if(this.txtConsecutivo == "" || this.txtNombreCargo == "" || this.selectDependencia == 0 || this.selectNSalarial == 0){
				Swal.fire("Error","Todos los campos con (*) son obligatorios","error");
				return false;
			}
			Swal.fire({
				title:"¿Estás segur@ de guardar?",
				text:"",
				icon: 'warning',
				showCancelButton:true,
				confirmButtonColor: '#3085d6',
				cancelButtonColor: '#d33',
				confirmButtonText:"Sí, guardar",
				cancelButtonText:"No, cancelar"
			}).then(async function(result){
				if(result.isConfirmed){
					const formData = new FormData();
					formData.append("action","save");
					formData.append("nombre",vueContext.txtNombreCargo);
					formData.append("codigo",vueContext.txtConsecutivo);
					formData.append("jefe",vueContext.selectJefe);
					formData.append("dependencia",vueContext.selectDependencia);
					formData.append("nsalarial",vueContext.selectNSalarial);
                    formData.append("requisitos",JSON.stringify(vueContext.arrRequisitos));
					vueContext.isLoading = true;
					const response = await fetch(URL,{method:"POST",body:formData});
					const objData = await response.json();
					if(objData.status){
						Swal.fire("Guardado",objData.msg,"success");
                        setTimeout(function(){
                            window.location.href='mipg-cargosAdmEditar.php?id='+app.txtConsecutivo;
                        },1500);
					}else{
						Swal.fire("Error",objData.msg,"error");
					}
					vueContext.isLoading = false;
				}
			});
		}
	}
});
