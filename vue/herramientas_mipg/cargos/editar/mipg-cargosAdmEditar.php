<?php
	require_once '../../../../comun.inc';
	require '../../../../funciones.inc';
	require '../../../../funcionesSP.inc.php';
	session_start();
    /*ini_set('display_errors', '1');
    ini_set('display_startup_errors', '1');
    error_reporting(E_ALL);*/
	if($_POST){
		$base = isset($_POST['base']) ?  base64_decode($_POST['base']) : null;
		$usuario = isset($_POST['usuario']) ? base64_decode($_POST['usuario']) : null;
		$obj = new Plantilla($base, $usuario);
		switch ($_POST['action']){
			case 'getData':
				$obj->getData();
			break;
			case 'save':
				$obj->save();
			break;

		}
	}
	class Plantilla{
		private $linkbd;
		public function __construct($base = null, $usuario = null) {
			if (!empty($base) && !empty($usuario)) {
				$this->linkbd = conectar_Multi($base, $usuario);
			} else {
				$this->linkbd = conectar_v7();
			}
			$this->linkbd->set_charset("utf8");
		}
		public function getData(){
			if(!empty($_SESSION)){
				$id = strClean($_POST['codigo']);
				$request = $this->selectData($id);
				if(!empty($request['data'])){
                    $arrCargoRequisitos = $request['data']['requisitos'];
                    $arrRequisitos = $this->selectRequisitos();
                    if(!empty($arrCargoRequisitos)){
                        for ($i=0; $i < count($arrRequisitos) ; $i++) {
                            $areas = $arrRequisitos[$i];
                            $tipos = $areas['tipos'];
                            for ($j=0; $j < count($tipos) ; $j++) {
                                $tipo = $tipos[$j];
                                $requisitos = $tipo['requisitos'];
                                for ($k=0; $k < count($requisitos)  ; $k++) {
                                    $requisito = $requisitos[$k];
                                    $cargoReq = array_filter($arrCargoRequisitos,function($e)use($requisito,$areas,$tipo){
                                        return $requisito['id'] == $e['requisito_id'] && $tipo['id'] == $e['tipo_id']
                                        && $areas['id'] == $e['funciones_id'];
                                    });
                                    $requisito['is_checked'] = !empty($cargoReq) ? 1 : 0;
                                    $requisitos[$k] = $requisito;
                                }
                                $tipos[$j]['requisitos'] = $requisitos;
                                $tipos[$j]['is_checked'] = !empty(array_filter($requisitos,function($e){return $e['is_checked'] == 1;})) ? 1 : 0;
                            }
                            $arrRequisitos[$i]['tipos'] = $tipos;
                            $arrRequisitos[$i]['is_checked'] = !empty(array_filter($tipos,function($e){return $e['is_checked'] == 1;})) ? 1 : 0;
                        }
                    }
					$arrData = array(
						"iniJefe"=>$this->selectJefe(),
						"iniDependencia"=>$this->selectDependencia(),
						"iniNSalarial"=>$this->selectNSalarial(),
                        "requisitos"=>$arrRequisitos,
						"status"=>true,
						"data"=>$request
					);
				}else{
					$arrData = array("status"=>false,"consecutivo"=>$this->selectConsecutivo()-1);
				}
				echo json_encode($arrData,JSON_UNESCAPED_UNICODE);
			}
			die();
		}
        public function selectRequisitos(){
            $sql = "SELECT * FROM hum_caracterizacion_funciones ORDER BY orden";
            $request = mysqli_fetch_all(mysqli_query($this->linkbd,$sql),MYSQLI_ASSOC);
            $arrData = [];
            if(!empty($request)){
                $total = count($request);
                for ($i=0; $i < $total ; $i++) {
                    $e = $request[$i];
                    $idFuncion = $e['id'];
                    $sqlTipos = "SELECT * FROM hum_caracterizacion_tipo WHERE funcion_id = $idFuncion ORDER BY orden ";
                    $arrTipos = mysqli_fetch_all(mysqli_query($this->linkbd,$sqlTipos),MYSQLI_ASSOC);
                    if(!empty($arrTipos)){
                        $totalTipos = count($arrTipos);
                        for ($j=0; $j < $totalTipos ; $j++) {

                            $idTipo = $arrTipos[$j]['id'];
                            $sqlRequisitos = "SELECT * FROM hum_caracterizacion_requisito WHERE tipo_id = $idTipo";
                            $arrRequisitos = mysqli_fetch_all(mysqli_query($this->linkbd,$sqlRequisitos),MYSQLI_ASSOC);
                            $totalRequisitos = count($arrRequisitos);
                            for ($k=0; $k < $totalRequisitos ; $k++) {
                                $arrRequisitos[$k]['is_checked'] = 0;
                            }
                            $arrTipos[$j]['requisitos'] = $arrRequisitos;
                            $arrTipos[$j]['is_checked'] = 0;
                        }
                        $arrTipos = array_values(array_filter($arrTipos,function($e){return !empty($e['requisitos']);}));
                        $request[$i]['tipos'] = $arrTipos;
                        $request[$i]['is_checked'] = 0;
                        array_push($arrData,$request[$i]);
                    }
                }
            }
            return $arrData;
        }
		public function selectData($id){
			$this->intId = $id;
			$sql = "SELECT * FROM planaccargos ORDER BY codcargo ASC";
			$request = mysqli_fetch_all(mysqli_query($this->linkbd,$sql),MYSQLI_ASSOC);
			$data = array_values(array_filter($request,function($e){return $e['codcargo'] == $this->intId;}))[0];

            $sqlReq = "SELECT hf.id as funciones_id, ht.id as tipo_id, hr.id as requisito_id
            FROM planaccargos_requisitos pr
            INNER JOIN hum_caracterizacion_requisito hr ON hr.id = pr.requisito_id
            INNER JOIN hum_caracterizacion_tipo ht ON ht.id = hr.tipo_id
            INNER JOIN hum_caracterizacion_funciones hf ON hf.id = ht.funcion_id
            WHERE hr.estado = 'S' AND ht.estado = 'S' AND hf.estado = 'S' AND pr.cargo_id = $this->intId";

            $data['requisitos'] = mysqli_fetch_all(mysqli_query($this->linkbd,$sqlReq),MYSQLI_ASSOC);
			return array("consecutivos"=>$request,"data"=>$data);
		}
		public function selectConsecutivo(){
			$sql = "SELECT MAX(codcargo) AS max FROM planaccargos";
			$request = mysqli_query($this->linkbd,$sql)->fetch_assoc()['max']+1;
			return $request;
		}
		public function selectJefe(){
			$sql = "SELECT codcargo, nombrecargo FROM planaccargos ORDER BY codcargo ASC";
			$request = mysqli_fetch_all(mysqli_query($this->linkbd,$sql),MYSQLI_ASSOC);
			return $request;
		}
		public function selectDependencia(){
			$sql = "SELECT codarea, nombrearea FROM planacareas ORDER BY codarea ASC";
			$request = mysqli_fetch_all(mysqli_query($this->linkbd,$sql),MYSQLI_ASSOC);
			return $request;
		}
		public function selectNSalarial(){
			$sql = "SELECT id_nivel, nombre FROM humnivelsalarial ORDER BY id_nivel ASC";
			$request = mysqli_fetch_all(mysqli_query($this->linkbd,$sql),MYSQLI_ASSOC);
			return $request;
		}
		public function save(){
			if(!empty($_SESSION)){
				$varCodigo = $_POST['codigo'];
				$varNombre = replaceChar(strClean($_POST['nombre']));
				$varJefe = $_POST['jefe'];
				$varDependencia = $_POST['dependencia'];
				$varNivel = $_POST['nsalarial'];
                $arrRequisitos = json_decode($_POST['requisitos'],true);
				$request = $this->updateData(array("codigo"=>$varCodigo,"nombre"=>$varNombre, "jefe"=>$varJefe, "dependencia"=>$varDependencia, "nivel"=>$varNivel),$arrRequisitos);
				if(is_numeric($request) && $request> 0 ){
                    insertAuditoria("mipg_auditoria","mipg_funciones_id",4,"Editar",$varCodigo);
					$arrResponse = array("status"=>true,"msg"=>"Datos guardados.");
				}else if($request =="existe"){
					$arrResponse = array("status"=>false,"msg"=>"El nombre del ya existe, pruebe con otro.");
				}else{
					$arrResponse = array("status"=>false,"msg"=>"Ha ocurrido un error, intente de nuevo.");
				}
				echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
			}
			die();
		}
		public function updateData(array $data,array $requisitos){
			$this->arrData = $data;
			$sql = "SELECT * FROM planaccargos WHERE nombrecargo = '{$this->arrData['nombre']}' AND codcargo!={$this->arrData['codigo']}";
			$request = mysqli_fetch_all(mysqli_query($this->linkbd,$sql),MYSQLI_ASSOC);
			if(empty($request)){
				$sql = "UPDATE planaccargos SET codpadre = '{$this->arrData['jefe']}', nombrecargo = '{$this->arrData['nombre']}', dependencia = '{$this->arrData['dependencia']}', clasificacion = '{$this->arrData['nivel']}' WHERE codcargo = '{$this->arrData['codigo']}'";
				$request = intval(mysqli_query($this->linkbd,$sql));
                if(!empty($requisitos)){
                    mysqli_query($this->linkbd,"DELETE FROM planaccargos_requisitos WHERE cargo_id = {$this->arrData['codigo']}");
                    foreach ($requisitos as $e) {
                        $tipos = $e['tipos'];
                        foreach ($tipos as $f) {
                            $req = $f['requisitos'];
                            foreach ($req as $g) {
                                if($g['is_checked'] && $f['is_checked'] && $e['is_checked']){
                                    $sqlReq = "INSERT INTO planaccargos_requisitos(cargo_id,requisito_id) VALUES({$this->arrData['codigo']},{$g['id']})";
                                    mysqli_query($this->linkbd,$sqlReq);
                                }
                            }
                        }
                    }
                }
			}else{
				$request = "existe";
			}
			return $request;
		}
	}
?>
