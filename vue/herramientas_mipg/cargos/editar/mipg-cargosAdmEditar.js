const URL ='vue/herramientas_mipg/cargos/editar/mipg-cargosAdmEditar.php';
var app = new Vue({
	el: '#myapp',
	data() {
		return {
			isLoading: false,
			txtConsecutivo: '',
			arrConsecutivos:[],
			txtNombreCargo: '',
			selectJefe: 0,
			arrJefe: [],
			selectDependencia: 0,
			arrDependencia: [],
			selectNSalarial: 0,
			arrNSalarial: [],
            arrRequisitos:[],
		};
	},
	mounted(){
		this.getData();
	},
	methods: {
		getData: async function(){
			let codigo = new URLSearchParams(window.location.search).get('id');
			const formData = new FormData();
			formData.append("action","getData");
			formData.append("codigo",codigo);
			this.isLoading = true;
			const response = await fetch(URL,{method:"POST",body:formData});
			const objData = await response.json();
			this.arrJefe = objData.iniJefe;
			this.arrDependencia = objData.iniDependencia;
			this.arrNSalarial = objData.iniNSalarial;
            this.arrRequisitos = objData.requisitos;
			if(objData.status){
				const data = objData.data.data;
				this.txtConsecutivo = data.codcargo;
				this.txtNombreCargo = data.nombrecargo;
				this.selectJefe = data.codpadre;
				this.selectDependencia = data.dependencia;
				this.selectNSalarial = data.clasificacion;
				this.arrConsecutivos = objData.data.consecutivos;
			}else{
				codigo = objData.consecutivo;
				window.location.href="mipg-cargosAdmEditar.php?id="+codigo
			}
			this.isLoading = false;
		},
		save:function(){
			const vueContext = this;
			if(this.txtConsecutivo == "" || this.txtNombreCargo == "" || this.selectDependencia == 0 || this.selectNSalarial == 0){
				Swal.fire("Error","Todos los campos con (*) son obligatorios","error");
				return false;
			}
			Swal.fire({
				title:"¿Estás segur@ de guardar?",
				text:"",
				icon: 'warning',
				showCancelButton:true,
				confirmButtonColor: '#3085d6',
				cancelButtonColor: '#d33',
				confirmButtonText:"Sí, guardar",
				cancelButtonText:"No, cancelar"
			}).then(async function(result){
				if(result.isConfirmed){
					const formData = new FormData();
					formData.append("action","save");
					formData.append("nombre",vueContext.txtNombreCargo);
					formData.append("codigo",vueContext.txtConsecutivo);
					formData.append("jefe",vueContext.selectJefe);
					formData.append("dependencia",vueContext.selectDependencia);
					formData.append("nsalarial",vueContext.selectNSalarial);
                    formData.append("requisitos",JSON.stringify(vueContext.arrRequisitos));
					vueContext.isLoading = true;
					const response = await fetch(URL,{method:"POST",body:formData});
					const objData = await response.json();
					if(objData.status){
						Swal.fire("Guardado",objData.msg,"success");
                        //app.getData();
					}else{
						Swal.fire("Error",objData.msg,"error");
					}
					vueContext.isLoading = false;
				}
			});
		},
		editItem:function(type){
			let vueContext = this;
			let id = this.txtConsecutivo;
			let index = this.arrConsecutivos.findIndex(function(e){return e.codcargo == id});
			if(type=="next" && vueContext.arrConsecutivos[++index]){
				id = this.arrConsecutivos[index++].codcargo;
			}else if(type=="prev" && vueContext.arrConsecutivos[--index]){
				id = this.arrConsecutivos[index--].codcargo;
			}
			window.location.href="mipg-cargosAdmEditar?id="+id;
		},
		iratras:function(type){
			let id = this.txtConsecutivo;
			window.location.href="mipg-cargosAdmBuscar?id="+id;
		},
	}
});
