const URL ='vue/herramientas_mipg/cargos/buscar/mipg-cargosAdmBuscar.php';
var app = new Vue({
	el: '#myapp',
	data() {
		return {
			isLoading: false,
			txtSearch: '',
			txtResults: 0,
			arrData:[],
			arrDataCopy:[]
		};
	},
	mounted(){
		this.getData();
	},
	methods: {
		getData: async function(){
			const urlParams = new URLSearchParams(window.location.search);
			const id = urlParams.get('id');
			const formData = new FormData();
			formData.append("action","get");
			formData.append("id_gia",id);
			this.isLoading = true;
			const response = await fetch(URL,{method:"POST",body:formData});
			const objData = await response.json();
			this.arrData = objData;
			this.arrDataCopy = objData;
			this.txtResults = this.arrData.length;
			this.isLoading = false;

			this.$nextTick(() => {
				const highlightedRow = this.$refs.highlightedRow;
				if (highlightedRow && highlightedRow.length > 0) {
					const row = highlightedRow.find(row => row.classList.contains('bg-warning'));
					if (row) {
						row.scrollIntoView({ behavior: 'auto', block: 'center' });
					}
				}
			});
		},
		search:function(){
			let search = this.txtSearch.toLowerCase();
			this.arrDataCopy = [...this.arrData.filter(e=>e.nombrecargo.toLowerCase().includes(search)
			|| e.codcargo.toLowerCase().includes(search))];
			this.txtResults = this.arrDataCopy.length;
		},
		editItem:function(id){
			window.location.href='mipg-cargosAdmEditar.php?id='+id;
		},
		changeStatus:function(item){
			const vueThis = this;
			Swal.fire({
				title:"¿Estás segur@ de cambiar el estado?",
				text:"",
				icon: 'warning',
				showCancelButton:true,
				confirmButtonColor: '#3085d6',
				cancelButtonColor: '#d33',
				confirmButtonText:"Sí",
				cancelButtonText:"No, cancelar"
			}).then(async function(result){
				if(result.isConfirmed){
					const formData = new FormData();
					formData.append("action","change");
					formData.append("id",item.codcargo);
					formData.append("estado",item.estado =='S' ? 'N' : 'S');
					const response = await fetch(URL,{method:"POST",body:formData});
					const objData = await response.json();
					if(objData.status){
						let index = vueThis.arrData.findIndex(e => e.codcargo == item.codcargo);
						vueThis.arrData[index].estado = item.estado =='S' ? 'N' : 'S';
						vueThis.arrData[index].is_status = vueThis.arrData[index].estado =='S' ? 1 : 0;
						vueThis.search();
						Swal.fire("Estado actualizado","","success");
					}else{
						Swal.fire("Error",objData.msg,"error");
					}
				}else{
					vueThis.search();
				}
			});
		},
	}
});