<?php
	require_once '../../../../comun.inc';
	require '../../../../funciones.inc';
	require '../../../../funcionesSP.inc.php';
	session_start();
	if($_POST){
		$base = isset($_POST['base']) ?  base64_decode($_POST['base']) : null;
		$usuario = isset($_POST['usuario']) ? base64_decode($_POST['usuario']) : null;
		$obj = new Plantilla($base, $usuario);
		if($_POST['action']=="get"){
			$obj->getData();
		}else if($_POST['action']=="change"){
			$obj->changeData();
		}
	}
	class Plantilla{
		private $linkbd;
		public function __construct($base = null, $usuario = null) {
			if (!empty($base) && !empty($usuario)) {
				$this->linkbd = conectar_Multi($base, $usuario);
			} else {
				$this->linkbd = conectar_v7();
			}
			$this->linkbd->set_charset("utf8");
		}
		public function getData(){
			if(!empty($_SESSION)){
				echo json_encode($this->selectData(),JSON_UNESCAPED_UNICODE);
			}
			die();
		}
		public function selectData(){
			$sql = "SELECT codcargo, codpadre, nombrecargo, dependencia, clasificacion, estado FROM planaccargos ORDER BY codcargo ASC";
			$request = mysqli_fetch_all(mysqli_query($this->linkbd,$sql),MYSQLI_ASSOC);
			$total = count($request);
			for ($i=0; $i < $total; $i++) {
				$request[$i]['is_status'] = $request[$i]['estado'] == "S"? 1 : 0;
				$request[$i]['is_yellow'] = $request[$i]['codcargo'] == $_POST['id_gia']? 1 : 0;
				$request[$i]['nombreJefe'] = $this->jefeDirecto($request[$i]['codpadre']);
				$request[$i]['nomDependencia'] = $this->nombreDependencia($request[$i]['dependencia']);
				$request[$i]['tipoCargo'] = $this->tipoCargo($request[$i]['clasificacion']);
			}
			return $request;
		}
		public function jefeDirecto($codigo){
			if($codigo == 0){
				$nompadre['nombrecargo'] = "NINGUNO";
			} else {
				$sql = "SELECT nombrecargo FROM planaccargos WHERE codcargo = '$codigo'";
				$result = mysqli_query($this->linkbd, $sql);
				$nompadre = mysqli_fetch_assoc($result);
			}
			return $nompadre['nombrecargo'];
		}
		public function nombreDependencia($codigo){
			$sql = "SELECT nombrearea FROM planacareas WHERE  codarea = '$codigo'";
			$result = mysqli_query($this->linkbd, $sql);
			$nomarea = mysqli_fetch_assoc($result);
			return $nomarea['nombrearea'];
		}
		public function tipoCargo($codigo){
			$sql="SELECT nombre FROM humnivelsalarial WHERE id_nivel = '$codigo'";
			$result = mysqli_query($this->linkbd, $sql);
			$nomtipo = mysqli_fetch_assoc($result);
			return $nomtipo['nombre'];
		}
		public function changeData(){
			if(!empty($_SESSION)){
				$id = intval($_POST['id']);
				$estado = $_POST['estado'];
				$sql = "UPDATE planaccargos SET estado = '$estado' WHERE codcargo = $id";
				$request = mysqli_query($this->linkbd,$sql);
				if($request == 1){
					$arrResponse = array("status"=>true);
				}else{
					$arrResponse = array("status"=>false,"msg"=>"Error");
				}
				echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
			}
			die();
		}
	}
?>