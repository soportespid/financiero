<?php
	require_once '../../../../comun.inc';
	require '../../../../funciones.inc';
	require '../../../../funcionesSP.inc.php';
	session_start();
	if($_POST){
		$base = isset($_POST['base']) ?  base64_decode($_POST['base']) : null;
		$usuario = isset($_POST['usuario']) ? base64_decode($_POST['usuario']) : null;
		$obj = new Plantilla($base, $usuario);
		if($_POST['action']=="get"){
			$obj->getData();
		}else if($_POST['action']=="change"){
			$obj->changeData();
		}
	}
	class Plantilla{
		private $linkbd;
		public function __construct($base = null, $usuario = null) {
			if (!empty($base) && !empty($usuario)) {
				$this->linkbd = conectar_Multi($base, $usuario);
			} else {
				$this->linkbd = conectar_v7();
			}
			$this->linkbd->set_charset("utf8");
		}
		public function getData(){
			if(!empty($_SESSION)){
				echo json_encode($this->selectData(),JSON_UNESCAPED_UNICODE);
			}
			die();
		}
		public function selectData(){
			$sql = "SELECT codestter, codcargo, cedulanit, estado FROM planestructura_terceros ORDER BY codestter ASC";
			$request = mysqli_fetch_all(mysqli_query($this->linkbd,$sql),MYSQLI_ASSOC);
			$total = count($request);
			for ($i=0; $i < $total; $i++) {
				$request[$i]['is_status'] = $request[$i]['estado'] == "S"? 1 : 0;
				$request[$i]['is_yellow'] = $request[$i]['codestter'] == $_POST['id_gia']? 1 : 0;
				$varcargos = $this->nombreCargo($request[$i]['codcargo']);
				$request[$i]['nombrecargo'] = $varcargos['nombrecargo'];
				$request[$i]['nomDependencia'] = $this->nombreDependencia($varcargos['dependencia']);
				$request[$i]['nomFuncionario'] = $this->nombreTercero($request[$i]['cedulanit']);
			}
			return $request;
		}
		public function nombreCargo($codigo){
			$sql = "SELECT nombrecargo, dependencia FROM planaccargos WHERE codcargo = '$codigo'";
			$result = mysqli_query($this->linkbd, $sql);
			$nompadre = mysqli_fetch_assoc($result);
			return $nompadre;
		}
		public function nombreDependencia($codigo){
			$sql = "SELECT nombrearea FROM planacareas WHERE  codarea = '$codigo'";
			$result = mysqli_query($this->linkbd, $sql);
			$nomarea = mysqli_fetch_assoc($result);
			return $nomarea['nombrearea'];
		}
		public function nombreTercero($codigo){
			$sql = "SELECT nombre1, nombre2, apellido1, apellido2, razonsocial FROM terceros WHERE cedulanit = '$codigo'";
			$result = mysqli_query($this->linkbd, $sql);
			$nomarea = mysqli_fetch_assoc($result);
			$nombreArray = array_filter([ 
				$nomarea['nombre1'], 
				$nomarea['nombre2'], 
				$nomarea['apellido1'], 
				$nomarea['apellido2'], 
				$nomarea['razonsocial'] 
			]);
			$nombre = implode(' ', $nombreArray);
			return $nombre;
		}
		public function changeData(){
			if(!empty($_SESSION)){
				$id = intval($_POST['id']);
				$estado = $_POST['estado'];
				$sql = "UPDATE planestructura_terceros SET estado = '$estado' WHERE codestter = $id";
				$request = mysqli_query($this->linkbd,$sql);
				if($request == 1){
					$arrResponse = array("status"=>true);
				}else{
					$arrResponse = array("status"=>false,"msg"=>"Error");
				}
				echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
			}
			die();
		}
	}
?>