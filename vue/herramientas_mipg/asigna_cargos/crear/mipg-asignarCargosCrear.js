const URL ='vue/herramientas_mipg/asigna_cargos/crear/mipg-asignarCargosCrear.php';
var app = new Vue({
	el: '#myapp',
	data() {
		return {
			isLoading: false,
			isModal:false,
			txtConsecutivo: '',
			txtBuscar:"",
			txtResultados:0,
			selectCargo: 0,
			arrCargo: [],
			objTercero:{nombre:"",documento:""},
			arrTerceros:[],
			arrTercerosCopy:[],
		};
	},
	mounted(){
		this.getData();
		this.iniTercero();
	},
	methods: {
		getData: async function(){
			const formData = new FormData();
			formData.append("action","getData");
			this.isLoading = true;
			const response = await fetch(URL,{method:"POST",body:formData});
			const objData = await response.json();
			this.txtConsecutivo = objData.consecutivo;
			this.arrCargo = objData.iniCargo;
			this.isLoading = false;
		},
		iniTercero: async function() {
			const formData = new FormData();
            formData.append("action","iniTerceros");
            this.isLoading = true;
            const response = await fetch(URL,{method:"POST",body:formData});
            const objData = await response.json();
            this.arrTerceros = objData.terceros;
            this.arrTercerosCopy = objData.terceros;
            this.txtResultados = this.arrTerceros.length;
            if(objData.data.tercero != null){
                this.objTercero = objData.data.tercero;
            }
		},
		selectItem: function({...item}){
			this.objTercero = item;
			this.isModal = false;
		},
		search: function(type = "", objeto = "Tercero") {
			let obj = null; 
			if (type === "documento") {
				obj = this.arrTerceros.find(e => e.documento == this[objeto].documento); 
				if (obj) {
					this[objeto].nombre = obj.nombre; 
				} else { 
					this[objeto].documento = "";
					this[objeto].nombre = "";
					Swal.fire( 
						'No encontrado', 
						'No se encuentra almacenado ese tercero.', 
						'warning' 
					); 
				} 
			} else { 
				let search = this.txtBuscar.toLowerCase(); 
				this.arrTercerosCopy = this.arrTerceros.filter(e => e.nombre.toLowerCase().includes(search) || e.documento.toLowerCase().includes(search) ); 
				this.txtResultados = this.arrTercerosCopy.length; 
			} 
		},
		save: async function() {
			const vueContext = this;
			if (this.txtConsecutivo == "" || this.objTercero.documento == "" || this.objTercero.nombre == '' || this.selectCargo == 0) {
				Swal.fire("Error", "Todos los campos con (*) son obligatorios", "error");
				return false;
			}
			Swal.fire({
				title: "¿Estás segur@ de guardar?",
				text: "",
				icon: 'warning',
				showCancelButton: true,
				confirmButtonColor: '#3085d6',
				cancelButtonColor: '#d33',
				confirmButtonText: "Sí, guardar",
				cancelButtonText: "No, cancelar"
			}).then(async function(result) {
				if (result.isConfirmed) {
					const formData = new FormData();
					formData.append("action", "save");
					formData.append("codigo", vueContext.txtConsecutivo);
					formData.append("documento", vueContext.objTercero.documento);
					formData.append("nombre", vueContext.objTercero.nombre);
					formData.append("cargo", vueContext.selectCargo);
					vueContext.isLoading = true;
					const response = await fetch(URL, { method: "POST", body: formData });
					const objData = await response.json();
					if (objData.status) {
						Swal.fire({
							title: "Guardado",
							text: objData.msg,
							icon: "success",
							confirmButtonText: "OK"
						}).then((result) => {
							if (result.isConfirmed) {
								const id = vueContext.txtConsecutivo;
								window.location.href = 'mipg-asignarCargosEditar.php?id=' + id;
							}
						});
						setTimeout(() => {
							const id = vueContext.txtConsecutivo;
							window.location.href = 'mipg-asignarCargosEditar.php?id=' + id;
						}, 2000);
					} else {
						Swal.fire("Error", objData.msg, "error");
					}
					vueContext.isLoading = false;
				}
			});
		},
		editItem:function(id){
			window.location.href='mipg-asignarCargosEditar.php?id='+id;
		},
	}
});