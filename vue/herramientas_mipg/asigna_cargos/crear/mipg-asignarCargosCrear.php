<?php
	require_once '../../../../comun.inc';
	require '../../../../funciones.inc';
	require '../../../../funcionesSP.inc.php';
	session_start();
	if($_POST){
		$base = isset($_POST['base']) ?  base64_decode($_POST['base']) : null;
		$usuario = isset($_POST['usuario']) ? base64_decode($_POST['usuario']) : null;
		$obj = new Plantilla($base, $usuario);
		switch ($_POST['action']){
			case 'getData':
				$obj->getData();
			break;
			case 'iniTerceros':
				$obj->iniTerceros();
			break;
			case 'save':
				$obj->save();
			break;
			
		}
	}
	class Plantilla{
		private $linkbd;
		public function __construct($base = null, $usuario = null) {
			if (!empty($base) && !empty($usuario)) {
				$this->linkbd = conectar_Multi($base, $usuario);
			} else {
				$this->linkbd = conectar_v7();
			}
			$this->linkbd->set_charset("utf8");
		}
		public function getData(){
			if(!empty($_SESSION)){
				$arrData = array(
					"consecutivo"=>$this->selectConsecutivo(),
					"iniCargo"=>$this->selectCargo(),
				);
				echo json_encode($arrData,JSON_UNESCAPED_UNICODE);
			}
			die();
		}
		public function selectConsecutivo(){
			$sql = "SELECT MAX(codestter) AS max FROM planestructura_terceros";
			$request = mysqli_query($this->linkbd,$sql)->fetch_assoc()['max']+1;
			return $request;
		}
		public function selectCargo(){
			$sql = "SELECT codcargo, nombrecargo FROM planaccargos ORDER BY codcargo ASC";
			$request = mysqli_fetch_all(mysqli_query($this->linkbd,$sql),MYSQLI_ASSOC);
			return $request;
		}
		public function iniTerceros(){
			if(!empty($_SESSION)){
				$arrData = array(
					"terceros" =>$this->selectTerceros()
				);
				echo json_encode($arrData,JSON_UNESCAPED_UNICODE);
			}
			die();
		}
		public function selectTerceros(){
			$sql = "SELECT CONCAT(razonsocial,' ',nombre1,' ',nombre2,' ',apellido1,' ',apellido2) as nombre, cedulanit as documento
			FROM terceros WHERE estado = 'S' AND empleado = '1'";
			$request = mysqli_fetch_all(mysqli_query($this->linkbd,$sql),MYSQLI_ASSOC);
			return $request;
		}
		public function save(){
			if(!empty($_SESSION)){
				$varCodigo = $_POST['codigo'];
				$varDocumento = $_POST['documento'];
				$varCargo = $_POST['cargo'];
				$request = $this->insertData(array("codigo"=>$varCodigo, "cargo"=>$varCargo, "documento"=>$varDocumento));
				if(is_numeric($request) && $request> 0 ){
					$arrResponse = array("status"=>true,"msg"=>"Datos guardados.");
				}else if($request =="existe"){
					$arrResponse = array("status"=>false,"msg"=>"El nombre del ya existe, pruebe con otro.");
				}else{
					$arrResponse = array("status"=>false,"msg"=>"Ha ocurrido un error, intente de nuevo.");
				}
				echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
			}
			die();
		}
		public function insertData(array $data){
			$this->arrData = $data;
			$request = mysqli_fetch_all(mysqli_query($this->linkbd,"SELECT * FROM planestructura_terceros WHERE codcargo = '{$this->arrData['cargo']}' AND cedulanit = '{$this->arrData['documento']}'"),MYSQLI_ASSOC);
			if(empty($request)){
				$sql = "INSERT INTO planestructura_terceros (codestter, codcargo, cedulanit, estado) VALUES (
					'{$this->arrData['codigo']}',
					'{$this->arrData['cargo']}',
					'{$this->arrData['documento']}',
					'S'
				)";
				$request = intval(mysqli_query($this->linkbd,$sql));
			}else{
				$request = "existe";
			}
			return $request;
		}
	}
?>