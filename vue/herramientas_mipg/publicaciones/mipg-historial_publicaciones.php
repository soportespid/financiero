<?php
	require_once '../../../comun.inc';
	require '../../../funciones.inc';
	session_start();
	if($_POST){
		$obj = new Plantilla();
		switch ($_POST['action']){
			case 'get':
				$obj->getData();
				break;
		}
	}
	class Plantilla{
		private $linkbd;
		private $arrData;
		public function __construct() {
			$this->linkbd = conectar_v7();
			$this->linkbd->set_charset("utf8");
		}
		public function getData(){
			if(!empty($_SESSION)){
				echo json_encode($this->selectData(),JSON_UNESCAPED_UNICODE);
			}
			die();
		}
		public function selectData(){
			$sql = "SELECT indices, fecha_inicio, titulos, texnombre FROM infor_interes ORDER BY indices DESC";
			$request = mysqli_fetch_all(mysqli_query($this->linkbd,$sql),MYSQLI_ASSOC);
			$total = count($request);
			for ($i=0; $i < $total; $i++) {
				$archivo = "../../../informacion/archivos/".$request[$i]['texnombre'];
				if (file_exists($archivo)) {
					$handle = fopen($archivo, 'r');
					if ($handle) {
						$contenido = fread($handle, 500);
						fclose($handle);
						$request[$i]['descripcion'] = $contenido;
					} else {
						$request[$i]['descripcion'] = 'No se pudo abrir el archivo';
					}
				} else {
					$request[$i]['descripcion'] = 'Archivo no encontrado';
				}
			}
			return $request;
		}
	}
?>