const URL ='vue/herramientas_mipg/publicaciones/mipg-historial_publicaciones.php';
var app = new Vue({ 
	el: '#myapp',
	data() {
		return {
			txtResults:0,
			txtSearch:"",
			arrData:[],
			arrDataCopy:[],
			showModal_programaticoProyectos: false,
			noMember: false,
		};
	},
	mounted(){
		this.getData();
	},
	methods: {
		getData: async function(){
			const formData = new FormData();
			formData.append("action","get");
			const response = await fetch(URL,{method:"POST",body:formData});
			const objData = await response.json();
			this.arrData = objData;
			this.arrDataCopy = objData;
			this.txtResults = this.arrData.length;
		},
		loadContentIntoModal(url) {
			const url2 = "plan-mostrainformacion.php?idinfo=" + url;									
			this.showModal_programaticoProyectos = true;
			fetch(url2)
			.then(response => response.text())
			.then(html => {
				document.querySelector('.modal-body').innerHTML = html;
			})
			.catch(error => {
				console.error('Error al cargar el contenido:', error);
			});
		},
		search() {
			let search = this.txtSearch.toLowerCase();
			this.arrDataCopy = this.arrData.filter(e => 
				e.titulos.toLowerCase().includes(search)
			);
			this.txtResults = this.arrDataCopy.length;
		}
	}
});