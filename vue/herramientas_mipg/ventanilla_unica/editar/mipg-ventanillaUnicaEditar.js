const URL ='vue/herramientas_mipg/ventanilla_unica/editar/mipg-ventanillaUnicaEditar.php';
var app = new Vue({
	el: '#myapp',
	data() {
		return {
			isLoading:false,
			isModal:false,
			isModal2:false,
			txtDivipola: '',
			txtRadicado: '',
			txtFecha: '',
			txtHora: '',
			selectTipoRadica: '0',
			arrTipoRadica: [],
			txtFechaLimite: '00/00/0000',
			txtDescripcion: '',
			checkEscrita: 0,
			checkTelefonica: 0,
			checkEmail: 0,
			txtFolios: '',
			selectTipopqr: 'O',
			txtBuscar: '',
			objTercero:{nombre:"", documento:"", direccion:"", email:"", telefono:"", celular:""},
			arrTerceros: [],
			arrTercerosCopy: [],
			txtResultados: 0,
			txtBuscar2: '',
			objFuncionario:{nombre:"", documento:"", nombrecargo:"", codcargo:"", dependencia:"", nombrearea:"", usuarioest:""},
			arrFuncionarios: [],
			arrFuncionariosCopy: [],
			txtResultados2: 0,
			listaFuncionarios: [],
			listaArchivos: []
		};
	},
	mounted: async function(){
		
		await this.infoInicial();
		this.iniTipoRadica();
		this.iniTercero();
		this.iniFuncionario();
	},
	methods: {
		infoInicial: async function(){
			let codigo = new URLSearchParams(window.location.search).get('id');
			const formData = new FormData();
			formData.append("action","infoInicial");
			formData.append("codigo",codigo);
			this.isLoading = true;
			const response = await fetch(URL,{method:"POST",body:formData});
			const objData = await response.json();
			if(objData.status){
				const data = objData.data.data;
				const planacradicacion = objData.data.consecutivos.planacradicacion[0];
				const planradicacionsindoc = objData.data.consecutivos.planradicacionsindoc[0];
				const planacresponsables = objData.data.consecutivos.planacresponsables;
				const planacarchivosad = objData.data.consecutivos.planacarchivosad; console.log(planacarchivosad);
				//carga informacion pestaña 1
				this.txtRadicado = planacradicacion.numeror;
				this.txtDivipola = planacradicacion.divipola;
				this.txtFecha = this.formatDate(planacradicacion.fechar);
				this.txtHora = this.convertTime24to12(planacradicacion.horar);
				await this.iniTipoRadica();
				this.selectTipoRadica = this.arrTipoRadica.find(item => item.codigo === planacradicacion.tipor);
				if(planacradicacion.fechalimite != '0000-00-00'){
					this.txtFechaLimite = this.formatDate(planacradicacion.fechalimite);
				}
				this.txtDescripcion = planacradicacion.descripcionr;
				this.checkTelefonica = planacradicacion.ttelefono === '1';
				this.checkEscrita = planacradicacion.tescrito === '1';
				this.checkEmail = planacradicacion.temail === '1';
				this.selectTipopqr = planacradicacion.mrecepcion;
				this.txtFolios = planacradicacion.nfolios;
				//carga informacion pestaña 2
				if(planacradicacion.idtercero != "" && planacradicacion.idtercero != null){
					this.objTercero.documento = planacradicacion.idtercero;
					this.objTercero.nombre = planacradicacion.nombreTerceros;
					this.objTercero.direccion = data.planacradicacion.direcciont;
					this.objTercero.email = data.planacradicacion.emailt;
					this.objTercero.telefono = data.planacradicacion.telefonot;
					this.objTercero.celular = data.planacradicacion.celulart;
				} else {
					this.objTercero.documento = '';
					this.objTercero.nombre = planradicacionsindoc.nombre;
					this.objTercero.direccion = planradicacionsindoc.direccion;
					this.objTercero.email = planradicacionsindoc.email;
					this.objTercero.telefono = planradicacionsindoc.telefono;
					this.objTercero.celular = planradicacionsindoc.celular;
				}
				//carga informacion pestaña 3
				const numeroResponsables = Array.isArray(planacresponsables) ? planacresponsables.length : 0;
				for (let i = 0; i < numeroResponsables; i++) {
					let datosFuncionario = {
						funNombre: planacresponsables[i].nombreFuncionario,
						funDocumento: planacresponsables[i].usuariocon,
						funNombreCargo: planacresponsables[i].nombreCargo,
						funCodCargo: planacresponsables[i].codcargo,
						funDependencia: planacresponsables[i].dependencia,
						funNombreArea: planacresponsables[i].nombreDependencia,
						funEstado: planacresponsables[i].usuarioest,
						funResponsable: Number(planacresponsables[i].estadores)
					}
					this.listaFuncionarios.push(datosFuncionario); 
				}
				//carga informacion pestaña 4
				const numeroArchivos = Array.isArray(planacarchivosad) ? planacarchivosad.length : 0;
				for (let i = 0; i < numeroArchivos; i++) { 
					let archivo = { 
						name: planacarchivosad[i].nomarchivo, 
						size: planacarchivosad[i].tamano || 0, 
						path: `informacion/documentosradicados/RA/${planacarchivosad[i].idradicacion}/${planacarchivosad[i].nomarchivo}` 
					}; 
					this.listaArchivos.push(archivo);
				}
			}else{
				codigo = objData.consecutivo;
				//window.location.href="mipg-ventanillaUnicaEditar?id="+codigo
			}
			this.isLoading = false;
		},
		formatDate: function (fecha) { 
			let date = new Date(fecha); 
			let day = ("0" + date.getDate()).slice(-2); 
			let month = ("0" + (date.getMonth() + 1)).slice(-2); 
			let year = date.getFullYear(); 
			return `${day}/${month}/${year}`;
		},
		convertTime24to12: function (time24) { 
			const [hours, minutes, seconds] = time24.split(':'); 
			let period = 'a.m.';
			let hours12 = parseInt(hours, 10); 
			if (hours12 >= 12) { 
				period = 'p.m.'; 
				if (hours12 > 12) { 
					hours12 -= 12; 
				} 
			} else if (hours12 === 0) { 
				hours12 = 12; 
			} 
			const formattedHours = ("0" + hours12).slice(-2); 
			const formattedMinutes = ("0" + minutes).slice(-2); 
			const formattedSeconds = ("0" + seconds).slice(-2); 
			return `${formattedHours}:${formattedMinutes}:${formattedSeconds} ${period}`;
		},
		iniTipoRadica: async function(){
			const formData = new FormData();
			formData.append("action","iniTipoRadica");
			this.isLoading = true;
			const response = await fetch(URL,{method:"POST",body:formData});
			const objData = await response.json();
			this.arrTipoRadica = objData;
			this.isLoading = false;
		},
		cambioTipoRadica: function() {
			if (this.selectTipoRadica) {
				const { codigo, dias, tdias, slectura, adjunto, clasificacion } = this.selectTipoRadica;
				if(tdias != 'N'){
					this.txtFechaLimite = this.sumarDiasHabiles(this.txtFecha, dias, tdias);
				} else {
					this.txtFechaLimite = '00/00/00'
				}
				if(slectura == 'S'){
					this.checkEscrita = 1;
				} else {
					this.checkEscrita = 0;
					this.checkTelefonica = 0;
					this.checkEmail = 0;
				}
			} 
		},
		sumarDiasHabiles: function(fechaStr, diasHabiles, tipoDias) {
			let partesFecha = fechaStr.split('/');
			let fecha = new Date(partesFecha[2], partesFecha[1] - 1, partesFecha[0]);
			let contadorDias = 0;
			while (diasHabiles > 0) {
				fecha.setDate(fecha.getDate() + 1);
				contadorDias++;
				if (tipoDias === 'H') {
					if (fecha.getDay() !== 6 && fecha.getDay() !== 0) {
						diasHabiles--;
					}
				} else if (tipoDias === 'C') {
					diasHabiles--;
				}
			}
			let nuevaFecha = `${('0' + fecha.getDate()).slice(-2)}/${('0' + (fecha.getMonth() + 1)).slice(-2)}/${fecha.getFullYear()}`;
			return nuevaFecha;
		},
		iniTercero: async function() {
			const formData = new FormData();
			formData.append("action","iniTerceros");
			this.isLoading = true;
			const response = await fetch(URL,{method:"POST",body:formData});
			const objData = await response.json(); 
			this.arrTerceros = objData.terceros;
			this.arrTercerosCopy = objData.terceros;
			this.txtResultados = this.arrTerceros.length;
			if(objData.data.tercero != null){
				this.objTercero = objData.data.tercero;
			}
		},
		search: function(type = "", objeto = "objTercero") { 
			let obj = null; 
			if (type === "documento") { 
				obj = this.arrTerceros.find(e => e.documento == this[objeto].documento); 
				if (obj) { 
					this[objeto].nombre = obj.nombre || ""; 
					this[objeto].direccion = obj.direccion || "Sin Información"; 
					this[objeto].email = obj.email || "Sin Información"; 
					this[objeto].telefono = obj.telefono || "Sin Información"; 
					this[objeto].celular = obj.celular || "Sin Información"; 
				} else { 
					this[objeto].documento = ""; 
					this[objeto].nombre = ""; 
					this[objeto].direccion = "Sin Información"; 
					this[objeto].email = "Sin Información"; 
					this[objeto].telefono = "Sin Información"; 
					this[objeto].celular = "Sin Información"; 
					Swal.fire('No encontrado', 'No se encuentra almacenado ese tercero.', 'warning'); 
				} 
			} else { 
				let search = this.txtBuscar.toLowerCase(); 
				this.arrTercerosCopy = this.arrTerceros.filter(e => e.nombre.toLowerCase().includes(search) || e.documento.toLowerCase().includes(search)); 
				if (this.arrTercerosCopy.length === 0) { 
					this.objTercero.nombre = ""; 
					this.objTercero.direccion = "Sin Información"; 
					this.objTercero.email = "Sin Información"; 
					this.objTercero.telefono = "Sin Información"; 
					this.objTercero.celular = "Sin Información"; 
				} this.txtResultados = this.arrTercerosCopy.length; 
			} 
		},
		selectItem: function({ ...item }) { 
			this.objTercero.nombre = item.nombre || "";
			this.objTercero.documento = item.documento || "Sin Información";
			this.objTercero.direccion = item.direccion || "Sin Información";
			this.objTercero.email = item.email || "Sin Información";
			this.objTercero.telefono = item.telefono || "Sin Información";
			this.objTercero.celular = item.celular || "Sin Información";
			this.isModal = false;
		},
		iniFuncionario: async function() {
			const formData = new FormData();
			formData.append("action","iniFuncionarios");
			this.isLoading = true;
			const response = await fetch(URL,{method:"POST",body:formData});
			const objData = await response.json();
			this.arrFuncionarios = objData.funcionarios;
			this.arrFuncionariosCopy = objData.funcionarios;
			this.txtResultados2 = this.arrFuncionarios.length;
			if(objData.data.funcionario != null){
				this.objFunsionario = objData.data.funcionario;
			}
			this.filterFuncionarios();
		},
		search2: function(type = "", objeto = "objFuncionario") {
			let obj = null; 
			if (type === "documento") {
				obj = this.arrFuncionarios.find(e => e.documento == this[objeto].documento); 
				if (obj && !this.listaFuncionarios.some(funcionario => funcionario.funDocumento === obj.documento)) { 
					this[objeto].nombre = obj.nombre;
					this[objeto].nombrecargo = obj.nombrecargo;
					this[objeto].codcargo = obj.codcargo;
					this[objeto].dependencia = obj.dependencia;
					this[objeto].nombrearea = obj.nombrearea;
					this[objeto].usuarioest = obj.usuarioest;
				} else { 
					this[objeto].documento = ""; 
					Swal.fire('No encontrado', 'No se encuentra almacenado ese funcionario o ya está agregado.', 'warning'); 
				}
			} else { 
				this.filterFuncionarios();
			} 
		},
		filterFuncionarios: function() { 
			let search = this.txtBuscar2.toLowerCase(); 
			this.arrFuncionariosCopy = this.arrFuncionarios.filter(e => { 
				return (e.nombre.toLowerCase().includes(search) || e.documento.toLowerCase().includes(search) || e.nombrecargo.toLowerCase().includes(search)) && !this.listaFuncionarios.some(funcionario => funcionario.funDocumento === e.documento); 
			}); 
			this.txtResultados2 = this.arrFuncionariosCopy.length; 
		},
		selectItem2: function({...item}){
			this.objFuncionario = item;
			this.isModal2 = false;
		},
		agregarFuncionario: function() {
			if (this.objFuncionario.nombre != "" && this.objFuncionario.documento != "" && this.objFuncionario.codcargo != "") {
				let datosFuncionario = {
					funNombre: this.objFuncionario.nombre,
					funDocumento: this.objFuncionario.documento,
					funNombreCargo: this.objFuncionario.nombrecargo,
					funCodCargo: this.objFuncionario.codcargo,
					funDependencia: this.objFuncionario.dependencia,
					funNombreArea: this.objFuncionario.nombrearea,
					funEstado: this.objFuncionario.usuarioest,
					funResponsable: this.listaFuncionarios.length > 0 ? 0 : 1
				}
				this.listaFuncionarios.push(datosFuncionario);
				
				datosFuncionario = [];
				this.objFuncionario = { nombre: "", documento: "", nombrecargo: "", codcargo: "", dependencia: "", nombrearea:"", usuarioest:"" };
				this.filterFuncionarios();
			}
			else {
				Swal.fire("Atención!","Todos los campos con (*) son obligatorios","warning");
			}
		},
		deleteFuncionario: function(index) {
            this.listaFuncionarios.splice(index, 1);
			this.filterFuncionarios();
        },
		changeStatus: function(index) { 
			if (this.listaFuncionarios[index].funResponsable === 0) { 
				this.listaFuncionarios.forEach((funcionario, idx) => {
					if (idx === index) { 
						funcionario.funResponsable = 1; 
					} else { 
						funcionario.funResponsable = 0; 
					} 
				}); 
			} else { 
				this.listaFuncionarios[index].funResponsable = 0; 
			} 
		},
		agregarArchivo: function(event) {
			const archivo = event.target.files[0];
			if (archivo) {
				this.listaArchivos.push(archivo);
			}
		},
		eliminarArchivo: function(index) {
			this.listaArchivos.splice(index, 1); 
		},
		showTab:function(tab){
			let tabs = this.$refs.rTabs.children;
			let tabsContent = this.$refs.rTabsContent.children;
			for (let i = 0; i < tabs.length; i++) {
				tabs[i].classList.remove("active");
				tabsContent[i].classList.remove("active")
			}
			tabs[tab-1].classList.add("active");
			tabsContent[tab-1].classList.add("active")
		},
		save: async function() {
			if (this.selectTipoRadica && this.txtDescripcion && this.txtFolios && this.selectTipoRadica.codigo) {
				if (this.objTercero.nombre && this.objTercero.direccion && this.objTercero.email && this.objTercero.telefono && this.objTercero.celular) {
					if (this.listaFuncionarios.length == 0) {
						this.showTab(3);
						Swal.fire('¡Error!', 'Falta ingresar responsables en el Modulo de "Asignar Responsables".', 'error');
					} else {
						if (this.listaArchivos.length == 0) {
							this.showTab(4);
							Swal.fire('¡Error!', 'Falta adjuntar archivos en el Modulo de "Archivos Adjuntos".', 'error');
						} else {
							Swal.fire({
								title: "¿Estás segur@ de guardar?",
								icon: 'warning',
								showCancelButton: true,
								confirmButtonColor: '#3085d6',
								cancelButtonColor: '#d33',
								confirmButtonText: "Sí, guardar",
								cancelButtonText: "No, cancelar"
							}).then(async function(result) {
								if (result.isConfirmed) {
									const formData = new FormData();
									formData.append("action", "save");
									formData.append("aDivipola", app.txtDivipola);
									formData.append("aRadicado", app.txtRadicado);
									formData.append("aFecha", app.txtFecha);
									formData.append("aHora", app.txtHora);
									formData.append("aTRcodigo", app.selectTipoRadica.codigo);
									formData.append("aTRslectura", app.selectTipoRadica.slectura);
									formData.append("aTRadjunto", app.selectTipoRadica.adjunto);
									formData.append("aTRclasificacion", app.selectTipoRadica.clasificacion);
									formData.append("aFechaLimite", app.txtFechaLimite);
									formData.append("aDescripcion", app.txtDescripcion);
									formData.append("aTelefonica", app.checkTelefonica);
									formData.append("aEscrita", app.checkEscrita);
									formData.append("aEmail", app.checkEmail);
									formData.append("aTipopqr", app.selectTipopqr);
									formData.append("aFolios", app.txtFolios);
									formData.append("bdocumento", app.objTercero.documento);
									formData.append("bnombre", app.objTercero.nombre);
									formData.append("bdireccion", app.objTercero.direccion);
									formData.append("bemail", app.objTercero.email);
									formData.append("btelefono", app.objTercero.telefono);
									formData.append("bcelular", app.objTercero.celular);
									formData.append("clistaFuncionarios", JSON.stringify(app.listaFuncionarios));
									app.listaArchivos.forEach(archivo => { 
										formData.append('archivos[]', archivo); 
									}); 
									
									formData.append('txtRadicado', app.txtRadicado); 
									app.isLoading = true;
									try {
										const response = await fetch(URL, { method: "POST", body: formData });
										if (!response.ok) {
											throw new Error(`HTTP error! status: ${response.status}`);
										}
										const objData = await response.json();
										if (objData.status) {
											app.isLoading = false;
											Swal.fire({
												title: "Guardado",
												text: objData.msg,
												icon: "success",
												confirmButtonText: "OK"
											});
										} else {
											Swal.fire("Error", objData.msg, "error");
											console.error("Errores: ", objData.errors);
										}
									} catch (error) {
										console.error('Error en la solicitud:', error);
										Swal.fire("Error", "No se pudo completar la solicitud. Intente nuevamente.", "error");
									} finally {
										app.isLoading = false;
									}
								}
							});
						}
					}
				} else {
					this.showTab(2);
					Swal.fire('¡Error!', 'Falta diligenciar información en el Modulo de "informacón Remitente".', 'error');
				}
			} else {
				this.showTab(1);
				Swal.fire('¡Error!', 'Falta diligenciar información en el Modulo de "informacón General".', 'error');
			}
		}
	}
});