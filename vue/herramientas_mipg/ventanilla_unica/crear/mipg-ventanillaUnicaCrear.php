<?php
	require_once '../../../../comun.inc';
	require '../../../../funciones.inc';
	require '../../../../funcionesSP.inc.php';
	session_start();
	if($_POST){
		$base = isset($_POST['base']) ?  base64_decode($_POST['base']) : null;
		$usuario = isset($_POST['usuario']) ? base64_decode($_POST['usuario']) : null;
		$obj = new Plantilla($base, $usuario);
		switch ($_POST['action']){
			case 'infoInicial':
				$obj->getData();
			break;
			case 'iniTipoRadica':
				$obj->iniTipoRadica();
			break;
			case 'iniTerceros':
				$obj->iniTerceros();
			break;
			case 'iniFuncionarios':
				$obj->iniFuncionarios();
			break;
			case 'save':
				$obj->guardaRadicacion();
			break;
			case 'uploadFiles': 
				$obj->uploadFiles($_POST['txtRadicado'], $_FILES['archivos']); 
			break;
			
		}
	}
	class Plantilla{
		private $linkbd;
		public function __construct($base = null, $usuario = null) {
			if (!empty($base) && !empty($usuario)) {
				$this->linkbd = conectar_Multi($base, $usuario);
			} else {
				$this->linkbd = conectar_v7();
			}
			$this->linkbd->set_charset("utf8");
		}
		public function getData(){
			if(!empty($_SESSION)){
				$arrData = array(
					"consecutivo"=>$this->selectConsecutivo(),
					"divipola"=>$this->selectDivipola()
				);
				echo json_encode($arrData,JSON_UNESCAPED_UNICODE);
			}
			die();
		}
		public function selectConsecutivo(){
			$vigusu = date('Y');
			$sql = "SELECT MAX(numeror) AS max FROM planacradicacion WHERE tipot = 'RA' AND YEAR(FECHAR) = '$vigusu'";
			$request = mysqli_fetch_all(mysqli_query($this->linkbd,$sql),MYSQLI_ASSOC);
			$mx = $request[0]['max'];
			if ($mx==Null || $mx ==0){
				$mx = $vigusu."000001"; 
			} else {
				$mx++; 
			}
			return $mx;
		}
		public function selectDivipola(){
			$sql = "SELECT depto, mnpio FROM configbasica";
			$request = mysqli_fetch_all(mysqli_query($this->linkbd,$sql),MYSQLI_ASSOC);
			$divipola = $request[0]['depto'].$request[0]['mnpio'];
			return $divipola;
		}
		public function iniTipoRadica(){
			if(!empty($_SESSION)){
				echo json_encode($this->selectTipoRadica(),JSON_UNESCAPED_UNICODE);
			}
			die();
		}
		public function selectTipoRadica(){
			$sql = "SELECT codigo, nombre, dias, tdias, slectura, adjunto, clasificacion  FROM plantiporadicacion WHERE estado = 'S' AND radotar = 'RA' ORDER BY nombre ASC";
			$request = mysqli_fetch_all(mysqli_query($this->linkbd,$sql),MYSQLI_ASSOC);
			$total = count($request);
			$tiposPQR = array( 
				"N" => "N - Ninguno",
				"P" => "P - Petición",
				"Q" => "Q - Queja",
				"R" => "R - Reclamo",
				"S" => "S - Sugerencia",
				"D" => "D - Denuncia",
				"F" => "F - Felicitación"
			);
			for ($i = 0; $i < $total; $i++) {
				$request[$i]['tipoPQR'] = isset($tiposPQR[$request[$i]['clasificacion']]) ? $tiposPQR[$request[$i]['clasificacion']] : $request[$i]['clasificacion'];
			}
			return $request;
		}
		public function iniTerceros(){
			if(!empty($_SESSION)){
				$arrData = array(
					"terceros" =>$this->selectTerceros()
				);
				echo json_encode($arrData,JSON_UNESCAPED_UNICODE);
			}
			die();
		}
		public function selectTerceros(){
			$sql = "SELECT CONCAT(razonsocial,' ',nombre1,' ',nombre2,' ',apellido1,' ',apellido2) AS nombre, cedulanit AS documento, direccion, email, telefono, celular FROM terceros WHERE estado = 'S'";
			$request = mysqli_fetch_all(mysqli_query($this->linkbd,$sql),MYSQLI_ASSOC);
			return $request;
		}
		public function iniFuncionarios(){
			if(!empty($_SESSION)){
				$arrData = array(
					"funcionarios" =>$this->selectFuncionarios()
				);
				echo json_encode($arrData,JSON_UNESCAPED_UNICODE);
			}
			die();
		}
		public function selectFuncionarios(){
			$sql="SELECT CONCAT(t.razonsocial, ' ', t.nombre1, ' ', t.nombre2, ' ', t.apellido1, ' ', t.apellido2) AS nombre, t.cedulanit AS documento, pl.nombrecargo, pl.codcargo, pl.dependencia , pa.nombrearea
			FROM planestructura_terceros pt 
			INNER JOIN terceros t ON pt.cedulanit = t.cedulanit 
			INNER JOIN planaccargos pl ON pt.codcargo = pl.codcargo 
			INNER JOIN planacareas pa ON pa.codarea = pl.dependencia WHERE pt.estado = 'S';";
			$request = mysqli_fetch_all(mysqli_query($this->linkbd,$sql),MYSQLI_ASSOC);
			$total = count($request);
			for ($i=0; $i < $total; $i++) {
				$sql = "SELECT id_usu FROM usuarios WHERE cc_usu = '".$request[$i]['documento']."' AND est_usu = '1'";
				$usuarios = mysqli_fetch_all(mysqli_query($this->linkbd,$sql),MYSQLI_ASSOC);
				$totalUsuarios = count($usuarios);
				$request[$i]['usuarioest'] = $totalUsuarios > 0 ? 'S' : 'N';
			}
			return $request;
		}
		/*public function uploadFiles($txtRadicado, $files) {
			$uploadDir = '../../../../informacion/documentosradicados/RA/' . $txtRadicado . '/'; 
			if (!file_exists($uploadDir)) { 
				mkdir($uploadDir, 0777, true); 
			} $idArchivoad = $this->getNextArchivoId(); 
			foreach ($files['name'] as $key => $name) { 
				$tmpName = $files['tmp_name'][$key]; 
				$destino = $uploadDir . $name; 
				if (move_uploaded_file($tmpName, $destino)) { 
					$this->saveFileNameToDatabase($idArchivoad++, $txtRadicado, $name); 
				} 
			}
		}*/
		public function uploadFiles($txtRadicado, $files) {
			$uploadDir = '../../../../informacion/documentosradicados/RA/' . $txtRadicado . '/'; 
			$zipDir = '../../../../informacion/documentosradicados/RA/zip/'; 
			if (!file_exists($uploadDir)) { 
				mkdir($uploadDir, 0777, true); 
			}
			if (!file_exists($zipDir)) { 
				mkdir($zipDir, 0777, true); 
			}
			$zip = new ZipArchive();
			$zipFile = $zipDir . $txtRadicado . '.zip';
			if ($zip->open($zipFile, ZipArchive::CREATE | ZipArchive::OVERWRITE) === TRUE) {
				foreach ($files['name'] as $key => $name) { 
					$tmpName = $files['tmp_name'][$key]; 
					$destino = $uploadDir . $name; 
					
					// Mover archivo a la carpeta correspondiente
					if (move_uploaded_file($tmpName, $destino)) { 
						// Agregar archivo al ZIP
						if ($zip->addFile($destino, $name)) {
							// Archivo agregado al ZIP exitosamente
						} else {
							echo 'Error al agregar el archivo: ' . $name;
						}
						$this->saveFileNameToDatabase($this->getNextArchivoId(), $txtRadicado, $name);
					} else {
						echo 'Error al mover el archivo: ' . $name;
					}
				}
				$zip->close();
			} else {
				echo 'Error al crear el archivo ZIP';
			}
		}
		private function getNextArchivoId() { 
			$sql = "SELECT MAX(idarchivoad) as max_id FROM planacarchivosad"; 
			$request = mysqli_fetch_all(mysqli_query($this->linkbd, $sql), MYSQLI_ASSOC); 
			$maxId = $request[0]['max_id']; 
			return $maxId + 1; 
		}
		private function saveFileNameToDatabase($idArchivoad, $txtRadicado, $name) { 
			$sql = "INSERT INTO planacarchivosad (idarchivoad, idradicacion, nomarchivo) VALUES (?, ?, ?)"; 
			$stmt = $this->linkbd->prepare($sql); 
			$stmt->bind_param("iis", $idArchivoad, $txtRadicado, $name); 
			$stmt->execute(); 
		}
		public function saveResponsables($arrData) {
			$numeror = $arrData['numeror'];
			$listaFuncionarios = json_decode($arrData['listafun'], true);
			$fecha = $arrData['fechar'];
			$uAsigna = $arrData['usuarior'];
			$gestado = $arrData['estado'];
			$horaA = $arrData['horar'];
			foreach ($listaFuncionarios as $index => $funcionario) {
				$estado = ($gestado == "LN") ? "LN" : (($funcionario['funResponsable'] == '1') ? 'AN' : 'LN');
				$numid = selconsecutivo('planacresponsables', 'codigo');
				$documento = isset($funcionario['funDocumento']) ? $funcionario['funDocumento'] : '';
				$codcargo = isset($funcionario['funCodCargo']) ? $funcionario['funCodCargo'] : '';
				$dependencia = isset($funcionario['funDependencia']) ? $funcionario['funDependencia'] : '';
				$responsable = isset($funcionario['funResponsable']) ? $funcionario['funResponsable'] : '';
				$sql = sprintf(
					"INSERT INTO planacresponsables (codigo, codradicacion, fechasig, fechares, usuarioasig, usuariocon, estado, archivos, respuesta, codcargo, tipot, consulta, idhistory, horasig, horresp, proceso, dependencia, estadores) VALUES ('%s', '%s', '%s', '', '%s', '%s', '%s', '', '', '%s', 'RA', '', '0', '%s', '', '', '%s', '%s')",
					mysqli_real_escape_string($this->linkbd, $numid),
					mysqli_real_escape_string($this->linkbd, $numeror),
					mysqli_real_escape_string($this->linkbd, $fecha),
					mysqli_real_escape_string($this->linkbd, $uAsigna),
					mysqli_real_escape_string($this->linkbd, $documento),
					mysqli_real_escape_string($this->linkbd, $estado),
					mysqli_real_escape_string($this->linkbd, $codcargo),
					mysqli_real_escape_string($this->linkbd, $horaA),
					mysqli_real_escape_string($this->linkbd, $dependencia),
					mysqli_real_escape_string($this->linkbd, $responsable)
				);
				mysqli_query($this->linkbd, $sql);
			}
		}
		public function idPlanacradicacion(){
			$sql = "SELECT MAX(id) as max_id FROM planacradicacion"; 
			$request = mysqli_fetch_all(mysqli_query($this->linkbd, $sql), MYSQLI_ASSOC); 
			$maxId = $request[0]['max_id']; 
			return $maxId + 1; 
		}
		public function guardaRadicacion(){
			if(!empty($_SESSION)){
				$vaDivipola = $_POST["aDivipola"];
				$vaRadicado = $_POST["aRadicado"];
				$vaFecha = date('Y-m-d', strtotime(str_replace('/', '-', $_POST["aFecha"])));
				$vaTRcodigo = $_POST["aTRcodigo"];
				$vaTRslectura = $_POST["aTRslectura"];
				$vaTRadjunto = $_POST["aTRadjunto"];
				$vaTRclasificacion = $_POST["aTRclasificacion"];
				$vaFechaLimite = date('Y-m-d', strtotime(str_replace('/', '-', $_POST["aFechaLimite"])));
				$vaDescripcion = $_POST["aDescripcion"];
				$vaTelefonica = $_POST["aTelefonica"];
				$vaEscrita = $_POST["aEscrita"];
				$vaEmail = $_POST["aEmail"];
				$vaTipopqr = $_POST["aTipopqr"];
				$vaFolios = $_POST["aFolios"];
				$vbdocumento = $_POST["bdocumento"];
				$vbnombre = $_POST["bnombre"];
				$vbdireccion = $_POST["bdireccion"];
				$vbemail = $_POST["bemail"];
				$vbtelefono = $_POST["btelefono"];
				$vbcelular = $_POST["bcelular"];
				$vclistaFuncionarios = isset($_POST["clistaFuncionarios"]) ? json_decode($_POST["clistaFuncionarios"], true) : array();
				$vusuario = $_SESSION['cedulausu'];
				$varchivoZip = $vaRadicado.'zip';
				$vid = $this->idPlanacradicacion();
				$vestado = ($vaTRslectura == 'S') ? "AN" : "LN";
				$horaOriginal = $_POST["aHora"];
				$horaFormateada = preg_replace('/[^0-9:APM]/i', '', $horaOriginal);
				$date = DateTime::createFromFormat('h:i:sa', $horaFormateada);
				$vaHora = ($date === false) ? "00:00:00" : $date->format('H:i:s');
				$this->uploadFiles($_POST['txtRadicado'], $_FILES['archivos']);
				$this->saveResponsables(array(
					"listafun" => json_encode($vclistaFuncionarios),
					"numeror"=>$vaRadicado, 
					"fechar"=>$vaFecha, 
					"usuarior"=>$vusuario, 
					"estado"=>$vestado, 
					"horar"=>$vaHora
				));
				$request = $this->insertData(array(
					"divipola"=>$vaDivipola, 
					"numeror"=>$vaRadicado, 
					"fechar"=>$vaFecha, 
					"horar"=>$vaHora, 
					"usuarior"=>$vusuario, 
					"tipor"=>$vaTRcodigo, 
					"fechalimite"=>$vaFechaLimite, 
					"idtercero"=>$vbdocumento, 
					"descripcionr"=>$vaDescripcion, 
					"tescrito"=>$vaEscrita, 
					"ttelefono"=>$vaTelefonica, 
					"temail"=>$vaEmail, 
					"telefonot"=>$vbtelefono, 
					"celulart"=>$vbcelular, 
					"direcciont"=>$vbdireccion, 
					"emailt"=>$vbemail, 
					"documento"=>$varchivoZip, 
					"nfolios"=>$vaFolios, 
					"estado"=>$vestado, 
					"mrecepcion"=>$vaTipopqr, 
					"id"=>$vid, 
					"nombret"=>$vbnombre
				));
				if(is_numeric($request) && $request > 0 ){
					$arrResponse = array("status" => true, "msg" => "Datos guardados.");
				} else {
					$arrResponse = array("status" => false, "msg" => "Ha ocurrido un error, intente de nuevo.");
				}
				echo json_encode($arrResponse, JSON_UNESCAPED_UNICODE);
			}
			die();
		}		
		public function insertData(array $data){
			$this->arrData = $data;
			if($this->arrData['idtercero'] == ''){
				$sql = "INSERT INTO planradicacionsindoc (numeror, nombre, direccion, email, telefono, celular) VALUES (
					'{$this->arrData['numeror']}',
					'{$this->arrData['nombret']}',
					'{$this->arrData['direcciont']}',
					'{$this->arrData['emailt']}',
					'{$this->arrData['telefonot']}',
					'{$this->arrData['celulart']}'
				)";
				mysqli_query($this->linkbd,$sql);
			}
			$sql = "INSERT INTO planacradicacion (numeror, codigobarras, fechar, horar, usuarior, tipor, fechalimite, idtercero, descripcionr, tescrito, ttelefono, temail, telefonot, celulart, direcciont, emailt, documento, numeromod, nfolios, narchivosad, estado, tipot,  estado2, horalimite, mrecepcion, id, divipola) VALUES (
				'{$this->arrData['numeror']}',
				'{$this->arrData['numeror']}',
				'{$this->arrData['fechar']}',
				'{$this->arrData['horar']}',
				'{$this->arrData['usuarior']}',
				'{$this->arrData['tipor']}',
				'{$this->arrData['fechalimite']}',
				'{$this->arrData['idtercero']}',
				'{$this->arrData['descripcionr']}',
				'{$this->arrData['tescrito']}',
				'{$this->arrData['ttelefono']}',
				'{$this->arrData['temail']}',
				'{$this->arrData['telefonot']}',
				'{$this->arrData['celulart']}',
				'{$this->arrData['direcciont']}',
				'{$this->arrData['emailt']}',
				'{$this->arrData['documento']}',
				'0',
				'{$this->arrData['nfolios']}',
				'0',
				'{$this->arrData['estado']}',
				'RA',
				'',
				'0',
				'{$this->arrData['mrecepcion']}',
				'{$this->arrData['id']}',
				'{$this->arrData['divipola']}'
			)";
			$request = intval(mysqli_query($this->linkbd,$sql));
			return $request;
		}
	}
?>