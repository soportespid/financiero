<?php
	require_once '../../../../comun.inc';
	require '../../../../funciones.inc';
	require '../../../../funcionesSP.inc.php';
	session_start();
	if($_POST){
		$base = isset($_POST['base']) ?  base64_decode($_POST['base']) : null;
		$usuario = isset($_POST['usuario']) ? base64_decode($_POST['usuario']) : null;
		$obj = new Plantilla($base, $usuario);
		switch ($_POST['action']){
			case 'get':
				$obj->getData();
			break;
			case 'iniDependencias':
				$obj->iniDependencias();
			break;
			case 'Anular':
				$obj->anulaRadicado();
			break;
		}
	}
	class Plantilla{
		private $linkbd;
		public function __construct($base = null, $usuario = null) {
			if (!empty($base) && !empty($usuario)) {
				$this->linkbd = conectar_Multi($base, $usuario);
			} else {
				$this->linkbd = conectar_v7();
			}
			$this->linkbd->set_charset("utf8");
		}
		public function getData(){
			if(!empty($_SESSION)){
				echo json_encode($this->selectData(),JSON_UNESCAPED_UNICODE);
			}
			die();
		}
		public function selectData() {
			$fechaI = date('Y-m-d', strtotime(str_replace('/', '-', $_POST['FechaI'])));
			$fechaF = date('Y-m-d', strtotime(str_replace('/', '-', $_POST['FechaF'])));
			$sql = "SELECT numeror, fechar, fechalimite, idtercero, descripcionr, estado, estado2 FROM planacradicacion WHERE tipot = 'RA' AND fechar BETWEEN '$fechaI' AND '$fechaF' ORDER BY numeror DESC";
			$request = mysqli_fetch_all(mysqli_query($this->linkbd, $sql), MYSQLI_ASSOC);
			$total = count($request);
			for ($i = 0; $i < $total; $i++) {
				$sql2 = "SELECT fechares, estado, dependencia FROM planacresponsables WHERE estadores = '1' AND codradicacion = '".$request[$i]['numeror']."'";
				$infoResp = mysqli_fetch_all(mysqli_query($this->linkbd,$sql2),MYSQLI_ASSOC);
				if($request[$i]['fechalimite'] != '0000-00-00'){
					$request[$i]['fLimite'] = date('d/m/Y', strtotime($request[$i]['fechalimite']));
					$timestamp_fechalimite = strtotime($request[$i]['fechalimite']);
					if($infoResp[0]['fechares'] != '0000-00-00' && $infoResp[0]['fechares'] != NULL){
						$request[$i]['fRespuesta'] = date('d/m/Y', strtotime($infoResp[0]['fechares']));
						$request[$i]['fechares'] = $infoResp[0]['fechares'];
						$timestamp_fechares = strtotime($infoResp[0]['fechares']);
						if ($timestamp_fechares > $timestamp_fechalimite) {
							$request[$i]['estadoTiempo'] = '2';
						} else {
							$request[$i]['estadoTiempo'] = '1';
						}
					} else {
						$timestamp_fechaActual = time();
						if ($timestamp_fechaActual > $timestamp_fechalimite) {
							$request[$i]['estadoTiempo'] = '2';
						} else {
							$request[$i]['estadoTiempo'] = '1';
						}
						$request[$i]['fRespuesta'] = "00/00/0000";
						$request[$i]['fechares'] = "0000-00-00";
					}
				} else {
					$request[$i]['fRespuesta'] = "00/00/0000";
					$request[$i]['fechares'] = "0000-00-00";
					$request[$i]['estadoTiempo'] = '0';
					$request[$i]['fLimite'] = 'Sin Limite';
				}
				if($request[$i]['idtercero'] != ''){
					$sqlt = "SELECT CONCAT(razonsocial,' ',nombre1,' ',nombre2,' ',apellido1,' ',apellido2) AS nombre FROM terceros WHERE cedulanit = '".$request[$i]['idtercero']."'";
					$infoTerc = mysqli_fetch_all(mysqli_query($this->linkbd,$sqlt),MYSQLI_ASSOC);
					$request[$i]['nomtercero'] = $infoTerc[0]['nombre'];
				} else {
					$sqlt = "SELECT nombre FROM planradicacionsindoc WHERE numeror = '".$request[$i]['numeror']."'";
					$infoTerc = mysqli_fetch_all(mysqli_query($this->linkbd,$sqlt),MYSQLI_ASSOC);
					$request[$i]['nomtercero'] = $infoTerc[0]['nombre'];
				}
				$request[$i]['dependencia'] = $infoResp[0]['dependencia'];
				$request[$i]['fRadicado'] = date('d/m/Y', strtotime($request[$i]['fechar']));
				$request[$i]['is_yellow'] = $request[$i]['numeror'] == $_POST['id_gia'] ? 1 : 0;
			
			}
			return $request;
		}
		public function iniDependencias(){
			if(!empty($_SESSION)){
				$arrData = array(
					"dependencias" =>$this->selectDependencias()
				);
				echo json_encode($arrData,JSON_UNESCAPED_UNICODE);
			}
			die();
		}
		public function selectDependencias(){
			$sql = "SELECT codarea AS codigo, nombrearea AS nombre FROM planacareas WHERE estado = 'S' ORDER BY nombrearea ASC";
			$request = mysqli_fetch_all(mysqli_query($this->linkbd,$sql),MYSQLI_ASSOC);
			return $request;
		}
		public function anulaRadicado(){
			if(!empty($_SESSION)){
				$request = $this->insertData();
				if(is_numeric($request) && $request > 0 ){
					$radicado = $_POST['id_radicacion'];
					$arrResponse = array("status" => true, "msg" => "Radicacion N° $radicado Anulada.");
				} else {
					$arrResponse = array("status" => false, "msg" => "Ha ocurrido un error, intente de nuevo.");
				}
				echo json_encode($arrResponse, JSON_UNESCAPED_UNICODE);
			}
			die();
		}
		public function insertData(){
			$radicado = $_POST['id_radicacion'];
			$sql = "UPDATE planacradicacion SET estado2 = '3' WHERE numeror = '$radicado'";
			$request = intval(mysqli_query($this->linkbd,$sql));
			return $request;
		}
	}
?>