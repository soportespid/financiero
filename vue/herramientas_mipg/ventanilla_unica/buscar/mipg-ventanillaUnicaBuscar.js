const URL ='vue/herramientas_mipg/ventanilla_unica/buscar/mipg-ventanillaUnicaBuscar.php';
var app = new Vue({
	el: '#myapp',
	data() {
		return {
			isLoading: false,
			isModal:false,
			txtSearch: '',
			txtResults: 0,
			arrData:[],
			arrDataCopy:[],
			objDependencia:{codigo:"", nombre:""},
			arrDependencias: [],
			arrDependenciasCopy: [],
			txtResultados: 0,
			txtBuscar: '',
			selectEstado: '0',
		};
	},
	mounted(){
		this.iniFechas();
		this.iniDependencias();
	},
	methods: {
		iniFechas: function(){
			const fecha1 = new Date();
			fecha1.setDate(1);
			const primerDiaMes = fecha1.toLocaleDateString('es-ES', { day: '2-digit', month: '2-digit', year: 'numeric' });
			const fecha2 = new Date(fecha1);
			fecha2.setMonth(fecha2.getMonth() + 1);
			fecha2.setDate(0);
			const ultimoDiaMes = fecha2.toLocaleDateString('es-ES', { day: '2-digit', month: '2-digit', year: 'numeric' });
			document.getElementById('fc_1198971545').value = primerDiaMes;
			document.getElementById('fc_1198971546').value = ultimoDiaMes;
		},
		getData: async function(){
			const urlParams = new URLSearchParams(window.location.search);
			const id = urlParams.get('id');
			const formData = new FormData();
			let fecha1 = document.getElementById('fc_1198971545').value;
			let fecha2 = document.getElementById('fc_1198971546').value;
			formData.append("action","get");
			formData.append("FechaI",fecha1);
			formData.append("FechaF",fecha2);
			formData.append("id_gia",id);
			this.isLoading = true;
			const response = await fetch(URL,{method:"POST",body:formData});
			const objData = await response.json();
			this.arrData = objData;
			this.arrDataCopy = objData;
			this.txtResults = this.arrData.length;
			this.isLoading = false;

			this.$nextTick(() => {
				const highlightedRow = this.$refs.highlightedRow;
				if (highlightedRow && highlightedRow.length > 0) {
					const row = highlightedRow.find(row => row.classList.contains('bg-warning'));
					if (row) {
						row.scrollIntoView({ behavior: 'auto', block: 'center' });
					}
				}
			});
		},
		searchx: function() {
			let bestado = this.selectEstado.toLowerCase();
			let searchv = this.objDependencia.codigo.toLowerCase();
			let search = this.txtSearch.toLowerCase();
			let sEstados = "";
			let searcht = "";
			switch (bestado){
				case '1':
					sEstados = 'estado2';
					searcht = "0";break;
				case '2':
					sEstados = 'estado2';
					searcht = "2";break;
				case '3':
					sEstados = 'estado2';
					searcht = "3";break;
				case '4':
					sEstados = 'estadoTiempo';
					searcht = "2";break;
				case '5':
					sEstados = 'estadoTiempo';
					searcht = "0";break;
			}

			if (searchv !== '') {
				if (bestado != '0') {
					this.arrDataCopy = [...this.arrData.filter(e => e.dependencia.toLowerCase().includes(searchv) && (e[sEstados].toLowerCase().includes(searcht)) && (e.descripcionr.toLowerCase().includes(search) || e.numeror.toLowerCase().includes(search)))];
				}else{
					this.arrDataCopy = [...this.arrData.filter(e => e.dependencia.toLowerCase().includes(searchv) && (e.descripcionr.toLowerCase().includes(search) || e.numeror.toLowerCase().includes(search)))];
				}
			} else {
				if (bestado != '0') {
					this.arrDataCopy = [...this.arrData.filter(e => e[sEstados].toLowerCase().includes(searcht) && (e.descripcionr.toLowerCase().includes(search) || e.numeror.toLowerCase().includes(search)))];
				} else {
					this.arrDataCopy = [...this.arrData.filter(e => e.descripcionr.toLowerCase().includes(search) || e.numeror.toLowerCase().includes(search))];
				}
				
			}
			this.txtResults = this.arrDataCopy.length;
		},
		clearDependencia: function() { 
			this.objDependencia.nombre = ''; 
			this.objDependencia.codigo = '';
			this.searchx(); 
		},
		selectItem: function({ ...item }) { 
			this.objDependencia.codigo = item.codigo;
			this.objDependencia.nombre = item.nombre;
			this.searchx();
			this.isModal = false;
		},
		iniDependencias: async function() {
			const formData = new FormData();
			formData.append("action","iniDependencias");
			this.isLoading = true;
			const response = await fetch(URL,{method:"POST",body:formData});
			const objData = await response.json();
			this.arrDependencias = objData.dependencias;
			this.arrDependenciasCopy = objData.dependencias;
			this.txtResultados = this.arrDependencias.length;
			if(objData.dependencia != null){
				this.objDependencia = objData.dependencia;
			}
			this.isLoading = false;
		},
		search: function(type = "", objeto = "objDependencia") {
			let obj = null; 
			if (type === "documento") {
				obj = this.arrDependencias.find(e => e.codigo == this[objeto].codigo); 
				if (obj) {
					this[objeto].codigo = obj.codigo;
					this[objeto].nombre = obj.nombre; 
				} else { 
					this[objeto].codigo = "";
					this[objeto].nombre = "";
					Swal.fire( 
						'No encontrado', 
						'No se encuentra almacenada ese dependencia.', 
						'warning' 
					); 
				} 
			} else { 
				let search = this.txtBuscar.toLowerCase(); 
				this.arrDependenciasCopy = this.arrDependencias.filter(e => e.nombre.toLowerCase().includes(search) || e.codigo.toLowerCase().includes(search) ); 
				this.txtResultados = this.arrDependenciasCopy.length; 
			} 
		},
		cargarInfo: function(){
			this.getData();
		},
		editItem: function(id){
			window.location.href='mipg-ventanillaUnicaVisualizar.php?id='+id;
		},
		anularRadicado: async function(id){
			Swal.fire({
				title: "¿Estás segur@ de Anular?",
				icon: 'warning',
				showCancelButton: true,
				confirmButtonColor: '#3085d6',
				cancelButtonColor: '#d33',
				confirmButtonText: "Sí, guardar",
				cancelButtonText: "No, cancelar"
			}).then(async function(result) {
				if (result.isConfirmed) {
					const formData = new FormData();
					formData.append("action","Anular");
					formData.append("id_radicacion",id);
					app.isLoading = true;
					const response = await fetch(URL,{method:"POST",body:formData});
					if (!response.ok) {
						throw new Error(`HTTP error! status: ${response.status}`);
					}
					const objData = await response.json();
					if (objData.status) {
						app.arrDataCopy = app.arrDataCopy.map(e => { 
							if (e.numeror === id) { 
								return { ...e, estado2: '3' }; 
							} return e; 
						});
						app.arrData = app.arrData.map(e => { 
							if (e.numeror === id) { 
								return { ...e, estado2: '3' }; 
							} return e; 
						});
						app.isLoading = false;
						Swal.fire({
							title: "Anulado",
							text: objData.msg,
							icon: "success",
							confirmButtonText: "OK"
						});
					} else {
						Swal.fire("Error", objData.msg, "error");
						console.error("Errores: ", objData.errors);
					}
				}
			});




			
		}
	}
});