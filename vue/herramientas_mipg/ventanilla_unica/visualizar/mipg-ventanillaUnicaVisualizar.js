const URL ='vue/herramientas_mipg/ventanilla_unica/Visualizar/mipg-ventanillaUnicaVisualizar.php';
var app = new Vue({
	el: '#myapp',
	data() {
		return {
			isLoading:false,
			txtDivipola: '',
			txtRadicado: '',
			txtFecha: '',
			txtHora: '',
			selectTipoRadica: '0',
			arrTipoRadica: [],
			txtFechaLimite: '00/00/0000',
			txtDescripcion: '',
			checkEscrita: 0,
			checkTelefonica: 0,
			checkEmail: 0,
			txtFolios: '',
			selectTipopqr: 'O',
			objTercero:{nombre:"", documento:"", direccion:"", email:"", telefono:"", celular:""},
			arrTerceros: [],
			arrTercerosCopy: [],
			txtResultados: 0,
			objFuncionario:{nombre:"", documento:"", nombrecargo:"", codcargo:"", dependencia:"", nombrearea:"", usuarioest:""},
			arrFuncionarios: [],
			arrFuncionariosCopy: [],
			txtResultados2: 0,
			listaFuncionarios: [],
			listaArchivos: [],
			numerorAnterior: '',
			numerorSiguiente: ''
		};
	},
	mounted: async function(){
		
		await this.infoInicial();
		this.iniTipoRadica();
		this.obtenerMinMax();
	},
	methods: {
		infoInicial: async function(){
			let codigo = new URLSearchParams(window.location.search).get('id');
			const formData = new FormData();
			formData.append("action","infoInicial");
			formData.append("codigo",codigo);
			this.isLoading = true;
			const response = await fetch(URL,{method:"POST",body:formData});
			const objData = await response.json();
			if(objData.status){
				const data = objData.data.data;
				const planacradicacion = objData.data.consecutivos.planacradicacion[0];
				const planradicacionsindoc = objData.data.consecutivos.planradicacionsindoc[0];
				const planacresponsables = objData.data.consecutivos.planacresponsables;
				const planacarchivosad = objData.data.consecutivos.planacarchivosad; 
				//carga informacion pestaña 1
				this.txtRadicado = planacradicacion.numeror;
				this.txtDivipola = planacradicacion.divipola;
				this.txtFecha = this.formatDate(planacradicacion.fechar);
				this.txtHora = this.convertTime24to12(planacradicacion.horar);
				await this.iniTipoRadica();
				this.selectTipoRadica = this.arrTipoRadica.find(item => item.codigo === planacradicacion.tipor);
				if(planacradicacion.fechalimite != '0000-00-00'){
					this.txtFechaLimite = this.formatDate(planacradicacion.fechalimite);
				}
				this.txtDescripcion = planacradicacion.descripcionr;
				this.checkTelefonica = planacradicacion.ttelefono === '1';
				this.checkEscrita = planacradicacion.tescrito === '1';
				this.checkEmail = planacradicacion.temail === '1';
				this.selectTipopqr = planacradicacion.mrecepcion;
				this.txtFolios = planacradicacion.nfolios;
				//carga informacion pestaña 2
				if(planacradicacion.idtercero != "" && planacradicacion.idtercero != null){
					this.objTercero.documento = planacradicacion.idtercero;
					this.objTercero.nombre = planacradicacion.nombreTerceros;
					this.objTercero.direccion = planacradicacion.direcciont;
					this.objTercero.email = planacradicacion.emailt;
					this.objTercero.telefono = planacradicacion.telefonot;
					this.objTercero.celular = planacradicacion.celulart;
				} else {
					this.objTercero.documento = '';
					this.objTercero.nombre = planradicacionsindoc.nombre;
					this.objTercero.direccion = planradicacionsindoc.direccion;
					this.objTercero.email = planradicacionsindoc.email;
					this.objTercero.telefono = planradicacionsindoc.telefono;
					this.objTercero.celular = planradicacionsindoc.celular;
				}
				//carga informacion pestaña 3
				const numeroResponsables = Array.isArray(planacresponsables) ? planacresponsables.length : 0; 
				for (let i = 0; i < numeroResponsables; i++) {
					let fechaRes = planacresponsables[i].fechares === '0000-00-00' ? '00-00-0000' : this.formatDate(planacresponsables[i].fechares);
					let horaRes = planacresponsables[i].horresp === '00:00:00' ? '00:00:00' : this.formatDate(planacresponsables[i].horresp);
					let colorEstado = '0'; // Valor por defecto
					if (planacresponsables[i].estado === 'LS' || planacresponsables[i].estado === 'AC') {
						colorEstado = '1';
					} else if (planacresponsables[i].estado === 'AR') {
						colorEstado = '2';
					}
					let datosFuncionario = {
						funNombre: planacresponsables[i].nombreFuncionario,
						funDocumento: planacresponsables[i].usuariocon,
						funNombreCargo: planacresponsables[i].nombreCargo,
						funCodCargo: planacresponsables[i].codcargo,
						funDependencia: planacresponsables[i].dependencia,
						funNombreArea: planacresponsables[i].nombreDependencia,
						funEstado: planacresponsables[i].usuarioest,
						funResponsable: Number(planacresponsables[i].estadores),
						funFechasig: this.formatDate(planacresponsables[i].fechasig),
						funHorasig: this.convertTime24to12(planacresponsables[i].horasig),
						funFechares: fechaRes,
						funHorares: horaRes,
						funAsignado: planacresponsables[i].nombreAsigna,
						funEstadopro: planacresponsables[i].estado,
						funColorestado: colorEstado,
						funRespuesta: planacresponsables[i].respuesta
					}
					this.listaFuncionarios.push(datosFuncionario); 
				}
				//carga informacion pestaña 4
				const numeroArchivos = Array.isArray(planacarchivosad) ? planacarchivosad.length : 0;
				for (let i = 0; i < numeroArchivos; i++) { 
					let archivo = { 
						name: planacarchivosad[i].nomarchivo, 
						size: planacarchivosad[i].tamano || 0, 
						path: `informacion/documentosradicados/RA/${planacarchivosad[i].idradicacion}/${planacarchivosad[i].nomarchivo}` 
					}; 
					this.listaArchivos.push(archivo);
				}
			}else{
				codigo = objData.consecutivo;
				//window.location.href="mipg-ventanillaUnicaEditar?id="+codigo
			}
			this.isLoading = false;
		},
		obtenerMinMax: async function(){ 
			let id = new URLSearchParams(window.location.search).get('id');
			const formData = new FormData(); 
			formData.append("action", "obtenerMinMax"); 
			formData.append("id", id);
			try { 
				const response = await fetch(URL, { method: "POST", body: formData }); 
				const objData = await response.json(); 
				if (objData.error) { 
					console.error('Error:', objData.error); 
				} else { 
					this.numerorAnterior = objData.anterior; 
					this.numerorSiguiente = objData.siguiente;
				} 
			} catch (error) { 
				console.error('Hubo un error al hacer la solicitud:', error); 
			} 
		},
		formatDate: function (fecha) { 
			let date = new Date(fecha); 
			let day = ("0" + date.getDate()).slice(-2); 
			let month = ("0" + (date.getMonth() + 1)).slice(-2); 
			let year = date.getFullYear(); 
			return `${day}/${month}/${year}`;
		},
		convertTime24to12: function (time24) { 
			const [hours, minutes, seconds] = time24.split(':'); 
			let period = 'a.m.';
			let hours12 = parseInt(hours, 10); 
			if (hours12 >= 12) { 
				period = 'p.m.'; 
				if (hours12 > 12) { 
					hours12 -= 12; 
				} 
			} else if (hours12 === 0) { 
				hours12 = 12; 
			} 
			const formattedHours = ("0" + hours12).slice(-2); 
			const formattedMinutes = ("0" + minutes).slice(-2); 
			const formattedSeconds = ("0" + seconds).slice(-2); 
			return `${formattedHours}:${formattedMinutes}:${formattedSeconds} ${period}`;
		},
		iniTipoRadica: async function(){
			const formData = new FormData();
			formData.append("action","iniTipoRadica");
			this.isLoading = true;
			const response = await fetch(URL,{method:"POST",body:formData});
			const objData = await response.json();
			this.arrTipoRadica = objData;
			this.isLoading = false;
		},
		iniTercero: async function() {
			const formData = new FormData();
			formData.append("action","iniTerceros");
			this.isLoading = true;
			const response = await fetch(URL,{method:"POST",body:formData});
			const objData = await response.json(); 
			this.arrTerceros = objData.terceros;
			this.arrTercerosCopy = objData.terceros;
			this.txtResultados = this.arrTerceros.length;
			if(objData.data.tercero != null){
				this.objTercero = objData.data.tercero;
			}
		},
		iniFuncionario: async function() {
			const formData = new FormData();
			formData.append("action","iniFuncionarios");
			this.isLoading = true;
			const response = await fetch(URL,{method:"POST",body:formData});
			const objData = await response.json();
			this.arrFuncionarios = objData.funcionarios;
			this.arrFuncionariosCopy = objData.funcionarios;
			this.txtResultados2 = this.arrFuncionarios.length;
			if(objData.data.funcionario != null){
				this.objFunsionario = objData.data.funcionario;
			}
			this.filterFuncionarios();
		},
		showTab:function(tab){
			let tabs = this.$refs.rTabs.children;
			let tabsContent = this.$refs.rTabsContent.children;
			for (let i = 0; i < tabs.length; i++) {
				tabs[i].classList.remove("active");
				tabsContent[i].classList.remove("active")
			}
			tabs[tab-1].classList.add("active");
			tabsContent[tab-1].classList.add("active")
		},
		descargarArchivo: function(archivo) { 
			const link = document.createElement('a'); 
			link.href = archivo.path;; 
			link.download = archivo.name;
			document.body.appendChild(link); 
			link.click();
			document.body.removeChild(link); 
		},
		editItem: function(idTab){ 
			if(idTab == 'prev'){
				if(this.numerorAnterior !== '' && this.numerorAnterior !== null){
					let id = this.numerorAnterior;
					window.location.href='mipg-ventanillaUnicaVisualizar.php?id='+id;
				}
			} else { 
				if(this.numerorSiguiente !== '' && this.numerorSiguiente !== null){
					let id = this.numerorSiguiente;
					window.location.href='mipg-ventanillaUnicaVisualizar.php?id='+id;
				}
			}
		}
	}
});