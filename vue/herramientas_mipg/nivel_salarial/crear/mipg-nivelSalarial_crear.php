<?php
	require_once '../../../../comun.inc';
	require '../../../../funciones.inc';
	require '../../../../funcionesSP.inc.php';
	/*ini_set('display_errors', '1');
	ini_set('display_startup_errors', '1');
	error_reporting(E_ALL);*/
	session_start();
	//dep($_POST);exit;
	if($_POST){
		$obj = new Plantilla();
		if($_POST['action']=="get"){
			$obj->getData();
		}else if($_POST['action']=="save"){
			$obj->save();
		}
	}

	class Plantilla{
		private $linkbd;
		private $arrData;
		public function __construct() {
			$this->linkbd = conectar_v7();
			$this->linkbd->set_charset("utf8");
		}
		public function save(){
			if(!empty($_SESSION)){
				if($_POST){
					if(empty($_POST['codigo']) || empty($_POST['valor']) || empty($_POST['nombre'])){
						$arrResponse = array("status"=>false,"msg"=>"Error de datos");
					}else{
						$intCodigo = intval($_POST['codigo']);
						$dblValor = doubleval($_POST['valor']);
						$strNombre = ucwords(replaceChar(strClean($_POST['nombre'])));
						$request = $this->insertData(array("codigo"=>$intCodigo,"valor"=>$dblValor,"nombre"=>$strNombre));
						if(is_numeric($request) && $request> 0 ){
							$arrResponse = array("status"=>true,"msg"=>"Datos guardados.");
						}else if($request =="existe"){
							$arrResponse = array("status"=>false,"msg"=>"El nombre del ya existe, pruebe con otro.");
						}else{
							$arrResponse = array("status"=>false,"msg"=>"Ha ocurrido un error, intente de nuevo.");
						}
					}
				}
				echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
			}
			die();
		}
		public function getData(){
			if(!empty($_SESSION)){
				echo json_encode($this->selectConsecutivo(),JSON_UNESCAPED_UNICODE);
			}
			die();
		}
		public function insertData(array $data){
			$this->arrData = $data;
			$request = mysqli_fetch_all(mysqli_query($this->linkbd,"SELECT * FROM humnivelsalarial WHERE nombre = '{$this->arrData['nombre']}'"),MYSQLI_ASSOC);
			if(empty($request)){
				$sql = "INSERT INTO humnivelsalarial(id_nivel,nombre,valor,estado) VALUES(
					{$this->arrData['codigo']},
					'{$this->arrData['nombre']}',
					{$this->arrData['valor']},
					'S'
				)";
				$request = intval(mysqli_query($this->linkbd,$sql));
			}else{
				$request="existe";
			}
			return $request;
		}
		public function selectConsecutivo(){
			$sql = "SELECT MAX(id_nivel) as max FROM humnivelsalarial";
			$request = mysqli_query($this->linkbd,$sql)->fetch_assoc()['max']+1;
			return $request;
		}

	}
?>
