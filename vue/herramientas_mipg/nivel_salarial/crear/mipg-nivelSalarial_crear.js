const URL ='vue/herramientas_mipg/nivel_salarial/crear/mipg-nivelSalarial_crear.php';

var app = new Vue({
	el:"#myapp",
	data() {
		return {
			isLoading:false,
			txtValor:0,
			txtNombre:"",
			txtConsecutivo:0
		}
	},
	mounted() {
		this.getData();
	},
	methods: {
		getData: async function(){
			const formData = new FormData();
			formData.append("action","get");
			this.isLoading = true;
			const response = await fetch(URL,{method:"POST",body:formData});
			const objData = await response.json();
			this.txtConsecutivo = objData;
			this.isLoading = false;
		},
		save:function(){
			const vueContext = this;
			if(this.txtValor == "" || this.txtValor <= 0|| this.txtNombre =="" || this.txtCodigo ==""){
				Swal.fire("Error","Todos los campos con (*) son obligatorios","error");
				return false;
			}
			Swal.fire({
				title:"¿Estás segur@ de guardar?",
				text:"",
				icon: 'warning',
				showCancelButton:true,
				confirmButtonColor: '#3085d6',
				cancelButtonColor: '#d33',
				confirmButtonText:"Sí, guardar",
				cancelButtonText:"No, cancelar"
			}).then(async function(result){
				if(result.isConfirmed){
					const formData = new FormData();
					formData.append("action","save");
					formData.append("valor",vueContext.txtValor);
					formData.append("nombre",vueContext.txtNombre);
					formData.append("codigo",vueContext.txtCodigo);
					vueContext.isLoading = true;
					const response = await fetch(URL,{method:"POST",body:formData});
					const objData = await response.json();
					if(objData.status){
						vueContext.txtValor = "";
						vueContext.txtCodigo ="";
						vueContext.txtNombre ="";
						vueContext.getData();
						Swal.fire("Guardado",objData.msg,"success");
					}else{
						Swal.fire("Error",objData.msg,"error");
					}
					vueContext.isLoading = false;
				}
			});
		}
	},
	computed:{

	}
})
