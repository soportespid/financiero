const URL ='vue/herramientas_mipg/nivel_salarial/editar/mipg-nivelSalarial_editar.php';

var app = new Vue({
	el:"#myapp",
	data() {
		return {
			isLoading:false,
			txtValor:0,
			txtNombre:"",
			txtConsecutivo:0,
			arrConsecutivos:[]
		}
	},
	mounted() {
		this.getData();
	},
	methods: {
		getData: async function(){
			let codigo = new URLSearchParams(window.location.search).get('id');
			const formData = new FormData();
			formData.append("action","get");
			formData.append("codigo",codigo);
			this.isLoading = true;
			const response = await fetch(URL,{method:"POST",body:formData});
			const objData = await response.json();
			if(objData.status){
				const data = objData.data.data;
				this.txtConsecutivo = data.id_nivel;
				this.txtNombre = data.nombre;
				this.txtValor = data.valor;
				this.arrConsecutivos = objData.data.consecutivos;
			}else{
				codigo = objData.consecutivo;
				window.location.href="mipg-nivelSalarial_editar?id="+codigo
			}
			this.isLoading = false;
		},
		save:function(){
			const vueContext = this;
			if(this.txtValor == "" || this.txtValor <= 0|| this.txtNombre =="" || this.txtCodigo ==""){
				Swal.fire("Error","Todos los campos con (*) son obligatorios","error");
				return false;
			}
			Swal.fire({
				title:"¿Estás segur@ de guardar?",
				text:"",
				icon: 'warning',
				showCancelButton:true,
				confirmButtonColor: '#3085d6',
				cancelButtonColor: '#d33',
				confirmButtonText:"Sí, guardar",
				cancelButtonText:"No, cancelar"
			}).then(async function(result){
				if(result.isConfirmed){
					const formData = new FormData();
					formData.append("action","save");
					formData.append("valor",vueContext.txtValor);
					formData.append("nombre",vueContext.txtNombre);
					formData.append("codigo",vueContext.txtConsecutivo);
					vueContext.isLoading = true;
					const response = await fetch(URL,{method:"POST",body:formData});
					const objData = await response.json();
					if(objData.status){
						Swal.fire("Guardado",objData.msg,"success");
					}else{
						Swal.fire("Error",objData.msg,"error");
					}
					vueContext.isLoading = false;
				}
			});
		},
		editItem:function(type){
			let vueContext = this;
			let id = this.txtConsecutivo
			let index = this.arrConsecutivos.findIndex(function(e){return e.id_nivel == id});
			if(type=="next" && vueContext.arrConsecutivos[++index]){
				id = this.arrConsecutivos[index++].id_nivel;
			}else if(type=="prev" && vueContext.arrConsecutivos[--index]){
				id = this.arrConsecutivos[index--].id_nivel;
			}
			window.location.href="mipg-nivelSalarial_editar?id="+id;
		},
		iratras:function(type){
			let id = this.txtConsecutivo;
			window.location.href="mipg-nivelSalarial_buscar.php?id="+id;
		},
	},
	computed:{

	}
})
