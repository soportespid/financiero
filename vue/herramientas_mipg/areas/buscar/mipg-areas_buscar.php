<?php
	require_once '../../../../comun.inc';
	require '../../../../funciones.inc';
	require '../../../../funcionesSP.inc.php';
	session_start();
	if($_POST){
		$obj = new Plantilla();
		if($_POST['action']=="get"){
			$obj->getData();
		}else if($_POST['action']=="change"){
			$obj->changeData();
		}
	}

	class Plantilla{
		private $linkbd;
		private $arrData;
		public function __construct() {
			$this->linkbd = conectar_v7();
			$this->linkbd->set_charset("utf8");
		}

		public function getData(){
			if(!empty($_SESSION)){
				echo json_encode($this->selectData(),JSON_UNESCAPED_UNICODE);
			}
			die();
		}
		public function changeData(){
			if(!empty($_SESSION)){
				$id = intval($_POST['id']);
				$estado = $_POST['estado'];
				$sql = "UPDATE admareas SET estado = '$estado' WHERE id_cc = $id";
				$request = mysqli_query($this->linkbd,$sql);
				if($request == 1){
					$arrResponse = array("status"=>true);
				}else{
					$arrResponse = array("status"=>false,"msg"=>"Error");
				}
				echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
			}
			die();
		}
		public function selectData(){
			$sql = "SELECT id_cc, nombre, estado FROM admareas ORDER BY id_cc ASC";
			$request = mysqli_fetch_all(mysqli_query($this->linkbd,$sql),MYSQLI_ASSOC);
			$total = count($request);
			for ($i=0; $i < $total; $i++) {
				$request[$i]['is_status'] = $request[$i]['estado'] == "S"? 1 : 0;
				$request[$i]['is_yellow'] = $request[$i]['id_cc'] == $_POST['id_gia']? 1 : 0;
			}
			return $request;
		}
	}
?>
