<?php
	require_once '../../../../comun.inc';
	require '../../../../funciones.inc';
	require '../../../../funcionesSP.inc.php';
	session_start();
	if($_POST){
		$base = isset($_POST['base']) ?  base64_decode($_POST['base']) : null;
		$usuario = isset($_POST['usuario']) ? base64_decode($_POST['usuario']) : null;
		$obj = new Plantilla($base, $usuario);
		switch ($_POST['action']){
			case 'get':
				$obj->getData();
			break;
			case 'iniCargo':
				$obj->iniCargo();
			case 'Anular':
				$obj->anulaRadicado();
			break;
		}
	}
	class Plantilla{
		private $linkbd;
		public function __construct($base = null, $usuario = null) {
			if (!empty($base) && !empty($usuario)) {
				$this->linkbd = conectar_Multi($base, $usuario);
			} else {
				$this->linkbd = conectar_v7();
			}
			$this->linkbd->set_charset("utf8");
		}
		public function getData(){
			if(!empty($_SESSION)){
				echo json_encode($this->selectData(),JSON_UNESCAPED_UNICODE);
			}
			die();
		}
		public function selectData() {
			$fechaI = date('Y-m-d', strtotime(str_replace('/', '-', $_POST['FechaI'])));
			$fechaF = date('Y-m-d', strtotime(str_replace('/', '-', $_POST['FechaF'])));
			$cargoUsuario = $_POST['Cargo'];
			$sql = "SELECT TB1.*, TB2.numeror, TB2.fechalimite, TB2.descripcionr, TB2.codigobarras, TB2.estado2, TB2.usuarior FROM planacresponsables TB1 JOIN planacradicacion TB2 ON TB1.codradicacion = TB2.numeror AND TB1.tipot = TB2.tipot WHERE TB1.codcargo = '$cargoUsuario' AND TB2.fechar BETWEEN '$fechaI' AND '$fechaF' ORDER BY TB1.codradicacion DESC;";
			$request = mysqli_fetch_all(mysqli_query($this->linkbd, $sql), MYSQLI_ASSOC);
			$total = count($request);
			for ($i = 0; $i < $total; $i++) {

				$request[$i]['fradicado'] = date('d/m/Y', strtotime($request[$i]['fechasig']));

				if($request[$i]['fechares'] != '0000-00-00' && $request[$i]['fechares'] != NULL){
					$request[$i]['frespuesta'] = date('d/m/Y', strtotime($request[$i]['fechares']));
				} else {
					$request[$i]['frespuesta'] = "00/00/0000";
				}

				if($request[$i]['fechalimite'] != '0000-00-00' && $request[$i]['fechalimite'] != NULL){
					$request[$i]['fLimite'] = date('d/m/Y', strtotime($request[$i]['fechalimite']));
				} else {
					$request[$i]['fLimite'] = "00/00/0000";
				}

				if($request[$i]['usuarior'] != ''){
					$sqlt = "SELECT CONCAT(razonsocial,' ',nombre1,' ',nombre2,' ',apellido1,' ',apellido2) AS nombre FROM terceros WHERE cedulanit = '".$request[$i]['usuarior']."'";
					$infoTerc = mysqli_fetch_all(mysqli_query($this->linkbd,$sqlt),MYSQLI_ASSOC);
					$request[$i]['nomtercero'] = $infoTerc[0]['nombre'];
				} else {
					$request[$i]['nomtercero'] = '';
				}
				
				$request[$i]['fRadicado'] = date('d/m/Y', strtotime($request[$i]['fechar']));

				$request[$i]['colorEstado'] = '0';
				if ($request[$i]['estado'] === 'LS' || $request[$i]['estado'] === 'AC') {
					$request[$i]['colorEstado'] = '1';
				} else if ($request[$i]['estado'] === 'AR') {
					$request[$i]['colorEstado'] = '2';
				}
				$request[$i]['is_yellow'] = $request[$i]['numeror'] == $_POST['id_gia'] ? 1 : 0;
			
			}
			return $request;
		}
		public function iniCargo() {
			if(!empty($_SESSION['cedulausu'])) {
				$usuario = $_SESSION['cedulausu'];
		
				// Preparar la consulta
				$stmt = $this->linkbd->prepare("SELECT codcargo FROM planestructura_terceros WHERE cedulanit = ? AND estado = 'S'");
				if ($stmt === false) {
					echo json_encode(['error' => 'Error al preparar la consulta'], JSON_UNESCAPED_UNICODE);
					die();
				}
				$stmt->bind_param('s', $usuario);
				$stmt->execute();
				$result = $stmt->get_result();
		
				if ($result->num_rows > 0) {
					$cargo = $result->fetch_assoc();
					echo json_encode(['cargo' => $cargo['codcargo']], JSON_UNESCAPED_UNICODE);  // Devolver solo el valor
				} else {
					$mensaje = 'No se encontró el cargo para el usuario ' + $usuario;
					echo json_encode(['error' => $mensaje], JSON_UNESCAPED_UNICODE);
				}
				$stmt->close();
			} else {
				echo json_encode(['error' => 'No hay cédula en la sesión'], JSON_UNESCAPED_UNICODE);
			}
			die();
		}

		public function anulaRadicado(){
			if(!empty($_SESSION)){
				$request = $this->insertData();
				if(is_numeric($request) && $request > 0 ){
					$radicado = $_POST['id_radicacion'];
					$arrResponse = array("status" => true, "msg" => "Radicacion N° $radicado Anulada.");
				} else {
					$arrResponse = array("status" => false, "msg" => "Ha ocurrido un error, intente de nuevo.");
				}
				echo json_encode($arrResponse, JSON_UNESCAPED_UNICODE);
			}
			die();
		}
		public function insertData(){
			$radicado = $_POST['id_radicacion'];
			$sql = "UPDATE planacradicacion SET estado2 = '3' WHERE numeror = '$radicado'";
			$request = intval(mysqli_query($this->linkbd,$sql));
			return $request;
		}
	}
?>