const URL ='vue/herramientas_mipg/tareas_ideal/buscar/mipg-tereasIdealBuscar.php';
var app = new Vue({
	el: '#myapp',
	data() {
		return {
			isLoading: false,
			selectEstado: '0',
			selectTipo: '0',
			txtSearch: '',
			arrData:[],
			arrDataCopy:[],
			txtResults: 0,
			cargoUsuario: ''
		};
	},
	mounted(){
		this.iniFechas();
		this.iniCargo();
	},
	methods: {
		iniFechas: function(){
			const fecha1 = new Date();
			fecha1.setDate(1);
			const primerDiaMes = fecha1.toLocaleDateString('es-ES', { day: '2-digit', month: '2-digit', year: 'numeric' });
			const fecha2 = new Date(fecha1);
			fecha2.setMonth(fecha2.getMonth() + 1);
			fecha2.setDate(0);
			const ultimoDiaMes = fecha2.toLocaleDateString('es-ES', { day: '2-digit', month: '2-digit', year: 'numeric' });
			document.getElementById('fc_1198971545').value = primerDiaMes;
			document.getElementById('fc_1198971546').value = ultimoDiaMes;
		},
		iniCargo: async function(){
			const formData = new FormData();
			formData.append("action","iniCargo");
			this.isLoading = true;
			const response = await fetch(URL,{method:"POST",body:formData});
			const objData = await response.json();
			if (objData.error) { 
				console.error('Error:', objData.error); 
			} else { 
				this.cargoUsuario = objData.cargo;
			}
			this.isLoading = false;
		},
		cargarInfo: async function(){
			const urlParams = new URLSearchParams(window.location.search);
			const id = urlParams.get('id');
			const formData = new FormData();
			let fecha1 = document.getElementById('fc_1198971545').value;
			let fecha2 = document.getElementById('fc_1198971546').value;
			formData.append("action","get");
			formData.append("FechaI",fecha1);
			formData.append("FechaF",fecha2);
			formData.append("Cargo",this.cargoUsuario);
			formData.append("id_gia",id);
			this.isLoading = true;
			const response = await fetch(URL,{method:"POST",body:formData});
			const objData = await response.json();
			this.arrData = objData;
			this.arrDataCopy = objData;
			this.txtResults = this.arrData.length;
			this.isLoading = false;
		},
		searchx: function() {
		},
		iniFecha: function() {
		},
		editItem: function(id, estado, responsable, tipoTarea){
			if(tipoTarea === 'RA'){
				if(responsable === '1'){
					switch (estado){
						case 'LN':
						case 'LS':
						case 'AC':
						case 'AR':

						break;
						case 'AN':
							let carg = app.cargoUsuario;
							window.location.href='mipg-tereasIdealResponder.php?id='+id+'&est='+estado+'&carg='+carg;
						break;
					}
				}
			} else {
				if(responsable === '1'){

				}
			}
		},
		
	}
});