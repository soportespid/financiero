<?php
	require_once '../../../../comun.inc';
	require '../../../../funciones.inc';
	require '../../../../funcionesSP.inc.php';
	session_start();
	if($_POST){
		$base = isset($_POST['base']) ?  base64_decode($_POST['base']) : null;
		$usuario = isset($_POST['usuario']) ? base64_decode($_POST['usuario']) : null;
		$obj = new Plantilla($base, $usuario);
		switch ($_POST['action']){
			case 'infoInicial':
				$obj->getData();
			break;
			case 'iniTipoRadica':
				$obj->iniTipoRadica();
			break;
			case 'obtenerMinMax':
				$obj->obtenerMinMax();
			break;
			case 'iniFuncionarios':
				$obj->iniFuncionarios();
			break;
			case 'iniFuncionarios':
				$obj->iniFuncionarios();
			break;
			case 'save':
				$obj->guardaRespuesta();
			break;
			case 'uploadFiles': 
				$obj->uploadFiles($_POST['txtRadicado'], $_FILES['archivos']); 
			break;
		}
	}
	class Plantilla{
		private $linkbd;
		public function __construct($base = null, $usuario = null) {
			if (!empty($base) && !empty($usuario)) {
				$this->linkbd = conectar_Multi($base, $usuario);
			} else {
				$this->linkbd = conectar_v7();
			}
			$this->linkbd->set_charset("utf8");
		}
		public function getData(){
			if(!empty($_SESSION)){
				$id = strClean($_POST['codigo']);
				$request = $this->selectData($id);
				$arrData = array(
					"status"=>true,
					"data"=>$request
				);
				echo json_encode($arrData,JSON_UNESCAPED_UNICODE);
			}
			die();
		}
		public function selectData($id){
			$this->intId = $id;
			
			//carga la infoermacion el archivo general
			$sql = "SELECT * FROM planacradicacion WHERE tipot = 'RA' AND numeror = '$id'";
			$request['planacradicacion'] = mysqli_fetch_all(mysqli_query($this->linkbd, $sql), MYSQLI_ASSOC);
			if (!empty($request['planacradicacion'])) {
				$registro = $request['planacradicacion'][0]; 
				if (!empty($registro['idtercero'])) {
					$tercerosSql = "SELECT CONCAT(razonsocial, ' ', nombre1, ' ', nombre2, ' ', apellido1, ' ', apellido2) AS nombre FROM terceros WHERE cedulanit = {$registro['idtercero']}";
					$tercerosResult = mysqli_query($this->linkbd, $tercerosSql);
					$nombreTerceros = mysqli_fetch_assoc($tercerosResult)['nombre'];
					$registro['nombreTerceros'] = $nombreTerceros;
				} else {
					$registro['nombreTerceros'] = '';
				}
				$request['planacradicacion'][0] = $registro;
			}
			
			//carga la informacion del tercero sin documento
			$sql = "SELECT * FROM planradicacionsindoc WHERE numeror = '$id'";
			$request['planradicacionsindoc'] = mysqli_fetch_all(mysqli_query($this->linkbd,$sql),MYSQLI_ASSOC);

			//cargar informacion responsables 
			$sql = "SELECT * FROM planacresponsables WHERE tipot = 'RA' AND codradicacion = '$id' ORDER BY codradicacion ASC"; 
			$request['planacresponsables'] = mysqli_fetch_all(mysqli_query($this->linkbd, $sql), MYSQLI_ASSOC); 
			$total = count($request['planacresponsables']);
			for ($i=0; $i < $total; $i++) {
				$registro = $request['planacresponsables'][$i];

				// Traer nombre del funcionario y estado del usuario
				if (!empty($registro['usuariocon'])) { 
					$funcionarioSql = "SELECT CONCAT(razonsocial, ' ', nombre1, ' ', nombre2, ' ', apellido1, ' ', apellido2) AS nombre FROM terceros WHERE cedulanit = {$registro['usuariocon']}"; 
					$funcionarioResult = mysqli_query($this->linkbd, $funcionarioSql); 
					$registro['nombreFuncionario'] = mysqli_fetch_assoc($funcionarioResult)['nombre']; 

					$usuarioSql = "SELECT id_usu FROM usuarios WHERE cc_usu = {$registro['usuariocon']} AND est_usu = '1'"; 
					$usuarios = mysqli_fetch_all(mysqli_query($this->linkbd, $usuarioSql), MYSQLI_ASSOC); 
					$totalUsuarios = count($usuarios); 
					$registro['usuarioest'] = $totalUsuarios > 0 ? 'S' : 'N';
				} else { 
					$registro['nombreFuncionario'] = '';
					$registro['usuarioest'] = 'N';
				}
				if (!empty($registro['usuarioasig'])) { 
					$funcionarioSql = "SELECT CONCAT(razonsocial, ' ', nombre1, ' ', nombre2, ' ', apellido1, ' ', apellido2) AS nombre FROM terceros WHERE cedulanit = {$registro['usuarioasig']}"; 
					$funcionarioResult = mysqli_query($this->linkbd, $funcionarioSql); 
					$registro['nombreAsigna'] = mysqli_fetch_assoc($funcionarioResult)['nombre']; 
				} else {
					$registro['nombreAsigna'] = '';
				}

				// Traer nombre del cargo 
				if (!empty($registro['codcargo'])) { 
					$cargoSql = "SELECT nombrecargo FROM planaccargos WHERE codcargo = {$registro['codcargo']}"; 
					$cargoResult = mysqli_query($this->linkbd, $cargoSql); 
					$registro['nombreCargo'] = mysqli_fetch_assoc($cargoResult)['nombrecargo']; 
				} else { 
					$registro['nombreCargo'] = ''; 
				}

				// Traer nombre de la dependencia 
				if (!empty($registro['dependencia'])) { 
					$dependenciaSql = "SELECT nombrearea FROM planacareas WHERE codarea = {$registro['dependencia']}"; 
					$dependenciaResult = mysqli_query($this->linkbd, $dependenciaSql); 
					$registro['nombreDependencia'] = mysqli_fetch_assoc($dependenciaResult)['nombrearea']; 
				} else { 
					$registro['nombreDependencia'] = ''; 
				}

				$request['planacresponsables'][$i] = $registro;
			}

			$sql = "SELECT * FROM planacarchivosad WHERE tipot = 'RA' AND idradicacion = '$id'";
			$request['planacarchivosad'] = mysqli_fetch_all(mysqli_query($this->linkbd,$sql),MYSQLI_ASSOC);
			

			return array("consecutivos"=>$request);
		}
		public function iniTipoRadica(){
			if(!empty($_SESSION)){
				echo json_encode($this->selectTipoRadica(),JSON_UNESCAPED_UNICODE);
			}
			die();
		}
		public function selectTipoRadica(){
			$sql = "SELECT codigo, nombre, dias, tdias, slectura, adjunto, clasificacion  FROM plantiporadicacion WHERE estado = 'S' AND radotar = 'RA' ORDER BY nombre ASC";
			$request = mysqli_fetch_all(mysqli_query($this->linkbd,$sql),MYSQLI_ASSOC);
			$total = count($request);
			$tiposPQR = array( 
				"N" => "N - Ninguno",
				"P" => "P - Petición",
				"Q" => "Q - Queja",
				"R" => "R - Reclamo",
				"S" => "S - Sugerencia",
				"D" => "D - Denuncia",
				"F" => "F - Felicitación"
			);
			for ($i = 0; $i < $total; $i++) {
				$request[$i]['tipoPQR'] = isset($tiposPQR[$request[$i]['clasificacion']]) ? $tiposPQR[$request[$i]['clasificacion']] : $request[$i]['clasificacion'];
			}
			return $request;
		}
		public function obtenerMinMax() {
			$id = $_POST['id'];
			$cargo = $_POST['cargo'];
		
			// Encontrar el número anterior con el cargo correspondiente
			$stmtAnterior = $this->linkbd->prepare(
				"SELECT p.numeror, r.estado
				FROM planacradicacion AS p
				INNER JOIN planacresponsables AS r ON p.numeror = r.codradicacion
				WHERE p.numeror < ? AND r.codcargo = ? AND p.tipot = r.tipot
				ORDER BY p.numeror DESC LIMIT 1"
			);
			$stmtAnterior->bind_param('ii', $id, $cargo);
			$stmtAnterior->execute();
			$resultAnterior = $stmtAnterior->get_result();
			$resultAnterior = $resultAnterior->fetch_assoc();
			$anterior = isset($resultAnterior['numeror']) ? $resultAnterior['numeror'] : null;
			$estadoAnterior = isset($resultAnterior['estado']) ? $resultAnterior['estado'] : null;
			$stmtAnterior->close();
		
			// Encontrar el número siguiente con el cargo correspondiente
			$stmtSiguiente = $this->linkbd->prepare(
				"SELECT p.numeror, r.estado
				FROM planacradicacion AS p
				INNER JOIN planacresponsables AS r ON p.numeror = r.codradicacion
				WHERE p.numeror > ? AND r.codcargo = ? AND p.tipot = r.tipot
				ORDER BY p.numeror ASC LIMIT 1"
			);
			$stmtSiguiente->bind_param('ii', $id, $cargo);
			$stmtSiguiente->execute();
			$resultSiguiente = $stmtSiguiente->get_result();
			$resultSiguiente = $resultSiguiente->fetch_assoc();
			$siguiente = isset($resultSiguiente['numeror']) ? $resultSiguiente['numeror'] : null;
			$estadoSiguiente = isset($resultSiguiente['estado']) ? $resultSiguiente['estado'] : null;
			$stmtSiguiente->close();
		
			echo json_encode(
				[
					'anterior' => $anterior, 
					'estadoAnterior' => $estadoAnterior,
					'siguiente' => $siguiente,
					'estadoSiguiente' => $estadoSiguiente
				], 
				JSON_UNESCAPED_UNICODE
			);
		}		
		public function iniFuncionarios(){
			if(!empty($_SESSION)){
				$arrData = array(
					"funcionarios" =>$this->selectFuncionarios()
				);
				echo json_encode($arrData,JSON_UNESCAPED_UNICODE);
			}
			die();
		}
		public function selectFuncionarios(){
			$sql="SELECT CONCAT(t.razonsocial, ' ', t.nombre1, ' ', t.nombre2, ' ', t.apellido1, ' ', t.apellido2) AS nombre, t.cedulanit AS documento, pl.nombrecargo, pl.codcargo, pl.dependencia , pa.nombrearea
			FROM planestructura_terceros pt 
			INNER JOIN terceros t ON pt.cedulanit = t.cedulanit 
			INNER JOIN planaccargos pl ON pt.codcargo = pl.codcargo 
			INNER JOIN planacareas pa ON pa.codarea = pl.dependencia WHERE pt.estado = 'S';";
			$request = mysqli_fetch_all(mysqli_query($this->linkbd,$sql),MYSQLI_ASSOC);
			$total = count($request);
			for ($i=0; $i < $total; $i++) {
				$sql = "SELECT id_usu FROM usuarios WHERE cc_usu = '".$request[$i]['documento']."' AND est_usu = '1'";
				$usuarios = mysqli_fetch_all(mysqli_query($this->linkbd,$sql),MYSQLI_ASSOC);
				$totalUsuarios = count($usuarios);
				$request[$i]['usuarioest'] = $totalUsuarios > 0 ? 'S' : 'N';
			}
			return $request;
		}
		public function guardaRespuesta(){
			if(!empty($_SESSION)){
				$vRadicado = $_POST["Radicado"];
				$vEstado = $_POST["Estado"];
				$vRespuesta = $_POST["Respuesta"];
				$vConcepto = $_POST["Concepto"];
				$vdocFuncionario = $_POST["docFuncionario"];
				$this->uploadFiles($vRadicado, $_FILES['archivos']);
				$request = $this->insertData(array(
					"Radicado"=>$vRadicado, 
					"Estado"=>$vEstado, 
					"Respuesta"=>$vRespuesta, 
					"Concepto"=>$vConcepto, 
					"Funcionario"=>$vdocFuncionario
				));
				if(is_numeric($request) && $request > 0 ){
					$arrResponse = array("status" => true, "msg" => "Datos guardados.");
				} else {
					$arrResponse = array("status" => false, "msg" => "Ha ocurrido un error, intente de nuevo.");
				}
				echo json_encode($arrResponse, JSON_UNESCAPED_UNICODE);
			}
			die();
		}
		public function uploadFiles($txtRadicado, $files) {
			$uploadDir = '../../../../informacion/documentosradicados/RP/' . $txtRadicado . '/'; 
			$zipDir = '../../../../informacion/documentosradicados/RP/zip/'; 
			if (!file_exists($uploadDir)) { 
				mkdir($uploadDir, 0777, true); 
			}
			if (!file_exists($zipDir)) { 
				mkdir($zipDir, 0777, true); 
			}
			$zip = new ZipArchive();
			$zipFile = $zipDir . $txtRadicado . '.zip';
			if ($zip->open($zipFile, ZipArchive::CREATE | ZipArchive::OVERWRITE) === TRUE) {
				foreach ($files['name'] as $key => $name) { 
					$tmpName = $files['tmp_name'][$key]; 
					$destino = $uploadDir . $name; 
					
					// Mover archivo a la carpeta correspondiente
					if (move_uploaded_file($tmpName, $destino)) { 
						// Agregar archivo al ZIP
						if ($zip->addFile($destino, $name)) {
							// Archivo agregado al ZIP exitosamente
						} else {
							echo 'Error al agregar el archivo: ' . $name;
						}
						$this->saveFileNameToDatabase($this->getNextArchivoId(), $txtRadicado, $name);
					} else {
						echo 'Error al mover el archivo: ' . $name;
					}
				}
				$zip->close();
			} else {
				echo 'Error al crear el archivo ZIP';
			}
		}
		public function insertData(array $data){
			$this->arrData = $data;
			$fecha_actual = date("Y-m-d");
			$hora_actual = date("H:i:s");
			if($this->arrData['Funcionario'] == ''){
				$sql = "UPDATE planacresponsables SET
				estado = 'AC',
				fechares = '$fecha_actual',
				respuesta = '{$this->arrData['Respuesta']}',
				idhistory = '1',
				horresp = '$hora_actual' 
				WHERE codradicacion = '{$this->arrData['Radicado']}' AND tipot='RA' AND estadores = '1'
				)";
				mysqli_query($this->linkbd,$sql);
				$sqlr = "UPDATE planacradicacion SET
				estado = 'AC' 
				WHERE numeror = '{$this->arrData['Radicado']}' AND tipot='RA'"; 
				$request = intval(mysqli_query($this->linkbd,$sql));
				return $request;
			}else{
				$sql = "UPDATE planacresponsables SET
				estado = 'AR',
				fechares = '$fecha_actual',
				respuesta = '{$this->arrData['Concepto']}',
				idhistory = '1',
				horresp = '$hora_actual',
				estadores = '0'
				WHERE codradicacion = '{$this->arrData['Radicado']}' AND tipot='RA' AND estadores = '1'
				)";
				
			}
		}
	}
?>