const URL ='vue/administracion/menus/adm-organizarMenus.php';
var app = new Vue({
	el: '#myapp',
	data() {
		return {
			isLoading:false,
			selectEntidades:1,
			arrEntidades:[],
			selectModulos1:'',
			arrModulos1:[],
			arrModulosDetalles: [],
			arrOpciones: [],
			selectedDetalle: null, 
			selectModulos2:'',
			arrModulos2:[],
			arrModulosDetalles2: [],
			arrOpciones2: [],
			selectedDetalle2: null,
			showModal: false,
			currentOption: { 
				id_opcion: null,
				nom_opcion: '',
				ruta_opcion: '',
				comando: '',
				orden: ''
			}
		};
	},
	mounted(){
		this.entidadInicio(); 
		this.modulo1Inicio(); 
		this.modulo2Inicio(); 
		this.$nextTick(() => { 
			this.initSortable(); 
		});
	},
	methods: {
		entidadInicio: async function(){
			const formData = new FormData();
			formData.append("action","iniEntidad");
			this.isLoading = true;
			const response = await fetch(URL,{method:"POST",body:formData});
			const objData = await response.json();
			this.arrEntidades = objData;
			this.isLoading = false;
		},
		modulo1Inicio: async function(){
			const entiIndex = this.getSelectedIndex();
			const base = this.arrEntidades[entiIndex]?.base || '';
			const usuario = this.arrEntidades[entiIndex]?.usuario || '';
			const formData = new FormData();
			formData.append("action","iniModulo1");
			formData.append("base", base);
			formData.append("usuario", usuario);
			this.isLoading = true;
			const response = await fetch(URL,{method:"POST",body:formData});
			const objData = await response.json();
			this.arrModulos1 = objData;
			this.isLoading = false;
		},
		modulo2Inicio: async function(){ 
			const entiIndex = this.getSelectedIndex();
			const base = this.arrEntidades[entiIndex]?.base || '';
			const usuario = this.arrEntidades[entiIndex]?.usuario || '';
			const formData = new FormData();
			formData.append("action","iniModulo2");
			formData.append("base", base);
			formData.append("usuario", usuario);
			this.isLoading = true;
			const response = await fetch(URL,{method:"POST",body:formData});
			const objData = await response.json();
			this.arrModulos2 = objData;
			this.isLoading = false; 
		},
		obtenerDetallesModulo: async function(){
			const formData = new FormData();
			formData.append("action", "obtenerDetallesModulo");
			formData.append("id_modulo", this.selectModulos1); 
			this.isLoading = true;
			const response = await fetch(URL, { method: "POST", body: formData });
			const objData = await response.json();
			this.arrModulosDetalles = objData;
			this.isLoading = false;
		},
		obtenerOpciones: async function(id_modulo, id_detalle, targetArray) { 
			const formData = new FormData(); 
			formData.append("action", "obtenerOpciones"); 
			formData.append("id_modulo", id_modulo); 
			formData.append("id_detalle", id_detalle); 
			this.isLoading = true; 
			const response = await fetch(URL, { method: "POST", body: formData }); 
			const objData = await response.json(); 
			this.$set(targetArray, id_detalle, objData); 
			this.isLoading = false; 
			this.initSortable(); 
		},
		cambioSelectEntidades: function(){
			this.selectModulos1 = '';
			this.arrModulos1 = [];
			this.arrModulosDetalles = [];
			this.arrOpciones = [];
			this.selectedDetalle = null;
			this.modulo1Inicio();
			this.selectModulos2 = '';
			this.arrModulos2 = [];
			this.arrModulosDetalles2 = [];
			this.arrOpciones2 = [];
			this.selectedDetalle2 = null;
			this.modulo2Inicio(); 
		},
		cambioSelectModulos1: function(){
			this.arrModulosDetalles = [];
			this.arrOpciones = [];
			this.selectedDetalle = null;
			this.obtenerDetallesModulo1();
		},
		
		cambioSelectModulos2: function(){ 
			this.arrModulosDetalles2 = [];
			this.arrOpciones2 = [];
			this.selectedDetalle2 = null;
			this.obtenerDetallesModulo2();
		},
		obtenerDetallesModulo1: async function(){
			const formData = new FormData();
			formData.append("action", "obtenerDetallesModulo");
			formData.append("id_modulo", this.selectModulos1);
			this.isLoading = true;
			const response = await fetch(URL, { method: "POST", body: formData });
			const objData = await response.json();
			this.arrModulosDetalles = objData;
			this.isLoading = false;
		},
		obtenerDetallesModulo2: async function(){
			const formData = new FormData();
			formData.append("action", "obtenerDetallesModulo");
			formData.append("id_modulo", this.selectModulos2);
			this.isLoading = true;
			const response = await fetch(URL, { method: "POST", body: formData });
			const objData = await response.json();
			this.arrModulosDetalles2 = objData;
			this.isLoading = false;
		},
		getSelectedIndex: function(){
			return this.arrEntidades.findIndex(e => e.id === this.selectEntidades);
		},
		toggleSubmenu: async function(id_detalle) {
			const allItems1 = document.querySelectorAll('.dropdown-menu li[data-id^="1-"]');
			allItems1.forEach(item => item.classList.remove('selected'));
			const selectedItem = document.querySelector(`.dropdown-menu li[data-id="1-${id_detalle}"]`);
			selectedItem.classList.add('selected');
			this.selectedDetalle = id_detalle;
			await this.obtenerOpciones(this.selectModulos1, id_detalle, this.arrOpciones);
		},
		toggleSubmenu2: async function(id_detalle) {
			const allItems2 = document.querySelectorAll('.dropdown-menu li[data-id^="2-"]');
			allItems2.forEach(item => item.classList.remove('selected'));
			const selectedItem = document.querySelector(`.dropdown-menu li[data-id="2-${id_detalle}"]`);
			selectedItem.classList.add('selected');
			this.selectedDetalle2 = id_detalle;
			await this.obtenerOpciones(this.selectModulos2, id_detalle, this.arrOpciones2);
		},
		save:function(){
			
		},
		openModal: function(option){
			this.currentOption = { ...option };
			this.showModal = true;
		},
		saveOption: async function(){
			const formData = new FormData();
			formData.append("action", "updateOption");
			formData.append("id_opcion", this.currentOption.id_opcion);
			formData.append("nom_opcion", this.currentOption.nom_opcion);
			formData.append("ruta_opcion", this.currentOption.ruta_opcion);
			formData.append("comando", this.currentOption.comando);
			formData.append("orden", this.currentOption.orden);
			this.isLoading = true; await fetch(URL, { method: "POST", body: formData });
			this.isLoading = false; this.showModal = false;
			this.obtenerOpciones(this.selectModulos1, this.selectedDetalle, this.arrOpciones);
			this.obtenerOpciones(this.selectModulos2, this.selectedDetalle2, this.arrOpciones2); 
		},
		cancelEdit: function() {
			this.showModal = false;
		},
		initSortable: function() {
			this.$nextTick(() => {
				if (this.$refs.sortable1) {
					new Sortable(this.$refs.sortable1, {
						group: 'shared', 
						animation: 150,
						onEnd: (evt) => {
							this.updateOrder(evt, this.arrOpciones, this.selectedDetalle, this.selectModulos1);
						},
						draggable: '.draggable'
					});
				}
				if (this.$refs.sortable2) {
					new Sortable(this.$refs.sortable2, {
						group: 'shared',
						animation: 150,
						onEnd: (evt) => {
							this.updateOrder(evt, this.arrOpciones2, this.selectedDetalle2, this.selectModulos2);
						},
						draggable: '.draggable'
					});
				}
			});
		},		
		updateOrder: async function(evt, targetArray, selectedDetail, currentModule) {
			const oldModule = evt.from === this.$refs.sortable1 ? this.selectModulos1 : this.selectModulos2;
			const oldDetail = evt.from === this.$refs.sortable1 ? this.selectedDetalle : this.selectedDetalle2;
			const newModule = evt.to === this.$refs.sortable1 ? this.selectModulos1 : this.selectModulos2;
			const newDetail = evt.to === this.$refs.sortable1 ? this.selectedDetalle : this.selectedDetalle2;
			const movedItem = targetArray[selectedDetail].splice(evt.oldIndex, 1)[0];
			if (evt.to === this.$refs.sortable1) {
				this.arrOpciones[newDetail].splice(evt.newIndex, 0, movedItem);
			} else {
				this.arrOpciones2[newDetail].splice(evt.newIndex, 0, movedItem);
			}
			this.arrOpciones.forEach((opciones, detailIndex) => {
				if (Array.isArray(opciones)) {
					opciones.forEach((item, index) => {
						item.orden = index;
					});
				}
			});
			this.arrOpciones2.forEach((opciones, detailIndex) => {
				if (Array.isArray(opciones)) {
					opciones.forEach((item, index) => {
						item.orden = index;
					});
				}
			});
			const formData = new FormData();
			formData.append("action", "updateOrder");
			formData.append("id_opcion", movedItem.id_opcion);
			formData.append("new_order", evt.newIndex);
			formData.append("new_module", newModule);
			formData.append("new_detail", newDetail);
			formData.append("old_module", oldModule);
			formData.append("old_detail", oldDetail);
			console.log('FormData before sending:', {
				id_opcion: movedItem.id_opcion,
				new_order: evt.newIndex,
				new_module: newModule,
				new_detail: newDetail,
				old_module: oldModule,
				old_detail: oldDetail,
			});
			this.isLoading = true;
			const response = await fetch(URL, { method: "POST", body: formData });
			const result = await response.json();
			this.isLoading = false;
			if (result.status === 'success') {
				console.log('Order updated successfully');
			} else {
				console.error('Error updating order:', result.message);
			}
		},
		deleteOption: async function() {
			const formData = new FormData();
			formData.append("action", "deleteOption");
			formData.append("id_opcion", this.currentOption.id_opcion);
			this.isLoading = true; 
			await fetch(URL, { method: "POST", body: formData });
			this.isLoading = false;
			this.showModal = false;
			this.obtenerOpciones(this.selectModulos1, this.selectedDetalle, this.arrOpciones);
			this.obtenerOpciones(this.selectModulos2, this.selectedDetalle2, this.arrOpciones2);
		},
		confirmSave() {
			Swal.fire({
				title: '¿Estás seguro?',
				text: "¡Vas a guardar los cambios!",
				icon: 'warning',
				showCancelButton: true,
				confirmButtonColor: '#3085d6',
				cancelButtonColor: '#d33',
				confirmButtonText: 'Sí, guardar'
			}).then((result) => {
				if (result.isConfirmed) {
					this.saveOption();
					Swal.fire(
						'¡Guardado!',
						'Tus cambios han sido guardados.',
						'success'
					)
				} else if (result.dismiss === Swal.DismissReason.cancel) {
					Swal.fire(
					'Cancelado',
					'No se han realizado cambios.',
					'error'
					)
				}
			})
		},
		confirmDelete() {
			Swal.fire({
				title: '¿Estás seguro?',
				text: "¡No podrás revertir esta acción!",
				icon: 'warning',
				showCancelButton: true,
				confirmButtonColor: '#3085d6',
				cancelButtonColor: '#d33',
				confirmButtonText: 'Sí, eliminar'
			}).then((result) => {
				if (result.isConfirmed) {
					this.deleteOption();
					Swal.fire(
						'¡Eliminado!',
						'El ítem ha sido eliminado.',
						'success'
					)
				}
			})
		},
	}
});