<?php
	require_once '../../../comun.inc';
	require '../../../funciones.inc';
	session_start();
	if($_POST){
		$base = isset($_POST['base']) ?  base64_decode($_POST['base']) : null;
		$usuario = isset($_POST['usuario']) ? base64_decode($_POST['usuario']) : null;
		$obj = new Plantilla($base, $usuario);
		switch ($_POST['action']){
			case 'iniEntidad':
				$obj->entidadInicio();
			break;
			case 'iniModulo1':
				$obj->modulo1Inicio();
			break;
			case 'iniModulo2':
				$obj->modulo2Inicio(); 
			break;
			case 'obtenerDetallesModulo':
				$obj->getModuleDetails();
			break;
			case 'obtenerOpciones':
				$obj->getOpciones();
			break;
			case 'updateOption':
				$obj->updateOption();
			break;
			case 'updateOrder':
				$obj->updateOrder();
			break;
			case 'deleteOption':
				$obj->deleteOption();
			break;
		}
	}
	class Plantilla{
		private $linkbd;
		public function __construct($base = null, $usuario = null) {
			if (!empty($base) && !empty($usuario)) {
				$this->linkbd = conectar_Multi($base, $usuario);
			} else {
				$this->linkbd = conectar_v7();
			}
			$this->linkbd->set_charset("utf8");
		}
		public function entidadInicio(){
			if(!empty($_SESSION)){
				echo json_encode($this->selectEntidades(),JSON_UNESCAPED_UNICODE);
			}
			die();
		}
		public function selectEntidades(){
			$sql = "SELECT id, entidad, base, usuario FROM infogeneral ORDER BY id ASC";
			$request = mysqli_fetch_all(mysqli_query($this->linkbd,$sql),MYSQLI_ASSOC);
			return $request;
		}
		public function modulo1Inicio(){
			if(!empty($_SESSION)){
				echo json_encode($this->selectModulo1(),JSON_UNESCAPED_UNICODE);
			}
			die();
		}
		public function modulo2Inicio(){
			if(!empty($_SESSION)){ 
				echo json_encode($this->selectModulo2(), JSON_UNESCAPED_UNICODE); 
			} 
			die(); 
		}
		public function selectModulo1(){
			$sql = "SELECT id_modulo, nombre, libre FROM modulos ORDER BY id_modulo ASC";
			$request = mysqli_fetch_all(mysqli_query($this->linkbd,$sql),MYSQLI_ASSOC);
			return $request;
		}
		public function selectModulo2(){
			$sql = "SELECT id_modulo, nombre, libre FROM modulos ORDER BY id_modulo ASC";
			$request = mysqli_fetch_all(mysqli_query($this->linkbd, $sql), MYSQLI_ASSOC);
			return $request;
		}
		public function getModuleDetails() {
			if (!empty($_SESSION)) {
				$id_modulo = $_POST['id_modulo'];
				echo json_encode($this->selectModulosDet($id_modulo), JSON_UNESCAPED_UNICODE);
			}
			die();
		}
		public function selectModulosDet($id_modulo) {
			$sql = "SELECT id_item, id_detalle, id_modulo, nombre FROM modulos_det WHERE id_modulo = '$id_modulo' ORDER BY id_detalle ASC";
			$request = mysqli_fetch_all(mysqli_query($this->linkbd, $sql), MYSQLI_ASSOC);
			return $request;
		}
		public function getOpciones() {
			if (!empty($_SESSION)) {
				$id_modulo = $_POST['id_modulo'];
				$niv_opcion = $_POST['id_detalle'];
				echo json_encode($this->selectOpciones($id_modulo, $niv_opcion), JSON_UNESCAPED_UNICODE);
			}
			die();
		}
		public function selectOpciones($id_modulo, $niv_opcion) {
			$sql = "SELECT id_opcion, nom_opcion, ruta_opcion, orden, comando FROM opciones WHERE modulo = '$id_modulo' AND niv_opcion = '$niv_opcion' ORDER BY orden ASC";
			$request = mysqli_fetch_all(mysqli_query($this->linkbd, $sql), MYSQLI_ASSOC);
			return $request;
		}
		public function updateOption() {
			if (!empty($_SESSION)) {
				$id_opcion = $_POST['id_opcion'];
				$nom_opcion = $_POST['nom_opcion'];
				$ruta_opcion = $_POST['ruta_opcion'];
				$comando = $_POST['comando'];
				$orden = $_POST['orden'];
				$sql = "UPDATE opciones SET nom_opcion = '$nom_opcion', ruta_opcion = '$ruta_opcion', comando = '$comando', orden = '$orden' WHERE id_opcion = '$id_opcion'";
				mysqli_query($this->linkbd, $sql);
			}
			die();
		}
		public function updateOrder() {
			if (!empty($_SESSION)) {
				$id_opcion = $_POST['id_opcion'];
				$new_order = $_POST['new_order'];
				$new_module = $_POST['new_module'];
				$new_detail = $_POST['new_detail'];
				$old_module = $_POST['old_module'];
				$old_detail = $_POST['old_detail'];
				error_log("Received new_detail: $new_detail");
				$sql = "UPDATE opciones SET orden = '$new_order', modulo = '$new_module', niv_opcion = '$new_detail' WHERE id_opcion = '$id_opcion'";
				if (mysqli_query($this->linkbd, $sql)) {
					$sql_reorder_new = "SET @count = -1;
										UPDATE opciones SET orden = @count:= @count + 1
										WHERE modulo = '$new_module' AND niv_opcion = '$new_detail'
										ORDER BY orden ASC";
					mysqli_multi_query($this->linkbd, $sql_reorder_new);
					if ($old_module !== $new_module || $old_detail !== $new_detail) {
						$sql_reorder_old = "SET @count = -1;
											UPDATE opciones SET orden = @count:= @count + 1
											WHERE modulo = '$old_module' AND niv_opcion = '$old_detail'
											ORDER BY orden ASC";
						mysqli_multi_query($this->linkbd, $sql_reorder_old);
					}
					echo json_encode(['status' => 'success']);
				} else {
					echo json_encode(['status' => 'error', 'message' => 'Error al actualizar el orden.']);
				}
			}
			die();
		}
		public function deleteOption() {
			if (!empty($_SESSION)) {
				$id_opcion = $_POST['id_opcion'];
				$sql = "DELETE FROM opciones WHERE id_opcion = '$id_opcion'";
				mysqli_query($this->linkbd, $sql);
			}
			die();
		}
	}
?>