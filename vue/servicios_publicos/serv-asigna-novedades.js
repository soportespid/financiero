var app = new Vue({
    el: '#myapp',
	data:{

        corte: '',
        showModal_novedades: false,
        posicionActual: '',
        datosUsuarios: [],
        novedad: [],
        codigoNovedad: [],
        listadoNovedades: [],
        busqueda_codigos: '',
        ruta: '',
        rutas: [],
        error: '',
        loading: false,
    },

    mounted: function(){

        this.valoresIniciales();
    },

    methods: 
    {
        llenadoNovedad: function() {

            for (let i = 0; i < app.datosUsuarios.length; i++) {
                            
                this.novedad[i] = app.datosUsuarios[i][4];
                this.codigoNovedad[i] = app.datosUsuarios[i][3];
            }
        },

        valoresIniciales: async function()
        {
            await axios.post('vue/servicios_publicos/serv-asigna-novedades.php')
            .then((response) => {
                app.corte = response.data.corte; 
                app.rutas = response.data.rutas;
                
            });
        },

        buscaUsuarios: async function()
        {
        
            if (this.corte != '')
            {
                if (this.ruta != '')
                {
                    this.loading =  true;

                    await axios.post('vue/servicios_publicos/serv-asigna-novedades.php?action=listausuarios&ruta='+this.ruta+'&corte='+this.corte)
                    .then((response) => {

                        app.datosUsuarios = response.data.datosUsuarios;
                        app.llenadoNovedad();
                    }).catch((error) => {
                        this.error = true;
                        console.log(error)
                    }).finally(() => {
                        this.loading =  false
                    });     
                }
                else
                {
                    Swal.fire({
                        icon: 'warning',
                        title: 'Falta infomación',
                        text: 'Seleccione una ruta'
                    })    
                }
            }
            else
            {
                let oops = "404 no encontre corte";
                throw new Error(oops);
            }
        },

        ventanaCuenta: function(posicion){

            app.showModal_novedades = true;
            this.posicionActual = posicion;

            axios.post('vue/servicios_publicos/serv-asigna-novedades.php?action=cargaNovedades')
            .then((response) => {

                app.listadoNovedades = response.data.listadoNovedades;
            });     
        },

        seleccionarNovedad: function(novedad) {

            this.novedad[this.posicionActual] = novedad[0]+" - "+novedad[1];
            this.codigoNovedad[this.posicionActual] = novedad[0];
            app.showModal_novedades = false;
        },  

        guardar: function() {

            Swal.fire({
                icon: 'question',
                title: 'Seguro que quieres guardar?',
                showDenyButton: true,
                confirmButtonText: 'Guardar',
                denyButtonText: 'Cancelar',
                }).then((result) => {
                if (result.isConfirmed) {

                    var formData = new FormData();
    
                    for(let i=0; i <= this.datosUsuarios.length-1; i++)
                    {
                        formData.append('codigoNovedad[]', this.codigoNovedad[i]);
                        formData.append('clientes[]', this.datosUsuarios[i][5]);
                    }

                    formData.append("corte", this.corte);

                    this.loading =  true;

                    axios.post('vue/servicios_publicos/serv-asigna-novedades.php?action=guardar', formData)
                    .then((response) => {
                        
                        if(response.data.insertaBien){
                            this.loading =  false;
                            Swal.fire({
                                icon: 'success',
                                title: 'Se ha guardado con exito',
                                showConfirmButton: false,
                                timer: 1500
                                }).then((response) => {
                                    app.redireccionar();
                                });
                        }
                        else{
                                
                            Swal.fire(
                                'Error!',
                                'No se pudo guardar.',
                                'error'
                            );
                        }
                    });
                } 
                else if (result.isDenied) {
                    Swal.fire('Guardar cancelado', '', 'info')
                }
            })  
        },

        redireccionar: function()
        {
            location.href = "serv-asigna-novedades.php";
        },
    }
});