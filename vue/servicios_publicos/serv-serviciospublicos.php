<?php

    require_once '../../comun.inc';
    require '../../funciones.inc';
    require '../../funcionesSP.inc.php';

    $linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");

    $out = array('error' => false);

    $action = "show";

    if(isset($_GET['action'])){
        $action = $_GET['action'];
    }

    if($action == 'show')
    {
        $zonas = array();
        
        $sqlZonasDeUso = "SELECT id, nombre, estado, alumbrado_publico FROM srvtipo_uso";   
        $resZonasDeUso = mysqli_query($linkbd,$sqlZonasDeUso);
        while($rowZonasDeUso = mysqli_fetch_row($resZonasDeUso))
        {
            array_push($zonas,$rowZonasDeUso);
        }

        $out['zonas'] = $zonas;
    }

    if ($action == 'guardarAlumbrado')
    {
        $id = 1;

        for ($i=0; $i < count($_POST['porcentajes']); $i++) 
        {
            $sql = "UPDATE srvtipo_uso SET alumbrado_publico = ".$_POST['porcentajes'][$i]." WHERE id = $id";
            mysqli_query($linkbd,$sql);

            $id++;
        }

        $out['insertaBien'] = true;
    }

    header("Content-type: application/json");
    echo json_encode($out);
    die();