<?php

    require_once '../../comun.inc';
    require '../../funciones.inc';
    require '../../funcionesSP.inc.php';

    $linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");

    $out = array('error' => false);

    $action = "show";

    if(isset($_GET['action'])){
        $action = $_GET['action'];
    }

    if($action == 'show')
    {
        //Corte 
        $siguienteCorte = '';
        $siguienteCorte = siguienteCorte();
        
        if ($siguienteCorte < 10)
        {
            $siguienteCorte = '0'.$siguienteCorte;
        }
        
        $out['corte'] = $siguienteCorte;


        //servicios

        $listadoServicios = array();

        $sqlServicios = "SELECT id, upper(nombre) FROM srvservicios WHERE estado = 'S' AND tarifa_medidor = 'S'";
        $resServicios = mysqli_query($linkbd,$sqlServicios);
        while($rowServicios = mysqli_fetch_row($resServicios))
        {
            array_push($listadoServicios,$rowServicios);
        }

        $out['listadoServicios'] = $listadoServicios;


        //rutas

        $rutas = array();

        $sqlRutas = "SELECT id, nombre FROM srvrutas WHERE estado = 'S'";
        $resRutas = mysqli_query($linkbd,$sqlRutas);
        while($rowRutas = mysqli_fetch_row($resRutas))
        {
            array_push($rutas, $rowRutas);
        }

        $out['rutas'] = $rutas;
    }

    if ($action == 'listausuarios')
    {
        $idRuta = $_GET['ruta'];
        $corteActual = $_GET['corte'];
        $corteAnterior = $corteActual - 1;
        $corte2 = $corteActual - 2;
        $corte3 = $corteActual - 3;
        $datosUsuarios = array();

        $sqlServicio = "SELECT id FROM srvservicios WHERE nombre = 'ACUEDUCTO'";
        $rowServicio = mysqli_fetch_row(mysqli_query($linkbd, $sqlServicio));

        if ($rowServicio[0] != "") {
            $idAcueducto = $rowServicio[0];
        }
        else {
            $idAcueducto = "";
        }

        $sqlServicio = "SELECT id FROM srvservicios WHERE nombre = 'ALCANTARILLADO'";
        $rowServicio = mysqli_fetch_row(mysqli_query($linkbd, $sqlServicio));

        if ($rowServicio[0] != "") {
            $idAlcantarillado = $rowServicio[0];
        }
        else {
            $idAlcantarillado = "";
        }
        
        $sqlClientes = "SELECT C.id, C.cod_usuario, C.codigo_ruta, T.nombre1, T.nombre2, T.apellido1, T.apellido2, T.razonsocial, D.direccion FROM srvclientes AS C INNER JOIN terceros AS T ON C.id_tercero = T.id_tercero INNER JOIN srvdireccion_cliente AS D ON C.id = D.id_cliente WHERE C.id_ruta = $idRuta AND C.estado = 'S' ORDER BY C.codigo_ruta ASC";
        $resClientes = mysqli_query($linkbd, $sqlClientes);
        while ($rowClientes = mysqli_fetch_row($resClientes)) {

            $sqlAsignaServicios = "SELECT id_medidor, id_servicio FROM srvasignacion_servicio WHERE id_clientes = $rowClientes[0] AND (id_servicio LIKE '$idAcueducto' OR id_servicio LIKE '$idAlcantarillado') AND consumo = 'S' AND estado = 'S'";
            $rowAsignaServicios = mysqli_fetch_row(mysqli_query($linkbd, $sqlAsignaServicios));

            if ($rowAsignaServicios[1] != '') {

                //Inicializo variables para constante uso
                $datos = array();
                $nombre = "";
                $lecturaActual = 0;
                $lecturaAnterior = 0;
                $consumoActual = 0;
                $consumoAnterior = 0;
                $consumo2 = $consumo3 = 0;

                //Concateno nombre o razonsocial
                if ($rowClientes[7] == '') {

                    $nombre = $rowClientes[3] . " " . $rowClientes[4] . " " . $rowClientes[5] . " " . $rowClientes[6];
                    $nombre = Quitar_Espacios($nombre);
                }
                else {
                    $nombre = $rowClientes[7];
                }

                $sqlLectura2 = "SELECT consumo FROM srvlectura WHERE id_cliente = $rowClientes[0] AND corte = $corte2 AND estado = 'S' LIMIT 1";
                $rowLectura2 = mysqli_fetch_row(mysqli_query($linkbd, $sqlLectura2));

                $consumo2 = $rowLectura2[0];

                $sqlLectura3 = "SELECT consumo FROM srvlectura WHERE id_cliente = $rowClientes[0] AND corte = $corte3 AND estado = 'S' LIMIT 1";
                $rowLectura3 = mysqli_fetch_row(mysqli_query($linkbd, $sqlLectura3));

                $consumo3 = $rowLectura3[0];

                $sqlLecturaAnt = "SELECT lectura_medidor, consumo FROM srvlectura WHERE id_cliente = $rowClientes[0] AND corte = $corteAnterior AND estado = 'S' LIMIT 1";
                $rowLecturaAnt = mysqli_fetch_row(mysqli_query($linkbd, $sqlLecturaAnt));

                if ($rowLecturaAnt[0] != '') {
                    $lecturaAnterior = $rowLecturaAnt[0];
                    $consumoAnterior = $rowLecturaAnt[1];
                }

                $sqlLecturaAct = "SELECT lectura_medidor, consumo FROM srvlectura WHERE id_cliente = $rowClientes[0] AND corte = $corteActual AND estado = 'S' LIMIT 1";
                $rowLecturaAct = mysqli_fetch_row(mysqli_query($linkbd, $sqlLecturaAct));

                if ($rowLecturaAct[0] == '') {
                    $lecturaActual = $rowLecturaAnt[0];
                    $consumoActual = 0;
                }
                else {
                    $lecturaActual = $rowLecturaAct[0];
                    $consumoActual = $rowLecturaAct[1];
                }

                array_push($datos, $rowClientes[2]);
                array_push($datos, $rowClientes[1]);
                array_push($datos, $nombre);
                array_push($datos, $rowClientes[8]);
                array_push($datos, $rowAsignaServicios[0]);
                array_push($datos, $lecturaAnterior);
                array_push($datos, $rowClientes[0]);
                array_push($datos, $lecturaActual);
                array_push($datos, $consumoActual);
                array_push($datos, $consumoAnterior);
                array_push($datos, $consumo2);
                array_push($datos, $consumo3);
                array_push($datosUsuarios, $datos);
            }

           
        }
        
        $out['datosUsuarios'] = $datosUsuarios;
    }

    if ($action == 'guardar')
    {
        $usuario = $_SESSION['usuario'];

        $idAcueducto = 1;
        $idAlcantarillado = 2;

        $sqlServicio = "SELECT * FROM srvservicios WHERE id = $idAcueducto AND estado = 'S'";
        $rowServicio = mysqli_fetch_row(mysqli_query($linkbd, $sqlServicio));

        if ($rowServicio[0] != "") {
            $estadoAcueducto = "S";
        }
        else {
            $estadoAcueducto = 'N';
        }

        $sqlServicio = "SELECT * FROM srvservicios WHERE id = $idAlcantarillado AND estado = 'S'";
        $rowServicio = mysqli_fetch_row(mysqli_query($linkbd, $sqlServicio));
    
        if ($rowServicio[0] != "") {
            $estadoAlcantarillado = "S";
        }
        else {
            $estadoAlcantarillado = 'N';
        }

        for ($i=0; $i < count($_POST['lecturas']); $i++) { 
            
            $sqlVal01 = "DELETE FROM srvlectura WHERE corte = '$_POST[corte]' AND id_cliente = '".$_POST["clientes"][$i]."'";
            mysqli_query($linkbd, $sqlVal01);


            if ($estadoAcueducto == 'S') {
                $sqlLectura = "INSERT INTO srvlectura (corte, id_cliente, id_servicio, lectura_medidor, consumo, observacion, usuario, estado) VALUES ('$_POST[corte]', '".$_POST["clientes"][$i]."', '$idAcueducto', '".$_POST["lecturas"][$i]."', '".$_POST["consumos"][$i]."', '0', '$usuario', 'S')";
                mysqli_query($linkbd, $sqlLectura);
            } 

            if ($estadoAlcantarillado == 'S') {
                $sqlLectura2 = "INSERT INTO srvlectura (corte, id_cliente, id_servicio, lectura_medidor, consumo, observacion, usuario, estado) VALUES ('$_POST[corte]', '".$_POST["clientes"][$i]."', '$idAlcantarillado', '".$_POST["lecturas"][$i]."', '".$_POST["consumos"][$i]."', '0', '$usuario', 'S')";
                mysqli_query($linkbd, $sqlLectura2);
            }
        }

        $usuario = $_SESSION["usuario"];
        $cedula = $_SESSION["cedulausu"];
        $ip = getRealIP();
        $hoy = date("Y-m-d H:i:s");

        $sqlAuditoria = "INSERT INTO srv_auditoria (id_srv_funciones, accion, cedula, usuario, ip, fecha_hora, consecutivo) VALUES (2, 'Crear', '$cedula', '$usuario', '$ip', '$hoy', '$_POST[corte]')";
        mysqli_query($linkbd, $sqlAuditoria);

        $out['insertaBien'] = true;
    }

    header("Content-type: application/json");
    echo json_encode($out);
    die();