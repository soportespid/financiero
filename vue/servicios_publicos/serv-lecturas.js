var app = new Vue({
    el: '#myapp',
	data:{

        corte: '',
        servicio: '',
        serviciosDisponibles: [],
        ruta: '',
        rutas: [],
        usuariosLecturas: [],
        lecturaActual: [],
        consumo: [],
        estilos: [],
        showModal_novedades: false,
        posicionActual: '',
        loading: false,
        error: '',
    },

    mounted: function(){

        this.valoresIniciales();
    },

    methods: 
    {
        valoresIniciales: function()
        {
            axios.post('vue/servicios_publicos/serv-lecturas.php')
            .then((response) => {
                app.corte = response.data.corte; 
                app.serviciosDisponibles = response.data.listadoServicios;  
                app.rutas = response.data.rutas;
            });
        },

        llenadoNovedad: function() {

            for (let i = 0; i < app.usuariosLecturas.length; i++) {
                            
                this.lecturaActual[i] = app.usuariosLecturas[i][7];
                this.consumo[i] = app.usuariosLecturas[i][8];
            }
        },

        buscaUsuarios: async function()
        {
            if (this.corte != '')
            {
                if (this.ruta != '')
                {
                    this.loading = true;

                    await axios.post('vue/servicios_publicos/serv-lecturas.php?action=listausuarios&ruta='+this.ruta+'&corte='+this.corte)
                    .then((response) => {
                        app.usuariosLecturas = response.data.datosUsuarios;
                        app.llenadoNovedad();
                    }).catch((error) => {
                        this.error = true;
                        console.log(error)
                    }).finally(() => {
                        this.loading =  false;
                    });     
                }
                else
                {
                    Swal.fire({
                        icon: 'warning',
                        title: 'Falta infomación',
                        text: 'Seleccione una ruta'
                    })    
                }
            }
            else
            {
                let oops = "404 no encontre corte";
                throw new Error(oops);
            }
        },

        calcularConsumo: function(posicion)
        {
            var lecturaAnterior = this.usuariosLecturas[posicion][5];
            var lecturaActual = this.lecturaActual[posicion];
            var consumoActual = lecturaActual - lecturaAnterior;

            if (consumoActual < 0)
            {
                this.estilos[posicion] = "text-align: center; background: red;";
            }
            else
            {
                this.estilos[posicion] = "text-align: center;";
            }

            this.consumo[posicion] = consumoActual;
            this.consumo = Object.values(this.consumo);
        },

        guardar: function()
        {
            if (this.lecturaActual != '')
            {
                Swal.fire({
                    icon: 'question',
                    title: 'Seguro que quieres guardar?',
                    showDenyButton: true,
                    confirmButtonText: 'Guardar',
                    denyButtonText: 'Cancelar',
                    }).then((result) => {
                    if (result.isConfirmed) 
                    {
                        if (result.isConfirmed) {
                            var formData = new FormData();

                            formData.append("corte", this.corte);
                            formData.append("ruta", this.ruta);
    
                            for(let i=0; i <= this.usuariosLecturas.length-1; i++)
                            {
                                formData.append('lecturas[]', this.lecturaActual[i]);   
                                formData.append('consumos[]', this.consumo[i]);
                                formData.append('clientes[]', this.usuariosLecturas[i][6]);
                            }
                            
                            this.loading = true;
                            
                            axios.post('vue/servicios_publicos/serv-lecturas.php?action=guardar', formData)
                            .then((response) => {
                                // console.log(response.data);
                            }).catch((error) => {
                                
                            }).finally(() => {
                        
                                this.loading = false;
                                Swal.fire({
                                    icon: 'success',
                                    title: 'Se ha guardado con exito',
                                    showConfirmButton: false,
                                    timer: 2500
                                }).then((response) => {
                                        app.redireccionar();
                                });    
                            });       
                        }
                    } 
                    else if (result.isDenied) 
                    {
                        Swal.fire('Guardar cancelado', '', 'info')
                    }
                })   
            }
            else
            {
                Swal.fire({
                    icon: 'warning',
                    title: 'No se puede guardar',
                    text: 'Todas las lecturas estan vacias'
                })
            }
        },

        redireccionar: function()
        {
            location.href = "serv-lecturas.php";
        },

        excel: function() {

            if (this.usuariosLecturas != '') {

                document.form2.action="serv-excel-lecturas.php";
				document.form2.target="_BLANK";
				document.form2.submit(); 
            }
            else {
                Swal.fire("Faltan datos", "Genera el informe primero", "warning");
            }
        },
    }
});