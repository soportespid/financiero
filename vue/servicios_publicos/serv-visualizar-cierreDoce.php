<?php

    require_once '../../comun.inc';
    require '../../funciones.inc';
    require '../../funcionesSP.inc.php';

    $linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");

    $out = array('error' => false);

    $action = "show";

    if(isset($_GET['action'])){
        $action = $_GET['action'];
    }

    if($action == 'show') {
        
        $consecutivo = $_GET['cod'];
        
        $sqlCab = "SELECT consecutivo_cierre, fecha, fecha_cierre, num_billetes, num_monedas, num_consignaciones, num_cheques, total_conteo, total_recaudo, total_caja, total_banco FROM srvcierre_diario_cab WHERE consecutivo_cierre = $consecutivo";
        $rowCab = mysqli_fetch_assoc(mysqli_query($linkbd, $sqlCab));

        $out['consecutivo'] = $rowCab['consecutivo_cierre'];
        $out['fecha'] = $rowCab['fecha'];
        $out['fechaCierre'] = $rowCab['fecha_cierre'];
        $out['numBilletes'] = $rowCab['num_billetes'];
        $out['numMonedas'] = $rowCab['num_monedas'];
        $out['numConsignaciones'] = $rowCab['num_consignaciones'];
        $out['numCheques'] = $rowCab['num_cheques'];
        $out['totalConteo'] = $rowCab['total_conteo'];
        $out['totalRecaudo'] = number_format($rowCab['total_recaudo'],2);
        $out['totalCaja'] = number_format($rowCab['total_caja'],2);
        $out['totalBanco'] = number_format($rowCab['total_banco'],2);

        $detalles = array();

        $sqlDet = "SELECT codigo_recaudo, detalle, valor, tipo, forma_pago FROM srvcierre_diario_det WHERE consecutivo_cierre = $consecutivo";
        $resDet = mysqli_query($linkbd, $sqlDet);
        while ($rowDet = mysqli_fetch_row($resDet)) {
            array_push($detalles, $rowDet);
        }

        $out['detalles'] = $detalles;
    }

    header("Content-type: application/json");
    echo json_encode($out);
    die();