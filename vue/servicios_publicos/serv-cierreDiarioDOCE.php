<?php

    require_once '../../comun.inc';
    require '../../funciones.inc';
    require '../../funcionesSP.inc.php';

    $linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");

    $out = array('error' => false);

    $action = "show";

    if(isset($_GET['action'])){
        $action = $_GET['action'];
    }

    if($action == 'show') {
        
        $numeroCierre = selconsecutivo('srvcierre_diario_cab', 'consecutivo_cierre');
        $out['consecutivo'] = $numeroCierre;
    }

    if ($action == 'listadoRecaudos') {

        $fechaCierre = $_GET['diaCierre'];
        $fec = explode("/", $fechaCierre);
        $fecha = $fec[2].'-'.$fec[1].'-'.$fec[0];
        $datosRecaudos = array();

        $sqlRecaudoFactura = "SELECT consecutivo, concepto, valor_pago, tipo_pago FROM srvrecibo_caja WHERE fecha = '$fecha' AND estado = 'S'";
        $resRecaudoFactura = mysqli_query($linkbd, $sqlRecaudoFactura);
        while ($rowRecaudoFactura = mysqli_fetch_row($resRecaudoFactura)) {
            
            $datos = array();
            $tipo = "Pago de factura";

            array_push($datos, $rowRecaudoFactura[0]);
            array_push($datos, $rowRecaudoFactura[1]);
            $valor = number_format($rowRecaudoFactura[2], 2, '.', ',');	
            array_push($datos, $valor);
            array_push($datos, $tipo);
            array_push($datos, $rowRecaudoFactura[3]);
            array_push($datos, $rowRecaudoFactura[2]);
            array_push($datosRecaudos, $datos);
        }

        $sqlAnticipos = "SELECT id, valor_total, recaudo_en FROM srvrecaudo_anticipo WHERE fecha = '$fecha' AND estado != 'N'";
        $resAnticipos = mysqli_query($linkbd, $sqlAnticipos);
        while ($rowAnticipos = mysqli_fetch_row($resAnticipos)) {
            
            $detalle = "PAGO ANTICIPO";
            $tipo = "Recaudo anticipo";
            $valor = number_format($rowAnticipos[1], 2, '.', ',');	
            $datos = array();
            array_push($datos, $rowAnticipos[0]);
            array_push($datos, $detalle);
            array_push($datos, $valor);
            array_push($datos, $tipo);
            array_push($datos, $rowAnticipos[2]);
            array_push($datos, $rowAnticipos[1]);
            array_push($datosRecaudos, $datos);
        }

        $sqlAcuerdosPago = "SELECT id, valor_abono, medio_pago FROM srvacuerdos_pago WHERE fecha = '$fecha' AND estado != 'N' AND valor_abono > 0";
        $resAcuerdosPago = mysqli_query($linkbd, $sqlAcuerdosPago);
        while ($rowAcuerdosPago = mysqli_fetch_row($resAcuerdosPago)) {

            $detalle = "ABONO ACUERDO DE PAGO";
            $tipo = "Acuerdo de pago";
            $tipo2 = $rowAcuerdosPago[2];
            $valor = number_format($rowAcuerdosPago[1], 2, '.', ',');
            $datos = array();

            array_push($datos, $rowAcuerdosPago[0]);
            array_push($datos, $detalle);
            array_push($datos, $valor);
            array_push($datos, $tipo);
            array_push($datos, $tipo2);
            array_push($datos, $rowAcuerdosPago[1]);
            array_push($datosRecaudos, $datos);
        }

        $valorIngresos = 0;
        $valorCaja = 0;
        $valorBanco = 0;

        for ($i=0; $i < count($datosRecaudos); $i++) { 
            
            $valorIngresos += $datosRecaudos[$i][5];

            if ($datosRecaudos[$i][4] == 'caja') {
                $valorCaja += $datosRecaudos[$i][5];
            }
            else {
                $valorBanco += $datosRecaudos[$i][5];
            }
        }

        $out['valorIngresosPuro'] = $valorIngresos;
        $out['valorCajaPuro'] = $valorCaja;
        $out['valorBancoPuro'] = $valorBanco;

        $valorIngresos = number_format($valorIngresos, 2, '.', ',');
        $valorCaja = number_format($valorCaja, 2, '.', ',');
        $valorBanco = number_format($valorBanco, 2, '.', ',');

        $out['datosRecaudos'] = $datosRecaudos;
        $out['valorIngresos'] = $valorIngresos;
        $out['valorCaja'] = $valorCaja;
        $out['valorBanco'] = $valorBanco;
    }
    
    if ($action == 'guardar') {

        $usuario = $_SESSION['usuario'];
        $numeroCierre = selconsecutivo('srvcierre_diario_cab', 'consecutivo_cierre');
        $numBilletes = $_POST['numBilletes'];
        $numMonedas = $_POST['numMonedas'];
        $numConsignaciones = $_POST['numConsignaciones'];
        $numCheques = $_POST['numCheques'];
        $totalConteo = $_POST['totalConteo'];
        $totalRecaudo = $_POST['totalRecaudo'];
        $totalCaja = $_POST['totalCaja'];
        $totalBanco = $_POST['totalBanco'];
        $fecha = $_POST['fecha'];
        $fechaCierre = $_POST['fechaCierre'];
        $fec1 = explode("/", $fecha);
        $fecha = $fec1[2].'-'.$fec1[1].'-'.$fec1[0];
        $fec2 = explode("/", $fechaCierre);
        $fechaCierre = $fec2[2].'-'.$fec2[1].'-'.$fec2[0];

        $sqlCierreCab = "INSERT INTO srvcierre_diario_cab (consecutivo_cierre, fecha, fecha_cierre, num_billetes, num_monedas, num_consignaciones, num_cheques, total_conteo, total_recaudo, total_caja, total_banco, estado) VALUES ($numeroCierre, '$fecha', '$fechaCierre', $numBilletes, $numMonedas, $numConsignaciones, $numCheques, $totalConteo, $totalRecaudo, $totalCaja, $totalBanco, 'S')";
        mysqli_query($linkbd, $sqlCierreCab);

        for ($i=0; $i < count($_POST['consecutivos']); $i++) { 
            
            $sqlCierreDet = "INSERT INTO srvcierre_diario_det (consecutivo_cierre, codigo_recaudo, detalle, valor, tipo, forma_pago) VALUES ($numeroCierre, '".$_POST["consecutivos"][$i]."', '".$_POST["detalle"][$i]."', '".$_POST["valor"][$i]."', '".$_POST["tipo"][$i]."', '".$_POST["formaPago"][$i]."')";
            mysqli_query($linkbd, $sqlCierreDet);
        }

        $out['insertaBien'] = true;
    }

    header("Content-type: application/json");
    echo json_encode($out);
    die();