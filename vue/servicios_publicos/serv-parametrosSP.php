<?php

    require_once '../../comun.inc';
    require '../../funciones.inc';
    require '../../funcionesSP.inc.php';

    $linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");

    $out = array('error' => false);

    $action = "show";

    if(isset($_GET['action'])){
        $action = $_GET['action'];
    }

    if($action == 'show')
    {
        $sql = "SELECT rango_consumo, periodo_facturado, aplica_interes, porcentaje, desactivacion_servicios, facturas_vencidas, porcentaje_acuerdo_pago FROM srvparametros WHERE id = 1";
        $res = mysqli_query($linkbd, $sql);
        $row = mysqli_fetch_row($res);

        $out['parametrizacion'] = $row;
    }

    if ($action == 'guardar')
    {
        $limpiar = "TRUNCATE srvparametros";
        mysqli_query($linkbd,$limpiar);

        $rango_consumo = '';
        $periodo_facturado = '';
        $aplica_interes = '';
        $porcentaje = '';
        $desactivacion_servicios = '';
        $facturas_vencidas = '';
        $porcentaje_acuerdo_pago = '';

        $rango_consumo = $_POST['rangoConsumo'];
        $periodo_facturado = $_POST['periodoFacturado'];
        $aplica_interes = $_POST['interesesMora'];
        $porcentaje_acuerdo_pago = $_POST['porcentajeAcuerdo'];

        if ($aplica_interes == 'S')
        {
            $porcentaje = $_POST['porcentajeIntereses'];
        }
        else
        {
            $porcentaje = 0;
        }

        $desactivacion_servicios = $_POST['desactivacionAutomatica'];

        if ($desactivacion_servicios == 'S')
        {
            $facturas_vencidas = $_POST['facturasVencidas'];
        }
        else
        {
            $facturas_vencidas = 0;
        }

        
        $query = "INSERT INTO srvparametros (id, rango_consumo, periodo_facturado, aplica_interes, porcentaje, desactivacion_servicios, facturas_vencidas, porcentaje_acuerdo_pago) VALUES (1, '$rango_consumo', '$periodo_facturado', '$aplica_interes', $porcentaje, '$desactivacion_servicios', $facturas_vencidas, '$porcentaje_acuerdo_pago')";
        if(mysqli_query($linkbd,$query))
        {
            $out['insertaBien'] = true;    
        }
        else
        {
            $out['insertaBien'] = false;
        }
        
    }

    header("Content-type: application/json");
    echo json_encode($out);
    die();