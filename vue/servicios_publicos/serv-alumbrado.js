var app = new Vue({
    el: '#myapp',
	data:{

        zonas: [],
        porcentajes: [],
        estado: [],
        titulosEstado: [],
    },

    mounted: function(){

        this.valoresIniciales();
    },

    methods: 
    {
        valoresIniciales: function()
        {
            axios.post('vue/servicios_publicos/serv-serviciospublicos.php')
            .then((response) => {
                app.zonas = response.data.zonas;
                for (let i = 0; i <= app.zonas.length-1; i++) {
                    
                    if (app.zonas[i][2] == 'S')
                    {
                        this.estado[i] = "imagenes/sema_verdeON.jpg";
                        this.titulosEstado[i] = 'Activo';
                    }

                    if (app.zonas[i][2] == 'N')
                    {
                        this.estado[3] = "imagenes/sema_rojoON.jpg";
                        this.titulosEstado[i] = 'Inactivo';
                    }

                    this.porcentajes[i] = app.zonas[i][3];
                }
            });
        },

        guardar: function()
        {
            if (this.porcentajes != '')
            {
                Swal.fire({
                    icon: 'question',
                    title: 'Seguro que quieres guardar?',
                    showDenyButton: true,
                    confirmButtonText: 'Guardar',
                    denyButtonText: 'Cancelar',
                    }).then((result) => {
                    if (result.isConfirmed) 
                    {
                        if (result.isConfirmed) {
                            var formData = new FormData();
    
                            var porcentajeAlumbrado = [];
                            

                            for(let i=0; i <= this.porcentajes.length-1; i++)
                            {
                                if (this.porcentajes[i] == undefined)
                                {
                                    this.porcentajes[i] = '';
                                }

                                porcentajeAlumbrado[i] = this.porcentajes[i];
                                formData.append('porcentajes[]', porcentajeAlumbrado[i]);   
                
                            }
                            
                            axios.post('vue/servicios_publicos/serv-serviciospublicos.php?action=guardarAlumbrado', formData)
                            .then((response) => {
                                // console.log(response.data);
                                if(response.data.insertaBien){
                                    Swal.fire({
                                        icon: 'success',
                                        title: 'Se ha guardado con exito',
                                        showConfirmButton: false,
                                        timer: 1500
                                      }).then((response) => {
                                            app.redireccionar();
                                        });
                                }else{
                                    
                                    Swal.fire(
                                        'Error!',
                                        'No se pudo guardar.',
                                        'error'
                                    );
                                }
                                
                            });
                            
                        }
                    } 
                    else if (result.isDenied) 
                    {
                        Swal.fire('Guardar cancelado', '', 'info')
                    }
                })   
            }
            else
            {
                Swal.fire({
                    icon: 'error',
                    title: 'No se puede guardar',
                    text: 'Todas los valores estan vacios'
                })
            }
        },

        redireccionar: function()
        {
            location.href = "serv-alumbrado.php";
        }
    }
});