var app = new Vue({
    el: '#myapp',
	data:{

        loading: false,
        numCierre: '',
        numBilletes: 0,
        numMonedas: 0,
        numConsignaciones: 0,
        numCheques: 0,
        totalConteo: 0,
        ingresos: [],
        error: '',
        totalRecaudo: '',
        totalCaja: '',
        totalBanco: '',
        totalRecaudoPuro: '',
        totalCajaPuro: '',
        totalBancoPuro: '',
        idcodigo: '',
    },

    mounted: function(){

        this.valoresIniciales();
    },

    computed: {
        
    },

    methods: 
    {
        valoresIniciales: function() {
            axios.post('vue/servicios_publicos/serv-visualizar-cierreDoce.php?cod='+this.idcodigo)
            .then((response) => {
                app.numCierre = response.data.consecutivo; 
                document.getElementById('fecha').value = response.data.fecha;
                document.getElementById('diaCierre').value = response.data.fechaCierre;
                app.numBilletes = response.data.numBilletes; 
                app.numMonedas = response.data.numMonedas; 
                app.numConsignaciones = response.data.numConsignaciones; 
                app.numCheques = response.data.numCheques; 
                app.totalConteo = response.data.totalConteo; 
                app.totalRecaudo = response.data.totalRecaudo; 
                app.totalCaja = response.data.totalCaja; 
                app.totalBanco = response.data.totalBanco; 
                app.ingresos = response.data.detalles;
                console.log(response.data.detalles);
            });
        },

        redireccionar: function() {
            location.href = "serv-cierreDiarioDOCE.php";
        },
    }
});