var app = new Vue({
    el: '#myapp',
	data:{

        datosGuardados: [],
        rangoConsumo: '',
        periodoFacturacion: '',
        intereses: '',
        desactivacionServicios: '',
        showIntereses: false,
        showDesactivacion: false,
        porcentajeIntereses: '',
        porcentajeAcuerdo: '',
        facturasVencidas: '',
    },

    mounted: function(){

        this.traerDatosGuardados();
    },

    methods: 
    {
        traerDatosGuardados: function()
        {
            axios.post('vue/servicios_publicos/serv-parametrosSP.php')
            .then((response) => {
                app.datosGuardados = response.data.parametrizacion;
                this.rangoConsumo = app.datosGuardados[0];
                this.periodoFacturacion = app.datosGuardados[1];
                this.intereses = app.datosGuardados[2];

                if (this.intereses == 'S')
                {
                    this.porcentajeIntereses = app.datosGuardados[3];
                    this.showIntereses = true;
                }

                this.desactivacionServicios = app.datosGuardados[4];

                if (this.desactivacionServicios == 'S')
                {
                    this.facturasVencidas = app.datosGuardados[5];
                    this.showDesactivacion = true;
                }

                this.porcentajeAcuerdo = app.datosGuardados[6];
            });
        },

        mostrarIntereses: function()
        {            
            if (this.intereses == 'S')
            {
                this.showIntereses = true;
            } 
            
            if (this.intereses == 'N')
            {
                this.showIntereses = false;
            }
        },        

        mostrarFacturas: function()
        {
            if (this.desactivacionServicios == 'S')
            {
                this.showDesactivacion = true;
            }

            if (this.desactivacionServicios == 'N')
            {
                this.showDesactivacion = false;
            }
        },

        guardar: function()
        {
            if (this.rangoConsumo != '')
            {
                if (this.periodoFacturacion != '')
                {
                    if (this.intereses != '')
                    {
                        if (this.porcentajeIntereses != '')
                        {
                            if (this.desactivacionServicios != '')
                            {
                                if (this.facturasVencidas > 0)
                                {
                                    Swal.fire({
                                        icon: 'question',
                                        title: 'Seguro que quieres guardar?',
                                        showDenyButton: true,
                                        confirmButtonText: 'Guardar',
                                        denyButtonText: 'Cancelar',
                                        }).then((result) => {
                                        if (result.isConfirmed) 
                                        {
                                            if (result.isConfirmed) 
                                            {
                                                var formData = new FormData();

                                                formData.append("rangoConsumo", this.rangoConsumo);
                                                formData.append("periodoFacturado", this.periodoFacturacion);
                                                formData.append("interesesMora", this.intereses);
                                                formData.append("porcentajeIntereses", this.porcentajeIntereses);
                                                formData.append("desactivacionAutomatica", this.desactivacionServicios);
                                                formData.append("facturasVencidas", this.facturasVencidas);
                                                formData.append("porcentajeAcuerdo", this.porcentajeAcuerdo);
                                                
                                                axios.post('vue/servicios_publicos/serv-parametrosSP.php?action=guardar', formData)
                                                .then((response) => {
                                            
                                                    if(response.data.insertaBien)
                                                    {
                                                        Swal.fire({
                                                            icon: 'success',
                                                            title: 'Se ha guardado con Exito',
                                                            showConfirmButton: false,
                                                            timer: 1500
                                                            }).then((response) => {
                                                                app.redireccionar();
                                                            });
                                                    }
                                                    else
                                                    {
                                                        
                                                        Swal.fire(
                                                            'Error!',
                                                            'No se pudo guardar.',
                                                            'error'
                                                        );
                                                    }
                                                    
                                                });
                                                
                                            }
                                        } 
                                        else if (result.isDenied) 
                                        {
                                            Swal.fire('Guardar cancelado', '', 'info')
                                        }
                                    })
                                }
                                else
                                {
                                    Swal.fire({
                                        icon: 'error',
                                        title: 'Error',
                                        text: 'El numero de facturas vencidas debe ser mayor a 0'
                                    })            
                                }
                            }
                            else
                            {
                                Swal.fire({
                                    icon: 'error',
                                    title: 'Error',
                                    text: 'No puede dejar la desactivación de servicios sin seleccionar'
                                })        
                            }
                        }
                        else
                        {
                            Swal.fire({
                                icon: 'error',
                                title: 'Error',
                                text: 'No puede dejar el porcentaje vacío'
                            })        
                        }
                        
                    }
                    else
                    {
                        Swal.fire({
                            icon: 'error',
                            title: 'Error',
                            text: 'No puede dejar el cobro de intereses sin seleccionar'
                        })        
                    }
                }
                else
                {
                    Swal.fire({
                        icon: 'error',
                        title: 'Error',
                        text: 'No puede dejar periodo facturado sin seleccionar'
                    })        
                }
            }
            else
            {
                Swal.fire({
                    icon: 'error',
                    title: 'Error',
                    text: 'No puede dejar el rango de consumo sin seleccionar'
                })        
            }
        },

        redireccionar: function()
        {
            location.href = "serv-parametrosSP.php";
        }
    }
});