var app = new Vue({
    el: '#myapp',
	data:{

        loading: false,
        numCierre: '',
        numBilletes: 0,
        numMonedas: 0,
        numConsignaciones: 0,
        numCheques: 0,
        totalConteo: 0,
        ingresos: [],
        error: '',
        totalRecaudo: '',
        totalCaja: '',
        totalBanco: '',
        totalRecaudoPuro: '',
        totalCajaPuro: '',
        totalBancoPuro: '',
    },

    mounted: function(){

        this.cargarFecha();
        this.valoresIniciales();
    },

    computed: {
        operacion: function() {
            this.totalConteo = parseInt(this.numBilletes) + parseInt(this.numMonedas) + parseInt(this.numConsignaciones) + parseInt(this.numCheques);
        },
    },

    methods: 
    {
        cargarFecha: function(){
            
            const fechaAct = new Date().toJSON().slice(0,10).replace(/-/g,'/');
            const fechaArr = fechaAct.split('/');
            const fechaV = fechaArr[2]+'/'+fechaArr[1]+'/'+fechaArr[0];
            document.getElementById('fecha').value = fechaV;
        },

        valoresIniciales: function()
        {
            axios.post('vue/servicios_publicos/serv-cierreDiarioDOCE.php')
            .then((response) => {
                app.numCierre = response.data.consecutivo; 
            });
        },

        traerRecaudos: async function() {

            var fecha = document.getElementById('fecha').value;
            var fechaCierre = document.getElementById('diaCierre').value;

            if (fecha != '' && fechaCierre != '') {

                this.loading = true;

                await axios.post('vue/servicios_publicos/serv-cierreDiarioDOCE.php?action=listadoRecaudos&diaCierre='+fechaCierre)
                .then((response) => {
                    //console.log(response.data);
                    app.ingresos = response.data.datosRecaudos;
                    app.totalRecaudo = response.data.valorIngresos;
                    app.totalCaja = response.data.valorCaja;
                    app.totalBanco = response.data.valorBanco;
                    app.totalRecaudoPuro = response.data.valorIngresosPuro;
                    app.totalCajaPuro = response.data.valorCajaPuro;
                    app.totalBancoPuro = response.data.valorBancoPuro;

                }).catch((error) => {
                    this.error = true;
                    console.log(error)
                    
                }).finally(() => {
                    this.loading = false;
                }); 
            }
            else {
                Swal.fire({
                    icon: 'warning',
                    title: 'Falta infomación',
                    text: 'Debes seleccionar las dos fechas'
                })    
            }
        },

        guardar: function() {
            if (this.numCierre != '' && this.ingresos != '') {
                Swal.fire({
                    icon: 'question',
                    title: 'Seguro que quieres guardar?',
                    showDenyButton: true,
                    confirmButtonText: 'Guardar',
                    denyButtonText: 'Cancelar',
                    }).then((result) => {
                    if (result.isConfirmed) 
                    {
                        //this.loading = true;

                        var formData = new FormData();
    
                        formData.append("numBilletes", this.numBilletes);
                        formData.append("numMonedas", this.numMonedas);
                        formData.append("numConsignaciones", this.numConsignaciones);
                        formData.append("numCheques", this.numCheques);
                        formData.append("totalConteo", this.totalConteo);
                        formData.append("totalRecaudo", this.totalRecaudoPuro);
                        formData.append("totalCaja", this.totalCajaPuro);
                        formData.append("totalBanco", this.totalBancoPuro);
                        formData.append("fecha", document.getElementById('fecha').value);
                        formData.append("fechaCierre", document.getElementById('diaCierre').value);

                        for(let i=0; i <= this.ingresos.length-1; i++) {
                            formData.append('consecutivos[]', this.ingresos[i][0]);
                            formData.append('detalle[]', this.ingresos[i][1]);
                            formData.append('valor[]', this.ingresos[i][5]);
                            formData.append('tipo[]', this.ingresos[i][3]);   
                            formData.append('formaPago[]', this.ingresos[i][4]);

                        }

                        axios.post('vue/servicios_publicos/serv-cierreDiarioDOCE.php?action=guardar', formData)
                        .then((response) => {
                            if(response.data.insertaBien){
                                this.loading = false;
                                Swal.fire({
                                    icon: 'success',
                                    title: 'Se ha guardado con exito',
                                    showConfirmButton: false,
                                    timer: 1500
                                }).then((response) => {
                                    app.redireccionar();
                                });
                            }
                            else {
                                Swal.fire(
                                    'Error!',
                                    'No se pudo guardar.',
                                    'error'
                                );
                            }
                        });
                    } 
                    else if (result.isDenied) 
                    {
                        Swal.fire('Guardar cancelado', '', 'info')
                    }
                })   
            }
            else {
                Swal.fire({
                    icon: 'warning',
                    title: 'No se puede guardar',
                    text: 'Campos vacios'
                })
            }
        },

        redireccionar: function()
        {
            location.href = "serv-cierreDiarioDOCE.php";
        },
    }
});