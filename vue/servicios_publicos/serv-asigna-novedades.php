<?php

use Illuminate\Support\Arr;

    require_once '../../comun.inc';
    require '../../funciones.inc';
    require '../../funcionesSP.inc.php';

    $linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");

    $out = array('error' => false);

    $action = "show";

    if(isset($_GET['action'])){
        $action = $_GET['action'];
    }

    if($action == 'show')
    {
        //Corte 
        $siguienteCorte = '';
        $siguienteCorte = siguienteCorte();
        
        if ($siguienteCorte < 10)
        {
            $siguienteCorte = '0'.$siguienteCorte;
        }
        
        $out['corte'] = $siguienteCorte;

        $rutas = array();

        $sqlRutas = "SELECT id, nombre FROM srvrutas WHERE estado = 'S'";
        $resRutas = mysqli_query($linkbd,$sqlRutas);
        while($rowRutas = mysqli_fetch_row($resRutas))
        {
            array_push($rutas, $rowRutas);
        }

        $out['rutas'] = $rutas;
    }

    if ($action == 'cargaNovedades') {
         
        $listadoNovedades = array();

        $sqlNovedades = "SELECT codigo_observacion, descripcion, afecta_facturacion FROM srv_tipo_observacion WHERE estado = 'S' ORDER BY codigo_observacion ASC";
        $resNovedades = mysqli_query($linkbd, $sqlNovedades);
        while ($rowNovedades = mysqli_fetch_row($resNovedades)) {
            
            array_push($listadoNovedades,$rowNovedades);
        }

        $out['listadoNovedades'] = $listadoNovedades;
    }

    if ($action == 'listausuarios') {

        if ($_GET['ruta'] == 0) {
            $filtro01 = "";
        } else {
            $filtro01 = "AND C.id_ruta = '$_GET[ruta]'";
        }

        $corte = $_GET['corte'];
        $corteAnt = $corte - 1;

        $datosUsuarios = array();

        $sqlUsuarios = "SELECT C.id, C.cod_usuario, DC.direccion, C.id_tercero FROM srvclientes AS C INNER JOIN srvdireccion_cliente AS DC ON C.id = DC.id_cliente WHERE C.estado = 'S' $filtro01 ORDER BY C.id_ruta ASC, C.codigo_ruta ASC";
        $resUsuarios = mysqli_query($linkbd, $sqlUsuarios);
        while ($rowUsuarios = mysqli_fetch_assoc($resUsuarios)) {
            
            $datos = array();

            $sqlTercero = "SELECT nombre1, nombre2, apellido1, apellido2, razonsocial FROM terceros WHERE id_tercero = $rowUsuarios[id_tercero]";
            $resTercero = mysqli_query($linkbd,$sqlTercero);
            $rowTercero = mysqli_fetch_assoc($resTercero);

            if($rowTercero['razonsocial'] == '') {
                $nombre = $rowTercero['nombre1']. ' ' .$rowTercero['nombre2']. ' ' .$rowTercero['apellido1']. ' ' .$rowTercero['apellido2'];
            } else {
                $nombre = $rowTercero['razonsocial'];
            }

            $nombre = Quitar_Espacios($nombre);

            //novedad anterior
            $sqlNovedadAnt = "SELECT codigo_observacion FROM srv_asigna_novedades WHERE id_cliente = '$rowUsuarios[id]' AND corte = $corteAnt AND estado = 'S'";
            $rowNovedadAnt = mysqli_fetch_assoc(mysqli_query($linkbd ,$sqlNovedadAnt));

            if ($rowNovedadAnt['codigo_observacion'] == '') {
                $codigoNovedadAnt = 0;
            } else {
                $codigoNovedadAnt = $rowNovedadAnt['codigo_observacion'];
            }

            $sqlNombreNovedadAnt = "SELECT descripcion FROM srv_tipo_observacion WHERE codigo_observacion = $codigoNovedadAnt";
            $rowNombreAnt = mysqli_fetch_assoc(mysqli_query($linkbd, $sqlNombreNovedadAnt));

            $novedadAnt = $codigoNovedadAnt." - ".$rowNombreAnt['descripcion'];

            //novedad actual
            $sqlNovedades = "SELECT codigo_observacion FROM srv_asigna_novedades WHERE id_cliente = '$rowUsuarios[id]' AND corte = $corte AND estado = 'S'";
            $rowNovedades = mysqli_fetch_assoc(mysqli_query($linkbd ,$sqlNovedades));

            if ($rowNovedades['codigo_observacion'] == '') {
                $codigoNovedad = 0;
            } else {
                $codigoNovedad = $rowNovedades['codigo_observacion'];
            }

            $sqlNombreNovedad = "SELECT descripcion FROM srv_tipo_observacion WHERE codigo_observacion = $codigoNovedad";
            $rowNombre = mysqli_fetch_assoc(mysqli_query($linkbd, $sqlNombreNovedad));

            $novedad = $codigoNovedad." - ".$rowNombre['descripcion'];

            array_push($datos, $rowUsuarios['cod_usuario']);
            array_push($datos, $nombre);
            array_push($datos, $rowUsuarios['direccion']);
            array_push($datos, $codigoNovedad);
            array_push($datos, $novedad);
            array_push($datos, $rowUsuarios['id']);
            array_push($datos, $novedadAnt);
            array_push($datosUsuarios, $datos);
        }

        $out['datosUsuarios'] = $datosUsuarios;
    }

    if ($action == 'guardar')
    {
        $corte = $_POST['corte'];

        $listadoClientes = implode(",", $_POST['clientes']);

        $sqlBorraAnteriores = "DELETE FROM srv_asigna_novedades WHERE corte = $corte AND  id_cliente IN ($listadoClientes)";
        mysqli_query($linkbd, $sqlBorraAnteriores);            

        foreach ($_POST['clientes'] as $x => $cliente) {
    
            $codigoNovedad = $_POST['codigoNovedad'][$x];

            $sqlNovedad = "SELECT afecta_facturacion FROM srv_tipo_observacion WHERE codigo_observacion = '$codigoNovedad'";
            $rowNovedad = mysqli_fetch_row(mysqli_query($linkbd, $sqlNovedad));

            if ($rowNovedad[0] = "S") {

                $sqlNovedadDet = "SELECT cambia_consumo, lectura_consumo, id_servicio FROM srv_observacion_det WHERE codigo_observacion = '$codigoNovedad' AND estado = 'S'";
                $resNovedadDet = mysqli_query($linkbd, $sqlNovedadDet);
                while ($rowNovedadDet = mysqli_fetch_row($resNovedadDet)) {

                    if ($rowNovedadDet[0] == 'S') {

                        $sqlValida = "SELECT * FROM srvlectura WHERE corte = '$corte' AND id_cliente = '$cliente' AND id_servicio = '$rowNovedadDet[2]'";
                        $resValida = mysqli_query($linkbd, $sqlValida);
                        $afecta = mysqli_num_rows($resValida);

                        if ($afecta > 0) {
                        
                            $updateConsumo = "UPDATE srvlectura SET consumo = '$rowNovedadDet[1]' WHERE corte = '$corte' AND id_cliente = '$cliente' AND id_servicio = '$rowNovedadDet[2]' ";
                            mysqli_query($linkbd, $updateConsumo);
                        }
                        else {
                            $insertConsumo = "INSERT INTO srvlectura (corte, id_cliente, id_servicio, lectura_medidor, consumo, estado) VALUES ('$corte', '$cliente', '$rowNovedadDet[2]', 0, '$rowNovedadDet[1]', 'S')";
                            mysqli_query($linkbd, $insertConsumo);
                        }
                        
                    }
                }
            }

            $sqlNovedades = "INSERT INTO srv_asigna_novedades (corte, id_cliente, codigo_observacion, estado) VALUES ($corte, $cliente, '$codigoNovedad', 'S')"; 
            mysqli_query($linkbd, $sqlNovedades);    
        }

        $usuario = $_SESSION["usuario"];
        $cedula = $_SESSION["cedulausu"];
        $ip = getRealIP();
        $hoy = date("Y-m-d H:i:s");

        $sqlAuditoria = "INSERT INTO srv_auditoria (id_srv_funciones, accion, cedula, usuario, ip, fecha_hora, consecutivo) VALUES (3, 'Crear', '$cedula', '$usuario', '$ip', '$hoy', '$corte')";
        mysqli_query($linkbd, $sqlAuditoria);

        $out['insertaBien'] = true;
    }

    header("Content-type: application/json");
    echo json_encode($out);
    die();