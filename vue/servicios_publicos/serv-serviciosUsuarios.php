<?php

    require_once '../../comun.inc';
    require '../../funciones.inc';
    require '../../funcionesSP.inc.php';

    $linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");

    $out = array('error' => false);

    $action = "show";

    if(isset($_GET['action'])){
        $action = $_GET['action'];
    }

    if($action == 'show')
    {
        $x = 0;
        $usuarios = array();
        $nombre = array();
        $nombreUsuario = '';
        
        $queryClientes = "SELECT id, id_tercero, cod_usuario, id_estrato FROM srvclientes WHERE estado = 'S'";
        $respClientes = mysqli_query($linkbd, $queryClientes);
        while ($rowClientes = mysqli_fetch_row($respClientes))
        {
            $datos = array();

            $nombreUsuario = encuentraNombreTerceroConIdCliente($rowClientes[0]);

            array_push($datos, $rowClientes[0]);
            array_push($datos, $nombreUsuario);
    
            array_push($usuarios, $datos);
            
            // array_push($usuarios, $nombre);
            // $direccion = encuentraDireccionConIdCliente($rowClientes['id']);
            // $estrato = buscarNombreEstrato($rowClientes['id_estrato']);
    
            // array_push($usuarios, $rowClientes[0]);
            // array_push($usuarios, $direccion);  
            // array_push($usuarios, $estrato);

            $x++;
        }

        //var_dump($usuarios);
        $out['usuarios22'] = $usuarios;
    }

    header("Content-type: application/json");
    echo json_encode($out);
    die();