var app = new Vue({
	el: '#myapp',
	data:{
		loading: false,
		showModal2: false,
		tipoMovimiento:'',
		numeroNota:'',
		centroCosto:'',
		concepto:'',
		docBanco:'',
		cuentaBancaria:'',
		cuentaBancariaH:'',
		nomBanco:'',
		numBanco:'',
		gastoBancario:'',
		valorNota:'',
		searchCuenta:'',
		valorListaNotas:'0',
		cuentaContable:'',
		selectTipoMovimiento:[],
		selectCentroCosto:[],
		selectGastoBancario:[],
		selectCuentas:[],
		selectListaNotas:[],
	},
	watch:{
		gastoBancario: function(newVal){
			let selectedOption = this.selectGastoBancario.find(option => option[3] === newVal);
			if (selectedOption) {
				this.cuentaContable = selectedOption[4];
			}
		}
	},
	mounted:
	function()
	{
		this.loading = false;
		this.cargarFechaActual();
		this.buscarNumComprobante();
		this.buscarTipoMovimiento();
		this.buscarCentroCosto().then(() => {
			this.buscarGastoBancario();
		});
	},
	methods:{
		buscarNumComprobante: async function(){
			await axios.post('vue/tesoreria/teso-notasbancarias_new.php?accion=buscaNC')
			.then(
				(response)=>{
					this.numeroNota = response.data.datosproceso;
				}
			).catch((error) => {
				this.error = true;
			});
			this.loading = false;
		},
		buscarTipoMovimiento: async function(){
			await axios.post('vue/tesoreria/teso-notasbancarias_new.php?accion=buscaTM')
			.then(
				(response)=>{
					this.selectTipoMovimiento = response.data.datosproceso;
					app.tipoMovimiento = app.selectTipoMovimiento[0][3];
				}
			).catch((error) => {
				this.error = true;
			});
			this.loading = false;
		},
		buscarCentroCosto: async function(){
			await axios.post('vue/tesoreria/teso-notasbancarias_new.php?accion=buscaCC')
			.then(
				(response)=>{
					this.selectCentroCosto = response.data.datosproceso;
					app.centroCosto = app.selectCentroCosto[0][0];
				}
			).catch((error) => {
				this.error = true
			});
			this.loading = false;
		},
		buscarGastoBancario: async function(){
			this.gastoBancario = '';
			let fecha = document.getElementById('fc_1198971545').value;
			let cc = this.centroCosto;
			var formData = new FormData();
			formData.append("fecha",fecha);
			formData.append("cc",cc);
			await axios.post('vue/tesoreria/teso-notasbancarias_new.php?accion=buscaGB', formData)
			.then(
				(response)=>{
					this.selectGastoBancario = response.data.datosproceso;
				}
			).catch((error) => {
				this.error = true
			});
			this.loading = false;
		},
		buscaCuentaVentana: async function(){
			var formData = new FormData();
			formData.append("cuenta",this.searchCuenta);
			await axios.post('vue/tesoreria/teso-notasbancarias_new.php?accion=buscaCU',formData)
			.then(
				(response)=>{
					this.selectCuentas = response.data.datosproceso;
				}
			).catch((error) => {
				this.error = true
			});
		},
		validarcuenta: async function(){
			var formData = new FormData();
			formData.append("cuenta",this.cuentaBancaria);
			await axios.post('vue/tesoreria/teso-notasbancarias_new.php?accion=vCuenta',formData)
			.then(
				(response)=>{
					if (response.data.datosproceso[0] != ''){
						this.cuentaBancariaH = response.data.datosproceso[0];
						this.nomBanco = response.data.datosproceso[2];
						this.numBanco = response.data.datosproceso[1];
					}else{
						Swal.fire({
							icon: 'error',
							title: 'Error!',
							text: 'Número de cuenta Bancaria equivocada',
							confirmButtonText: 'Continuar',
							confirmButtonColor: '#FF121A',
							timer: 2500
						});
						this.cuentaBancariaH = '';
						this.nomBanco = '';
						this.numBanco = '';
					}

				}
			).catch((error) => {
				this.error = true
			});
			this.loading = false;
		},
		toggleModal2: function(){
			this.showModal2 = !this.showModal2;
			if(this.showModal2 == true){
				this.buscaCuentaVentana();
			}
		},
		cargaInfoCuenta: function(documento, nombre, cuentaC, cuentaB){
			this.cuentaBancaria = cuentaB;
			this.cuentaBancariaH = cuentaC;
			this.nomBanco = nombre;
			this.numBanco = documento;
			this.showModal2 = !this.showModal2;
		},
		agregarListaNotas: function(){
			if((this.centroCosto != '') && (this.concepto != '') && (this.docBanco != '') && (this.cuentaBancaria != '')  && (this.nomBanco != '') && (this.cuentaBancariaH != '') && (this.numBanco != '') && (this.gastoBancario != '') && (this.valorNota != '')){
				let varAux = [this.centroCosto, '', this.docBanco, this.cuentaBancaria, this.nomBanco, this.cuentaBancariaH, this.numBanco, this.gastoBancario, this.valorNota, this.cuentaContable];
				this.valorListaNotas = parseFloat(this.valorListaNotas) + parseFloat(this.valorNota);
				this.selectListaNotas.push(varAux);
				this.gastoBancario = this.valorNota = '';
			}else{
				Swal.fire({
					icon: 'error',
					title: 'Error!',
					text: 'Se debe diligenciar todos los campos para agregar',
					confirmButtonText: 'Continuar',
					confirmButtonColor: '#FF121A',
					timer: 2500
				});
			}
		},
		cargarFechaActual: function(){
			let hoy = new Date();
			let fechaFormateada = hoy.toLocaleDateString("es-CO", {
				year: "numeric",
				month: "2-digit",
				day: "2-digit"
			});
			document.getElementById('fc_1198971545').value = fechaFormateada;
		},
		formatonumero: function(valor){
			return new Intl.NumberFormat("es-CO", {style: "currency", currency: "COP"}).format(valor);
		},
		eliminaListaNotas: function(index,valdescuento){
			Swal.fire({
				icon: 'question',
				title: '¿Esta seguro de eliminar esta nota?',
				showDenyButton: true,
				confirmButtonText: 'Guardar',
				confirmButtonColor: '#01CC42',
				denyButtonText: 'Cancelar',
				denyButtonColor: '#FF121A',
			}).then(
				(result) => {
					if (result.isConfirmed){
						this.valorListaNotas = parseFloat(this.valorListaNotas) - parseFloat(valdescuento);
						this.selectListaNotas.splice(index, 1);
					}
					else if (result.isDenied){
						Swal.fire({
							icon: 'info',
							title: 'No se alimino la nota',
							confirmButtonText: 'Continuar',
							confirmButtonColor: '#FF121A',
							timer: 2500
						});
					}
				}
			)
		},
		validarGuardar: function(){
			if(this.selectListaNotas.length > 0){
				Swal.fire({
					icon: 'question',
					title: '¿Esta seguro guardar la nota?',
					showDenyButton: true,
					confirmButtonText: 'Guardar',
					confirmButtonColor: '#01CC42',
					denyButtonText: 'Cancelar',
					denyButtonColor: '#FF121A',
				}).then(
					(result) => {
						if (result.isConfirmed){
							this.activaGuardar();
							this.dirigirNuevaVentana();
						}
						else if (result.isDenied){
							Swal.fire({
								icon: 'info',
								title: 'No se guardo la nota',
								confirmButtonText: 'Continuar',
								confirmButtonColor: '#FF121A',
								timer: 2500
							});
						}
					}
				)
			}else{
				Swal.fire({
					icon: 'error',
					title: 'Error!',
					text: 'Se debe agregar por lo menos una nota para guardar',
					confirmButtonText: 'Continuar',
					confirmButtonColor: '#FF121A',
					timer: 2500
				});
			}
		},
		activaGuardar: function(){
			let idnota =this.numeroNota;
			let fecha = document.getElementById('fc_1198971545').value;
			let concepto = this.concepto;
			let tipomov = this.tipoMovimiento;
			var formData = new FormData();
			formData.append("idnota",idnota);
			formData.append("fecha",fecha);
			formData.append("concepto",concepto);
			formData.append("tipomov",tipomov);
			Object.keys(this.selectListaNotas).forEach(e => {formData.append(`inflistanotas[${e}]`, this.selectListaNotas[e])});
			axios.post('vue/tesoreria/teso-notasbancarias_new.php?accion=guardar', formData)
			.then();
		},
		dirigirNuevaVentana: function(){
			Swal.fire({
				icon: 'success',
				title: 'Se ha guardo con Exito la Nota bancaria',
				confirmButtonText: 'Continuar',
				confirmButtonColor: '#FF121A',
				timer: 3000
			}).then((response) => {
				location.href = 'teso-notasbancarias_new.php';
			});
		},
	},
});
