<?php
	require_once '../../comun.inc';
	require '../../funciones.inc';
    require '../../funcionesSP.inc.php';
	$linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");

	if(isset($_GET['accion'])){
		$accion = $_GET['accion'];
		switch ($accion){
			case 'buscaNC':{
				$sql = "SELECT MAX(id_comp) FROM tesonotasbancarias_cab";
				$res = mysqli_query($linkbd, $sql);
				$row = mysqli_fetch_row($res);
				$datosproceso = $row[0] + 1;
				$out['datosproceso'] = $datosproceso;
			}break;
			case 'buscaTM':{
				$datosproceso = array();
				$sql = "SELECT id, codigo, descripcion FROM tipo_movdocumentos WHERE codigo = '01' AND modulo = '3'";
				$res = mysqli_query($linkbd, $sql);
				while($row = mysqli_fetch_row($res)){
					$datos = array();
					$idcod = $row[0].$row[1];
					array_push($datos, $row[0]);//id
					array_push($datos, $row[1]);//codigo
					array_push($datos, $row[2]);//descripcion
					array_push($datos, $idcod);//id + codigo
					array_push($datosproceso, $datos);
				}
				$out['datosproceso'] = $datosproceso;
			}break;
			case 'buscaCC':{
				$datosproceso = array();
				$sql = "SELECT id_cc, nombre FROM centrocosto WHERE estado = 'S' AND entidad = 'S'";
				$res = mysqli_query($linkbd, $sql);
				while($row = mysqli_fetch_row($res)){
					$datos = array();
					array_push($datos, $row[0]);//id
					array_push($datos, $row[1]);//nombre
					array_push($datosproceso, $datos);
				}
				$out['datosproceso'] = $datosproceso;
			}break;
			case 'buscaGB':{
				preg_match("/([0-9]{2})\/([0-9]{2})\/([0-9]{4})/", $_POST['fecha'],$fecha);
				$fechaf = "$fecha[3]-$fecha[2]-$fecha[1]";
				$cc = $_POST['cc'];
				$datosproceso = array();
				$sql = "SELECT codigo, nombre, tipo FROM tesogastosbancarios WHERE estado = 'S'";
				$res = mysqli_query($linkbd, $sql);
				while($row = mysqli_fetch_row($res)){
					$datos = array();
					$varaux = "$row[2] - $row[0]";

					$sqlcon = "SELECT concepto FROM tesogastosbancarios_det WHERE codigo = '$row[0]' AND vigencia = (SELECT MAX(vigencia) FROM tesogastosbancarios_det WHERE codigo = '$row[0]' AND tipoconce = 'GB' AND modulo = '4' AND estado = 'S') AND tipoconce = 'GB' AND modulo = '4' AND estado = 'S'";
					$rescon = mysqli_query($linkbd, $sqlcon);
					$rowcon = mysqli_fetch_row($rescon);
					$concepcontable = $rowcon[0];

					$sqlrcu = "SELECT DISTINCT cuenta FROM conceptoscontables_det WHERE modulo = '4' AND tipo = 'GB' AND cc = '$cc' AND tipocuenta = 'N' AND codigo = '$concepcontable' AND fechainicial = (SELECT MAX(fechainicial) FROM conceptoscontables_det WHERE fechainicial <= '$fechaf' AND modulo = '4' AND tipo = 'GB' AND cc = '$cc' AND tipocuenta = 'N' AND codigo = '$concepcontable')  ORDER BY credito";
					$respcu = mysqli_query($linkbd,$sqlrcu);
					$rowcu = mysqli_fetch_row($respcu);
					if($rowcu[0] != ''){
						$ctaconcepto = $rowcu[0];
					}else{
						$ctaconcepto = '';
					}

					array_push($datos, $row[0]);//codigo
					array_push($datos, $row[1]);//nombre
					array_push($datos, $row[2]);//tipo
					array_push($datos, $varaux);//tipo - codigo
					array_push($datos, $ctaconcepto);//cuenta
					array_push($datosproceso, $datos);
				}
				$out['datosproceso'] = $datosproceso;
			}break;
			case 'buscaCU':{
				$datosproceso = array();
				if($_POST['cuenta'] != ''){
					$crit1="AND TB3.nombre LIKE '%".$_POST['cuenta']."%'";
				}else{
					$crit1='';
				}
				$sql = "SELECT TB1.razonsocial, TB3.nombre, TB2.cuenta, TB2.ncuentaban, TB2.tipo, TB1.cedulanit
				FROM terceros AS TB1
				INNER JOIN tesobancosctas AS TB2 ON TB2.tercero = TB1.cedulanit
				INNER JOIN cuentasnicsp AS TB3 ON TB3.cuenta = TB2.cuenta
				WHERE TB2.estado='S' $crit1 ORDER BY TB3.cuenta, TB1.cedulanit";
				$res = mysqli_query($linkbd, $sql);
				while($row = mysqli_fetch_row($res)){
					$datos = array();
					array_push($datos, $row[0]);//razon social
					array_push($datos, $row[1]);//nombre cuenta
					array_push($datos, $row[2]);//cuenta contable
					array_push($datos, $row[3]);//cuenta banco
					array_push($datos, $row[4]);//tipo cuenta
					array_push($datos, $row[5]);//documento tercero
					array_push($datosproceso, $datos);
				}
				$out['datosproceso'] = $datosproceso;
			}break;
			case 'vCuenta':{
				$datosproceso = array();
				$ntercero = $codigo = $doctercero = '';
				$sql = "SELECT cuenta, tercero FROM tesobancosctas WHERE ncuentaban = '".$_POST['cuenta']."'";
				$res = mysqli_query($linkbd, $sql);
				$row = mysqli_fetch_row($res);
				$codigo = "$row[0]";
				$doctercero = "$row[1]";
				array_push($datosproceso, $codigo);//codigo
				array_push($datosproceso, $doctercero);//documento tercero
				if($row[1] != ''){
					$sqlt = "SELECT razonsocial, nombre1, nombre2, apellido1, apellido2 FROM terceros WHERE cedulanit = '$row[1]'";
					$rest = mysqli_query($linkbd, $sqlt);
					$rowt = mysqli_fetch_row($rest);
					if($rowt[0] != ''){
						$ntercero = $rowt[0];
					}elseif($rowt[1] != ''){
						$ntercero = $rowt[1];
						if($rowt[2] != ''){
							$ntercero = $ntercero.' '.$rowt[2];
						}
						$ntercero = $ntercero.' '.$rowt[3];
						if($rowt[4] != ''){
							$ntercero = $ntercero.' '.$rowt[4];
						}
					}
				}
				array_push($datosproceso, $ntercero);//nombre tercero
				$out['datosproceso'] = $datosproceso;
			}break;
			case 'guardar':{
				$idnota = $_POST['idnota'];

                $sql = "DELETE FROM pptonotasbanppto WHERE idrecibo = '$idnota'";
				mysqli_query($linkbd, $sql);

				$sql = "DELETE FROM tesonotasbancarias_cab WHERE id_comp = '$idnota'";
				mysqli_query($linkbd, $sql);

				$sql = "DELETE FROM tesonotasbancarias_det WHERE id_notabancab = '$idnota'";
				mysqli_query($linkbd, $sql);

				$sql = "DELETE FROM comprobante_cab WHERE numerotipo = '$idnota' AND tipo_comp = '9'";
				mysqli_query($linkbd, $sql);

				$sql = "DELETE FROM comprobante_det WHERE numerotipo = '$idnota' AND tipo_comp = '9'";
				mysqli_query($linkbd, $sql);

				preg_match("/([0-9]{2})\/([0-9]{2})\/([0-9]{4})/", $_POST['fecha'],$fecha);
				$fechaf = "$fecha[3]-$fecha[2]-$fecha[1]";
				$vigencia = $fecha[3];
				$concepto = $_POST['concepto'];
				$tipomov = $_POST['tipomov'];
				$user = $_SESSION['nickusu'];
				$listanotas = $_POST['inflistanotas'];

				$sql = "INSERT INTO tesonotasbancarias_cab (id_comp, fecha, vigencia, estado, concepto, tipo_mov, user) VALUES ('$idnota', '$fechaf', '$vigencia', 'S', '$concepto', '$tipomov','$user')";
				mysqli_query($linkbd, $sql);

				$sql = "INSERT INTO comprobante_cab (numerotipo, tipo_comp, fecha, concepto, total, total_debito, total_credito, diferencia, estado) VALUES ($idnota, 9, '$fechaf', '$concepto', 0, 0, 0, 0, '1')";
				mysqli_query($linkbd, $sql);

				for($x=0;$x < count($listanotas);$x++){
					$notaindividual = explode(",", $listanotas[$x]);
                    //Añadir nota bancaria a ptto
                    $notaBanco = str_replace(' ', '', $notaindividual[7]);
                    $arrNota = explode("-",$notaBanco);
                    $codigoNota = $arrNota[1];
                    $sql = "SELECT cuentapres,fuente,cuenta_clasificadora FROM tesogastosbancarios_det WHERE codigo = '$codigoNota' ORDER BY vigencia DESC";
                    $arrDataNota = mysqli_query($linkbd, $sql)->fetch_assoc();

                    if($arrDataNota['cuentapres'] !="" && $arrDataNota['fuente'] ){
                        $sql = "INSERT INTO pptonotasbanppto (cuenta,idrecibo,valor,vigencia,fuente,condigo,seccion_presupuestal,medio_pago,vigencia_gasto,cuenta_clasificadora) VALUES('$arrDataNota[cuentapres]','$idnota','$notaindividual[8]','$vigencia','$arrDataNota[fuente]','$codigoNota',
                        '16','CSF','1','$arrDataNota[cuenta_clasificadora]')";
                        mysqli_query($linkbd, $sql);
                    }

					$gasto_ban = substr($notaindividual[7], 4);
					$tipo_gasto = substr($notaindividual[7], 0, 1);

					$sql = "INSERT INTO  tesonotasbancarias_det (id_notabancab, docban, cc, ncuentaban, tercero, gastoban, valor, estado, 	tipo_mov) VALUES ('$idnota', '$notaindividual[2]', '$notaindividual[0]', '$notaindividual[3]', '$notaindividual[6]', '$gasto_ban', '$notaindividual[8]', 'S', '$tipomov')";
					mysqli_query($linkbd, $sql);

					if($tipo_gasto == 'G'){
						$credito1 = $debito2 = 0;
						$credito2 = $debito1 = $notaindividual[8];
					}else{
						$credito1 = $debito2 = $notaindividual[8];
						$credito2 = $debito1 = 0;
					}
					$sql = "INSERT INTO comprobante_det (id_comp, cuenta, tercero, centrocosto, detalle, cheque, valdebito, valcredito, estado, vigencia) VALUES ('9 $idnota', '$notaindividual[5]', '$notaindividual[6]', '$notaindividual[0]', 'Nota bancaria $notaindividual[7]', '', '$credito1', '$debito1', '1', '$vigencia')";
					mysqli_query($linkbd, $sql);

					$sql = "INSERT INTO comprobante_det (id_comp, cuenta, tercero, centrocosto, detalle, cheque, valdebito, valcredito, estado, vigencia) VALUES ('9 $idnota', '$notaindividual[9]', '$notaindividual[6]', '$notaindividual[0]', 'Nota bancaria $notaindividual[7]', '', '$credito2', '$debito2', '1', '$vigencia')";
					mysqli_query($linkbd, $sql);
				}
			}break;
			case 'vNotas':{
				$datosproceso = array();
				if($_POST['vnnotas'] != ''){
					$condi = "id_comp = '".$_POST['vnnotas']."'";
				}else{
					$condi = "fecha BETWEEN '".$_POST['fecha1']."' AND '".$_POST['fecha2']."'";
				}
				$sql = "SELECT id_comp, fecha, concepto, estado FROM tesonotasbancarias_cab WHERE $condi ORDER BY id_comp DESC";
				$res = mysqli_query($linkbd, $sql);
				while($row = mysqli_fetch_row($res)){
					$datos = array();
					$sqldet = "SELECT SUM(valor) FROM tesonotasbancarias_det WHERE 	id_notabancab = ' $row[0]'";
					$resdet = mysqli_query($linkbd, $sqldet);
					$rowdet = mysqli_fetch_row($resdet);
					$sutotal = $rowdet[0];
					if($_POST['id'] != ''){
						$numid = $_POST['id'];
					}else{
						$numid = '';
					}
					array_push($datos, $row[0]);//id notas
					array_push($datos, $row[1]);//fecha
					array_push($datos, $row[2]);//concepto
					array_push($datos, $sutotal);//valor
					array_push($datos, $row[3]);//estado
					array_push($datos, $numid);//id
					array_push($datosproceso, $datos);
				}
				$out['datosproceso'] = $datosproceso;
			}break;
			case 'anulaN':{
				$fecha_actual = date('Y-m-d');
				$user = $_SESSION['nickusu'];
				$sql = "UPDATE tesonotasbancarias_cab SET estado = 'N' WHERE id_comp = '".$_POST['id_nota']."'";
				if(!mysqli_query($linkbd, $sql)){
					$valida = 0;
				}else{
					$sql2 = "UPDATE comprobante_cab SET estado = '0' WHERE tipo_comp = '9' AND numerotipo = '".$_POST['id_nota']."'";
					mysqli_query($linkbd, $sql2);
					$sql2 = "INSERT INTO tesonotasbancarias_anu (id_comp, fecha, detalle, usuario, estado) VALUES ('".$_POST['id_nota']."', '$fecha_actual', '".$_POST['observacion']."', '$user', 'S')";
					mysqli_query($linkbd, $sql2);
					$valida = 1;
				}
				$out['datosproceso'] = $valida;
			}break;
			case 'inicioEditar':{
				$datosproceso = array();
				$sql = "SELECT fecha, estado, concepto, tipo_mov FROM tesonotasbancarias_cab WHERE id_comp = '".$_POST['idcomp']."'";
				$res = mysqli_query($linkbd, $sql);
				$row = mysqli_fetch_row($res);
				$fecha = date('d/m/Y',strtotime($row[0]));
				if($row[1] == 'S'){
					$estadov = "ACTIVA";
				}else{
					$estadov = "ANULADA";
				}
				array_push($datosproceso, $fecha);//fecha
				array_push($datosproceso, $estadov);//estado
				array_push($datosproceso, $row[2]);//concepto
				array_push($datosproceso, $row[3]);//tipo movimineto
				$out['datosproceso'] = $datosproceso;
			}break;
			case 'cargaDT':{
				preg_match("/([0-9]{2})\/([0-9]{2})\/([0-9]{4})/", $_POST['fecha'],$fecha);
				$fechaf = "$fecha[3]-$fecha[2]-$fecha[1]";
				$datosproceso = array();
				$sql = "SELECT id_notabandet, docban, cc, tercero, gastoban, valor, estado, tipo_mov, ncuentaban FROM tesonotasbancarias_det WHERE id_notabancab = '".$_POST['idcomp']."'";
				$res = mysqli_query($linkbd, $sql);
				while($row = mysqli_fetch_row($res)){
					$datos = array();
					$sql1 = "SELECT TB1.razonsocial, TB3.nombre, TB2.cuenta, TB2.tipo, TB1.cedulanit
					FROM terceros AS TB1
					INNER JOIN tesobancosctas AS TB2 ON TB2.tercero = TB1.cedulanit
					INNER JOIN cuentasnicsp AS TB3 ON TB3.cuenta = TB2.cuenta
					WHERE TB2.estado='S' AND TB2.ncuentaban = '$row[8]'";
					$res1 = mysqli_query($linkbd, $sql1);
					$row1 = mysqli_fetch_row($res1);
					$nombanco = $row1[0];
					$cuentacont = $row1[2];
					$nitbanco = $row1[4];

					$sql2 = "SELECT tipo FROM tesogastosbancarios WHERE codigo = '$row[4]'";
					$res2 = mysqli_query($linkbd, $sql2);
					$row2 = mysqli_fetch_row($res2);
					$varaux = "$row2[0] - $row[4]";

					$sqlcon = "SELECT concepto FROM tesogastosbancarios_det WHERE codigo = '$row[4]' AND vigencia = (SELECT MAX(vigencia) FROM tesogastosbancarios_det WHERE codigo = '$row[4]' AND tipoconce = 'GB' AND modulo = '4' AND estado = 'S') AND tipoconce = 'GB' AND modulo = '4' AND estado = 'S'";
					$rescon = mysqli_query($linkbd, $sqlcon);
					$rowcon = mysqli_fetch_row($rescon);
					$concepcontable = $rowcon[0];

					$sqlrcu = "SELECT DISTINCT cuenta FROM conceptoscontables_det WHERE modulo = '4' AND tipo = 'GB' AND cc = '$row[2]' AND tipocuenta = 'N' AND codigo = '$concepcontable' AND fechainicial = (SELECT MAX(fechainicial) FROM conceptoscontables_det WHERE fechainicial <= '$fechaf' AND modulo = '4' AND tipo = 'GB' AND cc = '$row[2]' AND tipocuenta = 'N' AND codigo = '$concepcontable')  ORDER BY credito";
					$respcu = mysqli_query($linkbd,$sqlrcu);
					$rowcu = mysqli_fetch_row($respcu);
					if($rowcu[0] != ''){
						$ctaconcepto = $rowcu[0];
					}else{
						$ctaconcepto = '';
					}

					array_push($datos, $row[2]);//centro de costo
					array_push($datos, '');//concepto
					array_push($datos, $row[1]);//documento banco
					array_push($datos, $row[8]);//numero cuenta
					array_push($datos, $nombanco);//nombre banco
					array_push($datos, $cuentacont);//cuenta contable banco
					array_push($datos, $nitbanco);//nit banco
					array_push($datos, $varaux);//tipo - codigo gasto bancario
					array_push($datos, $row[5]);//valor
					array_push($datos, $ctaconcepto);//cuanta contable gasto
					array_push($datos, $row[0]);//id detalle
					array_push($datos, $row[6]);//estado
					array_push($datos, $row[7]);//tipo movimiento
					array_push($datosproceso, $datos);
				}
				$out['datosproceso'] = $datosproceso;
			}break;
			case 'nbMyMr':{
				$datosproceso = array();
				$sql = "SELECT MAX(id_comp), MIN(id_comp) FROM tesonotasbancarias_cab";
				$res = mysqli_query($linkbd, $sql);
				$row = mysqli_fetch_row($res);
				array_push($datosproceso, $row[0]);
				array_push($datosproceso, $row[1]);
				$out['datosproceso'] = $datosproceso;
			}
		}
	}
	header("Content-type: application/json");
	echo json_encode($out);
	die();
?>

