var app = new Vue({ 
	el: '#myapp',
	data:{
		loading: false,
		infobasico1:[],
		searchCodigo: '',
		searchNombre: '',
		searchDirecc: '',
	},
	mounted:
	function(){
		this.loading = false;
	},
	methods:{
		buscarinformacion: async function(){
			this.loading = true;
			var formData = new FormData();
			formData.append('keywordCodigo', this.searchCodigo);
			formData.append('keywordNombre', this.searchNombre);
			formData.append('keywordDirecc', this.searchDirecc);
			let dirreporte;
			dirreporte = 'vue/tesoreria/teso-certificadoexistencia.php?accion=buscar';
			await axios.post(dirreporte, formData)
			.then(
				(response)=>{
					this.infobasico1 = response.data.datosproceso1;
				}
			).catch((error) => {
				this.error = true;
			});
			this.loading = false;
		},
		iniciabuscar: function(){
			this.loading = true;
			this.buscarinformacion();
		},
		formatonumero: function(valor){
			return new Intl.NumberFormat("en-US", {style: "currency", currency: "USD"}).format(valor);
		},
	},
});