var app = new Vue({ 
	el: '#myapp',
	data:{
		loading: false,
		showModal2: false,
		tipoMovimiento:'',
		numeroNota:'',
		centroCosto:'',
		concepto:'',
		docBanco:'',
		cuentaBancaria:'',
		cuentaBancariaH:'',
		nomBanco:'',
		numBanco:'',
		gastoBancario:'',
		valorNota:'',
		searchCuenta:'',
		valorListaNotas:'0',
		cuentaContable:'',
		estado:'',
		nbMayor:'',
		nbMenor:'',
		selectTipoMovimiento:[],
		selectCentroCosto:[],
		selectGastoBancario:[],
		selectCuentas:[],
		selectListaNotas:[],
	},
	watch:{
		gastoBancario: function(newVal){
			let selectedOption = this.selectGastoBancario.find(option => option[3] === newVal);
			if (selectedOption) {
				this.cuentaContable = selectedOption[4];
			}
		}
	},
	computed: {
		inputClass() {
			return this.estado === "ACTIVA" ? "neon_verde" : "neon_rojo";
		}
	},
	mounted: 
	function()
	{
		this.loading = false;
        this.buscarTipoMovimiento().then(() => {
            this.cargaInicial().then(() => {
				this.buscarCentroCosto().then(() => {
					this.buscarGastoBancario();
				});
			});
        });
	},
	methods:{
        cargaInicial: async function(){
            var formData = new FormData();
			formData.append("idcomp",this.numeroNota);
            await axios.post('vue/tesoreria/teso-notasbancarias_new.php?accion=inicioEditar', formData)
			.then(
				(response)=>{
					document.getElementById('fc_1198971545').value = response.data.datosproceso[0];
                    this.tipoMovimiento = response.data.datosproceso[3];
                    this.concepto = response.data.datosproceso[2];
					this.estado = response.data.datosproceso[1];
					this.cargaDetalles(response.data.datosproceso[0]);
					this.nbMayorMenor();
				}
			).catch((error) => {
				this.error = true
			});
        },
		cargaDetalles: async function(fecha){
			var formData = new FormData();
			formData.append("idcomp",this.numeroNota);
			formData.append("fecha",fecha);
			await axios.post('vue/tesoreria/teso-notasbancarias_new.php?accion=cargaDT', formData)
			.then(
				(response)=>{
					this.selectListaNotas =  response.data.datosproceso;
					let xx = response.data.datosproceso.length;
					for(yy = 0; yy < xx; yy++){
						this.valorListaNotas = parseFloat(this.valorListaNotas) + parseFloat(response.data.datosproceso[yy][8]);
					}
				}
			).catch((error) => {
				this.error = true
			});
		},
		buscarTipoMovimiento: async function(){
			await axios.post('vue/tesoreria/teso-notasbancarias_new.php?accion=buscaTM')
			.then(
				(response)=>{
					this.selectTipoMovimiento = response.data.datosproceso;
					//app.tipoMovimiento = app.selectTipoMovimiento[0][3];
				}
			).catch((error) => {
				this.error = true;
			});
			this.loading = false;
		},
		buscarCentroCosto: async function(){
			await axios.post('vue/tesoreria/teso-notasbancarias_new.php?accion=buscaCC')
			.then(
				(response)=>{
					this.selectCentroCosto = response.data.datosproceso;
					app.centroCosto = app.selectCentroCosto[0][0];
				}
			).catch((error) => {
				this.error = true
			});
			this.loading = false;
		},
		buscarGastoBancario: async function(){
			this.gastoBancario = '';
			let fecha = document.getElementById('fc_1198971545').value;
			let cc = this.centroCosto;
			var formData = new FormData();
			formData.append("fecha",fecha);
			formData.append("cc",cc);
			await axios.post('vue/tesoreria/teso-notasbancarias_new.php?accion=buscaGB', formData)
			.then(
				(response)=>{
					this.selectGastoBancario = response.data.datosproceso;
				}
			).catch((error) => {
				this.error = true
			});
			this.loading = false;
		},
		buscaCuentaVentana: async function(){
			var formData = new FormData();
			formData.append("cuenta",this.searchCuenta);
			await axios.post('vue/tesoreria/teso-notasbancarias_new.php?accion=buscaCU',formData)
			.then(
				(response)=>{
					this.selectCuentas = response.data.datosproceso;
				}
			).catch((error) => {
				this.error = true
			});
		},
		validarcuenta: async function(){
			var formData = new FormData();
			formData.append("cuenta",this.cuentaBancaria);
			await axios.post('vue/tesoreria/teso-notasbancarias_new.php?accion=vCuenta',formData)
			.then(
				(response)=>{
					if (response.data.datosproceso[0] != ''){
						this.cuentaBancariaH = response.data.datosproceso[0];
						this.nomBanco = response.data.datosproceso[2];
						this.numBanco = response.data.datosproceso[1];
					}else{
						Swal.fire({
							icon: 'error',
							title: 'Error!',
							text: 'Número de cuenta Bancaria equivocada',
							confirmButtonText: 'Continuar',
							confirmButtonColor: '#FF121A',
							timer: 2500
						});
						this.cuentaBancariaH = '';
						this.nomBanco = '';
						this.numBanco = '';
					}

				}
			).catch((error) => {
				this.error = true
			});
			this.loading = false;
		},
		toggleModal2: function(){
			this.showModal2 = !this.showModal2;
			if(this.showModal2 == true){
				this.buscaCuentaVentana();
			}
		},
		cargaInfoCuenta: function(documento, nombre, cuentaC, cuentaB){
			this.cuentaBancaria = cuentaB;
			this.cuentaBancariaH = cuentaC;
			this.nomBanco = nombre;
			this.numBanco = documento;
			this.showModal2 = !this.showModal2;
		},
		agregarListaNotas: function(){
			if((this.centroCosto != '') && (this.concepto != '') && (this.docBanco != '') && (this.cuentaBancaria != '')  && (this.nomBanco != '') && (this.cuentaBancariaH != '') && (this.numBanco != '') && (this.gastoBancario != '') && (this.valorNota != '')){
				let varAux = [this.centroCosto, '', this.docBanco, this.cuentaBancaria, this.nomBanco, this.cuentaBancariaH, this.numBanco, this.gastoBancario, this.valorNota, this.cuentaContable,'N','S','201'];
				this.valorListaNotas = parseFloat(this.valorListaNotas) + parseFloat(this.valorNota);
				this.selectListaNotas.push(varAux);
				this.gastoBancario = this.valorNota = '';
			}else{
				Swal.fire({
					icon: 'error',
					title: 'Error!',
					text: 'Se debe diligenciar todos los campos para agregar',
					confirmButtonText: 'Continuar',
					confirmButtonColor: '#FF121A',
					timer: 2500
				});
			}
		},
		formatonumero: function(valor){
			return new Intl.NumberFormat("es-CO", {style: "currency", currency: "COP"}).format(valor);
		},
		eliminaListaNotas: function(index,valdescuento){
			Swal.fire({
				icon: 'question',
				title: '¿Esta seguro de eliminar esta nota?',
				showDenyButton: true,
				confirmButtonText: 'Eliminar',
				confirmButtonColor: '#01CC42',
				denyButtonText: 'Cancelar',
				denyButtonColor: '#FF121A',
			}).then(
				(result) => {
					if (result.isConfirmed){
						this.valorListaNotas = parseFloat(this.valorListaNotas) - parseFloat(valdescuento);
						this.selectListaNotas.splice(index, 1);
					}
					else if (result.isDenied){
						Swal.fire({
							icon: 'info',
							title: 'No se alimino la nota',
							confirmButtonText: 'Continuar',
							confirmButtonColor: '#FF121A',
							timer: 2500
						});
					}
				}
			)
		},
		validarGuardar: function(){
			if(this.selectListaNotas.length > 0){
				let titulos = "";
				if(this.estado == "ACTIVA"){
					titulos = "¿Esta seguro de modificar la nota bancaria?";
				}else{
					titulos = "¿la nota bancaria esta anulada si guarda la modificación se activara de nuevo?";
				}
				Swal.fire({
					icon: 'question',
					title: titulos,
					showDenyButton: true,
					confirmButtonText: 'Guardar',
					confirmButtonColor: '#01CC42',
					denyButtonText: 'Cancelar',
					denyButtonColor: '#FF121A',
				}).then(
					(result) => {
						if (result.isConfirmed){
							this.activaGuardar();
							Swal.fire({
								icon: 'success',
								title: 'Se ha modifico con Exito la Nota bancaria',
								confirmButtonText: 'Continuar',
								confirmButtonColor: '#FF121A',
								timer: 3000
							})
						}
						else if (result.isDenied){
							Swal.fire({
								icon: 'info',
								title: 'No se modifico la nota bancaria',
								confirmButtonText: 'Continuar',
								confirmButtonColor: '#FF121A',
								timer: 2500
							});
						}
					}
				)
			}else{
				Swal.fire({
					icon: 'error',
					title: 'Error!',
					text: 'Se debe agregar por lo menos una nota bancaria para modificar',
					confirmButtonText: 'Continuar',
					confirmButtonColor: '#FF121A',
					timer: 2500
				});
			}
		},
		activaGuardar: function(){
			let idnota = this.numeroNota;
			let fecha = document.getElementById('fc_1198971545').value;
			let concepto = this.concepto;
			let tipomov = this.tipoMovimiento;
			var formData = new FormData();
			formData.append("idnota",idnota);
			formData.append("fecha",fecha);
			formData.append("concepto",concepto);
			formData.append("tipomov",tipomov);
			Object.keys(this.selectListaNotas).forEach(e => {formData.append(`inflistanotas[${e}]`, this.selectListaNotas[e])});
			axios.post('vue/tesoreria/teso-notasbancarias_new.php?accion=guardar', formData)
			.then();
		},
		nbMayorMenor: async function(){
			await axios.post('vue/tesoreria/teso-notasbancarias_new.php?accion=nbMyMr')
			.then(
				(response)=>{
					this.nbMayor = response.data.datosproceso[0];
					this.nbMenor = response.data.datosproceso[1];
				}
			).catch((error) => {
				this.error = true
			});
		},
		atrasc: function(){
			let id = this.numeroNota;
			let minim = this.nbMenor;
			id = parseFloat(id) - 1;
			if(id >= minim){
				let url = window.location.href;
				let params = url.split("?")[1].split("&");
				let fecha1 = params[1].split("=")[1];
				let fecha2 = params[2].split("=")[1];
				location.href = "teso-editanotasbancarias_new.php?id=" + id + "&fechab1=" + fecha1 + "&fechab2=" + fecha2;
			}
		},
		adelante: function(){
			let id = this.numeroNota;
			let maxim = this.nbMayor;
			id = parseFloat(id) + 1;
			if(id <= maxim){
				let url = window.location.href;
				let params = url.split("?")[1].split("&");
				let fecha1 = params[1].split("=")[1];
				let fecha2 = params[2].split("=")[1];
				location.href = "teso-editanotasbancarias_new.php?id=" + id + "&fechab1=" + fecha1 + "&fechab2=" + fecha2;
			}
		},
		flechaVerde: function(){
			let id = this.numeroNota;
			let url = window.location.href;
			let params = url.split("?")[1].split("&");
			let fecha1 = params[1].split("=")[1];
			let fecha2 = params[2].split("=")[1];
			location.href = "teso-buscanotasbancarias_new.php?id=" + id + "&fechab1=" + fecha1 + "&fechab2=" + fecha2;
		},
	},
});