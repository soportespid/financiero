var app = new Vue({ 
	el: '#myapp',
	data:{
		loading: false,
		tabgroup1:'1',
		vcodigo:'',
		vnombre:'',
		vcuenta:'',
		vncuenta:'',
		vtercero:'',
		vntercero:'',
		vcodigon:'',
		vnombren:'',
		vcuentan:'',
		vncuentan:'',
		infocuenta:'',
		infocuentas:[],
		infotercero:'',
		infoterceros:[],
		infocodigo:'',
		infocodigos:[],
		infocodigo1:'',
		infocodigos1:[],
		showModalCuentas: false,
		showModalTerceros: false,
		searchCuenta: {keywordCuenta: ''},
		searchTercero: {keywordTercero: ''},
		searchCodigo: {keywordCodigo: ''},
		searchCodigo1: {keywordCodigo1: ''},
		idmedio:'',
		idbuscar:'',
		idtipo:'',
	},
	mounted:
	function(){
		if(this.idmedio != ''){this.inicioeditar();}
		if(this.idbuscar == '1'){
			this.buscarCodigo();
			this.buscarCodigo1();
		}
	},
	methods:{
		inicioeditar: async function(){
			await axios.post('vue/tesoreria/teso-mediosdepagonew.php?accion=cargar&idmedio='+this.idmedio+'&idtipo='+this.idtipo)
			.then(
				(response)=>
				{
					if (response.data.infoinicial.length > 0){
						if(response.data.infoinicial[0][5] == 'O'){
							this.tabgroup1 = '1';
							this.vcodigo = response.data.infoinicial[0][0];
							this.vnombre = response.data.infoinicial[0][1];
							this.vcuenta = response.data.infoinicial[0][2];
							this.vtercero = response.data.infoinicial[0][3];
							this.verificacuenta();
							this.verificatercero();
						}else{
							this.tabgroup1 = '2';
							this.vcodigon = response.data.infoinicial[0][0];
							this.vnombren = response.data.infoinicial[0][1];
							this.vcuentan = response.data.infoinicial[0][2];
							this.vterceron = response.data.infoinicial[0][3];
							this.verificacuenta();
						}
					}else{
						Swal.fire(
							'Error!',
							'hay un error al cargar el medio de pago Nº '+this.idmedio,
							'error'
						);
					}
				}
			);
		},
		toggleModalCuentas: function(){
			this.showModalCuentas = !this.showModalCuentas;
			if(this.showModalCuentas == true){
				this.buscarCuentas();
			}
		},
		toggleModalTerceros: function(){
			this.showModalTerceros = !this.showModalTerceros;
			if(this.showModalTerceros == true){
				this.buscarTerceros();
			}
		},
		buscarCuentas: async function(){
			this.loading = true;
			let keywordCuenta = this.toFormData(this.searchCuenta);
			await axios.post('vue/tesoreria/teso-mediosdepagonew.php?accion=sel1', keywordCuenta)
			.then(
				(response)=>{
					this.infocuentas = response.data.infocuentas;
				}
			);
			this.loading = false;
		},
		buscarTerceros: async function(){
			this.loading = true;
			var keywordTercero = this.toFormData(this.searchTercero);
			await axios.post('vue/tesoreria/teso-mediosdepagonew.php?accion=sel2', keywordTercero)
			.then(
				(response)=>{
					this.infoterceros = response.data.infoterceros;
				}
			);
			this.loading = false;
		},
		buscarCodigo: async function(){
			this.loading = true;
			var keywordCodigo = this.toFormData(this.searchCodigo);
			await axios.post('vue/tesoreria/teso-mediosdepagonew.php?accion=buscar', keywordCodigo)
			.then(
				(response)=>
				{
					if (response.data.infocodigos.length > 0){
						this.infocodigos = response.data.infocodigos;
					}
				}
			);
		},
		buscarCodigo1: async function(){
			this.loading = true;
			var keywordCodigo1 = this.toFormData(this.searchCodigo1);
			await axios.post('vue/tesoreria/teso-mediosdepagonew.php?accion=buscar1', keywordCodigo1)
			.then(
				(response)=>
				{
					if (response.data.infocodigos.length > 0){
						this.infocodigos1 = response.data.infocodigos;
					}
				}
			);
		},
		ingresarCuenta: function(cuenta, ncuenta){
			if(this.tabgroup1 == '1'){
				this.vcuenta = cuenta;
				this.vncuenta = ncuenta;
				
			}else{
				this.vcuentan = cuenta;
				this.vncuentan = ncuenta;
			}
			this.showModalCuentas = false;
		},
		ingresarTercero: function(razonsocial, apellido1, apellido2, nombre1, nombre2, documento){			
			this.vtercero = documento;
			if(razonsocial != ''){
				this.vntercero = razonsocial;
			}else{
				this.vntercero = apellido1 + ' ' + apellido2 + ' ' + nombre1 + ' ' + nombre2;
			}
			this.showModalTerceros = false;
		},
		verificacuenta: async function(){
			this.loading = true;
			if(this.tabgroup1 == '1'){var keyword = this.vcuenta;}
			else{var keyword = this.vcuentan;}
			await axios.post('vue/tesoreria/teso-mediosdepagonew.php?accion=sel3&keywordCuenta='+keyword)
			.then(
				(response)=>{
					if (response.data.infocuentas.length > 0){
						if(this.tabgroup1 == '1'){this.vncuenta = response.data.infocuentas[0][1];}
						else{this.vncuentan = response.data.infocuentas[0][1];}
					}else{
						if(this.tabgroup1 == '1'){this.vncuenta = '';}
						else{this.vncuentan = '';}
						Swal.fire(
							'Error!',
							'La cuenta Nº ' + keyword + ' no exite o no es una cuenta Auxiliar',
							'error'
						);
					}
				}
			);
			this.loading = false;
		},
		verificatercero: async function(){
			this.loading = true;
			var keyword = this.vtercero;
			await axios.post('vue/tesoreria/teso-mediosdepagonew.php?accion=sel4&keywordTercero='+keyword)
			.then(
				(response)=>{
					if (response.data.infoterceros.length > 0){
						if(response.data.infoterceros[0][0] != ''){
							this.vntercero = response.data.infoterceros[0][0];
						}else{
							this.vntercero = response.data.infoterceros[0][1] + ' ' + response.data.infoterceros[0][2] + ' ' + response.data.infoterceros[0][3] + ' ' + response.data.infoterceros[0][4];
						}
					}else{
						this.vntercero='';
						Swal.fire(
							'Error!',
							'No exite un tercero con el numero de documento: '+ keyword,
							'error'
						);
					}
				}
			);
			this.loading = false;
		},
		validarguardar: function(){
			if(this.tabgroup1 == '1'){
				if(this.vcodigo != '' && this.vnombre != ''){
					if(this.vcuenta != '' && this.vncuenta != ''){
						if(this.vtercero != '' && this.vntercero != ''){
							this.preguntaguardar();
						}else{
							Swal.fire(
								'Error!',
								'Se debe ingresar la información del tercero',
								'error'
							);
						}
					}else{
						Swal.fire(
							'Error!',
							'Se debe ingresar la información de la cuenta',
							'error'
						);
					}
				}else{
					Swal.fire(
						'Error!',
						'Se debe ingresar la información del codigo y nombre',
						'error'
					);
				}
			}else{
				if(this.vcodigon != '' && this.vnombren != ''){
					if(this.vcuentan != '' && this.vncuentan != ''){
						this.preguntaguardar();
					}else{
						Swal.fire(
							'Error!',
							'Se debe ingresar la información de la cuenta',
							'error'
						);
					}
				}else{
					Swal.fire(
						'Error!',
						'Se debe ingresar la información del codigo y nombre',
						'error'
					);
				}
				
			}
		},
		validareditar: function(){
			if(this.tabgroup1 == '1'){
				if(this.vcodigo != '' && this.vnombre != ''){
					if(this.vcuenta != '' && this.vncuenta != ''){
						if(this.vtercero != '' && this.vntercero != ''){
							this.preguntaeditar();
						}else{
							Swal.fire(
								'Error!',
								'Se debe ingresar la información del tercero',
								'error'
							);
						}
					}else{
						Swal.fire(
							'Error!',
							'Se debe ingresar la información de la cuenta',
							'error'
						);
					}
				}else{
					Swal.fire(
						'Error!',
						'Se debe ingresar la información del codigo y nombre',
						'error'
					);
				}
			}else{
				if(this.vcodigon != '' && this.vnombren != ''){
					if(this.vcuentan != '' && this.vncuentan != ''){
						this.preguntaeditar();
					}else{
						Swal.fire(
							'Error!',
							'Se debe ingresar la información de la cuenta',
							'error'
						);
					}
				}else{
					Swal.fire(
						'Error!',
						'Se debe ingresar la información del codigo y nombre',
						'error'
					);
				}
				
			}
		},
		preguntaguardar: function(){
			Swal.fire({
				icon: 'question',
				title: 'Seguro que quieres guardar la información?',
				showDenyButton: true,
				confirmButtonText: 'Guardar',
				denyButtonText: 'Cancelar',
			}).then(
				(result) => {
					if (result.isConfirmed){this.guardarinfo();}
					else if (result.isDenied){
						Swal.fire('No se guardo la información', '', 'info');
					}
				}
			)
		},
		preguntaeditar: function(){
			Swal.fire({
				icon: 'question',
				title: 'Seguro que quieres modificar la información?',
				showDenyButton: true,
				confirmButtonText: 'Guardar',
				denyButtonText: 'Cancelar',
			}).then(
				(result) => {
					if (result.isConfirmed){this.editarinfo();}
					else if (result.isDenied){
						Swal.fire('No se almaceno la información', '', 'info');
					}
				}
			)
		},
		guardarinfo: async function(){
			this.loading = true;
			var formData = new FormData();
			if(this.tabgroup1 == '1'){
				formData.append("tipo",this.tabgroup1);
				formData.append("codigo",this.vcodigo);
				formData.append("nombre",this.vnombre);
				formData.append("cuenta",this.vcuenta);;
				formData.append("tercero",this.vtercero);
				var codigosv = this.vcodigo;
			}else{
				formData.append("tipo",this.tabgroup1);
				formData.append("codigo",this.vcodigon);
				formData.append("nombre",this.vnombren);
				formData.append("cuenta",this.vcuentan);
				formData.append("tercero",'');
				var codigosv = this.vcodigon;
			}
			await axios.post('vue/tesoreria/teso-mediosdepagonew.php?accion=guardar', formData)
			.then(
				(response) => {
					if(response.data.insertaBien){
						Swal.fire({
							icon: 'success',
							title: 'Se ha creado con exito el tipo de pago',
							showConfirmButton: false,
							timer: 3500
						}).then((response) => {
							this.moveraeditar(codigosv);
						});
					}else{
						Swal.fire(
							'Error!',
							'No se pudo crear el tipo de pago, favor verificar que el codigo no este duplicado.',
							'error'
						);
					}
					this.loading = false;
				}
			);
			this.loading = false;
		},
		editarinfo: async function(){
			this.loading = true;
			var formData = new FormData();
			if(this.tabgroup1 == '1'){
				formData.append("tipo",this.tabgroup1);
				formData.append("codigo",this.vcodigo);
				formData.append("nombre",this.vnombre);
				formData.append("cuenta",this.vcuenta);;
				formData.append("tercero",this.vtercero);
				var codigosv = this.vcodigo;
			}else{
				formData.append("tipo",this.tabgroup1);
				formData.append("codigo",this.vcodigon);
				formData.append("nombre",this.vnombren);
				formData.append("cuenta",this.vcuentan);
				formData.append("tercero",'');
				var codigosv = this.vcodigon;
			}
			await axios.post('vue/tesoreria/teso-mediosdepagonew.php?accion=editar', formData)
			.then(
				(response) => {console.log(response.data);
					if(response.data.insertaBien){
						Swal.fire({
							icon: 'success',
							title: 'Se modifico con exito el tipo de pago',
							showConfirmButton: false,
							timer: 3500
						});
					}else{
						Swal.fire(
							'Error!',
							'No se pudo modificar el tipo de pago',
							'error'
						);
					}
					this.loading = false;
				}
			);
			this.loading = false;
		},
		moveraeditar: function(codigo,tipo){
			location.href="teso-mediosdepagoneweditar.php?idr=" + codigo + "&tipo="+tipo;
		},
		toFormData: function(obj)
		{
			var form_data = new FormData();
			for(var key in obj)
			{
				form_data.append(key, obj[key]);
			}
			return form_data;
		},
		
		

		
		
		
	},
});