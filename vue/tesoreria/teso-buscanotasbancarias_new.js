var app = new Vue({ 
	el: '#myapp',
	data:{
		loading: false,
		numeroNota:'',
		numnota:'',
		selectNotas:[],
	},
	mounted: 
	function()
	{
		this.loading = false;
		this.cargaInicial();
		
	},
	methods:{
		cargaInicial: function(){
			let url = window.location.href;
			if (url.includes("?")){
				params = url.split("?")[1].split("&");
				id = params[0].split("=")[1];
				fecha1 = params[1].split("=")[1];
				fecha2 = params[2].split("=")[1];
			}else{
				params = '';
				id = '';
				fecha1 = '';
				fecha2 = '';
			}
			if(id != ''){
				this.numeroNota = id;
			}
			if(fecha1 != ''){
				document.getElementById('fc_1198971545').value = fecha1;
				document.getElementById('fc_1198971546').value = fecha2;
			}else{
				let fecha = new Date();
				let dia = fecha.getDate();
				if (dia < 10) {
					dia = "0" + dia;
				}
				let mes = fecha.getMonth() + 1;
				if (mes < 10) {
					mes = "0" + mes;
				}
				let anio = fecha.getFullYear();
				let fechaInicial = "01/" + mes + "/" + anio;
				let fechaActual = dia + "/" + mes + "/" + anio;
				document.getElementById('fc_1198971545').value = fechaInicial;
				document.getElementById('fc_1198971546').value = fechaActual;
				this.numnota = id;
			}
			this.validarBusqueda(); 
		},
		buscaNotasVentana: async function(fecha1,fecha2,nnota){
			this.loading = true;
			var formData = new FormData();
			formData.append('fecha1', fecha1);
			formData.append('fecha2', fecha2);
			formData.append('id', this.numeroNota);
			formData.append('vnnotas', nnota);
			await axios.post('vue/tesoreria/teso-notasbancarias_new.php?accion=vNotas',formData)
			.then(
				(response)=>{
					this.selectNotas = response.data.datosproceso;
				}
			).catch((error) => {
				this.error = true
			});
			this.loading = false;
		},
		validarBusqueda: function(){
			let fecha1 = document.getElementById('fc_1198971545').value;
			let fecha2 = document.getElementById('fc_1198971546').value;
			if(fecha1 != '' && fecha2 != ''){
				let divfecha1 = fecha1.split('/');
				let divfecha2 = fecha2.split('/');
				let f1 = new Date(divfecha1[2], divfecha1[1], divfecha1[0]);
				let f2 = new Date(divfecha2[2], divfecha2[1], divfecha2[0]);
				let fecha1b = divfecha1[2]+"-"+divfecha1[1]+"-"+divfecha1[0];
				let fecha2b = divfecha2[2]+"-"+divfecha2[1]+"-"+divfecha2[0];
				if(f1 <= f2){
					this.buscaNotasVentana(fecha1b,fecha2b,'');
				}
				else{
					Swal.fire(
						'Error!',
						'La Fecha Inicial no puede ser mayor a la Fecha Final',
						'error'
					);
				}
			}else if(this.numnota != ''){
				this.buscaNotasVentana('','',this.numnota);
			}else{
				Swal.fire(
					'Error!',
					'Se debe ingresar un numero de nota bancaria o una Fecha Inicial y una Fecha Final',
					'error'
				);
			}
		},
		formatonumero: function(valor){
			return new Intl.NumberFormat("es-CO", {style: "currency", currency: "COP"}).format(valor);
		},
		validaAnular: function(id){
			Swal.fire({
				title: 'ANULAR CDP:',
				input: 'text',
				inputLabel: '¿por que desea anular la nota bancaria?',
				width: 500,
				inputAttributes: {
					autocapitalize: 'off',
					step: 'any'
				},
				showCancelButton: true,
				confirmButtonColor: '#01CC42',
				cancelButtonColor: '#FF121A',
				confirmButtonText: 'Aceptar',
				cancelButtonText: 'Cancelar',
				showLoaderOnConfirm: true,
				preConfirm: (observacion) => {
					let digitos = observacion.length;
					if(digitos > 10){
						this.anularNota(observacion,id);
					}else {
						Swal.showValidationMessage(
						`La descripción debe contener por lo menos 10 caracteres`
						)
					}
					
				},
			});
		},
		anularNota: async function(observacion,id){
			this.loading = true;
			var formData = new FormData();
			formData.append('id_nota', id);
			formData.append('observacion', observacion);
			await axios.post('vue/tesoreria/teso-notasbancarias_new.php?accion=anulaN',formData)
			.then(
				(response)=>{
					let valproceso = response.data.datosproceso;
					if(valproceso == '1'){
						Swal.fire({
							icon: 'success',
							title: 'Se Anulo la nota bancaria N° '.id,
							confirmButtonText: 'Continuar',
							confirmButtonColor: '#FF121A',
							timer: 3000
						});
						this.validarBusqueda();
					}else{
						Swal.fire({
							icon: 'error',
							title: 'Error!',
							text: 'No se anulo la nota bancaria',
							confirmButtonText: 'Continuar',
							confirmButtonColor: '#FF121A',
							timer: 2500
						});
					}
					
				}
			).catch((error) => {
				this.error = true
			});
			this.loading = false;
		},
		notaAnulada: function(id){
			$descrip = "La nota bancaria N° " + id + " ya esta anulada";
			Swal.fire({
				icon: 'info',
				title: $descrip,
				confirmButtonText: 'Continuar',
				confirmButtonColor: '#FF121A',
				timer: 2500
			});
		},
		visualizarNotas: function(id)
		{
			let fecha1 = document.getElementById('fc_1198971545').value;
			let fecha2 = document.getElementById('fc_1198971546').value;
			location.href = "teso-editanotasbancarias_new.php?id=" + id + "&fechab1=" + fecha1 + "&fechab2=" + fecha2;
		},
	},
});