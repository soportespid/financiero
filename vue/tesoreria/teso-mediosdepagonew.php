<?php
	require_once '../../comun.inc';
	require '../../funciones.inc';
	$linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");
	if(isset($_GET['accion'])){
		$accion = $_GET['accion'];
		switch ($accion){
			case 'sel1':{
				$infocuentas = array();
				$keyword = $_POST['keywordCuenta'];
				$sql = "SELECT * FROM (SELECT cn1.cuenta, cn1.nombre, cn1.naturaleza, cn1.centrocosto, cn1.tercero, cn1.tipo, cn1.estado FROM cuentasnicsp AS cn1 INNER JOIN cuentasnicsp AS cn2 ON UCASE(cn2.tipo) = 'AUXILIAR' AND cn2.cuenta LIKE CONCAT( cn1.cuenta,  '%' ) WHERE UCASE(cn1.tipo) = 'MAYOR' AND cn1.cuenta like '$keyword%' GROUP BY cn1.cuenta UNION SELECT cuenta,nombre,naturaleza,centrocosto,tercero,tipo,estado FROM cuentasnicsp WHERE UCASE(tipo) = 'AUXILIAR' AND cuenta like '$keyword%') AS tabla ORDER BY 1";
				$res = mysqli_query($linkbd,$sql);
				while($row = mysqli_fetch_row($res)){
					array_push($infocuentas, $row);
				}
				$out['infocuentas'] = $infocuentas;
			}break;
			case 'sel2':{
				$infoterceros = array();
				$keyword = $_POST['keywordTercero'];
				if($keyword == ''){
					$crit1 = '';
				}else{
					$crit1 = "AND concat_ws(' ', nombre1,nombre2,apellido1,apellido2,razonsocial,cedulanit) LIKE '%$keyword%'";
				}
				$sql = "SELECT razonSocial, apellido1, apellido2, nombre1, nombre2, cedulanit FROM terceros WHERE estado='S' $crit1  ORDER BY razonSocial, apellido1, apellido2, nombre1, nombre2";
				$res = mysqli_query($linkbd,$sql);
				while($row = mysqli_fetch_row($res)){
					array_push($infoterceros, $row);
				}
				$out['infoterceros'] = $infoterceros;
			}break;
			case 'sel3':{
				$infocuentas = array();
				$keyword = $_GET['keywordCuenta'];
				$sql = "SELECT cuenta, nombre FROM cuentasnicsp WHERE UCASE(tipo) = 'AUXILIAR' AND estado = 'S' AND cuenta = '$keyword'";
				$res = mysqli_query($linkbd,$sql);
				while($row = mysqli_fetch_row($res)){
					array_push($infocuentas, $row);
				}
				$out['infocuentas'] = $infocuentas;
			}break;
			case 'sel4':{
				$infoterceros = array();
				$keyword = $_GET['keywordTercero'];
				$sql = "SELECT razonSocial, apellido1, apellido2, nombre1, nombre2, cedulanit FROM terceros WHERE cedulanit = '$keyword'";
				$res = mysqli_query($linkbd,$sql);
				while($row = mysqli_fetch_row($res)){
					array_push($infoterceros, $row);
				}
				$out['infoterceros'] = $infoterceros;
			}break;
			case 'cargar':{
				$infoinicial = array();
				if($_GET['idtipo'] == '1'){$sql = "SELECT * FROM tesomediodepago WHERE id = '".$_GET['idmedio']."'";}
				else {$sql = "SELECT * FROM tesomediodepagossf WHERE id = '".$_GET['idmedio']."'";}
				$res = mysqli_query($linkbd,$sql);
				while($row = mysqli_fetch_row($res)){
					array_push($infoinicial, $row);
				}
				$out['infoinicial'] = $infoinicial;
			}break;
			case 'guardar':{
				if($_POST['tipo'] == '1'){
					$tipoc = 'O';
					$sql = "INSERT INTO tesomediodepago (id, nombre, cuentacontable, tercero, estado, tipo) VALUES ('".$_POST['codigo']."', '".$_POST['nombre']."', '".$_POST['cuenta']."', '".$_POST['tercero']."', 'S', '$tipoc')";
				}else{
					$tipoc = 'N';
					$sql = "INSERT INTO tesomediodepagossf (id, nombre, cuentacontable, tercero, estado, tipo) VALUES ('".$_POST['codigo']."', '".$_POST['nombre']."', '".$_POST['cuenta']."', '".$_POST['tercero']."', 'S', '$tipoc')";
				}
				if (!mysqli_query($linkbd,$sql)) {$out['insertaBien'] = false;}
				else{$out['insertaBien'] = true;}
			}break;
			case 'editar':{
				if($_POST['tipo'] == '1'){
					$sql = "UPDATE tesomediodepago SET nombre = '".$_POST['nombre']."', cuentacontable = '".$_POST['cuenta']."', tercero = '".$_POST['tercero']."' WHERE id = '".$_POST['codigo']."'";
				}
				else{
					$sql = "UPDATE tesomediodepagossf SET nombre = '".$_POST['nombre']."', cuentacontable = '".$_POST['cuenta']."', tercero = '".$_POST['tercero']."' WHERE id = '".$_POST['codigo']."'";
				}
				if (!mysqli_query($linkbd,$sql)) {$out['insertaBien'] = false;}
				else{$out['insertaBien'] = true;}
			}break;
			case 'buscar':{
				$infocodigos = array();
				$keyword = $_POST['keywordCodigo'];
				$sql = "SELECT * FROM tesomediodepago WHERE nombre LIKE '%$keyword%' ORDER BY id DESC";
				$res = mysqli_query($linkbd,$sql);
				while($row = mysqli_fetch_row($res)){
					array_push($infocodigos, $row);
				}
				$out['infocodigos'] = $infocodigos;
			}break;
			case 'buscar1':{
				$infocodigos = array();
				$keyword = $_POST['keywordCodigo1'];
				$sql = "SELECT * FROM tesomediodepagossf WHERE nombre LIKE '%$keyword%' ORDER BY id DESC";
				$res = mysqli_query($linkbd,$sql);
				while($row = mysqli_fetch_row($res)){
					array_push($infocodigos, $row);
				}
				$out['infocodigos'] = $infocodigos;
			}break;
		}
	}

	header("Content-type: application/json");
	echo json_encode($out);
	die();
?>