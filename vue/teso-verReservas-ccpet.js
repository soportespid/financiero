var app = new Vue({
    el: '#myapp',
    data:{
        mostrarIngresos: [],
        acuerdo: '',
        concepto: '',
        vigencia: '',
        numero: '',
        valorTotal: '',
        idcodigo: '',
        fecha: '',
    },
    mounted:
        function(){
            this.verSuperavitCreado();
    },
    methods:{
        verSuperavitCreado: async function()
        {
            await axios.post('vue/presupuesto_ccp/teso-verReservas-ccpet.php?action=show&id='+this.idcodigo)
			.then(
				(response)=>
				{
					app.fecha = response.data.fecha;
                    app.vigencia = response.data.vigencia;
                    app.concepto = response.data.concepto;
                    app.acuerdo = response.data.acuerdo;
                    app.valorTotal = response.data.valorTotal;
                    app.mostrarIngresos = response.data.detalles;
				}
			);
        },
        
    },

    
});
