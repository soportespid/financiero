var app = new Vue({

    el: '#myapp',
    data: {
        mostrar_resultados_proyectos: false,
        proyectos: [],
        proyectos_det_indicador: [],
        sector: '',
        uri :'data:application/vnd.ms-excel;base64,',
        template:'<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--></head><body><table>{table}</table></body></html>',
        base64: function(s){ return window.btoa(unescape(encodeURIComponent(s))) },
        format: function(s, c) { return s.replace(/{(\w+)}/g, function(m, p) { return c[p]; }) }
    },

    mounted: function(){
        this.buscarProyectos();
    },

    methods: {

        tableToExcel: function(table, name){
            if (!table.nodeType) table = this.$refs.table
            var ctx = {worksheet: name || 'Worksheet', table: table.innerHTML}
            window.location.href = this.uri + this.base64(this.format(this.template, ctx))
        },

        buscarProyectos: async function(){
            await axios.post('vue/ccp-reportegastosinversionprogramatico.php')
            .then(function(response){
                app.proyectos = response.data.proyectos;
                //console.log(response.data);
            });
            this.proyectosDetalle();
        },

        proyectosDetalle: async function(){
            var formData = new FormData();

            for(i = 0; i < this.proyectos.length; i++){
                formData.append("proyectos[]", this.proyectos[i]);
            }
            //console.log(this.proyectos);
            await axios.post('vue/ccp-reportegastosinversionprogramatico.php?action=proyectoProgramatico', formData)
            .then(function(response){
                console.log(response.data);
                app.proyectos_det_indicador = response.data.proyectos_det_indicador;
                app.mostrar_resultados_proyectos = true;
                
            });
            console.log(this.proyectos_det_indicador);
        }

    },
});