var app = new Vue({
    el: '#myapp',
    data: {
        codigosDet: [], 
        gastos: [],
        codigos_cont:[],
        codigos_cuentap:[],
        mostrar_resultados_gastos: false,
        showModal_fuentes: false,
        showModal_fuentes_nomina: false,
        searchFuente: {keywordFuente: ''},
        selected: '01',
        cuentaActual: '',
        codigo_p: '',
        tipoCuentaContable: '',
        tipoCuenta: '',
        fecha: '',
        valorIngresoCuenta: [],
        valorIngresoCuentaNom: [],
        fuentes: [],
        tipoCuentaPresupuestal: [],
        codigo_p_nom: '',
        tipoCuentaContable_nom: '',
        valorNomina: '',
        valorGeneral: '',
    },
    mounted: function(){
        this.buscarGastos();
    },
    methods: {

        buscarNombre: async function(codigo_buscar){
            await axios.post('vue/ccp-bienestransportables.php?action=buscaNombre', 
            JSON.stringify({
                name: codigo_buscar
                })
            )
            .then((response)  => {
                this.codigo_buscar_nombre = response.data.nombreCodigo;
            });
            ;
            return this.codigo_buscar_nombre;
        },

        searchMonitorFuente: function() {
            var keywordFuente = app.toFormData(app.searchFuente);
            
            axios.post('vue/cont-programacion-contable-ccpet.php?action=searchFuente&tipoCuenta='+ this.tipoCuenta, keywordFuente)
                .then(function(response){
                    console.log(response.data);
                    app.codigosDet = response.data.codigos;
                    
                    if(response.data.codigosDet == ''){
                        
                        app.noMember = true;
                        // app.show_table_search = false
                    }
                    else{
                        app.noMember = false;
                        //app.show_table_search = true
                    }
                    
                });
                
                
        },

        toFormData: function(obj){
            var form_data = new FormData();
            for(var key in obj){
                form_data.append(key, obj[key]);
            }
            return form_data;
        },
        
        fetchMembersBienes: function(){
            axios.post('vue/ccp-bienestransportables.php')
                .then(function(response){
                    app.secciones = response.data.secciones;
                });
        },

        fetchMembersServicios: function(){
            axios.post('vue/ccp-servicios.php')
                .then(function(response){
                    app.seccionesServicios = response.data.secciones;
                });
        },

        fetchMembersUnidadEjecutora: function(){
            axios.post('vue/ccp-unidadejecutora.php')
                .then(function(response){
                    app.unidadesejecutoras = response.data.unidades;
                    
                });
        },
        fetchMembersFuente: function(){
            axios.post('vue/cont-fuentes.php')
                .then(function(response){
                    app.fuentes = response.data.codigos;
                    
                });
        },
        buscarGastos: async function(){
            await axios.post('vue/cont-programacion-contable-ccpet.php?vigencia='+this.vigencia + '&unidadEjecutora='+this.selected + '&medioPago='+this.selectMedioPago)
                .then((response) => {
                    app.gastos = response.data.gastos;
                    app.valorIngresoCuenta = response.data.valorPorCuenta;
                    app.valorIngresoCuentaNom = response.data.valorPorCuentaNom;

                    if(app.gastos == ''){
                        app.mostrar_resultados_gastos = false;
                    }else{
                        app.mostrar_resultados_gastos = true;
                        this.fetchMembersUnidadEjecutora();
                    }
                });
          },
          
        agregarPresupuesto: async function(cuenta, nomina){
            
            this.tipoCuenta = cuenta;
            this.valorNomina = nomina;
 
            if(this.vigencia != 0 && this.vigencia != ''){
                await axios.post('vue/cont-programacion-contable-ccpet.php?action=buscarClasificadores&cuentaIngreso=' + cuenta)
                    .then(function(response){
                        app.codigosDet = response.data.codigo;

                        if(response.data.ingresosBuscados == ''){
                        }
                        else{
            
                        }                    
                    });
                    this.buscarFuente();
            }else{
                this.buscarFuente();
            }
        },


        agregarPresupuestoNom: async function(cuenta, general){
            
            this.tipoCuenta = cuenta;
            this.valorGeneral = general;
            
            if(this.vigencia != 0 && this.vigencia != ''){
                await axios.post('vue/cont-programacion-contable-ccpet.php?action=buscarClasificadoresNom&cuentaIngreso=' + cuenta)
                    .then(function(response){
                        app.codigosDet = response.data.codigo;
                        

                        if(response.data.ingresosBuscados == ''){
                        }
                        else{
            
                        }                    
                    });
                    this.buscarFuenteNom();
            }else{
                this.buscarFuenteNom();
            }
        },

        buscarFuente: function(){
            this.showModal_fuentes = true;
        },

        buscarFuenteNom: function(){
            this.showModal_fuentes_nomina = true;
        },
        
        guardarTotal: function()
        {
            if(this.showModal_fuentes)
            {
                return;
            }

            var formData = new FormData();

            var conceptoContable = [];

            for(i=0; i < Object.keys(this.valorIngresoCuenta).length ; i++){
                this.codigos_cuentap[i] = this.valorIngresoCuenta[Object.keys(this.valorIngresoCuenta)[i]];

                conceptoContable[i] = [Object.keys(this.valorIngresoCuenta)[i], this.valorIngresoCuenta[Object.keys(this.valorIngresoCuenta)[i]], this.gastos[i][2]];
                
                formData.append('conceptoContable[]', conceptoContable[i]);
            }

            
            
            axios.post('vue/cont-programacion-contable-ccpet.php?action=guardarTotal', formData)
                .then(function(response){
                    
                    for(var i = 0; i < response.data.insertaBien.length; i += 2)
                    {
                        if(response.data.insertaBien[i] == "401 INVALID DATA VALUE ERROR")
                        {
                            alert(response.data.insertaBien[i + 1]);
                        }
                    }

                    


                    app.codigosDet = response.data.codigos;
                    
                    if(response.data.codigosDet == ''){
                        app.buscarGastos();
                    }
                });
        },
        
        guardar: function(){
            if(this.codigo_p != null){
                var formData = new FormData();

                formData.append("cuentaPresupuestal",this.tipoCuenta);
                formData.append("conceptoContable", this.codigo_p);
                formData.append("tipo", this.tipoCuentaContable);
                formData.append("nomina", this.valorNomina);
                axios.post('vue/cont-programacion-contable-ccpet.php?action=guardarValorSolo',
                        formData
                    )
                    .then(function(response){
                        
                        if(response.data.insertaBien[0] == "200 OK"){
                            app.showModal_fuentes = false;
                            app.buscarGastos();
                        }
                });

                this.codigo_p = null;
            }
        },

        guardarNomina: function(){
            if(this.codigo_p_nom != null){
                var formData = new FormData();

                formData.append("cuentaPresupuestal",this.tipoCuenta);
                formData.append("conceptoContable", this.codigo_p_nom);
                formData.append("tipo", this.tipoCuentaContable_nom);
                formData.append("general", this.valorGeneral);
                axios.post('vue/cont-programacion-contable-ccpet.php?action=guardarValorSoloNom',
                        formData
                    )
                    .then(function(response){
                        
                        if(response.data.insertaBien[0] == "200 OK"){
                            app.showModal_fuentes_nomina = false;
                            app.buscarGastos();
                        }
                });

                this.codigo_p_nom = null;
            }
        },

        seleccionarCuenta: function(cuenta, cuenta2){
            this.codigo_p = cuenta;
            this.tipoCuentaContable = cuenta2;
        },

        seleccionarCuentaNom: function(cuenta, cuenta2){
            
            this.codigo_p_nom = cuenta;
            this.tipoCuentaContable_nom = cuenta2;
        },

        cerrarModalCuenta: function(){
            this.showModal_fuentes = false;
            this.codigo_p = null;
        },

        cerrarModalCuentaNom: function(){
            this.showModal_fuentes_nomina = false;
            this.codigo_p_nom = null;
        },
    },
});