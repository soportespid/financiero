<?php
	require_once '../../../../comun.inc';
	require '../../../../funciones.inc';
	require '../../../../funcionesSP.inc.php';
	session_start();
	if($_POST){
		$base = isset($_POST['base']) ?  base64_decode($_POST['base']) : null;
		$usuario = isset($_POST['usuario']) ? base64_decode($_POST['usuario']) : null;
		$obj = new Plantilla($base, $usuario);
		if($_POST['action']=="get"){
			$obj->getData();
		}else if($_POST['action']=="change"){
			$obj->changeData();
		}
	}
	class Plantilla{
		private $linkbd;
		public function __construct($base = null, $usuario = null) {
			if (!empty($base) && !empty($usuario)) {
				$this->linkbd = conectar_Multi($base, $usuario);
			} else {
				$this->linkbd = conectar_v7();
			}
			$this->linkbd->set_charset("utf8");
		}
		public function getData(){
			if(!empty($_SESSION)){
				echo json_encode($this->selectData(),JSON_UNESCAPED_UNICODE);
			}
			die();
		}
		public function selectData() {
			$sql = "SELECT codigo, nombre, descripcion, dias, tdias, adjunto, clasificacion, estado FROM plantiporadicacion ORDER BY codigo ASC";
			$request = mysqli_fetch_all(mysqli_query($this->linkbd, $sql), MYSQLI_ASSOC);
			$total = count($request);
			$tiposPQR = array( 
				"N" => "N - Ninguno",
				"P" => "P - Petición",
				"Q" => "Q - Queja",
				"R" => "R - Reclamo",
				"S" => "S - Sugerencia",
				"D" => "D - Denuncia",
				"F" => "F - Felicitación"
			);
			for ($i = 0; $i < $total; $i++) {
				$request[$i]['is_status'] = $request[$i]['estado'] == "S" ? 1 : 0;
				$request[$i]['is_yellow'] = $request[$i]['codigo'] == $_POST['id_gia'] ? 1 : 0;
				$request[$i]['valAdjunto'] = $request[$i]['adjunto'] == "S" ? 'SI' : 'NO';
				$request[$i]['tipoDias'] = $request[$i]['tdias'] == "H" ? 'HABILES' : 'CALENDARIO';
				$request[$i]['tipoPQR'] = isset($tiposPQR[$request[$i]['clasificacion']]) ? $tiposPQR[$request[$i]['clasificacion']] : $request[$i]['clasificacion'];
			}
			return $request;
		}
		public function changeData(){
			if(!empty($_SESSION)){
				$id = intval($_POST['id']);
				$estado = $_POST['estado'];
				$sql = "UPDATE plantiporadicacion SET estado = '$estado' WHERE codigo = $id";
				$request = mysqli_query($this->linkbd,$sql);
				if($request == 1){
					$arrResponse = array("status"=>true);
				}else{
					$arrResponse = array("status"=>false,"msg"=>"Error");
				}
				echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
			}
			die();
		}
	}
?>