<?php
	require_once '../../../../comun.inc';
	require '../../../../funciones.inc';
	require '../../../../funcionesSP.inc.php';
	session_start();
	if($_POST){
		$base = isset($_POST['base']) ?  base64_decode($_POST['base']) : null;
		$usuario = isset($_POST['usuario']) ? base64_decode($_POST['usuario']) : null;
		$obj = new Plantilla($base, $usuario);
		switch ($_POST['action']){
			case 'getData':
				$obj->getData();
			break;
			case 'save':
				$obj->save();
			break;
			
		}
	}
	class Plantilla{
		private $linkbd;
		private $arrData;
		private $intId;
		public function __construct($base = null, $usuario = null) {
			if (!empty($base) && !empty($usuario)) {
				$this->linkbd = conectar_Multi($base, $usuario);
			} else {
				$this->linkbd = conectar_v7();
			}
			$this->linkbd->set_charset("utf8");
		}
		public function getData(){
			if(!empty($_SESSION)){
				$id = strClean($_POST['codigo']);
				$request = $this->selectData($id);
				if(!empty($request['data'])){
					$arrData = array(
						"consecutivo"=>$this->selectConsecutivo(),
						"status"=>true,
						"data"=>$request
					);
				}else{
					$arrData = array("status"=>false,"consecutivo"=>$this->selectConsecutivo()-1);
				}
				echo json_encode($arrData,JSON_UNESCAPED_UNICODE);
			}
			die();
		}
		public function selectData($id){
			$this->intId = $id;
			$sql = "SELECT * FROM plantiporadicacion ORDER BY codigo ASC";
			$request = mysqli_fetch_all(mysqli_query($this->linkbd,$sql),MYSQLI_ASSOC);
			$data = array_values(array_filter($request,function($e){return $e['codigo'] == $this->intId;}))[0];
			return array("consecutivos"=>$request,"data"=>$data);
		}
		public function selectConsecutivo(){
			$sql = "SELECT MAX(codigo) AS max FROM plantiporadicacion WHERE codigo";
			$request = mysqli_query($this->linkbd,$sql)->fetch_assoc()['max']+1;
			return $request;
		}
		public function save(){
			if(!empty($_SESSION)){
				if($_POST){
					if(empty($_POST['codigo']) || empty($_POST['nombre'])){
						$arrResponse = array("status"=>false,"msg"=>"Error de datos");
					}else{
						$varCodigo = intval($_POST['codigo']);
						$varNombre = replaceChar(strClean($_POST['nombre']));
						$varTipopqr = $_POST['tipopqr'];
						$varTiporespuesta = $_POST['tiporespuesta'];
						$varTiempo = $_POST['tiempo'];
						$varDescripcion = replaceChar(strClean($_POST['descripcion']));
						$varTipocalendario = $_POST['tipocalendario'];
						$varAdjuntos = $_POST['adjuntos'];
						$request = $this->updateData(array("codigo"=>$varCodigo,"nombre"=>$varNombre, "tipopqr"=>$varTipopqr, "tiporespuesta"=>$varTiporespuesta, "tiempo"=>$varTiempo, "descripcion"=>$varDescripcion, "tipocalendario"=>$varTipocalendario, "adjuntos"=>$varAdjuntos));
						if(is_numeric($request) && $request> 0 ){
							$arrResponse = array("status"=>true,"msg"=>"Datos guardados.");
						}else if($request =="existe"){
							$arrResponse = array("status"=>false,"msg"=>"El nombre del ya existe, pruebe con otro.");
						}else{
							$arrResponse = array("status"=>false,"msg"=>"Ha ocurrido un error, intente de nuevo.");
						}
					}
				}
				echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
			}
			die();
		}
		public function updateData(array $data){
			$this->arrData = $data;
			$sql = "SELECT * FROM plantiporadicacion WHERE nombre = '{$this->arrData['nombre']}' AND codigo != {$this->arrData['codigo']}";
			$request = mysqli_fetch_all(mysqli_query($this->linkbd,$sql),MYSQLI_ASSOC);
			if(empty($request)){
				$sql = "UPDATE plantiporadicacion SET nombre = '{$this->arrData['nombre']}', descripcion = '{$this->arrData['descripcion']}', dias = '{$this->arrData['tiempo']}', tdias = '{$this->arrData['tipocalendario']}', slectura = '{$this->arrData['tiporespuesta']}', adjunto = '{$this->arrData['adjuntos']}', clasificacion = '{$this->arrData['tipopqr']}' WHERE codigo = {$this->arrData['codigo']}";
				$request = intval(mysqli_query($this->linkbd,$sql));
			}else{
				$request="existe";
			}
			return $request;
		}
	}
?>
