const URL ='vue/parametrizacion/tipos_radicacion/editar/para-tiposRadicacionEditar.php';
var app = new Vue({
	el: '#myapp',
	data() {
		return {
			isLoading: false,
			txtConsecutivo: '',
			arrConsecutivos:[],
			selectTipopqr: 'N',
			txtNombre: '',
			selectRespuesta: 'N',
			txtTiempo: 0,
			txtdescripcion: '',
			selecTipcal: 'N',
			selectReadjunto: 'S',
		};
	},
	mounted(){
		this.getData();
	},
	methods: {
		getData: async function(){
			let codigo = new URLSearchParams(window.location.search).get('id');
			const formData = new FormData();
			formData.append("action","getData");
			formData.append("codigo",codigo);
			this.isLoading = true;
			const response = await fetch(URL,{method:"POST",body:formData});
			const objData = await response.json();
			if(objData.status){
				const data = objData.data.data;
				this.txtConsecutivo = data.codigo;
				this.selectTipopqr = data.clasificacion;
				this.txtNombre = data.nombre;
				this.selectRespuesta = data.slectura;
				this.txtTiempo = data.dias;
				this.txtdescripcion = data.descripcion;
				this.selecTipcal = data.tdias;
				this.selectReadjunto = data.adjunto;
				this.arrConsecutivos = objData.data.consecutivos;
			}else{
				codigo = objData.consecutivo;
				window.location.href="para-tiposRadicacionEditar.php?id="+codigo
			}
			this.isLoading = false;
		},
		save:function(){
			const vueContext = this;
			if(this.txtConsecutivo == "" || this.txtNombre == '' || this.txtTiempo == '' || this.txtdescripcion == '' || (this.txtTiempo > 0 && this.selecTipcal == 'N')){
				
				Swal.fire("Error","Todos los campos con (*) son obligatorios","error");
				return false;
			}
			Swal.fire({
				title:"¿Estás segur@ de guardar?",
				text:"",
				icon: 'warning',
				showCancelButton:true,
				confirmButtonColor: '#3085d6',
				cancelButtonColor: '#d33',
				confirmButtonText:"Sí, guardar",
				cancelButtonText:"No, cancelar"
			}).then(async function(result){
				if(result.isConfirmed){
					const formData = new FormData();
					formData.append("action","save");
					formData.append("codigo",vueContext.txtConsecutivo);
					formData.append("nombre",vueContext.txtNombre);
					formData.append("tipopqr",vueContext.selectTipopqr);
					formData.append("tiporespuesta",vueContext.selectRespuesta);
					formData.append("tiempo",vueContext.txtTiempo);
					formData.append("descripcion",vueContext.txtdescripcion);
					formData.append("tipocalendario",vueContext.selecTipcal);
					formData.append("adjuntos",vueContext.selectReadjunto);
					vueContext.isLoading = true;
					const response = await fetch(URL,{method:"POST",body:formData});
					const objData = await response.json();
					if(objData.status){
						Swal.fire("Guardado",objData.msg,"success");
					}else{
						Swal.fire("Error",objData.msg,"error");
					}
					vueContext.isLoading = false;
				}
			});
		},
		editItem:function(type){
			let vueContext = this;
			let id = this.txtConsecutivo;
			let index = this.arrConsecutivos.findIndex(function(e){return e.codigo == id});
			if(type=="next" && vueContext.arrConsecutivos[++index]){
				id = this.arrConsecutivos[index++].codigo;
			}else if(type=="prev" && vueContext.arrConsecutivos[--index]){
				id = this.arrConsecutivos[index--].codigo;
			}
			window.location.href="para-tiposRadicacionEditar?id="+id;
		},
		iratras:function(type){
			let id = this.txtConsecutivo;
			window.location.href="para-tiposRadicacionBuscar?id="+id;
		},
	}
});
