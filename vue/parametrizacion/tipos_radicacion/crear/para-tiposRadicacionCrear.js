const URL ='vue/parametrizacion/tipos_radicacion/crear/para-tiposRadicacionCrear.php';
var app = new Vue({
	el: '#myapp',
	data() {
		return {
			isLoading: false,
			txtConsecutivo: '',
			selectTipopqr: 'N',
			txtNombre: '',
			selectRespuesta: 'N',
			txtTiempo: 0,
			txtdescripcion: '',
			selecTipcal: 'N',
			selectReadjunto: 'S',
		};
	},
	mounted(){
		this.getData();
	},
	methods: {
		getData: async function(){
			const formData = new FormData();
			formData.append("action","getData");
			this.isLoading = true;
			const response = await fetch(URL,{method:"POST",body:formData});
			const objData = await response.json();
			this.txtConsecutivo = objData.consecutivo;
			this.isLoading = false;
		},
		save:function(){
			const vueContext = this;
			if(this.txtConsecutivo == "" || this.txtNombre == '' || this.txtTiempo == '' || this.txtdescripcion == '' || (this.txtTiempo > 0 && this.selecTipcal == 'N')){
				Swal.fire("Error","Todos los campos con (*) son obligatorios","error");
				return false;
			}
			Swal.fire({
				title:"¿Estás segur@ de guardar?",
				text:"",
				icon: 'warning',
				showCancelButton:true,
				confirmButtonColor: '#3085d6',
				cancelButtonColor: '#d33',
				confirmButtonText:"Sí, guardar",
				cancelButtonText:"No, cancelar"
			}).then(async function(result){
				if(result.isConfirmed){
					const formData = new FormData();
					formData.append("action","save");
					formData.append("codigo",vueContext.txtConsecutivo);
					formData.append("nombre",vueContext.txtNombre);
					formData.append("tipopqr",vueContext.selectTipopqr);
					formData.append("tiporespuesta",vueContext.selectRespuesta);
					formData.append("tiempo",vueContext.txtTiempo);
					formData.append("descripcion",vueContext.txtdescripcion);
					formData.append("tipocalendario",vueContext.selecTipcal);
					formData.append("adjuntos",vueContext.selectReadjunto);
					vueContext.isLoading = true;
					const response = await fetch(URL,{method:"POST",body:formData});
					const objData = await response.json();
					if(objData.status){
						Swal.fire({
							title: "Guardado",
							text: objData.msg,
							icon: "success",
							confirmButtonText: "OK"
						}).then((result) => {
							if (result.isConfirmed) {
								const id = vueContext.txtConsecutivo;
								window.location.href = 'para-tiposRadicacionEditar.php?id=' + id;
							}
						});
						setTimeout(() => {
							const id = vueContext.txtConsecutivo;
							window.location.href = 'para-tiposRadicacionEditar.php?id=' + id;
						}, 2000);
					}else{
						Swal.fire("Error",objData.msg,"error");
					}
					vueContext.isLoading = false;
				}
			});
		}
	}
});