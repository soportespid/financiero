<?php
	require_once '../../../../comun.inc';
	require '../../../../funciones.inc';
	require '../../../../funcionesSP.inc.php';
	session_start();
	if($_POST){
		$base = isset($_POST['base']) ?  base64_decode($_POST['base']) : null;
		$usuario = isset($_POST['usuario']) ? base64_decode($_POST['usuario']) : null;
		$obj = new Plantilla($base, $usuario);
		switch ($_POST['action']){
			case 'getData':
				$obj->getData();
			break;
			case 'save':
				$obj->save();
			break;
			
		}
	}
	class Plantilla{
		private $linkbd;
		public function __construct($base = null, $usuario = null) {
			if (!empty($base) && !empty($usuario)) {
				$this->linkbd = conectar_Multi($base, $usuario);
			} else {
				$this->linkbd = conectar_v7();
			}
			$this->linkbd->set_charset("utf8");
		}
		public function getData(){
			if(!empty($_SESSION)){
				$arrData = array(
					"consecutivo"=>$this->selectConsecutivo(),
				);
				echo json_encode($arrData,JSON_UNESCAPED_UNICODE);
			}
			die();
		}
		public function selectConsecutivo(){
			$sql = "SELECT MAX(codigo) AS max FROM plantiporadicacion";
			$request = mysqli_query($this->linkbd,$sql)->fetch_assoc()['max']+1;
			return $request;
		}
		public function save(){
			if(!empty($_SESSION)){
				$varCodigo = $_POST['codigo'];
				$varNombre = replaceChar(strClean($_POST['nombre']));
				$varTipopqr = $_POST['tipopqr'];
				$varTiporespuesta = $_POST['tiporespuesta'];
				$varTiempo = $_POST['tiempo'];
				$varDescripcion = replaceChar(strClean($_POST['descripcion']));
				$varTipocalendario = $_POST['tipocalendario'];
				$varAdjuntos = $_POST['adjuntos'];
				$request = $this->insertData(array("codigo"=>$varCodigo,"nombre"=>$varNombre, "tipopqr"=>$varTipopqr, "tiporespuesta"=>$varTiporespuesta, "tiempo"=>$varTiempo, "descripcion"=>$varDescripcion, "tipocalendario"=>$varTipocalendario, "adjuntos"=>$varAdjuntos));
				if(is_numeric($request) && $request> 0 ){
					$arrResponse = array("status"=>true,"msg"=>"Datos guardados.");
				}else if($request =="existe"){
					$arrResponse = array("status"=>false,"msg"=>"El nombre del ya existe, pruebe con otro.");
				}else{
					$arrResponse = array("status"=>false,"msg"=>"Ha ocurrido un error, intente de nuevo.");
				}
				echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
			}
			die();
		}
		public function insertData(array $data){
			$this->arrData = $data;
			$request = mysqli_fetch_all(mysqli_query($this->linkbd,"SELECT * FROM plantiporadicacion WHERE nombre = '{$this->arrData['nombre']}'"),MYSQLI_ASSOC);
			if(empty($request)){
				$sql = "INSERT INTO plantiporadicacion(codigo, nombre, descripcion, dias, tdias, slectura, adjunto, estado, radotar,  clasificacion) VALUES (
					'{$this->arrData['codigo']}',
					'{$this->arrData['nombre']}',
					'{$this->arrData['descripcion']}',
					'{$this->arrData['tiempo']}',
					'{$this->arrData['tipocalendario']}',
					'{$this->arrData['tiporespuesta']}',
					'{$this->arrData['adjuntos']}',
					'S',
					'RA',
					'{$this->arrData['tipopqr']}'
				)";
				$request = intval(mysqli_query($this->linkbd,$sql));
			}else{
				$request = "existe";
			}
			return $request;
		}
	}
?>