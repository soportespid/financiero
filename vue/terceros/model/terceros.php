<?php
    require_once '../../../Librerias/core/Helpers.php';
    require_once '../../../Librerias/core/Mysql.php';

    header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=utf8");
    session_start();

    class terceros extends Mysql {
        
        public function get_data_create(){
            if(!empty($_SESSION)){       
                $arrResponse = [];

                $sql_personas = "SELECT id_persona as personaId, nombre FROM personas WHERE estado = '1' ORDER BY id_persona ASC";
                $personas = $this->select_all($sql_personas);

                $sql_regimenes = "SELECT id_regimen as regimenId, nombre FROM regimen WHERE estado = '1' AND id_regimen > 0 ORDER BY id_regimen ASC";
                $regimenes = $this->select_all($sql_regimenes);

                $sql_tipo_documento = "SELECT doc.id_tipodocid as documentoId,
                doc.nombre,
                tipo.persona as personaId 
                FROM docindentidad as doc INNER JOIN documentopersona as tipo ON doc.id_tipodocid = tipo.tipodoc
                WHERE doc.estado = '1' ORDER BY doc.id_tipodocid ASC";
                $tipoDocumentos = $this->select_all($sql_tipo_documento);

                $sql_departamentos = "SELECT danedpto as departamentoId, nombredpto as nombre FROM danedpto ORDER BY nombredpto ASC";
                $departamentos = $this->select_all($sql_departamentos);

                $sql_municipios = "SELECT danedpto as departamentoId, danemnpio as municipioId, nom_mnpio as nombre FROM danemnpio ORDER BY danemnpio ASC";
                $municipios = $this->select_all($sql_municipios);

                $sql_bancos = "SELECT codigo, nombre FROM hum_bancosfun WHERE estado = 'S' ORDER BY codigo ASC";
                $bancos = $this->select_all($sql_bancos);

                $arrResponse = ["status" => true, "personas" => $personas, "regimenes" => $regimenes, "tipoDocumentos" => $tipoDocumentos, "departamentos" => $departamentos, "municipios" => $municipios, "bancos" => $bancos];
            }
            echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
            die();
        }

        public function validate_document() {
            if(!empty($_SESSION)) {
                $arrResponse = [];
                $documento = $_POST["documento"];

                $sql_tercero = "SELECT * FROM terceros WHERE cedulanit = '$documento'";
                $documento = $this->select($sql_tercero);
    
                if (empty($documento)) {
                    $arrResponse = ["status" => true];
                } else {
                    $arrResponse = ["status" => false, "msg" => "El documento digitado ya existe"];
                }
            }

            echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
            die();
        }

        public function save_create() {
            if (!empty($_SESSION)) {
                $arrResponse = [];
                $persona = $_POST["persona"];
                $regimen = $_POST["regimen"];
                $tipoDocumento = $_POST["tipoDocumento"];
                $documento = $_POST["documento"];
                $digitoVerificacion = $_POST["digitoVerificacion"];
                $departamento = $_POST["departamento"];
                $municipio = $_POST["municipio"];
                $datos = json_decode($_POST['datos'], true);
                $bancos = json_decode($_POST['bancos'], true);
                $consecutivo = searchConsec("terceros", "id_tercero");
        
                if ($tipoDocumento == 31) {
                    $sql_tercero = "INSERT INTO terceros (id_tercero, razonsocial, direccion, telefono, celular, email, web, tipodoc, cedulanit, codver, depto, mnpio, persona, regimen, contribuyente, proveedor, empleado, estado) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";

                    $values_tercero = [
                        $consecutivo,
                        strClean(replaceChar(strtoupper($datos["razonsocial"]))),
                        strClean(replaceChar(strtoupper($datos["direccion"]))),
                        $datos["telefono"],
                        $datos["celular"],
                        $datos["email"],
                        $datos["paginaWeb"],
                        $tipoDocumento,
                        $documento,
                        $digitoVerificacion,
                        "$departamento",
                        "$municipio",
                        $persona,
                        $regimen,
                        (int) $datos["contribuyente"],
                        (int) $datos["proveedor"],
                        (int) $datos["empleado"],
                        "S"
                    ];
                    
                    $request = $this->insert($sql_tercero, $values_tercero);
                } else {
                    $sql_tercero = "INSERT INTO terceros (id_tercero, nombre1, nombre2, apellido1, apellido2, direccion, telefono, celular, email, web, tipodoc, cedulanit, depto, mnpio, persona, regimen, contribuyente, proveedor, empleado, estado) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";

                    $values_tercero = [
                        $consecutivo,
                        strClean(replaceChar(strtoupper($datos["primerNombre"]))),
                        strClean(replaceChar(strtoupper($datos["segundoNombre"]))),
                        strClean(replaceChar(strtoupper($datos["primerApellido"]))),
                        strClean(replaceChar(strtoupper($datos["segundoApellido"]))),
                        strClean(replaceChar(strtoupper($datos["direccion"]))),
                        $datos["telefono"],
                        $datos["celular"],
                        $datos["email"],
                        $datos["paginaWeb"],
                        $tipoDocumento,
                        $documento,
                        "$departamento",
                        "$municipio",
                        $persona,
                        $regimen,
                        (int) $datos["contribuyente"],
                        (int) $datos["proveedor"],
                        (int) $datos["empleado"],
                        "S"
                    ];
                    $request = $this->insert($sql_tercero, $values_tercero);
                }

                if ($request > 0) {
                    if (!empty($bancos)) {
                        foreach ($bancos as $banco) {
                            $sql_bancos = "INSERT INTO teso_cuentas_terceros (id_tercero, tipo_cuenta, codigo_banco, numero_cuenta) VALUES (?, ?, ?, ?)";
    
                            $values_banco = [
                                $consecutivo,
                                $banco["tipoCuenta"],
                                $banco["codigoBanco"],
                                $banco["numeroCuenta"]
                            ];

                            $this->insert($sql_bancos, $values_banco);
                        }
                    }

                    $sql_funcion = "SELECT id FROM gen_funciones WHERE nombre = 'Terceros'";
                    $funcion = $this->select($sql_funcion);
                    insertAuditoria("gen_auditoria","gen_funciones_id",1,"Crear",$consecutivo);

                    $arrResponse = ["status" => true];
                }
                else {
                    $arrResponse = ["status" => false, "msg" => "Error en guardado, contacte a soporte"];
                }
            }
            echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
            die();
        }

        public function get_data_search() {
            if (!empty($_SESSION)) {
                $sql_terceros = "SELECT UPPER(CONCAT(nombre1, ' ', nombre2, ' ', apellido1, ' ', apellido2)) as nombre, UPPER(razonsocial) as razonsocial, cedulanit, id_tercero, estado FROM terceros ORDER BY id_tercero DESC";
                $terceros = $this->select_all($sql_terceros);

                foreach ($terceros as $key => $tercero) {
                    $terceros[$key]["nombre"] = preg_replace("/\s+/", " ", trim($tercero["nombre"]));
                }

                $arrResponse = ["status" => true, "terceros" => $terceros];
            }
            echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
            die();
        }

        public function get_data_edit() {
            if (!empty($_SESSION)) {
                $arrResponse = $datos = [];
                $terceroId = $_POST["id"];

                $sql_regimenes = "SELECT id_regimen as regimenId, nombre FROM regimen WHERE estado = '1' AND id_regimen > 0 ORDER BY id_regimen ASC";
                $regimenes = $this->select_all($sql_regimenes); 

                $sql_departamentos = "SELECT danedpto as departamentoId, nombredpto as nombre FROM danedpto ORDER BY nombredpto ASC";
                $departamentos = $this->select_all($sql_departamentos);

                $sql_municipios = "SELECT danedpto as departamentoId, danemnpio as municipioId, nom_mnpio as nombre FROM danemnpio ORDER BY danemnpio ASC";
                $municipios = $this->select_all($sql_municipios);

                $sql_bancos = "SELECT codigo, nombre FROM hum_bancosfun WHERE estado = 'S' ORDER BY codigo ASC";
                $bancos = $this->select_all($sql_bancos);

                $sql_tercero = "SELECT nombre1, nombre2, apellido1, apellido2, razonsocial, direccion, telefono, celular, email, web, tipodoc, cedulanit, codver, depto, mnpio, persona, regimen, contribuyente, proveedor, empleado FROM terceros WHERE id_tercero = $terceroId";
                $tercero = $this->select($sql_tercero);

                $datos = [
                    "primerNombre" => $tercero["nombre1"],
                    "segundoNombre" => $tercero["nombre2"],
                    "primerApellido" => $tercero["apellido1"],
                    "segundoApellido" => $tercero["apellido2"],
                    "razonsocial" => $tercero["razonsocial"],
                    "direccion" => $tercero["direccion"],
                    "telefono" => $tercero["telefono"],
                    "celular" => $tercero["celular"],
                    "email" => $tercero["email"],
                    "paginaWeb" => $tercero["web"],
                    "contribuyente" => (int) $tercero["contribuyente"],
                    "proveedor" => (int) $tercero["proveedor"],
                    "empleado" => (int) $tercero["empleado"],
                    "documento" => $tercero["cedulanit"],
                    "codVerificacion" => $tercero["codver"]
                ];

                $sql_departamento = "SELECT danedpto as departamentoId, nombredpto as nombre FROM danedpto WHERE danedpto = $tercero[depto]";
                $depto = $this->select($sql_departamento);

                $sql_municipio = "SELECT danedpto as departamentoId, danemnpio as municipioId, nom_mnpio as nombre 
                FROM danemnpio 
                WHERE danedpto = $tercero[depto] AND danemnpio = $tercero[mnpio]";
                $municipio = $this->select($sql_municipio);

                $sql_persona = "SELECT id_persona as personaId, nombre FROM personas WHERE estado = '1' AND id_persona = $tercero[persona]";
                $persona = $this->select($sql_persona);

                $sql_regimen = "SELECT id_regimen as regimenId, nombre FROM regimen WHERE estado = '1' AND id_regimen > 0 AND id_regimen = $tercero[regimen]";
                $regimen = $this->select($sql_regimen);

                $sql_tipo_documento = "SELECT doc.id_tipodocid as documentoId,
                doc.nombre,
                tipo.persona as personaId 
                FROM docindentidad as doc INNER JOIN documentopersona as tipo ON doc.id_tipodocid = tipo.tipodoc
                WHERE doc.estado = '1' AND doc.id_tipodocid = $tercero[tipodoc]";
                $tipoDocumento = $this->select($sql_tipo_documento);

                $sql_cuentas_banco = "SELECT tipo_cuenta as tipoCuenta, codigo_banco as codigoBanco, numero_cuenta as numeroCuenta FROM teso_cuentas_terceros WHERE id_tercero = $terceroId";
                $cuentasBancos = $this->select_all($sql_cuentas_banco);

                foreach ($cuentasBancos as $key => $banco) {
                    $sql_name_banco = "SELECT codigo, nombre FROM hum_bancosfun WHERE codigo = $banco[codigoBanco]";
                    $name = $this->select($sql_name_banco);

                    $cuentasBancos[$key]["nombreBanco"] = $name["nombre"];
                }

                $arrResponse = [
                    "status" => true, 
                    "datos" => $datos, 
                    "regimenes" => $regimenes, 
                    "departamentos" => $departamentos, 
                    "municipios" => $municipios, 
                    "bancos" => $bancos,
                    "departamento" => $depto,
                    "municipio" => $municipio,
                    "persona" => $persona,
                    "regimen" => $regimen,
                    "tipoDocumento" => $tipoDocumento,
                    "listaBancos" => $cuentasBancos
                ];
                
            }
            echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
            die();
        }

        public function save_edit() {
            if (!empty($_SESSION)) {
                $terceroId = $_POST["id"];
                $regimen = $_POST["regimen"];
                $departamento = $_POST["departamento"];
                $municipio = $_POST["municipio"];
                $datos = json_decode($_POST['datos'], true);
                $bancos = json_decode($_POST['bancos'], true);
                $sql_update = "UPDATE terceros SET 
                nombre1 = ?,
                nombre2 = ?,
                apellido1 = ?,
                apellido2 = ?,
                razonsocial = ?,
                direccion = ?,
                telefono = ?,
                celular = ?,
                email = ?,
                web = ?,
                depto = ?,
                mnpio = ?,
                regimen = ?,
                contribuyente = ?,
                proveedor = ?,
                empleado = ?
                WHERE id_tercero = $terceroId";

                $values_update = [
                    strClean(replaceChar(strtoupper("$datos[primerNombre]"))),
                    strClean(replaceChar(strtoupper("$datos[segundoNombre]"))),
                    strClean(replaceChar(strtoupper("$datos[primerApellido]"))),
                    strClean(replaceChar(strtoupper("$datos[segundoApellido]"))),
                    strClean(replaceChar(strtoupper("$datos[razonsocial]"))),
                    strClean(replaceChar(strtoupper("$datos[direccion]"))),
                    "$datos[telefono]",
                    "$datos[celular]",
                    "$datos[email]",
                    "$datos[paginaWeb]",
                    "$departamento",
                    "$municipio",
                    "$regimen",
                    (int) $datos["contribuyente"],
                    (int) $datos["proveedor"],
                    (int) $datos["empleado"]
                ];

                $this->update($sql_update, $values_update);

                if (!empty($bancos)) {

                    $sql_delete_bancos = "DELETE FROM teso_cuentas_terceros WHERE id_tercero = $terceroId";
                    $this->delete($sql_delete_bancos);

                    foreach ($bancos as $banco) {
                        $sql_bancos = "INSERT INTO teso_cuentas_terceros (id_tercero, tipo_cuenta, codigo_banco, numero_cuenta) VALUES (?, ?, ?, ?)";

                        $values_banco = [
                            $terceroId,
                            $banco["tipoCuenta"],
                            $banco["codigoBanco"],
                            $banco["numeroCuenta"]
                        ];

                        $this->insert($sql_bancos, $values_banco);
                    }
                }
                insertAuditoria("gen_auditoria","gen_funciones_id",1,"Editar",1);

                $arrResponse = ["status" => true, "msg" => "Editado con exito"];
            }

            echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
            die();
        }
    }

    if($_POST){
        $obj = new terceros();

        switch ($_POST["action"]) {
            case 'getDataCreate':
                $obj->get_data_create();
                break;
            
            case 'validateDocument':
                $obj->validate_document();
                break;
            
            case 'saveCreate':
                $obj->save_create();
                break;

            case 'getDataSearch':
                $obj->get_data_search();
                break;

            case 'getDataEdit':
                $obj->get_data_edit();
                break;

            case 'saveEdit':
                $obj->save_edit();
                break;

            default:
                # code...
                break;
        }
    }

    header("Content-type: application/json");
    die();