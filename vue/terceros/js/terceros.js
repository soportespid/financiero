const URL = "vue/terceros/model/terceros.php";
const URLEXCEL = "tercerosExcel.php";

var app = new Vue({
    el:"#myapp",
    data() {
        return {
            isLoading:false,
            infoTercero: true,
            infoBancario: false,
            modalBanco: false,
            txtSearchBanco: '',
            bancos: [],
            bancos_copy: [],
            tipoCuenta: '',
            banco: [],
            numeroCuenta: '',
            listaBancos: [],
            persona: [],
            personas: [],
            regimenes: [],
            regimen: [],
            documentos: [],
            tipoDocumento: [],
            documento: '',
            digitoVerificacion: '',
            datos: {
                    "primerNombre": '',
                    "segundoNombre": '',
                    "primerApellido": '',
                    "segundoApellido": '',
                    "razonsocial": '',
                    "dirección": '',
                    "telefono": '',
                    "celular": '',
                    "email": '',
                    "paginaWeb": '',
                    "contribuyente": false,
                    "proveedor": false,
                    "empleado": false,
                },
            departamentos: [],
            departamento: [],
            municipios: [],
            municipio: [],

            terceros: [],
            terceros_copy: [],
            txtSearchTercero: '',
            txtResultados: 0,
        }
    },
    mounted() {
        this.intPageVal = this.$refs.pageType.value;

        if (this.intPageVal == 1) {
            this.getDataCreate();
        } else if (this.intPageVal == 2) {
            this.getDataSearch();
        } else if (this.intPageVal == 3) {
            this.getDataEdit();
        }
    },
    methods: {
        getDataCreate: async function(){
            const formData = new FormData();
            formData.append("action","getDataCreate");
            const response = await fetch(URL,{method:"POST",body:formData});
            const objData = await response.json();

            this.personas = objData.personas;
            this.regimenes = objData.regimenes;
            this.documentos = objData.tipoDocumentos;
            this.departamentos = objData.departamentos;
            this.municipios = objData.municipios;
            this.bancos_copy = objData.bancos;
            this.bancos = objData.bancos;
        },

        validateDocument: async function(document) {
            if (document != "") {
                if (this.tipoDocumento != "") {
                    const formData = new FormData();
                    formData.append("action","validateDocument");
                    formData.append("documento",document);
                    const response = await fetch(URL,{method:"POST",body:formData});
                    const objData = await response.json();
    
                    if (objData.status) {
                        if (this.tipoDocumento.documentoId == 31) {
                            this.digitoVerificacion = this.calcularDigitoVerificacion(document);
                            console.log(this.digitoVerificacion);
                        }
                    }
                    else {
                        Swal.fire("Atención!",objData.msg,"warning");
                        this.documento = "";
                    }
                }
                else {
                    Swal.fire("Atención!","Primer debes seleccionar el tipo de documento","warning");
                    this.documento = "";
                }
            }
        },

        calcularDigitoVerificacion (myNit)  {
            var vpri,
                x,
                y,
                z;
            
            // Se limpia el Nit
            myNit = myNit.replace ( /\s/g, "" ) ; // Espacios
            myNit = myNit.replace ( /,/g,  "" ) ; // Comas
            myNit = myNit.replace ( /\./g, "" ) ; // Puntos
            myNit = myNit.replace ( /-/g,  "" ) ; // Guiones
            
            // Se valida el nit
            if  ( isNaN ( myNit ) )  {
                console.log ("El nit/cédula '" + myNit + "' no es válido(a).") ;
                return "" ;
            };
            
            // Procedimiento
            vpri = new Array(16) ; 
            z = myNit.length ;
            
            vpri[1]  =  3 ;
            vpri[2]  =  7 ;
            vpri[3]  = 13 ; 
            vpri[4]  = 17 ;
            vpri[5]  = 19 ;
            vpri[6]  = 23 ;
            vpri[7]  = 29 ;
            vpri[8]  = 37 ;
            vpri[9]  = 41 ;
            vpri[10] = 43 ;
            vpri[11] = 47 ;  
            vpri[12] = 53 ;  
            vpri[13] = 59 ; 
            vpri[14] = 67 ; 
            vpri[15] = 71 ;
            
            x = 0 ;
            y = 0 ;
            for  ( var i = 0; i < z; i++ )  { 
                y = ( myNit.substr (i, 1 ) ) ;
                // console.log ( y + "x" + vpri[z-i] + ":" ) ;
            
                x += ( y * vpri [z-i] ) ;
                // console.log ( x ) ;    
            }
            
            y = x % 11 ;
            // console.log ( y ) ;
            
            let result = ( y > 1 ) ? 11 - y : y;
            return result;
        },

        searchDataBancos: function() {
            let search = "";
            search = this.txtSearchBanco.toLowerCase();
            
            this.bancos_copy = [...this.bancos.filter(e=>e.codigo.toLowerCase().includes(search) || e.nombre.toLowerCase().includes(search))];
        },

        selectBanco: function(banco) {
            this.banco = banco;
            this.bancos_copy = this.bancos;
            this.txtSearchBanco = '';
            this.modalBanco = false;
        },

        agregarBanco: function() {

            if (this.tipoCuenta != "" && this.banco.codigo != "" && this.numeroCuenta != "") {
               
                let datosBancos = {
                    tipoCuenta: this.tipoCuenta,
                    codigoBanco: this.banco.codigo,
                    nombreBanco: this.banco.nombre,
                    numeroCuenta: this.numeroCuenta
                }
                this.listaBancos.push(datosBancos);
                
                datosBancos = [];
                this.banco = [];
                this.tipoCuenta = "";
                this.numeroCuenta = "";
            }
            else {
                Swal.fire("Atención!","Todos los campos con (*) son obligatorios","warning");
            }
        },

        deleteBanco: function(index) {
            this.listaBancos.splice(index, 1);
        },

        saveCreate: async function() {
            console.log("informacion:", this.digitoVerificacion, "tipo:", typeof this.digitoVerificacion);
            if (this.persona == "" || this.regimen == "" || this.tipoDocumento == "" || this.documento == "") {
                Swal.fire("Atención!","Por favor, completa los campos de Tipo persona, Regimen, Tipo documento y Documento","warning");
                return false;
            }

            if (this.digitoVerificacion !== "") {
                if (this.datos.razonsocial == "") {
                    Swal.fire("Atención!","Por favor, completa el campo Razón social","warning");
                    return false;
                }
            } else {
                if (this.datos.primerNombre == "" || this.datos.primerApellido == "") {
                    Swal.fire("Atención!","Por favor, completa el campo Primer nombre y Primer apellido","warning");
                    return false;
                }
            }

            if (!this.datos.direccion|| this.datos.direccion == "" || !this.departamento.departamentoId || this.departamento.departamentoId == "" || !this.municipio.municipioId || this.municipio.municipioId == "") {
                Swal.fire("Atención!","Por favor, completa los campos Dirección, Departamento y Municipio","warning");
                return false;
            }

            if (this.datos.contribuyente == false && this.datos.proveedor == false && this.datos.empleado == false) {
                Swal.fire("Atención!","Por favor, selecciona al menos un Tipo de tercero","warning");
                return false;
            }

            Swal.fire({
                title:"¿Estás segur@ de guardar?",
                text:"",
                icon: 'warning',
                showCancelButton:true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText:"Sí, guardar",
                cancelButtonText:"No, cancelar"
            }).then(async function(result){
                if(result.isConfirmed){
                    const formData = new FormData();
                    formData.append("action","saveCreate");
                    formData.append("persona", app.persona.personaId);
                    formData.append("regimen",app.regimen.regimenId);
                    formData.append("tipoDocumento",app.tipoDocumento.documentoId);
                    formData.append("documento",app.documento);
                    formData.append("digitoVerificacion",app.digitoVerificacion);
                    formData.append("departamento",app.departamento.departamentoId);
                    formData.append("municipio",app.municipio.municipioId);
                    formData.append("datos", JSON.stringify(app.datos));
                    formData.append("bancos", JSON.stringify(app.listaBancos));

                    app.isLoading = true;
                    const response = await fetch(URL,{method:"POST",body:formData});
                    const objData = await response.json();
                    app.isLoading = false;

                    if(objData.status){
                        Swal.fire("Guardado",objData.msg,"success");
                        setTimeout(function(){
                            location.reload();
                        },1000);
                    }else{
                        Swal.fire("Error",objData.msg,"error");
                    }
                }
            });
        },

        getDataSearch: async function() {
            const formData = new FormData();
            formData.append("action","getDataSearch");
            
            this.isLoading = true;
            const response = await fetch(URL,{method:"POST",body:formData});
            const objData = await response.json();
            this.isLoading = false;

            this.terceros = objData.terceros;
            this.terceros_copy = objData.terceros;
            this.txtResultados = this.terceros_copy.length;
        },

        searchData: async function() {
            let search = "";
            search = this.txtSearchTercero.toLowerCase();
        
            this.terceros_copy = [...this.terceros.filter(e=>e.cedulanit.toLowerCase().includes(search) || e.razonsocial.toLowerCase().includes(search) || e.nombre.toLowerCase().includes(search))];
            this.txtResultados = this.terceros_copy.length;
        },

        downloadFormat: function() {
            const form = document.createElement("form");
            form.method = "post";
            form.target = "_blank";
            form.action = URLEXCEL;
            
            function addField(name,value){
                const input = document.createElement("input");
                input.type="hidden";
                input.name=name;
                input.value = value;
                form.appendChild(input);
            }
         
            addField("datos", JSON.stringify(this.terceros_copy));
        
            document.body.appendChild(form);
            form.submit();
            document.body.removeChild(form);
        },

        getDataEdit: async function () {
            let id = new URLSearchParams(window.location.search).get('id');
            
            const formData = new FormData();
            formData.append("action", "getDataEdit");
            formData.append("id", id);
            this.isLoading = true;
            const response = await fetch(URL,{method:"POST",body:formData});
            const objData = await response.json();
            this.isLoading = false;

            this.datos = objData.datos;
            this.regimenes = objData.regimenes;
            this.departamentos = objData.departamentos;
            this.municipios = objData.municipios;
            this.departamento = objData.departamento;
            this.municipio = objData.municipio;
            this.persona = objData.persona;
            this.regimen = objData.regimen;
            this.tipoDocumento = objData.tipoDocumento;
            this.bancos = objData.bancos;
            this.bancos_copy = objData.bancos;
            this.listaBancos = objData.listaBancos;
        },

        saveEdit: async function() {
            let id = new URLSearchParams(window.location.search).get('id');
            
            if (this.persona == "" || this.regimen == "" || this.tipoDocumento == "") {
                Swal.fire("Atención!","Por favor, completa los campos de Tipo persona, Regimen, Tipo documento y Documento","warning");
                return false;
            }

            if (this.datos.codVerificacion == "") {
                if (this.datos.primerNombre == "" || this.datos.primerApellido == "") {
                    Swal.fire("Atención!","Por favor, completa el campo Primer nombre y Primer apellido","warning");
                    return false;
                }
            } else {
                if (this.datos.razonsocial == "") {
                    Swal.fire("Atención!","Por favor, completa el campo Razón social","warning");
                    return false;
                }
            }

            if (!this.datos.direccion|| this.datos.direccion == "" || !this.departamento.departamentoId || this.departamento.departamentoId == "" || !this.municipio.municipioId || this.municipio.municipioId == "") {
                Swal.fire("Atención!","Por favor, completa los campos Dirección, Departamento y Municipio","warning");
                return false;
            }

            if (this.datos.contribuyente == false && this.datos.proveedor == false && this.datos.empleado == false) {
                Swal.fire("Atención!","Por favor, selecciona al menos un Tipo de tercero","warning");
                return false;
            }
          
            Swal.fire({
                title:"¿Estás segur@ de guardar?",
                text:"",
                icon: 'warning',
                showCancelButton:true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText:"Sí, guardar",
                cancelButtonText:"No, cancelar"
            }).then(async function(result){
                if(result.isConfirmed){
                    const formData = new FormData();
                    formData.append("action","saveEdit");
                    formData.append("id",id);
                    formData.append("regimen",app.regimen.regimenId);
                    formData.append("departamento",app.departamento.departamentoId);
                    formData.append("municipio",app.municipio.municipioId);
                    formData.append("datos", JSON.stringify(app.datos));
                    formData.append("bancos", JSON.stringify(app.listaBancos));

                    app.isLoading = true;
                    const response = await fetch(URL,{method:"POST",body:formData});
                    const objData = await response.json();
                    app.isLoading = false;

                    if(objData.status){
                        Swal.fire("Guardado",objData.msg,"success");
                        setTimeout(function(){
                            app.getDataEdit();
                        },1500);
                    }else{
                        Swal.fire("Error",objData.msg,"error");
                    }
                }
            });
           
        },
    },
    computed:{

    }
})
