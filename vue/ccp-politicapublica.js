var app = new Vue({
    el: '#mypolitp',
    data:{
        search: {keywordpp: ''},
        noMember: false,
        results: [],
        show_table_search: false,
        show_resultados: true,
    },
  
    mounted: function(){
        this.fetchMembers();

    },
  
    methods:{
        searchMonitor: function() {
            var keywordpp = app.toFormData(app.search);
            axios.post('vue/ccp-politicapublica.php?action=search', keywordpp)
                .then(function(response){
                    
                    app.results = response.data.codigos;
                    
                    if(response.data.codigos == ''){
                        app.noMember = true;
                        app.show_resultados = false;
                    }
                    else{
                        app.noMember = false;
                        app.show_resultados = true;
                    }
                    
                });
            setTimeout(() => {
                document.getElementById("end_page").scrollIntoView({behavior: 'smooth'});   
            }, 50);
            app.search.keywordpp = '';
        },
  
        fetchMembers: function(){
            axios.post('vue/ccp-politicapublica.php')
                .then(function(response){
                    console.log(response.data.codigos);
                    app.results = response.data.codigos;
                    
                    // app.show_table_search = true
                });
        },
  
        toFormData: function(obj){
            var form_data = new FormData();
            for(var key in obj){
                form_data.append(key, obj[key]);
            }
            return form_data;
        },
    }
});
