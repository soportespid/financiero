var app = new Vue({
    el: '#myapp',
    data:{
        acuerdos: [],
        ingresos: [],
        mostrarIngresos: [],
        acuerdo: '',
        colortitulosmensaje: '',
        titulomensaje: '',
        contenidomensaje: '',
        colortitulosmensaje:'',
        opcionmensaje: '',
        showMensaje: false,
        showModal_Solo_Presupuesto: false,
        valor: '',
        valores: [],
        valorSolo: '',
        guardarValorSolo: '',
        validacionValores: [],
        posicion: '',
        concepto: '',
        vigencia: '',
        years: [],
        numero: '',
        cabecera: [],
        valorTotal: 0,
    },
    mounted:
        function(){
            this.cargaAcuerdos();
            this.cargayears();
            this.consecutivo();
    },
    methods:{
        cargaAcuerdos: async function()
		{
			await axios.post('vue/presupuesto_ccp/teso-superavit-ccpet.php?action=show')
			.then(
				(response)=>
				{
					app.acuerdos = response.data.acuerdos;
				}
			);
		},

        consecutivo: async function()
		{
			await axios.post('vue/presupuesto_ccp/teso-superavit-ccpet.php?action=show')
			.then(
				(response)=>
				{
                    console.log(response.data);
					app.numero = response.data.consecutivo;
				}
			);
		},

        cargayears: async function()
		{
			await axios.post('vue/presupuesto_ccp/ccp-bancoproyectos.php?buscar=years')
			.then(
				(response)=>
				{
					app.years=response.data.anio;
				}
			);
		},

        muestraIngresos: async function()
        {
            if(this.vigencia != '')
            {
                if(this.acuerdo != '')
                {
                    await axios.post('vue/presupuesto_ccp/teso-superavit-ccpet.php?buscar=buscarAcuerdos&acuerdo='+this.acuerdo+'&vigencia='+this.vigencia)
                    .then(
                        (response)=>
                        {
                            console.log(response.data);
                            app.ingresos = response.data.codigos;

                        }
                    );
                    this.mostrarIngresos = [];

                    for(let i = 0; i < this.ingresos.length; i++)
                    {
                        var rubro = this.ingresos[i][0];
                        var fuente = this.ingresos[i][1];
                        var vigenciaGasto = this.ingresos[i][2];
                        var clasificacion = this.ingresos[i][3];
                        var clasificador = '';
                        var valorIngreso = this.ingresos[i][4];
                        var secPresu = this.ingresos[i][5];
                        var medioPago = this.ingresos[i][6];
                        var clasificadorSuperavit = this.ingresos[i][7];
                        var mostrar = [rubro,fuente,vigenciaGasto,clasificador,valorIngreso,clasificacion,secPresu,medioPago,clasificadorSuperavit];
                        this.mostrarIngresos.push(mostrar);
                        this.validacionValores.push(valorIngreso);
                    }
                }
                else
                {
                    alert('Seleccione un tipo de acuerdo');
                }
            }
            else
            {
                alert('Digite la vigencia');
            }
        },

        toggleModal: function(posicion)
        {
            this.posicion = '',
            this.posicion = posicion;
            this.showModal_Solo_Presupuesto = !this.showModal_Solo_Presupuesto;
        },

        cambiarValor: function()
        {
            valorOriginal = parseInt(this.validacionValores[this.posicion]);
            valorNuevo = parseInt(this.valorSolo);

            if(valorNuevo >= 0)
            {
                if(valorOriginal >= valorNuevo)
                {
                    this.mostrarIngresos[this.posicion][4] = this.valorSolo;
                    this.valorSolo = '';
                    this.showModal_Solo_Presupuesto = !this.showModal_Solo_Presupuesto;
                }
                else
                {
                    alert("Valor digitado mayor al original, error digitacion");
                    this.valorSolo = '';
                }
            }
            else
            {
                alert('Valor digitado menor a 0, error de digitacion');
                this.valorSolo = '';
            }
        },

        guardar: function()
        {
            var fecha = document.getElementById('fecha').value;

            for (let i = 0; i < this.mostrarIngresos.length; i++)
            {
                var valorUnitario = parseInt(this.mostrarIngresos[i][4]);
                this.valorTotal = this.valorTotal + valorUnitario;
            }

            var informacionCabecera = [this.numero,this.acuerdo,fecha,this.vigencia,this.concepto,this.valorTotal];

            this.cabecera.push(informacionCabecera);

            if(this.concepto != '' && fecha != '' && this.vigencia != '' && this.acuerdo != '')
            {
                var formData = new FormData();
                for(var x = 0; x < this.cabecera.length; x++){
                    formData.append("cabecera[]", this.cabecera[x]);
                }

                for(var x = 0; x < this.mostrarIngresos.length; x++){
                    formData.append("detalles[]", this.mostrarIngresos[x]);
                }

                axios.post('vue/presupuesto_ccp/teso-superavit-ccpet.php?action=guardar',
                    formData
                    )
                    .then(function(response){
                        console.log(response.data);
                        location.reload();
                });

            }
            else
            {
                alert('Falta informacion para guardar.');
            }
        }

    },


});
