var app = new Vue({
    el: '#myapp',
    data:{
        id: '',
        valor: '',
        variable: '',
        _valor: '',
        v: '',
        numero:0,
        vigencia:0,
        nombre: '',
        tipo: 'Gasto',
        tipogf: 'I3',
        correlacion: '',
        conceptos: [],
        conceptoContableHermano: '',
        fecha: '',
        cuenta: '',
        debcred: '',
        nombreCuenta: '',
        showModal_cuentas: false,
        searchCuenta : {keywordCuenta: ''},
        cuentasContables: [],
        debcredOpciones: ['DEBITO', 'CREDITO'],
        centroCostos: [],
        cc: '',
        detalles: [],


    },

    mounted: function(){
        this.inicializaCodigo();
    },

    methods: {

        inicializaCodigo: async function(){
            await axios.post('vue/cont-inversion-creaCorrientes.php?tipogf=' + this.tipogf)
            .then((response) => {
                //console.log(response.data.codigo);
                app.numero = response.data.codigo;
                console.log(response.data.conceptos);
                app.conceptos = response.data.conceptos;
                app.centroCostos = response.data.centroCostos;
                // app.show_table_search = true
            });
        },

        despliegamodal2: function()
        {
            app.showModal_cuentas = true
            axios.post('vue/cont-inversion-creaCorrientes.php?action=buscarCuentas')
            .then((response) => {
                
                app.cuentasContables = response.data.cuentas;
                
            });
        },

        buscarCta: function(){

            if(this.cuenta != '' && this.showModal_cuentas == false){

                if(this.cuenta.length == 9){

                    axios.post('vue/cont-inversion-creaCorrientes.php?action=buscaCta&cuenta=' + this.cuenta)
                    .then((response) => {

                        this.nombreCuenta = response.data.nombreCuenta;
                        this.debcred = response.data.naturalezaCuenta;

                        if(response.data.nombreCuenta == '' || response.data.nombreCuenta == null){
                            alert('Esta cuenta no esta creada en el catalogo.');
                            document.getElementById("cuenta").focus();
                        }

                    });

                }else{

                    alert('Cuenta incorrecta.');
                    document.getElementById("cuenta").focus();
                }

            }else{
                this.nombreCuenta = '';
            }
        },

        searchMonitorCuenta: function(){
            var keywordCuenta = app.toFormData(app.searchCuenta);
            axios.post('vue/cont-inversion-creaCorrientes.php?action=filtrarCuentas', keywordCuenta)
            .then((response) => {
                app.cuentasContables = response.data.cuentas;
                
            });
        },

        toFormData: function(obj){
            var form_data = new FormData();
            for(var key in obj){
                form_data.append(key, obj[key]);
            }
            return form_data;
        },

        agregarDetalle: function(){

            var fec = document.getElementById('fecha').value;
            if(this.cuenta != '' && fec != '' && this.debcred != '' && this.cc != '' && this.nombreCuenta != ''){
                var detallesAgr = [];
                detallesAgr.push(fec);
                detallesAgr.push(this.cuenta);
                detallesAgr.push(this.nombreCuenta);
                detallesAgr.push(this.cc);
                //console.log(this.cc);
                detallesAgr.push(this.debcred);
                console.log(this.detallesAgr);
                this.detalles.push(detallesAgr);
                //console.log(this.detalles);
            }else{
                alert('Falta informacion para agregar');
            }
        },

        seleccionarCuenta: function(cuentaSelec){
            if(cuentaSelec[5] == 'Auxiliar'){
                this.cuenta = cuentaSelec [0];
                this.nombreCuenta = cuentaSelec [1];
                this.debcred = cuentaSelec [2];
                this.showModal_cuentas = false;
            }else{
                alert('Debe seleccionar una cuenta de tipo Auxiliar');
            }
            
        },

        eliminarDetalle: function(detalleEliminar){
            this.removeItemFromArr(detalleEliminar);
        },

        removeItemFromArr: function(item){
            var i = this.detalles.indexOf( item );
            if ( i !== -1 ) {
                this.detalles.splice( i, 1 );
            }
        },

        guardar: function(){
            if(this.nombre != '' && this.detalles.length > 0){

                var formData = new FormData();
                for(var x = 0; x < this.detalles.length; x++){
                    formData.append("detallesAgr[]", this.detalles[x]);
                }
                
                formData.append("nombre", this.nombre);
                formData.append("tipo", this.tipogf);
                formData.append("codigo", this.numero);

                formData.append("correlacion", this.correlacion);
                formData.append("conceptoContableHermano", this.conceptoContableHermano);

                axios.post('vue/cont-inversion-creaCorrientes.php?action=guardar',
                    formData
                    )
                    .then(function(response){
                        console.log(response.data);
                        if(response.data.insertaBien){
                            location.href = "cont-inversion-creaCorrientes.php";
                        }
                });

            }else{
                alert('Falta informacion para guardar.');
            }
        },
    },
})