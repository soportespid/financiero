<?php
	require '../../comun.inc';
    require '../../validaciones.inc';
	$linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");
	$out = array('error' => false);

	$action="show";

	if(isset($_GET['buscar']))
	{
		$buscar=$_GET['buscar'];
		switch ($buscar)
		{			
            case 'llenado':
            {
                //Llenado de ingresos
                //var_dump($_GET);
                $acuerdo =$_GET['acuerdo'];

                $ingresosCodigos = array();
                $nombreRubro = array();
                $nombreFuente = array();
                $nombreVigenciaGasto = array();
                $nombrePoliticaPublica = array();
                $nombreClasificador = array();

                $sqlReduccionIngresos = "SELECT acuerdo,rubro,fuente,vigencia_gasto,politica_publica,id_metas,indicador_producto,medio_pago,clasificacion,clasificador,cod_proyecto,bpin,vigencia,valor FROM ccpetreduccion WHERE acuerdo = $acuerdo AND rubro LIKE '1%' ORDER BY id ";
                $resReduccionIngresos = mysqli_query($linkbd,$sqlReduccionIngresos);

                while($rowReduccionIngresos = mysqli_fetch_row($resReduccionIngresos))
                {
                    $sqlRubroIngresos = "SELECT nombre FROM cuentasingresosccpet WHERE codigo = '$rowReduccionIngresos[1]' GROUP BY nombre ";
                    $resRubroIngresos = mysqli_query($linkbd,$sqlRubroIngresos);
                    $rowRubroIngresos = mysqli_fetch_row($resRubroIngresos);

                    array_push($nombreRubro, $rowRubroIngresos);

                    $sqlFuente = "SELECT fuente_financiacion FROM ccpet_fuentes WHERE id_fuente = $rowReduccionIngresos[2] ";
                    $resFuente = mysqli_query($linkbd,$sqlFuente);
                    $rowFuente = mysqli_fetch_row($resFuente);

                    if($rowFuente[0] == null)
                    {
                        $sqlFuente = "SELECT nombre FROM ccpet_fuentes_cuipo WHERE codigo_fuente = '$rowReduccionIngresos[2]' ";
                        $resFuente = mysqli_query($linkbd,$sqlFuente);
                        $rowFuente = mysqli_fetch_row($resFuente);
                    }

                    array_push($nombreFuente,$rowFuente);

                    if($rowReduccionIngresos[3] != '')
                    {
                        $sqlVigenciaGasto = "SELECT nombre FROM ccpet_vigenciadelgasto WHERE codigo = $rowReduccionIngresos[3] ";
                        $resVigenciaGasto = mysqli_query($linkbd,$sqlVigenciaGasto);
                        $rowVigenciaGasto = mysqli_fetch_row($resVigenciaGasto);
                    }
                    else
                    {
                        $rowVigenciaGasto = '';
                    }

                    array_push($nombreVigenciaGasto, $rowVigenciaGasto);

                    if($rowReduccionIngresos[4] != '')
                    {
                        $sqlPoliticaPublica = "SELECT nombre FROM ccpet_politicapublica WHERE codigo = $rowReduccionIngresos[4] ";
                        $resPoliticaPublica = mysqli_query($linkbd,$sqlPoliticaPublica);
                        $rowPoliticaPublica = mysqli_fetch_row($resPoliticaPublica);
                    }
                    else
                    {
                        $rowPoliticaPublica = '';
                    }

                    array_push($nombrePoliticaPublica,$rowPoliticaPublica);

                    switch($rowReduccionIngresos[8])
                    {
                        case 1:
                            $sqlClasificador = "SELECT nombre FROM ccpet_cuin WHERE codigo_cuin = $rowReduccionIngresos[9] ";
                            $resClasificador = mysqli_query($linkbd,$sqlClasificador);
                            $rowClasificador = mysqli_fetch_row($resClasificador);
                        break;

                        case 2:
                            $sqlClasificador = "SELECT titulo FROM ccpetbienestransportables WHERE grupo = $rowReduccionIngresos[9] ";
                            $resClasificador = mysqli_query($linkbd,$sqlClasificador);
                            $rowClasificador = mysqli_fetch_row($resClasificador);
                        break;

                        case 3:
                            $sqlClasificador = "SELECT titulo FROM ccpetservicios WHERE grupo = $rowReduccionIngresos[9]";
                            $resClasificador = mysqli_query($linkbd,$sqlClasificador);
                            $rowClasificador = mysqli_fetch_row($resClasificador);
                        break;

                        case 0:
                            $rowClasificador = '';
                        break;
                    }
                    array_push($nombreClasificador,$rowClasificador);

                    array_push($ingresosCodigos,$rowReduccionIngresos);
                }
                $out['ingresosCodigosIngresos'] = $ingresosCodigos;
                $out['nombreRubroIngresos'] = $nombreRubro;
                $out['nombreFuenteIngresos'] = $nombreFuente;
                $out['nombrevigenciaIngresos'] = $nombreVigenciaGasto;
                $out['nombrePoliticaIngresos'] = $nombrePoliticaPublica;
                $out['nombreClasificadorIngresos'] = $nombreClasificador;


                //Llenado de Gastos

                $gastosCodigos = array();
                $nombreRubro = array();
                $nombreFuente = array();
                $nombreVigencia = array();
                $nombrePolitica = array();
                $nombreMeta = array();
                $nombreClasificador = array();
                
                $sqlReduccionGastos = "SELECT acuerdo,rubro,fuente,vigencia_gasto,politica_publica,id_metas,indicador_producto,medio_pago,clasificacion,clasificador,cod_proyecto,bpin,vigencia,valor FROM ccpetreduccion WHERE acuerdo = $acuerdo AND rubro LIKE '2%' ORDER BY id ";
                $resReduccionGastos = mysqli_query($linkbd, $sqlReduccionGastos);
                while($rowReduccionGastos = mysqli_fetch_row($resReduccionGastos))
                {
                    $sqlRubroGastos = "SELECT nombre FROM cuentasccpet WHERE codigo = '$rowReduccionGastos[1]' GROUP BY nombre ";
                    $resRubroGastos = mysqli_query($linkbd,$sqlRubroGastos);
                    $rowRubroGastos = mysqli_fetch_row($resRubroGastos);

                    array_push($nombreRubro,$rowRubroGastos);

                    $sqlFuente = "SELECT fuente_financiacion FROM ccpet_fuentes WHERE id_fuente = $rowReduccionGastos[2] ";
                    $resFuente = mysqli_query($linkbd,$sqlFuente);
                    $rowFuente = mysqli_fetch_row($resFuente);

                    if($rowFuente[0] == null)
                    {
                        $sqlFuente = "SELECT nombre FROM ccpet_fuentes_cuipo WHERE codigo_fuente = '$rowReduccionGastos[2]' ";
                        $resFuente = mysqli_query($linkbd,$sqlFuente);
                        $rowFuente = mysqli_fetch_row($resFuente);
                    }

                    array_push($nombreFuente,$rowFuente);

                    if($rowReduccionGastos[3] != '')
                    {
                        $sqlVigenciaGastos = "SELECT nombre FROM ccpet_vigenciadelgasto WHERE codigo = $rowReduccionGastos[3] ";
                        $resVigenciaGastos = mysqli_query($linkbd,$sqlVigenciaGastos);
                        $rowVigenciaGastos = mysqli_fetch_row($resVigenciaGastos);
                    }
                    else
                    {
                        $rowVigenciaGastos = '';
                    }

                    array_push($nombreVigencia,$rowVigenciaGastos);

                    if($rowReduccionGastos[4] != '')
                    {
                        $sqlPoliticaPublica = "SELECT nombre FROM ccpet_politicapublica WHERE codigo = $rowReduccionGastos[4] ";
                        $resPoliticaPublica = mysqli_query($linkbd,$sqlPoliticaPublica);
                        $rowPoliticaPublica = mysqli_fetch_row($resPoliticaPublica);
                    }
                    elseif($rowReduccionGastos[4] == '')
                    {
                        $rowPoliticaPublica = '';
                    }
                    
                    array_push($nombrePolitica,$rowPoliticaPublica);

                    if($rowReduccionGastos[5] != 0)
                    {
                        $sqlMetas = "SELECT meta_pdm FROM ccpet_metapdm WHERE id = $rowReduccionGastos[5] ";
                        $resMetas = mysqli_query($linkbd,$sqlMetas);
                        $rowMetas = mysqli_fetch_row($resMetas);
                    }
                    elseif($rowReduccionGastos[5] == 0)
                    {
                        $rowMetas = '';
                    }

                    array_push($nombreMeta,$rowMetas);

                    switch($rowReduccionGastos[8])
                    {
                        case 1:
                            $sqlClasificador = "SELECT nombre FROM ccpet_cuin WHERE codigo_cuin = $rowReduccionGastos[9] ";
                            $resClasificador = mysqli_query($linkbd,$sqlClasificador);
                            $rowClasificador = mysqli_fetch_row($resClasificador);
                        break;

                        case 2:
                            $sqlClasificador = "SELECT titulo FROM ccpetbienestransportables WHERE grupo = $rowReduccionGastos[9] ";
                            $resClasificador = mysqli_query($linkbd,$sqlClasificador);
                            $rowClasificador = mysqli_fetch_row($resClasificador);
                        break;

                        case 3:
                            $sqlClasificador = "SELECT titulo FROM ccpetservicios WHERE grupo = $rowReduccionGastos[9]";
                            $resClasificador = mysqli_query($linkbd,$sqlClasificador);
                            $rowClasificador = mysqli_fetch_row($resClasificador);
                        break;

                        case 0:
                            $rowClasificador = '';
                        break;
                    }

                    array_push($nombreClasificador,$rowClasificador);

                    array_push($gastosCodigos,$rowReduccionGastos);
                }

                $out['gastosCodigosGastos'] = $gastosCodigos;
                $out['nombreRubroGastos'] = $nombreRubro;
                $out['nombreFuenteGastos'] = $nombreFuente;
                $out['nombrevigenciaGastos'] = $nombreVigencia;
                $out['nombrePoliticaGastos'] = $nombrePolitica;
                $out['nombreMetasGastos'] = $nombreMeta;
                $out['nombreClasificadorGastos'] = $nombreClasificador;

            }break;
        }
    }

	header("Content-type: application/json");
	echo json_encode($out);
	die();
?>