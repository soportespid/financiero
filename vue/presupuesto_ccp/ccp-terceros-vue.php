<?php
	require_once '../../comun.inc';
    require '../../funciones.inc';
    session_start();

	$linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");


    $out = array('error' => false);

    $action = "show";

    if(isset($_GET['action'])){
        $action = $_GET['action'];
    }

    if($action == 'cargarTiposDePersona'){

        $tiposDePersona = array();

        $sqlr = "SELECT * FROM personas WHERE estado = '1' ORDER BY id_persona ASC";
        $res = mysqli_query($linkbd, $sqlr);
        while($row = mysqli_fetch_row($res)){
            array_push($tiposDePersona, $row);
        }

        $out['tiposDePersona'] = $tiposDePersona;
    }

    if($action == 'cargarTiposDeRegimen'){

        $tiposDeRegimen = array();

        $sqlr = "SELECT * FROM regimen WHERE estado = '1' ORDER BY id_regimen ASC";
        $res = mysqli_query($linkbd, $sqlr);
        while($row = mysqli_fetch_row($res)){
            array_push($tiposDeRegimen, $row);
        }

        $out['tiposDeRegimen'] = $tiposDeRegimen;
    }

    if($action == 'cargarDepartamentos'){

        $departamentos = array();

        $sqlr = "SELECT * FROM danedpto ORDER BY id_dpto ASC";
        $res = mysqli_query($linkbd, $sqlr);
        while($row = mysqli_fetch_row($res)){
            array_push($departamentos, $row);
        }

        $out['departamentos'] = $departamentos;
    }

    if($action == 'cargarMunicipios'){

        $departamento = $_GET['departamento'];

        $municipios = array();

        $sqlr = "SELECT * FROM danemnpio WHERE danedpto = '$departamento' ORDER BY id_mnpio ASC";
        $res = mysqli_query($linkbd, $sqlr);
        while($row = mysqli_fetch_row($res)){
            array_push($municipios, $row);
        }

        $out['municipios'] = $municipios;
    }

    if($action == 'cargarTiposDeDocumento'){

        $tiposDeDocumento = array();

        $tipoPersona = $_GET['tipoPersona'];

        $sqlr = "SELECT docindentidad.id_tipodocid,docindentidad.nombre FROM  docindentidad, documentopersona WHERE docindentidad.estado='1' AND documentopersona.persona='$tipoPersona' AND documentopersona.tipodoc=docindentidad.id_tipodocid";
        $res = mysqli_query($linkbd, $sqlr);
        while($row = mysqli_fetch_row($res)){
            array_push($tiposDeDocumento, $row);
        }

        $out['tiposDeDocumento'] = $tiposDeDocumento;
    }

    if($action == 'buscarTercero'){

        $existeTercero = false;
        $documento = $_GET['documento'];

        $resultado = buscatercero($documento);
        if($resultado!='')
        {
            $existeTercero = true;
        }

        $out['existeTercero'] = $existeTercero;
    }

    header("Content-type: application/json");
    echo json_encode($out);
    die();