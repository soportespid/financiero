<?php
	require '../../comun.inc';
    require '../../funciones.inc';

	$linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");

    $out = array('error' => false);

    $maxVersion = ultimaVersionGastosCCPET();

    $action = "show";

    if(isset($_GET['action'])){
        $action = $_GET['action'];
    }

    if($action == 'show'){
        $vigencia = $_GET['vigencia'];
        $seccionPresupuestal = $_GET['seccionPresupuestal'];
        $vigenciaDelGasto = $_GET['vigenciaDelGasto'];
        $medioPago = $_GET['medioPago'];
        $sql = "SELECT codigo, nombre, tipo FROM cuentasccpet WHERE municipio=1 AND version='$maxVersion' AND codigo NOT LIKE '2.3%' ORDER BY id ASC";
        $res = mysqli_query($linkbd,$sql);
        $gastos = array();
        $valorPorCuenta = array();

        while($row=mysqli_fetch_row($res)){

            $sql_cuenta = "SELECT sum(valor) FROM ccpetinicialgastosfun WHERE cuenta LIKE '$row[0]%' AND vigencia = '$vigencia' AND  seccion_presupuestal = '$seccionPresupuestal' AND vigencia_gasto = '$vigenciaDelGasto' AND medio_pago = '$medioPago'";
            //echo $sql_cuenta;
            $res_cuenta = mysqli_query($linkbd, $sql_cuenta);
            $row_cuneta = mysqli_fetch_row($res_cuenta);
            $valorPorCuenta[$row[0]] = $row_cuneta[0];

            array_push($gastos, $row);
        }

        $out['gastos'] = $gastos;
        $out['valorPorCuenta'] = $valorPorCuenta;

    }

    if($action == 'secciones'){

        if(isset($_POST['base']) && isset($_POST['usuario'])){
            $base = $_POST['base'];
            $usuarioBase = $_POST['usuario'];
            $linkmulti = conectar_Multi($base, $usuarioBase);
            $linkmulti -> set_charset("utf8");
        }else{
            $linkmulti = $linkbd;
        }
        

        $sql = "SELECT * FROM pptoseccion_presupuestal WHERE estado = 'S'";
        $res = mysqli_query($linkmulti, $sql);
        $secciones = array();
        
        $codigo = '';

        while ($row = mysqli_fetch_row($res)){
            $seccion = array();
            $codigo = str_pad($row[0], 2, '0', STR_PAD_LEFT);
            array_push($seccion, $codigo);
            array_push($seccion, $row[2]);
            array_push($secciones, $seccion);
        }
        $out['secciones'] = $secciones;

    }

    if($action == 'vigenciasDelGasto'){
        $sql = "SELECT * FROM ccpet_vigenciadelgasto WHERE version = (SELECT MAX(version) FROM ccpet_vigenciadelgasto)";
        $res = mysqli_query($linkbd, $sql);
        $vigenciasDelGasto = array();

        while ($row = mysqli_fetch_row($res)) {
            array_push($vigenciasDelGasto, $row);
        }
        $out['vigenciasDelGasto'] = $vigenciasDelGasto;
    }

    if($action == 'areas'){
        $sql = "SELECT * FROM planacareas WHERE estado = 'S'";
        $res = mysqli_query($linkbd, $sql);
        $areas = array();

        while ($row = mysqli_fetch_row($res)) {
            array_push($areas, $row);
        }
        $out['areas'] = $areas;
    }

    if($action == 'buscarClasificadores'){
        $cuenta = $_GET['cuentaIngreso'];
        $sql = "SELECT clasificadores FROM ccpetprogramarclasificadoresgastos WHERE cuenta = '$cuenta'";
        $res = mysqli_query($linkbd, $sql);
        $clasificadores = array();
    
        while ($row = mysqli_fetch_row($res)) {
            array_push($clasificadores, $row);
        }
    
        $out['clasificadores'] = $clasificadores;
    }

    if($action=='searchFuente'){ 
        $keywordFuente=$_POST['keywordFuente'];
           // $sql="SELECT * FROM ccpet_cuin WHERE (nit like '%$keyword%' || nombre like '%$keyword%') AND version=(SELECT MAX(version) FROM ccpet_cuin )";
        $sql="SELECT * FROM ccpet_fuentes_cuipo WHERE concat_ws(' ', codigo_fuente, nombre) LIKE '%$keywordFuente%' AND version=(SELECT MAX(version) FROM ccpet_fuentes_cuipo ) AND  LENGTH(codigo_fuente) > 6";
        $res=mysqli_query($linkbd,$sql);
        $codigos = array();
    
        while($row=mysqli_fetch_row($res))
        {
            array_push($codigos, $row);
        }
    
        $out['codigos'] = $codigos;
    }

    if($action=='varlorFuente'){ 
        $cuentaAgr = $_POST['cuentaPresupuestal'];
        $unidadEjecutora = $_POST['unidadEjecutora'];
        $vigenciaDelGasto = $_POST['vigenciaDelGasto'];
        $medioPago = $_POST['medioPago'];
        $vigencia = $_POST['vigencia'];
        //$fuente = $_GET['fuente'];
        $valor = array();
        
        $sql = "SELECT SUM(valor), fuente FROM ccpetinicialgastosfun WHERE cuenta = '$cuentaAgr' AND seccion_presupuestal = '$unidadEjecutora' AND vigencia_gasto = '$vigenciaDelGasto' AND medio_pago = '$medioPago' AND vigencia = '$vigencia' GROUP BY fuente";
        $res = mysqli_query($linkbd,$sql);
        while($row = mysqli_fetch_row($res)){
            $valor[$row[1]] = $row[0];
        }
    
        $out['valor'] = $valor;
    }

    if($action=='searchValorSolo'){ 
        $cuentaAgr = $_GET['cuentaAgr'];
        $unidadEjecutora = $_GET['unidadEjecutora'];
        $fuente = $_GET['fuente'];
        $medioPago = $_GET['medioPago'];
        $vigencia = $_GET['vigencia'];
        $vigenciaDelGasto = $_GET['vigenciaDelGasto'];
        
        $sql = "SELECT valor FROM ccpetinicialgastosfun WHERE cuenta = '$cuentaAgr' AND seccion_presupuestal = '$unidadEjecutora' AND fuente='$fuente' AND vigencia_gasto = '$vigenciaDelGasto' AND medio_pago = '$medioPago' AND vigencia = '$vigencia' AND producto_servicio = '' "; 
        $res = mysqli_query($linkbd,$sql);
        $row = mysqli_fetch_row($res);
        $out['valor'] = $row[0];
    }

    if($action=='guardarValorSolo'){

        $sql = "DELETE FROM ccpetinicialgastosfun WHERE cuenta = '".$_POST['cuentaPresupuestal']."' AND seccion_presupuestal = '".$_POST['unidadEjecutora']."' AND fuente='".$_POST['fuente']."' AND medio_pago = '".$_POST['medioPago']."' AND vigencia_gasto = '".$_POST['vigenciaDelGasto']."' AND vigencia = '".$_POST['vigencia']."'";
        mysqli_query($linkbd, $sql);
    
        if($_POST['valorSolo'] >= 0){
            
            $sql = "INSERT INTO ccpetinicialgastosfun (cuenta, fuente, seccion_presupuestal, vigencia_gasto, medio_pago, vigencia, valor, finalizar) VALUES ('".$_POST['cuentaPresupuestal']."', '".$_POST['fuente']."', '".$_POST['unidadEjecutora']."', '".$_POST['vigenciaDelGasto']."', '".$_POST['medioPago']."', '".$_POST['vigencia']."', '".$_POST['valorSolo']."', 'N')";//echo $sql;
    
            //$sql="INSERT INTO  ccpetinicialvalorgastos (cuenta, valor, unidad, fuente, medioPago, vigencia) VALUES ('".$_POST['cuentaPresupuestal']."', '".$_POST['valorSolo']."', '".$_POST['unidadEjecutora']."', '".$_POST['fuente']."', '".$_POST['medioPago']."', '".$_POST['vigencia']."')";
            if(mysqli_query($linkbd, $sql))
            {
                $out['insertaBien'] = true; 
            }
            else
            {
                $out['insertaBien'] = false; 
            }
        }else{
            $out['insertaBien'] = true; 
        }
        
        
    }

    if($action == "searchCuentaAgr"){
        $cuentaAgr = $_GET['cuentaAgr'];
        $unidadEjecutora = $_GET['unidadEjecutora'];
        $fuente = $_GET['fuente'];
        $medioPago = $_GET['medioPago'];
        $vigencia = $_GET['vigencia'];
        $vigenciaDelGasto = $_GET['vigenciaDelGasto'];

        $sql = "SELECT producto_servicio, valor FROM ccpetinicialgastosfun WHERE cuenta = '$cuentaAgr' AND seccion_presupuestal = '$unidadEjecutora' AND fuente = '$fuente' AND medio_pago = '$medioPago' AND vigencia='$vigencia' AND vigencia_gasto = '$vigenciaDelGasto' ";
        $res = mysqli_query($linkbd, $sql);//echo $sql;
        $codigos = array();
        $codigosEliminar = array();
        $valorTotal = 0;
    
        while($row = mysqli_fetch_row($res))
        {
            $sql_bienes = "SELECT grupo, titulo, ud FROM ccpetbienestransportables WHERE grupo = '$row[0]'";
            $res_bienes = mysqli_query($linkbd, $sql_bienes);
            $row_bienes = mysqli_fetch_row($res_bienes);
    
            //array_push($codigosEliminar, $row_bienes);
    
            array_push($row_bienes, $row[1]);
    
            array_push($codigos, $row_bienes);
    
            $valorTotal += $row[1];
        }
        $out['codigos'] = $codigos;
        //$out['codigosEliminar'] = $codigosEliminar;
        $out['valorTotal'] = $valorTotal;
    }

    if($action == "searchCuentaServiciosAgr"){
        $cuentaAgr = $_GET['cuentaAgr'];
        $unidadEjecutora = $_GET['unidadEjecutora'];
        $fuente = $_GET['fuente'];
        $medioPago = $_GET['medioPago'];
        $vigencia = $_GET['vigencia'];
        $vigenciaDelGasto = $_GET['vigenciaDelGasto'];

        $sql = "SELECT producto_servicio, valor FROM ccpetinicialgastosfun WHERE cuenta = '$cuentaAgr' AND seccion_presupuestal = '$unidadEjecutora' AND fuente = '$fuente' AND medio_pago = '$medioPago' AND vigencia='$vigencia' AND vigencia_gasto = '$vigenciaDelGasto' ";
        $res = mysqli_query($linkbd, $sql);//echo $sql;
        $codigos = array();
        $codigosEliminar = array();
        $valorTotalServicios = 0;
    
        while($row = mysqli_fetch_row($res))
        {
            $sql_bienes = "SELECT grupo, titulo, ciiu, cpc FROM ccpetservicios WHERE grupo = '$row[0]'";
            $res_bienes = mysqli_query($linkbd, $sql_bienes);
            $row_bienes = mysqli_fetch_row($res_bienes);

            //array_push($codigosEliminar, $row_bienes);

            array_push($row_bienes, $row[1]);

            array_push($codigos, $row_bienes);

            $valorTotalServicios += $row[1];
        }
        $out['codigos'] = $codigos;
        //$out['codigosEliminar'] = $codigosEliminar;
        $out['valorTotalServicios'] = $valorTotalServicios;
    }

    if($action=='guardarBienes'){

        //var_dump($_POST);
        $sql = "DELETE FROM ccpetinicialgastosfun WHERE cuenta = '".$_POST['cuentaPresupuestal']."' AND seccion_presupuestal = '".$_POST['unidadEjecutora']."' AND fuente='".$_POST['fuente']."' AND medio_pago = '".$_POST['medioPago']."' AND vigencia_gasto = '".$_POST['vigenciaDelGasto']."' AND vigencia = '".$_POST['vigencia']."'";
        mysqli_query($linkbd, $sql);
    
        if($_POST['valorTotal'] > 0){

            for($x = 0; $x < count($_POST['cuentasSubclase']); $x++){

                $cuentas_separadas = explode(",", $_POST['cuentasSubclase'][$x]);

                $sql = "INSERT INTO ccpetinicialgastosfun (cuenta, fuente, seccion_presupuestal, vigencia_gasto, medio_pago, vigencia, producto_servicio, valor, finalizar) VALUES ('".$_POST['cuentaPresupuestal']."', '".$_POST['fuente']."', '".$_POST['unidadEjecutora']."', '".$_POST['vigenciaDelGasto']."', '".$_POST['medioPago']."', '".$_POST['vigencia']."', '".$cuentas_separadas[0]."', '".$cuentas_separadas[1]."', 'N')";

                /* $sql = "INSERT INTO  ccpetinicialigastosbienestransportables (cuenta, subclase, valor, unidad, fuente, medioPago, vigencia) VALUES ('".$_POST['cuentaPresupuestal']."', '".$cuentas_separadas[0]."', '".$cuentas_separadas[1]."', '".$_POST['unidadEjecutora']."', '".$_POST[fuente]."', '".$_POST['medioPago']."', '".$_POST['vigencia']."')"; */

                if(mysqli_query($linkbd,$sql))
                {
                    $out['insertaBien'] = true; 
                }
                else
                {
                    $out['insertaBien'] = false; 
                }
            }
        }else{
            $out['insertaBien'] = true; 
        }
        
    }

    if($action=='guardarServicios'){ 

        //var_dump($_POST);
        $sql = "DELETE FROM ccpetinicialgastosfun WHERE cuenta = '".$_POST['cuentaPresupuestal']."' AND seccion_presupuestal = '".$_POST['unidadEjecutora']."' AND fuente='".$_POST['fuente']."' AND medio_pago = '".$_POST['medioPago']."' AND vigencia_gasto = '".$_POST['vigenciaDelGasto']."' AND vigencia = '".$_POST['vigencia']."'";
        mysqli_query($linkbd, $sql);
    
        if($_POST['valorTotalServicios'] > 0){

            //var_dump($_POST['cuentasCuin']);
            for($x = 0; $x < count($_POST['cuentasSubclaseServicios']); $x++){

                $cuentas_separadas = explode(",", $_POST['cuentasSubclaseServicios'][$x]);

                $sql = "INSERT INTO ccpetinicialgastosfun (cuenta, fuente, seccion_presupuestal, vigencia_gasto, medio_pago, vigencia, producto_servicio, valor, finalizar) VALUES ('".$_POST['cuentaPresupuestal']."', '".$_POST['fuente']."', '".$_POST['unidadEjecutora']."', '".$_POST['vigenciaDelGasto']."', '".$_POST['medioPago']."', '".$_POST['vigencia']."', '".$cuentas_separadas[0]."', '".$cuentas_separadas[1]."', 'N')";
                
                /* $sql = "INSERT INTO  ccpetinicialservicios (cuenta, subclase, valor, unidad, fuente, medioPago, vigencia) VALUES ('".$_POST['cuentaPresupuestal']."', '".$cuentas_separadas[0]."', '".$cuentas_separadas[1]."', '".$_POST['unidadEjecutora']."', '".$_POST['fuente']."', '".$_POST['medioPago']."', '".$_POST['vigencia']."')"; */

                if(mysqli_query($linkbd,$sql))
                {
                    $out['insertaBien'] = true; 
                }
                else
                {
                    $out['insertaBien'] = false; 
                }
            }
        }else{
            $out['insertaBien'] = true; 
        }
        
    }

    header("Content-type: application/json");
    echo json_encode($out);
    die();

