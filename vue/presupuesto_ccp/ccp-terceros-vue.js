var app = new Vue({

    el: '#myapp',
	data:{

        tipoPersona: '-1',
        tiposDePersona: [],

        regimen: '-1',
        tiposDeRegimen: [],

        tipoDocumento: '-1',
        tiposDeDocumento: [],
        documento: '',
        digitoVerificacion: '',
        primerApellido: '',
        segundoApellido: '',
        primerNombre: '',
        segundoNombre: '',
        razonSocial: '',
        direccion: '',
        telefono: '',
        celular: '',
        email: '',
        paginaWeb: '',
        departamento: '',
        departamentos: [],
        municipio: '',
        municipios: [],
        checked: false,


    },

    mounted: async function(){

        this.cargarTiposDePersona();
        this.cargarTiposDeRegimen();
        this.cargarDepartamentos();

    },

    methods: {

        cargarTiposDePersona: async function(){

            await axios.post('vue/presupuesto_ccp/ccp-terceros-vue.php?action=cargarTiposDePersona')
            .then((response) => {
                
                app.tiposDePersona = response.data.tiposDePersona;

            });

        },

        cargarTiposDeRegimen: async function(){

            await axios.post('vue/presupuesto_ccp/ccp-terceros-vue.php?action=cargarTiposDeRegimen')
            .then((response) => {
                
                app.tiposDeRegimen = response.data.tiposDeRegimen;

            });

        },

        cargarDepartamentos: async function(){

            await axios.post('vue/presupuesto_ccp/ccp-terceros-vue.php?action=cargarDepartamentos')
            .then((response) => {
                
                app.departamentos = response.data.departamentos;

            });

        },

        cargarMunicipios: async function(){

            await axios.post('vue/presupuesto_ccp/ccp-terceros-vue.php?action=cargarMunicipios&departamento=' + this.departamento)
            .then((response) => {
                
                app.municipios = response.data.municipios;

            });

        },

        cargarTiposDeDocumento: async function(){

            await axios.post('vue/presupuesto_ccp/ccp-terceros-vue.php?action=cargarTiposDeDocumento&tipoPersona=' + this.tipoPersona)
            .then((response) => {
                
                app.tiposDeDocumento = response.data.tiposDeDocumento;

            });

        },

        buscaTercero: function(){
            if(this.documento != ''){
                axios.post('vue/presupuesto_ccp/ccp-terceros-vue.php?action=buscarTercero&documento=' + this.documento)
                .then((response) => {
                    if(response.data.existeTercero){
                        Swal.fire(
                            'El tercero ya esta creado.',
                            'Consultar el terceros',
                            'warning'
                            ).then((result) => {
                                app.documento = '';
                                app.digitoVerificacion = '';
                                
                            });
                    }else{
                        app.codigoDeVerificacion();
                    }

                });
            }
        },

        codigoDeVerificacion: function(){
            var vpri, x, y, z, i, nit1, dv1;
            nit1=this.documento;
            if (isNaN(nit1)){this.digitoVerificacion="";}
            else {
                if(this.tipoDocumento=='31')
                {
                    vpri = new Array(16);
                    x=0; 
                    y=0; 
                    z=nit1.length;
                    vpri[1]=3;
                    vpri[2]=7;
                    vpri[3]=13;
                    vpri[4]=17;
                    vpri[5]=19;
                    vpri[6]=23;
                    vpri[7]=29;
                    vpri[8]=37;
                    vpri[9]=41;
                    vpri[10]=43;
                    vpri[11]=47;
                    vpri[12]=53;
                    vpri[13]=59;
                    vpri[14]=67;
                    vpri[15]=71;
                    for(i=0 ; i<z ; i++)
                    {
                        y=(nit1.substr(i,1));
                        //document.write(y+"x"+ vpri[z-i] +":");
                        x+=(y*vpri[z-i]);
                        //document.write(x+"<br>");
                    }
                    y=x%11
                    //document.write(y+"<br>");
                    if (y > 1){dv1=11-y;}
                    else {dv1=y;}
                    this.digitoVerificacion=dv1;
                }
                else{this.digitoVerificacion="";}
            }
        },

        cambiaTipoDePersona: function(){
            this.cargarTiposDeDocumento();
        },

        guardar: function(){
            

        },


    }
});