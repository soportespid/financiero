var app = new Vue({ 
	el: '#myapp',
	data:
	{
		acuerdo: '',
		tabheight10: '90%',
		presupuestoTotalIngresos: [],
		presupuestoTotalGastos: [],
		tabgroupMostrar:1,
		guardarCodigosGastos: [],
		guardarCodigosIngresos: [],
		medioPagoIngresos: '',
		medioPagoFuncionamiento: '',
		nombreRubroIngresos: [],
		nombreFuenteIngresos: [],
		nombreVigenciaIngresos: [],
		nombrePoliticaIngresos: [],
		nombreClasificadorIngresos: [],
		nombreRubroGastos: [],
		nombreFuenteGastos: [],
		nombreVigenciaGastos: [],
		nombrePoliticaGastos: [],
		nombreMetasGastos: [],
		nombreClasificadorGastos: [],
		valorActoadministrativo: [],
		valorActoAdmin: '',
		valorTotalGastos : 0,
		valorTotalIngresos : 0,
		nombreActoAdministrativo: '',
		nombreActoRecibir: '',
		checked: false,
	},
	mounted: 
	function()
	{
		this.llenadoGastosIngresos();
	},
	
	methods:
	{
        llenadoGastosIngresos: async function()
        {
            await axios.post('vue/presupuesto_ccp/ccp-verReduccion.php?buscar=llenado&acuerdo=' + this.acuerdo)
			.then(
				(response)=>
				{
					app.guardarCodigosIngresos = response.data.ingresosCodigosIngresos;
					app.nombreRubroIngresos = response.data.nombreRubroIngresos;
					app.nombreFuenteIngresos = response.data.nombreFuenteIngresos;
					app.nombreVigenciaIngresos = response.data.nombrevigenciaIngresos;
					app.nombrePoliticaIngresos = response.data.nombrePoliticaIngresos;
					app.clasificadorIngresos = response.data.nombreClasificadorIngresos;

					app.guardarCodigosGastos = response.data.gastosCodigosGastos;
					app.nombreRubroGastos = response.data.nombreRubroGastos;
					app.nombreFuenteGastos = response.data.nombreFuenteGastos;
					app.nombreVigenciaGastos = response.data.nombrevigenciaGastos;
					app.nombrePoliticaGastos = response.data.nombrePoliticaGastos;
					app.nombreMetasGastos = response.data.nombreMetasGastos;
					app.nombreClasificadorGastos = response.data.nombreClasificadorGastos;
				}
			);

            

			for (let i = 0; i < this.guardarCodigosIngresos.length; i++) 
			{
				this.valorTotalIngresos = parseInt(this.valorTotalIngresos) + parseInt(this.guardarCodigosIngresos[i][13]);

				var mostrarIngresos = [];
				var rubro = this.guardarCodigosIngresos[i][1] + ' - ' + this.nombreRubroIngresos[i][0];
				var fuente = this.guardarCodigosIngresos[i][2] + ' - ' + this.nombreFuenteIngresos[i][0];
				var vigencia;
				var politica;
				var clasificador;

				if(this.guardarCodigosIngresos[i][3] != '')
				{
					vigencia = this.guardarCodigosIngresos[i][3] + ' - ' + this.nombreVigenciaIngresos[i][0];	
				}
				else
				{
					vigencia = '';
				}
				

				if(this.guardarCodigosIngresos[i][4] != '')
				{
					politica = this.guardarCodigosIngresos[i][4] + ' - ' + this.nombrePoliticaIngresos[i][0];	
				}
				else
				{
					politica = '';
				}
				
				if(this.guardarCodigosIngresos[i][9] != '')
				{
					clasificador = this.guardarCodigosIngresos[i][9] + ' - ' + this.clasificadorIngresos[i][0];
				}
				else
				{
					clasificador = '';
				}
				
				
				var valor = this.guardarCodigosIngresos[i][13];

				mostrarIngresos = [rubro,fuente,vigencia,politica,clasificador,valor];
				this.presupuestoTotalIngresos.push(mostrarIngresos);
			}

			for (let i = 0; i < this.guardarCodigosGastos.length; i++) 
			{
				this.valorTotalGastos = parseInt(this.valorTotalGastos) + parseInt(this.guardarCodigosGastos[i][13]);

				var bpin = rubro = fuente = vigencia = politica = meta = medioPago = clasificador = valor = mostrarGastos =[];
	
				bpin = this.guardarCodigosGastos[i][11];
				rubro = this.guardarCodigosGastos[i][1] + ' - ' + this.nombreRubroGastos[i][0];
				fuente = this.guardarCodigosGastos[i][2] + ' - ' + this.nombreFuenteGastos[i][0];
				
				if(this.guardarCodigosGastos[i][3] != '')
				{
					vigencia = this.guardarCodigosGastos[i][3] + ' - ' + this.nombreVigenciaGastos[i][0];	
				}
				else
				{
					vigencia = '';
				}

				if(this.guardarCodigosGastos[i][4] != '')
				{
					politica = this.guardarCodigosGastos[i][4] + ' - ' + this.nombrePoliticaGastos[i][0];	
				}
				else
				{
					politica = '';
				}

				if(this.guardarCodigosGastos[i][5] != '0')
				{
					meta = this.guardarCodigosGastos[i][5] + ' - ' + this.nombreMetasGastos[i][0];
				}
				else
				{
					meta = '';
				}

				medioPago = this.guardarCodigosGastos[i][7];

				if(this.guardarCodigosGastos[i][9] != '')
				{
					clasificador = this.guardarCodigosGastos[i][9] + ' - ' + this.nombreClasificadorGastos[i][0];
				}
				else
				{
					clasificador = '';
				}

				valor = this.guardarCodigosGastos[i][13];

				mostrarGastos = [bpin,rubro,fuente,vigencia,politica,meta,medioPago,clasificador,valor];

				this.presupuestoTotalGastos.push(mostrarGastos);
			}

        },            
		
	},
});