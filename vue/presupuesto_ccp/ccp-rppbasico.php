<?php
	require_once '../../comun.inc';
	require '../../validaciones.inc';
    require '../../funciones.inc';
    require 'funcionesccp.inc.php';

    session_start();

	$linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");

    $maxVersion = ultimaVersionGastosCCPET();


    $out = array('error' => false);


    $action = "show";

    if(isset($_GET['action'])){
        $action = $_GET['action'];
    }

    if($action == 'seleccionarConsecutivo'){
        
        $consecutivo = 0;
        $sqlr = "SELECT MAX(consvigencia) FROM ccpetrp WHERE vigencia=$_GET[vigencia] AND tipo_mov='201'";
        $res = mysqli_query($linkbd, $sqlr);
        while($r = mysqli_fetch_row($res)){
            $maximo = $r[0];
        }
        if(!$maximo){
            $consecutivo = 1;
        }
        else{
            $consecutivo = $maximo+1;
        }

        $out['consecutivo'] = $consecutivo;
    }

    if($action == 'seleccionarOrigenRp'){
        $origen = '';
        $sqlr = "SELECT orden FROM configbasica";
        $resr = mysqli_query($linkbd, $sqlr);
        $rowr = mysqli_fetch_array($resr);
        $origen = $rowr['orden'];
        $out['origen'] = $origen;
    }

    if($action == 'cargarDestinosDeCompra'){

        $destinosDeCompra = array();

        $sqlr = "SELECT * FROM almdestinocompra WHERE estado = 'S'";
        $res = mysqli_query($linkbd, $sqlr);
        while($row = mysqli_fetch_row($res)){
            array_push($destinosDeCompra, $row);
        }

        $out['destinosDeCompra'] = $destinosDeCompra;
    }

    if($action == 'cargarCdps'){

        $cdps = array();
        $fecha = $_GET['fecha'];
        $vigencia = $_GET['vigencia'];
        $sqlr = "SELECT consvigencia, vigencia, objeto FROM ccpetcdp WHERE vigencia = '$vigencia' AND tipo_mov = '201'";
        $res = mysqli_query($linkbd, $sqlr);
        while($row = mysqli_fetch_row($res)){
            $valorCDP = 0;
            $valorCDP = generaSaldoCDPccpetBasico($row[0],$row[1],$fecha);
			if($valorCDP>0){
                array_push($row, $valorCDP);
                array_push($cdps, $row);
            }
            
        }

        $out['cdps'] = $cdps;
    }

    if($action == 'cargarRegContratos'){

        $regContratos = array();
        $fecha = $_GET['fecha'];
        $vigencia = $_GET['vigencia'];
        $sqlr = "SELECT * FROM ccp_registro_contrato WHERE estado != 'N' AND vigencia = '$vigencia'";
        $res = mysqli_query($linkbd, $sqlr);
        while($row = mysqli_fetch_assoc($res)){
            $valorCDP = generaSaldoCDPccpetBasico($row['cdp'], $vigencia, $fecha);
            
            if($valorCDP>0){
                $nombre = buscatercero($row['tercero']);
                $row['nombreTercero'] = $nombre;
                array_push($regContratos, $row);
            }
            
        }

        $out['regContratos'] = $regContratos;
    }

    if($action == 'cuentasCcpet'){

        $cuentasCppet = [];
        $proyectos = [];
        $fuentes = [];

        $sqlr = "SELECT codigo, nombre FROM cuentasccpet WHERE tipo = 'C' AND version = $maxVersion";
        $res = mysqli_query($linkbd, $sqlr);

        while($row = mysqli_fetch_row($res)){
            array_push($cuentasCppet, $row);
        }

        $sqlrProyectos = "SELECT codigo, nombre FROM ccpproyectospresupuesto";
        $resProyectos = mysqli_query($linkbd, $sqlrProyectos);
        while($row = mysqli_fetch_row($resProyectos)){
            array_push($proyectos, $row);
        }

        $sqlrFuentes = "SELECT codigo_fuente, nombre FROM ccpet_fuentes_cuipo";
        $resFuentes = mysqli_query($linkbd, $sqlrFuentes);
        while($row = mysqli_fetch_row($resFuentes)){
            array_push($fuentes, $row);
        }

        $out['fuentes'] = $fuentes;

        $out['proyectos'] = $proyectos;

        $out['cuentasCppet'] = $cuentasCppet;



    }

    if($action == 'cargarRps'){

        $rps = array();
        $fecha = $_GET['fecha'];
        $vigencia = $_GET['vigencia'];
        $sqlr = "SELECT consvigencia, vigencia, detalle, fecha FROM ccpetrp WHERE vigencia = '$vigencia' AND tipo_mov = '201'";
        $res = mysqli_query($linkbd, $sqlr);
        while($row = mysqli_fetch_row($res)){
            $valorRP = 0;
            $valorRP = generaSaldoRPccpet($row[0],$row[1]);
			if($valorRP>0){
                array_push($row, $valorRP);
                array_push($rps, $row);
            }
            
        }

        $out['rps'] = $rps;
    }


    if($action == 'filtrarCdps'){

        $keywordCdp=$_POST['keywordCdp'];

        $cdps = array();
        $fecha = $_GET['fecha'];
        $vigencia = $_GET['vigencia'];
        
        $sqlr = "SELECT consvigencia, vigencia, objeto FROM ccpetcdp WHERE vigencia = '$vigencia' AND tipo_mov = '201' AND concat_ws(' ', consvigencia, objeto) LIKE '%$keywordCdp%'";
        $res = mysqli_query($linkbd, $sqlr);
        while($row = mysqli_fetch_row($res)){
            $valorCDP = 0;
            $valorCDP = generaSaldoCDPccpetBasico($row[0],$row[1],$fecha);
            if($valorCDP>0){
                array_push($row, $valorCDP);
                array_push($cdps, $row);
            }
        }

        $out['cdps'] = $cdps;
    }


    if($action == 'buscarCdp'){

        
        $vigencia = $_GET['vigencia'];
        $fecha = $_GET['fecha'];
        $consvigencia = $_GET['cdp'];
        $conSaldo = false;
        $sqlr = "SELECT consvigencia, vigencia FROM ccpetcdp WHERE vigencia = '$vigencia' AND tipo_mov = '201' AND consvigencia = '$consvigencia'";
        $res = mysqli_query($linkbd, $sqlr);
        while($row = mysqli_fetch_row($res)){
            $valorCDP = 0;
            $valorCDP = generaSaldoCDPccpetBasico($row[0],$row[1],$fecha);
            if($valorCDP>0){
                $conSaldo = true;
            }
        }

        $out['conSaldo'] = $conSaldo;
    }

    if($action == 'buscarSiEsDeNomina'){
        
        $vigencia = $_GET['vigencia'];
        $cdp = $_GET['cdp'];
        $cdpDeNomina = false;
        $sqlr = "SELECT * FROM hum_nom_cdp_rp WHERE vigencia = '$vigencia' AND cdp = '$cdp'";
        $res = mysqli_query($linkbd, $sqlr);
        while($row = mysqli_fetch_row($res)){
            if($row[0] != ''){
                $cdpDeNomina = true;
            }
        }

        $out['cdpDeNomina'] = $cdpDeNomina;
    }
    

    if($action == 'buscaInformacionCdp'){

        $vigencia = $_GET['vigencia'];
        $consvigencia = $_GET['consvigencia'];
        $fechaf = $_GET['fecha'];
        $conSaldo = false;
        $cdpCab = array();
        $cdpDet = array();
        $valorReg = array();
        $fecha = '';

        $sqlr = "SELECT consvigencia, fecha, solicita, objeto FROM ccpetcdp WHERE vigencia = '$vigencia' AND tipo_mov = '201' AND consvigencia = '$consvigencia'";
        $res = mysqli_query($linkbd, $sqlr);
        while($row = mysqli_fetch_row($res)){
            $fecha = $row[1];
            array_push($cdpCab, $row);
            
        }

        $sqlrD = "SELECT * FROM ccpetcdp_detalle WHERE vigencia = '$vigencia' AND tipo_mov = '201' AND consvigencia = '$consvigencia' ORDER BY cuenta DESC";
        $resD = mysqli_query($linkbd, $sqlrD);
        while($rowD = mysqli_fetch_row($resD)){
            
            //$cpc = $rowD[16] != '' ? '%' : $rowD[4];
            $cpc = $rowD[4];
            $saldo = generaSaldoCDPccpetBasico($rowD[2], $vigencia, $fechaf, $rowD[3], $rowD[5], $cpc, $rowD[11], $rowD[16], $rowD[17], $rowD[12], $rowD[14], $rowD[22]);
            array_push($valorReg, $saldo);

            array_push($cdpDet, $rowD);
        }

        $out['cdpCab'] = $cdpCab;
        $out['cdpDet'] = $cdpDet;
        $out['valorReg'] = $valorReg;
    }

    if($action == 'buscaInformacionRp'){

        $vigencia = $_GET['vigencia'];
        $consvigencia = $_GET['consvigencia'];
        $fechaf = $_GET['fecha'];
        $conSaldo = false;
        $rpCab = array();
        $rpDet = array();
        $valorReg = array();
        $fecha = '';
        $tercero = [];

        $sqlr = "SELECT consvigencia, fecha, tercero, detalle, idcdp FROM ccpetrp WHERE vigencia = '$vigencia' AND tipo_mov = '201' AND consvigencia = '$consvigencia'";
        $res = mysqli_query($linkbd, $sqlr);
        while($row = mysqli_fetch_row($res)){
            $fecha = $row[1];
            $tercero = buscatercero($row[2]);
            array_push($rpCab, $row);
            
        }

        $sqlrD = "SELECT * FROM ccpetrp_detalle WHERE vigencia = '$vigencia' AND tipo_mov = '201' AND consvigencia = '$consvigencia' ORDER BY cuenta DESC";
        $resD = mysqli_query($linkbd, $sqlrD);
        while($rowD = mysqli_fetch_row($resD)){
            /* generaSaldoRPxcuentaccpet($id_compro,$cuenta,$vigencia,$fuente,$producto='', $indicador_producto='', $bpim='', $codigo_vigenciag='', $seccion_presupuestal, $tipo_gasto); */
            $saldo = generaSaldoRPxcuentaccpet($rowD[2], $rowD[3], $vigencia, $rowD[5], $rowD[4], $rowD[11], $rowD[16], $rowD[14], $rowD[17], $rowD[18]);
            array_push($valorReg, $saldo);

            array_push($rpDet, $rowD);
        }

        $out['rpCab'] = $rpCab;
        $out['rpDet'] = $rpDet;
        $out['nomTercero'] = $tercero;
        $out['valorReg'] = $valorReg;
    }

    if($action == 'validarRpRev'){

        $rpValido = false;
        $fecha = $_GET['fecha'];
        $vigencia = $_GET['vigencia'];
        $consvigencia = $_GET['consvigencia'];
        $sqlr = "SELECT consvigencia, vigencia, detalle FROM ccpetrp WHERE vigencia = '$vigencia' AND tipo_mov = '201' AND consvigencia = '$consvigencia' ";
        $res = mysqli_query($linkbd, $sqlr);
        while($row = mysqli_fetch_row($res)){
            $valorRP = 0;
            $valorRP = generaSaldoRPccpet($row[0],$row[1]);
			if($valorRP>0){
                $rpValido = true;
            }
            
        }

        $out['rpValido'] = $rpValido;
    }

    if($action == 'buscarTercero'){
        
        $nombreTercero = buscatercero($_GET['tercero']);

        $out['nombreTercero'] = $nombreTercero;

    }

    if ($action == "buscaBancoTercero") {

        $documento = $_GET['tercero'];
        $cuentasBanco = array();

        $sqlTercero = "SELECT id_tercero FROM terceros WHERE cedulanit = '$documento'";
        $rowTercero = mysqli_fetch_row(mysqli_query($linkbd, $sqlTercero));

        $sqlCuentasTercero = "SELECT tipo_cuenta, codigo_banco, numero_cuenta FROM teso_cuentas_terceros WHERE id_tercero = $rowTercero[0]";
        $resCuentasTercero = mysqli_query($linkbd, $sqlCuentasTercero);
        while ($rowCuentasTercero = mysqli_fetch_row($resCuentasTercero)) {

            $datos = array();

            $sqlBancos = "SELECT nombre FROM hum_bancosfun WHERE codigo = '$rowCuentasTercero[1]'";
            $rowBancos = mysqli_fetch_row(mysqli_query($linkbd, $sqlBancos));

            array_push($datos, $rowCuentasTercero[0]);
            array_push($datos, $rowCuentasTercero[1]);
            array_push($datos, $rowBancos[0]);
            array_push($datos, $rowCuentasTercero[2]);
            array_push($cuentasBanco, $datos);
        }

        $out['cuentasBanco'] = $cuentasBanco;
    }

    if($action == 'guardarRp'){
       
        $user = $_SESSION['nickusu'];
        $estado = '';
        if($_POST["tipoDeMovimiento"] == '201'){
            $estado = 'S';
            $sqlrUpdateSolicitud = "UPDATE ccp_registro_contrato SET estado = 'A' WHERE codigo = '".$_POST["reg_contrato"]."'";
            mysqli_query($linkbd, $sqlrUpdateSolicitud);
        }else{
            $estado = 'R';
            $sqlr = "UPDATE ccpetrp SET estado = 'R' WHERE consvigencia = '".$_POST["consecutivo"]."' AND vigencia = '".$_POST["vigencia"]."'";
            mysqli_query($linkbd, $sqlr);
        }

        preg_match("/([0-9]{2})\/([0-9]{2})\/([0-9]{4})/", $_POST["fecha"], $fecha);
		$fechaf = $fecha[3]."-".$fecha[2]."-".$fecha[1];

        if($_POST["cdpNomina"] == 'checked'){
            $sqlrRp ="UPDATE hum_nom_cdp_rp SET rp='$_POST[consecutivo]' WHERE cdp='$_POST[cdp]' AND vigencia='$_POST[vigencia]'";
			mysqli_query($linkbd, $sqlrRp);
        }

        $sqlrBanco = "INSERT INTO ccpetrp_banco(vigencia, consvigencia, banco, estado) VALUES('".$_POST["vigencia"]."', '".$_POST["consecutivo"]."', '".$_POST["banco"]."', 'S')";
        mysqli_query($linkbd, $sqlrBanco);

        $sqlr = "INSERT INTO ccpetrp (vigencia, consvigencia, idcdp, estado, fecha, tercero, valor, saldo, contrato, vigenciacdp, tipo_mov, detalle, user) VALUES ('".$_POST["vigencia"]."', '".$_POST["consecutivo"]."', '".$_POST["cdp"]."', '$estado', '$fechaf', '".$_POST["tercero"]."', '".$_POST["totalRp"]."', '".$_POST["totalRp"]."', '".$_POST["contrato"]."', '".$_POST["vigencia"]."', '".$_POST["tipoDeMovimiento"]."', '".$_POST["objeto"]."', '$user')";
        
        if(mysqli_query($linkbd, $sqlr)){
            
            $cantReg = count($_POST["detallesRp"]);
            for($x = 0; $x < $cantReg; $x++){
                
                    $vigencia = '';
                    $consecutivo = '';
                    $rubro = '';
                    $fuente = '';
                    $valor = '';
                    $saldo = '';
                    $indicador_producto = '';
                    $medio_pago = '';
                    $codigo_vigenciag = '';
                    $bpim = '';
                    $seccion_presupuestal = '';
                    $tipo_gasto = '';
                    $divipola = '';
                    $chip = '';
                    $productoservicio = '';
                    $area = '';
                    $detalleSectorial = '';

                    

                    $vigencia = $_POST["vigencia"];
                    $consecutivo = $_POST["consecutivo"];
                    if($estado == 'S'){

                        if(isset( $_POST["detallesRp"][$x][24])){
                            $productoservicio = $_POST["detallesRp"][$x][24];
                            $valor = $_POST["detallesRp"][$x][25];
                            $saldo = $_POST["detallesRp"][$x][25];
                        }else{
                            $valor = $_POST["valorPorReg"][$x];
                            $saldo = $_POST["valorPorReg"][$x];
                            $productoservicio = $_POST["detallesRp"][$x][4];
                        }

                        $rubro = $_POST["detallesRp"][$x][3];
                        $fuente = $_POST["detallesRp"][$x][5];
                        
                        
                        $indicador_producto = $_POST["detallesRp"][$x][11];
                        $medio_pago = $_POST["detallesRp"][$x][12];
                        $codigo_vigenciag = $_POST["detallesRp"][$x][14];
                        $bpim = $_POST["detallesRp"][$x][16];
                        $seccion_presupuestal = $_POST["detallesRp"][$x][17];
                        $tipo_gasto = $_POST["detallesRp"][$x][18];
                        $divipola = $_POST["detallesRp"][$x][19];
                        $chip = $_POST["detallesRp"][$x][20];
                        $area = $_POST["detallesRp"][$x][21];
                        $detalleSectorial = $_POST["detallesRp"][$x][22];
                       
                    }else{
                        /* if(count($_POST["detallesRp"][$x]) > 23){
                            $productoservicio = $_POST["detallesRp"][$x][23];
                            $valor = $_POST["detallesRp"][$x][24];
                        }else{
                            $valor = $_POST["valorPorReg"][$x];
                            $productoservicio = $_POST["detallesRp"][$x][4];
                        } */
                        $valor = $_POST["valorRev"][$x];
                        $productoservicio = $_POST["detallesRp"][$x][4];
                        $rubro = $_POST["detallesRp"][$x][3];
                        $fuente = $_POST["detallesRp"][$x][5];
                        //$valor = $_POST["valorRev"][$x];
                        $saldo = $_POST["valorRev"][$x];
                        $indicador_producto = $_POST["detallesRp"][$x][11];
                        $medio_pago = $_POST["detallesRp"][$x][12];
                        $codigo_vigenciag = $_POST["detallesRp"][$x][14];
                        $bpim = $_POST["detallesRp"][$x][16];
                        $seccion_presupuestal = $_POST["detallesRp"][$x][17];
                        $tipo_gasto = $_POST["detallesRp"][$x][18];
                        $divipola = $_POST["detallesRp"][$x][19];
                        $chip = $_POST["detallesRp"][$x][20];
                        $area = $_POST["detallesRp"][$x][21];
                        $detalleSectorial = $_POST["detallesRp"][$x][22];
                        //$productoservicio = $_POST["detallesRp"][$x][4];
                    }
                
              

                $sqlrD = "INSERT INTO ccpetrp_detalle(vigencia, consvigencia, cuenta, productoservicio, fuente, valor, estado, saldo, tipo_mov, indicador_producto, medio_pago, codigo_vigenciag, bpim, seccion_presupuestal, tipo_gasto, divipola, chip, area, detalle_sectorial) VALUES ('".$vigencia."', '".$consecutivo."', '".$rubro."', '".$productoservicio."', '".$fuente."', '".$valor."', '$estado', '".$saldo."', '".$_POST["tipoDeMovimiento"]."', '".$indicador_producto."', '".$medio_pago."', '".$codigo_vigenciag."', '$bpim', '".$seccion_presupuestal."', '".$tipo_gasto."', '".$divipola."', '".$chip."',  '".$area."','".$detalleSectorial."')";
                mysqli_query($linkbd, $sqlrD);
            }
            $out['insertaBien'] = true;
        }else{
            $out['insertaBien'] = false;
        }

    }


    header("Content-type: application/json");
    echo json_encode($out);
    die();