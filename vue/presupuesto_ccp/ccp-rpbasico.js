var app = new Vue({
    el: '#myapp',
	data:{
        tiposDeMovimiento: [],
        tipoMovimiento: '',
        mostrarCrearRp : false,
        mostrarReversionrp : false,
        consecutivo : 0,
        contrato: '',
        checked: false,
        cdp: '',

        showModal_cdp: false,
        cdps: [],
        searchCdp : {keywordCdp: ''},

        fechaCdp: '',
        solicita: '',
        objeto: '',

        detallesRp: '',
        detallesCdp: '',

        cdpCab: [],
        cdpDet: [],



        show_table_search: false,
        
        valorTotal: 0,

        searchGeneral: {keywordGeneral: ''},

        valorReg: [],
        valorRegOriginal : [],
        indicador : [],

        loading: false,
        error: "",

        nameCuenta: "",
        codigoCuenta: "",
        showBancos: false,
        listaBancos: "",

        // variables de reversion
        numRpRev: '',
        descripcionRev: '',
        fechaRp: '',
        terceroRp: '',
        objetoRp: '',
        detallesRev: [],
        nombreTerceroRev: '',

        showModal_rps_rev: false,
        rps_rev: [],
        rpCabRev: [],
        valorRev: [],
        searchRpRev : {keywordRpRev: ''},

        origen: '-1',
        origenes: [
            {value: 'cdp_basico', text: 'CDP Basico'},
            {value: 'registro_contrato', text: 'Registro de Contrato'},
        ],

        show_cdp_basico: false,
        show_registro_contrato: false,

        reg_contrato: '',
        showModal_reg_contrato: false,
        reg_contratos: [],
        reg_contratosCopy: [],
        tercero: '',

        vigencias_delgasto: [],
        dependencias: [],
        cuentas_ccpet: [],
        proyectos: [],
        fuentes: [],
        cuentaConCpc: '',

        showModal_cpc : false,
        catalogo_cpc : [],
        cuenta_ccpet_cpc : '',
        searchGeneral: {keywordGeneral: ''},
        codigo_cpc_seleccionados: [],
        valorGastoCpc: [],

        cpc_con_valor: [],

        valorRubroSel: 0,
        totalCpc: 0,

        fechaIni: '',
        fechaFin: '',
        txtSearch: '',
    },

    mounted: async function(){

        await this.cargarFecha();
        this.cargarTiposDeMovientoUsuario();
        this.vigenciasDelgasto();
        this.cargarDependencias();
        this.cargarCuentasCcpet();
        
    },

    methods: {

        cargarTiposDeMovientoUsuario: async function(){
            await axios.post('vue/presupuesto_ccp/ccp-cdpbasico.php')
            .then((response) => {
                //console.log(response.data);
                app.tiposDeMovimiento = response.data.tiposDeMovimiento;

                if(response.data.tiposDeMovimiento.length > 0){
                    this.tipoMovimiento = response.data.tiposDeMovimiento[0][0];
                }

            });

            this.seleccionarFormulario();
        },

        cargarCuentasCcpet: async function(){
            await axios.post('vue/presupuesto_ccp/ccp-rppbasico.php?action=cuentasCcpet')
            .then(
                (response) => {
                    this.cuentas_ccpet = response.data.cuentasCppet;
                    this.fuentes = response.data.fuentes;
                    this.proyectos = response.data.proyectos;
                }
            );
        },

        vigenciasDelgasto: async function(){
            await axios.post('vue/presupuesto_ccp/ccp-inicialgastosfun.php?action=vigenciasDelGasto')
            .then(
                (response) => {
                    this.vigencias_delgasto = response.data.vigenciasDelGasto;
                }
            );
        },

        cargarDependencias: async function(){
            await axios.post('vue/presupuesto_ccp/ccp-inicialgastosfun.php?action=secciones')
            .then(
                (response) => {
                    this.dependencias = response.data.secciones;
                }
            );
        },

        buscarDependencia: function(dependencia){
            if(dependencia == 0 || dependencia == null || dependencia == undefined || dependencia == ''){
                return '';
            }
            return this.dependencias.find(dep => dep[0] == dependencia)[1];
        },

        buscarVigencia: function(vigencia_gasto){
            if(vigencia_gasto == 0 || vigencia_gasto == null || vigencia_gasto == undefined || vigencia_gasto == ''){
                return '';
            }
            return this.vigencias_delgasto.find(vig => vig[1] == vigencia_gasto)[2];
        },

        buscarCuentaCcp: function(cuenta){
            if(cuenta == 0 || cuenta == null || cuenta == undefined || cuenta == ''){
                return '';
            }
            return this.cuentas_ccpet.find(c => c[0] == cuenta)[1];
        },

        buscarFuente: function(fuente){
            if(fuente == 0 || fuente == null || fuente == undefined || fuente == ''){
                return '';
            }
            return this.fuentes.find(f => f[0] == fuente)[1];
        },

        buscarBpim: function(bpim){
            if(bpim == 0 || bpim == null || bpim == undefined || bpim == ''){
                return '';
            }
            return this.proyectos.find(f => f[0] == bpim)[1];
        },

        selectOrigen: function(){
            if(this.origen == 'cdp_basico'){
                this.show_cdp_basico = true;
                this.show_registro_contrato = false;
            }else if(this.origen == 'registro_contrato'){
                this.show_cdp_basico = false;
                this.show_registro_contrato = true;
            }
        },

        

        seleccionarFormulario: function(){

            this.fechaRp = '';
            this.numRpRev = '';
            this.terceroRp = '';
            this.nombreTerceroRev = '';
            this.objetoRp = '';
            this.detallesRev = [];
            this.valorRev = [];

            if(this.tipoMovimiento == '201'){
                this.mostrarCrearRp = true;
                this.mostrarReversionrp = false;
                this.seleccioncarConsecutivo();
                this.seleccionarOrigenRp();
            }else if(this.tipoMovimiento == '401' || this.tipoMovimiento == '402'){
                this.mostrarReversionrp = true;
                this.mostrarCrearRp = false;
            }else{

            }
        },

        seleccionarOrigenRp: function(){
            this.loading = true;
            axios.post('vue/presupuesto_ccp/ccp-rppbasico.php?action=seleccionarOrigenRp')
            .then((response) => {
                if(response.data.origen == 'Dptal'){
                    
                    this.show_cdp_basico = false;
                    this.show_registro_contrato = true;
                    
                }else{
                    this.show_cdp_basico = true;
                    this.show_registro_contrato = false;
                }
            }).finally(() => {
                this.loading =  false;
            });
        },

        seleccioncarConsecutivo: function(){

            this.loading = true;
            var vig = document.getElementById('vigencia').value;
            axios.post('vue/presupuesto_ccp/ccp-rppbasico.php?action=seleccionarConsecutivo&vigencia=' + vig)
            .then((response) => {
                app.consecutivo = response.data.consecutivo;
            }).finally(() => {
                this.loading =  false;
            });

        },

        cargarFecha: function(){
            
            const fechaAct = new Date().toJSON().slice(0,10).replace(/-/g,'/');
            const fechaArr = fechaAct.split('/');
            const fechaV = fechaArr[2]+'/'+fechaArr[1]+'/'+fechaArr[0];
            document.getElementById('fecha').value = fechaV;
            document.getElementById('vigencia').value = fechaArr[0];
            //this.vigencia = fechaArr[0];
        },

        ventanaCdp: async function(){

            
            this.loading = true;
            const fechat = document.getElementById('fecha').value;
            const fechatAr = fechat.split('/');
            const fechaf = fechatAr[2]+'-'+fechatAr[1]+'-'+fechatAr[0];
            var vig = document.getElementById('vigencia').value;
            await  axios.post('vue/presupuesto_ccp/ccp-rppbasico.php?action=cargarCdps&vigencia=' + vig + '&fecha=' + fechaf)
                .then((response) => {
                    app.showModal_cdp = true;
                    app.cdps = response.data.cdps;
                }).finally(() => {
                    this.loading =  false;
                });
            this.seleccioncarConsecutivo();
        },

        ventanaRegistroContrato: async function(){

            this.loading = true;
            const fechat = document.getElementById('fecha').value;
            const fechatAr = fechat.split('/');
            const fechaf = fechatAr[2]+'-'+fechatAr[1]+'-'+fechatAr[0];
            var vig = document.getElementById('vigencia').value;
            await  axios.post('vue/presupuesto_ccp/ccp-rppbasico.php?action=cargarRegContratos&vigencia=' + vig + '&fecha=' + fechaf)
                .then((response) => {
                    console.log(response.data);
                    this.showModal_reg_contrato = true;
                    this.reg_contratos = this.reg_contratosCopy = response.data.regContratos;
                }).finally(() => {
                    this.loading =  false;
                });
            this.seleccioncarConsecutivo();
        },

        seleccionarRegContrato: async function(reg_contrato_selecionado){
            this.reg_contrato = reg_contrato_selecionado.codigo;
            this.showModal_reg_contrato = false;
            this.cdp = reg_contrato_selecionado.cdp;
            this.contrato = reg_contrato_selecionado.contrato;
            this.tercero = reg_contrato_selecionado.tercero;
            this.consecutivo = reg_contrato_selecionado.cdp;
            document.getElementById('tercero').value = reg_contrato_selecionado.tercero;

            this.buscarCdp();
            this.buscarTercero();
            
        },

        buscarCdp: async function(){
            if(this.cdp != ''){
                const fechat = document.getElementById('fecha').value;
                const fechatAr = fechat.split('/');
                const fechaf = fechatAr[2]+'-'+fechatAr[1]+'-'+fechatAr[0];
                var vig = document.getElementById('vigencia').value;
                this.loading = true;
                await axios.post('vue/presupuesto_ccp/ccp-rppbasico.php?action=buscarCdp&cdp=' + this.cdp + '&vigencia=' + vig + '&fecha=' + fechaf)
                .then((response) => {
                    if(response.data.conSaldo){
                        app.buscaInformacionCdp();
                    }else{
                        Swal.fire(
                            'El cdp no tiene saldo.',
                            'Este cdp no exites o no tiene saldo.',
                            'warning'
                            ).then((result) => {
                                this.cdp = '';
                                this.fechaCdp = '';
                                this.solicita = '';
                                this.objeto = '';
                                this.detallesCdp = [];  
                            });
                    }
                    
                }).finally(() => {
                    this.loading =  false;
                });
                
            }else{
                
                this.fechaCdp = '';
                this.solicita = '';
                this.objeto = '';
                this.detallesCdp = [];
            }
            this.seleccioncarConsecutivo();
            //this.parametrosCompletos();
        },

        buscarTercero: async function(){
            
            let tercero = document.getElementById('tercero').value;

            if(tercero != ''){
                this.loading = true;
                await axios.post('vue/presupuesto_ccp/ccp-rppbasico.php?action=buscarTercero&tercero=' + tercero)
                .then((response) => {
                    
                    if(response.data.nombreTercero){
                        document.getElementById('ntercero').value = response.data.nombreTercero;
                    }else{
                        Swal.fire(
                            'El tercero no existe.',
                            'Este tercero no esta creado en el sistema.',
                            'warning'
                            ).then((result) => {
                                document.getElementById('tercero').value = '';
                                document.getElementById('ntercero').value = '';
                            });
                    }
                    
                }).finally(() => {
                    this.loading =  false;
                });
                
            }else{
                
                document.getElementById('ntercero').value = '';
            }
        },

        /* buscarTercero: async function(){
            
            let tercero = document.getElementById('tercero').value;

            if(tercero != ''){
                
                await axios.post('vue/presupuesto_ccp/ccp-rppbasico.php?action=buscarTercero&tercero=' + tercero)
                .then((response) => {
                    
                    if(response.data.nombreTercero){
                        document.getElementById('ntercero').value = response.data.nombreTercero;
                    }else{
                        Swal.fire(
                            'El tercero no existe.',
                            'Este tercero no esta creado en el sistema.',
                            'warning'
                            ).then((result) => {
                                document.getElementById('tercero').value = '';
                                document.getElementById('ntercero').value = '';
                            });
                    }
                    
                });
                
            }else{
                
                document.getElementById('ntercero').value = '';
            }
        }, */

        searchMonitorCdp: function(){
            var keywordCdp = app.toFormData(app.searchCdp);

            const fechat = document.getElementById('fecha').value;
            const fechatAr = fechat.split('/');
            const fechaf = fechatAr[2]+'-'+fechatAr[1]+'-'+fechatAr[0];
            var vig = document.getElementById('vigencia').value;
            axios.post('vue/presupuesto_ccp/ccp-rppbasico.php?action=filtrarCdps&vigencia=' + vig + '&fecha=' + fechaf, keywordCdp)
            .then((response) => {
               app.cdps = response.data.cdps;
                
            });
        },


        seleccionarCdp: function(cdpSelec){
            this.cdp = cdpSelec [0];
            this.showModal_cdp = false;
            this.buscaInformacionCdp();
        },

        buscaInformacionCdp: function(){
            var vig = document.getElementById('vigencia').value;

            const fechat = document.getElementById('fecha').value;
            const fechatAr = fechat.split('/');
            const fechaf = fechatAr[2]+'-'+fechatAr[1]+'-'+fechatAr[0];
            this.loading = true;
            axios.post('vue/presupuesto_ccp/ccp-rppbasico.php?action=buscaInformacionCdp&vigencia=' + vig + '&consvigencia=' +  this.cdp + '&fecha=' + fechaf)
            .then((response) => {
                /* console.log(response.data); */
                app.cdpCab = response.data.cdpCab;
                app.detallesCdp = response.data.cdpDet;
                app.valorReg = response.data.valorReg;
                app.llenarInformacionCdp();
                app.validarSiEsDeNomina();
                //app.consecutivo = response.data.consecutivo;
            }).finally(() => {
                this.loading =  false;
            });
        },

        llenarInformacionCdp: function(){
            for(var x = 0; x < this.cdpCab.length; x++){
                this.fechaCdp = this.cdpCab[x][1];
                this.solicita = this.cdpCab[x][2];
                this.objeto = this.cdpCab[x][3];
            }
            //console.log(this.detallesCdp);
        },

        validarSiEsDeNomina: function(){
            var vig = document.getElementById('vigencia').value;
            axios.post('vue/presupuesto_ccp/ccp-rppbasico.php?action=buscarSiEsDeNomina&cdp=' + this.cdp + '&vigencia=' + vig)
                .then((response) => {
                    if(response.data.cdpDeNomina){
                        this.checked = 'checked';
                    }else{
                        this.checked = '';
                    }
                    
                });

        },

        cambiarValor: function(index){
            
            if(this.indicador[index] == undefined){
                
                this.valorRegOriginal[index] = this.valorReg[index]; 

            }

            Swal.fire({
                title: 'Digite el valor:',
                input: 'number',
                inputLabel: 'decimales separados por punto. ',
                width: 300,
                inputAttributes: {
                  autocapitalize: 'off',
                  step: 'any'
                },
                
                showCancelButton: true,
                confirmButtonText: 'Aceptar',
                showLoaderOnConfirm: true,
                preConfirm: (valorDelReg) => {
                    
                    if(parseFloat(valorDelReg) > parseFloat(this.valorRegOriginal[index])){
                        Swal.showValidationMessage(
                            `El valor no puede ser mayor al disponible. `
                            //`El valor no puede ser mayor al disponible: ${error}`
                          )
                    }else{
                        this.indicador[index] = 1;
                        this.valorReg[index] = valorDelReg;
                        this.valorReg = Object.values(this.valorReg);
                        this.totalRp();
                    }
                },
                allowOutsideClick: () => !Swal.isLoading()
            });

        },

        verificarCpc: function(index, cuenta_ccpet_v){

            if(cuenta_ccpet_v == '2.1.1.01.01.001.10'){
                Swal.fire("Atención!","Esta cuenta no requiere CPC","warning");
            }else{
                axios.post('presupuesto_ccpet/solicitudcdp/ccp-solicitudcdp.php?action=tieneCpc&cuenta=' + cuenta_ccpet_v)
                    .then((response) => {

                    if(response.data.tieneCpc == null){
                        app.cuentaConCpc = '';
                        this.cuenta_ccpet_cpc = '';
                        this.valorRubroSel = 0;
                    }else{
                        this.cuenta_ccpet_cpc = cuenta_ccpet_v;
                        this.valorRubroSel = this.valorReg[index];
                        app.cuentaConCpc = response.data.tieneCpc;
                    }

                }).finally(() => {
                    this.loading =  false;
                    if(this.cuentaConCpc !== ''){
                        this.buscaCatalogoCpc();
                    }else{
                        Swal.fire("Atención!","Esta cuenta no requiere CPC","warning");
                    }
                });
            }
            
        },

        verificaCpcGuardar: async function(cuenta_buscar){
            let result = false;
            if(cuenta_buscar == '2.1.1.01.01.001.10')
                return result;
            await axios.post('presupuesto_ccpet/solicitudcdp/ccp-solicitudcdp.php?action=tieneCpc&cuenta=' + cuenta_buscar)
                    .then((response) => {

                    if(response.data.tieneCpc !== null){
                        result = true
                    }

                }).finally(() => {
                    /* this.loading =  false;
                    if(this.cuentaConCpc !== ''){
                        this.buscaCatalogoCpc();
                    } */
                });
            return result;
        },

        searchGeneralCpc: function(){
            var keywordGeneral = app.toFormData(app.searchGeneral);
            let ultimoDigito = this.cuenta_ccpet_cpc.substr(-1,1);
            if(this.cuentaConCpc == '3'){
                axios.post('vue/ccp-servicios.php?action=searchGeneral1&ultimoDigito='+ultimoDigito, keywordGeneral)
                    .then(function(response){
                    console.log(response.data);
                    app.catalogo_cpc = response.data.subClasesGeneral;
                })
            }else{
                axios.post('vue/ccp-bienestransportables.php?action=searchGeneral&ultimoDigito='+ultimoDigito, keywordGeneral)
                    .then(function(response){
                        app.catalogo_cpc = response.data.subClasesGeneral;
                })
            }
        },

        seleccionarCpc: function(cpc_sel, cpc_sel_nombre){
            //agregar al array de codigo_cpc_seleccionados y verificar que no se repita
            //verificar que no se repita
            const cpc_sel_ag = [cpc_sel, cpc_sel_nombre];

            if(this.codigo_cpc_seleccionados.includes(cpc_sel_ag)){
                this.codigo_cpc_seleccionados.splice(this.codigo_cpc_seleccionados.indexOf(cpc_sel),1);
            }

            
            this.codigo_cpc_seleccionados.push(cpc_sel_ag);
        },

        

        verificarSaldos: function(){
            if(this.codigo_cpc_seleccionados.length > 0){
                if(this.totalCpc == this.valorRubroSel){
                    this.showModal_cpc = false;
                    /* this.cpc_con_valor = []; */
                    this.totalCpc = 0;
                    this.cuenta_ccpet_cpc = '';
                    this.cuentaConCpc = '';
                    this.valorRubroSel = 0;
                }
                else{
                    this.showModal_cpc = true;
                    Swal.fire("Error", "El valor total de los codigos no coincide con el valor del rubro", "warning");
                }
            }else{
                this.showModal_cpc = false;
            }
        },

        buscaCatalogoCpc: function(){
            if(this.cuentaConCpc !== ''){
                let ultimoDigito = this.cuenta_ccpet_cpc.substr(-1,1);
                if(this.cuentaConCpc == '3'){
                    
                    this.fetchMembersServicios(ultimoDigito);
                    
                }else{
                    
                    this.fetchMembersBienes(ultimoDigito);
                }
                this.verificaCpcsSeleccionados();
                this.showModal_cpc = true;
            }
        },

        calcularTotal: function(codigo_cpc, valor_cpc, nombre_cpc){

            valor_cpc = valor_cpc === '' ? 0 : parseFloat(valor_cpc).toFixed(2);

            objCpc = {
                cuenta_ccpet_cpc: this.cuenta_ccpet_cpc,
                codigo_cpc: codigo_cpc,
                nombre_cpc: nombre_cpc,
                valor_cpc: valor_cpc
            };
            //quitar objeto del array cpc_con_valor cuando el valor del cpc es 0
            let index = this.cpc_con_valor.findIndex(cpc => cpc.codigo_cpc === codigo_cpc && cpc.cuenta_ccpet_cpc === this.cuenta_ccpet_cpc);
            
            if (index !== -1) {
                this.cpc_con_valor.splice(index, 1);
            }
            if(valor_cpc > 0 ){
                this.cpc_con_valor.push(objCpc);
            }
            //this.cpc_con_valor.push(objCpc);
            this.calcularValorCpc();
        },

        verificaCpcsSeleccionados: function(){
            //let objCpc = {};
            this.codigo_cpc_seleccionados = [];
            let cpc_sel_ag = [];
            this.totalCpc = 0;
            
            this.cpc_con_valor.forEach((cpc) => {
                if(cpc.cuenta_ccpet_cpc === this.cuenta_ccpet_cpc) {
                    const cpc_sel_ag = [cpc.codigo_cpc, cpc.nombre_cpc];
                    this.codigo_cpc_seleccionados.push(cpc_sel_ag);
                    this.totalCpc += parseFloat(cpc.valor_cpc);
                }
            });
        },

        eliminarCpc: function(codigo_cpc_e){
            
            let index = this.cpc_con_valor.findIndex(cpc => cpc.codigo_cpc === codigo_cpc_e && cpc.cuenta_ccpet_cpc === this.cuenta_ccpet_cpc);
            if (index !== -1) {
                this.cpc_con_valor.splice(index, 1);
            }

            let ind = this.codigo_cpc_seleccionados.findIndex(cpc => cpc[0] === codigo_cpc_e);
            if (ind !== -1) {
                this.codigo_cpc_seleccionados.splice(ind, 1);
            }
            this.calcularValorCpc();
        },

        calcularValorCpc: function(){
            let total = 0;
            this.cpc_con_valor.forEach (function(cpc){
                if(cpc.cuenta_ccpet_cpc == app.cuenta_ccpet_cpc){
                    total += parseFloat(cpc.valor_cpc);
                }
                //total += parseFloat(cpc.valor_cpc);
            });
            this.totalCpc = total;
        },

        buscarCpcAgregados: function(cuenta_ccpet_cpc_v){
            let cpcMostrar = '';
            let cpc = this.cpc_con_valor.filter(cpc => cpc.cuenta_ccpet_cpc === cuenta_ccpet_cpc_v);
            for(let i = 0; i <= cpc.length-1; i++){
                cpcMostrar += cpc[i].codigo_cpc + ' - ' + cpc[i].nombre_cpc + '  ';
                //console.log(cpc[i].codigo_cpc);
            }
            return cpcMostrar;
        },

        buscarCpcAgregadosGuardar: function(cuenta_ccpet_cpc_v){
            let cpcMostrar = '';
            let cpc = this.cpc_con_valor.filter(cpc => cpc.cuenta_ccpet_cpc === cuenta_ccpet_cpc_v);
            for(let i = 0; i <= cpc.length-1; i++){
                if(i == cpc.length-1){
                    cpcMostrar += cpc[i].codigo_cpc + '-' + cpc[i].valor_cpc;
                }else{
                    cpcMostrar += cpc[i].codigo_cpc + '-' + cpc[i].valor_cpc + ',';
                }
                //cpcMostrar += cpc[i].codigo_cpc +  '  ';
                //console.log(cpc[i].codigo_cpc);
            }
            return cpcMostrar;
        },

        fetchMembersServicios: function(ultimoDigito){
            axios.post('vue/ccp-servicios.php?action=searchGeneral&ultimoDigito='+ultimoDigito)
                .then(function(response){
                    app.catalogo_cpc = response.data.subClasesGeneral;
                    //app.seccionesServicios = response.data.secciones;
                });
        },

        fetchMembersBienes: function(ultimoDigito){
            axios.post('vue/ccp-bienestransportables.php?action=searchGeneral&ultimoDigito='+ultimoDigito)
                .then(function(response){
                    app.catalogo_cpc = response.data.bienes_transportables;
                    //app.secciones = response.data.secciones;
                });
        },

        totalRp: function(){
            
            let total = 0;
            this.valorReg.forEach (function(numero){
                total += parseFloat(numero);
            });
            return total;
            //console.log():
        },


        guardarRp: async function(){
            
            if(this.tipoMovimiento == '201'){

                if(document.getElementById('fecha').value != '' && this.contrato != '' && this.cdp != '' && document.getElementById('tercero').value != '' && this.objeto != '' && this.consecutivo != '' && this.detallesCdp.length > 0 && this.codigoCuenta != ""){

                    if(this.reg_contrato != ''){

                        for(let i=0; i <= this.detallesCdp.length-1; i++){
                            let resultCpc = await this.verificaCpcGuardar(this.detallesCdp[i][3]);
                            let cpcDetallesCdp = this.detallesCdp[i][4];
                            if(resultCpc && cpcDetallesCdp == ''){
                                if(this.cpc_con_valor.filter(cpc => cpc.cuenta_ccpet_cpc === this.detallesCdp[i][3]).length == 0 && this.valorReg[i] > 0){
                                    
                                    Swal.fire("Atención!","Falta agregar CPC","warning");
                                    return false;
                                    
                                }
                            }

                        }
                    }
                    Swal.fire({
                            title: 'Esta seguro de guardar?',
                            text: `Guardar rp en la base de datos, confirmar campos`,
                            icon: 'question',
                            showCancelButton: true,
                            confirmButtonColor: '#3085d6',
                            cancelButtonColor: '#d33',
                            confirmButtonText: 'Si, guardar!'
                        }).then((result) => {
                        if (result.isConfirmed) {
                            var formData = new FormData();

                            let contador = 0;
                            let contador2 = 0;
                            
                            for(let i=0; i <= this.detallesCdp.length-1; i++){
                                const val = Object.values(this.detallesCdp[i]).length;
                                
                                /* console.log(Object.values(this.detallesCdp[i])[3]); */
                                let buscarCpcAd = this.buscarCpcAgregadosGuardar(Object.values(this.detallesCdp[i])[3]);
                                if(buscarCpcAd != ''){
                                    formData.append("valorPorReg["+i+"]", this.valorReg[i]);
                                    let partsCpcAd = buscarCpcAd.split(',');
                                    let cont = partsCpcAd.length-1;
                                    for(let zz = 0; zz <= partsCpcAd.length-1; zz++){
                                        for(let x = 0; x <= val; x++){
                                            formData.append("detallesRp["+contador2+"][]", Object.values(this.detallesCdp[i])[x]);
                                        
                                        }
                                        let partsValor = partsCpcAd[zz].split('-');
                                        formData.append("detallesRp["+contador2+"][]", partsValor[0]);
                                        formData.append("detallesRp["+contador2+"][]", parseInt(partsValor[1]));
                                        contador++;
                                        contador2++;
                                    }
                                    contador--;
                                }else{
                                    let pos = i + contador;
                                    for(let x = 0; x <= val; x++){

                                        formData.append("detallesRp["+pos+"][]", Object.values(this.detallesCdp[i])[x]);
                                    
                                    }
                                    formData.append("valorPorReg["+pos+"]", this.valorReg[i]);
                                }
                                
                                    /* console.log(this.cpc_con_valor);
                                    valDet = this.cpc_con_valor;
                                    console.log(valDet);
                                    if(valDet.length > 0){
                                        for(let y = 0; y <= valDet.length-1; y++){
                                            formData.append("detallesRp["+i+"][]", Object.values(this.cpc_con_valor)[y]);
                                        }
                                    } */
                                
                                

                                /* if(this.cpc_con_valor.length > 0){
                                    for(let y = 0; y <= this.cpc_con_valor.length-1; y++){

                                        let valCpc = Object.values(this.cpc_con_valor[y]);

                                        console.log(valCpc[0], Object.values(this.detallesCdp[i])[3]);
                                        
                                        if(valCpc[0] == Object.values(this.detallesCdp[i])[3]){
                                            for(let zz = 0; zz < valCpc.length; zz++){
                                                formData.append("cpcCpcet["+i+"][]", Object.values(this.cpc_con_valor[y])[zz]);
                                            }
                                            
                                        }
                                    }
                                } */
                                
                            }
                            var vig = document.getElementById('vigencia').value;
                            formData.append("consecutivo", this.consecutivo);
                            formData.append("cdp", this.cdp);
                            formData.append("vigencia", vig);
                            formData.append("contrato", this.contrato);
                            formData.append("fecha", document.getElementById('fecha').value);
                            formData.append("tercero", document.getElementById('tercero').value);
                            formData.append("banco", this.codigoCuenta);
                            formData.append("solicita", this.solicita);
                            formData.append("objeto", this.objeto);
                            formData.append("totalRp", this.totalRp());
                            formData.append("tipoDeMovimiento", this.tipoMovimiento);
                            formData.append("cdpNomina", this.checked);
                            formData.append("reg_contrato", this.reg_contrato);
                            
                            axios.post('vue/presupuesto_ccp/ccp-rppbasico.php?action=guardarRp', formData)
                            .then((response) => {
                                console.log(response.data);
                                if(response.data.insertaBien){
                                    Swal.fire({
                                        position: 'top-end',
                                        icon: 'success',
                                        title: 'El rp se guard&oacute; con Exito',
                                        showConfirmButton: false,
                                        timer: 1500
                                    }).then((response) => {
                                            app.redireccionar();
                                        });
                                }else{
                                    
                                    Swal.fire(
                                        'Error!',
                                        'No se pudo guardar.',
                                        'error'
                                    );
                                }
                                
                            });
                            
                        }
                    });
                    
                }else{
                    Swal.fire(
                        'Falta informaci&oacute;n para guardar el rp.',
                        'Verifique que todos los campos esten diligenciados.',
                        'warning'
                    );
                }
            }else{
                if(document.getElementById('fc_1198971545').value != '' && this.numRpRev != '' && this.descripcionRev != '' && this.detallesRev.length > 0){

                    Swal.fire({
                            title: 'Esta seguro de reversar?',
                            text: "Reversar rp en la base de datos, confirmar campos!",
                            icon: 'question',
                            showCancelButton: true,
                            confirmButtonColor: '#3085d6',
                            cancelButtonColor: '#d33',
                            confirmButtonText: 'Si, reversar!'
                        }).then((result) => {
                        if (result.isConfirmed) {
                            var formData = new FormData();

                            for(let i=0; i <= this.detallesRev.length-1; i++){
                                const val = Object.values(this.detallesRev[i]).length;
                                
                                for(let x = 0; x <= val; x++){
                                    formData.append("detallesRp["+i+"][]", Object.values(this.detallesRev[i])[x]);
                                    
                                }
                                formData.append("valorRev["+i+"]", this.valorRev[i]);
                                
                            }
                            
                            /* var vig = document.getElementById('vigencia').value; */
                            const fechat = document.getElementById('fc_1198971545').value;
                            const fechatAr = fechat.split('/');
                            var vig = fechatAr[2];

                            formData.append("consecutivo", this.numRpRev);
                            formData.append("cdp", this.cdpRev);
                            formData.append("vigencia", vig);
                            formData.append("contrato", '');
                            formData.append("fecha", document.getElementById('fc_1198971545').value);
                            formData.append("tercero", this.terceroRp);
                            formData.append("banco", '');
                            formData.append("solicita", '');
                            formData.append("objeto", this.descripcionRev);
                            formData.append("totalRp", 0);
                            formData.append("tipoDeMovimiento", this.tipoMovimiento);
                            formData.append("cdpNomina", this.checked);
                            
                            axios.post('vue/presupuesto_ccp/ccp-rppbasico.php?action=guardarRp', formData)
                            .then((response) => {
                                
                                if(response.data.insertaBien){
                                    Swal.fire({
                                        position: 'top-end',
                                        icon: 'success',
                                        title: 'El rp se revers&oacute;; con Exito',
                                        showConfirmButton: false,
                                        timer: 1500
                                    }).then((response) => {
                                            app.redireccionar();
                                        });
                                }else{
                                    
                                    Swal.fire(
                                        'Error!',
                                        'No se pudo guardar.',
                                        'error'
                                    );
                                }
                                
                            });
                            
                        }
                    });
                    
                }else{
                    Swal.fire(
                        'Falta informaci&oacute;n para reversar el cdp.',
                        'Verifique que todos los campos esten diligenciados.',
                        'warning'
                    );
                }
            }
            
        },

        buscarValorInversion: function(index){
            return this.cuentaInversion[index];
        },


        searchMonitorCuenta: function(){
            var keywordCuenta = app.toFormData(app.searchCuenta);
            this.loading = true;
            axios.post('vue/presupuesto_ccp/ccp-rppbasico.php?action=filtrarCuentasInversion', keywordCuenta)
            .then((response) => {   
                app.cuentasCcpetInversion = response.data.cuentasInversion;
            }).finally(() => {
                this.loading =  false;
            });
        },

        toFormData: function(obj){
            var form_data = new FormData();
            for(var key in obj){
                form_data.append(key, obj[key]);
            }
            return form_data;
        },

        redireccionar: function(){
            let rp = 0;
            let vig = 0; 
            rp = this.consecutivo;
            if(this.tipoMovimiento == '201'){
                rp = this.consecutivo;
                vig = document.getElementById('vigencia').value;
            }else{
                const fechat = document.getElementById('fc_1198971545').value;
                const fechatAr = fechat.split('/');
                vig = fechatAr[2];
                rp = this.numRpRev;
            }

            location.href ="ccp-rpVisualizar.php?is="+rp+"&vig="+vig;
        },

        cuentaBancariaTercero: async function() {

            let tercero = document.getElementById('tercero').value;

            if (tercero != "") {

                this.loading = true;

                await axios.post('vue/presupuesto_ccp/ccp-rppbasico.php?action=buscaBancoTercero&tercero='+tercero)
                .then((response) => {
                   app.listaBancos = response.data.cuentasBanco;
                }).catch((error) => {
                    this.error = true;
                    console.log(error)
                }).finally(() => {
                    this.loading = false;
                    this.showBancos = true;
                });  
            }
            else {
                Swal.fire("Error", "Seleccionar primero el tercero", "warning");
            }
        },

        seleccionaBanco: function(banco) {
            this.nameCuenta = banco[3] + " " + banco[0] + " " + banco[2];
            this.codigoCuenta = banco[3];
            this.showBancos = false;
        },

        formatonumero: function(valor){
			return new Intl.NumberFormat("en-US", {style: "currency", currency: "USD"}).format(valor);
		},

        //Metodos de reversion
        ventanaRpRev: async function(){

            const fechat = document.getElementById('fc_1198971545').value;
            if(fechat != ''){
                const fechatAr = fechat.split('/');
                const fechaf = fechatAr[2]+'-'+fechatAr[1]+'-'+fechatAr[0];
                var vig = fechatAr[2];
                this.loading = true;
                await  axios.post('vue/presupuesto_ccp/ccp-rppbasico.php?action=cargarRps&vigencia=' + vig + '&fecha=' + fechaf)
                    .then((response) => {
                        app.showModal_rps_rev = true;
                        app.rps_rev = response.data.rps;
                    }).finally(() => {
                        this.loading =  false;
                        this.seguirAgregandoDet();
                    });
            }else{
                Swal.fire(
                    'Falta la fecha de reversi&oacute;n.',
                    'Falta Escoger la fecha de reversi&oacute;n.',
                    'warning'
                );
            }
        },

        seleccionarRpRev: function(rpRev){
            
            this.numRpRev = rpRev [0];
            this.showModal_rps_rev = false;
            this.buscaInformacionRpRev();
        },

        buscaInformacionRpRev: function(){

            const fechat = document.getElementById('fc_1198971545').value;
            const fechatAr = fechat.split('/');
            const fechaf = fechatAr[2]+'-'+fechatAr[1]+'-'+fechatAr[0];
            var vig = fechatAr[2];
            this.loading = true;
            axios.post('vue/presupuesto_ccp/ccp-rppbasico.php?action=buscaInformacionRp&vigencia=' + vig + '&consvigencia=' +  this.numRpRev + '&fecha=' + fechaf)
            .then((response) => {
                console.log(response.data);
                app.rpCabRev = response.data.rpCab;
                app.detallesRev = response.data.rpDet;
                app.valorRev = response.data.valorReg;
                app.nombreTerceroRev = response.data.nomTercero;
                app.llenarInformacionRpRev();
                //app.consecutivo = response.data.consecutivo;
            }).finally(() => {
                this.loading =  false;
            });
        },

        llenarInformacionRpRev: function(){
            for(var x = 0; x < this.rpCabRev.length; x++){
                this.fechaRp = this.rpCabRev[x][1];
                this.terceroRp = this.rpCabRev[x][2];
                this.objetoRp = this.rpCabRev[x][3];
                this.cdpRev = this.rpCabRev[x][4];
            }
            //console.log(this.detallesCdp);
        },

        validarRpRev: async function(){
            
            const fechat = document.getElementById('fc_1198971545').value;
            if(fechat != ''){
                if(this.numRpRev != ''){
                    const fechatAr = fechat.split('/');
                    const fechaf = fechatAr[2]+'-'+fechatAr[1]+'-'+fechatAr[0];
                    var vig = fechatAr[2];
                    this.loading = true;
                    await  axios.post('vue/presupuesto_ccp/ccp-rppbasico.php?action=validarRpRev&vigencia=' + vig + '&consvigencia=' + this.numRpRev + '&fecha=' + fechaf )
                    .then((response) => {
                        if(response.data.rpValido){
                            this.buscaInformacionRpRev();
                        }else{
                            this.limpiarFormRev();
                        }
                    }).finally(() => {
                        this.loading =  false;
                    });
                }
            }else{
                Swal.fire(
                    'Falta la fecha de reversi&oacute;n.',
                    'Falta Escoger la fecha de reversi&oacute;n.',
                    'warning'
                );
                this.numRpRev = '';
            }
        },

        limpiarFormRev: function(){
            Swal.fire(
                'Sin saldo.',
                'No tiene saldo en este rp.',
                'warning'
            );
            this.fechaRp = '';
            this.numRpRev = '';
            this.terceroRp = '';
            this.nombreTerceroRev = '';
            this.objetoRp = '';
            this.detallesRev = [];
            this.valorRev = [];
        },

        filterByDate() {
            if (this.fechaIni == "" || this.fechaFin == "") {
                this.reg_contratos = this.reg_contratosCopy;
                return false;
            }
        
            const start = new Date(this.fechaIni);
            const end = new Date(this.fechaFin);
    
            this.reg_contratos = this.reg_contratosCopy.filter(registro => {
                const cdpDate = new Date(registro.fecha);
                return cdpDate >= start && cdpDate <= end;
            });
        },

        filter(type="") {
            if (type=="modalCdpTxt") {
                let search = this.txtSearch.toLowerCase();
                this.reg_contratos = [...this.reg_contratosCopy.filter(e=>e.contrato.toLowerCase().includes(search) || e.cdp.toLowerCase().includes(search) || e.tercero.toLowerCase().includes(search) || e.nombreTercero.toLowerCase().includes(search) || e.objeto.toLowerCase().includes(search))];
            }
        },

    }
});
