const URL = 'vue/presupuesto_ccp/plan-bancoproyectos.php';
var app = new Vue({
	el: '#myapp',
	data:{
		isLoading:false,
		idproyecto:'',
		idproyectoaux:'',
		valida_proyecto:'NO',
		preguntasn:'',
		//INICIO MODALES
		showMensaje: false,
		showModal: false,
		showModal2: false,
		showModal3: false,
		showModalUnidadEj: false,
		showopcion1:false,
		showopcion2:false,
		showopcion2_3:false,
		titulomensaje:'',
		contenidomensaje:'',
		colortitulosmensaje:'',
		//BUSCADORES
		search: {keyword: ''},
		searchProgram: {keywordProgram: ''},
		searchProduct: {keywordProduct: ''},


		//MODULO PROYECTO
		unidadejecutoradobleclick: 'colordobleclik',
		sectordobleclick:'colordobleclik',
		programadobleclick:'colordobleclik',
		indicadordobleclick:'colordobleclik',
		fuentedobleclick:'colordobleclik',
		secciondobleclick:'colordobleclik',
		divisiondobleclick:'colordobleclik',
		grupodobleclick:'colordobleclik',
		clasedobleclick:'colordobleclik',
		subclasedobleclick:'colordobleclik',
		subproductodobleclick:'colordobleclik',
		parpadeomediopago:'',
		parpadeovalorrubro:'',
		codigo:'',
		vigencia: '',
		nombre:'',
		valorproyecto:'0',
		descripcion:'',
		unidadejecutora: '',
		cunidadejecutora: '',
		sector:'',
		csector:'',
		programa:'',
		cprograma:'',
		subprograma:'',
		csubprograma:'',
		producto:'',
		cproducto:'',
		indicadorpro:'',
		cindicadorpro:'',
		sectores: [],
		unidadesejecutoras: [],
		sector_p: '',
		programas:[],
		programa_P:'',
		productos:[],
		programas_subprogramas: [],
		sombra: '',
		selecproductosa:[],
		selecproductosb:[],
		vcproducto:'',
		years:[],
		//MODULO PRESUPUESTO
		mediopago:'',
		codrubro:'',
		nrubro:'',
		nombre_r: '',
		valorrubro:'',
		cuentapre:'',
		cuentaspres:[],
		cpadre_p:'',
		selectcuetasa:[],
		selectcuetasb:[],
		selectcuetasc:[],
		selectcuetasd:[],
		selectcuetase:[],
		selectcuetasf:[],
		selectbuscar:[],
		selectbuscar1:[],
		selectbuscar2:[],
		vccuenta:'',
		secciones:[],
		seccion:'',
		seccion_p:'',
		cseccion:'',
		clasificador:'',
		identproducto:'',
		nomidentproducto:'',
		valoridentproducto:[],
		codigoidentproducto:[],
		nombreidentproducto:[],
		cclasifica:'',
		cclasificados:[],
		clasificadorescuentas: [],
		cdivision:'',
		division:'',
		division_p:'',
		grupo:'',
		cgrupo:'',
		grupo_p:'',
		clase:'',
		cclase:'',
		clase_p:'',
		subclase:'',
		csubclase:'',
		subclase_p:'',


		subproducto:'',
		csubproducto:'',

		identidad:'',
		nitentidad:'',
		nomentidad:'',
		validacuin: false,
		validaclaservi: false,
		validaclabienes: false,
		codigocuin:'',
		clacuin:'',
		vcodigocuin:'',
		valorcuin:'',
		valorsinclasifi: '',
		deshabilitar_seccion: false,
		//MODULO FUENTES PROYECTO
		cfuentef:'',
		fuentef:'',
		cmetaf:'',
		metaf:'',
		results:[],
		resultspp:[],
		infometas:[],
		vfuente:'',
		opcionmensaje:'',
		tb1:1,
		tb2:2,
		tb3:3,
		tabgroup2:1,
		tapheight1:'79%',
		tapheight2:'79%',
		tapheight3:'79%',
		contador:0,
		nPoliticaPublica: '',
		codigoPoliticaPublica: '',

		proyectosProgramaticos: [],
		proyectosProgramaticosCopy:[],
		showModal_programaticoProyectos: false,
		selectSector:0,
		arrSectores:[],
		arrSectoresFilter:[],
		selectPrograma:0,
		arrProgramasFilter:[],
		selectProducto:0,
		arrProductosFilter:[],
	},
	mounted(){
		this.fetchMembers();
		this.fetchMembersUnidadEj();
		this.cargayears();
		this.getData();
		this.conocerid();
	},
	computed:{
	},
	methods:{
		onPaste (evt){
			setTimeout(this.validacodigo, 100);
		},
		onlyNumber ($event){
			let keyCode = ($event.keyCode ? $event.keyCode : $event.which);
			if ((keyCode < 48 || keyCode > 57) && keyCode !== 46){
				$event.preventDefault();
			}
			setTimeout(this.validarcodrep, 100);
		},
		validacodigo: function(){
			if(isNaN(this.codigo)){
				Swal.fire({
					icon: 'error',
					title: 'Error!',
					text: 'La informacion que se pego no es un numero',
					confirmButtonText: 'Continuar',
					confirmButtonColor: '#FF121A',
					timer: 3500
				});
				this.codigo = '';
			}else{
				this.validarcodrep();
			}
		},
		validarcodrep: async function(){
			if (this.codigo.length >= 13){
				const formData = new FormData();
				formData.append("buscar","validacod");
				formData.append("codi",this.codigo);
				formData.append("vigen",this.vigencia);
				formData.append("seccionPresu",this.cunidadejecutora);
				const response = await fetch(URL,{method:"POST",body:formData});
				const idProyecto = await response.json();
				if (idProyecto != null && idProyecto != ""){
					this.seleccionarProgramaticoProyecto(idProyecto);
				}else{
					this.limpiarNuevoBpim();

				}
			}
		},
		cargayears: async function(){
			const formData = new FormData();
			formData.append("buscar", "years");
			try {
				const response = await fetch(URL,{method:"POST",body:formData});
				const data = await response.json();
				if (Array.isArray(data)) {
					this.years = data.map(year => [year]);
					this.vigencia = data[0];
				} else {
					console.error("Error cargando años");
				}
			} catch (error) {
				console.error("Error en la petición:", error);
			}
		},
		toggleMensaje: function(){
			this.showMensaje = !this.showMensaje;
			if(this.opcionmensaje!='' && this.showMensaje== false){
				switch (this.opcionmensaje){
					case '1':
						this.opcionmensaje='';
						this.$refs.codigo.focus();
						break;
					case'2':
						this.opcionmensaje='';
						this.$refs.nombre.focus();
						break;
					case'3':
						this.opcionmensaje='';
						this.$refs.descripcion.focus();
						break;
					case '4':
						this.opcionmensaje='';
						this.$refs.valorrubro.focus();
						break;
					case '5':
						this.opcionmensaje='';
						this.$refs.fuentef.focus();
						break;
				}
			}
		},
		toggleModalUnidadEje: function(){
			if(this.selecproductosa.length==0){
				this.sectordobleclick='colordobleclik',
				this.showModalUnidadEj = !this.showModalUnidadEj;
			}
		},
		toggleModal: function(){
			if(this.selecproductosa.length==0){
				this.sectordobleclick='colordobleclik',
				this.showModal = !this.showModal;
			}else{
				this.toggleMensaje();
				this.colortitulosmensaje='crimson';
				this.titulomensaje='Mensaje de Error';
				this.contenidomensaje='Ya se ingresaron productos no se puede cambiar el sector';
			}
		},
		toggleModal2: function(){
			if(this.selecproductosa.length==0){
				this.programadobleclick='colordobleclik';
				if(this.csector!=''){
					this.showModal2 = !this.showModal2;
					if(this.showModal2== true){
						this.programasp(this.csector);
					}
				}else{
					this.toggleMensaje();
					this.colortitulosmensaje='crimson';
					this.titulomensaje='Mensaje de Error';
					this.contenidomensaje='Se debe ingresar primero un sector';
					this.sectordobleclick='parpadea colordobleclik';
				}
			}else{
				this.toggleMensaje();
				this.colortitulosmensaje='crimson';
				this.titulomensaje='Mensaje de Error';
				this.contenidomensaje='Ya se ingresaron productos no se puede cambiar el Programa';
			}
		},
		toggleModal3: function(){
			if(this.cprograma!=''){
				this.showModal3 = !this.showModal3;
				if(this.showModal3== true){
					this.buscarProductos(this.cprograma);
				}
			}else{
				this.toggleMensaje();
				this.colortitulosmensaje='crimson';
				this.titulomensaje='Mensaje de Error';
				this.contenidomensaje='Se debe ingresar primero un programa';
				this.programadobleclick='parpadea colordobleclik';
			}
		},
		fetchMembers: async function(){
			const formData = new FormData();
			formData.append("buscar", "sectores");
			try {
				const response = await fetch(URL,{method:"POST",body:formData});
				const data = await response.json();

				if (Array.isArray(data)) {
					this.sectores = data;
				} else {
					console.error("Error cargando sectores");
				}
			} catch (error) {
				console.error("Error en la petición:", error);
			}
		},
		fetchMembersUnidadEj: async function() {
			const formData = new FormData();
			formData.append("buscar", "seccionpresupuestal");
			try {
				const response = await fetch(URL,{method:"POST",body:formData});
				const data = await response.json();
				if (Array.isArray(data)) {
					this.unidadesejecutoras = data;
					if (data.length > 0) {
						this.unidadejecutora = data[0].codigo + " - " + data[0].nombre;
						this.cunidadejecutora = data[0].codigo;
					}
				} else {
					console.error("Error cargando unidades ejecutoras");
				}
			} catch (error) {
				console.error("Error en la petición:", error);
			}
		},
		programasp: async function(sector){
			this.searchProgram = { keywordProgram: '' };
			this.programas_subprogramas = [];
			this.productos = [];
			this.programa_p = '';
			this.mostrarProductos = false;
			this.sector_p = sector;
			const formData = new FormData();
			formData.append("buscar", "subprogramas");
			formData.append("sector", this.sector_p);
			try {
				const response = await fetch(URL,{method:"POST",body: formData});
				const data = await response.json();
				if (Array.isArray(data)) {
					this.programas_subprogramas = data;
				} else {
					console.error("Error cargando programas");
				}
			} catch (error) {
				console.error("Error en la petición:", error);
			}

		},
		buscarProductos: async function(programa){
			this.searchProduct = { keywordProduct: '' };
			this.productos = [];
			this.programa_p = programa;
			const formData = new FormData();
			formData.append("buscar", "subprogramas2");
			formData.append("programa", this.programa_p);
			try {
				const response = await fetch(URL,{method:"POST",body:formData});
				const data = await response.json();
				if (Array.isArray(data)) {
					this.productos = data;
				} else {
					console.error("Error cargando productos");
				}
			} catch (error) {
				console.error("Error en la petición:", error);
			}
		},
		searchMonitorPrograms: async function() {
			const formData = new FormData();
			formData.append("buscar", "searchProgram");
			formData.append("keywordProgram", this.searchProgram.keywordProgram);
			formData.append("sectorSearch", this.sector_p);
			try {
				const response = await fetch(URL,{method:"POST",body:formData});
				const data = await response.json();
				if (Array.isArray(data)) {
					this.programas_subprogramas = data;
					this.noMember = data.length === 0;
				} else {
					console.error("Error cargando programas");
					this.noMember = true;
				}
			} catch (error) {
				console.error("Error en la petición:", error);
				this.noMember = true;
			}
		},
		searchMonitor: async function() {
			const formData = new FormData();
			formData.append("buscar", "searchSector");
			formData.append("keyword", this.search.keyword);
			try {
				const response = await fetch(URL, {method: "POST",body: formData});
				const data = await response.json();
				if (Array.isArray(data)) {
					this.sectores = data;
					this.noMember = data.length === 0;
				} else {
					console.error("Error cargando sectores");
					this.noMember = true;
				}
			} catch (error) {
				console.error("Error en la petición:", error);
				this.noMember = true;
			}
		},
		searchMonitorProducts: async function() {
			var formData = new FormData();
			formData.append("buscar", "searchProduct");
			formData.append("keywordProduct", this.searchProduct.keywordProduct);
			formData.append("programSearch", this.programa_p);
			try {
				const response = await fetch(URL,{method:"POST",body:formData});
				const data = await response.json();
				if (Array.isArray(data)) {
					this.productos = data;
					this.noMember = data.length === 0;
				} else {
					console.error("Error cargando productos:", data.error);
					this.noMember = true;
				}
			} catch (error) {
				console.error("Error en la petición:", error);
				this.noMember = true;
			}
		},
		toFormData: function(obj){
			var form_data = new FormData();
			for(var key in obj){
				form_data.append(key, obj[key]);
			}
			return form_data;
		},
		cargaunidadejecutora: function(id,cod,nom){
			this.cunidadejecutora = id;
			this.unidadejecutora = id + ' - ' + nom;
			this.showModalUnidadEj = false;
			this.deshacer('8');
		},
		inicioproyecto: async function(idini){
			await axios.post('vue/presupuesto_ccp/plan-bancoproyectoseditar.php?accion=inicialproyecto&idproy='+idini)
			.then(
				(response)=> {
					//this.iniciaunieje(response.data.codigos[0][1]);
					this.codigo=response.data.codigos[0][2];
					this.nombre=response.data.codigos[0][4];
					this.descripcion=response.data.codigos[0][5];
					this.valorproyecto=response.data.codigos[0][6];
				}
			);
		},
		iniciosector: async function(idini){
			await axios.post('vue/presupuesto_ccp/plan-bancoproyectoseditar.php?accion=iniciasector&idproy='+idini)
			.then(
				(response)=>{
					this.csector=response.data.codigos[0][0];
					this.sector=response.data.codigos[0][0]+' - '+response.data.codigos[0][1];
				}
			);
		},
		iniciprograma: async function(idini){
			await axios.post('vue/presupuesto_ccp/plan-bancoproyectoseditar.php?accion=iniciaprograma&idproy='+idini)
			.then(
				(response)=>{
					this.cprograma=response.data.codigos[0][0];
					this.programa=response.data.codigos[0][0]+" - "+response.data.codigos[0][2];
					this.csubprograma=response.data.codigos[0][1];
					this.subprograma=response.data.codigos[0][1]+" - "+response.data.codigos[0][3];
				}
			);
		},
		inicioproductos: async function(idunieje){
			await axios.post('vue/presupuesto_ccp/plan-bancoproyectoseditar.php?accion=iniciaproductos&idunieje='+idunieje)
			.then(
				(response)=>{
					for(var x=0; x < response.data.codigos.length;x++){
						var codproducto = response.data.codigos[x][3];
						var nomproducto = response.data.codigos[x][3] + " - " + response.data.codigos[x][6];
						var codindicador = response.data.codigos[x][4];
						var nomindicador = response.data.codigos[x][4] + " - " + response.data.codigos[x][5];
						var codsertor = response.data.codigos[x][0];
						var codprograma = response.data.codigos[x][1];
						var codsubprograma = response.data.codigos[x][2];
						var varauxia=[codproducto,nomproducto,codindicador,nomindicador,codsertor,codprograma,codsubprograma];
						var varauxib=[codproducto,codindicador,codsertor,codprograma,codsubprograma];
						this.selecproductosa.push(varauxia);
						this.selecproductosb.push(varauxib);
					}
				}
			);
		},

		cargasector: function(cod,nom)
		{
			this.csector=cod;
			this.sector=cod+' - '+nom;
			this.showModal = false;
			this.deshacer('8');
		},
		cargaprograma: function(cod,nom,scod,snom){
			this.cprograma = cod;
			this.programa = cod+" - "+nom;
			this.csubprograma = scod;
			this.subprograma = scod+" - "+snom;
			this.showModal2 = false;
			this.deshacer('9');
		},
		cargaproducto: function(cod,nom,pcod,pnom)
		{
			this.cproducto=cod;
			this.producto=cod+" - "+nom;
			this.cindicadorpro=pcod;
			this.indicadorpro=pcod+" - "+pnom;
			this.showModal3 = false;
		},
		deplegar: function()
		{
			switch (this.clasificador)
			{	case '1':
					this.tapheight1='61%';
					this.tapheight2='61%';
					this.tapheight3='61%';
					this.tabgroup2=2;
					this.showopcion1=true;
					this.showopcion2=false;
					this.showopcion2_3=false;
					break;
				case '2':
				case '3':
					this.tapheight1='52.5%';
					this.tapheight2='52.5%';
					this.tapheight3='52.5%';
					this.tabgroup2=3;
					this.showopcion1=false;
					this.showopcion2=false;
					this.showopcion2_3=true;
					break;
			}
		},

		agregarproducto: function()
		{
			var bandera01=0;
			if(this.selecproductosb.length>0)
			{
				for (const auxarray in this.selecproductosb)
				{
					var auxlong=`${this.selecproductosb[auxarray]}`;
					if(auxlong.includes(this.cindicadorpro)==true)
					{
						bandera01=1;
					}
				}
			}
			if(this.indicadorpro != '')
			{
				if(bandera01==0)
				{
					var varauxia=[this.cproducto,this.producto,this.cindicadorpro,this.indicadorpro,this.csector,this.cprograma,this.csubprograma,this.contador];
					var varauxib=[this.cproducto,this.cindicadorpro,this.csector,this.cprograma,this.csubprograma,this.contador];
					this.valoridentproducto[this.contador]=0;
					this.codigoidentproducto[this.contador]=this.cindicadorpro;
					this.nombreidentproducto[this.contador]=this.indicadorpro;
					this.selecproductosa.push(varauxia);
					this.selecproductosb.push(varauxib);
					this.cindicadorpro=this.indicadorpro=this.cproducto=this.producto='';
					this.sectordobleclick='';
					this.programadobleclick= '';
					this.contador++;
				}
				else
				{
					this.toggleMensaje();
					this.colortitulosmensaje='crimson';
					this.titulomensaje='Mensaje de Error';
					this.contenidomensaje='Producto duplicado';
				}
			}
			else
			{
				this.toggleMensaje();
				this.colortitulosmensaje='crimson';
				this.titulomensaje='Mensaje de Error';
				this.contenidomensaje='Falta seleccionar un producto para agregar';
			}
		},
		eliminaproducto: async function(index){
			var formData = new FormData();

			formData.append("bpim", this.codigo);
			formData.append("vigencia", this.vigencia);
			formData.append("programatico", this.selecproductosa[index][2]);
            formData.append("seccionPresu", this.cunidadejecutora);

			await axios.post(URL + '?action=consultarProgramatico', formData)
			.then((response) => {
				if(response.data.existe){
					this.toggleMensaje();
					this.colortitulosmensaje='crimson';
					this.titulomensaje='Mensaje de Error';
					this.contenidomensaje='No se puede eliminar detalle, debido a que se utilizó este indicador producto en un proceso presupuestal.';
				}else{

					this.selecproductosa.splice(index, 1);
					this.selecproductosb.splice(index, 1);
					if(this.selecproductosa.length==0){this.programadobleclick=this.sectordobleclick='colordobleclik';}
				}
			});
		},



		deshacer: function(id)
		{
			switch (id)
			{
				case'0':
					this.codrubro=this.nrubro=this.valorrubro=this.cseccion=this.seccion=this.cdivision=this.division=this.cgrupo=this.grupo='';
					this.clasificador=this.cclase=this.clase=this.csubclase=this.subclase=this.csubproducto=this.subproducto='';
					break;
				case '1':
					this.clasificador=this.valorrubro=this.cseccion=this.seccion=this.cdivision=this.division=this.cgrupo=this.grupo='';
					this.cclase=this.clase=this.csubclase=this.subclase=this.csubproducto=this.subproducto='';
					break;
				case '2':
					this.valorrubro=this.cseccion=this.seccion=this.cdivision=this.division=this.cgrupo=this.grupo='';
					this.cclase=this.clase=this.csubclase=this.subclase=this.csubproducto=this.subproducto='';
					break;
				case '3':
					this.cdivision=this.division=this.cgrupo=this.grupo=this.cclase=this.clase=this.csubclase=this.subclase=this.valorrubro='';
					this.csubproducto=this.subproducto='';
					break;
				case '4':
					this.cgrupo=this.grupo=this.cclase=this.clase=this.csubclase=this.subclase=this.valorrubro=this.csubproducto=this.subproducto='';
					break;
				case '5':
					this.cclase=this.clase=this.csubclase=this.subclase=this.valorrubro=this.csubproducto=this.subproducto='';
					break;
				case '6':
					this.csubclase=this.subclase=this.valorrubro=this.csubproducto=this.subproducto='';
					break;
				case '7':
					this.valorrubro=this.csubproducto=this.subproducto='';
					break;
				case '8':
					this.cproducto=this.producto=this.cindicadorpro=this.indicadorpro=this.programa=this.cprograma=this.subprograma=this.csubprograma='';
					break;
				case '9':
					this.cproducto=this.producto=this.cindicadorpro=this.indicadorpro='';
					break;
				case '10':
					this.codrubro=this.nrubro=this.clasificador='';
					break;
				case '11':
					this.clasificador=this.valorrubro=this.cdivision=this.division=this.cgrupo=this.grupo='';
					this.cclase=this.clase=this.csubclase=this.subclase='';
					break;
				case '12':
					this.valorrubro=this.cdivision=this.division=this.cgrupo=this.grupo='';
					this.cclase=this.clase=this.csubclase=this.subclase='';
					break;
			}
		},
		preguntaguardar: function(){
			Swal.fire({
				title:"¿Estás segur@ de guardar el proyecto?",
				text:"",
				icon: 'warning',
				showCancelButton:true,
				confirmButtonColor: '#3085d6',
				cancelButtonColor: '#d33',
				confirmButtonText:"Sí, guardar",
				cancelButtonText:"No, cancelar"
			}).then(async (result) =>{
				if(result.isConfirmed){
					await this.guardarglobal();
				}
			});
		},
		guardarglobal: async function(){
			await this.validarguardar();
			if(this.valida_proyecto == 'SI'){
				var idnum = this.idproyecto;
				var idnumaux = this.idproyectoaux;
				var formData = new FormData();
				//guardar cabecera
				formData.append("idnum",idnum);
				formData.append("idnumaux",idnumaux);
				formData.append("idunidadej", this.cunidadejecutora);
				formData.append("codigo", this.codigo);
				formData.append("vigencia", this.vigencia);
				formData.append("nombre", this.nombre);
				formData.append("descripcion", this.descripcion);
				formData.append("valortotal", this.valorproyecto);
				//guardar productos
				Object.keys(this.selecproductosb).forEach(e => {formData.append(`infproyectos[${e}]`, this.selecproductosb[e])});
				Object.keys(this.valoridentproducto).forEach(e => {formData.append(`valproductos[${e}]`, this.valoridentproducto[e])});
				//enviar todo
				axios.post('vue/presupuesto_ccp/plan-bancoproyectos.php?guardar=gglobal', formData).then();
				this.toggleMensaje();
				this.colortitulosmensaje='darkgreen';
				this.titulomensaje='Almacenado en el Sitema';
				this.contenidomensaje='La informaci\xf3n del Proyecto No '+idnum+' se almaceno con exito';
				setTimeout(()=>{location.reload()}, 3000)
			}
		},

		conocerid: async function(){
			await axios.post('vue/presupuesto_ccp/plan-bancoproyectos.php?numid=si')
			.then((response)=>{
				this.idproyecto = response.data.numid;
			});
		},
		guardarcabecera: function(idnum)
		{
			//console.log(app.cunidadejecutora)
			var formData = new FormData();
			formData.append("idnum",idnum);
			formData.append("idunidadej", app.cunidadejecutora);
			formData.append("codigo", app.codigo);
			formData.append("vigencia", app.vigencia);
			formData.append("nombre", app.nombre);
			formData.append("descripcion", app.descripcion);
			formData.append("valortotal", app.valorproyecto);
			axios.post('vue/presupuesto_ccp/plan-bancoproyectos.php?guardar=cabecera', formData)
			.then();
		},
		guardarproductos: function(idnum)
		{
			var formData = new FormData();
			formData.append("idnum",idnum);
			Object.keys(this.selecproductosb).forEach(e => {formData.append(`infproyectos[${e}]`, this.selecproductosb[e])});
			Object.keys(this.valoridentproducto).forEach(e => {formData.append(`valproductos[${e}]`, this.valoridentproducto[e])});
			axios.post('vue/presupuesto_ccp/plan-bancoproyectos.php?guardar=productos', formData)
			.then();
		},
		guardarcuin: function(idnum)
		{
			var formData = new FormData();
			formData.append("idnum",idnum);
			Object.keys(this.selectcuetasd).forEach(e => {formData.append(`infcuentascuin[${e}]`, this.selectcuetasd[e])});
			axios.post('vue/presupuesto_ccp/plan-bancoproyectos.php?guardar=ccuentascuin', formData)
			.then();
		},
		guardarsinclasificador: function(idnum)
		{
			var formData = new FormData();
			formData.append("idnum",idnum);
			Object.keys(this.selectcuetasf).forEach(e => {formData.append(`infcuentassinclasificador[${e}]`, this.selectcuetasf[e])});
			axios.post('vue/presupuesto_ccp/plan-bancoproyectos.php?guardar=ccuentassinclasificador', formData)
			.then();
		},
		validarguardar: async function()
		{
			if(this.codigo.trim()!='')
			{
				if(this.nombre.trim()!='')
				{
					if(this.descripcion.trim()!='')
					{
						if(this.selecproductosa.length > 0)
						{
							if (this.codigo.length >= 13)
							{
								this.valida_proyecto = 'SI';
							}
							else
							{
								this.toggleMensaje();
								this.colortitulosmensaje='crimson';
								this.titulomensaje='Mensaje de Error';
								this.contenidomensaje='El Código debe contener 15 digitos';
							}
						}
						else
						{
							this.toggleMensaje();
							this.colortitulosmensaje='crimson';
							this.titulomensaje='Mensaje de Error';
							this.contenidomensaje='Falta agregar productos para poder guardar el proyecto';
						}
					}
					else
					{
						this.opcionmensaje='3';
						this.toggleMensaje();
						this.colortitulosmensaje='crimson';
						this.titulomensaje='Mensaje de Error';
						this.contenidomensaje='Falta agregar la descripci\xf3n del proyecto para poder guardar';
					}
				}
				else
				{
					this.opcionmensaje='2';
					this.toggleMensaje();
					this.colortitulosmensaje='crimson';
					this.titulomensaje='Mensaje de Error';
					this.contenidomensaje='Falta agregar el nombre del proyecto para poder guardar';
				}
			}
			else
			{
				this.opcionmensaje='1';
				this.toggleMensaje();
				this.colortitulosmensaje='crimson';
				this.titulomensaje='Mensaje de Error';
				this.contenidomensaje='Falta agregar el c\xf3digo del proyecto para poder guardar';
			}


		},
		validasubproducto: function(codigo)
		{
			if(codigo.length==7)
			{
				var formData = new FormData();
				var codbuscador='';
				var nivbuscador='';
				if (this.csubclase!='')
				{
					codbuscador=this.csubclase;
					nivbuscador='5';
				}
				else if (this.cclase!='')
				{
					codbuscador=this.cclase;
					nivbuscador='4';
				}
				else if (this.cgrupo!='')
				{
					codbuscador=this.cgrupo;
					nivbuscador='3';
				}
				else if (this.cdivision!='')
				{
					codbuscador=this.cdivision;
					nivbuscador='2';
				}
				else if (this.cseccion!='')
				{
					codbuscador=this.cseccion;
					nivbuscador='1';
				}
				formData.append("seccion",codbuscador);
				formData.append("nivel",nivbuscador);
				formData.append("codigo",codigo);
				axios.post('vue/presupuesto_ccp/plan-bancoproyectos.php?buscar=valsubproducto',formData)
				.then(
					(response)=>
					{
						if(response.data.codigos[0][0]!='')
						{
							this.csubproducto=codigo;
							this.subproducto=codigo+" - "+response.data.codigos[0][0];
							if(this.cseccion=='')
							{
								this.cseccion=codigo.substr(0,1);
								axios.post('vue/presupuesto_ccp/plan-bancoproyectos.php?buscar=nombregrupo&grupo='+this.cseccion)
								.then((response)=>{this.seccion=this.cseccion+" - "+response.data.codigos[0][0];});
							}
							if(this.cdivision=='')
							{
								this.cdivision=codigo.substr(0,2);
								axios.post('vue/presupuesto_ccp/plan-bancoproyectos.php?buscar=nombregrupo&grupo='+this.cdivision)
								.then((response)=>{this.division=this.cdivision+" - "+response.data.codigos[0][0];});
							}
							if(this.cgrupo=='')
							{
								this.cgrupo=codigo.substr(0,3);
								axios.post('vue/presupuesto_ccp/plan-bancoproyectos.php?buscar=nombregrupo&grupo='+this.cgrupo)
								.then((response)=>{this.grupo=this.cgrupo+" - "+response.data.codigos[0][0];});
							}
							if(this.cclase=='')
							{
								this.cclase=codigo.substr(0,4);
								axios.post('vue/presupuesto_ccp/plan-bancoproyectos.php?buscar=nombregrupo&grupo='+this.cclase)
								.then((response)=>{this.clase=this.cclase+" - "+response.data.codigos[0][0];});
							}
							if(this.csubclase)
							{
								this.csubclase=codigo.substr(0,5);
								axios.post('vue/presupuesto_ccp/plan-bancoproyectos.php?buscar=nombregrupo&grupo='+this.csubclase)
								.then((response)=>{this.subclase=this.csubclase+" - "+response.data.codigos[0][0];});
							}
						}
					}
				);
			}
		},
		validasubclase: function(codigo)
		{
			if(codigo.length==5)
			{
				var formData = new FormData();
				var codbuscador='';
				var nivbuscador='';
				if (this.cclase!='')
				{
					codbuscador=this.cclase;
					nivbuscador='4';
				}
				else if (this.cgrupo!='')
				{
					codbuscador=this.cgrupo;
					nivbuscador='3';
				}
				else if (this.cdivision!='')
				{
					codbuscador=this.cdivision;
					nivbuscador='2';
				}
				else if (this.cseccion!='')
				{
					codbuscador=this.cseccion;
					nivbuscador='1';
				}
				formData.append("seccion",codbuscador);
				formData.append("nivel",nivbuscador);
				formData.append("codigo",codigo);
				if(this.clasificador=='2')
				{
					axios.post('vue/presupuesto_ccp/plan-bancoproyectos.php?buscar=valsubclaseb',formData)
					.then(
						(response)=>
						{
							if(response.data.codigos[0][0]!='')
							{
								this.csubclase=codigo;
								this.subclase=codigo+" - "+response.data.codigos[0][0];
								if(this.cseccion=='')
								{
									this.cseccion=codigo.substr(0,1);
									axios.post('vue/presupuesto_ccp/plan-bancoproyectos.php?buscar=nombregrupo&grupo='+this.cseccion)
									.then((response)=>{this.seccion=this.cseccion+" - "+response.data.codigos[0][0];});
								}
								if(this.cdivision=='')
								{
									this.cdivision=codigo.substr(0,2);
									axios.post('vue/presupuesto_ccp/plan-bancoproyectos.php?buscar=nombregrupo&grupo='+this.cdivision)
									.then((response)=>{this.division=this.cdivision+" - "+response.data.codigos[0][0];});
								}
								if(this.cgrupo=='')
								{
									this.cgrupo=codigo.substr(0,3);
									axios.post('vue/presupuesto_ccp/plan-bancoproyectos.php?buscar=nombregrupo&grupo='+this.cgrupo)
									.then((response)=>{this.grupo=this.cgrupo+" - "+response.data.codigos[0][0];});
								}
								if(this.cclase=='')
								{
									this.cclase=codigo.substr(0,4);
									axios.post('vue/presupuesto_ccp/plan-bancoproyectos.php?buscar=nombregrupo&grupo='+this.cclase)
									.then((response)=>{this.clase=this.cclase+" - "+response.data.codigos[0][0];});
								}
							}
						}
					);
				}
				else
				{
					axios.post('vue/presupuesto_ccp/plan-bancoproyectos.php?buscar=valsubclases',formData)
					.then(
						(response)=>
						{
							if(response.data.codigos[0][0]!='')
							{
								this.csubclase=codigo;
								this.subclase=codigo+" - "+response.data.codigos[0][0];
								if(this.cseccion=='')
								{
									this.cseccion=codigo.substr(0,1);
									axios.post('vue/presupuesto_ccp/plan-bancoproyectos.php?buscar=nombregrupos&grupo='+this.cseccion)
									.then((response)=>{this.seccion=this.cseccion+" - "+response.data.codigos[0][0];});
								}
								if(this.cdivision=='')
								{
									this.cdivision=codigo.substr(0,2);
									axios.post('vue/presupuesto_ccp/plan-bancoproyectos.php?buscar=nombregrupos&grupo='+this.cdivision)
									.then((response)=>{this.division=this.cdivision+" - "+response.data.codigos[0][0];});
								}
								if(this.cgrupo=='')
								{
									this.cgrupo=codigo.substr(0,3);
									axios.post('vue/presupuesto_ccp/plan-bancoproyectos.php?buscar=nombregrupos&grupo='+this.cgrupo)
									.then((response)=>{this.grupo=this.cgrupo+" - "+response.data.codigos[0][0];});
								}
								if(this.cclase=='')
								{
									this.cclase=codigo.substr(0,4);
									axios.post('vue/presupuesto_ccp/plan-bancoproyectos.php?buscar=nombregrupos&grupo='+this.cclase)
									.then((response)=>{this.clase=this.cclase+" - "+response.data.codigos[0][0];});
								}
							}
						}
					);
				}
			}
		},
		validaindicadorproducto:function(codigo)
		{
			var bandera1=0;
			if(this.selecproductosa.length!=0)
			{
				if(codigo.length>=4)
				{
					if(codigo.substr(0,4) == this.cprograma)
					{
						bandera1=0;
					}
					else
					{
						bandera1=1;
						this.toggleMensaje();
						this.colortitulosmensaje='crimson';
						this.titulomensaje='Mensaje de Error';
						this.contenidomensaje='Todos los productos deben ser del mismo Sector y Programa';
					}
				}
			}
			if(codigo.length == 9 && bandera1 == 0)
			{

				var formData = new FormData();
				formData.append("codigo",codigo);
				axios.post('vue/presupuesto_ccp/plan-bancoproyectos.php?buscar=valindicproducto',formData)
				.then(
					(response)=>
					{
						if(response.data.codigos[0][0]!='')
						{
							this.cindicadorpro = codigo;
							this.indicadorpro = codigo +" - " + response.data.codigos[0][0];
							this.cproducto = response.data.codigos[0][1];
							this.producto = response.data.codigos[0][1] + " - " + response.data.codigos[0][2];
							this.cprograma = codigo.substr(0,4);
							this.csector = codigo.substr(0,2);
							if(this.cprograma!='')
							{
								axios.post('vue/presupuesto_ccp/plan-bancoproyectos.php?buscar=valprograma&programa='+this.cprograma)
								.then(
									(response)=>
									{
										this.programa = this.cprograma + " - "+response.data.programas[0][0];
										this.csubprograma=response.data.programas[0][1];
										this.subprograma=this.csubprograma + " - " + response.data.programas[0][2];
									}
								);
							}
							if(this.csector!='')
							{
								axios.post('vue/presupuesto_ccp/plan-bancoproyectos.php?buscar=valsector&sector='+this.csector)
								.then(
									(response)=>
									{
										this.sector = this.csector + " - "+response.data.sectores[0][0];
									}
								);
							}
						}
					}
				);
			}
		},
		valorproductoxx:function(valor)
		{
			console.log(this.valoridentproducto);
		},
		getData: async function(){
			const formData = new FormData();
			formData.append("action","get");
			const response = await fetch('presupuesto_ccpet/traslado/crear/ccp-traslados-vue.php',{method:"POST",body:formData});
			const objData = await response.json();
			this.arrProgramas = objData.data.programas;
			this.arrSectores = objData.data.sectores;
		},
		ventanaRubroCredito: function(){
			app.showModal_programaticoProyectos = true;
			axios.post('presupuesto_ccpet/traslado/crear/ccp-traslados-vue.php?action=cargarProyectosCredito&vigencia=' + this.vigencia)
				.then((response) => {
					app.proyectosProgramaticos = response.data.proyectosProgramaticos;
					app.proyectosProgramaticosCopy= response.data.proyectosProgramaticos;
					this.resultsBuscar = [...this.proyectosProgramaticos];
				});
		},
		seleccionarProgramaticoProyecto: function(idini){
			this.idproyecto = this.idproyectoaux = idini;
			this.inicioproyecto(idini);
			this.iniciosector(idini);
			this.iniciprograma(idini);
			this.selecproductosa = [];
			this.inicioproductos(idini);
			this.selectSector = 0,
			this.selectPrograma = 0,
			this.selectProducto = 0,
			this.showModal_programaticoProyectos = false;
		},
		limpiarNuevoBpim: function(){
			this.nombre = '';
			this.descripcion = '';
			this.sector = '';
			this.programa = '';
			this.subprograma = '';
			this.indicadorpro = '';
			this.producto = '';
			this.selecproductosa = [];
			this.idproyectoaux = '';
			this.conocerid();
		},
		filter:function(type=""){
			if(type=="sector" && this.selectSector != 0){
				this.arrProductosFilter = [];
				this.proyectosProgramaticosCopy = [...this.proyectosProgramaticos.filter((e)=>{return e[7]==app.selectSector})];
				this.arrProgramasFilter = Array.from(new Set(
					app.proyectosProgramaticosCopy.map(e=>{return JSON.stringify({codigo:e[5],nombre:e[8]})})
				)).map(item => JSON.parse(item));
			}else if(type=="programa" && this.selectPrograma != 0){
				this.proyectosProgramaticosCopy = [...this.proyectosProgramaticos.filter((e)=>{return e[7]==app.selectSector && e[5]==app.selectPrograma})];
				this.arrProductosFilter = Array.from(new Set(
					app.proyectosProgramaticosCopy.map(e=>{return JSON.stringify({codigo:e[6],nombre:e[9]})})
				)).map(item => JSON.parse(item));
			}else if(type=="producto" && this.selectProducto != 0){
				this.proyectosProgramaticosCopy = [...this.proyectosProgramaticos.filter((e)=>{
					return e[7]==app.selectSector && e[5]==app.selectPrograma && e[6]==app.selectProducto}
				)];
			}else{
				this.proyectosProgramaticosCopy = [...this.proyectosProgramaticos];
			}
		}
	}
});
