<?php
	require_once '../../comun.inc';
    require '../../funciones.inc';
    require 'funcionesccp.inc.php';
    session_start();

	$linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");

    $maxVersion = ultimaVersionGastosCCPET();


    $out = array('error' => false);

    $action = "show";

    if(isset($_GET['action'])){
        $action = $_GET['action'];
    }

    if($action == 'show'){

        
        
    }

    if($action == 'clasificadoresDeInversion'){

        $nombreSectores = array();
        $nombreProgramas = array();
        $nombreSubProgramas = array();
        $nombreSubProgramasOp = array();
        $nombreProductos = array();
        $nombreProgramatico = array();

        $sqlrSectores = "SELECT codigo, nombre FROM ccpetsectores WHERE version = (SELECT MAX(version) FROM ccpetsectores)";
        $respSectores = mysqli_query($linkbd, $sqlrSectores);

        while($rowSectores = mysqli_fetch_row($respSectores)){
            array_push($nombreSectores, $rowSectores);
        }

        $sqlrProgramas = "SELECT codigo, nombre FROM ccpetprogramas WHERE version = (SELECT MAX(version) FROM ccpetprogramas)";
        $respProgramas = mysqli_query($linkbd, $sqlrProgramas);

        while($rowProgramas = mysqli_fetch_row($respProgramas)){
            array_push($nombreProgramas, $rowProgramas);
        }

        $sqlrSubProgramas = "SELECT codigo_subprograma, nombre_subprograma FROM ccpetprogramas WHERE version = (SELECT MAX(version) FROM ccpetprogramas) GROUP BY codigo_subprograma";
        $respSubProgramas = mysqli_query($linkbd, $sqlrSubProgramas);

        while($rowSubProgramas = mysqli_fetch_row($respSubProgramas)){
            array_push($nombreSubProgramas, $rowSubProgramas);
        }

        $sqlrSubProgramasOp = "SELECT codigo_subprograma, nombre_subprograma, codigo FROM ccpetprogramas WHERE version = (SELECT MAX(version) FROM ccpetprogramas) GROUP BY codigo_subprograma, codigo";
        $respSubProgramasOp = mysqli_query($linkbd, $sqlrSubProgramasOp);

        while($rowSubProgramasOp = mysqli_fetch_row($respSubProgramasOp)){
            array_push($nombreSubProgramasOp, $rowSubProgramasOp);
        }

        $sqlrProductos = "SELECT cod_producto, producto FROM ccpetproductos WHERE version = (SELECT MAX(version) FROM ccpetproductos) GROUP BY cod_producto";
        $respProductos = mysqli_query($linkbd, $sqlrProductos);

        while($rowProductos = mysqli_fetch_row($respProductos)){
            array_push($nombreProductos, $rowProductos);
        }

        $sqlrProgramatico = "SELECT codigo_indicador, indicador_producto FROM ccpetproductos WHERE version = (SELECT MAX(version) FROM ccpetproductos) GROUP BY codigo_indicador";
        $respProgramatico = mysqli_query($linkbd, $sqlrProgramatico);

        while($rowProgramatico = mysqli_fetch_row($respProgramatico)){
            array_push($nombreProgramatico, $rowProgramatico);
        }

        $productoTam = [];

        $sqlrProducto = "SELECT cod_producto, codigo_indicador FROM ccpetproductos LIMIT 1";
        $resProducto = mysqli_query($linkbd, $sqlrProducto);
        $rowProducto = mysqli_fetch_row($resProducto);
        array_push($productoTam, strlen($rowProducto[0]));
        array_push($productoTam, strlen($rowProducto[1]));

        $out['nombreSectores'] = $nombreSectores;
        $out['nombreProgramas'] = $nombreProgramas;
        $out['nombreSubProgramas'] = $nombreSubProgramas;
        $out['nombreSubProgramasOp'] = $nombreSubProgramasOp;
        $out['nombreProductos'] = $nombreProductos;
        $out['nombreProgramatico'] = $nombreProgramatico;
        $out['productoTam'] = $productoTam; 

    }

    if($action == 'fuentes'){

        $fuentes = [];
        $fuentesUsadas = [];
        $vigencia = $_POST['vigencia'];

        $sqlrAd = "SELECT DISTINCT fuente FROM ccpet_adiciones WHERE vigencia = '".$vigencia."' AND tipo_cuenta = 'G' AND bpim != ''";
        $respAd = mysqli_query($linkbd, $sqlrAd);
        while($rowAd = mysqli_fetch_row($respAd)){
            array_push($fuentesUsadas, $rowAd[0]);
        }

        $sqlrTrasladosCred = "SELECT DISTINCT fuente FROM ccpet_traslados WHERE vigencia = '".$vigencia."' AND tipo = 'C' AND bpim != ''";
        $respTrasladosCred = mysqli_query($linkbd, $sqlrTrasladosCred);
        while($rowTrasladosCred = mysqli_fetch_row($respTrasladosCred)){
            array_push($fuentesUsadas, $rowTrasladosCred[0]);
        }

        $sqlrInv = "SELECT TB2.id_fuente FROM ccpproyectospresupuesto AS TB1, ccpproyectospresupuesto_presupuesto AS TB2 WHERE TB1.id = TB2.codproyecto AND TB1.vigencia = '$vigencia'";
        $respInv = mysqli_query($linkbd, $sqlrInv);
        while($rowInv = mysqli_fetch_row($respInv)){
            array_push($fuentesUsadas, $rowInv[0]);
        }

        $sqlrFuentes = "SELECT codigo_fuente, nombre FROM ccpet_fuentes_cuipo WHERE version = (SELECT MAX(version) FROM ccpet_fuentes_cuipo)";
        $respFuentes = mysqli_query($linkbd, $sqlrFuentes);
        while($rowFuentes = mysqli_fetch_row($respFuentes)){
            array_push($fuentes, $rowFuentes);
        }

        $out['fuentes'] = $fuentes;
        $out['fuentesUsadas'] = $fuentesUsadas;
    }

    if($action == 'presupuestoInicial'){
        
        //echo $_POST['seccionPresupuestal'];
        $seccionPresupuestal = $_POST['seccionPresupuestal'];
        $medioDePago = $_POST['medioDePago'];
        $vigenciaDelGasto = $_POST['vigenciaDelGasto'];
        
        $vigencia = $_POST['vigencia'];
        $vigencia_ini = $_POST['vigencia_ini'];
        $vigencia_fin = $_POST['vigencia_fin'];

        $fechaIni = $_POST['fechaIni'];
        $fechaFin = $_POST['fechaFin'];

        $tipoGasto = $_POST['tipoGasto'];

        $crit1_inv = '';
        $crit1_inv_P5 = '';
        $crit1_inv_P8 = '';
        $crit1_Ad = '';
        $crit1_Nom = '';
        $crit1_egresp_Nom = '';

        
        $crit3 = '';

        if($seccionPresupuestal != '-1'){
            $crit1Det = "AND DET.seccion_presupuestal = '$seccionPresupuestal'";
            $crit1_inv = "AND P1.idunidadej = '$seccionPresupuestal'";
            $crit1_inv_ini = "AND CP.idunidadej = '$seccionPresupuestal'";
            $crit1_inv_P5 = "AND P5.idunidadej = '$seccionPresupuestal'";
            $crit1_inv_P8 = "AND P8.idunidadej = '$seccionPresupuestal'";
            $crit1_Ad = "AND CA.seccion_presupuestal = '$seccionPresupuestal'";
            $crit1_Nom = "AND HD.seccion_presupuestal = '$seccionPresupuestal'";
            $crit1_egresp_Nom = "AND TD.seccion_presupuestal = '$seccionPresupuestal'";
            
        }

        $crit2 = '';
        $crit2_inv = '';
        $crit2_inv_P6 = '';
        $crit2_inv_P7 = '';
        $crit2_Ad = '';
        $crit2_Nom = '';
        $crit2_egreso_Nom = '';
        

        if($medioDePago != '-1'){

            $crit2 = "AND medio_pago = '$medioDePago'";
            $crit2Det = "AND DET.medio_pago = '$medioDePago'";
            $crit2_inv = "AND P3.medio_pago = '$medioDePago'";
            $crit2_inv_ini = "AND CPD.medio_pago = '$medioDePago'";
            $crit2_inv_P6 = "AND P6.medio_pago = '$medioDePago'";
            $crit2_inv_P9 = "AND P9.medio_pago = '$medioDePago'";
            $crit2_Ad = "AND CA.medio_pago = '$medioDePago'";
            $crit2_Nom = "AND HD.medio_pago = '$medioDePago'";
            $crit2_egreso_Nom = "AND TD.medio_pago = '$medioDePago'";
        }

        $crit3 = "";
        $crit3_inv = "";
        $crit3_inv_ini = "";
        $crit3_inv_P6 = "";
        $crit3_inv_P9 = "";
        $crit3_Ad = "";
        $crit3_Nom = "";
        $crit3_egreso_Nom = "";
        $crit3_egreso_Nom_td = "";

        if($vigenciaDelGasto != '-1'){

            $crit3 = "AND vigencia_gasto = '$vigenciaDelGasto'";
            $crit3_inv = "AND P3.vigencia_gasto = '$vigenciaDelGasto'";
            $crit3_inv_cdp = "AND codigo_vigenciag = '$vigenciaDelGasto'";
            $crit3_inv_cdpDet = "AND DET.codigo_vigenciag = '$vigenciaDelGasto'";
            $crit3_inv_ini = "AND CPD.vigencia_gasto = '$vigenciaDelGasto'";
            $crit3_inv_P6 = "AND P6.codigo_vigenciag = '$vigenciaDelGasto'";
            $crit3_inv_P9 = "AND P9.codigo_vigenciag = '$vigenciaDelGasto'";
            $crit3_Ad = "AND CA.codigo_vigenciag = '$vigenciaDelGasto'";
            $crit3_Nom = "AND HD.vigencia_gasto = '$vigenciaDelGasto'";
            $crit3_egreso_Nom = "AND TD.vigencia_gasto = '$vigenciaDelGasto'";
            $crit3_egreso_Nom_td = "AND TD.codigo_vigenciag = '$vigenciaDelGasto'";

        }

        $cuentasCcpetUsadasProy = array();
        $cuentasCcpetUsadasCdpInv = array();
        $cuentasCcpetUsadasRpInv = array();
        $cuentasCcpetUsadasCxpInv = array();
        $cuentasCcpetUsadasEgresoInv = array();
        $cuentasCcpetUsadasIniInv = array();
        $cuentasCcpetUsadasAdInv = array();
        $cuentasCcpetUsadasRedInv = array();
        $cuentasCcpetUsadasTrasladoCredInv = array();
        $cuentasCcpetUsadasTrasladoContraCredInv = array();

        $x = 0;
        $tamFuentes = count($_POST["fuentes"]);
        do{
            $critFuente = $tamFuentes>0 ? "AND fuente LIKE '%".$_POST["fuentes"][$x]."%'" : '';
            $critFuenteInv = $tamFuentes>0 ? $_POST["fuentes"][$x] : '';

            //adicion inversion
            
            $sqlrProy = "SELECT P2.*, P1.codigo, P1.nombre, P3.id_fuente, P1.idunidadej, P3.vigencia_gasto, P3.medio_pago FROM ccpproyectospresupuesto AS P1, ccpproyectospresupuesto_productos AS P2, ccpproyectospresupuesto_presupuesto AS P3 WHERE (P1.vigencia = '$vigencia_ini' OR P1.vigencia = '$vigencia_fin') AND P1.id = P2.codproyecto AND P1.id=P3.codproyecto AND  P2.indicador = P3.indicador_producto AND P3.id_fuente LIKE '%$critFuenteInv%' $crit1_inv $crit2_inv $crit3_inv UNION SELECT P4.*, P5.codigo, P5.nombre, P6.fuente, P5.idunidadej, P6.codigo_vigenciag, P6.medio_pago FROM ccpproyectospresupuesto AS P5, ccpproyectospresupuesto_productos AS P4, ccpet_adiciones AS P6 WHERE (P5.vigencia = '$vigencia_ini' OR P5.vigencia = '$vigencia_fin') AND P5.id = P4.codproyecto AND P5.codigo=P6.bpim AND P4.indicador = P6.programatico AND P6.fecha BETWEEN '$fechaIni' AND '$fechaFin' AND P6.fuente LIKE '%$critFuenteInv%' $crit1_inv_P5 $crit2_inv_P6 $crit3_inv_P6 UNION SELECT P7.*, P8.codigo, P8.nombre, P9.fuente, P8.idunidadej, P9.codigo_vigenciag, P9.medio_pago FROM ccpproyectospresupuesto AS P8, ccpproyectospresupuesto_productos AS P7, ccpet_traslados AS P9 WHERE (P8.vigencia = '$vigencia_ini' OR P8.vigencia = '$vigencia_fin') AND P9.fecha BETWEEN '$fechaIni' AND '$fechaFin' AND P8.id = P7.codproyecto AND P8.codigo=P9.bpim AND P7.indicador = P9.programatico AND P9.fuente LIKE '%$critFuenteInv%' $crit1_inv_P8 $crit2_inv_P9 $crit3_inv_P9 ORDER BY indicador ASC";

            $respProy = mysqli_query($linkbd, $sqlrProy);
            while($rowProy = mysqli_fetch_row($respProy)){
                array_push($cuentasCcpetUsadasProy, $rowProy);
            }

            //Inicial inversion
            $sqlrIniInv = "SELECT CP.idunidadej, CP.codigo, CPD.id_fuente, CPD.medio_pago, CPD.indicador_producto, CPD.valorcsf, CPD.valorssf, CPD.vigencia_gasto FROM ccpproyectospresupuesto AS CP, ccpproyectospresupuesto_presupuesto AS CPD WHERE (CP.vigencia = '$vigencia_ini' OR CP.vigencia = '$vigencia_fin') AND CP.id=CPD.codproyecto AND CPD.id_fuente LIKE '%$critFuenteInv%' $crit1_inv_ini $crit2_inv_ini $crit3_inv_ini";
            $respIniInv = mysqli_query($linkbd, $sqlrIniInv);
            while($rowIniInv = mysqli_fetch_row($respIniInv)){
                array_push($cuentasCcpetUsadasIniInv, $rowIniInv);
            }
            

            //Adicional inversion
            $sqlrAdInv = "SELECT CA.seccion_presupuestal, CA.bpim, CA.fuente, CA.medio_pago, CA.programatico, SUM(CA.valor), CA.codigo_vigenciag FROM ccpet_adiciones AS CA WHERE (CA.vigencia = '$vigencia_ini' OR CA.vigencia = '$vigencia_fin') $crit1_Ad $crit2_Ad $crit3_Ad AND CA.fecha BETWEEN '$fechaIni' AND '$fechaFin' AND CA.fuente LIKE '%$critFuenteInv%' GROUP BY CA.seccion_presupuestal, CA.bpim, CA.fuente, CA.medio_pago, CA.programatico, CA.codigo_vigenciag";
            //$sqlrIniInv = "SELECT CP.idunidadej, CP.codigo, CPD.id_fuente, CPD.medio_pago, CPD.indicador_producto, CPD.valorcsf, CPD.valorssf, CPD.vigencia_gasto FROM ccpproyectospresupuesto AS CP, ccpproyectospresupuesto_presupuesto AS CPD WHERE CP.vigencia = '$vigencia' AND CP.id=CPD.codproyecto";
            $respAdInv = mysqli_query($linkbd, $sqlrAdInv);
            while($rowAdInv = mysqli_fetch_row($respAdInv)){
                array_push($cuentasCcpetUsadasAdInv, $rowAdInv);
            }

            //Reduccion inversion
            $sqlrRedInv = "SELECT CA.seccion_presupuestal, CA.bpim, CA.fuente, CA.medio_pago, CA.programatico, SUM(CA.valor), CA.codigo_vigenciag FROM ccpet_reducciones AS CA WHERE (CA.vigencia = '$vigencia_ini' OR CA.vigencia = '$vigencia_fin') AND AND CA.fecha BETWEEN '$fechaIni' AND '$fechaFin' AND CA.fuente LIKE '%$critFuenteInv%' $crit1_Ad $crit2_Ad $crit3_Ad GROUP BY CA.seccion_presupuestal, CA.bpim, CA.fuente, CA.medio_pago, CA.programatico, CA.codigo_vigenciag";
            //$sqlrIniInv = "SELECT CP.idunidadej, CP.codigo, CPD.id_fuente, CPD.medio_pago, CPD.indicador_producto, CPD.valorcsf, CPD.valorssf, CPD.vigencia_gasto FROM ccpproyectospresupuesto AS CP, ccpproyectospresupuesto_presupuesto AS CPD WHERE CP.vigencia = '$vigencia' AND CP.id=CPD.codproyecto";
            $respRedInv = mysqli_query($linkbd, $sqlrRedInv);
            while($rowRedInv = mysqli_fetch_row($respRedInv)){
                array_push($cuentasCcpetUsadasRedInv, $rowRedInv);
            }

            //Traslados credito inversion
            $sqlrTrasladoInv = "SELECT CA.seccion_presupuestal, CA.bpim, CA.fuente, CA.medio_pago, CA.programatico, SUM(CA.valor), CA.codigo_vigenciag FROM ccpet_traslados AS CA WHERE (CA.vigencia = '$vigencia_ini' OR CA.vigencia = '$vigencia_fin') AND CA.fecha BETWEEN '$fechaIni' AND '$fechaFin' AND CA.bpim!='' AND CA.tipo = 'C' AND CA.fuente LIKE '%$critFuenteInv%' $crit1_Ad $crit2_Ad $crit3_Ad GROUP BY CA.seccion_presupuestal, CA.bpim, CA.fuente, CA.medio_pago, CA.programatico, CA.codigo_vigenciag";
            //$sqlrIniInv = "SELECT CP.idunidadej, CP.codigo, CPD.id_fuente, CPD.medio_pago, CPD.indicador_producto, CPD.valorcsf, CPD.valorssf, CPD.vigencia_gasto FROM ccpproyectospresupuesto AS CP, ccpproyectospresupuesto_presupuesto AS CPD WHERE CP.vigencia = '$vigencia' AND CP.id=CPD.codproyecto";
            $respTrasladoInv = mysqli_query($linkbd, $sqlrTrasladoInv);
            while($rowTrasladoInv = mysqli_fetch_row($respTrasladoInv)){
                array_push($cuentasCcpetUsadasTrasladoCredInv, $rowTrasladoInv);
            }

            //Traslados contra credito inversion
            $sqlrTrasladoContraInv = "SELECT CA.seccion_presupuestal, CA.bpim, CA.fuente, CA.medio_pago, CA.programatico, SUM(CA.valor), CA.codigo_vigenciag FROM ccpet_traslados AS CA WHERE (CA.vigencia = '$vigencia_ini' OR CA.vigencia = '$vigencia_fin') AND CA.fecha BETWEEN '$fechaIni' AND '$fechaFin' AND CA.bpim!='' AND CA.tipo = 'R' AND CA.fuente LIKE '%$critFuenteInv%' $crit1_Ad $crit2_Ad $crit3_Ad GROUP BY CA.seccion_presupuestal, CA.bpim, CA.fuente, CA.medio_pago, CA.programatico, CA.codigo_vigenciag";
            //$sqlrIniInv = "SELECT CP.idunidadej, CP.codigo, CPD.id_fuente, CPD.medio_pago, CPD.indicador_producto, CPD.valorcsf, CPD.valorssf, CPD.vigencia_gasto FROM ccpproyectospresupuesto AS CP, ccpproyectospresupuesto_presupuesto AS CPD WHERE CP.vigencia = '$vigencia' AND CP.id=CPD.codproyecto";
            $respTrasladoContraInv = mysqli_query($linkbd, $sqlrTrasladoContraInv);
            while($rowTrasladoContraInv = mysqli_fetch_row($respTrasladoContraInv)){
                array_push($cuentasCcpetUsadasTrasladoContraCredInv, $rowTrasladoContraInv);
            }

            //Cdp Inversion
            $sqlrCdpInv = "SELECT DISTINCT DET.seccion_presupuestal, DET.bpim, DET.fuente, DET.medio_pago, DET.indicador_producto, SUM(DET.valor), DET.codigo_vigenciag, CAB.consvigencia, CAB.vigencia FROM ccpetcdp_detalle AS DET, ccpetcdp AS CAB WHERE CAB.consvigencia = DET.consvigencia AND CAB.vigencia = DET.vigencia AND CAB.fecha BETWEEN '$fechaIni' AND '$fechaFin' AND (DET.vigencia = '$vigencia_ini' OR DET.vigencia = '$vigencia_fin') AND SUBSTRING(DET.cuenta, 1, 3) = '2.3' AND CAB.tipo_mov = '201' AND DET.tipo_mov = '201' AND DET.fuente LIKE '%$critFuenteInv%' $crit1Det $crit2Det $crit3_inv_cdpDet GROUP BY DET.seccion_presupuestal, DET.bpim, DET.fuente, DET.medio_pago, DET.indicador_producto, DET.codigo_vigenciag, CAB.consvigencia, CAB.vigencia ";
            $respCdpInv = mysqli_query($linkbd, $sqlrCdpInv);
            while($rowCdpInv = mysqli_fetch_row($respCdpInv)){

                $sqlrCdpInvRev = "SELECT SUM(DET.valor) FROM ccpetcdp_detalle AS DET, ccpetcdp AS CAB WHERE CAB.consvigencia = DET.consvigencia AND CAB.vigencia = DET.vigencia AND CAB.fecha BETWEEN '$fechaIni' AND '$fechaFin' AND CAB.consvigencia = '$rowCdpInv[7]'  AND CAB.vigencia = '$rowCdpInv[8]' AND (DET.vigencia = '$vigencia_ini' OR DET.vigencia = '$vigencia_fin') AND SUBSTRING(DET.cuenta, 1, 3) = '2.3' AND CAB.tipo_mov LIKE '4%' AND DET.tipo_mov LIKE '4%' AND DET.seccion_presupuestal = '$rowCdpInv[0]' AND DET.bpim = '$rowCdpInv[1]' AND DET.fuente = '$rowCdpInv[2]' AND DET.medio_pago = '$rowCdpInv[3]' AND DET.indicador_producto = '$rowCdpInv[4]' AND DET.codigo_vigenciag = '$rowCdpInv[6]' $crit1Det $crit2Det $crit3_inv_cdpDet GROUP BY DET.seccion_presupuestal, DET.bpim, DET.fuente, DET.medio_pago, DET.indicador_producto, DET.codigo_vigenciag";
                $respCdpInvRev = mysqli_query($linkbd, $sqlrCdpInvRev);
                $rowCdpInvRev = mysqli_fetch_row($respCdpInvRev);

                $cdpsInv = [];
                $totalCdpInv = 0;

                $totalCdpInv = $rowCdpInv[5] - $rowCdpInvRev[0];
                array_push($cdpsInv, $rowCdpInv[0]);
                array_push($cdpsInv, $rowCdpInv[1]);
                array_push($cdpsInv, $rowCdpInv[2]);
                array_push($cdpsInv, $rowCdpInv[3]);
                array_push($cdpsInv, $rowCdpInv[4]);
                array_push($cdpsInv, $totalCdpInv);
                array_push($cdpsInv, $rowCdpInv[6]);

                array_push($cuentasCcpetUsadasCdpInv, $cdpsInv);
            }

            //RPS DE INVERSION
            $sqlrRpInv = "SELECT DET.seccion_presupuestal, DET.bpim, DET.fuente, DET.medio_pago, DET.indicador_producto, SUM(DET.valor), DET.codigo_vigenciag, CAB.consvigencia, CAB.vigencia FROM ccpetrp_detalle AS DET, ccpetrp AS CAB WHERE CAB.consvigencia = DET.consvigencia AND CAB.vigencia = DET.vigencia AND CAB.fecha BETWEEN '$fechaIni' AND '$fechaFin' AND (DET.vigencia = '$vigencia_ini' OR DET.vigencia = '$vigencia_fin') AND SUBSTRING(DET.cuenta, 1, 3) = '2.3' AND CAB.tipo_mov = '201' AND DET.tipo_mov = '201' AND CAB.tipo_mov = DET.tipo_mov AND DET.fuente LIKE '%$critFuenteInv%' $crit1Det $crit2Det $crit3_inv_cdpDet GROUP BY DET.seccion_presupuestal, DET.bpim, DET.fuente, DET.medio_pago, DET.indicador_producto, DET.codigo_vigenciag, DET.tipo_mov, CAB.consvigencia, CAB.vigencia ";
            $respRpInv = mysqli_query($linkbd, $sqlrRpInv);
            while($rowRpInv = mysqli_fetch_row($respRpInv)){

                $sqlrRpInvRev = "SELECT SUM(DET.valor) FROM ccpetrp_detalle AS DET, ccpetrp AS CAB WHERE CAB.consvigencia = DET.consvigencia AND CAB.vigencia = DET.vigencia AND CAB.fecha BETWEEN '$fechaIni' AND '$fechaFin' AND CAB.consvigencia = '$rowRpInv[7]' AND CAB.vigencia = '$rowRpInv[8]' AND (DET.vigencia = '$vigencia_ini' OR DET.vigencia = '$vigencia_fin') AND SUBSTRING(DET.cuenta, 1, 3) = '2.3' AND CAB.tipo_mov LIKE '4%' AND DET.tipo_mov LIKE '4%' AND DET.seccion_presupuestal = '$rowRpInv[0]' AND DET.bpim = '$rowRpInv[1]' AND DET.fuente = '$rowRpInv[2]' AND DET.medio_pago = '$rowRpInv[3]' AND  DET.indicador_producto = '$rowRpInv[4]' AND DET.codigo_vigenciag = '$rowRpInv[6]' $crit1Det $crit2Det $crit3_inv_cdpDet GROUP BY DET.seccion_presupuestal, DET.bpim, DET.fuente, DET.medio_pago, DET.indicador_producto, DET.codigo_vigenciag";
                /* if($rowRpInv[7] == '429'){
                    echo $sqlrRpInvRev."separador";
                } */
                $respRpInvRev = mysqli_query($linkbd, $sqlrRpInvRev);
                $rowRpInvRev = mysqli_fetch_row($respRpInvRev);

                $rpsInv = [];
                $totalRpInv = 0;
                $totalRpInv = $rowRpInv[5] - $rowRpInvRev[0];

                array_push($rpsInv, $rowRpInv[0]);
                array_push($rpsInv, $rowRpInv[1]);
                array_push($rpsInv, $rowRpInv[2]);
                array_push($rpsInv, $rowRpInv[3]);
                array_push($rpsInv, $rowRpInv[4]);
                array_push($rpsInv, $totalRpInv);
                array_push($rpsInv, $rowRpInv[6]);

                array_push($cuentasCcpetUsadasRpInv, $rpsInv);
            }

            //Cxp Inversion
            $sqlrCxpInv = "SELECT DISTINCT DET.seccion_presupuestal, DET.bpim, DET.fuente, DET.medio_pago, DET.indicador_producto, SUM(DET.valor), DET.codigo_vigenciag FROM tesoordenpago_det AS DET, tesoordenpago AS CAB WHERE CAB.id_orden = DET.id_orden AND CAB.fecha BETWEEN '$fechaIni' AND '$fechaFin' AND (DET.vigencia = '$vigencia_ini' OR DET.vigencia = '$vigencia_fin') AND SUBSTRING(DET.cuentap, 1, 3) = '2.3' AND CAB.estado != 'R' AND DET.fuente LIKE '%$critFuenteInv%' $crit1Det $crit2Det $crit3_inv_cdpDet GROUP BY DET.seccion_presupuestal, DET.bpim, DET.fuente, DET.medio_pago, DET.indicador_producto, DET.codigo_vigenciag ";
            $respCxpInv = mysqli_query($linkbd, $sqlrCxpInv);
            while($rowCxpInv = mysqli_fetch_row($respCxpInv)){
                array_push($cuentasCcpetUsadasCxpInv, $rowCxpInv);
            }

            //Cxp Inversion Nomina
            $sqlrCxpInvN = "SELECT DISTINCT HD.seccion_presupuestal, HD.bpin, HD.fuente, HD.medio_pago, HD.indicador, SUM(HD.valor), HD.vigencia_gasto FROM humnom_presupuestal AS HD, humnomina_aprobado AS HN WHERE HN.id_nom = HD.id_nom AND HN.fecha BETWEEN '$fechaIni' AND '$fechaFin' AND HD.estado='P' AND SUBSTRING(HD.cuenta, 1, 3) = '2.3' AND HD.fuente LIKE '%$critFuenteInv%' $crit1_Nom $crit2_Nom $crit3_Nom GROUP BY HD.seccion_presupuestal, HD.bpin, HD.fuente, HD.medio_pago, HD.indicador, HD.vigencia_gasto";
            $respCxpInvN = mysqli_query($linkbd, $sqlrCxpInvN);
            while($rowCxpInvN = mysqli_fetch_row($respCxpInvN)){
                array_push($cuentasCcpetUsadasCxpInv, $rowCxpInvN);
            }

            //Egreso Inversion
            $sqlrEgresoInv = "SELECT DISTINCT TD.seccion_presupuestal, TD.bpim, TD.fuente, TD.medio_pago, TD.indicador_producto, SUM(TD.valor), TD.codigo_vigenciag FROM tesoordenpago_det AS TD, tesoegresos AS TE WHERE (TD.vigencia = '$vigencia_ini' OR TD.vigencia = '$vigencia_fin') AND TE.vigencia = '$vigencia_ini' AND TE.fecha BETWEEN '$fechaIni' AND '$fechaFin' AND TE.estado != 'N' AND TE.estado != 'R' AND TD.id_orden = TE.id_orden AND SUBSTRING(TD.cuentap, 1, 3) = '2.3' AND TD.fuente LIKE '%$critFuenteInv%' $crit1_egresp_Nom $crit2_egreso_Nom $crit3_egreso_Nom_td GROUP BY TD.seccion_presupuestal, TD.bpim, TD.fuente, TD.medio_pago, TD.indicador_producto, TD.codigo_vigenciag ";
            $respEgresoInv = mysqli_query($linkbd, $sqlrEgresoInv);
            while($rowEgresoInv = mysqli_fetch_row($respEgresoInv)){
                array_push($cuentasCcpetUsadasEgresoInv, $rowEgresoInv);
            }

            $sqlrEgresoInvN = "SELECT DISTINCT TD.seccion_presupuestal, TD.bpin, TD.fuente, TD.medio_pago, TD.indicador_producto, SUM(TD.valordevengado), TD.vigencia_gasto FROM tesoegresosnomina_det AS TD, tesoegresosnomina AS TE WHERE (TE.vigencia = '$vigencia_ini' OR TE.vigencia = '$vigencia_fin') AND TE.fecha BETWEEN '$fechaIni' AND '$fechaFin' AND TD.id_egreso = TE.id_egreso AND SUBSTRING(TD.cuentap, 1, 3) = '2.3' AND TE.estado = 'S' AND NOT(TD.tipo='SE' OR TD.tipo='PE' OR TD.tipo='DS' OR TD.tipo='RE' OR TD.tipo='FS') AND TD.fuente LIKE '%$critFuenteInv%' $crit1_egresp_Nom $crit2_egreso_Nom $crit3_egreso_Nom GROUP BY TD.seccion_presupuestal, TD.bpin, TD.fuente, TD.medio_pago, TD.indicador_producto,TD.vigencia_gasto";
            $respEgresoInvN = mysqli_query($linkbd, $sqlrEgresoInvN);
            while($rowEgresoInvN = mysqli_fetch_row($respEgresoInvN)){
                array_push($cuentasCcpetUsadasEgresoInv, $rowEgresoInvN);
            }

            $x++;
        }
        while($x < $tamFuentes);

        
        
        
        $out['cuentasCcpetUsadasProy'] = $cuentasCcpetUsadasProy;
        $out['cuentasCcpetUsadasCdpInv'] = $cuentasCcpetUsadasCdpInv;
        $out['cuentasCcpetUsadasRpInv'] = $cuentasCcpetUsadasRpInv;
        $out['cuentasCcpetUsadasCxpInv'] = $cuentasCcpetUsadasCxpInv;
        $out['cuentasCcpetUsadasEgresoInv'] = $cuentasCcpetUsadasEgresoInv;
        $out['cuentasCcpetUsadasIniInv'] = $cuentasCcpetUsadasIniInv;
        $out['cuentasCcpetUsadasAdInv'] = $cuentasCcpetUsadasAdInv;
        $out['cuentasCcpetUsadasRedInv'] = $cuentasCcpetUsadasRedInv;
        $out['cuentasCcpetUsadasTrasladoCredInv'] = $cuentasCcpetUsadasTrasladoCredInv;
        $out['cuentasCcpetUsadasTrasladoContraCredInv'] = $cuentasCcpetUsadasTrasladoContraCredInv;
             

    }

    header("Content-type: application/json");
    echo json_encode($out);
    die();