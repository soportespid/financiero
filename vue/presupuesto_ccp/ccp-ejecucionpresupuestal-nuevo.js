var app = new Vue({
    el: '#myapp',
    components: { Multiselect: window.VueMultiselect.default },
	data:{

        jsonData: [{
            "Nombre": " ",
            "Telfono de contacto": "021-33829544",
            "Direccin de la casa": "1706 Wulian Road, Jinqiao Town, Pudong New Area"
        }],

        mostrarEjecucion: false,
        loading: false,
        seccionesPresupuestales: [],
        selectSeccion: '-1',
        optionsMediosPagos: [
			{ text: 'Todos', value: '-1' },
			{ text: 'CSF', value: 'CSF' },
			{ text: 'SSF', value: 'SSF' }
		],
        selectMedioPago: '-1',
        vigenciasdelgasto: [],
        selectVigenciaGasto: '1',
        years: [],
        vigencia: '',

        clasificadores: [],
        selectClasificador: '-1',

        arbol : [],
        arbolFuncionamiento : [],
        arbolInversion : [],
        presupuestoInicial: [],

        nombreSectores: [],
        nombreProgramas: [],
        nombreSubProgramas: [],
        nombreProductos: [],
        nombreProgramatico: [],

        /*cuentasCcpetUsadas: [],
        cuentasCcpetUsadasProy: [],
        cuentasCcpetUsadasAd: [],
        cuentasCcpetUsadasRed: [],
        cuentasCcpetUsadasCred: [],
        cuentasCcpetUsadasContraCred: [],
        cuentasCcpet: [],
        cuentasCcpetUsadasCdp: [],
        cuentasCcpetUsadasCdpInv: [],
        cuentasCcpetUsadasRpInv: [],
        cuentasCcpetUsadasCxpInv: [],
        cuentasCcpetUsadasEgresoInv: [],
        cuentasCcpetUsadasRp: [],
        cuentasCcpetUsadasCxp: [],
        cuentasCcpetUsadasEgreso: [],
        cuentasCcpetUsadasIniInv: [],
        cuentasCcpetUsadasAdInv: [],
        cuentasCcpetUsadasRedInv: [],
        cuentasCcpetUsadasTrasladoCredInv: [],
        cuentasCcpetUsadasTrasladoContraCredInv: [],*/
        arrArbol:[],

        sumaTotalGastos: [],
        sumaTotalGastosFuncionamiento: [],
        sumaTotalGastosInversion: [],

        indicadorProductoTam: 0,
        productoTam: 0,
        results_det: [],
        cuentaMaxLargo: '',
        fuentes: [],
        valueFuentes: [],
        optionsFuentes: [],
        fuentesUsadas: [],

        tiposDeGasto: [],
        tipoGasto: '-1',

        unidadesEjecutoras: [],
        selectUnidad: '-1',

        basesApp: [],
    },

    computed:{
        /*existeTotalGastosFun(){
            return this.sumaTotalGastosFuncionamiento[0][12] > 0 ? true : false
        },

        existeTotalGastosInv(){
            return this.sumaTotalGastosInversion[0][12] > 0 ? true : false
        },*/
    },

    mounted: async function(){
        await this.cargayears();
        await this.uniEjecutorasSearch();
        this.seccionPresupuestal();
        this.vigenciasDelgasto();
        this.clasificadoresPresupuestales();
        this.fuentesOpciones();
        this.cargarTiposDeGasto();
        document.getElementById('fc_1198971545').value = '01'+'/'+'01'+'/'+this.vigencia;
        let today = new Date();

        // `getDate()` devuelve el día del mes (del 1 al 31)
        let day = today.getDate();

        // `getMonth()` devuelve el mes (de 0 a 11)
        let month = today.getMonth() + 1;


        // `getFullYear()` devuelve el año completo
        let year = today.getFullYear();

        // muestra la fecha de hoy en formato `MM/DD/YYYY`

        let dayFormat = day.toString().padStart(2, '0');
        let monthFormat = month.toString().padStart(2, '0');

        document.getElementById('fc_1198971546').value = `${dayFormat}/${monthFormat}/${year}`;

    },

    methods: {

        cargarTiposDeGasto: async function(){

            await axios.post('vue/presupuesto_ccp/ccp-cdpbasico.php?action=cargarTiposDeGasto')
            .then((response) => {
                app.tiposDeGasto = response.data.tiposDeGasto;

                /* if(response.data.tiposDeGasto.length > 0){
                    this.tipoGasto = response.data.tiposDeGasto[0][0];
                } */
            });
        },

        uniEjecutorasSearch: async function(){

            await axios.post('presupuesto_ccpet/adicion/crear/ccp-adicion.php?action=unidades')
            .then(
                (response)=>{

                    this.unidadesEjecutoras = response.data.uniEjecutoras;
                    this.selectUnidad = this.unidadesEjecutoras[0][0];
                }
            );
            this.bases();
        },

        bases(){
            const basesUnidad = this.unidadesEjecutoras.map(uniEjecutora => {
                return  {
                            base: uniEjecutora[2],
                            usuario: uniEjecutora[6],
                            unidad: uniEjecutora[1],
                            nombre: uniEjecutora[3].toLowerCase(),
                            tipo: uniEjecutora[4],
                            id: uniEjecutora[0],
                        };
            });
            this.basesApp = basesUnidad;
        },

        cambiaCriteriosBusqueda: function(){
            this.arbol = [];
            this.arbolFuncionamiento = [];
            this.arbolInversion = [];
            this.presupuestoInicial = [];
            this.cuentasCcpetUsadas = [];
            this.cuentasCcpetUsadasProy = [];
            this.cuentasCcpetUsadasAd = [];
            this.cuentasCcpetUsadasRed = [];
            this.cuentasCcpetUsadasCred = [];
            this.cuentasCcpetUsadasContraCred = [];
            this.cuentasCcpet = [];
            this.cuentasCcpetUsadasCdp = [];
            this.cuentasCcpetUsadasCdpInv = [];
            this.cuentasCcpetUsadasRpInv = [];
            this.cuentasCcpetUsadasCxpInv = [];
            this.cuentasCcpetUsadasEgresoInv = [];
            this.cuentasCcpetUsadasRp = [];
            this.cuentasCcpetUsadasCxp = [];
            this.cuentasCcpetUsadasEgreso = [];
            this.cuentasCcpetUsadasIniInv = [];
            this.cuentasCcpetUsadasAdInv = [];
            this.cuentasCcpetUsadasRedInv = [];
            this.cuentasCcpetUsadasTrasladoCredInv = [];
            this.cuentasCcpetUsadasTrasladoContraCredInv = [];
            this.mostrarEjecucion = false;

            this.sumaTotalGastos = [];
            this.sumaTotalGastosFuncionamiento = [];
            this.sumaTotalGastosInversion = [];

        },

        generarEjecucion: async function(){

            await this.fuentesOpciones();
            this.cargaPresuInicial();

        },

        cargaPresuInicial: async function(){

            this.arbol = [];
            var formData = new FormData();

            this.valueFuentes.forEach((e, i) => {
                formData.append("fuentes["+i+"]", e.fuente);
            });


            formData.append("seccionPresupuestal", this.selectSeccion);
            formData.append("medioDePago", this.selectMedioPago);
            formData.append("vigenciaDelGasto", this.selectVigenciaGasto);
            formData.append("vigencia", this.vigencia);
            formData.append("tipoGasto", this.tipoGasto);
            let fechaIni = document.getElementById('fc_1198971545').value;
            let fechaFin = document.getElementById('fc_1198971546').value;
            let fechaInicial = '';
            let fechaFinal = '';

            if(fechaIni != undefined && fechaIni != '' && fechaFin != undefined && fechaFin != ''){

                this.loading =  true;

                let fecI = fechaIni.split('/');
                let fecF = fechaFin.split('/');
                fechaInicial = `${fecI[2]}-${fecI[1]}-${fecI[0]}`;
                fechaFinal = `${fecF[2]}-${fecF[1]}-${fecF[0]}`;

                formData.append("fechaIni", fechaInicial);
                formData.append("fechaFin", fechaFinal);
                formData.append("vigencia_ini", fecI[2]);
                formData.append("vigencia_fin", fecF[2]);
                formData.append("unidad", this.selectUnidad);
                if(this.selectUnidad == -1){
                    this.basesApp.map((b, i) => {
                        formData.append("basesApp["+i+"][]", Object.values(b));
                    })
                }else{

                    formData.append("basesApp[][]", Object.values(this.basesApp[this.selectUnidad-1]));
                }

                await axios.post('vue/presupuesto_ccp/ccp-ejecucionpresupuestal-nuevo.php?action=presupuestoInicial',
                formData)
                .then(
                    (response)=>{
                        let fechaIni = document.getElementById('fc_1198971545').value;
                        let fechaFin = document.getElementById('fc_1198971546').value;
                        app.arrArbol = response.data.arbol;
                        let totalInicial = 0;
                        let totalAdicion = 0;
                        let totalReduccion = 0;
                        let totalCredito = 0;
                        let totalContracredito = 0;
                        let totalDefinitivo = 0;
                        let totalDisponibilidad = 0;
                        let totalCompromisos = 0;
                        let totalObligacion = 0;
                        let totalEgreso = 0;
                        let totalSaldo = 0;

                        let funcionamientoInicial = 0;
                        let funcionamientoAdicion = 0;
                        let funcionamientoReduccion = 0;
                        let funcionamientoCredito = 0;
                        let funcionamientoContracredito = 0;
                        let funcionamientoDefinitivo = 0;
                        let funcionamientoDisponibilidad = 0;
                        let funcionamientoCompromisos = 0;
                        let funcionamientoObligacion = 0;
                        let funcionamientoEgreso = 0;
                        let funcionamientoSaldo = 0;

                        let inversionInicial = 0;
                        let inversionAdicion = 0;
                        let inversionReduccion = 0;
                        let inversionCredito = 0;
                        let inversionContracredito = 0;
                        let inversionDefinitivo = 0;
                        let inversionDisponibilidad = 0;
                        let inversionCompromisos = 0;
                        let inversionObligacion = 0;
                        let inversionEgreso = 0;
                        let inversionSaldo = 0;

                        let html = "";
                        app.arrArbol.forEach(function(e){
                            const arrInfo = e.info;
                            htmlDet ="";
                            arrInfo.forEach(function(info){
                                htmlDet+=`
                                    <tr class="fw-bold bg-blue-water">
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td>${info.tipo_nombre == "Subprograma" ? info.codigo_subprograma : info.codigo}</td>
                                        <td>${info.tipo_nombre ? info.tipo_nombre+": "+info.nombre : info.nombre}</td>
                                        <td class="text-center">${info.tipo}</td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td class="text-right">${app.formatonumero(info.total_inicial)}</td>
                                        <td class="text-right">${app.formatonumero(info.total_adicion)}</td>
                                        <td class="text-right">${app.formatonumero(info.total_reduccion)}</td>
                                        <td class="text-right">${app.formatonumero(info.total_credito)}</td>
                                        <td class="text-right">${app.formatonumero(info.total_contracredito)}</td>
                                        <td class="text-right">${app.formatonumero(info.total_definitivo)}</td>
                                        <td class="text-right">${app.formatonumero(info.total_disponibilidad)}</td>
                                        <td class="text-right">${app.formatonumero(info.total_compromisos)}</td>
                                        <td class="text-right">${app.formatonumero(info.total_obligacion)}</td>
                                        <td class="text-right">${app.formatonumero(info.total_egreso)}</td>
                                        <td class="text-right">${app.formatonumero(info.total_saldo)}</td>
                                    </tr>
                                `;
                                const arrDet = info.detalle ? info.detalle : [];
                                if(arrDet.length>0){
                                    info.detalle.forEach(function(det){
                                        let bgWarning = "";
                                        let url = "ccp-auxiliarGastosCuenta.php?vig="+det.codigo_vigencia+"&sec="+det.codigo_seccion+
                                        "&cuenta="+det.codigo_cuenta+"&fuente="+det.codigo_fuente+"&medio="+det.codigo_pago+"&fechaIni="+fechaIni+"&fechaFin="+fechaFin;

                                        if(Math.round(det.saldo) < 0 || Math.round(det.disponibilidad) < Math.round(det.compromisos) ||
                                        Math.round(det.compromisos) < Math.round(det.obligacion) || Math.round(det.obligacion) < Math.round(det.egreso)
                                        ){
                                            bgWarning = "bg-warning fw-bold";
                                        }
                                        htmlDet+=`
                                            <tr class="${bgWarning}" ondblclick="mypop=window.open('${url}','',''); mypop.focus();">
                                                <td>${det.codigo_unidad+" - "+det.nombre_unidad}</td>
                                                <td class="text-center">${det.codigo_seccion}</td>
                                                <td class="text-center">${det.codigo_vigencia}</td>
                                                <td>${det.codigo_cuenta}</td>
                                                <td>${det.nombre_cuenta}</td>
                                                <td class="text-center">${det.tipo}</td>
                                                <td>${det.codigo_fuente}</td>
                                                <td>${det.nombre_fuente}</td>
                                                <td>${det.codigo_pago}</td>
                                                <td class="text-right">${app.formatonumero(det.inicial)}</td>
                                                <td class="text-right">${app.formatonumero(det.adicion)}</td>
                                                <td class="text-right">${app.formatonumero(det.reduccion)}</td>
                                                <td class="text-right">${app.formatonumero(det.credito)}</td>
                                                <td class="text-right">${app.formatonumero(det.contracredito)}</td>
                                                <td class="text-right">${app.formatonumero(det.definitivo)}</td>
                                                <td class="text-right">${app.formatonumero(det.disponibilidad)}</td>
                                                <td class="text-right">${app.formatonumero(det.compromisos)}</td>
                                                <td class="text-right">${app.formatonumero(det.obligacion)}</td>
                                                <td class="text-right">${app.formatonumero(det.egreso)}</td>
                                                <td class="text-right">${app.formatonumero(det.saldo)}</td>
                                            </tr>
                                        `;
                                    });
                                }
                            })
                            html+=`
                                <tr class="fw-bold" style="position: sticky; top: 0; z-index: 1;">
                                    <td colspan="20" class="text-center bg-primary text-white">${e.nombre}</td>
                                </tr>
                                <tr class="fw-bold text-center" style="position: sticky; top: 5.5%; z-index: 1;" class="text-center">
                                    <td class="bg-primary text-white">Unidad</td>
                                    <td class="bg-primary text-white">Sec. Presu</td>
                                    <td class="bg-primary text-white">Vig. gasto</td>
                                    <td class="bg-primary text-white">Rubro</td>
                                    <td class="bg-primary text-white">Nombre</td>
                                    <td class="bg-primary text-white">Tipo</td>
                                    <td class="bg-primary text-white">Fuente</td>
                                    <td class="bg-primary text-white">Nombre Fuente</td>
                                    <td class="bg-primary text-white">CSF</td>
                                    <td class="bg-primary text-white">Inicial</td>
                                    <td class="bg-primary text-white">Adici&oacute;n</td>
                                    <td class="bg-primary text-white">Reducci&oacute;n</td>
                                    <td class="bg-primary text-white">Credito</td>
                                    <td class="bg-primary text-white">Contracredito</td>
                                    <td class="bg-primary text-white">Definitivo</td>
                                    <td class="bg-primary text-white">Disponibilidad</td>
                                    <td class="bg-primary text-white">Compromisos</td>
                                    <td class="bg-primary text-white">Oblicaci&oacute;n</td>
                                    <td class="bg-primary text-white">Egreso</td>
                                    <td class="bg-primary text-white">Saldo</td>
                                </tr>
                                <tr class="fw-bold bg-secondary">
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td>Total</td>
                                    <td>Total gastos</td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td class="text-right">${app.formatonumero(e.total.total_inicial)}</td>
                                    <td class="text-right">${app.formatonumero(e.total.total_adicion)}</td>
                                    <td class="text-right">${app.formatonumero(e.total.total_reduccion)}</td>
                                    <td class="text-right">${app.formatonumero(e.total.total_credito)}</td>
                                    <td class="text-right">${app.formatonumero(e.total.total_contracredito)}</td>
                                    <td class="text-right">${app.formatonumero(e.total.total_definitivo)}</td>
                                    <td class="text-right">${app.formatonumero(e.total.total_disponibilidad)}</td>
                                    <td class="text-right">${app.formatonumero(e.total.total_compromisos)}</td>
                                    <td class="text-right">${app.formatonumero(e.total.total_obligacion)}</td>
                                    <td class="text-right">${app.formatonumero(e.total.total_egreso)}</td>
                                    <td class="text-right">${app.formatonumero(e.total.total_saldo)}</td>
                                </tr>
                                <tr class="fw-bold bg-secondary">
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td>Total</td>
                                    <td>Total Gastos de Funcionamiento, servicio de la deuda</td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td class="text-right">${app.formatonumero(e.funcionamiento.total_inicial)}</td>
                                    <td class="text-right">${app.formatonumero(e.funcionamiento.total_adicion)}</td>
                                    <td class="text-right">${app.formatonumero(e.funcionamiento.total_reduccion)}</td>
                                    <td class="text-right">${app.formatonumero(e.funcionamiento.total_credito)}</td>
                                    <td class="text-right">${app.formatonumero(e.funcionamiento.total_contracredito)}</td>
                                    <td class="text-right">${app.formatonumero(e.funcionamiento.total_definitivo)}</td>
                                    <td class="text-right">${app.formatonumero(e.funcionamiento.total_disponibilidad)}</td>
                                    <td class="text-right">${app.formatonumero(e.funcionamiento.total_compromisos)}</td>
                                    <td class="text-right">${app.formatonumero(e.funcionamiento.total_obligacion)}</td>
                                    <td class="text-right">${app.formatonumero(e.funcionamiento.total_egreso)}</td>
                                    <td class="text-right">${app.formatonumero(e.funcionamiento.total_saldo)}</td>
                                </tr>
                                <tr class="fw-bold bg-secondary">
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td>Total</td>
                                    <td>Total Gastos de Inversion</td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td class="text-right">${app.formatonumero(e.inversion.total_inicial)}</td>
                                    <td class="text-right">${app.formatonumero(e.inversion.total_adicion)}</td>
                                    <td class="text-right">${app.formatonumero(e.inversion.total_reduccion)}</td>
                                    <td class="text-right">${app.formatonumero(e.inversion.total_credito)}</td>
                                    <td class="text-right">${app.formatonumero(e.inversion.total_contracredito)}</td>
                                    <td class="text-right">${app.formatonumero(e.inversion.total_definitivo)}</td>
                                    <td class="text-right">${app.formatonumero(e.inversion.total_disponibilidad)}</td>
                                    <td class="text-right">${app.formatonumero(e.inversion.total_compromisos)}</td>
                                    <td class="text-right">${app.formatonumero(e.inversion.total_obligacion)}</td>
                                    <td class="text-right">${app.formatonumero(e.inversion.total_egreso)}</td>
                                    <td class="text-right">${app.formatonumero(e.inversion.total_saldo)}</td>
                                </tr>
                                ${htmlDet}
                            `;
                            totalInicial += e.total.total_inicial;
                            totalAdicion +=e.total.total_adicion;
                            totalReduccion +=e.total.total_reduccion;
                            totalCredito +=e.total.total_credito;
                            totalContracredito +=e.total.total_contracredito;
                            totalDefinitivo +=e.total.total_definitivo;
                            totalDisponibilidad +=e.total.total_disponibilidad;
                            totalCompromisos +=e.total.total_compromisos;
                            totalObligacion +=e.total.total_obligacion;
                            totalEgreso +=e.total.total_egreso;
                            totalSaldo +=e.total.total_saldo;

                            funcionamientoInicial += e.funcionamiento.total_inicial;
                            funcionamientoAdicion +=e.funcionamiento.total_adicion;
                            funcionamientoReduccion +=e.funcionamiento.total_reduccion;
                            funcionamientoCredito +=e.funcionamiento.total_credito;
                            funcionamientoContracredito +=e.funcionamiento.total_contracredito;
                            funcionamientoDefinitivo +=e.funcionamiento.total_definitivo;
                            funcionamientoDisponibilidad +=e.funcionamiento.total_disponibilidad;
                            funcionamientoCompromisos +=e.funcionamiento.total_compromisos;
                            funcionamientoObligacion +=e.funcionamiento.total_obligacion;
                            funcionamientoEgreso +=e.funcionamiento.total_egreso;
                            funcionamientoSaldo +=e.funcionamiento.total_saldo;

                            inversionInicial +=  e.inversion.total_inicial;
                            inversionAdicion += e.inversion.total_adicion;
                            inversionReduccion += e.inversion.total_reduccion;
                            inversionCredito += e.inversion.total_credito;
                            inversionContracredito += e.inversion.total_contracredito;
                            inversionDefinitivo += e.inversion.total_definitivo;
                            inversionDisponibilidad += e.inversion.total_disponibilidad;
                            inversionCompromisos += e.inversion.total_compromisos;
                            inversionObligacion += e.inversion.total_obligacion;
                            inversionEgreso += e.inversion.total_egreso;
                            inversionSaldo += e.inversion.total_saldo;
                        });
                        let htmlTotal ="";
                        if(app.selectUnidad == -1){
                            htmlTotal = `
                                <tr class="fw-bold" style="position: sticky; top: 0; z-index: 1;">
                                        <td colspan="20" class="text-center bg-primary text-white">TOTAL UNIDADES EJECUTORAS</td>
                                </tr>
                                <tr class="fw-bold text-center" style="position: sticky; top: 5.5%; z-index: 1;" class="text-center">
                                    <td class="bg-primary text-white">Unidad</td>
                                    <td class="bg-primary text-white">Sec. Presu</td>
                                    <td class="bg-primary text-white">Vig. gasto</td>
                                    <td class="bg-primary text-white">Rubro</td>
                                    <td class="bg-primary text-white">Nombre</td>
                                    <td class="bg-primary text-white">Tipo</td>
                                    <td class="bg-primary text-white">Fuente</td>
                                    <td class="bg-primary text-white">Nombre Fuente</td>
                                    <td class="bg-primary text-white">CSF</td>
                                    <td class="bg-primary text-white">Inicial</td>
                                    <td class="bg-primary text-white">Adici&oacute;n</td>
                                    <td class="bg-primary text-white">Reducci&oacute;n</td>
                                    <td class="bg-primary text-white">Credito</td>
                                    <td class="bg-primary text-white">Contracredito</td>
                                    <td class="bg-primary text-white">Definitivo</td>
                                    <td class="bg-primary text-white">Disponibilidad</td>
                                    <td class="bg-primary text-white">Compromisos</td>
                                    <td class="bg-primary text-white">Oblicaci&oacute;n</td>
                                    <td class="bg-primary text-white">Egreso</td>
                                    <td class="bg-primary text-white">Saldo</td>
                                </tr>
                                <tr class="fw-bold bg-secondary">
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td>Total</td>
                                    <td>Total gastos</td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td class="text-right">${app.formatonumero(totalInicial)}</td>
                                    <td class="text-right">${app.formatonumero(totalAdicion)}</td>
                                    <td class="text-right">${app.formatonumero(totalReduccion)}</td>
                                    <td class="text-right">${app.formatonumero(totalCredito)}</td>
                                    <td class="text-right">${app.formatonumero(totalContracredito)}</td>
                                    <td class="text-right">${app.formatonumero(totalDefinitivo)}</td>
                                    <td class="text-right">${app.formatonumero(totalDisponibilidad)}</td>
                                    <td class="text-right">${app.formatonumero(totalCompromisos)}</td>
                                    <td class="text-right">${app.formatonumero(totalObligacion)}</td>
                                    <td class="text-right">${app.formatonumero(totalEgreso)}</td>
                                    <td class="text-right">${app.formatonumero(totalSaldo)}</td>
                                </tr>
                                <tr class="fw-bold bg-secondary">
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td>Total</td>
                                    <td>Total Gastos de Funcionamiento, servicio de la deuda</td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td class="text-right">${app.formatonumero(funcionamientoInicial)}</td>
                                    <td class="text-right">${app.formatonumero(funcionamientoAdicion)}</td>
                                    <td class="text-right">${app.formatonumero(funcionamientoReduccion)}</td>
                                    <td class="text-right">${app.formatonumero(funcionamientoCredito)}</td>
                                    <td class="text-right">${app.formatonumero(funcionamientoContracredito)}</td>
                                    <td class="text-right">${app.formatonumero(funcionamientoDefinitivo)}</td>
                                    <td class="text-right">${app.formatonumero(funcionamientoDisponibilidad)}</td>
                                    <td class="text-right">${app.formatonumero(funcionamientoCompromisos)}</td>
                                    <td class="text-right">${app.formatonumero(funcionamientoObligacion)}</td>
                                    <td class="text-right">${app.formatonumero(funcionamientoEgreso)}</td>
                                    <td class="text-right">${app.formatonumero(funcionamientoSaldo)}</td>
                                </tr>
                                <tr class="fw-bold bg-secondary">
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td>Total</td>
                                    <td>Total Gastos de Inversion</td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td class="text-right">${app.formatonumero(inversionInicial)}</td>
                                    <td class="text-right">${app.formatonumero(inversionAdicion)}</td>
                                    <td class="text-right">${app.formatonumero(inversionReduccion)}</td>
                                    <td class="text-right">${app.formatonumero(inversionCredito)}</td>
                                    <td class="text-right">${app.formatonumero(inversionContracredito)}</td>
                                    <td class="text-right">${app.formatonumero(inversionDefinitivo)}</td>
                                    <td class="text-right">${app.formatonumero(inversionDisponibilidad)}</td>
                                    <td class="text-right">${app.formatonumero(inversionCompromisos)}</td>
                                    <td class="text-right">${app.formatonumero(inversionObligacion)}</td>
                                    <td class="text-right">${app.formatonumero(inversionEgreso)}</td>
                                    <td class="text-right">${app.formatonumero(inversionSaldo)}</t
                                </tr>
                            `;
                        }
                        html = `
                        <table class="table fw-normal table-hover">
                            <tbody>
                                ${htmlTotal}
                                ${html}
                            </tbody>
                        </table>
                        `
                        document.querySelector("#tableId").innerHTML =html;

                    }
                ).finally(() => {
                    this.mostrarEjecucion = true;
                    this.loading =  false;
                });

            }else{

                Swal.fire(
                    'Falta escoger fechas.',
                    'Escoja la fecha inicial y fecha final',
                    'warning'
                    )

            }

        },

        seccionPresupuestal: async function(){
            var formData = new FormData();

            if(this.selectUnidad == -1){
                this.seccionesPresupuestales = [];
            }else{
                formData.append("base", this.unidadesEjecutoras[this.selectUnidad-1][2]);
                formData.append("usuario", this.unidadesEjecutoras[this.selectUnidad-1][6]);

                await axios.post('vue/presupuesto_ccp/ccp-inicialgastosfun.php?action=secciones', formData)
                .then(
                    (response)=>{
                        this.seccionesPresupuestales = response.data.secciones;
                    }
                );
            }

            this.cambiaCriteriosBusqueda();
        },

        vigenciasDelgasto: async function(){
            await axios.post('vue/presupuesto_ccp/ccp-inicialgastosfun.php?action=vigenciasDelGasto')
                .then(
                    (response) => {
                        this.vigenciasdelgasto = response.data.vigenciasDelGasto;
                        /* this.selectVigenciaGasto = this.vigenciasdelgasto[0][0]; */
                    }
                );
        },

        async fuentesOpciones(){
            let formDataDet = new FormData();
            let fechaIni = document.getElementById('fc_1198971545').value;

            let fechaPartes = fechaIni.split("/");
            let vigFuentes = !fechaIni ? this.vigencia : fechaPartes[2];

            formDataDet.append("vigencia", vigFuentes)
            await axios.post('vue/presupuesto_ccp/ccp-ejecucionpresupuestal-nuevo.php?action=fuentes', formDataDet)
                .then(
                    (response) => {
                        this.fuentes = response.data.fuentes;
                        this.fuentesUsadas = response.data.fuentesUsadas;
                        /* this.vigenciasdelgasto = response.data.vigenciasDelGasto; */
                        /* this.selectVigenciaGasto = this.vigenciasdelgasto[0][0]; */
                    }
                );
            const dataArrSet = new Set(this.fuentesUsadas);
            const dataArr = Array.from(dataArrSet);
            const fuentesOrg = await dataArr.map((e) => {
                return {fuente : e, nombre : this.buscarFuente(e)}
            });
            this.optionsFuentes = fuentesOrg;
        },

        fuenteConNombre({ fuente, nombre }){
            return `${fuente} - ${nombre}`
        },

        clasificadoresPresupuestales: async function(){
            await axios.post('presupuesto_ccpet/clasificadores/ccp-buscaclasificadoresgastos.php')
                .then(
                    (response) => {
                        this.clasificadores = response.data.clasificadores;
                        /* this.selectVigenciaGasto = this.vigenciasdelgasto[0][0]; */
                    }
                );
        },

        async cuentasDelClasificador(){

            this.results_det = [];
            let formDataDet = new FormData();
            formDataDet.append("id_clasificador", this.selectClasificador)
            await axios.post('presupuesto_ccpet/clasificadores/ccp-buscaclasificadoresgastos.php?action=search_clasificador_det_aux', formDataDet)
                .then(
                    (response) => {
                        this.results_det = response.data.clasificadores_det;
                        /* this.selectVigenciaGasto = this.vigenciasdelgasto[0][0]; */
                    }
                );
            this.cambiaCriteriosBusqueda();
        },

        cargayears: async function(){
			await axios.post('vue/presupuesto_ccp/ccp-bancoproyectos.php?buscar=years')
			.then(
				(response)=>
				{
					this.years=response.data.anio;
					var idanio=response.data.anio.length -1;
					if (idanio>=0){this.vigencia = response.data.anio[idanio][0];}
					else{this.vigencia ='';}
				}
            );

		},

        buscarCuentas: async function(){

            await axios.post('vue/presupuesto_ccp/ccp-ejecucionpresupuestal-nuevo.php?vigencia='+this.vigencia)
            .then(function(response){


            });

        },

        buscarFuente(fuente){
            const nombFuente = this.fuentes.find(e => e[0] == fuente)
            const nombF = typeof nombFuente == 'object' ? Object.values(nombFuente) : ''

            return typeof nombFuente == 'string' ? '' : nombF[1];
        },


        formatNumber: function (value) {

            return numeralIntl.NumberFormat(value);
        },

        downloadExl() {

            if (this.arrArbol != '') {
                const fechaIni = document.getElementById('fc_1198971545').value;
                const fechaFin = document.getElementById('fc_1198971546').value;
                const form = document.createElement("form");
                form.method ="post";
                form.target="_blank";
                form.action="vue/presupuesto_ccp/ccp-ejecucionpresupuestal-export-excel.php";

                function addField(name,value){
                    const input = document.createElement("input");
                    input.type="hidden";
                    input.name=name;
                    input.value = value;
                    form.appendChild(input);
                }
                addField("action","excel");
                addField("fecha_inicial",fechaIni);
                addField("fecha_final",fechaFin);
                addField("data",JSON.stringify(this.arrArbol));
                document.body.appendChild(form);
                form.submit();
                document.body.removeChild(form);
            }
            else {
                Swal.fire("Faltan datos", "warning");
            }

        },

        toFormData: function(obj){
            var form_data = new FormData();
            for(var key in obj){
                form_data.append(key, obj[key]);
            }
            return form_data;
        },

        formatonumero: function(valor){
			return new Intl.NumberFormat("en-US", {style: "currency", currency: "USD"}).format(valor);
		},

    }
});
