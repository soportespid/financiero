<?php
	require '../../comun.inc';
	$linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");
	$out = array('error' => false);

	$action="show";

    if(isset($_GET['action'])){
        $action=$_GET['action'];
    }

    if($action == "show")
    {

        //Creacion de variables
        $acuerdos = array();
        $consecutivo;

        $sqlr = "SELECT id_acuerdo, numero_acuerdo, consecutivo FROM ccpetacuerdos WHERE valoradicion > 0";
        $res = mysqli_query($linkbd,$sqlr);

        while($row=mysqli_fetch_row($res))
        {
            array_push($acuerdos, $row);
        }
        $out['acuerdos'] = $acuerdos;

        $sqlConsecutivo = "SELECT MAX(id) FROM tesosuperavit ";
        $resConsecutivo = mysqli_query($linkbd,$sqlConsecutivo);
        $rowConsecutivo = mysqli_fetch_row($resConsecutivo);

        $consecutivo = $rowConsecutivo[0] + 1;

        if(strlen($consecutivo) === 1)
        {
            $consecutivo = '0'.$consecutivo;
        }

        $out['consecutivo'] = $consecutivo;
    }

    if ($action == "guardar")
    {
        //echo "golaa";
        for($x = 0; $x < count($_POST['cabecera']); $x++){

            $cabecera = explode(",", $_POST['cabecera'][$x]);

            $fecha = '';

            $fec = explode("/", $cabecera[2]);

            $fecha = $fec[2].'-'.$fec[1].'-'.$fec[0];


            //var informacionCabecera = [this.numero,this.acuerdo,fecha,this.vigencia,this.concepto,this.valorTotal];

            $sql = "INSERT INTO tesosuperavit(id,id_acuerdo,concepto,fecha,vigencia,valor,estado) VALUES ($cabecera[0],$cabecera[1],'$cabecera[4]','$fecha',$cabecera[3],$cabecera[5],'S')";
            mysqli_query($linkbd, $sql);

            $out['insertaBien'] = true;

        }

        for($x = 0; $x < count($_POST['detalles']); $x++)
        {
            $detalles = explode(",", $_POST['detalles'][$x]);

            $sql = "INSERT INTO tesosuperavit_det(id_tesosuperavit,rubro,fuente,vigencia_gasto,clasificacion,clasificador,valor,seccion_presupuestal,medio_pago,clasificador_superavit) VALUES ($cabecera[0],'$detalles[0]','$detalles[1]','$detalles[2]','$detalles[5]','$detalles[3]',$detalles[4],'$detalles[6]','$detalles[7]','$detalles[8]')";
            mysqli_query($linkbd, $sql);
            $out['insertaBien'] = true;
        }
    }

    if(isset($_GET['buscar']))
	{
		$buscar=$_GET['buscar'];

		switch ($buscar)
		{
			case 'buscarAcuerdos':
            {
                $id_acuerdo = $_GET['acuerdo'];
                $vigencia   = $_GET['vigencia'];
                $codigos = array();

                $sqlDetalles = "SELECT cuenta,fuente,codigo_vigenciag,medio_pago,valor,seccion_presupuestal,medio_pago,clasificador FROM ccpet_adiciones WHERE id_acuerdo = '$id_acuerdo' and vigencia = '$vigencia' AND cuenta LIKE '1%' ";
                $resDetalles = mysqli_query($linkbd,$sqlDetalles);
                while($rowDetalles = mysqli_fetch_row($resDetalles))
                {
                    array_push($codigos,$rowDetalles);
                }


                $out['codigos'] = $codigos;

            }
		}
	}


header("Content-type: application/json");
echo json_encode($out);
die();

?>
