var app = new Vue({
    el: '#myapp',
    components: { Multiselect: window.VueMultiselect.default },
	data:{

        jsonData: [{
            "Nombre": " ",
            "Telfono de contacto": "021-33829544",
            "Direccin de la casa": "1706 Wulian Road, Jinqiao Town, Pudong New Area"
        }],

        mostrarEjecucion: false,
        loading: false,
        seccionesPresupuestales: [],
        selectSeccion: '-1',
        optionsMediosPagos: [
			{ text: 'Todos', value: '-1' },
			{ text: 'CSF', value: 'CSF' },
			{ text: 'SSF', value: 'SSF' }
		],
        selectMedioPago: '-1',
        vigenciasdelgasto: [],
        selectVigenciaGasto: '-1',
        years: [],
        vigencia: '',

        clasificadores: [],
        selectClasificador: '-1',

        arbol : [],
        arbolInversion : [],

        nombreSectores: [],
        nombreProgramas: [],
        nombreSubProgramas: [],
        nombreSubProgramasOp: [],
        nombreProductos: [],
        nombreProgramatico: [],

        nombreSectoresCopia: [],
        nombreSubProgramasOpCopia: [],
        nombreProductosCopia: [],

        cuentasCcpetUsadasProy: [],
        cuentasCcpetUsadasCdpInv: [],
        cuentasCcpetUsadasRpInv: [],
        cuentasCcpetUsadasCxpInv: [],
        cuentasCcpetUsadasEgresoInv: [],
        cuentasCcpetUsadasIniInv: [],
        cuentasCcpetUsadasAdInv: [],
        cuentasCcpetUsadasRedInv: [],
        cuentasCcpetUsadasTrasladoCredInv: [],
        cuentasCcpetUsadasTrasladoContraCredInv: [],

        sumaTotalGastos: [],
        sumaTotalGastosInversion: [],

        indicadorProductoTam: 0,
        productoTam: 0,
        results_det: [],
        fuentes: [],
        valueFuentes: [],
        optionsFuentes: [],
        fuentesUsadas: [],

        tiposDeGasto: [],
        tipoGasto: '-1',

        valueSectores: [],
        optionsSectores: [],

        valueProgramas: [],
        optionsProgramas: [],

        valueSubProgramas: [],
        optionsSubProgramas: [],

        valueProductos: [],
        optionsProductos: [],

        niveles: [
                { text: 'Todos', value: '-1' },
                { text: 'Sector', value: '1' },
                { text: 'Programa', value: '2' },
                { text: 'Subprograma', value: '3' },
                { text: 'Producto', value: '4' },
                { text: 'Programático', value: '5' },
            ],
        selectNivel: '-1',
        cambiaSector: false,
        cambiaPrograma: false,
        cambiaSubPrograma: false,
    },



    mounted: async function(){
        await this.cargayears();

        this.seccionPresupuestal();
        this.vigenciasDelgasto();
        this.clasificadoresPresupuestales();
        this.fuentesOpciones();
        this.cargarClasificadoresDeInversion();
        document.getElementById('fc_1198971545').value = '01'+'/'+'01'+'/'+this.vigencia;
        let today = new Date();

        // `getDate()` devuelve el día del mes (del 1 al 31)
        let day = today.getDate();

        // `getMonth()` devuelve el mes (de 0 a 11)
        let month = today.getMonth() + 1;


        // `getFullYear()` devuelve el año completo
        let year = today.getFullYear();

        // muestra la fecha de hoy en formato `MM/DD/YYYY`

        let dayFormat = day.toString().padStart(2, '0');
        let monthFormat = month.toString().padStart(2, '0');

        document.getElementById('fc_1198971546').value = `${dayFormat}/${monthFormat}/${year}`;

    },

    methods: {

        cambiaCriteriosBusqueda: function(){
            this.arbol = [];
            this.arbolInversion = [];
            this.cuentasCcpetUsadasProy = [];
            this.cuentasCcpetUsadasCdpInv = [];
            this.cuentasCcpetUsadasRpInv = [];
            this.cuentasCcpetUsadasCxpInv = [];
            this.cuentasCcpetUsadasEgresoInv = [];
            this.cuentasCcpetUsadasIniInv = [];
            this.cuentasCcpetUsadasAdInv = [];
            this.cuentasCcpetUsadasRedInv = [];
            this.cuentasCcpetUsadasTrasladoCredInv = [];
            this.cuentasCcpetUsadasTrasladoContraCredInv = [];
            this.mostrarEjecucion = false;

            this.sumaTotalGastos = [];
            this.sumaTotalGastosInversion = [];

        },

        generarEjecucion: async function(){

            await this.fuentesOpciones();
            this.cargaPresuInicial();

        },



        cargaPresuInicial: async function(){

            this.arbol = [];
            this.arbolInversion = [];
            this.cuentasCcpetUsadasProy = [];
            this.cuentasCcpetUsadasCdpInv = [];
            this.cuentasCcpetUsadasRpInv = [];
            this.cuentasCcpetUsadasCxpInv = [];
            this.cuentasCcpetUsadasEgresoInv = [];
            this.cuentasCcpetUsadasIniInv = [];
            this.cuentasCcpetUsadasAdInv = [];
            this.cuentasCcpetUsadasRedInv = [];
            this.cuentasCcpetUsadasTrasladoCredInv = [];
            this.cuentasCcpetUsadasTrasladoContraCredInv = [];

            this.sumaTotalGastos = [];
            this.sumaTotalGastosInversion = [];

            var formData = new FormData();

            this.valueFuentes.forEach((e, i) => {
                formData.append("fuentes["+i+"]", e.fuente);
            });


            formData.append("seccionPresupuestal", this.selectSeccion);
            formData.append("medioDePago", this.selectMedioPago);
            formData.append("vigenciaDelGasto", this.selectVigenciaGasto);
            formData.append("vigencia", this.vigencia);
            let fechaIni = document.getElementById('fc_1198971545').value;
            let fechaFin = document.getElementById('fc_1198971546').value;
            let fechaInicial = '';
            let fechaFinal = '';

            if(fechaIni != undefined && fechaIni != '' && fechaFin != undefined && fechaFin != ''){

                this.loading =  true;

                let fecI = fechaIni.split('/');
                let fecF = fechaFin.split('/');
                fechaInicial = `${fecI[2]}-${fecI[1]}-${fecI[0]}`;
                fechaFinal = `${fecF[2]}-${fecF[1]}-${fecF[0]}`;

                formData.append("fechaIni", fechaInicial);
                formData.append("fechaFin", fechaFinal);
                formData.append("vigencia_ini", fecI[2]);
                formData.append("vigencia_fin", fecF[2]);

                await axios.post('vue/presupuesto_ccp/ccp-ejecucionpresupuestal-inv.php?action=presupuestoInicial',
                formData)
                .then(
                    (response)=>{
                        app.cuentasCcpetUsadasProy = response.data.cuentasCcpetUsadasProy;
                        app.cuentasCcpetUsadasCdpInv = response.data.cuentasCcpetUsadasCdpInv;
                        app.cuentasCcpetUsadasRpInv = response.data.cuentasCcpetUsadasRpInv;
                        app.cuentasCcpetUsadasCxpInv = response.data.cuentasCcpetUsadasCxpInv;
                        app.cuentasCcpetUsadasEgresoInv = response.data.cuentasCcpetUsadasEgresoInv;
                        app.cuentasCcpetUsadasIniInv = response.data.cuentasCcpetUsadasIniInv;
                        app.cuentasCcpetUsadasAdInv = response.data.cuentasCcpetUsadasAdInv;
                        app.cuentasCcpetUsadasRedInv = response.data.cuentasCcpetUsadasRedInv;
                        app.cuentasCcpetUsadasTrasladoCredInv = response.data.cuentasCcpetUsadasTrasladoCredInv;
                        app.cuentasCcpetUsadasTrasladoContraCredInv = response.data.cuentasCcpetUsadasTrasladoContraCredInv;


                    }
                ).finally(() => {
                    this.mostrarEjecucion = true;
                    this.loading =  false;
                });

                await this.buscarCuentasClasificador();
                this.cuentasMayoresAux();

            }else{

                Swal.fire(
                    'Falta escoger fechas.',
                    'Escoja la fecha inicial y fecha final',
                    'warning'
                    )

            }

        },

        async cargarClasificadoresDeInversion(){
            await axios.post('vue/presupuesto_ccp/ccp-ejecucionpresupuestal-inv.php?action=clasificadoresDeInversion')
            .then(
                (response) => {
                    this.nombreSectores = response.data.nombreSectores;
                    this.nombreProgramas = response.data.nombreProgramas;
                    this.nombreSubProgramas = response.data.nombreSubProgramas;
                    this.nombreSubProgramasOp = response.data.nombreSubProgramasOp;
                    this.nombreProductos = response.data.nombreProductos;
                    this.nombreProgramatico = response.data.nombreProgramatico;
                    this.productoTam = response.data.productoTam[0];
                    this.indicadorProductoTam = response.data.productoTam[1];
                }
            );
            this.nombreProgramasCopia = [...this.nombreProgramas];
            this.nombreSubProgramasOpCopia = [...this.nombreSubProgramasOp];
            this.nombreProductosCopia = [...this.nombreProductos];
            this.organizarOpciones();
        },

        organizarOpciones(){
            const sectoresOrg = this.nombreSectores.map((e) => {
                return {sector : e[0], nombre : e[1]}
            });
            this.optionsSectores = sectoresOrg;

            const programasOrg = this.nombreProgramas.map((e) => {
                return {programa : e[0], nombre : e[1]}
            });
            this.optionsProgramas = programasOrg;

            const subProgramasOrg = this.nombreSubProgramasOp.map((e) => {
                return {subprograma : e[0], nombre : e[1], codigo: e[2]}
            });
            this.optionsSubProgramas = subProgramasOrg;

            const productosOrg = this.nombreProductos.map((e) => {
                return {producto : e[0], nombre : e[1]}
            });
            this.optionsProductos = productosOrg;
        },

        async buscarCuentasClasificador(){
            if(this.valueSectores.length > 0){
                this.cuentasCcpetUsadasProy = await this.filtrarPorClasificadorInv({filtrarRubros: this.cuentasCcpetUsadasProy, dataFind: this.valueSectores, buscador: 'sector' });
                this.cuentasCcpetUsadasCdpInv = await this.filtrarPorClasificador({filtrarRubros: this.cuentasCcpetUsadasCdpInv, dataFind: this.valueSectores, buscador: 'sector' });
                this.cuentasCcpetUsadasRpInv = await this.filtrarPorClasificador({ filtrarRubros: this.cuentasCcpetUsadasRpInv, dataFind: this.valueSectores, buscador: 'sector' });
                this.cuentasCcpetUsadasCxpInv = await this.filtrarPorClasificador({ filtrarRubros: this.cuentasCcpetUsadasCxpInv, dataFind: this.valueSectores, buscador: 'sector' });
                this.cuentasCcpetUsadasEgresoInv = await this.filtrarPorClasificador({ filtrarRubros: this.cuentasCcpetUsadasEgresoInv, dataFind: this.valueSectores, buscador: 'sector' });
                this.cuentasCcpetUsadasIniInv = await this.filtrarPorClasificador({ filtrarRubros: this.cuentasCcpetUsadasIniInv, dataFind: this.valueSectores, buscador: 'sector' });
                this.cuentasCcpetUsadasAdInv = await this.filtrarPorClasificador({ filtrarRubros: this.cuentasCcpetUsadasAdInv, dataFind: this.valueSectores, buscador: 'sector' });
                this.cuentasCcpetUsadasRedInv = await this.filtrarPorClasificador({ filtrarRubros: this.cuentasCcpetUsadasRedInv, dataFind: this.valueSectores, buscador: 'sector' });
                this.cuentasCcpetUsadasTrasladoCredInv = await this.filtrarPorClasificador({ filtrarRubros: this.cuentasCcpetUsadasTrasladoCredInv, dataFind: this.valueSectores, buscador: 'sector' });
                this.cuentasCcpetUsadasTrasladoContraCredInv = await this.filtrarPorClasificador({ filtrarRubros: this.cuentasCcpetUsadasTrasladoContraCredInv, dataFind: this.valueSectores, buscador: 'sector' });
            }

            if(this.valueProgramas.length > 0){
                this.cuentasCcpetUsadasProy = await this.filtrarPorClasificadorInv({filtrarRubros: this.cuentasCcpetUsadasProy, dataFind: this.valueProgramas, buscador: 'programa' });
                this.cuentasCcpetUsadasCdpInv = await this.filtrarPorClasificador({filtrarRubros: this.cuentasCcpetUsadasCdpInv, dataFind: this.valueProgramas, buscador: 'programa' });
                this.cuentasCcpetUsadasRpInv = await this.filtrarPorClasificador({ filtrarRubros: this.cuentasCcpetUsadasRpInv, dataFind: this.valueProgramas, buscador: 'programa' });
                this.cuentasCcpetUsadasCxpInv = await this.filtrarPorClasificador({ filtrarRubros: this.cuentasCcpetUsadasCxpInv, dataFind: this.valueProgramas, buscador: 'programa' });
                this.cuentasCcpetUsadasEgresoInv = await this.filtrarPorClasificador({ filtrarRubros: this.cuentasCcpetUsadasEgresoInv, dataFind: this.valueProgramas, buscador: 'programa' });
                this.cuentasCcpetUsadasIniInv = await this.filtrarPorClasificador({ filtrarRubros: this.cuentasCcpetUsadasIniInv, dataFind: this.valueProgramas, buscador: 'programa' });
                this.cuentasCcpetUsadasAdInv = await this.filtrarPorClasificador({ filtrarRubros: this.cuentasCcpetUsadasAdInv, dataFind: this.valueProgramas, buscador: 'programa' });
                this.cuentasCcpetUsadasRedInv = await this.filtrarPorClasificador({ filtrarRubros: this.cuentasCcpetUsadasRedInv, dataFind: this.valueProgramas, buscador: 'programa' });
                this.cuentasCcpetUsadasTrasladoCredInv = await this.filtrarPorClasificador({ filtrarRubros: this.cuentasCcpetUsadasTrasladoCredInv, dataFind: this.valueProgramas, buscador: 'programa' });
                this.cuentasCcpetUsadasTrasladoContraCredInv = await this.filtrarPorClasificador({ filtrarRubros: this.cuentasCcpetUsadasTrasladoContraCredInv, dataFind: this.valueProgramas, buscador: 'programa' });
            }

            if(this.valueProductos.length > 0){
                this.cuentasCcpetUsadasProy = await this.filtrarPorClasificadorInv({filtrarRubros: this.cuentasCcpetUsadasProy, dataFind: this.valueProductos, buscador: 'producto' });
                this.cuentasCcpetUsadasCdpInv = await this.filtrarPorClasificador({filtrarRubros: this.cuentasCcpetUsadasCdpInv, dataFind: this.valueProductos, buscador: 'producto' });
                this.cuentasCcpetUsadasRpInv = await this.filtrarPorClasificador({ filtrarRubros: this.cuentasCcpetUsadasRpInv, dataFind: this.valueProductos, buscador: 'producto' });
                this.cuentasCcpetUsadasCxpInv = await this.filtrarPorClasificador({ filtrarRubros: this.cuentasCcpetUsadasCxpInv, dataFind: this.valueProductos, buscador: 'producto' });
                this.cuentasCcpetUsadasEgresoInv = await this.filtrarPorClasificador({ filtrarRubros: this.cuentasCcpetUsadasEgresoInv, dataFind: this.valueProductos, buscador: 'producto' });
                this.cuentasCcpetUsadasIniInv = await this.filtrarPorClasificador({ filtrarRubros: this.cuentasCcpetUsadasIniInv, dataFind: this.valueProductos, buscador: 'producto' });
                this.cuentasCcpetUsadasAdInv = await this.filtrarPorClasificador({ filtrarRubros: this.cuentasCcpetUsadasAdInv, dataFind: this.valueProductos, buscador: 'producto' });
                this.cuentasCcpetUsadasRedInv = await this.filtrarPorClasificador({ filtrarRubros: this.cuentasCcpetUsadasRedInv, dataFind: this.valueProductos, buscador: 'producto' });
                this.cuentasCcpetUsadasTrasladoCredInv = await this.filtrarPorClasificador({ filtrarRubros: this.cuentasCcpetUsadasTrasladoCredInv, dataFind: this.valueProductos, buscador: 'producto' });
                this.cuentasCcpetUsadasTrasladoContraCredInv = await this.filtrarPorClasificador({ filtrarRubros: this.cuentasCcpetUsadasTrasladoContraCredInv, dataFind: this.valueProductos, buscador: 'producto' });
            }

            console.log(this.cuentasCcpetUsadasCdpInv);
        },

        seccionPresupuestal: async function(){
            await axios.post('vue/presupuesto_ccp/ccp-inicialgastosfun.php?action=secciones')
                .then(
                    (response)=>{
                        this.seccionesPresupuestales = response.data.secciones;
                    }
                );
        },

        vigenciasDelgasto: async function(){
            await axios.post('vue/presupuesto_ccp/ccp-inicialgastosfun.php?action=vigenciasDelGasto')
                .then(
                    (response) => {
                        this.vigenciasdelgasto = response.data.vigenciasDelGasto;
                    }
                );
        },

        async fuentesOpciones(){
            let formDataDet = new FormData();
            let fechaIni = document.getElementById('fc_1198971545').value;

            let fechaPartes = fechaIni.split("/");
            let vigFuentes = !fechaIni ? this.vigencia : fechaPartes[2];

            formDataDet.append("vigencia", vigFuentes)
            /* console.log(fechaPartes[2]); */
            await axios.post('vue/presupuesto_ccp/ccp-ejecucionpresupuestal-inv.php?action=fuentes', formDataDet)
                .then(
                    (response) => {
                        this.fuentes = response.data.fuentes;
                        this.fuentesUsadas = response.data.fuentesUsadas;
                    }
                );
            const dataArrSet = new Set(this.fuentesUsadas);
            const dataArr = Array.from(dataArrSet);
            const fuentesOrg = await dataArr.map((e) => {
                return {fuente : e, nombre : this.buscarFuente(e)}
            });
            this.optionsFuentes = fuentesOrg;
        },

        fuenteConNombre({ fuente, nombre }){
            return `${fuente} - ${nombre}`
        },

        sectorConNombre({ sector, nombre }){
            return `${sector} - ${nombre}`
        },

        programaConNombre({ programa, nombre }){
            return `${programa} - ${nombre}`
        },

        subProgramaConNombre({ subprograma, nombre, codigo }){
            return `${subprograma} - ${nombre} - ${codigo}`
        },

        productoConNombre({ producto, nombre }){
            return `${producto} - ${nombre}`
        },

        changeSector(){

            const tamSector = this.valueSectores.length;
            let newData =  [];
            if(tamSector > 0){
                this.cambiaSector = true;
                this.nombreProgramas = [];
                this.valueProgramas = [];
                for(let i = 0; i < tamSector; i++ ){
                    let dataFilter = [];
                    dataFilter = this.nombreProgramasCopia.filter((e) => {
                        return e[0].substring(0, this.valueSectores[i].sector.length) == this.valueSectores[i].sector;
                    });
                    newData.push(dataFilter);
                }
                this.nombreProgramas = newData.flat();
            }else{
                this.valueSubProgramas = [];
                this.valueProgramas = [];
                this.valueProductos = [];
                this.nombreProgramas = this.nombreProgramasCopia;
                this.cambiaSector = false;
            }


            let newDataSub =  [];
            if(tamSector > 0){
                this.nombreSubProgramasOp = [];
                this.valueSubProgramas = [];
                for(let i = 0; i < tamSector; i++ ){
                    let dataFilter = [];
                    dataFilter = this.nombreSubProgramasOpCopia.filter((e) => {
                        return e[2].substring(0, this.valueSectores[i].sector.length) == this.valueSectores[i].sector;
                    });
                    newDataSub.push(dataFilter);
                }
                this.nombreSubProgramasOp = newDataSub.flat();
            }else{
                this.nombreSubProgramasOp = this.nombreSubProgramasOpCopia;
            }


            let newDataPro =  [];
            if(tamSector > 0){
                this.nombreProductos = [];
                this.valueProductos = [];
                for(let i = 0; i < tamSector; i++ ){
                    let dataFilter = [];
                    dataFilter = this.nombreProductosCopia.filter((e) => {
                        return e[0].substring(0, this.valueSectores[i].sector.length) == this.valueSectores[i].sector;
                    });
                    newDataPro.push(dataFilter);
                }
                this.nombreProductos = newDataPro.flat();
            }else{
                this.nombreProductos = this.nombreProductosCopia;
            }

            this.organizarOpciones();
            this.cambiaCriteriosBusqueda();

        },

        changePrograma(){

            const tamProgramas = this.valueProgramas.length;

            let newDataSub =  [];
            let newDataSec =  [];
            if(tamProgramas > 0){
                this.nombreSubProgramasOp = [];
                this.valueSubProgramas = [];
                this.cambiaPrograma = true;
                for(let i = 0; i < tamProgramas; i++ ){
                    let dataFilter = [];
                    let dataFilterSec = [];
                    dataFilter = this.nombreSubProgramasOpCopia.filter((e) => {
                        return e[2].substring(0, this.valueProgramas[i].programa.length) == this.valueProgramas[i].programa;
                    });
                    newDataSub.push(dataFilter);

                    dataFilterSec = this.optionsSectores.find(e => e.sector == this.valueProgramas[i].programa.substring(0, 2));
                    newDataSec.push(dataFilterSec);
                }
                this.nombreSubProgramasOp = newDataSub.flat();
            }else{
                this.valueSubProgramas = [];
                this.valueProductos = [];
                this.nombreSubProgramasOp = this.nombreSubProgramasOpCopia;
                this.cambiaPrograma = false;
            }

            if(!this.cambiaSector){
                const sect = new Set(newDataSec);
                this.valueSectores = Array.from(sect);
            }



            let newDataPro =  [];
            if(tamProgramas > 0){
                this.nombreProductos = [];
                this.valueProductos = [];
                for(let i = 0; i < tamProgramas; i++ ){
                    let dataFilter = [];
                    dataFilter = this.nombreProductosCopia.filter((e) => {
                        return e[0].substring(0, this.valueProgramas[i].programa.length) == this.valueProgramas[i].programa;
                    });
                    newDataPro.push(dataFilter);
                }
                this.nombreProductos = newDataPro.flat();
            }else{
                this.nombreProductos = this.nombreProductosCopia;
            }

            this.organizarOpciones();
            this.cambiaCriteriosBusqueda();
        },

        changeSubPrograma(){
            const tamSubProgramas = this.valueSubProgramas.length;

            let newDataPro =  [];
            let newDataPrograma =  [];
            let newDataSec =  [];

            if(tamSubProgramas > 0){

                this.nombreProductos = [];
                this.valueProductos = [];
                this.cambiaSubPrograma = true;
                for(let i = 0; i < tamSubProgramas; i++ ){
                    let dataFilter = [];
                    let dataFilterSec = [];
                    let dataFilterPrograma = [];
                    dataFilter = this.nombreProductosCopia.filter((e) => {
                        return e[0].substring(0, this.valueSubProgramas[i].codigo.length) == this.valueSubProgramas[i].codigo;
                    });
                    newDataPro.push(dataFilter);

                    dataFilterPrograma = this.optionsProgramas.find(e => e.programa == this.valueSubProgramas[i].codigo);
                    newDataPrograma.push(dataFilterPrograma);

                    dataFilterSec = this.optionsSectores.find(e => e.sector == this.valueSubProgramas[i].codigo.substring(0, 2));
                    newDataSec.push(dataFilterSec);
                }
                this.nombreProductos = newDataPro.flat();
            }else{
                this.valueProductos = [];
                this.nombreProductos = this.nombreProductosCopia;
                this.cambiaSubPrograma = false;
            }

            if(!this.cambiaSector && !this.cambiaPrograma){
                const sect = new Set(newDataSec);
                this.valueSectores = Array.from(sect);

                const prog = new Set(newDataPrograma);
                this.valueProgramas = Array.from(prog);
            }

            this.organizarOpciones();
            this.cambiaCriteriosBusqueda();

        },

        changeProducto(){
            const tamProductos = this.valueProductos.length;

            let newDataPrograma =  [];
            let newDataSubPrograma =  [];
            let newDataSec =  [];

            if(tamProductos > 0){
                for(let i = 0; i < tamProductos; i++ ){
                    let dataFilterSec = [];
                    let dataFilterPrograma = [];
                    let dataFilterSubPrograma = [];

                    dataFilterSubPrograma = this.optionsSubProgramas.find(e => e.codigo == this.valueProductos[i].producto.substring(0, e.codigo.length));
                    newDataSubPrograma.push(dataFilterSubPrograma);

                    dataFilterPrograma = this.optionsProgramas.find(e => e.programa == this.valueProductos[i].producto.substring(0, e.programa.length));
                    newDataPrograma.push(dataFilterPrograma);

                    dataFilterSec = this.optionsSectores.find(e => e.sector == this.valueProductos[i].producto.substring(0, e.sector.length));
                    newDataSec.push(dataFilterSec);
                }
            }

            if(!this.cambiaSector && !this.cambiaPrograma && !this.cambiaSubPrograma){
                const subProg = new Set(newDataSubPrograma);
                this.valueSubProgramas = Array.from(subProg);

                const prog = new Set(newDataPrograma);
                this.valueProgramas = Array.from(prog);

                const sect = new Set(newDataSec);
                this.valueSectores = Array.from(sect);

            }

            this.organizarOpciones();
            this.cambiaCriteriosBusqueda();
        },

        clasificadoresPresupuestales: async function(){
            await axios.post('presupuesto_ccpet/clasificadores/ccp-buscaclasificadoresgastos.php')
                .then(
                    (response) => {
                        this.clasificadores = response.data.clasificadores;
                    }
                );
        },

        async cuentasDelClasificador(){

            this.results_det = [];
            let formDataDet = new FormData();
            formDataDet.append("id_clasificador", this.selectClasificador)
            await axios.post('presupuesto_ccpet/clasificadores/ccp-buscaclasificadoresgastos.php?action=search_clasificador_det_aux', formDataDet)
                .then(
                    (response) => {
                        this.results_det = response.data.clasificadores_det;
                        /* this.selectVigenciaGasto = this.vigenciasdelgasto[0][0]; */
                    }
                );
            this.cambiaCriteriosBusqueda();
        },

        async filtrarPorClasificador({filtrarRubros: filtrarRubros, dataFind: dataFind, buscador: buscador}){

            const filtro = await filtrarRubros.map((element) => {
                let a = (dataFind.find((e) => e[buscador] == element[4].substring(0, e[buscador].length)));
                return a != undefined ? element : ''
            });
            const resp = filtro.filter((element) => element != '');
            return resp;
        },

        async filtrarPorClasificadorInv({filtrarRubros: filtrarRubros, dataFind: dataFind, buscador: buscador}){

            const filtro = await filtrarRubros.map((element) => {

                let a = (dataFind.find((e) => e[buscador] == element[6].substring(0, e[buscador].length)));
                return a != undefined ? element : ''
            });
            const resp = filtro.filter((element) => element != '');
            return resp;
        },

        cargayears: async function(){
			await axios.post('vue/presupuesto_ccp/ccp-bancoproyectos.php?buscar=years')
			.then(
				(response)=>
				{
					this.years=response.data.anio;
					var idanio=response.data.anio.length -1;
					if (idanio>=0){this.vigencia = response.data.anio[idanio][0];}
					else{this.vigencia ='';}
				}
            );

		},

        buscarFuente(fuente){
            const nombFuente = this.fuentes.find(e => e[0] == fuente)
            const nombF = typeof nombFuente == 'object' ? Object.values(nombFuente) : ''

            return typeof nombFuente == 'string' ? '' : nombF[1];
        },

        cuentasMayoresAux: function(){

            this.cuentasCcpetUsadasProy.forEach(element => {

                var resultArbol = [];
                resultArbol = this.arbolInversion.find(cuentaArbol => cuentaArbol[0] == element[2]);
                if(resultArbol == undefined){

                    var ramas = [];
                    ramas.push(element[2]);

                    this.nombreSectores.forEach(function logArrayElements(elementSec, index, array) {
                        if(elementSec[0] == element[2]){
                            ramas.push('Sector: ' + elementSec[1].toLowerCase());
                        }
                    });

                    if(this.selectNivel == 1){
                        ramas.push('C');
                    }else{
                        ramas.push('A');
                    }

                    ramas.push('');
                    ramas.push('');
                    ramas.push('');
                    ramas.push('');

                    //Inicial
                    var totalIniInv = 0;
                    this.cuentasCcpetUsadasIniInv.forEach(elementIniInv => {
                        if(elementIniInv[4].substring(0, 2) == element[2]){
                            totalIniInv = parseFloat(totalIniInv) + parseFloat(elementIniInv[5]) + parseFloat(elementIniInv[6]);
                        }
                    });
                    ramas.push(totalIniInv);

                    //Adicion
                    var totalAdInv = 0;
                    this.cuentasCcpetUsadasAdInv.forEach(elementAdInv => {
                        if(elementAdInv[4].substring(0, 2) == element[2]){
                            totalAdInv = parseFloat(totalAdInv) + parseFloat(elementAdInv[5]);
                        }
                    });
                    ramas.push(totalAdInv);

                    //Reduccion

                    var totalRedInv = 0;

                    this.cuentasCcpetUsadasRedInv.forEach(elementRedInv => {
                        if(elementRedInv[4].substring(0, 2) == element[2]){
                            totalRedInv = parseFloat(totalRedInv) + parseFloat(elementRedInv[5]);
                        }
                    });

                    ramas.push(totalRedInv);

                    //Credito

                    //Traslado Cred
                    var totalCredInv = 0;
                    this.cuentasCcpetUsadasTrasladoCredInv.forEach(elementCredInv => {
                        if(elementCredInv[4].substring(0, 2) == element[2]){
                            totalCredInv = parseFloat(totalCredInv) + parseFloat(elementCredInv[5]);
                        }
                    });
                    ramas.push(totalCredInv);

                    //Contracredito

                    var totalContraInv = 0;

                    this.cuentasCcpetUsadasTrasladoContraCredInv.forEach(elementContraCredInv => {
                        if(elementContraCredInv[4].substring(0, 2) == element[2]){
                            totalContraInv = parseFloat(totalContraInv) + parseFloat(elementContraCredInv[5]);
                        }
                    });

                    ramas.push(totalContraInv);

                    var totalInv = totalIniInv + totalAdInv - totalRedInv + totalCredInv - totalContraInv;

                    ramas.push(totalInv);

                    //Cdp Inversion
                    var totalCdpInv = 0;
                    this.cuentasCcpetUsadasCdpInv.forEach(elementCdpInv => {
                        if(elementCdpInv[4].substring(0, 2) == element[2]){
                            totalCdpInv = parseFloat(totalCdpInv) + parseFloat(elementCdpInv[5]);
                        }
                    });
                    ramas.push(totalCdpInv);

                    //Rp Inversion
                    var totalRpInv = 0;
                    this.cuentasCcpetUsadasRpInv.forEach(elementRpInv => {
                        if(elementRpInv[4].substring(0, 2) == element[2]){
                            totalRpInv = parseFloat(totalRpInv) + parseFloat(elementRpInv[5]);
                        }
                    });
                    ramas.push(totalRpInv);

                    //Cxp Inversion
                    var totalCxpInv = 0;
                    this.cuentasCcpetUsadasCxpInv.forEach(elementCxpInv => {
                        if(elementCxpInv[4].substring(0, 2) == element[2]){
                            totalCxpInv = parseFloat(totalCxpInv) + parseFloat(elementCxpInv[5]);
                        }
                    });
                    ramas.push(totalCxpInv);

                    //Egreso Inversion
                    var totalEgresoInv = 0;
                    this.cuentasCcpetUsadasEgresoInv.forEach(elementEgresoInv => {
                        if(elementEgresoInv[4].substring(0, 2) == element[2]){
                            totalEgresoInv = parseFloat(totalEgresoInv) + parseFloat(elementEgresoInv[5]);
                        }
                    });
                    ramas.push(totalEgresoInv);

                    var saldoInv = totalInv - totalCdpInv;

                    ramas.push(saldoInv);

                    this.arbolInversion.push(ramas);
                }

                if(this.selectNivel == 1) return


                var resultArbol = [];
                resultArbol = this.arbolInversion.find(cuentaArbol => cuentaArbol[0] == element[3]);
                if(resultArbol == undefined){

                    var ramas = [];
                    ramas.push(element[3]);
                    this.nombreProgramas.forEach(function logArrayElements(elementProg, index, array) {
                        if(elementProg[0] == element[3]){
                            ramas.push('Programa: ' + elementProg[1].toLowerCase());
                        }
                    });
                    if(this.selectNivel == 2){
                        ramas.push('C');
                    }else{
                        ramas.push('A');
                    }
                    ramas.push('');
                    ramas.push('');
                    ramas.push('');
                    ramas.push('');
                    /* ramas.push(resultadoBusqueda[2]);
                    ramas.push(resultadoBusqueda[6]); */

                    //Inicial
                    var totalIniInv = 0;
                    this.cuentasCcpetUsadasIniInv.forEach(elementIniInv => {
                        if(elementIniInv[4].substring(0, 4) == element[3]){
                            totalIniInv = parseFloat(totalIniInv) + parseFloat(elementIniInv[5]) + parseFloat(elementIniInv[6]);
                        }
                    });
                    ramas.push(totalIniInv);

                    //Adicion
                    var totalAdInv = 0;
                    this.cuentasCcpetUsadasAdInv.forEach(elementAdInv => {
                        if(elementAdInv[4].substring(0, 4) == element[3]){
                            totalAdInv = parseFloat(totalAdInv) + parseFloat(elementAdInv[5]);
                        }
                    });
                    ramas.push(totalAdInv);

                    //Reduccion

                    var totalRedInv = 0;

                    this.cuentasCcpetUsadasRedInv.forEach(elementRedInv => {
                        if(elementRedInv[4].substring(0, 4) == element[3]){
                            totalRedInv = parseFloat(totalRedInv) + parseFloat(elementRedInv[5]);
                        }
                    });

                    ramas.push(totalRedInv);

                    //Credito

                    var totalCredInv = 0;

                    this.cuentasCcpetUsadasTrasladoCredInv.forEach(elementCredInv => {
                        if(elementCredInv[4].substring(0, 4) == element[3]){
                            totalCredInv = parseFloat(totalCredInv) + parseFloat(elementCredInv[5]);
                        }
                    });

                    ramas.push(totalCredInv);

                    //Contracredito

                    var totalContraInv = 0;

                    this.cuentasCcpetUsadasTrasladoContraCredInv.forEach(elementContraCredInv => {
                        if(elementContraCredInv[4].substring(0, 4) == element[3]){
                            totalContraInv = parseFloat(totalContraInv) + parseFloat(elementContraCredInv[5]);
                        }
                    });

                    ramas.push(totalContraInv);

                    var totalInv = totalIniInv + totalAdInv - totalRedInv + totalCredInv - totalContraInv;

                    ramas.push(totalInv);


                    //Cdp Inversion
                    var totalCdpInv = 0;
                    this.cuentasCcpetUsadasCdpInv.forEach(elementCdpInv => {
                        if(elementCdpInv[4].substring(0, 4) == element[3]){
                            totalCdpInv = parseFloat(totalCdpInv) + parseFloat(elementCdpInv[5]);
                        }
                    });
                    ramas.push(totalCdpInv);

                    //Rp Inversion
                    var totalRpInv = 0;
                    this.cuentasCcpetUsadasRpInv.forEach(elementRpInv => {
                        if(elementRpInv[4].substring(0, 4) == element[3]){
                            totalRpInv = parseFloat(totalRpInv) + parseFloat(elementRpInv[5]);
                        }
                    });
                    ramas.push(totalRpInv);

                    //Cxp Inversion
                    var totalCxpInv = 0;
                    this.cuentasCcpetUsadasCxpInv.forEach(elementCxpInv => {
                        if(elementCxpInv[4].substring(0, 4) == element[3]){
                            totalCxpInv = parseFloat(totalCxpInv) + parseFloat(elementCxpInv[5]);
                        }
                    });
                    ramas.push(totalCxpInv);

                    //Egreso Inversion
                    var totalEgresoInv = 0;
                    this.cuentasCcpetUsadasEgresoInv.forEach(elementEgresoInv => {
                        if(elementEgresoInv[4].substring(0, 4) == element[3]){
                            totalEgresoInv = parseFloat(totalEgresoInv) + parseFloat(elementEgresoInv[5]);
                        }
                    });
                    ramas.push(totalEgresoInv);

                    var saldoInv = totalInv - totalCdpInv;

                    ramas.push(saldoInv);

                    this.arbolInversion.push(ramas);
                }

                if(this.selectNivel == 2) return

                var resultArbol = [];
                subprograma = '';
                subprograma = element[3]+''+element[4];
                resultArbol = this.arbolInversion.find(cuentaArbol => cuentaArbol[0] == subprograma);
                if(resultArbol == undefined){
                    var ramas = [];
                    ramas.push(subprograma);
                    this.nombreSubProgramas.forEach(function logArrayElements(elementSubProg, index, array) {
                        if(elementSubProg[0] == element[4]){
                            ramas.push('Subprograma: ' + elementSubProg[1].toLowerCase());
                        }
                    });
                    if(this.selectNivel == 3){
                        ramas.push('C');
                    }else{
                        ramas.push('A');
                    }
                    ramas.push('');
                    ramas.push('');
                    ramas.push('');
                    ramas.push('');

                    var totalIniInv = 0;
                    this.cuentasCcpetUsadasIniInv.forEach(elementIniInv => {
                        if(elementIniInv[4].substring(0, 4) == element[3]){
                            totalIniInv = parseFloat(totalIniInv) + parseFloat(elementIniInv[5]) + parseFloat(elementIniInv[6]);
                        }
                    });
                    ramas.push(totalIniInv);

                    //Adicion
                    var totalAdInv = 0;
                    this.cuentasCcpetUsadasAdInv.forEach(elementAdInv => {
                        if(elementAdInv[4].substring(0, 4) == element[3]){
                            totalAdInv = parseFloat(totalAdInv) + parseFloat(elementAdInv[5]);
                        }
                    });
                    ramas.push(totalAdInv);

                    //Reduccion

                    var totalRedInv = 0;

                    this.cuentasCcpetUsadasRedInv.forEach(elementRedInv => {
                        if(elementRedInv[4].substring(0, 4) == element[3]){
                            totalRedInv = parseFloat(totalRedInv) + parseFloat(elementRedInv[5]);
                        }
                    });

                    ramas.push(totalRedInv);

                    //Credito

                    var totalCredInv = 0;

                    this.cuentasCcpetUsadasTrasladoCredInv.forEach(elementCredInv => {
                        if(elementCredInv[4].substring(0, 4) == element[3]){
                            totalCredInv = parseFloat(totalCredInv) + parseFloat(elementCredInv[5]);
                        }
                    });

                    ramas.push(totalCredInv);

                    //Contracredito

                    var totalContraInv = 0;

                    this.cuentasCcpetUsadasTrasladoContraCredInv.forEach(elementContraCredInv => {
                        if(elementContraCredInv[4].substring(0, 4) == element[3]){
                            totalContraInv = parseFloat(totalContraInv) + parseFloat(elementContraCredInv[5]);
                        }
                    });

                    ramas.push(totalContraInv);


                    /* ramas.push(totalContraInv); */

                    var totalInv = totalIniInv + totalAdInv - totalRedInv + totalCredInv - totalContraInv;

                    ramas.push(totalInv);

                    //Cdp Inversion
                    var totalCdpInv = 0;
                    this.cuentasCcpetUsadasCdpInv.forEach(elementCdpInv => {
                        if(elementCdpInv[4].substring(0, 4) == element[3]){
                            totalCdpInv = parseFloat(totalCdpInv) + parseFloat(elementCdpInv[5]);
                        }
                    });
                    ramas.push(totalCdpInv);

                    //Rp Inversion
                    var totalRpInv = 0;
                    this.cuentasCcpetUsadasRpInv.forEach(elementRpInv => {
                        if(elementRpInv[4].substring(0, 4) == element[3]){
                            totalRpInv = parseFloat(totalRpInv) + parseFloat(elementRpInv[5]);
                        }
                    });
                    ramas.push(totalRpInv);

                    //Cxp Inversion
                    var totalCxpInv = 0;
                    this.cuentasCcpetUsadasCxpInv.forEach(elementCxpInv => {
                        if(elementCxpInv[4].substring(0, 4) == element[3]){
                            totalCxpInv = parseFloat(totalCxpInv) + parseFloat(elementCxpInv[5]);
                        }
                    });
                    ramas.push(totalCxpInv);

                    //Egreso Inversion
                    var totalEgresoInv = 0;
                    this.cuentasCcpetUsadasEgresoInv.forEach(elementEgresoInv => {
                        if(elementEgresoInv[4].substring(0, 4) == element[3]){
                            totalEgresoInv = parseFloat(totalEgresoInv) + parseFloat(elementEgresoInv[5]);
                        }
                    });
                    ramas.push(totalEgresoInv);

                    var saldoInv = totalInv - totalCdpInv;

                    ramas.push(saldoInv);

                    this.arbolInversion.push(ramas);
                }

                if(this.selectNivel == 3) return

                var resultArbol = [];
                resultArbol = this.arbolInversion.find(cuentaArbol => cuentaArbol[0] == element[5]);
                if(resultArbol == undefined){

                    var ramas = [];
                    ramas.push(element[5]);
                    this.nombreProductos.forEach(function logArrayElements(elementProducto, index, array) {
                        if(elementProducto[0] == element[5]){
                            ramas.push('Producto: ' + elementProducto[1].toLowerCase());
                        }
                    });
                    if(this.selectNivel == 4){
                        ramas.push('C');
                    }else{
                        ramas.push('A');
                    }
                    ramas.push('');
                    ramas.push('');
                    ramas.push('');
                    ramas.push('');

                    //Inicial
                    var totalIniInv = 0;
                    this.cuentasCcpetUsadasIniInv.forEach(elementIniInv => {
                        if(elementIniInv[4].substring(0, this.productoTam) == element[5]){
                            totalIniInv = parseFloat(totalIniInv) + parseFloat(elementIniInv[5]) + parseFloat(elementIniInv[6]);
                        }
                    });

                    ramas.push(totalIniInv);

                    //Adicion
                    var totalAdInv = 0;
                    this.cuentasCcpetUsadasAdInv.forEach(elementAdInv => {
                        if(elementAdInv[4].substring(0, this.productoTam) == element[5]){
                            totalAdInv = parseFloat(totalAdInv) + parseFloat(elementAdInv[5]);
                        }
                    });
                    ramas.push(totalAdInv);

                    //Reduccion

                    var totalRedInv = 0;

                    this.cuentasCcpetUsadasRedInv.forEach(elementRedInv => {
                        if(elementRedInv[4].substring(0, this.productoTam) == element[5]){
                            totalRedInv = parseFloat(totalRedInv) + parseFloat(elementRedInv[5]);
                        }
                    });

                    ramas.push(totalRedInv);

                    //Credito

                    var totalCredInv = 0;
                    //
                    this.cuentasCcpetUsadasTrasladoCredInv.forEach(elementCredInv => {
                        if(elementCredInv[4].substring(0, this.productoTam) == element[5]){

                            totalCredInv = parseFloat(totalCredInv) + parseFloat(elementCredInv[5]);
                        }
                    });

                    ramas.push(totalCredInv);

                    //Contracredito

                    var totalContraInv = 0;

                    this.cuentasCcpetUsadasTrasladoContraCredInv.forEach(elementContraCredInv => {
                        if(elementContraCredInv[4].substring(0, this.productoTam) == element[5]){

                            totalContraInv = parseFloat(totalContraInv) + parseFloat(elementContraCredInv[5]);
                        }
                    });

                    ramas.push(totalContraInv);

                    var totalInv = totalIniInv + totalAdInv - totalRedInv + totalCredInv - totalContraInv;

                    ramas.push(totalInv);


                    //Cdp Inversion
                    var totalCdpInv = 0;
                    this.cuentasCcpetUsadasCdpInv.forEach(elementCdpInv => {
                        if(elementCdpInv[4].substring(0, this.productoTam) == element[5]){
                            totalCdpInv = parseFloat(totalCdpInv) + parseFloat(elementCdpInv[5]);
                        }
                    });
                    ramas.push(totalCdpInv);

                    //Rp Inversion
                    var totalRpInv = 0;
                    this.cuentasCcpetUsadasRpInv.forEach(elementRpInv => {
                        if(elementRpInv[4].substring(0, this.productoTam) == element[5]){
                            totalRpInv = parseFloat(totalRpInv) + parseFloat(elementRpInv[5]);
                        }
                    });
                    ramas.push(totalRpInv);

                    //Cxp Inversion
                    var totalCxpInv = 0;
                    this.cuentasCcpetUsadasCxpInv.forEach(elementCxpInv => {
                        if(elementCxpInv[4].substring(0, this.productoTam) == element[5]){
                            totalCxpInv = parseFloat(totalCxpInv) + parseFloat(elementCxpInv[5]);
                        }
                    });
                    ramas.push(totalCxpInv);

                    //Egreso Inversion
                    var totalEgresoInv = 0;
                    this.cuentasCcpetUsadasEgresoInv.forEach(elementEgresoInv => {
                        if(elementEgresoInv[4].substring(0, this.productoTam) == element[5]){
                            totalEgresoInv = parseFloat(totalEgresoInv) + parseFloat(elementEgresoInv[5]);
                        }
                    });
                    ramas.push(totalEgresoInv);

                    var saldoInv = totalInv - totalCdpInv;

                    ramas.push(saldoInv);

                    this.arbolInversion.push(ramas);
                }

                if(this.selectNivel == 4) return

                var resultArbol = [];
                resultArbol = this.arbolInversion.find(cuentaArbol => cuentaArbol[0] == element[6]);
                if(resultArbol == undefined){

                    var ramas = [];
                    ramas.push(element[6]);
                    this.nombreProgramatico.forEach(function logArrayElements(elementProgramatico, index, array) {
                        if(elementProgramatico[0] == element[6]){
                            ramas.push('Programatico: ' + elementProgramatico[1].toLowerCase());
                        }
                    });
                    ramas.push('A');
                    ramas.push('');
                    ramas.push('');
                    ramas.push('');
                    ramas.push('');

                    //Inicial
                    var totalIniInv = 0;
                    this.cuentasCcpetUsadasIniInv.forEach(elementIniInv => {
                        if(elementIniInv[4] == element[6]){
                            totalIniInv = parseFloat(totalIniInv) + parseFloat(elementIniInv[5]) + parseFloat(elementIniInv[6]);
                        }
                    });
                    ramas.push(totalIniInv);

                    //Adicion
                    var totalAdInv = 0;
                    this.cuentasCcpetUsadasAdInv.forEach(elementAdInv => {
                        if(elementAdInv[4] == element[6]){
                            totalAdInv = parseFloat(totalAdInv) + parseFloat(elementAdInv[5]);
                        }
                    });
                    ramas.push(totalAdInv);

                    //Reduccion

                    var totalRedInv = 0;

                    this.cuentasCcpetUsadasRedInv.forEach(elementRedInv => {
                        if(elementRedInv[4] == element[6]){
                            totalRedInv = parseFloat(totalRedInv) + parseFloat(elementRedInv[5]);
                        }
                    });

                    ramas.push(totalRedInv);

                    //Credito

                    var totalCredInv = 0;

                    this.cuentasCcpetUsadasTrasladoCredInv.forEach(elementCredInv => {
                        if(elementCredInv[4] == element[6]){
                            totalCredInv = parseFloat(totalCredInv) + parseFloat(elementCredInv[5]);
                        }
                    });

                    ramas.push(totalCredInv);

                    //Contracredito

                    var totalContraInv = 0;

                    this.cuentasCcpetUsadasTrasladoContraCredInv.forEach(elementContraCredInv => {
                        if(elementContraCredInv[4] == element[6]){
                            totalContraInv = parseFloat(totalContraInv) + parseFloat(elementContraCredInv[5]);
                        }
                    });

                    ramas.push(totalContraInv);

                    var totalInv = totalIniInv + totalAdInv - totalRedInv + totalCredInv - totalContraInv;

                    ramas.push(totalInv);


                    //Cdp Inversion
                    var totalCdpInv = 0;
                    this.cuentasCcpetUsadasCdpInv.forEach(elementCdpInv => {
                        if(elementCdpInv[4] == element[6]){
                            totalCdpInv = parseFloat(totalCdpInv) + parseFloat(elementCdpInv[5]);
                        }
                    });
                    ramas.push(totalCdpInv);

                    //Rp Inversion
                    var totalRpInv = 0;
                    this.cuentasCcpetUsadasRpInv.forEach(elementRpInv => {
                        if(elementRpInv[4] == element[6]){
                            totalRpInv = parseFloat(totalRpInv) + parseFloat(elementRpInv[5]);
                        }
                    });
                    ramas.push(totalRpInv);

                    //Cxp Inversion
                    var totalCxpInv = 0;
                    this.cuentasCcpetUsadasCxpInv.forEach(elementCxpInv => {
                        if(elementCxpInv[4] == element[6]){
                            totalCxpInv = parseFloat(totalCxpInv) + parseFloat(elementCxpInv[5]);
                        }
                    });
                    ramas.push(totalCxpInv);

                    //Egreso Inversion
                    var totalEgresoInv = 0;
                    this.cuentasCcpetUsadasEgresoInv.forEach(elementEgresoInv => {
                        if(elementEgresoInv[4] == element[6]){
                            totalEgresoInv = parseFloat(totalEgresoInv) + parseFloat(elementEgresoInv[5]);
                        }
                    });
                    ramas.push(totalEgresoInv);

                    var saldoInv = totalInv - totalCdpInv;

                    ramas.push(saldoInv);

                    this.arbolInversion.push(ramas);
                }

                var resultArbol = [];
                programaticoBpim = '';
                programaticoBpim = element[6]+'-'+element[9];
                programatico_Bpim_Fuente_Sec_Vig_Med = element[6]+'-'+element[9]+'-'+element[11]+'-'+element[12]+'-'+element[13]+'-'+element[14];
                resultArbol = this.arbolInversion.find(cuentaArbol => cuentaArbol[0] == programatico_Bpim_Fuente_Sec_Vig_Med);
                if(resultArbol == undefined){

                    var ramas = [];
                    ramas.push(programatico_Bpim_Fuente_Sec_Vig_Med);
                    ramas.push('Proyecto: ' + element[10]);
                    ramas.push('C');
                    ramas.push(element[11]);
                    ramas.push(element[12]);
                    ramas.push(element[13]);
                    ramas.push(element[14]);

                    /* ramas.push(resultadoBusqueda[2]);
                    ramas.push(resultadoBusqueda[6]); */
                    var totalInv = 0;

                    //Inicial
                    var totalIniInv = 0;
                    this.cuentasCcpetUsadasIniInv.forEach(elementIniInv => {
                        var detProy = elementIniInv[4]+'-'+elementIniInv[1]+'-'+elementIniInv[2]+'-'+elementIniInv[0]+'-'+elementIniInv[7]+'-'+elementIniInv[3];
                        if(detProy == programatico_Bpim_Fuente_Sec_Vig_Med){
                            totalIniInv = parseFloat(totalIniInv) + parseFloat(elementIniInv[5]) + parseFloat(elementIniInv[6]);
                        }
                    });
                    ramas.push(totalIniInv);

                    //Adicion

                    var totalAdInv = 0;
                    this.cuentasCcpetUsadasAdInv.forEach(elementAdInv => {
                        var detProy = elementAdInv[4]+'-'+elementAdInv[1]+'-'+elementAdInv[2]+'-'+elementAdInv[0]+'-'+elementAdInv[6]+'-'+elementAdInv[3];
                        if(detProy == programatico_Bpim_Fuente_Sec_Vig_Med){
                            totalAdInv = parseFloat(totalAdInv) + parseFloat(elementAdInv[5]);
                        }
                    });

                    ramas.push(totalAdInv);

                    //Reduccion

                    var totalRedInv = 0;

                    this.cuentasCcpetUsadasRedInv.forEach(elementRedInv => {
                        var detProy = elementRedInv[4]+'-'+elementRedInv[1]+'-'+elementRedInv[2]+'-'+elementRedInv[0]+'-'+elementRedInv[6]+'-'+elementRedInv[3];
                        if(detProy == programatico_Bpim_Fuente_Sec_Vig_Med){
                            totalRedInv = parseFloat(totalRedInv) + parseFloat(elementRedInv[5]);
                        }
                    });

                    ramas.push(totalRedInv);

                    //Credito

                    var totalCredInv = 0;

                    this.cuentasCcpetUsadasTrasladoCredInv.forEach(elementCredInv => {
                        var detProy = elementCredInv[4]+'-'+elementCredInv[1]+'-'+elementCredInv[2]+'-'+elementCredInv[0]+'-'+elementCredInv[6]+'-'+elementCredInv[3];
                        if(detProy == programatico_Bpim_Fuente_Sec_Vig_Med){
                            totalCredInv = parseFloat(totalCredInv) + parseFloat(elementCredInv[5]);
                        }
                    });

                    ramas.push(totalCredInv);

                    //Contracredito

                    var totalContraInv = 0;

                    this.cuentasCcpetUsadasTrasladoContraCredInv.forEach(elementContraCredInv => {
                        var detProy = elementContraCredInv[4]+'-'+elementContraCredInv[1]+'-'+elementContraCredInv[2]+'-'+elementContraCredInv[0]+'-'+elementContraCredInv[6]+'-'+elementContraCredInv[3];
                        if(detProy == programatico_Bpim_Fuente_Sec_Vig_Med){
                            totalContraInv = parseFloat(totalContraInv) + parseFloat(elementContraCredInv[5]);
                        }
                    });

                    ramas.push(totalContraInv);


                    totalInv = totalIniInv + totalAdInv - totalRedInv + totalCredInv - totalContraInv;

                    ramas.push(totalInv);


                    //Cdp Inversion
                    var totalCdpInv = 0;
                    this.cuentasCcpetUsadasCdpInv.forEach(elementCdpInv => {
                        var detProy = elementCdpInv[4]+'-'+elementCdpInv[1]+'-'+elementCdpInv[2]+'-'+elementCdpInv[0]+'-'+elementCdpInv[6]+'-'+elementCdpInv[3];
                        if(detProy == programatico_Bpim_Fuente_Sec_Vig_Med){
                            totalCdpInv = parseFloat(totalCdpInv) + parseFloat(elementCdpInv[5]);
                        }
                    });

                    ramas.push(totalCdpInv);

                    //Rp Inversion
                    var totalRpInv = 0;
                    this.cuentasCcpetUsadasRpInv.forEach(elementRpInv => {
                        var detProy = elementRpInv[4]+'-'+elementRpInv[1]+'-'+elementRpInv[2]+'-'+elementRpInv[0]+'-'+elementRpInv[6]+'-'+elementRpInv[3];
                        if(detProy == programatico_Bpim_Fuente_Sec_Vig_Med){
                            totalRpInv = parseFloat(totalRpInv) + parseFloat(elementRpInv[5]);
                        }
                    });

                    ramas.push(totalRpInv);

                    //Cxp Inversion
                    var totalCxpInv = 0;
                    this.cuentasCcpetUsadasCxpInv.forEach(elementCxpInv => {
                        var detProy = elementCxpInv[4]+'-'+elementCxpInv[1]+'-'+elementCxpInv[2]+'-'+elementCxpInv[0]+'-'+elementCxpInv[6]+'-'+elementCxpInv[3];
                        if(detProy == programatico_Bpim_Fuente_Sec_Vig_Med){
                            totalCxpInv = parseFloat(totalCxpInv) + parseFloat(elementCxpInv[5]);
                        }
                    });

                    ramas.push(totalCxpInv);

                    //Egreso Inversion
                    var totalEgresoInv = 0;
                    this.cuentasCcpetUsadasEgresoInv.forEach(elementEgresoInv => {
                        var detProy = elementEgresoInv[4]+'-'+elementEgresoInv[1]+'-'+elementEgresoInv[2]+'-'+elementEgresoInv[0]+'-'+elementEgresoInv[6]+'-'+elementEgresoInv[3];
                        if(detProy == programatico_Bpim_Fuente_Sec_Vig_Med){
                            totalEgresoInv = parseFloat(totalEgresoInv) + parseFloat(elementEgresoInv[5]);
                        }
                    });

                    ramas.push(totalEgresoInv);

                    var saldoInv = totalInv - totalCdpInv;

                    ramas.push(saldoInv);

                    this.arbolInversion.push(ramas);
                }


            });


            this.arbolInversion.forEach(element => {
                this.arbol.push(element);
            });

            this.sumaTotalInversion();
        },

        sumaTotalInversion: async function(){
            let sumInicial = 0;
            let sumAdicion = 0;
            let sumReduccion = 0;
            let sumCredito = 0;
            let sumContraCredito = 0;
            let sumDefinitivo = 0;
            let sumCdp = 0;
            let sumRp = 0;
            let sumCxp = 0;
            let sumEgreso = 0;
            let sumSaldo = 0;
            let sumTot = [];

            await this.arbolInversion.forEach(element => {
                if(element[2] == 'C'){
                    sumInicial += Number.parseFloat(element[7]);
                    sumAdicion += Number.parseFloat(element[8]);
                    sumReduccion += Number.parseFloat(element[9]);
                    sumCredito += Number.parseFloat(element[10]);
                    sumContraCredito += Number.parseFloat(element[11]);
                    sumDefinitivo += Number.parseFloat(element[12]);
                    sumCdp += Number.parseFloat(element[13]);
                    sumRp += Number.parseFloat(element[14]);
                    sumCxp += Number.parseFloat(element[15]);
                    sumEgreso += Number.parseFloat(element[16]);
                    sumSaldo += Number.parseFloat(element[17]);
                }
            });

            sumTot.push('');
            sumTot.push('');
            sumTot.push('Total');
            sumTot.push('Total Gastos de Inversion');
            sumTot.push('');
            sumTot.push('');
            sumTot.push('');
            sumTot.push(sumInicial);
            sumTot.push(sumAdicion);
            sumTot.push(sumReduccion);
            sumTot.push(sumCredito);
            sumTot.push(sumContraCredito);
            sumTot.push(sumDefinitivo);
            sumTot.push(sumCdp);
            sumTot.push(sumRp);
            sumTot.push(sumCxp);
            sumTot.push(sumEgreso);
            sumTot.push(sumSaldo);

            this.sumaTotalGastosInversion.push(sumTot);
        },

        formatNumber: function (value) {

            return numeralIntl.NumberFormat(value);
        },

        toFormData: function(obj){
            var form_data = new FormData();
            for(var key in obj){
                form_data.append(key, obj[key]);
            }
            return form_data;
        },

        auxiliarPorComprobante: function(tipo_comprobante, fuente_f, cuenta_inversion){

            let datos;
            const tipo_comp = tipo_comprobante;
            const fechaini = document.getElementById('fc_1198971545').value;
            const fechafin = document.getElementById('fc_1198971546').value;
            const seccion_presupuestal = this.selectSeccion;
            const medio_pago = this.selectMedioPago;
            const vigencia_gasto = this.selectVigenciaGasto;
            const vigencia_fiscal = this.vigencia;
            const fuente = fuente_f;
            const parts = cuenta_inversion.split('-');
            let bpim_inv = '';
            let programatico_inv = '';
            let cuenta_f = '';
            if(parts.length > 1){
                bpim_inv = parts[1].toString().trim();
                programatico_inv = parts[0].toString().trim();
            }else{
                cuenta_f = cuenta_inversion;
            }

            datos = "tipo_comprobante=" + tipo_comp + "&fechaIni=" + fechaini + "&fechaFin=" + fechafin + "&seccion_presupuestal=" + seccion_presupuestal + "&medio_pago=" + medio_pago + "&vigencia_gasto=" + vigencia_gasto + "&vigencia_fiscal=" + vigencia_fiscal + "&fuente=" + fuente + "&bpim_inv=" + bpim_inv + "&programatico_inv=" + programatico_inv + "&cuenta_f=" + cuenta_f;

            mypop=window.open("ccp-auxiliarporcomprobante.php?" + datos,'','');
            mypop.focus();

		},

        downloadExl() {

            if (this.arbol != '') {

                document.form2.action="ccp-ejecucionpresupuestal-vue-excel.php";
				document.form2.target="_BLANK";
				document.form2.submit();
            }
            else {
                Swal.fire("Faltan datos", "warning");
            }
        },

        formatonumero: function(valor){
			return new Intl.NumberFormat("en-US", {style: "currency", currency: "USD"}).format(valor);
		},

    }
});
