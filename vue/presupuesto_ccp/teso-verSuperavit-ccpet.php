<?php
	require '../../comun.inc';
	$linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");
	$out = array('error' => false);

	$action="show";

    if(isset($_GET['action'])){
        $action=$_GET['action'];
    }

    if($action == "show")
    {

        $id = $_GET['id'];

        $sqlCabecera = "SELECT id_acuerdo,concepto,fecha,vigencia,valor FROM tesosuperavit WHERE id = $id ";
        $resCabecera = mysqli_query($linkbd,$sqlCabecera);
        $rowCabecera = mysqli_fetch_row($resCabecera);

        $sqlAcuerdo = "SELECT numero_acuerdo FROM ccpetacuerdos WHERE id_acuerdo = $rowCabecera[0]";
        $resAcuerdo = mysqli_query($linkbd,$sqlAcuerdo);
        $rowAcuerdo = mysqli_fetch_row($resAcuerdo);

        $acuerdo    = $rowCabecera[0]. ' - '.$rowAcuerdo[0];
        $concepto   = $rowCabecera[1];
        $fecha      = $rowCabecera[2];
        $vigencia   = $rowCabecera[3];
        $valorTotal = $rowCabecera[4];

        $sqlDetalles = "SELECT rubro,fuente,vigencia_gasto,clasificador,valor,clasificador_superavit FROM tesosuperavit_det WHERE id_tesosuperavit = $id ";
        $resDetalles = mysqli_query($linkbd,$sqlDetalles);
        $detalles = array();

        while($rowDetalles = mysqli_fetch_row($resDetalles))
        {
            array_push($detalles,$rowDetalles);
        }

        $out['acuerdo']    = $acuerdo;
        $out['concepto']   = $concepto;
        $out['fecha']      = $fecha;
        $out['vigencia']   = $vigencia;
        $out['valorTotal'] = $valorTotal;
        $out['detalles']   = $detalles;
    }





header("Content-type: application/json");
echo json_encode($out);
die();

?>
