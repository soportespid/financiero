<?php
	require '../../comun.inc';
	$linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");
	$out = array('error' => false);

	if(isset($_GET['buscar'])){
		switch ($_GET['buscar']){
			case 'years':{
				$sqlr="SELECT anio FROM admbloqueoanio WHERE bloqueado='N'";
				$res = mysqli_query($linkbd,$sqlr);
				$anio = array();
				while($row=mysqli_fetch_row($res)){
					array_push($anio, $row);
				}
				$out['anio'] = $anio;
			}break;
            case 'secpresu':{
                $Secpre = array();
                $sql = "SELECT codigo, nombre, base, usuario, tipo FROM redglobal WHERE cuipo = 'S'";
                $res = mysqli_query($linkbd,$sql);
				while($row = mysqli_fetch_row($res)){
					$datos = array();
					$agrupa = "$row[0]<->$row[2]<->$row[3]";
					array_push($datos, $row[0]);//codigo
					array_push($datos, $row[1]);//nombre
					array_push($datos, $row[2]);//base
					array_push($datos, $row[3]);//usuario
					array_push($datos, $row[4]);//tipo
					array_push($datos, $agrupa);//agrupado
					array_push($Secpre, $datos);
				}
				$out['Secpre'] = $Secpre;
            }break;
			case 'detSecto':{
				$detSector = array();
				$sql = "SELECT codigo, nombre FROM ccpparametroscuipo WHERE tipo = '1' AND estado = 'S'";
                $res = mysqli_query($linkbd,$sql);
				while($row = mysqli_fetch_row($res)){
					array_push($detSector, $row);
				}
				$out['detSector'] = $detSector;
			}break;
			case 'tnorma':{
				$dettNorma = array();
				$sql = "SELECT codigo, nombre FROM ccpparametroscuipo WHERE tipo = '2' AND estado = 'S'";
                $res = mysqli_query($linkbd,$sql);
				while($row = mysqli_fetch_row($res)){
					array_push($dettNorma, $row);
				}
				$out['dettNorma'] = $dettNorma;
			}break;
			case 'nfuentes':{
				$detfuente = array();
				$sql = "SELECT codigo, nombre FROM ccpparametroscuipo WHERE tipo = '3' AND estado = 'S'";
                $res = mysqli_query($linkbd,$sql);
				while($row = mysqli_fetch_row($res)){
					array_push($detfuente, $row);
				}
				$out['detfuente'] = $detfuente;
			}break;
		}
	}
	
	
	header("Content-type: application/json");
	echo json_encode($out);
	die();
?>