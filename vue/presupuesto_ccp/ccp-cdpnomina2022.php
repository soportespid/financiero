<?php
	require_once '../../comun.inc';
    require '../../funciones.inc';
    require 'funcionesccp.inc.php';
    session_start();

	$linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");


    $out = array('error' => false);

    $maxVersion = ultimaVersionGastosCCPET();

    $action = "show";

    if(isset($_GET['action'])){
        $action = $_GET['action'];
    }

    if($action == 'show'){

        $tiposDeMovimiento = array();

        $user = $_SESSION['cedulausu'];
        $sql = "SELECT * FROM permisos_movimientos WHERE usuario='$user' AND estado='T' ";
        $res = mysqli_query($linkbd, $sql);
        $num = mysqli_num_rows($res);
        if($num == 1){
            $sqlr = "SELECT id, codigo, descripcion FROM tipo_movdocumentos WHERE estado='S' AND modulo=3 AND (id='2' OR id='4')";
            $resp = mysqli_query($linkbd, $sqlr);
            while ($row = mysqli_fetch_row($resp)) 
            {
                $unionMovimiento = '';
                $codigoMovimiento = array();

                $unionMovimiento = $row[0]."".$row[1];

                array_push($codigoMovimiento, $unionMovimiento);
                array_push($codigoMovimiento, $row[2]);

                array_push($tiposDeMovimiento, $codigoMovimiento);
            }
        }else{
            $sql = "SELECT codmov,tipomov FROM permisos_movimientos WHERE usuario='$user' AND estado='S' AND modulo='3' AND transaccion='PGA' ";
            $res = mysqli_query($linkbd, $sql);
            while($row = mysqli_fetch_row($res)){
                array_push($tiposDeMovimiento, $row);
            }
        }

        $out['tiposDeMovimiento'] = $tiposDeMovimiento;
    }

    if($action == 'seleccionarConsecutivo'){
        
        $consecutivo = 0;
        $sqlr = "SELECT MAX(consvigencia) FROM ccpetcdp WHERE vigencia=$_GET[vigencia] AND tipo_mov='201'";
        $res = mysqli_query($linkbd, $sqlr);
        while($r = mysqli_fetch_row($res)){
            $maximo = $r[0];
        }
        if(!$maximo){
            $consecutivo = 1;
        }
        else{
            $consecutivo = $maximo+1;
        }

        $out['consecutivo'] = $consecutivo;
	}

	if($action == 'cargarVigencia'){

		$vigencia = vigencia_usuarios($_SESSION['cedulausu']); 
		$out['vigencia'] = $vigencia;

	}

	if($action == 'cargarnomina')
	{
		$numerosdenomina = array();
		$sqlr = "
		SELECT T1.nomina, T3.nombre, T2.vigencia
		FROM hum_nom_cdp_rp AS T1 
		INNER JOIN humnomina AS T2 
		ON T1.nomina = T2.id_nom
		INNER JOIN meses as T3
		ON T2.mes = T3.id
		WHERE T1.cdp = '0'AND T2.estado = 'S'
		ORDER BY T1.nomina DESC";
		$res = mysqli_query($linkbd, $sqlr);
		while($row = mysqli_fetch_row($res))
		{
			array_push($numerosdenomina, $row);
		}
		$out['numerosdenomina'] = $numerosdenomina;
	}

	if($action == 'cargarDetallesCdp'){
		$detallesCdp = array();
		
		$numNomina = $_GET['numNomina'];
		
		$sqlr = "SELECT * FROM humnom_presupuestal WHERE id_nom = '$numNomina' ORDER BY tipo_gasto , seccion_presupuestal";
		$res = mysqli_query($linkbd, $sqlr);
		while($row = mysqli_fetch_row($res)){
			array_push($detallesCdp, $row);
		}
		
		$out['detallesCdp'] = $detallesCdp;
	}


    if($action == 'cargarTiposDeGasto'){

        $tiposDeGasto = array();

        $sqlr = "SELECT * FROM ccpettipo_gasto WHERE estado = 'S'";
        $res = mysqli_query($linkbd, $sqlr);
        while($row = mysqli_fetch_row($res)){
            array_push($tiposDeGasto, $row);
        }

        $out['tiposDeGasto'] = $tiposDeGasto;
    }

    if($action == 'cargarCuentas'){

        $cuentasCcpet = array();
        $inicioCuenta = $_GET['inicioCuenta'];
        $sqlr = "SELECT * FROM cuentasccpet WHERE version = $maxVersion AND codigo LIKE '$inicioCuenta%' ORDER BY id ASC";
        $res = mysqli_query($linkbd, $sqlr);
        while($row = mysqli_fetch_row($res)){
            array_push($cuentasCcpet, $row);
        }

        $out['cuentasCcpet'] = $cuentasCcpet;
    }

    if($action == 'cargarProgramaticos'){

        $cuentasCcpet = array();
        
        $programaticos = array();
        $proyecto = $_GET['proyecto'];
        $sqlr = "SELECT id FROM ccpproyectospresupuesto WHERE codigo = '$proyecto'";
        $res = mysqli_query($linkbd, $sqlr);
        
        while($row = mysqli_fetch_row($res)){
            $sqlrP = "SELECT indicador_producto FROM ccpproyectospresupuesto_presupuesto WHERE codproyecto = '$row[0]' GROUP BY indicador_producto";
            $resP = mysqli_query($linkbd, $sqlrP);
            $programatico = array();
            while($rowP = mysqli_fetch_row($resP)){
                $programatico = array();
                $sqlrNom = "SELECT indicador_producto FROM ccpetproductos WHERE codigo_indicador = '$rowP[0]'";
                $resNom = mysqli_query($linkbd, $sqlrNom);
                $rowNom = mysqli_fetch_row($resNom);
                array_push($programatico, $rowP[0]);
                array_push($programatico, $rowNom[0]);
                
                array_push($programaticos, $programatico);

            }
            
        }
        

        $out['programaticos'] = $programaticos;
    }

    if($action == 'cargarProyecto'){

        $proyectosCcpet = array();
        $vigencia = $_GET['vigencia'];
        $sqlr = "SELECT * FROM ccpproyectospresupuesto WHERE vigencia = '$vigencia'";
        $res = mysqli_query($linkbd, $sqlr);
        while($row = mysqli_fetch_row($res)){
            array_push($proyectosCcpet, $row);
        }

        $out['proyectosCcpet'] = $proyectosCcpet;
    }

    if($action == 'buscarCuenta'){
        $cuenta = $_GET['cuenta'];
        $resultBusquedaCuenta = array();

        $sqlr = "SELECT nombre, tipo FROM cuentasccpet WHERE version = $maxVersion AND codigo = '$cuenta' ORDER BY id ASC";
        $res = mysqli_query($linkbd, $sqlr);//echo $sqlr;
        while($row = mysqli_fetch_row($res)){
            array_push($resultBusquedaCuenta, $row);
        }

        $out['resultBusquedaCuenta'] = $resultBusquedaCuenta;

    }

    if($action == 'buscarProyecto'){
        $proyecto = $_GET['proyecto'];
        $resultBusquedaProyecto = array();

        $sqlr = "SELECT nombre FROM ccpproyectospresupuesto WHERE codigo = '$proyecto'";
        $res = mysqli_query($linkbd, $sqlr);//echo $sqlr;
        while($row = mysqli_fetch_row($res)){
            array_push($resultBusquedaProyecto, $row);
        }

        $out['resultBusquedaProyecto'] = $resultBusquedaProyecto;

    }

    if($action == 'buscarProgramatico'){
        $programatico = $_GET['programatico'];
        $resultBusquedaProgramatico = array();

        $sqlr = "SELECT indicador_producto FROM ccpetproductos WHERE codigo_indicador = '$programatico'";
        $res = mysqli_query($linkbd, $sqlr);//echo $sqlr;
        while($row = mysqli_fetch_row($res)){
            array_push($resultBusquedaProgramatico, $row);
        }

        $out['resultBusquedaProgramatico'] = $resultBusquedaProgramatico;

    }

    if($action == 'buscarFuente'){
        $fuente = $_GET['fuente'];
        $resultBusquedaFuente = array();

        $sqlr = "SELECT nombre FROM ccpet_fuentes_cuipo WHERE version = (SELECT MAX(version) FROM ccpet_fuentes_cuipo ) AND codigo_fuente = '$fuente'";
        $res = mysqli_query($linkbd, $sqlr);//echo $sqlr;
        while($row = mysqli_fetch_row($res)){
            array_push($resultBusquedaFuente, $row);
        }

        $out['resultBusquedaFuente'] = $resultBusquedaFuente;
    }

    if($action == 'buscarDivipola'){
        $divipola = $_GET['divipola'];
        $resultBusquedaDivipola = array();

        $sqlr = "SELECT nombre FROM ccpet_divipolas WHERE version = (SELECT MAX(version) FROM ccpet_divipolas ) AND codigo = '$divipola'";
        $res = mysqli_query($linkbd, $sqlr);//echo $sqlr;
        while($row = mysqli_fetch_row($res)){
            array_push($resultBusquedaDivipola, $row);
        }

        $out['resultBusquedaDivipola'] = $resultBusquedaDivipola;
    }

    if($action == 'buscarChip'){
        $chip = $_GET['chip'];
        $resultBusquedaChip = array();

        $sqlr = "SELECT nombre FROM ccpet_chip WHERE version = (SELECT MAX(version) FROM ccpet_chip ) AND codigo = '$chip'";
        $res = mysqli_query($linkbd, $sqlr);//echo $sqlr;
        while($row = mysqli_fetch_row($res)){
            array_push($resultBusquedaChip, $row);
        }

        $out['resultBusquedaChip'] = $resultBusquedaChip;
    }

    if($action == 'buscarCpc'){
        $codigoCpc = $_GET['codigoCpc'];

        $resultBusquedaCpc = array();

        $rest = substr($codigoCpc, 0, 1);

        if($rest <= 4){
            $sqlr = "SELECT titulo FROM ccpetbienestransportables WHERE version = (SELECT MAX(version) FROM ccpetbienestransportables ) AND grupo = '$codigoCpc'";
            
        }else{
            $sqlr = "SELECT titulo FROM ccpetservicios WHERE version = (SELECT MAX(version) FROM ccpetservicios ) AND grupo = '$codigoCpc'";
        }
        
        $res = mysqli_query($linkbd, $sqlr);
        while($row = mysqli_fetch_row($res)){
            array_push($resultBusquedaCpc, $row);
        }
        

        $out['resultBusquedaCpc'] = $resultBusquedaCpc;
    }



    if($action == 'filtrarCuentas'){

        $keywordCuenta=$_POST['keywordCuenta'];

        $cuentasCcpet = array();
        $inicioCuenta = $_GET['inicioCuenta'];

        $sqlr = "SELECT * FROM cuentasccpet WHERE version = $maxVersion AND codigo LIKE '$inicioCuenta%' AND concat_ws(' ', codigo, nombre) LIKE '%$keywordCuenta%' ORDER BY id ASC";//echo $sqlr;
        $res = mysqli_query($linkbd, $sqlr);
        while($row = mysqli_fetch_row($res)){
            array_push($cuentasCcpet, $row);
        }

        $out['cuentasCcpet'] = $cuentasCcpet;
    }

    if($action == 'filtrarProyectos'){

        $keywordProyecto = $_POST['keywordProyecto'];

        $proyectosCcpet = array();
        $vigencia = $_GET['vigencia'];
        $sqlr = "SELECT * FROM ccpproyectospresupuesto WHERE vigencia = '$vigencia' AND concat_ws(' ', codigo, nombre) LIKE '%$keywordProyecto%'";
        $res = mysqli_query($linkbd, $sqlr);
        while($row = mysqli_fetch_row($res)){
            array_push($proyectosCcpet, $row);
        }

        $out['proyectosCcpet'] = $proyectosCcpet;
    }

    if($action == 'cargarFuentes'){

        $fuentes = array();
        $sqlrMaxFuente = "SELECT MAX(version) FROM ccpet_fuentes_cuipo";
        $resMaxFuente = mysqli_query($linkbd, $sqlrMaxFuente);
        $rowMaxFuente = mysqli_fetch_row($resMaxFuente);

        $cuenta = $_GET['cuenta'];
        $tipoGasto = $_GET['tipoGasto'];
        $proyecto = $_GET['proyecto'];
        $programatico = $_GET['programatico'];

        //echo $_GET['programatico'];

        if($tipoGasto != '3'){
            $sql = "SELECT ccp_f.codigo_fuente, ccp_f.nombre FROM ccpet_fuentes_cuipo AS ccp_f, ccpetinicialgastosfun AS ccp_g_i WHERE ccp_f.version = '$rowMaxFuente[0]' AND ccp_f.codigo_fuente =  ccp_g_i.fuente AND ccp_g_i.cuenta = '$cuenta' AND ccp_g_i.valor > 0 GROUP BY ccp_f.codigo_fuente";
        
        }else{
            $sql = "SELECT ccp_f.codigo_fuente, ccp_f.nombre FROM ccpet_fuentes_cuipo AS ccp_f, ccpproyectospresupuesto AS ccp_g_i, ccpproyectospresupuesto_presupuesto AS cpp_g_i_d WHERE ccp_f.version = '$rowMaxFuente[0]' AND ccp_f.codigo_fuente =  cpp_g_i_d.id_fuente AND ccp_g_i.codigo = '$proyecto' AND (cpp_g_i_d.valorcsf > 0 OR cpp_g_i_d.valorssf > 0) AND cpp_g_i_d.indicador_producto = '$programatico' AND ccp_g_i.id = cpp_g_i_d.codproyecto GROUP BY ccp_f.codigo_fuente";
        }

        //echo $sql;
        $res = mysqli_query($linkbd, $sql);

        while($row = mysqli_fetch_row($res))
        {
            array_push($fuentes, $row);
        }
    
        $out['fuentes'] = $fuentes;

    }

    if($action == 'cargarDivipolas'){

        $divipolas = array();
        $sql = "SELECT codigo, nombre FROM ccpet_divipolas WHERE version = (SELECT MAX(version) FROM ccpet_divipolas )";
        $res = mysqli_query($linkbd, $sql);
        
        while($row = mysqli_fetch_row($res))
        {
            array_push($divipolas, $row);
        }
    
        $out['divipolas'] = $divipolas;

    }

    if($action == 'cargarChips'){

        $chips = array();
        $sql = "SELECT codigo, nombre FROM ccpet_chip WHERE version = (SELECT MAX(version) FROM ccpet_chip )";
        $res = mysqli_query($linkbd, $sql);
        
        while($row = mysqli_fetch_row($res))
        {
            array_push($chips, $row);
        }
    
        $out['chips'] = $chips;

    }

    if($action == 'filtrarFuentes'){

        $keywordFuente = $_POST['keywordFuente'];
        
        $sql = "SELECT codigo_fuente, nombre FROM ccpet_fuentes_cuipo WHERE concat_ws(' ', codigo_fuente, nombre) LIKE '%$keywordFuente%' AND version = (SELECT MAX(version) FROM ccpet_fuentes_cuipo )";
        $res = mysqli_query($linkbd,$sql);
        
        $fuentes = array();
    
        while($row = mysqli_fetch_row($res))
        {
            array_push($fuentes, $row);
        }
    
        $out['fuentes'] = $fuentes;

    }

    if($action == 'filtrarDivipolas'){

        $keywordDivipola = $_POST['keywordDivipola'];
        
        $sql = "SELECT codigo, nombre FROM ccpet_divipolas WHERE concat_ws(' ', codigo, nombre) LIKE '%$keywordDivipola%' AND version = (SELECT MAX(version) FROM ccpet_divipolas )";
        $res = mysqli_query($linkbd,$sql);
        $divipolas = array();
    
        while($row = mysqli_fetch_row($res))
        {
            array_push($divipolas, $row);
        }
    
        $out['divipolas'] = $divipolas;

    }

    if($action == 'filtrarChips'){

        $keywordChip = $_POST['keywordChip'];
        
        $sql = "SELECT codigo, nombre FROM ccpet_chip WHERE concat_ws(' ', codigo, nombre) LIKE '%$keywordChip%' AND version = (SELECT MAX(version) FROM ccpet_chip )";
        $res = mysqli_query($linkbd,$sql);
        $chips = array();
    
        while($row = mysqli_fetch_row($res))
        {
            array_push($chips, $row);
        }
    
        $out['chips'] = $chips;

    }


    //[rubro, fuente, vigencia, tipo_gasto, seccion_presupuestal, medio_pago, vigencia_gasto]
    if($action == 'saldoPorCuenta'){
        
        $parametros = [
            'rubro' => $_POST['rubro'],
            'fuente' => $_POST['fuente'],
            'vigencia' => $_POST['vigencia'],
            'tipo_gasto' => $_POST['tipo_gasto'],
            'seccion_presupuestal' => $_POST['seccion_presupuestal'],
            'medio_pago' => $_POST['medio_pago'],
            'vigencia_gasto' => $_POST['vigencia_gasto'],

            'codProyecto' => $_POST['codProyecto'],
            'programatico' => $_POST['programatico'],
        ];

        $saldoPorCuenta = 0;
        
        $saldoPorCuenta = saldoPorRubro($parametros);

        $out['saldoPorCuenta'] = $saldoPorCuenta;
    }

    

    if($action == 'guardarCdp'){
        $user = $_SESSION['nickusu'];
        $estado = '';
        if($_POST["tipoDeMovimiento"] == '201'){
            $estado = 'S';
        }else{
            $estado = 'R';
        }

        $vigencia = $_POST["vigencia"];

        $sqlr = "SELECT MAX(consvigencia) FROM ccpetcdp WHERE vigencia = $vigencia AND tipo_mov = '201'";
        $res = mysqli_query($linkbd, $sqlr);
        while($r = mysqli_fetch_row($res)){
            $maximo = $r[0];
        }
        if(!$maximo){
            $consecutivo = 1;
        }
        else{
            $consecutivo = $maximo+1;
        }
        
        preg_match("/([0-9]{2})\/([0-9]{2})\/([0-9]{4})/", $_POST["fecha"], $fecha);
		$fechaf = $fecha[3]."-".$fecha[2]."-".$fecha[1];
		$sql = "UPDATE hum_nom_cdp_rp SET cdp = '".$_POST['consecutivo']."' WHERE nomina = '".$_POST['nomina']."'";
		mysqli_query($linkbd, $sql);

        $sqlr = "INSERT INTO ccpetcdp (vigencia, consvigencia, fecha, valor, estado, solicita, objeto, saldo, tipo_mov, user) VALUES ('".$_POST["vigencia"]."', '".$_POST["consecutivo"]."', '$fechaf', '".$_POST["totalCdp"]."', '$estado', '".$_POST["solicita"]."', '".$_POST["objeto"]."', '".$_POST["totalCdp"]."', '".$_POST["tipoDeMovimiento"]."', '$user')";

        if(mysqli_query($linkbd, $sqlr)){
            for($x = 0; $x < count($_POST["detallesCdp"]); $x++){
                $rubro = '';
                $fuente = '';
                $valor = '';
                $saldo = '';
                $indicador_producto = '';
                $medio_pago = '';
                $codigo_vigenciag = '';
                $bpim = '';
                $seccion_presupuestal = '';
                $tipo_gasto = '';
                $divipola = '';
                $chip = '';
                $productoservicio = '';
                
                $rubro = $_POST["detallesCdp"][$x][1];
                $fuente = $_POST["detallesCdp"][$x][4];
                $valor = $_POST["detallesCdp"][$x][2];
                $saldo = $_POST["detallesCdp"][$x][2];
                $indicador_producto = $_POST["detallesCdp"][$x][9];
                $medio_pago = $_POST["detallesCdp"][$x][6];
                $codigo_vigenciag = $_POST["detallesCdp"][$x][13];
                $bpim = $_POST["detallesCdp"][$x][10];
                $seccion_presupuestal = $_POST["detallesCdp"][$x][12];
                $tipo_gasto = $_POST["detallesCdp"][$x][0];
                $divipola = $_POST["detallesCdp"][$x][14];
                $chip = $_POST["detallesCdp"][$x][15];
                $productoservicio = $_POST["detallesCdp"][$x][8];


                /* $rubro = $_POST["detallesCdp"][$x][10];
                $bpim = '';

                if($_POST["detallesCdp"][$x][0] == 'Inversion'){
                    $rubro = '2.99';
                    $bpim = $_POST["detallesCdp"][$x][6];
                }else{
                    $rubro = $_POST["detallesCdp"][$x][6];
                } */

                $sqlrD = "INSERT INTO ccpetcdp_detalle(vigencia, consvigencia, cuenta, productoservicio, fuente, valor, estado, saldo, tipo_mov, indicador_producto, medio_pago, codigo_vigenciag, bpim, seccion_presupuestal, tipo_gasto, divipola, chip) VALUES ('".$vigencia."', '".$consecutivo."', '".$rubro."', '".$productoservicio."', '".$fuente."', '".$valor."', '$estado', '".$saldo."', '".$_POST["tipoDeMovimiento"]."', '".$indicador_producto."', '".$medio_pago."', '".$codigo_vigenciag."', '$bpim', '".$seccion_presupuestal."', '".$tipo_gasto."', '".$divipola."', '".$chip."')";
                mysqli_query($linkbd, $sqlrD);
            }
            $out['insertaBien'] = true;
        }else{
            $out['insertaBien'] = false;
        }

    }

    if($action == 'tieneCpc'){

        $cuenta = $_GET['cuenta'];

        $tieneCpc = '';

        $sqlr = "SELECT clasificadores FROM ccpetprogramarclasificadoresgastos WHERE cuenta = '$cuenta'";
        $res = mysqli_query($linkbd,$sqlr);
        $row = mysqli_fetch_row($res);
        $tieneCpc =  $row[0];
        $out['tieneCpc'] = $tieneCpc;

    }

    header("Content-type: application/json");
    echo json_encode($out);
    die();