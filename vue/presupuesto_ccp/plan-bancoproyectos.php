<?php
	require '../../comun.inc';
	$linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");
	$out = array('error' => false);
	session_start();
	/*ini_set('display_errors', '1');
    ini_set('display_startup_errors', '1');
    error_reporting(E_ALL);*/
	$action="show";

	if($_POST){
		$obj = new Plantilla();
		switch ($_POST['buscar']){
			case 'validacod':
				$obj->validarProyecto();
				break;
			case 'years':
				$obj->buscarYear();
				break;
			case 'sectores':
				$obj->buscarSectores();
				break;
			case 'seccionpresupuestal':
				$obj->buscarSeccionPresupuestal();
				break;
			case 'subprogramas':
				$obj->buscarSubprogramas();
				break;
			case 'subprogramas2':
				$obj->buscarProductos();
				break;
			case 'searchProgram':
				$obj->buscarPrograma();
				break;
			case 'searchSector':
				$obj->buscarSector();
				break;
			case 'searchProduct':
				$obj->buscarProducto();
				break;
		}
	}
	class Plantilla{
		private $linkbd;
		private $arrData;
		public function __construct() {
			$this->linkbd = conectar_v7();
			$this->linkbd->set_charset("utf8");
		}
		public function validarProyecto(){
			if(!empty($_SESSION)){
				$idProyecto = $this->selectIdProyecto();
				echo json_encode($idProyecto, JSON_UNESCAPED_UNICODE);
			}
			die();
		}
		public function selectIdProyecto(){
			$codigo = $_POST['codi'];
			$vigencia = $_POST['vigen'];
			$seccionPresu = $_POST['seccionPresu'];
			$sql = "SELECT id FROM ccpproyectospresupuesto WHERE codigo = '$codigo' AND vigencia = '$vigencia' AND idunidadej = '$seccionPresu'";
			$request = mysqli_fetch_assoc(mysqli_query($this->linkbd, $sql));
			return $request['id'];
		}
		public function buscarYear(){
			$sqlr = "SELECT anio FROM admbloqueoanio WHERE bloqueado = 'N' ORDER BY anio DESC";
			$res = mysqli_query($this->linkbd, $sqlr);
			$anio = array();
			while($row = mysqli_fetch_assoc($res)) {
				$anio[] = $row['anio'];
			}
			echo json_encode($anio, JSON_UNESCAPED_UNICODE);
			die();
		}
		public function buscarSectores(){
			$sql = "SELECT codigo, nombre, aplicacion FROM ccpetsectores WHERE version = (SELECT MAX(version) FROM ccpetsectores)";
			$res = mysqli_query($this->linkbd, $sql);
			$sectores = array();
			while($row = mysqli_fetch_assoc($res)) {
				$sectores[] = $row;
			}
			echo json_encode($sectores, JSON_UNESCAPED_UNICODE);
			die();
		}
		public function buscarSeccionPresupuestal() {
			$sql = "SELECT id_seccion_presupuestal AS codigo, id_unidad_ejecutora AS idUE, nombre, estado, aesgpriet FROM pptoseccion_presupuestal WHERE estado = 'S' ORDER BY id_seccion_presupuestal ASC";
			$res = mysqli_query($this->linkbd, $sql);
			$unidadesejecutoras = array();
			while($row = mysqli_fetch_assoc($res)) {
				$unidadesejecutoras[] = $row;
			}
			echo json_encode($unidadesejecutoras, JSON_UNESCAPED_UNICODE);
			die();
		}
		public function buscarSubprogramas() {
			$sector = $_POST['sector'];
			$sqlr = "SELECT codigo, nombre, codigo_subprograma, nombre_subprograma, aplicacion FROM ccpetprogramas WHERE LEFT(codigo, 2) = '$sector'";
			$res = mysqli_query($this->linkbd, $sqlr);
			$programas = array();
			while ($row = mysqli_fetch_assoc($res)) {
				$programas[] = $row;
			}
			echo json_encode($programas, JSON_UNESCAPED_UNICODE);
			die();
		}
		public function buscarProductos() {
			$programa = $_POST['programa'];
			$sqlr = "SELECT cod_producto, producto, descripcion, medio_a_traves, codigo_indicador, indicador_producto, unidad_medida, indicador_principal FROM ccpetproductos WHERE LEFT(cod_producto, 4) = '$programa'";
			$res = mysqli_query($this->linkbd, $sqlr);
			$productos = array();
			while ($row = mysqli_fetch_assoc($res)) {
				$productos[] = $row;
			}
			echo json_encode($productos, JSON_UNESCAPED_UNICODE);
			die();
		}
		public function buscarPrograma() {
			$keyword = $_POST['keywordProgram'];
			$sectorSearch = $_POST['sectorSearch'];
			$sql = "SELECT codigo, nombre, codigo_subprograma, nombre_subprograma, aplicacion FROM ccpetprogramas WHERE CONCAT(codigo, nombre) LIKE '%$keyword%' AND LEFT(codigo, 2) = '$sectorSearch'";
			$res = mysqli_query($this->linkbd, $sql);
			$programas = array();
			while ($row = mysqli_fetch_assoc($res)) {
				$programas[] = $row;
			}
			echo json_encode($programas, JSON_UNESCAPED_UNICODE);
			die();
		}
		public function buscarSector() {
			$keyword = $_POST['keyword'];
			$sql = "SELECT codigo, nombre, aplicacion FROM ccpetsectores WHERE CONCAT(codigo, nombre) LIKE '%$keyword%' AND version = (SELECT MAX(version) FROM ccpetsectores)";
			$res = mysqli_query($this->linkbd, $sql);
			$codigos = array();
			while ($row = mysqli_fetch_assoc($res)) {
				$codigos[] = $row;
			}
			echo json_encode($codigos, JSON_UNESCAPED_UNICODE);
			die();
		}
		public function buscarProducto() {
			$keywordProduct = $_POST['keywordProduct'];
			$programSearch = $_POST['programSearch'];
			$sql = "SELECT cod_producto, producto, descripcion, medio_a_traves, codigo_indicador, indicador_producto, unidad_medida, indicador_principal FROM ccpetproductos WHERE CONCAT(cod_producto, producto, codigo_indicador) LIKE '%$keywordProduct%' AND LEFT(cod_producto, 4) = '$programSearch'";
			$res = mysqli_query($this->linkbd, $sql);
			$productos = array();
			while ($row = mysqli_fetch_assoc($res)) {
				$productos[] = $row;
			}
			echo json_encode($productos, JSON_UNESCAPED_UNICODE);
			die();
		}


	}
	if(isset($_GET['action'])){
        $action = $_GET['action'];
    }


	if($action == 'consultarProgramatico'){
		$existe = false;
		if(isset($_POST['bpim']) && isset($_POST['programatico']))
		{
            $seccionPresu = $_POST['seccionPresu'];
			$sqlr = "SELECT SUM(TB2.valorcsf), SUM(TB2.valorssf) FROM ccpproyectospresupuesto AS TB1, ccpproyectospresupuesto_presupuesto AS TB2 WHERE TB1.codigo = '".$_POST['bpim']."' AND TB1.idunidadej = '$seccionPresu' AND TB1.vigencia = '".$_POST['vigencia']."' AND TB1.id = TB2.codproyecto AND TB2.indicador_producto = '".$_POST['programatico']."'";
			$resp = mysqli_query($linkbd, $sqlr);
			$row = mysqli_fetch_row($resp);

			if($row[0] > 0 || $row[1] > 0){
				$existe = true;
			}

			$sqlrAd = "SELECT SUM(valor) FROM ccpet_adiciones WHERE bpim = '".$_POST['bpim']."' AND programatico = '".$_POST['programatico']."' AND vigencia = '".$_POST['vigencia']."' ";
			$respAd = mysqli_query($linkbd, $sqlrAd);
			$rowAd = mysqli_fetch_row($respAd);

			if($rowAd[0] > 0){
				$existe = true;
			}

			$sqlrTraslado = "SELECT SUM(valor) FROM ccpet_traslados WHERE bpim = '".$_POST['bpim']."' AND programatico = '".$_POST['programatico']."' AND vigencia = '".$_POST['vigencia']."' ";
			$respTraslado = mysqli_query($linkbd, $sqlrTraslado);
			$rowTraslado = mysqli_fetch_row($respTraslado);

			if($rowTraslado[0] > 0){
				$existe = true;
			}

		}
		$out['existe'] = $existe;
	}
	function selconsecutivo($base,$campo)
	{
		$conexion=conectar_v7();
		$sqlr="SELECT MAX(CONVERT($campo, SIGNED INTEGER)) FROM $base";
		$resp = mysqli_query($conexion,$sqlr);
		while ($row =mysqli_fetch_row($resp)){$mx=$row[0];}
		$mx=$mx+1;
		return $mx;
	}
	if(isset($_GET['numid']))
	{
		$sqlr="SELECT MAX(id) FROM ccpproyectospresupuesto";
		$resp = mysqli_query($linkbd,$sqlr);
		while ($row =mysqli_fetch_row($resp)){$mx=$row[0];}
		$mx=$mx+1;
		$out['numid']=$mx;
	}
	if(isset($_GET['guardar']))
	{
		$guardar=$_GET['guardar'];
		switch ($guardar)
		{
			case 'cabecera':
			{
				$numid = $_POST['idnum'];
				$numidaux = $_POST['idnumaux'];
				$idunidadej=$_POST['idunidadej'];
				$codigo=$_POST['codigo'];
				$vigencia=$_POST['vigencia'];
				$nombre=$_POST['nombre'];
				$descripcion=$_POST['descripcion'];
				$valortotal=$_POST['valortotal'];
				if($numidaux != ''){
					$sql="DELETE FROM ccpproyectospresupuesto WHERE id = $numidaux";
					mysqli_query($linkbd,$sql);
				}
				$sql="INSERT INTO ccpproyectospresupuesto (id,idunidadej,codigo,vigencia,nombre,descripcion,valortotal,aprobado,estado) VALUES ('$numid', '$idunidadej', '$codigo','$vigencia','$nombre', '$descripcion','$valortotal','N','S')";
				mysqli_query($linkbd,$sql);
			}break;
			case 'productos':
			{
				$numid = $_POST['idnum'];
				$numidaux = $_POST['idnumaux'];
				$codigo=$_POST['infproyectos'];
				$valorpro=$_POST['valproductos'];
				if($numidaux != ''){
					$sql = "DELETE FROM ccpproyectospresupuesto_productos WHERE codproyecto = $numidaux";
					mysqli_query($linkbd,$sql);
				}
				for($x=0;$x < count($codigo);$x++)
				{
					$productos = explode(",", $codigo[$x]);
					$idproducto=selconsecutivo('ccpproyectospresupuesto_productos','id');
					$sql="INSERT INTO ccpproyectospresupuesto_productos (id,codproyecto,sector,programa,subprograma,producto,indicador,estado,valortotal) VALUES ('$idproducto','$numid','$productos[2]','$productos[3]','$productos[4]','$productos[0]','$productos[1]','S','".$valorpro[$productos[5]]."')";
					mysqli_query($linkbd,$sql);
					unset($productos);
				}
			}break;
			case 'ccuentascuin':
			{
				$numid=$_POST['idnum'];
				$codigo=$_POST['infcuentascuin'];
				for($x=0;$x < count($codigo);$x++)
				{
					$cuentas = explode(",", $codigo[$x]);
					if($cuentas[6]=='CSF')
					{
						$valcsf=$cuentas[4];
						$valssf=0;
					}
					else
					{
						$valcsf=0;
						$valssf=$cuentas[4];
					}
					$idcuenta=selconsecutivo('ccpproyectospresupuesto_presupuesto','id');
					$sql="INSERT INTO ccpproyectospresupuesto_presupuesto (id,codproyecto,id_fuente,medio_pago,rubro,clasificacion,seccion,divicion,grupo, clase,subclase,identidad,nitentidad,codigocuin,valorcsf,valorssf,estado,indicador_producto,politicas_publicas,vigencia_gasto) VALUES ('$idcuenta','$numid','$cuentas[5]', '$cuentas[6]','$cuentas[0]','1','','','','','','$cuentas[1]','$cuentas[2]','$cuentas[3]','$valcsf','$valssf','S','$cuentas[8]','$cuentas[9]','$cuentas[10]')";
					mysqli_query($linkbd,$sql);
					unset($cuentas);
				}
			}break;
			case 'ccuentassinclasificador':
			{
				$numid=$_POST['idnum'];
				$codigo=$_POST['infcuentassinclasificador'];
				for($x=0;$x < count($codigo);$x++)
				{
					$cuentas = explode(",", $codigo[$x]);
					if($cuentas[3]=='CSF')
					{
						$valcsf=$cuentas[1];
						$valssf=0;
					}
					else
					{
						$valcsf=0;
						$valssf=$cuentas[1];
					}
					$idcuenta=selconsecutivo('ccpproyectospresupuesto_presupuesto','id');
					$sql="INSERT INTO ccpproyectospresupuesto_presupuesto (id,codproyecto,id_fuente,medio_pago,rubro,clasificacion,seccion,divicion,grupo,clase, subclase,identidad,nitentidad,codigocuin,valorcsf,valorssf,estado,indicador_producto,politicas_publicas,vigencia_gasto) VALUES ('$idcuenta','$numid','$cuentas[2]','$cuentas[3]', '$cuentas[0]','0', '','','','','','','','','$valcsf','valssf','S','$cuentas[5]','$cuentas[7]','$cuentas[8]')";
					mysqli_query($linkbd,$sql);
					unset($cuentas);
				}
			}break;
			case 'cuentasb':
			{
				$numid=$_POST['idnum'];
				$codigo=$_POST['infcuentasb'];
				for($x=0;$x < count($codigo);$x++)
				{
					$cuentas = explode(",", $codigo[$x]);
					if($cuentas[9]=='CSF')
					{
						$valcsf=$cuentas[7];
						$valssf=0;
					}
					else
					{
						$valcsf=0;
						$valssf=$cuentas[7];
					}
					$idcuenta=selconsecutivo('ccpproyectospresupuesto_presupuesto','id');
					$sql="INSERT INTO ccpproyectospresupuesto_presupuesto (id,codproyecto,id_fuente,medio_pago,rubro,clasificacion,seccion,divicion,grupo, clase,subclase,subproducto,identidad,nitentidad,codigocuin,valorcsf,valorssf,estado,indicador_producto,politicas_publicas,vigencia_gasto) VALUES ('$idcuenta','$numid', '$cuentas[8]','$cuentas[9]','$cuentas[0]','$cuentas[1]','$cuentas[2]','$cuentas[3]','$cuentas[4]','$cuentas[5]', '$cuentas[6]','$cuentas[10]', '','','','$valcsf','$valssf', 'S','$cuentas[12]','$cuentas[13]','$cuentas[14]')";
					mysqli_query($linkbd,$sql);
					unset($cuentas);
				}
			}break;
			case 'gglobal':
			{
				//guardar cabecera
				$numid = $_POST['idnum'];
				$numidaux = $_POST['idnumaux'];
				$idunidadej = $_POST['idunidadej'];
				$codigo1 = $_POST['codigo'];
				$vigencia = $_POST['vigencia'];
				$nombre = $_POST['nombre'];
				$descripcion = $_POST['descripcion'];
				$valortotal = $_POST['valortotal'];
				if($numidaux != ''){
					$sql="DELETE FROM ccpproyectospresupuesto WHERE id = '$numidaux'";
					mysqli_query($linkbd,$sql);
					$sql = "DELETE FROM ccpproyectospresupuesto_productos WHERE codproyecto = '$numidaux'";
					mysqli_query($linkbd,$sql);
				}
				$sql = "INSERT INTO ccpproyectospresupuesto (id,idunidadej,codigo,vigencia,nombre,descripcion,valortotal,aprobado,estado) VALUES ('$numid', '$idunidadej', '$codigo1','$vigencia','$nombre', '$descripcion','$valortotal','N','S')";
				mysqli_query($linkbd,$sql);
				//guardar productos
				$codigo2 = $_POST['infproyectos'];
				$valorpro = $_POST['valproductos'];
				for($x = 0; $x < count($codigo2); $x++)
				{
					$productos = explode(",", $codigo2[$x]);
					$idproducto = selconsecutivo('ccpproyectospresupuesto_productos','id');
					$sql = "INSERT INTO ccpproyectospresupuesto_productos (id, codproyecto, sector, programa, subprograma, producto, indicador, estado, valortotal) VALUES ('$idproducto', '$numid', '$productos[2]', '$productos[3]', '$productos[4]', '$productos[0]', '$productos[1]', 'S', '".$valorpro[$productos[5]]."')";
					mysqli_query($linkbd,$sql);
					unset($productos);
				}
			}break;
			//default: echo 'H:'.$guardar;
		}
	}
	if(isset($_GET['buscar']))
	{
		$buscar=$_GET['buscar'];
		switch ($buscar)
		{
			case 'nombregrupo':
			{
				$grupo=$_GET['grupo'];
				$sql = "SELECT titulo FROM ccpetbienestransportables WHERE grupo like '$grupo'";
				$res = mysqli_query($linkbd,$sql);
				$codigos = array();
				while($row=mysqli_fetch_row($res))
				{
					array_push($codigos, $row);
				}
				$out['codigos'] = $codigos;
			}break;
			case 'nombregrupos':
			{
				$grupo=$_GET['grupo'];
				$sql = "SELECT titulo FROM ccpetservicios WHERE grupo like '$grupo'";
				$res = mysqli_query($linkbd,$sql);
				$codigos = array();
				while($row=mysqli_fetch_row($res))
				{
					array_push($codigos, $row);
				}
				$out['codigos'] = $codigos;
			}break;
			case 'subproducto':
			{
				$keywordsubproductos = $_POST['keywordsubproductos'];
				if(isset($_GET['seccion']))
				{
					if($_GET['seccion']!='')
					{
						$idt="AND LEFT(grupo,".$_GET['nivel'].")='".$_GET['seccion']."'";
					}
					else {$idt='';}
				}
				else {$idt='';}
				$sql="SELECT MAX(version) FROM ccpetbienestransportables";
				$res = mysqli_query($linkbd,$sql);
				$row=mysqli_fetch_row($res);
				$maxversion=$row[0];
				$sql = "SELECT grupo, titulo, ciiu, sistema_armonizado, cpc FROM ccpetbienestransportables WHERE concat_ws(' ', grupo, titulo) like '%$keywordsubproductos%' AND version='$maxversion' AND LENGTH(grupo) = '7' $idt";
				$res = mysqli_query($linkbd,$sql);
				$codigos = array();
				while($row=mysqli_fetch_row($res))
				{
					array_push($codigos, $row);
				}
				$out['codigos'] = $codigos;
			}break;
			case 'valsubproducto':
			{
				if(isset($_POST['codigo']))
				{
					if(isset($_POST['seccion']))
					{
						if($_POST['seccion']!='')
						{
							$idt="AND LEFT(grupo,".$_POST['nivel'].")='".$_POST['seccion']."'";
						}
						else {$idt='';}
					}
					else {$idt='';}
					$sql="SELECT MAX(version) FROM ccpetbienestransportables";
					$res = mysqli_query($linkbd,$sql);
					$row=mysqli_fetch_row($res);
					$maxversion=$row[0];
					$sql = "SELECT titulo FROM ccpetbienestransportables WHERE concat_ws(' ', grupo, titulo) like '%".$_POST['codigo']."%' AND version='$maxversion' $idt";
					$res = mysqli_query($linkbd,$sql);
					$codigos = array();
					while($row=mysqli_fetch_row($res))
					{
						array_push($codigos, $row);
					}
					$out['codigos'] = $codigos;
				}
			}break;
			case 'valsubclases':
			{
				if(isset($_POST['codigo']))
				{
					if(isset($_POST['seccion']))
					{
						if($_POST['seccion']!='')
						{
							$idt="AND LEFT(grupo,".$_POST['nivel'].")='".$_POST['seccion']."'";
						}
						else {$idt='';}
					}
					else {$idt='';}
					$sql="SELECT MAX(version) FROM ccpetservicios";
					$res = mysqli_query($linkbd,$sql);
					$row=mysqli_fetch_row($res);
					$maxversion=$row[0];
					$sql = "SELECT titulo FROM ccpetservicios WHERE concat_ws(' ', grupo, titulo) like '%".$_POST['codigo']."%' AND version='$maxversion' $idt";
					$res = mysqli_query($linkbd,$sql);
					$codigos = array();
					while($row=mysqli_fetch_row($res))
					{
						array_push($codigos, $row);
					}
					$out['codigos'] = $codigos;
				}
			}break;


			case 'valsubclaseb':
			{
				if(isset($_POST['codigo']))
				{
					if(isset($_POST['seccion']))
					{
						if($_POST['seccion']!='')
						{
							$idt="AND LEFT(grupo,".$_POST['nivel'].")='".$_POST['seccion']."'";
						}
						else {$idt='';}
					}
					else {$idt='';}
					$sql="SELECT MAX(version) FROM ccpetbienestransportables";
					$res = mysqli_query($linkbd,$sql);
					$row=mysqli_fetch_row($res);
					$maxversion=$row[0];
					$sql = "SELECT titulo FROM ccpetbienestransportables WHERE concat_ws(' ', grupo, titulo) like '%".$_POST['codigo']."%' AND version='$maxversion' $idt";
					$res = mysqli_query($linkbd,$sql);
					$codigos = array();
					while($row=mysqli_fetch_row($res))
					{
						array_push($codigos, $row);
					}
					$out['codigos'] = $codigos;
				}
			}break;
			case 'subclaseb':
			{
				$keywordsubclase = $_POST['keywordsubclase'];
				if(isset($_GET['seccion']))
				{
					if($_GET['seccion']!='')
					{
						$idt="AND LEFT(grupo,".$_GET['nivel'].")='".$_GET['seccion']."'";
					}
					else {$idt='';}
				}
				else {$idt='';}
				$sql="SELECT MAX(version) FROM ccpetbienestransportables";
				$res = mysqli_query($linkbd,$sql);
				$row=mysqli_fetch_row($res);
				$maxversion=$row[0];
				$sql = "SELECT grupo, titulo, ciiu, sistema_armonizado, cpc FROM ccpetbienestransportables WHERE concat_ws(' ', grupo, titulo) like '%$keywordsubclase%' AND version='$maxversion' AND LENGTH(grupo) = '5' $idt";
				$res = mysqli_query($linkbd,$sql);
				$subClases = array();
				while($row=mysqli_fetch_row($res))
				{
					array_push($subClases, $row);
				}
				$out['subClases'] = $subClases;
			}break;
			case 'subclases':
			{
				$keywordsubclase = $_POST['keywordsubclase'];
				if(isset($_GET['seccion']))
				{
					if($_GET['seccion']!='')
					{
						$idt="AND LEFT(grupo,".$_GET['nivel'].")='".$_GET['seccion']."'";
					}
					else {$idt='';}
				}
				else {$idt='';}
				$sql="SELECT MAX(version) FROM ccpetservicios";
				$res = mysqli_query($linkbd,$sql);
				$row=mysqli_fetch_row($res);
				$maxversion=$row[0];
				$sql = "SELECT grupo, titulo, ciiu,'', cpc FROM ccpetservicios WHERE concat_ws(' ', grupo, titulo) like '%$keywordsubclase%' AND version='$maxversion' AND LENGTH(grupo) = '5' $idt";
				$res = mysqli_query($linkbd,$sql);
				$subClases = array();
				while($row=mysqli_fetch_row($res))
				{
					array_push($subClases, $row);
				}
				$out['subClases'] = $subClases;
			}break;
			case 'valindicproducto':
			{
				if(isset($_POST['codigo']))
				{
					$sql="SELECT MAX(version) FROM ccpetbienestransportables";
					$res = mysqli_query($linkbd,$sql);
					$row=mysqli_fetch_row($res);
					$maxversion=$row[0];
					$sql = "SELECT indicador_producto,cod_producto,producto FROM ccpetproductos WHERE codigo_indicador = '".$_POST['codigo']."' AND version='$maxversion'";
					$res = mysqli_query($linkbd,$sql);
					$codigos = array();
					while($row=mysqli_fetch_row($res))
					{
						array_push($codigos, $row);
					}
					$out['codigos'] = $codigos;
				}
			}break;
			case 'valprograma':
			{
				$sql="SELECT MAX(version) FROM ccpetprogramas";
				$res = mysqli_query($linkbd,$sql);
				$row=mysqli_fetch_row($res);
				$maxversion=$row[0];
				$sqlr = "SELECT nombre,codigo_subprograma,nombre_subprograma FROM ccpetprogramas WHERE codigo = '".$_GET['programa']."' AND version='$maxversion'";
				$res=mysqli_query($linkbd,$sqlr);
				$programas = array();
				while($row=mysqli_fetch_row($res))
				{
					array_push($programas, $row);
				}
				$out['programas'] = $programas;
			}break;
			case 'valsector':
			{
				$sql = "SELECT MAX(version) FROM ccpetsectores";
				$res = mysqli_query($linkbd,$sql);
				$row=mysqli_fetch_row($res);
				$maxversion=$row[0];
				$sqlr="SELECT nombre FROM ccpetsectores WHERE codigo='".$_GET['sector']."' AND version='$maxversion'";
				$res = mysqli_query($linkbd,$sqlr);
				$sectores = array();
				while($row=mysqli_fetch_row($res))
				{
					array_push($sectores, $row);
				}
				$out['sectores'] = $sectores;
			}break;

		}
	}

	if(isset($_GET['tablas']))
	{
		$ntablas=$_GET['tablas'];
		switch ($ntablas)
		{
			case 'metas':
			{
				$sql="SELECT * FROM ccpet_metapdm ORDER BY id";
				$res=mysqli_query($linkbd,$sql);
				$codigos = array();
				while($row=mysqli_fetch_row($res))
				{
					array_push($codigos, $row);
				}
				$out['codigos'] = $codigos;
			}break;
		}
	}

	header("Content-type: application/json");
	echo json_encode($out);
	die();
?>
