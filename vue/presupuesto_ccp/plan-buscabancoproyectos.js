const URL = 'vue/presupuesto_ccp/plan-buscabancoproyectos.php';
const URL2 = 'vue/presupuesto_ccp/plan-bancoproyectos.php';

var app = new Vue({ 
	el: '#myapp',
	data:
	{
		proyectos:[],
		proyecto:'',
		search: {keyword: ''},
		showMensaje: false,
		showModal: false,
		showMensajeSN: false,
		titulomensaje:'',
		contenidomensaje:'',
		colortitulosmensaje:'',
		numerodel:'',
		vigencia:'',
		years:[],
		opcionmensaje:'',
	},
	mounted: async function(){
		await this.cargayears();
		this.cargabase();
	},
	computed:
	{
		
	},
	methods:{
		cargayears: async function(){
			const formData = new FormData();
			formData.append("buscar", "years");
			try {
				const response = await fetch(URL2,{method:"POST",body:formData});
				const data = await response.json();
				if (Array.isArray(data)) {
					this.years = data.map(year => [year]);
					this.vigencia = data[0];
				} else {
					console.error("Error cargando años");
				}
			} catch (error) {
				console.error("Error en la petición:", error);
			}
		},
		cargabase: async function()
		{
			await axios.post('vue/presupuesto_ccp/plan-buscabancoproyectos.php?visualizar=cabecera&vigencia='+this.vigencia)
			.then(
				(response)=>
				{
					this.proyectos = response.data.codigos;
				}
			);
		},
		toggleMensajeSN:function(preg,resp)
		{
			this.showMensajeSN = !this.showMensajeSN;
			if(this.showMensajeSN==false)
			{
				switch (preg)
				{
					case '1': 
						if(resp=='S'){this.eliminaproyectos(this.numerodel)}
						this.numerodel=''
						break;
				}
			}
		},
		toggleMensaje: function()
		{
			this.showMensaje = !this.showMensaje;
			if(this.opcionmensaje!='' && this.showMensaje== false)
			{
				switch (this.opcionmensaje)
				{
					case '1': 
						this.opcionmensaje='';
						this.$refs.codigo.focus();
						break;
					case'2':
						this.opcionmensaje='';
						this.$refs.nombre.focus();
						break;
					case'3':
						this.opcionmensaje='';
						this.$refs.descripcion.focus();
						break;
					case '4':
						this.opcionmensaje='';
						this.$refs.valorrubro.focus();
						break;
					case '5':
						this.opcionmensaje='';
						this.$refs.fuentef.focus();
						break;
				}
			}
		},
		preguntardel: async function(iddel,coddel,activo)
		{
			var formData = new FormData();

			formData.append("bpim", coddel);
			formData.append("vigencia", this.vigencia);
		
			await axios.post(URL + '?action=existePresupuestoProyecto', formData)
			.then((response) => {
				if(response.data.existe){
					this.toggleMensaje();
					this.colortitulosmensaje='crimson';
					this.titulomensaje='Mensaje de Error';
					this.contenidomensaje='No se puede eliminar detalle, debido a que se utilizó este proyecto en un proceso presupuestal.';
				}else{
				
					this.toggleMensajeSN();
					this.colortitulosmensaje='darkgreen';
					this.titulomensaje='Aprobar Eliminar';
					this.contenidomensaje='Desea Eliminar el proyecto No'+coddel;
					this.numerodel=iddel;
				}
				
			});

			
		},
		eliminaproyectos: async function(idproy)
		{
			await axios.post('vue/presupuesto_ccp/plan-buscabancoproyectos.php?elimina='+idproy)
			.then();
			this.cargabase();
		},
		editarsector: function(idproy)
		{
			location.href="plan-bancoproyectoseditar.php?idr="+idproy;
		},
		searchProyecto: async function()
		{
			var keyword = this.toFormData(this.search);
			await axios.post('vue/presupuesto_ccp/plan-buscabancoproyectos.php?action=searchProyecto', keyword)
			.then(
				(response)=>
				{
					this.proyectos = response.data.codigos;
					if(response.data.codigos == ''){this.noMember = true;}
					else {this.noMember = false;}
				}
			);
		},
		toFormData: function(obj)
		{
			var form_data = new FormData();
			for(var key in obj)
			{
				form_data.append(key, obj[key]);
			}
			return form_data;
		},
	},
});