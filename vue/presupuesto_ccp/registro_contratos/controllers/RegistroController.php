<?php
    require_once '../../../../Librerias/core/Helpers.php';
    require_once '../../../../PHPExcel/Classes/PHPExcel.php';
    require_once '../models/RegistroModel.php';
    session_start();

    class RegistroController extends RegistroModel{
        public function getData(){
            if(!empty($_SESSION)){
                $arrResponse = [
                    "terceros"=>getTerceros(),
                    "cdps"=>$this->selectCdps(),
                ];
                echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
            }
        }
        public function save(){
            if(!empty($_SESSION)){
                $arrData = json_decode($_POST['data'],true);
                $arrCdp = json_decode($_POST['cdp'],true);
                if(empty($arrData) || empty($arrCdp) || empty($_POST['contrato']) || empty($_POST['tercero'])){
                    $arrResponse = array("status"=>false,"msg"=>"Error de datos");
                }else{  
                    $strContrato = strtoupper(replaceChar(strClean($_POST['contrato'])));
                    $strAnexos = replaceChar(strClean($_POST['anexos']));
                    $strFecha = strClean($_POST['fecha']);
                    $strTercero = strClean($_POST['tercero']);
                    $strGarantia = strclean($_POST['garantia']);
                    $intConsecutivo = intval($_POST['consecutivo']);
                    $opcion="";
                    if($intConsecutivo == 0){
                        $opcion = 1;
                        $request = $this->insertData($strFecha,$strContrato,$strAnexos,$strTercero,$strGarantia,$arrCdp);
                        $id = $request;
                    }else{
                        $opcion = 2;
                        $id = $intConsecutivo;
                        $request = $this->updateData($intConsecutivo,$strFecha,$strContrato,$strAnexos,$strTercero,$strGarantia,$arrCdp);
                    }
                    if($request > 0){
                        $this->insertDet($id,$arrData);
                        if($opcion == 1){
                            insertAuditoria("ccpet_auditoria","ccpet_funciones_id",2,"Crear",$request,"Registro de contrato","ccpet_funciones");
                            $arrResponse = array("status"=>true,"msg"=>"Datos guardados correctamente.","id"=>$request);
                        }else{
                            insertAuditoria("ccpet_auditoria","ccpet_funciones_id",2,"Editar",$intConsecutivo,"Registro de contrato","ccpet_funciones");
                            $arrResponse = array("status"=>true,"msg"=>"Datos actualizados correctamente.");
                        }
                    }else if($request =="existe"){
                        $arrResponse = array("status"=>false,"msg"=>"La disponibilidad ya pertenece a un registro de contrato, intente con otro o anule el registro que lo usa.");
                    }else{
                        $arrResponse = array("status"=>false,"msg"=>"No se ha podido guardar, inténte de nuevo.");
                    }
                }
                echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
            }
            die();
        }
        public function getEdit(){
            if(!empty($_SESSION)){
                $intId = intval($_POST['codigo']);
                $request = $this->selectEdit($intId);
                if(!empty($request)){
                    $arrResponse = array(
                        "data" => $request,
                        "status"=>true,
                        "terceros"=>getTerceros(),
                        "cdps"=>$this->selectCdps(),
                        "consecutivos"=>getConsecutivos("ccp_registro_contrato","id"));
                }else{
                    $arrResponse = array("status"=>false,"consecutivo"=>searchConsec("ccp_registro_contrato ","id")-1);
                }
                echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
            }
            die();
        }
        public function getSearch(){
            if(!empty($_SESSION)){
                $strFechaInicial = strClean($_POST['fecha_inicial']);
                $strFechaFinal = strClean($_POST['fecha_final']);
                $strCodigo = strClean($_POST['codigo']);
                $strTercero = strClean($_POST['documento']);
                $intConsecutivo = strClean($_POST['consecutivo']);
                $intCdp = strClean($_POST['cdp']);
                $request = $this->selectSearch($strFechaInicial,$strFechaFinal,$strCodigo,$strTercero,$intConsecutivo,$intCdp);
                echo json_encode($request,JSON_UNESCAPED_UNICODE);
            }
            die();
        }
        public function changeData(){
            if(!empty($_SESSION)){
                $id = intval($_POST['id']);
                $request = $this->updateStatus($id);
                if($request == 1){
                    insertAuditoria("ccpet_auditoria","ccpet_funciones_id",2,"Anular",$id,"Registro de contrato","ccpet_funciones");
                    $arrResponse = array("status"=>true);
                }else{
                    $arrResponse = array("status"=>false,"msg"=>"Error");
                }
                echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
            }
            die();
        }
    }
    if($_POST){
        $obj = new RegistroController();
        if($_POST['action'] == "save"){
            $obj->save();
        }else if($_POST['action']=="edit"){
            $obj->getEdit();
        }else if($_POST['action']=="search"){
            $obj->getSearch();
        }else if($_POST['action']=="change"){
            $obj->changeData();
        }else if($_POST['action']=="get"){
            $obj->getData();
        }
    }

?>
