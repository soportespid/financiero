<?php
    require_once '../../../../Librerias/core/Helpers.php';
    require_once '../../../../tcpdf/tcpdf.php';
    require_once '../../../../Librerias/core/Mysql.php';
    session_start();
    /*ini_set('display_errors', '1');
    ini_set('display_startup_errors', '1');
    error_reporting(E_ALL);*/
    class RegistroExportController {
        public function exportPdf(){
            if(!empty($_SESSION)){
                $con = new Mysql();
                $data = json_decode($_POST['data'],true);
                if(!empty($data)){
                    define("ESTADO",$data['estado']);
                    $strContrato = $data['contrato'];
                    $strDocumentoTercero = $data['tercero_info']['codigo'];
                    $strNombreTercero = $data['tercero_info']['nombre'];
                    $strFechaSolicitud = $data['fecha_solicitud'];
                    $strFechaVencimiento = $data['fecha_vencimiento'];
                    $strAnexos = $data['anexo'];
                    $strFechaActo = $data['fecha_acto'];
                    $strFechaContrato = $data['fecha_formato'];
                    $strNombreArea = strtoupper($data['nombrearea']);
                    $strModalidad = $data['nombre_modalidad'];
                    $strSector = $data['nombre_sector'];
                    $strObjeto = $data['objeto'];
                    $intIdArea = $data['id_area_solicitante'];
                    $intIdSolicitud = $data['id_solicitud_cdp'];
                    $intTipoDocumento = $data['tipo_documento'];
                    $intId = $data['consecutivo'];
                    $intTipoContrato = $data['tipo_contrato'];
                    $intNumeroActo = $data['numero_acto'];
                    $intTipoPtto = $data['tipo_gasto'];
                    $intCodPaa = $data['cod_plan'];
                    $intCodSector = $data['cod_sector'];
                    $sql_cab = "SELECT codigo, valor, forma_pago FROM ccp_registro_contrato WHERE id = $intId";
                    $cab = $con->select($sql_cab);
                    $intValor = formatNum($cab['valor']);
                    $formaPago = $cab['forma_pago'];

                    $cedula = $_SESSION["cedulausu"];
                    $vigencia = getVigenciaUsuario($cedula);

                    $sql_cuentas_fun = "SELECT cuenta, cpc, fuente, valor FROM plan_solicitud_cdp_det WHERE id_solicitud_cdp = $intIdSolicitud AND indicador = ''";
                    $cuentasFun = $con->select_all($sql_cuentas_fun);

                    $sql_cuentas_inv = "SELECT s.indicador AS codIndicador, b.codigo AS bpin, s.fuente AS codFuente, s.cuenta, s.valor FROM plan_solicitud_cdp_det AS s LEFT JOIN ccpproyectospresupuesto AS b ON s.id_bpin = b.id WHERE s.id_solicitud_cdp = $intIdSolicitud AND s.indicador != ''";
                    $cuentasInv = $con->select_all($sql_cuentas_inv);

                    $sql_items = "SELECT descripcion, unidad, valor_unitario AS valor, cantidad, valor_total AS subtotal FROM ccp_registro_contrato_det WHERE codigo = $cab[codigo]";
                    $arrDetalle = $con->select_all($sql_items);
                    $arrDetalleSolicitud = $data['detalle_solicitud'];
                    $arrContratos=[1=>"OBRA",2=>"CONSULTORA DE SERVICIO",3=>"SUMINISTRO Y/O COMPRAVENTA",4=>"PRESTACIÓN DE SERVICIOS",""=>"OTRO"];

                    $sqlDependencia = "SELECT d.nombre AS nombre_dependencia 
                    FROM plan_solicitud_cdp_det AS s 
                    INNER JOIN pptoseccion_presupuestal AS d ON s.dependencia = d.id_seccion_presupuestal 
                    WHERE s.id_solicitud_cdp = '$intIdSolicitud' LIMIT 1";
                    $strDependencia = $con->select($sqlDependencia)['nombre_dependencia'];

                    $sql_cargo = "SELECT codcargo, nombrecargo FROM planaccargos WHERE codpadre = 1 AND dependencia = $intIdArea";
                    $row_cargo = $con->select($sql_cargo);
                    $cargo = $row_cargo["nombrecargo"] != "" ? $row_cargo["nombrecargo"] : "";
                    $cargo = strtoupper($cargo);
                    $codCargo = $row_cargo["codcargo"] != "" ? $row_cargo["codcargo"] : "";

                    $sql_documento = "SELECT cedulanit FROM planestructura_terceros WHERE codcargo = '$codCargo' AND estado = 'S'";
                    $row_documento = $con->select($sql_documento);
                    $documento = $row_documento["cedulanit"] != "" ? $row_documento["cedulanit"] : "";

                    $sql_nombre = "SELECT CONCAT(nombre1, ' ', nombre2, ' ', apellido1, ' ', apellido2) AS nombre FROM terceros WHERE cedulanit = '$documento'";
                    $row_nombre = $con->select($sql_nombre);
                    $nombre = preg_replace("/\s+/", " ", trim($row_nombre["nombre"]));
                    $nombre = strtoupper($nombre);
                    define("NOMBRE",$nombre);
                    define("CARGO",$cargo);
                    $pdf = new MYPDF('P','mm','Letter', true, 'iso-8859-1', false);
                    $pdf->SetDocInfoUnicode (true);
                    // set document information
                    $pdf->SetCreator(PDF_CREATOR);
                    $pdf->SetAuthor('IDEALSAS');
                    $pdf->SetTitle('REGISTRO DE CONTRATO');
                    $pdf->SetSubject('REGISTRO DE CONTRATO');
                    $pdf->SetKeywords('TCPDF, PDF, example, test, guide');
                    $pdf->SetMargins(10, 38, 10);// set margins
                    $pdf->SetHeaderMargin(38);// set margins
                    $pdf->SetFooterMargin(17);// set margins
                    $pdf->SetAutoPageBreak(TRUE, 20);// set auto page breaks
                    // set some language-dependent strings (optional)
                    if (@file_exists(dirname(__FILE__).'/lang/spa.php'))
                    {
                        require_once(dirname(__FILE__).'/lang/spa.php');
                        $pdf->setLanguageArray($l);
                    }
                    $pdf->SetFillColor(255,255,255);
                    $pdf->AddPage();
                    $pdf->SetFont('helvetica','B',9);
                    $pdf->SetFillColor(153,221,255);
                    $pdf->MultiCell(190,10,"LA SECRETARIA","LRBT",'C',true,0,'','',true,0,false,true,0,'M',true);
                    $pdf->ln();
                    $pdf->SetFillColor(255,255,255);
                    $pdf->SetTextColor(0,0,0);
                    $pdf->SetFont('Helvetica','',8);
                    $pdf->MultiCell(20,5,"CONSECUTIVO:","LRBT",'L',true,0,'','',true,0,false,true,0,'M',true);
                    $pdf->MultiCell(15,5,$cab["codigo"],"LRBT",'C',true,0,'','',true,0,false,true,0,'M',true);
                    $pdf->MultiCell(25,5,"CONSECUTIVO CDP:","LRBT",'L',true,0,'','',true,0,false,true,0,'M',true);
                    $pdf->MultiCell(25,5,$intIdSolicitud,"LRBT",'C',true,0,'','',true,0,false,true,0,'M',true);
                    $pdf->MultiCell(25,5,"FECHA SOLICITUD:","LRBT",'L',true,0,'','',true,0,false,true,0,'M',true);
                    $pdf->MultiCell(25,5,$strFechaContrato,"LRBT",'C',true,0,'','',true,0,false,true,0,'M',true);
                    $pdf->MultiCell(20,5,"VALOR:","LRBT",'L',true,0,'','',true,0,false,true,0,'M',true);
                    $pdf->MultiCell(35,5,$intValor,"LRBT",'R',true,0,'','',true,0,false,true,0,'M',true);
                    $pdf->ln();
                    $pdf->SetFont('helvetica','B',9);
                    $pdf->SetFillColor(153,221,255);
                    $pdf->MultiCell(190,5,"INFORMACIÓN GENERAL","LRBT",'C',true,0,'','',true,0,false,true,0,'M',true);
                    $pdf->ln();
                    $pdf->SetFont('Helvetica','',8);
                    $pdf->SetFillColor(255,255,255);
                    $pdf->MultiCell(35,5,"PLAN DE COMPRAS:","LRBT",'L',true,0,'','',true,0,false,true,0,'M',true);
                    $pdf->MultiCell(25,5,$intCodPaa,"LRBT",'C',true,0,'','',true,0,false,true,0,'M',true);
                    $pdf->MultiCell(50,5,"MODALIDAD DE CONTRATACIÓN:","LRBT",'L',true,0,'','',true,0,false,true,0,'M',true);
                    $pdf->MultiCell(80,5,$strModalidad,"LRBT",'L',true,0,'','',true,0,false,true,0,'M',true);
                    $pdf->ln();
                    $pdf->MultiCell(50,5,"TIPO DE PRESUPUESTO ASIGNADO:","LRBT",'L',true,0,'','',true,0,false,true,0,'M',true);
                    $pdf->MultiCell(25,5,"FUNCIONAMIENTO","LBT",'C',true,0,'','',true,0,false,true,0,'M',true);
                    $pdf->MultiCell(21,5,$intTipoPtto == 1 ? "X" : "","RBT",'C',true,0,'','',true,0,false,true,0,'M',true);
                    $pdf->MultiCell(25,5,"INVERSION","LBT",'C',true,0,'','',true,0,false,true,0,'M',true);
                    $pdf->MultiCell(22,5,$intTipoPtto == 2 ? "X" : "","RBT",'C',true,0,'','',true,0,false,true,0,'M',true);
                    $pdf->MultiCell(25,5,"FUN/INV","LBT",'C',true,0,'','',true,0,false,true,0,'M',true);
                    $pdf->MultiCell(22,5,$intTipoPtto == "" ? "X" : "","RBT",'C',true,0,'','',true,0,false,true,0,'M',true);
                    if ($tipoPresupuesto == 1) {
                        $pdf->ln();
                        $version = getVersionGastosCcpet();
                        $sqlTipoGasto = "SELECT nombre FROM cuentasccpet WHERE codigo = '$intTipoPtto' AND nivel = 3 AND version = $version";
                        $nombreGasto = strtoupper($con->select($sqlTipoGasto)['nombre']);    
                        $pdf->MultiCell(50,5,"TIPO DE GASTO:","LRBT",'L',true,0,'','',true,0,false,true,0,'M',true);
                        $pdf->MultiCell(140,5,$nombreGasto,"LRBT",'L',true,0,'','',true,0,false,true,0,'M',true);
                    } else if ($tipoPresupuesto == 2) {
                        $pdf->ln();
                        $pdf->MultiCell(40,5,"CODIGO SECTOR:","LRBT",'L',true,0,'','',true,0,false,true,0,'M',true);
                        $pdf->MultiCell(20,5,$strSector,"LRBT",'C',true,0,'','',true,0,false,true,0,'M',true);
                        $pdf->MultiCell(40,5,"NOMBRE SECTOR:","LRBT",'L',true,0,'','',true,0,false,true,0,'M',true);
                        $pdf->MultiCell(90,5,$intCodSector,"LRBT",'L',true,0,'','',true,0,false,true,0,'M',true);
                    }
                    $pdf->ln();
                    $pdf->MultiCell(35,5,"TIPO DE DOCUMENTO:","LRBT",'L',true,0,'','',true,0,false,true,0,'M',true);
                    $pdf->MultiCell(35,5,"TIPO DE CONTRATO","LBT",'C',true,0,'','',true,0,false,true,0,'M',true);
                    $pdf->MultiCell(15,5,$intTipoDocumento == 1 ? "X" : "","RBT",'C',true,0,'','',true,0,false,true,0,'M',true);
                    $pdf->MultiCell(35,5,"ACTO ADMINISTRATIVO","LBT",'C',true,0,'','',true,0,false,true,0,'M',true);
                    $pdf->MultiCell(20,5,$intTipoDocumento != 1 ? "X" : "","RBT",'C',true,0,'','',true,0,false,true,0,'M',true);
                    $pdf->MultiCell(25,5,"CONTRATO","LBT",'C',true,0,'','',true,0,false,true,0,'M',true);
                    $pdf->MultiCell(25,5,$strContrato,"RBT",'C',true,0,'','',true,0,false,true,0,'M',true);
                    $pdf->ln();
                    if ($tipoDocumento == 1) {
                        $pdf->MultiCell(50,5,"TIPO DE CONTRATO:","LRBT",'L',true,0,'','',true,0,false,true,0,'M',true);
                        $pdf->MultiCell(140,5,$arrContratos[$intTipoContrato],"LRBT",'L',true,0,'','',true,0,false,true,0,'M',true);
                    } else {
                        $pdf->MultiCell(47.5,5,"NÚMERO DE ACTO:","LRBT",'L',true,0,'','',true,0,false,true,0,'M',true);
                        $pdf->MultiCell(47.5,5,$intNumeroActo,"LRBT",'C',true,0,'','',true,0,false,true,0,'M',true);
                        $pdf->MultiCell(47.5,5,"FECHA DE ACTO:","LRBT",'L',true,0,'','',true,0,false,true,0,'M',true);
                        $pdf->MultiCell(47.5,5,$strFechaActo,"LRBT",'C',true,0,'','',true,0,false,true,0,'M',true);
                    }
                    $pdf->ln();
                    $pdf->SetFont('helvetica','B',8);
                    $pdf->SetFillColor(153,221,255);
                    $pdf->MultiCell(190,5,"DATOS BENEFICIARIO","LRBT",'C',true,0,'','',true,0,false,true,0,'M',true);
                    $pdf->SetFont('helvetica','',8);
                    $pdf->SetFillColor(245,245,245);
                    $pdf->ln();
                    $pdf->MultiCell(30,5,"DOCUMENTO:","LRBT",'L',true,0,'','',true,0,false,true,0,'M',true);
                    $pdf->MultiCell(30,5,$strDocumentoTercero,"LRBT",'C',true,0,'','',true,0,false,true,0,'M',true);
                    $pdf->MultiCell(30,5,"BENEFICIARIO:","LRBT",'L',true,0,'','',true,0,false,true,0,'M',true);
                    $pdf->MultiCell(100,5,$strNombreTercero,"LRBT",'L',true,0,'','',true,0,false,true,0,'M',true);
                    $pdf->ln();
                    $pdf->MultiCell(30,12,"OBJETO:","LRBT",'LRBT',true,0,'','',true,0,false,true,0,'M',true);
                    $pdf->MultiCell(160,12,$strObjeto,"LRBT",'L',true,0,'','',true,0,false,true,0,'M',true);
                    $pdf->ln();
                    $pdf->MultiCell(30,16,"FORMA DE PAGO:","LRBT",'LRBT',true,0,'','',true,0,false,true,0,'M',true);
                    $pdf->MultiCell(160,16,"$formaPago","LRBT",'L',true,0,'','',true,0,false,true,0,'M',true);
                    $pdf->ln();
                    $pdf->SetFont('helvetica','B',8);
                    $pdf->SetFillColor(153,221,255);
                    $pdf->MultiCell(190,5,"ALCANCE OBJETO CONTRACTUAL","LRBT",'C',true,0,'','',true,0,false,true,0,'M',true);
                    $pdf->ln();
                
                    $pdf->MultiCell(78,10,"Descripción","LRBT",'C',true,0,'','',true,0,false,true,0,'M',true);
                    $pdf->MultiCell(18,10,"Unidad de medida","LRBT",'C',true,0,'','',true,0,false,true,0,'M',true);
                    $pdf->MultiCell(38,10,"Valor unitario","LRBT",'C',true,0,'','',true,0,false,true,0,'M',true);
                    $pdf->MultiCell(18,10,"Cantidad","LRBT",'C',true,0,'','',true,0,false,true,0,'M',true);
                    $pdf->MultiCell(38,10,"Subtotal","LRBT",'C',true,0,'','',true,0,false,true,0,'M',true);
                    $pdf->ln();
                    $pdf->SetFont('helvetica','',8);
                    $pdf->SetFillColor(255,255,255);
                    foreach ($arrDetalle as $det) {
                        $height = $pdf->getNumLines($det['descripcion'])*5;
                        $pdf->MultiCell(78,$height,$det['descripcion'],"LRBT",'',true,0,'','',true,0,false,true,0,'M',true);
                        $pdf->MultiCell(18,$height,$det['unidad'],"LRBT",'C',true,0,'','',true,0,false,true,0,'M',true);
                        $pdf->MultiCell(38,$height,formatNum($det['valor']),"LRBT",'R',true,0,'','',true,0,false,true,0,'M',true);
                        $pdf->MultiCell(18,$height,$det['cantidad'],"LRBT",'C',true,0,'','',true,0,false,true,0,'M',true);
                        $pdf->MultiCell(38,$height,formatNum($det['subtotal']),"LRBT",'R',true,0,'','',true,0,false,true,0,'M',true);
                        $pdf->ln();
                    }

                    $pdf->SetFont('helvetica','B',9);
                    $pdf->SetFillColor(153,221,255);

                    if (!empty($cuentasFun)) {
                        $sql_version_cuentas = "SELECT MAX(version) AS version FROM cuentasccpet";
                        $row_version_cuentas = $con->select($sql_version_cuentas);
                        $versionCuentas = $row_version_cuentas["version"];
                
                        $pdf->SetFont('helvetica','B',8);
                        $pdf->SetFillColor(153,221,255);
                        $pdf->MultiCell(190,5,"DESCRIMINACIÓN PRESUPUESTAL","LRBT",'C',true,0,'','',true,0,false,true,0,'M',true);
                        $pdf->ln();
                        
                        $pdf->MultiCell(28,10,"Tipo presupuesto","LRBT",'C',true,0,'','',true,0,false,true,0,'M',true);
                        $pdf->MultiCell(28,10,"Tipo gasto","LRBT",'C',true,0,'','',true,0,false,true,0,'M',true);
                        $pdf->MultiCell(28,10,"División gasto","LRBT",'C',true,0,'','',true,0,false,true,0,'M',true);
                        $pdf->MultiCell(28,10,"Subdivision gasto","LRBT",'C',true,0,'','',true,0,false,true,0,'M',true);
                        $pdf->MultiCell(32,10,"CCPET","LRBT",'C',true,0,'','',true,0,false,true,0,'M',true);
                        $pdf->MultiCell(18,10,"CPC","LRBT",'C',true,0,'','',true,0,false,true,0,'M',true);
                        $pdf->MultiCell(28,10,"Fuente","LRBT",'C',true,0,'','',true,0,false,true,0,'M',true);
                        $pdf->ln();
                        $pdf->SetFillColor(245,245,245);
                        $pdf->SetFont('helvetica','',8);
                        $fill = true;
                
                        function search_name_cuenta($version, $cuenta){
                            $con = new Mysql();
                            $sql = "SELECT nombre FROM cuentasccpet WHERE codigo = '$cuenta' AND version = $version";
                            $row = $con->select($sql);
                            return $row["nombre"];
                        }
                
                        foreach ($cuentasFun as $fun) {
                            $codTipoPresu = substr($fun["cuenta"], 0, 3);
                            $nomTipoPresu = search_name_cuenta($versionCuentas, $codTipoPresu);
                            $codTipoGasto = substr($fun["cuenta"], 0, 5);
                            $nomTipoGasto = search_name_cuenta($versionCuentas, $codTipoGasto);
                            $codDivisionGasto = substr($fun["cuenta"], 0, 8);
                            $nameDivisionGasto = search_name_cuenta($versionCuentas, $codDivisionGasto);
                            $codSubdivisionGasto = substr($fun["cuenta"], 0, 11);
                            $nameSubdivisionGasto = search_name_cuenta($versionCuentas, $codSubdivisionGasto);
                            $nameCuenta = search_name_cuenta($versionCuentas, $fun["cuenta"]);
                            $sql_fuentes = "SELECT nombre FROM ccpet_fuentes_cuipo WHERE codigo_fuente = '$fun[fuente]'";
                            $row_fuentes = $con->select($sql_fuentes);
                            $nomFuente = $row_fuentes["nombre"];
                
                            $pdf->SetFont('helvetica','',8);
                            $pdf->SetFillColor(255,255,255);
                            $pdf->MultiCell(28,18,"$codTipoPresu - $nomTipoPresu","LRBT",'J',true,0,'','',true,0,false,true,0,'M',true);
                            $pdf->MultiCell(28,18,"$codTipoGasto - $nomTipoGasto","LRBT",'J',true,0,'','',true,0,false,true,0,'M',true);
                            $pdf->MultiCell(28,18,"$codDivisionGasto - $nameDivisionGasto","LRBT",'J',true,0,'','',true,0,false,true,0,'M',true);
                            $pdf->MultiCell(28,18,"$codSubdivisionGasto - $nameSubdivisionGasto","LRBT",'J',true,0,'','',true,0,false,true,0,'M',true);
                            $pdf->MultiCell(32,18,"$fun[cuenta] - $nameCuenta","LRBT",'J',true,0,'','',true,0,false,true,0,'M',true);
                            $pdf->MultiCell(18,18,"$fun[cpc]","LRBT",'J',true,0,'','',true,0,false,true,0,'M',true);
                            $pdf->MultiCell(28,18,"$fun[fuente] - $nomFuente","LRBT",'J',true,0,'','',true,0,false,true,0,'M',true);
                            $pdf->ln();
                            $fill = !$fill;
                
                            $getY = $pdf->getY();
                            if ($getY > 190) {
                                $pdf->AddPage();
                            }
                        }
                    }
                    
                    if (!empty($cuentasInv)) {
                        $pdf->SetFont('helvetica','B',8);
                        $pdf->SetFillColor(153,221,255);
                        $pdf->MultiCell(190,5,"DESCRIMINACIÓN PRESUPUESTAL","LRBT",'C',true,0,'','',true,0,false,true,0,'M',true);
                        $pdf->ln();
                        
                        $pdf->MultiCell(22,10,"Sector","LRBT",'C',true,0,'','',true,0,false,true,0,'M',true);
                        $pdf->MultiCell(24,10,"Programa","LRBT",'C',true,0,'','',true,0,false,true,0,'M',true);
                        $pdf->MultiCell(24,10,"SubPrograma","LRBT",'C',true,0,'','',true,0,false,true,0,'M',true);
                        $pdf->MultiCell(24,10,"Producto","LRBT",'C',true,0,'','',true,0,false,true,0,'M',true);
                        $pdf->MultiCell(24,10,"Indicador","LRBT",'C',true,0,'','',true,0,false,true,0,'M',true);
                        $pdf->MultiCell(28,10,"BPIN","LRBT",'C',true,0,'','',true,0,false,true,0,'M',true);
                        $pdf->MultiCell(24,10,"Fuente","LRBT",'C',true,0,'','',true,0,false,true,0,'M',true);
                        $pdf->MultiCell(20,10,"CCPET","LRBT",'C',true,0,'','',true,0,false,true,0,'M',true);
                        $pdf->ln();
                        $pdf->SetFillColor(245,245,245);
                        $pdf->SetFont('helvetica','',8);
                        $fill = true;
                
                        foreach ($cuentasInv as $inv) {
                            $codSector = $nombreSector = "";
                            $codIndicador = $inv["codIndicador"];
                            $bpin = $inv["bpin"];
                            $codSector = substr($codIndicador, 0, 2);
                            $sql_sector = "SELECT nombre FROM ccpetsectores WHERE codigo = '$codSector'";
                            $row_sector = $con->select($sql_sector);
                            $nomSector = $row_sector["nombre"];
                            $codPrograma = substr($codIndicador, 0, 4);
                            $sql_programa = "SELECT nombre, codigo_subprograma, nombre_subprograma FROM ccpetprogramas WHERE codigo LIKE '$codPrograma'";
                            $row_programa = $con->select($sql_programa);
                            $nomPrograma = $row_programa["nombre"];
                            $codSubPrograma = $codPrograma.$row_programa["codigo_subprograma"];
                            $nomSubPrograma = $row_programa["nombre_subprograma"];
                            $codProducto = substr($codIndicador, 0, 7);
                            $sql_producto = "SELECT DISTINCT producto AS nombre FROM ccpetproductos WHERE cod_producto LIKE '$codProducto'";
                            $row_producto = $con->select($sql_producto);
                            $nomProducto = $row_producto["nombre"];
                            $sql_indicador = "SELECT indicador_producto AS nombre FROM ccpetproductos WHERE codigo_indicador LIKE '$codIndicador'";
                            $row_indicador = $con->select($sql_indicador);
                            $nomIndicador = $row_indicador["nombre"];
                            $sql_bpin = "SELECT nombre FROM ccpproyectospresupuesto WHERE codigo = '$bpin' AND vigencia = '$vigencia'";
                            $row_bpin = $con->select($sql_bpin);
                            $nomBpin = $row_bpin["nombre"];
                            
                            $sql_fuentes = "SELECT nombre FROM ccpet_fuentes_cuipo WHERE codigo_fuente = '$inv[codFuente]'";
                            $row_fuentes = $con->select($sql_fuentes);
                            $nomFuente = $row_fuentes["nombre"];
                            $pdf->SetFont('helvetica','',8);
                            $pdf->SetFillColor(255,255,255);
                            $pdf->MultiCell(22,18,"$codSector - $nomSector","LRBT",'J',true,0,'','',true,0,false,true,0,'M',true);
                            $pdf->MultiCell(24,18,"$codPrograma - $nomPrograma","LRBT",'J',true,0,'','',true,0,false,true,0,'M',true);
                            $pdf->MultiCell(24,18,"$codSubPrograma - $nomSubPrograma","LRBT",'J',true,0,'','',true,0,false,true,0,'M',true);
                            $pdf->MultiCell(24,18,"$codProducto - $nomProducto","LRBT",'J',true,0,'','',true,0,false,true,0,'M',true);
                            $pdf->MultiCell(24,18,"$inv[codIndicador] - $nomIndicador","LRBT",'J',true,0,'','',true,0,false,true,0,'M',true);
                            $pdf->MultiCell(28,18,"$inv[bpin] - $nomBpin","LRBT",'J',true,0,'','',true,0,false,true,0,'M',true);
                            $pdf->MultiCell(24,18,"$inv[codFuente] - $nomFuente","LRBT",'J',true,0,'','',true,0,false,true,0,'M',true);
                            $pdf->SetFont('helvetica','',6);
                            $pdf->MultiCell(20,18,$inv["cuenta"],"LRBT",'C',true,0,'','',true,0,false,true,0,'M',true);           
                            
                            $pdf->ln();
                            $fill = !$fill;
                
                            $getY = $pdf->getY();
                            if ($getY > 190) {
                                $pdf->AddPage();
                            }
                        }
                    }
                    $pdf->ln();
                    $pdf->SetFont('helvetica','B',8);
                    $pdf->SetFillColor(153,221,255);
                    $pdf->MultiCell(190,6,"CUMPLIMIENTO DE LOS REQUISITOS ESTABLEDICOS","LRBT",'C',true,0,'','',true,0,false,true,0,'M',true);
                    $pdf->ln();
                    $pdf->SetFillColor(245,245,245);
                    $pdf->MultiCell(190,6,"SE CERTIFICA QUE CUMPLE CON LOS REQUISITOS PRECONTRACTUALES Y CONTRACTUALES EXIGIDOS PARA ESTE REGISTRO","LRBT",'L',true,0,'','',true,0,false,true,0,'M',true);
                    // $pdf->SetFont('Helvetica','',8);
                    // $pdf->SetFillColor(255,255,255);
                    // $pdf->ln();
                    // $pdf->MultiCell(30,5,"SECCION:","LRBT",'L',true,0,'','',true,0,false,true,0,'M',true);
                    // $pdf->MultiCell(33,5,"ADMINISTRACIÓN CENTRAL","LRBT",'C',true,0,'','',true,0,false,true,0,'M',true);
                    // $pdf->MultiCell(30,5,"COMPONENTE SECCIÓN:","LRBT",'L',true,0,'','',true,0,false,true,0,'M',true);
                    // $pdf->MultiCell(33,5,$strDependencia,"LRBT",'C',true,0,'','',true,0,false,true,0,'M',true);
                    // $pdf->MultiCell(31,5,"DEPENDENCIA:","LRBT",'L',true,0,'','',true,0,false,true,0,'M',true);
                    // $pdf->MultiCell(33,5,$strNombreArea,"LRBT",'C',true,0,'','',true,0,false,true,0,'M',true);
                    // $pdf->ln();
                    // $pdf->ln();
                    // foreach ($arrDetalleSolicitud as $d) {
                    //     if ($d["codIndicador"] == "") {
                    //         $armadoNumero = "$d[cuenta]-$d[cpc]-$d[codFuente]";
                    //     } else  if ($d["codIndicador"] != ""){
                    //         $codIndicador = $d["codIndicador"];
                    //         $sector = substr($codIndicador, 0, 2);
                    //         $codPrograma = substr($codIndicador, 0, 4);
                    //         $programa = substr($codIndicador, 2, 2);
                    //         $sql_programa = "SELECT codigo_subprograma FROM ccpetprogramas WHERE codigo LIKE '$codPrograma'";
                    //         $subPrograma = $con->select($sql_programa)['codigo_subprograma'];
                    //         $producto = substr($codIndicador, 4, 3);
                    //         $indicador = substr($codIndicador, 7, 2);
                    //         $armadoNumero = "$sector.$programa.$subPrograma.$producto.$indicador-$d[bpin]-$d[codFuente]-$d[cuenta]";
                    //     }
                        
                    //     $pdf->MultiCell(30,5,"CODIGO PRESUPUESTAL:","LRBT",'L',true,0,'','',true,0,false,true,0,'M',true);
                    //     $pdf->MultiCell(90,5,$armadoNumero,"LRBT",'L',true,0,'','',true,0,false,true,0,'M',true);
                    //     $pdf->MultiCell(30,5,"VALOR:","LBT",'C',true,0,'','',true,0,false,true,0,'M',true);
                    //     $pdf->MultiCell(40,5,formatNum($d["valor"], 2),"RBT",'C',true,0,'','',true,0,false,true,0,'M',true);
                    //     $pdf->ln();
                    // }
                    // $pdf->ln();
                   
                    //Campo para recibido y sello

                    $strFile = 'registro_contrato_'.$strContrato.'_'.$intId;
                    $pdf->Output($strFile.'.pdf', 'I');
                }
            }
            die();
        }

    }
    class MYPDF extends TCPDF {

		public function Header(){
			$request = configBasica();
            $strNit = $request['nit'];
            $strRazon = $request['razonsocial'];

			//Parte Izquierda
			$this->Image('../../../../imagenes/escudo.jpg',  13, 13, 20, 20, 'JPG', '', 'T', true, 300, '', false, false, 0, false, false, false);// Logo
			$this->SetFont('helvetica','B',8);
			$this->SetY(10);
			$this->RoundedRect(10, 10, 190, 25, 1,'');
			$this->Cell(0.1);
			$this->Cell(26,25,'','R',0,'L');
			$this->SetY(8);
			$this->SetX(40);
			$this->SetFont('helvetica','B',9);
			$this->Cell(140,15,strtoupper("$strRazon"),0,0,'C');
			$this->SetFont('helvetica','B',7);
			$this->SetY(12);
			$this->SetX(40);
			$this->Cell(140,15,'NIT: '.$strNit,0,0,'C');
			//*****************************************************************************************************************************
			$this->SetFont('helvetica','B',9);
			$this->SetY(23);
			$this->SetX(36);
            $this->Cell(164,12,"FORMATO REGISTRO DE CONTRATO",'T',0,'C');

			//**********************************************************
			$this->SetFont('times','B',10);
			$this->ln(12);
           
			//**********************************************************
            if(ESTADO=="N"){
                $img_file = "../../../../assets/img/anulado.png";
                $this->SetAlpha(0.35);
                $this->Image($img_file, 0, 20, 250, 280, '', '', '', false, 300, '', false, false, 300);
                $this->SetAlpha(1);
            }
		}
		public function Footer(){

			$request = configBasica();
            $strDireccion = $request['direccion'] != "" ? "Dirección: ".strtoupper($request['direccion']) : "";
            $strWeb = $request['web'] != "" ? "Pagina web: ".strtoupper($request['web']) :"";
            $strEmail = $request['email'] !="" ? "Email: ".strtoupper($request['email']) :"";
            $strTelefono = $request['telefono'] != "" ? "Telefonos: ".$request['telefono'] : "";
            $strUsuario = searchUser($_SESSION['cedulausu'])['nom_usu'];
			$strNick = $_SESSION['nickusu'];
			$strFecha = date("d/m/Y H:i:s");
			$strIp = $_SERVER['REMOTE_ADDR'];
            $this->setY(240);
            $this->setX(30);
            $this->SetFont('helvetica','B',6);
            $this->SetFillColor(153,221,255);
            $this->setX(80);
            $this->cell(70,4,'DATOS QUIEN CERTIFICA','LRTB',0,'C',1);
            $this->ln();
            $this->setX(30);
            $this->SetFont('helvetica','B',6);
            $this->SetFillColor(255,255,255);
            $this->setX(80);
            $this->cell(70,4,'NOMBRE:','LRTB',0,'L',1);
            $this->ln();
            $this->setX(30);
            $this->SetFont('helvetica','B',6);
            $this->SetFillColor(255,255,255);
            $this->setX(80);
            $this->cell(70,4,'CARGO: ','LRTB',0,'L',1);
            $this->ln();
            $this->setX(80);
            $this->cell(70,4,'FECHA RECEPCIÓN: ','LRTB',0,'L',1);
            $this->ln();
            $this->setX(30);
            $this->SetFont('helvetica','',6);
            $this->SetFillColor(255,255,255);
            $this->setX(80);
            $this->cell(70,15,'Firma','LRTB',0,'C',1);

            $this->setY(280);
			$this->SetFont('helvetica', 'I', 8);
			$txt = <<<EOD
			$strDireccion $strTelefono
			$strEmail $strWeb
			EOD;
			$this->SetFont('helvetica', 'I', 6);
			$this->Cell(190,10,'','T',0,'T');
			$this->ln(2);
			$this->Write(0, $txt , '', 0, 'C', true, 0, false, false, 0);

			$this->Cell(50, 10, 'Hecho por: '.$strUsuario, 00, false, 'C', 0, '', 0, false, 'T', 'M');
			$this->Cell(30, 10, 'Impreso por: '.$strNick, 0, false, 'C', 0, '', 0, false, 'T', 'M');
			$this->Cell(30, 10, 'IP: '.$strIp, 0, false, 'C',0, '', 0, false, 'T', 'M');
			$this->Cell(30, 10, 'Fecha: '.$strFecha, 0, false, 'C', 0, '', 0, false, 'T', 'M');
			$this->Cell(30, 10, 'IDEAL.10 S.A.S', 0, false, 'C', 0, '', 0, false, 'T', 'M');
            $this->Cell(20, 10, 'Pagina '.$this->getAliasNumPage().'/'.$this->getAliasNbPages(), 0, false, 'C', 0, '', 0, false, 'T', 'M');
		}
	}

    if($_POST){
        $obj = new RegistroExportController();
        if($_POST['action'] == "pdf"){
            $obj->exportPdf();
        }
    }

?>
