const URL ='vue/presupuesto_ccp/registro_contratos/controllers/RegistroController.php';
const URLEXPORT ='vue/presupuesto_ccp/registro_contratos/controllers/RegistroExportController.php';
var app = new Vue({
	el: '#myapp',
	data() {
		return {
			isLoading:false,
			isModalTercero: false,
			isModalCdp: false,
			txtConsecutivo: '',
			txtContrato: '',
			txtPlazo: '',
			txtGarantias: 'N',
			txtAnexos: '',
			txtConcepto: '',
			txtSearchCdp: '',
			txtSearchTercero: '',
			txtItemValor:0,
			txtItemUnidad:"",
			txtItemDescripcion:"",
			txtItemCantidad:0,
			txtItemTotal:0,
			objCdp:{codigo:"", id_plan:"", codigo_plan:"", modalidad:"", tipo_contrato:"", objeto:"", valor:"", nombre_modalidad:'',plazo:""},
			objTercero:{nombre:"", codigo:""},
			arrConsecutivos:[],
			arrTerceros: [],
			arrTercerosCopy: [],
			arrCdps: [],
			arrCdpsCopy: [],
			arrItems:[],
			txtResultadosTercero: 0,
			txtResultadosCdp: 0,
			txtEstado:"",
			txtFecha:new Date().toISOString().split("T")[0],
			searchFechaInicial:new Date(new Date().getFullYear(), 0, 1).toISOString().split("T")[0],
            searchFechaFinal:new Date().toISOString().split("T")[0],
            searchDocumento:"",
            searchCodigo:"",
            searchConsecutivo:"",
			searchDisponibilidad:"",
			searchResultados:0,
			arrSearchCopy:[],
			arrSearch:[],
            arrInfo:[],
            txtBpim: '',
		};
	},
	mounted(){
		const intPageVal = this.$refs.pageType.value;
        if(intPageVal == 2){
            this.getEdit();
        }else if(intPageVal == 3){
            this.getSearch();
        }else{
            this.getData();
        }
	},
	computed:{
        valorItem:function() {
            return this.formatMoney(this.txtItemValor)
        },
    },
	methods: {
		getData:async function(){
            const formData = new FormData();
            formData.append("action","get");
			this.isLoading = true;
            const response = await fetch(URL,{method:"POST",body:formData});
            const objData = await response.json();
			this.isLoading = false;
			this.arrCdps = objData.cdps;
			this.arrCdpsCopy = objData.cdps;
			this.arrTerceros = objData.terceros;
			this.arrTercerosCopy = objData.terceros;
			this.txtResultadosCdp = this.arrCdpsCopy.length;
			this.txtResultadosTercero = this.arrTercerosCopy.length;
        },
		getEdit:async function(){
            const codigo = new URLSearchParams(window.location.search).get('id');
            let formData = new FormData();
            formData.append("action","edit");
            formData.append("codigo",codigo);
            const response = await fetch(URL,{method:"POST",body:formData});
            const objData = await response.json();
            if(objData.status){
                let data = objData.data;
                this.arrInfo = data;
				this.arrCdps = objData.cdps;
				this.arrCdpsCopy = objData.cdps;
				this.arrTerceros = objData.terceros;
				this.arrTercerosCopy = objData.terceros;
				this.arrConsecutivos = objData.consecutivos;
				this.txtResultadosCdp = this.arrCdpsCopy.length;
				this.txtResultadosTercero = this.arrTercerosCopy.length;
				this.arrItems = data.detalle;
				this.txtFecha = data.fecha;
				this.txtGarantias = data.garantias;
				this.txtEstado = data.estado;
				this.txtConsecutivo = data.consecutivo;
				this.txtContrato = data.contrato;
				this.objCdp = this.arrCdps.filter(function(e){return e.codigo == data.cdp})[0];
				this.objTercero = this.arrTerceros.filter(function(e){return e.codigo == data.tercero})[0];
                this.arrInfo.tercero_info = this.objTercero;
				this.txtAnexos = data.anexo;
				this.calcularTotal();
            }else{
                window.location.href='ccp-registroContratoEditar?id='+objData.consecutivo;
            }
        },
        getSearch:async function(){
            const formData = new FormData();
            formData.append("action","search");
            formData.append("fecha_inicial",this.searchFechaInicial);
            formData.append("fecha_final",this.searchFechaFinal);
            formData.append("documento",this.searchDocumento);
            formData.append("consecutivo",this.searchConsecutivo);
            formData.append("codigo",this.searchCodigo);
			formData.append("cdp",this.searchDisponibilidad);
            this.isLoading = true;
            const response = await fetch(URL,{method:"POST",body:formData});
            const objData = await response.json();
            this.isLoading = false;
            this.arrSearch = objData;
            this.arrSearchCopy = objData;
            this.searchResultados = this.arrSearchCopy.length;
        },
		search:function(type=""){
            let search = "";
            if(type=="modal_cdp"){
				search = this.txtSearchCdp.toLowerCase();
                this.arrCdpsCopy = [...this.arrCdps.filter(e=>e.codigo.toLowerCase().includes(search) || e.objeto.toLowerCase().includes(search))];
                this.txtResultadosCdp = this.arrCdpsCopy.length;
            }else if(type=="modal_tercero"){
				search = this.txtSearchTercero.toLowerCase();
                this.arrTercerosCopy = [...this.arrTerceros.filter(e=>e.codigo.toLowerCase().includes(search) || e.nombre.toLowerCase().includes(search))];
                this.txtResultadosTercero = this.arrTercerosCopy.length;
            }else if(type=="cod_tercero"){
				search = this.objTercero.codigo;
                const data = [...this.arrTerceros.filter(e=>e.codigo == search)];
                this.objTercero = data.length > 0 ? JSON.parse(JSON.stringify(data[0])) : {codigo:"",nombre:""};
            }else if(type=="cod_cdp"){
				search = this.objCdp.codigo;
                const data = [...this.arrCdps.filter(e=>e.codigo == search)];
                this.objCdp = data.length > 0 ? JSON.parse(JSON.stringify(data[0])) : {codigo:"", id_plan:"", codigo_plan:"", modalidad:"", tipo_contrato:"", objeto:"", valor:"", nombre_modalidad:'',plazo:""};
            } else if (type == "bpim") {
                search = this.txtBpim.toLowerCase();    
                this.arrSearchCopy = [...this.arrSearch.filter(e => e.bpim.toLowerCase().includes(search) || e.contrato.toLowerCase().includes(search))];
                this.searchResultados = this.arrSearchCopy.length;
            }
        },
		selectItem:function (type="",{...data}){
			if(type=="cdp"){
				this.objCdp = data;
				this.isModalCdp = false;
			}else{
				this.objTercero = data;
				this.isModalTercero = false;
			}
        },
		setValue:function(event){
            const input = event.target.value;
            const numericValue = input.replace(/[^0-9,]/g, "");
            const parts = numericValue.split(",");
            this.txtItemValor = numericValue;
            if (parts.length > 2) {
                this.txtItemValor = parts[0] + "," + parts[1];
            } else {
                this.txtItemValor = numericValue;
            }
        },
		formatMoney:function(valor){
            valor = new String(valor);
            // Separar la parte entera y decimal
            const [integerPart, decimalPart] = valor.split(",");

            // Formatear la parte entera
            const formattedInteger = integerPart.replace(/\B(?=(\d{3})+(?!\d))/g, ".");
            return decimalPart !== undefined
            ? `$${formattedInteger},${decimalPart}`
            : `$${formattedInteger}`;
        },
		save:async function(){
            if(this.objCdp.codigo=="" || this.objCdp.plazo =="" ||  this.objTercero.codigo=="" || this.txtContrato ==""){
                Swal.fire("Atención!","Los campos con (*) son obligatorios.","warning");
                return false;
            }
            if(app.txtItemTotal != this.objCdp.valor){
                Swal.fire("Atención!","El total de los items debe ser igual al valor de disponibilidad","warning");
                return false;
            }
            Swal.fire({
                title:"¿Estás segur@ de guardar?",
                text:"",
                icon: 'warning',
                showCancelButton:true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText:"Sí, guardar",
                cancelButtonText:"No, cancelar"
            }).then(async function(result){
                if(result.isConfirmed){
                    const formData = new FormData();
                    formData.append("action","save");
                    formData.append("consecutivo",app.txtConsecutivo);
                    formData.append("fecha",app.txtFecha);
                    formData.append("contrato",app.txtContrato);
                    formData.append("tercero",app.objTercero.codigo);
					formData.append("anexos",app.txtAnexos);
					formData.append("garantia",app.txtGarantias);
                    formData.append("data",JSON.stringify(app.arrItems));
                    formData.append("cdp",JSON.stringify(app.objCdp));
                    const response = await fetch(URL,{method:"POST",body:formData});
                    const objData = await response.json();
                    if(objData.status){
                        Swal.fire("Guardado",objData.msg,"success");
                        if(objData.id && app.txtConsecutivo == 0){
                            setTimeout(function(){
                                window.location.href='ccp-registroContratoEditar?id='+objData.id;
                            },1500);
                        }else{
                            app.getEdit();
                        }
                    }else{
                        Swal.fire("Error",objData.msg,"error");
                    }
                }
            });
        },
		showTab:function(tab){
			let tabs = this.$refs.rTabs.children;
			let tabsContent = this.$refs.rTabsContent.children;
			for (let i = 0; i < tabs.length; i++) {
				tabs[i].classList.remove("active");
				tabsContent[i].classList.remove("active")
			}
			tabs[tab-1].classList.add("active");
			tabsContent[tab-1].classList.add("active")
		},
		addItem:function(){
			if(this.objCdp.codigo ==""){
				Swal.fire("Atención!","Debe seleccionar la disponibilidad","warning");
				return false;
			}
			if(this.txtItemValor == "" || this.txtItemCantidad =="" || this.txtItemDescripcion =="" || this.txtItemUnidad=="" ){
				Swal.fire("Atención!","Todos los campos con (*) son obligatorios","warning");
				return false;
			}
			if(this.txtItemValor <=0){
				Swal.fire("Atención!","El valor del item debe ser mayor a cero","warning");
				return false;
			}
			let valorCdp = parseInt(this.objCdp.valor);
			if((this.txtItemValor*this.txtItemCantidad) > valorCdp){
				Swal.fire("Atención!","El subtotal del item no puede ser mayor al valor de disponibilidad","warning");
				return false;
			}
			const data = this.arrItems;
			if(data.length > 0){
				if(parseInt(app.txtItemTotal)+parseInt(app.txtItemValor) > valorCdp){
					Swal.fire("Atención!","El total de los items no puede ser mayor al valor de disponibilidad","warning");
					return false;
				}
				data.push({
					"descripcion":app.txtItemDescripcion,
					"unidad":app.txtItemUnidad.toUpperCase(),
					"valor":parseInt(app.txtItemValor),
					"cantidad":app.txtItemCantidad,
					"subtotal":parseInt(app.txtItemCantidad)*parseInt(app.txtItemValor)
				});
			}else{
				data.push({
					"descripcion":app.txtItemDescripcion,
					"unidad":app.txtItemUnidad.toUpperCase(),
					"valor":parseInt(app.txtItemValor),
					"cantidad":app.txtItemCantidad,
					"subtotal":parseInt(app.txtItemCantidad)*parseInt(app.txtItemValor)
				});
			}
			this.calcularTotal();
		},
		calcularTotal:function(){
			this.txtItemTotal = 0;
			this.arrItems.forEach(e => {this.txtItemTotal+= parseInt(e.subtotal)});
		},
		delItem: function(index=-1) {
			if(index>=0){
				this.arrItems.splice(index,1);
			}else{
				this.arrItems = [];
			}
			this.calcularTotal();
        },
		nextItem:function(type){
            let id = this.txtConsecutivo;
            let index = this.arrConsecutivos.findIndex(function(e){return e.id == id});
            if(type=="next" && app.arrConsecutivos[++index]){
                id = this.arrConsecutivos[index++].id;
            }else if(type=="prev" && app.arrConsecutivos[--index]){
                id = this.arrConsecutivos[index--].id;
            }
           window.location.href='ccp-registroContratoEditar?id='+id;
        },
		changeStatus:function(item){
            Swal.fire({
                title:"¿Estás segur@ de anular?",
                text:"Tendrás que volverlo a hacer...",
                icon: 'warning',
                showCancelButton:true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText:"Sí",
                cancelButtonText:"No, cancelar"
            }).then(async function(result){
                if(result.isConfirmed){
                    const formData = new FormData();
                    formData.append("action","change");
                    formData.append("id",item.codigo);
                    const response = await fetch(URL,{method:"POST",body:formData});
                    const objData = await response.json();
                    if(objData.status){
                        app.getSearch();
                        Swal.fire("Registro de contrato anulada","","success");
                    }else{
                        Swal.fire("Error",objData.msg,"error");
                    }
                }else{
                    app.getSearch();
                }
            });
        },
        exportData:function(){
            const form = document.createElement("form");
            form.method ="post";
            form.target="_blank";
            form.action=URLEXPORT;

            function addField(name,value){
                const input = document.createElement("input");
                input.type="hidden";
                input.name=name;
                input.value = value;
                form.appendChild(input);
            }
            addField("action","pdf");
            addField("data",JSON.stringify(this.arrInfo));
            document.body.appendChild(form);
            form.submit();
            document.body.removeChild(form);
        },
	}
});