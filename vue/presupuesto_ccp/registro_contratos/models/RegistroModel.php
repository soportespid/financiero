<?php
    require_once '../../../../Librerias/core/Helpers.php';
    require_once '../../../../Librerias/core/Mysql.php';
    session_start();
    /*ini_set('display_errors', '1');
    ini_set('display_startup_errors', '1');
    error_reporting(E_ALL);*/
    class RegistroModel extends Mysql{
        private $intVigencia;
        private $strFecha;
        private $strContrato;
        private $strTercero;
        private $strGarantia;
        private $strAnexos;
        private $intConsecutivo;
        private $intConsecutivoVigencia;
        function __construct(){
            parent::__construct();
            $this->intVigencia = date_format(date_create("now"),"Y");
        }
        public function selectCdps(){
			$sql = "SELECT c.consvigencia as codigo, c.vigencia, c.objeto, c.valor, sc.id_solicitud_cdp,
            pc.id as id_plan, pc.codplan as cod_plan, pc.modalidad as id_modalidad, pc.duracionest as plazo, ps.tipo_contrato,pm.nombre as nombre_modalidad
			FROM ccpetcdp AS c
			INNER JOIN ccpet_solicitud_cdp AS sc ON c.consvigencia = sc.consecutivo AND c.vigencia = sc.vigencia
            INNER JOIN plan_solicitud_cdp ps ON ps.id = sc.id_solicitud_cdp
            INNER JOIN contraplancompras pc ON ps.id_paa = pc.id
            INNER JOIN plan_modalidad_seleccion pm ON pm.codigo = pc.modalidad 
			WHERE c.vigencia = '$this->intVigencia' AND c.tipo_mov = '201'";
			$request = $this->select_all($sql);
            $arrData = [];
            $total = count($request);
            if($total > 0){
                for ($i=0; $i < $total ; $i++) {
                    $e = $request[$i]; 
                    $e['valor'] = round($e['valor']);
                    $sql = "SELECT * FROM ccpetrp WHERE idcdp = {$e['codigo']} AND vigencia = {$e['vigencia']} AND estado = 'S'";
                    $requestRp = $this->select_all($sql);
                    if(empty($requestRp)){
                        array_push($arrData,$e);
                    }
                }
            }
			return $arrData;
		}
        public function selectEdit(int $intConsecutivo){
            $this->intConsecutivo = $intConsecutivo;
			$sql = "SELECT c.consvigencia as codigo, 
            c.vigencia, 
            c.objeto, 
            c.valor, 
            sc.id_solicitud_cdp,
            pc.id as id_plan, 
            pc.codplan as cod_plan, 
            pc.modalidad as id_modalidad, 
            ps.tipo_contrato,
            pm.nombre as nombre_modalidad,
            rc.contrato, 
            rc.fecha,
            date_format(rc.fecha,'%d-%m-%Y') as fecha_formato, 
            rc.cdp, 
            rc.tercero,
            rc.garantias, 
            rc.anexo, 
            rc.estado,
            rc.id as consecutivo,
            rc.plazo,
            rc.contrato,
            ps.tipo_contrato_o_acto AS tipo_documento,
            ps.numero_acto,
            date_format(ps.fecha_acto,'%d-%m-%Y') as fecha_acto, 
            ps.tipo_gasto,
            ps.id_area_solicitante,
            ps.tipo_cdp,
            ps.sector AS cod_sector,
            s.nombre AS nombre_sector,
            date_format(ps.fecha_solicitud,'%d-%m-%Y') as fecha_solicitud,
            date_format(ps.fecha_vencimiento,'%d-%m-%Y') as fecha_vencimiento, 
            pa.nombrearea
			FROM ccpetcdp AS c
			INNER JOIN ccpet_solicitud_cdp AS sc ON c.consvigencia = sc.consecutivo AND c.vigencia = sc.vigencia
            LEFT JOIN plan_solicitud_cdp ps ON ps.id = sc.id_solicitud_cdp
            LEFT JOIN contraplancompras pc ON ps.id_paa = pc.id
            LEFT JOIN plan_modalidad_seleccion pm ON pm.codigo = pc.modalidad
            LEFT JOIN ccp_registro_contrato rc ON c.consvigencia = rc.cdp AND c.vigencia = rc.vigencia
            LEFT JOIN ccpetsectores AS s ON ps.sector = s.codigo
            LEFT JOIN planacareas AS pa ON ps.id_area_solicitante = pa.codarea
			WHERE rc.id = '$this->intConsecutivo' AND c.tipo_mov = '201'";
			$request = $this->select($sql);
            if(!empty($request)){
                $sql = "SELECT descripcion,unidad, valor_unitario as valor, valor_total as subtotal, cantidad 
                FROM ccp_registro_contrato_det WHERE codigo = '{$request['consecutivo']}'";
                $request['detalle'] = $this->select_all($sql);

                //Solicitud detalle
                $sql = "SELECT scdp.tipo_presupuesto AS tipoPresupuesto,
                dependencia.nombre AS nameDependencia,
                scdp.medio_pago AS medioPago,
                vig.nombre AS nameVigGasto,
                scdp.cuenta,
                scdp.fuente AS codFuente,
                scdp.cpc,
                scdp.indicador AS codIndicador,
                bpin.codigo AS bpin,
                scdp.id_detalle_sectorial AS codSectorial,
                scdp.id_politica_publica AS codPoliticaPublica,
                scdp.valor
                FROM plan_solicitud_cdp_det AS scdp
                INNER JOIN pptoseccion_presupuestal AS dependencia
                ON scdp.dependencia = dependencia.id_seccion_presupuestal
                INNER JOIN ccpet_vigenciadelgasto AS vig
                ON scdp.id_vig_gasto = vig.codigo
                LEFT JOIN ccpproyectospresupuesto AS bpin
                ON scdp.id_bpin = bpin.id
                WHERE scdp.id_solicitud_cdp = {$request['id_solicitud_cdp']}";
                $request['detalle_solicitud'] = $this->select_all($sql);
            }
			return $request;
		}
        public function insertData(string $strFecha, string $strContrato, string $strAnexos,string $strTercero,string $strGarantia, array $arrCdp){
            $this->strContrato = $strContrato;
            $this->strAnexos = $strAnexos;
            $this->strFecha = $strFecha;
            $this->strTercero = $strTercero;
            $this->strGarantia = $strGarantia;
			$this->intConsecutivoVigencia = searchConsecVigencia("ccp_registro_contrato","codigo","vigencia");
            $sql = "SELECT * FROM ccp_registro_contrato WHERE cdp = {$arrCdp['codigo']} AND estado = 'S'";
            $request = $this->select_all($sql);
            $return ="";
            if(empty($request)){
                $sql = "INSERT INTO ccp_registro_contrato (codigo, contrato, fecha, vigencia, cdp, tercero, id_paa, modalidad, 
                tipo_contrato, valor, plazo, garantias, anexo, objeto, estado) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
                $request = $this->insert($sql,[
                    $this->intConsecutivoVigencia,
                    $this->strContrato,
                    $this->strFecha,
                    $arrCdp['vigencia'],
                    $arrCdp['codigo'],
                    $this->strTercero,
                    $arrCdp['id_plan'],
                    $arrCdp['id_modalidad'],
                    $arrCdp['tipo_contrato'],
                    $arrCdp['valor'],
                    $arrCdp['plazo'],
                    $this->strGarantia,
                    $this->strAnexos,
                    $arrCdp['objeto'],
                    'S'
                ]);
                $return = $request;
            }else{
                $return = "existe";
            }
			return $return;
		}
        public function updateData(int $intConsecutivo,string $strFecha, string $strContrato, string $strAnexos,string $strTercero,string $strGarantia, array $arrCdp){
            $this->strContrato = $strContrato;
            $this->strAnexos = $strAnexos;
            $this->strFecha = $strFecha;
            $this->strTercero = $strTercero;
            $this->strGarantia = $strGarantia;
            $this->intConsecutivo = $intConsecutivo;
			$sql = "UPDATE ccp_registro_contrato SET contrato=?, fecha=?, tercero=?, plazo=?, garantias=?, anexo=? 
            WHERE id = $this->intConsecutivo";
			$request = $this->update($sql,[
				$this->strContrato,
				$this->strFecha,
				$this->strTercero,
                $arrCdp['plazo'],
                $this->strGarantia,
                $this->strAnexos
            ]);
			return $request;
		}
        public function insertDet(int $intConsecutivo,array $data){
            $this->intConsecutivo = $intConsecutivo;
            $this->delete("DELETE FROM ccp_registro_contrato_det WHERE codigo = $this->intConsecutivo");
            foreach ($data as $d) {
                $sql="INSERT INTO ccp_registro_contrato_det(codigo, descripcion, unidad, valor_unitario,valor_total,cantidad) VALUES(?,?,?,?,?,?)";
                $request = $this->insert($sql,[
                    $this->intConsecutivo,
                    $d['descripcion'],
                    $d['unidad'],
                    $d['valor'],
                    $d['subtotal'],
                    $d['cantidad'],
                ]);
            }
            return $request;
        }
        public function selectSearch($strFechaInicial,$strFechaFinal,$strCodigo,$strTercero,$intConsecutivo,$intCdp){
            $sql = "SELECT *,DATE_FORMAT(fecha,'%d/%m/%Y') as fecha
            FROM humretenempleados WHERE estado != '' AND fecha BETWEEN '$strFechaInicial' AND '$strFechaFinal'
            AND empleado LIKE '$strDocumento%' AND idfuncionario LIKE '$intContrato%' AND descripcion LIKE '$strDescripcion%'
            ORDER BY id DESC";

            $sql = "SELECT c.consvigencia as cdp, 
            c.vigencia, 
            c.objeto, 
            c.valor, 
            sc.id_solicitud_cdp,
            pc.id as id_plan, 
            pc.codplan as cod_plan, 
            pc.modalidad as id_modalidad, 
            ps.tipo_contrato,pm.nombre as nombre_modalidad,
            rc.contrato, 
            DATE_FORMAT(rc.fecha,'%d/%m/%Y') as fecha, 
            rc.tercero,
            rc.garantias, 
            rc.anexo, 
            rc.estado,
            rc.codigo as consecutivo,
            rc.id as codigo,
            rc.plazo,
            rc.contrato,
            rc.tercero as tercero,
            CASE WHEN t.razonsocial IS NULL OR t.razonsocial = ''
            THEN CONCAT(t.nombre1,' ',t.nombre2,' ',t.apellido1,' ',t.apellido2)
            ELSE t.razonsocial END AS nombre_tercero
			FROM ccpetcdp AS c
			INNER JOIN ccpet_solicitud_cdp AS sc ON c.consvigencia = sc.consecutivo AND c.vigencia = sc.vigencia
            INNER JOIN plan_solicitud_cdp ps ON ps.id = sc.id_solicitud_cdp
            INNER JOIN contraplancompras pc ON ps.id_paa = pc.id
            INNER JOIN plan_modalidad_seleccion pm ON pm.codigo = pc.modalidad
            INNER JOIN ccp_registro_contrato rc ON c.consvigencia = rc.cdp AND c.vigencia = rc.vigencia
            INNER JOIN terceros t ON rc.tercero = t.cedulanit
			WHERE rc.cdp LIKE '$strCodigo%' AND rc.fecha BETWEEN '$strFechaInicial' AND '$strFechaFinal' AND c.tipo_mov = '201'
            AND rc.tercero LIKE '$strTercero%' AND c.consvigencia LIKE '$intCdp%' AND rc.codigo LIKE '$intConsecutivo%' ORDER BY rc.id DESC";
            $request = $this->select_all($sql);

            foreach ($request as &$data) {
                $sql_bpim = "SELECT bpim FROM ccpetcdp_detalle WHERE consvigencia = '$data[cdp]' AND vigencia = '$data[vigencia]' LIMIT 1";
                $bpim = $this->select($sql_bpim);
                $data['bpim'] = $bpim['bpim'];
            }

            return $request;
        }
        public function updateStatus(int $intConsecutivo){
            $this->intConsecutivo = $intConsecutivo;
            $sql = "UPDATE ccp_registro_contrato SET estado = ? WHERE id = $this->intConsecutivo";
            $request = $this->update($sql,['N']);
            return $request;
        }
    }
?>
