<?php
    require_once '../../Librerias/core/Helpers.php';
    require_once '../../PHPExcel/Classes/PHPExcel.php';
    session_start();
    /*ini_set('display_errors', '1');
    ini_set('display_startup_errors', '1');
    error_reporting(E_ALL);*/
    class EjecucionExportControler {
        public function exportExcel(){
            if(!empty($_SESSION)){
                $arrData = json_decode($_POST['data'],true);
                if(!empty($arrData)){
                    $arrData = json_decode($_POST['data'],true);
                    $request = configBasica();
                    $strNit = $request['nit'];
                    $strRazon = $request['razonsocial'];
                    $strFechaInicial = $_POST['fecha_inicial'];
                    $strFechaFinal = $_POST['fecha_final'];
                    $objPHPExcel = new PHPExcel();
                    $objPHPExcel->getActiveSheet()->getStyle('A:W')->applyFromArray(
                        array(
                            'alignment' => array(
                                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
                                'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER
                            )
                        )
                    );
                    $objPHPExcel->getProperties()
                    ->setCreator("IDEAL 10")
                    ->setLastModifiedBy("IDEAL 10")
                    ->setTitle("Exportar Excel con PHP")
                    ->setSubject("Documento de prueba")
                    ->setDescription("Documento generado con PHPExcel")
                    ->setKeywords("usuarios phpexcel")
                    ->setCategory("reportes");
                    //----Cuerpo de Documento----
                    $objPHPExcel->setActiveSheetIndex(0)
                    ->mergeCells('A1:W1')
                    ->mergeCells('A2:W2')
                    ->mergeCells('A3:W3')
                    ->setCellValue('A1', 'PRESUPUESTO - '.$strRazon)
                    ->setCellValue('A2', 'INFORME DE EJECUCIÓN PRESUPUESTAL DE GASTOS')
                    ->setCellValue('A3', "Desde ".$strFechaInicial." Hasta ".$strFechaFinal);
                    $objPHPExcel-> getActiveSheet ()
                    -> getStyle ("A1")
                    -> getFill ()
                    -> setFillType (PHPExcel_Style_Fill :: FILL_SOLID)
                    -> getStartColor ()
                    -> setRGB ('3399cc');

                    $objPHPExcel-> getActiveSheet ()
                    -> getStyle ('A1:A3')
                    -> getAlignment ()
                    -> applyFromArray (array ( 'horizontal'  =>  PHPExcel_Style_Alignment :: HORIZONTAL_CENTER ,) );

                    $objPHPExcel-> getActiveSheet ()
                    -> getStyle ("A2")
                    -> getFill ()
                    -> setFillType (PHPExcel_Style_Fill :: FILL_SOLID);

                    $borders = array(
                        'borders' => array(
                            'allborders' => array(
                                'style' => PHPExcel_Style_Border::BORDER_THIN,
                            'color' => array('argb' => 'FF000000'),
                            )
                        ),
                    );
                    $totalInicial = 0;
                    $totalAdicion = 0;
                    $totalReduccion = 0;
                    $totalCredito = 0;
                    $totalContracredito = 0;
                    $totalDefinitivo = 0;
                    $totalDisponibilidad = 0;
                    $totalCompromisos = 0;
                    $totalObligacion = 0;
                    $totalEgreso = 0;
                    $totalSaldo = 0;

                    $funcionamientoInicial = 0;
                    $funcionamientoAdicion = 0;
                    $funcionamientoReduccion = 0;
                    $funcionamientoCredito = 0;
                    $funcionamientoContracredito = 0;
                    $funcionamientoDefinitivo = 0;
                    $funcionamientoDisponibilidad = 0;
                    $funcionamientoCompromisos = 0;
                    $funcionamientoObligacion = 0;
                    $funcionamientoEgreso = 0;
                    $funcionamientoSaldo = 0;

                    $inversionInicial = 0;
                    $inversionAdicion = 0;
                    $inversionReduccion = 0;
                    $inversionCredito = 0;
                    $inversionContracredito = 0;
                    $inversionDefinitivo = 0;
                    $inversionDisponibilidad = 0;
                    $inversionCompromisos = 0;
                    $inversionObligacion = 0;
                    $inversionEgreso = 0;
                    $inversionSaldo = 0;

                    $objPHPExcel->getActiveSheet()->getStyle("A1:A3")->getFont()->setBold(true);
                    $objPHPExcel->getActiveSheet()->getStyle('A1:W1')->applyFromArray($borders);
                    $objPHPExcel->getActiveSheet()->getStyle('A2:W2')->applyFromArray($borders);
                    $objPHPExcel->getActiveSheet()->getStyle('A3:W3')->applyFromArray($borders);
                    $objPHPExcel->getActiveSheet()->getStyle("A1")->getFont()->getColor()->setRGB("ffffff");
                    $row = 4;
                    foreach ($arrData as $arbol) {
                        $totalInicial += $arbol['total']['total_inicial'];
                        $totalAdicion += $arbol['total']['total_adicion'];
                        $totalReduccion += $arbol['total']['total_reduccion'];
                        $totalCredito += $arbol['total']['total_credito'];
                        $totalContracredito += $arbol['total']['total_contracredito'];
                        $totalDefinitivo += $arbol['total']['total_definitivo'];
                        $totalDisponibilidad += $arbol['total']['total_disponibilidad'];
                        $totalCompromisos += $arbol['total']['total_compromisos'];
                        $totalObligacion += $arbol['total']['total_obligacion'];
                        $totalEgreso += $arbol['total']['total_egreso'];
                        $totalSaldo += $arbol['total']['total_saldo'];

                        $funcionamientoInicial += $arbol['funcionamiento']['total_inicial'];
                        $funcionamientoAdicion += $arbol['funcionamiento']['total_adicion'];
                        $funcionamientoReduccion += $arbol['funcionamiento']['total_reduccion'];
                        $funcionamientoCredito += $arbol['funcionamiento']['total_credito'];
                        $funcionamientoContracredito += $arbol['funcionamiento']['total_contracredito'];
                        $funcionamientoDefinitivo += $arbol['funcionamiento']['total_definitivo'];
                        $funcionamientoDisponibilidad += $arbol['funcionamiento']['total_disponibilidad'];
                        $funcionamientoCompromisos += $arbol['funcionamiento']['total_compromisos'];
                        $funcionamientoObligacion += $arbol['funcionamiento']['total_obligacion'];
                        $funcionamientoEgreso += $arbol['funcionamiento']['total_egreso'];
                        $funcionamientoSaldo += $arbol['funcionamiento']['total_saldo'];

                        $inversionInicial += $arbol['inversion']['total_inicial'];
                        $inversionAdicion += $arbol['inversion']['total_adicion'];
                        $inversionReduccion += $arbol['inversion']['total_reduccion'];
                        $inversionCredito += $arbol['inversion']['total_credito'];
                        $inversionContracredito += $arbol['inversion']['total_contracredito'];
                        $inversionDefinitivo += $arbol['inversion']['total_definitivo'];
                        $inversionDisponibilidad += $arbol['inversion']['total_disponibilidad'];
                        $inversionCompromisos += $arbol['inversion']['total_compromisos'];
                        $inversionObligacion += $arbol['inversion']['total_obligacion'];
                        $inversionEgreso += $arbol['inversion']['total_egreso'];
                        $inversionSaldo += $arbol['inversion']['total_saldo'];
                    }
                    $totalSaldosPorComprometer = round($totalDefinitivo-$totalCompromisos, 2);
                    $totalCompromisosEnEjecucion = round($totalCompromisos-$totalObligacion, 2);
                    $totalCuentasPorPagar = round($totalObligacion-$totalEgreso, 2);

                    $funcionamientoSaldosPorComprometer = round($funcionamientoDefinitivo-$funcionamientoCompromisos, 2);
                    $funcionamientoCompromisosEnEjecucion = round($funcionamientoCompromisos-$funcionamientoObligacion, 2);
                    $funcionamientoCuentasPorPagar = round($funcionamientoObligacion-$funcionamientoEgreso, 2);

                    $inversionSaldosPorComprometer = round($inversionDefinitivo-$inversionCompromisos, 2);
                    $inversionCompromisosEnEjecucion = round($inversionCompromisos-$inversionObligacion, 2);
                    $inversionCuentasPorPagar = round($inversionObligacion-$inversionEgreso, 2);

                    $objPHPExcel->setActiveSheetIndex(0)
                    ->mergeCells("A$row:W$row")
                    ->setCellValue("A$row", "TOTAL UNIDADES EJECUTORAS");
                    $objPHPExcel-> getActiveSheet ()
                    -> getStyle ("A$row")
                    -> getFill ()
                    -> setFillType (PHPExcel_Style_Fill :: FILL_SOLID)
                    -> getStartColor ()
                    -> setRGB ('3399cc');
                    $objPHPExcel-> getActiveSheet ()
                    -> getStyle ("A$row")
                    -> getAlignment ()
                    -> applyFromArray (array ( 'horizontal'  =>  PHPExcel_Style_Alignment :: HORIZONTAL_CENTER ,) );
                    $objPHPExcel->getActiveSheet()->getStyle("A$row")->getFont()->setBold(true);
                    $objPHPExcel->getActiveSheet()->getStyle("A$row:W$row")->applyFromArray($borders);
                    $objPHPExcel->getActiveSheet()->getStyle("A$row:W$row")->getFont()->getColor()->setRGB("ffffff");
                    $row++;

                    $objPHPExcel->setActiveSheetIndex(0)
                    ->setCellValue("A$row", "Sección")
                    ->setCellValue("B$row", "Dependencia")
                    ->setCellValue("C$row", "Vigencia gasto")
                    ->setCellValue("D$row", "Rubro")
                    ->setCellValue("E$row", "Nombre")
                    ->setCellValue("F$row", "Tipo")
                    ->setCellValue("G$row", "Fuente")
                    ->setCellValue("H$row", "Nombre fuente")
                    ->setCellValue("I$row", "Medio de pago")
                    ->setCellValue("J$row", "Presupuesto inicial")
                    ->setCellValue("K$row", "Adiciones")
                    ->setCellValue("L$row", "Reducciones")
                    ->setCellValue("M$row", "Creditos")
                    ->setCellValue("N$row", "Contracreditos")
                    ->setCellValue("O$row", "Definitivo")
                    ->setCellValue("P$row", "Disponibilidades")
                    ->setCellValue("Q$row", "Compromisos")
                    ->setCellValue("R$row", "Saldos por comprometer")
                    ->setCellValue("S$row", "Obligaciones")
                    ->setCellValue("T$row", "Compromisos en ejecución")
                    ->setCellValue("U$row", "Pagos")
                    ->setCellValue("V$row", "Cuentas por pagar")
                    ->setCellValue("W$row", "Saldos por disponer");
                    $objPHPExcel-> getActiveSheet ()
                    -> getStyle ("A$row:W$row")
                    -> getFill ()
                    -> setFillType (PHPExcel_Style_Fill :: FILL_SOLID)
                    -> getStartColor ()
                    -> setRGB ('3399cc');
                    $objPHPExcel-> getActiveSheet () -> getStyle ("A$row:W$row") -> getAlignment () -> applyFromArray (array ( 'horizontal'  =>  PHPExcel_Style_Alignment :: HORIZONTAL_CENTER ,) );
                    $objPHPExcel->getActiveSheet()->getStyle("A$row:W$row")->getFont()->setBold(true);
                    $objPHPExcel->getActiveSheet()->getStyle("A$row:W$row")->applyFromArray($borders);
                    $objPHPExcel->getActiveSheet()->getStyle("A$row:W$row")->getFont()->getColor()->setRGB("ffffff");

                    $row++;
                    $objPHPExcel->setActiveSheetIndex(0)
                    ->setCellValue("A$row", "")
                    ->setCellValue("B$row", "")
                    ->setCellValue("C$row", "")
                    ->setCellValue("D$row", "Total")
                    ->setCellValue("E$row", "Total gastos")
                    ->setCellValue("F$row", "")
                    ->setCellValue("G$row", "")
                    ->setCellValue("H$row", "")
                    ->setCellValue("I$row", "")
                    ->setCellValue("J$row", formatNum($totalInicial))
                    ->setCellValue("K$row", formatNum($totalAdicion))
                    ->setCellValue("L$row", formatNum($totalReduccion))
                    ->setCellValue("M$row", formatNum($totalCredito))
                    ->setCellValue("N$row", formatNum($totalContracredito))
                    ->setCellValue("O$row", formatNum($totalDefinitivo))
                    ->setCellValue("P$row", formatNum($totalDisponibilidad))
                    ->setCellValue("Q$row", formatNum($totalCompromisos))
                    ->setCellValue("R$row", formatNum($totalSaldosPorComprometer))
                    ->setCellValue("S$row", formatNum($totalObligacion))
                    ->setCellValue("T$row", formatNum($totalCompromisosEnEjecucion))
                    ->setCellValue("U$row", formatNum($totalEgreso))
                    ->setCellValue("V$row", formatNum($totalCuentasPorPagar))
                    ->setCellValue("W$row", formatNum($totalSaldo));
                    $objPHPExcel->getActiveSheet()->getStyle("A$row:W$row")->getFont()->setBold(true);
                    $objPHPExcel->getActiveSheet()->getStyle("A$row:W$row")->applyFromArray($borders);
                    $objPHPExcel-> getActiveSheet () -> getStyle ("J$row:W$row") -> getAlignment () -> applyFromArray (array ( 'horizontal'  =>  PHPExcel_Style_Alignment :: HORIZONTAL_RIGHT ,) );
                    $objPHPExcel-> getActiveSheet ()
                    -> getStyle ("A$row:W$row")
                    -> getFill ()
                    -> setFillType (PHPExcel_Style_Fill :: FILL_SOLID)
                    -> getStartColor ()
                    -> setRGB ('99ddff');
                    $row++;

                    $objPHPExcel->setActiveSheetIndex(0)
                    ->setCellValue("A$row", "")
                    ->setCellValue("B$row", "")
                    ->setCellValue("C$row", "")
                    ->setCellValue("D$row", "Total")
                    ->setCellValue("E$row", "Total Gastos de Funcionamiento, servicio de la deuda")
                    ->setCellValue("F$row", "")
                    ->setCellValue("G$row", "")
                    ->setCellValue("H$row", "")
                    ->setCellValue("I$row", "")
                    ->setCellValue("J$row", formatNum($funcionamientoInicial))
                    ->setCellValue("K$row", formatNum($funcionamientoAdicion))
                    ->setCellValue("L$row", formatNum($funcionamientoReduccion))
                    ->setCellValue("M$row", formatNum($funcionamientoCredito))
                    ->setCellValue("N$row", formatNum($funcionamientoContracredito))
                    ->setCellValue("O$row", formatNum($funcionamientoDefinitivo))
                    ->setCellValue("P$row", formatNum($funcionamientoDisponibilidad))
                    ->setCellValue("Q$row", formatNum($funcionamientoCompromisos))
                    ->setCellValue("R$row", formatNum($funcionamientoSaldosPorComprometer))
                    ->setCellValue("S$row", formatNum($funcionamientoObligacion))
                    ->setCellValue("T$row", formatNum($funcionamientoCompromisosEnEjecucion))
                    ->setCellValue("U$row", formatNum($funcionamientoEgreso))
                    ->setCellValue("V$row", formatNum($funcionamientoCuentasPorPagar))
                    ->setCellValue("W$row", formatNum($funcionamientoSaldo));
                    $objPHPExcel->getActiveSheet()->getStyle("A$row:W$row")->getFont()->setBold(true);
                    $objPHPExcel->getActiveSheet()->getStyle("A$row:W$row")->applyFromArray($borders);
                    $objPHPExcel-> getActiveSheet () -> getStyle ("J$row:W$row") -> getAlignment () -> applyFromArray (array ( 'horizontal'  =>  PHPExcel_Style_Alignment :: HORIZONTAL_RIGHT ,) );
                    $objPHPExcel-> getActiveSheet ()
                    -> getStyle ("A$row:W$row")
                    -> getFill ()
                    -> setFillType (PHPExcel_Style_Fill :: FILL_SOLID)
                    -> getStartColor ()
                    -> setRGB ('99ddff');
                    $row++;

                    $objPHPExcel->setActiveSheetIndex(0)
                    ->setCellValue("A$row", "")
                    ->setCellValue("B$row", "")
                    ->setCellValue("C$row", "")
                    ->setCellValue("D$row", "Total")
                    ->setCellValue("E$row", "Total Gastos de Inversion")
                    ->setCellValue("F$row", "")
                    ->setCellValue("G$row", "")
                    ->setCellValue("H$row", "")
                    ->setCellValue("I$row", "")
                    ->setCellValue("J$row", formatNum($inversionInicial))
                    ->setCellValue("K$row", formatNum($inversionAdicion))
                    ->setCellValue("L$row", formatNum($inversionReduccion))
                    ->setCellValue("M$row", formatNum($inversionCredito))
                    ->setCellValue("N$row", formatNum($inversionContracredito))
                    ->setCellValue("O$row", formatNum($inversionDefinitivo))
                    ->setCellValue("P$row", formatNum($inversionDisponibilidad))
                    ->setCellValue("Q$row", formatNum($inversionCompromisos))
                    ->setCellValue("R$row", formatNum($inversionSaldosPorComprometer))
                    ->setCellValue("S$row", formatNum($inversionObligacion))
                    ->setCellValue("T$row", formatNum($inversionCompromisosEnEjecucion))
                    ->setCellValue("U$row", formatNum($inversionEgreso))
                    ->setCellValue("V$row", formatNum($inversionCuentasPorPagar))
                    ->setCellValue("W$row", formatNum($inversionSaldo));
                    $objPHPExcel->getActiveSheet()->getStyle("A$row:W$row")->getFont()->setBold(true);
                    $objPHPExcel->getActiveSheet()->getStyle("A$row:W$row")->applyFromArray($borders);
                    $objPHPExcel-> getActiveSheet () -> getStyle ("J$row:W$row") -> getAlignment () -> applyFromArray (array ( 'horizontal'  =>  PHPExcel_Style_Alignment :: HORIZONTAL_RIGHT ,) );
                    $objPHPExcel-> getActiveSheet ()
                    -> getStyle ("A$row:W$row")
                    -> getFill ()
                    -> setFillType (PHPExcel_Style_Fill :: FILL_SOLID)
                    -> getStartColor ()
                    -> setRGB ('99ddff');
                    $row++;

                    foreach ($arrData as $arbol) {

                        $objPHPExcel->setActiveSheetIndex(0)
                        ->mergeCells("A$row:W$row")
                        ->setCellValue("A$row", $arbol['nombre']);
                        $objPHPExcel-> getActiveSheet ()
                        -> getStyle ("A$row")
                        -> getFill ()
                        -> setFillType (PHPExcel_Style_Fill :: FILL_SOLID)
                        -> getStartColor ()
                        -> setRGB ('3399cc');
                        $objPHPExcel-> getActiveSheet ()
                        -> getStyle ("A$row")
                        -> getAlignment ()
                        -> applyFromArray (array ( 'horizontal'  =>  PHPExcel_Style_Alignment :: HORIZONTAL_CENTER ,) );
                        $objPHPExcel->getActiveSheet()->getStyle("A$row")->getFont()->setBold(true);
                        $objPHPExcel->getActiveSheet()->getStyle("A$row:W$row")->applyFromArray($borders);
                        $objPHPExcel->getActiveSheet()->getStyle("A$row:W$row")->getFont()->getColor()->setRGB("ffffff");
                        $row++;

                        $objPHPExcel->setActiveSheetIndex(0)
                        ->setCellValue("A$row", "Unidad ejecutora")
                        ->setCellValue("B$row", "Sección presupuestal")
                        ->setCellValue("C$row", "Vigencia gasto")
                        ->setCellValue("D$row", "Rubro")
                        ->setCellValue("E$row", "Nombre")
                        ->setCellValue("F$row", "Tipo")
                        ->setCellValue("G$row", "Fuente")
                        ->setCellValue("H$row", "Nombre fuente")
                        ->setCellValue("I$row", "Medio de pago")
                        ->setCellValue("J$row", "Presupuesto inicial")
                        ->setCellValue("K$row", "Adiciones")
                        ->setCellValue("L$row", "Reducciones")
                        ->setCellValue("M$row", "Creditos")
                        ->setCellValue("N$row", "Contracreditos")
                        ->setCellValue("O$row", "Definitivo")
                        ->setCellValue("P$row", "Disponibilidades")
                        ->setCellValue("Q$row", "Compromisos")
                        ->setCellValue("R$row", "Saldos por comprometer")
                        ->setCellValue("S$row", "Obligaciones")
                        ->setCellValue("T$row", "Compromisos en ejecución")
                        ->setCellValue("U$row", "Pagos")
                        ->setCellValue("V$row", "Cuentas por pagar")
                        ->setCellValue("W$row", "Saldos por disponer");
                        $objPHPExcel-> getActiveSheet ()
                        -> getStyle ("A$row:W$row")
                        -> getFill ()
                        -> setFillType (PHPExcel_Style_Fill :: FILL_SOLID)
                        -> getStartColor ()
                        -> setRGB ('3399cc');
                        $objPHPExcel-> getActiveSheet () -> getStyle ("A$row:W$row") -> getAlignment () -> applyFromArray (array ( 'horizontal'  =>  PHPExcel_Style_Alignment :: HORIZONTAL_CENTER ,) );
                        $objPHPExcel->getActiveSheet()->getStyle("A$row:W$row")->getFont()->setBold(true);
                        $objPHPExcel->getActiveSheet()->getStyle("A$row:W$row")->applyFromArray($borders);
                        $objPHPExcel->getActiveSheet()->getStyle("A$row:W$row")->getFont()->getColor()->setRGB("ffffff");
                        $row++;

                        //Total
                        $saldosPorComprometer = round($arbol['total']['total_definitivo']-$arbol['total']['total_compromisos'], 2);
                        $compromisosEnEjecucion = round($arbol['total']['total_compromisos']-$arbol['total']['total_obligacion'], 2);
                        $cuentasPorPagar = round($arbol['total']['total_obligacion']-$arbol['total']['total_egreso'], 2);
                        $objPHPExcel->setActiveSheetIndex(0)
                        ->setCellValue("A$row", "")
                        ->setCellValue("B$row", "")
                        ->setCellValue("C$row", "")
                        ->setCellValue("D$row", "Total")
                        ->setCellValue("E$row", "Total gastos")
                        ->setCellValue("F$row", "")
                        ->setCellValue("G$row", "")
                        ->setCellValue("H$row", "")
                        ->setCellValue("I$row", "")
                        ->setCellValue("J$row", formatNum($arbol['total']['total_inicial']))
                        ->setCellValue("K$row", formatNum($arbol['total']['total_adicion']))
                        ->setCellValue("L$row", formatNum($arbol['total']['total_reduccion']))
                        ->setCellValue("M$row", formatNum($arbol['total']['total_credito']))
                        ->setCellValue("N$row", formatNum($arbol['total']['total_contracredito']))
                        ->setCellValue("O$row", formatNum($arbol['total']['total_definitivo']))
                        ->setCellValue("P$row", formatNum($arbol['total']['total_disponibilidad']))
                        ->setCellValue("Q$row", formatNum($arbol['total']['total_compromisos']))
                        ->setCellValue("R$row", formatNum($saldosPorComprometer))
                        ->setCellValue("S$row", formatNum($arbol['total']['total_obligacion']))
                        ->setCellValue("T$row", formatNum($compromisosEnEjecucion))
                        ->setCellValue("U$row", formatNum($arbol['total']['total_egreso']))
                        ->setCellValue("V$row", formatNum($cuentasPorPagar))
                        ->setCellValue("W$row", formatNum($arbol['total']['total_saldo']));
                        $objPHPExcel->getActiveSheet()->getStyle("A$row:W$row")->getFont()->setBold(true);
                        $objPHPExcel->getActiveSheet()->getStyle("A$row:W$row")->applyFromArray($borders);
                        $objPHPExcel-> getActiveSheet () -> getStyle ("J$row:W$row") -> getAlignment () -> applyFromArray (array ( 'horizontal'  =>  PHPExcel_Style_Alignment :: HORIZONTAL_RIGHT ,) );
                        $objPHPExcel-> getActiveSheet ()
                        -> getStyle ("A$row:W$row")
                        -> getFill ()
                        -> setFillType (PHPExcel_Style_Fill :: FILL_SOLID)
                        -> getStartColor ()
                        -> setRGB ('99ddff');
                        $row++;

                        //Total gastos
                        $saldosPorComprometer = round($arbol['funcionamiento']['total_definitivo']-$arbol['funcionamiento']['total_compromisos'], 2);
                        $compromisosEnEjecucion = round($arbol['funcionamiento']['total_compromisos']-$arbol['funcionamiento']['total_obligacion'], 2);
                        $cuentasPorPagar = round($arbol['funcionamiento']['total_obligacion']-$arbol['funcionamiento']['total_egreso'], 2);
                        $objPHPExcel->setActiveSheetIndex(0)
                        ->setCellValue("A$row", "")
                        ->setCellValue("B$row", "")
                        ->setCellValue("C$row", "")
                        ->setCellValue("D$row", "Total")
                        ->setCellValue("E$row", "Total Gastos de Funcionamiento, servicio de la deuda")
                        ->setCellValue("F$row", "")
                        ->setCellValue("G$row", "")
                        ->setCellValue("H$row", "")
                        ->setCellValue("I$row", "")
                        ->setCellValue("J$row", formatNum($arbol['funcionamiento']['total_inicial']))
                        ->setCellValue("K$row", formatNum($arbol['funcionamiento']['total_adicion']))
                        ->setCellValue("L$row", formatNum($arbol['funcionamiento']['total_reduccion']))
                        ->setCellValue("M$row", formatNum($arbol['funcionamiento']['total_credito']))
                        ->setCellValue("N$row", formatNum($arbol['funcionamiento']['total_contracredito']))
                        ->setCellValue("O$row", formatNum($arbol['funcionamiento']['total_definitivo']))
                        ->setCellValue("P$row", formatNum($arbol['funcionamiento']['total_disponibilidad']))
                        ->setCellValue("Q$row", formatNum($arbol['funcionamiento']['total_compromisos']))
                        ->setCellValue("R$row", formatNum($saldosPorComprometer))
                        ->setCellValue("S$row", formatNum($arbol['funcionamiento']['total_obligacion']))
                        ->setCellValue("T$row", formatNum($compromisosEnEjecucion))
                        ->setCellValue("U$row", formatNum($arbol['funcionamiento']['total_egreso']))
                        ->setCellValue("V$row", formatNum($cuentasPorPagar))
                        ->setCellValue("W$row", formatNum($arbol['funcionamiento']['total_saldo']));
                        $objPHPExcel->getActiveSheet()->getStyle("A$row:W$row")->getFont()->setBold(true);
                        $objPHPExcel->getActiveSheet()->getStyle("A$row:W$row")->applyFromArray($borders);
                        $objPHPExcel-> getActiveSheet () -> getStyle ("J$row:W$row") -> getAlignment () -> applyFromArray (array ( 'horizontal'  =>  PHPExcel_Style_Alignment :: HORIZONTAL_RIGHT ,) );
                        $objPHPExcel-> getActiveSheet ()
                        -> getStyle ("A$row:W$row")
                        -> getFill ()
                        -> setFillType (PHPExcel_Style_Fill :: FILL_SOLID)
                        -> getStartColor ()
                        -> setRGB ('99ddff');
                        $row++;

                        //Total inversion
                        $saldosPorComprometer = round($arbol['total']['total_definitivo']-$arbol['total']['total_compromisos'], 2);
                        $compromisosEnEjecucion = round($arbol['total']['total_compromisos']-$arbol['total']['total_obligacion'], 2);
                        $cuentasPorPagar = round($arbol['total']['total_obligacion']-$arbol['total']['total_egreso'], 2);
                        $objPHPExcel->setActiveSheetIndex(0)
                        ->setCellValue("A$row", "")
                        ->setCellValue("B$row", "")
                        ->setCellValue("C$row", "")
                        ->setCellValue("D$row", "Total")
                        ->setCellValue("E$row", "Total Gastos de Inversion")
                        ->setCellValue("F$row", "")
                        ->setCellValue("G$row", "")
                        ->setCellValue("H$row", "")
                        ->setCellValue("I$row", "")
                        ->setCellValue("J$row", formatNum($arbol['total']['total_inicial']))
                        ->setCellValue("K$row", formatNum($arbol['total']['total_adicion']))
                        ->setCellValue("L$row", formatNum($arbol['total']['total_reduccion']))
                        ->setCellValue("M$row", formatNum($arbol['total']['total_credito']))
                        ->setCellValue("N$row", formatNum($arbol['total']['total_contracredito']))
                        ->setCellValue("O$row", formatNum($arbol['total']['total_definitivo']))
                        ->setCellValue("P$row", formatNum($arbol['total']['total_disponibilidad']))
                        ->setCellValue("Q$row", formatNum($arbol['total']['total_compromisos']))
                        ->setCellValue("R$row", formatNum($saldosPorComprometer))
                        ->setCellValue("S$row", formatNum($arbol['total']['total_obligacion']))
                        ->setCellValue("T$row", formatNum($compromisosEnEjecucion))
                        ->setCellValue("U$row", formatNum($arbol['total']['total_egreso']))
                        ->setCellValue("V$row", formatNum($cuentasPorPagar))
                        ->setCellValue("W$row", formatNum($arbol['total']['total_saldo']));
                        $objPHPExcel->getActiveSheet()->getStyle("A$row:W$row")->getFont()->setBold(true);
                        $objPHPExcel->getActiveSheet()->getStyle("A$row:W$row")->applyFromArray($borders);
                        $objPHPExcel-> getActiveSheet () -> getStyle ("J$row:W$row") -> getAlignment () -> applyFromArray (array ( 'horizontal'  =>  PHPExcel_Style_Alignment :: HORIZONTAL_RIGHT ,) );
                        $objPHPExcel-> getActiveSheet ()
                        -> getStyle ("A$row:W$row")
                        -> getFill ()
                        -> setFillType (PHPExcel_Style_Fill :: FILL_SOLID)
                        -> getStartColor ()
                        -> setRGB ('99ddff');
                        $row++;
                        foreach ($arbol['info'] as $info) {
                            $saldosPorComprometer = round($info['total_definitivo']-$info['total_compromisos'], 2);
                            $compromisosEnEjecucion = round($info['total_compromisos']-$info['total_obligacion'], 2);
                            $cuentasPorPagar = round($info['total_obligacion']-$info['total_egreso'], 2);
                            $nombreNivel = isset($info['tipo_nombre']) != "" ? $info['tipo_nombre'].": ".$info['nombre'] : $info['nombre'];
                            $objPHPExcel->setActiveSheetIndex(0)
                            ->setCellValue("A$row", "")
                            ->setCellValue("B$row", "")
                            ->setCellValue("C$row", "")
                            ->setCellValue("D$row", $info['tipo_nombre'] == "Subprograma" ? $info['codigo_subprograma'] : $info['codigo'])
                            ->setCellValue("E$row", $nombreNivel)
                            ->setCellValue("F$row", $info['tipo'])
                            ->setCellValue("G$row", "")
                            ->setCellValue("H$row", "")
                            ->setCellValue("I$row", "")
                            ->setCellValue("J$row", formatNum($info['total_inicial']))
                            ->setCellValue("K$row", formatNum($info['total_adicion']))
                            ->setCellValue("L$row", formatNum($info['total_reduccion']))
                            ->setCellValue("M$row", formatNum($info['total_credito']))
                            ->setCellValue("N$row", formatNum($info['total_contracredito']))
                            ->setCellValue("O$row", formatNum($info['total_definitivo']))
                            ->setCellValue("P$row", formatNum($info['total_disponibilidad']))
                            ->setCellValue("Q$row", formatNum($info['total_compromisos']))
                            ->setCellValue("R$row", formatNum($saldosPorComprometer))
                            ->setCellValue("S$row", formatNum($info['total_obligacion']))
                            ->setCellValue("T$row", formatNum($compromisosEnEjecucion))
                            ->setCellValue("U$row", formatNum($info['total_egreso']))
                            ->setCellValue("V$row", formatNum($cuentasPorPagar))
                            ->setCellValue("W$row", formatNum($info['total_saldo']));
                            $objPHPExcel->getActiveSheet()->getStyle("A$row:W$row")->getFont()->setBold(true);
                            $objPHPExcel->getActiveSheet()->getStyle("A$row:W$row")->applyFromArray($borders);
                            $objPHPExcel-> getActiveSheet () -> getStyle ("F$row") -> getAlignment () -> applyFromArray (array ( 'horizontal'  =>  PHPExcel_Style_Alignment :: HORIZONTAL_CENTER ,) );
                            $objPHPExcel-> getActiveSheet () -> getStyle ("J$row:W$row") -> getAlignment () -> applyFromArray (array ( 'horizontal'  =>  PHPExcel_Style_Alignment :: HORIZONTAL_RIGHT ,) );
                            $objPHPExcel-> getActiveSheet ()
                            -> getStyle ("A$row:W$row")
                            -> getFill ()
                            -> setFillType (PHPExcel_Style_Fill :: FILL_SOLID)
                            -> getStartColor ()
                            -> setRGB ('d1f0ff');
                            $row++;
                            if(!empty($info['detalle'])){
                                foreach ($info['detalle'] as $det) {
                                    $saldosPorComprometer = round($det['definitivo']-$det['compromisos'], 2);
                                    $compromisosEnEjecucion = round($det['compromisos']-$det['obligacion'], 2);
                                    $cuentasPorPagar = round($det['obligacion']-$det['egreso'], 2);
                                    $objPHPExcel->setActiveSheetIndex(0)
                                    ->setCellValue("A$row", $det['codigo_unidad']." - ".$det['nombre_unidad'])
                                    ->setCellValue("B$row", $det['codigo_seccion'])
                                    ->setCellValue("C$row", $det['codigo_vigencia'])
                                    ->setCellValue("D$row", $det['codigo_cuenta'])
                                    ->setCellValue("E$row", $det['nombre_cuenta'])
                                    ->setCellValue("F$row", $det['tipo'])
                                    ->setCellValue("G$row", $det['codigo_fuente'])
                                    ->setCellValue("H$row", $det['nombre_fuente'])
                                    ->setCellValue("I$row", $det['codigo_pago'])
                                    ->setCellValue("J$row", formatNum($det['inicial']))
                                    ->setCellValue("K$row", formatNum($det['adicion']))
                                    ->setCellValue("L$row", formatNum($det['reduccion']))
                                    ->setCellValue("M$row", formatNum($det['credito']))
                                    ->setCellValue("N$row", formatNum($det['contracredito']))
                                    ->setCellValue("O$row", formatNum($det['definitivo']))
                                    ->setCellValue("P$row", formatNum($det['disponibilidad']))
                                    ->setCellValue("Q$row", formatNum($det['compromisos']))
                                    ->setCellValue("R$row", formatNum($saldosPorComprometer))
                                    ->setCellValue("S$row", formatNum($det['obligacion']))
                                    ->setCellValue("T$row", formatNum($compromisosEnEjecucion))
                                    ->setCellValue("U$row", formatNum($det['egreso']))
                                    ->setCellValue("V$row", formatNum($cuentasPorPagar))
                                    ->setCellValue("W$row", formatNum($det['saldo']));
                                    $objPHPExcel->getActiveSheet()->getStyle("A$row:W$row")->applyFromArray($borders);
                                    $objPHPExcel-> getActiveSheet () -> getStyle ("B$row:C$row") -> getAlignment () -> applyFromArray (array ( 'horizontal'  =>  PHPExcel_Style_Alignment :: HORIZONTAL_CENTER ,) );
                                    $objPHPExcel-> getActiveSheet () -> getStyle ("F$row") -> getAlignment () -> applyFromArray (array ( 'horizontal'  =>  PHPExcel_Style_Alignment :: HORIZONTAL_CENTER ,) );
                                    $objPHPExcel-> getActiveSheet () -> getStyle ("J$row:W$row") -> getAlignment () -> applyFromArray (array ( 'horizontal'  =>  PHPExcel_Style_Alignment :: HORIZONTAL_RIGHT ,) );
                                    if(round($det['saldo']) < 0 || round($det['disponibilidad']) < round($det['compromisos']) ||
                                        round($det['compromisos']) < round($det['obligacion']) || round($det['obligacion']) < round($det['egreso'])
                                        ){
                                            $objPHPExcel->getActiveSheet()->getStyle("A$row:W$row")->getFont()->setBold(true);"bg-warning fw-bold";
                                            $objPHPExcel-> getActiveSheet ()
                                            -> getStyle ("A$row:W$row")
                                            -> getFill ()
                                            -> setFillType (PHPExcel_Style_Fill :: FILL_SOLID)
                                            -> getStartColor ()
                                            -> setRGB ('f8ba00');
                                        }
                                    $row++;
                                }
                            }
                        }
                    }

                    $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
                    $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
                    $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
                    $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
                    $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(80);
                    $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
                    $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(10);
                    $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(50);
                    $objPHPExcel->getActiveSheet()->getColumnDimension('I')->setAutoSize(true);
                    $objPHPExcel->getActiveSheet()->getColumnDimension('J')->setAutoSize(true);
                    $objPHPExcel->getActiveSheet()->getColumnDimension('K')->setAutoSize(true);
                    $objPHPExcel->getActiveSheet()->getColumnDimension('L')->setAutoSize(true);
                    $objPHPExcel->getActiveSheet()->getColumnDimension('M')->setAutoSize(true);
                    $objPHPExcel->getActiveSheet()->getColumnDimension('N')->setAutoSize(true);
                    $objPHPExcel->getActiveSheet()->getColumnDimension('O')->setAutoSize(true);
                    $objPHPExcel->getActiveSheet()->getColumnDimension('P')->setAutoSize(true);
                    $objPHPExcel->getActiveSheet()->getColumnDimension('Q')->setAutoSize(true);
                    $objPHPExcel->getActiveSheet()->getColumnDimension('R')->setAutoSize(true);
                    $objPHPExcel->getActiveSheet()->getColumnDimension('S')->setAutoSize(true);
                    $objPHPExcel->getActiveSheet()->getColumnDimension('T')->setAutoSize(true);
                    $objPHPExcel->getActiveSheet()->getColumnDimension('U')->setAutoSize(true);
                    $objPHPExcel->getActiveSheet()->getColumnDimension('V')->setAutoSize(true);
                    $objPHPExcel->getActiveSheet()->getColumnDimension('W')->setAutoSize(true);
                    //----Guardar documento----
                    header('Content-Type: application/vnd.ms-excel');
                    header('Content-Disposition: attachment;filename="informe_ejecucion_gastos.xlsx"');
                    header('Cache-Control: max-age=0');
                    $objWriter=PHPExcel_IOFactory::createWriter($objPHPExcel,'Excel2007');
                    $objWriter->save('php://output');
                }
            }
            die();
        }

    }

    if($_POST){
        $obj = new EjecucionExportControler();
        if($_POST['action'] == "excel"){
            $obj->exportExcel();
        }
    }

?>
