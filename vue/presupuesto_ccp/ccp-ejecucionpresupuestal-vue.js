var app = new Vue({
    el: '#myapp',
    components: { Multiselect: window.VueMultiselect.default },
	data:{

        jsonData: [{
            "Nombre": " ",
            "Telfono de contacto": "021-33829544",
            "Direccin de la casa": "1706 Wulian Road, Jinqiao Town, Pudong New Area"
        }],

        mostrarEjecucion: false,
        loading: false,
        seccionesPresupuestales: [],
        selectSeccion: '-1',
        optionsMediosPagos: [
			{ text: 'Todos', value: '-1' },
			{ text: 'CSF', value: 'CSF' },
			{ text: 'SSF', value: 'SSF' }
		],
        selectMedioPago: '-1',
        vigenciasdelgasto: [],
        selectVigenciaGasto: '1',
        years: [],
        vigencia: '',

        clasificadores: [],
        selectClasificador: '-1',

        arbol : [],
        arbolFuncionamiento : [],
        arbolInversion : [],
        presupuestoInicial: [],

        nombreSectores: [],
        nombreProgramas: [],
        nombreSubProgramas: [],
        nombreProductos: [],
        nombreProgramatico: [],

        cuentasCcpetUsadas: [],
        cuentasCcpetUsadasProy: [],
        cuentasCcpetUsadasAd: [],
        cuentasCcpetUsadasRed: [],
        cuentasCcpetUsadasCred: [],
        cuentasCcpetUsadasContraCred: [],
        cuentasCcpet: [],
        cuentasCcpetUsadasCdp: [],
        cuentasCcpetUsadasCdpInv: [],
        cuentasCcpetUsadasRpInv: [],
        cuentasCcpetUsadasCxpInv: [],
        cuentasCcpetUsadasEgresoInv: [],
        cuentasCcpetUsadasRp: [],
        cuentasCcpetUsadasCxp: [],
        cuentasCcpetUsadasEgreso: [],
        cuentasCcpetUsadasIniInv: [],
        cuentasCcpetUsadasAdInv: [],
        cuentasCcpetUsadasRedInv: [],
        cuentasCcpetUsadasTrasladoCredInv: [],
        cuentasCcpetUsadasTrasladoContraCredInv: [],

        sumaTotalGastos: [],
        sumaTotalGastosFuncionamiento: [],
        sumaTotalGastosInversion: [],

        indicadorProductoTam: 0,
        productoTam: 0,
        results_det: [],
        cuentaMaxLargo: '',
        fuentes: [],
        valueFuentes: [],
        optionsFuentes: [],
        fuentesUsadas: [],

        tiposDeGasto: [],
        tipoGasto: '-1',

        unidadesEjecutoras: [],
        selectUnidad: '-1',

        basesApp: [],
    },

    computed:{
        existeTotalGastosFun(){
            return this.sumaTotalGastosFuncionamiento[0][12] > 0 ? true : false
        },

        existeTotalGastosInv(){
            return this.sumaTotalGastosInversion[0][12] > 0 ? true : false
        },
    },

    mounted: async function(){
        await this.cargayears();
        await this.uniEjecutorasSearch();
        this.seccionPresupuestal();
        this.vigenciasDelgasto();
        this.clasificadoresPresupuestales();
        this.fuentesOpciones();
        this.cargarTiposDeGasto();
        document.getElementById('fc_1198971545').value = '01'+'/'+'01'+'/'+this.vigencia;
        let today = new Date();

        // `getDate()` devuelve el día del mes (del 1 al 31)
        let day = today.getDate();

        // `getMonth()` devuelve el mes (de 0 a 11)
        let month = today.getMonth() + 1;


        // `getFullYear()` devuelve el año completo
        let year = today.getFullYear();

        // muestra la fecha de hoy en formato `MM/DD/YYYY`

        let dayFormat = day.toString().padStart(2, '0');
        let monthFormat = month.toString().padStart(2, '0');

        document.getElementById('fc_1198971546').value = `${dayFormat}/${monthFormat}/${year}`;

    },

    methods: {

        cargarTiposDeGasto: async function(){

            await axios.post('vue/presupuesto_ccp/ccp-cdpbasico.php?action=cargarTiposDeGasto')
            .then((response) => {
                app.tiposDeGasto = response.data.tiposDeGasto;

                /* if(response.data.tiposDeGasto.length > 0){
                    this.tipoGasto = response.data.tiposDeGasto[0][0];
                } */
            });
        },

        uniEjecutorasSearch: async function(){

            await axios.post('presupuesto_ccpet/adicion/crear/ccp-adicion.php?action=unidades')
            .then(
                (response)=>{

                    this.unidadesEjecutoras = response.data.uniEjecutoras;
                    this.selectUnidad = this.unidadesEjecutoras[0][0];
                }
            );
            this.bases();
        },

        bases(){
            const basesUnidad = this.unidadesEjecutoras.map(uniEjecutora => {
                return  {
                            base: uniEjecutora[2],
                            usuario: uniEjecutora[6],
                            unidad: uniEjecutora[1],
                            nombre: uniEjecutora[3].toLowerCase(),
                            tipo: uniEjecutora[4],
                            id: uniEjecutora[0],
                        };
            });
            this.basesApp = basesUnidad;
        },

        cambiaCriteriosBusqueda: function(){
            this.arbol = [];
            this.arbolFuncionamiento = [];
            this.arbolInversion = [];
            this.presupuestoInicial = [];
            this.cuentasCcpetUsadas = [];
            this.cuentasCcpetUsadasProy = [];
            this.cuentasCcpetUsadasAd = [];
            this.cuentasCcpetUsadasRed = [];
            this.cuentasCcpetUsadasCred = [];
            this.cuentasCcpetUsadasContraCred = [];
            this.cuentasCcpet = [];
            this.cuentasCcpetUsadasCdp = [];
            this.cuentasCcpetUsadasCdpInv = [];
            this.cuentasCcpetUsadasRpInv = [];
            this.cuentasCcpetUsadasCxpInv = [];
            this.cuentasCcpetUsadasEgresoInv = [];
            this.cuentasCcpetUsadasRp = [];
            this.cuentasCcpetUsadasCxp = [];
            this.cuentasCcpetUsadasEgreso = [];
            this.cuentasCcpetUsadasIniInv = [];
            this.cuentasCcpetUsadasAdInv = [];
            this.cuentasCcpetUsadasRedInv = [];
            this.cuentasCcpetUsadasTrasladoCredInv = [];
            this.cuentasCcpetUsadasTrasladoContraCredInv = [];
            this.mostrarEjecucion = false;

            this.sumaTotalGastos = [];
            this.sumaTotalGastosFuncionamiento = [];
            this.sumaTotalGastosInversion = [];

        },

        generarEjecucion: async function(){

            await this.fuentesOpciones();
            this.cargaPresuInicial();

        },



        cargaPresuInicial: async function(){

            this.arbol = [];
            this.arbolFuncionamiento = [];
            this.arbolInversion = [];
            this.presupuestoInicial = [];
            this.cuentasCcpetUsadas = [];
            this.cuentasCcpetUsadasProy = [];
            this.cuentasCcpetUsadasAd = [];
            this.cuentasCcpetUsadasRed = [];
            this.cuentasCcpetUsadasCred = [];
            this.cuentasCcpetUsadasContraCred = [];
            this.cuentasCcpet = [];
            this.cuentasCcpetUsadasCdp = [];
            this.cuentasCcpetUsadasCdpInv = [];
            this.cuentasCcpetUsadasRpInv = [];
            this.cuentasCcpetUsadasCxpInv = [];
            this.cuentasCcpetUsadasEgresoInv = [];
            this.cuentasCcpetUsadasRp = [];
            this.cuentasCcpetUsadasCxp = [];
            this.cuentasCcpetUsadasEgreso = [];
            this.cuentasCcpetUsadasIniInv = [];
            this.cuentasCcpetUsadasAdInv = [];
            this.cuentasCcpetUsadasRedInv = [];
            this.cuentasCcpetUsadasTrasladoCredInv = [];
            this.cuentasCcpetUsadasTrasladoContraCredInv = [];

            this.sumaTotalGastos = [];
            this.sumaTotalGastosFuncionamiento = [];
            this.sumaTotalGastosInversion = [];

            var formData = new FormData();

            this.valueFuentes.forEach((e, i) => {
                formData.append("fuentes["+i+"]", e.fuente);
            });


            formData.append("seccionPresupuestal", this.selectSeccion);
            formData.append("medioDePago", this.selectMedioPago);
            formData.append("vigenciaDelGasto", this.selectVigenciaGasto);
            formData.append("vigencia", this.vigencia);
            formData.append("tipoGasto", this.tipoGasto);
            let fechaIni = document.getElementById('fc_1198971545').value;
            let fechaFin = document.getElementById('fc_1198971546').value;
            let fechaInicial = '';
            let fechaFinal = '';

            if(fechaIni != undefined && fechaIni != '' && fechaFin != undefined && fechaFin != ''){

                this.loading =  true;

                let fecI = fechaIni.split('/');
                let fecF = fechaFin.split('/');
                fechaInicial = `${fecI[2]}-${fecI[1]}-${fecI[0]}`;
                fechaFinal = `${fecF[2]}-${fecF[1]}-${fecF[0]}`;

                formData.append("fechaIni", fechaInicial);
                formData.append("fechaFin", fechaFinal);
                formData.append("vigencia_ini", fecI[2]);
                formData.append("vigencia_fin", fecF[2]);

                if(this.selectUnidad == -1){
                    this.basesApp.map((b, i) => {
                        formData.append("basesApp["+i+"][]", Object.values(b));
                    })
                }else{

                    formData.append("basesApp[][]", Object.values(this.basesApp[this.selectUnidad-1]));
                }

                await axios.post('vue/presupuesto_ccp/ccp-ejecucionpresupuestal-vue.php?action=presupuestoInicial',
                formData)
                .then(
                    (response)=>{

                        app.cuentasCcpet = response.data.cuentasCcpet;
                        app.cuentasCcpetUsadas = response.data.cuentasCcpetUsadas;
                        app.cuentasCcpetUsadasAd = response.data.cuentasCcpetUsadasAd;
                        app.cuentasCcpetUsadasRed = response.data.cuentasCcpetUsadasRed;
                        app.cuentasCcpetUsadasCred = response.data.cuentasCcpetUsadasCred;
                        app.cuentasCcpetUsadasContraCred = response.data.cuentasCcpetUsadasContraCred;
                        app.cuentasCcpetUsadasProy = response.data.cuentasCcpetUsadasProy;
                        app.nombreSectores = response.data.nombreSectores;
                        app.nombreProgramas = response.data.nombreProgramas;
                        app.nombreSubProgramas = response.data.nombreSubProgramas;
                        app.nombreProductos = response.data.nombreProductos;
                        app.nombreProgramatico = response.data.nombreProgramatico;
                        app.cuentasCcpetUsadasCdp = response.data.cuentasCcpetUsadasCdp;
                        app.cuentasCcpetUsadasCdpInv = response.data.cuentasCcpetUsadasCdpInv;
                        app.cuentasCcpetUsadasRpInv = response.data.cuentasCcpetUsadasRpInv;
                        app.cuentasCcpetUsadasCxpInv = response.data.cuentasCcpetUsadasCxpInv;
                        app.cuentasCcpetUsadasEgresoInv = response.data.cuentasCcpetUsadasEgresoInv;
                        app.cuentasCcpetUsadasRp = response.data.cuentasCcpetUsadasRp;
                        app.cuentasCcpetUsadasCxp = response.data.cuentasCcpetUsadasCxp;
                        app.cuentasCcpetUsadasEgreso = response.data.cuentasCcpetUsadasEgreso;
                        app.cuentasCcpetUsadasIniInv = response.data.cuentasCcpetUsadasIniInv;
                        app.cuentasCcpetUsadasAdInv = response.data.cuentasCcpetUsadasAdInv;
                        app.cuentasCcpetUsadasRedInv = response.data.cuentasCcpetUsadasRedInv;
                        app.cuentasCcpetUsadasTrasladoCredInv = response.data.cuentasCcpetUsadasTrasladoCredInv;
                        app.cuentasCcpetUsadasTrasladoContraCredInv = response.data.cuentasCcpetUsadasTrasladoContraCredInv;
                        app.productoTam = response.data.productoTam[0];
                        app.indicadorProductoTam = response.data.productoTam[1];
                        app.cuentaMaxLargo = response.data.cuentaMaxLargo;

                    }
                ).finally(() => {
                    this.mostrarEjecucion = true;
                    this.loading =  false;
                });

                await this.buscarCuentasClasificador();
                this.cuentasMayoresAux();

            }else{

                Swal.fire(
                    'Falta escoger fechas.',
                    'Escoja la fecha inicial y fecha final',
                    'warning'
                    )

            }

        },

        async buscarCuentasClasificador(){
            if(this.results_det.length > 0){
                this.cuentasCcpetUsadas = await this.filtrarPorClasificador(this.cuentasCcpetUsadas);
                this.cuentasCcpetUsadasAd = await this.filtrarPorClasificador(this.cuentasCcpetUsadasAd);
                this.cuentasCcpetUsadasRed = await this.filtrarPorClasificador(this.cuentasCcpetUsadasRed);
                this.cuentasCcpetUsadasCred = await this.filtrarPorClasificador(this.cuentasCcpetUsadasCred);
                this.cuentasCcpetUsadasContraCred = await this.filtrarPorClasificador(this.cuentasCcpetUsadasContraCred);
                this.cuentasCcpetUsadasProy = await this.filtrarPorClasificadorInv(this.cuentasCcpetUsadasProy);
                this.cuentasCcpetUsadasCdp = await this.filtrarPorClasificador(this.cuentasCcpetUsadasCdp);
                this.cuentasCcpetUsadasCdpInv = await this.filtrarPorClasificador(this.cuentasCcpetUsadasCdpInv);
                this.cuentasCcpetUsadasRpInv = await this.filtrarPorClasificador(this.cuentasCcpetUsadasRpInv);
                this.cuentasCcpetUsadasCxpInv = await this.filtrarPorClasificador(this.cuentasCcpetUsadasCxpInv);
                this.cuentasCcpetUsadasEgresoInv = await this.filtrarPorClasificador(this.cuentasCcpetUsadasEgresoInv);
                this.cuentasCcpetUsadasRp = await this.filtrarPorClasificador(this.cuentasCcpetUsadasRp);
                this.cuentasCcpetUsadasCxp = await this.filtrarPorClasificador(this.cuentasCcpetUsadasCxp);
                this.cuentasCcpetUsadasEgreso = await this.filtrarPorClasificador(this.cuentasCcpetUsadasEgreso);
                this.cuentasCcpetUsadasIniInv = await this.filtrarPorClasificador(this.cuentasCcpetUsadasIniInv);
                this.cuentasCcpetUsadasAdInv = await this.filtrarPorClasificador(this.cuentasCcpetUsadasAdInv);
                this.cuentasCcpetUsadasRedInv = await this.filtrarPorClasificador(this.cuentasCcpetUsadasRedInv);
                this.cuentasCcpetUsadasTrasladoCredInv = await this.filtrarPorClasificador(this.cuentasCcpetUsadasTrasladoCredInv);
                this.cuentasCcpetUsadasTrasladoContraCredInv = await this.filtrarPorClasificador(this.cuentasCcpetUsadasTrasladoContraCredInv);
            }

        },

        seccionPresupuestal: async function(){
            var formData = new FormData();

            if(this.selectUnidad == -1){
                this.seccionesPresupuestales = [];
            }else{
                formData.append("base", this.unidadesEjecutoras[this.selectUnidad-1][2]);
                formData.append("usuario", this.unidadesEjecutoras[this.selectUnidad-1][6]);

                await axios.post('vue/presupuesto_ccp/ccp-inicialgastosfun.php?action=secciones', formData)
                .then(
                    (response)=>{
                        this.seccionesPresupuestales = response.data.secciones;
                    }
                );
            }

            this.cambiaCriteriosBusqueda();
        },

        vigenciasDelgasto: async function(){
            await axios.post('vue/presupuesto_ccp/ccp-inicialgastosfun.php?action=vigenciasDelGasto')
                .then(
                    (response) => {
                        this.vigenciasdelgasto = response.data.vigenciasDelGasto;
                        /* this.selectVigenciaGasto = this.vigenciasdelgasto[0][0]; */
                    }
                );
        },

        async fuentesOpciones(){
            let formDataDet = new FormData();
            let fechaIni = document.getElementById('fc_1198971545').value;

            let fechaPartes = fechaIni.split("/");
            let vigFuentes = !fechaIni ? this.vigencia : fechaPartes[2];

            formDataDet.append("vigencia", vigFuentes)
            await axios.post('vue/presupuesto_ccp/ccp-ejecucionpresupuestal-vue.php?action=fuentes', formDataDet)
                .then(
                    (response) => {
                        this.fuentes = response.data.fuentes;
                        this.fuentesUsadas = response.data.fuentesUsadas;
                        /* this.vigenciasdelgasto = response.data.vigenciasDelGasto; */
                        /* this.selectVigenciaGasto = this.vigenciasdelgasto[0][0]; */
                    }
                );
            const dataArrSet = new Set(this.fuentesUsadas);
            const dataArr = Array.from(dataArrSet);
            const fuentesOrg = await dataArr.map((e) => {
                return {fuente : e, nombre : this.buscarFuente(e)}
            });
            this.optionsFuentes = fuentesOrg;
        },

        fuenteConNombre({ fuente, nombre }){
            return `${fuente} - ${nombre}`
        },

        clasificadoresPresupuestales: async function(){
            await axios.post('presupuesto_ccpet/clasificadores/ccp-buscaclasificadoresgastos.php')
                .then(
                    (response) => {
                        this.clasificadores = response.data.clasificadores;
                        /* this.selectVigenciaGasto = this.vigenciasdelgasto[0][0]; */
                    }
                );
        },

        async cuentasDelClasificador(){

            this.results_det = [];
            let formDataDet = new FormData();
            formDataDet.append("id_clasificador", this.selectClasificador)
            await axios.post('presupuesto_ccpet/clasificadores/ccp-buscaclasificadoresgastos.php?action=search_clasificador_det_aux', formDataDet)
                .then(
                    (response) => {
                        this.results_det = response.data.clasificadores_det;
                        /* this.selectVigenciaGasto = this.vigenciasdelgasto[0][0]; */
                    }
                );
            this.cambiaCriteriosBusqueda();
        },

        async filtrarPorClasificador(filtrarRubros){

            const filtro = await filtrarRubros.map((element) => {
                /* let largeAccount = element[0].length; */
                let a = (this.results_det.find((e) => {
                    if(e.length === 3){
                        return (e[0] == element[4] && e[1] == element[1] && e[2] == element[2])
                    }else{
                        return (e[0] == element[0] && e[1] == element[1])
                    }

                }));
                return a != undefined ? element : ''
            });
            const resp = filtro.filter((element) => element != '');
            return resp;
        },

        async filtrarPorClasificadorInv(filtrarRubros){

            const filtro = await filtrarRubros.map((element) => {
                /* let largeAccount = element[0].length; */
                let a = (this.results_det.find((e) => {
                    if(e.length === 3){
                        return (e[0] == element[6] && e[1] == element[9] && e[2] == element[11])
                    }else{
                        return (e[0] == element[0] && e[1] == element[1])
                    }

                }));
                return a != undefined ? element : ''
            });
            const resp = filtro.filter((element) => element != '');
            return resp;
        },

        cargayears: async function(){
			await axios.post('vue/presupuesto_ccp/ccp-bancoproyectos.php?buscar=years')
			.then(
				(response)=>
				{
					this.years=response.data.anio;
					var idanio=response.data.anio.length -1;
					if (idanio>=0){this.vigencia = response.data.anio[idanio][0];}
					else{this.vigencia ='';}
				}
            );

		},

        buscarCuentas: async function(){

            await axios.post('vue/presupuesto_ccp/ccp-ejecucionpresupuestal-vue.php?vigencia='+this.vigencia)
            .then(function(response){


            });

        },

        buscarFuente(fuente){
            const nombFuente = this.fuentes.find(e => e[0] == fuente)
            const nombF = typeof nombFuente == 'object' ? Object.values(nombFuente) : ''

            return typeof nombFuente == 'string' ? '' : nombF[1];
        },

        /* quitarPesosYComas: function(valor=''){

            var valorACambiar = '';
            valorACambiar = valor.replace(/[$]+/g, '');
            valorACambiar = valorACambiar.replace(/[,]+/g, '');

            if(valorACambiar == '[object Undefined]'){
                valorACambiar = 0;
            }

            return valorACambiar;
        }, */


        cuentasMayoresAux: function(){

            /* let ordenPorNiveles = [
                { nivel: 10, descontar: 3},
                { nivel: 9, descontar: 3},
                { nivel: 8, descontar: 3},
                { nivel: 7, descontar: 3},
                { nivel: 6, descontar: 4},
                { nivel: 5, descontar: 3},
                { nivel: 4, descontar: 3},
                { nivel: 3, descontar: 2},
                { nivel: 2, descontar: 2},
                { nivel: 1, descontar: 2}
            ]; */

            this.arbolFuncionamiento = [];

            const partsPorNiveles = this.cuentaMaxLargo.split('.');

            const ordenPorNiveles = partsPorNiveles.reverse().map((e,i) => {
                return { nivel: partsPorNiveles.length - i, descontar: e.length + 1}
            });

            this.cuentasCcpetUsadas.forEach(element => {

                var resultado = this.cuentasCcpet.find( cuenta => cuenta[1] === element[0] );

                var niveles = ordenPorNiveles.filter(niv => niv.nivel <= resultado[4]);

                var restante = 0;
                var total = 0;
                niveles.forEach(elementNiv => {
                    var resultArbol = [];
                    var largo =  resultado[1].length-restante;

                    var cuentaABuscar = resultado[1].substring(0, largo);

                    var resultadoBusqueda = this.cuentasCcpet.find( cuenta => cuenta[1] === cuentaABuscar );

                    resultArbol = this.arbolFuncionamiento.find(cuentaArbol => cuentaArbol[0] == resultadoBusqueda[1]);

                    if(resultArbol == undefined){
                        var ramas = [];
                        ramas.push(resultadoBusqueda[1]);
                        ramas.push(resultadoBusqueda[2]);
                        ramas.push(resultadoBusqueda[6]);
                        if(resultadoBusqueda[6] == 'C'){
                            ramas.push(element[1]);
                            ramas.push(element[2]);
                            ramas.push(element[3]);
                            ramas.push(element[4]);
                            ramas.push(element[6]);
                            ramas.push(element[7]);
                            ramas.push(element[5]);


                        }else{

                            largoCuenta = resultadoBusqueda[1].length;

                            //Inicial
                            let arregloSum = this.cuentasCcpetUsadas.filter(elementSum =>
                                (elementSum[0].substring(0, largoCuenta) == resultadoBusqueda[1])
                            );

                            var total = 0;
                            arregloSum.forEach(elementSuma => {
                                //total += elementSuma.reduce((a, b) => a + b);
                                total = parseFloat(total) + parseFloat(elementSuma[5]);
                            });

                            //Traslado Credito

                            let arregloSumCred = this.cuentasCcpetUsadasCred.filter(elementSumCred =>
                                (elementSumCred[0].substring(0, largoCuenta) == resultadoBusqueda[1])
                            );
                            var totalCred = 0;
                            arregloSumCred.forEach(elementSumaCred => {
                                //total += elementSuma.reduce((a, b) => a + b);
                                totalCred = parseFloat(totalCred) + parseFloat(elementSumaCred[5]);
                            });

                            //Traslado ContraCredito

                            let arregloSumContraCred = this.cuentasCcpetUsadasContraCred.filter(elementSumContraCred =>
                                (elementSumContraCred[0].substring(0, largoCuenta) == resultadoBusqueda[1])
                            );
                            var totalContraCred = 0;

                            arregloSumContraCred.forEach(elementSumaContraCred => {
                                //total += elementSuma.reduce((a, b) => a + b);
                                totalContraCred = parseFloat(totalContraCred) + parseFloat(elementSumaContraCred[5]);
                            });

                            //Adicion
                            let arregloSumAd = this.cuentasCcpetUsadasAd.filter(elementSumAd =>
                                (elementSumAd[0].substring(0, largoCuenta) == resultadoBusqueda[1])
                            );
                            var totalAd = 0;
                            arregloSumAd.forEach(elementSumaAd => {
                                //total += elementSuma.reduce((a, b) => a + b);
                                totalAd = parseFloat(totalAd) + parseFloat(elementSumaAd[5]);
                            });

                            //Reduccion
                            let arregloSumRed = this.cuentasCcpetUsadasRed.filter(elementSumRed =>
                                (elementSumRed[0].substring(0, largoCuenta) == resultadoBusqueda[1])
                            );
                            var totalRed = 0;
                            arregloSumRed.forEach(elementSumaRed => {
                                //total += elementSuma.reduce((a, b) => a + b);
                                totalRed = parseFloat(totalRed) + parseFloat(elementSumaRed[5]);
                            });

                            //CDPS
                            let arregloSumCdp = this.cuentasCcpetUsadasCdp.filter(elementSumCdp =>
                                (elementSumCdp[0].substring(0, largoCuenta) == resultadoBusqueda[1])
                            );
                            var totalCdp = 0;
                            arregloSumCdp.forEach(elementSumaCdp => {
                                //total += elementSuma.reduce((a, b) => a + b);
                                totalCdp = parseFloat(totalCdp) + parseFloat(elementSumaCdp[5]);
                            });

                            //RPS
                            let arregloSumRp = this.cuentasCcpetUsadasRp.filter(elementSumRp =>
                                (elementSumRp[0].substring(0, largoCuenta) == resultadoBusqueda[1])
                            );
                            var totalRp = 0;
                            arregloSumRp.forEach(elementSumaRp => {
                                //total += elementSuma.reduce((a, b) => a + b);
                                totalRp = parseFloat(totalRp) + parseFloat(elementSumaRp[5]);
                            });

                            //CXPS
                            let arregloSumCxp = this.cuentasCcpetUsadasCxp.filter(elementSumCxp =>
                                (elementSumCxp[0].substring(0, largoCuenta) == resultadoBusqueda[1])
                            );
                            var totalCxp = 0;
                            arregloSumCxp.forEach(elementSumaCxp => {
                                //total += elementSuma.reduce((a, b) => a + b);
                                totalCxp = parseFloat(totalCxp) + parseFloat(elementSumaCxp[5]);
                            });

                            //EGRESOS

                            let arregloSumEgreso = this.cuentasCcpetUsadasEgreso.filter(elementSumEgreso =>
                                (elementSumEgreso[0].substring(0, largoCuenta) == resultadoBusqueda[1])
                            );
                            var totalEgreso = 0;
                            arregloSumEgreso.forEach(elementSumaEgreso => {
                                //total += elementSuma.reduce((a, b) => a + b);
                                totalEgreso = parseFloat(totalEgreso) + parseFloat(elementSumaEgreso[5]);
                            });

                            var def = 0;
                            var Saldo = 0;

                            def = parseFloat(total) + parseFloat(totalAd) - parseFloat(totalRed) + parseFloat(totalCred) - parseFloat(totalContraCred);
                            Saldo = parseFloat(def) - parseFloat(totalCdp);

                            total = Number(total.toFixed(2));
                            totalAd = Number(totalAd.toFixed(2));
                            totalRed = Number(totalRed.toFixed(2));
                            totalCred = Number(totalCred.toFixed(2));
                            totalContraCred = Number(totalContraCred.toFixed(2));
                            def = Number(def.toFixed(2));
                            totalCdp = Number(totalCdp.toFixed(2));
                            totalRp = Number(totalRp.toFixed(2));
                            totalCxp = Number(totalCxp.toFixed(2));


                            totalEgreso = Number(totalEgreso.toFixed(2));
                            Saldo = Number(Saldo.toFixed(2));
                            //total = Math.round10(total, 2);
                            ramas.push('');
                            ramas.push('');
                            ramas.push('');
                            ramas.push('');
                            ramas.push('');
                            ramas.push('');
                            ramas.push(total);
                            ramas.push(totalAd);
                            ramas.push(totalRed);
                            ramas.push(totalCred);
                            ramas.push(totalContraCred);
                            ramas.push(def);
                            ramas.push(totalCdp);
                            ramas.push(totalRp);
                            ramas.push(totalCxp);
                            ramas.push(totalEgreso);
                            ramas.push(Saldo);
                        }

                        this.arbolFuncionamiento.push(ramas);
                    }else{

                        resultArbolFuente = this.arbolFuncionamiento.find(cuentaArbol => (cuentaArbol[0] == element[0] && cuentaArbol[3] == element[1] && cuentaArbol[4] == element[2] && cuentaArbol[5] == element[3] && cuentaArbol[6] == element[4]));
                        if(resultArbolFuente == undefined){

                            if(resultadoBusqueda[6] == 'C'){
                                var ramas = [];
                                ramas.push(resultadoBusqueda[1]);
                                ramas.push(resultadoBusqueda[2]);
                                ramas.push(resultadoBusqueda[6]);
                                ramas.push(element[1]);
                                ramas.push(element[2]);
                                ramas.push(element[3]);
                                ramas.push(element[4]);
                                ramas.push(element[6]);
                                ramas.push(element[7]);
                                ramas.push(element[5]);



                                this.arbolFuncionamiento.push(ramas);
                            }
                        }
                    }
                    restante += elementNiv.descontar;
                });

            });



            this.cuentasCcpetUsadasAd.forEach(element => {

                var resultado = this.cuentasCcpet.find( cuenta => cuenta[1] === element[0] );

                var niveles = ordenPorNiveles.filter(niv => niv.nivel <= resultado[4]);

                var restante = 0;
                niveles.forEach(elementNiv => {
                    var resultArbol = [];
                    var largo =  resultado[1].length-restante;

                    var cuentaABuscar = resultado[1].substring(0, largo);

                    var resultadoBusqueda = this.cuentasCcpet.find( cuenta => cuenta[1] === cuentaABuscar );

                    resultArbol = this.arbolFuncionamiento.find(cuentaArbol => cuentaArbol[0] == resultadoBusqueda[1]);

                    if(resultArbol == undefined){
                        var ramas = [];
                        ramas.push(resultadoBusqueda[1]);
                        ramas.push(resultadoBusqueda[2]);
                        ramas.push(resultadoBusqueda[6]);
                        if(resultadoBusqueda[6] == 'C'){
                            ramas.push(element[1]);
                            ramas.push(element[2]);
                            ramas.push(element[3]);
                            ramas.push(element[4]);
                            ramas.push(element[6]);
                            ramas.push(element[7]);
                            ramas.push(0);
                            ramas.push(element[5]);

                        }else{

                            largoCuenta = resultadoBusqueda[1].length;
                            let arregloSum = this.cuentasCcpetUsadasAd.filter(elementSum =>
                                (elementSum[0].substring(0, largoCuenta) == resultadoBusqueda[1])
                            );
                            var total = 0;
                            arregloSum.forEach(elementSuma => {
                                //total += elementSuma.reduce((a, b) => a + b);
                                total = parseFloat(total) + parseFloat(elementSuma[5]);
                            });
                            ramas.push('');
                            ramas.push('');
                            ramas.push('');
                            ramas.push('');
                            ramas.push('');
                            ramas.push('');
                            ramas.push(0);
                            ramas.push(total);
                        }

                        this.arbolFuncionamiento.push(ramas);
                    }else{

                        resultArbolFuente = this.arbolFuncionamiento.find(cuentaArbol => (cuentaArbol[0] == element[0] && cuentaArbol[3] == element[1] && cuentaArbol[4] == element[2] && cuentaArbol[5] == element[3] && cuentaArbol[6] == element[4]));
                        if(resultArbolFuente == undefined){

                            if(resultadoBusqueda[6] == 'C'){
                                var ramas = [];
                                ramas.push(resultadoBusqueda[1]);
                                ramas.push(resultadoBusqueda[2]);
                                ramas.push(resultadoBusqueda[6]);
                                ramas.push(element[1]);
                                ramas.push(element[2]);
                                ramas.push(element[3]);
                                ramas.push(element[4]);
                                ramas.push(element[6]);
                                ramas.push(element[7]);
                                ramas.push(0);
                                ramas.push(element[5]);


                                this.arbolFuncionamiento.push(ramas);
                            }
                        }else{
                            this.arbolFuncionamiento.forEach((elementAd, index) => {

                                if(elementAd[0] == resultadoBusqueda[1] && elementAd[3] == element[1] && elementAd[4] == element[2] && elementAd[5] == element[3] && elementAd[6] == element[4]){

                                    this.arbolFuncionamiento[index].push(element[5]);
                                }else{

                                    //this.arbolFuncionamiento[index].push(0);
                                }


                            });
                        }
                    }
                    restante += elementNiv.descontar;
                });

            });

            this.arbolFuncionamiento.forEach((elemnt, index) => {
                //if(elemnt[2] == 'C'){
                    var cant = 11 - elemnt.length;
                    if(cant>=0){
                        while(cant != 0){
                            this.arbolFuncionamiento[index].push(0);
                            cant-=1;
                        }
                    }
                //}
            });

            this.cuentasCcpetUsadasRed.forEach(element => {

                var resultado = this.cuentasCcpet.find( cuenta => cuenta[1] === element[0] );

                var niveles = ordenPorNiveles.filter(niv => niv.nivel <= resultado[4]);

                var restante = 0;
                niveles.forEach(elementNiv => {
                    var resultArbol = [];
                    var largo =  resultado[1].length-restante;

                    var cuentaABuscar = resultado[1].substring(0, largo);

                    var resultadoBusqueda = this.cuentasCcpet.find( cuenta => cuenta[1] === cuentaABuscar );

                    resultArbol = this.arbolFuncionamiento.find(cuentaArbol => cuentaArbol[0] == resultadoBusqueda[1]);

                    if(resultArbol == undefined){
                        var ramas = [];
                        ramas.push(resultadoBusqueda[1]);
                        ramas.push(resultadoBusqueda[2]);
                        ramas.push(resultadoBusqueda[6]);
                        if(resultadoBusqueda[6] == 'C'){
                            ramas.push(element[1]);
                            ramas.push(element[2]);
                            ramas.push(element[3]);
                            ramas.push(element[4]);
                            ramas.push(element[6]);
                            ramas.push(element[7]);
                            ramas.push(0);
                            ramas.push(0);
                            ramas.push(element[5]);


                        }else{

                            largoCuenta = resultadoBusqueda[1].length;
                            let arregloSum = this.cuentasCcpetUsadasAd.filter(elementSum =>
                                (elementSum[0].substring(0, largoCuenta) == resultadoBusqueda[1])
                            );
                            var total = 0;
                            arregloSum.forEach(elementSuma => {
                                //total += elementSuma.reduce((a, b) => a + b);
                                total = parseFloat(total) + parseFloat(elementSuma[5]);
                            });
                            ramas.push('');
                            ramas.push('');
                            ramas.push('');
                            ramas.push('');
                            ramas.push('');
                            ramas.push('');
                            ramas.push(0);
                            ramas.push(0);

                            ramas.push(total);
                        }

                        this.arbolFuncionamiento.push(ramas);
                    }else{

                        resultArbolFuente = this.arbolFuncionamiento.find(cuentaArbol => (cuentaArbol[0] == element[0] && cuentaArbol[3] == element[1] && cuentaArbol[4] == element[2] && cuentaArbol[5] == element[3] && cuentaArbol[6] == element[4]));
                        if(resultArbolFuente == undefined){
                            if(resultadoBusqueda[6] == 'C'){
                                var ramas = [];
                                ramas.push(resultadoBusqueda[1]);
                                ramas.push(resultadoBusqueda[2]);
                                ramas.push(resultadoBusqueda[6]);
                                ramas.push(element[1]);
                                ramas.push(element[2]);
                                ramas.push(element[3]);
                                ramas.push(element[4]);
                                ramas.push(element[6]);
                                ramas.push(element[7]);
                                ramas.push(0);
                                ramas.push(0);
                                ramas.push(element[5]);


                                this.arbolFuncionamiento.push(ramas);
                            }
                        }else{
                            this.arbolFuncionamiento.forEach((elementAd, index) => {

                                if(elementAd[0] == resultadoBusqueda[1] && elementAd[3] == element[1] && elementAd[4] == element[2] && elementAd[5] == element[3] && elementAd[6] == element[4]){

                                    this.arbolFuncionamiento[index].push(element[5]);
                                }else{

                                    //this.arbolFuncionamiento[index].push(0);
                                }


                            });
                        }
                    }
                    restante += elementNiv.descontar;
                });

            });

            this.arbolFuncionamiento.forEach((elemnt, index) => {
                //if(elemnt[2] == 'C'){
                    var cant = 12 - elemnt.length;
                    if(cant>=0){
                        while(cant != 0){
                            this.arbolFuncionamiento[index].push(0);
                            cant-=1;
                        }
                    }
                //}
            });


            this.cuentasCcpetUsadasCred.forEach(element => {

                var resultado = this.cuentasCcpet.find( cuenta => cuenta[1] === element[0] );

                var niveles = ordenPorNiveles.filter(niv => niv.nivel <= resultado[4]);

                var restante = 0;
                niveles.forEach(elementNiv => {
                    var resultArbol = [];
                    var largo =  resultado[1].length-restante;

                    var cuentaABuscar = resultado[1].substring(0, largo);

                    var resultadoBusqueda = this.cuentasCcpet.find( cuenta => cuenta[1] === cuentaABuscar );


                    resultArbol = this.arbolFuncionamiento.find(cuentaArbol => cuentaArbol[0] == resultadoBusqueda[1]);

                    if(resultArbol == undefined){
                        var ramas = [];
                        ramas.push(resultadoBusqueda[1]);
                        ramas.push(resultadoBusqueda[2]);
                        ramas.push(resultadoBusqueda[6]);
                        if(resultadoBusqueda[6] == 'C'){
                            ramas.push(element[1]);
                            ramas.push(element[2]);
                            ramas.push(element[3]);
                            ramas.push(element[4]);
                            ramas.push(element[6]);
                            ramas.push(element[7]);
                            ramas.push(0);
                            ramas.push(0);
                            ramas.push(0);
                            ramas.push(element[5]);

                        }else{

                            largoCuenta = resultadoBusqueda[1].length;
                            let arregloSum = this.cuentasCcpetUsadasCred.filter(elementSum =>
                                (elementSum[0].substring(0, largoCuenta) == resultadoBusqueda[1])
                            );
                            var total = 0;
                            arregloSum.forEach(elementSuma => {
                                //total += elementSuma.reduce((a, b) => a + b);
                                total = parseFloat(total) + parseFloat(elementSuma[5]);
                            });
                            ramas.push('');
                            ramas.push('');
                            ramas.push('');
                            ramas.push('');
                            ramas.push('');
                            ramas.push('');
                            ramas.push(0);
                            ramas.push(0);
                            ramas.push(0);
                            ramas.push(total);
                        }

                        this.arbolFuncionamiento.push(ramas);
                    }else{

                        resultArbolFuente = this.arbolFuncionamiento.find(cuentaArbol => (cuentaArbol[0] == element[0] && cuentaArbol[3] == element[1] && cuentaArbol[4] == element[2] && cuentaArbol[5] == element[3] && cuentaArbol[6] == element[4]));
                        if(resultArbolFuente == undefined){

                            if(resultadoBusqueda[6] == 'C'){
                                var ramas = [];
                                ramas.push(resultadoBusqueda[1]);
                                ramas.push(resultadoBusqueda[2]);
                                ramas.push(resultadoBusqueda[6]);
                                ramas.push(element[1]);
                                ramas.push(element[2]);
                                ramas.push(element[3]);
                                ramas.push(element[4]);
                                ramas.push(element[6]);
                                ramas.push(element[7]);
                                ramas.push(0);
                                ramas.push(0);
                                ramas.push(0);
                                ramas.push(element[5]);


                                this.arbolFuncionamiento.push(ramas);
                            }
                        }else{
                            this.arbolFuncionamiento.forEach((elementCred, index) => {

                                if(elementCred[0] == resultadoBusqueda[1] && elementCred[3] == element[1] && elementCred[4] == element[2] && elementCred[5] == element[3] && elementCred[6] == element[4]){

                                    this.arbolFuncionamiento[index].push(element[5]);
                                }else{

                                    //this.arbolFuncionamiento[index].push(0);
                                }


                            });
                        }
                    }
                    restante += elementNiv.descontar;
                });

            });

            this.arbolFuncionamiento.forEach((elemnt, index) => {
                //if(elemnt[2] == 'C'){
                    var cant = 13 - elemnt.length;
                    if(cant>=0){
                        while(cant != 0){
                            this.arbolFuncionamiento[index].push(0);
                            cant-=1;
                        }
                    }
                //}
            });

            this.cuentasCcpetUsadasContraCred.forEach(element => {

                var resultado = this.cuentasCcpet.find( cuenta => cuenta[1] === element[0] );

                var niveles = ordenPorNiveles.filter(niv => niv.nivel <= resultado[4]);

                var restante = 0;
                niveles.forEach(elementNiv => {
                    var resultArbol = [];
                    var largo =  resultado[1].length-restante;

                    var cuentaABuscar = resultado[1].substring(0, largo);

                    var resultadoBusqueda = this.cuentasCcpet.find( cuenta => cuenta[1] === cuentaABuscar );

                    resultArbol = this.arbolFuncionamiento.find(cuentaArbol => cuentaArbol[0] == resultadoBusqueda[1]);

                    if(resultArbol == undefined){
                        var ramas = [];
                        ramas.push(resultadoBusqueda[1]);
                        ramas.push(resultadoBusqueda[2]);
                        ramas.push(resultadoBusqueda[6]);
                        if(resultadoBusqueda[6] == 'C'){

                            ramas.push(element[1]);
                            ramas.push(element[2]);
                            ramas.push(element[3]);
                            ramas.push(element[4]);
                            ramas.push(element[6]);
                            ramas.push(element[7]);
                            ramas.push(0);
                            ramas.push(0);
                            ramas.push(0);
                            ramas.push(0);
                            ramas.push(element[5]);

                        }else{

                            largoCuenta = resultadoBusqueda[1].length;
                            let arregloSum = this.cuentasCcpetUsadasContraCred.filter(elementSum =>
                                (elementSum[0].substring(0, largoCuenta) == resultadoBusqueda[1])
                            );
                            var total = 0;
                            arregloSum.forEach(elementSuma => {
                                //total += elementSuma.reduce((a, b) => a + b);
                                total = parseFloat(total) + parseFloat(elementSuma[5]);
                            });
                            ramas.push('');
                            ramas.push('');
                            ramas.push('');
                            ramas.push('');
                            ramas.push('');
                            ramas.push('');
                            ramas.push(0);
                            ramas.push(0);
                            ramas.push(0);
                            ramas.push(total);
                        }

                        this.arbolFuncionamiento.push(ramas);
                    }else{

                        resultArbolFuente = this.arbolFuncionamiento.find(cuentaArbol => (cuentaArbol[0] == element[0] && cuentaArbol[3] == element[1] && cuentaArbol[4] == element[2] && cuentaArbol[5] == element[3] && cuentaArbol[6] == element[4]));
                        if(resultArbolFuente == undefined){

                            if(resultadoBusqueda[6] == 'C'){
                                var ramas = [];
                                ramas.push(resultadoBusqueda[1]);
                                ramas.push(resultadoBusqueda[2]);
                                ramas.push(resultadoBusqueda[6]);
                                ramas.push(element[1]);
                                ramas.push(element[2]);
                                ramas.push(element[3]);
                                ramas.push(element[4]);
                                ramas.push(element[6]);
                                ramas.push(element[7]);
                                ramas.push(0);
                                ramas.push(0);
                                ramas.push(0);
                                ramas.push(0);
                                ramas.push(element[5]);


                                this.arbolFuncionamiento.push(ramas);
                            }
                        }else{
                            this.arbolFuncionamiento.forEach((elementCred, index) => {

                                if(elementCred[0] == resultadoBusqueda[1] && elementCred[3] == element[1] && elementCred[4] == element[2] && elementCred[5] == element[3] && elementCred[6] == element[4]){

                                    this.arbolFuncionamiento[index].push(element[5]);
                                }else{

                                    //this.arbolFuncionamiento[index].push(0);
                                }


                            });
                        }
                    }
                    restante += elementNiv.descontar;
                });

            });

            this.arbolFuncionamiento.forEach((elemnt, index) => {
                //if(elemnt[2] == 'C'){
                    var cant = 14 - elemnt.length;
                    if(cant>=0){
                        while(cant != 0){
                            this.arbolFuncionamiento[index].push(0);
                            cant-=1;
                        }
                    }
                //}
            });

            /* this.arbolFuncionamiento.forEach((elemnt, index) => {
                if(elemnt.length == 8){
                    this.arbolFuncionamiento[index].push(0);
                    this.arbolFuncionamiento[index].push(0);
                    this.arbolFuncionamiento[index].push(0);
                }
                if(elemnt[2] == 'C'){
                    this.arbolFuncionamiento[index].push(0);
                }

            }); */

            this.arbolFuncionamiento.forEach((element, index) => {
                var def = 0;
                //if(element[2] == 'C'){

                    var inicial = element[9];
                    var adicion = element[10];
                    var reduccion = element[11];
                    var credito = element[12];

                    var contracredito = element[13];

                    def = parseFloat(inicial) + parseFloat(adicion) - parseFloat(reduccion) + parseFloat(credito) - parseFloat(contracredito);
                    this.arbolFuncionamiento[index].push(def);
                //}


            });

            //CDPs
            this.cuentasCcpetUsadasCdp.forEach(element => {

                this.arbolFuncionamiento.forEach((elementCdp, index) => {

                    if(elementCdp[0] == element[0] && elementCdp[3] == element[1] && elementCdp[4] == element[2] && elementCdp[5] == element[3] && elementCdp[6] == element[4]){

                        var cant = 15 - this.arbolFuncionamiento[index].length;
                        if(cant>=0){
                            while(cant != 0){
                                this.arbolFuncionamiento[index].push(0);
                                cant-=1;
                            }
                            this.arbolFuncionamiento[index].push(element[5]);
                        }

                    }
                });

            });

            //RPs
            this.cuentasCcpetUsadasRp.forEach(element => {

                this.arbolFuncionamiento.forEach((elementRp, index) => {

                    if(elementRp[0] == element[0] && elementRp[3] == element[1] && elementRp[4] == element[2] && elementRp[5] == element[3] && elementRp[6] == element[4]){

                        var cant = 16 - this.arbolFuncionamiento[index].length;
                        if(cant>=0){
                            while(cant != 0){
                                this.arbolFuncionamiento[index].push(0);
                                cant-=1;
                            }
                            this.arbolFuncionamiento[index].push(element[5]);
                        }

                    }
                });

            });

            //Cxps

            const miArraySinDuplicados = this.cuentasCcpetUsadasCxp.reduce((acumulador, valorActual) => {
                const elementoYaExiste = acumulador.find(elemento => (elemento[0] === valorActual[0] && elemento[1] === valorActual[1] && elemento[2] === valorActual[2] && elemento[3] === valorActual[3] && elemento[4] === valorActual[4]));
                if (elementoYaExiste) {
                    return acumulador.map((elemento) => {
                        if (elemento[0] === valorActual[0] && elemento[1] === valorActual[1] && elemento[2] === valorActual[2] && elemento[3] === valorActual[3] && elemento[4] === valorActual[4]) {
                            return {
                            ...elemento,
                            5: Number(elemento[5]) + Number(valorActual[5])
                            }
                        }
                        return elemento;
                    });
                }

                return [...acumulador, valorActual];
            }, []);

            miArraySinDuplicados.forEach(element => {

                this.arbolFuncionamiento.forEach((elementCxp, index) => {

                    if(elementCxp[0] == element[0] && elementCxp[3] == element[1] && elementCxp[4] == element[2] && elementCxp[5] == element[3] && elementCxp[6] == element[4]){
                        var cant = 17 - this.arbolFuncionamiento[index].length;
                        if(cant>=0){
                            while(cant != 0){
                                this.arbolFuncionamiento[index].push(0);
                                cant-=1;
                            }
                        }

                        this.arbolFuncionamiento[index].push(element[5]);
                    }
                });

            });

            //Egresos

            const miArraySinDuplicadosEgresos = this.cuentasCcpetUsadasEgreso.reduce((acumulador, valorActual) => {
                const elementoYaExiste = acumulador.find(elemento => (elemento[0] === valorActual[0] && elemento[1] === valorActual[1] && elemento[2] === valorActual[2] && elemento[3] === valorActual[3] && elemento[4] === valorActual[4]));
                if (elementoYaExiste) {
                    return acumulador.map((elemento) => {
                        if (elemento[0] === valorActual[0] && elemento[1] === valorActual[1] && elemento[2] === valorActual[2] && elemento[3] === valorActual[3] && elemento[4] === valorActual[4]) {
                            return {
                            ...elemento,
                            5: Number(elemento[5]) + Number(valorActual[5])
                            }
                        }
                        return elemento;
                    });
                }

                return [...acumulador, valorActual];
            }, []);

            miArraySinDuplicadosEgresos.forEach(element => {

                this.arbolFuncionamiento.forEach((elementEgreso, index) => {

                    if(elementEgreso[0] == element[0] && elementEgreso[3] == element[1] && elementEgreso[4] == element[2] && elementEgreso[5] == element[3] && elementEgreso[6] == element[4]){
                        var cant = 18 - this.arbolFuncionamiento[index].length;
                        if(cant>=0){
                            while(cant != 0){
                                this.arbolFuncionamiento[index].push(0);
                                cant-=1;
                            }
                            this.arbolFuncionamiento[index].push(element[5]);
                        }


                    }
                });

            });

            this.arbolFuncionamiento.forEach((elemnt, index) => {
                //if(elemnt[2] == 'C'){
                    var cant = 19 - elemnt.length;
                    if(cant>=0){
                        while(cant != 0){
                            this.arbolFuncionamiento[index].push(0);
                            cant-=1;
                        }
                    }
                //}
            });

            this.arbolFuncionamiento.forEach((element, index) => {
                var saldo = 0;
                if(element[2] == 'C'){
                    var definitivo = element[14];
                    var cdps = element[15];
                    saldo = parseFloat(definitivo) - parseFloat(cdps);
                    this.arbolFuncionamiento[index].push(saldo);
                }


            });

            this.arbolFuncionamiento.sort();


            this.cuentasCcpetUsadasProy.forEach(element => {

                var resultArbol = [];
                resultArbol = this.arbolInversion.find(cuentaArbol => cuentaArbol[0] == element[2]);
                if(resultArbol == undefined){

                    var ramas = [];
                    ramas.push(element[2]);

                    this.nombreSectores.forEach(function logArrayElements(elementSec, index, array) {
                        if(elementSec[0] == element[2]){
                            ramas.push('Sector: ' + elementSec[1].toLowerCase());
                        }
                    });
                    ramas.push('A');
                    ramas.push('');
                    ramas.push('');
                    ramas.push('');
                    ramas.push('');
                    ramas.push('');
                    ramas.push('');

                    //Inicial
                    var totalIniInv = 0;
                    this.cuentasCcpetUsadasIniInv.forEach(elementIniInv => {
                        if(elementIniInv[4].substring(0, 2) == element[2]){
                            totalIniInv = parseFloat(totalIniInv) + parseFloat(elementIniInv[5]) + parseFloat(elementIniInv[6]);
                        }
                    });
                    ramas.push(totalIniInv);

                    //Adicion
                    var totalAdInv = 0;
                    this.cuentasCcpetUsadasAdInv.forEach(elementAdInv => {
                        if(elementAdInv[4].substring(0, 2) == element[2]){
                            totalAdInv = parseFloat(totalAdInv) + parseFloat(elementAdInv[5]);
                        }
                    });
                    ramas.push(totalAdInv);

                    //Reduccion

                    var totalRedInv = 0;

                    this.cuentasCcpetUsadasRedInv.forEach(elementRedInv => {
                        if(elementRedInv[4].substring(0, 2) == element[2]){
                            totalRedInv = parseFloat(totalRedInv) + parseFloat(elementRedInv[5]);
                        }
                    });

                    ramas.push(totalRedInv);

                    //Credito

                    //Traslado Cred
                    var totalCredInv = 0;
                    this.cuentasCcpetUsadasTrasladoCredInv.forEach(elementCredInv => {
                        if(elementCredInv[4].substring(0, 2) == element[2]){
                            totalCredInv = parseFloat(totalCredInv) + parseFloat(elementCredInv[5]);
                        }
                    });
                    ramas.push(totalCredInv);

                    //Contracredito

                    var totalContraInv = 0;

                    this.cuentasCcpetUsadasTrasladoContraCredInv.forEach(elementContraCredInv => {
                        if(elementContraCredInv[4].substring(0, 2) == element[2]){
                            totalContraInv = parseFloat(totalContraInv) + parseFloat(elementContraCredInv[5]);
                        }
                    });

                    ramas.push(totalContraInv);

                    var totalInv = totalIniInv + totalAdInv - totalRedInv + totalCredInv - totalContraInv;

                    ramas.push(totalInv);

                    //Cdp Inversion
                    var totalCdpInv = 0;
                    this.cuentasCcpetUsadasCdpInv.forEach(elementCdpInv => {
                        if(elementCdpInv[4].substring(0, 2) == element[2]){
                            totalCdpInv = parseFloat(totalCdpInv) + parseFloat(elementCdpInv[5]);
                        }
                    });
                    ramas.push(totalCdpInv);

                    //Rp Inversion
                    var totalRpInv = 0;
                    this.cuentasCcpetUsadasRpInv.forEach(elementRpInv => {
                        if(elementRpInv[4].substring(0, 2) == element[2]){
                            totalRpInv = parseFloat(totalRpInv) + parseFloat(elementRpInv[5]);
                        }
                    });
                    ramas.push(totalRpInv);

                    //Cxp Inversion
                    var totalCxpInv = 0;
                    this.cuentasCcpetUsadasCxpInv.forEach(elementCxpInv => {
                        if(elementCxpInv[4].substring(0, 2) == element[2]){
                            totalCxpInv = parseFloat(totalCxpInv) + parseFloat(elementCxpInv[5]);
                        }
                    });
                    ramas.push(totalCxpInv);

                    //Egreso Inversion
                    var totalEgresoInv = 0;
                    this.cuentasCcpetUsadasEgresoInv.forEach(elementEgresoInv => {
                        if(elementEgresoInv[4].substring(0, 2) == element[2]){
                            totalEgresoInv = parseFloat(totalEgresoInv) + parseFloat(elementEgresoInv[5]);
                        }
                    });
                    ramas.push(totalEgresoInv);

                    var saldoInv = Number(totalInv.toFixed(2)) - Number(totalCdpInv.toFixed(2));

                    ramas.push(Number(saldoInv.toFixed(2)));

                    this.arbolInversion.push(ramas);
                }

                var resultArbol = [];
                resultArbol = this.arbolInversion.find(cuentaArbol => cuentaArbol[0] == element[3]);
                if(resultArbol == undefined){

                    var ramas = [];
                    ramas.push(element[3]);
                    this.nombreProgramas.forEach(function logArrayElements(elementProg, index, array) {
                        if(elementProg[0] == element[3]){
                            ramas.push('Programa: ' + elementProg[1].toLowerCase());
                        }
                    });
                    ramas.push('A');
                    ramas.push('');
                    ramas.push('');
                    ramas.push('');
                    ramas.push('');
                    ramas.push('');
                    ramas.push('');
                    /* ramas.push(resultadoBusqueda[2]);
                    ramas.push(resultadoBusqueda[6]); */

                    //Inicial
                    var totalIniInv = 0;
                    this.cuentasCcpetUsadasIniInv.forEach(elementIniInv => {
                        if(elementIniInv[4].substring(0, 4) == element[3]){
                            totalIniInv = parseFloat(totalIniInv) + parseFloat(elementIniInv[5]) + parseFloat(elementIniInv[6]);
                        }
                    });
                    ramas.push(totalIniInv);

                    //Adicion
                    var totalAdInv = 0;
                    this.cuentasCcpetUsadasAdInv.forEach(elementAdInv => {
                        if(elementAdInv[4].substring(0, 4) == element[3]){
                            totalAdInv = parseFloat(totalAdInv) + parseFloat(elementAdInv[5]);
                        }
                    });
                    ramas.push(totalAdInv);

                    //Reduccion

                    var totalRedInv = 0;

                    this.cuentasCcpetUsadasRedInv.forEach(elementRedInv => {
                        if(elementRedInv[4].substring(0, 4) == element[3]){
                            totalRedInv = parseFloat(totalRedInv) + parseFloat(elementRedInv[5]);
                        }
                    });

                    ramas.push(totalRedInv);

                    //Credito

                    var totalCredInv = 0;

                    this.cuentasCcpetUsadasTrasladoCredInv.forEach(elementCredInv => {
                        if(elementCredInv[4].substring(0, 4) == element[3]){
                            totalCredInv = parseFloat(totalCredInv) + parseFloat(elementCredInv[5]);
                        }
                    });

                    ramas.push(totalCredInv);

                    //Contracredito

                    var totalContraInv = 0;

                    this.cuentasCcpetUsadasTrasladoContraCredInv.forEach(elementContraCredInv => {
                        if(elementContraCredInv[4].substring(0, 4) == element[3]){
                            totalContraInv = parseFloat(totalContraInv) + parseFloat(elementContraCredInv[5]);
                        }
                    });

                    ramas.push(totalContraInv);

                    var totalInv = totalIniInv + totalAdInv - totalRedInv + totalCredInv - totalContraInv;

                    ramas.push(totalInv);


                    //Cdp Inversion
                    var totalCdpInv = 0;
                    this.cuentasCcpetUsadasCdpInv.forEach(elementCdpInv => {
                        if(elementCdpInv[4].substring(0, 4) == element[3]){
                            totalCdpInv = parseFloat(totalCdpInv) + parseFloat(elementCdpInv[5]);
                        }
                    });
                    ramas.push(totalCdpInv);

                    //Rp Inversion
                    var totalRpInv = 0;
                    this.cuentasCcpetUsadasRpInv.forEach(elementRpInv => {
                        if(elementRpInv[4].substring(0, 4) == element[3]){
                            totalRpInv = parseFloat(totalRpInv) + parseFloat(elementRpInv[5]);
                        }
                    });
                    ramas.push(totalRpInv);

                    //Cxp Inversion
                    var totalCxpInv = 0;
                    this.cuentasCcpetUsadasCxpInv.forEach(elementCxpInv => {
                        if(elementCxpInv[4].substring(0, 4) == element[3]){
                            totalCxpInv = parseFloat(totalCxpInv) + parseFloat(elementCxpInv[5]);
                        }
                    });
                    ramas.push(totalCxpInv);

                    //Egreso Inversion
                    var totalEgresoInv = 0;
                    this.cuentasCcpetUsadasEgresoInv.forEach(elementEgresoInv => {
                        if(elementEgresoInv[4].substring(0, 4) == element[3]){
                            totalEgresoInv = parseFloat(totalEgresoInv) + parseFloat(elementEgresoInv[5]);
                        }
                    });
                    ramas.push(totalEgresoInv);

                    var saldoInv = Number(totalInv.toFixed(2)) - Number(totalCdpInv.toFixed(2));

                    ramas.push(Number(saldoInv.toFixed(2)));

                    this.arbolInversion.push(ramas);
                }

                var resultArbol = [];
                subprograma = '';
                subprograma = element[3]+''+element[4];
                resultArbol = this.arbolInversion.find(cuentaArbol => cuentaArbol[0] == subprograma);
                if(resultArbol == undefined){
                    var ramas = [];
                    ramas.push(subprograma);
                    this.nombreSubProgramas.forEach(function logArrayElements(elementSubProg, index, array) {
                        if(elementSubProg[0] == element[4]){
                            ramas.push('Subprograma: ' + elementSubProg[1].toLowerCase());
                        }
                    });
                    ramas.push('A');
                    ramas.push('');
                    ramas.push('');
                    ramas.push('');
                    ramas.push('');
                    ramas.push('');
                    ramas.push('');

                    var totalIniInv = 0;
                    this.cuentasCcpetUsadasIniInv.forEach(elementIniInv => {
                        if(elementIniInv[4].substring(0, 4) == element[3]){
                            totalIniInv = parseFloat(totalIniInv) + parseFloat(elementIniInv[5]) + parseFloat(elementIniInv[6]);
                        }
                    });
                    ramas.push(totalIniInv);

                    //Adicion
                    var totalAdInv = 0;
                    this.cuentasCcpetUsadasAdInv.forEach(elementAdInv => {
                        if(elementAdInv[4].substring(0, 4) == element[3]){
                            totalAdInv = parseFloat(totalAdInv) + parseFloat(elementAdInv[5]);
                        }
                    });
                    ramas.push(totalAdInv);

                    //Reduccion

                    var totalRedInv = 0;

                    this.cuentasCcpetUsadasRedInv.forEach(elementRedInv => {
                        if(elementRedInv[4].substring(0, 4) == element[3]){
                            totalRedInv = parseFloat(totalRedInv) + parseFloat(elementRedInv[5]);
                        }
                    });

                    ramas.push(totalRedInv);

                    //Credito

                    var totalCredInv = 0;

                    this.cuentasCcpetUsadasTrasladoCredInv.forEach(elementCredInv => {
                        if(elementCredInv[4].substring(0, 4) == element[3]){
                            totalCredInv = parseFloat(totalCredInv) + parseFloat(elementCredInv[5]);
                        }
                    });

                    ramas.push(totalCredInv);

                    //Contracredito

                    var totalContraInv = 0;

                    this.cuentasCcpetUsadasTrasladoContraCredInv.forEach(elementContraCredInv => {
                        if(elementContraCredInv[4].substring(0, 4) == element[3]){
                            totalContraInv = parseFloat(totalContraInv) + parseFloat(elementContraCredInv[5]);
                        }
                    });

                    ramas.push(totalContraInv);


                    /* ramas.push(totalContraInv); */

                    var totalInv = totalIniInv + totalAdInv - totalRedInv + totalCredInv - totalContraInv;

                    ramas.push(totalInv);

                    //Cdp Inversion
                    var totalCdpInv = 0;
                    this.cuentasCcpetUsadasCdpInv.forEach(elementCdpInv => {
                        if(elementCdpInv[4].substring(0, 4) == element[3]){
                            totalCdpInv = parseFloat(totalCdpInv) + parseFloat(elementCdpInv[5]);
                        }
                    });
                    ramas.push(totalCdpInv);

                    //Rp Inversion
                    var totalRpInv = 0;
                    this.cuentasCcpetUsadasRpInv.forEach(elementRpInv => {
                        if(elementRpInv[4].substring(0, 4) == element[3]){
                            totalRpInv = parseFloat(totalRpInv) + parseFloat(elementRpInv[5]);
                        }
                    });
                    ramas.push(totalRpInv);

                    //Cxp Inversion
                    var totalCxpInv = 0;
                    this.cuentasCcpetUsadasCxpInv.forEach(elementCxpInv => {
                        if(elementCxpInv[4].substring(0, 4) == element[3]){
                            totalCxpInv = parseFloat(totalCxpInv) + parseFloat(elementCxpInv[5]);
                        }
                    });
                    ramas.push(totalCxpInv);

                    //Egreso Inversion
                    var totalEgresoInv = 0;
                    this.cuentasCcpetUsadasEgresoInv.forEach(elementEgresoInv => {
                        if(elementEgresoInv[4].substring(0, 4) == element[3]){
                            totalEgresoInv = parseFloat(totalEgresoInv) + parseFloat(elementEgresoInv[5]);
                        }
                    });
                    ramas.push(totalEgresoInv);

                    var saldoInv = Number(totalInv.toFixed(2)) - Number(totalCdpInv.toFixed(2));

                    ramas.push(Number(saldoInv.toFixed(2)));

                    this.arbolInversion.push(ramas);
                }

                var resultArbol = [];
                resultArbol = this.arbolInversion.find(cuentaArbol => cuentaArbol[0] == element[5]);
                if(resultArbol == undefined){

                    var ramas = [];
                    ramas.push(element[5]);
                    this.nombreProductos.forEach(function logArrayElements(elementProducto, index, array) {
                        if(elementProducto[0] == element[5]){
                            ramas.push('Producto: ' + elementProducto[1].toLowerCase());
                        }
                    });
                    ramas.push('A');
                    ramas.push('');
                    ramas.push('');
                    ramas.push('');
                    ramas.push('');
                    ramas.push('');
                    ramas.push('');

                    //Inicial
                    var totalIniInv = 0;
                    this.cuentasCcpetUsadasIniInv.forEach(elementIniInv => {
                        if(elementIniInv[4].substring(0, this.productoTam) == element[5]){
                            totalIniInv = parseFloat(totalIniInv) + parseFloat(elementIniInv[5]) + parseFloat(elementIniInv[6]);
                        }
                    });

                    ramas.push(totalIniInv);

                    //Adicion
                    var totalAdInv = 0;
                    this.cuentasCcpetUsadasAdInv.forEach(elementAdInv => {
                        if(elementAdInv[4].substring(0, this.productoTam) == element[5]){
                            totalAdInv = parseFloat(totalAdInv) + parseFloat(elementAdInv[5]);
                        }
                    });
                    ramas.push(totalAdInv);

                    //Reduccion

                    var totalRedInv = 0;

                    this.cuentasCcpetUsadasRedInv.forEach(elementRedInv => {
                        if(elementRedInv[4].substring(0, this.productoTam) == element[5]){
                            totalRedInv = parseFloat(totalRedInv) + parseFloat(elementRedInv[5]);
                        }
                    });

                    ramas.push(totalRedInv);

                    //Credito

                    var totalCredInv = 0;
                    //
                    this.cuentasCcpetUsadasTrasladoCredInv.forEach(elementCredInv => {
                        if(elementCredInv[4].substring(0, this.productoTam) == element[5]){

                            totalCredInv = parseFloat(totalCredInv) + parseFloat(elementCredInv[5]);
                        }
                    });

                    ramas.push(totalCredInv);

                    //Contracredito

                    var totalContraInv = 0;

                    this.cuentasCcpetUsadasTrasladoContraCredInv.forEach(elementContraCredInv => {
                        if(elementContraCredInv[4].substring(0, this.productoTam) == element[5]){

                            totalContraInv = parseFloat(totalContraInv) + parseFloat(elementContraCredInv[5]);
                        }
                    });

                    ramas.push(totalContraInv);

                    var totalInv = totalIniInv + totalAdInv - totalRedInv + totalCredInv - totalContraInv;

                    ramas.push(totalInv);


                    //Cdp Inversion
                    var totalCdpInv = 0;
                    this.cuentasCcpetUsadasCdpInv.forEach(elementCdpInv => {
                        if(elementCdpInv[4].substring(0, this.productoTam) == element[5]){
                            totalCdpInv = parseFloat(totalCdpInv) + parseFloat(elementCdpInv[5]);
                        }
                    });
                    ramas.push(totalCdpInv);

                    //Rp Inversion
                    var totalRpInv = 0;
                    this.cuentasCcpetUsadasRpInv.forEach(elementRpInv => {
                        if(elementRpInv[4].substring(0, this.productoTam) == element[5]){
                            totalRpInv = parseFloat(totalRpInv) + parseFloat(elementRpInv[5]);
                        }
                    });
                    ramas.push(totalRpInv);

                    //Cxp Inversion
                    var totalCxpInv = 0;
                    this.cuentasCcpetUsadasCxpInv.forEach(elementCxpInv => {
                        if(elementCxpInv[4].substring(0, this.productoTam) == element[5]){
                            totalCxpInv = parseFloat(totalCxpInv) + parseFloat(elementCxpInv[5]);
                        }
                    });
                    ramas.push(totalCxpInv);

                    //Egreso Inversion
                    var totalEgresoInv = 0;
                    this.cuentasCcpetUsadasEgresoInv.forEach(elementEgresoInv => {
                        if(elementEgresoInv[4].substring(0, this.productoTam) == element[5]){
                            totalEgresoInv = parseFloat(totalEgresoInv) + parseFloat(elementEgresoInv[5]);
                        }
                    });
                    ramas.push(totalEgresoInv);

                    var saldoInv = Number(totalInv.toFixed(2)) - Number(totalCdpInv.toFixed(2));

                    ramas.push(Number(saldoInv.toFixed(2)));

                    this.arbolInversion.push(ramas);
                }

                var resultArbol = [];
                resultArbol = this.arbolInversion.find(cuentaArbol => cuentaArbol[0] == element[6]);
                if(resultArbol == undefined){

                    var ramas = [];
                    ramas.push(element[6]);
                    this.nombreProgramatico.forEach(function logArrayElements(elementProgramatico, index, array) {
                        if(elementProgramatico[0] == element[6]){
                            ramas.push('Programatico: ' + elementProgramatico[1].toLowerCase());
                        }
                    });
                    ramas.push('A');
                    ramas.push('');
                    ramas.push('');
                    ramas.push('');
                    ramas.push('');
                    ramas.push('');
                    ramas.push('');

                    //Inicial
                    var totalIniInv = 0;
                    this.cuentasCcpetUsadasIniInv.forEach(elementIniInv => {
                        if(elementIniInv[4] == element[6]){
                            totalIniInv = parseFloat(totalIniInv) + parseFloat(elementIniInv[5]) + parseFloat(elementIniInv[6]);
                        }
                    });
                    ramas.push(totalIniInv);

                    //Adicion
                    var totalAdInv = 0;
                    this.cuentasCcpetUsadasAdInv.forEach(elementAdInv => {
                        if(elementAdInv[4] == element[6]){
                            totalAdInv = parseFloat(totalAdInv) + parseFloat(elementAdInv[5]);
                        }
                    });
                    ramas.push(totalAdInv);

                    //Reduccion

                    var totalRedInv = 0;

                    this.cuentasCcpetUsadasRedInv.forEach(elementRedInv => {
                        if(elementRedInv[4] == element[6]){
                            totalRedInv = parseFloat(totalRedInv) + parseFloat(elementRedInv[5]);
                        }
                    });

                    ramas.push(totalRedInv);

                    //Credito

                    var totalCredInv = 0;

                    this.cuentasCcpetUsadasTrasladoCredInv.forEach(elementCredInv => {
                        if(elementCredInv[4] == element[6]){
                            totalCredInv = parseFloat(totalCredInv) + parseFloat(elementCredInv[5]);
                        }
                    });

                    ramas.push(totalCredInv);

                    //Contracredito

                    var totalContraInv = 0;

                    this.cuentasCcpetUsadasTrasladoContraCredInv.forEach(elementContraCredInv => {
                        if(elementContraCredInv[4] == element[6]){
                            totalContraInv = parseFloat(totalContraInv) + parseFloat(elementContraCredInv[5]);
                        }
                    });

                    ramas.push(totalContraInv);

                    var totalInv = totalIniInv + totalAdInv - totalRedInv + totalCredInv - totalContraInv;

                    ramas.push(totalInv);


                    //Cdp Inversion
                    var totalCdpInv = 0;
                    this.cuentasCcpetUsadasCdpInv.forEach(elementCdpInv => {
                        if(elementCdpInv[4] == element[6]){
                            totalCdpInv = parseFloat(totalCdpInv) + parseFloat(elementCdpInv[5]);
                        }
                    });
                    ramas.push(totalCdpInv);

                    //Rp Inversion
                    var totalRpInv = 0;
                    this.cuentasCcpetUsadasRpInv.forEach(elementRpInv => {
                        if(elementRpInv[4] == element[6]){
                            totalRpInv = parseFloat(totalRpInv) + parseFloat(elementRpInv[5]);
                        }
                    });
                    ramas.push(totalRpInv);

                    //Cxp Inversion
                    var totalCxpInv = 0;
                    this.cuentasCcpetUsadasCxpInv.forEach(elementCxpInv => {
                        if(elementCxpInv[4] == element[6]){
                            totalCxpInv = parseFloat(totalCxpInv) + parseFloat(elementCxpInv[5]);
                        }
                    });
                    ramas.push(totalCxpInv);

                    //Egreso Inversion
                    var totalEgresoInv = 0;
                    this.cuentasCcpetUsadasEgresoInv.forEach(elementEgresoInv => {
                        if(elementEgresoInv[4] == element[6]){
                            totalEgresoInv = parseFloat(totalEgresoInv) + parseFloat(elementEgresoInv[5]);
                        }
                    });
                    ramas.push(totalEgresoInv);

                    var saldoInv = Number(totalInv.toFixed(2)) - Number(totalCdpInv.toFixed(2));

                    ramas.push(Number(saldoInv.toFixed(2)));

                    this.arbolInversion.push(ramas);
                }

                var resultArbol = [];
                programaticoBpim = '';
                programaticoBpim = element[6]+'-'+element[9];
                programatico_Bpim_Fuente_Sec_Vig_Med = element[6]+'-'+element[9]+'-'+element[11]+'-'+element[12]+'-'+element[13]+'-'+element[14];
                resultArbol = this.arbolInversion.find(cuentaArbol => cuentaArbol[0] == programatico_Bpim_Fuente_Sec_Vig_Med);
                if(resultArbol == undefined){

                    var ramas = [];
                    ramas.push(programatico_Bpim_Fuente_Sec_Vig_Med);
                    ramas.push('Proyecto: ' + element[10]);
                    ramas.push('C');
                    ramas.push(element[11]);
                    ramas.push(element[12]);
                    ramas.push(element[13]);
                    ramas.push(element[14]);

                    /* ramas.push(resultadoBusqueda[2]);
                    ramas.push(resultadoBusqueda[6]); */
                    var totalInv = 0;

                    //Inicial
                    var totalIniInv = 0;
                    let unidadInv = 16;
                    let unidadNomInv = 'Administracion central';
                    this.cuentasCcpetUsadasIniInv.forEach(elementIniInv => {
                        var detProy = elementIniInv[4]+'-'+elementIniInv[1]+'-'+elementIniInv[2]+'-'+elementIniInv[0]+'-'+elementIniInv[7]+'-'+elementIniInv[3];
                        if(detProy == programatico_Bpim_Fuente_Sec_Vig_Med){
                            totalIniInv = parseFloat(totalIniInv) + parseFloat(elementIniInv[5]) + parseFloat(elementIniInv[6]);
                            unidadInv = elementIniInv[8];
                            unidadNomInv = elementIniInv[9];
                        }
                    });
                    ramas.push(unidadInv);
                    ramas.push(unidadNomInv);
                    ramas.push(totalIniInv);

                    //Adicion

                    var totalAdInv = 0;
                    this.cuentasCcpetUsadasAdInv.forEach(elementAdInv => {
                        var detProy = elementAdInv[4]+'-'+elementAdInv[1]+'-'+elementAdInv[2]+'-'+elementAdInv[0]+'-'+elementAdInv[6]+'-'+elementAdInv[3];
                        if(detProy == programatico_Bpim_Fuente_Sec_Vig_Med){
                            totalAdInv = parseFloat(totalAdInv) + parseFloat(elementAdInv[5]);
                        }
                    });

                    ramas.push(totalAdInv);

                    //Reduccion

                    var totalRedInv = 0;

                    this.cuentasCcpetUsadasRedInv.forEach(elementRedInv => {
                        var detProy = elementRedInv[4]+'-'+elementRedInv[1]+'-'+elementRedInv[2]+'-'+elementRedInv[0]+'-'+elementRedInv[6]+'-'+elementRedInv[3];
                        if(detProy == programatico_Bpim_Fuente_Sec_Vig_Med){
                            totalRedInv = parseFloat(totalRedInv) + parseFloat(elementRedInv[5]);
                        }
                    });

                    ramas.push(totalRedInv);

                    //Credito

                    var totalCredInv = 0;

                    this.cuentasCcpetUsadasTrasladoCredInv.forEach(elementCredInv => {
                        var detProy = elementCredInv[4]+'-'+elementCredInv[1]+'-'+elementCredInv[2]+'-'+elementCredInv[0]+'-'+elementCredInv[6]+'-'+elementCredInv[3];
                        if(detProy == programatico_Bpim_Fuente_Sec_Vig_Med){
                            totalCredInv = parseFloat(totalCredInv) + parseFloat(elementCredInv[5]);
                        }
                    });

                    ramas.push(totalCredInv);

                    //Contracredito

                    var totalContraInv = 0;

                    this.cuentasCcpetUsadasTrasladoContraCredInv.forEach(elementContraCredInv => {
                        var detProy = elementContraCredInv[4]+'-'+elementContraCredInv[1]+'-'+elementContraCredInv[2]+'-'+elementContraCredInv[0]+'-'+elementContraCredInv[6]+'-'+elementContraCredInv[3];
                        if(detProy == programatico_Bpim_Fuente_Sec_Vig_Med){
                            totalContraInv = parseFloat(totalContraInv) + parseFloat(elementContraCredInv[5]);
                        }
                    });

                    ramas.push(totalContraInv);


                    totalInv = totalIniInv + totalAdInv - totalRedInv + totalCredInv - totalContraInv;

                    ramas.push(totalInv);


                    //Cdp Inversion
                    var totalCdpInv = 0;
                    this.cuentasCcpetUsadasCdpInv.forEach(elementCdpInv => {
                        var detProy = elementCdpInv[4]+'-'+elementCdpInv[1]+'-'+elementCdpInv[2]+'-'+elementCdpInv[0]+'-'+elementCdpInv[6]+'-'+elementCdpInv[3];
                        if(detProy == programatico_Bpim_Fuente_Sec_Vig_Med){
                            totalCdpInv = parseFloat(totalCdpInv) + parseFloat(elementCdpInv[5]);
                        }
                    });

                    ramas.push(totalCdpInv);

                    //Rp Inversion
                    var totalRpInv = 0;
                    this.cuentasCcpetUsadasRpInv.forEach(elementRpInv => {
                        var detProy = elementRpInv[4]+'-'+elementRpInv[1]+'-'+elementRpInv[2]+'-'+elementRpInv[0]+'-'+elementRpInv[6]+'-'+elementRpInv[3];
                        if(detProy == programatico_Bpim_Fuente_Sec_Vig_Med){
                            totalRpInv = parseFloat(totalRpInv) + parseFloat(elementRpInv[5]);
                        }
                    });

                    ramas.push(totalRpInv);

                    //Cxp Inversion
                    var totalCxpInv = 0;
                    this.cuentasCcpetUsadasCxpInv.forEach(elementCxpInv => {
                        var detProy = elementCxpInv[4]+'-'+elementCxpInv[1]+'-'+elementCxpInv[2]+'-'+elementCxpInv[0]+'-'+elementCxpInv[6]+'-'+elementCxpInv[3];
                        if(detProy == programatico_Bpim_Fuente_Sec_Vig_Med){
                            totalCxpInv = parseFloat(totalCxpInv) + parseFloat(elementCxpInv[5]);
                        }
                    });

                    ramas.push(totalCxpInv);

                    //Egreso Inversion
                    var totalEgresoInv = 0;
                    this.cuentasCcpetUsadasEgresoInv.forEach(elementEgresoInv => {
                        var detProy = elementEgresoInv[4]+'-'+elementEgresoInv[1]+'-'+elementEgresoInv[2]+'-'+elementEgresoInv[0]+'-'+elementEgresoInv[6]+'-'+elementEgresoInv[3];
                        if(detProy == programatico_Bpim_Fuente_Sec_Vig_Med){
                            totalEgresoInv = parseFloat(totalEgresoInv) + parseFloat(elementEgresoInv[5]);
                        }
                    });

                    ramas.push(totalEgresoInv);

                    var saldoInv = Number(totalInv.toFixed(2)) - Number(totalCdpInv.toFixed(2));

                    ramas.push(Number(saldoInv.toFixed(2)));

                    this.arbolInversion.push(ramas);
                }


            });
            //this.arbolInversion.sort();
            this.arbolFuncionamiento.forEach(element => {
                this.arbol.push(element);
            });

            this.arbolInversion.forEach(element => {
                this.arbol.push(element);
            });

            this.sumaTotal();
            this.sumaTotalFuncionamiento();
            this.sumaTotalInversion();
        },

        sumaTotal: async function(){
            let sumInicial = 0;
            let sumAdicion = 0;
            let sumReduccion = 0;
            let sumCredito = 0;
            let sumContraCredito = 0;
            let sumDefinitivo = 0;
            let sumCdp = 0;
            let sumRp = 0;
            let sumCxp = 0;
            let sumEgreso = 0;
            let sumSaldo = 0;
            let sumTot = [];

            await this.arbol.forEach(element => {
                if(element[0].length == 1 && element[2] == 'A'){
                    sumInicial += Number.parseFloat(element[9]);
                    sumAdicion += Number.parseFloat(element[10]);
                    sumReduccion += Number.parseFloat(element[11]);
                    sumCredito += Number.parseFloat(element[12]);
                    sumContraCredito += Number.parseFloat(element[13]);
                    sumDefinitivo += Number.parseFloat(element[14]);
                    sumCdp += Number.parseFloat(element[15]);
                    sumRp += Number.parseFloat(element[16]);
                    sumCxp += Number.parseFloat(element[17]);
                    sumEgreso += Number.parseFloat(element[18]);
                    sumSaldo += Number.parseFloat(element[19]);
                }

                if(element[0].substring(0, 2) !== '2.' && element[2] == 'C'){
                    sumInicial += Number.parseFloat(element[9]);
                    sumAdicion += Number.parseFloat(element[10]);
                    sumReduccion += Number.parseFloat(element[11]);
                    sumCredito += Number.parseFloat(element[12]);
                    sumContraCredito += Number.parseFloat(element[13]);
                    sumDefinitivo += Number.parseFloat(element[14]);
                    sumCdp += Number.parseFloat(element[15]);
                    sumRp += Number.parseFloat(element[16]);
                    sumCxp += Number.parseFloat(element[17]);
                    sumEgreso += Number.parseFloat(element[18]);
                    sumSaldo += Number.parseFloat(element[19]);
                }
            });

            sumTot.push('');
            sumTot.push('');
            sumTot.push('Total');
            sumTot.push('Total Gastos');
            sumTot.push('');
            sumTot.push('');
            sumTot.push('');
            sumTot.push(sumInicial);
            sumTot.push(sumAdicion);
            sumTot.push(sumReduccion);
            sumTot.push(sumCredito);
            sumTot.push(sumContraCredito);
            sumTot.push(sumDefinitivo);
            sumTot.push(sumCdp);
            sumTot.push(sumRp);
            sumTot.push(sumCxp);
            sumTot.push(sumEgreso);
            sumTot.push(sumSaldo);

            this.sumaTotalGastos.push(sumTot);
        },

        sumaTotalFuncionamiento: async function(){
            let sumInicial = 0;
            let sumAdicion = 0;
            let sumReduccion = 0;
            let sumCredito = 0;
            let sumContraCredito = 0;
            let sumDefinitivo = 0;
            let sumCdp = 0;
            let sumRp = 0;
            let sumCxp = 0;
            let sumEgreso = 0;
            let sumSaldo = 0;
            let sumTot = [];
            

            await this.arbolFuncionamiento.forEach(element => {

                if(element[0].length == 1){

                    sumInicial += Number.parseFloat(element[9]);
                    sumAdicion += Number.parseFloat(element[10]);
                    sumReduccion += Number.parseFloat(element[11]);
                    sumCredito += Number.parseFloat(element[12]);
                    sumContraCredito += Number.parseFloat(element[13]);
                    sumDefinitivo += Number.parseFloat(element[14]);
                    sumCdp += Number.parseFloat(element[15]);
                    sumRp += Number.parseFloat(element[16]);
                    sumCxp += Number.parseFloat(element[17]);
                    sumEgreso += Number.parseFloat(element[18]);
                    sumSaldo += Number.parseFloat(element[19]);
                }

                /* if(element[2] == 'C'){

                    sumInicial += Number.parseFloat(element[9]);
                    sumAdicion += Number.parseFloat(element[10]);
                    sumReduccion += Number.parseFloat(element[11]);
                    sumCredito += Number.parseFloat(element[12]);
                    sumContraCredito += Number.parseFloat(element[13]);
                    sumDefinitivo += Number.parseFloat(element[14]);
                    sumCdp += Number.parseFloat(element[15]);
                    sumRp += Number.parseFloat(element[16]);
                    sumCxp += Number.parseFloat(element[17]);
                    sumEgreso += Number.parseFloat(element[18]);
                    sumSaldo += Number.parseFloat(element[19]);
                } */
            });


            sumTot.push('');
            sumTot.push('');
            sumTot.push('Total');
            sumTot.push('Total Gastos de Funcionamiento, servicio de la deuda');
            sumTot.push('');
            sumTot.push('');
            sumTot.push('');
            sumTot.push(sumInicial);
            sumTot.push(sumAdicion);
            sumTot.push(sumReduccion);
            sumTot.push(sumCredito);
            sumTot.push(sumContraCredito);
            sumTot.push(sumDefinitivo);
            sumTot.push(sumCdp);
            sumTot.push(sumRp);
            sumTot.push(sumCxp);
            sumTot.push(sumEgreso);
            sumTot.push(sumSaldo);



            this.sumaTotalGastosFuncionamiento.push(sumTot);
        },

        sumaTotalInversion: async function(){
            let sumInicial = 0;
            let sumAdicion = 0;
            let sumReduccion = 0;
            let sumCredito = 0;
            let sumContraCredito = 0;
            let sumDefinitivo = 0;
            let sumCdp = 0;
            let sumRp = 0;
            let sumCxp = 0;
            let sumEgreso = 0;
            let sumSaldo = 0;
            let sumTot = [];

            await this.arbolInversion.forEach(element => {
                if(element[2] == 'C'){
                    sumInicial += Number.parseFloat(element[9]);
                    sumAdicion += Number.parseFloat(element[10]);
                    sumReduccion += Number.parseFloat(element[11]);
                    sumCredito += Number.parseFloat(element[12]);
                    sumContraCredito += Number.parseFloat(element[13]);
                    sumDefinitivo += Number.parseFloat(element[14]);
                    sumCdp += Number.parseFloat(element[15]);
                    sumRp += Number.parseFloat(element[16]);
                    sumCxp += Number.parseFloat(element[17]);
                    sumEgreso += Number.parseFloat(element[18]);
                    sumSaldo += Number.parseFloat(element[19]);
                }
            });

            sumTot.push('');
            sumTot.push('');
            sumTot.push('Total');
            sumTot.push('Total Gastos de Inversion');
            sumTot.push('');
            sumTot.push('');
            sumTot.push('');
            sumTot.push(sumInicial);
            sumTot.push(sumAdicion);
            sumTot.push(sumReduccion);
            sumTot.push(sumCredito);
            sumTot.push(sumContraCredito);
            sumTot.push(sumDefinitivo);
            sumTot.push(sumCdp);
            sumTot.push(sumRp);
            sumTot.push(sumCxp);
            sumTot.push(sumEgreso);
            sumTot.push(sumSaldo);

            this.sumaTotalGastosInversion.push(sumTot);
        },

        auxiliarDetalle: function(aux) {
            if (aux[2] == 'C') {

                var fechaIni = document.getElementById('fc_1198971545').value;
                var fechaFin = document.getElementById('fc_1198971546').value;

                var x = "ccp-auxiliarGastosCuenta.php?vig="+aux[5]+"&sec="+aux[4]+"&cuenta="+aux[0]+"&fuente="+aux[3]+"&medio="+aux[6]+"&fechaIni="+fechaIni+"&fechaFin="+fechaFin;
                window.open(x, '_blank');
                window.focus();
            }
        },

        formatNumber: function (value) {

            return numeralIntl.NumberFormat(value);
        },

        downloadExl() {

            if (this.arbol != '') {

                document.form2.action="ccp-ejecucionpresupuestal-vue-excel.php";
				document.form2.target="_BLANK";
				document.form2.submit();
            }
            else {
                Swal.fire("Faltan datos", "warning");
            }


            /* let wb = XLSX.utils.table_to_book(document.getElementById('tableId')),
                wopts = {
                    bookType: 'xls',
                    bookSST: false,
                    type: 'binary'
                },
                wbout = XLSX.write(wb, wopts);

                saveAs(new Blob([this.s2ab(wbout)], {
                type: "application/octet-stream;charset=utf-8"
                            }), "Ejecucion presupuestal de gastos.xls"); */
        },
        s2ab(s) {
            if (typeof ArrayBuffer !== 'undefind') {
                var buf = new ArrayBuffer(s.length);
                var view = new Uint8Array(buf);
                for (var i = 0; i != s.length; ++i) view[i] = s.charCodeAt(i) & 0xFF;
                return buf;
            } else {
                var buf = new Array(s.length);
                for (var i = 0; i != s.length; ++i) buf[i] = s.charCodeAt(i) & 0xFF;
                return buf;
            }
        },

        toFormData: function(obj){
            var form_data = new FormData();
            for(var key in obj){
                form_data.append(key, obj[key]);
            }
            return form_data;
        },

        auxiliarPorComprobante: function(tipo_comprobante, fuente_f, cuenta_inversion){

            let datos;
            const tipo_comp = tipo_comprobante;
            const fechaini = document.getElementById('fc_1198971545').value;
            const fechafin = document.getElementById('fc_1198971546').value;
            const seccion_presupuestal = this.selectSeccion;
            const medio_pago = this.selectMedioPago;
            const vigencia_gasto = this.selectVigenciaGasto;
            const vigencia_fiscal = this.vigencia;
            const fuente = fuente_f;
            const parts = cuenta_inversion.split('-');
            let bpim_inv = '';
            let programatico_inv = '';
            let cuenta_f = '';
            if(parts.length > 1){
                bpim_inv = parts[1].toString().trim();
                programatico_inv = parts[0].toString().trim();
            }else{
                cuenta_f = cuenta_inversion;
            }

            datos = "tipo_comprobante=" + tipo_comp + "&fechaIni=" + fechaini + "&fechaFin=" + fechafin + "&seccion_presupuestal=" + seccion_presupuestal + "&medio_pago=" + medio_pago + "&vigencia_gasto=" + vigencia_gasto + "&vigencia_fiscal=" + vigencia_fiscal + "&fuente=" + fuente + "&bpim_inv=" + bpim_inv + "&programatico_inv=" + programatico_inv + "&cuenta_f=" + cuenta_f;

            mypop=window.open("ccp-auxiliarporcomprobante.php?" + datos,'','');
            mypop.focus();

		},

        formatonumero: function(valor){
			return new Intl.NumberFormat("en-US", {style: "currency", currency: "USD"}).format(valor);
		},

        nombreSeccion: function(sec){
            let  nomSeccion = this.seccionesPresupuestales.find(x => x[0] == sec);
            return nomSeccion == undefined ? '' : nomSeccion[1];
        }

    }
});