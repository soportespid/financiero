<?php
    $URL = $URL_BBDD ?  '../../../comun.inc' :  '../../comun.inc';
    require_once "$URL";

    //[rubro, fuente, vigencia, tipo_gasto, seccion_presupuestal, medio_pago, vigencia_gasto]
    /**
     * It takes a bunch of parameters, and returns a number
     * 
     * @param parametros {
     * 
     * @return the value of .
     */
    function saldoPorRubro($parametros){

        $linkbd = conectar_v7();
	    $linkbd -> set_charset("utf8");
        //Calcular los saldos iniciales de cada rubro, fuente, tipo de gasto, seccion presupuestal, medio de pago, vigencia del gasto.
        // Inicial, funcionamiento,  deuda: ccpetinicialgastosfun.
        //adicion
        //reduccion
        //credito, contracredito
        //definitivo = inicial + adicion - reduccion + credito - contracredito.

        //INICIAL
        $inicial = 0;
        $adicion = 0;
        $reduccion = 0;
        $credito = 0;
        $contracredito = 0;
        $saldoCdp = 0;
        $saldoFinal = 0;

        //***SALDO INICIAL */
        if($parametros['tipo_gasto'] == '3'){
            $sqlr = "SELECT SUM(TB2.valorcsf), SUM(TB2.valorssf) FROM ccpproyectospresupuesto AS TB1, ccpproyectospresupuesto_presupuesto AS TB2 WHERE TB1.codigo = '".$parametros['codProyecto']."' AND TB1.idunidadej = '".$parametros['seccion_presupuestal']."' AND TB2.id_fuente = '".$parametros['fuente']."' AND TB2.indicador_producto = '".$parametros['programatico']."' AND TB1.id = TB2.codproyecto AND TB1.vigencia = '".$parametros['vigencia']."' AND TB2.vigencia_gasto = '".$parametros['vigencia_gasto']."'";
            //var_dump($sqlr);
            $resp = mysqli_query($linkbd, $sqlr);
            $row = mysqli_fetch_row($resp);

            if($parametros['medio_pago'] == 'CSF'){
                $inicial += $row[0];
            }else{
                $inicial += $row[1];
            }

            //saldo del cdp de inversion
            $sqlrValorCdp = "SELECT SUM(valor) FROM ccpetcdp_detalle WHERE bpim = '".$parametros['codProyecto']."' AND fuente = '".$parametros['fuente']."' AND indicador_producto = '".$parametros['programatico']."' AND seccion_presupuestal = '".$parametros['seccion_presupuestal']."' AND vigencia = '".$parametros['vigencia']."' AND codigo_vigenciag = '".$parametros['vigencia_gasto']."' AND medio_pago = '".$parametros['medio_pago']."' AND tipo_mov = '201'";
            $respValorCdp = mysqli_query($linkbd, $sqlrValorCdp);
            $rowValorCdp = mysqli_fetch_row($respValorCdp);

            $sqlrValorCdpRev = "SELECT SUM(valor) FROM ccpetcdp_detalle WHERE bpim = '".$parametros['codProyecto']."' AND fuente = '".$parametros['fuente']."' AND indicador_producto = '".$parametros['programatico']."' AND seccion_presupuestal = '".$parametros['seccion_presupuestal']."' AND vigencia = '".$parametros['vigencia']."' AND codigo_vigenciag = '".$parametros['vigencia_gasto']."' AND medio_pago = '".$parametros['medio_pago']."' AND tipo_mov LIKE '4%'";
            $respValorCdpRev = mysqli_query($linkbd, $sqlrValorCdpRev);
            $rowValorCdpRev = mysqli_fetch_row($respValorCdpRev);

            $saldoCdp = $rowValorCdp[0]-$rowValorCdpRev[0];
            
        }else{
            $sqlr = "SELECT SUM(valor) FROM ccpetinicialgastosfun WHERE cuenta = '".$parametros['rubro']."' AND fuente = '".$parametros['fuente']."' AND vigencia = '".$parametros['vigencia']."' AND seccion_presupuestal = '".$parametros['seccion_presupuestal']."' AND  medio_pago = '".$parametros['medio_pago']."' AND vigencia_gasto = '".$parametros['vigencia_gasto']."' ";
            /* var_dump($sqlr); */
            
            $resp = mysqli_query($linkbd, $sqlr);
            $row = mysqli_fetch_row($resp);

            $inicial += $row[0];

            $sqlrValorCdp = "SELECT SUM(valor) FROM ccpetcdp_detalle WHERE cuenta = '".$parametros['rubro']."' AND fuente = '".$parametros['fuente']."' AND vigencia = '".$parametros['vigencia']."' AND seccion_presupuestal = '".$parametros['seccion_presupuestal']."' AND codigo_vigenciag = '".$parametros['vigencia_gasto']."' AND medio_pago = '".$parametros['medio_pago']."' AND tipo_mov = '201'";
            $respValorCdp = mysqli_query($linkbd, $sqlrValorCdp);
            $rowValorCdp = mysqli_fetch_row($respValorCdp);

            $sqlrValorCdpRev = "SELECT SUM(valor) FROM ccpetcdp_detalle WHERE cuenta = '".$parametros['rubro']."' AND fuente = '".$parametros['fuente']."' AND vigencia = '".$parametros['vigencia']."' AND seccion_presupuestal = '".$parametros['seccion_presupuestal']."' AND codigo_vigenciag = '".$parametros['vigencia_gasto']."' AND medio_pago = '".$parametros['medio_pago']."' AND tipo_mov LIKE '4%'";
            $respValorCdpRev = mysqli_query($linkbd, $sqlrValorCdpRev);
            $rowValorCdpRev = mysqli_fetch_row($respValorCdpRev);

            $saldoCdp = $rowValorCdp[0] - $rowValorCdpRev[0];
        }
        /** FINAL DEL CALCULO DEL SALDO INICIAL */

        //***ADICION */

        $sqlrAdd = "SELECT SUM(valor) FROM ccpet_adiciones WHERE cuenta = '".$parametros['rubro']."' AND fuente = '".$parametros['fuente']."' AND vigencia = '".$parametros['vigencia']."' AND seccion_presupuestal = '".$parametros['seccion_presupuestal']."' AND codigo_vigenciag = '".$parametros['vigencia_gasto']."' AND medio_pago = '".$parametros['medio_pago']."' AND programatico = '".$parametros['programatico']."' AND bpim = '".$parametros['codProyecto']."'";

        $respValorAdd = mysqli_query($linkbd, $sqlrAdd);
        $rowValorAdd = mysqli_fetch_row($respValorAdd);

        $adicion = $rowValorAdd[0];

        /** FINAL DEL CALCULO DEL SALDO ADICION */

        //***REDUCCION */

        $sqlrRed = "SELECT SUM(valor) FROM ccpet_reducciones WHERE cuenta = '".$parametros['rubro']."' AND fuente = '".$parametros['fuente']."' AND vigencia = '".$parametros['vigencia']."' AND seccion_presupuestal = '".$parametros['seccion_presupuestal']."' AND codigo_vigenciag = '".$parametros['vigencia_gasto']."' AND medio_pago = '".$parametros['medio_pago']."' AND programatico = '".$parametros['programatico']."' AND bpim = '".$parametros['codProyecto']."'";

        $respValorRed = mysqli_query($linkbd, $sqlrRed);
        $rowValorRed = mysqli_fetch_row($respValorRed);

        $reduccion = $rowValorRed[0];

        /** FINAL DEL CALCULO DEL SALDO REDUCCION */

        //***TRASLADO CREDITO */

        $sqlrCredito = "SELECT SUM(valor) FROM ccpet_traslados WHERE cuenta = '".$parametros['rubro']."' AND fuente = '".$parametros['fuente']."' AND vigencia = '".$parametros['vigencia']."' AND seccion_presupuestal = '".$parametros['seccion_presupuestal']."' AND codigo_vigenciag = '".$parametros['vigencia_gasto']."' AND medio_pago = '".$parametros['medio_pago']."' AND programatico = '".$parametros['programatico']."' AND bpim = '".$parametros['codProyecto']."' AND tipo = 'C'";

        $respValorCredito = mysqli_query($linkbd, $sqlrCredito);
        $rowValorCredito = mysqli_fetch_row($respValorCredito);

        $credito = $rowValorCredito[0];

        /** FINAL DEL CALCULO DEL SALDO TRASLADO CREDITO */

        //***TRASLADO CONTRA CREDITO */

        $sqlrContraCredito = "SELECT SUM(valor) FROM ccpet_traslados WHERE cuenta = '".$parametros['rubro']."' AND fuente = '".$parametros['fuente']."' AND vigencia = '".$parametros['vigencia']."' AND seccion_presupuestal = '".$parametros['seccion_presupuestal']."' AND codigo_vigenciag = '".$parametros['vigencia_gasto']."' AND medio_pago = '".$parametros['medio_pago']."' AND programatico = '".$parametros['programatico']."' AND bpim = '".$parametros['codProyecto']."' AND tipo = 'R'";

        $respValorContraCredito = mysqli_query($linkbd, $sqlrContraCredito);
        $rowValorContraCredito = mysqli_fetch_row($respValorContraCredito);

        $contracredito = $rowValorContraCredito[0];

        /** FINAL DEL CALCULO DEL SALDO TRASLADO CONTRA CREDITO */
        
        //Calculo del saldo final sumando y restando la progrmacion presupuestal menos saldo del cdp.
        $saldoFinal = round($inicial, 2) +  round($adicion, 2) - round($reduccion, 2) + round($credito, 2) - round($contracredito, 2) - round($saldoCdp, 2);

        return $saldoFinal;
        
    }

    function saldoPorRubroIngreso($parametros){

        $linkbd = conectar_v7();
	    $linkbd -> set_charset("utf8");
        //Calcular los saldos iniciales de cada rubro, fuente, seccion presupuestal, medio de pago, vigencia del gasto.
        // Inicial: ccpetinicialing.
        //adicion
        //reduccion
        //definitivo = inicial + adicion - reduccion.

        //INICIAL
        $inicial = 0;
        $adicion = 0;
        $reduccion = 0;
        $saldoFinal = 0;

        //***SALDO INICIAL */
        
        $sqlr = "SELECT SUM(valor) FROM ccpetinicialing WHERE cuenta = '".$parametros['rubro']."' AND fuente = '".$parametros['fuente']."' AND vigencia = '".$parametros['vigencia']."' AND seccion_presupuestal = '".$parametros['seccion_presupuestal']."' AND  medio_pago = '".$parametros['medio_pago']."' AND vigencia_gasto = '".$parametros['vigencia_gasto']."' ";
        /* var_dump($sqlr); */
        
        $resp = mysqli_query($linkbd, $sqlr);
        $row = mysqli_fetch_row($resp);

        $inicial = $row[0];

        /** FINAL DEL CALCULO DEL SALDO INICIAL */

        //***ADICION */

        $sqlrAdd = "SELECT SUM(valor) FROM ccpet_adiciones WHERE cuenta = '".$parametros['rubro']."' AND fuente = '".$parametros['fuente']."' AND vigencia = '".$parametros['vigencia']."' AND seccion_presupuestal = '".$parametros['seccion_presupuestal']."' AND codigo_vigenciag = '".$parametros['vigencia_gasto']."' AND medio_pago = '".$parametros['medio_pago']."' AND programatico = '".$parametros['programatico']."' AND bpim = '".$parametros['codProyecto']."'";

        $respValorAdd = mysqli_query($linkbd, $sqlrAdd);
        $rowValorAdd = mysqli_fetch_row($respValorAdd);

        $adicion = $rowValorAdd[0];

        /** FINAL DEL CALCULO DEL SALDO ADICION */

        //***REDUCCION */

        $sqlrRed = "SELECT SUM(valor) FROM ccpet_reducciones WHERE cuenta = '".$parametros['rubro']."' AND fuente = '".$parametros['fuente']."' AND vigencia = '".$parametros['vigencia']."' AND seccion_presupuestal = '".$parametros['seccion_presupuestal']."' AND codigo_vigenciag = '".$parametros['vigencia_gasto']."' AND medio_pago = '".$parametros['medio_pago']."' AND programatico = '".$parametros['programatico']."' AND bpim = '".$parametros['codProyecto']."'";

        $respValorRed = mysqli_query($linkbd, $sqlrRed);
        $rowValorRed = mysqli_fetch_row($respValorRed);

        $reduccion = $rowValorRed[0];

        /** FINAL DEL CALCULO DEL SALDO REDUCCION */

        //Calculo del saldo final sumando y restando la progrmacion presupuestal menos saldo del cdp.
        $saldoFinal = round($inicial, 2) +  round($adicion, 2) - round($reduccion, 2);

        return $saldoFinal;
        
    }