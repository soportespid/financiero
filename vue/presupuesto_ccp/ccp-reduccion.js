var app = new Vue({ 
	el: '#myapp',
	data:
	{
		idproyecto:'',
		valida_proyecto:'NO',
		valida_presupuesto:'NO',
		valida_fuentes:'NO',
		preguntasn:'',
		//INICIO MODALES
		showMensaje: false,
		showMensajeSN: false,
		showModal: false,
		showModal2: false,
		showModal3: false,
		showModal4: false,
		showModal5: false,
		showModal6: false,
		showModal7: false,
		showModal8: false,
		showModal9: false,
		showModal10: false,
		showModal11: false,
		showModal12: false,
		showModal13: false,
		showModal14: false,
        showModal15: false,
		showModalUnidadEj: false,
		showopcion1:false,
		showopcion2:false,
		showopcion2_3:false,
		titulomensaje:'',
		contenidomensaje:'',
		colortitulosmensaje:'',
		//BUSCADORES
		search: {keyword: ''},
		searchProgram: {keywordProgram: ''},
		searchProduct: {keywordProduct: ''},
		searchCuentaPresupuestal: {keywordCuentaPresupuestal: ''},
		searchfuentes:{keyword: ''},
		searchpoliticaspublicas:{keywordpp: ''},
		searchsubproductos:{keywordsubproductos: ''},
		searchsubclase:{keywordsubclase:''},
		//MODULO PROYECTO
		unidadejecutoradobleclick: 'colordobleclik',
		sectordobleclick:'colordobleclik',
		programadobleclick:'colordobleclik',
		indicadordobleclick:'colordobleclik',
		fuentedobleclick:'colordobleclik',
		secciondobleclick:'colordobleclik',
		divisiondobleclick:'colordobleclik',
		grupodobleclick:'colordobleclik',
		clasedobleclick:'colordobleclik',
		subclasedobleclick:'colordobleclik',
		subproductodobleclick:'colordobleclik',
		parpadeomediopago:'',
		parpadeovalorrubro:'',
		codigo:'',
		vigencia: new Date().getFullYear(),
		nombre:'',
		valorproyecto:'0',
		descripcion:'',
		unidadejecutora: '',
		cunidadejecutora: '',
		sector:'',
		csector:'',
		programa:'',
		cprograma:'',
		subprograma:'',
		csubprograma:'',
		producto:'',
		cproducto:'',
		indicadorpro:'',
		cindicadorpro:'',
		sectores: [],
		unidadesejecutoras: [],
		sector_p: '',
		programas:[],
		programa_P:'',
		productos:[],
		programas_subprogramas: [],
		sombra: '',
		selecproductosa:[],
		selecproductosb:[],
		vcproducto:'',
		years:[],
		//MODULO PRESUPUESTO
		mediopago:'',
		codrubro:'',
		nrubro:'',
		nombre_r: '',
		valorrubro:'',
		cuentapre:'',
		cuentaspres:[],
		cpadre_p:'',
		selectcuetasa:[],
		selectcuetasb:[],
		selectcuetasc:[],
		selectcuetasd:[],
		selectcuetase:[],
		selectcuetasf:[],
		selectbuscar:[],
		selectbuscar1:[],
		selectbuscar2:[],
		vccuenta:'',
		secciones:[],
		seccion:'',
		seccion_p:'',
		cseccion:'',
		clasificador:'',
		identproducto:'',
		nomidentproducto:'',
		valoridentproducto:[],
		codigoidentproducto:[],
		nombreidentproducto:[],
		cclasifica:'',
		cclasificados:[],
		clasificadorescuentas: [],
		cdivision:'',
		division:'',
		division_p:'',
		divisiones:[],
		grupo:'',
		cgrupo:'',
		grupo_p:'',
		grupos:[],
		clase:'',
		cclase:'',
		clase_p:'',
		clases:[],
		subclase:'',
		csubclase:'',
		subclase_p:'',
		subClases:[],
		subproducto:'',
		csubproducto:'',
		subproductos:[],
		identidad:'',
		nitentidad:'',
		nomentidad:'',
		validacuin: false,
		validaclaservi: false,
		validaclabienes: false,
		codigocuin:'',
		codigoscuin:[],
		clacuin:'',
		vcodigocuin:'',
		valorcuin:'',
		valorsinclasifi: '',
		deshabilitar_seccion: false,
		//MODULO FUENTES PROYECTO
		cfuentef:'',
		fuentef:'',
		cmetaf:'',
		metaf:'',
		results:[],
		resultspp:[],
		infometas:[],
		vfuente:'',
		opcionmensaje:'',
		tb1:1,
		tb2:2,
		tb3:3,
		tabgroup2:1,
		tapheight1:'60%%',
		tapheight2:'60%%',
		tapheight3:'60%%',
		contador:0,
		nPoliticaPublica: '',
		codigoPoliticaPublica: '',
		vigenciaGasto: '',
		vigenciasdeGastos: [],
        adicionProyecto: '',
        proyectos: [],
        proyecto: '',
        cProyecto: '',
		showTable1: false,
		showTable2: false,
		showProyectos: false,
		codigosProyectosProductos: [],
		nombresProyectosProductos: [],
		selectProductosExistentes: [],
		idProductos: [],
		presupuestoGastos: [],
		selectPresupuestoGastos: [],
		fuenteIngreso: '',
		codigoFuenteIngreso: '',
		rubroIngreso: '',
		codigoRubroIngreso: '',
		showModalIngresos1: false,
		showModalIngresos2: false,
		showModalIngresos3: false,
		showModalIngresos4: false,
		showModalIngresos5: false,
		showModalIngresos5: false,
		showModalIngresos6: false,
		showModalIngresos7: false,
		showModalIngresos8: false,
		showModalIngresos9: false,
		showModalIngresos10: false,
		cuentasIngresos: [],
		clasificadorIngresos: '',
		clasificadosIngresos: [],
		tabheight1:'60%',
		tabheight2:'60%',
		tabheight3:'60%',
		tabgroupIngresos:1,
		showopcionIngresos1:false,
		showopcionIngresos2:false,
		showopcionIngresos3:false,
		identidadIngresos: '',
		nitentidadIngresos: '',
		codigocuinIngresos: '',
		nomentidadIngrsos: '',
		valorcuinIngresos: '',
		selectcuetascIngresos:[],
		selectcuetasdIngresos:[],
		selectcuetaseIngresos:[],
		selectcuetasfIngresos:[],
		nPoliticaPublicaIngresos: '',
		codigoPoliticaPublicaIngresos: '',
		vigenciaGastoIngresos: '',
		valorsinclasifiIngresos: '',
		seccionIngresos:'',
		cseccionIngresos:'',
		divisionIngresos: '',
		cdivisionIngresos: '',
		grupoIngresos: '',
		cgrupoIngresos: '',
		claseIngresos: '',
		cclaseIngresos: '',
		subclaseIngresos: '',
		csubclaseIngresos: '',
		subproductoIngresos: '',
		csubproductoIngresos: '',
		valorrubroIngresos: '',
		selectcuetasaIngresos: [],
		selectcuetasbIngresos: [],
		presupuestoTotalIngresos: [],
		presupuestoTotalGastos: [],
		tabGastos:1,
		actosAdministrativos: [],
		actoAdministrativo: '',
        rubrosExistentes: [],
		nombreClasificador: '',
		nombreVigenciaGasto: '',
		maximaReduccion: 0,
		maximaReduccionFuncionamiento: 0,
		showopcionFuncionamiento1: false,
		showopcionFuncionamiento2: false,
		showopcionFuncionamiento3: false,
		tabgroupFuncionamiento: 1,
		tabheightFun1:'60%',
		tabheightFun2:'60%',
		tabheightFun3:'60%',
		th1:1,
		th2:2,
		th3:3,
		tf1:1,
		tf2:2,
		tf3:3,
		showModalFuncionamiento1: false,
		showModalFuncionamiento2: false,
		showModalFuncionamiento3: false,
		showModalFuncionamiento4: false,
		showModalFuncionamiento5: false,
		showModalFuncionamiento6: false,
		showModalFuncionamiento7: false,
		showModalFuncionamiento8: false,
		showModalFuncionamiento9: false,
		showModalFuncionamiento10: false,
		fuenteFuncionamiento: '',
		codigoFuenteFuncionamiento: '',
		rubroFuncionamiento: '',
		codigoRubroFuncionamiento: '',
		cuentasFuncionamiento: [],
		clasificadosFuncionamiento: [],
		clasificadorFuncionamiento: '',
		nPoliticaPublicaFuncionamiento: '',
		codigoPoliticaPublicaFuncionamiento: '',
		vigenciaGastoFuncionamiento: '',
		identidadFuncionamiento: '',
		nitentidadFuncionamiento: '',
		codigocuinFuncionamiento: '',
		nomentidadFuncionamiento: '',
		valorcuinFuncionamiento: '',
		selectcuetascFuncionamiento: [],
		selectcuetasdFuncionamiento: [],
		valorsinclasifiFuncionamiento: '',
		selectcuetaseFuncionamiento: [],
		selectcuetasfFuncionamiento: [],
		seccionFuncionamiento: '',
		cseccionFuncionamiento: '',
		divisionFuncionamiento: '',
		cdivisionFuncionamiento: '',
		grupoFuncionamiento: '',
		cgrupoFuncionamiento: '',
		claseFuncionamiento: '',
		cclaseFuncionamiento: '',
		subclaseFuncionamiento: '',
		csubclaseFuncionamiento: '',
		subproductoFuncionamiento: '',
		csubproductoFuncionamiento: '',
		valorrubroFuncionamiento: '',
		selectcuetasaFuncionamiento: [],
		selectcuetasbFuncionamiento: [],
		tabgroupMostrar:1,
		guardarCodigosGastos: [],
		guardarCodigosIngresos: [],
		medioPagoIngresos: '',
		medioPagoFuncionamiento: '',
		valorActoAdmin: '',
		valorActoadministrativo: [],
		valorTotalGastos: 0,
		valorTotalIngresos: 0,

	},
	mounted: 
	function()
	{
		this.fetchMembers();
		this.cargayears();
		this.buscarvigencias();
		this.buscaractos();
	},
	computed:
	{
		/*years22()
		{
			const year = new Date().getFullYear() + 50
			return Array.from({length: year - 1980}, (value, index) => 1951 + index)
		}*/
	},
	methods:
	{
		cargayears: async function()
		{
			await axios.post('vue/presupuesto_ccp/ccp-bancoproyectos.php?buscar=years')
			.then(
				(response)=>
				{
					app.years=response.data.anio;
				}
			);
		},
		toggleMensajeSN:function(preg,resp)
		{
			this.showMensajeSN = !this.showMensajeSN;
			if(this.showMensajeSN==false)
			{
				switch (preg)
				{
					case '1': 
						if(resp=='S'){this.guardar();}
						break;
				}
			}
		},
		toggleMensaje: function()
		{
			this.showMensaje = !this.showMensaje;
			if(this.opcionmensaje!='' && this.showMensaje== false)
			{
				switch (this.opcionmensaje)
				{
					case '1': 
						this.opcionmensaje='';
						this.$refs.codigo.focus();
						break;
					case'2':
						this.opcionmensaje='';
						this.$refs.nombre.focus();
						break;
					case'3':
						this.opcionmensaje='';
						this.$refs.descripcion.focus();
						break;
					case '4':
						this.opcionmensaje='';
						this.$refs.valorrubro.focus();
						break;
					case '5':
						this.opcionmensaje='';
						this.$refs.fuentef.focus();
						break;
				}
			}
		},
        toggleModalProyecto: function()
        {
			this.showModal15 = !this.showModal15;
			if(this.showModal15 == true)
			{
				this.buscarProyectos();
			} 
        },
		toggleModal4: function()
		{
			this.showModal4 = !this.showModal4;
			if(this.showModal4== true)
			{
				//this.buscarcuentas('2.3');
				this.showopcion1=false;
				this.showopcion2=false;
				this.showopcion2_3=false;
				this.clasificador='';
			}
		},
		toggleModalCuentasIngresos: function()
		{
			this.showModalIngresos2 = !this.showModalIngresos2;
			if(this.showModalIngresos2== true)
			{
				this.burcarCuentasIngresos();
				this.showopcionIngresos1=false;
				this.showopcionIngresos2=false;
				this.showopcionIngresos3=false;
				this.clasificadorIngresos='';
			}
		},
		toggleModalCuentasFuncionamiento: function()
		{
			this.showModalFuncionamiento2 = !this.showModalFuncionamiento2;
			if(this.showModalFuncionamiento2== true)
			{
				this.burcarCuentasFuncionamiento();
				this.showopcionFuncionamiento1=false;
				this.showopcionFuncionamiento2=false;
				this.showopcionFuncionamiento3=false;
				this.clasificadorFuncionamiento='';
			}
		},
		toggleModal9: function()
		{
			this.subclasedobleclick='colordobleclik';
			this.showModal9 = !this.showModal9;
			if(this.showModal9== true)
			{this.buscarsubclases();}
		},
		toggleModalIngresoFuente: function()
		{
			this.fuentedobleclick='colordobleclik';
			this.showModalIngresos1 = !this.showModalIngresos1;
			if(this.showModalIngresos1== true)
			{this.buscarfuentes()}
		},
		toggleModalFuncionamientoFuente: function()
		{
			this.fuentedobleclick='colordobleclik';
			this.showModalFuncionamiento1 = !this.showModalFuncionamiento1;
			if(this.showModalFuncionamiento1== true)
			{this.buscarfuentes()}
		},
		toggleModalIngresosCuin: function()
		{
			this.showModalIngresos3 = !this.showModalIngresos3;
			if(this.showModalIngresos3== true)
			{this.buscarcodigocuin()}
		},
		toggleModalFuncionamientoCuin: function()
		{
			this.showModalFuncionamiento4 = !this.showModalFuncionamiento4;
			if(this.showModalFuncionamiento4== true)
			{this.buscarcodigocuin()}
		},
		toggleModalPoliticaIngresos: function()
		{
			this.showModalIngresos4 = !this.showModalIngresos4;
			if(this.showModalIngresos4 == true)
			{
				this.buscarpoliticaspublicas()
			}
		},
		toggleModalPoliticaFuncionamiento: function()
		{
			this.showModalFuncionamiento3 = !this.showModalFuncionamiento3;
			if(this.showModalFuncionamiento3 == true)
			{
				this.buscarpoliticaspublicas()
			}
		},
		toggleModalSecciones: function()
		{
			this.secciondobleclick='colordobleclik';
			
			this.showModalIngresos5 = !this.showModalIngresos5;
			if(this.showModalIngresos5 == true)
			{
				this.buscarsectoresIngresos();
			}	
		},
		toggleModalSeccionesFuncionamiento: function()
		{
			this.secciondobleclick='colordobleclik';
			
			this.showModalFuncionamiento5 = !this.showModalFuncionamiento5;
			if(this.showModalFuncionamiento5 == true)
			{
				this.buscarsectoresFuncionamiento();
			}	
		},
		toggleModalDivisiones: function()
		{
			this.divisiondobleclick='colordobleclik';
			if(this.cseccionIngresos!='')
			{
				this.showModalIngresos6 = !this.showModalIngresos6;
				if(this.showModalIngresos6== true)
				{this.buscardivisionesIngresos();}
			}
			else
			{
				this.secciondobleclick='parpadea colordobleclik';
				this.toggleMensaje();
				this.colortitulosmensaje='crimson';
				this.titulomensaje='Mensaje de Error';
				this.contenidomensaje='Se debe seleccionar primero una Secci\xf3n';
			}
		},
		toggleModalDivisionesFuncionamiento: function()
		{
			this.divisiondobleclick='colordobleclik';
			if(this.cseccionFuncionamiento!='')
			{
				this.showModalFuncionamiento6 = !this.showModalFuncionamiento6;
				if(this.showModalFuncionamiento6 == true)
				{this.buscardivisionesFuncionamiento();}
			}
			else
			{
				this.secciondobleclick='parpadea colordobleclik';
				this.toggleMensaje();
				this.colortitulosmensaje='crimson';
				this.titulomensaje='Mensaje de Error';
				this.contenidomensaje='Se debe seleccionar primero una Secci\xf3n';
			}
		},
		toggleModalGrupos: function()
		{
			this.grupodobleclick='colordobleclik';
			if(this.cdivisionIngresos!='')
			{
				this.showModalIngresos7 = !this.showModalIngresos7;
				if(this.showModalIngresos7== true)
				{this.buscargruposIngresos();}
			}
			else
			{
				this.divisiondobleclick='parpadea colordobleclik';
				this.toggleMensaje();
				this.colortitulosmensaje='crimson';
				this.titulomensaje='Mensaje de Error';
				this.contenidomensaje='Se debe seleccionar primero una Divisi\xf3n';
			}
		},
		toggleModalGruposFuncionamiento: function()
		{
			this.grupodobleclick='colordobleclik';
			if(this.cdivisionFuncionamiento!='')
			{
				this.showModalFuncionamiento7 = !this.showModalFuncionamiento7;
				if(this.showModalFuncionamiento7 == true)
				{this.buscargruposFuncionamiento();}
			}
			else
			{
				this.divisiondobleclick='parpadea colordobleclik';
				this.toggleMensaje();
				this.colortitulosmensaje='crimson';
				this.titulomensaje='Mensaje de Error';
				this.contenidomensaje='Se debe seleccionar primero una Divisi\xf3n';
			}
		},
		toggleModalClase: function()
		{
			this.clasedobleclick='colordobleclik';
			if(this.cgrupoIngresos!='')
			{
				this.showModalIngresos8 = !this.showModalIngresos8;
				if(this.showModalIngresos8== true)
				{this.buscarclasesIngresos();}
			}
			else
			{
				this.grupodobleclick='parpadea colordobleclik';
				this.toggleMensaje();
				this.colortitulosmensaje='crimson';
				this.titulomensaje='Mensaje de Error';
				this.contenidomensaje='Se debe seleccionar primero un Grupo';
			}
		},
		toggleModalClaseFuncionamiento: function()
		{
			this.clasedobleclick='colordobleclik';
			if(this.cgrupoFuncionamiento!='')
			{
				this.showModalFuncionamiento8 = !this.showModalFuncionamiento8;
				if(this.showModalFuncionamiento8 == true)
				{this.buscarclasesFuncionamiento();}
			}
			else
			{
				this.grupodobleclick='parpadea colordobleclik';
				this.toggleMensaje();
				this.colortitulosmensaje='crimson';
				this.titulomensaje='Mensaje de Error';
				this.contenidomensaje='Se debe seleccionar primero un Grupo';
			}
		},
		toggleModalSubClases: function()
		{
			this.subclasedobleclick='colordobleclik';
			this.showModalIngresos9 = !this.showModalIngresos9;
			if(this.showModalIngresos9== true)
			{this.buscarsubclasesIngresos();}
		},
		toggleModalSubClasesFuncionamiento: function()
		{
			this.subclasedobleclick='colordobleclik';
			this.showModalFuncionamiento9 = !this.showModalFuncionamiento9;
			if(this.showModalFuncionamiento9 == true)
			{this.buscarsubclasesFuncionamiento();}
		},
		toggleModalProducto: function()
		{
			this.showModalIngresos10 = !this.showModalIngresos10;
			if(this.showModalIngresos10== true)
			{this.buscarsubproductosIngresos()}
		},
		toggleModalProductoFuncionamiento: function()
		{
			this.showModalFuncionamiento10 = !this.showModalFuncionamiento10;
			if(this.showModalFuncionamiento10 == true)
			{this.buscarsubproductosFuncionamiento()}
		},
		fetchMembers: async function()
		{
			await axios.post('vue/ccp-producto.php')
			.then(
				(response)=>
				{
					this.sectores = response.data.codigos;
				}
			);
		},
		buscarsubproductosIngresos: function()
		{
			var codbuscador='';
			var nivbuscador='';
			if (this.csubclaseIngresos!='')
			{
				codbuscador=this.csubclaseIngresos;
				nivbuscador='5';
			}
			else if (this.cclaseIngresos!='')
			{
				codbuscador=this.cclaseIngresos;
				nivbuscador='4';
			}
			else if (this.cgrupoIngresos!='')
			{
				codbuscador=this.cgrupoIngresos;
				nivbuscador='3';
			}
			else if (this.cdivisionIngresos!='')
			{
				codbuscador=this.cdivisionIngresos;
				nivbuscador='2';
			}
			else if (this.cseccionIngresos!='')
			{
				codbuscador=this.cseccionIngresos;
				nivbuscador='1';
			}
			var keywordsubproductos = this.toFormData(this.searchsubproductos);
			axios.post('vue/presupuesto_ccp/ccp-bancoproyectos.php?buscar=subproducto&seccion='+codbuscador + '&nivel='+nivbuscador,keywordsubproductos)
			.then(
				(response)=>
				{
					app.subproductos = response.data.codigos;
				}
			);
		},
		programasp: function(sector)
		{
			app.searchProgram = {keywordProgram: ''};
			app.programas_subprogramas = [];
			app.productos = [];
			this.programa_p = '';
			app.mostrarProductos = false;
			this.sector_p = sector;
			axios.post('vue/ccp-producto.php?sector='+this.sector_p)
			.then(function(response)
			{
				app.programas_subprogramas = response.data.programas;
			});
			
		},
		buscarcuentas: function(cpadre)
		{
			app.cuentaspres = [];
			this.cpadre_p = cpadre;
			axios.post('vue/presupuesto_ccp/cuentasccpet.php?padre='+this.cpadre_p)
			.then(function(response)
			{
				app.cuentaspres = response.data.cuentaspresu;
			});
		},
		burcarCuentasIngresos: function()
		{
			axios.post('vue/presupuesto_ccp/ccp-adicionInversion.php?buscar=cuentasIngresos')
			.then(function(response)
			{
				app.cuentasIngresos = response.data.codigos;
			});
		},
		burcarCuentasFuncionamiento: function()
		{
			axios.post('vue/presupuesto_ccp/ccp-reduccion.php?buscar=cuentasFuncionamiento')
			.then(function(response)
			{
				app.cuentasFuncionamiento = response.data.codigos;
			});
		},
		buscarclasificador: async function()
		{
			await axios.post('vue/presupuesto_ccp/cuentasccpet.php?action=buscaclasificador&cuenta='+this.codrubro)
			.then((response)=>
			{
				this.cclasificados = response.data.cuentaclasifi;
				if(this.cclasificados == '')
				{
					this.tabgroup2=1;
					this.tapheight1='60%';
					this.tapheight2='60%';
					this.tapheight3='60%';
					this.showopcion1=false;
					this.showopcion2=true;
					this.showopcion2_3=false;
				}
				else
				{
					this.deshacer('12');
				}
			});
		},
		buscarcodigocuin:function()
		{
			axios.post('vue/ccp-cuin.php')
			.then(function(response)
			{
				app.codigoscuin = response.data.codigos;
			});
		},
		buscarsectores: function()
		{
			switch (this.clasificador)
			{
				case '': 
					this.codrubro='Rubro Sin Clasificar';
					break;
				case '2':
					axios.post('vue/ccp-bienestransportables.php')
					.then(function(response)
					{
						app.secciones = response.data.secciones;
					});
					break;
				case '3':
					axios.post('vue/ccp-servicios.php')
					.then(function(response){
						app.secciones = response.data.secciones;
					});
					break;
				default:
					this.codrubro='Rubro con clasificacion fuera de rango';
					break;
			}
		},
		buscarsectoresIngresos: function()
		{
			switch (this.clasificadorIngresos)
			{
				case '': 
					this.codrubro='Rubro Sin Clasificar';
					break;
				case '2':
					axios.post('vue/ccp-bienestransportables.php')
					.then(function(response)
					{
						app.secciones = response.data.secciones;
					});
					break;
				case '3':
					axios.post('vue/ccp-servicios.php')
					.then(function(response){
						app.secciones = response.data.secciones;
					});
					break;
				default:
					this.codrubro='Rubro con clasificacion fuera de rango';
					break;
			}
		},
		buscarsectoresFuncionamiento: function()
		{
			switch (this.clasificadorFuncionamiento)
			{
				case '': 
					this.codigoRubroFuncionamiento='Rubro Sin Clasificar';
					break;
				case '2':
					axios.post('vue/ccp-bienestransportables.php')
					.then(function(response)
					{
						app.secciones = response.data.secciones;
					});
					break;
				case '3':
					axios.post('vue/ccp-servicios.php')
					.then(function(response){
						app.secciones = response.data.secciones;
					});
					break;
				default:
					this.codigoRubroFuncionamiento='Rubro con clasificacion fuera de rango';
					break;
			}
		},
		buscardivisiones: function()
		{
			
			switch (this.clasificador)
			{
				case '': 
					this.codrubro='Rubro Sin Clasificar';
					break;
				case '2':
					this.seccion_p = this.cseccion;
					axios.post('vue/ccp-bienestransportables.php?seccion='+this.seccion_p)
					.then(function(response)
					{
						app.divisiones = response.data.divisiones;
					});
					break;
				case '3':
					this.seccion_p = this.cseccion;
					axios.post('vue/ccp-servicios.php?seccion='+this.seccion_p)
					.then(function(response)
					{
						app.divisiones = response.data.divisiones;
					});
					break;
				default:
					this.codrubro='Rubro con clasificacion fuera de rango';
					break;
			}
		},
		buscardivisionesIngresos: function()
		{
			
			switch (this.clasificadorIngresos)
			{
				case '': 
					this.codrubro='Rubro Sin Clasificar';
					break;
				case '2':
					this.seccion_p = this.cseccionIngresos;
					axios.post('vue/ccp-bienestransportables.php?seccion='+this.seccion_p)
					.then(function(response)
					{
						app.divisiones = response.data.divisiones;
					});
					break;
				case '3':
					this.seccion_p = this.cseccionIngresos;
					axios.post('vue/ccp-servicios.php?seccion='+this.seccion_p)
					.then(function(response)
					{
						app.divisiones = response.data.divisiones;
					});
					break;
				default:
					this.codrubro='Rubro con clasificacion fuera de rango';
					break;
			}
		},
		buscardivisionesFuncionamiento: function()
		{
			
			switch (this.clasificadorFuncionamiento)
			{
				case '': 
					this.codigoRubroFuncionamiento='Rubro Sin Clasificar';
					break;
				case '2':
					this.seccion_p = this.cseccionFuncionamiento;
					axios.post('vue/ccp-bienestransportables.php?seccion='+this.seccion_p)
					.then(function(response)
					{
						app.divisiones = response.data.divisiones;
					});
					break;
				case '3':
					this.seccion_p = this.cseccionFuncionamiento;
					axios.post('vue/ccp-servicios.php?seccion='+this.seccion_p)
					.then(function(response)
					{
						app.divisiones = response.data.divisiones;
					});
					break;
				default:
					this.codigoRubroFuncionamiento='Rubro con clasificacion fuera de rango';
					break;
			}
		},
		buscargruposIngresos: function()
		{
			switch (this.clasificadorIngresos)
			{
				case '': 
					this.codrubro='Rubro Sin Clasificar';
					break;
				case '2':
					this.division_p = this.cdivisionIngresos;
					axios.post('vue/ccp-bienestransportables.php?division='+this.division_p)
					.then(function(response)
					{
						app.grupos = response.data.grupos;
					});
					break;
				case '3':
					this.division_p = this.cdivisionIngresos;
					axios.post('vue/ccp-servicios.php?division='+this.division_p)
					.then(function(response)
					{
						app.grupos = response.data.grupos;
					});
					break;
				default:
					this.codrubro='Rubro con clasificacion fuera de rango';
					break;
			}
		},
		buscargruposFuncionamiento: function()
		{
			switch (this.clasificadorFuncionamiento)
			{
				case '': 
					this.codigoRubroFuncionamiento='Rubro Sin Clasificar';
					break;
				case '2':
					this.division_p = this.cdivisionFuncionamiento;
					axios.post('vue/ccp-bienestransportables.php?division='+this.division_p)
					.then(function(response)
					{
						app.grupos = response.data.grupos;
					});
					break;
				case '3':
					this.division_p = this.cdivisionFuncionamiento;
					axios.post('vue/ccp-servicios.php?division='+this.division_p)
					.then(function(response)
					{
						app.grupos = response.data.grupos;
					});
					break;
				default:
					this.codigoRubroFuncionamiento='Rubro con clasificacion fuera de rango';
					break;
			}
		},
		buscarclasesIngresos: function()
		{
			switch (this.clasificadorIngresos)
			{
				case '': 
					this.codrubro='Rubro Sin Clasificar';
					break;
				case '2':
					this.grupo_p = this.cgrupoIngresos;
					axios.post('vue/ccp-bienestransportables.php?grupo='+this.grupo_p)
					.then(function(response)
					{
						app.clases = response.data.clases;
					});
					break;
				case '3':
					this.grupo_p = this.cgrupoIngresos;
					axios.post('vue/ccp-servicios.php?grupo='+this.grupo_p)
					.then(function(response)
					{
						app.clases = response.data.clases;
					});
					break;
				default:
					this.codrubro='Rubro con clasificacion fuera de rango';
					break;
			}
		},
		buscarclasesFuncionamiento: function()
		{
			switch (this.clasificadorFuncionamiento)
			{
				case '': 
					this.codigoRubroFuncionamiento='Rubro Sin Clasificar';
					break;
				case '2':
					this.grupo_p = this.cgrupoFuncionamiento;
					axios.post('vue/ccp-bienestransportables.php?grupo='+this.grupo_p)
					.then(function(response)
					{
						app.clases = response.data.clases;
					});
					break;
				case '3':
					this.grupo_p = this.cgrupoFuncionamiento
					axios.post('vue/ccp-servicios.php?grupo='+this.grupo_p)
					.then(function(response)
					{
						app.clases = response.data.clases;
					});
					break;
				default:
					this.codigoRubroFuncionamiento='Rubro con clasificacion fuera de rango';
					break;
			}
		},
		buscarsubclasesIngresos: function()
		{
			switch (this.clasificadorIngresos)
			{
				case '': 
					this.codrubroIn='Rubro Sin Clasificar';
					break;
				case '2':
				{
					var codbuscador='';
					var nivbuscador='';
					if (this.cclaseIngresos!='')
					{
						codbuscador=this.cclaseIngresos;
						nivbuscador='4';
					}
					else if (this.cgrupoIngresos!='')
					{
						codbuscador=this.cgrupoIngresos;
						nivbuscador='3';
					}
					else if (this.cdivisionIngresos!='')
					{
						codbuscador=this.cdivisionIngresos;
						nivbuscador='2';
					}
					else if (this.cseccionIngresos!='')
					{
						codbuscador=this.cseccionIngresos;
						nivbuscador='1';
					}
					var keywordsubclase = this.toFormData(this.searchsubclase);
					axios.post('vue/presupuesto_ccp/ccp-bancoproyectos.php?buscar=subclaseb&seccion='+codbuscador + '&nivel='+nivbuscador,keywordsubclase)
					.then(function(response)
					{
						app.subClases = response.data.subClases;
					});
				}break;
				case '3':
				{
					var codbuscador='';
					var nivbuscador='';
					if (this.cclaseIngresos!='')
					{
						codbuscador=this.cclaseIngresos;
						nivbuscador='4';
					}
					else if (this.cgrupoIngresos!='')
					{
						codbuscador=this.cgrupoIngresos;
						nivbuscador='3';
					}
					else if (this.cdivisionIngresos!='')
					{
						codbuscador=this.cdivisionIngresos;
						nivbuscador='2';
					}
					else if (this.cseccionIngresos!='')
					{
						codbuscador=this.cseccionIngresos;
						nivbuscador='1';
					}
					var keywordsubclase = this.toFormData(this.searchsubclase);
					axios.post('vue/presupuesto_ccp/ccp-bancoproyectos.php?buscar=subclases&seccion='+codbuscador + '&nivel='+nivbuscador,keywordsubclase)
					.then(function(response)
					{
						app.subClases = response.data.subClases;
					});
				}break;
				default:
					this.codrubro='Rubro con clasificacion fuera de rango';
					break;
			}
		},
		buscarsubclasesFuncionamiento: function()
		{
			switch (this.clasificadorFuncionamiento)
			{
				case '': 
					this.codigoRubroFuncionamiento='Rubro Sin Clasificar';
					break;
				case '2':
				{
					var codbuscador='';
					var nivbuscador='';
					if (this.cclaseFuncionamiento!='')
					{
						codbuscador=this.cclaseFuncionamiento;
						nivbuscador='4';
					}
					else if (this.cgrupoFuncionamiento!='')
					{
						codbuscador=this.cgrupoFuncionamiento;
						nivbuscador='3';
					}
					else if (this.cdivisionFuncionamiento!='')
					{
						codbuscador=this.cdivisionFuncionamiento;
						nivbuscador='2';
					}
					else if (this.cseccionFuncionamiento!='')
					{
						codbuscador=this.cseccionFuncionamiento;
						nivbuscador='1';
					}
					var keywordsubclase = this.toFormData(this.searchsubclase);
					axios.post('vue/presupuesto_ccp/ccp-bancoproyectos.php?buscar=subclaseb&seccion='+codbuscador + '&nivel='+nivbuscador,keywordsubclase)
					.then(function(response)
					{
						app.subClases = response.data.subClases;
					});
				}break;
				case '3':
				{
					var codbuscador='';
					var nivbuscador='';
					if (this.cclaseFuncionamiento!='')
					{
						codbuscador=this.cclaseFuncionamiento;
						nivbuscador='4';
					}
					else if (this.cgrupoFuncionamiento!='')
					{
						codbuscador=this.cgrupoFuncionamiento;
						nivbuscador='3';
					}
					else if (this.cdivisionFuncionamiento!='')
					{
						codbuscador=this.cdivisionFuncionamiento;
						nivbuscador='2';
					}
					else if (this.cseccionFuncionamiento!='')
					{
						codbuscador=this.cseccionFuncionamiento;
						nivbuscador='1';
					}
					var keywordsubclase = this.toFormData(this.searchsubclase);
					axios.post('vue/presupuesto_ccp/ccp-bancoproyectos.php?buscar=subclases&seccion='+codbuscador + '&nivel='+nivbuscador,keywordsubclase)
					.then(function(response)
					{
						app.subClases = response.data.subClases;
					});
				}break;
				default:
					this.codigoRubroFuncionamiento='Rubro con clasificacion fuera de rango';
					break;
			}
		},
		buscarsubproductosFuncionamiento: function()
		{
			var codbuscador='';
			var nivbuscador='';

			if (this.csubclaseFuncionamiento!='')
			{
				codbuscador=this.csubclaseFuncionamiento;
				nivbuscador='5';
			}
			else if (this.cclaseFuncionamiento!='')
			{
				codbuscador=this.cclaseFuncionamiento;
				nivbuscador='4';
			}
			else if (this.cgrupoFuncionamiento!='')
			{
				codbuscador=this.cgrupoFuncionamiento;
				nivbuscador='3';
			}
			else if (this.cdivisionFuncionamiento!='')
			{
				codbuscador=this.cdivisionFuncionamiento;
				nivbuscador='2';
			}
			else if (this.cseccionFuncionamiento!='')
			{
				codbuscador=this.cseccionFuncionamiento;
				nivbuscador='1';
			}
			var keywordsubproductos = this.toFormData(this.searchsubproductos);
			axios.post('vue/presupuesto_ccp/ccp-bancoproyectos.php?buscar=subproducto&seccion='+codbuscador + '&nivel='+nivbuscador,keywordsubproductos)
			.then(
				(response)=>
				{
					app.subproductos = response.data.codigos;
				}
			);
		},
		buscargrupos: function()
		{
			switch (this.clasificador)
			{
				case '': 
					this.codrubro='Rubro Sin Clasificar';
					break;
				case '2':
					this.division_p = this.cdivision;
					axios.post('vue/ccp-bienestransportables.php?division='+this.division_p)
					.then(function(response)
					{
						app.grupos = response.data.grupos;
					});
					break;
				case '3':
					this.division_p = this.cdivision;
					axios.post('vue/ccp-servicios.php?division='+this.division_p)
					.then(function(response)
					{
						app.grupos = response.data.grupos;
					});
					break;
				default:
					this.codrubro='Rubro con clasificacion fuera de rango';
					break;
			}
		},
		buscarclases: function()
		{
			switch (this.clasificador)
			{
				case '': 
					this.codrubro='Rubro Sin Clasificar';
					break;
				case '2':
					this.grupo_p = this.cgrupo;
					axios.post('vue/ccp-bienestransportables.php?grupo='+this.grupo_p)
					.then(function(response)
					{
						app.clases = response.data.clases;
					});
					break;
				case '3':
					this.grupo_p = this.cgrupo;
					axios.post('vue/ccp-servicios.php?grupo='+this.grupo_p)
					.then(function(response)
					{
						app.clases = response.data.clases;
					});
					break;
				default:
					this.codrubro='Rubro con clasificacion fuera de rango';
					break;
			}
		},
		buscarsubclases: function()
		{
			switch (this.clasificador)
			{
				case '': 
					this.codrubro='Rubro Sin Clasificar';
					break;
				case '2':
				{
					var codbuscador='';
					var nivbuscador='';
					if (this.cclase!='')
					{
						codbuscador=this.cclase;
						nivbuscador='4';
					}
					else if (this.cgrupo!='')
					{
						codbuscador=this.cgrupo;
						nivbuscador='3';
					}
					else if (this.cdivision!='')
					{
						codbuscador=this.cdivision;
						nivbuscador='2';
					}
					else if (this.cseccion!='')
					{
						codbuscador=this.cseccion;
						nivbuscador='1';
					}
					var keywordsubclase = this.toFormData(this.searchsubclase);
					axios.post('vue/presupuesto_ccp/ccp-bancoproyectos.php?buscar=subclaseb&seccion='+codbuscador + '&nivel='+nivbuscador,keywordsubclase)
					.then(function(response)
					{
						app.subClases = response.data.subClases;
					});
				}break;
				case '3':
				{
					var codbuscador='';
					var nivbuscador='';
					if (this.cclase!='')
					{
						codbuscador=this.cclase;
						nivbuscador='4';
					}
					else if (this.cgrupo!='')
					{
						codbuscador=this.cgrupo;
						nivbuscador='3';
					}
					else if (this.cdivision!='')
					{
						codbuscador=this.cdivision;
						nivbuscador='2';
					}
					else if (this.cseccion!='')
					{
						codbuscador=this.cseccion;
						nivbuscador='1';
					}
					var keywordsubclase = this.toFormData(this.searchsubclase);
					axios.post('vue/presupuesto_ccp/ccp-bancoproyectos.php?buscar=subclases&seccion='+codbuscador + '&nivel='+nivbuscador,keywordsubclase)
					.then(function(response)
					{
						app.subClases = response.data.subClases;
					});
				}break;
				default:
					this.codrubro='Rubro con clasificacion fuera de rango';
					break;
			}
		},
		buscarfuentes: function()
		{
			axios.post('vue/ccp-fuentes-cuipo.php')
			.then(function(response)
			{
				app.results = response.data.codigos;
			});
		},
		buscarpoliticaspublicas: function()
		{
			axios.post('vue/ccp-politicapublica.php')
			.then(function(response)
			{
				app.resultspp = response.data.codigos;
			});
		},
		buscarvigencias: function()
		{
			axios.post('vue/ccp-vigenciadelgasto.php')
			.then(function(response)
			{
				app.vigenciasdeGastos = response.data.codigos;
			});
		},
		buscaractos: function()
		{
			axios.post('vue/presupuesto_ccp/ccp-reduccion.php?buscar=actos')
			.then(function(response)
			{
				app.actosAdministrativos = response.data.codigos;
				app.valorActoadministrativo = response.data.valorAdicion;
			});
		},
		buscarValorActoAdmin:function()
		{
			var actoAdmin = this.actoAdministrativo;
			this.valorActoAdmin = this.valorActoadministrativo[actoAdmin];
		},
		buscarmetas: function()
		{
			axios.post('vue/presupuesto_ccp/ccp-adicionInversion.php?tablas=metas')
			.then(function(response)
			{
				app.infometas = response.data.codigos;
			});
		},
		buscarProyectos: function()
		{
			axios.post('vue/presupuesto_ccp/ccp-reduccion.php?tablas=proyectosexistentes')
			.then(function(response)
			{
				app.proyectos = response.data.codigos;
			});
		},
		
		buscarsubproductos: function()
		{
			var codbuscador='';
			var nivbuscador='';
			if (this.csubclase!='')
			{
				codbuscador=this.csubclase;
				nivbuscador='5';
			}
			else if (this.cclase!='')
			{
				codbuscador=this.cclase;
				nivbuscador='4';
			}
			else if (this.cgrupo!='')
			{
				codbuscador=this.cgrupo;
				nivbuscador='3';
			}
			else if (this.cdivision!='')
			{
				codbuscador=this.cdivision;
				nivbuscador='2';
			}
			else if (this.cseccion!='')
			{
				codbuscador=this.cseccion;
				nivbuscador='1';
			}
			var keywordsubproductos = this.toFormData(this.searchsubproductos);
			axios.post('vue/presupuesto_ccp/ccp-bancoproyectos.php?buscar=subproducto&seccion='+codbuscador + '&nivel='+nivbuscador,keywordsubproductos)
			.then(
				(response)=>
				{
					app.subproductos = response.data.codigos;
				}
			);
		},
		searchMonitorPrograms: function()
		{
			var keywordProgram = app.toFormData(app.searchProgram);
			axios.post('vue/ccp-producto.php?action=searchProgram&sectorSearch='+this.sector_p, keywordProgram)
			.then(function(response)
			{
				app.programas_subprogramas = response.data.programas;
				if(response.data.codigos == ''){app.noMember = true;}
				else{app.noMember = false;}
			});
		},
		searchMonitor: async function()
		{
			var keyword = this.toFormData(this.search);
			await axios.post('vue/ccp-producto.php?action=searchSector', keyword)
			.then(
				(response)=>
				{
					this.sectores = response.data.codigos;
					if(response.data.codigos == ''){this.noMember = true;}
					else {this.noMember = false;}
				}
			);
		},
		searchMonitorProducts: function()
		{
			var keywordProduct = app.toFormData(app.searchProduct);
			axios.post('vue/ccp-producto.php?action=searchProduct&programSearch='+this.programa_p, keywordProduct)
			.then(function(response)
			{
				app.productos = response.data.productos;
				if(response.data.productos == ''){app.noMember = true;}
				else {app.noMember = false;}
			});
		},
		searchMonitorCuentasPresupuestales: function()// Buscar cuentas presupuestales
		{
			var keywordCuentaPresupuestal = app.toFormData(app.searchCuentaPresupuestal);
			axios.post('vue/ccp-producto.php?action=searchCuentaPresupuestal', keywordCuentaPresupuestal)
			.then(function(response)
			{
				app.cuentaspres = response.data.cuentaspresu;
				if(response.data.cuentaspresu == ''){app.noMember = true;}
				else {app.noMember = false;}
			});
		},
		searchMonitorfuentes: async function()
		{
			var keyword = this.toFormData(this.searchfuentes);
			await axios.post('vue/ccp-fuentes-cuipo.php?action=search', keyword)
			.then(function(response)
			{
				app.results = response.data.codigos;
				if(response.data.codigos == ''){app.noMember = true;}
				else {app.noMember = false;}
			});
		},
		searchMonitorpp: async function()
		{
			var keywordpp = this.toFormData(this.searchpoliticaspublicas);
			await axios.post('vue/ccp-politicapublica.php?action=search', keywordpp)
			.then(function(response)
			{
				app.resultspp = response.data.codigos;
				if(response.data.codigos == ''){app.noMember = true;}
				else {app.noMember = false;}
			});
		},
		toFormData: function(obj)
		{
			var form_data = new FormData();
			for(var key in obj)
			{
				form_data.append(key, obj[key]);
			}
			return form_data;
		},
		deplegar: function()
		{
			switch (this.clasificador)
			{	case '1':
					this.tapheight1='60%';
					this.tapheight2='60%';
					this.tapheight3='60%';
					this.tabgroup2=2;
					this.showopcion1=true;
					this.showopcion2=false;
					this.showopcion2_3=false;
					break;
				case '2':
				case '3':
					this.tapheight1='60%';
					this.tapheight2='60%';
					this.tapheight3='60%';
					this.tabgroup2=3;
					this.showopcion1=false;
					this.showopcion2=false;
					this.showopcion2_3=true;
					break;
			}
		},
		cargacuenta: async function(cod,nom,tipo,valor,clasificacion)
		{
			this.deshacer('13');
			this.nrubro = cod + ' - ' + nom;
			this.codrubro = cod;
			var vigencia = this.vigencia;

			await axios.post('vue/presupuesto_ccp/ccp-reduccion.php?buscar=cuentasGastos&id=' + this.cProyecto + '&codigo=' + cod + '&tipo=' + tipo + '&valor=' + valor + '&clasificacion=' + clasificacion + '&vigencia=' + vigencia)
			.then(function(response)
			{
				app.fuentef = response.data.codigoFuente + ' - ' + response.data.nombreFuente;
				app.cfuentef = response.data.codigoFuente;
				app.nomidentproducto = response.data.codigoIndicador + ' ' + response.data.nombreIndicador;
				app.identproducto = response.data.codigoIndicador;
				app.metaf = response.data.codigoMeta + ' ' + response.data.nombreMeta;
				app.cmetaf = response.data.codigoMeta;
				app.mediopago = tipo;
				app.clasificador = clasificacion;
				app.nPoliticaPublica = response.data.codigoPoliticaPublica + ' ' + response.data.nombrePoliticaPublica;
				app.codigoPoliticaPublica = response.data.codigoPoliticaPublica;
				app.nombreVigenciaGasto = response.data.codigoVigenciaGasto + ' ' + response.data.nombreVigenciaGasto;
				app.vigenciaGasto = response.data.codigoVigenciaGasto;
				app.maximaReduccion = response.data.saldo;

				if(clasificacion == '1')
				{
					app.identidad = response.data.idEntidad;
					app.nitentidad = response.data.nit;
					app.codigocuin = response.data.cuin;
					app.nomentidad = response.data.nombreCuin;
				}

				if(clasificacion == '2')
				{
					app.seccion = response.data.codigoSeccion + ' ' + response.data.nombreSeccion;
					app.cseccion = response.data.codigoSeccion;
					app.division = response.data.codigoDivision + ' ' + response.data.nombreDivision;
					app.cdivision = response.data.codigoDivision;
					app.grupo = response.data.codigoGrupo + ' ' + response.data.nombreGrupo;
					app.cgrupo = response.data.codigoGrupo;
					app.clase = response.data.codigoClase + ' ' + response.data.nombreClase;
					app.cclase = response.data.codigoClase;
					app.subclase = response.data.codigoSubclase + ' ' + response.data.nombreSubclase;
					app.csubclase = response.data.codigoSubclase;
					app.subproducto = response.data.codigoSubproducto + ' ' + response.data.nombreSubproducto;
					app.csubproducto = response.data.codigoSubproducto;
				}

				if(clasificacion == '3')
				{
					app.seccion = response.data.codigoSeccion + ' ' + response.data.nombreSeccion;
					app.cseccion = response.data.codigoSeccion;
					app.division = response.data.codigoDivision + ' ' + response.data.nombreDivision;
					app.cdivision = response.data.codigoDivision;
					app.grupo = response.data.codigoGrupo + ' ' + response.data.nombreGrupo;
					app.cgrupo = response.data.codigoGrupo;
					app.clase = response.data.codigoClase + ' ' + response.data.nombreClase;
					app.cclase = response.data.codigoClase;
					app.subclase = response.data.codigoSubclase + ' ' + response.data.nombreSubclase;
					app.csubclase = response.data.codigoSubclase;
				}

			});	
			
			switch (this.clasificador)
			{
				case '0':
					this.nombreClasificador = "Sin Clasificador";
					this.showopcion1=false;
					this.showopcion2=true;
					this.showopcion2_3=false;
					break;
				case '1':
					this.nombreClasificador = "Clasificador CUIN";

					this.tapheight1='60%';
					this.tapheight2='60%';
					this.tapheight3='60%';
					this.tabgroup2=2;
					this.showopcion1=true;
					this.showopcion2=false;
					this.showopcion2_3=false;
					break;
				case '2':
					this.nombreClasificador = "Clasificador bienes transportables Sec. 0 - 4";
				case '3':
					this.nombreClasificador = "Clasificador servicios Sec. 5 - 9";
					this.tapheight1='60%';
					this.tapheight2='60%';
					this.tapheight3='60%';
					this.tabgroup2=3;
					this.showopcion1=false;
					this.showopcion2=false;
					this.showopcion2_3=true;
					break;
			}
			this.showModal4 = false;
		},
		cargaCuentaIngresos: function(cod,nom,tipo)
		{
			if(tipo == 'C')
			{
				this.rubroIngreso = cod + ' - ' + nom;
				this.codigoRubroIngreso = cod;
				this.showModalIngresos2 = false;
				this.buscarclasificadorIngresos();
			}
		},
		cargaCuentaFuncionamiento: function(cod,nom,tipo)
		{
			if(tipo == 'C')
			{
				this.rubroFuncionamiento = cod + ' - ' + nom;
				this.codigoRubroFuncionamiento = cod;
				this.showModalFuncionamiento2 = false;
				this.buscarclasificadorFuncionamiento();
			}
		},
		buscarclasificadorIngresos: async function()
		{
			await axios.post('vue/presupuesto_ccp/ccp-adicionInversion.php?buscar=clasificadores&cuenta='+this.codigoRubroIngreso)
			.then((response)=>
			{
				app.clasificadosIngresos = response.data.cuentaclasifi;
				if(this.clasificadosIngresos == '')
				{
					this.tabgroup2=1;
					this.tapheight1='60%';
					this.tapheight2='60%';
					this.tapheight3='60%';
					this.showopcionIngresos1=false;
					this.showopcionIngresos2=true;
					this.showopcionIngresos3=false;
				}
				else
				{
					this.deshacer('12');
				}
			});
		},
		buscarclasificadorFuncionamiento: async function()
		{
			await axios.post('vue/presupuesto_ccp/ccp-reduccion.php?buscar=clasificadores&cuenta='+this.codigoRubroFuncionamiento)
			.then((response)=>
			{
				app.clasificadosFuncionamiento = response.data.cuentaclasifi;
				if(this.clasificadosFuncionamiento == '')
				{
					this.tabgroupFuncionamiento=1;
					this.tabheightFun1='60%';
					this.tabheightFun2='60%';
					this.tabheightFun3='60%';
					this.showopcionFuncionamiento1=false;
					this.showopcionFuncionamiento2=true;
					this.showopcionFuncionamiento3=false;
				}
				else
				{
					this.deshacer('12');
				}
			});
		},
		desplegar: function()
		{
			switch (this.clasificadorIngresos)
			{	case '1':
					this.tabheight1='60%';
					this.tabheight2='60%';
					this.tabheight3='60%';
					this.tabgroup2=2;
					this.showopcionIngresos1=true;
					this.showopcionIngresos2=false;
					this.showopcionIngresos3=false;
					break;
				case '2':
				case '3':
					this.tabheight1='60%';
					this.tabheight2='60%';
					this.tabheight3='60%';
					this.tabgroup2=3;
					this.showopcionIngresos1=false;
					this.showopcionIngresos2=false;
					this.showopcionIngresos3=true;
					break;
			}
		},
		desplegarFuncionamiento: function()
		{
			switch (this.clasificadorFuncionamiento)
			{	case '1':
					this.tabgroupFuncionamiento=2;
					this.showopcionFuncionamiento1=true;
					this.showopcionFuncionamiento2=false;
					this.showopcionFuncionamiento3=false;
					break;
				case '2':
				case '3':
					this.tabgroupFuncionamiento=3;
					this.showopcionFuncionamiento1=false;
					this.showopcionFuncionamiento2=false;
					this.showopcionFuncionamiento3=true;
					break;
			}
		},
		cargacodigocuin:function(ident,nitent,noment,codcuin)
		{
			if(this.identidad!=ident)
			{
				this.identidad=ident;
				this.nitentidad=nitent;
				this.codigocuin=codcuin;
				this.nomentidad=noment
				this.showModal11 = false;
			}
		},
		cargacodigocuinIngresos:function(ident,nitent,noment,codcuin)
		{
			if(this.identidadIngresos!=ident)
			{
				this.identidadIngresos=ident;
				this.nitentidadIngresos=nitent;
				this.codigocuinIngresos=codcuin;
				this.nomentidadIngrsos=noment
				this.showModalIngresos3 = false;
			}
		},
		cargacodigocuinFuncionamiento:function(ident,nitent,noment,codcuin)
		{
			if(this.identidadFuncionamiento!=ident)
			{
				this.identidadFuncionamiento=ident;
				this.nitentidadFuncionamiento=nitent;
				this.codigocuinFuncionamiento=codcuin;
				this.nomentidadFuncionamiento=noment
				this.showModalFuncionamiento4 = false;
			}
		},
		cargaseccion: function(cod,nom)
		{
			if(this.cseccion!=cod)
			{
				this.cseccion=cod;
				this.seccion=cod+" - "+nom;
				this.showModal5 = false;
				this.deshacer('3');
			}
			else{this.showModal5 = false;}
		},
		cargaseccionIngresos: function(cod,nom)
		{
			if(this.cseccionIngresos!=cod)
			{
				this.cseccionIngresos=cod;
				this.seccionIngresos=cod+" - "+nom;
				this.showModalIngresos5 = false;
				this.deshacer('3');
			}
			else{this.showModal5 = false;}
		},
		cargaseccionFuncionamiento: function(cod,nom)
		{
			if(this.cseccionFuncionamiento!=cod)
			{
				this.cseccionFuncionamiento=cod;
				this.seccionFuncionamiento=cod+" - "+nom;
				this.showModalFuncionamiento5 = false;
				this.deshacer('3');
			}
			else{this.showModal5 = false;}
		},
		cargadivision: function(cod,nom)
		{
			if(this.cdivision!=cod)
			{
				this.cdivision=cod;
				this.division=cod+" - "+nom;
				this.showModal6 = false;
				this.deshacer('4');
			}
			else{this.showModal6 = false;}
		},
		cargadivisionIngresos: function(cod,nom)
		{
			if(this.cdivisionIngresos!=cod)
			{
				this.cdivisionIngresos=cod;
				this.divisionIngresos=cod+" - "+nom;
				this.showModalIngresos6 = false;
				this.deshacer('4');
			}
			else{this.showModal6 = false;}
		},
		cargadivisionFuncionamiento: function(cod,nom)
		{
			if(this.cdivisionFuncionamiento != cod)
			{
				this.cdivisionFuncionamiento = cod;
				this.divisionFuncionamiento = cod+" - "+nom;
				this.showModalFuncionamiento6 = false;
			}
			else
			{
				this.showModalFuncionamiento6 = false;
			}
		},
		cargagrupo: function(cod,nom)
		{
			if(this.cgrupo!=cod)
			{
				this.cgrupo=cod;
				this.grupo=cod+" - "+nom;
				this.showModal7 = false;
				this.deshacer('5');
			}
			else{this.showModal7 = false;}
		},
		cargagrupoIngresos: function(cod,nom)
		{
			if(this.cgrupoIngresos!=cod)
			{
				this.cgrupoIngresos=cod;
				this.grupoIngresos=cod+" - "+nom;
				this.showModalIngresos7 = false;
				this.deshacer('5');
			}
			else{this.showModal7 = false;}
		},
		cargagrupoFuncionamiento: function(cod,nom)
		{
			if(this.cgrupoFuncionamiento!=cod)
			{
				this.cgrupoFuncionamiento=cod;
				this.grupoFuncionamiento=cod+" - "+nom;
				this.showModalFuncionamiento7 = false;
			}
			else{this.showModalFuncionamiento7 = false;}
		},
		cargaclase: function(cod,nom)
		{
			if(this.cclase!=cod)
			{
				this.cclase=cod;
				this.clase=cod+" - "+nom;
				this.showModal8 = false;
				this.deshacer('6');
			}
			else{this.showModal8 = false;}
		},
		cargaclaseIngresos: function(cod,nom)
		{
			if(this.cclaseIngresos!=cod)
			{
				this.cclaseIngresos=cod;
				this.claseIngresos=cod+" - "+nom;
				this.showModalIngresos8 = false;
				this.deshacer('6');
			}
			else{this.showModalIngresos8 = false;}
		},
		cargaclaseFuncionamiento: function(cod,nom)
		{
			if(this.cclaseFuncionamiento!=cod)
			{
				this.cclaseFuncionamiento=cod;
				this.claseFuncionamiento=cod+" - "+nom;
				this.showModalFuncionamiento8 = false;
			}
			else{this.showModalFuncionamiento8 = false;}
		},
		cargasubclase: function(cod,nom)
		{
			if(this.csubclase!=cod)
			{
				this.csubclase=cod;
				this.subclase=cod+" - "+nom;
				if(this.clasificador=='2')
				{
					if(this.cseccion=='')
					{
						this.cseccion=cod.substr(0,1);
						axios.post('vue/presupuesto_ccp/ccp-bancoproyectos.php?buscar=nombregrupo&grupo='+this.cseccion)
						.then((response)=>{this.seccion=this.cseccion+" - "+response.data.codigos[0][0];});
					}
					if(this.cdivision=='')
					{
						this.cdivision=cod.substr(0,2);
						axios.post('vue/presupuesto_ccp/ccp-bancoproyectos.php?buscar=nombregrupo&grupo='+this.cdivision)
						.then((response)=>{this.division=this.cdivision+" - "+response.data.codigos[0][0];});
					}
					if(this.cgrupo=='')
					{
						this.cgrupo=cod.substr(0,3);
						axios.post('vue/presupuesto_ccp/ccp-bancoproyectos.php?buscar=nombregrupo&grupo='+this.cgrupo)
						.then((response)=>{this.grupo=this.cgrupo+" - "+response.data.codigos[0][0];});
					}
					if(this.cclase=='')
					{
						this.cclase=cod.substr(0,4);
						axios.post('vue/presupuesto_ccp/ccp-bancoproyectos.php?buscar=nombregrupo&grupo='+this.cclase)
						.then((response)=>{this.clase=this.cclase+" - "+response.data.codigos[0][0];});
					}
				}
				else
				{
					if(this.cseccion=='')
					{
						this.cseccion=cod.substr(0,1);
						axios.post('vue/presupuesto_ccp/ccp-bancoproyectos.php?buscar=nombregrupos&grupo='+this.cseccion)
						.then((response)=>{this.seccion=this.cseccion+" - "+response.data.codigos[0][0];});
					}
					if(this.cdivision=='')
					{
						this.cdivision=cod.substr(0,2);
						axios.post('vue/presupuesto_ccp/ccp-bancoproyectos.php?buscar=nombregrupos&grupo='+this.cdivision)
						.then((response)=>{this.division=this.cdivision+" - "+response.data.codigos[0][0];});
					}
					if(this.cgrupo=='')
					{
						this.cgrupo=cod.substr(0,3);
						axios.post('vue/presupuesto_ccp/ccp-bancoproyectos.php?buscar=nombregrupos&grupo='+this.cgrupo)
						.then((response)=>{this.grupo=this.cgrupo+" - "+response.data.codigos[0][0];});
					}
					if(this.cclase=='')
					{
						this.cclase=cod.substr(0,4);
						axios.post('vue/presupuesto_ccp/ccp-bancoproyectos.php?buscar=nombregrupos&grupo='+this.cclase)
						.then((response)=>{this.clase=this.cclase+" - "+response.data.codigos[0][0];});
					}
				}
				this.showModal9 = false;
				this.deshacer('7');
			}
			else{this.showModal9 = false;}
		},
		cargasubclaseIngresos: function(cod,nom)
		{
			if(this.csubclaseIngresos!=cod)
			{
				this.csubclaseIngresos=cod;
				this.subclaseIngresos=cod+" - "+nom;
				if(this.clasificadorIngresos=='2')
				{
					if(this.cseccionIngresos=='')
					{
						this.cseccionIngresos=cod.substr(0,1);
						axios.post('vue/presupuesto_ccp/ccp-bancoproyectos.php?buscar=nombregrupo&grupo='+this.cseccion)
						.then((response)=>{this.seccion=this.cseccion+" - "+response.data.codigos[0][0];});
					}
					if(this.cdivisionIngresos=='')
					{
						this.cdivisionIngresos=cod.substr(0,2);
						axios.post('vue/presupuesto_ccp/ccp-bancoproyectos.php?buscar=nombregrupo&grupo='+this.cdivision)
						.then((response)=>{this.division=this.cdivision+" - "+response.data.codigos[0][0];});
					}
					if(this.cgrupoIngresos=='')
					{
						this.cgrupoIngresos=cod.substr(0,3);
						axios.post('vue/presupuesto_ccp/ccp-bancoproyectos.php?buscar=nombregrupo&grupo='+this.cgrupo)
						.then((response)=>{this.grupo=this.cgrupo+" - "+response.data.codigos[0][0];});
					}
					if(this.cclaseIngresos=='')
					{
						this.cclase=cod.substr(0,4);
						axios.post('vue/presupuesto_ccp/ccp-bancoproyectos.php?buscar=nombregrupo&grupo='+this.cclase)
						.then((response)=>{this.clase=this.cclase+" - "+response.data.codigos[0][0];});
					}
				}
				else
				{
					if(this.cseccionIngresos=='')
					{
						this.cseccionIngresos=cod.substr(0,1);
						axios.post('vue/presupuesto_ccp/ccp-bancoproyectos.php?buscar=nombregrupos&grupo='+this.cseccion)
						.then((response)=>{this.seccion=this.cseccion+" - "+response.data.codigos[0][0];});
					}
					if(this.cdivisionIngresos=='')
					{
						this.cdivisionIngresos=cod.substr(0,2);
						axios.post('vue/presupuesto_ccp/ccp-bancoproyectos.php?buscar=nombregrupos&grupo='+this.cdivision)
						.then((response)=>{this.division=this.cdivision+" - "+response.data.codigos[0][0];});
					}
					if(this.cgrupoIngresos=='')
					{
						this.cgrupoIngresos=cod.substr(0,3);
						axios.post('vue/presupuesto_ccp/ccp-bancoproyectos.php?buscar=nombregrupos&grupo='+this.cgrupo)
						.then((response)=>{this.grupo=this.cgrupo+" - "+response.data.codigos[0][0];});
					}
					if(this.cclaseIngresos=='')
					{
						this.cclase=cod.substr(0,4);
						axios.post('vue/presupuesto_ccp/ccp-bancoproyectos.php?buscar=nombregrupos&grupo='+this.cclase)
						.then((response)=>{this.clase=this.cclase+" - "+response.data.codigos[0][0];});
					}
				}
				this.showModalIngresos9 = false;
				this.deshacer('7');
			}
			else{this.showModalIngresos9 = false;}
		},
		cargasubclaseFuncionamiento: function(cod,nom)
		{
			if(this.csubclaseFuncionamiento!=cod)
			{
				this.csubclaseFuncionamiento=cod;
				this.subclaseFuncionamiento=cod+" - "+nom;
				if(this.clasificadorFuncionamiento=='2')
				{
					if(this.cseccionFuncionamiento=='')
					{
						this.cseccionFuncionamiento=cod.substr(0,1);
						axios.post('vue/presupuesto_ccp/ccp-bancoproyectos.php?buscar=nombregrupo&grupo='+this.cseccionFuncionamiento)
						.then((response)=>{this.seccionFuncionamiento=this.cargaseccionFuncionamiento+" - "+response.data.codigos[0][0];});
					}
					if(this.cdivisionFuncionamiento=='')
					{
						this.cdivisionFuncionamiento=cod.substr(0,2);
						axios.post('vue/presupuesto_ccp/ccp-bancoproyectos.php?buscar=nombregrupo&grupo='+this.cdivisionFuncionamiento)
						.then((response)=>{this.divisionFuncionamiento=this.cdivisionFuncionamiento+" - "+response.data.codigos[0][0];});
					}
					if(this.cgrupoFuncionamiento=='')
					{
						this.cgrupoFuncionamiento=cod.substr(0,3);
						axios.post('vue/presupuesto_ccp/ccp-bancoproyectos.php?buscar=nombregrupo&grupo='+this.cgrupoFuncionamiento)
						.then((response)=>{this.grupoFuncionamiento=this.cgrupoFuncionamiento+" - "+response.data.codigos[0][0];});
					}
					if(this.cclaseFuncionamiento=='')
					{
						this.cclaseFuncionamiento=cod.substr(0,4);
						axios.post('vue/presupuesto_ccp/ccp-bancoproyectos.php?buscar=nombregrupo&grupo='+this.cclaseFuncionamiento)
						.then((response)=>{this.claseFuncionamiento=this.csubclaseFuncionamiento+" - "+response.data.codigos[0][0];});
					}
				}
				else
				{
					if(this.cseccionFuncionamiento=='')
					{
						this.cseccionFuncionamiento=cod.substr(0,1);
						axios.post('vue/presupuesto_ccp/ccp-bancoproyectos.php?buscar=nombregrupos&grupo='+this.cseccionFuncionamiento)
						.then((response)=>{this.seccionFuncionamiento=this.cseccionFuncionamiento+" - "+response.data.codigos[0][0];});
					}
					if(this.cdivisionFuncionamiento=='')
					{
						this.cdivisionFuncionamiento=cod.substr(0,2);
						axios.post('vue/presupuesto_ccp/ccp-bancoproyectos.php?buscar=nombregrupos&grupo='+this.cdivisionFuncionamiento)
						.then((response)=>{this.divisionFuncionamiento=this.cdivisionFuncionamiento+" - "+response.data.codigos[0][0];});
					}
					if(this.cgrupoFuncionamiento=='')
					{
						this.cgrupoFuncionamiento=cod.substr(0,3);
						axios.post('vue/presupuesto_ccp/ccp-bancoproyectos.php?buscar=nombregrupos&grupo='+this.cargagrupoFuncionamiento)
						.then((response)=>{this.grupoFuncionamiento=this.cgrupoFuncionamiento+" - "+response.data.codigos[0][0];});
					}
					if(this.cclaseFuncionamiento=='')
					{
						this.cclaseFuncionamiento=cod.substr(0,4);
						axios.post('vue/presupuesto_ccp/ccp-bancoproyectos.php?buscar=nombregrupos&grupo='+this.cclaseFuncionamiento)
						.then((response)=>{this.claseFuncionamiento=this.cclaseFuncionamiento+" - "+response.data.codigos[0][0];});
					}
				}
				this.showModalFuncionamiento9 = false;
			}
			else{this.showModalFuncionamiento9 = false;}
		},
		cargafuente: function(cod,nom)
		{
			this.cfuentef=cod;
			this.fuentef=cod+" - "+nom;
			this.showModal10 = false;
		},
		cargaFuenteIngresos: function(cod,nom)
		{
			this.codigoFuenteIngreso=cod;
			this.fuenteIngreso=cod+" - "+nom;
			this.showModalIngresos1 = false;
		},
		cargaFuenteFuncionamiento: function(cod,nom)
		{
			this.codigoFuenteFuncionamiento=cod;
			this.fuenteFuncionamiento=cod+" - "+nom;
			this.showModalFuncionamiento1 = false;
		},
		cargapp: function(cod,nom)
		{
			this.codigoPoliticaPublica = cod;
			this.nPoliticaPublica = cod + " - " + nom;
			this.showModal14 = false;
		},
		cargapoliticaIngresos: function(cod,nom)
		{
			this.codigoPoliticaPublicaIngresos = cod;
			this.nPoliticaPublicaIngresos = cod + " - " + nom;
			this.showModalIngresos4 = false;
		},
		cargapoliticaFuncionamiento: function(cod,nom)
		{
			this.codigoPoliticaPublicaFuncionamiento = cod;
			this.nPoliticaPublicaFuncionamiento = cod + " - " + nom;
			this.showModalFuncionamiento3 = false;
		},
		cargaProyectosExistentes: function(id,nom,descripcion,unidadejecutora,codigo)
		{
			this.deshacer('13');
            this.rubrosExistentes = [];
            this.selectProductosExistentes= [];
			this.buscarProyectosProductos(id,unidadejecutora);
			this.proyecto = id + " - " + nom;
			this.cProyecto = id;
			this.codigo = codigo;
			this.nombre = nom;
			this.descripcion = descripcion;
			this.showModal15 = false;
		},
		buscarProyectosProductos: async function(id,unidadejecutora)
		{
			await axios.post('vue/presupuesto_ccp/ccp-reduccion.php?buscar=proyectosProductos&id=' + id + '&ejecutora=' + unidadejecutora)
			.then(function(response)
			{
				//console.log(response.data);
				app.codigosProyectosProductos = response.data.codigos;
				app.nombresProyectosProductos = response.data.nombres;
				app.presupuestoGastos = response.data.presupuesto;
			});	

			this.sector = this.codigosProyectosProductos[0][0] + " - " + this.nombresProyectosProductos[0][0];
			this.programa = this.codigosProyectosProductos[0][1] + " - " + this.nombresProyectosProductos[0][1];
			this.subprograma = this.codigosProyectosProductos[0][2] + " - " + this.nombresProyectosProductos[0][2];
			this.unidadejecutora = unidadejecutora + " - " + this.nombresProyectosProductos[0][3];

			for (let i = 0; i < this.codigosProyectosProductos.length; i++) 
			{
				this.csector = this.codigosProyectosProductos[i][0];
				this.cprograma = this.codigosProyectosProductos[i][1];
				this.csubprograma = this.codigosProyectosProductos[i][2];
				this.producto = this.codigosProyectosProductos[i][3] + " - " + this.nombresProyectosProductos[i][4];
				this.cproducto = this.codigosProyectosProductos[i][3];
				this.indicadorpro = this.codigosProyectosProductos[i][4] + " - " + this.nombresProyectosProductos[i][5];
				this.cindicadorpro = this.codigosProyectosProductos[i][4];
                var valorProducto = this.codigosProyectosProductos[i][5];

                //Producto
				var varauxia=[this.cproducto,this.producto,this.cindicadorpro,this.indicadorpro,this.csector,this.cprograma,this.csubprograma,this.contador,valorProducto];

				this.valoridentproducto[this.contador]=0;
				this.codigoidentproducto[this.contador]=this.cindicadorpro;
				this.nombreidentproducto[this.contador]=this.indicadorpro;

				this.selectProductosExistentes.push(varauxia);
				this.idProductos=this.selectProductosExistentes;
				this.cindicadorpro=this.indicadorpro=this.cproducto=this.producto='';
				this.sectordobleclick='';
				this.programadobleclick= '';
				this.contador++;
			}
			
			for (let j = 0; j < this.presupuestoGastos.length; j++) 
			{
				var codigoRubro = this.presupuestoGastos[j][0];
                var nombreRubro = this.presupuestoGastos[j][9];
				var clasificacion = this.presupuestoGastos[j][4];
				var valor;
                var medioPago = this.presupuestoGastos[j][1];
				var tipo = this.presupuestoGastos[j][10];
				var subclase = this.presupuestoGastos[j][5];
				var subproducto = this.presupuestoGastos[j][6];
				var fuente = this.presupuestoGastos[j][7];
				var indicador = this.presupuestoGastos[j][8];

				if(medioPago == 'CSF')
				{
					valor = this.presupuestoGastos[j][2];
				}
				else
				{
					valor = this.presupuestoGastos[j][3];
				}

                var rubros = [codigoRubro,nombreRubro,tipo,valor,medioPago,clasificacion,subclase,subproducto,fuente,indicador];
                
                this.rubrosExistentes.push(rubros);

				this.valorproyecto = Number(this.valorproyecto) + Number(valor);
				
			}
			
		},
		cargametas: function(id,cod,nom)
		{
			this.cmetaf=id;
			this.metaf=cod+" - "+nom;
			this.showModal13 = false;
		},
		cargasubproducto: function(cod,nom)
		{
			this.csubproducto=cod;
			this.subproducto=cod+" - "+nom;
			if(this.cseccion=='')
			{
				this.cseccion=cod.substr(0,1);
				axios.post('vue/presupuesto_ccp/ccp-bancoproyectos.php?buscar=nombregrupo&grupo='+this.cseccion)
				.then((response)=>{this.seccion=this.cseccion+" - "+response.data.codigos[0][0];});
			}
			if(this.cdivision=='')
			{
				this.cdivision=cod.substr(0,2);
				axios.post('vue/presupuesto_ccp/ccp-bancoproyectos.php?buscar=nombregrupo&grupo='+this.cdivision)
				.then((response)=>{this.division=this.cdivision+" - "+response.data.codigos[0][0];});
			}
			if(this.cgrupo=='')
			{
				this.cgrupo=cod.substr(0,3);
				axios.post('vue/presupuesto_ccp/ccp-bancoproyectos.php?buscar=nombregrupo&grupo='+this.cgrupo)
				.then((response)=>{this.grupo=this.cgrupo+" - "+response.data.codigos[0][0];});
			}
			if(this.cclase=='')
			{
				this.cclase=cod.substr(0,4);
				axios.post('vue/presupuesto_ccp/ccp-bancoproyectos.php?buscar=nombregrupo&grupo='+this.cclase)
				.then((response)=>{this.clase=this.cclase+" - "+response.data.codigos[0][0];});
			}
			if(this.csubclase=='')
			{
				this.csubclase=cod.substr(0,5);
				axios.post('vue/presupuesto_ccp/ccp-bancoproyectos.php?buscar=nombregrupo&grupo='+this.csubclase)
				.then((response)=>{this.subclase=this.csubclase+" - "+response.data.codigos[0][0];});
			}
			this.showModal12 = false;
		},
		cargasubproductoIngresos: function(cod,nom)
		{
			this.csubproductoIngresos=cod;
			this.subproductoIngresos=cod+" - "+nom;
			if(this.cseccionIngresos=='')
			{
				this.cseccionIngresos=cod.substr(0,1);
				axios.post('vue/presupuesto_ccp/ccp-bancoproyectos.php?buscar=nombregrupo&grupo='+this.cseccion)
				.then((response)=>{this.seccion=this.cseccion+" - "+response.data.codigos[0][0];});
			}
			if(this.cdivisionIngresos=='')
			{
				this.cdivisionIngresos=cod.substr(0,2);
				axios.post('vue/presupuesto_ccp/ccp-bancoproyectos.php?buscar=nombregrupo&grupo='+this.cdivision)
				.then((response)=>{this.division=this.cdivision+" - "+response.data.codigos[0][0];});
			}
			if(this.cgrupoIngresos=='')
			{
				this.cgrupoIngresos=cod.substr(0,3);
				axios.post('vue/presupuesto_ccp/ccp-bancoproyectos.php?buscar=nombregrupo&grupo='+this.cgrupo)
				.then((response)=>{this.grupo=this.cgrupo+" - "+response.data.codigos[0][0];});
			}
			if(this.cclaseIngresos=='')
			{
				this.cclaseIngresos=cod.substr(0,4);
				axios.post('vue/presupuesto_ccp/ccp-bancoproyectos.php?buscar=nombregrupo&grupo='+this.cclase)
				.then((response)=>{this.clase=this.cclase+" - "+response.data.codigos[0][0];});
			}
			if(this.csubclaseIngresos=='')
			{
				this.csubclaseIngresos=cod.substr(0,5);
				axios.post('vue/presupuesto_ccp/ccp-bancoproyectos.php?buscar=nombregrupo&grupo='+this.csubclase)
				.then((response)=>{this.subclase=this.csubclase+" - "+response.data.codigos[0][0];});
			}
			this.showModalIngresos10 = false;
		},
		cargasubproductoFuncionamiento: function(cod,nom)
		{
			this.csubproductoFuncionamiento=cod;
			this.subproductoFuncionamiento=cod+" - "+nom;
			if(this.cseccionFuncionamiento=='')
			{
				this.cseccionFuncionamiento=cod.substr(0,1);
				axios.post('vue/presupuesto_ccp/ccp-bancoproyectos.php?buscar=nombregrupo&grupo='+this.cseccionFuncionamiento)
				.then((response)=>{this.seccionFuncionamiento=this.cargaseccionFuncionamiento+" - "+response.data.codigos[0][0];});
			}
			if(this.cdivisionFuncionamiento=='')
			{
				this.cdivisionFuncionamiento=cod.substr(0,2);
				axios.post('vue/presupuesto_ccp/ccp-bancoproyectos.php?buscar=nombregrupo&grupo='+this.cdivisionFuncionamiento)
				.then((response)=>{this.divisionFuncionamiento=this.cdivisionFuncionamiento+" - "+response.data.codigos[0][0];});
			}
			if(this.cgrupoFuncionamiento=='')
			{
				this.cgrupoFuncionamiento=cod.substr(0,3);
				axios.post('vue/presupuesto_ccp/ccp-bancoproyectos.php?buscar=nombregrupo&grupo='+this.cgrupoFuncionamiento)
				.then((response)=>{this.grupoFuncionamiento=this.cargagrupoFuncionamiento+" - "+response.data.codigos[0][0];});
			}
			if(this.cclaseFuncionamiento=='')
			{
				this.cclaseFuncionamiento=cod.substr(0,4);
				axios.post('vue/presupuesto_ccp/ccp-bancoproyectos.php?buscar=nombregrupo&grupo='+this.cclaseFuncionamiento)
				.then((response)=>{this.claseFuncionamiento=this.cclaseFuncionamiento+" - "+response.data.codigos[0][0];});
			}
			if(this.csubclaseFuncionamiento=='')
			{
				this.csubclaseFuncionamiento=cod.substr(0,5);
				axios.post('vue/presupuesto_ccp/ccp-bancoproyectos.php?buscar=nombregrupo&grupo='+this.csubclaseFuncionamiento)
				.then((response)=>{this.subclaseFuncionamiento=this.csubclaseFuncionamiento+" - "+response.data.codigos[0][0];});
			}
			this.showModalFuncionamiento10 = false;
		},
		eliminaproducto: function(index)
		{
			this.selecproductosa.splice(index, 1);
			this.selecproductosb.splice(index, 1);
			if(this.selecproductosa.length==0){this.programadobleclick=this.sectordobleclick='colordobleclik';}
		},
		agregarcuenta: function()
		{
			if(this.valorrubro <= this.maximaReduccion)
			{
				var bandera01=0;
				if(this.selectbuscar.length>0)
				{
					for (const auxarray in this.selectbuscar)
					{
						var auxlong=`${this.selectbuscar[auxarray]}`;
						var auxbusq=this.cfuentef + '<->' + this.clasificador + '<->' + this.codrubro + '<->' + this.csubclase + '<->' +this.identproducto;
						
					}
				}
				
			if(!this.valorrubro.includes('.'))
			{
				if(this.valorrubro != '')
				{
					if(this.codrubro != '')
					{	
						if(bandera01==0)
						{
							
							var tempidpriducto = this.identproducto;
							var codigoidt=this.codigoidentproducto[tempidpriducto];
							var nombreidt=this.nombreidentproducto[tempidpriducto];
							var unionaux = this.cfuentef + '<->' + this.clasificador + '<->' + this.codrubro + '<->' + this.csubclase + '<->' + tempidpriducto;
							this.tabgroup2=3;
							this.valoridentproducto[tempidpriducto]=Number(this.valoridentproducto[tempidpriducto]) + Number(this.valorrubro);
							this.valorproyecto = Number(this.valorproyecto) + Number(this.valorrubro);
							var varauxia=[this.clasificador,this.nrubro,this.subclase,this.valorrubro,this.fuentef,this.mediopago, this.subproducto,tempidpriducto,nombreidt,this.metaf,this.nPoliticaPublica,this.vigenciaGasto];
							
							var varauxib=[this.codrubro,this.clasificador,this.cseccion,this.cdivision,this.cgrupo,this.cclase,this.csubclase, this.valorrubro,this.cfuentef,this.mediopago,this.csubproducto,tempidpriducto,codigoidt,this.cmetaf,this.codigoPoliticaPublica,this.vigenciaGasto];

							var buscaraux=[unionaux];
							this.selectcuetasa.push(varauxia);
							this.selectcuetasb.push(varauxib);
							
							this.selectbuscar.push(unionaux);
							this.csubproducto=this.subproducto=this.cseccion=this.cdivision=this.cgrupo=this.cclase=this.csubclase= this.seccion=this.division=this.grupo=this.clase=this.subclase=this.valorrubro=this.nrubro=this.codrubro=''; this.clasificador=0;
						}
						else
						{
							this.toggleMensaje();
							this.colortitulosmensaje='crimson';
							this.titulomensaje='Mensaje de Error';
							this.contenidomensaje='Rubro duplicado';
						}
					}
					else
					{
						this.toggleMensaje();
						this.colortitulosmensaje='crimson';
						this.titulomensaje='Mensaje de Error';
						this.contenidomensaje='Rubro no ingresado';
					}
				}
				else
				{
					this.toggleMensaje();
					this.colortitulosmensaje='crimson';
					this.titulomensaje='Mensaje de Error';
					this.contenidomensaje='Valor no ingresado';
				}
			}
														
			}
			else
			{
				this.toggleMensaje();
				this.colortitulosmensaje='crimson';
				this.titulomensaje='Mensaje de Error';
				this.contenidomensaje='Valor reduccion es mayor al valor posible a reducir';
			}
		},
		agregarcuentaIngresos: function()
		{
			var bandera01=0;
			
			if(this.fuenteIngreso!='')
			{
				if(this.seccionIngresos!='')
				{
					if(this.divisionIngresos!='')
					{
						if(this.grupoIngresos!='')
						{
							if(this.claseIngresos!='')
							{
								if(this.csubclaseIngresos!='')
								{
									if(this.valorrubroIngresos > 0 )
									{
										if(!this.valorrubroIngresos.includes('.'))
										{
											if(bandera01==0)
											{
												this.tabgroupIngresos=3;
												this.tabgroupIngresos=3;

												this.valorproyecto = Number(this.valorproyecto) + Number(this.valorrubroIngresos);

												var varauxia=[this.clasificadorIngresos,this.rubroIngreso,this.subclaseIngresos,this.valorrubroIngresos,this.fuenteIngreso,this.subproductoIngresos,this.nPoliticaPublicaIngresos,this.vigenciaGastoIngresos,this.medioPagoIngresos];

												var varauxib=[this.codigoRubroIngreso,this.clasificadorIngresos,this.cseccionIngresos,this.cdivisionIngresos,this.cgrupoIngresos,this.cclaseIngresos,this.csubclaseIngresos, this.valorrubroIngresos,this.codigoFuenteIngreso,this.csubproductoIngresos,this.codigoPoliticaPublicaIngresos,this.vigenciaGastoIngresos,this.medioPagoIngresos];
		
												this.selectcuetasaIngresos.push(varauxia);
												this.selectcuetasbIngresos.push(varauxib);
												
												this.csubproductoIngresos=this.subproductoIngresos=this.cseccionIngresos=this.cdivisionIngresos=this.cgrupoIngresos=this.cclaseIngresos=this.csubclaseIngresos= this.seccionIngresos=this.divisionIngresos=this.grupoIngresos=this.claseIngresos=this.subclaseIngresos=this.valorrubroIngresos=this.rubroIngreso=this.codigoRubroIngreso=''; this.clasificadorIngresos=0;
											}
											else
											{
												this.toggleMensaje();
												this.colortitulosmensaje='crimson';
												this.titulomensaje='Mensaje de Error';
												this.contenidomensaje='Rubro duplicado';
											}
										}
										else
										{
											this.parpadeovalorrubro='parpadea';
											this.toggleMensaje();
											this.colortitulosmensaje='crimson';
											this.titulomensaje='Mensaje de Error';
											this.contenidomensaje='No puede ingresar el valor en decimales!';
										}
									}
									else
									{
										this.parpadeovalorrubro='parpadea';
										this.opcionmensaje='4';
										this.toggleMensaje();
										this.colortitulosmensaje='crimson';
										this.titulomensaje='Mensaje de Error';
										this.contenidomensaje='Falta ingresar el valor para poder agregar';
									}
										
								}
								else
								{
									this.subclasedobleclick='parpadea colordobleclik';
									this.toggleMensaje();
									this.colortitulosmensaje='crimson';
									this.titulomensaje='Mensaje de Error';
									this.contenidomensaje='Falta ingresar la Subclase para poder agregar';
								}
							}
							else
							{
								this.clasedobleclick='parpadea colordobleclik';
								this.toggleMensaje();
								this.colortitulosmensaje='crimson';
								this.titulomensaje='Mensaje de Error';
								this.contenidomensaje='Falta ingresar una Clase para poder agregar';
							}
						}
						else
						{
							this.grupodobleclick='parpadea colordobleclik';
							this.toggleMensaje();
							this.colortitulosmensaje='crimson';
							this.titulomensaje='Mensaje de Error';
							this.contenidomensaje='Falta ingresar un Grupo para poder agregar';
						}
					}
					else
					{
						this.divisiondobleclick='parpadea colordobleclik';
						this.toggleMensaje();
						this.colortitulosmensaje='crimson';
						this.titulomensaje='Mensaje de Error';
						this.contenidomensaje='Falta ingresar Divisi\xf3n para poder agregar';
					}
				}
				else
				{
					this.secciondobleclick='parpadea colordobleclik';
					this.toggleMensaje();
					this.colortitulosmensaje='crimson';
					this.titulomensaje='Mensaje de Error';
					this.contenidomensaje='Falta ingresar Secci\xf3n para poder agregar';
				}
				
			}
			else
			{
				this.fuentedobleclick='parpadea colordobleclik';
				this.toggleMensaje();
				this.colortitulosmensaje='crimson';
				this.titulomensaje='Mensaje de Error';
				this.contenidomensaje='Falta ingresar la Fuente para poder agregar';
			}
		},
		agregarcuentaFuncionamiento: async function()
		{
			var bandera01=0;
			
			if(this.valorrubroFuncionamiento < this.maximaReduccionFuncionamiento)
			{
				if(this.fuenteFuncionamiento!='')
				{
					if(this.seccionFuncionamiento!='')
					{
						if(this.divisionFuncionamiento!='')
						{
							if(this.grupoFuncionamiento!='')
							{
								if(this.claseFuncionamiento!='')
								{
									if(this.csubclaseFuncionamiento!='')
									{
										if(this.valorrubroFuncionamiento > 0 )
										{
											if(!this.valorrubroFuncionamiento.includes('.'))
											{
												if(bandera01==0)
												{
													this.tabgroupFuncionamiento = 3;

													this.valorproyecto = Number(this.valorproyecto) + Number(this.valorrubroFuncionamiento);

													var varauxia=[this.clasificadorFuncionamiento,this.rubroFuncionamiento,this.subclaseFuncionamiento,this.valorrubroFuncionamiento,this.fuenteFuncionamiento,this.subproductoFuncionamiento,this.nPoliticaPublicaFuncionamiento,this.vigenciaGastoFuncionamiento,this.medioPagoFuncionamiento];

													var varauxib=[this.codigoRubroFuncionamiento,this.clasificadorFuncionamiento,this.cseccionFuncionamiento,this.cdivisionFuncionamiento,this.cgrupoFuncionamiento,this.cclaseFuncionamiento,this.csubclaseFuncionamiento, this.valorrubroFuncionamiento,this.codigoFuenteFuncionamiento,this.csubproductoFuncionamiento,this.codigoPoliticaPublicaFuncionamiento,this.vigenciaGastoFuncionamiento,this.medioPagoFuncionamiento];
			
													this.selectcuetasaFuncionamiento.push(varauxia);
													this.selectcuetasbFuncionamiento.push(varauxib);
													
													this.csubproductoFuncionamiento=this.subproductoFuncionamiento=this.cseccionFuncionamiento=this.cdivisionFuncionamiento=this.cgrupoFuncionamiento=this.cclaseFuncionamiento=this.csubclaseFuncionamiento=this.seccionFuncionamiento=this.divisionFuncionamiento=this.grupoFuncionamiento=this.claseFuncionamiento=this.subclaseFuncionamiento=this.valorrubroFuncionamiento=this.rubroFuncionamiento=this.codigoRubroFuncionamiento=this.fuenteFuncionamiento=this.nPoliticaPublicaFuncionamiento=''; this.clasificadorFuncionamiento=0;
												}
												else
												{
													this.toggleMensaje();
													this.colortitulosmensaje='crimson';
													this.titulomensaje='Mensaje de Error';
													this.contenidomensaje='Rubro duplicado';
												}
											}
											else
											{
												this.parpadeovalorrubro='parpadea';
												this.toggleMensaje();
												this.colortitulosmensaje='crimson';
												this.titulomensaje='Mensaje de Error';
												this.contenidomensaje='No puede ingresar el valor en decimales!';
											}
										}
										else
										{
											this.parpadeovalorrubro='parpadea';
											this.opcionmensaje='4';
											this.toggleMensaje();
											this.colortitulosmensaje='crimson';
											this.titulomensaje='Mensaje de Error';
											this.contenidomensaje='Falta ingresar el valor para poder agregar';
										}
											
									}
									else
									{
										this.subclasedobleclick='parpadea colordobleclik';
										this.toggleMensaje();
										this.colortitulosmensaje='crimson';
										this.titulomensaje='Mensaje de Error';
										this.contenidomensaje='Falta ingresar la Subclase para poder agregar';
									}
								}
								else
								{
									this.clasedobleclick='parpadea colordobleclik';
									this.toggleMensaje();
									this.colortitulosmensaje='crimson';
									this.titulomensaje='Mensaje de Error';
									this.contenidomensaje='Falta ingresar una Clase para poder agregar';
								}
							}
							else
							{
								this.grupodobleclick='parpadea colordobleclik';
								this.toggleMensaje();
								this.colortitulosmensaje='crimson';
								this.titulomensaje='Mensaje de Error';
								this.contenidomensaje='Falta ingresar un Grupo para poder agregar';
							}
						}
						else
						{
							this.divisiondobleclick='parpadea colordobleclik';
							this.toggleMensaje();
							this.colortitulosmensaje='crimson';
							this.titulomensaje='Mensaje de Error';
							this.contenidomensaje='Falta ingresar Divisi\xf3n para poder agregar';
						}
					}
					else
					{
						this.secciondobleclick='parpadea colordobleclik';
						this.toggleMensaje();
						this.colortitulosmensaje='crimson';
						this.titulomensaje='Mensaje de Error';
						this.contenidomensaje='Falta ingresar Secci\xf3n para poder agregar';
					}
					
				}
				else
				{
					this.fuentedobleclick='parpadea colordobleclik';
					this.toggleMensaje();
					this.colortitulosmensaje='crimson';
					this.titulomensaje='Mensaje de Error';
					this.contenidomensaje='Falta ingresar la Fuente para poder agregar';
				}
			}
			else
			{
				this.toggleMensaje();
				this.colortitulosmensaje='crimson';
				this.titulomensaje='Mensaje de Error';
				this.contenidomensaje='Valor a ingresar es mayor al permitido a reducir';
			}
		},
		eliminacuentas: function(index,valor,idenproducto)
		{
			this.valoridentproducto[idenproducto]=Number(this.valoridentproducto[idenproducto]) - Number(valor);
			this.valorproyecto = Number(this.valorproyecto) - Number(valor);
			this.valorproyecto = Number(this.valorproyecto)
			this.selectcuetasa.splice(index, 1);
			this.selectcuetasb.splice(index, 1);
		},
		eliminacuentasIngresos: function(index,valor,idenproducto)
		{
			this.valorproyecto = Number(this.valorproyecto) - Number(valor);
			this.valorproyecto = Number(this.valorproyecto)

			this.selectcuetasaIngresos.splice(index, 1);
			this.selectcuetasbIngresos.splice(index, 1);
		},
		eliminacuentasFuncionamiento: function(index,valor,idenproducto)
		{
			this.valorproyecto = Number(this.valorproyecto) - Number(valor);
			this.valorproyecto = Number(this.valorproyecto)

			this.selectcuetasaFuncionamiento.splice(index, 1);
			this.selectcuetasbFuncionamiento.splice(index, 1);
		},
		agregarcuenta1:function()
		{
			if(this.valorcuin <= this.maximaReduccion)
			{
				var bandera01=0;
				if(this.selectcuetasc.length>0)
				{
					for (const auxarray in this.selectbuscar1)
					{
						var auxlong=`${this.selectbuscar1[auxarray]}`;
						var auxbusq=this.cfuentef + '<->' + this.codigocuin + '<->' + this.identproducto.slice(0,9);
						
					}
				}
				
				if(!this.valorcuin.includes('.'))
				{
					if(this.valorcuin != '')
					{
						if(this.codrubro != '')
						{
							if(bandera01==0)
							{
								this.tabgroup2=2;
								var tempidpriducto = this.identproducto;
								var codigoidt=this.codigoidentproducto[tempidpriducto];
								var nombreidt=this.nombreidentproducto[tempidpriducto];
								var unionaux = this.cfuentef + '<->' + this.codigocuin + '<->' + tempidpriducto;
								this.valoridentproducto[tempidpriducto]=Number(this.valoridentproducto[tempidpriducto]) + Number(this.valorcuin);
								this.valorproyecto = Number(this.valorproyecto) + Number(this.valorcuin);
								var varauxia=[this.identidad,this.nitentidad,this.nomentidad,this.codigocuin,this.valorcuin,this.fuentef,this.mediopago, this.codrubro,tempidpriducto,nombreidt,this.metaf,this.nPoliticaPublica,this.vigenciaGasto];

								var varauxib=[this.codrubro,this.identidad,this.nitentidad,this.codigocuin,this.valorcuin,this.cfuentef,this.mediopago, tempidpriducto,codigoidt,this.cmetaf,this.codigoPoliticaPublica,this.vigenciaGasto];
								this.selectcuetasc.push(varauxia);
								this.selectcuetasd.push(varauxib);
								
								this.identidad=this.nitentidad=this.codigocuin=this.nomentidad=this.valorcuin=this.nrubro=this.codrubro='';
								this.clasificador=0;
							}
							else
							{
								this.toggleMensaje();
								this.colortitulosmensaje='crimson';
								this.titulomensaje='Mensaje de Error';
								this.contenidomensaje='Rubro duplicado';
							}
						}
						else
						{
							this.toggleMensaje();
							this.colortitulosmensaje='crimson';
							this.titulomensaje='Mensaje de Error';
							this.contenidomensaje='Rubro no ingresado';
						}
					}
					else
					{
						this.toggleMensaje();
						this.colortitulosmensaje='crimson';
						this.titulomensaje='Mensaje de Error';
						this.contenidomensaje='Valor no ingresado';
					}
				}
				else
				{
					this.toggleMensaje();
					this.colortitulosmensaje='crimson';
					this.titulomensaje='Mensaje de Error';
					this.contenidomensaje='No puede ingresar el valor en decimales!';
				}
									
			}
			else
			{
				this.toggleMensaje();
				this.colortitulosmensaje='crimson';
				this.titulomensaje='Mensaje de Error';
				this.contenidomensaje='Valor reduccion es mayor al valor posible a reducir';
			}
		},
		agregarcuentaIngresos1:function()
		{
			var bandera01=0;

			if(this.codigoFuenteIngreso!='')
			{
				if(this.identidadIngresos!='')
				{
					if(this.valorcuinIngresos>0)
					{
						
						if(!this.valorcuinIngresos.includes('.'))
						{
							if(bandera01==0)
							{
								this.tabgroupIngresos=2;
								this.tabgroupIngresos=2;
								this.valorproyecto = Number(this.valorproyecto) + Number(this.valorcuin);
								
								var varauxia=[this.identidadIngresos,this.nitentidadIngresos,this.nomentidadIngrsos,this.codigocuinIngresos,this.valorcuinIngresos,this.fuenteIngreso,this.rubroIngreso,this.nPoliticaPublicaIngresos,this.vigenciaGastoIngresos,this.medioPagoIngresos];

								var varauxib=[this.codigoRubroIngreso,this.identidadIngresos,this.nitentidadIngresos,this.codigocuinIngresos,this.valorcuinIngresos,this.codigoFuenteIngreso,this.codigoPoliticaPublicaIngresos,this.vigenciaGastoIngresos,this.medioPagoIngresos];

								this.selectcuetascIngresos.push(varauxia);
								this.selectcuetasdIngresos.push(varauxib);
								
								this.identidadIngresos=this.nitentidadIngresos=this.codigocuinIngresos=this.nomentidadIngrsos=this.valorcuinIngresos=this.rubroIngreso=this.codigoRubroIngreso=this.nPoliticaPublicaIngresos=this.codigoPoliticaPublicaIngresos=this.vigenciaGastoIngresos='';
								this.clasificadorIngresos=0;
							}
							else
							{
								this.toggleMensaje();
								this.colortitulosmensaje='crimson';
								this.titulomensaje='Mensaje de Error';
								this.contenidomensaje='Rubro duplicado';
							}
						}
						else
						{
							this.toggleMensaje();
							this.colortitulosmensaje='crimson';
							this.titulomensaje='Mensaje de Error';
							this.contenidomensaje='No puede ingresar el valor en decimales!';
						}
					}
					else
					{
						this.opcionmensaje='4';
						this.toggleMensaje();
						this.colortitulosmensaje='crimson';
						this.titulomensaje='Mensaje de Error';
						this.contenidomensaje='Falta ingresar el valor para poder agregar';
					}
				}
				else
				{
					this.toggleMensaje();
					this.colortitulosmensaje='crimson';
					this.titulomensaje='Mensaje de Error';
					this.contenidomensaje='Falta ingresar la Entidad para poder agregar';
				}
			}
			else
			{
				this.toggleMensaje();
				this.colortitulosmensaje='crimson';
				this.titulomensaje='Mensaje de Error';
				this.contenidomensaje='Falta ingresar Fuente para poder agregar';
			}
		},
		agregarcuentaFuncionamiento1: async function()
		{
			var bandera01=0;

			if(this.valorcuinFuncionamiento < this.maximaReduccionFuncionamiento)
			{
				if(this.codigoFuenteFuncionamiento!='')
				{
					if(this.identidadFuncionamiento!='')
					{
						if(this.valorcuinFuncionamiento>0)
						{
							
							if(!this.valorcuinFuncionamiento.includes('.'))
							{
								if(bandera01==0)
								{
									this.tabgroupFuncionamiento=2;
									
									this.valorproyecto = Number(this.valorproyecto) + Number(this.valorcuinFuncionamiento);
									
									var varauxia=[this.identidadFuncionamiento,this.nitentidadFuncionamiento,this.nomentidadFuncionamiento,this.codigocuinFuncionamiento,this.valorcuinFuncionamiento,this.fuenteFuncionamiento,this.rubroFuncionamiento,this.nPoliticaPublicaFuncionamiento,this.vigenciaGastoFuncionamiento,this.medioPagoFuncionamiento];

									
									var varauxib=[this.codigoRubroFuncionamiento,this.identidadFuncionamiento,this.nitentidadFuncionamiento,this.codigocuinFuncionamiento,this.valorcuinFuncionamiento,this.codigoFuenteFuncionamiento,this.codigoPoliticaPublicaFuncionamiento,this.vigenciaGastoFuncionamiento,this.medioPagoFuncionamiento];

									this.selectcuetascFuncionamiento.push(varauxia);
									
									this.selectcuetasdFuncionamiento.push(varauxib);
									
									this.identidadFuncionamiento=this.nitentidadFuncionamiento=this.codigocuinFuncionamiento=this.nomentidadFuncionamiento=this.valorcuinFuncionamiento=this.rubroFuncionamiento=this.codigoRubroFuncionamiento=this.nPoliticaPublicaFuncionamiento=this.codigoPoliticaPublicaFuncionamiento=this.vigenciaGastoFuncionamiento='';
									this.clasificadorFuncionamiento=0;
								}
								else
								{
									this.toggleMensaje();
									this.colortitulosmensaje='crimson';
									this.titulomensaje='Mensaje de Error';
									this.contenidomensaje='Rubro duplicado';
								}
							}
							else
							{
								this.toggleMensaje();
								this.colortitulosmensaje='crimson';
								this.titulomensaje='Mensaje de Error';
								this.contenidomensaje='No puede ingresar el valor en decimales!';
							}
						}
						else
						{
							this.opcionmensaje='4';
							this.toggleMensaje();
							this.colortitulosmensaje='crimson';
							this.titulomensaje='Mensaje de Error';
							this.contenidomensaje='Falta ingresar el valor para poder agregar';
						}
					}
					else
					{
						this.toggleMensaje();
						this.colortitulosmensaje='crimson';
						this.titulomensaje='Mensaje de Error';
						this.contenidomensaje='Falta ingresar la Entidad para poder agregar';
					}
				}
				else
				{
					this.toggleMensaje();
					this.colortitulosmensaje='crimson';
					this.titulomensaje='Mensaje de Error';
					this.contenidomensaje='Falta ingresar Fuente para poder agregar';
				}
			}
			else
			{
				this.toggleMensaje();
				this.colortitulosmensaje='crimson';
				this.titulomensaje='Mensaje de Error';
				this.contenidomensaje='Valor a ingresar es mayor al permitido a reducir';
			}
		},
		eliminacuentaIngresos1:function(index,valor)
		{
			this.valorproyecto = Number(this.valorproyecto) - Number(valor);
			this.selectcuetascIngresos.splice(index, 1);
			this.selectcuetasdIngresos.splice(index, 1);
		},
		eliminacuentaFuncionamiento1:function(index,valor)
		{
			this.valorproyecto = Number(this.valorproyecto) - Number(valor);
			this.selectcuetascFuncionamiento.splice(index, 1);
			//falta arreglar
			this.selectcuetasdIngresos.splice(index, 1);
		},
		eliminacuenta1:function(index,valor,idenproducto)
		{
			this.valoridentproducto[idenproducto]=Number(this.valoridentproducto[idenproducto]) - Number(valor);
			this.valorproyecto = Number(this.valorproyecto) - Number(valor);
			this.selectcuetasc.splice(index, 1);
			this.selectcuetasd.splice(index, 1);
		},
		agregarcuenta2:function()
		{
			if(this.valorsinclasifi <= this.maximaReduccion)
			{
				var bandera01=0;
				if(this.selectcuetasf.length>0)
				{
					for (const auxarray in this.selectbuscar2)
					{
						var auxlong=`${this.selectbuscar2[auxarray]}`;
						var auxbusq=this.cfuentef + '<->' + this.codrubro + '<->' + this.identproducto;
						
					}
				}
				
				if(bandera01==0)
				{
					if(this.nrubro != '')
					{
						if(!this.valorsinclasifi.includes('.'))
						{
							if(this.valorsinclasifi != '')
							{
								this.tabgroup2=1;
								var tempidpriducto=this.identproducto;
								var codigoidt=this.codigoidentproducto[tempidpriducto];
								var nombreidt=this.nombreidentproducto[tempidpriducto];
								var unionaux = this.cfuentef + '<->' + this.codrubro + '<->' + tempidpriducto;
								this.valoridentproducto[tempidpriducto]=Number(this.valoridentproducto[tempidpriducto]) + Number(this.valorsinclasifi);
								this.valorproyecto = Number(this.valorproyecto) + Number(this.valorsinclasifi);
								var varauxia=[this.codrubro,this.nrubro,this.valorsinclasifi,this.cfuentef,this.mediopago,tempidpriducto,nombreidt,this.metaf,this.nPoliticaPublica,this.vigenciaGasto];

								var varauxib=[this.codrubro,this.valorsinclasifi,this.cfuentef,this.mediopago,tempidpriducto,codigoidt,this.cmetaf,this.codigoPoliticaPublica,this.vigenciaGasto];
								
								this.selectcuetase.push(varauxia);
								this.selectcuetasf.push(varauxib);
								
								this.selectbuscar2.push(unionaux);
								this.valorsinclasifi=this.nrubro=this.codrubro='';
								this.clasificador=0;
							}
							else
							{
								this.toggleMensaje();
								this.colortitulosmensaje='crimson';
								this.titulomensaje='Mensaje de Error';
								this.contenidomensaje='Valor no ingresado';
							}
						}
						else
						{
							this.toggleMensaje();
							this.colortitulosmensaje='crimson';
							this.titulomensaje='Mensaje de Error';
							this.contenidomensaje='No puede ingresar el valor en decimales!';
						}
					}
					else
					{
						this.toggleMensaje();
						this.colortitulosmensaje='crimson';
						this.titulomensaje='Mensaje de Error';
						this.contenidomensaje='Rubro no ingresado';
					}
				}
				else
				{
					this.toggleMensaje();
					this.colortitulosmensaje='crimson';
					this.titulomensaje='Mensaje de Error';
					this.contenidomensaje='Rubro duplicado';
				}
									
			}
			else
			{
				this.toggleMensaje();
				this.colortitulosmensaje='crimson';
				this.titulomensaje='Mensaje de Error';
				this.contenidomensaje='Valor reduccion es mayor al valor posible a reducir';
			}
				
		},
		agregarcuentaIngresos2:function()
		{
			var bandera01=0;

			if(this.fuenteIngreso!='')
			{
				
				if(this.valorsinclasifiIngresos!='')
				{
					
					if(bandera01==0)
					{
						if(!this.valorsinclasifi.includes('.'))
						{
							this.tabgroupIngresos=1;
							this.tabgroupIngresos=1;
							
							this.valorproyecto = Number(this.valorproyecto) + Number(this.valorsinclasifiIngresos);

							var varauxia=[this.codigoRubroIngreso,this.rubroIngreso,this.valorsinclasifiIngresos,this.fuenteIngreso,this.nPoliticaPublicaIngresos,this.vigenciaGastoIngresos,this.medioPagoIngresos];

							var varauxib=[this.codigoRubroIngreso,this.valorsinclasifiIngresos,this.codigoFuenteIngreso,this.codigoPoliticaPublicaIngresos,this.vigenciaGastoIngresos,this.medioPagoIngresos];

							this.selectcuetaseIngresos.push(varauxia);
							this.selectcuetasfIngresos.push(varauxib);
							
							this.valorsinclasifiIngresos=this.rubroIngreso=this.codigoRubroIngreso=this.fuenteIngreso=this.codigoFuenteIngreso=this.nPoliticaPublicaIngresos=this.codigoPoliticaPublicaIngresos=this.vigenciaGastoIngresos='';
							this.clasificadorIngresos=0;
						}
						else
						{
							this.toggleMensaje();
							this.colortitulosmensaje='crimson';
							this.titulomensaje='Mensaje de Error';
							this.contenidomensaje='No puede ingresar el valor en decimales!';
						}
					}
					else
					{
						this.toggleMensaje();
						this.colortitulosmensaje='crimson';
						this.titulomensaje='Mensaje de Error';
						this.contenidomensaje='Rubro duplicado';
					}
						
				}
				else
				{
					this.toggleMensaje();
					this.colortitulosmensaje='crimson';
					this.titulomensaje='Mensaje de Error';
					this.contenidomensaje='Falta ingresar el valor para poder agregar';
				}
			}
			else
			{
				this.toggleMensaje();
				this.colortitulosmensaje='crimson';
				this.titulomensaje='Mensaje de Error';
				this.contenidomensaje='Falta ingresar la Fuente para poder agregar';
			}
			
		},
		validarSaldo: function()
		{
			if(this.fuenteFuncionamiento!='')
			{				
				if(this.medioPagoFuncionamiento != '')
				{
					axios.post('vue/presupuesto_ccp/ccp-reduccion.php?buscar=clasificadorbys&cuenta=' + this.codigoRubroFuncionamiento + '&vigencia=' + this.vigencia + '&fuente=' + this.codigoFuenteFuncionamiento + '&medioPago=' + this.medioPagoFuncionamiento + '&subclase=' + this.csubclaseIngresos + '&producto=' + this.csubproductoFuncionamiento)
					.then(function(response)
					{
						app.maximaReduccionFuncionamiento = response.data.saldo;
						console.log(response.data);
					});
				}
				else
				{
					this.toggleMensaje();
					this.colortitulosmensaje='crimson';
					this.titulomensaje='Mensaje de Error';
					this.contenidomensaje='Falta ingresar el medio de pago';	
				}
			}
			else
			{
				this.toggleMensaje();
				this.colortitulosmensaje='crimson';
				this.titulomensaje='Mensaje de Error';
				this.contenidomensaje='Falta ingresar la Fuente para poder agregar';
			}
		},

		agregarcuentaFuncionamiento2: function()
		{
			var bandera01=0;

			if(this.maximaReduccionFuncionamiento > this.valorsinclasifiFuncionamiento)
			{
				if(this.fuenteFuncionamiento!='')
				{
					
					if(this.valorsinclasifiFuncionamiento!='')
					{
						
						if(bandera01==0)
						{
							if(!this.valorsinclasifiFuncionamiento.includes('.'))
							{
								this.tabgroupFuncionamiento=1;
								
								this.valorproyecto = Number(this.valorproyecto) + Number(this.valorsinclasifiFuncionamiento);

								var varauxia=[this.codigoRubroFuncionamiento,this.rubroFuncionamiento,this.valorsinclasifiFuncionamiento,this.fuenteFuncionamiento,this.nPoliticaPublicaFuncionamiento,this.vigenciaGastoFuncionamiento,this.medioPagoFuncionamiento];

								var varauxib=[this.codigoRubroFuncionamiento,this.valorsinclasifiFuncionamiento,this.codigoFuenteFuncionamiento,this.codigoPoliticaPublicaFuncionamiento,this.vigenciaGastoFuncionamiento,this.medioPagoFuncionamiento];

								this.selectcuetaseFuncionamiento.push(varauxia);
								this.selectcuetasfFuncionamiento.push(varauxib);
								
								this.valorsinclasifiFuncionamiento=this.rubroFuncionamiento=this.codigoRubroFuncionamiento=this.fuenteFuncionamiento=this.codigoFuenteFuncionamiento=this.nPoliticaPublicaFuncionamiento=this.codigoPoliticaPublicaFuncionamiento=this.vigenciaGastoFuncionamiento=this.fuenteFuncionamiento='';
								this.clasificadorFuncionamiento=0;
							}
							else
							{
								this.toggleMensaje();
								this.colortitulosmensaje='crimson';
								this.titulomensaje='Mensaje de Error';
								this.contenidomensaje='No puede ingresar el valor en decimales!';
							}
						}
						else
						{
							this.toggleMensaje();
							this.colortitulosmensaje='crimson';
							this.titulomensaje='Mensaje de Error';
							this.contenidomensaje='Rubro duplicado';
						}
							
					}
					else
					{
						this.toggleMensaje();
						this.colortitulosmensaje='crimson';
						this.titulomensaje='Mensaje de Error';
						this.contenidomensaje='Falta ingresar el valor para poder agregar';
					}
				}
				else
				{
					this.toggleMensaje();
					this.colortitulosmensaje='crimson';
					this.titulomensaje='Mensaje de Error';
					this.contenidomensaje='Falta ingresar la Fuente para poder agregar';
				}
			}
			else
			{
				this.toggleMensaje();
				this.colortitulosmensaje='crimson';
				this.titulomensaje='Mensaje de Error';
				this.contenidomensaje='Valor a ingresar es mayor al permitido a reducir';
			}
		},
		eliminacuenta2:function(index,valor,idenproducto)
		{
			this.valoridentproducto[idenproducto]=Number(this.valoridentproducto[idenproducto]) - Number(valor);
			this.valorproyecto = Number(this.valorproyecto) - Number(valor);
			this.selectcuetase.splice(index, 1);
			this.selectcuetasf.splice(index, 1);
		},
		eliminacuentaIngresos2:function(index,valor)
		{
			this.valorproyecto = Number(this.valorproyecto) - Number(valor);
			this.selectcuetaseIngresos.splice(index, 1);
			this.selectcuetasfIngresos.splice(index, 1);
		},
		eliminacuentaFuncionamiento2:function(index,valor)
		{
			this.valorproyecto = Number(this.valorproyecto) - Number(valor);
			this.selectcuetaseFuncionamiento.splice(index, 1);
			this.selectcuetasfFuncionamiento.splice(index, 1);
		},
		MostrarPresupuesto: function()
		{
			if(this.actoAdministrativo != '')
			{			
				var gastos = [];
				var codigosGastos = [];
				var codigosIngresos = [];
				var ingresos = [];

				//Gastos

				for (let i = 0; i < this.selectcuetase.length; i++) 
				{
					gastos = [this.codigo,this.selectcuetase[i][1],this.selectcuetase[i][3],this.selectcuetase[i][9],this.selectcuetase[i][8],this.selectcuetase[i][7],this.selectcuetase[i][4],'',this.selectcuetase[i][2]];
					this.presupuestoTotalGastos.push(gastos);
				}			
				
				for (let i = 0; i < this.selectcuetasc.length; i++) 
				{
					gastos=[this.codigo,this.selectcuetasc[i][7],this.selectcuetasc[i][5],this.selectcuetasc[i][12],this.selectcuetasc[i][11],this.selectcuetasc[i][10],this.selectcuetasc[i][6],this.selectcuetasc[i][3],this.selectcuetasc[i][4]];	
					this.presupuestoTotalGastos.push(gastos);
				}

				for (let i = 0; i < this.selectcuetasa.length; i++) 
				{
					gastos=[this.codigo,this.selectcuetasa[i][1],this.selectcuetasa[i][4],this.selectcuetasa[i][11],this.selectcuetasa[i][10],this.selectcuetasa[i][9],this.selectcuetasa[i][5],this.selectcuetasa[i][2],this.selectcuetasa[i][3]];	
					this.presupuestoTotalGastos.push(gastos);
				}

				
				if(this.selectcuetase != '' || this.selectcuetasc != '' || this.selectcuetasa != '')
				{
					//Sin clasificador
					for (let i = 0; i < this.selectcuetasf.length; i++) 
					{
						codigosGastos = [this.actoAdministrativo,this.selectcuetasf[i][0],this.selectcuetasf[i][2],this.selectcuetasf[i][8],this.selectcuetasf[i][7],this.selectcuetasf[i][6],this.selectcuetasf[i][4],this.selectcuetasf[i][3],0,'',this.cProyecto,this.codigo,this.vigencia,this.selectcuetasf[i][1]];

						this.guardarCodigosGastos.push(codigosGastos);
					}

					for (let i = 0; i < this.selectcuetasd.length; i++) 
					{
						codigosGastos = [this.actoAdministrativo,this.selectcuetasd[i][0],this.selectcuetasd[i][5],this.selectcuetasd[i][11],this.selectcuetasd[i][10],this.selectcuetasd[i][9],this.selectcuetasd[i][7],this.selectcuetasd[i][6],'1',this.selectcuetasd[i][3],this.cProyecto,this.codigo,this.vigencia,this.selectcuetasd[i][4]];	

						this.guardarCodigosGastos.push(codigosGastos);
					}

					for (let i = 0; i < this.selectcuetasb.length; i++) 
					{
						var clasificacion;
						var clasificador;
						if(this.selectcuetasb[i][10] != '')
						{
							clasificacion = '2';
							clasificador = this.selectcuetasb[i][10];
						}
						else
						{
							clasificacion = '3';
							clasificador = this.selectcuetasb[i][6];
						}

						codigosGastos = [this.actoAdministrativo,this.selectcuetasb[i][0],this.selectcuetasb[i][8],this.selectcuetasb[i][15],this.selectcuetasb[i][14],this.selectcuetasb[i][13],this.selectcuetasb[i][11],this.selectcuetasb[i][9],clasificacion,clasificador,this.cProyecto,this.codigo,this.vigencia,this.selectcuetasb[i][7]];

						this.guardarCodigosGastos.push(codigosGastos);

					}
					
				}

				//Ingresos
				for (let i = 0; i < this.selectcuetaseIngresos.length; i++) 
				{
					ingresos = [this.selectcuetaseIngresos[i][1],this.selectcuetaseIngresos[i][3],this.selectcuetaseIngresos[i][5],this.selectcuetaseIngresos[i][4],'',this.selectcuetaseIngresos[i][2]];	
					this.presupuestoTotalIngresos.push(ingresos);
				}
		
				for (let i = 0; i < this.selectcuetascIngresos.length; i++) 
				{
					ingresos=[this.selectcuetascIngresos[i][6],this.selectcuetascIngresos[i][5],this.selectcuetascIngresos[i][8],this.selectcuetascIngresos[i][7],this.selectcuetascIngresos[i][3],this.selectcuetascIngresos[i][4]];	
					this.presupuestoTotalIngresos.push(ingresos);
				}

				for (let i = 0; i < this.selectcuetasaIngresos.length; i++) 
				{
					ingresos=[this.selectcuetasaIngresos[i][1],this.selectcuetasaIngresos[i][4],this.selectcuetasaIngresos[i][7],this.selectcuetasaIngresos[i][6],this.selectcuetasaIngresos[i][2],this.selectcuetasaIngresos[i][3]];	
					this.presupuestoTotalIngresos.push(ingresos);
				}

				if(this.selectcuetaseIngresos != '' || this.selectcuetascIngresos != '' || this.selectcuetasaIngresos)
				{
					for (let i = 0; i < this.selectcuetasfIngresos.length; i++) 
					{
						codigosIngresos = [this.actoAdministrativo,this.selectcuetasfIngresos[i][0],this.selectcuetasfIngresos[i][2],this.selectcuetasfIngresos[i][4],this.selectcuetasfIngresos[i][3],'','',this.selectcuetasfIngresos[i][5],'0','','','',this.vigencia,this.selectcuetasfIngresos[i][1]];

						this.guardarCodigosIngresos.push(codigosIngresos);
					}

					for (let i = 0; i < this.selectcuetasdIngresos.length; i++) 
					{
						codigosIngresos = [this.actoAdministrativo,this.selectcuetasdIngresos[i][0],this.selectcuetasdIngresos[i][5],this.selectcuetasdIngresos[i][7],this.selectcuetasdIngresos[i][6],'','',this.selectcuetasdIngresos[i][8],'1',this.selectcuetasdIngresos[i][3],'','',this.vigencia,this.selectcuetasdIngresos[i][4]];

						this.guardarCodigosIngresos.push(codigosIngresos);
					}

					for (let i = 0; i < this.selectcuetasbIngresos.length; i++) 
					{
						var clasificacion;
						var clasificador;
		
						if(this.selectcuetasbIngresos[i][9] != '')
						{
							clasificacion = '2';
							clasificador = this.selectcuetasbIngresos[i][9];
						}
						else
						{
							clasificacion = '3';
							clasificador = this.selectcuetasbIngresos[i][6];
						}

						codigosIngresos = [this.actoAdministrativo,this.selectcuetasbIngresos[i][0],this.selectcuetasbIngresos[i][8],this.selectcuetasbIngresos[i][11],this.selectcuetasbIngresos[i][10],'','',this.selectcuetasbIngresos[i][12],clasificacion,clasificador,'','',this.vigencia,this.selectcuetasbIngresos[i][7]];	

						this.guardarCodigosIngresos.push(codigosIngresos);
					}
				}

				
				//Funcionamiento

				for (let i = 0; i < this.selectcuetaseFuncionamiento.length; i++) 
				{
					ingresos = ['',this.selectcuetaseFuncionamiento[i][1],this.selectcuetaseFuncionamiento[i][3],this.selectcuetaseFuncionamiento[i][5],this.selectcuetaseFuncionamiento[i][4],'',this.selectcuetaseFuncionamiento[i][6],'',this.selectcuetaseFuncionamiento[i][2]];	
					this.presupuestoTotalGastos.push(ingresos);
				}
		
				for (let i = 0; i < this.selectcuetascFuncionamiento.length; i++) 
				{
					ingresos=['',this.selectcuetascFuncionamiento[i][6],this.selectcuetascFuncionamiento[i][5],this.selectcuetascFuncionamiento[i][8],this.selectcuetascFuncionamiento[i][7],'',this.selectcuetascFuncionamiento[i][9],this.selectcuetascFuncionamiento[i][3],this.selectcuetascFuncionamiento[i][4]];	
					this.presupuestoTotalGastos.push(ingresos);
				}

				for (let i = 0; i < this.selectcuetasaFuncionamiento.length; i++) 
				{
					ingresos=['',this.selectcuetasaFuncionamiento[i][1],this.selectcuetasaFuncionamiento[i][4],this.selectcuetasaFuncionamiento[i][7],this.selectcuetasaFuncionamiento[i][6],'',this.selectcuetasaFuncionamiento[i][8],this.selectcuetasaFuncionamiento[i][2],this.selectcuetasaFuncionamiento[i][3]];	
					this.presupuestoTotalGastos.push(ingresos);
				}


				if(this.selectcuetaseFuncionamiento != '' || this.selectcuetascFuncionamiento != '' || this.selectcuetasaFuncionamiento)
				{
					for (let i = 0; i < this.selectcuetasfFuncionamiento.length; i++) 
					{
						codigosGastos = [this.actoAdministrativo,this.selectcuetasfFuncionamiento[i][0],this.selectcuetasfFuncionamiento[i][2],this.selectcuetasfFuncionamiento[i][4],this.selectcuetasfFuncionamiento[i][3],'','',this.selectcuetasfFuncionamiento[i][5],'0','','','',this.vigencia,this.selectcuetasfFuncionamiento[i][1]];

						this.guardarCodigosGastos.push(codigosGastos);
					}

					for (let i = 0; i < this.selectcuetasdFuncionamiento.length; i++) 
					{
						codigosGastos = [this.actoAdministrativo,this.selectcuetasdFuncionamiento[i][0],this.selectcuetasdFuncionamiento[i][5],this.selectcuetasdFuncionamiento[i][7],this.selectcuetasdFuncionamiento[i][6],'','',this.selectcuetasdFuncionamiento[i][8],'1',this.selectcuetasdFuncionamiento[i][3],'','',this.vigencia,this.selectcuetasdFuncionamiento[i][4]];

						this.guardarCodigosGastos.push(codigosGastos);
					}

					for (let i = 0; i < this.selectcuetasbFuncionamiento.length; i++) 
					{
						var clasificacion;
						var clasificador;
		
						if(this.selectcuetasbFuncionamiento[i][9] != '')
						{
							clasificacion = '2';
							clasificador = this.selectcuetasbFuncionamiento[i][9];
						}
						else
						{
							clasificacion = '3';
							clasificador = this.selectcuetasbFuncionamiento[i][6];
						}

						codigosGastos = [this.actoAdministrativo,this.selectcuetasbFuncionamiento[i][0],this.selectcuetasbFuncionamiento[i][8],this.selectcuetasbFuncionamiento[i][11],this.selectcuetasbFuncionamiento[i][10],'','',this.selectcuetasbFuncionamiento[i][12],clasificacion,clasificador,'','',this.vigencia,this.selectcuetasbFuncionamiento[i][7]];	

						this.guardarCodigosGastos.push(codigosGastos);
					}
				}

				for (let i = 0; i < this.guardarCodigosIngresos.length; i++) 
				{
					this.valorTotalIngresos += parseInt(this.guardarCodigosIngresos[i][13]);	
				}
				for (let i = 0; i < this.guardarCodigosGastos.length; i++) 
				{
					this.valorTotalGastos += parseInt(this.guardarCodigosGastos[i][13]);
				}
				//console.log(this.guardarCodigosGastos);

				this.sector=this.csector=this.programa=this.cprograma=this.subprograma=this.csubprograma=this.indicadorpro=this.cindicadorpro=this.producto=this.cproducto=this.identproducto=this.fuentef=this.cfuentef=this.metaf=this.cmetaf=this.mediopago=this.nrubro=this.codrubro=this.clasificador=this.nPoliticaPublica=this.vigenciaGasto=this.fuenteIngreso=this.rubroIngreso=this.clasificadorIngresos=this.nPoliticaPublicaIngresos=this.vigenciaGastoIngresos='';
				//this.proyecto=this.cProyecto=this.codigo=this.nombre=this.valorproyecto=this.descripcion

				this.selectProductosExistentes=[];
				this.selecproductosa=[];
				this.cclasificados=[];
				this.selectPresupuestoGastos=[];
				this.selectcuetase=[];
				this.selectcuetasc=[];
				this.selectcuetasa=[];
				this.clasificadosIngresos=[];
				this.selectcuetaseIngresos=[];
				this.selectcuetascIngresos=[];
				this.tabgroupIngresos=[];
				this.selectcuetasf = [];
				this.selectcuetasd = [];
				this.selectcuetasb = [];

				this.selectcuetascFuncionamiento = [];
				this.selectcuetasdFuncionamiento = [];
				this.selectcuetaseFuncionamiento = [];
				this.selectcuetasfFuncionamiento = [];
				this.selectcuetasaFuncionamiento = [];
				this.selectcuetasbFuncionamiento = [];

				this.tabgroupMostrar = 1;
			}
			else
			{
				this.toggleMensaje();
				this.colortitulosmensaje='crimson';
				this.titulomensaje='Mensaje de Error';
				this.contenidomensaje='Falta ingresar el acto administrativo';
			}
		},
		deshacer: function(id)
		{
			switch (id)
			{
				case'0':
					this.codrubro=this.nrubro=this.valorrubro=this.cseccion=this.seccion=this.cdivision=this.division=this.cgrupo=this.grupo='';
					this.clasificador=this.cclase=this.clase=this.csubclase=this.subclase=this.csubproducto=this.subproducto='';
					break;
				case '1':
					this.clasificador=this.valorrubro=this.cseccion=this.seccion=this.cdivision=this.division=this.cgrupo=this.grupo='';
					this.cclase=this.clase=this.csubclase=this.subclase=this.csubproducto=this.subproducto='';
					break;
				case '2':
					this.valorrubro=this.cseccion=this.seccion=this.cdivision=this.division=this.cgrupo=this.grupo='';
					this.cclase=this.clase=this.csubclase=this.subclase=this.csubproducto=this.subproducto='';
					break;
				case '3':
					this.cdivision=this.division=this.cgrupo=this.grupo=this.cclase=this.clase=this.csubclase=this.subclase=this.valorrubro='';
					this.csubproducto=this.subproducto='';
					break;
				case '4':
					this.cgrupo=this.grupo=this.cclase=this.clase=this.csubclase=this.subclase=this.valorrubro=this.csubproducto=this.subproducto='';
					break;
				case '5':
					this.cclase=this.clase=this.csubclase=this.subclase=this.valorrubro=this.csubproducto=this.subproducto='';
					break;
				case '6':
					this.csubclase=this.subclase=this.valorrubro=this.csubproducto=this.subproducto='';
					break;
				case '7':
					this.valorrubro=this.csubproducto=this.subproducto='';
					break; 
				case '8':
					this.cproducto=this.producto=this.cindicadorpro=this.indicadorpro=this.programa=this.cprograma=this.subprograma=this.csubprograma='';
					break;
				case '9':
					this.cproducto=this.producto=this.cindicadorpro=this.indicadorpro='';
					break;
				case '10':
					this.codrubro=this.nrubro=this.clasificador='';
					break;
				case '11':
					this.clasificador=this.valorrubro=this.cdivision=this.division=this.cgrupo=this.grupo='';
					this.cclase=this.clase=this.csubclase=this.subclase='';
					break;
				case '12':
					this.valorrubro=this.cdivision=this.division=this.cgrupo=this.grupo='';
					this.cclase=this.clase=this.csubclase=this.subclase='';
					break;
				case '13':
					this.nomidentproducto=this.identproducto=this.fuentef=this.cfuentef=this.metaf=this.cmetaf=this.mediopago=this.nrubro=this.codrubro=this.nombreClasificador=this.clasificador=this.nPoliticaPublica=this.codigoPoliticaPublica=this.nombreVigenciaGasto=this.vigenciaGasto=this.identidad=this.nitentidad=this.codigocuin=this.nomentidad=this.valorcuin=this.valorsinclasifi=this.seccion=this.cseccion=this.division=this.cdivision=this.grupo=this.cgrupo=this.clase=this.cclase=this.subclase=this.csubclase=this.subproducto=this.csubproducto=this.valorrubro='';
					break;
			}
		},
		preguntaguardar: function()
		{
			this.toggleMensajeSN();
			this.colortitulosmensaje='darkgreen';
			this.titulomensaje='Almacenado en el Sitema';
			this.contenidomensaje='Desea guardar el Proyecto';
		},
		guardar: function()
		{
			if(this.actosAdministrativos != '')
			{
                var formData = new FormData();
				
                for(var x = 0; x < this.guardarCodigosGastos.length; x++)
				{
                    formData.append("gastos[]", this.guardarCodigosGastos[x]);
                }

                for(var x = 0; x < this.guardarCodigosIngresos.length; x++)
				{
                    formData.append("ingresos[]", this.guardarCodigosIngresos[x]);
                }

                axios.post('vue/presupuesto_ccp/ccp-reduccion.php?tablas=guardar',formData)
				.then(function(response)
				{
					console.log(response.data);
					if(response.data.insertaBien){
						location.href="ccp-reduccion.php";
					}
                });

            }
            else
            {
                this.toggleMensaje();
				this.colortitulosmensaje='crimson';
				this.titulomensaje='Mensaje de Error';
				this.contenidomensaje='Falta ingresar el acto administrativo';
            }
		},
		validasubproducto: function(codigo)
		{
			if(codigo.length==7)
			{
				var formData = new FormData();
				var codbuscador='';
				var nivbuscador='';
				if (this.csubclase!='')
				{
					codbuscador=this.csubclase;
					nivbuscador='5';
				}
				else if (this.cclase!='')
				{
					codbuscador=this.cclase;
					nivbuscador='4';
				}
				else if (this.cgrupo!='')
				{
					codbuscador=this.cgrupo;
					nivbuscador='3';
				}
				else if (this.cdivision!='')
				{
					codbuscador=this.cdivision;
					nivbuscador='2';
				}
				else if (this.cseccion!='')
				{
					codbuscador=this.cseccion;
					nivbuscador='1';
				}
				formData.append("seccion",codbuscador);
				formData.append("nivel",nivbuscador);
				formData.append("codigo",codigo);
				axios.post('vue/presupuesto_ccp/ccp-bancoproyectos.php?buscar=valsubproducto',formData)
				.then(
					(response)=>
					{
						if(response.data.codigos[0][0]!='')
						{
							this.csubproducto=codigo;
							this.subproducto=codigo+" - "+response.data.codigos[0][0];
							if(this.cseccion=='')
							{
								this.cseccion=codigo.substr(0,1);
								axios.post('vue/presupuesto_ccp/ccp-bancoproyectos.php?buscar=nombregrupo&grupo='+this.cseccion)
								.then((response)=>{this.seccion=this.cseccion+" - "+response.data.codigos[0][0];});
							}
							if(this.cdivision=='')
							{
								this.cdivision=codigo.substr(0,2);
								axios.post('vue/presupuesto_ccp/ccp-bancoproyectos.php?buscar=nombregrupo&grupo='+this.cdivision)
								.then((response)=>{this.division=this.cdivision+" - "+response.data.codigos[0][0];});
							}
							if(this.cgrupo=='')
							{
								this.cgrupo=codigo.substr(0,3);
								axios.post('vue/presupuesto_ccp/ccp-bancoproyectos.php?buscar=nombregrupo&grupo='+this.cgrupo)
								.then((response)=>{this.grupo=this.cgrupo+" - "+response.data.codigos[0][0];});
							}
							if(this.cclase=='')
							{
								this.cclase=codigo.substr(0,4);
								axios.post('vue/presupuesto_ccp/ccp-bancoproyectos.php?buscar=nombregrupo&grupo='+this.cclase)
								.then((response)=>{this.clase=this.cclase+" - "+response.data.codigos[0][0];});
							}
							if(this.csubclase)
							{
								this.csubclase=codigo.substr(0,5);
								axios.post('vue/presupuesto_ccp/ccp-bancoproyectos.php?buscar=nombregrupo&grupo='+this.csubclase)
								.then((response)=>{this.subclase=this.csubclase+" - "+response.data.codigos[0][0];});
							}
						}
					}
				);
			}
		},
		validasubclase: function(codigo)
		{
			if(codigo.length==5)
			{
				var formData = new FormData();
				var codbuscador='';
				var nivbuscador='';
				if (this.cclase!='')
				{
					codbuscador=this.cclase;
					nivbuscador='4';
				}
				else if (this.cgrupo!='')
				{
					codbuscador=this.cgrupo;
					nivbuscador='3';
				}
				else if (this.cdivision!='')
				{
					codbuscador=this.cdivision;
					nivbuscador='2';
				}
				else if (this.cseccion!='')
				{
					codbuscador=this.cseccion;
					nivbuscador='1';
				}
				formData.append("seccion",codbuscador);
				formData.append("nivel",nivbuscador);
				formData.append("codigo",codigo);
				if(this.clasificador=='2')
				{
					axios.post('vue/presupuesto_ccp/ccp-bancoproyectos.php?buscar=valsubclaseb',formData)
					.then(
						(response)=>
						{
							if(response.data.codigos[0][0]!='')
							{
								this.csubclase=codigo;
								this.subclase=codigo+" - "+response.data.codigos[0][0];
								if(this.cseccion=='')
								{
									this.cseccion=codigo.substr(0,1);
									axios.post('vue/presupuesto_ccp/ccp-bancoproyectos.php?buscar=nombregrupo&grupo='+this.cseccion)
									.then((response)=>{this.seccion=this.cseccion+" - "+response.data.codigos[0][0];});
								}
								if(this.cdivision=='')
								{
									this.cdivision=codigo.substr(0,2);
									axios.post('vue/presupuesto_ccp/ccp-bancoproyectos.php?buscar=nombregrupo&grupo='+this.cdivision)
									.then((response)=>{this.division=this.cdivision+" - "+response.data.codigos[0][0];});
								}
								if(this.cgrupo=='')
								{
									this.cgrupo=codigo.substr(0,3);
									axios.post('vue/presupuesto_ccp/ccp-bancoproyectos.php?buscar=nombregrupo&grupo='+this.cgrupo)
									.then((response)=>{this.grupo=this.cgrupo+" - "+response.data.codigos[0][0];});
								}
								if(this.cclase=='')
								{
									this.cclase=codigo.substr(0,4);
									axios.post('vue/presupuesto_ccp/ccp-bancoproyectos.php?buscar=nombregrupo&grupo='+this.cclase)
									.then((response)=>{this.clase=this.cclase+" - "+response.data.codigos[0][0];});
								}
							}
						}
					);
				}
				else
				{
					axios.post('vue/presupuesto_ccp/ccp-bancoproyectos.php?buscar=valsubclases',formData)
					.then(
						(response)=>
						{
							if(response.data.codigos[0][0]!='')
							{
								this.csubclase=codigo;
								this.subclase=codigo+" - "+response.data.codigos[0][0];
								if(this.cseccion=='')
								{
									this.cseccion=codigo.substr(0,1);
									axios.post('vue/presupuesto_ccp/ccp-bancoproyectos.php?buscar=nombregrupos&grupo='+this.cseccion)
									.then((response)=>{this.seccion=this.cseccion+" - "+response.data.codigos[0][0];});
								}
								if(this.cdivision=='')
								{
									this.cdivision=codigo.substr(0,2);
									axios.post('vue/presupuesto_ccp/ccp-bancoproyectos.php?buscar=nombregrupos&grupo='+this.cdivision)
									.then((response)=>{this.division=this.cdivision+" - "+response.data.codigos[0][0];});
								}
								if(this.cgrupo=='')
								{
									this.cgrupo=codigo.substr(0,3);
									axios.post('vue/presupuesto_ccp/ccp-bancoproyectos.php?buscar=nombregrupos&grupo='+this.cgrupo)
									.then((response)=>{this.grupo=this.cgrupo+" - "+response.data.codigos[0][0];});
								}
								if(this.cclase=='')
								{
									this.cclase=codigo.substr(0,4);
									axios.post('vue/presupuesto_ccp/ccp-bancoproyectos.php?buscar=nombregrupos&grupo='+this.cclase)
									.then((response)=>{this.clase=this.cclase+" - "+response.data.codigos[0][0];});
								}
							}
						}
					);
				}
			}
		},
		validaindicadorproducto:function(codigo)
		{
			var bandera1=0;
			if(this.selecproductosa.length!=0)
			{
				if(codigo.length>=4)
				{
					if(codigo.substr(0,4) == this.cprograma)
					{
						bandera1=0;
					}
					else 
					{
						bandera1=1;
						this.toggleMensaje();
						this.colortitulosmensaje='crimson';
						this.titulomensaje='Mensaje de Error';
						this.contenidomensaje='Todos los productos deben ser del mismo Sector y Programa';
					}
				}
			}
			if(codigo.length == 9 && bandera1 == 0)
			{
				
				var formData = new FormData();
				formData.append("codigo",codigo);
				axios.post('vue/presupuesto_ccp/ccp-bancoproyectos.php?buscar=valindicproducto',formData)
				.then(
					(response)=>
					{
						if(response.data.codigos[0][0]!='')
						{
							this.cindicadorpro = codigo;
							this.indicadorpro = codigo +" - " + response.data.codigos[0][0];
							this.cproducto = response.data.codigos[0][1];
							this.producto = response.data.codigos[0][1] + " - " + response.data.codigos[0][2];
							this.cprograma = codigo.substr(0,4);
							this.csector = codigo.substr(0,2);
							if(this.cprograma!='')
							{
								axios.post('vue/presupuesto_ccp/ccp-bancoproyectos.php?buscar=valprograma&programa='+this.cprograma)
								.then(
									(response)=>
									{
										this.programa = this.cprograma + " - "+response.data.programas[0][0];
										this.csubprograma=response.data.programas[0][1];
										this.subprograma=this.csubprograma + " - " + response.data.programas[0][2];
									}
								);
							}
							if(this.csector!='')
							{
								axios.post('vue/presupuesto_ccp/ccp-bancoproyectos.php?buscar=valsector&sector='+this.csector)
								.then(
									(response)=>
									{
										this.sector = this.csector + " - "+response.data.sectores[0][0];
									}
								);
							}
						}
					}
				);
			}
		},
		valorproductoxx:function(valor)
		{
			console.log(this.valoridentproducto);
		},
	},
});