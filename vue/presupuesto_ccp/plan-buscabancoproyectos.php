<?php
	require '../../comun.inc';
	$linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");
	$out = array('error' => false);
	if(isset($_GET['visualizar']))
	{
		$visualizar=$_GET['visualizar'];
		switch ($visualizar)
		{
			case 'cabecera':
				$sql="SELECT id,idunidadej,codigo,vigencia,nombre,descripcion,valortotal,aprobado,estado FROM ccpproyectospresupuesto WHERE vigencia ='".$_GET['vigencia']."' ORDER BY id DESC";
				$res=mysqli_query($linkbd,$sql);
				$codigos = array();
				while($row=mysqli_fetch_row($res))
				{
					array_push($codigos, $row);
				}
				$out['codigos'] = $codigos;
		}
	}
	if(isset($_GET['action']))
	{
		$action=$_GET['action'];
		if($action=='searchProyecto')
		{
			$keyword=$_POST['keyword'];
			$sql="SELECT id,idunidadej,codigo,vigencia,nombre,descripcion,valortotal,aprobado,estado FROM ccpproyectospresupuesto WHERE nombre like '%$keyword%' OR descripcion like '%$keyword%' OR codigo like '%$keyword%' ORDER BY id DESC";
			$res=mysqli_query($linkbd,$sql);
			$codigos = array();
			while($row=mysqli_fetch_row($res))
			{
				array_push($codigos, $row);
			}
				$out['codigos'] = $codigos;
		}
	}
	if(isset($_GET['elimina']))
	{
		$numdel = $_GET['elimina'];
		if($numdel != '')
		{
			$sql = "DELETE FROM ccpproyectospresupuesto WHERE id = $numdel";
			$res = mysqli_query($linkbd,$sql);
			$sql = "DELETE FROM ccpproyectospresupuesto_productos WHERE codproyecto = $numdel";
			$res = mysqli_query($linkbd,$sql);
			$sql = "DELETE FROM ccpproyectospresupuesto_presupuesto WHERE codproyecto = $numdel";
			$res = mysqli_query($linkbd,$sql);
		}
	}

	if($action == 'existePresupuestoProyecto'){
		$existe = false;
		if(isset($_POST['bpim']))
		{
			$sqlr = "SELECT SUM(TB2.valorcsf), SUM(TB2.valorssf) FROM ccpproyectospresupuesto AS TB1, ccpproyectospresupuesto_presupuesto AS TB2 WHERE TB1.codigo = '".$_POST['bpim']."' AND TB1.vigencia = '".$_POST['vigencia']."' AND TB1.id = TB2.codproyecto";
			$resp = mysqli_query($linkbd, $sqlr);
			$row = mysqli_fetch_row($resp);

			if($row[0] > 0 || $row[1] > 0){
				$existe = true;
			}

			$sqlrAd = "SELECT SUM(valor) FROM ccpet_adiciones WHERE bpim = '".$_POST['bpim']."' AND vigencia = '".$_POST['vigencia']."' ";
			$respAd = mysqli_query($linkbd, $sqlrAd);
			$rowAd = mysqli_fetch_row($respAd);

			if($rowAd[0] > 0){
				$existe = true;
			}

			$sqlrTraslado = "SELECT SUM(valor) FROM ccpet_traslados WHERE bpim = '".$_POST['bpim']."' AND vigencia = '".$_POST['vigencia']."' ";
			$respTraslado = mysqli_query($linkbd, $sqlrTraslado);
			$rowTraslado = mysqli_fetch_row($respTraslado);
			
			if($rowTraslado[0] > 0){
				$existe = true;
			}

		}
		$out['existe'] = $existe;
	}

	header("Content-type: application/json");
	echo json_encode($out);
	die();
?>