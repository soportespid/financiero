<?php
	require '../../comun.inc';
    require '../../validaciones.inc';
	$linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");
	$out = array('error' => false);

	$action="show";

	if(isset($_GET['buscar']))
	{
		$buscar=$_GET['buscar'];
		switch ($buscar)
		{			
			case 'actos':
			{
				$sqlActos = "SELECT id_acuerdo,numero_acuerdo,valorreduccion FROM ccpetacuerdos WHERE estado = 'S' AND valorreduccion > 0 ";
				$resActos = mysqli_query($linkbd,$sqlActos);
				$codigos = array();
                $valorAdicion = array();
				while($row = mysqli_fetch_row($resActos))
				{
                    $valorAdicion[$row[0]] = $row[2];
					array_push($codigos, $row);
				}
				$out['codigos'] = $codigos;
                $out['valorAdicion'] = $valorAdicion;
			}break;

            case 'proyectosProductos':
            {
                $id = $_GET['id'];
                $unidadEjercutora = $_GET['ejecutora'];

                $sqlProyectosProductos = "SELECT sector,programa,subprograma,producto,indicador,valortotal FROM ccpproyectospresupuesto_productos WHERE codproyecto = $id AND estado = 'S'";
                $resProyectosProductos = mysqli_query($linkbd,$sqlProyectosProductos);
                $codigos = array();
                $nombres = array();
                while($rowProyectosProductos = mysqli_fetch_row($resProyectosProductos))
                {
                    $sqlNombres = "SELECT S.nombre,P.nombre,P.nombre_subprograma,U.nombre,PR.producto,PR.indicador_producto FROM ccpetsectores S, ccpetprogramas P, pptouniejecu U, ccpetproductos PR WHERE S.codigo = $rowProyectosProductos[0] AND P.codigo = $rowProyectosProductos[1] AND P.codigo_subprograma = $rowProyectosProductos[2] AND U.id_cc = '$unidadEjercutora' AND PR.cod_producto = $rowProyectosProductos[3] AND PR.codigo_indicador = $rowProyectosProductos[4] ";
                    $resNombres = mysqli_query($linkbd,$sqlNombres);
                    while($rowNombres = mysqli_fetch_row($resNombres))
                    {
                        array_push($nombres,$rowNombres);
                    }

                    array_push($codigos,$rowProyectosProductos);
                }

                $sqlPresupuesto = "SELECT rubro,medio_pago,valorcsf,valorssf,clasificacion,subclase,subproducto,id_fuente,indicador_producto FROM ccpproyectospresupuesto_presupuesto WHERE codproyecto = $id ";
                $presupuestoGasto = array();
                $nombresCCPET = array();
                $resPresupuesto = mysqli_query($linkbd,$sqlPresupuesto);
                while($rowPresupuesto = mysqli_fetch_row($resPresupuesto))
                {
                    $sqlCuentasCCPET = "SELECT nombre,tipo FROM cuentasccpet WHERE codigo = '$rowPresupuesto[0]' ";
                    $resCuentasCCPET = mysqli_query($linkbd,$sqlCuentasCCPET);
                    while($rowCuentasCCPET = mysqli_fetch_row($resCuentasCCPET))
                    {
                        array_push($rowPresupuesto, $rowCuentasCCPET[0]);
                        array_push($rowPresupuesto, $rowCuentasCCPET[1]);
                    }
                    
                    array_push($presupuestoGasto, $rowPresupuesto);
                }
                

                $out['presupuesto'] = $presupuestoGasto;
                $out['codigos'] = $codigos;
                $out['nombres'] = $nombres;


            }break;

            case 'cuentasGastos':
            {
                $id     = $_GET['id'];
                $codigo = $_GET['codigo'];
                $tipo   = $_GET['tipo'];
                $valor  = $_GET['valor'];
                $clasificacion = $_GET['clasificacion'];
                $vigencia = $_GET['vigencia'];

                switch($clasificacion)
                {
                    case '0':
                    {
                        //Sin clasificador CSF
                        if($tipo == 'CSF')
                        {
                            $sql = "SELECT id_fuente,indicador_producto,metas,politicas_publicas,vigencia_gasto FROM ccpproyectospresupuesto_presupuesto WHERE codproyecto = $id AND rubro = '$codigo' AND medio_pago = '$tipo' AND valorcsf = $valor ";
                            $res = mysqli_query($linkbd,$sql);
                            $row = mysqli_fetch_row($res);

                            $sqlFuente = "SELECT nombre FROM ccpet_fuentes_cuipo WHERE codigo_fuente = '$row[0]' ";
                            $resFuente = mysqli_query($linkbd,$sqlFuente);
                            $rowFuente = mysqli_fetch_row($resFuente);

                            if($rowFuente[0] == NULL)
                            {
                                $sqlFuente = "SELECT fuente_financiacion FROM ccpet_fuentes WHERE id_fuente = '$row[0]' ";
                                $resFuente = mysqli_query($linkbd,$sqlFuente);
                                $rowFuente = mysqli_fetch_row($resFuente);
                            }

                            $out['codigoFuente'] = $row[0];
                            $out['nombreFuente'] = $rowFuente[0];

                            if($row[1] != '')
                            {
                                $sqlIndicador = "SELECT indicador_producto FROM ccpetproductos WHERE codigo_indicador = '$row[1]' ";
                                $resIndicador = mysqli_query($linkbd,$sqlIndicador);
                                $rowIndicador = mysqli_fetch_row($resIndicador);    

                                $out['codigoIndicador'] = $row[1];
                                $out['nombreIndicador'] = $rowIndicador[0];
                            }
                            else
                            {
                                $out['codigoIndicador'] = $row[1];
                                $out['nombreIndicador'] = '';
                            }

                            if($row[2] != 0)
                            {
                                $sqlMeta = "SELECT meta_pdm FROM ccpet_metapdm WHERE id = $row[2] ";
                                $resMeta = mysqli_query($linkbd,$sqlMeta);
                                $rowMeta = mysqli_fetch_row($resMeta);

                                $out['codigoMeta'] = $row[2];
                                $out['nombreMeta'] = $rowMeta[0];
                            }
                            else
                            {
                                $out['codigoMeta'] = '';
                                $out['nombreMeta'] = '';
                            }

                            if($row[3] != '')
                            {
                                $sqlPoliticaPublica = "SELECT nombre FROM ccpet_politicapublica WHERE codigo = '$row[3]' ";
                                $resPoliticaPublica = mysqli_query($linkbd,$sqlPoliticaPublica);
                                $rowPoliticaPublica = mysqli_fetch_row($resPoliticaPublica);

                                $out['codigoPoliticaPublica'] = $row[3];
                                $out['nombrePoliticaPublica'] = $rowPoliticaPublica[0];
                            }
                            else
                            {
                                $out['codigoPoliticaPublica'] = $row[3];
                                $out['nombrePoliticaPublica'] = '';
                            }

                            if($row[4] != '')
                            {
                                $sqlVigenciaGasto = "SELECT nombre FROM ccpet_vigenciadelgasto WHERE codigo = '$row[4]' ";
                                $resVigenciaGasto = mysqli_query($linkbd,$sqlVigenciaGasto);
                                $rowVigenciaGasto = mysqli_fetch_row($resVigenciaGasto);

                                $out['codigoVigenciaGasto'] = $row[4];
                                $out['nombreVigenciaGasto'] = $rowVigenciaGasto[0];
                            }
                            else
                            {
                                $out['codigoVigenciaGasto'] = $row[4];
                                $out['nombreVigenciaGasto'] = '';
                            }
                            
                            $saldo = generaSaldoCcpet($codigo,$vigencia,$row[0],$tipo,'','',$row[1]);
                            $out['saldo'] = $saldo;
                            
                        }

                        //Sin clasificador SSF
                        
                        if($tipo == 'SSF')
                        {
                            $sql = "SELECT id_fuente,indicador_producto,metas,politicas_publicas,vigencia_gasto FROM ccpproyectospresupuesto_presupuesto WHERE codproyecto = $id AND rubro = '$codigo' AND medio_pago = '$tipo' AND valorssf = $valor ";
                            $res = mysqli_query($linkbd,$sql);
                            $row = mysqli_fetch_row($res);

                            $sqlFuente = "SELECT nombre FROM ccpet_fuentes_cuipo WHERE codigo_fuente = '$row[0]' ";
                            $resFuente = mysqli_query($linkbd,$sqlFuente);
                            $rowFuente = mysqli_fetch_row($resFuente);

                            if($rowFuente[0] == NULL)
                            {
                                $sqlFuente = "SELECT fuente_financiacion FROM ccpet_fuentes WHERE id_fuente = '$row[0]' ";
                                $resFuente = mysqli_query($linkbd,$sqlFuente);
                                $rowFuente = mysqli_fetch_row($resFuente);
                            }

                            $out['codigoFuente'] = $row[0];
                            $out['nombreFuente'] = $rowFuente[0];

                            if($row[1] != '')
                            {
                                $sqlIndicador = "SELECT indicador_producto FROM ccpetproductos WHERE codigo_indicador = '$row[1]' ";
                                $resIndicador = mysqli_query($linkbd,$sqlIndicador);
                                $rowIndicador = mysqli_fetch_row($resIndicador);    

                                $out['codigoIndicador'] = $row[1];
                                $out['nombreIndicador'] = $rowIndicador[0];
                            }
                            else
                            {
                                $out['codigoIndicador'] = $row[1];
                                $out['nombreIndicador'] = '';
                            }

                            if($row[2] != 0)
                            {
                                $sqlMeta = "SELECT meta_pdm FROM ccpet_metapdm WHERE id = $row[2] ";
                                $resMeta = mysqli_query($linkbd,$sqlMeta);
                                $rowMeta = mysqli_fetch_row($resMeta);

                                $out['codigoMeta'] = $row[2];
                                $out['nombreMeta'] = $rowMeta[0];
                            }
                            else
                            {
                                $out['codigoMeta'] = '';
                                $out['nombreMeta'] = '';
                            }

                            if($row[3] != '')
                            {
                                $sqlPoliticaPublica = "SELECT nombre FROM ccpet_politicapublica WHERE codigo = '$row[3]' ";
                                $resPoliticaPublica = mysqli_query($linkbd,$sqlPoliticaPublica);
                                $rowPoliticaPublica = mysqli_fetch_row($resPoliticaPublica);

                                $out['codigoPoliticaPublica'] = $row[3];
                                $out['nombrePoliticaPublica'] = $rowPoliticaPublica[0];
                            }
                            else
                            {
                                $out['codigoPoliticaPublica'] = $row[3];
                                $out['nombrePoliticaPublica'] = '';
                            }

                            if($row[4] != '')
                            {
                                $sqlVigenciaGasto = "SELECT nombre FROM ccpet_vigenciadelgasto WHERE codigo = '$row[4]' ";
                                $resVigenciaGasto = mysqli_query($linkbd,$sqlVigenciaGasto);
                                $rowVigenciaGasto = mysqli_fetch_row($resVigenciaGasto);

                                $out['codigoVigenciaGasto'] = $row[4];
                                $out['nombreVigenciaGasto'] = $rowVigenciaGasto[0];
                            }
                            else
                            {
                                $out['codigoVigenciaGasto'] = $row[4];
                                $out['nombreVigenciaGasto'] = '';
                            }

                            $saldo = generaSaldoCcpet($codigo,$vigencia,$row[0],$tipo,'','',$row[1]);
                            $out['saldo'] = $saldo;
                        }
                    }break;

                    case '1':
                    {
                        if($tipo == 'CSF')
                        {
                            $sql = "SELECT id_fuente,indicador_producto,metas,politicas_publicas,vigencia_gasto,identidad,nitentidad,codigocuin FROM ccpproyectospresupuesto_presupuesto WHERE codproyecto = $id AND rubro = '$codigo' AND medio_pago = '$tipo' AND valorcsf = $valor ";
                            $res = mysqli_query($linkbd,$sql);
                            $row = mysqli_fetch_row($res);

                            $sqlFuente = "SELECT nombre FROM ccpet_fuentes_cuipo WHERE codigo_fuente = '$row[0]' ";
                            $resFuente = mysqli_query($linkbd,$sqlFuente);
                            $rowFuente = mysqli_fetch_row($resFuente);

                            if($rowFuente[0] == NULL)
                            {
                                $sqlFuente = "SELECT fuente_financiacion FROM ccpet_fuentes WHERE id_fuente = '$row[0]' ";
                                $resFuente = mysqli_query($linkbd,$sqlFuente);
                                $rowFuente = mysqli_fetch_row($resFuente);
                            }

                            $out['codigoFuente'] = $row[0];
                            $out['nombreFuente'] = $rowFuente[0];

                            if($row[1] != '')
                            {
                                $sqlIndicador = "SELECT indicador_producto FROM ccpetproductos WHERE codigo_indicador = '$row[1]' ";
                                $resIndicador = mysqli_query($linkbd,$sqlIndicador);
                                $rowIndicador = mysqli_fetch_row($resIndicador);    

                                $out['codigoIndicador'] = $row[1];
                                $out['nombreIndicador'] = $rowIndicador[0];
                            }
                            else
                            {
                                $out['codigoIndicador'] = $row[1];
                                $out['nombreIndicador'] = '';
                            }

                            if($row[2] != 0)
                            {
                                $sqlMeta = "SELECT meta_pdm FROM ccpet_metapdm WHERE id = $row[2] ";
                                $resMeta = mysqli_query($linkbd,$sqlMeta);
                                $rowMeta = mysqli_fetch_row($resMeta);

                                $out['codigoMeta'] = $row[2];
                                $out['nombreMeta'] = $rowMeta[0];
                            }
                            else
                            {
                                $out['codigoMeta'] = '';
                                $out['nombreMeta'] = '';
                            }

                            if($row[3] != '')
                            {
                                $sqlPoliticaPublica = "SELECT nombre FROM ccpet_politicapublica WHERE codigo = '$row[3]' ";
                                $resPoliticaPublica = mysqli_query($linkbd,$sqlPoliticaPublica);
                                $rowPoliticaPublica = mysqli_fetch_row($resPoliticaPublica);

                                $out['codigoPoliticaPublica'] = $row[3];
                                $out['nombrePoliticaPublica'] = $rowPoliticaPublica[0];
                            }
                            else
                            {
                                $out['codigoPoliticaPublica'] = $row[3];
                                $out['nombrePoliticaPublica'] = '';
                            }

                            if($row[4] != '')
                            {
                                $sqlVigenciaGasto = "SELECT nombre FROM ccpet_vigenciadelgasto WHERE codigo = '$row[4]' ";
                                $resVigenciaGasto = mysqli_query($linkbd,$sqlVigenciaGasto);
                                $rowVigenciaGasto = mysqli_fetch_row($resVigenciaGasto);

                                $out['codigoVigenciaGasto'] = $row[4];
                                $out['nombreVigenciaGasto'] = $rowVigenciaGasto[0];
                            }
                            else
                            {
                                $out['codigoVigenciaGasto'] = $row[4];
                                $out['nombreVigenciaGasto'] = '';
                            }

                            $sqlCUIN = "SELECT nombre FROM ccpet_cuin WHERE codigo_cuin = '$row[7]' ";
                            $resCUIN = mysqli_query($linkbd,$sqlCUIN);
                            $rowCUIN = mysqli_fetch_row($resCUIN);

                            $out['idEntidad'] = $row[5];
                            $out['nit'] = $row[6];
                            $out['cuin'] = $row[7];
                            $out['nombreCuin'] = $rowCUIN[0];

                            $saldo = generaSaldoCcpet($codigo,$vigencia,$row[0],$tipo,'','',$row[1]);
                            $out['saldo'] = $saldo;
                        }

                        if($tipo == 'SSF')
                        {
                            $sql = "SELECT id_fuente,indicador_producto,metas,politicas_publicas,vigencia_gasto,identidad,nitentidad,codigocuin FROM ccpproyectospresupuesto_presupuesto WHERE codproyecto = $id AND rubro = '$codigo' AND medio_pago = '$tipo' AND valorssf = $valor ";
                            $res = mysqli_query($linkbd,$sql);
                            $row = mysqli_fetch_row($res);

                            $sqlFuente = "SELECT nombre FROM ccpet_fuentes_cuipo WHERE codigo_fuente = '$row[0]' ";
                            $resFuente = mysqli_query($linkbd,$sqlFuente);
                            $rowFuente = mysqli_fetch_row($resFuente);

                            if($rowFuente[0] == NULL)
                            {
                                $sqlFuente = "SELECT fuente_financiacion FROM ccpet_fuentes WHERE id_fuente = '$row[0]' ";
                                $resFuente = mysqli_query($linkbd,$sqlFuente);
                                $rowFuente = mysqli_fetch_row($resFuente);
                            }

                            $out['codigoFuente'] = $row[0];
                            $out['nombreFuente'] = $rowFuente[0];

                            if($row[1] != '')
                            {
                                $sqlIndicador = "SELECT indicador_producto FROM ccpetproductos WHERE codigo_indicador = '$row[1]' ";
                                $resIndicador = mysqli_query($linkbd,$sqlIndicador);
                                $rowIndicador = mysqli_fetch_row($resIndicador);    

                                $out['codigoIndicador'] = $row[1];
                                $out['nombreIndicador'] = $rowIndicador[0];
                            }
                            else
                            {
                                $out['codigoIndicador'] = $row[1];
                                $out['nombreIndicador'] = '';
                            }

                            if($row[2] != 0)
                            {
                                $sqlMeta = "SELECT meta_pdm FROM ccpet_metapdm WHERE id = $row[2] ";
                                $resMeta = mysqli_query($linkbd,$sqlMeta);
                                $rowMeta = mysqli_fetch_row($resMeta);

                                $out['codigoMeta'] = $row[2];
                                $out['nombreMeta'] = $rowMeta[0];
                            }
                            else
                            {
                                $out['codigoMeta'] = '';
                                $out['nombreMeta'] = '';
                            }

                            if($row[3] != '')
                            {
                                $sqlPoliticaPublica = "SELECT nombre FROM ccpet_politicapublica WHERE codigo = '$row[3]' ";
                                $resPoliticaPublica = mysqli_query($linkbd,$sqlPoliticaPublica);
                                $rowPoliticaPublica = mysqli_fetch_row($resPoliticaPublica);

                                $out['codigoPoliticaPublica'] = $row[3];
                                $out['nombrePoliticaPublica'] = $rowPoliticaPublica[0];
                            }
                            else
                            {
                                $out['codigoPoliticaPublica'] = $row[3];
                                $out['nombrePoliticaPublica'] = '';
                            }

                            if($row[4] != '')
                            {
                                $sqlVigenciaGasto = "SELECT nombre FROM ccpet_vigenciadelgasto WHERE codigo = '$row[4]' ";
                                $resVigenciaGasto = mysqli_query($linkbd,$sqlVigenciaGasto);
                                $rowVigenciaGasto = mysqli_fetch_row($resVigenciaGasto);

                                $out['codigoVigenciaGasto'] = $row[4];
                                $out['nombreVigenciaGasto'] = $rowVigenciaGasto[0];
                            }
                            else
                            {
                                $out['codigoVigenciaGasto'] = $row[4];
                                $out['nombreVigenciaGasto'] = '';
                            }

                            $sqlCUIN = "SELECT nombre FROM ccpet_cuin WHERE codigo_cuin = '$row[7]' ";
                            $resCUIN = mysqli_query($linkbd,$sqlCUIN);
                            $rowCUIN = mysqli_fetch_row($resCUIN);

                            $out['idEntidad'] = $row[5];
                            $out['nit'] = $row[6];
                            $out['cuin'] = $row[7];
                            $out['nombreCuin'] = $rowCUIN[0];

                            $saldo = generaSaldoCcpet($codigo,$vigencia,$row[0],$tipo,'','',$row[1]);
                            $out['saldo'] = $saldo;
                        }
                    }break;

                    case '2':
                    {
                        if($tipo == 'CSF')
                        {
                            $sql = "SELECT id_fuente,indicador_producto,metas,politicas_publicas,vigencia_gasto,seccion,divicion,grupo,clase,subclase,subproducto FROM ccpproyectospresupuesto_presupuesto WHERE codproyecto = $id AND rubro = '$codigo' AND medio_pago = '$tipo' AND valorcsf = $valor ";
                            $res = mysqli_query($linkbd,$sql);
                            $row = mysqli_fetch_row($res);

                            $sqlFuente = "SELECT nombre FROM ccpet_fuentes_cuipo WHERE codigo_fuente = '$row[0]' ";
                            $resFuente = mysqli_query($linkbd,$sqlFuente);
                            $rowFuente = mysqli_fetch_row($resFuente);

                            if($rowFuente[0] == NULL)
                            {
                                $sqlFuente = "SELECT fuente_financiacion FROM ccpet_fuentes WHERE id_fuente = '$row[0]' ";
                                $resFuente = mysqli_query($linkbd,$sqlFuente);
                                $rowFuente = mysqli_fetch_row($resFuente);
                            }

                            $out['codigoFuente'] = $row[0];
                            $out['nombreFuente'] = $rowFuente[0];

                            if($row[1] != '')
                            {
                                $sqlIndicador = "SELECT indicador_producto FROM ccpetproductos WHERE codigo_indicador = '$row[1]' ";
                                $resIndicador = mysqli_query($linkbd,$sqlIndicador);
                                $rowIndicador = mysqli_fetch_row($resIndicador);    

                                $out['codigoIndicador'] = $row[1];
                                $out['nombreIndicador'] = $rowIndicador[0];
                            }
                            else
                            {
                                $out['codigoIndicador'] = $row[1];
                                $out['nombreIndicador'] = '';
                            }

                            if($row[2] != 0)
                            {
                                $sqlMeta = "SELECT meta_pdm FROM ccpet_metapdm WHERE id = $row[2] ";
                                $resMeta = mysqli_query($linkbd,$sqlMeta);
                                $rowMeta = mysqli_fetch_row($resMeta);

                                $out['codigoMeta'] = $row[2];
                                $out['nombreMeta'] = $rowMeta[0];
                            }
                            else
                            {
                                $out['codigoMeta'] = '';
                                $out['nombreMeta'] = '';
                            }

                            if($row[3] != '')
                            {
                                $sqlPoliticaPublica = "SELECT nombre FROM ccpet_politicapublica WHERE codigo = '$row[3]' ";
                                $resPoliticaPublica = mysqli_query($linkbd,$sqlPoliticaPublica);
                                $rowPoliticaPublica = mysqli_fetch_row($resPoliticaPublica);

                                $out['codigoPoliticaPublica'] = $row[3];
                                $out['nombrePoliticaPublica'] = $rowPoliticaPublica[0];
                            }
                            else
                            {
                                $out['codigoPoliticaPublica'] = $row[3];
                                $out['nombrePoliticaPublica'] = '';
                            }

                            if($row[4] != '')
                            {
                                $sqlVigenciaGasto = "SELECT nombre FROM ccpet_vigenciadelgasto WHERE codigo = '$row[4]' ";
                                $resVigenciaGasto = mysqli_query($linkbd,$sqlVigenciaGasto);
                                $rowVigenciaGasto = mysqli_fetch_row($resVigenciaGasto);

                                $out['codigoVigenciaGasto'] = $row[4];
                                $out['nombreVigenciaGasto'] = $rowVigenciaGasto[0];
                            }
                            else
                            {
                                $out['codigoVigenciaGasto'] = $row[4];
                                $out['nombreVigenciaGasto'] = '';
                            }

                            $sqlSeccion = "SELECT titulo FROM ccpetbienestransportables WHERE grupo = '$row[5]' ";
                            $resSeccion = mysqli_query($linkbd,$sqlSeccion);
                            $rowSeccion = mysqli_fetch_row($resSeccion);

                            $sqlDivision = "SELECT titulo FROM ccpetbienestransportables WHERE grupo = '$row[6]' ";
                            $resDivision = mysqli_query($linkbd,$sqlDivision);
                            $rowDivision = mysqli_fetch_row($resDivision);

                            $sqlGrupo = "SELECT titulo FROM ccpetbienestransportables WHERE grupo = '$row[7]' ";
                            $resGrupo = mysqli_query($linkbd,$sqlGrupo);
                            $rowGrupo = mysqli_fetch_row($resGrupo);

                            $sqlClase = "SELECT titulo FROM ccpetbienestransportables WHERE grupo = '$row[8]' ";
                            $resClase = mysqli_query($linkbd,$sqlClase);
                            $rowClase = mysqli_fetch_row($resClase);

                            $sqlSubclase = "SELECT titulo FROM ccpetbienestransportables WHERE grupo = '$row[9]' ";
                            $resSubclase = mysqli_query($linkbd,$sqlSubclase);
                            $rowSubclase = mysqli_fetch_row($resSubclase);

                            $sqlSubproducto = "SELECT titulo FROM ccpetbienestransportables WHERE grupo = '$row[10]' ";
                            $resSubproducto = mysqli_query($linkbd,$sqlSubproducto);
                            $rowSubproducto = mysqli_fetch_row($resSubproducto);

                            $out['codigoSeccion'] = $row[5];
                            $out['nombreSeccion'] = $rowSeccion[0];
                            $out['codigoDivision'] = $row[6];
                            $out['nombreDivision'] = $rowDivision[0];
                            $out['codigoGrupo'] = $row[7];
                            $out['nombreGrupo'] = $rowGrupo[0];
                            $out['codigoClase'] = $row[8];
                            $out['nombreClase'] = $rowClase[0];
                            $out['codigoSubclase'] = $row[9];
                            $out['nombreSubclase'] = $rowSubclase[0];
                            $out['codigoSubproducto'] = $row[10];
                            $out['nombreSubproducto'] = $rowSubproducto[0];

                            $saldo = generaSaldoCcpet($codigo,$vigencia,$row[0],$tipo,'',$row[10],$row[1]);
                            $out['saldo'] = $saldo;
                        
                        }

                        if($tipo == 'SSF')
                        {
                            $sql = "SELECT id_fuente,indicador_producto,metas,politicas_publicas,vigencia_gasto,seccion,divicion,grupo,clase,subclase,subproducto FROM ccpproyectospresupuesto_presupuesto WHERE codproyecto = $id AND rubro = '$codigo' AND medio_pago = '$tipo' AND valorssf = $valor ";
                            $res = mysqli_query($linkbd,$sql);
                            $row = mysqli_fetch_row($res);

                            $sqlFuente = "SELECT nombre FROM ccpet_fuentes_cuipo WHERE codigo_fuente = '$row[0]' ";
                            $resFuente = mysqli_query($linkbd,$sqlFuente);
                            $rowFuente = mysqli_fetch_row($resFuente);

                            if($rowFuente[0] == NULL)
                            {
                                $sqlFuente = "SELECT fuente_financiacion FROM ccpet_fuentes WHERE id_fuente = '$row[0]' ";
                                $resFuente = mysqli_query($linkbd,$sqlFuente);
                                $rowFuente = mysqli_fetch_row($resFuente);
                            }

                            $out['codigoFuente'] = $row[0];
                            $out['nombreFuente'] = $rowFuente[0];

                            if($row[1] != '')
                            {
                                $sqlIndicador = "SELECT indicador_producto FROM ccpetproductos WHERE codigo_indicador = '$row[1]' ";
                                $resIndicador = mysqli_query($linkbd,$sqlIndicador);
                                $rowIndicador = mysqli_fetch_row($resIndicador);    

                                $out['codigoIndicador'] = $row[1];
                                $out['nombreIndicador'] = $rowIndicador[0];
                            }
                            else
                            {
                                $out['codigoIndicador'] = $row[1];
                                $out['nombreIndicador'] = '';
                            }

                            if($row[2] != 0)
                            {
                                $sqlMeta = "SELECT meta_pdm FROM ccpet_metapdm WHERE id = $row[2] ";
                                $resMeta = mysqli_query($linkbd,$sqlMeta);
                                $rowMeta = mysqli_fetch_row($resMeta);

                                $out['codigoMeta'] = $row[2];
                                $out['nombreMeta'] = $rowMeta[0];
                            }
                            else
                            {
                                $out['codigoMeta'] = '';
                                $out['nombreMeta'] = '';
                            }

                            if($row[3] != '')
                            {
                                $sqlPoliticaPublica = "SELECT nombre FROM ccpet_politicapublica WHERE codigo = '$row[3]' ";
                                $resPoliticaPublica = mysqli_query($linkbd,$sqlPoliticaPublica);
                                $rowPoliticaPublica = mysqli_fetch_row($resPoliticaPublica);

                                $out['codigoPoliticaPublica'] = $row[3];
                                $out['nombrePoliticaPublica'] = $rowPoliticaPublica[0];
                            }
                            else
                            {
                                $out['codigoPoliticaPublica'] = $row[3];
                                $out['nombrePoliticaPublica'] = '';
                            }

                            if($row[4] != '')
                            {
                                $sqlVigenciaGasto = "SELECT nombre FROM ccpet_vigenciadelgasto WHERE codigo = '$row[4]' ";
                                $resVigenciaGasto = mysqli_query($linkbd,$sqlVigenciaGasto);
                                $rowVigenciaGasto = mysqli_fetch_row($resVigenciaGasto);

                                $out['codigoVigenciaGasto'] = $row[4];
                                $out['nombreVigenciaGasto'] = $rowVigenciaGasto[0];
                            }
                            else
                            {
                                $out['codigoVigenciaGasto'] = $row[4];
                                $out['nombreVigenciaGasto'] = '';
                            }

                            $sqlSeccion = "SELECT titulo FROM ccpetbienestransportables WHERE grupo = '$row[5]' ";
                            $resSeccion = mysqli_query($linkbd,$sqlSeccion);
                            $rowSeccion = mysqli_fetch_row($resSeccion);

                            $sqlDivision = "SELECT titulo FROM ccpetbienestransportables WHERE grupo = '$row[6]' ";
                            $resDivision = mysqli_query($linkbd,$sqlDivision);
                            $rowDivision = mysqli_fetch_row($resDivision);

                            $sqlGrupo = "SELECT titulo FROM ccpetbienestransportables WHERE grupo = '$row[7]' ";
                            $resGrupo = mysqli_query($linkbd,$sqlGrupo);
                            $rowGrupo = mysqli_fetch_row($resGrupo);

                            $sqlClase = "SELECT titulo FROM ccpetbienestransportables WHERE grupo = '$row[8]' ";
                            $resClase = mysqli_query($linkbd,$sqlClase);
                            $rowClase = mysqli_fetch_row($resClase);

                            $sqlSubclase = "SELECT titulo FROM ccpetbienestransportables WHERE grupo = '$row[9]' ";
                            $resSubclase = mysqli_query($linkbd,$sqlSubclase);
                            $rowSubclase = mysqli_fetch_row($resSubclase);

                            $sqlSubproducto = "SELECT titulo FROM ccpetbienestransportables WHERE grupo = '$row[10]' ";
                            $resSubproducto = mysqli_query($linkbd,$sqlSubproducto);
                            $rowSubproducto = mysqli_fetch_row($resSubproducto);

                            $out['codigoSeccion'] = $row[5];
                            $out['nombreSeccion'] = $rowSeccion[0];
                            $out['codigoDivision'] = $row[6];
                            $out['nombreDivision'] = $rowDivision[0];
                            $out['codigoGrupo'] = $row[7];
                            $out['nombreGrupo'] = $rowGrupo[0];
                            $out['codigoClase'] = $row[8];
                            $out['nombreClase'] = $rowClase[0];
                            $out['codigoSubclase'] = $row[9];
                            $out['nombreSubclase'] = $rowSubclase[0];
                            $out['codigoSubproducto'] = $row[10];
                            $out['nombreSubproducto'] = $rowSubproducto[0];

                            $saldo = generaSaldoCcpet($codigo,$vigencia,$row[0],$tipo,'',$row[10],$row[1]);
                            $out['saldo'] = $saldo;
                        }

                    }break;
                    
                    case '3':
                    {
                        if($tipo == 'CSF')
                        {
                            $sql = "SELECT id_fuente,indicador_producto,metas,politicas_publicas,vigencia_gasto,seccion,divicion,grupo,clase,subclase FROM ccpproyectospresupuesto_presupuesto WHERE codproyecto = $id AND rubro = '$codigo' AND medio_pago = '$tipo' AND valorcsf = $valor ";
                            $res = mysqli_query($linkbd,$sql);
                            $row = mysqli_fetch_row($res);

                            $sqlFuente = "SELECT nombre FROM ccpet_fuentes_cuipo WHERE codigo_fuente = '$row[0]' ";
                            $resFuente = mysqli_query($linkbd,$sqlFuente);
                            $rowFuente = mysqli_fetch_row($resFuente);

                            if($rowFuente[0] == NULL)
                            {
                                $sqlFuente = "SELECT fuente_financiacion FROM ccpet_fuentes WHERE id_fuente = '$row[0]' ";
                                $resFuente = mysqli_query($linkbd,$sqlFuente);
                                $rowFuente = mysqli_fetch_row($resFuente);
                            }

                            $out['codigoFuente'] = $row[0];
                            $out['nombreFuente'] = $rowFuente[0];

                            if($row[1] != '')
                            {
                                $sqlIndicador = "SELECT indicador_producto FROM ccpetproductos WHERE codigo_indicador = '$row[1]' ";
                                $resIndicador = mysqli_query($linkbd,$sqlIndicador);
                                $rowIndicador = mysqli_fetch_row($resIndicador);    

                                $out['codigoIndicador'] = $row[1];
                                $out['nombreIndicador'] = $rowIndicador[0];
                            }
                            else
                            {
                                $out['codigoIndicador'] = $row[1];
                                $out['nombreIndicador'] = '';
                            }

                            if($row[2] != 0)
                            {
                                $sqlMeta = "SELECT meta_pdm FROM ccpet_metapdm WHERE id = $row[2] ";
                                $resMeta = mysqli_query($linkbd,$sqlMeta);
                                $rowMeta = mysqli_fetch_row($resMeta);

                                $out['codigoMeta'] = $row[2];
                                $out['nombreMeta'] = $rowMeta[0];
                            }
                            else
                            {
                                $out['codigoMeta'] = '';
                                $out['nombreMeta'] = '';
                            }

                            if($row[3] != '')
                            {
                                $sqlPoliticaPublica = "SELECT nombre FROM ccpet_politicapublica WHERE codigo = '$row[3]' ";
                                $resPoliticaPublica = mysqli_query($linkbd,$sqlPoliticaPublica);
                                $rowPoliticaPublica = mysqli_fetch_row($resPoliticaPublica);

                                $out['codigoPoliticaPublica'] = $row[3];
                                $out['nombrePoliticaPublica'] = $rowPoliticaPublica[0];
                            }
                            else
                            {
                                $out['codigoPoliticaPublica'] = $row[3];
                                $out['nombrePoliticaPublica'] = '';
                            }

                            if($row[4] != '')
                            {
                                $sqlVigenciaGasto = "SELECT nombre FROM ccpet_vigenciadelgasto WHERE codigo = '$row[4]' ";
                                $resVigenciaGasto = mysqli_query($linkbd,$sqlVigenciaGasto);
                                $rowVigenciaGasto = mysqli_fetch_row($resVigenciaGasto);

                                $out['codigoVigenciaGasto'] = $row[4];
                                $out['nombreVigenciaGasto'] = $rowVigenciaGasto[0];
                            }
                            else
                            {
                                $out['codigoVigenciaGasto'] = $row[4];
                                $out['nombreVigenciaGasto'] = '';
                            }

                            $sqlSeccion = "SELECT titulo FROM ccpetservicios WHERE grupo = '$row[5]' ";
                            $resSeccion = mysqli_query($linkbd,$sqlSeccion);
                            $rowSeccion = mysqli_fetch_row($resSeccion);

                            $sqlDivision = "SELECT titulo FROM ccpetservicios WHERE grupo = '$row[6]' ";
                            $resDivision = mysqli_query($linkbd,$sqlDivision);
                            $rowDivision = mysqli_fetch_row($resDivision);

                            $sqlGrupo = "SELECT titulo FROM ccpetservicios WHERE grupo = '$row[7]' ";
                            $resGrupo = mysqli_query($linkbd,$sqlGrupo);
                            $rowGrupo = mysqli_fetch_row($resGrupo);

                            $sqlClase = "SELECT titulo FROM ccpetservicios WHERE grupo = '$row[8]' ";
                            $resClase = mysqli_query($linkbd,$sqlClase);
                            $rowClase = mysqli_fetch_row($resClase);

                            $sqlSubclase = "SELECT titulo FROM ccpetservicios WHERE grupo = '$row[9]' ";
                            $resSubclase = mysqli_query($linkbd,$sqlSubclase);
                            $rowSubclase = mysqli_fetch_row($resSubclase);

                            $out['codigoSeccion'] = $row[5];
                            $out['nombreSeccion'] = $rowSeccion[0];
                            $out['codigoDivision'] = $row[6];
                            $out['nombreDivision'] = $rowDivision[0];
                            $out['codigoGrupo'] = $row[7];
                            $out['nombreGrupo'] = $rowGrupo[0];
                            $out['codigoClase'] = $row[8];
                            $out['nombreClase'] = $rowClase[0];
                            $out['codigoSubclase'] = $row[9];
                            $out['nombreSubclase'] = $rowSubclase[0];

                            $saldo = generaSaldoCcpet($codigo,$vigencia,$row[0],$tipo,$row[9],'',$row[1]);
                            $out['saldo'] = $saldo;
                            
                        }

                        if($tipo == 'SSF')
                        {
                            $sql = "SELECT id_fuente,indicador_producto,metas,politicas_publicas,vigencia_gasto,seccion,divicion,grupo,clase,subclase FROM ccpproyectospresupuesto_presupuesto WHERE codproyecto = $id AND rubro = '$codigo' AND medio_pago = '$tipo' AND valorssf = $valor ";
                            $res = mysqli_query($linkbd,$sql);
                            $row = mysqli_fetch_row($res);

                            $sqlFuente = "SELECT nombre FROM ccpet_fuentes_cuipo WHERE codigo_fuente = '$row[0]' ";
                            $resFuente = mysqli_query($linkbd,$sqlFuente);
                            $rowFuente = mysqli_fetch_row($resFuente);

                            if($rowFuente[0] == NULL)
                            {
                                $sqlFuente = "SELECT fuente_financiacion FROM ccpet_fuentes WHERE id_fuente = '$row[0]' ";
                                $resFuente = mysqli_query($linkbd,$sqlFuente);
                                $rowFuente = mysqli_fetch_row($resFuente);
                            }

                            $out['codigoFuente'] = $row[0];
                            $out['nombreFuente'] = $rowFuente[0];

                            if($row[1] != '')
                            {
                                $sqlIndicador = "SELECT indicador_producto FROM ccpetproductos WHERE codigo_indicador = '$row[1]' ";
                                $resIndicador = mysqli_query($linkbd,$sqlIndicador);
                                $rowIndicador = mysqli_fetch_row($resIndicador);    

                                $out['codigoIndicador'] = $row[1];
                                $out['nombreIndicador'] = $rowIndicador[0];
                            }
                            else
                            {
                                $out['codigoIndicador'] = $row[1];
                                $out['nombreIndicador'] = '';
                            }

                            if($row[2] != 0)
                            {
                                $sqlMeta = "SELECT meta_pdm FROM ccpet_metapdm WHERE id = $row[2] ";
                                $resMeta = mysqli_query($linkbd,$sqlMeta);
                                $rowMeta = mysqli_fetch_row($resMeta);

                                $out['codigoMeta'] = $row[2];
                                $out['nombreMeta'] = $rowMeta[0];
                            }
                            else
                            {
                                $out['codigoMeta'] = '';
                                $out['nombreMeta'] = '';
                            }

                            if($row[3] != '')
                            {
                                $sqlPoliticaPublica = "SELECT nombre FROM ccpet_politicapublica WHERE codigo = '$row[3]' ";
                                $resPoliticaPublica = mysqli_query($linkbd,$sqlPoliticaPublica);
                                $rowPoliticaPublica = mysqli_fetch_row($resPoliticaPublica);

                                $out['codigoPoliticaPublica'] = $row[3];
                                $out['nombrePoliticaPublica'] = $rowPoliticaPublica[0];
                            }
                            else
                            {
                                $out['codigoPoliticaPublica'] = $row[3];
                                $out['nombrePoliticaPublica'] = '';
                            }

                            if($row[4] != '')
                            {
                                $sqlVigenciaGasto = "SELECT nombre FROM ccpet_vigenciadelgasto WHERE codigo = '$row[4]' ";
                                $resVigenciaGasto = mysqli_query($linkbd,$sqlVigenciaGasto);
                                $rowVigenciaGasto = mysqli_fetch_row($resVigenciaGasto);

                                $out['codigoVigenciaGasto'] = $row[4];
                                $out['nombreVigenciaGasto'] = $rowVigenciaGasto[0];
                            }
                            else
                            {
                                $out['codigoVigenciaGasto'] = $row[4];
                                $out['nombreVigenciaGasto'] = '';
                            }

                            $sqlSeccion = "SELECT titulo FROM ccpetservicios WHERE grupo = '$row[5]' ";
                            $resSeccion = mysqli_query($linkbd,$sqlSeccion);
                            $rowSeccion = mysqli_fetch_row($resSeccion);

                            $sqlDivision = "SELECT titulo FROM ccpetservicios WHERE grupo = '$row[6]' ";
                            $resDivision = mysqli_query($linkbd,$sqlDivision);
                            $rowDivision = mysqli_fetch_row($resDivision);

                            $sqlGrupo = "SELECT titulo FROM ccpetservicios WHERE grupo = '$row[7]' ";
                            $resGrupo = mysqli_query($linkbd,$sqlGrupo);
                            $rowGrupo = mysqli_fetch_row($resGrupo);

                            $sqlClase = "SELECT titulo FROM ccpetservicios WHERE grupo = '$row[8]' ";
                            $resClase = mysqli_query($linkbd,$sqlClase);
                            $rowClase = mysqli_fetch_row($resClase);

                            $sqlSubclase = "SELECT titulo FROM ccpetservicios WHERE grupo = '$row[9]' ";
                            $resSubclase = mysqli_query($linkbd,$sqlSubclase);
                            $rowSubclase = mysqli_fetch_row($resSubclase);

                            $out['codigoSeccion'] = $row[5];
                            $out['nombreSeccion'] = $rowSeccion[0];
                            $out['codigoDivision'] = $row[6];
                            $out['nombreDivision'] = $rowDivision[0];
                            $out['codigoGrupo'] = $row[7];
                            $out['nombreGrupo'] = $rowGrupo[0];
                            $out['codigoClase'] = $row[8];
                            $out['nombreClase'] = $rowClase[0];
                            $out['codigoSubclase'] = $row[9];
                            $out['nombreSubclase'] = $rowSubclase[0];

                            $saldo = generaSaldoCcpet($codigo,$vigencia,$row[0],$tipo,$row[9],'',$row[1]);
                            $out['saldo'] = $saldo;
                        }
                    }break;

                    
                }
                
            }

            case 'cuentasFuncionamiento':
            {
                $sql = "SELECT codigo,nombre,tipo FROM cuentasccpet WHERE codigo NOT LIKE '%2.3%' ";
                $res = mysqli_query($linkbd,$sql);
                $codigos = array();
                while($row = mysqli_fetch_row($res))
                {
                    array_push($codigos, $row);
                }
                $out['codigos'] = $codigos;

            }break;

            case 'clasificadores':
            {
                $cuenta = $_GET['cuenta'];

                $sql="SELECT clasificadores FROM ccpetprogramarclasificadoresgastos WHERE cuenta like '$cuenta' ORDER BY clasificadores"; 
                $res=mysqli_query($linkbd,$sql);
                $row=mysqli_fetch_row($res);
                $idclasificador = explode(",",$row[0]);
                $numidecla=count($idclasificador);
                if($numidecla > 0)
                {
                    $codigos = array();
                    for($x=0;$x < $numidecla;$x++)
                    {
                        if($idclasificador[$x]!='')
                        {
                            $sql2="SELECT id,nombre FROM ccpetclasificadores WHERE id='$idclasificador[$x]'";
                            $res2=mysqli_query($linkbd,$sql2);
                            $row2=mysqli_fetch_row($res2);
                            array_push($codigos, $row2);
                        }
                    }
                    $out['cuentaclasifi'] = $codigos;
                }
            }break;

            case 'clasificadorbys':
            {
                $cuenta    = $_GET['cuenta'];
                $vigencia  = $_GET['vigencia'];
                $fuente    = $_GET['fuente'];
                $medioPago = $_GET['medioPago'];
                $servicio  = $_GET['subclase'];
                $producto  = $_GET['producto'];
                $saldo;

                //echo $cuenta.' '.$vigencia.' '.$fuente.' '.$medioPago.' '.$servicio.' '.$producto;

                if($producto != '')
                {
                    $saldo = generaSaldoCcpet($cuenta,$vigencia,$fuente,$medioPago,'',$producto,'');
                }
                if($producto == '')
                {
                    $saldo = generaSaldoCcpet($cuenta,$vigencia,$fuente,$medioPago,$servicio,'','');
                }
                if($producto == '' && $servicio == '')
                {
                    $saldo = generaSaldoCcpet($cuenta,$vigencia,$fuente,$medioPago,'','','');
                }

                $out['saldo'] = $saldo;
            }break;
		}
	}
	if(isset($_GET['tablas']))
	{
		$ntablas=$_GET['tablas'];
		switch ($ntablas)
		{
			case 'metas':
			{
				$sql="SELECT * FROM ccpet_metapdm ORDER BY id";
				$res=mysqli_query($linkbd,$sql);
				$codigos = array();
				while($row=mysqli_fetch_row($res))
				{
					array_push($codigos, $row);
				}
				$out['codigos'] = $codigos;
			}break;

            case 'proyectosexistentes':
                {
                    $sqlProyectos = "SELECT id,nombre,descripcion,valortotal,idunidadej,codigo,vigencia FROM ccpproyectospresupuesto WHERE estado = 'S' ORDER BY id ";
                    $resProyectos = mysqli_query($linkbd,$sqlProyectos);
                    $codigos = array();
    
                    while($rowProyectos = mysqli_fetch_row($resProyectos))
                    {
                        array_push($codigos,$rowProyectos);
                    }
    
                    $sqlProyectos = "SELECT id,nombre,descripcion,valortotal,idunidadej,codigo,vigencia FROM ccpetadicion_inversion WHERE estado = 'S' ORDER BY id ";
                    $resProyectos = mysqli_query($linkbd,$sqlProyectos);
                    while($rowProyectos = mysqli_fetch_row($resProyectos))
                    {
                        array_push($codigos,$rowProyectos);
                    }
    
                    $out['codigos'] = $codigos;
                }break;
            case 'guardar':
            {
                for($x = 0; $x < count($_POST['gastos']); $x++){
        
                    $gastos = explode(",", $_POST['gastos'][$x]);
        
                    $sql = "INSERT INTO ccpetreduccion(acuerdo,rubro,fuente,vigencia_gasto,politica_publica,id_metas,indicador_producto,medio_pago,clasificacion,clasificador,cod_proyecto,bpin,vigencia,valor) VALUES ('$gastos[0]','$gastos[1]','$gastos[2]','$gastos[3]','$gastos[4]','$gastos[5]','$gastos[6]','$gastos[7]','$gastos[8]','$gastos[9]','$gastos[10]','$gastos[11]','$gastos[12]','$gastos[13]') ";
                    
                    mysqli_query($linkbd, $sql);
                    $out['insertaBien'] = true;
                }
                
                //Guarda Debito
                for($x = 0; $x < count($_POST['ingresos']); $x++){
        
                    $ingresos = explode(",", $_POST['ingresos'][$x]);
        
                    $sql = "INSERT INTO ccpetreduccion(acuerdo,rubro,fuente,vigencia_gasto,politica_publica,id_metas,indicador_producto,medio_pago,clasificacion,clasificador,cod_proyecto,bpin,vigencia,valor) VALUES ('$ingresos[0]','$ingresos[1]','$ingresos[2]','$ingresos[3]','$ingresos[4]','$ingresos[5]','$ingresos[6]','$ingresos[7]','$ingresos[8]','$ingresos[9]','$ingresos[10]','$ingresos[11]','$ingresos[12]','$ingresos[13]')";
                    //echo $sql;
                    mysqli_query($linkbd, $sql);
        
                    $out['insertaBien'] = true;
        
                }
            }
		}
	}
    
    if($action == "guardar")
    {
        
    }

	header("Content-type: application/json");
	echo json_encode($out);
	die();
?>