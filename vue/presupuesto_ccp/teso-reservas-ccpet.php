<?php
	require '../../comun.inc';
	$linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");
	$out = array('error' => false);

	$action="show";

    if(isset($_GET['action'])){
        $action=$_GET['action'];
    }

    if($action == "show")
    {
        //Creacion de variables
        $acuerdos = array();   
        $consecutivo;

        $sqlr = "SELECT id_acuerdo, numero_acuerdo, consecutivo FROM ccpetacuerdos WHERE valoradicion > 0 AND vigencia='".$_GET['vig']."'";  
        $res = mysqli_query($linkbd,$sqlr);
        
        while($row=mysqli_fetch_row($res))
        {
            array_push($acuerdos, $row);
        }
        $out['acuerdos'] = $acuerdos;

        $sqlConsecutivo = "SELECT MAX(id) FROM tesoreservas ";
        $resConsecutivo = mysqli_query($linkbd,$sqlConsecutivo);
        $rowConsecutivo = mysqli_fetch_row($resConsecutivo);

        $consecutivo = $rowConsecutivo[0] + 1;

        if(strlen($consecutivo) === 1)
        {
            $consecutivo = '0'.$consecutivo;
        }

        $out['consecutivo'] = $consecutivo;
    }
    
    if ($action == "guardar") 
    {
        for($x = 0; $x < count($_POST['cabecera']); $x++){

            $cabecera = explode(",", $_POST['cabecera'][$x]);

            $fecha = '';

            $fec = explode("/", $cabecera[2]);

            $fecha = $fec[2].'-'.$fec[1].'-'.$fec[0];

            $sql = "INSERT INTO tesoreservas(id,id_acuerdo,concepto,fecha,vigencia,valor,estado) VALUES ($cabecera[0],$cabecera[1],'$cabecera[4]','$fecha',$cabecera[3],$cabecera[5],'S')";
            mysqli_query($linkbd, $sql);

            $out['insertaBien'] = true;

        }

        for($x = 0; $x < count($_POST['detalles']); $x++)
        {
            $detalles = explode(",", $_POST['detalles'][$x]);

            $sql = "INSERT INTO tesoreservas_det(id_reserva,rubro,fuente,vigencia_gasto,clasificacion,clasificador,valor) VALUES ($cabecera[0],'$detalles[0]','$detalles[1]','$detalles[2]','16','$detalles[3]',$detalles[4])";
            mysqli_query($linkbd, $sql);
            echo $sql;
            $out['insertaBien'] = true;
        }
    }

    if(isset($_GET['buscar']))
	{
		$buscar   = $_GET['buscar'];
        $vigencia = $_GET['vigencia'];

		switch ($buscar)
		{
			case 'buscarAcuerdos':
            {
                $id_acuerdo=$_GET['acuerdo'];
                $codigos = array();

                $sqlAdicion = "SELECT cuenta, fuente, codigo_vigenciag, medio_pago, valor FROM ccpet_adiciones WHERE id_acuerdo = $id_acuerdo AND vigencia = $vigencia AND tipo_cuenta = 'I' ";    
                $resAdicion = mysqli_query($linkbd, $sqlAdicion);
                while($rowAdicion = mysqli_fetch_row($resAdicion))//2
                {
                    array_push($codigos, $rowAdicion);
                }

                $out['codigos'] = $codigos;

            }
		}
	}


header("Content-type: application/json");
echo json_encode($out);
die();

?>
