<?php
	require_once '../../comun.inc';
    require '../../funciones.inc';
    require '../../funcionesSP.inc.php';
    require 'funcionesccp.inc.php';
    session_start();
    /*ini_set('display_errors', '1');
    ini_set('display_startup_errors', '1');
    error_reporting(E_ALL);*/

	$linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");

    $maxVersion = ultimaVersionGastosCCPET();


    $out = array('error' => false);

    $action = "show";

    if(isset($_GET['action'])){
        $action = $_GET['action'];
    }

    if($action == 'show'){



    }

    if($action == 'fuentes'){

        $fuentes = [];
        $fuentesUsadas = [];
        $vigencia = $_POST['vigencia'];

        $sqlrIni = "SELECT DISTINCT fuente FROM ccpetinicialgastosfun WHERE vigencia = '".$vigencia."'";
        $resIni = mysqli_query($linkbd, $sqlrIni);
        while($rowIni = mysqli_fetch_row($resIni)){
            array_push($fuentesUsadas, $rowIni[0]);
        }

        $sqlrAd = "SELECT DISTINCT fuente FROM ccpet_adiciones WHERE vigencia = '".$vigencia."' AND tipo_cuenta = 'G'";
        $respAd = mysqli_query($linkbd, $sqlrAd);
        while($rowAd = mysqli_fetch_row($respAd)){
            array_push($fuentesUsadas, $rowAd[0]);
        }

        $sqlrTrasladosCred = "SELECT DISTINCT fuente FROM ccpet_traslados WHERE vigencia = '".$vigencia."' AND tipo = 'C'";
        $respTrasladosCred = mysqli_query($linkbd, $sqlrTrasladosCred);
        while($rowTrasladosCred = mysqli_fetch_row($respTrasladosCred)){
            array_push($fuentesUsadas, $rowTrasladosCred[0]);
        }

        $sqlrInv = "SELECT TB2.id_fuente FROM ccpproyectospresupuesto AS TB1, ccpproyectospresupuesto_presupuesto AS TB2 WHERE TB1.id = TB2.codproyecto AND TB1.vigencia = '$vigencia'";
        $respInv = mysqli_query($linkbd, $sqlrInv);
        while($rowInv = mysqli_fetch_row($respInv)){
            array_push($fuentesUsadas, $rowInv[0]);
        }

        $sqlrFuentes = "SELECT codigo_fuente, nombre FROM ccpet_fuentes_cuipo WHERE version = (SELECT MAX(version) FROM ccpet_fuentes_cuipo)";
        $respFuentes = mysqli_query($linkbd, $sqlrFuentes);
        while($rowFuentes = mysqli_fetch_row($respFuentes)){
            array_push($fuentes, $rowFuentes);
        }

        $out['fuentes'] = $fuentes;
        $out['fuentesUsadas'] = $fuentesUsadas;
    }

    if($action == 'presupuestoInicial'){

        //echo $_POST['seccionPresupuestal'];
        $seccionPresupuestal = $_POST['seccionPresupuestal'];
        $medioDePago = $_POST['medioDePago'];
        $vigenciaDelGasto = $_POST['vigenciaDelGasto'];

        $vigencia = $_POST['vigencia'];
        $vigencia_ini = $_POST['vigencia_ini'];
        $vigencia_fin = $_POST['vigencia_fin'];

        $fechaIni = $_POST['fechaIni'];
        $fechaFin = $_POST['fechaFin'];

        $tipoGasto = $_POST['tipoGasto'];


        $crit1 = '';
        $crit1_inv = '';
        $crit1_inv_P5 = '';
        $crit1_inv_P8 = '';
        $crit1_Ad = '';
        $crit1_Nom = '';
        $crit1_egresp_Nom = '';

        $cuentaMaxLargo = '';

        $crit3 = '';

        if($seccionPresupuestal != '-1'){
            $crit1 = "AND seccion_presupuestal = '$seccionPresupuestal'";
            $crit1Det = "AND DET.seccion_presupuestal = '$seccionPresupuestal'";
            $crit1_inv = "AND P1.idunidadej = '$seccionPresupuestal'";
            $crit1_inv_ini = "AND CP.idunidadej = '$seccionPresupuestal'";
            $crit1_inv_P5 = "AND P5.idunidadej = '$seccionPresupuestal'";
            $crit1_inv_P8 = "AND P8.idunidadej = '$seccionPresupuestal'";
            $crit1_Ad = "AND CA.seccion_presupuestal = '$seccionPresupuestal'";
            $crit1_Nom = "AND HD.seccion_presupuestal = '$seccionPresupuestal'";
            $crit1_egresp_Nom = "AND TD.seccion_presupuestal = '$seccionPresupuestal'";

        }

        $crit2 = '';
        $crit2_inv = '';
        $crit2_inv_P6 = '';
        $crit2_inv_P7 = '';
        $crit2_Ad = '';
        $crit2_Nom = '';
        $crit2_egreso_Nom = '';


        if($medioDePago != '-1'){

            $crit2 = "AND medio_pago = '$medioDePago'";
            $crit2Det = "AND DET.medio_pago = '$medioDePago'";
            $crit2_inv = "AND P3.medio_pago = '$medioDePago'";
            $crit2_inv_ini = "AND CPD.medio_pago = '$medioDePago'";
            $crit2_inv_P6 = "AND P6.medio_pago = '$medioDePago'";
            $crit2_inv_P9 = "AND P9.medio_pago = '$medioDePago'";
            $crit2_Ad = "AND CA.medio_pago = '$medioDePago'";
            $crit2_Nom = "AND HD.medio_pago = '$medioDePago'";
            $crit2_egreso_Nom = "AND TD.medio_pago = '$medioDePago'";
        }

        $crit3 = "";
        $crit3_inv = "";
        $crit3_inv_ini = "";
        $crit3_inv_P6 = "";
        $crit3_inv_P9 = "";
        $crit3_Ad = "";
        $crit3_Nom = "";
        $crit3_egreso_Nom = "";
        $crit3_egreso_Nom_td = "";

        if($vigenciaDelGasto != '-1'){

            $crit3 = "AND vigencia_gasto = '$vigenciaDelGasto'";
            $crit3_inv = "AND P3.vigencia_gasto = '$vigenciaDelGasto'";
            $crit3_inv_cdp = "AND codigo_vigenciag = '$vigenciaDelGasto'";
            $crit3_inv_cdpDet = "AND DET.codigo_vigenciag = '$vigenciaDelGasto'";
            $crit3_inv_ini = "AND CPD.vigencia_gasto = '$vigenciaDelGasto'";
            $crit3_inv_P6 = "AND P6.codigo_vigenciag = '$vigenciaDelGasto'";
            $crit3_inv_P9 = "AND P9.codigo_vigenciag = '$vigenciaDelGasto'";
            $crit3_Ad = "AND CA.codigo_vigenciag = '$vigenciaDelGasto'";
            $crit3_Nom = "AND HD.vigencia_gasto = '$vigenciaDelGasto'";
            $crit3_egreso_Nom = "AND TD.vigencia_gasto = '$vigenciaDelGasto'";
            $crit3_egreso_Nom_td = "AND TD.codigo_vigenciag = '$vigenciaDelGasto'";

        }

        $crit_vig_gasto = '';
        $crit_vig_gasto_det = '';
        $crit_vig_gasto_det_p = '';
        $crit_vig_gasto_det_nom = '';
        $crit_vig_gasto_det_td = '';
        $crit_vig_gasto_ad_inv = '';
        $crit_vig_gasto_tras_inv = '';
        $crit_vig_gasto_ad_inv_ca = '';

        if($tipoGasto != '-1'){
            $crit_vig_gasto = "AND cuenta LIKE '$tipoGasto%'";
            $crit_vig_gasto_det = "AND DET.cuenta LIKE '$tipoGasto%'";
            $crit_vig_gasto_det_p = "AND DET.cuentap LIKE '$tipoGasto%'";
            $crit_vig_gasto_det_nom = "AND HD.cuenta LIKE '$tipoGasto%'";
            $crit_vig_gasto_det_td = "AND TD.cuentap LIKE '$tipoGasto%'";
            if($tipoGastos == '2.3'){
                $crit_vig_gasto_ad_inv = "AND P6.bpim != ''";
                $crit_vig_gasto_tras_inv = "AND P9.bpim != ''";
                $crit_vig_gasto_ad_inv_ca = "AND CA.bpim != ''";
            }

        }

        $cuentasCcpet = array();
        $cuentasCcpetUsadasFun = array();
        $cuentasCcpetUsadas = array();
        $cuentasCcpetUsadasAd = array();
        $cuentasCcpetUsadasRed = array();
        $cuentasCcpetUsadasCred = array();
        $cuentasCcpetUsadasContraCred = array();
        $cuentasCcpetUsadasProy = array();
        $nombreSectores = array();
        $nombreProgramas = array();
        $nombreSubProgramas = array();
        $nombreProductos = array();
        $nombreProgramatico = array();
        $cuentasCcpetUsadasCdp = array();
        $cuentasCcpetUsadasCdpInv = array();
        $cuentasCcpetUsadasRpInv = array();
        $cuentasCcpetUsadasRp = array();
        $cuentasCcpetUsadasCxpInv = array();
        $cuentasCcpetUsadasCxp = array();
        $cuentasCcpetUsadasEgreso = array();
        $cuentasCcpetUsadasEgresoInv = array();
        $cuentasCcpetUsadasIniInv = array();
        $cuentasCcpetUsadasAdInv = array();
        $cuentasCcpetUsadasRedInv = array();
        $cuentasCcpetUsadasTrasladoCredInv = array();
        $cuentasCcpetUsadasTrasladoContraCredInv = array();

        $sqlrMaxLargo = "SELECT codigo FROM cuentasccpet WHERE LENGTH(codigo) = (SELECT MAX(LENGTH(codigo)) FROM cuentasccpet WHERE version = '".$maxVersion."') AND version = '".$maxVersion."' LIMIT 1";
        $respMaxLargo = mysqli_query($linkbd, $sqlrMaxLargo);
        $rowMaxLargo = mysqli_fetch_row($respMaxLargo);
        $cuentaMaxLargo = $rowMaxLargo[0];

        //var_dump($_POST["basesApp"]);

        for($xy = 0; $xy < count($_POST["basesApp"]); $xy++){

            $partConectBase = explode(",", $_POST["basesApp"][$xy][0]);
            $base = $partConectBase[0];
            $usuarioBase = $partConectBase[1];
            $unidad = $partConectBase[2];
            $nombre = $partConectBase[3];
            $tipo = $partConectBase[4];
            $id = $partConectBase[5];

            $linkmulti = conectar_Multi($base, $usuarioBase);
	        $linkmulti -> set_charset("utf8");
            $x = 0;
            $tamFuentes = isset($_POST["fuentes"]);
            do{
                $critFuente = $tamFuentes>0 ? "AND fuente LIKE '%".$_POST["fuentes"][$x]."%'" : '';
                $critFuenteInv = $tamFuentes>0 ? $_POST["fuentes"][$x] : '';

                //PRESUPUESTO INICIAL DE FUNCIONAMIENTO
                $sqlr = "SELECT cuenta, fuente, seccion_presupuestal, vigencia_gasto, medio_pago, valor FROM ccpetinicialgastosfun WHERE (vigencia = '$vigencia_ini' OR vigencia = '$vigencia_fin') $crit1 $crit2 $crit3 $critFuente $crit_vig_gasto ORDER BY cuenta ASC";
                $resp = mysqli_query($linkmulti, $sqlr);
                while($row = mysqli_fetch_row($resp)){
                    array_push($row, $unidad);
                    array_push($row, $nombre);
                    array_push($cuentasCcpetUsadas, $row);
                }

                //adicion funcionamiento
                $sqlrAd = "SELECT cuenta, fuente, seccion_presupuestal, codigo_vigenciag, medio_pago, SUM(valor) FROM ccpet_adiciones WHERE (vigencia = '$vigencia_ini' OR vigencia = '$vigencia_fin') $crit1 $crit2 $crit3_inv_cdp $critFuente $crit_vig_gasto AND ( SUBSTRING(cuenta, 1, 3) = '2.1' OR SUBSTRING(cuenta, 1, 3) = '2.2' OR SUBSTRING(cuenta, 1, 3) = '2.4') AND fecha BETWEEN '$fechaIni' AND '$fechaFin' GROUP BY cuenta, fuente, seccion_presupuestal, codigo_vigenciag, medio_pago ORDER BY cuenta ASC";
                $respAd = mysqli_query($linkmulti, $sqlrAd);
                while($rowAd = mysqli_fetch_row($respAd)){
                    array_push($rowAd, $unidad);
                    array_push($rowAd, $nombre);
                    array_push($cuentasCcpetUsadasAd, $rowAd);
                }

                //Reduccion funcionamiento
                $sqlrRed = "SELECT cuenta, fuente, seccion_presupuestal, codigo_vigenciag, medio_pago, SUM(valor) FROM ccpet_reducciones WHERE (vigencia = '$vigencia_ini' OR vigencia = '$vigencia_fin') $crit1 $crit2 $crit3_inv_cdp $critFuente $crit_vig_gasto AND ( SUBSTRING(cuenta, 1, 3) = '2.1' OR SUBSTRING(cuenta, 1, 3) = '2.2' OR SUBSTRING(cuenta, 1, 3) = '2.4')  GROUP BY cuenta, fuente, seccion_presupuestal, codigo_vigenciag, medio_pago ORDER BY cuenta ASC";
                $respRed = mysqli_query($linkmulti, $sqlrRed);
                while($rowRed = mysqli_fetch_row($respRed)){
                    array_push($rowRed, $unidad);
                    array_push($rowRed, $nombre);
                    array_push($cuentasCcpetUsadasRed, $rowRed);
                }

                //Traslado credito funcionamiento
                $sqlrCred = "SELECT cuenta, fuente, seccion_presupuestal, codigo_vigenciag, medio_pago, SUM(valor) FROM ccpet_traslados WHERE (vigencia = '$vigencia_ini' OR vigencia = '$vigencia_fin') $crit1 $crit2 $crit3_inv_cdp $critFuente $crit_vig_gasto AND ( SUBSTRING(cuenta, 1, 3) = '2.1' OR SUBSTRING(cuenta, 1, 3) = '2.2' OR SUBSTRING(cuenta, 1, 3) = '2.4') AND fecha BETWEEN '$fechaIni' AND '$fechaFin' AND tipo='C' GROUP BY cuenta, fuente, seccion_presupuestal, codigo_vigenciag, medio_pago ORDER BY cuenta ASC";
                $respCred = mysqli_query($linkmulti, $sqlrCred);
                while($rowCred = mysqli_fetch_row($respCred)){
                    array_push($rowCred, $unidad);
                    array_push($rowCred, $nombre);
                    array_push($cuentasCcpetUsadasCred, $rowCred);
                }

                //Traslado contracredito funcionamiento
                $sqlrContraCred = "SELECT cuenta, fuente, seccion_presupuestal, codigo_vigenciag, medio_pago, SUM(valor) FROM ccpet_traslados WHERE (vigencia = '$vigencia_ini' OR vigencia = '$vigencia_fin') $crit1 $crit2 $crit3_inv_cdp $critFuente $crit_vig_gasto AND ( SUBSTRING(cuenta, 1, 3) = '2.1' OR SUBSTRING(cuenta, 1, 3) = '2.2' OR SUBSTRING(cuenta, 1, 3) = '2.4') AND fecha BETWEEN '$fechaIni' AND '$fechaFin' AND tipo='R' GROUP BY cuenta, fuente, seccion_presupuestal, codigo_vigenciag, medio_pago ORDER BY cuenta ASC";
                $respContraCred = mysqli_query($linkmulti, $sqlrContraCred);
                while($rowContraCred = mysqli_fetch_row($respContraCred)){
                    array_push($rowContraCred, $unidad);
                    array_push($rowContraCred, $nombre);
                    array_push($cuentasCcpetUsadasContraCred, $rowContraCred);
                }

                //adicion inversion
                if($tipoGasto != '-1'){
                    if($tipoGasto == '2.3'){
                        $sqlrProy = "SELECT P2.*, P1.codigo, P1.nombre, P3.id_fuente, P1.idunidadej, P3.vigencia_gasto, P3.medio_pago FROM ccpproyectospresupuesto AS P1, ccpproyectospresupuesto_productos AS P2, ccpproyectospresupuesto_presupuesto AS P3 WHERE (P1.vigencia = '$vigencia_ini' OR P1.vigencia = '$vigencia_fin') AND P1.id = P2.codproyecto AND P1.id=P3.codproyecto AND  P2.indicador = P3.indicador_producto AND P3.id_fuente LIKE '%$critFuenteInv%' $crit1_inv $crit2_inv $crit3_inv UNION SELECT P4.*, P5.codigo, P5.nombre, P6.fuente, P5.idunidadej, P6.codigo_vigenciag, P6.medio_pago FROM ccpproyectospresupuesto AS P5, ccpproyectospresupuesto_productos AS P4, ccpet_adiciones AS P6 WHERE (P5.vigencia = '$vigencia_ini' OR P5.vigencia = '$vigencia_fin') AND P5.id = P4.codproyecto AND P5.codigo=P6.bpim AND P4.indicador = P6.programatico AND P6.fecha BETWEEN '$fechaIni' AND '$fechaFin' AND P6.fuente LIKE '%$critFuenteInv%' AND P5.idunidadej = P6.seccion_presupuestal $crit1_inv_P5 $crit2_inv_P6 $crit3_inv_P6 $crit_vig_gasto_ad_inv UNION SELECT P7.*, P8.codigo, P8.nombre, P9.fuente, P8.idunidadej, P9.codigo_vigenciag, P9.medio_pago FROM ccpproyectospresupuesto AS P8, ccpproyectospresupuesto_productos AS P7, ccpet_traslados AS P9 WHERE (P8.vigencia = '$vigencia_ini' OR P8.vigencia = '$vigencia_fin') AND P8.id = P7.codproyecto AND P8.codigo=P9.bpim AND P7.indicador = P9.programatico AND P9.fuente LIKE '%$critFuenteInv%' $crit1_inv_P8 $crit2_inv_P9 $crit3_inv_P9 $crit_vig_gasto_tras_inv ORDER BY indicador ASC";

                        $respProy = mysqli_query($linkmulti, $sqlrProy);
                        while($rowProy = mysqli_fetch_row($respProy)){
                            array_push($rowProy, $unidad);
                            array_push($rowProy, $nombre);
                            array_push($cuentasCcpetUsadasProy, $rowProy);
                        }

                        //Inicial inversion
                        $sqlrIniInv = "SELECT CP.idunidadej, CP.codigo, CPD.id_fuente, CPD.medio_pago, CPD.indicador_producto, CPD.valorcsf, CPD.valorssf, CPD.vigencia_gasto FROM ccpproyectospresupuesto AS CP, ccpproyectospresupuesto_presupuesto AS CPD WHERE (CP.vigencia = '$vigencia_ini' OR CP.vigencia = '$vigencia_fin') AND CP.id=CPD.codproyecto AND CPD.id_fuente LIKE '%$critFuenteInv%' $crit1_inv_ini $crit2_inv_ini $crit3_inv_ini";
                        $respIniInv = mysqli_query($linkmulti, $sqlrIniInv);
                        while($rowIniInv = mysqli_fetch_row($respIniInv)){
                            array_push($rowIniInv, $unidad);
                            array_push($rowIniInv, $nombre);
                            array_push($cuentasCcpetUsadasIniInv, $rowIniInv);
                        }
                    }

                }else{
                    $sqlrProy = "SELECT P2.*, P1.codigo, P1.nombre, P3.id_fuente, P1.idunidadej, P3.vigencia_gasto, P3.medio_pago FROM ccpproyectospresupuesto AS P1, ccpproyectospresupuesto_productos AS P2, ccpproyectospresupuesto_presupuesto AS P3 WHERE (P1.vigencia = '$vigencia_ini' OR P1.vigencia = '$vigencia_fin') AND P1.id = P2.codproyecto AND P1.id=P3.codproyecto AND  P2.indicador = P3.indicador_producto AND P3.id_fuente LIKE '%$critFuenteInv%' $crit1_inv $crit2_inv $crit3_inv UNION SELECT P4.*, P5.codigo, P5.nombre, P6.fuente, P5.idunidadej, P6.codigo_vigenciag, P6.medio_pago FROM ccpproyectospresupuesto AS P5, ccpproyectospresupuesto_productos AS P4, ccpet_adiciones AS P6 WHERE (P5.vigencia = '$vigencia_ini' OR P5.vigencia = '$vigencia_fin') AND P5.id = P4.codproyecto AND P5.codigo=P6.bpim AND P4.indicador = P6.programatico AND P6.fecha BETWEEN '$fechaIni' AND '$fechaFin' AND P6.fuente LIKE '%$critFuenteInv%' AND P5.idunidadej = P6.seccion_presupuestal $crit1_inv_P5 $crit2_inv_P6 $crit3_inv_P6 UNION SELECT P7.*, P8.codigo, P8.nombre, P9.fuente, P8.idunidadej, P9.codigo_vigenciag, P9.medio_pago FROM ccpproyectospresupuesto AS P8, ccpproyectospresupuesto_productos AS P7, ccpet_traslados AS P9 WHERE (P8.vigencia = '$vigencia_ini' OR P8.vigencia = '$vigencia_fin') AND P8.id = P7.codproyecto AND P8.codigo=P9.bpim AND P7.indicador = P9.programatico AND P9.fuente LIKE '%$critFuenteInv%' $crit1_inv_P8 $crit2_inv_P9 $crit3_inv_P9 ORDER BY indicador ASC";
                    $respProy = mysqli_query($linkmulti, $sqlrProy);
                    while($rowProy = mysqli_fetch_row($respProy)){
                        array_push($rowProy, $unidad);
                        array_push($rowProy, $nombre);
                        array_push($cuentasCcpetUsadasProy, $rowProy);
                    }

                    //Inicial inversion
                    $sqlrIniInv = "SELECT CP.idunidadej, CP.codigo, CPD.id_fuente, CPD.medio_pago, CPD.indicador_producto, CPD.valorcsf, CPD.valorssf, CPD.vigencia_gasto FROM ccpproyectospresupuesto AS CP, ccpproyectospresupuesto_presupuesto AS CPD WHERE (CP.vigencia = '$vigencia_ini' OR CP.vigencia = '$vigencia_fin') AND CP.id=CPD.codproyecto AND CPD.id_fuente LIKE '%$critFuenteInv%' $crit1_inv_ini $crit2_inv_ini $crit3_inv_ini";
                    $respIniInv = mysqli_query($linkmulti, $sqlrIniInv);
                    while($rowIniInv = mysqli_fetch_row($respIniInv)){
                        array_push($rowIniInv, $unidad);
                        array_push($rowIniInv, $nombre);
                        array_push($cuentasCcpetUsadasIniInv, $rowIniInv);
                    }
                }


                //Adicional inversion
                $sqlrAdInv = "SELECT CA.seccion_presupuestal, CA.bpim, CA.fuente, CA.medio_pago, CA.programatico, SUM(CA.valor), CA.codigo_vigenciag FROM ccpet_adiciones AS CA WHERE (CA.vigencia = '$vigencia_ini' OR CA.vigencia = '$vigencia_fin') $crit1_Ad $crit2_Ad $crit3_Ad $crit_vig_gasto_ad_inv_ca AND CA.fecha BETWEEN '$fechaIni' AND '$fechaFin' AND CA.fuente LIKE '%$critFuenteInv%' GROUP BY CA.seccion_presupuestal, CA.bpim, CA.fuente, CA.medio_pago, CA.programatico, CA.codigo_vigenciag";
                //$sqlrIniInv = "SELECT CP.idunidadej, CP.codigo, CPD.id_fuente, CPD.medio_pago, CPD.indicador_producto, CPD.valorcsf, CPD.valorssf, CPD.vigencia_gasto FROM ccpproyectospresupuesto AS CP, ccpproyectospresupuesto_presupuesto AS CPD WHERE CP.vigencia = '$vigencia' AND CP.id=CPD.codproyecto";
                $respAdInv = mysqli_query($linkmulti, $sqlrAdInv);
                while($rowAdInv = mysqli_fetch_row($respAdInv)){
                    array_push($rowAdInv, $unidad);
                    array_push($rowAdInv, $nombre);
                    array_push($cuentasCcpetUsadasAdInv, $rowAdInv);
                }

                //Reduccion inversion
                $sqlrRedInv = "SELECT CA.seccion_presupuestal, CA.bpim, CA.fuente, CA.medio_pago, CA.programatico, SUM(CA.valor), CA.codigo_vigenciag FROM ccpet_reducciones AS CA WHERE (CA.vigencia = '$vigencia_ini' OR CA.vigencia = '$vigencia_fin') AND CA.fuente LIKE '%$critFuenteInv%' $crit1_Ad $crit2_Ad $crit3_Ad $crit_vig_gasto_ad_inv_ca GROUP BY CA.seccion_presupuestal, CA.bpim, CA.fuente, CA.medio_pago, CA.programatico, CA.codigo_vigenciag";
                //$sqlrIniInv = "SELECT CP.idunidadej, CP.codigo, CPD.id_fuente, CPD.medio_pago, CPD.indicador_producto, CPD.valorcsf, CPD.valorssf, CPD.vigencia_gasto FROM ccpproyectospresupuesto AS CP, ccpproyectospresupuesto_presupuesto AS CPD WHERE CP.vigencia = '$vigencia' AND CP.id=CPD.codproyecto";
                $respRedInv = mysqli_query($linkmulti, $sqlrRedInv);
                while($rowRedInv = mysqli_fetch_row($respRedInv)){
                    array_push($rowRedInv, $unidad);
                    array_push($rowRedInv, $nombre);
                    array_push($cuentasCcpetUsadasRedInv, $rowRedInv);
                }

                //Traslados credito inversion
                $sqlrTrasladoInv = "SELECT CA.seccion_presupuestal, CA.bpim, CA.fuente, CA.medio_pago, CA.programatico, SUM(CA.valor), CA.codigo_vigenciag FROM ccpet_traslados AS CA WHERE (CA.vigencia = '$vigencia_ini' OR CA.vigencia = '$vigencia_fin') AND CA.bpim!='' AND CA.fecha BETWEEN '$fechaIni' AND '$fechaFin' AND CA.tipo = 'C' AND CA.fuente LIKE '%$critFuenteInv%' $crit1_Ad $crit2_Ad $crit3_Ad $crit_vig_gasto_ad_inv_ca GROUP BY CA.seccion_presupuestal, CA.bpim, CA.fuente, CA.medio_pago, CA.programatico, CA.codigo_vigenciag";
                //$sqlrIniInv = "SELECT CP.idunidadej, CP.codigo, CPD.id_fuente, CPD.medio_pago, CPD.indicador_producto, CPD.valorcsf, CPD.valorssf, CPD.vigencia_gasto FROM ccpproyectospresupuesto AS CP, ccpproyectospresupuesto_presupuesto AS CPD WHERE CP.vigencia = '$vigencia' AND CP.id=CPD.codproyecto";
                $respTrasladoInv = mysqli_query($linkmulti, $sqlrTrasladoInv);
                while($rowTrasladoInv = mysqli_fetch_row($respTrasladoInv)){
                    array_push($rowTrasladoInv, $unidad);
                    array_push($rowTrasladoInv, $nombre);
                    array_push($cuentasCcpetUsadasTrasladoCredInv, $rowTrasladoInv);
                }

                //Traslados contra credito inversion
                $sqlrTrasladoContraInv = "SELECT CA.seccion_presupuestal, CA.bpim, CA.fuente, CA.medio_pago, CA.programatico, SUM(CA.valor), CA.codigo_vigenciag FROM ccpet_traslados AS CA WHERE (CA.vigencia = '$vigencia_ini' OR CA.vigencia = '$vigencia_fin') AND CA.bpim!='' AND CA.fecha BETWEEN '$fechaIni' AND '$fechaFin' AND CA.tipo = 'R' AND CA.fuente LIKE '%$critFuenteInv%' $crit1_Ad $crit2_Ad $crit3_Ad $crit_vig_gasto_ad_inv_ca GROUP BY CA.seccion_presupuestal, CA.bpim, CA.fuente, CA.medio_pago, CA.programatico, CA.codigo_vigenciag";
                //$sqlrIniInv = "SELECT CP.idunidadej, CP.codigo, CPD.id_fuente, CPD.medio_pago, CPD.indicador_producto, CPD.valorcsf, CPD.valorssf, CPD.vigencia_gasto FROM ccpproyectospresupuesto AS CP, ccpproyectospresupuesto_presupuesto AS CPD WHERE CP.vigencia = '$vigencia' AND CP.id=CPD.codproyecto";
                $respTrasladoContraInv = mysqli_query($linkmulti, $sqlrTrasladoContraInv);
                while($rowTrasladoContraInv = mysqli_fetch_row($respTrasladoContraInv)){
                    array_push($rowTrasladoContraInv, $unidad);
                    array_push($rowTrasladoContraInv, $nombre);
                    array_push($cuentasCcpetUsadasTrasladoContraCredInv, $rowTrasladoContraInv);
                }

                //Cdp funcionamiento, sercicio a la deuda AND fecha BETWEEN '$fechaIni' AND '$fechaFin'
                $sqlrCdp = "SELECT DISTINCT DET.cuenta, DET.fuente, DET.seccion_presupuestal, DET.codigo_vigenciag, DET.medio_pago, SUM(DET.valor), CAB.consvigencia, CAB.vigencia FROM ccpetcdp_detalle AS DET, ccpetcdp CAB WHERE CAB.consvigencia = DET.consvigencia AND CAB.vigencia = DET.vigencia AND CAB.fecha BETWEEN '$fechaIni' AND '$fechaFin' AND (DET.vigencia = '$vigencia_ini' OR DET.vigencia = '$vigencia_fin') AND ( SUBSTRING(DET.cuenta, 1, 3) = '2.1' OR SUBSTRING(DET.cuenta, 1, 3) = '2.2' OR SUBSTRING(DET.cuenta, 1, 3) = '2.4') AND CAB.tipo_mov = '201' AND DET.tipo_mov = '201' AND DET.fuente LIKE '%$critFuenteInv%' $crit1Det $crit2Det $crit3_inv_cdpDet $crit_vig_gasto_det GROUP BY DET.cuenta, DET.fuente, DET.seccion_presupuestal, DET.codigo_vigenciag, DET.medio_pago ORDER BY DET.cuenta ASC ";
                $respCdp = mysqli_query($linkmulti, $sqlrCdp);
                while($rowCdp = mysqli_fetch_row($respCdp)){

                    $sqlrCdpRev = "SELECT SUM(DET.valor) FROM ccpetcdp_detalle AS DET, ccpetcdp CAB WHERE CAB.consvigencia = DET.consvigencia AND CAB.vigencia = DET.vigencia AND CAB.fecha BETWEEN '$fechaIni' AND '$fechaFin' AND CAB.vigencia = '$rowCdp[7]' AND (DET.vigencia = '$vigencia_ini' OR DET.vigencia = '$vigencia_fin') AND DET.cuenta = '$rowCdp[0]' AND CAB.tipo_mov LIKE '4%' AND DET.tipo_mov LIKE '4%' AND DET.fuente = '$rowCdp[1]' AND DET.seccion_presupuestal = '$rowCdp[2]' AND DET.codigo_vigenciag = '$rowCdp[3]' AND DET.medio_pago = '$rowCdp[4]' $crit1Det $crit2Det $crit3_inv_cdpDet GROUP BY DET.cuenta, DET.fuente, DET.seccion_presupuestal, DET.codigo_vigenciag, DET.medio_pago ";
                    $respCdpRev = mysqli_query($linkmulti, $sqlrCdpRev);
                    $rowCdpRev = mysqli_fetch_row($respCdpRev);

                    $cdps = [];
                    $totalCdp = 0;

                    $totalCdp = round($rowCdp[5], 2) - round($rowCdpRev[0], 2);
                    array_push($cdps, $rowCdp[0]);
                    array_push($cdps, $rowCdp[1]);
                    array_push($cdps, $rowCdp[2]);
                    array_push($cdps, $rowCdp[3]);
                    array_push($cdps, $rowCdp[4]);
                    array_push($cdps, round($totalCdp, 2));
                    array_push($cdps, $unidad);
                    array_push($cdps, $nombre);

                    array_push($cuentasCcpetUsadasCdp, $cdps);
                }

                //Cdp Inversion
                $sqlrCdpInv = "SELECT DISTINCT DET.seccion_presupuestal, DET.bpim, DET.fuente, DET.medio_pago, DET.indicador_producto, SUM(DET.valor), DET.codigo_vigenciag, CAB.consvigencia, CAB.vigencia FROM ccpetcdp_detalle AS DET, ccpetcdp AS CAB WHERE CAB.consvigencia = DET.consvigencia AND CAB.vigencia = DET.vigencia AND CAB.fecha BETWEEN '$fechaIni' AND '$fechaFin' AND (DET.vigencia = '$vigencia_ini' OR DET.vigencia = '$vigencia_fin') AND SUBSTRING(DET.cuenta, 1, 3) = '2.3' AND CAB.tipo_mov = '201' AND DET.tipo_mov = '201' AND DET.fuente LIKE '%$critFuenteInv%' $crit1Det $crit2Det $crit3_inv_cdpDet $crit_vig_gasto_det GROUP BY DET.seccion_presupuestal, DET.bpim, DET.fuente, DET.medio_pago, DET.indicador_producto, DET.codigo_vigenciag, CAB.consvigencia, CAB.vigencia ";
                $respCdpInv = mysqli_query($linkmulti, $sqlrCdpInv);
                while($rowCdpInv = mysqli_fetch_row($respCdpInv)){

                    $sqlrCdpInvRev = "SELECT SUM(DET.valor) FROM ccpetcdp_detalle AS DET, ccpetcdp AS CAB WHERE CAB.consvigencia = DET.consvigencia AND CAB.vigencia = DET.vigencia AND CAB.fecha BETWEEN '$fechaIni' AND '$fechaFin' AND CAB.consvigencia = '$rowCdpInv[7]'  AND CAB.vigencia = '$rowCdpInv[8]' AND (DET.vigencia = '$vigencia_ini' OR DET.vigencia = '$vigencia_fin') AND SUBSTRING(DET.cuenta, 1, 3) = '2.3' AND CAB.tipo_mov LIKE '4%' AND DET.tipo_mov LIKE '4%' AND DET.seccion_presupuestal = '$rowCdpInv[0]' AND DET.bpim = '$rowCdpInv[1]' AND DET.fuente = '$rowCdpInv[2]' AND DET.medio_pago = '$rowCdpInv[3]' AND DET.indicador_producto = '$rowCdpInv[4]' AND DET.codigo_vigenciag = '$rowCdpInv[6]' $crit1Det $crit2Det $crit3_inv_cdpDet GROUP BY DET.seccion_presupuestal, DET.bpim, DET.fuente, DET.medio_pago, DET.indicador_producto, DET.codigo_vigenciag";
                    $respCdpInvRev = mysqli_query($linkmulti, $sqlrCdpInvRev);
                    $rowCdpInvRev = mysqli_fetch_row($respCdpInvRev);

                    $cdpsInv = [];
                    $totalCdpInv = 0;

                    $totalCdpInv = round($rowCdpInv[5], 2) - round($rowCdpInvRev[0], 2);
                    array_push($cdpsInv, $rowCdpInv[0]);
                    array_push($cdpsInv, $rowCdpInv[1]);
                    array_push($cdpsInv, $rowCdpInv[2]);
                    array_push($cdpsInv, $rowCdpInv[3]);
                    array_push($cdpsInv, $rowCdpInv[4]);
                    array_push($cdpsInv, round($totalCdpInv, 2));
                    array_push($cdpsInv, $rowCdpInv[6]);
                    array_push($cdpsInv, $unidad);
                    array_push($cdpsInv, $nombre);

                    array_push($cuentasCcpetUsadasCdpInv, $cdpsInv);
                }

                //RPS DE INVERSION
                $sqlrRpInv = "SELECT DET.seccion_presupuestal, DET.bpim, DET.fuente, DET.medio_pago, DET.indicador_producto, SUM(DET.valor), DET.codigo_vigenciag, CAB.consvigencia, CAB.vigencia FROM ccpetrp_detalle AS DET, ccpetrp AS CAB WHERE CAB.consvigencia = DET.consvigencia AND CAB.vigencia = DET.vigencia AND CAB.fecha BETWEEN '$fechaIni' AND '$fechaFin' AND (DET.vigencia = '$vigencia_ini' OR DET.vigencia = '$vigencia_fin') AND SUBSTRING(DET.cuenta, 1, 3) = '2.3' AND CAB.tipo_mov = '201' AND DET.tipo_mov = '201' AND CAB.tipo_mov = DET.tipo_mov AND DET.fuente LIKE '%$critFuenteInv%' $crit1Det $crit2Det $crit3_inv_cdpDet $crit_vig_gasto_det GROUP BY DET.seccion_presupuestal, DET.bpim, DET.fuente, DET.medio_pago, DET.indicador_producto, DET.codigo_vigenciag, DET.tipo_mov, CAB.consvigencia, CAB.vigencia ";
                $respRpInv = mysqli_query($linkmulti, $sqlrRpInv);
                while($rowRpInv = mysqli_fetch_row($respRpInv)){

                    $sqlrRpInvRev = "SELECT SUM(DET.valor) FROM ccpetrp_detalle AS DET, ccpetrp AS CAB WHERE CAB.consvigencia = DET.consvigencia AND CAB.vigencia = DET.vigencia AND CAB.fecha BETWEEN '$fechaIni' AND '$fechaFin' AND CAB.consvigencia = '$rowRpInv[7]' AND CAB.vigencia = '$rowRpInv[8]' AND (DET.vigencia = '$vigencia_ini' OR DET.vigencia = '$vigencia_fin') AND SUBSTRING(DET.cuenta, 1, 3) = '2.3' AND CAB.tipo_mov LIKE '4%' AND DET.tipo_mov LIKE '4%' AND DET.seccion_presupuestal = '$rowRpInv[0]' AND DET.bpim = '$rowRpInv[1]' AND DET.fuente = '$rowRpInv[2]' AND DET.medio_pago = '$rowRpInv[3]' AND  DET.indicador_producto = '$rowRpInv[4]' AND DET.codigo_vigenciag = '$rowRpInv[6]' $crit1Det $crit2Det $crit3_inv_cdpDet GROUP BY DET.seccion_presupuestal, DET.bpim, DET.fuente, DET.medio_pago, DET.indicador_producto, DET.codigo_vigenciag";
                    /* if($rowRpInv[7] == '429'){
                        echo $sqlrRpInvRev."separador";
                    } */
                    $respRpInvRev = mysqli_query($linkmulti, $sqlrRpInvRev);
                    $rowRpInvRev = mysqli_fetch_row($respRpInvRev);

                    $rpsInv = [];
                    $totalRpInv = 0;
                    $totalRpInv = round($rowRpInv[5] - $rowRpInvRev[0], 2);

                    array_push($rpsInv, $rowRpInv[0]);
                    array_push($rpsInv, $rowRpInv[1]);
                    array_push($rpsInv, $rowRpInv[2]);
                    array_push($rpsInv, $rowRpInv[3]);
                    array_push($rpsInv, $rowRpInv[4]);
                    array_push($rpsInv, $totalRpInv);
                    array_push($rpsInv, $rowRpInv[6]);
                    array_push($rpsInv, $unidad);
                    array_push($rpsInv, $nombre);

                    array_push($cuentasCcpetUsadasRpInv, $rpsInv);
                }

                //rp funcionamiento
                $sqlrRp = "SELECT DISTINCT DET.cuenta, DET.fuente, DET.seccion_presupuestal, DET.codigo_vigenciag, DET.medio_pago, SUM(DET.valor), CAB.consvigencia, CAB.vigencia FROM ccpetrp_detalle AS DET, ccpetrp AS CAB WHERE CAB.consvigencia = DET.consvigencia AND CAB.vigencia = DET.vigencia AND CAB.fecha BETWEEN '$fechaIni' AND '$fechaFin' AND (DET.vigencia = '$vigencia_ini' OR DET.vigencia = '$vigencia_fin') AND ( SUBSTRING(DET.cuenta, 1, 3) = '2.1' OR SUBSTRING(DET.cuenta, 1, 3) = '2.2' OR SUBSTRING(DET.cuenta, 1, 3) = '2.4') AND CAB.tipo_mov = '201' AND DET.tipo_mov = '201' AND DET.fuente LIKE '%$critFuenteInv%' $crit1Det $crit2Det $crit3_inv_cdpDet $crit_vig_gasto_det GROUP BY DET.cuenta, DET.fuente, DET.seccion_presupuestal, DET.codigo_vigenciag, DET.medio_pago ORDER BY DET.cuenta ASC ";
                $respRp = mysqli_query($linkmulti, $sqlrRp);
                while($rowRp = mysqli_fetch_row($respRp)){

                    $sqlrRpRev = "SELECT SUM(DET.valor) FROM ccpetrp_detalle AS DET, ccpetrp AS CAB WHERE CAB.consvigencia = DET.consvigencia AND CAB.vigencia = DET.vigencia AND CAB.fecha BETWEEN '$fechaIni' AND '$fechaFin' AND CAB.vigencia = '$rowRp[7]' AND (DET.vigencia = '$vigencia_ini' OR DET.vigencia = '$vigencia_fin') AND DET.cuenta = '$rowRp[0]' AND CAB.tipo_mov LIKE '4%' AND DET.tipo_mov LIKE '4%' AND DET.fuente = '$rowRp[1]' AND DET.seccion_presupuestal = '$rowRp[2]' AND DET.codigo_vigenciag = '$rowRp[3]' AND DET.medio_pago = '$rowRp[4]' $crit1Det $crit2Det $crit3_inv_cdpDet GROUP BY DET.cuenta, DET.fuente, DET.seccion_presupuestal, DET.codigo_vigenciag, DET.medio_pago";
                    $respRpRev = mysqli_query($linkmulti, $sqlrRpRev);
                    $rowRpRev = mysqli_fetch_row($respRpRev);

                    $rps = [];
                    $totalRp = round($rowRp[5] - $rowRpRev[0], 2);

                    array_push($rps, $rowRp[0]);
                    array_push($rps, $rowRp[1]);
                    array_push($rps, $rowRp[2]);
                    array_push($rps, $rowRp[3]);
                    array_push($rps, $rowRp[4]);
                    array_push($rps, $totalRp);
                    array_push($rps, $unidad);
                    array_push($rps, $nombre);

                    array_push($cuentasCcpetUsadasRp, $rps);
                }

                //cxp funcionamiento
                $sqlrCxp = "SELECT DISTINCT DET.cuentap, DET.fuente, DET.seccion_presupuestal, DET.codigo_vigenciag, DET.medio_pago, SUM(DET.valor) FROM tesoordenpago_det AS DET, tesoordenpago AS CAB WHERE CAB.id_orden = DET.id_orden AND CAB.fecha BETWEEN '$fechaIni' AND '$fechaFin' AND (DET.vigencia = '$vigencia_ini' OR DET.vigencia = '$vigencia_fin') AND ( SUBSTRING(DET.cuentap, 1, 3) = '2.1' OR SUBSTRING(DET.cuentap, 1, 3) = '2.2' OR SUBSTRING(DET.cuentap, 1, 3) = '2.4') AND CAB.estado != 'R' AND DET.fuente LIKE '%$critFuenteInv%' $crit1Det $crit2Det $crit3_inv_cdpDet $crit_vig_gasto_det_p GROUP BY DET.cuentap, DET.fuente, DET.seccion_presupuestal, DET.codigo_vigenciag, DET.medio_pago ORDER BY DET.cuentap ASC ";

                $respCxp = mysqli_query($linkmulti, $sqlrCxp);
                while($rowCxp = mysqli_fetch_row($respCxp)){
                    array_push($rowCxp, $unidad);
                    array_push($rowCxp, $nombre);
                    array_push($cuentasCcpetUsadasCxp, $rowCxp);
                }

                //cxp nomina
                $sqlrCxpN = "SELECT DISTINCT HD.cuenta, HD.fuente, HD.seccion_presupuestal, HD.vigencia_gasto, HD.medio_pago, SUM(HD.valor) FROM humnom_presupuestal AS HD, humnomina_aprobado AS HN WHERE HN.id_nom = HD.id_nom AND HN.fecha BETWEEN '$fechaIni' AND '$fechaFin' AND HN.estado!='N' AND HD.estado='P' AND ( SUBSTRING(HD.cuenta, 1, 3) = '2.1' OR SUBSTRING(HD.cuenta, 1, 3) = '2.2' OR SUBSTRING(HD.cuenta, 1, 3) = '2.4') AND HD.fuente LIKE '%$critFuenteInv%' $crit1_Nom $crit2_Nom $crit3_Nom $crit_vig_gasto_det_nom GROUP BY HD.cuenta, HD.fuente, HD.seccion_presupuestal, HD.vigencia_gasto, HD.medio_pago ORDER BY HD.cuenta ASC ";
                $respCxpN = mysqli_query($linkmulti, $sqlrCxpN);
                while($rowCxpN = mysqli_fetch_row($respCxpN)){
                    array_push($rowCxpN, $unidad);
                    array_push($rowCxpN, $nombre);
                    array_push($cuentasCcpetUsadasCxp, $rowCxpN);
                }

                //Cxp Inversion
                $sqlrCxpInv = "SELECT DISTINCT DET.seccion_presupuestal, DET.bpim, DET.fuente, DET.medio_pago, DET.indicador_producto, SUM(DET.valor), DET.codigo_vigenciag FROM tesoordenpago_det AS DET, tesoordenpago AS CAB WHERE CAB.id_orden = DET.id_orden AND CAB.fecha BETWEEN '$fechaIni' AND '$fechaFin' AND (DET.vigencia = '$vigencia_ini' OR DET.vigencia = '$vigencia_fin') AND SUBSTRING(DET.cuentap, 1, 3) = '2.3' AND CAB.estado != 'R' AND DET.fuente LIKE '%$critFuenteInv%' $crit1Det $crit2Det $crit3_inv_cdpDet $crit_vig_gasto_det_p GROUP BY DET.seccion_presupuestal, DET.bpim, DET.fuente, DET.medio_pago, DET.indicador_producto, DET.codigo_vigenciag ";
                $respCxpInv = mysqli_query($linkmulti, $sqlrCxpInv);
                while($rowCxpInv = mysqli_fetch_row($respCxpInv)){
                    array_push($rowCxpInv, $unidad);
                    array_push($rowCxpInv, $nombre);
                    array_push($cuentasCcpetUsadasCxpInv, $rowCxpInv);
                }

                //Cxp Inversion Nomina
                $sqlrCxpInvN = "SELECT DISTINCT HD.seccion_presupuestal, HD.bpin, HD.fuente, HD.medio_pago, HD.indicador, SUM(HD.valor), HD.vigencia_gasto FROM humnom_presupuestal AS HD, humnomina_aprobado AS HN WHERE HN.id_nom = HD.id_nom AND HN.fecha BETWEEN '$fechaIni' AND '$fechaFin' AND HD.estado='P' AND SUBSTRING(HD.cuenta, 1, 3) = '2.3' AND HD.fuente LIKE '%$critFuenteInv%' $crit1_Nom $crit2_Nom $crit3_Nom $crit_vig_gasto_det_nom GROUP BY HD.seccion_presupuestal, HD.bpin, HD.fuente, HD.medio_pago, HD.indicador, HD.vigencia_gasto";
                $respCxpInvN = mysqli_query($linkmulti, $sqlrCxpInvN);
                while($rowCxpInvN = mysqli_fetch_row($respCxpInvN)){
                    array_push($rowCxpInvN, $unidad);
                    array_push($rowCxpInvN, $nombre);
                    array_push($cuentasCcpetUsadasCxpInv, $rowCxpInvN);
                }

                //egreso funcionamiento
                $sqlrEgreso = "SELECT DISTINCT TD.cuentap, TD.fuente, TD.seccion_presupuestal, TD.codigo_vigenciag, TD.medio_pago, SUM(TD.valor) FROM tesoordenpago_det AS TD, tesoegresos AS TE WHERE (TD.vigencia = '$vigencia_ini' OR TD.vigencia = '$vigencia_fin') AND TE.fecha BETWEEN '$fechaIni' AND '$fechaFin' AND TD.id_orden = TE.id_orden AND ( SUBSTRING(TD.cuentap, 1, 3) = '2.1' OR SUBSTRING(TD.cuentap, 1, 3) = '2.2' OR SUBSTRING(TD.cuentap, 1, 3) = '2.4') AND TE.estado != 'N' AND TE.estado != 'R' AND TD.fuente LIKE '%$critFuenteInv%' $crit1_egresp_Nom $crit2_egreso_Nom $crit3_egreso_Nom_td $crit_vig_gasto_det_td GROUP BY TD.cuentap, TD.fuente, TD.seccion_presupuestal, TD.codigo_vigenciag, TD.medio_pago ORDER BY TD.cuentap ASC ";
                $respEgreso = mysqli_query($linkmulti, $sqlrEgreso);
                while($rowEgreso = mysqli_fetch_row($respEgreso)){
                    array_push($rowEgreso, $unidad);
                    array_push($rowEgreso, $nombre);
                    array_push($cuentasCcpetUsadasEgreso, $rowEgreso);
                }

                //egreso nomina funcionamiento
                $sqlrEgreso = "SELECT DISTINCT TD.cuentap, TD.fuente, TD.seccion_presupuestal, TD.vigencia_gasto, TD.medio_pago, SUM(TD.valordevengado) FROM tesoegresosnomina_det AS TD, tesoegresosnomina AS TE WHERE (TE.vigencia = '$vigencia_ini' OR TE.vigencia = '$vigencia_fin') AND TE.fecha BETWEEN '$fechaIni' AND '$fechaFin' AND TD.id_egreso = TE.id_egreso AND (SUBSTRING(TD.cuentap, 1, 3) = '2.1' OR SUBSTRING(TD.cuentap, 1, 3) = '2.2' OR SUBSTRING(TD.cuentap, 1, 3) = '2.4') AND TE.estado = 'S' AND NOT(TD.tipo='SE' OR TD.tipo='PE' OR TD.tipo='DS' OR TD.tipo='RE' OR TD.tipo='FS') AND TD.fuente LIKE '%$critFuenteInv%' $crit1_egresp_Nom $crit2_egreso_Nom $crit3_egreso_Nom $crit_vig_gasto_det_td GROUP BY TD.cuentap, TD.fuente, TD.seccion_presupuestal, TD.vigencia_gasto, TD.medio_pago ORDER BY TD.cuentap ASC ";
                $respEgreso = mysqli_query($linkmulti, $sqlrEgreso);
                while($rowEgreso = mysqli_fetch_row($respEgreso)){
                    array_push($rowEgreso, $unidad);
                    array_push($rowEgreso, $nombre);
                    array_push($cuentasCcpetUsadasEgreso, $rowEgreso);
                }

                //Egreso Inversion
                $sqlrEgresoInv = "SELECT DISTINCT TD.seccion_presupuestal, TD.bpim, TD.fuente, TD.medio_pago, TD.indicador_producto, SUM(TD.valor), TD.codigo_vigenciag FROM tesoordenpago_det AS TD, tesoegresos AS TE WHERE (TD.vigencia = '$vigencia_ini' OR TD.vigencia = '$vigencia_fin') AND TE.fecha BETWEEN '$fechaIni' AND '$fechaFin' AND TE.estado != 'N' AND TE.estado != 'R' AND TD.id_orden = TE.id_orden AND SUBSTRING(TD.cuentap, 1, 3) = '2.3' AND TD.fuente LIKE '%$critFuenteInv%' $crit1_egresp_Nom $crit2_egreso_Nom $crit3_egreso_Nom_td $crit_vig_gasto_det_td GROUP BY TD.seccion_presupuestal, TD.bpim, TD.fuente, TD.medio_pago, TD.indicador_producto, TD.codigo_vigenciag ";
                $respEgresoInv = mysqli_query($linkmulti, $sqlrEgresoInv);
                while($rowEgresoInv = mysqli_fetch_row($respEgresoInv)){
                    array_push($rowEgresoInv, $unidad);
                    array_push($rowEgresoInv, $nombre);
                    array_push($cuentasCcpetUsadasEgresoInv, $rowEgresoInv);
                }

                $sqlrEgresoInvN = "SELECT DISTINCT TD.seccion_presupuestal, TD.bpin, TD.fuente, TD.medio_pago, TD.indicador_producto, SUM(TD.valordevengado), TD.vigencia_gasto FROM tesoegresosnomina_det AS TD, tesoegresosnomina AS TE WHERE (TE.vigencia = '$vigencia_ini' OR TE.vigencia = '$vigencia_fin') AND TE.fecha BETWEEN '$fechaIni' AND '$fechaFin' AND TD.id_egreso = TE.id_egreso AND SUBSTRING(TD.cuentap, 1, 3) = '2.3' AND TE.estado = 'S' AND NOT(TD.tipo='SE' OR TD.tipo='PE' OR TD.tipo='DS' OR TD.tipo='RE' OR TD.tipo='FS') AND TD.fuente LIKE '%$critFuenteInv%' $crit1_egresp_Nom $crit2_egreso_Nom $crit3_egreso_Nom $crit_vig_gasto_det_td GROUP BY TD.seccion_presupuestal, TD.bpin, TD.fuente, TD.medio_pago, TD.indicador_producto,TD.vigencia_gasto";
                $respEgresoInvN = mysqli_query($linkmulti, $sqlrEgresoInvN);
                while($rowEgresoInvN = mysqli_fetch_row($respEgresoInvN)){
                    array_push($rowEgresoInvN, $unidad);
                    array_push($rowEgresoInvN, $nombre);
                    array_push($cuentasCcpetUsadasEgresoInv, $rowEgresoInvN);
                }

                $x++;
            }
            while($x < $tamFuentes);
        }

        $sqlr = "SELECT * FROM cuentasccpet WHERE version = $maxVersion";
        $arrCuentas = mysqli_fetch_all(mysqli_query($linkbd, $sqlr),MYSQLI_ASSOC);

        $sqlrSectores = "SELECT codigo, nombre FROM ccpetsectores WHERE version = (SELECT MAX(version) FROM ccpetsectores)";
        $arrSectores = mysqli_fetch_all(mysqli_query($linkbd, $sqlrSectores),MYSQLI_ASSOC);


        $sqlrProgramas = "SELECT codigo, nombre,codigo_subprograma,nombre_subprograma FROM ccpetprogramas WHERE version = (SELECT MAX(version) FROM ccpetprogramas)";
        $arrProgramas = mysqli_fetch_all(mysqli_query($linkbd, $sqlrProgramas),MYSQLI_ASSOC);


        $sqlrProductos = "SELECT cod_producto, producto FROM ccpetproductos WHERE version = (SELECT MAX(version) FROM ccpetproductos) GROUP BY cod_producto";
        $arrProductos = mysqli_fetch_all(mysqli_query($linkbd, $sqlrProductos),MYSQLI_ASSOC);

        $sqlrProgramatico = "SELECT cod_producto,codigo_indicador, indicador_producto FROM ccpetproductos WHERE version = (SELECT MAX(version) FROM ccpetproductos) GROUP BY codigo_indicador";
        $arrProgramatico = mysqli_fetch_all(mysqli_query($linkbd, $sqlrProgramatico),MYSQLI_ASSOC);

        $sqlrFuentes = "SELECT codigo_fuente, nombre FROM ccpet_fuentes_cuipo WHERE version = (SELECT MAX(version) FROM ccpet_fuentes_cuipo)";
        $arrFuentes = mysqli_fetch_all(mysqli_query($linkbd, $sqlrFuentes),MYSQLI_ASSOC);

        $sqlrUnidades = "SELECT * FROM redglobal WHERE cuipo = 'S' ORDER BY id ASC";
        $arrUnidades = mysqli_fetch_all(mysqli_query($linkbd, $sqlrUnidades),MYSQLI_ASSOC);

        $arrArbol = [];
        $arrCuentasPrincipales = array_values(array_filter($arrCuentas,function($e){return $e['tipo'] == "A";}));
        $arrCuentasSecundarias = array_values(array_filter($arrCuentas,function($e){return $e['tipo'] == "C";}));

        $arrColumCuentasPrincipales = array_column($arrCuentasPrincipales,'codigo');
        usort($arrColumCuentasPrincipales,'strnatcmp');
        if($_POST['unidad'] != -1){
            $strUnidad = $_POST['unidad'];
            $arrUnidades = array_values(array_filter($arrUnidades,function($e) use($strUnidad){return $e['id'] == $strUnidad;}));
        }
        foreach ($arrUnidades as $unidad) {
            $arbolInversion = [];
            $arrArbolUnidad = [];

            $totalFuncionamientoInicial = 0;
            $totalFuncionamientoAdicion = 0;
            $totalFuncionamientoReduccion = 0;
            $totalFuncionamientoCredito = 0;
            $totalFuncionamientoContraCredito = 0;
            $totalFuncionamientoDefinitivo = 0;
            $totalFuncionamientoDisponibilidad = 0;
            $totalFuncionamientoCompromisos = 0;
            $totalFuncionamientoObligacion =0;
            $totalFuncionamientoSaldo = 0;
            $totalFuncionamientoEgreso = 0;

            $totalInversionInicial = 0;
            $totalInversionAdicion = 0;
            $totalInversionReduccion = 0;
            $totalInversionCredito = 0;
            $totalInversionContraCredito = 0;
            $totalInversionDefinitivo = 0;
            $totalInversionDisponibilidad = 0;
            $totalInversionCompromisos = 0;
            $totalInversionObligacion =0;
            $totalInversionSaldo = 0;
            $totalInversionEgreso = 0;

            //Se construye árbol por unidad ejecutora

            //Se recorre las cuentas principales, las tipo A
            foreach ($arrColumCuentasPrincipales as $cuentaColumna) {
                //En este for encuentro todas las cuentas tipo C según la cuenta padre que esta tenga
                foreach ($arrCuentasPrincipales as $cuentaPrincipal) {
                    if($cuentaPrincipal['codigo'] == $cuentaColumna){
                        $arrCuentaCab = [
                            "id"=>$cuentaPrincipal['id'],
                            "codigo"=>$cuentaPrincipal['codigo'],
                            "nombre"=>$cuentaPrincipal['nombre'],
                            "padre"=>$cuentaPrincipal['padre'],
                            "nivel"=>$cuentaPrincipal['nivel'],
                            "tipo"=>$cuentaPrincipal['tipo']
                        ];
                        $arrCuentasDetalle = array_values(array_filter($arrCuentasSecundarias,function($e) use($cuentaPrincipal){
                            return $e['padre'] == $cuentaPrincipal['codigo'];
                        }));
                        //En este for asigno todas las cuentas usadas a la cuenta tipo C correspondiente
                        $totalCuentasDetalle = count($arrCuentasDetalle);
                        for ($i=0; $i < $totalCuentasDetalle ; $i++) {
                            $e = $arrCuentasDetalle[$i];
                            $arrInicial = array_values(array_filter($cuentasCcpetUsadas,function($cuenta) use($e,$unidad){return $cuenta[0] == $e['codigo'] && $cuenta[6] == $unidad['codigo'];}));
                            $arrAdicion = array_values(array_filter($cuentasCcpetUsadasAd,function($cuenta) use($e,$unidad){return $cuenta[0] == $e['codigo'] && $cuenta[6] == $unidad['codigo'];}));
                            $arrReduccion = array_values(array_filter($cuentasCcpetUsadasRed,function($cuenta) use($e,$unidad){return $cuenta[0] == $e['codigo'] && $cuenta[6] == $unidad['codigo'];}));
                            $arrCredito = array_values(array_filter($cuentasCcpetUsadasCred,function($cuenta) use($e,$unidad){return $cuenta[0] == $e['codigo'] && $cuenta[6] == $unidad['codigo'];}));
                            $arrContracredito = array_values(array_filter($cuentasCcpetUsadasContraCred,function($cuenta) use($e,$unidad){return $cuenta[0] == $e['codigo'] && $cuenta[6] == $unidad['codigo'];}));
                            $arrCdp = array_values(array_filter($cuentasCcpetUsadasCdp,function($cuenta) use($e,$unidad){return $cuenta[0] == $e['codigo'] && $cuenta[6] == $unidad['codigo'];}));
                            $arrRp = array_values(array_filter($cuentasCcpetUsadasRp,function($cuenta) use($e,$unidad){return $cuenta[0] == $e['codigo'] && $cuenta[6] == $unidad['codigo'];}));
                            $arrCxp = array_values(array_filter($cuentasCcpetUsadasCxp,function($cuenta) use($e,$unidad){return $cuenta[0] == $e['codigo'] && $cuenta[6] == $unidad['codigo'];}));
                            $arrEgreso = array_values(array_filter($cuentasCcpetUsadasEgreso,function($cuenta) use($e,$unidad){return $cuenta[0] == $e['codigo'] && $cuenta[6] == $unidad['codigo'];}));
                            $arrDataDet = [];
                            foreach ($arrInicial as $data) {$data['tipo'] = "inicial";array_push($arrDataDet,$data);}
                            foreach ($arrAdicion as $data) {$data['tipo'] = "adicion";array_push($arrDataDet,$data);}
                            foreach ($arrReduccion as $data) {$data['tipo'] = "reduccion";array_push($arrDataDet,$data);}
                            foreach ($arrCredito as $data) {$data['tipo'] = "credito";array_push($arrDataDet,$data);}
                            foreach ($arrContracredito as $data) {$data['tipo'] = "contracredito";array_push($arrDataDet,$data);}
                            foreach ($arrCdp as $data) {$data['tipo'] = "cdp";array_push($arrDataDet,$data);}
                            foreach ($arrRp as $data) {$data['tipo'] = "rp";array_push($arrDataDet,$data);}
                            foreach ($arrCxp as $data) {$data['tipo'] = "cxp";array_push($arrDataDet,$data);}
                            foreach ($arrEgreso as $data) {$data['tipo'] = "egreso";array_push($arrDataDet,$data);}
                            $arrCuentasDetalle[$i]['data']=$arrDataDet;
                        }
                        /**
                         * Al tener las cuentas usandas en su respectiva cuenta tipo C, que ahora es el detalle del árbol, lo que hago
                         * es recorrerlo para luego realizar las operaciones correspondientes, el cual son los resultados a mostrar en el informe.
                         */
                        $arbolDet = $arrCuentasDetalle;
                        if(!empty($arbolDet)){
                            $arbolDet = array_values(array_filter($arbolDet,function($e){return $e['tipo']==!empty($e['data']);}));
                            sort($arbolDet);
                            $arrCuentasDet = [];
                            foreach ($arbolDet as $data) {
                                $arbolDetData = $data['data'];
                                $totalArbolDet = count($arbolDetData);
                                for ($i=0; $i < $totalArbolDet; $i++) {
                                    if(!empty($arrCuentasDet)){
                                        $totalCuentasDet = count($arrCuentasDet);
                                        $flag = true;

                                        for ($j=0; $j < $totalCuentasDet; $j++) {
                                            if($arrCuentasDet[$j]['codigo_cuenta']==$arbolDetData[$i][0] && $arrCuentasDet[$j]['codigo_fuente']==$arbolDetData[$i][1] &&
                                            $arrCuentasDet[$j]['codigo_seccion']==$arbolDetData[$i][2] && $arrCuentasDet[$j]['codigo_vigencia']==$arbolDetData[$i][3] &&
                                            $arrCuentasDet[$j]['codigo_pago']==$arbolDetData[$i][4] && $arrCuentasDet[$j]['codigo_unidad']==$arbolDetData[$i][6] ){
                                                $flag = false;
                                                $totalInicial = $arbolDetData[$i]['tipo'] == "inicial" ? $arbolDetData[$i][5]+$arrCuentasDet[$j]['inicial'] : $arrCuentasDet[$j]['inicial'];
                                                $totalAdicion = $arbolDetData[$i]['tipo'] == "adicion" ? $arbolDetData[$i][5]+$arrCuentasDet[$j]['adicion'] : $arrCuentasDet[$j]['adicion'];
                                                $totalReduccion = $arbolDetData[$i]['tipo'] == "reduccion" ? $arbolDetData[$i][5]+$arrCuentasDet[$j]['reduccion'] : $arrCuentasDet[$j]['reduccion'];
                                                $totalCredito = $arbolDetData[$i]['tipo'] == "credito" ? $arbolDetData[$i][5]+$arrCuentasDet[$j]['credito'] : $arrCuentasDet[$j]['credito'];
                                                $totalContraCredito = $arbolDetData[$i]['tipo'] == "contracredito" ? $arbolDetData[$i][5]+$arrCuentasDet[$j]['contracredito'] : $arrCuentasDet[$j]['contracredito'];
                                                $totalCdp = $arbolDetData[$i]['tipo'] == "cdp" ? $arbolDetData[$i][5]+$arrCuentasDet[$j]['cdp'] : $arrCuentasDet[$j]['cdp'];
                                                $totalRp = $arbolDetData[$i]['tipo'] == "rp" ? $arbolDetData[$i][5]+$arrCuentasDet[$j]['rp'] : $arrCuentasDet[$j]['rp'];
                                                $totalCxp = $arbolDetData[$i]['tipo'] == "cxp" ? $arbolDetData[$i][5]+$arrCuentasDet[$j]['cxp'] : $arrCuentasDet[$j]['cxp'];
                                                $totalEgreso = $arbolDetData[$i]['tipo'] == "egreso" ? $arbolDetData[$i][5]+$arrCuentasDet[$j]['egreso'] : $arrCuentasDet[$j]['egreso'];

                                                $totalDefinitivo =  ($totalInicial+$totalAdicion-$totalReduccion) + ($totalCredito-$totalContraCredito);
                                                $saldo = $totalDefinitivo-$totalCdp;
                                                $arrCuentasDet[$j]['inicial'] = $totalInicial;
                                                $arrCuentasDet[$j]['adicion'] = $totalAdicion;
                                                $arrCuentasDet[$j]['reduccion'] = $totalReduccion;
                                                $arrCuentasDet[$j]['credito'] = $totalCredito;
                                                $arrCuentasDet[$j]['contracredito'] = $totalContraCredito;
                                                $arrCuentasDet[$j]['cdp'] = $totalCdp;
                                                $arrCuentasDet[$j]['rp'] = $totalRp;
                                                $arrCuentasDet[$j]['egreso'] =$totalEgreso;
                                                $arrCuentasDet[$j]['cxp'] = $totalCxp;
                                                $arrCuentasDet[$j]['definitivo'] = $totalDefinitivo;
                                                $arrCuentasDet[$j]['disponibilidad'] = $totalCdp;
                                                $arrCuentasDet[$j]['compromisos'] = $totalRp;
                                                $arrCuentasDet[$j]['obligacion'] = $totalCxp;
                                                $arrCuentasDet[$j]['saldo'] = $saldo;
                                                break;
                                            }

                                        }
                                        if($flag){
                                            $totalInicial = $arbolDetData[$i]['tipo'] == "inicial" ? $arbolDetData[$i][5] : 0;
                                            $totalAdicion = $arbolDetData[$i]['tipo'] == "adicion" ? $arbolDetData[$i][5] : 0;
                                            $totalReduccion = $arbolDetData[$i]['tipo'] == "reduccion" ? $arbolDetData[$i][5] : 0;
                                            $totalCredito = $arbolDetData[$i]['tipo'] == "credito" ? $arbolDetData[$i][5] : 0;
                                            $totalContraCredito = $arbolDetData[$i]['tipo'] == "contracredito" ? $arbolDetData[$i][5] : 0;
                                            $totalCdp = $arbolDetData[$i]['tipo'] == "cdp" ? $arbolDetData[$i][5] : 0;
                                            $totalRp = $arbolDetData[$i]['tipo'] == "rp" ? $arbolDetData[$i][5] : 0;
                                            $totalCxp = $arbolDetData[$i]['tipo'] == "cxp" ? $arbolDetData[$i][5] : 0;
                                            $totalEgreso = $arbolDetData[$i]['tipo'] == "egreso" ? $arbolDetData[$i][5] : 0;
                                            $totalDefinitivo =  ($totalInicial+$totalAdicion-$totalReduccion) + ($totalCredito-$totalContraCredito);
                                            $saldo = $totalDefinitivo-$totalCdp;
                                            $codigoFuente = $arbolDetData[$i][1];
                                            $nombreFuente = array_values(array_filter($arrFuentes,function($e) use($codigoFuente) {return $e['codigo_fuente'] == $codigoFuente;}))[0]['nombre'];
                                            array_push($arrCuentasDet,[
                                                'codigo_cuenta' => $arbolDetData[$i][0],
                                                'codigo_fuente' => $arbolDetData[$i][1],
                                                'codigo_seccion' => $arbolDetData[$i][2],
                                                'codigo_vigencia' => $arbolDetData[$i][3],
                                                'codigo_pago' => $arbolDetData[$i][4],
                                                'codigo_unidad' => $arbolDetData[$i][6],
                                                'nombre_unidad' => $arbolDetData[$i][7],
                                                'nombre_cuenta' => $data['nombre'],
                                                'nombre_fuente'=> $nombreFuente,
                                                'tipo' => "C",
                                                'inicial' => $totalInicial,
                                                'adicion' => $totalAdicion ,
                                                'reduccion' => $totalReduccion,
                                                'credito' => $totalCredito,
                                                'contracredito' => $totalContraCredito,
                                                'cdp' => $arbolDetData[$i]['tipo'] == "cdp" ? $arbolDetData[$i][5] : 0 ,
                                                'rp' => $arbolDetData[$i]['tipo'] == "rp" ? $arbolDetData[$i][5] : 0 ,
                                                'egreso' => $totalEgreso ,
                                                'cxp' => $arbolDetData[$i]['tipo'] == "cxp" ? $arbolDetData[$i][5] : 0 ,
                                                'definitivo'=>$totalDefinitivo,
                                                'disponibilidad'=>$totalCdp,
                                                'compromisos'=>$totalRp,
                                                'obligacion'=>$totalCxp,
                                                'saldo'=>$saldo,
                                                "nombre_tipo"=>"funcionamiento"
                                            ]);
                                        }
                                    }else{
                                        $totalInicial = $arbolDetData[$i]['tipo'] == "inicial" ? $arbolDetData[$i][5] : 0;
                                        $totalAdicion = $arbolDetData[$i]['tipo'] == "adicion" ? $arbolDetData[$i][5] : 0;
                                        $totalReduccion = $arbolDetData[$i]['tipo'] == "reduccion" ? $arbolDetData[$i][5] : 0;
                                        $totalCredito = $arbolDetData[$i]['tipo'] == "credito" ? $arbolDetData[$i][5] : 0;
                                        $totalContraCredito = $arbolDetData[$i]['tipo'] == "contracredito" ? $arbolDetData[$i][5] : 0;
                                        $totalCdp = $arbolDetData[$i]['tipo'] == "cdp" ? $arbolDetData[$i][5] : 0;
                                        $totalRp = $arbolDetData[$i]['tipo'] == "rp" ? $arbolDetData[$i][5] : 0;
                                        $totalCxp = $arbolDetData[$i]['tipo'] == "cxp" ? $arbolDetData[$i][5] : 0;
                                        $totalEgreso = $arbolDetData[$i]['tipo'] == "egreso" ? $arbolDetData[$i][5] : 0;
                                        $totalDefinitivo =  ($totalInicial+$totalAdicion-$totalReduccion) + ($totalCredito-$totalContraCredito);
                                        $saldo = $totalDefinitivo-$totalCdp;
                                        $codigoFuente = $arbolDetData[$i][1];
                                        $nombreFuente = array_values(array_filter($arrFuentes,function($e) use($codigoFuente) {return $e['codigo_fuente'] == $codigoFuente;}))[0]['nombre'];
                                        array_push($arrCuentasDet,[
                                            'codigo_cuenta' => $arbolDetData[$i][0],
                                            'codigo_fuente' => $arbolDetData[$i][1],
                                            'codigo_seccion' => $arbolDetData[$i][2],
                                            'codigo_vigencia' => $arbolDetData[$i][3],
                                            'codigo_pago' => $arbolDetData[$i][4],
                                            'codigo_unidad' => $arbolDetData[$i][6],
                                            'nombre_unidad' => $arbolDetData[$i][7],
                                            'nombre_cuenta' => $data['nombre'],
                                            'nombre_fuente'=> $nombreFuente,
                                            'nombre_tipo'=>"funcionamiento",
                                            'tipo' => "C",
                                            'inicial' => $totalInicial,
                                            'adicion' => $totalAdicion ,
                                            'reduccion' => $totalReduccion,
                                            'credito' => $totalCredito,
                                            'contracredito' => $totalContraCredito,
                                            'cdp' => $arbolDetData[$i]['tipo'] == "cdp" ? $arbolDetData[$i][5] : 0 ,
                                            'rp' => $arbolDetData[$i]['tipo'] == "rp" ? $arbolDetData[$i][5] : 0 ,
                                            'egreso' => $totalEgreso ,
                                            'cxp' => $arbolDetData[$i]['tipo'] == "cxp" ? $arbolDetData[$i][5] : 0 ,
                                            'definitivo'=>$totalDefinitivo,
                                            'disponibilidad'=>$totalCdp,
                                            'compromisos'=>$totalRp,
                                            'obligacion'=>$totalCxp,
                                            'saldo'=>$saldo,
                                        ]);
                                    }
                                }
                            }
                            $arbolDet = array_values(array_filter($arrCuentasDet,function($e){return $e['codigo_fuente'] !="";}));
                        }
                        $totalInicial = 0;
                        $totalAdicion = 0;
                        $totalReduccion = 0;
                        $totalCredito = 0;
                        $totalContraCredito = 0;
                        $totalDefinitivo = 0;
                        $totalDisponibilidad = 0;
                        $totalCompromisos = 0;
                        $totalObligacion =0;
                        $totalEgreso = 0;
                        $totalSaldo = 0;
                        foreach ($arbolDet as $det) {
                            $totalInicial += $det['inicial'];
                            $totalAdicion += $det['adicion'];
                            $totalReduccion += $det['reduccion'];
                            $totalCredito += $det['credito'];
                            $totalContraCredito += $det['contracredito'];
                            $totalDefinitivo += $det['definitivo'];
                            $totalDisponibilidad +=$det['disponibilidad'];
                            $totalCompromisos += $det['compromisos'];
                            $totalObligacion +=$det['obligacion'];
                            $totalSaldo +=$det['saldo'];
                            $totalEgreso+=$det['egreso'];
                        }
                        $total = $totalInicial+$totalAdicion+$totalReduccion+$totalCredito+
                        $totalContraCredito+$totalDefinitivo+$totalDisponibilidad+$totalCompromisos+$totalObligacion+$totalSaldo+$totalEgreso;
                        $arrCuentaCab['detalle'] = $arbolDet;
                        $arrCuentaCab['total_inicial'] = $totalInicial;
                        $arrCuentaCab['total_adicion'] = $totalAdicion;
                        $arrCuentaCab['total_reduccion'] = $totalReduccion;
                        $arrCuentaCab['total_credito'] = $totalCredito;
                        $arrCuentaCab['total_contracredito'] = $totalContraCredito;
                        $arrCuentaCab['total_definitivo'] = $totalDefinitivo;
                        $arrCuentaCab['total_disponibilidad'] = $totalDisponibilidad;
                        $arrCuentaCab['total_compromisos'] = $totalCompromisos;
                        $arrCuentaCab['total_obligacion'] = $totalObligacion;
                        $arrCuentaCab['total_saldo'] = $totalSaldo;
                        $arrCuentaCab['total_egreso'] = $totalEgreso;
                        $arrCuentaCab['total'] = $total;

                        $totalFuncionamientoInicial += $totalInicial;
                        $totalFuncionamientoAdicion += $totalAdicion;
                        $totalFuncionamientoReduccion += $totalReduccion;
                        $totalFuncionamientoCredito += $totalCredito;
                        $totalFuncionamientoContraCredito += $totalContraCredito;
                        $totalFuncionamientoDefinitivo += $totalDefinitivo;
                        $totalFuncionamientoDisponibilidad += $totalDisponibilidad;
                        $totalFuncionamientoCompromisos += $totalCompromisos;
                        $totalFuncionamientoObligacion+=$totalObligacion;
                        $totalFuncionamientoSaldo += $totalSaldo;
                        $totalFuncionamientoEgreso += $totalEgreso;
                        array_push($arrArbolUnidad,$arrCuentaCab);
                    }
                }
            }

            //Se construye árbol para inversion
            foreach($arrSectores as $sector){
                array_push($arbolInversion,[
                        "codigo"=>$sector['codigo'],
                        "nombre"=>$sector['nombre'],
                        "tipo_nombre"=>"Sector",
                        "tipo"=>"A",
                        'total_inicial' => 0,
                        'total_adicion' => 0,
                        'total_reduccion' => 0,
                        'total_credito' => 0,
                        'total_contracredito' => 0,
                        'total_definitivo' => 0,
                        'total_disponibilidad' => 0,
                        'total_compromisos' => 0,
                        'total_obligacion' => 0,
                        'total_saldo' => 0,
                        'total_egreso' => 0,
                        'total' => 0,
                    ]
                );
                //Filtro los programas por sector
                $programas = array_values(array_filter($arrProgramas,function($pro) use($sector){
                    return strpos($pro['codigo'],$sector['codigo']) === 0;
                }));
                if(!empty($programas)){

                    foreach ($programas as $pro) {
                        array_push($arbolInversion,[
                                "codigo"=>$pro['codigo'],
                                "nombre"=>$pro['nombre'],
                                "tipo_nombre"=>"Programa",
                                "tipo"=>"A",
                                'total_inicial' => 0,
                                'total_adicion' => 0,
                                'total_reduccion' => 0,
                                'total_credito' => 0,
                                'total_contracredito' => 0,
                                'total_definitivo' => 0,
                                'total_disponibilidad' => 0,
                                'total_compromisos' => 0,
                                'total_obligacion' => 0,
                                'total_saldo' => 0,
                                'total_egreso' => 0,
                                'total' => 0,
                            ]
                        );
                        $subprogramas = array_values(array_filter($arrProgramas,function($sub) use($pro){
                            return $pro['codigo']==$sub['codigo'];
                        }));
                        foreach ($subprogramas as $sub) {
                            $codigoSubprograma = $sub['codigo'].$sub['codigo_subprograma'];
                            array_push($arbolInversion,[
                                    "codigo"=>$pro['codigo'],
                                    "codigo_subprograma"=>$codigoSubprograma,
                                    "nombre"=>$sub['nombre_subprograma'],
                                    "tipo_nombre"=>"Subprograma",
                                    "tipo"=>"A",
                                    'total_inicial' => 0,
                                    'total_adicion' => 0,
                                    'total_reduccion' => 0,
                                    'total_credito' => 0,
                                    'total_contracredito' => 0,
                                    'total_definitivo' => 0,
                                    'total_disponibilidad' => 0,
                                    'total_compromisos' => 0,
                                    'total_obligacion' => 0,
                                    'total_saldo' => 0,
                                    'total_egreso' => 0,
                                    'total' => 0,
                                ]
                            );

                            //Filtro los productos que pertenecen al subprograma
                            $productos = array_values(array_filter($arrProductos,function($e) use($sub){
                                return strpos($e['cod_producto'],$sub['codigo'])  ===0;
                            }));
                            foreach ($productos as $p) {
                                $programaticos = array_values(array_filter($arrProgramatico,function($e) use($p){
                                    return strpos($e['cod_producto'],$p['cod_producto'])  ===0;
                                }));

                                if(!empty($programaticos)){
                                    array_push($arbolInversion,
                                        [
                                            "codigo"=>$p['cod_producto'],
                                            "nombre"=>$p['producto'],
                                            "tipo_nombre"=>"Producto",
                                            "tipo"=>"A",
                                            'total_inicial' => 0,
                                            'total_adicion' => 0,
                                            'total_reduccion' => 0,
                                            'total_credito' => 0,
                                            'total_contracredito' => 0,
                                            'total_definitivo' => 0,
                                            'total_disponibilidad' => 0,
                                            'total_compromisos' => 0,
                                            'total_obligacion' => 0,
                                            'total_saldo' => 0,
                                            'total_egreso' => 0,
                                            'total' => 0,
                                        ]
                                    );
                                    foreach ($programaticos as $ind) {
                                        $arrProyectos = array_values(array_filter($cuentasCcpetUsadasProy,function($e) use($ind,$unidad){
                                            return $e[6] == $ind['codigo_indicador'] && $e[15] == $unidad['codigo'];
                                        }));
                                        //En este for asigno todas los proyectos usados al indicador producto correspondiente correspondiente
                                        $totalProyectos = count($arrProyectos);
                                        $arbolDet = [];
                                        if($totalProyectos > 0){
                                            for ($i=0; $i < $totalProyectos ; $i++) {
                                                $e = $arrProyectos[$i];
                                                $indicadorProducto = $e[6];
                                                $proyecto = $e[9];
                                                $fuente = $e[11];
                                                $seccion = $e[12];
                                                $vigencia = $e[13];
                                                $pago = $e[14];
                                                $unidadEj = $e[15];

                                                $arrInicial = array_filter($cuentasCcpetUsadasIniInv,function($cuenta) use($unidadEj,$proyecto,$fuente,$seccion,$vigencia,$pago,$indicadorProducto){
                                                    return $cuenta[4] == $indicadorProducto && $cuenta[8] == $unidadEj && $cuenta[0] == $seccion
                                                    && $cuenta[1] == $proyecto && $cuenta[2] == $fuente && $vigencia == $cuenta[7] && $pago == $cuenta[3];
                                                });
                                                $arrAdicion = array_filter($cuentasCcpetUsadasAdInv,function($cuenta) use($unidadEj,$proyecto,$fuente,$seccion,$vigencia,$pago,$indicadorProducto){
                                                    return $cuenta[4] == $indicadorProducto && $cuenta[7] == $unidadEj && $cuenta[0] == $seccion && $cuenta[1] == $proyecto
                                                    && $cuenta[2] == $fuente && $vigencia == $cuenta[6] && $pago == $cuenta[3];
                                                });
                                                $arrReduccion = array_filter($cuentasCcpetUsadasRedInv,function($cuenta) use($unidadEj,$proyecto,$fuente,$seccion,$vigencia,$pago,$indicadorProducto){
                                                    return $cuenta[4] == $indicadorProducto && $cuenta[7] == $unidadEj && $cuenta[0] == $seccion && $cuenta[1] == $proyecto
                                                    && $cuenta[2] == $fuente && $vigencia == $cuenta[6] && $pago == $cuenta[3];
                                                });
                                                $arrCredito = array_filter($cuentasCcpetUsadasTrasladoCredInv,function($cuenta) use($unidadEj,$proyecto,$fuente,$seccion,$vigencia,$pago,$indicadorProducto){
                                                    return $cuenta[4] == $indicadorProducto && $cuenta[7] == $unidadEj && $cuenta[0] == $seccion && $cuenta[1] == $proyecto
                                                    && $cuenta[2] == $fuente && $vigencia == $cuenta[6] && $pago == $cuenta[3];
                                                });
                                                $arrContracredito = array_filter($cuentasCcpetUsadasTrasladoContraCredInv,function($cuenta) use($unidadEj,$proyecto,$fuente,$seccion,$vigencia,$pago,$indicadorProducto){
                                                    return $cuenta[4] == $indicadorProducto && $cuenta[7] == $unidadEj && $cuenta[0] == $seccion && $cuenta[1] == $proyecto
                                                    && $cuenta[2] == $fuente && $vigencia == $cuenta[6] && $pago == $cuenta[3];
                                                });
                                                $arrCdp = array_filter($cuentasCcpetUsadasCdpInv,function($cuenta) use($unidadEj,$proyecto,$fuente,$seccion,$vigencia,$pago,$indicadorProducto){
                                                    return $cuenta[4] == $indicadorProducto && $cuenta[7] == $unidadEj && $cuenta[0] == $seccion && $cuenta[1] == $proyecto
                                                    && $cuenta[2] == $fuente && $vigencia == $cuenta[6] && $pago == $cuenta[3];
                                                });
                                                $arrRp = array_filter($cuentasCcpetUsadasRpInv,function($cuenta) use($unidadEj,$proyecto,$fuente,$seccion,$vigencia,$pago,$indicadorProducto){
                                                    return $cuenta[4] == $indicadorProducto && $cuenta[7] == $unidadEj && $cuenta[0] == $seccion && $cuenta[1] == $proyecto
                                                    && $cuenta[2] == $fuente && $vigencia == $cuenta[6] && $pago == $cuenta[3];
                                                });
                                                $arrCxp = array_filter($cuentasCcpetUsadasCxpInv,function($cuenta) use($unidadEj,$proyecto,$fuente,$seccion,$vigencia,$pago,$indicadorProducto){
                                                    return $cuenta[4] == $indicadorProducto && $cuenta[7] == $unidadEj && $cuenta[0] == $seccion && $cuenta[1] == $proyecto
                                                    && $cuenta[2] == $fuente && $vigencia == $cuenta[6] && $pago == $cuenta[3];
                                                });
                                                $arrEgreso = array_filter($cuentasCcpetUsadasEgresoInv,function($cuenta) use($unidadEj,$proyecto,$fuente,$seccion,$vigencia,$pago,$indicadorProducto){
                                                    return $cuenta[4] == $indicadorProducto && $cuenta[7] == $unidadEj && $cuenta[0] == $seccion && $cuenta[1] == $proyecto
                                                    && $cuenta[2] == $fuente && $vigencia == $cuenta[6] && $pago == $cuenta[3];
                                                });
                                                $arrDataDet = [];
                                                foreach ($arrInicial as $data) {$data['tipo'] = "inicial";$data['vigencia'] =$vigencia;array_push($arrDataDet,$data);}
                                                foreach ($arrAdicion as $data) {$data['tipo'] = "adicion";$data['vigencia'] = $vigencia;array_push($arrDataDet,$data);}
                                                foreach ($arrReduccion as $data) {$data['tipo'] = "reduccion";$data['vigencia'] = $vigencia;array_push($arrDataDet,$data);}
                                                foreach ($arrCredito as $data) {$data['tipo'] = "credito";$data['vigencia'] = $vigencia;array_push($arrDataDet,$data);}
                                                foreach ($arrContracredito as $data) {$data['tipo'] = "contracredito";$data['vigencia'] = $vigencia;array_push($arrDataDet,$data);}
                                                foreach ($arrCdp as $data) {$data['tipo'] = "cdp";$data['vigencia'] = $vigencia;array_push($arrDataDet,$data);}
                                                foreach ($arrRp as $data) {$data['tipo'] = "rp";$data['vigencia'] = $vigencia;array_push($arrDataDet,$data);}
                                                foreach ($arrCxp as $data) {$data['tipo'] = "cxp";$data['vigencia'] = $vigencia;array_push($arrDataDet,$data);}
                                                foreach ($arrEgreso as $data) {$data['tipo'] = "egreso";$data['vigencia'] = $vigencia;array_push($arrDataDet,$data);}

                                                $arrProyectos[$i]['data']=$arrDataDet;
                                            }
                                            $arbolDet = $arrProyectos;
                                            $arbolDet = array_values(array_filter($arbolDet,function($e){return !empty($e['data']);}));
                                            $arrCuentasDet = [];
                                            if(!empty($arbolDet)){
                                                sort($arbolDet);
                                            }

                                            foreach ($arbolDet as $data) {
                                                $arbolDetData = $data['data'];
                                                $rubro = $arbolDetData[0][4]."-".$arbolDetData[0][1]."-".$arbolDetData[0][2]."-".$arbolDetData[0][0]."-".$arbolDetData[0]["vigencia"]."-".$arbolDetData[0][3];
                                                $totalInicial = 0;
                                                $totalAdicion = 0;
                                                $totalReduccion = 0;
                                                $totalCredito = 0;
                                                $totalContraCredito = 0;
                                                $totalCdp = 0;
                                                $totalRp = 0;
                                                $totalCxp = 0;
                                                $totalEgreso = 0;
                                                foreach ($data['data'] as $arbolDetData) {
                                                    $valor = 0;
                                                    if($arbolDetData['tipo'] == "inicial" && $arbolDetData[3]== "SSF"){
                                                        $valor = $arbolDetData[6];
                                                    }else if($arbolDetData['tipo'] == "inicial" && $arbolDetData[3]== "CSF"){
                                                        $valor = $arbolDetData[5];
                                                    }
                                                    $inicial =  $valor;
                                                    $adicion = $arbolDetData['tipo'] == "adicion" ? $arbolDetData[5] : 0;
                                                    $reduccion = $arbolDetData['tipo'] == "reduccion" ? $arbolDetData[5]:0;
                                                    $credito = $arbolDetData['tipo'] == "credito" ? $arbolDetData[5]: 0;
                                                    $contracredito = $arbolDetData['tipo'] == "contracredito" ? $arbolDetData[5] : 0;
                                                    $cdp = $arbolDetData['tipo'] == "cdp" ? $arbolDetData[5] : 0;
                                                    $rp = $arbolDetData['tipo'] == "rp" ? $arbolDetData[5] : 0;
                                                    $cxp = $arbolDetData['tipo'] == "cxp" ? $arbolDetData[5] : 0;
                                                    $egreso = $arbolDetData['tipo'] == "egreso" ? $arbolDetData[5] : 0;

                                                    $totalInicial += $inicial;
                                                    $totalAdicion += $adicion;
                                                    $totalReduccion += $reduccion;
                                                    $totalCredito += $credito;
                                                    $totalContraCredito += $contracredito;
                                                    $totalCdp += $cdp;
                                                    $totalRp += $rp;
                                                    $totalCxp += $cxp;
                                                    $totalEgreso += $egreso;
                                                }
                                                $totalDefinitivo =  ($totalInicial+$totalAdicion-$totalReduccion) + ($totalCredito-$totalContraCredito);
                                                $saldo = $totalDefinitivo-$totalCdp;
                                                $codigoFuente = $arbolDetData[2];
                                                $nombreFuente = array_values(array_filter($arrFuentes,function($e) use($codigoFuente) {return $e['codigo_fuente'] == $codigoFuente;}))[0]['nombre'];
                                                array_push($arrCuentasDet,[
                                                    'codigo_cuenta' => $rubro,
                                                    'codigo_fuente' => $arbolDetData[2],
                                                    'codigo_seccion' => $arbolDetData[0],
                                                    'codigo_vigencia' => $arbolDetData['vigencia'],
                                                    'codigo_pago' => $arbolDetData[3],
                                                    'codigo_unidad' => $unidad['codigo'],
                                                    'nombre_unidad' => $unidad['nombre'],
                                                    'nombre_cuenta' => $data[10],
                                                    'nombre_fuente'=> $nombreFuente,
                                                    'tipo' => "C",
                                                    'inicial' => $totalInicial,
                                                    'adicion' => $totalAdicion ,
                                                    'reduccion' => $totalReduccion,
                                                    'credito' => $totalCredito,
                                                    'contracredito' => $totalContraCredito,
                                                    'cdp' => $totalCdp ,
                                                    'rp' => $totalRp,
                                                    'egreso' => $totalEgreso ,
                                                    'cxp' => $totalCxp,
                                                    'definitivo'=>$totalDefinitivo,
                                                    'disponibilidad'=>$totalCdp,
                                                    'compromisos'=>$totalRp,
                                                    'obligacion'=>$totalCxp,
                                                    'saldo'=>$saldo,
                                                    "nombre_tipo"=>"inversion"
                                                ]);
                                            }
                                            $arbolDet = $arrCuentasDet;
                                        }

                                        $totalInicial = 0;
                                        $totalAdicion = 0;
                                        $totalReduccion = 0;
                                        $totalCredito = 0;
                                        $totalContraCredito = 0;
                                        $totalDefinitivo = 0;
                                        $totalDisponibilidad = 0;
                                        $totalCompromisos = 0;
                                        $totalObligacion =0;
                                        $totalEgreso = 0;
                                        $totalSaldo = 0;
                                        foreach ($arbolDet as $det) {
                                            $totalInicial += $det['inicial'];
                                            $totalAdicion += $det['adicion'];
                                            $totalReduccion += $det['reduccion'];
                                            $totalCredito += $det['credito'];
                                            $totalContraCredito += $det['contracredito'];
                                            $totalDefinitivo += $det['definitivo'];
                                            $totalDisponibilidad +=$det['disponibilidad'];
                                            $totalCompromisos += $det['compromisos'];
                                            $totalObligacion +=$det['obligacion'];
                                            $totalSaldo +=$det['saldo'];
                                            $totalEgreso+=$det['egreso'];
                                        }
                                        $total = $totalInicial+$totalAdicion+$totalReduccion+$totalCredito+
                                        $totalContraCredito+$totalDefinitivo+$totalDisponibilidad+$totalCompromisos+$totalObligacion+$totalSaldo+$totalEgreso;
                                        array_push($arbolInversion,[
                                                "codigo"=>$ind['codigo_indicador'],
                                                "nombre"=>$ind['indicador_producto'],
                                                "tipo_nombre"=>"Programático",
                                                "tipo"=>"A",
                                                'total_inicial' => $totalInicial,
                                                'total_adicion' => $totalAdicion,
                                                'total_reduccion' => $totalReduccion,
                                                'total_credito' => $totalCredito,
                                                'total_contracredito' => $totalContraCredito,
                                                'total_definitivo' => $totalDefinitivo,
                                                'total_disponibilidad' => $totalDisponibilidad,
                                                'total_compromisos' => $totalCompromisos,
                                                'total_obligacion' => $totalObligacion,
                                                'total_saldo' => $totalSaldo,
                                                'total_egreso' => $totalEgreso,
                                                'total' => $total,
                                                'detalle'=>$arbolDet
                                            ]
                                        );
                                        $totalInversionInicial += $totalInicial;
                                        $totalInversionAdicion += $totalAdicion;
                                        $totalInversionReduccion += $totalReduccion;
                                        $totalInversionCredito += $totalCredito;
                                        $totalInversionContraCredito += $totalContraCredito;
                                        $totalInversionDefinitivo += $totalDefinitivo;
                                        $totalInversionDisponibilidad += $totalDisponibilidad;
                                        $totalInversionCompromisos += $totalCompromisos;
                                        $totalInversionObligacion+=$totalObligacion;
                                        $totalInversionSaldo += $totalSaldo;
                                        $totalInversionEgreso += $totalEgreso;
                                    }
                                }
                            }
                        }
                    }
                }
            }
            array_push($arrArbol,[
                "nombre"=>$unidad['codigo']." - ".$unidad['nombre'],
                "info"=>array_merge($arrArbolUnidad,$arbolInversion),
                "funcionamiento"=>[
                    'total_inicial' => $totalFuncionamientoInicial,
                    'total_adicion' => $totalFuncionamientoAdicion,
                    'total_reduccion' => $totalFuncionamientoReduccion,
                    'total_credito' => $totalFuncionamientoCredito,
                    'total_contracredito' => $totalFuncionamientoContraCredito,
                    'total_definitivo' => $totalFuncionamientoDefinitivo,
                    'total_disponibilidad' => $totalFuncionamientoDisponibilidad,
                    'total_compromisos' => $totalFuncionamientoCompromisos,
                    'total_obligacion' => $totalFuncionamientoObligacion,
                    'total_saldo' => $totalFuncionamientoSaldo,
                    'total_egreso' => $totalFuncionamientoEgreso,
                ],
                "inversion"=>[
                    'total_inicial' => $totalInversionInicial,
                    'total_adicion' => $totalInversionAdicion,
                    'total_reduccion' => $totalInversionReduccion,
                    'total_credito' => $totalInversionCredito,
                    'total_contracredito' => $totalInversionContraCredito,
                    'total_definitivo' => $totalInversionDefinitivo,
                    'total_disponibilidad' => $totalInversionDisponibilidad,
                    'total_compromisos' => $totalInversionCompromisos,
                    'total_obligacion' => $totalInversionObligacion,
                    'total_saldo' => $totalInversionSaldo,
                    'total_egreso' => $totalInversionEgreso,
                ],
                "total"=>[
                    'total_inicial' => $totalInversionInicial+$totalFuncionamientoInicial,
                    'total_adicion' => $totalInversionAdicion+$totalFuncionamientoAdicion,
                    'total_reduccion' => $totalInversionReduccion+$totalFuncionamientoReduccion,
                    'total_credito' => $totalInversionCredito+$totalFuncionamientoCredito,
                    'total_contracredito' => $totalInversionContraCredito+$totalFuncionamientoContraCredito,
                    'total_definitivo' => $totalInversionDefinitivo+$totalFuncionamientoDefinitivo,
                    'total_disponibilidad' => $totalInversionDisponibilidad+$totalFuncionamientoDisponibilidad,
                    'total_compromisos' => $totalInversionCompromisos+$totalFuncionamientoCompromisos,
                    'total_obligacion' => $totalInversionObligacion+$totalFuncionamientoObligacion,
                    'total_saldo' => $totalInversionSaldo+$totalFuncionamientoSaldo,
                    'total_egreso' => $totalInversionEgreso+$totalFuncionamientoEgreso,
                ]
            ]);
        }
        $totalArbol = count($arrArbol);
        for ($i=0; $i < $totalArbol; $i++) {
            $arbol = $arrArbol[$i];
            $arbolInfo = $arbol['info'];
            $totalArbolInfo = count($arbolInfo);
            for ($j=0; $j < $totalArbolInfo ; $j++) {
                $info = $arbolInfo[$j];
                $arrNiveles = array_values(array_filter(
                    $arbolInfo,function($e) use($info){return strpos($e['codigo'],$info['codigo'])===0;}
                ));
                if(!empty($arrNiveles)){
                    $totalInicial = 0;
                    $totalAdicion = 0;
                    $totalReduccion = 0;
                    $totalCredito = 0;
                    $totalContraCredito = 0;
                    $totalDefinitivo = 0;
                    $totalDisponibilidad = 0;
                    $totalCompromisos = 0;
                    $totalObligacion =0;
                    $totalSaldo = 0;
                    $totalEgreso = 0;
                    foreach ($arrNiveles as $det) {
                        $totalInicial += $det['total_inicial'];
                        $totalAdicion += $det['total_adicion'];
                        $totalReduccion += $det['total_reduccion'];
                        $totalCredito += $det['total_credito'];
                        $totalContraCredito += $det['total_contracredito'];
                        $totalDefinitivo += $det['total_definitivo'];
                        $totalDisponibilidad +=$det['total_disponibilidad'];
                        $totalCompromisos += $det['total_compromisos'];
                        $totalObligacion +=$det['total_obligacion'];
                        $totalSaldo +=$det['total_saldo'];
                        $totalEgreso +=$det['total_egreso'];
                    }
                    $total = $totalInicial+$totalAdicion+$totalReduccion+$totalCredito+
                    $totalContraCredito+$totalDefinitivo+$totalDisponibilidad+$totalCompromisos+$totalObligacion+$totalSaldo+$totalEgreso;
                    $info['total_inicial'] =  $info['tipo_nombre'] == "Subprograma" ? $totalInicial/2: $totalInicial;
                    $info['total_adicion'] = $info['tipo_nombre'] == "Subprograma" ? $totalAdicion/2: $totalAdicion;
                    $info['total_reduccion'] = $info['tipo_nombre'] == "Subprograma" ? $totalReduccion/2: $totalReduccion;
                    $info['total_credito'] = $info['tipo_nombre'] == "Subprograma" ? $totalCredito/2: $totalCredito;
                    $info['total_contracredito'] = $info['tipo_nombre'] == "Subprograma" ? $totalContraCredito/2: $totalContraCredito;
                    $info['total_definitivo'] = $info['tipo_nombre'] == "Subprograma" ? $totalDefinitivo/2: $totalDefinitivo;
                    $info['total_disponibilidad'] = $info['tipo_nombre'] == "Subprograma" ? $totalDisponibilidad/2: $totalDisponibilidad;
                    $info['total_compromisos'] = $info['tipo_nombre'] == "Subprograma" ? $totalCompromisos/2: $totalCompromisos;
                    $info['total_obligacion'] = $info['tipo_nombre'] == "Subprograma" ? $totalObligacion/2: $totalObligacion;
                    $info['total_saldo'] = $info['tipo_nombre'] == "Subprograma" ? $totalSaldo/2: $totalSaldo;
                    $info['total_egreso'] = $info['tipo_nombre'] == "Subprograma" ? $totalEgreso /2: $totalEgreso;
                    $info['total'] = $info['tipo_nombre'] == "Subprograma" ? $total/2: $total;
                    $arbolInfo[$j] =$info;
                }
            }

            $arbolInfo[0]['total_inicial'] =  $arbol['funcionamiento']['total_inicial'];
            $arbolInfo[0]['total_adicion'] = $arbol['funcionamiento']['total_adicion'];
            $arbolInfo[0]['total_reduccion'] = $arbol['funcionamiento']['total_reduccion'];
            $arbolInfo[0]['total_credito'] = $arbol['funcionamiento']['total_credito'];
            $arbolInfo[0]['total_contracredito'] = $arbol['funcionamiento']['total_contracredito'];
            $arbolInfo[0]['total_definitivo'] = $arbol['funcionamiento']['total_definitivo'];
            $arbolInfo[0]['total_disponibilidad'] = $arbol['funcionamiento']['total_disponibilidad'];
            $arbolInfo[0]['total_compromisos'] = $arbol['funcionamiento']['total_compromisos'];
            $arbolInfo[0]['total_obligacion'] = $arbol['funcionamiento']['total_obligacion'];
            $arbolInfo[0]['total_saldo'] =  $arbol['funcionamiento']['total_saldo'];
            $arbolInfo[0]['total_egreso'] = $arbol['funcionamiento']['total_egreso'];
            $arbol['info'] = array_values(array_filter($arbolInfo,function($e){return $e['total']!=0;}));
            $arrArbol[$i] = $arbol;
        }
        $arrArbol = array_values(array_filter($arrArbol,function($e){return !empty($e['info']);}));
        $out['arbol'] = $arrArbol;
    }

    header("Content-type: application/json");
    echo json_encode($out);
    die();
