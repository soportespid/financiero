var app = new Vue({
	el: '#myapp',
	data:{
		numerosdenomina: [],
		numeronomina: '',
		tiposDeMovimiento: [],
		tipoMovimiento: '',
		consecutivo: '',
		mostrarCrearCdp : false,
		mostrarReversionCdp : false,
		mostrarInversion : false,
		mostrarFuncionamiento : true,
		vigencia: '',
		solicita: '',
		objeto: '',
		tiposDeGasto: [],
		tipoGasto: '',
		unidadesejecutoras: [],
		selectUnidadEjecutora : '',
		optionsMediosPagos: [
			{ text: 'CSF', value: 'CSF' },
			{ text: 'SSF', value: 'SSF' }
		],
		selectMedioPago: 'CSF',
		vigenciasdelgasto: [],
		selectVigenciaGasto: '',
		cuenta:'',
		nombreCuenta: '',
		showModal_cuentas: false,
		fuente: '',
		nombreFuente: '',
		valor: 0,
		saldo: 0,
		saldonuevo:[],
		saldonuevo2:[],

		detalles: [],

		cuentasCcpet:[],
		searchCuenta : {keywordCuenta: ''},
		searchProyecto : {keywordProyecto: ''},

		showModal_fuentes: false,
		showModal_divipola: false,
		searchFuente : {keywordFuente: ''},
		searchDivipola : {keywordDivipola: ''},
		searchChip : {keywordChip: ''},

		showModal_chips: false,
		chips : [],

		fuentesCuipo: [],

		totalCdp : 0,

		codProyecto: '',
		nombreProyecto: '',
		showModal_proyectos: false,
		proyectos: [],

		programatico: '',
		nombreProgramatico: '',
		showModal_programatico: false,
		programaticos: [],

		divipola: '',
		nombreDivipola: '',

		chip: '',
		nombreChip: '',

		codigoCpc: '',
		nombreCodigoCpc: '',
		cuentaConCpc: '',

		showModal_bienes_transportables: false,
		show_table_search: false,
		secciones: [],

		searchBienes: {keyword: ''},
		searchDivision: {keywordDivision: ''},
		searchGrupo: {keywordGrupo: ''},
		searchClase: {keywordClase: ''},
		searchSubClase: {keywordSubClase: ''},

		divisiones: [],
		grupos: [],
		clases: [],
		subClases: [],
		subClasesServicios: [],
		subClases_captura: [],

		division_p: '',
		division_p_nombre: '',
		grupo_p: '',
		grupo_p_nombre: '',
		clase_p: '',
		clase_p_nombre: '',
		subClase_p: '',
		subClase_p_nombre: '',
		seccion_p: '',
		seccion_p_nombre: '',

		mostrarDivision: false,
		mostrarGrupo: false,
		mostrarClase: false,
		mostrarSubClase: false,
		mostrarSubClaseProducto: false,

		valorTotal: 0,
		valorBienesTranspotables: 0,

		cuentasSelectSubClase: [],
		searchGeneral: {keywordGeneral: ''},
		searchGeneral2: {keywordGeneral: ''},

		cuentaActual: '',

		showModal_servicios: false,
		seccionesServicios: [],

		gruposServicios: [],
		clasesServicios: [],
		subClasesServicios: [],
		divisionesServicios: [],

		divisionServicios_p: '',
		grupoServicios_p: '',
		claseServicios_p: '',
		subClaseServicios_p: '',
		subClaseServicios_p_nombre: '',
		seccionServicios_p: '',
		seccionServicios_p_nombre: '',

		mostrarGrupoServicios: false,
		mostrarClaseServicios: false,
		mostrarSubClaseServicios: false,
		mostrarDivisionServicios: false,

		searchDivisionServicios: {keywordDivisionServicios: ''},
		searchGrupoServicios: {keywordGrupoServicios: ''},
		searchClaseServicios: {keywordClaseServicios: ''},
		searchSubClaseServicios: {keywordSubClaseServicios: ''},

		divipolas: [],
	},
	mounted: async function(){
		await this.cargarFecha();
		this.cargarTiposDeMovientoUsuario();
		this.cargarpresupupuestonomina();
	},
	updated(){
		if(this.vigencia != document.getElementById('vigencia').value){
			this.vigencia = document.getElementById('vigencia').value;
			this.seleccioncarConsecutivo();
		}
	},
	methods:{
		formatonumero: function(valor){
			return new Intl.NumberFormat("en-US", {style: "currency", currency: "USD"}).format(valor);
		},
		cargarTiposDeMovientoUsuario: async function(){
			await axios.post('vue/presupuesto_ccp/ccp-cdpnomina2022.php')
				.then(
					(response) => {
						app.tiposDeMovimiento = response.data.tiposDeMovimiento;
						if(response.data.tiposDeMovimiento.length > 0){
							this.tipoMovimiento = response.data.tiposDeMovimiento[0][0];
						}
					}
				);
			this.seleccionarFormulario();
		},
		cargarpresupupuestonomina: async function(){
			await axios.post('vue/presupuesto_ccp/ccp-cdpnomina2022.php?action=cargarnomina')
			.then((response) => {
				app.numerosdenomina = response.data.numerosdenomina;
			});
		},
		cargarFecha: function(){
			const fechaAct = new Date().toJSON().slice(0,10).replace(/-/g,'/');
			const fechaArr = fechaAct.split('/');
			const fechaV = fechaArr[2]+'/'+fechaArr[1]+'/'+fechaArr[0];
			document.getElementById('fecha').value = fechaV;
			this.vigencia = document.getElementById('vigencia').value = fechaArr[0];
		},
		cargarDetalles: async function(){
			await axios.post('vue/presupuesto_ccp/ccp-cdpnomina2022.php?action=cargarDetallesCdp&numNomina=' + this.numeronomina)
			.then(
				(response) => {
					app.detalles = response.data.detallesCdp;
				}
			);
			var totalsum = 0
			var saldolocal
			for(var x = 0; x< this.detalles.length;x++){
				totalsum += parseInt(this.detalles[x][2]);
				if(this.detalles[x][1].slice(2,3) == '3'){
					//this.calcularSaldoInversionnuevo(this.detalles[x][5], this.detalles[x][9], this.detalles[x][4], this.vigencia, this.detalles[x][11], this.detalles[x][12], this.detalles[x][6], this.detalles[x][13], this.detalles[x][2], x);
					app.saldonuevo[x] = 0;
				}
				else{
					this.calcularSaldoNuevo(this.detalles[x][1], this.detalles[x][4], this.vigencia, this.detalles[x][11], this.detalles[x][12], this.detalles[x][6], this.detalles[x][13], this.detalles[x][2], x);
				}
			}
			this.totalCdp = totalsum;
		},
		seleccionarFormulario: function(){
			if(this.tipoMovimiento == '201'){
				this.mostrarCrearCdp = true;
				this.mostrarReversionCdp = false;
				this.seleccioncarConsecutivo();
				this.seccionPresupuestal();
				this.vigenciasDelgasto();
			}
			else if(this.tipoMovimiento == '401' || this.tipoMovimiento == '402'){
				this.mostrarReversionCdp = true;
				this.mostrarCrearCdp = false;
			}
			else{}
		},
		seccionPresupuestal: async function(){
			await axios.post('vue/presupuesto_ccp/ccp-inicialgastosfun.php?action=secciones')
			.then(
				(response) => {
					this.unidadesejecutoras = response.data.secciones;
					this.selectUnidadEjecutora = this.unidadesejecutoras[0][0];
				}
			);
		},
		vigenciasDelgasto: async function(){
			await axios.post('vue/presupuesto_ccp/ccp-inicialgastosfun.php?action=vigenciasDelGasto')
			.then(
				(response) => {
					this.vigenciasdelgasto = response.data.vigenciasDelGasto;
					this.selectVigenciaGasto = this.vigenciasdelgasto[0][0];
				}
			);
		},
		cambiarParametros: async function(){
			var formData = new FormData();
			formData.append("seccionPresupuestal", this.nombre);
			formData.append("tipo", this.tipogf);
			formData.append("codigo", this.numero);
			formData.append("correlacion", this.correlacion);
			await axios.post('vue/presupuesto_ccp/ccp-funciones.php?action=saldoCuentaCcpet')
			.then(
				(response) => {
					this.saldo = response.data.saldoPorCuenta;
				}
			);
		},
		buscarCta: async function(){
			if(this.cuenta != ''){
				await axios.post('vue/presupuesto_ccp/cp-cdpnomina2022.php?action=buscarCuenta&cuenta=' + this.cuenta)
				.then(
					(response) => {
						if(response.data.resultBusquedaCuenta.length > 0){
							if(response.data.resultBusquedaCuenta[0][1] == 'C'){
								app.nombreCuenta = response.data.resultBusquedaCuenta[0][0];
								app.codigoCpc = '';
								app.nombreCodigoCpc = '';
								app.tieneCpc();
							}
							else{
								Swal.fire(
								'Tipo de cuenta incorrecto.',
								'Escoger una cuenta de captura (C)',
								'warning'
								).then(
									(result) => {
										app.nombreCuenta = '';
										app.cuenta = '';
										app.codigoCpc = '';
										app.nombreCodigoCpc = '';
										app.tieneCpc();
									}
								);
							}
						}
						else{
							Swal.fire(
							'Cuenta incorrecta.',
							'Esta cuenta no existe en el catalogo CCPET.',
							'warning'
							).then(
								(result) => {
									app.nombreCuenta = '';
									app.cuenta = '';
									app.codigoCpc = '';
									app.nombreCodigoCpc = '';
									app.tieneCpc();
								}
							);
						}
					}
				);
			}
			else {this.nombreCuenta = '';}
			this.parametrosCompletos();
		},
		buscarProyecto: async function(){
			if(this.codProyecto != ''){
				await axios.post('vue/presupuesto_ccp/cp-cdpnomina2022.php?action=buscarProyecto&proyecto=' + this.codProyecto)
				.then(
					(response) => {
						if(response.data.resultBusquedaProyecto.length > 0){
							app.nombreProyecto = response.data.resultBusquedaProyecto[0][0];
						}
						else{
							Swal.fire(
							'Proyecto incorrecto.',
							'Este proyecto no existe.',
							'warning'
							).then(
								(result) => {
									app.nombreProyecto = '';
								}
							);
						}
					}
				);
			}
			else {this.nombreProyecto = '';}
			this.parametrosCompletos();
		},
		buscarProgramatico: async function(){
			if(this.programatico != ''){
				if(this.codProyecto != ''){
					await axios.post('vue/presupuesto_ccp/cp-cdpnomina2022.php?action=buscarProgramatico&programatico=' + this.programatico)
					.then(
						(response) => {
							if(response.data.resultBusquedaProgramatico.length > 0){
									app.nombreProgramatico = response.data.resultBusquedaProgramatico[0][0];
							}else{
								Swal.fire(
									'Proyecto incorrecto.',
									'Este proyecto no existe.',
									'warning'
									).then((result) => {
										app.nombreProgramatico = '';
									}
								);
							}
						}
					);
				}
				else{
					Swal.fire(
						'Falta escoger un proyecto.',
						'',
						'info'
					);
				}
			}
			else {this.nombreProgramatico = '';}
			this.parametrosCompletos();
		},
		seleccionarFuente: function(fuenteSelec){
			this.fuente = fuenteSelec [0];
			this.nombreFuente = fuenteSelec [1];
			this.showModal_fuentes = false;
			this.parametrosCompletos();
		},
		seleccionarDivipola: function(divipolaSelec){
			this.divipola = divipolaSelec [0];
			this.nombreDivipola = divipolaSelec [1];
			this.showModal_divipola = false;
		},
		seleccionarChip: function(chipSelec){
			this.chip = chipSelec [0];
			this.nombreChip = chipSelec [1];
			this.showModal_chips = false;
		},
		buscarFuente: async function(){
			if(this.fuente != ''){
				await axios.post('vue/presupuesto_ccp/cp-cdpnomina2022.php?action=buscarFuente&fuente=' + this.fuente)
				.then(
					(response) => {
						if(response.data.resultBusquedaFuente.length > 0)
						{
							app.nombreFuente = response.data.resultBusquedaFuente[0][0];
						}
						else{
							Swal.fire(
								'Fuente incorrecta.',
								'Esta Fuente no existe en las fuentes CUIPO del sistema.',
								'warning'
							).then(
								(result) => {
									app.nombreFuente = '';
									app.fuente = '';
								}
							);
						}
					}
				);
			}
			else{
				this.nombreFuente = '';
				this.fuente = '';
			}
			this.parametrosCompletos();
		},
		buscarCodigoCpc: async function(){
			if(this.codigoCpc != ''){
				let iniciCodigoCpc = this.codigoCpc.substr(0,1);
				let finCuenta = this.cuenta.substr(-1, 1);
				if (iniciCodigoCpc == finCuenta){
					await axios.post('vue/presupuesto_ccp/cp-cdpnomina2022.php?action=buscarCpc&codigoCpc=' + this.codigoCpc)
					.then(
						(response) => {
							if(response.data.resultBusquedaCpc.length > 0){
								app.nombreCodigoCpc = response.data.resultBusquedaCpc[0][0];
							}
							else{
								Swal.fire(
									'C&oacute;digo CPC incorrecto.',
									'Este C&oacute;digo no existe en el catalogo CPC del sistema.',
									'warning'
								).then(
									(result) => {
										app.nombreCodigoCpc = '';
										app.codigoCpc = '';
									}
								);
							}
						}
					);
				}
				else{
					Swal.fire(
						'C&oacute;digo CPC equivocado.',
						'Este C&oacute;digo no pertenece a esta esta cuenta ccpet.',
						'warning'
					).then(
						(result) => {
							app.nombreCodigoCpc = '';
							app.codigoCpc = '';
						}
					);
				}
			}
			else{
				this.nombreCodigoCpc = '';
			}
		},
		buscarChip: async function(){
			if(this.chip != ''){
				await axios.post('vue/presupuesto_ccp/cp-cdpnomina2022.php?action=buscarChip&chip=' + this.chip)
				.then(
					(response) => {
						if(response.data.resultBusquedaChip.length > 0){
							app.nombreChip = response.data.resultBusquedaChip[0][0];
						}
						else{
							Swal.fire(
								'Chip incorrecto.',
								'Este tercero chip no existe en el sistema.',
								'warning'
							).then(
								(result) => {
									app.nombreChip = '';
									app.chip = '';
								}
							);
						}
					}
				);
			}
			else{
				this.nombreChip = '';
				this.chip = '';
			}
		},
		buscarDivipola: async function(){
			if(this.divipola != ''){
				await axios.post('vue/presupuesto_ccp/cp-cdpnomina2022.php?action=buscarDivipola&divipola=' + this.divipola)
				.then(
					(response) => {
						if(response.data.resultBusquedaDivipola.length > 0){
							
							app.nombreDivipola = response.data.resultBusquedaDivipola[0][0];
							
						}else{
							Swal.fire(
								'Divipola incorrecto.',
								'Este codigo divipola no existe en el sistema.',
								'warning'
							).then(
								(result) => {
									app.nombreDivipola = '';
									app.divipola = '';
								}
							);
						}
					}
				);
			}
			else{
				this.nombreDivipola = '';
				this.divipola = '';
			}
		},
		ventanaFuente: function(){
			if(this.tipoGasto != '3'){
				if(this.cuenta != ''){
					app.showModal_fuentes = true;
					axios.post('vue/presupuesto_ccp/cp-cdpnomina2022.php?action=cargarFuentes&cuenta=' + this.cuenta + '&tipoGasto=' + this.tipoGasto)
						.then(
							(response) => {
								app.fuentesCuipo = response.data.fuentes;
							}
						);
				}
				else{
					Swal.fire(
						'Falta escoger la cuenta presupuestal CCPET.',
						'Escoger la cuenta antes de buscar la fuente, para acotar la busqueda.',
						'warning'
					)
				}
			}
			else{
				if(this.codProyecto != '' && this.programatico != ''){
					app.showModal_fuentes = true;
					axios.post('vue/presupuesto_ccp/cp-cdpnomina2022.php?action=cargarFuentes&tipoGasto=' + this.tipoGasto + '&proyecto=' + this.codProyecto + '&programatico=' + this.programatico)
					.then(
						(response) => {
							app.fuentesCuipo = response.data.fuentes;
						}
					);
				}
				else{
					Swal.fire(
						'Falta escoger el proyecto o indicador programatico.',
						'Escoger el proyecto o indicador programatico antes de buscar la fuente, para acotar la busqueda.',
						'warning'
					)
				}
			}
		},
		ventanaProyecto: function(){
			app.showModal_proyectos = true;
			axios.post('vue/presupuesto_ccp/cp-cdpnomina2022.php?action=cargarProyecto&vigencia='+this.vigencia)
			.then(
				(response) => {
					app.proyectos = response.data.proyectosCcpet;
				}
			);
		},
		ventanaCuenta: function(){
			app.showModal_cuentas = true;
			axios.post('vue/presupuesto_ccp/cp-cdpnomina2022.php?action=cargarCuentas&inicioCuenta=' + this.tiposDeGasto[this.tipoGasto-1][2])
			.then(
				(response) => {
					app.cuentasCcpet = response.data.cuentasCcpet;
					app.tieneCpc();
				}
			);
		},
		ventanaCodigoCpc: function(){
			this.buscaVentanaModal(this.cuentaConCpc, this.cuenta);
		},
		ventanaDivipola: function(){
			app.showModal_divipola = true;
			axios.post('vue/presupuesto_ccp/cp-cdpnomina2022.php?action=cargarDivipolas')
			.then(
				(response) => {
					app.divipolas = response.data.divipolas;
				}
			);
		},
		ventanaChip: function(){
			app.showModal_chips = true;
			axios.post('vue/presupuesto_ccp/cp-cdpnomina2022.php?action=cargarChips')
			.then(
				(response) => {
					app.chips = response.data.chips;
				}
			);
		},
		ventanaProgramatico: function(){
			if(this.codProyecto != ''){
				app.showModal_programatico = true;
				axios.post('vue/presupuesto_ccp/cp-cdpnomina2022.php?action=cargarProgramaticos&proyecto=' + this.codProyecto)
				.then(
					(response) => {
						app.programaticos = response.data.programaticos;
					}
				);
			}
			else{
				Swal.fire(
					'Falta escoger el proyecto.',
					'Antes de seleccionar el program&aacute;tico se debe seleccionar un proyecto.',
					'warning'
				).then(
					(result) => {
						app.nombreFuente = '';
					}
				);
			}
		},
		seleccionarCuenta: function(cuentaSelec){
			if(cuentaSelec[6] == 'C'){
				this.cuenta = cuentaSelec [1];
				this.nombreCuenta = cuentaSelec [2];
				this.showModal_cuentas = false;
				this.tieneCpc();
			}
			else{
				this.showModal_cuentas = false;
				Swal.fire(
					'Tipo de cuenta incorrecto.',
					'Escoger una cuenta de captura (C)',
					'warning'
				).then(
					(result) => {
						if(result.isConfirmed || result.isDismissed){
							this.showModal_cuentas = true;
						}
					}
				);
			}
			this.parametrosCompletos();
		},
		seleccionarProyecto: function(proyectoSelec){
			this.codProyecto = proyectoSelec [2];
			this.nombreProyecto = proyectoSelec [4];
			this.showModal_proyectos = false;
			this.parametrosCompletos();
		},
		seleccionarProgramatico: function(programaticoSelec){
			this.programatico = programaticoSelec [0];
			this.nombreProgramatico = programaticoSelec [1];
			this.showModal_programatico = false;
			this.parametrosCompletos();
		},
		agregarDetalle: function(){
			if(this.tipoGasto != ''){
				if(this.selectUnidadEjecutora != ''){
					if(this.selectMedioPago != ''){
						if(this.selectVigenciaGasto){
							if(this.cuenta != '' ){
								if(this.fuente != ''){
									if(this.valor > 0){
										if(this.valor <= this.saldo){
											if(this.tipoGasto == '3'){
												if(this.codProyecto != ''){
													if(this.programatico != ''){
														if(this.divipola != ''){
															if(this.chip != ''){
																if(this.cuentaConCpc != ''){
																	if(this.codigoCpc != ''){
																		var detallesAgr = [];
																		this.tiposDeGasto.forEach(function logArrayElements(element, index, array) {
																			if(element[0] == app.tipoGasto){
																				detallesAgr.push(element[1]);
																			}
																		});
																		detallesAgr.push(this.selectUnidadEjecutora);
																		this.unidadesejecutoras.forEach(function logArrayElements(element, index, array) {
																			if(element[0] == app.selectUnidadEjecutora){
																				detallesAgr.push(element[1].toLowerCase());
																			}
																		});
																		detallesAgr.push(this.selectMedioPago);
																		detallesAgr.push(this.selectVigenciaGasto);
																		this.vigenciasdelgasto.forEach(function logArrayElements(element, index, array) {
																			if(element[0] == app.selectVigenciaGasto){
																				detallesAgr.push(element[2].toLowerCase());
																			}
																		});
																		detallesAgr.push(this.codProyecto);
																		detallesAgr.push(this.nombreProyecto.toLowerCase().slice(0,30));
																		detallesAgr.push(this.programatico);
																		detallesAgr.push(this.nombreProgramatico.toLowerCase().slice(0,30));
																		detallesAgr.push(this.cuenta);
																		detallesAgr.push(this.nombreCuenta.toLowerCase().slice(0,30));
																		detallesAgr.push(this.fuente);
																		detallesAgr.push(this.nombreFuente.toLowerCase().slice(0,30));
																		detallesAgr.push(this.codigoCpc);
																		detallesAgr.push(this.nombreCodigoCpc.toLowerCase().slice(0,15));
																		detallesAgr.push(this.divipola);
																		detallesAgr.push(this.chip);
																		detallesAgr.push(this.valor);
																		let existeDetalle = 0;
																		this.detalles.forEach(function(elemento){
																			result = app.comparaArrays(elemento, detallesAgr);
																			if(result){
																				existeDetalle = 1;
																				return false;
																			}
																		});
																		if(existeDetalle == 0){
																			this.detalles.push(detallesAgr);
																			this.cuenta = '';
																			this.codProyecto = '';
																			this.nombreProyecto = '';
																			this.programatico = '';
																			this.nombreProgramatico = '';
																			this.nombreCuenta = '';
																			this.fuente = '';
																			this.nombreFuente = '';
																			this.divipola = '';
																			this.nombreDivipola = '';
																			this.codigoCpc = '';
																			this.nombreCodigoCpc = '';
																			this.chip = '';
																			this.nombreChip = '';
																			this.totalCdp = parseFloat(this.totalCdp) + parseFloat(this.valor);
																			this.valor = 0;
																			this.saldo = 0;
																			this.cargarTiposDeGasto();
																			this.tieneCpc();
																		}
																		else{
																			Swal.fire(
																				'Detalle repetido.',
																				'Debe tener el rubro o alguno de sus clasificadores diferentes a los detalles que se han agregado.',
																				'warning'
																			);
																		}
																	}
																	else{
																		Swal.fire(
																			'Falta informaci&oacute;n.',
																			'Falta escoger el c&oacute;digo CPC.',
																			'warning'
																		);
																	}
																}
																else{
																	var detallesAgr = [];
																	this.tiposDeGasto.forEach(function logArrayElements(element, index, array) {
																		if(element[0] == app.tipoGasto){
																			detallesAgr.push(element[1]);
																		}
																	});
																	detallesAgr.push(this.selectUnidadEjecutora);
																	this.unidadesejecutoras.forEach(function logArrayElements(element, index, array) {
																		if(element[0] == app.selectUnidadEjecutora){
																			detallesAgr.push(element[1].toLowerCase());
																		}
																	});
																	detallesAgr.push(this.selectMedioPago);
																	detallesAgr.push(this.selectVigenciaGasto);
																	this.vigenciasdelgasto.forEach(function logArrayElements(element, index, array) {
																		if(element[0] == app.selectVigenciaGasto){
																			detallesAgr.push(element[2].toLowerCase());
																		}
																	});
																	detallesAgr.push(this.codProyecto);
																	detallesAgr.push(this.nombreProyecto.toLowerCase().slice(0,30));
																	detallesAgr.push(this.programatico);
																	detallesAgr.push(this.nombreProgramatico.toLowerCase().slice(0,30));
																	detallesAgr.push(this.cuenta);
																	detallesAgr.push(this.nombreCuenta.toLowerCase().slice(0,30));
																	detallesAgr.push(this.fuente);
																	detallesAgr.push(this.nombreFuente.toLowerCase().slice(0,30));
																	detallesAgr.push(this.codigoCpc);
																	detallesAgr.push(this.nombreCodigoCpc.toLowerCase().slice(0,15));
																	detallesAgr.push(this.divipola);
																	detallesAgr.push(this.chip);
																	detallesAgr.push(this.valor);
																	let existeDetalle = 0;
																	this.detalles.forEach(function(elemento){
																		result = app.comparaArrays(elemento, detallesAgr);
																		if(result){
																			existeDetalle = 1;
																			return false;
																		}
																	});
																	if(existeDetalle == 0){
																		this.detalles.push(detallesAgr);
																		this.cuenta = '';
																		this.codProyecto = '';
																		this.nombreProyecto = '';
																		this.programatico = '';
																		this.nombreProgramatico = '';
																		this.nombreCuenta = '';
																		this.fuente = '';
																		this.nombreFuente = '';
																		this.divipola = '';
																		this.nombreDivipola = '';
																		this.codigoCpc = '';
																		this.nombreCodigoCpc = '';
																		this.chip = '';
																		this.nombreChip = '';
																		this.totalCdp = parseFloat(this.totalCdp) + parseFloat(this.valor);
																		this.valor = 0;
																		this.saldo = 0;
																		this.cargarTiposDeGasto();
																		this.tieneCpc();
																	}
																	else{
																		Swal.fire(
																			'Detalle repetido.',
																			'Debe tener el rubro o alguno de sus clasificadores diferentes a los detalles que se han agregado.',
																			'warning'
																		);
																	}
																}
															}
															else{
																Swal.fire(
																	'Falta informaci&oacute;n.',
																	'Falta escoger el c&oacute;digo CHIP.',
																	'warning'
																);
															}
														}
														else{
															Swal.fire(
																'Falta informaci&oacute;n.',
																'Falta escoger el c&oacute;digo DIVIPOLA.',
																'warning'
															);
														}
													}
													else{
														Swal.fire(
															'Falta informaci&oacute;n.',
															'Falta escoger el indicador program&aacute;tico MGA.',
															'warning'
														);
													}
												}
												else{
													Swal.fire(
														'Falta informaci&oacute;n.',
														'Falta escoger el proyecto de inversi&oacute;n.',
														'warning'
													);
												}
											}
											else{
												if(this.divipola != ''){
													if(this.chip != ''){
														if(this.cuentaConCpc != ''){
															if(this.codigoCpc != ''){
																var detallesAgr = [];
																this.tiposDeGasto.forEach(function logArrayElements(element, index, array) {
																	if(element[0] == app.tipoGasto){
																		detallesAgr.push(element[1]);
																	}
																});
																detallesAgr.push(this.selectUnidadEjecutora);
																this.unidadesejecutoras.forEach(function logArrayElements(element, index, array) {
																	if(element[0] == app.selectUnidadEjecutora){
																		detallesAgr.push(element[1].toLowerCase());
																	}
																});
																detallesAgr.push(this.selectMedioPago);
																detallesAgr.push(this.selectVigenciaGasto);
																this.vigenciasdelgasto.forEach(function logArrayElements(element, index, array) {
																	if(element[0] == app.selectVigenciaGasto){
																		detallesAgr.push(element[2].toLowerCase());
																	}
																});
																detallesAgr.push('');
																detallesAgr.push('');
																detallesAgr.push('');
																detallesAgr.push('');
																detallesAgr.push(this.cuenta);
																detallesAgr.push(this.nombreCuenta.toLowerCase().slice(0,30));
																detallesAgr.push(this.fuente);
																detallesAgr.push(this.nombreFuente.toLowerCase().slice(0,30));
																detallesAgr.push(this.codigoCpc);
																detallesAgr.push(this.nombreCodigoCpc.toLowerCase().slice(0,15));
																detallesAgr.push(this.divipola);
																detallesAgr.push(this.chip);
																detallesAgr.push(this.valor);
																let existeDetalle = 0;
																this.detalles.forEach(function(elemento){
																	result = app.comparaArrays(elemento, detallesAgr);
																	if(result){
																		existeDetalle = 1;
																		return false;
																	}
																});
																if(existeDetalle == 0){
																	this.detalles.push(detallesAgr);
																	this.cuenta = '';
																	this.codProyecto = '';
																	this.nombreProyecto = '';
																	this.programatico = '';
																	this.nombreProgramatico = '';
																	this.nombreCuenta = '';
																	this.fuente = '';
																	this.nombreFuente = '';
																	this.divipola = '';
																	this.nombreDivipola = '';
																	this.codigoCpc = '';
																	this.nombreCodigoCpc = '';
																	this.chip = '';
																	this.nombreChip = '';
																	this.totalCdp = parseFloat(this.totalCdp) + parseFloat(this.valor);
																	this.valor = 0;
																	this.saldo = 0;
																	this.cargarTiposDeGasto();
																	this.tieneCpc();
																}else{
																	Swal.fire(
																		'Detalle repetido.',
																		'Debe tener el rubro o alguno de sus clasificadores diferentes a los detalles que se han agregado.',
																		'warning'
																	);
																}
															}
															else{
																Swal.fire(
																	'Falta informaci&oacute;n.',
																	'Falta escoger el c&oacute;digo CPC.',
																	'warning'
																);
															}
														}else{
															var detallesAgr = [];
															this.tiposDeGasto.forEach(function logArrayElements(element, index, array) {
																if(element[0] == app.tipoGasto){
																	detallesAgr.push(element[1]);
																}
															});
															detallesAgr.push(this.selectUnidadEjecutora);
															this.unidadesejecutoras.forEach(function logArrayElements(element, index, array) {
																//console.log("a[" + index + "] = " + element);
																if(element[0] == app.selectUnidadEjecutora){
																	detallesAgr.push(element[1].toLowerCase());
																}
															});
															detallesAgr.push(this.selectMedioPago);
															detallesAgr.push(this.selectVigenciaGasto);
															this.vigenciasdelgasto.forEach(function logArrayElements(element, index, array) {
																if(element[0] == app.selectVigenciaGasto){
																	detallesAgr.push(element[2].toLowerCase());
																}
															});
															detallesAgr.push('');
															detallesAgr.push('');
															detallesAgr.push('');
															detallesAgr.push('');
															detallesAgr.push(this.cuenta);
															detallesAgr.push(this.nombreCuenta.toLowerCase().slice(0,30));
															detallesAgr.push(this.fuente);
															detallesAgr.push(this.nombreFuente.toLowerCase().slice(0,30));
															detallesAgr.push(this.codigoCpc);
															detallesAgr.push(this.nombreCodigoCpc.toLowerCase().slice(0,15));
															detallesAgr.push(this.divipola);
															detallesAgr.push(this.chip);
															detallesAgr.push(this.valor);
															let existeDetalle = 0;
															this.detalles.forEach(function(elemento){
																result = app.comparaArrays(elemento, detallesAgr);
																if(result){
																	existeDetalle = 1;
																	return false;
																}
															});
															if(existeDetalle == 0){
																this.detalles.push(detallesAgr);
																this.cuenta = '';
																this.codProyecto = '';
																this.nombreProyecto = '';
																this.programatico = '';
																this.nombreProgramatico = '';
																this.nombreCuenta = '';
																this.fuente = '';
																this.nombreFuente = '';
																this.divipola = '';
																this.nombreDivipola = '';
																this.codigoCpc = '';
																this.nombreCodigoCpc = '';
																this.chip = '';
																this.nombreChip = '';
																this.totalCdp = parseFloat(this.totalCdp) + parseFloat(this.valor);
																this.valor = 0;
																this.saldo = 0;
																this.cargarTiposDeGasto();
																this.tieneCpc();
															}
															else{
																Swal.fire(
																	'Detalle repetido.',
																	'Debe tener el rubro o alguno de sus clasificadores diferentes a los detalles que se han agregado.',
																	'warning'
																);
															}
														}
													}
													else{
														Swal.fire(
															'Falta informaci&oacute;n.',
															'Falta escoger el c&oacute;digo CHIP.',
															'warning'
														);
													}
												}
												else{
													Swal.fire(
														'Falta informaci&oacute;n.',
														'Falta escoger el c&oacute;digo DIVIPOLA.',
														'warning'
													);
												}
											}
										}
										else{
											Swal.fire(
												'No tiene este saldo disponible.',
												'El valor no puede ser mayor al saldo disponible.',
												'warning'
											);
										}
									}
									else{
										Swal.fire(
											'Falta informaci&oacute;n.',
											'Falta digitar el valor.',
											'warning'
										);
									}
								}
								else{
									Swal.fire(
										'Falta informaci&oacute;n.',
										'Falta escoger la fuente.',
										'warning'
									);
								}
							}
							else{
								Swal.fire(
									'Falta informaci&oacute;n.',
									'Falta escoger la cuenta presupuestal CCPET.',
									'warning'
								);
							}
						}
						else{
							Swal.fire(
								'Falta informaci&oacute;n.',
								'Falta escoger la vigencia del gasto.',
								'warning'
							);
						}
					}
					else{
						Swal.fire(
							'Falta informaci&oacute;n.',
							'Falta escoger el medio de pago.',
							'warning'
						);
					}
				}
				else{
					Swal.fire(
						'Falta informaci&oacute;n.',
						'Falta escoger la secci&oacute;n presupuestal.',
						'warning'
					);
				}
			}else{
				Swal.fire(
					'Falta informaci&oacute;n.',
					'Falta escoger el tipo de gasto.',
					'warning'
				);
			}
		},
		comparaArrays: function(array1, array2){
			array1 = array1.slice(0,-3);
			array2 = array2.slice(0,-3);
			return Array.isArray(array1) && Array.isArray(array2) && array1.length === array2.length && array1.every((val, index) => val === array2[index]);
		},
		eliminarDetalle: function(item){
			this.totalCdp = parseFloat(this.totalCdp) - parseFloat(item[18]);
			var i = this.detalles.indexOf( item );
			if ( i !== -1 ) {
				this.detalles.splice( i, 1 );
			}
			this.parametrosCompletos();
		},
		searchMonitorCuenta: function(){
			var keywordCuenta = app.toFormData(app.searchCuenta);
			axios.post('vue/presupuesto_ccp/cp-cdpnomina2022.php?action=filtrarCuentas&inicioCuenta=' + this.tiposDeGasto[this.tipoGasto-1][2], keywordCuenta)
			.then((response) => {   
				app.cuentasCcpet = response.data.cuentasCcpet;
			});
		},
		searchMonitorChip: function(){
			var keywordChip = app.toFormData(app.searchChip);
			axios.post('vue/presupuesto_ccp/cp-cdpnomina2022.php?action=filtrarChips', keywordChip)
			.then(
				(response) => {   
					app.chips = response.data.chips;
				}
			);
		},
		searchMonitorProyecto: function(){
			var keywordProyecto = app.toFormData(app.searchProyecto);
			axios.post('vue/presupuesto_ccp/cp-cdpnomina2022.php?action=filtrarProyectos&vigencia=' + this.vigencia, keywordProyecto)
			.then(
				(response) => {
					app.proyectos = response.data.proyectosCcpet;
				}
			);
		},
		searchMonitorFuente: function()
		{
			var keywordFuente = app.toFormData(app.searchFuente);
			axios.post('vue/presupuesto_ccp/cp-cdpnomina2022.php?action=filtrarFuentes', keywordFuente)
			.then(
				(response) => {   
					app.fuentesCuipo = response.data.fuentes;
				}
			);
		},
		seleccioncarConsecutivo: function(){
			axios.post('vue/presupuesto_ccp/ccp-cdpnomina2022.php?action=seleccionarConsecutivo&vigencia=' + this.vigencia)
			.then(
				(response) => {
					app.consecutivo = response.data.consecutivo;
				}
			);
		},
		cambiaTipoDeGasto: function(){
			this.cuenta = '';
			this.nombreCuenta = '';
			this.fuente = '';
			this.nombreFuente = '';
			this.saldo = '';
			this.valor = '';
			this.programatico = '';
			this.nombreProgramatico = '';
			this.codProyecto = '';
			this.nombreProyecto = '';
			if(this.tiposDeGasto[this.tipoGasto-1][2] == 2.3){
				this.mostrarInversion = true;
				this.mostrarFuncionamiento = false;
			}
			else{
				this.mostrarFuncionamiento = true;
				this.mostrarInversion = false;
			}
			this.parametrosCompletos();
		},
		toFormData: function(obj){
			var form_data = new FormData();
			for(var key in obj){
				form_data.append(key, obj[key]);
			}
			return form_data;
		},
		//Revisar si existen todos los parametros
		parametrosCompletos: function(){
			if(this.tipoGasto == 3){
				if(this.codProyecto != '' &&this.programatico != ''  && this.fuente != '' && this.vigencia != '' && this.tipoGasto != '' && this.selectUnidadEjecutora != '' && this.selectMedioPago != '' && this.selectVigenciaGasto != ''){
					this.calcularSaldoInversion();
					this.tieneCpc();
				}
				else {this.saldo = 0;}
			}
			else{
				if(this.cuenta != '' && this.fuente != '' && this.vigencia != '' && this.tipoGasto != '' && this.selectUnidadEjecutora != '' && this.selectMedioPago != '' && this.selectVigenciaGasto != ''){
					this.calcularSaldo();
					this.tieneCpc();
				}
				else {this.saldo = 0;}
			}
		},
		tieneCpc(){
			if(this.cuenta != ''){
				axios.post('vue/presupuesto_ccp/cp-cdpnomina2022.php?action=tieneCpc&cuenta=' + this.cuenta)
				.then(
					(response) => {
						if(response.data.tieneCpc == null){app.cuentaConCpc = '';}
						else {app.cuentaConCpc = response.data.tieneCpc;}
					}
				);
			}
			else {this.cuentaConCpc = '';}
		},
		//Calcular el saldo de la cuenta
		calcularSaldo: async function(){
			var formData = new FormData();
			//guardar cabecera
			//[rubro, fuente, vigencia, tipo_gasto, seccion_presupuestal, medio_pago, vigencia_gasto]
			var unioninfo = this.cuenta+"<->"+this.fuente+"<->"+this.vigencia+"<->"+this.tipoGasto+"<->"+this.selectUnidadEjecutora+"<->"+this.selectMedioPago+"<->"+this.selectVigenciaGasto;
			alert(unioninfo);
			formData.append("rubro",this.cuenta);
			formData.append("fuente", this.fuente);
			formData.append("vigencia", this.vigencia);
			formData.append("tipo_gasto", this.tipoGasto);
			formData.append("seccion_presupuestal", this.selectUnidadEjecutora);
			formData.append("medio_pago", this.selectMedioPago);
			formData.append("vigencia_gasto", this.selectVigenciaGasto);
			await axios.post('vue/presupuesto_ccp/cp-cdpnomina2022.php?action=saldoPorCuenta', formData)
					.then((response) => {
					app.saldo = response.data.saldoPorCuenta;
					});
			this.calcularSaldoConDetalles();
		},
		calcularSaldoConDetalles: function(){
			this.detalles.forEach(
				function(elemento){
					if(app.cuenta == elemento[10] && app.fuente == elemento[12] && app.selectUnidadEjecutora == elemento[1] && app.selectMedioPago == elemento[3] && app.selectVigenciaGasto == elemento[4]){app.saldo -= elemento[18];}
				}
			);
		},
		calcularSaldoInversion: async function(){
			var formData = new FormData();
			//guardar cabecera
			//[rubro, fuente, vigencia, tipo_gasto, seccion_presupuestal, medio_pago, vigencia_gasto]
			formData.append("codProyecto",this.codProyecto);
			formData.append("programatico",this.programatico);
			formData.append("fuente", this.fuente);
			formData.append("vigencia", this.vigencia);
			formData.append("tipo_gasto", this.tipoGasto);
			formData.append("seccion_presupuestal", this.selectUnidadEjecutora);
			formData.append("medio_pago", this.selectMedioPago);
			formData.append("vigencia_gasto", this.selectVigenciaGasto);
			await axios.post('vue/presupuesto_ccp/cp-cdpnomina2022.php?action=saldoPorCuenta', formData)
			.then(
				(response) => {
					app.saldo = response.data.saldoPorCuenta;
				}
			);
			this.calcularSaldoConDetallesInv();
		},
		calcularSaldoConDetallesInv: function(){
			this.detalles.forEach(
				function(elemento){
					if(app.codProyecto == elemento[6] && app.programatico == elemento[8] && app.fuente == elemento[12] && app.selectUnidadEjecutora == elemento[1] && app.selectMedioPago == elemento[3] && app.selectVigenciaGasto == elemento[4]){app.saldo -= elemento[18];}
				}
			);
		},
		buscaVentanaModal: async function(clasificador, cuentaCpc){
			switch (clasificador){
				case "2":{
					this.showModal_bienes_transportables = true;
					app.show_table_search = false;
					this.fetchMembersBienes();
					var ultimoCaracter = cuentaCpc.substr(-1,1);
					var cuentaArr = [];
					cuentaArr[0] = ultimoCaracter;
					cuentaArr[1] = "nombre";
					this.division(cuentaArr);
					app.show_table_search = true;
				}break;
				case "3":{
					this.showModal_servicios = true;
					app.show_table_search = false;
					this.fetchMembersServicios();
					var ultimoCaracter = cuentaCpc.substr(-1,1);
					var cuentaArr = [];
					cuentaArr[0] = ultimoCaracter;
					cuentaArr[1] = "nombre";
					this.divisionServicios(cuentaArr);
					app.show_table_search = true;
				}break;
			}
		},
		fetchMembersBienes: function(){
			axios.post('vue/ccp-bienestransportables.php')
			.then(
				function(response){
					app.secciones = response.data.secciones;
				}
			);
		},
		division: function(seccion, scrollArriba = false){
			this.searchDivision= {keywordDivision: ''};
			this.searchGrupo= {keywordGrupo: ''};
			this.searchClase= {keywordClase: ''};
			this.searchSubClase= {keywordSubClase: ''};
			app.divisiones = [];
			app.grupos = [];
			app.clases = [];
			app.subClases = [];
			this.division_p = '';
			this.division_p_nombre = '';
			this.grupo_p = '';
			this.grupo_p_nombre = '';
			this.clase_p = '';
			this.clase_p_nombre = '';
			//this.subClase_p = '';
			this.subClase_p_nombre = '';
			app.mostrarGrupo = false;
			app.mostrarClase = false;
			app.mostrarSubClase = false;
			app.mostrarSubClaseProducto = false;
			this.seccion_p = seccion[0];
			this.seccion_p_nombre = seccion[1];
			axios.post('vue/ccp-bienestransportables.php?seccion='+this.seccion_p)
			.then(
				function(response){
					app.divisiones = response.data.divisiones;
					if(response.data.divisiones == ''){app.mostrarDivision = false;}
					else {app.mostrarDivision = true;}
				}
			);
			if(!scrollArriba)
			{
				setTimeout(function(){ document.getElementById("end_page").scrollIntoView({block: "end", behavior: "smooth"}); }, 1)
			}
		},
		buscarGrupo: function(division, scrollArriba = false){
			this.searchGrupo= {keywordGrupo: ''};
			this.searchClase= {keywordClase: ''};
			this.searchSubClase = {keywordSubClase: ''};
			app.grupos = [];
			app.clases = [];
			app.subClases = [];
			app.mostrarSubClaseProducto = false;
			app.subClases_captura = [];
			app.mostrarClase = false;
			app.mostrarSubClase = false;
			this.grupoServicios_p = '';
			this.claseServicios_p = '';
			this.subClaseServicios_p = '';
			if(this.division_p == '' || this.division_p == division[0] || !app.mostrarGrupo){
				app.mostrarGrupo = !app.mostrarGrupo;
			}
			if(app.mostrarGrupo){
				this.division_p = division[0];
				axios.post('vue/ccp-bienestransportables.php?division='+this.division_p)
				.then(
					function(response){
						app.grupos = response.data.grupos;
						if(response.data.grupos == ''){app.mostrarGrupo = false;}
						else{app.mostrarGrupo = true;}
					}
				);
			}
			if(!scrollArriba){
				setTimeout(function(){ document.getElementById("end_page").scrollIntoView({behavior: "smooth"}); }, 1)
			}
		},
		buscarClase: function(grupo, scrollArriba = false){
			this.searchClase= {keywordClase: ''};
			this.searchSubClase= {keywordSubClase: ''};
			app.mostrarSubClaseProducto = false;
			app.subClases_captura = [];
			app.clases = [];
			app.subClases = [];
			app.mostrarSubClase = false;
			app.searchProduct = {keywordProduct: ''};
			this.clase_p = '';
			this.clase_p_nombre = '';
			this.subClase_p_nombre = '';
			if(this.grupo_p == '' || this.grupo_p == grupo[0] || !app.mostrarClase){
				app.mostrarClase = !app.mostrarClase;
			}
			if(app.mostrarClase){
				this.grupo_p = grupo[0];
				this.grupo_p_nombre = grupo[1];
				axios.post('vue/ccp-bienestransportables.php?grupo='+this.grupo_p)
				.then(
					function(response){
						app.clases = response.data.clases;
						if(response.data.clases == ''){app.mostrarClase = false;}
						else {app.mostrarClase = true;}
					}
				);
			}
			if(!scrollArriba)
			{
				setTimeout(function(){ document.getElementById("end_page").scrollIntoView({behavior: "smooth"}); }, 1)

			}
		},
		buscarSubclase: function(clase, scrollArriba = false){
			this.searchSubClase = {keywordSubClase: ''};
			app.subClases = [];
			app.mostrarSubClase = false;
			app.mostrarSubClaseProducto = false;
			app.subClases_captura = [];
			app.searchProduct = {keywordProduct: ''};
			this.subClaseServicios_p = '';
			this.subClase_p_nombre = '';
			if(this.clase_p == '' || this.clase_p == clase[0] || !app.mostrarSubClase){
				app.mostrarSubClase = !app.mostrarSubClase;
			}
			if(app.mostrarSubClase){
				this.clase_p = clase[0];
				this.clase_p_nombre = clase[1];
				axios.post('vue/ccp-bienestransportables.php?clase='+this.clase_p)
				.then(
					function(response){
						app.subClases = response.data.subClases;
						if(response.data.subClases == ''){
							app.mostrarSubClase = false;
						}
						else{
							app.mostrarSubClase = true;
						}
					}
				);
			}
			if(!scrollArriba){
				setTimeout(function(){ document.getElementById("end_page").scrollIntoView({behavior: "smooth"}); }, 1);
			}
		},
		seleccionarSublaseProducto: async function(subClase, scrollArriba = false)
		{
			this.subClase_p = subClase[0];
			this.subClase_p_nombre = subClase[1];
			await axios.post('vue/ccp-bienestransportables.php?subClase='+this.subClase_p)
			.then(
				function(response){
					app.subClases_captura = response.data.subClases_captura;
					if(response.data.subClases_captura == ''){app.mostrarSubClaseProducto = false;}
					else{app.mostrarSubClaseProducto = true;}
				}
			);
			if(!scrollArriba){
				setTimeout(function(){ document.getElementById("end_page").scrollIntoView({behavior: "smooth"}); }, 1);
			}
		},
		seleccionarBienes: function(subClase){
			this.cuentasSelectSubClase = subClase;
			this.subClase_p = subClase[0];
			this.subClase_p_nombre = subClase[1];
		},
		buscarGeneral: function(){
			if(this.cuentaActual.slice(-1)==app.searchGeneral.keywordGeneral.substr(0,1)){
				var parsedobj = JSON.parse(JSON.stringify(app.searchGeneral))
				if(parsedobj.keywordGeneral == ''){
					app.mostrarSeccion = true;
					app.show_table_search = false;
					this.fetchMembers();
				}
				else{
					app.grupos = [];
					app.clases = [];
					app.subClases = [];
					app.result_search = [];
					app.mostrarGrupo = false;
					app.mostrarClase = false;
					app.mostrarSubClase = false;
					app.mostrarSeccion = false;
					var keywordGeneral = app.toFormData(app.searchGeneral);
					axios.post('vue/ccp-bienestransportables.php?action=searchGeneral', keywordGeneral)
					.then(
						function(response){
							app.result_search = response.data.subClasesGeneral;
							if(response.data.subClasesGeneral == ''){
								app.noMember = true;
								app.show_table_search = true;
							}
							else{
								app.noMember = false;
								app.show_levels(app.result_search[0]);
								app.show_table_search = true;
							}
						}
					);
					// Enviar el scroll al final cuando ya esta definido
					setTimeout(function(){ document.getElementById("end_page").scrollIntoView({block: "end", behavior: "smooth"}); }, 1);
					app.searchGeneral.keywordGeneral = '';
				}
			}
			else{
				this.showModal_bienes_transportables = false;
				Swal.fire(
					'Error en el c&oacute;digo.',
					'Digite un c&oacute;digo que empiece con el mismo n&uacute;mero que termina la cuenta CCPET.',
					'error'
				).then(
					(result) => {
						if(result.isConfirmed || result.isDismissed){
							this.showModal_bienes_transportables = true;
						}
					}
				);
			}
		},
		buscarGeneral2: function(){
			if(this.cuentaActual.slice(-1)==app.searchGeneral2.keywordGeneral.substr(0,1)){
				var parsedobj = JSON.parse(JSON.stringify(app.searchGeneral2))
				if(parsedobj.keywordGeneral == ''){
					app.mostrarSeccion = true;
					app.show_table_search = false;
					this.fetchMembers();
				}
				else{
					app.grupos = [];
					app.clases = [];
					app.subClases = [];
					app.result_search = [];
					app.mostrarGrupo = false;
					app.mostrarClase = false;
					app.mostrarSubClase = false;
					app.mostrarSeccion = false;
					this.cuentasSelectSubClaseServicios = [];
					var keywordGeneral = app.toFormData(app.searchGeneral2);
					axios.post('vue/ccp-servicios.php?action=searchGeneral', keywordGeneral)
						.then(
							function(response){
								app.result_search = response.data.subClasesGeneral;
								if(response.data.subClasesGeneral == '')
								{
									app.noMember = true;
									app.show_table_search = true;
								}
								else
								{
									app.noMember = false;
									app.show_levels2(app.result_search[0]);
									app.show_table_search = true;
								}
							}
						);
					setTimeout(function(){ document.getElementById("end_page").scrollIntoView({block: "end", behavior: "smooth"}); }, 1);
					app.searchGeneral2.keywordGeneral = '';
				}
			}
			else
			{
				this.toggleMensaje();
				this.colortitulosmensaje='crimson';
				this.titulomensaje='Mensaje de Error';
				this.contenidomensaje='Error en el c\xf3digo, no se puede cambiar el sector';
			}
		},
		show_levels: function(codigo){
			app.mostrarSeccion = true;
			codigo_final = codigo[0];
			codigo_subClase = codigo_final.slice(0,-2);  
			codigo_Clase = codigo_subClase.slice(0,-1); 
			codigo_grupo = codigo_Clase.slice(0,-1);
			codigo_division = codigo_grupo.slice(0,-1);
			codigo_seccion = codigo_division.slice(0,-1);
			this.searchHastaSubclase(codigo_final, codigo_subClase, codigo_Clase, codigo_grupo, codigo_division, codigo_seccion, codigo[5]);            
		},
		searchHastaSubclase: async function(codigo_final, codigo_subClase, codigo_Clase, codigo_grupo, codigo_division, codigo_seccion, ud){
			app.secciones = [];
			app.divisiones = [];
			app.grupos = [];
			app.clases = [];
			app.subClases = [];
			this.seccion_general = [];
			this.division_general = [];
			this.grupo_general = [];
			this.clase_general = [];
			this.subClase_general = [];
			this.producto_general = [];
			this.seccion_p = codigo_seccion;
			this.fetchMembersBienes();
			// aqui empieza a buscar la seccion 
			this.seccion_general.push(this.seccion_p);
			this.division(this.seccion_general, false);
			// aqui termina
			//aqui empiza a buscar el nombre de la division
			this.division_general.push(codigo_division);
			this.buscarGrupo(this.division_general, false);
			//qui termina
			//aqui empiza a buscar el nombre del grupo
			this.grupo_general.push(codigo_grupo);
			this.buscarClase(this.grupo_general, false);
			//aqui termina
			//aqui empiza a buscar el nombre de la clase
			this.clase_general.push(codigo_Clase);
			this.buscarSubclase(this.clase_general, false);
			this.producto_general.push(codigo_subClase);
			this.seleccionarSublaseProducto(this.producto_general);
			var seccion_nombre_buscar =  await this.buscarNombre(codigo_final);
			this.cuentasSelectSubClase.push(codigo_final);
			this.cuentasSelectSubClase.push(seccion_nombre_buscar);
			this.cuentasSelectSubClase.push(ud);
			this.seleccionarBienes(this.cuentasSelectSubClase);
			//aqui termina
			this.searchGeneral = {keywordGeneral: ''};
		},
		searchMonitorGrupos: function(){
			var keywordGrupo = app.toFormData(app.searchGrupo);
			app.clases = [];
			app.subClases = [];
			app.mostrarClase = false;
			app.mostrarSubClase = false;
			this.searchClase= {keywordClase: ''};
			this.searchSubClase= {keywordSubClase: ''};
			axios.post('vue/ccp-bienestransportables.php?action=searchGrupo&divisionSearch='+this.division_p, keywordGrupo)
			.then(
				function(response){
					app.grupos = response.data.grupos;
					if(response.data.grupos == ''){
						app.noMember = true;
						// app.show_table_search = false
					}
					else{
						app.noMember = false;
						//app.show_table_search = true
					}
					
				}
			);
		},
		searchMonitorClases: function(){
			var keywordClase = app.toFormData(app.searchClase);
			app.subClases = [];
			app.mostrarSubClase = false;
			this.searchSubClase= {keywordSubClase: ''};
			axios.post('vue/ccp-bienestransportables.php?action=searchClase&grupoSearch='+this.grupo_p, keywordClase)
			.then(
				function(response){
					app.clases = response.data.clases;
					if(response.data.clases == ''){	app.noMember = true;}
					else{app.noMember = false;}
				}
			);
		},
		searchMonitorSubClases: function(){
			var keywordSubClase = app.toFormData(app.searchSubClase);
			axios.post('vue/ccp-bienestransportables.php?action=searchSubClase&subClaseSearch='+this.clase_p, keywordSubClase)
			.then(
				function(response){
					app.subClases = response.data.subClases;
					if(response.data.subClases == ''){app.noMember = true;}
					else{app.noMember = false;}
				}
			);
		},
		seleccionarCpc: function(){
			if(this.subClase_p != ''){
				this.codigoCpc = this.subClase_p;
				this.nombreCodigoCpc = this.subClase_p_nombre;
				this.subClase = '';
				this.subClase_p_nombre = '';
				this.showModal_bienes_transportables = false;
			}
			else{
				Swal.fire(
					'Falta informaci&oacute;n.',
					'Falta escoger el producto o servicio.',
					'warning'
				);
			}
		},
		seleccionarServicioCpc: function(){
			if(this.subClaseServicios_p != ''){
				this.codigoCpc = this.subClaseServicios_p;
				this.nombreCodigoCpc = this.subClaseServicios_p_nombre;
				this.subClase = '';
				this.subClaseServicios_p_nombre = '';
				this.showModal_servicios = false;
			}else{
				Swal.fire(
					'Falta informaci&oacute;n.',
					'Falta escoger el producto o servicio.',
					'warning'
				);
			}
		},
		fetchMembersServicios: function(){
			axios.post('vue/ccp-servicios.php')
			.then(
				function(response){
					app.seccionesServicios = response.data.secciones;
				});
		},
		divisionServicios: function(seccion, scrollArriba = false){
			this.searchDivisionServicios = {keywordDivisionServicios: ''};
			this.searchGrupoServicios = {keywordGrupoServicios: ''};
			this.searchClaseServicios = {keywordClaseServicios: ''};
			this.searchSubClaseServicios = {keywordSubClaseServicios: ''};
			app.gruposServicios = [];
			app.clasesServicios = [];
			app.subClasesServicios = [];
			this.divisionServicios_p = '';
			this.grupoServicios_p = '';
			this.claseServicios_p = '';
			this.subClaseServicios_p = '';
			app.mostrarGrupoServicios = false;
			app.mostrarClaseServicios = false;
			app.mostrarSubClaseServicios = false;
			this.seccionServicios_p = seccion[0];
			this.seccionServicios_p_nombre = seccion[1];
			axios.post('vue/ccp-servicios.php?seccion='+this.seccionServicios_p)
			.then(
				function(response){
					app.divisionesServicios = response.data.divisiones;
					if(response.data.divisiones == ''){app.mostrarDivisionServicios = false;}
					else{app.mostrarDivisionServicios = true;}
				}
			);
			if(!scrollArriba){
				setTimeout(function(){ document.getElementById("end_page_servicios").scrollIntoView({block: "end", behavior: "smooth"}); }, 1)
			}
		},
		buscarGrupoServicios: function(division, scrollArriba = false){
			this.searchGrupoServicios= {keywordGrupoServicios: ''};
			this.searchClaseServicios= {keywordClaseServicios: ''};
			this.searchSubClaseServicios = {keywordSubClaseServicios: ''};
			app.gruposServicios = [];
			app.clasesServicios = [];
			app.subClasesServicios = [];
			app.mostrarClaseServicios = false;
			app.mostrarSubClaseServicios = false;
			this.grupoServicios_p = '';
			this.claseServicios_p = '';
			this.subClaseServicios_p = '';
			if(this.divisionServicios_p == '' || this.divisionServicios_p == division[0] || !app.mostrarGrupoServicios){
				app.mostrarGrupoServicios = !app.mostrarGrupoServicios;
			}
			if(app.mostrarGrupoServicios){
				this.divisionServicios_p = division[0];
				axios.post('vue/ccp-servicios.php?division='+this.divisionServicios_p)
				.then(
					function(response){
						app.gruposServicios = response.data.grupos;
						if(response.data.grupos == ''){app.mostrarGrupoServicios = false;}
						else{app.mostrarGrupoServicios = true;}
					}
				);
			}
			if(!scrollArriba){
				setTimeout(function(){ document.getElementById("end_page_servicios").scrollIntoView({behavior: "smooth"}); }, 1)

			}
		},
		buscarClaseServicios: function(grupo, scrollArriba = false){
			this.searchClaseServicios= {keywordClaseServicios: ''};
			this.searchSubClaseServicios = {keywordSubClaseServicios: ''};
			app.clasesServicios = [];
			app.subClasesServicios = [];
			app.mostrarSubClaseServicios = false;
			this.claseServicios_p = '';
			this.subClaseServicios_p = '';
			if(this.grupoServicios_p == '' || this.grupoServicios_p == grupo[0] || !app.mostrarClaseServicios){
				app.mostrarClaseServicios = !app.mostrarClaseServicios;
			}
			if(app.mostrarClaseServicios){
				this.grupoServicios_p = grupo[0];
				axios.post('vue/ccp-servicios.php?grupo='+this.grupoServicios_p)
				.then(
					function(response){
						app.clasesServicios = response.data.clases;
						if(response.data.clases == ''){app.mostrarClaseServicios = false;}
						else{app.mostrarClaseServicios = true;}
					}
				);
			}
			if(!scrollArriba){
				setTimeout(function(){ document.getElementById("end_page_servicios").scrollIntoView({behavior: "smooth"}); }, 1);
			}
		},
		buscarSubclaseServicios: function(clase, scrollArriba = false){
			this.searchSubClaseServicios = {keywordSubClaseServicios: ''};
			app.subClasesServicios = [];
			this.subClaseServicios_p = '';
			if(this.claseServicios_p == '' || this.claseServicios_p == clase[0] || !app.mostrarSubClaseServicios){
				app.mostrarSubClaseServicios = !app.mostrarSubClaseServicios;
			}
			
			if(app.mostrarSubClaseServicios){
				this.claseServicios_p = clase[0];
				axios.post('vue/ccp-servicios.php?clase='+this.claseServicios_p)
				.then(
					function(response){
						app.subClasesServicios = response.data.subClases;
						if(response.data.subClases == ''){
							app.mostrarSubClaseServicios = false;
						}
						else{
							app.mostrarSubClaseServicios = true;
						}
					}
				);
			}
			if(!scrollArriba){
				setTimeout(function(){ document.getElementById("end_page_servicios").scrollIntoView({behavior: "smooth"}); }, 1);

			}
		},
		seleccionarServicios: function(subClase){
			this.cuentasSelectSubClaseServicios = subClase;
			this.subClaseServicios_p = subClase[0];
			this.subClaseServicios_p_nombre = subClase[1];
		},
		searchHastaSubclase2: async function(codigo_final, codigo_subClase, codigo_Clase, codigo_grupo, codigo_division, codigo_seccion){
			app.secciones = [];
			app.divisiones = [];
			app.grupos = [];
			app.clases = [];
			app.subClases = [];
			this.seccion_general = [];
			this.division_general = [];
			this.grupo_general = [];
			this.clase_general = [];
			this.subClase_general = [];
			this.seccion_p = codigo_seccion;
			// aqui empieza a buscar la seccion 
			this.seccion_general.push(this.seccion_p);
			this.divisionServicios(this.seccion_general, false);
			// aqui termina
			//aqui empiza a buscar el nombre de la division
			this.division_general.push(codigo_division);
			//console.log(this.division_general);
			this. buscarGrupoServicios(this.division_general, false);
			//qui termina
			//aqui empiza a buscar el nombre del grupo
			this.grupo_general.push(codigo_grupo);
			this.buscarClaseServicios(this.grupo_general, false);
			//aqui termina
			//aqui empiza a buscar el nombre de la clase
			this.clase_general.push(codigo_Clase);
			this.buscarSubclaseServicios(this.clase_general, false);
			this.cuentasSelectSubClaseServicios.push(codigo_final);
			this.seleccionarServicios(this.cuentasSelectSubClaseServicios);
			//aqui termina
			this.searchGeneral = {keywordGeneral: ''};
		},
		calcularSaldoNuevo: async function(cuenta, fuente , vigencia, tgastos, secpresu, mpago, vgasto, valpago, posvar){
			var saldolocal = 0;
			var formData = new FormData();
			formData.append("rubro",cuenta);
			formData.append("fuente", fuente);
			formData.append("vigencia", vigencia);
			formData.append("tipo_gasto", tgastos);
			formData.append("seccion_presupuestal", secpresu);
			formData.append("medio_pago", mpago);
			formData.append("vigencia_gasto", vgasto);
			await axios.post('vue/presupuesto_ccp/ccp-cdpnomina2022.php?action=saldoPorCuenta', formData)
			.then(
				(response) => {
					saldolocal = response.data.saldoPorCuenta - valpago;
				}
			);
			app.saldonuevo[posvar] = parseInt(saldolocal);
		},
		calcularSaldoInversionnuevo: async function(codproy, codprogra, fuente, vigencia, tgasto, secpresu, mpago, vgasto, valpago, posvar)
		{
			var saldolocal = 0;
			var formData = new FormData();
			formData.append("codProyecto",codproy);
			formData.append("programatico",codprogra);
			formData.append("fuente", fuente);
			formData.append("vigencia", vigencia);
			formData.append("tipo_gasto", tgasto);
			formData.append("seccion_presupuestal", secpresu);
			formData.append("medio_pago", mpago);
			formData.append("vigencia_gasto", vgasto);
			await axios.post('vue/presupuesto_ccp/ccp-cdpnomina2022.php?action=saldoPorCuenta', formData)
			.then(
				(response) => {
					saldolocal = response.data.saldoPorCuenta - valpago;
				}
			);
			app.saldonuevo[posvar] = parseInt(saldolocal);
			//this.calcularSaldoConDetallesInv();
		},
		//guardar cdp
		guardarCdp: function(){
			var contadoir = 0;
			var cuentasrojas = ''
			for(let x=0; x <= this.detalles.length-1; x++){
				if(app.saldonuevo[x] < 0){
					contadoir++;
					if(contadoir == 1){cuentasrojas = this.detalles[x][1];}
					else {cuentasrojas += ' - ' + this.detalles[x][1];}
				}
			}
			if (contadoir>0){
				if (contadoir == 1){
					Swal.fire(
						'Error!',
						'La cuenta: ' + cuentasrojas + ' No tiene saldo disponible',
						'warning'
					);
				}
				else{
					Swal.fire(
						'Error!',
						'Las cuentas: ' + cuentasrojas + ' No tienen saldo disponible',
						'warning'
					);
				}
				
			}
			else if(document.getElementById('fecha').value != '' && this.objeto != '' && this.consecutivo != '' && this.detalles.length > 0)
			{
				Swal.fire(
					{
						title: 'Esta seguro de guardar?',
						text: "Guardar cdp en la base de datos, confirmar campos!",
						icon: 'question',
						showCancelButton: true,
						confirmButtonColor: '#3085d6',
						cancelButtonColor: '#d33',
						confirmButtonText: 'Si, guardar!'
					}
				).then(
					(result) => {
						if (result.isConfirmed){
							var formData = new FormData();
							for(let i=0; i <= this.detalles.length-1; i++){
								const val = Object.values(this.detalles[i]).length;
								for(let x = 0; x <= val; x++)
								{
									formData.append("detallesCdp["+i+"][]", Object.values(this.detalles[i])[x]);
								}
							}
							formData.append("consecutivo", this.consecutivo);
							formData.append("vigencia", this.vigencia);
							formData.append("fecha", document.getElementById('fecha').value);
							formData.append("solicita", this.solicita);
							formData.append("objeto", this.objeto);
							formData.append("totalCdp", this.totalCdp);
							formData.append("tipoDeMovimiento", this.tipoMovimiento);
							formData.append("nomina", this.numeronomina);
							axios.post('vue/presupuesto_ccp/ccp-cdpnomina2022.php?action=guardarCdp', formData)
							.then(
								(response) => {
									if(response.data.insertaBien){
										Swal.fire(
											{
												position: 'top-end',
												icon: 'success',
												title: 'El cdp se guard&oacute; con Exito',
												showConfirmButton: false,
												timer: 1500
											}
										).then(
											(response) => {
												app.redireccionar();
											}
										);
									}
									else{
										Swal.fire(
											'Error!',
											'No se pudo guardar.',
											'error'
										);
									}
								}
							);
						}
					}
				);
			}
			else
			{
				Swal.fire(
					'Falta informaci&oacute;n para guardar el cdp.',
					'Verifique que todos los campos esten diligenciados.',
					'warning'
				);
			}
		},
		redireccionar: function(){
			const  cdp = this.consecutivo;
			const  vig = this.vigencia;
			location.href ="ccp-cdpVisualizar.php?is="+cdp+"&vig="+vig;
		},
	}
});