<?php

use Illuminate\Support\Arr;

    require '../comun.inc';
    require '../funciones.inc';

    $linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");

    $out = array('error' => false);

    $action = "show";

    if(isset($_GET['action'])){
        $action = $_GET['action'];
    }

    if($action == 'show')
    {
        
    }

    if ($action == 'informeTrazabilidad') {

        preg_match("/([0-9]{2})\/([0-9]{2})\/([0-9]{4})/", $_GET['fechaIni'], $f);
		$fechaInicial = $f[3]."-".$f[2]."-".$f[1];
        $vigencia = $f[3];
        preg_match("/([0-9]{2})\/([0-9]{2})\/([0-9]{4})/", $_GET['fechaFin'], $f);
		$fechaFinal = $f[3]."-".$f[2]."-".$f[1];
        $datosDestino = array();
        $rps = array();

        $sqlDestinoCompra = "SELECT id, fecha FROM ccpetdc WHERE fecha BETWEEN '$fechaInicial' AND '$fechaFinal' AND estado = 'S' ORDER BY id ASC";
        $resDestinoCompra = mysqli_query($linkbd, $sqlDestinoCompra);
        while ($rowDestinoCompra = mysqli_fetch_row($resDestinoCompra)) {
            
            $datos = array();

            $sqlDestinoCompraDet = "SELECT SUM(valor), idrp FROM ccpetdc_detalle WHERE consvigencia = $rowDestinoCompra[0] AND destino_compra = '01'";
            $rowDestinoCompraDet = mysqli_fetch_row(mysqli_query($linkbd, $sqlDestinoCompraDet));

            if ($rowDestinoCompraDet[0] != '') {
                
                array_push($datos, $rowDestinoCompra[0]);
                array_push($datos, $rowDestinoCompra[1]);
                array_push($datos, $rowDestinoCompraDet[1]);
                $rps[] = $rowDestinoCompraDet[1];
                array_push($datos, number_format($rowDestinoCompraDet[0], 2));
                array_push($datosDestino, $datos);
            }
        }

        $datosOrdenPago = array();
        $datosEntrada = array();

        foreach ($rps as $rp) {

            $valortotalOrden = 0;
            $valortotalEntrada = 0;

            $sqlOrdenPago = "SELECT id_orden, fecha, valorpagar FROM tesoordenpago WHERE id_rp = $rp AND fecha BETWEEN '$fechaInicial' AND '$fechaFinal' ORDER BY fecha ASC ";
            $resOrdenPago = mysqli_query($linkbd, $sqlOrdenPago);
            while ($rowOrdenPago = mysqli_fetch_row($resOrdenPago)) {

                $datos = array();

                $valor = 0;
                $valor = $rowOrdenPago[2];
                $valortotalOrden += $valor;

                array_push($datos, $rp);
                array_push($datos, $rowOrdenPago[0]);
                array_push($datos, $rowOrdenPago[1]);
                array_push($datos, number_format($valor, 2));
                array_push($datosOrdenPago, $datos);
            }    

            $datos3 = array();

            array_push($datos3, $rp);
            array_push($datos3, "TOTAL: ");
            array_push($datos3, "---------------");
            array_push($datos3, number_format($valortotalOrden, 2));
            array_push($datosOrdenPago, $datos3);

            $sqlEntrada = "SELECT consec, fecha, valortotal  FROM almginventario WHERE codmov = $rp AND vigenciadoc = '$vigencia' AND tipomov = 1 AND (tiporeg = '01' OR tiporeg = '02' OR tiporeg = '03' OR tiporeg = '04' OR tiporeg = '05' OR tiporeg = '06') ORDER BY consec ASC";
            $resEntrada = mysqli_query($linkbd, $sqlEntrada);
            while ($rowEntrada = mysqli_fetch_row($resEntrada)) {
                
                $datos2 = array();

                $valortotalEntrada += $rowEntrada[2];

                array_push($datos2, $rp);
                array_push($datos2, $rowEntrada[0]);
                array_push($datos2, $rowEntrada[1]);
                array_push($datos2, number_format($rowEntrada[2], 2));
                array_push($datosEntrada, $datos2);
            }

            $datos4 = array();

            $valortotalEntrada += $rowEntrada[2];

            array_push($datos4, $rp);
            array_push($datos4, "TOTAL: ");
            array_push($datos4, "---------------");
            array_push($datos4, number_format($valortotalEntrada, 2));
            array_push($datosEntrada, $datos4);
        }

        //var_dump($datosEntrada);
        $out['destinoCompra'] = $datosDestino;
        $out['ordenPagos'] = $datosOrdenPago;
        $out['entradas'] = $datosEntrada;
    }

    if ($action == 'buscarEntradas') {

        preg_match("/([0-9]{2})\/([0-9]{2})\/([0-9]{4})/", $_GET['fechaIni'], $f);
		$fechaInicial = $f[3]."-".$f[2]."-".$f[1];
        $vigencia = $f[3];
        preg_match("/([0-9]{2})\/([0-9]{2})\/([0-9]{4})/", $_GET['fechaFin'], $f);
		$fechaFinal = $f[3]."-".$f[2]."-".$f[1];

        $almacen = array();
        $rps = array();
        $codigoEntrada = array();

        $sqlEntradaAlmacen = "SELECT consec, tiporeg, fecha FROM almginventario WHERE tipomov = 1 AND fecha BETWEEN '$fechaInicial' AND '$fechaFinal' ORDER BY consec ASC ";
        $resEntradaAlmacen = mysqli_query($linkbd, $sqlEntradaAlmacen);
        while ($rowEntradaAlmacen = mysqli_fetch_row($resEntradaAlmacen)) {
            
            $datos = array();

            $sqlEntradaAlmacenDet = "SELECT DISTINCT solicitud, SUM(valortotal) FROM almginventario_det WHERE codigo = $rowEntradaAlmacen[0] AND tipomov = 1 AND tiporeg = '$rowEntradaAlmacen[1]'";
            $rowEntradaAlmacenDet = mysqli_fetch_row(mysqli_query($linkbd, $sqlEntradaAlmacenDet));

            array_push($datos, $rowEntradaAlmacen[0]);
            $codigoEntrada[] = $rowEntradaAlmacen[0];
            array_push($datos, $rowEntradaAlmacen[1]);
            array_push($datos, $rowEntradaAlmacen[2]);
            array_push($datos, number_format($rowEntradaAlmacenDet[1], 2));
            array_push($datos, $rowEntradaAlmacenDet[0]);
            $rps[] = $rowEntradaAlmacenDet[0];
            array_push($almacen, $datos);
        }

        $datosDestinoCompra = array();

        foreach ($rps as $key => $rp) {
            
            $sqlDestinoCompra = "SELECT id FROM ccpetdc WHERE consvigencia = $rp AND fecha BETWEEN '$fechaInicial' AND '$fechaFinal'";
            $resDestinoCompra = mysqli_query($linkbd, $sqlDestinoCompra);
            while ($rowDestinoCompra = mysqli_fetch_row($resDestinoCompra)) {
                
                $sqlDestinoCompraDet = "SELECT cuenta, destino_compra FROM ccpetdc_detalle WHERE consvigencia = $rowDestinoCompra[0]";
                $resDestinoCompraDet = mysqli_query($linkbd, $sqlDestinoCompraDet);
                while ($rowDestinoCompraDet = mysqli_fetch_row($resDestinoCompraDet)) {
                    
                    $datos = array();

                    $sqlTipoCompra = "SELECT nombre FROM almdestinocompra WHERE codigo = '$rowDestinoCompraDet[1]'";
                    $rowTipoCompra = mysqli_fetch_row(mysqli_query($linkbd, $sqlTipoCompra));

                    array_push($datos, $codigoEntrada[$key]);
                    array_push($datos, $rowDestinoCompra[0]);
                    array_push($datos, $rowDestinoCompraDet[0]);
                    array_push($datos, $rowTipoCompra[0]);
                    array_push($datosDestinoCompra, $datos);
                }
            }
        }

        $out['almacen'] = $almacen;
        $out['datosDestinoCompra'] = $datosDestinoCompra;
    }

    header("Content-type: application/json");
    echo json_encode($out);
    die();