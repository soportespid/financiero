const URL = "vue/inve-salidaDirecta.php";
import { filtroEnArrayDeObjetos } from './../funciones.js';

var app = new Vue({
    el: '#myapp',
	data:{

        consecutivo: '',
        descripcion: '',
        tipoMovimiento: '2',
        bodega: '',
        bodegas: [],
        cc: '',
        centroCostos: [],
        articulo: '',
        nombreArticulo: '',
        unidadMedida: '',
        cantidadBodega: '',
        cantidadSalida: '',
        destinoSalida: '',
        cuentaContable: '',
        cuenta: '',
        ccDestino : '',
        showModal_articulos: false,
        showModal_cuentas: false,
        cuentas: [],
        posicionActual: '',
        listadoArticulos:  [],
        listadoArticuloscopy:  [],
        datosUsuarios: [],
        detalleInventario: [],
        ccArticulo: '',
        bodegaArticulo: '',
        valorUnitario: '',
        codunspsc: '',
        cuentaContableArticulo: '',
        valorTotal: 0,
        searchArticulo : '',
        articulo2: '',
        error: '',
        loading: false,
        sector_cuentas: false,
        sector: '',
        sectores: [],
        contrucciones: false,
        descripcionOrdenConstruccion: '',
        ordenContruccion: '',
        montaje: false,
        ordenMontaje: '',
        descripcionOrdenMontaje: '',
        showModal_contrucciones: false,
        todasContrucciones: [],
        showModal_montajes: false,
        todosMontajes: [],

        documento: '',
        valorDocumento: '',
        showModalSalidas: false,
        salidas: [],
        salidas2: [],
        searchSalida: '',
        listaArticulos: [],
        totalValores: 0,
    },

    mounted: function(){

        this.valoresIniciales();
        this.cargarFecha();
        this.buscaConsecutivo();
    },

    methods: 
    {
        //Desglosar array
        toFormData: function(obj){
            var form_data = new FormData();
            for(var key in obj){
                form_data.append(key, obj[key]);
            }
            return form_data;
        },

        formatonumero: function(valor){
            return new Intl.NumberFormat("en-US", {style: "currency", currency: "USD"}).format(valor);
        },

        cargarFecha: function(){
            
            const fechaAct = new Date().toJSON().slice(0,10).replace(/-/g,'/');
            const fechaArr = fechaAct.split('/');
            const fechaV = fechaArr[2]+'/'+fechaArr[1]+'/'+fechaArr[0];
            document.getElementById('fecha').value = fechaV;
            document.getElementById('vigencia').value = fechaArr[0];
        },

        valoresIniciales: async function()
        {
            await axios.post('vue/inve-salidaDirecta.php')
            .then((response) => {
                
                app.bodegas = response.data.bodegas;
                app.centroCostos = response.data.centroCostos;
                app.sectores = response.data.sectores;
            });
        },

        buscaConsecutivo: function() {
            axios.post('vue/inve-salidaDirecta.php?action=consecutivo&movimiento='+this.tipoMovimiento)
            .then((response) => {
               app.consecutivo = response.data.consecutivo;
                
            });
        },

        ventanaArticulo: async function() {

            if (this.bodega != '' && this.cc != '') {
                this.loading = true;
                await axios.post('vue/inve-salidaDirecta.php?action=listaArticulos&bodega='+this.bodega+'&cc='+this.cc)
                .then((response) => {
                    app.listadoArticulos = response.data.articulos;
                    app.listadoArticuloscopy = response.data.articulos;
                }).catch((error) => {
                    this.error = true;
                    console.log(error)
                }).finally(() => {
                    this.loading =  false;
                    this.showModal_articulos = true;
                });     
                this.cantidadSalida = '';
            }
            else {
                Swal.fire("Error", "Debes seleccionar la bodega y centro de costos", "error");
            }
        },

        buscarArticulo: async function() {

            // if (this.bodega != '' && this.cc != '' && this.articulo != '') {

            //     let grupo = this.articulo.slice(0,4);
            //     let codigo = this.articulo.slice(4,9);
            //     //alert(grupo);

            //     // this.loading = true;
            //     // await axios.post('vue/inve-salidaDirecta.php?action=buscarArticulos&bodega='+this.bodega+'&cc='+this.cc+'&articulo='+this.articulo)
            //     // .then((response) => {
            //     //     console.log(response.data);

            //     // }).catch((error) => {
            //     //     this.error = true;
            //     //     console.log(error)
            //     // }).finally(() => {
            //     //     this.loading =  false;
            //     // });     

            //     // this.cantidadSalida = '';
            // }
            // else {
            //     Swal.fire("Error", "Debes seleccionar la bodega y centro de costos", "error");
            // }
        },

        seleccionaArticulo: function(articulo) {

            this.articulo = articulo[0];
            this.articulo2 = articulo[0];
            this.nombreArticulo = articulo[1];
            this.unidadMedida = articulo[4];
            this.cantidadBodega = articulo[3];
            this.showModal_articulos = false;
            this.ccArticulo = articulo[6];
            this.bodegaArticulo = articulo[5];
            this.valorUnitario = articulo[7];
            this.codunspsc = articulo[8];
            this.cuentaContableArticulo = articulo[9];
            this.ccDestino = articulo[6];
        },

        searchCodigoArticulos: async function() {
            let search = "";
            search = this.searchArticulo.toLowerCase();
            this.listadoArticulos = [...this.listadoArticuloscopy.filter(e=>e[0].toLowerCase().includes(search) || e[1].toLowerCase().includes(search) )];

            const data = this.listadoArticuloscopy;
            var text = this.searchArticulo;
            var resultado = [];
            
            resultado = await filtroEnArrayDeObjetos({'data': data, 'text': text});

            this.listadoArticulos = resultado;
        },

        validaCantidadSalida: function() {

            if (this.articulo2 != '') {

                var cantidadArticuloSalida = 0;
                var cantidadSalida = parseInt(this.cantidadSalida);

                for (let i = 0; i < app.detalleInventario.length; i++) {
                    
                    if (this.detalleInventario[i][0] == this.articulo2) {

                        cantidadArticuloSalida += parseInt(this.detalleInventario[i][3]);
                    }
                }

                cantidadArticuloSalida = parseInt(cantidadArticuloSalida) + parseInt(cantidadSalida);

                if (cantidadArticuloSalida > this.cantidadBodega || this.cantidadSalida <= 0 ) {

                    this.cantidadSalida = '';
                    Swal.fire("Error", "Error en la cantidad de salida", "error");
                }
            }
            else {
                this.cantidadSalida = '';
                Swal.fire("Error", "Seleccione un articulo", "error");
            }
            
        },

        validaDestino: function() {
    
            if (this.cc != '') {

                switch (this.destinoSalida) {
                    case 'AI':
                        this.sector_cuentas = true;
                        this.contrucciones = false;
                        this.montaje = false;
                        this.cuentaContable = '';   
                        this.cuenta = '';
                        break;
                
                    case 'ACC':
                        this.contrucciones = true;
                        this.sector_cuentas = false;
                        this.montaje = false;
                        this.cuentaContable = '';
                        this.cuenta = '';
                        break;

                    case 'AM':
                        this.montaje = true;
                        this.contrucciones = false;
                        this.sector_cuentas = false;
                        this.cuentaContable = '';
                        this.cuenta = '';
                        break;

                    default:
                        this.sector_cuentas = false;
                        this.contrucciones = false,
                        this.montaje = false,
                        this.sector = '';
                        this.cuentaContable = '';
                        this.cuenta = '';
                        break;
                }
            }
            else {
                Swal.fire("Error", "Debe seleccionar un centro de costos", "error");
                this.destinoSalida = '';
            }
            
        },

        modalContrucciones: async function() {
            
            await axios.post('vue/inve-salidaDirecta.php?action=buscarContrucciones')
            .then((response) => {
                //console.log(response.data);
                app.todasContrucciones = response.data.contrucciones;
            }).catch((error) => {
                this.error = true;
                console.log(error)
            }).finally(() => {
                
            });  
        },

        desplegaModalContrucciones: function() {

            this.modalContrucciones();
            this.showModal_contrucciones = true;
        },

        seleccionaConstruccion: function(contruccion) {

            this.cuentaContable = contruccion[0] + " - " + contruccion[2];
            this.cuenta = contruccion[3];
            this.showModal_contrucciones = false;
        },

        modalMontajes: async function() {

            await axios.post('vue/inve-salidaDirecta.php?action=buscarMontajes')
            .then((response) => {
                //console.log(response.data);
                app.todosMontajes = response.data.montajes;
            }).catch((error) => {
                this.error = true;
                console.log(error)
            }).finally(() => {
                
            });  
        },

        desplegaModalMontajes: function() {

            this.modalMontajes();
            this.showModal_montajes = true;
        },

        seleccionaMontaje: function(montaje) {
            this.cuentaContable = montaje[0] + " - " + montaje[2];
            this.cuenta = montaje[4];
            this.showModal_montajes = false;
        },

        ventanaCuentaContable: async function() {

            if (this.destinoSalida != '') {

                //Destino de salida gasto, costo y ppp
                if (this.destinoSalida == 'AG' || this.destinoSalida == 'AC' || this.destinoSalida == 'AP') {

                    this.loading = true;

                    await axios.post('vue/inve-salidaDirecta.php?action=buscarCuentasDebito&destino='+this.destinoSalida+'&cc='+this.cc)
                    .then((response) => {
                        app.cuentas = response.data.cuentas;
                    }).catch((error) => {
                        this.error = true;
                        console.log(error)
                    }).finally(() => {
                        this.loading = false;
                        this.showModal_cuentas = true;        
                    });  
                }
                   
                //Destino de salida inversión
                if (this.destinoSalida == 'AI') {

                    if (this.sector != '') {

                        this.loading = true;

                        await axios.post('vue/inve-salidaDirecta.php?action=buscarCuentasDebito&destino='+this.destinoSalida+'&cc='+this.cc+'&sector='+this.sector)
                        .then((response) => {
                            app.cuentas = response.data.cuentas;
                        }).catch((error) => {
                            this.error = true;
                            console.log(error)
                        }).finally(() => {
                            this.loading = false;
                            this.showModal_cuentas = true;        
                        });  
                    }
                    else {
                        Swal.fire("Error", "Debe seleccionar un sector", "error");
                    }
                }
            }
            else {
                Swal.fire("Error", "Debes seleccionar un destino de salida", "error");
            }
        },

        seleccionaCuenta: function(cuenta) {
            
            this.cuentaContable = cuenta[0]+" - " +cuenta[1];
            this.cuenta = cuenta[2];
            this.showModal_cuentas = false;   
        },

        agregararticulo: function() {

            if (this.articulo2 != '' 
                && this.nombreArticulo != ''
                && this.unidadMedida != '' 
                && this.cantidadBodega 
                && this.cantidadSalida != '' 
                && this.cuentaContable != '' 
                && this.cuenta != '' 
                && this.ccDestino != ''
                && this.codunspsc != ''
                && this.cuentaContableArticulo != ''
            ) {
              
                var detalles = [];
                var valorTotal = 0;
                detalles.push(this.articulo2);
                detalles.push(this.nombreArticulo);
                detalles.push(this.cantidadBodega);
                detalles.push(this.cantidadSalida);
                detalles.push(this.unidadMedida);
                detalles.push(this.bodegaArticulo);
                detalles.push(this.ccArticulo);
                detalles.push(this.cuenta);
                detalles.push(this.cuentaContable);
                detalles.push(this.ccDestino);
                detalles.push(this.valorUnitario);
                valorTotal = this.valorUnitario * this.cantidadSalida;
                valorTotal = Math.round((valorTotal + Number.EPSILON) * 100) / 100;
                this.valorTotal += valorTotal;
                this.valorTotal = Math.round((this.valorTotal + Number.EPSILON) * 100) / 100;
                detalles.push(valorTotal);
                detalles.push(this.codunspsc);
                detalles.push(this.cuentaContableArticulo);
                this.detalleInventario.push(detalles);
                //console.log(this.detalleInventario);
                this.articulo = ''; 
                this.articulo2 = '';
                this.nombreArticulo = '';
                this.cantidadBodega = '';
                this.cantidadSalida = '';
                this.unidadMedida = '';
                this.bodegaArticulo = '';
                this.ccArticulo = '';
                this.cuenta = '';
                this.cuentaContable = '';
                this.ccDestino = '';
                this.valorUnitario = '';
                this.valorUnitario = '';
                this.codunspsc = '';
                this.cuentaContableArticulo = '';
            }
            else {
                Swal.fire("Error", "Faltan datos para poder agregar articulo", "error");
            }
        },

        eliminarArticulo: function(item) {
            var i = this.detalleInventario.indexOf( item );
            
            if ( i !== -1 ) {
                this.detalleInventario.splice( i, 1 );
            }
            
            this.valorTotal = parseFloat(this.valorTotal) - parseFloat(item[10]);
        },

        guardar: function() {

            if (this.tipoMovimiento == "2") {

                var fecha = document.getElementById('fecha').value;
                var vigencia = document.getElementById('vigencia').value;

                if (this.tipoMovimiento != '' && this.consecutivo != '' && fecha != '' && vigencia != '' && this.descripcion != '' && this.detalleInventario != '') {

                    Swal.fire({
                        icon: 'question',
                        title: 'Seguro que quieres guardar?',
                        showDenyButton: true,
                        confirmButtonText: 'Guardar',
                        denyButtonText: 'Cancelar',
                        }).then((result) => {
                        if (result.isConfirmed) {
        
                            var totalSalida = 0;
                            var formData = new FormData();
                            
                            formData.append("tipoMovimiento", this.tipoMovimiento);
                            formData.append("consecutivo", this.consecutivo);
                            formData.append("fecha", document.getElementById('fecha').value);
                            formData.append("vigencia", document.getElementById('vigencia').value);
                            formData.append("descripcion", this.descripcion);
                            formData.append("bodega", this.bodega);
                            
                            for(let i=0; i <= this.detalleInventario.length-1; i++) {
    
                                const val = Object.values(this.detalleInventario[i]).length-1;
        
                                for(let x = 0; x <= val; x++) {
                                    formData.append("detalles["+i+"][]", Object.values(this.detalleInventario[i])[x]);
                                }
    
                                totalSalida += this.detalleInventario[i][11];
                            }
    
                            formData.append("totalSalida", totalSalida);
    
                            axios.post('vue/inve-salidaDirecta.php?action=guardar', formData)
                            .then((response) => {
                                
                                   this.loading = false;
                        
                                    if (response.data.bloqueo) {
                                        if(response.data.insertaBien){
                                            Swal.fire({
                                                icon: 'success',
                                                title: 'Se ha guardado con exito',
                                                showConfirmButton: false,
                                                timer: 1500
                                                }).then((response) => {
                                                    app.redireccionar();
                                                });
                                        }
                                        else {
                                            Swal.fire(
                                                'Error!',
                                                'No se pudo guardar.',
                                                'error'
                                            );
                                        }
                                    }
                                    else {
                                        Swal.fire({
                                            icon: 'error',
                                            title: 'Fecha bloqueada',
                                            text: 'Tu usuario no tiene permiso para guardar el documento en esta fecha'
                                          })
                                    }
                                
                            });
                        } 
                        else if (result.isDenied) {
                            Swal.fire('Guardar cancelado', '', 'info')
                        }
                    })  
                }
                else {
                    Swal.fire("Error", "Faltan datos para poder realizar el guardado", "error");
                }
            }
            else if (this.tipoMovimiento == "4") {
                
                const fechaRev = document.getElementById('fechaRev').value;
                
                if (this.documento != "" && this.valorDocumento != "" && fechaRev != "") {

                    Swal.fire({
                        icon: 'question',
                        title: 'Esta seguro que quiere reversar?',
                        showDenyButton: true,
                        confirmButtonText: 'Reversar!',
                        denyButtonText: 'Cancelar',
                        }).then((result) => {

                        if (result.isConfirmed) {
        
                            var formData = new FormData();

                            formData.append("fechaRev", fechaRev);
                            formData.append("consecutivo", this.consecutivo);
                            formData.append("documento", this.documento);
                            formData.append("valorDocumento", this.valorDocumento);

                            for(let i=0; i <= this.listaArticulos.length-1; i++) {

                                const val = Object.values(this.listaArticulos[i]).length-1;
        
                                for(let x = 0; x <= val; x++) {
                                    formData.append("listaArticulos["+i+"][]", Object.values(this.listaArticulos[i])[x]);
                                }
                            }
                
                            axios.post(URL+'?action=guardarRev',formData)
                            .then((response) => {
                                
                                switch (response.data.respuesta) {
                                    case "0":
                                        Swal.fire({
                                            position: "top-end",
                                            icon: "success",
                                            title: "Reversado con exito",
                                            showConfirmButton: false,
                                            timer: 4500
                                        });

                                        this.redireccionar();
                                    break;

                                    case "1":
                                        Swal.fire({
                                            icon: 'warning',
                                            title: 'No es posible reversar',
                                            text: 'Fecha seleccionada bloqueada para usuario'
                                        })
                                    break;
                                
                                    default:
                                        break;
                                }
                            });
                        } 
                    })  
                }
                else {
                    Swal.fire({
                        icon: 'warning',
                        title: 'Falta información',
                        text: 'Falta información para poder reversar el documento'
                    })
                }
            }  
        },

        redireccionar: function()
        {
            location.href = "inve-salidaDirecta.php";
        },
        
        excel: function() {

            if (this.listadoArticulos != '') {

                document.form2.action="inve-disponibilidadInventarioExcel.php";
				document.form2.target="_BLANK";
				document.form2.submit(); 
            }
            else {
                Swal.fire("Faltan datos", "Genera el informe primero", "warning");
            }
        },

        buscaSalidas: async function() {

            const fechaRev = document.getElementById('fechaRev').value;

            if (fechaRev != "") {
                var formData = new FormData();
                formData.append("fechaRev", fechaRev);
    
                await axios.post(URL+'?action=buscaSalidas',formData)
                .then((response) => {
                    
                    this.salidas = response.data.salidas;
                    this.salidas2 = response.data.salidas;
                });

                this.showModalSalidas = true;
            }
            else {
                Swal.fire({
                    icon: 'warning',
                    title: 'Falta información',
                    text: 'Debe ingresar una fecha'
                })
            }
            
        },

        filtroSalidas: async function() {            

            const data = this.salidas2;
            var text = this.searchSalida;
            var resultado = [];

            resultado = await filtroEnArrayDeObjetos({'data': data, 'text': text});

            this.salidas = resultado;
        },

        seleccionaSalida: async function(salida) {

            if (salida.length != 0) {
                
                var formData = new FormData();
                formData.append("consecutivo", salida[0]);
    
                await axios.post(URL+'?action=buscaDatosSalida',formData)
                .then((response) => {

                    this.documento = salida[0];
                    this.valorDocumento = response.data.total; 
                    this.totalValores = response.data.total;
                    this.listaArticulos = response.data.listadoArticulos;
                });
                
                this.showModalSalidas = false;
            }
        },
    }
});