var app = new Vue({ 
	el: '#myapp',
	data() {
		return {
			search: {keyword: ''},
			searchProgram: {keywordProgram: ''},
			searchProduct: {keywordProduct: ''},
			sectores: [],
			programas_subprogramas: [],
			productos: [],
			showModal_programaticoProyectos: false,
			noMember: false,
		};
	},
	mounted(){
	},
	methods: {
		loadContentIntoModal(url) {
			this.showModal_programaticoProyectos = true;
			fetch(url)
			.then(response => response.text())
			.then(html => {
				document.querySelector('.modal-body').innerHTML = html;
			})
			.catch(error => {
				console.error('Error al cargar el contenido:', error);
			});
		},
	}
});