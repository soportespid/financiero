<?php
require '../comun.inc';
require '../funciones.inc';
$linkbd = conectar_v7();
$linkbd -> set_charset("utf8");

$maxVersion = ultimaVersionGastosCCPET();

$out = array('error' => false);
$codigo = "";

$action="show";

if(isset($_GET['action'])){
	$action=$_GET['action'];
}

if(isset($_GET['codigo'])){
	$codigo=$_GET['codigo'];
}

if($action=='show'){
    $sql="SELECT * FROM cuentasccpet WHERE version='$maxVersion' AND nivel=2 ORDER BY id ASC";
    $res=mysqli_query($linkbd,$sql);
    $codigos = array();

    while($row=mysqli_fetch_row($res))
    {
        array_push($codigos, $row);
    }

	$out['codigos'] = $codigos;
}


if($codigo != ""){ 
    //$padre=$_POST['padre'];
    //var_dump($padre);
    $sql="SELECT * FROM cuentasccpet WHERE version='$maxVersion' AND padre='$codigo' ORDER BY id ASC";
    $res=mysqli_query($linkbd,$sql);
    $codigos = array();

    while($row=mysqli_fetch_row($res))
    {
        array_push($codigos, $row);
    }

	$out['codigos'] = $codigos;
}

if(isset($_GET['nombre'])){
    $codigo_nivel = $_GET['nombre'];
    $sql="SELECT nombre FROM cuentasccpet WHERE version='$maxVersion' AND codigo='$codigo_nivel' ORDER BY id ASC";
    $res=mysqli_query($linkbd,$sql);
	$out['nombre'] = $row=mysqli_fetch_row($res);
}

if($action=='search'){
	$keyword=$_POST['keyword'];
	$sql="SELECT * FROM cuentasccpet WHERE version='$maxVersion' AND nombre like '%$keyword%' AND tipo like 'C' ORDER BY id ASC";
    $res=mysqli_query($linkbd,$sql);
    $codigos = array(); 

	while($row=mysqli_fetch_row($res))
    {
        array_push($codigos, $row);
    }

	$out['codigos'] = $codigos;
}

header("Content-type: application/json");
echo json_encode($out);
die();
