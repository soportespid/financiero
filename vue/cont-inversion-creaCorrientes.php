<?php 
    require '../comun.inc';
    $linkbd = conectar_v7();
    $linkbd -> set_charset("utf8");

    $out = array('error' => false);

    $action = "show";

    if($_GET['action'] != ''){
        $action = $_GET['action'];
    }

    if($action == 'show'){

        $sqlr = 'SELECT MAX(codigo) FROM conceptoscontables WHERE modulo=3 AND tipo=\'I3\' ORDER BY codigo DESC';
        $res = mysqli_query($linkbd, $sqlr);
        $row = mysqli_fetch_row($res);

        $tipo = $_GET['tipogf'];
        $conceptos = array();
        //$codigo = '';
        $codigo = $row[0]+1;

        if(strlen($codigo) === 1)
        {
            $codigo = '0'.$codigo;
        }

        $sqlr = "SELECT C.codigo, C.nombre FROM conceptoscontables C, conceptoscontableshermanos CH  WHERE C.modulo = '3' AND C.tipo='$tipo' AND C.codigo = CH.codigo  AND C.modulo = CH.modulo AND CH.tipo = C.tipo AND CH.principal='1'";

        $res = mysqli_query($linkbd, $sqlr);

        while ($row = mysqli_fetch_row($res))
        {
            array_push($conceptos, $row);
        }

        $destinoCompras = array();

        $sqlDestinoCompra = "SELECT codigo, nombre FROM almdestinocompra WHERE estado = 'S' ORDER BY codigo"; 
        $respDestinoCompra = mysqli_query($linkbd,$sqlDestinoCompra);
        while($rowDestinoCompra = mysqli_fetch_row($respDestinoCompra))
        {
            array_push($destinoCompras, $rowDestinoCompra);
        }

        $out['destinoCompras'] = $destinoCompras;

        $sqlrS = 'SELECT id_cc, nombre FROM centrocosto WHERE estado=\'S\'';

        $resS = mysqli_query($linkbd, $sqlrS);

        $centroCostos = array();

        while($rowS = mysqli_fetch_row($resS))
        {
            array_push($centroCostos, $rowS);
        }

        $vigencia=date("Y");

        $out['codigo'] = $codigo;
        $out['vigencia'] = $vigencia;
        $out['conceptos'] = $conceptos;
        $out['centroCostos'] = $centroCostos;
        
    }

    if($action == 'buscarCuentas'){

        $sqlr = 'SELECT * FROM (SELECT cn1.cuenta,cn1.nombre,cn1.naturaleza,cn1.centrocosto,cn1.tercero,cn1.tipo,cn1.estado FROM cuentasnicsp AS cn1 INNER JOIN cuentasnicsp AS cn2 ON cn2.tipo=\'Auxiliar\' AND cn2.naturaleza=\'CREDITO\' AND cn2.cuenta LIKE CONCAT( cn1.cuenta,  \'%\' ) WHERE cn1.tipo=\'Mayor\' AND cn1.naturaleza=\'CREDITO\' GROUP BY cn1.cuenta UNION SELECT cuenta,nombre,naturaleza,centrocosto,tercero,tipo,estado FROM cuentasnicsp WHERE tipo=\'Auxiliar\' AND naturaleza=\'CREDITO\') AS tabla ORDER BY 1';
        
        $res = mysqli_query($linkbd, $sqlr);

        $cuentas = array();
        while($row = mysqli_fetch_row($res))
        {
            array_push($cuentas, $row);
        }
        //var_dump($cuentas);
        $out['cuentas'] = $cuentas;
    }

    if($action == 'filtrarCuentas'){

        $keywordCuenta = $_POST['keywordCuenta'];

        $sqlr = 'SELECT * FROM (SELECT cn1.cuenta,cn1.nombre,cn1.naturaleza,cn1.centrocosto,cn1.tercero,cn1.tipo,cn1.estado FROM cuentasnicsp AS cn1 INNER JOIN cuentasnicsp AS cn2 ON cn2.tipo=\'Auxiliar\'  AND cn2.cuenta LIKE CONCAT( cn1.cuenta,  \'%\' ) WHERE cn1.tipo=\'Mayor\' AND concat_ws(" ", cn1.cuenta, cn1.nombre) LIKE \'%'.$keywordCuenta.'%\' GROUP BY cn1.cuenta UNION SELECT cuenta,nombre,naturaleza,centrocosto,tercero,tipo,estado FROM cuentasnicsp WHERE tipo=\'Auxiliar\' AND concat_ws(" ", cuenta, nombre) LIKE \'%'.$keywordCuenta.'%\')  AS tabla ORDER BY 1';
        
        $res = mysqli_query($linkbd, $sqlr);

        $cuentas = array();
        while($row = mysqli_fetch_row($res))
        {
            array_push($cuentas, $row);
        }
        //var_dump($cuentas);
        $out['cuentas'] = $cuentas;
    }

    if($action == 'buscarCuentasDebito')
    {
        $destinoCompra = $_GET['destinoCompra'];

        $fec = $_GET['fecha'];

        preg_match("/([0-9]{2})\/([0-9]{2})\/([0-9]{4})/", $fec,$fech);

        $fecha = "$fech[3]-$fech[2]-$fech[1]";

        $cuentas = array();

        $sqlAlmacen = "SELECT cuenta_inicial, cuenta_final FROM `almdestinocompra_det` WHERE codigo = '$destinoCompra'";
        $resAlmacen = mysqli_query($linkbd,$sqlAlmacen);
        while ($rowAlmacen = mysqli_fetch_assoc($resAlmacen)) {

            $sqlr = "SELECT cuenta, nombre, naturaleza, centrocosto, tercero, tipo, estado FROM cuentasnicsp WHERE estado = 'S' AND (cuenta BETWEEN '$rowAlmacen[cuenta_inicial]' AND '$rowAlmacen[cuenta_final]') OR cuenta LIKE '$rowAlmacen[cuenta_final]%' ORDER BY cuenta ASC";
        
            $res = mysqli_query($linkbd, $sqlr);
            
            while($row = mysqli_fetch_row($res))
            {
                array_push($cuentas, $row);
            }
        }
        
        $out['cuentas'] = $cuentas;
    }

    if($action == 'filtrarCuentasDebito')
    {
        $destinoCompra = $_GET['destinoCompra'];

        $fec = $_GET['fecha'];

        preg_match("/([0-9]{2})\/([0-9]{2})\/([0-9]{4})/", $fec,$fech);

		$fecha = "$fech[3]-$fech[2]-$fech[1]";

        $sqlAlmacen = "SELECT cuenta_inicial, cuenta_final FROM `almdestinocompra_det` WHERE codigo = '$destinoCompra'";
        $resAlmacen = mysqli_query($linkbd,$sqlAlmacen);
        $rowAlmacen = mysqli_fetch_assoc($resAlmacen);

        $keywordCuenta = $_POST['keywordCuenta'];
        echo $keywordCuenta;

        $sqlr = "SELECT * FROM (SELECT cn1.cuenta,cn1.nombre,cn1.naturaleza,cn1.centrocosto,cn1.tercero,cn1.tipo,cn1.estado FROM cuentasnicsp AS cn1 INNER JOIN cuentasnicsp AS cn2 ON cn2.tipo=\'Auxiliar\'  AND cn2.cuenta LIKE CONCAT( cn1.cuenta,  \'%\' ) WHERE cn1.tipo=\'Mayor\' AND cn1.cuenta BETWEEN '$rowAlmacen[cuenta_inicial]' AND '$rowAlmacen[cuenta_final]' AND concat_ws(' ', cn1.cuenta, cn1.nombre) LIKE \'%'.$keywordCuenta.'%\' GROUP BY cn1.cuenta UNION SELECT cuenta,nombre,naturaleza,centrocosto,tercero,tipo,estado FROM cuentasnicsp WHERE tipo=\'Auxiliar\' AND cuenta BETWEEN '$rowAlmacen[cuenta_inicial]' AND '$rowAlmacen[cuenta_final]' AND concat_ws(' ', cuenta, nombre) LIKE \'%'.$keywordCuenta.'%\')  AS tabla ORDER BY 1";

        //echo $sqlr;
        
        $res = mysqli_query($linkbd, $sqlr);

        $cuentas = array();
        while($row = mysqli_fetch_row($res))
        {
            array_push($cuentas, $row);
        }
        
        //$out['cuentas'] = $cuentas;
    }

    if($action == "buscaCta"){

        $cuenta = $_GET["cuenta"];

        $sqlr = "SELECT nombre, naturaleza FROM cuentasnicsp WHERE cuenta = $cuenta AND naturaleza = 'CREDITO' ";

        $res = mysqli_query($linkbd, $sqlr);

        $row = mysqli_fetch_row($res);

        $out['nombreCuenta'] = $row[0];
        $out['naturalezaCuenta'] = $row[1];

    }

    if($action == "buscaCta2"){

        $fec = $_GET['fecha'];

        preg_match("/([0-9]{2})\/([0-9]{2})\/([0-9]{4})/", $fec,$fech);

		$fecha = "$fech[3]-$fech[2]-$fech[1]";

        $destinoCompra = $_GET['destinoCompra'];
    
        $sqlAlmacen = "SELECT cuenta_inicial, cuenta_final FROM `almdestinocompra_det` WHERE codigo = '$destinoCompra'";
        $resAlmacen = mysqli_query($linkbd,$sqlAlmacen);
        $rowAlmacen = mysqli_fetch_assoc($resAlmacen);

        $cuenta = $_GET["cuenta"];

        $sqlr = "SELECT nombre, naturaleza FROM cuentasnicsp WHERE cuenta BETWEEN '$rowAlmacen[cuenta_inicial]' AND '$rowAlmacen[cuenta_final]' AND cuenta = $cuenta AND naturaleza = 'DEBITO' ";
        $res = mysqli_query($linkbd, $sqlr);
        $row = mysqli_fetch_row($res);

        $out['nombreCuenta'] = $row[0];
        $out['naturalezaCuenta'] = $row[1];

    }

    if($action == "guardar")
    {

        $sqlr = 'INSERT INTO conceptoscontables (codigo, nombre, modulo, tipo, almacen) VALUES ("'.$_POST['codigo'].'", "'.$_POST['nombre'].'", \'3\', "'.$_POST['tipo'].'", \'\')';

        if(mysqli_query($linkbd, $sqlr)){
            
            //Guarda Credito
            for($x = 0; $x < count($_POST['detallesAgr']); $x++){

                $conceptos_det = explode(",", $_POST['detallesAgr'][$x]);

                $cred = 'S';
                $deb = 'N';
                $fecha = '';

                $fec = explode("/", $conceptos_det[0]);

                $fecha = $fec[2].'-'.$fec[1].'-'.$fec[0];

                $sql = 'INSERT INTO conceptoscontables_det(codigo, tipo, tipocuenta, cuenta, debito, credito, estado, modulo, fechainicial) VALUES ("'.$_POST['codigo'].'", "'.$_POST['tipo'].'", \'N\', "'.$conceptos_det[1].'", "'.$deb.'", "'.$cred.'", \'S\', \'3\', "'.$fecha.'")';
                
                mysqli_query($linkbd, $sql);

                $out['insertaBien'] = true;

            }
            
            //Guarda Debito
            for($x = 0; $x < count($_POST['detallesAgr2']); $x++){

                $conceptos_det2 = explode(",", $_POST['detallesAgr2'][$x]);

                $cred = '';
                $deb = '';
                $fecha = '';

                if($conceptos_det22[4] == 'DEBITO'){
                    $deb = 'S';
                    $cred = 'N';
                }else{
                    $deb = 'N';
                    $cred = 'S';
                }

                $fec = explode("/", $conceptos_det2[0]);

                $fecha = $fec[2].'-'.$fec[1].'-'.$fec[0];

                $sql = 'INSERT INTO conceptoscontables_det(codigo, tipo, tipocuenta, cuenta, cc, debito, credito, estado, modulo, fechainicial, asociado_a,destino_compra) VALUES ("'.$_POST['codigo'].'", "'.$_POST['tipo'].'", \'N\', "'.$conceptos_det2[1].'", "'.$conceptos_det2[3].'", \'S\', \'N\', \'S\', \'3\', "'.$fecha.'", "'.$conceptos_det2[5].'", "'.$conceptos_det2[6].'")';
                
                mysqli_query($linkbd, $sql);

                $out['insertaBien'] = true;

            }
            

        }else{

            $out['insertaBien'] = false;

        }

    }

    header("Content-type: application/json");
    echo json_encode($out);
    die();
