<?php
	require "comun.inc";
	require "funciones.inc";
	session_start();
	$linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");
	cargarcodigopag($_GET['codpag'],$_SESSION["nivel"]);
	header("Cache-control: private"); // Arregla IE 6
	date_default_timezone_set("America/Bogota");
?>
<!DOCTYPE >
<html lang="es">
	<head>
		<meta charset="UTF-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
    	<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<title>:: IDEAL 10 - Tesorer&iacute;a</title>

        <link href="css/css2.css" rel="stylesheet" type="text/css" />
        <link href="css/css3.css" rel="stylesheet" type="text/css" />
		<script type="text/javascript" src="css/programas.js"></script>
		<script> 
			function ponprefijo(norden)
			{
   				parent.document.form2.idrecaudo.value = norden;
				parent.document.form2.idrecaudo.focus();
                parent.document.form2.submit();
				parent.despliegamodal2("hidden");
			}
		</script>
	</head>
	<body>
  	<form action="" method="post" name="form2">
    	<?php if($_POST['oculto']==""){$_POST['numpos']=0;$_POST['numres']=10;$_POST['nummul']=0;}?>
		<table  class="inicio" style="width:99.4%;">
      		<tr>
        		<td class="titulos" colspan="4">:: Buscar Reconocimiento ingresos internos</td>
                <td class="cerrar"><a onClick="parent.despliegamodal2('hidden');" href="#" >&nbsp;Cerrar</a></td>
      		</tr>
      		<tr>
        		<td class="saludo1" style="width:7cm">:: N&uacute;mero o nombre del Reconocimiento de ingreso interno:</td>
        		<td style="width:35%"><input type="search" name="numero" id="numero" value="<?php echo $_POST['numero'];?>" style="width:98%;"></td>
                <td><input type="button" name="bboton" onClick="limbusquedas();" value="&nbsp;&nbsp;Buscar&nbsp;&nbsp;" /></td>
       		</tr>
    	</table>
    	<input type="hidden" name="oculto" id="oculto" value="1">
        <input type="hidden" name="numres" id="numres" value="<?php echo $_POST['numres'];?>"/>
        <input type="hidden" name="numpos" id="numpos" value="<?php echo $_POST['numpos'];?>"/>
        <input type="hidden" name="nummul" id="nummul" value="<?php echo $_POST['nummul'];?>"/>
		<div class="subpantalla" style="height:86%; width:99%; overflow-x:hidden;">
			<?php
				
				$crit1 = "";
				$crit2 = " ";
				if ($_POST['numero'] != "")
				{
					$crit1 = "AND  concat_ws(' ',  id_recaudo,concepto) LIKE '%$_POST[numero]%'";
				}
				

				$sqlr = "SELECT * FROM tesosinrecaudos AS TRL WHERE TRL.estado='S' $crit1  AND NOT EXISTS (SELECT 1 FROM tesosinreciboscaja AS TRT WHERE TRL.id_recaudo = TRT.id_recaudo AND TRT.estado = 'S') ORDER BY TRL.id_recaudo DESC";
				$resp = mysqli_query($linkbd, $sqlr);
				$con = 1;
				echo "
				<table class='inicio' align='center' width='99%'>
					<tr>
						<td colspan='6' class='titulos'>.: Resultados Busqueda:</td>
					</tr>
					<tr>
						<td class='titulos2' style='width:10%'>id recaudo</td>
						<td class='titulos2' style='width:10%'>Fecha</td>
						<td class='titulos2' style='width:10%'>Vigencia</td>
						<td class='titulos2' >Concepto</td>
						<td class='titulos2' style='width:8%'>Valor</td>
					</tr>";	
					$iter='saludo1a';
					$iter2='saludo2';
					while ($row = mysqli_fetch_row($resp)) 
					{
						$con2 = $con+ $_POST['numpos'];
						echo"<tr class='$iter' style='text-transform:uppercase' onClick=\"javascript:ponprefijo('$row[0]')\" onMouseOver=\"anterior=this.style.backgroundColor;this.style.backgroundColor='#40b3ff';\" onMouseOut=\"this.style.backgroundColor=anterior\" >
						<td>$row[0]</td>
						<td>$row[2]</td>
						<td>$row[3]</td>
						<td>$row[6]</td>
						<td style='text-align:right;'>$row[5]</td></tr>";
						$con+=1;
						$aux=$iter;
						$iter=$iter2;
						$iter2=$aux;
					}
				echo "</table>";
			?>
			</div>
<input type="hidden" name="numtop" id="numtop" value="<?php echo $_POST['numtop'];?>" />
</form>
</body>
</html>
