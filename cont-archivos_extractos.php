<?php
	header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=utf8");
	require "comun.inc";
	require "funciones.inc";
	session_start();
	$linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");
	date_default_timezone_set("America/Bogota");
?>
<!DOCTYPE > 
<html lang="es">
	<head>
		<meta charset="utf-8"/>
		<meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1"/>
		<meta name="viewport" content="user-scalable=no">
		<title>:: IDEAL 10 - Contabilidad</title>
		<link href="css/css2.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<link href="css/css4.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
		<link href="sweetalert2/dist/sweetalert2.min.css" rel="stylesheet">
		<script src="sweetalert2/dist/sweetalert2.min.js"></script>
		<script type="text/javascript" src="css/programas.js"></script>
		<script language="javascript">
			function guardarextracto(){
				if(document.form2.saldoext.value != '' && document.form2.nomarch.value !== ''){
					document.form2.oculto.value = '2';
					document.form2.submit();
				}else{
					Swal.fire({
						icon: 'error',
						title: 'Error!',
						text: 'se debe ingresar archivo y saldo de la cuenta',
						confirmButtonText: 'Continuar',
						confirmButtonColor: '#FF121A',
						timer: 2500
					});
				}
			}
			function cerrar_ventana(){
				if(document.form2.bandera.value == '1'){
					parent.buscar();
					parent.despliegamodal2('hidden');
				}else{
					parent.despliegamodal2('hidden');
				}
			}
			function validarsaldo(){
				let saldo = document.form2.saldoext
				if(saldo.value == ''){
					saldo.value = 0;
				}
			}
		</script> 
		<?php titlepag();?>
	</head>
	<body >
		<form name="form2" id="form2" method="post" enctype="multipart/form-data">
			<?php
				if($_POST['oculto'] == ''){
					$_POST['bandera'] = '0';
					$_POST['cuenta'] = $_GET['cuenta'];
					$_POST['fechaini'] = $_GET['fecha1'];
					$_POST['fechafin'] = $_GET['fecha2'];
					$_POST['saldocal'] = $_GET['saldocal'];
					$_POST['diferencia'] = $_GET['difer'];
					if($_GET['saldoex'] != ''){
						$_POST['saldoext'] = $_GET['saldoex'];
					}else{
						$_POST['saldoext'] = 0;
					}
					$_POST['rutaad'] = "informacion/extractos/temp/";
					$ruta = "informacion/extractos/temp";
					if(!file_exists($ruta)){
						mkdir ($ruta);
					}else {
						array_map('unlink', glob("informacion/extractos/temp/*"));mkdir ($ruta);
					}
				}
			?>
			<table class='tablamv3 ancho' style="width:99.4%;">
				<thead>
					<tr>
						<th class="titulos" style="text-align: left;">:: <?php echo "Cuenta ".$_POST['cuenta'].", Periodo: ".$_POST['fechaini']." - ".$_POST['fechafin']; ?> </th>
						<th class="cerrar" style="width:7%" onClick="cerrar_ventana()">Cerrar</th>
					</tr>
					<tr>
						<th class="tamano01" style='width:2cm;'>Saldo:</th>
						<th style="width:20%"><input type="search" name="saldoext" id="saldoext" style="width:100%" value="<?php echo $_POST['saldoext'];?>" autocomplete="off" onchange="validarsaldo();"></th>
						<th class="tamano01" style='width:2cm;'>Extracto:</th>
						<th><input type="text" name="nomarch" id="nomarch" style="width:100%" value="<?php echo $_POST['nomarch'];?>" readonly class="tamano02"/></th>
						<th>
							<div class='upload' style="height: 23px">
								<input type="file" name="plantillaad" onChange="document.form2.submit();" style='cursor:pointer;' title='Cargar Extracto' accept='application/pdf'/>
								<img  src='imagenes/upload01.png' style="width:23px"/>
							</div> 
						</th>
						<th style="padding-bottom:0px"><em class="botonflechaverde" onClick="guardarextracto();">Guardar Ext.</em></th>
						<th style="width:7%"></th>
					</tr>
					<input type="hidden" name="cuenta" id="cuenta" value="<?php echo $_POST['cuenta'];?>">
					<input type="hidden" name="fechaini" id="fechaini" value="<?php echo $_POST['fechaini'];?>">
					<input type="hidden" name="fechafin" id="fechafin" value="<?php echo $_POST['fechafin'];?>">
					<input type="hidden" name="rutaad" id="rutaad" value="<?php echo $_POST['rutaad']?>">
					<input type="hidden" name="bandera" id="bandera" value="<?php echo $_POST['bandera']?>">
					<input type="hidden" name="saldocal" id="saldocal" value="<?php echo $_POST['saldocal']?>">
					<input type="hidden" name="diferencia" id="diferencia" value="<?php echo $_POST['diferencia']?>">
					<input type="hidden" name="oculto" id="oculto" value="1">
					<?php 
						preg_match("/([0-9]{2})\/([0-9]{2})\/([0-9]{4})/", $_POST['fechaini'],$fecha);
						$fecha1 = $fecha[3]."-".$fecha[2]."-".$fecha[1];
						$vigusu = $fecha[3];
						preg_match("/([0-9]{2})\/([0-9]{2})\/([0-9]{4})/", $_POST['fechafin'],$fecha);
						$fecha2 = $fecha[3]."-".$fecha[2]."-".$fecha[1];
						$fechahoy = date("Y-m-d");
						if($_POST['oculto'] == '2'){
							$consec = selconsecutivo("cont_extractos_conciliacion","id");
							$nomarchivo = "extracto_".$_POST['cuenta']."_$consec.pdf";
							$sql = "UPDATE cont_extractos_conciliacion SET estado = 'N' WHERE cuenta = '".$_POST['cuenta']."' AND fechaini >= '$fecha1' AND fechafin <= '$fecha2'";
							mysqli_query($linkbd,$sql);
							$sql = "INSERT INTO cont_extractos_conciliacion (id, cuenta, archivo, saldo, fechaini, fechafin, usuario, creado, estado) VALUES ('$consec', '".$_POST['cuenta']."', '$nomarchivo', '".$_POST['saldoext']."', '$fecha1', '$fecha2', '".$_SESSION['cedulausu']."', '$fechahoy', 'S')";
							mysqli_query($linkbd,$sql);
							$dirorigen = $_POST['rutaad'].$_POST['nomarch'];
							$dirdestino = "informacion/extractos/".$nomarchivo;
							copy($dirorigen,$dirdestino);

							$sql = "DELETE FROM conciliacion_cab WHERE cuenta = '".$_POST['cuenta']."' AND periodo1 ='$fecha1' AND periodo2 = '$fecha2'";
							mysqli_query($linkbd,$sql);
							$sql = "INSERT INTO conciliacion_cab (cuenta, extractobanc, extractocalc, diferencia, fecha, vigencia, periodo1, periodo2) values ('".$_POST['cuenta']."', '".$_POST['saldoext']."', '".$_POST['saldocal']."', '".$_POST['diferencia']."', '$fechahoy', '$vigusu', '$fecha1', '$fecha2')";
							mysqli_query($linkbd,$sql);

							echo"
							<script>
								document.form2.bandera.value = '1';
								Swal.fire({
									icon: 'success',
									title: 'se almaceno el extracto y el saldo correctamente',
									confirmButtonText: 'Continuar',
									confirmButtonColor: '#FF121A',
									timer: 3000
								});
							</script>
							";
						}
						$sqlr = "SELECT * FROM cont_extractos_conciliacion WHERE cuenta = '".$_POST['cuenta']."' AND fechaini >= '$fecha1' AND fechafin <= '$fecha2' ORDER BY id DESC";
						$resp = mysqli_query($linkbd,$sqlr);
						$totalbusqueda = mysqli_num_rows($resp);
						echo "
						<tr>
							<th class='titulos'>.: Resultados Busqueda: $totalbusqueda</th>
						<tr>
							<th class='titulosnew00' style='width:5%;'>ID</th>
							<th class='titulosnew00' style='width:25%;'>ARCHIVO</th>
							<th class='titulosnew00' style='width:8%;'>DESCARGA</th>
							<th class='titulosnew00' style='width:10.5%;'>SALDO</th>
							<th class='titulosnew00' style='width:40%;'>USUARIO</th>
							<th class='titulosnew00' >CREADO</th>
						</tr>";
					?>
				</thead>
				<tbody style='height:72vh;'>
					<?php	
						$iter='saludo1a';
						$iter2='saludo2';
						while ($row = mysqli_fetch_row($resp)){
							$sqluser = "SELECT nom_usu FROM usuarios WHERE cc_usu = '$row[6]'";
							$resuser = mysqli_query($linkbd,$sqluser);
							$rowuser = mysqli_fetch_row($resuser);
							echo"
							<tr >
								<td class='$iter' style='width:5.2%;'>$row[0]</td>
								<td class='$iter' style='width:25.5%;'>$row[2]</td>
								<td class='$iter' style='width:8.1%;text-align:center;'><a href='informacion/extractos/$row[2]' download><img src='imagenes/descargar_pdf.png' style='height:25px; width:25px;' title='Descargar Extracto'></a></td>
								<td class='$iter' style='width:10.5%;text-align:right;'>$".number_format($row[3],2,',','.')."</td>
								<td class='$iter' style='width:40%;text-align:center;'>$rowuser[0]</td>
								<td class='$iter' style='text-align:center;'>$row[7]</td>
							</tr>";
							$aux = $iter;
							$iter = $iter2;
							$iter2 = $aux;
						}
						if ($totalbusqueda == 0){
							echo "
							<tr>
								<td class='saludo1' style='text-align:center;width:100%'><img src='imagenes\alert.png' style='width:25px'>No se ha almacenado extractos en este periodo para esta cuenta<img src='imagenes\alert.png' style='width:25px'></td>
							</tr>
							";
						}
						//archivos
						if (is_uploaded_file($_FILES['plantillaad']['tmp_name'])){
							copy($_FILES['plantillaad']['tmp_name'], $_POST['rutaad'].$_FILES['plantillaad']['name']);
							if (file_exists($_POST['rutaad'].$_FILES['plantillaad']['name'])){
								echo"<script>document.getElementById('nomarch').value='".$_FILES['plantillaad']['name']."';</script>";
							}else{
								echo"
								<script>
									Swal.fire({
										icon: 'error',
										title: 'Error!',
										text: 'No Se pudo cargar el archivo, favor intentar de nuevo',
										confirmButtonText: 'Continuar',
										confirmButtonColor: '#FF121A',
										timer: 2500
									});
								</script>";
							}
						}
					?>
				</tbody>
			</table>
		</form>
	</body>
</html>
