<?php
	header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=utf8");

	require "comun.inc";
	require "funciones.inc";
	require "conversor.php";
	require "validaciones.inc";

	$linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");

	session_start();
	date_default_timezone_set("America/Bogota");
    ini_set('max_execution_time',0);
?>
<!DOCTYPE > 
<html lang="es">
	<head>
		<meta charset="utf-8"/>
		<meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1"/>
		<meta name="viewport" content="user-scalable=no">
		<title>:: IDEAL 10 - Tesorer&iacute;a</title>
		<link href="favicon.ico" rel="shortcut icon"/>
		
			<script type="text/javascript" src="jquery-1.11.0.min.js"></script> 
			<script type="text/javascript" src="css/programas.js"></script>
			<script>
				function verUltimaPos(idcta, filas, filtro)
				{
					var scrtop=$('#divdet').scrollTop();
					var altura=$('#divdet').height();
					var numpag=$('#nummul').val();
					var limreg=$('#numres').val();
					if((numpag<=0)||(numpag==""))
						numpag=0;
					if((limreg==0)||(limreg==""))
						limreg=10;
					numpag++;	//location.href="teso-predialver.php?idpredial="+idcta+"&scrtop="+scrtop+"&totreg="+filas+"&altura="+altura+"&numpag="+numpag+"&limreg="+limreg+"&filtro="+filtro;
				}
				function eliminar(idr)
				{
					if (confirm("Esta Seguro de Eliminar"))
					{
						document.form2.oculto.value=2;
						document.form2.var1.value=idr;
						document.form2.submit();
					}
				}
				
				function excell()
				{
					document.form2.action="teso-buscareportecobropredialexcel.php";
					//document.form2.excel.value=1;
					document.form2.target="_BLANK";
					document.form2.submit();
					refrescar(); 

				}
				function crearexcel()
				{
					document.form2.action="teso-buscapredialexcel.php";
					document.form2.target="_BLANK";
					document.form2.submit();
					refrescar();
				}

				function refrescar()
				{
					document.form2.excel.value="";
					document.form2.action="";
					document.form2.target="";
					//document.form2.submit();
				}
				function filtrarcatastral(codigo)
				{
					document.getElementById('nombre').value=codigo;
					limbusquedas();
				}
                $(window).load(function () {
                    $('#cargando').hide();
                });
			</script>

			<script src="css/calendario.js"></script>
			<link href="css/css2.css" rel="stylesheet" type="text/css" />
			<link href="css/css3.css" rel="stylesheet" type="text/css" />
				
				<?php
				$scrtop=$_GET['scrtop'];
				if($scrtop=="") $scrtop=0;
				echo"<script>
					window.onload=function(){
						$('#divdet').scrollTop(".$scrtop.")
					}
				</script>";
				$gidcta=$_GET['idcta'];
				if(isset($_GET['filtro']))
					$_POST['nombre']=$_GET['filtro'];
				?>
	</head>
    <body>
        <div id="cargando" style=" position:absolute;left: 46%; bottom: 45%">
			<img src="imagenes/loading.gif" style=" width: 80px; height: 80px"/>
		</div>
		<IFRAME src="alertas.php" name="alertas" id="alertas" style="display:none"></IFRAME>
		<span id="todastablas2"></span>
		<table>
			<tr><script>barra_imagenes("teso");</script><?php cuadro_titulos();?></tr>	 
			<tr><?php menu_desplegable("teso");?></tr>
			<tr>
				<td colspan="3" class="cinta">
					<a href="teso-reportecarterapredial.php" class="mgbt"><img src="imagenes/add.png" alt="Nuevo" title="Nuevo"/></a> 
					<a href="#" class="mgbt"><img src="imagenes/guardad.png" alt="Guardar" title="Guardar"/> </a>
					<a onClick="document.form2.submit();" href="#" class="mgbt"><img src="imagenes/busca.png" alt="Buscar" title="Buscar" /></a> 
					<a onClick="mypop=window.open('plan-agenda.php','','');mypop.focus()" class="mgbt"><img src="imagenes/agenda1.png" title="Agenda" /></a>
					<a href="#" onClick="mypop=window.open('teso-principal.php','','');mypop.focus();" class="mgbt"><img src="imagenes/nv.png" alt="nueva ventana" title="Nueva ventana"></a> 
					<a href="<?php echo "archivos/".$_SESSION['usuario']."-reportecarterapredial.csv"; ?>" download="<?php echo "archivos/".$_SESSION['usuario']."-reportecarterapredial.csv"; ?>" target="_blank" class="mgbt"><img src="imagenes/csv.png"  alt="csv" title="csv"></a>
					
					<a href="teso-informespredios.php" class="mgbt"><img src="imagenes/iratras.png" title="Atr&aacute;s"></a>
				</td>
			</tr>	
		</table>
		<tr><td colspan="5" class="tablaprin"> 
        <?php
		if($_GET['numpag']!=""){
			$oculto=$_POST['oculto'];
			if($oculto!=2){
				$_POST['numres']=$_GET['limreg'];
				$_POST['numpos']=$_GET['limreg']*($_GET['numpag']-1);
				$_POST['nummul']=$_GET['numpag']-1;
			}
		}
		else{
			if($_POST['nummul']==""){
				$_POST['numres']=10;
				$_POST['numpos']=0;
				$_POST['nummul']=0;
			}
		}
        $sqlrFec = "SELECT fecha FROM tesocarterapredial LIMIT 1";
        $respFec = mysqli_query($linkbd, $sqlrFec);
        $rowFec = mysqli_fetch_row($respFec);
        $fechaCartera = $rowFec[0];

		?>
<form name="form2" method="post" action="">
    <input type="hidden" name="numres" id="numres" value="<?php echo $_POST['numres'];?>"/>
	<input type="hidden" name="numpos" id="numpos" value="<?php echo $_POST['numpos'];?>"/>
	<input type="hidden" name="nummul" id="nummul" value="<?php echo $_POST['nummul'];?>"/>
	
	<table  class="inicio" align="center" >
    	<tr >
        	<td class="titulos" colspan="7">:. Buscar Cartera Predial</td>
        	<td style="width:7%" class="cerrar" ><a href="teso-principal.php">Cerrar</a></td>
      	</tr>
      	<tr >
			<td style="width:15%" class="saludo1"> Codigo Catastral:</td>
        	<td style="width:15%" >
        		<input name="nombre" id="nombre" type="search" style="width:90%" value="<?php echo $_POST['nombre'] ?>" />
        		<input name="oculto" id="oculto" type="hidden" value="<?php echo $_POST['oculto'] ?>" />
        		<input name="excel"  id="excel"  type="hidden" value="<?php echo $_POST['excel'] ?>" />
        		<input name="var1" id="var1" type="hidden" value="<?php echo $_POST['var1'] ?>" />
        	</td>
			
			<td><input type="button" name="bboton" onClick="limbusquedas();" value="&nbsp;&nbsp;Buscar&nbsp;&nbsp;" /></td>
            <td style="width:30%" class="saludo1"> <p>Nota: Esta cartera fue generada el dia <?php echo $fechaCartera; ?>, a esta fecha estan calculados los intereses. </p></td>
            
        </tr>
		
    </table>    
	<div class="subpantallac5" style="height:62.5%; width:99.6%;" id="divdet">
    	<?php
    	
		$cmoff='imagenes/sema_rojoOFF.jpg';
		$cmrojo='imagenes/sema_rojoON.jpg';
		$cmamarillo='imagenes/sema_amarilloON.jpg';
		$cmverde='imagenes/sema_verdeON.jpg';
		$oculto=$_POST['oculto'];
		$excel=$_POST['excel'];
		$crit1="";
		$crit2="";
		if ($_POST['nombre']!=""){
			$crit1="and codcatastral LIKE '%$_POST[nombre]%'";
		}
		
		//sacar el consecutivo 
		$sqlr = "select *from tesocarterapredial where tesocarterapredial.idtesoreporte>-1 $crit1";
		$resp = mysqli_query($linkbd, $sqlr);
		$ntr = mysqli_num_rows($resp);

		$_POST['numtop']=$ntr;
		$nuncilumnas=ceil($_POST['numtop']/$_POST['numres']);

		$sqlr="select *from tesocarterapredial where tesocarterapredial.idtesoreporte>-1 $crit1";
        $resp = mysqli_query($linkbd, $sqlr);

		$numcontrol=$_POST['nummul']+1;
		if(($nuncilumnas==$numcontrol)||($_POST['numres']=="-1")){
			$imagenforward="<img src='imagenes/forward02.png' style='width:17px'>";
			$imagensforward="<img src='imagenes/skip_forward02.png' style='width:16px' >";
		}
		else{
			$imagenforward="<img src='imagenes/forward01.png' style='width:17px' title='Siguiente' onClick='numsiguiente()'>";
			$imagensforward="<img src='imagenes/skip_forward01.png' style='width:16px' title='Fin' onClick='saltocol(\"$nuncilumnas\")'>";
		}
		if(($_POST['numpos']==0)||($_POST['numres']=="-1")){
			$imagenback="<img src='imagenes/back02.png' style='width:17px'>";
			$imagensback="<img src='imagenes/skip_back02.png' style='width:16px'>";
		}
		else{
			$imagenback="<img src='imagenes/back01.png' style='width:17px' title='Anterior' onClick='numanterior();'>";
			$imagensback="<img src='imagenes/skip_back01.png' style='width:16px' title='Inicio' onClick='saltocol(\"1\")'>";
		}

		$con=1;
		echo "<table class='inicio' align='center'>
			<tr>
				<td colspan='15' class='titulos'>.: Resultados Busqueda:</td>
			</tr>
			<tr>
				<td colspan='15' class='saludo3'>Predios Encontradas: $ntr</td>
			</tr>
			<tr>
				<td class='titulos2'>Vigencia</td>
				<td class='titulos2'>Codigo Catastral</td>
				<td class='titulos2'>Documento</td>
				<td class='titulos2'>Nombre propietario</td>
				<td class='titulos2'>Direccion</td>
				<td class='titulos2'>Predial</td>
				<td class='titulos2'>Intereses Predial</td>
				<td class='titulos2'>Sobretasa Bomberil</td>
				<td class='titulos2'>Intereses Bomberil</td>
				<td class='titulos2'>Sobretasa ambiental</td>
				<td class='titulos2'>Intereses ambiental</td>
				<td class='titulos2'>Descuentos</td>
				<td class='titulos2'>Total</td>
				<td class='titulos2'>Fecha deuda</td>
				<td class='titulos2'>Estado</td>
				
			</tr>";	
          	$iter='saludo1a';
            $iter2='saludo2';
			$filas=1;

            $namearch="archivos/".$_SESSION['usuario']."-reportecarterapredial.csv";
            $Descriptor1 = fopen($namearch,"w+"); 
            fputs($Descriptor1,"VIGENCIA;CODIGO CATASTRAL;DOCUMENTO;NOMBRE PROPIETARIO;DIRECCION;PREDIAL;INTERESES PREDIAL;BOMBERIL;INTERESES BOMBERIL;AMBIENTAL;INTERESES AMBIENTAL;DESCUENTOS;TOTAL;FECHA DEUDA;ESTADO\r\n");
			
 			while ($row = mysqli_fetch_row($resp)){
					
				$sql1="SELECT pago FROM tesoprediosavaluos WHERE codigocatastral='$row[3]' and vigencia='$row[2]'";
				$r1=mysqli_query($linkbd, $sql1);
				$row3=mysqli_fetch_row($r1);
                $titleFlag = '';
				if($row3[0]!="N")
				{
					$p2luzcem1=$cmverde;
                    $titleFlag = 'Pago';
				}else{
                    $p2luzcem1=$cmrojo;
                    $titleFlag = 'Debe';
                }
				
				
				$idcta="'".$row[0]."'";

				$sqlrDoc = "SELECT documento, nombrepropietario, direccion FROM tesopredios WHERE cedulacatastral = '$row[3]'";
				$respDoc = mysqli_query($linkbd, $sqlrDoc);
				$rowDoc = mysqli_fetch_row($respDoc);

				$numfil="'".$filas."'";
				$filtro="'".$_POST['nombre']."'";
				echo"<tr class='$iter' onMouseOver=\"anterior=this.style.backgroundColor;this.style.backgroundColor='#40b3ff';\"
			onMouseOut=\"this.style.backgroundColor=anterior\" style='text-transform:uppercase; $estilo' >
					<td>$row[2]</td>
					<td>$row[3]</td>
					<td>$rowDoc[0]</td>
					<td>$rowDoc[1]</td>
					<td>$rowDoc[2]</td>
					<td>$ ".number_format($row[4],2)."</td>
					<td>$ ".number_format($row[5],2)."</td>
					<td>$ ".number_format($row[6],2)."</td>
					<td>$ ".number_format($row[7],2)."</td>
					<td>$ ".number_format($row[8],2)."</td>
					<td>$ ".number_format($row[9],2)."</td>
					<td>$ ".number_format($row[10],2)."</td>
					<td>$ ".number_format($row[12],2)."</td>
                    <td>$row[17]</td>
					<td align='center'>
                        <img src='$p2luzcem1' title='$titleFlag' width='16' height='16'>
					</td>
				</tr>";
                fputs($Descriptor1,$row[2].";'".$row[3]."';".$rowDoc[0].";".$rowDoc[1].";".$rowDoc[2].";".round($row[4]).";".round($row[5],0).";".round($row[6],0).";".round($row[7],0).";".round($row[8],0).";".round($row[9],0).";".round($row[10],0).";".round($row[12],0).";".round($row[17],0).";".$titleFlag."\r\n");

                $con+=1;
                $con+=1;
                $aux=$iter;
                $iter=$iter2;
                $iter2=$aux;
				$filas++;

                
           	}
		/* if($_POST['excel']==1){
		?>
			<script>crearexcel();</script>
		<?php
		} */
		
		?>
	</div>
</form> 
</td></tr>     
</table>
</body>
</html>