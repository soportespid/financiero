<?php
	header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=utf8");
	require "comun.inc";
	require "funciones.inc";
	session_start();
	$linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");
	date_default_timezone_set("America/Bogota");
?>
<!DOCTYPE >
<html lang="es">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1">
		<meta name="viewport" content="user-scalable=no">
		<title>:: IDEAL 10 - Tesorer&iacute;a</title>
		<link href="favicon.ico" rel="shortcut icon"/>
		<link href="css/css2z.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet"/>
		<link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet"/>
		<script type="text/javascript" src="css/programas.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script> 
			var anterior;
			function ponprefijo(pref,opc){
				opener.document.form2.cuentap.value =pref  ;
				opener.document.form2.ncuentap.value =opc ;
				opener.document.form2.cuentap.focus();
				//opener.document.form2.tercero.select();
				window.close() ;
			} 
		</script> 
	</head>
	<body >
		<?php
			$vigusu=vigencia_usuarios($_SESSION['cedulausu']); 
			if(!$_POST['tipo']){
				$_POST['tipo']=$_GET['ti'];
			}
		?>
		<form action="" method="post" enctype="multipart/form-data" name="form1">
			<table class="inicio">
				<tr><td height="25" colspan="4" class="titulos" >Buscar CUENTAS PRESUPUESTALES</td></tr>
				<tr><td colspan="4" class="titulos2" >:&middot; Por Descripcion </td></tr>
				<tr >
					<td class="saludo1" >:&middot; Numero Cuenta:</td>
					<td  colspan="3"><input name="numero" type="text" size="30" >
					<input name="oculto" type="hidden" id="tipo" value="<?php echo $_POST['tipo']?>" >
					<input name="oculto" type="hidden" id="oculto" value="1" >
					<input type="submit" name="Submit" value="Buscar" >
					</td>
				</tr>
				<tr ><td colspan="4">&nbsp;</td></tr>
			</table>
			<div class="subpantalla" style="height:76.5%; width:99.6%; overflow-x:hidden;">
				<table class="inicio">
					<tr><td height="25" colspan="5" class="titulos" >Resultados Busqueda </td></tr>
					<tr>
						<td width="32" class="titulos2" >Item</td>
						<td width="76" class="titulos2" >Cuenta </td>
						<td width="140" class="titulos2" >Descripcion</td>	  
						<td width="140" class="titulos2" >Tipo</td>
						<td width="140" class="titulos2" >Estado</td>	  	  
					</tr>
					<?php 
						$oculto=$_POST['oculto'];
						$vigusu=vigencia_usuarios($_SESSION['cedulausu']); 
						$cond=" (cuenta like'%".$_POST['numero']."%' or nombre like '%".strtoupper($_POST['numero'])."%')";
						if($_POST['tipo']=='1'){
							$sqlr="Select distinct * from pptocuentas where  clasificacion='ingresos' and".$cond." and vigencia=".$vigusu." order by cuenta";
						}else{
							$sqlr="Select distinct * from pptocuentas where  clasificacion!='ingresos' and".$cond." and vigencia=".$vigusu." order by cuenta";
						}
						$resp = mysqli_query($linkbd,$sqlr);			
						$co='saludo1';
						$co2='saludo2';	
						$i=1;
						while ($r =mysqli_fetch_row($resp)){
							echo"<tr class='$co'";
							if ($r[2]=='Auxiliar'){
								echo" onClick=\"javascript:ponprefijo('$r[0]','$r[1]')\"";
							}
							echo"><td>$i</td>
							<td>".$r[0]."</td>
							<td>".ucwords(strtolower($r[1]))."</td>
							<td>".$r[2]."</td>
							<td>".ucwords(strtolower($r[3]))."</td></tr>";
							$aux=$co;
							$co=$co2;
							$co2=$aux;
							$i=1+$i;
						}
						$_POST['oculto']="";
					?>
				</table>
			</div>
		</form>
	</body>
</html>
