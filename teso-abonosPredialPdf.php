<?php
    require_once("tcpdf/tcpdf_include.php");
	require('comun.inc');
	require "funciones.inc";
    require 'funcionesSP.inc.php';
	date_default_timezone_set("America/Bogota");
	session_start();
	class MYPDF extends TCPDF
	{
		public function Header()
		{
			$linkbd = conectar_v7();
			$linkbd -> set_charset("utf8");
			$sqlr="select *from configbasica where estado='S' ";
			//echo $sqlr;
			$res=mysqli_query($linkbd, $sqlr);
			while($row=mysqli_fetch_row($res))
			{
				$nit=$row[0];
				$rs=$row[1];

			}
			//Parte Izquierda
			$this->Image('imagenes/escudo.jpg',  13, 13, 20, 20, 'JPG', '', 'T', true, 300, '', false, false, 0, false, false, false);// Logo
			$this->SetFont('helvetica','B',8);
			$this->SetY(10);
			$this->RoundedRect(10, 10, 190, 25, 1,'');
			$this->Cell(0.1);
			$this->Cell(26,25,'','R',0,'L');
			$this->SetY(8);
			$this->SetX(40);
			$this->SetFont('helvetica','B',9);
			$this->Cell(140,15,strtoupper("$rs"),0,0,'C');
			$this->SetFont('helvetica','B',7);
			$this->SetY(12);
			$this->SetX(40);
			$this->Cell(140,15,'NIT: '.$nit,0,0,'C');
			//*****************************************************************************************************************************
			$this->SetFont('helvetica','B',9);
			$this->SetY(23);
			$this->SetX(36);
            $this->Cell(164,12,"RECIBO DE ABONO ACUERDO DE PAGO PREDIAL",'T',0,'C');


            $this->SetFont('helvetica','B',7);
			$this->SetY(10);
			$this->SetX(167);
			$this->Cell(30,7," FECHA: ". FECHA,"L",0,'L');
			$this->SetY(17);
			$this->SetX(167);
			$this->Cell(35,6," NO. ABONO: ".CONSECUTIVO,"L",0,'L');

			//**********************************************************
			$this->SetFont('times','B',10);
			$this->ln(12);
			//**********************************************************
            if(MOV=="401"){
                $img_file = './assets/img/reversado.png';
                $this->Image($img_file, 0, 20, 250, 280, '', '', '', false, 300, '', false, false, 0);
            }
		}
		public function Footer()
		{
			$linkbd = conectar_v7();
			$linkbd -> set_charset("utf8");
			$sqlr="SELECT direccion,telefono,web,email FROM configbasica WHERE estado='S'";
			$resp=mysqli_query($linkbd, $sqlr);
			$user = $_SESSION['nickusu'];
            $cedula = $_SESSION['cedulausu'];
			$fecha = date("Y-m-d H:i:s");
			$ip = $_SERVER['REMOTE_ADDR'];
			while($row=mysqli_fetch_row($resp))
			{
				$direcc=strtoupper($row[0]);
				$telefonos=$row[1];
				$dirweb=strtoupper($row[3]);
				$coemail=strtoupper($row[2]);
			}
			if($direcc!=''){$vardirec="Dirección: $direcc, ";}
			else {$vardirec="";}
			if($telefonos!=''){$vartelef="Telefonos: $telefonos";}
			else{$vartelef="";}
			if($dirweb!=''){$varemail="Email: $dirweb, ";}
			else {$varemail="";}
			if($coemail!=''){$varpagiw="Pagina Web: $coemail";}
			else{$varpagiw="";}
			$this->SetFont('helvetica', 'I', 8);
			$txt = <<<EOD
			$vardirec $vartelef
			$varemail $varpagiw
			EOD;
			$this->SetFont('helvetica', 'I', 6);
			$this->Cell(277,10,'','T',0,'T');
			$this->ln(2);
			$this->Write(0, $txt , '', 0, 'C', true, 0, false, false, 0);


            $sqlrcc="SELECT * FROM `usuarios` WHERE `cc_usu` = '$cedula'";
            $respcc=mysqli_query($linkbd, $sqlrcc);
            $rowcc=mysqli_fetch_row($respcc);
            //echo $rowcc[1];

			$this->Cell(50, 10, 'Hecho por: '.$rowcc[1], 00, false, 'C', 0, '', 0, false, 'T', 'M');
			$this->Cell(30, 10, 'Impreso por: '.$user, 0, false, 'C', 0, '', 0, false, 'T', 'M');
			$this->Cell(30, 10, 'IP: '.$ip, 0, false, 'C',0, '', 0, false, 'T', 'M');
			$this->Cell(30, 10, 'Fecha: '.$fecha, 0, false, 'C', 0, '', 0, false, 'T', 'M');
			$this->Cell(30, 10, 'IDEAL.10 S.A.S', 0, false, 'C', 0, '', 0, false, 'T', 'M');
            $this->Cell(20, 10, 'Pagina '.$this->getAliasNumPage().'/'.$this->getAliasNbPages(), 0, false, 'C', 0, '', 0, false, 'T', 'M');
		}
	}

	if($_GET['data']){
        $arrData = json_decode($_GET['data'],true);
        $arrCabecera = $arrData['cabecera'];
        $arrDetalle = $arrData['detalle'];
        define("CONSECUTIVO",$arrCabecera['idabono']);
        define("FECHA",$arrCabecera['fecha']);
        define("MOV",$arrCabecera['tipomovimiento']);
        $pdf = new MYPDF('P','mm','Letter', true, 'iso-8859-1', false);
        $pdf->SetDocInfoUnicode (true);
        // set document information
        $pdf->SetCreator(PDF_CREATOR);
        $pdf->SetAuthor('IDEALSAS');
        $pdf->SetTitle('ABONO ACUERDO DE PAGO PREDIAL');
        $pdf->SetSubject('ABONO ACUERDO DE PAGO PREDIAL');
        $pdf->SetKeywords('TCPDF, PDF, example, test, guide');
        $pdf->SetMargins(10, 38, 10);// set margins
        $pdf->SetHeaderMargin(38);// set margins
        $pdf->SetFooterMargin(17);// set margins
        $pdf->SetAutoPageBreak(TRUE, 20);// set auto page breaks
        // set some language-dependent strings (optional)
        if (@file_exists(dirname(__FILE__).'/lang/spa.php'))
        {
            require_once(dirname(__FILE__).'/lang/spa.php');
            $pdf->setLanguageArray($l);
        }
        $pdf->SetFillColor(255,255,255);
        $pdf->AddPage();
        /*if($arrCabecera['tipomovimiento']=="401"){
            $pdf->SetTextColor(255,0,0);
            $pdf->SetFont('Helvetica','B',30);
            $pdf->Cell(190, 10, 'REVERSADO', 00, false, 'C', 0, '', 0, false, 'T', 'M');
            $pdf->ln();
        }*/
        $pdf->SetTextColor(0,0,0);
        $pdf->SetFont('Helvetica','',9);
        $pdf->MultiCell(40,4,"Contribuyente","LRBT",'L',true,0,'','',true,0,false,true,0,'M',true);
        $pdf->MultiCell(150,4,$arrCabecera['nombre'],"RBT",'L',true,0,'','',true,0,false,true,0,'M',true);
        $pdf->ln();
        $pdf->MultiCell(40,4,"Documento","LRBT",'L',true,0,'','',true,0,false,true,0,'M',true);
        $pdf->MultiCell(150,4,$arrCabecera['tercero'],"RBT",'L',true,0,'','',true,0,false,true,0,'M',true);
        $pdf->ln();
        $pdf->MultiCell(40,4,"Cedula catastral","LRBT",'L',true,0,'','',true,0,false,true,0,'M',true);
        $pdf->MultiCell(150,4,$arrCabecera['codigo'],"RBT",'L',true,0,'','',true,0,false,true,0,'M',true);
        $pdf->ln();
        $pdf->MultiCell(40,4,"Recaudado en ","LRBT",'L',true,0,'','',true,0,false,true,0,'M',true);
        $pdf->MultiCell(150,4,$arrCabecera['recaudado'],"RBT",'L',true,0,'','',true,0,false,true,0,'M',true);
        $pdf->ln();
        if($arrCabecera['recaudado'] == "banco"){
            $pdf->MultiCell(40,4,"Cuenta ","LRBT",'L',true,0,'','',true,0,false,true,0,'M',true);
            $pdf->MultiCell(150,4,$arrCabecera['cuenta_banco']." - ".$arrCabecera['nombre_banco'],"RBT",'L',true,0,'','',true,0,false,true,0,'M',true);
            $pdf->ln();
        }
        $pdf->SetFont('Helvetica','B',9);
        $pdf->SetFillColor(153,221,255);
        $pdf->MultiCell(95,4,"Descripción","LRBT",'L',true,0,'','',true,0,false,true,0,'M',true);
        $pdf->MultiCell(95,4,"Valor","LRBT",'L',true,0,'','',true,0,false,true,0,'M',true);
        $pdf->ln();
        $pdf->SetFont('Helvetica','',9);
        $pdf->SetFillColor(255,255,255);
        $pdf->MultiCell(95,4,$arrCabecera['concepto'],"LRBT",'L',true,0,'','',true,0,false,true,0,'M',true);
        $pdf->MultiCell(95,4,"$".number_format($arrCabecera['valortotal'],0,",","."),"LRBT",'R',true,0,'','',true,0,false,true,0,'M',true);
        $pdf->ln();
        $pdf->ln();
        if(!empty($arrDetalle)){
            $pdf->MultiCell(190,4,"Vigencias liberadas","LRBT",'C',true,0,'','',true,0,false,true,0,'M',true);
            $pdf->ln();
            $pdf->SetFillColor(153,221,255);
            $pdf->SetFont('helvetica','B',6);
            $pdf->MultiCell(11,10,"Vigencia","LRBT",'C',true,0,'','',true,0,false,true,0,'M',true);
            $pdf->MultiCell(13,10,"Avaluo","LRBT",'C',true,0,'','',true,0,false,true,0,'M',true);
            $pdf->MultiCell(8,10,"Tasa x mil","LRBT",'C',true,0,'','',true,0,false,true,0,'M',true);
            $pdf->MultiCell(12,10,"Predial","LRBT",'C',true,0,'','',true,0,false,true,0,'M',true);
            $pdf->MultiCell(15,10,"Descuento incentivo","LRBT",'C',true,0,'','',true,0,false,true,0,'M',true);
            $pdf->MultiCell(13,10,"Recaudo predial","LRBT",'C',true,0,'','',true,0,false,true,0,'M',true);
            $pdf->MultiCell(13,10,"Intereses predial","LRBT",'C',true,0,'','',true,0,false,true,0,'M',true);
            $pdf->MultiCell(15,10,"Descuento intereses predial","LRBT",'C',true,0,'','',true,0,false,true,0,'M',true);
            $pdf->MultiCell(12,10,"Bomberil","LRBT",'C',true,0,'','',true,0,false,true,0,'M',true);
            $pdf->MultiCell(13,10,"Intereses bomberil","LRBT",'C',true,0,'','',true,0,false,true,0,'M',true);
            $pdf->MultiCell(13,10,"Ambiental","LRBT",'C',true,0,'','',true,0,false,true,0,'M',true);
            $pdf->MultiCell(13,10,"Intereses ambiental","LRBT",'C',true,0,'','',true,0,false,true,0,'M',true);
            $pdf->MultiCell(14,10,"Alumbrado","LRBT",'C',true,0,'','',true,0,false,true,0,'M',true);
            $pdf->MultiCell(14,10,"Liquidación","LRBT",'C',true,0,'','',true,0,false,true,0,'M',true);
            $pdf->MultiCell(11,10,"Dias de mora","LRBT",'C',true,0,'','',true,0,false,true,0,'M',true);
            $pdf->ln();
            $pdf->SetFillColor(245,245,245);
            $pdf->SetFont('helvetica','',6);
            $fill = true;
            foreach ($arrDetalle as $data) {
                $pdf->cell(11,4,$data['vigencia'],'',0,'C',$fill);
                $pdf->cell(13,4,"$".number_format($data['valor_avaluo']),'',0,'R',$fill);
                $pdf->cell(8,4,$data['tasa_por_mil'],'',0,'C',$fill);
                $pdf->cell(12,4,"$".number_format($data['predial']),'',0,'R',$fill);
                $pdf->cell(15,4,"$".number_format($data['predial_descuento']),'',0,'R',$fill);
                $pdf->cell(13,4,"$".number_format($data['predial']-$data['predial_descuento']),'',0,'R',$fill);
                $pdf->cell(13,4,"$".number_format($data['predial_intereses']),'',0,'R',$fill);
                $pdf->cell(15,4,"$".number_format($data['predial_descuento_intereses']),'',0,'R',$fill);
                $pdf->cell(12,4,"$".number_format($data['bomberil']),'',0,'R',$fill);
                $pdf->cell(13,4,"$".number_format($data['bomberil_intereses']),'',0,'R',$fill);
                $pdf->cell(13,4,"$".number_format($data['ambiental']),'',0,'R',$fill);
                $pdf->cell(13,4,"$".number_format($data['ambiental_intereses']),'',0,'R',$fill);
                $pdf->cell(14,4,"$".number_format($data['alumbrado']),'',0,'R',$fill);
                $pdf->cell(14,4,"$".number_format($data['total_liquidacion']),'',0,'R',$fill);
                $pdf->cell(11,4,$data['dias_mora'],'',0,'R',$fill);
                $pdf->ln();
                $fill = !$fill;
            }
            $pdf->ln();
            $pdf->ln();
            $pdf->ln();
        }

        //Campo para recibido y sello
        $getY = $pdf->getY();
        $pdf->setX(60);
        $pdf->SetFont('helvetica','B',9);
        $pdf->SetFillColor(153,221,255);
        $pdf->cell(95,4,'Recibido y sello','LRTB',0,'C',1);
        $pdf->ln();
        $pdf->setX(60);
        $pdf->SetFont('helvetica','',7);
        $pdf->SetFillColor(255,255,255);
        $pdf->cell(95,20,'','LRTB',0,'L',1);
        $pdf->ln();
        $pdf->Output('abono_acuerdo_predial_'.CONSECUTIVO.'.pdf', 'I');
    }
?>
