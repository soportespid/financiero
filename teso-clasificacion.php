<?php
    header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=utf8");

	require 'comun.inc';
	require 'funciones.inc';

	session_start();
    date_default_timezone_set("America/Bogota");

?>

<html lang="en">
    <head>
        <meta charset="utf-8"/>
        <meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1"/>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>:: IDEAL 10 - Tesorer&iacute;a</title>
        <link href="favicon.ico" rel="shortcut icon"/>

        <link href="css/css2.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
        <link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
        <link href="css/css4.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
        <script type="text/javascript" src="css/programas.js"></script>
        <script type="text/javascript" src="css/calendario.js"></script>
        <script type="text/javascript" src="JQuery/jquery-2.1.4.min.js"></script>

        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">

        <style>
            [v-cloak]{
                display : none;
            }

            label{
                font-size:13px;
            }

            input{
                height: calc(1em + 0.6rem + 0.5px) !important; 
                font-size: 14px !important; 
                /* margin-top: 4px !important; */
            }
        </style>

    </head>
    <body>
        <IFRAME src="alertas.php" name="alertas" id="alertas" style="display:none"></IFRAME>
        <span id="todastablas2"></span>

        <header>
            <table>
                <tr><script>barra_imagenes("teso");</script><?php cuadro_titulos();?></tr>
            </table>
        </header>

        <section id="myapp" v-cloak>
            <nav>
                <table>
                    <tr><?php menu_desplegable("teso");?></tr>
                    <tr>
                        <td colspan="3" class="cinta">
                            <img src="imagenes/add.png" onclick="location.href='teso-clasificacion.php'" class="mgbt" title="Nuevo" >

                            <img src="imagenes/guarda.png" @click = "guardarClasificador" title="Guardar" class="mgbt">

                            <img src="imagenes/busca.png" class="mgbt" title="Buscar" onclick="location.href='teso-buscaclasificacion.php'">

                            <a onClick="mypop=window.open('teso-principal.php','','');mypop.focus();" class="mgbt"><img src="imagenes/nv.png" title="Nueva ventana"></a>

                            <img src="imagenes/iratras.png" title="Atr&aacute;s" onClick="location.href='teso-gestionclasificacionpredial.php'" class="mgbt">
                        </td>
                    </tr>
                </table>
            </nav>
            <article>
                <table class="inicio ancho">
                    <tr>
                        <td class="titulos" colspan="9" >Clasificaci&oacute;n </td>
                        <td class="cerrar" style="width:7%" onClick="location.href='teso-principal.php'">Cerrar</td>
                    </tr>
                    <tr>
                        <td class = "textoNewR" style = "width: 8%;">
                            <label class="labelR">
                                Consecutivo:
                            </label>
                        </td>
                        <td style = "width: 10%;">
                            <input type="text" style = "width: 90%;" v-model="consecutivo" readonly>
                        </td>

                        <td class = "textoNewR" style = "width: 14%;">
                            <label class="labelR">
                                Nombre Clasificaci&oacute;n:
                            </label>
                        </td>
                        <td>
                            <input type="text" style = "width: 98%;" v-model="nomClasificacion">
                        </td>
                        
                    </tr>
                </table>

                
                    
                <div id="cargando" v-if="loading" class="loading">
                    <span>Cargando...</span>
                </div>

            </article>
        </section>

        <script type="module" src="./tesoreria/predial/teso-clasificacion.js"></script>
        <script src="Librerias/vue/axios.min.js"></script>
        <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/vue/3.2.37/vue.global.min.js"></script> -->
        <!-- <script src="https://unpkg.com/vue@3"></script> -->
        <script src="sweetalert2/dist/sweetalert2.min.js"></script>
        <link rel="stylesheet" href="sweetalert2/dist/sweetalert2.min.css">
        <script src="https://unpkg.com/vue@3/dist/vue.global.js"></script>

    </body>
</html>
