<?php

    header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=utf8");
	require 'comun.inc';
	require 'funciones.inc';
	session_start();
    if(empty($_SESSION)){
        header("location: index.php");
    }
	$linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");
	cargarcodigopag($_GET['codpag'],$_SESSION["nivel"]);
	date_default_timezone_set("America/Bogota");
?>
<!DOCTYPE >
<html lang=es>
	<head>
    <meta charset="utf-8"/>
		<meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1"/>
		<meta name="viewport" content="user-scalable=no">
		<title>:: IDEAL 10 - Tesorería</title>
		<link href="favicon.ico" rel="shortcut icon"/>
		<link href="css/css2z.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
		<link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
		<link href="css/css4.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
        <link href="css/tabs.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css"/>
        <link href="css/style.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css"/>
        <link rel="stylesheet" href="sweetalert2/dist/sweetalert2.min.css">
        <script type="text/javascript" src="css/programas.js"></script>
        <script src="sweetalert2/dist/sweetalert2.min.js"></script>
		<script src="vue/vue.min.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script src="css/programas.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script src="css/calendario.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
	</head>
	<body>
		<header>
			<table>
				<tr><script>barra_imagenes("teso");</script><?php cuadro_titulos();?></tr>
			</table>
		</header>

		<form name="form2" method="post" action="">
			<section id="myapp" v-cloak >
                <div class="loading-container" v-show="isLoading" >
                    <p class="text-loading"data-loading="IDEAL 10">IDEAL 10</p>
                </div>
				<nav>
					<table>
						<tr><?php menu_desplegable("teso");?></tr>
                        <tr>
							<td colspan="3" class="cinta">
                                <img src="imagenes/add2.png"  class="mgbt" title="Nuevo">
								<img src="imagenes/guardad.png"  title="Guardar"  class="mgbt">
								<img src="imagenes/buscad.png"  class="mgbt" title="Buscar">
								<img src="imagenes/nv.png" @click="mypop=window.open('teso-principal.php','',''); mypop.focus();" class="mgbt" title="Nueva Ventana">
                                <img src="imagenes/print.png" title="Imprimir" @click="selectMode=1;exportData();" class="mgbt">
                                <img src="imagenes/excel.png" @click="selectMode=2;exportData();" alt="excel" title="Excel" class="mgbt">
                                <a href="teso-declaracionretenciones.php"  class="mgbt"><img src="imagenes/iratras.png" title="Atr&aacute;s"></a>
                            </td>
						</tr>
					</table>
				</nav>
				<article>
                     <div  class="bg-white">
                        <div>
                            <h2 class="titulos m-0">Declaración de retenciones</h2>
                            <div class="d-flex">
                                <div class="d-flex w-25">
                                    <div class="form-control">
                                        <label class="form-label m-0" for="">Fecha inicial:</label>
                                        <input type="date" v-model="txtFechaInicial" @change="filter()">
                                    </div>
                                    <div class="form-control">
                                        <label class="form-label m-0" for="">Fecha final:</label>
                                        <input type="date" v-model="txtFechaFinal" @change="filter()">
                                    </div>
                                </div>
                                <div class="d-flex w-75">
                                    <div class="form-control w-50">
                                        <label class="form-label m-0" for="">Destino económico:</label>
                                        <select v-model="selectDestino" @change="filter">
                                            <option value="">Otros</option>
                                            <option value="N">Nacional</option>
                                            <option value="D">Departamental</option>
                                            <option value="M">Municipal</option>
                                        </select>
                                    </div>
                                    <div class="form-control">
                                        <label class="form-label m-0" for="">Tipo:</label>
                                        <div class="d-flex">
                                            <select class="w-25" v-model="selectTipo" @change="filter">
                                                <option value="T">Todos</option>
                                                <option value="R">Retenciones</option>
                                                <option value="I">Ingresos</option>
                                            </select>
                                            <select class="w-50" v-model="selectRetenciones">
                                                <option value="">Todos</option>
                                                <option v-for="(data,index) in arrDataCopy" :key="index" :value="data.codigo">{{data.sigla+"-"+data.codigo+"-"+data.nombre}}</option>
                                            </select>
                                            <button type="button" @click="add" class="btn btn-primary">Actualizar</button>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                        <div>
                            <h2 class="titulos m-0">Retenciones e ingresos</h2>
                            <div class="table-responsive">
                                <table  class="table fw-normal">
                                    <thead>
                                        <tr>
                                            <th>Tipo</th>
                                            <th>Nombre</th>
                                            <th>Opciones</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr v-for="(data,index) in arrRetenciones" :key="index">
                                            <td>{{data.sigla+"-"+data.codigo}}</td>
                                            <td>{{data.nombre}}</td>
                                            <td><button type="button" class="btn btn-sm btn-danger" @click="del(data.codigo)">Eliminar</button></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div>
                            <h2 class="titulos m-0">Detalle</h2>
                            <div class="table-responsive">
                                <table  class="table fw-normal">
                                    <thead>
                                        <tr>
                                            <th>Retenciones/ingresos</th>
                                            <th>Tercero</th>
                                            <th>Nombre</th>
                                            <th>No. documento</th>
                                            <th>Tipo documento</th>
                                            <th>Base</th>
                                            <th>Valor</th>
                                        </tr>
                                    </thead>
                                    <tbody ref="tableData"></tbody>
                                    <tfoot>
                                        <tr>
                                            <td class="fw-bold">Valor en letras</td>
                                            <td>{{txtTotalLetras}}</td>
                                            <td colspan="3" class="text-right fw-bold">Total</td>
                                            <td>{{formatNumero(txtCuentas)}}</td>
                                            <td>{{formatNumero(txtTotal)}}</td>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                    </div>
				</article>
			</section>
		</form>

		<script src="node_modules/read-excel-file/bundle/read-excel-file.min.js"></script>
		<script src="node_modules/xlsx/dist/xlsx.full.min.js"></script>
        <script src="Librerias/vue/vue.min.js"></script>
		<script src="Librerias/vue/axios.min.js"></script>
		<script type="module" src="tesoreria/declaracion_retenciones/visualizar/teso-declaracionRetenciones.js?<?= date('d_m_Y_h_i_s');?>"></script>

	</body>
</html>
