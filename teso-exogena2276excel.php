<?php
	ini_set('max_execution_time', 3600);
	require_once 'PHPExcel/Classes/PHPExcel.php';//Incluir la libreria PHPExcel
	include 'PHPExcel/Classes/PHPExcel/IOFactory.php';// PHPExcel_IOFactory
	require "comun.inc";
	require "funciones.inc";
	session_start();
	$linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");
	$objPHPExcel = new PHPExcel();// Crea un nuevo objeto PHPExcel
	$objReader = PHPExcel_IOFactory::createReader('Excel2007');// Leemos un archivo Excel 2007
	$objPHPExcel = $objReader->load("formatos/Formato 2276.xlsx");
	$borders = array(
      'borders' => array(
        'allborders' => array(
          'style' => PHPExcel_Style_Border::BORDER_THIN,
          'color' => array('argb' => 'FF000000'),
        )
      ),
    );
	// Agregar Informacion
	$sqlr="select distinct concepto,tercero,sum(valor),id_cxp from exogena_det_2276 where id_exo='$_POST[idexo]' AND tipo!='SE' AND tipo!='PE' group by concepto,tercero order by concepto,tercero";
	$res=mysqli_query($linkbd, $sqlr);
	$xy=4;
	while ($row = mysqli_fetch_row($res))
	{
		$_POST['nomina'][]=$row[3];
		if($row[0] == '101')
		{
			$_POST['pagoSalarios'][]=$row[2];
			$_POST['eclesiasticos'][]=0;
			$_POST['pagoAbonoElectronico'][]=0;//nueva
			$_POST['valorExcesoPagoAlimentacion'][]=0;//nueva
			$_POST['honorario'][]=0;
			$_POST['pagoPorServicio'][]=0;
			$_POST['pagoPorComisiones'][]=0;
			$_POST['pagoPorPrestasionesSociales'][]=0;
			$_POST['pagoPorViaticos'][]=0;
			$_POST['pagoPorRepresentacion'][]=0;
			$_POST['pagoPorCompensaciones'][]=0;
			$_POST['apoyoEconomicoNoReembolso'][]=0;//nueva
			$_POST['otrosPagos'][]=0;
			$_POST['interesesCesantias'][]=0;
			$_POST['cesantiasFondoCesantias'][]=0;
			$_POST['auxilioCesantias'][]=0;
			$_POST['pensionesJubilacion'][]=0;
			$_POST['rentasTrabajoPrension'][]=0;
			$_POST['aportesSalud'][]=0;
			$_POST['aportesPensiones'][]=0;
			$_POST['aporteVoluntarioRegimenIndividual'][]=0;
			$_POST['aportesPensionesVoluntarios'][]=0;
			$_POST['cuentasAFC'][]=0;
			$_POST['cuentasAVC'][]=0;
			$_POST['retencionesFuenteRentas'][]=0;
			$_POST['iva'][]=0;
			$_POST['retencionIva'][]=0;
			$_POST['pagoAlimentacion'][]=0;
			$_POST['ingresoLaboralPromedio'][]=0;
			$_POST['terceros1'][] = $row[1];
		}
		else if($row[0] == '102')
		{
            $_POST['pagoSalarios'][]=0;
			$_POST['eclesiasticos'][]=$row[2];
			$_POST['pagoAbonoElectronico'][]=0;//nueva
			$_POST['valorExcesoPagoAlimentacion'][]=0;//nueva
			$_POST['honorario'][]=0;
			$_POST['pagoPorServicio'][]=0;
			$_POST['pagoPorComisiones'][]=0;
			$_POST['pagoPorPrestasionesSociales'][]=0;
			$_POST['pagoPorViaticos'][]=0;
			$_POST['pagoPorRepresentacion'][]=0;
			$_POST['pagoPorCompensaciones'][]=0;
			$_POST['apoyoEconomicoNoReembolso'][]=0;//nueva
			$_POST['otrosPagos'][]=0;
			$_POST['interesesCesantias'][]=0;
			$_POST['cesantiasFondoCesantias'][]=0;
			$_POST['auxilioCesantias'][]=0;
			$_POST['pensionesJubilacion'][]=0;
			$_POST['rentasTrabajoPrension'][]=0;
			$_POST['aportesSalud'][]=0;
			$_POST['aportesPensiones'][]=0;
			$_POST['aporteVoluntarioRegimenIndividual'][]=0;
			$_POST['aportesPensionesVoluntarios'][]=0;
			$_POST['cuentasAFC'][]=0;
			$_POST['cuentasAVC'][]=0;
			$_POST['retencionesFuenteRentas'][]=0;
			$_POST['iva'][]=0;
			$_POST['retencionIva'][]=0;
			$_POST['pagoAlimentacion'][]=0;
			$_POST['ingresoLaboralPromedio'][]=0;
			$_POST['terceros1'][] = $row[1];
		}
		else if($row[0] == '103')
		{
            $_POST['pagoSalarios'][]=0;
			$_POST['eclesiasticos'][]=0;
			$_POST['pagoAbonoElectronico'][]=$row[2];//nueva
			$_POST['valorExcesoPagoAlimentacion'][]=0;//nueva
			$_POST['honorario'][]=0;
			$_POST['pagoPorServicio'][]=0;
			$_POST['pagoPorComisiones'][]=0;
			$_POST['pagoPorPrestasionesSociales'][]=0;
			$_POST['pagoPorViaticos'][]=0;
			$_POST['pagoPorRepresentacion'][]=0;
			$_POST['pagoPorCompensaciones'][]=0;
			$_POST['apoyoEconomicoNoReembolso'][]=0;//nueva
			$_POST['otrosPagos'][]=0;
			$_POST['interesesCesantias'][]=0;
			$_POST['cesantiasFondoCesantias'][]=0;
			$_POST['auxilioCesantias'][]=0;
			$_POST['pensionesJubilacion'][]=0;
			$_POST['rentasTrabajoPrension'][]=0;
			$_POST['aportesSalud'][]=0;
			$_POST['aportesPensiones'][]=0;
			$_POST['aporteVoluntarioRegimenIndividual'][]=0;
			$_POST['aportesPensionesVoluntarios'][]=0;
			$_POST['cuentasAFC'][]=0;
			$_POST['cuentasAVC'][]=0;
			$_POST['retencionesFuenteRentas'][]=0;
			$_POST['iva'][]=0;
			$_POST['retencionIva'][]=0;
			$_POST['pagoAlimentacion'][]=0;
			$_POST['ingresoLaboralPromedio'][]=0;
			$_POST['terceros1'][] = $row[1];


		}
		else if($row[0] == '104')
		{
            $_POST['pagoSalarios'][]=0;
			$_POST['eclesiasticos'][]=0;
			$_POST['pagoAbonoElectronico'][]=0;//nueva
			$_POST['valorExcesoPagoAlimentacion'][]=$row[2];//nueva
			$_POST['honorario'][]=0;
			$_POST['pagoPorServicio'][]=0;
			$_POST['pagoPorComisiones'][]=0;
			$_POST['pagoPorPrestasionesSociales'][]=0;
			$_POST['pagoPorViaticos'][]=0;
			$_POST['pagoPorRepresentacion'][]=0;
			$_POST['pagoPorCompensaciones'][]=0;
			$_POST['apoyoEconomicoNoReembolso'][]=0;//nueva
			$_POST['otrosPagos'][]=0;
			$_POST['interesesCesantias'][]=0;
			$_POST['cesantiasFondoCesantias'][]=0;
			$_POST['auxilioCesantias'][]=0;
			$_POST['pensionesJubilacion'][]=0;
			$_POST['rentasTrabajoPrension'][]=0;
			$_POST['aportesSalud'][]=0;
			$_POST['aportesPensiones'][]=0;
			$_POST['aporteVoluntarioRegimenIndividual'][]=0;
			$_POST['aportesPensionesVoluntarios'][]=0;
			$_POST['cuentasAFC'][]=0;
			$_POST['cuentasAVC'][]=0;
			$_POST['retencionesFuenteRentas'][]=0;
			$_POST['iva'][]=0;
			$_POST['retencionIva'][]=0;
			$_POST['pagoAlimentacion'][]=0;
			$_POST['ingresoLaboralPromedio'][]=0;
			$_POST['terceros1'][] = $row[1];


		}
		else if($row[0] == '105')
		{
			$_POST['pagoSalarios'][]=0;
			$_POST['eclesiasticos'][]=0;
			$_POST['pagoAbonoElectronico'][]=0;//nueva
			$_POST['valorExcesoPagoAlimentacion'][]=0;//nueva
			$_POST['honorario'][]=$row[2];
			$_POST['pagoPorServicio'][]=0;
			$_POST['pagoPorComisiones'][]=0;
			$_POST['pagoPorPrestasionesSociales'][]=0;
			$_POST['pagoPorViaticos'][]=0;
			$_POST['pagoPorRepresentacion'][]=0;
			$_POST['pagoPorCompensaciones'][]=0;
			$_POST['apoyoEconomicoNoReembolso'][]=0;//nueva
			$_POST['otrosPagos'][]=0;
			$_POST['interesesCesantias'][]=0;
			$_POST['cesantiasFondoCesantias'][]=0;
			$_POST['auxilioCesantias'][]=0;
			$_POST['pensionesJubilacion'][]=0;
			$_POST['rentasTrabajoPrension'][]=0;
			$_POST['aportesSalud'][]=0;
			$_POST['aportesPensiones'][]=0;
			$_POST['aporteVoluntarioRegimenIndividual'][]=0;
			$_POST['aportesPensionesVoluntarios'][]=0;
			$_POST['cuentasAFC'][]=0;
			$_POST['cuentasAVC'][]=0;
			$_POST['retencionesFuenteRentas'][]=0;
			$_POST['iva'][]=0;
			$_POST['retencionIva'][]=0;
			$_POST['pagoAlimentacion'][]=0;
			$_POST['ingresoLaboralPromedio'][]=0;
			$_POST['terceros1'][] = $row[1];
		}
		else if($row[0] == '106')
		{
			$_POST['pagoSalarios'][]=0;
			$_POST['eclesiasticos'][]=0;
			$_POST['pagoAbonoElectronico'][]=0;//nueva
			$_POST['valorExcesoPagoAlimentacion'][]=0;//nueva
			$_POST['honorario'][]=0;
			$_POST['pagoPorServicio'][]=$row[2];
			$_POST['pagoPorComisiones'][]=0;
			$_POST['pagoPorPrestasionesSociales'][]=0;
			$_POST['pagoPorViaticos'][]=0;
			$_POST['pagoPorRepresentacion'][]=0;
			$_POST['pagoPorCompensaciones'][]=0;
			$_POST['apoyoEconomicoNoReembolso'][]=0;//nueva
			$_POST['otrosPagos'][]=0;
			$_POST['interesesCesantias'][]=0;
			$_POST['cesantiasFondoCesantias'][]=0;
			$_POST['auxilioCesantias'][]=0;
			$_POST['pensionesJubilacion'][]=0;
			$_POST['rentasTrabajoPrension'][]=0;
			$_POST['aportesSalud'][]=0;
			$_POST['aportesPensiones'][]=0;
			$_POST['aporteVoluntarioRegimenIndividual'][]=0;
			$_POST['aportesPensionesVoluntarios'][]=0;
			$_POST['cuentasAFC'][]=0;
			$_POST['cuentasAVC'][]=0;
			$_POST['retencionesFuenteRentas'][]=0;
			$_POST['iva'][]=0;
			$_POST['retencionIva'][]=0;
			$_POST['pagoAlimentacion'][]=0;
			$_POST['ingresoLaboralPromedio'][]=0;
			$_POST['terceros1'][] = $row[1];
		}
		else if($row[0] == '107')
		{
			$_POST['pagoSalarios'][]=0;
			$_POST['eclesiasticos'][]=0;
			$_POST['pagoAbonoElectronico'][]=0;//nueva
			$_POST['valorExcesoPagoAlimentacion'][]=0;//nueva
			$_POST['honorario'][]=0;
			$_POST['pagoPorServicio'][]=0;
			$_POST['pagoPorComisiones'][]=$row[2];
			$_POST['pagoPorPrestasionesSociales'][]=0;
			$_POST['pagoPorViaticos'][]=0;
			$_POST['pagoPorRepresentacion'][]=0;
			$_POST['pagoPorCompensaciones'][]=0;
			$_POST['apoyoEconomicoNoReembolso'][]=0;//nueva
			$_POST['otrosPagos'][]=0;
			$_POST['interesesCesantias'][]=0;
			$_POST['cesantiasFondoCesantias'][]=0;
			$_POST['auxilioCesantias'][]=0;
			$_POST['pensionesJubilacion'][]=0;
			$_POST['rentasTrabajoPrension'][]=0;
			$_POST['aportesSalud'][]=0;
			$_POST['aportesPensiones'][]=0;
			$_POST['aporteVoluntarioRegimenIndividual'][]=0;
			$_POST['aportesPensionesVoluntarios'][]=0;
			$_POST['cuentasAFC'][]=0;
			$_POST['cuentasAVC'][]=0;
			$_POST['retencionesFuenteRentas'][]=0;
			$_POST['iva'][]=0;
			$_POST['retencionIva'][]=0;
			$_POST['pagoAlimentacion'][]=0;
			$_POST['ingresoLaboralPromedio'][]=0;
			$_POST['terceros1'][] = $row[1];
		}
		else if($row[0] == '108')
		{
			$_POST['pagoSalarios'][]=0;
			$_POST['eclesiasticos'][]=0;
			$_POST['pagoAbonoElectronico'][]=0;//nueva
			$_POST['valorExcesoPagoAlimentacion'][]=0;//nueva
			$_POST['honorario'][]=0;
			$_POST['pagoPorServicio'][]=0;
			$_POST['pagoPorComisiones'][]=0;
			$_POST['pagoPorPrestasionesSociales'][]=$row[2];
			$_POST['pagoPorViaticos'][]=0;
			$_POST['pagoPorRepresentacion'][]=0;
			$_POST['pagoPorCompensaciones'][]=0;
			$_POST['apoyoEconomicoNoReembolso'][]=0;//nueva
			$_POST['otrosPagos'][]=0;
			$_POST['interesesCesantias'][]=0;
			$_POST['cesantiasFondoCesantias'][]=0;
			$_POST['auxilioCesantias'][]=0;
			$_POST['pensionesJubilacion'][]=0;
			$_POST['rentasTrabajoPrension'][]=0;
			$_POST['aportesSalud'][]=0;
			$_POST['aportesPensiones'][]=0;
			$_POST['aporteVoluntarioRegimenIndividual'][]=0;
			$_POST['aportesPensionesVoluntarios'][]=0;
			$_POST['cuentasAFC'][]=0;
			$_POST['cuentasAVC'][]=0;
			$_POST['retencionesFuenteRentas'][]=0;
			$_POST['iva'][]=0;
			$_POST['retencionIva'][]=0;
			$_POST['pagoAlimentacion'][]=0;
			$_POST['ingresoLaboralPromedio'][]=0;
			$_POST['terceros1'][] = $row[1];
		}
		else if($row[0] == '109')
		{
			$_POST['pagoSalarios'][]=0;
			$_POST['eclesiasticos'][]=0;
			$_POST['pagoAbonoElectronico'][]=0;//nueva
			$_POST['valorExcesoPagoAlimentacion'][]=0;//nueva
			$_POST['honorario'][]=0;
			$_POST['pagoPorServicio'][]=0;
			$_POST['pagoPorComisiones'][]=0;
			$_POST['pagoPorPrestasionesSociales'][]=0;
			$_POST['pagoPorViaticos'][]=$row[2];
			$_POST['pagoPorRepresentacion'][]=0;
			$_POST['pagoPorCompensaciones'][]=0;
			$_POST['apoyoEconomicoNoReembolso'][]=0;//nueva
			$_POST['otrosPagos'][]=0;
			$_POST['interesesCesantias'][]=0;
			$_POST['cesantiasFondoCesantias'][]=0;
			$_POST['auxilioCesantias'][]=0;
			$_POST['pensionesJubilacion'][]=0;
			$_POST['rentasTrabajoPrension'][]=0;
			$_POST['aportesSalud'][]=0;
			$_POST['aportesPensiones'][]=0;
			$_POST['aporteVoluntarioRegimenIndividual'][]=0;
			$_POST['aportesPensionesVoluntarios'][]=0;
			$_POST['cuentasAFC'][]=0;
			$_POST['cuentasAVC'][]=0;
			$_POST['retencionesFuenteRentas'][]=0;
			$_POST['iva'][]=0;
			$_POST['retencionIva'][]=0;
			$_POST['pagoAlimentacion'][]=0;
			$_POST['ingresoLaboralPromedio'][]=0;
			$_POST['terceros1'][] = $row[1];
		}
		else if($row[0] == '110')
		{
			$_POST['pagoSalarios'][]=0;
			$_POST['eclesiasticos'][]=0;
			$_POST['pagoAbonoElectronico'][]=0;//nueva
			$_POST['valorExcesoPagoAlimentacion'][]=0;//nueva
			$_POST['honorario'][]=0;
			$_POST['pagoPorServicio'][]=0;
			$_POST['pagoPorComisiones'][]=0;
			$_POST['pagoPorPrestasionesSociales'][]=0;
			$_POST['pagoPorViaticos'][]=0;
			$_POST['pagoPorRepresentacion'][]=$row[2];
			$_POST['pagoPorCompensaciones'][]=0;
			$_POST['apoyoEconomicoNoReembolso'][]=0;//nueva
			$_POST['otrosPagos'][]=0;
			$_POST['interesesCesantias'][]=0;
			$_POST['cesantiasFondoCesantias'][]=0;
			$_POST['auxilioCesantias'][]=0;
			$_POST['pensionesJubilacion'][]=0;
			$_POST['rentasTrabajoPrension'][]=0;
			$_POST['aportesSalud'][]=0;
			$_POST['aportesPensiones'][]=0;
			$_POST['aporteVoluntarioRegimenIndividual'][]=0;
			$_POST['aportesPensionesVoluntarios'][]=0;
			$_POST['cuentasAFC'][]=0;
			$_POST['cuentasAVC'][]=0;
			$_POST['retencionesFuenteRentas'][]=0;
			$_POST['iva'][]=0;
			$_POST['retencionIva'][]=0;
			$_POST['pagoAlimentacion'][]=0;
			$_POST['ingresoLaboralPromedio'][]=0;
			$_POST['terceros1'][] = $row[1];
		}
		else if($row[0] == '111')
		{
			$_POST['pagoSalarios'][]=0;
			$_POST['eclesiasticos'][]=0;
			$_POST['pagoAbonoElectronico'][]=0;//nueva
			$_POST['valorExcesoPagoAlimentacion'][]=0;//nueva
			$_POST['honorario'][]=0;
			$_POST['pagoPorServicio'][]=0;
			$_POST['pagoPorComisiones'][]=0;
			$_POST['pagoPorPrestasionesSociales'][]=0;
			$_POST['pagoPorViaticos'][]=0;
			$_POST['pagoPorRepresentacion'][]=0;
			$_POST['pagoPorCompensaciones'][]=$row[2];
			$_POST['apoyoEconomicoNoReembolso'][]=0;//nueva
			$_POST['otrosPagos'][]=0;
			$_POST['interesesCesantias'][]=0;
			$_POST['cesantiasFondoCesantias'][]=0;
			$_POST['auxilioCesantias'][]=0;
			$_POST['pensionesJubilacion'][]=0;
			$_POST['rentasTrabajoPrension'][]=0;
			$_POST['aportesSalud'][]=0;
			$_POST['aportesPensiones'][]=0;
			$_POST['aporteVoluntarioRegimenIndividual'][]=0;
			$_POST['aportesPensionesVoluntarios'][]=0;
			$_POST['cuentasAFC'][]=0;
			$_POST['cuentasAVC'][]=0;
			$_POST['retencionesFuenteRentas'][]=0;
			$_POST['iva'][]=0;
			$_POST['retencionIva'][]=0;
			$_POST['pagoAlimentacion'][]=0;
			$_POST['ingresoLaboralPromedio'][]=0;
			$_POST['terceros1'][] = $row[1];
		}
		else if($row[0] == '112')
		{
			$_POST['pagoSalarios'][]=0;
			$_POST['eclesiasticos'][]=0;
			$_POST['pagoAbonoElectronico'][]=0;//nueva
			$_POST['valorExcesoPagoAlimentacion'][]=0;//nueva
			$_POST['honorario'][]=0;
			$_POST['pagoPorServicio'][]=0;
			$_POST['pagoPorComisiones'][]=0;
			$_POST['pagoPorPrestasionesSociales'][]=0;
			$_POST['pagoPorViaticos'][]=0;
			$_POST['pagoPorRepresentacion'][]=0;
			$_POST['pagoPorCompensaciones'][]=0;
			$_POST['apoyoEconomicoNoReembolso'][]=$row[2];//nueva
			$_POST['otrosPagos'][]=0;
			$_POST['interesesCesantias'][]=0;
			$_POST['cesantiasFondoCesantias'][]=0;
			$_POST['auxilioCesantias'][]=0;
			$_POST['pensionesJubilacion'][]=0;
			$_POST['rentasTrabajoPrension'][]=0;
			$_POST['aportesSalud'][]=0;
			$_POST['aportesPensiones'][]=0;
			$_POST['aporteVoluntarioRegimenIndividual'][]=0;
			$_POST['aportesPensionesVoluntarios'][]=0;
			$_POST['cuentasAFC'][]=0;
			$_POST['cuentasAVC'][]=0;
			$_POST['retencionesFuenteRentas'][]=0;
			$_POST['iva'][]=0;
			$_POST['retencionIva'][]=0;
			$_POST['pagoAlimentacion'][]=0;
			$_POST['ingresoLaboralPromedio'][]=0;
			$_POST['terceros1'][] = $row[1];
		}
		else if($row[0] == '113')
		{
			$_POST['pagoSalarios'][]=0;
			$_POST['eclesiasticos'][]=0;
			$_POST['pagoAbonoElectronico'][]=0;//nueva
			$_POST['valorExcesoPagoAlimentacion'][]=0;//nueva
			$_POST['honorario'][]=0;
			$_POST['pagoPorServicio'][]=0;
			$_POST['pagoPorComisiones'][]=0;
			$_POST['pagoPorPrestasionesSociales'][]=0;
			$_POST['pagoPorViaticos'][]=0;
			$_POST['pagoPorRepresentacion'][]=0;
			$_POST['pagoPorCompensaciones'][]=0;
			$_POST['apoyoEconomicoNoReembolso'][]=0;//nueva
			$_POST['otrosPagos'][]=$row[2];
			$_POST['interesesCesantias'][]=0;
			$_POST['cesantiasFondoCesantias'][]=0;
			$_POST['auxilioCesantias'][]=0;
			$_POST['pensionesJubilacion'][]=0;
			$_POST['rentasTrabajoPrension'][]=0;
			$_POST['aportesSalud'][]=0;
			$_POST['aportesPensiones'][]=0;
			$_POST['aporteVoluntarioRegimenIndividual'][]=0;
			$_POST['aportesPensionesVoluntarios'][]=0;
			$_POST['cuentasAFC'][]=0;
			$_POST['cuentasAVC'][]=0;
			$_POST['retencionesFuenteRentas'][]=0;
			$_POST['iva'][]=0;
			$_POST['retencionIva'][]=0;
			$_POST['pagoAlimentacion'][]=0;
			$_POST['ingresoLaboralPromedio'][]=0;
			$_POST['terceros1'][] = $row[1];
		}
		else if($row[0] == '114')
		{
			$_POST['pagoSalarios'][]=0;
			$_POST['eclesiasticos'][]=0;
			$_POST['pagoAbonoElectronico'][]=0;//nueva
			$_POST['valorExcesoPagoAlimentacion'][]=0;//nueva
			$_POST['honorario'][]=0;
			$_POST['pagoPorServicio'][]=0;
			$_POST['pagoPorComisiones'][]=0;
			$_POST['pagoPorPrestasionesSociales'][]=0;
			$_POST['pagoPorViaticos'][]=0;
			$_POST['pagoPorRepresentacion'][]=0;
			$_POST['pagoPorCompensaciones'][]=0;
			$_POST['apoyoEconomicoNoReembolso'][]=0;//nueva
			$_POST['otrosPagos'][]=0;
			$_POST['interesesCesantias'][]=$row[2];
			$_POST['cesantiasFondoCesantias'][]=0;
			$_POST['auxilioCesantias'][]=0;
			$_POST['pensionesJubilacion'][]=0;
			$_POST['rentasTrabajoPrension'][]=0;
			$_POST['aportesSalud'][]=0;
			$_POST['aportesPensiones'][]=0;
			$_POST['aporteVoluntarioRegimenIndividual'][]=0;
			$_POST['aportesPensionesVoluntarios'][]=0;
			$_POST['cuentasAFC'][]=0;
			$_POST['cuentasAVC'][]=0;
			$_POST['retencionesFuenteRentas'][]=0;
			$_POST['iva'][]=0;
			$_POST['retencionIva'][]=0;
			$_POST['pagoAlimentacion'][]=0;
			$_POST['ingresoLaboralPromedio'][]=0;
			$_POST['terceros1'][] = $row[1];
		}
		else if($row[0] == '115')
		{
			$_POST['pagoSalarios'][]=0;
			$_POST['eclesiasticos'][]=0;
			$_POST['pagoAbonoElectronico'][]=0;//nueva
			$_POST['valorExcesoPagoAlimentacion'][]=0;//nueva
			$_POST['honorario'][]=0;
			$_POST['pagoPorServicio'][]=0;
			$_POST['pagoPorComisiones'][]=0;
			$_POST['pagoPorPrestasionesSociales'][]=0;
			$_POST['pagoPorViaticos'][]=0;
			$_POST['pagoPorRepresentacion'][]=0;
			$_POST['pagoPorCompensaciones'][]=0;
			$_POST['apoyoEconomicoNoReembolso'][]=0;//nueva
			$_POST['otrosPagos'][]=0;
			$_POST['interesesCesantias'][]=0;
			$_POST['cesantiasFondoCesantias'][]=$row[2];
			$_POST['auxilioCesantias'][]=0;
			$_POST['pensionesJubilacion'][]=0;
			$_POST['rentasTrabajoPrension'][]=0;
			$_POST['aportesSalud'][]=0;
			$_POST['aportesPensiones'][]=0;
			$_POST['aporteVoluntarioRegimenIndividual'][]=0;
			$_POST['aportesPensionesVoluntarios'][]=0;
			$_POST['cuentasAFC'][]=0;
			$_POST['cuentasAVC'][]=0;
			$_POST['retencionesFuenteRentas'][]=0;
			$_POST['iva'][]=0;
			$_POST['retencionIva'][]=0;
			$_POST['pagoAlimentacion'][]=0;
			$_POST['ingresoLaboralPromedio'][]=0;
			$_POST['terceros1'][] = $row[1];
		}
		else if($row[0] == '116')
		{
			$_POST['pagoSalarios'][]=0;
			$_POST['eclesiasticos'][]=0;
			$_POST['pagoAbonoElectronico'][]=0;//nueva
			$_POST['valorExcesoPagoAlimentacion'][]=0;//nueva
			$_POST['honorario'][]=0;
			$_POST['pagoPorServicio'][]=0;
			$_POST['pagoPorComisiones'][]=0;
			$_POST['pagoPorPrestasionesSociales'][]=0;
			$_POST['pagoPorViaticos'][]=0;
			$_POST['pagoPorRepresentacion'][]=0;
			$_POST['pagoPorCompensaciones'][]=0;
			$_POST['apoyoEconomicoNoReembolso'][]=0;//nueva
			$_POST['otrosPagos'][]=0;
			$_POST['interesesCesantias'][]=0;
			$_POST['cesantiasFondoCesantias'][]=0;
			$_POST['auxilioCesantias'][]=$row[2];
			$_POST['pensionesJubilacion'][]=0;
			$_POST['rentasTrabajoPrension'][]=0;
			$_POST['aportesSalud'][]=0;
			$_POST['aportesPensiones'][]=0;
			$_POST['aporteVoluntarioRegimenIndividual'][]=0;
			$_POST['aportesPensionesVoluntarios'][]=0;
			$_POST['cuentasAFC'][]=0;
			$_POST['cuentasAVC'][]=0;
			$_POST['retencionesFuenteRentas'][]=0;
			$_POST['iva'][]=0;
			$_POST['retencionIva'][]=0;
			$_POST['pagoAlimentacion'][]=0;
			$_POST['ingresoLaboralPromedio'][]=0;
			$_POST['terceros1'][] = $row[1];
		}
		else if($row[0] == '117')
		{
			$_POST['pagoSalarios'][]=0;
			$_POST['eclesiasticos'][]=0;
			$_POST['pagoAbonoElectronico'][]=0;//nueva
			$_POST['valorExcesoPagoAlimentacion'][]=0;//nueva
			$_POST['honorario'][]=0;
			$_POST['pagoPorServicio'][]=0;
			$_POST['pagoPorComisiones'][]=0;
			$_POST['pagoPorPrestasionesSociales'][]=0;
			$_POST['pagoPorViaticos'][]=0;
			$_POST['pagoPorRepresentacion'][]=0;
			$_POST['pagoPorCompensaciones'][]=0;
			$_POST['apoyoEconomicoNoReembolso'][]=0;//nueva
			$_POST['otrosPagos'][]=0;
			$_POST['interesesCesantias'][]=0;
			$_POST['cesantiasFondoCesantias'][]=0;
			$_POST['auxilioCesantias'][]=0;
			$_POST['pensionesJubilacion'][]=$row[2];
			$_POST['rentasTrabajoPrension'][]=0;
			$_POST['aportesSalud'][]=0;
			$_POST['aportesPensiones'][]=0;
			$_POST['aporteVoluntarioRegimenIndividual'][]=0;
			$_POST['aportesPensionesVoluntarios'][]=0;
			$_POST['cuentasAFC'][]=0;
			$_POST['cuentasAVC'][]=0;
			$_POST['retencionesFuenteRentas'][]=0;
			$_POST['iva'][]=0;
			$_POST['retencionIva'][]=0;
			$_POST['pagoAlimentacion'][]=0;
			$_POST['ingresoLaboralPromedio'][]=0;
			$_POST['terceros1'][] = $row[1];
		}
		else if($row[0] == '118')
		{
			$_POST['pagoSalarios'][]=0;
			$_POST['eclesiasticos'][]=0;
			$_POST['pagoAbonoElectronico'][]=0;//nueva
			$_POST['valorExcesoPagoAlimentacion'][]=0;//nueva
			$_POST['honorario'][]=0;
			$_POST['pagoPorServicio'][]=0;
			$_POST['pagoPorComisiones'][]=0;
			$_POST['pagoPorPrestasionesSociales'][]=0;
			$_POST['pagoPorViaticos'][]=0;
			$_POST['pagoPorRepresentacion'][]=0;
			$_POST['pagoPorCompensaciones'][]=0;
			$_POST['apoyoEconomicoNoReembolso'][]=0;//nueva
			$_POST['otrosPagos'][]=0;
			$_POST['interesesCesantias'][]=0;
			$_POST['cesantiasFondoCesantias'][]=0;
			$_POST['auxilioCesantias'][]=0;
			$_POST['pensionesJubilacion'][]=0;
			$_POST['rentasTrabajoPrension'][]=$row[2];
			$_POST['aportesSalud'][]=0;
			$_POST['aportesPensiones'][]=0;
			$_POST['aporteVoluntarioRegimenIndividual'][]=0;
			$_POST['aportesPensionesVoluntarios'][]=0;
			$_POST['cuentasAFC'][]=0;
			$_POST['cuentasAVC'][]=0;
			$_POST['retencionesFuenteRentas'][]=0;
			$_POST['iva'][]=0;
			$_POST['retencionIva'][]=0;
			$_POST['pagoAlimentacion'][]=0;
			$_POST['ingresoLaboralPromedio'][]=0;
			$_POST['terceros1'][] = $row[1];
		}
        else if($row[0] == '119')
		{
			$_POST['pagoSalarios'][]=0;
			$_POST['eclesiasticos'][]=0;
			$_POST['pagoAbonoElectronico'][]=0;//nueva
			$_POST['valorExcesoPagoAlimentacion'][]=0;//nueva
			$_POST['honorario'][]=0;
			$_POST['pagoPorServicio'][]=0;
			$_POST['pagoPorComisiones'][]=0;
			$_POST['pagoPorPrestasionesSociales'][]=0;
			$_POST['pagoPorViaticos'][]=0;
			$_POST['pagoPorRepresentacion'][]=0;
			$_POST['pagoPorCompensaciones'][]=0;
			$_POST['apoyoEconomicoNoReembolso'][]=0;//nueva
			$_POST['otrosPagos'][]=0;
			$_POST['interesesCesantias'][]=0;
			$_POST['cesantiasFondoCesantias'][]=0;
			$_POST['auxilioCesantias'][]=0;
			$_POST['pensionesJubilacion'][]=0;
			$_POST['rentasTrabajoPrension'][]=0;
			$_POST['aportesSalud'][]=$row[2];
			$_POST['aportesPensiones'][]=0;
			$_POST['aporteVoluntarioRegimenIndividual'][]=0;
			$_POST['aportesPensionesVoluntarios'][]=0;
			$_POST['cuentasAFC'][]=0;
			$_POST['cuentasAVC'][]=0;
			$_POST['retencionesFuenteRentas'][]=0;
			$_POST['iva'][]=0;
			$_POST['retencionIva'][]=0;
			$_POST['pagoAlimentacion'][]=0;
			$_POST['ingresoLaboralPromedio'][]=0;
			$_POST['terceros1'][] = $row[1];
		}
        else if($row[0] == '120')
		{
			$_POST['pagoSalarios'][]=0;
			$_POST['eclesiasticos'][]=0;
			$_POST['pagoAbonoElectronico'][]=0;//nueva
			$_POST['valorExcesoPagoAlimentacion'][]=0;//nueva
			$_POST['honorario'][]=0;
			$_POST['pagoPorServicio'][]=0;
			$_POST['pagoPorComisiones'][]=0;
			$_POST['pagoPorPrestasionesSociales'][]=0;
			$_POST['pagoPorViaticos'][]=0;
			$_POST['pagoPorRepresentacion'][]=0;
			$_POST['pagoPorCompensaciones'][]=0;
			$_POST['apoyoEconomicoNoReembolso'][]=0;//nueva
			$_POST['otrosPagos'][]=0;
			$_POST['interesesCesantias'][]=0;
			$_POST['cesantiasFondoCesantias'][]=0;
			$_POST['auxilioCesantias'][]=0;
			$_POST['pensionesJubilacion'][]=0;
			$_POST['rentasTrabajoPrension'][]=0;
			$_POST['aportesSalud'][]=0;
			$_POST['aportesPensiones'][]=$row[2];
			$_POST['aporteVoluntarioRegimenIndividual'][]=0;
			$_POST['aportesPensionesVoluntarios'][]=0;
			$_POST['cuentasAFC'][]=0;
			$_POST['cuentasAVC'][]=0;
			$_POST['retencionesFuenteRentas'][]=0;
			$_POST['iva'][]=0;
			$_POST['retencionIva'][]=0;
			$_POST['pagoAlimentacion'][]=0;
			$_POST['ingresoLaboralPromedio'][]=0;
			$_POST['terceros1'][] = $row[1];
		}
        else if($row[0] == '121')
		{
			$_POST['pagoSalarios'][]=0;
			$_POST['eclesiasticos'][]=0;
			$_POST['pagoAbonoElectronico'][]=0;//nueva
			$_POST['valorExcesoPagoAlimentacion'][]=0;//nueva
			$_POST['honorario'][]=0;
			$_POST['pagoPorServicio'][]=0;
			$_POST['pagoPorComisiones'][]=0;
			$_POST['pagoPorPrestasionesSociales'][]=0;
			$_POST['pagoPorViaticos'][]=0;
			$_POST['pagoPorRepresentacion'][]=0;
			$_POST['pagoPorCompensaciones'][]=0;
			$_POST['apoyoEconomicoNoReembolso'][]=0;//nueva
			$_POST['otrosPagos'][]=0;
			$_POST['interesesCesantias'][]=0;
			$_POST['cesantiasFondoCesantias'][]=0;
			$_POST['auxilioCesantias'][]=0;
			$_POST['pensionesJubilacion'][]=0;
			$_POST['rentasTrabajoPrension'][]=0;
			$_POST['aportesSalud'][]=0;
			$_POST['aportesPensiones'][]=0;
			$_POST['aporteVoluntarioRegimenIndividual'][]=$row[2];
			$_POST['aportesPensionesVoluntarios'][]=0;
			$_POST['cuentasAFC'][]=0;
			$_POST['cuentasAVC'][]=0;
			$_POST['retencionesFuenteRentas'][]=0;
			$_POST['iva'][]=0;
			$_POST['retencionIva'][]=0;
			$_POST['pagoAlimentacion'][]=0;
			$_POST['ingresoLaboralPromedio'][]=0;
			$_POST['terceros1'][] = $row[1];
		}
        else if($row[0] == '122')
		{
			$_POST['pagoSalarios'][]=0;
			$_POST['eclesiasticos'][]=0;
			$_POST['pagoAbonoElectronico'][]=0;//nueva
			$_POST['valorExcesoPagoAlimentacion'][]=0;//nueva
			$_POST['honorario'][]=0;
			$_POST['pagoPorServicio'][]=0;
			$_POST['pagoPorComisiones'][]=0;
			$_POST['pagoPorPrestasionesSociales'][]=0;
			$_POST['pagoPorViaticos'][]=0;
			$_POST['pagoPorRepresentacion'][]=0;
			$_POST['pagoPorCompensaciones'][]=0;
			$_POST['apoyoEconomicoNoReembolso'][]=0;//nueva
			$_POST['otrosPagos'][]=0;
			$_POST['interesesCesantias'][]=0;
			$_POST['cesantiasFondoCesantias'][]=0;
			$_POST['auxilioCesantias'][]=0;
			$_POST['pensionesJubilacion'][]=0;
			$_POST['rentasTrabajoPrension'][]=0;
			$_POST['aportesSalud'][]=0;
			$_POST['aportesPensiones'][]=0;
			$_POST['aporteVoluntarioRegimenIndividual'][]=0;
			$_POST['aportesPensionesVoluntarios'][]=$row[2];
			$_POST['cuentasAFC'][]=0;
			$_POST['cuentasAVC'][]=0;
			$_POST['retencionesFuenteRentas'][]=0;
			$_POST['iva'][]=0;
			$_POST['retencionIva'][]=0;
			$_POST['pagoAlimentacion'][]=0;
			$_POST['ingresoLaboralPromedio'][]=0;
			$_POST['terceros1'][] = $row[1];
		}
        else if($row[0] == '123')
		{
			$_POST['pagoSalarios'][]=0;
			$_POST['eclesiasticos'][]=0;
			$_POST['pagoAbonoElectronico'][]=0;//nueva
			$_POST['valorExcesoPagoAlimentacion'][]=0;//nueva
			$_POST['honorario'][]=0;
			$_POST['pagoPorServicio'][]=0;
			$_POST['pagoPorComisiones'][]=0;
			$_POST['pagoPorPrestasionesSociales'][]=0;
			$_POST['pagoPorViaticos'][]=0;
			$_POST['pagoPorRepresentacion'][]=0;
			$_POST['pagoPorCompensaciones'][]=0;
			$_POST['apoyoEconomicoNoReembolso'][]=0;//nueva
			$_POST['otrosPagos'][]=0;
			$_POST['interesesCesantias'][]=0;
			$_POST['cesantiasFondoCesantias'][]=0;
			$_POST['auxilioCesantias'][]=0;
			$_POST['pensionesJubilacion'][]=0;
			$_POST['rentasTrabajoPrension'][]=0;
			$_POST['aportesSalud'][]=0;
			$_POST['aportesPensiones'][]=0;
			$_POST['aporteVoluntarioRegimenIndividual'][]=0;
			$_POST['aportesPensionesVoluntarios'][]=0;
			$_POST['cuentasAFC'][]=$row[2];
			$_POST['cuentasAVC'][]=0;
			$_POST['retencionesFuenteRentas'][]=0;
			$_POST['iva'][]=0;
			$_POST['retencionIva'][]=0;
			$_POST['pagoAlimentacion'][]=0;
			$_POST['ingresoLaboralPromedio'][]=0;
			$_POST['terceros1'][] = $row[1];
		}
        else if($row[0] == '124')
		{
			$_POST['pagoSalarios'][]=0;
			$_POST['eclesiasticos'][]=0;
			$_POST['pagoAbonoElectronico'][]=0;//nueva
			$_POST['valorExcesoPagoAlimentacion'][]=0;//nueva
			$_POST['honorario'][]=0;
			$_POST['pagoPorServicio'][]=0;
			$_POST['pagoPorComisiones'][]=0;
			$_POST['pagoPorPrestasionesSociales'][]=0;
			$_POST['pagoPorViaticos'][]=0;
			$_POST['pagoPorRepresentacion'][]=0;
			$_POST['pagoPorCompensaciones'][]=0;
			$_POST['apoyoEconomicoNoReembolso'][]=0;//nueva
			$_POST['otrosPagos'][]=0;
			$_POST['interesesCesantias'][]=0;
			$_POST['cesantiasFondoCesantias'][]=0;
			$_POST['auxilioCesantias'][]=0;
			$_POST['pensionesJubilacion'][]=0;
			$_POST['rentasTrabajoPrension'][]=0;
			$_POST['aportesSalud'][]=0;
			$_POST['aportesPensiones'][]=0;
			$_POST['aporteVoluntarioRegimenIndividual'][]=0;
			$_POST['aportesPensionesVoluntarios'][]=0;
			$_POST['cuentasAFC'][]=0;
			$_POST['cuentasAVC'][]=$row[2];
			$_POST['retencionesFuenteRentas'][]=0;
			$_POST['iva'][]=0;
			$_POST['retencionIva'][]=0;
			$_POST['pagoAlimentacion'][]=0;
			$_POST['ingresoLaboralPromedio'][]=0;
			$_POST['terceros1'][] = $row[1];
		}
        else if($row[0] == '125')
		{
			$_POST['pagoSalarios'][]=0;
			$_POST['eclesiasticos'][]=0;
			$_POST['pagoAbonoElectronico'][]=0;//nueva
			$_POST['valorExcesoPagoAlimentacion'][]=0;//nueva
			$_POST['honorario'][]=0;
			$_POST['pagoPorServicio'][]=0;
			$_POST['pagoPorComisiones'][]=0;
			$_POST['pagoPorPrestasionesSociales'][]=0;
			$_POST['pagoPorViaticos'][]=0;
			$_POST['pagoPorRepresentacion'][]=0;
			$_POST['pagoPorCompensaciones'][]=0;
			$_POST['apoyoEconomicoNoReembolso'][]=0;//nueva
			$_POST['otrosPagos'][]=0;
			$_POST['interesesCesantias'][]=0;
			$_POST['cesantiasFondoCesantias'][]=0;
			$_POST['auxilioCesantias'][]=0;
			$_POST['pensionesJubilacion'][]=0;
			$_POST['rentasTrabajoPrension'][]=0;
			$_POST['aportesSalud'][]=0;
			$_POST['aportesPensiones'][]=0;
			$_POST['aporteVoluntarioRegimenIndividual'][]=0;
			$_POST['aportesPensionesVoluntarios'][]=0;
			$_POST['cuentasAFC'][]=0;
			$_POST['cuentasAVC'][]=0;
			$_POST['retencionesFuenteRentas'][]=$row[2];
			$_POST['iva'][]=0;
			$_POST['retencionIva'][]=0;
			$_POST['pagoAlimentacion'][]=0;
			$_POST['ingresoLaboralPromedio'][]=0;
			$_POST['terceros1'][] = $row[1];
		}
        else if($row[0] == '126')
		{
			$_POST['pagoSalarios'][]=0;
			$_POST['eclesiasticos'][]=0;
			$_POST['pagoAbonoElectronico'][]=0;//nueva
			$_POST['valorExcesoPagoAlimentacion'][]=0;//nueva
			$_POST['honorario'][]=0;
			$_POST['pagoPorServicio'][]=0;
			$_POST['pagoPorComisiones'][]=0;
			$_POST['pagoPorPrestasionesSociales'][]=0;
			$_POST['pagoPorViaticos'][]=0;
			$_POST['pagoPorRepresentacion'][]=0;
			$_POST['pagoPorCompensaciones'][]=0;
			$_POST['apoyoEconomicoNoReembolso'][]=0;//nueva
			$_POST['otrosPagos'][]=0;
			$_POST['interesesCesantias'][]=0;
			$_POST['cesantiasFondoCesantias'][]=0;
			$_POST['auxilioCesantias'][]=0;
			$_POST['pensionesJubilacion'][]=0;
			$_POST['rentasTrabajoPrension'][]=0;
			$_POST['aportesSalud'][]=0;
			$_POST['aportesPensiones'][]=0;
			$_POST['aporteVoluntarioRegimenIndividual'][]=0;
			$_POST['aportesPensionesVoluntarios'][]=0;
			$_POST['cuentasAFC'][]=0;
			$_POST['cuentasAVC'][]=0;
			$_POST['retencionesFuenteRentas'][]=0;
			$_POST['iva'][]=$row[2];
			$_POST['retencionIva'][]=0;
			$_POST['pagoAlimentacion'][]=0;
			$_POST['ingresoLaboralPromedio'][]=0;
			$_POST['terceros1'][] = $row[1];
		}
        else if($row[0] == '127')
		{
			$_POST['pagoSalarios'][]=0;
			$_POST['eclesiasticos'][]=0;
			$_POST['pagoAbonoElectronico'][]=0;//nueva
			$_POST['valorExcesoPagoAlimentacion'][]=0;//nueva
			$_POST['honorario'][]=0;
			$_POST['pagoPorServicio'][]=0;
			$_POST['pagoPorComisiones'][]=0;
			$_POST['pagoPorPrestasionesSociales'][]=0;
			$_POST['pagoPorViaticos'][]=0;
			$_POST['pagoPorRepresentacion'][]=0;
			$_POST['pagoPorCompensaciones'][]=0;
			$_POST['apoyoEconomicoNoReembolso'][]=0;//nueva
			$_POST['otrosPagos'][]=0;
			$_POST['interesesCesantias'][]=0;
			$_POST['cesantiasFondoCesantias'][]=0;
			$_POST['auxilioCesantias'][]=0;
			$_POST['pensionesJubilacion'][]=0;
			$_POST['rentasTrabajoPrension'][]=0;
			$_POST['aportesSalud'][]=0;
			$_POST['aportesPensiones'][]=0;
			$_POST['aporteVoluntarioRegimenIndividual'][]=0;
			$_POST['aportesPensionesVoluntarios'][]=0;
			$_POST['cuentasAFC'][]=0;
			$_POST['cuentasAVC'][]=0;
			$_POST['retencionesFuenteRentas'][]=0;
			$_POST['iva'][]=0;
			$_POST['retencionIva'][]=$row[2];
			$_POST['pagoAlimentacion'][]=0;
			$_POST['ingresoLaboralPromedio'][]=0;
			$_POST['terceros1'][] = $row[1];
		}
        else if($row[0] == '128')
		{
			$_POST['pagoSalarios'][]=0;
			$_POST['eclesiasticos'][]=0;
			$_POST['pagoAbonoElectronico'][]=0;//nueva
			$_POST['valorExcesoPagoAlimentacion'][]=0;//nueva
			$_POST['honorario'][]=0;
			$_POST['pagoPorServicio'][]=0;
			$_POST['pagoPorComisiones'][]=0;
			$_POST['pagoPorPrestasionesSociales'][]=0;
			$_POST['pagoPorViaticos'][]=0;
			$_POST['pagoPorRepresentacion'][]=0;
			$_POST['pagoPorCompensaciones'][]=0;
			$_POST['apoyoEconomicoNoReembolso'][]=0;//nueva
			$_POST['otrosPagos'][]=0;
			$_POST['interesesCesantias'][]=0;
			$_POST['cesantiasFondoCesantias'][]=0;
			$_POST['auxilioCesantias'][]=0;
			$_POST['pensionesJubilacion'][]=0;
			$_POST['rentasTrabajoPrension'][]=0;
			$_POST['aportesSalud'][]=0;
			$_POST['aportesPensiones'][]=0;
			$_POST['aporteVoluntarioRegimenIndividual'][]=0;
			$_POST['aportesPensionesVoluntarios'][]=0;
			$_POST['cuentasAFC'][]=0;
			$_POST['cuentasAVC'][]=0;
			$_POST['retencionesFuenteRentas'][]=0;
			$_POST['iva'][]=0;
			$_POST['retencionIva'][]=0;
			$_POST['pagoAlimentacion'][]=$row[2];
			$_POST['ingresoLaboralPromedio'][]=0;
			$_POST['terceros1'][] = $row[1];
		}
        else if($row[0] == '129')
		{
			$_POST['pagoSalarios'][]=0;
			$_POST['eclesiasticos'][]=0;
			$_POST['pagoAbonoElectronico'][]=0;//nueva
			$_POST['valorExcesoPagoAlimentacion'][]=0;//nueva
			$_POST['honorario'][]=0;
			$_POST['pagoPorServicio'][]=0;
			$_POST['pagoPorComisiones'][]=0;
			$_POST['pagoPorPrestasionesSociales'][]=0;
			$_POST['pagoPorViaticos'][]=0;
			$_POST['pagoPorRepresentacion'][]=0;
			$_POST['pagoPorCompensaciones'][]=0;
			$_POST['apoyoEconomicoNoReembolso'][]=0;//nueva
			$_POST['otrosPagos'][]=0;
			$_POST['interesesCesantias'][]=0;
			$_POST['cesantiasFondoCesantias'][]=0;
			$_POST['auxilioCesantias'][]=0;
			$_POST['pensionesJubilacion'][]=0;
			$_POST['rentasTrabajoPrension'][]=0;
			$_POST['aportesSalud'][]=0;
			$_POST['aportesPensiones'][]=0;
			$_POST['aporteVoluntarioRegimenIndividual'][]=0;
			$_POST['aportesPensionesVoluntarios'][]=0;
			$_POST['cuentasAFC'][]=0;
			$_POST['cuentasAVC'][]=0;
			$_POST['retencionesFuenteRentas'][]=0;
			$_POST['iva'][]=0;
			$_POST['retencionIva'][]=0;
			$_POST['pagoAlimentacion'][]=0;
			$_POST['ingresoLaboralPromedio'][]=$row[2];
			$_POST['terceros1'][] = $row[1];
		}

	}
	$sqlr="SELECT distinct ED.concepto,ED.tercero,sum(ED.valor),ED.id_cxp,sum(ED.retefte) FROM exogena_det AS ED, exogena_cab AS E WHERE E.vigencia = '$_POST[vigencias]' AND E.id_exo=ED.id_exo AND E.estado='S' group by ED.concepto,ED.tercero order by ED.tercero";
	$res=mysqli_query($linkbd, $sqlr);
	$xy=4;
	while ($row = mysqli_fetch_row($res))
	{
		$_POST['nomina'][]=$row[3];
		if($row[0] == '101')
		{
			$_POST['pagoSalarios'][]=$row[2];
			$_POST['eclesiasticos'][]=0;
			$_POST['pagoAbonoElectronico'][]=0;//nueva
			$_POST['valorExcesoPagoAlimentacion'][]=0;//nueva
			$_POST['honorario'][]=0;
			$_POST['pagoPorServicio'][]=0;
			$_POST['pagoPorComisiones'][]=0;
			$_POST['pagoPorPrestasionesSociales'][]=0;
			$_POST['pagoPorViaticos'][]=0;
			$_POST['pagoPorRepresentacion'][]=0;
			$_POST['pagoPorCompensaciones'][]=0;
			$_POST['apoyoEconomicoNoReembolso'][]=0;//nueva
			$_POST['otrosPagos'][]=0;
			$_POST['interesesCesantias'][]=0;
			$_POST['cesantiasFondoCesantias'][]=0;
			$_POST['auxilioCesantias'][]=0;
			$_POST['pensionesJubilacion'][]=0;
			$_POST['rentasTrabajoPrension'][]=0;
			$_POST['aportesSalud'][]=0;
			$_POST['aportesPensiones'][]=0;
			$_POST['aporteVoluntarioRegimenIndividual'][]=0;
			$_POST['aportesPensionesVoluntarios'][]=0;
			$_POST['cuentasAFC'][]=0;
			$_POST['cuentasAVC'][]=0;
			$_POST['retencionesFuenteRentas'][]=$row[4];
			$_POST['iva'][]=0;
			$_POST['retencionIva'][]=0;
			$_POST['pagoAlimentacion'][]=0;
			$_POST['ingresoLaboralPromedio'][]=0;
			$_POST['terceros1'][] = $row[1];
		}
		else if($row[0] == '102')
		{
            $_POST['pagoSalarios'][]=0;
			$_POST['eclesiasticos'][]=$row[2];
			$_POST['pagoAbonoElectronico'][]=0;//nueva
			$_POST['valorExcesoPagoAlimentacion'][]=0;//nueva
			$_POST['honorario'][]=0;
			$_POST['pagoPorServicio'][]=0;
			$_POST['pagoPorComisiones'][]=0;
			$_POST['pagoPorPrestasionesSociales'][]=0;
			$_POST['pagoPorViaticos'][]=0;
			$_POST['pagoPorRepresentacion'][]=0;
			$_POST['pagoPorCompensaciones'][]=0;
			$_POST['apoyoEconomicoNoReembolso'][]=0;//nueva
			$_POST['otrosPagos'][]=0;
			$_POST['interesesCesantias'][]=0;
			$_POST['cesantiasFondoCesantias'][]=0;
			$_POST['auxilioCesantias'][]=0;
			$_POST['pensionesJubilacion'][]=0;
			$_POST['rentasTrabajoPrension'][]=0;
			$_POST['aportesSalud'][]=0;
			$_POST['aportesPensiones'][]=0;
			$_POST['aporteVoluntarioRegimenIndividual'][]=0;
			$_POST['aportesPensionesVoluntarios'][]=0;
			$_POST['cuentasAFC'][]=0;
			$_POST['cuentasAVC'][]=0;
			$_POST['retencionesFuenteRentas'][]=$row[4];
			$_POST['iva'][]=0;
			$_POST['retencionIva'][]=0;
			$_POST['pagoAlimentacion'][]=0;
			$_POST['ingresoLaboralPromedio'][]=0;
			$_POST['terceros1'][] = $row[1];
		}
		else if($row[0] == '103')
		{
            $_POST['pagoSalarios'][]=0;
			$_POST['eclesiasticos'][]=0;
			$_POST['pagoAbonoElectronico'][]=$row[2];//nueva
			$_POST['valorExcesoPagoAlimentacion'][]=0;//nueva
			$_POST['honorario'][]=0;
			$_POST['pagoPorServicio'][]=0;
			$_POST['pagoPorComisiones'][]=0;
			$_POST['pagoPorPrestasionesSociales'][]=0;
			$_POST['pagoPorViaticos'][]=0;
			$_POST['pagoPorRepresentacion'][]=0;
			$_POST['pagoPorCompensaciones'][]=0;
			$_POST['apoyoEconomicoNoReembolso'][]=0;//nueva
			$_POST['otrosPagos'][]=0;
			$_POST['interesesCesantias'][]=0;
			$_POST['cesantiasFondoCesantias'][]=0;
			$_POST['auxilioCesantias'][]=0;
			$_POST['pensionesJubilacion'][]=0;
			$_POST['rentasTrabajoPrension'][]=0;
			$_POST['aportesSalud'][]=0;
			$_POST['aportesPensiones'][]=0;
			$_POST['aporteVoluntarioRegimenIndividual'][]=0;
			$_POST['aportesPensionesVoluntarios'][]=0;
			$_POST['cuentasAFC'][]=0;
			$_POST['cuentasAVC'][]=0;
			$_POST['retencionesFuenteRentas'][]=$row[4];
			$_POST['iva'][]=0;
			$_POST['retencionIva'][]=0;
			$_POST['pagoAlimentacion'][]=0;
			$_POST['ingresoLaboralPromedio'][]=0;
			$_POST['terceros1'][] = $row[1];


		}
		else if($row[0] == '104')
		{
            $_POST['pagoSalarios'][]=0;
			$_POST['eclesiasticos'][]=0;
			$_POST['pagoAbonoElectronico'][]=0;//nueva
			$_POST['valorExcesoPagoAlimentacion'][]=$row[2];//nueva
			$_POST['honorario'][]=0;
			$_POST['pagoPorServicio'][]=0;
			$_POST['pagoPorComisiones'][]=0;
			$_POST['pagoPorPrestasionesSociales'][]=0;
			$_POST['pagoPorViaticos'][]=0;
			$_POST['pagoPorRepresentacion'][]=0;
			$_POST['pagoPorCompensaciones'][]=0;
			$_POST['apoyoEconomicoNoReembolso'][]=0;//nueva
			$_POST['otrosPagos'][]=0;
			$_POST['interesesCesantias'][]=0;
			$_POST['cesantiasFondoCesantias'][]=0;
			$_POST['auxilioCesantias'][]=0;
			$_POST['pensionesJubilacion'][]=0;
			$_POST['rentasTrabajoPrension'][]=0;
			$_POST['aportesSalud'][]=0;
			$_POST['aportesPensiones'][]=0;
			$_POST['aporteVoluntarioRegimenIndividual'][]=0;
			$_POST['aportesPensionesVoluntarios'][]=0;
			$_POST['cuentasAFC'][]=0;
			$_POST['cuentasAVC'][]=0;
			$_POST['retencionesFuenteRentas'][]=$row[4];
			$_POST['iva'][]=0;
			$_POST['retencionIva'][]=0;
			$_POST['pagoAlimentacion'][]=0;
			$_POST['ingresoLaboralPromedio'][]=0;
			$_POST['terceros1'][] = $row[1];


		}
		else if($row[0] == '105')
		{
			$_POST['pagoSalarios'][]=0;
			$_POST['eclesiasticos'][]=0;
			$_POST['pagoAbonoElectronico'][]=0;//nueva
			$_POST['valorExcesoPagoAlimentacion'][]=0;//nueva
			$_POST['honorario'][]=$row[2];
			$_POST['pagoPorServicio'][]=0;
			$_POST['pagoPorComisiones'][]=0;
			$_POST['pagoPorPrestasionesSociales'][]=0;
			$_POST['pagoPorViaticos'][]=0;
			$_POST['pagoPorRepresentacion'][]=0;
			$_POST['pagoPorCompensaciones'][]=0;
			$_POST['apoyoEconomicoNoReembolso'][]=0;//nueva
			$_POST['otrosPagos'][]=0;
			$_POST['interesesCesantias'][]=0;
			$_POST['cesantiasFondoCesantias'][]=0;
			$_POST['auxilioCesantias'][]=0;
			$_POST['pensionesJubilacion'][]=0;
			$_POST['rentasTrabajoPrension'][]=0;
			$_POST['aportesSalud'][]=0;
			$_POST['aportesPensiones'][]=0;
			$_POST['aporteVoluntarioRegimenIndividual'][]=0;
			$_POST['aportesPensionesVoluntarios'][]=0;
			$_POST['cuentasAFC'][]=0;
			$_POST['cuentasAVC'][]=0;
			$_POST['retencionesFuenteRentas'][]=$row[4];
			$_POST['iva'][]=0;
			$_POST['retencionIva'][]=0;
			$_POST['pagoAlimentacion'][]=0;
			$_POST['ingresoLaboralPromedio'][]=0;
			$_POST['terceros1'][] = $row[1];
		}
		else if($row[0] == '106')
		{
			$_POST['pagoSalarios'][]=0;
			$_POST['eclesiasticos'][]=0;
			$_POST['pagoAbonoElectronico'][]=0;//nueva
			$_POST['valorExcesoPagoAlimentacion'][]=0;//nueva
			$_POST['honorario'][]=0;
			$_POST['pagoPorServicio'][]=$row[2];
			$_POST['pagoPorComisiones'][]=0;
			$_POST['pagoPorPrestasionesSociales'][]=0;
			$_POST['pagoPorViaticos'][]=0;
			$_POST['pagoPorRepresentacion'][]=0;
			$_POST['pagoPorCompensaciones'][]=0;
			$_POST['apoyoEconomicoNoReembolso'][]=0;//nueva
			$_POST['otrosPagos'][]=0;
			$_POST['interesesCesantias'][]=0;
			$_POST['cesantiasFondoCesantias'][]=0;
			$_POST['auxilioCesantias'][]=0;
			$_POST['pensionesJubilacion'][]=0;
			$_POST['rentasTrabajoPrension'][]=0;
			$_POST['aportesSalud'][]=0;
			$_POST['aportesPensiones'][]=0;
			$_POST['aporteVoluntarioRegimenIndividual'][]=0;
			$_POST['aportesPensionesVoluntarios'][]=0;
			$_POST['cuentasAFC'][]=0;
			$_POST['cuentasAVC'][]=0;
			$_POST['retencionesFuenteRentas'][]=$row[4];
			$_POST['iva'][]=0;
			$_POST['retencionIva'][]=0;
			$_POST['pagoAlimentacion'][]=0;
			$_POST['ingresoLaboralPromedio'][]=0;
			$_POST['terceros1'][] = $row[1];
		}
		else if($row[0] == '107')
		{
			$_POST['pagoSalarios'][]=0;
			$_POST['eclesiasticos'][]=0;
			$_POST['pagoAbonoElectronico'][]=0;//nueva
			$_POST['valorExcesoPagoAlimentacion'][]=0;//nueva
			$_POST['honorario'][]=0;
			$_POST['pagoPorServicio'][]=0;
			$_POST['pagoPorComisiones'][]=$row[2];
			$_POST['pagoPorPrestasionesSociales'][]=0;
			$_POST['pagoPorViaticos'][]=0;
			$_POST['pagoPorRepresentacion'][]=0;
			$_POST['pagoPorCompensaciones'][]=0;
			$_POST['apoyoEconomicoNoReembolso'][]=0;//nueva
			$_POST['otrosPagos'][]=0;
			$_POST['interesesCesantias'][]=0;
			$_POST['cesantiasFondoCesantias'][]=0;
			$_POST['auxilioCesantias'][]=0;
			$_POST['pensionesJubilacion'][]=0;
			$_POST['rentasTrabajoPrension'][]=0;
			$_POST['aportesSalud'][]=0;
			$_POST['aportesPensiones'][]=0;
			$_POST['aporteVoluntarioRegimenIndividual'][]=0;
			$_POST['aportesPensionesVoluntarios'][]=0;
			$_POST['cuentasAFC'][]=0;
			$_POST['cuentasAVC'][]=0;
			$_POST['retencionesFuenteRentas'][]=$row[4];
			$_POST['iva'][]=0;
			$_POST['retencionIva'][]=0;
			$_POST['pagoAlimentacion'][]=0;
			$_POST['ingresoLaboralPromedio'][]=0;
			$_POST['terceros1'][] = $row[1];
		}
		else if($row[0] == '108')
		{
			$_POST['pagoSalarios'][]=0;
			$_POST['eclesiasticos'][]=0;
			$_POST['pagoAbonoElectronico'][]=0;//nueva
			$_POST['valorExcesoPagoAlimentacion'][]=0;//nueva
			$_POST['honorario'][]=0;
			$_POST['pagoPorServicio'][]=0;
			$_POST['pagoPorComisiones'][]=0;
			$_POST['pagoPorPrestasionesSociales'][]=$row[2];
			$_POST['pagoPorViaticos'][]=0;
			$_POST['pagoPorRepresentacion'][]=0;
			$_POST['pagoPorCompensaciones'][]=0;
			$_POST['apoyoEconomicoNoReembolso'][]=0;//nueva
			$_POST['otrosPagos'][]=0;
			$_POST['interesesCesantias'][]=0;
			$_POST['cesantiasFondoCesantias'][]=0;
			$_POST['auxilioCesantias'][]=0;
			$_POST['pensionesJubilacion'][]=0;
			$_POST['rentasTrabajoPrension'][]=0;
			$_POST['aportesSalud'][]=0;
			$_POST['aportesPensiones'][]=0;
			$_POST['aporteVoluntarioRegimenIndividual'][]=0;
			$_POST['aportesPensionesVoluntarios'][]=0;
			$_POST['cuentasAFC'][]=0;
			$_POST['cuentasAVC'][]=0;
			$_POST['retencionesFuenteRentas'][]=$row[4];
			$_POST['iva'][]=0;
			$_POST['retencionIva'][]=0;
			$_POST['pagoAlimentacion'][]=0;
			$_POST['ingresoLaboralPromedio'][]=0;
			$_POST['terceros1'][] = $row[1];
		}
		else if($row[0] == '109')
		{
			$_POST['pagoSalarios'][]=0;
			$_POST['eclesiasticos'][]=0;
			$_POST['pagoAbonoElectronico'][]=0;//nueva
			$_POST['valorExcesoPagoAlimentacion'][]=0;//nueva
			$_POST['honorario'][]=0;
			$_POST['pagoPorServicio'][]=0;
			$_POST['pagoPorComisiones'][]=0;
			$_POST['pagoPorPrestasionesSociales'][]=0;
			$_POST['pagoPorViaticos'][]=$row[2];
			$_POST['pagoPorRepresentacion'][]=0;
			$_POST['pagoPorCompensaciones'][]=0;
			$_POST['apoyoEconomicoNoReembolso'][]=0;//nueva
			$_POST['otrosPagos'][]=0;
			$_POST['interesesCesantias'][]=0;
			$_POST['cesantiasFondoCesantias'][]=0;
			$_POST['auxilioCesantias'][]=0;
			$_POST['pensionesJubilacion'][]=0;
			$_POST['rentasTrabajoPrension'][]=0;
			$_POST['aportesSalud'][]=0;
			$_POST['aportesPensiones'][]=0;
			$_POST['aporteVoluntarioRegimenIndividual'][]=0;
			$_POST['aportesPensionesVoluntarios'][]=0;
			$_POST['cuentasAFC'][]=0;
			$_POST['cuentasAVC'][]=0;
			$_POST['retencionesFuenteRentas'][]=$row[4];
			$_POST['iva'][]=0;
			$_POST['retencionIva'][]=0;
			$_POST['pagoAlimentacion'][]=0;
			$_POST['ingresoLaboralPromedio'][]=0;
			$_POST['terceros1'][] = $row[1];
		}
		else if($row[0] == '110')
		{
			$_POST['pagoSalarios'][]=0;
			$_POST['eclesiasticos'][]=0;
			$_POST['pagoAbonoElectronico'][]=0;//nueva
			$_POST['valorExcesoPagoAlimentacion'][]=0;//nueva
			$_POST['honorario'][]=0;
			$_POST['pagoPorServicio'][]=0;
			$_POST['pagoPorComisiones'][]=0;
			$_POST['pagoPorPrestasionesSociales'][]=0;
			$_POST['pagoPorViaticos'][]=0;
			$_POST['pagoPorRepresentacion'][]=$row[2];
			$_POST['pagoPorCompensaciones'][]=0;
			$_POST['apoyoEconomicoNoReembolso'][]=0;//nueva
			$_POST['otrosPagos'][]=0;
			$_POST['interesesCesantias'][]=0;
			$_POST['cesantiasFondoCesantias'][]=0;
			$_POST['auxilioCesantias'][]=0;
			$_POST['pensionesJubilacion'][]=0;
			$_POST['rentasTrabajoPrension'][]=0;
			$_POST['aportesSalud'][]=0;
			$_POST['aportesPensiones'][]=0;
			$_POST['aporteVoluntarioRegimenIndividual'][]=0;
			$_POST['aportesPensionesVoluntarios'][]=0;
			$_POST['cuentasAFC'][]=0;
			$_POST['cuentasAVC'][]=0;
			$_POST['retencionesFuenteRentas'][]=$row[4];
			$_POST['iva'][]=0;
			$_POST['retencionIva'][]=0;
			$_POST['pagoAlimentacion'][]=0;
			$_POST['ingresoLaboralPromedio'][]=0;
			$_POST['terceros1'][] = $row[1];
		}
		else if($row[0] == '111')
		{
			$_POST['pagoSalarios'][]=0;
			$_POST['eclesiasticos'][]=0;
			$_POST['pagoAbonoElectronico'][]=0;//nueva
			$_POST['valorExcesoPagoAlimentacion'][]=0;//nueva
			$_POST['honorario'][]=0;
			$_POST['pagoPorServicio'][]=0;
			$_POST['pagoPorComisiones'][]=0;
			$_POST['pagoPorPrestasionesSociales'][]=0;
			$_POST['pagoPorViaticos'][]=0;
			$_POST['pagoPorRepresentacion'][]=0;
			$_POST['pagoPorCompensaciones'][]=$row[2];
			$_POST['apoyoEconomicoNoReembolso'][]=0;//nueva
			$_POST['otrosPagos'][]=0;
			$_POST['interesesCesantias'][]=0;
			$_POST['cesantiasFondoCesantias'][]=0;
			$_POST['auxilioCesantias'][]=0;
			$_POST['pensionesJubilacion'][]=0;
			$_POST['rentasTrabajoPrension'][]=0;
			$_POST['aportesSalud'][]=0;
			$_POST['aportesPensiones'][]=0;
			$_POST['aporteVoluntarioRegimenIndividual'][]=0;
			$_POST['aportesPensionesVoluntarios'][]=0;
			$_POST['cuentasAFC'][]=0;
			$_POST['cuentasAVC'][]=0;
			$_POST['retencionesFuenteRentas'][]=$row[4];
			$_POST['iva'][]=0;
			$_POST['retencionIva'][]=0;
			$_POST['pagoAlimentacion'][]=0;
			$_POST['ingresoLaboralPromedio'][]=0;
			$_POST['terceros1'][] = $row[1];
		}
		else if($row[0] == '112')
		{
			$_POST['pagoSalarios'][]=0;
			$_POST['eclesiasticos'][]=0;
			$_POST['pagoAbonoElectronico'][]=0;//nueva
			$_POST['valorExcesoPagoAlimentacion'][]=0;//nueva
			$_POST['honorario'][]=0;
			$_POST['pagoPorServicio'][]=0;
			$_POST['pagoPorComisiones'][]=0;
			$_POST['pagoPorPrestasionesSociales'][]=0;
			$_POST['pagoPorViaticos'][]=0;
			$_POST['pagoPorRepresentacion'][]=0;
			$_POST['pagoPorCompensaciones'][]=0;
			$_POST['apoyoEconomicoNoReembolso'][]=$row[2];//nueva
			$_POST['otrosPagos'][]=0;
			$_POST['interesesCesantias'][]=0;
			$_POST['cesantiasFondoCesantias'][]=0;
			$_POST['auxilioCesantias'][]=0;
			$_POST['pensionesJubilacion'][]=0;
			$_POST['rentasTrabajoPrension'][]=0;
			$_POST['aportesSalud'][]=0;
			$_POST['aportesPensiones'][]=0;
			$_POST['aporteVoluntarioRegimenIndividual'][]=0;
			$_POST['aportesPensionesVoluntarios'][]=0;
			$_POST['cuentasAFC'][]=0;
			$_POST['cuentasAVC'][]=0;
			$_POST['retencionesFuenteRentas'][]=$row[4];
			$_POST['iva'][]=0;
			$_POST['retencionIva'][]=0;
			$_POST['pagoAlimentacion'][]=0;
			$_POST['ingresoLaboralPromedio'][]=0;
			$_POST['terceros1'][] = $row[1];
		}
		else if($row[0] == '113')
		{
			$_POST['pagoSalarios'][]=0;
			$_POST['eclesiasticos'][]=0;
			$_POST['pagoAbonoElectronico'][]=0;//nueva
			$_POST['valorExcesoPagoAlimentacion'][]=0;//nueva
			$_POST['honorario'][]=0;
			$_POST['pagoPorServicio'][]=0;
			$_POST['pagoPorComisiones'][]=0;
			$_POST['pagoPorPrestasionesSociales'][]=0;
			$_POST['pagoPorViaticos'][]=0;
			$_POST['pagoPorRepresentacion'][]=0;
			$_POST['pagoPorCompensaciones'][]=0;
			$_POST['apoyoEconomicoNoReembolso'][]=0;//nueva
			$_POST['otrosPagos'][]=$row[2];
			$_POST['interesesCesantias'][]=0;
			$_POST['cesantiasFondoCesantias'][]=0;
			$_POST['auxilioCesantias'][]=0;
			$_POST['pensionesJubilacion'][]=0;
			$_POST['rentasTrabajoPrension'][]=0;
			$_POST['aportesSalud'][]=0;
			$_POST['aportesPensiones'][]=0;
			$_POST['aporteVoluntarioRegimenIndividual'][]=0;
			$_POST['aportesPensionesVoluntarios'][]=0;
			$_POST['cuentasAFC'][]=0;
			$_POST['cuentasAVC'][]=0;
			$_POST['retencionesFuenteRentas'][]=$row[4];
			$_POST['iva'][]=0;
			$_POST['retencionIva'][]=0;
			$_POST['pagoAlimentacion'][]=0;
			$_POST['ingresoLaboralPromedio'][]=0;
			$_POST['terceros1'][] = $row[1];
		}
		else if($row[0] == '114')
		{
			$_POST['pagoSalarios'][]=0;
			$_POST['eclesiasticos'][]=0;
			$_POST['pagoAbonoElectronico'][]=0;//nueva
			$_POST['valorExcesoPagoAlimentacion'][]=0;//nueva
			$_POST['honorario'][]=0;
			$_POST['pagoPorServicio'][]=0;
			$_POST['pagoPorComisiones'][]=0;
			$_POST['pagoPorPrestasionesSociales'][]=0;
			$_POST['pagoPorViaticos'][]=0;
			$_POST['pagoPorRepresentacion'][]=0;
			$_POST['pagoPorCompensaciones'][]=0;
			$_POST['apoyoEconomicoNoReembolso'][]=0;//nueva
			$_POST['otrosPagos'][]=0;
			$_POST['interesesCesantias'][]=$row[2];
			$_POST['cesantiasFondoCesantias'][]=0;
			$_POST['auxilioCesantias'][]=0;
			$_POST['pensionesJubilacion'][]=0;
			$_POST['rentasTrabajoPrension'][]=0;
			$_POST['aportesSalud'][]=0;
			$_POST['aportesPensiones'][]=0;
			$_POST['aporteVoluntarioRegimenIndividual'][]=0;
			$_POST['aportesPensionesVoluntarios'][]=0;
			$_POST['cuentasAFC'][]=0;
			$_POST['cuentasAVC'][]=0;
			$_POST['retencionesFuenteRentas'][]=$row[4];
			$_POST['iva'][]=0;
			$_POST['retencionIva'][]=0;
			$_POST['pagoAlimentacion'][]=0;
			$_POST['ingresoLaboralPromedio'][]=0;
			$_POST['terceros1'][] = $row[1];
		}
		else if($row[0] == '115')
		{
			$_POST['pagoSalarios'][]=0;
			$_POST['eclesiasticos'][]=0;
			$_POST['pagoAbonoElectronico'][]=0;//nueva
			$_POST['valorExcesoPagoAlimentacion'][]=0;//nueva
			$_POST['honorario'][]=0;
			$_POST['pagoPorServicio'][]=0;
			$_POST['pagoPorComisiones'][]=0;
			$_POST['pagoPorPrestasionesSociales'][]=0;
			$_POST['pagoPorViaticos'][]=0;
			$_POST['pagoPorRepresentacion'][]=0;
			$_POST['pagoPorCompensaciones'][]=0;
			$_POST['apoyoEconomicoNoReembolso'][]=0;//nueva
			$_POST['otrosPagos'][]=0;
			$_POST['interesesCesantias'][]=0;
			$_POST['cesantiasFondoCesantias'][]=$row[2];
			$_POST['auxilioCesantias'][]=0;
			$_POST['pensionesJubilacion'][]=0;
			$_POST['rentasTrabajoPrension'][]=0;
			$_POST['aportesSalud'][]=0;
			$_POST['aportesPensiones'][]=0;
			$_POST['aporteVoluntarioRegimenIndividual'][]=0;
			$_POST['aportesPensionesVoluntarios'][]=0;
			$_POST['cuentasAFC'][]=0;
			$_POST['cuentasAVC'][]=0;
			$_POST['retencionesFuenteRentas'][]=$row[4];
			$_POST['iva'][]=0;
			$_POST['retencionIva'][]=0;
			$_POST['pagoAlimentacion'][]=0;
			$_POST['ingresoLaboralPromedio'][]=0;
			$_POST['terceros1'][] = $row[1];
		}
		else if($row[0] == '116')
		{
			$_POST['pagoSalarios'][]=0;
			$_POST['eclesiasticos'][]=0;
			$_POST['pagoAbonoElectronico'][]=0;//nueva
			$_POST['valorExcesoPagoAlimentacion'][]=0;//nueva
			$_POST['honorario'][]=0;
			$_POST['pagoPorServicio'][]=0;
			$_POST['pagoPorComisiones'][]=0;
			$_POST['pagoPorPrestasionesSociales'][]=0;
			$_POST['pagoPorViaticos'][]=0;
			$_POST['pagoPorRepresentacion'][]=0;
			$_POST['pagoPorCompensaciones'][]=0;
			$_POST['apoyoEconomicoNoReembolso'][]=0;//nueva
			$_POST['otrosPagos'][]=0;
			$_POST['interesesCesantias'][]=0;
			$_POST['cesantiasFondoCesantias'][]=0;
			$_POST['auxilioCesantias'][]=$row[2];
			$_POST['pensionesJubilacion'][]=0;
			$_POST['rentasTrabajoPrension'][]=0;
			$_POST['aportesSalud'][]=0;
			$_POST['aportesPensiones'][]=0;
			$_POST['aporteVoluntarioRegimenIndividual'][]=0;
			$_POST['aportesPensionesVoluntarios'][]=0;
			$_POST['cuentasAFC'][]=0;
			$_POST['cuentasAVC'][]=0;
			$_POST['retencionesFuenteRentas'][]=$row[4];
			$_POST['iva'][]=0;
			$_POST['retencionIva'][]=0;
			$_POST['pagoAlimentacion'][]=0;
			$_POST['ingresoLaboralPromedio'][]=0;
			$_POST['terceros1'][] = $row[1];
		}
		else if($row[0] == '117')
		{
			$_POST['pagoSalarios'][]=0;
			$_POST['eclesiasticos'][]=0;
			$_POST['pagoAbonoElectronico'][]=0;//nueva
			$_POST['valorExcesoPagoAlimentacion'][]=0;//nueva
			$_POST['honorario'][]=0;
			$_POST['pagoPorServicio'][]=0;
			$_POST['pagoPorComisiones'][]=0;
			$_POST['pagoPorPrestasionesSociales'][]=0;
			$_POST['pagoPorViaticos'][]=0;
			$_POST['pagoPorRepresentacion'][]=0;
			$_POST['pagoPorCompensaciones'][]=0;
			$_POST['apoyoEconomicoNoReembolso'][]=0;//nueva
			$_POST['otrosPagos'][]=0;
			$_POST['interesesCesantias'][]=0;
			$_POST['cesantiasFondoCesantias'][]=0;
			$_POST['auxilioCesantias'][]=0;
			$_POST['pensionesJubilacion'][]=$row[2];
			$_POST['rentasTrabajoPrension'][]=0;
			$_POST['aportesSalud'][]=0;
			$_POST['aportesPensiones'][]=0;
			$_POST['aporteVoluntarioRegimenIndividual'][]=0;
			$_POST['aportesPensionesVoluntarios'][]=0;
			$_POST['cuentasAFC'][]=0;
			$_POST['cuentasAVC'][]=0;
			$_POST['retencionesFuenteRentas'][]=$row[4];
			$_POST['iva'][]=0;
			$_POST['retencionIva'][]=0;
			$_POST['pagoAlimentacion'][]=0;
			$_POST['ingresoLaboralPromedio'][]=0;
			$_POST['terceros1'][] = $row[1];
		}
		else if($row[0] == '118')
		{
			$_POST['pagoSalarios'][]=0;
			$_POST['eclesiasticos'][]=0;
			$_POST['pagoAbonoElectronico'][]=0;//nueva
			$_POST['valorExcesoPagoAlimentacion'][]=0;//nueva
			$_POST['honorario'][]=0;
			$_POST['pagoPorServicio'][]=0;
			$_POST['pagoPorComisiones'][]=0;
			$_POST['pagoPorPrestasionesSociales'][]=0;
			$_POST['pagoPorViaticos'][]=0;
			$_POST['pagoPorRepresentacion'][]=0;
			$_POST['pagoPorCompensaciones'][]=0;
			$_POST['apoyoEconomicoNoReembolso'][]=0;//nueva
			$_POST['otrosPagos'][]=0;
			$_POST['interesesCesantias'][]=0;
			$_POST['cesantiasFondoCesantias'][]=0;
			$_POST['auxilioCesantias'][]=0;
			$_POST['pensionesJubilacion'][]=0;
			$_POST['rentasTrabajoPrension'][]=$row[2];
			$_POST['aportesSalud'][]=0;
			$_POST['aportesPensiones'][]=0;
			$_POST['aporteVoluntarioRegimenIndividual'][]=0;
			$_POST['aportesPensionesVoluntarios'][]=0;
			$_POST['cuentasAFC'][]=0;
			$_POST['cuentasAVC'][]=0;
			$_POST['retencionesFuenteRentas'][]=$row[4];
			$_POST['iva'][]=0;
			$_POST['retencionIva'][]=0;
			$_POST['pagoAlimentacion'][]=0;
			$_POST['ingresoLaboralPromedio'][]=0;
			$_POST['terceros1'][] = $row[1];
		}
        else if($row[0] == '119')
		{
			$_POST['pagoSalarios'][]=0;
			$_POST['eclesiasticos'][]=0;
			$_POST['pagoAbonoElectronico'][]=0;//nueva
			$_POST['valorExcesoPagoAlimentacion'][]=0;//nueva
			$_POST['honorario'][]=0;
			$_POST['pagoPorServicio'][]=0;
			$_POST['pagoPorComisiones'][]=0;
			$_POST['pagoPorPrestasionesSociales'][]=0;
			$_POST['pagoPorViaticos'][]=0;
			$_POST['pagoPorRepresentacion'][]=0;
			$_POST['pagoPorCompensaciones'][]=0;
			$_POST['apoyoEconomicoNoReembolso'][]=0;//nueva
			$_POST['otrosPagos'][]=0;
			$_POST['interesesCesantias'][]=0;
			$_POST['cesantiasFondoCesantias'][]=0;
			$_POST['auxilioCesantias'][]=0;
			$_POST['pensionesJubilacion'][]=0;
			$_POST['rentasTrabajoPrension'][]=0;
			$_POST['aportesSalud'][]=$row[2];
			$_POST['aportesPensiones'][]=0;
			$_POST['aporteVoluntarioRegimenIndividual'][]=0;
			$_POST['aportesPensionesVoluntarios'][]=0;
			$_POST['cuentasAFC'][]=0;
			$_POST['cuentasAVC'][]=0;
			$_POST['retencionesFuenteRentas'][]=$row[4];
			$_POST['iva'][]=0;
			$_POST['retencionIva'][]=0;
			$_POST['pagoAlimentacion'][]=0;
			$_POST['ingresoLaboralPromedio'][]=0;
			$_POST['terceros1'][] = $row[1];
		}
        else if($row[0] == '120')
		{
			$_POST['pagoSalarios'][]=0;
			$_POST['eclesiasticos'][]=0;
			$_POST['pagoAbonoElectronico'][]=0;//nueva
			$_POST['valorExcesoPagoAlimentacion'][]=0;//nueva
			$_POST['honorario'][]=0;
			$_POST['pagoPorServicio'][]=0;
			$_POST['pagoPorComisiones'][]=0;
			$_POST['pagoPorPrestasionesSociales'][]=0;
			$_POST['pagoPorViaticos'][]=0;
			$_POST['pagoPorRepresentacion'][]=0;
			$_POST['pagoPorCompensaciones'][]=0;
			$_POST['apoyoEconomicoNoReembolso'][]=0;//nueva
			$_POST['otrosPagos'][]=0;
			$_POST['interesesCesantias'][]=0;
			$_POST['cesantiasFondoCesantias'][]=0;
			$_POST['auxilioCesantias'][]=0;
			$_POST['pensionesJubilacion'][]=0;
			$_POST['rentasTrabajoPrension'][]=0;
			$_POST['aportesSalud'][]=0;
			$_POST['aportesPensiones'][]=$row[2];
			$_POST['aporteVoluntarioRegimenIndividual'][]=0;
			$_POST['aportesPensionesVoluntarios'][]=0;
			$_POST['cuentasAFC'][]=0;
			$_POST['cuentasAVC'][]=0;
			$_POST['retencionesFuenteRentas'][]=$row[4];
			$_POST['iva'][]=0;
			$_POST['retencionIva'][]=0;
			$_POST['pagoAlimentacion'][]=0;
			$_POST['ingresoLaboralPromedio'][]=0;
			$_POST['terceros1'][] = $row[1];
		}
        else if($row[0] == '121')
		{
			$_POST['pagoSalarios'][]=0;
			$_POST['eclesiasticos'][]=0;
			$_POST['pagoAbonoElectronico'][]=0;//nueva
			$_POST['valorExcesoPagoAlimentacion'][]=0;//nueva
			$_POST['honorario'][]=0;
			$_POST['pagoPorServicio'][]=0;
			$_POST['pagoPorComisiones'][]=0;
			$_POST['pagoPorPrestasionesSociales'][]=0;
			$_POST['pagoPorViaticos'][]=0;
			$_POST['pagoPorRepresentacion'][]=0;
			$_POST['pagoPorCompensaciones'][]=0;
			$_POST['apoyoEconomicoNoReembolso'][]=0;//nueva
			$_POST['otrosPagos'][]=0;
			$_POST['interesesCesantias'][]=0;
			$_POST['cesantiasFondoCesantias'][]=0;
			$_POST['auxilioCesantias'][]=0;
			$_POST['pensionesJubilacion'][]=0;
			$_POST['rentasTrabajoPrension'][]=0;
			$_POST['aportesSalud'][]=0;
			$_POST['aportesPensiones'][]=0;
			$_POST['aporteVoluntarioRegimenIndividual'][]=$row[2];
			$_POST['aportesPensionesVoluntarios'][]=0;
			$_POST['cuentasAFC'][]=0;
			$_POST['cuentasAVC'][]=0;
			$_POST['retencionesFuenteRentas'][]=$row[4];
			$_POST['iva'][]=0;
			$_POST['retencionIva'][]=0;
			$_POST['pagoAlimentacion'][]=0;
			$_POST['ingresoLaboralPromedio'][]=0;
			$_POST['terceros1'][] = $row[1];
		}
        else if($row[0] == '122')
		{
			$_POST['pagoSalarios'][]=0;
			$_POST['eclesiasticos'][]=0;
			$_POST['pagoAbonoElectronico'][]=0;//nueva
			$_POST['valorExcesoPagoAlimentacion'][]=0;//nueva
			$_POST['honorario'][]=0;
			$_POST['pagoPorServicio'][]=0;
			$_POST['pagoPorComisiones'][]=0;
			$_POST['pagoPorPrestasionesSociales'][]=0;
			$_POST['pagoPorViaticos'][]=0;
			$_POST['pagoPorRepresentacion'][]=0;
			$_POST['pagoPorCompensaciones'][]=0;
			$_POST['apoyoEconomicoNoReembolso'][]=0;//nueva
			$_POST['otrosPagos'][]=0;
			$_POST['interesesCesantias'][]=0;
			$_POST['cesantiasFondoCesantias'][]=0;
			$_POST['auxilioCesantias'][]=0;
			$_POST['pensionesJubilacion'][]=0;
			$_POST['rentasTrabajoPrension'][]=0;
			$_POST['aportesSalud'][]=0;
			$_POST['aportesPensiones'][]=0;
			$_POST['aporteVoluntarioRegimenIndividual'][]=0;
			$_POST['aportesPensionesVoluntarios'][]=$row[2];
			$_POST['cuentasAFC'][]=0;
			$_POST['cuentasAVC'][]=0;
			$_POST['retencionesFuenteRentas'][]=$row[4];
			$_POST['iva'][]=0;
			$_POST['retencionIva'][]=0;
			$_POST['pagoAlimentacion'][]=0;
			$_POST['ingresoLaboralPromedio'][]=0;
			$_POST['terceros1'][] = $row[1];
		}
        else if($row[0] == '123')
		{
			$_POST['pagoSalarios'][]=0;
			$_POST['eclesiasticos'][]=0;
			$_POST['pagoAbonoElectronico'][]=0;//nueva
			$_POST['valorExcesoPagoAlimentacion'][]=0;//nueva
			$_POST['honorario'][]=0;
			$_POST['pagoPorServicio'][]=0;
			$_POST['pagoPorComisiones'][]=0;
			$_POST['pagoPorPrestasionesSociales'][]=0;
			$_POST['pagoPorViaticos'][]=0;
			$_POST['pagoPorRepresentacion'][]=0;
			$_POST['pagoPorCompensaciones'][]=0;
			$_POST['apoyoEconomicoNoReembolso'][]=0;//nueva
			$_POST['otrosPagos'][]=0;
			$_POST['interesesCesantias'][]=0;
			$_POST['cesantiasFondoCesantias'][]=0;
			$_POST['auxilioCesantias'][]=0;
			$_POST['pensionesJubilacion'][]=0;
			$_POST['rentasTrabajoPrension'][]=0;
			$_POST['aportesSalud'][]=0;
			$_POST['aportesPensiones'][]=0;
			$_POST['aporteVoluntarioRegimenIndividual'][]=0;
			$_POST['aportesPensionesVoluntarios'][]=0;
			$_POST['cuentasAFC'][]=$row[2];
			$_POST['cuentasAVC'][]=0;
			$_POST['retencionesFuenteRentas'][]=$row[4];
			$_POST['iva'][]=0;
			$_POST['retencionIva'][]=0;
			$_POST['pagoAlimentacion'][]=0;
			$_POST['ingresoLaboralPromedio'][]=0;
			$_POST['terceros1'][] = $row[1];
		}
        else if($row[0] == '124')
		{
			$_POST['pagoSalarios'][]=0;
			$_POST['eclesiasticos'][]=0;
			$_POST['pagoAbonoElectronico'][]=0;//nueva
			$_POST['valorExcesoPagoAlimentacion'][]=0;//nueva
			$_POST['honorario'][]=0;
			$_POST['pagoPorServicio'][]=0;
			$_POST['pagoPorComisiones'][]=0;
			$_POST['pagoPorPrestasionesSociales'][]=0;
			$_POST['pagoPorViaticos'][]=0;
			$_POST['pagoPorRepresentacion'][]=0;
			$_POST['pagoPorCompensaciones'][]=0;
			$_POST['apoyoEconomicoNoReembolso'][]=0;//nueva
			$_POST['otrosPagos'][]=0;
			$_POST['interesesCesantias'][]=0;
			$_POST['cesantiasFondoCesantias'][]=0;
			$_POST['auxilioCesantias'][]=0;
			$_POST['pensionesJubilacion'][]=0;
			$_POST['rentasTrabajoPrension'][]=0;
			$_POST['aportesSalud'][]=0;
			$_POST['aportesPensiones'][]=0;
			$_POST['aporteVoluntarioRegimenIndividual'][]=0;
			$_POST['aportesPensionesVoluntarios'][]=0;
			$_POST['cuentasAFC'][]=0;
			$_POST['cuentasAVC'][]=$row[2];
			$_POST['retencionesFuenteRentas'][]=$row[4];
			$_POST['iva'][]=0;
			$_POST['retencionIva'][]=0;
			$_POST['pagoAlimentacion'][]=0;
			$_POST['ingresoLaboralPromedio'][]=0;
			$_POST['terceros1'][] = $row[1];
		}
        else if($row[0] == '125')
		{
			$_POST['pagoSalarios'][]=0;
			$_POST['eclesiasticos'][]=0;
			$_POST['pagoAbonoElectronico'][]=0;//nueva
			$_POST['valorExcesoPagoAlimentacion'][]=0;//nueva
			$_POST['honorario'][]=0;
			$_POST['pagoPorServicio'][]=0;
			$_POST['pagoPorComisiones'][]=0;
			$_POST['pagoPorPrestasionesSociales'][]=0;
			$_POST['pagoPorViaticos'][]=0;
			$_POST['pagoPorRepresentacion'][]=0;
			$_POST['pagoPorCompensaciones'][]=0;
			$_POST['apoyoEconomicoNoReembolso'][]=0;//nueva
			$_POST['otrosPagos'][]=0;
			$_POST['interesesCesantias'][]=0;
			$_POST['cesantiasFondoCesantias'][]=0;
			$_POST['auxilioCesantias'][]=0;
			$_POST['pensionesJubilacion'][]=0;
			$_POST['rentasTrabajoPrension'][]=0;
			$_POST['aportesSalud'][]=0;
			$_POST['aportesPensiones'][]=0;
			$_POST['aporteVoluntarioRegimenIndividual'][]=0;
			$_POST['aportesPensionesVoluntarios'][]=0;
			$_POST['cuentasAFC'][]=0;
			$_POST['cuentasAVC'][]=0;
			$_POST['retencionesFuenteRentas'][]=$row[4];
			$_POST['iva'][]=0;
			$_POST['retencionIva'][]=0;
			$_POST['pagoAlimentacion'][]=0;
			$_POST['ingresoLaboralPromedio'][]=0;
			$_POST['terceros1'][] = $row[1];
		}
        else if($row[0] == '126')
		{
			$_POST['pagoSalarios'][]=0;
			$_POST['eclesiasticos'][]=0;
			$_POST['pagoAbonoElectronico'][]=0;//nueva
			$_POST['valorExcesoPagoAlimentacion'][]=0;//nueva
			$_POST['honorario'][]=0;
			$_POST['pagoPorServicio'][]=0;
			$_POST['pagoPorComisiones'][]=0;
			$_POST['pagoPorPrestasionesSociales'][]=0;
			$_POST['pagoPorViaticos'][]=0;
			$_POST['pagoPorRepresentacion'][]=0;
			$_POST['pagoPorCompensaciones'][]=0;
			$_POST['apoyoEconomicoNoReembolso'][]=0;//nueva
			$_POST['otrosPagos'][]=0;
			$_POST['interesesCesantias'][]=0;
			$_POST['cesantiasFondoCesantias'][]=0;
			$_POST['auxilioCesantias'][]=0;
			$_POST['pensionesJubilacion'][]=0;
			$_POST['rentasTrabajoPrension'][]=0;
			$_POST['aportesSalud'][]=0;
			$_POST['aportesPensiones'][]=0;
			$_POST['aporteVoluntarioRegimenIndividual'][]=0;
			$_POST['aportesPensionesVoluntarios'][]=0;
			$_POST['cuentasAFC'][]=0;
			$_POST['cuentasAVC'][]=0;
			$_POST['retencionesFuenteRentas'][]=$row[4];
			$_POST['iva'][]=$row[2];
			$_POST['retencionIva'][]=0;
			$_POST['pagoAlimentacion'][]=0;
			$_POST['ingresoLaboralPromedio'][]=0;
			$_POST['terceros1'][] = $row[1];
		}
        else if($row[0] == '127')
		{
			$_POST['pagoSalarios'][]=0;
			$_POST['eclesiasticos'][]=0;
			$_POST['pagoAbonoElectronico'][]=0;//nueva
			$_POST['valorExcesoPagoAlimentacion'][]=0;//nueva
			$_POST['honorario'][]=0;
			$_POST['pagoPorServicio'][]=0;
			$_POST['pagoPorComisiones'][]=0;
			$_POST['pagoPorPrestasionesSociales'][]=0;
			$_POST['pagoPorViaticos'][]=0;
			$_POST['pagoPorRepresentacion'][]=0;
			$_POST['pagoPorCompensaciones'][]=0;
			$_POST['apoyoEconomicoNoReembolso'][]=0;//nueva
			$_POST['otrosPagos'][]=0;
			$_POST['interesesCesantias'][]=0;
			$_POST['cesantiasFondoCesantias'][]=0;
			$_POST['auxilioCesantias'][]=0;
			$_POST['pensionesJubilacion'][]=0;
			$_POST['rentasTrabajoPrension'][]=0;
			$_POST['aportesSalud'][]=0;
			$_POST['aportesPensiones'][]=0;
			$_POST['aporteVoluntarioRegimenIndividual'][]=0;
			$_POST['aportesPensionesVoluntarios'][]=0;
			$_POST['cuentasAFC'][]=0;
			$_POST['cuentasAVC'][]=0;
			$_POST['retencionesFuenteRentas'][]=$row[4];
			$_POST['iva'][]=0;
			$_POST['retencionIva'][]=0;
			$_POST['pagoAlimentacion'][]=0;
			$_POST['ingresoLaboralPromedio'][]=0;
			$_POST['terceros1'][] = $row[1];
		}
        else if($row[0] == '128')
		{
			$_POST['pagoSalarios'][]=0;
			$_POST['eclesiasticos'][]=0;
			$_POST['pagoAbonoElectronico'][]=0;//nueva
			$_POST['valorExcesoPagoAlimentacion'][]=0;//nueva
			$_POST['honorario'][]=0;
			$_POST['pagoPorServicio'][]=0;
			$_POST['pagoPorComisiones'][]=0;
			$_POST['pagoPorPrestasionesSociales'][]=0;
			$_POST['pagoPorViaticos'][]=0;
			$_POST['pagoPorRepresentacion'][]=0;
			$_POST['pagoPorCompensaciones'][]=0;
			$_POST['apoyoEconomicoNoReembolso'][]=0;//nueva
			$_POST['otrosPagos'][]=0;
			$_POST['interesesCesantias'][]=0;
			$_POST['cesantiasFondoCesantias'][]=0;
			$_POST['auxilioCesantias'][]=0;
			$_POST['pensionesJubilacion'][]=0;
			$_POST['rentasTrabajoPrension'][]=0;
			$_POST['aportesSalud'][]=0;
			$_POST['aportesPensiones'][]=0;
			$_POST['aporteVoluntarioRegimenIndividual'][]=0;
			$_POST['aportesPensionesVoluntarios'][]=0;
			$_POST['cuentasAFC'][]=0;
			$_POST['cuentasAVC'][]=0;
			$_POST['retencionesFuenteRentas'][]=0;
			$_POST['iva'][]=0;
			$_POST['retencionIva'][]=0;
			$_POST['pagoAlimentacion'][]=$row[2];
			$_POST['ingresoLaboralPromedio'][]=0;
			$_POST['terceros1'][] = $row[1];
		}
        else if($row[0] == '129')
		{
			$_POST['pagoSalarios'][]=0;
			$_POST['eclesiasticos'][]=0;
			$_POST['pagoAbonoElectronico'][]=0;//nueva
			$_POST['valorExcesoPagoAlimentacion'][]=0;//nueva
			$_POST['honorario'][]=0;
			$_POST['pagoPorServicio'][]=0;
			$_POST['pagoPorComisiones'][]=0;
			$_POST['pagoPorPrestasionesSociales'][]=0;
			$_POST['pagoPorViaticos'][]=0;
			$_POST['pagoPorRepresentacion'][]=0;
			$_POST['pagoPorCompensaciones'][]=0;
			$_POST['apoyoEconomicoNoReembolso'][]=0;//nueva
			$_POST['otrosPagos'][]=0;
			$_POST['interesesCesantias'][]=0;
			$_POST['cesantiasFondoCesantias'][]=0;
			$_POST['auxilioCesantias'][]=0;
			$_POST['pensionesJubilacion'][]=0;
			$_POST['rentasTrabajoPrension'][]=0;
			$_POST['aportesSalud'][]=0;
			$_POST['aportesPensiones'][]=0;
			$_POST['aporteVoluntarioRegimenIndividual'][]=0;
			$_POST['aportesPensionesVoluntarios'][]=0;
			$_POST['cuentasAFC'][]=0;
			$_POST['cuentasAVC'][]=0;
			$_POST['retencionesFuenteRentas'][]=0;
			$_POST['iva'][]=0;
			$_POST['retencionIva'][]=0;
			$_POST['pagoAlimentacion'][]=0;
			$_POST['ingresoLaboralPromedio'][]=$row[2];
			$_POST['terceros1'][] = $row[1];
		}

	}
	$max = MAX($_POST['nomina']);
	$min = MIN($_POST['nomina']);
	$sqlr="create  temporary table exogena2276 (id int(11),tercero varchar(20),pagoSalarios double,eclesiasticos double,pagoAbonoElectronico double, valorExcesoPagoAlimentacion double,honorario double,pagoPorServicio double,pagoPorComisiones double,pagoPorPrestasionesSociales double,pagoPorViaticos double,pagoPorRepresentacion double,pagoPorCompensaciones double, apoyoEconomicoNoReembolso double, otrosPagos double,interesesCesantias double, cesantiasFondoCesantias double, auxilioCesantias double, pensionesJubilacion double, rentasTrabajoPrension double, aportesSalud double, aportesPensiones double, aporteVoluntarioRegimenIndividual double, aportesPensionesVoluntarios double, cuentasAFC double, cuentasAVC double, retencionesFuenteRentas double, iva double, retencionIva double, pagoAlimentacion double, ingresoLaboralPromedio double,nomina int(11))";
	mysqli_query($linkbd, $sqlr);
    for($x = 0 ; $x < count($_POST['pagoSalarios']) ; $x++)
    {

		//$sqlr="insert into exogena2276 (id,tercero,salarios,viaticos,cesantias,prestaciones,otrospagos,nomina) values($x,'".$_POST[terceros1][$x]."','".$_POST[salarios][$x]."','".$_POST[viaticos][$x]."','".$_POST[cesantias][$x]."','".$_POST[prestaciones][$x]."','".$_POST[otrosPagos][$x]."','".$_POST[nomina][$x]."')";
		$sqlr="insert into exogena2276 (id,tercero,pagoSalarios,eclesiasticos, pagoAbonoElectronico, valorExcesoPagoAlimentacion,honorario,pagoPorServicio,pagoPorComisiones,pagoPorPrestasionesSociales,pagoPorViaticos,pagoPorRepresentacion,pagoPorCompensaciones, apoyoEconomicoNoReembolso,otrosPagos,interesesCesantias, cesantiasFondoCesantias, auxilioCesantias,pensionesJubilacion,rentasTrabajoPrension,aportesSalud,aportesPensiones,aporteVoluntarioRegimenIndividual,aportesPensionesVoluntarios,cuentasAFC,cuentasAVC,retencionesFuenteRentas,iva,retencionIva,pagoAlimentacion,ingresoLaboralPromedio,nomina) values($x,'".$_POST['terceros1'][$x]."','".$_POST['pagoSalarios'][$x]."','".$_POST['eclesiasticos'][$x]."','".$_POST['pagoAbonoElectronico'][$x]."','".$_POST['valorExcesoPagoAlimentacion'][$x]."','".$_POST['honorario'][$x]."','".$_POST['pagoPorServicio'][$x]."','".$_POST['pagoPorComisiones'][$x]."','".$_POST['pagoPorPrestasionesSociales'][$x]."','".$_POST['pagoPorViaticos'][$x]."','".$_POST['pagoPorRepresentacion'][$x]."','".$_POST['pagoPorCompensaciones'][$x]."','".$_POST['apoyoEconomicoNoReembolso'][$x]."','".$_POST['otrosPagos'][$x]."','".$_POST['interesesCesantias'][$x]."','".$_POST['cesantiasFondoCesantias'][$x]."','".$_POST['auxilioCesantias'][$x]."','".$_POST['pensionesJubilacion'][$x]."','".$_POST['rentasTrabajoPrension'][$x]."','".$_POST['aportesSalud'][$x]."','".$_POST['aportesPensiones'][$x]."','".$_POST['aporteVoluntarioRegimenIndividual'][$x]."','".$_POST['aportesPensionesVoluntarios'][$x]."','".$_POST['cuentasAFC'][$x]."','".$_POST['cuentasAVC'][$x]."','".$_POST['retencionesFuenteRentas'][$x]."','".$_POST['iva'][$x]."','".$_POST['retencionIva'][$x]."','".$_POST['pagoAlimentacion'][$x]."','".$_POST['ingresoLaboralPromedio'][$x]."','".$_POST['nomina'][$x]."')";
		//echo "hola".$_POST[terceros1][$x];
		mysqli_query($linkbd, $sqlr);
	}
	$sqlrExogena = "select tercero,sum(pagoSalarios),sum(eclesiasticos),sum(pagoAbonoElectronico),sum(valorExcesoPagoAlimentacion),sum(honorario),sum(pagoPorServicio),sum(pagoPorComisiones),sum(pagoPorPrestasionesSociales),sum(pagoPorViaticos),sum(pagoPorRepresentacion),sum(pagoPorCompensaciones),sum(apoyoEconomicoNoReembolso),sum(otrosPagos),sum(interesesCesantias),sum(cesantiasFondoCesantias),sum(auxilioCesantias),sum(pensionesJubilacion),sum(rentasTrabajoPrension),sum(aportesSalud),sum(aportesPensiones),sum(aporteVoluntarioRegimenIndividual),sum(aportesPensionesVoluntarios),sum(cuentasAFC),sum(cuentasAVC),sum(retencionesFuenteRentas),sum(iva),sum(retencionIva),sum(pagoAlimentacion),sum(ingresoLaboralPromedio),nomina from exogena2276 group by tercero order by tercero";
	$resExogena=mysqli_query($linkbd, $sqlrExogena);
	//echo "hola".$sqlrExogena;
	while($row=mysqli_fetch_row($resExogena))
  	{
		$sqlrNomina = "select salud, pension, fondosolid from humnomina_det where id_nom>='$min' and id_nom<=$max and cedulanit='$row[0]'";
		//echo $sqlrNomina."<br>";
		$restNomina = mysqli_query($linkbd, $sqlrNomina);
		$salud = 0;
		$pension = 0;
		while($rowtNomina = mysqli_fetch_row($restNomina))
		{
			$salud = $salud + $rowtNomina[0];

			$pension = $pension + $rowtNomina[1] + $rowtNomina[2];

		}
		$row[19] = $row[19] + $salud;
		$row[20] = $row[20] + $pension;
		$sqlrt="select * from terceros where cedulanit='$row[0]'";
		//echo $sqlrt." - ".$_POST[terceros1][$x];
		$rest=mysqli_query($linkbd, $sqlrt);
		$rowt=mysqli_fetch_row($rest);
		$filbor="A".$xy.":AS".$xy;
		$objPHPExcel-> getActiveSheet ()
				-> getStyle ($filbor)
		-> getFont ()
		-> setBold ( false )
				-> setName ('Arial')
				-> setSize ( 10 )
		-> getColor ()
		-> setRGB ('000000');
		$objPHPExcel->getActiveSheet()->getStyle($filbor)->applyFromArray($borders);
		$objPHPExcel->setActiveSheetIndex(0)
		->setCellValueExplicit ("A".$xy, utf8_encode('1'), PHPExcel_Cell_DataType :: TYPE_STRING)
		->setCellValueExplicit ("B".$xy, utf8_encode($rowt[11]), PHPExcel_Cell_DataType :: TYPE_STRING)
		->setCellValueExplicit ("C".$xy, utf8_encode($rowt[12]), PHPExcel_Cell_DataType :: TYPE_STRING)
		->setCellValueExplicit ("D".$xy, utf8_encode($rowt[3]), PHPExcel_Cell_DataType :: TYPE_STRING)
		->setCellValueExplicit ("E".$xy, utf8_encode($rowt[4]), PHPExcel_Cell_DataType :: TYPE_STRING)
		->setCellValueExplicit ("F".$xy, utf8_encode($rowt[1]), PHPExcel_Cell_DataType :: TYPE_STRING)
		->setCellValueExplicit ("G".$xy, utf8_encode($rowt[2]), PHPExcel_Cell_DataType :: TYPE_STRING)
		->setCellValueExplicit ("H".$xy, utf8_encode($rowt[6]), PHPExcel_Cell_DataType :: TYPE_STRING)
		->setCellValueExplicit ("I".$xy, utf8_encode($rowt[14]), PHPExcel_Cell_DataType :: TYPE_STRING)
		->setCellValueExplicit ("J".$xy, utf8_encode($rowt[15]), PHPExcel_Cell_DataType :: TYPE_STRING)
		->setCellValueExplicit ("K".$xy, "169", PHPExcel_Cell_DataType :: TYPE_STRING)
		->setCellValueExplicit ("L".$xy, utf8_encode(round($row[1])), PHPExcel_Cell_DataType :: TYPE_NUMERIC)
		->setCellValueExplicit ("M".$xy, utf8_encode(round($row[2])), PHPExcel_Cell_DataType :: TYPE_NUMERIC)
		->setCellValueExplicit ("N".$xy, utf8_encode(round($row[3])), PHPExcel_Cell_DataType :: TYPE_NUMERIC)
		->setCellValueExplicit ("O".$xy, utf8_encode(round($row[4])), PHPExcel_Cell_DataType :: TYPE_NUMERIC)
		->setCellValueExplicit ("P".$xy, utf8_encode(round($row[5])), PHPExcel_Cell_DataType :: TYPE_NUMERIC)
		->setCellValueExplicit ("Q".$xy, utf8_encode(round($row[6])), PHPExcel_Cell_DataType :: TYPE_NUMERIC)
		->setCellValueExplicit ("R".$xy, utf8_encode(round($row[7])), PHPExcel_Cell_DataType :: TYPE_NUMERIC)
		->setCellValueExplicit ("S".$xy, utf8_encode(round($row[8])), PHPExcel_Cell_DataType :: TYPE_NUMERIC)
		->setCellValueExplicit ("T".$xy, utf8_encode(round($row[9])), PHPExcel_Cell_DataType :: TYPE_NUMERIC)
		->setCellValueExplicit ("U".$xy, utf8_encode(round($row[10])), PHPExcel_Cell_DataType :: TYPE_NUMERIC)
		->setCellValueExplicit ("V".$xy, utf8_encode(round($row[11])), PHPExcel_Cell_DataType :: TYPE_NUMERIC)
		->setCellValueExplicit ("W".$xy, utf8_encode(round($row[12])), PHPExcel_Cell_DataType :: TYPE_NUMERIC)
		->setCellValueExplicit ("X".$xy, utf8_encode(round($row[13])), PHPExcel_Cell_DataType :: TYPE_NUMERIC)
		->setCellValueExplicit ("Y".$xy, utf8_encode(round($row[14])), PHPExcel_Cell_DataType :: TYPE_NUMERIC)
		->setCellValueExplicit ("Z".$xy, utf8_encode(round($row[15])), PHPExcel_Cell_DataType :: TYPE_NUMERIC)
		->setCellValueExplicit ("AA".$xy, utf8_encode(round($row[16])), PHPExcel_Cell_DataType :: TYPE_NUMERIC)
		->setCellValueExplicit ("AB".$xy, utf8_encode(round($row[17])), PHPExcel_Cell_DataType :: TYPE_NUMERIC)
		->setCellValueExplicit ("AC".$xy, utf8_encode(round($row[18])), PHPExcel_Cell_DataType :: TYPE_NUMERIC)
		->setCellValueExplicit ("AD".$xy, utf8_encode(round($row[19])), PHPExcel_Cell_DataType :: TYPE_NUMERIC)
		->setCellValueExplicit ("AE".$xy, utf8_encode(round($row[20])), PHPExcel_Cell_DataType :: TYPE_NUMERIC)
		->setCellValueExplicit ("AF".$xy, utf8_encode(round($row[21])), PHPExcel_Cell_DataType :: TYPE_NUMERIC)
		->setCellValueExplicit ("AG".$xy, utf8_encode(round($row[22])), PHPExcel_Cell_DataType :: TYPE_NUMERIC)
		->setCellValueExplicit ("AH".$xy, utf8_encode(round($row[23])), PHPExcel_Cell_DataType :: TYPE_NUMERIC)
		->setCellValueExplicit ("AI".$xy, utf8_encode(round($row[24])), PHPExcel_Cell_DataType :: TYPE_NUMERIC)
		->setCellValueExplicit ("AJ".$xy, utf8_encode(round($row[25])), PHPExcel_Cell_DataType :: TYPE_NUMERIC)
		->setCellValueExplicit ("AK".$xy, utf8_encode(round($row[26])), PHPExcel_Cell_DataType :: TYPE_NUMERIC)
		->setCellValueExplicit ("AL".$xy, utf8_encode(round($row[27])), PHPExcel_Cell_DataType :: TYPE_NUMERIC)
		->setCellValueExplicit ("AM".$xy, utf8_encode(round($row[28])), PHPExcel_Cell_DataType :: TYPE_NUMERIC)
		->setCellValueExplicit ("AN".$xy, utf8_encode(round($row[29])), PHPExcel_Cell_DataType :: TYPE_NUMERIC)

		->setCellValueExplicit ("AO".$xy, utf8_encode(''), PHPExcel_Cell_DataType :: TYPE_NUMERIC)
		->setCellValueExplicit ("AP".$xy, utf8_encode(''), PHPExcel_Cell_DataType :: TYPE_NUMERIC)
		->setCellValueExplicit ("AQ".$xy, utf8_encode(''), PHPExcel_Cell_DataType :: TYPE_NUMERIC)
		->setCellValueExplicit ("AR".$xy, utf8_encode(''), PHPExcel_Cell_DataType :: TYPE_NUMERIC)
		->setCellValueExplicit ("AS".$xy, utf8_encode(''), PHPExcel_Cell_DataType :: TYPE_NUMERIC);
		$xy++;
	}

	// Renombrar Hoja
	//$objPHPExcel->getActiveSheet()->setTitle('Listado Asistencia');
	// Establecer la hoja activa, para que cuando se abra el documento se muestre primero.
	$objPHPExcel->setActiveSheetIndex(0);
	// Se modifican los encabezados del HTTP para indicar que se envia un archivo de Excel.
	header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
	header('Content-Disposition: attachment;filename="fmt2276_'.$_POST['vigencias'].'.xlsx"');
	header('Cache-Control: max-age=0');
	$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
	$objWriter->save('php://output');
	exit;
?>
