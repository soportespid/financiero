<?php
	require"comun.inc";
	require"funciones.inc";
	session_start();
	$linkbd=conectar_bd();	
	header("Cache-control: private"); // Arregla IE 6
	date_default_timezone_set("America/Bogota");
?>
<!DOCTYPE >
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="es">
	<head>
	 	<meta http-equiv="Content-Type" content="text/html" charset="iso-8859-1"/>
        <meta http-equiv="X-UA-Compatible" content="IE=9"/>
		<title>:: SPID - Servicios Publicos</title>
        <link href="css/css2.css" rel="stylesheet" type="text/css" />
		<link href="css/css3.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="css/programas.js"></script>
		<script type="text/javascript" src="css/calendario.js"></script>
        <script type='text/javascript' src='JQuery/jquery-2.1.4.min.js'></script>
		<script>
			function fucanular(recibo, factura, codusu)
			{
				document.getElementById('idanul').value=recibo+'-'+factura+'-'+codusu;
				despliegamodalm('visible','4','Esta seguro de Anular el Recibo N� '+recibo,'1');
				
			}
			function fucdeshacer(recibo, factura, codusu)
			{
				document.getElementById('idanul').value=recibo+'-'+factura+'-'+codusu;
				despliegamodalm('visible','4','Deshacer la Anulaci�n del Recibo N� '+recibo,'2');
				
			}
			function despliegamodalm(_valor,_tip,mensa,pregunta)
			{
				document.getElementById("bgventanamodalm").style.visibility=_valor;
				if(_valor=="hidden"){document.getElementById('ventanam').src="";}
				else
				{
					switch(_tip)
					{
						case "1":
							document.getElementById('ventanam').src="ventana-mensaje1.php?titulos="+mensa;break;
						case "2":
							document.getElementById('ventanam').src="ventana-mensaje3.php?titulos="+mensa;break;
						case "3":
							document.getElementById('ventanam').src="ventana-mensaje2.php?titulos="+mensa;break;
						case "4":
							document.getElementById('ventanam').src="ventana-consulta1.php?titulos="+mensa+"&idresp="+pregunta;break;	
					}
				}
			}
			function funcionmensaje(){document.location.href = "";}
			function respuestaconsulta(pregunta)
			{
				switch(pregunta)
				{
					case "1":	document.form2.oculto.value='3';
								document.form2.submit();
								break;
					case "2":	document.form2.oculto.value='4';
								document.form2.submit();
								break;
				}
			}
			var ctrlPressed = false;
			var tecla01 = 16, tecla02 = 80, tecla03 = 81;
			$(document).keydown(
				function(e){
					if (e.keyCode == tecla01){ctrlPressed = true;}
					if (e.keyCode == tecla03){tecla3Pressed = true;}
					if (ctrlPressed && (e.keyCode == tecla02) && tecla3Pressed)
					{
						
						if(document.form2.iddesh.value=="0"){document.form2.iddesh.value="1";}
						else {document.form2.iddesh.value="0";}
						document.form2.submit();
					}
					})
					$(document).keyup(function(e){if (e.keyCode ==tecla01){ctrlPressed = false;}})
					$(document).keyup(function(e){if (e.keyCode ==tecla03){tecla3Pressed = false;}
				})
		</script>
	</head>
	<body>
		<IFRAME src="alertas.php" name="alertas" id="alertas" style="display:none"></IFRAME>
		<span id="todastablas2"></span>
		<table>
			<tr><script>barra_imagenes("serv");</script><?php cuadro_titulos();?></tr>	 
			<tr><?php menu_desplegable("serv");?></tr>
			<tr>
  				<td colspan="3" class="cinta"><a onClick="location.href='serv-abonos.php'" class='mgbt'><img src="imagenes/add.png" title="Nuevo"/></a><a class='mgbt'><img src="imagenes/guardad.png" /></a><a onClick="document.form2.submit();" class='mgbt'><img src="imagenes/busca.png" title="Buscar" /></a><a onClick="mypop=window.open('serv-principal.php','','');mypop.focus();" class='mgbt'><img src="imagenes/nv.png" title="Nueva Ventana"></a></td>
          	</tr>	
		</table>
        <div id="bgventanamodalm" class="bgventanamodalm">
            <div id="ventanamodalm" class="ventanamodalm">
                <IFRAME src="" name="ventanam" marginWidth=0 marginHeight=0 frameBorder=0 id="ventanam" frameSpacing=0 style=" width:700px; height:130px; top:200; overflow:hidden;"> 
                </IFRAME>
            </div>
        </div>
 		<form name="form2" method="post" action="serv-buscarabonos.php">
        	<?php if ($_POST[oculto]==""){$_POST[numres]=10;$_POST[numpos]=0;$_POST[nummul]=0;$_POST[iddesh]=0;}?>
			<table  class="inicio" align="center" >
      			<tr>
        			<td class="titulos" colspan="6">:. Buscar Abonos </td>
                    <td class="cerrar" style="width:7%"><a onClick="location.href='serv-principal.php';" >&nbsp;Cerrar</a></td>
     			</tr>
      			<tr>
        			<td class="saludo1" style="width:2.5cm">N&deg; Recibo:</td>
        			<td  style="width:15%"><input type="search" name="numero" id="numero" value="<?php echo $_POST[numero];?>"/></td>
         			<td class="saludo1"  style="width:2.5cm">Factura: </td>
   					<td >
                    	<input type="search" name="nombre" id="nombre" value="<?php echo $_POST[nombre];?>"/>
                        <input type="button" name="bboton" onClick="limbusquedas();" value="&nbsp;&nbsp;Buscar&nbsp;&nbsp;" />
                    </td>
        		</tr>                       
			</table> 
            <input type="hidden" name="oculto" id="oculto"  value="1"/>  
            <input type="hidden" name="idanul" id="idanul" value="<?php echo $_POST[idanul];?>"/>
            <input type="hidden" name="iddesh" id="iddesh" value="<?php echo $_POST[iddesh];?>"/>
			<input type="hidden" name="numres" id="numres" value="<?php echo $_POST[numres];?>"/>
    		<input type="hidden" name="numpos" id="numpos" value="<?php echo $_POST[numpos];?>"/>
       		<input type="hidden" name="nummul" id="nummul" value="<?php echo $_POST[nummul];?>"/>
           	<div class="subpantalla"  style="height:65%; width:99.6%; overflow-x:hidden;" >
      			<?php
					if($_POST[oculto]=='3')
					{
						$idcod = explode('-', $_POST[idanul]); 
						$sqlr="UPDATE servreciboscaja SET estado='N' WHERE id_recibos='$idcod[0]'";
						mysql_query($sqlr,$linkbd);
						$sqlr="UPDATE servreciboscaja_det SET estado='N' WHERE id_recibos='$idcod[0]'";
						mysql_query($sqlr,$linkbd);
						$sqlr="UPDATE servliquidaciones_det TB1 SET TB1.abono=TB1.abono-(SELECT TB2.valor FROM servreciboscaja_det TB2 WHERE TB2.ingreso=TB1.servicio AND TB2.id_recibos='$idcod[0]') WHERE TB1.id_liquidacion='$idcod[1]'";
						mysql_query($sqlr,$linkbd);
						$sqlr="UPDATE servclientes SET intereses=intereses+(SELECT SUM(intereses) FROM servreciboscaja_det WHERE id_recibos='$idcod[0]')  WHERE codigo='$idcod[2]'";
						mysql_query($sqlr,$linkbd);
					}
					if($_POST[oculto]=='4')
					{
						$idcod = explode('-', $_POST[idanul]); 
						$sqlr="UPDATE servreciboscaja SET estado='S' WHERE id_recibos='$idcod[0]'";
						mysql_query($sqlr,$linkbd);
						$sqlr="UPDATE servreciboscaja_det SET estado='S' WHERE id_recibos='$idcod[0]'";
						mysql_query($sqlr,$linkbd);
						$sqlr="UPDATE servliquidaciones_det TB1 SET TB1.abono=TB1.abono+(SELECT TB2.valor FROM servreciboscaja_det TB2 WHERE TB2.ingreso=TB1.servicio AND TB2.id_recibos='$idcod[0]') WHERE TB1.id_liquidacion='$idcod[1]'";
						mysql_query($sqlr,$linkbd);
						$sqlr="UPDATE servclientes SET intereses=IF(intereses<>0,intereses-(SELECT SUM(intereses),0) FROM servreciboscaja_det WHERE id_recibos='$idcod[0]')  WHERE codigo='$idcod[2]'";
						mysql_query($sqlr,$linkbd);
					}
	  				$vigusu=vigencia_usuarios($_SESSION[cedulausu]);
					$crit1=" ";
					$crit2=" ";
					if ($_POST[numero]!=""){$crit1=" AND TB1.id_recibos LIKE '%$_POST[numero]%' ";}
					if ($_POST[nombre]!=""){$crit2=" AND TB1.id_recaudo LIKE '%$_POST[nombre]%'  ";}
					//$sqlr="select *from pptosideforigen where".$crit1.$crit2." order by pptosideforigen.codigo";
					$sqlr="SELECT TB1.*,TB2.codusuario FROM servreciboscaja TB1,servfacturas TB2, servclientes TB3,terceros TB4 WHERE  TB1.id_recaudo=TB2.id_factura AND TB1.tipo='5' AND TB2.codusuario=TB3.codigo AND TB3.terceroactual=TB4.cedulanit $crit1 $crit2";
					$resp = mysql_query($sqlr,$linkbd);
					$_POST[numtop]=mysql_num_rows($resp);
					$nuncilumnas=ceil($_POST[numtop]/$_POST[numres]);
					$cond2="";
					if ($_POST[numres]!="-1"){$cond2="LIMIT $_POST[numpos], $_POST[numres]";}
					$sqlr="SELECT TB1.*,TB2.codusuario,concat_ws(' ',TB4.nombre1,TB4.nombre2,TB4.apellido1,TB4.apellido2,TB4.razonsocial) FROM servreciboscaja TB1,servfacturas TB2,servclientes TB3,terceros TB4 WHERE TB1.id_recaudo=TB2.id_factura AND TB1.tipo='5' AND TB2.codusuario=TB3.codigo AND TB3.terceroactual=TB4.cedulanit $crit1 $crit2 ORDER BY TB1.id_recibos DESC $cond2";
					$resp = mysql_query($sqlr,$linkbd);
					$con=1;
					$numcontrol=$_POST[nummul]+1;
					if(($nuncilumnas==$numcontrol)||($_POST[numres]=="-1"))
					{
						$imagenforward="<img src='imagenes/forward02.png' style='width:17px;cursor:default;'/>";
						$imagensforward="<img src='imagenes/skip_forward02.png' style='width:16px;cursor:default;'/>";
					}
					else 
					{
						$imagenforward="<img src='imagenes/forward01.png' style='width:17px;cursor:pointer;' title='Siguiente' onClick='numsiguiente()'/>";
						$imagensforward="<img src='imagenes/skip_forward01.png' style='width:16px;cursor:pointer;' title='Fin' onClick='saltocol(\"$nuncilumnas\")'/>";
					}
					if(($_POST[numpos]==0)||($_POST[numres]=="-1"))
					{
						$imagenback="<img src='imagenes/back02.png' style='width:17px;cursor:default;'/>";
						$imagensback="<img src='imagenes/skip_back02.png' style='width:16px;cursor:default;'/>";
					}
					else
					{
						$imagenback="<img src='imagenes/back01.png' style='width:17px;cursor:pointer;' title='Anterior' onClick='numanterior();'/>";
						$imagensback="<img src='imagenes/skip_back01.png' style='width:16px;cursor:pointer;' title='Inicio' onClick='saltocol(\"1\")'/>";
					}
					echo "
					<table class='inicio' align='center' >
						<tr>
							<td colspan='7' class='titulos'>.: Resultados Busqueda:</td>
							<td class='submenu'>
								<select name='renumres' id='renumres' onChange='cambionum();' style='width:100%'>
									<option value='10'"; if ($_POST[renumres]=='10'){echo 'selected';} echo ">10</option>
									<option value='20'"; if ($_POST[renumres]=='20'){echo 'selected';} echo ">20</option>
									<option value='30'"; if ($_POST[renumres]=='30'){echo 'selected';} echo ">30</option>
									<option value='50'"; if ($_POST[renumres]=='50'){echo 'selected';} echo ">50</option>
									<option value='100'"; if ($_POST[renumres]=='100'){echo 'selected';} echo ">100</option>
									<option value='-1'"; if ($_POST[renumres]=='-1'){echo 'selected';} echo ">Todos</option>
								</select>
							</td>
						</tr>
						<tr><td colspan='8'>Encontrados: $_POST[numtop]</td></tr>
						<tr>
							<td class='titulos2' style='width:8%;'>No Recibo</td>
							<td class='titulos2' style='width:8%;'>Factura</td>
							<td class='titulos2' style='width:8%;'>Fecha</td>
							<td class='titulos2'>Tercero</td>
							<td class='titulos2'>Valor</td>";
					if($_POST[iddesh]==1){echo "<td class='titulos2' style='text-align:center;width:5%;'>Deshacer</td>";}
					echo"
							<td class='titulos2' style='text-align:center;width:5%;'>Estado</td>
							<td class='titulos2' style='text-align:center;width:5%;'>Anular</td>
							<td class='titulos2' style='text-align:center;width:5%;'>Editar</td>
						</tr>";	
					//echo "nr:".$nr;
					$iter='saludo1a';
					$iter2='saludo2';
 					while ($row =mysql_fetch_row($resp)) 
 					{	
						if($row[9]=='S')
						{
							$imgsem="src='imagenes/sema_verdeON.jpg' title='Activo'";
							$imgdes="<td style='text-align:center;'><img src='imagenes/flechadesd.png' style='width:20px;'></td>";
						}
						else
						{
							$imgsem="src='imagenes/sema_rojoON.jpg' title='Anulada'";
							$imgdes="<td style='text-align:center;cursor:pointer;' onClick=\"fucdeshacer('$row[0]','$row[4]','$row[11]')\"><img src='imagenes/flechades.png' title='Deshacer Anular' style='width:20px;'></td>";
						}
	 					echo "
						<tr class='$iter' onMouseOver=\"anterior=this.style.backgroundColor;this.style.backgroundColor='#40b3ff';\"
		onMouseOut=\"this.style.backgroundColor=anterior\" style='text-transform:uppercase;' >
							<td>$row[0]</td>
							<td>$row[4]</td>
							<td>".date('d-m-Y',strtotime($row[2]))."</td>
							<td>$row[12]</td>
							<td>$row[8]</td>";
						if($_POST[iddesh]=="1"){echo $imgdes;}
						echo"
							<td style='text-align:center;'><img $imgsem style='width:20px'/></td>
							<td style='text-align:center;cursor:pointer;' onClick=\"fucanular('$row[0]','$row[4]','$row[11]')\"><img src='imagenes/anular.png' title='Anular'></td>
							<td style='text-align:center;'><a href='serv-abonosver.php?idr=$row[0]'><img src='imagenes/buscarep.png'></a></td>
						</tr>";
						$con+=1;
						$aux=$iter;
						$iter=$iter2;
						$iter2=$aux;
 					}
 					if ($_POST[numtop]==0)
				{
					echo "
					<table class='inicio'>
						<tr>
							<td class='saludo1' style='text-align:center;width:100%'><img src='imagenes\alert.png' style='width:25px'>No hay coincidencias en la b&uacute;squeda $tibusqueda<img src='imagenes\alert.png' style='width:25px'></td>
						</tr>
					</table>";
				}
				echo"
						</table>
						<table class='inicio'>
							<tr>
								<td style='text-align:center;'>
									<a>$imagensback</a>&nbsp;
									<a>$imagenback</a>&nbsp;&nbsp;";
					if($nuncilumnas<=9){$numfin=$nuncilumnas;}
					else{$numfin=9;}
					for($xx = 1; $xx <= $numfin; $xx++)
					{
						if($numcontrol<=9){$numx=$xx;}
						else{$numx=$xx+($numcontrol-9);}
						if($numcontrol==$numx){echo"<a  onClick='saltocol(\"$numx\")'; style='color:#24D915;cursor:pointer;'> $numx </a>";}
						else {echo"<a onClick='saltocol(\"$numx\")'; style='color:#000000;cursor:pointer;'> $numx </a>";}
					}
					echo"			&nbsp;&nbsp;<a>$imagenforward</a>
									&nbsp;<a>$imagensforward</a>
								</td>
							</tr>
						</table>";
				?>
			</div> 
            <input type="hidden" name="numtop" id="numtop" value="<?php echo $_POST[numtop];?>" />
  		</form> 
	</body>
</html>