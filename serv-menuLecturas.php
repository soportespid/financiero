<?php
	require 'comun.inc';
	require 'funciones.inc'; 
	session_start();
	cargarcodigopag(@$_GET['codpag'], @$_SESSION['nivel']);
	header('Cache-control: private'); 
	date_default_timezone_set('America/Bogota')
?>

<!DOCTYPE >
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="es">
	
    <head>
	 	<meta http-equiv="Content-Type" content="text/html" charset="iso-8859-1"/>
        <meta http-equiv="X-UA-Compatible" content="IE=9"/>
		<title>:: IDEAL 10 - Servicios P&uacute;blicos</title>
        <link href="css/css2.css" rel="stylesheet" type="text/css" />
		<link href="css/css3.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="css/programas.js"></script>
		<script type="text/javascript" src="css/calendario.js"></script>
        
		<?php titlepag();?>
    </head>

    <body>
        <IFRAME src="alertas.php" name="alertas" id="alertas" style="display:none"></IFRAME>
        <span id="todastablas2"></span>
        <table>
            <tr><script>barra_imagenes("serv");</script><?php cuadro_titulos();?></tr>	 
            <tr><?php menu_desplegable("serv");?></tr>
       		<tr>
          		<td colspan="3" class="cinta">
                    <a class="mgbt"><img src="imagenes/add2.png"/></a>
                    <a class="mgbt"><img src="imagenes/guardad.png" title="Guardar" /></a>
                    <a class="mgbt"><img src="imagenes/buscad.png"/></a><a href="#" class="mgbt" onClick="mypop=window.open('serv-principal.php','','');mypop.focus();"><img src="imagenes/nv.png" title="Nueva Ventana"></a>      
                </td>
       	 	</tr>
        </table>

        <form name="form2" method="post" action="">
			<table class="inicio">
      			<tr>
        			<td class="titulos" colspan="2">.: Asignaci&oacute;n de servicios </td>
        			<td class="cerrar" style="width:7%;" ><a href="serv-principal.php">&nbsp;Cerrar</a></td>
      			</tr>
      			
				<tr>
                    <td>
                        <ol id="lista2">
							<li onClick="location.href='serv-registrarLectura.php'" style="cursor:pointer;">Lectura Manual Masiva</li>
							<li onClick="location.href='serv-lecturamedidores.php'" style="cursor:pointer;">Lectura Manual uno a uno</li>
                            <li onClick="location.href=''" style="cursor:pointer;">Lectura por archivo plano</li>
                        </ol>
                    </td>
				</tr>							
    		</table>
		</form>
    </body>

</html>
