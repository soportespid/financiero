<?php
	header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=utf8");

	require "comun.inc";
	require "funciones.inc";
	require "conversor.php";
	require "validaciones.inc";

	session_start();
	date_default_timezone_set("America/Bogota");

?>
<!DOCTYPE > 
<html lang="es">
	<head>
		<meta charset="utf-8"/>
		<meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1"/>
		<meta name="viewport" content="user-scalable=no">
		<title>:: IDEAL 10 - Contabilidad</title>
		<link href="favicon.ico" rel="shortcut icon"/>
        <link href="css/css2.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="css/programas.js"></script>
    </head>
	<style>
	
	</style>
    <body>
        <IFRAME src="alertas.php" name="alertas" id="alertas" style="display:none"></IFRAME>
        <span id="todastablas2"></span>
        <table>
            <tr><script>barra_imagenes("cont");</script><?php cuadro_titulos();?></tr>	 
            <tr><?php menu_desplegable("cont");?></tr>
        	<tr>
          		<td colspan="3" class="cinta">
                    <a class="mgbt"><img src="imagenes/add2.png"/></a>
                    <a class="mgbt"><img src="imagenes/guardad.png"/></a>
                    <a class="mgbt"><img src="imagenes/buscad.png"/></a>
					<a onClick="mypop=window.open('plan-agenda.php','','');mypop.focus()" class="mgbt"><img src="imagenes/agenda1.png" title="Agenda"/></a>
                    <a onClick="mypop=window.open('cont-principal.php','','');mypop.focus();" class="mgbt"><img src="imagenes/nv.png" title="Nueva ventana"></a>
                    <a onClick="mypop=window.open('<?php echo $url2; ?>','','');mypop.focus();" class="tooltip bottom mgbt"><img src="imagenes/duplicar_pantalla.png" title="Duplicar pesta&ntilde;a"></a>
                </td>
        	</tr>
        </table>
 		<form name="form2" method="post" action="">
    		<table class="inicio">
      			<tr>
        			<td class="titulos" colspan="2">.: Auxiliares Contabilidad </td>
        			<td class="cerrar" style="width:7%;" ><a href="cont-principal.php">&nbsp;Cerrar</a></td>
      			</tr>
					<td style="background-repeat:no-repeat; background-position:center;">
						<ol id="lista2">
							<li onClick="location.href='cont-auxiliarcuenta.php'" style="cursor:pointer;">Auxiliar por cuenta</li>
                            <li onClick="location.href='cont-auxiliartercero.php'" style="cursor:pointer;">Auxiliar por tercero</li>
							<li onClick="location.href='cont-auxiliarcuentatercero.php'" style="cursor:pointer;">Auxiliar por cuenta y tercero</li>
							<li onClick="location.href='cont-auxiliartercerocuenta.php'" style="cursor:pointer;">Auxiliar por tercero y cuenta</li>
                            <li onClick="location.href='cont-auxiliarmovimientos.php'" style="cursor:pointer;">Movimientos por periodos</li>                                                 
                            <li onClick="location.href='cont-auxiliarcuentas.php'" style="cursor:pointer;">Auxiliar de varias cuentas</li>      
							<li onClick="location.href='cont-balanceportercero.php'" style="cursor:pointer;">Balance de prueba por terceros</li>                                            
                        </ol>
				</td>                 
				</tr>							
    		</table>
		</form>
	</body>
</html>
