<?php
	header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=utf8");
	require "comun.inc";
	require "funciones.inc";
	require "funcionesSP.inc.php";
	session_start();
	$linkbd = conectar_v7();
	cargarcodigopag($_GET['codpag'],$_SESSION["nivel"]);
	date_default_timezone_set("America/Bogota");
?>
<!DOCTYPE >
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="es">
	<head>
		<meta http-equiv="Content-type" content="text/html; charset=utf8"/>
		<meta http-equiv="X-UA-Compatible" content="IE=9"/>
		<title>:: IDEAL 10 - Servicios P&uacute;blicos</title>
		<link href="css/css2.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
        <link href="css/css4.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<link href="css/cssSP.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<link href="css/style.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css"/>
		<script type="text/javascript" src="css/programas.js"></script>
        <script type="text/javascript" src="css/calendario.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
        <script type="text/javascript" src="jquery-1.11.0.min.js"></script>
		
        <script>
            $(window).load(function() {
                $('#cargando').hide();
            });
        </script>

		<script>
			function despliegamodalm(_valor,_tip,mensa,pregunta)
			{
				document.getElementById("bgventanamodalm").style.visibility=_valor;
				if(_valor == "hidden")
				{
					document.getElementById('ventanam').src = "";
				}
				else
				{
					switch(_tip)
					{
						case "1":	document.getElementById('ventanam').src="ventana-mensaje1.php?titulos="+mensa;
                        break;
						case "2":	document.getElementById('ventanam').src="ventana-mensaje3.php?titulos="+mensa;
                        break;
						case "3":	document.getElementById('ventanam').src="ventana-mensaje2.php?titulos="+mensa;
                        break;
						case "4":	document.getElementById('ventanam').src="ventana-consulta1.php?titulos="+mensa+"&idresp="+pregunta;
                        break;	
					}
				}
			}

			function respuestaconsulta(pregunta)
			{
				switch(pregunta)
				{
					case "1":	document.form2.oculto.value = '2';
								document.form2.submit();
								break;
				}
			}

            function generarFacturacion()
            {
				var Servicio = document.getElementById('servicio').value;
				var FechaInicial = document.getElementById('fecha_inicial').value;
				var FechaFinal = document.getElementById('fecha_final').value;
				var FechaLimite = document.getElementById('fecha_limite').value;
				var FechaImpresion = document.getElementById('fecha_impresion').value;
				var TipoMovimiento = document.getElementById('tipoMovimiento').value;

                if(Servicio != '' && FechaInicial != '' && FechaFinal != '' && FechaLimite != '' && FechaImpresion != '' && TipoMovimiento != '-1')
                {	 
                    document.form2.oculto.value = '3';
                    document.form2.submit();
                }
                else 
                {
                    alert("Falta informacion para poder Agregar");
                }
            }

			function funcionmensaje()
			{
				document.location.href = "serv-generarFacturacionPeriodo.php";
			}

			function guardar()
			{
				var validacion01 = document.getElementById('codban').value;
				var validacion02 = document.getElementById('nomban').value;
				var validacion03 = document.getElementById('servicio').value

				if (validacion01.trim() != '' && validacion02.trim() && validacion03 != '') 
				{
					despliegamodalm('visible','4','Esta Seguro de Guardar','1');
				}
				else 
				{
					despliegamodalm('visible','2','Falta informaci&oacute;n para crear la Anomalia');
				}
			}

			function cambiocheck()
			{
				if(document.getElementById('myonoffswitch').value=='S')
				{
					document.getElementById('myonoffswitch').value='N';
				}
				else
				{
					document.getElementById('myonoffswitch').value='S';
				}

				document.form2.submit();
			}

            function actualizar(){
                document.form2.submit();
            }

			function callprogress(vValor)
			{
				document.getElementById("getprogress").innerHTML = vValor;
				document.getElementById("getProgressBarFill").innerHTML = '<div class="ProgressBarFill" style="width: '+vValor+'%;"></div>';
				document.getElementById("titulog1").style.display='block';
				document.getElementById("progreso").style.display='block';
				document.getElementById("getProgressBarFill").style.display='block';
			}        
		</script>

		<?php titlepag();?>

	</head>

	<body>
		<IFRAME src="alertas.php" name="alertas" id="alertas" style="display:none"></IFRAME>
		<span id="todastablas2"></span>
		<table>
			<tr><script>barra_imagenes("serv");</script><?php cuadro_titulos();?></tr>
			<tr><?php menu_desplegable("serv");?></tr>
			<tr>
				<td colspan="3" class="cinta">
					<a href="" class="mgbt"><img src="imagenes/add2.png"/></a>

					<a onclick="guardar()" class="mgbt"><img src="imagenes/guarda.png" title="Guardar"/></a>

					<a href="" class="mgbt"><img src="imagenes/busca.png" title="Buscar"/></a>

					<a onclick="mypop=window.open('plan-agenda.php','','');mypop.focus()" class="mgbt"><img src="imagenes/agenda1.png" title="Agenda" /></a>

					<a onclick="mypop=window.open('serv-principal.php','','');mypop.focus();" class="mgbt"><img src="imagenes/nv.png" title="Nueva ventana"></a>

					<a onclick="mypop=window.open('<?php echo $url2; ?>','','');mypop.focus();" class="mgbt"><img src="imagenes/duplicar_pantalla.png" title="Duplicar pesta&ntilde;a"></a>

					<a href="serv-menuFacturacion01.php" class="mgbt"><img src="imagenes/iratras.png" title="Atr&aacute;s"></a>
				</td>
			</tr>
		</table>
		<div id="bgventanamodalm" class="bgventanamodalm">
			<div id="ventanamodalm" class="ventanamodalm">
				<IFRAME src="" name="ventanam" marginWidth=0 marginHeight=0 frameBorder=0 id="ventanam" frameSpacing=0 style=" width:700px; height:130px; top:200; overflow:hidden;"></IFRAME>
			</div>
		</div>
		<form name="form2" method="post" action="">

			<?php 
				if(@ $_POST['oculto'] == "")
				{
					$_POST['onoffswitch'] = "N";
					$_POST['codigo'] = selconsecutivo('srvcortes','numero_corte');
					
				}
			?>
            
			<table class="inicio grande">
				<tr>
					<td class="titulos" colspan="6">.: Generar Facturaci&oacute;n</td>
					<td class="cerrar" style="width:10%" onClick="location.href='serv-principal.php'">Cerrar</td>
				</tr>

                <tr>
                    <td class="tamano01" style="width:3cm;text-align:center;">Corte:</td>
					<td style="width:15%;">
						<input type="text" name="codigo" id="codigo" onKeyUp="return tabular(event,this)" value="<?php echo @ $_POST['codigo'];?>" style="width:98%;height:30px;text-align:center" readonly/>
					</td>

                    <td class="tamano01" style="width:3cm;">Servicios a Facturar</td>
                    <td style="width:20%">
                        <select name="servicio" id="servicio" style="width:98%;height:30px;" class="centrarSelect">
                            <option class="aumentarTamaño" value="TODOS">TODOS LOS SERVICIOS</option>
						</select>
                    </td>
                </tr>

                <tr>
                    <td class="tamano01" style="width:3cm;">Fecha Inicial de Corte:</td>
					<td style="width:15%;">
                        <input type="text" name="fecha_inicial" id="fecha_inicial" value="<?php echo @ $_POST['fecha_inicial']?>" maxlength="10" onKeyUp="return tabular(event,this)"  id="fc_1198971545" onKeyDown="mascara(this,'/',patron,true)" title="DD/MM/YYYY" style="height:30px;text-align:center;width:85%;" onchange="" readonly/>
                            <a href="#" onClick="displayCalendarFor('fecha_inicial');" title="Calendario">
                                <img src="imagenes/calendario04.png" style="width:25px;">
                            </a>
                    </td>

                    <td class="tamano01" style="width: 15%;">Fecha Final de Corte:</td>
					<td>
                        <input type="text" name="fecha_final" id="fecha_final" value="<?php echo @ $_POST['fecha_final']?>" maxlength="10" onKeyUp="return tabular(event,this)"  id="fc_1198971545" onKeyDown="mascara(this,'/',patron,true)" title="DD/MM/YYYY" style="height:30px;text-align:center;width:85%;" onchange="" readonly/>
                            <a href="#" onClick="displayCalendarFor('fecha_final');" title="Calendario">
                                <img src="imagenes/calendario04.png" style="width:25px;">
                            </a>
                    </td>
                </tr>
                <tr>
                    <td class="tamano01" style="width:10%;">Fecha Limite de Pago:</td>
					<td>
                        <input type="text" name="fecha_limite" id="fecha_limite" value="<?php echo @ $_POST['fecha_limite']?>" maxlength="10" onKeyUp="return tabular(event,this)"  id="fc_1198971545" onKeyDown="mascara(this,'/',patron,true)" title="DD/MM/YYYY" style="height:30px;text-align:center;width:85%;" onchange="" readonly/>
                            <a href="#" onClick="displayCalendarFor('fecha_limite');" title="Calendario">
                                <img src="imagenes/calendario04.png" style="width:25px;">
                            </a>
                    </td>
					<td class="tamano01" style="width:15%;">Fecha de Impresi&oacute;n:</td>
					<td>
                        <input type="text" name="fecha_impresion" id="fecha_impresion" value="<?php echo @ $_POST['fecha_impresion']?>" maxlength="10" onKeyUp="return tabular(event,this)"  id="fc_1198971545" onKeyDown="mascara(this,'/',patron,true)" title="DD/MM/YYYY" style="height:30px;text-align:center;width:85%;" onchange="" readonly/>
                            <a href="#" onClick="displayCalendarFor('fecha_impresion');" title="Calendario">
                                <img src="imagenes/calendario04.png" style="width:25px;">
                            </a>
                    </td>

                </tr>
				<tr>
					<td  class="tamano01">Tipo de Movimiento</td>
                    <td colspan="1">
                        <select  name="tipoMovimiento" id="tipoMovimiento" style="width:100%;height:30px;" class="centrarSelect">
							<option class='aumentarTamaño' value="-1">SELECCIONE UN MOVIMIENTO</option>
							<?php
								$sqlr="SELECT * FROM srvtipo_movimiento WHERE codigo = '01' AND tipo_movimiento = '1' ";
								$resp = mysqli_query($linkbd,$sqlr);
								while ($row =mysqli_fetch_row($resp))
								{
									if(@ $_POST['tipoMovimiento']==$row[0])
									{
										echo "<option class='aumentarTamaño' value='$row[1]$row[0]' SELECTED>$row[1]$row[0] - $row[2]</option>";
									}
									else{echo "<option class='aumentarTamaño' value='$row[1]$row[0]'>$row[1]$row[0] - $row[2]</option>";}
								}
							?>
						</select>
                    </td>

					<td  class="tamano01">Intereses moratorios:</td>
					<td>
						<div class="onoffswitch">
							<input type="checkbox" name="onoffswitch" class="onoffswitch-checkbox" id="myonoffswitch" value="<?php echo @ $_POST['onoffswitch'];?>" <?php if(@ $_POST['onoffswitch']=='S'){echo "checked";}?> onChange="cambiocheck();"/>
							<label class="onoffswitch-label" for="myonoffswitch">
								<span class="onoffswitch-inner"></span>
								<span class="onoffswitch-switch"></span>
							</label>
						</div>
					</td>
						

					<td>
					    <input type="button" name="agregar" id="agregar" style="height:30px;" value="Generar Facturaci&oacute;n" onClick="generarFacturacion();" >
                    </td>

					<?php
					echo"
						<td>
							<div id='titulog1' style='display:none; float:left'></div>
							<div id='progreso' class='ProgressBar' style='display:none; float:left'>
								<div class='ProgressBarText'><span id='getprogress'></span>&nbsp;% </div>
								<div id='getProgressBarFill'></div>
							</div>
						</td>";
					?>
				</tr>
			</table>
        
			<input type="hidden" name="oculto" id="oculto" value="1"/>
            
			<?php 

				$corte = $_POST['codigo'];
				$id_cliente = array();
				$numero_facturacion = array();
				$tipoMovimiento = array();
				$fechaMovimiento = array();
				$id_servicio = array();
				$id_tipo_cobro = array();
				$credito = array();
				$debito = array();
				$saldo = array();
				$año = '2022';

				//srvcortes_detalles
				$id_cliente2 = array();
				$id_corte = array();
				$numero_facturacion2 = array();
				$estado_pago = array();

				//Variables publicas
				$valortotalLectura = 0;
				$cargoFijo = 0;
				$porcentaje = 0;
				$totalSubsidio = 0;
				$contFacturacion = 0;

				//id's de otros cobros, subsidios, exoneraciones y contribución
				$idOtrosCobros    = array();
				$idSubsidios      = array();
				$idExoneraciones  = array();
				$idContribuciones = array();
				

                if(@$_POST['oculto'] == "3")
				{	
					if (@$_POST['onoffswitch'] != 'S') {
						$cobro_mora = 'N';
					}
					else {
						$cobro_mora = 'S';
					}

					$validacionCorteActivo = consultaCortesActivos();

					if($validacionCorteActivo == false)
					{
						$numeroTipoMovimiento = $_POST['tipoMovimiento'];
						$contFacturacion = selconsecutivo('srvcortes_detalle','numero_facturacion');

						$sql = "SELECT MAX(version) FROM srvcargo_fijo";
						$row = mysqli_fetch_row(mysqli_query($linkbd, $sql));
						
						$versionTarifas = $row[0];

						$sqlMAXCorte = "SELECT MAX(numero_facturacion) FROM srvcortes_detalle";
						$respMAXCorte = mysqli_query($linkbd,$sqlMAXCorte);
						$rowMAXCorte = mysqli_fetch_row($respMAXCorte);

						$facturaInicial = $rowMAXCorte[0] + 1;

						$sqlCliente = "SELECT id, id_tercero, id_estrato FROM srvclientes WHERE estado = 'S' ORDER BY id_barrio ASC, cod_usuario ASC";
						$respCliente = mysqli_query($linkbd,$sqlCliente);
						
						$totalClientes = mysqli_num_rows($respCliente); //total Clientes
						$c = 0;

						while($rowCliente = mysqli_fetch_row($respCliente))
						{			
							$query = "SELECT id_servicio FROM srvasignacion_servicio WHERE id_clientes = '$rowCliente[0]' AND estado = 'S'";
							$resp = mysqli_query($linkbd,$query);
							$validaServicio = mysqli_num_rows($resp);
							$row = mysqli_fetch_row($resp);

							$c ++;	//contador;

							if ($validaServicio > 0) {
								/*
								Tipo de cobro
								1. CARGO FIJO
								2. TARIFA SIN MEDIDOR
								3. TARIFA CON MEDIDOR
								4. OTROS COBROS
								5. SUBSIDIOS
								6. EXONERACIONES
								7. CONTRIBUCIÓN
								8. Saldo (Cargo fijo, tarifas, otros cobros, contribuciones -  subsidios y exoneraciones)
								*/

								//srvcortes_detalles

								$id_cliente2[] 		  = $rowCliente[0];
								$id_corte[] 		  = $_POST['codigo'];
								$numero_facturacion2[] = $contFacturacion;
								$estado_pago[]		  = 'S';


								$sqlAsignacionServicio = "SELECT id_servicio, tarifa_medidor FROM srvasignacion_servicio WHERE id_clientes = '$rowCliente[0]' AND estado = 'S' ORDER BY id_servicio";
								$respAsignacionServicio = mysqli_query($linkbd,$sqlAsignacionServicio);

								while($rowAsignacionServicio = mysqli_fetch_row($respAsignacionServicio))
								{
									//nombre servicio
									$sqlServicio = "SELECT nombre FROM srvservicios WHERE id = '$rowAsignacionServicio[0]' ";
									$respServicio = mysqli_query($linkbd,$sqlServicio);
									$rowServicio = mysqli_fetch_row($respServicio);

									//Cargo fijo de cada servicio
									$sqlCargoFijo = "SELECT costo_unidad FROM srvcargo_fijo WHERE id_servicio = '$rowAsignacionServicio[0]' AND id_estrato = '$rowCliente[2]' AND version = $versionTarifas ";
									$respCargoFijo = mysqli_query($linkbd,$sqlCargoFijo);
									$rowCargoFijo = mysqli_fetch_row($respCargoFijo);

									$cargoFijo = $rowCargoFijo[0];

									//Tiene datos

									$id_cliente[] 		  = $rowCliente[0];
									$numero_facturacion[] = $contFacturacion;
									$tipoMovimiento[]	  = $_POST['tipoMovimiento'];
									$id_servicio[] 		  = $rowAsignacionServicio[0];	
									$id_tipo_cobro[]	  = 1;
									$credito[]		   	  = $cargoFijo;
									$debito[]		   	  = 0;
									$saldo[] 			  = 0;
									
									
									$sqlCostoEstandar = "SELECT costo_unidad FROM srvcostos_estandar WHERE id_servicio = '$rowAsignacionServicio[0]' AND id_estrato = '$rowCliente[2]' AND version = $versionTarifas";
									$respCostoEstandar = mysqli_query($linkbd,$sqlCostoEstandar);
									$rowCostoEstandar = mysqli_fetch_row($respCostoEstandar);
									
									$valortotalLectura = $rowCostoEstandar[0];						

									//Tiene datos
									$id_cliente[] 		  = $rowCliente[0];
									$numero_facturacion[] = $contFacturacion;
									$tipoMovimiento[] 	  = $_POST['tipoMovimiento'];
									$id_servicio[] 		  = $rowAsignacionServicio[0];	
									$id_tipo_cobro[]	  = 2;
									$credito[]		   	  = $valortotalLectura;
									$debito[]		   	  = 0;
									$saldo[]			  = 0;
										

									//Otros Cobros

									$sqlAsignacionOtroCobro = "SELECT id, id_otro_cobro, financiado_a_cuotas, valor_pago, numero_cuotas FROM srvasignacion_otroscobros WHERE id_cliente = '$rowCliente[0]' AND estado = 'S' ";
									$respAsignacionOtroCobro = mysqli_query($linkbd,$sqlAsignacionOtroCobro);
									$rowAsignacionOtroCobro = mysqli_fetch_row($respAsignacionOtroCobro);
				
									if($rowAsignacionOtroCobro[0] != '')
									{
										if($rowAsignacionOtroCobro[2] == 'S')
										{				
											$valorPagoOtroCobroCorteActual = $rowAsignacionOtroCobro[3] / $rowAsignacionOtroCobro[4];

											$id_cliente[]  		  = $rowCliente[0];
											$numero_facturacion[] = $contFacturacion;
											$tipoMovimiento[]  	  = $_POST['tipoMovimiento'];
											$id_servicio[] 		  = $rowAsignacionServicio[0];	
											$id_tipo_cobro[]	  = 4;
											$credito[]		   	  = $valorPagoOtroCobroCorteActual;
											$debito[]		   	  = 0;
											$saldo[]			  = 0;

											$valorNuevo = $rowAsignacionOtroCobro[3] - $valorPagoOtroCobroCorteActual;
											$cuotas = $rowAsignacionOtroCobro[4] - 1;

											if($cuotas != 0)
											{
												$sql = "UPDATE srvasignacion_otroscobros SET valor_pago = $valorNuevo, numero_cuotas = $cuotas WHERE id = $rowAsignacionOtroCobro[0] ";
												mysqli_query($linkbd,$sql);
											}
											else
											{
												$sql = "UPDATE srvasignacion_otroscobros SET valor_pago = $valorNuevo, numero_cuotas = $cuotas, estado = 'F' WHERE id = $rowAsignacionOtroCobro[0] ";
												mysqli_query($linkbd,$sql);
											}
										}
										else
										{
											$id_cliente[]  		  = $rowCliente[0];
											$numero_facturacion[] = $contFacturacion;
											$tipoMovimiento[]  	  = $_POST['tipoMovimiento'];
											$id_servicio[] 		  = $rowAsignacionServicio[0];	
											$id_tipo_cobro[]	  = 4;
											$credito[]		   	  = $rowAsignacionOtroCobro[3];
											$debito[]		   	  = 0;
											$saldo[]			  = 0;

											$sql = "UPDATE srvasignacion_otroscobros SET estado = 'F' WHERE id = $rowAsignacionOtroCobro[0] ";
											mysqli_query($linkbd,$sql);
										}
									}
									else
									{
										//NO Tiene datos
									}
									
									//Exoneraciones

									$sqlExoneraciones = "SELECT * FROM srvexoneraciones WHERE id_servicio = '$rowAsignacionServicio[0]' AND estado = 'S' ";
									$respExoneraciones = mysqli_query($linkbd,$sqlExoneraciones);
									$rowExoneraciones = mysqli_fetch_row($respExoneraciones);
									if(isset($rowExoneraciones[0]))
									{
										$sqlAsignacionExoneracion = "SELECT id,id_exoneracion FROM srvasignacion_exoneracion WHERE id_cliente ='$rowCliente[0]' AND id_exoneracion = '$rowExoneraciones[0]' AND estado = 'S' ";
										$respAsignacionExoneracion = mysqli_query($linkbd,$sqlAsignacionExoneracion);
										$rowAsignacionExoneracion = mysqli_fetch_row($respAsignacionExoneracion);

										if(isset($rowAsignacionExoneracion[0]))
										{
											//Se guardan los id para hacer la contabilización más adelante
											$idExoneraciones[] = $rowAsignacionExoneracion[1];

											$totalExoneracion = $valortotalLectura + $cargoFijo;

											$ExoneracionTotal = $valortotalLectura + $cargoFijo;							

											//Tiene datos
											$id_cliente[] 		  = $rowCliente[0];
											$numero_facturacion[] = $contFacturacion;
											$tipoMovimiento[] 	  = $_POST['tipoMovimiento'];
											$id_servicio[] 		  = $rowAsignacionServicio[0];	
											$id_tipo_cobro[]	  = 6;
											$credito[]		   	  = 0;
											$debito[]		   	  = $ExoneracionTotal;
											$saldo[]			  = 0;

											$ExoneracionTotal = 0;
										}
										else
										{
											//NO Tiene datos
										}
									}
									else
									{
										//NO Tiene datos
									}

									//Interes Moratorio
									if($cobro_mora == 'S') {

										$sqlCobroMora = "SELECT SUM(valor_mora) FROM srvfactura_valor_mora WHERE id_cliente = $rowCliente[0] AND id_servicio = $rowAsignacionServicio[0]";
										$resCobraMora = mysqli_query($linkbd,$sqlCobroMora);
										$rowCobroMora = mysqli_fetch_row($resCobraMora);

										if($rowCobroMora != NULL) {
											
											$id_cliente[] 		  = $rowCliente[0];
											$numero_facturacion[] = $contFacturacion;
											$tipoMovimiento[] 	  = $_POST['tipoMovimiento'];
											$id_servicio[] 		  = $rowAsignacionServicio[0];	
											$id_tipo_cobro[]	  = 11;
											$credito[]		   	  = $rowCobroMora[0];
											$debito[]		   	  = 0;
											$saldo[]			  = 0;
										}
										else {
											//Cliente no tiene mora
										}
									}
									else {
										//No se cobra mora
									}

									if($corte == '1')//cargar saldos iniciales al kardex
									{
										$query = "SELECT * FROM srvsaldos_iniciales WHERE id_cliente = $rowCliente[0] AND estado = 'S'";
										$res = mysqli_query($linkbd,$query);
										$row = mysqli_fetch_assoc($res);

										if($row['id'] != '')
										{
											$id_cliente[] 		  = $rowCliente[0];
											$numero_facturacion[] = $contFacturacion;
											$tipoMovimiento[] 	  = $_POST['tipoMovimiento'];
											$id_servicio[] 		  = $rowAsignacionServicio[0];	
											$id_tipo_cobro[]	  = 10;
											$credito[]		   	  = $row['saldo'];
											$debito[]		   	  = 0;

											$query = "UPDATE srvsaldos_iniciales SET estado = 'N' WHERE id = $row[id]";
											mysqli_query($linkbd,$query);
										}
										
									}


									//Consultar estado de factura a anterior cliente
									if($corte != '1')
									{
										$corteAnterior = $corte - 1;
										$sqlConsultaFacturaAnterior = "SELECT estado_pago FROM srvcortes_detalle WHERE id_cliente = $rowCliente[0] AND id_corte = $corteAnterior";
										//echo $sqlConsultaFacturaAnterior;
										
										$resConsultaFacturaAnterior = mysqli_query($linkbd,$sqlConsultaFacturaAnterior);
										$rowConsultaFacturaAnterior = mysqli_fetch_row($resConsultaFacturaAnterior);
									}

									$saldoAnterior = 0;

									switch ($rowConsultaFacturaAnterior[0]) 
									{
										case 'V':
											$sql = "SELECT SUM(credito), SUM(debito) FROM srvdetalles_facturacion WHERE corte = $corteAnterior AND id_cliente = $rowCliente[0] AND tipo_movimiento = '101' AND id_servicio = $rowAsignacionServicio[0]";
											$res = mysqli_query($linkbd,$sql);
											$row = mysqli_fetch_row($res);

											$saldoAnterior = $row[0] - $row[1];
										break;

										default:
											$saldoAnterior = 0;
										break;
									}

									$id_cliente[] 		  = $rowCliente[0];
									$numero_facturacion[] = $contFacturacion;
									$tipoMovimiento[] 	  = $_POST['tipoMovimiento'];
									$id_servicio[] 		  = $rowAsignacionServicio[0];	
									$id_tipo_cobro[]	  = 8;
									$credito[]		   	  = $saldoAnterior;
									$debito[]		   	  = 0;

									//Consulta si el cliente tiene acuerdos de pagos activos
									$sqlAcuerdoPago = "SELECT * FROM srvacuerdos_pago WHERE id_cliente = '$rowCliente[0]' AND estado = 'S'";
									$resAcuerdoPago = mysqli_query($linkbd,$sqlAcuerdoPago);
									$rowAcuerdoPago = mysqli_fetch_assoc($resAcuerdoPago);

									if(isset($rowAcuerdoPago['id']))
									{
										$cuotaActual = $rowAcuerdoPago['cuotas_inicial'] - $rowAcuerdoPago['cuotas_pagadas'];
										$valorActual = $rowAcuerdoPago['valor_acuerdo'] - $rowAcuerdoPago['valor_pagado'];

										$valorAcuerdoPago = $valorActual / $cuotaActual;

										$id_cliente[] 		  = $rowCliente[0];
										$numero_facturacion[] = $contFacturacion;
										$tipoMovimiento[] 	  = $_POST['tipoMovimiento'];
										$id_servicio[] 		  = $rowAsignacionServicio[0];	
										$id_tipo_cobro[]	  = 9;
										$credito[]		   	  = round($valorAcuerdoPago,2);
										$debito[]		   	  = 0;

										$cuotaPagada = $rowAcuerdoPago['cuotas_pagadas'] + 1;
										$valorPagado = $rowAcuerdoPago['valor_pagado'] + $valorAcuerdoPago;

										if($rowAcuerdoPago['cuotas_inicial'] == $cuotaPagada)
										{
											$sqlr = "UPDATE srvacuerdos_pago SET cuotas_pagadas = '$cuotaPagada', valor_pagado = '$valorPagado', estado = 'F' WHERE id = '$rowAcuerdoPago[id]' ";
											mysqli_query($linkbd,$sqlr);
										}
										else
										{
											$sqlr = "UPDATE srvacuerdos_pago SET cuotas_pagadas = '$cuotaPagada', valor_pagado = '$valorPagado' WHERE id = '$rowAcuerdoPago[id]' ";
											mysqli_query($linkbd,$sqlr);
										}

										$sqlAcuerdosPagoDetalles = "INSERT INTO srvacuerdos_pago_det (id_acuerdo, fecha, cuota, valor) VALUES('$rowAcuerdoPago[id]', '$fechaImpresion', '$cuotaPagada', '$valorPagado')";
										mysqli_query($linkbd,$sqlAcuerdosPagoDetalles);
									}

									$saldoTotal = 0;
									$cargoFijo = 0;
									$valortotalLectura = 0;
									$otroCobro = 0;
									$totalContribucion = 0;
									$totalSubsidio = 0;
									$ExoneracionTotal = 0;
									
								}
								$contFacturacion++;
							}

							$porcentaje = $c * 100 / $totalClientes;

							echo"
							<script>
								progres='".round($porcentaje)."';callprogress(progres);
								
							</script>";
							flush();
							ob_flush();
							usleep(5);

							$enumeradorFacturas[] = $facturaInicial;
							$facturaInicial++;
						}
						
						preg_match("/([0-9]{2})\/([0-9]{2})\/([0-9]{4})/", $_POST['fecha_inicial'],$f);
						$fechaInicial="$f[3]-$f[2]-$f[1]";

						preg_match("/([0-9]{2})\/([0-9]{2})\/([0-9]{4})/", $_POST['fecha_final'],$f);
						$fechaFinal="$f[3]-$f[2]-$f[1]";

						preg_match("/([0-9]{2})\/([0-9]{2})\/([0-9]{4})/", $_POST['fecha_limite'],$f);
						$fechaLimite="$f[3]-$f[2]-$f[1]";

						preg_match("/([0-9]{2})\/([0-9]{2})\/([0-9]{4})/", $_POST['fecha_impresion'],$f);
						$fechaImpresion="$f[3]-$f[2]-$f[1]";
						$vigencia = $f[3];

						$sqlCorte = "INSERT INTO srvcortes (numero_corte, fecha_inicial, fecha_final, fecha_limite_pago, fecha_impresion, servicio, estado) VALUES ('".$_POST['codigo']."', '$fechaInicial', '$fechaFinal', '$fechaLimite', '$fechaImpresion', '".$_POST['servicio']."', 'S')";
						mysqli_query($linkbd,$sqlCorte);
						
						for($x=0; $x < count($id_cliente2); $x++)
						{
							$sqlCortesDetalles = "INSERT INTO srvcortes_detalle (id_cliente, id_corte, numero_facturacion, estado_pago) VALUES ('$id_cliente2[$x]', '$id_corte[$x]', '$numero_facturacion2[$x]', '$estado_pago[$x]') ";
							mysqli_query($linkbd,$sqlCortesDetalles);
						}
						

						for($x=0; $x < count($id_cliente); $x++)
						{
							$sqlDetalles_Facturacion = "INSERT INTO srvdetalles_facturacion (corte, id_cliente, numero_facturacion, tipo_movimiento, fecha_movimiento, id_servicio, id_tipo_cobro, credito, debito, saldo, estado) VALUES ('$corte', '$id_cliente[$x]', '$numero_facturacion[$x]', '$tipoMovimiento[$x]', '$fechaImpresion', '$id_servicio[$x]', '$id_tipo_cobro[$x]', '$credito[$x]', '$debito[$x]', '$saldo[$x]','S') ";
							mysqli_query($linkbd,$sqlDetalles_Facturacion);
						}

						$codigoComprobante = '29';

						//Recaudo anticipo

						foreach ($id_cliente2 as $i => $cliente) {
							
							// $dineroDisponible = $idRecaudoAnticipo =  $valorTotalAnticipado = $anticipoFacturado = $valorFacturaActual = $contador = 0;

							$sqlAnticipo = "SELECT id, valor_total, anticipo_facturado FROM srvrecaudo_anticipo WHERE id_cliente = $cliente AND estado = 'S' ORDER BY id ASC LIMIT 1";
							$rowAnticipo = mysqli_fetch_assoc(mysqli_query($linkbd, $sqlAnticipo));

							$disponible = $rowAnticipo["valor_total"] - $rowAnticipo["anticipo_facturado"];

							$querySum = "SELECT SUM(credito) AS credito FROM srvdetalles_facturacion WHERE numero_facturacion = '$numero_facturacion2[$i]' AND tipo_movimiento = '101'";
							$rowSum = mysqli_fetch_assoc(mysqli_query($linkbd, $querySum));									
							$valorFacturaActual = $rowSum['credito'];

							if ($disponible > $valorFacturaActual) {
								
								$sqlDetFact = "INSERT INTO srvdetalles_facturacion (corte, id_cliente, numero_facturacion, tipo_movimiento, fecha_movimiento, id_servicio, id_tipo_cobro, credito, debito, saldo, estado) VALUES ('$corte', $cliente, '$numero_facturacion2[$i]', '204', '$fechaImpresion', 1, 1, 0, $valorFacturaActual, 0,'S') ";
								mysqli_query($linkbd, $sqlDetFact);

								$updateAnt = "UPDATE srvrecaudo_anticipo SET anticipo_facturado =+ $valorFacturaActual WHERE id = $rowAnticipo[id]";
								mysqli_query($linkbd, $updateAnt);

								$insertAntDet = "INSERT INTO srvrecaudo_anticipo_det (id_anticipo, fecha, tipo_movimiento, valor_anticipo) VALUES ($rowAnticipo[id], '$fechaImpresion', '101', $valorFacturaActual)";
								mysqli_query($linkbd, $insertAntDet);

								$updateStatus = "UPDATE srvcortes_detalle SET estado_pago = 'P' WHERE numero_facturacion = $numero_facturacion2[$i]";
								mysqli_query($linkbd, $updateStatus);
							}
							else {
								$sqlDetFact = "INSERT INTO srvdetalles_facturacion (corte, id_cliente, numero_facturacion, tipo_movimiento, fecha_movimiento, id_servicio, id_tipo_cobro, credito, debito, saldo, estado) VALUES ('$corte', $cliente, '$numero_facturacion2[$i]', '204', '$fechaImpresion', 1, 1, 0, $disponible, 0,'S') ";
								mysqli_query($linkbd, $sqlDetFact);

								$updateAnt = "UPDATE srvrecaudo_anticipo SET anticipo_facturado =+ $disponible, estado = 'F' WHERE id = $rowAnticipo[id]";
								mysqli_query($linkbd, $updateAnt);

								$insertAntDet = "INSERT INTO srvrecaudo_anticipo_det (id_anticipo, fecha, tipo_movimiento, valor_anticipo) VALUES ($rowAnticipo[id], '$fechaImpresion', '101', $disponible)";
								mysqli_query($linkbd, $insertAntDet);

								if ($disponible == $valorFacturaActual) {
									$updateStatus = "UPDATE srvcortes_detalle SET estado_pago = 'P' WHERE numero_facturacion = $numero_facturacion2[$i]";
									mysqli_query($linkbd, $updateStatus);
								}
							}
						}	
					}
					else
					{
						echo"<script>despliegamodalm('visible','2','Hay un corte ACTIVO, no puede generar más cortes.');</script>";
					}
				}

				$totalCargoFijoAndLectura = $valortotalLectura + $cargoFijo;

				if(@ $_POST['oculto'] == "2")
				{
	
				}
			?>
			
		</form>
		<div id="bgventanamodal2">
			<div id="ventanamodal2">
				<IFRAME  src="" name="buster" marginWidth=0 marginHeight=0 frameBorder=0 id="ventana2" frameSpacing=0 style="left:500px; width:900px; height:500px; top:200;"></IFRAME>
			</div>
		</div>
	</body>
</html>
