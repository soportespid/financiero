<?php
	
    header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=utf8");
	require 'comun.inc';
	require 'funciones.inc';
	session_start();
    if(empty($_SESSION)){
        header("location: index.php");
    }
	$linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");
	cargarcodigopag($_GET['codpag'],$_SESSION["nivel"]);
	date_default_timezone_set("America/Bogota");
?>
<!DOCTYPE >
<html lang=es>
	<head>
    <meta charset="utf-8"/>
		<meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1"/>
		<meta name="viewport" content="user-scalable=no">
		<title>:: IDEAL 10 - Almacén</title>
		<link href="favicon.ico" rel="shortcut icon"/>
		<link href="css/css2z.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
		<link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
		<link href="css/css4.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
        <link href="css/tabs.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css"/>
        <link rel="stylesheet" href="sweetalert2/dist/sweetalert2.min.css">
        <script type="text/javascript" src="css/programas.js"></script>
        <script src="sweetalert2/dist/sweetalert2.min.js"></script>
		<script src="vue/vue.min.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script src="css/programas.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script src="css/calendario.js?<?php echo date('d_m_Y_h_i_s');?>"></script>

        <style>
            .checkbox-wrapper-31:hover .check {
                stroke-dashoffset: 0;
            }
        
            .checkbox-wrapper-31 {
                position: relative;
                display: inline-block;
                width: 40px;
                height: 40px;
            }
            .checkbox-wrapper-31 .background {
                fill: #ccc;
                transition: ease all 0.6s;
                -webkit-transition: ease all 0.6s;
            }
            .checkbox-wrapper-31 .stroke {
                fill: none;
                stroke: #fff;
                stroke-miterlimit: 10;
                stroke-width: 2px;
                stroke-dashoffset: 100;
                stroke-dasharray: 100;
                transition: ease all 0.6s;
                -webkit-transition: ease all 0.6s;
            }
            .checkbox-wrapper-31 .check {
                fill: none;
                stroke: #fff;
                stroke-linecap: round;
                stroke-linejoin: round;
                stroke-width: 2px;
                stroke-dashoffset: 22;
                stroke-dasharray: 22;
                transition: ease all 0.6s;
                -webkit-transition: ease all 0.6s;
            }
            .checkbox-wrapper-31 input[type=checkbox] {
                position: absolute;
                width: 100%;
                height: 100%;
                left: 0;
                top: 0;
                margin: 0;
                opacity: 0;
                -appearance: none;
            }
            .checkbox-wrapper-31 input[type=checkbox]:hover {
                cursor: pointer;
            }
            .checkbox-wrapper-31 input[type=checkbox]:checked + svg .background {
                fill: #6cbe45;
            }
            .checkbox-wrapper-31 input[type=checkbox]:checked + svg .stroke {
                stroke-dashoffset: 0;
            }
            .checkbox-wrapper-31 input[type=checkbox]:checked + svg .check {
                stroke-dashoffset: 0;
            }
        </style>
	</head>
	<body>
		<header>
			<table>
				<tr><script>barra_imagenes("inve");</script><?php cuadro_titulos();?></tr>
			</table>
		</header>

		<form name="form2" method="post" action="">
			<section id="myapp" v-cloak >
                <div id="cargando" v-show="isLoading" class="loading" style="z-index: 10000;">
                    <span>Cargando...</span>
                </div>
				<nav>
					<table>
						<tr><?php menu_desplegable("inve");?></tr>
					</table>
                    <div class="bg-white group-btn p-1" id="newNavStyle">
                        <button type="button" class="btn btn-white btn-primary-hover d-flex justify-between align-items-center" @click="location.href='inve-gestionEntradaAjuste.php'">
                            <span>Nuevo</span>
                            <svg viewBox="0 -960 960 960" ><path d="M440-440H200v-80h240v-240h80v240h240v80H520v240h-80v-240Z"/></svg>
                        </button>
                        <button type="button" class="btn btn-white btn-primary-hover d-flex justify-between align-items-center" @click="saveCreate()">
                            <span>Guardar</span>
                            <svg viewBox="0 -960 960 960" ><path d="M840-680v480q0 33-23.5 56.5T760-120H200q-33 0-56.5-23.5T120-200v-560q0-33 23.5-56.5T200-840h480l160 160Zm-80 34L646-760H200v560h560v-446ZM480-240q50 0 85-35t35-85q0-50-35-85t-85-35q-50 0-85 35t-35 85q0 50 35 85t85 35ZM240-560h360v-160H240v160Zm-40-86v446-560 114Z"/></svg>
                        </button>
                        <button type="button" class="btn btn-white btn-primary-hover d-flex justify-between align-items-center" @click="location.href='inve-buscarGestionEntradaAjuste.php'">
                            <span>Buscar</span>
                            <svg class="fill-black group-hover:fill-white w-5 h-5" viewBox="0 -960 960 960" ><path d="M784-120 532-372q-30 24-69 38t-83 14q-109 0-184.5-75.5T120-580q0-109 75.5-184.5T380-840q109 0 184.5 75.5T640-580q0 44-14 83t-38 69l252 252-56 56ZM380-400q75 0 127.5-52.5T560-580q0-75-52.5-127.5T380-760q-75 0-127.5 52.5T200-580q0 75 52.5 127.5T380-400Z"/></svg>
                        </button>
                        <button type="button" class="btn btn-white btn-primary-hover d-flex justify-between align-items-center" @click="mypop=window.open('inve-principal.php','',''); mypop.focus();">
                            <span>Nueva ventana</span>
                            <svg class="fill-black group-hover:fill-white w-5 h-5" viewBox="0 -960 960 960" ><path d="M320-120v-80h80v-80H160q-33 0-56.5-23.5T80-360v-400q0-33 23.5-56.5T160-840h640q33 0 56.5 23.5T880-760v400q0 33-23.5 56.5T800-280H560v80h80v80H320ZM160-360h640v-400H160v400Zm0 0v-400 400Z"/></svg>
                        </button>
                        <button type="button" class="btn btn-danger btn-danger-hover d-flex justify-between align-items-center" @click="printPDF()">
                            <span>Exportar PDF</span>
                            <svg viewBox="0 0 512 512"><path d="M64 464l48 0 0 48-48 0c-35.3 0-64-28.7-64-64L0 64C0 28.7 28.7 0 64 0L229.5 0c17 0 33.3 6.7 45.3 18.7l90.5 90.5c12 12 18.7 28.3 18.7 45.3L384 304l-48 0 0-144-80 0c-17.7 0-32-14.3-32-32l0-80L64 48c-8.8 0-16 7.2-16 16l0 384c0 8.8 7.2 16 16 16zM176 352l32 0c30.9 0 56 25.1 56 56s-25.1 56-56 56l-16 0 0 32c0 8.8-7.2 16-16 16s-16-7.2-16-16l0-48 0-80c0-8.8 7.2-16 16-16zm32 80c13.3 0 24-10.7 24-24s-10.7-24-24-24l-16 0 0 48 16 0zm96-80l32 0c26.5 0 48 21.5 48 48l0 64c0 26.5-21.5 48-48 48l-32 0c-8.8 0-16-7.2-16-16l0-128c0-8.8 7.2-16 16-16zm32 128c8.8 0 16-7.2 16-16l0-64c0-8.8-7.2-16-16-16l-16 0 0 96 16 0zm80-112c0-8.8 7.2-16 16-16l48 0c8.8 0 16 7.2 16 16s-7.2 16-16 16l-32 0 0 32 32 0c8.8 0 16 7.2 16 16s-7.2 16-16 16l-32 0 0 48c0 8.8-7.2 16-16 16s-16-7.2-16-16l0-64 0-64z"/></svg>
                        </button>
                        <button type="button" class="btn btn-success d-flex justify-between align-items-center" @click="window.location.href='inve-buscarGestionEntradaAjuste'">
                            <span>Atrás</span>
                            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 -960 960 960"><path d="M760-200v-160q0-50-35-85t-85-35H273l144 144-57 56-240-240 240-240 57 56-144 144h367q83 0 141.5 58.5T840-360v160h-80Z"/></svg>
                        </button>
                    </div>
				</nav>
				<article>
                    <table class="inicio">
                        <tbody>
                            <tr>
                                <td class="titulos" colspan="9">.: Entrada por ajuste</td>
                            </tr>
                            <tr>
                                <td style="width:10%;">.: Consecutivo:</td>
                                <td><input type="text" v-model="intConsecutivo" style="width:60%;text-align:center" disabled readonly></td>
                                <td style="width:10%;">.: Fecha:</td>
                                <td><input style="width:80%;" type="text" v-model="strFecha" disabled readonly></td>
                                <td style="width:10%;">.: Descripción:</td>
                                <td><textarea type="text" v-model="strDescripcion" style="width:100%" disabled readonly></textarea></td>
                            </tr>
                            <tr>
                                <td style="width:10%;">.: Solicitud:</td>
                                <td><input type="text" v-model="objDocumento.codmov" style="width:60%;text-align:center" disabled readonly></td>
                                <td style="width:10%;">.: Centro de costo:</td>
                                <td><input style="width:80%;" type="text" v-model="selectCostos" disabled readonly></td>
                                <td style="width:10%;">.: Bodega:</td>
                                <td><input type="text" v-model="selectBodegas" style="width:100%" disabled readonly></input></td>
                            </tr>
                        </tbody>
                    </table>
                    <div   class='subpantalla' style='height:50vh; width:100%; margin-top:0px;  overflow-x:hidden'>
                        <table class='inicio' align='center'>
                            <tbody>
                                <tr>
                                    <td colspan='13' class='titulos'>.: Detalles:</td>
                                </tr>
                            </tbody>
                        </table>
                        <table class='inicio'>        
                            <thead>
                                <tr>
                                    <th class="titulosnew00" style="width: 7.69%;">Código</th>
                                    <th class="titulosnew00" style="width: 7.69%;">Nombre</th> 
                                    <th class="titulosnew00" style="width: 7.69%;">Bodega</th> 
                                    <th class="titulosnew00" style="width: 7.69%;">Centro</th> 
                                    <th class="titulosnew00" style="width: 7.69%;">Modelo</th> 
                                    <th class="titulosnew00" style="width: 7.69%;">Marca</th> 
                                    <th class="titulosnew00" style="width: 7.69%;">Serie</th> 
                                    <th class="titulosnew00" style="width: 7.69%;">Crédito</th> 
                                    <th class="titulosnew00" style="width: 7.69%;">Débito</th> 
                                    <th class="titulosnew00" style="width: 7.69%;">Unidad</th> 
                                    <th class="titulosnew00" style="width: 7.69%;">Valor</th> 
                                    <th class="titulosnew00" style="width: 7.69%;">Cantidad</th> 
                                    <th class="titulosnew00" style="width: 7.69%;">Valor total</th> 
                                </tr>
                            </thead>
                            <tbody>
                                <tr v-if="arrProductos < 1">
                                    <td colspan="13" align="center">No hay articulos, por favor agrega al menos uno</td>
                                </tr>
                                <tr v-else  style="height:50px;" v-for="(producto,index) in arrProductos" :key="index" v-bind:class="index % 2 ? 'saludo1a' : 'saludo2'">
                                    <td style="text-align:center">{{ producto.codigo }}</td>
                                    <td style="text-align:center">{{ producto.nombre }}</td>
                                    <td style="text-align:center">{{ producto.bodega}} - {{producto.bodega_nombre}}</td>
                                    <td style="text-align:center">{{ producto.cc}} - {{producto.cc_nombre}}</td>
                                    <td style="text-align:center">{{ producto.modelo}}</td>
                                    <td style="text-align:center">{{ producto.marca}}</td>
                                    <td style="text-align:center">{{ producto.serie}}</td>
                                    <td style="text-align:center">{{producto.credito}}</td>
                                    <td style="text-align:center">{{producto.debito}}</td>
                                    <td style="text-align:center">{{ producto.unidad}}</td>
                                    <td style="text-align:center">{{formatNumero(producto.valor)}}</td>
                                    <td style="text-align:center">{{producto.cantidad}}</td>
                                    <td style="text-align:center">{{ formatNumero(producto.total)}}</td>
                                </tr>
                                <tr>
                                    <td colspan="12" align="right">Total:</td>
                                    <td align="center">{{formatNumero(intTotalValor)}}</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
				</article>
                <!--MODALES-->
                <div v-show="isModalArticulos">
                    <transition name="modal">
                        <div class="modal-mask">
                            <div class="modal-wrapper">
                                <div class="modal-container">
                                    <table class="inicio ancho">
                                        <tr>
                                            <td class="titulos" colspan="2" >.: Artículos</td>
                                            <td class="cerrar" style="width:7%" @click="isModalArticulos = false">Cerrar</td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <input type="search" v-model="strSearch" @keyup="search(1,'articulo')" placeholder="Buscar por código o nombre" style="width: 100%;">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="titulos" colspan="3" >.: Resultados de búsqueda</td>
                                        </tr>
                                        <tr>
                                            <td>Total: {{intResults}}</td>
                                        </tr>
                                    </table>
                                    <table class='tablamv'>
                                        <thead>
                                            <th style="width:20%"class="titulosnew00" >Código</th>
                                            <th align="start" class="titulosnew00">Nombre</th>
                                        </thead>
                                        <tbody>
                                            <tr v-for="(data,index) in arrArticulos" @click="selectItem(data,'articulo');isModalArticulos = false"  :key="index" v-bind:class="index % 2 ? 'saludo1a' : 'saludo2'">
                                                <td align="center" style="width:20%">{{ data.codigo_articulo}}</td>
                                                <td >{{ data.nombre}}</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    <div  v-if="arrArticulos !=''" class="inicio">
                                        <p style="text-align:center">Página {{ intPage }} de {{ intTotalPages }}</p>
                                        <ul style="list-style:none; padding:0;display:flex;justify-content: center;align-items:center">
                                            <li v-show="intPage > 1" @click="search(intPage = 1)" style="cursor:pointer;padding:4px 10px;background:#fff; border:1px solid #39c"><< </li>
                                            <li v-show="intPage > 1" @click="search(--intPage)" style="cursor:pointer;padding:4px 10px;background:#fff; border:1px solid #39c" ><</li>
                                            <li v-show="intPage < intTotalPages" @click="search(++intPage)" style="cursor:pointer;padding:4px 10px;background:#fff; border:1px solid #39c" >></li>
                                            <li v-show="intPage < intTotalPages" @click="search(intPage = intTotalPages)" style="cursor:pointer;padding:4px 10px;background:#fff; border:1px solid #39c">>></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </transition>
                </div> 

                <div v-show="isModalRegistro">
                    <transition name="modal">
                        <div class="modal-mask">
                            <div class="modal-wrapper">
                                <div class="modal-container">
                                    <table class="inicio ancho">
                                        <tr>
                                            <td class="titulos" colspan="2" >.: Documentos</td>
                                            <td class="cerrar" style="width:7%" @click="isModalRegistro = false">Cerrar</td>
                                        </tr>
                                    </table>
                                    <table class='tablamv'>
                                        <thead>
                                            <tr>
                                                <th style="width:10%" class="titulosnew00" >Rp</th>
                                                <th style="width:10%" class="titulosnew00" >Fecha</th>
                                                <th style="width:60%" class="titulosnew00" >Detalle</th>
                                                <th style="width:20%" class="titulosnew00" >Valor</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr v-for="(data,index) in arrRegistros" @click="selectItem(data,'registro');isModalRegistro = false"  :key="index" v-bind:class="index % 2 ? 'saludo1a' : 'saludo2'">
                                                <td align="center" style="width:10%">{{data.consvigencia}}</td>
                                                <td align="center" style="width:10%">{{data.fecha}}</td>
                                                <td style="width:60%">{{data.detalle}}</td>
                                                <td align="right" style="width:20%">{{formatNumero(data.valor)}}</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </transition>
                </div> 
                <div v-show="isModalDocumento">
                    <transition name="modal">
                        <div class="modal-mask">
                            <div class="modal-wrapper">
                                <div class="modal-container">
                                    <table class="inicio ancho">
                                        <tr>
                                            <td class="titulos" colspan="2" >.: Documentos</td>
                                            <td class="cerrar" style="width:7%" @click="isModalDocumento = false">Cerrar</td>
                                        </tr>
                                    </table>
                                    <table class='tablamv'>
                                        <thead>
                                            <tr>
                                                <th style="width:20%" class="titulosnew00" >Código</th>
                                                <th style="width:60%" class="titulosnew00" >Nombre</th>
                                                <th style="width:20%" class="titulosnew00" >Fecha registro</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr v-for="(data,index) in arrDocumentos" @click="selectItem(data,'documento');isModalDocumento = false"  :key="index" v-bind:class="index % 2 ? 'saludo1a' : 'saludo2'">
                                                <td align="center" style="width:20%">{{data.consec}}</td>
                                                <td align="left" style="width:60%">{{data.nombre}}</td>
                                                <td align="center" style="width:20%">{{data.fecha}}</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </transition>
                </div> 
			</section>
		</form>

		<script src="node_modules/read-excel-file/bundle/read-excel-file.min.js"></script>
		<script src="node_modules/xlsx/dist/xlsx.full.min.js"></script>
        <script src="Librerias/vue/vue.min.js"></script>
		<script src="Librerias/vue/axios.min.js"></script>
		<script type="module" src="almacen/EntradaAjuste/ver/inve-verEntradaAjuste.js?<?= date('d_m_Y_h_i_s');?>"></script>
        
	</body>
</html>