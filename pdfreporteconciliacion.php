<?php
	require_once "tcpdf/tcpdf_include.php";
	require 'comun.inc';
	class MYPDF extends TCPDF {
		public function Header() {	
			$linkbd = conectar_v7();
			$linkbd -> set_charset("utf8");
			$sqlr="SELECT nit, razonsocial FROM configbasica WHERE estado='S'";
			$resp=mysqli_query($linkbd,$sqlr);
			while($row=mysqli_fetch_row($resp)){
				$nit = $row[0];
				$rs = $row[1];
			}
			$this->Image('imagenes/escudo.jpg', 22, 12, 25, 23.9, 'JPG', '', 'T', true, 300, '', false, false, 0, false, false, false);
			$this->SetFont('helvetica','B',8);
			$this->SetY(10);
			$this->RoundedRect(10, 10, 280, 31, 2.5,'');
			$this->Cell(50,31,'','R',0,'L');
			$this->SetY(10);
			$this->SetFont('helvetica','B',12);

			if(strlen($rs)<40){
				$this->SetX(58);
				$this->Cell(230,15,$rs,0,0,'C');
				$this->SetY(16);
			}else{
				$this->Cell(71);
				$this->MultiCell(200,15,$rs,0,'C',false,1,'','',true,4,false,true,19,'T',false);
				$this->SetY(18);
			}

			$this->SetX(58); 
			$this->SetFont('helvetica','B',11);
			$this->Cell(230,10,"NIT: $nit",0,0,'C'); 
			$this->SetY(27);
			$this->SetX(60);
			$this->Cell(192,14,"REPORTE CONCILIACION ".$_POST['fechai']." AL ".$_POST['fechaf'],1,0,'C');
			$this->SetFont('helvetica','I',10);
			$this->SetY(27);
			$this->SetX(62);

			$this->SetFont('helvetica','B',10);
			$this->Cell(200,7,'','T',0,'C',false,0,1); 
			$this->SetFont('helvetica','B',9);
			$this->SetY(27);
			$this->SetX(253);
			$this->Cell(37,5,"",'T',0,'L');
			$this->SetY(31);
			$this->SetX(253);
			$this->Cell(35,6,"FECHA IMPRESION ".$_POST['fecha'],0,0,'C');
			$this->SetY(36);
			$this->SetX(253);
			$this->Cell(35,5,date('d/m/Y'),0,0,'C');

			$this->ln(4);
			$this->line(10,45,290,45);
			$this->SetFont('times','B',10);
			$this->SetY(46);
			$this->Cell(0.1);
			$this->Cell(10,5,'Item',1,0,'C'); 
			$this->Cell(22,5,'Cuenta Cont.',1,0,'C');
			$this->Cell(96,5,'Banco',1,0,'C');
			$this->Cell(32,5,'Saldo Extracto',1,0,'C');
			$this->Cell(32,5,'Saldo Extracto Calc',1,0,'C');
			$this->Cell(32,5,'Diferencia',1,0,'C');
			$this->Cell(32,5,'Saldo Libros',1,0,'C');
			$this->Cell(24,5,'Estado',1,0,'C');
			$this->line(10,52,290,52);
			$this->ln(2);
		}
		public function Footer() {
			$this->SetY(-15);
			$this->SetFont('times','I',10);
			$this->Cell(0, 10, 'Impreso por: Software IDEAL.10 - IDEAL 10 SAS. - Pagina '.$this->getAliasNumPage().'/'.$this->getAliasNbPages(), 0, false, 'C', 0, '', 0, false, 'T', 'M'); 	
		}
	}
	$pdf = new MYPDF('L','mm','Letter', true, 'utf8', false);
	$pdf->SetDocInfoUnicode (true); 
	$pdf->SetMargins(10, 52, 10);
	$pdf->SetHeaderMargin(10);
	$pdf->SetFooterMargin(20);
	$pdf->SetAutoPageBreak(TRUE, 20);
	$pdf->AddPage();
	$niter = count($_POST['concepto1']);
	$concolor = 0;
	$pdf->SetFont('times','',9);
	for($xy = 0; $xy < $niter; $xy++){
		if ($concolor==0){
			$pdf->SetFillColor(200,200,200);
			$concolor = 1;
		}else{
			$pdf->SetFillColor(255,255,255);
			$concolor = 0;
		}
		$pdf->Cell(0.1);
		$pdf->Cell(10,5,($xy+1),1,0,'C',true,0,0,false,'T','C');
		$pdf->Cell(22,5,$_POST['concepto1'][$xy],1,0,'C',true,0,0,false,'T','C');
		$pdf->Cell(96,5,$_POST['concepto2'][$xy],1,0,'L',true,0,0,false,'T','C');
		$pdf->Cell(32,5,$_POST['concepto3'][$xy],1,0,'R',true,0,0,false,'T','C');
		$pdf->Cell(32,5,$_POST['concepto4'][$xy],1,0,'R',true,0,0,false,'T','C');
		$pdf->Cell(32,5,$_POST['concepto5'][$xy],1,0,'R',true,0,0,false,'T','C');
		$pdf->Cell(32,5,$_POST['concepto6'][$xy],1,0,'R',true,0,0,false,'T','C');
		$pdf->Cell(24,5,$_POST['concepto7'][$xy],1,1,'C',true,0,0,false,'T','C');
	}
	$pdf->Output();
?>