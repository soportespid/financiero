<?php
	header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=uft8");
	require "comun.inc";
	require "funciones.inc";
    require "funcionesSP.inc.php";
	session_start();
	$linkbd=conectar_v7();
	$linkbd -> set_charset("utf8");
	cargarcodigopag($_GET['codpag'],$_SESSION["nivel"]);
	date_default_timezone_set("America/Bogota");
?>
<!DOCTYPE >
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="es">
	<head>
		<meta http-equiv="Content-Type" content="text/html" charset="utf8"/>
		<meta http-equiv="X-UA-Compatible" content="IE=9"/>
		<title>:: Reporte Subsidios</title>
		<link href="css/css2.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<link href="css/css4.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
        <link href="css/cssSP.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<script type="text/javascript" src="css/programas.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
        <script type="text/javascript" src="jquery-1.11.0.min.js"></script> 
        
		<script>
			$(window).load(function () {
				$('#cargando').hide();
			});

			function generarfacturas()
			{
				var corte = parseInt(document.getElementById('corte').value);

				if((corte != '-1'))
				{
					document.form2.oculto.value='2';
					document.form2.submit();
				}
			}

			function actualizar()
			{
				document.form2.submit();
			}

			function excell()
			{
				document.form2.action="serv-reporteSubsidiosExcel.php";
				document.form2.target="_BLANK";
				document.form2.submit();
			}
		</script> 
		<?php titlepag();?>
	</head>
	<body>
        <IFRAME src="alertas.php" name="alertas" id="alertas" style="display:none"></IFRAME>
		<span id="todastablas2"></span>
		<table>
			<tr><script>barra_imagenes("serv");</script><?php cuadro_titulos();?></tr>	 
			<tr><?php menu_desplegable("serv");?></tr>
			<tr>
				<td colspan="3" class="cinta">
					<a class="mgbt"><img src="imagenes/add2.png"/></a>
					<a class="mgbt"><img src="imagenes/guardad.png" title="Guardar"/></a>
					<a href="" class="mgbt"><img src="imagenes/busca.png" title="Buscar"/></a>
					<a onclick="mypop=window.open('plan-agenda.php','','');mypop.focus()" class="mgbt"><img src="imagenes/agenda1.png" title="Agenda" /></a>
					<a onclick="mypop=window.open('serv-principal.php','','');mypop.focus();" class="mgbt"><img src="imagenes/nv.png" title="Nueva ventana"></a>
					<a onclick="mypop=window.open('<?php echo $url2; ?>','','');mypop.focus();" class="mgbt"><img src="imagenes/duplicar_pantalla.png" title="Duplicar pesta&ntilde;a"></a>
					<a href=""><img src="imagenes/excel.png" title="Excel" onClick="excell()" class="mgbt"></a>
					<a href="serv-menuInformes"><img src="imagenes/iratras.png" class="mgbt" alt="Atrás"></a>
                </td>
			</tr>
		</table>

		<form name="form2" method="post">
			<table class="inicio ancho" style="width:99.5%">
				<tr>
					<td class="titulos" colspan="3">Reporte Subsidio Servicios Públicos</td>
					<td class="cerrar" style="width:7%" onClick="location.href='serv-principal.php'">Cerrar</td>
				</tr>
				
				<tr>
					<td class="tamano01" style="width: 3cm;">Corte Liquidado:</td>
                    <td style="width: 20%;">
                        <select name="corte" id="corte" class="centrarSelect" style="width: 100%;" onchange="actualizar();">
                            <option value="-1" class="aumentarTamaño">SELECCIONE CORTE</option>
                            <?php
                                $sql = "SET lc_time_names = 'es_ES'";
                                mysqli_query($linkbd,$sql);

								$sqlr = "SELECT numero_corte, UPPER(MONTHNAME(fecha_inicial)), UPPER(MONTHNAME(fecha_final)), YEAR(fecha_inicial), YEAR(fecha_final) FROM srvcortes WHERE numero_corte > 0";
								$resp = mysqli_query($linkbd,$sqlr);
								while ($row = mysqli_fetch_row($resp))
								{
									if(@ $_POST['corte'] == $row[0])
									{
										echo "<option class='aumentarTamaño' value='$row[0]' SELECTED>$row[1] $row[3] - $row[2] $row[4]</option>";
									}
									else{echo "<option class='aumentarTamaño' value='$row[0]'>$row[1] $row[3] - $row[2] $row[4]</option>";}
								}
							?>
                        </select>
                    </td>

					<td style="padding-bottom:0px;height:35px;"><em class="botonflecha" onclick="generarfacturas()">Generar Reporte</em></td>
					
				</tr>
			</table>

            <div id="cargando" style=" position:absolute;left: 46%; bottom: 45%">
				<img src="imagenes/loading.gif" style=" width: 80px; height: 80px"/>
			</div>

			<div class="subpantalla" style="height:60%; width:99.2%;">
				<table class='inicio' align='center' width='99%'>
					

					<?php
						if(@ $_POST['oculto']=="2")
						{
							$iter  = 'saludo1a';
							$iter2 = 'saludo2';
							$tablaPorServicio = 1;
							$tot = 0;
							$cont = 0;
							$servicio = 0;

							//$sqlConsumoM3 = "SELECT DISTINCT SS.id, SS.nombre, SS.unidad_medida, SE.id, SE.descripcion, SUM(SL.consumo) FROM srvservicios AS SS, srvestratos AS SE, srvclientes AS SC, srvlectura AS SL WHERE SC.id_estrato = SE.id AND SL.id_cliente=SC.id AND SS.id = SL.id_servicio AND SL.corte=$_POST[corte] GROUP BY SS.id, SS.nombre, SS.unidad_medida, SE.id, SE.descripcion";	

							$sqlConsumoM3 = "SELECT DISTINCT SS.id, SS.nombre, SS.unidad_medida, SE.id, SE.descripcion, SUM(SL.consumo) FROM srvservicios AS SS, srvestratos AS SE, srvclientes AS SC, srvlectura AS SL WHERE SC.id_estrato = SE.id AND SL.id_cliente=SC.id AND SS.id = SL.id_servicio AND SL.corte=$_POST[corte] GROUP BY SS.id, SS.nombre, SS.unidad_medida, SE.id, SE.descripcion";
							$resConsumoM3 = mysqli_query($linkbd, $sqlConsumoM3);
							$encontrados = mysqli_num_rows($resConsumoM3);
							
							while ($rowConsumoM3 = mysqli_fetch_row($resConsumoM3))
							{
								$consumo = 0;
								$cargofijo = 0;
								$subsidio = 0;
								$contribucion = 0;

								
								$cont+=1;

								$sqlrCargoFijo = "SELECT COUNT(SF.id_cliente), SUM(SF.credito), SF.id_servicio FROM srvdetalles_facturacion AS SF, srvclientes AS SC WHERE SC.id = SF.id_cliente AND SF.corte=$_POST[corte] AND SF.id_servicio = '$rowConsumoM3[0]' AND SC.id_estrato=$rowConsumoM3[3] AND SF.id_tipo_cobro = 1";
								
								$resCargoFijo = mysqli_query($linkbd, $sqlrCargoFijo);

								$encontradosCargoFijo = mysqli_num_rows($resCargoFijo);
								
								$rowCargoFijo = mysqli_fetch_row($resCargoFijo);

								$sqlrConsumo = "SELECT SUM(SF.credito) FROM srvdetalles_facturacion AS SF, srvclientes AS SC WHERE SC.id = SF.id_cliente AND SF.corte=$_POST[corte] AND SF.id_servicio = '$rowConsumoM3[0]' AND SC.id_estrato=$rowConsumoM3[3] AND SF.id_tipo_cobro = 2";
								$resConsumo = mysqli_query($linkbd, $sqlrConsumo);
								$rowConsumo = mysqli_fetch_row($resConsumo);


								$sqlrSubsidio = "SELECT SUM(SF.debito) FROM srvdetalles_facturacion AS SF, srvclientes AS SC WHERE SC.id = SF.id_cliente AND SF.corte=$_POST[corte] AND SF.id_servicio = '$rowConsumoM3[0]' AND SC.id_estrato=$rowConsumoM3[3] AND (SF.id_tipo_cobro = 3 OR SF.id_tipo_cobro = 4)";
								$resSubsidio = mysqli_query($linkbd, $sqlrSubsidio);
								$rowSubsidio = mysqli_fetch_row($resSubsidio);

								$sqlrContribucion = "SELECT SUM(SF.credito) FROM srvdetalles_facturacion AS SF, srvclientes AS SC WHERE SC.id = SF.id_cliente AND SF.corte=$_POST[corte] AND SF.id_servicio = '$rowConsumoM3[0]' AND SC.id_estrato=$rowConsumoM3[3] AND SF.id_tipo_cobro = 5";
								$resContribucion = mysqli_query($linkbd, $sqlrContribucion);
								$rowContribucion = mysqli_fetch_row($resContribucion);

								$cargofijo = round($rowCargoFijo[1],2);
								$consumo = round($rowConsumo[0],2);
								$subsidio = round($rowSubsidio[0],2);
								$contribucion = round($rowContribucion[0],2);

								if($serv != $rowConsumoM3[0]){
									if($tablaPorServicio == 0){
										$tot = 1;
									}
									$tablaPorServicio = 1;
									//$tot = 0;
								}
								
								
									

								if($tot == 1){

									
									$total = $totSubsidio-$totContribucion;
									echo "
										<input type='hidden' name='totUsuarios[$cont]' value='$totUsuarios'>
										<input type='hidden' name='totConsumoM3[$cont]' value='$totConsumoM3'>
										<input type='hidden' name='totConsumo[$cont]' value='$totConsumo'>
										<input type='hidden' name='totCargofijo[$cont]' value='$totCargofijo'>
										<input type='hidden' name='totSubsidio[$cont]' value='$totSubsidio'>
										<input type='hidden' name='totContribucion[$cont]' value='$totContribucion'>
										<input type='hidden' name='total[$cont]' value='$total'>
									";
									?>		
										
									<tr class='<?php echo $iter ?>' style='text-align:center; text-transform:uppercase;font-weight: 700;'>
										<td><?php echo 'TOTAL' ?></td>
										<td><?php echo $totUsuarios ?></td>
										<td><?php echo $totConsumoM3 ?></td>
										<td><?php echo $totConsumo ?></td>
										<td><?php echo $totCargofijo ?></td>
										<td><?php echo $totSubsidio ?></td>
										<td><?php echo $totContribucion ?></td>
										<td><?php echo $total ?></td>
									</tr>
									<?php
									$totClaseUso = 0;
									$totUsuarios = 0;
									$totConsumoM3 = 0;
									$totConsumo = 0;
									$totCargofijo = 0;
									$totSubsidio = 0;
									$totContribucion = 0;
									$tot = 0;
									$total = 0;
								}
								
								if($tablaPorServicio == 1){

									?>
									<tr>
										<td colspan='90' class='titulos'>Resultados Busqueda: <?php echo $rowConsumoM3[1] ?></td>
									</tr>
								
									<tr class='titulos2' style='text-align:center;'>
										<td>Clase de uso </td>
										<td>Usuarios</td>
										<td>Consumo M3</td>
										<td>Facturaci&oacute;n de consumo</td>
										<td>Cargo fijo</td>
										<td>Valor subsidio</td>
										<td>Valor contribucion</td>
										<td>Valor a pagar (Diferencia Sub. menos Cont.)</td>
										
									</tr>
									<?php
									$serv = $rowConsumoM3[0];
									$tablaPorServicio = 0;
								}
								echo "
									<input type='hidden' name='claseDeUso[]' value='$rowConsumoM3[4]'>
									<input type='hidden' name='servicio[]' value='$rowConsumoM3[0]'>
									<input type='hidden' name='nombreServ[]' value='$rowConsumoM3[1]'>
									<input type='hidden' name='usuarios[]' value='$rowCargoFijo[0]'>
									<input type='hidden' name='consumoM3[]' value='$rowConsumoM3[5]'>
									<input type='hidden' name='consumo[]' value='$consumo'>
									<input type='hidden' name='cargoFijo[]' value='$cargofijo'>
									<input type='hidden' name='valorSubsidio[]' value='$subsidio'>
									<input type='hidden' name='valorContribucion[]' value='$contribucion'>
								";
								?>			
								<tr class='<?php echo $iter ?>' style='text-align:center; text-transform:uppercase;'>
									<td><?php echo $rowConsumoM3[4] ?></td>
									<td><?php echo $rowCargoFijo[0] ?></td>
									<td><?php echo $rowConsumoM3[5] ?></td>
									<td><?php echo $consumo ?></td>
									<td><?php echo $cargofijo ?></td>
									<td><?php echo $subsidio ?></td>
									<td><?php echo $contribucion ?></td>
									<td><?php echo '' ?></td>
								</tr>
								<?php

								//$totClaseUso += $rowConsumoM3[4];
								$totUsuarios += $rowCargoFijo[0];
								$totConsumoM3 += $rowConsumoM3[5];
								$totConsumo += $consumo;
								$totCargofijo += $cargofijo;
								$totSubsidio += $subsidio;
								$totContribucion += $contribucion;
								
								if($encontrados == $cont){
									$total = $totSubsidio-$totContribucion;
									echo "
										<input type='hidden' name='totUsuarios[$cont]' value='$totUsuarios'>
										<input type='hidden' name='totConsumoM3[$cont]' value='$totConsumoM3'>
										<input type='hidden' name='totConsumo[$cont]' value='$totConsumo'>
										<input type='hidden' name='totCargofijo[$cont]' value='$totCargofijo'>
										<input type='hidden' name='totSubsidio[$cont]' value='$totSubsidio'>
										<input type='hidden' name='totContribucion[$cont]' value='$totContribucion'>
										<input type='hidden' name='total[$cont]' value='$total'>
									";
									?>		
										
									<tr class='<?php echo $iter ?>' style='text-align:center; text-transform:uppercase;font-weight: 700;'>
										<td><?php echo 'TOTAL' ?></td>
										<td><?php echo $totUsuarios ?></td>
										<td><?php echo $totConsumoM3 ?></td>
										<td><?php echo $totConsumo ?></td>
										<td><?php echo $totCargofijo ?></td>
										<td><?php echo $totSubsidio ?></td>
										<td><?php echo $totContribucion ?></td>
										<td><?php echo $total ?></td>
									</tr>
									<?php
									$totClaseUso = 0;
									$totUsuarios = 0;
									$totConsumoM3 = 0;
									$totConsumo = 0;
									$totCargofijo = 0;
									$totSubsidio = 0;
									$totContribucion = 0;
									$tot = 0;
								}
								
							}

							$sqlConsumoM3 = "SELECT DISTINCT SS1.id, SS1.nombre, SS1.unidad_medida, SE1.id, SE1.descripcion, '' FROM srvservicios AS SS1, srvestratos AS SE1 WHERE NOT EXISTS(SELECT 1 FROM srvlectura SL1 WHERE SL1.id_servicio = SS1.id)";
							$resConsumoM3 = mysqli_query($linkbd, $sqlConsumoM3);
							$encontrados = mysqli_num_rows($resConsumoM3);
							$tablaPorServicio = 1;
							$tot = 0;
							$cont = 0;
							$contador = 0;
							while ($rowConsumoM3 = mysqli_fetch_row($resConsumoM3))
							{
								$consumo = 0;
								$cargofijo = 0;
								$subsidio = 0;
								$contribucion = 0;

								
								$cont+=1;

								$sqlrCargoFijo = "SELECT COUNT(SF.id_cliente), SUM(SF.credito), SF.id_servicio FROM srvdetalles_facturacion AS SF, srvclientes AS SC WHERE SC.id = SF.id_cliente AND SF.corte=$_POST[corte] AND SF.id_servicio = '$rowConsumoM3[0]' AND SC.id_estrato=$rowConsumoM3[3] AND SF.id_tipo_cobro = 1";
								
								$resCargoFijo = mysqli_query($linkbd, $sqlrCargoFijo);

								$encontradosCargoFijo = mysqli_num_rows($resCargoFijo);
								
								$rowCargoFijo = mysqli_fetch_row($resCargoFijo);

								$sqlrConsumo = "SELECT SUM(SF.credito) FROM srvdetalles_facturacion AS SF, srvclientes AS SC WHERE SC.id = SF.id_cliente AND SF.corte=$_POST[corte] AND SF.id_servicio = '$rowConsumoM3[0]' AND SC.id_estrato=$rowConsumoM3[3] AND SF.id_tipo_cobro = 2";
								$resConsumo = mysqli_query($linkbd, $sqlrConsumo);
								$rowConsumo = mysqli_fetch_row($resConsumo);


								$sqlrSubsidio = "SELECT SUM(SF.debito) FROM srvdetalles_facturacion AS SF, srvclientes AS SC WHERE SC.id = SF.id_cliente AND SF.corte=$_POST[corte] AND SF.id_servicio = '$rowConsumoM3[0]' AND SC.id_estrato=$rowConsumoM3[3] AND (SF.id_tipo_cobro = 3 OR SF.id_tipo_cobro = 4)";
								$resSubsidio = mysqli_query($linkbd, $sqlrSubsidio);
								$rowSubsidio = mysqli_fetch_row($resSubsidio);

								$sqlrContribucion = "SELECT SUM(SF.credito) FROM srvdetalles_facturacion AS SF, srvclientes AS SC WHERE SC.id = SF.id_cliente AND SF.corte=$_POST[corte] AND SF.id_servicio = '$rowConsumoM3[0]' AND SC.id_estrato=$rowConsumoM3[3] AND SF.id_tipo_cobro = 5";
								$resContribucion = mysqli_query($linkbd, $sqlrContribucion);
								$rowContribucion = mysqli_fetch_row($resContribucion);

								$cargofijo = round($rowCargoFijo[1],2);
								$consumo = round($rowConsumo[0],2);
								$subsidio = round($rowSubsidio[0],2);
								$contribucion = round($rowContribucion[0],2);

								if($serv != $rowConsumoM3[0]){
									if($tablaPorServicio == 0){
										$tot = 1;
									}
									$tablaPorServicio = 1;
									//$tot = 0;
								}
								if($tablaPorServicio == 1){

									?>
									<tr>
										<td colspan='90' class='titulos'>Resultados Busqueda: <?php echo $rowConsumoM3[1] ?></td>
									</tr>
								
									<tr class='titulos2' style='text-align:center;'>
										<td>Clase de uso </td>
										<td>Usuarios</td>
										<td>Valor Total Tarifa Cobrada</td>
										<td>Valor Subsidio </td>
										<td>Contribución Facturaci&oacute;n de consumo</td>
										<td>Valor a pagar (Diferencia Sub. menos Cont.)</td>
										
										
									</tr>
									<?php
									$serv = $rowConsumoM3[0];
									$tablaPorServicio = 0;
								}
								$totalTarifaCobradaD = $cargofijo + $consumo;

								
								if($rowCargoFijo[0]>0){
									echo "
										<input type='hidden' name='claseDeUso1[]' value='$rowConsumoM3[4]'>
										<input type='hidden' name='rowCargoFijo1[]' value='$rowCargoFijo[0]'>
										<input type='hidden' name='totalTarifaCobradaD1[]' value='$totalTarifaCobradaD'>
										<input type='hidden' name='subsidio1[]' value='$subsidio'>
										<input type='hidden' name='contribucion1[]' value='$contribucion[0]'>
										<input type='hidden' name='servicio1[]' value='$rowConsumoM3[0]'>
										<input type='hidden' name='nombreServ1[]' value='$rowConsumoM3[1]'>
									";
									?>			
									<tr class='<?php echo $iter ?>' style='text-align:center; text-transform:uppercase;'>
										<td><?php echo $rowConsumoM3[4] ?></td>
										<td><?php echo $rowCargoFijo[0] ?></td>
										<td><?php echo $totalTarifaCobradaD ?></td>
										<td><?php echo $subsidio ?></td>
										<td><?php echo $contribucion ?></td>
										<td><?php echo '' ?></td>
									</tr>
									<?php

									//$totClaseUso += $rowConsumoM3[4];
									$totUsuarios += $rowCargoFijo[0];
									$totConsumoM3 += $rowConsumoM3[5];
									$totConsumo += $consumo;
									$totCargofijo += $cargofijo;
									$totSubsidio += $subsidio;
									$totContribucion += $contribucion;

									$contador++;
								}
								if($encontrados == $cont){

									$total = $totSubsidio-$totContribucion;
									$totalTarifaCobrada = $totCargofijo + $totConsumo;
									
									echo "
										<input type='hidden' name='totUsuarios1[$contador]' value='$totUsuarios'>
										<input type='hidden' name='totalTarifaCobrada1[$contador]' value='$totalTarifaCobrada'>
										<input type='hidden' name='totSubsidio1[$contador]' value='$totSubsidio'>
										<input type='hidden' name='totContribucion1[$contador]' value='$totContribucion'>
										<input type='hidden' name='total1[$contador]' value='$total'>
									";

									?>		
										
									<tr class='<?php echo $iter ?>' style='text-align:center; text-transform:uppercase;font-weight: 700;'>
										<td><?php echo 'TOTAL' ?></td>
										<td><?php echo $totUsuarios ?></td>
										<td><?php echo $totalTarifaCobrada ?></td>
										<td><?php echo $totSubsidio ?></td>
										<td><?php echo $totContribucion ?></td>
										<td><?php echo $total ?></td>
									</tr>
									<?php

									$totClaseUso = 0;
									$totUsuarios = 0;
									$totConsumoM3 = 0;
									$totConsumo = 0;
									$totCargofijo = 0;
									$totSubsidio = 0;
									$totContribucion = 0;
									$tot = 0;

								}
							}
                            //$total = consultaValorTotalSRVFACTURAS($numeroFactura);
                           /*  $total = ($total / 100);
                            $total = ceil($total);
                            $total = $total * 100;	
                            $total = number_format($total, 2, '.', ''); */

								
							if ($encontrados == 0)
							{
					        ?>
								<table class='inicio'>
									<tr>
										<td class='saludo1' style='text-align:center;'>
											<img src='imagenes\alert.png' style='width:25px; height: 30px;'> No se han encontrado registros <img src='imagenes\alert.png' style='width:25px; height: 30px;'>
										</td>
									</tr>
								</table>
					        <?php
							}
						}
					?>
				</table>
			</div>

			<input type="hidden" name="oculto" id="oculto" value="1"/>
		</form>
	</body>
</html>
