<?php
    ini_set('max_execution_time',3600);
	require "comun.inc";
	require "funciones.inc";
	session_start();
	$linkbd = conectar_v7();	
	$linkbd -> set_charset("utf8");	
	cargarcodigopag($_GET['codpag'],$_SESSION["nivel"]);
	header("Cache-control: private"); // Arregla IE 6
	date_default_timezone_set("America/Bogota");

?>
<!DOCTYPE >
<html lang="es">
	<head>
		<meta charset="UTF-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
    	<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<title>:: IDEAL 10 - Tesorer&iacute;a</title>
        <link href="favicon.ico" rel="shortcut icon"/>
        <link href="css/css2.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<link href="css/css4.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<script type="text/javascript" src="jquery-1.11.0.min.js"></script> 
        <script type="text/javascript" src="css/programas.js"></script>
        <script type="text/javascript" src="css/calendario.js"></script>

        <script>
            $(window).load(function () { $('#cargando').hide();});

            function guardar()
            {
                if(document.form2.periodo.value!='')
                    {if (confirm("Esta Seguro de Guardar")){document.form2.oculto.value=2;document.form2.listar.value=2;document.form2.submit();}}
                else{alert('Seleccione un MES para realizar la Depreciaci�n');}
            }


            function buscacta(e)
            {
                if (document.form2.cuenta.value!=""){document.form2.bc.value='1';document.form2.submit();}
            }

            function buscacc(e)
            {
                if (document.form2.cc.value!=""){document.form2.bcc.value='1';document.form2.submit();}
            }

            function validar(){document.form2.submit();}

            function buscaract()
            {
                //alert("Balance Descuadrado");
                document.form2.listar.value=2;
                document.form2.submit();
            }

            function excell()
            {
                document.form2.action="teso-reporteacuerdosdepagoexcel.php";
                document.form2.target="_BLANK";
                document.form2.submit(); 
                document.form2.action="";
                document.form2.target="";
            }
        </script>

    </head>
    <body>
        <table>
            <tr><script>barra_imagenes("teso");</script><?php cuadro_titulos();?></tr>
            <tr><?php menu_desplegable("teso");?></tr>
            <tr>
                <td colspan="3" class="cinta">
                    <a href="teso-reporteacuerdosdepago.php" class="mgbt"><img src="imagenes/add.png"  title="Nuevo"/></a>
                    <a href="teso-reporteacuerdosdepago.php" class="mgbt"><img src="imagenes/busca.png"  title="Buscar"/></a>
                    <a onClick="mypop=window.open('plan-agenda.php','','');mypop.focus()" class="mgbt"><img src="imagenes/agenda1.png" title="Agenda" /></a>
                    <a href="#" onClick="mypop=window.open('teso-principal.php','','');mypop.focus();" class="mgbt"><img src="imagenes/nv.png" title="Nueva Ventana"></a>
                    <img src="imagenes/excel.png" title="Excel" onClick='excell()' class="mgbt"/>
                    <a href="teso-informespredios.php" class="mgbt"><img src="imagenes/iratras.png" title="Atr&aacute;s"></a>
                </td>
            </tr>
        </table>
        <form name="form2" method="post" action=""> 
            <div class="loading" id="divcarga"><span>Cargando...</span></div> 
            
            <table class="inicio" align="center"  >
                <tr>
                    <td class="titulos" colspan="8">.: Reporte de acuerdos de pago predial</td>
                    <td  class="cerrar"  style="width:5%;"><a href="teso-principal.php">Cerrar</a></td>
                </tr>
                <tr>
                    <td class="saludo1" style="width:10%;">Fecha Inicial:</td>
                    <td style="width:10%;">
                        <input type="text" name="fecha1" value="<?php echo $_POST['fecha1']?>" maxlength="10" onchange="" onKeyPress="javascript:return solonumeros(event)" onKeyUp="return tabular(event,this)"  id="fc_1198971545" onKeyDown="mascara(this,'/',patron,true)" style="width:75%;" title="DD/MM/YYYY"/>&nbsp;<a href="#" onClick="displayCalendarFor('fc_1198971545');" title="Calendario"><img src="imagenes/calendario04.png" style="width:20px;"/></a>
                    </td>
                    <td class="saludo1" style="width:10%;">Fecha Final:</td>
                    <td style="width:10%;">
                        <input name="fecha2" type="text" value="<?php echo $_POST['fecha2']?>" maxlength="10" onchange=""  onKeyPress="javascript:return solonumeros(event)" onKeyUp="return tabular(event,this)"  id="fc_1198971546" onKeyDown="mascara(this,'/',patron,true)" style="width:75%;" title="DD/MM/YYYY"/>&nbsp;<a href="#" onClick="displayCalendarFor('fc_1198971546');" title="Calendario"><img src="imagenes/calendario04.png" style="width:20px;"/></a>
                    </td>
                    
                    <input name="oculto" type="hidden" value="1">
                    </td>
                    <td style=" padding-bottom: 0em"><input type="hidden" name="listar" id="listar" value="<?php echo $_POST['listar'] ?>" /><em class="botonflecha" onClick="buscaract()">Buscar</em></td>
                </tr>
                <tr>    
            
                </tr>          
            </table>    
            <div class="subpantalla" style="height:66.5%; width:99.6%;">
                <table class="inicio">
                    <tr><td class="titulos" colspan="12">Listado de acuerdos de pago predial</td></tr>
                    <tr>
                        <td class="titulos2">No de acuerdo</td>
                        <td class="titulos2">Cod. Catastral</td>
                        <td class="titulos2">Fecha</td>
                        <td class="titulos2">Valor</td>
                        <td class="titulos2">Tercero</td>
                        <td class="titulos2">Cuotas pactadas</td>
                        <td class="titulos2">Cuotas pagas</td>
                        <td class="titulos2">Cuotas faltantes</td>
                        <td class="titulos2">Valor pagado</td>
                        <td class="titulos2">Valor faltante</td>
                        <td class="titulos2">Estado</td>
                    </tr>   
                    <?php
                    if($_POST['listar']=='2')
                    {
                        $vigenciaRp = 0;
                        if($_POST['fecha1']!='')
                        {
                            preg_match("/([0-9]{2})\/([0-9]{2})\/([0-9]{4})/", $_POST['fecha1'],$fech1);
                            
                            $f1=$fech1[3]."-".$fech1[2]."-".$fech1[1];
                            $vigenciaRp = $fech1[3];
                            if($_POST['fecha2']!='')
                            {
                                preg_match("/([0-9]{2})\/([0-9]{2})\/([0-9]{4})/", $_POST['fecha2'],$fech2);
                                $f2=$fech2[3]."-".$fech2[2]."-".$fech2[1];
                                $criterio=" fecha_acuerdo between '$f1' AND '$f2'";
                            }
                            else{$criterio=" fecha_acuerdo >= '$f1'";}
                        }
                        else if($_POST['fecha2']!='') 
                        {
                            preg_match("/([0-9]{2})\/([0-9]{2})\/([0-9]{4})/", $_POST['fecha2'],$fech2);
                            $f2=$fech2[3]."-".$fech2[2]."-".$fech2[1];
                            $vigenciaRp = $fech1[3];
                            $criterio=" fecha_acuerdo <= '$f2'";
                        }
                        else
                        {
                            $criterio="";
                            $vigenciaRp = date('Y');
                        }
                
                        $sqlr="SELECT idacuerdo, codcatastral, fecha_acuerdo, valor_pago, tercero, cuotas, estado FROM tesoacuerdopredial WHERE $criterio ORDER BY idacuerdo DESC";
                        //echo $sqlr."<br>";
                        $row = view($sqlr);
                        $tama=count($row);
                        $con=0;
                        $co="zebra1";
                        $co2='zebra2';
                        
                        while($con<$tama) 
                        {
                            
                            $sqlrAbonos = "SELECT COUNT(id_abono), SUM(valortotal) FROM tesoabono WHERE idacuerdo = '".$row[$con]['idacuerdo']."' ";
                            $respAbonos = mysqli_query($linkbd, $sqlrAbonos);
                            $rowAbono = mysqli_fetch_row($respAbonos);

                            $cuostasFaltantes = $row[$con]['cuotas'] - $rowAbono[0];
                            $valorFaltante = $row[$con]['valor_pago'] - $rowAbono[1];

                            $tercero = buscatercero($row[$con]['tercero']);
                            echo "<tr class='$co' onMouseOver=\"anterior=this.style.backgroundColor;this.style.backgroundColor='#40b3ff';\" onMouseOut=\"this.style.backgroundColor=anterior\" >
                            <td style='width:7%; text-align:center;'>".$row[$con]['idacuerdo']."</td>
                            <td style='width:8%'>".$row[$con]['codcatastral']."</td>
                            <td style='width:8%'>".$row[$con]['fecha_acuerdo']."</td>
                            <td style='width:8%'>".number_format($row[$con]['valor_pago'],2)."</td>
                            <td>".$row[$con]['tercero']." - ".$tercero."</td>
                            <td style='width:8%'>".$row[$con]['cuotas']."</td>
                            <td style='width:8%'>".$rowAbono[0]."</td>
                            <td style='width:8%'>".$cuostasFaltantes."</td>
                            <td style='width:8%'>".number_format($rowAbono[1],2)."</td>
                            <td style='width:8%'>".number_format($valorFaltante,2)."</td>
                            <td style='width:8%'>".$row[$con]['estado']."</td>

                            </tr>";
                            echo "
                                <input type='hidden' name='idacuerdo[]' id='idacuerdo[]' value='".$row[$con]['idacuerdo']."'>
                                <input type='hidden' name='codcatastral[]' id='codcatastral[]' value='".$row[$con]['codcatastral']."'>
                                <input type='hidden' name='fecha_acuerdo[]' id='fecha_acuerdo[]' value='".$row[$con]['fecha_acuerdo']."'>
                                <input type='hidden' name='valor_pago[]' id='valor_pago[]' value='".$row[$con]['valor_pago']."'>
                                <input type='hidden' name='tercero[]' id='tercero[]' value='".$row[$con]['tercero']." - ".$tercero."'>
                                <input type='hidden' name='cuotas[]' id='cuotas[]' value='".$row[$con]['cuotas']."'>
                                <input type='hidden' name='cuotasPagas[]' id='cuotasPagas[]' value='".$rowAbono[0]."'>
                                <input type='hidden' name='cuotasFaltantes[]' id='cuotasFaltantes[]' value='".$cuostasFaltantes."'>
                                <input type='hidden' name='valorPagado[]' id='valorPagado[]' value='".$rowAbono[1]."'>
                                <input type='hidden' name='valorFaltante[]' id='valorFaltante[]' value='".$valorFaltante."'>
                                <input type='hidden' name='estado[]' id='estado[]' value='".$row[$con]['estado']."'>

                                ";
                            $aux=$co;
                            $co=$co2;
                            $co2=$aux;
                        
                            $con+=1;
                        }
                    }
                    ?><script>document.getElementById('divcarga').style.display='none';</script>
                </table>
            </div>
        </form>
    </body>
</html>