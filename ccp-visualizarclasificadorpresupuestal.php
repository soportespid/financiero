<?php
	header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=utf8");
	require "comun.inc";
	require "funciones.inc";
	session_start();
	$linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");
	cargarcodigopag($_GET['codpag'],$_SESSION["nivel"]);
	date_default_timezone_set("America/Bogota");
?>
<!DOCTYPE >
<html lang=es>
	<head>
		<meta charset="utf-8"/>
		<meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1"/>
		<meta name="viewport" content="user-scalable=no">
		<title>:: Ideal - Presupuesto</title>
		<link href="favicon.ico" rel="shortcut icon"/>
		<link href="css/css2.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<script type="text/javascript" src="css/programas.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script type="text/javascript" src="JQuery/jquery-2.1.4.min.js"></script>
	</head>
	<body>
		<IFRAME src="alertas.php" name="alertas" id="alertas" style="display:none"></IFRAME>
		<span id="todastablas2"></span>
		<table>
			<tr><script>barra_imagenes("ccpet");</script><?php cuadro_titulos();?></tr>
			<tr><?php menu_desplegable("ccpet");?></tr>
			<tr>
				<td colspan="3" class="cinta"><a class="mgbt"><img src="imagenes/add2.png" /></a> <a class="mgbt"><img src="imagenes/guardad.png" style="width:24px;"/></a> <a class="mgbt"><img src="imagenes/buscad.png"/></a> <a href="#" onClick="mypop=window.open('ccp-principal.php','','');mypop.focus();" class="mgbt"><img src="imagenes/nv.png" title="Nueva Ventana"></a></td>
			</tr>
		</table>
		<form name="form2" method="post" action="" >
			<div style="margin: 0px 10px 10px; border-radius: 0 0 0 5px; height: 500px; overflow: scroll; overflow-x: hidden; resize: vertical">
				<table class="inicio">
					<tr>
						<td class="titulos" colspan="2">.: Clasificadores </td>
						<td class="cerrar" style="width:7%;" ><a href="ccp-principal.php">&nbsp;Cerrar</a></td>
					</tr>
					<td style="background-repeat:no-repeat; background-position:center;">
						<ol id="lista2">
							<li onClick="location.href='ccp-ingreso.php'" style="cursor:pointer;">Clasificadores de ingresos CCPET</li>
							<li onClick="location.href='ccp-gasto.php'" style="cursor:pointer;">Clasificadores de gastos CCPET</li>
							<li onClick="location.href='ccp-producto.php'" style="cursor:pointer;">Clasificador program&aacute;tico de inversi&oacute;n</li>
							<li onClick="location.href='ccp-bienestransportables.php'" style="cursor:pointer;">Clasificador Bienes transportables sec. 0-4</li>
							<li onClick="location.href='ccp-servicios.php'" style="cursor:pointer;">Clasificador Servicios sec. 5-9</li>
							<li onClick="location.href='ccp-cuin.php'" style="cursor:pointer;">Clasificador CUIN</li>
							<li onClick="location.href='ccp-fuentes-cuipo.php'" style="cursor:pointer;">Fuentes CUIPO</li>
							<li onClick="location.href='ccp-politicapublica.php'" style="cursor:pointer;">Politica publica</li>
							<li onClick="location.href='ccp-vigenciadelgasto.php'" style="cursor:pointer;">Vigencia del gasto</li>
							<li onClick="location.href='ccp-generarclasificadorgastos.php'" style="cursor:pointer;">Crear Clasificadores de gastos.</li>
							<li onClick="location.href='ccp-generarclasificadoringresos.php'" style="cursor:pointer;">Crear Clasificadores de ingresos.</li>
							<li onClick="location.href='ccp-seleccionarcuentasingresos.php'" style="cursor:pointer;">Codificacion catalogo de presupuesto entidad territorial - Ingresos.</li>
                            <li onClick="location.href='ccpet-catalogoCompIngresoBuscar.php'" style="cursor:pointer;">Clasificador complementario de ingresos.</li>
                            <li onClick="location.href='ccpet-catalogoSuperavitBuscar.php'" style="cursor:pointer;">Clasificador de superavit.</li>
						</ol>
					</td>
				</table>
			</div>
		</form>
	</body>
</html>
