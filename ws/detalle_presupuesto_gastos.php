<?php
    require_once "../comun.inc";
    require_once "../funciones.inc";

    $linkbd = conectar_v7();	
    $linkbd_V7 = conectar_v7();	

    $cuenta = $_POST['cuenta'];
    $unidadEjecutora = $_POST['unidadEjecutora'];
    $medioPago = $_POST['medioPago'];
    $vigencia = $_POST['vigencia'];
    $codInv = $_POST['codInv'];
    $fechaIni = $_POST['fechaIni'];
    $fechaFin = $_POST['fechaFin'];

    $crit = '';
    $critBienesTransportables = '';
    if($_POST['unidadEjecutora'] != ''){
        $crit = "AND unidad = '$_POST[unidadEjecutora]'";
        $critBienesTransportables = " AND CB.unidad = '$_POST[unidadEjecutora]'";
    }
    
    $crit1 = '';
    $crit1BienesTransportables = '';
    if($_POST['medioPago'] != ''){
        $crit1 = "AND medioPago = '$medioPago'";
        $crit1BienesTransportables = " AND CB.medioPago = '$medioPago'";
    }

    $critinv1 = '';
    if($_POST['medioPago'] != ''){
        $critinv1 = "AND CP.medio_pago = '$medioPago'";
    }

    $tabla='<table class="titulos" width="70%">';


    $sqlr = "SELECT CB.fuente, CB.medioPago, CB.unidad, CB.valor FROM ccpetinicialvalorgastos AS CB WHERE CB.cuenta = '$cuenta' AND CB.vigencia='$vigencia' $critBienesTransportables $crit1BienesTransportables UNION SELECT CA.fuente, CA.mediopago, CA.tipo, CA.valor FROM ccpetadiciones AS CA WHERE CA.cuenta = '$cuenta' AND CA.vigencia='$vigencia' AND LEFT(CA.cuenta, 3)!='2.3' AND CA.cuenta_clasificadora='' AND NOT EXISTS(SELECT 1 FROM ccpetinicialvalorgastos AS CBT WHERE CBT.cuenta = '$cuenta' AND CBT.vigencia='$vigencia' AND CA.fuente = CBT.fuente) UNION SELECT CT.fuente, CT.mediopago, CT.tipo, CT.valor FROM ccpettraslados AS CT WHERE CT.cuenta = '$cuenta' AND CT.productoservicio = '' AND CT.vigencia='$vigencia' AND LEFT(CT.cuenta, 3)!='2.3' AND NOT EXISTS(SELECT 1 FROM ccpetinicialvalorgastos AS CTT WHERE CTT.cuenta = '$cuenta' AND CTT.vigencia='$vigencia' AND CT.fuente = CTT.fuente) AND NOT EXISTS(SELECT 1 FROM ccpetadiciones AS CAT WHERE CAT.cuenta = '$cuenta' AND CAT.vigencia='$vigencia' AND CT.fuente = CAT.fuente)";


    $result = mysqli_query($linkbd, $sqlr);
    if(mysqli_num_rows($result) > 0){
        $tabla.='<tr>
                    <td>Fuente</td>
                    <td>MedioPago</td>
                    <td>Unidad</td>
                    <td>Inicial</td>
                    <td>Adicion</td>
                    <td>Reduccion</td>
                    <td>Credito</td>
                    <td>Contracredito</td>
                    <td>Definitivo</td>
                    <td>Disponibilidad</td>
                    <td>Compromiso</td>
                    <td>Obligacion</td>
                    <td>Pago</td>
                    <td>Saldo</td>
                </tr>';
               
        while($row = mysqli_fetch_array($result)){

            $inicial = 0;
            $adicion = 0;
            $reduccion = 0;
            $credito = 0;
            $contracredito = 0;
            $presupuestoDefinitivo = 0;
            $totalCDPEnt = 0;
            $totalRPEnt = 0;
            $totalCxPEnt = 0;
            $totalEgresoEnt = 0;
            $saldo = 0;

            $sqlr_fuente = "SELECT fuente_financiacion FROM ccpet_fuentes WHERE id_fuente = '$row[0]'";
            $result_fuente = mysqli_query($linkbd, $sqlr_fuente);
            $row_fuente = mysqli_fetch_array($result_fuente);

            if($row_fuente[0] == '')
            {
                $sqlr_fuente = "SELECT nombre FROM ccpet_fuentes_cuipo WHERE codigo_fuente = '$row[0]'";
                $result_fuente = mysqli_query($linkbd, $sqlr_fuente);
                $row_fuente = mysqli_fetch_array($result_fuente);
            }

            $sqlr_unidad = "SELECT nombre FROM pptouniejecu WHERE id_cc = '$row[2]'";
            $result_unidad = mysqli_query($linkbd, $sqlr_unidad);
            $row_unidad = mysqli_fetch_array($result_unidad);

            if($row[1] == 'CSF'){
                $medioPago = 'Con situacion de fondos';
            }else{
                $medioPago = 'Sin situacion de fondos';
            }

            //============= AQUI INICIA EL PROCESO PARA CALCULAR LA ADICION =========//


            $sqlrAdicion = "SELECT SUM(valor) FROM ccpetadiciones WHERE cuenta = '$cuenta' AND fecha BETWEEN '$fechaIni' AND '$fechaFin' AND vigencia='$vigencia' AND estado!='N' AND mediopago = '".$row[1]."' AND fuente = '".$row[0]."' AND cuenta_clasificadora = ''";
            $resAdicion = mysqli_query($linkbd_V7, $sqlrAdicion);
            $rowAdicion = mysqli_fetch_row($resAdicion);

            $adicion = $rowAdicion[0];

            //============= AQUI FINALIZA EL PROCESO PARA CALCULAR LA ADICION =========//

            //============= AQUI INICIA EL PROCESO PARA CALCULAR LA REDUCCION =========//

            $sqlrReduccion = "SELECT SUM(valor) FROM ccpetreducciones WHERE cuenta = '$cuenta' AND fecha BETWEEN '$fechaIni' AND '$fechaFin' AND vigencia='$vigencia' AND estado!='N' AND mediopago = '".$row[1]."' AND fuente = '".$row[0]."' AND cuenta_clasificadora = ''";
            $resReduccion = mysqli_query($linkbd_V7, $sqlrReduccion);
            $rowReduccion = mysqli_fetch_row($resReduccion);

            $reduccion = $rowReduccion[0];

            $sqlrReduccion_n = "SELECT SUM(CR.valor) FROM ccpetreduccion AS CR, ccpetacuerdos AS CAC WHERE CR.rubro = '$cuenta' AND CR.acuerdo = CAC.id_acuerdo AND CAC.fecha BETWEEN '$fechaIni' AND '$fechaFin' AND CR.vigencia='$vigencia' AND CR.medio_pago = '".$row[1]."' AND CR.fuente = '".$row[0]."'";
            $resReduccion_n = mysqli_query($linkbd_V7, $sqlrReduccion_n);
            $rowReduccion_n = mysqli_fetch_row($resReduccion_n);

            $reduccion += $rowReduccion_n[0];

            //============= AQUI FINALIZA EL PROCESO PARA CALCULAR LA REDUCCION =========//


            //============= AQUI INICIA EL PROCESO PARA CALCULAR EL CREDITO =========//
            
            $sqlrCredito = "SELECT SUM(valor) FROM ccpettraslados WHERE cuenta = '$cuenta' AND fecha BETWEEN '$fechaIni' AND '$fechaFin' AND vigencia='$vigencia' AND estado!='N' AND mediopago = '".$row[1]."' AND tipo='C' AND productoservicio = '' AND fuente='".$row[0]."'";
            $resCredito = mysqli_query($linkbd_V7, $sqlrCredito);
            $rowCredito = mysqli_fetch_row($resCredito);

            $credito = $rowCredito[0];

            //============= AQUI FINALIZA EL PROCESO PARA CALCULAR EL CREDITO =========//

            //============= AQUI INICIA EL PROCESO PARA CALCULAR EL CONTRACREDITO =========//

            $sqlrContracreditoCredito = "SELECT SUM(valor) FROM ccpettraslados WHERE cuenta = '$cuenta' AND fecha BETWEEN '$fechaIni' AND '$fechaFin' AND vigencia='$vigencia' AND estado!='N' AND mediopago = '".$row[1]."' AND tipo='R' AND productoservicio = '' AND fuente='".$row[0]."'";
            $resContracreditoCredito = mysqli_query($linkbd_V7, $sqlrContracreditoCredito);
            $rowContracreditoCredito = mysqli_fetch_row($resContracreditoCredito);

            $contracredito = $rowContracreditoCredito[0];

            //============= AQUI FINALIZA EL PROCESO PARA CALCULAR EL CONTRACREDITO =========//

            //============= AQUI INICIA EL PROCESO PARA CALCULAR EL VALOR DE LOS CDPS =========//


            $sqlrCdp = "SELECT C.consvigencia, C.tipo_mov,D.valor,D.tipo_mov FROM ccpetcdp C, ccpetcdp_detalle D WHERE D.cuenta = '$cuenta' AND D.vigencia='$vigencia' AND D.consvigencia=C.consvigencia AND C.vigencia='$vigencia' AND NOT(D.estado='N') AND D.valor>0 AND D.tipo_mov=C.tipo_mov AND D.tipo_mov='201' AND C.fecha BETWEEN '$fechaIni' AND '$fechaFin' AND D.fuente = '".$row[0]."' AND D.productoservicio = '' AND D.medio_pago = '".$row[1]."' UNION SELECT C.consvigencia, C.tipo_mov,SUM(D.valor),D.tipo_mov FROM ccpetcdp C, ccpetcdp_detalle D WHERE D.cuenta = '$cuenta' AND D.vigencia='$vigencia' AND D.consvigencia=C.consvigencia AND C.vigencia='$vigencia' AND NOT(D.estado='N') AND D.valor>0 AND D.tipo_mov=C.tipo_mov AND (D.tipo_mov='401' OR D.tipo_mov='402') AND D.fuente = '".$row[0]."' AND D.productoservicio = '' AND D.medio_pago = '".$row[1]."' AND EXISTS(SELECT 1 FROM ccpetcdp DAUX WHERE DAUX.consvigencia=D.consvigencia AND DAUX.tipo_mov='201' AND DAUX.fecha BETWEEN '$fechaIni' AND '$fechaFin')";
            
            $resCdp = mysqli_query($linkbd_V7, $sqlrCdp);
            if(mysqli_num_rows($resCdp)!=0)
            {
                while($rowCdp = mysqli_fetch_row($resCdp))
                {
                    if($rowCdp[3]=='201')
                    {
                        $totalCDPEnt+=round($rowCdp[2],2);
                    }
                    else if( substr($rowCdp[3],0,1) == '4' )
                    {
                        $totalCDPEnt-=round($rowCdp[2],2);
                    }
                }
            }

            //============= AQUI FINALIZA EL PROCESO PARA CALCULAR EL VALOR DE LOS CDPS =========//

            //============= AQUI INICIA EL PROCESO PARA CALCULAR EL VALOR DE LOS RPS =========//


            $sqlrRp = "SELECT R.consvigencia,R.fecha,R.tipo_mov,RD.valor,RD.tipo_mov FROM ccpetrp R,ccpetrp_detalle RD where  R.vigencia='$vigencia' AND RD.cuenta = '$cuenta' AND RD.consvigencia=R.consvigencia AND RD.vigencia='$vigencia'  AND NOT(R.estado='N') AND R.tipo_mov=RD.tipo_mov AND R.tipo_mov='201' AND RD.valor>0 AND R.fecha BETWEEN '$fechaIni' AND '$fechaFin' AND RD.fuente = '".$row[0]."' AND RD.productoservicio = '' AND RD.medio_pago = '".$row[1]."' UNION SELECT R.consvigencia,R.fecha,R.tipo_mov,SUM(RD.valor),RD.tipo_mov FROM ccpetrp R,ccpetrp_detalle RD where  R.vigencia='$vigencia' AND RD.cuenta = '$cuenta' AND RD.consvigencia=R.consvigencia AND RD.vigencia='$vigencia'  AND NOT(R.estado='N') AND R.tipo_mov=RD.tipo_mov AND (R.tipo_mov='401' OR R.tipo_mov='402') AND RD.valor>0 AND EXISTS(SELECT 1 FROM ccpetrp RAUX WHERE RAUX.consvigencia=R.consvigencia AND RAUX.tipo_mov='201' AND RAUX.fecha BETWEEN '$fechaIni' AND '$fechaFin') AND RD.fuente = '".$row[0]."' AND RD.productoservicio = '' AND RD.medio_pago = '".$row[1]."'";
            //echo $sqlrRp."<br>";
            $resRp = mysqli_query($linkbd_V7, $sqlrRp);
            if(mysqli_num_rows($resRp)!=0)
            {
                while($rowRp = mysqli_fetch_row($resRp))
                {
                    if( $rowRp[4]=='201')
                    {
                        $totalRPEnt+=$rowRp[3];
                        $arregloRP[]=$rowRp[0];
                    }
                    else if( substr($rowRp[4],0,1) == '4')
                    {
                        $totalRPEnt-=$rowRp[3];
                    }
                }
            }

            //============= AQUI FINALIZA EL PROCESO PARA CALCULAR EL VALOR DE LOS RPS =========//

            //============= AQUI INICIA EL PROCESO PARA CALCULAR EL VALOR DE LAS CXP =========//


            $sqlrCxp = "SELECT T.id_orden,T.fecha,T.tipo_mov,TD.valor,T.estado,TD.tipo_mov FROM tesoordenpago T,tesoordenpago_det TD WHERE T.vigencia=$vigencia  AND T.id_orden=TD.id_orden AND NOT(T.estado='N') AND TD.valor>0 AND TD.cuentap = '$cuenta' AND T.tipo_mov=TD.tipo_mov AND T.tipo_mov='201' AND T.fecha BETWEEN '$fechaIni' AND '$fechaFin'  AND TD.productoservicio = '' AND TD.fuente = '".$row[0]."' AND TD.medio_pago = '".$row[1]."' UNION SELECT T.id_orden,T.fecha,T.tipo_mov,TD.valor,T.estado,TD.tipo_mov FROM tesoordenpago T,tesoordenpago_det TD WHERE T.vigencia=$vigencia  AND T.id_orden=TD.id_orden AND NOT(T.estado='N') AND TD.valor>0 AND TD.cuentap = '$cuenta' AND T.tipo_mov=TD.tipo_mov AND (T.tipo_mov='401' OR T.tipo_mov='402') AND EXISTS(SELECT 1 FROM tesoordenpago TAUX WHERE TAUX.id_orden=T.id_orden AND TAUX.tipo_mov='201' AND TAUX.fecha BETWEEN '$fechaIni' AND '$fechaFin')  AND TD.productoservicio = '' AND TD.fuente = '".$row[0]."' AND TD.medio_pago = '".$row[1]."'";

            $resCxp = mysqli_query($linkbd_V7, $sqlrCxp);
            if(mysqli_num_rows($resCxp)!=0)
            {
                while($rowCxp = mysqli_fetch_row($resCxp))
                {
                    if($rowCxp[5]=='201')
                    {
                        $totalCxPEnt+=$rowCxp[3];
                    }
                    else if($rowCxp[5]=='401')
                    {
                        $totalCxPEnt-=$rowCxp[3];
                    }
                }
            }

            for ($i=0; $i <sizeof($arregloRP); $i++) 
            {
                $sqlrCxpNomina = "SELECT HN.id_nom,HN.periodo,HNP.valor,HN.fecha FROM hum_nom_cdp_rp HNR,humnom_presupuestal HNP,humnomina HN WHERE HNR.rp=$arregloRP[$i] AND HNR.nomina=HNP.id_nom AND HNP.cuenta = '$cuenta' AND HNR.vigencia='$vigencia' AND  NOT(HNR.estado='N' OR HNR.estado='R') AND HNP.valor>0 AND HN.id_nom=HNR.nomina AND HN.fecha BETWEEN '$fechaIni' AND '$fechaFin' AND HNP.medio_pago = '".$row[1]."' AND HNP.fuente = '".$row[0]."'";
                
                $resCxpNomina = mysqli_query($linkbd_V7, $sqlrCxpNomina);

                if(mysqli_num_rows($resCxpNomina)==0)
                    echo "";
                else
                {
                    while($rowCxpNomina = mysqli_fetch_row($resCxpNomina))
                    {
                        $sqlrEgresoNomina = "SELECT TEND.valordevengado FROM tesoegresosnomina TEN,tesoegresosnomina_det TEND WHERE  TEN.id_orden=".$rowCxpNomina[0]." AND TEN.vigencia='$vigencia' AND NOT(TEN.estado='N' OR TEN.estado='R')  AND TEN.fecha BETWEEN '$fechaIni' AND '$fechaFin' AND TEN.id_egreso=TEND.id_egreso AND NOT(TEND.tipo='SE' OR TEND.tipo='PE' OR TEND.tipo='DS' OR TEND.tipo='RE' OR TEND.tipo='FS') AND TEND.cuentap LIKE '$cuenta%' AND TEND.fuente = '".$row[0]."' ";
                        
                        $resEgresoNomina = mysqli_query($linkbd_V7, $sqlrEgresoNomina);
                        if(mysqli_num_rows($resEgresoNomina)!=0)
                        {
                            while($rowEgresoNomina = mysqli_fetch_row($resEgresoNomina))
                            {
                                $totalEgresoEnt+=$rowEgresoNomina[0];
                            }
                        }
                        $totalCxPEnt+=$rowCxpNomina[2];
                        //$totalEgresoEnt+=$rowEgresoNomina[0];
                    }
                }

            }

            //============= AQUI FINALIZA EL PROCESO PARA CALCULAR EL VALOR DE LAS CXP =========//

            //============= AQUI INICIA EL PROCESO PARA CALCULAR EL VALOR DE LOS EGRESOS =========//


            $sqlrEgreso = "SELECT TE.id_egreso,TE.fecha,TE.tipo_mov,TD.valor FROM tesoegresos TE,tesoordenpago_det TD, tesoordenpago TIA where TIA.vigencia='$vigencia' AND TIA.id_orden=TD.id_orden AND TD.cuentap = '$cuenta' AND TE.id_orden=TD.id_orden AND  NOT(TE.estado='N') AND TD.valor >0 AND TE.tipo_mov='201' AND TE.fecha BETWEEN '$fechaIni' AND '$fechaFin' AND TD.productoservicio = '' AND TD.fuente = '".$row[0]."' AND TD.medio_pago = '".$row[1]."' UNION SELECT TE.id_egreso,TE.fecha,TE.tipo_mov,TD.valor FROM tesoegresos TE,tesoordenpago_det TD,tesoordenpago TIO where TIO.vigencia='$vigencia' AND TIO.id_orden=TD.id_orden AND TD.cuentap = '$cuenta' AND TE.id_orden=TD.id_orden AND  NOT(TE.estado='N') AND TD.valor >0 AND TE.tipo_mov='401'  AND EXISTS(SELECT 1 FROM tesoegresos TEAUX WHERE TEAUX.id_egreso=TE.id_egreso AND TEAUX.tipo_mov='201' AND TEAUX.fecha BETWEEN '$fechaIni' AND '$fechaFin') AND TD.productoservicio = '' AND TD.fuente = '".$row[0]."' AND TD.medio_pago = '".$row[1]."'";

            $resEgreso = mysqli_query($linkbd_V7, $sqlrEgreso);
            if(mysqli_num_rows($resEgreso)!=0)
            {
                while($rowEgreso = mysqli_fetch_row($resEgreso))
                {
                    if($rowEgreso[2]=='201')
                    {
                        $totalEgresoEnt+=$rowEgreso[3];
                    }
                    else if($rowEgreso[2]=='401')
                    {
                        $totalEgresoEnt-=$rowEgreso[3];
                    }
                }
            }

            //============= AQUI FINALIZA EL PROCESO PARA CALCULAR EL VALOR DE LOS EGRESOS =========//




            /* $sqlr_cdp = "SELECT sum(D.valor) FROM ccpetcdp AS C, ccpetcdp_detalle AS D WHERE C.consvigencia = D.consvigencia AND C.vigencia = D.vigencia AND D.cuenta='$cuenta' AND C.fecha BETWEEN '$fechaIni' AND '$fechaFin' AND D.tipo_mov = '201' AND D.fuente = '".$row[0]."' AND D.productoservicio = '' AND D.medio_pago='".$row[1]."'";

            //$sqlr_cdp = "SELECT sum(valor) FROM ccpetcdp_detalle WHERE cuenta='$cuenta' AND fuente = '".$row[0]."' AND productoservicio = '' AND medio_pago='".$row[1]."'";
            $res_cdp = mysqli_query($linkbd, $sqlr_cdp);
            $row_cdp = mysqli_fetch_array($res_cdp); 

            $sqlr_rp = "SELECT sum(D.valor) FROM ccpetrp AS C, ccpetrp_detalle AS D WHERE C.consvigencia = D.consvigencia AND C.vigencia = D.vigencia AND D.cuenta='$cuenta' AND C.fecha BETWEEN '$fechaIni' AND '$fechaFin' AND D.tipo_mov = '201' AND D.fuente = '".$row[0]."' AND D.productoservicio = '' AND D.medio_pago='".$row[1]."'";

            //$sqlr_rp = "SELECT sum(valor) FROM ccpetrp_detalle WHERE cuenta='$cuenta' AND fuente = '".$row[0]."' AND productoservicio = '' AND medio_pago='".$row[1]."'";
            $res_rp = mysqli_query($linkbd, $sqlr_rp);
            $row_rp = mysqli_fetch_array($res_rp);

            $sqlr_cxp = "SELECT sum(D.valor) FROM tesoordenpago AS C, tesoordenpago_det AS D WHERE C.id_orden =  D.id_orden AND D.cuentap = '$cuenta' AND C.fecha BETWEEN '$fechaIni' AND '$fechaFin' AND  NOT(C.estado='N' OR C.estado='R') AND  C.tipo_mov = '201' AND D.productoservicio = '' AND D.fuente = '".$row[0]."' AND D.medio_pago='".$row[1]."'";

            //$sqlr_cxp = "SELECT sum(valor) FROM tesoordenpago_det WHERE cuentap='$cuenta' AND productoservicio = '' AND fuente = '".$row[0]."' AND medio_pago='".$row[1]."'";
            $res_cxp = mysqli_query($linkbd, $sqlr_cxp);
            $row_cxp = mysqli_fetch_array($res_cxp);

            $sqlr_cxp_nom = "SELECT sum(D.valor) FROM humnomina AS C, humnom_presupuestal AS D WHERE C.id_nom = D.id_nom AND C.fecha BETWEEN '$fechaIni' AND '$fechaFin' AND D.cuenta = '$cuenta' AND D.fuente = '".$row[0]."' AND C.estado!='N'";//echo $sqlr_cxp_nom."<br>";
            $res_cxp_nom = mysqli_query($linkbd, $sqlr_cxp_nom);
            $row_cxp_nom = mysqli_fetch_row($res_cxp_nom);

            $valorCxp = $row_cxp[0] + $row_cxp_nom[0];

            $sqlr_egreso = "SELECT sum(TD.valor) FROM tesoordenpago T, tesoordenpago_det TD, tesoegresos TE WHERE TE.id_orden=T.id_orden AND T.id_orden=TD.id_orden AND TD.cuentap = '$cuenta' AND T.fecha BETWEEN '$fechaIni' AND '$fechaFin' AND  NOT(TE.estado='N' OR TE.estado='R') AND TD.valor >0 AND TE.tipo_mov='201' AND TD.productoservicio = '' AND TD.fuente = '".$row[0]."' AND TD.medio_pago='".$row[1]."'";

           // $sqlr_egreso = "SELECT sum(valor) FROM tesoordenpago T, tesoordenpago_det TD, tesoegresos TE WHERE TE.id_orden=T.id_orden AND T.id_orden=TD.id_orden AND TD.cuentap = '$cuenta' AND TE.vigencia='$vigencia' AND TD.vigencia='$vigencia' AND  NOT(TE.estado='N') AND TD.valor >0 AND TE.tipo_mov='201' AND TD.productoservicio = '' AND TD.fuente = '".$row[0]."' AND TD.medio_pago='".$row[1]."'";
            $res_egreso = mysqli_query($linkbd, $sqlr_egreso);
            $row_egreso = mysqli_fetch_row($res_egreso);

            $sqlr_egreso_nom = "SELECT sum(D.valordevengado) FROM tesoegresosnomina AS C, tesoegresosnomina_det AS D WHERE C.id_egreso = D.id_egreso AND C.fecha BETWEEN '$fechaIni' AND '$fechaFin' AND NOT(D.tipo='SE' OR D.tipo='PE' OR D.tipo='DS' OR D.tipo='RE') AND D.cuentap = '$cuenta'";
            $res_egreso_nom = mysqli_query($linkbd, $sqlr_egreso_nom);
            $row_egreso_nom = mysqli_fetch_row($res_egreso_nom);

            $valorEgreso = $row_egreso[0] + $row_egreso_nom[0];*/

            if($row[2] == 'I' || $row[2] == 'G' || $row[2] == 'C' || $row[2] == 'R')
            {
                $row[3] = 0;
            }

            $presupuestoDefinitivo = $row[3] + $adicion - $reduccion + $credito - $contracredito;

		    $saldo = $presupuestoDefinitivo - $totalCDPEnt;

            $tabla.='<tr>
                        <td class="cssdeta">'.$row[0].' - '.$row_fuente[0].'</td>
                        <td class="cssdeta" >'.$medioPago.'</td>
                        <td class="cssdeta" >'.$row[2].' - '.$row_unidad[0].'</td>
                        <td class="cssdeta" >'.number_format($row[3], 2, ',', '.').'</td>
                        <td class="cssdeta" >'.number_format($adicion, 2, ',', '.').'</td>
                        <td class="cssdeta" >'.number_format($reduccion, 2, ',', '.').'</td>
                        <td class="cssdeta" >'.number_format($credito, 2, ',', '.').'</td>
                        <td class="cssdeta" >'.number_format($contracredito, 2, ',', '.').'</td>
                        <td class="cssdeta" >'.number_format($presupuestoDefinitivo, 2, ',', '.').'</td>
                        <td class="cssdeta" >'.number_format($totalCDPEnt, 2, ',', '.').'</td>
                        <td class="cssdeta" >'.number_format($totalRPEnt, 2, ',', '.').'</td>
                        <td class="cssdeta" >'.number_format($totalCxPEnt, 2, ',', '.').'</td>
                        <td class="cssdeta" >'.number_format($totalEgresoEnt, 2, ',', '.').'</td>
                        <td class="cssdeta" >'.number_format($saldo, 2, ',', '.').'</td>
                    </tr>
                    ';
        }
    }

    $sqlr = "SELECT CB.fuente, CB.medioPago, CB.subclase, CB.unidad, CB.valor FROM ccpetinicialigastosbienestransportables AS CB WHERE CB.cuenta = '$cuenta' AND CB.vigencia='$vigencia' $critBienesTransportables $crit1BienesTransportables UNION SELECT CA.fuente, CA.mediopago, CA.cuenta_clasificadora, CA.tipo, CA.valor FROM ccpetadiciones AS CA WHERE CA.cuenta = '$cuenta' AND CA.vigencia='$vigencia' AND LEFT(CA.cuenta, 3)!='2.3' AND CA.clasificador='2' AND CA.cuenta_clasificadora!='' AND NOT EXISTS(SELECT 1 FROM ccpetinicialigastosbienestransportables AS CBT WHERE CBT.cuenta = '$cuenta' AND CBT.vigencia='$vigencia' AND CA.cuenta_clasificadora = CBT.subclase AND CA.fuente = CBT.fuente) UNION SELECT CT.fuente, CT.mediopago, CT.productoservicio, CT.tipo, CT.valor FROM ccpettraslados AS CT WHERE CT.cuenta = '$cuenta' AND CT.vigencia='$vigencia' AND CT.productoservicio!='' AND LENGTH(CT.productoservicio)=7 AND LEFT(CT.cuenta, 3)!='2.3' AND NOT EXISTS(SELECT 1 FROM ccpetinicialigastosbienestransportables AS CTT WHERE CTT.cuenta = '$cuenta' AND CT.productoservicio = CTT.subclase AND CTT.vigencia='$vigencia' AND CT.fuente = CTT.fuente) AND NOT EXISTS(SELECT 1 FROM ccpetadiciones AS CAT WHERE CAT.cuenta = '$cuenta' AND CT.productoservicio = CAT.cuenta_clasificadora AND CAT.vigencia='$vigencia' AND CT.fuente = CAT.fuente) GROUP BY CT.fuente, CT.mediopago, CT.productoservicio";
    $result = mysqli_query($linkbd, $sqlr);
    if(mysqli_num_rows($result) > 0){
        $tabla.='<tr>
                    <td>Fuente</td>
                    <td>MedioPago</td>
                    <td>subClase</td>
                    <td>Unidad</td>
                    <td>Inicial</td>
                    <td>Adicion</td>
                    <td>Reduccion</td>
                    <td>Credito</td>
                    <td>Contracredito</td>
                    <td>Definitivo</td>
                    <td>Disponibilidad</td>
                    <td>Compromiso</td>
                    <td>Obligacion</td>
                    <td>Pago</td>
                    <td>Saldo</td>
                </tr>';

        while($row = mysqli_fetch_array($result)){

            $inicial = 0;
            $adicion = 0;
            $reduccion = 0;
            $credito = 0;
            $contracredito = 0;
            $presupuestoDefinitivo = 0;
            $totalCDPEnt = 0;
            $totalRPEnt = 0;
            $totalCxPEnt = 0;
            $totalEgresoEnt = 0;
            $saldo = 0;



            $sqlr_fuente = "SELECT fuente_financiacion FROM ccpet_fuentes WHERE id_fuente = '$row[0]'";
            $result_fuente = mysqli_query($linkbd, $sqlr_fuente);
            $row_fuente = mysqli_fetch_array($result_fuente);

            $sqlr_bienes = "SELECT titulo FROM ccpetbienestransportables WHERE grupo = '$row[2]'";
            $result_bienes = mysqli_query($linkbd, $sqlr_bienes);
            $row_bienes = mysqli_fetch_array($result_bienes);

            $sqlr_unidad = "SELECT nombre FROM pptouniejecu WHERE id_cc = '$row[3]'";
            $result_unidad = mysqli_query($linkbd, $sqlr_unidad);
            $row_unidad = mysqli_fetch_array($result_unidad);

            if($row[1] == 'CSF'){
                $medioPago = 'Con situacion de fondos';
            }else{
                $medioPago = 'Sin situacion de fondos';
            }

            //============= AQUI INICIA EL PROCESO PARA CALCULAR LA ADICION =========//


            $sqlrAdicion = "SELECT SUM(valor) FROM ccpetadiciones WHERE cuenta = '$cuenta' AND fecha BETWEEN '$fechaIni' AND '$fechaFin' AND vigencia='$vigencia' AND estado!='N' AND mediopago = '".$row[1]."' AND fuente = '".$row[0]."' AND cuenta_clasificadora = '".$row[2]."'";
            $resAdicion = mysqli_query($linkbd_V7, $sqlrAdicion);
            $rowAdicion = mysqli_fetch_row($resAdicion);

            $adicion = $rowAdicion[0];

            //============= AQUI FINALIZA EL PROCESO PARA CALCULAR LA ADICION =========//

            //============= AQUI INICIA EL PROCESO PARA CALCULAR LA REDUCCION =========//

            $sqlrReduccion = "SELECT SUM(valor) FROM ccpetreducciones WHERE cuenta = '$cuenta' AND fecha BETWEEN '$fechaIni' AND '$fechaFin' AND vigencia='$vigencia' AND estado!='N' AND mediopago = '".$row[1]."' AND fuente = '".$row[0]."' AND cuenta_clasificadora = '".$row[2]."'";
            $resReduccion = mysqli_query($linkbd_V7, $sqlrReduccion);
            $rowReduccion = mysqli_fetch_row($resReduccion);

            $reduccion = $rowReduccion[0];

            $sqlrReduccion_n = "SELECT SUM(CR.valor) FROM ccpetreduccion AS CR, ccpetacuerdos AS CAC WHERE CR.rubro = '$cuenta' AND CR.acuerdo = CAC.id_acuerdo AND CAC.fecha BETWEEN '$fechaIni' AND '$fechaFin' AND CR.vigencia='$vigencia' AND CR.medio_pago = '".$row[1]."' AND CR.fuente = '".$row[0]."' AND CR.clasificador = '".$row[2]."'";
            $resReduccion_n = mysqli_query($linkbd_V7, $sqlrReduccion_n);
            $rowReduccion_n = mysqli_fetch_row($resReduccion_n);

            $reduccion += $rowReduccion_n[0];

            //============= AQUI FINALIZA EL PROCESO PARA CALCULAR LA REDUCCION =========//


            //============= AQUI INICIA EL PROCESO PARA CALCULAR EL CREDITO =========//
            
            $sqlrCredito = "SELECT SUM(valor) FROM ccpettraslados WHERE cuenta = '$cuenta' AND fecha BETWEEN '$fechaIni' AND '$fechaFin' AND vigencia='$vigencia' AND estado!='N' AND mediopago = '".$row[1]."' AND tipo='C' AND productoservicio = '".$row[2]."' AND fuente='".$row[0]."'";
            $resCredito = mysqli_query($linkbd_V7, $sqlrCredito);
            $rowCredito = mysqli_fetch_row($resCredito);

            $credito = $rowCredito[0];

            //============= AQUI FINALIZA EL PROCESO PARA CALCULAR EL CREDITO =========//

            //============= AQUI INICIA EL PROCESO PARA CALCULAR EL CONTRACREDITO =========//

            $sqlrContracreditoCredito = "SELECT SUM(valor) FROM ccpettraslados WHERE cuenta = '$cuenta' AND fecha BETWEEN '$fechaIni' AND '$fechaFin' AND vigencia='$vigencia' AND estado!='N' AND mediopago = '".$row[1]."' AND tipo='R' AND productoservicio = '".$row[2]."' AND fuente='".$row[0]."'";
            $resContracreditoCredito = mysqli_query($linkbd_V7, $sqlrContracreditoCredito);
            $rowContracreditoCredito = mysqli_fetch_row($resContracreditoCredito);

            $contracredito = $rowContracreditoCredito[0];

            //============= AQUI FINALIZA EL PROCESO PARA CALCULAR EL CONTRACREDITO =========//

            //============= AQUI INICIA EL PROCESO PARA CALCULAR EL VALOR DE LOS CDPS =========//


            $sqlrCdp = "SELECT C.consvigencia, C.tipo_mov,D.valor,D.tipo_mov FROM ccpetcdp C, ccpetcdp_detalle D WHERE D.cuenta = '$cuenta' AND D.vigencia='$vigencia' AND D.consvigencia=C.consvigencia AND C.vigencia='$vigencia' AND NOT(D.estado='N') AND D.valor>0 AND D.tipo_mov=C.tipo_mov AND D.tipo_mov='201' AND C.fecha BETWEEN '$fechaIni' AND '$fechaFin' AND D.fuente = '".$row[0]."' AND D.productoservicio = '".$row[2]."' AND D.medio_pago = '".$row[1]."' UNION SELECT C.consvigencia, C.tipo_mov,SUM(D.valor),D.tipo_mov FROM ccpetcdp C, ccpetcdp_detalle D WHERE D.cuenta = '$cuenta' AND D.vigencia='$vigencia' AND D.consvigencia=C.consvigencia AND C.vigencia='$vigencia' AND NOT(D.estado='N') AND D.valor>0 AND D.tipo_mov=C.tipo_mov AND (D.tipo_mov='401' OR D.tipo_mov='402') AND D.fuente = '".$row[0]."' AND D.productoservicio = '".$row[2]."' AND D.medio_pago = '".$row[1]."' AND EXISTS(SELECT 1 FROM ccpetcdp DAUX WHERE DAUX.consvigencia=D.consvigencia AND DAUX.tipo_mov='201' AND DAUX.fecha BETWEEN '$fechaIni' AND '$fechaFin')";
            
            $resCdp = mysqli_query($linkbd_V7, $sqlrCdp);
            if(mysqli_num_rows($resCdp)!=0)
            {
                while($rowCdp = mysqli_fetch_row($resCdp))
                {
                    if($rowCdp[3]=='201')
                    {
                        $totalCDPEnt+=round($rowCdp[2],2);
                    }
                    else if( substr($rowCdp[3],0,1) == '4' )
                    {
                        $totalCDPEnt-=round($rowCdp[2],2);
                    }
                }
            }

            //============= AQUI FINALIZA EL PROCESO PARA CALCULAR EL VALOR DE LOS CDPS =========//

            //============= AQUI INICIA EL PROCESO PARA CALCULAR EL VALOR DE LOS RPS =========//

            
            $sqlrRp = "SELECT R.consvigencia,R.fecha,R.tipo_mov,RD.valor,RD.tipo_mov FROM ccpetrp R,ccpetrp_detalle RD where  R.vigencia='$vigencia' AND RD.cuenta = '$cuenta' AND RD.consvigencia=R.consvigencia AND RD.vigencia='$vigencia'  AND NOT(R.estado='N') AND R.tipo_mov=RD.tipo_mov AND R.tipo_mov='201' AND RD.valor>0 AND R.fecha BETWEEN '$fechaIni' AND '$fechaFin' AND RD.fuente = '".$row[0]."' AND RD.productoservicio = '".$row[2]."' AND RD.medio_pago = '".$row[1]."' UNION SELECT R.consvigencia,R.fecha,R.tipo_mov,SUM(RD.valor),RD.tipo_mov FROM ccpetrp R,ccpetrp_detalle RD where  R.vigencia='$vigencia' AND RD.cuenta = '$cuenta' AND RD.consvigencia=R.consvigencia AND RD.vigencia='$vigencia'  AND NOT(R.estado='N') AND R.tipo_mov=RD.tipo_mov AND (R.tipo_mov='401' OR R.tipo_mov='402') AND RD.valor>0 AND EXISTS(SELECT 1 FROM ccpetrp RAUX WHERE RAUX.consvigencia=R.consvigencia AND RAUX.tipo_mov='201' AND RAUX.fecha BETWEEN '$fechaIni' AND '$fechaFin') AND RD.fuente = '".$row[0]."' AND RD.productoservicio = '".$row[2]."' AND RD.medio_pago = '".$row[1]."'";
            //echo $sqlrRp."<br>";
            $resRp = mysqli_query($linkbd_V7, $sqlrRp);
            if(mysqli_num_rows($resRp)!=0)
            {
                while($rowRp = mysqli_fetch_row($resRp))
                {
                    if( $rowRp[4]=='201')
                    {
                        $totalRPEnt+=$rowRp[3];
                        $arregloRP[]=$rowRp[0];
                    }
                    else if( substr($rowRp[4],0,1) == '4')
                    {
                        $totalRPEnt-=$rowRp[3];
                    }
                }
            }

            //============= AQUI FINALIZA EL PROCESO PARA CALCULAR EL VALOR DE LOS RPS =========//

            //============= AQUI INICIA EL PROCESO PARA CALCULAR EL VALOR DE LAS CXP =========//


            $sqlrCxp = "SELECT T.id_orden,T.fecha,T.tipo_mov,TD.valor,T.estado,TD.tipo_mov FROM tesoordenpago T,tesoordenpago_det TD WHERE T.vigencia=$vigencia  AND T.id_orden=TD.id_orden AND NOT(T.estado='N') AND TD.valor>0 AND TD.cuentap = '$cuenta' AND T.tipo_mov=TD.tipo_mov AND T.tipo_mov='201' AND T.fecha BETWEEN '$fechaIni' AND '$fechaFin'  AND TD.productoservicio = '".$row[2]."' AND TD.fuente = '".$row[0]."' AND TD.medio_pago = '".$row[1]."' UNION SELECT T.id_orden,T.fecha,T.tipo_mov,TD.valor,T.estado,TD.tipo_mov FROM tesoordenpago T,tesoordenpago_det TD WHERE T.vigencia=$vigencia  AND T.id_orden=TD.id_orden AND NOT(T.estado='N') AND TD.valor>0 AND TD.cuentap = '$cuenta' AND T.tipo_mov=TD.tipo_mov AND (T.tipo_mov='401' OR T.tipo_mov='402') AND EXISTS(SELECT 1 FROM tesoordenpago TAUX WHERE TAUX.id_orden=T.id_orden AND TAUX.tipo_mov='201' AND TAUX.fecha BETWEEN '$fechaIni' AND '$fechaFin')  AND TD.productoservicio = '".$row[2]."' AND TD.fuente = '".$row[0]."' AND TD.medio_pago = '".$row[1]."'";

            $resCxp = mysqli_query($linkbd_V7, $sqlrCxp);
            if(mysqli_num_rows($resCxp)!=0)
            {
                while($rowCxp = mysqli_fetch_row($resCxp))
                {
                    if($rowCxp[5]=='201')
                    {
                        $totalCxPEnt+=$rowCxp[3];
                    }
                    else if($rowCxp[5]=='401')
                    {
                        $totalCxPEnt-=$rowCxp[3];
                    }
                }
            }

            for ($i=0; $i <sizeof($arregloRP); $i++) 
            {
                $sqlrCxpNomina = "SELECT HN.id_nom,HN.periodo,HNP.valor,HN.fecha FROM hum_nom_cdp_rp HNR,humnom_presupuestal HNP,humnomina HN WHERE HNR.rp=$arregloRP[$i] AND HNR.nomina=HNP.id_nom AND HNP.cuenta = '$cuenta' AND HNR.vigencia='$vigencia' AND  NOT(HNR.estado='N' OR HNR.estado='R') AND HNP.valor>0 AND HN.id_nom=HNR.nomina AND HN.fecha BETWEEN '$fechaIni' AND '$fechaFin' AND HNP.medio_pago = '".$row[1]."' AND HNP.fuente = '".$row[0]."'";
                
                $resCxpNomina = mysqli_query($linkbd_V7, $sqlrCxpNomina);

                if(mysqli_num_rows($resCxpNomina)==0)
                    echo "";
                else
                {
                    while($rowCxpNomina = mysqli_fetch_row($resCxpNomina))
                    {
                        $sqlrEgresoNomina = "SELECT TEND.valordevengado FROM tesoegresosnomina TEN,tesoegresosnomina_det TEND WHERE  TEN.id_orden=".$rowCxpNomina[0]." AND TEN.vigencia='$vigencia' AND NOT(TEN.estado='N' OR TEN.estado='R')  AND TEN.fecha BETWEEN '$fechaIni' AND '$fechaFin' AND TEN.id_egreso=TEND.id_egreso AND NOT(TEND.tipo='SE' OR TEND.tipo='PE' OR TEND.tipo='DS' OR TEND.tipo='RE' OR TEND.tipo='FS') AND TEND.cuentap LIKE '$cuenta%' AND TEND.fuente = '".$row[0]."%' ";
                        
                        $resEgresoNomina = mysqli_query($linkbd_V7, $sqlrEgresoNomina);
                        if(mysqli_num_rows($resEgresoNomina)!=0)
                        {
                            while($rowEgresoNomina = mysqli_fetch_row($resEgresoNomina))
                            {
                                $totalEgresoEnt+=$rowEgresoNomina[0];
                            }
                        }
                        $totalCxPEnt+=$rowCxpNomina[2];
                        //$totalEgresoEnt+=$rowEgresoNomina[0];
                    }
                }

            }

            //============= AQUI FINALIZA EL PROCESO PARA CALCULAR EL VALOR DE LAS CXP =========//

            //============= AQUI INICIA EL PROCESO PARA CALCULAR EL VALOR DE LOS EGRESOS =========//


            $sqlrEgreso = "SELECT TE.id_egreso,TE.fecha,TE.tipo_mov,TD.valor FROM tesoegresos TE,tesoordenpago_det TD, tesoordenpago TIA where TIA.vigencia='$vigencia' AND TIA.id_orden=TD.id_orden AND TD.cuentap = '$cuenta' AND TE.id_orden=TD.id_orden AND  NOT(TE.estado='N') AND TD.valor >0 AND TE.tipo_mov='201' AND TE.fecha BETWEEN '$fechaIni' AND '$fechaFin' AND TD.productoservicio = '".$row[2]."' AND TD.fuente = '".$row[0]."' AND TD.medio_pago = '".$row[1]."' UNION SELECT TE.id_egreso,TE.fecha,TE.tipo_mov,TD.valor FROM tesoegresos TE,tesoordenpago_det TD,tesoordenpago TIO where TIO.vigencia='$vigencia' AND TIO.id_orden=TD.id_orden AND TD.cuentap = '$cuenta' AND TE.id_orden=TD.id_orden AND  NOT(TE.estado='N') AND TD.valor >0 AND TE.tipo_mov='401'  AND EXISTS(SELECT 1 FROM tesoegresos TEAUX WHERE TEAUX.id_egreso=TE.id_egreso AND TEAUX.tipo_mov='201' AND TEAUX.fecha BETWEEN '$fechaIni' AND '$fechaFin') AND TD.productoservicio = '".$row[2]."' AND TD.fuente = '".$row[0]."' AND TD.medio_pago = '".$row[1]."'";

            $resEgreso = mysqli_query($linkbd_V7, $sqlrEgreso);
            if(mysqli_num_rows($resEgreso)!=0)
            {
                while($rowEgreso = mysqli_fetch_row($resEgreso))
                {
                    if($rowEgreso[2]=='201')
                    {
                        $totalEgresoEnt+=$rowEgreso[3];
                    }
                    else if($rowEgreso[2]=='401')
                    {
                        $totalEgresoEnt-=$rowEgreso[3];
                    }
                }
            }

            //============= AQUI FINALIZA EL PROCESO PARA CALCULAR EL VALOR DE LOS EGRESOS =========//

            /* $sqlr_cdp = "SELECT sum(D.valor) FROM ccpetcdp AS C, ccpetcdp_detalle AS D WHERE C.consvigencia = D.consvigencia AND C.vigencia = D.vigencia AND D.cuenta='$cuenta' AND C.fecha BETWEEN '$fechaIni' AND '$fechaFin' AND D.tipo_mov = '201' AND D.fuente = '".$row[0]."' AND D.productoservicio = ".$row[2]." AND D.medio_pago='".$row[1]."'";

            //$sqlr_cdp = "SELECT sum(valor) FROM ccpetcdp_detalle WHERE cuenta='$cuenta' AND productoservicio = ".$row[2]." AND fuente = '".$row[0]."' AND medio_pago='".$row[1]."'";
            $res_cdp = mysqli_query($linkbd, $sqlr_cdp);
            $row_cdp = mysqli_fetch_array($res_cdp);

            $sqlr_rp = "SELECT sum(D.valor) FROM ccpetrp AS C, ccpetrp_detalle AS D WHERE C.consvigencia = D.consvigencia AND C.vigencia = D.vigencia AND D.cuenta='$cuenta' AND C.fecha BETWEEN '$fechaIni' AND '$fechaFin' AND D.tipo_mov = '201' AND D.fuente = '".$row[0]."' AND D.productoservicio = ".$row[2]." AND D.medio_pago='".$row[1]."'";

            //$sqlr_rp = "SELECT sum(valor) FROM ccpetrp_detalle WHERE cuenta='$cuenta' AND productoservicio = ".$row[2]." AND fuente = '".$row[0]."' AND medio_pago='".$row[1]."'";
            $res_rp = mysqli_query($linkbd, $sqlr_rp);
            $row_rp = mysqli_fetch_array($res_rp);

            $sqlr_cxp = "SELECT sum(D.valor) FROM tesoordenpago AS C, tesoordenpago_det AS D WHERE C.id_orden =  D.id_orden AND D.cuentap = '$cuenta' AND C.fecha BETWEEN '$fechaIni' AND '$fechaFin' AND  NOT(C.estado='N' OR C.estado='R') AND  C.tipo_mov = '201' AND D.productoservicio = ".$row[2]." AND D.fuente = '".$row[0]."' AND D.medio_pago='".$row[1]."'";

            //$sqlr_cxp = "SELECT sum(valor) FROM tesoordenpago_det WHERE cuentap='$cuenta' AND productoservicio = ".$row[2]." AND fuente = '".$row[0]."' AND medio_pago='".$row[1]."'";
            $res_cxp = mysqli_query($linkbd, $sqlr_cxp);
            $row_cxp = mysqli_fetch_array($res_cxp);

            $sqlr_cxp_nom = "SELECT sum(D.valor) FROM humnomina AS C, humnom_presupuestal AS D WHERE C.id_nom = D.id_nom AND C.fecha BETWEEN '$fechaIni' AND '$fechaFin' AND D.cuenta = '$cuenta' AND D.fuente = '".$row[0]."' AND C.estado!='N'";//echo $sqlr_cxp_nom."<br>";
            $res_cxp_nom = mysqli_query($linkbd, $sqlr_cxp_nom);
            $row_cxp_nom = mysqli_fetch_row($res_cxp_nom);

            $valorCxp = $row_cxp[0] + $row_cxp_nom[0];

            $sqlr_egreso = "SELECT sum(TD.valor) FROM tesoordenpago T, tesoordenpago_det TD, tesoegresos TE WHERE TE.id_orden=T.id_orden AND T.id_orden=TD.id_orden AND TD.cuentap = '$cuenta' AND T.fecha BETWEEN '$fechaIni' AND '$fechaFin' AND  NOT(TE.estado='N' OR TE.estado='R') AND TD.valor >0 AND TE.tipo_mov='201' AND TD.productoservicio = ".$row[2]." AND TD.fuente = '".$row[0]."' AND TD.medio_pago='".$row[1]."'";

            //$sqlr_egreso = "SELECT sum(valor) FROM tesoordenpago T, tesoordenpago_det TD, tesoegresos TE WHERE TE.id_orden=T.id_orden AND T.id_orden=TD.id_orden AND TD.cuentap = '$cuenta' AND TE.vigencia='$vigencia' AND TD.vigencia='$vigencia' AND  NOT(TE.estado='N') AND TD.valor >0 AND TE.tipo_mov='201' AND TD.productoservicio = ".$row[2]." AND TD.fuente = '".$row[0]."' AND TD.medio_pago='".$row[1]."'";
            $res_egreso = mysqli_query($linkbd, $sqlr_egreso);
            $row_egreso = mysqli_fetch_row($res_egreso);

            $sqlr_egreso_nom = "SELECT sum(D.valordevengado) FROM tesoegresosnomina AS C, tesoegresosnomina_det AS D WHERE C.id_egreso = D.id_egreso AND C.fecha BETWEEN '$fechaIni' AND '$fechaFin' AND NOT(D.tipo='SE' OR D.tipo='PE' OR D.tipo='DS' OR D.tipo='RE') AND D.cuentap = '$cuenta'";
            $res_egreso_nom = mysqli_query($linkbd, $sqlr_egreso_nom);
            $row_egreso_nom = mysqli_fetch_row($res_egreso_nom);

            $valorEgreso = $row_egreso[0] + $row_egreso_nom[0];

            $saldo = $row[4] - $row_cdp[0]; */

            if($row[3] == 'I' || $row[3] == 'G' || $row[3] == 'C' || $row[3] == 'R')
            {
                $row[4] = 0;
            }

            $presupuestoDefinitivo = $row[4] + $adicion - $reduccion + $credito - $contracredito;

		    $saldo = $presupuestoDefinitivo - $totalCDPEnt;

            $tabla.='<tr>
                        <td class="cssdeta">'.$row[0].' - '.$row_fuente[0].'</td>
                        <td class="cssdeta" >'.$medioPago.'</td>
                        <td class="cssdeta" >'.$row[2].' - '.$row_bienes[0].'</td>
                        <td class="cssdeta" >'.$row[3].' - '.$row_unidad[0].'</td>
                        <td class="cssdeta" >'.number_format($row[4], 2, ',', '.').'</td>
                        <td class="cssdeta" >'.number_format($adicion, 2, ',', '.').'</td>
                        <td class="cssdeta" >'.number_format($reduccion, 2, ',', '.').'</td>
                        <td class="cssdeta" >'.number_format($credito, 2, ',', '.').'</td>
                        <td class="cssdeta" >'.number_format($contracredito, 2, ',', '.').'</td>
                        <td class="cssdeta" >'.number_format($presupuestoDefinitivo, 2, ',', '.').'</td>
                        <td class="cssdeta" >'.number_format($totalCDPEnt, 2, ',', '.').'</td>
                        <td class="cssdeta" >'.number_format($totalRPEnt, 2, ',', '.').'</td>
                        <td class="cssdeta" >'.number_format($totalCxPEnt, 2, ',', '.').'</td>
                        <td class="cssdeta" >'.number_format($totalEgresoEnt, 2, ',', '.').'</td>
                        <td class="cssdeta" >'.number_format($saldo, 2, ',', '.').'</td>
                    </tr>
                    ';
        }
    }

    $sqlr = "SELECT CB.fuente, CB.medioPago, CB.subclase, CB.unidad, CB.valor FROM ccpetinicialservicios AS CB WHERE CB.cuenta = '$cuenta' AND CB.vigencia='$vigencia' $critBienesTransportables $crit1BienesTransportables UNION SELECT CA.fuente, CA.mediopago, CA.cuenta_clasificadora, CA.tipo, CA.valor FROM ccpetadiciones AS CA WHERE CA.cuenta = '$cuenta' AND CA.vigencia='$vigencia' AND LEFT(CA.cuenta, 3)!='2.3' AND CA.cuenta_clasificadora!='' AND CA.clasificador='3' AND NOT EXISTS(SELECT 1 FROM ccpetinicialservicios AS CBT WHERE CBT.cuenta = '$cuenta' AND CBT.vigencia='$vigencia' AND CA.cuenta_clasificadora = CBT.subclase AND CA.fuente = CBT.fuente) UNION SELECT CT.fuente, CT.mediopago, CT.productoservicio, CT.tipo, CT.valor FROM ccpettraslados AS CT WHERE CT.cuenta = '$cuenta' AND CT.vigencia='$vigencia' AND LENGTH(CT.productoservicio)=5 AND CT.productoservicio!='' AND LEFT(CT.cuenta, 3)!='2.3' AND NOT EXISTS(SELECT 1 FROM ccpetinicialservicios AS CTT WHERE CTT.cuenta = '$cuenta' AND CT.productoservicio = CTT.subclase AND CTT.vigencia='$vigencia' AND CT.fuente = CTT.fuente) AND NOT EXISTS(SELECT 1 FROM ccpetadiciones AS CAT WHERE CAT.cuenta = '$cuenta' AND CT.productoservicio = CAT.cuenta_clasificadora AND CAT.vigencia='$vigencia' AND CT.fuente = CAT.fuente) GROUP BY CT.fuente, CT.mediopago, CT.productoservicio";

    $result = mysqli_query($linkbd, $sqlr);
    if(mysqli_num_rows($result) > 0){
        $tabla.='<tr>
                    <td>Fuente</td>
                    <td>MedioPago</td>
                    <td>subClase</td>
                    <td>Unidad</td>
                    <td>Inicial</td>
                    <td>Adicion</td>
                    <td>Reduccion</td>
                    <td>Credito</td>
                    <td>Contracredito</td>
                    <td>Definitivo</td>
                    <td>Disponibilidad</td>
                    <td>Compromiso</td>
                    <td>Obligacion</td>
                    <td>Pago</td>
                    <td>Saldo</td>
                </tr>';

        while($row = mysqli_fetch_array($result)){

            $inicial = 0;
            $adicion = 0;
            $reduccion = 0;
            $credito = 0;
            $contracredito = 0;
            $presupuestoDefinitivo = 0;
            $totalCDPEnt = 0;
            $totalRPEnt = 0;
            $totalCxPEnt = 0;
            $totalEgresoEnt = 0;
            $saldo = 0;


            $sqlr_fuente = "SELECT fuente_financiacion FROM ccpet_fuentes WHERE id_fuente = '$row[0]'";
            $result_fuente = mysqli_query($linkbd, $sqlr_fuente);
            $row_fuente = mysqli_fetch_array($result_fuente);

            $sqlr_bienes = "SELECT titulo FROM ccpetservicios WHERE grupo = '$row[2]'";
            $result_bienes = mysqli_query($linkbd, $sqlr_bienes);
            $row_bienes = mysqli_fetch_array($result_bienes);

            $sqlr_unidad = "SELECT nombre FROM pptouniejecu WHERE id_cc = '$row[3]'";
            $result_unidad = mysqli_query($linkbd, $sqlr_unidad);
            $row_unidad = mysqli_fetch_array($result_unidad);

            if($row[1] == 'CSF'){
                $medioPago = 'Con situacion de fondos';
            }else{
                $medioPago = 'Sin situacion de fondos';
            }

            //============= AQUI INICIA EL PROCESO PARA CALCULAR LA ADICION =========//


            $sqlrAdicion = "SELECT SUM(valor) FROM ccpetadiciones WHERE cuenta = '$cuenta' AND fecha BETWEEN '$fechaIni' AND '$fechaFin' AND vigencia='$vigencia' AND estado!='N' AND mediopago = '".$row[1]."' AND fuente = '".$row[0]."' AND cuenta_clasificadora = '".$row[2]."'";
            $resAdicion = mysqli_query($linkbd_V7, $sqlrAdicion);
            $rowAdicion = mysqli_fetch_row($resAdicion);

            $adicion = $rowAdicion[0];

            //============= AQUI FINALIZA EL PROCESO PARA CALCULAR LA ADICION =========//

            //============= AQUI INICIA EL PROCESO PARA CALCULAR LA REDUCCION =========//

            $sqlrReduccion = "SELECT SUM(valor) FROM ccpetreducciones WHERE cuenta = '$cuenta' AND fecha BETWEEN '$fechaIni' AND '$fechaFin' AND vigencia='$vigencia' AND estado!='N' AND mediopago = '".$row[1]."' AND fuente = '".$row[0]."' AND cuenta_clasificadora = '".$row[2]."'";
            $resReduccion = mysqli_query($linkbd_V7, $sqlrReduccion);
            $rowReduccion = mysqli_fetch_row($resReduccion);

            $reduccion = $rowReduccion[0];

            $sqlrReduccion_n = "SELECT SUM(CR.valor) FROM ccpetreduccion AS CR, ccpetacuerdos AS CAC WHERE CR.rubro = '$cuenta' AND CR.acuerdo = CAC.id_acuerdo AND CAC.fecha BETWEEN '$fechaIni' AND '$fechaFin' AND CR.vigencia='$vigencia' AND CR.medio_pago = '".$row[1]."' AND CR.fuente = '".$row[0]."' AND CR.lasificador = '".$row[2]."'";
            $resReduccion_n = mysqli_query($linkbd_V7, $sqlrReduccion_n);
            $rowReduccion_n = mysqli_fetch_row($resReduccion_n);

            $reduccion += $rowReduccion_n[0];

            //============= AQUI FINALIZA EL PROCESO PARA CALCULAR LA REDUCCION =========//


            //============= AQUI INICIA EL PROCESO PARA CALCULAR EL CREDITO =========//
            
            $sqlrCredito = "SELECT SUM(valor) FROM ccpettraslados WHERE cuenta = '$cuenta' AND fecha BETWEEN '$fechaIni' AND '$fechaFin' AND vigencia='$vigencia' AND estado!='N' AND mediopago = '".$row[1]."' AND tipo='C' AND productoservicio = '".$row[2]."' AND fuente='".$row[0]."'";
            $resCredito = mysqli_query($linkbd_V7, $sqlrCredito);
            $rowCredito = mysqli_fetch_row($resCredito);

            $credito = $rowCredito[0];

            //============= AQUI FINALIZA EL PROCESO PARA CALCULAR EL CREDITO =========//

            //============= AQUI INICIA EL PROCESO PARA CALCULAR EL CONTRACREDITO =========//

            $sqlrContracreditoCredito = "SELECT SUM(valor) FROM ccpettraslados WHERE cuenta = '$cuenta' AND fecha BETWEEN '$fechaIni' AND '$fechaFin' AND vigencia='$vigencia' AND estado!='N' AND mediopago = '".$row[1]."' AND tipo='R' AND productoservicio = '".$row[2]."' AND fuente='".$row[0]."'";
            $resContracreditoCredito = mysqli_query($linkbd_V7, $sqlrContracreditoCredito);
            $rowContracreditoCredito = mysqli_fetch_row($resContracreditoCredito);

            $contracredito = $rowContracreditoCredito[0];

            //============= AQUI FINALIZA EL PROCESO PARA CALCULAR EL CONTRACREDITO =========//

            //============= AQUI INICIA EL PROCESO PARA CALCULAR EL VALOR DE LOS CDPS =========//

            $sqlrCdp = "SELECT C.consvigencia, C.tipo_mov,D.valor,D.tipo_mov FROM ccpetcdp C, ccpetcdp_detalle D WHERE D.cuenta = '$cuenta' AND D.vigencia='$vigencia' AND D.consvigencia=C.consvigencia AND C.vigencia='$vigencia' AND NOT(D.estado='N') AND D.valor>0 AND D.tipo_mov=C.tipo_mov AND D.tipo_mov='201' AND C.fecha BETWEEN '$fechaIni' AND '$fechaFin' AND D.fuente = '".$row[0]."' AND D.productoservicio = '".$row[2]."' AND D.medio_pago = '".$row[1]."' UNION SELECT C.consvigencia, C.tipo_mov,SUM(D.valor),D.tipo_mov FROM ccpetcdp C, ccpetcdp_detalle D WHERE D.cuenta = '$cuenta' AND D.vigencia='$vigencia' AND D.consvigencia=C.consvigencia AND C.vigencia='$vigencia' AND NOT(D.estado='N') AND D.valor>0 AND D.tipo_mov=C.tipo_mov AND (D.tipo_mov='401' OR D.tipo_mov='402') AND D.fuente = '".$row[0]."' AND D.productoservicio = '".$row[2]."' AND D.medio_pago = '".$row[1]."' AND EXISTS(SELECT 1 FROM ccpetcdp DAUX WHERE DAUX.consvigencia=D.consvigencia AND DAUX.tipo_mov='201' AND DAUX.fecha BETWEEN '$fechaIni' AND '$fechaFin')";
            
            $resCdp = mysqli_query($linkbd_V7, $sqlrCdp);
            if(mysqli_num_rows($resCdp)!=0)
            {
                while($rowCdp = mysqli_fetch_row($resCdp))
                {
                    if($rowCdp[3]=='201')
                    {
                        $totalCDPEnt+=round($rowCdp[2],2);
                    }
                    else if( substr($rowCdp[3],0,1) == '4' )
                    {
                        $totalCDPEnt-=round($rowCdp[2],2);
                    }
                }
            }

            //============= AQUI FINALIZA EL PROCESO PARA CALCULAR EL VALOR DE LOS CDPS =========//

            //============= AQUI INICIA EL PROCESO PARA CALCULAR EL VALOR DE LOS RPS =========//

            
            $sqlrRp = "SELECT R.consvigencia,R.fecha,R.tipo_mov,RD.valor,RD.tipo_mov FROM ccpetrp R,ccpetrp_detalle RD where  R.vigencia='$vigencia' AND RD.cuenta = '$cuenta' AND RD.consvigencia=R.consvigencia AND RD.vigencia='$vigencia'  AND NOT(R.estado='N') AND R.tipo_mov=RD.tipo_mov AND R.tipo_mov='201' AND RD.valor>0 AND R.fecha BETWEEN '$fechaIni' AND '$fechaFin' AND RD.fuente = '".$row[0]."' AND RD.productoservicio = '".$row[2]."' AND RD.medio_pago = '".$row[1]."' UNION SELECT R.consvigencia,R.fecha,R.tipo_mov,SUM(RD.valor),RD.tipo_mov FROM ccpetrp R,ccpetrp_detalle RD where  R.vigencia='$vigencia' AND RD.cuenta = '$cuenta' AND RD.consvigencia=R.consvigencia AND RD.vigencia='$vigencia'  AND NOT(R.estado='N') AND R.tipo_mov=RD.tipo_mov AND (R.tipo_mov='401' OR R.tipo_mov='402') AND RD.valor>0 AND EXISTS(SELECT 1 FROM ccpetrp RAUX WHERE RAUX.consvigencia=R.consvigencia AND RAUX.tipo_mov='201' AND RAUX.fecha BETWEEN '$fechaIni' AND '$fechaFin') AND RD.fuente = '".$row[0]."' AND RD.productoservicio = '".$row[2]."' AND RD.medio_pago = '".$row[1]."'";
            //echo $sqlrRp."<br>";
            $resRp = mysqli_query($linkbd_V7, $sqlrRp);
            if(mysqli_num_rows($resRp)!=0)
            {
                while($rowRp = mysqli_fetch_row($resRp))
                {
                    if( $rowRp[4]=='201')
                    {
                        $totalRPEnt+=$rowRp[3];
                        $arregloRP[]=$rowRp[0];
                    }
                    else if( substr($rowRp[4],0,1) == '4')
                    {
                        $totalRPEnt-=$rowRp[3];
                    }
                }
            }

            //============= AQUI FINALIZA EL PROCESO PARA CALCULAR EL VALOR DE LOS RPS =========//

            //============= AQUI INICIA EL PROCESO PARA CALCULAR EL VALOR DE LAS CXP =========//


            $sqlrCxp = "SELECT T.id_orden,T.fecha,T.tipo_mov,TD.valor,T.estado,TD.tipo_mov FROM tesoordenpago T,tesoordenpago_det TD WHERE T.vigencia=$vigencia  AND T.id_orden=TD.id_orden AND NOT(T.estado='N') AND TD.valor>0 AND TD.cuentap = '$cuenta' AND T.tipo_mov=TD.tipo_mov AND T.tipo_mov='201' AND T.fecha BETWEEN '$fechaIni' AND '$fechaFin'  AND TD.productoservicio = '".$row[2]."' AND TD.fuente = '".$row[0]."' AND TD.medio_pago = '".$row[1]."' UNION SELECT T.id_orden,T.fecha,T.tipo_mov,TD.valor,T.estado,TD.tipo_mov FROM tesoordenpago T,tesoordenpago_det TD WHERE T.vigencia=$vigencia  AND T.id_orden=TD.id_orden AND NOT(T.estado='N') AND TD.valor>0 AND TD.cuentap = '$cuenta' AND T.tipo_mov=TD.tipo_mov AND (T.tipo_mov='401' OR T.tipo_mov='402') AND EXISTS(SELECT 1 FROM tesoordenpago TAUX WHERE TAUX.id_orden=T.id_orden AND TAUX.tipo_mov='201' AND TAUX.fecha BETWEEN '$fechaIni' AND '$fechaFin')  AND TD.productoservicio = '".$row[2]."' AND TD.fuente = '".$row[0]."' AND TD.medio_pago = '".$row[1]."'";

            $resCxp = mysqli_query($linkbd_V7, $sqlrCxp);
            if(mysqli_num_rows($resCxp)!=0)
            {
                while($rowCxp = mysqli_fetch_row($resCxp))
                {
                    if($rowCxp[5]=='201')
                    {
                        $totalCxPEnt+=$rowCxp[3];
                    }
                    else if($rowCxp[5]=='401')
                    {
                        $totalCxPEnt-=$rowCxp[3];
                    }
                }
            }

            for ($i=0; $i <sizeof($arregloRP); $i++) 
            {
                $sqlrCxpNomina = "SELECT HN.id_nom,HN.periodo,HNP.valor,HN.fecha FROM hum_nom_cdp_rp HNR,humnom_presupuestal HNP,humnomina HN WHERE HNR.rp=$arregloRP[$i] AND HNR.nomina=HNP.id_nom AND HNP.cuenta = '$cuenta' AND HNR.vigencia='$vigencia' AND  NOT(HNR.estado='N' OR HNR.estado='R') AND HNP.valor>0 AND HN.id_nom=HNR.nomina AND HN.fecha BETWEEN '$fechaIni' AND '$fechaFin' AND HNP.medio_pago = '".$row[1]."' AND HNP.fuente = '".$row[0]."'";
                
                $resCxpNomina = mysqli_query($linkbd_V7, $sqlrCxpNomina);

                if(mysqli_num_rows($resCxpNomina)==0)
                    echo "";
                else
                {
                    while($rowCxpNomina = mysqli_fetch_row($resCxpNomina))
                    {
                        $sqlrEgresoNomina = "SELECT TEND.valordevengado FROM tesoegresosnomina TEN,tesoegresosnomina_det TEND WHERE  TEN.id_orden=".$rowCxpNomina[0]." AND TEN.vigencia='$vigencia' AND NOT(TEN.estado='N' OR TEN.estado='R')  AND TEN.fecha BETWEEN '$fechaIni' AND '$fechaFin' AND TEN.id_egreso=TEND.id_egreso AND NOT(TEND.tipo='SE' OR TEND.tipo='PE' OR TEND.tipo='DS' OR TEND.tipo='RE' OR TEND.tipo='FS') AND TEND.cuentap LIKE '$cuenta%' AND TEND.fuente = '".$row[0]."' ";
                        
                        $resEgresoNomina = mysqli_query($linkbd_V7, $sqlrEgresoNomina);
                        if(mysqli_num_rows($resEgresoNomina)!=0)
                        {
                            while($rowEgresoNomina = mysqli_fetch_row($resEgresoNomina))
                            {
                                $totalEgresoEnt+=$rowEgresoNomina[0];
                            }
                        }
                        $totalCxPEnt+=$rowCxpNomina[2];
                        //$totalEgresoEnt+=$rowEgresoNomina[0];
                    }
                }

            }

            //============= AQUI FINALIZA EL PROCESO PARA CALCULAR EL VALOR DE LAS CXP =========//

            //============= AQUI INICIA EL PROCESO PARA CALCULAR EL VALOR DE LOS EGRESOS =========//


            $sqlrEgreso = "SELECT TE.id_egreso,TE.fecha,TE.tipo_mov,TD.valor FROM tesoegresos TE,tesoordenpago_det TD, tesoordenpago TIA where TIA.vigencia='$vigencia' AND TIA.id_orden=TD.id_orden AND TD.cuentap = '$cuenta' AND TE.id_orden=TD.id_orden AND  NOT(TE.estado='N') AND TD.valor >0 AND TE.tipo_mov='201' AND TE.fecha BETWEEN '$fechaIni' AND '$fechaFin' AND TD.productoservicio = '".$row[2]."' AND TD.fuente = '".$row[0]."' AND TD.medio_pago = '".$row[1]."' UNION SELECT TE.id_egreso,TE.fecha,TE.tipo_mov,TD.valor FROM tesoegresos TE,tesoordenpago_det TD,tesoordenpago TIO where TIO.vigencia='$vigencia' AND TIO.id_orden=TD.id_orden AND TD.cuentap = '$cuenta' AND TE.id_orden=TD.id_orden AND  NOT(TE.estado='N') AND TD.valor >0 AND TE.tipo_mov='401'  AND EXISTS(SELECT 1 FROM tesoegresos TEAUX WHERE TEAUX.id_egreso=TE.id_egreso AND TEAUX.tipo_mov='201' AND TEAUX.fecha BETWEEN '$fechaIni' AND '$fechaFin') AND TD.productoservicio = '".$row[2]."' AND TD.fuente = '".$row[0]."' AND TD.medio_pago = '".$row[1]."'";

            $resEgreso = mysqli_query($linkbd_V7, $sqlrEgreso);
            if(mysqli_num_rows($resEgreso)!=0)
            {
                while($rowEgreso = mysqli_fetch_row($resEgreso))
                {
                    if($rowEgreso[2]=='201')
                    {
                        $totalEgresoEnt+=$rowEgreso[3];
                    }
                    else if($rowEgreso[2]=='401')
                    {
                        $totalEgresoEnt-=$rowEgreso[3];
                    }
                }
            }

            //============= AQUI FINALIZA EL PROCESO PARA CALCULAR EL VALOR DE LOS EGRESOS =========//


            /* $sqlr_cdp = "SELECT sum(D.valor) FROM ccpetcdp AS C, ccpetcdp_detalle AS D WHERE C.consvigencia = D.consvigencia AND C.vigencia = D.vigencia AND D.cuenta='$cuenta' AND C.fecha BETWEEN '$fechaIni' AND '$fechaFin' AND D.tipo_mov = '201' AND D.fuente = '".$row[0]."' AND D.productoservicio = ".$row[2]." AND D.medio_pago='".$row[1]."'";

            //$sqlr_cdp = "SELECT sum(valor) FROM ccpetcdp_detalle WHERE cuenta='$cuenta' AND productoservicio = ".$row[2]." AND fuente = '".$row[0]."' AND medio_pago='".$row[1]."'";
            $res_cdp = mysqli_query($linkbd, $sqlr_cdp);
            $row_cdp = mysqli_fetch_array($res_cdp);

            $sqlr_rp = "SELECT sum(D.valor) FROM ccpetrp AS C, ccpetrp_detalle AS D WHERE C.consvigencia = D.consvigencia AND C.vigencia = D.vigencia AND D.cuenta='$cuenta' AND C.fecha BETWEEN '$fechaIni' AND '$fechaFin' AND D.tipo_mov = '201' AND D.fuente = '".$row[0]."' AND D.productoservicio = ".$row[2]." AND D.medio_pago='".$row[1]."'";

            //$sqlr_rp = "SELECT sum(valor) FROM ccpetrp_detalle WHERE cuenta='$cuenta' AND productoservicio = ".$row[2]." AND fuente = '".$row[0]."' AND medio_pago='".$row[1]."'";
            $res_rp = mysqli_query($linkbd, $sqlr_rp);
            $row_rp = mysqli_fetch_array($res_rp);

            $sqlr_cxp = "SELECT sum(D.valor) FROM tesoordenpago AS C, tesoordenpago_det AS D WHERE C.id_orden =  D.id_orden AND D.cuentap = '$cuenta' AND C.fecha BETWEEN '$fechaIni' AND '$fechaFin' AND  NOT(C.estado='N' OR C.estado='R') AND  C.tipo_mov = '201' AND D.productoservicio = ".$row[2]." AND D.fuente = '".$row[0]."' AND D.medio_pago='".$row[1]."'";

            //$sqlr_cxp = "SELECT sum(valor) FROM tesoordenpago_det WHERE cuentap='$cuenta' AND productoservicio = ".$row[2]." AND fuente = '".$row[0]."' AND medio_pago='".$row[1]."'";
            $res_cxp = mysqli_query($linkbd, $sqlr_cxp);
            $row_cxp = mysqli_fetch_array($res_cxp);

            $sqlr_cxp_nom = "SELECT sum(D.valor) FROM humnomina AS C, humnom_presupuestal AS D WHERE C.id_nom = D.id_nom AND C.fecha BETWEEN '$fechaIni' AND '$fechaFin' AND D.cuenta = '$cuenta' AND D.fuente = '".$row[0]."' AND C.estado!='N'";//echo $sqlr_cxp_nom."<br>";
            $res_cxp_nom = mysqli_query($linkbd, $sqlr_cxp_nom);
            $row_cxp_nom = mysqli_fetch_row($res_cxp_nom);

            $valorCxp = $row_cxp[0] + $row_cxp_nom[0];

            $sqlr_egreso = "SELECT sum(TD.valor) FROM tesoordenpago T, tesoordenpago_det TD, tesoegresos TE WHERE TE.id_orden=T.id_orden AND T.id_orden=TD.id_orden AND TD.cuentap = '$cuenta' AND T.fecha BETWEEN '$fechaIni' AND '$fechaFin' AND  NOT(TE.estado='N' OR TE.estado='R') AND TD.valor >0 AND TE.tipo_mov='201' AND TD.productoservicio = ".$row[2]." AND TD.fuente = '".$row[0]."' AND TD.medio_pago='".$row[1]."'";

            //$sqlr_egreso = "SELECT sum(valor) FROM tesoordenpago T, tesoordenpago_det TD, tesoegresos TE WHERE TE.id_orden=T.id_orden AND T.id_orden=TD.id_orden AND TD.cuentap = '$cuenta' AND TE.vigencia='$vigencia' AND TD.vigencia='$vigencia' AND  NOT(TE.estado='N') AND TD.valor >0 AND TE.tipo_mov='201' AND TD.productoservicio = ".$row[2]." AND TD.fuente = '".$row[0]."' AND TD.medio_pago='".$row[1]."'";
            $res_egreso = mysqli_query($linkbd, $sqlr_egreso);
            $row_egreso = mysqli_fetch_row($res_egreso);

            $sqlr_egreso_nom = "SELECT sum(D.valordevengado) FROM tesoegresosnomina AS C, tesoegresosnomina_det AS D WHERE C.id_egreso = D.id_egreso AND C.fecha BETWEEN '$fechaIni' AND '$fechaFin' AND NOT(D.tipo='SE' OR D.tipo='PE' OR D.tipo='DS' OR D.tipo='RE') AND D.cuentap = '$cuenta'";
            $res_egreso_nom = mysqli_query($linkbd, $sqlr_egreso_nom);
            $row_egreso_nom = mysqli_fetch_row($res_egreso_nom);

            $valorEgreso = $row_egreso[0] + $row_egreso_nom[0]; 

            $saldo = $row[4] - $row_cdp[0];*/

            if($row[3] == 'I' || $row[3] == 'G' || $row[3] == 'C' || $row[3] == 'R')
            {
                $row[4] = 0;
            }

            $presupuestoDefinitivo = $row[4] + $adicion - $reduccion + $credito - $contracredito;

		    $saldo = $presupuestoDefinitivo - $totalCDPEnt;
        

            $tabla.='<tr>
                        <td class="cssdeta">'.$row[0].' - '.$row_fuente[0].'</td>
                        <td class="cssdeta" >'.$medioPago.'</td>
                        <td class="cssdeta" >'.$row[2].' - '.$row_bienes[0].'</td>
                        <td class="cssdeta" >'.$row[3].' - '.$row_unidad[0].'</td>
                        <td class="cssdeta" >'.number_format($row[4], 2, ',', '.').'</td>
                        <td class="cssdeta" >'.number_format($adicion, 2, ',', '.').'</td>
                        <td class="cssdeta" >'.number_format($reduccion, 2, ',', '.').'</td>
                        <td class="cssdeta" >'.number_format($credito, 2, ',', '.').'</td>
                        <td class="cssdeta" >'.number_format($contracredito, 2, ',', '.').'</td>
                        <td class="cssdeta" >'.number_format($presupuestoDefinitivo, 2, ',', '.').'</td>
                        <td class="cssdeta" >'.number_format($totalCDPEnt, 2, ',', '.').'</td>
                        <td class="cssdeta" >'.number_format($totalRPEnt, 2, ',', '.').'</td>
                        <td class="cssdeta" >'.number_format($totalCxPEnt, 2, ',', '.').'</td>
                        <td class="cssdeta" >'.number_format($totalEgresoEnt, 2, ',', '.').'</td>
                        <td class="cssdeta" >'.number_format($saldo, 2, ',', '.').'</td>
                    </tr>
                    ';
        }
    }

    //$sqlr = "SELECT CB.fuente, CB.medioPago, CB.subclase, CB.unidad, CB.valor FROM ccpetinicialservicios AS CB WHERE CB.cuenta = '$cuenta' AND CB.vigencia='$vigencia' $critBienesTransportables $crit1BienesTransportables UNION SELECT CA.fuente, CA.mediopago, CA.cuenta_clasificadora, CA.tipo, CA.valor FROM ccpetadiciones AS CA WHERE CA.cuenta = '$cuenta' AND CA.vigencia='$vigencia' AND NOT EXISTS(SELECT 1 FROM ccpetinicialservicios AS CBT WHERE CBT.cuenta = '$cuenta' AND CBT.vigencia='$vigencia' AND CA.cuenta_clasificadora = CBT.subclase AND CA.fuente = CBT.fuente)";


    $sqlr = "SELECT CP.id_fuente, CP.medio_pago, CP.subproducto, CP.codigocuin, SUM(CP.valorcsf), SUM(CP.valorssf), CP.subclase, CP.codproyecto, CP.indicador_producto, CP.metas FROM ccpproyectospresupuesto_presupuesto AS CP WHERE CP.rubro = '$cuenta' $critinv1 GROUP BY CP.id_fuente, CP.medio_pago, CP.subproducto, CP.codigocuin, CP.subclase, CP.indicador_producto UNION SELECT CA.fuente, CA.mediopago, CA.cuenta_clasificadora, CA.clasificador, 0,0, CA.cuenta_clasificadora, CA.tipo, CA.indicadorproducto, CA.tipo FROM ccpetadiciones AS CA WHERE CA.cuenta = '$cuenta' AND CA.vigencia='$vigencia' AND LEFT(CA.cuenta, 3)='2.3' AND NOT EXISTS(SELECT 1 FROM ccpproyectospresupuesto_presupuesto AS CBT WHERE CBT.rubro = '$cuenta' AND (CA.cuenta_clasificadora = CBT.subproducto OR CA.cuenta_clasificadora = CBT.subclase) AND CA.fuente = CBT.id_fuente AND CA.indicadorproducto = CBT.indicador_producto) GROUP BY CA.fuente, CA.mediopago, CA.cuenta_clasificadora, CA.indicadorproducto UNION SELECT CPA.id_fuente, CPA.medio_pago, CPA.subproducto, CPA.codigo_cuin, 0, 0, CPA.subclase, CPA.codproyecto, CPA.indicador_producto, CPA.metas FROM ccpetadicion_inversion_detalles AS CPA WHERE CPA.rubro = '$cuenta' AND NOT EXISTS(SELECT 1 FROM ccpproyectospresupuesto_presupuesto AS CBTA WHERE CBTA.rubro = '$cuenta' AND (CPA.subclase = CBTA.subproducto OR CPA.subclase = CBTA.subclase) AND CPA.id_fuente = CBTA.id_fuente AND CPA.indicador_producto = CBTA.indicador_producto) GROUP BY CPA.id_fuente, CPA.medio_pago, CPA.subproducto, CPA.codigo_cuin, CPA.subclase, CPA.indicador_producto";
    $result = mysqli_query($linkbd, $sqlr);
    if(mysqli_num_rows($result) > 0){
        $tabla.='<tr>
                    <td>Sector</td>
                    <td>Fuente</td>
                    <td>MedioPago</td>
                    <td>Programatico</td>
                    <td>Producto/Servicio</td>
                    <td>Meta PDM</td>
                    <td>Unidad</td>
                    <td>CodigoCuin</td>
                    <td>Inicial</td>
                    <td>Adicion</td>
                    <td>Reduccion</td>
                    <td>Credito</td>
                    <td>Contracredito</td>
                    <td>Definitivo</td>
                    <td>Disponibilidad</td>
                    <td>Compromiso</td>
                    <td>Obligacion</td>
                    <td>Pago</td>
                    <td>Saldo</td>
                </tr>';

        while($row = mysqli_fetch_array($result)){

            $inicial = 0;
            $adicion = 0;
            $reduccion = 0;
            $credito = 0;
            $contracredito = 0;
            $presupuestoDefinitivo = 0;
            $totalCDPEnt = 0;
            $totalRPEnt = 0;
            $totalCxPEnt = 0;
            $totalEgresoEnt = 0;
            $saldo = 0;


            $sqlr_fuente = "SELECT fuente_financiacion FROM ccpet_fuentes WHERE id_fuente = '$row[0]'";
            $result_fuente = mysqli_query($linkbd, $sqlr_fuente);
            $row_fuente = mysqli_fetch_array($result_fuente);

            $sqlr_bienes = "SELECT titulo FROM ccpetbienestransportables WHERE grupo = '$row[2]'";
            $result_bienes = mysqli_query($linkbd, $sqlr_bienes);
            $row_bienes = mysqli_fetch_array($result_bienes);
            $codigoProducto = $row[2];
            if($row_bienes[0] == ''){
                $sqlr_bienes = "SELECT titulo FROM ccpetservicios WHERE grupo = '$row[6]'";
                $result_bienes = mysqli_query($linkbd, $sqlr_bienes);
                $row_bienes = mysqli_fetch_array($result_bienes);
                $codigoProducto = $row[6];
            }

            $sqlr_unidad = "SELECT nombre FROM pptouniejecu WHERE id_cc = '".$_POST['unidadEjecutora']."'";
            $result_unidad = mysqli_query($linkbd, $sqlr_unidad);
            $row_unidad = mysqli_fetch_array($result_unidad);

            $sqlr_cuin = "SELECT nombre FROM ccpet_cuin WHERE codigo_cuin = '$row[3]'";
            $result_cuin = mysqli_query($linkbd, $sqlr_cuin);
            $row_cuin = mysqli_fetch_array($result_cuin);

            if($row[1] == 'CSF'){
                $medioPago = 'Con situacion de fondos';
                $valor = $row[4];
            }else{
                $medioPago = 'Sin situacion de fondos';
                $valor = $row[5];
            }


            //============= AQUI INICIA EL PROCESO PARA CALCULAR LA ADICION =========//


            $sqlrAdicion = "SELECT SUM(valor) FROM ccpetadiciones WHERE cuenta = '$cuenta' AND fecha BETWEEN '$fechaIni' AND '$fechaFin' AND vigencia='$vigencia' AND estado!='N' AND mediopago = '".$row[1]."' AND fuente = '".$row[0]."' AND (cuenta_clasificadora = '".$row[2]."' OR cuenta_clasificadora = '".$row[6]."') AND indicadorproducto = '".$row[8]."'";
            $resAdicion = mysqli_query($linkbd_V7, $sqlrAdicion);
            $rowAdicion = mysqli_fetch_row($resAdicion);

            $adicion = $rowAdicion[0];

            $sqlrAdicionInversion = "SELECT SUM(PD.valorcsf) FROM ccpetadicion_inversion AS P, ccpetadicion_inversion_detalles AS PD, ccpetacuerdos AS CA WHERE P.id = PD.codproyecto AND P.id_acuerdo = CA.id_acuerdo AND CA.fecha BETWEEN '$fechaIni' AND '$fechaFin' AND PD.rubro='$cuenta' AND P.vigencia='$vigencia' AND medio_pago = '".$row[1]."' AND id_fuente = '".$row[0]."'  AND (subclase = '".$row[2]."' OR subclase = '".$row[6]."') AND indicador_producto = '".$row[8]."'";
            
            $resAdicionInversion = mysqli_query($linkbd_V7, $sqlrAdicionInversion);
            $rowAdicionInversion = mysqli_fetch_row($resAdicionInversion);
            $adicion += $rowAdicionInversion[0];

            //============= AQUI FINALIZA EL PROCESO PARA CALCULAR LA ADICION =========//

            //============= AQUI INICIA EL PROCESO PARA CALCULAR LA REDUCCION =========//

            $sqlrReduccion = "SELECT SUM(valor) FROM ccpetreducciones WHERE cuenta = '$cuenta' AND fecha BETWEEN '$fechaIni' AND '$fechaFin' AND vigencia='$vigencia' AND estado!='N' AND mediopago = '".$row[1]."' AND fuente = '".$row[0]."' AND (cuenta_clasificadora = '".$row[2]."' OR cuenta_clasificadora = '".$row[6]."') AND indicadorproducto = '".$row[8]."'";
            $resReduccion = mysqli_query($linkbd_V7, $sqlrReduccion);
            $rowReduccion = mysqli_fetch_row($resReduccion);

            $reduccion = $rowReduccion[0];

            $sqlrReduccion_n = "SELECT SUM(CR.valor) FROM ccpetreduccion AS CR, ccpetacuerdos AS CAC WHERE CR.rubro = '$cuenta' AND CR.acuerdo = CAC.id_acuerdo AND CAC.fecha BETWEEN '$fechaIni' AND '$fechaFin' AND CR.vigencia='$vigencia' AND CR.medio_pago = '".$row[1]."' AND CR.fuente = '".$row[0]."' AND (CR.clasificador = '".$row[2]."' OR CR.clasificador = '".$row[6]."') AND CR.indicador_producto = '".$row[8]."'";
            $resReduccion_n = mysqli_query($linkbd_V7, $sqlrReduccion_n);
            $rowReduccion_n = mysqli_fetch_row($resReduccion_n);

            $reduccion += $rowReduccion_n[0];

            //============= AQUI FINALIZA EL PROCESO PARA CALCULAR LA REDUCCION =========//


            //============= AQUI INICIA EL PROCESO PARA CALCULAR EL CREDITO =========//
            
            $sqlrCredito = "SELECT SUM(valor) FROM ccpettraslados WHERE cuenta = '$cuenta' AND fecha BETWEEN '$fechaIni' AND '$fechaFin' AND vigencia='$vigencia' AND estado!='N' AND mediopago = '".$row[1]."' AND tipo='C' AND (productoservicio = '".$row[2]."' OR productoservicio = '".$row[6]."') AND fuente='".$row[0]."' AND indicador_producto = '".$row[8]."'";
            $resCredito = mysqli_query($linkbd_V7, $sqlrCredito);
            $rowCredito = mysqli_fetch_row($resCredito);

            $credito = $rowCredito[0];

            //============= AQUI FINALIZA EL PROCESO PARA CALCULAR EL CREDITO =========//

            //============= AQUI INICIA EL PROCESO PARA CALCULAR EL CONTRACREDITO =========//

            $sqlrContracreditoCredito = "SELECT SUM(valor) FROM ccpettraslados WHERE cuenta = '$cuenta' AND fecha BETWEEN '$fechaIni' AND '$fechaFin' AND vigencia='$vigencia' AND estado!='N' AND mediopago = '".$row[1]."' AND tipo='R' AND (productoservicio = '".$row[2]."' OR productoservicio = '".$row[6]."') AND fuente='".$row[0]."' AND indicador_producto = '".$row[8]."'";
            $resContracreditoCredito = mysqli_query($linkbd_V7, $sqlrContracreditoCredito);
            $rowContracreditoCredito = mysqli_fetch_row($resContracreditoCredito);

            $contracredito = $rowContracreditoCredito[0];

            //============= AQUI FINALIZA EL PROCESO PARA CALCULAR EL CONTRACREDITO =========//

            //============= AQUI INICIA EL PROCESO PARA CALCULAR EL VALOR DE LOS CDPS =========//

            $sqlrCdp = "SELECT C.consvigencia, C.tipo_mov,D.valor,D.tipo_mov FROM ccpetcdp C, ccpetcdp_detalle D WHERE D.cuenta = '$cuenta' AND D.vigencia='$vigencia' AND D.consvigencia=C.consvigencia AND C.vigencia='$vigencia' AND NOT(D.estado='N') AND D.valor>0 AND D.tipo_mov=C.tipo_mov AND D.tipo_mov='201' AND C.fecha BETWEEN '$fechaIni' AND '$fechaFin' AND D.fuente = '".$row[0]."' AND (D.productoservicio = '".$row[2]."' OR D.productoservicio = '".$row[6]."') AND D.medio_pago = '".$row[1]."' AND D.indicador_producto = '".$row[8]."' UNION SELECT C.consvigencia, C.tipo_mov,D.valor,D.tipo_mov FROM ccpetcdp C, ccpetcdp_detalle D WHERE D.cuenta = '$cuenta' AND D.vigencia='$vigencia' AND D.consvigencia=C.consvigencia AND C.vigencia='$vigencia' AND NOT(D.estado='N') AND D.valor>0 AND D.tipo_mov=C.tipo_mov AND (D.tipo_mov='401' OR D.tipo_mov='402') AND D.fuente = '".$row[0]."' AND (D.productoservicio = '".$row[2]."' OR D.productoservicio = '".$row[6]."') AND D.medio_pago = '".$row[1]."' AND D.indicador_producto = '".$row[8]."' AND EXISTS(SELECT 1 FROM ccpetcdp DAUX WHERE DAUX.consvigencia=D.consvigencia AND DAUX.tipo_mov='201' AND DAUX.fecha BETWEEN '$fechaIni' AND '$fechaFin')";
            
            $resCdp = mysqli_query($linkbd_V7, $sqlrCdp);
            if(mysqli_num_rows($resCdp)!=0)
            {
                while($rowCdp = mysqli_fetch_row($resCdp))
                {
                    if($rowCdp[3]=='201')
                    {
                        $totalCDPEnt+=round($rowCdp[2],2);
                    }
                    else if( substr($rowCdp[3],0,1) == '4' )
                    {
                        $totalCDPEnt-=round($rowCdp[2],2);
                    }
                }
            }

            //============= AQUI FINALIZA EL PROCESO PARA CALCULAR EL VALOR DE LOS CDPS =========//

            //============= AQUI INICIA EL PROCESO PARA CALCULAR EL VALOR DE LOS RPS =========//

            
            $sqlrRp = "SELECT R.consvigencia,R.fecha,R.tipo_mov,RD.valor,RD.tipo_mov FROM ccpetrp R,ccpetrp_detalle RD where  R.vigencia='$vigencia' AND RD.cuenta = '$cuenta' AND RD.consvigencia=R.consvigencia AND RD.vigencia='$vigencia'  AND NOT(R.estado='N') AND R.tipo_mov=RD.tipo_mov AND R.tipo_mov='201' AND RD.valor>0 AND R.fecha BETWEEN '$fechaIni' AND '$fechaFin' AND RD.fuente = '".$row[0]."' AND (RD.productoservicio = '".$row[2]."' OR RD.productoservicio = '".$row[6]."') AND RD.medio_pago = '".$row[1]."' AND RD.indicador_producto = '".$row[8]."' UNION SELECT R.consvigencia,R.fecha,R.tipo_mov,RD.valor,RD.tipo_mov FROM ccpetrp R,ccpetrp_detalle RD where  R.vigencia='$vigencia' AND RD.cuenta = '$cuenta' AND RD.consvigencia=R.consvigencia AND RD.vigencia='$vigencia'  AND NOT(R.estado='N') AND R.tipo_mov=RD.tipo_mov AND (R.tipo_mov='401' OR R.tipo_mov='402') AND RD.valor>0 AND EXISTS(SELECT 1 FROM ccpetrp RAUX WHERE RAUX.consvigencia=R.consvigencia AND RAUX.tipo_mov='201' AND RAUX.fecha BETWEEN '$fechaIni' AND '$fechaFin') AND RD.fuente = '".$row[0]."' AND (RD.productoservicio = '".$row[2]."' OR RD.productoservicio = '".$row[6]."') AND RD.medio_pago = '".$row[1]."' AND RD.indicador_producto = '".$row[8]."'";
            //echo $sqlrRp."<br>";
            $resRp = mysqli_query($linkbd_V7, $sqlrRp);
            if(mysqli_num_rows($resRp)!=0)
            {
                while($rowRp = mysqli_fetch_row($resRp))
                {
                    if( $rowRp[4]=='201')
                    {
                        $totalRPEnt+=$rowRp[3];
                        $arregloRP[]=$rowRp[0];
                    }
                    else if( substr($rowRp[4],0,1) == '4')
                    {
                        $totalRPEnt-=$rowRp[3];
                    }
                }
            }

            //============= AQUI FINALIZA EL PROCESO PARA CALCULAR EL VALOR DE LOS RPS =========//

            //============= AQUI INICIA EL PROCESO PARA CALCULAR EL VALOR DE LAS CXP =========//


            $sqlrCxp = "SELECT T.id_orden,T.fecha,T.tipo_mov,TD.valor,T.estado,TD.tipo_mov FROM tesoordenpago T,tesoordenpago_det TD WHERE T.vigencia=$vigencia  AND T.id_orden=TD.id_orden AND NOT(T.estado='N') AND TD.valor>0 AND TD.cuentap = '$cuenta' AND T.tipo_mov=TD.tipo_mov AND T.tipo_mov='201' AND T.fecha BETWEEN '$fechaIni' AND '$fechaFin'  AND (TD.productoservicio = '".$row[2]."' OR TD.productoservicio = '".$row[6]."') AND TD.fuente = '".$row[0]."' AND TD.medio_pago = '".$row[1]."' AND TD.indicador_producto = '".$row[8]."' UNION SELECT T.id_orden,T.fecha,T.tipo_mov,TD.valor,T.estado,TD.tipo_mov FROM tesoordenpago T,tesoordenpago_det TD WHERE T.vigencia=$vigencia  AND T.id_orden=TD.id_orden AND NOT(T.estado='N') AND TD.valor>0 AND TD.cuentap = '$cuenta' AND T.tipo_mov=TD.tipo_mov AND (T.tipo_mov='401' OR T.tipo_mov='402') AND EXISTS(SELECT 1 FROM tesoordenpago TAUX WHERE TAUX.id_orden=T.id_orden AND TAUX.tipo_mov='201' AND TAUX.fecha BETWEEN '$fechaIni' AND '$fechaFin')  AND (TD.productoservicio = '".$row[2]."' OR TD.productoservicio = '".$row[6]."') AND TD.fuente = '".$row[0]."' AND TD.medio_pago = '".$row[1]."' AND TD.indicador_producto = '".$row[8]."'";

            $resCxp = mysqli_query($linkbd_V7, $sqlrCxp);
            if(mysqli_num_rows($resCxp)!=0)
            {
                while($rowCxp = mysqli_fetch_row($resCxp))
                {
                    if($rowCxp[5]=='201')
                    {
                        $totalCxPEnt+=$rowCxp[3];
                    }
                    else if($rowCxp[5]=='401')
                    {
                        $totalCxPEnt-=$rowCxp[3];
                    }
                }
            }

            for ($i=0; $i <sizeof($arregloRP); $i++) 
            {
                $sqlrCxpNomina = "SELECT HN.id_nom,HN.periodo,HNP.valor,HN.fecha FROM hum_nom_cdp_rp HNR,humnom_presupuestal HNP,humnomina HN WHERE HNR.rp=$arregloRP[$i] AND HNR.nomina=HNP.id_nom AND HNP.cuenta = '$cuenta' AND HNR.vigencia='$vigencia' AND  NOT(HNR.estado='N' OR HNR.estado='R') AND HNP.valor>0 AND HN.id_nom=HNR.nomina AND HN.fecha BETWEEN '$fechaIni' AND '$fechaFin' AND HNP.medio_pago = '".$row[1]."' AND HNP.fuente = '".$row[0]."'";
                
                $resCxpNomina = mysqli_query($linkbd_V7, $sqlrCxpNomina);

                if(mysqli_num_rows($resCxpNomina)==0)
                    echo "";
                else
                {
                    while($rowCxpNomina = mysqli_fetch_row($resCxpNomina))
                    {
                        $sqlrEgresoNomina = "SELECT TEND.valordevengado FROM tesoegresosnomina TEN,tesoegresosnomina_det TEND WHERE  TEN.id_orden=".$rowCxpNomina[0]." AND TEN.vigencia='$vigencia' AND NOT(TEN.estado='N' OR TEN.estado='R')  AND TEN.fecha BETWEEN '$fechaIni' AND '$fechaFin' AND TEN.id_egreso=TEND.id_egreso AND NOT(TEND.tipo='SE' OR TEND.tipo='PE' OR TEND.tipo='DS' OR TEND.tipo='RE' OR TEND.tipo='FS') AND TEND.cuentap LIKE '$cuenta%' AND TEND.fuente = '".$row[0]."' ";
                        
                        $resEgresoNomina = mysqli_query($linkbd_V7, $sqlrEgresoNomina);
                        if(mysqli_num_rows($resEgresoNomina)!=0)
                        {
                            while($rowEgresoNomina = mysqli_fetch_row($resEgresoNomina))
                            {
                                $totalEgresoEnt+=$rowEgresoNomina[0];
                            }
                        }
                        $totalCxPEnt+=$rowCxpNomina[2];
                        //$totalEgresoEnt+=$rowEgresoNomina[0];
                    }
                }

            }

            //============= AQUI FINALIZA EL PROCESO PARA CALCULAR EL VALOR DE LAS CXP =========//

            //============= AQUI INICIA EL PROCESO PARA CALCULAR EL VALOR DE LOS EGRESOS =========//


            $sqlrEgreso = "SELECT TE.id_egreso,TE.fecha,TE.tipo_mov,TD.valor FROM tesoegresos TE,tesoordenpago_det TD, tesoordenpago TIA where TIA.vigencia='$vigencia' AND TIA.id_orden=TD.id_orden AND TD.cuentap = '$cuenta' AND TE.id_orden=TD.id_orden AND  NOT(TE.estado='N') AND TD.valor >0 AND TE.tipo_mov='201' AND TE.fecha BETWEEN '$fechaIni' AND '$fechaFin' AND (TD.productoservicio = '".$row[2]."' OR TD.productoservicio = '".$row[6]."') AND TD.fuente = '".$row[0]."' AND TD.medio_pago = '".$row[1]."' AND TD.indicador_producto = '".$row[8]."' UNION SELECT TE.id_egreso,TE.fecha,TE.tipo_mov,TD.valor FROM tesoegresos TE,tesoordenpago_det TD,tesoordenpago TIO where TIO.vigencia='$vigencia' AND TIO.id_orden=TD.id_orden AND TD.cuentap = '$cuenta' AND TE.id_orden=TD.id_orden AND  NOT(TE.estado='N') AND TD.valor >0 AND TE.tipo_mov='401'  AND EXISTS(SELECT 1 FROM tesoegresos TEAUX WHERE TEAUX.id_egreso=TE.id_egreso AND TEAUX.tipo_mov='201' AND TEAUX.fecha BETWEEN '$fechaIni' AND '$fechaFin') AND (TD.productoservicio = '".$row[2]."' OR TD.productoservicio = '".$row[6]."') AND TD.fuente = '".$row[0]."' AND TD.medio_pago = '".$row[1]."' AND TD.indicador_producto = '".$row[8]."'";

            $resEgreso = mysqli_query($linkbd_V7, $sqlrEgreso);
            if(mysqli_num_rows($resEgreso)!=0)
            {
                while($rowEgreso = mysqli_fetch_row($resEgreso))
                {
                    if($rowEgreso[2]=='201')
                    {
                        $totalEgresoEnt+=$rowEgreso[3];
                    }
                    else if($rowEgreso[2]=='401')
                    {
                        $totalEgresoEnt-=$rowEgreso[3];
                    }
                }
            }

            //============= AQUI FINALIZA EL PROCESO PARA CALCULAR EL VALOR DE LOS EGRESOS =========//



            /* $sqlr_cdp = "SELECT sum(D.valor) FROM ccpetcdp AS C, ccpetcdp_detalle AS D WHERE C.consvigencia = D.consvigencia AND C.vigencia = D.vigencia AND D.cuenta='$cuenta' AND C.fecha BETWEEN '$fechaIni' AND '$fechaFin' AND D.tipo_mov = '201' AND D.fuente = '".$row[0]."' AND indicador_producto = '".$row[8]."' AND D.productoservicio = ".$codigoProducto." AND D.medio_pago='".$row[1]."'";


            //$sqlr_cdp = "SELECT sum(valor) FROM ccpetcdp_detalle WHERE cuenta='$cuenta' AND productoservicio = ".$codigoProducto." AND fuente = '".$row[0]."' AND indicador_producto = '".$row[8]."' AND medio_pago='".$row[1]."'";
            $res_cdp = mysqli_query($linkbd, $sqlr_cdp);
            $row_cdp = mysqli_fetch_array($res_cdp);

            $sqlr_rp = "SELECT sum(D.valor) FROM ccpetrp AS C, ccpetrp_detalle AS D WHERE C.consvigencia = D.consvigencia AND C.vigencia = D.vigencia AND D.cuenta='$cuenta' AND C.fecha BETWEEN '$fechaIni' AND '$fechaFin' AND D.tipo_mov = '201' AND D.fuente = '".$row[0]."' AND indicador_producto = '".$row[8]."' AND D.productoservicio = ".$codigoProducto." AND D.medio_pago='".$row[1]."'";

            //$sqlr_rp = "SELECT sum(valor) FROM ccpetrp_detalle WHERE cuenta='$cuenta' AND productoservicio = ".$codigoProducto." AND fuente = '".$row[0]."' AND indicador_producto = '".$row[8]."' AND medio_pago='".$row[1]."'";
            $res_rp = mysqli_query($linkbd, $sqlr_rp);
            $row_rp = mysqli_fetch_array($res_rp);

            $sqlr_cxp = "SELECT sum(D.valor) FROM tesoordenpago AS C, tesoordenpago_det AS D WHERE C.id_orden =  D.id_orden AND D.cuentap = '$cuenta' AND C.fecha BETWEEN '$fechaIni' AND '$fechaFin' AND  NOT(C.estado='N' OR C.estado='R') AND  C.tipo_mov = '201' AND D.productoservicio = ".$codigoProducto." AND indicador_producto = '".$row[8]."' AND D.fuente = '".$row[0]."' AND D.medio_pago='".$row[1]."'";

            //$sqlr_cxp = "SELECT sum(valor) FROM tesoordenpago_det WHERE cuentap='$cuenta' AND productoservicio = ".$codigoProducto." AND fuente = '".$row[0]."' AND indicador_producto = '".$row[8]."' AND medio_pago='".$row[1]."'";
            $res_cxp = mysqli_query($linkbd, $sqlr_cxp);
            $row_cxp = mysqli_fetch_array($res_cxp);

            $sqlr_cxp_nom = "SELECT sum(D.valor) FROM humnomina AS C, humnom_presupuestal AS D WHERE C.id_nom = D.id_nom AND C.fecha BETWEEN '$fechaIni' AND '$fechaFin' AND D.cuenta = '$cuenta' AND D.fuente = '".$row[0]."' AND C.estado!='N'";//echo $sqlr_cxp_nom."<br>";
            $res_cxp_nom = mysqli_query($linkbd, $sqlr_cxp_nom);
            $row_cxp_nom = mysqli_fetch_row($res_cxp_nom);

            $valorCxp = $row_cxp[0] + $row_cxp_nom[0];

            $sqlr_egreso = "SELECT sum(TD.valor) FROM tesoordenpago T, tesoordenpago_det TD, tesoegresos TE WHERE TE.id_orden=T.id_orden AND T.id_orden=TD.id_orden AND TD.cuentap = '$cuenta' AND T.fecha BETWEEN '$fechaIni' AND '$fechaFin' AND  NOT(TE.estado='N' OR TE.estado='R') AND TD.valor >0 AND TE.tipo_mov='201' AND TD.productoservicio = ".$codigoProducto." AND TD.fuente = '".$row[0]."' AND TD.indicador_producto = '".$row[8]."' AND TD.medio_pago='".$row[1]."'";

            //$sqlr_egreso = "SELECT sum(valor) FROM tesoordenpago T, tesoordenpago_det TD, tesoegresos TE WHERE TE.id_orden=T.id_orden AND T.id_orden=TD.id_orden AND TD.cuentap = '$cuenta' AND TE.vigencia='$vigencia' AND TD.vigencia='$vigencia' AND  NOT(TE.estado='N') AND TD.valor >0 AND TE.tipo_mov='201' AND TD.productoservicio = ".$codigoProducto." AND TD.fuente = '".$row[0]."' AND TD.indicador_producto = '".$row[8]."' AND TD.medio_pago='".$row[1]."'";
            $res_egreso = mysqli_query($linkbd, $sqlr_egreso);
            $row_egreso = mysqli_fetch_row($res_egreso);

            $sqlr_egreso_nom = "SELECT sum(D.valordevengado) FROM tesoegresosnomina AS C, tesoegresosnomina_det AS D WHERE C.id_egreso = D.id_egreso AND C.fecha BETWEEN '$fechaIni' AND '$fechaFin' AND NOT(D.tipo='SE' OR D.tipo='PE' OR D.tipo='DS' OR D.tipo='RE') AND D.cuentap = '$cuenta'";
            $res_egreso_nom = mysqli_query($linkbd, $sqlr_egreso_nom);
            $row_egreso_nom = mysqli_fetch_row($res_egreso_nom);

            $valorEgreso = $row_egreso[0] + $row_egreso_nom[0]; */

            $sector = substr($row[8], 0, 2);
            $sqlr_sector = "SELECT nombre FROM ccpetsectores WHERE codigo=".$sector;
            $res_sector = mysqli_query($linkbd, $sqlr_sector);
            $row_sector = mysqli_fetch_array($res_sector);

            $sqlr_producto = "SELECT indicador_producto FROM ccpetproductos WHERE codigo_indicador='".$row[8]."'";
            $res_producto = mysqli_query($linkbd, $sqlr_producto);
            $row_producto = mysqli_fetch_array($res_producto);

            $sqlr_metapdm = "SELECT meta_pdm FROM ccpet_metapdm WHERE id_meta='".$row[9]."'";
            $res_metapdm = mysqli_query($linkbd, $sqlr_metapdm);
            $row_metapdm = mysqli_fetch_array($res_metapdm);
            
            //$saldo = $valor - $row_cdp[0];

            if($row[6] == 'I' || $row[6] == 'G')
            {
                $valor = 0;
            }

            $presupuestoDefinitivo = $valor + $adicion - $reduccion + $credito - $contracredito;

		    $saldo = $presupuestoDefinitivo - $totalCDPEnt;
        
            
            $tabla.='<tr>
                        <td class="cssdeta">'.$sector.' - '.$row_sector[0].'</td>
                        <td class="cssdeta">'.$row[0].' - '.$row_fuente[0].'</td>
                        <td class="cssdeta" >'.$medioPago.'</td>
                        <td class="cssdeta" >'.$row[8].' - '.$row_producto[0].'</td>
                        <td class="cssdeta" >'.$codigoProducto.' - '.$row_bienes[0].'</td>
                        <td class="cssdeta" >'.$row[9].' - '.$row_metapdm[0].'</td>
                        <td class="cssdeta" >'.$_POST['unidadEjecutora'].' - '.$row_unidad[0].'</td>
                        <td class="cssdeta" >'.$row[3].' - '.$row_cuin[0].'</td>
                        <td class="cssdeta" >'.number_format($valor, 2, ',', '.').'</td>
                        <td class="cssdeta" >'.number_format($adicion, 2, ',', '.').'</td>
                        <td class="cssdeta" >'.number_format($reduccion, 2, ',', '.').'</td>
                        <td class="cssdeta" >'.number_format($credito, 2, ',', '.').'</td>
                        <td class="cssdeta" >'.number_format($contracredito, 2, ',', '.').'</td>
                        <td class="cssdeta" >'.number_format($presupuestoDefinitivo, 2, ',', '.').'</td>
                        <td class="cssdeta" >'.number_format($totalCDPEnt, 2, ',', '.').'</td>
                        <td class="cssdeta" >'.number_format($totalRPEnt, 2, ',', '.').'</td>
                        <td class="cssdeta" >'.number_format($totalCxPEnt, 2, ',', '.').'</td>
                        <td class="cssdeta" >'.number_format($totalEgresoEnt, 2, ',', '.').'</td>
                        <td class="cssdeta" >'.number_format($saldo, 2, ',', '.').'</td>
                    </tr>
                    ';
        }
    }
    
	 
    $tabla.='</table>';

    $data_row = array('detalle'=>$tabla);
    header('Content-Type: application/json');
    echo json_encode($data_row);