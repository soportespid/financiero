<?php
    require_once "../comun.inc";
    require_once "../funciones.inc";

    $linkbd = conectar_v7();	
    $maxVersion = ultimaVersionIngresosCCPET();

    $cuenta = $_POST['cuenta'];
    $unidadEjecutora = $_POST['unidadEjecutora'];
    $medioPago = $_POST['medioPago'];
    $vigencia = $_POST['vigencia'];
    $fechaIni = $_POST['fechaIni'];
    $fechaFin = $_POST['fechaFin'];

    $crit = '';
    if($_POST['unidadEjecutora'] != ''){
        $crit = "AND unidad = '$unidadEjecutora'";
    }

    $crit1 = '';
    if($_POST['medioPago'] != ''){
        $crit1 = "AND medioPago = '$medioPago'";
    }

    $tabla='<table class="titulos" width="70%">';

    

    $sqlr = "SELECT fuente, medio_pago, producto_servicio, seccion_presupuestal, valor, vigencia_gasto FROM ccpetinicialing WHERE cuenta = '$cuenta' AND vigencia='$vigencia' $crit $crit1";
    $result = mysqli_query($linkbd, $sqlr);
    if(mysqli_num_rows($result) > 0){
        $tabla.='<tr>
                    <td>Fuente</td>
                    <td>MedioPago</td>
                    <td>Unidad</td>
                    <td>Bienes</td>
                    <td>Inicial</td>
                    <td>Adicion</td>
                    <td>Reduccion</td>
                    <td>Definitivo</td>
                    <td>Superavit</td>
                    <td>Recaudos anteriores</td>
                    <td>Recaudos de consulta</td>
                    <td>Total recaudos</td>
                    <td>Saldo por recaudar</td>
                    <td>En ejecucion</td>
                </tr>';

        while($row = mysqli_fetch_array($result)){
            $sqlr_fuente = "SELECT nombre FROM ccpet_fuentes_cuipo WHERE codigo_fuente = '$row[0]'";
            $result_fuente = mysqli_query($linkbd, $sqlr_fuente);
            $row_fuente = mysqli_fetch_array($result_fuente);

            $sqlr_bienes = "SELECT titulo FROM ccpetbienestransportables WHERE grupo = '$row[2]'";
            $result_bienes = mysqli_query($linkbd, $sqlr_bienes);
            $row_bienes = mysqli_fetch_array($result_bienes);

            $sqlr_unidad = "SELECT nombre FROM pptoseccion_presupuestal WHERE id_seccion_presupuestal = '$row[3]'";
            $result_unidad = mysqli_query($linkbd, $sqlr_unidad);
            $row_unidad = mysqli_fetch_array($result_unidad);

            if($row[1] == 'CSF'){
                $medioPago = 'Con situacion de fondos';
            }else{
                $medioPago = 'Sin situacion de fondos';
            }

            $arregloGastos = reporteIngresosCcpet($cuenta, $row[3], $row[1], $vigencia, $fechaIni, $fechaFin, $row[0], $row[5]);

			$recaudoPorCuenta = generaRecaudoCcpet($cuenta,$vigencia,$vigencia,$fechaIni,$fechaFin, $row[0], $row[2], $row[1], $row[3]);

            $totalRecaudoCuenta = $recaudoPorCuenta;

            $saldoPorRecaudar = $arregloGastos[3] - $totalRecaudoCuenta;
            $enEjecucion = round((($totalRecaudoCuenta/$arregloGastos[3])*100),0);

            $tabla.='<tr>
                        <td class="cssdeta">'.$row[0].' - '.$row_fuente[0].'</td>
                        <td class="cssdeta" >'.$medioPago.'</td>
                        <td class="cssdeta" >'.$row[3].' - '.$row_unidad[0].'</td>
                        <td class="cssdeta" >'.$row[2].' - '.$row_bienes[0].'</td>
                        <td class="cssdeta" >'.number_format($arregloGastos[0], 2, ',', '.').'</td>
                        <td class="cssdeta" >'.number_format($arregloGastos[1], 2, ',', '.').'</td>
                        <td class="cssdeta" >'.number_format($arregloGastos[2], 2, ',', '.').'</td>
                        <td class="cssdeta" >'.number_format($arregloGastos[3], 2, ',', '.').'</td>
                        <td class="cssdeta" >'.number_format(0, 2, ',', '.').'</td>
                        <td class="cssdeta" >'.number_format(0, 2, ',', '.').'</td>
                        <td class="cssdeta" >'.number_format($recaudoPorCuenta, 2, ',', '.').'</td>
                        <td class="cssdeta" >'.number_format($totalRecaudoCuenta, 2, ',', '.').'</td>
                        <td class="cssdeta" >'.number_format($saldoPorRecaudar, 2, ',', '.').'</td>
                        <td class="cssdeta" >'.$enEjecucion.'%</td>
                    </tr>
                    ';
        }
    }
    
	 
    $tabla.='</table>';

    $data_row = array('detalle'=>$tabla);
    header('Content-Type: application/json');
    echo json_encode($data_row);