<?php

    header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=utf8");
	require 'comun.inc';
	require 'funciones.inc';
	session_start();
    if(empty($_SESSION)){
        header("location: index.php");
    }
	$linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");
	cargarcodigopag($_GET['codpag'],$_SESSION["nivel"]);
	date_default_timezone_set("America/Bogota");
?>
<!DOCTYPE >
<html lang=es>
	<head>
    <meta charset="utf-8"/>
		<meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1"/>
		<meta name="viewport" content="user-scalable=no">
		<title>:: IDEAL 10 - Informes</title>
		<link href="favicon.ico" rel="shortcut icon"/>
		<link href="css/css2z.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
		<link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
		<link href="css/css4.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
        <link href="css/tabs.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css"/>
        <link href="css/style.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css"/>
        <link rel="stylesheet" href="sweetalert2/dist/sweetalert2.min.css">
        <script type="text/javascript" src="css/programas.js"></script>
        <script src="sweetalert2/dist/sweetalert2.min.js"></script>
		<script src="vue/vue.min.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script src="css/programas.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script src="css/calendario.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/html2pdf.js/0.10.1/html2pdf.bundle.min.js" integrity="sha512-GsLlZN/3F2ErC5ifS5QtgpiJtWd43JWSuIgh7mbzZ8zBps+dvLusV+eNQATqgA/HdeKFVgA5v3S/cIrLF7QnIg==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>

        <style>
            .ultimo-aporte{
                background-color: rgb(56,100,146) !important;
                color:#fff;
            }
            .nota-220{
                font-size: 8pt;
            }
            #printSection{
                page-break-inside: avoid;
            }
            #printSection p{
                margin: 0;
            }
        </style>
	</head>
	<body>
		<header>
			<table>
				<tr><script>barra_imagenes("info");</script><?php cuadro_titulos();?></tr>
			</table>
		</header>

		<form name="form2" method="post" action="">
			<section id="myapp" v-cloak >
                <div id="cargando" v-show="isLoading" class="loading" style="z-index: 10000;">
                    <span>Cargando...</span>
                </div>
				<nav>
					<table>
						<tr><?php menu_desplegable("info");?></tr>
                        <tr>
							<td colspan="3" class="cinta">
                                <img src="imagenes/add2.png"  class="mgbt" title="Nuevo">
								<img src="imagenes/guardad.png"   title="Guardar"  class="mgbt">
								<img src="imagenes/buscad.png"   class="mgbt" title="Buscar">
								<img src="imagenes/nv.png" @click="mypop=window.open('info-principal.php','',''); mypop.focus();" class="mgbt" title="Nueva Ventana">
                                <img src="imagenes/print.png" title="Imprimir" @click="printPDF()" class="mgbt">
                                <a href="teso-formatoexogena.php"  class="mgbt"><img src="imagenes/iratras.png" title="Atr&aacute;s"></a>
							</td>
						</tr>
					</table>
				</nav>
				<article>
                    <div class="inicio">
                        <div>
                            <h2 class="titulos m-0">.: Generar certificado de ingresos y retenciones por rentas de trabajo y pensiones para exógena 1001</h2>
                            <p class="ms-2 text-black-light">Todos los campos con (<span class="text-danger fw-bolder">*</span>) son obligatorios</p>
                            <div class="d-flex">
                                <div class="form-control w-25">
                                    <label class="form-label" for="">.: Seleccionar exógena 1001 <span class="text-danger fw-bolder">*</span>:</label>
                                    <select @change="getTerceros()" v-model="selectExogena">
                                        <option value="0">Seleccione</option>
                                        <option v-for="(data,index) in arrExogenas" :value="data.id" :key="index">
                                            {{ data.vigencia+" - "+data.descripcion}}
                                        </option>
                                    </select>
                                </div>
                                <div class="form-control w-50">
                                    <label class="form-label" for="">.: Tercero <span class="text-danger fw-bolder">*</span>:</label>
                                    <div class="d-flex">
                                        <input v-model="objTercero.documento" type="text" @change="search('cod_cuenta')"  @dblclick="isModal=true" class="w-50 colordobleclik">
                                        <input v-model="objTercero.nombre" type="text" disabled readony>
                                    </div>
                                </div>
                            </div>
                            <div v-if="isTable" class="overflow-auto max-vh-50 overflow-x-hidden p-2" ref="printSection" id="printSection">
                                <!-- Tabla datos a básicos del empleador -->
                                <table class="table table-base fw-normal">
                                    <thead>
                                        <tr>
                                            <th colspan="2" >
                                                <div class="d-flex justify-center">
                                                    <svg
                                                        xmlns:dc="http://purl.org/dc/elements/1.1/"
                                                        xmlns:cc="http://creativecommons.org/ns#"
                                                        xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
                                                        xmlns:svg="http://www.w3.org/2000/svg"
                                                        xmlns="http://www.w3.org/2000/svg"
                                                        xmlns:xlink="http://www.w3.org/1999/xlink"
                                                        xmlns:sodipodi="http://sodipodi.sourceforge.net/DTD/sodipodi-0.dtd"
                                                        xmlns:inkscape="http://www.inkscape.org/namespaces/inkscape"
                                                        width="250"
                                                        height="42.5"
                                                        viewBox="0 0 80 13.6"
                                                        version="1.1"
                                                        id="svg2"
                                                        sodipodi:version="0.32"
                                                        inkscape:version="0.92.4 (5da689c313, 2019-01-14)"
                                                        sodipodi:docname="Sony Channel logo.svg">
                                                        <title
                                                            id="title4296">Dian Colombia logo</title>
                                                        <metadata
                                                            id="metadata59">
                                                            <rdf:RDF>
                                                            <cc:Work
                                                                rdf:about="">
                                                                <dc:format>image/svg+xml</dc:format>
                                                                <dc:type
                                                                rdf:resource="http://purl.org/dc/dcmitype/StillImage" />
                                                                <dc:title>Dian Colombia logo</dc:title>
                                                                <cc:license
                                                                rdf:resource="http://scripts.sil.org/OFL" />
                                                                <dc:date>27 feb 2020</dc:date>
                                                                <dc:creator>
                                                                <cc:Agent>
                                                                    <dc:title>EEIM</dc:title>
                                                                </cc:Agent>
                                                                </dc:creator>
                                                            </cc:Work>
                                                            <cc:License
                                                                rdf:about="http://scripts.sil.org/OFL">
                                                                <cc:permits
                                                                rdf:resource="http://scripts.sil.org/pub/OFL/Reproduction" />
                                                                <cc:permits
                                                                rdf:resource="http://scripts.sil.org/pub/OFL/Distribution" />
                                                                <cc:permits
                                                                rdf:resource="http://scripts.sil.org/pub/OFL/Embedding" />
                                                                <cc:permits
                                                                rdf:resource="http://scripts.sil.org/pub/OFL/DerivativeWorks" />
                                                                <cc:requires
                                                                rdf:resource="http://scripts.sil.org/pub/OFL/Notice" />
                                                                <cc:requires
                                                                rdf:resource="http://scripts.sil.org/pub/OFL/Attribution" />
                                                                <cc:requires
                                                                rdf:resource="http://scripts.sil.org/pub/OFL/ShareAlike" />
                                                                <cc:requires
                                                                rdf:resource="http://scripts.sil.org/pub/OFL/DerivativeRenaming" />
                                                                <cc:requires
                                                                rdf:resource="http://scripts.sil.org/pub/OFL/BundlingWhenSelling" />
                                                            </cc:License>
                                                            </rdf:RDF>
                                                        </metadata>
                                                        <defs
                                                            id="defs57">
                                                            <linearGradient
                                                            inkscape:collect="always"
                                                            xlink:href="#linearGradient846"
                                                            id="linearGradient848"
                                                            x1="387.57999"
                                                            y1="120.6496"
                                                            x2="268.66495"
                                                            y2="64.499718"
                                                            gradientUnits="userSpaceOnUse" />
                                                            <linearGradient
                                                            inkscape:collect="always"
                                                            id="linearGradient846">
                                                            <stop
                                                                style="stop-color:#19b3d8;stop-opacity:1;"
                                                                offset="0"
                                                                id="stop842" />
                                                            <stop
                                                                style="stop-color:#73ba72;stop-opacity:1"
                                                                offset="1"
                                                                id="stop844" />
                                                            </linearGradient>
                                                            <linearGradient
                                                            inkscape:collect="always"
                                                            xlink:href="#linearGradient838"
                                                            id="linearGradient840"
                                                            x1="342.7793"
                                                            y1="306.98022"
                                                            x2="345.49976"
                                                            y2="421.23999"
                                                            gradientUnits="userSpaceOnUse" />
                                                            <linearGradient
                                                            inkscape:collect="always"
                                                            id="linearGradient838">
                                                            <stop
                                                                style="stop-color:#0bb4db;stop-opacity:1;"
                                                                offset="0"
                                                                id="stop834" />
                                                            <stop
                                                                style="stop-color:#0089b7;stop-opacity:1"
                                                                offset="1"
                                                                id="stop836" />
                                                            </linearGradient>
                                                        </defs>
                                                        <sodipodi:namedview
                                                            inkscape:window-height="705"
                                                            inkscape:window-width="1366"
                                                            inkscape:pageshadow="2"
                                                            inkscape:pageopacity="0"
                                                            guidetolerance="1"
                                                            gridtolerance="1"
                                                            objecttolerance="1"
                                                            borderopacity="1.0"
                                                            bordercolor="#666666"
                                                            pagecolor="#ffffff"
                                                            id="base"
                                                            inkscape:zoom="1"
                                                            inkscape:cx="69.942304"
                                                            inkscape:cy="27.508014"
                                                            inkscape:window-x="-8"
                                                            inkscape:window-y="-8"
                                                            inkscape:current-layer="svg2"
                                                            showgrid="false"
                                                            showguides="false"
                                                            inkscape:showpageshadow="false"
                                                            units="px"
                                                            inkscape:window-maximized="1" />
                                                        <g
                                                            id="#ffffffff"
                                                            transform="translate(0,-1127.4)" />
                                                        <g
                                                            id="#2acbffff-4"
                                                            transform="matrix(0.74035173,0,0,0.75447038,-840.95699,-813.55387)"
                                                            style="fill:#00ff00" />
                                                        <g
                                                            id="#20aabcff"
                                                            transform="matrix(0.14862441,0,0,0.14862441,-1.8088634,-4.7757921)"
                                                            style="fill:#00ff00">
                                                            <path
                                                            d="m 301.78,35.219603 h 8.05 l 77.75,85.429997 -15.56,0.12 -65.72,-72.899997 -40.7,44.97 h 55.07 c 0,0 5.21,5.85 7.81,8.789997 -23.22,0.05 -47.43,-0.22875 -70.65,-0.14875 0,0 -18,19.33009 -17.83,19.25875 -5.27,0.19 -10.56,0.1 -15.83,-0.04 0,0 54.58621,-60.214901 77.61,-85.479997 z"
                                                            id="path6"
                                                            inkscape:connector-curvature="0"
                                                            style="opacity:1;fill:url(#linearGradient848);fill-opacity:1"
                                                            sodipodi:nodetypes="cccccccccccc" />
                                                            <path
                                                            style="opacity:1;fill:url(#linearGradient840);fill-opacity:1;stroke-width:1.33333325"
                                                            d="m 459.83008,307.13477 c -2.79334,0.0117 -5.0293,0.0781 -5.0293,0.0781 L 455,420 l 16.26758,-0.0137 0.0391,-93.77344 156.96094,93.75977 h 15 l 0.0527,-112.77344 h -16.43945 l 0.0391,94.1875 c 0,0 -99.34573,-59.12063 -149.03906,-88.64063 -3.2,-1.82666 -6.21497,-4.06604 -9.68164,-5.35937 -2.22667,-0.22 -5.57581,-0.26362 -8.36914,-0.25195 z m -493.529299,0.0781 c -20.241666,0.003 -36.433593,0.0137 -36.433593,0.0137 l 0.01367,13.82617 c 0,0 69.29198041,-0.0926 103.945313,0.10743 15.680005,0.58666 31.627032,1.28 46.707031,6 C 91.19987,330.34682 100.70664,338.28 104.30664,349 c 3.36,10.10667 3.22635,21.41279 -0.32031,31.43945 -3.56,9.98667 -12.453804,17.36032 -22.480471,20.32032 -15.426666,4.62666 -31.693354,5.22713 -47.693359,5.48046 -29.0533326,0.0667 -87.146484,0.0274 -87.146484,0.0274 l 0.04102,-23.44141 H -70.080078 L -70,420 c 38.573332,-0.0667 77.1473654,0.11924 115.720703,-0.10742 19.106666,-0.57334 39.411876,-1.69326 56.171877,-11.91992 12.53333,-7.62667 20.61474,-21.65357 21.4414,-36.24024 0.42667,-9.76 0.71959,-19.94648 -3.09375,-29.14648 -4.96,-13.26667 -16.57453,-23.34542 -29.894527,-27.71875 -20.319999,-6.92 -42.051688,-7.56029 -63.318359,-7.62696 -16.193333,-0.0267 -40.484897,-0.0307 -60.726563,-0.0273 z m 204.769529,0.008 c -3.41666,0 -6.15039,0.006 -6.15039,0.006 L 165,420 h 16.19922 l 0.12109,-112.74609 c -2.73333,-0.0267 -6.83333,-0.0332 -10.25,-0.0332 z"
                                                            transform="matrix(0.75000002,0,0,0.75000002,66.669997,-194.3004)"
                                                            id="path8"
                                                            inkscape:connector-curvature="0" />
                                                        </g>
                                                    </svg>
                                                </div>
                                            </th>
                                            <th colspan="4" class="text-center">
                                                <p class="m-0" style="font-size:14px">Certificado de Ingresos y Retenciones por Rentas de Trabajo y de Pensiones
                                                Año gravable {{ txtVigencia}}</p>
                                            </th>
                                            <th >
                                                <div class="ultimo-aporte text-center" style="border-radius:5px;font-size:35px;width:100%;height:100%;">220</div>
                                            </th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td rowspan="2" class="text-center fw-bold"style="transform:rotate(270deg);height:70px; width:20px;">Retenedor</td>
                                            <td>
                                                <p class="fw-bold">5. Número de Identificación Tributaria (NIT)</p><br>
                                                <p>{{objRetenedor.nit}}</p>
                                            </td>
                                            <td>
                                                <p class="fw-bold">6. DV</p><br>
                                                <p>{{objRetenedor.sigla}}</p>
                                            <td>
                                                <p class="fw-bold">7. Primer apellido</p><br>
                                                <p></p>
                                            </td>
                                            <td>
                                                <p class="fw-bold">8. Segundo apellido</p><br>
                                                <p></p>
                                            </td>
                                            <td>
                                                <p class="fw-bold">9. Primer nombre</p><br>
                                                <p></p>
                                            </td>
                                            <td>
                                                <p class="fw-bold">10. Otros nombres</p><br>
                                                <p></p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="6">
                                                <p class="fw-bold">11. Razón social</p><br>
                                                <p>{{objRetenedor.razonsocial}}</p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="text-center fw-bold" style="transform:rotate(270deg);height:70px">Empleado</td>
                                            <td>
                                                <p class="fw-bold">24. Tipo de documento </p><br>
                                                <p>{{objTercero.tipo_documento}}</p>
                                            </td>
                                            <td>
                                                <p class="fw-bold">25. Número de Identificación </p><br>
                                                <p>{{objTercero.documento}}</p>
                                            </td>
                                            <td>
                                                <p class="fw-bold">26. Primer apellido </p><br>
                                                <p>{{objTercero.primer_apellido}}</p>
                                            </td>
                                            <td>
                                                <p class="fw-bold">27. Segundo apellido </p><br>
                                                <p>{{objTercero.segundo_apellido}}</p>
                                            </td>
                                            <td>
                                                <p class="fw-bold">28. Primer nombre </p><br>
                                                <p>{{objTercero.primer_nombre}}</p>
                                            </td>
                                            <td>
                                                <p class="fw-bold">29. Otros nombres </p><br>
                                                <p>{{objTercero.segundo_nombre}}</p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="3">
                                                <p class="text-center fw-bold">Periodo de certificación </p><br>
                                                <div class="d-flex justify-center">
                                                    <div class="d-flex me-2">
                                                        <p class="fw-bold me-2">30. DE:</p>
                                                        <p>{{txtVigencia}}-01-01</p>
                                                    </div>
                                                    <div class="d-flex">
                                                        <p class="fw-bold me-2">31. A:</p><br>
                                                        <p>{{txtVigencia}}-12-31</p>
                                                    </div>
                                                </div>
                                            </td>
                                            <td>
                                                <p class="text-center fw-bold">32. Fecha de expedición </p><br>
                                                <p class="text-center">{{txtFechaAct}}</p>
                                            </td>
                                            <td>
                                                <p class="fw-bold">33. Lugar donde se practicó la retención </p><br>
                                                <p>{{objRetenedor.razonsocial}}</p>
                                            </td>
                                            <td>
                                                <p class="fw-bold">34. Cód.Dpto </p><br>
                                                <p>{{objRetenedor.depto}}</p>
                                            </td>
                                            <td>
                                                <p class="fw-bold">35. Cód. Ciudad/Municipio </p><br>
                                                <p>{{objRetenedor.mnpio}}</p>
                                            </td>
                                        </tr>
                                        <!-- Concepto de ingresos -->
                                        <tr style="background-color:rgb(225,231,238)">
                                            <td class="fw-bold text-center" colspan="5" >Concepto de los Ingresos</td>
                                            <td colspan="3" class="fw-bold text-center">Valor</td>
                                        </tr>
                                        <tr v-for="(data,index) in objTercero.columnas_ingresos" :key="index">
                                            <td :class="[index%2==0 ? 'custom-cell-2' : 'custom-cell-1']"colspan="5">{{data.nombre}}</td>
                                            <td class="text-center" :class="[index%2==0 ? 'custom-cell-2' : 'custom-cell-1']">{{data.renglon}}</td>
                                            <td class="text-right" :class="[index%2==0 ? 'custom-cell-2' : 'custom-cell-1']">{{formatNumero(data.valor)}}</td>
                                        </tr>
                                        <tr>
                                            <td colspan="5" class="fw-bold">
                                                Total de ingresos brutos (Sume {{
                                                objTercero.columnas_ingresos[0]['renglon']+" a " +
                                                objTercero.columnas_ingresos[objTercero.columnas_ingresos.length-1]['renglon']
                                                }})
                                            </td>
                                            <td class="text-center">{{objTercero.renglon_ingreso}}</td>
                                            <td class="text-right">{{formatNumero(objTercero.total_ingresos)}}</td>
                                        </tr>
                                        <!-- Concepto de aportes -->
                                        <tr style="background-color:#fff">
                                            <td class="fw-bold text-center" colspan="5" >Concepto de los aportes</td>
                                            <td colspan="3" class="fw-bold text-center">Valor</td>
                                        </tr>
                                        <tr v-for="(data,index) in objTercero.columnas_aportes" :key="index">
                                            <td :class="[(data.renglon == objTercero.renglon_aporte ? 'ultimo-aporte custom-cell-3' :''),
                                            index%2==0 ? 'custom-cell-2' : 'custom-cell-1']" colspan="5">{{data.nombre}}</td>
                                            <td class="text-center" :class="[index%2==0 ? 'custom-cell-2' : 'custom-cell-1']">{{data.renglon}}</td>
                                            <td class="text-right" :class="[index%2==0 ? 'custom-cell-2' : 'custom-cell-1']">{{formatNumero(data.valor)}}</td>
                                        </tr>
                                        <tr>
                                            <td colspan="7" class="custom-cell-3">Nombre del pagador o agente retenedor:</td>
                                        </tr>
                                         <!-- Tabla datos a cargo del empleado -->
                                        <tr>
                                            <td colspan="7" class="text-center fw-bold">Datos a cargo del trabajador o pensionado</td>
                                        </tr>
                                        <tr style="background-color:rgb(225,231,238)">
                                            <td class="text-center fw-bold" colspan="3">Concepto de otros ingresos</td>
                                            <td class="text-center fw-bold" colspan="2" >Valor recibido</td>
                                            <td class="text-center fw-bold" colspan="2">Valor retenido</td>
                                        </tr>
                                        <tr v-for="(data,index) in arrCamposEmpleado" :key="index">
                                            <td colspan="3" :class="[index%2==0 ? 'custom-cell-2' : 'custom-cell-1']">{{ data.nombre }}</td>
                                            <td class="text-center":class="[index%2==0 ? 'custom-cell-2' : 'custom-cell-1']">{{data.renglon1}}</td>
                                            <td :class="[index%2==0 ? 'custom-cell-2' : 'custom-cell-1']"></td>
                                            <td :class="[index%2==0 ? 'custom-cell-2' : 'custom-cell-1']" class="text-center">{{data.renglon2}}</td>
                                            <td :class="[index%2==0 ? 'custom-cell-2' : 'custom-cell-1']"></td>
                                        </tr>
                                        <!-- 71. Identificación de los bienes poseídos -->
                                        <tr style="background-color:rgb(225,231,238)">
                                            <td class="text-center fw-bold">Item</td>
                                            <td class="text-center fw-bold" colspan="5">{{arrCamposEmpleado[7].renglon2+1}}. Identificación de los bienes poseídos</td>
                                            <td class="text-center fw-bold" > {{arrCamposEmpleado[7].renglon2+2}}. Valor patrimonial</td>
                                        </tr>
                                        <tr>
                                            <td class="text-center fw-bold custom-cell-1">1</td>
                                            <td colspan="5" class="custom-cell-1"></td>
                                            <td class="custom-cell-1"></td>
                                        </tr>
                                        <tr>
                                            <td class="text-center fw-bold custom-cell-2">2</td>
                                            <td colspan="5" class="custom-cell-2"></td>
                                            <td class="custom-cell-2"></td>
                                        </tr>
                                        <tr>
                                            <td class="text-center fw-bold custom-cell-1">3</td>
                                            <td colspan="5" class="custom-cell-1"></td>
                                            <td class="custom-cell-1"></td>
                                        </tr>
                                        <tr>
                                            <td class="text-center fw-bold custom-cell-2">4</td>
                                            <td colspan="5" class="custom-cell-2"></td>
                                            <td class="custom-cell-2"></td>
                                        </tr>
                                        <tr>
                                            <td class="text-center fw-bold custom-cell-1">5</td>
                                            <td colspan="5" class="custom-cell-1"></td>
                                            <td class="custom-cell-1"></td>
                                        </tr>
                                        <tr>
                                            <td class="text-center fw-bold custom-cell-2">6</td>
                                            <td colspan="5" class="custom-cell-2 "></td>
                                            <td class="custom-cell-2"></td>
                                        </tr>
                                        <tr class="row-custom">
                                            <td class="fw-bold ultimo-aporte" colspan="5">Deudas vigentes a 31 de Diciembre de {{ txtVigencia}}</td>
                                            <td class="text-center custom-cell-3">{{arrCamposEmpleado[7].renglon2+3}}</td>
                                            <td class="custom-cell-3"></td>
                                        </tr>
                                        <tr style="background-color:rgb(225,231,238)">
                                            <td class="text-center fw-bold" colspan="7">Identificación del dependiente económico de acuerdo al parágrafo 2 del artículo 387 del Estatuto Tributario</td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <p class="fw-bold">{{arrCamposEmpleado[7].renglon2+4}}. Tipo documento </p><br>
                                                <p></p>
                                            </td>
                                            <td>
                                                <p class="fw-bold">{{arrCamposEmpleado[7].renglon2+5}}. No. documento </p><br>
                                                <p></p>
                                            </td>
                                            <td colspan="4">
                                                <p class="fw-bold">{{arrCamposEmpleado[7].renglon2+6}}. Apellidos y nombres </p><br>
                                                <p></p>
                                            </td>
                                            <td>
                                                <p class="fw-bold">{{arrCamposEmpleado[7].renglon2+7}}. Parentesco </p><br>
                                                <p></p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="6" class="nota-220">
                                                Certifico que durante el año gravable {{ txtVigencia}}:
                                                <ol>
                                                    <li>Mi patrimonio bruto no excedió de 4.500 UVT ($171.018.000)</li>
                                                    <li>Mis ingresos brutos fueron inferiores a 1.400 UVT ($53.206.000).</li>
                                                    <li>No fui responsable del impuesto sobre las ventas a 31 de diciembre de 2022</li>
                                                    <li>Mis consumos mediante tarjeta de crédito no excedieron la suma de 1.400 UVT ($53.206.000)</li>
                                                    <li>Que el total de mis compras y consumos no superaron la suma de 1.400 UVT ($53.206.000)</li>
                                                    <li>Que el valor total de mis consignaciones bancarias, depósitos o inversiones financieras no excedieron los 1.400 UVT ($53.206.000)</li>
                                                </ol>
                                                Por lo tanto, manifiesto que no estoy obligado a presentar declaración de renta y complementario por el año gravable {{ txtVigencia}}.
                                            </td>
                                            <td style="vertical-align:top">Firma del Trabajador o Pensionado</td>
                                        </tr>
                                    </tbody>
                                </table>
                                <div class="p-3 nota-220">
                                    <span class="fw-bold">Nota:</span>: este certificado sustituye para todos los efectos legales la declaración de Renta y Complementario para el trabajador o pensionado que lo firme.
                                    Para aquellos trabajadores independientes contribuyentes del impuesto unificado deberán presentar la declaración anual consolidada del Régimen Simple de Tributación (SIMPLE)
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--MODALES-->
                    <div v-show="isModal">
                        <transition name="modal">
                            <div class="modal-mask">
                                <div class="modal-wrapper">
                                    <div class="modal-container">
                                        <table class="inicio ancho">
                                            <tr>
                                                <td class="titulos" colspan="2" >.: Buscar terceros</td>
                                                <td class="cerrar" style="width:7%" @click="isModal = false">Cerrar</td>
                                            </tr>
                                        </table>
                                        <div class="bg-white">
                                            <div class="form-control m-0 p-2 w-inherit">
                                                <input type="search" placeholder="Buscar" @keyup="search('modal_tercero')" v-model="txtSearch">
                                            </div>
                                            <p class="fw-bolder m-0 p-2">Resultados de búsqueda: {{txtResults}}</p>
                                            <div class="overflow-auto max-vh-50 overflow-x-hidden p-2">
                                                <table class="table table-hover fw-normal">
                                                    <thead>
                                                        <tr>
                                                            <th>Documento</th>
                                                            <th>Nombre</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <tr v-for="(data,index) in arrTercerosCopy" :key="index" @dblclick="selectItem(data)">
                                                            <td>{{data.documento}}</td>
                                                            <td>{{data.nombre}}</td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </transition>
                    </div>
				</article>
			</section>
		</form>

		<script src="node_modules/read-excel-file/bundle/read-excel-file.min.js"></script>
		<script src="node_modules/xlsx/dist/xlsx.full.min.js"></script>
        <script src="Librerias/vue/vue.min.js"></script>
		<script src="Librerias/vue/axios.min.js"></script>
		<script type="module" src="contabilidad_vue/certificado_220_1001/cont-certificado.js?<?= date('d_m_Y_h_i_s');?>"></script>

	</body>
</html>
