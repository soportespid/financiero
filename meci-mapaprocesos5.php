<?php
	header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=utf8");
	require "comun.inc";
	require "funciones.inc";
	session_start();
	$linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");
	cargarcodigopag($_GET['codpag'],$_SESSION["nivel"]);
	date_default_timezone_set("America/Bogota");
?>
<!DOCTYPE >
<html lang="es">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1">
		<meta name="viewport" content="user-scalable=no">
		<title>:: IDEAL 10 - Meci Calidad</title>
		<link href="favicon.ico" rel="shortcut icon"/>
		<link href="css/css2.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<script type="text/javascript" src="JQuery/jquery-2.1.4.min.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script type="text/javascript" src="bootstrap/js/bootstrap.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script type="text/javascript" src="css/programas.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<style>
			.cselect{
				position: absolute;
			}
			.container {
				overflow-x:hidden; 
				overflow-y:scroll;
				width: 100%;
				height: 100%;
				display:flex;
				flex-direction: row;
				overflow: auto;
				position: relative;
				margin-left: auto;
				margin-right: auto;
				text-align: center;
				align-items: center;
				justify-content:center;
			}
			.entradaysalida{
				width: 12%;
				height: 100%;
				display:flex;
				flex-direction:row;
				max-width: 12%;
			}
			.ventana_es1 {
				width: 35%;
				height: 100%;
				margin-left: auto;
				margin-right: auto;
				text-align: center;
				align-items: center;
				display: flex;
				justify-content:center;
			}
			.ventana_es2 {
				width: 65%;
				height: 100%;
				margin-left: auto;
				margin-right: auto;
				text-align: center;
				align-items: center;
				display: flex;
				justify-content:center;
			}
			.ventana_mid {
				height: 100%;
				display:flex;
				flex-direction: column;
				max-width: 76%;
			}
			.ventana_cen {
				width: 100%;
				height: 25%;
				display:flex;
				flex-direction: column;
			}
			.ventana_cen1 {
				width: 100%;
				height: 40;
				margin-left: auto;
				margin-right: auto;
				text-align: center;
				align-items: center;
				display: flex;
				justify-content:center;
			}
			.ventana_cen2 {border:#eeeeee 1px solid;
				width: 100%;
				height: 70%;
				display: flex;
				margin-left: auto;
				margin-right: auto;
				text-align: center;
				align-items: center;
				display: flex;
				justify-content:center;
			}
			.titulo_1mp {
				text-shadow: 2px 2px #000000;
				color: #ffffff;
				background-color: #4CAF50;
				margin-left: 6px;
				border-radius: 10px;
				transform: rotate(-180deg);
				writing-mode: vertical-lr;
				width: max-content;
				font-size:25px;
			}
			.titulo_2mp {
				text-shadow: 2px 2px #000000;
				color: #ffffff;
				background-color:cornflowerblue;
				margin-left: 6px;
				border-radius: 10px;
				transform: rotate(-180deg);
				writing-mode: vertical-rl;
				width: max-content;
				font-size:19px;
				padding: 5px;
			}
			.titulo_3mp{
				text-shadow: 2px 2px #000000;
				color: #ffffff;
				background-color: #4CAF50;
				margin-left: 6px;
				border-radius: 10px;
				width: max-content;
				font-size:20px;
			}
			.titulo_4mp{
				text-shadow: 2px 2px #000000;
				color: #ffffff;
				background-color:cornflowerblue;
				margin-left: 6px;
				border-radius: 5px;
				width: max-content;
				font-size:16px;
				padding-left: 5px;
				padding-right: 5px;
				padding-bottom: 4px;
			}
		</style>
	</head>
	<body>
		<?php
			$tprocesos[] = array();
			$sqlr = "SELECT * FROM dominios WHERE nombre_dominio = 'PROCESOS_CALIDAD' ORDER BY valor_final";
			$resp = mysqli_query($linkbd,$sqlr);
			while ($row = mysqli_fetch_row($resp)){
				$tprocesos[$row[1]][0] = $row[0]; //Código desconocido por ahora
				$tprocesos[$row[1]][1] = $row[1]; //Código proceso
				$tprocesos[$row[1]][2] = $row[2]; //Nombre proceso
				$tprocesos[$row[1]][4] = $row[4]; //Tipo proceso
			}
		?> 
		<form name="form2" method="post" action="" autocomplete='off'>
			<div class="container">
				<div class="entradaysalida">
					<div class="ventana_es1">
						<legend class="titulo_1mp">&nbsp;&nbsp;&darr;&nbsp; ENTRADA &nbsp;&darr;&nbsp;&nbsp;</legend>
					</div>
					<div class="ventana_es2">
						<?php
							$sqlr="SELECT nombre FROM calprocesos WHERE estado = 'S' AND clasificacion = '5'";
							$resp = mysqli_query($linkbd,$sqlr);
							while ($row = mysqli_fetch_row($resp)){
								echo"
								<legend class='titulo_2mp'>$row[0] </legend>";
							}
						?>
					</div>
				</div>
				<div class="ventana_mid">
					<div class="ventana_cen">
						<div class="ventana_cen1">
							<legend class="titulo_3mp">&nbsp;&nbsp;&nbsp;<?php echo $tprocesos[1][2];?>&nbsp;&nbsp;&nbsp;</legend>
						</div>
						<?php
							$sqlr="SELECT nombre FROM calprocesos WHERE estado = 'S' AND clasificacion = '1'";
							$resp = mysqli_query($linkbd,$sqlr);
							$total = mysqli_num_rows($resp);
							if($total > 5){
								$conta1 = 0;
								while ($row = mysqli_fetch_row($resp)){
									$conta1++;
									if($conta1 == 1){
										echo "<div class='ventana_cen2'>";
									}
									echo"
									<legend class='titulo_4mp' style='font-size:14px !important;background-color:".$_POST['colorprocesos1']."!important;'>$row[0] $conta1</legend>";
									if($conta1 == 5){
										echo"</div>";
										$conta1 = 0;
									}
								}
								if($conta1 < 5){
									echo"</div>";
								}
							}else{
								echo "<div class='ventana_cen2'>";
								while ($row = mysqli_fetch_row($resp)){
									echo"
									<legend class='titulo_4mp' style='background-color:".$_POST['colorprocesos1']."!important;'>$row[0]</legend>";
								}
								echo"</div>";
							}
						?>
					</div>
					<div class="ventana_cen">
						<div class="ventana_cen1">
							<legend class="titulo_3mp">&nbsp;&nbsp;&nbsp;<?php echo $tprocesos[2][2];?>&nbsp;&nbsp;&nbsp;</legend>
						</div>
						<?php
							$sqlr="SELECT nombre FROM calprocesos WHERE estado = 'S' AND clasificacion = '2'";
							$resp = mysqli_query($linkbd,$sqlr);
							$total = mysqli_num_rows($resp);
							if($total > 5){
								$conta1 = 0;
								while ($row = mysqli_fetch_row($resp)){
									$conta1++;
									if($conta1 == 1){
										echo "<div class='ventana_cen2'>";
									}
									echo"
									<legend class='titulo_4mp' style='font-size:14px !important;background-color:".$_POST['colorprocesos1']."!important;'>$row[0]</legend>";
									if($conta1 == 5){
										echo"</div>";
										$conta1 = 0;
									}
								}
								if($conta1 < 5){
									echo"</div>";
								}
							}else{
								echo "<div class='ventana_cen2'>";
								while ($row = mysqli_fetch_row($resp)){
									echo"
									<legend class='titulo_4mp' style='background-color:".$_POST['colorprocesos1']."!important;'>$row[0]</legend>";
								}
								echo"</div>";
							}
						?>
					</div>
					<div class="ventana_cen">
						<div class="ventana_cen1">
							<legend class="titulo_3mp">&nbsp;&nbsp;&nbsp;<?php echo $tprocesos[3][2];?>&nbsp;&nbsp;&nbsp;</legend>
						</div>
						<?php
							$sqlr="SELECT nombre FROM calprocesos WHERE estado = 'S' AND clasificacion = '3'";
							$resp = mysqli_query($linkbd,$sqlr);
							$total = mysqli_num_rows($resp);
							if($total > 5){
								$conta1 = 0;
								while ($row = mysqli_fetch_row($resp)){
									$conta1++;
									if($conta1 == 1){
										echo "<div class='ventana_cen2'>";
									}
									echo"
									<legend class='titulo_4mp' style='font-size:14px !important;background-color:".$_POST['colorprocesos1']."!important;'>$row[0]</legend>";
									if($conta1 == 5){
										echo"</div>";
										$conta1 = 0;
									}
								}
								if($conta1 < 5){
									echo"</div>";
								}
							}else{
								echo "<div class='ventana_cen2'>";
								while ($row = mysqli_fetch_row($resp)){
									echo"
									<legend class='titulo_4mp' style='background-color:".$_POST['colorprocesos1']."!important;'>$row[0]</legend>";
								}
								echo"</div>";
							}
						?>
					</div>
					<div class="ventana_cen">
						<div class="ventana_cen1">
							<legend class="titulo_3mp">&nbsp;&nbsp;&nbsp;<?php echo $tprocesos[4][2];?>&nbsp;&nbsp;&nbsp;</legend>
						</div>
						<?php
							$sqlr="SELECT nombre FROM calprocesos WHERE estado = 'S' AND clasificacion = '4'";
							$resp = mysqli_query($linkbd,$sqlr);
							$total = mysqli_num_rows($resp);
							if($total > 5){
								$conta1 = 0;
								while ($row = mysqli_fetch_row($resp)){
									$conta1++;
									if($conta1 == 1){
										echo "<div class='ventana_cen2'>";
									}
									echo"
									<legend class='titulo_4mp' style='font-size:14px !important;background-color:".$_POST['colorprocesos1']."!important;'>$row[0]</legend>";
									if($conta1 == 5){
										echo"</div>";
										$conta1 = 0;
									}
								}
								if($conta1 < 5){
									echo"</div>";
								}
							}else{
								echo "<div class='ventana_cen2'>";
								while ($row = mysqli_fetch_row($resp)){
									echo"
									<legend class='titulo_4mp' style='background-color:".$_POST['colorprocesos1']."!important;'>$row[0]</legend>";
								}
								echo"</div>";
							}
						?>
					</div>
				</div>
				<div class="entradaysalida">
					<div class="Ventana_es2">
					<?php
							$sqlr="SELECT nombre FROM calprocesos WHERE estado = 'S' AND clasificacion = '6'";
							$resp = mysqli_query($linkbd,$sqlr);
							while ($row =mysqli_fetch_row($resp)){
								echo"
								<legend class='titulo_2mp'>$row[0] </legend>";
							}
						?>
					</div>
					<div class="Ventana_es1">
						<legend class="titulo_1mp">&nbsp;&nbsp;&darr;&nbsp; SALIDA &nbsp;&darr;&nbsp;&nbsp;</legend>
					</div>
				</div>
			</div>
		</form>
	</body>
</html>

