

/* Funcion para ordenar un arbol con las rubros mayores y auxiliares de funcionamiento, servicios a la deuda o ingresos
// @params 
// accounts: Cuentas mayores o agregadoras.
// rubrosJoin: cuentas auxiliares utilizadas.
*/
function ordenarArbol(accountsMayor, rubrosJoin){
    
    const filterAccounts = accountsMayor.map((element) => {
        let largeAccount = element[0].length;
        let a = (rubrosJoin.find((e) => e[0].substring(0, largeAccount) == element[0]));
        return a != undefined ? element : ''
    });

    const shortenAccount = filterAccounts.filter((element) => element != '');
    const completeAccounts = rubrosJoin.concat(shortenAccount);

    const ord = completeAccounts.map(e => Object.values(e));
    
    return ord.sort();
}

/* Buesca nombre de cuentas ccpet y fuentes, 
// arrRubros => es el array de rubros (ccpet-fuente), sin nombre.
//arrNombresRubro => nombre de cuentas.
*/

function buscaNombreRubros(arrRubros = [], arrNombres = []){
    
    const rubrosConNombre = arrRubros.map((element) => {
        let partesRubro = typeof element == 'string' ? element.split('-') : element[0].split('-');
        let rubro = [];
        /* let fuente = []; */
        let nombreRubro = '';
        let tamPartes = partesRubro.length;
        for(let i = 0; i < tamPartes; i++){
            if(arrNombres.length > 0){
                rubro = arrNombres.find( e => e[0] == partesRubro[i]);
            }
            rubro = rubro == undefined ? ['0','NO EXISTE'] : rubro

            nombreRubro += i == tamPartes-1 ? rubro[1] : rubro[1]+'-' 
        }
        
        /* if(arrNombresFuente.length > 0){
            fuente = arrNombresFuente.find( e => e[0] == partesRubro[1]);
        } */
        /* rubro = rubro == undefined ? 'NO EXISTE' : rubro
        fuente = fuente == undefined ? 'NO EXISTE' : fuente */
        let elementRubro = typeof element == 'string' ? element : element[0];
        return  { 
                    0: elementRubro, 
                    1: nombreRubro,
                    2: 'C'
                }
    })
    
    return rubrosConNombre;
}


//funcion para filtrar en un array de objetos (BUSCADORES)
function filtroEnArrayDeObjetos({data: data, text: text}){

    const newData = data.filter(function(item){
        item = Object.values(item);
        let tamPartes = item.length;
        let campo = '';
        for(let i = 0; i < tamPartes; i++){
            campo += i == tamPartes-1 ? item[i].toString().toUpperCase() : item[i].toString().toUpperCase()+" " 
        }
        const textData = text.toUpperCase();
        return campo.indexOf(textData) > -1
    });
    
    return newData;
}

//funcion para buscar variables enviados por url

function traeParametros(name){
    //Captura parametros
    name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
    results = regex.exec(location.search);
    return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
}

//Funcion que recibe un rubro presupuestal, se divide en partes y devuelve un objeto con el nombre de cada parte

function divideRubro(rubro){

    let partes = rubro.replace(/\s+/g, '').split('-');
    let objeto = {};

    let esFuncionamiento = partes[0].includes('.');

    if(esFuncionamiento){

        const opcionesRubro = {
            '2.1': 'funcionamiento',
            '2.2': 'deuda',
            '2.4': 'comercializacion',
            '1.1': 'ingresos',
            '1.2': 'ingresos'
        };

        const InicioRubro = partes[0].toString().substring(0, 3);

        objeto['tipoObjeto'] = opcionesRubro[InicioRubro];

        const opciones = ['cuenta', 'fuente', 'seccion_presupuestal', 'vigencia_gasto', 'medio_pago'];

        for(let i = 0; i < partes.length; i++){
            objeto[opciones[i]] = partes[i].trim();
        }

    }else{

        const opciones = ['programatico', 'bpim', 'fuente', 'seccion_presupuestal', 'vigencia_gasto', 'medio_pago'];
        objeto['tipoObjeto'] = 'inversion';

        for(let i = 0; i < partes.length; i++){
            objeto[opciones[i]] = partes[i].trim();
        }
    }

    return objeto;
}


export {ordenarArbol, buscaNombreRubros, filtroEnArrayDeObjetos, traeParametros, divideRubro};