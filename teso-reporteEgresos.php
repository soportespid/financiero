<?php

    header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=utf8");
	require 'comun.inc';
	require 'funciones.inc';
	session_start();
    if(empty($_SESSION)){
        header("location: index.php");
    }
	$linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");
	cargarcodigopag($_GET['codpag'],$_SESSION["nivel"]);
	date_default_timezone_set("America/Bogota");
?>
<!DOCTYPE >
<html lang=es>
	<head>
    <meta charset="utf-8"/>
		<meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1"/>
		<meta name="viewport" content="user-scalable=no">
		<title>:: IDEAL 10 - Informes</title>
		<link href="favicon.ico" rel="shortcut icon"/>
		<link href="css/css2z.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
		<link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
		<link href="css/css4.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
        <link href="css/tabs.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css"/>
        <link href="css/style.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css"/>
        <link rel="stylesheet" href="sweetalert2/dist/sweetalert2.min.css">
        <script type="text/javascript" src="css/programas.js"></script>
        <script src="sweetalert2/dist/sweetalert2.min.js"></script>
		<script src="vue/vue.min.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script src="css/programas.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script src="css/calendario.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
	</head>
	<body>
		<header>
			<table>
				<tr><script>barra_imagenes("info");</script><?php cuadro_titulos();?></tr>
			</table>
		</header>

        <main id="myapp" v-cloak>
            <div class="loading-container" v-show="isLoading" >
                <p class="text-loading"data-loading="IDEAL 10">IDEAL 10</p>
            </div>
            <nav>
                <table>
                    <tr><?php menu_desplegable("info");?></tr>
                </table>
                <div class="bg-white group-btn p-1" id="newNavStyle">
                    <button type="button" class="btn btn-white btn-primary-hover d-flex justify-between align-items-center" @click="mypop=window.open('info-principal.php','',''); mypop.focus();">
                        <span>Nueva ventana</span>
                        <svg class="fill-black group-hover:fill-white w-5 h-5" xmlns="http://www.w3.org/2000/svg"  viewBox="0 -960 960 960" ><path d="M320-120v-80h80v-80H160q-33 0-56.5-23.5T80-360v-400q0-33 23.5-56.5T160-840h640q33 0 56.5 23.5T880-760v400q0 33-23.5 56.5T800-280H560v80h80v80H320ZM160-360h640v-400H160v400Zm0 0v-400 400Z"/></svg>
                    </button>
                    <button type="button" @click="mypop=window.open('teso-reporteEgresos','','');mypop.focus();" class="btn btn-white btn-primary-hover d-flex justify-between align-items-center">
                        <span class="group-hover:text-white">Duplicar pantalla</span>
                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 -960 960 960"><path d="M640-160v-360H160v360h480Zm80-200v-80h80v-360H320v200h-80v-200q0-33 23.5-56.5T320-880h480q33 0 56.5 23.5T880-800v360q0 33-23.5 56.5T800-360h-80ZM160-80q-33 0-56.5-23.5T80-160v-360q0-33 23.5-56.5T160-600h480q33 0 56.5 23.5T720-520v360q0 33-23.5 56.5T640-80H160Zm400-603ZM400-340Z"></path></svg>
                    </button>
                    <button type="button" @click="exportData(1)" class="btn btn-danger btn-danger-hover d-flex justify-between align-items-center">
                        <span>Exportar PDF</span>
                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><!-- !Font Awesome Free 6.5.1 by @fontawesome - https://fontawesome.com License - https://fontawesome.com/license/free Copyright 2024 Fonticons, Inc. --><path d="M64 464l48 0 0 48-48 0c-35.3 0-64-28.7-64-64L0 64C0 28.7 28.7 0 64 0L229.5 0c17 0 33.3 6.7 45.3 18.7l90.5 90.5c12 12 18.7 28.3 18.7 45.3L384 304l-48 0 0-144-80 0c-17.7 0-32-14.3-32-32l0-80L64 48c-8.8 0-16 7.2-16 16l0 384c0 8.8 7.2 16 16 16zM176 352l32 0c30.9 0 56 25.1 56 56s-25.1 56-56 56l-16 0 0 32c0 8.8-7.2 16-16 16s-16-7.2-16-16l0-48 0-80c0-8.8 7.2-16 16-16zm32 80c13.3 0 24-10.7 24-24s-10.7-24-24-24l-16 0 0 48 16 0zm96-80l32 0c26.5 0 48 21.5 48 48l0 64c0 26.5-21.5 48-48 48l-32 0c-8.8 0-16-7.2-16-16l0-128c0-8.8 7.2-16 16-16zm32 128c8.8 0 16-7.2 16-16l0-64c0-8.8-7.2-16-16-16l-16 0 0 96 16 0zm80-112c0-8.8 7.2-16 16-16l48 0c8.8 0 16 7.2 16 16s-7.2 16-16 16l-32 0 0 32 32 0c8.8 0 16 7.2 16 16s-7.2 16-16 16l-32 0 0 48c0 8.8-7.2 16-16 16s-16-7.2-16-16l0-64 0-64z"/></svg>
                    </button>
                    <button type="button" @click="exportData(2)" class="btn btn-success btn-success-hover d-flex justify-between align-items-center">
                        <span>Exportar Excel</span>
                        <svg class="fill-black group-hover:fill-white w-4 h-4" xmlns="http://www.w3.org/2000/svg"  viewBox="0 0 384 512"><!--!Font Awesome Free 6.5.1 by @fontawesome - https://fontawesome.com License - https://fontawesome.com/license/free Copyright 2024 Fonticons, Inc. --><path d="M48 448V64c0-8.8 7.2-16 16-16H224v80c0 17.7 14.3 32 32 32h80V448c0 8.8-7.2 16-16 16H64c-8.8 0-16-7.2-16-16zM64 0C28.7 0 0 28.7 0 64V448c0 35.3 28.7 64 64 64H320c35.3 0 64-28.7 64-64V154.5c0-17-6.7-33.3-18.7-45.3L274.7 18.7C262.7 6.7 246.5 0 229.5 0H64zm90.9 233.3c-8.1-10.5-23.2-12.3-33.7-4.2s-12.3 23.2-4.2 33.7L161.6 320l-44.5 57.3c-8.1 10.5-6.3 25.5 4.2 33.7s25.5 6.3 33.7-4.2L192 359.1l37.1 47.6c8.1 10.5 23.2 12.3 33.7 4.2s12.3-23.2 4.2-33.7L222.4 320l44.5-57.3c8.1-10.5 6.3-25.5-4.2-33.7s-25.5-6.3-33.7 4.2L192 280.9l-37.1-47.6z"/></svg>
                    </button>
                    <button type="button" @click="window.location.href='teso-informestesoreria.php'" class="btn btn-success btn-success-hover d-flex justify-between align-items-center">
                        <span>Atras</span>
                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 -960 960 960"><path d="M760-200v-160q0-50-35-85t-85-35H273l144 144-57 56-240-240 240-240 57 56-144 144h367q83 0 141.5 58.5T840-360v160h-80Z"/></svg>
                    </button>
                </div>
            </nav>
            <section class="bg-white">
                <div>
                    <h2 class="titulos m-0">Reporte por egresos</h2>
                    <div class="d-flex">
                        <div class="form-control w-25">
                            <label class="form-label">Fecha inicial:</label>
                            <input type="date" v-model="txtFechaInicial">
                        </div>
                        <div class="form-control w-25">
                            <label class="form-label">Fecha final:</label>
                            <input type="date" v-model="txtFechaFinal">
                        </div>
                        <div class="form-control w-25">
                            <label class="form-label">Número de egreso:</label>
                            <input type="number" v-model="txtEgreso">
                        </div>
                        <div class="form-control w-75">
                            <label class="form-label">Tercero:</label>
                            <div class="d-flex">
                                <input type="text" class="w-25 bg-warning cursor-pointer" v-model="objTercero.codigo" @dblclick="isModalTercero=true" @change="search('cod_tercero')" >
                                <input type="text" v-model="objTercero.nombre" disabled>
                            </div>
                        </div>
                        <div class="form-control w-75">
                            <label class="form-label">Fuente: </label>
                            <div class="d-flex">
                                <input type="text" class="w-25 bg-warning cursor-pointer" v-model="objFuente.codigo" @dblclick="isModal=true" @change="search('cod_fuente')" >
                                <input type="text" v-model="objFuente.nombre" disabled>
                            </div>
                        </div>
                        <div class="form-control justify-between w-25">
                            <label class="form-label"></label>
                            <button type="button" class="btn btn-primary" @click="generate()">Generar</button>
                        </div>
                    </div>
                    <div class="table-responsive">
                        <table class="table fw-normal">
                            <thead>
                                <tr class="text-center">
                                    <th class="bg-primary text-white">Nro Egreso</th>
                                    <th class="bg-primary text-white">Fecha Egreso</th>
                                    <th class="bg-primary text-white">Nro obligación</th>
                                    <th class="bg-primary text-white">Fecha obligación</th>
                                    <th class="bg-primary text-white">Cuenta</th>
                                    <th class="bg-primary text-white">Nombre cuenta</th>
                                    <th class="bg-primary text-white">Tercero</th>
                                    <th class="bg-primary text-white">Objeto</th>
                                    <th class="bg-primary text-white">Valor bruto</th>
                                    <th class="bg-primary text-white">Valor retención</th>
                                    <th class="bg-primary text-white">Valor neto</th>
                                    <th class="bg-primary text-white">Fuente</th>
                                    <th class="bg-primary text-white">Rubro</th>
                                    <th class="bg-primary text-white">Vigencia del gasto</th>
                                    <th class="bg-primary text-white">Medio pago</th>
                                    <th class="bg-primary text-white">Valor</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr v-for="(data,index) in arrData" :key = "index">
                                    <td>{{data.id_egreso}}</td>
                                    <td>{{data.fecha}}</td>
                                    <td>{{data.id_orden}}</td>
                                    <td>{{data.fecha}}</td>
                                    <td>{{data.cuenta_contable}}</td>
                                    <td>{{data.nombre_cuenta_contable}}</td>
                                    <td>{{data.tercero}}</td>
                                    <td>{{data.concepto}}</td>
                                    <td>{{formatNum(data.valor_bruto)}}</td>
                                    <td>{{formatNum(data.valor_retencion)}}</td>
                                    <td>{{formatNum(data.valor_neto)}}</td>
                                    <td>{{data.fuente}}</td>
                                    <td>{{data.rubro}}</td>
                                    <td>{{data.vigencia}}</td>
                                    <td>{{data.medio_pago}}</td>
                                    <td>{{formatNum(data.valor)}}</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </section>
            <div v-show="isModal" class="modal">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title">Buscar fuentes</h5>
                            <button type="button" @click="isModal=false;" class="btn btn-close"><div></div><div></div></button>
                        </div>
                        <div class="modal-body">
                            <div class="d-flex flex-column">
                                <div class="form-control m-0 mb-3">
                                    <input type="search" placeholder="Buscar" v-model="txtSearch" @keyup="search('modal')" id="labelInputName">
                                </div>
                                <div class="form-control m-0 mb-3">
                                    <label class="form-label" for="labelInputName">Resultados: <span class="fw-bold">{{txtResultados}}</span></label>
                                </div>
                            </div>
                            <div class="overflow-auto max-vh-50 overflow-x-hidden " >
                                <table class="table table-hover fw-normal">
                                    <thead>
                                        <tr>
                                            <th>Item</th>
                                            <th>Fuente</th>
                                            <th>Nombre</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr v-for="(data,index) in arrFuentesCopy" :key="index" @dblclick="selectItem(data,'fuente')">
                                            <td>{{index+1}}</td>
                                            <td>{{data.codigo}}</td>
                                            <td>{{data.nombre}}</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div v-show="isModalTercero" class="modal">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title">Buscar terceros</h5>
                            <button type="button" @click="isModalTercero=false;" class="btn btn-close"><div></div><div></div></button>
                        </div>
                        <div class="modal-body">
                            <div class="d-flex flex-column">
                                <div class="form-control m-0 mb-3">
                                    <input type="search" placeholder="Buscar" v-model="txtSearchTercero" @keyup="search('modal_tercero')" id="labelInputName">
                                </div>
                                <div class="form-control m-0 mb-3">
                                    <label class="form-label" for="labelInputName">Resultados: <span class="fw-bold">{{txtResultadosTercero}}</span></label>
                                </div>
                            </div>
                            <div class="overflow-auto max-vh-50 overflow-x-hidden " >
                                <table class="table table-hover fw-normal">
                                    <thead>
                                        <tr>
                                            <th>Item</th>
                                            <th>Fuente</th>
                                            <th>Nombre</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr v-for="(data,index) in arrTercerosCopy" :key="index" @dblclick="selectItem(data)">
                                            <td>{{index+1}}</td>
                                            <td>{{data.codigo}}</td>
                                            <td>{{data.nombre}}</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </main>
		<script src="node_modules/read-excel-file/bundle/read-excel-file.min.js"></script>
		<script src="node_modules/xlsx/dist/xlsx.full.min.js"></script>
        <script src="Librerias/vue/vue.min.js"></script>
		<script src="Librerias/vue/axios.min.js"></script>
		<script type="module" src="tesoreria/reportes/js/functions_reporte_egreso.js?<?= date('d_m_Y_h_i_s');?>"></script>
	</body>
</html>
