<?php
require 'comun.inc';
require 'funciones.inc';
session_start();
cargarcodigopag(@$_GET['codpag'], @$_SESSION['nivel']);
header('Cache-control: private');
date_default_timezone_set('America/Bogota')
    ?>
<!DOCTYPE>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="es">

<head>
    <meta http-equiv="Content-Type" content="text/html" charset="iso-8859-1" />
    <meta http-equiv="X-UA-Compatible" content="IE=9" />
    <title>:: IDEAL 10 - Parametrización</title>
    <link href="css/css2.css" rel="stylesheet" type="text/css" />
    <link href="css/css3.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="css/programas.js"></script>
    <script type="text/javascript" src="css/calendario.js"></script>

    <?php titlepag(); ?>
</head>

<body>
    <IFRAME src="alertas.php" name="alertas" id="alertas" style="display:none"></IFRAME>
    <span id="todastablas2"></span>
    <table>
        <tr>
            <script>barra_imagenes("para");</script><?php cuadro_titulos(); ?>
        </tr>
        <tr><?php menu_desplegable("para"); ?></tr>
    </table>
    <div class="bg-white group-btn p-1">
        <button type="button"onclick="mypop=window.open('plan-agenda','','');mypop.focus()" class="btn btn-white btn-primary-hover d-flex justify-between align-items-center">
            <span class="group-hover:text-white">Agenda</span>
            <svg class="fill-black group-hover:fill-white w-5 h-5" xmlns="http://www.w3.org/2000/svg"
                viewBox="0 -960 960 960">
                <path
                    d="M560-564v-68q33-14 67.5-21t72.5-7q26 0 51 4t49 10v64q-24-9-48.5-13.5T700-600q-38 0-73 9.5T560-564Zm0 220v-68q33-14 67.5-21t72.5-7q26 0 51 4t49 10v64q-24-9-48.5-13.5T700-380q-38 0-73 9t-67 27Zm0-110v-68q33-14 67.5-21t72.5-7q26 0 51 4t49 10v64q-24-9-48.5-13.5T700-490q-38 0-73 9.5T560-454ZM260-320q47 0 91.5 10.5T440-278v-394q-41-24-87-36t-93-12q-36 0-71.5 7T120-692v396q35-12 69.5-18t70.5-6Zm260 42q44-21 88.5-31.5T700-320q36 0 70.5 6t69.5 18v-396q-33-14-68.5-21t-71.5-7q-47 0-93 12t-87 36v394Zm-40 118q-48-38-104-59t-116-21q-42 0-82.5 11T100-198q-21 11-40.5-1T40-234v-482q0-11 5.5-21T62-752q46-24 96-36t102-12q58 0 113.5 15T480-740q51-30 106.5-45T700-800q52 0 102 12t96 36q11 5 16.5 15t5.5 21v482q0 23-19.5 35t-40.5 1q-37-20-77.5-31T700-240q-60 0-116 21t-104 59ZM280-494Z">
                </path>
            </svg>
        </button>
        <button type="button" onclick="mypop=window.open('para-principal','','');mypop.focus();" class="btn btn-white btn-primary-hover d-flex justify-between align-items-center">
            <span>Nueva ventana</span>
            <svg class="fill-black group-hover:fill-white w-5 h-5" xmlns="http://www.w3.org/2000/svg"
                viewBox="0 -960 960 960">
                <path
                    d="M320-120v-80h80v-80H160q-33 0-56.5-23.5T80-360v-400q0-33 23.5-56.5T160-840h640q33 0 56.5 23.5T880-760v400q0 33-23.5 56.5T800-280H560v80h80v80H320ZM160-360h640v-400H160v400Zm0 0v-400 400Z">
                </path>
            </svg>
        </button>
        <button type="button" onclick="mypop=window.open('/financiero/cont-menupresupuesto','','');mypop.focus();" class="btn btn-white btn-primary-hover d-flex justify-between align-items-center">
            <span class="group-hover:text-white">Duplicar pantalla</span>
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 -960 960 960">
                <path d="M640-160v-360H160v360h480Zm80-200v-80h80v-360H320v200h-80v-200q0-33 23.5-56.5T320-880h480q33 0 56.5 23.5T880-800v360q0 33-23.5 56.5T800-360h-80ZM160-80q-33 0-56.5-23.5T80-160v-360q0-33 23.5-56.5T160-600h480q33 0 56.5 23.5T720-520v360q0 33-23.5 56.5T640-80H160Zm400-603ZM400-340Z">
                </path>
            </svg>
        </button>
        <button type="button" onclick="window.location.href='cont-programacioncontable'" class="btn btn-success btn-success-hover d-flex justify-between align-items-center">
            <span>Atras</span>
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 -960 960 960">
                <path d="M760-200v-160q0-50-35-85t-85-35H273l144 144-57 56-240-240 240-240 57 56-144 144h367q83 0 141.5 58.5T840-360v160h-80Z">
                </path>
            </svg>
        </button>
    </div>
    <form name="form2" method="post" action="">
        <table class="inicio">
            <tr>
                <td class="titulos" colspan="2">.: Parametros Presupuesto </td>
                <td class="cerrar" style="width:7%;"><a href="para-principal.php">&nbsp;Cerrar</a></td>
            </tr>

            <tr>
                <td>
                    <ol id="lista2">
                        <li onClick="location.href='cont-cuentasectores.php'" style="cursor:pointer;">Sectores - cuentas
                        </li>
                        <li onClick="location.href='cont-menuFuncionamiento.php'" style="cursor:pointer;">Funcionamiento
                        </li>
                        <li onClick="location.href='cont-menuServicio.php'" style="cursor:pointer;">Servicio de la deuda
                            p&uacute;blica</li>
                        <li onClick="location.href='cont-menuInversion.php'" style="cursor:pointer;">Inversi&oacute;n
                        </li>
                        <li onClick="location.href='cont-menuComercio.php'" style="cursor:pointer;">Gastos de
                            comercializaci&oacute;n</li>

                    </ol>
                </td>
            </tr>
        </table>
    </form>
</body>

</html>
