<?php
	header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=uft8");
	require "comun.inc";
	require "funciones.inc";
    require "funcionesSP.inc.php";
	session_start();
	$linkbd=conectar_v7();
	$linkbd -> set_charset("utf8");
	cargarcodigopag($_GET['codpag'],$_SESSION["nivel"]);
	date_default_timezone_set("America/Bogota");
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>IDEAL 10 - Contabilidad</title>
    <link href="favicon.ico" rel="shortcut icon"/>
    <link href="css/css2z.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
    <link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
    <link href="css/css4.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
    <link href="css/tabs.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css"/>
    <link rel="stylesheet" href="sweetalert2/dist/sweetalert2.min.css">
    <script src="sweetalert2/dist/sweetalert2.min.js"></script>
    <script src="vue/vue.min.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
    <script src="css/programas.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
    <script src="css/calendario.js?<?php echo date('d_m_Y_h_i_s');?>"></script>

    <script>
        function buscaOrden() {
            document.form2.submit();
        }

        function anterior() {

            var ordenAnterior = document.form2.anterior.value;

            if (ordenAnterior != "") {
                document.form2.consecutivo.value = parseInt(ordenAnterior);
                document.form2.submit();    
            }
        }

        function siguiente() {

            var ordenSiguiente = document.form2.siguiente.value;

            if (ordenSiguiente != "") {
                document.form2.consecutivo.value = parseInt(ordenSiguiente);
                document.form2.submit();   
            }
        }

        function reflejar() {
            Swal.fire({
                icon: 'question',
                title: 'Seguro que quieres reflejar?',
                showDenyButton: true,
                confirmButtonText: 'Guardar',
                denyButtonText: 'Cancelar',
                }).then((result) => {
                    if (result.isConfirmed) {
                        document.form2.oculto.value='2';
                        document.form2.submit();
                    } 
                }
            )
        }
    </script>
</head>
<body>
    <IFRAME src="alertas.php" name="alertas" id="alertas" style="display:none"></IFRAME>
	<span id="todastablas2"></span>
    <table>
        <tr><script>barra_imagenes("cont");</script><?php cuadro_titulos();?></tr>	 
        <tr><?php menu_desplegable("cont");?></tr>
        <tr>
            <td colspan="3" class="cinta">
                <a class="mgbt"><img src="imagenes/add2.png"/></a>
                <a class="mgbt"><img src="imagenes/guardad.png"/></a>
                <a href="cont-ordenactivacion-reflejar.php" class="mgbt"><img src="imagenes/busca.png" title="Buscar"/></a>
                <a onClick="mypop=window.open('plan-agenda.php','','');mypop.focus()" class="mgbt"><img src="imagenes/agenda1.png" title="Agenda" /></a>
                <a class="mgbt" onClick="mypop=window.open('cont-principal.php','','');mypop.focus();"><img src="imagenes/nv.png" title="Nueva ventana"></a>
                <a onClick="mypop=window.open('<?php echo $url2; ?>','','');mypop.focus();" class="tooltip bottom mgbt"><img src="imagenes/duplicar_pantalla.png" title="Duplicar pesta&ntilde;a">
                <a href="#" class="mgbt" onClick="reflejar()"><img src="imagenes/reflejar1.png" title="Reflejar" style="width:24px;"/>
                <a href="cont-reflejardocs.php" class="mgbt"><img src="imagenes/iratras.png" alt="atras" title="Atr&aacute;s"></a>
            </td>
        </tr>		  
    </table>

    <div id="bgventanamodalm" class="bgventanamodalm">
        <div id="ventanamodalm" class="ventanamodalm">
            <IFRAME src="" name="ventanam" marginWidth=0 marginHeight=0 frameBorder=0 id="ventanam" frameSpacing=0 style=" width:700px; height:130px; top:200; overflow:hidden;"> 
            </IFRAME>
        </div>
    </div>

    <?php

    if(!$_POST['oculto']) {
        $query = "SELECT codigo FROM acticrearact ORDER BY codigo DESC LIMIT 1";
        $row = mysqli_fetch_assoc(mysqli_query($linkbd, $query));

        $_POST['consecutivo'] = $row['codigo'];
    }

    $sqlAnterior = "SELECT codigo FROM acticrearact WHERE codigo = (SELECT MAX(codigo) FROM acticrearact WHERE codigo < '$_POST[consecutivo]')";
    $rowAnterior = mysqli_fetch_assoc(mysqli_query($linkbd, $sqlAnterior));

    $_POST['anterior'] = $rowAnterior['codigo'];

    $sqlSiguiente = "SELECT codigo FROM acticrearact WHERE codigo = (SELECT MIN(codigo) FROM acticrearact WHERE codigo > '$_POST[consecutivo]')";
    $rowSiguiente = mysqli_fetch_assoc(mysqli_query($linkbd, $sqlSiguiente));
    
    $_POST['siguiente'] = $rowSiguiente['codigo'];

    $sqlCabActivo = "SELECT codigo, fechareg, documento, valor, estado, origen FROM acticrearact WHERE codigo = '$_POST[consecutivo]' ORDER BY codigo DESC LIMIT 1";
    $rowCabActivo = mysqli_fetch_assoc(mysqli_query($linkbd, $sqlCabActivo));

    switch ($rowCabActivo['estado']) {
        case 'S':
            $estado = "ACTIVO";
            $estilo = "background-color:#0CD02A; color:white; text-align:center;";
            break;
            
        case 'N':
            $estado = "ANULADO";
            $estilo = "background-color:#FF0000; color:white; text-align:center;";
            break;

        case 'R':
            $estado = "REVERSADO";
            $estilo = "background-color:#FF0000; color:white; text-align:center;";
            break;
    }

    $_POST['consecutivo'] = $rowCabActivo['codigo'];
    $_POST['fecha'] = date("d-m-Y", strtotime($rowCabActivo['fechareg']));
    $_POST['origen'] = $rowCabActivo['origen'];
    $_POST['rp'] = $rowCabActivo['documento'];
    $_POST['valorTotal'] = "$ ".number_format($rowCabActivo['valor'],$_SESSION["ndecimales"],$_SESSION["spdecimal"],$_SESSION["spmillares"]);
    $_POST['estado'] = $estado;
    ?>

    <form name="form2" method="post" action="">

    <input type="hidden" value="1" name="oculto">
    <input type="hidden" name="anterior" id="anterior" value="<?php echo $_POST['anterior'] ?>">
    <input type="hidden" name="siguiente" id="siguiente" value="<?php echo $_POST['siguiente'] ?>">
    
        <table class="inicio">
            <tr>
                <td class="titulos" colspan="12">:: Reflejar orden de activación PPE</td>
                <td class="cerrar" style="width:7%" onClick="location.href='serv-principal.php'">Cerrar</td>
            </tr>

            <tr>
                <td style="width:2.5cm" class="tamano01">Número orden: </td>
                <td style="width:10%">
                    <a href="#" onclick="anterior();"><img src="imagenes/back.png" alt="anterior"></a>
                    <input type="text" name="consecutivo" id="consecutivo" value="<?php echo $_POST['consecutivo']?>" onblur="buscaOrden();" style="width:50%; text-align:center;">
                    <a href="#" onclick="siguiente();"><img src="imagenes/next.png" alt="siguiente"></a> 
                </td>

                <td style="width:2.5cm" class="tamano01">Fecha: </td>
                <td>
                    <input type="text" id="fecha" name="fecha" value="<?php echo $_POST['fecha'] ?>" style="text-align:center;" readonly>
                </td>

                <td style="width:2.5cm" class="tamano01">Origen: </td>
                <td>
                    <input type="text" id="origen" name="origen" value="<?php echo $_POST['origen'] ?>" style="text-align:center;" readonly>
                </td>

                <td style="width:2.5cm" class="tamano01">Registro: </td>
                <td>
                    <input type="text" id="rp" name="rp" value="<?php echo $_POST['rp'] ?>" style="text-align:center;" readonly>
                </td>

                <td style="width:2.5cm" class="tamano01">Valor total: </td>
                <td>
                    <input type="text" id="valorTotal" name="valorTotal" value="<?php echo $_POST['valorTotal'] ?>" style="text-align:center;" readonly>
                </td>

                <td style="width:2.5cm" class="tamano01">Estado: </td>
                <td>
                    <input type="text" id="estado" name="estado" value="<?php echo $_POST['estado'] ?>" style="<?php echo $estilo ?>" readonly>
                </td>
            </tr>
        </table>

        <div class='subpantalla' style='height:64vh; width:99.2%; margin-top:0px; overflow-x:hidden; resize: vertical;'>
            <table class='inicio'>
                <thead>
                    <tr>
                        <th class="titulosnew00" style="width:10%;">Placa</th>
                        <th class="titulosnew00">Nombre</th>
                        <th class="titulosnew00" style="width:5%;">Clase</th>
                        <th class="titulosnew00" style="width:5%;">Grupo</th>
                        <th class="titulosnew00" style="width:5%;">Tipo</th>
                        <th class="titulosnew00" style="width:5%;">Prototipo</th>
                        <th class="titulosnew00" style="width:5%;">Dependencia</th>
                        <th class="titulosnew00" style="width:5%;">Ubicación</th>
                        <th class="titulosnew00" style="width:5%;">Disposición</th>
                        <th class="titulosnew00" style="width:5%;">CC</th>
                        <th class="titulosnew00" style="width:10%;">Valor</th>
                        <th class="titulosnew00" style="width:10%;">Cuenta Debito</th>
                        <th class="titulosnew00" style="width:10%;">Cuenta Credito</th>
                        <th class="titulosnew00" style="width:5%;">Estado</th>
                    </tr>
                </thead>

                <?php
                $sqlDetalles = "SELECT placa, nombre, clasificacion, grupo, tipo, prototipo, area, ubicacion, dispoact, cc, valor, YEAR(fechact), estadoactivo FROM acticrearact_det WHERE codigo = '$_POST[consecutivo]'";
                $resDetalles = mysqli_query($linkbd, $sqlDetalles);
                while ($rowDetalles = mysqli_fetch_assoc($resDetalles)) {
                    
                    /* Buscamos cuenta debito que es la que esta programada para cada activo - programacion activos fijos
                    Para encontrar la columna cuenta activo debemos tomar los primeros 6 digitos de la placa, disposicion y cc */
                    $tipo = substr($rowDetalles['placa'],0,6);

                    $sqlDebito = "SELECT cuenta_activo FROM acti_activos_det WHERE tipo LIKE '$tipo' AND disposicion_activos = '$rowDetalles[dispoact]' AND centro_costos LIKE '$rowDetalles[cc]'";
                    $rowDebito = mysqli_fetch_assoc(mysqli_query($linkbd, $sqlDebito));

                    /* Buscamos la cuenta credito que depende del origen del 1 - 4 van por registro presupuestral y de 5 en adelante
                    se busca por origen de activos*/

                    if ($_POST['origen'] < 4) {
                        $vigencia = $rowDetalles['YEAR(fechact)'];

                        $sqlCredito = "SELECT cuenta_debito FROM ccpetdc_detalle WHERE idrp = '$_POST[rp]' AND vigencia = '$vigencia'";
                        $rowCredito = mysqli_fetch_assoc(mysqli_query($linkbd, $sqlCredito));

                        $cuentaCredito = $rowCredito['cuenta_debito'];
                    }
                    else if ($_POST['origen'] == 4) {
                        
                        $sqlConceptosContables = "SELECT CC.codigo, CC.nombre, CCD.cuenta FROM conceptoscontables AS CC INNER JOIN conceptoscontables_det AS CCD ON CCD.codigo = CC.codigo WHERE CC.modulo = 5 AND CC.tipo = 'CT' AND CCD.tipo = 'CT' AND CCD.cc = '01' ORDER BY CCD.fechainicial DESC";
                        $rowConceptosContables = mysqli_fetch_row(mysqli_query($linkbd, $sqlConceptosContables));
        
                        $cuentaCredito = $rowConceptosContables[2];
                    }
                    else if ($_POST['origen'] > 4) {

                        $codigo = $_POST['origen'] - 4;

                        if ($codigo <= 9) {
                            $codigo = '0' . $codigo;
                        }

                        $sqlCredito = "SELECT cuenta FROM actiorigenes_det WHERE codigo = '$codigo' AND credito = 'S' AND estado = 'S'";
                        $rowCredito = mysqli_fetch_assoc(mysqli_query($linkbd,$sqlCredito));

                        $cuentaCredito = $rowCredito['cuenta'];
                    }
                ?>
                    <tbody>
                        <tr class="saludo1a">
                            <td style="width:5%; text-align:center;"><?php echo $rowDetalles['placa'] ?></td>
                            <td style="text-align:center;"><?php echo $rowDetalles['nombre'] ?></td>
                            <td style="width:5%; text-align:center;"><?php echo $rowDetalles['clasificacion'] ?></td>
                            <td style="width:5%; text-align:center;"><?php echo $rowDetalles['grupo'] ?></td>
                            <td style="width:5%; text-align:center;"><?php echo $rowDetalles['tipo'] ?></td>
                            <td style="width:5%; text-align:center;"><?php echo $rowDetalles['prototipo'] ?></td>
                            <td style="width:5%; text-align:center;"><?php echo $rowDetalles['area'] ?></td>
                            <td style="width:5%; text-align:center;"><?php echo $rowDetalles['ubicacion'] ?></td>
                            <td style="width:5%; text-align:center;"><?php echo $rowDetalles['dispoact'] ?></td>
                            <td style="width:5%; text-align:center;"><?php echo $rowDetalles['cc'] ?></td>
                            <td style="width:10%; text-align:center; "><?php echo "$ ".number_format($rowDetalles['valor'],$_SESSION["ndecimales"],$_SESSION["spdecimal"],$_SESSION["spmillares"]) ?></td>
                            <td style="width:10%; text-align:center;"><?php echo $rowDebito['cuenta_activo'] ?></td>
                            <td style="width:10%; text-align:center;"><?php echo $cuentaCredito ?></td>
                            <td style="width:5%; text-align:center;"><?php echo $rowDetalles['estadoactivo'] ?></td>
                        </tr>
                    </tbody>
                <?php  
                }
                ?>
            </table>		
        </div>
    </form>
            
    <?php 
    if($_POST['oculto'] == '2') {

        $tipoComprobante = "70";

        $sqlr="SELECT nit FROM configbasica WHERE estado='S'";
        $row = mysqli_fetch_assoc(mysqli_query($linkbd, $sqlr));

        $nit = $row['nit'];
        $consecutivo = $_POST['consecutivo'];

        $queryClean = "DELETE FROM comprobante_det WHERE id_comp = '$tipoComprobante $consecutivo'";
        mysqli_query($linkbd, $queryClean);

        $updateCab = "UPDATE comprobante_cab SET concepto = 'Orden de activacion numero $_POST[consecutivo]' WHERE tipo_comp = '$tipoComprobante' AND numerotipo = '$consecutivo'";
        mysqli_query($linkbd, $updateCab);

        $sqlDetalles = "SELECT placa, nombre, clasificacion, grupo, tipo, prototipo, area, ubicacion, dispoact, cc, valor, YEAR(fechact), estadoactivo FROM acticrearact_det WHERE codigo = '$_POST[consecutivo]'";
        $resDetalles = mysqli_query($linkbd, $sqlDetalles);
        while ($rowDetalles = mysqli_fetch_assoc($resDetalles)) {

            $vigencia = $rowDetalles['YEAR(fechact)'];
            $tipo = substr($rowDetalles['placa'],0,6);

            $sqlDebito = "SELECT cuenta_activo FROM acti_activos_det WHERE tipo LIKE '$tipo' AND disposicion_activos = '$rowDetalles[dispoact]' AND centro_costos LIKE '$rowDetalles[cc]'";
            $rowDebito = mysqli_fetch_assoc(mysqli_query($linkbd, $sqlDebito));

            $cuentaDebito = $rowDebito['cuenta_activo'];
            /* Buscamos la cuenta credito que depende del origen del 1 - 4 van por registro presupuestral y de 5 en adelante
            se busca por origen de activos*/

            if ($_POST['origen'] < 4) {
                $sqlCredito = "SELECT cuenta_debito FROM ccpetdc_detalle WHERE idrp = '$_POST[rp]' AND vigencia = '$vigencia'";
                $rowCredito = mysqli_fetch_assoc(mysqli_query($linkbd, $sqlCredito));

                $cuentaCredito = $rowCredito['cuenta_debito'];
            }
            else if ($_POST['origen'] == 4) {
                
                $sqlConceptosContables = "SELECT CC.codigo, CC.nombre, CCD.cuenta FROM conceptoscontables AS CC INNER JOIN conceptoscontables_det AS CCD ON CCD.codigo = CC.codigo WHERE CC.modulo = 5 AND CC.tipo = 'CT' AND CCD.tipo = 'CT' AND CCD.cc = '01' ORDER BY CCD.fechainicial DESC";
                $rowConceptosContables = mysqli_fetch_row(mysqli_query($linkbd, $sqlConceptosContables));

                $cuentaCredito = $rowConceptosContables[2];
            }
            else if ($_POST['origen'] > 4) {

                $codigo = $_POST['origen'] - 4;

                if ($codigo <= 9) {
                    $codigo = '0' . $codigo;
                }

                $sqlCredito = "SELECT cuenta FROM actiorigenes_det WHERE codigo = '$codigo' AND credito = 'S' AND estado = 'S'";
                $rowCredito = mysqli_fetch_assoc(mysqli_query($linkbd,$sqlCredito));

                $cuentaCredito = $rowCredito['cuenta'];
            }


            //Debito
            $queryCompDebito="insert into comprobante_det (id_comp,cuenta,tercero,centrocosto,detalle,cheque,valdebito,valcredito,estado,vigencia, numacti) values ('$tipoComprobante $consecutivo', '$cuentaDebito','$nit','$rowDetalles[cc]','$rowDetalles[nombre]','','$rowDetalles[valor]',0,'1','$vigencia','$rowDetalles[placa]')";
            mysqli_query($linkbd, $queryCompDebito);

            //Credito
            $queryCompCredito="insert into comprobante_det (id_comp,cuenta,tercero,centrocosto,detalle,cheque,valdebito,valcredito,estado,vigencia, numacti) values ('$tipoComprobante $consecutivo', '$cuentaCredito','$nit','$rowDetalles[cc]','$rowDetalles[nombre]','',0,'$rowDetalles[valor]','1','$vigencia','$rowDetalles[placa]')";
            mysqli_query($linkbd, $queryCompCredito);
        }

        echo "
        <script>
            Swal.fire({
                icon: 'success',
                title: 'Reflejado exitoso!',
                }).then((result) => {
                if (result.value) {
                    document.form2.submit();
                } 
            })
        </script>";
    }
    ?>
</body>
</html>