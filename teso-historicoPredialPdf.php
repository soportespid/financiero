<?php
    require_once("tcpdf/tcpdf_include.php");
	require('comun.inc');
	require "funciones.inc";
    require 'funcionesSP.inc.php';
	date_default_timezone_set("America/Bogota");
    /*ini_set('display_errors', '1');
    ini_set('display_startup_errors', '1');
    error_reporting(E_ALL);*/
	session_start();

    class Plantilla{
        private $linkbd;
        private $arrData;
        public function __construct() {
            $this->linkbd = conectar_v7();
            $this->linkbd->set_charset("utf8");
        }
        public function search(string $search){
            if(!empty($_SESSION)){
                $request = $this->selectData($search);
                $detalle = $this->selectDetalle($request['codigo_catastro']);
                $arrResponse = array("data"=>$request,"detalle"=>$detalle);
                return $arrResponse;
            }
        }
        public function selectData($search){
            $sql="SELECT
            p.id,p.codigo_catastro,
            p.main_propietario as ord,p.total as tot,
            pp.documento,
            pp.nombre_propietario as nombrepropietario,
            pa.valor_avaluo as avaluo,pa.tasa_por_mil as tasa,
            pa.vigencia
            FROM predios p
            LEFT JOIN predio_propietarios pp ON p.id = pp.predio_id AND p.main_propietario = pp.orden
            LEFT JOIN (
            	SELECT * FROM predio_avaluos
                WHERE (vigencia) IN (
                    SELECT MAX(vigencia) FROM predio_avaluos
                    )
            ) pa ON p.id = pa.predio_id
            WHERE (p.codigo_catastro like '$search%' OR pp.documento like '$search%'
            OR pp.nombre_propietario like '$search%') GROUP BY p.id;";
            $request = mysqli_query($this->linkbd,$sql)->fetch_assoc();

            $sql_info = "SELECT
            tp.direccion,tp.hectareas as ha,
            tp.metros_cuadrados as met2,
            tp.area_construida as areacon,
            tp.created_at,
            CONCAT(de.nombre,' ( Código: ',cd.codigo,')') as destino,
            CONCAT(pt.nombre,' (Código: ',pt.codigo,')') as tipopredio
            FROM predio_informacions tp
            LEFT JOIN predio_tipos pt ON tp.predio_tipo_id = pt.id
            LEFT JOIN codigo_destino_economicos cd ON cd.id = tp.codigo_destino_economico_id
            LEFT JOIN destino_economicos de ON de.id = cd.destino_economico_id
            WHERE tp.predio_id = '$request[id]' AND tp.created_at = (SELECT MAX(created_at) FROM predio_informacions WHERE predio_id = '$request[id]')
            GROUP BY tp.predio_id";
            $request_info = mysqli_query($this->linkbd,$sql_info)->fetch_assoc();

            $request['direccion'] = $request_info['direccion'];
            $request['ha'] = $request_info['ha'];
            $request['met2'] = $request_info['met2'];
            $request['areacon'] = $request_info['areacon'];
            $request['created_at'] = $request_info['created_at'];
            $request['destino'] = $request_info['destino'];
            $request['tipopredio'] = $request_info['tipopredio'];
            return $request;
        }
        public function selectDetalle($cedula){
            $sql = "SELECT t.idpredial,t.estado,td.vigliquidada,DATE_FORMAT(t.fecha,'%d/%m/%Y') as fechali,
            td.avaluo,td.tasav,td.predial,td.intpredial,td.bomberil,td.intbomb,
            td.medioambiente,td.intmedioambiente,td.descuentos,td.totaliquidavig
            FROM tesoliquidapredial t
            INNER JOIN tesoliquidapredial_det td
            ON t.idpredial = td.idpredial
            WHERE t.codigocatastral = '$cedula'";
            $request = mysqli_fetch_all(mysqli_query($this->linkbd,$sql),MYSQLI_ASSOC);
            if(!empty($request)){
                $total = count($request);
                for ($i=0; $i < $total ; $i++) {
                    $estado = $request[$i]['estado'];
                    $request[$i]['recibo'] ="";
                    if($estado == "P"){
                        $idPredial = $request[$i]['idpredial'];
                        $request_recibo = mysqli_query(
                            $this->linkbd,
                            "SELECT r.id_recibos, DATE_FORMAT(r.fecha,'%d/%m/%Y') as fecha,
                            b.ncuentaban as cuenta
                            FROM tesoreciboscaja r
                            INNER JOIN tesobancosctas b
                            ON b.cuenta = r.cuentabanco
                            WHERE r.id_recaudo = $idPredial"
                        )->fetch_assoc();
                        $request[$i]['recibo'] = $request_recibo;
                    }
                }
            }
            return $request;
        }

    }
	class MYPDF extends TCPDF{

		public function Header()
		{
			$linkbd = conectar_v7();
			$linkbd -> set_charset("utf8");
			$sqlr="select *from configbasica where estado='S' ";
			//echo $sqlr;
			$res=mysqli_query($linkbd, $sqlr);
			while($row=mysqli_fetch_row($res))
			{
				$nit=$row[0];
				$rs=$row[1];

			}
			//Parte Izquierda
			$this->Image('imagenes/escudo.jpg',  13, 13, 20, 20, 'JPG', '', 'T', true, 300, '', false, false, 0, false, false, false);// Logo
			$this->SetFont('helvetica','B',8);
			$this->SetY(10);
			$this->RoundedRect(10, 10, 277, 25, 1,'');
			$this->Cell(0.1);
			$this->Cell(26,25,'','R',0,'L');
			$this->SetY(8);
			$this->SetX(80);
			$this->SetFont('helvetica','B',9);
			$this->Cell(160,15,strtoupper("$rs"),0,0,'C');
			$this->SetFont('helvetica','B',7);
			$this->SetY(12);
			$this->SetX(80);
			$this->Cell(160,15,'NIT: '.$nit,0,0,'C');
			//*****************************************************************************************************************************
			$this->SetFont('helvetica','B',9);
			$this->SetY(23);
			$this->SetX(36);
            $this->Cell(251,12,"HISTÓRICO PREDIAL",'T',0,'C');


            $this->SetFont('helvetica','B',7);
			$this->SetY(10);
			$this->SetX(260);
			$this->Cell(30,7," FECHA: ".date("d/m/Y"),"L",0,'L');
			$this->SetY(17);
			$this->SetX(260);
			$this->Cell(30,6,"","L",0,'C');

			//**********************************************************
			$this->SetFont('times','B',10);
			$this->ln(12);
			//**********************************************************
		}
        public function ColoredTable($data) {
            // Colors, line width and bold font
            $this->SetFillColor(222, 222, 222);
            $this->SetTextColor(000);
            //$this->SetDrawColor(128, 0, 0);
            $this->SetLineWidth(0.3);
            $this->SetFont('helvetica','B',7);
            // Header
            $w = array(15, 15, 13,15,16,13,20,10,15,15,15,20,20,15,20,20,20,20);
            $arrEstado = array("S"=>"Por pagar","P"=>"Pago","N"=>"Anulado");
            $header =array(
                "No liquidación",
                "Fecha liquidación",
                "Recibo",
                "Fecha recibo",
                "cuenta",
                "Periodo",
                "Avaluo",
                "Tasa x mil",
                "Predial",
                "Interés predial",
                "Bomberil",
                "Interés bomberil",
                "Ambiente",
                "Interés ambiente",
                "Descuentos",
                "Total liquidado",
                "Estado"
            );
            $num_headers = count($header);
            $this->SetFillColor(153,221,255);
            for ($i=0; $i < $num_headers; $i++) {
                $this->SetDrawColor(255);
                $this->MultiCell($w[$i],8,$header[$i],"LRB",'C',true,0,'','',true,0,false,true,0,'M',true);
            }
            $this->SetFont('helvetica','',6);
            $this->Ln();
            // Color and font restoration
            $this->SetFillColor(245,245,245);
            $this->SetTextColor(0);
            $this->SetFont('');
            // Data
            $fill = 0;
            $total = 0;
            foreach($data as $row) {
                $alturas = 5;
                $this->MultiCell($w[0],$alturas,$row['idpredial'],0,'C',$fill,0,'','',true,0,false,true,0,'M',true);
                $this->MultiCell($w[1],$alturas,$row['fechali'] ,0,'C',$fill,0,'','',true,0,false,true,0,'M',true);
                $this->MultiCell($w[2],$alturas,$row['estado'] == "P" ? $row['recibo']['id_recibos'] : "-" ,0,'C',$fill,0,'','',true,0,false,true,0,'M',true);
                $this->MultiCell($w[3],$alturas,$row['estado'] == "P" ? $row['recibo']['fecha'] : "-",0,'C',$fill,0,'','',true,0,false,true,0,'M',true);
                $this->MultiCell($w[4],$alturas,$row['estado'] == "P" ? $row['recibo']['cuenta'] : "-",0,'C',$fill,0,'','',true,0,false,true,0,'M',true);
                $this->MultiCell($w[5],$alturas,$row['vigliquidada'],0,'C',$fill,0,'','',true,0,false,true,0,'M',true);
                $this->MultiCell($w[6],$alturas,'$'.number_format($row['avaluo'],0,",","."),0,'C',$fill,0,'','',true,0,false,true,0,'M',true);
                $this->MultiCell($w[7],$alturas,$row['tasav'],0,'C',$fill,0,'','',true,0,false,true,0,'M',true);
                $this->MultiCell($w[8],$alturas,'$'.number_format($row['predial'],0,",","."),0,'C',$fill,0,'','',true,0,false,true,0,'M',true);
                $this->MultiCell($w[9],$alturas,'$'.number_format($row['intpredial'],0,",","."),0,'C',$fill,0,'','',true,0,false,true,0,'M',true);
                $this->MultiCell($w[10],$alturas,'$'.number_format($row['bomberil'],0,",","."),0,'C',$fill,0,'','',true,0,false,true,0,'M',true);
                $this->MultiCell($w[11],$alturas,'$'.number_format($row['intbomb'],0,",","."),0,'C',$fill,0,'','',true,0,false,true,0,'M',true);
                $this->MultiCell($w[12],$alturas,'$'.number_format($row['medioambiente'],0,",","."),0,'C',$fill,0,'','',true,0,false,true,0,'M',true);
                $this->MultiCell($w[13],$alturas,'$'.number_format($row['intmedioambiente'],0,",","."),0,'C',$fill,0,'','',true,0,false,true,0,'M',true);
                $this->MultiCell($w[14],$alturas,'$'.number_format($row['descuentos'],0,",","."),0,'C',$fill,0,'','',true,0,false,true,0,'M',true);
                $this->MultiCell($w[15],$alturas,'$'.number_format($row['totaliquidavig'],0,",","."),0,'C',$fill,0,'','',true,0,false,true,0,'M',true);
                $this->MultiCell($w[16],$alturas,$arrEstado[$row['estado']],0,'C',$fill,0,'','',true,0,false,true,0,'M',true);
                $total+=$row['total'];
                $this->Ln();
                if($this->GetY()>170){
                    $this->AddPage();
                }
                $fill=!$fill;
            }
        }
		public function Footer(){

			$linkbd = conectar_v7();
			$linkbd -> set_charset("utf8");
			$sqlr="SELECT direccion,telefono,web,email FROM configbasica WHERE estado='S'";
			$resp=mysqli_query($linkbd, $sqlr);
			$user = $_SESSION['nickusu'];
            $cedula = $_SESSION['cedulausu'];
			$fecha = date("d/m/Y H:i:s");
			$ip = $_SERVER['REMOTE_ADDR'];
			while($row=mysqli_fetch_row($resp))
			{
				$direcc=strtoupper($row[0]);
				$telefonos=$row[1];
				$dirweb=strtoupper($row[3]);
				$coemail=strtoupper($row[2]);
			}
			if($direcc!=''){$vardirec="Dirección: $direcc, ";}
			else {$vardirec="";}
			if($telefonos!=''){$vartelef="Telefonos: $telefonos";}
			else{$vartelef="";}
			if($dirweb!=''){$varemail="Email: $dirweb, ";}
			else {$varemail="";}
			if($coemail!=''){$varpagiw="Pagina Web: $coemail";}
			else{$varpagiw="";}
			$this->SetFont('helvetica', 'I', 8);
			$txt = <<<EOD
			$vardirec $vartelef
			$varemail $varpagiw
			EOD;
			$this->SetFont('helvetica', 'I', 6);
			$this->Cell(277,10,'','T',0,'T');
			$this->ln(2);
			$this->Write(0, $txt , '', 0, 'C', true, 0, false, false, 0);


            $sqlrcc="SELECT * FROM `usuarios` WHERE `cc_usu` = '$cedula'";
            $respcc=mysqli_query($linkbd, $sqlrcc);
            $rowcc=mysqli_fetch_row($respcc);
            //echo $rowcc[1];

			$this->Cell(50, 10, 'Hecho por: '.$rowcc[1], 00, false, 'C', 0, '', 0, false, 'T', 'M');
			$this->Cell(49, 10, 'Impreso por: '.$user, 0, false, 'C', 0, '', 0, false, 'T', 'M');
			$this->Cell(49, 10, 'IP: '.$ip, 0, false, 'C',0, '', 0, false, 'T', 'M');
			$this->Cell(49, 10, 'Fecha: '.$fecha, 0, false, 'C', 0, '', 0, false, 'T', 'M');
			$this->Cell(49, 10, 'IDEAL.10 S.A.S', 0, false, 'C', 0, '', 0, false, 'T', 'M');
            $this->Cell(49, 10, 'Pagina '.$this->getAliasNumPage().'/'.$this->getAliasNbPages(), 0, false, 'C', 0, '', 0, false, 'T', 'M');
		}
	}
	if($_GET){
        $obj = new Plantilla();
        $request = $obj->search($_GET['search']);
        $cabecera = $request['data'];
        $detalle = $request['detalle'];
        //dep($request);exit;


        $pdf = new MYPDF('L','mm','Letter', true, 'iso-8859-1', false);
        $pdf->SetDocInfoUnicode (true);
        // set document information
        $pdf->SetCreator(PDF_CREATOR);
        $pdf->SetAuthor('IDEALSAS');
        $pdf->SetTitle('HISTÓRICO PREDIAL');

        $pdf->SetSubject('HISTÓRICO PREDIAL');
        $pdf->SetKeywords('TCPDF, PDF, example, test, guide');
        $pdf->SetMargins(10, 38, 10);// set margins
        $pdf->SetHeaderMargin(38);// set margins
        $pdf->SetFooterMargin(17);// set margins
        $pdf->SetAutoPageBreak(TRUE, 20);// set auto page breaks
        // set some language-dependent strings (optional)
        if (@file_exists(dirname(__FILE__).'/lang/spa.php'))
        {
            require_once(dirname(__FILE__).'/lang/spa.php');
            $pdf->setLanguageArray($l);
        }
        $pdf->AddPage();

        $pdf->ln(2);
        $pdf->SetFillColor(51,153,204);
        $pdf->setTextColor(255);
        $pdf->SetFont('helvetica','B',9);
        //Información general
        $pdf->cell(277,4,'Información general','LRTB',0,'L',1);
        $pdf->ln(4);
        $pdf->SetFillColor(255,255,255);
        $pdf->setTextColor(0);
        $pdf->SetFont('helvetica','B',7);
        $pdf->cell(40,4,'Código catastral:','LRT',0,'L',1);
        $pdf->cell(30,4,'Nro documento:','LRT',0,'L',1);
        $pdf->cell(70,4,'Nombre propietario:','LRT',0,'L',1);
        $pdf->cell(28,4,'Área del terreno:','LRT',0,'L',1);
        $pdf->cell(31,4,'Área construida:','LRT',0,'L',1);
        $pdf->cell(78,4,'Dirección:','LRT',0,'L',1);
        $pdf->ln(4);
        $pdf->SetFont('helvetica','',7);
        $pdf->cell(40,4,$cabecera['codigo_catastro'],'LR',0,'L',1);
        $pdf->cell(30,4,$cabecera['documento'],'LR',0,'L',1);
        $pdf->cell(70,4,$cabecera['nombrepropietario'],'LR',0,'L',1);
        $pdf->cell(28,4,$cabecera['areacon']." m²",'LR',0,'L',1);
        $pdf->cell(31,4,$cabecera['ha']." ha"+" "+$cabecera['met2']." m²",'LR',0,'L',1);
        $pdf->cell(78,4,$cabecera['direccion'],'LR',0,'L',1);
        $pdf->ln(4);
        $pdf->SetFont('helvetica','B',7);
        $pdf->cell(40,4,'Total:','LRT',0,'L',1);
        $pdf->cell(50,4,'Orden:','LRT',0,'L',1);
        $pdf->cell(50,4,'Avaluo:','LRT',0,'L',1);
        $pdf->cell(50,4,'Vigencia:','LRT',0,'L',1);
        $pdf->cell(40,4,'Tipo predio:','LRT',0,'L',1);
        $pdf->cell(47,4,'Destino económico:','LRT',0,'L',1);
        $pdf->ln(4);
        $pdf->SetFont('helvetica','',7);


        $pdf->MultiCell(40,4,$cabecera['tot'],"LRB",'L',false,0,'','',true,0,false,true,0,'M',true);
        $pdf->MultiCell(50,4,$cabecera['ord'],"LRB",'L',false,0,'','',true,0,false,true,0,'M',true);
        $pdf->MultiCell(50,4,"$".number_format($cabecera['avaluo'],0,",","."),"LRB",'L',false,0,'','',true,0,false,true,0,'M',true);
        $pdf->MultiCell(50,4,$cabecera['vigencia'],"LRB",'L',false,0,'','',true,0,false,true,0,'M',true);
        $pdf->MultiCell(40,4,$cabecera['tipopredio'],"LRB",'L',false,0,'','',true,0,false,true,0,'M',true);
        $pdf->MultiCell(47,4,$cabecera['destino'],"LRB",'L',false,0,'','',true,0,false,true,0,'M',true);

        $pdf->ln(4);
        $pdf->SetFillColor(153,221,255);
        $pdf->SetFont('helvetica','B',9);
        $pdf->SetFillColor(51,153,204);
        $pdf->setTextColor(255);
        $pdf->cell(277,4,'Histórico','LRTB',0,'L',1);
        $pdf->ln();
        $pdf->ColoredTable($detalle);
        /*
        $pdf->SetFillColor(255,255,255);
        $pdf->SetFont('helvetica','B',7);
        $pdf->cell(20,4,'No liquidación','LRTB',0,'L',1);
        $pdf->cell(23,4,'Fecha liquidación','LRTB',0,'L',1);
        $pdf->cell(15,4,'Recibo','LRTB',0,'L',1);
        $pdf->cell(18,4,'Fecha recibo','LRTB',0,'L',1);
        $pdf->cell(30,4,'Cuenta','LRTB',0,'L',1);
        $pdf->cell(30,4,'Periodo','LRTB',0,'L',1);
        $pdf->cell(30,4,'Avaluo','LRTB',0,'L',1);
        $pdf->cell(30,4,'Tasa x mil','LRTB',0,'L',1);
        $pdf->cell(30,4,'Predial','LRTB',0,'L',1);
        $pdf->cell(30,4,'Interés predial','LRTB',0,'L',1);
        $pdf->cell(30,4,'Bomberil','LRTB',0,'L',1);
        $pdf->cell(30,4,'Interés bomberil','LRTB',0,'L',1);
        $pdf->cell(30,4,'Ambiente','LRTB',0,'L',1);
        $pdf->cell(30,4,'Interés ambiente','LRTB',0,'L',1);
        $pdf->cell(30,4,'Descuentos','LRTB',0,'L',1);
        $pdf->cell(30,4,'Total liquidado','LRTB',0,'L',1);
        $pdf->cell(30,4,'Estado','LRTB',0,'L',1);*/

        $pdf->Output('formulario_inscripcion_'.CABECERA['consecutivo'].'.pdf', 'I');
    }
?>
