<?php
	header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=iso-8859-1");
	require "comun.inc";
	require "funciones.inc";
    require "funcionesSP.inc.php";
	session_start();
	$linkbd = conectar_v7();
	cargarcodigopag($_GET['codpag'],$_SESSION["nivel"]);
	date_default_timezone_set("America/Bogota");
	$scroll=$_GET['scrtop'];
	$totreg=$_GET['totreg'];
	$idcta=$_GET['idcta'];
	$altura=$_GET['altura'];
	$filtro="'".$_GET['filtro']."'";
?>
<!DOCTYPE >
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="es">
	<head>
		<meta http-equiv="Content-type" content="text/html; charset=iso-8859-1"/>
		<meta http-equiv="X-UA-Compatible" content="IE=9"/>
		<title>:: IDEAL 10 - Servicios P&uacute;blicos</title>
		<link href="css/css2.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<link href="css/css4.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
        <link href="css/cssSP.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
        <link href="css/style.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css"/>
		<script type="text/javascript" src="css/calendario.js"></script>
		<script type="text/javascript" src="css/programas.js"></script>

		<script>	
            function despliegamodalm(_valor,_tip,mensa,pregunta)
            {
                document.getElementById("bgventanamodalm").style.visibility=_valor;

                if(_valor=="hidden")
                {
                    document.getElementById('ventanam').src="";
                }
                else
                {
                    switch(_tip)
                    {
                        case "1":	document.getElementById('ventanam').src="ventana-mensaje1.php?titulos="+mensa;break;
                        case "2":	document.getElementById('ventanam').src="ventana-mensaje3.php?titulos="+mensa;break;
                        case "3":	document.getElementById('ventanam').src="ventana-mensaje2.php?titulos="+mensa;break;
                        case "4":	document.getElementById('ventanam').src="ventana-consulta1.php?titulos="+mensa+"&idresp="+pregunta;break;	
                    }
                }
            }

			function iratras(scrtop, numpag, limreg, filtro)
			{
				var idcta = document.getElementById('codigoFactura').value;
				location.href="serv-buscarFacturacion.php?idcta="+idcta+"&scrtop="+scrtop+"&numpag="+numpag+"&limreg="+limreg+ "&filtro="+filtro;
			}

            function reversar()
            {
                var estado = document.getElementById('estado').value;
                var numeroFactura = document.getElementById('codigoFactura').value;
                var corte = document.getElementById('codigoCorte').value;

                if(estado == 'Pago Pendiente')
                {
                    location.href="serv-reversarFactura.php?cd="+numeroFactura+'&corte='+corte;
                }
                else
                {
                    despliegamodalm('visible','2','No se puede realizar reversion a esta factura');
                }
                
            }
		</script>

		<?php titlepag();?>

	</head>

	<body>
		<IFRAME src="alertas.php" name="alertas" id="alertas" style="display:none"></IFRAME>
		<span id="todastablas2"></span>
		<?php
			$numpag=@ $_GET['numpag'];
			$limreg=@ $_GET['limreg'];
			$scrtop=26*$totreg;
		?>
		<table>
			<tr><script>barra_imagenes("serv");</script><?php cuadro_titulos();?></tr>
			<tr><?php menu_desplegable("serv");?></tr>
			<tr>
				<td colspan="3" class="cinta">
                    <a class="mgbt"><img src="imagenes/add2.png"/></a>

					<a class="mgbt"><img src="imagenes/guardad.png" title="Guardar"/></a>

					<a href="serv-buscarFacturacion.php" class="mgbt"><img src="imagenes/busca.png" title="Buscar"/></a>

					<a onclick="mypop=window.open('plan-agenda.php','','');mypop.focus()" class="mgbt"><img src="imagenes/agenda1.png" title="Agenda" /></a>

					<a onclick="mypop=window.open('serv-principal.php','','');mypop.focus();" class="mgbt"><img src="imagenes/nv.png" title="Nueva ventana"></a>

					<a onclick="mypop=window.open('<?php echo $url2; ?>','','');mypop.focus();" class="mgbt"><img src="imagenes/duplicar_pantalla.png" title="Duplicar pesta&ntilde;a"></a>

					<a onClick="iratras(<?php echo "$scrtop, $numpag, $limreg, $filtro"; ?>)" class="mgbt"><img src="imagenes/iratras.png" title="Atr&aacute;s"></a>

                    <a onclick="reversar()" class="mgbt"><img src="imagenes/reversado.png" alt="Reversar Documento"></a>
                </td>
			</tr>
		</table>
        
		<div id="bgventanamodalm" class="bgventanamodalm">
			<div id="ventanamodalm" class="ventanamodalm">
				<IFRAME src="" name="ventanam" marginWidth=0 marginHeight=0 frameBorder=0 id="ventanam" frameSpacing=0 style=" width:700px; height:130px; top:200; overflow:hidden;"></IFRAME>
			</div>
		</div>

		<form name="form2" method="post" action="">
			<?php
				if(@$_POST['oculto'] == "")
				{
					$sqlCorte = "SELECT numero_corte,fecha_inicial,fecha_final,fecha_limite_pago,fecha_impresion,servicio FROM srvcortes WHERE numero_corte = '".$_GET['corte']."' ";
                    $resCorte = mysqli_query($linkbd,$sqlCorte);
                    $rowCorte = mysqli_fetch_row($resCorte);

                    $sqlCorteDetalles = "SELECT id_cliente,numero_facturacion,estado_pago FROM srvcortes_detalle WHERE id_corte = '".$_GET['corte']."' AND numero_facturacion = '".$_GET['idban']."' ";
                    $resCorteDetalles = mysqli_query($linkbd,$sqlCorteDetalles);
                    $rowCorteDetalles = mysqli_fetch_row($resCorteDetalles);

                    $sqlCliente = "SELECT id_tercero,cod_usuario,cod_catastral,id_barrio,id_estrato,id_vereda FROM srvclientes WHERE id = '$rowCorteDetalles[0]' ";
                    $resCliente = mysqli_query($linkbd,$sqlCliente);
                    $rowCliente = mysqli_fetch_row($resCliente);

                    $sqlDireccion = "SELECT direccion FROM srvdireccion_cliente WHERE id_cliente = '$rowCorteDetalles[0]' ";
                    $resDireccion = mysqli_query($linkbd,$sqlDireccion);
                    $rowDireccion = mysqli_fetch_row($resDireccion);

                    $sqlTercero = "SELECT cedulanit FROM terceros WHERE id_tercero = '$rowCliente[0]' ";
                    $resTercero = mysqli_query($linkbd,$sqlTercero);
                    $rowTercero = mysqli_fetch_row($resTercero);

                    $nombreCompleto = encuentraNombreTerceroConIdCliente($rowCorteDetalles[0]);

                    $sqlBarrio = "SELECT nombre FROM srvbarrios WHERE id = '$rowCliente[3]' ";
                    $resBarrio = mysqli_query($linkbd,$sqlBarrio);
                    $rowBarrio = mysqli_fetch_row($resBarrio);
                    
                    if($rowBarrio[0] == '')
                    {
                        $sqlBarrio = "SELECT nombre FROM srvveredas WHERE id = '$rowCliente[5]' ";
                        $resBarrio = mysqli_query($linkbd,$sqlBarrio);
                        $rowBarrio = mysqli_fetch_row($resBarrio);
                    }

                    $sqlEstrato = "SELECT descripcion,uso FROM srvestratos WHERE id = '$rowCliente[4]' ";
                    $resEstrato = mysqli_query($linkbd,$sqlEstrato);
                    $rowEstrato = mysqli_fetch_row($resEstrato);

                    $sqlUsoDeSuelo = "SELECT nombre FROM srvusosdesuelo WHERE id = '$rowEstrato[1]' ";
                    $resUsoDeSuelo = mysqli_query($linkbd,$sqlUsoDeSuelo);
                    $rowUsoDeSuelo = mysqli_fetch_row($resUsoDeSuelo);

                    //Validacion de estado de pago
                    switch ($rowCorteDetalles[2]) 
                    {
                        case 'S': 	$estado="Pago Pendiente";
                                    $color = "text-align:center; height:30px; background: #F2FA49";
                                    break;
                        case 'P':	$estado="Pago Realizado";
                                    $color = "text-align:center; height:30px; background: #8FFF4B";
                                    break;
                        case 'V':	$estado="Pago Vencido";
                                    $color = "text-align:center; height:30px; background: #FA4949";
                                    break;
                        case 'R':	$estado="Factura Reversada";
                                    $color = "text-align:center; height:30px; background: #ACACAC";
                                    break;
                        case 'A':	$estado="Acuero de Pago";
                                    $color = "text-align:center; height:30px; background: #137ED8";
                                    break;
                        default:	$estado="";
                    }

                    //Variables para guardar en detalles facturacion
                    $saldos = array();
                    $abonos = array();

                    $sqlAsignacionServicios = "SELECT id_servicio FROM srvasignacion_servicio WHERE id_clientes = '$rowCorteDetalles[0]' AND estado = 'S' ";
                    $resAsignacionServicios = mysqli_query($linkbd,$sqlAsignacionServicios);
                    while($rowAsignacionServicios = mysqli_fetch_row($resAsignacionServicios))
                    {
                        /*
						Tipo de cobro
						1. CARGO FIJO
						2. TARIFA SIN MEDIDOR
						3. TARIFA CON MEDIDOR
						4. OTROS COBROS
						5. SUBSIDIOS
						6. EXONERACIONES
						*/

                        $sqlServicios = "SELECT nombre FROM srvservicios WHERE id = '$rowAsignacionServicios[0]' ";
                        $resServicios = mysqli_query($linkbd,$sqlServicios);
                        $rowServicios = mysqli_fetch_row($resServicios);

                        //Codigo y nombre de servicio
                        $_POST['codigoServicio'][] = $rowAsignacionServicios[0];
                        $_POST['nombreServicio'][] = $rowServicios[0];

                        $sqlDetallesFacturacion = "SELECT credito FROM srvdetalles_facturacion WHERE corte = '$rowCorte[0]' AND id_cliente = '$rowCorteDetalles[0]' AND numero_facturacion = '$rowCorteDetalles[1]' AND id_servicio = '$rowAsignacionServicios[0]' AND id_tipo_cobro = 1 ";
                        $resDetallesFacturacion = mysqli_query($linkbd,$sqlDetallesFacturacion);
                        $rowDetallesFacturacion = mysqli_fetch_row($resDetallesFacturacion);

                        //Cargo Fijo del servicio
                        $cargoFijo = $rowDetallesFacturacion[0]; 

                        $sqlDetallesFacturacion = "SELECT credito FROM srvdetalles_facturacion WHERE corte = '$rowCorte[0]' AND id_cliente = '$rowCorteDetalles[0]' AND numero_facturacion = '$rowCorteDetalles[1]' AND id_servicio = '$rowAsignacionServicios[0]' AND id_tipo_cobro = 3 ";
                        $resDetallesFacturacion = mysqli_query($linkbd,$sqlDetallesFacturacion);
                        $rowDetallesFacturacion = mysqli_fetch_row($resDetallesFacturacion);

                        //Valida si existe cobro por medio de medidor
                        if(isset($rowDetallesFacturacion[0]))
                        {
                            $cobroConMedidor = $rowDetallesFacturacion[0]; 
                        } 
                        else
                        {
                            $cobroConMedidor = 0;
                        }

                        $sqlDetallesFacturacion = "SELECT credito FROM srvdetalles_facturacion WHERE corte = '$rowCorte[0]' AND id_cliente = '$rowCorteDetalles[0]' AND numero_facturacion = '$rowCorteDetalles[1]' AND id_servicio = '$rowAsignacionServicios[0]' AND id_tipo_cobro = 2 ";
                        $resDetallesFacturacion = mysqli_query($linkbd,$sqlDetallesFacturacion);
                        $rowDetallesFacturacion = mysqli_fetch_row($resDetallesFacturacion);

                        //Valida si existe cobro sin medidor
                        if(isset($rowDetallesFacturacion[0]))
                        {
                            $cobroSinMedidor = $rowDetallesFacturacion[0]; 
                        } 
                        else
                        {
                            $cobroSinMedidor = 0;
                        }

                        $sqlDetallesFacturacion = "SELECT debito FROM srvdetalles_facturacion WHERE corte = '$rowCorte[0]' AND id_cliente = '$rowCorteDetalles[0]' AND numero_facturacion = '$rowCorteDetalles[1]' AND id_servicio = '$rowAsignacionServicios[0]' AND id_tipo_cobro = 5 ";
                        $resDetallesFacturacion = mysqli_query($linkbd,$sqlDetallesFacturacion);
                        $rowDetallesFacturacion = mysqli_fetch_row($resDetallesFacturacion);

                        //Valida si existe subsidios
                        if(isset($rowDetallesFacturacion[0]))
                        {
                            $subsidios = $rowDetallesFacturacion[0]; 
                        } 
                        else
                        {
                            $subsidios = 0;
                        }

                        $sqlDetallesFacturacion = "SELECT debito FROM srvdetalles_facturacion WHERE corte = '$rowCorte[0]' AND id_cliente = '$rowCorteDetalles[0]' AND numero_facturacion = '$rowCorteDetalles[1]' AND id_servicio = '$rowAsignacionServicios[0]' AND id_tipo_cobro = 6 ";
                        $resDetallesFacturacion = mysqli_query($linkbd,$sqlDetallesFacturacion);
                        $rowDetallesFacturacion = mysqli_fetch_row($resDetallesFacturacion);

                        //Valida si existe exoneraciones
                        if(isset($rowDetallesFacturacion[0]))
                        {
                            $exoneraciones = $rowDetallesFacturacion[0]; 
                        } 
                        else
                        {
                            $exoneraciones = 0;
                        }

                        $sqlDetallesFacturacion = "SELECT credito FROM srvdetalles_facturacion WHERE corte = '$rowCorte[0]' AND id_cliente = '$rowCorteDetalles[0]' AND numero_facturacion = '$rowCorteDetalles[1]' AND id_servicio = '$rowAsignacionServicios[0]' AND id_tipo_cobro = 7 ";
                        $resDetallesFacturacion = mysqli_query($linkbd,$sqlDetallesFacturacion);
                        $rowDetallesFacturacion = mysqli_fetch_row($resDetallesFacturacion);

                        //Valida si existe contribuciones
                        if(isset($rowDetallesFacturacion[0]))
                        {
                            $contribuciones = $rowDetallesFacturacion[0]; 
                        } 
                        else
                        {
                            $contribuciones = 0;
                        }

                        //Ver factura anterior y verifica si tiene saldo

                        $corteAnterior = $rowCorte[0] - 1;

                        $sqlConsultaFacturaAnterior = "SELECT estado_pago FROM srvcortes_detalle WHERE id_cliente = $rowCorteDetalles[0] AND id_corte = $corteAnterior";
                        $resConsultaFacturaAnterior = mysqli_query($linkbd,$sqlConsultaFacturaAnterior);
                        $rowConsultaFacturaAnterior = mysqli_fetch_row($resConsultaFacturaAnterior);

                        if($rowConsultaFacturaAnterior[0] == 'V')
                        {
                            $sql = "SELECT credito FROM srvdetalles_facturacion WHERE corte = $rowCorte[0] AND numero_facturacion = '$rowCorteDetalles[1]' AND id_servicio = $rowAsignacionServicios[0] AND id_tipo_cobro = 8 ";
                            $res = mysqli_query($linkbd,$sql);
                            $row = mysqli_fetch_row($res);

                            $saldo = $row[0];
                        }
                        else
                        {
                            $queryConsulta = "SELECT credito FROM srvdetalles_facturacion WHERE numero_facturacion = '$rowCorteDetalles[1]' AND id_tipo_cobro = '10'";
                            $res = mysqli_query($linkbd,$queryConsulta);
                            $row = mysqli_fetch_row($res);

                            if($row[0] != '')
                            {
                                $saldo = $row[0];
                            }
                            else
                            {
                                $saldo = 0;
                            }
                        }

                        $sql = "SELECT credito FROM srvdetalles_facturacion WHERE corte = $rowCorte[0] AND numero_facturacion = '$rowCorteDetalles[1]' AND id_servicio = $rowAsignacionServicios[0] AND id_tipo_cobro = 9";
                        $res = mysqli_query($linkbd,$sql);
                        $row = mysqli_fetch_row($res);

                        if(isset($row[0]))
                        {
                            $acuerdoPago = $row[0];
                        } 
                        else
                        {
                            $acuerdoPago = 0;
                        }

                        $sqlDetallesFacturacion = "SELECT SUM(credito), SUM(debito) FROM srvdetalles_facturacion WHERE corte = '$rowCorte[0]' AND id_cliente = '$rowCorteDetalles[0]' AND numero_facturacion = '$rowCorteDetalles[1]' AND id_servicio = '$rowAsignacionServicios[0]' AND tipo_movimiento = '101'";
                        $resDetallesFacturacion = mysqli_query($linkbd,$sqlDetallesFacturacion);
                        $rowDetallesFacturacion = mysqli_fetch_row($resDetallesFacturacion);

                        if(isset($rowDetallesFacturacion))
                        {
                            $total = $rowDetallesFacturacion[0] - $rowDetallesFacturacion[1];
                        } 
                        else
                        {
                            $total = 0;
                        }

                        $abonos = 0;

                        $sqlDetallesFacturacion = "SELECT debito, credito FROM srvdetalles_facturacion WHERE corte = '$rowCorte[0]' AND id_cliente = '$rowCorteDetalles[0]' AND numero_facturacion = '$rowCorteDetalles[1]' AND tipo_movimiento = '204' AND id_servicio = '$rowAsignacionServicios[0]' ";
                        $resDetallesFacturacion = mysqli_query($linkbd,$sqlDetallesFacturacion);
                        while($rowDetallesFacturacion = mysqli_fetch_row($resDetallesFacturacion))
                        {
                            $abonos = $abonos + $rowDetallesFacturacion[0] - $rowDetallesFacturacion[1];
                        }

                        $sqlDetallesFacturacion = "SELECT debito, credito FROM srvdetalles_facturacion WHERE corte = '$rowCorte[0]' AND id_cliente = '$rowCorteDetalles[0]' AND numero_facturacion = '$rowCorteDetalles[1]' AND tipo_movimiento = '202' AND id_servicio = '$rowAsignacionServicios[0]' ";
                        $resDetallesFacturacion = mysqli_query($linkbd,$sqlDetallesFacturacion);
                        while($rowDetallesFacturacion = mysqli_fetch_row($resDetallesFacturacion))
                        {
                            $abonos = $abonos + $rowDetallesFacturacion[0] - $rowDetallesFacturacion[1];
                        }

                        $sqlDetallesFacturacion = "SELECT debito, credito FROM srvdetalles_facturacion WHERE corte = '$rowCorte[0]' AND id_cliente = '$rowCorteDetalles[0]' AND numero_facturacion = '$rowCorteDetalles[1]' AND tipo_movimiento = '201' AND id_servicio = '$rowAsignacionServicios[0]' ";
                        $resDetallesFacturacion = mysqli_query($linkbd,$sqlDetallesFacturacion);
                        while($rowDetallesFacturacion = mysqli_fetch_row($resDetallesFacturacion))
                        {
                            $abonos = $abonos + $rowDetallesFacturacion[0] - $rowDetallesFacturacion[1];
                        }

                        $sqlDetallesFacturacion = "SELECT debito, credito FROM srvdetalles_facturacion WHERE corte = '$rowCorte[0]' AND id_cliente = '$rowCorteDetalles[0]' AND numero_facturacion = '$rowCorteDetalles[1]' AND tipo_movimiento = '401' AND id_servicio = '$rowAsignacionServicios[0]' ";
                        $resDetallesFacturacion = mysqli_query($linkbd,$sqlDetallesFacturacion);
                        while($rowDetallesFacturacion = mysqli_fetch_row($resDetallesFacturacion))
                        {
                            $abonos = $abonos + $rowDetallesFacturacion[0] - $rowDetallesFacturacion[1];
                        }

                        $subsidios - $exoneraciones;

                        $sqlInteresMora = "SELECT credito FROM srvdetalles_facturacion WHERE corte = '$rowCorte[0]' AND id_cliente = '$rowCorteDetalles[0]' AND numero_facturacion = '$rowCorteDetalles[1]' AND tipo_movimiento = '101' AND id_servicio = '$rowAsignacionServicios[0]' AND id_tipo_cobro = 11 ";
                        $resInteresMora = mysqli_query($linkbd,$sqlInteresMora);
                        $rowInteresMora = mysqli_fetch_row($resInteresMora);

                        if (isset($rowInteresMora[0])) {

                            $interesMora = $rowInteresMora[0];
                        }

                        $_POST['cargoFijo'][]      = $cargoFijo;
                        $_POST['conMedidor'][]     = $cobroConMedidor;
                        $_POST['sinMedidor'][]     = $cobroSinMedidor;
                        $_POST['interesMora'][]    = $interesMora;
                        $_POST['subsidios'][]      = $subsidios;
                        $_POST['exoneraciones'][]  = $exoneraciones;
                        $_POST['contribuciones'][] = $contribuciones;
                        $_POST['saldo'][]          = $saldo;
                        $_POST['acuerdo'][]        = $acuerdoPago;
                        $_POST['valorReal'][]      = $total;
                        $_POST['abonos'][]         = $abonos;


                        $_POST['totalCargoFijo']       = $_POST['totalCargoFijo'] + $cargoFijo;
                        $_POST['totalCobroConMedidor'] = $_POST['totalCobroConMedidor'] + $cobroConMedidor;
                        $_POST['totalCobroSinMedidor'] = $_POST['totalCobroSinMedidor'] + $cobroSinMedidor;
                        $_POST['totalInteresMora']     = $_POST['totalInteresMora'] + $interesMora;
                        $_POST['totalContribuciones']  = $_POST['totalContribuciones'] + $contribuciones;
                        $_POST['totalSubsidios']       = $_POST['totalSubsidios'] + $subsidios;
                        $_POST['totalExoneraciones']   = $_POST['totalExoneraciones'] + $exoneraciones;
                        $_POST['totalSaldo']           = $_POST['totalSaldo'] + $saldo;
                        $_POST['totalAcuerdo']         = $_POST['totalAcuerdo'] + $acuerdoPago;
                        $_POST['totalValorReal']       = $_POST['totalValorReal'] + $total;
                        $_POST['totalAbonos']          = $_POST['totalAbonos'] + $abonos;  
                        $_POST['valor_factura'] = $_POST['totalValorReal'] - $_POST['totalAbonos'];
                    }
                                        

                    //Llenado información General de Factura

                    $_POST['codigoFactura']  = $rowCorteDetalles[1];
                    $_POST['fechaImpresion'] = $rowCorte[4];
                    $_POST['codigoCorte']    = $rowCorte[0];
                    $_POST['estado']         = $estado;
                    $_POST['codigoUsuario']  = $rowCliente[1];
                    $_POST['fechaInicial']   = $rowCorte[1];
                    $_POST['fechaFinal']     = $rowCorte[2];


                    //Llenado informacion General Usuario
                    $_POST['documentoUsuario'] = $rowTercero[0];
                    $_POST['nombre'] = $nombreCompleto;
                    $_POST['codigoCatastral'] = $rowCliente[2];
                    $_POST['barrio'] = $rowBarrio[0];
                    $_POST['usoDeSuelo'] = $rowUsoDeSuelo[0];
                    $_POST['estrato'] = $rowEstrato[0];
                    $_POST['direccion'] = $rowDireccion[0];
				}
			?>
            <div>
                <table class="inicio grande">
                    <tr>
                        <td class="titulos" colspan="9">.: Informaci&oacute;n General de Factura</td>

                        <td class="cerrar" style="width:7%" onClick="location.href='serv-principal.php'">Cerrar</td>
                    </tr>
                    
                    <tr>
                        <td class="tamano01" style="width:3.5cm;">C&oacute;digo de Factura</td>

                        <td>
                            <input type="text" name="codigoFactura" id="codigoFactura" style="text-align:center;height:30px;" value="<?php echo @ $_POST['codigoFactura'];?>"  readonly/>
                        </td>

                        <td class="tamano01" style="width:3.5cm;">Fecha de Impresi&oacute;n</td>

                        <td>
                            <input type="date" name="fechaImpresion" style="text-align:center;height:30px;" value="<?php echo @$_POST['fechaImpresion']; ?>" readonly/>
                        </td>

                        <td class="tamano01" style="text-align:center;width:3.5cm;">N&deg; Corte</td>

                        <td>
                            <input type="text" name="codigoCorte" id="codigoCorte" style="text-align:center;height:30px;" value="<?php echo @ $_POST['codigoCorte'];?>"  readonly/>
                        </td>

                        <td class="tamano01" style="text-align:center;width:3.5cm;">Estado</td>

                        <td>
                            <input type="text" name="estado" id="estado" style="<?php echo $color ?>" value="<?php echo @ $_POST['estado'];?>"  readonly/>
                        </td>
                    </tr>

                    <tr>
                        <td class="tamano01" style="width:3.5cm;">C&oacute;digo Usuario</td>

                        <td>
                            <input type="text" name="codigoUsuario" style="text-align:center;height:30px;" value="<?php echo @ $_POST['codigoUsuario'];?>" readonly/>
                        </td>

                        <td class="tamano01" style="width:3.5cm;">Fecha Inicial de Corte</td>

                        <td>
                            <input type="date" name="fechaInicial" style="text-align:center;height:30px;" value="<?php echo @$_POST['fechaInicial']; ?>" readonly/> 
                        </td>

                        <td class="tamano01" style="width:3.5cm;">Fecha Final de Corte</td>

                        <td>
                            <input type="date" name="fechaFinal" style="text-align:center;width:91%;height:30px;" value="<?php echo @$_POST['fechaFinal']; ?>" readonly/> 
                        </td>
                    </tr>
                </table>
            </div>
            
            <div>
                <table class="inicio ancho">
                    <tr>
                        <td class="titulos" colspan="6">.: Informaci&oacute;n General Usuario</td>
                    </tr>
                    
                    <tr>
                        <td class="tamano01" style="width:3.5cm;">Documento Usuario</td>

                        <td style="width:13%;">
                            <input type="text" name="documentoUsuario" style="text-align:center;width:93%;height:30px;" value="<?php echo @ $_POST['documentoUsuario'];?>"  readonly/>
                        </td>

                        <td class="tamano01" style="text-align:center;width:3.5cm;">Nombre</td>

                        <td colspan="3">
                            <input type="text" name="nombre" style="width:90%;height:30px;" value="<?php echo @ $_POST['nombre'];?>" readonly/>
                        </td>
                    </tr>

                    <tr>
                        <td class="tamano01">C&oacute;odigo Catastral</td>

                        <td>
                            <input type="text" name="codigoCatastral" style="text-align:center;height:30px;" value="<?php echo @ $_POST['codigoCatastral'];?>" readonly/>
                        </td>

                        <td class="tamano01" style="text-align:center;width:3.5cm;">Uso</td>

                        <td style="width:11.5%;">
                            <input type="text" name="usoDeSuelo" style="text-align:center;width:90%;height:30px;" value="<?php echo @ $_POST['usoDeSuelo'];?>" readonly/>
                        </td>

                        <td class="tamano01" style="text-align:center;width:3.5cm;">Sitio de Residencia:</td>

                        <td >
                            <input type="text" name="barrio" style="text-align:center;width:98%;height:30px;" value="<?php echo @ $_POST['barrio'];?>" readonly/>
                        </td>
                    </tr>
                    
                    <tr>
                        <td class="tamano01" style="width: 3cm;">Valor a Pagar</td>

                        <td>
                            <input type="text" name="valor_factura" id="valor_factura" value="<?php echo @$_POST['valor_factura'] ?>" style="text-align: center;" readonly>
                        </td>

                        <td class="tamano01" style="text-align:center;width:3.5cm;">Estrato</td>

                        <td style="width:13%;">
                            <input type="text" name="estrato" style="text-align:center;width:93%;height:30px;" value="<?php echo @ $_POST['estrato'];?>"  readonly/>
                        </td>

                        <td class="tamano01" style="text-align:center;width:3.5cm;">Direcci&oacute;n</td>

                        <td colspan="">
                            <input type="text" name="direccion" style="width:98%;height:30px;" value="<?php echo @ $_POST['direccion'];?>" readonly/>
                        </td>
                    </tr>

                    <tr>
                        <?php
                        if($_POST['estado'] == 'Factura Reversada')
                        {
                            $sql = "SELECT motivo,fecha FROM srvreversion_factura WHERE factura_origen = '$_POST[codigoFactura]'";
                            $res = mysqli_query($linkbd,$sql);
                            $row = mysqli_fetch_row($res);
                            $_POST['motivo'] = $row[0];
                            $_POST['fecha_reversion'] = $row[1];
                        ?>
                            <td class="tamano01">Fecha Reversi&oacute;n</td>

                            <td>
                                <input type="text" name="fecha_reversion" id="fecha_reversion" value="<?php echo $_POST['fecha_reversion'] ?>" style="width: 98%; height: 30px; text-align:center;" readonly/>
                            </td>

                            <td class="tamano01" style="width: 3cm; text-align:center;">Motivo:</td>

                            <td colspan="3">
                                <input type="text" id="motivo" name="motivo" value="<?php echo $_POST['motivo'] ?>" style="width: 98%;" readonly>
                            </td>


                        <?php
                        }
                        ?>    
                    </tr>
                </table>
            </div>

            <div class="subpantalla" style="height: 40%;">
                <table class="inicio">
                    <tr><td class="titulos" colspan="13">.: Informaci&oacute;n Servicios</td></tr>

                    <tr class="titulos2">
                        <td style="text-align:center;">C&oacute;digo</td>
                        <td style="text-align:center;">Servicio</td>
                        <td style="text-align:center;">Cargo Fijo</td>
                        <td style="text-align:center;">Consumo Basico</td>
                        <td style="text-align:center;">Consumo con Lectura</td>
                        <td style="text-align:center;">Contribuciones</td>
                        <td style="text-align:center;">Subsidios</td>
                        <td style="text-align:center;">Exoneraciones</td>
                        <td style="text-align:center;">Saldos</td>
                        <td style="text-align:center;">Interes Mora</td>
                        <td style="text-align:center;">Acuerdos</td>
                        <td style="text-align:center;">Valor real</td>
                        <td style="text-align:center;">Pago Realizado</td>
                    </tr>

                    <?php
                    for ($x=0;$x< count($_POST['codigoServicio']);$x++)
                    {
                        echo "
                            <tr class='saludo1a'>
                                <td class='' style='text-align:center;width:3%;'>".$_POST['codigoServicio'][$x]."</td>

                                <td class='' style='text-align:center;width:10%;'>".$_POST['nombreServicio'][$x]."</td>

                                <td class='' style='text-align:center;width:8%;'>".number_format($_POST['cargoFijo'][$x],2,',','.')."</td>
                            
                                <td class='' style='text-align:center;width:8%;'>".number_format($_POST['sinMedidor'][$x],2,',','.')."</td>

                                <td class='' style='text-align:center;width:8%;'>".number_format($_POST['conMedidor'][$x],2,',','.')."</td>

                                <td class='' style='text-align:center;width:8%;'>".number_format($_POST['contribuciones'][$x],2,',','.')."</td>
                                    
                                <td class='' style='text-align:center;width:8%;'>".number_format($_POST['subsidios'][$x],2,',','.')."</td>
                    
                                <td class='' style='text-align:center;width:8%;'>".number_format($_POST['exoneraciones'][$x],2,',','.')."</td>
                            
                                <td class='' style='text-align:center;width:8%;'>".number_format($_POST['saldo'][$x],2,',','.')."</td>

                                <td class='' style='text-align:center;width:8%;'>".number_format($_POST['interesMora'][$x],2,',','.')."</td>

                                <td class='' style='text-align:center;width:8%;'>".number_format($_POST['acuerdo'][$x],2,',','.')."</td>
                            
                                <td class='' style='text-align:center;width:8%;'>".number_format($_POST['valorReal'][$x],2,',','.')."</td>
                        
                                <td class='' style='text-align:center;width:8%;'>".number_format($_POST['abonos'][$x],2,',','.')."</td>	
                            </tr>";
                    }
                    
                    echo "
                    <tr class='titulos2'>
                        <td style='text-align:center;' colspan='2'>Total:</td>

                        <td style='text-align:center;'>".number_format($_POST['totalCargoFijo'],2,',','.')."</td>
                        
                        <td style='text-align:center;'>".number_format($_POST['totalCobroSinMedidor'],2,',','.')."</td>

                        <td style='text-align:center;'>".number_format($_POST['totalCobroConMedidor'],2,',','.')."</td>

                        <td style='text-align:center;'>".number_format($_POST['totalContribuciones'],2,',','.')."</td>
                            
                        <td style='text-align:center;'>".number_format($_POST['totalSubsidios'],2,',','.')."</td>
                            
                        <td style='text-align:center;'>".number_format($_POST['totalExoneraciones'],2,',','.')."</td>

                        <td style='text-align:center;'>".number_format($_POST['totalSaldo'],2,',','.')."</td>

                        <td style='text-align:center;'>".number_format($_POST['totalInteresMora'],2,',','.')."</td>

                        <td style='text-align:center;'>".number_format($_POST['totalAcuerdo'],2,',','.')."</td>

                        <td style='text-align:center;'>".number_format($_POST['totalValorReal'],2,',','.')."</td>

                        <td style='text-align:center;'>".number_format($_POST['totalAbonos'],2,',','.')."</td>
                    </tr>";    
                ?>
                </table>
            </div>

			<input type="hidden" name="maximo" id="maximo" value="<?php echo @ $_POST['maximo']?>"/>
			<input type="hidden" name="minimo" id="minimo" value="<?php echo @ $_POST['minimo']?>"/>
			<input type="hidden" name="bc" id="bc" value=""/>
			<input type="hidden" name="oculto" id="oculto" value="1"/>

			<?php
				if(@ $_POST['oculto']=="2")
				{
					
				}
			?>
		</form>
		<div id="bgventanamodal2">
			<div id="ventanamodal2">
				<IFRAME  src="" name="buster" marginWidth=0 marginHeight=0 frameBorder=0 id="ventana2" frameSpacing=0 style="left:500px; width:900px; height:500px; top:200;"></IFRAME>
			</div>
		</div>
	</body>
</html>