<?php
	require"comun.inc";
	require"funciones.inc";
	session_start();
	$linkbd=conectar_bd();	
	cargarcodigopag($_GET[codpag],$_SESSION["nivel"]);
	header("Cache-control: private"); // Arregla IE 6
	header("Content-Type: text/html;charset=iso-8859-1");
	date_default_timezone_set("America/Bogota");
?>
<!DOCTYPE >
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="es">
	<head>
	 	<meta http-equiv="Content-type" content="text/html; charset=iso-8859-1"/>
        <meta http-equiv="X-UA-Compatible" content="IE=9"/>
		<title>:: Spid - Servicios P&uacute;blicos</title>
        <link href="css/css2.css" rel="stylesheet" type="text/css" />
        <link href="css/css3.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="css/programas.js"></script>
		<?php titlepag();?>
    </head>
    <body>
        <IFRAME src="alertas.php" name="alertas" id="alertas" style="display:none"></IFRAME>
        <span id="todastablas2"></span>
        <table>
            <tr><script>barra_imagenes("serv");</script><?php cuadro_titulos();?></tr>	 
            <tr><?php menu_desplegable("serv");?></tr>
        	<tr>
          		<td colspan="3" class="cinta"><img src="imagenes/add2.png" class="mgbt1"/><img src="imagenes/guardad.png" style="width:24px;" class="mgbt1"/><img src="imagenes/buscad.png" class="mgbt1"/><img src="imagenes/nv.png" title="Nueva Ventana" class="mgbt" onClick="mypop=window.open('serv-principal.php','','');mypop.focus();"/></td>
        	</tr>
        </table>
 		<form name="form2" method="post" action="">
    		<table class="inicio">
      			<tr>
        			<td class="titulos" colspan="2">.: Men&uacute; Reportes Generales</td>
                    <td class="cerrar" style="width:7%"><a onClick="location.href='serv-principal.php'">Cerrar</a></td>
      			</tr>
                <tr>
                	<td class="titulos2">&nbsp;Reportes Mes a Mes</td>
                </tr>
				<tr>
                    <td class='saludo1' >
             			<ol id="lista2">
  							<li class='icoom' onClick="location.href='serv-reportegeneralfacturacion.php'">Reporte General Facturaci&oacute;n</li>
                            <li class='icoom' onClick="location.href='serv-reportegeneralrecaudo.php'">Reporte General Recaudos Mes</li>
							<li class='icoom' onClick="location.href='serv-reportegeneralrecaudociclo.php'">Reporte General Recaudo Ciclo</li>
							<li class='icoom' onClick="location.href='serv-reporteindirecaudociclo.php'">Reporte Individual Recaudo Ciclo</li>
                 			<li class='icoom' onClick="location.href='serv-reportegeneralsubsidios.php'">Reporte General Subsidios</li>
                            <li class='icoom' onClick="location.href='serv-reportetercerosubs.php'">Reporte Terceros Subsidios</li>
                            <li class='icoom' onClick="location.href='serv-reportegeneralcontribuciones.php'">Reporte General Contribuciones</li>
                            <li class='icoom' onClick="location.href='serv-reporteterceroscontr.php'">Reporte Terceros Contribuciones</li>
                            <li class='icoom' onClick="location.href='serv-reportegeneralingresos.php'">Reporte General Ingresos</li>
                            <li class='icoom' onClick="location.href='serv-reportegeneralusuarios.php'">Reporte General de Usuarios</li>
                            <li class='icoom' onClick="location.href='serv-reporteusuariosmora.php'">Reporte Usuarios Morosos</li>
                        </ol>
                    </td>
				</tr>							
    		</table>
		</form>
	</body>
</html>