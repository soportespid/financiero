<?php
	header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=utf8");
	require 'comun.inc';
	require 'funciones.inc';
	require 'funcionesSP.inc.php';
	session_start();
	if(empty($_SESSION)){
		header("location: index.php");
	}
	$linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");
	cargarcodigopag($_GET['codpag'],$_SESSION["nivel"]);
	date_default_timezone_set("America/Bogota");
?>
<!DOCTYPE >
<html lang=es>
	<head>
		<meta charset="utf-8"/>
		<meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1"/>
		<meta name="viewport" content="user-scalable=no">
		<title>:: IDEAL 10 - Herramientas MIPG</title>
		<link href="favicon.ico" rel="shortcut icon"/>
		<link href="css/css2.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
		<link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
		<link href="css/css4.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
		<link href="css/style.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css"/>
		<script type="text/javascript" src="css/programas.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
	</head>
	<body>
		<section id="myapp" v-cloak >
			<div class="main-container">
				<header>
					<table>
						<tr><script>barra_imagenes("meci");</script><?php cuadro_titulos();?></tr>
					</table>
				</header>
				<nav>
					<?php menu_desplegable("meci");?>
				</nav>
				<div>
					<h2 class="titulos m-0">.: Buscar Publicación</h2>
					<div class="form-control w-50">
					<div class="d-flex">
						<input type="search" placeholder="buscar" v-model="txtSearch" @keyup="search">
					</div>
					</div>
					<h2 class="titulos m-0">.: Resultados: {{ txtResults }} </h2>
				</div>
				<div class="overflow-auto max-vh-50 overflow-x-hidden p-2" ref="tableContainer">
					<table class="table table-hover fw-normal">
						<thead>
							<tr>
								<th class="text-center">Código</th>
								<th class="text-center">Fecha Inicial</th>
								<th>Nombre</th>
								<th>Descripción</th>
							</tr>
						</thead>
						<tbody>
							<tr v-for="(data, index) in arrDataCopy" :key="index" ref="highlightedRow" @dblclick="loadContentIntoModal( data.indices)">
								<td class="text-center">{{ data.indices }}</td>
								<td class="text-center">{{ data.fecha_inicio }}</td>
								<td>{{ data.titulos }}</td>
								<td>{{ data.descripcion }}</td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
			<div v-show="showModal_programaticoProyectos">
				<transition name="modal">
					<div class="modal-mask">
						<div class="modal-wrapper">
							<div class="modal-dialog modal-xl" style="max-width: 1200px !important;" role="document">
								<div class="modal-content" style="width: 100% !important;" scrollable>
									<div class="modal-header">
									<h5 class="modal-title">Publicaciones Ideal 10</h5>
									<button type="button" @click="showModal_programaticoProyectos=false;" class="btn btn-close">
										<div></div>
										<div></div>
									</button>
									</div>
									<div class="modal-body overflow-hidden"></div>
								</div>
							</div>
						</div>
					</div>
				</transition>
			</div>
		</section>
		<script src="Librerias/vue/vue.min.js"></script>
		<script src="Librerias/vue/axios.min.js"></script>
		<script src="vue/herramientas_mipg/publicaciones/mipg-historial_publicaciones.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
	</body>
</html>