<!--V 1.0 24/02/2015-->
<?php
	require "comun.inc";
	require "funciones.inc";
	session_start();
	$linkbd=conectar_v7();	
	cargarcodigopag($_GET['codpag'],$_SESSION["nivel"]);
	header("Cache-control: private"); // Arregla IE 6
	date_default_timezone_set("America/Bogota");
?>
<!DOCTYPE >
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="es">
	<head>
	 	<meta http-equiv="Content-Type" content="text/html" charset="iso-8859-1"/>
        <meta http-equiv="X-UA-Compatible" content="IE=9"/>
		<title>:: IDEAL 10 - Parametrización</title>
        <script src="css/calendario.js"></script>
        <link href="css/css2.css" rel="stylesheet" type="text/css" />
        <link href="css/css3.css" rel="stylesheet" type="text/css" />
		<link href="css/style.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css"/>
		<script type="text/javascript" src="css/programas.js"></script>
<script>
function guardar()
{
	if (document.form2.numero.value!='' && document.form2.nombre.value!='')
  		{if (confirm("Esta Seguro de Guardar")){document.form2.oculto.value=2;document.form2.submit();}}
 	else{alert('Faltan datos para completar el registro');document.form2.numero.focus();document.form2.numero.select();}
}

function clasifica(formulario)
{
	//document.form2.action="presu-recursos.php";
	document.form2.submit();
}

function agregardetalle()
{
	if(document.form2.cuenta.value!=""  && document.form2.cc.value!="" ){document.form2.agregadet.value=1;document.form2.submit();}
	else {alert("Falta informacion para poder Agregar");}
}

function eliminar(variable)
{
	if (confirm("Esta Seguro de Eliminar"))
  	{
		document.form2.elimina.value=variable;
		//eli=document.getElementById(elimina);
		vvend=document.getElementById('elimina');
		//eli.value=elimina;
		vvend.value=variable;
		document.form2.submit();
	}
}

function buscacta(e){if (document.form2.cuenta.value!=""){document.form2.bc.value='1';document.form2.submit();}}

function buscacc(e){if (document.form2.cc.value!=""){document.form2.bcc.value='1';document.form2.submit();}}

function validar2()
{
	//alert("Balance Descuadrado");
	document.form2.oculto.value=2;
	document.form2.action="acti-origen.php";
	document.form2.submit();
}

function validar(){document.form2.submit();}
</script>
<?php titlepag();?>
</head>
<body>
<IFRAME src="alertas.php" name="alertas" id="alertas" style="display:none"></IFRAME>
<span id="todastablas2"></span>
<table>
   	<tr><script>barra_imagenes("para");</script><?php cuadro_titulos();?></tr>
    <tr><?php menu_desplegable("para");?></tr>
	<tr>
		<td colspan="3" class="cinta">
			<a href="inve-origen.php" class="mgbt"><img src="imagenes/add.png"  title="Nuevo"/></a>
			<a onClick="guardar()" class="mgbt"><img src="imagenes/guarda.png" title="Guardar"/></a>
			<a href="acti-buscaorigen.php" class="mgbt"><img src="imagenes/busca.png"  title="Buscar"/></a>
			<a onClick="mypop=window.open('plan-agenda.php','','');mypop.focus()" class="mgbt"><img src="imagenes/agenda1.png" title="Agenda" /></a>
			<a onClick="mypop=window.open('para-principal.php','','');mypop.focus();" class="mgbt"><img src="imagenes/nv.png" title="Nueva ventana"></a>
            <a onClick="mypop=window.open('<?php echo $url2; ?>','','');mypop.focus();" class="tooltip bottom mgbt"><img src="imagenes/duplicar_pantalla.png" title="Duplicar pesta&ntilde;a"></a>
            <a href="acti-buscaorigen.php" class="mgbt"><img src="imagenes/iratras.png" title="Atr&aacute;s"></a>
		</td>
	</tr>
</table>
<?php $vigencia=date('Y'); ?>	
<?php
if(!$_POST['oculto'])
{
 		 $fec=date("d/m/Y");
		 $_POST['fecha']=$fec; 	
 	 	 $_POST['vigencia']=$vigencia;
		 $_POST['valoradicion']=0;
		 $_POST['valorreduccion']=0;
		 $_POST['valortraslados']=0;		 		  			 
		 $_POST['valor']=0;		 
		  $sqlr="select MAX(RIGHT(codigo,2)) from actiorigenes order by codigo Desc";
		 //echo $sqlr;
		  $res=mysqli_query($linkbd,$sqlr);
		  $row=mysqli_fetch_row($res);
		  $_POST['numero']=$row[0]+1;
		  if(strlen($_POST['numero'])==1)
		   {
			   $_POST['numero']='0'.$_POST['numero'];
			}
}
?>
 <form name="form2" method="post" action=""> 
<?php //**** busca cuenta
  			if($_POST['bc']=='1')
			 {
			  $nresul=buscacuenta($_POST['cuenta']);
			  if($nresul!='')
			   {
			  $_POST['ncuenta']=$nresul;
  	
			  }
			 else
			 {
			  $_POST['ncuenta']="";
			  }
			 }
			  //**** busca centro costo
  			if($_POST['bcc']=='1')
			 {
			  $nresul=buscacentro($_POST['cc']);
			  if($nresul!='')
			   {
			  $_POST['ncc']=$nresul;
  	
			  }
			 else
			 {
			  $_POST['ncc']="";
			  }
			 }
			 ?>
    <table class="inicio" align="center"  >
      <tr >
        <td class="titulos" colspan="8">.: Origen </td>
        <td width="61" class="cerrar" ><a href="para-principal.php">Cerrar</a></td>
      </tr>
      <tr  >
		<td width="119" class="saludo1">Codigo:</td>
          <td width="197" valign="middle" ><input type="text" id="numero" name="numero" size="10" onKeyPress="javascript:return solonumeros(event)" 
		  onKeyUp="return tabular(event,this)" value="<?php echo $_POST['numero']?>" onClick="document.getElementById('numero').focus();document.getElementById('numero').select();"></td>
		 <td width="119" class="saludo1">Nombre:</td>
          <td width="197" valign="middle" ><input type="text" id="nombre" name="nombre" size="80" 
		  onKeyUp="return tabular(event,this)" value="<?php echo $_POST['nombre']?>" onClick="document.getElementById('acuerdo').focus();document.getElementById('acuerdo').select();"></td>
	    </tr>
    </table>	
	<table class="inicio">
	<tr><td colspan="4" class="titulos2">Destino Compra</td></tr>
	<tr><td class="saludo1">CC:</td><td colspan="2">
	<select name="cc"  onChange="validar()" onKeyUp="return tabular(event,this)">
	<?php
	$sqlr="select *from centrocosto where estado='S'";
	$res=mysqli_query($linkbd,$sqlr);
	while ($row =mysqli_fetch_row($res)) 
				    {
					echo "<option value=$row[0] ";
					$i=$row[0];
		
					 if($i==$_POST['cc'])
			 			{
						 echo "SELECTED";
						 }
					  echo ">".$row[0]." - ".$row[1]."</option>";	 	 
					}	 	
	?>
   </select>
	 </td>
        </tr>
	<tr>
	<td class="saludo1">Cuenta: </td>
          <td  valign="middle" ><input type="text" id="cuenta" name="cuenta" size="8" onKeyPress="javascript:return solonumeros(event)" 
		  onKeyUp="return tabular(event,this)" onBlur="buscacta(event)" value="<?php echo $_POST['cuenta']?>" onClick="document.getElementById('cuenta').focus();document.getElementById('cuenta').select();"><input type="hidden" value="0" name="bc"><a href="#" onClick="mypop=window.open('cuentas-ventana.php','','menubar=0,scrollbars=yes, toolbar=no, location=no, width=700px,height=500px');mypop.focus();"><img src="imagenes/buscarep.png" align="absmiddle" border="0"></a>  </td><td ><input name="ncuenta" type="text" value="<?php echo $_POST['ncuenta']?>" size="70" readonly></td>
	</tr>
	<tr>
	<td class="saludo1">Tipo:</td><td><select name="debcred">
		   <option value="2" <?php if($_POST['debcred']=='2') echo "SELECTED"; ?>>Credito</option>
   		  </select></td><td><input type="button" name="agrega" value="  Agregar  " onClick="agregardetalle()" ><input type="hidden" value="0" name="agregadet">
		  <?php
		if (!$_POST['oculto'])
		 {
		 ?>
		<script>
    	//document.form2.cc.focus();
		</script>	
		<?php
		}
		if($_POST['bc']=='1')
			 {
			  $nresul=buscacuenta($_POST['cuenta']);
			  if($nresul!='')
			   {
			  $_POST['ncuenta']=$nresul;
  			  ?>
			  <script>
			  document.getElementById('debcred').focus();</script>
			  <?php
			  }
			 else
			 {
			  $_POST['ncuenta']="";
			  ?>
			  <script>alert("Cuenta Incorrecta");document.form2.cuenta.focus();</script>
			  <?php
			  }
			 }
	 //*** centro  costo
			 if($_POST['bcc']=='1')
			 {
			  $nresul=buscacentro($_POST['cc']);
			  if($nresul!='')
			   {
			  $_POST['ncc']=$nresul;
  			  ?>
			  <script>
			  document.getElementById('cuenta').focus();document.getElementById('cuenta').select();</script>
			  <?php
			  }
			 else
			 {
			  $_POST['ncc']="";
			  ?>
			  <script>alert("Centro de Costos Incorrecto");document.form2.cc.focus();</script>
			  <?php
			  }
			 }
		
		?>
		  <input type="hidden" value="0" name="oculto">	
		  </td>
	</tr>
	</table><div class="subpantallac" style="height:51.5%; width:99.6%; overflow-x:hidden;">
	<table class="inicio">
	<tr><td class="titulos" colspan="6">Detalle Destino Compra</td></tr>
	<tr><td class="titulos2">CC</td><td class="titulos2">Cuenta</td><td class="titulos2">Nombre Cuenta</td><td class="titulos2">Debito</td><td class="titulos2">Credito</td><td class="titulos2"  style='text-align:center;'><img src="imagenes/del.png" ><input type='hidden' name='elimina' id='elimina'></td></tr>
	<?php			 
	if ($_POST['elimina']!='')
		 { 
		 $posi=$_POST['elimina'];
		 unset($_POST['dcuentas'][$posi]);
 		 unset($_POST['dncuentas'][$posi]);
		 unset($_POST['dccs'][$posi]);
		 unset($_POST['dcreditos'][$posi]);		 		 		 		 		 
		 unset($_POST['ddebitos'][$posi]);		 
		 $_POST['dcuentas']= array_values($_POST['dcuentas']); 
		 $_POST['dncuentas']= array_values($_POST['dncuentas']); 		 		 
		 $_POST['dccs']= array_values($_POST['dccs']); 
		 $_POST['dcreditos']= array_values($_POST['dcreditos']); 
		 $_POST['ddebitos']= array_values($_POST['ddebitos']); 		 		 		 		 
		 }
	
	if ($_POST['agregadet']=='1')
		 {
		  $cuentacred=0;
		  $cuentadeb=0;
		  $diferencia=0;
		 $_POST['dcuentas'][]=$_POST['cuenta'];
		 $_POST['dncuentas'][]=$_POST['ncuenta'];
		 $_POST['dccs'][]=$_POST['cc'];		 
		 if ($_POST['debcred']==1)
		  {
		  $_POST['dcreditos'][]='N';
		  $_POST['ddebitos'][]="S";
	 	  }
		 else
		  {
		  $_POST['dcreditos'][]='S';
		  $_POST['ddebitos'][]="N";
		  }
		 $_POST['agregadet']=0;
		  ?><script>
		  		//document.form2.cuenta.focus();	
				document.form2.cc.select();
				document.form2.cc.value="";
		 </script><?php
		 }
		 $iter='saludo1a';
		 $iter2='saludo2';
		 for ($x=0;$x< count($_POST['dcuentas']);$x++)
		 {echo "<tr class='$iter' onMouseOver=\"anterior=this.style.backgroundColor;this.style.backgroundColor='#40b3ff';\" onMouseOut=\"this.style.backgroundColor=anterior\">
		 <td><input name='dccs[]' value='".$_POST['dccs'][$x]."' type='text' size='2' class='inpnovisibles' readonly></td>
		 <td><input name='dcuentas[]' value='".$_POST['dcuentas'][$x]."' type='text' size='8' class='inpnovisibles' readonly></td>
		 <td><input name='dncuentas[]' value='".$_POST['dncuentas'][$x]."' type='text' size='70' class='inpnovisibles' readonly></td>
		 <td><input name='ddebitos[]' value='".$_POST['ddebitos'][$x]."' type='text' size='3' onDblClick='llamarventanadeb(this,$x)' class='inpnovisibles' readonly></td>
		 <td><input name='dcreditos[]' value='".$_POST['dcreditos'][$x]."' type='text' size='3' onDblClick='llamarventanacred(this,$x)' class='inpnovisibles' readonly></td>
		 <td style='text-align:center;'><a href='#' onclick='eliminar($x)'><img src='imagenes/del.png'></a></td></tr>";
		 $aux=$iter;
		 $iter=$iter2;
		 $iter2=$aux;
		 }	 
		 ?>
	</table>
	</div>
</form>
	<?php 
	//********** GUARDAR EL COMPROBANTE ***********
	if($_POST['oculto']=='2')	
		{
		//rutina de guardado cabecera
		$sqlr="insert into actiorigenes (codigo,nombre,estado) values ('$_POST[numero]','$_POST[nombre]','S')";
		if(!mysqli_query($linkbd,$sqlr))
		 {
		   echo "<table class='inicio'><tr><td class='saludo1'><center>No Se ha Almacenado con Exito El Origen, Error: </center></td></tr></table>";
		 }
		 else
		 {
		 echo "<table class='inicio'><tr><td class='saludo1'><center>Se ha Almacenado con Exito El Origen</center></td></tr></table>";
		  //**** crear el detalle del concepto
		for($x=0;$x<count($_POST['dcuentas']);$x++) 
		 {
		  $sqlr="insert into actiorigenes_det (codigo,tipocuenta,cuenta,cc,debito,credito,estado) values ('$_POST[numero]','N','".$_POST['dcuentas'][$x]."','".$_POST['dccs'][$x]."','".$_POST['ddebitos'][$x]."','".$_POST['dcreditos'][$x]."','S')";
		  $res=mysqli_query($linkbd,$sqlr);
		 }
		 }	
	   }
	?>	

</body>
</html>
