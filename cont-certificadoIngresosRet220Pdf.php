<?php
    require_once("tcpdf/tcpdf_include.php");
	require('comun.inc');
	require "funciones.inc";
    require 'funcionesSP.inc.php';
	date_default_timezone_set("America/Bogota");
	session_start();
	class MYPDF extends TCPDF
	{
		public function Header()
		{
			$linkbd = conectar_v7();
			$linkbd -> set_charset("utf8");
			$sqlr="select *from configbasica where estado='S' ";
			//echo $sqlr;
			$res=mysqli_query($linkbd, $sqlr);
			while($row=mysqli_fetch_row($res))
			{
				$nit=$row[0];
				$rs=$row[1];

			}

            $this->SetLineStyle(array('color' => array(101,125,150)));
			$this->Image('contabilidad_vue/certificado_220/logodian.png',  14, 11, 30, 8, 'PNG', '', 'C', true, 300, '', false, false, 0, false, false, false);// Logo
			$this->SetFont('helvetica','B',9);
			$this->SetY(10);
			$this->RoundedRect(10, 10, 190, 10, 1,'');
            $this->setX(45);
            $this->Cell(2,10,'','R',0,'C');
            $this->setX(87);
            $this->Cell(40,5,'Certificado de Ingresos y Retenciones por Rentas de Trabajo y de Pensiones','',0,'C');
            $this->ln();
            $this->setX(87);
            $this->Cell(40,5,"Año gravable ".VIGENCIA,'',0,'C');
            $this->setY(10);
            $this->setX(170);
            $this->SetFillColor(56,100,146);
            $this->SetTextColor(255,255,255);
            $this->SetFont('helvetica','B',20);
			$this->Cell(30,10,"220","L",0,'C',1);
			$this->SetFont('times','B',10);
			$this->ln();
		}
		public function Footer()
		{

		}
	}

	if($_POST['data'] ){
        $alturaFilas = 3.5;
        $arrData = json_decode($_POST['data'],true);
        $arrRetenedor = $arrData['retenedor'];
        $arrEmpleado = $arrData['empleado'];
        $arrTercero = $arrData['tercero'];
        define("VIGENCIA",$arrData['vigencia']);
        $pdf = new MYPDF('P','mm','Letter', true, 'iso-8859-1', false);
        $pdf->SetDocInfoUnicode (true);
        // set document information
        $pdf->SetCreator(PDF_CREATOR);
        $pdf->SetAuthor('IDEALSAS');
        $pdf->SetTitle('Formulario_220_'.VIGENCIA);

        $pdf->SetSubject('Formulario_220_'.VIGENCIA);
        $pdf->SetKeywords('TCPDF, PDF, example, test, guide');
        $pdf->SetMargins(10, 38, 10);// set margins
        $pdf->SetHeaderMargin(38);// set margins
        $pdf->SetFooterMargin(17);// set margins
        $pdf->SetAutoPageBreak(TRUE, 20);// set auto page breaks
        $pdf->SetLineStyle(array('color' => array(101,125,150)));
        $pdf->SetFont('helvetica','B',8);
        // set some language-dependent strings (optional)
        if (@file_exists(dirname(__FILE__).'/lang/spa.php'))
        {
            require_once(dirname(__FILE__).'/lang/spa.php');
            $pdf->setLanguageArray($l);
        }
        $pdf->AddPage();
        $pdf->SetFillColor(255,255,255);
        $pdf->SetTextColor(0,0,0);
        $pdf->StartTransform();
        $pdf->Rotate(90);
        $pdf->setX(8);
        $pdf->SetFont('helvetica','',7);
        $pdf->MultiCell(20, 4, "Retenedor", 1, 'C', 1, 0);
        $pdf->StopTransform();
        $pdf->setY(20);
        $pdf->setX(14);
        $pdf->SetFont('helvetica','',7);
        $pdf->MultiCell(56, 5,"5. Número de Identificación Tributaria (NIT)", 'T', 'L', 1, 0);
        $pdf->MultiCell(10, 5,"6. DV", 'T', 'L', 1, 0);
        $pdf->MultiCell(30, 5,"7. Primer apellido", 'T', 'L', 1, 0);
        $pdf->MultiCell(30, 5,"8. Segundo apellido", 'T', 'L', 1, 0);
        $pdf->MultiCell(30, 5,"9. Primer nombre", 'T', 'L', 1, 0);
        $pdf->MultiCell(30, 5,"10. Otros nombres", 'TR', 'L', 1, 0);
        $pdf->ln();
        $pdf->MultiCell(60, 5,"      ".$arrRetenedor['nit'], 'BRL', 'L', 1, 0);
        $pdf->MultiCell(10, 5,$arrRetenedor['sigla'], 'BRL', 'L', 1, 0);
        $pdf->MultiCell(30, 5,"", 'BRL', 'L', 1, 0);
        $pdf->MultiCell(30, 5,"", 'BRL', 'L', 1, 0);
        $pdf->MultiCell(30, 5,"", 'BRL', 'L', 1, 0);
        $pdf->MultiCell(30, 5,"", 'BRL', 'L', 1, 0);
        $pdf->ln();
        $pdf->setX(14);
        $pdf->MultiCell(186, 5,"11. Razón social", 'TRL', 'L', 1, 0);
        $pdf->ln();
        $pdf->setX(14);
        $pdf->MultiCell(186, 5,$arrRetenedor['razonsocial'], 'BRL', 'L', 1, 0);
        $pdf->setY(48);
        $pdf->ln();
        $pdf->SetFillColor(255,255,255);
        $pdf->SetTextColor(0,0,0);
        $pdf->StartTransform();
        $pdf->Rotate(90);
        $pdf->setX(10.6);
        $pdf->SetFont('helvetica','B',6);
        $pdf->MultiCell(12.5, 4, "Empleado", 1, 'C', 1, 0);
        $pdf->StopTransform();
        $pdf->setY(40);
        $pdf->setX(14);
        $pdf->SetFont('helvetica','',7);
        $pdf->MultiCell(20, 7.5,"24. Tipo de documento", 'RL', 'L', 1, 0);
        $pdf->MultiCell(46, 7.5,"25. Número de identificación", 'RL', 'L', 1, 0);
        $pdf->MultiCell(30, 7.5,"26. Primer apellido", 'LT', 'L', 1, 0);
        $pdf->MultiCell(30, 7.5,"27. Segundo apellido", 'T', 'L', 1, 0);
        $pdf->MultiCell(30, 7.5,"28. Primer nombre", 'T', 'L', 1, 0);
        $pdf->MultiCell(30, 7.5,"29. Otros nombres", 'TR', 'L', 1, 0);
        $pdf->ln();
        $pdf->setX(14);
        $pdf->MultiCell(20, 5,$arrTercero['tipo_documento'], 'BRL', 'L', 1, 0);
        $pdf->MultiCell(46, 5,$arrTercero['documento'], 'BRL', 'L', 1, 0);
        $pdf->MultiCell(30, 5,$arrTercero['primer_apellido'], 'BRL', 'L', 1, 0);
        $pdf->MultiCell(30, 5,$arrTercero['segundo_apellido'], 'BRL', 'L', 1, 0);
        $pdf->MultiCell(30, 5,$arrTercero['primer_nombre'], 'BRL', 'L', 1, 0);
        $pdf->MultiCell(30, 5,$arrTercero['segundo_nombre'], 'BRL', 'L', 1, 0);
        $pdf->ln();
        $pdf->SetFont('helvetica','',7);
        $pdf->MultiCell(54, 5,"Periodo de la certificación", 'TRL', 'C', 1, 0);
        $pdf->MultiCell(30, 5,"32. Fecha de expedición", 'TRL', 'C', 1, 0);
        $pdf->MultiCell(50, 5,"33. Lugar donde se practicó la retención", 'TRL', 'L', 1, 0);
        $pdf->MultiCell(26, 5,"34. Cód. Dpto.", 'TRL', 'L', 1, 0);
        $pdf->MultiCell(30, 5,"35. Cód. Municipio", 'TRL', 'L', 1, 0);
        $pdf->ln();
        $pdf->SetFont('helvetica','',7);
        $pdf->MultiCell(27, $alturaFilas,"30. DE: ".VIGENCIA.' - 01 - 01', 'LB', 'C', 1, 0);
        $pdf->MultiCell(27, $alturaFilas,"31. A: ".VIGENCIA.' - 12 - 31', 'BR', 'C', 1, 0);
        $pdf->MultiCell(30, $alturaFilas,date("Y-m-d"), 'LBR', 'C', 1, 0);
        $pdf->MultiCell(50, $alturaFilas,$arrRetenedor['razonsocial'], 'BRL', 'L', 1, 0);
        $pdf->MultiCell(26, $alturaFilas,$arrRetenedor['depto'], 'BRL', 'L', 1, 0);
        $pdf->MultiCell(30, $alturaFilas,$arrRetenedor['mnpio'], 'BRL', 'L', 1, 0);
        $pdf->ln();
        $pdf->SetFont('helvetica','B',7);
        $pdf->SetFillColor(225, 231, 238);
        $pdf->MultiCell(140, 3,"Concepto de los Ingresos", 'LBRT', 'C', 1, 0);
        $pdf->MultiCell(50, 3,"Valor", 'LBRT', 'C', 1, 0);
        $pdf->ln();
        $pdf->SetFillColor(241, 244, 247);
        $fill = 1;
        $columnasIngresos = $arrTercero['columnas_ingresos'];
        $columnasAportes = $arrTercero['columnas_aportes'];
        $totalIngresos = count($columnasIngresos);
        $totalAportes = count($columnasAportes);
        //Tabla ingresos
        $pdf->SetFont('helvetica','',7);
        for ($i=0; $i < $totalIngresos ; $i++) {
            $valor = "$".number_format($columnasIngresos[$i]['valor'],0,",",".");
            $pdf->Cell(140, $alturaFilas, $columnasIngresos[$i]['nombre'], 'LR', 0, 'L', $fill);
            $pdf->Cell(5, $alturaFilas, $columnasIngresos[$i]['renglon'], 'LR', 0, 'C', $fill);
            $pdf->Cell(45, $alturaFilas, $valor, 'LR', 0, 'R', $fill);
            $fill = !$fill;
            $pdf->ln();
        }
        $pdf->SetFont('helvetica','B',7);
        $pdf->Cell(140, $alturaFilas, 'Total de ingresos brutos (Sume '.$columnasIngresos[0]['renglon'].' a '.$columnasIngresos[$totalIngresos-1]['renglon'].')', 'LTRB', 0, 'L', true);
        $pdf->Cell(5, $alturaFilas, $arrTercero['renglon_ingreso'], 'LTRB', 0, 'L', true);
        $pdf->Cell(45, $alturaFilas, "$".number_format($arrTercero['total_ingresos'],0,",","."), 'LTRB', 0, 'R', true);
        $pdf->ln();
        $pdf->SetFont('helvetica','B',7);
        $pdf->SetFillColor(255, 255, 255);
        $pdf->cell(140, $alturaFilas,"Concepto de los aportes", 'LTRB',0, 'C', 1, 0);
        $pdf->cell(50, $alturaFilas,"Valor", 'LTRB',0, 'C', 1, 0);
        $pdf->ln();
        $pdf->SetFont('helvetica','',7);
        $pdf->SetFillColor(241, 244, 247);
        $fill = 1;
        //Tabla aportes
        for ($i=0; $i < $totalAportes ; $i++) {
            $pdf->SetFont('helvetica','',7);
            $pdf->SetFillColor(241, 244, 247);
            $pdf->SetTextColor(0,0,0);
            $borders ="LR";
            $valor = "$".number_format($columnasAportes[$i]['valor'],0,",",".");
            $linesNombre = $pdf->getNumLines($columnasAportes[$i]['nombre'], 30);
            $linesValor = $pdf->getNumLines($columnasAportes[$i]['valor'], 30);
            $linesRenglon = $pdf->getNumLines($columnasAportes[$i]['renglon'], 30);
            $alturas = $linesNombre > $alturaFilas ? $linesNombre:$alturaFilas;
            $pdf->SetFont('helvetica','',7);
            if($i==0 || $i == $totalAportes-1){
                $borders = "LTR";
                $lastRowBorders = $borders."B";
            }
            if($i == $totalAportes-1){
                $pdf->SetFont('helvetica','B',7);
                $pdf->SetFillColor(56,100,146);
                $pdf->SetTextColor(255,255,255);
                $pdf->MultiCell(140, $alturas,$columnasAportes[$i]['nombre'], $lastRowBorders, 'L', $fill, 0);
                $pdf->SetTextColor(0,0,0);
                $pdf->MultiCell(5, $alturas,$columnasAportes[$i]['renglon'], $lastRowBorders, 'C', 0, 0);
                $pdf->MultiCell(45, $alturas,$valor, $lastRowBorders, 'R', 0, 0);
            }else{
                $pdf->MultiCell(140, $alturas,$columnasAportes[$i]['nombre'], $borders, 'L', $fill, 0);
                $pdf->MultiCell(5, $alturas,$columnasAportes[$i]['renglon'], $borders, 'C', $fill, 0);
                $pdf->MultiCell(45, $alturas,$valor, $borders, 'R', $fill, 0);
            }
            $fill = !$fill;
            $pdf->ln();
        }
        $pdf->SetFillColor(255, 255, 255);
        $pdf->SetFont('helvetica','',7);
        $pdf->cell(190, $alturaFilas,"Nombre del pagador o agente retenedor", 'LTRB', 0,'L', 1, 0);
        $pdf->ln();

        //Tabla trabajador
        $pdf->SetFont('helvetica','B',7);
        $pdf->cell(190, $alturaFilas,"Datos a cargo del trabajador", 'LTRB', 0,'C', 1, 0);
        $pdf->ln();
        $pdf->SetFont('helvetica','B',7);
        $pdf->SetFillColor(225, 231, 238);
        $pdf->cell(90, $alturaFilas,"Concepto de otros ingresos", 1,'', 'C', 1, 0);
        $pdf->cell(50, $alturaFilas,"Valor recibido", 1,'LBRT', 'C', 1, 0);
        $pdf->cell(50, $alturaFilas,"Valor retenido", 1,'LBRT', 'C', 1, 0);
        $pdf->ln();
        $pdf->SetFillColor(241, 244, 247);
        $fill = 1;
        $rowsEmpleado = count($arrEmpleado);
        $pdf->SetFont('helvetica','',7);
        for ($i=0; $i < $rowsEmpleado ; $i++) {
            $pdf->Cell(90, $alturaFilas, $arrEmpleado[$i]['nombre'], 'LR', 0, 'L', $fill);
            $pdf->Cell(5, $alturaFilas, $arrEmpleado[$i]['renglon1'], 'LR', 0, 'C', $fill);
            $pdf->Cell(45, $alturaFilas, "", 'LR', 0, 'C', $fill);
            $pdf->Cell(5, $alturaFilas, $arrEmpleado[$i]['renglon2'], 'LR', 0, 'C', $fill);
            $pdf->Cell(45, $alturaFilas, "", 'LR', 0, 'C', $fill);
            $fill = !$fill;
            $pdf->ln();
        }
        $pdf->SetFont('helvetica','B',7);
        $pdf->SetFillColor(225, 231, 238);
        $pdf->cell(10, $alturaFilas,"Item", 1,'', 'C', 1, 0);
        $pdf->cell(135, $alturaFilas,($arrEmpleado[7]['renglon2']+1).". Identificación de los bienes poseídos", 1,'LBRT', 'C', 1, 0);
        $pdf->cell(45, $alturaFilas,($arrEmpleado[7]['renglon2']+2).". Valor patrimonial", 1,'LBRT', 'C', 1, 0);
        $pdf->ln();
        $pdf->SetFillColor(241, 244, 247);
        $pdf->cell(10, $alturaFilas,"1",'LRT',0, 'C', 0);
        $pdf->cell(135, $alturaFilas,"",'LRT',0, 'C', 0);
        $pdf->cell(45, $alturaFilas,"", 'LRT', 0,'C', 0);
        $pdf->ln();
        $pdf->cell(10, $alturaFilas,"2",'LR',0, 'C', 1);
        $pdf->cell(135, $alturaFilas,"",'LR',0, 'C', 1);
        $pdf->cell(45, $alturaFilas,"", 'LR',0, 'C', 1);
        $pdf->ln();
        $pdf->cell(10, $alturaFilas,"3",'LR',0, 'C', 0);
        $pdf->cell(135, $alturaFilas,"",'LR',0, 'C', 0);
        $pdf->cell(45, $alturaFilas,"", 'LR', 0,'C', 0);
        $pdf->ln();
        $pdf->cell(10, $alturaFilas,"4",'LR',0, 'C', 1);
        $pdf->cell(135, $alturaFilas,"",'LR',0, 'C', 1);
        $pdf->cell(45, $alturaFilas,"", 'LR',0, 'C', 1);
        $pdf->ln();
        $pdf->cell(10, $alturaFilas,"5",'LR',0, 'C', 0);
        $pdf->cell(135, $alturaFilas,"",'LR',0, 'C', 0);
        $pdf->cell(45, $alturaFilas,"", 'LR', 0,'C', 0);
        $pdf->ln();
        $pdf->cell(10, $alturaFilas,"6",'LR',0, 'C', 1);
        $pdf->cell(135, $alturaFilas,"",'LR',0, 'C', 1);
        $pdf->cell(45, $alturaFilas,"", 'LR',0, 'C', 1);
        $pdf->ln();
        $pdf->SetFont('helvetica','B',7);
        $pdf->SetFillColor(56,100,146);
        $pdf->SetTextColor(255,255,255);
        $pdf->cell(140, $alturaFilas,"Deudas vigentes a 31 de Diciembre de ".VIGENCIA,'LRBT',0, 'L', 1);
        $pdf->SetTextColor(0,0,0);
        $pdf->cell(5, $alturaFilas,($arrEmpleado[7]['renglon2']+3),'LRBT',0, 'C', 0);
        $pdf->cell(45, $alturaFilas,"",'LRBT',0, 'C', 0);
        $pdf->ln();
        $pdf->SetFont('helvetica','B',7);
        $pdf->SetFillColor(225, 231, 238);
        $pdf->cell(190, $alturaFilas,"Identificación del dependiente económico de acuerdo al parágrafo 2 del artículo 387 del Estatuto Tributario", 1,'LBRT', 'C', 1, 0);
        $pdf->ln();
        $pdf->SetFillColor(255,255,255);
        $pdf->SetFont('helvetica','',7);
        $pdf->cell(30, $alturaFilas,($arrEmpleado[7]['renglon2']+4).". Tipo documento",'LRT',0, 'L', 0);
        $pdf->cell(30, $alturaFilas,($arrEmpleado[7]['renglon2']+5).". No. Documento",'LRT',0, 'L', 0);
        $pdf->cell(100, $alturaFilas,($arrEmpleado[7]['renglon2']+6).". Apellidos y Nombres",'LRT',0, 'L', 0);
        $pdf->cell(30, $alturaFilas,($arrEmpleado[7]['renglon2']+7).". Parentesco",'LRT',0, 'L', 0);
        $pdf->ln();
        $pdf->cell(30, $alturaFilas+1,"",'LRB',0, 'L', 0);
        $pdf->cell(30, $alturaFilas+1,"",'LRB',0, 'L', 0);
        $pdf->cell(100, $alturaFilas+1,"",'LRB',0, 'L', 0);
        $pdf->cell(30, $alturaFilas+1,"",'LRB',0, 'L', 0);
        $pdf->ln();
        $pdf->SetFont('helvetica','',6);

        $strCertifico = "Certifico que durante el año gravable ".VIGENCIA.":
            Mi patrimonio bruto no excedió de 4.500 UVT ($171.018.000)
            Mis ingresos brutos fueron inferiores a 1.400 UVT ($53.206.000).
            No fui responsable del impuesto sobre las ventas a 31 de diciembre de 2022
            Mis consumos mediante tarjeta de crédito no excedieron la suma de 1.400 UVT ($53.206.000)
            Que el total de mis compras y consumos no superaron la suma de 1.400 UVT ($53.206.000)
            Que el valor total de mis consignaciones bancarias, depósitos o inversiones financieras no excedieron los 1.400 UVT ($53.206.000)
            Por lo tanto, manifiesto que no estoy obligado a presentar declaración de renta y complementario por el año gravable ".VIGENCIA.".";

        $strNota = "Nota: Este certificado sustituye para todos los efectos legales la declaración de Renta y";
        $strNota.=" complementario para el trabajador o pensionado que lo firme.";
        $strNota.=" Para aquellos trabajadores independientes contribuyentes del impuesto unificado deberán presentar la declaración anual consolidada del Régimen Simple de Tributación (SIMPLE)";
        $linesCertifico = $pdf->getNumLines($strCertifico,30)-8;
        $pdf->MultiCell(150, $linesCertifico,$strCertifico, 'LRB', 'L', 0, 0);
        $pdf->SetFont('helvetica','',7);
        $pdf->cell(40, $linesCertifico,"Firma del Trabajador o Pensionado",'LRB',0, 'L', 0,"",0,false,"T","T");
        $pdf->ln();
        $pdf->SetFont('helvetica','B',7);
        $pdf->MultiCell(190, 10,$strNota, '', 'L', 0, 0);
        $pdf->Output('Formulario_220_'.VIGENCIA.'.pdf', 'I');
    }
?>
