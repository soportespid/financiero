<?php
	header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=utf8");
	require "comun.inc";
	require "funciones.inc";
	require "conversor.php";
	session_start();
	cargarcodigopag($_GET['codpag'],$_SESSION["nivel"]);
	$linkbd=conectar_v7();
	titlepag();
?>
<html lang="es">
	<head>
		<meta charset="UTF-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
    	<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<title>:: IDEAL 10 - Tesorer&iacute;a</title>
		<link href="favicon.ico" rel="shortcut icon"/>
		<script src="css/programas.js"></script>
		<script src="css/calendario.js"></script>
		<link href="css/css2.css" rel="stylesheet" type="text/css" />
		<link href="css/css3.css" rel="stylesheet" type="text/css" />
		<link href="css/css4.css" rel="stylesheet" type="text/css" />
		<link href="css/tabs.css" rel="stylesheet" type="text/css" />

		<script>
			function validar() {
				document.form2.submit();
			}

			function buscater(e) {
				if (document.form2.tercero.value!="") {
					document.form2.bt.value='1';
					document.form2.submit();
				}
			}

			function agregardetalle() {

				if(document.form2.codingreso.value!="" &&  parseFloat(document.form2.valor.value)>0 && document.form2.cc.value!="") {

					document.form2.agregadet.value=1;
					document.form2.submit();
				}
				else {
					despliegamodalm('visible','2','Falta informacion para poder Agregar')
				}
			}

			/* function eliminar(variable)
			{
				if (confirm("Esta Seguro de Eliminar")) {

					document.form2.elimina.value=variable;
					vvend=document.getElementById('elimina');
					vvend.value=variable;
					document.form2.submit();
				}
			} */

			function eliminar(variable){
				document.form2.elimina.value=variable;
				vvend=document.getElementById('elimina');
				vvend.value=variable;
				document.form2.submit();
			}

			function guardar() {

				ingresos2=document.getElementsByName('dcoding[]');

				if (document.form2.fecha.value!='' && ingresos2.length>0) {

					despliegamodalm('visible','4','Esta Seguro de Guardar','2');

					/* if (confirm("Esta Seguro de Guardar")) {

						document.form2.oculto.value=2;
						document.form2.submit();
					} */
				}
				else {

					despliegamodalm('visible','2','Faltan datos para completar el registro');
					document.form2.fecha.focus();
					document.form2.fecha.select();
				}
			}

			function pdf() {

				document.form2.action="teso-pdfrecaudos.php";
				document.form2.target="_BLANK";
				document.form2.submit();
				document.form2.action="";
				document.form2.target="";
			}

			function buscater(e)
			{
				if (document.form2.tercero.value!="") {
					document.form2.bt.value='1';
					document.form2.submit();
				}
			}

			function buscaing(e)
			{
				if (document.form2.codingreso.value!="") {

					document.form2.bin.value='1';
					document.form2.submit();
				}
			}

			function despliegamodal2(_valor,_tip)
			{
				document.getElementById("bgventanamodal2").style.visibility=_valor;
				if(_valor=="hidden"){document.getElementById('ventana2').src="";}
				else
				{
					if(_tip=='1')
					{document.getElementById('ventana2').src="tercerosgral-ventana01.php?objeto=tercero&nobjeto=ntercero&nfoco=codingreso";}
					else
					{document.getElementById('ventana2').src="ingresos-ventana01.php?ti=I&modulo=4";}
				}
			}

			function despliegaModalFuentes(_valor){

				var codigo=document.getElementById('codingreso').value;

				if (codigo != "") {

					document.getElementById("bgventanamodal2").style.visibility=_valor;
					if(_valor=="hidden"){document.getElementById('ventana2').src="";}
					else
					{


						if (codigo != "") {
							document.getElementById('ventana2').src="teso-ventana-fuentes.php?codigo="+codigo;
						}
						else {

							despliegamodalm('visible','2','Seleccione primero el código de ingreso');

						}
					}
				}
				else {
					despliegamodalm('visible','2','Seleccione primero el código de ingreso');
				}
			}

			function despliegamodalm(_valor,_tip,mensa,pregunta){
				document.getElementById("bgventanamodalm").style.visibility=_valor;
				if(_valor=="hidden"){
					document.getElementById('ventanam').src="";
					switch(document.getElementById('valfocus').value){
						case "1":	document.getElementById('valfocus').value='';
									document.getElementById('tercero').focus();
									document.getElementById('tercero').select();
									break;
						case "2":	document.getElementById('valfocus').value='';
									document.getElementById('banco').value='';
									document.getElementById('banco').focus();
									document.getElementById('banco').select();
					}
				}else{
					switch(_tip){
						case "1":
							document.getElementById('ventanam').src="ventana-mensaje1.php?titulos="+mensa;break;
						case "2":
							document.getElementById('ventanam').src="ventana-mensaje3.php?titulos="+mensa;break;
						case "3":
							document.getElementById('ventanam').src="ventana-mensaje2.php?titulos="+mensa;break;
						case "4":
							document.getElementById('ventanam').src="ventana-consulta1.php?titulos="+mensa+"&idresp="+pregunta;break;
					}
				}
			}

			function funcionmensaje(){
				var numdocar=document.getElementById('idcomp').value;
				document.location.href = "teso-editasinrecaudos.php?idrecaudo="+numdocar+"&scrtop=null&totreg=1&altura=null&numpag=1&limreg=10&filtro=null";
			}

			function respuestaconsulta(pregunta){
				switch(pregunta){
					case "1":	document.form2.oculto.value='3';
								document.form2.submit();
								break;
					case "2":	document.form2.oculto.value='2';
								document.form2.submit();
								break;
				}
			}
		</script>
	</head>

<body>
	<IFRAME src="alertas.php" name="alertas" id="alertas" style="display:none"></IFRAME>
	<span id="todastablas2"></span>

	<table>
		<tr>
			<script>barra_imagenes("teso");</script><?php cuadro_titulos();?>
		</tr>

		<tr>
			<?php menu_desplegable("teso");?>
		</tr>

		<tr>
			<td colspan="3" class="cinta">
				<a href="teso-sinrecaudos.php" class="mgbt"><img src="imagenes/add.png" title="Nuevo"/></a>
				<a onClick="guardar()" class="mgbt"><img src="imagenes/guarda.png"  title="Guardar" /></a>
				<a href="teso-buscasinrecaudos.php" class="mgbt"> <img src="imagenes/busca.png"  title="Buscar" /></a>
				<a onClick="mypop=window.open('plan-agenda.php','','');mypop.focus()" class="mgbt"><img src="imagenes/agenda1.png" title="Agenda" /></a>
				<a onClick="mypop=window.open('teso-principal.php','','');mypop.focus();" class="mgbt"><img src="imagenes/nv.png" title="Nueva ventana"></a>
				<a onClick="mypop=window.open('<?php echo $url2; ?>','','');mypop.focus();" class="tooltip bottom mgbt"><img src="imagenes/duplicar_pantalla.png" title="Duplicar pesta&ntilde;a"></a>
				<a <?php if($_POST['oculto']==2) { ?> onClick="pdf()"  <?php } ?> class="mgbt"> <img src="imagenes/printd.png"  title="Buscar" /></a>
			</td>
		</tr>
	</table>

	<div id="bgventanamodalm" class="bgventanamodalm">
            <div id="ventanamodalm" class="ventanamodalm">
                <IFRAME src="" name="ventanam" marginWidth=0 marginHeight=0 frameBorder=0 id="ventanam" frameSpacing=0 style=" width:700px; height:130px; top:200; overflow:hidden;">
                </IFRAME>
            </div>
        </div>
	<?php
		$vigencia=date('Y');
		$vigusu=vigencia_usuarios($_SESSION['cedulausu']);

		if($_POST['oculto']=="") {
 			$_POST['dcoding']= array();
		 	$_POST['dncoding']= array();
			$_POST['dfuente']= array();
		 	$_POST['dvalores']= array();
			$check1="checked";
			$fec=date("d/m/Y");
			$_POST['vigencia']=$vigusu;

			$sqlr="select *from cuentacaja where estado='S' and vigencia=".$vigusu;
			$res=mysqli_query($linkbd,$sqlr);
			while ($row =mysqli_fetch_row($res)) {
	 			$_POST['cuentacaja']=$row[1];
			}

	 		$sqlr="select max(id_recaudo) from tesosinrecaudos ";
			$res=mysqli_query($linkbd,$sqlr);
			$consec=0;

			while($r=mysqli_fetch_row($res)) {
	  			$consec=$r[0];
	 		}

	 		$consec+=1;
	 		$_POST['idcomp']=$consec;
			$fec=date("d/m/Y");
			$_POST['fecha']=$fec;
			$_POST['valor']=0;
		}

		switch($_POST['tabgroup1']) {
			case 1:
				$check1='checked';
			break;

			case 2:
				$check2='checked';
			break;

			case 3:
				$check3='checked';
		}
	?>

	<form name="form2" method="post" action="">
 		<?php
 			//***** busca tercero
			if($_POST['bt']=='1') {

				$nresul=buscatercero($_POST['tercero']);

			  	if($nresul!='') {
			  		$_POST['ntercero']=$nresul;
  				}
				else{
					$_POST['ntercero']="";
				}
			}

			//******** busca ingreso *****
			if($_POST['bin']=='1') {

				$sql = "SELECT * FROM tesoingresos WHERE codigo = '$_POST[codingreso]' AND estado='S' ORDER BY codigo";
				$row = mysqli_fetch_row(mysqli_query($linkbd, $sql));

			  	if($row[0]!='') {
			  		$_POST['ningreso']=$row[1];
					if ($row[6] == 'N') {
						$_POST['causacion'] = 2;
					}
					else {
						$_POST['causacion'] = 1;
					}
  				}
			 	else {
			  		$_POST['ningreso']="";
					$_POST['codingreso']="";
					echo"
					  <script>
						  document.getElementById('valfocus').value='2';
						  despliegamodalm('visible','2','Codigo Ingresos Incorrecto');
					  </script>";
			  	}

				$_POST['conFuente'] = ($row[4] == '') ? true : false;
			}

 		?>


    	<table class="inicio" align="center" >
      		<tr>
				<td style="width:95%;" class="titulos" colspan="2">Reconocimiento Recaudos internos</td>
				<td style="width:5%;" class="cerrar" ><a href="teso-principal.php">Cerrar</a></td>
      		</tr>

			<tr>
				<td>
					<table>
						<tr>
							<td style="width:12%;" class="saludo1" >Numero Liquidacion:</td>
							<td  style="width:7%;" >
								<input name="idcomp" id = "idcomp" type="text"  value="<?php echo $_POST['idcomp']?>" onKeyUp="return tabular(event,this) "  readonly>
							</td>
							<td style="width:10%;"  class="saludo1">Fecha:</td>
							<td style="width:10%;">
								<input type="text" name="fecha"  value="<?php echo $_POST['fecha']?>" onKeyUp="return tabular(event,this)" id="fc_1198971545" title="DD/MM/YYYY" onDblClick="displayCalendarFor('fc_1198971545');" class="colordobleclik" autocomplete="off" onChange="" readonly>
							</td>
							<td style="width:5%;" class="saludo1">Vigencia:</td>
							<td style="width:10%;">
								<input type="text" id="vigencia" name="vigencia"  onKeyPress="javascript:return solonumeros(event)" onKeyUp="return tabular(event,this)" style="width:40%;" value="<?php echo $_POST['vigencia']?>" onClick="document.getElementById('tipocta').focus();document.getElementById('tipocta').select();" readonly>
							</td>
							<td style="width:10%;" class="saludo1">Causacion Contable:</td>
							<td>
								<select name="causacion" id="causacion" onKeyUp="return tabular(event,this)"  >
									<option value="1" <?php if($_POST['causacion']=='1') echo "SELECTED"; ?>>Si</option>
									<option value="2" <?php if($_POST['causacion']=='2') echo "SELECTED"; ?>>No</option>
								</select>
							</td>

						</tr>
						<tr>
							<td class="saludo1">Concepto Liquidacion:</td>
							<td colspan="9" >
								<textarea name="concepto" id="concepto" style="width:100%; form-sizing: content; max-height: 100px; min-height: 20px; resize: vertical; " class="estilos-scroll"><?php echo $_POST['concepto']?></textarea>
							</td>
						</tr>
						<tr>
							<td class="saludo1">Contribuyente: </td>
							<td>
								<input name="tercero" type="text" value="<?php echo $_POST['tercero']?>" style="width:80%;" onKeyUp="return tabular(event,this)" onchange="buscater(event)">
								<a href="#" onClick="mypop=window.open('terceros-ventana.php?ti=1','','menubar=0,scrollbars=yes, toolbar=no, location=no, width=700px,height=500px');mypop.focus();">
									<img src="imagenes/find02.png" style="width:20px;cursor:pointer;"/>
								</a>
							</td>
							<td colspan="8">
								<input type="text" id="ntercero" name="ntercero" value="<?php echo $_POST['ntercero']?>" style="width:100%;" onKeyUp="return tabular(event,this) "  readonly>
								<input type="hidden" value="0" name="bt">
								<input type="hidden" id="cb" name="cb" value="<?php echo $_POST['cb']?>" >
								<input type="hidden" id="ct" name="ct" value="<?php echo $_POST['ct']?>" >
								<input type="hidden" value="1" name="oculto" id="oculto">
								<input name="cobrorecibo" type="hidden" value="<?php echo $_POST['cobrorecibo']?>" >
								<input name="vcobrorecibo" type="hidden" value="<?php echo $_POST['vcobrorecibo']?>" >
								<input name="tcobrorecibo" type="hidden" value="<?php echo $_POST['tcobrorecibo']?>" >
							</td>

						</tr>
						<tr>
							<td style="width:95%;" class="titulos" colspan="9">Determinaci&oacute;n de c&oacute;digos de ingresos</td>
						</tr>
						<tr>
							<td class="saludo1">Cod Ingreso:</td>
							<td>
								<input type="text" id="codingreso" name="codingreso" value="<?php echo $_POST['codingreso']?>" style="width:80%;" onKeyUp="return tabular(event,this)" onchange="buscaing(event)" >
								<a href="#" onClick="despliegamodal2('visible','2');">
									<img src="imagenes/find02.png" style="width:20px;cursor:pointer;"/>
								</a>
							</td>
							<td colspan="8">
								<input type="hidden" value="0" name="bin">
								<input name="ningreso" type="text" id="ningreso" value="<?php echo $_POST['ningreso']?>" style="width:100%;" readonly>
							</td>
						</tr>
						<tr>
							<?php
								if($_POST['conFuente']){
									?>
									<td class="saludo1">Fuente:</td>
									<td>
										<input type="text" id="fuente" name="fuente" value="<?php echo $_POST['fuente'] ?>" style="width:80%;" readonly>
										<a onClick="despliegaModalFuentes('visible')" title="Listado de fuentes"><img src="imagenes/find02.png" style="width:20px;cursor:pointer;"/></a>
									</td>
									<td colspan="8">
										<input type="text" name="nfuente" id="nfuente" value="<?php echo $_POST['nfuente']?>" style="width:100%;" readonly>
									</td>
									<?php
								}
							?>
							<input type="hidden" name = 'conFuente' value="<?php echo $_POST['conFuente'] ?>">
						</tr>
						<tr>
							<td class="saludo1" >Valor:</td>
							<td>
								<input type="text" id="valor" name="valor" value="<?php echo $_POST['valor']?>" style="width:80%;" onKeyDown ="return tabular(event,this)" >
							</td>
							<td class="saludo1">Centro Costo:</td>
							<td>
								<select name="cc"  onChange="validar()" onKeyUp="return tabular(event,this)" style="width:100%;">
									<option value="">Seleccione ...</option>
									<?php
										$sqlr="select *from centrocosto where estado='S' order by id_cc	";
										$res=mysqli_query($linkbd,$sqlr);
										while ($row =mysqli_fetch_row($res))
										{
											if("$row[0]"==$_POST['cc']){echo "<option value='$row[0]' SELECTED>$row[0] - $row[1]</option>";}
											else {echo "<option value='$row[0]'>$row[0] - $row[1]</option>";}
										}
									?>
								</select>
							</td>
							<td></td>
							<td >
								<input  type="button" name="agregact" value="Agregar" onClick="agregardetalle()" style = "width: 80%">
								<input type="hidden" value="0" name="agregadet">
							</td>
						</tr>
					</table>
				</td>
			</tr>
    	</table>

       	<?php
           	//***** busca tercero
			if($_POST['bt']=='1') {
			  	$nresul=buscatercero($_POST['tercero']);

				if($nresul!='') {

			  		$_POST['ntercero']=$nresul;
  		?>
			  		<script>
			  			document.getElementById('codingreso').focus();
						document.getElementById('codingreso').select();
					</script>
		<?php
			  	}
			 	else {
			  		$_POST['ntercero']="";
		?>
			  		<script>

						despliegamodalm('visible','2','Tercero Incorrecto o no Existe');
						document.form2.tercero.focus();
			  		</script>
		<?php
			  	}
			}

			//*** ingreso

		?>

     	<div class="subpantallac7 estilos-scroll" style="resize: vertical;">
	   		<table class="inicio">
	   	   		<tr>
					<td colspan="6" class="titulos">Detalle Reconocimiento Recaudos internos</td>
				</tr>
				<tr>
					<td class="titulos2" width="10%">C&oacute;digo</td>
					<td class="titulos2">Ingreso</td>
					<td class="titulos2" width="10%">Centro Costo</td>
					<td class="titulos2" width="15%">Fuente</td>
					<td class="titulos2" width="10%">Valor</td>
					<td class="titulos2" style = "text-align: center;">
						<img src="imagenes/del.png" >
						<input type='hidden' name='elimina' id='elimina'>
					</td>
				</tr>

				<?php
					if ($_POST['elimina']!=''){

						$posi=$_POST['elimina'];

						unset($_POST['dcoding'][$posi]);
						unset($_POST['dncoding'][$posi]);
						unset($_POST['dncc'][$posi]);
						unset($_POST['dfuente'][$posi]);
						unset($_POST['dvalores'][$posi]);

						$_POST['dcoding']= array_values($_POST['dcoding']);
						$_POST['dncoding']= array_values($_POST['dncoding']);
						$_POST['dncc']= array_values($_POST['dncc']);
						$_POST['dfuente']= array_values($_POST['dfuente']);
						$_POST['dvalores']= array_values($_POST['dvalores']);
					}

					if ($_POST['agregadet']=='1'){

						preg_match("/([0-9]{2})\/([0-9]{2})\/([0-9]{4})/", $_POST['fecha'], $fecha);
						$fechaf = $fecha[3]."-".$fecha[2]."-".$fecha[1];

						$sqlri = "SELECT concepto FROM tesoingresos_det WHERE codigo='".$_POST['codingreso']."' AND vigencia=(SELECT MAX(vigencia) FROM tesoingresos_det WHERE codigo = '".$_POST['codingreso']."' AND estado='S')";
						$resi = mysqli_query($linkbd,$sqlri);
						$rowi = mysqli_fetch_row($resi);

						$sq = "SELECT fechainicial FROM conceptoscontables_det WHERE codigo='".$rowi[0]."' AND modulo='4' and tipo='C' AND fechainicial<'$fechaf' AND cc='".$_POST['cc']."' AND cuenta!='' order by fechainicial asc";
						$re = mysqli_query($linkbd,$sq);
						while($ro = mysqli_fetch_assoc($re))
						{
							$_POST['fechacausa']=$ro["fechainicial"];
						}
						$sqlrc = "SELECT * FROM conceptoscontables_det WHERE estado='S' AND modulo='4' AND codigo='".$rowi[0]."' AND tipo='C' AND cc='".$_POST['cc']."' AND fechainicial='".$_POST['fechacausa']."'";
						$resc = mysqli_query($linkbd,$sqlrc);
						$rowc = mysqli_fetch_row($resc);

						if($rowc[4]!='') {
							$_POST['dcoding'][]=$_POST['codingreso'];
							$_POST['dncoding'][]=$_POST['ningreso'];
							$_POST['dncc'][]=$_POST['cc'];
							$_POST['dfuente'][]=$_POST['fuente'];
							$_POST['valor']=str_replace(",","",$_POST['valor']);
							$_POST['dvalores'][]=$_POST['valor'];
							$_POST['agregadet']=0;
							?>
							<script>
								document.form2.codingreso.value="";
								document.form2.valor.value="0";
								document.form2.fuente.value="";
								document.form2.nfuente.value="";
								document.form2.ningreso.value="";
								document.form2.cc.value="";
								document.form2.codingreso.select();
								document.form2.codingreso.focus();
							</script>
							<?php
						}
						else {
							echo "<script>despliegamodalm('visible','2','No hay parametrizacion contable para este codigo de ingreso y centro de costo');</script>";
							$_POST['agregadet']=0;
						}


					}

					$_POST['totalc']=0;
					$co="saludo1a";
		  			$co2="saludo2";

					for ($x=0;$x<count($_POST['dcoding']);$x++)
					{
						echo "
							<input name='dcoding[]' value='".$_POST['dcoding'][$x]."' type='hidden'>
							<input name='dncoding[]' value='".$_POST['dncoding'][$x]."' type='hidden'>
							<input name='dncc[]' value='".$_POST['dncc'][$x]."' type='hidden'>
							<input name='dfuente[]' value='".$_POST['dfuente'][$x]."' type='hidden'>
							<input name='dvalores[]' value='".$_POST['dvalores'][$x]."' type='hidden'>

							<tr class='$co' onMouseOver=\"anterior=this.style.backgroundColor;this.style.backgroundColor='#40b3ff';\" onMouseOut=\"this.style.backgroundColor=anterior\">
								<td>".$_POST['dcoding'][$x]."</td>
								<td>".$_POST['dncoding'][$x]."</td>
								<td>".$_POST['dncc'][$x]."</td>
								<td>".$_POST['dfuente'][$x]."</td>
								<td style='text-align:right; padding-right:10px'>$ ".number_format($_POST['dvalores'][$x],2)."</td>
								<td style='text-align:center;'><a onclick='eliminar($x)'><img src='imagenes/del.png' style='cursor:pointer;'></a></td>
							</tr>";

						$_POST['totalc']=$_POST['totalc']+$_POST['dvalores'][$x];
						$aux=$co;
						$co=$co2;
						$co2=$aux;
					}

					$resultado = convertir($_POST['totalc']);
					$_POST['totalcf']=number_format($_POST['totalc'],2,".",",");
					$_POST['letras']=$resultado." Pesos";

					echo "
						<input name='totalcf' type='hidden' value='$_POST[totalcf]' readonly >
						<input name='totalc' type='hidden' value='$_POST[totalc]'>
						<tr class='$co'>
							<td colspan='4' style='font-weight:bold; font-size:14px'>Total:</td>
							<td style='text-align:right; padding-right:10px'>
								$ ".$_POST['totalcf']."
							</td>
							<td></td>
						</tr>
						<tr class='$co2'>
							<input name='letras' type='hidden' value='$_POST[letras]'>
							<td style='font-weight:bold; font-size:14px'>Son:</td>
							<td colspan='5'>
								".$_POST['letras']."
							</td>
						</tr>";
				?>
	  		</table>
		</div>

	 	<?php
			if($_POST['oculto']=='2'){
 				preg_match("/([0-9]{2})\/([0-9]{2})\/([0-9]{4})/", $_POST['fecha'],$fecha);
				$fechaf=$fecha[3]."-".$fecha[2]."-".$fecha[1];
				$bloq=bloqueos($_SESSION['cedulausu'],$fechaf);

				if($bloq>=1) {
					$consec=0;
					$sqlr="select max(id_recaudo) from tesosinrecaudos" ;
					//echo $sqlr;
					$res=mysqli_query($linkbd,$sqlr);
					while($r=mysqli_fetch_row($res))
					{
						$consec=$r[0];
					}

					$consec+=1;
					$conceptoCont = '';

					if($_POST['causacion']=='2') {
						$conceptoCont="ESTE DOCUMENTO NO REQUIERE CAUSACION CONTABLE - ".$_POST['concepto'];
					}else{
						$conceptoCont=$_POST['concepto'];
					}

					$sqlr="insert into comprobante_cab (numerotipo,tipo_comp,fecha,concepto,total,total_debito,total_credito,diferencia,estado) values ($consec,26,'$fechaf','".strtoupper($conceptoCont)."',0,$_POST[totalc],$_POST[totalc],0,'1')";
					mysqli_query($linkbd,$sqlr);
					$idcomp=mysqli_insert_id($linkbd);
					echo "<input type='hidden' name='ncomp' value='$idcomp'>";

					//******************* DETALLE DEL COMPROBANTE CONTABLE *********************
					if($_POST['causacion']!='2')
					{
						for($x=0;$x<count($_POST['dcoding']);$x++)
						{
							//***** BUSQUEDA INGRESO ********
							$sqlri="Select * from tesoingresos_det where codigo='".$_POST['dcoding'][$x]."' and vigencia=(SELECT MAX(vigencia) FROM tesoingresos_det WHERE codigo = '".$_POST['dcoding'][$x]."' AND estado='S')";
							$resi=mysqli_query($linkbd,$sqlri);
							//	echo "$sqlri <br>";
							while($rowi=mysqli_fetch_row($resi))
							{
								//**** busqueda concepto contable*****
								$sq="select fechainicial from conceptoscontables_det where codigo='".$rowi[2]."' and modulo='4' and tipo='C' and fechainicial<'$fechaf' and cc='".$_POST['dncc'][$x]."' and cuenta!='' order by fechainicial asc";
								$re=mysqli_query($linkbd,$sq);
								while($ro=mysqli_fetch_assoc($re))
								{
									$_POST['fechacausa']=$ro["fechainicial"];
								}
								$sqlrc="Select * from conceptoscontables_det where estado='S' and modulo='4' AND codigo='".$rowi[2]."' and tipo='C' and cc='".$_POST['dncc'][$x]."' and fechainicial='".$_POST['fechacausa']."'";
								$resc=mysqli_query($linkbd,$sqlrc);
								//	echo "con: $sqlrc <br>";
								while($rowc=mysqli_fetch_row($resc))
								{
									$porce=$rowi[5];
									if($rowc[6]=='S')
									{
										$valordeb=$_POST['dvalores'][$x]*($porce/100);
										$valorcred=0;
									}
									else
									{
										$valorcred=$_POST['dvalores'][$x]*($porce/100);
										$valordeb=0;
									}

									$sqlr="insert into comprobante_det (id_comp,cuenta,tercero,centrocosto,detalle,cheque,valdebito,valcredito,estado,vigencia) values ('26 $consec','".$rowc[4]."','".$_POST['tercero']."','".$rowc[5]."','Causacion ".strtoupper($_POST['dncoding'][$x])."','',".$valordeb.",".$valorcred.",'1','".$_POST['vigencia']."')";
									mysqli_query($linkbd,$sqlr);
									//echo "Conc: $sqlr <br>";
								}
							}
						}
					}

					//************ insercion de cabecera recaudos ************

					$sqlr="insert into tesosinrecaudos (id_comp,fecha,vigencia,tercero,valortotal,concepto,estado,cc) values($idcomp,'$fechaf',".$vigusu.",'$_POST[tercero]','$_POST[totalc]','".strtoupper($_POST['concepto'])."','S','".$_POST['cc']."')";
					mysqli_query($linkbd,$sqlr);

					$idrec=mysqli_insert_id($linkbd);
					//************** insercion de consignaciones **************
					for($x=0;$x<count($_POST['dcoding']);$x++)
					{
						$sqlr="insert into tesosinrecaudos_det (id_recaudo,ingreso,valor,estado,cc,fuente) values($idrec,'".$_POST['dcoding'][$x]."',".$_POST['dvalores'][$x].",'S','".$_POST['dncc'][$x]."','".$_POST['dfuente'][$x]."')";
						if (!mysqli_query($linkbd,$sqlr))
						{
							echo "<table class='inicio'><tr><td class='saludo1'><center><font color=blue><img src='imagenes/alert.png'> Manejador de Errores de la Clase BD<br><font size=1></font></font><br><p align=center>No se pudo ejecutar la petici�n: <br><font color=red><b>$sqlr</b></font></p>";
							//	 $e =mysql_error($respquery);
							echo "Ocurri� el siguiente problema:<br>";
							//echo htmlentities($e['message']);
							echo "<pre>";
							///echo htmlentities($e['sqltext']);
							// printf("\n%".($e['offset']+1)."s", "^");
							echo "</pre></center></td></tr></table>";
						}
						else
						{
							echo "<table class='inicio'><tr><td class='saludo1'><center>Se ha almacenado el Recaudo con Exito <img src='imagenes/confirm.png'></center></td></tr></table>";
							?>
							<script>
								document.form2.numero.value="";
								document.form2.valor.value=0;
							</script>
							<?php
						}
					}

					echo "<script>despliegamodalm('visible','1','Se ha almacenado el Egreso con Exito');</script>";

				}
				else
				{
					echo "<div class='inicio'><img src='imagenes\alert.png'> No Tiene los Permisos para Modificar este Documento</div>";
				}
			}
		?>
		<div id="bgventanamodal2">
			<div id="ventanamodal2">
				<IFRAME  src="" name="buster" marginWidth=0 marginHeight=0 frameBorder=0 id="ventana2" frameSpacing=0 style="left:500px; width:900px; height:500px; top:200;">
				</IFRAME>
			</div>
		</div>
	</form>
</body>
</html>
