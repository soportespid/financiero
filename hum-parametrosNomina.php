<?php

    header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=utf8");
	require 'comun.inc';
	require 'funciones.inc';
	session_start();
    if(empty($_SESSION)){
        header("location: index.php");
    }
	$linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");
	cargarcodigopag($_GET['codpag'],$_SESSION["nivel"]);
	date_default_timezone_set("America/Bogota");
?>
<!DOCTYPE >
<html lang=es>
	<head>
    <meta charset="utf-8"/>
		<meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1"/>
		<meta name="viewport" content="user-scalable=no">
		<title>:: IDEAL 10 - Parametrización</title>
		<link href="favicon.ico" rel="shortcut icon"/>
		<link href="css/css2z.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
		<link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
		<link href="css/css4.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
        <link href="css/tabs.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css"/>
        <link href="css/style.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css"/>
        <link rel="stylesheet" href="sweetalert2/dist/sweetalert2.min.css">
        <script type="text/javascript" src="css/programas.js"></script>
        <script src="sweetalert2/dist/sweetalert2.min.js"></script>
		<script src="vue/vue.min.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script src="css/programas.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script src="css/calendario.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
	</head>
	<body>
		<header>
			<table>
				<tr><script>barra_imagenes("para");</script><?php cuadro_titulos();?></tr>
			</table>
		</header>

		<form name="form2" method="post" action="">
			<section id="myapp" v-cloak >
                <div class="loading-container" v-show="isLoading" >
                    <p class="text-loading"data-loading="IDEAL 10">IDEAL 10</p>
                </div>
				<nav>
					<table>
						<tr><?php menu_desplegable("para");?></tr>
                        <tr>
							<td colspan="3" class="cinta">
                                <img src="imagenes/add2.png" class="mgbt" title="Nuevo">
								<img src="imagenes/guarda.png" @click="save" title="Guardar"  class="mgbt">
								<img src="imagenes/buscad.png"   class="mgbt" title="Buscar">
								<img src="imagenes/nv.png" @click="mypop=window.open('para-principal.php','',''); mypop.focus();" class="mgbt" title="Nueva Ventana">
							</td>
						</tr>
					</table>
				</nav>
				<article>
                    <div class="bg-white">
                        <div>
                            <h2 class="titulos m-0">Parámetros nómina</h2>
                            <div class="d-flex">
                                <div class="form-control">
                                    <label class="form-label" for="">Aprueba nómina:</label>
                                    <div class="d-flex">
                                        <input type="text"  class="colordobleclik w-25" @dblclick="isModal=true" @change="search('documento')" v-model="objTercero.documento">
                                        <input type="text" v-model="objTercero.nombre" disabled readonly >
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div>
                            <h2 class="titulos m-0">Parámetros parafiscales y otros</h2>
                            <div class="d-flex">
                                <div class="form-control">
                                    <label class="form-label" for="labelSelectName2">Caja de Compensacion Familiar:</label>
                                    <select id="labelSelectName2" v-model="selectCaja">
                                        <option value="0" selected>Seleccione</option>
                                        <option v-for="(data,index) in arrParafiscales" :key="index" :value="data.codigo">{{data.nombre}}</option>
                                    </select>
                                </div>
                                <div class="form-control">
                                    <label class="form-label" for="labelSelectName2">ICBF:</label>
                                    <select id="labelSelectName2" v-model="selectIcbf">
                                        <option value="0" selected>Seleccione</option>
                                        <option v-for="(data,index) in arrParafiscales" :key="index" :value="data.codigo">{{data.nombre}}</option>
                                    </select>
                                </div>
                                <div class="form-control">
                                    <label class="form-label" for="labelSelectName2">SENA:</label>
                                    <select id="labelSelectName2" v-model="selectSena">
                                        <option value="0" selected>Seleccione</option>
                                        <option v-for="(data,index) in arrParafiscales" :key="index" :value="data.codigo">{{data.nombre}}</option>
                                    </select>
                                </div>
                            </div>
                            <div class="d-flex">
                                <div class="form-control">
                                    <label class="form-label" for="labelSelectName2">Institutos Tecnicos:</label>
                                    <select id="labelSelectName2" v-model="selectInstituto">
                                        <option value="0" selected>Seleccione</option>
                                        <option v-for="(data,index) in arrParafiscales" :key="index" :value="data.codigo">{{data.nombre}}</option>
                                    </select>
                                </div>
                                <div class="form-control">
                                    <label class="form-label" for="labelSelectName2">ESAP:</label>
                                    <select id="labelSelectName2" v-model="selectEsap">
                                        <option value="0" selected>Seleccione</option>
                                        <option v-for="(data,index) in arrParafiscales" :key="index" :value="data.codigo">{{data.nombre}}</option>
                                    </select>
                                </div>
                                <div class="form-control">
                                    <label class="form-label" for="labelSelectName2">ARL:</label>
                                    <select id="labelSelectName2" v-model="selectArl">
                                        <option value="0" selected>Seleccione</option>
                                        <option v-for="(data,index) in arrParafiscales" :key="index" :value="data.codigo">{{data.nombre}}</option>
                                    </select>
                                </div>
                            </div>
                            <div class="d-flex">
                                <div class="form-control">
                                    <label class="form-label" for="labelSelectName2">Salud Empleador:</label>
                                    <select id="labelSelectName2" v-model="selectSaludEmpleador">
                                        <option value="0" selected>Seleccione</option>
                                        <option v-for="(data,index) in arrParafiscales" :key="index" :value="data.codigo">{{data.nombre}}</option>
                                    </select>
                                </div>
                                <div class="form-control">
                                    <label class="form-label" for="labelSelectName2">Salud Empleado:</label>
                                    <select id="labelSelectName2" v-model="selectSaludEmpleado">
                                        <option value="0" selected>Seleccione</option>
                                        <option v-for="(data,index) in arrParafiscales" :key="index" :value="data.codigo">{{data.nombre}}</option>
                                    </select>
                                </div>
                                <div class="form-control">
                                    <label class="form-label" for="labelSelectName2">Pension Empleador:</label>
                                    <select id="labelSelectName2" v-model="selectPensionEmpleador">
                                        <option value="0" selected>Seleccione</option>
                                        <option v-for="(data,index) in arrParafiscales" :key="index" :value="data.codigo">{{data.nombre}}</option>
                                    </select>
                                </div>
                            </div>
                            <div class="d-flex">
                                <div class="form-control">
                                    <label class="form-label" for="labelSelectName2">Pension Empleado:</label>
                                    <select id="labelSelectName2" v-model="selectPensionEmpleado">
                                        <option value="0" selected>Seleccione</option>
                                        <option v-for="(data,index) in arrParafiscales" :key="index" :value="data.codigo">{{data.nombre}}</option>
                                    </select>
                                </div>
                                <div class="form-control">
                                    <label class="form-label" for="labelSelectName2">Provision Cesantias:</label>
                                    <select id="labelSelectName2" v-model="selectCesantias">
                                        <option value="0" selected>Seleccione</option>
                                        <option v-for="(data,index) in arrParafiscales" :key="index" :value="data.codigo">{{data.nombre}}</option>
                                    </select>
                                </div>
                                <div class="form-control">
                                    <label class="form-label" for="labelSelectName2">Intereses Cesantias:</label>
                                    <select id="labelSelectName2" v-model="selectInteresesCesantias">
                                        <option value="0" selected>Seleccione</option>
                                        <option v-for="(data,index) in arrParafiscales" :key="index" :value="data.codigo">{{data.nombre}}</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="d-flex">
                            <div class="w-50">
                                <h2 class="titulos m-0">Parámetros pago</h2>
                                <div class="d-flex">
                                    <div class="form-control">
                                        <label class="form-label" for="labelSelectName2">Sueldo Personal de Nomina:</label>
                                        <select id="labelSelectName2" v-model="selectSueldo">
                                            <option value="0" selected>Seleccione</option>
                                            <option v-for="(data,index) in arrPago" :key="index" :value="data.codigo">{{data.nombre}}</option>
                                        </select>
                                    </div>
                                    <div class="form-control">
                                        <label class="form-label" for="labelSelectName2">Subsidio Alimentacion:</label>
                                        <select id="labelSelectName2" v-model="selectAlimentacion">
                                            <option value="0" selected>Seleccione</option>
                                            <option v-for="(data,index) in arrPago" :key="index" :value="data.codigo">{{data.nombre}}</option>
                                        </select>
                                    </div>
                                    <div class="form-control">
                                        <label class="form-label" for="labelSelectName2">Auxilio Transporte:</label>
                                        <select id="labelSelectName2" v-model="selectTransporte">
                                            <option value="0" selected>Seleccione</option>
                                            <option v-for="(data,index) in arrPago" :key="index" :value="data.codigo">{{data.nombre}}</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="w-50">
                                <h2 class="titulos m-0">Otros parámetros</h2>
                                <div class="d-flex">
                                    <div class="form-control">
                                        <label class="form-label" for="labelSelectName2">Horas mensuales:</label>
                                        <select id="labelSelectName2" v-model="selectHoras">
                                            <option value="0" selected>Seleccione</option>
                                            <option v-for="(data,index) in arrHoras" :key="index" :value="data.codigo">{{data.valor}}</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
				</article>
                <!-- MODALES -->
                <div v-show="isModal" class="modal">
                    <div class="modal-dialog modal-lg" >
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title">Buscar terceros</h5>
                                <button type="button" @click="isModal=false;" class="btn btn-close"><div></div><div></div></button>
                            </div>
                            <div class="modal-body">
                                <div class="d-flex flex-column">
                                    <div class="form-control m-0 mb-3">
                                        <input type="search" placeholder="Buscar" v-model="txtBuscar" @keyup="search()" id="labelInputName">
                                    </div>
                                    <div class="form-control m-0 mb-3">
                                        <label class="form-label" for="labelInputName">Resultados: <span class="fw-bold">{{txtResultados}}</span></label>
                                    </div>
                                </div>
                                <div class="overflow-auto max-vh-50 overflow-x-hidden " >
                                    <table class="table table-hover fw-normal">
                                        <thead>
                                            <tr>
                                                <th>Item</th>
                                                <th>Nombre</th>
                                                <th>Documento</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr v-for="(data,index) in arrTercerosCopy" @dblclick="selectItem(data)" :key="index">
                                                <td>{{index+1}}</td>
                                                <td>{{data.nombre}}</td>
                                                <td>{{data.documento}}</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
			</section>
		</form>

		<script src="node_modules/read-excel-file/bundle/read-excel-file.min.js"></script>
		<script src="node_modules/xlsx/dist/xlsx.full.min.js"></script>
        <script src="Librerias/vue/vue.min.js"></script>
		<script src="Librerias/vue/axios.min.js"></script>
		<script type="module" src="gestion_humana/parametros_nomina_config/crear/hum-parametrosNominaConfig.js?<?= date('d_m_Y_h_i_s');?>"></script>

	</body>
</html>
