<?php

    header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=utf8");
	require 'comun.inc';
	require 'funciones.inc';
	session_start();
    if(empty($_SESSION)){
        header("location: index.php");
    }
	$linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");
	cargarcodigopag($_GET['codpag'],$_SESSION["nivel"]);
	date_default_timezone_set("America/Bogota");
?>
<!DOCTYPE >
<html lang=es>
	<head>
    <meta charset="utf-8"/>
		<meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1"/>
		<meta name="viewport" content="user-scalable=no">
		<title>:: IDEAL 10 - Tesorería</title>
		<link href="favicon.ico" rel="shortcut icon"/>
		<link href="css/css2z.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
		<link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
		<link href="css/css4.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
        <link href="css/tabs.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css"/>
        <link href="css/style.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css"/>
        <link rel="stylesheet" href="sweetalert2/dist/sweetalert2.min.css">
        <script type="text/javascript" src="css/programas.js"></script>
        <script src="sweetalert2/dist/sweetalert2.min.js"></script>
		<script src="vue/vue.min.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script src="css/programas.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script src="css/calendario.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
	</head>
	<body>
		<header>
			<table>
				<tr><script>barra_imagenes("teso");</script><?php cuadro_titulos();?></tr>
			</table>
		</header>
        <main id="myapp" v-cloak >
            <div class="loading-container" v-show="isLoading" >
                <p class="text-loading"data-loading="IDEAL 10">IDEAL 10</p>
            </div>
            <nav>
                <table>
                    <tr><?php menu_desplegable("teso");?></tr>
                </table>
                <div class="bg-white group-btn p-1">
                    <button type="button" @click="save()" class="btn btn-white btn-primary-hover d-flex justify-between align-items-center">
                        <span>Guardar</span>
                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 -960 960 960">
                            <path d="M840-680v480q0 33-23.5 56.5T760-120H200q-33 0-56.5-23.5T120-200v-560q0-33 23.5-56.5T200-840h480l160 160Zm-80 34L646-760H200v560h560v-446ZM480-240q50 0 85-35t35-85q0-50-35-85t-85-35q-50 0-85 35t-35 85q0 50 35 85t85 35ZM240-560h360v-160H240v160Zm-40-86v446-560 114Z">
                            </path>
                        </svg>
                    </button>
                    <button type="button" onclick="mypop=window.open('teso-principal.php','','');mypop.focus();" class="btn btn-white btn-primary-hover d-flex justify-between align-items-center">
                        <span>Nueva ventana</span>
                        <svg class="fill-black group-hover:fill-white w-5 h-5" xmlns="http://www.w3.org/2000/svg" viewBox="0 -960 960 960"><path d="M320-120v-80h80v-80H160q-33 0-56.5-23.5T80-360v-400q0-33 23.5-56.5T160-840h640q33 0 56.5 23.5T880-760v400q0 33-23.5 56.5T800-280H560v80h80v80H320ZM160-360h640v-400H160v400Zm0 0v-400 400Z"></path></svg>
                    </button>
                    <button type="button" class="btn btn-white btn-primary-hover d-flex justify-between align-items-center" @click="mypop=window.open('teso-predial-autorizado','','');mypop.focus();">
                        <span class="group-hover:text-white">Duplicar pantalla</span>
                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 -960 960 960"><path d="M640-160v-360H160v360h480Zm80-200v-80h80v-360H320v200h-80v-200q0-33 23.5-56.5T320-880h480q33 0 56.5 23.5T880-800v360q0 33-23.5 56.5T800-360h-80ZM160-80q-33 0-56.5-23.5T80-160v-360q0-33 23.5-56.5T160-600h480q33 0 56.5 23.5T720-520v360q0 33-23.5 56.5T640-80H160Zm400-603ZM400-340Z"></path></svg>
                    </button>
                </div>
            </nav>
            <section>
                 <div  class="bg-white">
                    <div>
                        <h2 class="titulos m-0">Liquidar predial autorizado</h2>
                        <div class="d-flex w-100">
                            <div class="form-control w-25">
                                <label class="form-label m-0" for="">Digite el estado de cuenta a liquidar:</label>
                                <input type="number" v-model="txtCodigo" @keyup="getData()">
                            </div>
                            <div class="form-control">
                                <label class="form-label m-0" for="">Nro de liquidacion generado</label>
                                <input type="text" :value="txtCodigoFactura" disabled>
                            </div>
                        </div>
                        <h2 class="titulos m-0">Información predial</h2>
                        <div class="d-flex">
                            <div class="form-control d-flex flex-row">
                                <label class="form-label fw-bold">Código catastral:</label>
                                <p class="m-0">{{objEstado.codigo_catastro ? objEstado.codigo_catastro : ""}}</p>
                            </div>
                            <div class="form-control">
                                <label class="form-label fw-bold">Nro Documento:</label>
                                <p class="m-0">{{objEstado.codigo_catastro ? objEstado.documento :""}}</p>
                            </div>
                            <div class="form-control">
                                <label class="form-label fw-bold">Nombre propietario:</label>
                                <p class="m-0">{{objEstado.codigo_catastro ? objEstado.nombre_propietario :""}}</p>
                            </div>
                            <div class="form-control">
                                <label class="form-label fw-bold">Área del terreno:</label>
                                <p class="m-0">{{objEstado.codigo_catastro ? objEstado.hectareas+" ha "+objEstado.metros_cuadrados+" m²" : ""}}</p>
                            </div>
                            <div class="form-control">
                                <label class="form-label fw-bold">Área construida:</label>
                                <p class="m-0">{{objEstado.codigo_catastro ? objEstado.area_construida+" m²" : ""}}</p>
                            </div>
                            <div class="form-control">
                                <label class="form-label fw-bold">Dirección:</label>
                                <p class="m-0">{{objEstado.codigo_catastro ? objEstado.direccion : ""}}</p>
                            </div>
                        </div>
                        <div class="d-flex">
                            <div class="form-control">
                                <label class="form-label fw-bold">Total:</label>
                                <p class="m-0">{{objEstado.codigo_catastro ? objEstado.total :""}}</p>
                            </div>
                            <div class="form-control">
                                <label class="form-label fw-bold">Orden:</label>
                                <p class="m-0">{{objEstado.codigo_catastro ? objEstado.orden : ""}}</p>
                            </div>
                            <div class="form-control">
                                <label class="form-label fw-bold">Vigencia:</label>
                                <p class="m-0">{{objEstado.codigo_catastro ? arrVigencias[0].vigencia : ""}}</p>
                            </div>
                            <div class="form-control">
                                <label class="form-label fw-bold">Avaluo vigente:</label>
                                <p class="m-0">{{objEstado.codigo_catastro ? formatNum(objEstado.valor_avaluo) : ""}}</p>
                            </div>
                            <div class="form-control">
                                <label class="form-label fw-bold">Tipo predio:</label>
                                <p class="m-0">{{objEstado.codigo_catastro ? objEstado.predio_tipo+" ( Codigo: "+objEstado.predio_tipo_codigo+")" : ""}}</p>
                            </div>
                            <div class="form-control">
                                <label class="form-label fw-bold">Destino económico:</label>
                                <p class="m-0">{{objEstado.codigo_catastro ? objEstado.destino_economico+" ( Codigo: "+objEstado.codigo_destino_economico+")" : ""}}</p>
                            </div>
                        </div>
                        <div v-show="objEstado.codigo_catastro">
                            <h2 class="titulos m-0">Periodos a liquidar:  </h2>
                            <div class="table-responsive" >
                                <table  class="table fw-normal">
                                    <thead>
                                        <tr>
                                            <th class="d-flex justify-center">
                                                <div class="d-flex align-items-center ">
                                                    <label for="labelCheckAll" class="form-switch">
                                                        <input type="checkbox" id="labelCheckAll" v-model="isCheckAll" checked @change="changeAll()">
                                                        <span></span>
                                                    </label>
                                                </div>
                                            </th>
                                            <th>Vigencia</th>
                                            <th>Avaluo</th>
                                            <th>Tasa x mil</th>
                                            <th>Valor predial</th>
                                            <th>Descuento incentivo</th>
                                            <th>Recaudo predial</th>
                                            <th>Intereses predial</th>
                                            <th>Descuento intereses predial</th>
                                            <th>Bomberil</th>
                                            <th>Intereses bomberil</th>
                                            <th>Ambiental</th>
                                            <th>Intereses ambiental</th>
                                            <th>Alumbrado</th>
                                            <th>Liquidación</th>
                                            <th>Dias de mora</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr v-for="(data,index) in arrVigencias" :key="index">
                                            <td class="d-flex justify-center">
                                                <div class="d-flex align-items-center ">
                                                    <label :for="'labelCheckName'+index" class="form-switch">
                                                        <input type="checkbox" :id="'labelCheckName'+index" @change="changeStatus(index)" :checked="data.selected">
                                                        <span></span>
                                                    </label>
                                                </div>
                                            </td>
                                            <td>{{data.vigencia}}</td>
                                            <td>{{formatNum(data.valor_avaluo)}}</td>
                                            <td>{{data.tasa_por_mil}}</td>
                                            <td>{{formatNum(data.predial+data.predial_descuento)}}</td>
                                            <td>{{formatNum(data.predial_descuento)}}</td>
                                            <td>{{formatNum(data.predial)}}</td>
                                            <td>{{formatNum(data.predial_intereses)}}</td>
                                            <td>{{formatNum(data.predial_descuento_intereses)}}</td>
                                            <td>{{formatNum(data.bomberil)}}</td>
                                            <td>{{formatNum(data.bomberil_intereses)}}</td>
                                            <td>{{formatNum(data.ambiental)}}</td>
                                            <td>{{formatNum(data.ambiental_intereses)}}</td>
                                            <td>{{formatNum(data.alumbrado)}}</td>
                                            <td>{{formatNum(data.total_liquidacion)}}</td>
                                            <td>{{data.dias_mora}}</td>
                                        </tr>
                                        <tr class="bg-white fw-bold">
                                            <td colspan="2">Totales</td>
                                            <td>{{formatNum(objTotales.total_avaluo)}}</td>
                                            <td></td>
                                            <td>{{formatNum(objTotales.total_predial)}}</td>
                                            <td>{{formatNum(objTotales.total_incentivo)}}</td>
                                            <td>{{formatNum(objTotales.total_recaudo)}}</td>
                                            <td>{{formatNum(objTotales.total_predial_intereses)}}</td>
                                            <td>{{formatNum(objTotales.total_predial_descuento_intereses)}}</td>
                                            <td>{{formatNum(objTotales.total_bomberil)}}</td>
                                            <td>{{formatNum(objTotales.total_intereses_bomberil)}}</td>
                                            <td>{{formatNum(objTotales.total_ambiental)}}</td>
                                            <td>{{formatNum(objTotales.total_intereses_ambiental)}}</td>
                                            <td>{{formatNum(objTotales.total_alumbrado)}}</td>
                                            <td>{{formatNum(objTotales.total_liquidacion)}}</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </main>

		<script src="node_modules/read-excel-file/bundle/read-excel-file.min.js"></script>
		<script src="node_modules/xlsx/dist/xlsx.full.min.js"></script>
        <script src="Librerias/vue/vue.min.js"></script>
		<script src="Librerias/vue/axios.min.js"></script>
		<script type="module" src="tesoreria/predial_otros/js/functions_predial_autorizado.js?v=<?= date('d_m_Y_h_i_s');?>"></script>

	</body>
</html>
