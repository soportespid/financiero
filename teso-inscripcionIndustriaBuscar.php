<?php

header("Cache-control: no-cache, no-store, must-revalidate");
header("Content-Type: text/html;charset=utf8");
require 'comun.inc';
require 'funciones.inc';
session_start();
if (empty($_SESSION)) {
    header("location: index.php");
}
$linkbd = conectar_v7();
$linkbd->set_charset("utf8");
cargarcodigopag($_GET['codpag'], $_SESSION["nivel"]);
date_default_timezone_set("America/Bogota");
?>
<!DOCTYPE>
<html lang=es>

<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1" />
    <meta name="viewport" content="user-scalable=no">
    <title>:: IDEAL 10 - Tesorería</title>
    <link href="favicon.ico" rel="shortcut icon" />
    <link href="css/css2z.css?<?php echo date('d_m_Y_h_i_s'); ?>" rel="stylesheet" />
    <link href="css/css3.css?<?php echo date('d_m_Y_h_i_s'); ?>" rel="stylesheet" />
    <link href="css/css4.css?<?php echo date('d_m_Y_h_i_s'); ?>" rel="stylesheet" />
    <link href="css/tabs.css?<?php echo date('d_m_Y_h_i_s'); ?>" rel="stylesheet" type="text/css" />
    <link href="css/style.css?<?php echo date('d_m_Y_h_i_s'); ?>" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="sweetalert2/dist/sweetalert2.min.css">
    <script type="text/javascript" src="css/programas.js"></script>
    <script src="sweetalert2/dist/sweetalert2.min.js"></script>
    <script src="vue/vue.min.js?<?php echo date('d_m_Y_h_i_s'); ?>"></script>
    <script src="css/programas.js?<?php echo date('d_m_Y_h_i_s'); ?>"></script>
    <script src="css/calendario.js?<?php echo date('d_m_Y_h_i_s'); ?>"></script>
</head>

<body>
    <header>
        <table>
            <tr>
                <script>barra_imagenes("teso");</script><?php cuadro_titulos(); ?>
            </tr>
        </table>
    </header>

    <form name="form2" method="post" action="">
        <section id="myapp" v-cloak>
            <div id="cargando" v-show="isLoading" class="loading" style="z-index: 10000;">
                <span>Cargando...</span>
            </div>
            <nav>
                <table>
                    <tr><?php menu_desplegable("teso"); ?></tr>
                </table>
                <div class="bg-white group-btn p-1">
                    <button type="button" onclick="window.location.href='teso-inscripcionIndustriaCrear.php'"
                        class="btn btn-white btn-primary-hover d-flex justify-between align-items-center">
                        <span>Nuevo</span>
                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 -960 960 960">
                            <path d="M440-440H200v-80h240v-240h80v240h240v80H520v240h-80v-240Z"></path>
                        </svg>
                    </button>
                    <button type="button" class="btn btn-white btn-primary-hover d-flex justify-between align-items-center">
                        <span>Guardar</span>
                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 -960 960 960">
                            <path
                                d="M840-680v480q0 33-23.5 56.5T760-120H200q-33 0-56.5-23.5T120-200v-560q0-33 23.5-56.5T200-840h480l160 160Zm-80 34L646-760H200v560h560v-446ZM480-240q50 0 85-35t35-85q0-50-35-85t-85-35q-50 0-85 35t-35 85q0 50 35 85t85 35ZM240-560h360v-160H240v160Zm-40-86v446-560 114Z">
                            </path>
                        </svg>
                    </button>
                    <button type="button" @click="window.location.href='teso-inscripcionIndustriaBuscar.php'"
                        class="btn btn-white btn-primary-hover d-flex justify-between align-items-center">
                        <span>Buscar</span>
                        <svg class="fill-black group-hover:fill-white w-5 h-5" xmlns="http://www.w3.org/2000/svg"
                            viewBox="0 -960 960 960">
                            <path
                                d="M784-120 532-372q-30 24-69 38t-83 14q-109 0-184.5-75.5T120-580q0-109 75.5-184.5T380-840q109 0 184.5 75.5T640-580q0 44-14 83t-38 69l252 252-56 56ZM380-400q75 0 127.5-52.5T560-580q0-75-52.5-127.5T380-760q-75 0-127.5 52.5T200-580q0 75 52.5 127.5T380-400Z">
                            </path>
                        </svg>
                    </button>
                    <button type="button" onclick="mypop=window.open('teso-principal.php','','');mypop.focus();"
                        class="btn btn-white btn-primary-hover d-flex justify-between align-items-center">
                        <span>Nueva ventana</span>
                        <svg class="fill-black group-hover:fill-white w-5 h-5" xmlns="http://www.w3.org/2000/svg"
                            viewBox="0 -960 960 960">
                            <path
                                d="M320-120v-80h80v-80H160q-33 0-56.5-23.5T80-360v-400q0-33 23.5-56.5T160-840h640q33 0 56.5 23.5T880-760v400q0 33-23.5 56.5T800-280H560v80h80v80H320ZM160-360h640v-400H160v400Zm0 0v-400 400Z">
                            </path>
                        </svg>
                    </button>
                    <button type="button" @click="printExcel()"
                        class="btn btn-success btn-success-hover d-flex justify-between align-items-center">
                        <span>Exportar Excel</span>
                        <svg class="fill-black group-hover:fill-white w-4 h-4" xmlns="http://www.w3.org/2000/svg"
                            viewBox="0 0 384 512"><!--!Font Awesome Free 6.5.1 by @fontawesome - https://fontawesome.com License - https://fontawesome.com/license/free Copyright 2024 Fonticons, Inc. -->
                            <path
                                d="M48 448V64c0-8.8 7.2-16 16-16H224v80c0 17.7 14.3 32 32 32h80V448c0 8.8-7.2 16-16 16H64c-8.8 0-16-7.2-16-16zM64 0C28.7 0 0 28.7 0 64V448c0 35.3 28.7 64 64 64H320c35.3 0 64-28.7 64-64V154.5c0-17-6.7-33.3-18.7-45.3L274.7 18.7C262.7 6.7 246.5 0 229.5 0H64zm90.9 233.3c-8.1-10.5-23.2-12.3-33.7-4.2s-12.3 23.2-4.2 33.7L161.6 320l-44.5 57.3c-8.1 10.5-6.3 25.5 4.2 33.7s25.5 6.3 33.7-4.2L192 359.1l37.1 47.6c8.1 10.5 23.2 12.3 33.7 4.2s12.3-23.2 4.2-33.7L222.4 320l44.5-57.3c8.1-10.5 6.3-25.5-4.2-33.7s-25.5-6.3-33.7 4.2L192 280.9l-37.1-47.6z">
                            </path>
                        </svg>
                    </button>
                </div>
            </nav>
            <article>
                <div class="inicio">
                    <div>
                        <h2 class="titulos m-0">.: Buscar industria y comercio</h2>
                        <div class="form-control w-50">
                            <div class="d-flex">
                                <input type="search" placeholder="Buscar por consecutivo o documento" @keyup="search()"
                                    v-model="txtSearch">
                            </div>
                        </div>
                        <h2 class="titulos m-0">.: Resultados: {{ txtResults}} </h2>
                    </div>
                    <div class="overflow-auto max-vh-50 overflow-x-hidden p-2">
                        <table class="table table-hover fw-normal">
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Documento</th>
                                    <th>Nombre</th>
                                    <th>Fecha registro</th>
                                    <th class="text-center">Estado</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr @dblclick="editItem(data.id)" v-for="(data,index) in arrData" :key="data.id">
                                    <td>{{ data.id }}</td>
                                    <td>{{ data.cedulanit}}</td>
                                    <td>{{ data.nombre}}</td>
                                    <td>{{ data.fecha }}</td>
                                    <td class="text-center">
                                        <span
                                            :class="[data.estado =='S' || data.estado =='A'? 'badge-success' : 'badge-danger']"
                                            class="badge">{{ data.estado == "S" || data.estado == "A" ? "Matriculado" :
                                            "Matricula cancelada"}}</span>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div v-if="arrData !=''" class="inicio">
                    <p style="text-align:center">Página {{ intPage }} de {{ intTotalPages }}</p>
                    <ul style="list-style:none; padding:0;display:flex;justify-content: center;align-items:center">
                        <li v-show="intPage > 1" @click="search(intPage = 1)"
                            style="cursor:pointer;padding:4px 10px;background:#fff; border:1px solid #39c">
                            << </li>
                        <li v-show="intPage > 1" @click="search(--intPage)"
                            style="cursor:pointer;padding:4px 10px;background:#fff; border:1px solid #39c">
                            << /li>
                        <li v-show="intPage < intTotalPages" @click="search(++intPage)"
                            style="cursor:pointer;padding:4px 10px;background:#fff; border:1px solid #39c">></li>
                        <li v-show="intPage < intTotalPages" @click="search(intPage = intTotalPages)"
                            style="cursor:pointer;padding:4px 10px;background:#fff; border:1px solid #39c">>></li>
                    </ul>
                </div>
            </article>
        </section>
    </form>

    <script src="node_modules/read-excel-file/bundle/read-excel-file.min.js"></script>
    <script src="node_modules/xlsx/dist/xlsx.full.min.js"></script>
    <script src="Librerias/vue/vue.min.js"></script>
    <script src="Librerias/vue/axios.min.js"></script>
    <script type="module"
        src="tesoreria/inscripcion_industria/buscar/teso-inscripcionBuscar.js?<?= date('d_m_Y_h_i_s'); ?>"></script>

</body>

</html>
