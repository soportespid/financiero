<?php
	header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=utf8");
	require "comun.inc";
	require "funciones.inc";
	session_start();
	cargarcodigopag($_GET['codpag'],$_SESSION["nivel"]);
	date_default_timezone_set("America/Bogota");
?>
<!DOCTYPE >
<html lang="es">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1">
		<title>:: IDEAL 10 - Informes</title>
		<link href="css/css2.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
        <link href="css/css4.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css">
		<script type="text/javascript" src="css/programas.js"></script>
        <link href="css/style.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css"/>
		<?php titlepag();?>
	</head>
	<body>
		<IFRAME src="alertas.php" name="alertas" id="alertas" style="display:none"></IFRAME>
		<span id="todastablas2"></span>
		<table>
			<tr><script>barra_imagenes("info");</script><?php cuadro_titulos();?></tr>
			<tr><?php menu_desplegable("info");?></tr>
			<tr>
				<td colspan="3" class="cinta">
					<img src="imagenes/add2.png" class="mgbt1">
					<img src="imagenes/guardad.png" class="mgbt1">
					<img src="imagenes/buscad.png" class="mgbt1">
					<a class="tooltip bottom mgbt" onClick="mypop=window.open('plan-agenda.php','','');mypop.focus()" ><img src="imagenes/agenda1.png"><span class="tiptext">Agenda</span></a>
					<a class="tooltip bottom mgbt" onClick="mypop=window.open('info-principal.php','','');mypop.focus()" ><img src="imagenes/nv.png" ><span class="tiptext">Nueva Ventana</span></a>
				</td>
			</tr>
		</table>
		<form name="form2" method="post" action="">
			<table class="inicio">
				<tr>
					<td class="titulos" colspan="2">.: Men&uacute; CUIPO</td>
					<td class="cerrar" style="width:7%" onClick="location.href='info-principal.php'">Cerrar</td>
				</tr>
				<tr>
					<td class='saludo1'>
						<ol id="lista2">
							<li class='mgbt3' onClick="location.href='ccp-parametrizaringresoscuipo.php'">Parametrizar Ingresos</li>
                            <li class='mgbt3' onClick="location.href='ccp-homologardetallesectorialprogastos.php'">Detalle sectorial Programaci&oacute;n gastos</li>
							<li class='mgbt3' onClick="location.href='ccp-homologardetallesectorial.php'">Detalle sectorial Ejecuci&oacute;n gastos</li>
							<li class='mgbt3' onClick="location.href='ccp-homologarcuentasingsgr.php'">Parametrizar, Programaci&oacute;n de ingresos SGR</li>
							<li class='mgbt3' onClick="location.href='ccp-homologarcuentasingejecusgr.php'">Parametrizar, Ejecuci&oacute;n de ingresos SGR</li>
							<li class='mgbt3' onClick="location.href='ccp-homologarcuentassgr.php'">Parametrizar gastos SGR</li>
							<li class='mgbt3' onClick="location.href='ccp-reportescuipo2022.php'">Generar Reporte</li>
						</ol>
					</td>
				</tr>
			</table>
		</form>
	</body>
</html>
