<?php //V 1000 12/12/16 ?> 
<?php 
	require "comun.inc";
	require "funciones.inc";
	session_start();
	$linkbd = conectar_bd();	
	$linkbd_v7 = conectar_v7();	
	cargarcodigopag($_GET[codpag],$_SESSION["nivel"]);
	header("Cache-control: private"); // Arregla IE 6
	date_default_timezone_set("America/Bogota");
	$maxVersion = ultimaVersionGastosCCPET();
?>
<!DOCTYPE >
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="es">
	<head>
	 	<meta http-equiv="Content-Type" content="text/html" charset="iso-8859-1"/>
        <meta http-equiv="X-UA-Compatible" content="IE=9"/>
		<title>:: Spid - Presupuesto</title>
        <link href="css/css2.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="css/programas.js"></script>
		<script>
			var anterior;
			function ponprefijo(pref,opc)
			{ 
				parent.document.form2.cuenta.value =pref  ;
				parent.document.form2.ncuenta.value =opc ;
				parent.document.form2.cuenta.focus();
				parent.document.form2.valorvl.value='';
				parent.document.form2.valorvl.focus();
				parent.despliegamodal2("hidden");
			} 
		</script> 
		<?php titlepag();?>
	</head>
	<body>
    	<form action="" method="post" enctype="multipart/form-data" name="form2">
			<?php 
				if($_POST['oculto']=="")
				{
					$_POST['numpos']=0;
					$_POST['numres']=10;
					$_POST['nummul']=0;
					$_POST['tipo']=$_GET['ti'];
					$_POST['tipoGasto'] = $_GET['tipoGasto'];
					$_POST['proyecto'] = $_GET['proyecto'];
					$_POST['bpim'] = $_GET['bpim'];
				}
				?>
  			<table class="inicio" style="width:99.4%;">
    			<tr>
      				<td height="25" colspan="3" class="titulos" >Buscar CUENTAS PRESUPUESTALES</td>
                    <td class="cerrar"><a onClick="parent.despliegamodal2('hidden');">&nbsp;Cerrar</a></td>
    			</tr>
				<tr><td colspan="4" class="titulos2" >:&middot; Por Descripcion </td></tr>
				<tr>
					<td class="saludo1" style='width:3cm;'>:&middot; Numero Cuenta:</td>
				  	<td  colspan="3">
                    	<input type="search" name="numero" id="numero" value="<?php echo $_POST['numero'];?>" style='width:50%;'/>
				  		<input type="hidden" name="tipo" id="tipo" value="<?php echo $_POST['tipo']?>"/>
				  		<input type="hidden" name="tipoGasto" id="tipoGasto" value="<?php echo $_POST['tipoGasto']?>"/>
				  		<input type="hidden" name="proyecto" id="proyecto" value="<?php echo $_POST['proyecto']?>"/>
				  		<input type="hidden" name="bpim" id="bpim" value="<?php echo $_POST['bpim']?>"/>
			      		
                        <input type="button" name="bboton" onClick="limbusquedas();" value="&nbsp;&nbsp;Buscar&nbsp;&nbsp;" />
			      	</td>
			    </tr>
  			</table>
            <input type="hidden" name="oculto" id="oculto" value="1"/>
            <input type="hidden" name="numres" id="numres" value="<?php echo $_POST['numres'];?>"/>
    		<input type="hidden" name="numpos" id="numpos" value="<?php echo $_POST['numpos'];?>"/>
       		<input type="hidden" name="nummul" id="nummul" value="<?php echo $_POST['nummul'];?>"/>
			<div  class="subpantalla" style="height:80.5%; width:99.2%; overflow-x:hidden;">
				<?php
					$inicioCodigo = '';
					if($_POST['tipoGasto'] == '2')
					{
						$inicioCodigo = '2.1';
					}
					else if($_POST['tipoGasto'] == '3')
					{
						$inicioCodigo = '2.2';
					}
					else
					{
						$inicioCodigo = '2.3';
					}

					
					
					if($_POST['tipoGasto'] == '4')
					{
						$cond="AND (C.codigo LIKE '%$_POST[numero]%' OR C.nombre LIKE '%$_POST[numero]%')";

						$sqlrProy = "SELECT id FROM ccpproyectospresupuesto WHERE codigo = '$_POST[bpim]'";
						$respProy = mysqli_query($linkbd_v7, $sqlrProy);
						$rowProy = mysqli_fetch_row($respProy);

						if($rowProy [0] != '')
						{
							$sqlr = "SELECT DISTINCT C.codigo, C.nombre, C.tipo FROM cuentasccpet AS C, ccpproyectospresupuesto_presupuesto  AS CP WHERE  C.municipio = 1 AND CP.codproyecto = '$rowProy[0]' AND C.codigo = CP.rubro AND C.codigo like '$inicioCodigo%' AND C.version='$maxVersion' $cond";
						}

						$sqlrAd = "SELECT id FROM ccpetadicion_inversion WHERE codigo = '$_POST[bpim]'";
						$respAd = mysqli_query($linkbd_v7, $sqlrAd);
						$rowAd = mysqli_fetch_row($respAd);
						if($rowAd [0] != '')
						{
							$sqlr = "SELECT DISTINCT C.codigo, C.nombre, C.tipo FROM cuentasccpet AS C, ccpetadicion_inversion_detalles AS CPA WHERE  C.municipio = 1 AND  CPA.codproyecto = '$rowAd[0]' AND  C.codigo = CPA.rubro AND C.codigo like '$inicioCodigo%' AND C.version='$maxVersion' $cond";
						}
						
					}
					else
					{
						$cond="AND (codigo LIKE '%$_POST[numero]%' OR nombre LIKE '%$_POST[numero]%')";

						$sqlr="SELECT DISTINCT * FROM cuentasccpet WHERE  municipio = 1 AND codigo like '$inicioCodigo%' AND version='$maxVersion' $cond";
					}
                    
                    $resp = mysqli_query($linkbd_v7, $sqlr);	
                    
                    $_POST['numtop'] = mysqli_num_rows($resp);
                    
					$nuncilumnas=ceil($_POST['numtop']/$_POST['numres']);

					$cond2="";
                    if ($_POST['numres']!="-1"){$cond2="LIMIT $_POST[numpos], $_POST[numres]";}
                    
					if($_POST['tipoGasto'] == '4')
					{
						$sqlrProy = "SELECT id FROM ccpproyectospresupuesto WHERE codigo = '$_POST[bpim]'";
						$respProy = mysqli_query($linkbd_v7, $sqlrProy);
						$rowProy = mysqli_fetch_row($respProy);

						$sqlrAd = "SELECT id FROM ccpetadicion_inversion WHERE codigo = '$_POST[bpim]'";
						$respAd = mysqli_query($linkbd_v7, $sqlrAd);
						$rowAd = mysqli_fetch_row($respAd);

						$sqlrProy = "SELECT id FROM ccpproyectospresupuesto WHERE codigo = '$_POST[bpim]'";
						$respProy = mysqli_query($linkbd_v7, $sqlrProy);
						$rowProy = mysqli_fetch_row($respProy);

						if($rowProy [0] != '')
						{

							$sqlr="SELECT DISTINCT C.codigo, C.nombre, C.tipo FROM cuentasccpet AS C, ccpproyectospresupuesto_presupuesto  AS CP WHERE C.municipio = 1 AND CP.codproyecto = '$rowProy[0]'  AND C.codigo = CP.rubro AND C.codigo like '$inicioCodigo%'  AND C.version='$maxVersion' $cond ORDER BY codigo $cond2";
						}

						$sqlrAd = "SELECT id FROM ccpetadicion_inversion WHERE codigo = '$_POST[bpim]'";
						$respAd = mysqli_query($linkbd_v7, $sqlrAd);
						$rowAd = mysqli_fetch_row($respAd);
						if($rowAd [0] != '')
						{
							$sqlr = "SELECT DISTINCT C.codigo, C.nombre, C.tipo FROM cuentasccpet AS C, ccpetadicion_inversion_detalles AS CPA WHERE  C.municipio = 1 AND  CPA.codproyecto = '$rowAd[0]' AND  C.codigo = CPA.rubro AND C.codigo like '$inicioCodigo%' AND C.version='$maxVersion' $cond ORDER BY codigo $cond2";
						}
					}
					else
					{
						$sqlr="SELECT DISTINCT codigo, nombre, tipo FROM cuentasccpet WHERE municipio = 1 AND codigo like '$inicioCodigo%' AND version='$maxVersion' $cond ORDER BY codigo $cond2";
					}
					
					$resp = mysqli_query($linkbd_v7, $sqlr);	


					$co='saludo1a';
					$co2='saludo2';	
					$i=1;
					$numcontrol=$_POST['nummul']+1;
					if(($nuncilumnas==$numcontrol)||($_POST['numres']=="-1"))
					{
						$imagenforward="<img src='imagenes/forward02.png' style='width:17px'>";
						$imagensforward="<img src='imagenes/skip_forward02.png' style='width:16px' >";
					}
					else 
					{
						$imagenforward="<img src='imagenes/forward01.png' style='width:17px' title='Siguiente' onClick='numsiguiente()'>";
						$imagensforward="<img src='imagenes/skip_forward01.png' style='width:16px' title='Fin' onClick='saltocol(\"$nuncilumnas\")'>";
					}
					if(($_POST['numpos']==0)||($_POST['numres']=="-1"))
					{
						$imagenback="<img src='imagenes/back02.png' style='width:17px'>";
						$imagensback="<img src='imagenes/skip_back02.png' style='width:16px'>";
					}
					else
					{
						$imagenback="<img src='imagenes/back01.png' style='width:17px' title='Anterior' onClick='numanterior();'>";
						$imagensback="<img src='imagenes/skip_back01.png' style='width:16px' title='Inicio' onClick='saltocol(\"1\")'>";
					}
					echo"
					<table class='inicio'>
    					<tr>
      						<td colspan='4' class='titulos'>Resultados Busqueda</td>
							<td class='submenu'>
									<select name='renumres' id='renumres' onChange='cambionum();' style='width:100%'>
										<option value='10'"; if ($_POST['renumres']=='10'){echo 'selected';} echo ">10</option>
										<option value='20'"; if ($_POST['renumres']=='20'){echo 'selected';} echo ">20</option>
										<option value='30'"; if ($_POST['renumres']=='30'){echo 'selected';} echo ">30</option>
										<option value='50'"; if ($_POST['renumres']=='50'){echo 'selected';} echo ">50</option>
										<option value='100'"; if ($_POST['renumres']=='100'){echo 'selected';} echo ">100</option>
										<option value='-1'"; if ($_POST['renumres']=='-1'){echo 'selected';} echo ">Todos</option>
									</select>
								</td>
      						</tr>
							<tr><td colspan='5'>Cuentas Encontradas: $_POST[numtop]</td></tr>
    						<tr>
      							<td class='titulos2' style='width:8%;'>Item</td>
      							<td class='titulos2' style='width:20%;'>Cuenta</td>
      							<td class='titulos2'>Descripcion</td>	  
      							<td class='titulos2' style='width:15%;'>Tipo</td>
      							<td class='titulos2' style='width:8%;'>Estado</td>	  	  
    						</tr>";
					while ($r = mysqli_fetch_row($resp)) 
					{
						echo "
							<tr class='$co' onMouseOver=\"anterior=this.style.backgroundColor;this.style.backgroundColor='#40b3ff';\" onMouseOut=\"this.style.backgroundColor=anterior\" "; if ($r[2]=='C'){echo" onClick=\"javascript:ponprefijo('$r[0]','$r[1]')\"";} 
						echo">
								<td>$i</td>
								<td >$r[0]</td>
								<td>".ucwords(strtolower($r[1]))."</td>
								<td>$r[2]</td>
								<td> S </td></tr>";
       					$aux=$co;
        				$co=$co2;
        				$co2=$aux;
						$i=1+$i;
   					}
					if ($_POST['numtop']==0)
					{
						echo "
						<table class='inicio'>
							<tr>
								<td class='saludo1' style='text-align:center;width:100%'><img src='imagenes\alert.png' style='width:25px'>No hay coincidencias en la b&uacute;squeda $tibusqueda<img src='imagenes\alert.png' style='width:25px'></td>
							</tr>
						</table>";
					}
					$_POST['oculto']="";
					 echo"
					</table>
					<table class='inicio'>
						<tr>
							<td style='text-align:center;'>
								<a href='#'>$imagensback</a>&nbsp;
								<a href='#'>$imagenback</a>&nbsp;&nbsp;";
				if($nuncilumnas<=9){$numfin=$nuncilumnas;}
				else{$numfin=9;}
				for($xx = 1; $xx <= $numfin; $xx++)
				{
					if($numcontrol<=9){$numx=$xx;}
					else{$numx=$xx+($numcontrol-9);}
					if($numcontrol==$numx){echo"<a href='#' onClick='saltocol(\"$numx\")'; style='color:#24D915'> $numx </a>";}
					else {echo"<a href='#' onClick='saltocol(\"$numx\")'; style='color:#000000'> $numx </a>";}
				}
				echo"			&nbsp;&nbsp;<a href='#'>$imagenforward</a>
								&nbsp;<a href='#'>$imagensforward</a>
							</td>
						</tr>
					</table>";
				?>
			</div>
            <input type="hidden" name="numtop" id="numtop" value="<?php echo $_POST['numtop'];?>"/>
		</form>
	</body>
</html> 
