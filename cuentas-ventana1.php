<?php
	header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=utf8");
	require "comun.inc";
	require "funciones.inc";
	//require "encrip.inc";
	session_start();
	$linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");
	cargarcodigopag($_GET['codpag'],$_SESSION["nivel"]);
	date_default_timezone_set("America/Bogota");
?>
<!DOCTYPE >
<html lang="es">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1">
		<meta name="viewport" content="user-scalable=no">
		<title>:: SPID</title>
		<link href="favicon.ico" rel="shortcut icon"/>
		<link href="css/css2.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet"/>
		<link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet"/>
		<script type="text/javascript" src="css/calendario.js"></script>
		<script >
			function ponprefijo(pref,opc){   
				parent.document.form2.cuentact.value =pref;
				parent.document.form2.cuentact_.value =pref;
				parent.document.form2.ncuentact.value =opc;
				//opener.document.form2.nobjeto.value =opc ;
				//opener.document.form2.tercero.focus();	
				//opener.document.form2.cc.select();
				parent.despliegamodal2("hidden");
			} 
		</script> 
	</head>
	<body >
		<form action="" method="post" enctype="multipart/form-data" name="form1">
			<?php
				$_POST['tobjeto']=$_GET['objeto'];
				$_POST['tnobjeto']=$_GET['nobjeto'];
				$_POST['fecha']=$_GET['fecha'];
				$nfecha=cambiar_fecha($_POST['fecha']);
				$anio=date('Y',strtotime($nfecha));
			?>
			<table  class="inicio">
				<tr >
					<td height="25" colspan="4" class="titulos" >Buscar CUENTAS </td>
					<td class="cerrar"><a onClick="parent.despliegamodal2('hidden');">&nbsp;Cerrar</a></td>
				</tr>
				<tr>
					<td colspan="6" class="titulos2" >:&middot; Por Descripcion </td>
				</tr>
				<tr >
					<td class="saludo1" >:&middot; Numero Cuenta:</td>
				<td  colspan="3">
					<input name="numero" type="text" size="30" >
					<input name="oculto" type="hidden" id="oculto" value="1" >
					<input type="submit" name="Submit" value="Buscar" >
					<input name="oculto" type="hidden" value="1">
					<input name="tobjeto" type="hidden" value="<?php echo $_POST['tobjeto']?>">
					<input name="tnobjeto" type="hidden" value="<?php echo $_POST['tnobjeto']?>">
				</td>
				</tr>      
			</table>
			<div class="subpantalla" style="height:78.5%; width:99.6%; overflow-x:hidden;">
				<table class="inicio">
					<tr >
						<td height="25" colspan="5" class="titulos" >Resultados Busqueda </td>
					</tr>
					<tr>
						<td style='width:4%' class="titulos2" >Item</td>
						<td style='width:12%' class="titulos2" >Cuenta </td>
						<td class="titulos2" >Descripcion</td>
						<td style='width:16%' class="titulos2" >Tipo</td>
						<td style='width:6%' class="titulos2" >Estado<?php echo $anio; ?></td>
					</tr>
					<?php
						//$oculto=$_POST['oculto'];
						if($anio!=""){
							if ($anio>='2018'){
								$crit1=" ";
								$crit2=" ";
								$cond=" cuenta like'%".$_POST['numero']."%' or nombre like '%".strtoupper($_POST['numero'])."%'";
								$sqlr="SELECT distinct * from cuentasnicsp where".$cond." order by cuenta";
								//sacar el consecutivo 
								// echo "<div><div>sqlr:".$sqlr."</div></div>";
								$resp = mysqli_query($linkbd,$sqlr);
								$ntr = mysqli_num_rows($resp);
								$i=1;
								//echo "nr:".$nr;
								$co='saludo1a';
								$co2='saludo2';	
								while ($r =mysqli_fetch_row($resp)){			
									echo"
									<tr class='$co' style='text-transform:uppercase'";
									if ($r[5]=='Auxiliar'){echo "onClick=\"javascript:ponprefijo('$r[0]','$r[1]')\"";} 
									echo ">
										<td>$i</td>
										<td>$r[0]</td>
										<td>$r[1]</td>
										<td>$r[5]</td>
										<td style='text-align:center;'>$r[6]</td>
									</tr>";
									$aux=$co;
									$co=$co2;
									$co2=$aux;
									$i=1+$i;
								}
							}else{
								$crit1=" ";
								$crit2=" ";
								$cond=" cuenta like'%".$_POST['numero']."%' or nombre like '%".strtoupper($_POST['numero'])."%'";
								$sqlr="SELECT distinct * from cuentas where".$cond." order by cuenta";
								//sacar el consecutivo 
								// echo "<div><div>sqlr:".$sqlr."</div></div>";
								$resp = mysqli_query($linkbd,$sqlr);
								$ntr = mysqli_num_rows($resp);
								$i=1;
								//echo "nr:".$nr;
								$co='saludo1a';
								$co2='saludo2';	
								while ($r =mysqli_fetch_row($resp)){
									echo"
									<tr class='$co' style='text-transform:uppercase'";
									if ($r[5]=='Auxiliar'){echo "onClick=\"javascript:ponprefijo('$r[0]','$r[1]')\"";} 
									echo ">
										<td>$i</td>
										<td>$r[0]</td>
										<td>$r[1]</td>
										<td>$r[5]</td>
										<td style='text-align:center;'>$r[6]</td>
									</tr>";
									$aux=$co;
									$co=$co2;
									$co2=$aux;
									$i=1+$i;
								}
							}
						}else{
							echo'Por favor Seleccione la Fecha para visualizar el Plan de Cuentas';
						}
					?>
				</table>
			</div>
		</form>
	</body>
</html>