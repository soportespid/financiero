<?php
	ini_set('max_execution_time',3600);
	header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=utf8");

	require 'comun.inc';
	require 'funciones.inc';

	$linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");

	session_start();
    date_default_timezone_set("America/Bogota");
	if (!isset($_SESSION["usuario"]))
	{
		//*** verificar el username y el pass
		$users = $_POST['user'];
		$pass = $_POST['pass'];
		$sqlr = "Select usuarios.nom_usu,roles.nom_rol, usuarios.id_rol, usuarios.id_usu, usuarios.foto_usu, usuarios.usu_usu from usuarios, roles where usuarios.usu_usu='$users' and usuarios.pass_usu='$pass' and usuarios.id_rol=roles.id_rol and usuarios.est_usu='1'";
		$res = mysqli_query($linkbd,$sqlr);
		while($r = mysqli_fetch_row($res))
		{
			$user = $r[0];
			$perf = $r[1];
			$niv = $r[2];
			$idusu = $r[3];
			$nick = $r[5];
			$dirfoto = $r[4];
		}
		if ($user == "")
		{
			header("location: index2.php");
		}
		else
		{
			$_SESSION["usuario"] = array();
			$_SESSION["usuario"] = $user;
			$_SESSION["perfil"] = $perf;
			$_SESSION["idusuario"] = $idusu;
			$_SESSION["nickusu"] = $nick;
			$_SESSION["nivel"] = $niv;
			$_SESSION["linksetin"] = array();
			if($dirfoto == "")
			{
				$dirfoto="blanco.png";
			}
			$_SESSION["fotousuario"]="fotos/".$dirfoto;
			//******************* menuss ************************
			$sqlr="Select DISTINCT (opciones.nom_opcion),opciones.ruta_opcion, opciones.niv_opcion,opciones.comando  from rol_priv, opciones where rol_priv.id_rol=$niv and opciones.id_opcion=rol_priv.id_opcion group by (opciones.nom_opcion), opciones.ruta_opcion, opciones.niv_opcion,opciones.comando  order by opciones.orden";
			$linksetin[$x]="";
			$res = mysqli_query($linkbd,$sqlr);
			while($roww = mysqli_fetch_row($res))
			{
				$_SESSION['linksetin'][$roww[2]].='<li> <a onClick="location.href=\''.$roww[1].'\'" style="cursor:pointer;">'.$roww[0].' <span style="float:right">'.$roww[3].'</span></a></li>';
			}
		}
	}
	else
	{
		$_SESSION["linksetin"] = array();
		$niv = $_SESSION["nivel"];
		$sqlr = "Select DISTINCT (opciones.nom_opcion),opciones.ruta_opcion, opciones.niv_opcion,opciones.comando  from rol_priv, opciones where rol_priv.id_rol=$niv and opciones.id_opcion=rol_priv.id_opcion and opciones.modulo=5 group by (opciones.nom_opcion), opciones.ruta_opcion, opciones.niv_opcion ,opciones.comando order by opciones.orden";
		$linksetin[$x]="";
		$res = mysqli_query($linkbd,$sqlr);
		while($roww = mysqli_fetch_row($res))
		{
			$_SESSION['linksetin'][$roww[2]].='<li> <a onClick="location.href=\''.$roww[1].'\'" style="cursor:pointer;">'.$roww[0].' <span style="float:right">'.$roww[3].'</span></a></li>';
		}
	}
	//**niveles menu: Administracion (0) - Consultas (1) - Herramientas (2) - Reportes (3)
?>
<!DOCTYPE >
<html lang="es">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1">
		<title>:: IDEAL 10 - Almacen</title>
		<link href="favicon.ico" rel="shortcut icon"/>
		<link href="css/css2.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css">
		<link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css">
		<script type="text/javascript" src="css/programas.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
        <link href="css/style.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css"/>
	</head>
	<body>
		<IFRAME src="alertas.php" name="alertas" id="alertas" style="display:none"></IFRAME>
		<span id="todastablas2"></span>
		<table>
			<tr><script>barra_imagenes("inve");</script><?php cuadro_titulos();?></tr>
			<tr><?php menu_desplegable("inve");?></tr>
		</table>
	</body>
</html>
