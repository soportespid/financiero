<?php
    require_once 'PHPExcel/Classes/PHPExcel.php';
	require 'comun.inc';
	require "funciones.inc";
    require 'funcionesSP.inc.php';
    require 'conversor.php';
	date_default_timezone_set("America/Bogota");
	session_start();
    class Plantilla{
        private $linkbd;
        public function __construct() {
            $this->linkbd = conectar_v7();
            $this->linkbd->set_charset("utf8");
        }
        /******************Funciones para generar informe********************* */
        public function getInfo(){
            if(!empty($_SESSION)){
                $arrData = [];
                $strCuenta = $_GET['cuenta'] =="undefined" ? "" : $_GET['cuenta'];
                $strFuente = $_GET['fuente'];
                $strFechaInicial = $_GET['fecha_inicial'];
                $strFechaFinal = $_GET['fecha_final'];
                $intVigencia = $_GET['vigencia'];
                $strSeccion = $_GET['seccion'];
                $strPago = $_GET['pago'];
                $strBpim = $_GET['bpim'];
                $strProgramatico = $_GET['programatico'];
                $arrInicial = $this->selectInicial($strCuenta,$strSeccion,$intVigencia,$strPago,$strFuente,$strBpim,$strProgramatico,$strFechaInicial,$strFechaFinal);
                $arrAdicion = $this->selectAdicion($strCuenta,$strSeccion,$intVigencia,$strPago,$strFuente,$strBpim,$strProgramatico,$strFechaInicial,$strFechaFinal);
                $arrReduccion = $this->selectReduccion($strCuenta,$strSeccion,$intVigencia,$strPago,$strFuente,$strBpim,$strProgramatico,$strFechaInicial,$strFechaFinal);
                $arrCredito = $this->selectTraslados($strCuenta,$strSeccion,$intVigencia,$strPago,$strFuente,$strBpim,$strProgramatico,$strFechaInicial,$strFechaFinal,"C");
                $arrContraCredito = $this->selectTraslados($strCuenta,$strSeccion,$intVigencia,$strPago,$strFuente,$strBpim,$strProgramatico,$strFechaInicial,$strFechaFinal,"R");
                $arrCdp = $this->selectCdp($strCuenta,$strSeccion,$intVigencia,$strPago,$strFuente,$strBpim,$strProgramatico,$strFechaInicial,$strFechaFinal);
                $arrRp = $this->selectRp($strCuenta,$strSeccion,$intVigencia,$strPago,$strFuente,$strBpim,$strProgramatico,$strFechaInicial,$strFechaFinal);
                $arrCxp = $this->selectCxp($strCuenta,$strSeccion,$intVigencia,$strPago,$strFuente,$strBpim,$strProgramatico,$strFechaInicial,$strFechaFinal);
                $arrNomina = $this->selectNomina($strCuenta,$strSeccion,$intVigencia,$strPago,$strFuente,$strBpim,$strProgramatico,$strFechaInicial,$strFechaFinal);
                $arrEgresos = $this->selectEgresos($strCuenta,$strSeccion,$intVigencia,$strPago,$strFuente,$strBpim,$strProgramatico,$strFechaInicial,$strFechaFinal);
                $arrEgresosNomina = $this->selectEgresosNomina($strCuenta,$strSeccion,$intVigencia,$strPago,$strFuente,$strBpim,$strProgramatico,$strFechaInicial,$strFechaFinal);
                $definitivo = $arrInicial['total'] + $arrAdicion['total'] - $arrReduccion['total'] + $arrCredito['total'] - $arrContraCredito['total'];
                $totalObligaciones = $arrCxp['total'] + $arrNomina['total'];
                $totalEgresos = $arrEgresos['total'] + $arrEgresosNomina['total'];
                $totalSaldo = $definitivo - $arrCdp['total'];
                //Inicial
                array_push($arrData,$arrInicial['data']);
                //Adicion
                array_push($arrData,$arrAdicion['data']);
                //Reducción
                array_push($arrData,$arrReduccion['data']);
                //Crédito
                array_push($arrData,$arrCredito['data']);
                //Contra crédito
                array_push($arrData,$arrContraCredito['data']);
                //Definitivo
                array_push($arrData,[array("tipo"=>"T","comprobante"=>"Definitivo","valor"=>$definitivo)]);
                //Cdp
                array_push($arrData,$arrCdp['data']);
                //Rp
                array_push($arrData,$arrRp['data']);
                //CXP
                array_push($arrData,$arrCxp['data']);
                //Nomina
                array_push($arrData,$arrNomina['data']);
                //Total obligaciones
                array_push($arrData,[array("tipo"=>"T","comprobante"=>"Total obligaciones","valor"=>$totalObligaciones)]);
                //Egresos
                array_push($arrData,$arrEgresos['data']);
                array_push($arrData,$arrEgresosNomina['data']);
                //Total egresos
                array_push($arrData,[array("tipo"=>"T","comprobante"=>"Total egresos","valor"=>$totalEgresos)]);
                //Total saldo
                array_push($arrData,[array("tipo"=>"T","comprobante"=>"Total saldo","valor"=>$totalSaldo)]);
                $arrData = $this->fixArrayInfo($arrData);
                return $arrData;
            }
            die();
        }
        public function fixArrayInfo(array $arrInfo){
            $arrData = [];
            foreach ($arrInfo as $cab) {
                foreach ($cab as $det) {
                    array_push($arrData,$det);
                }
            }
            return $arrData;
        }
        public function selectEgresosNomina($cuenta,$secPresupuestal,$vigGasto,$medioPago,$fuente,$bpim,$programatico,$strFechaInicial,$strFechaFinal){
            $vigenciaIni = explode("-",$strFechaInicial)[0];
            $vigenciaFin = explode("-",$strFechaFinal)[0];
            $arrData = [];
            $vigGasto = $vigGasto != 0 ? " AND TD.vigencia_gasto LIKE '$vigGasto'" : "";
            $secPresupuestal = $secPresupuestal != 0 ? " AND TD.seccion_presupuestal LIKE '$secPresupuestal'" : "";
            $medioPago = !is_numeric($medioPago) ? " AND TD.medio_pago LIKE '$medioPago' " : "";

            $sql = "SELECT TE.id_egreso, TE.fecha, TD.valor,TE.concepto,TE.tercero
            FROM tesoegresosnomina_det AS TD, tesoegresosnomina AS TE
            WHERE (TE.vigencia = '$vigenciaIni' OR TE.vigencia = '$vigenciaFin') AND TE.fecha BETWEEN '$strFechaInicial' AND '$strFechaFinal'
            AND TD.id_egreso = TE.id_egreso AND TE.estado = 'S' AND NOT(TD.tipo='SE' OR TD.tipo='PE' OR TD.tipo='DS' OR TD.tipo='RE' OR TD.tipo='FS')
            AND TD.cuentap LIKE '%$cuenta%' $secPresupuestal $vigGasto $medioPago AND TD.fuente = '$fuente' AND TD.bpin LIKE '%$bpim%'
            AND TD.indicador_producto LIKE '%$programatico%'";
            $request = mysqli_fetch_all(mysqli_query($this->linkbd,$sql),MYSQLI_ASSOC);
            $total = 0;

            if(!empty($request)){
                foreach ($request as $e) {
                    $sql="SELECT cedulanit as documento,
                    CASE WHEN razonsocial IS NULL OR razonsocial = ''
                    THEN CONCAT(nombre1,' ',nombre2,' ',apellido1,' ',apellido2)
                    ELSE razonsocial END AS nombre FROM terceros  WHERE cedulanit = '$e[tercero]'";
                    $arrTercero = mysqli_query($this->linkbd,$sql)->fetch_assoc();
                    array_push($arrData,array(
                        "url"=>'teso-pagonominaver.php?idegre='.$e['id_egreso'],
                        "tipo"=>"D",
                        "comprobante"=>"Egresos nomina",
                        "consecutivo"=>$e['id_egreso'],
                        "fecha"=>$e['fecha'],
                        "estado"=>"Activo",
                        "valor"=>$e['valor'],
                        "tercero"=>$arrTercero['documento']."-".$arrTercero['nombre'],
                        "objeto"=>$e['concepto']
                    ));
                    $total+=$e['valor'];
                }
            }
            return array("data"=>$arrData,"total"=>$total);
        }
        public function selectEgresos($cuenta,$secPresupuestal,$vigGasto,$medioPago,$fuente,$bpim,$programatico,$strFechaInicial,$strFechaFinal){
            $vigenciaIni = explode("-",$strFechaInicial)[0];
            $vigenciaFin = explode("-",$strFechaFinal)[0];
            $arrData = [];
            $vigGasto = $vigGasto != 0 ? " AND TD.codigo_vigenciag LIKE '$vigGasto'" : "";
            $secPresupuestal = $secPresupuestal != 0 ? " AND TD.seccion_presupuestal LIKE '$secPresupuestal'" : "";
            $medioPago = !is_numeric($medioPago) ? " AND TD.medio_pago LIKE '$medioPago' " : "";


            $sql = "SELECT TE.id_egreso, TE.fecha, TD.valor,TE.tercero,TE.concepto
            FROM tesoordenpago_det AS TD, tesoegresos AS TE
            WHERE (TD.vigencia = '$vigenciaIni' OR TD.vigencia = '$vigenciaFin') AND TE.fecha BETWEEN '$strFechaInicial' AND '$strFechaFinal'
            AND TE.estado != 'N' AND TE.estado != 'R' AND TD.id_orden = TE.id_orden AND TD.cuentap LIKE '%$cuenta%' $secPresupuestal
            $vigGasto $medioPago AND TD.bpim LIKE '%$bpim%' AND TD.indicador_producto LIKE '%$programatico%'
            AND TD.fuente = '$fuente'";
            $request = mysqli_fetch_all(mysqli_query($this->linkbd,$sql),MYSQLI_ASSOC);
            $total = 0;

            if(!empty($request)){
                foreach ($request as $e) {
                    $sql="SELECT cedulanit as documento,
                    CASE WHEN razonsocial IS NULL OR razonsocial = ''
                    THEN CONCAT(nombre1,' ',nombre2,' ',apellido1,' ',apellido2)
                    ELSE razonsocial END AS nombre FROM terceros  WHERE cedulanit = '$e[tercero]'";
                    $arrTercero = mysqli_query($this->linkbd,$sql)->fetch_assoc();
                    array_push($arrData,array(
                        "url"=>'teso-girarchequesver-ccpet.php?idegre='.$e['id_egreso'],
                        "tipo"=>"D",
                        "comprobante"=>"Egresos",
                        "consecutivo"=>$e['id_egreso'],
                        "fecha"=>$e['fecha'],
                        "estado"=>"Activo",
                        "valor"=>$e['valor'],
                        "tercero"=>$arrTercero['documento']."-".$arrTercero['nombre'],
                        "objeto"=>$e['concepto']
                    ));
                    $total+=$e['valor'];
                }
            }
            return array("data"=>$arrData,"total"=>$total);
        }
        public function selectNomina($cuenta,$secPresupuestal,$vigGasto,$medioPago,$fuente,$bpim,$programatico,$strFechaInicial,$strFechaFinal){
            $arrData = [];
            $vigGasto = $vigGasto != 0 ? " AND HD.vigencia_gasto LIKE '$vigGasto'" : "";
            $secPresupuestal = $secPresupuestal != 0 ? " AND HD.seccion_presupuestal LIKE '$secPresupuestal'" : "";
            $medioPago = !is_numeric($medioPago) ? " AND HD.medio_pago LIKE '$medioPago' " : "";

            $sql = "SELECT HN.id_aprob, HN.fecha, HD.valor,HN.tercero
            FROM humnom_presupuestal AS HD, humnomina_aprobado AS HN
            WHERE HN.id_nom = HD.id_nom AND HN.fecha BETWEEN '$strFechaInicial' AND '$strFechaFinal'
            AND HN.estado!='N' AND HD.estado='P' AND HD.cuenta LIKE '%$cuenta%' $secPresupuestal $vigGasto $medioPago
            AND HD.fuente = '$fuente' AND HD.bpin LIKE '%$bpim%' AND HD.indicador LIKE '%$programatico%'";
            $request = mysqli_fetch_all(mysqli_query($this->linkbd,$sql),MYSQLI_ASSOC);
            $total = 0;

            if(!empty($request)){
                foreach ($request as $e) {
                    $sql="SELECT cedulanit as documento,
                    CASE WHEN razonsocial IS NULL OR razonsocial = ''
                    THEN CONCAT(nombre1,' ',nombre2,' ',apellido1,' ',apellido2)
                    ELSE razonsocial END AS nombre FROM terceros  WHERE cedulanit = '$e[tercero]'";
                    $arrTercero = mysqli_query($this->linkbd,$sql)->fetch_assoc();
                    array_push($arrData,array(
                        "url"=>'hum-aprobarnominaver.php?idr='.$e['id_aprob'],
                        "tipo"=>"D",
                        "comprobante"=>"Obligacion nomina",
                        "consecutivo"=>$e['id_aprob'],
                        "fecha"=>$e['fecha'],
                        "estado"=>"Activo",
                        "valor"=>$e['valor'],
                        "tercero"=>$arrTercero['documento']."-".$arrTercero['nombre'],
                        "objeto"=>""
                    ));
                    $total+=$e['valor'];
                }
            }
            return array("data"=>$arrData,"total"=>$total);
        }
        public function selectCxp($cuenta,$secPresupuestal,$vigGasto,$medioPago,$fuente,$bpim,$programatico,$strFechaInicial,$strFechaFinal){
            $vigenciaIni = explode("-",$strFechaInicial)[0];
            $vigenciaFin = explode("-",$strFechaFinal)[0];
            $arrData = [];
            $vigGasto = $vigGasto != 0 ? " AND DET.codigo_vigenciag LIKE '$vigGasto'" : "";
            $secPresupuestal = $secPresupuestal != 0 ? " AND DET.seccion_presupuestal LIKE '$secPresupuestal'" : "";
            $medioPago = !is_numeric($medioPago) ? " AND DET.medio_pago LIKE '$medioPago' " : "";

            $sql = "SELECT CAB.id_orden, CAB.fecha, DET.valor, CAB.estado,
            CAB.tercero,CAB.conceptorden
            FROM tesoordenpago_det AS DET, tesoordenpago AS CAB
            WHERE CAB.id_orden = DET.id_orden AND CAB.fecha BETWEEN '$strFechaInicial' AND '$strFechaFinal'
            AND (DET.vigencia = '$vigenciaIni' OR DET.vigencia = '$vigenciaFin') AND DET.cuentap LIKE '%$cuenta%'
            $secPresupuestal $medioPago $vigGasto AND DET.fuente = '$fuente' AND DET.bpim LIKE '%$bpim%'
            AND DET.indicador_producto LIKE '%$programatico%' AND CAB.estado != 'R'";
            $request = mysqli_fetch_all(mysqli_query($this->linkbd,$sql),MYSQLI_ASSOC);

            $total = 0;
            if(!empty($request)){
                foreach ($request as $e) {
                    $sql="SELECT cedulanit as documento,
                    CASE WHEN razonsocial IS NULL OR razonsocial = ''
                    THEN CONCAT(nombre1,' ',nombre2,' ',apellido1,' ',apellido2)
                    ELSE razonsocial END AS nombre FROM terceros  WHERE cedulanit = '$e[tercero]'";
                    $arrTercero = mysqli_query($this->linkbd,$sql)->fetch_assoc();
                    array_push($arrData,array(
                        "url"=>'teso-egresoverccpet.php?idop='.$e['id_orden'],
                        "tipo"=>"D",
                        "comprobante"=>"Obligacion",
                        "consecutivo"=>$e['id_orden'],
                        "fecha"=>$e['fecha'],
                        "estado"=>"Activo",
                        "valor"=>$e['valor'],
                        "tercero"=>$arrTercero['documento']."-".$arrTercero['nombre'],
                        "objeto"=>$e['conceptorden']
                    ));
                    $total+=$e['valor'];
                }
            }
            return array("data"=>$arrData,"total"=>$total);
        }
        public function selectRp($cuenta,$secPresupuestal,$vigGasto,$medioPago,$fuente,$bpim,$programatico,$strFechaInicial,$strFechaFinal){
            $vigenciaIni = explode("-",$strFechaInicial)[0];
            $vigenciaFin = explode("-",$strFechaFinal)[0];
            $arrData = [];
            $vigGasto = $vigGasto != 0 ? " AND TB2.codigo_vigenciag LIKE '$vigGasto'" : "";
            $secPresupuestal = $secPresupuestal != 0 ? " AND TB2.seccion_presupuestal LIKE '$secPresupuestal'" : "";
            $medioPago = !is_numeric($medioPago) ? " AND TB2.medio_pago LIKE '$medioPago' " : "";

            $sql = "SELECT TB2.consvigencia, TB1.fecha, TB2.valor, TB1.vigencia, TB1.estado,
            TB1.detalle,TB1.tercero
            FROM ccpetrp AS TB1, ccpetrp_detalle AS TB2
            WHERE TB1.vigencia = TB2.vigencia AND TB1.consvigencia = TB2.consvigencia AND (TB1.fecha BETWEEN '$strFechaInicial' AND '$strFechaFinal')
            AND (TB1.vigencia = '$vigenciaIni' OR TB1.vigencia = '$vigenciaFin')  $secPresupuestal
            $medioPago $vigGasto AND TB2.bpim LIKE '%$bpim%' AND TB2.indicador_producto LIKE '%$programatico%'
            AND TB2.cuenta LIKE '%$cuenta%' AND TB1.tipo_mov = '201'
            AND TB2.tipo_mov LIKE '201' AND TB2.fuente = '$fuente'";
            $request = mysqli_fetch_all(mysqli_query($this->linkbd,$sql),MYSQLI_ASSOC);

            $total = 0;
            if(!empty($request)){
                foreach ($request as $e) {
                    $sqlRev = "SELECT COALESCE(SUM(TB2.valor),0) as valor
                    FROM ccpetrp_detalle AS TB2, ccpetrp AS TB1
                    WHERE TB1.consvigencia = TB2.consvigencia AND TB1.vigencia = TB2.vigencia AND TB1.fecha BETWEEN '$strFechaInicial' AND '$strFechaFinal'
                    AND TB1.consvigencia = '$e[consvigencia]'  AND TB1.vigencia = '$e[vigencia]' AND (TB2.vigencia = '$vigenciaIni' OR TB2.vigencia = '$vigenciaFin')
                    AND TB2.cuenta = '%$cuenta%' AND TB1.tipo_mov LIKE '4%' AND TB2.tipo_mov LIKE '4%' AND TB2.bpim LIKE '%$bpim%'
                    AND TB2.indicador_producto LIKE '%$programatico%' $secPresupuestal AND TB2.fuente = '$fuente'
                    $medioPago $vigGasto";
                    $estado = $e['estado'] != 'N' ? 'Activo' : 'Anulado';
                    $requestRev = mysqli_query($this->linkbd,$sqlRev)->fetch_assoc();
                    $valor = $e['valor'];
                    if(!empty($requestRev)){
                        $valor = $e['valor']-$requestRev['valor'];
                    }
                    $strFecha = explode("-",$e['fecha'])[0];
                    $sql="SELECT cedulanit as documento,
                    CASE WHEN razonsocial IS NULL OR razonsocial = ''
                    THEN CONCAT(nombre1,' ',nombre2,' ',apellido1,' ',apellido2)
                    ELSE razonsocial END AS nombre FROM terceros  WHERE cedulanit = '$e[tercero]'";
                    $arrTercero = mysqli_query($this->linkbd,$sql)->fetch_assoc();
                    array_push($arrData,array(
                        "url"=>'ccp-rpVisualizar.php?is='.$e['consvigencia'].'&vig='.$strFecha,
                        "tipo"=>"D",
                        "comprobante"=>"Compromisos",
                        "consecutivo"=>$e['consvigencia'],
                        "fecha"=>$e['fecha'],
                        "estado"=>$estado,
                        "valor"=>$valor,
                        "tercero"=>$arrTercero['documento']."-".$arrTercero['nombre'],
                        "objeto"=>$e['detalle']
                    ));
                    $total+=$valor;
                }
            }
            array_push($arrData,array(
                "tipo"=>"T",
                "comprobante"=>"Total compromisos",
                "valor"=>$total,
            ));
            return array("data"=>$arrData,"total"=>$total);
        }
        public function selectCdp($cuenta,$secPresupuestal,$vigGasto,$medioPago,$fuente,$bpim,$programatico,$strFechaInicial,$strFechaFinal){
            $arrData = [];
            $vigGasto = $vigGasto != 0 ? " AND TB2.codigo_vigenciag LIKE '$vigGasto'" : "";
            $secPresupuestal = $secPresupuestal != 0 ? " AND TB2.seccion_presupuestal LIKE '$secPresupuestal'" : "";
            $medioPago = $medioPago != 0 ? " AND TB2.medio_pago LIKE '$medioPago' " : "";

            $sql = "SELECT TB2.consvigencia, TB1.fecha,TB1.solicita,TB1.objeto, TB2.valor, TB2.vigencia, TB1.estado
            FROM ccpetcdp AS TB1, ccpetcdp_detalle AS TB2
            WHERE TB1.vigencia = TB2.vigencia AND TB1.consvigencia = TB2.consvigencia $secPresupuestal $medioPago $vigGasto
            AND TB2.fuente = '$fuente' AND TB2.cuenta LIKE '%$cuenta%' AND TB2.tipo_mov LIKE '201' AND TB1.tipo_mov LIKE '201'
            AND TB2.bpim LIKE '%$bpim%' AND TB2.indicador_producto LIKE '%$programatico%'
            AND TB1.fecha BETWEEN '$strFechaInicial' AND '$strFechaFinal'";

            $request = mysqli_fetch_all(mysqli_query($this->linkbd,$sql),MYSQLI_ASSOC);
            $total = 0;
            if(!empty($request)){
                foreach ($request as $e) {
                    $sqlCdpRev = "SELECT COALESCE(SUM(TB2.valor),0) as valor
                    FROM ccpetcdp AS TB1, ccpetcdp_detalle AS TB2
                    WHERE TB1.vigencia = TB2.vigencia AND TB1.consvigencia = TB2.consvigencia AND TB1.consvigencia = '$e[consvigencia]'
                    $secPresupuestal $medioPago $vigGasto AND TB2.cuenta LIKE '%$cuenta%' AND TB2.tipo_mov LIKE '4%'
                    AND TB1.tipo_mov LIKE '4%' AND TB2.bpim LIKE '%$bpim%' AND TB2.indicador_producto LIKE '%$programatico%'
                    AND TB1.fecha BETWEEN '$strFechaInicial' AND '$strFechaFinal' AND TB2.fuente = '$fuente'";
                    $estado = $e['estado'] != 'N' ? 'Activo' : 'Anulado';
                    $requestCdp = mysqli_query($this->linkbd,$sqlCdpRev)->fetch_assoc();
                    $valor = $e['valor'];
                    if(!empty($requestCdp)){
                        $valor = $e['valor']-$requestCdp['valor'];
                    }
                    $strFecha = explode("-",$e['fecha'])[0];
                    $sql="SELECT cedulanit as documento,
                    CASE WHEN razonsocial IS NULL OR razonsocial = ''
                    THEN CONCAT(nombre1,' ',nombre2,' ',apellido1,' ',apellido2)
                    ELSE razonsocial END AS nombre FROM terceros  WHERE CONCAT(nombre1,' ',nombre2,' ',apellido1,' ',apellido2) = '$e[solicita]'
                    OR razonsocial = '$e[solicita]'";
                    $arrTercero = mysqli_query($this->linkbd,$sql)->fetch_assoc();
                    array_push($arrData,array(
                        "url"=>'ccp-cdpVisualizar.php?is='.$e['consvigencia'].'&vig='.$strFecha,
                        "tipo"=>"D",
                        "comprobante"=>"Disponibilidad",
                        "consecutivo"=>$e['consvigencia'],
                        "fecha"=>$e['fecha'],
                        "estado"=>$estado,
                        "valor"=>$valor,
                        "tercero"=>!empty($arrTercero) ? $arrTercero['documento']."-".$arrTercero['nombre'] : $e['solicita'],
                        "objeto"=>$e['objeto']
                    ));
                    $total+=$valor;
                }
            }
            array_push($arrData,array(
                "tipo"=>"T",
                "comprobante"=>"Total disponibilidad",
                "valor"=>$total,
            ));
            return array("data"=>$arrData,"total"=>$total);
        }
        public function selectTraslados($cuenta,$secPresupuestal,$vigGasto,$medioPago,$fuente,$bpim,$programatico,$strFechaInicial,$strFechaFinal,$tipo){
            $arrData = [];
            $vigGasto = $vigGasto != 0 ? " AND codigo_vigenciag LIKE '$vigGasto'" : "";
            $secPresupuestal = $secPresupuestal != 0 ? " AND seccion_presupuestal LIKE '$secPresupuestal'" : "";
            $medioPago = $medioPago != 0 ? " AND medio_pago LIKE '$medioPago'" : "";

            $sql = "SELECT id_acuerdo, fecha, valor, estado FROM ccpet_traslados
            WHERE cuenta LIKE '%$cuenta%' AND tipo = '$tipo' $secPresupuestal $medioPago $vigGasto AND fuente = '$fuente'
            AND estado = 'S' AND bpim LIKE '%$bpim%' AND programatico LIKE '%$programatico%' AND fecha
            BETWEEN '$strFechaInicial' AND '$strFechaFinal'";
            $request = mysqli_fetch_all(mysqli_query($this->linkbd,$sql),MYSQLI_ASSOC);
            $total = 0;
            if(!empty($request)){
                foreach ($request as $e) {
                    $sqlAcuerdo = "SELECT consecutivo, tipo_acto_adm FROM ccpetacuerdos WHERE id_acuerdo = $e[id_acuerdo]";
                    $requestAcuerdo = mysqli_query($this->linkbd,$sqlAcuerdo)->fetch_assoc();
                    $tipoActo = '';
                    $estado = $e['estado'] == 'S' ? 'Activo' : 'Anulado';
                    if($requestAcuerdo['tipo_acto_adm'] == 1) {
                        $tipoActo = 'Acuerdo';
                    }elseif ($requestAcuerdo['tipo_acto_adm'] == 2) {
                        $tipoActo = 'Resolucion';
                    }else {
                        $tipoActo = 'Decreto';
                    }
                    array_push($arrData,array(
                        "tipo"=>"D",
                        "comprobante"=>$tipo == "C" ? "Credito" :"Contra credito",
                        "consecutivo"=>$e['id_acuerdo']."-".$tipoActo.":".$requestAcuerdo['consecutivo'],
                        "fecha"=>$e['fecha'],
                        "estado"=>$estado,
                        "valor"=>$e['valor'],
                        "tercero"=>"",
                        "objeto"=>""
                    ));
                    $total+=$e['valor'];
                }
            }
            array_push($arrData,array(
                "tipo"=>"T",
                "comprobante"=>$tipo == "C" ? "Total creditos" :"Total contra creditos",
                "valor"=>$total,
            ));
            return array("data"=>$arrData,"total"=>$total);
        }
        public function selectReduccion($cuenta,$secPresupuestal,$vigGasto,$medioPago,$fuente,$bpim,$programatico,$strFechaInicial,$strFechaFinal){
            $arrData = [];
            $vigGasto = $vigGasto != 0 ? " AND codigo_vigenciag LIKE '$vigGasto'" : "";
            $secPresupuestal = $secPresupuestal != 0 ? " AND seccion_presupuestal LIKE '$secPresupuestal'" : "";
            $medioPago = $medioPago != 0 ? " AND medio_pago LIKE '$medioPago'" : "";

            $sql = "SELECT id_adicion, fecha, valor, estado FROM ccpet_reducciones
            WHERE cuenta LIKE '%$cuenta%' AND tipo_cuenta = 'G' $secPresupuestal $medioPago $vigGasto AND fuente = '$fuente'
            AND estado = 'S' AND bpim LIKE '%$bpim%' AND programatico LIKE '%$programatico%' AND fecha
            BETWEEN '$strFechaInicial' AND '$strFechaFinal'";
            $request = mysqli_fetch_all(mysqli_query($this->linkbd,$sql),MYSQLI_ASSOC);
            $total = 0;
            if(!empty($request)){
                foreach ($request as $e) {
                    $sqlAcuerdo = "SELECT consecutivo, tipo_acto_adm FROM ccpetacuerdos WHERE id_acuerdo = $e[id_acuerdo]";
                    $requestAcuerdo = mysqli_query($this->linkbd,$sqlAcuerdo)->fetch_assoc();
                    $tipoActo = '';
                    $estado = $e['estado'] == 'S' ? 'Activo' : 'Anulado';
                    if($requestAcuerdo['tipo_acto_adm'] == 1) {
                        $tipoActo = 'Acuerdo';
                    }elseif ($requestAcuerdo['tipo_acto_adm'] == 2) {
                        $tipoActo = 'Resolucion';
                    }else {
                        $tipoActo = 'Decreto';
                    }
                    array_push($arrData,array(
                        "tipo"=>"D",
                        "comprobante"=>"Reducción",
                        "consecutivo"=>$e['id_adicion']."-".$tipoActo.":".$requestAcuerdo['consecutivo'],
                        "fecha"=>$e['fecha'],
                        "estado"=>$estado,
                        "valor"=>$e['valor'],
                        "tercero"=>"",
                        "objeto"=>""
                    ));
                    $total+=$e['valor'];
                }
            }
            array_push($arrData,array(
                "tipo"=>"T",
                "comprobante"=>"Total reducción",
                "valor"=>$total,
            ));
            return array("data"=>$arrData,"total"=>$total);
        }
        public function selectAdicion($cuenta,$secPresupuestal,$vigGasto,$medioPago,$fuente,$bpim,$programatico,$strFechaInicial,$strFechaFinal){
            $arrData = [];
            $vigGasto = $vigGasto != 0 ? " AND codigo_vigenciag LIKE '$vigGasto'" : "";
            $secPresupuestal = $secPresupuestal != 0 ? " AND seccion_presupuestal LIKE '$secPresupuestal'" : "";
            $medioPago = $medioPago != 0 ? " AND medio_pago LIKE '$medioPago'" : "";

            $sql = "SELECT id_acuerdo, fecha, valor, estado FROM ccpet_adiciones
            WHERE cuenta LIKE '%$cuenta%' AND tipo_cuenta = 'G' $secPresupuestal $medioPago $vigGasto AND fuente = '$fuente'
            AND estado = 'S' AND bpim LIKE '%$bpim%' AND programatico LIKE '%$programatico%' AND fecha
            BETWEEN '$strFechaInicial' AND '$strFechaFinal'";
            $request = mysqli_fetch_all(mysqli_query($this->linkbd,$sql),MYSQLI_ASSOC);
            $total = 0;
            if(!empty($request)){
                foreach ($request as $e) {
                    $sqlAcuerdo = "SELECT consecutivo, tipo_acto_adm FROM ccpetacuerdos WHERE id_acuerdo = $e[id_acuerdo]";
                    $requestAcuerdo = mysqli_query($this->linkbd,$sqlAcuerdo)->fetch_assoc();
                    $tipoActo = '';
                    $estado = $e['estado'] == 'S' ? 'Activo' : 'Anulado';
                    if($requestAcuerdo['tipo_acto_adm'] == 1) {
                        $tipoActo = 'Acuerdo';
                    }elseif ($requestAcuerdo['tipo_acto_adm'] == 2) {
                        $tipoActo = 'Resolucion';
                    }else {
                        $tipoActo = 'Decreto';
                    }
                    array_push($arrData,array(
                        "url"=>'ccp-visualizaAdicion.php?id='.$e['id_acuerdo'],
                        "tipo"=>"D",
                        "comprobante"=>"Adición",
                        "consecutivo"=>$e['id_acuerdo']."-".$tipoActo.":".$requestAcuerdo['consecutivo'],
                        "fecha"=>$e['fecha'],
                        "estado"=>$estado,
                        "valor"=>$e['valor'],
                        "tercero"=>"",
                        "objeto"=>""
                    ));
                    $total+=$e['valor'];
                }
            }
            array_push($arrData,array(
                "tipo"=>"T",
                "comprobante"=>"Total adición",
                "valor"=>$total,
            ));
            return array("data"=>$arrData,"total"=>$total);
        }
        public function selectInicial($cuenta,$secPresupuestal,$vigGasto,$medioPago,$fuente,$bpim,$programatico,$strFechaInicial,$strFechaFinal){
            $arrData = [];
            $vigenciaIni = explode("-",$strFechaInicial)[0];
            $vigenciaFin = explode("-",$strFechaFinal)[0];

            if($cuenta !=""){
                $vigGasto = $vigGasto != 0 ? " AND vigencia_gasto LIKE '$vigGasto'" : "";
                $secPresupuestal = $secPresupuestal != 0 ? " AND seccion_presupuestal LIKE '$secPresupuestal'" : "";
                $medioPago = $medioPago != 0 ? " AND medio_pago LIKE '$medioPago'" : "";

                $sql = "SELECT COALESCE(valor,0) as valor, vigencia
                FROM ccpetinicialgastosfun
                WHERE cuenta LIKE '$cuenta' $secPresupuestal $vigGasto $medioPago AND fuente = '$fuente'
                AND (vigencia = '$vigenciaIni' OR vigencia = '$vigenciaFin')";
            }else{
                $sql="SELECT COALESCE(TB2.valorcsf + TB2.valorssf, 0) as valor, TB1.vigencia
                FROM ccpproyectospresupuesto AS TB1, ccpproyectospresupuesto_presupuesto AS TB2
                WHERE TB1.id = TB2.codproyecto AND TB1.codigo = '$bpim' AND TB1.vigencia = '$vigenciaIni' AND TB1.vigencia = '$vigenciaFin'
                AND TB2.id_fuente = '$fuente' AND TB2.medio_pago = '$medioPago' AND TB2.indicador_producto = '$programatico' AND TB2.vigencia_gasto = '$vigGasto'";
            }
            $request = mysqli_query($this->linkbd,$sql)->fetch_assoc();
            $total = $request['valor'];
            array_push($arrData,array(
                "tipo"=>"D",
                "comprobante"=>"Presupuesto inicial",
                "consecutivo"=>"",
                "fecha"=>$request['vigencia'],
                "estado"=>"",
                "valor"=>$request['valor'],
                "tercero"=>"",
                "objeto"=>""
            ));
            return array("data"=>$arrData,"total"=>$total);
        }
        /******************Funciones para datos iniciales********************* */
        public function selectProgramas(){
            $sql = "SELECT codigo, nombre FROM ccpetprogramas";
            $request = mysqli_fetch_all(mysqli_query($this->linkbd,$sql),MYSQLI_ASSOC);
            return $request;
        }
        public function selectSectores(){
            $sql = "SELECT codigo, nombre FROM ccpetsectores";
            $request = mysqli_fetch_all(mysqli_query($this->linkbd,$sql),MYSQLI_ASSOC);
            return $request;
        }
        public function selectProgramatico(){
            $sql = "SELECT DISTINCT p.codigo_indicador as codigo,p.indicador_producto as nombre,
            p.cod_producto,p.producto
            FROM ccpproyectospresupuesto_productos pp
            INNER JOIN ccpetproductos p ON pp.indicador = p.codigo_indicador
            WHERE pp.estado = 'S'";
            $request = mysqli_fetch_all(mysqli_query($this->linkbd,$sql),MYSQLI_ASSOC);
            return $request;
        }
        public function selectBpim(){
            $sql = "SELECT DISTINCT codigo,nombre FROM ccpproyectospresupuesto WHERE estado = 'S'";
            $request = mysqli_fetch_all(mysqli_query($this->linkbd,$sql),MYSQLI_ASSOC);
            return $request;
        }
        public function selectSecciones(){
            $sql = "SELECT * FROM pptoseccion_presupuestal WHERE estado = 'S'";
            $request = mysqli_fetch_all(mysqli_query($this->linkbd,$sql),MYSQLI_ASSOC);
            return $request;
        }
        public function selectVigencias(){
            $sql = "SELECT * FROM ccpet_vigenciadelgasto WHERE version = (SELECT MAX(version) FROM ccpet_vigenciadelgasto)";
            $request = mysqli_fetch_all(mysqli_query($this->linkbd,$sql),MYSQLI_ASSOC);
            return $request;
        }
        public function selectFuentes(){
            $sql="SELECT codigo_fuente as codigo, nombre
            FROM ccpet_fuentes_cuipo
            WHERE version = '1' ORDER BY codigo_fuente";
            $request = mysqli_fetch_all(mysqli_query($this->linkbd,$sql),MYSQLI_ASSOC);
            return $request;
        }
        public function selectCuentas(){
            $sql = "SELECT id,codigo, nombre, tipo, nivel
            FROM cuentasccpet WHERE tipo ='C' ORDER BY id ASC";
            $request = mysqli_fetch_all(mysqli_query($this->linkbd,$sql),MYSQLI_ASSOC);
            return $request;
        }
        public function selectOrden(){
            $sql="SELECT orden FROM ccpet_parametros";
            $request = mysqli_query($this->linkbd,$sql)->fetch_assoc();
            if(!empty($request)){
                $request = $request['orden'];
            }else{
                $request = 1;
            }
            return $request;
        }
    }

	if($_GET){
        $strCuenta = $_GET['cuenta'];
        $strNombreCuenta = $strCuenta =="" ? "" :$_GET['nombre_cuenta'];
        $strFuente = $_GET['fuente'];
        $strNombreFuente = $strFuente == "" ? "" : $_GET['nombre_fuente'];
        $strFechaInicial = date_format(date_create($_GET['fecha_inicial']),"d/m/Y");
        $strFechaFinal = date_format(date_create($_GET['fecha_final']),"d/m/Y");
        $intVigencia = $_GET['vigencia'];
        $strSeccion = $_GET['seccion'];
        $strPago = $_GET['pago'];
        $strBpim = $_GET['bpim'];
        $strProgramatico = $_GET['programatico'];
        $strNombreBpim = $_GET['nombre_bpim'];
        $strNombreProgramatico = $_GET['nombre_programatico'];
        $strCellProgramatico ="";
        if($strProgramatico != ""){
            $total = count(str_split($strProgramatico));
            if($total>4){
                $strCellProgramatico = "PROGRAMÁTICO";
            }else if($total>2 && $total< 5){
                $strCellProgramatico = "PROGRAMA";
            }else if($total < 3){
                $strCellProgramatico = "SECTOR";
            }
        }
        $obj = new Plantilla();
        $arrData = $obj->getInfo();
        $objPHPExcel = new PHPExcel();
        $objPHPExcel->getActiveSheet()->getStyle('A:G')->applyFromArray(
            array(
                'alignment' => array(
                    'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
                    'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER
                )
            )
        );
        $objPHPExcel->getProperties()
        ->setCreator("IDEAL 10")
        ->setLastModifiedBy("IDEAL 10")
        ->setTitle("Exportar Excel con PHP")
        ->setSubject("Documento de prueba")
        ->setDescription("Documento generado con PHPExcel")
        ->setKeywords("usuarios phpexcel")
        ->setCategory("reportes");

        //----Cuerpo de Documento----
        $objPHPExcel->setActiveSheetIndex(0)
        ->mergeCells('A1:G1')
        ->mergeCells('A2:G2')
        ->mergeCells('B3:G3')
        ->mergeCells('B4:G4')
        ->mergeCells('B5:G5')
        ->mergeCells('B6:G6')
        ->mergeCells('B7:G7')
        ->mergeCells('B8:G8')
        ->mergeCells('B9:G9')
        ->mergeCells('B10:G10')
        ->setCellValue('A1', 'PRESUPUESTO')
        ->setCellValue('A2', 'REPORTE AUXILIAR POR CUENTA GASTOS')
        ->setCellValue('A3', 'FECHA')
        ->setCellValue('A4', 'CUENTA')
        ->setCellValue('A5', 'BPIM')
        ->setCellValue('A6', $strCellProgramatico)
        ->setCellValue('A7', 'FUENTE')
        ->setCellValue('A8', 'MEDIO DE PAGO')
        ->setCellValue('A9', 'VIGENCIA DE GASTO')
        ->setCellValue('A10', 'SECCIÓN PRESUPUESTAL')
        ->setCellValue('B3', "Desde ".$strFechaInicial." Hasta ".$strFechaFinal)
        ->setCellValue('B4', $strCuenta."-".$strNombreCuenta)
        ->setCellValueExplicit('B5', $strBpim."-".$strNombreBpim,PHPExcel_Cell_DataType :: TYPE_STRING)
        ->setCellValueExplicit('B6', $strProgramatico."-".$strNombreProgramatico,PHPExcel_Cell_DataType :: TYPE_STRING)
        ->setCellValue('B7', $strFuente."-".$strNombreFuente)
        ->setCellValue('B8', $strPago)
        ->setCellValue('B9', $intVigencia)
        ->setCellValue('B10', $strSeccion);
        $objPHPExcel-> getActiveSheet ()
        -> getStyle ("A3")
        -> getFill ()
        -> setFillType (PHPExcel_Style_Fill :: FILL_SOLID)
        -> getStartColor ()
        -> setRGB ('3399cc');
        $objPHPExcel-> getActiveSheet ()
        -> getStyle ("A4")
        -> getFill ()
        -> setFillType (PHPExcel_Style_Fill :: FILL_SOLID)
        -> getStartColor ()
        -> setRGB ('3399cc');
        $objPHPExcel-> getActiveSheet ()
        -> getStyle ("A5")
        -> getFill ()
        -> setFillType (PHPExcel_Style_Fill :: FILL_SOLID)
        -> getStartColor ()
        -> setRGB ('3399cc');
        $objPHPExcel-> getActiveSheet ()
        -> getStyle ("A6")
        -> getFill ()
        -> setFillType (PHPExcel_Style_Fill :: FILL_SOLID)
        -> getStartColor ()
        -> setRGB ('3399cc');
        $objPHPExcel-> getActiveSheet ()
        -> getStyle ("A7")
        -> getFill ()
        -> setFillType (PHPExcel_Style_Fill :: FILL_SOLID)
        -> getStartColor ()
        -> setRGB ('3399cc');
        $objPHPExcel-> getActiveSheet ()
        -> getStyle ("A8")
        -> getFill ()
        -> setFillType (PHPExcel_Style_Fill :: FILL_SOLID)
        -> getStartColor ()
        -> setRGB ('3399cc');
        $objPHPExcel-> getActiveSheet ()
        -> getStyle ("A9")
        -> getFill ()
        -> setFillType (PHPExcel_Style_Fill :: FILL_SOLID)
        -> getStartColor ()
        -> setRGB ('3399cc');
        $objPHPExcel-> getActiveSheet ()
        -> getStyle ("A10")
        -> getFill ()
        -> setFillType (PHPExcel_Style_Fill :: FILL_SOLID)
        -> getStartColor ()
        -> setRGB ('3399cc');
        $objPHPExcel-> getActiveSheet ()
        -> getStyle ("A1")
        -> getFill ()
        -> setFillType (PHPExcel_Style_Fill :: FILL_SOLID)
        -> getStartColor ()
        -> setRGB ('C8C8C8');
        $objPHPExcel-> getActiveSheet ()
        -> getStyle ("A1:A2")
        -> getFont ()
        -> setBold ( true )
        -> setName ( 'Verdana' )
        -> setSize ( 10 )
        -> getColor ()
        -> setRGB ('000000');
        $objPHPExcel-> getActiveSheet ()
        -> getStyle ('A1:A2')
        -> getAlignment ()
        -> applyFromArray (array ( 'horizontal'  =>  PHPExcel_Style_Alignment :: HORIZONTAL_CENTER ,) );
        $objPHPExcel-> getActiveSheet ()
        -> getStyle ('A4:j4')
        -> getAlignment ()
        -> applyFromArray (array ( 'horizontal'  =>  PHPExcel_Style_Alignment :: VERTICAL_JUSTIFY ,) );
        $objPHPExcel-> getActiveSheet ()
        -> getStyle ("A2")
        -> getFill ()
        -> setFillType (PHPExcel_Style_Fill :: FILL_SOLID);

        $borders = array(
            'borders' => array(
                'allborders' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN,
                'color' => array('argb' => 'FF000000'),
                )
            ),
        );
        $objPHPExcel->setActiveSheetIndex(0)
        ->setCellValue('A11', 'Tipo comprobante')
        ->setCellValue('B11', "Consecutivo")
        ->setCellValue('C11', "Tercero")
        ->setCellValue('D11', "Objeto")
        ->setCellValue('E11', "Fecha")
        ->setCellValue('F11', "Estado")
        ->setCellValue('G11', "Valor");
        $objPHPExcel-> getActiveSheet ()
            -> getStyle ("A11:G11")
            -> getFill ()
            -> setFillType (PHPExcel_Style_Fill :: FILL_SOLID)
            -> getStartColor ()
            -> setRGB ('99ddff');
        $objPHPExcel->getActiveSheet()->getStyle("A11:G11")->getFont()->setBold(true);
        $objPHPExcel->getActiveSheet()->getStyle('A1:G1')->applyFromArray($borders);
        $objPHPExcel->getActiveSheet()->getStyle('A2:G2')->applyFromArray($borders);
        $objPHPExcel->getActiveSheet()->getStyle('A3:G3')->applyFromArray($borders);
        $objPHPExcel->getActiveSheet()->getStyle('A4:G4')->applyFromArray($borders);
        $objPHPExcel->getActiveSheet()->getStyle('A5:G5')->applyFromArray($borders);
        $objPHPExcel->getActiveSheet()->getStyle('A6:G6')->applyFromArray($borders);
        $objPHPExcel->getActiveSheet()->getStyle('A7:G7')->applyFromArray($borders);
        $objPHPExcel->getActiveSheet()->getStyle('A8:G8')->applyFromArray($borders);
        $objPHPExcel->getActiveSheet()->getStyle('A9:G9')->applyFromArray($borders);
        $objPHPExcel->getActiveSheet()->getStyle('A10:G10')->applyFromArray($borders);

        $objPHPExcel->getActiveSheet()->getStyle("A3")->getFont()->getColor()->setRGB("ffffff");
        $objPHPExcel->getActiveSheet()->getStyle("A4")->getFont()->getColor()->setRGB("ffffff");
        $objPHPExcel->getActiveSheet()->getStyle("A5")->getFont()->getColor()->setRGB("ffffff");
        $objPHPExcel->getActiveSheet()->getStyle("A6")->getFont()->getColor()->setRGB("ffffff");
        $objPHPExcel->getActiveSheet()->getStyle("A7")->getFont()->getColor()->setRGB("ffffff");
        $objPHPExcel->getActiveSheet()->getStyle("A8")->getFont()->getColor()->setRGB("ffffff");
        $objPHPExcel->getActiveSheet()->getStyle("A9")->getFont()->getColor()->setRGB("ffffff");
        $objPHPExcel->getActiveSheet()->getStyle("A10")->getFont()->getColor()->setRGB("ffffff");

        $objPHPExcel->getActiveSheet()->getStyle("A3")->getFont()->setBold(true);
        $objPHPExcel->getActiveSheet()->getStyle("A4")->getFont()->setBold(true);
        $objPHPExcel->getActiveSheet()->getStyle("A5")->getFont()->setBold(true);
        $objPHPExcel->getActiveSheet()->getStyle("A6")->getFont()->setBold(true);
        $objPHPExcel->getActiveSheet()->getStyle("A7")->getFont()->setBold(true);
        $objPHPExcel->getActiveSheet()->getStyle("A8")->getFont()->setBold(true);
        $objPHPExcel->getActiveSheet()->getStyle("A9")->getFont()->setBold(true);
        $objPHPExcel->getActiveSheet()->getStyle("A10")->getFont()->setBold(true);

        $row = 12;
        foreach ($arrData as $data) {
            $strFecha = date_create($data['fecha']);
            $strFechaFormat = date_format($strFecha,"d/m/Y");
            if($data['tipo'] == "T"){
                $objPHPExcel-> getActiveSheet ()
                -> getStyle ("A$row:G$row")
                -> getFill ()
                -> setFillType (PHPExcel_Style_Fill :: FILL_SOLID)
                -> getStartColor ()
                -> setRGB ('99ddff');
            }
            $objPHPExcel->setActiveSheetIndex(0)
            ->setCellValueExplicit ("A$row", $data['comprobante'], PHPExcel_Cell_DataType :: TYPE_STRING)
            ->setCellValueExplicit ("B$row", $data['tipo'] == "T" ? "" : $data['consecutivo'], PHPExcel_Cell_DataType :: TYPE_STRING)
            ->setCellValueExplicit ("C$row", $data['tipo'] == "T" ? "" : $data['tercero'], PHPExcel_Cell_DataType :: TYPE_STRING)
            ->setCellValueExplicit ("D$row", $data['tipo'] == "T" ? "" : $data['objeto'], PHPExcel_Cell_DataType :: TYPE_STRING)
            ->setCellValueExplicit ("E$row", $data['tipo'] == "T" ? "" : $strFechaFormat, PHPExcel_Cell_DataType :: TYPE_STRING)
            ->setCellValueExplicit ("F$row", $data['tipo'] == "T" ? "" : $data['estado'], PHPExcel_Cell_DataType :: TYPE_STRING)
            ->setCellValueExplicit ("G$row", $data['valor'], PHPExcel_Cell_DataType :: TYPE_NUMERIC);
            $row++;
        }

        $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);
        //----Guardar documento----
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="reporte-auxiliar_cuenta_gastos.xlsx"');
        header('Cache-Control: max-age=0');
        $objWriter=PHPExcel_IOFactory::createWriter($objPHPExcel,'Excel2007');
        $objWriter->save('php://output');
        die();

    }
?>
