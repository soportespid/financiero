<?php //V 1000 12/12/16 ?>
<?php
require "comun.inc";
require "funciones.inc";
session_start();
cargarcodigopag($_GET['codpag'],$_SESSION["nivel"]);
$linkbd = conectar_v7();
$linkbd -> set_charset("utf8");
//**niveles menu: Administracion (0) - Consultas (1) - Herramientas (2) - Reportes (3)
?>
<html lang="es">
    <head>
        <meta charset="utf-8"/>
		<meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1"/>
		<meta name="viewport" content="user-scalable=no">
        <title>:: IDEAL 10 - Presupuesto</title>
        <link href="favicon.ico" rel="shortcut icon"/>

		<script language="JavaScript1.2">
			function validar()
			{
				document.form2.submit();
			}
			function pdf()
			{
				document.form2.action="pdfpredialexoneracion.php";
				document.form2.target="_BLANK";
				document.form2.submit();
				document.form2.action="";
				document.form2.target="";
			}

			function atrasc(id){
				id--;
				if (id!=0) {
					document.form2.action="teso-exoneracionver.php?idpres="+id;
					document.form2.submit();
				}
			}

			function adelente(id){
				id++;
				if (id<=document.form2.maximo.value){
					document.form2.action="teso-exoneracionver.php?idpres="+id;
					document.form2.submit();
				}

			}
		</script>

		<script src="css/programas.js"></script>
		<script src="css/calendario.js"></script>
		<link href="css/css2.css" rel="stylesheet" type="text/css" />
		<link href="css/css3.css" rel="stylesheet" type="text/css" />

	</head>
	<body>
		<IFRAME src="alertas.php" name="alertas" id="alertas" style="display:none"></IFRAME>
		<span id="todastablas2"></span>
		<table>
			<tr><script>barra_imagenes("teso");</script><?php cuadro_titulos();?></tr>
			<tr><?php menu_desplegable("teso");?></tr>
			<tr>
				<td colspan="3" class="cinta">
					<a href="teso-exoneracionpredios.php" class="mgbt"><img src="imagenes/add.png" alt="Nuevo"  border="0" title="Nuevo"/></a>
					<a href="#"  onClick="guardar()" class="mgbt"><img src="imagenes/guardad.png"  alt="Guardar" title="Guardar"/></a>
					<a href="#" onClick="location.href='teso-buscaexoneraciones.php'" class="mgbt"> <img src="imagenes/busca.png"  alt="Buscar" title="Buscar"/></a>
					<a onClick="mypop=window.open('plan-agenda.php','','');mypop.focus()" class="mgbt"><img src="imagenes/agenda1.png" title="Agenda" /></a>
					<a href="#" onClick="mypop=window.open('teso-principal.php','','');mypop.focus();" class="mgbt"><img src="imagenes/nv.png" alt="nueva ventana" title="Nueva Ventana"> </a>
					<a href="#" onClick="pdf()" class="mgbt"> <img src="imagenes/print.png"  alt="Buscar" title="Imprimir" /></a>
					<a href="teso-buscaexoneraciones.php" class="mgbt"><img src="imagenes/iratras.png" title="Atr&aacute;s"></a>
				</td>
			</tr>
		</table>
		<tr>
			<td colspan="3" class="tablaprin" align="center">
				<?php
					$vigencia=date('Y');
					$vigusu=vigencia_usuarios($_SESSION['cedulausu']);
					$vigencia=$vigusu;

					$_POST['idpres']=$_GET['idpres'];
					$_POST['dcuentas']=array();
					$_POST['dncuentas']=array();
					$_POST['dtcuentas']=array();
					$_POST['dvalores']=array();

					$sqlr = "SELECT max(id) from tesoexoneracion";
					$res = mysqli_query($linkbd, $sqlr);
					$row = mysqli_fetch_row($res);
					$_POST['maximo']=$row[0];

					$sqlr="select *from tesoexoneracion where id=".$_POST['idpres']." ";
					$res=mysqli_query($linkbd, $sqlr);
					$row=mysqli_fetch_row($res);

					$_POST['idpres']=$row[0];
					$_POST['fecha']=$row[1];
					$_POST['nresol']=$row[2];
					$_POST['codcat']=$row[3];

	 				$sqlr="select *from tesopredios where cedulacatastral=".$_POST['codcat']." ";
					//echo "s:$sqlr";
					$res=mysqli_query($linkbd, $sqlr);
					while($row=mysqli_fetch_row($res))
	  				{
						//$_POST[vigencia]=$row[0];
						$_POST['catastral']=$row[0];
						$_POST['propietario']=$row[6];
						$_POST['documento']=$row[5];
						$_POST['direccion']=$row[7];
						$_POST['ha']=$row[8];
						$_POST['mt2']=$row[9];
						$_POST['areac']=$row[10];
						$_POST['avaluo']=number_format($row[11],2);
						$_POST['tipop']=$row[14];
						if($_POST['tipop']=='urbano')
							$_POST['estrato']=$row[15];
						else
							$_POST['rangos']=$row[15];
						// $_POST[dcuentas][]=$_POST[estrato];
						$_POST['dtcuentas'][]=$row[1];
						$_POST['dvalores'][]=$row[5];
						$_POST['buscav']="";
	  				}
					/*$sqlr2="select *from tesoexentos where cedulacatastral=".$_POST[codcat]." ";
					$res2=mysql_query($sqlr2,$linkbd);
					$row2=mysql_fetch_row($res2);

					*/
					//echo "s:$sqlr";
					?>

					<form  name="form2" method="post" action="">
						<table class="inicio" align="center" >

							<input type="hidden" name="maximo" value="<?php echo $_POST['maximo'] ?>" >
							<tr>
								<td class="titulos" colspan="6">.: Exoneraci&oacute;n Predios</td><td width="72" class="cerrar" ><a href="teso-principal.php">Cerrar</a></td>
							</tr>
							<tr>
								<td class="saludo1">No Exoneraci&oacute;n:</td>
								<td style="width:12%;">
									<a href="#" onclick="atrasc(<?php echo $_POST['idpres']?>)"><img src="imagenes/back.png" alt="anterior" align="absmiddle"></a>
									<input style="width:65%;" name="idpres" type="text" id="idpres"  onClick="document.getElementById('idpres').focus();document.getElementById('idpres').select();" onKeyUp="return tabular(event,this)" value="<?php echo $_POST['idpres']?>" readonly >
									<a href="#" onclick="adelente(<?php echo $_POST['idpres']?>)"><img src="imagenes/next.png" alt="anterior" align="absmiddle"></a>
								</td>
								<td class="saludo1" style="width:10%;"> Tipo:</td>
								<td style="width:15%;">
									<select name="tipoex" id="tipoex" onChange="validar();" onKeyUp="return tabular(event,this)" style="width:50%">
											<option value="exoneracion" <?php if($_POST['tipoex']=='exoneracion') echo "SELECTED"?>> Exoneraci&oacute;n</option>
									</select>
								</td>
        					</tr>
        					<tr>
								<td class="saludo1" style="width:10%;">C&oacute;digo Catastral:</td>
								<td>
									<input name="tesorero" type="hidden" value="<?php echo $_POST['tesorero'] ?>">
									<input id="codcat" type="text" name="codcat" style="width:100%;"onKeyUp="return tabular(event,this)" onBlur="buscar(event)" value="<?php echo $_POST['codcat']?>" onClick="document.getElementById('tercero').focus();document.getElementById('tercero').select();" readonly>
									<input type="hidden" value="0" name="bt"> <input type="hidden" name="chacuerdo" value="1">
									<input type="hidden" value="1" name="oculto" id="oculto">
								</td>
								<td class="saludo1" style="width:10%;">No Resoluci&oacute;n:</td>
								<td style="width:10%;">
									<input name="nresol" type="text" id="nresol"  onClick="document.getElementById('nresol').focus();document.getElementById('nresol').select();" onKeyUp="return tabular(event,this)" style="width:100%;" value="<?php echo $_POST['nresol']?>" readonly></td>
								<td class="saludo1">Fecha: </td>
								<td>
									<input name="fecha" type="text" value="<?php echo $_POST['fecha']?>" maxlength="10" onKeyPress="javascript:return solonumeros(event)" onKeyUp="return tabular(event,this)"  id="fc_1198971545" onKeyDown="mascara(this,'/',patron,true)" title="DD/MM/YYYY" readonly>
								</td>
							</tr>
						</table>
						<table class="inicio">
							<tr>
								<td class="titulos" colspan="8">Informaci&oacute;n Predio</td>
							</tr>
							<tr>
								<td width="119" class="saludo1">C&oacute;digo Catastral:</td>
								<td width="202" >
									<input type="hidden" value="<?php echo $_POST['nbanco']?>" name="nbanco">
									<input name="catastral" type="text" id="catastral" onBlur="buscater(event)" onClick="document.getElementById('tercero').focus();document.getElementById('tercero').select();" onKeyUp="return tabular(event,this)" value="<?php echo $_POST['catastral']?>" size="20" readonly>
								</td>
								<td width="82" class="saludo1">Avaluo:</td>
								<td colspan="5">
									<input name="avaluo" type="text" id="avaluo" onKeyUp="return tabular(event,this)" value="<?php echo $_POST['avaluo']?>" size="20" readonly>
								</td>
							</tr>
							<tr>
								<td width="82" class="saludo1">Documento:</td>
								<td>
									<input name="documento" type="text" id="documento" onKeyUp="return tabular(event,this)" value="<?php echo $_POST['documento']?>" size="20" readonly>
								</td>
								<td width="119" class="saludo1">Propietario:</td>
								<td width="202" >
									<input type="hidden" value="<?php echo $_POST['nbanco']?>" name="nbanco">
									<input name="propietario" type="text" id="propietario" onKeyUp="return tabular(event,this)" value="<?php echo $_POST['propietario']?>" size="40" readonly>
								</td>
							</tr>
							<tr>
								<td width="119" class="saludo1">Direcci&oacute;n:</td>
								<td width="202" >
									<input type="hidden" value="<?php echo $_POST['nbanco']?>" name="nbanco">
									<input name="direccion" type="text" id="direccion" onKeyUp="return tabular(event,this)" value="<?php echo $_POST['direccion']?>" size="40" readonly>
								</td>
								<td width="82" class="saludo1">Ha:</td>
								<td >
									<input name="ha" type="text" id="ha" onKeyUp="return tabular(event,this)" value="<?php echo $_POST['ha']?>" size="6" readonly>
								</td>
								<td  class="saludo1">Mt2:</td>
								<td width="144">
									<input name="mt2" type="text" id="mt2" onKeyUp="return tabular(event,this)" value="<?php echo $_POST['mt2']?>" size="6" readonly>
								</td>
								<td class="saludo1">Area Cons:</td>
								<td width="206"><input name="areac" type="text" id="areac" onKeyUp="return tabular(event,this)" value="<?php echo $_POST['areac']?>" size="6" readonly></td>
							</tr>
							<tr>
								<td width="119" class="saludo1">Tipo:</td>
								<td width="202">
									<select name="tipop" onChange="validar();" disabled>
										<option value="">Seleccione ...</option>
										<option value="urbano" <?php if($_POST['tipop']=='urbano') echo "SELECTED"?>>Urbano</option>
										<option value="rural" <?php if($_POST['tipop']=='rural') echo "SELECTED"?>>Rural</option>
									</select>
								</td>
								<?php
								if($_POST['tipop']=='urbano')
								{
									?>
									<td class="saludo1">Estratos:</td>
									<td>
										<select name="estrato" disabled>
											<option value="">Seleccione ...</option>
											<?php
											$sqlr = "select *from estratos where estado='S'";
											$res = mysqli_query($linkbd, $sqlr);
											while ($row = mysqli_fetch_row($res))
											{
												echo "<option value=$row[0] ";
												$i=$row[0];
												if($i==$_POST['estrato'])
												{
													echo "SELECTED";
													$_POST['nestrato']=$row[1];
												}
												echo ">".$row[1]."</option>";
											}
											?>
										</select>
										<input type="hidden" value="<?php echo $_POST['nestrato']?>" name="nestrato">
									</td>
									<?php
								}
								else
								{
									?>
									<td class="saludo1">Rango Avaluo:</td>
									<td>
										<select name="rangos" disabled>
											<option value="">Seleccione ...</option>
											<?php
												$sqlr="select *from rangoavaluos where estado='S'";
												$res=mysqli_query($linkbd, $sqlr);
												while ($row =mysqli_fetch_row($res))
												{
													echo "<option value=$row[0] ";
													$i=$row[0];
													if($i==$_POST['rangos'])
													{
														echo "SELECTED";
														$_POST['nrango']=$row[1]." - ".$row[2]." SMMLV";
													}
													echo ">Entre ".$row[1]." - ".$row[2]." SMMLV</option>";
												}
											?>
										</select>
										<input type="hidden" value="<?php echo $_POST['nrango']?>" name="nrango">
										<input type="hidden" value="0" name="agregadet">
									</td>
									<?php
								}
								?>
							</tr>
						</table>
    					<div class="subpantallac4">
							<table  class="inicio" >
								<tr>
									<td colspan="12" class="titulos">.: Detalles</td>
								</tr>
								<tr>
									<td class="titulos2">Vigencia</td>
									<td class="titulos2">Avaluo</td>
									<td class="titulos2"> Fecha Exonerada </td>
								</tr>
								<?php
								$sqlr="Select * from tesoexoneracion_det where cedulacatastral='$_POST[codcat]'";
								$res=mysqli_query($linkbd, $sqlr);
								while($r=mysqli_fetch_row($res))
								{
									$sqlrAvaluo="SELECT avaluo from tesoprediosavaluos where codigocatastral=$_POST[codcat] AND vigencia='$r[4]'";
									$resAvaluo=mysqli_query($linkbd, $sqlrAvaluo);
									$rowAvaluo=mysqli_fetch_row($resAvaluo);
									echo "<tr>
											<td class='saludo1'>
												<input type='hidden' name='pvigencias[]' id='pvigencias[]' value='".$r[4]."'>
												$r[4]
											</td>
											<td class='saludo1'>
											<input type='hidden' name='pavaluo[]' id='pavaluo[]' value='".$rowAvaluo[0]."'>
												".number_format($rowAvaluo[0],2)."
											</td>
											<td class='saludo1'>
												$r[1]
											</td>

										</tr>";
								}
								?>
							</table>
    					</div>
					</form>
			</table>
		</body>
</html>
