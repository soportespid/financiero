<?php
	header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=utf8");
	require "comun.inc";
	require "funciones.inc";
	session_start();
	date_default_timezone_set("America/Bogota");
?>
<!DOCTYPE >
<html lang="es">
	<head>
		<meta charset="utf-8"/>
		<meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1"/>
		<meta name="viewport" content="user-scalable=no">
		<title>:: IDEAL 10 - Planeación estrategica</title>
		<link href="favicon.ico" rel="shortcut icon"/>
        <link href="css/css2z.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
		<link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
		<link href="css/css4.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
        <link href="css/tabs.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css"/>
        <link href="css/style.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css"/>
        <script type="text/javascript" src="css/programas.js"></script>
	</head>
	<body>
		<IFRAME src="alertas.php" name="alertas" id="alertas" style="display:none"></IFRAME>
		
		<table>
    		<tr><script>barra_imagenes("plan");</script><?php cuadro_titulos();?></tr>
    		<tr><?php menu_desplegable("plan");?></tr>
     	</table>

        <div class="bg-white group-btn p-1" id="newNavStyle">
			<button type="button" class="btn btn-white btn-primary-hover d-flex justify-between align-items-center" onClick="mypop=window.open('plan-principal.php','','');mypop.focus();">
				<span>Nueva ventana</span>
				<svg class="fill-black group-hover:fill-white w-5 h-5" viewBox="0 -960 960 960" ><path d="M320-120v-80h80v-80H160q-33 0-56.5-23.5T80-360v-400q0-33 23.5-56.5T160-840h640q33 0 56.5 23.5T880-760v400q0 33-23.5 56.5T800-280H560v80h80v80H320ZM160-360h640v-400H160v400Zm0 0v-400 400Z"/></svg>
			</button>
        </div>

		<form name="form2" method="post" action="">
    		<table class="inicio">
     			<tr>
        			<td class="titulos" colspan="1">Menú de plan plurianual de inversiones</td>
        			<td class="cerrar" style="width:7%;"><a href="plan-principal.php">&nbsp;Cerrar</a></td>
      			</tr>

    			<tr>
					<td>
						<ol id="lista2">
							<li onclick="location.href='plan-buscaPlurianual'" style="cursor:pointer">Plan plurianual de inversiones</li>
                            <li onclick="location.href='plan-carguePlurianual'" style="cursor:pointer">Cargue masivo de plan plurianual</li>
						</ol>
					</td>
				</tr>
    		</table>
		</form>
	</body>
</html>