<?php 
	header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=utf8");
	require "comun.inc";
	require "funciones.inc";
	require "validaciones.inc";
	//require "head.php";
	session_start();
	$linkbd=conectar_v7();
	$linkbd -> set_charset("utf8");
	cargarcodigopag($_GET['codpag'],$_SESSION["nivel"]);
	date_default_timezone_set("America/Bogota");
?>
<!DOCTYPE >
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="es">
	<head>
		<meta http-equiv="Content-type" content="text/html; charset=utf8"/>
		<meta http-equiv="X-UA-Compatible" content="IE=9"/>
		<title>:: IDEAL 10 </title>
		<link href="css/css2.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css"/>
		<link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css"/>
		<link href="css/css4.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css"/>
		<script type="text/javascript" src="css/calendario.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script type="text/javascript" src="css/programas.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<?php titlepag();?>
		<script>
			function posrp(numrp)
			{
				parent.document.form2.rp.value = numrp;
				parent.buscarp();
				parent.despliegamodal2("hidden");
			}
			function buscarbotonfiltro()
			{
				if((document.form2.fecha1.value != "" && document.form2.fecha2.value == "") || (document.form2.fecha1.value == "" && document.form2.fecha2.value != ""))
				{
					alert("Falta digitar fecha");
				}
				else
				{
					document.getElementById('numpos').value=0;
					document.getElementById('nummul').value=0;
					document.form2.submit();
				}
			}
			function callprogress(vValor)
			{
				document.getElementById("getprogress").innerHTML = vValor+" %";
				document.getElementById("getProgressBarFill").innerHTML = '<div class="ProgressBarFill" style="width: '+vValor+'%;"></div>';
				document.getElementById("progreso").style.display='block';
				document.getElementById("getProgressBarFill").style.display='block';
			}
		</script>
	</head>
	<body>
		<form name="form2" method="post">
			<?php
				if($_POST['oculto']=="")
				{
					$_POST['numpos'] = 0;
					$_POST['numres'] = 10;
					$_POST['nummul'] = 0;
					$_POST['vigenciatem'] = $_GET['vigencia'];
				}
			?>
			<table class="inicio ancho" style="width:99.4%;" >
				<tr>
					<td class="titulos" colspan="6" >:: Buscar RP</td>
					<td class="cerrar" style="width:7%" onClick="parent.despliegamodal2('hidden');">Cerrar</td>
				</tr>
				<tr>
					<td class="tamano01" style="width:2.5cm;">N&uacute;mero o nombre:</td>
					<td colspan="5"><input type="text" name="nombre" id="nombre" value="<?php echo $_POST['nombre'];?>" style="width:100%;"/></td>
					<td></td>
				</tr>
				<tr>
					<td class="tamano01">Fecha inicial:</td>
					<td><input name="fecha1" type="text" value="<?php echo $_POST['fecha1']?>" maxlength="8"  onKeyPress="javascript:return solonumeros(event)" onKeyUp="return tabular(event,this)"  id="fc_1198971545" onKeyDown="mascara(this,'/',patron,true)" style="width:75%;height:30px;" title="DD/MM/YYYY" placeholder="DD/MM/YYYY"/>&nbsp;<img src="imagenes/calendario04.png" onClick="displayCalendarFor('fc_1198971545');" title="Calendario" class="icobut"/></td>
					<td class="tamano01">Fecha final:</td>
					<td><input name="fecha2" type="text" value="<?php echo $_POST['fecha2']?>" maxlength="8"  onKeyPress="javascript:return solonumeros(event)" onKeyUp="return tabular(event,this)"  id="fc_1198971546" onKeyDown="mascara(this,'/',patron,true)" style="width:75%;height:30px;" title="DD/MM/YYYY" placeholder="DD/MM/YYYY"/>&nbsp;<img src="imagenes/calendario04.png" onClick="displayCalendarFor('fc_1198971546');" title="Calendario" class="icobut"/></td>
					<td style="padding-bottom:0px"><em class="botonflecha" onClick="buscarbotonfiltro();">Buscar</em></td>
					<td></td>
				</tr>
			</table>
			<input type="hidden" name="oculto" id="oculto" value="1"/>
			<input type="hidden" name="vigenciatem" id="vigenciatem" value="<?php echo $_POST['vigenciatem'];?>"/>
			<input type="hidden" name="numres" id="numres" value="<?php echo $_POST['numres'];?>"/>
			<input type="hidden" name="numpos" id="numpos" value="<?php echo $_POST['numpos'];?>"/>
			<input type="hidden" name="nummul" id="nummul" value="<?php echo $_POST['nummul'];?>"/>
			<div class="subpantalla" style="height:77.5%; width:99%; overflow-x:hidden;">
				<?php
					preg_match("/([0-9]{2})\/([0-9]{2})\/([0-9]{4})/", $_POST['fecha1'],$fecha);
					$fechai="$fecha[3]-$fecha[2]-$fecha[1]";
					preg_match("/([0-9]{2})\/([0-9]{2})\/([0-9]{4})/", $_POST['fecha2'],$fecha);
					$fechaf="$fecha[3]-$fecha[2]-$fecha[1]";
					$crit1=" ";
					if ($_POST['nombre']!=""){$crit1="AND concat_ws(' ', rp.consvigencia, rp.detalle) LIKE '%".$_POST['nombre']."%'";}
					$sqlr="
					SELECT rp.consvigencia, rp.detalle, rp.vigencia, rp.fecha FROM ccpetrp AS rp
					LEFT JOIN ccpetdc AS dc
					ON rp.consvigencia = dc.consvigencia AND rp.vigenciacdp = dc.vigencia
					WHERE rp.vigenciacdp = '".$_POST['vigenciatem']."' AND rp.estado='S' AND dc.consvigencia IS null AND dc.vigencia IS null $crit1";
					if(isset($_POST['fecha1']) && isset($_POST['fecha2']))
					{
						if(!empty($_POST['fecha1']) && !empty($_POST['fecha2']))
						{
							$sqlr="
							SELECT rp.consvigencia, rp.detalle, rp.vigencia, rp.fecha FROM ccpetrp AS rp
							LEFT JOIN ccpetdc AS dc
							ON rp.consvigencia = dc.consvigencia AND rp.vigenciacdp = dc.vigencia
							WHERE rp.vigenciacdp = '".$_POST['vigenciatem']."' AND rp.estado='S' AND dc.consvigencia IS null AND dc.vigencia IS null $crit1 AND rp.fecha BETWEEN '$fechai' AND '$fechaf' ORDER BY rp.consvigencia ASC";
						}
					}
					$resp = mysqli_query($linkbd,$sqlr);
					$tresultados = mysqli_num_rows($resp);
					$con=1;
					echo "
					<table class='inicio' align='center' width='99%'>
						<tr class='titulos'>
							<td id='ttresul' colspan='2'>.: Resultados Busqueda: $tresultados</td>
							<td  style='width:30'>
								<div id='progreso' class='ProgressBar' style='display:none; float:left'>
									<div class='ProgressBarText'><span id='getprogress'></span></div>
									<div id='getProgressBarFill'></div>
								</div>
							</td>
							<td colspan='2'></td>
						</tr>
						<tr>
							<td class='titulos2' style='width:40'><center>N&deg; RP</center></td>
							<td class='titulos2' colspan='2'><center>DETALLE</center></td>
							<td class='titulos2' ><center>VIGENCIA</center></td>
							<td class='titulos2' style='width:80' ><center>FECHA</center></td>
						</tr>";
					$iter='saludo1a';
					$iter2='saludo2';
					$c=0;
					while ($row =mysqli_fetch_row($resp))
					{
						$c++;
						$saldoRP = generaSaldoRPccpet($row[0],$row[2]);
						/*$fsaldo = "select saldorp ('$row[0]','$row[2]')";
						$rsaldo = mysqli_query($linkbd,$fsaldo);
						$rowsal = mysqli_fetch_array($rsaldo);
						$saldoRP = round($rowsal[0],2);*/
						if($saldoRP!=0)
						{
							$con2=$con+ $_POST['numpos'];
							echo "
							<tr class='$iter' onClick=\"javascript:posrp('$row[0]')\">
								<td>$row[0]</td>
								<td colspan='2'>".strtoupper($row[1])."</td>
								<td>$row[2]</td>
								<td>$row[3]</td>
							</tr>";
							$con+=1;
							$aux=$iter;
							$iter=$iter2;
							$iter2=$aux;
						}
						$porcentaje = $c * 100 / $tresultados;
						echo"
						<script>
							progres='".round($porcentaje)."';callprogress(progres);
							document.getElementById('ttresul').innerHTML='.: Resultados Busqueda: $c / $tresultados';
						</script>";
						flush();
						ob_flush();
						usleep(1);
					}
					echo"
					</table>
					<script>document.getElementById('getprogress').innerHTML='';</script>";
				?>
			</div>
		</form>
	</body>
</html>
