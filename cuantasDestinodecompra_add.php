<?php
	header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=utf8");
	require "comun.inc";
	require "funciones.inc";
	require "validaciones.inc";
	session_start();
	$linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");
	cargarcodigopag($_GET['codpag'],$_SESSION["nivel"]);
	date_default_timezone_set("America/Bogota");
?>
<!doctype html>
<html lang="es">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=9">
		<title>:: Ideal</title>
		<link href="css/css2.css" rel="stylesheet" type="text/css">
		<link href="css/css3.css" rel="stylesheet" type="text/css">
		<script type="text/javascript" src="css/programas.js"></script>
		<script type="text/javascript" src="css/calendario.js"></script>
		<script> 
			function ponprefijo(cuentaDebito, cuentaCredito) {
				index = document.form2.posicion.value;
				parent.document.querySelectorAll("input[name='cuentaDebito[]']")[index].value = cuentaDebito;
				parent.document.querySelectorAll("input[name='cuentaCredito[]']")[index].value = cuentaCredito;
				parent.despliegamodal2('hidden');
			}
		</script>
		<?php titlepag();?>
	</head>
	<body>
		<form name="form2" action="" method="post">
			<?php
				if ($_POST['oculto'] == ''){
					$_POST['num_id'] = $_GET['id'];
					$_POST['posicion'] = $_GET['pos'];
					$_POST['cuentaDebito'] = $_GET['cuentadebito'];
					$_POST['cuentaCredito'] = $_GET['cuentacredito'];
				}
			?>
			<table class="inicio" style="width:99.4%;">
				<tr>
					<td class="titulos" colspan="4">:: Buscar Cuenta contable</td>
					<td class="cerrar" style="width:7%" onClick="parent.despliegamodal2('hidden');">Cerrar</td>
				</tr>
			</table>
			<input type="hidden" name="num_id" id="num_id" value="<?php echo $_POST['num_id']?>">
			<input type="hidden" name="posicion" id="posicion" value="<?php echo $_POST['posicion']?>">
			<input type="hidden" name="cuentaDebito" id="cuentaDebito" value="<?php echo $_POST['cuentaDebito']?>">
			<input type="hidden" name="cuentaCredito" id="cuentaCredito" value="<?php echo $_POST['cuentaCredito']?>">
			<div class="subpantalla" style="height:420px !important; width:99%; overflow-x:hidden;">
				<?php
					$iter = 'saludo1a';
					$iter2 = 'saludo2';
					$con = 2;
					$sqladd = "SELECT cuenta_debito, cuenta_credito FROM ccpetdc_cuentasadd WHERE id_detalle = '".$_POST['num_id']."'";
					$resadd = mysqli_query($linkbd, $sqladd);
					$nombreCuentaContable = buscacuenta($_POST['cuentaDebito']);
					echo "
					<table class='inicio' align='center' style='width:99%;'>
						<tr>
							<td class='titulos2' style='width:5%'>Item</td>
							<td class='titulos2' style='width:20%'>Cuenta Debito</td>
							<td class='titulos2'>Nombre Cuenta</td>
							<td class='titulos2' style='width:20%'>Cuenta Credito</td>
						</tr>
						<tr class='$iter' onClick=\"javascript:ponprefijo('".$_POST['cuentaDebito']."','".$_POST['cuentaCredito']."')\">
							<td>1</td>
							<td>".$_POST['cuentaDebito']."</td>
							<td>$nombreCuentaContable</td>
							<td>".$_POST['cuentaCredito']."</td>
						</tr>
						";
						while ($rowadd = mysqli_fetch_row($resadd)){
							$nombreCuentaContable = buscacuenta($rowadd[0]);
							echo"
							<tr class='$iter' onClick=\"javascript:ponprefijo('$rowadd[0]','$rowadd[1]')\" style='text-transform:uppercase'>
								<td>$con</td>
								<td>$rowadd[0]</td>
								<td>$nombreCuentaContable</td>
								<td>$rowadd[1]</td>
							</tr>";
							$con++;
							$aux = $iter;
							$iter = $iter2;
							$iter2 = $aux;
						} 
						echo"</table>";
				?>
			</div>
		</form>
	</body>
</html>
