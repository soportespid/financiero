<?php
	header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=utf8");

	require "comun.inc";
	require "funciones.inc";
	require "conversor.php";
	require "validaciones.inc";

	$linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");

	session_start();
	date_default_timezone_set("America/Bogota");
?>
<!DOCTYPE >
<html lang="es">
	<head>
		<meta charset="utf-8"/>
		<meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1"/>
		<meta name="viewport" content="user-scalable=no">
		<title>:: IDEAL 10 - Informes</title>
		<link href="favicon.ico" rel="shortcut icon"/>
		<link href="css/css2.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css">
		<link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css">
		<script type="text/javascript" src="css/programas.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script type='text/javascript' src='JQuery/jquery-2.1.4.min.js?<?php echo date('d_m_Y_h_i_s');?>'></script>
	</head>
	<body>
		<IFRAME src="alertas.php" name="alertas" id="alertas" style="display:none"></IFRAME>
		<span id="todastablas2"></span>
		<table>
			<tr><script>barra_imagenes("info");</script><?php cuadro_titulos();?></tr>
			<tr><?php menu_desplegable("info");?></tr>
			<tr>
				<td colspan="3" class="cinta"><img src="imagenes/add2.png" class="mgbt1"><img src="imagenes/guardad.png" style="width:24px;" class="mgbt1"><img src="imagenes/buscad.png" class="mgbt1"><img src="imagenes/nv.png" title="Nueva Ventana" onClick="mypop=window.open('info-principal.php','','');mypop.focus();" class="mgbt"></td>
			</tr>
		</table>
		<form name="form2" method="post" action="" >
			<div style="margin: 0px 10px 10px; border-radius: 0 0 0 5px; height: 500px; overflow: scroll; overflow-x: hidden; resize: vertical">
				<table class="inicio">
					<tr>
						<td class="titulos" colspan="2">.:  Reportes DIARI (Dirección de información, análisis y reacción inmediata) - APPUI </td>
						<td class="cerrar" style="width:7%;" ><a href="info-principal.php">&nbsp;Cerrar</a></td>
					</tr>
					<td style="background-repeat:no-repeat; background-position:center;">
						<ol id="lista2">
                            <li onClick="location.href='ccp-homologar_fuentes_diari.php'" style="cursor:pointer;">Homologar fuentes DIARI</li>
                            <li onClick="location.href='ccp-informeCDPDiari.php'" style="cursor:pointer;">Relación de CDP</li>
                            <li onClick="location.href='ccp-informeRpDiari.php'" style="cursor:pointer;">Relación de compromisos</li>
                            <li onClick="location.href='ccp-informeObligacionesDiari.php'" style="cursor:pointer;">Relación de obligaciones</li>
                            <li onClick="location.href='ccp-informePagosDiari.php'" style="cursor:pointer;">Relación de pagos</li>
                            <li onClick="location.href='ccp-informeIngresosDiari.php'" style="cursor:pointer;">Relación de ingresos</li>
						</ol>
					</td>
				</table>
			</div>
		</form>
	</body>
</html>
