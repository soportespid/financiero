<?php
	header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=utf8");
	require 'comun.inc';
	require 'funciones.inc';
	session_start();
	session_destroy();
	date_default_timezone_set("America/Bogota");
    header("location: index2.php");
?>
