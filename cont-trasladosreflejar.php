<?php
header("Cache-control: no-cache, no-store, must-revalidate");
header("Content-Type: text/html;charset=utf8");

require "comun.inc";
require "funciones.inc";
require "conversor.php";
require "validaciones.inc";

$linkbd = conectar_v7();
$linkbd->set_charset("utf8");

session_start();
date_default_timezone_set("America/Bogota");
?>
<!DOCTYPE>
<html lang="es">

<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1" />
    <meta name="viewport" content="user-scalable=no">
    <title>:: IDEAL 10 - Contabilidad</title>
    <link href="favicon.ico" rel="shortcut icon" />
    <link href="css/css2.css?<?php echo date('d_m_Y_h_i_s'); ?>" rel="stylesheet" type="text/css">
    <link href="css/css3.css?<?php echo date('d_m_Y_h_i_s'); ?>" rel="stylesheet" type="text/css">
    <link href="css/css4.css?<?php echo date('d_m_Y_h_i_s'); ?>" rel="stylesheet" type="text/css">

    <link href="css/style.css?<?php echo date('d_m_Y_h_i_s'); ?>" rel="stylesheet" type="text/css">
    <link href="sweetalert2/dist/sweetalert2.min.css" rel="stylesheet">
    <script src="sweetalert2/dist/sweetalert2.min.js"></script>
    <script type="text/javascript" src="css/calendario.js?<?php echo date('d_m_Y_h_i_s'); ?>"></script>
    <script type="text/javascript" src="css/programas.js?<?php echo date('d_m_Y_h_i_s'); ?>"></script>
    <script>
        function buscacta(e) {
            if (document.form2.cuenta.value != "") {
                document.form2.bc.value = '1';
                document.form2.submit();
            }
        }
        function validar() {
            document.form2.submit();
        }
        function buscater(e) {
            if (document.form2.tercero.value != "") {
                document.form2.bt.value = '1';
                document.form2.submit();
            }
        }
        function despliegamodalm(_valor, _tip, mensa, pregunta) {
            document.getElementById("bgventanamodalm").style.visibility = _valor;
            if (_valor == "hidden") { document.getElementById('ventanam').src = ""; }
            else {
                switch (_tip) {
                    case "1": document.getElementById('ventanam').src = "ventana-mensaje1.php?titulos=" + mensa; break;
                    case "2": document.getElementById('ventanam').src = "ventana-mensaje3.php?titulos=" + mensa; break;
                    case "3": document.getElementById('ventanam').src = "ventana-mensaje2.php?titulos=" + mensa; break;
                    case "4": document.getElementById('ventanam').src = "ventana-consulta1.php?titulos=" + mensa + "&idresp=" + pregunta; break;
                }
            }
        }
        function funcionmensaje() {
            var codig = document.form2.idcomp.value;
            location.href = "cont-trasladosreflejar.php?idr=" + codig;
        }
        function respuestaconsulta(pregunta) {
            switch (pregunta) {
                case "1":
                    document.form2.oculto.value = 2;
                    document.form2.submit();
                    break;
            }
        }
        function atrasc(scrtop, numpag, limreg, filtro, numcelt, feini, fefin, filnum) {
            var codig = document.form2.idcomp.value;
            var minim = document.form2.minimo.value;
            codig = parseFloat(codig) - 1;
            if (codig >= minim) {
                location.href = "cont-trasladosreflejar.php?idr=" + codig;
            }
        }
        function adelante(scrtop, numpag, limreg, filtro, numcelt, feini, fefin, filnum) {
            var codig = document.form2.idcomp.value;
            var maxim = document.form2.maximo.value;
            codig = parseFloat(codig) + 1;
            if (codig <= maxim) {
                location.href = "cont-trasladosreflejar.php?idr=" + codig;
            }
        }
        function cargarnuevo() {
            var codig = document.form2.idcomp.value;
            location.href = "cont-trasladosreflejar.php?idr=" + codig;
        }
        function guardar() {
            Swal.fire({
                icon: 'question',
                title: '¿Seguro que quieres reflejar la información?',
                showDenyButton: true,
                confirmButtonText: 'Guardar',
                confirmButtonColor: '#01CC42',
                denyButtonText: 'Cancelar',
                denyButtonColor: '#FF121A',
            }).then(
                (result) => {
                    if (result.isConfirmed) {
                        document.form2.oculto.value = 2;
                        document.form2.submit();
                    }
                    else if (result.isDenied) {
                        Swal.fire({
                            icon: 'info',
                            title: 'No se reflejo la información',
                            confirmButtonText: 'Continuar',
                            confirmButtonColor: '#FF121A',
                            timer: 2500
                        });
                    }
                }
            )
        }
    </script>
</head>

<body>
    <IFRAME src="alertas.php" name="alertas" id="alertas" style="display:none"></IFRAME>
    <span id="todastablas2"></span>
    <table>
        <tr>
            <script>barra_imagenes("cont");</script><?php cuadro_titulos(); ?>
        </tr>
        <tr><?php menu_desplegable("cont"); ?></tr>
    </table>
    <div class="bg-white group-btn p-1"><button type="button" onclick="window.location.href=''"
            class="btn btn-white btn-primary-hover d-flex justify-between align-items-center">
            <span>Nuevo</span>
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 -960 960 960">
                <path d="M440-440H200v-80h240v-240h80v240h240v80H520v240h-80v-240Z"></path>
            </svg>
        </button><button type="button" onclick="window.location.href=''"
            class="btn btn-white btn-primary-hover d-flex justify-between align-items-center">
            <span>Buscar</span>
            <svg class="fill-black group-hover:fill-white w-5 h-5" xmlns="http://www.w3.org/2000/svg"
                viewBox="0 -960 960 960">
                <path
                    d="M784-120 532-372q-30 24-69 38t-83 14q-109 0-184.5-75.5T120-580q0-109 75.5-184.5T380-840q109 0 184.5 75.5T640-580q0 44-14 83t-38 69l252 252-56 56ZM380-400q75 0 127.5-52.5T560-580q0-75-52.5-127.5T380-760q-75 0-127.5 52.5T200-580q0 75 52.5 127.5T380-400Z">
                </path>
            </svg>
        </button><button type="button" onclick="mypop=window.open('cont-principal.php','','');mypop.focus();"
            class="btn btn-white btn-primary-hover d-flex justify-between align-items-center">
            <span>Nueva ventana</span>
            <svg class="fill-black group-hover:fill-white w-5 h-5" xmlns="http://www.w3.org/2000/svg"
                viewBox="0 -960 960 960">
                <path
                    d="M320-120v-80h80v-80H160q-33 0-56.5-23.5T80-360v-400q0-33 23.5-56.5T160-840h640q33 0 56.5 23.5T880-760v400q0 33-23.5 56.5T800-280H560v80h80v80H320ZM160-360h640v-400H160v400Zm0 0v-400 400Z">
                </path>
            </svg>
        </button><button type="button"
            onclick="mypop=window.open('/financiero/cont-trasladosreflejar.php','','');mypop.focus();"
            class="btn btn-white btn-primary-hover d-flex justify-between align-items-center">
            <span class="group-hover:text-white">Duplicar pantalla</span>
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 -960 960 960">
                <path
                    d="M640-160v-360H160v360h480Zm80-200v-80h80v-360H320v200h-80v-200q0-33 23.5-56.5T320-880h480q33 0 56.5 23.5T880-800v360q0 33-23.5 56.5T800-360h-80ZM160-80q-33 0-56.5-23.5T80-160v-360q0-33 23.5-56.5T160-600h480q33 0 56.5 23.5T720-520v360q0 33-23.5 56.5T640-80H160Zm400-603ZM400-340Z">
                </path>
            </svg>
        </button><button type="button" onclick="guardar()"
            class="btn btn-white btn-primary-hover d-flex justify-between align-items-center">
            <span>Reflejar</span>
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 -960 960 960">
                <path
                    d="M400-280h160v-80H400v80Zm0-160h280v-80H400v80ZM280-600h400v-80H280v80Zm200 120ZM265-80q-79 0-134.5-55.5T75-270q0-57 29.5-102t77.5-68H80v-80h240v240h-80v-97q-37 8-61 38t-24 69q0 46 32.5 78t77.5 32v80Zm135-40v-80h360v-560H200v160h-80v-160q0-33 23.5-56.5T200-840h560q33 0 56.5 23.5T840-760v560q0 33-23.5 56.5T760-120H400Z">
                </path>
            </svg>
        </button><button type="button" onclick="location.href='cont-reflejardocsccpet.php'"
            class="btn btn-success btn-success-hover d-flex justify-between align-items-center">
            <span>Atras</span>
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 -960 960 960">
                <path
                    d="M760-200v-160q0-50-35-85t-85-35H273l144 144-57 56-240-240 240-240 57 56-144 144h367q83 0 141.5 58.5T840-360v160h-80Z">
                </path>
            </svg>
        </button></div>
    <div id="bgventanamodalm" class="bgventanamodalm">
        <div id="ventanamodalm" class="ventanamodalm">
            <IFRAME src="" name="ventanam" marginWidth=0 marginHeight=0 frameBorder=0 id="ventanam" frameSpacing=0
                style=" width:700px; height:130px; top:200; overflow:hidden;"></IFRAME>
        </div>
    </div>
    <?php
    if (!$_POST['oculto']) {
        $sqlr = "SELECT cuentatraslado FROM tesoparametros WHERE estado = 'S'";
        $res = mysqli_query($linkbd, $sqlr);
        while ($row = mysqli_fetch_row($res)) {
            $_POST['cuentatraslado'] = $row[0];
        }
        $sqlr = "SELECT MAX(id_consignacion), MIN(id_consignacion) FROM tesotraslados_cab";
        $res = mysqli_query($linkbd, $sqlr);
        $row = mysqli_fetch_row($res);
        $_POST['maximo'] = $row[0];
        $_POST['minimo'] = $row[1];
        if ($_GET['idr'] == '') {
            $_POST['idcomp'] = $_POST['maximo'];
        } else {
            $_POST['idcomp'] = $_GET['idr'];
        }

        $sqltt = "SELECT codigo FROM redglobal WHERE tipo = 'IN'";
        $restt = mysqli_query($linkbd, $sqltt);
        $rowtt = mysqli_fetch_row($restt);
        $_POST['unilocal'] = $rowtt[0];
        $sqlr = "SELECT fecha, concepto, origen, estado FROM tesotraslados_cab WHERE id_consignacion = '" . $_POST['idcomp'] . "'";
        $res = mysqli_query($linkbd, $sqlr);
        $row = mysqli_fetch_row($res);
        $_POST['fecha'] = date('d/m/Y', strtotime($row[0]));
        $_POST['concepto'] = $row[1];
        $_POST['dorigent'] = $row[2];
        switch ($row[3]) {
            case 'S':
                $_POST['estadoc'] = 'ACTIVO';
                $color = " style='background-color:#0CD02A ;color:#fff'";
                $_POST['estadonew'] = 1;
                break;
            case 'N':
                $_POST['estadoc'] = 'ANULADO';
                $color = " style='background-color:#aa0000 ; color:#fff'";
                $_POST['estadonew'] = 0;
                break;
            case 'R':
                $_POST['estadoc'] = 'REVERSADO';
                $color = " style='background-color:#aa0000 ; color:#fff'";
                $_POST['estadonew'] = 1;
                break;
        }
        $sqlorigen = "SELECT base, nombre, usuario FROM redglobal WHERE codigo = '$row[2]'";
        $resorigen = mysqli_query($linkbd, $sqlorigen);
        $roworigen = mysqli_fetch_row($resorigen);
        $_POST['baseorigen'] = $roworigen[0];
        $_POST['norigen'] = $roworigen[1];
        $_POST['usuariobase'] = $roworigen[2];
        $linkmulti = conectar_Multi($_POST['baseorigen'], $_POST['usuariobase']);
        $linkmulti->set_charset("utf8");
        $sqlr = "SELECT tipo_traslado, destino_ext, ntransaccion, cco, ncuentaban1, tercero1, ccd, ncuentaban2, tercero2, valor FROM tesotraslados WHERE id_trasladocab = '" . $_POST['idcomp'] . "'";
        $res = mysqli_query($linkbd, $sqlr);
        while ($row = mysqli_fetch_row($res)) {
            $_POST['dtipotraslado'][] = $row[0];
            $_POST['ddestino'][] = $row[1];
            $sqldest = "SELECT nombre, base FROM redglobal WHERE codigo = '$row[1]'";
            $resdest = mysqli_query($linkbd, $sqldest);
            $rowdest = mysqli_fetch_row($resdest);
            $_POST['dndestino'][] = $rowdest[0];
            $_POST['dbase'][] = $rowdest[1];
            $_POST['dconsig'][] = $row[2];
            $_POST['dccs'][] = $row[3];
            $sqlcc1 = "SELECT nombre FROM centrocosto WHERE estado = 'S' AND id_cc = '$row[3]'";
            $rescc1 = mysqli_query($linkmulti, $sqlcc1);
            $rowcc1 = mysqli_fetch_row($rescc1);
            $_POST['dccnombre'][] = $rowcc1[0];
            $_POST['dcbs'][] = $row[4];
            $sqlnumcuen1 = "SELECT cuenta FROM tesobancosctas WHERE ncuentaban = '$row[4]'";
            $resnumcuen1 = mysqli_query($linkmulti, $sqlnumcuen1);
            $rownumcuen1 = mysqli_fetch_row($resnumcuen1);
            $_POST['dbancos'][] = $rownumcuen1[0];
            $_POST['dcts'][] = $row[5];
            $sqlcuen1 = "SELECT razonsocial FROM terceros AS T2 WHERE T2.cedulanit = '$row[5]'";
            $rescuen1 = mysqli_query($linkmulti, $sqlcuen1);
            $rowcuen1 = mysqli_fetch_row($rescuen1);
            $_POST['dnbancos'][] = $rowcuen1[0];
            $_POST['dccs2'][] = $row[6];
            $sqlcc2 = "SELECT nombre FROM centrocosto WHERE estado = 'S' AND id_cc = '$row[6]'";
            if ($row[1] != '') {
                $rescc2 = mysqli_query($linkmulti, $sqlcc2);
            } else {
                $rescc2 = mysqli_query($linkbd, $sqlcc2);
            }
            $rowcc2 = mysqli_fetch_row($rescc2);
            $_POST['dccnombre2'][] = $rowcc2[0];
            $_POST['dcbs2'][] = $row[7];
            $sqlnumcuen2 = "SELECT cuenta FROM tesobancosctas WHERE ncuentaban = '$row[7]'";
            $resnumcuen2 = mysqli_query($linkmulti, $sqlnumcuen2);
            $rownumcuen2 = mysqli_fetch_row($resnumcuen2);
            $_POST['dbancos2'][] = $rownumcuen2[0];
            $_POST['dcts2'][] = $row[8];
            $sqlcuen2 = "SELECT razonsocial FROM terceros AS T2 WHERE T2.cedulanit = '$row[8]'";
            if ($row[1] != '') {
                $rescuen2 = mysqli_query($linkmulti, $sqlcuen2);
            } else {
                $rescuen2 = mysqli_query($linkbd, $sqlcuen2);
            }
            $rowcuen2 = mysqli_fetch_row($rescuen2);
            $_POST['dnbancos2'][] = $rowcuen2[0];
            $_POST['dvalores'][] = $row[9];

            if ('EXT' == $row[0]) {
                $sqlext = "SELECT codigo, base, nombre, usuario FROM redglobal WHERE codigo = $row[1]";
                $resext = mysqli_query($linkbd, $sqlext);
                $rowext = mysqli_fetch_row($resext);
                $_POST['baseext'] = $rowext[1];
                $_POST['ndestino'] = $rowext[2];
                $_POST['usuariobase'] = $rowext[3];
                echo "
							<input type='hidden' name='ndestino' id='ndestino' value='" . $_POST['ndestino'] . "'>
							<input type='hidden' name='baseext' id='baseext' value='" . $_POST['baseext'] . "'>
							<input type='hidden' name='usuariobase' id='usuariobase' value='" . $_POST['usuariobase'] . "'>";
            }

        }
        if ($_POST['dorigent'] != '') {
            if ($_POST['unilocal'] == $_POST['dorigent']) {

                $_POST['entradaosalidaimg'] = "<img src='imagenes/salida01.gif' style='height:30px;'>";
                $_POST['entradaosalida'] = "SALIDA";
            } else {
                $_POST['entradaosalidaimg'] = "<img src='imagenes/entrada01.gif' style='height:30px;'>";
                $_POST['entradaosalida'] = "ENTRADA";
            }
        } else {
            $_POST['entradaosalidaimg'] = '';
            $_POST['entradaosalida'] = "";
        }
    }
    preg_match("/([0-9]{2})\/([0-9]{2})\/([0-9]{4})/", $_POST['fecha'], $fecha);
    $_POST['vigencia'] = $fecha[3];
    //$linkmulti = conectar_Multi($_POST['baseorigen'], $_POST['usuariobase']);
    //$linkmulti -> set_charset("utf8");
    ?>
    <form name="form2" method="post" action="">
        <table class="inicio ancho">
            <tr>
                <td class="titulos" colspan="6">.: Agregar Traslados</td>
                <td class="cerrar" style="width:7%" onClick="location.href='cont-principal.php'">Cerrar</td>
            </tr>
            <tr>
                <td class="tamano01" style="width:2.5cm;">Numero Comp:</td>
                <td style="width:10%"><img src="imagenes/back.png"
                        onClick="atrasc(<?php echo "'$scrtop','$numpag','$limreg','$filtro','$numcelt','$fechaini', '$fechafin','$filnum'"; ?>)"
                        class="icobut" title="Anterior" />&nbsp;<input type="text" name="idcomp"
                        value="<?php echo $_POST['idcomp'] ?>" style="width:50%;" onchange="cargarnuevo();">&nbsp;<img
                        src="imagenes/next.png"
                        onClick="adelante(<?php echo "'$scrtop','$numpag','$limreg','$filtro', '$numcelt','$fechaini','$fechafin','$filnum'"; ?>)"
                        class="icobut" title="Sigiente" /></td>
                <input type="hidden" name="cuentatraslado" value="<?php echo $_POST['cuentatraslado'] ?>">
                <td class="tamano01" style="width:3cm;">Fecha:</td>
                <td style="width:12%"><input type="text" id="fecha" name="fecha" value="<?php echo $_POST['fecha'] ?>"
                        style="width:98%;" readonly></td>
                <td style="width:12%">
                    <input name="estadoc" type="text" id="estadoc" value="<?php echo $_POST['estadoc'] ?>" <?php echo $color; ?> readonly>
                </td>
                <td><?php echo $_POST['entradaosalidaimg']; ?></td>
            </tr>
            <tr>
                <td class="tamano01">Concepto Traslado:</td>
                <td colspan="5"><input type="text" name="concepto" value="<?php echo $_POST['concepto'] ?>"
                        onKeyUp="return tabular(event,this)" style="width:100%;" readonly></td>
            </tr>
        </table>
        <input type="hidden" name="agregadet" value="0">
        <input type="hidden" name="oculto" value="1">
        <input type="hidden" name="maximo" id="maximo" value="<?php echo $_POST['maximo'] ?>">
        <input type="hidden" name="minimo" id="minimo" value="<?php echo $_POST['minimo'] ?>">
        <input type="hidden" name="vigencia" value="<?php echo $_POST['vigencia']; ?>">
        <input type='hidden' name='dorigent' value="<?php echo $_POST['dorigent']; ?>">
        <input type='hidden' name='norigen' value="<?php echo $_POST['norigen']; ?>">
        <input type='hidden' name='baseorigen' value="<?php echo $_POST['baseorigen']; ?>">
        <input type='hidden' name='unilocal' value="<?php echo $_POST['unilocal']; ?>">
        <input type='hidden' name='estadonew' value="<?php echo $_POST['estadonew']; ?>">
        <input type='hidden' name='entradaosalidaimg' value="<?php echo $_POST['entradaosalidaimg']; ?>">
        <input type='hidden' name='entradaosalida' value="<?php echo $_POST['entradaosalida']; ?>">
        <script>
            document.form2.valor.focus();
            document.form2.valor.select();
        </script>
        <div class="subpantalla">
            <table class="inicio">
                <tr>
                    <td colspan="9" class="titulos">Detalle Traslados</td>
                </tr>
                <tr class="titulos2">
                    <td>Tipo</td>
                    <td>No Transacci&oacute;n</td>
                    <td>Origen</td>
                    <td>CC-Origen</td>
                    <td>Cuenta Bancaria Origen</td>
                    <td>Destino</td>
                    <td>CC-Destino</td>
                    <td>Cuenta Bancaria Destino</td>
                    <td>Valor</td>
                </tr>
                <?php
                $_POST['totalc'] = 0;
                $iter = 'saludo1a';
                $iter2 = 'saludo2';
                for ($x = 0; $x < count($_POST['dtipotraslado']); $x++) {
                    switch ($_POST['dtipotraslado'][$x]) {
                        case "INT":
                            $tipotraslado = 'Interno';
                            $nomorigen = $_POST['dorigent'] . " - " . $_POST['norigen'];
                            $nomdestino = $_POST['dorigent'] . " - " . $_POST['norigen'];
                            ;
                            break;
                        case "EXT":
                            $tipotraslado = 'Externo';
                            $nomorigen = $_POST['dorigent'] . " - " . $_POST['norigen'];
                            $nomdestino = $_POST['ddestino'][$x] . " - " . $_POST['dndestino'][$x];
                            break;
                        case "sdf":
                            $tipotraslado = 'No Definido';
                            $nomorigen = '--';
                            $nomdestino = '--';
                    }
                    echo "
								<input type='hidden' name='dnbancos[]' value='" . $_POST['dnbancos'][$x] . "'>
								<input type='hidden' name='dconsig[]' value='" . $_POST['dconsig'][$x] . "'>
								<input type='hidden' name='dccs[]' value='" . $_POST['dccs'][$x] . "'>
								<input type='hidden' name='dccnombre[]' value='" . $_POST['dccnombre'][$x] . "'>
								<input type='hidden' name='dcbs[]' value='" . $_POST['dcbs'][$x] . "'>
								<input type='hidden' name='dbancos[]' value='" . $_POST['dbancos'][$x] . "'>
								<input type='hidden' name='dcts[]' value='" . $_POST['dcts'][$x] . "'>
								<input type='hidden' name='dccs2[]' value='" . $_POST['dccs2'][$x] . "'>
								<input type='hidden' name='dccnombre2[]' value='" . $_POST['dccnombre2'][$x] . "'>
								<input type='hidden' name='dcts2[]' value='" . $_POST['dcts2'][$x] . "'>
								<input type='hidden' name='dnbancos2[]' value='" . $_POST['dnbancos2'][$x] . "'>
								<input type='hidden' name='dbancos2[]' value='" . $_POST['dbancos2'][$x] . "'>
								<input type='hidden' name='dcbs2[]' value='" . $_POST['dcbs2'][$x] . "'>
								<input type='hidden' name='dvalores[]' value='" . $_POST['dvalores'][$x] . "'>
								<input type='hidden' name='dtipotraslado[]' value='" . $_POST['dtipotraslado'][$x] . "'>
								<input type='hidden' name='ddestino[]' value='" . $_POST['ddestino'][$x] . "'>
								<input type='hidden' name='dndestino[]' value='" . $_POST['dndestino'][$x] . "'>
								<input type='hidden' name='dbase[]' value='" . $_POST['dbase'][$x] . "'>
								<input type='hidden' name='dorigen[]' value='" . $_POST['dorigen'][$x] . "'>
							<tr class='$iter'>
								<td>$tipotraslado</td>
								<td>" . $_POST['dconsig'][$x] . "</td>
								<td>$nomorigen</td>
								<td>" . $_POST['dccs'][$x] . " - " . $_POST['dccnombre'][$x] . "</td>
								<td>" . $_POST['dcbs'][$x] . " - " . $_POST['dnbancos'][$x] . "</td>
								<td>$nomdestino</td>
								<td>" . $_POST['dccs2'][$x] . " - " . $_POST['dccnombre2'][$x] . "</td>
								<td>" . $_POST['dcbs2'][$x] . " - " . $_POST['dnbancos2'][$x] . "</td>
								<td style='text-align:right;'>$ " . number_format($_POST['dvalores'][$x], 0) . "&nbsp;</td>
							</tr>";
                    $_POST['totalc'] = $_POST['totalc'] + $_POST['dvalores'][$x];
                    $_POST['totalcf'] = number_format($_POST['totalc'], 2, ".", ",");
                    $aux = $iter;
                    $iter = $iter2;
                    $iter2 = $aux;
                }
                $resultado = convertir($_POST['totalc']);
                $_POST['letras'] = $resultado . " Pesos";
                echo "
						<input type='hidden' name='totalcf' type='text' value='" . $_POST['totalcf'] . "'>
						<input type='hidden' name='totalc' value='" . $_POST['totalc'] . "'>
						<input type='hidden' name='letras' value='" . $_POST['letras'] . "'>
						<tr>
							<td colspan='6'></td>
							<td class='saludo2'>Total</td>
							<td class='saludo2'>" . $_POST['totalcf'] . "</td>
						</tr>
						<tr><td colspan='5'>Son: " . $_POST['letras'] . "</td></tr>";
                ?>
            </table>
        </div>
        <?php
        if ($_POST['oculto'] == '2') {
            preg_match("/([0-9]{2})\/([0-9]{2})\/([0-9]{4})/", $_POST['fecha'], $fecha);
            $fechaf = "$fecha[3]-$fecha[2]-$fecha[1]";
            $bloq = bloqueos($_SESSION['cedulausu'], $fechaf);
            if ($bloq >= 1) {
                $sqlr = "DELETE FROM comprobante_cab WHERE numerotipo = '" . $_POST['idcomp'] . "' AND tipo_comp = '10'";
                mysqli_query($linkbd, $sqlr);
                $sqlr = "DELETE FROM comprobante_det WHERE  numerotipo = '" . $_POST['idcomp'] . "' AND tipo_comp = '10'";
                mysqli_query($linkbd, $sqlr);
                $consec = 0;
                $consec = $_POST['idcomp'];
                $sqlr = "INSERT INTO comprobante_cab (numerotipo, tipo_comp, fecha, concepto, total, total_debito, total_credito, diferencia, estado) VALUES ('$consec','10', '$fechaf', '" . $_POST['concepto'] . "', '0', '" . $_POST['totalc'] . "', '" . $_POST['totalc'] . "', '0', '" . $_POST['estadonew'] . "')";
                mysqli_query($linkbd, $sqlr);
                $idcomp = mysqli_insert_id($linkbd);
                $idtraslados = mysqli_insert_id($linkbd);
                echo "<input type='hidden' name='ncomp' value='$idcomp'>";
                if ($tipotraslado == 'Interno') {
                    for ($x = 0; $x < count($_POST['dbancos']); $x++) {
                        //**** consignacion  BANCARIA*****
                        $sqlr = "INSERT INTO comprobante_det (id_comp, cuenta, tercero, centrocosto, detalle, cheque, valdebito, valcredito, estado, vigencia) VALUES ('10 $consec', '" . $_POST['dbancos'][$x] . "', '" . $_POST['dcts'][$x] . "', '" . $_POST['dccs'][$x] . "', 'Traslado " . $_POST['dconsig'][$x] . " " . $_POST['dnbancos'][$x] . "', '', 0, " . $_POST['dvalores'][$x] . ", '1', '" . $_POST['vigencia'] . "')";
                        mysqli_query($linkbd, $sqlr);
                        //*** Cuenta CAJA **
                        $sqlr = "INSERT INTO comprobante_det (id_comp, cuenta, tercero, centrocosto, detalle, cheque, valdebito, valcredito, estado, vigencia) VALUES ('10 $consec', '" . $_POST['dbancos2'][$x] . "', '" . $_POST['dcts2'][$x] . "', '" . $_POST['dccs2'][$x] . "', 'Traslado " . $_POST['dconsig'][$x] . " " . $_POST['dnbancos2'][$x] . "', '', " . $_POST['dvalores'][$x] . ", 0, '1', '" . $_POST['vigencia'] . "')";
                        mysqli_query($linkbd, $sqlr);
                        if ($_POST['dccs'][$x] != $_POST['dccs2'][$x]) {
                            //**** consignacion  BANCARIA*****
                            $cuentapatrimonio1 = buscarcuentapatrimonio($_POST['dccs'][$x]);
                            $sqlr = "INSERT INTO comprobante_det (id_comp, cuenta, tercero, centrocosto, detalle, cheque, valdebito, valcredito, estado, vigencia) VALUES ('10 $consec', '$cuentapatrimonio1', '" . $_POST['dcts'][$x] . "', '" . $_POST['dccs'][$x] . "', 'Traslado " . $_POST['dconsig'][$x] . " " . $_POST['dnbancos'][$x] . "', '', " . $_POST['dvalores'][$x] . ", 0, '1', '" . $_POST['vigencia'] . "')";
                            mysqli_query($linkbd, $sqlr);
                            //*** Cuenta CAJA **
                            $cuentapatrimonio2 = buscarcuentapatrimonio($_POST['dccs2'][$x]);
                            $sqlr = "INSERT INTO comprobante_det (id_comp, cuenta, tercero, centrocosto, detalle, cheque, valdebito, valcredito, estado, vigencia) VALUES ('10 $consec', '$cuentapatrimonio2', '" . $_POST['dcts2'][$x] . "', '" . $_POST['dccs2'][$x] . "', 'Traslado " . $_POST['dconsig'][$x] . " " . $_POST['dnbancos2'][$x] . "', '', 0, " . $_POST['dvalores'][$x] . ", '1', '" . $_POST['vigencia'] . "')";
                            mysqli_query($linkbd, $sqlr);
                        }
                    }
                    echo "
							<script>
								Swal.fire({
									icon: 'success',
									title: 'Se ha reflejado los Traslados con Exito',
									confirmButtonText: 'Continuar',
									confirmButtonColor: '#FF121A',
									timer: 3000
								});
							</script>";
                } else {
                    /*$linkmulti = conectar_Multi($_POST['baseext'],'');
                                         $linkmulti -> set_charset("utf8");
                                         $consecsal = selconsecutivomulti('tesotraslados_cab','id_consignacion',$_POST['baseext'],$_POST['usuariobase']);
                                         $idcompsal = selconsecutivomulti('tesotraslados_cab','id_comp',$_POST['baseext'],$_POST['usuariobase']);
                                         $idtrasladossal = $consecsal;

                                         $sqlr = "INSERT INTO comprobante_cab (numerotipo, tipo_comp, fecha, concepto, total, total_debito, total_credito, diferencia, estado) VALUES ('$consecsal','10', '$fechaf', '".$_POST['concepto']."', '0', '".$_POST['totalc']."', '".$_POST['totalc']."', '0', '".$_POST['estadonew']."')";
                                         mysqli_query($linkmulti,$sqlr);
                                         $sqlr = "INSERT INTO tesotraslados_cab (id_comp, fecha, vigencia, estado, concepto, origen) VALUES ('$idcompsal', '$fechaf', '".$_POST['vigencia']."', 'S', '".$_POST['concepto']."','".$_POST['dorigent']."')";
                                         mysqli_query($linkmulti ,$sqlr);*/
                    for ($x = 0; $x < count($_POST['dbancos']); $x++) {
                        if ($_POST['unilocal'] == $_POST['dorigent']) {
                            $debito1 = $credito2 = 0;
                            $credito1 = $debito2 = $_POST['dvalores'][$x];
                        } else {
                            $debito1 = $credito2 = $_POST['dvalores'][$x];
                            $credito1 = $debito2 = 0;
                        }
                        $sqlcuentab = "SELECT cuenta FROM tesobancosctas WHERE ncuentaban = '" . $_POST['dcbs2'][$x] . "'";
                        $rescuentab = mysqli_query($linkbd, $sqlcuentab);
                        $rowcuentab = mysqli_fetch_row($rescuentab);
                        if ($_POST['entradaosalida'] == "SALIDA") {
                            $sqlr = "INSERT INTO comprobante_det (id_comp, cuenta, tercero, centrocosto, detalle, cheque, valdebito, valcredito, estado, vigencia) VALUES ('10 $consec', '" . $_POST['dbancos'][$x] . "', '" . $_POST['dcts'][$x] . "', '" . $_POST['dccs'][$x] . "', 'Traslado " . $_POST['dconsig'][$x] . " " . $_POST['dnbancos'][$x] . "', '',$debito1, $credito1, '1', '" . $_POST['vigencia'] . "')";
                        } else {
                            $sqlr = "INSERT INTO comprobante_det (id_comp, cuenta, tercero, centrocosto, detalle, cheque, valdebito, valcredito, estado, vigencia) VALUES ('10 $consec', '" . $rowcuentab[0] . "', '" . $_POST['dcts'][$x] . "', '" . $_POST['dccs'][$x] . "', 'Traslado " . $_POST['dconsig'][$x] . " " . $_POST['dnbancos'][$x] . "', '',$debito1, $credito1, '1', '" . $_POST['vigencia'] . "')";
                        }
                        mysqli_query($linkbd, $sqlr);
                        $sqlr = "INSERT INTO comprobante_det (id_comp, cuenta, tercero, centrocosto, detalle, cheque, valdebito, valcredito, estado, vigencia) VALUES ('10 $consec', '" . $_POST['cuentatraslado'] . "', '" . $_POST['dcts'][$x] . "', '" . $_POST['dccs'][$x] . "', 'Traslado " . $_POST['dconsig'][$x] . " " . $_POST['dnbancos'][$x] . "', '', $debito2, $credito2, '1', '" . $_POST['vigencia'] . "')";
                        mysqli_query($linkbd, $sqlr);

                        /*$debito1 = $credito2 = $_POST['dvalores'][$x];
                                                $credito1 = $debito2 = 0;
                                                $sqlr = "INSERT INTO comprobante_det (id_comp, cuenta, tercero, centrocosto, detalle, cheque, valdebito, valcredito, estado, vigencia) VALUES ('10 $consecsal', '".$_POST['dbancos2'][$x]."', '".$_POST['dcts'][$x]."', '".$_POST['dccs'][$x]."', 'Traslado ".$_POST['dconsig'][$x]." ".$_POST['dnbancos'][$x]."', '',$debito1, $credito1, '1', '".$_POST['vigencia']."')";
                                                mysqli_query($linkmulti,$sqlr);
                                                $sqlr="INSERT INTO comprobante_det (id_comp, cuenta, tercero, centrocosto, detalle, cheque, valdebito, valcredito, estado, vigencia) VALUES ('10 $consecsal', '$rowct[0]', '".$_POST['dcts'][$x]."', '".$_POST['dccs'][$x]."', 'Traslado ".$_POST['dconsig'][$x]." ".$_POST['dnbancos'][$x]."', '', $debito2, $credito2, '1', '".$_POST['vigencia']."')";
                                                mysqli_query($linkmulti,$sqlr);*/
                    }
                    echo "
							<script>
								Swal.fire({
									icon: 'success',
									title: 'Se ha reflejado los Traslados con Exito',
									confirmButtonText: 'Continuar',
									confirmButtonColor: '#FF121A',
									timer: 3000
								});
							</script>";
                }
            } else {
                echo "
						<script>
							Swal.fire({
								icon: 'error',
								title: 'Error!',
								text: 'No Tiene los Permisos para Modificar este Documento',
								confirmButtonText: 'Continuar',
								confirmButtonColor: '#FF121A',
								timer: 3000
							});
						</script>";
            }
        }
        ?>
    </form>
</body>

</html>
