<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Gráficos con Vue 2 y Chart.js</title>
  <!-- CDN de Vue 2 -->
  <script src="https://cdn.jsdelivr.net/npm/vue@2.6.14/dist/vue.js"></script>
  <!-- CDN de Chart.js -->
  <script src="https://cdn.jsdelivr.net/npm/chart.js@2.9.4/dist/Chart.min.js"></script>
</head>
<body>
  <div id="app">
    <!-- Usamos el componente gráfico -->
    <line-chart></line-chart>
  </div>

  <script>
    Vue.component('line-chart', {
      template: '<canvas ref="canvas" width="400" height="200"></canvas>',
      mounted() {
        // Crear el gráfico usando directamente Chart.js
        new Chart(this.$refs.canvas.getContext('2d'), {
          type: 'line',
          data: {
            labels: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio'],
            datasets: [{
              label: 'Ventas',
              backgroundColor: 'rgba(54, 162, 235, 0.5)',
              borderColor: 'rgba(54, 162, 235, 1)',
              data: [40, 20, 12, 39, 10, 40, 39],
              fill: false
            }]
          },
          options: {
            responsive: true,
            maintainAspectRatio: false
          }
        });
      }
    });

    // Instancia principal de Vue
    new Vue({
      el: '#app'
    });
  </script>
</body>
</html>
