<?php 
	require 'comun.inc';
	require 'funciones.inc';
	$linkbd = conectar_v7();	
	cargarcodigopag($_GET['codpag'],$_SESSION["nivel"]);
	header("Cache-control: private"); // Arregla IE 6
	date_default_timezone_set("America/Bogota");
	
?>
<!DOCTYPE >
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="es">
	<head>
    	<meta http-equiv="Content-Type" content="text/html" charset="iso-8859-1"/>
    	<meta http-equiv="X-UA-Compatible" content="IE=9"/>
		<title>:: Spid - Gestion Humana</title>
        <link href="css/css2.css" rel="stylesheet" type="text/css" />
        <link href="css/css3.css" rel="stylesheet" type="text/css" />
		<script type="text/javascript" src="css/programas.js"></script>
        <script type="text/javascript" src="css/calendario.js"></script>
		<script>
			var anterior;
			function ponprefijo(dato1,dato2,dato3,dato4,dato5)
			{ 
    			parent.document.form2.idcargoad.value =dato1;
				parent.document.form2.nomcargoad.value =dato2;
				parent.document.form2.cargo.value =dato3;
				parent.document.form2.nivsal.value =dato4;
				parent.document.form2.asigbas.value =dato5;
				parent.document.form2.asigbas2.value =formatNumber.new(dato5, "$ ");	
    			//parent.document.form2.cc.focus();
				parent.despliegamodal2("hidden");
			} 
		</script> 
		<?php titlepag();?>
	</head>
	<body>
  		<form action="" method="post" enctype="multipart/form-data" name="form2">
        	<?php if($_POST['oculto']==""){$_POST['numpos']=0;$_POST['numres']=10;$_POST['nummul']=0;}?>
			<table class="inicio" align="center">
      			<tr>
               
                     <td class="titulos" colspan="4">:: Buscar Medidores: </td>
                      
        			<td class="cerrar"><a onClick="parent.despliegamodal2('hidden');" href="#" >&nbsp;Cerrar</a></td>
      			</tr>
      			<tr>
        			<td class="saludo1">:: Nombre:</td>
        			<td>
                        <input name="nombre" type="text" size="40">
                    </td>
        			<td class="saludo1">:: Codigo:</td>
        			<td>
                    	<input name="codigo" type="text" id="codigo">			      
                    	<input type="button" name="bboton" onClick="limbusquedas();" value="&nbsp;&nbsp;Buscar&nbsp;&nbsp;" />
                	</td>
       			</tr>                       
    		</table> 
            <input type="hidden" name="oculto" id="oculto" value="1">
            <input type="hidden" name="numres" id="numres" value="<?php echo $_POST['numres'];?>"/>
            <input type="hidden" name="numpos" id="numpos" value="<?php echo $_POST['numpos'];?>"/>
            <input type="hidden" name="nummul" id="nummul" value="<?php echo $_POST['nummul'];?>"/>

      		<?php

                // Codigo para busqueda
				$searchId = '';
				$searchNombre = '';
				 
				if (isset($_POST['nombre'])) { // Buscar por nombre
                    $searchNombre = 'AND descripcion LIKE \'%'.$_POST['nombre'].'%\'';
                }

				if (isset($_POST['codigo'])) { // Buscar por id
                    $searchId = 'AND id LIKE \'%'.$_POST['codigo'].'%\'';
                }

                // Codigo para paginación

                $queryString = 'SELECT id, descripcion FROM '.$_GET['table'].' WHERE estado=\'S\' '.$searchId.' '.$searchNombre;

                $resp = mysqli_query($linkbd, $queryString);
                
                $_POST['numtop'] = mysqli_num_rows($resp); // $_POST['numtop'] es el numero de filas que regresan de la base de datos.
                
                $nuncilumnas = ceil($_POST['numtop'] / $_POST['numres']);
                
				$queryString = 'SELECT id, descripcion FROM '.$_GET['table'].' WHERE estado=\'S\' '.$searchId.' '.$searchNombre.' ORDER BY id LIMIT '.$_POST['numpos'].', '.$_POST['numres'];
                
                $resp = mysqli_query($linkbd, $queryString);
                
                $con = 1;
                
                $numcontrol = $_POST['nummul'] + 1;
                
                if($nuncilumnas == $numcontrol)
				{
					$imagenforward = '<img src="imagenes/forward02.png" style="width: 17px;">';
					$imagensforward = '<img src="imagenes/skip_forward02.png" style="width: 16px;">';
				}
				else 
				{
					$imagenforward="<img src='imagenes/forward01.png' style='width:17px' title='Siguiente' onClick='numsiguiente()'>";
					$imagensforward="<img src='imagenes/skip_forward01.png' style='width:16px' title='Fin' onClick='saltocol(\"$nuncilumnas\")'>";
				}
				if($_POST['numpos'] == 0)
				{
					$imagenback="<img src='imagenes/back02.png' style='width:17px'>";
					$imagensback="<img src='imagenes/skip_back02.png' style='width:16px'>";
				}
				else
				{
					$imagenback="<img src='imagenes/back01.png' style='width:17px' title='Anterior' onClick='numanterior();'>";
					$imagensback="<img src='imagenes/skip_back01.png' style='width:16px' title='Inicio' onClick='saltocol(\"1\")'>";
				}
				echo "
				<table class='inicio' style=\"width: 99%;\">
                    <tr>
                        <td colspan='2' class='titulos'>.: Resultados Busqueda:</td>
                        <td class='submenu' style=\"width: 10%;\">
                            <select name='renumres' id='renumres' onChange='cambionum();' style='width:100%'>
                                <option value='10'"; if ($_POST['renumres'] == '10') echo 'selected'; echo ">10</option>
                                <option value='20'"; if ($_POST['renumres'] == '20') echo 'selected'; echo ">20</option>
                                <option value='30'"; if ($_POST['renumres'] == '30') echo 'selected'; echo ">30</option>
                                <option value='50'"; if ($_POST['renumres'] == '50') echo 'selected'; echo ">50</option>
                                <option value='100'"; if ($_POST['renumres'] == '100') echo 'selected'; echo ">100</option>
                            </select>
                        </td>
                    </tr>
                </table>
                <table class='inicio' style=\"width: 99%;\">
				    <tr>
					    <td class='titulos2' width='20%'>Codigo</td>
					    <td class='titulos2' width='80%'>Descripci&oacute;n</td>
					
				    </tr>";	
				$iter = 'saludo1a';
                $iter2 = 'saludo2';
                
 				while ($row = mysqli_fetch_assoc($resp)) 
 				{
	 				echo'
					<tr class="'.$iter.'" ondblclick="parent.respuestaModalBusqueda(\''.$_GET['table'].'\', \''.$row['descripcion'].'\', \''.$row['id'].'\');">
						<td>'.$row['id'].'</td>
						<td>'.$row['descripcion'].'</td>
                    </tr>';
                    
	 				$aux = $iter;
	 				$iter = $iter2;
	 				$iter2 = $aux;
                }
                
				if ($_POST['numtop'] == 0)
					{
						echo "
						<table class='inicio'>
							<tr>
								<td class='saludo1' style='text-align:center;width:100%'><img src='imagenes\alert.png' style='width:25px'>No hay coincidencias en la b&uacute;squeda $tibusqueda<img src='imagenes\alert.png' style='width:25px'></td>
							</tr>
						</table>";
					}
				echo"
					</table>
					<table class='inicio'>
						<tr>
							<td style='text-align:center;'>
								<a href='#'>$imagensback</a>&nbsp;
                                <a href='#'>$imagenback</a>&nbsp;&nbsp;";
                                
                if($nuncilumnas <= 9) {
                    $numfin = $nuncilumnas;
                }
                else {
                    $numfin = 9;
                }

				for($xx = 1; $xx <= $numfin; $xx++) {
                    if($numcontrol <= 9) {
                        $numx = $xx;
                    }
					else {
                        $numx = $xx + ($numcontrol - 9);
                    }

                    if($numcontrol == $numx) { 
                        echo"<a href='#' onClick='saltocol(\"$numx\")'; style='color:#24D915'> $numx </a>";
                    }
					else {
                        echo"<a href='#' onClick='saltocol(\"$numx\")'; style='color:#000000'> $numx </a>";
                    }
				}
				echo"			&nbsp;&nbsp;<a href='#'>$imagenforward</a>
								&nbsp;<a href='#'>$imagensforward</a>
							</td>
						</tr>
					</table>";
			?>
            <input type="hidden" name="numtop" id="numtop" value="<?php echo $_POST['numtop'];?>" />
		</form>
	</body>
</html>
