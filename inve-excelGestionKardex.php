<?php
	header("Content-Type: text/html;charset=utf-8");
    require_once 'PHPExcel/Classes/PHPExcel.php';
    require "comun.inc";
    require "funciones.inc";
    require 'funcionesSP.inc.php';
    session_start();
    class Articulo{
        private $linkbd;
        public function __construct() {
            $this->linkbd = conectar_v7();
            $this->linkbd->set_charset("utf8");
        }
        public function selectMovimientos($codigo,$bodega,$fechaInicial,$fechaFinal){
            $dateRange ="";
            if($fechaInicial !="" && $fechaFinal !=""){
                $arrFechaInicio = explode("/",$fechaInicial);
                $arrFechaFinal = explode("/",$fechaFinal);

                $dateInicio = date_format(date_create($arrFechaInicio[2]."-".$arrFechaInicio[1]."-".$arrFechaInicio[0]),"Y-m-d");
                $dateFinal = date_format(date_create($arrFechaFinal[2]."-".$arrFechaFinal[1]."-".$arrFechaFinal[0]),"Y-m-d");
                $dateRange = " AND i.fecha >= '$dateInicio' AND i.fecha <= '$dateFinal'";
            }
            $arrData = [];
            $articulos = mysqli_fetch_all(mysqli_query($this->linkbd,
                "SELECT CONCAT(grupoinven,codigo) as codigo,
                nombre FROM almarticulos WHERE CONCAT(grupoinven,codigo) like '$codigo%'"),MYSQLI_ASSOC);
            if(count($articulos)>0){
                $total = count($articulos);
                for ($i=0; $i < $total ; $i++) {
                    $cantidad = 0;
                    $valorTotal = 0;
                    $idArt = $articulos[$i]['codigo'];
                    $movimientos = mysqli_fetch_all(mysqli_query(
                        $this->linkbd,
                        "SELECT DATE_FORMAT(i.fecha,'%d/%m/%Y') as fecha,
                        DATE(i.fecha) as fechaorden ,
                        d.codart,
                        d.codigo as documento,
                        d.valortotal as total,
                        d.valorunit as valor,
                        d.cantidad_entrada as entrada,
                        d.cantidad_salida as salida,
                        d.unidad,
                        CONCAT(d.tipomov,d.tiporeg) as movimiento,
                        d.tipomov,
                        c.nombre as centro,
                        t.nombre as mov_nom
                        FROM almginventario_det d
                        INNER JOIN almginventario i ON i.consec = d.codigo AND i.estado = 'S' AND CONCAT(d.tipomov,d.tiporeg) = CONCAT(i.tipomov,i.tiporeg)
                        INNER JOIN centrocosto c ON c.id_cc = d.cc
                        INNER JOIN almtipomov t ON CONCAT(d.tipomov,d.tiporeg) = CONCAT(t.tipom,t.codigo)
                        WHERE d.tipomov != 3 AND d.tipomov != 4 AND d.bodega='$bodega' AND d.codart = $idArt $dateRange
                        ORDER BY fechaorden ASC;"),MYSQLI_ASSOC
                    );
                    $totalMov = count($movimientos);
                    if($totalMov > 0){
                        $arrComprobantes = array(
                            "101"=>50,
                            "104"=>53,
                            "106"=>54,
                            "107"=>51,
                            "201"=>60,
                            "202"=>61,
                            "203"=>62,
                            "204"=>63,
                            "205"=>64,
                            "206"=>65
                        );
                        for ($j=0; $j < $totalMov ; $j++) {
                            $cantidad += $movimientos[$j]['entrada'];
                            if($movimientos[$j]['salida'] > 0)$cantidad-=$movimientos[$j]['salida'];
                            $valorTotal = $cantidad * $movimientos[$j]['valor'];
                            $tipoComprobante = $arrComprobantes[$movimientos[$j]['movimiento']];
                            $validCant = $movimientos[$j]['entrada'] > 0 ? $movimientos[$j]['entrada'] : $movimientos[$j]['salida'];
                            $movimientos[$j]['cantidad_saldo'] = $cantidad;
                            $movimientos[$j]['total_saldo'] = $valorTotal;
                            $movimientos[$j]['tipo'] = $tipoComprobante;
                            $movimientos[$j]['estado_contable'] = $this->validContabilidad($movimientos[$j]['codart'],$tipoComprobante,$validCant,$movimientos[$j]['documento']);
                            $movimientos[$j]['total_entrada'] = $movimientos[$j]['valor'] * $movimientos[$j]['entrada'];
                            $movimientos[$j]['total_salida'] = $movimientos[$j]['valor'] * $movimientos[$j]['salida'];
                        }
                        $data['movimientos'] = $movimientos;
                        $data['nombre'] = $articulos[$i]['codigo']." - ".$articulos[$i]['nombre'];
                        $data['valor_total'] = $valorTotal;
                        $data['cantidad_total'] = $cantidad;
                        $data['bodega'] = mysqli_query($this->linkbd,"SELECT nombre FROM almbodegas WHERE id_cc = '$bodega'")->fetch_assoc()['nombre'];
                        array_push($arrData,$data);
                    }
                }
            }
            return $arrData;
        }
        public function validContabilidad($codigo,$comprobante,$cantidad,$documento){
            $sql="SELECT cantarticulo FROM comprobante_det
            WHERE numacti = '$codigo' AND tipo_comp = '$comprobante' AND numerotipo = '$documento'";
            $request = mysqli_query($this->linkbd,$sql)->fetch_assoc()['cantarticulo'];
            $result = 0;
            if($request > 0){
                $result = $request == $cantidad ? 1 : 2;
            }else{
                $result = 3;
            }
            return $result;
        }
    }
    if($_GET){
        $obj = new Articulo();
        $request = $obj->selectMovimientos($_GET['articulo'],$_GET['bodega'],$_GET['fecha_inicial'],$_GET['fecha_final']);
        $objPHPExcel = new PHPExcel();
        $objPHPExcel->getActiveSheet()->getStyle('A:J')->applyFromArray(
            array(
                'alignment' => array(
                    'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                    'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER
                )
            )
        );
        $objPHPExcel->getProperties()
        ->setCreator("IDEAL 10")
        ->setLastModifiedBy("IDEAL 10")
        ->setTitle("Exportar Excel con PHP")
        ->setSubject("Documento de prueba")
        ->setDescription("Documento generado con PHPExcel")
        ->setKeywords("usuarios phpexcel")
        ->setCategory("reportes");

        //----Cuerpo de Documento----
        $objPHPExcel->setActiveSheetIndex(0)
        ->mergeCells('A1:P1')
        ->mergeCells('A2:P2')
        ->setCellValue('A1', 'ALMACÉN')
        ->setCellValue('A2', 'REPORTE KARDEX - '.$request[0]['bodega']);
        $objPHPExcel-> getActiveSheet ()
        -> getStyle ("A1")
        -> getFill ()
        -> setFillType (PHPExcel_Style_Fill :: FILL_SOLID)
        -> getStartColor ()
        -> setRGB ('C8C8C8');
        $objPHPExcel-> getActiveSheet ()
        -> getStyle ("A1:A2")
        -> getFont ()
        -> setBold ( true )
        -> setName ( 'Verdana' )
        -> setSize ( 10 )
        -> getColor ()
        -> setRGB ('000000');
        $objPHPExcel-> getActiveSheet ()
        -> getStyle ('A1:A2')
        -> getAlignment ()
        -> applyFromArray (array ( 'horizontal'  =>  PHPExcel_Style_Alignment :: HORIZONTAL_CENTER ,) );
        $objPHPExcel-> getActiveSheet ()
        -> getStyle ('A3:P3')
        -> getAlignment ()
        -> applyFromArray (array ( 'horizontal'  =>  PHPExcel_Style_Alignment :: VERTICAL_JUSTIFY ,) );
        $objPHPExcel-> getActiveSheet ()
        -> getStyle ("A2")
        -> getFill ()
        -> setFillType (PHPExcel_Style_Fill :: FILL_SOLID);

        $borders = array(
            'borders' => array(
                'allborders' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN,
                'color' => array('argb' => 'FF000000'),
                )
            ),
        );
        $objPHPExcel->getActiveSheet()->getStyle('A1:P1')->applyFromArray($borders);
        $objPHPExcel->getActiveSheet()->getStyle('A2:P2')->applyFromArray($borders);
        $objPHPExcel->getActiveSheet()->getStyle('A3:P3')->applyFromArray($borders);
        $objPHPExcel->setActiveSheetIndex(0)
            ->mergeCells('A3:D3')
            ->mergeCells('E3:H3')
            ->mergeCells('I3:L3')
            ->mergeCells('M3:P3')
            ->setCellValue('A3', 'Convenciones:')
            ->setCellValue('E3', "Cantidad negativa")
            ->setCellValue('I3', "Cantidad no coincide contabilidad")
            ->setCellValue('M3', "No existe contabilidad");
        $objPHPExcel-> getActiveSheet ()
        -> getStyle ("E3:P3")
        -> getAlignment ()
        -> applyFromArray (array ( 'horizontal'  =>  PHPExcel_Style_Alignment :: HORIZONTAL_CENTER));
        $objPHPExcel-> getActiveSheet ()
        -> getStyle ("E3:H3")
        -> getFill ()
        -> setFillType (PHPExcel_Style_Fill :: FILL_SOLID)
        -> getStartColor ()
        -> setRGB ('ffeb3b');
        $objPHPExcel-> getActiveSheet ()
        -> getStyle ("I3:L3")
        -> getFill ()
        -> setFillType (PHPExcel_Style_Fill :: FILL_SOLID)
        -> getStartColor ()
        -> setRGB ('7D2181');
        $objPHPExcel-> getActiveSheet ()
        -> getStyle ("M3:P3")
        -> getFill ()
        -> setFillType (PHPExcel_Style_Fill :: FILL_SOLID)
        -> getStartColor ()
        -> setRGB ('ff0000');

        $objPHPExcel->getActiveSheet()->getStyle("I3:P3")->getFont()->getColor()->setRGB("ffffff");
        $objPHPExcel->getActiveSheet()->getStyle("A3:P3")->getFont()->setBold(true);


        $objWorksheet = $objPHPExcel->getActiveSheet();
        $totalData = count($request);
        $rowHead = 4;
        $rowSubH = 5;
        $rowMov = 6;
        for ($i=0; $i < $totalData ; $i++) {
            $objPHPExcel->setActiveSheetIndex(0)
            ->mergeCells('A'.$rowHead.':D'.$rowHead)
            ->mergeCells('E'.$rowHead.':H'.$rowHead)
            ->mergeCells('I'.$rowHead.':L'.$rowHead)
            ->mergeCells('M'.$rowHead.':P'.$rowHead)
            ->setCellValue('A'.$rowHead, 'Artículo: '.$request[$i]['nombre'])
            ->setCellValue('E'.$rowHead, "Entradas")
            ->setCellValue('I'.$rowHead, "Salidas")
            ->setCellValue('M'.$rowHead, "Saldo")
            ->setCellValue('A'.$rowSubH, 'Fecha')
            ->setCellValue('B'.$rowSubH, 'Documento')
            ->setCellValue('C'.$rowSubH, 'Movimiento')
            ->setCellValue('D'.$rowSubH, 'Centro costo')
            ->setCellValue('E'.$rowSubH, 'Unidad')
            ->setCellValue('F'.$rowSubH, 'Valor')
            ->setCellValue('G'.$rowSubH, 'Cantidad')
            ->setCellValue('H'.$rowSubH, 'Total')
            ->setCellValue('I'.$rowSubH, 'Unidad')
            ->setCellValue('J'.$rowSubH, 'Valor')
            ->setCellValue('K'.$rowSubH, 'Cantidad')
            ->setCellValue('L'.$rowSubH, 'Total')
            ->setCellValue('M'.$rowSubH, 'Unidad')
            ->setCellValue('N'.$rowSubH, 'Cantidad')
            ->setCellValue('O'.$rowSubH, 'Valor')
            ->setCellValue('P'.$rowSubH, 'Total');

            $objPHPExcel->getActiveSheet()->getStyle("A$rowHead:P$rowHead")->applyFromArray($borders);
            $objPHPExcel->getActiveSheet()->getStyle("A$rowSubH:P$rowSubH")->applyFromArray($borders);
            $objPHPExcel-> getActiveSheet ()
            -> getStyle ("A$rowHead:P$rowHead")
            -> getFill ()
            -> setFillType (PHPExcel_Style_Fill :: FILL_SOLID)
            -> getStartColor ()
            -> setRGB ('3399cc');
            $objPHPExcel-> getActiveSheet ()
            -> getStyle ("A$rowSubH:P$rowSubH")
            -> getFill ()
            -> setFillType (PHPExcel_Style_Fill :: FILL_SOLID)
            -> getStartColor ()
            -> setRGB ('99ddff');
            $objPHPExcel-> getActiveSheet ()
            -> getStyle ("A$rowHead:D$rowHead")
            -> getAlignment ()
            -> applyFromArray (array ( 'horizontal'  =>  PHPExcel_Style_Alignment :: HORIZONTAL_LEFT));
            $objPHPExcel-> getActiveSheet ()
            -> getStyle ("E$rowHead:P$rowHead")
            -> getAlignment ()
            -> applyFromArray (array ( 'horizontal'  =>  PHPExcel_Style_Alignment :: HORIZONTAL_CENTER));
            $objPHPExcel-> getActiveSheet ()
            -> getStyle ("A$rowSubH:P$rowSubH")
            -> getAlignment ()
            -> applyFromArray (array ( 'horizontal'  =>  PHPExcel_Style_Alignment :: HORIZONTAL_CENTER));

            $objPHPExcel->getActiveSheet()->getStyle("A$rowHead:P$rowHead")->getFont()->getColor()->setRGB("ffffff");
            $objPHPExcel->getActiveSheet()->getStyle("A$rowHead:P$rowHead")->getFont()->setBold(true);

            $arrMov = $request[$i]['movimientos'];
            $totalMovs = count($arrMov);
            for ($j=0; $j < $totalMovs ; $j++) {
                $objPHPExcel->setActiveSheetIndex(0)
                ->setCellValueExplicit ("A$rowMov", $arrMov[$j]['fecha'], PHPExcel_Cell_DataType :: TYPE_STRING)
                ->setCellValueExplicit ("B$rowMov", $arrMov[$j]['documento'], PHPExcel_Cell_DataType :: TYPE_STRING)
                ->setCellValueExplicit ("C$rowMov", $arrMov[$j]['movimiento'], PHPExcel_Cell_DataType :: TYPE_STRING)
                ->setCellValueExplicit ("D$rowMov", $arrMov[$j]['centro'], PHPExcel_Cell_DataType :: TYPE_STRING)
                ->setCellValueExplicit ("E$rowMov", $arrMov[$j]['unidad'], PHPExcel_Cell_DataType :: TYPE_STRING)
                ->setCellValueExplicit ("F$rowMov", $arrMov[$j]['valor'], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
                ->setCellValueExplicit ("G$rowMov", $arrMov[$j]['entrada'], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
                ->setCellValueExplicit ("H$rowMov", $arrMov[$j]['total_entrada'], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
                ->setCellValueExplicit ("I$rowMov", $arrMov[$j]['unidad'], PHPExcel_Cell_DataType :: TYPE_STRING)
                ->setCellValueExplicit ("J$rowMov", $arrMov[$j]['valor'], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
                ->setCellValueExplicit ("K$rowMov", $arrMov[$j]['salida'], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
                ->setCellValueExplicit ("L$rowMov", $arrMov[$j]['total_salida'], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
                ->setCellValueExplicit ("M$rowMov", $arrMov[$j]['unidad'], PHPExcel_Cell_DataType :: TYPE_STRING)
                ->setCellValueExplicit ("N$rowMov", $arrMov[$j]['cantidad_saldo'], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
                ->setCellValueExplicit ("O$rowMov", $arrMov[$j]['valor'], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
                ->setCellValueExplicit ("P$rowMov", $arrMov[$j]['total_saldo'], PHPExcel_Cell_DataType :: TYPE_NUMERIC);
                $objPHPExcel-> getActiveSheet ()
                -> getStyle ("F$rowMov")
                -> getAlignment ()
                -> applyFromArray (array ( 'horizontal'  =>  PHPExcel_Style_Alignment :: HORIZONTAL_RIGHT));
                $objPHPExcel-> getActiveSheet ()
                -> getStyle ("H$rowMov")
                -> getAlignment ()
                -> applyFromArray (array ( 'horizontal'  =>  PHPExcel_Style_Alignment :: HORIZONTAL_RIGHT));
                $objPHPExcel-> getActiveSheet ()
                -> getStyle ("K$rowMov")
                -> getAlignment ()
                -> applyFromArray (array ( 'horizontal'  =>  PHPExcel_Style_Alignment :: HORIZONTAL_CENTER));
                $objPHPExcel-> getActiveSheet ()
                -> getStyle ("J$rowMov")
                -> getAlignment ()
                -> applyFromArray (array ( 'horizontal'  =>  PHPExcel_Style_Alignment :: HORIZONTAL_RIGHT));
                $objPHPExcel-> getActiveSheet ()
                -> getStyle ("L$rowMov")
                -> getAlignment ()
                -> applyFromArray (array ( 'horizontal'  =>  PHPExcel_Style_Alignment :: HORIZONTAL_RIGHT));
                $objPHPExcel-> getActiveSheet ()
                -> getStyle ("M$rowMov")
                -> getAlignment ()
                -> applyFromArray (array ( 'horizontal'  =>  PHPExcel_Style_Alignment :: HORIZONTAL_CENTER));
                $objPHPExcel-> getActiveSheet ()
                -> getStyle ("N$rowMov")
                -> getAlignment ()
                -> applyFromArray (array ( 'horizontal'  =>  PHPExcel_Style_Alignment :: HORIZONTAL_CENTER));

                if($arrMov[$j]['estado_contable'] == 2){
                    $objPHPExcel-> getActiveSheet ()
                    -> getStyle ("A$rowMov:P$rowMov")
                    -> getFill ()
                    -> setFillType (PHPExcel_Style_Fill :: FILL_SOLID)
                    -> getStartColor ()
                    -> setRGB ('7D2181');

                    $objPHPExcel->getActiveSheet()->getStyle("A$rowMov:P$rowMov")->getFont()->getColor()->setRGB("ffffff");
                    $objPHPExcel->getActiveSheet()->getStyle("A$rowMov:P$rowMov")->getFont()->setBold(true);
                }else if($arrMov[$j]['estado_contable'] == 3){
                    $objPHPExcel-> getActiveSheet ()
                    -> getStyle ("A$rowMov:P$rowMov")
                    -> getFill ()
                    -> setFillType (PHPExcel_Style_Fill :: FILL_SOLID)
                    -> getStartColor ()
                    -> setRGB ('ff0000');
                    $objPHPExcel->getActiveSheet()->getStyle("A$rowMov:P$rowMov")->getFont()->getColor()->setRGB("ffffff");
                    $objPHPExcel->getActiveSheet()->getStyle("A$rowMov:P$rowMov")->getFont()->setBold(true);
                }
                $rowMov++;
            }
           $objPHPExcel->setActiveSheetIndex(0)
            ->mergeCells('A'.$rowMov.':M'.$rowMov)
            ->setCellValueExplicit ("A$rowMov", "En bodega: ", PHPExcel_Cell_DataType :: TYPE_STRING)
            ->setCellValueExplicit ("N$rowMov", $request[$i]['cantidad_total'], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
            ->setCellValueExplicit ("O$rowMov", "Valor total: ", PHPExcel_Cell_DataType :: TYPE_STRING)
            ->setCellValueExplicit ("P$rowMov", $request[$i]['valor_total'], PHPExcel_Cell_DataType :: TYPE_NUMERIC);
            $objPHPExcel->getActiveSheet()->getStyle("A$rowMov:P$rowMov")->applyFromArray($borders);
            $objPHPExcel->getActiveSheet()->getStyle("A$rowMov:P$rowMov")->getFont()->setBold(true);
            $objPHPExcel-> getActiveSheet ()
                -> getStyle ("A$rowMov:P$rowMov")
                -> getFill ()
                -> setFillType (PHPExcel_Style_Fill :: FILL_SOLID)
                -> getStartColor ()
                -> setRGB ('C8C8C8');
            $objPHPExcel-> getActiveSheet ()
            -> getStyle ("A$rowMov")
            -> getAlignment ()
            -> applyFromArray (array ( 'horizontal'  =>  PHPExcel_Style_Alignment :: HORIZONTAL_RIGHT));

            $objPHPExcel-> getActiveSheet ()
            -> getStyle ("O$rowMov")
            -> getAlignment ()
            -> applyFromArray (array ( 'horizontal'  =>  PHPExcel_Style_Alignment :: HORIZONTAL_RIGHT));

            if($request[$i]['cantidad_total'] < 0){
                $objPHPExcel-> getActiveSheet ()
                -> getStyle ("A$rowMov:P$rowMov")
                -> getFill ()
                -> setFillType (PHPExcel_Style_Fill :: FILL_SOLID)
                -> getStartColor ()
                -> setRGB ('ffeb3b');
            }

            $rowHead= $rowMov+1;
            $rowSubH= $rowHead+1;
            $rowMov = $rowSubH+1;
        }
        $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('I')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('J')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('K')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('L')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('M')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('N')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('O')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('P')->setAutoSize(true);
        //----Guardar documento----
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="reporte-kardex.xlsx"');
        header('Cache-Control: max-age=0');
        $objWriter=PHPExcel_IOFactory::createWriter($objPHPExcel,'Excel2007');
        $objWriter->save('php://output');
        die();
    }
?>
