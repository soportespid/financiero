<?php
	header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=utf8");
	require "comun.inc";
	require "funciones.inc";
	session_start();
	$linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");
	cargarcodigopag($_GET['codpag'],$_SESSION["nivel"]);
	date_default_timezone_set("America/Bogota");
?>
<!DOCTYPE >
<html lang="es">
	<head>
	<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="ie=edge"/>
		<meta name="viewport" content="user-scalable=no">
		<title>:: IDEAL 10 - Tesorer&iacute;a</title>
		<link href="favicon.ico" rel="shortcut icon"/>
		<link href="css/css2z.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css"/>
		<link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css"/>
		<link href="css/css4.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css"/>
		<link href="sweetalert2/dist/sweetalert2.min.css" rel="stylesheet">
		<script src="sweetalert2/dist/sweetalert2.min.js"></script>
		<script src="vue/vue.js"></script>
		<script type="text/javascript" src="css/programas.js?<?php echo date('d_m_Y_h_i_s');?>"> </script>
		<script type="text/javascript" src="css/calendario.js?<?php echo date('d_m_Y_h_i_s');?>"> </script>
	</head>
	<body>
		<header>
			<table>
				<tr><script>barra_imagenes("teso");</script><?php cuadro_titulos();?></tr>
			</table>
		</header>
		<section id="myapp" v-cloak >
			<nav>
				<table>
					<tr><?php menu_desplegable("teso");?></tr>
				</table>
                <div class="bg-white group-btn p-1">
                <button type="button" onclick="window.location.href='teso-notasbancarias_new.php'"
                    class="btn btn-white btn-primary-hover d-flex justify-between align-items-center">
                    <span>Nuevo</span>
                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 -960 960 960">
                        <path d="M440-440H200v-80h240v-240h80v240h240v80H520v240h-80v-240Z"></path>
                    </svg>
                </button>
                <button type="button" class="btn btn-white btn-primary-hover d-flex justify-between align-items-center">
                    <span>Guardar</span>
                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 -960 960 960">
                        <path
                            d="M840-680v480q0 33-23.5 56.5T760-120H200q-33 0-56.5-23.5T120-200v-560q0-33 23.5-56.5T200-840h480l160 160Zm-80 34L646-760H200v560h560v-446ZM480-240q50 0 85-35t35-85q0-50-35-85t-85-35q-50 0-85 35t-35 85q0 50 35 85t85 35ZM240-560h360v-160H240v160Zm-40-86v446-560 114Z">
                        </path>
                    </svg>
                </button>
                <button type="button" @click="window.location.href='teso-buscanotasbancarias_new.php'"
                    class="btn btn-white btn-primary-hover d-flex justify-between align-items-center">
                    <span>Buscar</span>
                    <svg class="fill-black group-hover:fill-white w-5 h-5" xmlns="http://www.w3.org/2000/svg"
                        viewBox="0 -960 960 960">
                        <path
                            d="M784-120 532-372q-30 24-69 38t-83 14q-109 0-184.5-75.5T120-580q0-109 75.5-184.5T380-840q109 0 184.5 75.5T640-580q0 44-14 83t-38 69l252 252-56 56ZM380-400q75 0 127.5-52.5T560-580q0-75-52.5-127.5T380-760q-75 0-127.5 52.5T200-580q0 75 52.5 127.5T380-400Z">
                        </path>
                    </svg>
                </button>
                <button type="button" onclick="mypop=window.open('plan-agenda.php','','');mypop.focus()"
                    class="btn btn-white btn-primary-hover d-flex justify-between align-items-center">
                    <span class="group-hover:text-white">Agenda</span>
                    <svg class="fill-black group-hover:fill-white w-5 h-5" xmlns="http://www.w3.org/2000/svg"
                        viewBox="0 -960 960 960">
                        <path
                            d="M560-564v-68q33-14 67.5-21t72.5-7q26 0 51 4t49 10v64q-24-9-48.5-13.5T700-600q-38 0-73 9.5T560-564Zm0 220v-68q33-14 67.5-21t72.5-7q26 0 51 4t49 10v64q-24-9-48.5-13.5T700-380q-38 0-73 9t-67 27Zm0-110v-68q33-14 67.5-21t72.5-7q26 0 51 4t49 10v64q-24-9-48.5-13.5T700-490q-38 0-73 9.5T560-454ZM260-320q47 0 91.5 10.5T440-278v-394q-41-24-87-36t-93-12q-36 0-71.5 7T120-692v396q35-12 69.5-18t70.5-6Zm260 42q44-21 88.5-31.5T700-320q36 0 70.5 6t69.5 18v-396q-33-14-68.5-21t-71.5-7q-47 0-93 12t-87 36v394Zm-40 118q-48-38-104-59t-116-21q-42 0-82.5 11T100-198q-21 11-40.5-1T40-234v-482q0-11 5.5-21T62-752q46-24 96-36t102-12q58 0 113.5 15T480-740q51-30 106.5-45T700-800q52 0 102 12t96 36q11 5 16.5 15t5.5 21v482q0 23-19.5 35t-40.5 1q-37-20-77.5-31T700-240q-60 0-116 21t-104 59ZM280-494Z">
                        </path>
                    </svg>
                </button>
                <button type="button" onclick="mypop=window.open('teso-principal.php','','');mypop.focus();"
                    class="btn btn-white btn-primary-hover d-flex justify-between align-items-center">
                    <span>Nueva ventana</span>
                    <svg class="fill-black group-hover:fill-white w-5 h-5" xmlns="http://www.w3.org/2000/svg"
                        viewBox="0 -960 960 960">
                        <path
                            d="M320-120v-80h80v-80H160q-33 0-56.5-23.5T80-360v-400q0-33 23.5-56.5T160-840h640q33 0 56.5 23.5T880-760v400q0 33-23.5 56.5T800-280H560v80h80v80H320ZM160-360h640v-400H160v400Zm0 0v-400 400Z">
                        </path>
                    </svg>
                </button>
            </div>
			</nav>
			<article>
				<table class='tablamv'>
					<tbody style="overflow:hidden !important;">
						<tr >
							<td class="titulos"  colspan="2" >Buscar Notas Bancarias</td>
							<td class="cerrar" style="width:7%" onClick="location.href='teso-principal.php'">Cerrar</td>
						</tr>
						<tr>
							<td class="tamano01" style="width:3cm;">:&middot; N° Nota:</td>
							<td style="width:15%;"><input type="text" v-model="numnota" style="width:100%; height: 30px !important;"/></td>
							<td class="tamano01" style="width:3cm;">:&middot; Fecha Inicial:</td>
							<td style="width:15%;"><input type="text" name="fecha"  value="<?php echo $_POST['fecha']?>" onKeyUp="return tabular(event,this)" id="fc_1198971545" title="DD/MM/YYYY" onDblClick="displayCalendarFor('fc_1198971545');" class="colordobleclik" autocomplete="off" onChange="" style="width:100%; height: 30px !important;" readonly></td>
							<td class="tamano01" style="width:3cm;" >:&middot; Fecha Final:</td>
							<td style="width:15%;"><input type="text" name="fecha2" value="<?php echo $_POST['fecha2']?>" onKeyUp="return tabular(event,this)" id="fc_1198971546" title="DD/MM/YYYY" onDblClick="displayCalendarFor('fc_1198971546');" class="colordobleclik" autocomplete="off" onChange="" style="width:100%; height: 30px !important;" readonly></td>
							<td style="padding-bottom:0px"><em class="botonflechaverde" v-on:Click="validarBusqueda();">Buscar</em></td>
							<td></td>
						</tr>
					</tbody>
					<thead>
						<tr>
							<th class='titulosnew00' style="width:10%; font: 130% sans-serif;">N° Nota Bancaria</th>
							<th class='titulosnew00' style="width:10%; font: 130% sans-serif;">Fecha Costo</th>
							<th class='titulosnew00' style="width:57%; font: 130% sans-serif;">Concepto Nota Bancaria</th>
							<th class='titulosnew00' style="width:15%; font: 130% sans-serif;">Valor</th>
							<th class='titulosnew00' style="font: 130% sans-serif; ">Anular</th>
						</tr>
					</thead>
					<tbody>
						<tr v-for="(vcNotas,index) in selectNotas" v-on:DblClick="visualizarNotas(vcNotas[0])" v-bind:class="vcNotas[5] == vcNotas[0] ? 'saludoyellow' : index % 2 ? 'saludo1a' : 'saludo2'" style="text-rendering: optimizeLegibility; cursor: pointer !important;" >
							<td style="width:10%; font: 110% sans-serif; padding-left:10px; text-align:center;">{{ vcNotas[0] }}</td>
							<td style="width:10%; font: 110% sans-serif; padding-left:10px; text-align:center;">{{ vcNotas[1] }}</td>
							<td style="width:58%; font: 110% sans-serif; padding-left:10px">{{ vcNotas[2] }}</td>
							<td style="width:15%; font: 110% sans-serif; padding-left:10px; text-align:right;">{{ formatonumero(vcNotas[3]) }}</td>
							<td style="font: 110% sans-serif; display: flex; justify-content: center;" v-on:click="vcNotas[4] == 'S' ? validaAnular(vcNotas[0]) : notaAnulada(vcNotas[0])"><div v-bind:class="vcNotas[4] == 'S' ? 'garbageX' : 'garbageY'" v-bind:title="vcNotas[4] == 'S' ? 'Nota Activa' : 'Nota Anulada'"></td>
						</tr>
					</tbody>
				</table>

			</article>
			<div id="cargando" v-if="loading" class="loading">
					<span>Cargando...</span>
			</div>
		</section>
		<script src="Librerias/vue/vue.min.js"></script>
		<script src="Librerias/vue/axios.min.js"></script>
		<script src="vue/tesoreria/teso-buscanotasbancarias_new.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
	</body>
</html>
