<?php 
	header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=utf8");
	require "comun.inc";
	require "funciones.inc";
	sesion();
	$linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");
	date_default_timezone_set("America/Bogota");
?>
<!DOCTYPE >
<html lang="es">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=9">
		<title>:: Spid - Gestion Humana</title>
		<link href="css/css2.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css">
		<link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css">
		<script type="text/javascript" src="css/calendario.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script type="text/javascript" src="css/programas.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script>
			function ponprefijo(dato1,dato2)
			{ 
				parent.document.form2.numcuenta.value = dato1;
				parent.document.form2.nomcuenta.value = dato2;
				parent.despliegamodal2("hidden");
			} 
		</script> 
		<?php titlepag();?>
	</head>
	<body>
		<form action="" method="post" enctype="multipart/form-data" name="form2">
			<table  class="inicio" align="center">
				<tr>
					<td class="titulos" colspan="4">:: Buscar Cargo Administrativo</td>
					<td class="cerrar" style="width:7%" onClick="parent.despliegamodal2('hidden');">Cerrar</td>
				</tr>
				<tr>
					<td class="saludo1">:: Nombre:</td>
					<td><input name="nombre" type="text" value="" size="40"></td>
					<td>
						<td style="padding-bottom:0px"><em class="botonflecha" onClick="document.form2.submit();">Buscar</em></td>

					</td>
				</tr>
			</table>
			<input type="hidden" name="oculto" id="oculto" value="1">
			<input type="hidden" name="numres" id="numres" value="<?php echo $_POST['numres'];?>"/>
			<input type="hidden" name="numpos" id="numpos" value="<?php echo $_POST['numpos'];?>"/>
			<input type="hidden" name="nummul" id="nummul" value="<?php echo $_POST['nummul'];?>"/>
			<div class="subpantalla" style="height:87.5%; width:99.6%; overflow-x:hidden;">
			<?php
				
				if ($_POST['nombre'] != ""){$crit1 = "AND nombre LIKE '%".$_POST['nombre']."%'";}
				else {$crit1 = " ";}
				$sqlmax = "SELECT MAX(version) FROM cuentasccpet";
				$resmax = mysqli_query($linkbd, $sqlmax);
				$rowmax = mysqli_fetch_row($resmax);
				$maxVersion = $rowmax[0];
				$sqlcta ="SELECT codigo, nombre, padre, tipo FROM cuentasccpet WHERE version = '$maxVersion' AND municipio = '1' $crit1 ORDER BY id ASC";
				$rescta = mysqli_query($linkbd,$sqlcta);
				$totalcli = mysqli_num_rows($rescta);
				echo "
				<table class='inicio' align='center' width='99%'>
					<tr><td colspan='5' class='titulos'>.: Resultados Busqueda:</td></tr>
					<tr><td colspan='5'>Centro Costo Encontrados: $totalcli</td></tr>
					<tr>
						<td class='titulos2' width='5%'>Item</td>
						<td class='titulos2' width='20%'>Cuenta </td>
						<td class='titulos2' >Nombre</td>
						<td class='titulos2' width='5%'>Padre</td>
						<td class='titulos2' width='5%'>Tipo</td>
					</tr>";
				$iter='saludo1a';
				$iter2='saludo2';
				while ($rowcta = mysqli_fetch_row($rescta))
				{
					$i = 1;
					if ($rowcta[3] == 'C'){$elegircuenta = "onClick=\"javascript:ponprefijo('$rowcta[0]','$rowcta[1]')\"";}
					else {$elegircuenta = "";}
					echo 
						"<tr class='$iter' $elegircuenta>
							<td>$i</td>
							<td>$rowcta[0]</td>
							<td>$rowcta[1]</td>
							<td>$rowcta[2]</td>
							<td>$rowcta[3]</td>
						</tr>";
					$aux=$iter;
					$iter=$iter2;
					$iter2=$aux;
					$i++;
				}
				echo "</table>"
			?>
			</div>
		</form>
	</body>
</html>
