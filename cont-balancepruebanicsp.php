<?php
ini_set('max_execution_time', 3600);
header("Cache-control: no-cache, no-store, must-revalidate");
header("Content-Type: text/html;charset=utf8");

require 'comun.inc';
require 'funciones.inc';

$linkbd_v7 = conectar_v7();
$linkbd_v7->set_charset("utf8");

session_start();
date_default_timezone_set("America/Bogota");
$_POST['fecha'] = isset($_POST['fecha']) && $_POST['fecha'] != "" ? $_POST['fecha'] : "01/01/".date("Y");
$_POST['fecha2'] = isset($_POST['fecha2']) && $_POST['fecha2'] != "" ? $_POST['fecha2'] : date("d/m/Y");
?>
<!DOCTYPE>
<html lang="es">

<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1" />
    <meta name="viewport" content="user-scalable=no">
    <title>:: IDEAL 10 - Contabilidad</title>
    <link href="favicon.ico" rel="shortcut icon" />
    <link href="css/css2.css?<?php echo date('d_m_Y_h_i_s'); ?>" rel="stylesheet" type="text/css" />
    <link href="css/css3.css?<?php echo date('d_m_Y_h_i_s'); ?>" rel="stylesheet" type="text/css" />
    <link href="css/css4.css?<?php echo date('d_m_Y_h_i_s'); ?>" rel="stylesheet" type="text/css" />
    <link href="css/style.css?<?php echo date('d_m_Y_h_i_s'); ?>" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="css/calendario.js"></script>
    <script type="text/javascript" src="css/programas.js"></script>
    <script type="text/javascript" src="jquery-1.11.0.min.js"></script>

    <style>
        .c2 input[type="checkbox"]:not(:checked),
        .c2 input[type="checkbox"]:checked {
            position: absolute !important;
            left: -9999px !important;
        }

        .c2 input[type="checkbox"]:not(:checked)+#t2,
        .c2 input[type="checkbox"]:checked+#t2 {
            position: relative !important;
            padding-left: 1.95em !important;
            cursor: pointer !important;
        }

        /* checkbox aspect */
        .c2 input[type="checkbox"]:not(:checked)+#t2:before,
        .c2 input[type="checkbox"]:checked+#t2:before {
            content: '' !important;
            position: absolute !important;
            left: 0 !important;
            top: -2 !important;
            width: 1.55em !important;
            height: 1.55em !important;
            border: 2px solid #ccc !important;
            background: #fff !important;
            border-radius: 4px !important;
            box-shadow: inset 0 1px 3px rgba(0, 0, 0, .1) !important;
        }

        /* checked mark aspect */
        .c2 input[type="checkbox"]:not(:checked)+#t2:after,
        .c2 input[type="checkbox"]:checked+#t2:after {
            content: url(imagenes/tilde.png) !important;
            position: absolute !important;
            top: .1em;
            left: .3em !important;
            font-size: 1.3em !important;
            line-height: 0.8 !important;
            color: #09ad7e !important;
            transition: all .2s !important;
        }

        /* checked mark aspect changes */
        .c2 input[type="checkbox"]:not(:checked)+#t2:after {
            opacity: 0 !important;
            transform: scale(0) !important;
        }

        .c2 input[type="checkbox"]:checked+#t2:after {
            opacity: 1 !important;
            transform: scale(1) !important;
        }

        /* disabled checkbox */
        .c2 input[type="checkbox"]:disabled:not(:checked)+#t2:before,
        .c2 input[type="checkbox"]:disabled:checked+#t2:before {
            box-shadow: none !important;
            border-color: #bbb !important;
            background-color: #ddd !important;
        }

        .c2 input[type="checkbox"]:disabled:checked+#t2:after {
            color: #999 !important;
        }

        .c2 input[type="checkbox"]:disabled+#t2 {
            color: #aaa !important;
        }

        /* accessibility */
        .c2 input[type="checkbox"]:checked:focus+#t2:before,
        .c2 input[type="checkbox"]:not(:checked):focus+#t2:before {
            border: 2px dotted blue !important;
        }

        /* hover style just for information */
        .c2 #t2:hover:before {
            border: 2px solid #4778d9 !important;
        }

        #t2 {
            background-color: white !important;
        }


        .c1 input[type="checkbox"]:not(:checked),
        .c1 input[type="checkbox"]:checked {
            position: absolute !important;
            left: -9999px !important;
        }

        .c1 input[type="checkbox"]:not(:checked)+#t1,
        .c1 input[type="checkbox"]:checked+#t1 {
            position: relative !important;
            padding-left: 1.95em !important;
            cursor: pointer !important;
        }

        /* checkbox aspect */
        .c1 input[type="checkbox"]:not(:checked)+#t1:before,
        .c1 input[type="checkbox"]:checked+#t1:before {
            content: '' !important;
            position: absolute !important;
            left: 0 !important;
            top: -2 !important;
            width: 1.55em !important;
            height: 1.55em !important;
            border: 2px solid #ccc !important;
            background: #fff !important;
            border-radius: 4px !important;
            box-shadow: inset 0 1px 3px rgba(0, 0, 0, .1) !important;
        }

        /* checked mark aspect */
        .c1 input[type="checkbox"]:not(:checked)+#t1:after,
        .c1 input[type="checkbox"]:checked+#t1:after {
            content: url(imagenes/tilde.png) !important;
            position: absolute !important;
            top: .1em;
            left: .3em !important;
            font-size: 1.3em !important;
            line-height: 0.8 !important;
            color: #09ad7e !important;
            transition: all .2s !important;
        }

        /* checked mark aspect changes */
        .c1 input[type="checkbox"]:not(:checked)+#t1:after {
            opacity: 0 !important;
            transform: scale(0) !important;
        }

        .c1 input[type="checkbox"]:checked+#t1:after {
            opacity: 1 !important;
            transform: scale(1) !important;
        }

        /* disabled checkbox */
        .c1 input[type="checkbox"]:disabled:not(:checked)+#t1:before,
        .c1 input[type="checkbox"]:disabled:checked+#t1:before {
            box-shadow: none !important;
            border-color: #bbb !important;
            background-color: #ddd !important;
        }

        .c1 input[type="checkbox"]:disabled:checked+#t1:after {
            color: #999 !important;
        }

        .c1 input[type="checkbox"]:disabled+#t1 {
            color: #aaa !important;
        }

        /* accessibility */
        .c1 input[type="checkbox"]:checked:focus+#t1:before,
        .c1 input[type="checkbox"]:not(:checked):focus+#t1:before {
            border: 2px dotted blue !important;
        }

        /* hover style just for information */
        .c1 #t1:hover:before {
            border: 2px solid #4778d9 !important;
        }

        #t1 {
            background-color: white !important;
        }



        .c3 input[type="checkbox"]:not(:checked),
        .c3 input[type="checkbox"]:checked {
            position: absolute !important;
            left: -9999px !important;
        }

        .c3 input[type="checkbox"]:not(:checked)+#t3,
        .c3 input[type="checkbox"]:checked+#t3 {
            position: relative !important;
            padding-left: 1.95em !important;
            cursor: pointer !important;
        }

        /* checkbox aspect */
        .c3 input[type="checkbox"]:not(:checked)+#t3:before,
        .c3 input[type="checkbox"]:checked+#t3:before {
            content: '' !important;
            position: absolute !important;
            left: 0 !important;
            top: -2 !important;
            width: 1.55em !important;
            height: 1.55em !important;
            border: 2px solid #ccc !important;
            background: #fff !important;
            border-radius: 4px !important;
            box-shadow: inset 0 1px 3px rgba(0, 0, 0, .1) !important;
        }

        /* checked mark aspect */
        .c3 input[type="checkbox"]:not(:checked)+#t3:after,
        .c3 input[type="checkbox"]:checked+#t3:after {
            content: url(imagenes/tilde.png) !important;
            position: absolute !important;
            top: .1em;
            left: .3em !important;
            font-size: 1.3em !important;
            line-height: 0.8 !important;
            color: #09ad7e !important;
            transition: all .2s !important;
        }

        /* checked mark aspect changes */
        .c3 input[type="checkbox"]:not(:checked)+#t3:after {
            opacity: 0 !important;
            transform: scale(0) !important;
        }

        .c3 input[type="checkbox"]:checked+#t3:after {
            opacity: 1 !important;
            transform: scale(1) !important;
        }

        /* disabled checkbox */
        .c3 input[type="checkbox"]:disabled:not(:checked)+#t3:before,
        .c3 input[type="checkbox"]:disabled:checked+#t3:before {
            box-shadow: none !important;
            border-color: #bbb !important;
            background-color: #ddd !important;
        }

        .c3 input[type="checkbox"]:disabled:checked+#t3:after {
            color: #999 !important;
        }

        .c3 input[type="checkbox"]:disabled+#t3 {
            color: #aaa !important;
        }

        /* accessibility */
        .c3 input[type="checkbox"]:checked:focus+#t3:before,
        .c3 input[type="checkbox"]:not(:checked):focus+#t3:before {
            border: 2px dotted blue !important;
        }

        /* hover style just for information */
        .c3 #t3:hover:before {
            border: 2px solid #4778d9 !important;
        }

        #t3 {
            background-color: white !important;
        }
    </style>

    <script>
        $(window).load(function () {
            $('#cargando').hide();
            /*  const fechaInicial = new Date(new Date().getFullYear(),0,1).toISOString().split("T")[0];
             const fechaFinal = new Date().toISOString().split("T")[0];
             const arrFechaInicial =fechaInicial.split("-");
             const arrFechaFinal =fechaFinal.split("-");
             document.querySelector("#fecha").value = arrFechaInicial[2]+"/"+arrFechaInicial[1]+"/"+arrFechaInicial[0];
             document.querySelector("#fecha2").value = arrFechaFinal[2]+"/"+arrFechaFinal[1]+"/"+arrFechaFinal[0]; */
        });
        function pdf() {
            document.form2.action = "pdfbalance.php";
            document.form2.target = "_BLANK";
            document.form2.submit();
            document.form2.action = "";
            document.form2.target = "";
        }
        function excell() {
            document.form2.action = "cont-balancepruebanicspexcel.php";
            document.form2.target = "_BLANK";
            document.form2.submit();
            document.form2.action = "";
            document.form2.target = "";
        }
        function despliegamodal2(_valor, _nomcu) {
            document.getElementById("bgventanamodal2").style.visibility = _valor;
            if (_valor == "hidden") { document.getElementById('ventana2').src = ""; }
            else {
                document.getElementById('ventana2').src = "cuentasgral-ventana01.php?vigencia=<?php echo $_SESSION['vigencia'] ?>&objeto=" + _nomcu + "&nobjeto=000";
            }
        }

        function direccionaCuentaGastos(row) {
            var cell = row.getElementsByTagName("td")[0];
            var id = cell.innerHTML;
            var fech = document.getElementById("fecha").value;
            var fech1 = document.getElementById("fecha2").value;
            window.open("cont-auxiliarcuenta.php?cod=" + id + "&fec=" + fech + "&fec1=" + fech1);
        }
        function validar() {
            var fech = document.getElementById("fecha").value;
            var fech1 = document.getElementById("fecha2").value;
            var separador1 = fech.split("/");
            var separador2 = fech1.split("/");
            if (separador1[2] < '2018' || separador2[2] < '2018') {
                alert('Las fechas deben ser mayores del 2017');
            }
            else {
                document.form2.submit();
            }
        }

        function validaExterno(e) {
            //console.log(e);
            document.form2.oculto.value = '';
            document.form2.externo.value = '1';
            document.form2.consolidado.checked = false;
            document.form2.submit();
        }

        function validaCierre(e) {
            //console.log(e);
            document.form2.oculto.value = '';
            document.form2.cierre.value = '1';
            document.form2.submit();
        }

        function validaConsolidado(e) {
            document.form2.oculto.value = '';
            document.form2.consolidado.value = '1';
            document.form2.cc.value = '';
            document.form2.externo.checked = false;
            document.form2.submit();
        }
        var expanded = false;
        function showCheckboxes() {
            var checkboxes = document.getElementById("checkboxes");
            if (!expanded) {
                checkboxes.style.display = "block";
                expanded = true;
            }
            else {
                checkboxes.style.display = "none";
                expanded = false;
            }
        }

        /* document.addEventListener("click", function(e)
        {
            var div = document.getElementById("checkboxes");
            console.log(div);
            //obtiendo informacion del DOM para
            var clic = e.target;

            if(div.style.display == "block" && clic != div)
            {
                div.style.display = "none";
            }
        }, true); */
    </script>
    <style type="text/css">
        .multiselect {
            width: 200px;
            float: left;
            padding-left: 10px;
            padding-top: 5px;
        }

        .generar-report {
            float: right;
            padding-right: 30px;
        }

        .selectBox {
            position: relative;
        }

        .selectBox select {
            width: 100%;
            font-weight: bold;
        }

        .overSelect {
            position: absolute;
            left: 0;
            right: 0;
            top: 0;
            bottom: 0;
        }

        #checkboxes {
            display: none;
            border: 1px #dadada solid;
            position: absolute;
            width: 18%;
            overflow-y: scroll;
            z-index: 999999999;
        }

        #checkboxes1 {
            display: none;
            border: 1px #dadada solid;
            position: absolute;
            width: 20%;
            overflow-y: scroll;
            z-index: 9999;
        }

        #checkboxes label,
        #checkboxes1 label {
            display: block;
            background: #ECEFF1;
            border-bottom: 1px solid #CFD8DC;
            font-size: 10px;
        }

        #checkboxes label:last-child,
        #checkboxes1 label:last-child {
            display: block;
            background: #ECEFF1;
            border-bottom: none;
        }

        #checkboxes label:hover,
        #checkboxes1 label:hover {
            background-color: #1e90ff;
            cursor: pointer;
        }
    </style>
</head>

<body>
    <div id="cargando" style=" position:absolute;left: 46%; bottom: 45%">
        <img src="imagenes/loading.gif" style=" width: 80px; height: 80px" />
    </div>
    <table>
        <tr>
            <script>barra_imagenes("cont");</script><?php cuadro_titulos(); ?>
        </tr>
        <tr><?php menu_desplegable("cont"); ?></tr>
    </table>
    <div class="bg-white group-btn p-1"><button type="button"
            onclick="window.location.href='cont-balancepruebanicsp.php'"
            class="btn btn-white btn-primary-hover d-flex justify-between align-items-center">
            <span>Nuevo</span>
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 -960 960 960">
                <path d="M440-440H200v-80h240v-240h80v240h240v80H520v240h-80v-240Z"></path>
            </svg>
        </button><button type="button" onclick="document.form2.submit()"
            class="btn btn-white btn-primary-hover d-flex justify-between align-items-center">
            <span>Buscar</span>
            <svg class="fill-black group-hover:fill-white w-5 h-5" xmlns="http://www.w3.org/2000/svg"
                viewBox="0 -960 960 960">
                <path
                    d="M784-120 532-372q-30 24-69 38t-83 14q-109 0-184.5-75.5T120-580q0-109 75.5-184.5T380-840q109 0 184.5 75.5T640-580q0 44-14 83t-38 69l252 252-56 56ZM380-400q75 0 127.5-52.5T560-580q0-75-52.5-127.5T380-760q-75 0-127.5 52.5T200-580q0 75 52.5 127.5T380-400Z">
                </path>
            </svg>
        </button><button type="button" onclick="mypop=window.open('plan-agenda.php','','');mypop.focus()"
            class="btn btn-white btn-primary-hover d-flex justify-between align-items-center">
            <span class="group-hover:text-white">Agenda</span>
            <svg class="fill-black group-hover:fill-white w-5 h-5" xmlns="http://www.w3.org/2000/svg"
                viewBox="0 -960 960 960">
                <path
                    d="M560-564v-68q33-14 67.5-21t72.5-7q26 0 51 4t49 10v64q-24-9-48.5-13.5T700-600q-38 0-73 9.5T560-564Zm0 220v-68q33-14 67.5-21t72.5-7q26 0 51 4t49 10v64q-24-9-48.5-13.5T700-380q-38 0-73 9t-67 27Zm0-110v-68q33-14 67.5-21t72.5-7q26 0 51 4t49 10v64q-24-9-48.5-13.5T700-490q-38 0-73 9.5T560-454ZM260-320q47 0 91.5 10.5T440-278v-394q-41-24-87-36t-93-12q-36 0-71.5 7T120-692v396q35-12 69.5-18t70.5-6Zm260 42q44-21 88.5-31.5T700-320q36 0 70.5 6t69.5 18v-396q-33-14-68.5-21t-71.5-7q-47 0-93 12t-87 36v394Zm-40 118q-48-38-104-59t-116-21q-42 0-82.5 11T100-198q-21 11-40.5-1T40-234v-482q0-11 5.5-21T62-752q46-24 96-36t102-12q58 0 113.5 15T480-740q51-30 106.5-45T700-800q52 0 102 12t96 36q11 5 16.5 15t5.5 21v482q0 23-19.5 35t-40.5 1q-37-20-77.5-31T700-240q-60 0-116 21t-104 59ZM280-494Z">
                </path>
            </svg>
        </button><button type="button" onclick="mypop=window.open('cont-principal.php','','');mypop.focus();"
            class="btn btn-white btn-primary-hover d-flex justify-between align-items-center">
            <span>Nueva ventana</span>
            <svg class="fill-black group-hover:fill-white w-5 h-5" xmlns="http://www.w3.org/2000/svg"
                viewBox="0 -960 960 960">
                <path
                    d="M320-120v-80h80v-80H160q-33 0-56.5-23.5T80-360v-400q0-33 23.5-56.5T160-840h640q33 0 56.5 23.5T880-760v400q0 33-23.5 56.5T800-280H560v80h80v80H320ZM160-360h640v-400H160v400Zm0 0v-400 400Z">
                </path>
            </svg>
        </button><button type="button" onclick="pdf()"
            class="btn btn-danger btn-danger-hover d-flex justify-between align-items-center">
            <span>Exportar PDF</span>
            <svg xmlns="http://www.w3.org/2000/svg"
                viewBox="0 0 512 512"><!-- !Font Awesome Free 6.5.1 by @fontawesome - https://fontawesome.com License - https://fontawesome.com/license/free Copyright 2024 Fonticons, Inc. -->
                <path
                    d="M64 464l48 0 0 48-48 0c-35.3 0-64-28.7-64-64L0 64C0 28.7 28.7 0 64 0L229.5 0c17 0 33.3 6.7 45.3 18.7l90.5 90.5c12 12 18.7 28.3 18.7 45.3L384 304l-48 0 0-144-80 0c-17.7 0-32-14.3-32-32l0-80L64 48c-8.8 0-16 7.2-16 16l0 384c0 8.8 7.2 16 16 16zM176 352l32 0c30.9 0 56 25.1 56 56s-25.1 56-56 56l-16 0 0 32c0 8.8-7.2 16-16 16s-16-7.2-16-16l0-48 0-80c0-8.8 7.2-16 16-16zm32 80c13.3 0 24-10.7 24-24s-10.7-24-24-24l-16 0 0 48 16 0zm96-80l32 0c26.5 0 48 21.5 48 48l0 64c0 26.5-21.5 48-48 48l-32 0c-8.8 0-16-7.2-16-16l0-128c0-8.8 7.2-16 16-16zm32 128c8.8 0 16-7.2 16-16l0-64c0-8.8-7.2-16-16-16l-16 0 0 96 16 0zm80-112c0-8.8 7.2-16 16-16l48 0c8.8 0 16 7.2 16 16s-7.2 16-16 16l-32 0 0 32 32 0c8.8 0 16 7.2 16 16s-7.2 16-16 16l-32 0 0 48c0 8.8-7.2 16-16 16s-16-7.2-16-16l0-64 0-64z">
                </path>
            </svg>
        </button><button type="button" onclick="excell()"
            class="btn btn-success btn-success-hover d-flex justify-between align-items-center">
            <span>Exportar Excel</span>
            <svg class="fill-black group-hover:fill-white w-4 h-4" xmlns="http://www.w3.org/2000/svg"
                viewBox="0 0 384 512"><!--!Font Awesome Free 6.5.1 by @fontawesome - https://fontawesome.com License - https://fontawesome.com/license/free Copyright 2024 Fonticons, Inc. -->
                <path
                    d="M48 448V64c0-8.8 7.2-16 16-16H224v80c0 17.7 14.3 32 32 32h80V448c0 8.8-7.2 16-16 16H64c-8.8 0-16-7.2-16-16zM64 0C28.7 0 0 28.7 0 64V448c0 35.3 28.7 64 64 64H320c35.3 0 64-28.7 64-64V154.5c0-17-6.7-33.3-18.7-45.3L274.7 18.7C262.7 6.7 246.5 0 229.5 0H64zm90.9 233.3c-8.1-10.5-23.2-12.3-33.7-4.2s-12.3 23.2-4.2 33.7L161.6 320l-44.5 57.3c-8.1 10.5-6.3 25.5 4.2 33.7s25.5 6.3 33.7-4.2L192 359.1l37.1 47.6c8.1 10.5 23.2 12.3 33.7 4.2s12.3-23.2 4.2-33.7L222.4 320l44.5-57.3c8.1-10.5 6.3-25.5-4.2-33.7s-25.5-6.3-33.7 4.2L192 280.9l-37.1-47.6z">
                </path>
            </svg>
        </button><button type="button" onclick="window.location.href='archivos/Ideal10balanceprueba-nivel.csv'"
            class="btn btn-success btn-success-hover d-flex justify-between align-items-center">
            <span>Exportar CSV</span>
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 -960 960 960">
                <path
                    d="M230-360h120v-60H250v-120h100v-60H230q-17 0-28.5 11.5T190-560v160q0 17 11.5 28.5T230-360Zm156 0h120q17 0 28.5-11.5T546-400v-60q0-17-11.5-31.5T506-506h-60v-34h100v-60H426q-17 0-28.5 11.5T386-560v60q0 17 11.5 30.5T426-456h60v36H386v60Zm264 0h60l70-240h-60l-40 138-40-138h-60l70 240ZM160-160q-33 0-56.5-23.5T80-240v-480q0-33 23.5-56.5T160-800h640q33 0 56.5 23.5T880-720v480q0 33-23.5 56.5T800-160H160Zm0-80h640v-480H160v480Zm0 0v-480 480Z">
                </path>
            </svg>
        </button></div>
    <form name="form2" method="post" action="cont-balancepruebanicsp.php">
        <?php
        $vigusu = vigencia_usuarios($_SESSION['cedulausu']);
        if ($_POST['consolidado'] == '') {
            $chkcomp = ' ';
        } else {
            $chkcomp = ' checked ';
        }
        if ($_POST['cierre'] == '') {
            $chkcierre = ' ';
        } else {
            $chkcierre = ' checked ';
        }
        $_POST['tipocc'] = "";

        if ($_POST['externo'] == '') {
            $chkexterno = ' ';
        } else {
            $chkexterno = ' checked ';
        }
        ?>
        <table align="center" class="inicio">
            <tr>
                <td class="titulos" colspan="8">.: Balance de Prueba</td>
                <td class="cerrar" style="width:7%;"><a href="cont-principal.php">&nbsp;Cerrar</a></td>
            </tr>
            <tr>
                <td class="saludo1" style="width:10%;">Nivel:</td>
                <td style="width:12%;">
                    <select name="nivel" id="nivel" style="width:80%;" tabindex="1">
                        <?php
                        $niveles = array();
                        $sqlr = "Select * from nivelesctas  where estado='S' order by id_nivel";
                        $resp = mysqli_query($linkbd_v7, $sqlr);
                        while ($row = mysqli_fetch_row($resp)) {
                            $niveles[] = $row[4];
                            if ($row[0] == $_POST['nivel']) {
                                echo "<option value=$row[0] SELECTED>$row[0]</option>";
                            } else {
                                echo "<option value=$row[0]>$row[0]</option>";
                            }
                        }
                        ?>
                    </select>
                </td>
                <td class="saludo1" style="width:10%;">Mes Inicial:</td>
                <td style="width:10%;">
                    <input type="text" name="fecha" value="<?php echo $_POST['fecha'] ?>"
                        onKeyUp="return tabular(event,this)" id="fecha" title="DD/MM/YYYY"
                        onDblClick="displayCalendarFor('fecha');" class="colordobleclik" autocomplete="off" onChange=""
                        readonly>
                </td>
                <td class="saludo1" style="width:10%;">Mes Final:</td>
                <td style="width:10%;">
                    <input type="text" name="fecha2" value="<?php echo $_POST['fecha2'] ?>"
                        onKeyUp="return tabular(event,this)" id="fecha2" title="DD/MM/YYYY"
                        onDblClick="displayCalendarFor('fecha2');" class="colordobleclik" autocomplete="off" onChange=""
                        readonly>
                </td>
                <td class="saludo1">

                    <div style="display:inline-block;">
                        <label>Agregado</label>
                    </div>
                    <div class="c1" style="display:inline-block; ">
                        <input type="checkbox" id="consolidado" name="consolidado" <?php if (isset($_POST['consolidado'])) {
                            echo "checked";
                        } ?>
                            value="<?php echo $_POST['consolidado'] ?>" onChange="validaConsolidado(this)" />
                        <label for="consolidado" id="t1">
                        </label>
                    </div>

                    <div style="display:inline-block;">
                        <label>Cierre</label>
                    </div>
                    <div class="c2" style="display:inline-block; ">
                        <input type="checkbox" id="cierre" name="cierre" <?php if (isset($_POST['cierre'])) {
                            echo "checked";
                        } ?> value="<?php echo $_POST['cierre'] ?>" onChange="validaCierre(this)" />
                        <label for="cierre" id="t2">
                        </label>
                    </div>

                    <div style="display:inline-block;">
                        <label>Externo</label>
                    </div>
                    <div class="c3" style="display:inline-block; ">
                        <input type="checkbox" id="externo" name="externo" <?php if (isset($_POST['externo'])) {
                            echo "checked";
                        } ?> value="<?php echo $_POST['externo'] ?>" onChange="validaExterno(this)" />
                        <label for="externo" id="t3">
                        </label>
                    </div>
                </td>
            </tr>
            <tr>
                <td class="saludo1">Cuenta Inicial:</td>
                <td><input name="cuenta1" type="text" id="cuenta1" style="width:80%;"
                        value="<?php echo $_POST['cuenta1']; ?>" onKeyPress="javascript:return solonumeros(event)"
                        onKeyUp="return tabular(event,this) " tabindex="6" />&nbsp;<a href="#" tabindex="7"
                        onClick="despliegamodal2('visible','cuenta1')"><img src="imagenes/find02.png"
                            style="width:20px;" align="absmiddle" border="0"></a></td>
                <td class="saludo1">Cuenta Final:</td>
                <td><input name="cuenta2" type="text" id="cuenta2" style="width:80%;"
                        value="<?php echo $_POST['cuenta2']; ?>" onKeyPress="javascript:return solonumeros(event)"
                        onKeyUp="return tabular(event,this) " tabindex="8">&nbsp;<a href="#" tabindex="9"
                        onClick="despliegamodal2('visible','cuenta2')"><img src="imagenes/find02.png"
                            style="width:20px;" align="absmiddle" border="0"></a></td>
                <td class="saludo1">Centro Costo:</td>
                <td colspan="2" style="width:25%;">
                    <?php
                    $deabilidato = '';
                    if ($_POST['consolidado'] == '1') {
                        $deabilidato = 'disabled';
                    }
                    if ($_POST['externo'] == '1') {
                        ?>
                        <select id='cc' name="cc" onKeyUp="return tabular(event,this)" style="width:70%;" tabindex="10">
                            <option value="">Seleccione...</option>
                            <?php
                            //$sqlr="select *from pptouniejecu where estado='S' AND  entidad='N'";
                            $sqlr = "SELECT codigo, base, nombre, usuario FROM redglobal WHERE tipo = 'EX' ORDER BY id";
                            $res = mysqli_query($linkbd_v7, $sqlr);
                            while ($row = mysqli_fetch_row($res)) {
                                if ($_POST['cc'] == $row[0]) {
                                    echo "<option value='$row[0]' SELECTED> $row[0] - $row[2]</option>";
                                    $_POST['baseext'] = $row[1];
                                    $_POST['ndestino'] = $row[2];
                                    $_POST['baseextUsuario'] = $row[3];
                                } else {
                                    echo "<option value='$row[0]'> $row[0] - $row[2]</option>";
                                }

                                /* if($row[0]==$_POST['cc'])
                                                           {
                                                               echo "<option value=$row[0] SELECTED>$row[0] - $row[1]</option>";
                                                               $_POST['tipocc']=$row[3];
                                                           }
                                                           else{echo "<option value=$row[0]>$row[0] - $row[1]</option>";} */
                            }
                            ?>
                        </select>
                        <input type="button" name="generar" value="Generar" onClick="validar()" tabindex="13">
                        <input type='hidden' name='ndestino' id='ndestino' value='"<?php $_POST['ndestino']; ?>"'>
                        <input type='hidden' name='baseext' id='baseext' value='"<?php $_POST['baseext']; ?>"'>
                        <input type='hidden' name='baseextUsuario' id='baseextUsuario'
                            value='"<?php $_POST['baseextUsuario']; ?>"'>
                        <?php
                    } else {
                        if ($_POST['consolidado'] != '1') {
                            $baseCentral = '';
                            $usuarioBase = '';
                            $sqlrCentral = "SELECT base, usuario FROM redglobal WHERE tipo = 'IN'";
                            $resCentral = mysqli_query($linkbd_v7, $sqlrCentral);
                            $rowCentral = mysqli_fetch_row($resCentral);
                            $baseCentral = $rowCentral[0];
                            $usuarioBase = $rowCentral[1];
                        }
                        ?>
                        <input type="hidden" name='cc'>
                        <div class="multiselect" id="multiselct">
                            <div class="selectBox" <?php if ($_POST['consolidado'] != '1') { ?> onclick='showCheckboxes()' ;
                                <?php } ?>>
                                <select>
                                    <option id="texto">Selecciona...</option>
                                </select>
                                <div class="overSelect"></div>
                            </div>
                            <div id="checkboxes">
                                <?php
                                $sql = "Select * from centrocosto where estado='S' AND  entidad='S'";
                                $query = mysqli_query($linkbd_v7, $sql);
                                while ($row = mysqli_fetch_array($query)) {
                                    echo "<label for='" . $row[0] . "'>
													<input type='checkbox' class='" . $row[0] . "' id='$row[0]' name='tipocc'/>$row[0] - " . utf8_encode($row[1]) . " </label>";
                                }
                                ?>
                            </div>
                        </div>
                        <div class='generar-report'>
                            <input type="button" name="generar" value="Generar" onClick="validar()" tabindex="13">
                        </div>
                        <input type="hidden" name="filtros" id="filtros" value="<?php echo $_POST['filtros']; ?>" value="">

                        <?php
                    }
                    ?>


                </td>
            </tr>
        </table>
        <input type="hidden" name="tipocc1" value='<?php echo $_POST['tipocc1'] ?>'>
        <input type="hidden" name="oculto" value="1"></td>
        <div class="subpantallap" style="height:62%; width:99.6%; overflow-x:hidden;">
            <?php
            function generarCentroCosto($conector, $tabla, $arreglo)
            {
                $resultado = "";
                if (isset($arreglo)) {
                    $tamArreglo = count($arreglo);
                    if ($tamArreglo > 0) {
                        $resultado = " and (";
                    } else {
                        $resultado = " ";
                    }

                    $tiene = false;
                    $unidades = "";
                    for ($i = 0; $i < $tamArreglo; $i++) {
                        $unidades .= ($arreglo[$i]) . ",";
                        if ($i == 0) {
                            $resultado .= " $tabla.centrocosto like '$arreglo[$i]'";
                        } else {
                            $resultado .= " or $tabla.centrocosto like '$arreglo[$i]'";
                        }

                    }
                    $unidades = substr($unidades, 0, -1);
                    if ($tamArreglo > 0) {
                        $resultado .= " )";
                    } else {
                        $resultado = " ";
                    }
                }
                //$resultado.="$tabla.centrocosto IN ($unidades)";
                return $resultado;
            }
            //echo "tipo :  ".$_POST[tipocc];
            //**** para sacar la consulta del balance se necesitan estos datos ********
            //**** nivel, mes inicial, mes final, cuenta inicial, cuenta final, cc inicial, cc final
            $oculto = $_POST['oculto'];
            if ($_POST['oculto']) {
                $critcons = " and comprobante_det.tipo_comp <> 19 ";

                $sqlrcc = "select id_cc from centrocosto where entidad='N'";
                $rescc = mysqli_query($linkbd_v7, $sqlrcc);
                while ($rowcc = mysqli_fetch_row($rescc)) {
                    $critcons2 .= " and comprobante_det.centrocosto <> '$rowcc[0]' ";
                }

                $vectorBusqueda = explode("-", $_POST['filtros']);
                if ($vectorBusqueda[0] == '') {
                    unset($vectorBusqueda);
                }
                $busqueda .= generarCentroCosto("LIKE", "comprobante_det", $vectorBusqueda);

                $ccExterno = '';
                if ($_POST['tipocc'] == 'N') {
                    $ccExterno = $_POST['cc'];
                    $_POST['cc'] = "";
                }

                if ($_POST['cierre'] == '1') {
                    $critconscierre = " ";
                } else {
                    $critconscierre = " and comprobante_det.tipo_comp <> 13 ";
                }
                //echo $critcons;
                $horaini = date('h:i:s');

                preg_match("/([0-9]{2})\/([0-9]{2})\/([0-9]{4})/", $_POST['fecha'], $fecha);
                $fechaf1 = $fecha[3] . "-" . $fecha[2] . "-" . $fecha[1];

                preg_match("/([0-9]{2})\/([0-9]{2})\/([0-9]{4})/", $_POST['fecha2'], $fecha);
                $fechaf2 = $fecha[3] . "-" . $fecha[2] . "-" . $fecha[1];

                preg_match("/([0-9]{2})\/([0-9]{2})\/([0-9]{4})/", $_POST['fecha'], $fecha);
                $fechaf = $fecha[3] . "-" . $fecha[2] . "-" . $fecha[1];

                $fechafa2 = mktime(0, 0, 0, $fecha[2], $fecha[1], $fecha[3]);
                $f1 = $fechafa2;
                $f2 = mktime(0, 0, 0, $fecha[2], $fecha[1], $fecha[3]);
                $fechafa = $vigusu . "-01-01";
                $fechafa2 = date('Y-m-d', $fechafa2 - ((24 * 60 * 60)));
                //Borrar el balance de prueba anterior
                $sqlr2 = "select distinct digitos, posiciones from nivelesctas where estado='S' ORDER BY id_nivel DESC ";
                $resn = mysqli_query($linkbd_v7, $sqlr2);
                $rown = mysqli_fetch_row($resn);
                $nivmax = $rown[0];
                $dignivmax = $rown[1];
                //continuar**** creacion balance de prueba
                //$namearch="archivos/".$_SESSION[usuario]."balanceprueba.csv";
                //$Descriptor1 = fopen($namearch,"w+");
                //fputs($Descriptor1,"CODIGO;CUENTA;SALDO ANTERIOR;DEBITO;CREDITO;SALDO FINAL\r\n");

                echo "<table class='inicio' ><tr><td colspan='6' class='titulos'>Balance de Prueba</td></tr>";
                echo "<tr><td class='titulos2'>Codigo</td><td class='titulos2'>Cuenta</td><td class='titulos2'>Saldo Anterior</td><td class='titulos2'>Debito</td><td class='titulos2'>Credito</td><td class='titulos2'>Saldo Final</td></tr>";
                $tam = $niveles[$_POST['nivel'] - 1];
                if (empty($_POST['cuenta1']) || empty($_POST['cuenta2'])) {
                    $_POST['cuenta1'] = "1";
                    $_POST['cuenta2'] = "99999999999";
                }

                $crit1 = " and left(cuenta,$tam)>='$_POST[cuenta1]' and left(cuenta,$tam)<='$_POST[cuenta2]' ";
                $sqlr2 = "select distinct cuenta,tipo from cuentasnicsp where estado ='S'  and length(cuenta)=$tam " . $crit1 . " group by cuenta,tipo order by cuenta ";
                $rescta = mysqli_query($linkbd_v7, $sqlr2);
                $i = 0;
                //echo $sqlr2;
                $pctas = array();
                $pctasb[] = array();
                while ($row = mysqli_fetch_row($rescta)) {
                    $pctas[] = $row[0];
                    $pctasb["$row[0]"][0] = $row[0];
                    $pctasb["$row[0]"][1] = 0;
                    $pctasb["$row[0]"][2] = 0;
                    $pctasb["$row[0]"][3] = 0;
                }
                mysqli_free_result($rescta);
                $tam = $niveles[$_POST['nivel'] - 1];
                //echo "tc:".count($pctas);
                //******MOVIMIENTOS PERIODO
                $sqlr3 = "SELECT DISTINCT
        		SUBSTR(comprobante_det.cuenta,1,$tam),
        		sum(comprobante_det.valdebito),
        		sum(comprobante_det.valcredito)
     			FROM comprobante_det, comprobante_cab
				WHERE comprobante_cab.tipo_comp = comprobante_det.tipo_comp
				AND comprobante_det.numerotipo = comprobante_cab.numerotipo
				AND comprobante_cab.estado = 1
				AND (comprobante_det.valdebito > 0
				OR comprobante_det.valcredito > 0)
				AND comprobante_cab.fecha BETWEEN '$fechaf1' AND '$fechaf2'
				AND comprobante_det.tipo_comp <> 7 AND comprobante_det.tipo_comp <> 102 AND comprobante_det.tipo_comp <> 100 AND comprobante_det.tipo_comp <> 101 AND comprobante_det.tipo_comp <> 103 AND comprobante_det.tipo_comp<>104 " . $critcons . " " . $critconscierre . "
				AND SUBSTR(comprobante_det.cuenta,1,$tam) >= '$_POST[cuenta1]' AND SUBSTR(comprobante_det.cuenta,1,$tam) <='$_POST[cuenta2]'
				$busqueda
				GROUP BY SUBSTR(comprobante_det.cuenta,1,$tam)
				ORDER BY comprobante_det.cuenta";
                //compara si no es una entidad externa

                if ($_POST['consolidado'] != '1' && $_POST['externo'] != '1') {
                    $linkmulti = conectar_Multi($baseCentral, $usuarioBase);
                    $linkmulti->set_charset("utf8");
                    $res = mysqli_query($linkmulti, $sqlr3);
                }

                if ($_POST['externo'] == '1') {
                    $linkmulti = conectar_Multi($_POST['baseext'], $_POST['baseextUsuario']);
                    $linkmulti->set_charset("utf8");
                    $res = mysqli_query($linkmulti, $sqlr3);
                }

                if ($_POST['consolidado'] == '1') {
                    $bases = [];
                    $usuarios = [];
                    $sqlrBases = "SELECT base, usuario FROM redglobal";
                    $resBases = mysqli_query($linkbd_v7, $sqlrBases);
                    while ($rowBases = mysqli_fetch_row($resBases)) {
                        array_push($bases, $rowBases[0]);
                        array_push($usuarios, $rowBases[1]);
                    }

                    foreach ($bases as $clave => $base) {
                        $linkmulti = conectar_Multi($base, $usuarios[$clave]);
                        $linkmulti->set_charset("utf8");
                        $res = mysqli_query($linkmulti, $sqlr3);
                        while ($row = mysqli_fetch_row($res)) {
                            $pctasb["$row[0]"][0] = $row[0];
                            $pctasb["$row[0]"][2] += $row[1];
                            $pctasb["$row[0]"][3] += $row[2];
                        }
                    }
                } else {
                    while ($row = mysqli_fetch_row($res)) {
                        $pctasb["$row[0]"][0] = $row[0];
                        $pctasb["$row[0]"][2] = $row[1];
                        $pctasb["$row[0]"][3] = $row[2];
                    }
                }
                /* if($_POST['tipocc'] != 'N' && $_POST['consolidado'] != '1')
                            {
                                $res = mysqli_query($linkbd_v7, $sqlr3);
                                // echo $sqlr3;
                            } */

                //compara si es entidad externa y si es concejo
                /* if($_POST['tipocc'] == 'N' && $ccExterno == '18' && $_POST['consolidado'] != '1')
                            {
                                $res = mysqli_query($linkbd_concejo_v7, $sqlr3);
                            }

                            //compara si es entidad externa y si es personeria
                            if($_POST['tipocc'] == 'N' && $ccExterno == '20' && $_POST['consolidado'] != '1')
                            {
                                $res = mysqli_query($linkbd_personeria_v7, $sqlr3);
                                // echo $sqlr3;
                            }

                            //compara si es entidad externa y si es servicios publicos
                            if($_POST['tipocc'] == 'N' && $ccExterno == '25' && $_POST['consolidado'] != '1')
                            {
                                $res = mysqli_query($linkbd_servicios_v7, $sqlr3);
                                // echo $sqlr3;
                            }
                            //compara si es entidad externa y si es Regalias
                            if($_POST['tipocc'] == 'N' && $ccExterno == '26' && $_POST['consolidado'] != '1')
                            {
                                $res = mysqli_query($linkbd_regalias_v7, $sqlr3);
                                // echo $sqlr3;
                            } */
                /* if($_POST['consolidado'] == '1')
                            {
                                $res = mysqli_query($linkbd_v7, $sqlr3);
                                while ($row = mysqli_fetch_row($res))
                                {
                                    //echo $row[0]." -> ".$row[1]."<br>";
                                    $pctasb["$row[0]"][0]=$row[0];
                                    $pctasb["$row[0]"][2]=$row[1];
                                    $pctasb["$row[0]"][3]=$row[2];
                                }

                                $res = mysqli_query($linkbd_concejo_v7, $sqlr3);

                                while ($row = mysqli_fetch_row($res))
                                {
                                    $pctasb["$row[0]"][0]=$row[0];
                                    $pctasb["$row[0]"][2]+=$row[1];
                                    $pctasb["$row[0]"][3]+=$row[2];
                                }

                                $res = mysqli_query($linkbd_personeria_v7, $sqlr3);
                                while ($row = mysqli_fetch_row($res))
                                {
                                    $pctasb["$row[0]"][0]=$row[0];
                                    $pctasb["$row[0]"][2]+=$row[1];
                                    $pctasb["$row[0]"][3]+=$row[2];
                                }

                                $res = mysqli_query($linkbd_servicios_v7, $sqlr3);
                                while ($row = mysqli_fetch_row($res))
                                {
                                    $pctasb["$row[0]"][0]=$row[0];
                                    $pctasb["$row[0]"][2]+=$row[1];
                                    $pctasb["$row[0]"][3]+=$row[2];
                                }

                                $res = mysqli_query($linkbd_regalias_v7, $sqlr3);
                                while ($row = mysqli_fetch_row($res))
                                {
                                    $pctasb["$row[0]"][0]=$row[0];
                                    $pctasb["$row[0]"][2]+=$row[1];
                                    $pctasb["$row[0]"][3]+=$row[2];
                                }
                            }
                            else
                            {
                                while ($row = mysqli_fetch_row($res))
                                {
                                    $pctasb["$row[0]"][0]=$row[0];
                                    $pctasb["$row[0]"][2]=$row[1];
                                    $pctasb["$row[0]"][3]=$row[2];
                                }
                            } */


                $sqlrTipoComp = "SELECT codigo FROM tipo_comprobante WHERE codigo=102";
                $resTipoComp = mysqli_query($linkbd_v7, $sqlrTipoComp);
                $rowTipoComp = mysqli_fetch_row($resTipoComp);
                if ($rowTipoComp[0] != '') {
                    $tipo_comp = 102;
                } else {
                    $tipo_comp = 7;
                }
                //**** SALDO INICIAL ***
                $sqlr3 = "SELECT DISTINCT
				SUBSTR(comprobante_det.cuenta,1,$tam),
				sum(comprobante_det.valdebito)-
				sum(comprobante_det.valcredito)
				FROM comprobante_det, comprobante_cab
				WHERE comprobante_cab.tipo_comp = comprobante_det.tipo_comp
				AND comprobante_det.numerotipo = comprobante_cab.numerotipo
				AND comprobante_cab.estado = 1
				AND (comprobante_det.valdebito > 0
				OR comprobante_det.valcredito > 0)
				AND comprobante_det.tipo_comp = $tipo_comp
				AND SUBSTR(comprobante_det.cuenta,1,$tam) >= '$_POST[cuenta1]' AND SUBSTR(comprobante_det.cuenta,1,$tam) <='$_POST[cuenta2]'
				$busqueda " . $critcons . " " . $critcons2 . "
				GROUP BY SUBSTR(comprobante_det.cuenta,1,$tam)
				ORDER BY comprobante_det.cuenta";

                if ($_POST['consolidado'] != '1' && $_POST['externo'] != '1') {
                    $linkmulti = conectar_Multi($baseCentral, $usuarioBase);
                    $linkmulti->set_charset("utf8");
                    $res = mysqli_query($linkmulti, $sqlr3);
                }

                if ($_POST['externo'] == '1') {
                    $linkmulti = conectar_Multi($_POST['baseext'], $_POST['baseextUsuario']);
                    $linkmulti->set_charset("utf8");
                    $res = mysqli_query($linkmulti, $sqlr3);
                }

                if ($_POST['consolidado'] == '1') {
                    $bases = [];
                    $usuarios = [];
                    $sqlrBases = "SELECT base, usuario FROM redglobal";
                    $resBases = mysqli_query($linkbd_v7, $sqlrBases);
                    while ($rowBases = mysqli_fetch_row($resBases)) {
                        array_push($bases, $rowBases[0]);
                        array_push($usuarios, $rowBases[1]);
                    }
                    foreach ($bases as $clave => $base) {
                        $linkmulti = conectar_Multi($base, $usuarios[$clave]);
                        $linkmulti->set_charset("utf8");
                        $res = mysqli_query($linkmulti, $sqlr3);
                        while ($row = mysqli_fetch_row($res)) {
                            $pctasb["$row[0]"][0] = $row[0];
                            $pctasb["$row[0]"][1] += $row[1];
                        }
                    }
                } else {
                    while ($row = mysqli_fetch_row($res)) {
                        $pctasb["$row[0]"][0] = $row[0];
                        $pctasb["$row[0]"][1] = $row[1];
                    }
                }

                /* if($_POST['tipocc'] != 'N' && $_POST['consolidado'] != '1')
                            {
                                $res = mysqli_query($linkbd_v7, $sqlr3);
                                // echo $sqlr3;
                            } */

                //compara si es entidad externa y si es concejo
                /* if($_POST['tipocc'] == 'N' && $ccExterno == '18' && $_POST['consolidado'] != '1')
                            {
                                $res = mysqli_query($linkbd_concejo_v7, $sqlr3);
                            }

                            //compara si es entidad externa y si es personeria
                            if($_POST['tipocc'] == 'N' && $ccExterno == '20' && $_POST['consolidado'] != '1')
                            {
                                $res = mysqli_query($linkbd_personeria_v7, $sqlr3);
                                // echo $sqlr3;
                            }

                            //compara si es entidad externa y si es servicios publicos
                            if($_POST['tipocc'] == 'N' && $ccExterno == '25' && $_POST['consolidado'] != '1')
                            {
                                $res = mysqli_query($linkbd_servicios_v7, $sqlr3);
                                // echo $sqlr3;
                            }

                            //compara si es entidad externa y si es regalias
                            if($_POST['tipocc'] == 'N' && $ccExterno == '26' && $_POST['consolidado'] != '1')
                            {
                                $res = mysqli_query($linkbd_regalias_v7, $sqlr3);
                                // echo $sqlr3;
                            } */

                /* if($_POST['consolidado'] == '1')
                            {
                                $res = mysqli_query($linkbd_v7, $sqlr3);
                                while ($row = mysqli_fetch_row($res))
                                {
                                    $pctasb["$row[0]"][0]=$row[0];
                                    $pctasb["$row[0]"][1]=$row[1];
                                }

                                 $res = mysqli_query($linkbd_concejo_v7, $sqlr3);

                                while ($row = mysqli_fetch_row($res))
                                {
                                    $pctasb["$row[0]"][0]=$row[0];
                                    $pctasb["$row[0]"][1]+=$row[1];
                                }

                                $res = mysqli_query($linkbd_personeria_v7, $sqlr3);
                                while ($row = mysqli_fetch_row($res))
                                {
                                    $pctasb["$row[0]"][0]=$row[0];
                                    $pctasb["$row[0]"][1]+=$row[1];
                                }

                                $res = mysqli_query($linkbd_servicios_v7, $sqlr3);
                                while ($row = mysqli_fetch_row($res))
                                {
                                    $pctasb["$row[0]"][0]=$row[0];
                                    $pctasb["$row[0]"][1]+=$row[1];
                                }

                                $res = mysqli_query($linkbd_regalias_v7, $sqlr3);
                                while ($row = mysqli_fetch_row($res))
                                {
                                    $pctasb["$row[0]"][0]=$row[0];
                                    $pctasb["$row[0]"][1]+=$row[1];
                                }
                            }
                            else
                            {
                                while ($row = mysqli_fetch_row($res))
                                {
                                    $pctasb["$row[0]"][0]=$row[0];
                                    $pctasb["$row[0]"][1]=$row[1];
                                }
                            } */
                //*******MOVIMIENTOS PREVIOS PERIODO
                if ($fechafa2 >= '2018-01-01') {
                    $fecini = '2018-01-01';
                    $sqlr3 = "SELECT DISTINCT
					SUBSTR(comprobante_det.cuenta,1,$tam),
					sum(comprobante_det.valdebito)-
					sum(comprobante_det.valcredito)
					FROM comprobante_det, comprobante_cab
					WHERE comprobante_cab.tipo_comp = comprobante_det.tipo_comp
					AND comprobante_det.numerotipo = comprobante_cab.numerotipo
					AND comprobante_cab.estado = 1
					AND comprobante_det.tipo_comp <> 100
					AND comprobante_det.tipo_comp <> 101
					AND comprobante_det.tipo_comp <> 103
					AND comprobante_det.tipo_comp <> 102
					AND comprobante_det.tipo_comp <> 104
					AND comprobante_det.cuenta!=''
					AND (comprobante_det.valdebito > 0
					OR comprobante_det.valcredito > 0)
					AND comprobante_det.tipo_comp <> 7 $critcons $critcons2
					AND comprobante_cab.fecha BETWEEN '$fecini' AND '$fechafa2'
					AND SUBSTR(comprobante_det.cuenta,1,$tam) >= '$_POST[cuenta1]' AND SUBSTR(comprobante_det.cuenta,1,$tam) <='$_POST[cuenta2]'
					$busqueda
					GROUP BY SUBSTR(comprobante_det.cuenta,1,$tam)
					ORDER BY comprobante_det.cuenta";

                    if ($_POST['consolidado'] != '1' && $_POST['externo'] != '1') {
                        $linkmulti = conectar_Multi($baseCentral, $usuarioBase);
                        $linkmulti->set_charset("utf8");
                        $res = mysqli_query($linkmulti, $sqlr3);
                    }

                    if ($_POST['externo'] == '1') {
                        $linkmulti = conectar_Multi($_POST['baseext'], $_POST['baseextUsuario']);
                        $linkmulti->set_charset("utf8");
                        $res = mysqli_query($linkmulti, $sqlr3);
                    }

                    if ($_POST['consolidado'] == '1') {
                        $bases = [];
                        $usuarios = [];
                        $sqlrBases = "SELECT base, usuario FROM redglobal";
                        $resBases = mysqli_query($linkbd_v7, $sqlrBases);
                        while ($rowBases = mysqli_fetch_row($resBases)) {
                            array_push($bases, $rowBases[0]);
                            array_push($usuarios, $rowBases[1]);
                        }

                        foreach ($bases as $clave => $base) {
                            $linkmulti = conectar_Multi($base, $usuarios[$clave]);
                            $linkmulti->set_charset("utf8");
                            $res = mysqli_query($linkmulti, $sqlr3);
                            while ($row = mysqli_fetch_row($res)) {
                                $pctasb["$row[0]"][0] = $row[0];
                                $pctasb["$row[0]"][1] += $row[1];
                            }
                        }
                    } else {
                        while ($row = mysqli_fetch_row($res)) {
                            $pctasb["$row[0]"][0] = $row[0];
                            $pctasb["$row[0]"][1] += $row[1];
                        }
                    }

                    /* if($_POST['tipocc'] != 'N' && $_POST['consolidado'] != '1')
                                   {
                                       $res = mysqli_query($linkbd_v7, $sqlr3);
                                       // echo $sqlr3;
                                   } */

                    //compara si es entidad externa y si es concejo
                    /* if($_POST['tipocc'] == 'N' && $ccExterno == '18' && $_POST['consolidado'] != '1')
                                   {
                                       $res = mysqli_query($linkbd_concejo_v7, $sqlr3);
                                   }

                                   //compara si es entidad externa y si es personeria
                                   if($_POST['tipocc'] == 'N' && $ccExterno == '20' && $_POST['consolidado'] != '1')
                                   {
                                       $res = mysqli_query($linkbd_personeria_v7, $sqlr3);
                                       // echo $sqlr3;
                                   }

                                   //compara si es entidad externa y si es servicios publicos
                                   if($_POST['tipocc'] == 'N' && $ccExterno == '25' && $_POST['consolidado'] != '1')
                                   {
                                       $res = mysqli_query($linkbd_servicios_v7, $sqlr3);
                                       // echo $sqlr3;
                                   }

                                   //compara si es entidad externa y si es regalias
                                   if($_POST['tipocc'] == 'N' && $ccExterno == '26' && $_POST['consolidado'] != '1')
                                   {
                                       $res = mysqli_query($linkbd_regalias_v7, $sqlr3);
                                       // echo $sqlr3;
                                   } */
                    //  sort($pctasb[]);
                    /* if($_POST['consolidado'] == '1')
                                   {
                                       $res = mysqli_query($linkbd_v7, $sqlr3);
                                       while ($row = mysqli_fetch_row($res))
                                       {
                                           $pctasb["$row[0]"][0]=$row[0];
                                           $pctasb["$row[0]"][1]+=$row[1];
                                       }

                                       $res = mysqli_query($linkbd_concejo_v7, $sqlr3);

                                       while ($row = mysqli_fetch_row($res))
                                       {
                                           $pctasb["$row[0]"][0]=$row[0];
                                           $pctasb["$row[0]"][1]+=$row[1];
                                       }

                                       $res = mysqli_query($linkbd_personeria_v7, $sqlr3);
                                       while ($row = mysqli_fetch_row($res))
                                       {
                                           $pctasb["$row[0]"][0]=$row[0];
                                           $pctasb["$row[0]"][1]+=$row[1];
                                       }

                                       $res = mysqli_query($linkbd_servicios_v7, $sqlr3);
                                       while ($row = mysqli_fetch_row($res))
                                       {
                                           $pctasb["$row[0]"][0]=$row[0];
                                           $pctasb["$row[0]"][1]+=$row[1];
                                       }

                                       $res = mysqli_query($linkbd_regalias_v7, $sqlr3);
                                       while ($row = mysqli_fetch_row($res))
                                       {
                                           $pctasb["$row[0]"][0]=$row[0];
                                           $pctasb["$row[0]"][1]+=$row[1];
                                       }
                                   }
                                   else
                                   {
                                       while ($row =mysqli_fetch_row($res))
                                       {
                                           $pctasb["$row[0]"][0]=$row[0];
                                           $pctasb["$row[0]"][1]+=$row[1];
                                       }
                                   } */
                }

                for ($y = 0; $y < $_POST['nivel']; $y++) {
                    $lonc = count($pctasb);
                    //foreach($pctasb as $k => $valores )
                    $k = 0;
                    // echo "lonc:".$lonc;
                    //   while($k<$lonc)
                    foreach ($pctasb as $k => $valores) {
                        $val = intVal($y) - 1;
                        if (strlen($pctasb[$k][0]) >= $niveles[$val]) {
                            $ncuenta = substr($pctasb[$k][0], 0, $niveles[$val]);
                            if ($ncuenta != '' && $niveles[$val] != '') {
                                $pctasb["$ncuenta"][0] = $ncuenta;
                                $pctasb["$ncuenta"][1] += $pctasb[$k][1];
                                $pctasb["$ncuenta"][2] += $pctasb[$k][2];
                                $pctasb["$ncuenta"][3] += $pctasb[$k][3];
                                //echo "<br>N:".$niveles[$y-1]." : cuenta:".$k." NC:".$ncuenta."  ".$pctasb["$ncuenta"][1]."  ".$pctasb["$ncuenta"][2]."  ".$pctasb["$ncuenta"][3];
                            }
                        }
                        $k++;
                    }
                }
                $sqlr = "create  temporary table usr_session (id int(11),cuenta varchar(20),nombrecuenta varchar(100),saldoinicial double,debito double,credito double,saldofinal double)";
                mysqli_query($linkbd_v7, $sqlr);
                $i = 1;
                foreach ($pctasb as $k => $valores) {
                    if (($pctasb[$k][1] < 0 || $pctasb[$k][1] > 0) || ($pctasb[$k][2] < 0 || $pctasb[$k][2] > 0) || ($pctasb[$k][3] < 0 || $pctasb[$k][3] > 0)) {
                        $saldofinal = $pctasb[$k][1] + $pctasb[$k][2] - $pctasb[$k][3];
                        $nomc = existecuentanicsp($pctasb[$k][0]);
                        /*if($nomc=='')
                                          {
                                              $nomc=existecuentanicsp($pctasb[$k][0]);
                                          }*/
                        $sqlr = "insert into usr_session (id,cuenta,nombrecuenta,saldoinicial,debito,credito,saldofinal) values($i,'" . $pctasb[$k][0] . "','" . $nomc . "'," . $pctasb[$k][1] . "," . $pctasb[$k][2] . "," . $pctasb[$k][3] . "," . $saldofinal . ")";
                        mysqli_query($linkbd_v7, $sqlr);
                        //echo "<br>".$sqlr;
                        $i += 1;
                    }
                    //echo "<br>cuenta:".$k."  ".$pctasb[$k][1]."  ".$pctasb[$k][2]."  ".$pctasb[$k][3];
                }
                $sqlr = "select *from usr_session order by cuenta";
                $res = mysqli_query($linkbd_v7, $sqlr);
                $_POST['tsaldoant'] = 0;
                $_POST['tdebito'] = 0;
                $_POST['tcredito'] = 0;
                $_POST['tsaldofinal'] = 0;
                $namearch = "archivos/" . $_SESSION['usuario'] . "balanceprueba-nivel$_POST[nivel].csv";
                $Descriptor1 = fopen($namearch, "w+");
                fputs($Descriptor1, "CODIGO;CUENTA;SALDO ANTERIOR;DEBITO;CREDITO;SALDO FINAL\r\n");
                $co = 'zebra1';
                $co2 = 'zebra2';
                while ($row = mysqli_fetch_row($res)) {
                    $negrilla = "style='font-weight:bold'";
                    $puntero = "";
                    $dobleclick = "";
                    if (strlen($row[1]) == ($dignivmax)) {
                        //$negrilla=" ";
                        //$_POST[tsaldoant]+=$row[3];
                        //$_POST[tdebito]+=$row[4];
                        //$_POST[tcredito]+=$row[5];
                    }
                    //echo $niveles[$_POST[nivel]-1]." ----- ".strlen($row[1])."<br>";
                    if ($niveles[$_POST['nivel'] - 1] == strlen($row[1])) {
                        $negrilla = " ";
                        $puntero = "style=\"cursor: hand\" ";
                        $dobleclick = "ondblclick='direccionaCuentaGastos(this)'";
                        $_POST['tsaldoant'] += $row[3];
                        $_POST['tdebito'] += $row[4];
                        $_POST['tcredito'] += $row[5];
                        $_POST['tsaldofinal'] += $row[6];
                    }
                    //echo $cursor."<br>";
                    echo "<tr class='$co' $puntero text-rendering: optimizeLegibility; $dobleclick>
							<td $negrilla >$row[1]</td>
							<td $negrilla>$row[2]</td>
							<td $negrilla align='right'>" . number_format($row[3], 2, ".", ",") . "</td>
							<td $negrilla align='right'>" . number_format($row[4], 2, ".", ",") . "</td>
							<td $negrilla align='right'>" . number_format($row[5], 2, ".", ",") . "</td>
							<td $negrilla align='right'>" . number_format($row[6], 2, ".", ",") . "";
                    echo "<input type='hidden' name='dcuentas[]' value= '" . $row[1] . "'> <input type='hidden' name='dncuentas[]' value= '" . $row[2] . "'><input type='hidden' name='dsaldoant[]' value= '" . number_format($row[3], 2, ",", ".") . "'> <input type='hidden' name='ddebitos[]' value= '" . number_format($row[4], 2, ",", ".") . "'> <input type='hidden' name='dcreditos[]' value= '" . number_format($row[5], 2, ",", ".") . "'><input type='hidden' name='dsaldo[]' value= '" . number_format($row[6], 2, ",", ".") . "'></td>
						</tr>";
                    fputs($Descriptor1, $row[1] . ";" . $row[2] . ";" . number_format($row[3], 3, ",", "") . ";" . number_format($row[4], 3, ",", "") . ";" . number_format($row[5], 3, ",", "") . ";" . number_format($row[6], 3, ",", "") . "\r\n");
                    $aux = $co;
                    $co = $co2;
                    $co2 = $aux;
                    $i = 1 + $i;
                }
                fclose($Descriptor1);
                echo "<tr class='$co'><td colspan='2'></td><td class='$co' align='right'>" . number_format($_POST['tsaldoant'], 2, ".", ",") . "<input type='hidden' name='tsaldoant' value= '$_POST[tsaldoant]'></td><td class='$co' align='right'>" . number_format($_POST['tdebito'], 2, ".", ",") . "<input type='hidden' name='tdebito' value= '$_POST[tdebito]'></td><td class='$co' align='right'>" . number_format($_POST['tcredito'], 2, ".", ",") . "<input type='hidden' name='tcredito' value= '$_POST[tcredito]'></td><td class='$co' align='right'>" . number_format($_POST['tsaldofinal'], 2, ".", ",") . "<input type='hidden' name='tsaldofinal' value= '$_POST[tsaldofinal]'></td></tr>";
                $horafin = date('h:i:s');
                echo "<DIV class='ejemplo'>INICIO:$horaini FINALIZO: $horafin</DIV>";
            }
            ?>
        </div>
        <div id="bgventanamodal2">
            <div id="ventanamodal2">
                <IFRAME src="" name="buster" marginWidth=0 marginHeight=0 frameBorder=0 id="ventana2" frameSpacing=0
                    style="left:500px; width:900px; height:500px; top:200;">
                </IFRAME>
            </div>
        </div>
    </form>
    <script type="text/javascript">
        let ftr = document.getElementById('filtros').value;

        jQuery(function ($) {
            if (jQuery) {
                var countChecked = function () {
                    var texto = "";
                    $("input[name=tipocc]:checked").each(
                        function () { texto += ($(this).attr('class')) + "-"; }
                    );
                    if (texto == '') { $("#texto").text("Selecciona..."); }
                    else {
                        $("#texto").text(texto.substring(0, texto.length - 1));
                        $('input[name=filtros]').val(texto.substring(0, texto.length - 1));
                    }
                };
                countChecked();
                $("input[name=tipocc][type=checkbox]").on("click", countChecked);

                if (ftr) {
                    let texto = ftr;
                    $("#texto").text(texto.substring(0, texto.length));
                }
                console.log('filtros: ' + ftr);

            }
        });

        $(document).on("click", function (e) {

            var container = $("#multiselct");

            if (!container.is(e.target) && container.has(e.target).length === 0) {
                //Se ha pulsado en cualquier lado fuera de los elementos contenidos en la variable container
                $('#checkboxes', this).css('visibility', 'hidden');

                //Fuente: https://www.iteramos.com/pregunta/18306/haga-clic-fuera-de-menu-para-cerrar-en-jquery

            } else {
                $('#checkboxes', this).css('visibility', 'visible');
            }
        });
    </script>
</body>

</html>
