<?php
	header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=utf8");

	require "comun.inc";
	require "funciones.inc";
	require "conversor.php";
	require "validaciones.inc";

	$linkbd_V7 = conectar_v7();
	$linkbd_V7 -> set_charset("utf8");

	session_start();
	date_default_timezone_set("America/Bogota");
?>

<!DOCTYPE >
<html lang="es">
	<head>
		<meta charset="utf-8"/>
		<meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1"/>
		<meta name="viewport" content="user-scalable=no">
		<title>:: IDEAL 10 - Tesorer&iacute;a</title>
		<link href="favicon.ico" rel="shortcut icon"/>
		<link href="css/css2.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<link href="css/css4.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<link href="css/tabs.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css"/>
		<link rel="stylesheet" href="bootstrap/css/bootstrap.css">
        <link rel="stylesheet" href="bootstrap/css/estilos.css">
		<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
        <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
        <script type="text/javascript" src="bootstrap/fontawesome.5.11.2/js/all.js"></script>
		<script src="vue/vue.js"></script>
		<script type="text/javascript" src="css/programas.js?<?php echo date('d_m_Y_h_i_s');?>"> </script>
		<script type="text/javascript" src="css/calendario.js"></script>
		<style>
			.modal-mask
			{
				position: fixed;
				z-index: 9998;
				top: 0;
				left: 0;
				width: 100%;
				height: 100%;
				background-color: rgba(0, 0, 0, .5);
				display: table;
				transition: opacity .3s ease;
			}
			.modal-wrapper
			{
				display: table-cell;
				vertical-align: middle;
			}
			.modal-container
			{
				width: 60%;
				margin: 0px auto;
				padding: 20px 30px;
				text-align: left;
				background:linear-gradient(#99bbcc, #B6CEDA);
				border-radius: 2px;
				box-shadow: 0 2px 8px rgba(0, 0, 0, .33);
				transition: all .3s ease;
			}
			.modal-container1
			{
				width: 50%;
				margin: 0px auto;
				padding: 20px 30px;
				text-align: left;
				background:linear-gradient(#99bbcc, #B6CEDA);
				border-radius: 10px;
				box-shadow: 0 2px 8px rgba(0, 0, 0, .33);
				transition: all .3s ease;
			}
			.modal-container2
			{
				width: 80%;
				margin: 0px auto;
				padding: 20px 30px;
				text-align: left;
				background:linear-gradient(#99bbcc, #B6CEDA);
				border-radius: 2px;
				box-shadow: 0 2px 8px rgba(0, 0, 0, .33);
				transition: all .3s ease;
			}
			.modal-container3
			{
				width: 90%;
				margin: 0px auto;
				padding: 20px 30px;
				text-align: left;
				background:linear-gradient(#99bbcc, #B6CEDA);
				border-radius: 2px;
				box-shadow: 0 2px 8px rgba(0, 0, 0, .33);
				transition: all .3s ease;
			}
			footer
			{
				text-align: right;
			}
		</style>
		<style>
			.background_active_color{
				background: #16a085;
			}
            .background_active_clasificador{
                font-family: calibri !important;
				font-weight: bold !important;
				font-size:14px !important;
            }
			.background_active{
				/* font: 115% sans-serif !important; */
    			/*font-weight: 700 !important;*/
				/*font-family: "Constantia", serif !important;*/
				font-family: calibri !important;
				font-weight: bold !important;
				font-size:20px !important;
			}
			.background_active_1{
				/* font: 115% sans-serif !important; */
    			/*font-weight: 700 !important;*/
				/*font-family: "Constantia", serif !important;*/
				font-family: helvética !important;
				font-size:20px !important;
			}
			.inicio--no-shadow{
				box-shadow: none;
			}
			.btn-delete{
				background: red;
				color: white;
				border-radius: 5px;
				border: none;
				font-size: 13px;
			}
			.btn-delete:hover, .btn-delete:focus{
				background: white;
				color: red;
			}
			.modal-mask {
			position: fixed;
			z-index: 9998;
			top: 0;
			left: 0;
			width: 100%;
			height: 100%;
			background-color: rgba(0, 0, 0, .5);
			display: table;
			transition: opacity .3s ease;
			}

			.modal-wrapper {
			display: table-cell;
			vertical-align: middle;
			}
			.modal-body{
				max-height: 250px;
				overflow-y: scroll;
			}
			.modal-intermetio{
				margin: 0 15px;
				font-family: helvética !important;
				font-size: 26px !important;
				padding: 10px 0;
			}
			.modal-intermedio-agregar{
				text-align:right;
				padding: 4px;
				margin-top: 6px;
				margin-right: 20px
			}
			.modal-body_1{
				padding-top: 15px;
				height: 40px;
			}

			.loader-table{
				/* background-color: #dff9fb;
				opacity: .5; */
				display: flex;
				align-items: center;
				justify-content: center;
				height: 75%;
			}
			.spinner{
				border: 4px solid rgba(0, 0, 0, 0.2);
				border-left-color: #39C;
				border-radius: 50%;
				width: 50px;
				height: 50px;
				animation: spin .9s linear infinite;
			}
			@keyframes spin {
				to { transform: rotate(360deg); }
			}
		</style>
	</head>

    <body>
		<div id="myapp">
			<table>
				<tr><script>barra_imagenes("teso");</script><?php cuadro_titulos();?></tr>
				<tr><?php menu_desplegable("teso");?></tr>
                <tr>
					<td colspan="3" class="cinta">
                        <a onClick="location.href='teso-superavit-ccpet.php'" class="mgbt"><img src="imagenes/add.png" title="Nuevo"/></a>
                        <a v-on:click="guardar()" class="mgbt"><img src="imagenes/guarda.png" title="Guardar"/></a>
                        <a onClick="location.href='teso-buscaSuperavit-ccpet.php'" class="mgbt"><img src="imagenes/busca.png" title="Buscar"/></a>
						<a onClick="mypop=window.open('plan-agenda.php','','');mypop.focus()" class="mgbt"><img src="imagenes/agenda1.png" title="Agenda"/></a>
                        <a onClick="mypop=window.open('teso-principal.php','',''); mypop.focus();" class="mgbt"><img src="imagenes/nv.png"  title="Nueva ventana"/></a>
						<a onClick="mypop=window.open('<?php echo $url2; ?>','','');mypop.focus();" class="mgbt"><img src="imagenes/duplicar_pantalla.png" title="Duplicar pesta&ntilde;a"></a>
					</td>
				</tr>
            </table>

            <form name="form" method="post" action="">
                <table class="inicio">
                    <tr>
                        <td class="titulos" colspan="6">.: Crear Superavit</td>
                        <td class="cerrar" style="width:7%"><a onClick="location.href='teso-principal.php'">Cerrar</a></td>
                    </tr>

					<tr>
						<td class="tamano01" style="width:5%">Consecutivo</td>
						<td style="width:15%;">
							<input type="text" id="numero" name="numero"v-model="numero" style="height:30px;text-align:center" readonly>
						</td>

						<td class="tamano01" style="width:5%;">Fecha:</td>
						<td style="width:15%;">
							<input type="text" name="fecha" value="<?php echo @ $_POST['fecha']?>" id="fecha" onchange="" title="DD/MM/YYYY" placeholder="DD/MM/YYYY" style="width:80%;height:30px;text-align:center;">&nbsp;
							<img src="imagenes/calendario04.png" onClick="displayCalendarFor('fecha');" title="Calendario" class="icobut"/>
						</td>

						<td class="tamano01" style="width:5%;">Vigencia:</td>
						<td style="width: 7%">
							<select v-model="vigencia" style="width:100%;height: 30px;">
								<option disabled value="">Seleccione Vigencia</option>
								<option v-for="year in years" :value="year[0]">{{ year[0] }}</option>
							</select>
						</td>
					</tr>

                    <tr>
                        <td class="tamano01" style="width:5%;">Concepto:</td>
						<td colspan="3">
							<input type="text" v-model="concepto" style="width:100%;height:30px;" ref="concepto"/>
						</td>
                    </tr>

					<tr>
						<td class="tamano01">Tipos de acuerdo:</td>
                        <td colspan="2">
                            <select v-model="acuerdo" style="height: 30px;width:100%;">
                                <option disabled value="">Seleccione tipo de acuerdo</option>
                                <option v-for="acuerdo in acuerdos" :value="acuerdo[0]">{{ acuerdo[0] }} - {{ acuerdo[2] }} - {{ acuerdo[1] }}</option>
                            </select>
                        </td>

						<td style=" height: 30px;"><em class="botonflecha" v-on:click="muestraIngresos()">Agregar</em></td>
					</tr>
                </table>
                <div class='subpantalla' style='height:59%; width:99.5%; margin-top:10px; overflow-x:hidden'>
                    <table class='inicio inicio--no-shadow'>
                        <tbody>
                            <tr class="titulos">
                                <td>Rubro</td>
                                <td>Fuente</td>
                                <td>Vigencia del Gasto</td>
                                <td>Cuin/Bienes/Servicios</td>
                                <td>Clasificador</td>
                                <td>Valor Ingresos</td>
                            </tr>
                            <tr v-for="(ingreso, index) in mostrarIngresos" v-bind:class="index % 2 ? 'saludo1a' : 'saludo2'" style='text-rendering: optimizeLegibility; cursor: pointer !important;'>
                                <td style="font: 140% sans-serif; padding-left:10px">{{ ingreso[0] }}</td>
                                <td style="font: 140% sans-serif; padding-left:10px">{{ ingreso[1] }}</td>
                                <td style="font: 140% sans-serif; padding-left:10px">{{ ingreso[2] }}</td>
								<td style="font: 140% sans-serif; padding-left:10px">{{ ingreso[3] }}</td>
                                <td style="font: 140% sans-serif; padding-left:10px">{{ ingreso[8] }}</td>
								<td v-on:dblclick="toggleModal(index)" style="text-align:center; background-color: FBE71B;font: 140% sans-serif; padding-left:10px;">{{ ingreso[4] }}</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
		    </form>


			<div v-show="showModal_Solo_Presupuesto">
			<transition name="modal">
				<div class="modal-mask">
					<div class="modal-wrapper">
						<div class="modal-dialog modal-lg" role="document">
							<div class="modal-content"  style = "width: 700px !important;" scrollable>
								<div class="modal-header">
								<div class="col-10">
											<h5 class="modal-title" style="text-align: center;">Digite valor:</h5>
										</div>
									<button type="button" class="close" data-dismiss="modal" aria-label="Close">
										<span aria-hidden="true" @click="showModal_Solo_Presupuesto = false">&times;</span>
									</button>
								</div>
								<div class="modal-body">
									<div class="modal-intermedio-agregar">
										<div class="row" style="margin-left: 15px;">
											<form class="form-inline">
												<div class="form-group" >
													<label style = "font-family:courier,arial,helvética;" for="valorSolo">Valor:</label>
													<input type="number" v-model="valorSolo" class="form-control mx-sm-3" style="text-align: center;">
												</div>
											</form>
										</div>
									</div>
								</div>
								<div class="modal-footer">
									<button type="button" class="btn btn-primary" v-on:click="cambiarValor">Guardar y Continuar</button>
									<button type="button" class="btn btn-secondary" @click="showModal_Solo_Presupuesto = false">Cerrar</button>
								</div>
							</div>
						</div>
					</div>
				</div>
			</transition>
		</div>
        </div>


        <script src="Librerias/vue/vue.min.js"></script>
		<script src="Librerias/vue/axios.min.js"></script>
		<script src="Librerias/vue/vue.min.js"></script>
		<script src="Librerias/vue/axios.min.js"></script>
		<script src="vue/teso-superavit-ccpet.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
    </body>
</html>
