<?php
header("Cache-control: no-cache, no-store, must-revalidate");
header("Content-Type: text/html;charset=utf8");
require "comun.inc";
require "funciones.inc";
session_start();
$linkbd = conectar_v7();
$linkbd->set_charset("utf8");
cargarcodigopag($_GET['codpag'], $_SESSION["nivel"]);
date_default_timezone_set("America/Bogota");
?>
<!DOCTYPE>
<html lang="es">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1">
    <meta name="viewport" content="user-scalable=no">
    <title>:: IDEAL 10 - Tesorer&iacute;a</title>
    <link href="favicon.ico" rel="shortcut icon" />
    <link href="css/css2z.css?<?php echo date('d_m_Y_h_i_s'); ?>" rel="stylesheet" />
    <link href="css/css3.css?<?php echo date('d_m_Y_h_i_s'); ?>" rel="stylesheet" />
    <link href="css/css4.css?<?php echo date('d_m_Y_h_i_s'); ?>" rel="stylesheet" />
    <link href="css/style.css?<?php echo date('d_m_Y_h_i_s'); ?>" rel="stylesheet" />
    <link href="sweetalert2/dist/sweetalert2.min.css" rel="stylesheet">
    <script src="sweetalert2/dist/sweetalert2.min.js"></script>
    <script type="text/javascript" src="css/programas.js?<?php echo date('d_m_Y_h_i_s'); ?>"></script>
    <script type="text/javascript" src="JQuery/jquery-2.1.4.min.js?<?php echo date('d_m_Y_h_i_s'); ?>"></script>
    <script>
        function verUltimaPos(idcta, filas, filtro) {
            var scrtop = $('#divdet').scrollTop();
            var altura = $('#divdet').height();
            var numpag = $('#nummul').val();
            var limreg = $('#numres').val();
            if ((numpag <= 0) || (numpag == ""))
                numpag = 0;
            if ((limreg == 0) || (limreg == ""))
                limreg = 10;
            numpag++;
            location.href = "teso-editaactividades.php?idtipocom=" + idcta + "&scrtop=" + scrtop + "&totreg=" + filas + "&altura=" + altura + "&numpag=" + numpag + "&limreg=" + limreg + "&filtro=" + filtro;
        }
        function guardar() {
            Swal.fire({
                icon: 'question',
                title: '¿Esta seguro de modificar los porcentajes?',
                showDenyButton: true,
                confirmButtonText: 'Guardar',
                confirmButtonColor: '#01CC42',
                denyButtonText: 'Cancelar',
                denyButtonColor: '#FF121A',
            }).then(
                (result) => {
                    if (result.isConfirmed) {
                        document.getElementById('oculto').value = '2';
                        document.form2.submit();
                    }
                    else if (result.isDenied) {
                        Swal.fire({
                            icon: 'info',
                            title: 'No se modificaron los porcentajes',
                            confirmButtonText: 'Continuar',
                            confirmButtonColor: '#FF121A',
                            timer: 2500
                        });
                    }
                }
            )
        }
        function validar(formulario) {
            document.form2.action = "cont-terceros.php";
            document.form2.submit();
        }
        function cleanForm() {
            document.form2.nombre1.value = "";
            document.form2.nombre2.value = "";
            document.form2.apellido1.value = "";
            document.form2.apellido2.value = "";
            document.form2.documento.value = "";
            document.form2.codver.value = "";
            document.form2.telefono.value = "";
            document.form2.direccion.value = "";
            document.form2.email.value = "";
            document.form2.web.value = "";
            document.form2.celular.value = "";
            document.form2.razonsocial.value = "";
        }
        function crearexcel() {
            document.form2.action = "teso-buscaactividadesexcel.php";
            document.form2.target = "_BLANK";
            document.form2.submit();
            document.form2.action = "";
            document.form2.target = "";
        }
        function allowDrop(ev) {
            ev.preventDefault();
        }
        function drag(ev, id) {
            ev.dataTransfer.setData("text", ev.target.value);
            ev.dataTransfer.setData("idcod", id);
        }
        function drop(ev) {
            ev.preventDefault();
            var data = ev.dataTransfer.getData("text");
            ev.target.value = data;
            let vidmod = document.getElementById('idmod');
            if (vidmod.value != '') {
                vidmod.value = vidmod.value + ',' + ev.dataTransfer.getData("idcod");
            } else {
                vidmod.value = ev.dataTransfer.getData("idcod");
            }
        }
        function fcambio(id) {
            let vidmod = document.getElementById('idmod');
            if (vidmod.value != '') {
                vidmod.value = vidmod.value + ',' + id;
            } else {
                vidmod.value = id;
            }
        }
        function verificazero(id) {
            let numid = document.getElementsByName('vporcentaje1[]').item(id);
            if (numid.value == '0') {
                numid.value = '';
            }
        }
        function verificavacio(id) {
            let numid = document.getElementsByName('vporcentaje1[]').item(id);
            if (numid.value == '') {
                numid.value = '0';
            }
        }
    </script>
    <?php
    $scrtop = $_GET['scrtop'];
    if ($scrtop == "")
        $scrtop = 0;
    echo "<script>
				window.onload=function(){
					$('#divdet').scrollTop(" . $scrtop . ")
				}
			</script>";
    $gidcta = $_GET['idcta'];
    if (isset($_GET['filtro']))
        $_POST['nombre'] = $_GET['filtro'];
    ?>
</head>

<body>
    <IFRAME src="alertas.php" name="alertas" id="alertas" style="display:none"></IFRAME>
    <span id="todastablas2"></span>
    <table>
        <tr>
            <script>barra_imagenes("teso");</script><?php cuadro_titulos(); ?>
        </tr>
        <tr><?php menu_desplegable("teso"); ?></tr>
    </table>
    <div class="bg-white group-btn p-1"><button type="button" onclick="window.location.href='teso-actividades.php'"
            class="btn btn-white btn-primary-hover d-flex justify-between align-items-center">
            <span>Nuevo</span>
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 -960 960 960">
                <path d="M440-440H200v-80h240v-240h80v240h240v80H520v240h-80v-240Z"></path>
            </svg>
        </button><button type="button" onclick="guardar();"
            class="btn btn-white btn-primary-hover d-flex justify-between align-items-center">
            <span>Guardar</span>
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 -960 960 960">
                <path
                    d="M840-680v480q0 33-23.5 56.5T760-120H200q-33 0-56.5-23.5T120-200v-560q0-33 23.5-56.5T200-840h480l160 160Zm-80 34L646-760H200v560h560v-446ZM480-240q50 0 85-35t35-85q0-50-35-85t-85-35q-50 0-85 35t-35 85q0 50 35 85t85 35ZM240-560h360v-160H240v160Zm-40-86v446-560 114Z">
                </path>
            </svg>
        </button><button type="button" onclick="mypop=window.open('plan-agenda.php','','');mypop.focus()"
            class="btn btn-white btn-primary-hover d-flex justify-between align-items-center">
            <span class="group-hover:text-white">Agenda</span>
            <svg class="fill-black group-hover:fill-white w-5 h-5" xmlns="http://www.w3.org/2000/svg"
                viewBox="0 -960 960 960">
                <path
                    d="M560-564v-68q33-14 67.5-21t72.5-7q26 0 51 4t49 10v64q-24-9-48.5-13.5T700-600q-38 0-73 9.5T560-564Zm0 220v-68q33-14 67.5-21t72.5-7q26 0 51 4t49 10v64q-24-9-48.5-13.5T700-380q-38 0-73 9t-67 27Zm0-110v-68q33-14 67.5-21t72.5-7q26 0 51 4t49 10v64q-24-9-48.5-13.5T700-490q-38 0-73 9.5T560-454ZM260-320q47 0 91.5 10.5T440-278v-394q-41-24-87-36t-93-12q-36 0-71.5 7T120-692v396q35-12 69.5-18t70.5-6Zm260 42q44-21 88.5-31.5T700-320q36 0 70.5 6t69.5 18v-396q-33-14-68.5-21t-71.5-7q-47 0-93 12t-87 36v394Zm-40 118q-48-38-104-59t-116-21q-42 0-82.5 11T100-198q-21 11-40.5-1T40-234v-482q0-11 5.5-21T62-752q46-24 96-36t102-12q58 0 113.5 15T480-740q51-30 106.5-45T700-800q52 0 102 12t96 36q11 5 16.5 15t5.5 21v482q0 23-19.5 35t-40.5 1q-37-20-77.5-31T700-240q-60 0-116 21t-104 59ZM280-494Z">
                </path>
            </svg>
        </button><button type="button" onclick="mypop=window.open('teso-principal.php','','');mypop.focus();"
            class="btn btn-white btn-primary-hover d-flex justify-between align-items-center">
            <span>Nueva ventana</span>
            <svg class="fill-black group-hover:fill-white w-5 h-5" xmlns="http://www.w3.org/2000/svg"
                viewBox="0 -960 960 960">
                <path
                    d="M320-120v-80h80v-80H160q-33 0-56.5-23.5T80-360v-400q0-33 23.5-56.5T160-840h640q33 0 56.5 23.5T880-760v400q0 33-23.5 56.5T800-280H560v80h80v80H320ZM160-360h640v-400H160v400Zm0 0v-400 400Z">
                </path>
            </svg>
        </button><button type="button"
            onclick="mypop=window.open('/financiero/teso-buscaactividades.php','','');mypop.focus();"
            class="btn btn-white btn-primary-hover d-flex justify-between align-items-center">
            <span class="group-hover:text-white">Duplicar pantalla</span>
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 -960 960 960">
                <path
                    d="M640-160v-360H160v360h480Zm80-200v-80h80v-360H320v200h-80v-200q0-33 23.5-56.5T320-880h480q33 0 56.5 23.5T880-800v360q0 33-23.5 56.5T800-360h-80ZM160-80q-33 0-56.5-23.5T80-160v-360q0-33 23.5-56.5T160-600h480q33 0 56.5 23.5T720-520v360q0 33-23.5 56.5T640-80H160Zm400-603ZM400-340Z">
                </path>
            </svg>
        </button><button type="button" onclick="crearexcel()"
            class="btn btn-success btn-success-hover d-flex justify-between align-items-center">
            <span>Exportar Excel</span>
            <svg class="fill-black group-hover:fill-white w-4 h-4" xmlns="http://www.w3.org/2000/svg"
                viewBox="0 0 384 512"><!--!Font Awesome Free 6.5.1 by @fontawesome - https://fontawesome.com License - https://fontawesome.com/license/free Copyright 2024 Fonticons, Inc. -->
                <path
                    d="M48 448V64c0-8.8 7.2-16 16-16H224v80c0 17.7 14.3 32 32 32h80V448c0 8.8-7.2 16-16 16H64c-8.8 0-16-7.2-16-16zM64 0C28.7 0 0 28.7 0 64V448c0 35.3 28.7 64 64 64H320c35.3 0 64-28.7 64-64V154.5c0-17-6.7-33.3-18.7-45.3L274.7 18.7C262.7 6.7 246.5 0 229.5 0H64zm90.9 233.3c-8.1-10.5-23.2-12.3-33.7-4.2s-12.3 23.2-4.2 33.7L161.6 320l-44.5 57.3c-8.1 10.5-6.3 25.5 4.2 33.7s25.5 6.3 33.7-4.2L192 359.1l37.1 47.6c8.1 10.5 23.2 12.3 33.7 4.2s12.3-23.2 4.2-33.7L222.4 320l44.5-57.3c8.1-10.5 6.3-25.5-4.2-33.7s-25.5-6.3-33.7 4.2L192 280.9l-37.1-47.6z">
                </path>
            </svg>
        </button></div>
    <form name="form2" method="post" action="teso-buscaactividades.php">
        <?php
        if ($_POST['oculto'] == '') {
            $_POST['idmod'] = '';
        }
        if ($_GET['numpag'] != "") {
            $oculto = $_POST['oculto'];
            if ($oculto != 2) {
                $_POST['numres'] = $_GET['limreg'];
                $_POST['numpos'] = $_GET['limreg'] * ($_GET['numpag'] - 1);
                $_POST['nummul'] = $_GET['numpag'] - 1;
            }
        } else {
            if ($_POST['nummul'] == "") {
                $_POST['numres'] = 10;
                $_POST['numpos'] = 0;
                $_POST['nummul'] = 0;
            }
        }
        ?>
        <input type="hidden" name="numres" id="numres" value="<?php echo $_POST['numres']; ?>" />
        <input type="hidden" name="numpos" id="numpos" value="<?php echo $_POST['numpos']; ?>" />
        <input type="hidden" name="nummul" id="nummul" value="<?php echo $_POST['nummul']; ?>" />
        <table class="inicio ancho">
            <tr>
                <td class="titulos" colspan="3">:: Buscar Actividades </td>
                <td class="cerrar" style="width:7%" onClick="location.href='teso-principal.php'">Cerrar</td>
            </tr>
            <tr>
                <td style="width:3.5cm" class="saludo1">:. C&oacute;digo o Nombre:</td>
                <td style="width:60%"><input type="text" name="nombre" value="<?php echo $_POST['nombre']; ?>"
                        style="width:100%"></td>
                <td style="padding-bottom:0px"><em class="botonflechaverde"
                        onClick="document.form2.submit();">Buscar</em></td>
            </tr>
        </table>
        <input type="hidden" name="oculto" id="oculto" value="1">
        <input type="hidden" name="idmod" id="idmod" value="<?php echo $_POST['idmod']; ?>">
        <div class="subpantalla" style='height:66vh; width:99.5%; margin-top:0px; overflow:hidden' id="divdet">
            <?php
            $sql = "SELECT MAX(version) FROM codigosciiu";
            $res = mysqli_query($linkbd, $sql);
            $row = mysqli_fetch_row($res);
            $version = $row[0];
            $sql = "SELECT MAX(version) FROM codigosciiu WHERE version <> $version";
            $res = mysqli_query($linkbd, $sql);
            $row = mysqli_fetch_row($res);
            $version2 = $row[0];
            $crit1 = "";
            if ($_POST['nombre'] != "") {
                $crit1 = "and concat_ws(' ', codigo, nombre) LIKE '%" . $_POST['nombre'] . "%'";
            }
            //sacar el consecutivo
            $sqlr = "SELECT * FROM codigosciiu WHERE  version = '$version' $crit1 ORDER BY id";
            $resp = mysqli_query($linkbd, $sqlr);
            $ntr = mysqli_num_rows($resp);
            $_POST['numtop'] = $ntr;
            echo "
					<table class='tablamv'>
						<thead>
							<tr>
								<th class='titulosnew02' style='width:60.2%;'>.: versión N°: $version</th>
								<th class='titulosnew03'>.: versión N°: $version2</th>
							</tr>
							<tr>
								<th class='titulosnew00' style='width:5%;'>División</th>
								<th class='titulosnew00' style='width:5%;'>Grupo</th>
								<th class='titulosnew00' style='width:5%;'>Clase</th>
								<th class='titulosnew00' style='width:40%;'>Nombre</th>
								<th class='titulosnew00' style='width:5%;'>Porcentaje</th>
								<th class='titulosnew01' >Nombre</th>
								<th class='titulosnew01' style='width:5%;'>Porcentaje</th>
							</tr>
						</thead>
						<tbody>";
            $iter = 'saludo1a';
            $iter2 = 'saludo2';
            $filas = 1;
            $x = 0;
            while ($row = mysqli_fetch_row($resp)) {
                if ($gidcta != "") {
                    if ($gidcta == $row[0]) {
                        $estilo = 'background-color:yellow';
                    } else {
                        $estilo = "";
                    }
                } else {
                    $estilo = "";
                }
                $idcta = "'" . $row[0] . "'";
                $numfil = "'" . $filas . "'";
                $filtro = "'" . $_POST['nombre'] . "'";
                if ($row[3] == '' && $row[4] == '' && $row[0] == '') {
                    $division1 = $row[6];
                } else {
                    $division1 = $row[3];
                }
                $grupo1 = $row[4];
                $clase1 = $row[0];
                if ($row[0] != '') {
                    $negrita = '';
                    $sololectura = "tabindex='$x' onfocusout='verificavacio($x)'";
                    $sql2 = "SELECT nombre, porcentaje FROM codigosciiu WHERE codigo = '$row[0]' AND version = '$version2'";
                    $res2 = mysqli_query($linkbd, $sql2);
                    $row2 = mysqli_fetch_row($res2);
                    $nombre2 = $row2[0];
                    if ($_POST['oculto'] == '') {
                        $_POST['vporcentaje1'][$x] = $row[2];
                        $_POST['vporcentaje2'][$x] = $row2[1];
                    }
                } else {
                    $negrita = 'font-weight:bold;';
                    $sololectura = 'readonly';
                    if ($_POST['oculto'] == '') {
                        $_POST['vporcentaje1'][$x] = '';
                        $_POST['vporcentaje2'][$x] = '';
                    }
                    $nombre2 = '';
                }
                $_POST['codpor'][$x] = $clase1;
                echo "
						<input type='hidden' name='codpor[]' value='" . $_POST['codpor'][$x] . "'>
						<tr class='$iter' style='text-transform:uppercase;$negrita $estilo' >
							<td style='text-align:center; width:5%;'>$division1</td>
							<td style='text-align:center; width:5%;'>$grupo1</td>
							<td style='text-align:center; width:5%;'>$clase1</td>
							<td style='width:40%;'>" . (strtoupper($row[1])) . "</td>
							<td style='text-align:center; width:5%;' ><input type='text' name='vporcentaje1[]' value='" . $_POST['vporcentaje1'][$x] . "' style='text-align:right;width:100%;' class='inpnovisibles' autocomplete='off' onkeypress=\"return validanumeros(event);\" ondrop=\"drop(event)\" ondragover=\"allowDrop(event)\" onchange='fcambio($x);' onclick='verificazero($x)' $sololectura></td>
							<td>$nombre2</td>
							<td style='text-align:center; width:5%;'><input type='text' name='vporcentaje2[]' value='" . $_POST['vporcentaje2'][$x] . "' style='text-align:right;width:100%;' class='inpnovisibles' autocomplete='off' draggable='true' ondragstart=\"drag(event,$x)\" readonly></td>";
                $idcta = "'" . $row[0] . "'";
                $numfil = "'" . $filas . "'";
                echo "
						</tr>";
                $aux = $iter;
                $iter = $iter2;
                $iter2 = $aux;
                $filas++;
                $x++;
            }
            echo "
						</tbody>
					</table>";
            ?>
        </div>
        <?php
        if ($_POST['oculto'] == '2') {
            $idcambio = explode(",", $_POST['idmod']);
            $numid = count($idcambio);
            for ($x = 0; $x < $numid; $x++) {
                if ($_POST['codpor'][$idcambio[$x]] != '') {
                    $sql = "UPDATE codigosciiu SET porcentaje ='" . $_POST['vporcentaje1'][$idcambio[$x]] . "' WHERE codigo = '" . $_POST['codpor'][$idcambio[$x]] . "' AND version = '$version'";
                    $res = mysqli_query($linkbd, $sql);
                    $row = mysqli_fetch_row($res);
                }
            }
        }
        ?>
    </form>
</body>

</html>
