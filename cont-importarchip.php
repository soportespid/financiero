<?php
	header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=utf8");

	require "comun.inc";
	require "funciones.inc";
	require "conversor.php";
	require "validaciones.inc";

	$linkbn_v7 = conectar_v7();
	$linkbn_v7 -> set_charset("utf8");

	session_start();
	date_default_timezone_set("America/Bogota");
?>
<!DOCTYPE >
<html lang="es">
	<head>
		<meta charset="utf-8"/>
		<meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1"/>
		<meta name="viewport" content="user-scalable=no">
		<title>:: IDEAL 10 - Informes</title>
		<link href="favicon.ico" rel="shortcut icon"/>
        <link href="css/css2.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="css/calendario.js"></script>
		<script type="text/javascript" src="css/programas.js"></script>
		<script>
			function pdf()
			{
				document.form2.action="pdfbalance.php";
				document.form2.target="_BLANK";
				document.form2.submit();
				document.form2.action="";
				document.form2.target="";
			}

			function protocolofmt()
			{
				document.form2.action="formatos/FMT_PLAN_DE_CUENTAS_CHIP.csv";
				document.form2.target="_BLANK";
				document.form2.submit();
				document.form2.action="";
				document.form2.target="";
			}

			function validar()
			{
				document.form2.oculto.value=2;
				document.form2.submit();
			}

		</script>
	</head>
	<body>
		<IFRAME src="alertas.php" name="alertas" id="alertas" style="display:none"></IFRAME>
		<span id="todastablas2"></span>
		<table>
			<tr><script>barra_imagenes("info");</script><?php cuadro_titulos();?></tr>
			<tr><?php menu_desplegable("info");?></tr>
			<tr>
				<td colspan="3" class="cinta">
					<a href="cont-importarchip.php" class="mgbt"><img src="imagenes/add.png" title="Nuevo" /></a>
					<a class="mgbt"><img src="imagenes/guardad.png"/></a>
					<a class="mgbt"><img src="imagenes/buscad.png"/></a>
					<a onClick="mypop=window.open('plan-agenda.php','','');mypop.focus()" class="mgbt"><img src="imagenes/agenda1.png" title="Agenda" /></a>
					<a onClick="<?php echo paginasnuevas("info");?>" class="mgbt"><img src="imagenes/nv.png" title="Nueva ventana"></a>
                    <a onClick="mypop=window.open('<?php echo $url2; ?>','','');mypop.focus();" class="tooltip bottom mgbt"><img src="imagenes/duplicar_pantalla.png" title="Duplicar pesta&ntilde;a"></a>
					<a onClick="pdf()" class="mgbt"><img src="imagenes/print.png" title="Imprimir"></a>
					<a href="<?php echo "formatos/FMT_PLAN_DE_CUENTAS_CGR.csv"; ?>" target="_blank" class="mgbt"><img src="imagenes/csv.png" title="Csv"></a>
					<a href="cont-gestioninformecgr.php" class="mgbt"><img src="imagenes/iratras.png" title="Atr&aacute;s"></a>
				</td>
			</tr>
		</table>
		<form action="cont-importarchip.php" method="post" enctype="multipart/form-data" name="form2">
			<table  align="center" class="inicio" >
				<tr >
					<td class="titulos" colspan="7">.: Importar Cuentas CGN</td>
					<td width="7%" class="cerrar"><a href="info-principal.php">X Cerrar</a></td>
				</tr>
				<tr>
					<td width="10%" class="saludo1">Vigencia: </td>
					<td width="10%">
						<select name="vigencia" onchange="document.form2.submit()" >
							<option value="">Sel...</option>
							<?php
							for($i=date('Y');$i>=(date('Y')-3);$i--){
								if($_POST['vigencia']==$i)
									$selected='selected="selected"';
								else
									$selected='';
								echo'<option value="'.$i.'" '.$selected.'>'.$i.'</option>';
							}
							?>
						</select>
					</td>
					<td width="10%" class="saludo1">Trimestre: </td>
					<td width="12%">
						<select name="trimestre" onchange="document.form2.submit()" >
							<option value="">Seleccione...</option>
							<option value="1" <?php if($_POST['trimestre']==1) echo 'selected="selected"'; ?> >Enero - Marzo</option>
							<option value="2" <?php if($_POST['trimestre']==2) echo 'selected="selected"'; ?> >Abril - Junio</option>
							<option value="3" <?php if($_POST['trimestre']==3) echo 'selected="selected"'; ?> >Julio - Septiembre</option>
							<option value="4" <?php if($_POST['trimestre']==4) echo 'selected="selected"'; ?> >Octubre - Diciembre</option>
						</select>
					</td>
					<td width="15%"  class="saludo1">Seleccione Archivo: </td>
					<td width="15%" >
						<input type="file" name="archivotexto" id="archivotexto">
					</td>
					<td width="10%" ></td>
				</tr>
				<tr>
					<td colspan="7" >
						<input type="button" name="generar" value="Cargar Archivo" onClick="validar()">
						<input name="oculto" type="hidden" value="1">
					</td>
				</tr>
			</table>
			<div class="subpantalla" style="height:60.5%; width:99.6%; overflow-x:hidden;">
				<?php
				//**** para sacar la consulta del balance se necesitan estos datos ********
				//**** nivel, mes inicial, mes final, cuenta inicial, cuenta final, cc inicial, cc final
				if((isset($_POST['vigencia']))&&(isset($_POST['trimestre']))){
					echo "<table class='inicio'>";
						echo "<tr>
							<td class='titulos'>CODIGO</td>
							<td class='titulos'>NOMBRE CUENTA</td>
							<td class='titulos'>SALDO FINAL</td>
						</tr>";
					if($_POST['oculto']==2)
					{
						if(is_uploaded_file($_FILES['archivotexto']['tmp_name']))
						{
							$archivo = $_FILES['archivotexto']['name'];
							$archivoF = "$archivo";
							if(move_uploaded_file($_FILES['archivotexto']['tmp_name'],$archivoF))
							{
								//echo "El archivo se subio correctamente ";
								$subio=1;
							}
						}
						//echo $archivo;
							require_once 'PHPExcel/Classes/PHPExcel.php';
							$inputFileType = PHPExcel_IOFactory::identify($archivo);
							$objReader = PHPExcel_IOFactory::createReader($inputFileType);
							$objPHPExcel = $objReader->load($archivo);
							$sheet = $objPHPExcel->getSheet(0);
							$highestRow = $sheet->getHighestRow();
                            $highestColumn = $sheet->getHighestColumn();
                            $cod=1;
                            $co='saludo1';
							$co2='saludo2';
                            $sqlr="DELETE FROM cuentasaldos WHERE vigencia='".$_POST['vigencia']."' AND trimestre='".$_POST['trimestre']."'";
							mysqli_query($linkbn_v7, $sqlr);
							for ($row = 2; $row <= $highestRow; $row++){
                                    if($cod=='1')
                                        $codigo=$sheet->getCell("A".$row)->getValue();
                                    if($codigo=='CODIGO')
                                    {
                                        $row=$row+1;
                                        echo "<tr class='$co'>";
                                        $val1=$sheet->getCell("A".$row)->getValue();
                                        $val1 = str_replace(".", "",$val1);
                                        $val2 = utf8_decode($sheet->getCell("B".$row)->getValue());
                                        $val3 = $sheet->getCell("F".$row)->getValue();
                                        $val3 = str_replace(",", ".",$val3);
                                        echo "
                                            <td>".$val1."</td>
                                            <td>".$val2."</td>
                                            <td>".$val3."</td>
                                            </tr>
                                        ";
                                        $maxid=selconsecutivo('cuentasaldos','id');
                                        $sqlr="INSERT INTO cuentasaldos (cuenta, nommbre, saldofinal, vigencia, trimestre, id) VALUES ('".$val1."','".$val2."','".$val3."','".$_POST['vigencia']."','".$_POST['trimestre']."','".$maxid."')";
                                        if(!mysqli_query($linkbn_v7, $sqlr)){
                                            echo'<tr>
                                                <td>'.mysqli_error($linkbn_v7).'</td>
                                            </tr>';
                                        }

                                        $aux=$co;
                                        $co=$co2;
                                        $co2=$aux;
                                        $row=$row-1;
                                        $cod=0;
                                    }

							}
					}
					else{
						$sql="select * from cuentasaldos where vigencia='".$_POST['vigencia']."' and trimestre='".$_POST['trimestre']."' order by cuenta";
						$res=mysqli_query($linkbn_v7, $sql);
						while($row=mysqli_fetch_array($res)){
							echo'<tr>
								<td>'.$row[0].'</td>
								<td>'.utf8_encode($row[1]).'</td>
								<td align="right">'.number_format($row[2],2,',','.').'</td>
							</tr>';
						}
					}
				echo "</table>";
				}
				else{
				echo "<table class='inicio'>";
					echo "<tr>
						<td class='titulos'>SELECCIONES LA VIGENCIA Y EL TRIMESTRE</td>
					</tr>
				<table>";
				}
				?>
			</div>
		</form>
	</body>
</html>
