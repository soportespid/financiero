<?php
	header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=uft8");
	require "comun.inc";
	require "funciones.inc";
    require "funcionesSP.inc.php";
	session_start();
	$linkbd=conectar_v7();
	$linkbd -> set_charset("utf8");
	cargarcodigopag($_GET['codpag'],$_SESSION["nivel"]);
	date_default_timezone_set("America/Bogota");
?>
<!DOCTYPE >
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="es">
	<head>
    <meta charset="utf-8"/>
		<meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1"/>
		<meta name="viewport" content="user-scalable=no">
		<title>:: IDEAL 10 - Servicios Públicos</title>
		<link href="favicon.ico" rel="shortcut icon"/>
		<link href="css/css2z.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
		<link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
		<link href="css/css4.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
        <link href="css/tabs.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css"/>
		<link href="css/style.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css"/>
        <link rel="stylesheet" href="sweetalert2/dist/sweetalert2.min.css">
        <script src="sweetalert2/dist/sweetalert2.min.js"></script>
		<script src="css/programas.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script src="css/calendario.js?<?php echo date('d_m_Y_h_i_s');?>"></script>

		<script>
			$(window).load(function () {
				$('#cargando').hide();
			});
			
			function verUltimaPos(cliente, factura)
			{
				location.href="serv-visualizarFacturaNew.php?cliente="+cliente+"&factura="+factura;
			}

            function limbusquedas1()
            {
				var numeroFacturas = document.getElementById('numtop').value;
				
				if (numeroFacturas == '') {
						
					document.form2.bandera.value=1;
					document.form2.submit();
				
				}
				else {
					document.getElementById('numtop').value = '';
					document.form2.bandera.value=0;
					document.form2.submit();
				}
					
            }
		</script>
		<?php 
			titlepag();
			$scrtop= @ $_GET['scrtop'];
			if($scrtop=="") $scrtop=0;
			echo"<script>
				window.onload=function(){
					$('#divdet').scrollTop(".$scrtop.")
				}
			</script>";
			$gidcta=@ $_GET['idcta'];
			if(isset($_GET['filtro']))
			$_POST['nombre']=$_GET['filtro'];
		?>
	</head>
	<body>
		<IFRAME src="alertas.php" name="alertas" id="alertas" style="display:none"></IFRAME>
		<span id="todastablas2"></span>
		<table>
			<tr><script>barra_imagenes("serv");</script><?php cuadro_titulos();?></tr>	 
			<tr><?php menu_desplegable("serv");?></tr>
			<tr>
				<td colspan="3" class="cinta">
					<a class="mgbt"><img src="imagenes/add2.png"/></a>
					<a class="mgbt"><img src="imagenes/guardad.png" title="Guardar"/></a>
					<a href="serv-buscarLiquidacionNew.php" class="mgbt"><img src="imagenes/busca.png" title="Buscar"/></a>
					<a onclick="mypop=window.open('plan-agenda.php','','');mypop.focus()" class="mgbt"><img src="imagenes/agenda1.png" title="Agenda" /></a>
					<a onclick="mypop=window.open('serv-principal.php','','');mypop.focus();" class="mgbt"><img src="imagenes/nv.png" title="Nueva ventana"></a>
					<a onclick="mypop=window.open('<?php echo $url2; ?>','','');mypop.focus();" class="mgbt"><img src="imagenes/duplicar_pantalla.png" title="Duplicar pesta&ntilde;a"></a>
                </td>
			</tr>
		</table>
		<?php
			if(@ $_POST['oculto']=="")
			{
				$_POST['numres']=10;$_POST['numpos']=0;$_POST['nummul']=0;
			}
			if(@ $_GET['numpag']!="")
			{
				if(@ $_POST['oculto']!=2)
				{
					$_POST['numres']=$_GET['limreg'];
					$_POST['numpos']=$_GET['limreg']*($_GET['numpag']-1);
					$_POST['nummul']=$_GET['numpag']-1;
				}
			}
			else
			{
				if(@ $_POST['nummul']=="")
				{
					$_POST['numres']=10;
					$_POST['numpos']=0;
					$_POST['nummul']=0;
				}
			}
		?>
		<div id="bgventanamodalm" class="bgventanamodalm">
			<div id="ventanamodalm" class="ventanamodalm">
				<IFRAME src="" name="ventanam" marginWidth=0 marginHeight=0 frameBorder=0 id="ventanam" frameSpacing=0 style=" width:700px; height:130px; top:200; overflow:hidden;"></IFRAME>
			</div>
		</div>
		<form name="form2" method="post" action="">
			<input type="hidden" name="numres" id="numres" value="<?php echo @ $_POST['numres'];?>"/>
			<input type="hidden" name="numpos" id="numpos" value="<?php echo @ $_POST['numpos'];?>"/>
			<input type="hidden" name="nummul" id="nummul" value="<?php echo @ $_POST['nummul'];?>"/>
			<input type="hidden" name="oculto" id="oculto" value="1">
            <input type="hidden" name="bandera" id="bandera" value="<?php echo @ $_POST['bandera'];?>">
			
			<table class="inicio">
				<tr>
					<td class="titulos" colspan="9">:: Buscar Factura</td>
					<td class="cerrar" style="width:7%" onClick="location.href='serv-principal.php'">Cerrar</td>
				</tr>
				<tr>
					<td class="tamano01" style='width:3cm;'>:: Código Factura:</td>
					<td>
                        <input type="search" name="codigoFactura" id="codigoFactura" value="<?php echo @ $_POST['codigoFactura'];?>" style='width:100%;height:30px;' onkeydown = "if (event.keyCode == 13){document.getElementById('filtro').click()}"; autocomplete="off"/>
                    </td>

                    <td class="tamano01" style='width:3cm;'>:: Código Usuario:</td>

                    <td>
                        <input type="search" name="codigoUsuario" id="codigoUsuario" value="<?php echo @ $_POST['codigoUsuario'];?>" style='width:100%;height:30px;' onkeydown = "if (event.keyCode == 13){document.getElementById('filtro').click()}"; autocomplete="off"/>
                    </td>

					<td class="tamano01" style='width:3cm;'>:: Nombre Usuario:</td>

                    <td>
                        <input type="search" name="nombreUsuario" id="nombreUsuario" value="<?php echo @ $_POST['nombreUsuario'];?>" style='width:100%;height:30px;' onkeydown = "if (event.keyCode == 13){document.getElementById('filtro').click()}"; autocomplete="off"/>
                    </td>
                    
					<td style="padding-bottom:0px"><em class="botonflecha" id="filtro" onClick="limbusquedas1();">Buscar</em></td>
					
				</tr>

			</table>

			<div class='subpantalla' style='height:64vh; width:99.2%; margin-top:0px; overflow-x:hidden; resize: vertical;'>
                <table class='inicio'>
                    <thead>
                        <tr>
                            <th class="titulosnew00" style="width:5%;">Corte</th>
                            <th class="titulosnew00" style="width:15%;">Periodo</th>
                            <th class="titulosnew00" style="width:10%;">Número factura</th>
                            <th class="titulosnew00" style="width:10%;">Cod usuario</th>
                            <th class="titulosnew00">Nombre suscriptor</th>
                            <th class="titulosnew00" style="width:20%;">Dirección</th>
                            <th class="titulosnew00" style="width:8%;">Total Factura</th>
                            <th class="titulosnew00" style="width:5%;">Estado</th>
                        </tr>
                    </thead>

                    <?php
                    if(isset($_POST['codigoUsuario']) && ($_POST['codigoUsuario'] <> "")) {
                        $crit1 = "AND concat_ws('', C.cod_usuario) = '".$_POST['codigoUsuario']."' ";
                    }
                    
                    if(isset($_POST['codigoFactura']) && ($_POST['codigoFactura'] <> "")) {
                        $crit1 = "AND D.numero_facturacion = '".$_POST['codigoFactura']."' ";   
                    }
                    
                    if (isset($_POST['nombreUsuario']) && ($_POST['nombreUsuario'] <> "")) {

                        $nombredividido = array();
                        $nombredividido = explode(" ", $_POST['nombreUsuario']);
                        
                        for ($i=0; $i < count($nombredividido); $i++) 
                        { 
                            $busqueda = '';
                            $busqueda = "AND concat_ws(' ', T.nombre1, T.nombre2, T.apellido1, T.apellido2, T.razonsocial) LIKE '%$nombredividido[$i]%' ";

                            $crit1 = $crit1 . $busqueda;
                        }
                    }

                    if (isset($crit1) && ($crit1 <> "")) {

                        $sql = "SET lc_time_names = 'es_ES'";
                        mysqli_query($linkbd,$sql);

                        $sqlr = "SELECT C.id, D.id_cliente, D.id_corte, D.numero_facturacion, D.estado_pago, C.id_tercero, C.cod_usuario, T.nombre1, T.nombre2, T.apellido1, T.apellido2, T.razonsocial, UPPER(MONTHNAME(P.fecha_inicial)), UPPER(MONTHNAME(P.fecha_final)) FROM srvcortes_detalle AS D INNER JOIN srvclientes AS C ON D.id_cliente = C.id INNER JOIN terceros AS T ON C.id_tercero = T.id_tercero INNER JOIN srvcortes AS P ON D.id_corte = P.numero_corte WHERE D.id_corte != '' $crit1 ORDER BY numero_facturacion DESC";
                        $resp = mysqli_query($linkbd,$sqlr);
                        $informacionEncontrada = mysqli_num_rows($resp);

                        if ($informacionEncontrada > 0) {

                            while ($row = mysqli_fetch_row($resp)) {
                                if($row[11] != '') {
                                    $nombre = $row[11];
                                }
                                else {
                                    $nombre = $row[7].' '.$row[8].' '.$row[9].' '.$row[10];
                                }
    
                                $sqlDireccion = "SELECT direccion FROM srvdireccion_cliente WHERE id_cliente = '$row[1]' ";
                                $respDireccion = mysqli_query($linkbd,$sqlDireccion);
                                $rowDireccion = mysqli_fetch_row($respDireccion);
    
                                $sqlDetallesFacturacion = "SELECT SUM(credito), SUM(debito) FROM srvdetalles_facturacion WHERE id_cliente = '$row[1]' AND numero_facturacion = $row[3] AND corte = '$row[2]' AND tipo_movimiento = '101'";
                                $respDetallesFacturacion = mysqli_query($linkbd,$sqlDetallesFacturacion);
                                $rowDetallesFacturacion = mysqli_fetch_row($respDetallesFacturacion);
                                
                                $totalFactura = $rowDetallesFacturacion[0] - $rowDetallesFacturacion[1];
                                $totalFactura = ($totalFactura / 100);
                                $totalFactura = ceil($totalFactura);
                                $totalFactura = $totalFactura * 100;
    
                                $periodo = $row[12] . " - " . $row[13];
    
                                switch ($row[4]) 
                                {
                                    case 'S': 	$imgsem='<span style="padding:4px; border-radius:5px; background:#FFFF00;color:#000;font-weight:bold">PAGO PENDIENTE</span>';
                                                break;
                                    case 'P':	$imgsem='<span style="padding:4px; border-radius:5px; background:#008000;color:#fff;font-weight:bold">PAGO REALIZADO</span>';
                                                break;
                                    case 'V':	$imgsem='<span style="padding:4px; border-radius:5px; background:#FF0000;color:#fff;font-weight:bold">VENCIDO</span>';
                                                break;
									case 'N':	$imgsem='<span style="padding:4px; border-radius:5px; background:#FF0000;color:#fff;font-weight:bold">ANULADO</span>';
									break;
                                    case 'A':	$imgsem='<span style="padding:4px; border-radius:5px; background:#0000ff;color:#fff;font-weight:bold">ACUERDO DE PAGO</span>'
                                                ;break;
									case 'R': $imgsem='<span style="padding:4px; border-radius:5px; background:#808080;color:#fff;font-weight:bold">REVERSADO</span>';
												break;
									case 'PC': $imgsem='<span style="padding:4px; border-radius:5px; background:#808080;color:#fff;font-weight:bold">PRESCRIPCIÓN</span>';
												break;
                                    default:	$imgsem="";
                                }
                                    $iter='saludo1a';
                                    $iter2='saludo1a';
                                ?>
                                <tbody>
                                    <tr style="height:50px;" class="<?php echo $iter?>" ondblclick="verUltimaPos(<?php echo $row[0]?>, <?php echo $row[3] ?>)">
                                        <td style="width:5%; text-align:center; "><?php echo $row[2] ?></td>
                                        <td style="width:15%; text-align:center; "><?php echo $periodo ?></td>
                                        <td style="width:10%; text-align:center; "><?php echo $row[3] ?></td>
                                        <td style="width:10%; text-align:center; "><?php echo $row[6] ?></td>
                                        <td style="text-align:left; "><?php echo $nombre ?></td>
                                        <td style="width:20%; text-align:center; "><?php echo $rowDireccion[0] ?></td>
                                        <td style="width:8%; text-align:center; "><?php echo "$ ".number_format($totalFactura,$_SESSION["ndecimales"],$_SESSION["spdecimal"],$_SESSION["spmillares"]) ?></td>
                                        <td style="width:10%; text-align:center; "><?php echo "<a href='serv-visualizarFacturaNew.php?cliente=$row[0]&factura=$row[3]'>  $imgsem </a>" ?></td>
                                    </tr>
                                </tbody>
    
                                <?php
                    
                                $aux = $iter;
                                $iter = $iter2;
                                $iter2 = $aux;
                                $filas++;
                            }
                        }
                        else { 
                            echo "
							<table class='inicio'>
								<tr>
									<td class='saludo1' style='text-align:center;width:100%'><img src='imagenes\alert.png' style='width:25px'>No hay coincidencias en la busqueda<img src='imagenes\alert.png' style='width:25px'></td>
								</tr>
							</table>";
                        }
                    }
                    else {
                        echo
                        "<table class='inicio'>
                            <tr>
                                <td class='saludo1' style='text-align:center;width:100%;font-size:25px'>Utilice el filtro de busqueda</td>
                            </tr>
                        </table>";
                    }
                    ?>
                </table>		
            </div>

			<input type="hidden" name="ocules" id="ocules" value="<?php echo @ $_POST['ocules'];?>">
			<input type="hidden" name="actdes" id="actdes" value="<?php echo @ $_POST['actdes'];?>">
			<input type="hidden" name="numtop" id="numtop" value="<?php echo @ $_POST['numtop'];?>" />
		</form>
		<div id="bgventanamodal2">
			<div id="ventanamodal2">
				<IFRAME  src="" name="buster" marginWidth=0 marginHeight=0 frameBorder=0 id="ventana2" frameSpacing=0 style="left:500px; width:900px; height:500px; top:200;"></IFRAME>
			</div>
		</div>
	</body>
</html>