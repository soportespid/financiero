<?php

    header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=utf8");
	require 'comun.inc';
	require 'funciones.inc';
	session_start();
    if(empty($_SESSION)){
        header("location: index.php");
    }
	$linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");
	cargarcodigopag($_GET['codpag'],$_SESSION["nivel"]);
	date_default_timezone_set("America/Bogota");
?>
<!DOCTYPE >
<html lang=es>
	<head>
    <meta charset="utf-8"/>
		<meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1"/>
		<meta name="viewport" content="user-scalable=no">
		<title>:: IDEAL 10 - Presupuesto</title>
		<link href="favicon.ico" rel="shortcut icon"/>
		<link href="css/css2z.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
		<link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
		<link href="css/css4.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
        <link href="css/tabs.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css"/>
        <link href="css/style.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css"/>
        <link rel="stylesheet" href="sweetalert2/dist/sweetalert2.min.css">
        <script type="text/javascript" src="css/programas.js"></script>
        <script src="sweetalert2/dist/sweetalert2.min.js"></script>
		<script src="vue/vue.min.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script src="css/programas.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script src="css/calendario.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
        <style>[v-cloak]{display : none;}</style>
	</head>
	<body>
		<header>
			<table>
				<tr><script>barra_imagenes("ccpet");</script><?php cuadro_titulos();?></tr>
			</table>
		</header>

        <section id="myapp" v-cloak>
            <input type="hidden" value = "1" ref="pageType">
            <div class="loading-container" v-show="isLoading" >
                <p class="text-loading"data-loading="IDEAL 10">IDEAL 10</p>
            </div>
            <nav>
                <table>
                    <tr><?php menu_desplegable("ccpet");?></tr>
                </table>
                <div class="bg-white group-btn p-1" id="newNavStyle">
                    <button type="button" class="btn btn-white btn-primary-hover d-flex justify-between align-items-center" @click="window.location.reload()">
                        <span>Nuevo</span>
                        <svg viewBox="0 -960 960 960" ><path d="M440-440H200v-80h240v-240h80v240h240v80H520v240h-80v-240Z"/></svg>
                    </button>
                    <button type="button" class="btn btn-white btn-primary-hover d-flex justify-between align-items-center" @click="save()">
                        <span>Guardar</span>
                        <svg viewBox="0 -960 960 960" ><path d="M840-680v480q0 33-23.5 56.5T760-120H200q-33 0-56.5-23.5T120-200v-560q0-33 23.5-56.5T200-840h480l160 160Zm-80 34L646-760H200v560h560v-446ZM480-240q50 0 85-35t35-85q0-50-35-85t-85-35q-50 0-85 35t-35 85q0 50 35 85t85 35ZM240-560h360v-160H240v160Zm-40-86v446-560 114Z"/></svg>
                    </button>
                    <button type="button" class="btn btn-white btn-primary-hover d-flex justify-between align-items-center" @click="location.href='plan-buscaSolicitudCdp'">
                        <span>Buscar</span>
                        <svg class="fill-black group-hover:fill-white w-5 h-5" viewBox="0 -960 960 960" ><path d="M784-120 532-372q-30 24-69 38t-83 14q-109 0-184.5-75.5T120-580q0-109 75.5-184.5T380-840q109 0 184.5 75.5T640-580q0 44-14 83t-38 69l252 252-56 56ZM380-400q75 0 127.5-52.5T560-580q0-75-52.5-127.5T380-760q-75 0-127.5 52.5T200-580q0 75 52.5 127.5T380-400Z"/></svg>
                    </button>
                    <button type="button" class="btn btn-white btn-primary-hover d-flex justify-between align-items-center" @click="mypop=window.open('ccp-principal.php','',''); mypop.focus();">
                        <span>Nueva ventana</span>
                        <svg class="fill-black group-hover:fill-white w-5 h-5" viewBox="0 -960 960 960" ><path d="M320-120v-80h80v-80H160q-33 0-56.5-23.5T80-360v-400q0-33 23.5-56.5T160-840h640q33 0 56.5 23.5T880-760v400q0 33-23.5 56.5T800-280H560v80h80v80H320ZM160-360h640v-400H160v400Zm0 0v-400 400Z"/></svg>
                    </button>
                    <!-- <button type="button" class="btn btn-success d-flex justify-between align-items-center" @click="window.location.href='plan-menuPlanAnualAdquisiciones.php'">
                        <span>Atrás</span>
                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 -960 960 960"><path d="M760-200v-160q0-50-35-85t-85-35H273l144 144-57 56-240-240 240-240 57 56-144 144h367q83 0 141.5 58.5T840-360v160h-80Z"/></svg>
                    </button> -->
                </div>
            </nav>
            <article>
                <div class="bg-white">
                    <div>
                        <h2 class="titulos m-0">Solicitud de CDP</h2>
                        
                        <div class="d-flex">
                            <div class="form-control w-25">
                                <label class="form-label m-0" for="">Fecha solicitud<span class="text-danger fw-bolder">*</span></label>
                                <input type="date" class="text-center" v-model="fechaSolicitud" @change="sumaDias()">
                            </div>

                            <div class="form-control w-25">
                                <label class="form-label m-0" for="">Fecha de vencimiento</label>
                                <input type="date" class="text-center" v-model="fechaVencimiento" readonly>
                            </div>

                            <div class="form-control w-25">
                                <label class="form-label m-0" for="">Código PAA<span class="text-danger fw-bolder">*</span></label>
                                <input type="text" class="text-center colordobleclik" v-model="codPaa" @dblclick="modalPaa=true;" readonly>
                            </div>

                            <div class="form-control w-50">
                                <label class="form-label m-0" for="">Modalidad de contratación</label>
                                <input type="text" v-model="modalidad" readonly>
                            </div>

                            <div class="form-control justify-between">
                                <label class="form-label m-0" for=""></label>
                                <input type="text" v-model="nomPaa" readonly>
                            </div>
                        </div>

                        <div class="d-flex">
                            
                            <div class="form-control w-25">
                                <label class="form-label m-0" for="">Dependencia<span class="text-danger fw-bolder">*</span></label>
                                <select v-model="codDependencia" @change="calcularSaldo()">
                                    <option value="">Seleccione</option>
                                    <option v-for="seccion in secciones" :value="seccion">{{seccion.nombre}}</option>
                                </select>
                            </div>

                            <div class="form-control w-25">
                                <label class="form-label m-0" for="">Tipo de presupuesto<span class="text-danger fw-bolder">*</span></label>
                                <select v-model="tipoPresupuesto" @change="clearTipoPresupuesto(); clearCuentas(); limpiarProyecto();">
                                    <option value="">Seleccione</option>
                                    <option value="1">Funcionamiento</option>
                                    <option value="2">Inversión</option>
                                    <option value="3">Funcionamiento/Inversión</option>
                                </select>
                            </div>

                            <div class="form-control w-25" v-show="tipoPresupuesto==1">
                                <label class="form-label m-0" for="">Tipo de gasto<span class="text-danger fw-bolder">*</span></label>
                                <select v-model="tipoGasto" @change="clearCuentas();">
                                    <option value="">Seleccione</option>
                                    <option v-for="tipo in tipoGastos" :value="tipo.codigo">{{tipo.codigo}}-{{tipo.nombre}}</option>
                                </select>
                            </div>

                            <div class="form-control w-25" v-show="tipoPresupuesto==2">
                                <label class="form-label m-0" for="">Sector:</label>
                                <input type="text" class="text-center colordobleclik" v-model="codSector" @dblclick="isModalRubro=true;" readonly>
                            </div>

                            <div class="form-control justify-between w-25" v-show="tipoPresupuesto==2">
                                <label class="form-label m-0" for=""></label>
                                <!-- <input type="text" v-model="nombreBpinSeleccionado" readonly> -->
                                <input type="text" v-model="nomSector" readonly>
                            </div>


                            <div class="form-control w-25">
                                <label class="form-label m-0" for="">Contrato / Act. Adm<span class="text-danger fw-bolder">*</span></label>
                                <select v-model="tipoDocumento" @change="clearTipoDocumento()">
                                    <option value="">Seleccione</option>
                                    <option value="1">Tipo de contrato</option>
                                    <option value="2">Acto administrativo</option>
                                </select>
                            </div>

                            <div class="form-control w-25" v-show="tipoDocumento==1">
                                <label class="form-label m-0" for="">Tipo de contrato<span class="text-danger fw-bolder">*</span></label>
                                <select v-model="tipoContrato">
                                    <option value="">Seleccione</option>
                                    <option value="1">Obra</option>
                                    <option value="2">Consultora servicio</option>
                                    <option value="3">Suministro y/o compraventa</option>
                                    <option value="4">Prestacion de servicios</option>
                                    <option value="5">Otro</option>
                                </select>
                            </div>

                            <div class="form-control w-25" v-show="tipoDocumento==2">
                                <label class="form-label m-0" for="">Número acto<span class="text-danger fw-bolder">*</span></label>
                                <input type="text" class="text-center" v-model="numeroActo">
                            </div>

                            <div class="form-control w-25" v-show="tipoDocumento==2">
                                <label class="form-label m-0" for="">Fecha acto<span class="text-danger fw-bolder">*</span></label>
                                <input type="date" class="text-center" v-model="fechaActo">
                            </div>
                        </div>

                        <div class="d-flex">
                            <div class="form-control">
                                <label class="form-label m-0" for="">Objeto<span class="text-danger fw-bolder">*</span></label>
                                <input type="text" v-model="objeto">
                            </div>
                            <div class="form-control w-25">
                                <label class="form-label m-0" for="">Valor Solicitud: <span class="text-danger fw-bolder">*</span></label>
                                
                                <div class="form-number number-primary">
                                    <span></span>
                                    <input type="text" class="text-right" v-model="valorDisponibilidadFormat" @input="formatInputNumber('valorDisponibilidadFormat', 'valorDisponibilidad')">
                                </div>
                            </div>
                        </div>
                    </div>         
                    
                    <div>
                        <h5 class="titulos m-0">Cuenta CCPET y clasificadores complementarios</h5>
                        <div class="d-flex">
                            <div class="form-control w-25" v-show="tipoPresupuesto==3">
                                <label class="form-label m-0" for="">Tipo de presupuesto<span class="text-danger fw-bolder">*</span></label>
                                <select v-model="tipoPresuDet" @change="clearCuentas">
                                    <option value="">Seleccione</option>
                                    <option value="1">Funcionamiento</option>
                                    <option value="2">Inversión</option>
                                </select>
                            </div>
                            <div class="form-control w-25" v-show="tipoPresuDet==2">            
                                <label class="form-label m-0" for="">Sector:</label>
                                <input type="text" class="text-center colordobleclik" v-model="codSector" @dblclick="isModalRubro=true;" readonly>
                            </div>
                        </div>

                        <div class="d-flex">
                            
                            <div class="form-control w-25">
                                <label class="form-label m-0" for="">Area solicitante<span class="text-danger fw-bolder">*</span></label>
                                <select v-model="codArea" @change="calcularSaldo()">
                                    <option value="">Seleccione</option>
                                    <option v-for="area in areas" :value="area.codigo">{{area.nombre}}</option>
                                </select>
                            </div>

                            <div class="form-control w-25">
                                <label class="form-label m-0" for="">Medio de pago<span class="text-danger fw-bolder">*</span></label>
                                <select v-model="medioPago" @change="calcularSaldo()">
                                    <option value="CSF">CSF</option>
                                    <option value="SSF">SSF</option>
                                </select>
                            </div>

                            <div class="form-control w-25">
                                <label class="form-label m-0" for="">Vigencia gasto<span class="text-danger fw-bolder">*</span></label>
                                <select v-model="vigGasto" @change="calcularSaldo()">
                                    <option value="">Seleccione</option>
                                    <option v-for="vig in vigenciasGastos" :value="vig">{{vig.nombre}}</option>
                                </select>
                            </div> 

                            <div class="form-control w-25" v-show="tipoPresupuesto==1 || tipoPresuDet==1">
                                <label class="form-label m-0" for="">Rubro presupuestal<span class="text-danger fw-bolder">*</span></label>
                                <input type="text" class="text-center colordobleclik" v-model="rubroPresupuestal" @dblclick="deployModal('rubroPresupuestal');" readonly>
                            </div>
                        </div>

                        <div class="d-flex">
                            <div class="form-control" v-show="tipoPresupuesto==2 || tipoPresuDet==2">
                                <label class="form-label m-0" for="">Sector<span class="text-danger fw-bolder">*</span></label>
                                <input type="text" class="text-center" v-model="codSector" readonly>
                            </div>
                            
                            <div class="form-control" v-show="tipoPresupuesto==2 || tipoPresuDet==2">
                                <label class="form-label m-0" for="">Programa<span class="text-danger fw-bolder">*</span></label>
                                <input type="text" class="text-center" v-model="codPrograma"   readonly>
                            </div>

                            <div class="form-control" v-show="tipoPresupuesto==2 || tipoPresuDet==2">
                                <label class="form-label m-0" for="">Producto<span class="text-danger fw-bolder">*</span></label>
                                <input type="text" class="text-center" v-model="codProducto" readonly>
                            </div>

                            <div class="form-control" v-show="tipoPresupuesto==2 || tipoPresuDet==2">
                                <label class="form-label m-0" for="">Indicador<span class="text-danger fw-bolder">*</span></label>
                                <input type="text" class="text-center" v-model="codIndicador"  readonly>
                            </div>
                        </div>

                        <div class="d-flex">
                            <div class="form-control w-50" v-show="tipoPresupuesto==2 || tipoPresuDet==2">
                                <label class="form-label m-0" for="">BPIN<span class="text-danger fw-bolder">*</span></label>
                                <input type="text" class="text-center" v-model="codBpin" readonly>
                            </div>

                            <div class="form-control w-25">
                                <label class="form-label m-0" for="">Fuente:<span class="text-danger fw-bolder"> * </span></label>
                                <input type="text" class="text-center colordobleclik" v-model="codFuente" @dblclick="modalFuentes=true;" readonly>
                            </div>

                            <div class="form-control w-25">
                                <label class="form-label m-0" for="">Cuenta CCPET<span class="text-danger fw-bolder">*</span></label>
                                <input type="text" class="text-center colordobleclik" v-model="cuenta" @dblclick="deployModal('cuentaccpet')" readonly>
                            </div>

                            <div class="form-control w-15">
                                <label class="form-label m-0" for="">CPC</label>
                                <input type="text" class="text-center colordobleclik" v-model="cpc" @dblclick="deployModal('cpc')" readonly>
                            </div>

                            <div class="form-control w-15">
                                <label class="form-label m-0" for="">Detalle sectorial<span class="text-danger fw-bolder">*</span></label>
                                <input type="text" class="text-center colordobleclik" v-model="codigoSectorial" @dblclick="modalDetalleSectorial=true;" readonly>
                            </div>
                            <div class="form-control w-15">
                                <label class="form-label m-0" for="">Sectorial gasto<span class="text-danger fw-bolder">*</span></label>
                                <input type="text" class="text-center colordobleclik" v-model="codGastoSectorial" @dblclick="modalDetalleSectorialGasto=true;" readonly>
                            </div>
                            <div class="form-control w-15" v-show="tipoPresupuesto==2 || tipoPresuDet==2">
                                <label class="form-label m-0" for="">Politica pública<span class="text-danger fw-bolder">*</span></label>
                                <input type="text" class="text-center colordobleclik" v-model="codPoliticaPublica" @dblclick="modalPoliticaPublica=true;" readonly>
                            </div>

                            <div class="form-control w-25">
                                <label class="form-label m-0" for="">Saldo</label>
                                <div class="form-number number-primary">
                                    <span></span>
                                    <input type="text" class="text-right" v-model="viewFormatNumber(saldo)" readonly>
                                </div>
                            </div>

                            <div class="form-control w-25">
                                <label class="form-label m-0" for="">Valor<span class="text-danger fw-bolder">*</span></label>
                                <div class="form-number number-primary">
                                    <span></span>
                                    <input type="text" class="text-right" v-model="valorFormat" @input="formatInputNumber('valorFormat', 'valor')">
                                </div>
                            </div>

                            <div class="form-control justify-between w-15">
                                <label class="form-label m-0" for=""></label>
                                <button type="button" @click="addDet()" class="btn btn-primary">Agregar</button>
                            </div>
                        </div>
                    </div>

                    <div class="table-responsive" style="height: 30%;">
                        <table class="table table-hover fw-normal">
                            <thead>
                                <tr class="text-center">
                                    <th>Tipo de presupuesto</th>
                                    <th>Dependendencia</th>
                                    <th>Medio pago</th>
                                    <th>Vig de gasto</th>
                                    <th>Cuenta</th>
                                    <th>Fuente</th>
                                    <th>CPC</th>
                                    <th>Indicador</th>
                                    <th>BPIN</th>
                                    <th>Detalle sectorial</th>
                                    <th>Sectorial Gasto</th>
                                    <th>Politica</th>
                                    <th>Valor</th>
                                    <th>Opciones</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr tr v-for="(det,index) in arrayDetalles" :key="index" class="text-center">
                                    <td>{{(det.tipoPresupuesto == 1) ? "Funcionamiento" : (det.tipoPresupuesto == 2) ? "Inversión" : "Fun/inv"}}</td>
                                    <td>{{det.nameDependencia}}</td>
                                    <td>{{det.medioPago}}</td>
                                    <td>{{det.nameVigGasto}}</td>
                                    <td>{{det.cuenta}}</td>
                                    <td>{{det.codFuente}}</td>
                                    <td>{{det.cpc}}</td>
                                    <td>{{det.codIndicador}}</td>
                                    <td>{{det.bpin}}</td>
                                    <td>{{det.codSectorial}}</td>
                                    <td>{{det.codGastoSectorial}}</td>
                                    <td>{{det.codPoliticaPublica}}</td>
                                    <td>{{viewFormatNumber(det.valor)}}</td>
                                    <td>
                                        <button type="button" class="btn btn-danger btn-sm m-0" @click="delDet(index)">Eliminar</button>
                                    </td>
                                </tr>
                            </tbody>
                            <tfoot>
                                <tr class="fw-bold">
                                    <td class="text-right" colspan="12">Total:</td>
                                    <td class="text-right">{{viewFormatNumber(valorTotalDet)}}</td>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>

                <!-- modales -->
                <div v-show="modalPaa" class="modal">
                    <div class="modal-dialog modal-xl">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title">Plan anual de adquisiciones</h5>
                                <button type="button" @click="modalPaa=false;" class="btn btn-close"><div></div><div></div></button>
                            </div>
                            <div class="modal-body">
                                <div class="d-flex">
                                    <div class="form-control justify-between">
                                        <label class="form-label m-0" for=""></label>
                                        <input type="search" v-model="txtSearch" @keyup="searchData('paa')" placeholder="Busca código o descripción">
                                    </div>
                                    <div class="form-control">
                                        <label class="form-label m-0" for="">Responsable</label>
                                        <select v-model="respon" @change="filtroResponsable()">
                                            <option value="">Seleccione</option>
                                            <option v-for="responsable in responsables" :value="responsable.responsable">{{responsable.responsable}}</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="overflow-auto max-vh-50 overflow-x-hidden" >
                                    <table class="table table-hover fw-normal">
                                        <thead>
                                            <tr class="text-center">
                                                <th>Código</th>
                                                <th>Descripción</th>
                                                <th>Modalidad</th>
                                                <th>Fuente</th>
                                                <th>Valor total estimado</th>
                                                <th>Valor vigencia</th>
                                                <th>Responsable</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr v-for="(paa,index) in paa" :key="index" @click="selectItem('paa', paa)">
                                                <td class="text-center">{{paa.codigo}}</td>
                                                <td>{{paa.descripcion}}</td>
                                                <td>{{paa.nombre_modalidad}}</td>
                                                <td>{{paa.nombre_fuente}}</td>
                                                <td class="text-right">{{viewFormatNumber(paa.valor_total)}}</td>
                                                <td class="text-right">{{viewFormatNumber(paa.valor_vigencia)}}</td>
                                                <td>{{paa.responsable}}</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div v-show="isModalRubro" class="modal">
                    <div class="modal-dialog modal-xl" >
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title">Buscar proyectos</h5>
                                <button type="button" @click="isModalRubro=false;" class="btn btn-close"><div></div><div></div></button>
                            </div>
                            
                            <div class="modal-body">
                                <div class="d-flex flex-column">
                                    <div class="form-control m-0 mb-3">
                                        <input type="search" v-model="txtSearch" @keyup="searchData('modalProyectos')" placeholder="Busca por proyecto, nombre o fuente">
                                    </div>
                                </div>

                                <div class="d-flex">
                                    <div class="form-control" >
                                        <label class="form-label m-0" for="">Sector:</label>
                                        <select @change="filter('sector')" v-model="selectSector">
                                            <option value="0"  selected>Seleccione</option>
                                            <option v-for="(data,index) in arrSectores" :key="index" :value="data.codigo">
                                                {{data.codigo+"-"+data.nombre}}
                                            </option>
                                        </select>
                                    </div>
                                    <div class="form-control" >
                                        <label class="form-label m-0" for="">Programa:</label>
                                        <select @change="filter('programa')"  v-model="selectPrograma">
                                            <option value="0"  selected>Seleccione</option>
                                            <option v-for="(data,index) in arrProgramas" :key="index" :value="data.codigo">
                                                {{data.codigo+"-"+data.nombre}}
                                            </option>
                                        </select>
                                    </div>
                                    <div class="form-control" >
                                        <label class="form-label m-0" for="">Producto:</label>
                                        <select @change="filter('producto')"  v-model="selectProducto">
                                            <option value="0" selected>Seleccione</option>
                                            <option v-for="(data,index) in arrProductos" :key="index" :value="data.codigo">
                                                {{data.codigo+"-"+data.nombre}}
                                            </option>
                                        </select>
                                    </div>
                                </div>

                                <div class="overflow-auto max-vh-50 overflow-x-hidden " >
                                    <table class="table table-hover fw-normal p-2">
                                        <thead>
                                            <tr>
                                                <th>Sector</th>
                                                <th>Nombre sector</th>
                                                <th>Programa</th>
                                                <th>Nombre programa</th>
                                                <th>Producto</th>
                                                <th>Nombre producto</th>
                                                <th>Indicador producto</th>
                                                <th>Proyecto</th>
                                                <th>Nombre</th>
                                                <th>Fuente</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr v-for="(data,index) in arrBpimCopy" :key="index" @dblclick="selectProyecto(data)">
                                                <td>{{data.sector}}</td>
                                                <td>{{data.nombre_sector}}</td>
                                                <td>{{data.programa}}</td>
                                                <td>{{data.nombre_programa}}</td>
                                                <td>{{data.producto}}</td>
                                                <td>{{data.nombre_producto}}</td>
                                                <td>{{data.indicador}}</td>
                                                <td>{{data.codigo}}</td>
                                                <td>{{data.nombre}}</td>
                                                <td>{{data.fuente}} - {{data.nombre_fuente}}</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary"  @click="isModalRubro=false;">Cerrar</button>
                            </div>
                        </div>
                    </div>
                </div>
                
                <div v-show="modalRubro" class="modal">
                    <div class="modal-dialog modal-xl">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title">Rubros presupuestales</h5>
                                <button type="button" @click="modalRubro=false;" class="btn btn-close"><div></div><div></div></button>
                            </div>
                            <div class="modal-body">
                                <div class="d-flex flex-column">
                                    <div class="form-control m-0 mb-3">
                                        <input type="search" v-model="txtSearch" @keyup="searchData('modalRubro')" placeholder="Busca CCPET, CPC, fuente o nombres">
                                    </div>
                                </div>
                                <div class="overflow-auto max-vh-50 overflow-x-hidden" >
                                    <table class="table table-hover fw-normal">
                                        <thead>
                                            <tr>
                                                <th>Rubro</th>
                                                <th>Nombre CCPET</th>
                                                <th>Nombre CPC</th>
                                                <th>Nombre fuente</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr v-for="(rubro,index) in rubros" :key="index" @click="selectItem('modalRubro', rubro)">
                                                <td>{{rubro.cuenta}}-{{rubro.cpc}}-{{rubro.fuente}}</td>
                                                <td style="width: 25%;">{{rubro.nombre_cuenta}}</td>
                                                <td style="width: 25%;">{{rubro.nombre_cpc}}</td>
                                                <td style="width: 25%;">{{rubro.nombre_fuente}}</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div v-show="modalCuentasCcpet" class="modal">
                    <div class="modal-dialog modal-lg">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title">Cuentas CCPET</h5>
                                <button type="button" @click="modalCuentasCcpet=false;" class="btn btn-close"><div></div><div></div></button>
                            </div>
                            <div class="modal-body">
                                <div class="d-flex flex-column">
                                    <div class="form-control m-0 mb-3">
                                        <input type="search" v-model="txtSearch" @keyup="searchData('modalccpet')" placeholder="Busca código o nombre">
                                    </div>
                                </div>
                                <div class="overflow-auto max-vh-50 overflow-x-hidden" >
                                    <table class="table table-hover fw-normal">
                                        <thead>
                                            <tr>
                                                <th class="text-center">Código</th>
                                                <th>Nombre</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr v-for="(ccpet,index) in cuentasccpet" :key="index" @click="selectItem('modalccpet', ccpet)">
                                                <td class="text-center">{{ccpet.codigo}}</td>
                                                <td>{{ccpet.nombre}}</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div v-show="modalFuentes" class="modal">
                    <div class="modal-dialog modal-sm">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title">Fuentes</h5>
                                <button type="button" @click="modalFuentes=false;" class="btn btn-close"><div></div><div></div></button>
                            </div>
                            <div class="modal-body">
                                <div class="d-flex flex-column">
                                    <div class="form-control m-0 mb-3">
                                        <input type="search" v-model="txtSearch" @keyup="searchData('modalFuente')" placeholder="Busca código o nombre">
                                    </div>
                                </div>
                                <div class="overflow-auto max-vh-50 overflow-x-hidden" >
                                    <table class="table table-hover fw-normal">
                                        <thead>
                                            <tr>
                                                <th class="text-center">Código</th>
                                                <th>Nombre</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr v-for="(fuente,index) in fuentes" :key="index" @click="selectItem('modalFuente', fuente)">
                                                <td class="text-center">{{fuente.codigo}}</td>
                                                <td>{{fuente.nombre}}</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div v-show="modalCpc" class="modal">
                    <div class="modal-dialog modal-sm">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title">CPC</h5>
                                <button type="button" @click="modalCpc=false;" class="btn btn-close"><div></div><div></div></button>
                            </div>
                            <div class="modal-body">
                                <div class="d-flex flex-column">
                                    <div class="form-control m-0 mb-3">
                                        <input type="search" v-model="txtSearch" @keyup="searchData('modalCpc')" placeholder="Busca código o nombre">
                                    </div>
                                </div>
                                <div class="overflow-auto max-vh-50 overflow-x-hidden" >
                                    <table class="table table-hover fw-normal">
                                        <thead>
                                            <tr>
                                                <th class="text-center">Código</th>
                                                <th>Nombre</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr v-for="(cpc,index) in arrayCpc" :key="index" @click="selectItem('modalCpc', cpc)">
                                                <td class="text-center">{{cpc.codigo}}</td>
                                                <td>{{cpc.nombre}}</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
                <div v-show="modalDetalleSectorial" class="modal">
                    <div class="modal-dialog modal-lg">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title">Detalle sectorial</h5>
                                <button type="button" @click="modalDetalleSectorial=false;" class="btn btn-close"><div></div><div></div></button>
                            </div>
                            <div class="modal-body">
                                <div class="d-flex flex-column">
                                    <div class="form-control m-0 mb-3">
                                        <input type="search" v-model="txtSearch" @keyup="searchData('modalDetSectorial')" placeholder="Busca código o descripción">
                                    </div>
                                </div>
                                <div class="overflow-auto max-vh-50 overflow-x-hidden" >
                                    <table class="table table-hover fw-normal">
                                        <thead>
                                            <tr>
                                                <th class="text-center">Código</th>
                                                <th>Nombre</th>
                                                <th>Grupo</th>
                                                <th>Sector</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr v-for="(sectorial,index) in arraySectorial" :key="index" @click="selectItem('modalDetSectorial', sectorial)">
                                                <td class="text-center">{{sectorial.codigo}}</td>
                                                <td>{{sectorial.nombre}}</td>
                                                <td>{{sectorial.grupo}}</td>
                                                <td>{{sectorial.sector}}</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div v-show="modalDetalleSectorialGasto" class="modal">
                    <div class="modal-dialog modal-lg">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title">Sectorial de gasto</h5>
                                <button type="button" @click="modalDetalleSectorialGasto=false;" class="btn btn-close"><div></div><div></div></button>
                            </div>
                            <div class="modal-body">
                                <div class="d-flex flex-column">
                                    <div class="form-control m-0 mb-3">
                                        <input type="search" v-model="txtSearch" @keyup="searchData('modalDetSectorialGasto')" placeholder="Busca código o descripción">
                                    </div>
                                </div>
                                <div class="overflow-auto max-vh-50 overflow-x-hidden" >
                                    <table class="table table-hover fw-normal">
                                        <thead>
                                            <tr>
                                                <th class="text-center">Código</th>
                                                <th>Nombre</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr v-for="(sectorial,index) in arrSectorialGasto" :key="index" @click="selectItem('modalDetSectorialGasto', sectorial)">
                                                <td class="text-center">{{sectorial.codigo}}</td>
                                                <td>{{sectorial.nombre}}</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div v-show="modalPoliticaPublica" class="modal">
                    <div class="modal-dialog modal-lg">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title">Politicas públicas</h5>
                                <button type="button" @click="modalPoliticaPublica=false;" class="btn btn-close"><div></div><div></div></button>
                            </div>
                            <div class="modal-body">
                                <div class="d-flex flex-column">
                                    <div class="form-control m-0 mb-3">
                                        <input type="search" v-model="txtSearch" @keyup="searchData('modalPolitica')" placeholder="Busca código o descripción">
                                    </div>
                                </div>
                                <div class="overflow-auto max-vh-50 overflow-x-hidden" >
                                    <table class="table table-hover fw-normal">
                                        <thead>
                                            <tr>
                                                <th class="text-center">Código</th>
                                                <th>Descripción</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr v-for="(politica,index) in politicasPublicas" :key="index" @click="selectItem('modalPolitica', politica)">
                                                <td class="text-center">{{politica.codigo}}</td>
                                                <td>{{politica.nombre}}</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </article>
        </section>

        <script src="Librerias/vue/vue.min.js"></script>
		<script src="Librerias/vue/axios.min.js"></script>
		<script type="module" src="planeacion/solicitudCdp/js/cdp_functions.js?<?= date('d_m_Y_h_i_s');?>"></script>

	</body>
</html>