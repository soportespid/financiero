<?php
    require_once("tcpdf/tcpdf_include.php");
	require('comun.inc');
	require "funciones.inc";
    require 'funcionesSP.inc.php';
	date_default_timezone_set("America/Bogota");
	session_start();
	class MYPDF extends TCPDF {
        protected $last_page_flag = false;

        public function Close() {
            $this->last_page_flag = true;
            parent::Close();
        }

		public function Header() {
			$linkbd = conectar_v7();
			$linkbd -> set_charset("utf8");
			$sqlr="SELECT * FROM configbasica WHERE estado='S'";
			$res=mysqli_query($linkbd, $sqlr);
			while($row=mysqli_fetch_row($res)) {
				$nit = $row[0];
				$rs  = $row[1];
			}
			//Parte Izquierda
			$this->Image('imagenes/escudo.jpg',  13, 13, 20, 20, 'JPG', '', 'T', true, 300, '', false, false, 0, false, false, false);// Logo
			$this->SetFont('helvetica','B',8);
			$this->SetY(10);
			$this->RoundedRect(10, 10, 190, 25, 1,'');
			$this->Cell(0.1);
			$this->Cell(26,25,'','R',0,'L');
			$this->SetY(8);
			$this->SetX(40);
			$this->SetFont('helvetica','B',9);
			$this->Cell(140,15,strtoupper("$rs"),0,0,'C');
			$this->SetFont('helvetica','B',7);
			$this->SetY(12);
			$this->SetX(40);
			$this->Cell(140,15,'NIT: '.$nit,0,0,'C');
			//*****************************************************************************************************************************
			$this->SetFont('helvetica','B',9);
			$this->SetY(23);
			$this->SetX(36);
            $this->Cell(164,12,"REGISTRO PRESUPUESTAL DE COMPROMISO",'T',0,'C');

			//**********************************************************
			$this->SetFont('times','B',10);
			$this->ln(12);
			//**********************************************************
            if($_POST["tipomov"]=="401" || $_POST["tipomov"] == "402"){
                $img_file = "assets/img/reversado.png";
                $this->SetAlpha(0.35);
                $this->Image($img_file, 0, 20, 250, 280, '', '', '', false, 300, '', false, false, 300);
                $this->SetAlpha(1);
            }
		}
		public function Footer()
		{
			$linkbd = conectar_v7();
			$linkbd -> set_charset("utf8");
			$sqlr="SELECT direccion,telefono,web,email FROM configbasica WHERE estado='S'";
			$resp=mysqli_query($linkbd, $sqlr);
			$user = $_SESSION['nickusu'];
            $cedula = $_SESSION['cedulausu'];
			$fecha = date("Y-m-d H:i:s");
			$ip = $_SERVER['REMOTE_ADDR'];
			while($row=mysqli_fetch_row($resp))
			{
				$direcc=strtoupper($row[0]);
				$telefonos=$row[1];
				$dirweb=strtoupper($row[3]);
				$coemail=strtoupper($row[2]);
			}
			if($direcc!=''){$vardirec="Dirección: $direcc, ";}
			else {$vardirec="";}
			if($telefonos!=''){$vartelef="Telefonos: $telefonos";}
			else{$vartelef="";}
			if($dirweb!=''){$varemail="Email: $dirweb, ";}
			else {$varemail="";}
			if($coemail!=''){$varpagiw="Pagina Web: $coemail";}
			else{$varpagiw="";}

            //firmas
            if ($this->last_page_flag) {
                $this->setY(240);
                $this->setX(70);
                $this->SetFont('helvetica','B',6);
                $this->SetFillColor(153,221,255);
                $this->cell(70,4,'CERTIFICA','LRTB',0,'C',1);
                $this->ln();
                $this->setX(70);
                $this->SetFont('helvetica','B',6);
                $this->SetFillColor(255,255,255);
                $this->cell(70,4,"NOMBRE: ".NOMBRE,'LRTB',0,'L',1);
                $this->ln();
                $this->setX(70);
                $this->SetFont('helvetica','B',6);
                $this->SetFillColor(255,255,255);
                $this->cell(70,4,"CARGO: ".CARGO,'LRTB',0,'L',1);
                $this->ln();
                $this->setX(70);
                $this->SetFont('helvetica','',6);
                $this->SetFillColor(255,255,255);
                $this->cell(70,20,'Firma','LRTB',0,'C',1);
                $this->ln();
            }

            $this->setY(280);
			$this->SetFont('helvetica', 'I', 8);
			$txt = <<<EOD
			$vardirec $vartelef
			$varemail $varpagiw
			EOD;
			$this->SetFont('helvetica', 'I', 6);
			$this->Cell(277,10,'','T',0,'T');
			$this->ln(2);
			$this->Write(0, $txt , '', 0, 'C', true, 0, false, false, 0);


            $sqlrcc="SELECT nom_usu FROM usuarios WHERE usu_usu = '$_POST[user]'";
            $respcc=mysqli_query($linkbd, $sqlrcc);
            $rowcc=mysqli_fetch_assoc($respcc);

			$this->Cell(50, 10, 'Hecho por: '.$rowcc["nom_usu"], 00, false, 'C', 0, '', 0, false, 'T', 'M');
			$this->Cell(30, 10, 'Impreso por: '.$user, 0, false, 'C', 0, '', 0, false, 'T', 'M');
			$this->Cell(30, 10, 'IP: '.$ip, 0, false, 'C',0, '', 0, false, 'T', 'M');
			$this->Cell(30, 10, 'Fecha: '.$fecha, 0, false, 'C', 0, '', 0, false, 'T', 'M');
			$this->Cell(30, 10, 'IDEAL.10 S.A.S', 0, false, 'C', 0, '', 0, false, 'T', 'M');
            $this->Cell(20, 10, 'Pagina '.$this->getAliasNumPage().'/'.$this->getAliasNbPages(), 0, false, 'C', 0, '', 0, false, 'T', 'M');
		}
	}

        $pdf = new MYPDF('P','mm','Letter', true, 'iso-8859-1', false);
        $pdf->SetDocInfoUnicode (true);
        // set document information
        $pdf->SetCreator(PDF_CREATOR);
        $pdf->SetAuthor('IDEALSAS');
        $pdf->SetTitle('CERTIFICADO RP');
        $pdf->SetSubject('CERTIFICADO RP');
        $pdf->SetKeywords('TCPDF, PDF, example, test, guide');
        $pdf->SetMargins(10, 38, 10);// set margins
        $pdf->SetHeaderMargin(38);// set margins
        $pdf->SetFooterMargin(17);// set margins
        $pdf->SetAutoPageBreak(TRUE, 20);// set auto page breaks
        // set some language-dependent strings (optional)
        if (@file_exists(dirname(__FILE__).'/lang/spa.php'))
        {
            require_once(dirname(__FILE__).'/lang/spa.php');
            $pdf->setLanguageArray($l);
        }
        $pdf->SetFillColor(255,255,255);
        $pdf->AddPage();
       
        $linkbd = conectar_v7();
		$linkbd -> set_charset("utf8");

        $codigoRp = $_POST["idcomp"];
        $codigoCdp = $_POST["numerocdp"];
        $vigencia = $_POST["vigencia"];
        $fechaRp = $_POST["fecha"];
        $tercero = $_POST["tercero"];
        $nomTercero = $_POST["ntercero"];
        $objeto = $_POST["objeto"];
        $numeroContrato = $_POST["ncontrato"];
        $nombreArea = $_POST["solicita"];

        $sql_sum_det = "SELECT SUM(valor) AS valor_total FROM ccpetrp_detalle WHERE consvigencia = $codigoRp AND vigencia = '$vigencia' AND tipo_mov = '201'";
        $row_sum_det = mysqli_fetch_assoc(mysqli_query($linkbd, $sql_sum_det));
        $valorTotal = $row_sum_det["valor_total"];

        $sql_vig_gasto = "SELECT vig.nombre FROM ccpetrp_detalle AS rp INNER JOIN ccpet_vigenciadelgasto AS vig ON vig.id = rp.codigo_vigenciag WHERE rp.consvigencia = $codigoRp AND rp.vigencia = '$vigencia' AND tipo_mov = '201' LIMIT 1";
        $row_vig_gasto = mysqli_fetch_assoc(mysqli_query($linkbd, $sql_vig_gasto));
        $tipoVigencia = $row_vig_gasto["nombre"];

        $sql_relacion = "SELECT id_solicitud_cdp FROM ccpet_solicitud_cdp WHERE consecutivo = $codigoCdp AND vigencia = '$vigencia'";
        $row_relacion = mysqli_fetch_assoc(mysqli_query($linkbd, $sql_relacion));
        $solicitudCdpId = $row_relacion["id_solicitud_cdp"];

        $sql_solicitud_cdp = "SELECT consecutivo, id_paa, tipo_presupuesto, tipo_gasto, sector, tipo_contrato_o_acto, tipo_contrato, numero_acto, fecha_acto FROM plan_solicitud_cdp WHERE id = $solicitudCdpId";
        $row_solicitud_cdp = mysqli_fetch_assoc(mysqli_query($linkbd, $sql_solicitud_cdp));
        $codigoSolicitud = $row_solicitud_cdp["consecutivo"];
        $paaId = $row_solicitud_cdp["id_paa"];
        $tipoPresupuesto = $row_solicitud_cdp["tipo_presupuesto"];
        $tipoDocumento = $row_solicitud_cdp["tipo_contrato_o_acto"];

        if ($paaId != 0) {
            $sql_plan_compras = "SELECT p.codplan AS codigo, m.nombre AS nombre_modalidad FROM contraplancompras AS p INNER JOIN plan_modalidad_seleccion AS m ON p.modalidad = m.codigo WHERE p.id = $paaId AND p.estado = 'S'";
            $row_plan_compras = mysqli_fetch_assoc(mysqli_query($linkbd, $sql_plan_compras));
            $paaCod = $row_plan_compras["codigo"];
            $modalidad = strtoupper(replaceChar($row_plan_compras["nombre_modalidad"]));
        } else {
            $paaCod = "NO APLICA";
            $modalidad = "NO APLICA";
        }
        

        $fun = $inv = $fun_inv = $nombreGasto = $contrato = $acta = "";
        if ($tipoPresupuesto == 1) {
            $fun = "X";
            $sql_version = "SELECT MAX(version) AS version FROM cuentasccpet";
            $row_version = mysqli_fetch_assoc(mysqli_query($linkbd, $sql_version));
            $version = $row_version["version"];
    
            $tipoGasto = $row_solicitud_cdp["tipo_gasto"];
            $sql_tipo_gasto = "SELECT nombre FROM cuentasccpet WHERE codigo = '$tipoGasto' AND nivel = 3 AND version = $version";
            $row_tipo_gasto = mysqli_fetch_assoc(mysqli_query($linkbd, $sql_tipo_gasto));
            $nombreGasto = strtoupper($row_tipo_gasto["nombre"]);    
        } else if ($tipoPresupuesto == 2) {
            $inv = "X";
            $codSector = $row_solicitud_cdp["sector"];
            $sql_sector = "SELECT nombre FROM ccpetsectores WHERE codigo = '$codSector'";
            $row_sector = mysqli_fetch_assoc(mysqli_query($linkbd, $sql_sector));
            $nomSector = strtoupper($row_sector["nombre"]);
        } else {
            $fun_inv = "X";
        }

        if ($tipoDocumento == 1) {
            $contrato = "X";
        } else {
            $acta = "X";
        }

        $sql_dependencia = "SELECT d.nombre AS nombre_dependencia FROM plan_solicitud_cdp_det AS s INNER JOIN pptoseccion_presupuestal AS d ON s.dependencia = d.id_seccion_presupuestal WHERE s.id_solicitud_cdp = '$solicitudCdpId' LIMIT 1";
        $row_dependencia = mysqli_fetch_assoc(mysqli_query($linkbd, $sql_dependencia));
        $nomDependencia = $row_dependencia["nombre_dependencia"];

        $sql_cargo = "SELECT id_cargo FROM pptofirmas WHERE id_comprobante='6' AND vigencia='$vigencia' ORDER BY id_cargo DESC";
        $row_cargo = mysqli_fetch_assoc(mysqli_query($linkbd, $sql_cargo));
        
        $sql_nom_cargo = "SELECT nombrecargo FROM planaccargos WHERE codpadre = 1 AND codcargo = $row_cargo[id_cargo]";
        $row_nom_cargo = mysqli_fetch_assoc(mysqli_query($linkbd, $sql_nom_cargo));
        $cargo = $row_nom_cargo["nombrecargo"];

        $sql_documento = "SELECT cedulanit FROM planestructura_terceros WHERE codcargo = '$row_cargo[id_cargo]' AND estado = 'S'";
        $row_documento = mysqli_fetch_assoc(mysqli_query($linkbd, $sql_documento));
        $documento = $row_documento["cedulanit"] != "" ? $row_documento["cedulanit"] : "";

        $sql_nombre = "SELECT CONCAT(nombre1, ' ', nombre2, ' ', apellido1, ' ', apellido2) AS nombre FROM terceros WHERE cedulanit = '$documento'";
        $row_nombre = mysqli_fetch_assoc(mysqli_query($linkbd, $sql_nombre));
        $nombre = preg_replace("/\s+/", " ", trim($row_nombre["nombre"]));
        $nombre = strtoupper($nombre);

        define("NOMBRE", $nombre);
        define("CARGO", $cargo);
        
        $sql_cuentas_fun = "SELECT cuenta, productoservicio AS cpc, fuente, valor FROM ccpetrp_detalle WHERE consvigencia = $codigoRp AND vigencia = '$vigencia' AND indicador_producto = '' AND tipo_mov = '201'";
        $row_cuentas_fun = mysqli_fetch_all(mysqli_query($linkbd, $sql_cuentas_fun), MYSQLI_ASSOC);
        $cuentasFun = $row_cuentas_fun;

        $sql_cuentas_inv = "SELECT indicador_producto AS codIndicador, bpim AS bpin, fuente AS codFuente, cuenta, valor, productoservicio AS cpc FROM ccpetrp_detalle WHERE consvigencia = $codigoRp AND vigencia = '$vigencia' AND indicador_producto != '' AND tipo_mov = '201'";
        $row_cuentas_inv = mysqli_fetch_all(mysqli_query($linkbd, $sql_cuentas_inv), MYSQLI_ASSOC);
        $cuentasInv = $row_cuentas_inv;

        $sql_det = "SELECT cuenta, fuente AS codFuente, indicador_producto AS codIndicador, bpim AS bpin, valor FROM ccpetrp_detalle WHERE consvigencia = $codigoRp AND vigencia = '$vigencia' AND tipo_mov = '201'";
        $row_det = mysqli_fetch_all(mysqli_query($linkbd, $sql_det), MYSQLI_ASSOC);
        $data = $row_det;

        $sql = "SELECT co.codigo as codigo_sectorial, co.concepto as nombre_sectorial, 
        ga.codigo as codigo_gasto, ga.concepto as nombre_gasto, 
        det.id_detalle_sectorial as sectorial_detalle,det.id_sectorial_gasto as sectorial_gasto
        FROM plan_solicitud_cdp_det det 
        LEFT JOIN ppto_codigos_sectoriales co ON det.id_detalle_sectorial = co.codigo
        LEFT JOIN cuipo_sec_prog_gastos ga ON det.id_sectorial_gasto = ga.codigo
        WHERE det.id_solicitud_cdp = '$solicitudCdpId'";
        if($codSector == 19){
            $arrRequest = mysqli_fetch_all(mysqli_query($linkbd, $sql), MYSQLI_ASSOC);
            $arrCodigoSectorial = array_values(array_filter($arrRequest,function($e){return $e['sectorial_detalle'] != 0;}));
            $arrCodigoSectorialGasto = array_values(array_filter($arrRequest,function($e){return $e['sectorial_gasto'] != 0;}));
        }else if($codSector == 22){
            $arrCodigoSectorial = mysqli_fetch_all(mysqli_query($linkbd, $sql), MYSQLI_ASSOC);
        }

        $pdf->SetFont('helvetica','B',9);
        $pdf->SetFillColor(153,221,255);
        $pdf->MultiCell(190,10,"EL JEFE DE PRESUPUESTO \n CERTIFICA","LRBT",'C',true,0,'','',true,0,false,true,0,'M',true);
        $pdf->ln();
        $pdf->SetFillColor(255,255,255);
        $pdf->SetTextColor(0,0,0);
        $pdf->SetFont('Helvetica','',8);
        $pdf->MultiCell(26,5,"CONSECUTIVO RP:","LRBT",'L',true,0,'','',true,0,false,true,0,'M',true);
        $pdf->MultiCell(13,5,$codigoRp,"LRBT",'C',true,0,'','',true,0,false,true,0,'M',true);
        $pdf->MultiCell(20,5,"FECHA:","LRBT",'L',true,0,'','',true,0,false,true,0,'M',true);
        $pdf->MultiCell(25,5,$fechaRp,"LRBT",'C',true,0,'','',true,0,false,true,0,'M',true);
        $pdf->MultiCell(34,5,"CONSECUTIVO CDP:","LRBT",'L',true,0,'','',true,0,false,true,0,'M',true);
        $pdf->MultiCell(17,5,$codigoCdp  ,"LRBT",'C',true,0,'','',true,0,false,true,0,'M',true);
        $pdf->MultiCell(20,5,"VALOR:","LRBT",'L',true,0,'','',true,0,false,true,0,'M',true);
        $pdf->MultiCell(35,5,"$".number_format($valorTotal, 2),"LRBT",'R',true,0,'','',true,0,false,true,0,'M',true);
        $pdf->ln();
        $pdf->SetFont('helvetica','B',9);
        $pdf->SetFillColor(153,221,255);
        $pdf->MultiCell(190,5,"INFORMACIÓN GENERAL","LRBT",'C',true,0,'','',true,0,false,true,0,'M',true);
        $pdf->ln();
        $pdf->SetFont('Helvetica','',8);
        $pdf->SetFillColor(255,255,255);
        $pdf->MultiCell(20,5,"TIPO VIGENCIA:","LRBT",'L',true,0,'','',true,0,false,true,0,'M',true);
        $pdf->MultiCell(25,5,$tipoVigencia,"LRBT",'C',true,0,'','',true,0,false,true,0,'M',true);
        $pdf->MultiCell(25,5,"PLAN COMPRAS:","LRBT",'L',true,0,'','',true,0,false,true,0,'M',true);
        $pdf->MultiCell(15,5,$paaCod,"LRBT",'C',true,0,'','',true,0,false,true,0,'M',true);
        $pdf->MultiCell(40,5,"MODALIDAD DE CONTRATACIÓN:","LRBT",'L',true,0,'','',true,0,false,true,0,'M',true);
        $pdf->MultiCell(65,5,$modalidad,"LRBT",'L',true,0,'','',true,0,false,true,0,'M',true);
        $pdf->ln();
        $pdf->MultiCell(50,5,"TIPO DE PRESUPUESTO ASIGNADO:","LRBT",'L',true,0,'','',true,0,false,true,0,'M',true);
        $pdf->MultiCell(25,5,"FUNCIONAMIENTO","LBT",'C',true,0,'','',true,0,false,true,0,'M',true);
        $pdf->MultiCell(21,5,$fun,"RBT",'C',true,0,'','',true,0,false,true,0,'M',true);
        $pdf->MultiCell(25,5,"INVERSION","LBT",'C',true,0,'','',true,0,false,true,0,'M',true);
        $pdf->MultiCell(22,5,$inv,"RBT",'C',true,0,'','',true,0,false,true,0,'M',true);
        $pdf->MultiCell(25,5,"FUN/INV","LBT",'C',true,0,'','',true,0,false,true,0,'M',true);
        $pdf->MultiCell(22,5,$fun_inv,"RBT",'C',true,0,'','',true,0,false,true,0,'M',true);
        if ($tipoPresupuesto == 1) {
            $pdf->ln();
            $pdf->MultiCell(50,5,"TIPO DE GASTO:","LRBT",'L',true,0,'','',true,0,false,true,0,'M',true);
            $pdf->MultiCell(140,5,$nombreGasto,"LRBT",'C',true,0,'','',true,0,false,true,0,'M',true);
        } else if ($tipoPresupuesto == 2) {
            $pdf->ln();
            $pdf->MultiCell(50,5,"CODIGO SECTOR:","LRBT",'L',true,0,'','',true,0,false,true,0,'M',true);
            $pdf->MultiCell(20,5,$codSector,"LRBT",'C',true,0,'','',true,0,false,true,0,'M',true);
            $pdf->MultiCell(40,5,"NOMBRE SECTOR:","LRBT",'L',true,0,'','',true,0,false,true,0,'M',true);
            $pdf->MultiCell(80,5,$nomSector,"LRBT",'C',true,0,'','',true,0,false,true,0,'M',true);
        } else {
            
        }
        $pdf->ln();

        $pdf->MultiCell(50,5,"TIPO DE DOCUMENTO:","LRBT",'L',true,0,'','',true,0,false,true,0,'M',true);
        $pdf->MultiCell(25,5,"TIPO DE CONTRATO","LBT",'C',true,0,'','',true,0,false,true,0,'M',true);
        $pdf->MultiCell(15,5,$contrato,"RBT",'C',true,0,'','',true,0,false,true,0,'M',true);
        $pdf->MultiCell(25,5,"ACTO ADMINISTRATIVO","LBT",'L',true,0,'','',true,0,false,true,0,'M',true);
        $pdf->MultiCell(15,5,$acta,"RBT",'C',true,0,'','',true,0,false,true,0,'M',true);
        $pdf->MultiCell(35,5,"NÚMERO CONTRATO:","LRBT",'L',true,0,'','',true,0,false,true,0,'M',true);
        $pdf->MultiCell(25,5,$numeroContrato,"LRBT",'C',true,0,'','',true,0,false,true,0,'M',true);
        $pdf->ln();

        if ($tipoDocumento == 1) {
            $pdf->MultiCell(50,5,"TIPO DE CONTRATO:","LRBT",'L',true,0,'','',true,0,false,true,0,'M',true);
            $tipoContrato = $row_solicitud_cdp["tipo_contrato"];
            if ($tipoContrato == 1) {
                $nombreTipoContrato = "OBRA";
            } else if ($tipoContrato == 2){
                $nombreTipoContrato = "CONSULTORA DE SERVICIO";
            } else if ($tipoContrato == 3){
                $nombreTipoContrato = "SUMINISTRO Y/O COMPRAVENTA";
            } else if ($tipoContrato == 4){
                $nombreTipoContrato = "PRESTACIÓN DE SERVICIOS";
            } else {
                $nombreTipoContrato = "OTRO";
            }
            $pdf->MultiCell(140,5,$nombreTipoContrato,"LRBT",'L',true,0,'','',true,0,false,true,0,'M',true);
        } else {
            $numeroActo = $row_solicitud_cdp["numero_acto"];
            $fechaActo = DateTime::createFromFormat('Y-m-d', $row_solicitud_cdp["fecha_acto"])->format('d/m/Y');
            $pdf->MultiCell(50,5,"NÚMERO DE ACTO:","LRBT",'L',true,0,'','',true,0,false,true,0,'M',true);
            $pdf->MultiCell(40,5,$numeroActo,"LRBT",'C',true,0,'','',true,0,false,true,0,'M',true);
            $pdf->MultiCell(50,5,"FECHA DE ACTO:","LRBT",'C',true,0,'','',true,0,false,true,0,'M',true);
            $pdf->MultiCell(50,5,$fechaActo,"LRBT",'C',true,0,'','',true,0,false,true,0,'M',true);
        }
        $pdf->ln();
        $pdf->MultiCell(30,5,"DOCUMENTO:","LRBT",'L',true,0,'','',true,0,false,true,0,'M',true);
        $pdf->MultiCell(30,5,$tercero,"LRBT",'C',true,0,'','',true,0,false,true,0,'M',true);
        $pdf->MultiCell(30,5,"BENEFICIARIO:","LRBT",'L',true,0,'','',true,0,false,true,0,'M',true);
        $pdf->MultiCell(100,5,$nomTercero,"LRBT",'L',true,0,'','',true,0,false,true,0,'M',true);
        $pdf->ln();
        $ancho_celda = 160; // Ancho definido para el texto
        $tamanio_fuente = 10; // Tamaño de fuente

        // Calcular la altura necesaria para el texto
        $altura_texto = $pdf->getStringHeight($ancho_celda, $objeto, false, true, $tamanio_fuente);

        // Definir una altura mínima para evitar que se vea muy pequeño
        $altura_minima = 10; 
        $altura = max($altura_texto, $altura_minima);

        $pdf->MultiCell(30, $altura, "OBJETO:", "LRBT", 'LRBT', true, 0, '', '', true, 0, false, true, 0, 'M', true);
        $pdf->MultiCell($ancho_celda, $altura, $objeto, "LRBT", 'L', true, 0, '', '', true, 0, false, true, 0, 'M', true);

        /* $pdf->MultiCell(30,10,"OBJETO:","LRBT",'LRBT',true,0,'','',true,0,false,true,0,'M',true);
        $pdf->MultiCell(160,10,$objeto,"LRBT",'L',true,0,'','',true,0,false,true,0,'M',true); */
        $pdf->ln();
        $pdf->SetFont('helvetica','B',9);
        $pdf->SetFillColor(153,221,255);
        $pdf->MultiCell(190,5,"IMPUTACIÓN PRESUPUESTAL","LRBT",'C',true,0,'','',true,0,false,true,0,'M',true);
        $pdf->ln();
        $pdf->SetFont('Helvetica','',8);
        $pdf->SetFillColor(255,255,255);
        $pdf->MultiCell(30,5,"SECCION:","LRBT",'L',true,0,'','',true,0,false,true,0,'M',true);
        $pdf->MultiCell(33,5,"ADMINISTRACIÓN CENTRAL","LRBT",'C',true,0,'','',true,0,false,true,0,'M',true);
        $pdf->MultiCell(30,5,"COMPONENTE SECCIÓN:","LRBT",'L',true,0,'','',true,0,false,true,0,'M',true);
        $pdf->MultiCell(33,5,$nomDependencia,"LRBT",'C',true,0,'','',true,0,false,true,0,'M',true);
        $pdf->MultiCell(31,5,"DEPENDENCIA:","LRBT",'L',true,0,'','',true,0,false,true,0,'M',true);
        $pdf->MultiCell(33,5,$nombreArea,"LRBT",'C',true,0,'','',true,0,false,true,0,'M',true);
        $pdf->ln();
        
        if (!empty($cuentasFun)) {
            $sql_version_cuentas = "SELECT MAX(version) AS version FROM cuentasccpet";
            $row_version_cuentas = mysqli_fetch_assoc(mysqli_query($linkbd, $sql_version_cuentas));
            $versionCuentas = $row_version_cuentas["version"];

            $pdf->SetFont('helvetica','B',8);
            $pdf->SetFillColor(153,221,255);
            $pdf->MultiCell(190,5,"DISCRIMINACIÓN PRESUPUESTAL","LRBT",'C',true,0,'','',true,0,false,true,0,'M',true);
            $pdf->ln();
           
            $pdf->MultiCell(25,10,"Tipo presupuesto","LRBT",'C',true,0,'','',true,0,false,true,0,'M',true);
            $pdf->MultiCell(25,10,"Tipo gasto","LRBT",'C',true,0,'','',true,0,false,true,0,'M',true);
            $pdf->MultiCell(25,10,"División gasto","LRBT",'C',true,0,'','',true,0,false,true,0,'M',true);
            $pdf->MultiCell(25,10,"Subdivision gasto","LRBT",'C',true,0,'','',true,0,false,true,0,'M',true);
            $pdf->MultiCell(25,10,"CCPET","LRBT",'C',true,0,'','',true,0,false,true,0,'M',true);
            $pdf->MultiCell(15,10,"CPC","LRBT",'C',true,0,'','',true,0,false,true,0,'M',true);
            $pdf->MultiCell(25,10,"Fuente","LRBT",'C',true,0,'','',true,0,false,true,0,'M',true);
            $pdf->MultiCell(25,10,"Valor","LRBT",'C',true,0,'','',true,0,false,true,0,'M',true);
            $pdf->ln();
            $pdf->SetFillColor(255,255,255);
            $pdf->SetFont('helvetica','',8);
            $fill = true;

            function search_name_cuenta($version, $cuenta){
                $linkbd = conectar_v7();
                $linkbd -> set_charset("utf8");
                $sql = "SELECT nombre FROM cuentasccpet WHERE codigo = '$cuenta' AND version = $version";
                $row = mysqli_fetch_assoc(mysqli_query($linkbd, $sql));
                return $row["nombre"];
            }

            foreach ($cuentasFun as $fun) {
                $codTipoPresu = substr($fun["cuenta"], 0, 3);
                $nomTipoPresu = search_name_cuenta($versionCuentas, $codTipoPresu);
                $codTipoGasto = substr($fun["cuenta"], 0, 5);
                $nomTipoGasto = search_name_cuenta($versionCuentas, $codTipoGasto);
                $codDivisionGasto = substr($fun["cuenta"], 0, 8);
                $nameDivisionGasto = search_name_cuenta($versionCuentas, $codDivisionGasto);
                $codSubdivisionGasto = substr($fun["cuenta"], 0, 11);
                $nameSubdivisionGasto = search_name_cuenta($versionCuentas, $codSubdivisionGasto);
                $nameCuenta = search_name_cuenta($versionCuentas, $fun["cuenta"]);
                $sql_fuentes = "SELECT nombre FROM ccpet_fuentes_cuipo WHERE codigo_fuente = '$fun[fuente]'";
                $row_fuentes = mysqli_fetch_assoc(mysqli_query($linkbd, $sql_fuentes));
                $nomFuente = $row_fuentes["nombre"];

                $pdf->SetFont('helvetica','',8);
                $pdf->MultiCell(25,18,"$codTipoPresu - $nomTipoPresu","LRBT",'J',true,0,'','',true,0,false,true,0,'M',true);
                $pdf->MultiCell(25,18,"$codTipoGasto - $nomTipoGasto","LRBT",'J',true,0,'','',true,0,false,true,0,'M',true);
                $pdf->MultiCell(25,18,"$codDivisionGasto - $nameDivisionGasto","LRBT",'J',true,0,'','',true,0,false,true,0,'M',true);
                $pdf->MultiCell(25,18,"$codSubdivisionGasto - $nameSubdivisionGasto","LRBT",'J',true,0,'','',true,0,false,true,0,'M',true);
                $pdf->MultiCell(25,18,"$fun[cuenta] - $nameCuenta","LRBT",'J',true,0,'','',true,0,false,true,0,'M',true);
                $pdf->MultiCell(15,18,"$fun[cpc]","LRBT",'J',true,0,'','',true,0,false,true,0,'M',true);
                $pdf->MultiCell(25,18,"$fun[fuente] - $nomFuente","LRBT",'J',true,0,'','',true,0,false,true,0,'M',true);
                $pdf->MultiCell(25,18,"$".number_format($fun["valor"], 2),"LRBT",'R',true,0,'','',true,0,false,true,0,'M',true);      
                $pdf->ln();
                $fill = !$fill;

                $getY = $pdf->getY();
                if ($getY > 190) {
                    $pdf->AddPage();
                }
            }
        }

        if (!empty($cuentasInv)) {
            $pdf->SetFont('helvetica','B',8);
            $pdf->SetFillColor(153,221,255);
            $pdf->MultiCell(190,5,"DISCRIMINACIÓN PRESUPUESTAL","LRBT",'C',true,0,'','',true,0,false,true,0,'M',true);
            $pdf->ln();
           
            $pdf->MultiCell(20,10,"Sector","LRBT",'C',true,0,'','',true,0,false,true,0,'M',true);
            $pdf->MultiCell(21,10,"Programa","LRBT",'C',true,0,'','',true,0,false,true,0,'M',true);
            $pdf->MultiCell(21,10,"SubPrograma","LRBT",'C',true,0,'','',true,0,false,true,0,'M',true);
            $pdf->MultiCell(21,10,"Producto","LRBT",'C',true,0,'','',true,0,false,true,0,'M',true);
            $pdf->MultiCell(21,10,"Indicador","LRBT",'C',true,0,'','',true,0,false,true,0,'M',true);
            $pdf->MultiCell(23,10,"BPIN","LRBT",'C',true,0,'','',true,0,false,true,0,'M',true);
            $pdf->MultiCell(20,10,"Fuente","LRBT",'C',true,0,'','',true,0,false,true,0,'M',true);
            $pdf->MultiCell(20,10,"CPC","LRBT",'C',true,0,'','',true,0,false,true,0,'M',true);
            $pdf->MultiCell(23,10,"Valor","LRBT",'C',true,0,'','',true,0,false,true,0,'M',true);
            $pdf->ln();
            $pdf->SetFillColor(255,255,255);
            $pdf->SetFont('helvetica','',8);
            $fill = true;

            foreach ($cuentasInv as $inv) {
                $codSector = $nombreSector = "";
                $codIndicador = $inv["codIndicador"];
                $bpin = $inv["bpin"];
                $codSector = substr($codIndicador, 0, 2);
                $sql_sector = "SELECT nombre FROM ccpetsectores WHERE codigo = '$codSector'";
                $row_sector = mysqli_fetch_assoc(mysqli_query($linkbd, $sql_sector));
                $nomSector = $row_sector["nombre"];
                $codPrograma = substr($codIndicador, 0, 4);
                $sql_programa = "SELECT nombre, codigo_subprograma, nombre_subprograma FROM ccpetprogramas WHERE codigo LIKE '$codPrograma'";
                $row_programa = mysqli_fetch_assoc(mysqli_query($linkbd, $sql_programa));
                $nomPrograma = $row_programa["nombre"];
                $codSubPrograma = $codPrograma.$row_programa["codigo_subprograma"];
                $nomSubPrograma = $row_programa["nombre_subprograma"];
                $codProducto = substr($codIndicador, 0, 7);
                $sql_producto = "SELECT DISTINCT producto AS nombre FROM ccpetproductos WHERE cod_producto LIKE '$codProducto'";
                $row_producto = mysqli_fetch_assoc(mysqli_query($linkbd, $sql_producto));
                $nomProducto = $row_producto["nombre"];
                $sql_indicador = "SELECT indicador_producto AS nombre FROM ccpetproductos WHERE codigo_indicador LIKE '$codIndicador'";
                $row_indicador = mysqli_fetch_assoc(mysqli_query($linkbd, $sql_indicador));
                $nomIndicador = $row_indicador["nombre"];
                $sql_bpin = "SELECT nombre FROM ccpproyectospresupuesto WHERE codigo = '$bpin' AND vigencia = '$vigencia'";
                $row_bpin = mysqli_fetch_assoc(mysqli_query($linkbd, $sql_bpin));
                $nomBpin = $row_bpin["nombre"];
                
                $sql_fuentes = "SELECT nombre FROM ccpet_fuentes_cuipo WHERE codigo_fuente = '$inv[codFuente]'";
                $row_fuentes = mysqli_fetch_assoc(mysqli_query($linkbd, $sql_fuentes));
                $nomFuente = $row_fuentes["nombre"];
                $pdf->SetFont('helvetica','',8);

                $pdf->MultiCell(20,18,"$codSector - $nomSector","LRBT",'J',true,0,'','',true,0,false,true,0,'M',true);
                $pdf->MultiCell(21,18,"$codPrograma - $nomPrograma","LRBT",'J',true,0,'','',true,0,false,true,0,'M',true);
                $pdf->MultiCell(21,18,"$codSubPrograma - $nomSubPrograma","LRBT",'J',true,0,'','',true,0,false,true,0,'M',true);
                $pdf->MultiCell(21,18,"$codProducto - $nomProducto","LRBT",'J',true,0,'','',true,0,false,true,0,'M',true);
                $pdf->MultiCell(21,18,"$inv[codIndicador] - $nomIndicador","LRBT",'J',true,0,'','',true,0,false,true,0,'M',true);
                $pdf->MultiCell(23,18,"$inv[bpin] - $nomBpin","LRBT",'J',true,0,'','',true,0,false,true,0,'M',true);
                $pdf->MultiCell(20,18,"$inv[codFuente] - $nomFuente","LRBT",'J',true,0,'','',true,0,false,true,0,'M',true);
                $pdf->SetFont('helvetica','',7);
                $pdf->MultiCell(20,18,$inv["cpc"],"LRBT",'C',true,0,'','',true,0,false,true,0,'M',true);
                $pdf->MultiCell(23,18,"$".number_format($inv["valor"], 2),"LRBT",'R',true,0,'','',true,0,false,true,0,'M',true);            
                
                $pdf->ln();
                $fill = !$fill;

                $getY = $pdf->getY();
                if ($getY > 190) {
                    $pdf->AddPage();
                }
            }
        }

        $pdf->ln(5);
        foreach ($data as $d) {
            if ($d["codIndicador"] == "") {
                $armadoNumero = "$d[cuenta]-$d[cpc]-$d[codFuente]";
            } else  if ($d["codIndicador"] != ""){
                $codIndicador = $d["codIndicador"];
                $sector = substr($codIndicador, 0, 2);
                $codPrograma = substr($codIndicador, 0, 4);
                $programa = substr($codIndicador, 2, 2);
                $sql_programa = "SELECT codigo_subprograma FROM ccpetprogramas WHERE codigo LIKE '$codPrograma'";
                $row_programa = mysqli_fetch_assoc(mysqli_query($linkbd, $sql_programa));
                $subPrograma = $row_programa["codigo_subprograma"];
                $producto = substr($codIndicador, 4, 3);
                $indicador = substr($codIndicador, 7, 2);
                $armadoNumero = "$sector.$programa.$subPrograma.$producto.$indicador-$d[bpin]-$d[codFuente]-$d[cuenta]-$d[cpc]";
            }
            
            $pdf->MultiCell(30,5,"CODIGO PRESUPUESTAL:","LRBT",'C',true,0,'','',true,0,false,true,0,'M',true);
            $pdf->MultiCell(90,5,$armadoNumero,"LRBT",'L',true,0,'','',true,0,false,true,0,'M',true);
            $pdf->MultiCell(30,5,"VALOR:","LBT",'C',true,0,'','',true,0,false,true,0,'M',true);
            $pdf->MultiCell(40,5,"$".number_format($d["valor"], 2),"RBT",'C',true,0,'','',true,0,false,true,0,'M',true);
            $pdf->ln();

            $getY = $pdf->getY();
            if ($getY > 190) {
                $pdf->AddPage();
            }
        }
        
        $pdf->MultiCell(150,5,"VALOR TOTAL:","LBT",'C',true,0,'','',true,0,false,true,0,'M',true);
        $pdf->MultiCell(40,5,"$".number_format($valorTotal, 2),"RBT",'C',true,0,'','',true,0,false,true,0,'M',true);
        
        $pdf->ln();
        $pdf->ln();

        foreach ($arrCodigoSectorial as $d) {
            
            $pdf->MultiCell(50,5,"CÓDIGO DETALLE SECTORIAL:","LRBT",'L',true,0,'','',true,0,false,true,0,'M',true);
            $pdf->MultiCell(30,5,$d['codigo_sectorial'],"LRBT",'L',true,0,'','',true,0,false,true,0,'M',true);
            $pdf->MultiCell(20,5,"NOMBRE:","LBT",'C',true,0,'','',true,0,false,true,0,'M',true);
            $pdf->MultiCell(90,5,$d['nombre_sectorial'],"RBT",'L',true,0,'','',true,0,false,true,0,'M',true);
            $pdf->ln();

            $getY = $pdf->getY();
            if ($getY > 190) {
                $pdf->AddPage();
            }
        }

        $pdf->ln();

        foreach ($arrCodigoSectorialGasto as $d) {
            
            $pdf->MultiCell(50,5,"CÓDIGO SECTORIAL DE GASTO:","LRBT",'L',true,0,'','',true,0,false,true,0,'M',true);
            $pdf->MultiCell(30,5,$d['codigo_gasto'],"LRBT",'L',true,0,'','',true,0,false,true,0,'M',true);
            $pdf->MultiCell(20,5,"NOMBRE:","LBT",'C',true,0,'','',true,0,false,true,0,'M',true);
            $pdf->MultiCell(90,5,$d['nombre_gasto'],"RBT",'L',true,0,'','',true,0,false,true,0,'M',true);
            $pdf->ln();

            $getY = $pdf->getY();
            if ($getY > 190) {
                $pdf->AddPage();
            }
        }
        
        $pdf->ln();
        $pdf->ln();

        $pdf->Output('certificado_rp'.'.pdf', 'I');
?>
