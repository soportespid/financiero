<?php
	header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=utf8");
	require 'comun.inc';
	require 'funciones.inc';
	session_start();
	$linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");
	cargarcodigopag($_GET['codpag'],$_SESSION["nivel"]);
	date_default_timezone_set("America/Bogota");
?>
<!DOCTYPE >
<html lang=es>
	<head>
		<meta charset="utf-8"/>
		<meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1"/>
		<meta name="viewport" content="user-scalable=no">
		<title>:: IDEAL 10 - Gesti&oacute;n humana</title>
		<link href="css/css2z.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
		<link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
		<link href="css/css4.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
		<link href="favicon.ico" rel="shortcut icon" />
		<link rel="stylesheet" href="sweetalert2/dist/sweetalert2.min.css">
		<script src="sweetalert2/dist/sweetalert2.min.js"></script>
		<script src="vue/vue.min.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script src="css/programas.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script src="css/calendario.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script src='JQuery/jquery-2.1.4.min.js'></script>
		
	</head>
	<body>
		<span id="todastablas2"></span>
		<header>
			<table>
				<tr><script>barra_imagenes("hum");</script><?php cuadro_titulos();?></tr>
			</table>
		</header>
		<section id="myapp" v-cloak >
			<nav>
				<table>
					<tr><?php menu_desplegable("hum");?></tr>
					<tr>
						<td colspan="3" class="cinta">
							<img src="imagenes/add.png" v-on:Click="location.href='hum-aprobarnomina.php'" class="mgbt" title="Nuevo" >
							<img src="imagenes/guardad.png" title="Guardar" class="mgbt1">
							<img src="imagenes/busca.png" class="mgbt" title="Buscar" v-on:Click="location.href='hum-aprobarnominabuscar.php'">
							<img src="imagenes/nv.png" v-on:Click="mypop=window.open('hum-principal.php','',''); mypop.focus();" class="mgbt" title="Nueva Ventana">
							<img src='imagenes/iratras.png' title="Atr&aacute;s" onClick="location.href='hum-menunomina.php'" class="mgbt">
						</td>
					</tr>
				</table>
			</nav>
			<article>
				<table class="inicio ancho">
					<tr>
						<td class="titulos" colspan="8" >N&oacute;minas Aprobadas</td>
						<td class="cerrar" style="width:7%" onClick="location.href='cont-principal.php'">Cerrar</td>
					</tr>
					<tr>
						<td class="textonew01" style="width:2.5cm;">Fecha Inicial:</td>
						<td style="width:10%;"><input type="text" name="fecha" value="<?php echo $_POST['fecha']?>" onKeyUp="return tabular(event,this)" id="fc_1198971545" title="DD/MM/YYYY" onDblClick="displayCalendarFor('fc_1198971545');" class="colordobleclik" autocomplete="off" onChange="" readonly></td>
						<td class="textonew01" style="width:2.5cm;" >Fecha Final:</td>
						<td style="width:10%;"><input type="text" name="fecha2" value="<?php echo $_POST['fecha2']?>" onKeyUp="return tabular(event,this)" id="fc_1198971546" title="DD/MM/YYYY" onDblClick="displayCalendarFor('fc_1198971546');" class="colordobleclik" autocomplete="off" onChange="" readonly></td>
						<td colspan="2" style="padding-bottom:0px"><em class="botonflechaverde" v-on:Click="iniciabuscar();">Buscar</em></td>
						<td></td>
					</tr>
				</table>
				<div class='subpantalla' style='height:66vh; width:99.2%; margin-top:0px; overflow:hidden'>	
					<table class='tablamv'>
						<thead>
							<tr style="text-align:left;">
								<th class="titulos">Resultados Busqueda:</th>
							</tr>
							<tr style="text-align:Center;">
								<th class="titulosnew00" style="width:7%;">C&oacute;digo</th>
								<th class="titulosnew00" style="width:8%;">Fecha</th>
								<th class="titulosnew00" style="width:7%;">N&oacute;mina</th>
								<th class="titulosnew00" style="width:7%;">CDP</th>
								<th class="titulosnew00" style="width:7%;">RP</th>
								<th class="titulosnew00" >Descripci&oacute;n</th>
								<th class="titulosnew00" style="width:6%;">Estado</th>
								<th class="titulosnew00" style="width:6%;">Deshacer</th>
							</tr>
						</thead>
						<tbody id="divdet">
							<tr v-for="(infoaprobadas,index) in infoaprobada" v-on:dblclick="moveravisualizar(infoaprobadas[0])" v-bind:class="infoaprobadas[14] == 'S' ? 'saludoyellow' : (index % 2 ? 'saludo1a' : 'saludo2')" >
								<td style="font: 120% sans-serif; padding-left:10px; width:7%; text-align:Center;">{{ infoaprobadas[0]  }}</td>
								<td style="font: 120% sans-serif; padding-left:10px; width:8%; text-align:Center;">{{ infoaprobadas[2] }}</td>
								<td style="font: 120% sans-serif; padding-left:10px; width:7%; text-align:Center;">{{ infoaprobadas[1] }}</td>
								<td style="font: 120% sans-serif; padding-left:10px; width:7%; text-align:Center;">{{ infoaprobadas[12] }}</td>
								<td style="font: 120% sans-serif; padding-left:10px; width:7%; text-align:Center;">{{ infoaprobadas[3] }}</td>
								<td style="font: 120% sans-serif; padding-left:10px;">{{ infoaprobadas[7] }}</td>
								<td style='padding-left:10px; width:6%; text-align:Center;'><img v-bind:src='infoaprobadas[9]' style='width:19px; cursor:pointer' v-bind:title="infoaprobadas[8]"></td>
								<td style='padding-left:10px; width:6%; text-align:Center;' v-on:Click = "validardeshacer(infoaprobadas[11],infoaprobadas[1], infoaprobadas[2]);"><img  v-bind:src='infoaprobadas[10]' style='width:19px; cursor:pointer' v-bind:title="infoaprobadas[8]" ></td>
							</tr>
						</tbody>
					</table>
				</div>
				<div id="cargando" v-if="loading" class="loading" style="z-index: 9999;">
					<span>Cargando...</span>
				</div>
			</article>
			<input type="hidden" v-model="idbuscar" :value="idbuscar=3"/>
			<input type="hidden" v-model="fechagt1" :value="fechagt1='<?php echo $_GET['fechaini']?>'"/>
			<input type="hidden" v-model="fechagt2" :value="fechagt2='<?php echo $_GET['fechafin']?>'"/>
			<input type="hidden" v-model="scrtop" :value="scrtop='<?php echo $_GET['scrtop']?>'"/>
			<input type="hidden" v-model="idr" :value="idr='<?php echo $_GET['idr']?>'"/>
		</section>
		<script src="Librerias/vue/vue.min.js"></script>
		<script src="Librerias/vue/axios.min.js"></script>
		<script src="vue/gestion_humana/hum-aprobarnomina.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
	</body>
</html>