<?php
	header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=utf8");
	require 'comun.inc';
	require 'funciones.inc';
	session_start();
	cargarcodigopag($_GET['codpag'],$_SESSION['nivel']);
	date_default_timezone_set('America/Bogota');
?>
<!DOCTYPE >
<html lang="es">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=9">
		<title>:: IDEAL 10 - Presupuesto</title>
		<link href="css/css2.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css">
		<link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css">
		<link href="css/css4.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css">
		<script type="text/javascript" src="css/programas.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script type="text/javascript" src="css/calendario.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script type="text/javascript" src="JQuery/jquery-2.1.4.min.js"></script>
        <link href="css/style.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
		<script>
			function cambiaFecha()
			{
				var fecha = document.getElementById('fecha').value;
				fecArray = fecha.split('/');
				document.getElementById('vigencia').value = fecArray[2];
			}
		</script>

		<?php titlepag();?>
	</head>
	<body>
		<div class="subpantalla" style="height:640px; width:99.6%; overflow:hidden;">
			<div id="myapp" style="height:inherit;" v-cloak>
				<div class="row">
					<table>
						<tr><script>barra_imagenes("ccpet");</script><?php cuadro_titulos();?></tr>
						<tr><?php menu_desplegable("ccpet");?></tr>
					</table>
                    <div class="bg-white group-btn p-1" id="newNavStyle">
                        <button type="button" class="btn btn-white btn-primary-hover d-flex justify-between align-items-center" @click="window.location.href='ccp-cdpnomina2022.php'">
                            <span>Nuevo</span>
                            <svg  xmlns="http://www.w3.org/2000/svg"  viewBox="0 -960 960 960" ><path d="M440-440H200v-80h240v-240h80v240h240v80H520v240h-80v-240Z"/></svg>
                        </button>
                        <button type="button" class="btn btn-white btn-primary-hover d-flex justify-between align-items-center" v-on:Click="guardarCdp">
                            <span>Guardar</span>
                            <svg xmlns="http://www.w3.org/2000/svg"  viewBox="0 -960 960 960" ><path d="M840-680v480q0 33-23.5 56.5T760-120H200q-33 0-56.5-23.5T120-200v-560q0-33 23.5-56.5T200-840h480l160 160Zm-80 34L646-760H200v560h560v-446ZM480-240q50 0 85-35t35-85q0-50-35-85t-85-35q-50 0-85 35t-35 85q0 50 35 85t85 35ZM240-560h360v-160H240v160Zm-40-86v446-560 114Z"/></svg>
                        </button>
                        <button type="button" class="btn btn-white btn-primary-hover d-flex justify-between align-items-center" @click="window.location.href='ccp-buscarcdp.php'">
                            <span>Buscar</span>
                            <svg class="fill-black group-hover:fill-white w-5 h-5" xmlns="http://www.w3.org/2000/svg"  viewBox="0 -960 960 960" ><path d="M784-120 532-372q-30 24-69 38t-83 14q-109 0-184.5-75.5T120-580q0-109 75.5-184.5T380-840q109 0 184.5 75.5T640-580q0 44-14 83t-38 69l252 252-56 56ZM380-400q75 0 127.5-52.5T560-580q0-75-52.5-127.5T380-760q-75 0-127.5 52.5T200-580q0 75 52.5 127.5T380-400Z"/></svg>
                        </button>
                        <button type="button" class="btn btn-white btn-primary-hover d-flex justify-between align-items-center" @click="mypop=window.open('ccp-principal.php','',''); mypop.focus();">
                            <span>Nueva ventana</span>
                            <svg class="fill-black group-hover:fill-white w-5 h-5" xmlns="http://www.w3.org/2000/svg"  viewBox="0 -960 960 960" ><path d="M320-120v-80h80v-80H160q-33 0-56.5-23.5T80-360v-400q0-33 23.5-56.5T160-840h640q33 0 56.5 23.5T880-760v400q0 33-23.5 56.5T800-280H560v80h80v80H320ZM160-360h640v-400H160v400Zm0 0v-400 400Z"/></svg>
                        </button>
                        <button type="button" class="btn btn-white btn-primary-hover d-flex justify-between align-items-center" @click="mypop=window.open('ccp-cdpnomina2022.php','','');mypop.focus();">
                            <span class="group-hover:text-white">Duplicar pantalla</span>
                            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 -960 960 960"><path d="M640-160v-360H160v360h480Zm80-200v-80h80v-360H320v200h-80v-200q0-33 23.5-56.5T320-880h480q33 0 56.5 23.5T880-800v360q0 33-23.5 56.5T800-360h-80ZM160-80q-33 0-56.5-23.5T80-160v-360q0-33 23.5-56.5T160-600h480q33 0 56.5 23.5T720-520v360q0 33-23.5 56.5T640-80H160Zm400-603ZM400-340Z"></path></svg>
                        </button>
                        <button type="button" class="btn btn-success d-flex justify-between align-items-center" @click="window.location.href='ccp-gestioncdp'">
                            <span>Atrás</span>
                            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 -960 960 960"><path d="M760-200v-160q0-50-35-85t-85-35H273l144 144-57 56-240-240 240-240 57 56-144 144h367q83 0 141.5 58.5T840-360v160h-80Z"/></svg>
                        </button>
                    </div>
				</div>
                <h2 class="titulos m-0">Certificado de Disponibilidad Presupuestal</h2>
                <div class="d-flex w-50">
                    <div class="form-control flex-row align-items-center">
                        <label class="form-label m-0 w-25" for="">Tipo de movimiento:</label>
                        <select v-model="tipoMovimiento"  v-on:change="seleccionarFormulario()">
                            <option v-for="tipoDeMovimiento in tiposDeMovimiento" v-bind:value = "tipoDeMovimiento[0]">
                                {{ tipoDeMovimiento[0] }} - {{ tipoDeMovimiento[1] }}
                            </option>
                        </select>
                    </div>
                </div>
				<div v-show="mostrarCrearCdp">
                    <div class="d-flex">
                        <div class="d-flex w-50">
                            <div class="form-control flex-row align-items-center">
                                <label class="form-label m-0 w-75" for="">Consecutivo:</label>
                                <input type="text" id="consecutivo" name="consecutivo" class="text-center"  v-model="consecutivo" readonly disabled>
                            </div>
                            <div class="form-control flex-row align-items-center w-50">
                                <label class="form-label m-0" for="">Fecha:</label>
                                <input type="text" id="fecha" name="fecha" class="bg-warning cursor-pointer" onClick="displayCalendarFor('fecha');" title="DD/MM/YYYY" placeholder="DD/MM/YYYY" onchange="cambiaFecha();">
                            </div>
                            <div class="form-control flex-row align-items-center w-50">
                                <label class="form-label m-0" for="">Vigencia:</label>
                                <input type="text" id="vigencia" name="vigencia" readonly disabled>
                            </div>
                        </div>
                        <div class="d-flex w-50">
                            <div class="form-control flex-row align-items-center">
                                <label class="form-label m-0 w-25" for="">Nomina Nº:</label>
                                <select v-model="numeronomina"  v-on:change="cargarDetalles()">
                                    <option v-for="numerodenomina in numerosdenomina" v-bind:value = "numerodenomina[0]">
                                        {{ numerodenomina[0] }} - {{ numerodenomina[1] }} - {{ numerodenomina[2] }}
                                    </option>
                                </select>
                            </div>
                            <div class="form-control flex-row align-items-center">
                                <label class="form-label m-0" for="">Solicita:</label>
                                <input type="text" id="solicita" name="solicita" v-model = "solicita">
                            </div>
                        </div>
                    </div>
                    <div class="d-flex">
                        <div class="form-control">
                            <label class="form-label m-0" for="">Objeto:</label>
                            <textarea class="resize-vertical" id="objeto" name="objeto" rows="3" v-model="objeto"></textarea>
                        </div>
                    </div>
                    <div class="table-responsive" style="height:55vh">
                        <table  class="table fw-normal">
                            <thead>
                                <tr class="text-center">
                                    <th>Tipo de gasto</th>
                                    <th>Dependencia</th>
                                    <th>Medio pago</th>
                                    <th>Vig. del gasto</th>
                                    <th>Proyecto</th>
                                    <th>Programatico</th>
                                    <th>Cuenta</th>
                                    <th>Fuente</th>
                                    <th>CPC</th>
                                    <th>DIVIPOLA</th>
                                    <th>CHIP</th>
                                    <th>Valor</th>
                                    <th>Quitar</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr v-for="detalle in detalles">
                                    <td>{{ detalle[11] }}</td><!--Tipo gasto-->
                                    <td>{{ detalle[12] }}</td><!--Sec presu-->
                                    <td>{{ detalle[6] }}</td><!--Medio Pago-->
                                    <td>{{ detalle[13] }}</td><!--vig gasto-->
                                    <td>{{ detalle[10] }}</td><!--proyecto-->
                                    <td>{{ detalle[9] }}</td><!--programati-->
                                    <td>{{ detalle[1] }}</td><!--cuenta-->
                                    <td>{{ detalle[4] }}</td><!--fuente-->
                                    <td>{{ detalle[8] }}</td><!--CPC-->
                                    <td>{{ detalle[14] }}</td><!--divipola-->
                                    <td>{{ detalle[15] }}</td><!--chip-->
                                    <td>{{ formatonumero(detalle[2]) }}</td><!--valor-->
                                </tr>
                            </tbody>
                            <tfoot>
                                <tr class='saludo1'>
									<td colspan = '11' style='font-size:14px; text-align: center; color:gray !important; ' class='saludo1'>TOTAL:</td>
									<td align="right" style='font-size:12px;'> {{ formatonumero(totalCdp) }}</td>
									<td></td>
								</tr>
                            </tfoot>
                        </table>
                    </div>
					<div class="row" style=" height:50%; width:94,6%; overflow-x:hidden;" >
						<div class="col-12">
							<table>
								<thead>
									<tr>
										<td width="10%" class='titulos' style="font: 110% sans-serif; padding-left:2px; border-radius: 5px 0 0 0;">Tipo de gasto</td>
										<td width="10%" class='titulos' style="font: 110% sans-serif; ">Sec. Presupuestal</td>
										<td width="7%" class='titulos' style="font: 110% sans-serif; ">Medio pago</td>
										<td width="10%" class='titulos' style="font: 110% sans-serif; ">Vig. del gasto</td>
										<td width="10%" class='titulos' style="font: 110% sans-serif; ">Proyecto</td>
										<td width="10%" class='titulos' style="font: 110% sans-serif; ">Program&aacute;tico</td>
										<td width="20%" class='titulos' style="font: 110% sans-serif; ">Cuenta</td>
										<td width="15%" class='titulos' style="font: 110% sans-serif; ">Fuente</td>
										<td width="10%" class='titulos' style="font: 110% sans-serif; ">CPC</td>
										<td width="10%" class='titulos' style="font: 110% sans-serif; ">Divipola</td>
										<td width="10%" class='titulos' style="font: 110% sans-serif; ">CHIP</td>
										<td width="15%" class='titulos' style="font: 110% sans-serif; ">Valor</td>
									</tr>
								</thead>
								<tbody>
									<tr v-for="(detalle, index) in detalles" v-bind:class="index % 2 ? 'saludo1a' : 'saludo2'" style='text-rendering: optimizeLegibility; height: 10px; cursor: pointer !important; cursor: hand;'>
										<td style="font: 110% sans-serif; padding-left:10px">{{ detalle[11] }}</td><!--Tipo gasto-->
										<td style="font: 110% sans-serif; padding-left:10px">{{ detalle[12] }}</td><!--Sec presu-->
										<td style="font: 110% sans-serif; padding-left:10px">{{ detalle[6] }}</td><!--Medio Pago-->
										<td style="font: 110% sans-serif; padding-left:10px">{{ detalle[13] }}</td><!--vig gasto-->
										<td style="font: 110% sans-serif; padding-left:10px">{{ detalle[10] }}</td><!--proyecto-->
										<td style="font: 110% sans-serif; padding-left:10px">{{ detalle[9] }}</td><!--programati-->
										<td style="font: 110% sans-serif; padding-left:10px">{{ detalle[1] }}</td><!--cuenta-->
										<td style="font: 110% sans-serif; padding-left:10px">{{ detalle[4] }}</td><!--fuente-->
										<td style="font: 110% sans-serif; padding-left:10px">{{ detalle[8] }}</td><!--CPC-->
										<td style="font: 110% sans-serif; padding-left:10px">{{ detalle[14] }}</td><!--divipola-->
										<td style="font: 110% sans-serif; padding-left:10px">{{ detalle[15] }}</td><!--chip-->
										<td style="font: 110% sans-serif; padding-left:10px;text-align:right;">{{ formatonumero(detalle[2]) }}</td><!--valor-->
									</tr>
								</tbody>
								<tfoot>
								<tr class='saludo1'>
									<td colspan = '11' style='font-size:14px; text-align: center; color:gray !important; ' class='saludo1'>TOTAL:</td>
									<td align="right" style='font-size:12px;'> {{ formatonumero(totalCdp) }}</td>
									<td></td>
								</tr>
								</tfoot>
							</table>
						</div>
					</div>


					<div v-show="showModal_cuentas" class="modal">
                        <div class="modal-dialog modal-lg" >
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title">Cuentas CCPET</h5>
                                    <button type="button" @click="showModal_cuentas = false" class="btn btn-close"><div></div><div></div></button>
                                </div>
                                <div class="modal-body">
                                    <div class="d-flex flex-column">
                                        <div class="form-control m-0 mb-3">
                                            <input type="search" placeholder="Buscar" v-on:keyup="searchMonitorCuenta" v-model="searchCuenta.keywordCuenta">
                                        </div>
                                    </div>
                                    <div class="overflow-auto max-vh-50 overflow-x-hidden " >
                                        <table class="table table-hover fw-normal">
                                            <thead>
                                                <tr>
                                                    <th>Codigo</th>
                                                    <th>Nombre</th>
                                                    <th>Tipo</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr v-for="cuenta in cuentasCcpet" v-on:click="seleccionarCuenta(cuenta)">
                                                    <td>{{cuenta[1]}}</td>
                                                    <td>{{cuenta[2]}}</td>
                                                    <td>{{cuenta[6]}}</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div v-show="showModal_proyectos" class="modal">
                        <div class="modal-dialog modal-lg" >
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title">Proyectos de inversion</h5>
                                    <button type="button" @click="showModal_proyectos = false" class="btn btn-close"><div></div><div></div></button>
                                </div>
                                <div class="modal-body">
                                    <div class="d-flex flex-column">
                                        <div class="form-control m-0 mb-3">
                                            <input type="search" placeholder="Buscar por nombre o BPIM" v-on:keyup="searchMonitorProyecto" v-model="searchProyecto.keywordProyecto">
                                        </div>
                                    </div>
                                    <div class="overflow-auto max-vh-50 overflow-x-hidden " >
                                        <table class="table table-hover fw-normal">
                                            <thead>
                                                <tr>
                                                    <th>Codigo</th>
                                                    <th>Nombre</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr v-for="proyecto in proyectos" v-on:click="seleccionarProyecto(proyecto)">
                                                    <td>{{proyecto[2]}}</td>
                                                    <td>{{proyecto[4]}}</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div v-show="showModal_programatico" class="modal">
                        <div class="modal-dialog modal-lg" >
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title">Programatico</h5>
                                    <button type="button" @click="showModal_programatico = false" class="btn btn-close"><div></div><div></div></button>
                                </div>
                                <div class="modal-body">
                                    <div class="overflow-auto max-vh-50 overflow-x-hidden " >
                                        <table class="table table-hover fw-normal">
                                            <thead>
                                                <tr>
                                                    <th>Codigo</th>
                                                    <th>Nombre</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr v-for="programatico in programaticos" v-on:click="seleccionarProgramatico(programatico)">
                                                    <td>{{programatico[0]}}</td>
                                                    <td>{{programatico[1]}}</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
					<div v-show="showModal_fuentes" class="modal">
                        <div class="modal-dialog modal-lg" >
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title">Fuentes CUIPO</h5>
                                    <button type="button" @click="showModal_fuentes = false" class="btn btn-close"><div></div><div></div></button>
                                </div>
                                <div class="modal-body">
                                <div class="d-flex flex-column">
                                        <div class="form-control m-0 mb-3">
                                            <input type="search" placeholder="Buscar por nombre o codigo" v-on:keyup="searchMonitorFuente" v-model="searchFuente.keywordFuente">
                                        </div>
                                    </div>
                                    <div class="overflow-auto max-vh-50 overflow-x-hidden " >
                                        <table class="table table-hover fw-normal">
                                            <thead>
                                                <tr>
                                                    <th>Codigo</th>
                                                    <th>Nombre</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr v-for="fuente in fuentesCuipo" v-on:click="seleccionarFuente(fuente)">
                                                    <td>{{fuente[0]}}</td>
                                                    <td>{{fuente[1]}}</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div v-show="showModal_divipola" class="modal">
                        <div class="modal-dialog modal-lg" >
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title">DIVIPOLA</h5>
                                    <button type="button" @click="showModal_divipola = false" class="btn btn-close"><div></div><div></div></button>
                                </div>
                                <div class="modal-body">
                                <div class="d-flex flex-column">
                                        <div class="form-control m-0 mb-3">
                                            <input type="search" placeholder="Buscar por nombre o codigo" v-on:keyup="" v-model="searchDivipola.keywordDivipola">
                                        </div>
                                    </div>
                                    <div class="overflow-auto max-vh-50 overflow-x-hidden " >
                                        <table class="table table-hover fw-normal">
                                            <thead>
                                                <tr>
                                                    <th>Codigo</th>
                                                    <th>Nombre</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr v-for="divipola in divipolas" v-on:click="seleccionarDivipola(divipola)">
                                                    <td>{{divipola[0]}}</td>
                                                    <td>{{divipola[1]}}</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
					<div v-show="showModal_chips" class="modal">
                        <div class="modal-dialog modal-lg" >
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title">CHIPS</h5>
                                    <button type="button" @click="showModal_chips = false" class="btn btn-close"><div></div><div></div></button>
                                </div>
                                <div class="modal-body">
                                <div class="d-flex flex-column">
                                        <div class="form-control m-0 mb-3">
                                            <input type="search" placeholder="Buscar por nombre o codigo"  v-on:keyup="searchMonitorChip" v-model="searchChip.keywordChip">
                                        </div>
                                    </div>
                                    <div class="overflow-auto max-vh-50 overflow-x-hidden " >
                                        <table class="table table-hover fw-normal">
                                            <thead>
                                                <tr>
                                                    <th>Codigo</th>
                                                    <th>Nombre</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr v-for="chip in chips" v-on:click="seleccionarChip(chip)">
                                                    <td>{{chip[0]}}</td>
                                                    <td>{{chip[1]}}</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

					<div v-show="showModal_bienes_transportables">
                        <transition name="modal">
                            <div class="modal-mask">
                                <div class="modal-wrapper">
                                    <div class="modal-dialog modal-lg" style = "max-width: 1200px !important;" role="document">
                                        <div class="modal-content"  style = "width: 100% !important;" scrollable>
                                            <span id="start_page"> </span>
                                            <div class="modal-header">
                                                <div class="row">
                                                    <div class="col-12">
                                                        <h5 class="modal-title">Clasificador Bienes transportables Sec. 0-4</h5>
                                                    </div>
                                                </div>
                                                <button type="button" @click="showModal_bienes_transportables = false" class="btn btn-close"><div></div><div></div></button>
                                            </div>
                                            <div class="modal-body" style = "height: 400px; overflow: scroll; overflow-x: hidden;">
                                                <div v-show="mostrarDivision">
                                                    <div class="row" style="margin: 2px 0 0 0; border-radius: 5px !important; border-radius:4px; background-color: #E1E2E2; ">
                                                        <div class="col-md-3" style="padding: 12px 0px 0px 30px; font: 140% sans-serif;">
                                                            <label for="">Buscar Producto:</label>
                                                        </div>

                                                        <div class="col-md-6" style="padding: 4px">
                                                            <input type="text" class="form-control" style="height: auto; border-radius:2px;" placeholder="Ej: 1800001" v-on:keyup.enter="buscarGeneral"  v-model="searchGeneral.keywordGeneral">
                                                        </div>
                                                        <div class="col-md-2 col-sm-4 col-md-offset-1" style="padding: 4px">
                                                            <button type="submit" class="btn btn-dark" value="Buscar" style="height: auto; border-radius:5px;" v-on:click="buscarGeneral">Buscar</button>
                                                        </div>
                                                    </div>
                                                    <div style="margin: 2px 0 0 0; border-radius: 0 0 0 6px; height: 200px; overflow: scroll; overflow-x: hidden; background: white; ">
                                                        <table class='inicio inicio--no-shadow'>
                                                            <tbody>
                                                                <?php
                                                                    $co ='zebra1';
                                                                    $co2='zebra2';
                                                                ?>
                                                                <tr v-for="division in divisiones" v-on:click="buscarGrupo(division)" v-bind:class="division[0] === division_p ? 'background_active_color' : ''" class='<?php echo $co; ?>' style='text-rendering: optimizeLegibility; cursor: pointer !important; style=\"cursor: hand\"'>
                                                                    <td width="20%" style="font: 120% sans-serif; padding-left:10px">{{ division[0] }}</td>
                                                                    <td width="80%" style="font: 120% sans-serif; padding-left:10px">{{ division[1] }}</td>

                                                                    <?php
                                                                    $aux=$co;
                                                                    $co=$co2;
                                                                    $co2=$aux;
                                                                    ?>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                                <div v-show="mostrarGrupo">
                                                    <div style="margin: 2px 0 0 0;">
                                                        <div class="row" style="margin: 4px; border-radius:4px; background-color: #E1E2E2; ">
                                                            <div class="col-md-2" style="padding: 12px 0px 0px 30px; font: 140% sans-serif;">
                                                                <label for="">Grupos:</label>
                                                            </div>

                                                            <div class="col-md-6 col-md-offset-6" style="padding: 4px">
                                                                <input type="text" class="form-control" placeholder="Buscar por nombre o c&oacute;digo de grupo" v-on:keyup="searchMonitorGrupos" v-model="searchGrupo.keywordGrupo">
                                                            </div>
                                                        </div>
                                                        <table>
                                                            <thead>
                                                                <tr>
                                                                    <td width="20%" class='titulos' style="font: 120% sans-serif; padding-left:10px; border-radius: 5px 0 0 0;">C&oacute;digo</td>
                                                                    <td width="80%" class='titulos' style="font: 120% sans-serif; ">Nombre</td>
                                                                </tr>
                                                            </thead>
                                                        </table>
                                                    </div>
                                                    <div style="margin: 2px 0 0 0; border-radius: 0 0 0 6px; height: 200px; overflow: scroll; overflow-x: hidden; background: white; ">
                                                        <table class='inicio inicio--no-shadow'>
                                                            <tbody>
                                                                <?php
                                                                    $co ='zebra1';
                                                                    $co2='zebra2';
                                                                ?>
                                                                <tr v-for="grupo in grupos" v-on:click="buscarClase(grupo)"  v-bind:class="grupo[0] === grupo_p ? 'background_active_color' : ''" class='<?php echo $co; ?>' style='text-rendering: optimizeLegibility; cursor: pointer !important; style=\"cursor: hand\"'>
                                                                    <td width="20%" style="font: 120% sans-serif; padding-left:10px">{{ grupo[0] }}</td>
                                                                    <td width="80%" style="font: 120% sans-serif; padding-left:10px">{{ grupo[1] }}</td>

                                                                    <?php
                                                                    $aux=$co;
                                                                    $co=$co2;
                                                                    $co2=$aux;
                                                                    ?>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                                <div v-show="mostrarClase">
                                                    <div style="margin: 2px 0 0 0;">
                                                        <div class="row" style="margin: 4px; border-radius:4px; background-color: #E1E2E2; ">
                                                            <div class="col-md-2" style="padding: 12px 0px 0px 30px; font: 140% sans-serif;">
                                                                <label for="">Clases:</label>
                                                            </div>

                                                            <div class="col-md-6 col-md-offset-6" style="padding: 4px">
                                                                <input type="text" class="form-control" placeholder="Buscar por nombre o c&oacute;digo de clase" v-on:keyup="searchMonitorClases" v-model="searchClase.keywordClase">
                                                            </div>
                                                        </div>
                                                        <table>
                                                            <thead>
                                                                <tr>
                                                                    <td width="20%" class='titulos' style="font: 120% sans-serif; padding-left:10px; border-radius: 5px 0 0 0;">C&oacute;digo</td>
                                                                    <td width="80%" class='titulos' style="font: 120% sans-serif; ">Nombre</td>
                                                                </tr>
                                                            </thead>
                                                        </table>
                                                    </div>
                                                    <div style="margin: 2px 0 0 0; border-radius: 0 0 0 6px; height: 200px; overflow: scroll; overflow-x: hidden; background: white; ">
                                                        <table class='inicio inicio--no-shadow'>
                                                            <tbody>
                                                                <?php
                                                                    $co ='zebra1';
                                                                    $co2='zebra2';
                                                                ?>
                                                                <tr v-for="clase in clases" v-on:click="buscarSubclase(clase)"  v-bind:class="clase[0] === clase_p ? 'background_active_color' : ''" class='<?php echo $co; ?>' style='text-rendering: optimizeLegibility; cursor: pointer !important; style=\"cursor: hand\"'>
                                                                    <td width="20%" style="font: 120% sans-serif; padding-left:10px">{{ clase[0] }}</td>
                                                                    <td width="80%" style="font: 120% sans-serif; padding-left:10px">{{ clase[1] }}</td>

                                                                    <?php
                                                                    $aux=$co;
                                                                    $co=$co2;
                                                                    $co2=$aux;
                                                                    ?>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                                <div v-show="mostrarSubClase">
                                                    <div style="margin: 2px 0 0 0;">
                                                        <div class="row" style="margin: 4px; border-radius:4px; background-color: #E1E2E2; ">
                                                            <div class="col-md-2" style="padding: 12px 0px 0px 30px; font: 140% sans-serif;">
                                                                <label for="">Subclase:</label>
                                                            </div>

                                                            <div class="col-md-6 col-md-offset-6" style="padding: 4px">
                                                                <input type="text" class="form-control" placeholder="Buscar por nombre o c&oacute;digo de subclase" v-on:keyup="searchMonitorSubClases" v-model="searchSubClase.keywordSubClase">
                                                            </div>
                                                        </div>
                                                        <table>
                                                            <thead>
                                                                <tr>
                                                                    <td width="20%" class='titulos' style="font: 120% sans-serif; padding-left:10px; border-radius: 5px 0 0 0;">C&oacute;digo</td>
                                                                    <td width="30%" class='titulos' style="font: 120% sans-serif; ">Nombre</td>
                                                                    <td width="15%" class='titulos' style="font: 120% sans-serif; ">CIIU Rev. 4 A.C. </td>
                                                                    <td width="25%" class='titulos' style="font: 120% sans-serif; ">Sistema Armonizado 2012</td>
                                                                    <td width="10%" class='titulos' style="font: 120% sans-serif; ">CPC 2 A.C.</td>
                                                                </tr>
                                                            </thead>
                                                        </table>
                                                    </div>
                                                    <div style="margin: 2px 0 0 0; border-radius: 0 0 0 6px; height: 200px; overflow: scroll; overflow-x: hidden; background: white; ">
                                                        <table class='inicio inicio--no-shadow'>
                                                            <tbody>
                                                                <?php
                                                                    $co ='zebra1';
                                                                    $co2='zebra2';
                                                                ?>
                                                                <tr v-for="subclase in subClases" v-on:click="seleccionarSublaseProducto(subclase)"  v-bind:class="subclase[0] === subClase_p ? 'background_active_color' : ''" class='<?php echo $co; ?>' style='text-rendering: optimizeLegibility; cursor: pointer !important; style=\"cursor: hand\"'>
                                                                    <td width="20%" style="font: 120% sans-serif; padding-left:10px">{{ subclase[0] }}</td>
                                                                    <td width="30%" style="font: 120% sans-serif; padding-left:10px">{{ subclase[1] }}</td>
                                                                    <td width="15%" style="font: 120% sans-serif; padding-left:10px">{{ subclase[2] }}</td>
                                                                    <td width="25%" style="font: 120% sans-serif; padding-left:10px">{{ subclase[3] }}</td>
                                                                    <td width="10%" style="font: 120% sans-serif; padding-left:10px">{{ subclase[4] }}</td>

                                                                    <?php
                                                                    $aux=$co;
                                                                    $co=$co2;
                                                                    $co2=$aux;
                                                                    ?>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                                <div v-show="mostrarSubClaseProducto">
                                                    <div style="margin: 2px 0 0 0;">
                                                        <div class="row" style="margin: 4px; border-radius:4px; background-color: #E1E2E2; ">
                                                            <div class="col-md-2" style="padding: 12px 0px 0px 30px; font: 140% sans-serif;">
                                                                <label for="">Producto:</label>
                                                            </div>
                                                        </div>
                                                        <table>
                                                            <thead>
                                                                <tr>
                                                                    <td width="20%" class='titulos' style="font: 120% sans-serif; padding-left:10px; border-radius: 5px 0 0 0;">Subclase</td>
                                                                    <td width="60%" class='titulos' style="font: 120% sans-serif; ">Titulo</td>
                                                                    <td width="20%" class='titulos' style="font: 120% sans-serif; ">Ud </td>
                                                                </tr>
                                                            </thead>
                                                        </table>
                                                    </div>
                                                    <div style="margin: 2px 0 0 0; border-radius: 0 0 0 6px; height: 100px; overflow: scroll; overflow-x: hidden; background: white; ">
                                                        <table class='inicio inicio--no-shadow'>
                                                            <tbody>
                                                                <tr v-for="subclase in subClases_captura" v-on:click="seleccionarBienes(subclase)"
                                                                v-on:dblclick="seleccionarCpc"
                                                                v-bind:class="subclase[0] === subClase_p ? 'background_active_color' : ''" style='text-rendering: optimizeLegibility; cursor: pointer !important; cursor: hand;'>
                                                                    <td width="20%" style="font: 120% sans-serif; padding-left:10px">{{ subclase[0] }}</td>
                                                                    <td width="60%" style="font: 120% sans-serif; padding-left:10px">{{ subclase[1] }}</td>
                                                                    <td width="20%" style="font: 120% sans-serif; padding-left:10px">{{ subclase[2] }}</td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                                <span id="end_page"> </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </transition>
                    </div>
                    <div v-show="showModal_servicios">
                        <transition name="modal">
                            <div class="modal-mask">
                                <div class="modal-wrapper">
                                    <div class="modal-dialog modal-lg" style = "max-width: 1200px !important;" role="document">
                                        <div class="modal-content"  style = "width: 100% !important;" scrollable>
                                            <div class="modal-header">
                                                <div class="row">
                                                    <div class="col-12">
                                                        <h5 class="modal-title">Clasificador Servicios Sec. 5-9</h5>
                                                    </div>
                                                </div>
                                                <button type="button" @click="showModal_servicios = false" class="btn btn-close"><div></div><div></div></button>
                                            </div>
                                            <div class="modal-body" style = "height: 400px; overflow: scroll; overflow-x: hidden;">

                                                <div v-show="mostrarDivisionServicios">
                                                    <div style="margin: 2px 0 0 0;">
                                                        <div class="row" style="margin: 4px; border-radius:4px; background-color: #E1E2E2; ">
                                                            <div class="col-md-2" style="padding: 12px 0px 0px 30px; font: 140% sans-serif;">
                                                                <label for="">Divisi&oacute;n:</label>
                                                            </div>

                                                            <div class="col-md-6 col-md-offset-6" style="padding: 4px">
                                                                <input type="text" class="form-control" placeholder="Buscar por nombre o c&oacute;digo de divisi&oacute;n" v-on:keyup.enter="buscarGeneral2" v-model="searchGeneral2.keywordGeneral">
                                                            </div>
                                                            <div class="col-md-2 col-sm-4 col-md-offset-1" style="padding: 4px">
                                                                <button type="submit" class="btn btn-dark" value="Buscar" style="height: auto; border-radius:5px;" v-on:click="buscarGeneral2">Buscar</button>
                                                            </div>
                                                        </div>
                                                        <table>
                                                            <thead>
                                                                <tr>
                                                                    <td width="20%" class='titulos' style="font: 120% sans-serif; border-radius: 5px 0 0 0;">C&oacute;digo</td>
                                                                    <td width="80%" class='titulos' style="font: 120% sans-serif;">Nombre</td>
                                                                </tr>
                                                            </thead>
                                                        </table>
                                                    </div>
                                                    <div style="margin: 2px 0 0 0; border-radius: 0 0 0 6px; height: 200px; overflow: scroll; overflow-x: hidden; background: white; ">
                                                        <table class='inicio inicio--no-shadow'>
                                                            <tbody>
                                                                <?php
                                                                    $co ='zebra1';
                                                                    $co2='zebra2';
                                                                ?>
                                                                <tr v-for="division in divisionesServicios" v-on:click="buscarGrupoServicios(division)" v-bind:class="division[0] === divisionServicios_p ? 'background_active_color' : ''" class='<?php echo $co; ?>' style='text-rendering: optimizeLegibility; cursor: pointer !important; style=\"cursor: hand\"'>
                                                                    <td width="20%" style="font: 120% sans-serif; padding-left:10px">{{ division[0] }}</td>
                                                                    <td width="80%" style="font: 120% sans-serif; padding-left:10px">{{ division[1] }}</td>

                                                                    <?php
                                                                    $aux=$co;
                                                                    $co=$co2;
                                                                    $co2=$aux;
                                                                    ?>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                                <div v-show="mostrarGrupoServicios">
                                                    <div style="margin: 2px 0 0 0;">
                                                        <div class="row" style="margin: 4px; border-radius:4px; background-color: #E1E2E2; ">
                                                            <div class="col-md-2" style="padding: 12px 0px 0px 30px; font: 140% sans-serif;">
                                                                <label for="">Grupos:</label>
                                                            </div>

                                                            <div class="col-md-6 col-md-offset-6" style="padding: 4px">
                                                                <input type="text" class="form-control" placeholder="Buscar por nombre o c&oacute;digo de grupo" v-on:keyup="searchMonitorGrupos" v-model="searchGrupoServicios.keywordGrupoServicios">
                                                            </div>
                                                        </div>
                                                        <table>
                                                            <thead>
                                                                <tr>
                                                                    <td width="20%" class='titulos' style="font: 120% sans-serif; padding-left:10px; border-radius: 5px 0 0 0;">C&oacute;digo</td>
                                                                    <td width="80%" class='titulos' style="font: 120% sans-serif; ">Nombre</td>
                                                                </tr>
                                                            </thead>
                                                        </table>
                                                    </div>
                                                    <div style="margin: 2px 0 0 0; border-radius: 0 0 0 6px; height: 200px; overflow: scroll; overflow-x: hidden; background: white; ">
                                                        <table class='inicio inicio--no-shadow'>
                                                            <tbody>
                                                                <?php
                                                                    $co ='zebra1';
                                                                    $co2='zebra2';
                                                                ?>
                                                                <tr v-for="grupo in gruposServicios" v-on:click="buscarClaseServicios(grupo)"  v-bind:class="grupo[0] === grupoServicios_p ? 'background_active_color' : ''" class='<?php echo $co; ?>' style='text-rendering: optimizeLegibility; cursor: pointer !important; style=\"cursor: hand\"'>
                                                                    <td width="20%" style="font: 120% sans-serif; padding-left:10px">{{ grupo[0] }}</td>
                                                                    <td width="80%" style="font: 120% sans-serif; padding-left:10px">{{ grupo[1] }}</td>

                                                                    <?php
                                                                    $aux=$co;
                                                                    $co=$co2;
                                                                    $co2=$aux;
                                                                    ?>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                                <div v-show="mostrarClaseServicios">
                                                    <div style="margin: 2px 0 0 0;">
                                                        <div class="row" style="margin: 4px; border-radius:4px; background-color: #E1E2E2; ">
                                                            <div class="col-md-2" style="padding: 12px 0px 0px 30px; font: 140% sans-serif;">
                                                                <label for="">Clases:</label>
                                                            </div>

                                                            <div class="col-md-6 col-md-offset-6" style="padding: 4px">
                                                                <input type="text" class="form-control" placeholder="Buscar por nombre o c&oacute;digo de clase" v-on:keyup="searchMonitorClases" v-model="searchClaseServicios.keywordClaseServicios">
                                                            </div>
                                                        </div>
                                                        <table>
                                                            <thead>
                                                                <tr>
                                                                    <td width="20%" class='titulos' style="font: 120% sans-serif; padding-left:10px; border-radius: 5px 0 0 0;">C&oacute;digo</td>
                                                                    <td width="80%" class='titulos' style="font: 120% sans-serif; ">Nombre</td>
                                                                </tr>
                                                            </thead>
                                                        </table>
                                                    </div>
                                                    <div style="margin: 2px 0 0 0; border-radius: 0 0 0 6px; height: 200px; overflow: scroll; overflow-x: hidden; background: white; ">
                                                        <table class='inicio inicio--no-shadow'>
                                                            <tbody>
                                                                <?php
                                                                    $co ='zebra1';
                                                                    $co2='zebra2';
                                                                ?>
                                                                <tr v-for="clase in clasesServicios" v-on:click="buscarSubclaseServicios(clase)"  v-bind:class="clase[0] === claseServicios_p ? 'background_active_color' : ''" class='<?php echo $co; ?>' style='text-rendering: optimizeLegibility; cursor: pointer !important; style=\"cursor: hand\"'>
                                                                    <td width="20%" style="font: 120% sans-serif; padding-left:10px">{{ clase[0] }}</td>
                                                                    <td width="80%" style="font: 120% sans-serif; padding-left:10px">{{ clase[1] }}</td>

                                                                    <?php
                                                                    $aux=$co;
                                                                    $co=$co2;
                                                                    $co2=$aux;
                                                                    ?>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                                <div v-show="mostrarSubClaseServicios">
                                                    <div style="margin: 2px 0 0 0;">
                                                        <div class="row" style="margin: 4px; border-radius:4px; background-color: #E1E2E2; ">
                                                            <div class="col-md-2" style="padding: 12px 0px 0px 30px; font: 140% sans-serif;">
                                                                <label for="">Subclase:</label>
                                                            </div>

                                                            <div class="col-md-6 col-md-offset-6" style="padding: 4px">
                                                                <input type="text" class="form-control" placeholder="Buscar por nombre o c&oacute;digo de subclase" v-on:keyup="searchMonitorSubClases" v-model="searchSubClase.keywordSubClase">
                                                            </div>
                                                        </div>
                                                        <table>
                                                            <thead>
                                                                <tr>
                                                                    <td width="20%" class='titulos' style="font: 120% sans-serif; padding-left:10px; border-radius: 5px 0 0 0;">C&oacute;digo</td>
                                                                    <td width="30%" class='titulos' style="font: 120% sans-serif; ">Nombre</td>
                                                                    <td width="15%" class='titulos' style="font: 120% sans-serif; ">CIIU Rev. 4 A.C. </td>
                                                                    <td width="25%" class='titulos' style="font: 120% sans-serif; ">Sistema Armonizado 2012</td>
                                                                    <td width="10%" class='titulos' style="font: 120% sans-serif; ">CPC 2 A.C.</td>
                                                                </tr>
                                                            </thead>
                                                        </table>
                                                    </div>
                                                    <div style="margin: 2px 0 0 0; border-radius: 0 0 0 6px; height: 200px; overflow: scroll; overflow-x: hidden; background: white; ">
                                                        <table class='inicio inicio--no-shadow'>
                                                            <tbody>
                                                                <?php
                                                                    $co ='zebra1';
                                                                    $co2='zebra2';
                                                                ?>
                                                                <tr v-for="subclase in subClasesServicios" v-on:click="seleccionarServicios(subclase)"
                                                                v-on:dblclick="seleccionarServicioCpc"
                                                                v-bind:class="subclase[0] === subClaseServicios_p ? 'background_active_color' : ''" class='<?php echo $co; ?>' style='text-rendering: optimizeLegibility; cursor: pointer !important; style=\"cursor: hand\"'>
                                                                    <td width="20%" style="font: 120% sans-serif; padding-left:10px">{{ subclase[0] }}</td>
                                                                    <td width="30%" style="font: 120% sans-serif; padding-left:10px">{{ subclase[1] }}</td>
                                                                    <td width="15%" style="font: 120% sans-serif; padding-left:10px">{{ subclase[2] }}</td>
                                                                    <td width="25%" style="font: 120% sans-serif; padding-left:10px">{{ subclase[3] }}</td>
                                                                    <td width="10%" style="font: 120% sans-serif; padding-left:10px">{{ subclase[4] }}</td>

                                                                    <?php
                                                                    $aux=$co;
                                                                    $co=$co2;
                                                                    $co2=$aux;
                                                                    ?>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                                <span id="end_page_servicios"> </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </transition>
                    </div>

				</div>

				<div v-show = "mostrarReversionCdp">

				</div>

			</div>
		</div>
		<!-- <script>
			import VueSweetalert2 from 'node_modules/vue-sweetalert2';
		</script> -->
		<!-- <script src="node_modules/sweetalert2.all.min.js"></script> -->
		<!-- <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script> -->
		<!-- <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script> -->

		<!-- <script src="https://unpkg.com/vue-swal"></script> -->

		<script src="Librerias/vue/vue.min.js"></script>
		<script src="Librerias/vue/axios.min.js"></script>
		<script src="vue/presupuesto_ccp/ccp-cdpnomina2022.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script src="sweetalert2/dist/sweetalert2.min.js"></script>
		<link rel="stylesheet" href="sweetalert2/dist/sweetalert2.min.css">

	</body>
</html>
