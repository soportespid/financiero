<?php
header("Cache-control: no-cache, no-store, must-revalidate");
header("Content-Type: text/html;charset=utf8");
require "comun.inc";
require "funciones.inc";
require "funciones_nomina.inc";
require 'funcionesnomima.inc';
sesion();
$linkbd = conectar_v7();
$linkbd->set_charset("utf8");
cargarcodigopag($_GET['codpag'], $_SESSION["nivel"]);
date_default_timezone_set("America/Bogota");
ini_set('max_execution_time', 7200);
?>
<!DOCTYPE>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="es">

<head>
    <meta http-equiv="Content-Type" content="text/html" charset="utf8" />
    <meta http-equiv="X-UA-Compatible" content="IE=9" />
    <title>:: IDEAL 10 - Contabilidad</title>
    <link href="css/css2.css?<?php echo date('d_m_Y_h_i_s'); ?>" rel="stylesheet" type="text/css" />
    <link href="css/css3.css?<?php echo date('d_m_Y_h_i_s'); ?>" rel="stylesheet" type="text/css" />
    <link href="css/css4.css?<?php echo date('d_m_Y_h_i_s'); ?>" rel="stylesheet" type="text/css" />
    <link href="css/style.css?<?php echo date('d_m_Y_h_i_s'); ?>" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="css/programas.js"></script>
    <script type="text/javascript" src="css/calendario.js"></script>
    <script>
        function guardar() {
            if (document.form2.idliq.value != '') {
                if (confirm("Esta Seguro de Guardar")) {
                    document.form2.oculto.value = 2;
                    document.form2.submit();
                }
            }
            else { alert('Faltan datos para completar el registro'); }
        }
        function validar(formulario) {
            document.form2.action = "cont-liquidarnomina-reflejarccpet.php";
            document.form2.submit();
        }
        function marcar(indice, posicion) {
            vvigencias = document.getElementsByName('empleados[]');
            vtabla = document.getElementById('fila' + indice);
            clase = vtabla.className;
            if (vvigencias.item(posicion).checked) { vtabla.style.backgroundColor = '#3399bb'; }
            else {
                e = vvigencias.item(posicion).value;
                document.getElementById('fila' + e).style.backgroundColor = '#ffffff';
            }
            sumarconc();
        }
        function excell() {
            document.form2.action = "hum-liquidarnominaexcel.php";
            document.form2.target = "_BLANK";
            document.form2.submit();
            document.form2.action = "";
            document.form2.target = "";
        }
        function pdf() {
            document.form2.action = "pdfplanillapago.php";
            document.form2.target = "_BLANK";
            document.form2.submit();
            document.form2.action = "";
            document.form2.target = "";
        }
    </script>
    <?php titlepag(); ?>
</head>

<body>
    <IFRAME src="alertas.php" name="alertas" id="alertas" style="display:none"></IFRAME>
    <span id="todastablas2"></span>
    <table>
        <tr>
            <script>barra_imagenes("cont");</script><?php cuadro_titulos(); ?>
        </tr>
        <tr><?php menu_desplegable("cont"); ?></tr>
    </table>
    <div class="bg-white group-btn p-1"><button type="button" onclick=""
            class="btn btn-white btn-primary-hover d-flex justify-between align-items-center">
            <span>Nuevo</span>
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 -960 960 960">
                <path d="M440-440H200v-80h240v-240h80v240h240v80H520v240h-80v-240Z"></path>
            </svg>
        </button><button type="button" onclick="mypop=window.open('cont-principal.php','','');mypop.focus();"
            class="btn btn-white btn-primary-hover d-flex justify-between align-items-center">
            <span>Nueva ventana</span>
            <svg class="fill-black group-hover:fill-white w-5 h-5" xmlns="http://www.w3.org/2000/svg"
                viewBox="0 -960 960 960">
                <path
                    d="M320-120v-80h80v-80H160q-33 0-56.5-23.5T80-360v-400q0-33 23.5-56.5T160-840h640q33 0 56.5 23.5T880-760v400q0 33-23.5 56.5T800-280H560v80h80v80H320ZM160-360h640v-400H160v400Zm0 0v-400 400Z">
                </path>
            </svg>
        </button><button type="button"
            onclick="mypop=window.open('/financiero/cont-liquidarnomina-reflejarccpet.php','','');mypop.focus();"
            class="btn btn-white btn-primary-hover d-flex justify-between align-items-center">
            <span class="group-hover:text-white">Duplicar pantalla</span>
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 -960 960 960">
                <path
                    d="M640-160v-360H160v360h480Zm80-200v-80h80v-360H320v200h-80v-200q0-33 23.5-56.5T320-880h480q33 0 56.5 23.5T880-800v360q0 33-23.5 56.5T800-360h-80ZM160-80q-33 0-56.5-23.5T80-160v-360q0-33 23.5-56.5T160-600h480q33 0 56.5 23.5T720-520v360q0 33-23.5 56.5T640-80H160Zm400-603ZM400-340Z">
                </path>
            </svg>
        </button><button type="button" onclick="guardar()"
            class="btn btn-white btn-primary-hover d-flex justify-between align-items-center">
            <span>Reflejar</span>
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 -960 960 960">
                <path
                    d="M400-280h160v-80H400v80Zm0-160h280v-80H400v80ZM280-600h400v-80H280v80Zm200 120ZM265-80q-79 0-134.5-55.5T75-270q0-57 29.5-102t77.5-68H80v-80h240v240h-80v-97q-37 8-61 38t-24 69q0 46 32.5 78t77.5 32v80Zm135-40v-80h360v-560H200v160h-80v-160q0-33 23.5-56.5T200-840h560q33 0 56.5 23.5T840-760v560q0 33-23.5 56.5T760-120H400Z">
                </path>
            </svg>
        </button><button type="button" onclick="excell()"
            class="btn btn-success btn-success-hover d-flex justify-between align-items-center">
            <span>Exportar Excel</span>
            <svg class="fill-black group-hover:fill-white w-4 h-4" xmlns="http://www.w3.org/2000/svg"
                viewBox="0 0 384 512"><!--!Font Awesome Free 6.5.1 by @fontawesome - https://fontawesome.com License - https://fontawesome.com/license/free Copyright 2024 Fonticons, Inc. -->
                <path
                    d="M48 448V64c0-8.8 7.2-16 16-16H224v80c0 17.7 14.3 32 32 32h80V448c0 8.8-7.2 16-16 16H64c-8.8 0-16-7.2-16-16zM64 0C28.7 0 0 28.7 0 64V448c0 35.3 28.7 64 64 64H320c35.3 0 64-28.7 64-64V154.5c0-17-6.7-33.3-18.7-45.3L274.7 18.7C262.7 6.7 246.5 0 229.5 0H64zm90.9 233.3c-8.1-10.5-23.2-12.3-33.7-4.2s-12.3 23.2-4.2 33.7L161.6 320l-44.5 57.3c-8.1 10.5-6.3 25.5 4.2 33.7s25.5 6.3 33.7-4.2L192 359.1l37.1 47.6c8.1 10.5 23.2 12.3 33.7 4.2s12.3-23.2 4.2-33.7L222.4 320l44.5-57.3c8.1-10.5 6.3-25.5-4.2-33.7s-25.5-6.3-33.7 4.2L192 280.9l-37.1-47.6z">
                </path>
            </svg>
        </button><button type="button" onclick="window.location.href='cont-reflejardocsccpet.php'"
            class="btn btn-success btn-success-hover d-flex justify-between align-items-center">
            <span>Atras</span>
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 -960 960 960">
                <path
                    d="M760-200v-160q0-50-35-85t-85-35H273l144 144-57 56-240-240 240-240 57 56-144 144h367q83 0 141.5 58.5T840-360v160h-80Z">
                </path>
            </svg>
        </button></div>
    <form name="form2" method="post" action="">
        <?php
        $pf[] = array();
        $pfcp = array();
        ?>
        <div class="loading" id="divcarga"><span>Cargando...</span></div>
        <table class="inicio ancho">
            <tr>
                <td class="titulos" colspan="10">:: Buscar Liquidaciones</td>
                <td class="cerrar" style="width:7%" onClick="location.href='cont-principal.php'">Cerrar</td>
            </tr>
            <tr>
                <td class="tamano01">No Liquidaci&oacute;n</td>
                <td>
                    <select name="idliq" id="idliq" onChange="validar()" style="width: 100%;">
                        <option value="-1">Sel ...</option>
                        <?php
                        $sqlr = "SELECT T1.id_nom,T1.fecha,T1.periodo,T1.mes,T1.cc,T1.vigencia FROM humnomina AS T1 INNER JOIN humnomina_aprobado AS T2 ON T1.id_nom=T2.id_nom AND T2.estado='S' WHERE T1.vigencia >= '2021' ORDER BY T1.id_nom DESC";
                        $resp = mysqli_query($linkbd, $sqlr);
                        while ($row = mysqli_fetch_row($resp)) {
                            if ($row[0] == $_POST['idliq']) {
                                echo "<option value='$row[0]' SELECTED>$row[0]</option>";
                                $_POST['periodo'] = $row[3];
                                $_POST['cc'] = $row[4];
                                ereg("([0-9]{2,4})-([0-9]{1,2})-([0-9]{1,2})", $row[1], $fecha);
                                $fechaf = $fecha[3] . "/" . $fecha[2] . "/" . $fecha[1];
                                $_POST['fecha'] = $fechaf;
                                $_POST['fechafi'] = $row[1];
                                $_POST['vigenomi'] = $row[5];
                                $sqlr2 = "SELECT cdp,rp FROM hum_nom_cdp_rp WHERE nomina = '$row[0]' AND estado='S'";
                                $resp2 = mysqli_query($linkbd, $sqlr2);
                                $row2 = mysqli_fetch_row($resp2);
                                $_POST['cdp'] = $row2[0];
                                $_POST['rp'] = $row2[1];
                                $sqlr3 = "SELECT valor,detalle,tercero FROM ccpetrp WHERE consvigencia = '$row2[1]' AND vigencia = '$row[5]' AND idcdp = '$row2[0]'";
                                $resp3 = mysqli_query($linkbd, $sqlr3);
                                $row3 = mysqli_fetch_row($resp3);
                                $_POST['valorp'] = $row3[0];
                                $_POST['hvalorp'] = $row3[0];
                                $_POST['detallecdp'] = $row3[1];
                                $_POST['tercero'] = $row3[2];
                                $_POST['ntercero'] = buscatercero($row3[2]);
                            } else {
                                echo "<option value='$row[0]'>$row[0]</option>";
                            }
                        }
                        ?>
                    </select>
                </td>
                <td class="tamano01">Fecha</td>
                <td><input type="text" name="fecha" id="fecha" value="<?php echo @$_POST['fecha'] ?>"
                        style="width: 100%;" readonly /></td>
                <td class="tamano01">CDP:</td>
                <td><input type="text" id="cdp" name="cdp" value="<?php echo @$_POST['cdp'] ?>" style="width: 100%;"
                        readonly></td>
                <td class="tamano01">RP</td>
                <td><input type="text" name="rp" id="rp" value="<?php echo @$_POST['rp'] ?>" style="width: 100%;"
                        readonly /></td>
                <td class="tamano01">Valor RP:</td>
                <td><input type="text" name="valorp" id="valorp"
                        value="<?php echo @number_format($_POST['valorp'], 2) ?>" style="width: 100%;" readonly /></td>
                <input type="hidden" name="hvalorp" id="hvalorp" value="<?php echo @$_POST['hvalorp'] ?>" />
            </tr>
            <tr>
                <td class="tamano01">Detalle RP:</td>
                <td colspan="3"><input type="text" name="detallecdp" id="detallecdp"
                        value="<?php echo @$_POST['detallecdp'] ?>" style="width: 100%;" readonly /></td>
                <td class="tamano01">Tercero:</td>
                <td><input type="text" name="tercero" id="tercero" value="<?php echo @$_POST['tercero'] ?>"
                        style="width: 100%;" readonly />
                <td colspan="4"><input type="text" name="ntercero" id="ntercero"
                        value="<?php echo @$_POST['ntercero'] ?>" style="width: 100%;" readonly /></td>
            </tr>
            <tr>
                <td class="tamano01" style="width:3cm">Causacion Contable:</td>
                <td>
                    <select name="causacion" id="causacion" onKeyUp="return tabular(event,this)" style="width: 100%;">
                        <option value="1" <?php if ($_POST['causacion'] == '1')
                            echo "selected" ?>>Si</option>
                            <option value="2" <?php if ($_POST['causacion'] == '2')
                            echo "selected" ?>>No</option>
                        </select>
                    </td>
                    <?php
                        if (!$_POST['oculto']) {
                            $_POST['diast'] = array();
                            $_POST['devengado'] = array();
                            $_POST['empleados'] = array();
                        }
                        $sqlr = "SELECT balimentacion,btransporte,bfsol,alimentacion,transporte,salario,cajas,icbf,sena,esap,iti FROM admfiscales WHERE vigencia = '" . $_POST['vigenomi'] . "'";
                        $resp = mysqli_query($linkbd, $sqlr);
                        while ($row = mysqli_fetch_row($resp)) {
                            $_POST['balim'] = $row[0];
                            $_POST['btrans'] = $row[1];
                            $_POST['bfsol'] = $row[2];
                            $_POST['alim'] = $row[3];
                            $_POST['transp'] = $row[4];
                            $_POST['salmin'] = $row[5];
                            $_POST['cajacomp'] = $row[6];
                            $_POST['icbf'] = $row[7];
                            $_POST['sena'] = $row[8];
                            $_POST['esap'] = $row[9];
                            $_POST['iti'] = $row[10];
                        }
                        $sqlr = "SELECT sueldo,cajacompensacion,icbf,sena,iti,esap,arp,salud_empleador,salud_empleado,pension_empleador,pension_empleado, sub_alimentacion,aux_transporte,prima_navidad FROM humparametrosliquida";
                        $resp = mysqli_query($linkbd, $sqlr);
                        while ($row = mysqli_fetch_row($resp)) {
                            $_POST['psalmin'] = $row[0];
                            $_POST['pcajacomp'] = $row[1];
                            $_POST['picbf'] = $row[2];
                            $_POST['psena'] = $row[3];
                            $_POST['piti'] = $row[4];
                            $_POST['pesap'] = $row[5];
                            $_POST['parp'] = $row[6];
                            $_POST['psalud_empleador'] = $row[7];
                            $_POST['psalud_empleado'] = $row[8];
                            $_POST['ppension_empleador'] = $row[9];
                            $_POST['ppension_empleado'] = $row[10];
                            $_POST['palim'] = $row[11];
                            $_POST['ptransp'] = $row[12];
                            $_POST['pbfsol'] = $_POST['ppension_empleado'];
                            $_POST['tprimanav'] = $row[13];
                        }
                        ?>
                <input type="hidden" id="cajacomp" name="cajacomp" value="<?php echo @$_POST['cajacomp'] ?>" />
                <input type="hidden" id="icbf" name="icbf" value="<?php echo @$_POST['icbf'] ?>" />
                <input type="hidden" id="sena" name="sena" value="<?php echo @$_POST['sena'] ?>" />
                <input type="hidden" id="esap" name="esap" value="<?php echo @$_POST['esap'] ?>" />
                <input type="hidden" id="iti" name="iti" value="<?php echo @$_POST['iti'] ?>" />
                <input type="hidden" id="btrans" name="btrans" value="<?php echo @$_POST['btrans'] ?>" />
                <input type="hidden" id="balim" name="balim" value="<?php echo @$_POST['balim'] ?>" />
                <input type="hidden" id="bfsol" name="bfsol" value="<?php echo @$_POST['bfsol'] ?>" />
                <input type="hidden" id="transp" name="transp" value="<?php echo @$_POST['transp'] ?>" />
                <input type="hidden" id="alim" name="alim" value="<?php echo @$_POST['alim'] ?>">
                <input type="hidden" id="salmin" name="salmin" value="<?php echo @$_POST['salmin'] ?>" />
                <input type="hidden" id="tprimanav" name="tprimanav" value="<?php echo @$_POST['tprimanav'] ?>" />
                <input type="hidden" id="pcajacomp" name="pcajacomp" value="<?php echo @$_POST['pcajacomp'] ?>" />
                <input type="hidden" id="picbf" name="picbf" value="<?php echo @$_POST['picbf'] ?>" />
                <input type="hidden" id="psena" name="psena" value="<?php echo @$_POST['psena'] ?>" />
                <input type="hidden" id="pesap" name="pesap" value="<?php echo @$_POST['pesap'] ?>" />
                <input type="hidden" id="piti" name="piti" value="<?php echo @$_POST['piti'] ?>" />
                <input type="hidden" id="psalud_empleado" name="psalud_empleado"
                    value="<?php echo @$_POST['psalud_empleado'] ?>" />
                <input type="hidden" id="psalud_empleador" name="psalud_empleador"
                    value="<?php echo @$_POST['psalud_empleador'] ?>" />
                <input type="hidden" id="ppension_empleador" name="ppension_empleador"
                    value="<?php echo @$_POST['ppension_empleador'] ?>" />
                <input type="hidden" id="ppension_empleado" name="ppension_empleado"
                    value="<?php echo @$_POST['ppension_empleado'] ?>" />
                <input type="hidden" id="pbfsol" name="pbfsol" value="<?php echo @$_POST['pbfsol'] ?>" />
                <input type="hidden" id="ptransp" name="ptransp" value="<?php echo @$_POST['ptransp'] ?>" />
                <input type="hidden" id="palim" name="palim" value="<?php echo @$_POST['palim'] ?>" />
                <input type="hidden" id="psalmin" name="psalmin" value="<?php echo @$_POST['psalmin'] ?>" />
                <input type="hidden" id="parp" name="parp" value="<?php echo @$_POST['parp'] ?>" />
                <input type="hidden" id="vigenomi" name="vigenomi" value="<?php echo @$_POST['vigenomi'] ?>" />
                <input type="hidden" id="fechafi" name="fechafi" value="<?php echo @$_POST['fechafi'] ?>" />
                <input type="hidden" id="oculto" name="oculto" value="1" />
                <td class="saludo1">CC:</td>
                <td>
                    <select name="cc" onChange="validar()" onKeyUp="return tabular(event,this)" style="width: 100%;">
                        <option value='' <?php if ($_POST['cc'] == '')
                            echo "SELECTED" ?>>Todos</option>
                            <?php
                        $sqlr = "SELECT * FROM centrocosto WHERE estado='S'";
                        $res = mysqli_query($linkbd, $sqlr);
                        while ($row = mysqli_fetch_row($res)) {
                            if ($row[0] == $_POST['cc']) {
                                echo "<option value='$row[0]' SELECTED>$row[0] - $row[1]</option>";
                            }
                        }
                        ?>
                    </select>
                </td>
                <td class="saludo1" colspan="1">Mes:</td>
                <td>
                    <select name="periodo" id="periodo" onChange="validar()" style="width: 100%;">
                        <?php
                        $sqlr = "SELECT * FROM meses WHERE estado='S'";
                        $resp = mysqli_query($linkbd, $sqlr);
                        while ($row = mysqli_fetch_row($resp)) {
                            if ($row[0] == $_POST['periodo']) {
                                echo "<option value='$row[0]' SELECTED>$row[1]</option>";
                            }
                        }
                        ?>
                    </select>
                </td>
            </tr>
        </table>
        <div class="subpantalla">
            <?php
            $listacuentas = array();
            $listanombrecuentas = array();
            $listaterceros = array();
            $listanombreterceros = array();
            $listaccs = array();
            $listadetalles = array();
            $listadebitos = array();
            $listacreditos = array();
            $listacajacf[] = array();
            $listasena[] = array();
            $listaicbf[] = array();
            $listainstecnicos[] = array();
            $listaesap[] = array();
            $listatipo[] = array();
            $crit1 = $crit2 = " ";
            $con = 1;
            $mesnnomina = $_POST['periodo'];
            $meslnomina = mesletras($_POST['periodo']);
            $vigenomina = $_POST['vigenomi'];
            echo "
				<table class='inicio'>
					<tr><td colspan='89' class='titulos'>.: Detalles Comprobantes</td></tr>
					<tr>
						<td class='titulos2'>ITEM</td>
						<td class='titulos2'>CUENTA</td>
						<td class='titulos2'>NOMBRE CUENTA</td>
						<td class='titulos2'>TERCERO</td>
						<td class='titulos2'>NOMBRE TERCERO</td>
						<td class='titulos2'>CC</td>
						<td class='titulos2'>DETALLE</td>
						<td class='titulos2'>VLR. DEBITO</td>
						<td class='titulos2'>VLR. CREDITO</td>
					</tr>";
            $iter = "zebra1";
            $iter2 = "zebra2";
            $sqlr = "SELECT cedulanit,SUM(devendias),SUM(auxalim),SUM(auxtran),SUM(salud),SUM(saludemp),SUM(pension),SUM(pensionemp),SUM(fondosolid), SUM(otrasdeduc),SUM(arp),SUM(cajacf),SUM(sena),SUM(icbf),SUM(instecnicos),SUM(esap),tipofondopension,prima_navi,cc,tipopago,SUM(retefte),idfuncionario FROM humnomina_det WHERE id_nom='" . $_POST['idliq'] . "' GROUP BY idfuncionario, tipopago ORDER BY tipopago, cc";
            $resp = mysqli_query($linkbd, $sqlr);
            while ($row = mysqli_fetch_row($resp)) {
                $ccosto = $row[18];
                $empleado = buscatercero($row[0]);
                if ($row[1] != 0)//tipo de pago (Salarios, Subsidios, primas .....)
                {
                    $ctaconcepto = $ctacont = '';
                    //Cuenta debito salario empleado
                    $ctacont = cuentascontablesccpet::cuentadebito_tipomovccpet_2021($row[19], $row[21], $row[18], $_POST['fechafi'], '1', '');
                    $nresul = buscacuenta($ctacont);
                    $nomceunta = nombrevariblespagonomina($row[19]);
                    if ($ctacont == 'Sin Cuenta') {
                        $estilo = 'background-color: yellow;';
                    } else {
                        $estilo = "";
                    }
                    echo "
						<tr class='$iter' style='$estilo'>
							<td>$con</td>
							<td>$ctacont</td>
							<td>$nresul</td>
							<td>$row[0]</td>
							<td>$empleado</td>
							<td>$row[18]</td>
							<td>$nomceunta Mes $meslnomina </td>
							<td style='text-align:right;font-size:10px;'>&nbsp;$" . number_format($row[1], 0) . "</td>
							<td style='text-align:right;font-size:10px;'>&nbsp;$" . number_format(0, 0) . "</td>
						</tr>";
                    $listacuentas[] = $ctacont;
                    $listanombrecuentas[] = $nresul;
                    $listaterceros[] = $row[0];
                    $listanombreterceros[] = $empleado;
                    $listaccs[] = $row[18];
                    $listadetalles[] = "$nomceunta Mes $meslnomina";
                    $listadebitos[] = $row[1];
                    $listacreditos[] = 0;
                    $listatipo[] = "$row[19]<->DB";
                    $aux = $iter;
                    $iter = $iter2;
                    $iter2 = $aux;
                    $con += 1;
                    //Cuenta credito salario empleado
                    $ctaconcepto = cuentascontablesccpet::cuentacredito_tipomovccpet_2021($row[19], $row[21], $row[18], $_POST['fechafi'], '1', '');
                    $nresul = buscacuenta($ctaconcepto);
                    if ($ctaconcepto === 'Sin Cuenta') {
                        $estilo = 'background-color: yellow;';
                    } else {
                        $estilo = "";
                    }
                    echo "
						<tr class='$iter' style='$estilo'>
							<td>$con</td>
							<td>$ctaconcepto</td>
							<td>$nresul</td>
							<td>$row[0]</td>
							<td>$empleado</td>
							<td>$row[18]</td>
							<td>$nomceunta Mes $meslnomina</td>
							<td style='text-align:right;font-size:10px;'>&nbsp;$" . number_format(0, 0) . "</td>
							<td style='text-align:right;font-size:10px;'>&nbsp;$" . number_format($row[1], 0) . "</td>
						</tr>";
                    $listacuentas[] = $ctaconcepto;
                    $listanombrecuentas[] = $nresul;
                    $listaterceros[] = $row[0];
                    $listanombreterceros[] = $empleado;
                    $listaccs[] = $row[18];
                    $listadetalles[] = "$nomceunta Mes $meslnomina";
                    $listadebitos[] = 0;
                    $listacreditos[] = $row[1];
                    $listatipo[] = "$row[19]<->CR";
                    $aux = $iter;
                    $iter = $iter2;
                    $iter2 = $aux;
                    $con += 1;
                }
                if ($row[4] != 0)//Salud Empleado
                {
                    $ctaconcepto = $ctacont = '';
                    //Cuenta debito salud empleado
                    $ctacont = cuentascontablesccpet::cuentacredito_tipomovccpet_2021($row[19], $row[21], $row[18], $_POST['fechafi'], '1', '');
                    $nresul = buscacuenta($ctacont);
                    if ($ctacont == 'Sin Cuenta') {
                        $estilo = 'background-color: yellow;';
                    } else {
                        $estilo = "";
                    }
                    echo "
						<tr class='$iter' style='$estilo'>
							<td>$con</td>
							<td>$ctacont</td>
							<td>$nresul</td>
							<td>$row[0]</td>
							<td>$empleado</td>
							<td>$row[18]</td>
							<td>Aporte Salud Empleado Mes $meslnomina</td>
							<td style='text-align:right;font-size:10px;'>&nbsp;$" . number_format($row[4], 0) . "</td>
							<td style='text-align:right;font-size:10px;'>&nbsp;$" . number_format(0, 0) . "</td>
						</tr>";
                    $listacuentas[] = $ctacont;
                    $listanombrecuentas[] = $nresul;
                    $listaterceros[] = $row[0];
                    $listanombreterceros[] = $empleado;
                    $listaccs[] = $row[18];
                    $listadetalles[] = "Aporte Salud Empleado Mes $meslnomina";
                    $listadebitos[] = $row[4];
                    $listacreditos[] = 0;
                    $listatipo[] = "SE<->DB";
                    $aux = $iter;
                    $iter = $iter2;
                    $iter2 = $aux;
                    $con += 1;
                    //Cuenta credito salud empleado
                    $ctaconcepto = cuentascontablesccpet::cuentacredito_tipomovccpet_2021($_POST['psalud_empleado'], $row[21], $row[18], $_POST['fechafi'], '2', 'N/A');
                    $nresul = buscacuenta($ctaconcepto);
                    $epsnit = buscadatofuncionario($row[0], 'NUMEPS');
                    $epsnom = buscadatofuncionario($row[0], 'NOMEPS');
                    if ($ctaconcepto === 'Sin Cuenta') {
                        $estilo = 'background-color: yellow;';
                    } else {
                        $estilo = "";
                    }
                    echo "
						<tr class='$iter' style='$estilo'>
							<td>$con</td>
							<td>$ctaconcepto</td>
							<td>$nresul</td>
							<td>$epsnit</td>
							<td>$epsnom</td>
							<td>$row[18]</td>
							<td>Aporte Salud Empleado Mes $meslnomina</td>
							<td style='text-align:right;font-size:10px;'>&nbsp;$" . number_format(0, 0) . "</td>
							<td style='text-align:right;font-size:10px;'>&nbsp;$" . number_format($row[4], 0) . "</td>
						</tr>";
                    $listacuentas[] = $ctaconcepto;
                    $listanombrecuentas[] = $nresul;
                    $listaterceros[] = $epsnit;
                    $listanombreterceros[] = $epsnom;
                    $listaccs[] = $row[18];
                    $listadetalles[] = "Aporte Salud Empleado Mes $meslnomina";
                    $listadebitos[] = 0;
                    $listacreditos[] = $row[4];
                    $listatipo[] = "SE<->CR";
                    $aux = $iter;
                    $iter = $iter2;
                    $iter2 = $aux;
                    $con += 1;
                }
                if ($row[6] != 0)//Pension Empleado
                {
                    $ctaconcepto = $ctacont = '';
                    //Cuenta debito pension empleado
                    $ctacont = cuentascontablesccpet::cuentacredito_tipomovccpet_2021($row[19], $row[21], $row[18], $_POST['fechafi'], '1', '');
                    $nresul = buscacuenta($ctacont);
                    if ($ctacont == 'Sin Cuenta') {
                        $estilo = 'background-color: yellow;';
                    } else {
                        $estilo = "";
                    }
                    echo "
						<tr class='$iter' style='$estilo'>
							<td>$con</td>
							<td>$ctacont</td>
							<td>$nresul</td>
							<td>$row[0]</td>
							<td>$empleado</td>
							<td>$row[18]</td>
							<td>Aporte Pensi&oacute;n Empleado Mes $meslnomina</td>
							<td style='text-align:right;font-size:10px;'>&nbsp;$" . number_format($row[6], 0) . "</td>
							<td style='text-align:right;font-size:10px;'>&nbsp;$" . number_format(0, 0) . "</td>
						</tr>";
                    $listacuentas[] = $ctacont;
                    $listanombrecuentas[] = $nresul;
                    $listaterceros[] = $row[0];
                    $listanombreterceros[] = $empleado;
                    $listaccs[] = $row[18];
                    $listadetalles[] = "Aporte Pensi�n Empleado Mes $meslnomina";
                    $listadebitos[] = $row[6];
                    $listacreditos[] = 0;
                    $listatipo[] = "PE<->DB";
                    $aux = $iter;
                    $iter = $iter2;
                    $iter2 = $aux;
                    $con += 1;
                    //Cuenta credito pension empleado
                    $ctaconcepto = cuentascontablesccpet::cuentacredito_tipomovccpet_2021($_POST['ppension_empleado'], $row[21], $row[18], $_POST['fechafi'], '2', 'N/A');
                    $nresul = buscacuenta($ctaconcepto);
                    $epsnit = buscadatofuncionario($row[0], 'NUMAFP');
                    $epsnom = buscadatofuncionario($row[0], 'NOMAFP');
                    if ($ctaconcepto === 'Sin Cuenta') {
                        $estilo = 'background-color: yellow;';
                    } else {
                        $estilo = "";
                    }
                    echo "
						<tr class='$iter' style='$estilo'>
							<td>$con</td>
							<td>$ctaconcepto</td>
							<td>$nresul</td>
							<td>$epsnit</td>
							<td>$epsnom</td>
							<td>$row[18]</td>
							<td>Aporte Pension Empleado Mes $meslnomina</td>
							<td style='text-align:right;font-size:10px;'>&nbsp;$" . number_format(0, 0) . "</td>
							<td style='text-align:right;font-size:10px;'>&nbsp;$" . number_format($row[6], 0) . "</td>
						</tr>";
                    $listacuentas[] = $ctaconcepto;
                    $listanombrecuentas[] = $nresul;
                    $listaterceros[] = $epsnit;
                    $listanombreterceros[] = $epsnom;
                    $listaccs[] = $row[18];
                    $listadetalles[] = "Aporte Pension Empleado Mes $meslnomina";
                    $listadebitos[] = 0;
                    $listacreditos[] = $row[6];
                    $listatipo[] = "PE<->CR";
                    $aux = $iter;
                    $iter = $iter2;
                    $iter2 = $aux;
                    $con += 1;
                }
                if ($row[8] != 0)//Fondo Solidaridad
                {
                    $ctaconcepto = $ctacont = '';
                    //Cuenta debito fondo solidaridad
                    $ctacont = cuentascontablesccpet::cuentacredito_tipomovccpet_2021($row[19], $row[21], $row[18], $_POST['fechafi'], '1', '');
                    $nresul = buscacuenta($ctacont);
                    if ($ctacont == 'Sin Cuenta') {
                        $estilo = 'background-color: yellow;';
                    } else {
                        $estilo = "";
                    }
                    echo "
						<tr class='$iter' style='$estilo'>
							<td>$con</td>
							<td>$ctacont</td>
							<td>$nresul</td>
							<td>$row[0]</td>
							<td>$empleado</td>
							<td>$row[18]</td>
							<td>Aporte Fondo Solidaridad Empleado Mes $meslnomina</td>
							<td style='text-align:right;font-size:10px;'>&nbsp;$" . number_format($row[8], 0) . "</td>
							<td style='text-align:right;font-size:10px;'>&nbsp;$" . number_format(0, 0) . "</td>
						</tr>";
                    $listacuentas[] = $ctacont;
                    $listanombrecuentas[] = $nresul;
                    $listaterceros[] = $row[0];
                    $listanombreterceros[] = $empleado;
                    $listaccs[] = $row[18];
                    $listadetalles[] = "Aporte Fondo Solidaridad Empleado Mes $meslnomina";
                    $listadebitos[] = $row[8];
                    $listacreditos[] = 0;
                    $listatipo[] = "FS<->DB";
                    $aux = $iter;
                    $iter = $iter2;
                    $iter2 = $aux;
                    $con += 1;
                    //Cuenta credito fondo solidaridad
                    $ctaconcepto = cuentascontablesccpet::cuentacredito_tipomovccpet_2021($_POST['pbfsol'], $row[21], $row[18], $_POST['fechafi'], '2', 'N/A');
                    $nresul = buscacuenta($ctaconcepto);
                    $epsnit = buscadatofuncionario($row[0], 'NUMAFP');
                    $epsnom = buscadatofuncionario($row[0], 'NOMAFP');
                    if ($ctaconcepto === 'Sin Cuenta') {
                        $estilo = 'background-color: yellow;';
                    } else {
                        $estilo = "";
                    }
                    echo "
						<tr class='$iter' style='$estilo'>
							<td>$con</td>
							<td>$ctaconcepto</td>
							<td>$nresul</td>
							<td>$epsnit</td>
							<td>$epsnom</td>
							<td>$row[18]</td>
							<td>Aporte Fondo Solidaridad Empleado Mes $meslnomina</td>
							<td style='text-align:right;font-size:10px;'>&nbsp;$" . number_format(0, 0) . "</td>
							<td style='text-align:right;font-size:10px;'>&nbsp;$" . number_format($row[8], 0) . "</td>
						</tr>";
                    $listacuentas[] = $ctaconcepto;
                    $listanombrecuentas[] = $nresul;
                    $listaterceros[] = $epsnit;
                    $listanombreterceros[] = $epsnom;
                    $listaccs[] = $row[18];
                    $listadetalles[] = "Aporte Fondo Solidaridado Mes $meslnomina";
                    $listadebitos[] = 0;
                    $listacreditos[] = $row[8];
                    $listatipo[] = "FS<->CR";
                    $aux = $iter;
                    $iter = $iter2;
                    $iter2 = $aux;
                    $con += 1;
                }
                if ($row[9] != 0)//Otras Deducciones
                {
                    $ctaconcepto = $ctacont = '';
                    //$sqlrd1="SELECT T1.valor,T2.id_retencion,T2.tipopago FROM humnominaretenemp AS T1 INNER JOIN humretenempleados AS T2 ON T1.id=T2.id WHERE T1.id_nom='".$_POST['idliq']."' AND T1.cedulanit='$row[0]' AND T1.tipo_des='DS' AND T2.tipopago='".$row[19]."' AND T2.idfuncionario='".$row[21]."'";
                    $sqlrd1 = "SELECT T1.valor,T2.id_retencion,T2.tipopago FROM humnominaretenemp AS T1 INNER JOIN humretenempleados AS T2 ON T1.id=T2.id WHERE T1.id_nom='" . $_POST['idliq'] . "' AND T1.cedulanit='$row[0]' AND T1.tipo_des='DS' AND T2.tipopago='" . $row[19] . "' AND T2.empleado='$row[0]'";
                    $respd1 = mysqli_query($linkbd, $sqlrd1);
                    while ($rowd1 = mysqli_fetch_row($respd1)) {
                        //debito
                        $ctaconcepto = $ctacont = '';
                        $ctacont = cuentascontablesccpet::cuentacredito_tipomovccpet_2021($rowd1[2], $row[21], $row[18], $_POST['fechafi'], '1', '');
                        $nresul = buscacuenta($ctacont);
                        $sqlrdes = "SELECT nombre FROM humvariables WHERE estado='S' AND codigo='$row[19]'";
                        $resdes = mysqli_query($linkbd, $sqlrdes);
                        $rowdes = mysqli_fetch_row($resdes);
                        $nomceunta = ucwords(strtolower($rowdes[0]));
                        if ($ctacont == 'Sin Cuenta') {
                            $estilo = 'background-color: yellow;';
                        } else {
                            $estilo = "";
                        }
                        echo "
							<tr class='$iter' style='$estilo'>
								<td>$con</td>
								<td>$ctacont</td>
								<td>$nresul</td>
								<td>$row[0]</td>
								<td>$empleado</td>
								<td>$row[18]</td>
								<td>Descuento $nomceunta Mes $meslnomina </td>
								<td style='text-align:right;font-size:10px;'>&nbsp;$" . number_format($rowd1[0], 0) . "</td>
								<td style='text-align:right;font-size:10px;'>&nbsp;$" . number_format(0, 0) . "</td>
							</tr>";
                        $listacuentas[] = $ctacont;
                        $listanombrecuentas[] = $nresul;
                        $listaterceros[] = $row[0];
                        $listanombreterceros[] = $empleado;
                        $listaccs[] = $row[18];
                        $listadetalles[] = "Decuento $nomdescu Mes $meslnomina";
                        $listadebitos[] = $rowd1[0];
                        $listacreditos[] = 0;
                        $listatipo[] = "DS<->DB";
                        $aux = $iter;
                        $iter = $iter2;
                        $iter2 = $aux;
                        $con += 1;
                        //credito
                        $sqlrcu = "SELECT DISTINCT T1.nombre,T1.beneficiario,T2.cuenta FROM humvariablesretenciones T1, humvariablesretenciones_det T2 WHERE T1.codigo = '$rowd1[1]' AND T1.codigo = T2.codigo AND T2.credito = 'S' AND fechainicial=(SELECT MAX(T3.fechainicial) FROM humvariablesretenciones_det T3 WHERE T3.codigo=T2.codigo AND T3.credito = 'S' AND T3.fechainicial<='" . $_POST['fechafi'] . "')";
                        $respcu = mysqli_query($linkbd, $sqlrcu);
                        while ($rowcu = mysqli_fetch_row($respcu)) {
                            $ctaconcepto = $rowcu[2];
                            $docbenefi = $rowcu[1];
                            $nomdescu = ucwords(strtolower($rowcu[0]));
                        }
                        //Cuenta credito otras deducciones
                        $nresul = buscacuenta($ctaconcepto);
                        $nombenefi = buscatercero($docbenefi);
                        if ($ctaconcepto === 'Sin Cuenta') {
                            $estilo = 'background-color: yellow;';
                        } else {
                            $estilo = "";
                        }
                        echo "
							<tr class='$iter' style='$estilo'>
								<td>$con</td>
								<td>$ctaconcepto</td>
								<td>$nresul</td>
								<td>$docbenefi</td>
								<td>$nombenefi</td>
								<td>$row[18]</td>
								<td>Decuento $nomdescu Mes $meslnomina</td>
								<td style='text-align:right;font-size:10px;'>&nbsp;$" . number_format(0, 0) . "</td>
								<td style='text-align:right;font-size:10px;'>&nbsp;$" . number_format($rowd1[0], 0) . "</td>
							</tr>";
                        $listacuentas[] = $ctaconcepto;
                        $listanombrecuentas[] = $nresul;
                        $listaterceros[] = $docbenefi;
                        $listanombreterceros[] = $nombenefi;
                        $listaccs[] = $row[18];
                        $listadetalles[] = "Decuento $nomdescu Mes $meslnomina";
                        $listadebitos[] = 0;
                        $listacreditos[] = $rowd1[0];
                        $listatipo[] = "DS<->CR";
                        $aux = $iter;
                        $iter = $iter2;
                        $iter2 = $aux;
                        $con += 1;
                    }
                }
                if ($row[5] != 0)//Salud Empleador
                {
                    $ctaconcepto = $ctacont = '';
                    $epsnit = buscadatofuncionario($row[0], 'NUMEPS');
                    $epsnom = buscadatofuncionario($row[0], 'NOMEPS');
                    //Cuenta debito salud empleador
                    $ctacont = cuentascontablesccpet::cuentadebito_tipomovccpet_2021($_POST['psalud_empleador'], $row[21], $row[18], $_POST['fechafi'], '2', 'N/A');
                    $nresul = buscacuenta($ctacont);
                    if ($ctacont == 'Sin Cuenta') {
                        $estilo = 'background-color: yellow;';
                    } else {
                        $estilo = "";
                    }
                    echo "
						<tr class='$iter' style='$estilo'>
							<td>$con</td>
							<td>$ctacont</td>
							<td>$nresul</td>
							<td>$epsnit</td>
							<td>$epsnom</td>
							<td>$row[18]</td>
							<td>Aporte Salud Empleador Mes $meslnomina</td>
							<td style='text-align:right;font-size:10px;'>&nbsp;$" . number_format($row[5], 0) . "</td>
							<td style='text-align:right;font-size:10px;'>&nbsp;$" . number_format(0, 0) . "</td>
						</tr>";
                    $listacuentas[] = $ctacont;
                    $listanombrecuentas[] = $nresul;
                    $listaterceros[] = $epsnit;
                    $listanombreterceros[] = $epsnom;
                    $listaccs[] = $row[18];
                    $listadetalles[] = "Aporte Salud Empleador Mes $meslnomina";
                    $listadebitos[] = $row[5];
                    $listacreditos[] = 0;
                    $listatipo[] = "SR<->DB";
                    $aux = $iter;
                    $iter = $iter2;
                    $iter2 = $aux;
                    $con += 1;
                    //Cuenta credito salud empleador
                    $ctaconcepto = cuentascontablesccpet::cuentacredito_tipomovccpet_2021($_POST['psalud_empleador'], $row[21], $row[18], $_POST['fechafi'], '2', 'N/A');
                    $nresul = buscacuenta($ctaconcepto);
                    if ($ctaconcepto === 'Sin Cuenta') {
                        $estilo = 'background-color: yellow;';
                    } else {
                        $estilo = "";
                    }
                    echo "
						<tr class='$iter' style='$estilo'>
							<td>$con</td>
							<td>$ctaconcepto</td>
							<td>$nresul</td>
							<td>$epsnit</td>
							<td>$epsnom</td>
							<td>$row[18]</td>
							<td>Aporte Salud Empleador Mes $meslnomina</td>
							<td style='text-align:right;font-size:10px;'>&nbsp;$" . number_format(0, 0) . "</td>
							<td style='text-align:right;font-size:10px;'>&nbsp;$" . number_format($row[5], 0) . "</td>
						</tr>";
                    $listacuentas[] = $ctaconcepto;
                    $listanombrecuentas[] = $nresul;
                    $listaterceros[] = $epsnit;
                    $listanombreterceros[] = $epsnom;
                    $listaccs[] = $row[18];
                    $listadetalles[] = "Aporte Salud Empleador Mes $meslnomina";
                    $listadebitos[] = 0;
                    $listacreditos[] = $row[5];
                    $listatipo[] = "SR<->CR";
                    $aux = $iter;
                    $iter = $iter2;
                    $iter2 = $aux;
                    $con += 1;
                }
                if ($row[7] != 0)//Pension Empleador
                {
                    $ctaconcepto = $ctacont = '';
                    $epsnit = buscadatofuncionario($row[0], 'NUMAFP');
                    $epsnom = buscadatofuncionario($row[0], 'NOMAFP');
                    //Cuenta debito pension empleador
                    $ctacont = cuentascontablesccpet::cuentadebito_tipomovccpet_2021($_POST['ppension_empleador'], $row[21], $row[18], $_POST['fechafi'], '2', $row[16]);
                    $nresul = buscacuenta($ctacont);
                    if ($ctacont == 'Sin Cuenta') {
                        $estilo = 'background-color: yellow;';
                    } else {
                        $estilo = "";
                    }
                    echo "
						<tr class='$iter' style='$estilo'>
							<td>$con</td>
							<td>$ctacont</td>
							<td>$nresul</td>
							<td>$epsnit</td>
							<td>$epsnom</td>
							<td>$row[18]</td>
							<td>Aporte Pensi&oacute;n Empleador Mes $meslnomina</td>
							<td style='text-align:right;font-size:10px;'>&nbsp;$" . number_format($row[7], 0) . "</td>
							<td style='text-align:right;font-size:10px;'>&nbsp;$" . number_format(0, 0) . "</td>
						</tr>";
                    $listacuentas[] = $ctacont;
                    $listanombrecuentas[] = $nresul;
                    $listaterceros[] = $epsnit;
                    $listanombreterceros[] = $epsnom;
                    $listaccs[] = $row[18];
                    $listadetalles[] = "Aporte Pensi�n Empleador Mes $meslnomina";
                    $listadebitos[] = $row[7];
                    $listacreditos[] = 0;
                    $listatipo[] = "PR<->DB";
                    $aux = $iter;
                    $iter = $iter2;
                    $iter2 = $aux;
                    $con += 1;
                    //Cuenta credito pension empleador
                    $ctaconcepto = cuentascontablesccpet::cuentacredito_tipomovccpet_2021($_POST['ppension_empleador'], $row[21], $row[18], $_POST['fechafi'], '2', $row[16]);
                    $nresul = buscacuenta($ctaconcepto);
                    if ($ctaconcepto === 'Sin Cuenta') {
                        $estilo = 'background-color: yellow;';
                    } else {
                        $estilo = "";
                    }
                    echo "
						<tr class='$iter' style='$estilo'>
							<td>$con</td>
							<td>$ctaconcepto</td>
							<td>$nresul</td>
							<td>$epsnit</td>
							<td>$epsnom</td>
							<td>$row[18]</td>
							<td>Aporte Pensi&oacute;n Empleador Mes $meslnomina</td>
							<td style='text-align:right;font-size:10px;'>&nbsp;$" . number_format(0, 0) . "</td>
							<td style='text-align:right;font-size:10px;'>&nbsp;$" . number_format($row[7], 0) . "</td>
						</tr>";
                    $listacuentas[] = $ctaconcepto;
                    $listanombrecuentas[] = $nresul;
                    $listaterceros[] = $epsnit;
                    $listanombreterceros[] = $epsnom;
                    $listaccs[] = $row[18];
                    $listadetalles[] = "Aporte Pensi�n Empleador Mes $meslnomina";
                    $listadebitos[] = 0;
                    $listacreditos[] = $row[7];
                    $listatipo[] = "PR<->CR";
                    $aux = $iter;
                    $iter = $iter2;
                    $iter2 = $aux;
                    $con += 1;
                }
                if ($row[10] != 0)//ARL
                {
                    $ctaconcepto = $ctacont = '';
                    $epsnit = buscadatofuncionario($row[0], 'NUMARL');
                    $epsnom = buscadatofuncionario($row[0], 'NOMARL');
                    //Cuenta debito ARL empleador
                    $ctacont = cuentascontablesccpet::cuentadebito_tipomovccpet_2021($_POST['parp'], $row[21], $row[18], $_POST['fechafi'], '2', 'N/A');
                    $nresul = buscacuenta($ctacont);
                    if ($ctacont == 'Sin Cuenta') {
                        $estilo = 'background-color: yellow;';
                    } else {
                        $estilo = "";
                    }
                    echo "
						<tr class='$iter' style='$estilo'>
							<td>$con</td>
							<td>$ctacont</td>
							<td>$nresul</td>
							<td>$epsnit</td>
							<td>$epsnom</td>
							<td>$row[18]</td>
							<td>Aportes ARL Mes $meslnomina</td>
							<td style='text-align:right;font-size:10px;'>&nbsp;$" . number_format($row[10], 0) . "</td>
							<td style='text-align:right;font-size:10px;'>&nbsp;$" . number_format(0, 0) . "</td>
						</tr>";
                    $listacuentas[] = $ctacont;
                    $listanombrecuentas[] = $nresul;
                    $listaterceros[] = $epsnit;
                    $listanombreterceros[] = $epsnom;
                    $listaccs[] = $row[18];
                    $listadetalles[] = "Aportes ARL Mes $meslnomina";
                    $listadebitos[] = $row[10];
                    $listacreditos[] = 0;
                    $listatipo[] = "P6<->DB";
                    $aux = $iter;
                    $iter = $iter2;
                    $iter2 = $aux;
                    $con += 1;
                    //Cuenta credito ARL empleado
                    $ctaconcepto = cuentascontablesccpet::cuentacredito_tipomovccpet_2021($_POST['parp'], $row[21], $row[18], $_POST['fechafi'], '2', 'N/A');
                    $nresul = buscacuenta($ctaconcepto);
                    if ($ctaconcepto === 'Sin Cuenta') {
                        $estilo = 'background-color: yellow;';
                    } else {
                        $estilo = "";
                    }
                    echo "
						<tr class='$iter' style='$estilo'>
							<td>$con</td>
							<td>$ctaconcepto</td>
							<td>$nresul</td>
							<td>$epsnit</td>
							<td>$epsnom</td>
							<td>$row[18]</td>
							<td>Aportes ARL Mes $meslnomina</td>
							<td style='text-align:right;font-size:10px;'>&nbsp;$" . number_format(0, 0) . "</td>
							<td style='text-align:right;font-size:10px;'>&nbsp;$" . number_format($row[10], 0) . "</td>
						</tr>";
                    $listacuentas[] = $ctaconcepto;
                    $listanombrecuentas[] = $nresul;
                    $listaterceros[] = $epsnit;
                    $listanombreterceros[] = $epsnom;
                    $listaccs[] = $row[18];
                    $listadetalles[] = "Aportes ARL Mes $meslnomina";
                    $listadebitos[] = 0;
                    $listacreditos[] = $row[10];
                    $listatipo[] = "P6<->CR";
                    $aux = $iter;
                    $iter = $iter2;
                    $iter2 = $aux;
                    $con += 1;
                }
                if ($row[11] != 0)//COFREM
                {
                    $ctaconcepto = $ctacont = '';
                    $parafiscal = $_POST['pcajacomp'];
                    $nomparafiscal = buscatercero($_POST['cajacomp']);
                    $nitparafiscal = $_POST['cajacomp'];
                    $valparafiscal = $row[11];
                    //Cuenta debito Caja de compensaci�n familiar
                    $ctacont = cuentascontablesccpet::cuentadebito_tipomovccpet_2021($parafiscal, $row[21], $row[18], $_POST['fechafi'], '2', 'N/A');
                    $nresul = buscacuenta($ctacont);
                    if ($ctacont == 'Sin Cuenta') {
                        $estilo = 'background-color: yellow;';
                    } else {
                        $estilo = "";
                    }
                    echo "
						<tr class='$iter' style='$estilo'>
							<td>$con</td>
							<td>$ctacont</td>
							<td>$nresul</td>
							<td>$nitparafiscal</td>
							<td>$nomparafiscal</td>
							<td>$row[18]</td>
							<td>Aportes Caja Compensaci&oacute;n Mes $meslnomina</td>
							<td style='text-align:right;font-size:10px;'>&nbsp;$" . number_format($row[11], 0) . "</td>
							<td style='text-align:right;font-size:10px;'>&nbsp;$" . number_format(0, 0) . "</td>
						</tr>";
                    $listacuentas[] = $ctacont;
                    $listanombrecuentas[] = $nresul;
                    $listaterceros[] = $nitparafiscal;
                    $listanombreterceros[] = $nomparafiscal;
                    $listaccs[] = $row[18];
                    $listadetalles[] = "Aportes Caja Compensaci�n Mes $meslnomina";
                    $listadebitos[] = $row[11];
                    $listacreditos[] = 0;
                    $listatipo[] = "P1<->DB";
                    $aux = $iter;
                    $iter = $iter2;
                    $iter2 = $aux;
                    $con += 1;
                    //Cuenta credito Caja de compensaci�n familiar
                    $ctaconcepto = cuentascontablesccpet::cuentacredito_tipomovccpet_2021($parafiscal, $row[21], $row[18], $_POST['fechafi'], '2', 'N/A');
                    $nresul = buscacuenta($ctaconcepto);
                    if ($ctaconcepto === 'Sin Cuenta') {
                        $estilo = 'background-color: yellow;';
                    } else {
                        $estilo = "";
                    }
                    echo "
						<tr class='$iter' style='$estilo'>
							<td>$con</td>
							<td>$ctaconcepto</td>
							<td>$nresul</td>
							<td>$nitparafiscal</td>
							<td>$nomparafiscal</td>
							<td>$row[18]</td>
							<td>Aportes Caja Compensaci&oacute;n Mes $meslnomina</td>
							<td style='text-align:right;font-size:10px;'>&nbsp;$" . number_format(0, 0) . "</td>
							<td style='text-align:right;font-size:10px;'>&nbsp;$" . number_format($row[11], 0) . "</td>
						</tr>";
                    $listacuentas[] = $ctaconcepto;
                    $listanombrecuentas[] = $nresul;
                    $listaterceros[] = $nitparafiscal;
                    $listanombreterceros[] = $nomparafiscal;
                    $listaccs[] = $row[18];
                    $listadetalles[] = "Aportes Caja Compensaci�n Mes $meslnomina";
                    $listadebitos[] = 0;
                    $listacreditos[] = $row[11];
                    $listatipo[] = "P1<->CR";
                    $aux = $iter;
                    $iter = $iter2;
                    $iter2 = $aux;
                    $con += 1;
                }
                if ($row[12] != 0)//SENA
                {
                    $ctaconcepto = $ctacont = '';
                    $parafiscal = $_POST['psena'];
                    $nitparafiscal = $_POST['sena'];
                    $nomparafiscal = buscatercero($_POST['sena']);
                    //Cuenta debito SENA
                    $ctacont = cuentascontablesccpet::cuentadebito_tipomovccpet_2021($parafiscal, $row[21], $row[18], $_POST['fechafi'], '2', 'N/A');
                    $nresul = buscacuenta($ctacont);
                    if ($ctacont == 'Sin Cuenta') {
                        $estilo = 'background-color: yellow;';
                    } else {
                        $estilo = "";
                    }
                    echo "
						<tr class='$iter' style='$estilo'>
							<td>$con</td>
							<td>$ctacont</td>
							<td>$nresul</td>
							<td>$nitparafiscal</td>
							<td>$nomparafiscal</td>
							<td>$row[18]</td>
							<td>Aportes SENA Mes $meslnomina</td>
							<td style='text-align:right;font-size:10px;'>&nbsp;$" . number_format($row[12], 0) . "</td>
							<td style='text-align:right;font-size:10px;'>&nbsp;$" . number_format(0, 0) . "</td>
						</tr>";
                    $listacuentas[] = $ctacont;
                    $listanombrecuentas[] = $nresul;
                    $listaterceros[] = $nitparafiscal;
                    $listanombreterceros[] = $nomparafiscal;
                    $listaccs[] = $row[18];
                    $listadetalles[] = "Aportes SENA Mes $meslnomina";
                    $listadebitos[] = $row[12];
                    $listacreditos[] = 0;
                    $listatipo[] = "P3<->DB";
                    $aux = $iter;
                    $iter = $iter2;
                    $iter2 = $aux;
                    $con += 1;
                    //Cuenta credito ICBF
                    $ctaconcepto = cuentascontablesccpet::cuentacredito_tipomovccpet_2021($parafiscal, $row[21], $row[18], $_POST['fechafi'], '2', 'N/A');
                    $nresul = buscacuenta($ctaconcepto);
                    if ($ctaconcepto === 'Sin Cuenta') {
                        $estilo = 'background-color: yellow;';
                    } else {
                        $estilo = "";
                    }
                    echo "
						<tr class='$iter' style='$estilo'>
							<td>$con</td>
							<td>$ctaconcepto</td>
							<td>$nresul</td>
							<td>$nitparafiscal</td>
							<td>$nomparafiscal</td>
							<td>$row[18]</td>
							<td>Aportes SENA Mes $meslnomina</td>
							<td style='text-align:right;font-size:10px;'>&nbsp;$" . number_format(0, 0) . "</td>
							<td style='text-align:right;font-size:10px;'>&nbsp;$" . number_format($row[12], 0) . "</td>
						</tr>";
                    $listacuentas[] = $ctaconcepto;
                    $listanombrecuentas[] = $nresul;
                    $listaterceros[] = $nitparafiscal;
                    $listanombreterceros[] = $nomparafiscal;
                    $listaccs[] = $row[18];
                    $listadetalles[] = "Aportes SENA Mes $meslnomina";
                    $listadebitos[] = 0;
                    $listacreditos[] = $row[12];
                    $listatipo[] = "P3<->CR";
                    $aux = $iter;
                    $iter = $iter2;
                    $iter2 = $aux;
                    $con += 1;
                }
                if ($row[13] != 0)//ICBF
                {
                    $ctaconcepto = $ctacont = '';
                    $parafiscal = $_POST['picbf'];
                    $nomparafiscal = buscatercero($_POST['icbf']);
                    $nitparafiscal = $_POST['icbf'];
                    //Cuenta debito ICBF
                    $ctacont = cuentascontablesccpet::cuentadebito_tipomovccpet_2021($parafiscal, $row[21], $row[18], $_POST['fechafi'], '2', 'N/A');
                    $nresul = buscacuenta($ctacont);
                    if ($ctacont == 'Sin Cuenta') {
                        $estilo = 'background-color: yellow;';
                    } else {
                        $estilo = "";
                    }
                    echo "
						<tr class='$iter' style='$estilo'>
							<td>$con</td>
							<td>$ctacont</td>
							<td>$nresul</td>
							<td>$nitparafiscal</td>
							<td>$nomparafiscal</td>
							<td>$row[18]</td>
							<td>Aportes ICBF Mes $meslnomina</td>
							<td style='text-align:right;font-size:10px;'>&nbsp;$" . number_format($row[13], 0) . "</td>
							<td style='text-align:right;font-size:10px;'>&nbsp;$" . number_format(0, 0) . "</td>
						</tr>";
                    $listacuentas[] = $ctacont;
                    $listanombrecuentas[] = $nresul;
                    $listaterceros[] = $nitparafiscal;
                    $listanombreterceros[] = $nomparafiscal;
                    $listaccs[] = $row[18];
                    $listadetalles[] = "Aportes ICBF Mes $meslnomina";
                    $listadebitos[] = $row[13];
                    $listacreditos[] = 0;
                    $listatipo[] = "P2<->DB";
                    $aux = $iter;
                    $iter = $iter2;
                    $iter2 = $aux;
                    $con += 1;
                    //Cuenta credito ICBF
                    $ctaconcepto = cuentascontablesccpet::cuentacredito_tipomovccpet_2021($parafiscal, $row[21], $row[18], $_POST['fechafi'], '2', 'N/A');
                    $nresul = buscacuenta($ctaconcepto);
                    if ($ctaconcepto === 'Sin Cuenta') {
                        $estilo = 'background-color: yellow;';
                    } else {
                        $estilo = "";
                    }
                    echo "
						<tr class='$iter' style='$estilo'>
							<td>$con</td>
							<td>$ctaconcepto</td>
							<td>$nresul</td>
							<td>$nitparafiscal</td>
							<td>$nomparafiscal</td>
							<td>$row[18]</td>
							<td>Aportes ICBF Mes $meslnomina</td>
							<td style='text-align:right;font-size:10px;'>&nbsp;$" . number_format(0, 0) . "</td>
							<td style='text-align:right;font-size:10px;'>&nbsp;$" . number_format($row[13], 0) . "</td>
						</tr>";
                    $listacuentas[] = $ctaconcepto;
                    $listanombrecuentas[] = $nresul;
                    $listaterceros[] = $nitparafiscal;
                    $listanombreterceros[] = $nomparafiscal;
                    $listaccs[] = $row[18];
                    $listadetalles[] = "Aportes ICBF Mes $meslnomina";
                    $listadebitos[] = 0;
                    $listacreditos[] = $row[13];
                    $listatipo[] = "P2<->CR";
                    $aux = $iter;
                    $iter = $iter2;
                    $iter2 = $aux;
                    $con += 1;
                }
                if ($row[14] != 0)//INSTITUTOS TEC
                {
                    $ctaconcepto = $ctacont = '';
                    $parafiscal = $_POST['piti'];
                    $nitparafiscal = $_POST['iti'];
                    $nomparafiscal = buscatercero($_POST['iti']);
                    //Cuenta debito Inst tecnicos
                    $ctacont = cuentascontablesccpet::cuentadebito_tipomovccpet_2021($parafiscal, $row[21], $row[18], $_POST['fechafi'], '2', 'N/A');
                    $nresul = buscacuenta($ctacont);
                    if ($ctacont == 'Sin Cuenta') {
                        $estilo = 'background-color: yellow;';
                    } else {
                        $estilo = "";
                    }
                    echo "
						<tr class='$iter' style='$estilo'>
							<td>$con</td>
							<td>$ctacont</td>
							<td>$nresul</td>
							<td>$nitparafiscal</td>
							<td>$nomparafiscal</td>
							<td>$row[18]</td>
							<td>Aportes Inst t�cnicos Mes $meslnomina</td>
							<td style='text-align:right;font-size:10px;'>&nbsp;$" . number_format($row[14], 0) . "</td>
							<td style='text-align:right;font-size:10px;'>&nbsp;$" . number_format(0, 0) . "</td>
						</tr>";
                    $listacuentas[] = $ctacont;
                    $listanombrecuentas[] = $nresul;
                    $listaterceros[] = $nitparafiscal;
                    $listanombreterceros[] = $nomparafiscal;
                    $listaccs[] = $row[18];
                    $listadetalles[] = "Aportes Inst t�cnicos Mes $meslnomina";
                    $listadebitos[] = $row[14];
                    $listacreditos[] = 0;
                    $listatipo[] = "P4<->DB";
                    $aux = $iter;
                    $iter = $iter2;
                    $iter2 = $aux;
                    $con += 1;
                    //Cuenta credito Inst tecnicos
                    $ctaconcepto = cuentascontablesccpet::cuentacredito_tipomovccpet_2021($parafiscal, $row[21], $row[18], $_POST['fechafi'], '2', 'N/A');
                    $nresul = buscacuenta($ctaconcepto);
                    if ($ctaconcepto === 'Sin Cuenta') {
                        $estilo = 'background-color: yellow;';
                    } else {
                        $estilo = "";
                    }
                    echo "
						<tr class='$iter' style='$estilo'>
							<td>$con</td>
							<td>$ctaconcepto</td>
							<td>$nresul</td>
							<td>$nitparafiscal</td>
							<td>$nomparafiscal</td>
							<td>$row[18]</td>
							<td>Aportes Inst t�cnicos Mes $meslnomina</td>
							<td style='text-align:right;font-size:10px;'>&nbsp;$" . number_format(0, 0) . "</td>
							<td style='text-align:right;font-size:10px;'>&nbsp;$" . number_format($row[14], 0) . "</td>
						</tr>";
                    $listacuentas[] = $ctaconcepto;
                    $listanombrecuentas[] = $nresul;
                    $listaterceros[] = $nitparafiscal;
                    $listanombreterceros[] = $nomparafiscal;
                    $listaccs[] = $row[18];
                    $listadetalles[] = "Aportes Inst t�cnicos Mes $meslnomina";
                    $listadebitos[] = 0;
                    $listacreditos[] = $row[14];
                    $listatipo[] = "P4<->CR";
                    $aux = $iter;
                    $iter = $iter2;
                    $iter2 = $aux;
                    $con += 1;
                }
                if ($row[15] != 0)//ESAP
                {
                    $ctaconcepto = $ctacont = '';
                    $parafiscal = $_POST['pesap'];
                    $nitparafiscal = $_POST['esap'];
                    $nomparafiscal = buscatercero($_POST['esap']);
                    //Cuenta debito ESAP
                    $ctacont = cuentascontablesccpet::cuentadebito_tipomovccpet_2021($parafiscal, $row[21], $row[18], $_POST['fechafi'], '2', 'N/A');
                    $nresul = buscacuenta($ctacont);
                    if ($ctacont == 'Sin Cuenta') {
                        $estilo = 'background-color: yellow;';
                    } else {
                        $estilo = "";
                    }
                    echo "
						<tr class='$iter' style='$estilo'>
							<td>$con</td>
							<td>$ctacont</td>
							<td>$nresul</td>
							<td>$nitparafiscal</td>
							<td>$nomparafiscal</td>
							<td>$row[18]</td>
							<td>Aportes ESAP Mes $meslnomina</td>
							<td style='text-align:right;font-size:10px;'>&nbsp;$" . number_format($row[15], 0) . "</td>
							<td style='text-align:right;font-size:10px;'>&nbsp;$" . number_format(0, 0) . "</td>
						</tr>";
                    $listacuentas[] = $ctacont;
                    $listanombrecuentas[] = $nresul;
                    $listaterceros[] = $nitparafiscal;
                    $listanombreterceros[] = $nomparafiscal;
                    $listaccs[] = $row[18];
                    $listadetalles[] = "Aportes ESAP Mes $meslnomina";
                    $listadebitos[] = $row[15];
                    $listacreditos[] = 0;
                    $listatipo[] = "P5<->DB";
                    $aux = $iter;
                    $iter = $iter2;
                    $iter2 = $aux;
                    $con += 1;
                    //Cuenta credito Inst tecnicos
                    $ctaconcepto = cuentascontablesccpet::cuentacredito_tipomovccpet_2021($parafiscal, $row[21], $row[18], $_POST['fechafi'], '2', 'N/A');
                    $nresul = buscacuenta($ctaconcepto);
                    if ($ctaconcepto === 'Sin Cuenta') {
                        $estilo = 'background-color: yellow;';
                    } else {
                        $estilo = "";
                    }
                    echo "
						<tr class='$iter' style='$estilo'>
							<td>$con</td>
							<td>$ctaconcepto</td>
							<td>$nresul</td>
							<td>$nitparafiscal</td>
							<td>$nomparafiscal</td>
							<td>$row[18]</td>
							<td>Aportes ESAP Mes $meslnomina</td>
							<td style='text-align:right;font-size:10px;'>&nbsp;$" . number_format(0, 0) . "</td>
							<td style='text-align:right;font-size:10px;'>&nbsp;$" . number_format($row[15], 0) . "</td>
						</tr>";
                    $listacuentas[] = $ctaconcepto;
                    $listanombrecuentas[] = $nresul;
                    $listaterceros[] = $nitparafiscal;
                    $listanombreterceros[] = $nomparafiscal;
                    $listaccs[] = $row[18];
                    $listadetalles[] = "Aportes ESAP Mes $meslnomina";
                    $listadebitos[] = 0;
                    $listacreditos[] = $row[15];
                    $listatipo[] = "P5<->CR";
                    $aux = $iter;
                    $iter = $iter2;
                    $iter2 = $aux;
                    $con += 1;
                }
                if ($row[20] != 0)//Retenciones
                {
                    $ctaconcepto = $ctacont = '';
                    $sqlrd1 = "SELECT T1.valor,T2.tiporetencion FROM humnominaretenemp T1, hum_retencionesfun T2 WHERE T1.id_nom='" . $_POST['idliq'] . "' AND T1.cedulanit='$row[0]' AND T1.id=T2.id AND T1.tipo_des='RE'";
                    $respd1 = mysqli_query($linkbd, $sqlrd1);
                    while ($rowd1 = mysqli_fetch_row($respd1)) {
                        $sqlcodi = "SELECT T2.conceptoingreso,T1.nombre FROM tesoretenciones T1,tesoretenciones_det T2 WHERE T1.id=T2.codigo AND T1.id='$rowd1[1]'";
                        $rescodi = mysqli_query($linkbd, $sqlcodi);
                        $rowcodi = mysqli_fetch_row($rescodi);
                        $sqlrcu = "SELECT DISTINCT cuenta, debito, credito FROM conceptoscontables_det WHERE modulo='4' AND tipo='RI' AND cc='$row[18]' AND tipocuenta='N' AND codigo='$rowcodi[0]' AND fechainicial=(SELECT MAX(fechainicial) FROM conceptoscontables_det WHERE fechainicial <=  '" . $_POST['fechafi'] . "' AND modulo='4' AND tipo='RI' AND cc='$row[18]' AND tipocuenta='N' AND codigo='$rowcodi[0]')  ORDER BY credito";
                        $respcu = mysqli_query($linkbd, $sqlrcu);
                        while ($rowcu = mysqli_fetch_row($respcu)) {
                            $ctaconcepto = $rowcu[0];
                            $docbenefi = $rowcu[1];
                            $nomdescu = ucwords(strtolower($rowcu[0]));
                        }
                        //Cuenta debito otras deducciones
                        $ctacont = cuentascontablesccpet::cuentacredito_tipomovccpet_2021($rowd1[2], $row[21], $row[18], $_POST['fechafi'], '1', '');
                        $nresul = buscacuenta($ctacont);
                        if ($ctacont == 'Sin Cuenta') {
                            $estilo = 'background-color: yellow;';
                        } else {
                            $estilo = "";
                        }
                        echo "
							<tr class='$iter' style='$estilo'>
								<td>$con</td>
								<td>$ctacont</td>
								<td>$nresul</td>
								<td>$row[0]</td>
								<td>$empleado</td>
								<td>$row[18]</td>
								<td>Retenci�n $rowcodi[1] Mes $meslnomina</td>
								<td style='text-align:right;font-size:10px;'>&nbsp;$" . number_format($rowd1[0], 0) . "</td>
								<td style='text-align:right;font-size:10px;'>&nbsp;$" . number_format(0, 0) . "</td>
							</tr>";
                        $listacuentas[] = $ctacont;
                        $listanombrecuentas[] = $nresul;
                        $listaterceros[] = $row[0];
                        $listanombreterceros[] = $empleado;
                        $listaccs[] = $row[18];
                        $listadetalles[] = "Retencion $rowcodi[1] Mes $meslnomina";
                        $listadebitos[] = $rowd1[0];
                        $listacreditos[] = 0;
                        $listatipo[] = "RE<->DB";
                        $aux = $iter;
                        $iter = $iter2;
                        $iter2 = $aux;
                        $con += 1;
                        //Cuenta credito otras deducciones
                        $nresul = buscacuenta($ctaconcepto);
                        if ($ctaconcepto === 'Sin Cuenta') {
                            $estilo = 'background-color: yellow;';
                        } else {
                            $estilo = "";
                        }
                        echo "
							<tr class='$iter' style='$estilo'>
								<td>$con</td>
								<td>$ctaconcepto</td>
								<td>$nresul</td>
								<td>$row[0]</td>
								<td>$empleado</td>
								<td>$row[18]</td>
								<td>Retenci�n $rowcodi[1] Mes $meslnomina</td>
								<td style='text-align:right;font-size:10px;'>&nbsp;$" . number_format(0, 0) . "</td>
								<td style='text-align:right;font-size:10px;'>&nbsp;$" . number_format($rowd1[0], 0) . "</td>
							</tr>";
                        $listacuentas[] = $ctaconcepto;
                        $listanombrecuentas[] = $nresul;
                        $listaterceros[] = $row[0];
                        $listanombreterceros[] = $empleado;
                        $listaccs[] = $row[18];
                        $listadetalles[] = "Retenci�n $rowcodi[1] Mes $meslnomina";
                        $listadebitos[] = 0;
                        $listacreditos[] = $rowd1[0];
                        $listatipo[] = "RE<->CR";
                        $aux = $iter;
                        $iter = $iter2;
                        $iter2 = $aux;
                        $con += 1;
                    }
                }
            }
            echo "
					<tr class='titulos2'>
						<td colspan='4'></td>
						<td colspan='2' style='text-align:right;'>Diferencia:</td>
						<td> $" . number_format(array_sum($listadebitos) - array_sum($listacreditos), 0, ',', '.') . "</td>
						<td style='text-align:right;'>$" . number_format(array_sum($listadebitos), 0, ',', '.') . "</td>
						<td style='text-align:right;'>$" . number_format(array_sum($listacreditos), 0, ',', '.') . "</td>
					</tr>
				</table>
				<script>document.getElementById('divcarga').style.display='none';</script>";
            ?>
        </div>
        <?php
        if ($_POST['oculto'] == 2) {
            $id = $_POST['idliq'];
            $sqlr = "DELETE FROM comprobante_cab WHERE numerotipo='$id' AND tipo_comp='4'";
            mysqli_query($linkbd, $sqlr);
            $sqlr = "DELETE FROM comprobante_det WHERE numerotipo='$id' AND tipo_comp='4'";
            mysqli_query($linkbd, $sqlr);
            ereg("([0-9]{1,2})/([0-9]{1,2})/([0-9]{2,4})", $_POST['fecha'], $fecha);
            $fechaf = $fecha[3] . "-" . $fecha[2] . "-" . $fecha[1];
            $lastday = mktime(0, 0, 0, $_POST['periodo'], 1, $_POST['vigenomi']);
            if ($_POST['causacion'] != '2') {
                $descripgen = "CAUSACION $primanom MES $meslnomina";
            } else {
                $descripgen = "ESTE DOCUMENTO NO REQUIERE CAUSACION CONTABLE";
            }
            $sqlr = "INSERT INTO comprobante_cab (numerotipo,tipo_comp,fecha,concepto,total,total_debito,total_credito,diferencia,estado) VALUES ($id,4,'$fechaf','$descripgen',0,0,0,0,'1')";
            mysqli_query($linkbd, $sqlr);
            if ($_POST['causacion'] != '2') {
                for ($x = 0; $x < count($listacuentas); $x++) {
                    $sqlr = "INSERT INTO comprobante_det (id_comp,cuenta,tercero,centrocosto,detalle,cheque,valdebito,valcredito,estado,vigencia) VALUES ('4 $id','$listacuentas[$x]','$listaterceros[$x]','$listaccs[$x]','" . utf8_encode($listadetalles[$x]) . "','','$listadebitos[$x]','$listacreditos[$x]','1', '" . $_POST['vigenomi'] . "')";
                    mysqli_query($linkbd, $sqlr);
                }
            }
        }
        ?>
    </form>
</body>

</html>
