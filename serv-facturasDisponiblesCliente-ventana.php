<?php 
	require "comun.inc";
	require "funciones.inc";
    require "funcionesSP.inc.php";
	session_start();
	$linkbd = conectar_v7();	
	cargarcodigopag($_GET['codpag'],$_SESSION["nivel"]);
	header("Cache-control: private"); // Arregla IE 6
	date_default_timezone_set("America/Bogota");
?>
<!DOCTYPE >
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="es">
	<head>
	 	<meta http-equiv="Content-Type" content="text/html" charset="iso-8859-1"/>
        <meta http-equiv="X-UA-Compatible" content="IE=9"/>
		<title>:: IDEAL 10 - Servicios P&uacute;bilicos</title>
        <link href="css/css2.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="css/calendario.js"></script>
		<script>
			var anterior;
			function ponprefijo(factura,valor,corte)
			{
				parent.document.form2.numeroFactura.value = factura;
				parent.document.form2.numeroFactura.focus();
                parent.document.form2.valor.value = valor;
                parent.document.form2.corte.value = corte;
				parent.despliegamodal2("hidden");	
			} 
		</script> 
		<?php titlepag();?>
	</head>
	<body>
		<?php
		if(!$_POST['id_cliente'])
			$_POST['id_cliente'] = $_GET['cliente'];
			
		?>
	<form action="" method="post" enctype="multipart/form-data" name="form1">
		<table class="inicio">
			<tr>
				<td height="25" colspan="4" class="titulos" >Buscar Factura Activas para Cliente</td>
				<td class="cerrar"><a onClick="parent.despliegamodal2('hidden');">&nbsp;Cerrar</a></td>
			</tr>

			<tr>
				<td class="saludo1" >:&middot; C&oacute;digo:</td>

				<td colspan="3">
                    <input name="numero" type="text" size="30">
					<input type="hidden" name="id_cliente"  id="id_cliente" value="<?php echo $_POST['id_cliente']?>">
					<input type="hidden" name="pos" id="pos" value="<?php echo $_POST['pos']?>">
					<input type="hidden" name="oculto" id="oculto" value="1" >
					<input type="submit" name="Submit" value="Buscar">
				</td>
			</tr>

			<tr>
				<td colspan="4" align="center">&nbsp;</td>
			</tr>
		</table>

		<div class="subpantalla" style="height:73.5%; width:99.6%; overflow-x:hidden;">
			<table class="inicio">
				<tr >
					<td height="25" colspan="6" class="titulos" >Resultados Busqueda </td>
				</tr>
				<tr >
					<td width="7%" style="text-align: center;" class="titulos2" >ID Cliente</td>
					<td style="text-align: center;" class="titulos2">Cod Usuario</td>
					<td style="text-align: center;" class="titulos2">Corte</td>	  
					<td style="text-align: center;" class="titulos2">Numero Factura</td>
                    <td class="titulos2">Nombre</td>
                    <td style="text-align: center;" class="titulos2">Valor</td>
				</tr>
				<?php
				$vigusu=vigencia_usuarios($_SESSION['cedulausu']); 
				$oculto=$_POST['oculto'];
				//echo $oculto;
				//if($oculto!="")
				{
					$link = conectar_v7();

					$cond = " AND  (D.numero_facturacion LIKE'%".$_POST['numero']."%')";
					
					$maxVersion = ultimaVersionIngresosCCPET();

                    $sqlr = "SELECT D.id, D.id_cliente, D.id_corte, D.numero_facturacion, D.estado_pago, C.id_tercero, C.cod_usuario FROM srvcortes_detalle AS D INNER JOIN srvclientes AS C ON D.id_cliente = C.id WHERE D.id_cliente = '$_POST[id_cliente]' AND D.estado_pago = 'S' $cond";
                    $resp = mysqli_query($linkbd,$sqlr);
                    $_POST['numtop'] = mysqli_num_rows($resp);

					$sqlr = "SELECT D.id, D.id_cliente, D.id_corte, D.numero_facturacion, D.estado_pago, C.id_tercero, C.cod_usuario FROM srvcortes_detalle AS D INNER JOIN srvclientes AS C ON D.id_cliente = C.id WHERE D.id_cliente = '$_POST[id_cliente]' AND D.estado_pago = 'S' $cond";
					
					$resp = mysqli_query($link,$sqlr);			

					$co = 'saludo1a';
					$co2 = 'saludo2';	
					
					while ($row = mysqli_fetch_row($resp)) 
					{
                        $nombre = encuentraNombreTerceroConIdCliente($row[1]);

                        $sqlValor = "SELECT SUM(credito), SUM(debito) FROM srvdetalles_facturacion WHERE corte = '$row[2]' AND numero_facturacion = '$row[3]' AND id_cliente = '$_POST[id_cliente]' AND tipo_movimiento = '101' ";
                        $resValor = mysqli_query($linkbd,$sqlValor);
                        $rowValor = mysqli_fetch_row($resValor);

                        $total = $rowValor[0] - $rowValor[1];
						echo 
						"<tr class='$co' onMouseOver=\"anterior=this.style.backgroundColor;this.style.backgroundColor='#40b3ff';\" onMouseOut=\"this.style.backgroundColor=anterior\" "; 
						
					    echo" onClick=\"javascript:ponprefijo('$row[3]', '$total', '$row[2]')\"";
					 
						echo"
                            <td></td>
                            <td style='text-align: center;'>$row[1]</td>
                            <td style='text-align: center;'>$row[6]</td>
                            <td style='text-align: center;'>$row[2]</td>
                            <td style='text-align: center;'>$row[3]</td>
                            <td>$nombre</td>
                            <td style='text-align: center;'>$total</td>
						</tr> ";
						
						$aux=$co;
						$co=$co2;
						$co2=$aux;
					}
                    if (@$_POST['numtop']==0)
						{
							echo "
                                <table class='inicio'>
                                    <tr>
                                        <td class='saludo1' style='text-align:center;width:100%'><img src='imagenes\alert.png' style='width:25px'>No hay facturas pendientes para este cliente<img src='imagenes\alert.png' style='width:25px'></td>
                                    </tr>
                                </table>";
						}
					$_POST['oculto']="";
				}
				?>
			</table>
		</div>
        <input type="hidden" name="numtop" id="numtop" value="<?php echo @ $_POST['numtop'];?>" />
	</form>
</body>
</html> 
