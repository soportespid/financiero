<?php
	require_once 'PHPExcel/Classes/PHPExcel.php';
    include '/PHPExcel/Classes/PHPExcel/IOFactory.php';
	require "comun.inc";
	require "funciones.inc";
    require "Librerias/core/Helpers.php";
    ini_set('max_execution_time',99999999);
    header("Content-type: application/json");
	session_start();
	$linkbd=conectar_v7();

	$servicio = $_POST["servicio"];

	$objPHPExcel = new PHPExcel();
	
	$objPHPExcel->getProperties()
		->setCreator("IDEAL10")
		->setLastModifiedBy("IDEAL10")
		->setTitle("Plantilla cargue indicadores")
		->setSubject("")
		->setDescription("")
		->setKeywords("")
		->setCategory("Planeación estrategica");
	$objPHPExcel->getActiveSheet()->mergeCells('A1:H1');
	$objPHPExcel->setActiveSheetIndex(0)->setCellValue('A1', "Plantilla indicadores de resultado");
	$objFont=$objPHPExcel->getActiveSheet()->getStyle('A1')->getFont();
	$objFont->setName('Courier New');
	$objFont->setSize(15);
	$objFont->setBold(true);
	$objFont->setUnderline(PHPExcel_Style_Font::UNDERLINE_SINGLE);
	$objFont->getColor()->setARGB( PHPExcel_Style_Color::COLOR_BLACK);
	$objAlign=$objPHPExcel->getActiveSheet()->getStyle('A1')->getAlignment(); 
	$objAlign->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
	$objAlign->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
	$objPHPExcel-> getActiveSheet ()
		-> getStyle ("A2:H2")
		-> getFill ()
		-> setFillType (PHPExcel_Style_Fill :: FILL_SOLID)
		-> getStartColor ()
		-> setRGB ('A6E5F3');
	$objPHPExcel-> getActiveSheet ()
		-> getStyle ("A1")	
		-> getFill ()
		-> setFillType (PHPExcel_Style_Fill :: FILL_SOLID)
		-> getStartColor ()
		-> setRGB ('A6E5F3');
	$borders = array(
		'borders' => array(
			'allborders' => array(
				'style' => PHPExcel_Style_Border::BORDER_THIN,
				'color' => array('argb' => 'FF000000'),
			)
		),
	);

	$objPHPExcel->getActiveSheet()->getStyle('A2:H2')->applyFromArray($borders);
	$objPHPExcel->setActiveSheetIndex(0)
    ->setCellValue('A2', 'Código indicador')
    ->setCellValue('B2', 'Nombre indicador')
    ->setCellValue('C2', 'Unidad de medida')
    ->setCellValue('D2', 'Fuente')
    ->setCellValue('E2', 'Dato númerico')
    ->setCellValue('F2', 'Año-mes')
    ->setCellValue('G2', 'PND')
    ->setCellValue('H2', 'Transformación PND');

	$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth("20");
	$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth("40");
	$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth("40");
	$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth("40");
	$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth("20");
	$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth("20");
	$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth("20");
	$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth("40");
	$objPHPExcel->getActiveSheet()->setTitle('Indicadores');

    header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
	header('Content-Disposition: attachment;filename="plantilla_indicadores_resultados.xlsx"');
	header('Cache-Control: max-age=0');
	$objWriter=PHPExcel_IOFactory::createWriter($objPHPExcel,'Excel2007');
	$objWriter->save('php://output');
	exit;
?>