<?php
	header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=utf8");
	require "comun.inc";
	require "funciones.inc";
	require "funcionesSP.inc.php";
	session_start();
	$linkbd = conectar_v7();
	$linkbd -> set_charset("utf8"); 
	cargarcodigopag($_GET['codpag'],$_SESSION["nivel"]);
	
?>
<!DOCTYPE >
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="es">
	<head>
		<meta http-equiv="Content-type" content="text/html; charset=utf8"/>
		<meta http-equiv="X-UA-Compatible" content="IE=9"/>
		<title>:: IDEAL 10 - Servicios P&uacute;blicos</title>
		<link href="css/css2.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<link href="css/css4.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<link href="css/cssSP.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<link href="css/style.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css"/>
		<script src="sweetalert2/dist/sweetalert2.min.js"></script>
		<link rel="stylesheet" href="sweetalert2/dist/sweetalert2.min.css">
		<script type="text/javascript" src="css/programas.js"></script>
		
		<script>
			function despliegamodalm(_valor,_tip,mensa,pregunta)
			{
				document.getElementById("bgventanamodalm").style.visibility=_valor;
				if(_valor=="hidden"){document.getElementById('ventanam').src="";}
				else
				{
					switch(_tip)
					{
						case "1":	document.getElementById('ventanam').src="ventana-mensaje1.php?titulos="+mensa;break;
						case "2":	document.getElementById('ventanam').src="ventana-mensaje3.php?titulos="+mensa;break;
						case "3":	document.getElementById('ventanam').src="ventana-mensaje2.php?titulos="+mensa;break;
						case "4":	document.getElementById('ventanam').src="ventana-consulta1.php?titulos="+mensa+"&idresp="+pregunta;break;	
					}
				}
			}
			function funcionmensaje()
			{
				var idban=document.getElementById('codban').value;
				document.location.href = "serv-servicios.php";
			}
		
			function guardar()
			{
				var validacion01 = document.getElementById('codban').value;
				var validacion02 = document.getElementById('nomban').value;
				var validacion03 = document.getElementById('tipounidad').value;
				var validacion04 = document.getElementById('cc').value;

				if (validacion01.trim()!='' && validacion02.trim()) 
				{
					if (validacion03 != -1) {
						if(validacion04 != -1) {
							Swal.fire({
								icon: 'question',
								title: 'Seguro que quieres guardar?',
								showDenyButton: true,
								confirmButtonText: 'Guardar',
								denyButtonText: 'Cancelar',
								}).then((result) => {
								if (result.isConfirmed) {
									document.form2.oculto.value='2';
									document.form2.submit();
								} else if (result.isDenied) {
									Swal.fire('Guardar cancelado', '', 'info')
								}
							})
						}
						else {
							Swal.fire({
								icon: 'warning',
								title: 'Falta información',
								text: 'Seleccione el centro de costos'
							})
						}
					}
					else {
						Swal.fire({
							icon: 'warning',
							title: 'Falta información',
							text: 'Seleccione el tipo de unidad'
						})
					}
				}
				else 
				{
					Swal.fire({
						icon: 'warning',
						title: 'Falta información',
						text: 'Digite el nombre del servicio'
					})
				}
			}
			function cambiocheck(tipo)
			{
				if (tipo == 'estado') {
					
					if(document.getElementById('myonoffswitch').value=='S') {
						document.getElementById('myonoffswitch').value='N';
					}
					else {
						document.getElementById('myonoffswitch').value='S';
					}
				}

				if (tipo == 'medidor') {

					if(document.getElementById('tarifaMedidor').value=='S') {
						document.getElementById('tarifaMedidor').value='N';
					}
					else {
						document.getElementById('tarifaMedidor').value='S';
					}
				}
				
			}
		</script>
		<?php titlepag();?>
	</head>
	<body>
		<IFRAME src="alertas.php" name="alertas" id="alertas" style="display:none"></IFRAME>
		<span id="todastablas2"></span>
		<table>
			<tr><script>barra_imagenes("serv");</script><?php cuadro_titulos();?></tr>
			<tr><?php menu_desplegable("serv");?></tr>
			<tr>
				<td colspan="3" class="cinta">
					<a href="serv-servicios.php" class="mgbt"><img src="imagenes/add.png"/></a>
					<a onclick="guardar()" class="mgbt"><img src="imagenes/guarda.png" title="Guardar"/></a>
					<a href="serv-serviciosbuscar.php" class="mgbt"><img src="imagenes/busca.png" title="Buscar"/></a>
					<a onclick="mypop=window.open('plan-agenda.php','','');mypop.focus()" class="mgbt"><img src="imagenes/agenda1.png" title="Agenda" /></a>
					<a onclick="mypop=window.open('serv-principal.php','','');mypop.focus();" class="mgbt"><img src="imagenes/nv.png" title="Nueva ventana"></a>
					<a onclick="mypop=window.open('<?php echo $url2; ?>','','');mypop.focus();" class="mgbt"><img src="imagenes/duplicar_pantalla.png" title="Duplicar pesta&ntilde;a"></a>
				</td>
			</tr>
		</table>

		<div id="bgventanamodalm" class="bgventanamodalm">
			<div id="ventanamodalm" class="ventanamodalm">
				<IFRAME src="" name="ventanam" marginWidth=0 marginHeight=0 frameBorder=0 id="ventanam" frameSpacing=0 style=" width:700px; height:130px; top:200; overflow:hidden;"></IFRAME>
			</div>
		</div>
		
		<form name="form2" method="post" action="">
			<?php 
				if(@ $_POST['oculto']=="")
				{
					$_POST['onoffswitch'] = "S";
					$_POST['tarifaMedidor']  = 'S';
					$_POST['codban']=selconsecutivo('srvservicios','id');
				}
			?>
			<table class="inicio ancho">
				<tr>
					<td class="titulos" colspan="7">.: Crear Servicios</td>
					<td class="cerrar" style="width:7%" onClick="location.href='serv-principal.php'">Cerrar</td>
				</tr>

				<tr>
					<td class="tamano01" style="width:3cm;">C&oacute;digo:</td>
					<td style="width:15%;">
						<input  type="text" name="codban" id="codban" onKeyUp="return tabular(event,this)" value="<?php echo @ $_POST['codban'];?>" style="width:100%;height:30px;text-align:center" readonly/>
					</td>

					<td class="tamano01" style="width:3cm;">Nombre:</td>
					<td>
						<input type="text" name="nomban" id="nomban" value="<?php echo @ $_POST['nomban'];?>" style="width:100%;height:30px;text-transform:uppercase"/>
					</td>

					<td class="tamano01" style="width: 3cm;">Estado:</td>
					<td style="width: 15%;">
						<div class="onoffswitch">
							<input type="checkbox" name="onoffswitch" class="onoffswitch-checkbox" id="myonoffswitch" value="<?php echo @ $_POST['onoffswitch'];?>" <?php if(@ $_POST['onoffswitch']=='S'){echo "checked";}?> onChange="cambiocheck('estado');"/>
							<label class="onoffswitch-label" for="myonoffswitch">
								<span class="onoffswitch-inner"></span>
								<span class="onoffswitch-switch"></span>
							</label>
						</div>
					</td>
				</tr>

				<tr>
					<td class="tamano01" style="width:3cm;">Unidad medida:</td>
					<td>
						<select name="tipounidad" id="tipounidad" style="width:100%" class="centrarSelect">
							<option class="aumentarTamaño" value="-1">:: Seleccione unidad de medida ::</option>  
							<?php
								$sqlr="SELECT id,nombre FROM srvtipo_unidad";
								$resp = mysqli_query($linkbd,$sqlr);

								while ($row = mysqli_fetch_row($resp))
								{
									if($_POST['tipounidad']==$row[1])
									{
										echo "<option class='aumentarTamaño' style='text-transform:uppercase;' value='$row[1]' SELECTED>$row[1]</option>";
									}
									else
									{
										echo "<option class='aumentarTamaño' style='text-transform:uppercase;'	 value='$row[1]'>$row[1]</option>no";
									}
								}
							?>
						</select>
					</td>

					<td class="tamano01" style="width:3cm;">Centro de Costos:</td>
					<td style="width: 3cm;">
						<select name="cc" id="cc" class="centrarSelect">
							<option class='aumentarTamaño' value="-1">:: Seleccione Cento de Costos ::</option>
							<?php
								$sqlCentroCosto = "SELECT * FROM centrocosto WHERE estado = 'S' AND entidad = 'S' ORDER BY id_cc";
								$resCentroCosto = mysqli_query($linkbd,$sqlCentroCosto);
								while ($rowCentroCosto = mysqli_fetch_row($resCentroCosto))
								{
									if(@ $_POST['cc'] == $rowCentroCosto[0])
									{
										echo "<option class='aumentarTamaño' value='$rowCentroCosto[0]' SELECTED>$rowCentroCosto[0] - $rowCentroCosto[1]</option>";
									}
									else{echo "<option class='aumentarTamaño' value='$rowCentroCosto[0]'>$rowCentroCosto[0] - $rowCentroCosto[1]</option>";}
								}
							?>
						</select>
					</td>			
					
					<td class="tamano01" style="width: 3cm;">Uso de medidor:</td>
					<td style="width: 15%;">
						<div class="onoffswitch">
							<input type="checkbox" name="tarifaMedidor" class="onoffswitch-checkbox" id="tarifaMedidor" value="<?php echo @ $_POST['tarifaMedidor'];?>" <?php if(@ $_POST['tarifaMedidor']=='S'){echo "checked";}?> onChange="cambiocheck('medidor');"/>
							<label class="onoffswitch-label" for="tarifaMedidor">
								<span class="onoffswitch-inner"></span>
								<span class="onoffswitch-switch"></span>
							</label>
						</div>
					</td>
				</tr>
			</table>
			<br>
			<table class="inicio ancho">
				<tr style="text-align: center;">
					<td class="titulos" colspan="2">.: Conceptos Contables Urbano</td>
					<td class="titulos" colspan="2">.: Conceptos Contables Rural</td>
				</tr>

				<tr>
					<td class="tamano01" style="width:5cm;">Cargo Fijo:</td>
					<td>
						<select name="cargoFijoU" id="cargoFijoU" style="width:100%;" class="centrarSelect">
							<option class="aumentarTamaño" value="-1">:: Seleccione Concepto ::</option>
							<?php
								$sqlr="SELECT * FROM conceptoscontables WHERE modulo='10' AND tipo='SS' ORDER BY codigo";
								$resp = mysqli_query($linkbd,$sqlr);
								while ($row =mysqli_fetch_row($resp))
								{
									if(@$_POST['cargoFijoU']==$row[0])
									{
										echo "<option class='aumentarTamaño' value='$row[0]' SELECTED>$row[0] - $row[1]</option>";
									}
									else
									{
										echo "<option class='aumentarTamaño' value='$row[0]'>$row[0] - $row[1]</option>";
									}
								}
							?>
						</select>
					</td>		
					
					<td class="tamano01" style="width:5cm;">Cargo Fijo:</td>
					<td>
						<select name="cargoFijoR" id="cargoFijoR" style="width:100%;" class="centrarSelect">
							<option class="aumentarTamaño" value="-1">:: Seleccione Concepto ::</option>
							<?php
								$sqlr="SELECT * FROM conceptoscontables WHERE modulo='10' AND tipo='SS' ORDER BY codigo";
								$resp = mysqli_query($linkbd,$sqlr);
								while ($row =mysqli_fetch_row($resp))
								{
									if(@$_POST['cargoFijoR']==$row[0])
									{
										echo "<option class='aumentarTamaño' value='$row[0]' SELECTED>$row[0] - $row[1]</option>";
									}
									else
									{
										echo "<option class='aumentarTamaño' value='$row[0]'>$row[0] - $row[1]</option>";
									}
								}
							?>
						</select>
					</td>		
				</tr>

				<tr>
					<td class="tamano01" style="width:5cm;">Consumo:</td>
					<td>
						<select name="consumoU" id="consumoU" style="width:100%;" class="centrarSelect">
							<option class="aumentarTamaño" value="-1">:: Seleccione Concepto ::</option>
							<?php
								$sqlr="SELECT * FROM conceptoscontables WHERE modulo='10' AND tipo='CL' ORDER BY codigo";
								$resp = mysqli_query($linkbd,$sqlr);
								while ($row =mysqli_fetch_row($resp))
								{
									if(@$_POST['consumoU']==$row[0])
									{
										echo "<option class='aumentarTamaño' value='$row[0]' SELECTED>$row[3] - $row[0] - $row[1]</option>";
									}
									else
									{
										echo "<option class='aumentarTamaño' value='$row[0]'>$row[3] - $row[0] - $row[1]</option>";
									}
								}
							?>
						</select>
					</td>		

					<td class="tamano01" style="width:5cm;">Consumo:</td>
					<td>
						<select name="consumoR" id="consumoR" style="width:100%;" class="centrarSelect">
							<option class="aumentarTamaño" value="-1">:: Seleccione Concepto ::</option>
							<?php
								$sqlr="SELECT * FROM conceptoscontables WHERE modulo='10' AND tipo='CL' ORDER BY codigo";
								$resp = mysqli_query($linkbd,$sqlr);
								while ($row =mysqli_fetch_row($resp))
								{
									if(@$_POST['consumoR']==$row[0])
									{
										echo "<option class='aumentarTamaño' value='$row[0]' SELECTED>$row[0] - $row[1]</option>";
									}
									else
									{
										echo "<option class='aumentarTamaño' value='$row[0]'>$row[0] - $row[1]</option>";
									}
								}
							?>
						</select>
					</td>		
				</tr>

				<tr>
					<td class="tamano01" style="width:5cm;">Subsidio Cargo Fijo:</td>
					<td>
						<select name="subsidioCFU" id="subsidioCFU" style="width:100%;" class="centrarSelect">
							<option class="aumentarTamaño" value="-1">:: Seleccione Concepto ::</option>
							<?php
								$sqlr="SELECT * FROM conceptoscontables WHERE modulo='10' AND tipo='SB' ORDER BY codigo";
								$resp = mysqli_query($linkbd,$sqlr);
								while ($row =mysqli_fetch_row($resp))
								{
									if(@$_POST['subsidioCFU']==$row[0])
									{
										echo "<option class='aumentarTamaño' value='$row[0]' SELECTED>$row[0] - $row[1]</option>";
									}
									else
									{
										echo "<option class='aumentarTamaño' value='$row[0]'>$row[0] - $row[1]</option>";
									}
								}
							?>
						</select>
					</td>	

					<td class="tamano01" style="width:5cm;">Subsidio Cargo Fijo:</td>
					<td>
						<select name="subsidioCFR" id="subsidioCFR" style="width:100%;" class="centrarSelect">
							<option class="aumentarTamaño" value="-1">:: Seleccione Concepto ::</option>
							<?php
								$sqlr="SELECT * FROM conceptoscontables WHERE modulo='10' AND tipo='SB' ORDER BY codigo";
								$resp = mysqli_query($linkbd,$sqlr);
								while ($row =mysqli_fetch_row($resp))
								{
									if(@$_POST['subsidioCFR']==$row[0])
									{
										echo "<option class='aumentarTamaño' value='$row[0]' SELECTED>$row[0] - $row[1]</option>";
									}
									else
									{
										echo "<option class='aumentarTamaño' value='$row[0]'>$row[0] - $row[1]</option>";
									}
								}
							?>
						</select>
					</td>
				</tr>

				<tr>
					<td class="tamano01" style="width:5cm;">Subsidio Consumo:</td>
					<td>
						<select name="subsidioCSU" id="subsidioCSU" style="width:100%;" class="centrarSelect">
							<option class="aumentarTamaño" value="-1">:: Seleccione Concepto ::</option>
							<?php
								$sqlr="SELECT * FROM conceptoscontables WHERE modulo='10' AND tipo='SB' ORDER BY codigo";
								$resp = mysqli_query($linkbd,$sqlr);
								while ($row =mysqli_fetch_row($resp))
								{
									if(@$_POST['subsidioCSU']==$row[0])
									{
										echo "<option class='aumentarTamaño' value='$row[0]' SELECTED>$row[0] - $row[1]</option>";
									}
									else
									{
										echo "<option class='aumentarTamaño' value='$row[0]'>$row[0] - $row[1]</option>";
									}
								}
							?>
						</select>
					</td>

					<td class="tamano01" style="width:5cm;">Subsidio Consumo:</td>
					<td>
						<select name="subsidioCSR" id="subsidioCSR" style="width:100%;" class="centrarSelect">
							<option class="aumentarTamaño" value="-1">:: Seleccione Concepto ::</option>
							<?php
								$sqlr="SELECT * FROM conceptoscontables WHERE modulo='10' AND tipo='SB' ORDER BY codigo";
								$resp = mysqli_query($linkbd,$sqlr);
								while ($row =mysqli_fetch_row($resp))
								{
									if(@$_POST['subsidioCSR']==$row[0])
									{
										echo "<option class='aumentarTamaño' value='$row[0]' SELECTED>$row[0] - $row[1]</option>";
									}
									else
									{
										echo "<option class='aumentarTamaño' value='$row[0]'>$row[0] - $row[1]</option>";
									}
								}
							?>
						</select>
					</td>
				</tr>

				<tr>
					<td class="tamano01" style="width:5cm;">Contribución:</td>
					<td>
						<select name="contribucionU" id="contribucionU" style="width:100%;" class="centrarSelect">
							<option class="aumentarTamaño" value="-1">:: Seleccione Concepto ::</option>
							<?php
								$sqlr="SELECT * FROM conceptoscontables WHERE modulo='10' AND tipo='SC' ORDER BY codigo";
								$resp = mysqli_query($linkbd,$sqlr);
								while ($row =mysqli_fetch_row($resp))
								{
									if(@$_POST['contribucionU']==$row[0])
									{
										echo "<option class='aumentarTamaño' value='$row[0]' SELECTED>$row[0] - $row[1]</option>";
									}
									else
									{
										echo "<option class='aumentarTamaño' value='$row[0]'>$row[0] - $row[1]</option>";
									}
								}
							?>
						</select>
					</td>

					<td class="tamano01" style="width:5cm;">Contribución:</td>
					<td>
						<select name="contribucionR" id="contribucionR" style="width:100%;" class="centrarSelect">
							<option class="aumentarTamaño" value="-1">:: Seleccione Concepto ::</option>
							<?php
								$sqlr="SELECT * FROM conceptoscontables WHERE modulo='10' AND tipo='SC' ORDER BY codigo";
								$resp = mysqli_query($linkbd,$sqlr);
								while ($row =mysqli_fetch_row($resp))
								{
									if(@$_POST['contribucionR']==$row[0])
									{
										echo "<option class='aumentarTamaño' value='$row[0]' SELECTED>$row[0] - $row[1]</option>";
									}
									else
									{
										echo "<option class='aumentarTamaño' value='$row[0]'>$row[0] - $row[1]</option>";
									}
								}
							?>
						</select>
					</td>
				</tr>
			</table>
			
			<input type="hidden" name="oculto" id="oculto" value="1"/>
			<?php 
				if(@ $_POST['oculto']=="2")
				{
					if (@ $_POST['onoffswitch']!='S')
					{
						$valest='N';
					}
					else 
					{
						$valest='S';
					}

					if (@$_POST['tarifaMedidor'] != 'S') {

						$tarifaMedidor = 'N';
					}
					else {

						$tarifaMedidor = 'S';
					}

					$_POST['codban'] = selconsecutivo('srvservicios','id');

					$sqlr="INSERT INTO srvservicios (nombre, unidad_medida, cc, cargo_fijo_u, cargo_fijo_r, consumo_u, consumo_r, subsidio_cf_u, subsidio_cf_r, subsidio_cs_u, subsidio_cs_r, contribucion_u, contribucion_r, estado, tarifa_medidor) VALUES ('$_POST[nomban]', '$_POST[tipounidad]', '$_POST[cc]', '$_POST[cargoFijoU]', '$_POST[cargoFijoR]', '$_POST[consumoU]', '$_POST[consumoR]', '$_POST[subsidioCFU]', '$_POST[subsidioCFR]', '$_POST[subsidioCSU]', '$_POST[subsidioCSR]', '$_POST[contribucionU]', '$_POST[contribucionR]', '$valest', '$tarifaMedidor')";
					if (mysqli_query($linkbd,$sqlr))
					{
						echo "
							<script>
								Swal.fire({
									icon: 'success',
									title: 'Guardado exitoso!',
									}).then((result) => {
									if (result.value) {
										document.location.href = 'serv-servicios.php';
									} 
								})
							</script>";
					}
					else 
					{
						echo"
							<script>
								Swal.fire('Error en el guardar', '', 'error');
							</script>";
					}
				}
			?>
		</form>
		<div id="bgventanamodal2">
			<div id="ventanamodal2">
				<IFRAME  src="" name="buster" marginWidth=0 marginHeight=0 frameBorder=0 id="ventana2" frameSpacing=0 style="left:500px; width:900px; height:500px; top:200;"></IFRAME>
			</div>
		</div>
	</body>
</html>