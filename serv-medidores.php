<?php
	header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=utf8");
	require "comun.inc";
	require "funciones.inc";
	require "funcionesSP.inc.php";
	session_start();
	$linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");
	cargarcodigopag($_GET['codpag'],$_SESSION["nivel"]);
	date_default_timezone_set("America/Bogota");
?>
<!DOCTYPE >
<html xml:lang="es">
	<head>
		<meta http-equiv="Content-type" content="text/html; charset=utf8"/>
		<meta http-equiv="X-UA-Compatible" content="IE=9"/>
		<title>:: IDEAL 10 - Servicios P&uacute;blicos</title>
		<link href="css/css2.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<link href="css/css4.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<link href="css/cssSP.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<script src="sweetalert2/dist/sweetalert2.min.js"></script>
		<link rel="stylesheet" href="sweetalert2/dist/sweetalert2.min.css">
		<script type="text/javascript" src="css/programas.js"></script>
		<script type="text/javascript" src="css/calendario.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<?php titlepag();?>

		<script>
			function guardar() {
				var validacion01 = document.getElementById('codban').value;
				var validacion02 = document.getElementById('serial').value;
				var validacion03 = document.getElementById('fecha').value;
				var validacion04 = document.getElementById('idServicio').value;

				if (validacion01.trim() != '') 
				{
					if (validacion02.trim() != '') 
					{
						if (validacion03 != '') 
						{
							if (validacion04 != '-1')
							{
								Swal.fire({
									icon: 'question',
									title: 'Seguro que quieres guardar?',
									showDenyButton: true,
									confirmButtonText: 'Guardar',
									denyButtonText: 'Cancelar',
									}).then((result) => {
									if (result.isConfirmed) {
										document.form2.oculto.value='2';
										document.form2.submit();
									} else if (result.isDenied) {
										Swal.fire('Guardar cancelado', '', 'info')
									}
								});
							}
							else
							{
								Swal.fire({
									icon: 'warning',
									title: 'Falta información',
									text: 'Seleccione un servicio'
								});

								document.form2.idServicio.focus();
								document.form2.idServicio.select();
							}
						}
						else 
						{
							Swal.fire({
								icon: 'warning',
								title: 'Falta información',
								text: 'La fecha esta vacia'
							});

							document.form2.fecha.focus();
							document.form2.fecha.select();
						}
					}
					else 
					{
						Swal.fire({
							icon: 'warning',
							title: 'Falta información',
							text: 'El serial esta vacio'
						});

						document.form2.serial.focus();
						document.form2.serial.select();
					}
				}
				else 
				{
					Swal.fire({
						icon: 'warning',
						title: 'Falta información',
						text: 'Codigo vació error en guardado'
					});
				}
			}

			function cambiocheck() {
				if(document.getElementById('myonoffswitch').value == 'S') {
					document.getElementById('myonoffswitch').value = 'N';
				}
				else {
					document.getElementById('myonoffswitch').value = 'S';
				}
			}
		</script>

		<style>
			
		</style>
		

	</head>

	<body>
		<IFRAME src="alertas.php" name="alertas" id="alertas" style="display:none"></IFRAME>
		<span id="todastablas2"></span>
		<table>
			<tr>
				<script>barra_imagenes("serv");</script>
				<?php cuadro_titulos(); ?>
			</tr>	 

			<tr>
				<?php 
					menu_desplegable("serv"); 	
				?>
			</tr>

			<tr>
				<td colspan="3" class="cinta">
					<a href="serv-medidores.php" class="mgbt"><img src="imagenes/add.png"/></a>

					<a onclick="guardar()" class="mgbt"><img src="imagenes/guarda.png" title="Guardar"/></a>

					<a href="serv-medidoresbuscar.php" class="mgbt"><img src="imagenes/busca.png" title="Buscar"/></a>

					<a onclick="mypop=window.open('plan-agenda.php','','');mypop.focus()" class="mgbt"><img src="imagenes/agenda1.png" title="Agenda" /></a>

					<a onclick="mypop=window.open('serv-principal.php','','');mypop.focus();" class="mgbt"><img src="imagenes/nv.png" title="Nueva ventana"></a>

					<a onclick="mypop=window.open('<?php echo $url2; ?>','','');mypop.focus();" class="mgbt"><img src="imagenes/duplicar_pantalla.png" title="Duplicar pesta&ntilde;a"></a>
				</td>
			</tr>
		</table>

		<div id="bgventanamodalm" class="bgventanamodalm">
			<div id="ventanamodalm" class="ventanamodalm">
				<IFRAME src="" name="ventanam" marginWidth=0 marginHeight=0 frameBorder=0 id="ventanam" frameSpacing=0 style=" width:700px; height:130px; top:200; overflow:hidden;"></IFRAME>
			</div>
		</div>

		<form name="form2" method="post" action="serv-medidores.php">
			<?php 
				if(@ $_POST['oculto'] == "")
				{
					$_POST['onoffswitch'] = "S";
					$_POST['codban'] = selconsecutivo('srvmedidores','id');
				}
			?>

			<table class="inicio grande">
				<tr>
					<td class="titulos" colspan="6">.: Ingresar Medidor</td>

					<td class="cerrar" style="width:7%" onClick="location.href='serv-principal.php'">Cerrar</td>
				</tr>

				<tr>
					<td class="tamano01" style="width:3cm;">C&oacute;digo:</td>

					<td style="width:15%;">
						<input type="text" name="codban" id="codban" onKeyUp="return tabular(event,this)" value="<?php echo @$_POST['codban'];?>" style="width:90%;height:30px;text-align:center;" readonly/>
					</td>

					<td class="tamano01" style="width:3cm;">Serial:</td>

					<td style="width: 15%;">
						<input type="text" name="serial" id="serial" onKeyUp="return tabular(event,this)" value="<?php echo @ $_POST['serial'];?>" style="width:98%;height:30px;"/>
					</td>

					<td class="tamano01" style="width:3cm;">Fabricante:</td>

					<td>
						<input type="text" name="fabricante" id="fabricante" onKeyUp="return tabular(event,this)" value="<?php echo @$_POST['fabricante'];?>" style="width:97%;height:30px;"/>
					</td>
				</tr>

				<tr>
					<td class="tamano01" style="width:3cm;">Marca:</td>

					<td style="width:15%;">
						<input type="text" name="marca" id="marca" onKeyUp="return tabular(event,this)" value="<?php echo @ $_POST['marca'];?>" style="width:90%;height:30px;"/>
					</td>

					<td class="tamano01" style="width: 3cm;">Servicio:</td>

					<td>
						<select name="idServicio" id="idServicio" style="width:98%" class="centrarSelect">
							<option class="aumentarTamaño" value="-1">Seleccione un servicio</option>  
							<?php
								$sqlr="SELECT id,nombre FROM srvservicios";
								$resp = mysqli_query($linkbd,$sqlr);

								while ($row = mysqli_fetch_row($resp))
								{
									if($_POST['idServicio']==$row[0])
									{
										echo "<option class='aumentarTamaño' style='text-transform:uppercase;' value='$row[0]' SELECTED>$row[0] - $row[1]</option>";
									}
									else
									{
										echo "<option class='aumentarTamaño' style='text-transform:uppercase;'	 value='$row[0]'>$row[0] - $row[1]</option>no";
									}
								}
							?>
						</select>
					</td>

					<td class="tamano01" style="width:3cm;">Fecha Activación:</td>

					<td style="width: 15%;">
						<input type="text" name="fecha" id="fecha" value="<?php echo @ $_POST['fecha']?>" maxlength="10" onKeyUp="return tabular(event,this)"  id="fc_1198971545" onKeyDown="mascara(this,'/',patron,true)" title="DD/MM/YYYY" style="height:30px;text-align:center;width:80%;" readonly/>

						<a href="#" onClick="displayCalendarFor('fecha');" title="Calendario">
								<img src="imagenes/calendario04.png" style="width:25px;">
						</a>
					</td>
				</tr>
			</table>

			<input type="hidden" name="oculto" id="oculto" value="1"/>

			<?php 
				if(@ $_POST['oculto'] == "2")
				{
					if (@$_POST['onoffswitch'] != 'S') {
						$valest = 'N';
					}
					else {
						$valest = 'S';
					}

					preg_match("/([0-9]{2})\/([0-9]{2})\/([0-9]{4})/", $_POST['fecha'],$f);
					$fecha = "$f[3]-$f[2]-$f[1]";
	
					$queryMedidores = "INSERT INTO srvmedidores (id, fabricante, marca, serial, id_servicio, fecha_activacion) VALUES ($_POST[codban], '$_POST[fabricante]', '$_POST[marca]', '$_POST[serial]', '$_POST[idServicio]', '$fecha')";
					
					if(mysqli_query($linkbd,$queryMedidores))
					{
						echo "
							<script>
								var x = window.location.href;

								Swal.fire({
									icon: 'success',
									title: 'Guardado exitoso!',
									}).then((result) => {
									if (result.value) {
										document.location.href = x;
									} 
								})
							</script>";
					}
					else
					{
						echo"
						<script>
							Swal.fire('Error en el guardar', '', 'error');
						</script>";
					}
				}
			?>
		</form>

		<div id="bgventanamodal2">
			<div id="ventanamodal2">
				<IFRAME  src="" name="buster" marginWidth=0 marginHeight=0 frameBorder=0 id="ventana2" frameSpacing=0 style="left:500px; width:900px; height:500px; top:200;"></IFRAME>
			</div>
		</div>
		
	</body>
</html>