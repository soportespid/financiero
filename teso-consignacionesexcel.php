<?php  
require_once 'PHPExcel/Classes/PHPExcel.php';
require "comun.inc";
require "funciones.inc";
session_start(); 
$objPHPExcel = new PHPExcel();

//----Propiedades----
$objPHPExcel->getProperties()
        ->setCreator("IDEAL 10")
        ->setLastModifiedBy("IDEAL 10")
        ->setTitle("Exportar Excel con PHP")
        ->setSubject("Documento de prueba")
        ->setDescription("Documento generado con PHPExcel")
        ->setKeywords("usuarios phpexcel")
        ->setCategory("reportes");

//----Cuerpo de Documento----
$objPHPExcel->getActiveSheet()->mergeCells('A1:E1');
$objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue('A1', 'Traslados - Consignaciones');

$objFont=$objPHPExcel->getActiveSheet()->getStyle('A1')->getFont();
$objFont->setName('Courier New'); 
$objFont->setSize(15); 
$objFont->setBold(true); 
$objFont->setUnderline(PHPExcel_Style_Font::UNDERLINE_SINGLE);
$objFont->getColor()->setARGB( PHPExcel_Style_Color::COLOR_BLACK);

$objAlign=$objPHPExcel->getActiveSheet()->getStyle('A1')->getAlignment(); 
$objAlign->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER); 
$objAlign->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER); 

$objPHPExcel-> getActiveSheet ()
-> getStyle ("A1")
-> getFill ()
-> setFillType (PHPExcel_Style_Fill :: FILL_SOLID)
-> getStartColor ()
-> setRGB ('C8C8C8');

$objPHPExcel-> getActiveSheet ()
-> getStyle ("A2:E2")
-> getFill ()
-> setFillType (PHPExcel_Style_Fill :: FILL_SOLID)
-> getStartColor ()
-> setRGB ('99CCFF');

$borders = array(
    'borders' => array(
        'allborders' => array(
            'style' => PHPExcel_Style_Border::BORDER_THIN,
        'color' => array('argb' => 'FF000000'),
        )
    ),
);
$objPHPExcel->getActiveSheet()->getStyle('A1:E1')->applyFromArray($borders);
$objPHPExcel->getActiveSheet()->getStyle('A2:E2')->applyFromArray($borders);

$objPHPExcel->setActiveSheetIndex(0)
->setCellValue('A2', 'No Consig.')
->setCellValue('B2', 'Fecha')
->setCellValue('C2', 'Concepto consignacion')
->setCellValue('D2', 'Valor')
->setCellValue('E2', 'Estado')
;

$i = 3;

for($x = 0; $x < count($_POST['nConsigE']); $x++ )
{
	$objPHPExcel->setActiveSheetIndex(0)
	->setCellValueExplicit ("A$i", $_POST['nConsigE'][$x], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
	->setCellValueExplicit ("B$i", $_POST['fechaE'][$x], PHPExcel_Cell_DataType :: TYPE_STRING)
	->setCellValueExplicit ("C$i", $_POST['conceptoE'][$x], PHPExcel_Cell_DataType :: TYPE_STRING)
	->setCellValueExplicit ("D$i", $_POST['valorE'][$x], PHPExcel_Cell_DataType :: TYPE_STRING)
	->setCellValueExplicit ("E$i", $_POST['estadoE'][$x], PHPExcel_Cell_DataType :: TYPE_STRING)
    ;
	$i++;
}

//----Propiedades de la hoja
$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true); 
$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true); 
$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true); 
$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true); 
$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true); 
$objPHPExcel->getActiveSheet()->setTitle('Consignaciones');
$objPHPExcel->setActiveSheetIndex(0);

//----Guardar documento----
header('Content-Type: application/vnd.ms-excel');
header('Content-Disposition: attachment;filename="Teso-Traslados-Consignaciones.xls"');
header('Cache-Control: max-age=0');
 
$objWriter=PHPExcel_IOFactory::createWriter($objPHPExcel,'Excel5');
$objWriter->save('php://output');
exit;

?>