<?php
header("Cache-control: no-cache, no-store, must-revalidate");
header("Content-Type: text/html;charset=utf8");
require "comun.inc";
require "funciones.inc";
ini_set('max_execution_time', 99999999);
session_start();
$linkbd = conectar_v7();
$linkbd->set_charset("utf8");
cargarcodigopag($_GET['codpag'], $_SESSION["nivel"]);
date_default_timezone_set("America/Bogota");
?>
<!DOCTYPE>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="es">

<head>
	<meta http-equiv="Content-type" content="text/html; charset=iso-8859-1" />
	<meta http-equiv="X-UA-Compatible" content="IE=9" />
	<title>:: IDEAL 10 - Servicios P&uacute;blicos</title>
	<link href="css/css2.css?<?php echo date('d_m_Y_h_i_s'); ?>" rel="stylesheet" type="text/css" />
	<link href="css/css3.css?<?php echo date('d_m_Y_h_i_s'); ?>" rel="stylesheet" type="text/css" />
	<link href="css/css4.css?<?php echo date('d_m_Y_h_i_s'); ?>" rel="stylesheet" type="text/css" />
	<link href="css/cssSP.css?<?php echo date('d_m_Y_h_i_s'); ?>" rel="stylesheet" type="text/css" />
	<script type="text/javascript" src="css/programas.js?<?php echo date('d_m_Y_h_i_s'); ?>"></script>
	<script type="text/javascript" src="css/calendario.js?<?php echo date('d_m_Y_h_i_s'); ?>"></script>
	<script type="text/javascript" src="jquery-1.11.0.min.js"></script>
	<style>
		.orderby {
			text-align-last: center;
		}

		option {
			font-size: 15px;
		}
	</style>
	<script>
		<?php
		$sqlfactura = "SELECT nombre FROM srvarchivospdf WHERE id='1' ";
		$resfactura = mysqli_query($linkbd, $sqlfactura);
		$rowfactura = mysqli_fetch_row($resfactura);
		echo "
				function pdf()
				{
					document.form2.action='" . $rowfactura[0] . "';
					document.form2.target='_BLANK';
					document.form2.submit(); 
					document.form2.action='';
					document.form2.target='';
				}";
		?>

		function despliegamodalm(_valor, _tip, mensa, pregunta) {
			document.getElementById("bgventanamodalm").style.visibility = _valor;
			if (_valor == "hidden") {
				document.getElementById('ventanam').src = "";
			} else {
				switch (_tip) {
					case "1":
						document.getElementById('ventanam').src = "ventana-mensaje1.php?titulos=" + mensa;
						break;
					case "2":
						document.getElementById('ventanam').src = "ventana-mensaje3.php?titulos=" + mensa;
						break;
					case "3":
						document.getElementById('ventanam').src = "ventana-mensaje2.php?titulos=" + mensa;
						break;
					case "4":
						document.getElementById('ventanam').src = "ventana-consulta1.php?titulos=" + mensa + "&idresp=" + pregunta;
						break;
				}
			}
		}

		function funcionmensaje() {
			document.location.href = "serv-imprimirFacturacion.php";
		}

		function generarfacturas() {
			document.form2.oculto.value = '2';
			document.form2.submit();
		}

		function actualizar() {
			document.form2.submit();
		}

		function callprogress(vValor) {
			document.getElementById("getprogress").innerHTML = vValor;
			document.getElementById("getProgressBarFill").innerHTML = '<div class="ProgressBarFill" style="width: ' + vValor + '%;"></div>';
			document.getElementById("titulog1").style.display = 'block';
			document.getElementById("progreso").style.display = 'block';
			document.getElementById("getProgressBarFill").style.display = 'block';
		}

		function atras() {
			var link = document.referrer;

			location.href = link;
		}

		function callprogress(vValor)
		{
			document.getElementById("getprogress").innerHTML = vValor;
			document.getElementById("getProgressBarFill").innerHTML = '<div class="ProgressBarFill" style="width: '+vValor+'%;"></div>';
			document.getElementById("titulog1").style.display='block';
			document.getElementById("progreso").style.display='block';
			document.getElementById("getProgressBarFill").style.display='block';
		}       
	</script><?php echo $_SESSION['FACTURASP']; ?>
	<?php titlepag(); ?>
</head>

<body>
	<IFRAME src="alertas.php" name="alertas" id="alertas" style="display:none"></IFRAME>
	<span id="todastablas2"></span>
	<table>
		<tr>
			<script>
				barra_imagenes("serv");
			</script><?php cuadro_titulos(); ?>
		</tr>
		<tr><?php menu_desplegable("serv"); ?></tr>
		<tr>
			<td colspan="3" class="cinta">
				<img src="imagenes/add.png" title="Nuevo" onClick="location.href='serv-imprimirFacturacion.php'" class="mgbt" />
				<img src="imagenes/guardad.png" title="Guardar" class="mgbt" />
				<img src="imagenes/buscad.png" title="Buscar" class="mgbt" />
				<img src="imagenes/nv.png" title="Nueva Ventana" onClick="<?php echo paginasnuevas("serv"); ?>" class="mgbt">
				<img src='imagenes/iratras.png' title="Men&uacute; Nomina" onclick="atras();" class='mgbt'>
			</td>
		</tr>
	</table>
	<div id="bgventanamodalm" class="bgventanamodalm">
		<div id="ventanamodalm" class="ventanamodalm">
			<IFRAME src="" name="ventanam" marginWidth=0 marginHeight=0 frameBorder=0 id="ventanam" frameSpacing=0 style=" width:700px; height:130px; top:200; overflow:hidden;"></IFRAME>
		</div>
	</div>
	<form name="form2" method="post" action="">
		<table class="inicio" align="center">
			<tr>
				<td class="titulos" colspan="11">.: Facturaci&oacute;n Servicios</td>
				<td class="cerrar" style="width: 5%;"><a href="serv-principal.php">Cerrar</a></td>
			</tr>
			<tr>
				<td class="tamano01" style='width:3cm;'>Generar Corte:</td>
				<td style='width:10%;'>
					<select name="idCorte" id="idCorte" class="centrarSelect" style="width:98%;height:30px;align-content:center;" onchange="actualizar()">
						<option class='aumentarTamaño' value="-1">Seleccione Corte</option>
						<?php
						$sql = "SET lc_time_names = 'es_ES'";
						mysqli_query($linkbd, $sql);
						$sqlCorte = "SELECT numero_corte,fecha_inicial,fecha_final,fecha_limite_pago,fecha_impresion,servicio, UPPER(MONTHNAME(fecha_inicial)), UPPER(MONTHNAME(fecha_final)), YEAR(fecha_inicial), YEAR(fecha_final) FROM srvcortes WHERE numero_corte > 0 ORDER BY numero_corte DESC";
						$respCorte = mysqli_query($linkbd, $sqlCorte);
						while ($rowCorte = mysqli_fetch_row($respCorte)) {
							$sqlServicio = "SELECT nombre FROM srvservicios WHERE id = '$rowCorte[5]' ";
							$respServicio = mysqli_query($linkbd, $sqlServicio);
							$rowServicio = mysqli_fetch_row($respServicio);
							$sqlCorteDetalles = "SELECT MIN(numero_facturacion),MAX(numero_facturacion) FROM srvcortes_detalle WHERE id_corte = '$rowCorte[0]' AND estado_pago <> 'N'";
							$respCorteDetalles = mysqli_query($linkbd, $sqlCorteDetalles);
							$rowCorteDetalles = mysqli_fetch_row($respCorteDetalles);
							if ($rowServicio[0] == 0) {
								$rowServicio[0] = 'TODOS';
							}
							if ($_POST['idCorte'] == $rowCorte[0]) {
								echo "<option value='$rowCorte[0]' SELECTED>$rowCorte[0]: $rowCorte[6] $rowCorte[8] - $rowCorte[7] $rowCorte[9]</option>";
							} else {
								echo "<option value='$rowCorte[0]'>$rowCorte[0]: $rowCorte[6] $rowCorte[8] - $rowCorte[7] $rowCorte[9]</option>no";
							}
						}
						?>
					</select>
					<input type="hidden" name="idCorteaux" id="idCorteaux" value="<?php echo @$_POST['idCorteaux']; ?>" </td>

				<td style="padding-bottom:0px"><em class="botonflecha" onClick="generarfacturas();">Buscar</em></td>

				<?php
					echo"
						<td>
							<div id='titulog1' style='display:none; float:left'></div>
							<div id='progreso' class='ProgressBar' style='display:none; float:left'>
								<div class='ProgressBarText'><span id='getprogress'></span>&nbsp;% </div>
								<div id='getProgressBarFill'></div>
							</div>
						</td>";
				?>
			</tr>
			<input type="hidden" name="fechalimite" id="fechalimite" style="text-align:center;height:30px;" value="<?php echo @$_POST['fechalimite']; ?>" />
			<?php
			if (@$_POST['oculto'] == "2") {

				$sqlDatosFactura = "SELECT fecha_impresion, YEAR(fecha_impresion) FROM srvcortes WHERE numero_corte = '$_POST[idCorte]'";
				$rowDatosFactura = mysqli_fetch_row(mysqli_query($linkbd, $sqlDatosFactura));

				$sqlCorteDetalle = "SELECT numero_facturacion, id_cliente FROM srvcortes_detalle WHERE id_corte = '$_POST[idCorte]' ORDER BY numero_facturacion ASC";
				$resCorteDetalle = mysqli_query($linkbd, $sqlCorteDetalle);
				$totalClientes = mysqli_num_rows($resCorteDetalle); //total Clientes
				$c = 0;

				while ($rowCorteDetalle = mysqli_fetch_row($resCorteDetalle)) {
					//tercero
					$sqlCliente = "SELECT id_tercero FROM srvclientes WHERE id = $rowCorteDetalle[1]";
					$rowCliente = mysqli_fetch_row(mysqli_query($linkbd, $sqlCliente));

					$sqlTercero = "SELECT cedulanit FROM terceros WHERE id_tercero = $rowCliente[0]";
					$rowTercero = mysqli_fetch_row(mysqli_query($linkbd, $sqlTercero));

					$sqlBorrarCab = "DELETE FROM comprobante_cab WHERE tipo_comp = '29' AND numerotipo = '$rowCorteDetalle[0]'";
					mysqli_query($linkbd, $sqlBorrarCab);
					$sqlBorrarDet = "DELETE FROM comprobante_det WHERE tipo_comp = '29' AND numerotipo = '$rowCorteDetalle[0]'";
					mysqli_query($linkbd, $sqlBorrarDet);
					

					$sqlCab = "INSERT INTO comprobante_cab (numerotipo, tipo_comp, fecha, concepto, total_debito, total_credito, estado) VALUES ('$rowCorteDetalle[0]', '29', '$rowDatosFactura[0]', 'Facturacion de la factura numero $rowCorteDetalle[0]', 0, 0, 1)";
					mysqli_query($linkbd, $sqlCab);

					$sqlFacturaUsuario = "SELECT id_servicio, id_tipo_cobro, credito, debito FROM srvdetalles_facturacion WHERE numero_facturacion = '$rowCorteDetalle[0]' AND tipo_movimiento = '101' ORDER BY id_servicio ASC, id_tipo_cobro ASC";
					$resFacturaUsuario = mysqli_query($linkbd, $sqlFacturaUsuario);
					while ($rowFacturaUsuario = mysqli_fetch_row($resFacturaUsuario)) {

						switch ($rowFacturaUsuario[1]) {

							//Cargo fijo
							case 1:
								$sqlServicio = "SELECT cargo_fijo_u, cc, nombre FROM srvservicios WHERE id = '$rowFacturaUsuario[0]' ";
								$rowServicio = mysqli_fetch_row(mysqli_query($linkbd, $sqlServicio));

								$concepto = concepto_cuentasn2($rowServicio[0], 'SS', 10, $rowServicio[1], $rowDatosFactura[0]);

								for($z=0; $z<count($concepto); $z++) {
									if($concepto[$z][3] == 'N' and $concepto[$z][2] == 'S') {

										$sql = "INSERT INTO comprobante_det (id_comp,cuenta,tercero,centrocosto,detalle,cheque,valdebito, valcredito,estado,vigencia) VALUES ('29 $rowCorteDetalle[0]', '".$concepto[$z][0]."', '$rowTercero[0]', '$rowServicio[1]', 'Facturacion numero $rowCorteDetalle[0] cargo fijo servicio $rowServicio[2]', '', $rowFacturaUsuario[2], 0,'1', '$rowDatosFactura[1]')";
										mysqli_query($linkbd, $sql);
									}

									if($concepto[$z][3] == 'S' and $concepto[$z][2] == 'N') {

										$sql = "INSERT INTO comprobante_det (id_comp,cuenta,tercero,centrocosto,detalle,cheque,valdebito, valcredito,estado,vigencia) VALUES ('29 $rowCorteDetalle[0]', '".$concepto[$z][0]."', '$rowTercero[0]', '$rowServicio[1]', 'Facturacion numero $rowCorteDetalle[0] cargo fijo servicio $rowServicio[2]', '', 0, $rowFacturaUsuario[2], '1', '$rowDatosFactura[1]')";
										mysqli_query($linkbd, $sql);
									}
								}
							break;

								//Consumo
							case 2:
								$sqlServicio = "SELECT consumo_u, cc, nombre FROM srvservicios WHERE id = '$rowFacturaUsuario[0]' ";
								$rowServicio = mysqli_fetch_row(mysqli_query($linkbd, $sqlServicio));

								$concepto = concepto_cuentasn2($rowServicio[0],'CL',10,$rowServicio[1],$rowDatosFactura[0]);

								for($z=0; $z<count($concepto); $z++) {
									if($concepto[$z][3] == 'N' and $concepto[$z][2] == 'S') {

										$sql = "INSERT INTO comprobante_det (id_comp,cuenta,tercero,centrocosto,detalle,cheque,valdebito, valcredito,estado,vigencia) VALUES ('29 $rowCorteDetalle[0]', '".$concepto[$z][0]."', '$rowTercero[0]', '$rowServicio[1]', 'Facturacion numero $rowCorteDetalle[0] consumo servicio $rowServicio[2]', '', $rowFacturaUsuario[2], 0,'1', '$rowDatosFactura[1]')";
										mysqli_query($linkbd, $sql);
									}

									if($concepto[$z][3] == 'S' and $concepto[$z][2] == 'N') {

										$sql = "INSERT INTO comprobante_det (id_comp,cuenta,tercero,centrocosto,detalle,cheque,valdebito, valcredito,estado,vigencia) VALUES ('29 $rowCorteDetalle[0]', '".$concepto[$z][0]."', '$rowTercero[0]', '$rowServicio[1]', 'Facturacion numero $rowCorteDetalle[0] consumo servicio $rowServicio[2]', '', 0, $rowFacturaUsuario[2], '1', '$rowDatosFactura[1]')";
										mysqli_query($linkbd, $sql);
									}
								}
							break;

								//subsidio cargo fijo
							case 3:
								$sqlServicio = "SELECT subsidio_cf_u, cc, nombre FROM srvservicios WHERE id = '$rowFacturaUsuario[0]' ";
								$rowServicio = mysqli_fetch_row(mysqli_query($linkbd, $sqlServicio));

								$concepto = concepto_cuentasn2($rowServicio[0],'SB',10,$rowServicio[1],$rowDatosFactura[0]);

								for($z=0; $z<count($concepto); $z++) {
									if($concepto[$z][3] == 'N' and $concepto[$z][2] == 'S') {

										$sql = "INSERT INTO comprobante_det (id_comp,cuenta,tercero,centrocosto,detalle,cheque,valdebito, valcredito,estado,vigencia) VALUES ('29 $rowCorteDetalle[0]', '".$concepto[$z][0]."', '$rowTercero[0]', '$rowServicio[1]', 'Facturacion numero $rowCorteDetalle[0] subsidio cargo fijo servicio $rowServicio[2]', '', $rowFacturaUsuario[3], 0,'1', '$rowDatosFactura[1]')";
										mysqli_query($linkbd, $sql);
									}

									if($concepto[$z][3] == 'S' and $concepto[$z][2] == 'N') {

										$sql = "INSERT INTO comprobante_det (id_comp,cuenta,tercero,centrocosto,detalle,cheque,valdebito, valcredito,estado,vigencia) VALUES ('29 $rowCorteDetalle[0]', '".$concepto[$z][0]."', '$rowTercero[0]', '$rowServicio[1]', 'Facturacion numero $rowCorteDetalle[0] subsidio cargo fijo servicio $rowServicio[2]', '', 0, $rowFacturaUsuario[3], '1', '$rowDatosFactura[1]')";
										mysqli_query($linkbd, $sql);
									}
								}
							break;

								//subsidio consumo
							case 4:
								$sqlServicio = "SELECT subsidio_cs_u, cc, nombre FROM srvservicios WHERE id = '$rowFacturaUsuario[0]' ";
								$rowServicio = mysqli_fetch_row(mysqli_query($linkbd, $sqlServicio));

								$concepto = concepto_cuentasn2($rowServicio[0],'SB',10,$rowServicio[1],$rowDatosFactura[0]);

								for($z=0; $z<count($concepto); $z++) {
									if($concepto[$z][3] == 'N' and $concepto[$z][2] == 'S') {

										$sql = "INSERT INTO comprobante_det (id_comp,cuenta,tercero,centrocosto,detalle,cheque,valdebito, valcredito,estado,vigencia) VALUES ('29 $rowCorteDetalle[0]', '".$concepto[$z][0]."', '$rowTercero[0]', '$rowServicio[1]', 'Facturacion numero $rowCorteDetalle[0] subsidio consumo servicio $rowServicio[2]', '', $rowFacturaUsuario[3], 0,'1', '$rowDatosFactura[1]')";
										mysqli_query($linkbd, $sql);
									}

									if($concepto[$z][3] == 'S' and $concepto[$z][2] == 'N') {

										$sql = "INSERT INTO comprobante_det (id_comp,cuenta,tercero,centrocosto,detalle,cheque,valdebito, valcredito,estado,vigencia) VALUES ('29 $rowCorteDetalle[0]', '".$concepto[$z][0]."', '$rowTercero[0]', '$rowServicio[1]', 'Facturacion numero $rowCorteDetalle[0] subsidio consumo servicio $rowServicio[2]', '', 0, $rowFacturaUsuario[3], '1', '$rowDatosFactura[1]')";
										mysqli_query($linkbd, $sql);
									}
								}
							break;

							//contribucion
							case 5:
								$sqlServicio = "SELECT contribucion_u, cc, nombre FROM srvservicios WHERE id = '$rowFacturaUsuario[0]' ";
								$rowServicio = mysqli_fetch_row(mysqli_query($linkbd, $sqlServicio));

								$concepto = concepto_cuentasn2($rowServicio[0],'SC',10,$rowServicio[1],$rowDatosFactura[0]);

								for($z=0; $z<count($concepto); $z++) {
									if($concepto[$z][3] == 'N' and $concepto[$z][2] == 'S') {

										$sql = "INSERT INTO comprobante_det (id_comp,cuenta,tercero,centrocosto,detalle,cheque,valdebito, valcredito,estado,vigencia) VALUES ('29 $rowCorteDetalle[0]', '".$concepto[$z][0]."', '$rowTercero[0]', '$rowServicio[1]', 'Facturacion numero $rowCorteDetalle[0] contribucion servicio $rowServicio[2]', '', $rowFacturaUsuario[2], 0,'1', '$rowDatosFactura[1]')";
										mysqli_query($linkbd, $sql);
									}

									if($concepto[$z][3] == 'S' and $concepto[$z][2] == 'N') {

										$sql = "INSERT INTO comprobante_det (id_comp,cuenta,tercero,centrocosto,detalle,cheque,valdebito, valcredito,estado,vigencia) VALUES ('29 $rowCorteDetalle[0]', '".$concepto[$z][0]."', '$rowTercero[0]', '$rowServicio[1]', 'Facturacion numero $rowCorteDetalle[0] contribucion servicio $rowServicio[2]', '', 0, $rowFacturaUsuario[2], '1', '$rowDatosFactura[1]')";
										mysqli_query($linkbd, $sql);
									}
								}
							break;

								//interes moratorios
							case 10:
								$sqlServicio = "SELECT interes_moratorio, cc, nombre FROM srvservicios WHERE id = '$rowFacturaUsuario[0]' ";
								$rowServicio = mysqli_fetch_row(mysqli_query($linkbd, $sqlServicio));

								$concepto = concepto_cuentasn2($rowServicio[0],'SM',10,$rowServicio[1],$rowDatosFactura[0]);

								for($z=0; $z<count($concepto); $z++) {
									if($concepto[$z][3] == 'N' and $concepto[$z][2] == 'S') {

										$sql = "INSERT INTO comprobante_det (id_comp,cuenta,tercero,centrocosto,detalle,cheque,valdebito, valcredito,estado,vigencia) VALUES ('29 $rowCorteDetalle[0]', '".$concepto[$z][0]."', '$rowTercero[0]', '$rowServicio[1]', 'Facturacion numero $rowCorteDetalle[0] interes servicio $rowServicio[2]', '', $rowFacturaUsuario[2], 0,'1', '$rowDatosFactura[1]')";
										mysqli_query($linkbd, $sql);
									}

									if($concepto[$z][3] == 'S' and $concepto[$z][2] == 'N') {

										$sql = "INSERT INTO comprobante_det (id_comp,cuenta,tercero,centrocosto,detalle,cheque,valdebito, valcredito,estado,vigencia) VALUES ('29 $rowCorteDetalle[0]', '".$concepto[$z][0]."', '$rowTercero[0]', '$rowServicio[1]', 'Facturacion numero $rowCorteDetalle[0] interes servicio $rowServicio[2]', '', 0, $rowFacturaUsuario[2], '1', '$rowDatosFactura[1]')";
										mysqli_query($linkbd, $sql);
									}
								}
							break;
						}
					}

					$c ++;
					$porcentaje = $c * 100 / $totalClientes;

					echo"
					<script>
						progres='".round($porcentaje)."';callprogress(progres);
						
					</script>";
					flush();
					ob_flush();
					usleep(5);
				}
			}
			?>
		</table>

		<input type="hidden" name="oculto" id="oculto" value="1" />

	</form>
	<div id="bgventanamodal2">
		<div id="ventanamodal2">
			<IFRAME src="" name="buster" marginWidth=0 marginHeight=0 frameBorder=0 id="ventana2" frameSpacing=0 style="left:500px; width:900px; height:500px; top:200;"></IFRAME>
		</div>
	</div>
</body>

</html>