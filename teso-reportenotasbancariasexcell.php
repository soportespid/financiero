<?php 
	require_once 'PHPExcel/Classes/PHPExcel.php';// Incluir la libreria PHPExcel
	include 'PHPExcel/Classes/PHPExcel/IOFactory.php';// PHPExcel_IOFactory
	require "comun.inc";
	require "funciones.inc";
	session_start();
	$linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");
	$objPHPExcel = new PHPExcel();// Crea un nuevo objeto PHPExcel
	$objPHPExcel->getProperties()->setCreator("G&C Tecnoinversiones SAS")
		->setLastModifiedBy("HAFR")
		->setTitle("Lista Notas Bancarias")
		->setSubject("Notas Bancarias")
		->setDescription("Liastado de Notas Bancarias")
		->setKeywords("N. Bancarias")
		->setCategory("Tesoreria");
	$objPHPExcel->setActiveSheetIndex(0)
		->mergeCells('A1:I1')
		->mergeCells('A2:I2')
		->setCellValue('A1', 'Notas Bancarias')
		->setCellValue('A2', 'INFORMACION GENERAL');
	$objPHPExcel-> getActiveSheet ()
		-> getStyle ("A1")
		-> getFill ()
		-> setFillType (PHPExcel_Style_Fill :: FILL_SOLID)
		-> getStartColor ()
		-> setRGB ('C8C8C8');
	$objPHPExcel-> getActiveSheet ()
		-> getStyle ("A1:A2")
		-> getFont ()
		-> setBold ( true ) 
		-> setName ( 'Verdana' ) 
		-> setSize ( 10 ) 
		-> getColor ()
		-> setRGB ('000000');
	$objPHPExcel-> getActiveSheet ()	
		-> getStyle ('A1:A2')
		-> getAlignment ()
		-> applyFromArray (array ( 'horizontal'  =>  PHPExcel_Style_Alignment :: HORIZONTAL_CENTER ,) ); 
	$objPHPExcel-> getActiveSheet ()	
		-> getStyle ('A3:I3')
		-> getAlignment ()
		-> applyFromArray (array ( 'horizontal'  =>  PHPExcel_Style_Alignment :: VERTICAL_JUSTIFY ,) ); 
	$objPHPExcel-> getActiveSheet ()
		-> getStyle ("A2")
		-> getFill ()
		-> setFillType (PHPExcel_Style_Fill :: FILL_SOLID)
		-> getStartColor ()
		-> setRGB ('A6E5F3');
	$objPHPExcel-> getActiveSheet ()
		-> getStyle ("A3:I3")
		-> getFill ()
		-> setFillType (PHPExcel_Style_Fill :: FILL_SOLID)
		-> getStartColor ()
		-> setRGB ('22C6CB');
	$borders = array(
		'borders' => array(
		'allborders' => array(
			'style' => PHPExcel_Style_Border::BORDER_THIN,
			'color' => array('argb' => 'FF000000'),
		)
		),
	);
	$objPHPExcel->getActiveSheet()->getStyle('A1:I1')->applyFromArray($borders);
	$objPHPExcel->getActiveSheet()->getStyle('A2:I2')->applyFromArray($borders);
	$objPHPExcel->getActiveSheet()->getStyle('A3:I3')->applyFromArray($borders);
	$objPHPExcel-> getActiveSheet()->getColumnDimension('A')->setWidth(12); 
	$objPHPExcel-> getActiveSheet()->getColumnDimension('B')->setWidth(11);
	$objPHPExcel-> getActiveSheet()->getColumnDimension('C')->setWidth(55);
	$objPHPExcel-> getActiveSheet()->getColumnDimension('D')->setWidth(12);
	$objPHPExcel-> getActiveSheet()->getColumnDimension('E')->setWidth(11);
	$objPHPExcel-> getActiveSheet()->getColumnDimension('F')->setWidth(55);
	$objPHPExcel-> getActiveSheet()->getColumnDimension('G')->setWidth(55);
	$objPHPExcel-> getActiveSheet()->getColumnDimension('H')->setWidth(11);
	$objPHPExcel-> getActiveSheet()->getColumnDimension('I')->setWidth(11);
	$objWorksheet = $objPHPExcel->getActiveSheet();
	$objWorksheet->fromArray(array('Nota bancaria','Fecha','Concepto Nota Bancaria','Valor','Cuenta','Nombre Cuenta','Concepto','Tipo Nota','Estado'),NULL,'A3');
	if($_GET['notaban']!=''){
		$crit1 = "AND T2.gastoban LIKE '%".$_GET['notaban']."%' ";
	}
	if($_GET['fecha1']!="" && $_GET['fecha2']!=""){
		preg_match("/([0-9]{2})\/([0-9]{2})\/([0-9]{4})/", $_GET['fecha1'],$fecha);
		$fechaf = $fecha[3]."-".$fecha[2]."-".$fecha[1];
		preg_match("/([0-9]{2})\/([0-9]{2})\/([0-9]{4})/", $_GET['fecha2'],$fecha);
		$fechaf2 = $fecha[3]."-".$fecha[2]."-".$fecha[1];
		$crit2 = "AND T1.fecha BETWEEN '$fechaf' AND '$fechaf2'";
	}
	$cont=4;
	$sqlr = "SELECT * FROM tesonotasbancarias_cab AS T1 INNER JOIN tesonotasbancarias_det AS T2 ON T1.id_comp = T2.id_notabancab WHERE T1.estado <> '' $crit1 $crit2 ORDER BY T1.id_comp DESC";
	$resp = mysqli_query($linkbd,$sqlr);
	while ($row = mysqli_fetch_row($resp)) 
	{
		if($row[4]=='S'){
			$imgsem="ACTIVO";
		}			  
		if($row[4]=='N'){
			$imgsem="ANULADO";
		}
		$sqlrd = "SELECT SUM(valor),ncuentaban FROM tesonotasbancarias_det WHERE id_notabancab = '$row[1]'";
		$resn = mysqli_query($linkbd,$sqlrd);
		$rn = mysqli_fetch_row($resn);
		
		$sqlr1 = "SELECT * FROM tesobancosctas tb1, cuentasnicsp tb2 WHERE tb1.cuenta = tb2.cuenta AND tb1.ncuentaban = '$rn[1]'";
		$res1 = mysqli_query($linkbd,$sqlr1);
		$rn1 = mysqli_fetch_assoc($res1);

		$sqltn = "SELECT nombre, tipo FROM tesogastosbancarios WHERE codigo = '$row[14]'";
		$restn = mysqli_query($linkbd,$sqltn);
		$rowtn = mysqli_fetch_row($restn);
		if($rowtn[1] == 'G'){
			$tipnota = "GASTO";
		}else{
			$tipnota = "INGRESO";
		}
		$unircod = "$row[14] - $rowtn[0]";

		$objWorksheet->fromArray(array($row[1],date('d-m-Y',strtotime($row[2])),$row[5],$rn[0],$rn1["ncuentaban"],$rn1["nombre"],$unircod,$tipnota,$imgsem),NULL,"A$cont");
		$objPHPExcel->getActiveSheet()->getStyle("A$cont:I$cont")->applyFromArray($borders);
		$objPHPExcel->getActiveSheet()->getStyle("D$cont:D$cont")->getNumberFormat()
		->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);
		
		$objPHPExcel->getActiveSheet()->getStyle('A3:I3')->getAlignment()->applyFromArray(array('vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,));
		$objPHPExcel->getActiveSheet()->getStyle('A4:I4')->getAlignment()->applyFromArray(array('vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,));
		$objPHPExcel->getActiveSheet()->getStyle('A4')->getAlignment()->applyFromArray(array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,));
		$objPHPExcel->getActiveSheet()->getStyle('C3')->getAlignment()->applyFromArray(array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,));
		$objPHPExcel->getActiveSheet()->getStyle('A:I')->getAlignment()->applyFromArray(array('vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,));
		$objPHPExcel->getActiveSheet()->getStyle('A')->getAlignment()->applyFromArray(array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,));
		$objPHPExcel-> getActiveSheet()-> getStyle ("A3:I3")-> getFont ()-> setBold ( true );
		
		$cont=$cont+1;
	}
	$objPHPExcel->getActiveSheet()->setTitle('Listado 1');// Renombrar Hoja
	$objPHPExcel->setActiveSheetIndex(0);// Establecer la hoja activa, para que cuando se abra el documento se muestre primero.
	// --------Cerrar--------
	header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
	header('Content-Disposition: attachment;filename="Listado Egresos.xlsx"');
	header('Cache-Control: max-age=0');
	$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
	$objWriter->save('php://output');
	exit;
?>
