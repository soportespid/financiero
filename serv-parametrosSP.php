<?php
		require 'comun.inc';
		require 'funciones.inc';
		session_start();
		cargarcodigopag($_GET['codpag'],$_SESSION['nivel']);
		header('Cache-control: private'); // Arregla IE 6
		header("Cache-control: no-cache, no-store, must-revalidate");
		header("Content-Type: text/html;charset=utf8");
		date_default_timezone_set('America/Bogota');
		titlepag();
?>
<!DOCTYPE >
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="es">
	<head>
		<meta http-equiv="Content-type" content="text/html; charset=utf8"/>
		<meta http-equiv="X-UA-Compatible" content="IE=9"/>
		<title>:: IDEAL 10 - Servicios Públicos</title>
		<link href="css/css2.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<link href="css/css4.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<link href="css/cssSP.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<link href="css/tabs.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css"/>
		<link href="css/style.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css"/>
        <script type="text/javascript" src="css/calendario.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script src="vue/vue.js"></script>
		<script type="text/javascript" src="css/programas.js"></script>        

		<!-- sweetalert2 -->
		<script src="sweetalert2/dist/sweetalert2.min.js"></script>
		<link rel="stylesheet" href="sweetalert2/dist/sweetalert2.min.css">
	</head>
	<body>
		<div class="subpantalla" style="height:640px; width:99.6%; overflow:hidden;">
			<div id="myapp" style="height:inherit;" v-cloak>
				<table>
					<tr><script>barra_imagenes("serv");</script><?php cuadro_titulos();?></tr>
					<tr><?php menu_desplegable("serv");?></tr>
				</table>

				<div class="bg-white group-btn p-1" id="newNavStyle">
                    <button type="button" class="btn btn-white btn-primary-hover d-flex justify-between align-items-center" @click="window.location.reload()">
                        <span>Nuevo</span>
                        <svg viewBox="0 -960 960 960" ><path d="M440-440H200v-80h240v-240h80v240h240v80H520v240h-80v-240Z"/></svg>
                    </button>
                    <button type="button" class="btn btn-white btn-primary-hover d-flex justify-between align-items-center" @click="guardar()">
                        <span>Guardar</span>
                        <svg viewBox="0 -960 960 960" ><path d="M840-680v480q0 33-23.5 56.5T760-120H200q-33 0-56.5-23.5T120-200v-560q0-33 23.5-56.5T200-840h480l160 160Zm-80 34L646-760H200v560h560v-446ZM480-240q50 0 85-35t35-85q0-50-35-85t-85-35q-50 0-85 35t-35 85q0 50 35 85t85 35ZM240-560h360v-160H240v160Zm-40-86v446-560 114Z"/></svg>
                    </button>
                    <button type="button" class="btn btn-white btn-primary-hover d-flex justify-between align-items-center" @click="mypop=window.open('serv-principal.php','',''); mypop.focus();">
                        <span>Nueva ventana</span>
                        <svg class="fill-black group-hover:fill-white w-5 h-5" viewBox="0 -960 960 960" ><path d="M320-120v-80h80v-80H160q-33 0-56.5-23.5T80-360v-400q0-33 23.5-56.5T160-840h640q33 0 56.5 23.5T880-760v400q0 33-23.5 56.5T800-280H560v80h80v80H320ZM160-360h640v-400H160v400Zm0 0v-400 400Z"/></svg>
                    </button>
                </div>

				<table class="inicio grande">
                    <tr>
                        <td class="titulos" colspan="5">.: Parametros Servicios Públicos</td>
                        <td class="cerrar" style="width:7%" onClick="location.href='serv-principal.php'">Cerrar</td>
                    </tr>    

                    <tr>
                        <td class="tamano01" style="width: 10%;">Rango de consumo:</td>
                        <td style="width: 15%;">
                            <select id="rangoConsumo" v-model="rangoConsumo" class="centrarSelect" style="width: 98%;">
                                <option class="aumentarTamaño" value="unico">Rango único</option>
                                <option class="aumentarTamaño" value="multiple">Rango multiple</option>
                            </select>
                        </td>

                        <td class="tamano01" style="width: 10%;">Periodo de Facturación:</td>
                        <td style="width: 15%;">
                            <select id="periodoFacturacion" v-model="periodoFacturacion" class="centrarSelect" style="width: 98%;">
                                <option class="aumentarTamaño" value="mensual">Mensual</option>
                                <option class="aumentarTamaño" value="bimestral">Bimestral</option>
                                <option class="aumentarTamaño" value="trimestral">Trimestral</option>
                                <option class="aumentarTamaño" value="semestral">Semestral</option>
                            </select>
                        </td>
                    </tr>

                    <tr>
                        <td class="tamano01" style="width: 10%;">Aplica cobro de intereses por mora:</td>
                        <td style="width: 15%;">
                            <select id="intereses" v-model="intereses" class="centrarSelect" v-on:change="mostrarIntereses" style="width: 98%;">
                                <option class="aumentarTamaño" value="S">Aplica</option>
                                <option class="aumentarTamaño" value="N">No Aplica</option>
                            </select>
                        </td>

                    
                        <td class="tamano01" style="width: 10%;" v-show="showIntereses">Porcentaje de intereses:</td>
                        <td style="width: 15%;">
                            <input type="number" name="porcentajeIntereses" id="porcentajeIntereses" v-model="porcentajeIntereses" v-show="showIntereses" style="width: 98%; height: 30px; text-align:center;">
                        </td>
                    
                    </tr>

                    <tr>
                        <td class="tamano01" style="width: 10%;">Desactivación automatica de los servicios:</td>
                        <td style="width: 15%;">
                            <select id="desactivacionServicios" v-model="desactivacionServicios" v-on:change="mostrarFacturas" class="centrarSelect" style="width: 98%;">
                                <option class="aumentarTamaño" value="S">Si</option>
                                <option class="aumentarTamaño" value="N">No</option>
                            </select>
                        </td>

                        <td class="tamano01" style="width: 10%;" v-show="showDesactivacion">Numero de facturas vencidas:</td>
                        <td>
                            <input type="number" id="facturasVencidas" v-model="facturasVencidas" v-show="showDesactivacion" style="width: 98%; height: 30px; text-align:center;">
                        </td>
                    </tr>

					<tr>
						<td class="tamano01" style="width: 10%;" v-show="showIntereses">Porcentaje Minimo para Acuerdo de Pago:</td>
                        <td style="width: 15%;">
                            <input type="number" name="porcentajeAcuerdo" id="porcentajeAcuerdo" v-model="porcentajeAcuerdo" v-show="showIntereses" style="width: 98%; height: 30px; text-align:center;">
                        </td>
					</tr>
				</table>

			</div>
		</div>
		
		
		<script src="Librerias/vue/vue.min.js"></script>
		<script src="Librerias/vue/axios.min.js"></script>
		<script src="vue/servicios_publicos/serv-parametrosSP.js?<?php echo date('d_m_Y_h_i_s');?>"></script>	
	</body>
</html>