<?php //V 1000 12/12/16 ?> 
<?php
	ini_set('max_execution_time',3600);
	require"comun.inc";
	require"funciones.inc";
	session_start();
	//$linkbd=conectar_bd();	

	$linkbd_v7 = conectar_v7();
	$linkbd_v7 -> set_charset("utf8");

	//$linkbd_concejo = conectar_concejo_V7();

	cargarcodigopag($_GET['codpag'], $_SESSION["nivel"]);
	header("Cache-control: private");
	date_default_timezone_set("America/Bogota");
	//**niveles menu: Administracion (0) - Consultas (1) - Herramientas (2) - Reportes (3) - otro
?>
<!DOCTYPE >
<html lang="es">
	<head>
		<meta charset="utf-8"/>
		<meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1"/>
		<meta name="viewport" content="user-scalable=no">
		<title>:: IDEAL 10 - Contabilidad</title>
		<link href="favicon.ico" rel="shortcut icon"/>
        <link href="css/css2.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<link href="css/css4.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="css/calendario.js"></script>
		<script type="text/javascript" src="css/programas.js"></script>
		<script type="text/javascript" src="jquery-1.11.0.min.js"></script> 

		<script>
		$(window).load(function () {
				$('#cargando').hide();
			});
			function pdf()
			{
				document.form2.action="pdfbalance.php";
				document.form2.target="_BLANK";
				document.form2.submit(); 
				document.form2.action="";
				document.form2.target="";
			}
			function excell()
			{
				document.form2.action="cont-balanceGeneral-excel.php";
				document.form2.target="_BLANK";
				document.form2.submit(); 
				document.form2.action="";
				document.form2.target="";
			}
			function despliegamodal2(_valor,_nomcu)
			{
				document.getElementById("bgventanamodal2").style.visibility=_valor;
				if(_valor=="hidden"){document.getElementById('ventana2').src="";}
				else 
				{
					document.getElementById('ventana2').src="cuentasgral-ventana01.php?vigencia=<?php echo $_SESSION['vigencia']?>&objeto="+_nomcu+"&nobjeto=000";
				}
			}
			
			function direccionaCuentaGastos(row)
			{
				var cell = row.getElementsByTagName("td")[0];
				var id = cell.innerHTML;																			 
				var fech=document.getElementById("fecha").value;
				var fech1=document.getElementById("fecha2").value;
				window.open("cont-auxiliarcuenta.php?cod="+id+"&fec="+fech+"&fec1="+fech1);
			}
			function validar()
			{
				/* var fech=document.getElementById("fecha").value; */
				var fech1=document.getElementById("fecha2").value;
				/* var separador1=fech.split("/"); */
				var separador2=fech1.split("/");
				if(separador2[2]<'2018')
				{
					alert ('Las fechas deben ser mayores del 2017');
				}
				else
				{
					document.form2.submit();
				}
			}

			function validaExterno(e)
			{
				//console.log(e);
				document.form2.oculto.value = '';
				document.form2.externo.value = '1';
				document.form2.consolidado.checked=false;
				document.form2.submit();
			}

			function validaCierre(e)
			{
				//console.log(e);
				document.form2.oculto.value = '';
				document.form2.cierre.value = '1';
				document.form2.submit();
			}

			function validaConsolidado(e)
			{
				document.form2.oculto.value = '';
				document.form2.consolidado.value = '1';
				document.form2.cc.value = '';
				document.form2.externo.checked=false;
				document.form2.submit();
			}
			var expanded = false;
			function showCheckboxes()
			{
				var checkboxes = document.getElementById("checkboxes");
				if (!expanded) 
				{
					checkboxes.style.display = "block";
					expanded = true;
				}
				else
				{
					checkboxes.style.display = "none";
					expanded = false;
				}
			}
			
			/* document.addEventListener("click", function(e)
			{
				var div = document.getElementById("checkboxes");
				console.log(div);
				//obtiendo informacion del DOM para  
				var clic = e.target;
				
				if(div.style.display == "block" && clic != div)
				{
					div.style.display = "none";
				}
			}, true); */
		</script>
		<style type="text/css">
			.multiselect 
			{
				width: 200px;
				float:left;
				padding-left:10px;
				padding-top:5px;
			}
			.generar-report 
			{
				float:right;
				padding-right:30px;
			}
			.selectBox {position: relative;}
			.selectBox select
			{
				width: 100%;
				font-weight: bold;
			}
			.overSelect
			{
				position: absolute;
				left: 0;
				right: 0;
				top: 0;
				bottom: 0;
			}
			#checkboxes
			{
				display: none;
				border: 1px #dadada solid;
				position: absolute;
				width: 18%;
				overflow-y: scroll;
				z-index: 999999999;
			}
			#checkboxes1
			{
				display: none;
				border: 1px #dadada solid;
				position: absolute;
				width: 20%;
				overflow-y: scroll;
				z-index: 9999;	
			}
			#checkboxes label,#checkboxes1 label
			{
				display: block;
				background: #ECEFF1;
				border-bottom: 1px solid #CFD8DC;
				font-size: 10px;
			}
			#checkboxes label:last-child, #checkboxes1 label:last-child
			{
				display: block;
				background: #ECEFF1;
				border-bottom: none;
			}
			#checkboxes label:hover,#checkboxes1 label:hover
			{
				background-color: #1e90ff;
				cursor:pointer;
			}
		</style>
		<?php titlepag();?>
	</head>
	<body>
		<div id="cargando" style=" position:absolute;left: 46%; bottom: 45%">
			<img src="imagenes/loading.gif" style=" width: 80px; height: 80px"/>
		</div>
		<table>
   			<tr><script>barra_imagenes("cont");</script><?php cuadro_titulos();?></tr>
    		<tr><?php menu_desplegable("cont");?></tr>
			<tr>
  				<td colspan="3" class="cinta">
				<a href="cont-balanceGeneral.php" class="mgbt"><img src="imagenes/add.png" title="Nuevo" /></a>
				<a class="mgbt"><img src="imagenes/guardad.png"/></a>
				<a href="#" onClick="document.form2.submit()" class="mgbt"><img src="imagenes/busca.png" title="Buscar" /></a>
				<a onClick="mypop=window.open('plan-agenda.php','','');mypop.focus()" class="mgbt"><img src="imagenes/agenda1.png" title="Agenda" /></a>
				<a href="#" onClick="mypop=window.open('cont-principal.php','','');mypop.focus();" class="mgbt"><img src="imagenes/nv.png" title="nueva ventana"></a>
				<a href="#" onClick="pdf()" class="mgbt"><img src="imagenes/print.png" title="imprimir"></a>
				<a href="#"  onClick="excell()" class="mgbt"><img src="imagenes/excel.png"  title="excel"></a>
				<a href="<?php echo "archivos/".$_SESSION['usuario']."balanceprueba-nivel$_POST[nivel].csv"; ?>" target="_blank" class="mgbt"><img src="imagenes/csv.png"  title="csv"></a>
				<a href="cont-estadosfinancieros.php" class="mgbt"><img src="imagenes/iratras.png" title="Atr&aacute;s"></a>
			</td>
  			</tr>
		</table>
 		<form name="form2" method="post" action="cont-balanceGeneral.php">
 			<?php
 				$vigusu = vigencia_usuarios($_SESSION['cedulausu']);
 				if($_POST['consolidado'] == ''){$chkcomp=' ';}
 				else {$chkcomp = ' checked ';}
 				if($_POST['cierre'] == ''){$chkcierre=' ';}
 				else {$chkcierre = ' checked ';}
				$_POST['tipocc'] = "";

				if($_POST['externo'] == ''){$chkexterno=' ';}
 				else {$chkexterno = ' checked ';}
 			?>
			<table  align="center" class="inicio" >
			<tr>
				<td class="titulos" colspan="8" >.: Balance general</td>
        		<td class="cerrar" style="width:7%;"><a href="cont-principal.php">&nbsp;Cerrar</a></td>
    		</tr>
    		<tr>
       			<td class="saludo1" style="width:10%;">Nivel:</td>
        		<td style="width:12%;">
        			<select name="nivel" id="nivel" style="width:80%;" tabindex="1" disabled>
						<?php
                            $_POST['nivel'] = 2;
                            $niveles=array();
                            $sqlr = "Select * from nivelesctas  where estado='S' order by id_nivel";
                            $resp = mysqli_query($linkbd_v7, $sqlr);
                            while ($row = mysqli_fetch_row($resp)) 
                            {
                                $niveles[]=$row[4];
                                if($row[0]==$_POST['nivel']){echo "<option value=$row[0] SELECTED>$row[0]</option>";}
                                else{echo "<option value=$row[0]>$row[0]</option>";}
                            }
                        ?>
        			</select>
      			</td>
       	 		<!-- <td class="saludo1" style="width:10%;">Mes Inicial:</td>
        		<td style="width:10%;">
					<input name="fecha" type="text" id="fecha" title="DD/MM/YYYY" style="width:80%;" value="<?php /*echo $_POST['fecha'];*/ ?>" onKeyUp="return tabular(event,this) " onKeyDown="mascara(this,'/',patron,true)" onChange = "" maxlength="10" tabindex="2"/>&nbsp;<a href="#" onClick="displayCalendarFor('fecha');" tabindex="3" title="Calendario"><img src="imagenes/calendario04.png" align="absmiddle" style="width:20px;"></a>
				</td> -->
        		<td class="saludo1" style="width:10%;">Fecha:</td>
        		<td style="width:10%;">
					<input type="text" name="fecha2" id="fecha2" title="DD/MM/YYYY" style="width:80%;" value="<?php echo $_POST['fecha2']; ?>" onKeyUp="return tabular(event,this)" onKeyDown="mascara(this,'/',patron,true)" onChange = "" maxlength="10" tabindex="4"/>&nbsp;<a href="#" onClick="displayCalendarFor('fecha2');" tabindex="5" title="Calendario"><img src="imagenes/calendario04.png" align="absmiddle" style="width:20px;"></a>
				</td>
            	<td>
					<input type="button" name="generar" value="Generar" onClick="validar()" tabindex="13">
       			</td>
   	 		</tr>  
		</table>
 		<input type="hidden" name="tipocc1" value='<?php echo $_POST['tipocc1']?>'>
		<input type="hidden" name="oculto" value="1"></td>
		<div class="subpantallap" style="height:62%; width:99.6%; overflow-x:hidden;">
  		<?php
		  	function generarCentroCosto($conector,$tabla,$arreglo)
			{
				$tamArreglo = count($arreglo);
				if($tamArreglo > 0)
				{
					$resultado=" and (";
				}
				else
				{
					$resultado=" ";
				}
				
				$tiene=false;
				$unidades="";
				for($i=0;$i<$tamArreglo; $i++ )
				{
					$unidades.=($arreglo[$i]).",";
					if($i == 0)
					{
						$resultado.=" $tabla.centrocosto like '$arreglo[$i]'";
					}
					else
					{
						$resultado.=" or $tabla.centrocosto like '$arreglo[$i]'";
					}
						
				}
				$unidades=substr($unidades,0,-1);
				if($tamArreglo > 0)
				{
					$resultado.=" )";
				}
				else
				{
					$resultado=" ";
				}
				//$resultado.="$tabla.centrocosto IN ($unidades)";
				return $resultado;
			}
			//echo "tipo :  ".$_POST[tipocc];
  			//**** para sacar la consulta del balance se necesitan estos datos ********
  			//**** nivel, mes inicial, mes final, cuenta inicial, cuenta final, cc inicial, cc final  
			$oculto=$_POST['oculto'];
			if($_POST['oculto'])
			{
				$critcons=" and comprobante_det.tipo_comp <> 19 ";
				
				$sqlrcc = "select id_cc from centrocosto where entidad='N'";
				$rescc = mysqli_query($linkbd_v7, $sqlrcc);
				while($rowcc = mysqli_fetch_row($rescc))
				{ $critcons2.=" and comprobante_det.centrocosto <> '$rowcc[0]' ";}
				
				$vectorBusqueda=explode("-",$_POST['filtros']);
				if($vectorBusqueda[0]==''){unset($vectorBusqueda);}
				$busqueda.=generarCentroCosto("LIKE","comprobante_det",$vectorBusqueda);
				
				$ccExterno = '';
				if($_POST['tipocc']=='N' )
				{
					$ccExterno = $_POST['cc'];
					$_POST['cc']="";
				}
	
				if($_POST['cierre']=='1'){$critconscierre=" ";}
				else {$critconscierre=" and comprobante_det.tipo_comp <> 13 ";}
				//echo $critcons;
				$horaini=date('h:i:s');		

				/* preg_match("/([0-9]{2})\/([0-9]{2})\/([0-9]{4})/",$_POST['fecha'],$fecha);
				$fechaf1=$fecha[3]."-".$fecha[2]."-".$fecha[1]; */

				preg_match("/([0-9]{2})\/([0-9]{2})\/([0-9]{4})/",$_POST['fecha2'],$fecha);
				$fechaf2=$fecha[3]."-".$fecha[2]."-".$fecha[1];
				$vigAc = $fecha[3];
				$vigA=$fecha[3]-1;
				$fechaf3=$vigA."-".$fecha[2]."-".$fecha[1];

				/* preg_match("/([0-9]{2})\/([0-9]{2})\/([0-9]{4})/",$_POST['fecha'],$fecha);
				$fechaf=$fecha[3]."-".$fecha[2]."-".$fecha[1];

				$fechafa2=mktime(0,0,0,$fecha[2],$fecha[1],$fecha[3]);
				$f1=$fechafa2;	
				$f2=mktime(0,0,0,$fecha[2],$fecha[1],$fecha[3]);
				$fechafa=$vigusu."-01-01";
				$fechafa2=date('Y-m-d',$fechafa2-((24*60*60))); */
				
				//Borrar el balance de prueba anterior
				$sqlr2="select distinct digitos, posiciones from nivelesctas where estado='S' ORDER BY id_nivel DESC ";
				$resn = mysqli_query($linkbd_v7, $sqlr2);
				$rown = mysqli_fetch_row($resn);
				$nivmax=$rown[0];
				$dignivmax=$rown[1];
				//continuar**** creacion balance de prueba
				//$namearch="archivos/".$_SESSION[usuario]."balanceprueba.csv";
				//$Descriptor1 = fopen($namearch,"w+"); 
				//fputs($Descriptor1,"CODIGO;CUENTA;SALDO ANTERIOR;DEBITO;CREDITO;SALDO FINAL\r\n");
				
  				echo "<table class='inicio' ><tr><td colspan='6' class='titulos'>Balance de Prueba</td></tr>";
  				echo "<tr>
						<td class='titulos2'>Codigo</td>
						<td class='titulos2'>Cuenta</td>
						<td class='titulos2'>$vigAc</td>
						<td class='titulos2'>$vigA</td>
					</tr>";
    			$tam=$niveles[$_POST['nivel']-1];
				if(empty($_POST['cuenta1']) || empty($_POST['cuenta2'])){
					$_POST['cuenta1']="1";
					$_POST['cuenta2']="99999999999";
				}
				
				$crit1=" and left(cuenta,$tam)>='$_POST[cuenta1]' and left(cuenta,$tam)<='$_POST[cuenta2]' ";
				$sqlr2="select distinct cuenta,tipo from cuentasnicsp where estado ='S'  and length(cuenta)=$tam ".$crit1." group by cuenta,tipo order by cuenta ";
				$rescta=mysqli_query($linkbd_v7, $sqlr2);
				$i=0;
				//echo $sqlr2;
				$pctas=array();
				$pctasb[]=array();
				while ($row =mysqli_fetch_row($rescta)) 
 				{
  					$pctas[]=$row[0];
  					$pctasb["$row[0]"][0]=$row[0];
  					$pctasb["$row[0]"][1]=0;
  					$pctasb["$row[0]"][2]=0;
  					$pctasb["$row[0]"][3]=0;
				}
				mysqli_free_result($rescta);
				$tam = $niveles[$_POST['nivel']-1];
				//echo "tc:".count($pctas);
				//******MOVIMIENTOS PERIODO
				/* $sqlr3="SELECT DISTINCT
        		SUBSTR(comprobante_det.cuenta,1,$tam),
        		sum(comprobante_det.valdebito),
        		sum(comprobante_det.valcredito)
     			FROM comprobante_det, comprobante_cab
				WHERE comprobante_cab.tipo_comp = comprobante_det.tipo_comp
				AND comprobante_det.numerotipo = comprobante_cab.numerotipo
				AND comprobante_cab.estado = 1
				AND (comprobante_det.valdebito > 0
				OR comprobante_det.valcredito > 0)
				AND comprobante_cab.fecha BETWEEN '$fechaf1' AND '$fechaf2'
				AND comprobante_det.tipo_comp <> 7 AND comprobante_det.tipo_comp <> 102 AND comprobante_det.tipo_comp <> 100 AND comprobante_det.tipo_comp <> 101 AND comprobante_det.tipo_comp <> 103 AND comprobante_det.tipo_comp<>104 ".$critcons." ".$critconscierre."
				AND SUBSTR(comprobante_det.cuenta,1,$tam) >= '$_POST[cuenta1]' AND SUBSTR(comprobante_det.cuenta,1,$tam) <='$_POST[cuenta2]'
				$busqueda
				GROUP BY SUBSTR(comprobante_det.cuenta,1,$tam)
				ORDER BY comprobante_det.cuenta";
				//compara si no es una entidad externa
			
				$res = mysqli_query($linkbd_v7, $sqlr3);
				// echo $sqlr3;
			
				while ($row = mysqli_fetch_row($res))
				{
					$pctasb["$row[0]"][0]=$row[0];
					$pctasb["$row[0]"][2]=$row[1];
					$pctasb["$row[0]"][3]=$row[2];
				} */

				$sqlrTipoComp = "SELECT codigo FROM tipo_comprobante WHERE codigo=102";
				$resTipoComp = mysqli_query($linkbd_v7, $sqlrTipoComp);
				$rowTipoComp = mysqli_fetch_row($resTipoComp);
				if($rowTipoComp[0]!='')
				{
					$tipo_comp = 102;
				}
				else
				{
					$tipo_comp = 7;
				}
				//**** SALDO INICIAL ***
				$sqlr3="SELECT DISTINCT
				SUBSTR(comprobante_det.cuenta,1,$tam),
				sum(comprobante_det.valdebito)-
				sum(comprobante_det.valcredito)
				FROM comprobante_det, comprobante_cab
				WHERE comprobante_cab.tipo_comp = comprobante_det.tipo_comp
				AND comprobante_det.numerotipo = comprobante_cab.numerotipo
				AND comprobante_cab.estado = 1
				AND (comprobante_det.valdebito > 0
				OR comprobante_det.valcredito > 0)         
				AND comprobante_det.tipo_comp = $tipo_comp 
				AND SUBSTR(comprobante_det.cuenta,1,$tam) >= '$_POST[cuenta1]' AND SUBSTR(comprobante_det.cuenta,1,$tam) <='$_POST[cuenta2]'   
				$busqueda ".$critcons." ".$critcons2."
				GROUP BY SUBSTR(comprobante_det.cuenta,1,$tam)
				ORDER BY comprobante_det.cuenta";
				$res = mysqli_query($linkbd_v7, $sqlr3);
				while ($row = mysqli_fetch_row($res)) 
				{
					$pctasb["$row[0]"][0]=$row[0];
					$pctasb["$row[0]"][1]=$row[1];
					$pctasb["$row[0]"][2]=$row[1];
				}
				
				//*******MOVIMIENTOS PREVIOS PERIODO
				if($fechaf2>='2018-01-01')
				{
					$fecini='2018-01-01';
					$sqlr3="SELECT DISTINCT
					SUBSTR(comprobante_det.cuenta,1,$tam),
					sum(comprobante_det.valdebito)-
					sum(comprobante_det.valcredito)
					FROM comprobante_det, comprobante_cab
					WHERE comprobante_cab.tipo_comp = comprobante_det.tipo_comp
					AND comprobante_det.numerotipo = comprobante_cab.numerotipo
					AND comprobante_cab.estado = 1
					AND comprobante_det.tipo_comp <> 100
					AND comprobante_det.tipo_comp <> 101
					AND comprobante_det.tipo_comp <> 103
					AND comprobante_det.tipo_comp <> 102
					AND comprobante_det.tipo_comp <> 104
					AND comprobante_det.cuenta!=''
					AND (comprobante_det.valdebito > 0
					OR comprobante_det.valcredito > 0) 
					AND comprobante_det.tipo_comp <> 7 $critcons $critcons2
					AND comprobante_cab.fecha BETWEEN '$fecini' AND '$fechaf2'
					AND SUBSTR(comprobante_det.cuenta,1,$tam) >= '$_POST[cuenta1]' AND SUBSTR(comprobante_det.cuenta,1,$tam) <='$_POST[cuenta2]'
					$busqueda
					GROUP BY SUBSTR(comprobante_det.cuenta,1,$tam)
					ORDER BY comprobante_det.cuenta";

					$res = mysqli_query($linkbd_v7, $sqlr3);
					while ($row =mysqli_fetch_row($res)) 
					{
						$pctasb["$row[0]"][0]=$row[0];
						$pctasb["$row[0]"][1]+=$row[1]; 
					} 

					$sqlr3="SELECT DISTINCT
					SUBSTR(comprobante_det.cuenta,1,$tam),
					sum(comprobante_det.valdebito)-
					sum(comprobante_det.valcredito)
					FROM comprobante_det, comprobante_cab
					WHERE comprobante_cab.tipo_comp = comprobante_det.tipo_comp
					AND comprobante_det.numerotipo = comprobante_cab.numerotipo
					AND comprobante_cab.estado = 1
					AND comprobante_det.tipo_comp <> 100
					AND comprobante_det.tipo_comp <> 101
					AND comprobante_det.tipo_comp <> 103
					AND comprobante_det.tipo_comp <> 102
					AND comprobante_det.tipo_comp <> 104
					AND comprobante_det.cuenta!=''
					AND (comprobante_det.valdebito > 0
					OR comprobante_det.valcredito > 0) 
					AND comprobante_det.tipo_comp <> 7 $critcons $critcons2
					AND comprobante_cab.fecha BETWEEN '$fecini' AND '$fechaf3'
					AND SUBSTR(comprobante_det.cuenta,1,$tam) >= '$_POST[cuenta1]' AND SUBSTR(comprobante_det.cuenta,1,$tam) <='$_POST[cuenta2]'
					$busqueda
					GROUP BY SUBSTR(comprobante_det.cuenta,1,$tam)
					ORDER BY comprobante_det.cuenta";

					$res = mysqli_query($linkbd_v7, $sqlr3);
					while ($row =mysqli_fetch_row($res)) 
					{
						$pctasb["$row[0]"][0]=$row[0];
						$pctasb["$row[0]"][2]+=$row[1]; 
					} 
					
				}

			
				
				for ($y=0;$y<$_POST['nivel'];$y++)
				{
					$lonc=count($pctasb);
					//foreach($pctasb as $k => $valores )
					$k=0;
					// echo "lonc:".$lonc;
					//   while($k<$lonc)
					foreach($pctasb as $k => $valores )
					{
						if (strlen($pctasb[$k][0])>=$niveles[$y-1])
						{		 
							$ncuenta=substr($pctasb[$k][0],0,$niveles[$y-1]);
							if($ncuenta!='')
							{
								$pctasb["$ncuenta"][0]=$ncuenta;
								$pctasb["$ncuenta"][1]+=$pctasb[$k][1];
								$pctasb["$ncuenta"][2]+=$pctasb[$k][2];
								//echo "<br>N:".$niveles[$y-1]." : cuenta:".$k." NC:".$ncuenta."  ".$pctasb["$ncuenta"][1]."  ".$pctasb["$ncuenta"][2]."  ".$pctasb["$ncuenta"][3];	
							}
						}			
						$k++;
					}
				}
				$sqlr="create  temporary table usr_session (id int(11),cuenta varchar(20),nombrecuenta varchar(100),saldofinal double, saldofinalant double)";
				mysqli_query($linkbd_v7, $sqlr);
				$i=1;
				foreach($pctasb as $k => $valores )
				{	 
					if(($pctasb[$k][1]<0 || $pctasb[$k][1]>0))
					{
						$saldofinal = $pctasb[$k][1];
						$nomc = existecuentanicsp($pctasb[$k][0]);
						/*if($nomc=='')
						{
							$nomc=existecuentanicsp($pctasb[$k][0]);
						}*/
						$sqlr="insert into usr_session (id,cuenta,nombrecuenta,saldofinal, saldofinalant) values($i,'".$pctasb[$k][0]."','".$nomc."',".$saldofinal.", ".$pctasb[$k][2].")";
						mysqli_query($linkbd_v7, $sqlr);
						//echo "<br>".$sqlr;
						$i+=1;
					}
					//echo "<br>cuenta:".$k."  ".$pctasb[$k][1]."  ".$pctasb[$k][2]."  ".$pctasb[$k][3];	
				}
				$sqlr = "select *from usr_session order by cuenta";
				$res = mysqli_query($linkbd_v7, $sqlr);
				$_POST['tsaldofinal']=0;
				$_POST['tsaldofinalant']=0;
				$namearch="archivos/".$_SESSION['usuario']."balanceprueba-nivel$_POST[nivel].csv";
				$Descriptor1 = fopen($namearch,"w+"); 
				fputs($Descriptor1,"CODIGO;CUENTA;SALDO ANTERIOR;DEBITO;CREDITO;SALDO FINAL\r\n");
				$co='zebra1';
				$co2='zebra2';
  				while($row = mysqli_fetch_row($res))
  				{
					$negrilla="style='font-weight:bold'";
					$puntero="";
					$dobleclick="";
					
					if($niveles[$_POST['nivel']-1]==strlen($row[1]))
					{
						$negrilla=" ";  
						$puntero="style=\"cursor: hand\" ";
						$dobleclick="ondblclick='direccionaCuentaGastos(this)'";		  
						$_POST['tsaldofinal']+=$row[3];	
						$_POST['tsaldofinalant']+=$row[4];			  	
					}
					//echo $cursor."<br>";
					echo "<tr class='$co' $puntero text-rendering: optimizeLegibility; $dobleclick>
							<td $negrilla >$row[1]</td>
							<td $negrilla>$row[2]</td>
							<td $negrilla align='right'>".number_format($row[3],2,".",",")."</td>
							<td $negrilla align='right'>".number_format($row[4],2,".",",")."";
							echo "<input type='hidden' name='dcuentas[]' value= '".$row[1]."'> 
							<input type='hidden' name='dncuentas[]' value= '".$row[2]."'>
							<input type='hidden' name='dsaldo[]' value= '".number_format($row[3],2,",",".")."'><input type='hidden' name='dsaldoant[]' value= '".number_format($row[4],2,",",".")."'></td>
						</tr>";
					fputs($Descriptor1,$row[1].";".$row[2].";".number_format($row[3],3,",","").";".number_format($row[4],3,",","")."\r\n");
					$aux=$co;
					$co=$co2;
					$co2=$aux;
					$i=1+$i;
				}
				fclose($Descriptor1);
				echo "<tr class='$co'>
						<td colspan='2'></td>
						<td class='$co' align='right'>".number_format($_POST['tsaldofinal'],2,".",",")."<input type='hidden' name='tsaldofinal' value= '$_POST[tsaldofinal]'></td>
						<td class='$co' align='right'>".number_format($_POST['tsaldofinalant'],2,".",",")."<input type='hidden' name='tsaldofinalant' value= '$_POST[tsaldofinalant]'></td>
						</tr>";  
				$horafin=date('h:i:s');	
				echo "<DIV class='ejemplo'>INICIO:$horaini FINALIZO: $horafin</DIV>";
			}
		?> 
	</div>
     <div id="bgventanamodal2">
                <div id="ventanamodal2">
                    <IFRAME  src="" name="buster" marginWidth=0 marginHeight=0 frameBorder=0 id="ventana2" frameSpacing=0 style="left:500px; width:900px; height:500px; top:200;"> 
                    </IFRAME>
                </div>
       	 	</div>
</form>
<script type="text/javascript">
		let ftr = document.getElementById('filtros').value;
		
			jQuery(function($)
			{
				if(jQuery)
				{
					var countChecked = function() 
					{
						var texto="";
						$( "input[name=tipocc]:checked" ).each(
							function(){texto+=($(this).attr('class'))+"-";}
						);
						if(texto==''){$( "#texto" ).text("Selecciona...");}
						else
						{
							$( "#texto" ).text(texto.substring(0,texto.length-1));
							$('input[name=filtros]').val(texto.substring(0,texto.length-1));
						}
					};
					countChecked();
					$( "input[name=tipocc][type=checkbox]" ).on( "click", countChecked );

					if(ftr){
						let texto=ftr;
						$( "#texto" ).text(texto.substring(0,texto.length));
					}
					console.log('filtros: ' + ftr);
					
				}
			});	

			$(document).on("click",function(e) {
				
				var container = $("#multiselct");
				
				if (!container.is(e.target) && container.has(e.target).length === 0) { 
				//Se ha pulsado en cualquier lado fuera de los elementos contenidos en la variable container
					$('#checkboxes',this).css('visibility', 'hidden');

					//Fuente: https://www.iteramos.com/pregunta/18306/haga-clic-fuera-de-menu-para-cerrar-en-jquery  

				}else{
					$('#checkboxes',this).css('visibility', 'visible');
				}
			});
		</script>
	</body>
</html>