<?php
	require('comun.inc');
	require('fpdf.php');
	require "funciones.inc";
	session_start();
	require_once("tcpdf/tcpdf_include.php");
	//require_once('barras/tcpdf_include.php');
	date_default_timezone_set("America/Bogota");
	class MYPDF extends TCPDF 
	{
		//Cabecera de página
		public function Header() 
		{
			$linkbd = conectar_v7();
			$linkbd -> set_charset("utf8");
			$sqlr="select *from configbasica where estado='S'";
			$res=mysqli_query($linkbd,$sqlr);
			while($row=mysqli_fetch_row($res))
			{
				$nit=$row[0];
				$rs=$row[1];
			}
			//Parte Izquierda
			$this->Image('imagenes/eng.jpg',15,10,25,25);
			$this->SetFont('dejavusans','B',18);
			$this->SetY(10);
			$this->Cell(190,5,''.$rs,0,0,'C'); 
			//*****************************************************************************************************************************
			$this->SetFont('dejavusans','B',12);
			$this->SetY(10);
			$this->Cell(190,20,'SECRETARÍA DE HACIENDA MUNICIPAL',0,0,'C'); 
			$this->SetFont('dejavusans','B',10);
			$this->SetY(15);
			$this->Cell(190,20,'DIRECCIÓN DE IMPUESTOS MUNICIPALES',0,0,'C'); 
			$this->SetY(20);
			$this->Cell(190,20,'RECIBO DE COBRO',0,0,'C'); 
			$this->SetY(25);
			$this->Cell(190,20,'IMPUESTO PREDIAL UNIFICADO',0,0,'C'); 
			//************************************
			$this->SetY(28);
			$this->Cell(180,20,'Liquidación No. '.$_POST['idpredial'],0,0,'R'); 
			
	//************************	***********************************************************************************************************
		}
		//Pie de pagina
		public function Footer() {
			$linkbd = conectar_v7();
			$linkbd -> set_charset("utf8");
			$sqlr="SELECT direccion,telefono,web,email FROM configbasica WHERE estado='S'";
			$resp=mysqli_query($linkbd, $sqlr);
			$user = $_SESSION['nickusu'];	
			$fecha = date("Y-m-d H:i:s");
			$ip = $_SERVER['REMOTE_ADDR'];
			$useri = $_POST['user'];
			while($row=mysqli_fetch_row($resp))
			{
				$direcc=strtoupper($row[0]);
				$telefonos=$row[1];
				$dirweb=strtoupper($row[3]);
				$coemail=strtoupper($row[2]);
			}
			if($direcc!=''){$vardirec="Dirección: $direcc, ";}
			else {$vardirec="";}
			if($telefonos!=''){$vartelef="Telefonos: $telefonos";}
			else{$vartelef="";}
			if($dirweb!=''){$varemail="Email: $dirweb, ";}
			else {$varemail="";}
			if($coemail!=''){$varpagiw="Pagina Web: $coemail";}
			else{$varpagiw="";}
			
			$txt = <<<EOD
			$vardirec $vartelef
			$varemail $varpagiw
			EOD;
			$this->SetFont('helvetica', 'I', 5);
			$this->Cell(190,7,'','T',0,'T');
			$this->ln(1);
			$this->Write(0, $txt , '', 0, 'C', true, 0, false, false, 0);
			//$this->Cell(12, 7, 'Hecho por: '.$useri, 0, false, 'C', 0, '', 0, false, 'T', 'M');
			$this->Cell(15, 7, 'Impreso por: '.$user, 0, false, 'C', 0, '', 0, false, 'T', 'M');
			$this->Cell(57, 7, 'IP: '.$ip, 0, false, 'C', 0, '', 0, false, 'T', 'M');
			$this->Cell(50, 7, 'Fecha: '.$fecha, 0, false, 'C', 0, '', 0, false, 'T', 'M');
			$this->Cell(54, 7, 'IDEAL.10 S.A.S', 0, false, 'C', 0, '', 0, false, 'T', 'M');
			$this->Cell(25, 7, 'Pagina '.$this->getAliasNumPage().'/'.$this->getAliasNbPages(), 0, false, 'C', 0, '', 0, false, 'T', 'M');
		}
	}
	$linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");
	$sqlr="select *from configbasica where estado='S'";
	$res=mysqli_query($linkbd,$sqlr);
	while($row=mysqli_fetch_row($res)){
		$nit=$row[0];
		$rs=$row[1];
	}

	$fecha = '';
	$var = count($_POST['dvigencias']);

	if ($var > 1) {
		$fecha = $_POST['fecha'];
	}
	else {
		$vigencia = $_POST['vigencia'];

		$sqlFecha = "SELECT fechafin1, fechafin2, fechafin3 FROM tesodescuentoincentivo WHERE vigencia = '$vigencia'";
		$rowFecha = mysqli_fetch_row(mysqli_query($linkbd, $sqlFecha));

		$a = $_POST['fechaprueba'];
		$b = $rowFecha[0];
		$c = $rowFecha[1];
		$d = $rowFecha[2];

		if ($a <= $b) {
			preg_match("/([0-9]{4})\-([0-9]{2})\-([0-9]{2})/", $b, $f);
			$fecha="$f[3]/$f[2]/$f[1]";
		}
		else if ($a <= $c) {
			preg_match("/([0-9]{4})\-([0-9]{2})\-([0-9]{2})/", $c, $f);
			$fecha="$f[3]/$f[2]/$f[1]";
		}
		else if ($a <= $d) {
			preg_match("/([0-9]{4})\-([0-9]{2})\-([0-9]{2})/", $d, $f);
			$fecha="$f[3]/$f[2]/$f[1]";
		}
		else {
			preg_match("/([0-9]{4})\-([0-9]{2})\-([0-9]{2})/", $a, $f);
			$fecha="$f[3]/$f[2]/$f[1]";
		}
	}

	$tam=strlen($_POST['idpredial']);
	$ceros='';
	for($i=$tam;$i<9;$i++){
		$ceros.='0';
	}
	$ean=$ceros.$_POST['idpredial'].'001';

	//Creaci�n del objeto de la clase heredada
	//$pdf=new PDF('P','mm',array(210,140));
	$pdf = new MYPDF('P', 'mm', 'A4', true, 'UTF-8', false);
	//$pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
	//$pdf = new MYPDF('P','mm','Letter', true, 'iso-8859-1', false);// create new PDF document
	$pdf->SetFooterMargin(15);
	// set auto page breaks
	$pdf->SetAutoPageBreak(TRUE, 10);

	// set a barcode on the page footer
	$pdf->setBarcode(date('Y-m-d H:i:s'));

	// define barcode style
	$style = array(
		'position' => 'C',
		'align' => 'C',
		'stretch' => false,
		'fitwidth' => true,
		'cellfitalign' => '',
		'border' => false,
		'hpadding' => 'auto',
		'vpadding' => 'auto',
		'fgcolor' => array(0,0,0),
		'bgcolor' => false, //array(255,255,255),
		'text' => false,
		'font' => 'helvetica',
		'fontsize' => 8,
		'stretchtext' => 1
	);
	$pdf->AddPage();

//********************************************************************************************************************************
	$linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");
	$sqlr="SELECT codigo, codini FROM codigosbarras WHERE estado='S' AND tipo='01'";
	$res=mysqli_query($linkbd,$sqlr);
	$row=mysqli_fetch_row($res);
	$fecdi=explode('/', $fecha);
	$codini = $row[1];
	//$cod01="7709998803862";	//codigo GS1 Asignado
	$cod01=$row[0];//codigo GS1 Asignado
	$cod02="01"; //codigo tipo recaudo
	$cod03=str_pad($_POST['idpredial'],7,"0", STR_PAD_LEFT);//Numero Liquidacion
	//$cod04="$fecdi[0]$fecdi[1]".substr($fecdi[2],-2);//fecha de Liquidacion
	$cod04=""; 
	$cod05=str_pad("11100501",9,"0", STR_PAD_LEFT);//codigo cuenta bancaria
	//$cod05="";
	$cod06=str_pad($_POST['totliquida'],10,"0", STR_PAD_LEFT);//total a pagar
	$cod07="$fecdi[2]$fecdi[1]$fecdi[0]";//fecha limite
	$codtotn="($codini)$cod01(8020)$cod02$cod03$cod04$cod05(3900)$cod06(96)$cod07";
	$codtot=chr(241).$codini.$cod01."8020"."$cod02$cod03$cod04$cod05".chr(241)."3900".$cod06.chr(241)."96".$cod07;

	$pdf->ln(5);
	$pdf->RoundedRect(10, 41, 190, 24, 1.2, '1111', '');
	$pdf->SetFont('dejavusans','',6);
	$pdf->SetY(42);
	$pdf->Cell(50,4,'CÉDULA CATASTRAL',0,0,'L');
	$pdf->Line(58,41,58,53);
	$pdf->Cell(90,4,'DIRECCIÓN',0,0,'L');
	$pdf->Line(148,41,148,53);
	$pdf->Cell(50,4,'VEREDA',0,0,'L');
	$pdf->SetY(47);
	$pdf->Cell(50,4,$_POST['catastral'],0,0,'L');
	$pdf->Cell(90,4,substr(strtoupper($_POST['direccion']),0,80),0,0,'L');
	$pdf->Cell(50,4,$_POST['vereda'],0,0,'L');
	$pdf->Line(10,53,200,53);
	$pdf->SetY(54);
	$pdf->Cell(80,4,'NOMBRE',0,0,'L');
	$pdf->Line(88,53,88,65);
	$pdf->Cell(30,4,'CÉDULA / NIT',0,0,'L');
	$pdf->Line(118,53,118,65);
	$pdf->Cell(10,4,'HA',0,0,'L');
	$pdf->Line(128,53,128,65);
	$pdf->Cell(10,4,'M2',0,0,'L');
	$pdf->Line(138,53,138,65);
	$pdf->Cell(10,4,'AC',0,0,'L');
	$pdf->Line(148,53,148,65);
	$pdf->Cell(50,4,'FECHA DE LIQUIDACIÓN',0,0,'L');
	$pdf->SetY(59);
	$pdf->Cell(80,4,substr(strtoupper($_POST['ntercero']),0,50),0,0,'L');
	$pdf->Cell(30,4,$_POST['tercero'],0,0,'L');
	$pdf->Cell(8,4,$_POST['ha'],0,0,'L');
	$pdf->Cell(10,4,$_POST['mt2'],0,0,'L');
	$pdf->Cell(10,4,$_POST['areac'],0,0,'L');

	$pdf->Cell(50,4,$_POST['fecha'],0,0,'L');

	//detalle
	$pdf->SetFont('dejavusans','',6);
	$pdf->RoundedRect(10, 67, 190, 60, 1.2, '1111', '');
	$pdf->SetY(67.5);
	$pdf->SetX(10.6);
	$pdf->SetFillColor(150,150,150);
	$pdf->SetTextColor(255,255,255);
	$pdf->Cell(189,5,'',0,0,'C',1);
	$avaluoi=0;
	$tasai=0;
	$impuestoi=0;
	//HORIZONTAL
	$pdf->Line(10,73,200,73);
	if(count($_POST['dvalorAlumbrado'])>0){
		//VERTICAL
		$pdf->Line(20,73,20,127);
		$pdf->Line(35,73,35,127);
		$pdf->Line(60,73,60,127);
		$avaluoi=21;
		$pdf->Line(70,73,70,127);
		$tasai=18;
		$pdf->Line(90,73,90,127);
		$impuestoi=12;
		$pdf->Line(105,73,105,127);
		$interesi = 10;
		$pdf->Line(122,73,122,127);
		$impuestoAmbi=20;
		$pdf->Line(135,73,135,127);
		$interesesAmbi=10;
		$pdf->Line(150,73,150,127);
		$bombei=17;
		$pdf->Line(166,73,166,127);
		$descuentoi=14;
		$pdf->Line(180,73,180,127);
		$totali=15;
		$pdf->SetY(68);
		$pdf->Cell(10,4,'AÑO',0,0,'C');
		$pdf->Cell(15,4,'CONCEPTO',0,0,'C');
		$pdf->Cell(25,4,'AVALÚO',0,0,'C');
		$pdf->Cell(10,4,'TASA',0,0,'C');
		$pdf->Cell(20,4,'IMPUESTO',0,0,'C');
		$pdf->Cell(15,4,'INTERESES',0,0,'C');
		$pdf->Cell(15,4,'SOBRETASA',0,0,'C');
		$pdf->Cell(15,4,'INT/SOBRET',0,0,'C');
		$pdf->Cell(15,4,'BOMBEROS',0,0,'C');
		$pdf->Cell(20,4,'ALUMBRADO PUB.',0,0,'C');
		$pdf->Cell(14,4,'DESCUENTO',0,0,'C');
		$pdf->Cell(20,4,'VALOR TOTAL',0,0,'C');
	}else{
		$pdf->Line(20,73,20,127);
		$pdf->Line(35,73,35,127);
		$pdf->Line(60,73,60,127);
		$avaluoi=23;
		$pdf->Line(70,73,70,127);
		$tasai=10;
		$pdf->Line(95,73,95,127);
		$impuestoi=23;
		$pdf->Line(111,73,111,127);
		$interesi = 15;
		$pdf->Line(127,73,127,127);
		$impuestoAmbi = 15;
		$pdf->Line(143,73,143,127);
		$interesesAmbi=15;
		$pdf->Line(159,73,159,127);
		$bombei=15;
		$pdf->Line(175,73,175,127);
		$descuentoi=15;
		$totali=23;
		$pdf->SetY(68);
		$pdf->Cell(10,4,'AÑO',0,0,'C');
		$pdf->Cell(15,4,'CONCEPTO',0,0,'C');
		$pdf->Cell(25,4,'AVALÚO',0,0,'C');
		$pdf->Cell(10,4,'TASA',0,0,'C');
		$pdf->Cell(25,4,'IMPUESTO',0,0,'C');
		$pdf->Cell(16,4,'INTERESES',0,0,'C');
		$pdf->Cell(16,4,'SOBRETASA',0,0,'C');
		$pdf->Cell(16,4,'INT/SOBRET',0,0,'C');
		$pdf->Cell(16,4,'BOMBEROS',0,0,'C');
		$pdf->Cell(16,4,'DESCUENTO',0,0,'C');
		$pdf->Cell(25,4,'VALOR TOTAL',0,0,'C');
	}
	$pdf->SetTextColor(0,0,0);
	$pdf->SetY(74);
	if(count($_POST['dselvigencias']) > 10){
		$banderay = 10;
	}else{
		$banderay = count($_POST['dselvigencias']);
	}
	for($x=0; $x<$banderay; $x++){	
		$cont=0;
		while($cont<count($_POST['dvigencias'])){
			if($_POST['dvigencias'][$cont]==$_POST['dselvigencias'][$x]){
				$interes=$_POST['dinteres1'][$cont]+$_POST['dipredial'][$cont];
				$pdf->Cell(10,4,$_POST['dvigencias'][$cont],0,0,'C');
				$pdf->Cell(15,4,'PREDIAL',0,0,'L');
				$pdf->Cell($avaluoi,4,number_format($_POST['dvaloravaluo'][$cont],2,',','.'),0,0,'R');
				$pdf->Cell(2,4,'',0,0,'R');
				$pdf->Cell($tasai,4,''.$_POST['dtasavig'][$cont].' xmil',0,0,'C');
				$pdf->Cell($impuestoi,4,''.number_format($_POST['dpredial'][$cont],2,',','.'),0,0,'R');
				$pdf->Cell(2,4,'',0,0,'R');
				$pdf->Cell($interesi,4,''.number_format($interes,2,',','.'),0,0,'R');
				$pdf->Cell(1,4,'',0,0,'R');
				$pdf->Cell($impuestoAmbi,4,''.number_format($_POST['dimpuesto2'][$cont],2,',','.'),0,0,'R');
				$pdf->Cell(1,4,'',0,0,'R');
				$pdf->Cell($interesesAmbi,4,''.number_format($_POST['dinteres2'][$cont],2,',','.'),0,0,'R');
				$pdf->Cell(1,4,'',0,0,'R');
				$pdf->Cell($bombei,4,''.number_format($_POST['dimpuesto1'][$cont],2,',','.'),0,0,'R');
				$pdf->Cell(1,4,'',0,0,'R');
				if(count($_POST['dvalorAlumbrado'])>0){
					$pdf->Cell(15,4,''.number_format($_POST['dvalorAlumbrado'][$cont],2,',','.'),0,0,'R');
					$pdf->Cell(1,4,'',0,0,'R');
				}
				$pdf->Cell($descuentoi,4,''.number_format($_POST['ddescipredial'][$cont],2,',','.'),0,0,'R');
				$pdf->Cell(1,4,'',0,0,'R');
				$pdf->Cell($totali,4,''.number_format($_POST['dhavaluos'][$x],2,',','.'),0,0,'R');
				$pdf->Cell(2,4,'',0,1,'R');
			}
			$cont=$cont +1;
		}
	}
	$pdf->RoundedRect(130, 129, 70, 15, 1.2, '1111', '');
	$pdf->SetY(129.5);
	$pdf->SetX(130.6);
	$pdf->SetFillColor(150,150,150);
	//$pdf->SetTextColor(255,255,255);
	$pdf->Cell(25,14,'',0,0,'C',1);

	$pdf->SetY(130);
	$pdf->SetX(160);
	$pdf->SetFont('dejavusans','',6);
	$pdf->Cell(15,4,'FECHA',0,0,'C');
	$pdf->Cell(25,4,'VALOR',0,0,'C');
	$pdf->SetY(136);
	$pdf->SetX(160);

	$pdf->SetFont('dejavusans','',7);
	$pdf->Cell(15,4,$fecha,0,0,'C');
	$pdf->Cell(25,4,number_format($_POST['totliquida'],2,',','.'),0,0,'R');	

	$pdf->SetY(132);
	$pdf->SetX(135);
	$pdf->SetFont('dejavusans','B',10);
	$pdf->SetTextColor(255,255,255);
	$pdf->Cell(15,4,'PAGUESE',0,0,'C');
	$pdf->SetY(137);
	$pdf->SetX(135);
	$pdf->Cell(15,4,'HASTA',0,0,'C');
	$pdf->ln(8);
	$pdf->SetTextColor(0,0,0);
	$pdf->SetFont('dejavusans','',7);
	$pdf->multicell(190,4,'Contra la presente liquidacion procede el recurso de reconsideracion dentro de los dos (2) meses siguientes a su notificacion',0);

	$sqlr = "SELECT cuenta, nombre FROM bancopredial";
	$res = mysqli_query($linkbd, $sqlr);
	$row = mysqli_fetch_row($res);
	if($row[0] != '')
	{
		$pdf->SetTextColor(0,0,0);
		$pdf->SetFont('dejavusans','',7);
		$pdf->Cell(190,4,'CUENTA CORRIENTE: '.$row[0].' - '.$row[1].'',0,1,'C');
	}
	$pdf->ln(3);
	$pdf->Cell(190,4,'Copia Contribuyente',0,1,'C');

	if(count($_POST['dvigencias'])>1)
		$periodo=$_POST['dvigencias'][0].' - '.$_POST['dvigencias'][(count($_POST['dvigencias'])-1)];
	else
		$periodo=$_POST['dvigencias'][0];
	//*****************************************************************************************************************************
	$pdf->SetFont('dejavusans','',6);
	$pdf->SetY(162);
	$pdf->Cell(190,5,$rs,0,0,'C'); 
	$pdf->SetY(165);
	$pdf->Cell(190,5,'DIRECCIÓN DE IMPUESTOS MUNICIPALES',0,0,'C'); 
	$pdf->SetY(168);
	$pdf->Cell(190,5,'TESORERÍA MUNICIPAL',0,0,'C'); 
	$pdf->RoundedRect(10, 172, 190, 8, 1.2, '1111', '');
	$pdf->SetFont('dejavusans','',6);
	$pdf->SetY(173);
	$pdf->Cell(70,4,'CÉDULA CATASTRAL',0,0,'L');
	$pdf->Line(78,172,78,180);
	$pdf->Cell(60,4,'PERÍODO FACTURADO',0,0,'L');
	$pdf->Line(138,172,138,180);
	$pdf->Cell(60,4,'LIQUIDACIÓN No.',0,0,'L');
	$pdf->SetY(176);
	$pdf->Cell(70,4,$_POST['catastral'],0,0,'L');
	$pdf->Cell(60,4,$periodo,0,0,'L');
	$pdf->Cell(60,4,$_POST['idpredial'],0,0,'L');
	$pdf->RoundedRect(80, 181, 120, 8, 1.2, '1111', '');
	$pdf->SetFont('dejavusans','',6);
	$pdf->SetY(182);
	$pdf->SetX(81);
	$pdf->Cell(40,4,'PÁGUESE',0,0,'C');
	$pdf->Line(118,181,118,189);
	$pdf->Cell(40,4,'FECHA',0,0,'C');
	$pdf->Line(158,181,158,189);
	$pdf->Cell(40,4,'VALOR',0,0,'C');
	$pdf->SetY(185);
	$pdf->SetX(81);
	$pdf->Cell(40,4,'HASTA',0,0,'C');
	$pdf->Cell(40,4,$fecha,0,0,'C');
	$pdf->Cell(40,4,number_format($_POST['totliquida'],2,',','.'),0,0,'C');
	//*****************************************************************************************************************************
	$pdf->SetLineStyle(array('width' => 0.5, 'cap' => 'butt', 'join' => 'miter', 'dash' => 4, 'color' => array(0, 0, 0)));
	$pdf->ln(4);
	$posy=$pdf->GetY();
	$pdf->write1DBarcode($codtot, 'C128', '', '', 160, 18, 0.25, $style, 'N');
	$pdf->Cell(190,2,$codtotn,0,1,'C',FALSE);
	$pdf->SetFont('dejavusans','',6);
	$pdf->Cell(190,2,'Señor Cajero: Por favor no colocar el sello en el código de barras',0,1,'C');
	$pdf->Cell(190,2,'Copia Banco',0,1,'C');
	$pdf->Line(10,160,200,160);
	//*****************************************************************************************************************************
	$pdf->SetLineStyle(array('width' => 0.1, 'cap' => 'butt', 'join' => 'miter', 'dash' => 0, 'color' => array(0, 0, 0)));
	$pdf->SetFont('dejavusans','',6);
	$pdf->SetY(218);
	$pdf->Cell(190,5,$rs,0,0,'C'); 
	$pdf->SetY(221);
	$pdf->Cell(190,5,'DIRECCIÓN DE IMPUESTOS MUNICIPALES',0,0,'C'); 
	$pdf->SetY(223);
	$pdf->Cell(190,5,'TESORERÍA MUNICIPAL',0,0,'C'); 
	$pdf->RoundedRect(10, 228, 190, 8, 1.2, '1111', '');
	$pdf->SetFont('dejavusans','',6);
	$pdf->SetY(229);
	$pdf->Cell(70,4,'CÉDULA CATASTRAL',0,0,'L');
	$pdf->Line(78,228,78,236);
	$pdf->Cell(60,4,'PERÍODO FACTURADO',0,0,'L');
	$pdf->Line(138,228,138,236);
	$pdf->Cell(60,4,'LIQUIDACIÓN No.',0,0,'L');
	$pdf->SetY(232);
	$pdf->Cell(70,4,$_POST['catastral'],0,0,'L');
	$pdf->Cell(60,4,$periodo,0,0,'L');
	$pdf->Cell(60,4,$_POST['idpredial'],0,0,'L');
	$pdf->RoundedRect(80, 238, 120, 8, 1.2, '1111', '');
	$pdf->SetFont('dejavusans','',6);
	$pdf->SetY(239);
	$pdf->SetX(81);
	$pdf->Cell(40,4,'PÁGUESE',0,0,'C');
	$pdf->Line(118,238,118,246);
	$pdf->Cell(40,4,'FECHA',0,0,'C');
	$pdf->Line(158,238,158,246);
	$pdf->Cell(40,4,'VALOR',0,0,'C');
	$pdf->SetY(242);
	$pdf->SetX(81);
	$pdf->Cell(40,4,'HASTA',0,0,'C');
	$pdf->Cell(40,4,$fecha,0,0,'C');
	$pdf->Cell(40,4,number_format($_POST['totliquida'],2,',','.'),0,0,'C');
	//*****************************************************************************************************************************
	$pdf->SetLineStyle(array('width' => 0.5, 'cap' => 'butt', 'join' => 'miter', 'dash' => 4, 'color' => array(0, 0, 0)));
	$pdf->Line(10,215,200,215);
	$pdf->ln(5);
	$pdf->write1DBarcode($codtot,'C128', '', '', 160, 18, 0.25, $style, 'N');
	$pdf->Cell(190,2,$codtotn,0,1,'C',FALSE);
	$pdf->SetFont('dejavusans','',6);
	$pdf->Cell(190,2,'Señor Cajero: Por favor no colocar el sello en el código de barras',0,1,'C');
	$pdf->Cell(190,2,'Copia Tesorería',0,1,'C');
	//*****************************************************************************************************************************
	if(count($_POST['dselvigencias']) > 10){
		$pdf->AddPage();
		$pdf->SetLineStyle(array('width' => 0, 'cap' => 'butt', 'join' => 'miter', 'dash' => 0, 'color' => array(0, 0, 0)));
		$pdf->ln(5);
		$pdf->RoundedRect(10, 41, 190, 24, 1.2, '1111', '');
		$pdf->SetFont('dejavusans','',8);
		$pdf->SetY(42);
		$pdf->Cell(50,4,'CÉDULA CATASTRAL',0,0,'L');
		$pdf->Line(58,41,58,53);
		$pdf->Cell(90,4,'DIRECCIÓN',0,0,'L');
		$pdf->Line(148,41,148,53);
		$pdf->Cell(50,4,'VEREDA',0,0,'L');
		$pdf->SetY(47);
		$pdf->Cell(50,4,$_POST['catastral'],0,0,'L');
		$pdf->Cell(90,4,substr(strtoupper($_POST['direccion']),0,80),0,0,'L');
		$pdf->Cell(50,4,$_POST['vereda'],0,0,'L');
		$pdf->Line(10,53,200,53);
		$pdf->SetY(54);
		$pdf->Cell(80,4,'NOMBRE',0,0,'L');
		$pdf->Line(88,53,88,65);
		$pdf->Cell(30,4,'CÉDULA / NIT',0,0,'L');
		$pdf->Line(118,53,118,65);
		$pdf->Cell(10,4,'HA',0,0,'L');
		$pdf->Line(128,53,128,65);
		$pdf->Cell(10,4,'M2',0,0,'L');
		$pdf->Line(138,53,138,65);
		$pdf->Cell(10,4,'AC',0,0,'L');
		$pdf->Line(148,53,148,65);
		$pdf->Cell(50,4,'FECHA DE LIQUIDACIÓN',0,0,'L');
		$pdf->SetY(59);
		$pdf->Cell(80,4,substr(strtoupper($_POST['ntercero']),0,50),0,0,'L');
		$pdf->Cell(30,4,$_POST['tercero'],0,0,'L');
		$pdf->Cell(10,4,$_POST['ha'],0,0,'L');
		$pdf->Cell(10,4,$_POST['mt2'],0,0,'L');
		$pdf->Cell(10,4,$_POST['areac'],0,0,'L');
		$pdf->Cell(50,4,$_POST['fecha'],0,0,'L');
		//detalle
		$pdf->SetFont('dejavusans','',6);
		$pdf->RoundedRect(10, 67, 190, 132, 1.2, '1111', '');
		$pdf->SetY(67.5);
		$pdf->SetX(10.6);
		$pdf->SetFillColor(150,150,150);
		$pdf->SetTextColor(255,255,255);
		$pdf->Cell(189,5,'',0,0,'C',1);
		$avaluoi=0;
		$tasai=0;
		$impuestoi=0;
		//HORIZONTAL
		$pdf->Line(10,73,200,73);
		if(count($_POST['dvalorAlumbrado'])>0){
			//VERTICAL
			$pdf->Line(20,73,20,200);
			$pdf->Line(35,73,35,200);
			$pdf->Line(60,73,60,200);
			$avaluoi=21;
			$pdf->Line(70,73,70,200);
			$tasai=18;
			$pdf->Line(90,73,90,200);
			$impuestoi=12;
			$pdf->Line(105,73,105,200);
			$interesi = 10;
			$pdf->Line(122,73,122,200);
			$impuestoAmbi=20;
			$pdf->Line(135,73,135,200);
			$interesesAmbi=10;
			$pdf->Line(150,73,150,200);
			$bombei=17;
			$pdf->Line(166,73,166,200);
			$descuentoi=14;
			$pdf->Line(180,73,180,200);
			$totali=15;
			$pdf->SetY(68);
			$pdf->Cell(10,4,'AÑO',0,0,'C');
			$pdf->Cell(15,4,'CONCEPTO',0,0,'C');
			$pdf->Cell(25,4,'AVALÚO',0,0,'C');
			$pdf->Cell(10,4,'TASA',0,0,'C');
			$pdf->Cell(20,4,'IMPUESTO',0,0,'C');
			$pdf->Cell(15,4,'INTERESES',0,0,'C');
			$pdf->Cell(15,4,'SOBRETASA',0,0,'C');
			$pdf->Cell(15,4,'INT/SOBRET',0,0,'C');
			$pdf->Cell(15,4,'BOMBEROS',0,0,'C');
			$pdf->Cell(20,4,'ALUMBRADO PUB.',0,0,'C');
			$pdf->Cell(14,4,'DESCUENTO',0,0,'C');
			$pdf->Cell(20,4,'VALOR TOTAL',0,0,'C');
		}else{
			$pdf->Line(20,73,20,200);
			$pdf->Line(35,73,35,200);
			$pdf->Line(60,73,60,200);
			$avaluoi=23;
			$pdf->Line(70,73,70,200);
			$tasai=10;
			$pdf->Line(95,73,95,200);
			$impuestoi=23;
			$pdf->Line(111,73,111,200);
			$interesi = 15;
			$pdf->Line(127,73,127,200);
			$impuestoAmbi = 15;
			$pdf->Line(143,73,143,200);
			$interesesAmbi=15;
			$pdf->Line(159,73,159,200);
			$bombei=15;
			$pdf->Line(175,73,175,200);
			$descuentoi=15;
			$totali=23;
			$pdf->SetY(68);
			$pdf->Cell(10,4,'AÑO',0,0,'C');
			$pdf->Cell(15,4,'CONCEPTO',0,0,'C');
			$pdf->Cell(25,4,'AVALÚO',0,0,'C');
			$pdf->Cell(10,4,'TASA',0,0,'C');
			$pdf->Cell(25,4,'IMPUESTO',0,0,'C');
			$pdf->Cell(16,4,'INTERESES',0,0,'C');
			$pdf->Cell(16,4,'SOBRETASA',0,0,'C');
			$pdf->Cell(16,4,'INT/SOBRET',0,0,'C');
			$pdf->Cell(16,4,'BOMBEROS',0,0,'C');
			$pdf->Cell(16,4,'DESCUENTO',0,0,'C');
			$pdf->Cell(25,4,'VALOR TOTAL',0,0,'C');
		}
		$pdf->SetTextColor(0,0,0);
		$pdf->SetY(74);
		
		for($x=10; $x<count($_POST['dselvigencias']); $x++){	
			$cont=0;
			while($cont<count($_POST['dvigencias'])){
				if($_POST['dvigencias'][$cont]==$_POST['dselvigencias'][$x]){
					$interes=$_POST['dinteres1'][$cont]+$_POST['dipredial'][$cont];
					$pdf->Cell(10,4,$_POST['dvigencias'][$cont],0,0,'C');
					$pdf->Cell(15,4,'PREDIAL',0,0,'L');
					$pdf->Cell($avaluoi,4,number_format($_POST['dvaloravaluo'][$cont],2,',','.'),0,0,'R');
					$pdf->Cell(2,4,'',0,0,'R');
					$pdf->Cell($tasai,4,''.$_POST['dtasavig'][$cont].' xmil',0,0,'C');
					$pdf->Cell($impuestoi,4,''.number_format($_POST['dpredial'][$cont],2,',','.'),0,0,'R');
					$pdf->Cell(2,4,'',0,0,'R');
					$pdf->Cell($interesi,4,''.number_format($interes,2,',','.'),0,0,'R');
					$pdf->Cell(1,4,'',0,0,'R');
					$pdf->Cell($impuestoAmbi,4,''.number_format($_POST['dimpuesto2'][$cont],2,',','.'),0,0,'R');
					$pdf->Cell(1,4,'',0,0,'R');
					$pdf->Cell($interesesAmbi,4,''.number_format($_POST['dinteres2'][$cont],2,',','.'),0,0,'R');
					$pdf->Cell(1,4,'',0,0,'R');
					$pdf->Cell($bombei,4,''.number_format($_POST['dimpuesto1'][$cont],2,',','.'),0,0,'R');
					$pdf->Cell(1,4,'',0,0,'R');
					if(count($_POST['dvalorAlumbrado'])>0){
						$pdf->Cell(15,4,''.number_format($_POST['dvalorAlumbrado'][$cont],2,',','.'),0,0,'R');
						$pdf->Cell(1,4,'',0,0,'R');
					}
					$pdf->Cell($descuentoi,4,''.number_format($_POST['ddescipredial'][$cont],2,',','.'),0,0,'R');
					$pdf->Cell(1,4,'',0,0,'R');
					$pdf->Cell($totali,4,''.number_format($_POST['dhavaluos'][$x],2,',','.'),0,0,'R');
					$pdf->Cell(2,4,'',0,1,'R');
				}
				$cont=$cont +1;
			}
		}
		$pdf->RoundedRect(130, 201, 70, 15, 1.2, '1111', '');
		$pdf->SetY(201.5);
		$pdf->SetX(130.6);
		$pdf->SetFillColor(150,150,150);
		//$pdf->SetTextColor(255,255,255);
		$pdf->Cell(25,14,'',0,0,'C',1);

		$pdf->SetY(202);
		$pdf->SetX(160);
		$pdf->SetFont('dejavusans','',6);
		$pdf->Cell(15,4,'FECHA',0,0,'C');
		$pdf->Cell(25,4,'VALOR',0,0,'C');
		$pdf->SetY(208);
		$pdf->SetX(160);

		$pdf->SetFont('dejavusans','',7);
		$pdf->Cell(15,4,$fecha,0,0,'C');
		$pdf->Cell(25,4,number_format($_POST['totliquida'],2,',','.'),0,0,'R');	

		$pdf->SetY(204);
		$pdf->SetX(135);
		$pdf->SetFont('dejavusans','B',10);
		$pdf->SetTextColor(255,255,255);
		$pdf->Cell(15,4,'PAGUESE',0,0,'C');
		$pdf->SetY(209);
		$pdf->SetX(135);
		$pdf->Cell(15,4,'HASTA',0,0,'C');
		$pdf->ln(8);
		$pdf->SetTextColor(0,0,0);
		$pdf->SetFont('dejavusans','',7);
		$pdf->multicell(190,4,'Contra la presente liquidacion procede el recurso de reconsideracion dentro de los dos (2) meses siguientes a su notificacion',0);

		$sqlr = "SELECT cuenta, nombre FROM bancopredial";
		$res = mysqli_query($linkbd, $sqlr);
		$row = mysqli_fetch_row($res);
		if($row[0] != '')
		{
			$pdf->SetTextColor(0,0,0);
			$pdf->SetFont('dejavusans','',7);
			$pdf->Cell(190,4,'CUENTA CORRIENTE: '.$row[0].' - '.$row[1].'',0,1,'C');
		}
		
		//*****************************************************************************************************************************
		
	}

	$pdf->Output();
?> 