<?php

    header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=utf8");
	require 'comun.inc';
	require 'funciones.inc';
	session_start();
    if(empty($_SESSION)){
        header("location: index.php");
    }
	$linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");
	cargarcodigopag($_GET['codpag'],$_SESSION["nivel"]);
	date_default_timezone_set("America/Bogota");
?>
<!DOCTYPE >
<html lang=es>
	<head>
    <meta charset="utf-8"/>
		<meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1"/>
		<meta name="viewport" content="user-scalable=no">
		<title>:: IDEAL 10 - Parametrización</title>
		<link href="favicon.ico" rel="shortcut icon"/>
		<link href="css/css2z.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
		<link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
		<link href="css/css4.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
        <link href="css/style.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css"/>
        <link rel="stylesheet" href="sweetalert2/dist/sweetalert2.min.css">
        <script src="sweetalert2/dist/sweetalert2.min.js"></script>
		<script src="css/programas.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
	</head>
	<body>
		<header>
			<table>
				<tr><script>barra_imagenes("trans");</script><?php cuadro_titulos();?></tr>
			</table>
		</header>

        <main id="myapp" v-cloak>
            <input type="hidden" value = "3" ref="pageType">
            <div class="loading-container" v-show="isLoading" >
                <p class="text-loading"data-loading="IDEAL 10">IDEAL 10</p>
            </div>
            <nav>
                <table>
                    <tr><?php menu_desplegable("trans");?></tr>
                </table>
                <div class="bg-white group-btn p-1" id="newNavStyle">
                    <button type="button" @click="window.location.href='trans-declaracion-impuesto-crear'" class="btn btn-white btn-primary-hover d-flex justify-between align-items-center">
                        <span>Nuevo</span>
                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 -960 960 960"><path d="M440-440H200v-80h240v-240h80v240h240v80H520v240h-80v-240Z"></path></svg>
                    </button>
                    <button type="button" class="btn btn-white btn-primary-hover d-flex justify-between align-items-center" @click="mypop=window.open('trans-principal.php','',''); mypop.focus();">
                        <span>Nueva ventana</span>
                        <svg class="fill-black group-hover:fill-white w-5 h-5" xmlns="http://www.w3.org/2000/svg"  viewBox="0 -960 960 960" ><path d="M320-120v-80h80v-80H160q-33 0-56.5-23.5T80-360v-400q0-33 23.5-56.5T160-840h640q33 0 56.5 23.5T880-760v400q0 33-23.5 56.5T800-280H560v80h80v80H320ZM160-360h640v-400H160v400Zm0 0v-400 400Z"/></svg>
                    </button>
                    <button type="button" @click="mypop=window.open('trans-declaracion-impuesto-buscar','','');mypop.focus();" class="btn btn-white btn-primary-hover d-flex justify-between align-items-center">
                        <span class="group-hover:text-white">Duplicar pantalla</span>
                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 -960 960 960"><path d="M640-160v-360H160v360h480Zm80-200v-80h80v-360H320v200h-80v-200q0-33 23.5-56.5T320-880h480q33 0 56.5 23.5T880-800v360q0 33-23.5 56.5T800-360h-80ZM160-80q-33 0-56.5-23.5T80-160v-360q0-33 23.5-56.5T160-600h480q33 0 56.5 23.5T720-520v360q0 33-23.5 56.5T640-80H160Zm400-603ZM400-340Z"></path></svg>
                    </button>
                </div>
            </nav>
            <section class="bg-white">
                <div>
                    <h2 class="titulos m-0">Buscar declaracion de impuesto vehicular</h2>
                    <div class="d-flex">
                        <div class="form-control">
                            <label class="form-label" for="">Por página:</label>
                            <select v-model="selectPorPagina">
                                <option value="25">25</option>
                                <option value="50">50</option>
                                <option value="100">100</option>
                                <option value="250">250</option>
                                <option value="500">500</option>
                                <option value="1000">1000</option>
                                <option value="">Todo</option>
                            </select>
                        </div>
                        <div class="form-control">
                            <label class="form-label" for="">Fecha inicial:</label>
                            <input type="date" v-model="strFechaInicial">
                        </div>
                        <div class="form-control">
                            <label class="form-label" for="">Fecha final:</label>
                            <input type="date" v-model="strFechaFinal">
                        </div>
                        <div class="form-control">
                            <label class="form-label" for="">Código:</label>
                            <input type="text" v-model="strCodigo">
                        </div>
                        <div class="form-control">
                            <label class="form-label" for="">Placa:</label>
                            <input type="text" v-model="strPlaca">
                        </div>
                        <div class="form-control">
                            <label class="form-label" for="">CC/NIT:</label>
                            <input type="text" v-model="strDocumento">
                        </div>
                        <div class="form-control justify-between">
                            <div></div>
                            <button type="button" @click="getSearch(1,'buscar')" class="btn btn-primary">Buscar</button>
                        </div>
                    </div>
                </div>
                <div class="table-responsive overflow-auto" style="height:50vh">
                    <table class="table table-hover fw-normal">
                        <thead>
                            <tr class="text-center">
                                <th>Código</th>
                                <th>Fecha Trámite</th>
                                <th>Placa</th>
                                <th>Propietario</th>
                                <th>Nombre</th>
                                <th>Total</th>
                                <th>Estado</th>
                                <th>Opciones</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr v-for="(data,index) in arrData" :key="index" @dblclick="window.location.href='trans-declaracion-impuesto-visualizar?id='+data.id">
                                <td class="text-center">{{data.id}}</td>
                                <td class="text-center">{{data.fecha}}</td>
                                <td class="text-center">{{data.placa}}</td>
                                <td class="text-center">{{data.propietario}}</td>
                                <td>{{data.nombre}}</td>
                                <td class="text-right">{{formatMoney(data.total)}}</td>
                                <td class="text-center">
                                    <div>
                                    <span :class="[data.estado == 'S' ? 'badge-success' : data.estado=='P' ? 'badge-primary' : 'badge-danger']"class="badge">{{ data.estado=="S" ? "Activo" : data.estado=="P" ? "Pagado" : "Anulado"}}</span>
                                    </div>
                                </td>
                                <td>
                                    <div class="d-flex justify-center" v-if="data.estado == 'S'">
                                        <button type="button" @click="anular(data)" class="btn btn-sm btn-danger btn-danger-hover d-flex justify-between align-items-center">
                                            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 -960 960 960"><path d="M280-120q-33 0-56.5-23.5T200-200v-520h-40v-80h200v-40h240v40h200v80h-40v520q0 33-23.5 56.5T680-120H280Zm400-600H280v520h400v-520ZM360-280h80v-360h-80v360Zm160 0h80v-360h-80v360ZM280-720v520-520Z"/></svg>
                                        </button>
                                    </div>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div v-if="arrData.length > 0" class="list-pagination-container">
                    <p>Página {{ intPagina }} de {{ intTotalPaginas }}</p>
                    <ul class="list-pagination">
                        <li v-show="intPagina > 1" @click="getSearch(intPagina = 1,'buscar')"><< </li>
                        <li v-show="intPagina > 1" @click="getSearch(--intPagina,'buscar')"><</li>
                        <li v-for="(pagina,index) in arrBotones" :class="intPagina == pagina ? 'active' : ''" @click="getSearch(pagina,'buscar')" :key="index">{{pagina}}</li>
                        <li v-show="intPagina < intTotalPaginas" @click="getSearch(++intPagina,'buscar')">></li>
                        <li v-show="intPagina < intTotalPaginas" @click="getSearch(intPagina = intTotalPaginas,'buscar')" >>></li>
                    </ul>
                </div>
            </section>
        </main>
        <script src="Librerias/vue/vue.min.js"></script>
		<script type="module" src="transporte/js/functions_declaracion_impuesto.js?<?= date('d_m_Y_h_i_s');?>"></script>
	</body>
</html>
