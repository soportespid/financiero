<?php
	header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=utf8");

	require "comun.inc";
	require "funciones.inc";
	require "conversor.php";
	require "validaciones.inc";

	$linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");

	session_start();
	date_default_timezone_set("America/Bogota");
?>
<!DOCTYPE >
<html lang="es">
	<head>
		<meta charset="utf-8"/>
		<meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1"/>
		<meta name="viewport" content="user-scalable=no">
		<title>:: IDEAL 10 - Tesoreria</title>
		<link href="favicon.ico" rel="shortcut icon"/>
        <link href="css/css2.css" rel="stylesheet" type="text/css" />
        <link href="css/css3.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="css/programas.js"></script>

    </head>
	<style>

	</style>
    <body>
        <IFRAME src="alertas.php" name="alertas" id="alertas" style="display:none"></IFRAME>
        <span id="todastablas2"></span>
        <table>
            <tr><script>barra_imagenes("teso");</script><?php cuadro_titulos();?></tr>
            <tr><?php menu_desplegable("teso");?></tr>
        	<tr>
          		<td colspan="3" class="cinta">
				<a class="mgbt"><img src="imagenes/add2.png" /></a>
				<a class="mgbt"><img src="imagenes/guardad.png" style="width:24px;"/></a>
				<a class="mgbt"><img src="imagenes/buscad.png"/></a>
				<a href="#" onClick="mypop=window.open('teso-principal.php','','');mypop.focus();" class="mgbt"><img src="imagenes/nv.png" title="Nueva Ventana"></a></td>
        	</tr>
        </table>
 		<form name="form2" method="post" action="">
    		<table class="inicio">
      			<tr>
        			<td class="titulos" colspan="2">.: Declaraci&oacute;n retenciones </td>
        			<td class="cerrar" style="width:7%;" ><a href="teso-principal.php">&nbsp;Cerrar</a></td>
      			</tr>
					<td style="background-repeat:no-repeat; background-position:center;">
						<ol id="lista2">
							<li onClick="location.href='teso-reportesparatrasladar.php'" style="cursor:pointer;">Reportes para traslado de recursos</li>
                            <li onClick="location.href='teso-pagotercerosdetalle_rete.php'" style="cursor:pointer;">Declaraci&oacute;n detallado Retenciones - Documento</li>
                            <li onClick="location.href='teso-pagotercerosdetalle_fechas.php'" style="cursor:pointer;">Declaraci&oacute;n detallado Retenciones - Documento / por fechas</li>
                            <li onClick="location.href='teso-declaracionRetencionesDet.php'" style="cursor:pointer;">Nuevo declaraci&oacute;n detallado Retenciones - Documento / por fechas</li>
                            <li onClick="location.href='teso-declaracionRetencionesDetBanco.php'" style="cursor:pointer;">Nuevo declaraci&oacute;n detallado Retenciones - Banco / por fechas</li>
                            <!-- <li onClick="location.href='teso-ingresospropios.php'" style="cursor:pointer;">Retenciones ingresos internos - Banco</li>
                            <li onClick="location.href='teso-ingresospropiosdetalle.php'" style="cursor:pointer;">Retenciones detallado de ingresos internos - Documento</li>
							<li onClick="location.href='teso-ingresosretenciones.php'" style="cursor:pointer;">Retenciones e ingresos - Documento </li> -->
							<li onClick="location.href='teso-pagotercerosbancosfechas.php'" style="cursor:pointer;">Reporte de retenciones - Bancos - Por fechas </li>
							<li onClick="location.href='teso-pagoRetenciones.php'" style="cursor:pointer;">Reporte de pagos de retenciones </li>
                        </ol>
				</td>
				</tr>
    		</table>
		</form>
	</body>
</html>
