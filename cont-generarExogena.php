<?php

    header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=utf8");
	require 'comun.inc';
	require 'funciones.inc';
	session_start();
    if(empty($_SESSION)){
        header("location: index.php");
    }
	$linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");
	cargarcodigopag($_GET['codpag'],$_SESSION["nivel"]);
	date_default_timezone_set("America/Bogota");
?>
<!DOCTYPE >
<html lang=es>
	<head>
    <meta charset="utf-8"/>
		<meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1"/>
		<meta name="viewport" content="user-scalable=no">
		<title>:: IDEAL 10 - Informes</title>
		<link href="favicon.ico" rel="shortcut icon"/>
		<link href="css/css2z.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
		<link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
		<link href="css/css4.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
        <link href="css/tabs.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css"/>
        <link href="css/style.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css"/>
        <link rel="stylesheet" href="sweetalert2/dist/sweetalert2.min.css">
        <script type="text/javascript" src="css/programas.js"></script>
        <script src="sweetalert2/dist/sweetalert2.min.js"></script>
		<script src="vue/vue.min.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script src="css/programas.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script src="css/calendario.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
	</head>
	<body>
		<header>
			<table>
				<tr><script>barra_imagenes("info");</script><?php cuadro_titulos();?></tr>
			</table>
		</header>

		<form name="form2" method="post" action="">
			<section id="myapp" v-cloak >
                <div id="cargando" v-show="isLoading" class="loading" style="z-index: 10000;">
                    <span>Cargando...</span>
                </div>
				<nav>
					<table>
						<tr><?php menu_desplegable("info");?></tr>
                        <tr>
							<td colspan="3" class="cinta">
                                <img src="imagenes/add2.png" class="mgbt" title="Nuevo">
								<img src="imagenes/guardad.png"   title="Guardar"  class="mgbt">
								<img src="imagenes/buscad.png"   class="mgbt" title="Buscar">
								<img src="imagenes/nv.png" @click="mypop=window.open('info-principal.php','',''); mypop.focus();" class="mgbt" title="Nueva Ventana">
                                <a @click="printExcel" class="mgbt"><img src="imagenes/excel.png" title="Excel"></a>
                                <a href="cont-parametrosexogena.php"  class="mgbt"><img src="imagenes/iratras.png" title="Atr&aacute;s"></a>
							</td>
						</tr>
					</table>
				</nav>
				<article>
                    <div class="inicio">
                        <div>
                            <h2 class="titulos m-0">.: Generar exógena</h2>
                            <div class="d-flex w-50">
                                <div class="form-control w-50">
                                    <label for="form-label">.: Formato <span class="text-danger fw-bolder">*</span></label>
                                    <select v-model="selectFormato">
                                        <option selected value="0">Seleccione</option>
                                        <option v-for="(data,index) in arrData" :key="index" :value="data.formato">
                                            {{ data.formato+" - "+data.nombre}}
                                        </option>

                                    </select>
                                </div>
                                <div class="form-control w-50">
                                    <label for="form-label">.: Año <span class="text-danger fw-bolder">*</span></label>
                                    <div class="d-flex">
                                        <input type="number" v-model="txtAnio">
                                        <button type="button" class="btn btn-primary" @click="getExogena">Generar</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="overflow-auto max-vh-50 p-2" >
                            <table class="table exogena fw-normal">
                                <thead>
                                    <tr>
                                        <th class="text-center" v-for="(col,i) in arrTotalColumnas" :key="i">{{col.nombre}}</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr v-for="(row,i) in arrFilas" :key="i">
                                        <td v-show="arrColumnas[0].check">{{row.concepto}}</td>
                                        <td v-show="arrColumnas[1].check">{{row.tercero != null ? row.tercero.tipodoc : ""}}</td>
                                        <td v-show="arrColumnas[2].check">{{row.tercero != null ?row.tercero.cedulanit:""}}</td>
                                        <td v-show="arrColumnas[3].check">{{row.tercero != null ? row.tercero.codver:""}}</td>
                                        <td v-show="arrColumnas[4].check">{{row.tercero != null ?row.tercero.apellido1:""}}</td>
                                        <td v-show="arrColumnas[5].check">{{row.tercero != null ?row.tercero.apellido2:""}}</td>
                                        <td v-show="arrColumnas[6].check">{{row.tercero != null ?row.tercero.nombre1:""}}</td>
                                        <td v-show="arrColumnas[7].check">{{row.tercero != null ?row.tercero.nombre2:""}}</td>
                                        <td v-show="arrColumnas[8].check">{{row.tercero != null ?row.tercero.razonsocial:""}}</td>
                                        <td v-show="arrColumnas[9].check">{{row.tercero != null ?row.tercero.direccion:""}}</td>
                                        <td v-show="arrColumnas[10].check">{{row.tercero != null ?row.tercero.depto:""}}</td>
                                        <td v-show="arrColumnas[11].check">{{row.tercero != null ?row.tercero.mnpio:""}}</td>
                                        <td v-show="arrColumnas[12].check">167</td>
                                        <td v-for="(row_col,j) in row.columnas" :key="j">
                                            {{row_col.valor_cuenta ? formatNumero(row_col.valor_cuenta) : formatNumero(0)}}
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
				</article>
			</section>
		</form>

		<script src="node_modules/read-excel-file/bundle/read-excel-file.min.js"></script>
		<script src="node_modules/xlsx/dist/xlsx.full.min.js"></script>
        <script src="Librerias/vue/vue.min.js"></script>
		<script src="Librerias/vue/axios.min.js"></script>
		<script type="module" src="contabilidad_vue/exogena_informe/buscar/cont-informeBuscar.js?<?= date('d_m_Y_h_i_s');?>"></script>

	</body>
</html>
