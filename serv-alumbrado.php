<?php
		require 'comun.inc';
		require 'funciones.inc';
		session_start();
		cargarcodigopag($_GET['codpag'],$_SESSION['nivel']);
		header('Cache-control: private'); // Arregla IE 6
		header("Cache-control: no-cache, no-store, must-revalidate");
		header("Content-Type: text/html;charset=utf8");
		date_default_timezone_set('America/Bogota');
		titlepag();
?>
<!DOCTYPE >
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="es">
	<head>
		<meta http-equiv="Content-type" content="text/html; charset=utf8"/>
		<meta http-equiv="X-UA-Compatible" content="IE=9"/>
		<title>:: IDEAL 10 - Servicios P&uacute;blicos</title>
		<link href="css/css2.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<link href="css/css4.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<link href="css/cssSP.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<link href="css/tabs.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css"/>
        <script type="text/javascript" src="css/calendario.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script src="vue/vue.js"></script>
		<script type="text/javascript" src="css/programas.js"></script>        

		<!-- sweetalert2 -->
		<script src="sweetalert2/dist/sweetalert2.min.js"></script>
		<link rel="stylesheet" href="sweetalert2/dist/sweetalert2.min.css">
	</head>
	<body>
		<div class="subpantalla" style="height:640px; width:99.6%; overflow:hidden;">
			<div id="myapp" style="height:inherit;" v-cloak>
				<div>
					<table>
						<tr>
							<script>barra_imagenes("serv");</script>
							<?php cuadro_titulos();?>
						</tr>

						<tr>
							<?php menu_desplegable("serv");?>
						</tr>

						<tr>
							<td colspan="3" class="cinta">
								<a href="#" class="mgbt"><img src="imagenes/add2.png"/></a>
								<a v-on:click="guardar();" class="mgbt"><img src="imagenes/guarda.png" title="Guardar"/></a>
								<a href="#" class="mgbt"><img src="imagenes/buscad.png" title="Buscar"/></a>
								<a onclick="mypop=window.open('plan-agenda.php','','');mypop.focus()" class="mgbt"><img src="imagenes/agenda1.png" title="Agenda" /></a>
								<a onclick="mypop=window.open('serv-principal.php','','');mypop.focus();" class="mgbt"><img src="imagenes/nv.png" title="Nueva ventana"></a>
								<a onclick="mypop=window.open('<?php echo $url2; ?>','','');mypop.focus();" class="mgbt"><img src="imagenes/duplicar_pantalla.png" title="Duplicar pesta&ntilde;a"></a>
							</td>
						</tr>
					</table>
				</div>

                <div>
                    <table class="inicio">
                        <tr>
                            <td class="titulos" style="text-align: center;" colspan="4">.: Tarifas de Alumbrado Público</td>

                            <td class="cerrar" style="width:5%" onclick="location.href='serv-principal.php'">Cerrar</td>
                        </tr>

                        <tr style="text-align: center;" class="titulos2">
                            <td style="width: 7%;">Código</td>
                            <td>Zona de uso</td>
                            <td style="width: 25%;">Porcentaje de tarifa</td>
                            <td style="width: 15%;">Estado</td>
                        </tr>

                        <tr v-for="(zona,index) in zonas" v-bind:class="index % 2 ? 'zebra1' : 'zebra2'" style='text-rendering: optimizeLegibility; text-align:center;'>
                            <td style="font: 120% sans-serif; padding-left:10px; width:10%;">{{ zona[0] }}</td>
                            <td style="font: 120% sans-serif; padding-left:10px;">{{ zona[1] }}</td>
                            <td style="font: 120% sans-serif; padding-left:10px; width:10%;">
                                <input type="number" id="lecturaActual" v-model="porcentajes[index]" min="1" max="99" style="text-align: center; width: 80%">
                            </td>
                            <td>
                                <img v-bind:src="estado[index]" style="width: 20px;" v-bind:title="titulosEstado" alt="">
                            </td>
	    				</tr>
                    </table>
                </div>
				
		</div>
		
		<script src="Librerias/vue/vue.min.js"></script>
		<script src="Librerias/vue/axios.min.js"></script>
		<script src="vue/servicios_publicos/serv-alumbrado.js?<?php echo date('d_m_Y_h_i_s');?>"></script>	
	</body>
</html>