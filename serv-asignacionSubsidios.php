<?php
	header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=iso-8859-1");
	require "comun.inc";
	require "funciones.inc";
	session_start();
	$linkbd=conectar_v7();
	cargarcodigopag($_GET['codpag'],$_SESSION["nivel"]);
	date_default_timezone_set("America/Bogota");
?>
<!DOCTYPE >
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="es">
	<head>
		<meta http-equiv="Content-type" content="text/html; charset=iso-8859-1"/>
		<meta http-equiv="X-UA-Compatible" content="IE=9"/>
		<title>:: IDEAL 10 - Servicios P&uacute;blicos</title>
		<link href="css/css2.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<link href="css/css4.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<link href="css/cssSP.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<script type="text/javascript" src="css/programas.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
        <script type="text/javascript" src="css/calendario.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<style>
		</style>

		<script>
            function despliegamodal2(_valor, _table)
			{
                document.getElementById('bgventanamodal2').style.visibility = _valor;
                
                if (_table == 'srvclientes')
                {
                    document.getElementById('ventana2').src = 'ventana-clienteservicio.php?table=' + _table;
                }
                else if(_table == 'srvsubsidios')
                {
                    document.getElementById('ventana2').src = 'subsidio-ventana.php';
                }
            }

			function despliegamodalm(_valor,_tip,mensa,pregunta)
			{
				document.getElementById("bgventanamodalm").style.visibility=_valor;
				if(_valor=="hidden"){document.getElementById('ventanam').src="";}
				else
				{
					switch(_tip)
					{
						case "1":	document.getElementById('ventanam').src="ventana-mensaje1.php?titulos="+mensa;break;
						case "2":	document.getElementById('ventanam').src="ventana-mensaje3.php?titulos="+mensa;break;
						case "3":	document.getElementById('ventanam').src="ventana-mensaje2.php?titulos="+mensa;break;
						case "4":	document.getElementById('ventanam').src="ventana-consulta1.php?titulos="+mensa+"&idresp="+pregunta;break;	
					}
				}
			}

			function respuestaModalBusqueda2(tabla, id, nombre, serial, referencia)
			{
				switch(tabla)
				{
					case 'srvclientes': 
						document.getElementById('cliente').value = id;
						document.getElementById('nCliente').value = nombre;
						document.form2.submit();
					break;	
                }
            }

			function funcionmensaje()
			{
				var idban=document.getElementById('codban').value;
				document.location.href = "serv-asignacionSubsidios.php";
			}

			function respuestaconsulta(pregunta)
			{
				switch(pregunta)
				{
					case "1":	document.form2.oculto.value='2';
								document.form2.submit();
								break;
				}
			}

			function guardar()
			{
				var v1 = document.getElementById('codban').value;
                var v3 = document.getElementById('subsidio').value;

				if (v1 != "" && v3 != "") 
                {
                    despliegamodalm('visible','4','Esta Seguro de Guardar','1');
                }
				else {
                    despliegamodalm('visible','2','Falta información para crear la Asignación');
                }
			}

			function actualizar()
			{
				document.form2.submit();
			}
		</script>

		<?php 
        	titlepag();
        ?>

	</head>

	<body>
		<IFRAME src="alertas.php" name="alertas" id="alertas" style="display:none"></IFRAME>
		<span id="todastablas2"></span>
		<table>
			<tr><script>barra_imagenes("serv");</script><?php cuadro_titulos();?></tr>

			<tr><?php menu_desplegable("serv");?></tr>

			<tr>
				<td colspan="3" class="cinta">
					<a href="serv-asignacionSubsidios.php" class="mgbt"><img src="imagenes/add.png"/></a>

					<a onclick="guardar()" class="mgbt"><img src="imagenes/guarda.png" title="Guardar"/></a>

					<a href="serv-buscaAsignacionSubsidios.php" class="mgbt"><img src="imagenes/busca.png" title="Buscar"/></a>

					<a onclick="mypop=window.open('plan-agenda.php','','');mypop.focus()" class="mgbt"><img src="imagenes/agenda1.png" title="Agenda" /></a>

					<a onclick="mypop=window.open('serv-principal.php','','');mypop.focus();" class="mgbt"><img src="imagenes/nv.png" title="Nueva ventana"></a>

					<a onclick="mypop=window.open('<?php echo $url2; ?>','','');mypop.focus();" class="mgbt"><img src="imagenes/duplicar_pantalla.png" title="Duplicar pesta&ntilde;a"></a>

					<a href="serv-menuAsignaciones.php" class="mgbt"><img src="imagenes/iratras.png" title="Atr&aacute;s"></a>
                </td>
			</tr>
		</table>
		
		<div id="bgventanamodalm" class="bgventanamodalm">
			<div id="ventanamodalm" class="ventanamodalm">
				<IFRAME src="" name="ventanam" marginWidth=0 marginHeight=0 frameBorder=0 id="ventanam" frameSpacing=0 style=" width:700px; height:130px; top:200; overflow:hidden;"></IFRAME>
			</div>
		</div>

		<form name="form2" method="post" action="">
			<?php 
				if(@$_POST['oculto']=="")
				{
					$_POST['onoffswitch'] = "S";
                    $_POST['tipo']='P';
					$_POST['codban']=selconsecutivo('srvasignacion_subsidios','id');
				}
			?>

            <table class="inicio" align="center">
                <tr>
                    <td class="titulos" colspan="2">.: Asignaci&oacute;n de Subsidio</td>
                    <td  class="cerrar" style="width: 1%;"><a href="serv-principal.php">Cerrar</a></td>
                </tr>

                <tr>
                    <td style="width:5%" class="saludo1">Tipo de subsidio:</td>
			        <td style="width:8%; height:30px;">
				        <select style="width:100%; height:30px;" name="tipo" id="tipo"  onChange="actualizar()" >
					        <option value="P" <?php if($_POST['tipo']=='P') echo "SELECTED"?>>Particular</option>
					        <option value="G" <?php if($_POST['tipo']=='G') echo "SELECTED"?>>General</option>
				        </select>
			        </td>
                </tr>
            </table>

            <?php
	        if($_POST['tipo']=='P') //***** PARTICULAR
            {
			?>

			<table class="inicio ancho">
				<tr>
					<td class="titulos" colspan="8">.: Ingresar asignaci&oacute;n subsidio Particular</td>
				</tr>
				<tr>
					<td class="tamano01" style="width:3cm;">C&oacute;digo:</td>
					<td style="width:15%;">
                        <input type="text" name="codban" id="codban" onKeyUp="return tabular(event,this)" value="<?php echo @ $_POST['codban'];?>" style="width:100%;height:30px;text-align:center;" readonly/>
                    </td>
                    <td class="saludo1">Cliente:</td>
					<td>
						<input type="text" name='cliente' id='cliente'  value="<?php echo @$_POST['cliente']?>" style="width: 100%; height: 30px;" onclick = "despliegamodal2('visible', 'srvclientes');"  class="colordobleclik" readonly>
                    </td>
                    <td colspan="4">
						<input type="text" name="nCliente" id="nCliente" value="<?php echo @$_POST['nCliente']?>" style="width:100%;height:30px;" readonly>
					</td>
				</tr>
				<tr>
                    <td class="saludo1">Subsidio:</td>
					<td>
						<input type="text" name='subsidio' id='subsidio'  value="<?php echo @$_POST['subsidio']?>" style="width: 100%; height: 30px;" onclick = "despliegamodal2('visible', 'srvsubsidios');"  class="colordobleclik" readonly>
                    </td>
                    <td colspan="2">
						<input type="text" name="nSubsidio" id="nSubsidio" value="<?php echo @$_POST['nSubsidio']?>" style="width:100%;height:30px;" readonly>
					</td>

                    <td class="tamano01">Estado:</td>
					<td>
						<div class="onoffswitch">
							<input type="checkbox" name="onoffswitch" class="onoffswitch-checkbox" id="myonoffswitch" value="<?php echo @ $_POST['onoffswitch'];?>" <?php if(@ $_POST['onoffswitch']=='S'){echo "checked";}?> onChange="cambiocheck();"/>
							<label class="onoffswitch-label" for="myonoffswitch">
								<span class="onoffswitch-inner"></span>
								<span class="onoffswitch-switch"></span>
							</label>
						</div>
					</td>
				</tr>
			</table>
            <?php 
            } 
            if($_POST['tipo'] == 'G')//General
            {
            ?>
            <table class="inicio ancho">
				<tr>
					<td class="titulos" colspan="8">.: Ingresar asignaci&oacute;n subsidio General</td>
				</tr>
				<tr>
					<td class="tamano01" style="width:3cm;">C&oacute;digo:</td>
					<td style="width:15%;">
                        <input type="text" name="codban" id="codban" onKeyUp="return tabular(event,this)" value="<?php echo @ $_POST['codban'];?>" style="width:100%;height:30px;text-align:center;" readonly/>
                    </td>
                    <td class="tamano01">Estrato:</td>
					<td>
						<select name="idestrato" id="idestrato" style="width:100%;height:30px;">
							<option value="-1">:: Seleccione Estrato ::</option>
							<?php
								$sqlr="SELECT id,descripcion FROM srvestratos WHERE estado = 'S' ORDER BY id";
								$resp = mysqli_query($linkbd,$sqlr);
								while ($row =mysqli_fetch_row($resp))
								{
									$sqlUsoSuelo = "SELECT nombre FROM srvusosdesuelo WHERE id = '$row[2]' ";
									$respUsoSuelo = mysqli_query($linkbd,$sqlUsoSuelo);
									$rowUsoSuelo = mysqli_fetch_row($respUsoSuelo);

									if($_POST['idestrato']==$row[0])
									{
										echo "<option value='$row[0]' SELECTED>$row[1] - $rowUsoSuelo[0]</option>";
									}
									else {echo "<option value='$row[0]'>$row[1] - - $rowUsoSuelo[0]</option>";}
								}
							?>
						</select>
					</td>
				</tr>
				<tr>
                    <td class="saludo1">Subsidio:</td>
					<td>
						<input type="text" name='subsidio' id='subsidio'  value="<?php echo @$_POST['subsidio']?>" style="width: 100%; height: 30px;" onclick = "despliegamodal2('visible', 'srvsubsidios');"  class="colordobleclik" readonly>
                    </td>
                    <td colspan="2">
						<input type="text" name="nSubsidio" id="nSubsidio" value="<?php echo @$_POST['nSubsidio']?>" style="width:100%;height:30px;" readonly>
					</td>
                    

                    <td class="tamano01">Estado:</td>
					<td>
						<div class="onoffswitch">
							<input type="checkbox" name="onoffswitch" class="onoffswitch-checkbox" id="myonoffswitch" value="<?php echo @ $_POST['onoffswitch'];?>" <?php if(@ $_POST['onoffswitch']=='S'){echo "checked";}?> onChange="cambiocheck();"/>
							<label class="onoffswitch-label" for="myonoffswitch">
								<span class="onoffswitch-inner"></span>
								<span class="onoffswitch-switch"></span>
							</label>
						</div>
					</td>
				</tr>
			</table>
            <?php 
            }
            ?>
			<input type="hidden" name="oculto" id="oculto" value="1"/>
			<?php 
				if(@ $_POST['oculto']=="2")
				{
					if (@ $_POST['onoffswitch']!='S')
					{
						$valest='N';
					}
					else 
					{
						$valest='S';
					} 

					$_POST['codban']=selconsecutivo('srvasignacion_subsidios','id');

                    if($_POST['tipo'] == 'P')
                    {
                        $sqlr="INSERT INTO srvasignacion_subsidios (id, id_cliente, id_subsidio, particular, general, estado) VALUES ('".$_POST['codban']."','".$_POST['cliente']."','".$_POST['subsidio']."','S','N','$valest')";
                        if (!mysqli_query($linkbd,$sqlr))
                        {
                            $e =mysqli_error(mysqli_query($linkbd,$sqlr));
                            echo"<script>despliegamodalm('visible','2','No se pudo ejecutar la petici&oacute;n: $e');</script>";
                        }
                        else 
                        {
                            echo "<script>despliegamodalm('visible','1','Se ha almacenado con Exito');</script>";
                        }
                    }

                    if($_POST['tipo'] == 'G')
                    {
                        $sqlr="INSERT INTO srvasignacion_subsidios (id, id_estrato, id_subsidio, particular, general, estado) VALUES ('".$_POST['codban']."','".$_POST['idestrato']."','".$_POST['subsidio']."','N','S','$valest')";
                        if (!mysqli_query($linkbd,$sqlr))
                        {
                            $e =mysqli_error(mysqli_query($linkbd,$sqlr));
                            echo"<script>despliegamodalm('visible','2','No se pudo ejecutar la petici&oacute;n: $e');</script>";
                        }
                        else 
                        {
                            echo "<script>despliegamodalm('visible','1','Se ha almacenado con Exito');</script>";
                        }
                    }

					
				}
			?>
		</form>
		<div id="bgventanamodal2">
			<div id="ventanamodal2">
				<IFRAME  src="" name="buster" marginWidth=0 marginHeight=0 frameBorder=0 id="ventana2" frameSpacing=0 style="left:500px; width:900px; height:500px; top:200;"></IFRAME>
			</div>
		</div>
	</body>
</html>
