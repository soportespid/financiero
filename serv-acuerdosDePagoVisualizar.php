<?php
	header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=utf8");
	require "comun.inc";
	require "funciones.inc";
    require "funcionesSP.inc.php";
    require "conversor.php";
	session_start();
	$linkbd = conectar_v7();
	cargarcodigopag($_GET['codpag'],$_SESSION["nivel"]);
	date_default_timezone_set("America/Bogota");

    $scroll=$_GET['scrtop'];
	$totreg=$_GET['totreg'];
	$idcta=$_GET['idcta'];
	$altura=$_GET['altura'];
	$filtro="'".$_GET['filtro']."'";
?>
<!DOCTYPE >
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="es">
	<head>
		<meta http-equiv="Content-type" content="text/html; charset=iso-8859-1"/>
		<meta http-equiv="X-UA-Compatible" content="IE=9"/>
		<title>:: IDEAL 10 - Servicios P&uacute;blicos</title>
		<link href="css/css2.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<link href="css/css4.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<link href="css/cssSP.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<link href="css/style.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css"/>
		<script type="text/javascript" src="css/programas.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
        <script type="text/javascript" src="css/calendario.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<style>
		</style>

		<script>
			function despliegamodalm(_valor,_tip,mensa,pregunta)
			{
				document.getElementById("bgventanamodalm").style.visibility=_valor;

				if(_valor=="hidden")
				{
					document.getElementById('ventanam').src="";
				}
				else
				{
					switch(_tip)
					{
						case "1":	
							document.getElementById('ventanam').src="ventana-mensaje1.php?titulos="+mensa;
						break;

						case "2":	
							document.getElementById('ventanam').src="ventana-mensaje3.php?titulos="+mensa;
						break;

						case "3":	
							document.getElementById('ventanam').src="ventana-mensaje2.php?titulos="+mensa;
						break;

						case "4":	
							document.getElementById('ventanam').src="ventana-consulta1.php?titulos="+mensa+"&idresp="+pregunta;
						break;	
					}
				}
			}

			function funcionmensaje()
			{
				location.href="serv-acuerdosDePagoBuscar.php";
			}

			function respuestaconsulta(pregunta)
			{
				switch(pregunta)
				{
					case "1":	
						document.form2.oculto.value='2';
						document.form2.submit();
					break;
				}
			}

			function actualizar()
			{
				document.form2.submit();
			}

            function iratras(scrtop, numpag, limreg, filtro)
			{
				var idcta=document.getElementById('codban').value;

				location.href="serv-acuerdosDePagoBuscar.php?idcta="+idcta+"&scrtop="+scrtop+"&numpag="+numpag+"&limreg="+limreg+ "&filtro="+filtro;
			}

			function adelante(scrtop, numpag, limreg, filtro)
			{
				var maximo=document.getElementById('maximo').value;
				var actual=document.getElementById('codban').value;
				actual=parseFloat(actual)+1;

				if(actual<=parseFloat(maximo))
				{
					if(actual<10){actual="0"+actual;}
					location.href="serv-acuerdosDePagoVisualizar.php?idban=" +actual+ "&scrtop=" +scrtop+ "&numpag=" +numpag+ "&limreg=" +limreg+ "&filtro=" +filtro;
				}
			}

			function atrasc(scrtop, numpag, limreg, filtro, prev)
			{
				var minimo=document.getElementById('minimo').value;
				var actual=document.getElementById('codban').value;
				actual=parseFloat(actual)-1;

				if(actual>=parseFloat(minimo))
				{
					if(actual<10){actual="0"+actual;}
					location.href="serv-acuerdosDePagoVisualizar.php?idban=" +actual+ "&scrtop=" +scrtop+ "&numpag=" +numpag+ "&limreg=" +limreg+ "&filtro=" +filtro;
				}
			}

            function anular()
            {
                var estado = document.getElementById('estado').value;
                var cuotaInicial = document.getElementById('cuotas').value;
                var cuotaActual = document.getElementById('cuotaActual').value;

                if(estado != 'Acuerdo Anulado')
                {
					despliegamodalm('visible','4','¿Estas seguro de Anular?','1');	
                }
                else
                {
                    despliegamodalm('visible','2','Este acuerdo ya esta anulado.');
                }
            }

            function pdf()
			{
				document.form2.action="serv-pdfAcuerdoDePago.php";
				document.form2.target="_BLANK";
				document.form2.submit(); 
				document.form2.action="";
				document.form2.target="";
			}
		</script>

		<?php titlepag();?>

	</head>

	<body>
		<IFRAME src="alertas.php" name="alertas" id="alertas" style="display:none"></IFRAME>
		<span id="todastablas2"></span>
        <?php 
            $numpag = @$_GET['numpag'];
			$limreg = @$_GET['limreg'];
			$scrtop = 26 * $totreg;
        ?>
		<table>
			<tr><script>barra_imagenes("serv");</script><?php cuadro_titulos();?></tr>

			<tr><?php menu_desplegable("serv");?></tr>

			<tr>
				<td colspan="3" class="cinta">
					<a href="serv-acuerdosDePago.php" class="mgbt"><img src="imagenes/add.png"/></a>

					<a class="mgbt"><img src="imagenes/guardad.png" title="Guardar"/></a>

					<a href="serv-acuerdosDePagoBuscar.php" class="mgbt"><img src="imagenes/busca.png" title="Buscar"/></a>

					<a onclick="mypop=window.open('plan-agenda.php','','');mypop.focus()" class="mgbt"><img src="imagenes/agenda1.png" title="Agenda" /></a>

					<a onclick="mypop=window.open('serv-principal.php','','');mypop.focus();" class="mgbt"><img src="imagenes/nv.png" title="Nueva ventana"></a>

					<a onclick="mypop=window.open('<?php echo $url2; ?>','','');mypop.focus();" class="mgbt"><img src="imagenes/duplicar_pantalla.png" title="Duplicar pesta&ntilde;a"></a>

                    <a onClick="iratras(<?php echo "$scrtop, $numpag, $limreg, $filtro"; ?>)" class="mgbt"><img src="imagenes/iratras.png" title="Atr&aacute;s"></a>

                    <a onClick="anular();" class="mgbt"><img src="imagenes/anular.png" alt="Reversar" title="Reversar"></a>

                    <a onClick="pdf()" class="mgbt"><img src="imagenes/print.png" style="width:29px;height:25px;" title="Imprimir" /></a>
                </td>
			</tr>
		</table>

		<div id="bgventanamodalm" class="bgventanamodalm">
			<div id="ventanamodalm" class="ventanamodalm">
				<IFRAME src="" name="ventanam" marginWidth=0 marginHeight=0 frameBorder=0 id="ventanam" frameSpacing=0 style=" width:700px; height:130px; top:200; overflow:hidden;"></IFRAME>
			</div>
		</div>

		<form name="form2" method="post" action="">
			<?php 
				if(@$_POST['oculto']=="")
				{
                    $sqlr = "SELECT MIN(id), MAX(id) FROM srvacuerdos_pago";
					$res = mysqli_query($linkbd,$sqlr);
					$r = mysqli_fetch_row($res);

					$_POST['minimo']=$r[0];
					$_POST['maximo']=$r[1];

                    $_POST['codban'] = $_GET['idban'];

                    $sqlr = "SELECT id_cliente, numero_factura, fecha, cuotas_inicial, valor_inicial, cuotas_pagadas, valor_pagado, estado, tipo_movimiento, valor_abono, valor_acuerdo FROM srvacuerdos_pago WHERE id = '$_POST[codban]'";
                    $resp = mysqli_query($linkbd,$sqlr);
                    $row  = mysqli_fetch_row($resp);

                    $_POST['cliente'] = $row[0];
                    $_POST['nCliente'] = encuentraNombreTerceroConIdCliente($row[0]);
                    $_POST['numeroFactura'] = $row[1];
                    $_POST['fecha'] = $row[2];
                    $_POST['valor'] = $row[4];
                    $_POST['cuotas'] = $row[3];
                    $valorCuota = $row[10] / $row[3];
					$valorCuota = ($valorCuota / 100);
					$valorCuota = ceil($valorCuota);
					$valorCuota = $valorCuota * 100;
					$_POST['valorCuota'] = $valorCuota;
                    $_POST['tipoMovimiento'] = $row[8] ." - Acuerdo de pago Factura";
					$valorActual = $row[6];
					$valorActual = ($valorActual / 100);
					$valorActual = ceil($valorActual);
					$valorActual = $valorActual * 100;
					$_POST['valorActual'] = $valorActual;
                    $_POST['cuotaActual'] = $row[5];
                    $_POST['cedulanit'] = encuentraDocumentoTerceroConIdCliente($row[0]);
					$_POST['abono'] = $row[9];
					$_POST['valorAcuerdo'] = $row[10];
					$totalg = number_format($_POST['valorAcuerdo'],2,'.','');
                    $_POST['letras'] = convertirdecimal($totalg,'.');
                    
                    switch($row[7])
                    {
                        case 'S': 
                            $_POST['estado'] = "Acuerdo en Curso"; 
                            $color = "background: yellow;";
                        break;

                        case 'F':
                            $_POST['estado'] = "Acuerdo Finalizado";
                            $color = "background: green;";
                        break;

                        case 'N':
                            $_POST['estado'] = "Acuerdo Anulado";
                            $color = "background: red;";
                        break;
                    }
				}
			?>

			<table class="inicio ancho">
				<tr>
					<td class="titulos" colspan="6">.: Visualizar Acuerdo de Pago para Factura</td>

					<td class="cerrar" style="width:7%" onClick="location.href='serv-principal.php'">Cerrar</td>
				</tr>

				<tr>
					<td class="tamano01" style="width:3cm;">C&oacute;digo:</td>

					<td style="width:15%;">
                        <a onClick="atrasc(<?php echo "$scrtop, $numpag, $limreg, $filtro"; ?>)" class="icobut" title="Anterior"><img src="imagenes/back.png"/></a>

                        <input type="text" name="codban" id="codban" onKeyUp="return tabular(event,this)" value="<?php echo @ $_POST['codban'];?>" style="width:75%;height:30px;text-align:center;" readonly/>

                        <a onClick="adelante(<?php echo "$scrtop, $numpag, $limreg, $filtro" ?>);" class="icobut" title="Sigiente"><img src="imagenes/next.png"/></a>
                    </td>

                    <td class="tamano01">Cliente:</td>

					<td>
						<input type="text" name='cliente' id='cliente'  value="<?php echo @$_POST['cliente']?>" style="text-align:center; width:100%;" readonly>
                    </td>

                    <td colspan="2">
						<input type="text" name="nCliente" id="nCliente" value="<?php echo @$_POST['nCliente']?>" style="width:50%;height:30px;" readonly>
					</td>
				</tr>

				<tr>
                    <td class="tamano01">N&deg; Factura:</td>

					<td>
						<input type="text" name='numeroFactura' id='numeroFactura' value="<?php echo @$_POST['numeroFactura']?>" style="width: 95%; height: 30px; text-align:center;" readonly>
                    </td>
                    
					<td class="tamano01" style="width:3cm;">Fecha:</td>
                    
					<td style="width:15%;">
                        <input type="text" name="fecha" value="<?php echo @ $_POST['fecha']?>" maxlength="10" onKeyPress="javascript:return solonumeros(event)" onKeyUp="return tabular(event,this)" id="fc_1198971545" onKeyDown="mascara(this,'/',patron,true)" title="DD/MM/YYYY" style="width:80%; text-align:center;" readonly>
                        <img src="imagenes/calendario04.png" onClick="displayCalendarFor('fc_1198971545');" title="Calendario" class="icobut"/>
                    </td>

                    <td class="tamano01" style="width: 150px;">Estado de Acuerdo</td>
                    
                    <td>
                        <input type="text" name="estado" id="estado" value="<?php echo $_POST['estado'] ?>" style="height: 30px; <?php echo $color ?>" readonly>
                    </td>
				</tr>

				<tr>
					<td class="tamano01">Valor de la factura:</td>
					<td>
						<input type="text" name="valor" id="valor" value="<?php echo @$_POST['valor']?>" style="height:30px; width: 197px; text-align:center;" readonly>
					</td>

					<td class="tamano01">Valor Abonado:</td>
					<td>
						<input type="text" name="abono" id="abono" value="<?php echo $_POST['abono'] ?>" style="height:30px; width: 197px; text-align:center;" readonly>
					</td>

					<td class="tamano01">Valor Acuerdo:</td>
					<td>
						<input type="text" name="valorAcuerdo" id="valorAcuerdo" value="<?php echo $_POST['valorAcuerdo'] ?>" style="height:30px; width: 197px; text-align:center;" readonly>
					</td>
                </tr>

                <tr>
					<td class="tamano01" style="width: 68px; height: 30px">Numero de Cuotas</td>
                    <td>
                        <input type="text" name="cuotas" id="cuotas" value="<?php echo $_POST['cuotas'] ?>" maxlength="2" style="text-align:center; height: 30px;" onKeyPress="javascript:return solonumeros(event)" onKeyUp="return tabular(event,this)" onBlur="valorcuotas(event);" readonly>
                    </td>

                    <td class="tamano01">Valor de la cuota:</td>

                    <td>
                        <input type="text" name="valorCuota" id="valorCuota" value="<?php echo @$_POST['valorCuota']?>" style="height:30px; width: 197px; text-align:center;" readonly>
                    </td>

                    <td class="tamano01">Tipo de Movimiento:</td>

                    <td>
                        <input type="text" name="tipoMovimiento" id="tipoMovimiento" value="<?php echo $_POST['tipoMovimiento'] ?>" style="width: 225px; text-align:center;" readonly>
                    </td>
                </tr>

                <tr>
                    <td class="tamano01">Valor Facturado:</td>

                    <td>
                        <input type="text" name="valorActual" id="valorActual" value="<?php echo $_POST['valorActual'] ?>" style="text-align: center; width: 197px;" readonly>
                    </td>

                    <td class="tamano01">Cuotas Facturadas:</td>

                    <td>
                        <input type="text" name="cuotaActual" id="cuotaActual" value="<?php echo $_POST['cuotaActual'] ?>" style="text-align: center;" readonly>
                    </td>
                </tr>

				<tr>
					<td class="tamano01">Descripción</td>

					<td colspan="4"> 
						<input type="text" name="descripcion" id="descripcion" style="width: 100%;">
					</td>
				</tr>
			</table>

			<input type="hidden" name="oculto" id="oculto" value="1"/>
            <input type="hidden" name="maximo" id="maximo" value="<?php echo @ $_POST['maximo']?>"/>
			<input type="hidden" name="minimo" id="minimo" value="<?php echo @ $_POST['minimo']?>"/>
            <input type="hidden" name="cedulanit" id="cedulanit" value="<?php echo @$_POST['cedulanit']?>"/>
            <input type="hidden" name="letras" id="letras" value="<?php echo @$_POST['letras']?>"/>

			<?php 
				if(@$_POST['oculto'] == "2")
				{
					$comprobanteAcuerdo = '39';
					$comprobanteAcuerdoReversado = '2039';

					$fecha = date("Y-m-d");

					$sqlAcuerdoPago = "SELECT numero_factura, cuotas_pagadas FROM srvacuerdos_pago WHERE id = '$_POST[codban]'";
					$resAcuerdoPago = mysqli_query($linkbd,$sqlAcuerdoPago);
					$rowAcuerdoPago = mysqli_fetch_row($resAcuerdoPago);

					$sqlComprobanteCab = "SELECT total_debito FROM comprobante_cab WHERE numerotipo = '$_POST[codban]' AND tipo_comp = '$comprobanteAcuerdo'";
					$resComprobanteCab = mysqli_query($linkbd, $sqlComprobanteCab);
					$rowComprobanteCab = mysqli_fetch_row($resComprobanteCab);

					if ($rowAcuerdoPago[1] == 0) {

						$consecutivo_reversion = selconsecutivo('srvacuerdo_pago_reversado', 'id_acuerdo_reversado');

						$query = "INSERT INTO srvacuerdo_pago_reversado (id_acuerdo_reversado, id_acuerdo_pago, numero_factura, fecha) VALUES ($consecutivo_reversion, '$_POST[codban]', '$_POST[numeroFactura]', '$fecha')";
						mysqli_query($linkbd,$query);

						$sqlCorteDetalles = "UPDATE srvcortes_detalle SET estado_pago = 'S' WHERE numero_facturacion = '$_POST[numeroFactura]' ";
						mysqli_query($linkbd,$sqlCorteDetalles);

						$queryComprobanteCab = "INSERT INTO comprobante_cab (numerotipo, tipo_comp, fecha, concepto, total_debito, total_credito) VALUES ('$consecutivo_reversion', '$comprobanteAcuerdoReversado', '$fecha', 'REVERSION DE ACUERDO DE PAGO CONSECUTIVO $_POST[codban]', '$rowComprobanteCab[0]', '$rowComprobanteCab[0]')";
						mysqli_query($linkbd,$queryComprobanteCab);

						$sqlComprobanteDet = "SELECT * FROM comprobante_det WHERE id_comp = '$comprobanteAcuerdo $_POST[codban]'";
						$resComprobanteDet = mysqli_query($linkbd, $sqlComprobanteDet);
						while ($rowComprobanteDet = mysqli_fetch_row($resComprobanteDet)) {

							$queryComprobanteDet = "INSERT INTO comprobante_det (id_comp, cuenta, tercero, centrocosto, detalle, valdebito, valcredito, estado, vigencia, tipo_comp, numerotipo) VALUES ('$comprobanteAcuerdoReversado $consecutivo_reversion', '$rowComprobanteDet[2]', '$rowComprobanteDet[3]', '$rowComprobanteDet[4]', 'REVERSION DE ACUERDO DE PAGO', '$rowComprobanteDet[8]', '$rowComprobanteDet[7]', '$rowComprobanteDet[9]', '$rowComprobanteDet[10]', '$comprobanteAcuerdoReversado', '$consecutivo_reversion')";
							mysqli_query($linkbd,$queryComprobanteDet);
						}

						$queryComprobanteRev = "INSERT INTO comprobante_rev (tipo_comprobante_origen, num_comprobante_origen, tipo_comprobante_rev, num_comprobante_rev) VALUES ('$comprobanteAcuerdo', '$_POST[codban]', '$comprobanteAcuerdoReversado', '$consecutivo_reversion')";
						mysqli_query($linkbd,$queryComprobanteRev);

						$sqlr = "UPDATE srvacuerdos_pago SET estado = 'N' WHERE id = '$_POST[codban]'";
						if(mysqli_query($linkbd,$sqlr)) {	
							echo "<script>despliegamodalm('visible','1','Se ha reversado con exito');</script>";
						}
						else {
							echo"<script>despliegamodalm('visible','2','No se pudo reversar.');</script>";
						}
					}
					else {
						echo " <script>despliegamodalm('visible','2','No se pudo anular el acuerdo de pago porque ya esta en ejecucion');</script>";
					}

					
                }
			?>
		</form>
		<div id="bgventanamodal2">
			<div id="ventanamodal2">
				<IFRAME  src="" name="buster" marginWidth=0 marginHeight=0 frameBorder=0 id="ventana2" frameSpacing=0 style="left:500px; width:900px; height:500px; top:200;"></IFRAME>
			</div>
		</div>
	</body>
</html>
