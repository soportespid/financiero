<?php
header("Cache-control: no-cache, no-store, must-revalidate");
header("Content-Type: text/html;charset=utf8");
require "comun.inc";
require "funciones.inc";
require "conversor.php";
require "validaciones.inc";
session_start();
$linkbd = conectar_v7();
$linkbd->set_charset("utf8");
cargarcodigopag($_GET['codpag'], $_SESSION["nivel"]);
date_default_timezone_set("America/Bogota");
?>
<!DOCTYPE>
<html lang="es">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1">
    <meta name="viewport" content="user-scalable=no">
    <title>:: IDEAL 10 - Tesorer&iacute;a</title>
    <link href="favicon.ico" rel="shortcut icon" />
    <link href="css/css2.css?<?php echo date('d_m_Y_h_i_s'); ?>" rel="stylesheet" />
    <link href="css/css3.css?<?php echo date('d_m_Y_h_i_s'); ?>" rel="stylesheet" />
    <link href="css/css4.css?<?php echo date('d_m_Y_h_i_s'); ?>" rel="stylesheet" />
    <link href="css/tabs2.css?<?php echo date('d_m_Y_h_i_s'); ?>" rel="stylesheet" />
    <link href="css/tabs.css?<?php echo date('d_m_Y_h_i_s'); ?>" rel="stylesheet" />
    <link href="css/style.css?<?php echo date('d_m_Y_h_i_s'); ?>" rel="stylesheet" />
    <script type="text/javascript" src="css/calendario.js?<?php echo date('d_m_Y_h_i_s'); ?>"></script>
    <script type="text/javascript" src="css/programas.js?<?php echo date('d_m_Y_h_i_s'); ?>"></script>
    <script type="text/javascript" src="JQuery/jquery-2.1.4.min.js"></script>
    <script>
        function despliegamodal2(_valor, _num) {


            document.getElementById("bgventanamodal2").style.visibility = _valor;
            if (_valor == "hidden") { document.getElementById('ventana2').src = ""; }
            else {
                switch (_num) {

                    case '1': document.getElementById('ventana2').src = "ordenpago-ventana1.php?vigencia=" + document.form2.vigencia.value; break;
                    case '2': document.getElementById('ventana2').src = "cuentasbancarias-ventana01.php?tipoc=C"; break;
                    case '3':

                        var fuentes = document.getElementsByName('drecursos[]');
                        var codigoFuentes = [];

                        for (let i = 0; i < fuentes.length; i++) {

                            console.log(document.getElementsByName('drecursos[]').item(i).value);
                            codigoFuentes[i] = document.getElementsByName('drecursos[]').item(i).value;
                        }

                        console.log(codigoFuentes);

                        document.getElementById('ventana2').src = "cuentasbancarias-ventana01.php?tipoc=D&fuentes=" + codigoFuentes;
                        break;
                    case '4': document.getElementById('ventana2').src = "reversar-egreso.php?vigencia=" + document.form2.vigencia.value; break;
                    case '5': document.getElementById('ventana2').src = "mediosPago-ventana01.php?objeto=codingreso&nobjeto=ningreso&medioDePago=sgr"; break;
                }
            }
        }
        function validar() {
            document.form2.submit();
        }
        function validar2() {
            document.form2.egresoo.value = 1;
            document.form2.submit();
        }
        function buscaop(e) {
            if (document.form2.orden.value != "") {
                document.form2.bop.value = '1';
                document.form2.submit();
            }
        }
        function guardar() {
            var fechabloqueo = document.form2.fechabloq.value;
            var fechadocumento = document.form2.fecha.value;
            var nuevaFecha = fechadocumento.split("/");
            var fechaCompara = nuevaFecha[2] + "-" + nuevaFecha[1] + "-" + nuevaFecha[0];
            var validacion01 = document.form2.orden.value;
            if ((Date.parse(fechabloqueo)) > (Date.parse(fechaCompara))) {
                despliegamodalm('visible', '2', 'Fecha de documento menor que fecha de bloqueo');
            } else {

                if (document.form2.tipomovimiento.value == '201') {
                    var saldo = parseInt(document.form2.saldocxp.value);
                    if (saldo <= 0) {
                        despliegamodalm('visible', '2', 'No hay saldo en dicha orden de pago');
                    } else {
                        var medioDePago1 = document.form2.codingreso.value;
                        if (medioDePago1 != '') {
                            if ((validacion01.trim() != '') && document.form2.fecha.value != '') {
                                despliegamodalm('visible', '4', 'Esta Seguro de Guardar', '1');
                            }
                            else { despliegamodalm('visible', '2', 'Falta informacion para Crear la Cuenta'); }
                        }
                        else {
                            if ((validacion01.trim() != '') && (document.getElementById('tipop').value != '' || document.getElementById('tipop').value != '') && document.form2.fecha.value != '') {
                                // alert("tipo 1.1");
                                if (document.getElementById('tipop').value == 'cheque') {
                                    var validacion02 = document.getElementById('ncheque').value;
                                    if ((validacion02.trim() != '') && document.getElementById('nbanco').value != '') { despliegamodalm('visible', '4', 'Esta Seguro de Guardar', '1'); }
                                    else { despliegamodalm('visible', '2', 'Falta informacion para Crear la Cuenta'); }
                                }
                                else if (document.getElementById('tipop').value == 'transferencia') {
                                    var validacion02 = document.getElementById('ntransfe').value;
                                    if ((validacion02.trim() != '') && document.getElementById('nbanco').value != '') { despliegamodalm('visible', '4', 'Esta Seguro de Guardar', '1'); }
                                    else { despliegamodalm('visible', '2', 'Falta informacion para Crear la Cuenta'); }
                                }
                                else if (document.getElementById('tipop').value == 'caja') {
                                    var validacion02 = document.getElementById('banco').value;
                                    if ((validacion02.trim() != '') && document.getElementById('nbanco').value != '') { despliegamodalm('visible', '4', 'Esta Seguro de Guardar', '1'); }
                                    else { despliegamodalm('visible', '2', 'Falta informacion para Crear la Cuenta'); }
                                }
                                else { despliegamodalm('visible', '2', 'Falta informacion para Crear la Cuenta'); }
                            }
                            else { despliegamodalm('visible', '2', 'Falta informacion para Crear la Cuenta'); }
                        }
                    }
                } else {
                    let fechaComp = document.form2.fechaComp.value;
                    let fechaRev = document.form2.fecha.value;
                    let fechaFrac = fechaComp.split('-');
                    let fechaComprobante = fechaFrac[0] + '' + fechaFrac[1] + '' + fechaFrac[2];
                    let fechaFracRev = fechaRev.split('/');
                    let fechaReversion = fechaFracRev[2] + '' + fechaFracRev[1] + '' + fechaFracRev[0]

                    if (parseInt(fechaComprobante) > parseInt(fechaReversion)) {
                        despliegamodalm('visible', '2', 'La fecha de reversión no puede ser menor a la fecha de creación del documento.');
                    } else {
                        let descripcion = document.form2.descripcion.value;
                        if (descripcion.length > 12) {
                            despliegamodalm('visible', '4', 'Esta Seguro de Guardar', '1');
                        } else {
                            despliegamodalm('visible', '2', 'Descripción insufieciente.');
                        }
                    }
                }
            }

        }
        function calcularpago() {
            valorp = document.form2.valor.value;
            descuentos = document.form2.totaldes.value;
            valorc = valorp - descuentos;
            document.form2.valorcheque.value = valorc;
            document.form2.valoregreso.value = valorp;
            document.form2.valorretencion.value = descuentos;
        }
        function pdf() {
            document.form2.action = "pdfegreso.php";
            document.form2.target = "_BLANK";
            document.form2.submit();
            document.form2.action = "";
            document.form2.target = "";
        }
        function despliegamodalm(_valor, _tip, mensa, pregunta) {
            document.getElementById("bgventanamodalm").style.visibility = _valor;
            if (_valor == "hidden") { document.getElementById('ventanam').src = ""; }
            else {
                switch (_tip) {
                    case "1": document.getElementById('ventanam').src = "ventana-mensaje1.php?titulos=" + mensa; break;
                    case "2": document.getElementById('ventanam').src = "ventana-mensaje3.php?titulos=" + mensa; break;
                    case "3": document.getElementById('ventanam').src = "ventana-mensaje2.php?titulos=" + mensa; break;
                    case "4": document.getElementById('ventanam').src = "ventana-consulta1.php?titulos=" + mensa + "&idresp=" + pregunta; break;
                }
            }
        }
        function funcionmensaje() {
            var _cons = document.getElementById('egreso').value;
            document.location.href = "teso-girarchequesver-ccpet.php?idegre=" + _cons + "&scrtop=0&numpag=1&limreg=10&filtro1=&filtro2=";
        }
        function respuestaconsulta(pregunta) {
            switch (pregunta) {
                case "1":
                    // alert("case 1");
                    document.form2.oculto.value = '2';
                    document.form2.submit();
                    break;
            }
        }
        function direccionaCuentaGastos(row) {
            //alert (row);
            window.open("presu-editarcuentaspasiva.php?idcta=" + row);
        }
    </script>
</head>

<body>
    <IFRAME src="alertas.php" name="alertas" id="alertas" style="display:none"></IFRAME>
    <span id="todastablas2"></span>
    <table>
        <tr>
            <script>barra_imagenes("teso");</script><?php cuadro_titulos(); ?>
        </tr>
        <tr><?php menu_desplegable("teso"); ?></tr>
    </table>
    <div class="bg-white group-btn p-1"><button type="button" onclick="location.href='teso-girarcheques-ccpet.php'"
            class="btn btn-white btn-primary-hover d-flex justify-between align-items-center">
            <span>Nuevo</span>
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 -960 960 960">
                <path d="M440-440H200v-80h240v-240h80v240h240v80H520v240h-80v-240Z"></path>
            </svg>
        </button><button type="button" onclick="guardar()"
            class="btn btn-white btn-primary-hover d-flex justify-between align-items-center">
            <span>Guardar</span>
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 -960 960 960">
                <path
                    d="M840-680v480q0 33-23.5 56.5T760-120H200q-33 0-56.5-23.5T120-200v-560q0-33 23.5-56.5T200-840h480l160 160Zm-80 34L646-760H200v560h560v-446ZM480-240q50 0 85-35t35-85q0-50-35-85t-85-35q-50 0-85 35t-35 85q0 50 35 85t85 35ZM240-560h360v-160H240v160Zm-40-86v446-560 114Z">
                </path>
            </svg>
        </button><button type="button" onclick="location.href='teso-buscagirarcheques-ccpet.php'"
            class="btn btn-white btn-primary-hover d-flex justify-between align-items-center">
            <span>Buscar</span>
            <svg class="fill-black group-hover:fill-white w-5 h-5" xmlns="http://www.w3.org/2000/svg"
                viewBox="0 -960 960 960">
                <path
                    d="M784-120 532-372q-30 24-69 38t-83 14q-109 0-184.5-75.5T120-580q0-109 75.5-184.5T380-840q109 0 184.5 75.5T640-580q0 44-14 83t-38 69l252 252-56 56ZM380-400q75 0 127.5-52.5T560-580q0-75-52.5-127.5T380-760q-75 0-127.5 52.5T200-580q0 75 52.5 127.5T380-400Z">
                </path>
            </svg>
        </button><button type="button" onclick="mypop=window.open('plan-agenda.php','','');mypop.focus()"
            class="btn btn-white btn-primary-hover d-flex justify-between align-items-center">
            <span class="group-hover:text-white">Agenda</span>
            <svg class="fill-black group-hover:fill-white w-5 h-5" xmlns="http://www.w3.org/2000/svg"
                viewBox="0 -960 960 960">
                <path
                    d="M560-564v-68q33-14 67.5-21t72.5-7q26 0 51 4t49 10v64q-24-9-48.5-13.5T700-600q-38 0-73 9.5T560-564Zm0 220v-68q33-14 67.5-21t72.5-7q26 0 51 4t49 10v64q-24-9-48.5-13.5T700-380q-38 0-73 9t-67 27Zm0-110v-68q33-14 67.5-21t72.5-7q26 0 51 4t49 10v64q-24-9-48.5-13.5T700-490q-38 0-73 9.5T560-454ZM260-320q47 0 91.5 10.5T440-278v-394q-41-24-87-36t-93-12q-36 0-71.5 7T120-692v396q35-12 69.5-18t70.5-6Zm260 42q44-21 88.5-31.5T700-320q36 0 70.5 6t69.5 18v-396q-33-14-68.5-21t-71.5-7q-47 0-93 12t-87 36v394Zm-40 118q-48-38-104-59t-116-21q-42 0-82.5 11T100-198q-21 11-40.5-1T40-234v-482q0-11 5.5-21T62-752q46-24 96-36t102-12q58 0 113.5 15T480-740q51-30 106.5-45T700-800q52 0 102 12t96 36q11 5 16.5 15t5.5 21v482q0 23-19.5 35t-40.5 1q-37-20-77.5-31T700-240q-60 0-116 21t-104 59ZM280-494Z">
                </path>
            </svg>
        </button><button type="button" onclick="window.open('teso-principal');"
            class="btn btn-white btn-primary-hover d-flex justify-between align-items-center">
            <span>Nueva ventana</span>
            <svg class="fill-black group-hover:fill-white w-5 h-5" xmlns="http://www.w3.org/2000/svg"
                viewBox="0 -960 960 960">
                <path
                    d="M320-120v-80h80v-80H160q-33 0-56.5-23.5T80-360v-400q0-33 23.5-56.5T160-840h640q33 0 56.5 23.5T880-760v400q0 33-23.5 56.5T800-280H560v80h80v80H320ZM160-360h640v-400H160v400Zm0 0v-400 400Z">
                </path>
            </svg>
        </button><button type="button"
            onclick="mypop=window.open('/financiero/teso-girarcheques-ccpet.php','','');mypop.focus();"
            class="btn btn-white btn-primary-hover d-flex justify-between align-items-center">
            <span class="group-hover:text-white">Duplicar pantalla</span>
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 -960 960 960">
                <path
                    d="M640-160v-360H160v360h480Zm80-200v-80h80v-360H320v200h-80v-200q0-33 23.5-56.5T320-880h480q33 0 56.5 23.5T880-800v360q0 33-23.5 56.5T800-360h-80ZM160-80q-33 0-56.5-23.5T80-160v-360q0-33 23.5-56.5T160-600h480q33 0 56.5 23.5T720-520v360q0 33-23.5 56.5T640-80H160Zm400-603ZM400-340Z">
                </path>
            </svg>
        </button></div>
    <div id="bgventanamodalm" class="bgventanamodalm">
        <div id="ventanamodalm" class="ventanamodalm">
            <IFRAME src="" name="ventanam" marginWidth=0 marginHeight=0 frameBorder=0 id="ventanam" frameSpacing=0
                style=" width:700px; height:130px; top:200; overflow:hidden;">
            </IFRAME>
        </div>
    </div>
    <form name="form2" method="post" action="">

        <?php
        $maxVersion = ultimaVersionGastosCCPET();
        $vigencia = vigencia_usuarios($_SESSION['cedulausu']);
        $sesion = $_SESSION['cedulausu'];
        $sqlr = "Select dominios.valor_final from usuarios,dominios where usuarios.cc_usu=$sesion and dominios.NOMBRE_DOMINIO='PERMISO_MODIFICA_DOC' and dominios.valor_inicial=usuarios.cc_usu ";
        $resp = mysqli_query($linkbd, $sqlr);
        $fechaBloqueo = mysqli_fetch_row($resp);
        echo "<input type='hidden' name='fechabloq' id='fechabloq' value='$fechaBloqueo[0]' />";
        $vigencia = $_POST['vigenciaop'];
        //*********** cuenta origen va al credito y la destino al debito
        if (!$_POST['oculto']) {
            $check1 = "checked";
            $fec = date("d/m/Y");
            $_POST['fecha'] = $fec;
            $_POST['vigencia'] = $_POST['vigenciaop'];

            $sqlr = "select max(id_egreso) from tesoegresos";
            $res = mysqli_query($linkbd, $sqlr);
            $row = mysqli_fetch_row($res);
            $_POST['egreso'] = $row[0] + 1;
            if ($_POST['egresoo'] != 1) {
                $_POST['tipomovimiento'] = '201';
                $_POST['tabgroup1'] = 1;
                $_POST['orden'] = '';
                unset($_POST['dcuentas']);
                unset($_POST['dncuentas']);
                unset($_POST['drecursos']);
                unset($_POST['dvalores']);
            }
        }
        switch ($_POST['tabgroup1']) {
            case 1:
                $check1 = 'checked';
                break;
            case 2:
                $check2 = 'checked';
                break;
            case 3:
                $check3 = 'checked';
                break;
        }
        if ($_POST['bop'] == '1') {
            if ($_POST['orden'] != '') {
                $vigencia = date('Y');
                //*** busca detalle cdp
                $sqlr = "select * from tesoordenpago where id_orden='$_POST[orden]' and estado='S' and tipo_mov='201' ";
                $resp = mysqli_query($linkbd, $sqlr);
                $row = mysqli_fetch_row($resp);
                $_POST['concepto'] = $row[7];
                $_POST['tercero'] = $row[6];
                $_POST['ntercero'] = buscatercero($_POST['tercero']);
                $_POST['tercerocta'] = buscatercero_cta($_POST['tercero']);

                if ($_POST['tercerocta'] == '') {
                    $sqlr_rp_banco = "SELECT banco FROM ccpetrp_banco WHERE vigencia = '$row[3]' AND consvigencia = '$row[4]'";
                    $resp_rp_banco = mysqli_query($linkbd, $sqlr_rp_banco);
                    $row_rp_banco = mysqli_fetch_row($resp_rp_banco);

                    $sqlr_ter_ban = "select tipo_cuenta, codigo_banco from teso_cuentas_terceros where numero_cuenta='$row_rp_banco[0]' ";
                    $res_ter_ban = mysqli_query($linkbd, $sqlr_ter_ban);
                    $r_ter_ban = mysqli_fetch_row($res_ter_ban);

                    $sqlr_banco = "SELECT nombre FROM hum_bancosfun WHERE codigo = '$r_ter_ban[1]'";
                    $res_banco = mysqli_query($linkbd, $sqlr_banco);
                    $r_banco = mysqli_fetch_row($res_banco);

                    $_POST['tercerocta'] = $r_banco[0] . " CTA DE " . $r_ter_ban[0] . " " . $row_rp_banco[0];
                }

                $_POST['valororden'] = $row[10];

                $_POST['retenciones'] = $row[12];
                $_POST['cc'] = $row[5];
                $_POST['vigenciaop'] = $row[3];
                $_POST['totaldes'] = number_format($_POST['retenciones'], 2);
                $_POST['valorpagar'] = doubleVal($_POST['valororden']) - doubleVal($_POST['retenciones']);
                $_POST['base'] = $row[14];
                $_POST['iva'] = $row[15];
                $_POST['bop'] = "";

                $_POST['medioDePago'] = $row[19];
                if ($_POST['medioDePago'] == '')
                    $_POST['medioDePago'] = '-1';
            } else {
                $_POST['cdp'] = "";
                $_POST['detallecdp'] = "";
                $_POST['tercero'] = "";
                $_POST['ntercero'] = "";
                $_POST['bop'] = "";
            }
        }
        if (isset($_POST['orden']) && isset($_POST['vigenciaop'])) {
            $_POST['saldocxp'] = generaSaldoCXP($_POST['orden'], $_POST['vigenciaop']);
        }

        ?>
        <table class="inicio">
            <tr>
                <td class="titulos" style="width:100%;">.: Tipo de Movimiento
                    <input type="hidden" value="1" name="oculto" id="oculto">

                    <select name="tipomovimiento" id="tipomovimiento" onKeyUp="return tabular(event,this)"
                        onChange="validar()" style="width:20%;">
                        <?php
                        $user = $_SESSION['cedulausu'];
                        $sql = "SELECT * from permisos_movimientos WHERE usuario='$user' AND estado='T' ";
                        $res = mysqli_query($linkbd, $sql);
                        $num = mysqli_num_rows($res);
                        if ($num == 1) {
                            $sqlr = "select * from tipo_movdocumentos where estado='S' and modulo=4 AND (id='2' OR id='4')";
                            $resp = mysqli_query($linkbd, $sqlr);
                            while ($row = mysqli_fetch_row($resp)) {
                                if ($_POST['tipomovimiento'] == $row[0] . $row[1]) {
                                    echo "<option value='$row[0]$row[1]' SELECTED >$row[0]$row[1]-$row[2]</option>";
                                } else {
                                    echo "<option value='$row[0]$row[1]'>$row[0]$row[1]-$row[2]</option>";
                                }
                            }
                        } else {
                            $sql = "SELECT codmov,tipomov from permisos_movimientos WHERE usuario='$user' AND estado='S' AND modulo='4' AND transaccion='TPB' ";
                            $res = mysqli_query($linkbd, $sql);
                            while ($row = mysqli_fetch_row($res)) {
                                if ($_POST['tipomovimiento'] == $row[0]) {
                                    echo "<option value='$row[0]' SELECTED>$row[0] - $row[1]</option>";
                                } else {
                                    echo "<option value='$row[0]'>$row[0] - $row[1]</option>";
                                }

                            }
                        }

                        ?>
                    </select>
                </td>
                <td style="width:80%;">
                </td>
            </tr>
        </table>
        <?php if ($_POST['tipomovimiento'] == '201') { ?>
            <div class="tabsic" style="height:68%; width:99.6%;">
                <div class="tab">
                    <input type="radio" id="tab-1" name="tabgroup1" value="1" <?php echo $check1; ?>>
                    <label for="tab-1">Egreso</label>
                    <div class="content" style="overflow-x:hidden;">
                        <table class="inicio" align="center">
                            <tr>
                                <td colspan="7" class="titulos">Comprobante de Egreso CCPET</td>
                                <td class="cerrar" style="width:7%;"><a
                                        onClick="location.href='teso-principal.php'">&nbsp;Cerrar</a></td>
                            </tr>
                            <tr>
                                <td class="saludo1" style="width:2.7cm;">N&deg; Egreso:</td>
                                <td style="width:16%">
                                    <input type="hidden" name="cuentapagar" value="<?php echo $_POST['cuentapagar'] ?>">
                                    <input type="hidden" name="medioDePago" value="<?php echo $_POST['medioDePago'] ?>">
                                    <input type="hidden" name="entidadAdministradora" id="entidadAdministradora"
                                        value="<?php echo $_POST['entidadAdministradora'] ?>">
                                    <input type="text" name="egreso" id="egreso" value="<?php echo $_POST['egreso'] ?>"
                                        onKeyUp="return tabular(event,this)" onBlur="buscarp(event)" style="width:56%"
                                        readonly>
                                    <input type="text" name="vigencia" value="<?php echo $_POST['vigencia'] ?>"
                                        onKeyUp="return tabular(event,this)" style="width:22%;" readonly>
                                </td>
                                <td class="saludo1" style="width:2cm;">Fecha: </td>
                                <td style="width:20%">

                                    <input type="text" name="fecha" value="<?php echo $_POST['fecha'] ?>"
                                        onKeyUp="return tabular(event,this)" id="fc_1198971545" title="DD/MM/YYYY"
                                        onDblClick="displayCalendarFor('fc_1198971545');" class="colordobleclik"
                                        autocomplete="off" onChange="" readonly>

                                </td>
                                <?php
                                if ($_POST['medioDePago'] != '2') {
                                    ?>
                                    <td class="saludo1" style="width:2.8cm;">Forma de Pago:</td>
                                    <td style="width:14%">
                                        <select name="tipop" id="tipop" onChange="validar();"
                                            onKeyUp="return tabular(event,this)" style="width:100%">
                                            <option value="">Seleccione ...</option>
                                            <option value="cheque" <?php if ($_POST['tipop'] == 'cheque')
                                                echo "SELECTED" ?>>Cheque
                                                </option>
                                                <option value="transferencia" <?php if ($_POST['tipop'] == 'transferencia')
                                                echo "SELECTED" ?>>Transferencia</option>
                                                <option value="caja" <?php if ($_POST['tipop'] == 'caja')
                                                echo "SELECTED" ?>>Efectivo
                                                </option>
                                            </select>
                                            <input type="hidden" id="codingreso" name="codingreso" value="">
                                        </td>
                                        <td rowspan="7" colspan="2"
                                            style="background:url(imagenes/cheque04.png); background-repeat:no-repeat; background-position:right; background-size: 90% 90%">
                                        </td>
                                    <?php
                                } else {
                                    $_POST['tipop'] = '';
                                    //echo "<td style='width:2.8cm;'></td><td style='width:14%'></td>";
                                    ?>
                                    <td class="saludo1" style="width:10%;">Medio de pago:</td>
                                    <td style="width:5%;">
                                        <input type="text" id="codingreso" name="codingreso"
                                            value="<?php echo $_POST['codingreso'] ?>" onKeyUp="return tabular(event,this)"
                                            onBlur="buscaing(event)" style="width:80%;" readonly>&nbsp;<a
                                            onClick="despliegamodal2('visible','5');" title="Listado de Ingresos"><img
                                                src="imagenes/find02.png" style="width:20px;cursor:pointer;" /></a>
                                        <input type="hidden" value="0" name="bin">
                                    </td>
                                    <td><input type="text" name="ningreso" id="ningreso" value="<?php echo $_POST['ningreso'] ?>"
                                            style="width:100%;" readonly></td>
                                    <?php
                                }
                                ?>

                            </tr>
                            <tr>
                                <td class="saludo1">N&deg; Orden Pago:</td>
                                <td>
                                    <input type="text" name="orden" id="orden" value="<?php echo $_POST['orden'] ?>"
                                        onKeyUp="return tabular(event,this)" onBlur="buscaop(event)" style="width:80%"
                                        readonly>
                                    <input type="hidden" value="0" name="bop">&nbsp;<a
                                        onClick="despliegamodal2('visible','1');" style="cursor:pointer;"
                                        title="Listado Ordenes Pago"><img src="imagenes/find02.png"
                                            style="width:20px;" /></a>
                                </td>
                                <td class="saludo1">Vigencia:</td>
                                <td><input type="text" name="vigenciaop" value="<?php echo $_POST['vigenciaop'] ?>"
                                        onKeyUp="return tabular(event,this)" readonly style="width:60%"></td>
                            </tr>
                            <tr>
                                <td class="saludo1">Tercero:</td>
                                <td><input id="tercero" type="text" name="tercero" onKeyUp="return tabular(event,this)"
                                        onBlur="buscater(event)" value="<?php echo $_POST['tercero'] ?>" style="width:80%"
                                        readonly></td>
                                <td colspan="2"><input name="ntercero" type="text" value="<?php echo $_POST['ntercero'] ?>"
                                        style="width:100%" readonly></td>
                                <td class="saludo1">Cuenta:</td>
                                <td><input name="tercerocta" type="text" value="<?php echo $_POST['tercerocta'] ?>"
                                        style="width:100%" readonly></td>
                            </tr>
                            <tr>
                                <td class="saludo1">Concepto:</td>
                                <td colspan="5">
                                    <textarea id="concepto" name="concepto"
                                        style="width:100%; height:40px;resize:none;color:#333;border-color:#ccc;"
                                        ><?php echo $_POST['concepto']; ?>
                                    </textarea>
                                </td>
                            </tr>
                            <?php
                            if ($_POST['tipop'] == 'cheque') //**** if del cheques
                            {
                                if ($_POST['escuentas'] == '' || $_POST['escuentas'] == 'tran') {
                                    $_POST['escuentas'] = 'che';
                                    $_POST['cb'] = '';
                                    $_POST['nbanco'] = '';
                                    $_POST['banco'] = '';
                                    $_POST['tcta'] = '';
                                    $_POST['ter'] = '';
                                    $_POST['ncheque'] = '';
                                }
                                echo "
		   							<tr>
	  									<td class='saludo1'>Cuenta Bancaria:</td>
	  									<td>
											<input type='text' name='cb' id='cb' value='$_POST[cb]' style='width:80%'/>
		 								&nbsp;<a onClick=\"despliegamodal2('visible','2');\"  style='cursor:pointer;' title='Listado Cuentas Bancarias'><img src='imagenes/find02.png' style='width:20px;'/></a>
								  		</td>
										<td colspan='2'><input type='text' id='nbanco' name='nbanco' value='$_POST[nbanco]' style='width:100%' readonly></td>
										<td class='saludo1'>Cheque:</td>
										<td>
											<input type='text' id='ncheque' name='ncheque' value='$_POST[ncheque]' style='width:100%'/>
											<input type='hidden' id='nchequeh' name='nchequeh' value='$_POST[nchequeh]'/>
										</td>
	  								</tr>
									<input type='hidden' name='banco' id='banco' value='$_POST[banco]'/>
									<input type='hidden' name='tcta' id='tcta' value='$_POST[tcta]'/>
								   	<input type='hidden' name='ter' id='ter' value='$_POST[ter]'/>";
                                //-----------Asignacion del consecutivo de cheque----------------------------
                                if ($_POST['cb'] != '') {
                                    $sqlc = "select cheque from tesocheques where cuentabancaria='$_POST[cb]' and estado='S' order by cheque asc";
                                    //echo $sqlc;
                                    $resc = mysqli_query($linkbd, $sqlc);
                                    $rowc = mysqli_fetch_row($resc);
                                    //echo "cheque: ".$rowc[0];
                                    if ($rowc[0] == '') {

                                    } else {
                                        echo "<script>document.form2.ncheque.value='" . $rowc[0] . "';</script>";
                                    }
                                }


                            }//cierre del if de cheques
                            if ($_POST['tipop'] == 'transferencia')//**** if del transferencias
                            {
                                if ($_POST['escuentas'] == '' || $_POST['escuentas'] == 'che') {
                                    $_POST['escuentas'] = 'tran';
                                    $_POST['cb'] = '';
                                    $_POST['nbanco'] = '';
                                    $_POST['banco'] = '';
                                    $_POST['tcta'] = '';
                                    $_POST['ter'] = '';
                                    $_POST['ntransfe'] = '';
                                }
                                echo "
	  								<tr>
	  									<td class='saludo1'>Cuenta Bancaria:</td>
	  									<td>
											<input type='text' name='cb' id='cb' value='$_POST[cb]' style='width:80%'/>
		 								&nbsp;<a onClick=\"despliegamodal2('visible','3');\"  style='cursor:pointer;' title='Listado Cuentas Bancarias'><img src='imagenes/find02.png' style='width:20px;'/></a>
									  	</td>
									   	<td colspan='2'><input type='text' id='nbanco' name='nbanco' value='$_POST[nbanco]' style='width:100%' readonly></td>
										<td class='saludo1'>No Transferencia:</td>
										<td><input type='text' id='ntransfe' name='ntransfe' value='$_POST[ntransfe]' style='width:100%'></td>
	  								</tr>
									<input type='hidden' name='banco' id='banco' value='$_POST[banco]'/>
								   	<input type='hidden' name='tcta' id='tcta' value='$_POST[tcta]'/>
								   	<input type='hidden' name='ter' id='ter' value='$_POST[ter]'/>";
                            }//cierre del if de cheques
                            if ($_POST['tipop'] == 'caja') {
                                if ($_POST['escuentas'] == '' || $_POST['escuentas'] == 'che') {
                                    $_POST['escuentas'] = 'tran';
                                    $_POST['cb'] = '';
                                    $_POST['nbanco'] = '';
                                    $_POST['banco'] = '';
                                    $_POST['tcta'] = '';
                                    $_POST['ter'] = '';
                                    $_POST['ntransfe'] = '';
                                }
                                $sqlr = "select cuentacaja from tesoparametros";
                                $res = mysqli_query($linkbd, $sqlr);
                                while ($row = mysqli_fetch_row($res)) {
                                    $_POST['banco'] = $row[0];
                                    $_POST['nbanco'] = buscacuenta($row[0]);
                                }
                                echo "
	  								<tr>
	  									<td class='saludo1'>Cuenta Caja:</td>
	  									<td>
											<input type='text' name='banco' id='banco' value='$_POST[banco]' style='width:80%' readonly/>
		 								&nbsp;
									  	</td>
									   	<td colspan='2'><input type='text' id='nbanco' name='nbanco' value='$_POST[nbanco]' style='width:100%' readonly></td>

	  								</tr>
									<input type='hidden' name='banco' id='banco' value='$_POST[banco]'/>";
                            }
                            ?>
                            <tr>
                                <td class="saludo1">Valor Orden:</td>
                                <td><input name="valororden" type="text" id="valororden"
                                        onKeyUp="return tabular(event,this)" value="<?php echo $_POST['valororden'] ?>"
                                        style="width:80%" readonly></td>
                                <td class="saludo1">Retenciones:</td>
                                <td><input name="retenciones" type="text" id="retenciones"
                                        onKeyUp="return tabular(event,this)" value="<?php echo $_POST['retenciones'] ?>"
                                        readonly></td>
                                <td class="saludo1">Valor a Pagar:</td>
                                <td><input name="valorpagar" type="text" id="valorpagar"
                                        onKeyUp="return tabular(event,this)" value="<?php echo $_POST['valorpagar'] ?>"
                                        style="width:100%" readonly> </td>

                            </tr>
                            <tr>
                                <td class="saludo1">Base:</td>
                                <td><input type="text" id="base" name="base" value="<?php echo $_POST['base'] ?>"
                                        onKeyUp="return tabular(event,this)" onChange='calcularpago()' style="width:80%"
                                        readonly> </td>
                                <td class="saludo1">Iva:</td>
                                <td><input type="text" id="iva" name="iva" value="<?php echo $_POST['iva'] ?>"
                                        onKeyUp="return tabular(event,this)" onChange='calcularpago()' readonly> <input
                                        type="hidden" id="regimen" name="regimen" value="<?php echo $_POST['regimen'] ?>">
                                    <input type="hidden" id="cc" name="cc" value="<?php echo $_POST['cc'] ?>"></td>
                                <td class="saludo1">Saldo Orden:</td>
                                <td><input name="saldocxp" id="saldocxp" value="<?php echo $_POST['saldocxp'] ?>" type="text"
                                        readonly /> </td>
                            </tr>
                        </table>
                        <div class="subpantallac4" style="width:99.6%; height:52%; overflow-x:hidden;">
                            <table class="inicio">
                                <tr>
                                    <td colspan="10" class="titulos">Detalle Orden de Pago</td>
                                </tr>
                                <tr>
                                    <td class='titulos2' style='text-align:center;'>Vig. Gasto</td>
                                    <td class='titulos2' style='text-align:center;'>Sec. presupuestal</td>
                                    <td class='titulos2' style='text-align:center;'>Medio pago</td>
                                    <td class="titulos2">Cuenta</td>
                                    <td class="titulos2">Nombre Cuenta</td>
                                    <td class="titulos2">Fuente</td>
                                    <td class='titulos2' style='text-align:center;'>Producto/Servicio</td>
                                    <td class='titulos2' style='width:10%;text-align:center;'>Programatico MGA</td>
                                    <td class='titulos2' style='text-align:center;'>BPIM</td>
                                    <td class="titulos2">Valor</td>

                                    <!-- <td class="titulos2" style="width:15%;">Cuenta</td>
                                    <td class="titulos2">Nombre Cuenta</td>
                                    <td class="titulos2">Recurso</td>
                                    <td class="titulos2" style="width:15%;">Valor</td> -->
                                </tr>
                                <?php
                                if ($_POST['elimina'] != '') {
                                    $posi = $_POST['elimina'];
                                    unset($_POST['dccs'][$posi]);
                                    unset($_POST['dvalores'][$posi]);
                                    $_POST['dccs'] = array_values($_POST['dccs']);
                                }
                                if ($_POST['agregadet'] == '1') {
                                    $_POST['dccs'][] = $_POST['cc'];
                                    $_POST['agregadet'] = '0';
                                    echo "
										<script>
											document.form2.banco.value='';
											document.form2.nbanco.value='';
											document.form2.banco2.value='';
											document.form2.nbanco2.value='';
											document.form2.cb.value='';
											document.form2.cb2.value='';
											document.form2.valor.value='';
											document.form2.numero.value='';
											document.form2.agregadet.value='0';
											document.form2.numero.select();
											document.form2.numero.focus();
										</script>";
                                }
                                $_POST['totalc'] = 0;

                                // echo $sqlr;
                                $_POST['dvalores'] = array();
                                $_POST['dcuentas'] = array();
                                $_POST['dvaloresoc'] = array();
                                $_POST['dncuentas'] = array();
                                $_POST['dvigenciaGasto'] = array();
                                $_POST['dseccion_presupuestal'] = array();
                                $_POST['dtipo_gasto'] = array();
                                $_POST['ddivipola'] = array();
                                $_POST['dchip'] = array();
                                $_POST['rubros'] = array();
                                $_POST['inicios'] = array();
                                $_POST['drecursos'] = array();
                                $_POST['dnrecursos'] = array();
                                $_POST['productoServicio'] = array();
                                $_POST['indicador_producto'] = array();
                                $_POST['medio_pago'] = array();
                                $_POST['bpim'] = array();
                                $_POST['sector'] = array();
                                $_POST['nombreSector'] = array();

                                $iter = 'saludo1a';
                                $iter2 = 'saludo2';

                                $sqlr = "select * from tesoordenpago_det where id_orden='$_POST[orden]' and estado='S' and tipo_mov='201' AND id_orden > 0;";
                                $resp2 = mysqli_query($linkbd, $sqlr);
                                while ($row2 = mysqli_fetch_row($resp2)) {
                                    $_POST['dcuentas'][] = $row2[2];
                                    $_POST['dvaloresoc'][] = $row2[4];
                                    $_POST['dvalores'][] = $row2[4];
                                    $_POST['dncuentas'][] = buscacuentaccpetgastos($row2[2], $maxVersion);
                                    $_POST['dvigenciaGasto'][] = $row2[12];
                                    $_POST['dseccion_presupuestal'][] = $row2[15];
                                    $_POST['dtipo_gasto'][] = $row2[16];
                                    $_POST['ddivipola'][] = $row2[17];
                                    $_POST['dchip'][] = $row2[18];
                                    $_POST['rubros'][] = $row2[2];
                                    $_POST['inicios'][] = $row2[2];
                                    $nfuente = buscafuenteccpet($row2[8]);
                                    $cdfuente = substr($nfuente, 0, strpos($nfuente, "_"));
                                    $_POST['drecursos'][] = $row2[8];

                                    $_POST['productoServicio'][] = $row2[9];
                                    $_POST['indicador_producto'][] = $row2[10];
                                    $_POST['medio_pago'][] = $row2[11];
                                    $_POST['bpim'][] = $row2[14];
                                    $indicadorProducto = substr($row2[10], 0, 2);
                                    $_POST['dnrecursos'][] = $nfuente;
                                    $_POST['sector'][] = $indicadorProducto;
                                    $_POST['nombreSector'][] = buscaNombreSector($indicadorProducto);

                                    $tamProductoServicio = strlen($row2[9]);
                                    if ($tamProductoServicio == 5) {
                                        $_POST['nombreProductoServicio'][] = buscaservicioccpetgastos($row2[9]);
                                    } else {
                                        $_POST['nombreProductoServicio'][] = buscaproductoccpetgastos($row2[9]);
                                    }

                                    //$nombre=buscacuentapres($row2[2],2);
                                    /* $nombre = buscacuentaccpetgastos($row2[2]);
                                                                  $nfuente = buscafuenteccpet($row2[8]);
                                                                  $cdfuente = substr($nfuente,0,strpos($nfuente,"_"));
                                                                  $_POST['dvalores'][] = $row2[4];
                                                                  $_POST['dcuentas'][] = $row2[2];
                                                                  $_POST['dncuentas'][] = $nombre;
                                                                  $_POST['drecursos'][] = $row2[8];
                                                                  $_POST['productoServicio'][] = $row2[9];
                                                                  $_POST['indicador_producto'][] = $row2[10]; */

                                    /* echo "
                                                                  <input type='hidden' name='dcuentas[]' value='$row2[2]'/>
                                                                  <input type='hidden' name='dncuentas[]' value='$nombre'/>
                                                                  <input type='hidden' name='drecursos[]' value='$row2[8]'/>
                                                                  <input type='hidden' name='productoServicio[]' value='$row2[9]'/>
                                                                  <input type='hidden' name='indicador_producto[]' value='$row2[10]'/>
                                                                  <input type='hidden' name='dvalores[]' value='$row2[4]'/>
                                                                  <tr class='$iter' onMouseOver=\"anterior=this.style.backgroundColor; this.style.backgroundColor='#40b3ff';\" onMouseOut=\"this.style.backgroundColor=anterior\">
                                                                      <td >$row2[2]</td>
                                                                      <td >$nombre</td>
                                                                      <td>$nfuente</td>
                                                                      <td style='text-align:right;' onDblClick='llamarventanaegre(this,$x);'>".number_format($row2[4],$_SESSION["ndecimales"],$_SESSION["spdecimal"],$_SESSION["spmillares"])."</td>
                                                                  </tr>";
                                                                  $_POST['totalc'] = $_POST['totalc']+$row2[4];
                                                                  $_POST['totalcf'] = number_format($_POST['totalc'],$_SESSION["ndecimales"],$_SESSION["spdecimal"],$_SESSION["spmillares"]);

                                                                  $aux=$iter;
                                                                  $iter=$iter2;
                                                                  $iter2=$aux; */

                                }

                                for ($x = 0; $x < count($_POST['dcuentas']); $x++) {
                                    //$_POST[dcuentas][]=$row2[2];
                                    //$nombre=buscacuentaccpetgastos($row2[2]);
                                    //$_POST[dvalores][]=$row2[4];
                                    echo "
											<input type='hidden' name='dcuentas[]' value='" . $_POST['dcuentas'][$x] . "'/>
											<input type='hidden' name='dregalias[]' value='" . $ar[0] . "'/>
											<input type='hidden' name='dncuentas[]' value='" . $_POST['dncuentas'][$x] . "'/>
											<input type='hidden' name='dvigenciaGasto[]' value='" . $_POST['dvigenciaGasto'][$x] . "'/>
											<input type='hidden' name='dseccion_presupuestal[]' value='" . $_POST['dseccion_presupuestal'][$x] . "'/>
											<input type='hidden' name='dtipo_gasto[]' value='" . $_POST['dtipo_gasto'][$x] . "'/>
											<input type='hidden' name='ddivipola[]' value='" . $_POST['ddivipola'][$x] . "'/>
											<input type='hidden' name='dchip[]' value='" . $_POST['dchip'][$x] . "'/>
											<input type='hidden' name='drecursos[]' value='" . $_POST['drecursos'][$x] . "'/>
											<input type='hidden' name='dnrecursos[]' value='" . $_POST['dnrecursos'][$x] . "'/>
											<input type='hidden' name='productoServicio[]' value='" . $_POST['productoServicio'][$x] . "'/>
											<input type='hidden' name='nombreProductoServicio[]' value='" . $_POST['nombreProductoServicio'][$x] . "'/>
											<input type='hidden' name='indicador_producto[]' value='" . $_POST['indicador_producto'][$x] . "'/>
											<input type='hidden' name='bpim[]' value='" . $_POST['bpim'][$x] . "'/>
											<input type='hidden' name='medio_pago[]' value='" . $_POST['medio_pago'][$x] . "'/>
											<input type='hidden' name='sector[]' value='" . $_POST['sector'][$x] . "'/>
											<input type='hidden' name='nombreSector[]' value='" . $_POST['nombreSector'][$x] . "'/>
											<input type='hidden' name='dvalores[]' value='" . $_POST['dvalores'][$x] . "'/>
											<input type='hidden' name='dvaloresoc[]' value='" . $_POST['dvaloresoc'][$x] . "'/>
											<tr  class='$iter' onMouseOver=\"anterior=this.style.backgroundColor;this.style.backgroundColor='#40b3ff';\"
						onMouseOut=\"this.style.backgroundColor=anterior\" style='text-transform:uppercase'>

												<td>" . $_POST['dvigenciaGasto'][$x] . "</td>
												<td>" . $_POST['dseccion_presupuestal'][$x] . "</td>
												<td>" . $_POST['medio_pago'][$x] . "</td>
												<td style='width:10%;'>" . $_POST['dcuentas'][$x] . "</td>
												<td style='width:30%;'>" . $_POST['dncuentas'][$x] . "</td>
												<td style='width:15%;'>" . $_POST['drecursos'][$x] . " - " . $_POST['dnrecursos'][$x] . "</td>
												<td style='width:15%;'>" . $_POST['productoServicio'][$x] . " - " . $_POST['nombreProductoServicio'][$x] . "</td>
												<td style='width:6%;'>" . $_POST['indicador_producto'][$x] . "</td>
												<td style='width:8%;'>" . $_POST['bpim'][$x] . "</td>

												<td style='text-align:right;width:15%;' ";
                                    if ($ch == '1') {
                                        echo "onDblClick='llamarventanaegre(this,$x);'";
                                    }
                                    echo " >$ " . number_format($_POST['dvalores'][$x], 2, '.', ',') . "</td>
											</tr>";

                                    $_POST['totalc'] = $_POST['totalc'] + $_POST['dvalores'][$x];
                                    $_POST['totalcf'] = number_format($_POST['totalc'], 2, ".", ",");
                                    $aux = $iter;
                                    $iter = $iter2;
                                    $iter2 = $aux;
                                }
                                $resultado = convertir($_POST['valorpagar']);
                                $_POST['letras'] = $resultado . " PESOS M/CTE";
                                if (isset($_POST['totalcf'])) {
                                    echo "
										<input type='hidden' name='totalcf' value='$_POST[totalcf]'/>
										<input type='hidden' name='totalc' value='$_POST[totalc]'/>
										<input name='letras' type='hidden' value='$_POST[letras]'/>
										<tr class='$iter' style='text-align:right;font-weight:bold;'>
											<td colspan='9'>Total:</td>
											<td>$_POST[totalcf]</td>
										</tr>
										<tr class='titulos2'>
											<td>Son:</td>
											<td colspan='10'>$_POST[letras]</td>
										</tr>";
                                }

                                ?>
                                <script>
                                    document.form2.valor.value = <?php echo $_POST['totalc']; ?>;
                                    //calcularpago();
                                </script>
                            </table>
                        </div>
                    </div>

                </div>
                <input type="hidden" name="escuentas" id="escuentas" value="<?php echo $_POST['escuentas']; ?>" />
                <div class="tab">
                    <input type="radio" id="tab-2" name="tabgroup1" value="2" <?php echo $check2; ?>>
                    <label for="tab-2">Retenciones</label>
                    <div class="content" style="overflow-x:hidden;">
                        <table class="inicio" style="overflow:scroll">
                            <tr>
                                <td class="titulos" colspan="3">Retenciones</td>
                            </tr>
                            <tr>
                                <td class="titulos2">Descuento</td>
                                <td class="titulos2">%</td>
                                <td class="titulos2">Valor</td>
                            </tr>
                            <input type="hidden" id="totaldes" name="totaldes" value="<?php echo $_POST['totaldes'] ?>"
                                readonly>

                            <?php
                            if ($_POST['oculto'] != '') {

                                $totaldes = 0;
                                $_POST['dndescuentos'] = array();
                                $_POST['dporcentajes'] = array();

                                $gdndescuentos = array();
                                $gddescuentos = array();
                                $gdporcentajes = array();
                                $gddesvalores = array();
                                $sqlr = "select *from tesoordenpago_retenciones where id_orden=$_POST[orden] and estado='S'";

                                $resd = mysqli_query($linkbd, $sqlr);
                                $iter = 'saludo1a';
                                $iter2 = 'saludo2';
                                $cr = 0;
                                while ($rowd = mysqli_fetch_row($resd)) {
                                    $sqlr2 = "SELECT *from tesoretenciones where id=" . $rowd[0];
                                    $resd2 = mysqli_query($linkbd, $sqlr2);
                                    $rowd2 = mysqli_fetch_row($resd2);
                                    $gdndescuentos[$cr] = "$rowd2[1] - $rowd2[2]";
                                    $gddescuentos[$cr] = $rowd2[1];
                                    $gdporcentajes[$cr] = $rowd[2];
                                    $gddesvalores[$cr] = round($rowd[3], 0);
                                    echo "
										<input type='hidden' name='dndescuentos[]' value='$rowd2[1] - $rowd2[2]'/>
										<input type='hidden' name='ddescuentos[]' value='$rowd[0]'/>
										<input type='hidden' name='dporcentajes[]' value='$rowd[2]'/>
										<input type='hidden' name='ddesvalores[]' value='" . round($rowd[3], 0) . "'/>
										<tr class='$iter' onMouseOver=\"anterior=this.style.backgroundColor; this.style.backgroundColor='#40b3ff';\" onMouseOut=\"this.style.backgroundColor=anterior\">
											<td>$rowd2[1] - $rowd2[2]</td>
											<td style='width:10%;'>$rowd[2]</td>
											<td style='text-align:right;width:15%;'>$ " . number_format(round($rowd[3], 0), $_SESSION["ndecimales"], $_SESSION["spdecimal"], $_SESSION["spmillares"]) . "</td>
										</tr>";
                                    $totaldes = $totaldes + ($rowd[3]);
                                    $aux = $iter;
                                    $iter = $iter2;
                                    $iter2 = $aux;
                                    $cr = $cr + 1;
                                }
                                $_POST['contrete'] = $cr;
                            }
                            echo "
								<tr class='$iter' style='text-align:right;font-weight:bold;'>
									<td colspan='2'>Total:</td>
								 	<td>$ $_POST[totaldes]</td>
						   		</tr>";
                            echo "<script>document.form2.totaldes.value='$totaldes';calcularpago();</script>";
                            ?>
                            <input type='hidden' name='contrete' value="<?php echo $_POST['contrete'] ?>" />
                        </table>

                    </div>
                </div>

                <div class="tab">
                    <input type="radio" id="tab-3" name="tabgroup1" value="3" <?php echo $check3; ?>>
                    <label for="tab-3">Contabilidad</label>
                    <div class="content" style="overflow-x:hidden;">
                        <table class="inicio">
                            <tr>
                                <td colspan="11" class="titulos">Parametrizar Progranacion Contable</td>
                            </tr>
                            <?php
                            if ($_POST['todos'] == 1) {
                                $checkt = 'checked';
                            } else {
                                $checkt = '';
                            }
                            if ($_POST['inicios'] == 1) {
                                $checkint = 'checked';
                            } else {
                                $checkint = '';
                            }
                            ?>
                            <tr>
                                <td class='titulos2' style='text-align:center;'>Vig. Gasto</td>
                                <td class='titulos2' style='text-align:center;'>Sec. presupuestal</td>
                                <td class='titulos2' style='text-align:center;'>Cuenta</td>
                                <td class='titulos2' style='text-align:center;'>Nombre Cuenta</td>
                                <td class='titulos2' style='text-align:center;'>Fuente</td>
                                <td class='titulos2' style='text-align:center;'>Sector</td>
                                <td class='titulos2' style='text-align:center;'>Producto/Servicio</td>
                                <td class='titulos2' style='text-align:center;'>Programativo MGA</td>
                                <td class='titulos2' style='text-align:center;'>Medio de Pago</td>
                                <td class='titulos2' style='text-align:center;'>Valor</td>
                                <td class='titulos2' style='text-align:center;'>Cuenta debito</td>
                            </tr>
                            <?php
                            $_POST['totalc'] = 0;
                            $iter = 'saludo1a';
                            $iter2 = 'saludo2';
                            for ($x = 0; $x < count($_POST['dcuentas']); $x++) {
                                $chk = '';
                                $sqlrCuentaCont = "SELECT cuenta_cred FROM tesoordenpago_cuenta WHERE id_orden = '$_POST[orden]' AND rubro = '" . $_POST['dcuentas'][$x] . "' AND productoservicio = '" . $_POST['productoServicio'][$x] . "' AND fuente = '" . $_POST['drecursos'][$x] . "' AND indicador_producto = '" . $_POST['indicador_producto'][$x] . "' AND codigo_vigenciag = '" . $_POST['dvigenciaGasto'][$x] . "' AND medio_pago = '" . $_POST['medio_pago'][$x] . "' AND seccion_presupuestal = '" . $_POST['dseccion_presupuestal'][$x] . "' AND bpim = '" . $_POST['bpim'][$x] . "'";//echo $sqlrCuentaCont."<br>";

                                $resCuentaCont = mysqli_query($linkbd, $sqlrCuentaCont);
                                $rowCuentaCont = mysqli_fetch_row($resCuentaCont);


                                echo "
										<tr  class='$iter' style=\"cursor: hand\"  onMouseOver=\"anterior=this.style.backgroundColor;this.style.backgroundColor='#40b3ff';\"
					onMouseOut=\"this.style.backgroundColor=anterior\" style='text-transform:uppercase'>
											<td style='width:10%;'>" . $_POST['dvigenciaGasto'][$x] . "</td>
											<td style='width:10%;'>" . $_POST['dseccion_presupuestal'][$x] . "</td>
											<td style='width:10%;'>" . $_POST['dcuentas'][$x] . "</td>
											<td style='width:20%;'>" . $_POST['dncuentas'][$x] . "</td>
											<td style='width:20%;'>" . $_POST['drecursos'][$x] . " - " . $_POST['dnrecursos'][$x] . "</td>
											<td style='width:15%;'>" . $_POST['sector'][$x] . " - " . $_POST['nombreSector'][$x] . "</td>
											<td style='width:15%;'>" . $_POST['productoServicio'][$x] . " - " . $_POST['nombreProductoServicio'][$x] . "</td>
											<td style='width:8%;'>" . $_POST['indicador_producto'][$x] . "</td>
											<td style='width:4%;'>" . $_POST['medio_pago'][$x] . "</td>
											<td style='width:10%;'>" . number_format($_POST['dvalores'][$x], 2, ',', '.') . "</td>
											<td style='width:10%;'><input name='cuentaDebito[]' class='inpnovisibles' value='" . $rowCuentaCont[0] . "' readonly></td>

										</tr>";
                                $aux = $iter;
                                $iter = $iter2;
                                $iter2 = $aux;
                            }
                            $resultado = convertir($_POST['totalc']);
                            $_POST['letras'] = $resultado . " PESOS M/CTE";
                            $_POST['totalcf'] = number_format($_POST['totalc'], 2, ".", ",");


                            ?>
                            <script>
                                document.form2.valor.value = <?php echo $_POST['totalc']; ?>;
                                document.form2.valoregreso.value = <?php echo $_POST['totalc']; ?>;
                                document.form2.valorcheque.value = document.form2.valor.value - document.form2.valorretencion.value;
                            </script>
                        </table>
                    </div>
                </div>
            </div>
        <?php }
        if ($_POST['tipomovimiento'] == '401') {
            $sqlrEgreso = "SELECT fecha FROM tesoegresos WHERE id_egreso='$_POST[egreso]'";
            $resEgreso = mysqli_query($linkbd, $sqlrEgreso);
            $rowEgreso = mysqli_fetch_row($resEgreso);
            $_POST['fechaComp'] = $rowEgreso[0];
            ?>
            <table class="inicio">
                <tr>
                    <td class="titulos" colspan="6">.: Documento a Reversar</td>
                </tr>
                <tr>
                    <td class="saludo1" style="width:10%;">Numero Egreso:</td>
                    <td style="width:10%;">
                        <input type="hidden" name="egresoo" id="egresoo" value="<?php echo $_POST['egresoo'] ?>">
                        <input type="hidden" name="fechaComp" id="fechaComp" value="<?php echo $_POST['fechaComp']; ?>">
                        <input type="text" name="egreso" id="egreso" value="<?php echo $_POST['egreso']; ?>"
                            style="width:80%;" onKeyUp="return tabular(event,this)" onBlur="validar2()" readonly>
                        <a href="#" onClick="despliegamodal2('visible','4','<?php echo $_POST['vigencia'] ?>');"
                            title="Buscar CxP"><img src="imagenes/find02.png" style="width:20px;"></a>
                        <input type="hidden" name="vigencia" value="<?php echo $_POST['vigencia'] ?>">
                    </td>
                    <td class="saludo1" style="width:10%;">Fecha:</td>
                    <td style="width:10%;">

                        <input type="text" name="fecha" value="<?php echo $_POST['fecha'] ?>"
                            onKeyUp="return tabular(event,this)" id="fc_1198971545" title="DD/MM/YYYY"
                            onDblClick="displayCalendarFor('fc_1198971545');" class="colordobleclik" autocomplete="off"
                            onChange="" readonly>

                    </td>
                    <td class="saludo1" style="width:10%;">Descripcion:</td>
                    <td style="width:60%;" colspan="3">
                        <input type="text" name="descripcion" id="descripcion" value="<?php echo $_POST['descripcion'] ?>"
                            style="width:100%;">
                    </td>
                </tr>
                <tr>
                    <td class="saludo1">No CxP:</td>
                    <td><input type="text" name="orden" id="orden" value="<?php echo $_POST['orden']; ?>" readonly></td>
                    <td class="saludo1">Valor Egreso:</td>
                    <td><input type="text" name="valoregreso" id="valoregreso" value="<?php echo $_POST['valoregreso']; ?>"
                            readonly></td>
                </tr>
            </table>
        <?php } ?>

        <?php
        function generaRetenciones($orden, $valor)
        {
            $linkbd = conectar_v7();
            $arreglocuenta = array();
            $total = 0;
            $sqlRete = "SELECT id_retencion, valor, porcentaje FROM tesoordenpago_retenciones WHERE id_orden = '$orden' ";
            $resRete = mysqli_query($linkbd, $sqlRete);
            while ($rowRete = mysqli_fetch_row($resRete)) {
                //$dd=$_POST['ddescuentos'][$x];
                $sqlr = "select tesoretenciones_det.porcentaje, tesoretenciones.iva, tesoretenciones.retencion, tesoretenciones.codigo from tesoretenciones_det,tesoretenciones where tesoretenciones_det.codigo=tesoretenciones.id and tesoretenciones.id='$rowRete[0]' ORDER BY tesoretenciones_det.porcentaje DESC";
                $resdes = mysqli_query($linkbd, $sqlr);
                $valordes = 0;
                $valorProcentaje = 0;
                $idRetencion = '';
                $val2 = 0;
                while ($rowdes = mysqli_fetch_row($resdes)) {
                    if ($idRetencion == $rowdes[3]) {
                        $valorProcentaje = $val2 + $rowdes[0];
                    } else {
                        $valorProcentaje = $rowdes[0];
                        $idRetencion = $rowdes[3];
                    }

                    $val2 = 0;
                    $val2 = $rowdes[0];

                    if ($valorProcentaje <= 100) {
                        if ($rowdes[1] == 1) {
                            $valordes = round(($valor / $_POST['valororden']) * ($_POST['iva'] * ($rowdes[2] / 100)), 0);
                            $total += $valordes;
                        } else {
                            $valordes = round(($valor / $_POST['valororden']) * (($rowdes[0] / 100) * $rowRete[1]), 0);
                            $total += $valordes;
                        }
                    }
                }
            }
            return $total;
        }
        if ($_POST['oculto'] == '2') {
            $query = "SELECT conta_pago FROM tesoparametros";
            $resultado = mysqli_query($linkbd, $query);
            $arreglo = mysqli_fetch_row($resultado);
            $opcion = $arreglo[0];

            preg_match("/([0-9]{2})\/([0-9]{2})\/([0-9]{4})/", $_POST['fecha'], $fecha);
            $fechaf = $fecha[3] . "-" . $fecha[2] . "-" . $fecha[1];

            $vigegreso = $fecha[3];
            $bloq = bloqueos($_SESSION['cedulausu'], $fechaf);

            if ($bloq >= 1) {

                if ($_POST['tipomovimiento'] == '201') {
                    // echo "hola";
                    $_POST['egreso'] = selconsecutivo('tesoegresos', 'id_egreso');
                    echo "<script>document.getElementById('egreso').value=" . $_POST['egreso'] . ";</script>";

                    //ereg( "([0-9]{1,2})/([0-9]{1,2})/([0-9]{2,4})", $_POST['fecha'],$fecha);
                    //$fechaf=$fecha[3]."-".$fecha[2]."-".$fecha[1];


                    if ($_POST['tipop'] == 'cheque') {
                        $vartipos = $_POST['ncheque'];
                    }
                    if ($_POST['tipop'] == 'transferencia') {
                        $vartipos = $_POST['ntransfe'];
                    }


                    // echo "hola 1";
                    //************CREACION DEL COMPROBANTE CONTABLE ************************
                    $sqlr = "select count(*) from tesoegresos where id_orden=$_POST[orden] and estado ='S'";
                    $res = mysqli_query($linkbd, $sqlr);
                    $row = mysqli_fetch_row($res);
                    $nreg = $row[0];
                    if ($nreg == 0) {
                        $sqlr = "insert into tesoegresos (id_egreso,id_orden,fecha,vigencia,valortotal,retenciones,valorpago,concepto,banco,cheque, tercero, cuentabanco,estado,pago,tipo_propa,tipo_mov,user) values ('$_POST[egreso]','$_POST[orden]','$fechaf','$vigegreso','$_POST[valororden]','$_POST[retenciones]', '$_POST[valorpagar]','$_POST[concepto]','$_POST[banco]','$vartipos','$_POST[tercero]','$_POST[cb]','S','$_POST[tipop]','$_POST[codingreso]','201','" . $_SESSION['nickusu'] . "')";

                        if (!mysqli_query($linkbd, $sqlr)) {
                            echo "<script>despliegamodalm('visible','2','Manejador de Errores de la Clase BD, No se pudo ejecutar la petici�n');</script>";
                        } else {
                            $query = "update tesoordenpago_retenciones set pasa_ppto='S' WHERE id_orden=" . $_POST['orden'];
                            mysqli_query($linkbd, $query);
                            $ideg = $_POST['egreso'];
                            $sqlr = "update tesochequeras set consecutivo=consecutivo+1 where cuentabancaria='$_POST[cb]'  and estado='S'";
                            mysqli_query($linkbd, $sqlr);
                            $sqlr = "update tesocheques set estado='P', destino='EGRESO', idcomp='$_POST[egreso]' where cuentabancaria='$_POST[cb]' and cheque='$_POST[ncheque]'";
                            mysqli_query($linkbd, $sqlr);
                            //ppto


                            $sqlr = "insert into tesoegresos_cheque (id_cheque,id_egreso,estado,motivo) values ('$vartipos',$ideg,'S','')";
                            mysqli_query($linkbd, $sqlr);
                            $sqlr = "update tesoordenpago set estado='P' where id_orden=$_POST[orden] and estado='S' and tipo_mov='201' ";
                            mysqli_query($linkbd, $sqlr);
                            //*****hacer la afectacion presupuestal
                            $sqlr = "select *from tesoordenpago_det where id_orden=$_POST[orden] and tipo_mov='201'";
                            //echo "1 $sqlr <br> ";
                            $resp2 = mysqli_query($linkbd, $sqlr);
                            while ($row2 = mysqli_fetch_row($resp2)) {
                                $sqlr = "insert into pptorecibopagoppto  (cuenta,idrecibo,valor,vigencia) values ('$row2[2]','$ideg','$row2[4]', '$vigegreso')";
                                //echo "2 $sqlr <br> ";
                                mysqli_query($linkbd, $sqlr);
                                /* $sqlr="UPDATE  pptocuentaspptoinicial SET pagos=pagos+$row2[4] where (vigencia='$vigegreso' or vigenciaf='$vigegreso') and cuenta='$row2[2]' and vigenciaf='$vigegreso'";
                                                                 mysqli_query($linkbd, $sqlr);
                                                                 $sqlr="UPDATE  pptocuentaspptoinicial SET cxp=cxp-$row2[4] where (vigencia='$vigegreso' or vigenciaf='$vigegreso') and cuenta='$row2[2]' and vigenciaf='$vigegreso'";
                                                                 mysqli_query($linkbd, $sqlr); */



                            }

                            //***********crear el contabilidad
                            $sqlr = "insert into comprobante_cab (numerotipo,tipo_comp,fecha,concepto,total,total_debito,total_credito,diferencia,estado) values ($ideg,6,'$fechaf','$_POST[concepto]',0, $_POST[valororden],$_POST[valororden],0,'1')";
                            mysqli_query($linkbd, $sqlr);
                            $idcomp = selconsecutivo('comprobante_cab', 'id_comp');
                            $sqlr = "update tesoegresos set id_comp=$idcomp where id_egreso=$ideg and tipo_mov='201' ";
                            mysqli_query($linkbd, $sqlr);
                            $valopb = 0;
                            $sqt = "SELECT *FROM tesoordenpago WHERE id_orden='$_POST[orden]' AND tipo_mov='201'";
                            $resp = mysqli_query($linkbd, $sqt);
                            $rw = mysqli_fetch_assoc($resp);
                            for ($j = 0; $j < count($_POST['dcuentas']); $j++) {
                                if ($opcion == '2') {
                                    $cantDescuentos = count($gdndescuentos);
                                    for ($x = 0; $x < $cantDescuentos; $x++) {
                                        $sqlr = "select * from tesoretenciones,tesoretenciones_det where tesoretenciones_det.codigo=tesoretenciones.id and tesoretenciones.codigo='" . $gddescuentos[$x] . "' ORDER BY porcentaje DESC";
                                        //echo $sqlr."<br>";
                                        $resdes = mysqli_query($linkbd, $sqlr);
                                        $valordes = 0;
                                        $val2 = 0;
                                        $valorProcentaje = 0;
                                        $idRetencion = '';
                                        while ($rowdes = mysqli_fetch_assoc($resdes)) {
                                            $valordes = 0;
                                            if ($idRetencion == $rowdes['codigo']) {
                                                $valorProcentaje = $val2 + $rowdes['porcentaje'];
                                            } else {
                                                $valorProcentaje = $rowdes['porcentaje'];
                                                $idRetencion = $rowdes['codigo'];
                                            }
                                            $val2 = 0;
                                            $val2 = $rowdes['porcentaje'];

                                            if ($rowdes['iva'] > 0 && $rowdes['terceros'] == 1) {
                                                $val1 = $_POST['dvalores'][$j];
                                                $val3 = $gddesvalores[$x];
                                                $valordes = round(($val1 / $_POST['valororden']) * ($val2 / 100) * $val3, 0);
                                                //echo "$val1 ---> ".$_POST['valor']." ---> $val2 ------> $val3";
                                                //$valordes = round(($val2/100)*$val3,0);
                                            } else {
                                                $val1 = $_POST['dvalores'][$j];
                                                $val3 = $gddesvalores[$x];
                                                $valordes = round(($val1 / $_POST['valororden']) * ($val2 / 100) * $val3, 0);
                                                //echo "$val1 ---> ".$_POST['valor']." ---> $val2 ------> $val3";
                                                //$valordes = round(($val2/100)*$val3,0);
                                            }

                                            /* echo $valorProcentaje."<br>"; */
                                            if ($valorProcentaje <= 100) {
                                                $valopb += $valordes;
                                            }

                                            $sec_presu = intval($_POST['dseccion_presupuestal'][$j]);
                                            $sqlrCc = "SELECT id_cc FROM centrocostos_seccionpresupuestal WHERE (id_sp = $sec_presu OR id_sp = '" . $_POST['dseccion_presupuestal'][$j] . "')";
                                            $resCc = mysqli_query($linkbd, $sqlrCc);
                                            $rowCc = mysqli_fetch_row($resCc);
                                            $cc = $rowCc[0];


                                            $codigoRetencion = 0;
                                            $rest = 0;
                                            $codigoCausa = 0;
                                            if ($_POST['medioDePago'] != '1') {
                                                //concepto contable //********************************************* */
                                                $rest = substr($rowdes['tipoconce'], 0, 2);
                                                $sq = "select fechainicial from conceptoscontables_det where codigo='" . $rowdes['conceptocausa'] . "' and modulo='" . $rowdes['modulo'] . "' and tipo='$rest' and fechainicial<'$fechaf' and cuenta!='' order by fechainicial asc";
                                                $re = mysqli_query($linkbd, $sq);
                                                while ($ro = mysqli_fetch_assoc($re)) {
                                                    $_POST['fechacausa'] = $ro["fechainicial"];
                                                }
                                                $sqlr = "select * from conceptoscontables_det where codigo='" . $rowdes['conceptocausa'] . "' and modulo='" . $rowdes['modulo'] . "' and cc='" . $cc . "' and tipo='$rest' and fechainicial='" . $_POST['fechacausa'] . "'";
                                                //echo $sqlr." hoal <br>";
                                                $rst = mysqli_query($linkbd, $sqlr);
                                                $row1 = mysqli_fetch_assoc($rst);
                                                if ($row1['cuenta'] != '' && $valordes > 0) {
                                                    //echo "Hola 5  ";
                                                    $sqlr = "insert into comprobante_det (id_comp,cuenta,tercero,centrocosto,detalle, cheque,valdebito, valcredito, estado, vigencia) values ('6 $ideg','" . $row1['cuenta'] . "','" . $_POST['tercero'] . "' ,'" . $cc . "' , 'Descuento " . $gdndescuentos[$x] . "',''," . $valordes . ",0,'1' ,'" . $vigegreso . "')";
                                                    mysqli_query($linkbd, $sqlr);
                                                }

                                                //concepto contable //********************************************* */
                                                $rest = substr($rowdes['tipoconce'], -2);
                                                $sq = "select fechainicial from conceptoscontables_det where codigo='" . $rowdes['conceptoingreso'] . "' and modulo='" . $rowdes['modulo'] . "' and tipo='$rest' and fechainicial<'$fechaf' and cuenta!='' order by fechainicial asc";
                                                $re = mysqli_query($linkbd, $sq);
                                                while ($ro = mysqli_fetch_assoc($re)) {
                                                    $_POST['fechacausa'] = $ro["fechainicial"];
                                                }
                                                $sqlr = "select * from conceptoscontables_det where codigo='" . $rowdes['conceptoingreso'] . "' and modulo='" . $rowdes['modulo'] . "' and cc='" . $cc . "' and tipo='$rest' and fechainicial='" . $_POST['fechacausa'] . "'";
                                                //echo $sqlr." hoal <br>";
                                                $rst = mysqli_query($linkbd, $sqlr);
                                                $row1 = mysqli_fetch_assoc($rst);
                                                if ($row1['cuenta'] != '' && $valordes > 0) {
                                                    //echo "Hola 5  ";
                                                    $sqlr = "insert into comprobante_det (id_comp,cuenta,tercero,centrocosto,detalle, cheque,valdebito, valcredito, estado, vigencia) values ('6 $ideg','" . $row1['cuenta'] . "','" . $_POST['tercero'] . "' ,'" . $cc . "' , 'Descuento " . $gdndescuentos[$x] . "','',0," . $valordes . ",'1' ,'" . $vigegreso . "')";
                                                    mysqli_query($linkbd, $sqlr);

                                                    //$sqlr="insert into comprobante_det (id_comp,cuenta,tercero,centrocosto,detalle,cheque,valdebito, valcredito,estado,vigencia) values ('6 $ideg','".$_POST['cuentaCredito'][$_POST['dcuentas'][$x].''.$_POST['drecursos'][$x].''.$_POST['productoServicio'][$x].''.$_POST['indicador_producto'][$x]]."','$_POST[tercero]','$_POST[cc]','Descuento ".$gdndescuentos[$x]."','','".$valordes."','0','1', '".$vigegreso."')";
                                                    //mysqli_query($linkbd, $sqlr);
                                                }

                                                //concepto contable //********************************************* */
                                                $rest = "SR";
                                                $sq = "select fechainicial from conceptoscontables_det where codigo='" . $rowdes['conceptosgr'] . "' and modulo='" . $rowdes['modulo'] . "' and tipo='$rest' and fechainicial<'$fechaf' and cuenta!='' order by fechainicial asc";
                                                $re = mysqli_query($linkbd, $sq);
                                                while ($ro = mysqli_fetch_assoc($re)) {
                                                    $_POST['fechacausa'] = $ro["fechainicial"];
                                                }
                                                $sqlr = "select * from conceptoscontables_det where codigo='" . $rowdes['conceptosgr'] . "' and modulo='" . $rowdes['modulo'] . "' and cc='" . $cc . "' and tipo='$rest' and fechainicial='" . $_POST['fechacausa'] . "'";
                                                //echo $sqlr." hoal <br>";
                                                $rst = mysqli_query($linkbd, $sqlr);
                                                $row1 = mysqli_fetch_assoc($rst);
                                                if ($row1['cuenta'] != '' && $valordes > 0) {
                                                    //echo "Hola 5  ";
                                                    $sqlr = "insert into comprobante_det (id_comp,cuenta,tercero,centrocosto,detalle, cheque,valdebito, valcredito, estado, vigencia) values ('6 $ideg','" . $row1['cuenta'] . "','" . $_POST['tercero'] . "' ,'" . $cc . "' , 'Descuento " . $gdndescuentos[$x] . "','',0," . $valordes . ",'1' ,'" . $vigegreso . "')";
                                                    mysqli_query($linkbd, $sqlr);
                                                }



                                                continue;
                                            } else {
                                                $codigoIngreso = $rowdes['conceptoingreso'];
                                                if ($codigoIngreso != "-1") {
                                                    $codigoRetencion = $rowdes['conceptoingreso'];
                                                    $rest = substr($rowdes['tipoconce'], -2);
                                                    $val2 = $rowdes['porcentaje'];
                                                }
                                            }


                                            $sq = "select fechainicial from conceptoscontables_det where codigo='$codigoRetencion' and modulo='" . $rowdes['modulo'] . "' and tipo='$rest' and fechainicial<'$fechaf' and cuenta!='' order by fechainicial asc";
                                            $re = mysqli_query($linkbd, $sq);
                                            while ($ro = mysqli_fetch_assoc($re)) {
                                                $_POST['fechacausa'] = $ro["fechainicial"];
                                            }
                                            $sqlr = "select * from conceptoscontables_det where codigo='$codigoRetencion' and modulo='" . $rowdes['modulo'] . "' and cc='" . $cc . "' and tipo='$rest' and fechainicial='" . $_POST['fechacausa'] . "'";
                                            //echo $sqlr."<br>";
                                            $rst = mysqli_query($linkbd, $sqlr);
                                            $row1 = mysqli_fetch_assoc($rst);
                                            //TERMINA BUSQUEDA CUENTA CONTABLE////////////////////////

                                            if ($row1['cuenta'] != '' && $valordes > 0) {
                                                $sqlr = "insert into comprobante_det (id_comp,cuenta,tercero,centrocosto,detalle, cheque,valdebito, valcredito, estado, vigencia) values ('6 $ideg','" . $row1['cuenta'] . "','" . $_POST['tercero'] . "' ,'" . $cc . "' , 'Descuento " . $gdndescuentos[$x] . "','',0," . $valordes . ",'1' ,'" . $vigegreso . "')";
                                                //	echo $sqlr."<br>";
                                                mysqli_query($linkbd, $sqlr);

                                                //$sqlr="insert into comprobante_det (id_comp,cuenta,tercero,centrocosto,detalle,cheque,valdebito, valcredito,estado,vigencia) values ('6 $ideg','".$_POST['cuentaCredito'][$_POST['dcuentas'][$x].''.$_POST['drecursos'][$x].''.$_POST['productoServicio'][$x].''.$_POST['indicador_producto'][$x]]."','$_POST[tercero]','$rw[cc]','Descuento ".$gdndescuentos[$x]."','','".$valordes."','0','1', '".$vigegreso."')";
                                                //mysqli_query($linkbd, $sqlr);
                                            }

                                            $realizaCausacion = 0;
                                            if ($rowdes['terceros'] != 1 || $rowdes['tipo'] == 'C') {
                                                $realizaCausacion = 1;

                                                if ($rowdes['destino'] != 'M' && $rowdes['tipo'] == 'C') {
                                                    $realizaCausacion = 0;
                                                }
                                            }
                                            if ($rowdes['conceptocausa'] != '-1' && $_POST['medioDePago'] == '1' && $realizaCausacion == 1) {
                                                //concepto contable //********************************************* */
                                                $rest = substr($rowdes['tipoconce'], 0, 2);
                                                $sq = "select fechainicial from conceptoscontables_det where codigo='" . $rowdes['conceptocausa'] . "' and modulo='" . $rowdes['modulo'] . "' and tipo='$rest' and fechainicial<'$fechaf' and cuenta!='' order by fechainicial asc";
                                                $re = mysqli_query($linkbd, $sq);
                                                while ($ro = mysqli_fetch_assoc($re)) {
                                                    $_POST['fechacausa'] = $ro["fechainicial"];
                                                }
                                                $sqlr = "select * from conceptoscontables_det where codigo='" . $rowdes['conceptocausa'] . "' and modulo='" . $rowdes['modulo'] . "' and cc='" . $cc . "' and tipo='$rest' and fechainicial='" . $_POST['fechacausa'] . "'";
                                                //echo $sqlr." hoal <br>";
                                                $rst = mysqli_query($linkbd, $sqlr);
                                                $row1 = mysqli_fetch_assoc($rst);
                                                if ($row1['cuenta'] != '' && $valordes > 0) {
                                                    //echo "Hola 5  ";
                                                    $sqlr = "insert into comprobante_det (id_comp,cuenta,tercero,centrocosto,detalle, cheque,valdebito, valcredito, estado, vigencia) values ('6 $ideg','" . $row1['cuenta'] . "','" . $_POST['tercero'] . "' ,'" . $cc . "' , 'Descuento " . $gdndescuentos[$x] . "',''," . $valordes . ",0,'1' ,'" . $vigegreso . "')";
                                                    mysqli_query($linkbd, $sqlr);
                                                    if ($valorProcentaje <= 100) {
                                                        $sqlr = "insert into comprobante_det (id_comp,cuenta,tercero,centrocosto,detalle, cheque,valdebito, valcredito, estado, vigencia) values ('6 $ideg','" . $row1['cuenta'] . "','" . $_POST['tercero'] . "' ,'" . $cc . "' , 'Descuento " . $gdndescuentos[$x] . "','',0," . $valordes . ",'1' ,'" . $vigegreso . "')";
                                                        mysqli_query($linkbd, $sqlr);
                                                    }
                                                }
                                            }

                                        }
                                        //echo "$valopb $valordes <br>";
                                    }
                                }
                            }

                            $sqlrRetencion = "SELECT TB1.* FROM tesoordenpago_retenciones AS TB1, tesoretenciones AS TB2 WHERE TB1.id_orden='" . $_POST['orden'] . "' AND TB1.id_retencion = TB2.id AND TB2.terceros = ''";
                            $resRetencion = mysqli_query($linkbd, $sqlrRetencion);
                            while ($rowRetencion = mysqli_fetch_row($resRetencion)) {
                                $sqlr = "SELECT * FROM tesoretenciones_det,tesoretenciones_det_presu WHERE tesoretenciones_det.id=tesoretenciones_det_presu.id_retencion AND tesoretenciones_det.codigo='" . $rowRetencion[0] . "'";
                                //echo $sqlr."<br>";
                                $resdes = mysqli_query($linkbd, $sqlr);
                                $valordescuento = 0;
                                while ($rowdes = mysqli_fetch_assoc($resdes)) {
                                    if ($_POST['iva'] > 0 && $rowdes['tercero'] == 1) {
                                        $valordescuento = round($rowRetencion[3] * ($rowdes['porcentaje'] / 100), 0);
                                    } else {
                                        $valordescuento = round(($rowRetencion[3]) * ($rowdes['porcentaje'] / 100), 0);
                                    }
                                    if ($rowdes['cuentapres'] != "") {
                                        $sqlrFuente = "SELECT fuente FROM ccpetinicialing WHERE cuenta = '" . $rowdes['cuentapres'] . "' ORDER BY vigencia DESC LIMIT 1";
                                        $respFuente = mysqli_query($linkbd, $sqlrFuente);
                                        $rowFuente = mysqli_fetch_row($respFuente);
                                        $sql = "insert into pptoretencionpago(cuenta, idrecibo, valor, vigencia, tipo, fuente, seccion_presupuestal, medio_pago, vigencia_gasto) values ('" . $rowdes['cuentapres'] . "', '" . $_POST['egreso'] . "', $valordescuento,'" . $_POST['vigenciaop'] . "', 'egreso', '" . $rowFuente[0] . "', '16', 'CSF', '1')";
                                        mysqli_query($linkbd, $sql);
                                    }
                                }
                            }

                            $cantCuentas = count($_POST['dcuentas']);
                            $cuentaResta = 0;
                            $vigusu = $_POST['vigencia'];
                            for ($x = 0; $x < $cantCuentas; $x++) {
                                $sec_presu = intval($_POST['dseccion_presupuestal'][$x]);
                                $sqlrCc = "SELECT id_cc FROM centrocostos_seccionpresupuestal WHERE (id_sp = $sec_presu OR id_sp = '" . $_POST['dseccion_presupuestal'][$x] . "')";
                                $resCc = mysqli_query($linkbd, $sqlrCc);
                                $rowCc = mysqli_fetch_row($resCc);
                                $cc = $rowCc[0];

                                $valneto = $_POST['dvalores'][$x];
                                if ($opcion == "1") {
                                    $valneto -= generaRetenciones($_POST['orden'], $_POST['dvalores'][$x]);
                                }
                                if ($valneto > 0) {
                                    $sqlr = "insert into comprobante_det (id_comp,cuenta,tercero,centrocosto,detalle,cheque,valdebito, valcredito, estado, vigencia) values ('6 $ideg','" . $_POST['cuentaDebito'][$x] . "','$_POST[tercero]','$cc','Pago " . $_POST['dncuentas'][$x] . "','','" . $valneto . "',0,'1' ,'$vigegreso')";
                                    mysqli_query($linkbd, $sqlr);
                                    $valorades += $valneto;
                                }


                                //$totdes=$valorades-$valopb;
                                $totdes = $valneto - (round(($valopb * $valneto / $_POST['valororden']), 0));

                                if ($_POST['codingreso'] != '') {
                                    if ($_POST['entidadAdministradora'] == 'S') {
                                        //$sqlri="Select * from tesoingresos_det where codigo='".$_POST[codingreso]."' and vigencia='$vigegreso'";
                                        $sqlri = "SELECT * FROM tesomediodepago WHERE estado='S' AND id = '$_POST[codingreso]'";
                                        $resi = mysqli_query($linkbd, $sqlri);
                                        $rowi = mysqli_fetch_row($resi);
                                        if ($totdes > 0) {
                                            $sqlr = "insert into comprobante_det (id_comp,cuenta,tercero,centrocosto,detalle,cheque, valdebito,valcredito,estado,vigencia) values ('6 $ideg','" . $rowi[2] . "','" . $_POST['tercero'] . "' ,'$cc' , 'Pago Banco " . $_POST['dncuentas'][$x] . "','$_POST[cheque]  $_POST[ntransfe]',0," . $totdes . ",'1' ,'" . $_POST['vigenciaop'] . "')";
                                            //echo "5 $sqlr </br>";
                                            mysqli_query($linkbd, $sqlr);
                                        }

                                    } else {
                                        $sqlri = "Select * from tesoingresos_det where codigo='" . $_POST['codingreso'] . "' and vigencia='$vigegreso'";
                                        $resi = mysqli_query($linkbd, $sqlri);
                                        while ($rowi = mysqli_fetch_row($resi)) {
                                            //**** busqueda concepto contable*****
                                            $sq = "select fechainicial from conceptoscontables_det where codigo='$rowi[2]' and modulo='4' and tipo='C' and fechainicial<'$fechaf' and cuenta!='' order by fechainicial asc";
                                            $re = mysqli_query($linkbd, $sq);
                                            while ($ro = mysqli_fetch_assoc($re)) {
                                                $_POST['fechacausa'] = $ro["fechainicial"];
                                            }
                                            $sqlrc = "Select * from conceptoscontables_det where estado='S' and modulo='4' AND codigo='$rowi[2]' and tipo='C' and fechainicial='" . $_POST['fechacausa'] . "'";
                                            $resc = mysqli_query($linkbd, $sqlrc);
                                            while ($rowc = mysqli_fetch_row($resc)) {
                                                if ($cc == $rowc[5]) {
                                                    if ($rowc[3] == 'N') {
                                                        if ($rowc[6] == 'S') {
                                                            if ($totdes > 0) {
                                                                $sqlr = "insert into comprobante_det (id_comp,cuenta,tercero,centrocosto,detalle,cheque, valdebito,valcredito,estado,vigencia) values ('6 $ideg','" . $rowc[4] . "','" . $_POST['tercero'] . "' ,'$cc' , 'Pago Banco " . $_POST['dncuentas'][$x] . "','$_POST[cheque]  $_POST[ntransfe]',0," . $totdes . ",'1' ,'" . $_POST['vigenciaop'] . "')";
                                                                //echo "5 $sqlr </br>";
                                                                mysqli_query($linkbd, $sqlr);
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }

                                } else {
                                    if ($totdes > 0) {
                                        $sqlr = "insert into comprobante_det (id_comp,cuenta,tercero,centrocosto,detalle,cheque, valdebito,valcredito,estado,vigencia) values ('6 $ideg','" . $_POST['banco'] . "','" . $_POST['tercero'] . "' ,'$cc' , 'Pago Banco " . $_POST['dncuentas'][$x] . "','$_POST[cheque]  $_POST[ntransfe]',0," . $totdes . ",'1' ,'" . $_POST['vigenciaop'] . "')";
                                        //echo "5 $sqlr </br>";
                                        mysqli_query($linkbd, $sqlr);
                                    }
                                }
                            }

                            echo "<script>despliegamodalm('visible','1','Se ha almacenado el Egreso con Exito');</script>";
                        }
                    } else {
                        echo "<script>despliegamodalm('visible','2','No se puede almacenar, ya existe un egreso para esta orden');</script>";
                    }

                }
                if ($_POST['tipomovimiento'] == '401') {

                    $sqlrCab = "INSERT INTO comprobante_cab(numerotipo, tipo_comp, fecha, concepto, total_debito, total_credito, diferencia, estado) VALUES('$_POST[egreso]', '2006', '$fechaf', '$_POST[descripcion]', '$_POST[valoregreso]', '$_POST[valoregreso]', 0, 1)";
                    mysqli_query($linkbd, $sqlrCab);

                    cargartrazas('conta', 'comprobante_cab', 'insert', $_POST['egreso'], '2006', $_POST['egreso'], '', $_POST['descripcion'], $sqlrCab);

                    $sqlrDetSel = "SELECT * FROM comprobante_det WHERE numerotipo='$_POST[egreso]' AND tipo_comp='6'";
                    $resDetSel = mysqli_query($linkbd, $sqlrDetSel);

                    cargartrazas('conta', 'comprobante_det', 'select', $_POST['egreso'], '6', $_POST['egreso'], '', '', $sqlrDetSel);

                    while ($rowDetSel = mysqli_fetch_row($resDetSel)) {

                        $debito = 0;
                        $credito = 0;

                        if ($rowDetSel[7] > 0) {
                            $debito = 0;
                            $credito = $rowDetSel[7];
                        } else {
                            $debito = $rowDetSel[8];
                            $credito = 0;
                        }

                        $id_comp = "2006 " . $rowDetSel[12];

                        $sqlrDet = "INSERT INTO comprobante_det(id_comp,cuenta,tercero,centrocosto,detalle,cheque, valdebito,valcredito,estado,vigencia,tipo_comp,numerotipo) VALUES('$id_comp', '$rowDetSel[2]', '$rowDetSel[3]', '$rowDetSel[4]', '$rowDetSel[5]', '$rowDetSel[6]', $debito, $credito, '$rowDetSel[9]', '$rowDetSel[10]', '2006', '$rowDetSel[12]')";

                        mysqli_query($linkbd, $sqlrDet);

                        cargartrazas('conta', 'comprobante_det', 'insert', "$id_comp", '2006', $rowDetSel[12], '', '', $sqlrDet);

                    }

                    $sqlr = "update tesoegresos set estado='R' where id_egreso = '$_POST[egreso]' and tipo_mov='201' ";
                    mysqli_query($linkbd, $sqlr);

                    cargartrazas('teso', 'tesoegresos', 'update', "$_POST[egreso]", '', $_POST['egreso'], '', '', $sqlr);


                    $sqlr = "insert into tesoegresos(id_egreso,id_orden,fecha,vigencia,concepto,user,tipo_mov, estado) values ('$_POST[egreso]','$_POST[orden]','$fechaf','$_POST[vigencia]','$_POST[descripcion]','" . $_SESSION['nickusu'] . "','401', 'R')";
                    mysqli_query($linkbd, $sqlr);

                    cargartrazas('teso', 'tesoegresos', 'insert', "$_POST[egreso]", '', $_POST['egreso'], '', $_POST['descripcion'], $sqlr);


                    $sqlr = "select * from tesoegresos where id_egreso='$_POST[egreso]' and vigencia='$vigegreso'";
                    $resp = mysqli_query($linkbd, $sqlr);
                    $row = mysqli_fetch_row($resp);
                    $op = $row[2];
                    $vpa = $row[7];

                    //********RETENCIONES
                    $sqlr = "delete from pptoretencionpago where idrecibo=" . $_POST['egreso'] . " ";
                    mysqli_query($linkbd, $sqlr);

                    cargartrazas('ppto', 'pptoretencionpago', 'delete', "$_POST[egreso]", '', $_POST['egreso'], '', '', $sqlr);

                    $sqlr = "update tesoordenpago  set estado='S' where id_orden=$op and tipo_mov='201' ";
                    mysqli_query($linkbd, $sqlr);

                    cargartrazas('teso', 'tesoordenpago', 'update', "$op", '', $op, '', '', $sqlr);


                    echo "<table class='inicio'><tr><td class='saludo1'><center>Se ha reversado el Egreso con Exito <img src='imagenes\confirm.png'></center></td></tr></table><script>funcionmensaje();</script>";
                }

            } else {
                echo "<script>despliegamodalm('visible','2',' No Tiene los Permisos para Modificar este Documento');</script>";
            }
            //****fin if bloqueo
        }//************ FIN DE IF  	************
        //echo "<br>c: $gddesvalores[0]";
        ?>
        <div id="bgventanamodal2">
            <div id="ventanamodal2">
                <IFRAME src="" name="buster" marginWidth=0 marginHeight=0 frameBorder=0 id="ventana2" frameSpacing=0
                    style="left:500px; width:900px; height:500px; top:200;">
                </IFRAME>
            </div>
        </div>
    </form>
</body>

</html>
