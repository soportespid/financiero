<?php
	header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=utf8");
	require 'comun.inc';
	require 'funciones.inc';
	session_start();
	$linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");
	cargarcodigopag($_GET['codpag'],$_SESSION["nivel"]);
	date_default_timezone_set("America/Bogota");
?>
<!DOCTYPE >
<html lang=es>
	<head>
    <meta charset="utf-8"/>
		<meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1"/>
		<meta name="viewport" content="user-scalable=no">
		<title>:: IDEAL 10 - Servicios públicos</title>
		<link href="favicon.ico" rel="shortcut icon"/>
		<link href="css/css2z.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
		<link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
		<link href="css/css4.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
        <link href="css/tabs.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css"/>
        <link rel="stylesheet" href="sweetalert2/dist/sweetalert2.min.css">
        <script type="text/javascript" src="css/programas.js"></script>
        <script src="sweetalert2/dist/sweetalert2.min.js"></script>
		<script src="vue/vue.min.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script src="css/programas.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script src="css/calendario.js?<?php echo date('d_m_Y_h_i_s');?>"></script>

        <style>
            .checkbox-wrapper-31:hover .check {
                stroke-dashoffset: 0;
            }
        
            .checkbox-wrapper-31 {
                position: relative;
                display: inline-block;
                width: 40px;
                height: 40px;
            }
            .checkbox-wrapper-31 .background {
                fill: #ccc;
                transition: ease all 0.6s;
                -webkit-transition: ease all 0.6s;
            }
            .checkbox-wrapper-31 .stroke {
                fill: none;
                stroke: #fff;
                stroke-miterlimit: 10;
                stroke-width: 2px;
                stroke-dashoffset: 100;
                stroke-dasharray: 100;
                transition: ease all 0.6s;
                -webkit-transition: ease all 0.6s;
            }
            .checkbox-wrapper-31 .check {
                fill: none;
                stroke: #fff;
                stroke-linecap: round;
                stroke-linejoin: round;
                stroke-width: 2px;
                stroke-dashoffset: 22;
                stroke-dasharray: 22;
                transition: ease all 0.6s;
                -webkit-transition: ease all 0.6s;
            }
            .checkbox-wrapper-31 input[type=checkbox] {
                position: absolute;
                width: 100%;
                height: 100%;
                left: 0;
                top: 0;
                margin: 0;
                opacity: 0;
                -appearance: none;
            }
            .checkbox-wrapper-31 input[type=checkbox]:hover {
                cursor: pointer;
            }
            .checkbox-wrapper-31 input[type=checkbox]:checked + svg .background {
                fill: #6cbe45;
            }
            .checkbox-wrapper-31 input[type=checkbox]:checked + svg .stroke {
                stroke-dashoffset: 0;
            }
            .checkbox-wrapper-31 input[type=checkbox]:checked + svg .check {
                stroke-dashoffset: 0;
            }
        </style>
	</head>
	<body>
		<header>
			<table>
				<tr><script>barra_imagenes("serv");</script><?php cuadro_titulos();?></tr>
			</table>
		</header>

		<form name="form2" method="post" action="">
			<section id="myapp" v-cloak >
                <div id="cargando" v-if="isLoading" class="loading" style="z-index: 10000;">
                    <span>Cargando...</span>
                </div>
				<nav>
					<table>
						<tr><?php menu_desplegable("serv");?></tr>
						<tr>
							<td colspan="3" class="cinta">
								<img src="imagenes/add.png" v-on:click="location.href='serv-recaudoFactura'" class="mgbt" title="Nuevo">
								<img src="imagenes/guardad.png"   class="mgbt">
								<img src="imagenes/busca.png" v-on:click="location.href='serv-buscarRecaudoFactura'" class="mgbt" title="Buscar">
								<img src="imagenes/nv.png" onClick="mypop=window.open('serv-principal','',''); mypop.focus();" class="mgbt" title="Nueva Ventana">
                                <a @click="printPDF" class="mgbt"><img src="imagenes/print.png" title="Imprimir" /></a>
                                <a @click="printExcel" class="mgbt"><img src="imagenes/excel.png" title="Excel"></a>
							</td>
						</tr>
					</table>
				</nav>

				<article>
                    <div>
                        
                        <table class="inicio">
                            <tr>
                                <td class="titulos" colspan="11">.: Detalle de recaudo</td>
                            </tr>
                            <tr>
                                <td class="textonew01" >.: Fecha inicial:</td>
                                <td >
                                    <input type="text" name="fechaInicial"  onKeyUp="return tabular(event,this)" id="fechaInicial" title="DD/MM/YYYY" onDblClick="displayCalendarFor('fechaInicial');" class="colordobleclik" autocomplete="off" onChange=""  readonly>
                                </td>

                                <td class="textonew01" >.: Fecha final:</td>
                                <td >
                                    <input type="text" name="fechaFinal"  onKeyUp="return tabular(event,this)" id="fechaFinal" title="DD/MM/YYYY" onDblClick="displayCalendarFor('fechaFinal');" class="colordobleclik" autocomplete="off" onChange=""  readonly>
                                </td>

                                <td class="textonew01" >.: Consecutivo recaudo:</td>
                                <td >
                                    <input type="text" v-model="intConsecutivo" @keyup.enter="search()" style="width:100%;">
                                </td>
                                <td class="textonew01" >.: Código de usuario:</td>
                                <td >
                                    <input type="text" v-model="intCodigoUsuario" @keyup.enter="search()" style="width:100%;">
                                </td>

                                <td class="textonew01" >.: Número de factura:</td>
                                <td >
                                    <input type="text" v-model="intNumeroFactura" @keyup.enter="search()" style="width:100%;">
                                </td>

                                <td>
                                    <input type="button" value="Buscar" @click="search()" />
                                </td>
                            <tr>
                        </table>

                        <div class='subpantalla' style='height:50vh; width:100%; margin-top:0px;  overflow-x:hidden'>
                            <table class='inicio' align='center'>
                                <tbody>
                                    <tr>
                                        <td colspan='12' class='titulos'>.: Resultados Busqueda:</td>
                                    </tr>
                                    <tr>
                                        <td colspan='12'>Total: {{ intResults }}</td>
                                    </tr>
                                </tbody>
                            </table>
                            <table class='inicio'>        
                                <thead>
                                    <tr>
                                        <th class="titulosnew00" style="width: 10%;">Consecutivo de recaudo</th>
                                        <th class="titulosnew00" style="width: 10%;">Código de usuario</th>
                                        <th class="titulosnew00">Nombre</th>
                                        <th class="titulosnew00" style="width: 10%;">Número de factura</th>  
                                        <th class="titulosnew00" style="width: 10%;">Fecha de Recaudo</th>      
                                        <th class="titulosnew00" style="width: 10%;">Descuento</th>    
                                        <th class="titulosnew00" style="width: 10%;">Medio de pago</th>    
                                        <th class="titulosnew00" style="width: 10%;">Valor pago</th>    
                                        <th class="titulosnew00" style="width: 10%;">Realiza</th>   
                                        <th class="titulosnew00" style="width: 5%;">Estado</th>   
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr v-if="arrData == ''">
                                        <td colspan="9">
                                            <div style="text-align: center; color:turquoise; font-size:large" class="h4 text-primary text-center">
                                                Utilice los filtros para buscar información.
                                            </div>
                                        </td>
                                    </tr>
                                    
                                    <tr style="height:50px;" v-for="(data,index) in arrData" @dblclick="view(data.id)" v-bind:class="index % 2 ? 'saludo1a' : 'saludo2'">
                                        
                                        <td style="text-align:center">{{data.consecutivo}}</td>
                                        <td style="text-align:center">{{data.cod_usuario}}</td>
                                        <td style="text-transform:uppercase">{{data.nombre}}</td>
                                        <td style="text-align:center">{{data.numero_factura}}</td>
                                        <td style="text-align:center">{{data.fecha_recaudo}}</td>
                                        <td style="text-align:center">{{data.descuento}}%</td>
                                        <td style="text-align:center">{{data.medio_pago}}</td>
                                        <td style="text-align:right">{{formatNumero(data.valor_pago)}}</td>
                                        <td style="text-align:center; text-transform:uppercase">{{data.realiza}}</td>
                                        <td style="text-align:center;">
                                            <span v-if="data.estado == 'ACTIVO'" style="padding:4px; border-radius:5px; background:#008000;color:#fff;font-weight:bold">{{data.estado}}</span>
                                            <span v-else style="padding:4px; border-radius:5px; background:#ff0000;color:#fff;font-weight:bold">{{data.estado}}</span>
                                        </td>
                                    </tr>
                                </tbody>
                                
                            </table>
                        </div>
                        <div v-if="arrData !=''" class="inicio">
                            <p style="text-align:center">Página {{ intPage }} de {{ intTotalPages }}</p>
                            <ul style="list-style:none; padding:0;display:flex;justify-content: center;align-items:center">
                                <li v-show="intPage > 1" @click="search(intPage = 1)" style="cursor:pointer;padding:4px 10px;background:#fff; border:1px solid #39c"><< </li>
                                <li v-show="intPage > 1" @click="search(--intPage)" style="cursor:pointer;padding:4px 10px;background:#fff; border:1px solid #39c" ><</li>
                                <li v-show="intPage < intTotalPages" @click="search(++intPage)" style="cursor:pointer;padding:4px 10px;background:#fff; border:1px solid #39c" >></li>
                                <li v-show="intPage < intTotalPages" @click="search(intPage = intTotalPages)" style="cursor:pointer;padding:4px 10px;background:#fff; border:1px solid #39c">>></li>
                            </ul>
                        </div>
                    </div>
				</article>
			</section>
		</form>

		<script src="node_modules/read-excel-file/bundle/read-excel-file.min.js"></script>
		<script src="node_modules/xlsx/dist/xlsx.full.min.js"></script>
        <script src="Librerias/vue/vue.min.js"></script>
		<script src="Librerias/vue/axios.min.js"></script>
		<script type="module" src="servicios_publicos/recaudoFactura/buscar/serv-buscarRecaudoFactura.js?<?= date('d_m_Y_h_i_s');?>"></script>
        
	</body>
</html>