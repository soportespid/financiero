<?php

    header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=utf8");
	require 'comun.inc';
	require 'funciones.inc';
	session_start();
    if(empty($_SESSION)){
        header("location: index.php");
    }
	$linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");
	cargarcodigopag($_GET['codpag'],$_SESSION["nivel"]);
	date_default_timezone_set("America/Bogota");
?>
<!DOCTYPE >
<html lang=es>
	<head>
    <meta charset="utf-8"/>
		<meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1"/>
		<meta name="viewport" content="user-scalable=no">
		<title>:: IDEAL 10 - Tesoreria</title>
		<link href="favicon.ico" rel="shortcut icon"/>
		<link href="css/css2z.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
		<link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
		<link href="css/css4.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
        <link href="css/tabs.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css"/>
        <link href="css/style.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css"/>
        <link rel="stylesheet" href="sweetalert2/dist/sweetalert2.min.css">
        <script type="text/javascript" src="css/programas.js"></script>
        <script src="sweetalert2/dist/sweetalert2.min.js"></script>
		<script src="vue/vue.min.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script src="css/programas.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script src="css/calendario.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
	</head>
	<body>
		<header>
			<table>
				<tr><script>barra_imagenes("teso");</script><?php cuadro_titulos();?></tr>
			</table>
		</header>

        <section id="myapp" v-cloak>
            <input type="hidden" value = "1" ref="pageType">
            <div class="loading-container" v-show="isLoading" >
                <p class="text-loading"data-loading="IDEAL 10">IDEAL 10</p>
            </div>
            <nav>
                <table>
                    <tr><?php menu_desplegable("teso");?></tr>
                </table>
                <div class="bg-white group-btn p-1" id="newNavStyle">
                    <button type="button" class="btn btn-white btn-primary-hover d-flex justify-between align-items-center" @click="window.location.reload()">
                        <span>Nuevo</span>
                        <svg viewBox="0 -960 960 960" ><path d="M440-440H200v-80h240v-240h80v240h240v80H520v240h-80v-240Z"/></svg>
                    </button>
                    <button type="button" class="btn btn-white btn-primary-hover d-flex justify-between align-items-center" @click="save()">
                        <span>Guardar</span>
                        <svg viewBox="0 -960 960 960" ><path d="M840-680v480q0 33-23.5 56.5T760-120H200q-33 0-56.5-23.5T120-200v-560q0-33 23.5-56.5T200-840h480l160 160Zm-80 34L646-760H200v560h560v-446ZM480-240q50 0 85-35t35-85q0-50-35-85t-85-35q-50 0-85 35t-35 85q0 50 35 85t85 35ZM240-560h360v-160H240v160Zm-40-86v446-560 114Z"/></svg>
                    </button>
                    <button type="button" class="btn btn-white btn-primary-hover d-flex justify-between align-items-center" @click="location.href='teso-buscaLegalizacionComision'">
                        <span>Buscar</span>
                        <svg class="fill-black group-hover:fill-white w-5 h-5" viewBox="0 -960 960 960" ><path d="M784-120 532-372q-30 24-69 38t-83 14q-109 0-184.5-75.5T120-580q0-109 75.5-184.5T380-840q109 0 184.5 75.5T640-580q0 44-14 83t-38 69l252 252-56 56ZM380-400q75 0 127.5-52.5T560-580q0-75-52.5-127.5T380-760q-75 0-127.5 52.5T200-580q0 75 52.5 127.5T380-400Z"/></svg>
                    </button>
                    <button type="button" class="btn btn-white btn-primary-hover d-flex justify-between align-items-center" @click="mypop=window.open('teso-principal.php','',''); mypop.focus();">
                        <span>Nueva ventana</span>
                        <svg class="fill-black group-hover:fill-white w-5 h-5" viewBox="0 -960 960 960" ><path d="M320-120v-80h80v-80H160q-33 0-56.5-23.5T80-360v-400q0-33 23.5-56.5T160-840h640q33 0 56.5 23.5T880-760v400q0 33-23.5 56.5T800-280H560v80h80v80H320ZM160-360h640v-400H160v400Zm0 0v-400 400Z"/></svg>
                    </button>
                </div>
            </nav>
            <article>
                <div class="bg-white">
                    <div>
                        <h2 class="titulos m-0">Crear legalización de comisión</h2>
                        
                        <div class="d-flex">
                            <div class="form-control w-25">
                                <label class="form-label m-0" for="">Fecha<span class="text-danger fw-bolder">*</span></label>
                                <input type="date" class="text-center" v-model="fecha">
                            </div>

                            <div class="form-control w-15">
                                <label class="form-label m-0" for="">Obligación<span class="text-danger fw-bolder">*</span></label>
                                <input type="text" class="text-center colordobleclik" v-model="codObligacion" @dblclick="modalObligacion=true;" @change="inputFilter('obligacion')"> 
                            </div>

                            <div class="form-control">
                                <label class="form-label m-0" for="">Objeto obligación</label>
                                <input type="text" v-model="objetoObligacion" readonly> 
                            </div>

                            <div class="form-control w-25">
                                <label class="form-label m-0" for="">Valor obligación</label>
                                <input type="text" class="text-right" v-model="viewFormatNumber(valorObligacion)" readonly> 
                            </div>
                        </div>

                        <div class="d-flex">
                            <div class="form-control">
                                <label class="form-label m-0" for="">Beneficiario</label>
                                <input type="text" v-model="beneficiario" readonly> 
                            </div>
                        </div>

                        <div class="d-flex">
                            <div class="form-control">
                                <label class="form-label m-0" for="">Detalle<span class="text-danger fw-bolder">*</span></label>
                                <input type="text" v-model="txtDetalle"> 
                            </div>
                        </div>

                        <h2 class="titulos m-0">Desagregación</h2>

                        <div class="d-flex">
                            <div class="form-control w-50">
                                <label class="form-label m-0" for="">Tercero<span class="text-danger fw-bolder">*</span></label>
                                <input type="text" class="text-center colordobleclik" v-model="txtDocTercero" @dblclick="modalTercero=true;" @change="inputFilter('tercero')"> 
                            </div>

                            <div class="form-control justify-between">
                                <label class="form-label m-0" for=""></label>
                                <input type="text" v-model="txtNameTercero" readonly> 
                            </div>

                            <div class="form-control w-25">
                                <label class="form-label m-0" for="">Concepto<span class="text-danger fw-bolder">*</span></label>
                                <input type="text" v-model="txtCodConcepto" class="text-center colordobleclik" @dblclick="modalConcepto=true;" @change="inputFilter('concepto')"> 
                            </div>

                            <div class="form-control justify-between">
                                <label class="form-label m-0" for=""></label>
                                <input type="text" v-model="txtNameConcepto" readonly> 
                            </div>

                            <div class="form-control w-50">
                                <label class="form-label m-0" for="">Cuenta contable<span class="text-danger fw-bolder">*</span></label>
                                <input type="text" v-model="txtCuentaContable" readonly> 
                            </div>

                            <div class="form-control w-50">
                                <label class="form-label m-0" for="">Valor<span class="text-danger fw-bolder">*</span></label>
                                <div class="form-number number-primary">
                                    <span></span>
                                    <input type="text" class="text-right" v-model="txtValorFormat" @input="formatInputNumber('txtValorFormat', 'txtValor')"> 
                                </div>
                            </div>

                            <div class="form-control justify-between w-15">
                                <label class="form-label m-0" for=""></label>
                                <button type="button" class="btn btn-primary" @click="add()">Agregar</button>
                            </div>
                        </div>

                        <div class="table-responsive" style="height: 30%;">
                            <table class="table table-hover fw-normal">
                                <thead>
                                    <tr class="text-center">
                                        <th>Tercero</th>
                                        <th>Concepto</th>
                                        <th>Cuenta contable</th>
                                        <th>Valor</th>
                                        <th>Eliminar</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr tr v-for="(det,index) in arrDesagregacion" :key="index">
                                        <td>{{ det.documento }} - {{ det.nombre }}</td>
                                        <td>{{ det.codigo }} - {{ det.concepto }}</td>
                                        <td class="text-center">{{ det.cuenta }}</td>
                                        <td class="text-right">{{ viewFormatNumber(det.valor) }}</td>
                                        <td class="text-center">
                                            <button type="button" class="btn btn-danger btn-sm m-0" @click="delItem(index)">Eliminar</button>
                                        </td>
                                    </tr>
                                </tbody>
                                <tfoot>
                                    <tr class="fw-bold">
                                        <td class="text-right" colspan="3">Total:</td>
                                        <td class="text-right">{{ viewFormatNumber(totalDesagrergacion) }}</td>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>   
                    
                    <!-- modales -->
                    <div v-show="modalObligacion" class="modal">
                        <div class="modal-dialog modal-lg">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title">Obligaciones</h5>
                                    <button type="button" @click="modalObligacion=false;" class="btn btn-close"><div></div><div></div></button>
                                </div>
                                <div class="modal-body">
                                    <div class="d-flex flex-column">
                                        <div class="form-control m-0 mb-3">
                                            <input type="search" v-model="txtSearchObligacion" @keyup="modalFilter('obligacion')" placeholder="Busca código, objeto o beneficiario">
                                        </div>
                                    </div>
                                    <div class="overflow-auto max-vh-50 overflow-x-hidden" >
                                        <table class="table table-hover fw-normal">
                                            <thead>
                                                <tr class="text-center">
                                                    <th>Consecutivo</th>
                                                    <th>Objeto</th>
                                                    <th>Beneficiario</th>
                                                    <th>Valor</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr v-for="(obligacion,index) in arrObligaciones" :key="index" @click="selectItem('obligacion', obligacion)">
                                                    <td class="text-center">{{ obligacion.id_orden }}</td>
                                                    <td>{{ obligacion.conceptorden }}</td>
                                                    <td>{{ obligacion.tercero }} - {{ obligacion.nombre }}</td>
                                                    <td class="text-right">{{ viewFormatNumber(obligacion.valor) }}</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div v-show="modalTercero" class="modal">
                        <div class="modal-dialog modal-lg">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title">Terceros</h5>
                                    <button type="button" @click="modalTercero=false;" class="btn btn-close"><div></div><div></div></button>
                                </div>
                                <div class="modal-body">
                                    <div class="d-flex flex-column">
                                        <div class="form-control m-0 mb-3">
                                            <input type="search" v-model="txtSearchTercero" @keyup="modalFilter('tercero')" placeholder="Busca documento o nombre">
                                        </div>
                                    </div>
                                    <div class="overflow-auto max-vh-50 overflow-x-hidden" >
                                        <table class="table table-hover fw-normal">
                                            <thead>
                                                <tr class="text-center">
                                                    <th>Item</th>
                                                    <th>Documento</th>
                                                    <th>Nombre</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr v-show="arrTerceros.length == 0">
                                                    <td colspan="3">
                                                        <div style="text-align: center; font-size:large" class="h4 text-center">
                                                            Utilice los filtros para buscar información.
                                                        </div>
                                                    </td>
                                                </tr>

                                                <tr v-for="(tercero,index) in arrTerceros" :key="index" @click="selectItem('tercero', tercero)">
                                                    <td class="text-center w-15">{{ index+1 }}</td>
                                                    <td class="text-center">{{ tercero.cedulanit }}</td>
                                                    <td>{{ tercero.nombre }}</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div v-show="modalConcepto" class="modal">
                        <div class="modal-dialog modal-sm">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title">Conceptos</h5>
                                    <button type="button" @click="modalConcepto=false;" class="btn btn-close"><div></div><div></div></button>
                                </div>
                                <div class="modal-body">
                                    <div class="d-flex flex-column">
                                        <div class="form-control m-0 mb-3">
                                            <input type="search" v-model="txtSearchConcepto" @keyup="modalFilter('concepto')" placeholder="Busca código o nombre">
                                        </div>
                                    </div>
                                    <div class="overflow-auto max-vh-50 overflow-x-hidden" >
                                        <table class="table table-hover fw-normal">
                                            <thead>
                                                <tr class="text-center">
                                                    <th>Codigo</th>
                                                    <th>Nombre</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr v-for="(concepto,index) in arrConceptos" :key="index" @click="selectItem('concepto', concepto)">
                                                    <td class="text-center w-25">{{ concepto.codigo }}</td>
                                                    <td>{{ concepto.nombre }}</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </article>
        </section>

        <script src="Librerias/vue/vue.min.js"></script>
		<script src="Librerias/vue/axios.min.js"></script>
		<script type="module" src="tesoreria/legalizacion_comision/js/legalizacion_comision_functions.js?<?= date('d_m_Y_h_i_s');?>"></script>

	</body>
</html>
