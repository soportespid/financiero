<?php
	header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=utf8");

	require "comun.inc";
	require "funciones.inc";
	require "conversor.php";
	require "validaciones.inc";

	$linkbd_v7 = conectar_v7();
	$linkbd_v7 -> set_charset("utf8");

	session_start();
    date_default_timezone_set("America/Bogota");
?>
<!DOCTYPE >
<html lang="es">
	<head>
		<meta charset="utf-8"/>
		<meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1"/>
		<meta name="viewport" content="user-scalable=no">
		<title>:: IDEAL 10 - Tesorer&iacute;a</title>
		<link href="favicon.ico" rel="shortcut icon"/>

		<script>
			//************* ver reporte ************
			//***************************************
			function verep(idfac)
			{
				document.form1.oculto.value=idfac;
				document.form1.submit();
			}

			//************* genera reporte ************
			//***************************************
			function genrep(idfac)
			{
				document.form2.oculto.value=idfac;
				document.form2.submit();
			}

			function buscacta(e)
			{
				if (document.form2.cuenta.value!="")
				{
					document.form2.bc.value='1';
					document.form2.submit();
				}
			}

			function validar()
			{
				document.form2.submit();
			}
			function validarBanco()
			{
				document.form2.oculto.value = '';
				document.form2.submit();
				
			}

			function buscater(e)
			{
				if (document.form2.tercero.value!="")
				{
					document.form2.bt.value='1';
					document.form2.submit();
				}
			}

			function agregardetalle()
			{
				if(document.form2.numero.value!="" &&  document.form2.valor.value>0 &&  document.form2.banco.value!=""  )
				{ 
					document.form2.agregadet.value=1;
					//document.form2.chacuerdo.value=2;
					document.form2.submit();
				}
				else 
				{
					alert("Falta informacion para poder Agregar");
				}
			}

			function eliminar(variable)
			{
				if (confirm("Esta Seguro de Eliminar"))
				{
					document.form2.elimina.value=variable;
					//eli=document.getElementById(elimina);
					vvend=document.getElementById('elimina');
					//eli.value=elimina;
					vvend.value=variable;
					document.form2.submit();
				}
			}

			//************* genera reporte ************
			//***************************************
			function guardar()
			{

				if (document.form2.fecha.value!='' && ((document.form2.modorec.value=='banco' && document.form2.banco.value!='') || (document.form2.modorec.value=='caja' && document.form2.cuentacaja.value!='') || document.form2.modorec.value=='ssf') )
				{
					if (confirm("Esta Seguro de Guardar"))
					{
						document.form2.oculto.value=2;
						document.form2.submit();
					}
				}
				else
				{
					alert('Faltan datos para completar el registro');
					document.form2.fecha.focus();
					document.form2.fecha.select();
				}
			}

			function pdf()
			{
				document.form2.action="teso-pdfsinrecajasp.php";
				document.form2.target="_BLANK";
				document.form2.submit(); 
				document.form2.action="";
				document.form2.target="";
			}

			function despliegamodal2(_valor,_num)
			{
				document.getElementById("bgventanamodal2").style.visibility=_valor;
				if(_valor=="hidden"){document.getElementById('ventana2').src="";}
				else 
				{
					switch(_num)
					{
						case '1':	document.getElementById('ventana2').src="cuentasbancarias-ventana02.php?tipoc=D&obj01=banco&obj02=nbanco&obj03=&obj04=cb&obj05=ter";break;
						case '2':	document.getElementById('ventana2').src="ventana-liquidacionserviciopublico.php";break;
					}
				}
			}

			function calcularSuma(pos){

				let valor = 0;
				let valorPagar = 0;

				valor = document.getElementsByName('dvaloresPagar[]');
				valorPagar = document.getElementsByName('dvalores[]');

				
				if(parseInt(valorPagar.item(pos).value) > parseInt(valor.item(pos).value)){

					alert('No puede pagar mas de lo registrado en la liquidacion.');
					valorPagar.item(pos).value = valor.item(pos).value;

				}else{
					document.form2.oculto.value = '';
					document.form2.submit();
					/* let sumar = 0;
					
					for(let x = 0; x <= valorPagar.length; x++){

						sumar=parseInt(sumar)+parseInt(valorPagar.item(x).value);

					}

					document.getElementById('totalc').value = sumar; */
				}
			}

			function despliegamodalm(_valor,_tip,mensa,pregunta)
			{
				document.getElementById("bgventanamodalm").style.visibility=_valor;
				if(_valor=="hidden")
				{
					document.getElementById('ventanam').src="";
					switch(document.getElementById('valfocus').value)
					{
						case "1":	document.getElementById('valfocus').value='';
									document.getElementById('tercero').focus();
									document.getElementById('tercero').select();
									break;
						case "2":	document.getElementById('valfocus').value='';
									document.getElementById('codingreso').focus();
									document.getElementById('codingreso').select();
									break;
					}
				}
				else
				{
					switch(_tip)
					{
						case "1":
							document.getElementById('ventanam').src="ventana-mensaje1.php?titulos="+mensa;break;
						case "2":
							document.getElementById('ventanam').src="ventana-mensaje3.php?titulos="+mensa;break;
						case "3":
							document.getElementById('ventanam').src="ventana-mensaje2.php?titulos="+mensa;break;
						case "4":
							document.getElementById('ventanam').src="ventana-consulta1.php?titulos="+mensa+"&idresp="+pregunta;break;	
					}
				}
			}

			function respuestaconsulta(pregunta)
			{
				switch(pregunta)
				{
					case "1":	document.form2.oculto.value='3';
								document.form2.submit();
								break;
					case "2":	document.form2.oculto.value='2';
								document.form2.submit();
								break;
				}
			}

			function redireccionarAlgo()
			{
				var numdocar=document.getElementById('idcomp').value;
				
				document.location.href = "teso-sinrecibocajaspver.php?idrecibo="+numdocar;
			}

		</script>
		<script src="css/programas.js"></script>
		<script src="css/calendario.js"></script>
		<link href="css/css2.css" rel="stylesheet" type="text/css" />
		<link href="css/css3.css" rel="stylesheet" type="text/css" />
		<link href="css/tabs.css" rel="stylesheet" type="text/css" />
	</head>
	<body>
		<div id="bgventanamodalm" class="bgventanamodalm">
            <div id="ventanamodalm" class="ventanamodalm">
                <IFRAME src="" name="ventanam" marginWidth=0 marginHeight=0 frameBorder=0 id="ventanam" frameSpacing=0 style=" width:700px; height:130px; top:200; overflow:hidden;"> 
                </IFRAME>
            </div>
        </div>
		<IFRAME src="alertas.php" name="alertas" id="alertas" style="display:none"></IFRAME>
		<span id="todastablas2"></span>
		<table>
			<tr><script>barra_imagenes("teso");</script><?php cuadro_titulos();?></tr>	 
			<tr><?php menu_desplegable("teso");?></tr>
			<tr>
				<td colspan="3" class="cinta">
					<a href="teso-sinrecibocajasp.php" class="mgbt"><img src="imagenes/add.png" title="Nuevo"/></a>
					<a onClick="guardar()" class="mgbt"><img src="imagenes/guarda.png"  title="Guardar" /></a>
					<a href="teso-buscasinrecibocajasp.php" class="mgbt"> <img src="imagenes/busca.png"  title="Buscar"/></a>
					<a onClick="mypop=window.open('plan-agenda.php','','');mypop.focus()" class="mgbt"><img src="imagenes/agenda1.png" title="Agenda" /></a>
					<a onClick="mypop=window.open('teso-principal.php','','');mypop.focus();" class="mgbt"><img src="imagenes/nv.png" title="Nueva ventana"></a>
					<a onClick="mypop=window.open('<?php echo $url2; ?>','','');mypop.focus();" class="mgbt"><img src="imagenes/duplicar_pantalla.png" title="Duplicar pesta&ntilde;a"></a>
					<a <?php if($_POST['oculto']==2) { ?>onClick="pdf()" <?php } ?>> <img src="imagenes/print.png"  title="Imprimir" /></a>
				</td>
			</tr>		  
		</table>
		<tr>
			<td colspan="3" class="tablaprin" align="center"> 
			<?php
				//$vigencia=date(Y);
				$vigusu=vigencia_usuarios($_SESSION['cedulausu']);
				$vigencia=$vigusu;

				$sqlr="SELECT cuentacaja FROM tesoparametros";
				$res=mysqli_query($linkbd_v7, $sqlr);
				while ($row =mysqli_fetch_row($res)) 
				{
					$_POST['cuentacaja']=$row[0];
				}
 			
	  			//*********** 11050501	CAJA PRINCIPAL esta es la cuenta que va a credito en todas las consignacones
				if(!$_POST['oculto'])
				{
					$check1="checked";
					$fec=date("d/m/Y");
					$_POST['vigencia']=$vigencia;
					$sqlr="SELECT cuentacaja FROM tesoparametros";
					$res=mysqli_query($linkbd_v7, $sqlr);
					while ($row = mysqli_fetch_row($res)) 
					{
						$_POST['cuentacaja']=$row[0];
					}
	
					$sqlr="select valor_inicial,valor_final, tipo from dominios where nombre_dominio='COBRO_RECIBOS' AND descripcion_valor='$vigusu' and  tipo='S'";
					$res = mysqli_query($linkbd_v7, $sqlr);
					while ($row = mysqli_fetch_row($res)) 
					{
						$_POST['cobrorecibo']=$row[0];
						$_POST['vcobrorecibo']=$row[1];
						$_POST['tcobrorecibo']=$row[2];	 
					}
					$sqlr="select max(id_recibos) from tesosinreciboscajasp ";
					$res=mysqli_query($linkbd_v7, $sqlr);
					$consec=0;
					while($r=mysqli_fetch_row($res))
	 				{
	  					$consec=$r[0];	  
	 				}
					$consec+=1;
					$_POST['idcomp'] = $consec;	
					if($_POST['fecha'] == ''){
						$fec=date("d/m/Y");
						$_POST['fecha']=$fec; 	
					}
						 		  			 
					$_POST['valor']=0;		 
		 			// echo "ddd";
				}
				//echo "dddd".$_POST[oculto];
				switch($_POST['tabgroup1'])
				{
					case 1:
						$check1='checked';
					break;
					case 2:
						$check2='checked';
					break;
					case 3:
						$check3='checked';
				}
			?>
 			<form name="form2" method="post" action=""> 
				<input name="encontro" type="hidden" value="<?php echo $_POST['encontro']?>" >
				<input name="cobrorecibo" type="hidden" value="<?php echo $_POST['cobrorecibo']?>" >
				<input name="vcobrorecibo" type="hidden" value="<?php echo $_POST['vcobrorecibo']?>" >
				<input name="tcobrorecibo" type="hidden" value="<?php echo $_POST['tcobrorecibo']?>" > 
				<input name="codcatastral" type="hidden" value="<?php echo $_POST['codcatastral']?>" >
 				<?php 
				if($_POST['oculto'] && $_POST['oculto']!='2')
				{
					switch($_POST['tiporec']) 
  	 				{
		
						case 3:
							$sqlr="select *from tesosinrecaudossp where tesosinrecaudossp.id_recaudo=$_POST[idrecaudo] and estado='S' and 3=$_POST[tiporec]";
							//echo "$sqlr";
							$_POST['encontro']="";
							$res = mysqli_query($linkbd_v7, $sqlr);
							while ($row = mysqli_fetch_row($res)) 
							{
								$_POST['concepto']=$row[6];	
								$_POST['valorecaudo']=$row[7];		
								$_POST['totalc']=$row[5];	
								$_POST['tercero']=$row[4];	
								$_POST['ntercero']=buscatercero($row[4]);	
								$_POST['encontro']=1;
							}
						break;	
					}
 				}
 				?>
    			<table class="inicio" align="center" >
					<tr >
						<td class="titulos" colspan="9">Ingresos Servicios Publicos</td>
						<td width="68" class="cerrar" ><a href="teso-principal.php">Cerrar</a></td>
					</tr>
      				<tr>
        				<td  class="saludo1" >No Recibo:</td>
        				<td width="203"  > <input name="cuentacaja" type="text" value="<?php echo $_POST['cuentacaja']?>" ><input name="idcomp" id="idcomp" type="text" size="5" value="<?php echo $_POST['idcomp']?>" onKeyUp="return tabular(event,this) "  readonly></td>
	  					<td width="103"   class="saludo1">Fecha:        </td>
        				<td width="229" >
							<input name="fecha" type="text" value="<?php echo $_POST['fecha']?>" maxlength="10" size="10" onKeyPress="javascript:return solonumeros(event)" onKeyUp="return tabular(event,this)" onChange="" id="fc_1198971545" onKeyDown="mascara(this,'/',patron,true)" title="DD/MM/YYYY">   <a href="#" onClick="displayCalendarFor('fc_1198971545');"><img src="imagenes/buscarep.png" align="absmiddle" border="0"></a>          
						</td>
         				<td width="101" class="saludo1">Vigencia:</td>
		  				<td width="350">
							<input type="text" id="vigencia" name="vigencia" size="8" onKeyPress="javascript:return solonumeros(event)" onKeyUp="return tabular(event,this)"  value="<?php echo $_POST['vigencia']?>" onClick="document.getElementById('tipocta').focus();document.getElementById('tipocta').select();" readonly> 
						</td>     
        			</tr>
      				<tr>
						<td class="saludo1"> Recaudo:</td>
						<td> 
							<select name="tiporec" id="tiporec" onKeyUp="return tabular(event,this)" onChange="validar()" >
          						<option value="3" <?php if($_POST['tiporec']=='3') echo "SELECTED"; ?>>Otros Recaudos</option>
        					</select>
          				</td>
						<?php
						$sqlr="";
						?>
        				<td class="saludo1">No Liquid:</td>
						
						<td style="width:5%;">
							<input type="text" id="idrecaudo" name="idrecaudo" value="<?php echo $_POST['idrecaudo']?>" style="width:70%;" onKeyUp="return tabular(event,this)" onBlur="validar()" ><a onClick="despliegamodal2('visible','2');" style="cursor:pointer;" title="Listado ordenes ingresos serv pub"><img src="imagenes/find02.png" style="width:20px;"/></a> 
						</td>
	 					<td class="saludo1">Recaudado en:</td>
						<td> 
							<select name="modorec" id="modorec" onKeyUp="return tabular(event,this)" onChange="validarBanco()" >
         						<option value="caja" <?php if($_POST['modorec']=='caja') echo "SELECTED"; ?>>Caja</option>
          						<option value="banco" <?php if($_POST['modorec']=='banco') echo "SELECTED"; ?>>Banco</option>
								<option value="ssf" <?php if($_POST['modorec']=='ssf') echo "SELECTED"; ?>>Ssf</option>
        					</select>
							<?php
							if ($_POST['modorec']=='banco')
							{
								?>
         						<select id="banco" name="banco"  onChange="validarBanco()" onKeyUp="return tabular(event,this)">
	      							<option value="">Seleccione....</option>
		  							<?php
	
									$sqlr="select tesobancosctas.estado,tesobancosctas.cuenta,tesobancosctas.ncuentaban,tesobancosctas.tipo,terceros.razonsocial,tesobancosctas.tercero from tesobancosctas,terceros where tesobancosctas.tercero=terceros.cedulanit and tesobancosctas.estado='S' ";
									$res=mysqli_query($linkbd_v7, $sqlr);
									while ($row =mysqli_fetch_row($res)) 
				    				{
										echo "<option value=$row[1] ";
										$i=$row[1];
					 					if($i==$_POST['banco'])
			 							{
											echo "SELECTED";
											$_POST['nbanco']=$row[4];
											$_POST['ter']=$row[5];
											$_POST['cb']=$row[2];
						 				}
					  					echo ">".$row[2]." - Cuenta ".$row[3]."</option>";	 	 
									}	 	
									?>
            					</select>
       							<input name="cb" type="hidden" value="<?php echo $_POST['cb']?>" >
								<input type="hidden" id="ter" name="ter" value="<?php echo $_POST['ter']?>" >           
								</td>
								<td> 
									<input type="text" id="nbanco" name="nbanco" value="<?php echo $_POST['nbanco']?>" size="40" readonly>
								</td>
								<?php
		   					}
							?> 
       				</tr>
	  				<tr>
						<td class="saludo1" width="71">Concepto:</td>
						<td colspan="3">
							<input name="concepto" size="90" type="text" value="<?php echo $_POST['concepto'] ?>" onKeyUp="return tabular(event,this)">
						</td>
						<?php
						if($_POST['tiporec']==2)
						{
							?>   
      						<td class="saludo1">No Cuota:</td>
							<td>
								<input name="cuotas" size="1" type="text" value="<?php echo $_POST['cuotas'] ?>" readonly>/<input type="text" id="tcuotas" name="tcuotas" value="<?php echo $_POST['tcuotas']?>" size="1"  readonly >
							</td>
      						<?php
	   					}
	  					?>
	  				</tr>
      				<tr>
						<td class="saludo1" width="71">Valor:</td>
						<td>
							<input type="text" id="valorecaudo" name="valorecaudo" value="<?php echo $_POST['valorecaudo']?>" size="30" onKeyUp="return tabular(event,this)" readonly >
						</td>
						<td  class="saludo1">Documento: </td>
        				<td>
							<input name="tercero" type="text" value="<?php echo $_POST['tercero']?>" size="20" onKeyUp="return tabular(event,this)" readonly>
         				</td>
			  			<td class="saludo1">Contribuyente:</td>
	  						<td>
								<input type="text" id="ntercero" name="ntercero" value="<?php echo $_POST['ntercero']?>" size="50" onKeyUp="return tabular(event,this) "  readonly><input type="hidden" id="cb" name="cb" value="<?php echo $_POST['cb']?>" ><input type="hidden" id="ct" name="ct" value="<?php echo $_POST['ct']?>" >
	  						</td>
						<td>
							<input type="hidden" value="1" name="oculto">
							<input type="hidden" value="<?php echo $_POST['trec']?>"  name="trec">
							<input type="hidden" value="0" name="agregadet">
						</td>
					</tr>
     
      			</table>
     			<div class="subpantallac7">
      			<?php 
					if($_POST['oculto'] && $_POST['encontro']=='1')
					{
  						switch($_POST['tiporec']) 
  	 					{
	  						case 3: ///*****************otros recaudos *******************
								$_POST['trec']='OTROS RECAUDOS';	 
								// echo $_POST[tcobrorecibo];
								$_POST['dcoding']= array(); 		 
								$_POST['dncoding']= array(); 		 
								$_POST['dvalores']= array();
								$_POST['dvaloresPagar']= array();
		
  								$sqlr="select *from tesosinrecaudossp_det where tesosinrecaudossp_det.id_recaudo=$_POST[idrecaudo] and estado ='S'  and 3=$_POST[tiporec]";
								//echo "$sqlr";				 
								$res=mysqli_query($linkbd_v7, $sqlr);

								while ($row =mysqli_fetch_row($res)) 
								{
									$sqlrIng = "SELECT sum(sp2.valor) FROM tesosinreciboscajasp AS sp1, tesosinreciboscajasp_det AS sp2 WHERE sp1.id_recaudo = '$row[1]' AND sp2.ingreso = '$row[2]' AND sp1.id_recibos=sp2.id_recibos ";
									$resIng=mysqli_query($linkbd_v7, $sqlrIng);
									$rowIng =mysqli_fetch_row($resIng);

									$_POST['dcoding'][]=$row[2];
									$sqlr2="select nombre from tesoingresos where codigo='".$row[2]."'";
									$res2=mysqli_query($linkbd_v7, $sqlr2);
									$row2 =mysqli_fetch_row($res2); 
									$_POST['dncoding'][]=$row2[0];	

									$_POST['dvalores'][]=$row[3]-$rowIng[0];		 	
									$_POST['dvaloresPagar'][]=$row[3]-$rowIng[0];		 	
								}
							break;
   						}
					}
					?>
	   				<table class="inicio">
	   	   				<tr>
							<td colspan="4" class="titulos">Detalle Recibo de Caja</td>
						</tr>                  
						<tr>
							<td class="titulos2">Codigo</td>
							<td class="titulos2">Ingreso</td>
							<td class="titulos2">Valor</td>
							<td class="titulos2">Valor a ingresar</td>
						</tr>
						<?php 		
						if ($_POST['elimina']!='')
		 				{ 
							//echo "<TR><TD>ENTROS :".$_POST[elimina]."</TD></TR>";
							$posi=$_POST['elimina'];
		  
							unset($_POST['dcoding'][$posi]);	
							unset($_POST['dncoding'][$posi]);			 
							unset($_POST['dvalores'][$posi]);			  		 
							unset($_POST['dvaloresPagar'][$posi]);			  		 
							$_POST['dcoding']= array_values($_POST['dcoding']); 		 
							$_POST['dncoding']= array_values($_POST['dncoding']); 		 		 
							$_POST['dvalores']= array_values($_POST['dvalores']); 		 		 		 		 		 
							$_POST['dvaloresPagar']= array_values($_POST['dvaloresPagar']); 		 		 		 		 		 
		 				}	 
						if ($_POST['agregadet']=='1')
						{
							$_POST['dcoding'][]=$_POST['codingreso'];
							$_POST['dncoding'][]=$_POST['ningreso'];			 		
							$_POST['dvalores'][]=$_POST['valor'];
							$_POST['dvaloresPagar'][]=$_POST['valor'];
							$_POST['agregadet']=0;
		  					?>
							<script>
								//document.form2.cuenta.focus();	
								document.form2.codingreso.value="";
								document.form2.valor.value="";	
								document.form2.ningreso.value="";				
								document.form2.codingreso.select();
								document.form2.codingreso.focus();	
							</script>
		  					<?php
		  				}
		  				$_POST['totalc']=0;
						for ($x=0;$x<count($_POST['dcoding']);$x++)
						{		 
		 					echo "<tr>
							 		<td class='saludo1'>
									 <input name='dcoding[]' value='".$_POST['dcoding'][$x]."' type='text' size='4' readonly>
									</td>
									<td class='saludo1'>
										<input name='dncoding[]' value='".$_POST['dncoding'][$x]."' type='text' size='90' readonly>
									</td>
									<td class='saludo1'>
										<input name='dvaloresPagar[]'  value='".$_POST['dvaloresPagar'][$x]."' type='text' size='15' readonly>
									</td>
									<td class='saludo1'>
										<input name='dvalores[]'  value='".$_POST['dvalores'][$x]."' type='text' size='15' onChange='calcularSuma(".$x.")'>
									</td>
								</tr>";
								$_POST['totalc']=$_POST['totalc']+$_POST['dvalores'][$x];
								$_POST['totalcf']=number_format($_POST['totalc'],2);
		 				}
						$resultado = convertir($_POST['totalc'],"PESOS");
						$_POST['letras']=$resultado." PESOS M/CTE";
						echo "<tr>
								<td></td>
								<td class='saludo2'>Total</td>
								<td class='saludo1'>
									<input name='totalcf' id='totalcf' type='text' value='$_POST[totalcf]' readonly>
									
								</td>
								<td class='saludo1'>
									
									<input name='totalc' id='totalc' type='text' value='$_POST[totalc]' readonly>
								</td>
								</tr>
								<tr>
									<td class='saludo1'>Son:</td>
									<td colspan='5' >
										<input name='letras' type='text' value='$_POST[letras]' size='200'>
									</td>
								</tr>";
						?> 
	   				</table>
				</div>
	  			<?php
				if($_POST['oculto']=='2')
				{
					preg_match("/([0-9]{2})\/([0-9]{2})\/([0-9]{4})/", $_POST['fecha'], $fecha);
					$fechaf = $fecha[3]."-".$fecha[2]."-".$fecha[1];
					$bloq = bloqueos($_SESSION['cedulausu'],$fechaf);	
					if($bloq >= 1)
					{
						//************VALIDAR SI YA FUE GUARDADO ************************
						switch($_POST['tiporec']) 
						{
	   
	  						case 3: //**************OTROS RECAUDOS
								/* $sqlr="select count(*) from tesosinreciboscajasp where id_recaudo=$_POST[idrecaudo] and tipo='3' AND ESTADO='S'";
								$res=mysqli_query($linkbd_v7, $sqlr);
								//echo $sqlr;
								while($r=mysqli_fetch_row($res))
								{
									$numerorecaudos=$r[0];
								}
  								if($numerorecaudos==0)
   								{  */
									preg_match("/([0-9]{2})\/([0-9]{2})\/([0-9]{4})/", $_POST['fecha'], $fecha);
									$fechaf=$fecha[3]."-".$fecha[2]."-".$fecha[1];
									//*********************CREACION DEL COMPROBANTE CONTABLE ***************************
									//***busca el consecutivo del comprobante contable
									$concecc=0;
									$sqlr="select max(id_recibos ) from tesosinreciboscajasp  ";
									//echo $sqlr;
									$res=mysqli_query($linkbd_v7, $sqlr);
									while($r=mysqli_fetch_row($res))
									{
	  									$concecc=$r[0];	  
									}
									$concecc+=1;
	 								// $consec=$concecc;
									//***cabecera comprobante
	 								$sqlr="insert into comprobante_cab (numerotipo,tipo_comp,fecha,concepto,total,total_debito,total_credito,diferencia,estado) values ($concecc,42,'$fechaf','$_POST[concepto]',0,$_POST[totalc],$_POST[totalc],0,'1')";
									mysqli_query($linkbd_v7, $sqlr);
									$idcomp=mysqli_insert_id($linkbd_v7);
	
	 								$sqlr="insert into  pptocomprobante_cab (numerotipo,tipo_comp,fecha,concepto,vigencia,total_debito,total_credito,diferencia,estado) values($concecc,23,'$fechaf','INGRESOS SERV PUBLICOS $_POST[concepto]',$vigusu,0,0,0,'1')";
	 	 							mysqli_query($linkbd_v7, $sqlr);
									//echo "$sqlr <br>";	  
									echo "<input type='hidden' name='ncomp' value='$idcomp'>";
									//******************* DETALLE DEL COMPROBANTE CONTABLE *********************
									for($x=0;$x<count($_POST['dcoding']);$x++)
									{
										//***** BUSQUEDA INGRESO ********
										$sqlri="Select * from tesoingresos_det where codigo='".$_POST['dcoding'][$x]."' and vigencia=(SELECT MAX(vigencia) FROM tesoingresos_det WHERE codigo = '".$_POST['dcoding'][$x]."')";
										$resi=mysqli_query($linkbd_v7, $sqlri);
										//	echo "$sqlri <br>";	    
										while($rowi=mysqli_fetch_row($resi))
		 								{
											$porce=$rowi[5];
											$vi=$_POST['dvalores'][$x]*($porce/100);
	    									//**** busqueda cuenta presupuestal*****
											$sqlr="insert into  pptocomprobante_det (cuenta,tercero,detalle,valdebito,valcredito,estado,vigencia,tipo_comp,numerotipo) values('".$rowi[6]."','".$_POST['tercero']."','INGRESO SERV PUBLICOS ',".$vi.",0,'1','$_POST[vigencia]',23,'$concecc')";
	  										mysqli_query($linkbd_v7, $sqlr); 	
											//echo "$sqlr <br>";	  
											//busqueda concepto contable
											$sqlrc="Select * from conceptoscontables_det where estado='S' and modulo='4' AND codigo=".$rowi[2]." and tipo='C'";
											$resc=mysqli_query($linkbd_v7, $sqlrc);	  
											//echo "concc: $sqlrc - $_POST[cobrorecibo]<br>";	      
											while($rowc=mysqli_fetch_row($resc))
											{
												if($_POST['dcoding'][$x]==$_POST['cobrorecibo'])
												{
			  										//$columna= $rowc[7];
													//  echo "cred  $rowc[7]<br>";	      
													if($rowc[7]=='S')
													{
														$columna= $rowc[7];
													}
													else
													{
				  										$columna= 'N';
													}
			  										$cuentacont=$rowc[4];
			 									}
												else
												{
													$columna= $rowc[6];	
													$cuentacont=$rowc[4];			 
												}
												if($columna=='S')
												{				 
													$valorcred=$_POST['dvalores'][$x]*($porce/100);
													$valordeb=0;
													//	echo "cuenta: $rowc[4] - $columna <br>";	      
													if($rowc[3]=='N')
													{
														//*****inserta del concepto contable  
														//***********MODIFICAR CUENTA PPTAL DE INGRESO AGREGARLE EL RECAUDO *********
														$sqlrpto="Select * from pptocuentas where estado='S' and cuenta='".$rowi[6]."' and vigencia='$vigusu'";
														$respto=mysqli_query($linkbd_v7, $sqlrpto);	  
														//echo "con: $sqlrpto <br>";	      
														$rowpto=mysqli_fetch_row($respto);
			
														$vi=$_POST['dvalores'][$x]*($porce/100);
														$sqlr="update pptocuentaspptoinicial  set ingresos=ingresos+".$vi." where cuenta ='".$rowi[6]."' and vigencia='$vigusu'";
														mysqli_query($linkbd_v7, $sqlr);	
			
														//****creacion documento presupuesto ingresos
														$sqlr="insert into pptosinrecibocajappto (cuenta,idrecibo,valor,vigencia) values('$rowi[6]',$concecc,$vi,'".$vigusu."')";
														mysqli_query($linkbd_v7, $sqlr);	
														//echo "ppt:$sqlr";
														//************ FIN MODIFICACION PPTAL
			
														$sqlr="insert into comprobante_det (id_comp,cuenta,tercero,centrocosto,detalle,cheque,valdebito,valcredito,estado,vigencia) values ('42 $concecc','".$cuentacont."','".$_POST['tercero']."','".$rowc[5]."','Ingreso ".strtoupper($_POST['dncoding'][$x])."','',".$valordeb.",".$valorcred.",'1','".$_POST['vigencia']."')";
														mysqli_query($linkbd_v7, $sqlr);
														//echo $sqlr."<br>";						
														//***cuenta caja o banco
														if($_POST['modorec']=='caja')
														{				 
															$cuentacb=$_POST['cuentacaja'];
															$cajas=$_POST['cuentacaja'];
															$cbancos="";
														}
														if($_POST['modorec']=='banco')
														{
															$cuentacb=$_POST['banco'];				
															$cajas="";
															$cbancos=$_POST['banco'];
														}

														if($_POST['modorec']=='ssf')
														{				 
															$cuentacb='190690001';
															$cajas='190690001';
															$cbancos="";
														}

														//$valordeb=$_POST[dvalores][$x]*($porce/100);
														//$valorcred=0;
														//echo "bc:$_POST[modorec] - $cuentacb";
														$sqlr="insert into comprobante_det (id_comp,cuenta,tercero,centrocosto,detalle,cheque,valdebito,valcredito,estado,vigencia) values ('42 $concecc','".$cuentacb."','".$_POST['tercero']."','".$rowc[5]."','Ingreso ".strtoupper($_POST['dncoding'][$x])."','',".$valorcred.",0,'1','".$_POST['vigencia']."')";
														mysqli_query($linkbd_v7, $sqlr);

														//echo "Conc: $sqlr <br>";					
													}
			
			  									}
		 									}
		 								}
									}	
									//************ insercion de cabecera recaudos ************

									$sqlr="insert into tesosinreciboscajasp (id_comp,fecha,vigencia,id_recaudo,recaudado,cuentacaja,cuentabanco,valor,estado,tipo) values($idcomp,'$fechaf',".$vigusu.",$_POST[idrecaudo],'$_POST[modorec]','$cajas','$cbancos','$_POST[totalc]','S','$_POST[tiporec]')";	  
									if (!mysqli_query($linkbd_v7, $sqlr))
									{
	 									echo "<table><tr><td class='saludo1'><center><font color=blue><img src='imagenes/alert.png'> Manejador de Errores de la Clase BD<br><font size=1></font></font><br><p align=center>No se pudo ejecutar la petici�n: <br><font color=red><b>$sqlr</b></font></p>";
										//$e =mysql_error($respquery);
										echo "Ocurri� el siguiente problema:<br>";
										//echo htmlentities($e['message']);
										echo "<pre>";
										///echo htmlentities($e['sqltext']);
										// printf("\n%".($e['offset']+1)."s", "^");
										echo "</pre></center></td></tr></table>";
									}
  		
									else
  		 							{
		  								echo "<table><tr><td class='saludo1'><center>Se ha almacenado el Recibo de Caja con Exito <img src='imagenes/confirm.png'><script>redireccionarAlgo()</script></center></td></tr></table>";
										$totalRecaudoSp = 0;
										$totalIngresoSp = 0;
										$totalIng = 0;

										/* $sqlrRecaudosp = "SELECT SUM(valor) FROM tesosinrecaudossp_det WHERE id_recaudo = '$_POST[idrecaudo]' ";
										$resRecaudosp = mysqli_query($linkbd_v7, $sqlrRecaudosp);
										$rowRecaudoSp = mysqli_fetch_row($resRecaudoSp);
										$totalRecaudoSp = $rowRecaudoSp[0]; */

										$sqlrRecSp = "SELECT SUM(valor) FROM tesosinrecaudossp_det WHERE id_recaudo = '$_POST[idrecaudo]' ";
										$resRecSp = mysqli_query($linkbd_v7, $sqlrRecSp);
										$rowRecSp = mysqli_fetch_row($resRecSp);

										$totalRecaudoSp = round($rowRecSp[0],2);

										$sqlrIngresoSp = "SELECT sum(sp2.valor) FROM tesosinreciboscajasp AS sp1, tesosinreciboscajasp_det AS sp2 WHERE sp1.id_recaudo = '$_POST[idrecaudo]' AND sp1.id_recibos=sp2.id_recibos ";
										$resIngresoSp = mysqli_query($linkbd_v7, $sqlrIngresoSp);
										$rowIngresoSp = mysqli_fetch_row($resIngresoSp);

										$totalIngresoSp = $rowIngresoSp[0];

										$totalIng = round($totalIngresoSp, 2) + $_POST['totalc'];
										
										if(round($totalRecaudoSp,0) == round($totalIng,0)){
											$sqlr="update tesosinrecaudossp set estado='P' WHERE ID_RECAUDO=$_POST[idrecaudo]";
		  									mysqli_query($linkbd_v7, $sqlr);
										}


										//echo "<script>despliegamodalm('visible','1','Se ha almacenado el Recaudo con Exito');</script>";

		  								?>
										<script>
											
										</script>
		  								<?php
		  							}
    							/* } //fin de la verificacion
								else
								{
	  								echo "<table ><tr><td class='saludo1'><center>Ya Existe un Recibo de Caja para esta Liquidacion <img src='imagenes/alert.png'></center></td></tr></table>";
	 							} */
	   						break;
	   						//********************* INDUSTRIA Y COMERCIO
						} //*****fin del switch
						$_POST['ncomp']=$concecc;
						//******* GUARDAR DETALLE DEL RECIBO DE CAJA ******	
						for($x=0;$x<count($_POST['dcoding']);$x++)
						{
		  					$sqlr="insert into tesosinreciboscajasp_det (id_recibos,ingreso,valor,estado) values($concecc,'".$_POST['dcoding'][$x]."',".$_POST['dvalores'][$x].",'S')";	  
		  					mysqli_query($linkbd_v7, $sqlr);  
							//echo $sqlr."<br>";
		 				}		
						//***** FIN DETALLE RECIBO DE CAJA ***************		
	  				}
  					else
   					{
    					echo "<div class='inicio'><img src='imagenes\alert.png'> No Tiene los Permisos para Modificar este Documento</div>";
   					}
  					//****fin if bloqueo  
   				}//**fin del oculto 
				?>	
				<div id="bgventanamodal2">
					<div id="ventanamodal2">
						<IFRAME  src="" name="buster" marginWidth=0 marginHeight=0 frameBorder=0 id="ventana2" frameSpacing=0 style="left:500px; width:900px; height:500px; top:200;"> 
						</IFRAME>
					</div>
				</div>	
			</form>
 		</td>
	</tr>
</table>
</body>
</html>