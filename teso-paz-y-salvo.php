<?php
    require __DIR__ . '/vendor/autoload.php';

    use Laravel\Route;

    header("Cache-control: no-cache, no-store, must-revalidate");
    header("Content-Type: text/html;charset=utf8");

	require "comun.inc";
	require "funciones.inc";

	$linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");

	session_start();
    date_default_timezone_set("America/Bogota");

    $username = $_SESSION['nickusu'];
    $result = mysqli_query($linkbd, 'SELECT pass_usu FROM usuarios WHERE usu_usu = \'' . $username . '\';');
    $password = strtolower(mysqli_fetch_object($result)->pass_usu);
?>
<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="utf-8"/>
        <meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1"/>
        <meta name="viewport" content="user-scalable=no">
        <title>:: IDEAL 10 - Tesoreria</title>
        <link href="favicon.ico" rel="shortcut icon"/>
        <link href="css/css2.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
        <link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
        <script src="css/programas.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
        <script src="https://code.jquery.com/jquery-3.7.1.min.js" integrity="sha256-/JqT3SQfawRcv/BIHPThkBvs0OEvtFFmqPF/lYI/Cxo=" crossorigin="anonymous"></script>

        <style>
            html, body {
                width: 100%;
                height: 100%;
                margin: 0;
            }

            body {
                display: flex;
                flex-direction: column;
            }

            .laravel-iframe {
                flex-grow: 1;
                border: none;
            }
        </style>
    </head>
    <body>
        <iframe src="alertas.php" name="alertas" id="alertas" style="display:none"></iframe>
        <table>
            <tr><script>barra_imagenes("teso");</script><?php cuadro_titulos();?></tr>
            <tr><?php menu_desplegable("teso");?></tr>
        </table>

        <?php
            Route::iframe('financiero/paz_y_salvos', ['username' => $username, 'password' => $password]);
        ?>

        <script>
            $(document).ready(() => {
                $('#fullscreen-btn').click(() =>
                    window.open($('#laravel-iframe').src, '_blank')
                )

                $('#back-btn').click(() =>
                    $('#laravel-iframe').contentWindow.history.back()
                )

                $('#forward-btn').click(() =>
                    $('#laravel-iframe').contentWindow.history.forward()
                )

                window.addEventListener('message', evt => {
                    if (evt.data == 'duplicateWindow') {
                        window.open('teso-principal', '_blank')
                    }
                })
            })
        </script>
    </body>
</html>
