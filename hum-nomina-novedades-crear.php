<?php

    header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=utf8");
	require 'comun.inc';
	require 'funciones.inc';
	session_start();
    if(empty($_SESSION)){
        header("location: index.php");
    }
	$linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");
	cargarcodigopag($_GET['codpag'],$_SESSION["nivel"]);
	date_default_timezone_set("America/Bogota");
?>
<!DOCTYPE >
<html lang=es>
	<head>
    <meta charset="utf-8"/>
		<meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1"/>
		<meta name="viewport" content="user-scalable=no">
		<title>:: IDEAL 10 - Gestión humana</title>
		<link href="favicon.ico" rel="shortcut icon"/>
		<link href="css/css2z.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
		<link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
		<link href="css/css4.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
        <link href="css/style.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css"/>
        <link rel="stylesheet" href="sweetalert2/dist/sweetalert2.min.css">
        <script src="sweetalert2/dist/sweetalert2.min.js"></script>
		<script src="vue/vue.min.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script src="css/programas.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
	</head>
	<body>
		<header>
			<table>
				<tr><script>barra_imagenes("hum");</script><?php cuadro_titulos();?></tr>
			</table>
		</header>

        <main id="myapp" v-cloak>
            <input type="hidden" value = "1" ref="pageType">
            <div class="loading-container" v-show="isLoading" >
                <p class="text-loading"data-loading="IDEAL 10">IDEAL 10</p>
            </div>
            <nav>
                <table>
                    <tr><?php menu_desplegable("hum");?></tr>
                </table>
                <div class="bg-white group-btn p-1" id="newNavStyle">
                    <button type="button" @click="save()" class="btn btn-white btn-primary-hover d-flex justify-between align-items-center">
                        <span>Guardar</span>
                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 -960 960 960">
                            <path d="M840-680v480q0 33-23.5 56.5T760-120H200q-33 0-56.5-23.5T120-200v-560q0-33 23.5-56.5T200-840h480l160 160Zm-80 34L646-760H200v560h560v-446ZM480-240q50 0 85-35t35-85q0-50-35-85t-85-35q-50 0-85 35t-35 85q0 50 35 85t85 35ZM240-560h360v-160H240v160Zm-40-86v446-560 114Z">
                            </path>
                        </svg>
                    </button>
                    <button type="button" @click="window.location.href='hum-nomina-novedades-buscar'" class="btn btn-white btn-primary-hover d-flex justify-between align-items-center">
                        <span>Buscar</span>
                        <svg class="fill-black group-hover:fill-white w-5 h-5" xmlns="http://www.w3.org/2000/svg" viewBox="0 -960 960 960"><path d="M784-120 532-372q-30 24-69 38t-83 14q-109 0-184.5-75.5T120-580q0-109 75.5-184.5T380-840q109 0 184.5 75.5T640-580q0 44-14 83t-38 69l252 252-56 56ZM380-400q75 0 127.5-52.5T560-580q0-75-52.5-127.5T380-760q-75 0-127.5 52.5T200-580q0 75 52.5 127.5T380-400Z"></path></svg>
                    </button>
                    <button type="button" class="btn btn-white btn-primary-hover d-flex justify-between align-items-center" @click="mypop=window.open('hum-principal.php','',''); mypop.focus();">
                        <span>Nueva ventana</span>
                        <svg class="fill-black group-hover:fill-white w-5 h-5" xmlns="http://www.w3.org/2000/svg"  viewBox="0 -960 960 960" ><path d="M320-120v-80h80v-80H160q-33 0-56.5-23.5T80-360v-400q0-33 23.5-56.5T160-840h640q33 0 56.5 23.5T880-760v400q0 33-23.5 56.5T800-280H560v80h80v80H320ZM160-360h640v-400H160v400Zm0 0v-400 400Z"/></svg>
                    </button>
                    <button type="button" @click="mypop=window.open('hum-nomina-novedades-crear','','');mypop.focus();" class="btn btn-white btn-primary-hover d-flex justify-between align-items-center">
                        <span class="group-hover:text-white">Duplicar pantalla</span>
                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 -960 960 960"><path d="M640-160v-360H160v360h480Zm80-200v-80h80v-360H320v200h-80v-200q0-33 23.5-56.5T320-880h480q33 0 56.5 23.5T880-800v360q0 33-23.5 56.5T800-360h-80ZM160-80q-33 0-56.5-23.5T80-160v-360q0-33 23.5-56.5T160-600h480q33 0 56.5 23.5T720-520v360q0 33-23.5 56.5T640-80H160Zm400-603ZM400-340Z"></path></svg>
                    </button>
                    <button type="button" @click="window.location.href='hum-nomina-novedades-buscar'" class="btn btn-success btn-success-hover d-flex justify-between align-items-center">
                        <span>Atras</span>
                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 -960 960 960"><path d="M760-200v-160q0-50-35-85t-85-35H273l144 144-57 56-240-240 240-240 57 56-144 144h367q83 0 141.5 58.5T840-360v160h-80Z"></path></svg>
                    </button>
                </div>
            </nav>
            <section class="bg-white w-100">
                <div class="nav-content active">
                    <h2 class="titulos m-0">Crear preliquidación de nómina</h2>
                    <p class="m-0 ms-2">Todos los campos con (<span class="text-danger fw-bolder">*</span>) son obligatorios.</p>
                    <div class="d-flex">
                        <div class="form-control">
                            <label class="form-label">Fecha <span class="text-danger fw-bolder">*</span>:</label>
                            <input type="date" v-model="txtFecha">
                        </div>
                        <div class="form-control">
                            <label class="form-label">Año<span class="text-danger fw-bolder">*</span>:</label>
                            <select v-model="selectVigencia" @change="arrContratos=[]">
                                <option v-for="(data,index) in arrVigencias" :key="index" :value="data">{{data}}</option>
                            </select>
                        </div>
                        <div class="form-control">
                            <label class="form-label">Mes<span class="text-danger fw-bolder">*</span>:</label>
                            <div class="d-flex">
                                <select v-model="selectMes" @change="arrContratos=[]">
                                    <option v-for="(data,index) in arrMeses" :value="data.id">{{data.nombre}}</option>
                                </select>
                                <button type="button" @click="getData()" class="btn btn-primary">Preparar</button>
                            </div>
                        </div>
                    </div>
                </div>
                <div v-if="arrContratos.length>0">
                    <div class="d-flex">
                        <div class="form-control">
                            <label class="form-label">Descripción <span class="text-danger fw-bolder">*</span>:</label>
                            <textarea style="max-height:4rem;" v-model="txtDescripcion"></textarea>
                        </div>
                    </div>
                    <!--TABS-->
                    <div ref="rTabs" class="nav-tabs p-1">
                        <div class="nav-item active" @click="showTab(1);indexOtros=0">Sueldo</div>
                        <div class="nav-item" @click="showTab(2);indexOtros=4">Auxilio de transporte</div>
                        <div class="nav-item" @click="showTab(3);indexOtros=3">Subsidio de alimentación</div>
                        <div class="nav-item" @click="showTab(4);indexOtros=1">Horas extras</div>
                        <div class="nav-item" @click="showTab(5);indexOtros=2;selectTipoPago=arrSelectVariablesPago[0].codigo">Otros pagos</div>
                    </div>
                    <div ref="rTabsContent" class="nav-tabs-content">
                        <div class="nav-content active">
                            <div class="d-flex justify-center">
                                <div class="form-control justify-between w-25">
                                    <label for=""></label>
                                    <button type="button" class="btn btn-primary" @click="isModalContrato=true;">Seleccionar contratos</button>
                                </div>
                            </div>
                            <div class="overflow-auto" style="height:40vh">
                                <table class="table fw-normal">
                                    <thead class="text-center">
                                        <tr>
                                            <th>Empleado</th>
                                            <th>CC/NIT</th>
                                            <th>Básico</th>
                                            <th>Días</th>
                                            <th>Vacaciones</th>
                                            <th>Incapacidades</th>
                                            <th>Dias laborados</th>
                                            <th>Básico</th>
                                            <th>
                                                <div class="d-flex">
                                                    <div class="form-control">
                                                        <label>Salud empleado</label>
                                                        <div class="d-flex justify-center align-items-center">
                                                            <label for="labelCheckNameSF" class="form-switch">
                                                                <input type="checkbox" id="labelCheckNameSF"
                                                                v-model="arrVariablesPago[indexOtros].is_checked_salud_empleado" checked @change="changeAll('otro');" @click="tipoParam='SF'">
                                                                <span></span>
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </th>
                                            <th>
                                                <div class="d-flex">
                                                    <div class="form-control">
                                                        <label>Salud empresa</label>
                                                        <div class="d-flex justify-center align-items-center">
                                                            <label for="labelCheckNameSE" class="form-switch">
                                                                <input type="checkbox" id="labelCheckNameSE"
                                                                v-model="arrVariablesPago[indexOtros].is_checked_salud_empresa" checked @change="changeAll('otro');" @click="tipoParam='SE'">
                                                                <span></span>
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </th>
                                            <th>
                                                <div class="d-flex">
                                                    <div class="form-control">
                                                        <label>Pension empleado</label>
                                                        <div class="d-flex justify-center align-items-center">
                                                            <label for="labelCheckNamePF" class="form-switch">
                                                                <input type="checkbox" id="labelCheckNamePF"
                                                                v-model="arrVariablesPago[indexOtros].is_checked_pension_empleado" checked @change="changeAll('otro');" @click="tipoParam='PF'">
                                                                <span></span>
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </th>
                                            <th>
                                                <div class="d-flex">
                                                    <div class="form-control">
                                                        <label>Pension empresa</label>
                                                        <div class="d-flex justify-center align-items-center">
                                                            <label for="labelCheckNamePE" class="form-switch">
                                                                <input type="checkbox" id="labelCheckNamePE"
                                                                v-model="arrVariablesPago[indexOtros].is_checked_pension_empresa" checked @change="changeAll('otro');" @click="tipoParam='PE'">
                                                                <span></span>
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </th>
                                            <th>
                                                <div class="d-flex">
                                                    <div class="form-control">
                                                        <label>ARL</label>
                                                        <div class="d-flex justify-center align-items-center">
                                                            <label for="labelCheckNameARL" class="form-switch">
                                                                <input type="checkbox" id="labelCheckNameARL"
                                                                v-model="arrVariablesPago[indexOtros].is_checked_arl" checked @change="changeAll('otro');" @click="tipoParam='ARL'">
                                                                <span></span>
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </th>
                                            <th>
                                                <div class="d-flex">
                                                    <div class="form-control">
                                                        <label>CCF</label>
                                                        <div class="d-flex justify-center align-items-center">
                                                            <label for="labelCheckNameCCF" class="form-switch">
                                                                <input type="checkbox" id="labelCheckNameCCF"
                                                                v-model="arrVariablesPago[indexOtros].is_checked_ccf" checked @change="changeAll('otro');" @click="tipoParam='CCF'">
                                                                <span></span>
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </th>
                                            <th>
                                                <div class="d-flex">
                                                    <div class="form-control">
                                                        <label>ICBF</label>
                                                        <div class="d-flex justify-center align-items-center">
                                                            <label for="labelCheckNameICBF" class="form-switch">
                                                                <input type="checkbox" id="labelCheckNameICBF"
                                                                v-model="arrVariablesPago[indexOtros].is_checked_icbf" checked @change="changeAll('otro');" @click="tipoParam='ICBF'">
                                                                <span></span>
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </th>
                                            <th>
                                                <div class="d-flex">
                                                    <div class="form-control">
                                                        <label>SENA</label>
                                                        <div class="d-flex justify-center align-items-center">
                                                            <label for="labelCheckNameSENA" class="form-switch">
                                                                <input type="checkbox" id="labelCheckNameSENA"
                                                                v-model="arrVariablesPago[indexOtros].is_checked_sena" checked @change="changeAll('otro');" @click="tipoParam='SENA'">
                                                                <span></span>
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </th>
                                            <th>
                                                <div class="d-flex">
                                                    <div class="form-control">
                                                        <label>Ins.tecnicos</label>
                                                        <div class="d-flex justify-center align-items-center">
                                                            <label for="labelCheckNameINS" class="form-switch">
                                                                <input type="checkbox" id="labelCheckNameINS"
                                                                v-model="arrVariablesPago[indexOtros].is_checked_inst_tecnico" checked @change="changeAll('otro');" @click="tipoParam='INS'">
                                                                <span></span>
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </th>
                                            <th>
                                                <div class="d-flex">
                                                    <div class="form-control">
                                                        <label>ESAP</label>
                                                        <div class="d-flex justify-center align-items-center">
                                                            <label for="labelCheckNameESAP" class="form-switch">
                                                                <input type="checkbox" id="labelCheckNameESAP"
                                                                v-model="arrVariablesPago[indexOtros].is_checked_esap" checked @change="changeAll('otro');" @click="tipoParam='ESAP'">
                                                                <span></span>
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </th>
                                            <th>
                                                <div class="d-flex justify-center">
                                                    <button type="button" @click="delItem()" class="btn btn-danger">x</button>
                                                </div>
                                            </th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr v-for="(data,index) in arrVariablesPago[indexOtros].detalle" :key="index">
                                            <td>{{data.nombre}}</td>
                                            <td>{{data.documento}}</td>
                                            <td>
                                                <div class="d-flex">
                                                    <div class="form-control">
                                                        <input type="text" class="text-right" v-model="data.salario_formateado" @keyup="updateValores">
                                                    </div>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="d-flex">
                                                    <div class="form-control">
                                                        <input type="text" class="text-center" v-model="data.dias" @keyup="updateValores">
                                                    </div>
                                                </div>
                                            </td>
                                            <td class="text-center">{{data.dias_vacaciones}}</td>
                                            <td class="text-center">{{data.dias_incapacidad}}</td>
                                            <td class="text-center">{{data.dias_laborados}}</td>
                                            <td class="text-right">{{formatMoney(data.valor_dias)}}</td>
                                            <td>
                                                <div class="d-flex justify-center align-items-center">
                                                    <label :for="'labelCheckNameSF'+index" class="form-switch">
                                                        <input type="checkbox" :id="'labelCheckNameSF'+index"
                                                        @change="changeStatusItem(data.codigo,'sueldo');"  @click="tipoParam='SF'" :checked="data.is_checked_salud_empleado">
                                                        <span></span>
                                                    </label>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="d-flex justify-center align-items-center">
                                                    <label :for="'labelCheckNameSE'+index" class="form-switch">
                                                        <input type="checkbox" :id="'labelCheckNameSE'+index"
                                                        @change="changeStatusItem(data.codigo,'sueldo');" @click="tipoParam='SE'" :checked="data.is_checked_salud_empresa">
                                                        <span></span>
                                                    </label>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="d-flex justify-center align-items-center">
                                                    <label :for="'labelCheckNamePF'+index" class="form-switch">
                                                        <input type="checkbox" :id="'labelCheckNamePF'+index"
                                                        @change="changeStatusItem(data.codigo,'sueldo');" @click="tipoParam='PF'" :checked="data.is_checked_pension_empleado">
                                                        <span></span>
                                                    </label>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="d-flex justify-center align-items-center">
                                                    <label :for="'labelCheckNamePE'+index" class="form-switch">
                                                        <input type="checkbox" :id="'labelCheckNamePE'+index"
                                                        @change="changeStatusItem(data.codigo,'sueldo')" @click="tipoParam='PE'" :checked="data.is_checked_pension_empresa">
                                                        <span></span>
                                                    </label>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="d-flex justify-center align-items-center">
                                                    <label :for="'labelCheckNameARL'+index" class="form-switch">
                                                        <input type="checkbox" :id="'labelCheckNameARL'+index"
                                                        @change="changeStatusItem(data.codigo,'sueldo');" @click="tipoParam='ARL'" :checked="data.is_checked_arl">
                                                        <span></span>
                                                    </label>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="d-flex justify-center align-items-center">
                                                    <label :for="'labelCheckNameCCF'+index" class="form-switch">
                                                        <input type="checkbox" :id="'labelCheckNameCCF'+index"
                                                        @change="changeStatusItem(data.codigo,'sueldo');" @click="tipoParam='CCF'" :checked="data.is_checked_ccf">
                                                        <span></span>
                                                    </label>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="d-flex justify-center align-items-center">
                                                    <label :for="'labelCheckNameICBF'+index" class="form-switch">
                                                        <input type="checkbox" :id="'labelCheckNameICBF'+index"
                                                        @change="changeStatusItem(data.codigo,'sueldo');" @click="tipoParam='ICBF'" :checked="data.is_checked_icbf">
                                                        <span></span>
                                                    </label>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="d-flex justify-center align-items-center">
                                                    <label :for="'labelCheckNameSENA'+index" class="form-switch">
                                                        <input type="checkbox" :id="'labelCheckNameSENA'+index"
                                                        @change="changeStatusItem(data.codigo,'sueldo');" @click="tipoParam='SENA'" :checked="data.is_checked_sena">
                                                        <span></span>
                                                    </label>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="d-flex justify-center align-items-center">
                                                    <label :for="'labelCheckNameINS'+index" class="form-switch">
                                                        <input type="checkbox" :id="'labelCheckNameINS'+index"
                                                        @change="changeStatusItem(data.codigo,'sueldo');" @click="tipoParam='INS'" :checked="data.is_checked_inst_tecnico">
                                                        <span></span>
                                                    </label>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="d-flex justify-center align-items-center">
                                                    <label :for="'labelCheckNameESAP'+index" class="form-switch">
                                                        <input type="checkbox" :id="'labelCheckNameESAP'+index"
                                                        @change="changeStatusItem(data.codigo,'sueldo');"  @click="tipoParam='ESAP'":checked="data.is_checked_esap">
                                                        <span></span>
                                                    </label>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="d-flex justify-center">
                                                    <button type="button" @click="delItem(index)" class="btn btn-danger">x</button>
                                                </div>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="nav-content">
                            <div class="d-flex justify-center">
                                <div class="form-control justify-between w-25">
                                    <label for=""></label>
                                    <button type="button" class="btn btn-primary" @click="isModalContrato=true;">Seleccionar contratos</button>
                                </div>
                            </div>
                            <div class="overflow-auto" style="height:40vh">
                                <table class="table fw-normal">
                                    <thead class="text-center">
                                        <tr>
                                            <th>Empleado</th>
                                            <th>CC/NIT</th>
                                            <th>Básico</th>
                                            <th>Días</th>
                                            <th>Vacaciones</th>
                                            <th>Incapacidades</th>
                                            <th>Dias laborados</th>
                                            <th>Auxilio de transporte</th>
                                            <th>
                                                <div class="d-flex">
                                                    <div class="form-control">
                                                        <label>Salud empleado</label>
                                                        <div class="d-flex justify-center align-items-center">
                                                            <label for="labelCheckNameSF" class="form-switch">
                                                                <input type="checkbox" id="labelCheckNameSF"
                                                                v-model="arrVariablesPago[indexOtros].is_checked_salud_empleado" checked @change="changeAll('otro');" @click="tipoParam='SF'">
                                                                <span></span>
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </th>
                                            <th>
                                                <div class="d-flex">
                                                    <div class="form-control">
                                                        <label>Salud empresa</label>
                                                        <div class="d-flex justify-center align-items-center">
                                                            <label for="labelCheckNameSE" class="form-switch">
                                                                <input type="checkbox" id="labelCheckNameSE"
                                                                v-model="arrVariablesPago[indexOtros].is_checked_salud_empresa" checked @change="changeAll('otro');" @click="tipoParam='SE'">
                                                                <span></span>
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </th>
                                            <th>
                                                <div class="d-flex">
                                                    <div class="form-control">
                                                        <label>Pension empleado</label>
                                                        <div class="d-flex justify-center align-items-center">
                                                            <label for="labelCheckNamePF" class="form-switch">
                                                                <input type="checkbox" id="labelCheckNamePF"
                                                                v-model="arrVariablesPago[indexOtros].is_checked_pension_empleado" checked @change="changeAll('otro');" @click="tipoParam='PF'">
                                                                <span></span>
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </th>
                                            <th>
                                                <div class="d-flex">
                                                    <div class="form-control">
                                                        <label>Pension empresa</label>
                                                        <div class="d-flex justify-center align-items-center">
                                                            <label for="labelCheckNamePE" class="form-switch">
                                                                <input type="checkbox" id="labelCheckNamePE"
                                                                v-model="arrVariablesPago[indexOtros].is_checked_pension_empresa" checked @change="changeAll('otro');" @click="tipoParam='PE'">
                                                                <span></span>
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </th>
                                            <th>
                                                <div class="d-flex">
                                                    <div class="form-control">
                                                        <label>ARL</label>
                                                        <div class="d-flex justify-center align-items-center">
                                                            <label for="labelCheckNameARL" class="form-switch">
                                                                <input type="checkbox" id="labelCheckNameARL"
                                                                v-model="arrVariablesPago[indexOtros].is_checked_arl" checked @change="changeAll('otro');" @click="tipoParam='ARL'">
                                                                <span></span>
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </th>
                                            <th>
                                                <div class="d-flex">
                                                    <div class="form-control">
                                                        <label>CCF</label>
                                                        <div class="d-flex justify-center align-items-center">
                                                            <label for="labelCheckNameCCF" class="form-switch">
                                                                <input type="checkbox" id="labelCheckNameCCF"
                                                                v-model="arrVariablesPago[indexOtros].is_checked_ccf" checked @change="changeAll('otro');" @click="tipoParam='CCF'">
                                                                <span></span>
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </th>
                                            <th>
                                                <div class="d-flex">
                                                    <div class="form-control">
                                                        <label>ICBF</label>
                                                        <div class="d-flex justify-center align-items-center">
                                                            <label for="labelCheckNameICBF" class="form-switch">
                                                                <input type="checkbox" id="labelCheckNameICBF"
                                                                v-model="arrVariablesPago[indexOtros].is_checked_icbf" checked @change="changeAll('otro');" @click="tipoParam='ICBF'">
                                                                <span></span>
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </th>
                                            <th>
                                                <div class="d-flex">
                                                    <div class="form-control">
                                                        <label>SENA</label>
                                                        <div class="d-flex justify-center align-items-center">
                                                            <label for="labelCheckNameSENA" class="form-switch">
                                                                <input type="checkbox" id="labelCheckNameSENA"
                                                                v-model="arrVariablesPago[indexOtros].is_checked_sena" checked @change="changeAll('otro');" @click="tipoParam='SENA'">
                                                                <span></span>
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </th>
                                            <th>
                                                <div class="d-flex">
                                                    <div class="form-control">
                                                        <label>Ins.tecnicos</label>
                                                        <div class="d-flex justify-center align-items-center">
                                                            <label for="labelCheckNameINS" class="form-switch">
                                                                <input type="checkbox" id="labelCheckNameINS"
                                                                v-model="arrVariablesPago[indexOtros].is_checked_inst_tecnico" checked @change="changeAll('otro');" @click="tipoParam='INS'">
                                                                <span></span>
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </th>
                                            <th>
                                                <div class="d-flex">
                                                    <div class="form-control">
                                                        <label>ESAP</label>
                                                        <div class="d-flex justify-center align-items-center">
                                                            <label for="labelCheckNameESAP" class="form-switch">
                                                                <input type="checkbox" id="labelCheckNameESAP"
                                                                v-model="arrVariablesPago[indexOtros].is_checked_esap" checked @change="changeAll('otro');" @click="tipoParam='ESAP'">
                                                                <span></span>
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </th>
                                            <th>
                                                <div class="d-flex justify-center">
                                                    <button type="button" @click="delItem()" class="btn btn-danger">x</button>
                                                </div>
                                            </th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr v-for="(data,index) in arrVariablesPago[indexOtros].detalle" :key="index">
                                            <td>{{data.nombre}}</td>
                                            <td>{{data.documento}}</td>
                                            <td>
                                                <div class="d-flex">
                                                    <div class="form-control">
                                                        <input type="text" class="text-right" v-model="data.salario_formateado" @keyup="updateValores">
                                                    </div>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="d-flex">
                                                    <div class="form-control">
                                                        <input type="text" class="text-center" v-model="data.dias" @keyup="updateValores">
                                                    </div>
                                                </div>
                                            </td>
                                            <td class="text-center">{{data.dias_vacaciones}}</td>
                                            <td class="text-center">{{data.dias_incapacidad}}</td>
                                            <td class="text-center">{{data.dias_laborados}}</td>
                                            <td class="text-right">{{formatMoney(data.transporte)}}</td>
                                            <td>
                                                <div class="d-flex justify-center align-items-center">
                                                    <label :for="'labelCheckNameSF'+index" class="form-switch">
                                                        <input type="checkbox" :id="'labelCheckNameSF'+index"
                                                        @change="changeStatusItem(data.codigo,'sueldo');"  @click="tipoParam='SF'" :checked="data.is_checked_salud_empleado">
                                                        <span></span>
                                                    </label>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="d-flex justify-center align-items-center">
                                                    <label :for="'labelCheckNameSE'+index" class="form-switch">
                                                        <input type="checkbox" :id="'labelCheckNameSE'+index"
                                                        @change="changeStatusItem(data.codigo,'sueldo');" @click="tipoParam='SE'" :checked="data.is_checked_salud_empresa">
                                                        <span></span>
                                                    </label>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="d-flex justify-center align-items-center">
                                                    <label :for="'labelCheckNamePF'+index" class="form-switch">
                                                        <input type="checkbox" :id="'labelCheckNamePF'+index"
                                                        @change="changeStatusItem(data.codigo,'sueldo');" @click="tipoParam='PF'" :checked="data.is_checked_pension_empleado">
                                                        <span></span>
                                                    </label>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="d-flex justify-center align-items-center">
                                                    <label :for="'labelCheckNamePE'+index" class="form-switch">
                                                        <input type="checkbox" :id="'labelCheckNamePE'+index"
                                                        @change="changeStatusItem(data.codigo,'sueldo')" @click="tipoParam='PE'" :checked="data.is_checked_pension_empresa">
                                                        <span></span>
                                                    </label>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="d-flex justify-center align-items-center">
                                                    <label :for="'labelCheckNameARL'+index" class="form-switch">
                                                        <input type="checkbox" :id="'labelCheckNameARL'+index"
                                                        @change="changeStatusItem(data.codigo,'sueldo');" @click="tipoParam='ARL'" :checked="data.is_checked_arl">
                                                        <span></span>
                                                    </label>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="d-flex justify-center align-items-center">
                                                    <label :for="'labelCheckNameCCF'+index" class="form-switch">
                                                        <input type="checkbox" :id="'labelCheckNameCCF'+index"
                                                        @change="changeStatusItem(data.codigo,'sueldo');" @click="tipoParam='CCF'" :checked="data.is_checked_ccf">
                                                        <span></span>
                                                    </label>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="d-flex justify-center align-items-center">
                                                    <label :for="'labelCheckNameICBF'+index" class="form-switch">
                                                        <input type="checkbox" :id="'labelCheckNameICBF'+index"
                                                        @change="changeStatusItem(data.codigo,'sueldo');" @click="tipoParam='ICBF'" :checked="data.is_checked_icbf">
                                                        <span></span>
                                                    </label>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="d-flex justify-center align-items-center">
                                                    <label :for="'labelCheckNameSENA'+index" class="form-switch">
                                                        <input type="checkbox" :id="'labelCheckNameSENA'+index"
                                                        @change="changeStatusItem(data.codigo,'sueldo');" @click="tipoParam='SENA'" :checked="data.is_checked_sena">
                                                        <span></span>
                                                    </label>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="d-flex justify-center align-items-center">
                                                    <label :for="'labelCheckNameINS'+index" class="form-switch">
                                                        <input type="checkbox" :id="'labelCheckNameINS'+index"
                                                        @change="changeStatusItem(data.codigo,'sueldo');" @click="tipoParam='INS'" :checked="data.is_checked_inst_tecnico">
                                                        <span></span>
                                                    </label>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="d-flex justify-center align-items-center">
                                                    <label :for="'labelCheckNameESAP'+index" class="form-switch">
                                                        <input type="checkbox" :id="'labelCheckNameESAP'+index"
                                                        @change="changeStatusItem(data.codigo,'sueldo');"  @click="tipoParam='ESAP'":checked="data.is_checked_esap">
                                                        <span></span>
                                                    </label>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="d-flex justify-center">
                                                    <button type="button" @click="delItem(index)" class="btn btn-danger">x</button>
                                                </div>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="nav-content">
                            <div class="d-flex justify-center">
                                <div class="form-control justify-between w-25">
                                    <label for=""></label>
                                    <button type="button" class="btn btn-primary" @click="isModalContrato=true;">Seleccionar contratos</button>
                                </div>
                            </div>
                            <div class="overflow-auto" style="height:40vh">
                                <table class="table fw-normal">
                                    <thead class="text-center">
                                        <tr>
                                            <th>Empleado</th>
                                            <th>CC/NIT</th>
                                            <th>Básico</th>
                                            <th>Días</th>
                                            <th>Vacaciones</th>
                                            <th>Incapacidades</th>
                                            <th>Dias laborados</th>
                                            <th>Subsidio de alimentación</th>
                                            <th>
                                                <div class="d-flex">
                                                    <div class="form-control">
                                                        <label>Salud empleado</label>
                                                        <div class="d-flex justify-center align-items-center">
                                                            <label for="labelCheckNameSF" class="form-switch">
                                                                <input type="checkbox" id="labelCheckNameSF"
                                                                v-model="arrVariablesPago[indexOtros].is_checked_salud_empleado" checked @change="changeAll('otro');" @click="tipoParam='SF'">
                                                                <span></span>
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </th>
                                            <th>
                                                <div class="d-flex">
                                                    <div class="form-control">
                                                        <label>Salud empresa</label>
                                                        <div class="d-flex justify-center align-items-center">
                                                            <label for="labelCheckNameSE" class="form-switch">
                                                                <input type="checkbox" id="labelCheckNameSE"
                                                                v-model="arrVariablesPago[indexOtros].is_checked_salud_empresa" checked @change="changeAll('otro');" @click="tipoParam='SE'">
                                                                <span></span>
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </th>
                                            <th>
                                                <div class="d-flex">
                                                    <div class="form-control">
                                                        <label>Pension empleado</label>
                                                        <div class="d-flex justify-center align-items-center">
                                                            <label for="labelCheckNamePF" class="form-switch">
                                                                <input type="checkbox" id="labelCheckNamePF"
                                                                v-model="arrVariablesPago[indexOtros].is_checked_pension_empleado" checked @change="changeAll('otro');" @click="tipoParam='PF'">
                                                                <span></span>
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </th>
                                            <th>
                                                <div class="d-flex">
                                                    <div class="form-control">
                                                        <label>Pension empresa</label>
                                                        <div class="d-flex justify-center align-items-center">
                                                            <label for="labelCheckNamePE" class="form-switch">
                                                                <input type="checkbox" id="labelCheckNamePE"
                                                                v-model="arrVariablesPago[indexOtros].is_checked_pension_empresa" checked @change="changeAll('otro');" @click="tipoParam='PE'">
                                                                <span></span>
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </th>
                                            <th>
                                                <div class="d-flex">
                                                    <div class="form-control">
                                                        <label>ARL</label>
                                                        <div class="d-flex justify-center align-items-center">
                                                            <label for="labelCheckNameARL" class="form-switch">
                                                                <input type="checkbox" id="labelCheckNameARL"
                                                                v-model="arrVariablesPago[indexOtros].is_checked_arl" checked @change="changeAll('otro');" @click="tipoParam='ARL'">
                                                                <span></span>
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </th>
                                            <th>
                                                <div class="d-flex">
                                                    <div class="form-control">
                                                        <label>CCF</label>
                                                        <div class="d-flex justify-center align-items-center">
                                                            <label for="labelCheckNameCCF" class="form-switch">
                                                                <input type="checkbox" id="labelCheckNameCCF"
                                                                v-model="arrVariablesPago[indexOtros].is_checked_ccf" checked @change="changeAll('otro');" @click="tipoParam='CCF'">
                                                                <span></span>
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </th>
                                            <th>
                                                <div class="d-flex">
                                                    <div class="form-control">
                                                        <label>ICBF</label>
                                                        <div class="d-flex justify-center align-items-center">
                                                            <label for="labelCheckNameICBF" class="form-switch">
                                                                <input type="checkbox" id="labelCheckNameICBF"
                                                                v-model="arrVariablesPago[indexOtros].is_checked_icbf" checked @change="changeAll('otro');" @click="tipoParam='ICBF'">
                                                                <span></span>
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </th>
                                            <th>
                                                <div class="d-flex">
                                                    <div class="form-control">
                                                        <label>SENA</label>
                                                        <div class="d-flex justify-center align-items-center">
                                                            <label for="labelCheckNameSENA" class="form-switch">
                                                                <input type="checkbox" id="labelCheckNameSENA"
                                                                v-model="arrVariablesPago[indexOtros].is_checked_sena" checked @change="changeAll('otro');" @click="tipoParam='SENA'">
                                                                <span></span>
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </th>
                                            <th>
                                                <div class="d-flex">
                                                    <div class="form-control">
                                                        <label>Ins.tecnicos</label>
                                                        <div class="d-flex justify-center align-items-center">
                                                            <label for="labelCheckNameINS" class="form-switch">
                                                                <input type="checkbox" id="labelCheckNameINS"
                                                                v-model="arrVariablesPago[indexOtros].is_checked_inst_tecnico" checked @change="changeAll('otro');" @click="tipoParam='INS'">
                                                                <span></span>
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </th>
                                            <th>
                                                <div class="d-flex">
                                                    <div class="form-control">
                                                        <label>ESAP</label>
                                                        <div class="d-flex justify-center align-items-center">
                                                            <label for="labelCheckNameESAP" class="form-switch">
                                                                <input type="checkbox" id="labelCheckNameESAP"
                                                                v-model="arrVariablesPago[indexOtros].is_checked_esap" checked @change="changeAll('otro');" @click="tipoParam='ESAP'">
                                                                <span></span>
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </th>
                                            <th>
                                                <div class="d-flex justify-center">
                                                    <button type="button" @click="delItem()" class="btn btn-danger">x</button>
                                                </div>
                                            </th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr v-for="(data,index) in arrVariablesPago[indexOtros].detalle" :key="index">
                                            <td>{{data.nombre}}</td>
                                            <td>{{data.documento}}</td>
                                            <td>
                                                <div class="d-flex">
                                                    <div class="form-control">
                                                        <input type="text" class="text-right" v-model="data.salario_formateado" @keyup="updateValores">
                                                    </div>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="d-flex">
                                                    <div class="form-control">
                                                        <input type="text" class="text-center" v-model="data.dias" @keyup="updateValores">
                                                    </div>
                                                </div>
                                            </td>
                                            <td class="text-center">{{data.dias_vacaciones}}</td>
                                            <td class="text-center">{{data.dias_incapacidad}}</td>
                                            <td class="text-center">{{data.dias_laborados}}</td>
                                            <td class="text-right">{{formatMoney(data.alimentacion)}}</td>
                                            <td>
                                                <div class="d-flex justify-center align-items-center">
                                                    <label :for="'labelCheckNameSF'+index" class="form-switch">
                                                        <input type="checkbox" :id="'labelCheckNameSF'+index"
                                                        @change="changeStatusItem(data.codigo,'sueldo');"  @click="tipoParam='SF'" :checked="data.is_checked_salud_empleado">
                                                        <span></span>
                                                    </label>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="d-flex justify-center align-items-center">
                                                    <label :for="'labelCheckNameSE'+index" class="form-switch">
                                                        <input type="checkbox" :id="'labelCheckNameSE'+index"
                                                        @change="changeStatusItem(data.codigo,'sueldo');" @click="tipoParam='SE'" :checked="data.is_checked_salud_empresa">
                                                        <span></span>
                                                    </label>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="d-flex justify-center align-items-center">
                                                    <label :for="'labelCheckNamePF'+index" class="form-switch">
                                                        <input type="checkbox" :id="'labelCheckNamePF'+index"
                                                        @change="changeStatusItem(data.codigo,'sueldo');" @click="tipoParam='PF'" :checked="data.is_checked_pension_empleado">
                                                        <span></span>
                                                    </label>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="d-flex justify-center align-items-center">
                                                    <label :for="'labelCheckNamePE'+index" class="form-switch">
                                                        <input type="checkbox" :id="'labelCheckNamePE'+index"
                                                        @change="changeStatusItem(data.codigo,'sueldo')" @click="tipoParam='PE'" :checked="data.is_checked_pension_empresa">
                                                        <span></span>
                                                    </label>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="d-flex justify-center align-items-center">
                                                    <label :for="'labelCheckNameARL'+index" class="form-switch">
                                                        <input type="checkbox" :id="'labelCheckNameARL'+index"
                                                        @change="changeStatusItem(data.codigo,'sueldo');" @click="tipoParam='ARL'" :checked="data.is_checked_arl">
                                                        <span></span>
                                                    </label>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="d-flex justify-center align-items-center">
                                                    <label :for="'labelCheckNameCCF'+index" class="form-switch">
                                                        <input type="checkbox" :id="'labelCheckNameCCF'+index"
                                                        @change="changeStatusItem(data.codigo,'sueldo');" @click="tipoParam='CCF'" :checked="data.is_checked_ccf">
                                                        <span></span>
                                                    </label>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="d-flex justify-center align-items-center">
                                                    <label :for="'labelCheckNameICBF'+index" class="form-switch">
                                                        <input type="checkbox" :id="'labelCheckNameICBF'+index"
                                                        @change="changeStatusItem(data.codigo,'sueldo');" @click="tipoParam='ICBF'" :checked="data.is_checked_icbf">
                                                        <span></span>
                                                    </label>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="d-flex justify-center align-items-center">
                                                    <label :for="'labelCheckNameSENA'+index" class="form-switch">
                                                        <input type="checkbox" :id="'labelCheckNameSENA'+index"
                                                        @change="changeStatusItem(data.codigo,'sueldo');" @click="tipoParam='SENA'" :checked="data.is_checked_sena">
                                                        <span></span>
                                                    </label>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="d-flex justify-center align-items-center">
                                                    <label :for="'labelCheckNameINS'+index" class="form-switch">
                                                        <input type="checkbox" :id="'labelCheckNameINS'+index"
                                                        @change="changeStatusItem(data.codigo,'sueldo');" @click="tipoParam='INS'" :checked="data.is_checked_inst_tecnico">
                                                        <span></span>
                                                    </label>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="d-flex justify-center align-items-center">
                                                    <label :for="'labelCheckNameESAP'+index" class="form-switch">
                                                        <input type="checkbox" :id="'labelCheckNameESAP'+index"
                                                        @change="changeStatusItem(data.codigo,'sueldo');"  @click="tipoParam='ESAP'":checked="data.is_checked_esap">
                                                        <span></span>
                                                    </label>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="d-flex justify-center">
                                                    <button type="button" @click="delItem(index)" class="btn btn-danger">x</button>
                                                </div>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="nav-content">
                            <div class="d-flex justify-center">
                                <div class="form-control justify-between w-25">
                                    <label for=""></label>
                                    <button type="button" class="btn btn-primary" @click="isModalContrato=true;">Seleccionar contratos</button>
                                </div>
                            </div>
                            <div class="d-flex justify-center">
                                <div class="form-control">
                                    <span class="text-primary fw-bold text-center">{{"El valor hora se calculará con base en " + txtHoraMensual+" horas mensuales"}}</span>
                                </div>
                            </div>
                            <div class="overflow-auto" style="height:40vh">
                                <table class="table fw-normal">
                                    <thead class="text-center">
                                        <tr >
                                            <th class="bg-primary text-white" rowspan="4">Empleado</th>
                                            <th class="bg-primary text-white" rowspan="4">CC/NIT</th>
                                            <th class="bg-primary text-white" rowspan="4">Básico</th>
                                            <th class="bg-primary text-white" rowspan="4">Valor hora</th>
                                            <th class="bg-primary text-white" colspan="8">Horas extras</th>
                                            <th class="bg-primary text-white" rowspan="4" class="text-wrap">Total extras</th>
                                            <th class="bg-primary text-white" colspan="8">Recargos</th>
                                            <th class="bg-primary text-white" rowspan="4" class="text-wrap">Total recargo</th>
                                            <th class="bg-primary text-white" rowspan="4">Total</th>
                                            <th class="bg-primary text-white" rowspan="4">
                                                <div class="d-flex justify-center">
                                                    <button type="button" @click="delItem()" class="btn btn-danger">x</button>
                                                </div>
                                            </th>
                                        </tr>
                                        <tr>
                                            <th colspan="4">Ordinaria</th>
                                            <th colspan="4">Dominical o festivo</th>
                                            <th colspan="2" rowspan="2">Nocturno</th>
                                            <th colspan="2" rowspan="2" class="text-wrap">Dominical o festivo diurno</th>
                                            <th colspan="2" rowspan="2" class="text-wrap">Dominical o festivo nocturno</th>
                                        </tr>
                                        <tr>
                                            <th colspan="2">Diurno</th>
                                            <th colspan="2">Nocturno</th>
                                            <th colspan="2">Diurno</th>
                                            <th colspan="2">Nocturno</th>
                                        </tr>
                                        <tr>
                                            <th>Horas</th>
                                            <th>Valor</th>
                                            <th>Horas</th>
                                            <th>Valor</th>
                                            <th>Horas</th>
                                            <th>Valor</th>
                                            <th>Horas</th>
                                            <th>Valor</th>
                                            <th>Horas</th>
                                            <th>Valor</th>
                                            <th>Horas</th>
                                            <th>Valor</th>
                                            <th>Horas</th>
                                            <th>Valor</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr v-for="(data,index) in arrVariablesPago[indexOtros].detalle" :key="index">
                                            <td>{{data.nombre}}</td>
                                            <td>{{data.documento}}</td>
                                            <td>
                                                <div class="d-flex">
                                                    <div class="form-control">
                                                        <input type="text" class="text-right" v-model="data.salario_formateado" @keyup="updateValores">
                                                    </div>
                                                </div>
                                            </td>
                                            <td class="text-right">{{data.valor_hora_formateado}}</td>
                                            <td>
                                                <div class="d-flex">
                                                    <div class="form-control">
                                                        <input type="number" class="text-center" v-model="data.ordinaria_diurna" @keyup="updateValores">
                                                    </div>
                                                </div>
                                            </td>
                                            <td class="text-right">{{data.valor_formateado_ordinaria_diurna}}</td>
                                            <td>
                                                <div class="d-flex">
                                                    <div class="form-control">
                                                        <input type="number" class="text-center" v-model="data.ordinaria_nocturna" @keyup="updateValores">
                                                    </div>
                                                </div>
                                            </td>
                                            <td class="text-right">{{data.valor_formateado_ordinaria_nocturna}}</td>
                                            <td>
                                                <div class="d-flex">
                                                    <div class="form-control">
                                                        <input type="number" class="text-center" v-model="data.dominical_diurna" @keyup="updateValores">
                                                    </div>
                                                </div>
                                            </td>
                                            <td class="text-right">{{data.valor_formateado_dominical_diurna}}</td>
                                            <td>
                                                <div class="d-flex">
                                                    <div class="form-control">
                                                        <input type="number" class="text-center" v-model="data.dominical_nocturna" @keyup="updateValores">
                                                    </div>
                                                </div>
                                            </td>
                                            <td class="text-right">{{data.valor_formateado_dominical_nocturna}}</td>
                                            <td class="text-right">{{data.total_extra_formateado}}</td>
                                            <td>
                                                <div class="d-flex">
                                                    <div class="form-control">
                                                        <input type="number" class="text-center" v-model="data.recargo_nocturno" @keyup="updateValores">
                                                    </div>
                                                </div>
                                            </td>
                                            <td class="text-right">{{data.valor_formateado_recargo_nocturno}}</td>
                                            <td>
                                                <div class="d-flex">
                                                    <div class="form-control">
                                                        <input type="number" class="text-center" v-model="data.recargo_dominical_diurno" @keyup="updateValores">
                                                    </div>
                                                </div>
                                            </td>
                                            <td class="text-right">{{data.valor_formateado_recargo_dominical_diurno}}</td>
                                            <td>
                                                <div class="d-flex">
                                                    <div class="form-control">
                                                        <input type="number" class="text-center" v-model="data.recargo_dominical_nocturno" @keyup="updateValores">
                                                    </div>
                                                </div>
                                            </td>
                                            <td class="text-right" colspan="2">{{data.valor_formateado_recargo_dominical_nocturno}}</td>
                                            <td class="text-right" colspan="2">{{data.total_recargo_formateado}}</td>
                                            <td class="text-right">{{data.total_formateado}}</td>
                                            <td>
                                                <div class="d-flex justify-center">
                                                    <button type="button" @click="delItem(index)" class="btn btn-danger">x</button>
                                                </div>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="nav-content">
                            <div class="d-flex justify-center ">
                                <div class="w-50 d-flex">
                                    <div class="form-control">
                                        <label class="form-label">Tipo de pago:</label>
                                        <select v-model="selectTipoPago" @change="changeTipoPago">
                                            <option :class="data.detalle.length > 0 ? 'bg-success text-white' : ''" v-if="data.codigo != '01' && data.codigo != '02' && data.codigo != '04' && data.codigo != '05'"
                                            v-for="(data,index) in arrVariablesPago" :value="data.codigo">{{data.codigo+" - "+data.nombre}}</option>
                                        </select>
                                    </div>
                                    <div class="form-control justify-between">
                                        <label for=""></label>
                                        <button type="button" class="btn btn-primary" @click="isModalContrato=true;">Seleccionar contratos</button>
                                    </div>
                                </div>
                            </div>
                            <div class="overflow-auto" style="height:40vh">
                                <table class="table fw-normal">
                                    <thead class="text-center">
                                        <tr>
                                            <th>Empleado</th>
                                            <th>CC/NIT</th>
                                            <th>Días</th>
                                            <th>Valor</th>
                                            <th>
                                                <div class="d-flex">
                                                    <div class="form-control">
                                                        <label>Salud empleado</label>
                                                        <div class="d-flex justify-center align-items-center">
                                                            <label for="labelCheckNameSF" class="form-switch">
                                                                <input type="checkbox" id="labelCheckNameSF"
                                                                v-model="arrVariablesPago[indexOtros].is_checked_salud_empleado" checked @change="changeAll('otro');" @click="tipoParam='SF'">
                                                                <span></span>
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </th>
                                            <th>
                                                <div class="d-flex">
                                                    <div class="form-control">
                                                        <label>Salud empresa</label>
                                                        <div class="d-flex justify-center align-items-center">
                                                            <label for="labelCheckNameSE" class="form-switch">
                                                                <input type="checkbox" id="labelCheckNameSE"
                                                                v-model="arrVariablesPago[indexOtros].is_checked_salud_empresa" checked @change="changeAll('otro');" @click="tipoParam='SE'">
                                                                <span></span>
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </th>
                                            <th>
                                                <div class="d-flex">
                                                    <div class="form-control">
                                                        <label>Pension empleado</label>
                                                        <div class="d-flex justify-center align-items-center">
                                                            <label for="labelCheckNamePF" class="form-switch">
                                                                <input type="checkbox" id="labelCheckNamePF"
                                                                v-model="arrVariablesPago[indexOtros].is_checked_pension_empleado" checked @change="changeAll('otro');" @click="tipoParam='PF'">
                                                                <span></span>
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </th>
                                            <th>
                                                <div class="d-flex">
                                                    <div class="form-control">
                                                        <label>Pension empresa</label>
                                                        <div class="d-flex justify-center align-items-center">
                                                            <label for="labelCheckNamePE" class="form-switch">
                                                                <input type="checkbox" id="labelCheckNamePE"
                                                                v-model="arrVariablesPago[indexOtros].is_checked_pension_empresa" checked @change="changeAll('otro');" @click="tipoParam='PE'">
                                                                <span></span>
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </th>
                                            <th>
                                                <div class="d-flex">
                                                    <div class="form-control">
                                                        <label>ARL</label>
                                                        <div class="d-flex justify-center align-items-center">
                                                            <label for="labelCheckNameARL" class="form-switch">
                                                                <input type="checkbox" id="labelCheckNameARL"
                                                                v-model="arrVariablesPago[indexOtros].is_checked_arl" checked @change="changeAll('otro');" @click="tipoParam='ARL'">
                                                                <span></span>
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </th>
                                            <th>
                                                <div class="d-flex">
                                                    <div class="form-control">
                                                        <label>CCF</label>
                                                        <div class="d-flex justify-center align-items-center">
                                                            <label for="labelCheckNameCCF" class="form-switch">
                                                                <input type="checkbox" id="labelCheckNameCCF"
                                                                v-model="arrVariablesPago[indexOtros].is_checked_ccf" checked @change="changeAll('otro');" @click="tipoParam='CCF'">
                                                                <span></span>
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </th>
                                            <th>
                                                <div class="d-flex">
                                                    <div class="form-control">
                                                        <label>ICBF</label>
                                                        <div class="d-flex justify-center align-items-center">
                                                            <label for="labelCheckNameICBF" class="form-switch">
                                                                <input type="checkbox" id="labelCheckNameICBF"
                                                                v-model="arrVariablesPago[indexOtros].is_checked_icbf" checked @change="changeAll('otro');" @click="tipoParam='ICBF'">
                                                                <span></span>
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </th>
                                            <th>
                                                <div class="d-flex">
                                                    <div class="form-control">
                                                        <label>SENA</label>
                                                        <div class="d-flex justify-center align-items-center">
                                                            <label for="labelCheckNameSENA" class="form-switch">
                                                                <input type="checkbox" id="labelCheckNameSENA"
                                                                v-model="arrVariablesPago[indexOtros].is_checked_sena" checked @change="changeAll('otro');" @click="tipoParam='SENA'">
                                                                <span></span>
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </th>
                                            <th>
                                                <div class="d-flex">
                                                    <div class="form-control">
                                                        <label>Ins.tecnicos</label>
                                                        <div class="d-flex justify-center align-items-center">
                                                            <label for="labelCheckNameINS" class="form-switch">
                                                                <input type="checkbox" id="labelCheckNameINS"
                                                                v-model="arrVariablesPago[indexOtros].is_checked_inst_tecnico" checked @change="changeAll('otro');" @click="tipoParam='INS'">
                                                                <span></span>
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </th>
                                            <th>
                                                <div class="d-flex">
                                                    <div class="form-control">
                                                        <label>ESAP</label>
                                                        <div class="d-flex justify-center align-items-center">
                                                            <label for="labelCheckNameESAP" class="form-switch">
                                                                <input type="checkbox" id="labelCheckNameESAP"
                                                                v-model="arrVariablesPago[indexOtros].is_checked_esap" checked @change="changeAll('otro');" @click="tipoParam='ESAP'">
                                                                <span></span>
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </th>
                                            <th>
                                                <div class="d-flex justify-center">
                                                    <button type="button" @click="delItem()" class="btn btn-danger">x</button>
                                                </div>
                                            </th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr v-for="(data,index) in arrVariablesPago[indexOtros].detalle" :key="index">
                                            <td>{{data.nombre}}</td>
                                            <td>{{data.documento}}</td>
                                            <td>
                                                <div class="d-flex">
                                                    <div class="form-control">
                                                        <input type="text" class="text-center" v-model="data.dias" @keyup="updateValores">
                                                    </div>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="d-flex">
                                                    <div class="form-control">
                                                        <input type="text" class="text-right" v-model="data.valor_otros_formateado" @keyup="updateValores">
                                                    </div>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="d-flex justify-center align-items-center">
                                                    <label :for="'labelCheckNameSF'+index" class="form-switch">
                                                        <input type="checkbox" :id="'labelCheckNameSF'+index"
                                                        @change="changeStatusItem(data.codigo,'sueldo');"  @click="tipoParam='SF'" :checked="data.is_checked_salud_empleado">
                                                        <span></span>
                                                    </label>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="d-flex justify-center align-items-center">
                                                    <label :for="'labelCheckNameSE'+index" class="form-switch">
                                                        <input type="checkbox" :id="'labelCheckNameSE'+index"
                                                        @change="changeStatusItem(data.codigo,'sueldo');" @click="tipoParam='SE'" :checked="data.is_checked_salud_empresa">
                                                        <span></span>
                                                    </label>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="d-flex justify-center align-items-center">
                                                    <label :for="'labelCheckNamePF'+index" class="form-switch">
                                                        <input type="checkbox" :id="'labelCheckNamePF'+index"
                                                        @change="changeStatusItem(data.codigo,'sueldo');" @click="tipoParam='PF'" :checked="data.is_checked_pension_empleado">
                                                        <span></span>
                                                    </label>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="d-flex justify-center align-items-center">
                                                    <label :for="'labelCheckNamePE'+index" class="form-switch">
                                                        <input type="checkbox" :id="'labelCheckNamePE'+index"
                                                        @change="changeStatusItem(data.codigo,'sueldo')" @click="tipoParam='PE'" :checked="data.is_checked_pension_empresa">
                                                        <span></span>
                                                    </label>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="d-flex justify-center align-items-center">
                                                    <label :for="'labelCheckNameARL'+index" class="form-switch">
                                                        <input type="checkbox" :id="'labelCheckNameARL'+index"
                                                        @change="changeStatusItem(data.codigo,'sueldo');" @click="tipoParam='ARL'" :checked="data.is_checked_arl">
                                                        <span></span>
                                                    </label>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="d-flex justify-center align-items-center">
                                                    <label :for="'labelCheckNameCCF'+index" class="form-switch">
                                                        <input type="checkbox" :id="'labelCheckNameCCF'+index"
                                                        @change="changeStatusItem(data.codigo,'sueldo');" @click="tipoParam='CCF'" :checked="data.is_checked_ccf">
                                                        <span></span>
                                                    </label>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="d-flex justify-center align-items-center">
                                                    <label :for="'labelCheckNameICBF'+index" class="form-switch">
                                                        <input type="checkbox" :id="'labelCheckNameICBF'+index"
                                                        @change="changeStatusItem(data.codigo,'sueldo');" @click="tipoParam='ICBF'" :checked="data.is_checked_icbf">
                                                        <span></span>
                                                    </label>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="d-flex justify-center align-items-center">
                                                    <label :for="'labelCheckNameSENA'+index" class="form-switch">
                                                        <input type="checkbox" :id="'labelCheckNameSENA'+index"
                                                        @change="changeStatusItem(data.codigo,'sueldo');" @click="tipoParam='SENA'" :checked="data.is_checked_sena">
                                                        <span></span>
                                                    </label>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="d-flex justify-center align-items-center">
                                                    <label :for="'labelCheckNameINS'+index" class="form-switch">
                                                        <input type="checkbox" :id="'labelCheckNameINS'+index"
                                                        @change="changeStatusItem(data.codigo,'sueldo');" @click="tipoParam='INS'" :checked="data.is_checked_inst_tecnico">
                                                        <span></span>
                                                    </label>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="d-flex justify-center align-items-center">
                                                    <label :for="'labelCheckNameESAP'+index" class="form-switch">
                                                        <input type="checkbox" :id="'labelCheckNameESAP'+index"
                                                        @change="changeStatusItem(data.codigo,'sueldo');"  @click="tipoParam='ESAP'":checked="data.is_checked_esap">
                                                        <span></span>
                                                    </label>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="d-flex justify-center">
                                                    <button type="button" @click="delItem(index)" class="btn btn-danger">x</button>
                                                </div>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!-- MODALES-->
            <div v-show="isModalContrato" class="modal">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title">Buscar contratos</h5>
                            <button type="button" @click="isModalContrato=false;" class="btn btn-close"><div></div><div></div></button>
                        </div>
                        <div class="modal-body">
                            <div class="d-flex flex-column">
                                <div class="form-control m-0 mb-3">
                                    <input type="search" placeholder="Buscar" v-model="txtSearchContrato" @keyup="search('modal_contrato')" id="labelInputName">
                                </div>
                                <div class="form-control m-0 mb-3">
                                    <label class="form-label" for="labelInputName">Resultados: <span class="fw-bold">{{txtResultadosContrato}}</span></label>
                                </div>
                            </div>
                            <div class="overflow-auto max-vh-50 overflow-x-hidden " >
                                <table class="table table-hover fw-normal">
                                    <thead>
                                        <tr>
                                            <th>Código</th>
                                            <th>CC/NIT</th>
                                            <th>Nombre</th>
                                            <th>
                                                <div class="d-flex justify-center align-items-center">
                                                    <label for="labelCheckAll" class="form-switch">
                                                        <input type="checkbox" id="labelCheckAll" v-model="isCheckAll" checked @change="changeAll()">
                                                        <span></span>
                                                    </label>
                                                </div>
                                            </th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr v-for="(data,index) in arrContratosCopy" :key="index">
                                            <td>{{data.codigo}}</td>
                                            <td>{{data.tercero}}</td>
                                            <td>{{data.nombre}}</td>
                                            <td>
                                                <div class="d-flex justify-center align-items-center">
                                                    <label :for="'labelCheckName'+index" class="form-switch">
                                                        <input type="checkbox" :id="'labelCheckName'+index" @change="changeStatusItem(data.codigo)" :checked="data.is_checked">
                                                        <span></span>
                                                    </label>
                                                </div>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div class="d-flex justify-end mt-3 mb-3">
                                <button type="button" @click="generarEmpleados()" class="btn btn-primary">Agregar</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </main>
		<script src="Librerias/vue/vue.min.js"></script>
		<script type="module" src="gestion_humana/nomina/js/functions_novedades.js?<?= date('d_m_Y_h_i_s');?>"></script>
	</body>
</html>
