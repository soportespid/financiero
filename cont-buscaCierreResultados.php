<?php
	header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=utf8");
	require 'comun.inc';
	require 'funciones.inc';
	session_start();
	$linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");
	date_default_timezone_set("America/Bogota");
	titlepag();
?>
<!DOCTYPE >
<html lang=es>
	<head>
		<meta charset="utf-8"/>
		<meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1"/>
		<meta name="viewport" content="user-scalable=no">
		<title>:: IDEAL 10 - Contabilidad</title>
		<link href="css/css2z.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
		<link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
		<link href="css/css4.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
		<script src="vue/vue.min.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script src="css/programas.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script src="css/calendario.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<style>
			input[type=number]::-webkit-inner-spin-button,
			input[type=number]::-webkit-outer-spin-button { 
				-webkit-appearance: none; 
				margin: 0; 
			}
			input[type=number]{
				text-align:right;
				box-sizing: border-box;
				font: 120% sans-serif;
				width: 100% !important;
			}

			[v-cloak]{
                display : none;
            }
		</style>
		<script src="sweetalert2/dist/sweetalert2.min.js"></script>
		<link rel="stylesheet" href="sweetalert2/dist/sweetalert2.min.css">
	</head>
	<body>
		<header>
			<table>
				<tr><script>barra_imagenes("cont");</script><?php cuadro_titulos();?></tr>
			</table>
		</header>
		<section id="myapp" v-cloak >
			<nav>
				<table>
					<tr><?php menu_desplegable("cont");?></tr>
					<tr>
						<td colspan="3" class="cinta">
							<img src="imagenes/add.png" onClick="location.href='cont-cierreresultados.php'" class="mgbt" title="Nuevo">
							<img src="imagenes/guardad.png" title="Guardar"  class="mgbt1">
							<img src="imagenes/buscad.png" class="mgbt1" title="Buscar">
							<img src="imagenes/nv.png" onClick="mypop=window.open('cont-principal.php','',''); mypop.focus();" class="mgbt" title="Nueva Ventana">
						</td>
					</tr>
				</table>
			</nav>
			<article>
				<table class="inicio ancho">
					<tr>
						<td class="titulos" colspan="8" >Cierre de resultados</td>
						<td class="cerrar" style="width:7%" onClick="location.href='cont-principal.php'">Cerrar</td>
					</tr>
					<tr>
						<td class="tamano01" style="width:2.5cm;">Fecha Inicial:</td>
						<td style="width:10%;"><input type="text" name="fecha"  value="<?php echo $_POST['fecha']?>" onKeyUp="return tabular(event,this)" id="fc_1198971545" title="DD/MM/YYYY" onDblClick="displayCalendarFor('fc_1198971545');" class="colordobleclik" autocomplete="off" onChange="" readonly></td>
						<td class="tamano01" style="width:2.5cm;" >Fecha Final:</td>
						<td style="width:10%;"><input type="text" name="fecha2" value="<?php echo $_POST['fecha2']?>" onKeyUp="return tabular(event,this)" id="fc_1198971546" title="DD/MM/YYYY" onDblClick="displayCalendarFor('fc_1198971546');" class="colordobleclik" autocomplete="off" onChange="" readonly></td>
						<td colspan="2" style="padding-bottom:0px"><em class="botonflechaverde" v-on:Click="buscarCierreResultados();">Buscar</em></td>
						<td></td>
					</tr>
				</table>
				<div class='subpantalla' style='height:66vh; width:99.2%; margin-top:0px; overflow:hidden'>
					<table class='tablamv'>
						<thead>
							<tr style="text-align:Center;">
								<th class="titulosnew00" style="width:6%;">Consecutivo</th>
								<th class="titulosnew00" style="width:10%;">Fecha</th>
								<th class="titulosnew00" >Descripción</th>
								<th class="titulosnew00" style="width:6%;">Estado</th>
								<th class="titulosnew01" style="width:6%;">Deshacer</th>
							</tr>
						</thead>
						<tbody>
							<tr v-show="!existeInformacion">
                                <td colspan="7">
                                    <div style="text-align: center; color:turquoise; font-size:large" class="h4 text-primary text-center">
                                        Utilice los filtros para buscar informaci&oacute;n.
                                    </div>
                                </td>
                            </tr>
							<tr v-for="(detalle, index) in detalles" v-bind:class="index % 2 ? 'contenidonew00' : 'contenidonew01'" style='text-rendering: optimizeLegibility; cursor: pointer !important;'>
                                <td  style="width:6%; font: 120% sans-serif; padding-left:5px; text-align:center;">{{ detalle[0] }}</td>
                                <td style="width:10%;font: 120% sans-serif; padding-left:5px; text-align:center;">{{ detalle[1] }}</td>
                                <td style="font: 120% sans-serif; padding-left:5px;">{{ detalle[2] }}</td>
                                <td style="width:6%; font: 120% sans-serif; padding-left:5px;text-align:center;">{{ comprobarEstado(detalle[3]) }}</td>
								<td style="width:6%; font: 120% sans-serif; padding-left:5px;text-align:center;">
									<img v-show="index == 0" src='imagenes/flechades.png' title='Deshacer' v-on:Click='deshacer(detalle[0],detalle[1]);' style='width:20px'/>
								</td>
                            </tr>
                        </tbody>
					</table>
				</div>
				<div id="cargando" v-if="loading" class="loading">
					<span>Cargando...</span>
				</div>
			</article>
		</section>

        <script type="module" src="./contabilidad_vue/cierreResultado/buscar/cont-buscaCierreResultados.js"></script>
		<script src="Librerias/vue/axios.min.js"></script>
		<script src="https://unpkg.com/vue@3/dist/vue.global.js"></script>
	</body>
</html>