<?php
    header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=utf8");

	require 'comun.inc';
	require 'funciones.inc';

	session_start();
    date_default_timezone_set("America/Bogota");

?>
<!DOCTYPE >
<html lang="es">
	<head>
		<meta charset="utf-8"/>
		<meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1"/>
		<meta name="viewport" content="user-scalable=no">
		<title>:: IDEAL 10 - Almacen</title>
		<link href="favicon.ico" rel="shortcut icon"/>

		<link href="css/css2.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<link href="css/css2z.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
        <link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
        <link href="css/css4.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
        <script type="text/javascript" src="css/programas.js"></script>
        <script type="text/javascript" src="css/calendario.js"></script>
        <script src="sweetalert2/dist/sweetalert2.min.js"></script>
		<link rel="stylesheet" href="sweetalert2/dist/sweetalert2.min.css">

		<style>
			.inicio--no-shadow{
				box-shadow: none;
			}

			[v-cloak]{
				display : none;
			}
		</style>
    </head>
    <body>
        <header>
			<table>
				<tr><script>barra_imagenes("inve");</script><?php cuadro_titulos();?></tr>
			</table>
		</header>

        <section id="myapp" v-cloak>
            <form name="form2" method="post" action="">
                <nav>
					<table>
						<tr><?php menu_desplegable("inve");?></tr>
					</table>
                    <div class="bg-white group-btn p-1">
                        <button type="button" v-on:click="location.href='inve-salidaActivosFijos.php'" class="btn btn-white btn-primary-hover d-flex justify-between align-items-center">
                            <span>Nuevo</span>
                            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 -960 960 960">
                                <path d="M440-440H200v-80h240v-240h80v240h240v80H520v240h-80v-240Z"></path>
                            </svg>
                        </button>
                        <button type="button" class="btn btn-white btn-primary-hover d-flex justify-between align-items-center">
                            <span>Guardar</span>
                            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 -960 960 960">
                                <path
                                    d="M840-680v480q0 33-23.5 56.5T760-120H200q-33 0-56.5-23.5T120-200v-560q0-33 23.5-56.5T200-840h480l160 160Zm-80 34L646-760H200v560h560v-446ZM480-240q50 0 85-35t35-85q0-50-35-85t-85-35q-50 0-85 35t-35 85q0 50 35 85t85 35ZM240-560h360v-160H240v160Zm-40-86v446-560 114Z">
                                </path>
                            </svg>
                        </button>
                        <button type="button" v-on:click="location.href='inve-buscarSalidaActivosFijos'" class="btn btn-white btn-primary-hover d-flex justify-between align-items-center">
                            <span>Buscar</span>
                            <svg class="fill-black group-hover:fill-white w-5 h-5" xmlns="http://www.w3.org/2000/svg" viewBox="0 -960 960 960">
                                <path d="M784-120 532-372q-30 24-69 38t-83 14q-109 0-184.5-75.5T120-580q0-109 75.5-184.5T380-840q109 0 184.5 75.5T640-580q0 44-14 83t-38 69l252 252-56 56ZM380-400q75 0 127.5-52.5T560-580q0-75-52.5-127.5T380-760q-75 0-127.5 52.5T200-580q0 75 52.5 127.5T380-400Z">
                                </path>
                            </svg>
                        </button>
                        <button type="button" @click="mypop=window.open('inve-principal','',''); mypop.focus();" class="btn btn-white btn-primary-hover d-flex justify-between align-items-center">
                            <span>Nueva ventana</span>
                            <svg class="fill-black group-hover:fill-white w-5 h-5" xmlns="http://www.w3.org/2000/svg"
                                viewBox="0 -960 960 960">
                                <path
                                    d="M320-120v-80h80v-80H160q-33 0-56.5-23.5T80-360v-400q0-33 23.5-56.5T160-840h640q33 0 56.5 23.5T880-760v400q0 33-23.5 56.5T800-280H560v80h80v80H320ZM160-360h640v-400H160v400Zm0 0v-400 400Z">
                                </path>
                            </svg>
                        </button>
                        <button type="button" onclick="location.href='inve-menuSalidas'" class="btn btn-success btn-success-hover d-flex justify-between align-items-center">
                            <span>Atras</span>
                            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 -960 960 960">
                                <path
                                    d="M760-200v-160q0-50-35-85t-85-35H273l144 144-57 56-240-240 240-240 57 56-144 144h367q83 0 141.5 58.5T840-360v160h-80Z">
                                </path>
                            </svg>
                        </button>
                    </div>
				</nav>

                <article>
                    <table class="inicio">
                        <tr>
                            <td class="titulos" colspan="10" >Salidas por traslado:</td>
                            <td class="cerrar" style="width:7%" onClick="location.href='inve-principal.php'">Cerrar</td>
                        </tr>

                        <tr>
                            <td class="textonew01" style="width:3.5cm;">.: Consecutivo:</td>
                            <td>
                                <input type="text" v-model="consecutivo">
                            </td>

                            <td class="textonew01" style="width:3.5cm;">.: Fecha inicial:</td>
                            <td>
                                <input type="text" name="fechaIni"  value="<?php echo $_POST['fechaIni']?>" onKeyUp="return tabular(event,this)" id="fechaIni" title="DD/MM/YYYY" onDblClick="displayCalendarFor('fechaIni');" class="colordobleclik" autocomplete="off" onChange=""  readonly>
                            </td>

                            <td class="textonew01" style="width:3.5cm;">.: Fecha final:</td>
                            <td>
                                <input type="text" name="fechaFin" value="<?php echo $_POST['fechaFin']?>" onKeyUp="return tabular(event,this)" id="fechaFin" title="DD/MM/YYYY" onDblClick="displayCalendarFor('fechaFin');" class="colordobleclik" autocomplete="off" onChange=""  readonly>
                            </td>

                            <td>
                                <input type="button" class="btn btn-primary" value="Buscar traslados" @click="buscarDatos">
                            </td>
                        </tr>
                    </table>

                    <div class='subpantalla' style='height:54vh; width:100%; margin-top:0px;'>
                        <table class=''>
                            <thead>
                                <tr>
                                    <th class="titulosnew00" width="5%">Consecutivo</th>
                                    <th class="titulosnew00" width="7%">Fecha</th>
                                    <th class="titulosnew00">Descripción</th>
                                    <th class="titulosnew00" width="10%">Bodega</th>
                                    <th class="titulosnew00" width="10%">Realiza</th>
                                    <th class="titulosnew00" width="10%">Valor</th>
                                    <th class="titulosnew00" width="5%">Estado</th>
                                </tr>
                            </thead>

                            <tbody>
                                <tr v-for="(salida,index) in salidas" v-on:dblclick="visualizar(salida)" v-bind:class="(index % 2 ? 'saludo1a' : 'saludo2')"  style="font: 100% sans-serif;" >
                                    <td style="width: 5%; text-align:center;">{{ salida[0] }}</td>
                                    <td style="width: 7%; text-align:center;">{{ salida[1] }}</td>
                                    <td>{{ salida[2] }}</td>
                                    <td style="width: 10%; text-align:center;">{{ salida[3] }}</td>
                                    <td style="width: 10%; text-align:center;">{{ salida[4] }}</td>
                                    <td style="width: 10%; text-align:center;">{{ formatonumero(salida[5]) }}</td>
                                    <td style="width: 5%; text-align:center;">{{ salida[6] }}</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </article>
            </form>

            <div id="cargando" v-if="loading" class="loading">
                <span>Cargando...</span>
            </div>

        </section>

        <script src="node_modules/read-excel-file/bundle/read-excel-file.min.js"></script>
		<script src="node_modules/xlsx/dist/xlsx.full.min.js"></script>
        <script src="Librerias/vue/vue.min.js"></script>
		<script src="Librerias/vue/axios.min.js"></script>
		<script type="module" src="almacen/salidaTraslado/buscar/inve-buscarSalidaActivosFijos.js"></script>
	</body>
</html>
