<?php
	header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=utf8");

	require "comun.inc";
	require "funciones.inc";
	require "conversor.php";
	require "validaciones.inc";

	$linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");

	session_start();
	date_default_timezone_set("America/Bogota");
?>
<!DOCTYPE >
<html lang="es">
	<head>
		<meta charset="utf-8"/>
		<meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1"/>
		<meta name="viewport" content="user-scalable=no">
		<title>:: IDEAL 10 - Contabilidad</title>
		<link href="favicon.ico" rel="shortcut icon"/>
        <link href="css/css2.css" rel="stylesheet" type="text/css" />
        <link href="css/css3.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="css/programas.js"></script>
        <script type="text/javascript" src="css/calendario.js"></script>
		<script>
			//************* ver reporte ************
			//***************************************
			function verep(idfac)
			{
				document.form1.oculto.value=idfac;
				document.form1.submit();
			}
            //************* genera reporte ************
            //***************************************
            function genrep(idfac)
            {
                document.form2.oculto.value=idfac;
                document.form2.submit();
            }

            function buscacta(e)
            {
                if (document.form2.cuenta.value!="")
                {
                    document.form2.bc.value='1';
                    document.form2.submit();
                }
            }

            function validar()
            {
                document.form2.submit();
            }

            function buscater(e)
            {
                if (document.form2.tercero.value!="")
                {
                    document.form2.bt.value='1';
                    document.form2.submit();
                }
            }

            function agregardetalle()
            {
                if(document.form2.numero.value!="" &&  document.form2.valor.value>0 &&  document.form2.banco.value!=""  )
                {
                    document.form2.agregadet.value=1;
                    document.form2.submit();
                }
                else {
                    alert("Falta informacion para poder Agregar");
                }
            }

            //************* genera reporte ************
            //***************************************
            function eliminar(idr)
            {
                if (confirm("Esta Seguro de Eliminar El Egreso No "+idr))
                {
                document.form2.oculto.value=2;
                document.form2.var1.value=idr;
                document.form2.submit();
                }
            }

            //************* genera reporte ************
            //***************************************
            function guardar()
            {

                if (document.form2.fecha.value!='')
                {
                    if (confirm("Esta Seguro de Guardar"))
                    {
                    document.form2.oculto.value=2;
                    document.form2.submit();
                    }
                }
                else{
                    alert('Faltan datos para completar el registro');
                    document.form2.fecha.focus();
                    document.form2.fecha.select();
                }
            }

            function buscatercero(e)
            {
                if (document.form2.tercero.value!="")
                {
                    document.form2.bc.value='1';
                    document.form2.submit();
                }
            }

            function pdf()
            {
                document.form2.action="certificadospdf";
                document.form2.target="_BLANK";
                document.form2.submit();
                document.form2.action="";
                document.form2.target="";
            }

        </script>
    </head>
    <body>
        <IFRAME src="alertas.php" name="alertas" id="alertas" style="display:none"></IFRAME>
        <span id="todastablas2"></span>
        <table>
            <tr><script>barra_imagenes("teso");</script><?php cuadro_titulos();?></tr>
            <tr><?php menu_desplegable("teso");?></tr>
            <tr>
                <td colspan="3" class="cinta">
                    <a href="#" class="mgbt"><img src="imagenes/add2.png"/></a>
                    <a class="mgbt"><img src="imagenes/guardad.png" /></a>
                    <a class="mgbt" onClick="document.form2.submit();" href="#"><img src="imagenes/busca.png" title="Buscar" /></a>
                    <a onClick="mypop=window.open('plan-agenda.php','','');mypop.focus()" class="mgbt"><img src="imagenes/agenda1.png" title="Agenda" /></a>
                    <a href="#" class="mgbt" onClick="mypop=window.open('teso-principal','','');mypop.focus();"><img src="imagenes/nv.png" title="Nueva Ventana"></a>
                    <a href="#" class="mgbt" onClick="pdf()"><img src="imagenes/print.png" title="imprimir"></a>
                    <a href="<?php echo "archivos/".$_SESSION['usuario']."-reporteegresos.csv"; ?>" target="_blank" class="mgbt"><img src="imagenes/csv.png" title="csv"></a>
                </td>
            </tr>
        </table>
        <form name="form2" method="post" action="teso-certificados.php">
            <?php
            $vigusu=vigencia_usuarios($_SESSION['cedulausu']);
            if($_POST['bc']=='1')
            {
                $nresul=buscatercero($_POST['tercero']);
                if($nresul!='')
                {

                    $_POST['ntercero']=$nresul;

                }
                else
                {
                    $_POST['ntercero']="";
                }
            }

            ?>
            <table  class="inicio" align="center" >

                <tr>
                    <td class="titulos" colspan="12">:. Certificados</td>
                    <td  class="cerrar" ><a href="teso-principal.php">Cerrar</a></td>
                </tr>
                <tr>
                    <td  class="saludo1" style="width: 5%">Tercero:</td>
                    <td valign="middle" style="width: 10%">
                        <input type="text" id="tercero" name="tercero" style="width: 80%" onKeyPress="javascript:return solonumeros(event)"
		                onKeyUp="return tabular(event,this)" onBlur="buscatercero(event)" value="<?php echo $_POST['tercero']?>" onClick="document.getElementById('tercero').focus();document.getElementById('tercero').select();"><input type="hidden" value="0" name="bc"><a href="#" onClick="mypop=window.open('terceros-ventana.php','','menubar=0,scrollbars=yes, toolbar=no, location=no, width=700px,height=500px');mypop.focus();"><img src="imagenes/buscarep.png" align="absmiddle" border="0"></a>
                    </td>
                    <td><input name="ntercero" style="width: 90%;" type="text" id="ntercero" value="<?php echo $_POST['ntercero']?>" readonly>
                    </td>
                    <td class="saludo1" style="width: 5%;">Tipo Ret:</td>
                    <td style="width: 10%;">
                        <select name="tiporet" id="tiporet"  style="width: 100%;">
                            <option value="" >Seleccione...</option>
                            <option value="N" <?php if($_POST['tiporet']=='N') echo "SELECTED"?>>Nacional</option>
                            <option value="D" <?php if($_POST['tiporet']=='D') echo "SELECTED"?>>Departamental</option>
                            <option value="M" <?php if($_POST['tiporet']=='M') echo "SELECTED"?>>Municipal</option>
			            </select>
                    </td>
                    <td class="tamano01" style="width: 5%;">:&middot; Fecha Inicial:</td>
                    <td style="width:10%;">
                        <input type="text" name="fecha"  value="<?php echo $_POST['fecha']?>" onKeyUp="return tabular(event,this)" id="fc_1198971545" title="DD/MM/YYYY" onDblClick="displayCalendarFor('fc_1198971545');" class="colordobleclik" autocomplete="off" onChange="" style="width:100%; height: 30px !important;" readonly>
                    </td>
                    <td class="tamano01" style="width: 5%;" >:&middot; Fecha Final:</td>
                    <td style="width:10%;">
                        <input type="text" name="fecha2" value="<?php echo $_POST['fecha2']?>" onKeyUp="return tabular(event,this)" id="fc_1198971546" title="DD/MM/YYYY" onDblClick="displayCalendarFor('fc_1198971546');" class="colordobleclik" autocomplete="off" onChange="" style="width:100%; height: 30px !important;" readonly>
                    </td>

                    <!-- <td  class="saludo1">Vigencia:</td>
                    <td>
                        <select name="vigencias" id="vigencias">
                            <option value="">Sel..</option>
	                        <?php
                            /* for($x=$vigusu;$x>=$vigusu-5;$x--)
                            {
                                $i=$x;
                                echo "<option  value=$x ";
                                if($i==$_POST['vigencias'])
                                        {
                                        echo " SELECTED";
                                        }
                                echo " >".$x."</option>";
                            } */
	                        ?>
                        </select>
	                </td> -->

	                <td  class="saludo1" style="width: 5%;">ica:</td>
	                <td style="width: 10%;">
                        <select name="ica" id="ica" style="width: 90%;">
                            <option value="">Sel..</option>
                            <?php
                            $sqlr="SELECT id, nombre FROM tesoretenciones WHERE nombre LIKE '% ica %'";
                            $res = mysqli_query($linkbd,$sqlr);

                            while($row=mysqli_fetch_row($res))
                            {
                                echo "<option  value=$row[0] ";
                                if($row[0]==$_POST['ica'])
                                {
                                    echo " SELECTED";
                                }
                                echo " >".$row[1]."</option>";
                            }

                            ?>
                        </select>
	                </td>
                    <td style="width: 10%;">
                        <input type="button" name="generar" value="Generar" onClick="document.form2.submit()"> <input type="hidden" value="1" name="oculto">
                    </td>
                </tr>
            </table>
            <?php
			if($_POST['bc']=='1')
            {
                $nresul=buscatercero($_POST['tercero']);
                if($nresul!='')
                {
                    $_POST['ntercero']=$nresul;
                    ?>
                    <script>
                    document.form2.fecha.focus();document.form2.fecha.select();</script>
                    <?php
                }
                else
                {
                    $_POST['ntercero']="";
                    ?>
                    <script>alert("Tercero Incorrecta");document.form2.tercero.focus();</script>
                    <?php
                }
            }
			?>
            <div class="subpantallap" style="height:66.5%; width:99.6%; overflow-x:hidden;">
                <?php
                $oculto=$_POST['oculto'];
                $condFechaTeso = "";
                $condFechaEgreso = "";
                $condFechaOtrosEgresos = "";
                if($_POST['oculto'])
                {
                    if($_POST['fecha']!='')
					{
						preg_match("/([0-9]{2})\/([0-9]{2})\/([0-9]{4})/",$_POST['fecha'],$fecha);
						$fechai="$fecha[3]-$fecha[2]-$fecha[1]";
						if($_POST['fecha2']!='')
						{
							preg_match("/([0-9]{2})\/([0-9]{2})\/([0-9]{4})/",$_POST['fecha2'],$fecha);
							$fechaf="$fecha[3]-$fecha[2]-$fecha[1]";
							$condFechaTeso = "AND tesoordenpago.fecha BETWEEN '$fechai' AND '$fechaf'";
							$condFechaEgreso = "AND tesoegresos.fecha BETWEEN '$fechai' AND '$fechaf'";
							$condFechaOtrosEgresos = "AND TB1.fecha BETWEEN '$fechai' AND '$fechaf'";
						}
						else
						{
							$fechaf=date("Y-m-d");
							$condFechaTeso = "AND tesoordenpago.fecha BETWEEN '$fechai' AND '$fechaf'";
							$condFechaEgreso = "AND tesoegresos.fecha BETWEEN '$fechai' AND '$fechaf'";
							$condFechaOtrosEgresos = "AND TB1.fecha BETWEEN '$fechai' AND '$fechaf'";
						}
					}

                    if($_POST['fecha1']!="" && $_POST['fecha2']!="")
                    {
                        $cond=" and tesoegresos.fecha between '$_POST[fecha1]' and '$_POST[fecha2]' ";
                    }
                    //sacar el consecutivo
                    //$sqlr="select *from pptosideforigen where".$crit1.$crit2." order by pptosideforigen.codigo";
                        //consulta incluyendo retenciones
                    //	$sqlr="select tesoegresos.id_egreso, tesoegresos.id_orden, tesoegresos.tercero, tesoegresos.fecha, tesoegresos.valortotal, tesoegresos.concepto, tesoretenciones.codigo, tesoretenciones.nombre, tesoordenpago_retenciones.valor from tesoegresos, tesoordenpago, tesoordenpago_retenciones, tesoretenciones  where tesoegresos.TERCERO='$_POST[tercero]' AND YEAR(tesoegresos.FECHA) = '$_POST[vigencias]' AND tesoegresos.ESTADO='S' and tesoordenpago.id_orden=tesoegresos.id_orden and tesoordenpago_retenciones.id_orden=tesoordenpago.id_orden and tesoordenpago_retenciones.id_retencion=tesoretenciones.id and tesoretenciones.destino='$_POST[tiporet]' ".$cond." order by tesoegresos.id_egreso DESC";

                    //consulta sin las retenciones
                    $crit = "";
                    $crit2 = "";
                    if(!empty($_POST['ica']))
                    {
                        $crit = " AND tesoretenciones.id = ".$_POST['ica']." ";
                        $crit2 = " AND TB3.id = ".$_POST['ica']." ";
                    }

                    $query="SELECT conta_pago FROM tesoparametros";
                    $resultado=mysqli_query($linkbd, $query);
                    $arreglo=mysqli_fetch_row($resultado);
                    $opcion=$arreglo[0];
                    if($opcion=='1')
                    {
                        $sqlr="select tesoordenpago.id_orden, tesoordenpago.id_orden, tesoordenpago.tercero, tesoordenpago.fecha, tesoordenpago.valorpagar, tesoordenpago.conceptorden, tesoretenciones.codigo, tesoretenciones.nombre, tesoordenpago_retenciones.valor, tesoretenciones.iva, tesoordenpago.iva, tesoordenpago.base from tesoordenpago, tesoordenpago_retenciones, tesoretenciones  where tesoordenpago.tercero='$_POST[tercero]' ".$condFechaTeso." AND (tesoordenpago.estado!='R' AND tesoordenpago.estado!='N') and tesoordenpago_retenciones.id_orden=tesoordenpago.id_orden and tesoordenpago_retenciones.id_retencion=tesoretenciones.id and tesoretenciones.destino='$_POST[tiporet]'  ".$cond.$crit." order by tesoordenpago.id_orden DESC";

                    }
                    else
                    {
                        $sqlr="select tesoegresos.id_egreso, tesoegresos.id_orden, tesoegresos.tercero, tesoegresos.fecha, tesoegresos.valortotal, tesoegresos.concepto, tesoretenciones.codigo, tesoretenciones.nombre, tesoordenpago_retenciones.valor, tesoretenciones.iva, tesoordenpago.iva, tesoordenpago.base from tesoegresos, tesoordenpago, tesoordenpago_retenciones, tesoretenciones  where tesoegresos.tercero='$_POST[tercero]' ".$condFechaEgreso." AND (tesoordenpago.estado!='R' AND tesoordenpago.estado!='N') AND tesoegresos.ESTADO='S' and tesoordenpago.id_orden=tesoegresos.id_orden and tesoordenpago_retenciones.id_orden=tesoordenpago.id_orden and tesoordenpago_retenciones.id_retencion=tesoretenciones.id and tesoretenciones.destino='$_POST[tiporet]' ".$cond.$crit." order by tesoegresos.id_egreso DESC";
                    }

                    $resp = mysqli_query($linkbd, $sqlr);
                    $ntr = mysqli_num_rows($resp);
                    $con=1;
	                $namearch="archivos/".$_SESSION['usuario']."-reporteegresos.csv";
                    $Descriptor1 = fopen($namearch,"w+");
                    fputs($Descriptor1,"EGRESO;ORDEN PAGO;Doc Tercero;TERCERO;FECHA;VALOR_EGRESO;CONCEPTO;ESTADO\r\n");
                    echo "<table class='inicio' align='center' ><tr><td colspan='8' class='titulos'>.: Resultados Busqueda:</td></tr><tr><td colspan='8' class='saludo3'>Resultados Encontrados: $ntr</td></tr><tr><td  class='titulos2'>Egreso</td><td  class='titulos2'>Orden Pago</td><td class='titulos2'>Doc Tercero</td><td class='titulos2'>Tercero</td><td class='titulos2'>Fecha</td><td class='titulos2'>Valor Egreso</td><td class='titulos2' ><center>RETENCION</td><td class='titulos2'><center>VLR RETENCION</td></tr>";
                    //echo "nr:".$nr;
                    $iter='saludo1a';
                    $iter2='saludo2';
                    $sumaegresos=0;
                    $sumaretenciones=0;
                    $retenciones[]=array();
                    while ($row =mysqli_fetch_row($resp))
                    {
                        //	$sqlret="select tesoretenciones.codigo, tesoretenciones.nombre, tesoordenpago_retenciones.valor from  tesoordenpago_retenciones, tesoretenciones  where tesoordenpago_retenciones.id_orden=tesoordenpago.id_orden and tesoordenpago_retenciones.id_retencion=tesoretenciones.id and tesoretenciones.destino='$_POST[tiporet]'  order by";
                        $valorBase = 0;
                        if($row[9] == '1'){
                            $valorBase = $row[10];
                        }else{
                            //$valorBase = round(($row[4]-$row[10]),2);
                            $valorBase = $row[11];
                        }
                        $ntr=buscatercero($row[2]);
                        echo "<tr class='$iter'><td >$row[0]</td><td >$row[1]</td><td >$row[2]</td><td >$ntr</td><td >$row[3]</td><td >".number_format($valorBase,2)."</td><td >".strtoupper($row[6])." ".strtoupper($row[7])."</td><td >".strtoupper($row[8])."</td></tr>";
                        $retenciones["$row[6]"][0]=$row[6];
                        $retenciones["$row[6]"][1]=$row[7];
                        $retenciones["$row[6]"][2]+=$row[8];
                        $retenciones["$row[6]"][3]+=$valorBase;
                        $con+=1;
                        $aux=$iter;
                        $iter=$iter2;
                        $iter2=$aux;
                        $sumaegresos+=$row[4];
                        $sumaretenciones+=$row[8];
                        fputs($Descriptor1,$row[0].";".$row[2].";".''.";".$ntr.";".$row[3].";".number_format($row[7],2,",","").";".strtoupper($row[8]).";".strtoupper($row[13])."\r\n");

                    }

                    $sqlrOtrosEgresos = "SELECT TB1.id_pago, TB1.tercero, TB1.fecha, TB1.valor, TB1.concepto, TB3.codigo, TB3.nombre, TB2.valor FROM tesopagotercerosvigant AS TB1, tesopagotercerosvigant_retenciones AS TB2, tesoretenciones AS TB3 WHERE TB1.tercero='$_POST[tercero]' ".$condFechaOtrosEgresos." AND TB1.estado != 'N' AND TB1.id_pago = TB2.id_egreso AND TB2.id_retencion = TB3.id AND TB3.destino = '$_POST[tiporet]' ".$cond.$crit2." ORDER BY TB1.id_pago DESC";
                    $respOtrosEgresos = mysqli_query($linkbd, $sqlrOtrosEgresos);
                    while ($rowOtrosEgresos = mysqli_fetch_row($respOtrosEgresos)){

                        $valorBase = 0;

                        $valorBase = $rowOtrosEgresos[3];

                        $ntr=buscatercero($rowOtrosEgresos[1]);
                        echo "
                            <tr class='$iter'>
                                <td >$rowOtrosEgresos[0]</td>
                                <td >$rowOtrosEgresos[0]</td>
                                <td >$rowOtrosEgresos[1]</td>
                                <td >$ntr</td>
                                <td >$rowOtrosEgresos[2]</td>
                                <td >".number_format($valorBase,2)."</td>
                                <td >".strtoupper($rowOtrosEgresos[5])." ".strtoupper($rowOtrosEgresos[6])."</td>
                                <td >".strtoupper($rowOtrosEgresos[7])."</td>
                            </tr>";

                        $retenciones["$rowOtrosEgresos[5]"][0] = $rowOtrosEgresos[5];
                        $retenciones["$rowOtrosEgresos[5]"][1] = $rowOtrosEgresos[6];
                        $retenciones["$rowOtrosEgresos[5]"][2] += $rowOtrosEgresos[7];
                        $retenciones["$rowOtrosEgresos[5]"][3] += $valorBase;
                        $con+=1;
                        $aux=$iter;
                        $iter=$iter2;
                        $iter2=$aux;
                        $sumaegresos+=$rowOtrosEgresos[3];
                        $sumaretenciones+=$rowOtrosEgresos[7];
                        fputs($Descriptor1,$rowOtrosEgresos[0].";".$rowOtrosEgresos[1].";".''.";".$ntr.";".$rowOtrosEgresos[2].";".number_format($rowOtrosEgresos[6],2,",","").";".strtoupper($rowOtrosEgresos[7]).";".strtoupper($rowOtrosEgresos[4])."\r\n");

                    }


                    echo "<tr class='$iter'><td class='saludo1' colspan='7'>TOTALES:</td><td class='saludo1'>".number_format($sumaretenciones,2,".",",")."</td></tr>";
                    fclose($Descriptor1);
                    echo"</table>";
                    $tam=count($retenciones);
                    foreach($retenciones as $k => $valores )
                    {
                        // echo "<br>".$retenciones[$k][0]." ".$retenciones[$k][1]." ".$retenciones[$k][2];
                        echo "<input type='hidden' name='codigo[]' value='".$retenciones[$k][0]."'>";
                        echo "<input type='hidden' name='nombres[]' value='".$retenciones[$k][1]."'>";
                        echo "<input type='hidden' name='valores[]' value='".$retenciones[$k][2]."'>";
                        echo "<input type='hidden' name='valoresret[]' value='".$retenciones[$k][3]."'>";
                    }
                }
                ?>
                </div>
            </form>
        </td>
    </tr>
</table>
</body>
</html>
