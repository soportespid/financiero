<?php
	header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=utf8");

	require "comun.inc";
	require "funciones.inc";

	$linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");

	session_start();
	date_default_timezone_set("America/Bogota");
?>
<!DOCTYPE > 
<html lang="es">
	<head>
		<meta charset="utf-8"/>
		<meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1"/>
		<meta name="viewport" content="user-scalable=no">
		<title>:: IDEAL 10 - Contabilidad</title>
		<link href="favicon.ico" rel="shortcut icon"/>
		<link href="css/css2.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<script type="text/javascript" src="css/programas.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script type="text/javascript" src="css/calendario.js"></script>
		<script>
			var anterior;
			function ponprefijo(pref,opc )
			{
				parent.document.form2.aesgpriet.value =pref;
				parent.document.form2.nAesgpriet.value =opc ;
				parent.despliegamodal2("hidden");
			} 
		</script>
	</head>
	<body>
		<form method="post" name="form2">
			<table class="inicio">
				<tr>
					<td style="height:30px!important;" colspan="3" class="titulos" >Buscar Entidades AESGPRI</td>
					<td style="width:7%;padding-bottom:7px!important;"><label class="boton02" onClick="parent.despliegamodal2('hidden');">Cerrar</label></td>
				</tr>
				<tr><td colspan="4" class="titulos2" >:&middot; Por nombre </td></tr>
			</table>
			<div class="subpantalla" style="height:86.5%; width:99.6%; overflow-x:hidden;">
				<?php
					$sqlr="SELECT entidad,nombre_entidad FROM aesgpriet ORDER BY id";
					$resp = mysqli_query($linkbd, $sqlr);
					$numero = mysqli_num_rows($resp);
					$co='saludo1a';
					$co2='saludo2';
					$i=1;
					echo "
					<table class='inicio'>
						<tr>
							<td colspan='4' class='titulos' >Resultados Busqueda </td>
						</tr>
						<tr><td colspan='5'>Cuentas Encontradas: $numero</td></tr>
						<tr>
							<td width='32' class='titulos2' >Item</td>
							<td width='76' class='titulos2' >C&oacute;digo </td>
							<td width='140' class='titulos2' >Descripci&oacute;n</td>
						</tr>";
					while ($r =mysqli_fetch_row($resp)) 
					{
						echo"
						<tr class='$co' onClick=\"javascript:ponprefijo('$r[0]','$r[1]')\">
							<td>$i</td>
							<td>$r[0]</td>
							<td>".ucwords(strtolower($r[1]))."</td>
						</tr>";
						$aux=$co;
						$co=$co2;
						$co2=$aux;
						++$i;
					}
					
						$aux=$co;
						$co=$co2;
						$co2=$aux;
						++$i;
					echo"
					</table>";
				?>
			</div>
		</form>
	</body>
</html>
 