<?php
	header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=utf8");
	require 'comun.inc';
	require 'funciones.inc';
	session_start();
	$linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");
	cargarcodigopag($_GET['codpag'],$_SESSION["nivel"]);
	date_default_timezone_set("America/Bogota");
?>
<!DOCTYPE >
<html lang=es>
	<head>
		<meta charset="utf-8"/>
		<meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1"/>
		<meta name="viewport" content="user-scalable=no">
		<title>:: IDEAL 10 - Activos Fijos</title>
		<link href="favicon.ico" rel="shortcut icon"/>
		<link href="css/css2z.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
		<link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
		<link href="css/css4.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
        <link href="css/tabs.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css"/>
        <link rel="stylesheet" href="sweetalert2/dist/sweetalert2.min.css">
        <script src="sweetalert2/dist/sweetalert2.min.js"></script>
		<script src="vue/vue.min.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script src="css/programas.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script src="css/calendario.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
        <script>

        </script>
		<style>
			input[type=number]::-webkit-inner-spin-button,
			input[type=number]::-webkit-outer-spin-button {
				-webkit-appearance: none;
				margin: 0;
			}
			input[type=number]{
				text-align:right;
				box-sizing: border-box;
				font: 120% sans-serif;
				/* width: 100% !important; */
			}
			[v-cloak]{display : none;}

			.centrarSelect {

				height: 30px !important;
				text-align-last:center !important;
			}

			.aumentarTamaño {

				font-size:15px;
			}

            .tamano01 {
                text-align: center !important;
            }
		</style>
	</head>
	<body>
		<header>
			<table>
				<tr><script>barra_imagenes("acti");</script><?php cuadro_titulos();?></tr>
			</table>
		</header>

		<form name="form2" method="post" action="">
			<section id="myapp" v-cloak >
				<nav>
					<table>
						<tr><?php menu_desplegable("acti");?></tr>
					</table>
                    <div class="bg-white group-btn p-1">
                        <button type="button" class="btn btn-white btn-primary-hover d-flex justify-between align-items-center">
                            <span>Nuevo</span>
                            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 -960 960 960">
                                <path d="M440-440H200v-80h240v-240h80v240h240v80H520v240h-80v-240Z"></path>
                            </svg>
                        </button>
                        <button type="button" class="btn btn-white btn-primary-hover d-flex justify-between align-items-center">
                            <span>Guardar</span>
                            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 -960 960 960">
                                <path
                                    d="M840-680v480q0 33-23.5 56.5T760-120H200q-33 0-56.5-23.5T120-200v-560q0-33 23.5-56.5T200-840h480l160 160Zm-80 34L646-760H200v560h560v-446ZM480-240q50 0 85-35t35-85q0-50-35-85t-85-35q-50 0-85 35t-35 85q0 50 35 85t85 35ZM240-560h360v-160H240v160Zm-40-86v446-560 114Z">
                                </path>
                            </svg>
                        </button>
                        <button type="button" class="btn btn-white btn-primary-hover d-flex justify-between align-items-center">
                            <span>Buscar</span>
                            <svg class="fill-black group-hover:fill-white w-5 h-5" xmlns="http://www.w3.org/2000/svg" viewBox="0 -960 960 960">
                                <path d="M784-120 532-372q-30 24-69 38t-83 14q-109 0-184.5-75.5T120-580q0-109 75.5-184.5T380-840q109 0 184.5 75.5T640-580q0 44-14 83t-38 69l252 252-56 56ZM380-400q75 0 127.5-52.5T560-580q0-75-52.5-127.5T380-760q-75 0-127.5 52.5T200-580q0 75 52.5 127.5T380-400Z"></path>
                            </svg>
                        </button>
                        <button type="button" onclick="window.open('acti-principal');" class="btn btn-white btn-primary-hover d-flex justify-between align-items-center">
                            <span>Nueva ventana</span>
                            <svg class="fill-black group-hover:fill-white w-5 h-5" xmlns="http://www.w3.org/2000/svg"
                                viewBox="0 -960 960 960">
                                <path
                                    d="M320-120v-80h80v-80H160q-33 0-56.5-23.5T80-360v-400q0-33 23.5-56.5T160-840h640q33 0 56.5 23.5T880-760v400q0 33-23.5 56.5T800-280H560v80h80v80H320ZM160-360h640v-400H160v400Zm0 0v-400 400Z">
                                </path>
                            </svg>
                        </button>
                        <button type="button" v-on:click="excel" class="btn btn-success btn-success-hover d-flex justify-between align-items-center">
                            <span>Exportar Excel</span>
                            <svg class="fill-black group-hover:fill-white w-4 h-4" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 384 512"><!--!Font Awesome Free 6.5.1 by @fontawesome - https://fontawesome.com License - https://fontawesome.com/license/free Copyright 2024 Fonticons, Inc. -->
                                <path d="M48 448V64c0-8.8 7.2-16 16-16H224v80c0 17.7 14.3 32 32 32h80V448c0 8.8-7.2 16-16 16H64c-8.8 0-16-7.2-16-16zM64 0C28.7 0 0 28.7 0 64V448c0 35.3 28.7 64 64 64H320c35.3 0 64-28.7 64-64V154.5c0-17-6.7-33.3-18.7-45.3L274.7 18.7C262.7 6.7 246.5 0 229.5 0H64zm90.9 233.3c-8.1-10.5-23.2-12.3-33.7-4.2s-12.3 23.2-4.2 33.7L161.6 320l-44.5 57.3c-8.1 10.5-6.3 25.5 4.2 33.7s25.5 6.3 33.7-4.2L192 359.1l37.1 47.6c8.1 10.5 23.2 12.3 33.7 4.2s12.3-23.2 4.2-33.7L222.4 320l44.5-57.3c8.1-10.5 6.3-25.5-4.2-33.7s-25.5-6.3-33.7 4.2L192 280.9l-37.1-47.6z">
                                </path>
                            </svg>
                        </button>
                    </div>
				</nav>

				<article>
                    <div>
                        <table class="inicio">
                            <tr>
                                <td class="titulos" colspan="12">.: Reporte de activos</td>
								<td class="cerrar" style="width:4%" onClick="location.href='acti-principal.php'">Cerrar</td>
                            </tr>

                            <tr>
                                <td>Clase:</td>
                                <td style="width: 10%;">
                                    <select v-model="clase" style="width:98%;" v-on:change="buscarGrupo">
                                        <option value="">Todas</option>
                                        <option v-for="clas in clases" v-bind:value="clas[0]">
                                            {{ clas[0] }} - {{ clas[1] }}
                                        </option>
                                    </select>
                                </td>

                                <td>Grupo:</td>
                                <td style="width: 10%;">
                                    <select v-model="grupo" style="width:98%;" v-on:change="buscarTipo">
                                        <option value="">Todos</option>
                                        <option v-for="grup in grupos" v-bind:value="grup[0]">
                                            {{ grup[0] }} - {{ grup[1] }}
                                        </option>
                                    </select>
                                </td>

                                <td>Tipo:</td>
                                <td style="width: 10%;">
                                    <select v-model="tipo" style="width:98%;">
                                        <option value="">Todos</option>
                                        <option v-for="tip in tipos" v-bind:value="tip[0]">
                                            {{ tip[0] }} - {{ tip[1] }}
                                        </option>
                                    </select>
                                </td>

                                <td>Placa inicial: </td>
								<td style="width: 10%;">
									<input type="text" v-model="placaInicial">
								</td>

								<td>Placa final: </td>
								<td>
									<input type="text" v-model="placaFinal">
								</td>
                            </tr>

                            <tr>
                                <td >Fecha Inicial:</td>
                                <td>
                                    <input type="text" name="fechaIni"  value="<?php echo $_POST['fechaIni']?>" onKeyUp="return tabular(event,this)" id="fechaIni" title="DD/MM/YYYY" onDblClick="displayCalendarFor('fechaIni');" class="colordobleclik" autocomplete="off" onChange=""  readonly>
                                </td>

                                <td >Fecha Final:</td>
                                <td>
                                    <input type="text" name="fechaFin" value="<?php echo $_POST['fechaFin']?>" onKeyUp="return tabular(event,this)" id="fechaFin" title="DD/MM/YYYY" onDblClick="displayCalendarFor('fechaFin');" class="colordobleclik" autocomplete="off" onChange="" readonly>
                                </td>

                                <td>
									<button type="button" class="botonflechaverde" v-on:click="buscarActivos">Generar reporte</button>
								</td>
                            </tr>
                        </table>

						<div class='subpantalla' style='height:64vh; width:99.2%; margin-top:0px; resize: vertical;'>
                            <table class=''>
                                <thead>
                                    <tr>
										<th class="titulosnew00" style="width:6%;">Placa</th>
										<th class="titulosnew00">Nombre</th>
										<th class="titulosnew00" style="width:6%;">Fecha creación</th>
										<th class="titulosnew00" style="width:6%;">Fecha compra</th>
										<th class="titulosnew00" style="width:6%;">Fecha <br> activación</th>
										<th class="titulosnew00" style="width:10%;">Clase</th>
										<th class="titulosnew00" style="width:10%;">Grupo</th>
										<th class="titulosnew00" style="width:10%;">Tipo</th>
										<th class="titulosnew00" style="width:10%;">Dependendencia</th>
										<th class="titulosnew00" style="width:10%;">Ubicación</th>
										<th class="titulosnew00" style="width:10%;">Responsable</th>
										<th class="titulosnew00" style="width:7%;">Valor activo</th>
										<th class="titulosnew00" style="width:7%;">Valor activo Modulo</th>
										<th class="titulosnew00" style="width:7%;">Valor depreciado</th>
										<th class="titulosnew00" style="width:7%;">Valor correción</th>
										<th class="titulosnew00" style="width:7%;">Valor por <br> depreciar</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr v-for="(activo,index) in activos" v-bind:class="(activo[0] == '' ? 'saludo2a' : 'saludo1a')" style="font: 100% sans-serif;" v-on:dblclick="fichaTecnica(activo)">
										<td style="width:6%; text-align:left;">{{ activo[0] }}</td>
										<td style="text-align:left;">{{ activo[1] }}</td>
										<td style="width:6%; text-align:center;">{{ activo[2] }}</td>
										<td style="width:6%; text-align:center;">{{ activo[3] }}</td>
										<td style="width:6%; text-align:center;">{{ activo[4] }}</td>
										<td style="width:10%; text-align:left;">{{ activo[5] }}</td>
										<td style="width:10%; text-align:left;">{{ activo[6] }}</td>
										<td style="width:10%; text-align:left;">{{ activo[7] }}</td>
										<td style="width:10%; text-align:left;">{{ activo[13] }}</td>
										<td style="width:10%; text-align:left;">{{ activo[14] }}</td>
										<td style="width:10%; text-align:left;">{{ activo[15] }}</td>
										<td style="width:7%; text-align:right;" v-bind:class="(activo[8] != activo[9] ? 'contenidonew04' : '')">{{formatonumero(activo[8])}}</td>
										<td style="width:7%; text-align:right;" v-bind:class="(activo[9] != activo[8] ? 'contenidonew04' : '')">{{formatonumero(activo[9])}}</td>
										<td style="width:7%; text-align:right;">{{formatonumero(activo[10])}}</td>
										<td style="width:7%; text-align:right;">{{formatonumero(activo[11])}}</td>
										<td style="width:7%; text-align:right;">{{formatonumero(activo[12])}}</td>

										<input type='hidden' name='placa[]' v-model="activo[0]">
										<input type='hidden' name='nombre[]' v-model="activo[1]">
										<input type='hidden' name='fechaCreacion[]' v-model="activo[2]">
										<input type='hidden' name='fechaCompra[]' v-model="activo[3]">
										<input type='hidden' name='fechaActivacion[]' v-model="activo[4]">
										<input type='hidden' name='clase[]' v-model="activo[5]">
										<input type='hidden' name='grupo[]' v-model="activo[6]">
										<input type='hidden' name='tipo[]' v-model="activo[7]">
										<input type='hidden' name='dependencia[]' v-model="activo[13]">
										<input type='hidden' name='ubicacion[]' v-model="activo[14]">
										<input type='hidden' name='responsable[]' v-model="activo[15]">
										<input type='hidden' name='valorActivo[]' v-model="activo[8]">
										<input type='hidden' name='valorActivoModulo[]' v-model="activo[9]">
										<input type='hidden' name='valorDepreciado[]' v-model="activo[10]">
										<input type='hidden' name='valorCorrecion[]' v-model="activo[11]">
										<input type='hidden' name='ValorXDepreciar[]' v-model="activo[12]">
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>

					<div id="cargando" v-if="loading" class="loading">
						<span>Cargando...</span>
					</div>
				</article>
			</section>
		</form>

		<script src="node_modules/read-excel-file/bundle/read-excel-file.min.js"></script>
		<script src="node_modules/xlsx/dist/xlsx.full.min.js"></script>
		<script src="Librerias/vue/vue.min.js"></script>
		<script src="Librerias/vue/axios.min.js"></script>
		<script src="activos_fijos/reporte_activos/acti-reporteActivosFijos.js"></script>

	</body>
</html>
