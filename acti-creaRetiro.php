<?php

    header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=utf8");
	require 'comun.inc';
	require 'funciones.inc';
	session_start();
    if(empty($_SESSION)){
        header("location: index.php");
    }
	$linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");
	cargarcodigopag($_GET['codpag'],$_SESSION["nivel"]);
	date_default_timezone_set("America/Bogota");
?>
<!DOCTYPE >
<html lang=es>
	<head>
    <meta charset="utf-8"/>
		<meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1"/>
		<meta name="viewport" content="user-scalable=no">
		<title>:: IDEAL 10 - Activos fijos</title>
		<link href="favicon.ico" rel="shortcut icon"/>
		<link href="css/css2z.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
		<link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
		<link href="css/css4.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
        <link href="css/tabs.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css"/>
        <link href="css/style.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css"/>
        <link rel="stylesheet" href="sweetalert2/dist/sweetalert2.min.css">
        <script type="text/javascript" src="css/programas.js"></script>
        <script src="sweetalert2/dist/sweetalert2.min.js"></script>
		<script src="vue/vue.min.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script src="css/programas.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script src="css/calendario.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
	</head>
	<body>
		<header>
			<table>
				<tr><script>barra_imagenes("acti");</script><?php cuadro_titulos();?></tr>
			</table>
		</header>

        <section id="myapp" v-cloak>
            <div class="loading-container" v-show="isLoading" >
                <p class="text-loading"data-loading="IDEAL 10">IDEAL 10</p>
            </div>
            <nav>
                <table>
                    <tr><?php menu_desplegable("acti");?></tr>
                </table>
                <div class="bg-white group-btn p-1" id="newNavStyle">
                    <button type="button" class="btn btn-white btn-primary-hover d-flex justify-between align-items-center" @click="window.location.reload()">
                        <span>Nuevo</span>
                        <svg viewBox="0 -960 960 960" ><path d="M440-440H200v-80h240v-240h80v240h240v80H520v240h-80v-240Z"/></svg>
                    </button>
                    <button type="button" class="btn btn-white btn-primary-hover d-flex justify-between align-items-center" @click="save()">
                        <span>Guardar</span>
                        <svg viewBox="0 -960 960 960" ><path d="M840-680v480q0 33-23.5 56.5T760-120H200q-33 0-56.5-23.5T120-200v-560q0-33 23.5-56.5T200-840h480l160 160Zm-80 34L646-760H200v560h560v-446ZM480-240q50 0 85-35t35-85q0-50-35-85t-85-35q-50 0-85 35t-35 85q0 50 35 85t85 35ZM240-560h360v-160H240v160Zm-40-86v446-560 114Z"/></svg>
                    </button>
                    <button type="button" class="btn btn-white btn-primary-hover d-flex justify-between align-items-center" @click="location.href='acti-buscarRetiro'">
                        <span>Buscar</span>
                        <svg class="fill-black group-hover:fill-white w-5 h-5" viewBox="0 -960 960 960" ><path d="M784-120 532-372q-30 24-69 38t-83 14q-109 0-184.5-75.5T120-580q0-109 75.5-184.5T380-840q109 0 184.5 75.5T640-580q0 44-14 83t-38 69l252 252-56 56ZM380-400q75 0 127.5-52.5T560-580q0-75-52.5-127.5T380-760q-75 0-127.5 52.5T200-580q0 75 52.5 127.5T380-400Z"/></svg>
                    </button>
                    <button type="button" class="btn btn-white btn-primary-hover d-flex justify-between align-items-center" @click="mypop=window.open('acti-principal.php','',''); mypop.focus();">
                        <span>Nueva ventana</span>
                        <svg class="fill-black group-hover:fill-white w-5 h-5" viewBox="0 -960 960 960" ><path d="M320-120v-80h80v-80H160q-33 0-56.5-23.5T80-360v-400q0-33 23.5-56.5T160-840h640q33 0 56.5 23.5T880-760v400q0 33-23.5 56.5T800-280H560v80h80v80H320ZM160-360h640v-400H160v400Zm0 0v-400 400Z"/></svg>
                    </button>
                    <button type="button" class="btn btn-success d-flex justify-between align-items-center" @click="window.location.href='acti-gestiondelosactivos'">
                        <span>Atrás</span>
                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 -960 960 960"><path d="M760-200v-160q0-50-35-85t-85-35H273l144 144-57 56-240-240 240-240 57 56-144 144h367q83 0 141.5 58.5T840-360v160h-80Z"/></svg>
                    </button>
                </div>
            </nav>
            <article>
                <div class="bg-white">
                    <div>
                        <h2 class="titulos m-0">Crear acta de retiro</h2>
                        <p class="m-0 ms-2">Todos los campos con (<span class="text-danger fw-bolder">*</span>) son obligatorios</p>
                        
                        <div class="w-90">
                            <div class="d-flex">
                                <div class="form-control w-15">
                                    <label class="form-label m-0" for="">Consecutivo:</label>
                                    <input type="text" v-model="consecutivo" class="text-center" readonly>
                                </div>
    
                                <div class="form-control w-25">
                                    <label class="form-label m-0" for="">Fecha <span class="text-danger fw-bolder">*</span>:</label>
                                    <input type="date" v-model="fecha" class="text-center">
                                </div>
    
                                <div class="form-control">
                                    <label class="form-label m-0" for="">Descripción <span class="text-danger fw-bolder">*</span>:</label>
                                    <textarea v-model="descripcion" style="max-height: 7vh; resize:vertical"></textarea>
                                </div>
                            </div>

                            <div class="d-flex">
                                <div class="form-control W-25">
                                    <label class="form-label m-0" for="">Placa <span class="text-danger fw-bolder">*</span>:</label>
                                    <input type="text" v-model="activo.placa" v-on:dblclick="modalActivos=true;" class="colordobleclik" readonly>
                                </div>

                                <div class="form-control">
                                    <label class="form-label m-0" for="">Nombre:</label>
                                    <input type="text" v-model="activo.nombre" readonly>
                                </div>

                                <div class="form-control w-25">
                                    <label class="form-label m-0" for="">Ubicación:</label>
                                    <input type="text" v-model="activo.ubicacion" readonly>
                                </div>

                                <div class="form-control w-75">
                                    <label class="form-label m-0" for="">Responsable:</label>
                                    <input type="text" v-model="activo.name" readonly>
                                </div>
                            </div>

                            <div class="d-flex">
                                <div class="form-control w-25">
                                    <label class="form-label m-0" for="">Estado de activo <span class="text-danger fw-bolder">*</span>:</label>
                                    <select v-model="estado">
                                        <option value="">Seleccione</option>
                                        <option value="Bueno">Bueno</option>
                                        <option value="Regular">Regular</option>
                                        <option value="Malo">Malo</option>
                                    </select>
                                </div>

                                <div class="form-control w-50">
                                    <label class="form-label m-0" for="">Motivo de baja <span class="text-danger fw-bolder">*</span>:</label>
                                    <select v-model="motivo">
                                        <option value="">Seleccione</option>
                                        <option value="Inservible">Inservible</option>
                                        <option value="Desgaste">Desgaste</option>
                                        <option value="Deterioro">Deterioro</option>
                                        <option value="Insalubre">Insalubre</option>
                                        <option value="Obsolescencia">Obsolescencia</option>
                                        <option value="Siniestrado">Siniestrado</option>
                                        <option value="Hurtado">Hurtado</option>
                                    </select>
                                </div>

                                <div class="form-control w-50">
                                    <label class="form-label m-0" for="">Destino de activo <span class="text-danger fw-bolder">*</span>:</label>
                                    <select v-model="destino">
                                        <option value="">Seleccione</option>
                                        <option value="Donacion">Donacion</option>
                                        <option value="Destruccion">Destruccion</option>
                                        <option value="Venta">Venta</option>
                                        <option value="Baja por obsolescencia">Baja por obsolescencia</option>
                                    </select>
                                </div>

                                <div class="form-control justify-between w-25">
                                    <label for=""></label>
                                    <button type="button" class="btn btn-primary" @click="addActive()">Agregar activo</button>
                                </div>
                            </div>
                        </div>

                        <div class="table-responsive" style="max-height: 32vh;">
                            <table class="table table-hover fw-normal">
                                <thead>
                                    <tr class="text-center">
                                        <th>Placa</th>
                                        <th>Nombre</th>
                                        <th>Fecha compra</th>
                                        <th>Fecha ingreso</th>
                                        <th>Dependencia</th>
                                        <th>Ubicacion</th>
                                        <th>Responsable</th>
                                        <th>Valor activo</th>
                                        <th>Valor depreciado</th>
                                        <th>Valor a depreciar</th>
                                        <th>Estado</th>
                                        <th>Motivo de baja</th>
                                        <th>Destino</th>
                                        <th>Eliminar</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr v-for="(activo,index) in retiroActivos" :key="index">
                                        <td>{{activo.placa}}</td>
                                        <td>{{activo.nombre}}</td>
                                        <td class="text-center">{{formatFecha(activo.fechaCompra)}}</td>
                                        <td class="text-center">{{formatFecha(activo.fechaIngreso)}}</td>
                                        <td>{{activo.dependencia}}</td>
                                        <td>{{activo.ubicacion}}</td>
                                        <td>{{activo.name}}</td>
                                        <td class="text-center">{{formatNumero(activo.valorActivo)}}</td>
                                        <td class="text-center">{{formatNumero(activo.valorDepreciado)}}</td>
                                        <td class="text-center">{{formatNumero(activo.valorADepreciar)}}</td>
                                        <td>{{activo.estado}}</td>
                                        <td>{{activo.motivo}}</td>
                                        <td>{{activo.destino}}</td>
                                        <td>
                                            <button type="button" class="btn btn-danger btn-sm m-1" @click="deleteActive(index)">
                                                Eliminar
                                            </button>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>

                        <div v-show="modalActivos" class="modal">
                        <div class="modal-dialog modal-xl" >
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title">Listado de activos</h5>
                                    <button type="button" @click="modalActivos=false;" class="btn btn-close"><div></div><div></div></button>
                                </div>
                                <div class="modal-body">
                                    <div class="d-flex flex-column">
                                        <div class="form-control m-0 mb-3">
                                            <input type="search" v-model="txtSearch" @keyup="searchData()" placeholder="Busca por placa, nombre o responsable">
                                        </div>
                                        <div class="form-control m-0 p-2">
                                            <label class="form-label" for="labelInputName">Resultados: <span class="fw-bold">{{txtResultados}}</span></label>
                                        </div>
                                    </div>
                                    <div class="overflow-auto max-vh-50 overflow-x-hidden" >
                                        <table class="table table-hover fw-normal">
                                            <thead>
                                                <tr>
                                                    <th>Placa</th>
                                                    <th>Nombre</th>
                                                    <th>Dependencia</th>
                                                    <th>Ubicación</th>
                                                    <th>Responsable</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr v-for="(activo,index) in listadoActivosCopy" :key="index" @click="selecAct(activo)">
                                                    <td>{{activo.placa}}</td>
                                                    <td>{{activo.nombre}}</td>
                                                    <td>{{activo.dependencia}}</td>
                                                    <td>{{activo.ubicacion}}</td>
                                                    <td>{{activo.name}}</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    </div>                       
                </div>

                </div>
            </article>
        </section>

		<script src="node_modules/read-excel-file/bundle/read-excel-file.min.js"></script>
		<script src="node_modules/xlsx/dist/xlsx.full.min.js"></script>
        <script src="Librerias/vue/vue.min.js"></script>
		<script src="Librerias/vue/axios.min.js"></script>
		<script type="module" src="activos_fijos/retirarActivo/crear/acti-retirarActivo.js"></script>

	</body>
</html>
