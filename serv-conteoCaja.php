<?php
    header("Cache-control: no-cache, no-store, must-revalidate");
    header("Content-Type: text/html;charset=utf8");
    require "comun.inc";
    require "funciones.inc";
    require 'funcionesSP.inc.php';
    session_start();
    $linkbd = conectar_v7();
    $linkbd -> set_charset("utf8");
    cargarcodigopag($_GET['codpag'],$_SESSION["nivel"]);
    date_default_timezone_set("America/Bogota");
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>:: IDEAL 10 - Servicios P&uacute;blicos</title>
    <link href="css/css2n.css" rel="stylesheet" type="text/css"/>
    <link href="css/css3n.css" rel="stylesheet" type="text/css"/>
    <link href="css/css4.css" rel="stylesheet" type="text/css"/>
    <link href="css/cssSP.css" rel="stylesheet" type="text/css"/>
    <script type="text/javascript" src="css/programas.js"></script>
    <script type="text/javascript" src="css/calendario.js"></script>
    <?php titlepag();?>

    <script>
        function despliegamodal2(_valor, _table)
			{
                document.getElementById('bgventanamodal2').style.visibility = _valor;
                var cliente = document.getElementById('cliente').value;
                
                if (_table == '')
                {
                    document.getElementById('ventana2').src = '';
                }
                else if(_table == '')
                {
                    document.getElementById('ventana2').src = '';
                }
            }

			function despliegamodalm(_valor,_tip,mensa,pregunta)
			{
				document.getElementById("bgventanamodalm").style.visibility=_valor;

				if(_valor=="hidden")
				{
					document.getElementById('ventanam').src="";
				}
				else
				{
					switch(_tip)
					{
						case "1":	
							document.getElementById('ventanam').src="ventana-mensaje1.php?titulos="+mensa;
						    break;

						case "2":	
							document.getElementById('ventanam').src="ventana-mensaje3.php?titulos="+mensa;
						    break;

						case "3":	
							document.getElementById('ventanam').src="ventana-mensaje2.php?titulos="+mensa;
						    break;

						case "4":	
							document.getElementById('ventanam').src="ventana-consulta1.php?titulos="+mensa+"&idresp="+pregunta;
						    break;	
					}
				}
			}

			//ventanas servicios publicos
            function respuestaModalBusqueda2()
			{
				
            }

			function funcionmensaje()
			{
			
			}

			function respuestaconsulta(pregunta)
			{
				switch(pregunta)
				{
					case "1":	
						document.form2.oculto.value='2';
						document.form2.submit();
					break;
				}
			}

			function guardar()
			{
				// if () 
                // {
				// 	despliegamodalm('visible','4','Esta Seguro de Guardar','1');   
                // }
				// else 
				// {

                //     despliegamodalm('visible','2','Falta información para crear el acuerdo.');
                // }
			}

			function actualizar()
			{
				document.form2.submit();
			}
    </script>
</head>
<body>
    <IFRAME src="alertas.php" name="alertas" id="alertas" style="display:none"></IFRAME>
    <span id="todastablas2"></span>
    <table>
        <tr><script>barra_imagenes("serv");</script><?php cuadro_titulos();?></tr>

        <tr><?php menu_desplegable("serv");?></tr>

        <tr>
            <td colspan="3" class="cinta">
                <a href="" class="mgbt"><img src="imagenes/add.png"/></a>

                <a onclick="guardar()" class="mgbt"><img src="imagenes/guarda.png" title="Guardar"/></a>

                <a href="" class="mgbt"><img src="imagenes/busca.png" title="Buscar"/></a>

                <a onclick="mypop=window.open('plan-agenda.php','','');mypop.focus()" class="mgbt"><img src="imagenes/agenda1.png" title="Agenda" /></a>

                <a onclick="mypop=window.open('serv-principal.php','','');mypop.focus();" class="mgbt"><img src="imagenes/nv.png" title="Nueva ventana"></a>

                <a onclick="mypop=window.open('<?php echo $url2; ?>','','');mypop.focus();" class="mgbt"><img src="imagenes/duplicar_pantalla.png" title="Duplicar pesta&ntilde;a"></a>

                <a href="" class="mgbt"><img src="imagenes/iratras.png" title="Atr&aacute;s"></a>
            </td>
        </tr>
    </table>

    <div id="bgventanamodalm" class="bgventanamodalm">
        <div id="ventanamodalm" class="ventanamodalm">
            <IFRAME src="" name="ventanam" marginWidth=0 marginHeight=0 frameBorder=0 id="ventanam" frameSpacing=0 style=" width:700px; height:130px; top:200; overflow:hidden;"></IFRAME>
        </div>
    </div>

    <form name="form2" method="post" action="">
        <?php 
            if(@$_POST['oculto'] == "")
            {				
                
            }
        ?>
        <div>
            <table class="inicio ancho">
                <tr>
                    <td class="titulos" colspan="6">.: EJEMPLO</td>

                    <td class="cerrar" style="width:7%" onclick="location.href='serv-principal.php'">Cerrar</td>
                </tr>
            </table>

            <input type="hidden" name="oculto" id="oculto" value="1"/>
        </div>
        <?php 
            if(@$_POST['oculto'] == "2")
            {
                
            }
        ?>
    </form>
    
    <div id="bgventanamodal2">
        <div id="ventanamodal2">
            <IFRAME  src="" name="buster" marginWidth=0 marginHeight=0 frameBorder=0 id="ventana2" frameSpacing=0 style="left:500px; width:900px; height:500px; top:200;"></IFRAME>
        </div>
    </div>
</body>
</html>