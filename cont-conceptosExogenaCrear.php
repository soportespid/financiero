<?php

    header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=utf8");
	require 'comun.inc';
	require 'funciones.inc';
	session_start();
    if(empty($_SESSION)){
        header("location: index.php");
    }
	$linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");
	cargarcodigopag($_GET['codpag'],$_SESSION["nivel"]);
	date_default_timezone_set("America/Bogota");
?>
<!DOCTYPE >
<html lang=es>
	<head>
    <meta charset="utf-8"/>
		<meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1"/>
		<meta name="viewport" content="user-scalable=no">
		<title>:: IDEAL 10 - Informes</title>
		<link href="favicon.ico" rel="shortcut icon"/>
		<link href="css/css2z.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
		<link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
		<link href="css/css4.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
        <link href="css/tabs.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css"/>
        <link href="css/style.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css"/>
        <link rel="stylesheet" href="sweetalert2/dist/sweetalert2.min.css">
        <script type="text/javascript" src="css/programas.js"></script>
        <script src="sweetalert2/dist/sweetalert2.min.js"></script>
		<script src="vue/vue.min.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script src="css/programas.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script src="css/calendario.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
	</head>
	<body>
		<header>
			<table>
				<tr><script>barra_imagenes("info");</script><?php cuadro_titulos();?></tr>
			</table>
		</header>

		<form name="form2" method="post" action="">
			<section id="myapp" v-cloak >
                <div id="cargando" v-show="isLoading" class="loading" style="z-index: 10000;">
                    <span>Cargando...</span>
                </div>
				<nav>
					<table>
						<tr><?php menu_desplegable("info");?></tr>
                        <tr>
							<td colspan="3" class="cinta">
                                <img src="imagenes/add.png"  class="mgbt" @click="window.location.reload()" title="Nuevo">
								<img src="imagenes/guarda.png"   title="Guardar" @click="save" class="mgbt">
								<img src="imagenes/busca.png" @click="window.location.href='cont-conceptosExogenaBuscar.php'"   class="mgbt" title="Buscar">
                                <a @click="mypop=window.open('plan-agenda.php','','');mypop.focus()" class="mgbt"><img src="imagenes/agenda1.png" title="Agenda" /></a>
								<img src="imagenes/nv.png" @click="mypop=window.open('info-principal.php','',''); mypop.focus();" class="mgbt" title="Nueva Ventana">
                                <a href="cont-conceptosExogenaBuscar.php"  class="mgbt"><img src="imagenes/iratras.png" title="Atr&aacute;s"></a>
                            </td>
						</tr>
					</table>
				</nav>
				<article>
                    <div class="inicio">
                        <div>
                            <h2 class="titulos m-0">.: Configurar conceptos exógena</h2>
                            <div class="d-flex">
                                <div class="form-control w-25">
                                    <label for="form-label">.: Formato <span class="text-danger fw-bolder">*</span></label>
                                    <select @change="getConceptos" v-model="selectFormato">
                                        <option selected value="0">Seleccione</option>
                                        <option v-for="(data,index) in arrFormatos" :key="index" :value="data.formato">
                                            {{ data.formato+" - "+data.nombre}}
                                        </option>

                                    </select>
                                </div>
                                <div class="form-control w-50">
                                    <label for="form-label">.: Concepto <span class="text-danger fw-bolder">*</span></label>
                                    <div class="d-flex">
                                        <select class="w-25" @change="getConceptoNombre" v-model="selectConcepto">
                                            <option selected value="0">Seleccione</option>
                                            <option v-for="(data,index) in arrConceptosFormato" :key="index" :value="data.codigo">
                                                {{ data.codigo+" - "+data.nombre}}
                                            </option>
                                        </select>
                                        <input type="text" v-model="objConcepto.nombre" disabled>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div>
                            <h2 class="titulos m-0">.: Asignar cuentas <span class="text-danger fw-bolder">*</span></h2>
                            <div class="d-flex">
                                <div class="form-control w-50">
                                    <label class="form-label" for="">.: Cuenta contable:</label>
                                    <div class="d-flex">
                                        <input v-model="objCuenta.cuenta" type="text" @change="search('cod_cuenta')"  @dblclick="isModal=true" class="w-25 colordobleclik">
                                        <input v-model="objCuenta.nombre" type="text" disabled readony>
                                    </div>
                                </div>
                                <div class="form-control w-25">
                                    <label class="form-label" for="">.: Tipo:</label>
                                    <div class="d-flex">
                                        <select v-model="selectTipo">
                                            <option value="1">Debito</option>
                                            <option value="2">Credito</option>
                                            <option value="3">Saldo</option>
                                            <option value="4">Debito/Credito</option>
                                        </select>
                                        <button type="button" class="btn btn-primary" @click="addItem">Agregar</button>
                                    </div>
                                </div>
                            </div>
                            <div class="overflow-auto max-vh-25 overflow-x-hidden p-2" >
                                <table class="table fw-normal">
                                    <thead>
                                        <tr>
                                            <th>Cuenta</th>
                                            <th>Nombre cuenta</th>
                                            <th>Tipo</th>
                                            <th>Opciones</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr v-for="(data,index) in arrCuentasDetalle" :key="index">
                                            <td>{{ data.cuenta}}</td>
                                            <td>{{ data.nombre}}</td>
                                            <td>{{ data.tipo}}</td>
                                            <td>
                                                <button type="button" class="btn btn-danger m-1" @click="delItem(data)">Eliminar</button>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
				</article>
                <!--MODALES-->
                <div v-show="isModal">
                    <transition name="modal">
                        <div class="modal-mask">
                            <div class="modal-wrapper">
                                <div class="modal-container">
                                    <table class="inicio ancho">
                                        <tr>
                                            <td class="titulos" colspan="2" >.: Buscar cuentas</td>
                                            <td class="cerrar" style="width:7%" @click="isModal = false">Cerrar</td>
                                        </tr>
                                    </table>
                                    <div class="bg-white">
                                        <div class="form-control m-0 p-2 w-inherit">
                                            <input type="search" placeholder="Buscar" @keyup="search('modal_cuenta')" v-model="txtSearch">
                                        </div>
                                        <p class="fw-bolder m-0 p-2">Resultados de búsqueda: {{txtResults}}</p>
                                        <div class="overflow-auto max-vh-50 overflow-x-hidden p-2">
                                            <table class="table table-hover fw-normal">
                                                <thead>
                                                    <tr>
                                                        <th>Cuenta</th>
                                                        <th>Descripción</th>
                                                        <th>Tipo</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr v-for="(data,index) in arrCuentasCopy" @dblclick="selectItem(data)" :key="index">
                                                        <td>{{data.cuenta}}</td>
                                                        <td>{{data.nombre}}</td>
                                                        <td>{{data.tipo}}</td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </transition>
                </div>
			</section>
		</form>

		<script src="node_modules/read-excel-file/bundle/read-excel-file.min.js"></script>
		<script src="node_modules/xlsx/dist/xlsx.full.min.js"></script>
        <script src="Librerias/vue/vue.min.js"></script>
		<script src="Librerias/vue/axios.min.js"></script>
		<script type="module" src="contabilidad_vue/exogena_conceptos/crear/cont-conceptosCrear.js?<?= date('d_m_Y_h_i_s');?>"></script>

	</body>
</html>
