<?php
	header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=utf8");
	require 'comun.inc';
	require 'funciones.inc';
	session_start();
	$linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");
	cargarcodigopag($_GET['codpag'],$_SESSION["nivel"]);
	date_default_timezone_set("America/Bogota");
?>
<!DOCTYPE >
<html lang=es>
	<head>
    <meta charset="utf-8"/>
		<meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1"/>
		<meta name="viewport" content="user-scalable=no">
		<title>:: IDEAL 10 - Servicios públicos</title>
		<link href="favicon.ico" rel="shortcut icon"/>
		<link href="css/css2z.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
		<link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
		<link href="css/css4.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
        <link href="css/tabs.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css"/>
        <link rel="stylesheet" href="sweetalert2/dist/sweetalert2.min.css">
        <script type="text/javascript" src="css/programas.js"></script>
        <script src="sweetalert2/dist/sweetalert2.min.js"></script>
		<script src="vue/vue.min.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script src="css/programas.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script src="css/calendario.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
	</head>
	<body>
		<header>
			<table>
				<tr><script>barra_imagenes("serv");</script><?php cuadro_titulos();?></tr>
			</table>
		</header>

		<form name="form2" method="post" action="">
			<section id="myapp" v-cloak >
				<nav>
					<table>
						<tr><?php menu_desplegable("serv");?></tr>
						<tr>
							<td colspan="3" class="cinta">
								<img src="imagenes/add.png" v-on:click="location.href=''" class="mgbt" title="Nuevo">
								<img src="imagenes/guarda.png" @click="" title="Guardar"  class="mgbt">
								<img src="imagenes/busca.png" v-on:click="location.href=''" class="mgbt" title="Buscar">
								<img src="imagenes/nv.png" onClick="mypop=window.open('serv-principal','',''); mypop.focus();" class="mgbt" title="Nueva Ventana">
								<a @click="printExcel" class="mgbt"><img src="imagenes/excel.png" title="Excel"></a>
								<!-- <a @click="printPDF" class="mgbt"><img src="imagenes/print.png" title="Imprimir" /></a> -->
								<a href="serv-menuInformes"><img src="imagenes/iratras.png" class="mgbt" alt="Atrás"></a>
							</td>
						</tr>
					</table>
				</nav>

				<article>
                    <table class="inicio">
                        <tr>
                            <td class="titulos" colspan="8">.: Subsidios de facturación por usuario:</td>
                            <td class="cerrar" style="width:4%" onClick="location.href='serv-recaudoFactura'">Cerrar</td>
                        </tr>

                        <tr>
							<td class="textonew01" style="width:5cm;">.: Periodo liquidado:</td>
							<td>
								<select v-model="periodo" style="font-size: 15px; margin-top:4px; width: 100%">
									<option value="">Seleccionar periodo</option>
									<option v-for="periodo in periodos" v-bind:value="periodo[0]">
										{{ periodo[0] }} - {{ periodo[1] }} {{ periodo[3] }} - {{ periodo[2] }} {{ periodo[4] }}
									</option>
								</select>
							</td>
							<td style="padding-bottom:0px;height:35px;"><em class="botonflecha" @click="buscaDatos">Generar Reporte</em></td>
                        </tr>
                    </table>

                    <div class='subpantalla' style='height:66vh; width:99.2%; margin-top:0px;'>  
                        <table class=''>
							<thead>
								<tr style="text-align:center;">
									<th class="titulosnew00">Código de usuario</th>
									<th class="titulosnew00">Estrato</th>
									<th class="titulosnew00">Número de factura</th>
									<th class="titulosnew00">Subsidio o contribución AC</th>
									<th class="titulosnew00">Subsidio o contribución ALC</th>
									<th class="titulosnew00">Subsidio o contribución ASEO</th>
								</tr>
							</thead>
							<tbody>
								<tr v-for="(data,index) in arrData" v-bind:class="index % 2 ? 'saludo1a' : 'saludo2'">			
									<td style="text-align:center">{{data.codUsuario}}</td>
									<td style="text-align:center">{{data.estrato}}</td>
									<td style="text-align:center">{{data.numFactura}}</td>
									<td style="text-align:center">{{formatonumero(data.acueducto)}}</td>
									<td style="text-align:center">{{formatonumero(data.alcantarillado)}}</td>
									<td style="text-align:center">{{formatonumero(data.aseo)}}</td>

									<input type='hidden' name='codUsuario[]' v-model="data.codUsuario">	
									<input type='hidden' name='estrato[]' v-model="data.estrato">	
									<input type='hidden' name='numFactura[]' v-model="data.numFactura">	
									<input type='hidden' name='acueducto[]' v-model="data.acueducto">	
									<input type='hidden' name='alcantarillado[]' v-model="data.alcantarillado">	
									<input type='hidden' name='aseo[]' v-model="data.aseo">	
								</tr>
							</tbody>
						</table>
                    </div>
				</article>

				<div id="cargando" v-if="loading" class="loading">
					<span>Cargando...</span>
				</div>

			</section>
		</form>

		<script src="node_modules/read-excel-file/bundle/read-excel-file.min.js"></script>
		<script src="node_modules/xlsx/dist/xlsx.full.min.js"></script>
        <script src="Librerias/vue/vue.min.js"></script>
		<script src="Librerias/vue/axios.min.js"></script>
		<script type="module" src="servicios_publicos/informeSubsidios/serv-informeSubsidiosPorUsuario.js"></script>

	</body>
</html>
