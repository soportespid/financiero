<?php
	header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=uf8");
	require "comun.inc";
	require "funciones.inc";
	require "funcionesSP.inc.php";
	session_start();
	$linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");
	cargarcodigopag($_GET['codpag'],$_SESSION["nivel"]);
	date_default_timezone_set("America/Bogota");
    titlepag();
?>
<!DOCTYPE >
<html lang="en">
	<head>
		<meta charset="UTF-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
		<title>:: IDEAL 10 - Almacen</title>
		<link href="css/css2.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css">
		<link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css">
		<link href="css/css4.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css">
		<link href="css/cssSP.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css">
		<link href="css/style.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css"/>
		<script src="sweetalert2/dist/sweetalert2.min.js"></script>
        <link rel="stylesheet" href="sweetalert2/dist/sweetalert2.min.css">
		<script type="text/javascript" src="css/calendario.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script type='text/javascript' src='JQuery/jquery-2.1.4.min.js'></script>
		<script type="text/javascript" src="css/programas.js"></script>
		<script>
			function visualizar(codigo, tipomov)
			{
				location.href="inve-visualizar-salidaDirecta.php?consec="+codigo+"&mov="+tipomov;
			}

			function excel()
			{
				document.form2.action="serv-excel-recaudo.php";
				document.form2.target="_BLANK";
				document.form2.submit();
				document.form2.action="";
				document.form2.target="";
			}

		</script>

		<?php

			$scrtop = @ $_GET['scrtop'];
			if($scrtop == "") $scrtop=0;
			echo"<script>
					window.onload=function()
					{
						$('#divdet').scrollTop(".$scrtop.")
					}
				</script>";
		?>
	</head>
	<body>
		<IFRAME src="alertas.php" name="alertas" id="alertas" style="display:none"></IFRAME>
		<span id="todastablas2"></span>
        <nav>
            <table>
                <tr><script>barra_imagenes("inve");</script><?php cuadro_titulos();?></tr>
                <tr><?php menu_desplegable("inve");?></tr>
            </table>
            <div class="bg-white group-btn p-1">
                <button type="button" onclick="window.location.href='inve-salidaDirecta'" class="btn btn-white btn-primary-hover d-flex justify-between align-items-center">
                    <span>Nuevo</span>
                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 -960 960 960"><path d="M440-440H200v-80h240v-240h80v240h240v80H520v240h-80v-240Z"></path></svg>
                </button>
                <button type="button" onclick="window.location.href='inve-buscar-salidasDirectas'" class="btn btn-white btn-primary-hover d-flex justify-between align-items-center">
                    <span>Buscar</span>
                    <svg class="fill-black group-hover:fill-white w-5 h-5" xmlns="http://www.w3.org/2000/svg" viewBox="0 -960 960 960"><path d="M784-120 532-372q-30 24-69 38t-83 14q-109 0-184.5-75.5T120-580q0-109 75.5-184.5T380-840q109 0 184.5 75.5T640-580q0 44-14 83t-38 69l252 252-56 56ZM380-400q75 0 127.5-52.5T560-580q0-75-52.5-127.5T380-760q-75 0-127.5 52.5T200-580q0 75 52.5 127.5T380-400Z"></path></svg>
                </button>
                <button type="button" onclick="mypop=window.open('plan-agenda','','');mypop.focus()" class="btn btn-white btn-primary-hover d-flex justify-between align-items-center">
                    <span class="group-hover:text-white">Agenda</span>
                    <svg class="fill-black group-hover:fill-white w-5 h-5" xmlns="http://www.w3.org/2000/svg" viewBox="0 -960 960 960"><path d="M560-564v-68q33-14 67.5-21t72.5-7q26 0 51 4t49 10v64q-24-9-48.5-13.5T700-600q-38 0-73 9.5T560-564Zm0 220v-68q33-14 67.5-21t72.5-7q26 0 51 4t49 10v64q-24-9-48.5-13.5T700-380q-38 0-73 9t-67 27Zm0-110v-68q33-14 67.5-21t72.5-7q26 0 51 4t49 10v64q-24-9-48.5-13.5T700-490q-38 0-73 9.5T560-454ZM260-320q47 0 91.5 10.5T440-278v-394q-41-24-87-36t-93-12q-36 0-71.5 7T120-692v396q35-12 69.5-18t70.5-6Zm260 42q44-21 88.5-31.5T700-320q36 0 70.5 6t69.5 18v-396q-33-14-68.5-21t-71.5-7q-47 0-93 12t-87 36v394Zm-40 118q-48-38-104-59t-116-21q-42 0-82.5 11T100-198q-21 11-40.5-1T40-234v-482q0-11 5.5-21T62-752q46-24 96-36t102-12q58 0 113.5 15T480-740q51-30 106.5-45T700-800q52 0 102 12t96 36q11 5 16.5 15t5.5 21v482q0 23-19.5 35t-40.5 1q-37-20-77.5-31T700-240q-60 0-116 21t-104 59ZM280-494Z"></path></svg>
                </button>
                <button type="button" onclick="mypop=window.open('inve-principal','','');mypop.focus();" class="btn btn-white btn-primary-hover d-flex justify-between align-items-center">
                    <span>Nueva ventana</span>
                    <svg class="fill-black group-hover:fill-white w-5 h-5" xmlns="http://www.w3.org/2000/svg" viewBox="0 -960 960 960"><path d="M320-120v-80h80v-80H160q-33 0-56.5-23.5T80-360v-400q0-33 23.5-56.5T160-840h640q33 0 56.5 23.5T880-760v400q0 33-23.5 56.5T800-280H560v80h80v80H320ZM160-360h640v-400H160v400Zm0 0v-400 400Z"></path></svg>
                </button>
                <button type="button" onclick="excel()" class="btn btn-success btn-success-hover d-flex justify-between align-items-center">
                    <span>Exportar Excel</span>
                    <svg class="fill-black group-hover:fill-white w-4 h-4" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 384 512"><!--!Font Awesome Free 6.5.1 by @fontawesome - https://fontawesome.com License - https://fontawesome.com/license/free Copyright 2024 Fonticons, Inc. --><path d="M48 448V64c0-8.8 7.2-16 16-16H224v80c0 17.7 14.3 32 32 32h80V448c0 8.8-7.2 16-16 16H64c-8.8 0-16-7.2-16-16zM64 0C28.7 0 0 28.7 0 64V448c0 35.3 28.7 64 64 64H320c35.3 0 64-28.7 64-64V154.5c0-17-6.7-33.3-18.7-45.3L274.7 18.7C262.7 6.7 246.5 0 229.5 0H64zm90.9 233.3c-8.1-10.5-23.2-12.3-33.7-4.2s-12.3 23.2-4.2 33.7L161.6 320l-44.5 57.3c-8.1 10.5-6.3 25.5 4.2 33.7s25.5 6.3 33.7-4.2L192 359.1l37.1 47.6c8.1 10.5 23.2 12.3 33.7 4.2s12.3-23.2 4.2-33.7L222.4 320l44.5-57.3c8.1-10.5 6.3-25.5-4.2-33.7s-25.5-6.3-33.7 4.2L192 280.9l-37.1-47.6z"></path></svg>
                </button>
                <button type="button" onclick="mypop=window.open('inve-buscar-salidasDirectas','','');mypop.focus();" class="btn btn-white btn-primary-hover d-flex justify-between align-items-center">
                    <span class="group-hover:text-white">Duplicar pantalla</span>
                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 -960 960 960"><path d="M640-160v-360H160v360h480Zm80-200v-80h80v-360H320v200h-80v-200q0-33 23.5-56.5T320-880h480q33 0 56.5 23.5T880-800v360q0 33-23.5 56.5T800-360h-80ZM160-80q-33 0-56.5-23.5T80-160v-360q0-33 23.5-56.5T160-600h480q33 0 56.5 23.5T720-520v360q0 33-23.5 56.5T640-80H160Zm400-603ZM400-340Z"></path></svg>
                </button>
                <button type="button" onclick="window.location.href='inve-menuSalidas'" class="btn btn-success btn-success-hover d-flex justify-between align-items-center">
                    <span>Atras</span>
                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 -960 960 960"><path d="M760-200v-160q0-50-35-85t-85-35H273l144 144-57 56-240-240 240-240 57 56-144 144h367q83 0 141.5 58.5T840-360v160h-80Z"></path></svg>
                </button>
            </div>
        </nav>


		<?php
			if(@ $_POST['oculto'] == "")
			{
				$_POST['numres'] = 10;
				$_POST['numpos'] = 0;
				$_POST['nummul'] = 0;
			}

			if(@ $_GET['numpag'] != "")
			{
				if(@ $_POST['oculto'] != 2)
				{
					$_POST['numres'] = $_GET['limreg'];
					$_POST['numpos'] = $_GET['limreg'] * ($_GET['numpag']-1);
					$_POST['nummul'] = $_GET['numpag'] - 1;
				}
			}
			else
			{
				if(@ $_POST['nummul'] == "")
				{
					$_POST['numres'] = 10;
					$_POST['numpos'] = 0;
					$_POST['nummul'] = 0;
				}
			}
		?>

		<div id="bgventanamodalm" class="bgventanamodalm">
			<div id="ventanamodalm" class="ventanamodalm">
				<IFRAME src="" name="ventanam" marginWidth=0 marginHeight=0 frameBorder=0 id="ventanam" frameSpacing=0 style=" width:700px; height:130px; top:200; overflow:hidden;"></IFRAME>
			</div>
		</div>

		<form name="form2" method="post" action="">

			<input type="hidden" name="numres" id="numres" value="<?php echo @ $_POST['numres'];?>"/>
			<input type="hidden" name="numpos" id="numpos" value="<?php echo @ $_POST['numpos'];?>"/>
			<input type="hidden" name="nummul" id="nummul" value="<?php echo @ $_POST['nummul'];?>"/>
			<input type="hidden" name="oculto" id="oculto" value="1">
			<input type="hidden" name="cambioestado" id="cambioestado" value="<?php echo @ $_POST['cambioestado'];?>">
			<input type="hidden" name="nocambioestado" id="nocambioestado" value="<?php echo @ $_POST['nocambioestado'];?>">
			<input type="hidden" name="idestado" id="idestado" value="<?php echo @ $_POST['idestado'];?>">

			<table class="inicio">
				<tr>
					<td class="titulos" colspan="10">Buscar salidas directas</td>
					<td class="cerrar" style="width:7%" onClick="location.href='inve-principal.php'">Cerrar</td>
				</tr>

				<tr>
				    <td class="tamano01">Consecutivo: </td>
					<td>
                        <input type="search" name="busqueda_codigos" id="busqueda_codigos" value="<?php echo @$_POST['busqueda_codigos'];?>" style='width:100%;height:30px; text-align:center;' onkeydown = "if (event.keyCode == 13){document.getElementById('filtro').click()}";/>
                    </td>

					<td  class="tamano01" >Fecha Inicial: </td>
					<td style="text-align: center;">
						<input type="search" name="fechaini" id="fc_1198971545" title="DD/MM/YYYY" value="<?php echo $_POST['fechaini']; ?>" onKeyUp="return tabular(event,this)" onKeyDown="mascara(this,'/',patron,true)" maxlength="10" class="colordobleclik" onDblClick="displayCalendarFor('fc_1198971545');" autocomplete="off" onChange="">
					</td>

					<td class="tamano01" >Fecha Final: </td>
					<td style="text-align: center;">
						<input type="search" name="fechafin"  id="fc_1198971546" title="DD/MM/YYYY"  value="<?php echo $_POST['fechafin']; ?>" onKeyUp="return tabular(event,this) " onKeyDown="mascara(this,'/',patron,true)" maxlength="10" class="colordobleclik" onDblClick="displayCalendarFor('fc_1198971546');" autocomplete="off" onChange="">
					</td>

                    <td style="padding-bottom:0px">
                        <em class="botonflechaverde" id="filtro" onClick="limbusquedas();">Buscar</em>
                    </td>
				</tr>
			</table>

			<?php
				if ($_POST['busqueda_codigos']!='')
                {
                    $crit1 = "AND consec = '$_POST[busqueda_codigos]' ";
                }

				if ($_POST['fechaini'] != "" and $_POST['fechafin'] != "")
				{
					preg_match("/([0-9]{2})\/([0-9]{2})\/([0-9]{4})/", $_POST['fechaini'],$fecha);
					$fechai = $fecha[3]."-".$fecha[2]."-".$fecha[1];

					preg_match("/([0-9]{2})\/([0-9]{2})\/([0-9]{4})/", $_POST['fechafin'],$fecha);
					$fechaf = $fecha[3]."-".$fecha[2]."-".$fecha[1];

					$crit2 = "AND fecha BETWEEN '$fechai' AND '$fechaf'  ";
				}
				else
				{
					if(($_POST['fechaini'] != '' AND $_POST['fechafin'] == '') OR ($_POST['fechaini'] == '' AND $_POST['fechafin'] != ''))
					{
						echo "
							<script>
								Swal.fire({
									icon: 'error',
									title: 'Se deben colocar la fecha inicial y final para usar el filtro'
								})
							</script>
						";
					}
				}

				$cond2 = "";

				if (@ $_POST['numres'] != "-1")
				{
					$cond2 = " LIMIT ".$_POST['numpos'].", ".$_POST['numres'];
				}

				$sqlr = "SELECT consec, fecha, nombre, usuario, bodega1, tipomov FROM almginventario WHERE (tipomov = '2' OR tipomov = '04') AND tiporeg = '06' $crit1 $crit2 ORDER BY codigo DESC";
				$resp = mysqli_query($linkbd,$sqlr);

				$_POST['numtop'] = mysqli_num_rows($resp);
				$nuncilumnas = ceil($_POST['numtop'] / $_POST['numres']);

				$sqlr = "SELECT consec, fecha, nombre, usuario, bodega1, tipomov FROM almginventario WHERE (tipomov = '2' OR tipomov = '04') AND tiporeg = '06' $crit1 $crit2 ORDER BY codigo DESC $cond2";
				$resp = mysqli_query($linkbd,$sqlr);

				$con = 1;

				$numcontrol = $_POST['nummul'] + 1;

				if(($nuncilumnas == $numcontrol) || (@$_POST['numres'] == "-1"))
				{
					$imagenforward = "<img src='imagenes/forward02.png' style='width:17px;cursor:default;'>";
					$imagensforward = "<img src='imagenes/skip_forward02.png' style='width:16px;cursor:default;' >";
				}
				else
				{
					$imagenforward = "<img src='imagenes/forward01.png' style='width:17px;cursor:pointer;' title='Siguiente' onClick='numsiguiente()'>";
					$imagensforward = "<img src='imagenes/skip_forward01.png' style='width:16px;cursor:pointer;' title='Fin' onClick='saltocol(\"$nuncilumnas\")'>";
				}
				if((@$_POST['numpos'] == 0) || (@$_POST['numres'] == "-1"))
				{
					$imagenback = "<img src='imagenes/back02.png' style='width:17px;cursor:default;'>";
					$imagensback = "<img src='imagenes/skip_back02.png' style='width:16px;cursor:default;'>";
				}
				else
				{
					$imagenback = "<img src='imagenes/back01.png' style='width:17px;cursor:pointer;' title='Anterior' onClick='numanterior();'>";
					$imagensback = "<img src='imagenes/skip_back01.png' style='width:16px;cursor:pointer;' title='Inicio' onClick='saltocol(\"1\")'>";
				}

				$con = 1;
            ?>
                <div class="subpantalla" style="height:60%; width:99.6%; margin-top:0px; overflow-x:hidden">
					<table class='inicio'>
						<tr>
							<td colspan='5' class='titulos'>.: Resultados Busqueda: <label style='float:right;'></td>
							    <td class='submenu'>
                                    <select name='renumres' id='renumres' onChange='cambionum();' style='width:100%' class='centrarSelect'>
                                        <option class='centrarOption' value='10' <?php if (@$_POST['renumres']=='10'){echo 'selected';} ?>>10</option>
                                        <option class='centrarOption' value='20' <?php if (@$_POST['renumres']=='20'){echo 'selected';} ?>>20</option>
                                        <option class='centrarOption' value='30' <?php if (@$_POST['renumres']=='30'){echo 'selected';} ?>>30</option>
                                        <option class='centrarOption' value='50' <?php if (@$_POST['renumres']=='50'){echo 'selected';} ?>>50</option>
                                        <option class='centrarOption' value='100' <?php if (@$_POST['renumres']=='100'){echo 'selected';} ?>>100</option>
                                        <option class='centrarOption' value='-1' <?php if (@$_POST['renumres']=='-1'){echo 'selected';} ?>>Todos</option>
                                    </select>
						        </td>
                            </td>
						</tr>
						<tr>
							<td colspan='8'>Salidas directas encontradas: <?php echo $_POST['numtop'] ?></td>
						</tr>

						<tr class="titulos2" style='text-align:center;'>
							<td style="width: 10%;">Consecutivo </td>
							<td style="width: 10%;">Fecha </td>
							<td>Descripción </td>
							<td>Bodega </td>
							<td>Realiza </td>
							<td style="width: 10%;">Visualizar </td>
						</tr>
            <?php
						$iter = 'saludo1a';
						$iter2 = 'saludo2';
						$filas = 1;

						while ($row = mysqli_fetch_assoc($resp))
						{
							$con2 = $con + $_POST['numpos'];

							$sqlUsuarios = "SELECT usu_usu FROM usuarios WHERE cc_usu = '$row[usuario]'";
							$rowUsuarios = mysqli_fetch_row(mysqli_query($linkbd, $sqlUsuarios));

							$sqlBodegas = "SELECT nombre FROM almbodegas WHERE id_cc = '$row[bodega1]'";
							$rowBodegas = mysqli_fetch_row(mysqli_query($linkbd, $sqlBodegas));
                    ?>
							<tr class='<?php echo $iter ?>' ondblclick="visualizar(<?php echo $row['consec']; ?>, <?php echo $row['tipomov']; ?>)" style='text-transform:uppercase; <?php echo $estilo ?>; text-align:center;'>
								<td> <?php echo $row['consec'] ?> </td>
								<td> <?php echo date('d-m-Y',strtotime($row['fecha'])) ?> </td>
								<td> <?php echo $row['nombre'] ?> </td>
								<td> <?php echo $rowBodegas[0] ?> </td>
								<td> <?php echo $rowUsuarios[0] ?> </td>
								<td style='text-align:center;'>
                                    <img src='imagenes/lupa02.png' style='width:20px' class='icoop' title='Ver' onclick="visualizar(<?php echo $row['consec']; ?>, <?php echo $row['tipomov']; ?>)" />
                                </td>
							</tr>
                    <?php
							$con += 1;
							$aux = $iter;
							$iter = $iter2;
							$iter2 = $aux;
							$filas++;
						}

						if (@$_POST['numtop'] == 0)
						{
                    ?>
							<table class='inicio'>
								<tr>
									<td class='saludo1' style='text-align:center;width:100%'><img src='imagenes\alert.png' style='width:25px'>No hay coincidencias en la búsqueda<img src='imagenes\alert.png' style='width:25px'></td>
								</tr>
							</table>
                    <?php
						}
						echo"
					</table>

					<table class='inicio'>
						<tr>
							<td style='text-align:center;'>
								<a>$imagensback</a>&nbsp;
								<a>$imagenback</a>&nbsp;&nbsp;";

				if($nuncilumnas <= 9)
				{
					$numfin = $nuncilumnas;
				}
				else
				{
					$numfin = 9;
				}
				for($xx = 1; $xx <= $numfin; $xx++)
				{
					if($numcontrol <= 9)
					{
						$numx = $xx;
					}
					else
					{
						$numx = $xx + ($numcontrol - 9);
					}

					if($numcontrol == $numx)
					{
						echo"<a onClick='saltocol(\"$numx\")'; style='color:#24D915;cursor:pointer;'> $numx </a>";
					}
					else
					{
						echo"<a onClick='saltocol(\"$numx\")'; style='color:#000000;cursor:pointer;'> $numx </a>";
					}
				}
				echo"			&nbsp;&nbsp;<a>$imagenforward</a>
								&nbsp;<a>$imagensforward</a>
							</td>
						</tr>
					</table>";
			?>

			<input type="hidden" name="ocules" id="ocules" value="<?php echo @ $_POST['ocules'];?>"/>
			<input type="hidden" name="actdes" id="actdes" value="<?php echo @ $_POST['actdes'];?>"/>
			<input type="hidden" name="numtop" id="numtop" value="<?php echo @ $_POST['numtop'];?>"/>

		</form>

		<div id="bgventanamodal2">
			<div id="ventanamodal2">
				<IFRAME  src="" name="buster" marginWidth=0 marginHeight=0 frameBorder=0 id="ventana2" frameSpacing=0 style="left:500px; width:900px; height:500px; top:200;"></IFRAME>
			</div>
		</div>

	</body>
</html>
