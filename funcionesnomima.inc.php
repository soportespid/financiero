<?php
	function terceroparafiscal ($codigo,$vigencia){
		$linkbd = conectar_v7();
		$sqlr = "SELECT cajacompensacion, icbf, sena, iti, esap, arp FROM humparametrosliquida";
		$res = mysqli_query($linkbd, $sqlr);
		$row = mysqli_fetch_row($res);
		switch($codigo){
			case $row[0]: $vpara = 'cajas'; break;//CCF
			case $row[1]: $vpara = 'icbf'; break;//icbf
			case $row[2]: $vpara = 'sena'; break;//sena
			case $row[3]: $vpara = 'iti'; break;//institutos tecnicos
			case $row[4]: $vpara = 'esap'; break;//esap
			case $row[5]: $vpara = 'indiceinca'; break;//arl
			default:$vpara = '';//otro codigo que no es parafiscal
		}
		if ($vpara != '' && $vigencia !=''){
			$sqlr = "SELECT $vpara FROM admfiscales WHERE vigencia = '$vigencia'";
			$res = mysqli_query($linkbd, $sqlr);
			$row = mysqli_fetch_row($res);
			return $row[0];
		}
	}
	function traercuentapresu2($tipo,$cc,$vigencia){//ANTIGUO PRESUPUESTO
		$linkbd = conectar_v7();
		$sqlr = "SELECT cuentapres FROM humvariables_det WHERE codigo='$tipo' AND cc='$cc' AND estado='S' AND vigencia='$vigencia'";
		$res = mysqli_query($linkbd,$sqlr);
		$r = mysqli_fetch_row($res);
		return $r[0];
	}
	function traercuentapresu($tipo,$cc,$vigencia,$sector){//ANTIGUO PRESUPUESTO
		$linkbd = conectar_v7();
		if($sector != ''){
			if($sector=='PR'){
				$tpf=  'privado';
			}else{
				$tpf = 'publico';
			}
			$adic = "AND sector='$tpf'";
		}else{
			$adic = "";
		}
		$sqlr = "SELECT cuentapres FROM humparafiscales_det WHERE codigo='$tipo' AND cc='$cc' AND estado='S' AND vigencia='$vigencia' $adic";
		$res = mysqli_query($linkbd,$sqlr);
		$r = mysqli_fetch_row($res);
		return $r[0];
	}
	function buscaporcentajeparafiscal($codigo, $tipo){//ANTIGUO PRESUPUESTO
		$linkbd = conectar_v7();
		$sqlr = "SELECT porcentaje FROM humparafiscales WHERE tipo='$tipo' AND estado='S' AND codigo='$codigo'";
		$resp = mysqli_query($linkbd,$sqlr);
		$row = mysqli_fetch_row($resp);
		return $row[0];
	}
	function buscavariblespagonomina($codigo){//ANTIGUO PRESUPUESTO
		$linkbd = conectar_v7();
		$sql = "SELECT nombre FROM humvariables WHERE codigo='$codigo'";
		$res = mysqli_query($linkbd,$sql);
		$row = mysqli_fetch_row($res);
		return $row[0];
	}
	function buscavariablepagov($codigo,$cc,$vigencia){//ANTIGUO PRESUPUESTO
		$linkbd = conectar_v7();
		$sqlr = "SELECT cuentapres FROM humvariables_det WHERE codigo='$codigo' AND cc='$cc' AND vigencia=$vigencia AND estado='S'";
		$res = mysqli_query($linkbd,$sqlr);
		$r = mysqli_fetch_row($res);
		return $r[0];
	}
	function nombrevariblespagonomina($codigo){
		$linkbd = conectar_v7();
		$linkbd -> set_charset("utf8");
		$sql = "SELECT nombre FROM ccpethumvariables WHERE codigo='$codigo'";
		$res = mysqli_query($linkbd,$sql);
		$row = mysqli_fetch_row($res);
		return $row[0];
	}
	function buscarnombreparafiscal($codigo){
		$linkbd = conectar_v7();
		$linkbd -> set_charset("utf8");
		$sqlr = "SELECT nombre FROM humparafiscalesccpet WHERE estado='S' AND codigo='$codigo'";
		$resp = mysqli_query($linkbd,$sqlr);
		$row = mysqli_fetch_row($resp);
		return $row[0];
	}
	function buscaporcentajeparafiscalglobal($codigo){
		$linkbd = conectar_v7();
		$sqlr = "SELECT porcentaje FROM humparafiscalesccpet WHERE estado='S' AND codigo='$codigo'";
		$resp = mysqli_query($linkbd,$sqlr);
		$row = mysqli_fetch_row($resp);
		return $row[0];
	}
	function buscaporcentajeparafiscalindividual($codfun, $tipo){
		$linkbd = conectar_v7();
		switch ($tipo){
			case 'SF': $nompara="item = 'PORSF'";break;
			case 'SE': $nompara="item = 'PORSE'";break;
			case 'PF': $nompara="item = 'PORPF'";break;
			case 'PE': $nompara="item = 'PORPE'";break;
			case 'CCF': $nompara="item = 'PORCCF'";break;
			case 'ICBF': $nompara="item = 'PORICBF'";break;
			case 'SENA': $nompara="item = 'PORSENA'";break;
			case 'INTE': $nompara="item = 'PORINTE'";break;
			case 'ESAP': $nompara="item = 'PORESAP'";break;
		}
		$sqlrfun = "
		SELECT  GROUP_CONCAT(descripcion ORDER BY valor SEPARATOR '<->')
		FROM hum_funcionarios 
		WHERE ($nompara) AND estado='S' AND codfun='$codfun'
		GROUP BY codfun
		ORDER BY CONVERT(codfun, SIGNED INTEGER)";
		$resp = mysqli_query($linkbd,$sqlrfun);
		$row = mysqli_fetch_row($resp);
		return $row[0];
	}
	function buscacuentapresupuestalccp($tipo,$codfun,$modo,$mpension){ //codigo viejo
		$linkbd = conectar_v7();
		$tipmov = $tipopresupuesto = '';
		$sqlrfun = "
		SELECT  GROUP_CONCAT(descripcion ORDER BY valor SEPARATOR '<->')
		FROM hum_funcionarios 
		WHERE (item = 'TVINCULACION' OR item = 'TPRESUPUESTO' OR item = 'TPRESUPUESTOPARA') AND estado='S' AND codfun='$codfun'
		GROUP BY codfun
		ORDER BY CONVERT(codfun, SIGNED INTEGER)";
		$respfun = mysqli_query($linkbd,$sqlrfun);
		$rowfun = mysqli_fetch_row($respfun);
		$tipos = explode('<->', $rowfun[0]);
		if($modo == '1'){
			$tipopresupuesto = $tipos[1];
			if($tipos[0] == 'T'){
				if($tipos[1]=='F'){
					$sqlr1 = "SELECT funtemporal FROM ccpethumvariables WHERE codigo='$tipo'";
					$tipmov = 'FT';
				}else {
					$sqlr1 = "SELECT invtemporal FROM ccpethumvariables WHERE codigo='$tipo'";
					$tipmov = 'IT';
				}
			}else{
				if($tipos[1]=='F'){
					$sqlr1 = "SELECT funcionamiento FROM ccpethumvariables WHERE codigo='$tipo'";
					$tipmov = 'F';
				}else{
					$sqlr1 = "SELECT inversion FROM ccpethumvariables WHERE codigo='$tipo'";
					$tipmov = 'IN';
				}
			}
		}else{
			//if($mpension=='PR'){$mpension='privado';}
			//elseif($mpension=='PB'){$mpension='publico';}
			if($mpension != 'N/A'){
				$mpension = 'privado';
			}
			$tipopresupuesto = $tipos[2];
			if($tipos[0]=='T'){
				if($tipos[2]=='F'){
					$sqlr1 = "SELECT funcionamientotemp FROM humparafiscalesccpet_det WHERE codigo='$tipo' AND sector='$mpension' AND estado='S'";
					$tipmov = 'FT';
				}else{
					$sqlr1 = "SELECT inversiontemp FROM humparafiscalesccpet_det WHERE codigo='$tipo' AND sector='$mpension' AND estado='S'";
					$tipmov = 'IT';
				}
			}else{
				if($tipos[2]=='F'){
					$sqlr1 = "SELECT funcionamiento FROM humparafiscalesccpet_det WHERE codigo='$tipo' AND sector='$mpension' AND estado='S'";
					$tipmov = 'F';
				}
				else 
				{
					$sqlr1 = "SELECT inversion FROM humparafiscalesccpet_det WHERE codigo='$tipo' AND sector='$mpension' AND estado='S'";
					$tipmov = 'IN';
				}
			}
		}
		$res1 = mysqli_query($linkbd,$sqlr1);
		$row1 = mysqli_fetch_row($res1);
		//$sqlr2="SELECT cuenta_p FROM ccpetcuentas_concepto_contable WHERE concepto_cont='$row1[0]' AND tipo='$tipmov' AND estado='S'";
		$sqlr2 = "SELECT cuenta_p FROM ccpetcuentas_concepto_contable WHERE concepto_nom = '$row1[0]' AND tipo = '$tipmov' AND estado = 'S'";
		$res2 = mysqli_query($linkbd,$sqlr2);
		$row2 = mysqli_fetch_row($res2);
		if($row2[0]==''){
			//echo "$codfun($tipo)($tipos[0])($tipmov)($row2[0])($tipos[2])($modo) - ";
		}
		if($tipopresupuesto=='F'){
			return $row2[0];
		}else{
			$sqlrfun2 = "
			SELECT GROUP_CONCAT(descripcion ORDER BY CONVERT(codrad, SIGNED INTEGER) SEPARATOR '<->')
			FROM hum_funcionarios 
			WHERE (item = 'PROYECTOINV') AND estado='S' AND codfun='$codfun'
			GROUP BY codfun
			ORDER BY CONVERT(codfun, SIGNED INTEGER)";
			$respfun2 = mysqli_query($linkbd,$sqlrfun2);
			$rowfun2 = mysqli_fetch_row($respfun2);
			if($rowfun2[0]!=''){
				return $row2[0]."<_>".$rowfun2[0];
			}else {
				return $row2[0]."<_>NO";
			}
		}
	}
	function porcentajearl($codfun){
		$linkbd = conectar_v7();
		$nivel = itemfuncionarios($codfun,'27');
		$sqlr = "SELECT tarifa FROM hum_nivelesarl WHERE id='$nivel'";
		$res = mysqli_query($linkbd,$sqlr);
		$r = mysqli_fetch_row($res);
		return $r[0];
	}
	function porcentajeparafiscales($codfun,$parafiscal){
		$linkbd = conectar_v7();
		$sqlrfun="
		SELECT  GROUP_CONCAT(descripcion ORDER BY valor SEPARATOR '<->')
		FROM hum_funcionarios 
		WHERE (item = 'NIVELARL') AND estado='S'  AND codfun='$codfun'
		GROUP BY codfun
		ORDER BY CONVERT(codfun, SIGNED INTEGER)";
		$respfun = mysqli_query($linkbd,$sqlrfun);
		$rowfun = mysqli_fetch_row($respfun);
		$sqlr = "SELECT tarifa FROM hum_nivelesarl WHERE id='$rowfun[0]'";
		$res = mysqli_query($linkbd,$sqlr);
		$r = mysqli_fetch_row($res);
		return $r[0];
	}
	function nombrecuentapresu($cuenta){
		$linkbd = conectar_v7();
		$linkbd -> set_charset("utf8");
		$maxVersion = ultimaVersionGastosCCPET();
		$sqlr = "SELECT nombre FROM cuentasccpet WHERE codigo='$cuenta' AND version='$maxVersion'";
		$res = mysqli_query($linkbd,$sqlr);
		$r = mysqli_fetch_row($res);
		return $r[0];
	}
	function verificarsaldocuentas($cuenta,$vigencia,$valor,$proyecto){
		$linkbd = conectar_v7();
		if($proyecto!=''){
			$sqlr = "SELECT id_fuente,valorcsf,valorssf FROM ccpproyectospresupuesto_presupuesto WHERE rubro = '$cuenta' AND codproyecto = '$proyecto'";
			$res = mysqli_query($linkbd, $sqlr);
			$numcuentas = mysqli_num_rows($res);
			if($numcuentas != 0 && $numcuentas != ''){
				$salida = $xx = 0;
				do{
					$row = mysqli_fetch_row($res);
					if($row[1]>0){
						$valorcuentaproy = $row[1];
					}else{
						$valorcuentaproy=$row[2];
					}
					if($valor<=$valorcuentaproy){
						$salida = 1;
						$retorno[] = "$row[0]<->$valor";
					}
					$xx++;
				} while(($salida == 0) && ($xx < $numcuentas));
				if($salida == 0){
					$retorno[] = 'NO';
				}
			}else{
				$retorno[] = 'NO';
			}
		}else{
			$totalfuentes = 0;
			$sqlr = "SELECT fuente,SUM(valor) FROM ccpetinicialvalorgastos WHERE cuenta = '$cuenta' AND vigencia = '$vigencia' GROUP BY fuente";
			$res = mysqli_query($linkbd, $sqlr);
			while($row = mysqli_fetch_row($res)){
				$sqlt = "SELECT SUM(valor) FROM ccpettraslados WHERE cuenta = '$cuenta' AND fuente = '$row[0]' AND vigencia = '$vigencia' ";
				$rest = mysqli_query($linkbd, $sqlt);
				$rowt = mysqli_fetch_row($rest);
				$fuente[] = $row[0];
				$valorfuente[] = $row[1]+$rowt[0];
				$totalfuentes += $row[1]+$rowt[0];
			}
			//if($totalfuentes >= $valor)
			if(1==1){
				$fechaf = $vigencia."-01-01";
				$fechaf2 = $vigencia."-12-31";
				$conta=count($fuente);
				$x=$total = $bandera = 0;
				$solofuentes = '';
				$valortr = $valor;
				do{
					$valCDP = 0.0;
					$valCDPRevers = 0.0;
					$querySalidaPresuDefi = "SELECT SUM(D.valor) FROM ccpetcdp AS C, ccpetcdp_detalle AS D WHERE D.cuenta = '$cuenta' AND D.vigencia = '$vigencia' AND D.vigencia = C.vigencia AND D.tipo_mov = '201' AND D.consvigencia = C.consvigencia AND C.tipo_mov = '201' AND C.vigencia = '$vigencia' AND D.fuente = '$fuente[$x]' AND C.fecha BETWEEN '$fechaf' AND '$fechaf2' GROUP BY D.cuenta";
					$res = mysqli_query($linkbd,$querySalidaPresuDefi);
					if(mysqli_num_rows($res)!=0){
						while($row=mysqli_fetch_array($res)){
							$valCDP = $row[0];
						}
					}
					$querySalidaPresuDefi = "SELECT SUM(D.valor) FROM ccpetcdp AS C, ccpetcdp_detalle AS D WHERE D.cuenta = '$cuenta' AND D.vigencia = '$vigencia' OR D.vigencia = '$vigencia' AND D.tipo_mov like'4%' AND D.consvigencia = C.consvigencia AND C.tipo_mov like'4%' AND C.vigencia =' $vigencia' AND D.fuente = '$fuente[$x]' AND C.fecha BETWEEN '$fechaf' AND '$fechaf2' GROUP BY D.cuenta";
					$res = mysqli_query($linkbd,$querySalidaPresuDefi);
					if(mysqli_num_rows($res)!=0){
						while($row=mysqli_fetch_array($res)){
							$valCDPRevers=$row[0];
						}
					}
					$totaldisponible = $valorfuente[$x] - $valCDP + $valCDPRevers;
					$total = $totaldisponible - $valortr;
					if($solofuentes==''){
						$solofuentes = $fuente[$x];
					}else {
						$solofuentes = $solofuentes."-".$fuente[$x];
					}
					if($total>=0){
						$bandera = 1;
						$retorno[] = $fuente[$x].'<->'.$valortr;
					}else{
						$retorno[] = $fuente[$x].'<->'.$totaldisponible;
						$valortr = abs($total);
					}
					$x++;
				}while (($x < $conta) && ($bandera==0));
				if($bandera==0){
					unset($retorno);
					$retorno[] = $solofuentes;
				}
			}else{
				$retorno[] = 'NO';
			}
		}
		return $retorno;
	}
	function nombrefuente($fuente,$tipo){
		$linkbd = conectar_v7();
		$linkbd -> set_charset("utf8");
		$nom='';
		if($tipo=='I'){
			$sqlr = "SELECT nombre FROM pptofutfuenteinv WHERE codigo='$fuente'";
			$res = mysqli_query($linkbd,$sqlr);
			$r = mysqli_fetch_row($res);
			$nom = $r[0];
		}else{
			$sqlr = "SELECT nombre FROM pptofutfuentefunc WHERE codigo='$fuente'";
			$res = mysqli_query($linkbd,$sqlr);
			$r = mysqli_fetch_row($res);
			$nom = $r[0];
		}
		return $nom;
	}
	function nombreproyecto($id){
		$linkbd=conectar_v7();
		$linkbd -> set_charset("utf8");
		$sqlpro = "SELECT codigo,nombre FROM ccpproyectospresupuesto WHERE id='$id'";
		$respro = mysqli_query($linkbd,$sqlpro);
		$rowpro = mysqli_fetch_row($respro);
		$nombre = "$rowpro[0] - $rowpro[1]";
		return($nombre);
	}
	function nombrebpim($id){
		$linkbd = conectar_v7();
		$linkbd -> set_charset("utf8");
		$sqlpro = "SELECT codigo FROM ccpproyectospresupuesto WHERE id='$id'";
		$respro = mysqli_query($linkbd,$sqlpro);
		$rowpro = mysqli_fetch_row($respro);
		$nombre = "$rowpro[0]";
		return($nombre);
	}
	function nombreprogramatico($codigo){
		$linkbd = conectar_v7();
		$linkbd -> set_charset("utf8");
		$sqlpro = "SELECT indicador_producto FROM ccpetproductos WHERE codigo_indicador = '$codigo'";
		$respro = mysqli_query($linkbd,$sqlpro);
		$rowpro = mysqli_fetch_row($respro);
		$nombre = "$codigo - $rowpro[0]";
		return($nombre);
	}
	function nombrefuentecuipo($codigo){
		$linkbd=conectar_v7();
		$linkbd -> set_charset("utf8");
		$sqlpro = "SELECT nombre FROM ccpet_fuentes_cuipo WHERE codigo_fuente = '$codigo'";
		$respro = mysqli_query($linkbd,$sqlpro);
		$rowpro = mysqli_fetch_row($respro);
		$nombre = "$codigo - $rowpro[0]";
		return($nombre);
	}
	class cuentascontablesccpet{
		public static function cuentadebito_tipomovccpet_2021($tipo,$codfun,$cc,$fecha,$modo,$mpension){
			$linkbd = conectar_v7();
			$tipmov = $tipopresupuesto = $digcuenta = '';
			$sqlrfun = "
			SELECT  GROUP_CONCAT(descripcion ORDER BY valor SEPARATOR '<->')
			FROM hum_funcionarios 
			WHERE (item = 'TVINCULACION' OR item = 'TPRESUPUESTO' OR item = 'TPRESUPUESTOPARA' OR item = 'PROYECTOINV') AND estado='S' AND codfun='$codfun'
			GROUP BY codfun
			ORDER BY CONVERT(codfun, SIGNED INTEGER)";
			$respfun = mysqli_query($linkbd,$sqlrfun);
			$rowfun = mysqli_fetch_row($respfun);
			$tipos = explode('<->', $rowfun[0]);
			if($modo=='1'){
				$tipopresupuesto = $tipos[1];
				if($tipos[0]=='T'){
					if($tipos[1]=='F'){
						$sqlr1 = "SELECT funtemporal FROM ccpethumvariables WHERE codigo='$tipo'";
						$tipmov = 'FT';
					}else{
						$sqlr1 = "SELECT invtemporal FROM ccpethumvariables WHERE codigo='$tipo'";
						$tipmov = 'IT';
						$sqlrtt = "SELECT DISTINCT sector FROM ccpproyectospresupuesto_productos WHERE codproyecto='$tipos[2]'";
						$resptt = mysqli_query($linkbd,$sqlrtt);
						$rowtt = mysqli_fetch_row($resptt);
						$sqlrss = "SELECT id_cuenta FROM ccpet_cuentasectores WHERE id_sector='$rowtt[0]'";
						$respss = mysqli_query($linkbd,$sqlrss);
						$rowss = mysqli_fetch_row($respss);
						$digcuenta=$rowss[0];
					}
				}else{
					if($tipos[1]=='F'){
						$sqlr1 = "SELECT funcionamiento FROM ccpethumvariables WHERE codigo='$tipo'";
						$tipmov = 'F';
					}else{
						$sqlr1 = "SELECT inversion FROM ccpethumvariables WHERE codigo='$tipo'";
						$tipmov = 'IN';
						$sqlrtt = "SELECT DISTINCT sector FROM ccpproyectospresupuesto_productos WHERE codproyecto='$tipos[2]'";
						$resptt = mysqli_query($linkbd,$sqlrtt);
						$rowtt = mysqli_fetch_row($resptt);
						$sqlrss = "SELECT id_cuenta FROM ccpet_cuentasectores WHERE id_sector='$rowtt[0]'";
						$respss = mysqli_query($linkbd,$sqlrss);
						$rowss = mysqli_fetch_row($respss);
						$digcuenta = $rowss[0];
					}
				}
			}else{
				if($mpension=='PR'){
					$mpension = 'privado';
				}elseif($mpension=='PB'){
					$mpension = 'publico';
				}elseif($mpension!='N/A'){
					$mpension = 'privado';
				}
				$tipopresupuesto = $tipos[3];
				if($tipos[0]=='T'){
					if($tipos[3]=='F'){
						$sqlr1 = "SELECT funcionamientotemp FROM humparafiscalesccpet_det WHERE codigo='$tipo' AND sector='$mpension' AND estado='S'";
						$tipmov = 'FT';
					}else{
						$sqlr1 = "SELECT inversiontemp FROM humparafiscalesccpet_det WHERE codigo='$tipo' AND sector='$mpension' AND estado='S'";
						$tipmov = 'IT';
						$sqlrtt = "SELECT DISTINCT sector FROM ccpproyectospresupuesto_productos WHERE codproyecto='$tipos[2]'";
						$resptt = mysqli_query($linkbd,$sqlrtt);
						$rowtt = mysqli_fetch_row($resptt);
						$sqlrss = "SELECT id_cuenta FROM ccpet_cuentasectores WHERE id_sector='$rowtt[0]'";
						$respss = mysqli_query($linkbd,$sqlrss);
						$rowss = mysqli_fetch_row($respss);
						$digcuenta = $rowss[0];
					}
				}else{
					if($tipos[3]=='F'){
						$sqlr1 = "SELECT funcionamiento FROM humparafiscalesccpet_det WHERE codigo='$tipo' AND sector='$mpension' AND estado='S'";
						$tipmov = 'F';
					}else{
						$sqlr1 = "SELECT inversion FROM humparafiscalesccpet_det WHERE codigo='$tipo' AND sector='$mpension' AND estado='S'";
						$tipmov = 'IN';
						$sqlrtt = "SELECT DISTINCT sector FROM ccpproyectospresupuesto_productos WHERE codproyecto='$tipos[2]'";
						$resptt = mysqli_query($linkbd,$sqlrtt);
						$rowtt = mysqli_fetch_row($resptt);
						$sqlrss = "SELECT id_cuenta FROM ccpet_cuentasectores WHERE id_sector='$rowtt[0]'";
						$respss = mysqli_query($linkbd,$sqlrss);
						$rowss = mysqli_fetch_row($respss);
						$digcuenta = $rowss[0];
					}
				}
			}
			$res1 = mysqli_query($linkbd,$sqlr1);
			$row1 = mysqli_fetch_row($res1);
			if($digcuenta!=''){
				$q1tt = "AND cuenta LIKE '$digcuenta%'";
			}else {
				$q1tt = "";
			}
			$sqlr2 = "SELECT cuenta FROM conceptoscontables_det WHERE modulo = '2' AND debito = 'S' AND tipocuenta = 'N' AND codigo = '$row1[0]' AND tipo = '$tipmov' AND cc  = '$cc' $q1tt AND fechainicial = (SELECT MAX(fechainicial) FROM conceptoscontables_det WHERE fechainicial <= '$fecha' AND modulo = '2' AND debito = 'S' AND tipocuenta = 'N' AND codigo = '$row1[0]' AND tipo = '$tipmov' AND cc  = '$cc' $q1tt)";
			$res2 = mysqli_query($linkbd,$sqlr2);
			$row2 = mysqli_fetch_row($res2);
			if($row2[0]!=''){
				return $row2[0];
			}else{
				return 'Sin Cuenta';
			}
		}
		public static function cuentacredito_tipomovccpet_2021($tipo,$codfun,$cc,$fecha,$modo,$mpension){
			$linkbd = conectar_v7();
			$tipmov = $tipopresupuesto='';
			$sqlrfun = "
			SELECT  GROUP_CONCAT(descripcion ORDER BY valor SEPARATOR '<->')
			FROM hum_funcionarios 
			WHERE (item = 'TVINCULACION' OR item = 'TPRESUPUESTO' OR item = 'TPRESUPUESTOPARA') AND estado='S' AND codfun='$codfun'
			GROUP BY codfun
			ORDER BY CONVERT(codfun, SIGNED INTEGER)";
			$respfun = mysqli_query($linkbd,$sqlrfun);
			$rowfun =mysqli_fetch_row($respfun);
			$tipos = explode('<->', $rowfun[0]);
			if($modo=='1'){
				$tipopresupuesto = $tipos[1];
				if($tipos[0]=='T'){
					if($tipos[1]=='F'){
						$sqlr1 = "SELECT funtemporal FROM ccpethumvariables WHERE codigo='$tipo'";
						$tipmov = 'FT';
					}else{
						$sqlr1 = "SELECT invtemporal FROM ccpethumvariables WHERE codigo='$tipo'";
						$tipmov = 'IT';
					}
				}else{
					if($tipos[1]=='F'){
						$sqlr1 = "SELECT funcionamiento FROM ccpethumvariables WHERE codigo = '$tipo'";
						$tipmov = 'F';
					}else{
						$sqlr1 = "SELECT inversion FROM ccpethumvariables WHERE codigo = '$tipo'";
						$tipmov = 'IN';
					}
				}
			}else{	
				if($mpension=='PR'){
					$mpension = 'privado';
				}elseif($mpension=='PB'){
					$mpension = 'publico';
				}
				elseif($mpension!='N/A'){$mpension='privado';}
				$tipopresupuesto = $tipos[2];
				if($tipos[0]=='T'){
					if($tipos[2]=='F'){
						$sqlr1 = "SELECT funcionamientotemp FROM humparafiscalesccpet_det WHERE codigo='$tipo' AND sector='$mpension' AND estado='S'";
						$tipmov = 'FT';
					}else{
						$sqlr1 = "SELECT inversiontemp FROM humparafiscalesccpet_det WHERE codigo='$tipo' AND sector='$mpension' AND estado='S'";
						$tipmov = 'IT';
					}
				}else{
					if($tipos[2]=='F'){
						$sqlr1 = "SELECT funcionamiento FROM humparafiscalesccpet_det WHERE codigo='$tipo' AND sector='$mpension' AND estado='S'";
						$tipmov = 'F';
					}else{
						$sqlr1 = "SELECT inversion FROM humparafiscalesccpet_det WHERE codigo='$tipo' AND sector='$mpension' AND estado='S'";
						$tipmov = 'IN';
					}
				}
			}
			$res1 = mysqli_query($linkbd,$sqlr1);
			$row1 = mysqli_fetch_row($res1);
			$sqlr2 = "SELECT cuenta FROM conceptoscontables_det WHERE modulo = '2' AND credito = 'S' AND tipocuenta = 'N' AND codigo = '$row1[0]' AND tipo = '$tipmov' AND cc  = '$cc' AND fechainicial = (SELECT MAX(fechainicial) FROM conceptoscontables_det WHERE fechainicial <= '$fecha' AND modulo = '2' AND credito = 'S' AND tipocuenta = 'N' AND codigo = '$row1[0]' AND tipo = '$tipmov' AND cc  = '$cc')";
			$res2 = mysqli_query($linkbd,$sqlr2);
			$row2 = mysqli_fetch_row($res2);
			if($row2[0]!=''){
				return $row2[0];
			}else{
				return 'Sin Cuenta';
			}
		}
		public static function cuentadebito_tipomovccpet($tipo,$codfun,$cc,$fecha,$modo,$mpension,$nomina,$conceptonom){
			$linkbd = conectar_v7();
			$linkbd -> set_charset("utf8");
			$tipopagovin = buscartipovinculacion($nomina, $conceptonom, $codfun);
			//$nunproyecto = itemfuncionarios($codfun,'31');
			$numprogramatico  = '';
			if($mpension !='N/A'){
				$nomsector = 'privado';
			}else{
				$nomsector = 'N/A';
			}
			if($modo == 0){
				switch ($tipopagovin[$modo]){
					case 'F':	$sql1 = "SELECT funcionamiento FROM ccpethumvariables WHERE codigo = '$tipo'";break;
					case 'FT':	$sql1 = "SELECT funtemporal FROM ccpethumvariables WHERE codigo = '$tipo'";break;
					case 'IN':	$sql1 = "SELECT inversion FROM ccpethumvariables WHERE codigo = '$tipo'";
								$numprogramatico = substr(itemfuncionarios($codfun,'43'),0,2);break;
					case 'IT':	$sql1 = "SELECT invtemporal FROM ccpethumvariables WHERE codigo = '$tipo'";
								$numprogramatico = substr(itemfuncionarios($codfun,'43'),0,2);break;
					case 'CP':	$sql1 = "SELECT gastoscomerc FROM ccpethumvariables WHERE codigo = '$tipo'";break;
					case 'CT':	$sql1 = "SELECT gastoscomerctemporal FROM ccpethumvariables WHERE codigo = '$tipo'";break;
				}
			}else{
				switch ($tipopagovin[$modo]){
					case 'F':	$sql1 = "SELECT funcionamiento FROM humparafiscalesccpet_det WHERE codigo = '$tipo' AND sector = '$nomsector'";break;
					case 'FT':	$sql1 = "SELECT funcionamientotemp FROM humparafiscalesccpet_det WHERE codigo = '$tipo' AND sector = '$nomsector'";break;
					case 'IN':	$sql1 = "SELECT inversion FROM humparafiscalesccpet_det WHERE codigo = '$tipo' AND sector = '$nomsector'";
								$numprogramatico = substr(itemfuncionarios($codfun,'43'),0,2);break;
					case 'IT':	$sql1 = "SELECT inversiontemp FROM humparafiscalesccpet_dets WHERE codigo = '$tipo' AND sector = '$nomsector'";
								$numprogramatico = substr(itemfuncionarios($codfun,'43'),0,2);break;
					case 'CP':	$sql1 = "SELECT gastoscomerc FROM humparafiscalesccpet_det WHERE codigo = '$tipo' AND sector = '$nomsector'";break;
					case 'CT':	$sql1 = "SELECT gastoscomerctemp FROM humparafiscalesccpet_det WHERE codigo = '$tipo' AND sector = '$nomsector'";break;
				}
			}
			$res1 = mysqli_query($linkbd,$sql1);
			$row1 = mysqli_fetch_row($res1);
			if($numprogramatico != ''){
				$sqltt = "SELECT id_cuenta FROM ccpet_cuentasectores WHERE id_sector = '$numprogramatico'";
				$restt = mysqli_query($linkbd,$sqltt);
				$rowtt = mysqli_fetch_row($restt);
				$consl = "AND cuenta LIKE '$rowtt[0]%'";
			}else {
				$consl = "";
			}
			$sql2 = "SELECT cuenta FROM conceptoscontables_det WHERE modulo = '2' AND debito = 'S' AND tipocuenta = 'N' AND codigo = '$row1[0]' AND tipo = '$tipopagovin[$modo]' AND cc  = '$cc' $consl AND fechainicial = (SELECT MAX(fechainicial) FROM conceptoscontables_det WHERE fechainicial <= '$fecha' AND modulo = '2' AND debito = 'S' AND tipocuenta = 'N' AND codigo = '$row1[0]' AND tipo = '$tipopagovin[$modo]' AND cc  = '$cc' $consl)";
			$res2 = mysqli_query($linkbd,$sql2);
			$row2 = mysqli_fetch_row($res2);
			if($row2[0]!=''){
				return $row2[0];
			}else{
				return 'Sin Cuenta';
			}
		}
		public static function cuentacredito_tipomovccpet($tipo,$codfun,$cc,$fecha,$modo,$mpension,$nomina,$conceptonom){
			$linkbd = conectar_v7();
			$tipopagovin = buscartipovinculacion($nomina, $conceptonom, $codfun);
			$nunproyecto = itemfuncionarios($codfun,'31');
			if($mpension !='N/A'){
				$nomsector = 'privado';
			}else {
				$nomsector = 'N/A';}
			if($modo == 0){
				switch ($tipopagovin[$modo]){
					case 'F':	$sql1 = "SELECT funcionamiento FROM ccpethumvariables WHERE codigo = '$tipo'";break;
					case 'FT':	$sql1 = "SELECT funtemporal FROM ccpethumvariables WHERE codigo = '$tipo'";break;
					case 'IN':	$sql1 = "SELECT inversion FROM ccpethumvariables WHERE codigo = '$tipo'";break;
					case 'IT':	$sql1 = "SELECT invtemporal FROM ccpethumvariables WHERE codigo = '$tipo'";break;
					case 'CP':	$sql1 = "SELECT gastoscomerc FROM ccpethumvariables WHERE codigo = '$tipo'";break;
					case 'CT':	$sql1 = "SELECT gastoscomerctemporal FROM ccpethumvariables WHERE codigo = '$tipo'";break;
				}
			}else{
				switch ($tipopagovin[$modo]){
					case 'F':	$sql1 = "SELECT funcionamiento FROM humparafiscalesccpet_det WHERE codigo = '$tipo' AND sector = '$nomsector'";break;
					case 'FT':	$sql1 = "SELECT funcionamientotemp FROM humparafiscalesccpet_det WHERE codigo = '$tipo' AND sector = '$nomsector'";break;
					case 'IN':	$sql1 = "SELECT inversion FROM humparafiscalesccpet_det WHERE codigo = '$tipo' AND sector = '$nomsector'";break;
					case 'IT':	$sql1 = "SELECT inversiontemp FROM humparafiscalesccpet_dets WHERE codigo = '$tipo' AND sector = '$nomsector'";break;
					case 'CP':	$sql1 = "SELECT gastoscomerc FROM humparafiscalesccpet_det WHERE codigo = '$tipo' AND sector = '$nomsector'";break;
					case 'CT':	$sql1 = "SELECT gastoscomerctemp FROM humparafiscalesccpet_det WHERE codigo = '$tipo' AND sector = '$nomsector'";break;
				}
			}
			$res1 = mysqli_query($linkbd,$sql1);
			$row1 = mysqli_fetch_row($res1);
			$sql2  = "SELECT cuenta FROM conceptoscontables_det WHERE modulo = '2' AND credito = 'S' AND tipocuenta = 'N' AND codigo = '$row1[0]' AND tipo = '$tipopagovin[$modo]' AND cc  = '$cc' AND fechainicial = (SELECT MAX(fechainicial) FROM conceptoscontables_det WHERE fechainicial <= '$fecha' AND modulo = '2' AND credito = 'S' AND tipocuenta = 'N' AND codigo = '$row1[0]' AND tipo = '$tipopagovin[$modo]' AND cc  = '$cc')";
			$res2 = mysqli_query($linkbd,$sql2);
			$row2 = mysqli_fetch_row($res2);
			if($row2[0]!=''){
				return $row2[0];
			}else {
				return 'Sin Cuenta';
			}
		}
		public static function cuentadebito_egreso($cuentap,$cc,$fecha,$concepto){
			$linkbd = conectar_v7();
			$divcuenta = explode('<_>', $cuentap);
			//$sqlr1="SELECT concepto_cont,tipo FROM ccpetcuentas_concepto_contable WHERE cuenta_p = '$divcuenta[0]' AND estado='S'";
			$sqlr1 = "SELECT concepto_nom, tipo FROM ccpetcuentas_concepto_contable WHERE cuenta_p = '$divcuenta[0]' AND estado = 'S'";
			$res1 = mysqli_query($linkbd,$sqlr1);
			$row1 = mysqli_fetch_row($res1);
			if ($concepto==''){
				$valconc=$row1[0];
			}else {
				$valconc=$concepto;
			}
			$sqlr2 = "SELECT cuenta FROM conceptoscontables_det WHERE modulo = '2' AND credito = 'S' AND tipocuenta = 'N' AND codigo = '$valconc' AND tipo = '$row1[1]' AND cc  = '$cc' AND fechainicial = (SELECT MAX(fechainicial) FROM conceptoscontables_det WHERE fechainicial <= '$fecha' AND modulo = '2' AND credito = 'S' AND tipocuenta = 'N' AND codigo = '$valconc' AND tipo = '$row1[1]' AND cc  = '$cc')";
			$res2 = mysqli_query($linkbd,$sqlr2);
			$row2 = mysqli_fetch_row($res2);
			if($row2[0]!=''){
				return $row2[0];
			}
			else {
				return 'Sin Cuenta';
			}
		}
		public static function cuentadebito_egresoparafiscal($cuentap,$cc,$fecha,$concepto){
			$linkbd = conectar_v7();
			$divcuenta = explode('<_>', $cuentap);
			//$sqlr1="SELECT concepto_cont,tipo FROM ccpetcuentas_concepto_contable WHERE cuenta_p = '$divcuenta[0]' AND estado='S'";
			$sqlr1 = "SELECT concepto_nom, tipo FROM ccpetcuentas_concepto_contable WHERE cuenta_p = '$divcuenta[0]' AND estado = 'S'";
			$res1 = mysqli_query($linkbd,$sqlr1);
			$row1=mysqli_fetch_row($res1);
			if ($concepto==''){
				$valconc = $row1[0];
			}else{
				$valconc = $concepto;
			}
			$sqlr2 = "SELECT cuenta FROM conceptoscontables_det WHERE modulo = '2' AND credito = 'S' AND tipocuenta = 'N' AND codigo = '$valconc' AND tipo = '$row1[1]' AND fechainicial = (SELECT MAX(fechainicial) FROM conceptoscontables_det WHERE fechainicial <= '$fecha' AND modulo = '2' AND credito = 'S' AND tipocuenta = 'N' AND codigo = '$valconc' AND tipo = '$row1[1]')";
			$res2 = mysqli_query($linkbd,$sqlr2);
			$row2 = mysqli_fetch_row($res2);
			if($row2[0]!=''){
				return $row2[0];
			}else {
				return 'Sin Cuenta';
			}
		}
	}
	function producto_servicio_nomina ($cuenta,$vigencia){
		$linkbd = conectar_v7();
		$varcarga = array();
		if(substr($cuenta,0,3) == "2.1"){
			$total = 0;
			$sql = "SELECT medioPago FROM ccpetinicialvalorgastos WHERE cuenta = '$cuenta' AND vigencia = '$vigencia'";
			$res = mysqli_query($linkbd,$sql);
			$total = mysqli_num_rows($res);
			if($total>0){
				$row = mysqli_fetch_row($res);
				$varcarga[0] = '';
				$varcarga[1] = '';
				$varcarga[2] = $row[0];
				$varcarga[3] = '';
				$varcarga[4] = '';
				return $varcarga;//[0]productoservicio, [1]indicador producto, [2]medio de pago, [3]politicas publicas, [4]bpin
			}else{
				$sql = "SELECT subclase, medioPago FROM ccpetinicialservicios WHERE cuenta = '$cuenta' AND vigencia = '$vigencia'";
				$res = mysqli_query($linkbd,$sql);
				$row = mysqli_fetch_row($res);
				$varcarga[0] = $row[0];
				$varcarga[1] = '';
				$varcarga[2] = $row[1];
				$varcarga[3] = '';
				$varcarga[4] = '';
				return $varcarga;//[0]productoservicio, [1]indicador producto, [2]medio de pago, [3]politicas publicas, [4]bpin
			}
		}else{
			$sql = "
			SELECT T1.subclase, T1.indicador_producto, T1.medio_pago, T1.politicas_publicas, T2.codigo
			FROM ccpproyectospresupuesto_presupuesto AS T1
			INNER JOIN ccpproyectospresupuesto AS T2
			ON T1.codproyecto = T2.id
			WHERE T1.rubro = '$cuenta' and T2.vigencia = '$vigencia'";
			$res = mysqli_query($linkbd,$sql);
			$row = mysqli_fetch_row($res);
			$varcarga[0] = $row[0];
			$varcarga[1] = $row[1];
			$varcarga[2] = $row[2];
			$varcarga[3] = $row[3];
			$varcarga[4] = $row[4];
			return $varcarga;//[0]productoservicio, [1]indicador producto, [2]medio de pago, [3]politicas publicas, [4]bpin
		}
	}
	function itemfuncionarios($codfun, $tipo){
		$linkbd = conectar_v7();
		$linkbd -> set_charset("utf8");
		$sqlrfun="
		SELECT descripcion
		FROM hum_funcionarios 
		WHERE (valor = '$tipo') AND estado = 'S' AND codfun = '$codfun'
		GROUP BY codfun
		ORDER BY codrad DESC";
		$resp = mysqli_query($linkbd,$sqlrfun);
		$row = mysqli_fetch_row($resp);
		return $row[0];
	}
	function fechasfuncionarios($codfun, $tipo){
		$linkbd = conectar_v7();
		$sqlrfun = "
		SELECT  GROUP_CONCAT(fechain ORDER BY valor SEPARATOR '<->')
		FROM hum_funcionarios 
		WHERE (valor = '$tipo') AND estado = 'S' AND codfun = '$codfun'
		GROUP BY codfun
		ORDER BY codrad DESC";
		$resp = mysqli_query($linkbd,$sqlrfun);
		$row = mysqli_fetch_row($resp);
		return $row[0];
	}
	function idsfuncionarios($codfun, $tipo){
		$linkbd = conectar_v7();
		$sqlrfun="
		SELECT  GROUP_CONCAT(codrad ORDER BY valor SEPARATOR '<->')
		FROM hum_funcionarios 
		WHERE (valor = '$tipo') AND estado = 'S' AND codfun = '$codfun'
		GROUP BY codfun
		ORDER BY codrad DESC";
		$resp = mysqli_query($linkbd,$sqlrfun);
		$row = mysqli_fetch_row($resp);
		return $row[0];
	}
	function numero_novedad($prenomina){
		$linkbd = conectar_v7();
		$sql = "SELECT id FROM hum_novedadespagos_cab WHERE prenomina='$prenomina'";
		$resp = mysqli_query($linkbd,$sql);
		$row = mysqli_fetch_row($resp);
		return $row[0];
	}
	function tipopresufuncionario($noveda,$codfunc,$tipo){
		$linkbd = conectar_v7();
		$sql = "SELECT secpresu, tippresu1, tippresu2, pse, psr, ppe, ppr, parl, pccf, picbf, psena, pitec, pesap FROM hum_novedadespagos WHERE codigo = '$noveda' AND idfun = '$codfunc' AND tipo = '$tipo'";
		$resp = mysqli_query($linkbd,$sql);
		$row = mysqli_fetch_row($resp);
		$datos = array();
		$datos[0] = $row[0];
		$datos[1] = $row[1];
		$datos[2] = $row[2];
		$datos[3] = $row[3];
		$datos[4] = $row[4];
		$datos[5] = $row[5];
		$datos[6] = $row[6];
		$datos[7] = $row[7];
		$datos[8] = $row[8];
		$datos[9] = $row[9];
		$datos[10] = $row[10];
		$datos[11] = $row[11];
		$datos[12] = $row[12];
		return $datos;
	}
	function buscarcuentanuevocatalogo1($tipo,$tpresu){
		$linkbd = conectar_v7();
		switch ($tpresu){
			case 'F':	$sql1 = "SELECT funcionamiento FROM ccpethumvariables WHERE codigo = '$tipo'";break;
			case 'FT':	$sql1 = "SELECT funtemporal FROM ccpethumvariables WHERE codigo = '$tipo'";break;
			case 'IN':	$sql1 = "SELECT inversion FROM ccpethumvariables WHERE codigo = '$tipo'";break;
			case 'IT':	$sql1 = "SELECT invtemporal FROM ccpethumvariables WHERE codigo = '$tipo'";break;
			case 'CP':	$sql1 = "SELECT gastoscomerc FROM ccpethumvariables WHERE codigo = '$tipo'";break;
			case 'CT':	$sql1 = "SELECT gastoscomerctemporal FROM ccpethumvariables WHERE codigo = '$tipo'";break;
		}
		$res1 = mysqli_query($linkbd,$sql1);
		$row1 = mysqli_fetch_row($res1);
		//$sql2 = "SELECT cuenta_p FROM ccpetcuentas_concepto_contable WHERE concepto_cont = '$row1[0]' AND tipo = '$tpresu' AND estado = 'S'";
		$sql2 = "SELECT cuenta_p FROM ccpetcuentas_concepto_contable WHERE concepto_nom = '$row1[0]' AND tipo = '$tpresu' AND estado = 'S'";
		$res2 = mysqli_query($linkbd,$sql2);
		$row2 = mysqli_fetch_row($res2);
		return $row2[0];
	}
	function buscarcuentanuevocatalogo2($tipo,$tpresu,$sector){
		$linkbd = conectar_v7();
		if($sector != "N/A"){
			$nomsector = "privado";
		}else{
			$nomsector = $sector;
		}
		switch ($tpresu){
			case 'F':	$sql1 = "SELECT funcionamiento FROM humparafiscalesccpet_det WHERE codigo = '$tipo' AND sector = '$nomsector'";break;
			case 'FT':	$sql1 = "SELECT funcionamientotemp FROM humparafiscalesccpet_det WHERE codigo = '$tipo' AND sector = '$nomsector'";break;
			case 'IN':	$sql1 = "SELECT inversion FROM humparafiscalesccpet_det WHERE codigo = '$tipo' AND sector = '$nomsector'";break;
			case 'IT':	$sql1 = "SELECT inversiontemp FROM humparafiscalesccpet_dets WHERE codigo = '$tipo' AND sector = '$nomsector'";break;
			case 'CP':	$sql1 = "SELECT gastoscomerc FROM humparafiscalesccpet_det WHERE codigo = '$tipo' AND sector = '$nomsector'";break;
			case 'CT':	$sql1 = "SELECT gastoscomerctemp FROM humparafiscalesccpet_det WHERE codigo = '$tipo' AND sector = '$nomsector'";break;
		}
		$res1 = mysqli_query($linkbd,$sql1);
		$row1 = mysqli_fetch_row($res1);
		//$sql2 = "SELECT cuenta_p FROM ccpetcuentas_concepto_contable WHERE concepto_cont = '$row1[0]' AND tipo = '$tpresu' AND estado = 'S'";
		$sql2 = "SELECT cuenta_p FROM ccpetcuentas_concepto_contable WHERE concepto_nom = '$row1[0]' AND tipo = '$tpresu' AND estado = 'S'";
		$res2 = mysqli_query($linkbd,$sql2);
		$row2 = mysqli_fetch_row($res2);
		return $row2[0];
	}
	function buscarfuentespres($cuenta, $secpresu, $vigencia, $tpresu, $codfun,$valfuente, $pfcp=''){
		$linkbd = conectar_v7();
		switch ($tpresu){
			case 'F':
			case 'FT':
			case 'CP':
			case 'CT':{
				$busfuentes = array();
				$fuentesfin = '';
				$sql = "SELECT fuente FROM ccpetinicialgastosfun WHERE cuenta = '$cuenta' AND seccion_presupuestal = '$secpresu' AND vigencia = '$vigencia' AND valor > 0";
				$res = mysqli_query($linkbd,$sql);
				while ($row = mysqli_fetch_row($res)){
					$busfuentes[] = $row[0];
				}
				$sql = "SELECT fuente FROM ccpet_traslados WHERE cuenta = '$cuenta' AND seccion_presupuestal = '$secpresu' AND vigencia = '$vigencia' AND valor > 0";
				$res = mysqli_query($linkbd,$sql);
				while ($row = mysqli_fetch_row($res)){
					$busfuentes[] = $row[0];
				}
				$x = 0;
				$y = count($busfuentes);
				do{
					$fuentess = $busfuentes[$x];
					$tipogasto = '1';
					$mediopago = 'CSF';
					$viggasto = '1';
					$parametros = array(
						'rubro' => $cuenta,
						'fuente' => $fuentess,
						'vigencia' => $vigencia,
						'tipo_gasto' => $tipogasto,
						'seccion_presupuestal' => $secpresu,
						'medio_pago' => $mediopago,
						'vigencia_gasto' => $viggasto,
						'codProyecto' => '',
						'programatico' => ''
					);
					$cuentafuente = $secpresu.'<_>'.$cuenta.'<_>'.$fuentess;
					if($pfcp[$cuentafuente] == ''){
						$valsumatotal = $valfuente;
					}else{
						$valsumatotal = $pfcp[$cuentafuente] + $valfuente;
					}
					$valsaldo = saldoPorRubro($parametros);
					if ( ($valsaldo >= $valsumatotal) && $fuentesfin == ''){
						$fuentesfin = $fuentess;
						$x = $y;
					}
					$x++;
				}while ($x < $y);
				break;
			}
			case 'IN':
			case 'IT':
				$proyecto = itemfuncionarios($codfun,'31');
				$programatico = itemfuncionarios($codfun,'43');
				$fuente = itemfuncionarios($codfun,'44');
				$fuentesfin = $fuente.'<_>'.$programatico.'<_>'.$proyecto;
				break;
		}
		return $fuentesfin;
	}
	function saldoPorRubro($parametros){//[rubro, fuente, vigencia, tipo_gasto, seccion_presupuestal, medio_pago, vigencia_gasto]
		$linkbd = conectar_v7();
		$linkbd -> set_charset("utf8");
		//INICIAL
		$inicial = 0;
		$adicion = 0;
		$reduccion = 0;
		$credito = 0;
		$contracredito = 0;
		$saldoCdp = 0;
		$saldoFinal = 0;
		//***SALDO INICIAL */
		if($parametros['tipo_gasto'] == '3'){
			$sqlr = "SELECT SUM(TB2.valorcsf), SUM(TB2.valorssf) FROM ccpproyectospresupuesto AS TB1, ccpproyectospresupuesto_presupuesto AS TB2 WHERE TB1.codigo = '".$parametros['codProyecto']."' AND TB2.id_fuente = '".$parametros['fuente']."' AND TB2.indicador_producto = '".$parametros['programatico']."' AND TB1.id = TB2.codproyecto AND TB1.vigencia = '".$parametros['vigencia']."' AND TB2.vigencia_gasto = '".$parametros['vigencia_gasto']."'";
			$resp = mysqli_query($linkbd, $sqlr);
			$row = mysqli_fetch_row($resp);
			if($parametros['medio_pago'] == 'CSF'){
				$inicial += $row[0];
			}else {
				$inicial += $row[1];
			}
			//saldo del cdp de inversion
			$sqlrValorCdp = "SELECT SUM(valor) FROM ccpetcdp_detalle WHERE bpim = '".$parametros['codProyecto']."' AND fuente = '".$parametros['fuente']."' AND indicador_producto = '".$parametros['programatico']."' AND vigencia = '".$parametros['vigencia']."' AND codigo_vigenciag = '".$parametros['vigencia_gasto']."' AND medio_pago = '".$parametros['medio_pago']."'";
			$respValorCdp = mysqli_query($linkbd, $sqlrValorCdp);
			$rowValorCdp = mysqli_fetch_row($respValorCdp);
			$saldoCdp = $rowValorCdp[0];
		}else{
			$sqlr = "SELECT SUM(valor) FROM ccpetinicialgastosfun WHERE cuenta = '".$parametros['rubro']."' AND fuente = '".$parametros['fuente']."' AND vigencia = '".$parametros['vigencia']."' AND seccion_presupuestal = '".$parametros['seccion_presupuestal']."' AND  medio_pago = '".$parametros['medio_pago']."' AND vigencia_gasto = '".$parametros['vigencia_gasto']."' ";
			$resp = mysqli_query($linkbd, $sqlr);
			$row = mysqli_fetch_row($resp);
			$inicial += $row[0];
			$sqlrValorCdp = "SELECT SUM(valor) FROM ccpetcdp_detalle WHERE cuenta = '".$parametros['rubro']."' AND fuente = '".$parametros['fuente']."' AND vigencia = '".$parametros['vigencia']."' AND seccion_presupuestal = '".$parametros['seccion_presupuestal']."' AND codigo_vigenciag = '".$parametros['vigencia_gasto']."' AND medio_pago = '".$parametros['medio_pago']."'";
			$respValorCdp = mysqli_query($linkbd, $sqlrValorCdp);
			$rowValorCdp = mysqli_fetch_row($respValorCdp);
			$saldoCdp = $rowValorCdp[0];
		}
		/** FINAL DEL CALCULO DEL SALDO INICIAL */
		$sqlrAdd = "SELECT SUM(valor) FROM ccpet_adiciones WHERE cuenta = '".$parametros['rubro']."' AND fuente = '".$parametros['fuente']."' AND vigencia = '".$parametros['vigencia']."' AND seccion_presupuestal = '".$parametros['seccion_presupuestal']."' AND codigo_vigenciag = '".$parametros['vigencia_gasto']."' AND medio_pago = '".$parametros['medio_pago']."' AND programatico = '".$parametros['programatico']."' AND bpim = '".$parametros['codProyecto']."'";

        $respValorAdd = mysqli_query($linkbd, $sqlrAdd);
        $rowValorAdd = mysqli_fetch_row($respValorAdd);

        $adicion = $rowValorAdd[0];

        /** FINAL DEL CALCULO DEL SALDO ADICION */

        //***TRASLADO CREDITO */

        $sqlrCredito = "SELECT SUM(valor) FROM ccpet_traslados WHERE cuenta = '".$parametros['rubro']."' AND fuente = '".$parametros['fuente']."' AND vigencia = '".$parametros['vigencia']."' AND seccion_presupuestal = '".$parametros['seccion_presupuestal']."' AND codigo_vigenciag = '".$parametros['vigencia_gasto']."' AND medio_pago = '".$parametros['medio_pago']."' AND programatico = '".$parametros['programatico']."' AND bpim = '".$parametros['codProyecto']."' AND tipo = 'C'";

        $respValorCredito = mysqli_query($linkbd, $sqlrCredito);
        $rowValorCredito = mysqli_fetch_row($respValorCredito);

        $credito = $rowValorCredito[0];

        /** FINAL DEL CALCULO DEL SALDO TRASLADO CREDITO */

        //***TRASLADO CONTRA CREDITO */

        $sqlrContraCredito = "SELECT SUM(valor) FROM ccpet_traslados WHERE cuenta = '".$parametros['rubro']."' AND fuente = '".$parametros['fuente']."' AND vigencia = '".$parametros['vigencia']."' AND seccion_presupuestal = '".$parametros['seccion_presupuestal']."' AND codigo_vigenciag = '".$parametros['vigencia_gasto']."' AND medio_pago = '".$parametros['medio_pago']."' AND programatico = '".$parametros['programatico']."' AND bpim = '".$parametros['codProyecto']."' AND tipo = 'R'";

        $respValorContraCredito = mysqli_query($linkbd, $sqlrContraCredito);
        $rowValorContraCredito = mysqli_fetch_row($respValorContraCredito);

        $contracredito = $rowValorContraCredito[0];
		//Calculo del saldo final sumando y restando la progrmacion presupuestal menos saldo del cdp.
		$saldoFinal = round($inicial, 2) +  round($adicion, 2) - round($reduccion, 2) + round($credito, 2) - round($contracredito, 2) - round($saldoCdp, 2);
		return $saldoFinal;
	}
	function buscartipovinculacion($nomina, $concepto, $cod_fun){
		$linkbd = conectar_v7();
		$sql = "
		SELECT T1.tippresu1, T1.tippresu2 
		FROM hum_novedadespagos AS T1 
		INNER JOIN hum_novedadespagos_cab AS T2
		ON T1.codigo = T2.id
		INNER JOIN hum_prenomina AS T3
		ON T2.prenomina = T3.codigo
		WHERE T3.num_liq = '$nomina' AND T1.tipo = '$concepto' AND T1.idfun = '$cod_fun'";
		$res = mysqli_query($linkbd,$sql);
		$row = mysqli_fetch_row($res);
		$tipos[0] = $row[0];
		$tipos[1] = $row[1];
		return $tipos;
	}
	function cambiocodigosnomina($codentrada){
		switch ($codentrada){
			case 'PR':	$codsalida = 25;break;
			case 'SR':	$codsalida = 26;break;
		}
		return $codsalida;
	}
	function buscaSaludPension($cod){
		switch ($cod){
			case 'PR':	$nombre = "PENSION EMPRESA";break;
			case 'PE':	$nombre = "PENSION FUNCIONARIO";break;
			case 'FS':	$nombre = "FONDO DE SOLIDARIDAD";break;
			case 'SR':	$nombre = "SALUD EMPRESA";break;
			case 'SE':	$nombre = "SALUD FUNCIONARIO";break;
		}
		return $nombre;
	}
?>