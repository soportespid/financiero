<?php 
	require_once '/PHPExcel/Classes/PHPExcel.php';//Incluir la libreria PHPExcel 
	include '/PHPExcel/Classes/PHPExcel/IOFactory.php';// PHPExcel_IOFactory
	require"comun.inc";
	require"funciones.inc";
	session_start();
	$linkbd=conectar_bd();	
	$objPHPExcel = new PHPExcel();// Crea un nuevo objeto PHPExcel
	$objReader = PHPExcel_IOFactory::createReader('Excel2007');// Leemos un archivo Excel 2007
	$objPHPExcel = $objReader->load("formatos/Reporte Aseo.xlsx");
	$borders = array(
      'borders' => array(
        'allborders' => array(
          'style' => PHPExcel_Style_Border::BORDER_THIN,
          'color' => array('argb' => 'FF000000'),
        )
      ),
    );
	ini_set('max_execution_time', 7200);
	// Agregar Informacion
	$xy=2;
	$vigencias=$_POST['vigencias'];
	$mes=$_POST['mes'];
	$meses=array('','Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre');
	$crit2 = "WHERE T1.id_liquidacion=T2.id_liquidacion AND T1.servicio LIKE '03'";
	$crit4 = "AND T2.vigencia = '$_POST[vigencias]' AND (T2.mes='$_POST[mes]' OR  T2.mesfin='$_POST[mes]')";
	$crit5 = "ORDER BY T2.codusuario, T2.id_liquidacion";
	$sqlr1="SELECT DISTINCT T2.vigencia,T2.mes,T2.mesfin,T2.codusuario,T2.id_liquidacion,T1.servicio,T1.estrato,T1.tarifa, T1.subsidio,T1.contribucion, T2.fecha,T1.valorliquidacion,T1.abono,T1.saldo FROM servliquidaciones_det T1, servliquidaciones T2 $crit2 $crit3 $crit4 $crit5";
    $resp1 = mysql_query($sqlr1,$linkbd);
	while ($row =mysql_fetch_row($resp1)) 
	{
		$sqlr="select *from servclientes where codigo='$row[3]'";
		$respc=mysql_query($sqlr,$linkbd);
		$rowc =mysql_fetch_row($respc);
		$filbor="A".$xy.":AM".$xy;
		switch ($row[6])
		{
			case '11':	$estratot="1 Bajo-Bajo";break;
			case '12': 	$estratot="2 Bajo";break;
			case '13': 	$estratot="3 Medio-Bajo";break;
			case '141': $estratot="4 Medio";break;
			case '14': 	$estratot="Oficial";break;
			default:	$estratot="No Residencial";
		}
		if(($row[6]>10)&&($row[6]<15)){$tipoestra="R";}
		else {$tipoestra="NR";}
		if($row[12]>0)
		{
			$diasmora='30';
			$valormora=$row[12];	
		}
		else
		{
			$diasmora='0';
			$valormora='0';
		}
		if($row[8]>0){$subcon="-$row[8]";}
		else {$subcon="$row[9]";}
		$objPHPExcel-> getActiveSheet ()
        -> getStyle ($filbor)
		-> getFont ()
		-> setBold ( false ) 
      	-> setName ('Arial') 
      	-> setSize ( 10 ) 
		-> getColor ()
		-> setRGB ('000000');
		$objPHPExcel->getActiveSheet()->getStyle($filbor)->applyFromArray($borders);
		$objPHPExcel->setActiveSheetIndex(0)
		->setCellValueExplicit ("A".$xy,$rowc[2], PHPExcel_Cell_DataType :: TYPE_STRING)
		->setCellValueExplicit ("B".$xy,$row[3], PHPExcel_Cell_DataType :: TYPE_STRING)
		->setCellValueExplicit ("C".$xy,"50", PHPExcel_Cell_DataType :: TYPE_NUMERIC)
		->setCellValueExplicit ("D".$xy,"223", PHPExcel_Cell_DataType :: TYPE_NUMERIC)
		->setCellValueExplicit ("E".$xy,substr($rowc[2],0,2), PHPExcel_Cell_DataType :: TYPE_STRING)
		->setCellValueExplicit ("F".$xy,substr($rowc[2],2,2), PHPExcel_Cell_DataType :: TYPE_STRING)
		->setCellValueExplicit ("G".$xy,substr($rowc[2],4,4), PHPExcel_Cell_DataType :: TYPE_STRING)
		->setCellValueExplicit ("H".$xy,substr($rowc[2],8,4), PHPExcel_Cell_DataType :: TYPE_STRING)
		->setCellValueExplicit ("I".$xy,substr($rowc[2],12,3), PHPExcel_Cell_DataType :: TYPE_STRING)
		->setCellValueExplicit ("J".$xy,date('d-m-Y',strtotime($row[10])), PHPExcel_Cell_DataType :: TYPE_STRING)
		->setCellValueExplicit ("K".$xy,"27488", PHPExcel_Cell_DataType :: TYPE_NUMERIC)
		->setCellValueExplicit ("L".$xy,"1-$row[1]-$row[0]", PHPExcel_Cell_DataType :: TYPE_STRING)
		->setCellValueExplicit ("M".$xy,$row[4], PHPExcel_Cell_DataType :: TYPE_STRING)
		->setCellValueExplicit ("N".$xy,"0", PHPExcel_Cell_DataType :: TYPE_NUMERIC)
		->setCellValueExplicit ("O".$xy,"0", PHPExcel_Cell_DataType :: TYPE_NUMERIC)
		->setCellValueExplicit ("P".$xy,"0", PHPExcel_Cell_DataType :: TYPE_NUMERIC)
		->setCellValueExplicit ("Q".$xy,"2", PHPExcel_Cell_DataType :: TYPE_NUMERIC)
		->setCellValueExplicit ("R".$xy,$tipoestra, PHPExcel_Cell_DataType :: TYPE_STRING)
		->setCellValueExplicit ("S".$xy,$estratot, PHPExcel_Cell_DataType :: TYPE_STRING)
		->setCellValueExplicit ("T".$xy,"1", PHPExcel_Cell_DataType :: TYPE_NUMERIC)
		->setCellValueExplicit ("U".$xy,"0", PHPExcel_Cell_DataType :: TYPE_NUMERIC)
		->setCellValueExplicit ("V".$xy,"30", PHPExcel_Cell_DataType :: TYPE_NUMERIC)
		->setCellValueExplicit ("W".$xy,"1", PHPExcel_Cell_DataType :: TYPE_NUMERIC)
		->setCellValueExplicit ("X".$xy,"000000000.0000", PHPExcel_Cell_DataType :: TYPE_STRING)
		->setCellValueExplicit ("Y".$xy,$subcon, PHPExcel_Cell_DataType :: TYPE_NUMERIC)
		->setCellValueExplicit ("Z".$xy,"0", PHPExcel_Cell_DataType :: TYPE_NUMERIC)
		->setCellValueExplicit ("AA".$xy,"$row[13]", PHPExcel_Cell_DataType :: TYPE_NUMERIC)
		->setCellValueExplicit ("AB".$xy,"0", PHPExcel_Cell_DataType :: TYPE_NUMERIC)
		->setCellValueExplicit ("AC".$xy,"0", PHPExcel_Cell_DataType :: TYPE_NUMERIC)
		->setCellValueExplicit ("AD".$xy,$row[11], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
		->setCellValueExplicit ("AE".$xy,$row[12], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
		->setCellValueExplicit ("AF".$xy,"2", PHPExcel_Cell_DataType :: TYPE_NUMERIC)
		->setCellValueExplicit ("AG".$xy,"2", PHPExcel_Cell_DataType :: TYPE_NUMERIC)
		->setCellValueExplicit ("AH".$xy,$row[7], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
		->setCellValueExplicit ("AI".$xy,utf8_encode($rowc[4]), PHPExcel_Cell_DataType :: TYPE_STRING)
		->setCellValueExplicit ("AJ".$xy,"0", PHPExcel_Cell_DataType :: TYPE_NUMERIC)
		->setCellValueExplicit ("AK".$xy,"0", PHPExcel_Cell_DataType :: TYPE_NUMERIC)
		->setCellValueExplicit ("AL".$xy,"0", PHPExcel_Cell_DataType :: TYPE_NUMERIC)
		->setCellValueExplicit ("AM".$xy,"0", PHPExcel_Cell_DataType :: TYPE_NUMERIC);
		$xy++;
	}
	// Renombrar Hoja
	//$objPHPExcel->getActiveSheet()->setTitle('Listado Asistencia');
	// Establecer la hoja activa, para que cuando se abra el documento se muestre primero.
	$objPHPExcel->setActiveSheetIndex(0);
	// Se modifican los encabezados del HTTP para indicar que se envia un archivo de Excel.
	header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
	header('Content-Disposition: attachment;filename="Reporte Aseo.xlsx"');
	header('Cache-Control: max-age=0');
	$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
	$objWriter->save('php://output');
	exit;
?>
