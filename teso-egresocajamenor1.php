<?php
	require"comun.inc";
	require"funciones.inc";
	require "conversor.php";
	require "validaciones.inc";
	session_start();
	$linkbd_V7 = conectar_v7();
	cargarcodigopag($_GET[codpag],$_SESSION["nivel"]);
?>
<!DOCTYPE >
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="es">
	<head>
	 	<meta http-equiv="Content-Type" content="text/html" charset="iso-8859-1"/>
        <meta http-equiv="X-UA-Compatible" content="IE=9"/>
		<title>:: SPID - Tesoreria</title>
		<link href="css/css2.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
        <link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<script type="text/javascript" src="css/programas.js"></script>
		<script type="text/javascript" src="css/calendario.js"></script>

		<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">

        <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
        <script type="text/javascript" src="bootstrap/fontawesome.5.11.2/js/all.js"></script>

		<script>
			function validar(){document.form2.submit();}

			
			function validarTipo(){
				document.form2.validaTipo.value=1;
				
				document.form2.submit();

			}

			function validarReintegro(){
				document.form2.buscaReintegro.value=1;
				document.form2.submit();
			}
			
			function agregarDetalle()
			{
				var tipo_egreso = document.form2.tipoegreso.value;
				var valorAcuerdo = document.form2.valorac.value;
				var valor = document.form2.valor.value;
				
				//var document;
				if(document.form2.cc.value!="" &&  Number(valor)>0 )
				{
					if(Number(valor) > Number(valorAcuerdo)){
						despliegamodalm('visible','2','El valor que va agregar no puede ser mas al valor del acuerdo.');
					}else{
						document.form2.agregadet.value=1;
						//document.form2.chacuerdo.value=2;
						document.form2.submit();
					}

					
				}
				else {despliegamodalm('visible','2','Falta informacion para poder Agregar');}
			}
			/* 
			function eliminar(variable)
			{
				if (confirm("Esta Seguro de Eliminar"))
				{
					document.form2.elimina.value=variable;
					//eli=document.getElementById(elimina);
					vvend=document.getElementById('elimina');
					//eli.value=elimina;
					vvend.value=variable;
					document.form2.submit();
				}
			} */

			function eliminar(variable)
			{
				document.getElementById('elimina').value=variable;
				despliegamodalm('visible','4','Esta Seguro de Eliminar','2');
			}
			
			function guardar()
			{
				valoracuerdo=document.form2.valorac2.value;
				valortotal=document.form2.totalc.value;
				tipegreso = document.form2.tipoegreso.value;
				if(tipegreso == "apertura")
				{
					if(parseFloat(valoracuerdo) >= parseFloat(valortotal))
					{
						if (document.form2.fecha.value!='' && document.form2.detallegreso.value!='' && document.form2.tercero.value != '')
						{
							despliegamodalm('visible','4','Esta Seguro de Guardar','1');
							/* if (confirm("Esta Seguro de Guardar"))
							{
								document.form2.oculto.value=2;
								document.form2.submit();
							} */
						}
						else
						{
							despliegamodalm('visible','2','Faltan datos para completar el egreso');
							/* alert('Faltan datos para completar el registro'); */
							document.form2.fecha.focus();
							document.form2.fecha.select();
						}
					}
					else
					{
						/* alert('El valor del comprobante no debe ser mayor al acto administrativo'); */
						despliegamodalm('visible','2','Faltan datos para completar el egreso');
					}
				}
				else
				{
					if(parseFloat(valoracuerdo) == parseFloat(valortotal))
					{
						if (document.form2.fecha.value!='' && document.form2.detallegreso.value!='')
						{
                            despliegamodalm('visible','4','Esta Seguro de Guardar','1');
							//despliegamodalm('visible','4','Esta Seguro de Guardar','1');
							/* if (confirm("Esta Seguro de Guardar"))
							{
								document.form2.oculto.value=2;
								document.form2.submit();
							} */
						}
						else
						{
							despliegamodalm('visible','2','Faltan datos para completar el egreso');
							/* alert('Faltan datos para completar el registro'); */
							document.form2.fecha.focus();
							document.form2.fecha.select();
						}
					}
					else
					{
						despliegamodalm('visible','2','El valor del comprobante no debe ser diferente al valor del reintegro');
					}
				}
	
			}
			function despliegamodal2(_valor,_num)
			{
				document.getElementById("bgventanamodal2").style.visibility=_valor;
				if(_valor=="hidden"){document.getElementById('ventana2').src="";}
				else 
				{
					switch(_num)
					{
						case '1':	document.getElementById('ventana2').src="ordenpago-ventana1.php?vigencia="+document.form2.vigencia.value;break;
						case '2':	document.getElementById('ventana2').src="cuentasbancarias-ventana01.php?tipoc=C";break;
						case '3':	document.getElementById('ventana2').src="cuentasbancarias-ventana01.php?tipoc=D";break;
						case '4':	document.getElementById('ventana2').src="reversar-egreso.php?vigencia="+document.form2.vigencia.value;break;
					}
				}
			}
			function despliegamodalm(_valor,_tip,mensa,pregunta)
			{
				document.getElementById("bgventanamodalm").style.visibility=_valor;
				if(_valor=="hidden"){document.getElementById('ventanam').src="";}
				else
				{
					switch(_tip)
					{
						case "1":	document.getElementById('ventanam').src="ventana-mensaje1.php?titulos="+mensa;break;
						case "2":	document.getElementById('ventanam').src="ventana-mensaje3.php?titulos="+mensa;break;
						case "3":	document.getElementById('ventanam').src="ventana-mensaje2.php?titulos="+mensa;break;
						case "4":	document.getElementById('ventanam').src="ventana-consulta1.php?titulos="+mensa+"&idresp="+pregunta;break;	
					}
				}
			}
			function funcionmensaje()
			{
				var _cons=document.getElementById('idcomp').value;
				document.location.href = "teso-egresocajamenorver1.php?idegre="+_cons;
			}

			function respuestaconsulta(pregunta)
			{
				switch(pregunta)
				{
					case "1":	document.form2.oculto.value="2";
								document.form2.submit();
								break;
					case "2": 	
								document.form2.oculto.value="3";
								document.form2.submit();
								break;
				}
			}
			
			function pdf()
			{
				document.form2.action="pdfegresocajamenor.php";
				document.form2.target="_BLANK";
				document.form2.submit(); 
				document.form2.action="";
				document.form2.target="";
			}
			

			function buscater(e)
			{
				if (document.form2.tercero.value!="")
				{
					document.form2.bt.value='1';
					document.form2.submit();
				}
			}

			function cambiaVigencia(){
				var fecha = document.form2.fecha.value;
				var arrayFecha = fecha.split('/');
				document.form2.vigencia.value = arrayFecha[2];
				//alert(fecha);
			}
		</script>

		<?php titlepag();?>
	</head>
	<body>
		<IFRAME src="alertas.php" name="alertas" id="alertas" style="display:none"></IFRAME>
		<span id="todastablas2"></span>
		<table>
			<tr><script>barra_imagenes("teso");</script><?php cuadro_titulos();?></tr>	 
			<tr><?php menu_desplegable("teso");?></tr>
			<tr class="cinta">
				<td colspan="3" class="cinta"><img src="imagenes/add.png" title="Nuevo" onClick="location.href='teso-egresocajamenor1.php'" class="mgbt"/><img src="imagenes/guarda.png" title="Guardar" onClick="guardar()" class="mgbt"/><img src="imagenes/busca.png"  title="Buscar" onClick="location.href='teso-buscaegresocajamenor1.php'" class="mgbt"/><img src="imagenes/agenda1.png" title="Agenda" onClick="mypop=window.open('plan-agenda.php','','');mypop.focus()" class="mgbt"/><img src="imagenes/nv.png"title="Nueva Ventana" onClick="mypop=window.open('teso-principal.php','','');mypop.focus();" class="mgbt"/><img src="imagenes/iratras.png" title="Atr&aacute;s" onClick="location.href='teso-gestioncajamenor.php'" class="mgbt"/></td>
			</tr>
		</table>
		<div id="bgventanamodalm" class="bgventanamodalm">
            <div id="ventanamodalm" class="ventanamodalm">
                <IFRAME src="" name="ventanam" marginWidth=0 marginHeight=0 frameBorder=0 id="ventanam" frameSpacing=0 style=" width:700px; height:130px; top:200; overflow:hidden;"> 
                </IFRAME>
            </div>
        </div>
		<tr>
			<td colspan="3" class="tablaprin" align="center"> 

				<?php
	  			//*********** cuenta origen va al credito y la destino al debito
				if(!$_POST['oculto'])
				{
					$sqlr = "select cuentacajamenor from tesoparametros";
					$res = mysqli_query($linkbd_V7, $sqlr);
					while ($row = mysqli_fetch_row($res)) 
					{
						$_POST['ceuntacajamenor'] = $row[0];
					}
					$check1 = "checked";
					$fec = date("d/m/Y");
					$_POST['fecha'] = $fec; 		 		  			 
					$vigusu = date('Y');
					$_POST['vigencia'] = $vigusu; 		
					$sqlr = "select max(id) from tesoegresocajamenor";
					$res = mysqli_query($linkbd_V7, $sqlr);
					$consec = 0;
					while($r = mysqli_fetch_row($res))
					{
		  				$consec = $r[0];	  
	 				}
					$consec += 1;
					$_POST['idcomp'] = $consec;						 
				}
				?>
 				<form name="form2" method="post" action=""> 
 					<?php
 					
 					//***** busca tercero
			 		if($_POST['bt']=='1')
			 		{
			  			$nresul = buscatercero($_POST['tercero']);	
			  			if($nresul!='')
			   			{
			  				$_POST['ntercero']=$nresul;		 
			  			}
						else
						{
			  				$_POST['ntercero']="";
			  			}
			 		}
 
 					?>
    				<table class="inicio" align="center" >
						<tr >
							<td class="titulos" colspan="12">Egreso caja menor</td>
							<td class="cerrar" >
								<a href="teso-principal.php">Cerrar</a>
							</td>
						</tr>
						<tr>
							<td style="width:11%;" class="saludo1" >Numero Egreso:</td>
							<td style="width:15%;">
								<input name="idcomp" id="idcomp" type="text" style="width:90%;" value="<?php echo $_POST['idcomp']?>" readonly> 
							</td>
							<td style="width:8%;" class="saludo1">Fecha: </td>
							<td style="width:15%;">
								<input name="fecha" type="text" value="<?php echo $_POST['fecha']?>" maxlength="10"  onKeyPress="javascript:return solonumeros(event)" onKeyUp="return tabular(event,this)"  id="fc_1198971545" onChange="cambiaVigencia()" onKeyDown="mascara(this,'/',patron,true)" title="DD/MM/YYYY">&nbsp;<a href="#" onClick="displayCalendarFor('fc_1198971545');" title="Calendario"><img src="imagenes/calendario04.png" style="width:20px;"/></a>   
								
							</td>
							<td style="width:8%;" class="saludo1">Vigencia: </td>
							<td style="width:10%;">
								<input name="vigencia" type="text" value="<?php echo $_POST['vigencia']?>" maxlength="2"  onKeyPress="javascript:return solonumeros(event)" onKeyUp="return tabular(event,this)" readonly> 
							</td>
							<td class="saludo1" style="width:2.8cm;">Tipo:</td>
							<td style="width:14%">
								<select name="tipoegreso" id="tipoegreso" onChange="validarTipo();" onKeyUp="return tabular(event,this)" style="width:100%">
									<option value="">Seleccione ...</option>
									<option value="apertura" <?php if($_POST['tipoegreso']=='apertura') echo "SELECTED"?>>Apertura</option>
									<option value="reintegro" <?php if($_POST['tipoegreso']=='reintegro') echo "SELECTED"?>>Reintegro</option>
								</select>
							</td>
						</tr>
        				<?php
						if($_POST['tipoegreso']=='apertura')
						{
            				echo "
                    				<tr>
										<td class='saludo1'>Acto Administrativo:</td>
										<td  valign='middle' style='width:20%;'>
										<input type='hidden' name='consulta' id='consulta' value='$_POST[consulta]'>
										<select name='acuerdo' style='width:100%;' onChange='validar()' onKeyUp='return tabular(event,this)'>
										<option value='-1'>Seleccione</option>
           							";
            				$sqlr = "Select * from tesoacuerdo where estado='S'";
            				$resp = mysqli_query($linkbd_V7, $sqlr);
            
            				while ($row = mysqli_fetch_row($resp)) 
							{
                				echo "<option value=$row[0] ";
								$i=$row[0];
								if($i==$_POST['acuerdo'])
								{
									echo "SELECTED";
									$_POST['valorac'] = $row[5];
									$_POST['valorac2'] = $row[5];
									$_POST['valorac'] = number_format($_POST['valorac'],2,'.',',');
								}
								echo ">".$row[1]."-".$row[2]."</option>";	
            				}
            				echo "
									</select>
									<td style='width:10%;' class='saludo1'>
													<input type='hidden' value='1'>Valor Acuerdo:</td>
									<td style='width:10%;'>
									<input name='valorac' type='text' value='$_POST[valorac]' onKeyPress='javascript:return solonumeros(event)' onKeyUp='return tabular(event,this)' readonly>
									<input type='hidden'  id='valorac2' name='valorac2' value='$_POST[valorac2]'>
									</td>  
									";	
            
        				}
						else if($_POST['tipoegreso']=='reintegro')
						{
							echo "
							<tr>
								<td class='saludo1'>Reintegro:</td>
								<td  valign='middle' style='width:20%;'>
								<input type='hidden' name='consultareintegro' id='consultareintegro' value='$_POST[consultareintegro]'>
								<select name='reintegro' style='width:90%;' onChange='validarReintegro()' onKeyUp='return tabular(event,this)'>
												<option value='-1'>Seleccione</option>
							";
							$sqlr="Select * from tesocontabilizacajamenor where finaliza='1'";
							$resp = mysqli_query($linkbd_V7, $sqlr);
						
							while ($row =mysqli_fetch_row($resp)) 
							{
								$sqlrCaja="Select * from tesoegresocajamenor where reintegro='$row[0]'";
								$respCaja = mysqli_query($linkbd_V7, $sqlrCaja);
							
								$rowCaja =mysqli_fetch_row($respCaja);
								if($rowCaja[0] == ''){
									echo "<option value=$row[0] ";
									$i=$row[0];
									if($i==$_POST['reintegro'])
									{
											echo "SELECTED";
											$_POST['valorreintegro'] = $row[3];
											$_POST['valorreintegro2'] = $row[3];
											$_POST['valorreintegro'] = number_format($_POST['valorreintegro'],2,'.',',');
									}
									echo ">".$row[0]."-".$row[1]."</option>";
								}
										
							}
							echo "
							</select>
							<td style='width:10%;' class='saludo1'>
							<input type='hidden' value='1'>Valor Reintegro:</td>
							<td style='width:10%;'>
							<input name='valorreintegro' type='text' value='$_POST[valorreintegro]' onKeyPress='javascript:return solonumeros(event)' onKeyUp='return tabular(event,this)' readonly>
							<input type='hidden'  id='valorreintegro2' name='valorac2' value='$_POST[valorreintegro2]'>
							</td>  
							";	
						}
						if($_POST['tipoegreso']!='')
						{
							?>
							<td class="saludo1" style="width:2.8cm;">Forma de Pago:</td>
							<td style="width:14%">
								<select name="tipop" id="tipop" onChange="validar();" onKeyUp="return tabular(event,this)" style="width:100%">
									<option value="">Seleccione ...</option>
									<option value="cheque" <?php if($_POST['tipop']=='cheque') echo "SELECTED"?>>Cheque</option>
									<option value="transferencia" <?php if($_POST['tipop']=='transferencia') echo "SELECTED"?>>Transferencia</option>
									<option value="caja" <?php if($_POST['tipop']=='caja') echo "SELECTED"?>>Efectivo</option>
								</select> 
							</td>
							</tr>
							<?php
						}
						if($_POST['tipop']=='cheque') //**** if del cheques
						{
							if($_POST['escuentas']=='' || $_POST['escuentas']=='tran')
							{
								$_POST['escuentas']='che';
								$_POST['cb']='';
								$_POST['nbanco']='';
								$_POST['banco']='';
								$_POST['tcta']='';
								$_POST['ter']='';
								$_POST['ncheque']='';
							}
							echo" 
								<tr>
									<td class='saludo1'>Cuenta Bancaria:</td>
									<td>
										<input type='text' name='cb' id='cb' value='$_POST[cb]' style='width:80%'/>
										&nbsp;<a onClick=\"despliegamodal2('visible','2');\"  style='cursor:pointer;' title='Listado Cuentas Bancarias'><img src='imagenes/find02.png' style='width:20px;'/></a>
									</td>
									<td colspan='2'>
										<input type='text' id='nbanco' name='nbanco' value='$_POST[nbanco]' style='width:100%' readonly>
									</td>
									<td class='saludo1'>Cheque:</td>
									<td>
										<input type='text' id='ncheque' name='ncheque' value='$_POST[ncheque]' style='width:100%'/>
										<input type='hidden' id='nchequeh' name='nchequeh' value='$_POST[nchequeh]'/>
									</td>
								</tr>
								<input type='hidden' name='banco' id='banco' value='$_POST[banco]'/>
								<input type='hidden' name='tcta' id='tcta' value='$_POST[tcta]'/>
								<input type='hidden' name='ter' id='ter' value='$_POST[ter]'/>";
								//-----------Asignacion del consecutivo de cheque----------------------------
							if($_POST['cb']!=''){
								$sqlc="select cheque from tesocheques where cuentabancaria='$_POST[cb]' and estado='S' order by cheque asc";
								//echo $sqlc;
								$resc = mysqli_query($linkbd_V7, $sqlr);
								$rowc =mysqli_fetch_row($resc);
								if($rowc[0]==''){
									
								}else{
									echo "<script>document.form2.ncheque.value='".$rowc[0]."';</script>";
								}
							}	
					
					
						}//cierre del if de cheques
						if($_POST['tipop']=='transferencia')//**** if del transferencias
						{
							if($_POST['escuentas']=='' || $_POST['escuentas']=='che')
							{
								$_POST['escuentas']='tran';
								$_POST['cb']='';
								$_POST['nbanco']='';
								$_POST['banco']='';
								$_POST['tcta']='';
								$_POST['ter']='';
								$_POST['ntransfe']='';
							}
							echo"
								<tr>
									<td class='saludo1'>Cuenta Bancaria:</td>
									<td>
										<input type='text' name='cb' id='cb' value='$_POST[cb]' style='width:80%'/>
										&nbsp;<a onClick=\"despliegamodal2('visible','3');\"  style='cursor:pointer;' title='Listado Cuentas Bancarias'><img src='imagenes/find02.png' style='width:20px;'/></a>
									</td>
									<td colspan='2'>
										<input type='text' id='nbanco' name='nbanco' value='$_POST[nbanco]' style='width:100%' readonly>
									</td>
									<td class='saludo1'>No Transferencia:</td>
									<td><input type='text' id='ntransfe' name='ntransfe' value='$_POST[ntransfe]' style='width:100%'></td>
								</tr>
								<input type='hidden' name='banco' id='banco' value='$_POST[banco]'/>
								<input type='hidden' name='tcta' id='tcta' value='$_POST[tcta]'/>
								<input type='hidden' name='ter' id='ter' value='$_POST[ter]'/>";
						}//cierre del if de cheques
						if($_POST['tipop']=='caja')
						{
							$sqlr = "select cuentacaja from tesoparametros where estado='S'";
							$res = mysqli_query($linkbd_V7, $sqlr);
							$row = mysqli_fetch_row($res);
							$_POST['banco']=$row[0];
							// echo $_POST[banco];
							
							echo "
								<input type='hidden' name='cb' id='cb' value='$_POST[cb]' style='width:80%;'/>
								<input type='hidden' id='nbanco' name='nbanco' style='width:100%;' value='$_POST[nbanco]'  readonly>
								<input type='hidden' id='ntransfe' name='ntransfe' value='$_POST[ntransfe]' style='width:100%;'>
								<input type='hidden' name='banco' id='banco' value='$_POST[banco]'/>
								<input type='hidden' id='ter' name='ter' value='$_POST[ter]'/>";
							
						}
        				?>
        
	  					<tr>
							
	     					<td style="width:8%;" class="saludo1">Tercero:</td>
							<td style="width:20%;" >
								<input id="tercero" type="text" name="tercero" onKeyUp="return tabular(event,this)" onBlur="buscater(event)" style="width:80%;" value="<?php echo $_POST['tercero']?>" >
								<input type="hidden" value="0" name="bt">
								&nbsp;<a href="#" onClick="mypop=window.open('terceros-ventana.php','','menubar=0,scrollbars=yes, toolbar=no, location=no, width=900,height=500px');mypop.focus();"><img src="imagenes/find02.png" style="width:20px;"></a>
								
							</td>
							<td colspan="2">
								<input  id="ntercero" style="width:99%;"  name="ntercero" type="text" value="<?php echo $_POST['ntercero']?>"  readonly>
							</td>

							<td style="width:11%;" class="saludo1">Detalle:</td>
							<td colspan="4">
								<input type="text" id="detallegreso" name="detallegreso" style="width:99%;" value="<?php echo $_POST['detallegreso']?>" >
								<input type="hidden" value="0" name="buscaReintegro">
								<input type="hidden" value="0" name="validaTipo">
								<input type="hidden" value="1" name="oculto">
								<input type="hidden" name="escuentas" id="escuentas" value="<?php echo $_POST['escuentas'];?>"/>
							</td>
        				</tr>
						<?php
						if($_POST['tipoegreso'] == 'apertura'){

						?>
        				<tr>
						<td style="width:11%;" class="saludo1">Centro Costo:</td>
							<td style="width:15%;">
								<select name="cc" style="width:90%;" onKeyUp="return tabular(event,this)" >
									<?php
									$nombreCc = array();
									$sqlr="select *from centrocosto where estado='S'";
									$res=mysqli_query($linkbd_V7, $sqlr);
									while ($row =mysqli_fetch_row($res)) 
									{
										
										$nombreCc[$row[0]] = $row[1];
										echo "<option value=$row[0] ";
										$i=$row[0];
										if($i==$_POST['cc'])
										{
											echo "SELECTED";
										}
										echo ">".$row[0]." - ".$row[1]."</option>";	 	 
									}	 	
									?>
   								</select>
	 						</td>
							
							<td style="width:8%;" class="saludo1" >Valor apertura:</td>
							<td style="width:10%;">
								<input type="text" id="valor" name="valor" value="<?php echo $_POST['valor']?>" > 
								
								
								<input type="hidden" value="0" name="agregadet">
								

							</td>
							<td>
								<button type="button" class="btn btn-success btn-sm" onClick="agregarDetalle()">Agregar Detalle</button>
							</td>
						</tr>
						<?php
						}
						?>
    				</table>
      				<?php
					if(!$_POST['oculto'])
					{
						?>
        				<script>
							document.form2.fecha.focus();
							document.form2.fecha.select();
						</script>
        				<?php   
					}

					if($_POST['validaTipo'] == '1'){
						
						unset($_POST['dnombreCc']);
						unset($_POST['dvalor']);
						unset($_POST['diva']);
						unset($_POST['dcc']);
						unset($_POST['dcuentaCaja']);

						$_POST['validaTipo']=0;
					}

					if($_POST['buscaReintegro'] == '1'){

						$nombreCc = array();
						$sqlr="select *from centrocosto where estado='S'";
						$res=mysqli_query($linkbd_V7, $sqlr);
						while ($row =mysqli_fetch_row($res)) 
						{
							$nombreCc[$row[0]] = $row[1];
						}
						$sqlr = "SELECT cc, valor, conceptocontable, cuenta_cja FROM tesocontabilizacajamenor_det WHERE  id_cajamenor = $_POST[reintegro]";
						$res = mysqli_query($linkbd_V7, $sqlr);
						//echo $sqlr;
						while($row = mysqli_fetch_row($res))
						{
							$_POST['dcc'][] = $row[0];
							$_POST['dnombreCc'][] = $nombreCc[$row[0]];
							$_POST['dvalor'][] = $row[1];
							$_POST['dcuentaCaja'][] = $row[3];
						}

						$_POST['buscaReintegro']=0;
					}
					if ($_POST['agregadet']=='1')
					{
						/* $_POST['cc'] = array();
						$_POST['nombreCc'] = array();
						$_POST['valor'] = array(); */

						$ch=esta_en_array($_POST['dcc'],$_POST['cc']);
						if($ch!='1'){
							$totalAgregado = 0;
							foreach($_POST['dvalor'] as $valor){
								$totalAgregado += $valor;
							}
							
							/* echo $totalAgregado."<br>";
							echo $_POST['valor']."<br>"; */
							$tot = $totalAgregado + $_POST['valor'];
							if($tot > $_POST['valorac2']){
								echo "<script>despliegamodalm('visible','2','El valor que va agregar no puede ser mas al valor del acuerdo.')</script>";
							}else{
								$_POST['dcc'][] = $_POST['cc'];
								$_POST['dnombreCc'][] = $nombreCc[$_POST['cc']];
								$_POST['dvalor'][] = $_POST['valor'];
							}

							echo"
                                <script>							
                                    document.form2.valor.value='';
                                </script>";


						}else{
							echo"<script>despliegamodalm('visible','2','Ya existe este Centro de Costo');</script>";
						}	
						$_POST['agregadet']=0;
						
					}
	  		
					if ($_POST['oculto']=='3')
                    { 
                        $posi=$_POST['elimina'];
                        
                        unset($_POST['dcc'][$posi]);
                        unset($_POST['dnombreCc'][$posi]);
                        unset($_POST['dvalor'][$posi]);		 
                        $_POST['dcc']= array_values($_POST['dcc']); 
                        $_POST['dnombreCc']= array_values($_POST['dnombreCc']); 
                        $_POST['dvalor']= array_values($_POST['dvalor']); 			 		 	 	
                        $_POST['elimina']='';	 		 		 		 
                    }
			 
					?>  	  
					<input type='hidden' name='elimina' id='elimina'>    
	  				<div class="subpantallac2">
	   					<table class="inicio">
	   						<tr>
								<td colspan="8" class="titulos">Detalle</td>
							</tr>                  
       						<?php
	   						if($_POST['todos']==1)
							$checkt='checked';
							else
							$checkt='';
	   						?>
							<tr>
								<td class="titulos2">Centro costo</td>
								<td class="titulos2">Nombre Centro costo</td>
								<td class="titulos2" style="width: 10px;">Valor</td>
								<?php if($_POST['tipoegreso'] == 'apertura'){
									echo "<td class='titulos2' style='width:5%'>Eliminar</td>";
								}else{
									echo "<td class='titulos2' style='width:12%'>Cuenta Caja</td>";
								} 
								?>
							</tr>
							<?php
							$_POST['totalc']=0;
							$iter='saludo1a';
							$iter2='saludo2';
							for ($x=0;$x<count($_POST['dcc']);$x++)
							{
		 						$chk = ''; 
								$ch = esta_en_array($_POST['dcc'],$_POST['dcc'][$x]);
								if($ch=='1')
			 					{
									$chk="checked";
									//echo "ch:$x".$chk;
									$_POST['totalc']=$_POST['totalc']+$_POST['dvalor'][$x];
									// $_POST[totalcf]=number_format($_POST[totalc],2,".",",");
			 					}
             					echo "
                					<input type='hidden' name='dcc[]' value='".$_POST['dcc'][$x]."'/>
                					<input type='hidden' name='dnombreCc[]' value='".$_POST['dnombreCc'][$x]."'/>
									<input type='hidden' name='dvalor[]' value='".$_POST['dvalor'][$x]."'/>
									<input type='hidden' name='dcuentaCaja[]' value='".$_POST['dcuentaCaja'][$x]."'/>
									<tr class='$iter'>
										<td>".$_POST['dcc'][$x]."</td>
										<td>".$_POST['dnombreCc'][$x]."</td>
										<td>".$_POST['dvalor'][$x]."</td>
										";
										if($_POST['tipoegreso'] == 'apertura'){
											echo "									 
											<td style='text-align:center;'>
												<a style='cursor:pointer' onclick='eliminar($x)'>
													<img src='imagenes/del.png'>
												</a>
											</td>";
										}
										else{
											echo "									 
												<td>".$_POST['dcuentaCaja'][$x]."</td>";
										}
										echo "
									</tr>";
									$aux=$iter;
									$iter=$iter2;
									$iter2=$aux;
							}
							$resultado = convertir($_POST['totalc']);
							$_POST['letras']=$resultado." PESOS M/CTE";
							$_POST['totalcf']=number_format($_POST['totalc'],2,".",",");
							echo "<tr>
										<td></td>
										<td class='saludo2'>Total</td>
										<td class='saludo2'>
											<input name='totalcf' type='text' value='$_POST[totalcf]' readonly>
											<input name='totalc' type='hidden' value='$_POST[totalc]'>
											<input name='valoregreso' type='hidden' value='$_POST[valoregreso]'>
										</td>
									</tr>
									<tr>
										<td  class='saludo1'>Son:</td> 
										<td colspan='5' class='saludo1'>
											<input name='letras' type='text' value='$_POST[letras]' size='90'>
										</td>
									</tr>";
							?>
							<script>
								document.form2.valoregreso.value=<?php echo $_POST['totalc'];?>;		
										
								//calcularpago();
        					</script>
	   					</table>
					</div>
	  				<?php
					if($_POST['oculto']=='2')
					{
						$sqlrCajaMenor = "select cuentacajamenor from tesoparametros";
						$resCajaMenor = mysqli_query($linkbd_V7, $sqlrCajaMenor);
						$rowCajaMenor = mysqli_fetch_row($resCajaMenor);
						
						if($rowCajaMenor != ''){

						
							preg_match("/([0-9]{2})\/([0-9]{2})\/([0-9]{4})/", $_POST['fecha'], $fecha);
							$fechaf = $fecha[3]."-".$fecha[2]."-".$fecha[1];
							$bloq = bloqueos($_SESSION['cedulausu'],$fechaf);
							if($bloq >= 1)
							{
								$sqlr = "select count(*) from tesoegresocajamenor where id=$_POST[idcomp] ";	
								$res=mysqli_query($linkbd_V7, $sqlr);
								//echo $sqlr;
								while($r=mysqli_fetch_row($res))
								{
									$numerorecaudos=$r[0];
								}
								if($numerorecaudos==0)
								{
									//************CREACION DEL COMPROBANTE CONTABLE ************************
									//***busca el consecutivo del comprobante contable
									$consec=0;
									$sqlr="select max(id) from tesoegresocajamenor";
									//echo $sqlr;
									$res=mysqli_query($linkbd_V7, $sqlr);
									while($r=mysqli_fetch_row($res))
									{
										$consec=$r[0];	  
									}
									$consec+=1;
									//***cabecera comprobante SSF GASTO
									$sqlr = "insert into comprobante_cab (numerotipo,tipo_comp,fecha,concepto,total,total_debito,total_credito,diferencia,estado) values ($consec,37,'$fechaf','$_POST[detallegreso]',0,$_POST[totalc],$_POST[totalc],0,'1')";
									mysqli_query($linkbd_V7, $sqlr);
									//echo "<br>$sqlr ";
									$_POST['idcomp']=$idcomp;
									echo "<input type='hidden' name='ncomp' value='$idcomp'>";
									//******************* DETALLE DEL COMPROBANTE CONTABLE  *********************
									
									for ($y = 0; $y < count($_POST['dcc']); $y++)
									{
										//CONTABILIZACION
										$sqlr="insert into comprobante_det (id_comp,cuenta,tercero,centrocosto,detalle,cheque,valdebito,valcredito,estado,vigencia) values ('37 $consec','$_POST[banco]','".$_POST['tercero']."','".$_POST['dcc'][$y]."','Egreso caja menor ".$_POST['dnombreCc'][$y]."','',0,".$_POST['dvalor'][$y].",'1','".$_POST['vigencia']."')";
										mysqli_query($linkbd_V7, $sqlr);
                                        if($_POST['tipoegreso'] == 'apertura'){
                                            $sqlr="insert into comprobante_det (id_comp,cuenta,tercero,centrocosto,detalle,cheque,valdebito,valcredito,estado,vigencia) values ('37 $consec','$rowCajaMenor[0]','".$_POST['tercero']."','".$_POST['dcc'][$y]."','Egreso caja menor ".$_POST['dnombreCc'][$y]."','',".$_POST['dvalor'][$y].",0,'1','".$_POST['vigencia']."')";
										    mysqli_query($linkbd_V7, $sqlr);
                                        }else{
                                            $sqlr="insert into comprobante_det (id_comp,cuenta,tercero,centrocosto,detalle,cheque,valdebito,valcredito,estado,vigencia) values ('37 $consec',".$_POST['dcuentaCaja'][$y].",'".$_POST['tercero']."','".$_POST['dcc'][$y]."','Egreso caja menor ".$_POST['dnombreCc'][$y]."','',".$_POST['dvalor'][$y].",0,'1','".$_POST['vigencia']."')";
										    mysqli_query($linkbd_V7, $sqlr);
                                        }
										
											
									}
									
									//***ACTUALIZACION  DEL REGISTRO
									
									if($_POST['tipop']=='cheque'){$valtipo=$_POST['ncheque'];}
									else{$valtipo=$_POST['ntransfe'];}
									///*******INCIO DE TABLAS DE TESORERIA **************
									//**** ENCABEZADO ORDEN DE PAGO
									$sqlr = "insert into tesoegresocajamenor (id,fecha,vigencia,id_rp,tipo_egreso,actoadministrativo,valoracto,reintegro,valor_reintegro,cuentabanco,cc,tercero,detalle,valorrp,valorpagar,cuentapagar,estado,formapago,numformapago) values($consec,'$fechaf',".$_POST['vigencia'].",'','$_POST[tipoegreso]','$_POST[acuerdo]','$_POST[valorac2]','$_POST[reintegro]','$_POST[valorreintegro2]',$_POST[banco],'','$_POST[tercero]','$_POST[detallegreso]','',$_POST[totalc],'','S','$_POST[tipop]','$valtipo')";
									if (!mysqli_query($linkbd_V7, $sqlr)){

										echo "<table class='inicio'><tr><td class='saludo1'><center><font color=blue><img src='imagenes\alert.png'>Manejador de Errores de la Clase BD<br><font size=1></font></font><br><p align=center>No se pudo ejecutar la petici�n: <br><font color=red><b>$sqlr</b></font></p>";
										//	 $e =mysql_error($respquery);
										echo "Ocurrio el siguiente problema:<br>";
										//echo htmlentities($e['message']);
										echo "<pre>";
										///echo htmlentities($e['sqltext']);
										// printf("\n%".($e['offset']+1)."s", "^");
										echo "</pre></center></td></tr></table>";

									}else{

										for ($y=0;$y<count($_POST['dcc']);$y++)
										{
											$sqlr = "insert into tesoegresocajamenor_det (id_egreso,cuentap,cc,valor,estado) values ($consec,'','".$_POST['dcc'][$y]."',".$_POST['dvalor'][$y].",'S')";
											//echo "<br>".$sqlr;
											mysqli_query($linkbd_V7, $sqlr);
											/* if (!mysqli_query($linkbd_V7, $sqlr))
											{
												
											} */
										}

										echo "<script>despliegamodalm('visible','1','Se ha almacenado el Egreso con Exito');</script>";
										?>
										<script>
												document.form2.numero.value="";
												document.form2.valor.value=0;
												document.form2.oculto.value=1;
										</script>
										<?php
									}
									//****** DETALLE ORDEN DE PAGO
									
								}
							}
							else
							{
								echo "<div class='inicio'><img src='imagenes\alert.png'> No Tiene los Permisos para Modificar este Documento</div>";
							}
						}else{
							echo "<script>despliegamodalm('visible','2','Falta parametrizar la cuenta de caja. En parametros de tesoreria - caja menor ')</script>";
						}
  						//****fin if bloqueo  
					}//************ FIN DE IF OCULTO************
					?>
					<div id="bgventanamodal2">
						<div id="ventanamodal2">
							<IFRAME  src="" name="buster" marginWidth=0 marginHeight=0 frameBorder=0 id="ventana2" frameSpacing=0 style="left:500px; width:900px; height:500px; top:200;"> 
							</IFRAME>
						</div>
					</div>		
				</form>
 			</td>
		</tr>  
	</table>
	</body>
</html>	 