<?php
require 'comun.inc';
require 'funciones.inc';
session_start();
cargarcodigopag($_GET['codpag'], $_SESSION['nivel']);
header('Cache-control: private'); // Arregla IE 6
header("Cache-control: no-cache, no-store, must-revalidate");
header("Content-Type: text/html;charset=utf8");
date_default_timezone_set('America/Bogota');
titlepag();
?>
<!DOCTYPE >
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="es">

<head>
	<meta http-equiv="Content-type" content="text/html; charset=utf8" />
	<meta http-equiv="X-UA-Compatible" content="IE=9" />
	<meta name="viewport" content="user-scalable=no">
	<title>:: IDEAL 10 - Almacen</title>
	<link rel="shortcut icon" href="favicon.ico"/>
	<link href="css/css2.css?<?php echo date('d_m_Y_h_i_s'); ?>" rel="stylesheet" />
	<link href="css/css3.css?<?php echo date('d_m_Y_h_i_s'); ?>" rel="stylesheet" />
	<link href="css/css4.css?<?php echo date('d_m_Y_h_i_s'); ?>" rel="stylesheet" />
	<link href="css/cssSP.css?<?php echo date('d_m_Y_h_i_s'); ?>" rel="stylesheet" type="text/css" />
	<link href="css/tabs.css?<?php echo date('d_m_Y_h_i_s'); ?>" rel="stylesheet" type="text/css" />
	<link href="css/style.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css"/>
	<script type="text/javascript" src="css/calendario.js?<?php echo date('d_m_Y_h_i_s'); ?>"></script>
	<script src="vue/vue.js"></script>
	<script type="text/javascript" src="css/programas.js"></script>

	<!-- sweetalert2 -->
	<script src="sweetalert2/dist/sweetalert2.min.js"></script>
	<link rel="stylesheet" href="sweetalert2/dist/sweetalert2.min.css">

	<script>
		function cambiaFecha() {
			var fecha = document.getElementById('fecha').value;
			fecArray = fecha.split('/');
			document.getElementById('vigencia').value = fecArray[2];
		}
	</script>
</head>

<body>
	<div class="subpantalla" style="height:640px; width:99.6%; overflow:hidden; height: 95%;">
		<div id="myapp" style="height:inherit;" v-cloak>
			<div>
				<table>
					<tr>
						<script>
							barra_imagenes("inve");
						</script>
						<?php cuadro_titulos(); ?>
					</tr>

					<tr>
						<?php menu_desplegable("inve"); ?>
					</tr>
				</table>
                <div class="bg-white group-btn p-1">
                    <button type="button" onclick="location.href='inve-salidaDirecta'" class="btn btn-white btn-primary-hover d-flex justify-between align-items-center">
                        <span>Nuevo</span>
                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 -960 960 960">
                            <path d="M440-440H200v-80h240v-240h80v240h240v80H520v240h-80v-240Z"></path>
                        </svg>
                    </button>
                    <button type="button" v-on:click="guardar();" class="btn btn-white btn-primary-hover d-flex justify-between align-items-center">
                        <span>Guardar</span>
                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 -960 960 960">
                            <path
                                d="M840-680v480q0 33-23.5 56.5T760-120H200q-33 0-56.5-23.5T120-200v-560q0-33 23.5-56.5T200-840h480l160 160Zm-80 34L646-760H200v560h560v-446ZM480-240q50 0 85-35t35-85q0-50-35-85t-85-35q-50 0-85 35t-35 85q0 50 35 85t85 35ZM240-560h360v-160H240v160Zm-40-86v446-560 114Z">
                            </path>
                        </svg>
                    </button>
                    <button type="button" onclick="location.href='inve-buscar-salidasDirectas'" class="btn btn-white btn-primary-hover d-flex justify-between align-items-center">
                        <span>Buscar</span>
                        <svg class="fill-black group-hover:fill-white w-5 h-5" xmlns="http://www.w3.org/2000/svg"
                            viewBox="0 -960 960 960">
                            <path
                                d="M784-120 532-372q-30 24-69 38t-83 14q-109 0-184.5-75.5T120-580q0-109 75.5-184.5T380-840q109 0 184.5 75.5T640-580q0 44-14 83t-38 69l252 252-56 56ZM380-400q75 0 127.5-52.5T560-580q0-75-52.5-127.5T380-760q-75 0-127.5 52.5T200-580q0 75 52.5 127.5T380-400Z">
                            </path>
                        </svg>
                    </button>
                    <button type="button" onclick="mypop=window.open('plan-agenda','','');mypop.focus()" class="btn btn-white btn-primary-hover d-flex justify-between align-items-center">
                        <span class="group-hover:text-white">Agenda</span>
                        <svg class="fill-black group-hover:fill-white w-5 h-5" xmlns="http://www.w3.org/2000/svg"
                            viewBox="0 -960 960 960">
                            <path d="M560-564v-68q33-14 67.5-21t72.5-7q26 0 51 4t49 10v64q-24-9-48.5-13.5T700-600q-38 0-73 9.5T560-564Zm0 220v-68q33-14 67.5-21t72.5-7q26 0 51 4t49 10v64q-24-9-48.5-13.5T700-380q-38 0-73 9t-67 27Zm0-110v-68q33-14 67.5-21t72.5-7q26 0 51 4t49 10v64q-24-9-48.5-13.5T700-490q-38 0-73 9.5T560-454ZM260-320q47 0 91.5 10.5T440-278v-394q-41-24-87-36t-93-12q-36 0-71.5 7T120-692v396q35-12 69.5-18t70.5-6Zm260 42q44-21 88.5-31.5T700-320q36 0 70.5 6t69.5 18v-396q-33-14-68.5-21t-71.5-7q-47 0-93 12t-87 36v394Zm-40 118q-48-38-104-59t-116-21q-42 0-82.5 11T100-198q-21 11-40.5-1T40-234v-482q0-11 5.5-21T62-752q46-24 96-36t102-12q58 0 113.5 15T480-740q51-30 106.5-45T700-800q52 0 102 12t96 36q11 5 16.5 15t5.5 21v482q0 23-19.5 35t-40.5 1q-37-20-77.5-31T700-240q-60 0-116 21t-104 59ZM280-494Z">
                            </path>
                        </svg>
                    </button>
                    <button type="button" onclick="window.open('inve-principal');" class="btn btn-white btn-primary-hover d-flex justify-between align-items-center">
                        <span>Nueva ventana</span>
                        <svg class="fill-black group-hover:fill-white w-5 h-5" xmlns="http://www.w3.org/2000/svg"
                            viewBox="0 -960 960 960">
                            <path
                                d="M320-120v-80h80v-80H160q-33 0-56.5-23.5T80-360v-400q0-33 23.5-56.5T160-840h640q33 0 56.5 23.5T880-760v400q0 33-23.5 56.5T800-280H560v80h80v80H320ZM160-360h640v-400H160v400Zm0 0v-400 400Z">
                            </path>
                        </svg>
                    </button>
                    <button type="button" onclick="mypop=window.open('<?php echo $url2; ?>','','');mypop.focus();" class="btn btn-white btn-primary-hover d-flex justify-between align-items-center">
                        <span class="group-hover:text-white">Duplicar pantalla</span>
                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 -960 960 960">
                            <path d="M640-160v-360H160v360h480Zm80-200v-80h80v-360H320v200h-80v-200q0-33 23.5-56.5T320-880h480q33 0 56.5 23.5T880-800v360q0 33-23.5 56.5T800-360h-80ZM160-80q-33 0-56.5-23.5T80-160v-360q0-33 23.5-56.5T160-600h480q33 0 56.5 23.5T720-520v360q0 33-23.5 56.5T640-80H160Zm400-603ZM400-340Z">
                            </path>
                        </svg>
                    </button>
                    <button type="button" onclick="window.location.href='inve-menuSalidas'" class="btn btn-success btn-success-hover d-flex justify-between align-items-center">
                        <span>Atras</span>
                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 -960 960 960">
                            <path d="M760-200v-160q0-50-35-85t-85-35H273l144 144-57 56-240-240 240-240 57 56-144 144h367q83 0 141.5 58.5T840-360v160h-80Z">
                            </path>
                        </svg>
                    </button>
                </div>
			</div>

			<div v-if="tipoMovimiento == '2'">
				<table class="inicio grande">
					<tr>
						<td class="titulos" colspan="10">.: Salida directa</td>
						<td class="cerrar" style="width:4%" onClick="location.href='inve-principal.php'">Cerrar</td>
					</tr>

					<tr>
						<td class="tamano01" style="width: 10%;">Tipo de movimiento: </td>
						<td>
							<select name="tipoMovimiento" id="tipoMovimiento" v-model="tipoMovimiento" class="centrarSelect" style="width: 98%;" v-on:change="buscaConsecutivo">
								<option class="aumentarOption" v-bind:value="2">2 - Salida</option>
								<option class="aumentarOption" v-bind:value="4">4 - Reversión salida</option>
							</select>
						</td>

						<td class="tamano01" style="width: 8%;">Consecutivo:</td>
						<td style="width: 5%;">
							<input type="text" name='consecutivo' id='consecutivo' v-model='consecutivo' style="width: 100%; height: 30px; text-align:center;" readonly>
						</td>

						<td class="tamano01" style="width: 8%;">Fecha:</td>
						<td style="width:10%;">
							<input type="text" name="fecha" id="fecha" onchange="cambiaFecha();" value="<?php echo @$_POST['fecha'] ?>" maxlength="10" onKeyUp="return tabular(event,this)" id="fecha" onKeyDown="mascara(this,'/',patron,true)" title="DD/MM/YYYY" style="height:30px;text-align:center;width:100%;" onClick="displayCalendarFor('fecha');" class="colordobleclik" readonly />
						</td>

						<td class="tamano01" style="width: 8%;">Vigencia: </td>
						<td style="width: 5%;">
							<input type="text" name='vigencia' id='vigencia' style="width: 100%; height: 30px; text-align:center;" readonly>
						</td>

						<td class="tamano01" style="width: 8%;">Descripción: </td>
						<td>
							<input type="text" name='descripcion' id='descripcion' v-model='descripcion' style="width: 100%; height: 30px;">
						</td>
					</tr>
				</table>

				<table class="inicio grande">
					<tr>
						<td class="titulos" colspan="10">.: Gestión inventario</td>
					</tr>

					<tr>
						<td class="tamano01" style="width: 10%;">Bodega: </td>
						<td style="width: 15%;">
							<select name="bodega" id="bodega" v-model="bodega" class="centrarSelect" style="width: 98%;">
								<option class="aumentarOption" v-for="bodega in bodegas" v-bind:value="bodega[0]">
									{{ bodega[1] }}
								</option>
							</select>
						</td>

						<td class="tamano01" style="width: 10%;">Centro Costo: </td>
						<td style="width: 10%;">
							<select name="cc" id="cc" v-model="cc" class="centrarSelect" style="width: 98%;">
								<option class="aumentarOption" v-for="centro in centroCostos" v-bind:value="centro[0]">
									{{ centro[0] }} - {{ centro[1] }}
								</option>
							</select>
						</td>

						<td class="tamano01" style="width: 8%;">Articulo: </td>
						<td style="width: 15%;">
							<input type="text" name='articulo' id='articulo' v-model='articulo' style="width: 100%; height: 30px; text-align:center;" class="colordobleclik" v-on:dblclick="ventanaArticulo" v-on:keyup.13="buscarArticulo" autocomplete="off" readonly>
						</td>

						<td>
							<input type="text" name='nombreArticulo' id='nombreArticulo' v-model='nombreArticulo' style="width: 99%; height: 30px; text-align:center;" readonly>
						</td>
					</tr>
				</table>

				<table class="inicio grande">
					<tr>
						<tr>
							<td class="titulos" colspan="10">.: Detalle articulo</td>
						</tr>

					<td class="tamano01" style="width: 8%;">Código articulo:</td>
					<td style="width: 5%;">
						<input type="text" name='articulo2' id='articulo2' v-model='articulo2' style="width: 100%; height: 30px; text-align:center;" readonly>
						<input type="hidden" name="ccArticulo" id="ccArticulo" v-model="ccArticulo">
						<input type="hidden" name="bodegaArticulo" id="bodegaArticulo" v-model="bodegaArticulo">
						<input type="hidden" name="valorUnitario" id="valorUnitario" v-model="valorUnitario">
					</td>

					<td class="tamano01" style="width: 8%;">Nombre articulo:</td>
					<td colspan="2">
						<input type="text" name='nombreArticulo' id='nombreArticulo' v-model='nombreArticulo' style="width: 99%; height: 30px; text-align:center;" readonly>
					</td>

					<td class="tamano01" style="width: 8%;">Unidad medida:</td>
					<td style="width: 8;">
						<input type="text" name='unidadMedida' id='unidadMedida' v-model='unidadMedida' style="height: 30px; text-align:center;" readonly>
					</td>
					</tr>

					<tr>
						<td class="tamano01" style="width: 8%;">Cantidad bodega:</td>
						<td style="width: 5%;">
							<input type="text" name='cantidadBodega' id='cantidadBodega' v-model='cantidadBodega' style="width: 100%; height: 30px; text-align:center;" readonly>
						</td>

						<td class="tamano01" style="width: 8%;">Cantidad salida:</td>
						<td style="width: 5%;">
							<input type="text" name='cantidadSalida' id='cantidadSalida' v-model='cantidadSalida' v-on:change="validaCantidadSalida" style="width: 100%; height: 30px; text-align:center;">
						</td>

						<td class="tamano01" style="width: 10%;">Destino salida: </td>
						<td style="width: 8%;">
							<select name="destinoSalida" id="destinoSalida" v-model="destinoSalida" v-on:change="validaDestino" class="centrarSelect" style="width: 98%;">
								<option class="aumentarOption" value="AG">Gastos</option>
								<option class="aumentarOption" value="AC">Costos</option>
								<option class="aumentarOption" value="AI">Inversión</option>
								<option class="aumentarOption" value="AP">Propiedad, planta y equipo</option>
								<option class="aumentarOption" value="ACC">Contrucciones en curso</option>
								<option class="aumentarOption" value="AM">Maquinaria, planta y equipo en montaje</option>
							</select>
						</td>

						<td v-show="sector_cuentas" class="tamano01" style="width: 8%;">Sector: </td>
						<td v-show="sector_cuentas" style="width: 20%;">
							<select name="sector" id="sector" v-model="sector" class="centrarSelect" style="width: 98%;">
								<option class="aumentarOption" v-for="sector in sectores" v-bind:value="sector[1]">{{ sector[0] }}</option>
							</select>
						</td>

						<td v-show="contrucciones" class="tamano01" style="width: 8%;">Construcciones en curso: </td>
						<td v-show="contrucciones" style="width: 20%;">
							<input type="text" name="descripcionOrdenConstruccion" id="descripcionOrdenConstruccion" v-model="descripcionOrdenConstruccion" v-on:dblclick="desplegaModalContrucciones" style="width: 100%;" class="colordobleclik" autocomplete="off" readonly>
						</td>

						<td v-show="montaje" class="tamano01" style="width: 8%;">Maquinaria en montaje: </td>
						<td v-show="montaje" style="width: 20%;">
							<input type="text" name="descripcionOrdenMontaje" id="descripcionOrdenMontaje" v-model="descripcionOrdenMontaje" v-on:dblclick="desplegaModalMontajes" style="width: 100%;" class="colordobleclik" autocomplete="off" readonly>
						</td>
					</tr>

					<tr>
						<td class="tamano01" style="width: 8%;">Cuenta contable: </td>
						<td style="width: 15%;">
							<input type="text" name='cuentaContable' id='cuentaContable' v-model='cuentaContable' style="width: 100%; height: 30px; text-align:center;" class="colordobleclik" v-on:dblclick="ventanaCuentaContable" readonly>
							<input type="hidden" name="cuenta" id="cuenta" v-model="cuenta">
						</td>

						<td class="tamano01" style="width: 10%;">Centro costo destino: </td>
						<td style="width: 10%;">
							<select name="ccDestino" id="ccDestino" v-model="ccDestino" class="centrarSelect" style="width: 98%;">
								<option class="aumentarOption" v-for="centro in centroCostos" v-bind:value="centro[0]">
									{{ centro[0] }} - {{ centro[1] }}
								</option>
							</select>
						</td>

						<td colspan='2'><em class='botonflechaverde' v-on:click="agregararticulo" style='float:rigth;'>Agregar articulo</em></td>
					</tr>
				</table>

				<div id="cargando" v-if="loading" class="loading">
					<span>Cargando...</span>
				</div>

				<div class="subpantalla" style="height: 30%; overflow-x:hidden;">
					<table>
						<thead>
							<tr>
								<td class="titulos" colspan="20" height="25">Detalle Gestión Inventario - Salida</td>
							</tr>

							<tr class="titulos2">
								<td style="text-align: center; width: 4%">Cod de articulo </td>
								<td style="text-align: center; width: 4%">Nombre articulo </td>
								<td style="text-align: center; width: 4%">Cantidad en bodega </td>
								<td style="text-align: center; width: 4%">Cantidad de salida </td>
								<td style="text-align: center; width: 4%">Unidad medida </td>
								<td style="text-align: center; width: 4%">Bodega </td>
								<td style="text-align: center; width: 4%">CC articulo </td>
								<td style="text-align: center; width: 4%">Cod de cuenta </td>
								<td style="text-align: center; width: 4%">Nombre de cuenta </td>
								<td style="text-align: center; width: 4%">CC salida </td>
								<td style="text-align: center; width: 4%">Valor unitario </td>
								<td style="text-align: center; width: 4%">Valor total </td>
								<td style="text-align: center; width: 4%">Eliminar </td>
							</tr>
						</thead>


						<tbody>
							<tr v-for="(inventario,index) in detalleInventario" v-bind:class="index % 2 ? 'saludo1a' : 'saludo2'" style='text-rendering: optimizeLegibility; text-align:center;'>
								<td style="font: 110% sans-serif; padding-left:10px">{{ inventario[0] }}</td>
								<td style="font: 110% sans-serif; padding-left:10px">{{ inventario[1] }}</td>
								<td style="font: 110% sans-serif; padding-left:10px">{{ inventario[2] }}</td>
								<td style="font: 110% sans-serif; padding-left:10px">{{ inventario[3] }}</td>
								<td style="font: 110% sans-serif; padding-left:10px">{{ inventario[4] }}</td>
								<td style="font: 110% sans-serif; padding-left:10px">{{ inventario[5] }}</td>
								<td style="font: 110% sans-serif; padding-left:10px">{{ inventario[6] }}</td>
								<td style="font: 110% sans-serif; padding-left:10px">{{ inventario[7] }}</td>
								<td style="font: 110% sans-serif; padding-left:10px">{{ inventario[8] }}</td>
								<td style="font: 110% sans-serif; padding-left:10px">{{ inventario[9] }}</td>
								<td style="font: 110% sans-serif; padding-left:10px">{{ inventario[10] }}</td>
								<td style="font: 110% sans-serif; padding-left:10px">{{ inventario[11] }}</td>
								<td>
									<button type="button" class="btn btn-danger" v-on:click="eliminarArticulo(inventario)">Eliminar</button>
								</td>
							</tr>
						</tbody>

						<thead>
							<tr class='titulos2' style='text-align:right;'>
								<td colspan='11'>Total: </td>

								<td>
									<input type="text" name="valorTotal" id="valorTotal" v-model="valorTotal" readonly>
								</td>
							</tr>
						</thead>
					</table>
				</div>
				<!-- modales -->
				<div v-show="showModal_articulos" class="modal">
					<div class="modal-dialog modal-xl" >
						<div class="modal-content">
							<div class="modal-header">
								<h5 class="modal-title">Articulos disponibles</h5>
								<button type="button" @click="showModal_articulos=false;" class="btn btn-close"><div></div><div></div></button>
							</div>
							<div class="modal-body">
								<div class="d-flex flex-column">
									<div class="form-control m-0 mb-3">
										<input type="search" v-model="searchArticulo" @keyup="searchCodigoArticulos()" placeholder="Buscar por nombre o código del articulo">
									</div>
								</div>
								<div class="overflow-auto max-vh-50 overflow-x-hidden" >
									<table class="table table-hover fw-normal">
										<thead>
											<tr class="text-center">
												<th>Código</th>
												<th class="text-left">Nombre</th>
												<th class="text-left">Grupo inventario</th>
												<th>Disponible</th>
												<th>Unidad</th>
												<th>Bodega</th>
												<th>Centro costo</th>
											</tr>
										</thead>
										<tbody>
											<tr v-for="(articulo,index) in listadoArticulos" :key="index" @click="seleccionaArticulo(articulo)" class="text-center">
												<td>{{articulo[0]}}</td>
												<td class="text-left">{{articulo[1]}}</td>
												<td class="text-left">{{articulo[2]}}</td>
												<td>{{articulo[3]}}</td>
												<td>{{articulo[4]}}</td>
												<td>{{articulo[5]}}</td>
												<td>{{articulo[6]}}</td>

											</tr>
										</tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
				</div>

				<div v-show="showModal_cuentas" id="showModal_cuentas">
					<transition name="modal">
						<div class="modal-mask">
							<div class="modal-wrapper">
								<div class="modal-dialog modal-lg" style="max-width: 1200px !important;" role="document">
									<div class="modal-content" style="width: 100% !important;" scrollable>
										<div class="modal-header">
											<h5 class="modal-title">Cuentas Contables</h5>
											<button type="button" class="close" data-dismiss="modal" aria-label="Close">
												<span aria-hidden="true" @click="showModal_cuentas = false">&times;</span>
											</button>
										</div>
										<div class="modal-body">
											<div style="margin: 2px 0 0 0;">
												<div class="row" style="margin: 4px; border-radius:4px; background-color: #F6F6F6; ">
													<div class="col-md-2" style="padding: 12px 0px 0px 30px; font: 140% sans-serif;">
														<label for="">Código de cuenta:</label>
													</div>

													<div class="col-md-6 col-md-offset-6" style="padding: 4px">
														<input type="text" class="form-control" placeholder="Buscar por nombre o c&oacute;digo de la cuenta" style="font: sans-serif; ">
													</div>
												</div>
												<table>
													<thead>
														<tr class='titulos'>
															<td width="10%" style="font: 120% sans-serif; padding-left:10px; border-radius: 5px 0 0 0;">Codigo </td>
															<td width="20%" style="font: 120% sans-serif; ">Nombre </td>
														</tr>
													</thead>
												</table>
											</div>
											<div style="margin: 2px 0 0 0; border-radius: 0 0 0 6px; height: 200px; overflow: scroll; overflow-x: hidden; background: white; ">
												<table class='inicio inicio--no-shadow'>
													<tbody>
														<tr v-for="(cuenta,index) in cuentas" v-on:click="seleccionaCuenta(cuenta)" v-bind:class="index % 2 ? 'saludo1a' : 'saludo2'" style='text-rendering: optimizeLegibility; cursor: pointer !important;'>
															<td width="10%" style="font: 110% sans-serif; padding-left:10px">{{ cuenta[0] }}</td>
															<td width="20%" style="font: 110% sans-serif; padding-left:10px">{{ cuenta[1] }}</td>
														</tr>
													</tbody>
												</table>
											</div>
										</div>
										<div class="modal-footer">
											<button type="button" class="btn btn-secondary" @click="showModal_cuentas = false">Cerrar</button>
										</div>
									</div>
								</div>
							</div>
						</div>
					</transition>
				</div>

				<div v-show="showModal_contrucciones" id="showModal_contrucciones">
					<transition name="modal">
						<div class="modal-mask">
							<div class="modal-wrapper">
								<div class="modal-dialog modal-lg" style="max-width: 1200px !important;" role="document">
									<div class="modal-content" style="width: 100% !important;" scrollable>
										<div class="modal-header">
											<h5 class="modal-title">Contrucciones en curso</h5>
											<button type="button" class="close" data-dismiss="modal" aria-label="Close">
												<span aria-hidden="true" @click="showModal_contrucciones = false">&times;</span>
											</button>
										</div>
										<div class="modal-body">
											<div style="margin: 2px 0 0 0;">
												<div class="row" style="margin: 4px; border-radius:4px; background-color: #F6F6F6; ">
													<div class="col-md-2" style="padding: 12px 0px 0px 30px; font: 140% sans-serif;">
														<label for="">Orden de contrucción:</label>
													</div>

													<div class="col-md-6 col-md-offset-6" style="padding: 4px">
														<input type="text" class="form-control" placeholder="Buscar por nombre o c&oacute;digo de la cuenta" style="font: sans-serif; ">
													</div>
												</div>
												<table>
													<thead>
														<tr class='titulos'>
															<td width="10%" style="font: 120% sans-serif; padding-left:10px; border-radius: 5px 0 0 0;">Orden </td>
															<td width="10%" style="font: 120% sans-serif; ">Fecha </td>
															<td width="80%" style="font: 120% sans-serif; ">Descripción </td>
														</tr>
													</thead>
												</table>
											</div>
											<div style="margin: 2px 0 0 0; border-radius: 0 0 0 6px; height: 200px; overflow: scroll; overflow-x: hidden; background: white; ">
												<table class='inicio inicio--no-shadow'>
													<tbody>
														<tr v-for="(contruccion,index) in todasContrucciones" v-on:dblclick="seleccionaConstruccion(contruccion)" v-bind:class="index % 2 ? 'saludo1a' : 'saludo2'" style='text-rendering: optimizeLegibility; cursor: pointer !important;'>
															<td width="9%" style="font: 110% sans-serif; padding-left:10px">{{ contruccion[0] }}</td>
															<td width="10%" style="font: 110% sans-serif; padding-left:10px">{{ contruccion[1] }}</td>
															<td  style="font: 110% sans-serif; padding-left:10px">{{ contruccion[2] }}</td>
														</tr>
													</tbody>
												</table>
											</div>
										</div>
										<div class="modal-footer">
											<button type="button" class="btn btn-secondary" @click="showModal_contrucciones = false">Cerrar</button>
										</div>
									</div>
								</div>
							</div>
						</div>
					</transition>
				</div>

				<div v-show="showModal_montajes" id="showModal_montajes">
					<transition name="modal">
						<div class="modal-mask">
							<div class="modal-wrapper">
								<div class="modal-dialog modal-lg" style="max-width: 1200px !important;" role="document">
									<div class="modal-content" style="width: 100% !important;" scrollable>
										<div class="modal-header">
											<h5 class="modal-title">Maquinaria, planta y equipo en montaje</h5>
											<button type="button" class="close" data-dismiss="modal" aria-label="Close">
												<span aria-hidden="true" @click="showModal_montajes = false">&times;</span>
											</button>
										</div>
										<div class="modal-body">
											<div style="margin: 2px 0 0 0;">
												<div class="row" style="margin: 4px; border-radius:4px; background-color: #F6F6F6; ">
													<div class="col-md-2" style="padding: 12px 0px 0px 30px; font: 140% sans-serif;">
														<label for="">Orden de montaje:</label>
													</div>

													<div class="col-md-6 col-md-offset-6" style="padding: 4px">
														<input type="text" class="form-control" placeholder="Buscar por nombre o c&oacute;digo de la cuenta" style="font: sans-serif; ">
													</div>
												</div>
												<table>
													<thead>
														<tr class='titulos'>
															<td width="10%" style="font: 120% sans-serif; padding-left:10px; border-radius: 5px 0 0 0;">Orden </td>
															<td width="10%" style="font: 120% sans-serif; ">Fecha </td>
															<td width="60%" style="font: 120% sans-serif; ">Descripción </td>
															<td width="20%" style="font: 120% sans-serif; ">Supervisor </td>
														</tr>
													</thead>
												</table>
											</div>
											<div style="margin: 2px 0 0 0; border-radius: 0 0 0 6px; height: 200px; overflow: scroll; overflow-x: hidden; background: white; ">
												<table class='inicio inicio--no-shadow'>
													<tbody>
														<tr v-for="(montaje,index) in todosMontajes" v-on:dblclick="seleccionaMontaje(montaje)" v-bind:class="index % 2 ? 'saludo1a' : 'saludo2'" style='text-rendering: optimizeLegibility; cursor: pointer !important;'>
															<td width="9%" style="font: 110% sans-serif; padding-left:10px">{{ montaje[0] }}</td>
															<td width="10%" style="font: 110% sans-serif; padding-left:10px">{{ montaje[1] }}</td>
															<td width="61%" style="font: 110% sans-serif; padding-left:10px">{{ montaje[2] }}</td>
															<td  style="font: 110% sans-serif; padding-left:10px">{{ montaje[3] }}</td>
														</tr>
													</tbody>
												</table>
											</div>
										</div>
										<div class="modal-footer">
											<button type="button" class="btn btn-secondary" @click="showModal_montajes = false">Cerrar</button>
										</div>
									</div>
								</div>
							</div>
						</div>
					</transition>
				</div>
			</div>

			<div v-if="tipoMovimiento == '4'">
				<table class="inicio grande">
					<tr>
						<td class="titulos" colspan="10">.: Reversión salida directa</td>
						<td class="cerrar" style="width:4%" onClick="location.href='inve-principal.php'">Cerrar</td>
					</tr>

					<tr>
						<td class="tamano01" style="width: 10%;">Tipo de movimiento: </td>
						<td>
							<select name="tipoMovimiento" id="tipoMovimiento" v-model="tipoMovimiento" class="centrarSelect" style="width: 98%;" v-on:change="buscaConsecutivo">
								<option class="aumentarOption" v-bind:value="2">2 - Salida</option>
								<option class="aumentarOption" v-bind:value="4">4 - Reversión salida</option>
							</select>
						</td>

						<td class="tamano01" style="width: 8%;">Consecutivo:</td>
						<td style="width: 5%;">
							<input type="text" name='consecutivo' id='consecutivo' v-model='consecutivo' style="width: 100%; height: 30px; text-align:center;" readonly>
						</td>

						<td class="tamano01" style="width: 8%;">Fecha:</td>
						<td style="width:10%;">
							<input type="text" name="fechaRev" id="fechaRev" onchange="" value="<?php echo @$_POST['fechaRev'] ?>" maxlength="10" onKeyUp="return tabular(event,this)" onKeyDown="mascara(this,'/',patron,true)" title="DD/MM/YYYY" style="height:30px;text-align:center;width:100%;" onClick="displayCalendarFor('fechaRev');" class="colordobleclik" readonly />
						</td>

						<td class="tamano01">Documento:</td>
						<td>
							<input type="text" v-model="documento" @dblclick="buscaSalidas" class="colordobleclik" readonly>
						</td>

						<td class="tamano01">Valor Total:</td>
						<td>
							<input type="text" v-model="formatonumero(valorDocumento)" style="text-align: center;" readonly>
						</td>
					</tr>
				</table>

				<div class="subpantalla" style='height:56vh; width:100%; overflow-x:hidden; resize: vertical;'>
					<table>
						<thead>
							<tr style="text-align: center;">
								<th width="6%" class="titulosnew00">Bodega</th>
								<th width="6%" class="titulosnew00">Centro costos</th>
								<th width="10%" class="titulosnew00">Articulo</th>
								<th class="titulosnew00">Nombre articulo</th>
								<th width="10%" class="titulosnew00">Unidad medida</th>
								<th width="10%"class="titulosnew00">Cantidad</th>
								<th width="10%" class="titulosnew00">Valor unitario</th>
								<th width="10%" class="titulosnew00">Valor total</th>
							</tr>
						</thead>

						<tbody>
							<tr v-for="(articulo,index) in listaArticulos" v-bind:class="(index % 2 ? 'saludo1a' : 'saludo2')" style="font: 100% sans-serif;">
								<td style="text-align:center; width:6%;">{{ articulo[0] }}</td>
								<td style="text-align:center; width:6%;">{{ articulo[1] }}</td>
								<td style="text-align:center; width:10%;">{{ articulo[2] }}</td>
								<td style="text-align:center;">{{ articulo[3] }}</td>
								<td style="text-align:center; width:10%;">{{ articulo[4] }}</td>
								<td style="text-align:center; width:10%;">{{ articulo[5] }}</td>
								<td style="text-align:center; width:10%;">{{ formatonumero(articulo[6]) }}</td>
								<td style="text-align:center; width:10%;">{{ formatonumero(articulo[7]) }}</td>
							</tr>
							<tr>
								<td class="titulosnew00" style="text-align:right;" colspan="7">Total: </td>
								<td style="text-align:center;">{{ formatonumero(totalValores) }}</td>
							</tr>
						</tbody>
					</table>
				</div>

				<div v-show="showModalSalidas">
					<transition name="modal">
						<div class="modal-mask">
							<div class="modal-wrapper">
								<div class="modal-container2">
									<table class="inicio ancho">
										<tr>
											<td class="titulos" colspan="2" >Salidas disponibles</td>
											<td class="cerrar" style="width:7%" @click="showModalSalidas = false">Cerrar</td>
										</tr>

										<tr>
											<td class="textonew01" style="text-align:center;">Salida:</td>
											<td>
												<input type="text" v-model="searchSalida" v-on:keyup="filtroSalidas" placeholder="Buscar por consecutivo o descripcion" style="width: 100%;">
											</td>
										</tr>
									</table>
									<table class='tablamv'>
										<thead>
											<tr style="text-align:center;">
												<th class="titulosnew00" style="width: 20%;">Consecutivo documento</th>
												<th class="titulosnew00">Descripción</th>
												<th class="titulosnew00" style="width: 10%;">Fecha</th>
												<th class="titulosnew00" style="width: 20%;">Valor</th>
												<th class="titulosnew00" style="width: 1%;"></th>
											</tr>
										</thead>
										<tbody>
											<tr v-for="(salida,index) in salidas" v-bind:class="index % 2 ? 'saludo1a' : 'saludo2'" v-on:click="seleccionaSalida(salida)">
												<td style="width:20%; text-align:center;"> {{ salida[0] }} </td>
												<td style="text-align:left;"> {{ salida[2] }} </td>
												<td style="width:10%; text-align:center;"> {{ salida[1] }} </td>
												<td style="width:20%; text-align:center;"> {{ formatonumero(salida[3]) }} </td>
											</tr>
										</tbody>
									</table>
								</div>
							</div>
						</div>
					</transition>
				</div>
			</div>

		</div>
	</div>


	<script src="Librerias/vue/vue.min.js"></script>
	<script src="Librerias/vue/axios.min.js"></script>
	<script type="module" src="vue/inve-salidaDirecta.js?<?php echo date('d_m_Y_h_i_s'); ?>"></script>
</body>

</html>
