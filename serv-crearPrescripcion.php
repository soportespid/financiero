<?php
	header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=utf8");
	require 'comun.inc';
	require 'funciones.inc';
	session_start();
	$linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");
	cargarcodigopag($_GET['codpag'],$_SESSION["nivel"]);
	date_default_timezone_set("America/Bogota");
?>
<!DOCTYPE >
<html lang=es>
	<head>
    <meta charset="utf-8"/>
		<meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1"/>
		<meta name="viewport" content="user-scalable=no">
		<title>:: IDEAL 10 - Servicios públicos</title>
		<link href="favicon.ico" rel="shortcut icon"/>
		<link href="css/css2z.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
		<link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
		<link href="css/css4.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
        <link href="css/tabs.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css"/>
        <link rel="stylesheet" href="sweetalert2/dist/sweetalert2.min.css">
        <script type="text/javascript" src="css/programas.js"></script>
        <script src="sweetalert2/dist/sweetalert2.min.js"></script>
		<script src="vue/vue.min.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script src="css/programas.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script src="css/calendario.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
	</head>
	<body>
		<header>
			<table>
				<tr><script>barra_imagenes("serv");</script><?php cuadro_titulos();?></tr>
			</table>
		</header>

		<form name="form2" method="post" action="">
			<section id="myapp" v-cloak >
				<nav>
					<table>
						<tr><?php menu_desplegable("serv");?></tr>
						<tr>
							<td colspan="3" class="cinta">
								<img src="imagenes/add.png" v-on:click="location.href=''" class="mgbt" title="Nuevo">
								<img src="imagenes/guarda.png" @click="guardar" title="Guardar"  class="mgbt">
								<img src="imagenes/busca.png" v-on:click="location.href='serv-buscarPrescripcion'" class="mgbt" title="Buscar">
								<img src="imagenes/nv.png" onClick="mypop=window.open('serv-principal','',''); mypop.focus();" class="mgbt" title="Nueva Ventana">
							</td>
						</tr>
					</table>
				</nav>

				<article>
					<div>
						<table class="inicio">
							<tr>
								<td class="titulos" colspan="8">.: Prescripción de facturación:</td>
                            	<td class="cerrar" style="width:4%" onClick="location.href='serv-recaudoFactura'">Cerrar</td>
							</tr>

                            <tr>
                                <td class="textonew01" style="width:10%;">.: Consecutivo:</td>
                                <td style="width: 11%;"><input type="text" v-model="consec" style="text-align: center;" readonly></td>

                                <td class="textonew01" style="width:10%;">.: Fecha:</td>
                                <td style="width: 11%;"><input type="text" name="fecha" value="<?php echo $_POST['fecha']?>" onKeyUp="return tabular(event,this)" id="fecha" title="DD/MM/YYYY" onDblClick="displayCalendarFor('fecha');" class="colordobleclik" autocomplete="off" onChange=""  readonly></td>

                                <td class="textonew01" style="width:10%;">.: Resolución:</td>
                                <td><input type="text" v-model="resolucion" style="text-align:center;" required></td>

                                <td class="textonew01" style="width:10%;">.: Codigo de usuario:</td>
                                <td><input type="text" v-model="codUsu" @dblclick="showModalUsu" style="text-align:center; width: 80%;" class="colordobleclik" readonly></td>
                            </tr>

                            <tr>
                                <td class="textonew01" style="width:10%;">.: Documento:</td>
                                <td><input type="text" v-model="document" style="text-align:center;" readonly></td>

                                <td class="textonew01" style="width:10%;">.: Nombre suscriptor:</td>
                                <td colspan="3"><input type="text" v-model="name" style="text-align:center; width: 99%;" readonly></td>

                                <td class="textonew01" style="width:10%;">.: Codigo catastral:</td>
                                <td><input type="text" v-model="codCatastral" style="text-align:center; width: 80%;" readonly></td>
                            </tr>

                            <tr>
                                <td class="textonew01" style="width:10%;">.: Barrio:</td>
                                <td><input type="text" v-model="barrio" style="text-align:center;" readonly></td>

                                <td class="textonew01" style="width:10%;">.: Dirección:</td>
                                <td colspan="3"><input type="text" v-model="direccion" style="text-align:center; width: 99%;" readonly></td>

                                <td class="textonew01" style="width:10%;">.: Estrato:</td>
                                <td><input type="text" v-model="estrato" style="text-align:center; width: 80%;" readonly></td>
                            </tr>

                            <tr>
                                <td class="textonew01" style="width:10%;">.: Factura:</td>
                                <td><input type="text" v-model="numFactura" style="text-align:center;" readonly></td>

                                <td class="textonew01" style="width:10%;">.: Deuda total:</td>
                                <td><input type="text" v-model="formatonumero(deuda)" style="text-align:center;" readonly></td>

                                <td class="textonew01" style="width:10%;">.: Descuento:</td>
                                <td><input type="text" v-model="formatonumero(descuento)" style="text-align:center;" readonly></td>


                                <td class="textonew01" style="width:10%;">.: Total a pagar:</td>
                                <td><input type="text" v-model="formatonumero(totalPagar)" style="text-align:center;" readonly></td>
                            </tr>
						</table>
					</div>

					<div v-show="showUsuarios">
						<transition name="modal">
							<div class="modal-mask">
								<div class="modal-wrapper">
									<div class="modal-container">
										<table class="inicio ancho">
											<tr>
												<td class="titulos" colspan="2" >Usuarios activos</td>
												<td class="cerrar" style="width:7%" @click="showUsuarios = false">Cerrar</td>
											</tr>
										</table>
										<table class='tablamv'>
											<thead>
												<tr style="text-align:center;">
													<th class="titulosnew00" style="width: 15%;">Codigo usuario</th>
													<th class="titulosnew00" style="width: 15%;">Documento</th>
													<th class="titulosnew00">Nombre</th>
                                                    <th class="titulosnew00" style="width: 20%;">Cod catastral</th>
                                                    <th style="width: 1%;"></th>
												</tr>
											</thead>
											<tbody>
												<tr v-for="(usu,index) in dataUsu" v-bind:class="index % 2 ? 'saludo1a' : 'saludo2'" v-on:click="selecUsu(usu)">
													<td style="width:15%; text-align:center;"> {{ usu[0] }} </td>
													<td style="width:15%; text-align:center;"> {{ usu[1] }} </td>
                                                    <td> {{ usu[2] }} </td>
													<td style="width:20%; text-align:center;"> {{ usu[3] }} </td>
												</tr>
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</transition>
					</div>
				</article>
			</section>
		</form>

		<script src="node_modules/read-excel-file/bundle/read-excel-file.min.js"></script>
		<script src="node_modules/xlsx/dist/xlsx.full.min.js"></script>
        <script src="Librerias/vue/vue.min.js"></script>
		<script src="Librerias/vue/axios.min.js"></script>
		<script type="module" src="servicios_publicos/prescripcion/crear/serv-crearPrescripcion.js"></script>

	</body>
</html>
