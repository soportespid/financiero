<?php

    header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=utf8");
	require 'comun.inc';
	require 'funciones.inc';
	session_start();
    if(empty($_SESSION)){
        header("location: index.php");
    }
	$linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");
	cargarcodigopag($_GET['codpag'],$_SESSION["nivel"]);
	date_default_timezone_set("America/Bogota");
?>
<!DOCTYPE >
<html lang=es>
	<head>
        <meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1"/>
		<meta name="viewport" content="user-scalable=no">
		<title>:: IDEAL 10 </title>
		<link href="favicon.ico" rel="shortcut icon"/>
		<link href="css/css2z.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
		<link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
		<link href="css/css4.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
        <link href="css/style.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css"/>
        <link rel="stylesheet" href="sweetalert2/dist/sweetalert2.min.css">
        <script src="sweetalert2/dist/sweetalert2.min.js"></script>
		<script src="css/programas.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
        <style>
            [v-cloak]{
                display : none;
            }
        </style>
	</head>
	<body>
		<header>
			<table>
				<tr>
                    <script>barra_imagenes("trans");</script><?php cuadro_titulos();?>
                </tr>
			</table>
		</header>

        <section id="myapp" v-cloak>
            <!-- variables -->
            <input type="hidden" value = "1" ref="pageType">

            <!-- loading -->
            <div class="loading-container" v-show="isLoading" >
                <p class="text-loading"data-loading="IDEAL 10">IDEAL 10</p>
            </div>

            <nav>
                <table>
                    <tr><?php menu_desplegable("trans");?></tr>
                </table>
                <div class="bg-white group-btn p-1" id="newNavStyle">
                    <button type="button" class="btn btn-white btn-primary-hover d-flex justify-between align-items-center" @click="save()">
                        <span>Guardar</span>
                        <svg viewBox="0 -960 960 960" ><path d="M840-680v480q0 33-23.5 56.5T760-120H200q-33 0-56.5-23.5T120-200v-560q0-33 23.5-56.5T200-840h480l160 160Zm-80 34L646-760H200v560h560v-446ZM480-240q50 0 85-35t35-85q0-50-35-85t-85-35q-50 0-85 35t-35 85q0 50 35 85t85 35ZM240-560h360v-160H240v160Zm-40-86v446-560 114Z"/></svg>
                    </button>
                    <button type="button" class="btn btn-white btn-primary-hover d-flex justify-between align-items-center" @click="location.href='trans-recibo-impuesto-buscar'">
                        <span>Buscar</span>
                        <svg class="fill-black group-hover:fill-white w-5 h-5" viewBox="0 -960 960 960" ><path d="M784-120 532-372q-30 24-69 38t-83 14q-109 0-184.5-75.5T120-580q0-109 75.5-184.5T380-840q109 0 184.5 75.5T640-580q0 44-14 83t-38 69l252 252-56 56ZM380-400q75 0 127.5-52.5T560-580q0-75-52.5-127.5T380-760q-75 0-127.5 52.5T200-580q0 75 52.5 127.5T380-400Z"/></svg>
                    </button>
                    <button type="button" class="btn btn-white btn-primary-hover d-flex justify-between align-items-center" @click="mypop=window.open('trans-principal','',''); mypop.focus();">
                        <span>Nueva ventana</span>
                        <svg class="fill-black group-hover:fill-white w-5 h-5" viewBox="0 -960 960 960" ><path d="M320-120v-80h80v-80H160q-33 0-56.5-23.5T80-360v-400q0-33 23.5-56.5T160-840h640q33 0 56.5 23.5T880-760v400q0 33-23.5 56.5T800-280H560v80h80v80H320ZM160-360h640v-400H160v400Zm0 0v-400 400Z"/></svg>
                    </button>
                </div>
            </nav>
            <article class="bg-white">
                <div>
                    <h2 class="titulos m-0">Crear - Recaudo impuesto vehicular</h2>
                    
                    <div class="d-flex">
                        <div class="form-control w-25">
                            <label class="form-label m-0" for="">Fecha<span class="text-danger fw-bolder">*</span></label>
                            <input type="date" class="text-center" v-model="fecha">
                        </div>

                        <div class="form-control w-25">
                            <label class="form-label m-0" for="">N° liquidación<span class="text-danger fw-bolder">*</span></label>
                            <input type="text" class="colordobleclik" v-model="liquidacion_id" @dblclick="deployModal('Liquidaciones')" readonly>
                        </div>

                        <div class="form-control w-25">
                            <label class="form-label m-0" for="">Placa</label>
                            <input type="text" class="text-center" v-model="placa" disabled>
                        </div>

                        <div class="form-control w-25">
                            <label class="form-label m-0" for="">Propietario</label>
                            <input type="text" class="text-center" v-model="propietarioDocument" disabled>
                        </div>

                        <div class="form-control justify-between">
                            <label class="form-label m-0" for=""></label>
                            <input type="text" v-model="propietarioName" disabled>
                        </div>
                    </div>

                    <div class="d-flex">
                        <div class="form-control w-50">
                            <label class="form-label m-0" for="">Valor liquidación</label>
                            <input type="text" class="text-right" v-model="viewFormatNumber(valorLiquidacion)" disabled>
                        </div>

                        <div class="form-control w-50">
                            <label class="form-label m-0" for="">Medio de pago<span class="text-danger fw-bolder">*</span></label>
                            <select v-model="medioPago">
                                <option value="banco">Banco</option>
                            </select>
                        </div>

                        <div class="form-control w-50">
                            <label class="form-label m-0" for="">Cuenta<span class="text-danger fw-bolder">*</span></label>
                            <input type="text" class="colordobleclik" v-model="cuentaBancaria" @dblclick="deployModal('CuentaBancaria')" readonly>
                        </div>

                        <div class="form-control justify-between">
                            <label class="form-label m-0" for=""></label>
                            <input type="text" v-model="cuentaBancariaName" disabled>
                        </div>
                    </div>

                    <div class="d-flex">
                        <div class="form-control">
                            <label class="form-label m-0" for="">Detalle de recuado<span class="text-danger fw-bolder">*</span></label>
                            <textarea v-model="detalleRecaudo"></textarea>
                        </div>
                    </div>
                </div>                       
                
                <div v-show="modalLiquidacion" class="modal">
                    <div class="modal-dialog modal-xl">
                        <div class="modal-content">
                            <div class="modal-header m-0">
                                <h5 class="modal-title">Liquidaciones de impuesto vehicular</h5>
                                <button type="button" @click="modalLiquidacion=false;" class="btn btn-close"><div></div><div></div></button>
                            </div>
                            <div class="modal-body">
                                <div class="d-flex">
                                    <div class="form-control">
                                        <label class="form-label m-0" for="">Buscar</label>
                                        <input type="search" v-model="txtSearchLiquidacion" placeholder="Buscar por número de liquidación, placa o documento propietario">
                                    </div>

                                    <div class="form-control w-15">
                                        <label class="form-label m-0" for="">Por pagina</label>
                                        <select class="text-center" v-model="selectPorPagina">
                                            <option value="25">25</option>
                                            <option value="50">50</option>
                                            <option value="100">100</option>
                                            <option value="250">250</option>
                                            <option value="500">500</option>
                                            <option value="1000">1000</option>
                                            <option value="">Todo</option>
                                        </select>
                                    </div>

                                    <div class="form-control justify-between w-15">
                                        <label class="form-label m-0" for=""></label>
                                        <button type="button" class="btn btn-primary" @click="getLiquidaciones()">Buscar</button>
                                    </div>
                                </div>
                                <div class="overflow-auto max-vh-50 overflow-x-hidden" >
                                    <table class="table table-hover fw-normal">
                                        <thead>
                                            <tr class="text-center">
                                                <th>N° Liquidación</th>
                                                <th>Fecha</th>
                                                <th>Placa</th>
                                                <th class="text-left">Propietario</th>
                                                <th>Valor total</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr v-show="arrLiquidaciones.length == 0">
                                                <td colspan="5">
                                                    <div style="text-align: center; font-size:large" class="h4 text-center">
                                                        No hay información encontrada
                                                    </div>
                                                </td>
                                            </tr>

                                            <tr v-for="(liquidacion,index) in arrLiquidaciones" :key="index" @click="fillForm('Liquidaciones', liquidacion)">
                                                <td class="text-center w-15">{{ liquidacion.id }}</td>
                                                <td class="text-center">{{ liquidacion.fecha }}</td>
                                                <td class="text-center">{{ liquidacion.placa }}</td>
                                                <td>{{ liquidacion.propietario }} - {{ liquidacion.nombre }}</td>
                                                <td class="text-right">{{ viewFormatNumber(liquidacion.total_pago) }}</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>

                                <div v-if="arrLiquidaciones.length > 0" class="list-pagination-container">
                                    <p>Página {{ paginaActual }} de {{ intTotalPaginas }}</p>
                                    <ul class="list-pagination">
                                        <li v-show="paginaActual > 1" @click="getLiquidaciones(paginaActual = 1)"><< </li>
                                        <li v-show="paginaActual > 1" @click="getLiquidaciones(--paginaActual)"><</li>
                                        <li v-for="(pagina,index) in arrBotones" :class="paginaActual == pagina ? 'active' : ''" @click="getLiquidaciones(pagina)" :key="index">{{pagina}}</li>
                                        <li v-show="paginaActual < intTotalPaginas" @click="getLiquidaciones(++paginaActual)">></li>
                                        <li v-show="paginaActual < intTotalPaginas" @click="getLiquidaciones(paginaActual = intTotalPaginas)" >>></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>  

                <div v-show="modalCuentasBancarias" class="modal">
                    <div class="modal-dialog modal-sm">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title">Cuentas bancarias</h5>
                                <button type="button" @click="modalCuentasBancarias=false;" class="btn btn-close"><div></div><div></div></button>
                            </div>
                            <div class="modal-body">
                                <div class="d-flex flex-column">
                                    <div class="form-control m-0 mb-3">
                                        <input type="search" v-model="txtSearchBanco" @keyup="arrayFilter('CuentaBancaria')" placeholder="Busca cuenta o nombre">
                                    </div>
                                </div>
                                <div class="overflow-auto max-vh-50 overflow-x-hidden" >
                                    <table class="table table-hover fw-normal">
                                        <thead>
                                            <tr>
                                                <th class="text-center">Número cuenta</th>
                                                <th>Nombre</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr v-for="(cuenta,index) in arrCuentasBancarias" :key="index" @click="fillForm('CuentaBancaria', cuenta)">
                                                <td class="text-center w-25">{{ cuenta.cuenta_banco }}</td>
                                                <td>{{ cuenta.nombre }}</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </article>
        </section>

        <script src="Librerias/vue/vue.min.js"></script>
		<script src="Librerias/vue/axios.min.js"></script>
		<script type="module" src="transporte/js/functions_recibo_impuesto.js?<?= date('d_m_Y_h_i_s');?>"></script>

	</body>
</html>
