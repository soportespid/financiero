<?php

	header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=utf8");
	require 'comun.inc';
	require 'funciones.inc';
	session_start();
	if(empty($_SESSION)){
		header("location: index.php");
	}
	$linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");
	cargarcodigopag($_GET['codpag'],$_SESSION["nivel"]);
	date_default_timezone_set("America/Bogota");
?>
<!DOCTYPE >
<html lang=es>
	<head>
	<meta charset="utf-8"/>
		<meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1"/>
		<meta name="viewport" content="user-scalable=no">
		<title>:: IDEAL 10 - Parametrización</title>
		<link href="favicon.ico" rel="shortcut icon"/>
		<link href="css/css2z.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
		<link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
		<link href="css/css4.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
		<link href="css/tabs.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css"/>
		<link href="css/style.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css"/>
		<link rel="stylesheet" href="sweetalert2/dist/sweetalert2.min.css">
		<script src="sweetalert2/dist/sweetalert2.min.js"></script>
		<script src="vue/vue.min.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script src="css/programas.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script src="css/calendario.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
	</head>
	<body>
		<header>
			<table>
				<tr><script>barra_imagenes("para");</script><?php cuadro_titulos();?></tr>
			</table>
		</header>

		<form name="form2" method="post" action="">
			<section id="myapp" v-cloak >
				<div id="cargando" v-show="isLoading" class="loading" style="z-index: 10000;">
					<span>Cargando...</span>
				</div>
				<nav>
					<?php menu_desplegable("para");?>
					<div class="bg-white group-btn p-1">
						<button type="button"  @click="save" class="btn btn-white btn-primary-hover d-flex justify-between align-items-center">
							<span>Guardar</span>
							<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 -960 960 960"><path d="M840-680v480q0 33-23.5 56.5T760-120H200q-33 0-56.5-23.5T120-200v-560q0-33 23.5-56.5T200-840h480l160 160Zm-80 34L646-760H200v560h560v-446ZM480-240q50 0 85-35t35-85q0-50-35-85t-85-35q-50 0-85 35t-35 85q0 50 35 85t85 35ZM240-560h360v-160H240v160Zm-40-86v446-560 114Z"></path></svg>
						</button>
						<button type="button" onclick="window.location.href='hum-parametrosPagoNominaBuscar'" class="btn btn-white btn-primary-hover d-flex justify-between align-items-center">
							<span>Buscar</span>
							<svg class="fill-black group-hover:fill-white w-5 h-5" xmlns="http://www.w3.org/2000/svg" viewBox="0 -960 960 960"><path d="M784-120 532-372q-30 24-69 38t-83 14q-109 0-184.5-75.5T120-580q0-109 75.5-184.5T380-840q109 0 184.5 75.5T640-580q0 44-14 83t-38 69l252 252-56 56ZM380-400q75 0 127.5-52.5T560-580q0-75-52.5-127.5T380-760q-75 0-127.5 52.5T200-580q0 75 52.5 127.5T380-400Z"></path></svg>
						</button>
						<button type="button" @click="mypop=window.open('para-principal.php','',''); mypop.focus();" class="btn btn-white btn-primary-hover d-flex justify-between align-items-center">
							<span>Nueva ventana</span>
							<svg class="fill-black group-hover:fill-white w-5 h-5" xmlns="http://www.w3.org/2000/svg" viewBox="0 -960 960 960"><path d="M320-120v-80h80v-80H160q-33 0-56.5-23.5T80-360v-400q0-33 23.5-56.5T160-840h640q33 0 56.5 23.5T880-760v400q0 33-23.5 56.5T800-280H560v80h80v80H320ZM160-360h640v-400H160v400Zm0 0v-400 400Z"></path></svg>
						</button>
						<button type="button" onclick="mypop=window.open('hum-parametrosPagoNominaCrear.php','','');mypop.focus();" class="btn btn-white btn-primary-hover d-flex justify-between align-items-center">
							<span class="group-hover:text-white">Duplicar pantalla</span>
							<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 -960 960 960"><path d="M640-160v-360H160v360h480Zm80-200v-80h80v-360H320v200h-80v-200q0-33 23.5-56.5T320-880h480q33 0 56.5 23.5T880-800v360q0 33-23.5 56.5T800-360h-80ZM160-80q-33 0-56.5-23.5T80-160v-360q0-33 23.5-56.5T160-600h480q33 0 56.5 23.5T720-520v360q0 33-23.5 56.5T640-80H160Zm400-603ZM400-340Z"></path></svg>
						</button>
					</div>
				</nav>
				<article>
					<!--TABS-->
					<div ref="rTabs" class="nav-tabs bg-white p-1">
						<div class="nav-item active" @click="showTab(1)">Variables</div>
						<div class="nav-item" v-show="isProvisiona" @click="showTab(2)">Provisiona</div>
					</div>
					<!--CONTENIDO TABS-->
					<div ref="rTabsContent" class="nav-tabs-content bg-white">
						<div class="nav-content active">
							<h2 class="titulos m-0">Variables de pago</h2>
							<div>
								<p class="m-2">Todos los campos con (<span class="text-danger fw-bolder">*</span>) son obligatorios</p>
								<div class="d-flex w-100">
									<div class="form-control w-25">
										<label class="form-label" for="">Razon Social:</label>
										<input type="text" class="text-center" v-model="txtConsecutivo" disabled>
									</div>
									<div class="form-control">
										<label class="form-label" for="">Nombre<span class="text-danger fw-bolder">*</span>:</label>
										<input type="text" v-model="txtNombre">
									</div>
								</div>
								<div class="d-flex justify-between mb-3 mt-3">
									<div class="form-control">
										<div class="d-flex align-items-center ">
											<label for="labelCheckName1" class="me-2">Paga salud: </label>
											<label for="labelCheckName1" class="form-switch">
												<input type="checkbox" id="labelCheckName1" v-model="isSalud">
												<span></span>
											</label>
										</div>
									</div>
									<div class="form-control">
										<div class="d-flex align-items-center ">
											<label for="labelCheckName2" class="me-2">Paga pensión: </label>
											<label for="labelCheckName2" class="form-switch">
												<input type="checkbox" id="labelCheckName2" v-model="isPension">
												<span></span>
											</label>
										</div>
									</div>
									<div class="form-control">
										<div class="d-flex align-items-center ">
											<label for="labelCheckName3" class="me-2">Paga ARL: </label>
											<label for="labelCheckName3" class="form-switch">
												<input type="checkbox" id="labelCheckName3" v-model="isArl">
												<span></span>
											</label>
										</div>
									</div>
									<div class="form-control">
										<div class="d-flex align-items-center ">
											<label for="labelCheckName4" class="me-2">Paga parafiscales: </label>
											<label for="labelCheckName4" class="form-switch">
												<input type="checkbox" id="labelCheckName4" v-model="isFiscales">
												<span></span>
											</label>
										</div>
									</div>
									<div class="form-control">
										<div class="d-flex align-items-center ">
											<label for="labelCheckName5" class="me-2">Provisiona: </label>
											<label for="labelCheckName5" class="form-switch">
												<input type="checkbox" id="labelCheckName5" v-model="isProvisiona">
												<span></span>
											</label>
										</div>
									</div>
								</div>
								<div>
									<div class="d-flex">
										<div class="form-control">
											<label class="form-label" for="labelSelectName2">Funcionamiento:</label>
											<select id="labelSelectName2" v-model="selectFuncionamiento">
												<option value="0" selected>Seleccione</option>
												<option v-for="(data,index) in arrFuncionamiento" :key="index" :value="data.codigo">
													{{ data.nombre}}
												</option>
											</select>
										</div>
										<div class="form-control">
											<label class="form-label" for="labelSelectName2">Temporal funcionamiento:</label>
											<select id="labelSelectName2" v-model="selectTempFuncionamiento">
												<option value="0" selected>Seleccione</option>
												<option v-for="(data,index) in arrTempFuncionamiento" :key="index" :value="data.codigo">
													{{ data.nombre}}
												</option>
											</select>
										</div>
										<div class="form-control">
											<label class="form-label" for="labelSelectName2">Inversión:</label>
											<select id="labelSelectName2" v-model="selectInversion">
												<option value="0" selected>Seleccione</option>
												<option v-for="(data,index) in arrInversion" :key="index" :value="data.codigo">
													{{ data.nombre}}
												</option>
											</select>
										</div>
									</div>
									<div class="d-flex">
										<div class="form-control">
											<label class="form-label" for="labelSelectName2">Inversión temporal:</label>
											<select id="labelSelectName2" v-model="selectTempInversion">
												<option value="0" selected>Seleccione</option>
												<option v-for="(data,index) in arrTempInversion" :key="index" :value="data.codigo">
													{{ data.nombre}}
												</option>
											</select>
										</div>
										<div class="form-control">
											<label class="form-label" for="labelSelectName2">Gastos comercialización:</label>
											<select id="labelSelectName2" v-model="selectComercio">
												<option value="0" selected>Seleccione</option>
												<option v-for="(data,index) in arrComercio" :key="index" :value="data.codigo">
													{{ data.nombre}}
												</option>
											</select>
										</div>
										<div class="form-control">
											<label class="form-label" for="labelSelectName2">Temporal gastos comercialización:</label>
											<select id="labelSelectName2" v-model="selectTempComercio">
												<option value="0" selected>Seleccione</option>
												<option v-for="(data,index) in arrTempComercio" :key="index" :value="data.codigo">
													{{ data.nombre}}
												</option>
											</select>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="nav-content">
							<div class="d-flex">
								<div class="form-control w-75">
									<label class="form-label" for="labelSelectName2">Seleccionar variables:</label>
									<div class="d-flex">
										<select v-model="selectVariable" id="labelSelectName2" class="w-50">
											<option value="0" selected>Seleccione</option>
											<option v-for="(data,index) in arrVariables" :key="index" :value="data.codigo">
												{{ data.codigo+"-"+data.nombre}}
											</option>
										</select>
										<button type="button" class="btn btn-primary" @click="addVariable">Agregar</button>
									</div>
								</div>
							</div>
							<div class="overflow-auto max-vh-50 overflow-x-hidden p-2" >
								<table class="table fw-normal">
									<thead>
										<tr>
											<th>Código</th>
											<th>Nombre</th>
											<th>Opciones</th>
										</tr>
									</thead>
									<tbody>
										<tr v-for="(data,index) in arrSelVariables" :key="index">
											<td>{{data.codigo}}</td>
											<td>{{data.nombre}}</td>
											<td>
												<button type="button" class="btn btn-danger m-1" @click="delVariable(index)">Eliminar</button>
											</td>
										</tr>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</article>
			</section>
		</form>

		<script src="node_modules/read-excel-file/bundle/read-excel-file.min.js"></script>
		<script src="node_modules/xlsx/dist/xlsx.full.min.js"></script>
		<script src="Librerias/vue/vue.min.js"></script>
		<script src="Librerias/vue/axios.min.js"></script>
		<script type="module" src="gestion_humana/parametros_nomina/crear/hum-parametrosNomina.js?<?= date('d_m_Y_h_i_s');?>"></script>

	</body>
</html>
