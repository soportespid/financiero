<?php
	require_once 'PHPExcel/Classes/PHPExcel.php';
	require "comun.inc";
	require "funciones.inc";
    ini_set('max_execution_time',99999999);
	session_start();
	$linkbd=conectar_v7();
	$objPHPExcel = new PHPExcel();
	//----Propiedades----
	$objPHPExcel->getProperties()
		->setCreator("IDEAL10")
		->setLastModifiedBy("IDEAL10")
		->setTitle("REPORTE SUBSIDIOS")
		->setSubject("SP")
		->setDescription("SP")
		->setKeywords("SP")
		->setCategory("SERVICIOS PUBLICOS");
	
	$borders = array(
		'borders' => array(
			'allborders' => array(
				'style' => PHPExcel_Style_Border::BORDER_THIN,
				'color' => array('argb' => 'FF000000'),
			)
		),
	);

    $styleArray = array(
		"font"  => array(
		"bold" => false,
		"color" => array("rgb" => "FF0000"),
		"size"  => 12,
		"name" => "Verdana"
		));

    $tablaPorServicio = 1;
    $tot = 0;
    $cont = 0;
    $i=1;
    $encontrados = count($_POST['servicio']);

    for($ij = 0; $ij < $encontrados; $ij++){

        $cont+=1;
        
        if($serv != $_POST['servicio'][$ij]){
            
            if($tablaPorServicio == 0){
                $tot = 1;
            }
            $tablaPorServicio = 1;
            //$tot = 0;
        }

        

        if($tot == 1){

           
            $objPHPExcel->setActiveSheetIndex(0)
            ->setCellValueExplicit ("A$i", 'TOTAL', PHPExcel_Cell_DataType :: TYPE_STRING)
            ->setCellValueExplicit ("B$i", $_POST['totUsuarios'][$cont], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
            ->setCellValueExplicit ("C$i", $_POST['totConsumoM3'][$cont], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
            ->setCellValueExplicit ("D$i", $_POST['totConsumo'][$cont], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
            ->setCellValueExplicit ("E$i", $_POST['totCargofijo'][$cont], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
            ->setCellValueExplicit ("F$i", $_POST['totSubsidio'][$cont], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
            ->setCellValueExplicit ("G$i", $_POST['totContribucion'][$cont], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
            ->setCellValueExplicit ("H$i", $_POST['total'][$cont], PHPExcel_Cell_DataType :: TYPE_NUMERIC);
            $objPHPExcel->getActiveSheet()->getStyle("A$i:H$i")->applyFromArray($styleArray);
            $tot = 0;
            $i++;

        }

        if($tablaPorServicio == 1){

            $objPHPExcel->getActiveSheet()->mergeCells("A$i:H$i");
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue("A$i", 'REPORTE SUBSIDIO PERIODO DE FACTURACION: '.$_POST['corte'].' - '.$_POST['nombreServ'][$ij]);
            $objFont=$objPHPExcel->getActiveSheet()->getStyle("A$i")->getFont();
            $objFont->setName('Courier New');
            $objFont->setSize(15);
            $objFont->setBold(true);
            $objFont->setUnderline(PHPExcel_Style_Font::UNDERLINE_SINGLE);
            $objFont->getColor()->setARGB( PHPExcel_Style_Color::COLOR_BLACK);
            $objAlign=$objPHPExcel->getActiveSheet()->getStyle("A$i")->getAlignment(); 
            $objAlign->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $objAlign->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
            $objPHPExcel-> getActiveSheet ()
                -> getStyle ("A$i")	
                -> getFill ()
                -> setFillType (PHPExcel_Style_Fill :: FILL_SOLID)
                -> getStartColor ()
                -> setRGB ('A6E5F3');
            $i+=1;
            $objPHPExcel-> getActiveSheet ()
                -> getStyle ("A$i:H$i")
                -> getFill ()
                -> setFillType (PHPExcel_Style_Fill :: FILL_SOLID)
                -> getStartColor ()
                -> setRGB ('A6E5F3');

            $objPHPExcel->getActiveSheet()->getStyle("A$i:H$i")->applyFromArray($borders);
            $objPHPExcel->setActiveSheetIndex(0)
                ->setCellValue("A$i", 'Clase de uso')
                ->setCellValue("B$i", 'Usuarios')
                ->setCellValue("C$i", 'Consumo M3')
                ->setCellValue("D$i", 'Facturacion de consumo')
                ->setCellValue("E$i", 'Cargo fijo')
                ->setCellValue("F$i", 'Valor subsidio')
                ->setCellValue("G$i", 'Valor contribucion')
                ->setCellValue("H$i", 'Valor a pagar (Diferencia Sub. menos Cont.)');

            $serv = $_POST['servicio'][$ij];
            
			$tablaPorServicio = 0;
            $i+=1;
            
        }


        $objPHPExcel->setActiveSheetIndex(0)
            ->setCellValueExplicit ("A$i", $_POST['claseDeUso'][$ij], PHPExcel_Cell_DataType :: TYPE_STRING)
            ->setCellValueExplicit ("B$i", $_POST['usuarios'][$ij], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
            ->setCellValueExplicit ("C$i", $_POST['consumoM3'][$ij], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
            ->setCellValueExplicit ("D$i", $_POST['consumo'][$ij], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
            ->setCellValueExplicit ("E$i", $_POST['cargoFijo'][$ij], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
            ->setCellValueExplicit ("F$i", $_POST['valorSubsidio'][$ij], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
            ->setCellValueExplicit ("G$i", $_POST['valorContribucion'][$ij], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
            ->setCellValueExplicit ("H$i", '', PHPExcel_Cell_DataType :: TYPE_STRING);

        $i++;

        if($encontrados == $cont){
            $objPHPExcel->setActiveSheetIndex(0)
            ->setCellValueExplicit ("A$i", 'TOTAL', PHPExcel_Cell_DataType :: TYPE_STRING)
            ->setCellValueExplicit ("B$i", $_POST['totUsuarios'][$cont], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
            ->setCellValueExplicit ("C$i", $_POST['totConsumoM3'][$cont], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
            ->setCellValueExplicit ("D$i", $_POST['totConsumo'][$cont], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
            ->setCellValueExplicit ("E$i", $_POST['totCargofijo'][$cont], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
            ->setCellValueExplicit ("F$i", $_POST['totSubsidio'][$cont], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
            ->setCellValueExplicit ("G$i", $_POST['totContribucion'][$cont], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
            ->setCellValueExplicit ("H$i", $_POST['total'][$cont], PHPExcel_Cell_DataType :: TYPE_NUMERIC);
            $objPHPExcel->getActiveSheet()->getStyle("A$i:H$i")->applyFromArray($styleArray);
            $tot = 0;
            $i++;
        }
    }

    $objPHPExcel->getActiveSheet()->getStyle("A$i:H$i")->applyFromArray($borders);
	$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(20);
	$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(20);
	$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(20);
	$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(20);
	$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(20);
	$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(20);
	$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(20);
	$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(40);

    $tablaPorServicio = 1;
    $tot = 0;
    $cont = 0;

    $encontrados = count($_POST['servicio1']);

    for($ij = 0; $ij < $encontrados; $ij++){
        
        $cont+=1;
        if($serv != $_POST['servicio1'][$ij]){
            if($tablaPorServicio == 0){
                $tot = 1;
            }
            $tablaPorServicio = 1;
            //$tot = 0;
        }

        if($tot == 1){

            $objPHPExcel->setActiveSheetIndex(0)
            ->setCellValueExplicit ("A$i", 'TOTAL', PHPExcel_Cell_DataType :: TYPE_STRING)
            ->setCellValueExplicit ("B$i", $_POST['totUsuarios1'][$cont], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
            ->setCellValueExplicit ("C$i", $_POST['totalTarifaCobrada1'][$cont], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
            ->setCellValueExplicit ("D$i", $_POST['totSubsidio1'][$cont], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
            ->setCellValueExplicit ("E$i", $_POST['totContribucion1'][$cont], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
            ->setCellValueExplicit ("F$i", $_POST['total1'][$cont], PHPExcel_Cell_DataType :: TYPE_NUMERIC);
            $objPHPExcel->getActiveSheet()->getStyle("A$i:F$i")->applyFromArray($styleArray);
            $i++;
        }

        if($tablaPorServicio == 1){

            $objPHPExcel->getActiveSheet()->mergeCells("A$i:F$i");
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue("A$i", 'REPORTE SUBSIDIO PERIODO DE FACTURACION: '.$_POST['corte'].' - '.$_POST['nombreServ1'][$ij]);
            $objFont=$objPHPExcel->getActiveSheet()->getStyle("A$i")->getFont();
            $objFont->setName('Courier New');
            $objFont->setSize(15);
            $objFont->setBold(true);
            $objFont->setUnderline(PHPExcel_Style_Font::UNDERLINE_SINGLE);
            $objFont->getColor()->setARGB( PHPExcel_Style_Color::COLOR_BLACK);
            $objAlign=$objPHPExcel->getActiveSheet()->getStyle("A$i")->getAlignment(); 
            $objAlign->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $objAlign->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
            $objPHPExcel-> getActiveSheet ()
                -> getStyle ("A$i")	
                -> getFill ()
                -> setFillType (PHPExcel_Style_Fill :: FILL_SOLID)
                -> getStartColor ()
                -> setRGB ('A6E5F3');
            $i+=1;
            $objPHPExcel-> getActiveSheet ()
                -> getStyle ("A$i:F$i")
                -> getFill ()
                -> setFillType (PHPExcel_Style_Fill :: FILL_SOLID)
                -> getStartColor ()
                -> setRGB ('A6E5F3');

            $objPHPExcel->getActiveSheet()->getStyle("A$i:F$i")->applyFromArray($borders);
            $objPHPExcel->setActiveSheetIndex(0)
                ->setCellValue("A$i", 'Clase de uso')
                ->setCellValue("B$i", 'Usuarios')
                ->setCellValue("C$i", 'Valor Total Tarifa Cobrada')
                ->setCellValue("D$i", 'Valor Subsidio')
                ->setCellValue("E$i", 'Contribución Facturaci&oacute;n de consumo')
                ->setCellValue("F$i", 'Valor a pagar (Diferencia Sub. menos Cont.)');

            $serv = $_POST['servicio1'][$ij];
			$tablaPorServicio = 0;
            $i+=1;
            
        }


        $objPHPExcel->setActiveSheetIndex(0)
            ->setCellValueExplicit ("A$i", $_POST['claseDeUso1'][$ij], PHPExcel_Cell_DataType :: TYPE_STRING)
            ->setCellValueExplicit ("B$i", $_POST['rowCargoFijo1'][$ij], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
            ->setCellValueExplicit ("C$i", $_POST['totalTarifaCobradaD1'][$ij], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
            ->setCellValueExplicit ("D$i", $_POST['subsidio1'][$ij], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
            ->setCellValueExplicit ("E$i", $_POST['contribucion1'][$ij], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
            ->setCellValueExplicit ("F$i", '', PHPExcel_Cell_DataType :: TYPE_NUMERIC);

        $i++;
        
        if($encontrados == $cont){
            $objPHPExcel->setActiveSheetIndex(0)
            ->setCellValueExplicit ("A$i", 'TOTAL', PHPExcel_Cell_DataType :: TYPE_STRING)
            ->setCellValueExplicit ("B$i", $_POST['totUsuarios1'][$cont], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
            ->setCellValueExplicit ("C$i", $_POST['totalTarifaCobrada1'][$cont], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
            ->setCellValueExplicit ("D$i", $_POST['totSubsidio1'][$cont], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
            ->setCellValueExplicit ("E$i", $_POST['totContribucion1'][$cont], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
            ->setCellValueExplicit ("F$i", $_POST['total1'][$cont], PHPExcel_Cell_DataType :: TYPE_NUMERIC);
            $objPHPExcel->getActiveSheet()->getStyle("A$i:F$i")->applyFromArray($styleArray);
            $i++;
        }
    }

    
	$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(20);
	$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(20);
	$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(20);
	$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(20);
	$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(20);
	$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(30);

	$objPHPExcel->getActiveSheet()->setTitle('Subsidios');
	header('Content-Type: application/vnd.ms-excel');
	header('Content-Disposition: attachment;filename="Reporte Subsidios.xlsx"');
	header('Cache-Control: max-age=0');
	$objWriter=PHPExcel_IOFactory::createWriter($objPHPExcel,'Excel2007');
	$objWriter->save('php://output');
	exit;
?>