<?php
	header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=utf8");
	ini_set('max_execution_time',3600);

	require "comun.inc";
	require "funciones.inc";
	require "conversor.php";
	require "validaciones.inc";

	$linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");

	session_start();
	date_default_timezone_set("America/Bogota");
?>
<!DOCTYPE > 
<html lang="es">
	<head>
		<meta charset="utf-8"/>
		<meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1"/>
		<meta name="viewport" content="user-scalable=no">
		<title>:: IDEAL 10 - Contabilidad</title>
		<link href="favicon.ico" rel="shortcut icon"/>
		<link href="css/css2.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<link href="css/css4.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
		<link href="sweetalert2/dist/sweetalert2.min.css" rel="stylesheet">
		<script src="sweetalert2/dist/sweetalert2.min.js"></script>
		<script type="text/javascript" src="jquery-1.11.0.min.js"></script>
		<script type="text/javascript" src="css/programas.js"></script>
		<script type="text/javascript" src="css/calendario.js"></script>
		<script>
			$(window).load(function () {
				$('#cargando').hide();
			});
			function excell(){
				//document.form2.action = "cont-conciliacionexcel.php";
				document.form2.target = "_BLANK";
				document.form2.submit();
				document.form2.action = "";
				document.form2.target = "";
			}
			function buscar(){
				document.form2.oculto.value = "2";
				document.form2.submit();
			}
			function despliegamodal2(_valor,_tipo,_saldo,_salcal,_difer){
				document.getElementById("bgventanamodal2").style.visibility=_valor;
				if(_valor=="hidden"){
					document.getElementById('ventana2').src="";
				}else {
					let fecha1 = document.getElementById('fc_1198971545').value;
					let fecha2 = document.getElementById('fc_1198971546').value;
					document.getElementById('ventana2').src="cont-archivos_extractos.php?cuenta=" + _tipo + "&fecha1=" + fecha1 + "&fecha2=" + fecha2 + "&saldoex=" + _saldo + "&saldocal=" + _salcal + "&difer=" + _difer;
				}
			}
		</script>
	</head>
	<body>
		<div id="cargando" style="position:absolute;left: 46%; bottom: 45%">
			<img src="imagenes/loading.gif" style=" width: 80px; height: 80px"/>
		</div>
		<IFRAME src="alertas.php" name="alertas" id="alertas" style="display:none"></IFRAME>
		<span id="todastablas2"></span>
		<table>
			<tr>
				<script>barra_imagenes("teso");</script>
				<?php cuadro_titulos();?>
			</tr>
			<tr>
				<?php menu_desplegable("teso");?>
			</tr>
			<tr>
				<td colspan="3" class="cinta">
					<img src="imagenes/add.png" title="Nuevo" onClick="location.href='teso-cuentas_xp_nomina.php'" class="mgbt"/>
					<img src="imagenes/guardad.png" class="mgbt1"/>
					<img src="imagenes/buscad.png" class="mgbt1"/>
					<img src="imagenes/agenda1.png" title="Agenda" onClick="mypop=window.open('plan-agenda.php','','');mypop.focus()" class="mgbt"/>
					<img src="imagenes/nv.png" title="Nueva ventana" class="mgbt" onClick="mypop=window.open('principal.php','',''); mypop.focus();">
					<img src="imagenes/excel.png" title="Excel" onClick="excell()" class="mgbt">
					<img src="imagenes/iratras.png" title="Atr&aacute;s"  onClick="location.href='teso-informestesoreria.php'" class="mgbt">
				</td>
			</tr>
		</table>
		<form name="form2" id="form2" method="post"> 
			<?php
				if($_POST['oculto'] == ''){
					$_POST['fechai'] = date("01/m/Y");
					$fechafv = new DateTime();
					$fechafv->modify('last day of this month');
					$_POST['fechaf'] = $fechafv->format('d/m/Y');
				}
			?>
			<table class="inicio ancho">
				<tr>
					<td colspan='8' class='titulos'>.: Buscar Conciliacion</td>
					<td class="cerrar" style="width:7%" onClick="location.href='teso-principal.php'">Cerrar</td>
				</tr>
				<tr>
					<td class="saludo1" style="width:3cm;">Fecha Inicial:</td>
					<td style="width:15%;"><input type="text" name="fechai" id="fc_1198971545" title="DD/MM/YYYY" value="<?php echo $_POST['fechai']; ?>" onKeyUp="return tabular(event,this) " onKeyDown="mascara(this,'/',patron,true)" maxlength="10" class="colordobleclik" autocomplete="off" onChange="" onDblClick="displayCalendarFor('fc_1198971545');"></td>
					<td class="saludo1" style="width:3cm;">Fecha Final:</td>
					<td style="width:15%;"><input type="text" name="fechaf" id="fc_1198971546" title="DD/MM/YYYY" value="<?php echo $_POST['fechaf']; ?>" onKeyUp="return tabular(event,this) " onKeyDown="mascara(this,'/',patron,true)" maxlength="10"  class="colordobleclik" autocomplete="off" onChange="" onDblClick="displayCalendarFor('fc_1198971546');"></td>
					<td style="padding-bottom:0px"><em class="botonflechaverde" onClick="buscar()">Buscar</em></td>
				</tr>
			</table>
			<input type="hidden" name="oculto" id="oculto" value="1"/>
			<table class='tablamv3'>
				<?php
					$fanio = $_POST['anio'];
					$fecha = $_POST['fechai'];
					$ffecha = $_POST['fechaf'];
					preg_match("/([0-9]{2})\/([0-9]{2})\/([0-9]{4})/", $_POST['fechai'],$fechatt);
					$fechaini = $fechatt[3]."-".$fechatt[2]."-".$fechatt[1];
					preg_match("/([0-9]{2})\/([0-9]{2})\/([0-9]{4})/", $_POST['fechaf'],$fechatt);
					$fechafin = $fechatt[3]."-".$fechatt[2]."-".$fechatt[1];
					echo "
					<thead>	
						<tr style='text-align:Center;'>
							<th class='titulosnew00' style='width:3%;'></th>
							<th class='titulosnew00' style='width:10%;'>Nomina</th>
							<th class='titulosnew00' style='width:17%;'>Cuenta</th>
							<th class='titulosnew00' style='width:14%;'>Fuente</th>
							<th class='titulosnew00' style='width:14%;'>indicador</th>
							<th class='titulosnew00' style='width:14%;'>Bpin</th>
							<th class='titulosnew00' style='width:14%;'>Valor</th>
							<th class='titulosnew00' style='width:14%;'>Saldo</th>
						</tr>
					</thead>
					<tbody style='height:60vh;'>";
					if($fecha != '' && $ffecha != '' && $_POST['oculto'] == '2'){
						$sqlr="SELECT T1.id_nom, T1.cuenta, T1.valor, T1.fuente, T1.indicador, T1.bpin, T1.seccion_presupuestal FROM humnom_presupuestal AS T1 INNER JOIN humnomina AS T2 ON T1.id_nom = T2.id_nom WHERE T1.estado = 'P' AND T2.fecha BETWEEN '$fechaini' AND '$fechafin' ORDER BY T1.id_nom";
						$resp = mysqli_query($linkbd, $sqlr);
						$iter = 'saludo1a';
						$iter2 = 'saludo2';
						$cont = 1;
						while ($row = mysqli_fetch_row($resp)){
							$restric = " AND NOT(T2.estado = 'N' OR T2.estado = 'R') AND NOT(T1.tipo = 'SE' OR T1.tipo = 'PE' OR T1.tipo = 'DS' OR T1.tipo = 'RE' OR T1.tipo = 'FS') AND T2.fecha BETWEEN '$fechaini' AND '$fechafin'";
							$sqlr1 = "SELECT SUM(T1.valordevengado) FROM tesoegresosnomina_det AS T1 INNER JOIN tesoegresosnomina AS T2 ON T1.id_egreso = T2.id_egreso WHERE T1.id_orden = '$row[0]' AND T1.cuentap = '$row[1]' AND T1.fuente = '$row[3]' AND T1.indicador_producto = '$row[4]' AND T1.bpin = '$row[5]' AND T1.seccion_presupuestal = '$row[6]' $restric";
							$resp1 = mysqli_query($linkbd, $sqlr1);
							$row1 = mysqli_fetch_row($resp1);
							$dif = $row[2] - $row1[0];
							
							if ($dif != 0){
								$aux = $iter;
								$iter = $iter2;
								$iter2 = $aux;
								echo"<tr class='$iter'>
										<td style='width:3.2%;'>$cont</td>
										<td style='width:10%;'>$row[0]</td>
										<td style='width:17.3%;'>$row[1]</td>
										<td style='width:14%;'>$row[3]</td>
										<td style='width:14.2%;'>$row[4]</td>
										<td style='width:14.2%;'>$row[5]</td>
										<td style='width:14.1%;text-align:right;'>$".number_format($row[2],2,',','.')."</td>
										<td style='width:14.5%;text-align:center;'>$".number_format($dif,2,',','.')."</td>
									</tr>
									<input type='hidden' name='concepto1[]' value='".$row[1]."'/>
									<input type='hidden' name='concepto2[]' value='".$row[3]."'/>
									<input type='hidden' name='concepto3[]' value='$".number_format($val,2,',','.')."'/>
									<input type='hidden' name='concepto4[]' value='$".number_format($row1[2],2,',','.')."'/>
									<input type='hidden' name='concepto5[]' value='$".number_format($dif,2,',','.')."'/>
									<input type='hidden' name='concepto6[]' value='$".number_format($saldoperant,2,',','.')."'/>
									";
								$cont+=1;
							}
						}
					}elseif($_POST['oculto'] != ''){
						echo"
						<script>
							Swal.fire({
								icon: 'info',
								title: 'Debe ingresar fechas para el informe',
								confirmButtonText: 'Continuar',
								confirmButtonColor: '#FF121A',
								timer: 2500
							});
						</script>
						";

					}
				?>
			</table>
		</form>
		<div id="bgventanamodal2">
			<div id="ventanamodal2">
				<IFRAME  src="" name="buster" marginWidth=0 marginHeight=0 frameBorder=0 id="ventana2" frameSpacing=0 style="left:500px; width:900px; height:500px; top:200;"></IFRAME>
			</div>
		</div>
	</body>
</html>
