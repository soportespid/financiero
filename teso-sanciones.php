<?php
	header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=utf8");
	require "comun.inc";
	require "funciones.inc";
	session_start();
	$linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");
	cargarcodigopag($_GET['codpag'],$_SESSION["nivel"]);
	date_default_timezone_set("America/Bogota");
?>
<!DOCTYPE >
<html lang="es">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1">
		<meta name="viewport" content="user-scalable=no">
		<title>:: IDEAL 10 - Parametrización</title>
		<link href="favicon.ico" rel="shortcut icon"/>
		<link href="css/css2z.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet"/>
		<link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet"/>
		<link href="css/css4.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet"/>
		<link href="sweetalert2/dist/sweetalert2.min.css" rel="stylesheet">
		<script src="sweetalert2/dist/sweetalert2.min.js"></script>
		<script type="text/javascript" src="css/programas.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script>
			function validar(){
				document.form2.submit();
			}
			function guardar(){
				if (document.form2.codigo.value!='' && document.form2.nombre.value!=''){
					Swal.fire({
						icon: 'question',
						title: '¿Esta seguro de guardar la sanción?',
						showDenyButton: true,
						confirmButtonText: 'Guardar',
						confirmButtonColor: '#01CC42',
						denyButtonText: 'Cancelar',
						denyButtonColor: '#FF121A',
					}).then(
						(result) => {
							if (result.isConfirmed){
								document.getElementById('oculto').value = '2';
								document.form2.submit();
							}
							else if (result.isDenied){
								Swal.fire({
									icon: 'info',
									title: 'No se guardo la sanción',
									confirmButtonText: 'Continuar',
									confirmButtonColor: '#FF121A',
									timer: 2500
								});
							}
						}
					)
				}else{
					Swal.fire({
						icon: 'error',
						title: 'Error!',
						text: 'Faltan información para dejar guardar',
						confirmButtonText: 'Continuar',
						confirmButtonColor: '#FF121A',
						timer: 2500
					});
				}
			}
		</script>
	</head>
	<body>
		<IFRAME src="alertas.php" name="alertas" id="alertas" style="display:none"></IFRAME>
		<span id="todastablas2"></span>
		<table>
			<tr><script>barra_imagenes("para");</script><?php cuadro_titulos();?></tr>	 
			<tr><?php menu_desplegable("para");?></tr>
			<tr>
				<td colspan="3" class="cinta">
					<a href="teso-sanciones.php" class="mgbt"><img src="imagenes/add.png" title="Nuevo"/></a> 
					<a onClick="guardar();" class="mgbt"><img src="imagenes/guarda.png" title="Guardar"/></a>
					<a href="teso-buscasanciones.php" class="mgbt" > <img src="imagenes/busca.png" title="Buscar"/></a>
					<a onClick="mypop=window.open('plan-agenda.php','','');mypop.focus()" class="mgbt"><img src="imagenes/agenda1.png" title="Agenda"/></a> 
					<a onClick="mypop=window.open('para-principal.php','','');mypop.focus();" class="mgbt" ><img src="imagenes/nv.png" title="Nueva ventana"></a> 
					<a onClick="mypop=window.open('<?php echo $url2; ?>','','');mypop.focus();" class="tooltip bottom mgbt"><img src="imagenes/duplicar_pantalla.png" title="Duplicar pesta&ntilde;a"></a>
				</td>
			</tr>		  
		</table>
		<form name="form2" method="post" action="">
			<?php
				$vigencia=date('Y');
				if(!$_POST['oculto']){
					$fec=date("d/m/Y");
					$_POST['fecha']=$fec;
					$_POST['valoradicion']=0;
					$_POST['valorreduccion']=0;
					$_POST['valortraslados']=0;
					$_POST['valor']=0;
				}
			?>
			<table class="inicio ancho">
				<tr >
					<td class="titulos" colspan="9">Crear Sanción</td>
					<td class="cerrar" style="width:7%" onClick="location.href='teso-principal.php'">Cerrar</td>
				</tr>
				<tr >
					<td style="width:5%;" class="tamano01">Código:</td>
					<td style="width:10%;">
						<input name="codigo" id='codigo' type="text" value="<?php echo $_POST['codigo']?>" maxlength="2" onKeyPress="javascript:return solonumeros(event)" onKeyUp="return tabular(event,this)" >
					</td>
					<td style="width:5%;" class="tamano01">Nombre:</td>
					<td style="width:30%;">
						<input name="nombre" type="text" value="<?php echo $_POST['nombre']?>"  onKeyUp="return tabular(event,this)" style="width:100%;">       
					</td>
					<td style="width:6%;" class="tamano01">Porcentaje:</td>
					<td><input id="valor" name="valor" type="text" value="<?php echo $_POST['valor']?>" onKeyUp="return tabular(event,this)"  onKeyPress="javascript:return solonumeros(event)" style="width:25%;"> %</td>
				</tr> 
			</table>
			<input type="hidden" name="oculto" id="oculto" value="1">
			<div id="bgventanamodal2">
				<div id="ventanamodal2">
					<IFRAME  src="" name="buster" marginWidth=0 marginHeight=0 frameBorder=0 id="ventana2" frameSpacing=0 style="left:500px; width:900px; height:500px; top:200;"> 
					</IFRAME>
				</div>
			</div>
			<?php
				$oculto=$_POST['oculto'];
				if($_POST['oculto']=='2'){
					if ($_POST['nombre']!="" && $_POST['codigo']!="" ){  
						$nr="1";
						$sqlr="INSERT INTO tesosanciones (id_sancion, nombre, cuentacontable, cuentapptal, porcentaje, estado)VALUES ('".$_POST['codigo']."', '".$_POST['nombre']."', '','','".$_POST['valor']."','S')";
						if (!mysqli_query($linkbd,$sqlr)){
							echo"
							<script>
								Swal.fire({
									icon: 'error',
									title: 'Error!',
									text: 'No se pudo guardar la sanción',
									confirmButtonText: 'Continuar',
									confirmButtonColor: '#FF121A',
									timer: 2500
								});
							</script>";
						}else{
							echo"
							<script>
								Swal.fire({
									icon: 'success',
									title: 'Se ha guardo con Exito la sanció N°: ".$_POST['codigo']."',
									confirmButtonText: 'Continuar',
									confirmButtonColor: '#FF121A',
									timer: 3000
								}).then((response) => {
									location.href = 'teso-editasanciones.php?idr=".$_POST['codigo']."';
								});
							</script>";
						}
					}else{ 
						echo"
						<script>
							Swal.fire({
								icon: 'error',
								title: 'Error!',
								text: 'Falta informacion para crear la sanción',
								confirmButtonText: 'Continuar',
								confirmButtonColor: '#FF121A',
								timer: 2500
							});
						</script>";
					}
				}
			?> 
		</form>
	</body>
</html>