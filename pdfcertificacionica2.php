<?php
	require_once("tcpdf/tcpdf_include.php");
	require('comun.inc');
	require "funciones.inc";
	require 'funcionesnomima.inc';
	session_start();
	class MYPDF extends TCPDF {
		public function Header() {
			$linkbd = conectar_v7();
			$linkbd -> set_charset("utf8");
			$sqlr = "SELECT * FROM configbasica WHERE estado='S'";
			$res = mysqli_query($linkbd,$sqlr);
			while($row = mysqli_fetch_row($res)){
				$nit = $row[0];
				$rs = $row[1];
			}
			$sqlr = "SELECT codnovedad FROM hum_prenomina WHERE num_liq ='".$_POST['idcomp']."'";
			$res = mysqli_query($linkbd,$sqlr);
			$row = mysqli_fetch_row($res);
			$sqlrcc = "SELECT T1.descripcion FROM hum_novedadespagos_cab AS T1 INNER JOIN hum_prenomina AS T2 ON T1.prenomina = T2.codigo WHERE num_liq = '".$_POST['idcomp']."'";
			$rowcc = mysqli_fetch_row(mysqli_query($linkbd,$sqlrcc));
			$descrip = $rowcc[0];
			$this->Image('imagenes/escudo.jpg', 22, 12, 25, 23.9, 'JPG', '', 'T', true, 300, '', false, false, 0, false, false, false);// 
			$this->SetFont('helvetica','B',7);
			$this->SetY(10);
			$this->RoundedRect(10, 10, 199, 31, 2,'' );
			$this->Cell(0.1);
			$this->Cell(50,31,'','R',0,'L'); 
			$this->SetY(10);
			$this->SetX(58);
			$this->SetFont('helvetica','B',12);
			$this->Cell(142,15,"$rs",0,0,'C'); 
			$this->SetY(16);
			$this->SetX(58);
			$this->SetFont('helvetica','B',11);
			$this->Cell(142,10,"$nit",0,0,'C');
			
			//*****************************************************************************************************************************
			$this->SetFont('helvetica','B',14);
			$this->SetY(10);
			$this->Cell(50.1);
			$this->Cell(149,31,'',0,1,'C'); 
			$this->SetY(8);
			$this->Cell(50.1);
			//************************************
			$this->SetFont('helvetica','B',10);
			$this->SetY(27);
			$this->Cell(164.1);
			$this->Cell(35,14,'','TL',0,'L');
			$this->SetY(27.5);
			$this->Cell(164);
			$this->Cell(35,5,'NUMERO','B',0,'C');
			$this->SetY(34);
			$this->Cell(164);
			$this->Cell(35,5,$_POST['id'],0,0,'C');
		
			$ncc = buscacentro($_POST['cc']);
			if($ncc=='')
			$ncc='TODOS';
			$this->SetY(27);
			$this->Cell(50.2);
			$this->Cell(114,7,'CERTIFICACION DE INSCRIPCION INDUSTRIA Y COMERCIO','T',1,'C');
			$this->Cell(50.2);
			$this->Cell(114,5,'Fecha Expedicion: '.date("Y-m-d"),0,1,'C');
			$this->SetFont('helvetica','B',12);
		}
		public function Footer(){
			$linkbd = conectar_v7();
			$linkbd -> set_charset("utf8");
			$sqlr = "SELECT direccion, telefono, web, email FROM configbasica WHERE estado='S'";
			$resp = mysqli_query($linkbd,$sqlr);
			while($row = mysqli_fetch_row($resp)){
				$direcc = $row[0];
				$telefonos = $row[1];
				$dirweb = $row[3];
				$coemail = $row[2];
			}
			if($direcc != ''){
                $vardirec = "Dirección: $direcc, ";
            }else {
                $vardirec = "";
            }
			if($telefonos != ''){
                $vartelef="Telefonos: $telefonos";
            }
			else{$vartelef = "";}
			if($dirweb != ''){
                $varemail = "Email: $dirweb, ";
            } else {
                $varemail = "";
            }
			if($coemail != ''){
                $varpagiw = "Pagina Web: $coemail";
            } else{
                $varpagiw = "";
            }
			//$this->SetY(-16);
			$this->SetFont('helvetica', 'I', 8);
			$txt = <<<EOD
$vardirec $vartelef
$varemail $varpagiw
EOD;
			$this->Write(0, $txt, '', 0, 'C', true, 0, false, false, 0);
			//$this->SetY(-13);
			$this->Cell(0, 10, 'Page '.$this->getAliasNumPage().'/'.$this->getAliasNbPages(), 0, false, 'C', 0, '', 0, false, 'T', 'M');
			
		}
	}
	$pdf = new MYPDF('P','mm','Letter', true, 'UTF-8', false);
	$pdf->SetDocInfoUnicode (true); 
	$pdf->SetCreator(PDF_CREATOR);
	$pdf->SetAuthor('IDEAL10');
	$pdf->SetMargins(10, 101, 10);
	$pdf->SetHeaderMargin(101);
	$pdf->SetFooterMargin(20);
	$pdf->SetAutoPageBreak(TRUE, 20);
	if (file_exists(dirname(__FILE__).'/lang/spa.php')){
		require_once(dirname(__FILE__).'/lang/spa.php');
		$pdf->setLanguageArray($l);
	}
	// ---------------------------------------------------------
	$pdf->AddPage();
	$meses = array("", "Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre");
	$linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");
	$sqlr = "SELECT * FROM configbasica WHERE estado = 'S'";
	$res = mysqli_query($linkbd,$sqlr);
	while($row = mysqli_fetch_row($res)){
		$nit = $row[0];
		$rs = $row[1];
	}
	$sqlt = "SELECT titulo FROM firmaspdf_det WHERE idfirmas='5' AND estado ='S'";
	$rest = mysqli_query($linkbd,$sqlt);
	$row = mysqli_fetch_row($rest);
	$titulort = $row[0];
	$pdf->SetFont('times','B',14);
	$pdf->SetY(43.7);
	$pdf->ln(18);
	$pdf->Cell(20);
	$pdf->Cell(150,4,$titulort.' DEL '.strtoupper($rs),'',1,'C');
	$pdf->ln(14);
	$pdf->SetFont('times','',14);
	$otros="";
	$mm = substr($_POST['fechain'],5,2)*1;
	if($_POST['estmatricula'] == 'Matricula Activa'){
		$texto = "Certifica que ".$_POST['tnombrecom']." con NIT N° ".$_POST['nombrecom']." y cuyo representante legal actual es ".$_POST['ntercero']." con CC/NIT N° ".$_POST['tercero'].", ha sido debidamente inscrita en el Registro de Industria y Comercio del Municipio a partir del ".substr($_POST['fechain'],8,2)." días del Mes de ".$meses[$mm]." de ".substr($_POST['fechain'],0,4)." con número de matrícula ".$_POST['id'].".";
        
		$texto1 = "Se encuentran registrados los siguientes establecimientos: ";
		$texto2 ="Se encuentra registrado el siguiente establecimiento: ";
	} else {
        $texto = "Certifica que ".$_POST['tnombrecom']." con NIT N° ".$_POST['nombrecom']." y cuyo representante legal actual es ".$_POST['ntercero']." con CC/NIT N° ".$_POST['tercero'].", se encontraba debidamente inscrita en el Registro de Industria y Comercio del Municipio a partir del ".substr($_POST['fechain'],8,2)." días del Mes de ".$meses[$mm]." de ".substr($_POST['fechain'],0,4)." con número de matrícula ".$_POST['id'].".";
		$texto1 = "Se encontraban registrados los siguientes establecimientos con esta Matrícula: ";
		$texto2 ="Se encontraba registrado el siguiente establecimiento con esta Matrícula: ";
	}
	$pdf->MultiCell(0,4,$texto,0,'L');
	$pdf->ln(10);
	$pdf->ln(6);

	$texto='';
	if(count($_POST['numMatriculaDet'])>1){
		$pdf->SetFont('times','I',14);
		$pdf->MultiCell(0,4,$texto1,0,'L');
		$pdf->SetFont('times','',12);
		$pdf->ln(6);
	} elseif(count($_POST['numMatriculaDet'])==1){
		$pdf->SetFont('times','I',14);
		$pdf->MultiCell(0,4,$texto2,0,'L');
		$pdf->SetFont('times','',12);
		$pdf->ln(6);
	}
	for ($x=0;$x<count($_POST['numMatriculaDet']);$x++) {
		$texto = " - ".$_POST['razonSocialDet'][$x]." Dirección: ".$_POST['direccionEstablecimientoDet'][$x]." Valor Activos: ".$_POST['valorActivoDet'][$x];
		$pdf->SetFont('times','I',9);
		$pdf->MultiCell(0,4,$texto,0,'L');
		$pdf->SetFont('times','',12);
		$pdf->ln(6);
	}
	$pdf->ln(6);
	$mm = substr(date("Y-m-d"),5,2)*1;
	$texto = "La presente certificación se expide expide, a los ".substr(date("Y-m-d"),8,2)." días del Mes de ".$meses[$mm]." de ".substr(date("Y-m-d"),0,4);
	$pdf->MultiCell(0,4,$texto,0,'L');
	$pdf->ln(10);
	$sqlr = "SELECT funcionario, nomcargo FROM firmaspdf_det WHERE idfirmas='5' AND estado ='S' ORDER BY orden";
	$res = mysqli_query($linkbd,$sqlr);
	while($row = mysqli_fetch_row($res)){
		$_POST['ppto'][] = $row[0];
		$_POST['nomcargo'][] = $row[1];
	}
	for($x=0;$x<count($_POST['ppto']);$x++){
		$pdf->ln(14);
		$v = $pdf->gety();
		if($v>=251){
			$pdf->AddPage();
			$pdf->ln(20);
			$v = $pdf->gety();
		}
		$pdf->setFont('times','B',9);
		if (($x%2)==0){
			if(isset($_POST['ppto'][$x+1])){
				$pdf->Line(17,$v,107,$v);
				$pdf->Line(112,$v,202,$v);
				$v2 = $pdf->gety();
				$pdf->Cell(104,4,''.$_POST['ppto'][$x],0,1,'C',false,0,0,false,'T','C');
				$pdf->Cell(104,4,''.$_POST['nomcargo'][$x],0,1,'C',false,0,0,false,'T','C');
				$pdf->SetY($v2);
				$pdf->Cell(295,4,''.$_POST['ppto'][$x+1],0,1,'C',false,0,0,false,'T','C');
				$pdf->Cell(295,4,''.$_POST['nomcargo'][$x+1],0,1,'C',false,0,0,false,'T','C');
			} else {
				$pdf->Line(50,$v,160,$v);
				$pdf->Cell(190,4,''.$_POST['ppto'][$x],0,1,'C',false,0,0,false,'T','C');
				$pdf->Cell(190,4,''.$_POST['nomcargo'][$x],0,0,'C',false,0,0,false,'T','C');
			}
			$v3=$pdf->gety();
		}
		$pdf->SetY($v3);
		$pdf->SetFont('helvetica','',8);
	}
	
	$pdf->Output('solicitudcdp.pdf', 'I');
?>