<?php
	header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=utf8");
	require "comun.inc";
	require "funciones.inc";
	session_start();
	$linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");
	cargarcodigopag($_GET['codpag'],$_SESSION["nivel"]);
	date_default_timezone_set("America/Bogota");
?>
<!DOCTYPE >
<html lang="es">
	<head>
	<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="ie=edge"/>
		<meta name="viewport" content="user-scalable=no">
		<title>IDEAL 10 - Tesoreria</title>
		<link href="favicon.ico" rel="shortcut icon"/>
		<link href="css/css2z.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css"/>
		<link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css"/>
		<link href="css/css4.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css"/>
		<link href="css/tabs.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css"/>
		<link href="sweetalert2/dist/sweetalert2.min.css" rel="stylesheet">
		<script src="sweetalert2/dist/sweetalert2.min.js"></script>
		<script src="vue/vue.js"></script>
		<script type="text/javascript" src="css/programas.js?<?php echo date('d_m_Y_h_i_s');?>"> </script>
		<script type="text/javascript" src="css/calendario.js?<?php echo date('d_m_Y_h_i_s');?>"> </script>
		<script>
			function pdf(){
				let id_nota = document.getElementById('idnota').value; 
				document.form2.action="teso-notasbancariaspdf_new?idnota=" + id_nota;
				document.form2.target="_BLANK";
				document.form2.submit(); 
				document.form2.action="";
				document.form2.target="";
			}
		</script>
	</head>
	<body>
		<header>
			<table>
				<tr><script>barra_imagenes("teso");</script><?php cuadro_titulos();?></tr>
			</table>
		</header>
		<section id="myapp" v-cloak >
			<form method="post" name="form2" id="form2">
			<nav>
				<table>
					<tr><?php menu_desplegable("teso");?></tr>
					<tr>
						<td colspan="3" class="cinta">
							<img src="imagenes/add.png" onClick="location.href='teso-notasbancarias_new'" class="mgbt" title="Nuevo"/>
							<img src="imagenes/guarda.png" title="Guardar" v-on:click="validarGuardar();" class="mgbt"/>
							<img src="imagenes/busca.png" onClick="location.href='teso-buscanotasbancarias_new'" class="mgbt" title="Buscar"/>
							<img src="imagenes/agenda1.png" title="Agenda" onClick="mypop=window.open('teso-principal','','');mypop.focus()" class="mgbt"/>
							<img src="imagenes/nv.png" onClick="mypop=window.open('teso-principal','',''); mypop.focus();" class="mgbt" title="Nueva Ventana">
							<img src="imagenes/print.png" title="Imprimir" onClick="pdf()" class="mgbt" >
							<img src="imagenes/iratras.png" title="Atr&aacute;s" v-on:Click="flechaVerde()" class="mgbt">
						</td>
					</tr>
				</table>
			</nav>
			<article>
				<table class="inicio ancho">
					<tr>
						<td class="titulos" colspan="8" >Notas Bancarias</td>
						<td class="cerrar" style="width:7%" onClick="location.href='teso-principal.php'">Cerrar</td>
					</tr>
					<tr>
						<td class="tamano01" style="width:3cm">Tipo Movimiento:</td>
						<td colspan="3">
							<select v-model="tipoMovimiento" style="width:100%;text-transform: uppercase;" v-on:click="">
								<option v-for="vcTipoMivimiento in selectTipoMovimiento" :value="vcTipoMivimiento[3]" >{{ vcTipoMivimiento[0] }}{{ vcTipoMivimiento[1] }} - {{ vcTipoMivimiento[2] }}</option>
							</select>
						</td>
                        <td class="tamano01">:&middot; Estado:</td>
                        <td><input type="text" v-model="estado" :class="inputClass"></td>
					</tr>
					<tr>
						<td class="tamano01" style="width:3cm">:&middot; N° Comprobante:</td>
						<td><img src="imagenes/back.png" v-on:Click="atrasc()" class="icobut" title="Anterior"/>&nbsp;<input type="text" v-model="numeroNota" :value="numeroNota=<?php echo $_GET['id']; ?>" id="idnota" style="width:75%" readonly/>&nbsp;<img src="imagenes/next.png" v-on:Click="adelante()" class="icobut" title="Sigiente"/></td>
						<td class="tamano01" style="width:3cm;">:&middot; Fecha:</td>
						<td style="width:10%;"><input type="text" name="fecha"  value="<?php echo $_POST['fecha']?>" onKeyUp="return tabular(event,this)" id="fc_1198971545" title="DD/MM/YYYY" onDblClick="displayCalendarFor('fc_1198971545');" class="colordobleclik" autocomplete="off" onChange="" readonly></td>
						<td class="tamano01" style="width:3cm">:&middot; Centro de Costo:</td>
						<td colspan="2">
							<select v-model="centroCosto" style="width:100%;text-transform: uppercase;" v-on:click="" v-on:change="buscarGastoBancario();" >
								<option v-for="vcCentroCosto in selectCentroCosto" :value="vcCentroCosto[0]" >{{ vcCentroCosto[0] }} - {{ vcCentroCosto[1] }}</option>
							</select>
						</td>
					</tr>
					<tr>
						<td class="tamano01">:&middot; Concepto:</td>
						<td colspan="3"><input type="text" v-model="concepto" style="width:100%" placeholder="Objeto de la nota bancaria"/></td>
						<td class="tamano01">:&middot; Doc. Banco:</td>
						<td colspan="2"><input type="text" v-model="docBanco" style="width:100%" /></td>
					</tr>
					<tr>
						<td class="tamano01">:&middot; Cuenta:</td>
						<td><input type="text" v-model="cuentaBancaria" style="width:100%" class="colordobleclik" v-on:change="validarcuenta()"  v-on:dblclick='toggleModal2'/></td>
						<td colspan="5"><input type="text" v-model="nomBanco" style="width:100%" readonly/></td>
						<input type="hidden" v-model="cuentaBancariaH"/>
						<input type="hidden" v-model="numBanco"/>
					</tr>
					<tr>
						<td class="tamano01">:&middot; Gasto Bancario:</td>
						<td colspan="3">
							<select v-model="gastoBancario" style="width:100%;text-transform: uppercase;" v-on:click="">
								<option v-for="vcGastoBancario in selectGastoBancario" :value="vcGastoBancario[3]" :disabled="vcGastoBancario[4] === ''" v-bind:class="(vcGastoBancario[4] == '' ? 'contenidonew05': '')">{{ vcGastoBancario[2] }} - {{ vcGastoBancario[0] }} - {{ vcGastoBancario[1] }}</option>
							</select>
						</td>
						<td class="tamano01">:&middot; Valor Nota:</td>
						<td><input type="text" v-model="valorNota" style="width:100%" onKeyPress="javascript:return solonumeros(event)"/></td>
						<td style="height: 30px;"><em class="botonflechaverde" v-on:click="agregarListaNotas()">Agregar</em></td>
					</tr>
				</table>
				<table class='tablamv'>
					<thead>
						<tr class="titulos">
							<th style="width:5%; font: 110% sans-serif;">Item</th>
							<th style="width:10%; font: 110% sans-serif;">Centro Costo</th>
							<th style="width:15%; font: 110% sans-serif;">Doc. banco</th>
							<th style="width:43%; font: 110% sans-serif;">Banco</th>
							<th style="width:10%; font: 110% sans-serif;">Gasto Banco</th>
							<th style="width:10%; font: 110% sans-serif;">valor</th>
							<th style="font: 110% sans-serif;">Eliminar</th>
						</tr>
					</thead>
					<tbody style="max-height: 32vh !important;">
						<tr v-for="(vcListaNotas, index) in selectListaNotas" v-bind:class="index % 2 ? 'saludo1a' : 'saludo2'" style='text-rendering: optimizeLegibility; cursor: pointer !important;' >
							<td style="font: 120% sans-serif; padding-left:10px; width:5%; text-align:center;">{{ index + 1}}</td>
							<td style="font: 120% sans-serif; padding-left:10px; width:10%; text-align:center;">{{ vcListaNotas[0] }}</td>
							<td style="font: 120% sans-serif; padding-left:10px; width:15%; text-align:center;">{{ vcListaNotas[2] }}</td>
							<td style="font: 120% sans-serif; padding-left:10px; width:42%;">{{ vcListaNotas[3] }} - {{ vcListaNotas[4] }}</td>
							<td style="font: 120% sans-serif; padding-left:10px; width:10%; text-align:center;">{{ vcListaNotas[7] }}</td>
							<td style="font: 120% sans-serif; padding-left:10px; width:13%; text-align:right;">{{ formatonumero(vcListaNotas[8]) }}</td>
							<td style='display: flex; justify-content: center;' v-on:click="eliminaListaNotas(index,vcListaNotas[8])" title="Eliminar"><div class='garbageX'></div></td>
						</tr>
					</tbody>
					<tbody style="overflow:hidden !important;">
						<tr>
							<td style='text-align:right; font: 120% sans-serif; width:79%;'>Total:</td>
							<td style='text-align:right; font: 120% sans-serif; width:15%;'>{{ formatonumero(valorListaNotas) }}</td>
							<td></td>
						</tr>
					</tbody>
				</table>
			</article>
			<div v-show="showModal2">
				<transition name="modal">
					<div class="modal-mask">
						<div class="modal-wrapper">
							<div class="modal-container2">
								<table class='tablamv'>
									<tbody style="overflow:hidden !important;">
										<tr >
											<td class="titulos"  colspan="2" >SELECCIONAR CUENTA</td>
											<td class="cerrar" style="width:7%;" @click="showModal2 = false">Cerrar</td>
										</tr>
										<tr>
											<td class="tamano01" style="width:3cm;">Cuenta:</td>
											<td style="width: 60%;"><input type="text" class="form-control" placeholder="Buscar por nombre del programa" v-on:keyup="buscaCuentaVentana" v-model="searchCuenta" style="width:100%;  height: 30px !important; padding: 5px;" /></td>
											<td></td>
										</tr>
									</tbody>
									<thead>
										<tr>
											<th class='titulos' style="width:5%; font: 110% sans-serif; border-radius: 5px 0 0 0;">N°</th>
											<th class='titulos' style="width:30%; font: 110% sans-serif;">Razón Social</th>
											<th class='titulos' style="width:35%; font: 110% sans-serif;">Descripción</th>
											<th class='titulos' style="width:11%; font: 110% sans-serif;">Cuenta Contable</th>
											<th class='titulos' style="width:11%; font: 110% sans-serif;">Cuenta Bancaria</th>
											<th class='titulos' style="font: 110% sans-serif; border-radius: 0 5px 0 0;">Tipo Cuenta</th>
										</tr>
									</thead>
									<tbody>
										<tr v-for="(vcCuentas,index) in selectCuentas" v-on:click="cargaInfoCuenta( vcCuentas[5],vcCuentas[0], vcCuentas[2], vcCuentas[3])" v-bind:class="index % 2 ? 'saludo1a' : 'saludo2'" style="text-rendering: optimizeLegibility; cursor: pointer !important;" >
											<td style="width:5%; font: 110% sans-serif; padding-left:10px">{{ index }}</td>
											<td style="width:30%; font: 110% sans-serif; padding-left:10px">{{ vcCuentas[0] }}</td>
											<td style="width:36%;font: 110% sans-serif; padding-left:10px">{{ vcCuentas[1] }}</td>
											<td style="width:11%; font: 110% sans-serif; padding-left:10px">{{ vcCuentas[2] }}</td>
											<td style="width:11%; font: 110% sans-serif; padding-left:10px">{{ vcCuentas[3] }}</td>
											<td style="font: 110% sans-serif; padding-left:10px">{{ vcCuentas[4] }}</td>
										</tr>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</transition>
			</div>
			<div id="cargando" v-if="loading" class="loading">
					<span>Cargando...</span>
			</div>
			</form>
		</section>
		<script src="Librerias/vue/vue.min.js"></script>
		<script src="Librerias/vue/axios.min.js"></script>
		<script src="vue/tesoreria/teso-editanotasbancarias_new.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
	</body>
</html>