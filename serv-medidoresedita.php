<?php
require"comun.inc";
require"funciones.inc";
session_start();
//**niveles menu: Administracion (0) - Consultas (1) - Herramientas (2) - Reportes (3)
?>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html" />
<title>:: Spid - Servicios P&uacute;blicos</title>
<script>
//************* ver reporte ************
//***************************************
function verep(idfac)
{
  document.form1.oculto.value=idfac;
  document.form1.submit();
  }
//************* genera reporte ************
//***************************************
function genrep(idfac)
{
  document.form2.oculto.value=idfac;
  document.form2.submit();
  }
//************* genera reporte ************
//***************************************
function guardar()
{
if (document.form2.codigo.value!='' && document.form2.fabricante.value!='' && document.form2.marca.value!='' && document.form2.serial.value!='' && document.form2.descrip.value!='' && document.form2.referencia.value!='' && document.form2.diametro.value!='')
  {
	if (confirm("Esta Seguro de Guardar"))
  	{
  	document.form2.oculto.value=2;
  	document.form2.submit();
  	}
  }
  else{
  alert('Faltan datos para completar el registro');
  	document.form2.fabricante.focus();
  	document.form2.fabricante.select();
  }
 }
 
 function diametros()
{
	var salida=document.getElementById("diametro1").value;
	document.getElementById("diametro").value=salida +"\"";
	document.getElementById("diametroo").value=salida;
}

function clasifica(formulario)
{
//document.form2.action="presu-recursos.php";
document.form2.submit();
}
</script>
<script type="text/javascript" src="css/programas.js"></script>
<link href="css/css2.css" rel="stylesheet" type="text/css" />
<link href="css/css3.css" rel="stylesheet" type="text/css" />
<?php titlepag();?>
</head>
<body>
<table>
	<tr><script>barra_imagenes("serv");</script><?php cuadro_titulos();?></tr>	 
	<tr><?php menu_desplegable("serv");?></tr>
        <tr>
  <td colspan="3" class="cinta"><a href="serv-medidores.php" ><img src="imagenes/add.png"  alt="Nuevo" border="0" /></a> <a href="#"  onClick="guardar()"><img src="imagenes/guarda.png"  alt="Guardar" /></a> <a href="serv-medidoresbusca.php"><img src="imagenes/busca.png"  alt="Buscar" border="0" /></a> <a href="#" onClick="mypop=window.open('serv-principal.php','','');mypop.focus();"><img src="imagenes/nv.png" alt="nueva ventana"></a></td>
</tr></table>
 <tr> 
 <td colspan="3"> 
 <form name="form2" method="post"> 
 <?php
    $vigusu=vigencia_usuarios($_SESSION[cedulausu]); 
   	$linkbd=conectar_bd(); 
 		if($_POST[oculto]=="")
		{
		$sqlr="Select * from servmedidores where codigo=$_GET[idproceso] ";
		$resp = mysql_query($sqlr,$linkbd);
	    while ($row =mysql_fetch_row($resp))
		{
		$_POST[codigo]=$row[0];
		$_POST[fabricante]=$row[2];					
		$_POST[descrip]=$row[1];
		$_POST[serial]=$row[4];
		$_POST[referencia]=$row[5];
		$_POST[diametro1]=$row[6];
		$_POST[diametro]=$row[6].'&#34;';
		$_POST[diametroo]=$row[6];
		$_POST[fechaac]=$row[7];
		$_POST[marca]=$row[3];	
		$_POST[estado]=$row[8];		
		}
		$sqlr="Select * from servmedidores_servicios where codigo=$_GET[idproceso] ";
		$resp = mysql_query($sqlr,$linkbd);
	    while ($row =mysql_fetch_row($resp))
		{
		$_POST[servicios][]=$row[1];
		}
		}
 
 ?>
   <table class="inicio" >
        <tr>
            <td class="titulos" colspan="6">Editar Medidor</td>
            <td class="cerrar" ><a href="serv-principal.php">Cerrar</a></td>
        </tr>
   		<tr>
			<td class="saludo1" style="width:9%">C&oacute;digo:</td>
           	<td style="width:17%"><input name="codigo" type="text" value="<?php echo $_POST[codigo]?>" style="width:38%" readonly></td>
           	<td class="saludo1" style="width:7%">Fabricante:</td>
           	<td ><input type="text" name="fabricante" value="<?php echo $_POST[fabricante]?>" style="width:85%"></td>
           	<td class="saludo1" style="width:7%">Descripci&oacute;n:</td>
           	<td ><input type="text" name="descrip" value="<?php echo $_POST[descrip]?>" style="width:75%"></td>
      	</tr>
        <tr>
           <td class="saludo1" style="width:9%">Serial:</td>
           <td style="width:17%"><input name="serial" type="text" value="<?php echo $_POST[serial]?>" style="width:75%"></td>
           <td class="saludo1" style="width:7%">Referencia:</td>
           <td ><input type="text" name="referencia" value="<?php echo $_POST[referencia]?>" style="width:85%"></td>
           <td class="saludo1" style="width:7%">Di&aacute;metro:</td>
           <td>
           		<input type="range" id="diametro1" name="diametro1" value="<?php echo $_POST[diametro1]?>" min="0.50" max="3" step="0.50" onChange="diametros();">
            	<input type="text" id="diametro" name="diametro" value="<?php echo $_POST[diametro]?>" style="width:10%" readonly>
				<input type="hidden" id="diametroo" name="diametroo" value="<?php echo $_POST[diametroo]?>" style="width:10%" >
           </td>
  </tr>
   		<tr>
       <td class="saludo1" style="width:9%">Fecha Activaci&oacute;n:</td>
       <td style="width:17%"><input name="fechaac"  id="fechaac"type="date" value="<?php echo $_POST[fechaac]?>" style="width:75%"></td>
       <td class="saludo1" style="width:7%">Marca:</td>
       <td ><input type="text" name="marca" value="<?php echo $_POST[marca]?>" style="width:85%"></td>
        <td class="saludo1" style="width:7%">Estado</td><td> <select name="estado" id="estado" onKeyUp="return tabular(event,this)" >
          <option value="S" <?php if($_POST[estado]=='S') echo "SELECTED"; ?>>Activo</option>
          <option value="N" <?php if($_POST[estado]=='N') echo "SELECTED"; ?>>Inactivo</option>
        </select><input type="hidden" name="oculto" value="1"></td>
  </tr>
  <tr><td class="saludo1">Servicios:</td>
	<?php
	echo "<td colspan='5'>";
	$co="saludo1";
	$co2="saludo2";
	$sqlr="select *from servservicios ";
	 //echo "s:$sqlr";
	 $res=mysql_query($sqlr,$linkbd);
	 while($row=mysql_fetch_row($res))
	  {		  
		$chks=""; 
		if(esta_en_array($_POST[servicios],$row[0]))
		{
		$chks=" checked";	
		}  
		echo "<span class='saludo3'> $row[0] $row[1]<input name='servicios[]' value='".$row[0]."' type='checkbox'  $chks></span> ";
		//$aux=$co;
		//$co=$co2;
		//$co2=$aux;
	  }
	  echo "</td></tr>";
	?>  
   </table>
 <?php  
 //********guardar
 if($_POST[oculto]=="2")
	{
	$sqlr="update  servmedidores set descripcion='$_POST[descrip]',fabricante='$_POST[fabricante]',marca='$_POST[marca]',serial='$_POST[serial]',ref='$_POST[referencia]',diametro='$_POST[diametroo]',fecha_activacion='$_POST[fechaac]',estado='$_POST[estado]' where codigo='$_POST[codigo]'";	
	if (!mysql_query($sqlr,$linkbd))
		{
		 echo "<script>alert('ERROR EN LA CREACION DEL ANEXO');document.form2.nombre.focus();</script>";
		 echo $sqlr;
		}
		  else
		  {
		   echo "<table class='inicio'><tr><td class='saludo1'><center>Se ha Actualizado el MEDIDOR con Exito <img src='imagenes\confirm.png'></center></td></tr></table>";
		   $sqlr="delete from servmedidores_servicios where codigo='$_POST[codigo]'";
		   mysql_query($sqlr,$linkbd);
 	  	   $tam=count($_POST[servicios]);						
	//echo "tam:".$tam;
		for($x=0;$x<$tam;$x++)
			 {
	//************** modificacion del presupuesto **************
			$sqlr="insert into servmedidores_servicios (codigo, servicio, estado) values ('$_POST[codigo]','".$_POST[servicios][$x]."','S')";	  
			if (!mysql_query($sqlr,$linkbd))
			{
			 echo "<table class='inicio'><tr><td class='saludo1'><center><font color=blue><img src='imagenes\alert.png'> Manejador de Errores de la Clase BD<br><font size=1></font></font><br><p align=center>No se pudo ejecutar la petición: <br><font color=red><b>$sqlr</b></font></p>";
//	 $e =mysql_error($respquery);
			 echo "Ocurrió el siguiente problema:<br>";
  	 //echo htmlentities($e['message']);
  	 		echo "<pre>";
     ///echo htmlentities($e['sqltext']);
    // printf("\n%".($e['offset']+1)."s", "^");
		     echo "</pre></center></td></tr></table>";
			 }
  			 else
  		 	{
		  echo "<table class='inicio'><tr><td class='saludo1'><center>Se ha Creado el Medidor - Servicio: ".$_POST[servicios][$x]."  con Exito <img src='imagenes\confirm.png'></center></td></tr></table>";		
			  }	 
	  //fin if 	   
			}	  	//**** fin del ciclo	
		  }
	}
 ?>
 </form>       
 </td>       
 </tr>       
	</table>
</body>
</html>