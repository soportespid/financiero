<?php
	header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=utf8");
	require"comun.inc";
	require"funciones.inc";
	session_start();
	$linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");
	cargarcodigopag($_GET['codpag'],$_SESSION["nivel"]);
	date_default_timezone_set("America/Bogota");
?>
<!DOCTYPE >
<html lang="es">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="ie=edge"/>
		<title>:: IDEAL 10 - Planeación Estratégica</title>
		<link href="css/css2.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css"/>
		<link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css"/>
		<link href="css/css4.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css"/>
		<script type="text/javascript" src="css/programas.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script type="text/javascript" src="css/calendario.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script>
			function excell(){
				document.form2.action = "plan-pqesbuscarexcel.php";
				document.form2.target = "_BLANK";
				document.form2.submit(); 
				document.form2.action = "";
				document.form2.target = "";
			}
			function callprogress(vValor){
				var randomColor = "#"+((1<<24)*Math.random()|0).toString(16); 
				document.getElementById("getprogress").innerHTML = vValor;
				document.getElementById("getProgressBarFill").innerHTML = '<div class="ProgressBarFill" style="background-color:'+randomColor+'; width: '+vValor+'%;"></div>';
				document.getElementById("titulog1").style.display = 'block';
				document.getElementById("progreso").style.display = 'block';
				document.getElementById("getProgressBarFill").style.display = 'block';
			}
			function ultimodiaMes(){
				let fecha1 = document.getElementById('fc_1198971545').value.split("/");
				let ultimodia = new Date(fecha1[2], fecha1[1], 0).getDate();
				document.getElementById('fc_1198971546').value = ultimodia + "/" + fecha1[1] + "/" + fecha1[2];
			}
			function buscar(){
				document.form2.oculto.value = "2";
				document.form2.submit();
			}
		</script>
		<?php titlepag();?>
	</head>
	<body>
		<IFRAME src="alertas.php" name="alertas" id="alertas" style="display:none"></IFRAME>
		<span id="todastablas2"></span>
		<table>
			<tr><script>barra_imagenes("plan");</script><?php cuadro_titulos();?></tr>	 
			<tr><?php menu_desplegable("plan");?></tr>
			<tr>
				<td colspan="3" class="cinta"><img src="imagenes/add2.png" class="mgbt1"/>
					<img src="imagenes/guardad.png" class="mgbt1"/>
					<img src="imagenes/busca.png" title="Buscar" onClick="document.form2.submit();" class="mgbt"/>
					<img src="imagenes/nv.png" title="Nueva Ventana" onClick="mypop=window.open('plan-principal.php','','');mypop.focus();" class="mgbt">
					<img src="imagenes/excel.png" title="Excel" onClick="excell()" class="mgbt">
				</td>
			</tr>
		</table>
		<form name="form2" method="post" action="">
			<?php
				if($_POST['oculto'] == ''){
					$_POST['fecha'] = date("01/m/Y");
					$fechafv = new DateTime();
					$fechafv->modify('last day of this month');
					$_POST['fecha2'] = $fechafv->format('d/m/Y');
				}
			?>
			<table class="inicio ancho">
				<tr >
					<td height="25"colspan="9" class="titulos" >:.Buscar PQRS</td>
					<td class="cerrar" style="width:7%" onClick="location.href='plan-principal.php'">&nbsp;Cerrar</td>
				</tr>
				<tr><td colspan="10" class="titulos2" >:&middot; Por Descripci&oacute;n </td></tr>
				<tr>
					<td class="tamano01" style="width:2.5cm">Dependencia:</td>
					<td colspan="3">
						<select name="dependencias" id="dependencias" onKeyUp="return tabular(event,this)" style="width:100%;text-transform:uppercase" class="tamano02">
							<option value="" <?php if($_POST['dependencias']=='') {echo "SELECTED"; $_POST['nomdep']="TODAS";}?>>- Todas</option>
							<?php
								$sqlr="SELECT * FROM planacareas WHERE estado='S'";
								$resp = mysqli_query($linkbd,$sqlr);
								while ($row =mysqli_fetch_row($resp)){
									if("$row[0]" == $_POST['dependencias']){
										echo "<option value='$row[0]' SELECTED>- $row[1]</option>";
										$_POST['nomdep']="Dependencia: $row[1]";
									} else {
										echo "<option value='$row[0]'>- $row[1]</option>";
									}
								}
							?>
						</select>
					</td>
				</tr>
				<tr>
					<td class="tamano01" style="width:3cm">:&middot; Tipo PQR:</td>
					<td class="tamano02" style="width:12%">
						<select name="tipopqr" id="tipopqr" class="tamano02" style="width:100%">
							<option onChange="" value="" >Seleccione....</option>
							<option value="P" <?php if($_POST['tipopqr']=="P"){echo "SELECTED ";}?>>P - Petici&oacute;n</option>
							<option value="Q" <?php if($_POST['tipopqr']=="Q"){echo "SELECTED ";}?>>Q - Queja</option>
							<option value="R" <?php if($_POST['tipopqr']=="R"){echo "SELECTED ";}?>>R - Reclamo</option>
							<option value="S" <?php if($_POST['tipopqr']=="S"){echo "SELECTED ";}?>>S - Sugerencia</option>
							<option value="D" <?php if($_POST['tipopqr']=="D"){echo "SELECTED ";}?>>D - Denuncia</option>
							<option value="F" <?php if($_POST['tipopqr']=="F"){echo "SELECTED ";}?>>F - Felicitaci&oacute;n</option>
						</select>
					</td>
					<td class="tamano01" style="width:3.5cm">:&middot;Medio Recepci&oacute;n:</td>
					<td style="width:14%">
						<select name="mdrece" id="mdrece" class="tamano02">
							<option onChange="" value="" >Seleccione....</option>
							<option value="O" <?php if($_POST['mdrece']=="O"){echo "SELECTED ";}?>>O - Oficio</option>
							<option value="F" <?php if($_POST['mdrece']=="F"){echo "SELECTED ";}?>>F - Formato PQRSDF</option>
							<option value="E" <?php if($_POST['mdrece']=="E"){echo "SELECTED ";}?>>E - Correo Electrónico</option>
							<option value="P" <?php if($_POST['mdrece']=="P"){echo "SELECTED ";}?>>P - Pagina WEB</option>
						</select>
					</td>
					<td class="tamano01" style="width:2.5cm;">Fecha Inicial:</td>
					<td style="width:10%;"><input type="text" name="fecha" value="<?php echo $_POST['fecha']?>" maxlength="10"  onKeyPress="javascript:return solonumeros(event)" onKeyUp="return tabular(event,this)"  id="fc_1198971545" onKeyDown="mascara(this,'/',patron,true)" style="width:100%;" title="DD/MM/YYYY" class="colordobleclik" autocomplete="off" onChange="ultimodiaMes();" onDblClick="displayCalendarFor('fc_1198971545');"/></td>
					<td class="tamano01" style="width:2.5cm;">Fecha Final:</td>
					<td style="width:10%;"><input type="text" name="fecha2" value="<?php echo $_POST['fecha2']?>" maxlength="10"  onKeyPress="javascript:return solonumeros(event)" onKeyUp="return tabular(event,this)"  id="fc_1198971546" onKeyDown="mascara(this,'/',patron,true)" style="width:100%;" title="DD/MM/YYYY" class="colordobleclik" autocomplete="off" onChange="" onDblClick="displayCalendarFor('fc_1198971546');"/></td>
					<td style="padding-bottom:5px"><em class="botonflecha" onClick="buscar()">Buscar</em></td>
				</tr>
			</table>
			<input type="hidden" name="nomdep" id="nomdep" value="<?php echo $_POST['nomdep'];?>"/>
			<input type="hidden" name="oculto" id="oculto" value="1">
			<div class="subpantallap" style="height:57%; width:99.6%; overflow-x:hidden;" id="divdet">
				<?php 
					if($_POST['fecha'] != '' && $_POST['fecha2'] != '' && $_POST['oculto'] == '2'){
						$crit1 = "";
						if ($_POST['tipopqr'] != ''){
							$crit1 = "AND T2.clasificacion = '".$_POST['tipopqr']."'";
						} else {
							$crit1 = '';
						}
						if ($_POST['mdrece'] != ''){
							$crit2 = "WHERE T1.mrecepcion='".$_POST['mdrece']."'";
						} else {
							$crit2 = "";
						}
						if ($_POST['fecha'] != ''){
							$fech1 = explode("/",$_POST['fecha']);
							$fech2 = explode("/",$_POST['fecha2']);
							$f1 = $fech1[2]."-".$fech1[1]."-".$fech1[0];
							$f2 = $fech2[2]."-".$fech2[1]."-".$fech2[0];
							if($crit2 != ''){
								$crit3 = "AND T1.fechar between '$f1' AND '$f2'";
							} else {
								$crit3 = "WHERE T1.fechar between '$f1' AND '$f2'";
							}
						}
						$sqlr = "SELECT T1.fechar, T2.clasificacion, T1.mrecepcion, T1.numeror, T1.idtercero, T1.descripcionr, T1.fechalimite FROM planacradicacion AS T1 INNER JOIN plantiporadicacion AS T2 ON T2.codigo = T1.tipor AND T2.clasificacion <> 'N' $crit1 $crit2 $crit3 ORDER BY T1.numeror";
						$resp = mysqli_query($linkbd,$sqlr);
						$totalcli = mysqli_affected_rows ($linkbd);
						$_POST['numtop'] = mysqli_num_rows($resp);
						$conta = 0;
						echo"
							<table class='inicio'>
							<tr><td class='titulos' colspan='20'>:: Lista de PQRS</td></tr>
							<tr>
								<td colspan='11' id='idtd01'></td>
								<td colspan='9'>
									<div id='titulog1' style='display:none; float:left'></div>
									<div id='progreso' class='ProgressBar' style='display:none; float:left'>
										<div class='ProgressBarText'><span id='getprogress'></span>&nbsp;% </div>
										<div id='getProgressBarFill'></div>
									</div>
								</td>
							</tr>
							<tr style='text-align:center'>
								<td class='titulos2' rowspan='2' style='width:2.5%'>No</td>
								<td class='titulos2' colspan='6'>clasificaci&oacuten</td>
								<td class='titulos2' colspan='4'>M. Recepci&oacuten</td>
								<td class='titulos2' colspan='5' >Tramite</td>
								<td class='titulos2' colspan='4' >Respuesta</td>
							</tr>
							<tr style='text-align:center'>
								<td class='titulos2' style='width:1.2%' title='Petici&oacute'>P</td>
								<td class='titulos2' style='width:1.2%' title='Queja'>Q</td>
								<td class='titulos2' style='width:1.2%' title='Reclamo'>R</td>
								<td class='titulos2' style='width:1.2%' title='Sugerencia'>S</td>
								<td class='titulos2' style='width:1.2%' title='Denuncia'>D</td>
								<td class='titulos2' style='width:1.2%' title='Felicitaci&oacuten'>F</td>
								<td class='titulos2' style='width:1.2%' title='Oficio'>O</td>
								<td class='titulos2' style='width:1.2%' title='Formato PQRS'>F</td>
								<td class='titulos2' style='width:1.2%' title='Correo Electr&oacute;nico'>E</td>
								<td class='titulos2' style='width:1.2%' title='Pagina WEB'>P</td>
								<td class='titulos2' style='width:3.7%' title='N&uacute;mero Radicaci&oacute;n'>No Rad</td>
								<td class='titulos2' style='width:5.3%' title='Fecha Radicaci&oacute;n'>Fecha Rad</td>
								<td class='titulos2' style='width:6%' title='Documento Solicitante'>Documento</td>
								<td class='titulos2' style='width:15%' title='Nombre Solicitante'>Nombre</td>
								<td class='titulos2' style='width:15%'>Asunto</td>
								<td class='titulos2' style='width:10%' title='Cargo Responsable'>Cargo</td>
								<td class='titulos2' title='Tratamiento Interno'>Tratamiento</td>
								<td class='titulos2' style='width:15%'>Respuesta</td>
								<td class='titulos2' style='width:5%' title='Fecha Respuesta'>Fecha</td>
							</tr>";
						$c=0;
						$iter='saludo1b';
						$iter2='saludo2b';
						while ($row = mysqli_fetch_row($resp)){
							if($_POST['dependencias'] != ''){
								$sqlt1 = "SELECT MAX(codigo) FROM planacresponsables WHERE codradicacion = '$row[3]'";
								$rest1 = mysqli_query($linkbd,$sqlt1);
								$rowt1 = mysqli_fetch_row($rest1);
								$sqlt2 = "SELECT TB1.estado FROM planacresponsables TB1, planaccargos TB2 WHERE TB1.codigo = '$rowt1[0]' AND TB2.codcargo = TB1.codcargo AND TB2.dependencia = '".$_POST['dependencias']."'";
								$rest2 = mysqli_query($linkbd,$sqlt2);
								$rowt2 = mysqli_fetch_row($rest2);
								if($rowt2[0] != ''){
									$bandera1 = 1;
								} else {
									$bandera1 = 0;
								}
							} else {
								$bandera1 = 1;
							}
							if($bandera1 == 1){
								$conta++;
								$nproceso = $docres = $respuesta = $fechares = '';
								$fecha = date('d-m-Y',strtotime($row[0]));
								$tipoclasificacion = '';
								$tiporecepcion = '';
								switch ($row[1]){
									case "P":	$calsifi = "
												<td title='Petici&oacute;' style='text-align:center'>X</td>
												<td title='Queja' style='text-align:center'>-</td>
												<td title='Reclamo' style='text-align:center'>-</td>
												<td title='Sugerencia' style='text-align:center'>-</td>
												<td title='Denuncia' style='text-align:center'>-</td>
												<td title='Felicitaci&oacute;n' style='text-align:center'>-</td>";
												//$tipoclasificacion = utf8_encode('Petición');
												//$tipoclasificacion = mb_convert_encoding('Petición', 'UTF-8', 'ISO-8859-1');
												$tipoclasificacion = 'Petición';
												break;
									case "Q":	$calsifi = "
												<td title='Petici&oacute;' style='text-align:center'>-</td>
												<td title='Queja' style='text-align:center'>X</td>
												<td title='Reclamo' style='text-align:center'>-</td>
												<td title='Sugerencia' style='text-align:center'>-</td>
												<td title='Denuncia' style='text-align:center'>-</td>
												<td title='Felicitaci&oacute;n' style='text-align:center'>-</td>";
												$tipoclasificacion = 'Queja';
												break;
									case "R":	$calsifi = "
												<td title='Petici&oacute;' style='text-align:center'>-</td>
												<td title='Queja' style='text-align:center'>-</td>
												<td title='Reclamo' style='text-align:center'>X</td>
												<td title='Sugerencia' style='text-align:center'>-</td>
												<td title='Denuncia' style='text-align:center'>-</td>
												<td title='Felicitaci&oacute;n' style='text-align:center'>-</td>";
												$tipoclasificacion = 'Reclamo';
												break;
									case "S":	$calsifi = "
												<td title='Petici&oacute;' style='text-align:center'>-</td>
												<td title='Queja' style='text-align:center'>-</td>
												<td title='Reclamo' style='text-align:center'>-</td>
												<td title='Sugerencia' style='text-align:center'>X</td>
												<td title='Denuncia' style='text-align:center'>-</td>
												<td title='Felicitaci&oacute;n' style='text-align:center'>-</td>";
												$tipoclasificacion = 'Sugerencia';
												break;
									case "D":	$calsifi = "
												<td title='Petici&oacute;' style='text-align:center'>-</td>
												<td title='Queja' style='text-align:center'>-</td>
												<td title='Reclamo' style='text-align:center'>-</td>
												<td title='Sugerencia' style='text-align:center'>-</td>
												<td title='Denuncia' style='text-align:center'>X</td>
												<td title='Felicitaci&oacute;n' style='text-align:center'>-</td>";
												$tipoclasificacion = 'Denuncia';
												break;
									case "F":	$calsifi = "
												<td title='Petici&oacute;' style='text-align:center'>-</td>
												<td title='Queja' style='text-align:center'>-</td>
												<td title='Reclamo' style='text-align:center'>-</td>
												<td title='Sugerencia' style='text-align:center'>-</td>
												<td title='Denuncia' style='text-align:center'>-</td>
												<td title='Felicitaci&oacute;n' style='text-align:center'>X</td>";
												//$tipoclasificacion = utf8_encode('Felicitaci�n');
												//$tipoclasificacion = mb_convert_encoding('Felicitación', 'UTF-8', 'ISO-8859-1');
												$tipoclasificacion = 'Felicitación';
												
								}
								switch ($row[2]){
									case "O":	$mrecep="
												<td title='Oficio' style='text-align:center'>X</td>
												<td title='Formato PQRS' style='text-align:center'>-</td>
												<td title='Correo Electr&oacute;nico' style='text-align:center'>-</td>
												<td title='Pagina WEB' style='text-align:center'>-</td>";
												$tiporecepcion = 'Oficio';
												break;
									case "F":	$mrecep="
												<td title='Oficio' style='text-align:center'>-</td>
												<td title='Formato PQRS' style='text-align:center'>X</td>
												<td title='Correo Electr&oacute;nico' style='text-align:center'>-</td>
												<td title='Pagina WEB' style='text-align:center'>-</td>";
												$tiporecepcion = 'Formato PQRS';
												break;
									case "E":	$mrecep="
												<td title='Oficio' style='text-align:center'>-</td>
												<td title='Formato PQRS' style='text-align:center'>-</td>
												<td title='Correo Electr&oacute;nico' style='text-align:center'>X</td>
												<td title='Pagina WEB' style='text-align:center'>-</td>";
												$tiporecepcion = 'Correo';
												break;
									case "P":	$mrecep="
												<td title='Oficio' style='text-align:center'>-</td>
												<td title='Formato PQRS' style='text-align:center'>-</td>
												<td title='Correo Electr&oacute;nico' style='text-align:center'>-</td>
												<td title='Pagina WEB' style='text-align:center'>X</td>";
												$tiporecepcion = 'Pagina WEB';
								}
								$numradi = substr($row[3],4);
								if($row[4] != ''){
									$nomsoli = buscatercero($row[4]);
									$nundocu = number_format($row[4],0,'','.');
								} else {
									$sqlter = "SELECT nombre FROM planradicacionsindoc WHERE numeror = '$row[3]'";
									$rester = mysqli_query($linkbd,$sqlter);
									$rowter = mysqli_fetch_row($rester);
									$nomsoli = $rowter[0];
									$nundocu = "Sin Documento";
								}
								$descrip = $row[5];
								$sql = "SELECT estado,fechares,usuariocon,respuesta,proceso FROM planacresponsables WHERE codradicacion = '$row[3]' AND fechares <> '' ORDER BY fechares DESC";
								$res = mysqli_query($linkbd,$sql);
								$numres = mysqli_num_rows($res);
								$connumres = 1;
								if($numres == 1){
									$rows = mysqli_fetch_row($res);
									$docres = $rows[2];
									$respuesta = $rows[3];
									$nproceso = $rows[4];
									if($respuesta != ''){
										$fechares = date('d-m-Y',strtotime($rows[1]));
									} else {
										$fechares="00-00-0000";
									}
								} else {
									while ($rows = mysqli_fetch_row($res)){
										if(($rows[0] == 'AN') || ($rows[0] == 'AC') || ($rows[0] == 'AR') || ($rows[0] == 'CS') || ($rows[0] == 'CN')){
											$rows = mysqli_fetch_row($res);
											$docres = $rows[2];
											$respuesta = $rows[3];
											if($respuesta!=''){
												$fechares = date('d-m-Y',strtotime($rows[1]));
											} else {
												$fechares = "00-00-0000";
											}
										}
										$connumres++;
										if(($docres == '') && ($connumres == $numres)){
											$docres = $rows[2];
											$respuesta = $rows[3];
											if($respuesta != ''){
												$fechares = date('d-m-Y',strtotime($rows[1]));
											}else {
												$fechares = "00-00-0000";
											}
										}
									}
								}
								if ($row[6] == null){
									$flimite = "00-00-0000";
								} else {
									$flimite = date('d-m-Y',strtotime($row[6]));
								}
								$sqlr1="SELECT T1.nombrecargo FROM planaccargos AS T1 INNER JOIN planestructura_terceros AS T2 ON T2.codcargo=T1.codcargo AND T2.cedulanit = '$docres'";
								$resp1 = mysqli_query($linkbd,$sqlr1);
								$row1 = mysqli_fetch_row($resp1);
								echo"
								<input type='hidden' name='tpclasifica[]' value='$tipoclasificacion'/>
								<input type='hidden' name='tprecep[]' value='$tiporecepcion'/>
								<input type='hidden' name='numradicado[]' value='$numradi'/>
								<input type='hidden' name='fecharadicado[]' value='$fecha'/>
								<input type='hidden' name='numdocumento[]' value='$nundocu'/>
								<input type='hidden' name='nomsolicitante[]' value='$nomsoli'/>
								<input type='hidden' name='descradicado[]' value='$descrip'/>
								<input type='hidden' name='nomcargo[]' value='$row1[0]'/>
								<input type='hidden' name='desctratamiento[]' value='$nproceso'/>
								<input type='hidden' name='descrespuesta[]' value='".str_replace("&lt;br/&gt;"," ",$respuesta)."'/>
								<input type='hidden' name='fecharespuesta[]' value='$fechares'/>
								<input type='hidden' name='fechalimite[]' value='$flimite'/>
								<tr class='$iter'>
									<td>$conta</td>
									$calsifi
									$mrecep
									<td style='text-align:right;'>$numradi&nbsp;</td>
									<td style='text-align:center'>$fecha</td>
									<td style='text-align:right;'>$nundocu</td>
									<td>$nomsoli</td>
									<td>$descrip</td>
									<td>$row1[0]</td>
									<td>$nproceso</td>
									<td>".str_replace("&lt;br/&gt;"," ",$respuesta)."</td>
									<td style='text-align:center'>$fechares</td>
								</tr>";
								$aux = $iter;
								$iter = $iter2;
								$iter2 = $aux;
							}
							echo"<script>document.getElementById('idtd01').innerHTML='Encontrados: $conta (".$_POST['numtop'].")'</script>";
							$c+=1;
							$porcentaje = $c * 100 / $totalcli;
							echo"<script>progres='".round($porcentaje)."';callprogress(progres);</script>"; 
							flush();
							ob_flush();
							usleep(5);
						}
						echo"</table>";
					}
				?>
			</div>
		</form>
	</body>
</html>