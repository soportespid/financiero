<?php

    header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=utf8");
	require 'comun.inc';
	require 'funciones.inc';
	session_start();
    if(empty($_SESSION)){
        header("location: index.php");
    }
	$linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");
	cargarcodigopag($_GET['codpag'],$_SESSION["nivel"]);
	date_default_timezone_set("America/Bogota");
?>
<!DOCTYPE >
<html lang=es>
	<head>
    <meta charset="utf-8"/>
		<meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1"/>
		<meta name="viewport" content="user-scalable=no">
		<title>:: IDEAL 10 - Servicios públicos </title>
		<link href="favicon.ico" rel="shortcut icon"/>
		<link href="css/css2z.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
		<link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
		<link href="css/css4.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
        <link href="css/tabs.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css"/>
        <link href="css/style.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css"/>
        <link rel="stylesheet" href="sweetalert2/dist/sweetalert2.min.css">
        <script type="text/javascript" src="css/programas.js"></script>
        <script src="sweetalert2/dist/sweetalert2.min.js"></script>
		<script src="vue/vue.min.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script src="css/programas.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script src="css/calendario.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
        <style>
			[v-cloak]{display : none;}
		</style>
	</head>
	<body>
		<header>
			<table>
				<tr><script>barra_imagenes("serv");</script><?php cuadro_titulos();?></tr>
			</table>
		</header>

        <section id="myapp" v-cloak>
            <input type="hidden" value = "1" ref="pageType">
            <div class="loading-container" v-show="isLoading" >
                <p class="text-loading"data-loading="IDEAL 10">IDEAL 10</p>
            </div>
            <nav>
                <table>
                    <tr><?php menu_desplegable("serv");?></tr>
                </table>
                <div class="bg-white group-btn p-1" id="newNavStyle">
                    <button type="button" class="btn btn-white btn-primary-hover d-flex justify-between align-items-center" @click="window.location.reload()">
                        <span>Nuevo</span>
                        <svg viewBox="0 -960 960 960" ><path d="M440-440H200v-80h240v-240h80v240h240v80H520v240h-80v-240Z"/></svg>
                    </button>
                    <button type="button" class="btn btn-white btn-primary-hover d-flex justify-between align-items-center" @click="save()">
                        <span>Guardar</span>
                        <svg viewBox="0 -960 960 960" ><path d="M840-680v480q0 33-23.5 56.5T760-120H200q-33 0-56.5-23.5T120-200v-560q0-33 23.5-56.5T200-840h480l160 160Zm-80 34L646-760H200v560h560v-446ZM480-240q50 0 85-35t35-85q0-50-35-85t-85-35q-50 0-85 35t-35 85q0 50 35 85t85 35ZM240-560h360v-160H240v160Zm-40-86v446-560 114Z"/></svg>
                    </button>
                    <button type="button" class="btn btn-white btn-primary-hover d-flex justify-between align-items-center" @click="location.href='serv-buscaReliquidaciones'">
                        <span>Buscar</span>
                        <svg class="fill-black group-hover:fill-white w-5 h-5" viewBox="0 -960 960 960" ><path d="M784-120 532-372q-30 24-69 38t-83 14q-109 0-184.5-75.5T120-580q0-109 75.5-184.5T380-840q109 0 184.5 75.5T640-580q0 44-14 83t-38 69l252 252-56 56ZM380-400q75 0 127.5-52.5T560-580q0-75-52.5-127.5T380-760q-75 0-127.5 52.5T200-580q0 75 52.5 127.5T380-400Z"/></svg>
                    </button>
                    <button type="button" class="btn btn-white btn-primary-hover d-flex justify-between align-items-center" @click="mypop=window.open('serv-principal.php','',''); mypop.focus();">
                        <span>Nueva ventana</span>
                        <svg class="fill-black group-hover:fill-white w-5 h-5" viewBox="0 -960 960 960" ><path d="M320-120v-80h80v-80H160q-33 0-56.5-23.5T80-360v-400q0-33 23.5-56.5T160-840h640q33 0 56.5 23.5T880-760v400q0 33-23.5 56.5T800-280H560v80h80v80H320ZM160-360h640v-400H160v400Zm0 0v-400 400Z"/></svg>
                    </button>
                </div>
            </nav>
            <article>
                <div class="bg-white">
                    <div>
                        <h2 class="titulos m-0">Rehacer liquidación de facturación</h2>
                        <p class="m-0 ms-2">Todos los campos con (<span class="text-danger fw-bolder">*</span>) son obligatorios</p>

                        <div class="d-flex">
                            <div class="form-control w-25">
                                <label class="form-label m-0" for="">Fecha<span class="text-danger fw-bolder">*</span></label>
                                <input type="date" v-model="fecha" class="text-center">
                            </div>

                            <div class="form-control w-25">
                                <label class="form-label m-0" for="">Usuario<span class="text-danger fw-bolder">*</span></label>
                                <input type="text" v-model="codUsuario" @dblclick="modalUsu=true;" class="colordobleclik" readonly>
                            </div>

                            <div class="form-control w-25">
                                <label class="form-label m-0" for="">Documento</label>
                                <input type="text" v-model="documento" class="text-center" readonly>
                            </div>

                            <div class="form-control justify-between">
                                <label class="form-label m-0" for=""></label>
                                <input type="text" v-model="nombre" readonly>
                            </div>

                            <div class="form-control w-25">
                                <label class="form-label m-0" for="">Estrato</label>
                                <input type="text" v-model="estrato" readonly>
                            </div>
                        </div>

                        <div class="d-flex">
                            <div class="form-control">
                                <label class="form-label m-0" for="">Motivo<span class="text-danger fw-bolder">*</span></label>
                                <textarea v-model="motivo" style="max-height: 50px; resize: vertical;"></textarea>
                            </div>
                        </div>

                        <div class="d-flex w-50">
                            <div class="form-control">
                                <label class="form-label m-0" for="">Lectura<span class="text-danger fw-bolder">*</span></label>
                                <input type="text" v-model="lectura" class="text-center">
                            </div>

                            <div class="form-control">
                                <label class="form-label m-0" for="">Consumo<span class="text-danger fw-bolder">*</span></label>
                                <input type="text" v-model="consumo" class="text-center">
                            </div>

                            <div class="form-control">
                                <label class="form-label m-0" for="">Novedad<span class="text-danger fw-bolder">*</span></label>
                                <input type="text" v-model="novedad" @dblclick="modalNovedades=true;" class="colordobleclik">
                            </div>

                            <div class="form-control">
                                <label class="form-label m-0" for="">Factura</label>
                                <input type="text" v-model="factura" class="text-center" readonly>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- modales -->
                <div v-show="modalUsu" class="modal">
                    <div class="modal-dialog modal-lg">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title">Listado de usuarios</h5>
                                <button type="button" @click="modalUsu=false;" class="btn btn-close"><div></div><div></div></button>
                            </div>
                            <div class="modal-body">
                                <div class="d-flex flex-column">
                                    <div class="form-control m-0 mb-3">
                                        <input type="search" v-model="txtSearch" @keyup="searchData('modalUsuario')" placeholder="Busca código de usuario, documento o nombre">
                                    </div>
                                </div>
                                <div class="overflow-auto max-vh-50 overflow-x-hidden" >
                                    <table class="table table-hover fw-normal">
                                        <thead>
                                            <tr>
                                                <th class="text-center">Código de usuario</th>
                                                <th class="text-center">Documento</th>
                                                <th>Nombre</th>
                                                <th class="text-center">Número factura</th>
                                                <th>Estrato</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr v-for="(usu,index) in usuarios" :key="index" @click="selectItem('usuario', usu)">
                                                <td class="text-center">{{usu.cod_usuario}}</td>
                                                <td class="text-center">{{usu.cedulanit}}</td>
                                                <td>{{usu.view_nombre}}</td>
                                                <td class="text-center">{{usu.numero_facturacion}}</td>
                                                <td>{{usu.nombre_estrato}}</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div v-show="modalNovedades" class="modal">
                    <div class="modal-dialog modal-sm">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title">Listado de novedades</h5>
                                <button type="button" @click="modalNovedades=false;" class="btn btn-close"><div></div><div></div></button>
                            </div>
                            <div class="modal-body">
                                <div class="d-flex flex-column">
                                    <div class="form-control m-0 mb-3">
                                        <input type="search" v-model="txtSearch" @keyup="searchData('modalNovedad')" placeholder="Busca código o nombre">
                                    </div>
                                </div>
                                <div class="overflow-auto max-vh-50 overflow-x-hidden" >
                                    <table class="table table-hover fw-normal">
                                        <thead>
                                            <tr>
                                                <th class="text-center">Código</th>
                                                <th>Nombre</th>
                                                <th class="text-center">Afecta</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr v-for="(n,index) in novedades" :key="index" @click="selectItem('novedad', n)">
                                                <td class="text-center">{{n.codigo}}</td>
                                                <td>{{n.nombre}}</td>
                                                <td class="text-center">{{n.afecta}}</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </article>
        </section>

        <script src="Librerias/vue/vue.min.js"></script>
		<script src="Librerias/vue/axios.min.js"></script>
		<script type="module" src="servicios_publicos/rehacer_liquidacion/js/rehacerLiquidacion_functions.js"></script>
	</body>
</html>
