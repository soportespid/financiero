<?php
    header("Cache-control: no-cache, no-store, must-revalidate");
    header("Content-Type: text/html;charset=uft8");
    require "comun.inc";
    require "funciones.inc";
    require 'funcionesSP.inc.php';
    session_start();
    $linkbd = conectar_v7();
    $linkbd -> set_charset("utf8");
    cargarcodigopag($_GET['codpag'],$_SESSION["nivel"]);
    date_default_timezone_set("America/Bogota");
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>:: IDEAL 10 - Parametrización</title>
    <link href="css/css2n.css" rel="stylesheet" type="text/css"/>
    <link href="css/css3n.css" rel="stylesheet" type="text/css"/>
    <link href="css/css4.css" rel="stylesheet" type="text/css"/>
    <link href="css/cssSP.css" rel="stylesheet" type="text/css"/>
    <script type="text/javascript" src="css/programas.js"></script>
    <script type="text/javascript" src="css/calendario.js"></script>
    <?php titlepag();?>

    <script>
        function despliegamodal2(_valor)
        {
            document.getElementById("bgventanamodal2").style.visibility=_valor;
            if(_valor=="hidden"){document.getElementById('ventana2').src="";}
            else
            {document.getElementById('ventana2').src="aesgpriet-ventana.php";}
        }


			function despliegamodalm(_valor,_tip,mensa,pregunta)
			{
				document.getElementById("bgventanamodalm").style.visibility=_valor;

				if(_valor=="hidden")
				{
					document.getElementById('ventanam').src="";
				}
				else
				{
					switch(_tip)
					{
						case "1":
							document.getElementById('ventanam').src="ventana-mensaje1.php?titulos="+mensa;
						    break;

						case "2":
							document.getElementById('ventanam').src="ventana-mensaje3.php?titulos="+mensa;
						    break;

						case "3":
							document.getElementById('ventanam').src="ventana-mensaje2.php?titulos="+mensa;
						    break;

						case "4":
							document.getElementById('ventanam').src="ventana-consulta1.php?titulos="+mensa+"&idresp="+pregunta;
						    break;
					}
				}
			}

			//ventanas servicios publicos
            function respuestaModalBusqueda2()
			{

            }

			function funcionmensaje() {
                document.location.href = "ccpet-crearSeccionPresupuestal.php";
			}

			function respuestaconsulta(pregunta)
			{
				switch(pregunta)
				{
					case "1":
						document.form2.oculto.value='2';
						document.form2.submit();
					break;
				}
			}

			function guardar() {
                var nombre = document.getElementById('nombre').value;
                var unidadEjecutora = document.getElementById('unidadEjecutora').value;

				if (nombre.trim() != '' && unidadEjecutora != '-1') {
					despliegamodalm('visible','4','Esta Seguro de Guardar','1');
                }
				else {

                    despliegamodalm('visible','2','Falta información por completar');
                }
			}

			function actualizar()
			{
				document.form2.submit();
			}

            function validacodigo() {

                var codigo = document.getElementById('codigo').value;

                if (codigo.trim() != '') {
                    document.form2.oculto.value='3';
				    document.form2.submit();
                }
            }
    </script>
</head>
<body>
    <IFRAME src="alertas.php" name="alertas" id="alertas" style="display:none"></IFRAME>
    <span id="todastablas2"></span>
    <table>
        <tr><script>barra_imagenes("para");</script><?php cuadro_titulos();?></tr>

        <tr><?php menu_desplegable("para");?></tr>

        <tr>
            <td colspan="3" class="cinta">
                <a href="ccpet-crearSeccionPresupuestal.php" class="mgbt"><img src="imagenes/add.png"/></a>

                <a onclick="guardar()" class="mgbt"><img src="imagenes/guarda.png" title="Guardar"/></a>

                <a href="ccpet-buscaSeccionPresupuestal.php" class="mgbt"><img src="imagenes/busca.png" title="Buscar"/></a>

                <a onclick="mypop=window.open('plan-agenda.php','','');mypop.focus()" class="mgbt"><img src="imagenes/agenda1.png" title="Agenda" /></a>

                <a onclick="mypop=window.open('para-principal.php','','');mypop.focus();" class="mgbt"><img src="imagenes/nv.png" title="Nueva ventana"></a>

                <a onclick="mypop=window.open('<?php echo $url2; ?>','','');mypop.focus();" class="mgbt"><img src="imagenes/duplicar_pantalla.png" title="Duplicar pesta&ntilde;a"></a>

                <a href="" class="mgbt"><img src="imagenes/iratras.png" title="Atr&aacute;s"></a>
            </td>
        </tr>
    </table>

    <div id="bgventanamodalm" class="bgventanamodalm">
        <div id="ventanamodalm" class="ventanamodalm">
            <IFRAME src="" name="ventanam" marginWidth=0 marginHeight=0 frameBorder=0 id="ventanam" frameSpacing=0 style=" width:700px; height:130px; top:200; overflow:hidden;"></IFRAME>
        </div>
    </div>

    <form name="form2" method="post" action="">
        <?php
            if(@$_POST['oculto'] == "")
            {
                $_POST['onoffswitch'] = 'S';
            }

            if (@$_POST['oculto'] == 3) {

                $query = "SELECT id_seccion_presupuestal FROM pptoseccion_presupuestal WHERE id_seccion_presupuestal = '$_POST[codigo]'";
                $resp = mysqli_query($linkbd,$query);
                $row = mysqli_fetch_row($resp);

                if ($row[0] != '') {
                    echo"<script>despliegamodalm('visible','2','Codigo duplicado');</script>";
                    $_POST['codigo'] = '';
                }
            }
        ?>
        <div>
            <table class="inicio">
                <tr>
                    <td class="titulos" colspan="6">.: Crear Secci&oacute;n Presupuestal</td>

                    <td class="cerrar" style="width:7%" onclick="location.href='ccpet-principal.php'">Cerrar</td>
                </tr>

                <tr>
                    <td class="tamano01" style="width:3cm; text-align:center;">Codigo:</td>
                    <td style="width: 15%;">
                        <input type="text" name="codigo" id="codigo" value="<?php echo $_POST['codigo'] ?>" style="width:92%; text-align:center;" onblur="validacodigo();">
                    </td>

                    <td class="tamano01" style="width:3cm; text-align:center;">Nombre:</td>
                    <td colspan="4">
                        <input type="text" name="nombre" id="nombre" value="<?php echo $_POST['nombre'] ?>" style="width:98%;">
                    </td>
                </tr>

                <tr>
                    <td class="tamano01" style="width:3cm; text-align:center;">Unidad Ejecutora:</td>
                    <td style="width: 15%">
						<select name="unidadEjecutora" id="unidadEjecutora" class="centrarSelect" style="width:100%;">
                            <option value="-1">SELECCIONE UNIDAD EJECUTORA</option>
							<?php
                                $query = "SELECT id_cc, nombre FROM pptouniejecu WHERE estado = 'S'";
                                $resp = mysqli_query($linkbd,$query);
                                while ($row = mysqli_fetch_row($resp)) {

                                    if(@$_POST['unidadEjecutora'] == $row[0]) {
                                        echo "<option value='$row[0]' SELECTED>$row[0] - $row[1]</option>";
                                    }
                                    else {
                                        echo "<option value='$row[0]'>$row[0] - $row[1]</option>";
                                    }
                                }
							?>
						</select>
					</td>

                    <td class="tamano01" style="width:15%;">AESGPRI ET: </td>
					<td  style="width: 15%">
                        <input type="text" id="aesgpriet" name="aesgpriet" class="tamano02" onKeyUp="return tabular(event,this)" value="<?php echo @ $_POST['aesgpriet']?>" style="background-color:#40f3ff;width:98%;height:35px!important;" title="Doble Click Listado Cuentas" onDblClick="despliegamodal2('visible');"/>

                    </td>
					<td>
                        <input type="text" name="nAesgpriet" id="nAesgpriet" value="<?php echo @ $_POST['nAesgpriet'];?>" style="width:98%;height:35px!important;" class="tamano02" readonly/>
                    </td>

                    <td class="tamano01" style='text-align:center;'>Estado:</td>

					<td>
						<div class="onoffswitch">
							<input type="checkbox" name="onoffswitch" class="onoffswitch-checkbox" id="myonoffswitch" value="<?php echo @ $_POST['onoffswitch'];?>" <?php if(@ $_POST['onoffswitch']=='S'){echo "checked";}?> onChange="cambiocheck();"/>
							<label class="onoffswitch-label" for="myonoffswitch">
								<span class="onoffswitch-inner"></span>
								<span class="onoffswitch-switch"></span>
							</label>
						</div>
					</td>


                </tr>
            </table>

            <input type="hidden" name="oculto" id="oculto" value="1"/>
        </div>
        <?php
            if(@$_POST['oculto'] == "2")
            {
                $query = "INSERT INTO pptoseccion_presupuestal (id_seccion_presupuestal, id_unidad_ejecutora, nombre, estado, aesgpriet) VALUES ('$_POST[codigo]', '$_POST[unidadEjecutora]', '$_POST[nombre]', '$_POST[onoffswitch]', '$_POST[aesgpriet]')";

                if (mysqli_query($linkbd,$query)) {
                    echo "<script>despliegamodalm('visible','1','Se ha guardado con exito');</script>";
                }
                else {
                    echo"<script>despliegamodalm('visible','2','No se pudo guardar con exito');</script>";
                }
            }
        ?>
    </form>

    <div id="bgventanamodal2">
        <div id="ventanamodal2">
            <IFRAME  src="" name="buster" marginWidth=0 marginHeight=0 frameBorder=0 id="ventana2" frameSpacing=0 style="left:500px; width:900px; height:500px; top:200;"></IFRAME>
        </div>
    </div>
</body>
</html>
