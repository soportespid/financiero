<?php
	header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=utf8");
	require 'comun.inc';
	require 'funciones.inc';
	session_start();
	$linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");
	cargarcodigopag($_GET['codpag'],$_SESSION["nivel"]);
	date_default_timezone_set("America/Bogota");
?>
<!DOCTYPE >
<html lang=es>
	<head>
    <meta charset="utf-8"/>
		<meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1"/>
		<meta name="viewport" content="user-scalable=no">
		<title>:: IDEAL 10 - Servicios públicos</title>
		<link href="favicon.ico" rel="shortcut icon"/>
		<link href="css/css2z.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
		<link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
		<link href="css/css4.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
        <link href="css/tabs.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css"/>
        <link rel="stylesheet" href="sweetalert2/dist/sweetalert2.min.css">
        <script type="text/javascript" src="css/programas.js"></script>
        <script src="sweetalert2/dist/sweetalert2.min.js"></script>
		<script src="vue/vue.min.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script src="css/programas.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script src="css/calendario.js?<?php echo date('d_m_Y_h_i_s');?>"></script>

        <style>
            .checkbox-wrapper-31:hover .check {
                stroke-dashoffset: 0;
            }
        
            .checkbox-wrapper-31 {
                position: relative;
                display: inline-block;
                width: 40px;
                height: 40px;
            }
            .checkbox-wrapper-31 .background {
                fill: #ccc;
                transition: ease all 0.6s;
                -webkit-transition: ease all 0.6s;
            }
            .checkbox-wrapper-31 .stroke {
                fill: none;
                stroke: #fff;
                stroke-miterlimit: 10;
                stroke-width: 2px;
                stroke-dashoffset: 100;
                stroke-dasharray: 100;
                transition: ease all 0.6s;
                -webkit-transition: ease all 0.6s;
            }
            .checkbox-wrapper-31 .check {
                fill: none;
                stroke: #fff;
                stroke-linecap: round;
                stroke-linejoin: round;
                stroke-width: 2px;
                stroke-dashoffset: 22;
                stroke-dasharray: 22;
                transition: ease all 0.6s;
                -webkit-transition: ease all 0.6s;
            }
            .checkbox-wrapper-31 input[type=checkbox] {
                position: absolute;
                width: 100%;
                height: 100%;
                left: 0;
                top: 0;
                margin: 0;
                opacity: 0;
                -appearance: none;
            }
            .checkbox-wrapper-31 input[type=checkbox]:hover {
                cursor: pointer;
            }
            .checkbox-wrapper-31 input[type=checkbox]:checked + svg .background {
                fill: #6cbe45;
            }
            .checkbox-wrapper-31 input[type=checkbox]:checked + svg .stroke {
                stroke-dashoffset: 0;
            }
            .checkbox-wrapper-31 input[type=checkbox]:checked + svg .check {
                stroke-dashoffset: 0;
            }
        </style>
	</head>
	<body>
		<header>
			<table>
				<tr><script>barra_imagenes("serv");</script><?php cuadro_titulos();?></tr>
			</table>
		</header>

		<form name="form2" method="post" action="">
			<section id="myapp" v-cloak >
				<nav>
					<table>
						<tr><?php menu_desplegable("serv");?></tr>
						<tr>
							<td colspan="3" class="cinta">
								<img src="imagenes/add.png" v-on:click="location.href='serv-recaudoFactura'" class="mgbt" title="Nuevo">
								<img src="imagenes/guarda.png" @click="guardar" title="Guardar"  class="mgbt">
								<img src="imagenes/busca.png" v-on:click="location.href='serv-buscarRecaudoFactura'" class="mgbt" title="Buscar">
								<img src="imagenes/nv.png" onClick="mypop=window.open('serv-principal','',''); mypop.focus();" class="mgbt" title="Nueva Ventana">
							</td>
						</tr>
					</table>
				</nav>

				<article>
					<div>
						<table class="inicio">
							<tr>
								<td class="titulos" colspan="8">.: Movimiento de recaudo:</td>
                            	<td class="cerrar" style="width:4%" onClick="location.href='serv-recaudoFactura'">Cerrar</td>
							</tr>

                            <tr>
								<td class="textonew01" style="width:3.5cm;">.: Tipo movimiento:</td>
                            	<td>
                                    <select style="width: 25%; font-size: 10px; margin-top:4px" v-model="movimiento">
                                        <option v-bind:value="1">Recaudo</option>
                                        <option v-bind:value="3">Anular recaudo</option>
                                    </select>
                                </td>
							</tr>
						</table>
					</div>

                    <div v-if="movimiento == 1">
                        <table class="inicio">
                            <tr>
                                <td class="titulos" colspan="8">.: Detalle de recaudo</td>
                            </tr>
                            
                            <tr>
                                <td class="textonew01" style="width:10%;">.: Consecutivo recaudo:</td>
                                <td style="width: 11%;">
                                    <input type="text" v-model="consecutivo" style="text-align: center;" readonly>
                                </td>

                                <td class="textonew01" style="width:10%;">.: Fecha:</td>
                                <td style="width: 11%;">
                                    <input type="text" name="fechaRecaudo"  value="<?php echo $_POST['fechaRecaudo']?>" onKeyUp="return tabular(event,this)" id="fechaRecaudo" title="DD/MM/YYYY" onDblClick="displayCalendarFor('fechaRecaudo');" class="colordobleclik" autocomplete="off" onChange=""  readonly>
                                </td>

                                <td class="textonew01" style="width:10%;">.: Número de factura</td>
                                <td>
                                    <input type="number" v-model="factura" @change="buscaFactura" v-on:keyup.enter="buscaFactura" style="text-align:center;">
                                </td>
                            </tr>

                            <tr>
                                <td class="textonew01" style="width:10%;">.: Codigo de usuario:</td>
                                <td style="width: 11%;">
                                    <input type="text" v-model="codUsuario" style="text-align:center;" readonly>
                                </td>

                                <td class="textonew01" style="width:10%;">.: Documento tercero:</td>
                                <td style="width: 11%;">
                                    <input type="text" v-model="documento" style="text-align:center;" readonly>
                                </td>

                                <td class="textonew01" style="width:10%;">.: Nombre tercero:</td>
                                <td colspan="3">
                                    <input type="text" v-model="tercero" style="width: 95%;" readonly>
                                </td>
                            </tr>

                            <tr>
                                <td class="textonew01" style="width:10%;">.: Valor factura:</td>
                                <td style="width: 11%;">
                                    <input type="text" v-model="formatonumero(valorFactura)" style="text-align: center;" readonly>
                                </td>

                                <td class="textonew01" style="width:10%;">.: Descuento de intereses:</td>
                                <td style="text-align:center;">    
                                    <div class="checkbox-wrapper-31">
                                        <input type="checkbox" v-model="descuentoIntereses" @change="cambioIntereses"/>
                                        <svg viewBox="0 0 35.6 35.6">
                                            <circle class="background" cx="17.8" cy="17.8" r="17.8"></circle>
                                            <circle class="stroke" cx="17.8" cy="17.8" r="14.37"></circle>
                                            <polyline class="check" points="11.78 18.12 15.55 22.23 25.17 12.87"></polyline>
                                        </svg>
                                    </div>        
                                </td>
                                
                                <td class="textonew01" style="width:10%;">.: Porcentaje de descuento:</td>
                                <td style="width: 11%;" v-if="showPorcentaje == true">
                                    <select v-model="porcentaje" @change="calcularDescuento" style="width: 100%; text-align:center;">
                                        <option value="">Seleccione porcentaje</option>
                                        <option v-bind:value="10">10%</option>
                                        <option v-bind:value="20">20%</option>
                                        <option v-bind:value="30">30%</option>
                                        <option v-bind:value="40">40%</option>
                                        <option v-bind:value="50">50%</option>
                                        <option v-bind:value="60">60%</option>
                                        <option v-bind:value="70">70%</option>
                                        <option v-bind:value="80">80%</option>
                                        <option v-bind:value="90">90%</option>
                                        <option v-bind:value="100">100%</option>
                                    </select>
                                </td>
                                <td style="width:11%;" v-if="showPorcentaje == false">
                                    <select v-model="porcentaje" style="width: 100%; text-align:center;" disabled>
                                        <option value="">Seleccione porcentaje</option>
                                    </select>
                                </td>
            
                                <td class="textonew01" style="width:5%;">.: Valor pago:</td>
                                <td style="width: 11%;">
                                    <input type="text" v-model="formatonumero(valorPago)" style="text-align: center;" readonly>
                                </td>
                            </tr>

                            <tr>
                                <td class="textonew01" style="width:10%;">.: Medio de pago:</td>
                                <td style="width: 11%;">
                                    <select v-model="medioPago" @change="cambioBanco" style="width: 100%; text-align:center;">
                                        <option value="">Seleccione medio de recaudo</option>
                                        <option value="caja">Caja</option>
                                        <option value="banco">Banco</option>
                                    </select>
                                </td>

                                <td class="textonew01" style="width:10%;">.: Banco:</td>
                                <td v-if="showBanco==true" style="width: 11%;">
                                    <select v-model="banco" style="width: 100%; text-align:center;">
                                        <option value="">Seleccione medio de recaudo</option>
                                        <option v-for="banco in bancos" v-bind:value="banco[0]">
                                            {{ banco[2] }} - {{ banco[3] }} - {{ banco[4] }}
                                        </option>
                                    </select>
                                </td>

                                <td v-if="showBanco==false" style="width: 11%;">
                                    <select style="width: 100%; text-align:center;" disabled>
                                        <option value="">Seleccione medio de recaudo</option>
                                    </select>
                                </td>

                                <td class="textonew01" style="width:10%;">.: Concepto:</td>
                                <td colspan="3">
                                    <input type="text" v-model="concepto" style="width: 95%;">
                                </td>
                            </tr>

                        </table>
                    </div>

                    <div v-if="movimiento == 3">
                        <table class="inicio">
                            <tr>
                                <td class="titulos" colspan="8">.: Anular recaudo</td>
                            </tr>

                            <tr>
                                <td class="textonew01" style="width:10%;">.: Consecutivo recaudo:</td>
                                <td style="width: 11%;">
                                    <input type="text" v-model="consecRev" v-on:keyup.enter="buscaRecaudoRev" style="text-align: center;">
                                </td>

                                <td class="textonew01" style="width:10%;">.: Código de usuario</td>
                                <td style="width: 11%;">
                                    <input type="text" v-model="codUsuRev" style="text-align: center;" readonly>
                                </td>                                

                                <td class="textonew01" style="width:10%;">.: Numero de factura</td>
                                <td style="width: 11%;">
                                    <input type="text" v-model="numFacturaRev" style="text-align: center;" readonly>
                                </td>                                

                                <td class="textonew01" style="width:10%;">.: Valor pago</td>
                                <td style="width: 11%;">
                                    <input type="text" v-model="formatonumero(valorPagoRev)" style="text-align: center;" readonly>
                                </td>                                
                            </tr>
                        </table>
                    </div>
				</article>
			</section>
		</form>

		<script src="node_modules/read-excel-file/bundle/read-excel-file.min.js"></script>
		<script src="node_modules/xlsx/dist/xlsx.full.min.js"></script>
        <script src="Librerias/vue/vue.min.js"></script>
		<script src="Librerias/vue/axios.min.js"></script>
		<script type="module" src="servicios_publicos/recaudoFactura/crear/serv-recaudoFactura.js"></script>
        
	</body>
</html>