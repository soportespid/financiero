<?php
	header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=utf8");
	require "comun.inc";
	require "funciones.inc";
	session_start();
	$linkbd=conectar_v7();
	cargarcodigopag(@$_GET['codpag'],$_SESSION["nivel"]);
	date_default_timezone_set("America/Bogota");
?>
<!DOCTYPE >
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="es">
	<head>
		<meta http-equiv="Content-Type" content="text/html" charset="utf8"/>
		<meta http-equiv="X-UA-Compatible" content="IE=9"/>
		<title>:: IDEAL 10 - Tesorer&iacute;a</title>
		<link href="css/css2.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<link href="css/css4.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<script type="text/javascript" src="css/programas.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script type="text/javascript" src="css/calendario.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script type="text/javascript" src="jquery-1.11.0.min.js"></script>
		<style>
			input[type='text']{height:30px;}
			input[type='search']{height:30px;}
			select{height:30px;}
		</style>
		<script>
			function eliminar(idr)
			{
				if (confirm("Esta Seguro de Eliminar el Recibo de Caja"))
				{
				document.form2.oculto.value=2;
				document.form2.var1.value=idr;
				document.form2.submit();
				}
			}
			function archivocsv()
			{
				document.form2.action= "archivos/<?php echo $_SESSION['usuario'];?>-reporterecaudos.csv";
				document.form2.target="_BLANK";
				document.form2.submit();
				document.form2.action="";
				document.form2.target="";
			}
			function crearexcel()
			{
				document.form2.action="teso-buscarecaudosexcel.php";
				document.form2.target="_BLANK";
				document.form2.submit();
				document.form2.action="";
				document.form2.target="";
			}
			function verUltimaPos(idcta, fila, filtro1, filtro2, filtro3, filtro4)
			{
				var scrtop=$('#divdet').scrollTop();
				var altura=$('#divdet').height();
				var numpag=$('#nummul').val();
				var limreg=$('#numres').val();
				if((numpag<=0)||(numpag=="")){numpag=0;}
				if((limreg==0)||(limreg=="")){limreg=10;}
				numpag++;
				location.href="teso-editarecaudos.php?idrecaudo="+idcta+"&scrtop="+scrtop+"&totreg="+fila+"&altura="+altura+"&numpag="+numpag+"&limreg="+limreg+"&filtro1="+filtro1+"&filtro2="+filtro2+"&filtro3="+filtro3+"&filtro4="+filtro4;
			}
			function generabusqueda()
			{
				document.form2.oculto.value=10;
				document.form2.submit();
			}
			function buscarbotonfiltro()
            {
                if((document.form2.fechaini.value != "" && document.form2.fechafin.value == "") || (document.form2.fechaini.value == "" && document.form2.fechafin.value != "")){
                    alert("Falta digitar fecha");
                }else{
                    document.getElementById('numpos').value=0;
                    document.getElementById('nummul').value=0;
                    document.form2.submit();
                }
            }
			function cambionumero()
			{
				document.getElementById('numres').value=document.getElementById('renumres').value;
				document.getElementById('nummul').value=0;
				document.getElementById('numpos').value=0;
				document.getElementById('oculto').value=1;
				document.form2.submit();
			}
			function saltopag(pag)
			{
				document.getElementById('nummul').value=parseInt(pag)-1;
				document.getElementById('numpos').value=parseInt(document.getElementById('numres').value)*parseInt(document.getElementById('nummul').value);
				document.getElementById('oculto').value=1;
				document.form2.submit();
			}
			function numsig()
			{
				document.getElementById('nummul').value=parseInt(document.getElementById('nummul').value)+1;
				document.getElementById('numpos').value=parseInt(document.getElementById('numres').value)*parseInt(document.getElementById('nummul').value);
				document.getElementById('oculto').value=1;
				document.form2.submit();
			}
			function numant()
			{
				document.getElementById('nummul').value=parseInt(document.getElementById('nummul').value)-1;
				document.getElementById('numpos').value=parseInt(document.getElementById('numres').value)*parseInt(document.getElementById('nummul').value);
				document.getElementById('oculto').value=1;
				document.form2.submit();
			}
		</script>
		<?php titlepag();?>
	</head>
	<body>
		<IFRAME src="alertas.php" name="alertas" id="alertas" style="display:none"></IFRAME>
		<span id="todastablas2"></span>
		<table>
			<tr><script>barra_imagenes("teso");</script><?php cuadro_titulos();?></tr>
			<tr><?php menu_desplegable("teso");?></tr>
			<tr>
				<td colspan="3" class="cinta">
					<a onClick="location.href='teso-recaudos.php'" class="mgbt"><img src="imagenes/add.png" title="Nuevo"/></a>
					<a class="mgbt1"><img src="imagenes/guardad.png"/></a>
					<a class="mgbt1"><img src="imagenes/buscad.png"/></a>
					<a onClick="mypop=window.open('plan-agenda.php','','');mypop.focus()" class="mgbt"><img src="imagenes/agenda1.png" title="Agenda" /></a>
					<a onClick="<?php echo paginasnuevas("teso");?>" class="mgbt"><img src="imagenes/nv.png" title="Nueva ventana"/></a>
					<a onClick="mypop=window.open('<?php echo $url2; ?>','','');mypop.focus();" class="tooltip bottom mgbt"><img src="imagenes/duplicar_pantalla.png" title="Duplicar pesta&ntilde;a"></a>
					<a onclick="crearexcel()" class="mgbt"><img src="imagenes/excel.png" title="Excel"/></a>
				</td>
			</tr>
		</table>
		<div id="bgventanamodalm" class="bgventanamodalm">
			<div id="ventanamodalm" class="ventanamodalm">
				<IFRAME src="" name="ventanam" marginWidth=0 marginHeight=0 frameBorder=0 id="ventanam" frameSpacing=0 style=" width:700px; height:130px; top:200; overflow:hidden;">
				</IFRAME>
			</div>
		</div>
		<form name="form2" method="post" action="teso-buscarecaudos.php">
			<?php
				if($_POST['bandera'] == "")
                {
                    if(isset($_GET['filtro1']))
                    {
                        $_POST['nLiquid'] = $_GET['filtro1'];
                    }

                    if(isset($_GET['filtro2']))
                    {
                        $_POST['conLiquid'] = $_GET['filtro2'];
                    }

                    if(isset($_GET['filtro3']))
                    {
                        $_POST['fechaini'] = $_GET['filtro3'];
                    }

                    if(isset($_GET['filtro4']))
                    {
                        $_POST['fechafin']=$_GET['filtro4'];
                    }

                    $_POST['bandera'] = 1;
                    echo "<script>document.form2.bandera.value = 1;
                    document.form2.submit();</script>";

					$scrtop=$_GET['scrtop'];

					if($scrtop=="") $scrtop=0;
					echo"<script>
						window.onload=function(){
							$('#divdet').scrollTop(".$scrtop.")
						}
					</script>";
					$gidcta=$_GET['idcta'];
                }
			?>
			<div class="loading" id="divcarga"><span>Cargando...</span></div>
			<?php
				if($_GET['numpag']!="")
				{
					$oculto=$_POST['oculto'];
					if($oculto!=2)
					{
					$_POST['numres']=$_GET['limreg'];
					$_POST['numpos']=$_GET['limreg']*($_GET['numpag']-1);
					$_POST['nummul']=$_GET['numpag']-1;
					}
				}
				else
				{
					if($_POST['nummul']=="")
					{
						$_POST['numres']=10;
						$_POST['numpos']=0;
						$_POST['nummul']=0;
						echo"<script>document.getElementById('divcarga').style.display='none';</script>";
					}
				}
			?>
			<table  class="inicio" style="width:99.7%">
				<tr >
					<td class="titulos" colspan="13">:. Buscar Recaudos </td>
					<td style="width:2cm;" class="cerrar"><a href="teso-principal.php">Cerrar</a></td>
				</tr>
				<tr>
					<td class="saludo1" style="width:2.5cm;" >No Liquidaci&oacute;n: </td>
        			<td style="width:20%;" ><input type="serch" name="nLiquid" id="nLiquid" value="<?php echo $_POST['nLiquid'];?>" style="width:98%;" ></td>

                    <td class="saludo1" style="width:3.5cm;">Concepto Liquidaci&oacute;n: </td>
                    <td><input type="serch" name="conLiquid" id="conLiquid" value="<?php echo $_POST['conLiquid']?>"  style="width:98%;"></td>

                    <td class="saludo1" style="width:2.2cm;">Fecha inicial:</td>
       				<td style="width:9%;"><input name="fechaini"  type="text" value="<?php echo $_POST['fechaini']?>" maxlength="10"  onKeyPress="javascript:return solonumeros(event)" onKeyUp="return tabular(event,this)"  id="fc_1198971545" onchange="" onKeyDown="mascara(this,'/',patron,true)" style="width:75%;" title="DD/MM/YYYY" placeholder="DD/MM/YYYY"/>&nbsp;<a href="#" onClick="displayCalendarFor('fc_1198971545');" title="Calendario"><img src="imagenes/calendario04.png" style="width:20px;"/></a></td>

                    <td class="saludo1" style="width:2.2cm;">Fecha final:</td>
                    <td style="width:9%;"><input name="fechafin" type="text" value="<?php echo $_POST['fechafin']?>" maxlength="10"  onKeyPress="javascript:return solonumeros(event)" onKeyUp="return tabular(event,this)"  id="fc_1198971546" onchange="" onKeyDown="mascara(this,'/',patron,true)" style="width:75%;" title="DD/MM/YYYY" placeholder="DD/MM/YYYY"/>&nbsp;<a href="#" onClick="displayCalendarFor('fc_1198971546');" title="Calendario"><img src="imagenes/calendario04.png" style="width:20px;"/></a></td>

    				<td style="padding-bottom:0px"><em class="botonflecha" onClick="buscarbotonfiltro();">Buscar</em></td>
				</tr>
			</table>
			<input type="hidden" name="oculto" id="oculto"  value="1"/>
			<input type="hidden" name="var1" value="<?php echo $_POST['var1'];?>"/>
			<input type="hidden" name="bandera" id="bandera" value="1"/>
			<input type="hidden" name="numres" id="numres" value="<?php echo @$_POST['numres'];?>"/>
			<input type="hidden" name="numpos" id="numpos" value="<?php echo @$_POST['numpos'];?>"/>
			<input type="hidden" name="nummul" id="nummul" value="<?php echo @$_POST['nummul'];?>"/>
			<div class="subpantallap" style="height:62%; width:99.6%; overflow-x:hidden;" id="divdet">
				<?php
					if(@$_POST['oculto']==2)
					{
						$sqlr="SELECT * FROM tesorecaudos WHERE id_recaudo=".$_POST['var1']."";
						$resp = mysqli_query($linkbd,$sqlr);
						$row=mysqli_fetch_row($resp);
						//********Comprobante contable en 000000000000
						$sqlr=" UPDATE comprobante_cab SET total_debito=0,total_credito=0,estado='0' WHERE tipo_comp=2 AND numerotipo=$row[0]";
						mysqli_query($linkbd,$sqlr);
						$sqlr="UPDATE comprobante_det SET valdebito=0,valcredito=0 WHERE id_comp='2 $row[0]'";
						mysqli_query($linkbd,$sqlr);
						$sqlr="UPDATE tesorecaudos SET estado='N' WHERE id_recaudo=$row[0]";
						mysqli_query($linkbd,$sqlr);

						//echo "<script>";
					}

					if (@$_POST['nLiquid']!="")
					{
						$crit1="AND id_recaudo LIKE '".$_POST['nLiquid']."' ";
					}
					else
					{
						$crit1="";
					}

					if (@$_POST['conLiquid']!="")
					{
						$crit2="AND concepto LIKE '".$_POST['conLiquid']."' ";
					}
					else
					{
						$crit2="";
					}

					if(@$_POST['fechaini']!='')
					{
						preg_match("/([0-9]{2})\/([0-9]{2})\/([0-9]{4})/",$_POST['fechaini'],$fecha);
						$fechai="$fecha[3]-$fecha[2]-$fecha[1]";
						if(@$_POST['fechafin']!='')
						{
							preg_match("/([0-9]{2})\/([0-9]{2})\/([0-9]{4})/",$_POST['fechafin'],$fecha);
							$fechaf="$fecha[3]-$fecha[2]-$fecha[1]";
							$crit3 = "AND fecha BETWEEN '$fechai' AND '$fechaf'";
						}
						else
						{
							$fechaf=date("Y-m-d");
							$crit3 = "AND fecha BETWEEN '$fechai' AND '$fechaf'";
						}
					}
					else
					{
						$crit3='';
					}

					//sacar el consecutivo
					$sqlr="SELECT *FROM tesorecaudos WHERE id_recaudo>-1 $crit1 $crit2 $crit3 ORDER BY id_recaudo DESC";
					$resp = mysqli_query($linkbd,$sqlr);

					$ntr = mysqli_num_rows($resp);

					$_POST['numtop'] = $ntr;

					$nuncilumnas=ceil($_POST['numtop']/$_POST['numres']);

					$cond2="";

                    if ($_POST['numres'] != "-1"){ $cond2="LIMIT $_POST[numpos], $_POST[numres]";}

					$sqlr="SELECT *FROM tesorecaudos WHERE id_recaudo>-1 $crit1 $crit2 $crit3 ORDER BY id_recaudo DESC $cond2";
					$resp = mysqli_query($linkbd, $sqlr);

					$numcontrol=$_POST['nummul']+1;
                    if(($nuncilumnas==$numcontrol)||($_POST['numres']=="-1"))
                    {
                        $imagenforward="<img src='imagenes/forward02.png' style='width:17px'>";
                        $imagensforward="<img src='imagenes/skip_forward02.png' style='width:16px' >";
                    }
                    else
                    {
                        $imagenforward="<img src='imagenes/forward01.png' style='width:17px' title='Siguiente' onClick='numsig()'>";
                        $imagensforward="<img src='imagenes/skip_forward01.png' style='width:16px' title='Fin' onClick='saltopag(\"$nuncilumnas\")'>";
                    }
                    if(($_POST['numpos']==0)||($_POST['numres']=="-1"))
                    {
                        $imagenback="<img src='imagenes/back02.png' style='width:17px'>";
                        $imagensback="<img src='imagenes/skip_back02.png' style='width:16px'>";
                    }
                    else
                    {
                        $imagenback="<img src='imagenes/back01.png' style='width:17px' title='Anterior' onClick='numant();'>";
                        $imagensback="<img src='imagenes/skip_back01.png' style='width:16px' title='Inicio' onClick='saltopag(\"1\")'>";
                    }

					$con=1;

					$namearch="archivos/".$_SESSION['usuario']."-reporterecaudos.csv";
					$Descriptor1 = fopen($namearch,"w+");
					$lista = array ('ID_RECAUDO','CONCEPTO','FECHA','DOC TERCERO','TERCERO','VALOR','ESTADO');
					fputcsv($Descriptor1, $lista,";");

					echo "
					<table class='inicio' align='center' >
						<tr>
							<td colspan='7' class='titulos'>.: Resultados Busqueda:</td>
							<td class='submenu'>
                                <select name='renumres' id='renumres' onChange='cambionumero();' style='width:100%'>
                                    <option value='10'"; if ($_POST['renumres']=='10'){echo 'selected';} echo ">10</option>
                                    <option value='20'"; if ($_POST['renumres']=='20'){echo 'selected';} echo ">20</option>
                                    <option value='30'"; if ($_POST['renumres']=='30'){echo 'selected';} echo ">30</option>
                                    <option value='50'"; if ($_POST['renumres']=='50'){echo 'selected';} echo ">50</option>
                                    <option value='100'"; if ($_POST['renumres']=='100'){echo 'selected';} echo ">100</option>
                                    <option value='-1'"; if ($_POST['renumres']=='-1'){echo 'selected';} echo ">Todos</option>
                                </select>
                            </td>
						</tr>
						<tr><td colspan='2' id='RecEnc' >Recaudos Encontrados: $ntr2 </td></tr>
						<tr>
							<td class='titulos2' style='width:7%;'>Codigo</td>
							<td class='titulos2'>Nombre</td>
							<td class='titulos2' style='width:7%;'>Fecha</td>
							<td class='titulos2'  style='width:10%;'>Contribuyente</td>
							<td class='titulos2' style='width:12%;'>Valor</td>
							<td class='titulos2' style='width:5%;text-align:center;'>Estado</td>
							<td class='titulos2' style='width:5%;text-align:center;'>Anular</td>
							<td class='titulos2' style='width:5%;text-align:center;'>Ver</td>
						</tr>";
					$iter='zebra1';
					$iter2='zebra2';
					$filas=1;

					if($_POST['fechaini'] == '' && $_POST['fechafin'] == '' && $_POST['nLiquid'] == '' && $_POST['conLiquid'] == '')
					{
						echo "
						<table class='inicio'>
							<tr>
								<td class='saludo1' style='text-align:center;width:100%;font-size:25px'>Utilice el filtro de busqueda</td>
							</tr>
						</table>";
                        $nuncilumnas = 0;
					}
					elseif(mysqli_num_rows($resp) == 0 || mysqli_num_rows($resp) == '0')
					{
						echo "
						<table class='inicio'>
							<tr>
								<td class='saludo1' style='text-align:center;width:100%;font-size:25px'>No hay resultados de su busqueda.</td>
							</tr>
						</table>";
					}
					else
					{
						while ($row =mysqli_fetch_row($resp))
						{
							$ntr2 = $ntr;

							echo "<script>document.getElementById('RecEnc').innerHTML = 'Recaudos Encontrados: $ntr2'</script>";

							$ntercero=buscatercero($row[4]);
							unset($lista);
							$lista = array ($row[0],$row[6],$row[2],$row[4],$ntercero,number_format($row[5],2,",",""),$row[7]);
							fputcsv($Descriptor1, $lista,";");

							if($gidcta!="")
                            {
                                if($gidcta==$row[0])
								{
									$estilo='background-color:yellow';
								}
                                else
								{
									$estilo="";
								}
                            }
                            else
							{
								$estilo="";
							}

							$idcta="'$row[0]'";
							$numfil= "'$filas'" ;


							$filtro1 = "'$_POST[nLiquid]'";
                            $filtro2 = "'$_POST[conLiquid]'";
                            $filtro3 = "'$_POST[fechaini]'";
                            $filtro4 = "'$_POST[fechafin]'";

							$sqlrFac = "SELECT factura_electronica FROM tesorecaudo_factura_elec WHERE id_tesorecaudo = '".$row[0]."'";
							$respFac = mysqli_query($linkbd, $sqlrFac);
							$rowFac = mysqli_fetch_row($respFac);

							echo"
                                <input type='hidden' name='codigoE[]' value='".$row[0]."'>
                                <input type='hidden' name='nombreE[]' value='".$row[6]."'>
                                <input type='hidden' name='fechaE[]' value='".$row[2]."'>
                                <input type='hidden' name='nomContribuyente[]' value='".$row[4]."'>
                                <input type='hidden' name='valorE[]' value='".$row[5]."'>
                                <input type='hidden' name='facturaElectronica[]' value='".$rowFac[0]."'>
                            ";

                            if ($row[7]=='S')
                            {
                                echo"
                                <input type='hidden' name='estadoE[]' value='ACTIVO'>";
                            }
                            if ($row[7]=='N')
                            {
                                echo"
                                <input type='hidden' name='estadoE[]' value='ANULADO'>";
                            }
                            if ($row[7]=='P')
                            {
                                echo"
                                <input type='hidden' name='estadoE[]' value='PAGO'>";
                            }

							echo "
							<tr class='$iter' onDblClick=\"verUltimaPos($idcta, $numfil, $filtro1, $filtro2, $filtro3, $filtro4)\" style='text-transform:uppercase; $estilo' >
								<td >$row[0]</td>
								<td >$row[6]</td>
								<td >$row[2]</td>
								<td >$row[4]</td>
								<td style='text-align:right'>$ ".number_format($row[5],2,".",",")."</td>";
							if ($row[7]=='S')
							echo "<td style='text-align:center;'><img src='imagenes/confirm.png'></center></td><td ><a href='#' onClick=eliminar($row[0])><center><img src='imagenes/anular.png'></a></td>";
							if ($row[7]=='N')
							echo "<td style='text-align:center;'><img src='imagenes/del3.png'></td><td ></td>";
							if ($row[7]=='P')
							echo "<td style='text-align:center;'><img src='imagenes/dinero3.png'></td><td ></td>";
							echo "<td style='text-align:center;'><a onClick=\"verUltimaPos($idcta, $numfil, $filtro1, $filtro2, $filtro3, $filtro4)\" style='cursor:pointer;' ><img src='imagenes/lupa02.png'  style='width:19px;'></a></td></tr>";
							$con+=1;
							$aux=$iter;
							$iter=$iter2;
							$iter2=$aux;
							$filas++;
						}

					}

					fclose($Descriptor1);

					echo "<script>document.getElementById('divcarga').style.display='none';</script>";

					echo"</table>
                    <table class='inicio'>
                        <tr>
                            <td style='text-align:center;'>
                                <a href='#'>$imagensback</a>&nbsp;
                                <a href='#'>$imagenback</a>&nbsp;&nbsp;";

                                if($nuncilumnas<=9){$numfin=$nuncilumnas;}
                                else{$numfin=9;}
                                for($xx = 1; $xx <= $numfin; $xx++)
                                {
                                    if($numcontrol<=9){$numx=$xx;}
                                    else{$numx=$xx+($numcontrol-9);}
                                    if($numcontrol==$numx){echo"<a href='#' onClick='saltopag(\"$numx\")'; style='color:#24D915'> $numx </a>";}
                                    else {echo"<a href='#' onClick='saltopag(\"$numx\")'; style='color:#000000'> $numx </a>";}
                                }
                                echo"&nbsp;&nbsp;<a href='#'>$imagenforward</a>
                                    &nbsp;<a href='#'>$imagensforward</a>
                            </td>
                        </tr>
                    </table>";
				?>
			</div>
			<input type="hidden" name="numtop" id="numtop" value="<?php echo @$_POST['numtop'];?>" />
		</form>
	</body>
</html>
