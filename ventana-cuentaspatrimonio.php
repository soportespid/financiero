<?php
	header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=utf8");

	require 'comun.inc';
	require 'funciones.inc';

	$linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");

	session_start();
    date_default_timezone_set("America/Bogota");
?>
<!DOCTYPE >
<html lang="es">
	<head>
		<meta charset="utf-8"/>
		<meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1"/>
		<meta name="viewport" content="user-scalable=no">
		<title>:: IDEAL 10 - Contabilidad</title>
		<link href="favicon.ico" rel="shortcut icon"/>
		<link href="css/css2.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css">
		<link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css">
		<link href="css/css4.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css">
		<script>
		function ponprefijo(pref,opc)
		{
			parent.document.form2.cuenta.value = pref;
			parent.document.form2.ncuenta.value = opc;
			parent.despliegamodal2("hidden");
		}
		</script>
	</head>
	<body>
		<form name="form1" action="" method="post">
			<table class="inicio ancho" style="width:99.6%;">
				<tr>
					<td colspan="5" class="titulos" >Buscar Cuenta de Patrimonio</td>
					<td class="cerrar" style="width:7%" onClick="parent.despliegamodal2('hidden');">Cerrar</td>
				</tr>
				<tr><td colspan="6" class="titulos2" >:&middot; Por Descripcion </td></tr>
				<tr>
					<td class="saludo1" style="width:3cm;">:&middot; Numero Cuenta:</td>
					<td colspan="3">
						<input type="text" name="numero" id="numero" style="width: 100%;">
					</td>
					<td><em class="botonflechaverde" onClick="document.form1.submit();">Buscar</em></td>
				</tr>
			</table>
			<input name="oculto" type="hidden" id="oculto" value="1" >
			<div class="subpantalla" style="height:80.5%; width:99.3%; overflow-x:hidden;">
				<table class="inicio">
					<tr ><td height="25" colspan="5" class="titulos" >Resultados Busqueda </td></tr>
					<tr>
						<td style='width:4%' class="titulos2" >Item</td>
						<td style='width:12%' class="titulos2" >Cuenta </td>
						<td class="titulos2" >Descripcion</td>
						<td style='width:16%' class="titulos2" >Tipo</td>
						<td style='width:6%' class="titulos2" >Estado</td>
					</tr>
					<?php
						$crit1 = "";
						$crit2 = "";
						$cond = " where tabla.cuenta like'%".$_POST['numero']."%' or tabla.nombre like '%".strtoupper($_POST['numero'])."%'";
						$sqlr = "SELECT * FROM (SELECT cn1.cuenta, cn1.nombre, cn1.naturaleza, cn1.centrocosto, cn1.tercero, cn1.tipo, cn1.estado FROM cuentasnicsp AS cn1 INNER JOIN cuentasnicsp AS cn2 ON cn2.tipo='Auxiliar' AND cn2.cuenta LIKE CONCAT( cn1.cuenta, '%' ) WHERE cn1.tipo='Mayor' AND cn1.cuenta LIKE '3%' GROUP BY cn1.cuenta UNION SELECT cuenta, nombre, naturaleza, centrocosto, tercero, tipo, estado FROM cuentasnicsp WHERE tipo='Auxiliar' AND cuenta LIKE '3%') AS tabla $cond ORDER BY 1";
						$resp = mysqli_query($linkbd,$sqlr);
						$ntr = mysqli_num_rows($resp);
						$i = 1;
						$co = 'saludo1a';
						$co2 = 'saludo2';
						while ($r =mysqli_fetch_row($resp))
						{
							if ($r[5]=='Auxiliar' || $r[5] == 'AUXILIAR')
							{

								echo "<tr class='$co' onClick=\"javascript:ponprefijo('$r[0]','$r[1]')\">";
							} 
							else {

								echo "<tr class='$co'>";
							} 
							echo "
								<td>$i</td>
								<td>$r[0]</td>
								<td>$r[1]</td>
								<td>$r[5]</td>
								<td style='text-align:center;'>$r[6]</td></tr>";
							$aux = $co;
							$co = $co2;
							$co2 = $aux;
							$i = 1 + $i;
						}
					?>
				</table>
			</div>
 		</form>
	</body>
</html>