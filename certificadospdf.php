<?php
	require_once "tcpdf/tcpdf_include.php";
	require 'comun.inc';
    session_start();
	class MYPDF extends TCPDF
	{
        public function Header()
		{
            $linkbd=conectar_v7();
			$linkbd -> set_charset("utf8");
			$sqlr="select *from configbasica where estado='S'";
			$res=mysqli_query($linkbd,$sqlr);
			while($row=mysqli_fetch_row($res))
			{
				$nit=$row[0];
				$rs=$row[1];
				$nalca=$row[6];
			}
			$this->Image('imagenes/escudo.jpg', 12, 12, 25, 25, 'JPG', '', 'T', true, 300, '', false, false, 0, false, false, false);
			$this->SetFont('helvetica','B',7);
			$this->SetY(10);
            $this->RoundedRect(10, 10, 190, 29, 1,'1111' );
			$this->Cell(0.1);
			$this->Cell(30,29,'','R',0,'L');
			$this->SetY(8);
			$this->SetX(58);
			$this->SetFont('helvetica','B',12);
			$this->SetX(35);
            $this->Cell(149,15,"$rs",0,0,'C');
            $this->SetY(16);
			$this->SetX(35);
			$this->SetFont('helvetica','B',11);
			$this->Cell(149,10,"$nit",0,0,'C');
			//*****************************************************************************************************************************
			$this->SetFont('helvetica','B',14);
			$this->SetY(10);
			$this->Cell(50.1);
			$this->SetY(8);
			$this->Cell(50.1);
			//************************************
			$this->SetFont('helvetica','B',6);
			$this->SetY(25);
			$this->Cell(150);
			$this->Cell(40,14,'','TL',0,'L');
			$this->SetY(27.5);
			$this->Cell(162);
			$this->SetY(26);
			$this->Cell(152);
			$this->Cell(37,6,'PERIODO: '.$_POST['fecha'].'-'.$_POST['fecha2'],0,0,'L');
			$this->SetY(32);
			$this->Cell(152);
			$this->Cell(37,6,'FECHA: '.date('d/m/Y'),0,0,'L');

			$this->SetY(25);
			$this->Cell(30.1);
			$this->SetFont('helvetica','B',12);
			$this->Cell(120,14,'CERTIFICADO DE RETENCION','T',0,'C');

        }
        public function Footer()
        {
            $linkbd = conectar_v7();
			$linkbd -> set_charset("utf8");
			$sqlr="SELECT direccion,telefono,web,email FROM configbasica WHERE estado='S'";
			$resp=mysqli_query($linkbd, $sqlr);
			$user = $_SESSION['nickusu'];
			$fecha = date("Y-m-d H:i:s");
			$ip = $_SERVER['REMOTE_ADDR'];
			$useri = $_POST['user'];
			while($row=mysqli_fetch_row($resp))
			{
				$direcc=strtoupper($row[0]);
				$telefonos=$row[1];
				$dirweb=strtoupper($row[3]);
				$coemail=strtoupper($row[2]);
			}
			if($direcc!=''){$vardirec="Dirección: $direcc, ";}
			else {$vardirec="";}
			if($telefonos!=''){$vartelef="Telefonos: $telefonos";}
			else{$vartelef="";}
			if($dirweb!=''){$varemail="Email: $dirweb, ";}
			else {$varemail="";}
			if($coemail!=''){$varpagiw="Pagina Web: $coemail";}
			else{$varpagiw="";}

			$txt = <<<EOD
			$vardirec $vartelef
			$varemail $varpagiw
			EOD;
			$this->SetFont('helvetica', 'I', 5);
			$this->Cell(190,7,'','T',0,'T');
			$this->ln(1);
			$this->Write(0, $txt , '', 0, 'C', true, 0, false, false, 0);
			//$this->Cell(12, 7, 'Hecho por: '.$useri, 0, false, 'C', 0, '', 0, false, 'T', 'M');
			$this->Cell(15, 7, 'Impreso por: '.$user, 0, false, 'C', 0, '', 0, false, 'T', 'M');
			$this->Cell(57, 7, 'IP: '.$ip, 0, false, 'C', 0, '', 0, false, 'T', 'M');
			$this->Cell(50, 7, 'Fecha: '.$fecha, 0, false, 'C', 0, '', 0, false, 'T', 'M');
			$this->Cell(54, 7, 'IDEAL.10 S.A.S', 0, false, 'C', 0, '', 0, false, 'T', 'M');
			$this->Cell(25, 7, 'Pagina '.$this->getAliasNumPage().'/'.$this->getAliasNbPages(), 0, false, 'C', 0, '', 0, false, 'T', 'M');

        }
    }
    $pdf = new MYPDF('P','mm','Letter', true, 'utf8', false);
	$pdf->SetDocInfoUnicode (true);
	// set document information
	$pdf->SetCreator(PDF_CREATOR);
	$pdf->SetAuthor('IDEAL10');
	$pdf->SetTitle('Certificados');
	$pdf->SetSubject('CERTIFICADO DE RETENCION');
	$pdf->SetKeywords('CERTIFICADO DE RETENCION');
    $pdf->SetMargins(10, 40, 10);// set margins
    $pdf->SetHeaderMargin(40);// set margins
    $pdf->SetFooterMargin(17);// set margins
	$pdf->SetAutoPageBreak(TRUE, 20);// set auto page breaks
	// set some language-dependent strings (optional)
	if (@file_exists(dirname(__FILE__).'/lang/spa.php'))
	{
		require_once(dirname(__FILE__).'/lang/spa.php');
		$pdf->setLanguageArray($l);
	}
	$pdf->AddPage();
    $con=0;
	$linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");
	$sqlr="select *from configbasica where estado='S'";
	$res=mysqli_query($linkbd, $sqlr);
	while($row=mysqli_fetch_row($res))
	{
        $nit=$row[0];
        $rs=$row[1];
	}
    $pdf->SetFont('Helvetica','B',12);
	$pdf->ln(1);
    $pdf->Cell(190,12,'CERTIFICA:',0,1,'C');
    $pdf->SetFont('times','',10);
    if($_POST['fecha1']!="" && $_POST['fecha2']!=""){
        $cond=" y entre el periodo de $_POST[fecha1] al $_POST[fecha2], ";
    }
    $pdf->MultiCell(190,10,"Durante el periodo gravable de ".$_POST['fecha']." al ".$_POST['fecha2']." ".$cond."practicó en el ".$rs.", Retención en la Fuente al Proveedor ".$_POST['ntercero']." con Nit/Cc ".$_POST['tercero'].'.'."\n",0,'J',false,1);
	$pdf->ln(5);
    $yy=$pdf->GetY();
    $pdf->SetY($yy);
    $yy=$pdf->GetY();
    $pdf->SetFillColor(222,222,222);
    $pdf->SetFont('helvetica','B',7);
    $pdf->Cell(35,5,'Código',0,0,'C',1);
    $pdf->SetY($yy);
    $pdf->Cell(36);
    $pdf->Cell(70,5,'Retención',0,0,'C',1);
    $pdf->SetY($yy);
    $pdf->Cell(107);
    $pdf->Cell(50,5,'Monto del Pago Sujeto a Retención',0,0,'C',1);
    $pdf->SetY($yy);
    $pdf->Cell(158);
    $pdf->Cell(32,5,'Valor',0,1,'C',1);
    $pdf->SetFont('helvetica','',6);
    $pdf->ln(1);

    $con=0;
    $yy = $pdf -> GetY();
    $pdf->SetY($yy-3);
    for($y=0;$y<count($_POST['valores']);$y++)
	{
        if ($con%2==0){
            $pdf->SetFillColor(255,255,255);
        }
		else{
            $pdf->SetFillColor(245,245,245);
        }
        if($vv>=170)
        {
            $pdf->AddPage();
            $vv=$pdf->gety();
        }
        $pdf->MultiCell(35,3,''.$_POST['codigo'][$con],0,'C',true,0,'','',true,0,false,true,0,'M',true);
        $pdf->MultiCell(71,3,substr(''.$_POST['nombres'][$con],0,50),0,'C',true,0,'','',true,0,false,true,0,'M',true);
        $pdf->MultiCell(51,3,''.number_format($_POST['valoresret'][$con],2).' '.$_POST['ncompro'][$con],0,'C',true,0,'','',true,0,false,true,0,'M',true);
        $pdf->MultiCell(33,3,''.number_format($_POST['valores'][$con],2),0,'C',true,1,'','',true,0,false,true,0,'M',true);
        $con++;
		$total= $total + $_POST['valores'][$con];
    }
    $pdf->ln(2);
    $pdf->Cell(157,5,'Total','T',0,'R');
    $pdf->SetX(167);
	$pdf->Cell(33,5,'$'.number_format($total,2,".",","),'T','C',0);
    $pdf->SetFont('helvetica','',8);
    $pdf->ln(10);
    $v=$pdf->gety();

    $pdf->MultiCell(190,4,"El presente certificado se expide en concordancia con las disposiciones legales contenidas en el artículo 381 del Estatuto Tributario. \n",0,'J');
    $pdf->ln(2);

    $pdf->MultiCell(190,4,"Dicha retención fue consignada oportunamente a nombre de La Administración de Impuestos Nacionales en el $rs.\n",0,'J');
    $pdf->ln(2);

    $pdf->MultiCell(190,4,"Señor (a) recuerde que usted puede estar obligado a declarar renta por el año gravable de ".$_POST['vigencias'].".\n",0,'J');
    $pdf->ln(2);

    $pdf->MultiCell(190,4,"Para mayor información lo invitamos a que consulte la página en internet (www.dian.gov.co) o acerquese a la Administración más cercana a su domicilio.\n",0,'J');
    $pdf->ln(2);

    $pdf->MultiCell(190,4,"NO REQUIERE FIRMA AUTOGRAFA ART. 10 DECRETO 836/91 \n",0,'J');

    $pdf->Output();
