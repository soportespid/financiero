<?php 
	//ini_set('max_execution_time',3600);
	require "comun.inc"; 
	$linkbd=conectar_v7();	
	$sql="SELECT * FROM acticrearact_det";
	$res=mysqli_query($linkbd,$sql);

	if($res > 0){
		$delimiter = ",";
		$filename = "reportecontraloria_" . date('Y-m-d') . ".csv";
		
		$f = fopen('php://memory', 'w');
		
		$fields = array('Fecha Adquisición O Baja', 'Concepto', 'Codigo Contable', 'Detalle', 'Valor');
		fputcsv($f, $fields, $delimiter,"\t\n\r");
		
		while($row=mysqli_fetch_row($res)){
			$rest = substr($row[1],0,-4);
			$sqlDebito = "SELECT cuenta_activo FROM acti_activos_det WHERE tipo LIKE '$rest' AND disposicion_activos = '$row[30]' AND centro_costos LIKE '$row[14]'";
			$rowDebito = mysqli_fetch_row(mysqli_query($linkbd, $sqlDebito));
			
			$detalle = $row[2];
			$detalle = trim($detalle, ",;\t\n\r\x0B\0");
			$caracteres = array(",", ";", "\t", "\n", "\r", "\x0B", "\0", "?", "\r\n");
			$detalle = str_replace($caracteres, "", $detalle);

			$lineData = array($row[8], "ADQUISICION", $rowDebito[0], $detalle , $row[15]);
			fputcsv($f, $lineData, $delimiter,"\t\n\r");
		}
		
		fseek($f, 0);
		
		header('Content-Type: text/csv');
		header('Content-Disposition: attachment; filename="' . $filename . '";');
		
		fpassthru($f);
		fclose($f);
	}
	exit;

?>