<?php
	header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=utf8");

	require "comun.inc";
	require "funciones.inc";
	require "conversor.php";
	require "validaciones.inc";

	$linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");

	session_start();
	date_default_timezone_set("America/Bogota");
?>
<!DOCTYPE >
<html lang="es">
	<head>
		<meta charset="utf-8"/>
		<meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1"/>
		<meta name="viewport" content="user-scalable=no">
		<title>:: IDEAL 10 - Tesoreria</title>
		<link href="favicon.ico" rel="shortcut icon"/>

		<script>
			//************* ver reporte *************
			//***************************************
			function verep(idfac)
			{
				document.form1.oculto.value=idfac;
				document.form1.submit();
			}

			//************* genera reporte ************
			//***************************************
			function genrep(idfac)
			{
				document.form2.oculto.value=idfac;
				document.form2.submit();
			}

			function generar()
 			{
				document.form2.oculto.value='1';
				document.form2.submit();
 			}

			function validar()
			{
				document.form2.submit();
			}

			function buscarp(e)
 			{
				if (document.form2.rp.value!="")
				{
					document.form2.brp.value='1';
					document.form2.submit();
				}
			}

			function agregardetalle()
			{
				if(document.form2.numero.value!="" &&  document.form2.valor.value>0 &&  document.form2.banco.value!=""  )
				{
					document.form2.agregadet.value=1;
					//document.form2.chacuerdo.value=2;
					document.form2.submit();
				}
				else{
					alert("Falta informacion para poder Agregar");
				}
			}

			function agregardetalled()
			{
				if(document.form2.retencion.value!="" &&  document.form2.vporcentaje.value!=""  )
				{
					document.form2.agregadetdes.value=1;
					//document.form2.chacuerdo.value=2;
					document.form2.submit();
				}
				else
				{
					alert("Falta informacion para poder Agregar");
				}
			}

			function eliminar(variable)
			{
				document.form2.elimina.value=variable;
				//eli=document.getElementById(elimina);
				vvend=document.getElementById('elimina');
				//eli.value=elimina;
				vvend.value=variable;
				document.form2.submit();
			}

			function eliminard(variable)
			{
				document.form2.eliminad.value=variable;
				//eli=document.getElementById(elimina);
				vvend=document.getElementById('eliminad');
				//eli.value=elimina;
				vvend.value=variable;
				document.form2.submit();
			}

			//************* genera reporte ************
			//***************************************
			function guardar()
			{
				if (document.form2.fecha.value!='')
  				{
					if (confirm("Esta Seguro de Guardar"))
					{
						document.form2.oculto.value=2;
						document.form2.submit();
					}
  				}
  				else
				{
  					alert('Faltan datos para completar el registro');
					document.form2.fecha.focus();
					document.form2.fecha.select();
  				}
			}

			function calcularpago()
			{
				//alert("dddadadad");
				valorp=document.form2.valor.value;
				descuentos=document.form2.totaldes.value;
				valorc=valorp-descuentos;
				document.form2.valorcheque.value=valorc;
				document.form2.valoregreso.value=valorp;
				document.form2.valorretencion.value=descuentos;
			}

			function agregardetalled()
			{
				if(document.form2.retencion.value!="" )
				{
					var tipoRetencion = document.form2.tipoRetencion.value;
					if(tipoRetencion == 'C')
					{
						var tipoRete = document.form2.tiporet.value;
						if(tipoRete != '')
						{
							document.form2.agregadetdes.value=1;
							//document.form2.chacuerdo.value=2;
							document.form2.submit();
						}
						else
						{
							alert("Falta seleccionar el tipo de retencion");
						}
					}
					else
					{
						document.form2.agregadetdes.value=1;
						//document.form2.chacuerdo.value=2;
						document.form2.submit();
					}
				}
				else
				{
					alert("Seleccione una retencion");
				}
			}

			function eliminard(variable)
			{
				if (confirm("Esta Seguro de Eliminar"))
				{
					document.form2.eliminad.value=variable;
					//eli=document.getElementById(elimina);
					vvend=document.getElementById('eliminad');
					//eli.value=elimina;
					vvend.value=variable;
					document.form2.submit();
				}
			}

			function pdf()
			{
				document.form2.action="pdfpagotercerosreporte.php";
				document.form2.target="_BLANK";
				document.form2.submit();
				document.form2.action="";
				document.form2.target="";
			}

			function buscater(e)
			{
				//alert('rrrr');
				if (document.form2.tercero.value!="")
				{
					//	 alert('ffff');
					document.form2.bt.value='1';
					//alert('rrrr');
					document.form2.submit();
				}
			}

			function excell()
			{
				document.form2.action="teso-pagotercerosdetalleexcel.php";
				document.form2.target="_BLANK";
				document.form2.submit();
				document.form2.action="";
				document.form2.target="";
			}
		</script>
		<script src="css/calendario.js"></script>
		<script src="css/programas.js"></script>
		<link href="css/css2.css" rel="stylesheet" type="text/css" />
		<link href="css/css3.css" rel="stylesheet" type="text/css" />
		<link href="css/tabs.css" rel="stylesheet" type="text/css" />

	</head>
	<body>
		<IFRAME src="alertas.php" name="alertas" id="alertas" style="display:none"></IFRAME>
		<span id="todastablas2"></span>
		<table>
			<tr><script>barra_imagenes("teso");</script><?php cuadro_titulos();?></tr>
			<tr><?php menu_desplegable("teso");?></tr>
			<tr>
  				<td colspan="3" class="cinta">
					<a href="teso-pagotercerosdetalle_fechas.php" class="mgbt"><img src="imagenes/add.png" alt="Nuevo"  title="Nuevo" border="0" /></a>
					<a href="#" class="mgbt"  ><img src="imagenes/guardad.png"  alt="Guardar" title="Guardar"/></a>
					<a href="teso-buscapagoterceros.php" class="mgbt"> <img src="imagenes/busca.png"  alt="Buscar" border="0" title="Buscar"/></a>
					<a href="#" onClick="mypop=window.open('teso-principal.php','','');mypop.focus();" class="mgbt"><img src="imagenes/nv.png" alt="nueva ventana" title="Nueva ventana"></a>
					<a href="#" onClick="pdf()" class="mgbt"> <img src="imagenes/print.png"  alt="Buscar" title="imprimir"/></a>
					<a href="#" onClick="excell()" class="mgbt"><img src="imagenes/excel.png"  alt="excel" title="Excel"></a>
					<a href="teso-declaracionretenciones.php"  class="mgbt"><img src="imagenes/iratras.png" title="Atr&aacute;s"></a>
				</td>
			</tr>
		</table>
		<tr>
			<td colspan="3" class="tablaprin" align="center">
				<?php
				$vigusu=vigencia_usuarios($_SESSION['cedulausu']);
				$_POST['vigencia']=$vigusu;
				$vigencia=$vigusu;
  				$vact=$vigusu;
	  			//*********** cuenta origen va al credito y la destino al debito
				if(!$_POST['oculto'])
				{
					$sqlr="select *from cuentapagar where estado='S' ";
					$res=mysqli_query($linkbd, $sqlr);
					while ($row =mysqli_fetch_row($res))
					{
	 					$_POST['cuentapagar']=$row[1];
					}
					$check1="checked";
					$fec=date("d/m/Y");
					$_POST['fecha']=$fec;
					//$_POST[valor]=0;
					//$_POST[valorcheque]=0;
					//$_POST[valorretencion]=0;
					//$_POST[valoregreso]=0;
					//$_POST[totaldes]=0;
					$_POST['vigencia']=$vigusu;
					$sqlr="select max(id_pago) from tesopagoterceros";
					$res=mysqli_query($linkbd, $sqlr);
					$consec=0;
					while($r=mysqli_fetch_row($res))
		 			{
		  				$consec=$r[0];
					}
					$consec+=1;
					$_POST['idcomp']=$consec;
				}
				switch($_POST['tabgroup1'])
				{
					case 1:
						$check1='checked';
						break;
					case 2:
						$check2='checked';
						break;
					case 3:
						$check3='checked';
				}
				?>

 				<form name="form2" method="post" action="">
					<?php
					$meses=array('','Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre');
 					if($_POST['bt']=='1')
			 		{
						$nresul=buscatercero($_POST['tercero']);
						if($nresul!='')
						{
							$_POST['ntercero']=$nresul;
						}
			 			else
						{
							$_POST['ntercero']="";
						}
					}
 					?>
	   				<table class="inicio" align="center" >

						<tr>
							<td colspan="7" class="titulos">Declaracion Retenciones</td>
							<td width="5%" class="cerrar" >
								<a href="teso-principal.php">Cerrar</a>
							</td>
						</tr>
						<tr>
							<td class="saludo1" style="width:10%">Fecha: </td>
							<td style="width:12%">
								<input name="fecha" type="text" value="<?php echo $_POST['fecha']?>" style="width:100%; align-content: center;" onKeyPress="javascript:return solonumeros(event)" onKeyUp="return tabular(event,this)"  id="fc_1198971545" onKeyDown="mascara(this,'/',patron,true)" title="DD/MM/YYYY" readonly>
							</td>

                            <td  class="saludo1" style="width:10%;">Fecha Inicial: </td>
                            <td class="saludo1" style="width:10%;">
                                <input name="fechaini"  type="text" value="<?php echo $_POST['fechaini']?>" maxlength="10" onchange="" onKeyPress="javascript:return solonumeros(event)" onKeyUp="return tabular(event,this)"  id="fc_1198971546" onKeyDown="mascara(this,'/',patron,true)" style="width:80%;" title="DD/MM/YYYY"/>&nbsp;<a href="#" onClick="displayCalendarFor('fc_1198971546');" title="Calendario"><img src="imagenes/calendario04.png" style="width:20px;"/></a>
                            </td>
                            <td class="saludo1" style="width:10%;">Fecha Fin: </td>
                            <td style="width:10%;">
                                <input name="fechafin" type="text" value="<?php echo $_POST['fechafin']?>" maxlength="10" onchange="" onKeyPress="javascript:return solonumeros(event)" onKeyUp="return tabular(event,this)"  id="fc_1198971547" onKeyDown="mascara(this,'/',patron,true)" style="width:80%;" title="DD/MM/YYYY"/>&nbsp;<a href="#" onClick="displayCalendarFor('fc_1198971547');" title="Calendario"><img src="imagenes/calendario04.png" style="width:20px;"/></a>
                            </td>
							<td></td>
						</tr>
						<tr>
							<td class="saludo1">Valor a Pagar:</td>
							<td>
								<input name="valorpagar" type="text" id="valorpagar" onKeyUp="return tabular(event,this)" value="<?php echo $_POST['valorpagar']?>" style="width:100%" readonly>
							</td>
							<td class="saludo1">Retenciones e Ingresos:</td>
							<td colspan="4">
								<?php
								$reten=array();
								$nreten=array();
								$rdestino=array();
								?>
								<select name="retencion" style="width:60%" onChange="validar()" onKeyUp="return tabular(event,this)">
									<option value="">Seleccione ...</option>
									<option value="t"  <?php if($_POST['retencion']=='t') echo "  SELECTED"?>>Todos</option>
									<?php
									//PARA LA PARTE CONTABLE SE TOMA DEL DETALLE DE LA PARAMETRIZACION LAS CUENTAS QUE INICIAN EN 2**********************

									$sqlr="select *from tesoretenciones where estado='S' AND destino!='' order by codigo";
									$res=mysqli_query($linkbd, $sqlr);
									while ($row =mysqli_fetch_row($res))
									{
										echo "<option value='R-$row[0]' ";
										$i=$row[0];

										if('R-'.$i==$_POST['retencion'])
										{
											echo "SELECTED";
											$_POST['nretencion']='R - '.$row[1]." - ".$row[2];
										}
										echo ">R - ".$row[1]." - ".$row[2]."</option>";
										$reten[]= 'R-'.$row[0];
										$nreten[]= 'R - '.$row[1]." - ".$row[2];
										$rdestino[]=$row[8];
									}
									$sqlr="select *from tesoingresos where estado='S' and (terceros='1' or terceros='N' or terceros='D' or terceros='M') order by codigo";
									$res=mysqli_query($linkbd, $sqlr);
									while ($row =mysqli_fetch_row($res))
									{
										echo "<option value='I-$row[0]' ";
										$i=$row[0];

										if('I-'.$i==$_POST['retencion'])
										{
											echo "SELECTED";
											$_POST['nretencion']='I - '.$row[1]." - ".$row[2];
										}
										echo ">I - $row[0] - ".$row[1]." - ".$row[2]."</option>";
										$reten[]= 'I-'.$row[0];
										$nreten[]= 'I - '.$row[1]." - ".$row[2];
										$rdestino[]=$row[4];
									}
									?>
								</select>
								<input type="hidden" value="<?php echo $_POST['nretencion']?>" name="nretencion"><input type="hidden" value="<?php echo $_POST['oculto']?>" name="oculto">
								<?php
								if($_POST['retencion']=='t')
								{
									?>
									<select name="tiporet" id="tiporet">
										<option value="" >Seleccione...</option>
										<option value="N" <?php if($_POST['tiporet']=='N') echo "SELECTED"?>>Nacional</option>
										<option value="D" <?php if($_POST['tiporet']=='D') echo "SELECTED"?>>Departamental</option>
										<option value="M" <?php if($_POST['tiporet']=='M') echo "SELECTED"?>>Municipal</option>
									</select>
									<?php
								}
								else
								{
									$tipoRetencion ='';
									$tipoRetencion = buscaTipoDeRetencion(substr($_POST['retencion'],-2));
									if($tipoRetencion == 'C')
									{
										?>
										<select name="tiporet" id="tiporet">
											<option value="" >Seleccione...</option>
											<option value="N" <?php if($_POST['tiporet']=='N') echo "SELECTED"?>>Nacional</option>
											<option value="D" <?php if($_POST['tiporet']=='D') echo "SELECTED"?>>Departamental</option>
											<option value="M" <?php if($_POST['tiporet']=='M') echo "SELECTED"?>>Municipal</option>
										</select>
										<?php
									}
								}
								?>
								<input type='hidden' name='tipoRetencion' id='tipoRetencion' value='<?php echo $tipoRetencion ?>'>
								<input type="button" name="agregard" id="agregard" value="   Agregar   " onClick="agregardetalled()" ><input type="hidden" value="0" name="agregadetdes">
							</td>
						</tr>
      				</table>
					<div class="subpantallac3" style="height:22.5%; width:99.4%; overflow-x:hidden;">
						<table class="inicio" style="overflow:scroll">
							<?php
                            $fech1=explode("/",$_POST['fechaini']);
                            $fech2=explode("/",$_POST['fechafin']);
                            $f1=$fech1[2]."-".$fech1[1]."-".$fech1[0];
                            $f2=$fech2[2]."-".$fech2[1]."-".$fech2[0];

							if ($_POST['eliminad']!='')
							{
								//echo "<TR><TD>ENTROS :".$_POST[elimina]."</TD></TR>";
								$posi=$_POST['eliminad'];
								unset($_POST['ddescuentos'][$posi]);
								unset($_POST['dndescuentos'][$posi]);
								unset($_POST['dntipoRetencion'][$posi]);
								unset($_POST['dntipoRet'][$posi]);
								$_POST['ddescuentos']= array_values($_POST['ddescuentos']);
								$_POST['dndescuentos']= array_values($_POST['dndescuentos']);
								$_POST['dntipoRetencion']= array_values($_POST['dntipoRetencion']);
								$_POST['dntipoRet']= array_values($_POST['dntipoRet']);
							}
							if ($_POST['agregadetdes']=='1')
							{
								if($_POST['retencion']=='t')
								{
									//echo "DDDDD".count($_POST[retencion]);
									for($x=0;$x<count($reten);$x++)
									{
										// echo "f:".$k;
										if($rdestino[$x]==$_POST['tiporet'])
										{
											$_POST['ddescuentos'][]=$reten[$x];
											$_POST['dndescuentos'][]=$nreten[$x];
											$_POST['dntipoRetencion'][]=$_POST['tiporet'];
											$_POST['dntipoRet'][]=$_POST['tipoRetencion'];
										}
									}
									$_POST['agregadetdes']='0';
								}
								else
								{
									$_POST['ddescuentos'][]=$_POST['retencion'];
									$_POST['dndescuentos'][]=$_POST['nretencion'];
									$_POST['dntipoRetencion'][]=$_POST['tiporet'];
									$_POST['dntipoRet'][]=$_POST['tipoRetencion'];
									$_POST['agregadetdes']='0';
									?>
									<script>
										document.form2.porcentaje.value=0;
										document.form2.vporcentaje.value=0;
										document.form2.retencion.value='';
										document.form2.tiporet.value='';
										document.form2.tipoRetencion.value='';
									</script>
									<?php
								}
							}
							?>
							<tr>
								<td class="titulos">Retenciones e Ingresos</td>
								<td class="titulos2">
									<img src="imagenes/del.png" >
									<input type='hidden' name='eliminad' id='eliminad'>
								</td>
							</tr>
							<?php
							$totaldes=0;
							//echo "v:".$_POST[valor];
							for ($x=0;$x<count($_POST['ddescuentos']);$x++)
							{
								echo "<tr>
											<td class='saludo2'>
												<input name='dndescuentos[]' value='".$_POST['dndescuentos'][$x]."' type='text' size='100' readonly>
												<input name='ddescuentos[]' value='".$_POST['ddescuentos'][$x]."' type='hidden'>
												<input name='dntipoRetencion[]' value='".$_POST['dntipoRetencion'][$x]."' type='hidden'>
										<input name='dntipoRet[]' value='".$_POST['dntipoRet'][$x]."' type='hidden'>
											</td>";
								echo "<td class='saludo2'><a href='#' onclick='eliminard($x)'><img src='imagenes/del.png'></a></td></tr>";
							}
							$_POST['valorretencion']=$totaldes;

							?>
							<script>
								document.form2.totaldes.value=<?php echo $totaldes;?>;
								//	calcularpago();
								document.form2.valorretencion.value=<?php echo $totaldes;?>;
							</script>
							<?php
								$_POST['valorcheque']=$_POST['valoregreso']-$_POST['valorretencion'];
							?>
						</table>
						<?php
						//***** busca tercero
						if($_POST['bt']=='1')
						{
							$nresul=buscatercero($_POST['tercero']);
							if($nresul!='')
							{
								$_POST['ntercero']=$nresul;
								?>
								<script>
									document.getElementById('retencion').focus();document.getElementById('retencion').select();
								</script>
								<?php
							}
							else
							{
								$_POST['ntercero']="";
								?>
								<script>
									alert("Tercero Incorrecto o no Existe")
									document.form2.tercero.focus();
								</script>
								<?php
							}
						}
						?>
					</div>
	  				<div class="subpantallac" style="height:200px; width:99.4%; overflow-x:hidden;">
       					<table class="inicio" >
        					<tr>
								<td class="titulos">Retenciones / Ingresos</td>
								<td class="titulos">Tercero</td>
								<td class="titulos">Nombre Tercero</td>
								<td class="titulos">No Documento</td>
								<td class="titulos">Tipo Documento</td>
								<td class="titulos">Base</td>
								<td class="titulos">Valor</td>
							</tr>
      						<?php
							$_POST['mddescuentos']=array();
							$_POST['mnbancos']=array();
							$_POST['mctanbancos']=array();
							$_POST['mtdescuentos']=array();
							$_POST['mddesvalores']=array();
							$_POST['mddesvalores2']=array();
							$_POST['mdndescuentos']=array();
							$_POST['mdctas']=array();
							$_POST['mdtercero']=array();
							$_POST['mdnomtercero']=array();
							$totalpagar=0;

							preg_match("/([0-9]{2})\/([0-9]{2})\/([0-9]{4})/", $_POST["fecha"], $fecha);
							$fechaf=$fecha[3]."-".$fecha[2]."-".$fecha[1];
							//**** buscar movimientos
							for ($x=0;$x<count($_POST['ddescuentos']);$x++)
		 					{
								$tm=strlen($_POST['ddescuentos'][$x]);
								//********** RETENCIONES *********
								if(substr($_POST['ddescuentos'][$x],0,1)=='R')
		  						{
									if(substr($_POST['ddescuentos'][$x],2,$tm-2)=='34')
									{
										$sqlrete="SELECT SUM(valorretencion) FROM hum_retencionesfun WHERE tiporetencion='34' AND vigencia='$_POST[vigencias]' AND mes='$_POST[mes]' AND estadopago='N'";
										$resrete = mysqli_query($linkbd, $sqlrete);
										$rowrete =mysqli_fetch_row($resrete);
										if($rowrete[0] > 0){
											$vpor=100;
											$_POST['mtdescuentos'][]='R';
											$_POST['mddesvalores'][]=$rowrete[0]*($vpor/100);
											$_POST['mddesvalores2'][]=$rowrete[0]*($vpor/100);
											$_POST['mddescuentos'][]=35;
											$_POST['mdctas'][]=0;
											$_POST['mnbancos'][]='';
											$_POST['mdtercero'][]='';
											$_POST['mdnomtercero'][]='';
											$_POST['mctanbancos'][]="EGRESO NOMINA";
											$_POST['mdndescuentos'][]=buscaretencion(34);
											$totalpagar+=$rowrete[0]*($vpor/100);
										}

									}
									$query="SELECT conta_pago FROM tesoparametros";
									$resultado=mysqli_query($linkbd, $query);
									$arreglo=mysqli_fetch_row($resultado);
									$opcion=$arreglo[0];
									if($opcion=='1')
									{
										$sqlr="select tesoordenpago_retenciones.id_retencion, tesoordenpago_retenciones.valor,tesoordenpago.base,tesoordenpago.iva,tesoordenpago.id_orden,tesoordenpago.tercero from tesoordenpago, tesoordenpago_retenciones where tesoordenpago.fecha BETWEEN '$f1' AND '$f2' and tesoordenpago_retenciones.id_retencion='".substr($_POST['ddescuentos'][$x],2,$tm-2)."' and tesoordenpago.id_orden=tesoordenpago_retenciones.id_orden AND tesoordenpago.estado!='R' AND tesoordenpago.estado!='N' AND tesoordenpago.tipo_mov='201' order BY tesoordenpago.id_orden";
										$res=mysqli_query($linkbd, $sqlr);
										while($row = mysqli_fetch_row($res))
										{
											$crit ='';
											if($_POST['dntipoRet'][$x] == 'C')
											{
												$crit = "AND destino='".$_POST['dntipoRetencion'][$x]."' AND conceptoingreso!= '-1'";
											}
											$sqlr="select *from tesoretenciones_det where codigo='$row[0]' $crit ORDER BY porcentaje DESC";
											$rowRete = view($sqlr);
											$valorProcentaje = 0;
											$idRetencion = '';
											for($xx = 0; $xx<count($rowRete); $xx++)
											{
												if($idRetencion == $rowRete[$xx]['codigo']){
												$valorProcentaje = $val2 + $rowRete[$xx]['porcentaje'];
												}else{
													$valorProcentaje = $rowRete[$xx]['porcentaje'];
													$idRetencion = $rowRete[$xx]['codigo'];
												}
												$val2 = 0;
												$val2 = $rowRete[$xx]['porcentaje'];

												$rest = substr($rowRete[$xx]['tipoconce'],-2);
												$sq="select fechainicial from conceptoscontables_det where codigo='".$rowRete[$xx]['conceptoingreso']."' and modulo='".$rowRete[$xx]['modulo']."' and tipo='$rest' and fechainicial<'$fechaf' and cuenta!='' order by fechainicial asc";
												$re=mysqli_query($linkbd, $sq);
												while($ro=mysqli_fetch_assoc($re))
												{
													$_POST['fechacausa']=$ro["fechainicial"];
												}
												$sqlr="select * from conceptoscontables_det where codigo='".$rowRete[$xx]['conceptoingreso']."' and modulo='".$rowRete[$xx]['modulo']."' and tipo='$rest' and fechainicial='".$_POST['fechacausa']."'";
												$rst=mysqli_query($linkbd, $sqlr);
												$row1=mysqli_fetch_assoc($rst);
												if($_POST['dntipoRet'][$x] == 'C')
												{
													if($valorProcentaje <= 100){
														$_POST['mtdescuentos'][]='R';
														$_POST['mddesvalores'][]=$row[1]*($rowRete[$xx]['porcentaje']/100);
														$_POST['mddesvalores2'][]=$row[1]*($rowRete[$xx]['porcentaje']/100);
														$_POST['mddescuentos'][]=$row[0];
														$_POST['mdctas'][]=$row[2];
														$_POST['mnbancos'][]=$row[4];
														$_POST['mctanbancos'][]="CXP";
                                                        $_POST['mdtercero'][]=$row[5];
											            $_POST['mdnomtercero'][]=buscatercero($row[5]);
														$_POST['mdndescuentos'][]=buscaretencion($row[0]);
														$totalpagar+=$row[1]*($rowRete[$xx]['porcentaje']/100);
													}
												}
												else
												{
													$_POST['mtdescuentos'][]='R';
													$_POST['mddesvalores'][]=$row[1];
													$_POST['mddesvalores2'][]=$row[1];
													$_POST['mddescuentos'][]=$row[0];
													$_POST['mdctas'][]=$row[2];
													$_POST['mnbancos'][]=$row[4];
													$_POST['mctanbancos'][]="CXP";
                                                    $_POST['mdtercero'][]=$row[5];
											        $_POST['mdnomtercero'][]=buscatercero($row[5]);
													$_POST['mdndescuentos'][]=buscaretencion($row[0]);
													$totalpagar+=$row[1];
												}
											}
										}
									}
									else
									{
										$sqlr="select tesoordenpago_retenciones.id_retencion, tesoordenpago_retenciones.valor, tesoegresos.banco, tesoegresos.cuentabanco,tesoegresos.id_egreso,tesoordenpago.base,tesoordenpago.iva, tesoegresos.tercero from tesoordenpago, tesoordenpago_retenciones,tesoegresos where tesoegresos.id_orden=tesoordenpago.id_orden and tesoegresos.estado='S' AND tesoordenpago.estado!='N' and tesoegresos.fecha BETWEEN '$f1' AND '$f2' and tesoordenpago_retenciones.id_retencion='".substr($_POST['ddescuentos'][$x],2,$tm-2)."' and tesoordenpago.id_orden=tesoordenpago_retenciones.id_orden AND tesoegresos.ESTADO='S' AND tesoegresos.tipo_mov='201' AND tesoordenpago.tipo_mov='201' order BY tesoegresos.id_egreso";

										$res=mysqli_query($linkbd, $sqlr);
										//echo "<br> H1:".$sqlr;
										while($row = mysqli_fetch_row($res))
										{
											$crit ='';
											if($_POST['dntipoRet'][$x] == 'C')
											{
												$crit = "AND tesoretenciones_det.destino='".$_POST['dntipoRetencion'][$x]."'";
											}
											$sqlr="select *from tesoretenciones, tesoretenciones_det  where tesoretenciones_det.codigo='$row[0]' AND tesoretenciones.id=tesoretenciones_det.codigo $crit ORDER BY porcentaje DESC";
											$rowRete = view($sqlr);
											$valorProcentaje = 0;
											$idRetencion = '';
											for($xx = 0; $xx<count($rowRete); $xx++)
											{
												if($idRetencion == $rowRete[$xx]['codigo']){
													$valorProcentaje = $val2 + $rowRete[$xx]['porcentaje'];
												}else{
													$valorProcentaje = $rowRete[$xx]['porcentaje'];
													$idRetencion = $rowRete[$xx]['codigo'];
												}

												$val2 = 0;
												$val2 = $rowRete[$xx]['porcentaje'];

												if($rowRete[$xx]['tipo'] == 'C' && $rowRete[$xx]['destino'] != $_POST['dntipoRetencion'][$x])
												{
													continue;
												}
												$rest = substr($rowRete[$xx]['tipoconce'],-2);
												$sq="select fechainicial from conceptoscontables_det where codigo='".$rowRete[$xx]['conceptoingreso']."' and modulo='".$rowRete[$xx]['modulo']."' and tipo='$rest' and fechainicial<'$fechaf' and cuenta!='' order by fechainicial asc";
												$re=mysqli_query($linkbd, $sq);
												while($ro=mysqli_fetch_assoc($re))
												{
													$_POST['fechacausa']=$ro["fechainicial"];
												}
												$sqlr="select * from conceptoscontables_det where codigo='".$rowRete[$xx]['conceptoingreso']."' and modulo='".$rowRete[$xx]['modulo']."' and tipo='$rest' and fechainicial='".$_POST['fechacausa']."'";
												$rst=mysqli_query($linkbd, $sqlr);
												$row1=mysqli_fetch_assoc($rst);
												if($rowRete[$xx]['tipo'] == 'C')
												{
													if($valorProcentaje <= 100){
														$_POST['mtdescuentos'][]='R';
														$_POST['mddesvalores'][]=$row[1]*($rowRete[$xx]['porcentaje']/100);
														$_POST['mddesvalores2'][]=$row[1]*($rowRete[$xx]['porcentaje']/100);
														$_POST['mddescuentos'][]=$row[0];
														$_POST['mdctas'][]=$row[5];
														$_POST['mnbancos'][]=$row[4];
                                                        $_POST['mdtercero'][]=$row[7];
											            $_POST['mdnomtercero'][]=buscatercero($row[7]);
														$_POST['mctanbancos'][]="EGRESO";
														$_POST['mdndescuentos'][]=buscaretencion($row[0]);
														$totalpagar+=$row[1]*($rowRete[$xx]['porcentaje']/100);
													}
												}
												else
												{
													$_POST['mtdescuentos'][]='R';
													$_POST['mddesvalores'][]=$row[1];
													$_POST['mddesvalores2'][]=$row[1];
													$_POST['mddescuentos'][]=$row[0];
													$_POST['mdctas'][]=$row[5];
													$_POST['mnbancos'][]=$row[4];
													$_POST['mctanbancos'][]="EGRESO";
                                                    $_POST['mdtercero'][]=$row[7];
											        $_POST['mdnomtercero'][]=buscatercero($row[7]);
													$_POST['mdndescuentos'][]=buscaretencion($row[0]);
													$totalpagar+=$row[1];
												}
											}
										}
									}

									/* $sqlr="SELECT DISTINCT tesopagotercerosvigant_retenciones.id_retencion, sum(tesopagotercerosvigant_retenciones.valor), tesopagotercerosvigant.banco FROM tesopagotercerosvigant_retenciones, tesopagotercerosvigant where tesopagotercerosvigant.id_pago=tesopagotercerosvigant_retenciones.id_egreso and tesopagotercerosvigant.estado='S' and MONTH(tesopagotercerosvigant.fecha)='$_POST[mes]' AND YEAR(tesopagotercerosvigant.fecha)='".$_POST['vigencias']."' and tesopagotercerosvigant_retenciones.id_retencion='".substr($_POST['ddescuentos'][$x],2,$tm-2)."' GROUP BY tesopagotercerosvigant.banco"; */

									$sqlr="SELECT tesopagotercerosvigant_retenciones.id_retencion, tesopagotercerosvigant_retenciones.valor, tesopagotercerosvigant.banco, tesopagotercerosvigant.id_pago,tesopagotercerosvigant_retenciones.valor, tesopagotercerosvigant.tercero FROM tesopagotercerosvigant_retenciones, tesopagotercerosvigant WHERE tesopagotercerosvigant.id_pago=tesopagotercerosvigant_retenciones.id_egreso AND tesopagotercerosvigant.estado='S' AND tesopagotercerosvigant.fecha BETWEEN '$f1' AND '$f2' AND tesopagotercerosvigant_retenciones.id_retencion='".substr($_POST['ddescuentos'][$x],2,$tm-2)."' order BY tesopagotercerosvigant.id_pago";

									$res=mysqli_query($linkbd, $sqlr);
									//echo "<br> H1:".$sqlr;
									while($row = mysqli_fetch_row($res))
									{
										$crit ='';
										if($_POST['dntipoRet'][$x] == 'C')
										{
											$crit = "AND tesoretenciones_det.destino='".$_POST['dntipoRetencion'][$x]."'";
										}
										$sqlr="select *from tesoretenciones, tesoretenciones_det  where tesoretenciones_det.codigo='$row[0]' AND tesoretenciones.id=tesoretenciones_det.codigo $crit ORDER BY porcentaje DESC";
										$rowRete = view($sqlr);
										$valorProcentaje = 0;
										$idRetencion = '';
										for($xx = 0; $xx<count($rowRete); $xx++)
										{
											if($idRetencion == $rowRete[$xx]['codigo']){
												$valorProcentaje = $val2 + $rowRete[$xx]['porcentaje'];
											}else{
												$valorProcentaje = $rowRete[$xx]['porcentaje'];
												$idRetencion = $rowRete[$xx]['codigo'];
											}

											$val2 = 0;
											$val2 = $rowRete[$xx]['porcentaje'];

											if($rowRete[$xx]['tipo'] == 'C' && $rowRete[$xx]['destino'] != $_POST['dntipoRetencion'][$x])
											{
												continue;
											}
											$rest = substr($rowRete[$xx]['tipoconce'],-2);
											$sq="select fechainicial from conceptoscontables_det where codigo='".$rowRete[$xx]['conceptoingreso']."' and modulo='".$rowRete[$xx]['modulo']."' and tipo='$rest' and fechainicial<'$fechaf' and cuenta!='' order by fechainicial asc";
											$re=mysqli_query($linkbd, $sq);
											while($ro=mysqli_fetch_assoc($re))
											{
												$_POST['fechacausa']=$ro["fechainicial"];
											}
											$sqlr="select * from conceptoscontables_det where codigo='".$rowRete[$xx]['conceptoingreso']."' and modulo='".$rowRete[$xx]['modulo']."' and tipo='$rest' and fechainicial='".$_POST['fechacausa']."'";
											$rst=mysqli_query($linkbd, $sqlr);
											$row1=mysqli_fetch_assoc($rst);
											if($rowRete[$xx]['tipo'] == 'C')
											{
												if($valorProcentaje <= 100){
													$_POST['mtdescuentos'][]='R';
													$_POST['mddesvalores'][]=$row[1]*($rowRete[$xx]['porcentaje']/100);
													$_POST['mddesvalores2'][]=$row[1]*($rowRete[$xx]['porcentaje']/100);
													$_POST['mddescuentos'][]=$row[0];
													$_POST['mdctas'][]=$row[4];
													$_POST['mnbancos'][]=$row[3];
													$_POST['mctanbancos'][]="RETENCION DE OTROS EGRESOS";
                                                    $_POST['mdtercero'][]=$row[5];
											        $_POST['mdnomtercero'][]=buscatercero($row[5]);
													$_POST['mdndescuentos'][]=buscaretencion($row[0]);
													$totalpagar+=$row[1]*($rowRete[$xx]['porcentaje']/100);
												}
											}
											else
											{
												$_POST['mtdescuentos'][]='R';
												$_POST['mddesvalores'][]=$row[1];
												$_POST['mddesvalores2'][]=$row[1];
												$_POST['mddescuentos'][]=$row[0];
												$_POST['mdctas'][]=$row[4];
												$_POST['mnbancos'][]=$row[3];
												$_POST['mctanbancos'][]="RETENCION DE OTROS EGRESOS";
                                                $_POST['mdtercero'][]=$row[5];
											    $_POST['mdnomtercero'][]=buscatercero($row[5]);
												$_POST['mdndescuentos'][]=buscaretencion($row[0]);
												$totalpagar+=$row[1];
											}
										}
									}
								}
								//****** INGRESOS *******
								preg_match("/([0-9]{2})\/([0-9]{2})\/([0-9]{4})/", $_POST["fecha"], $fecha);
								$fechaf1=$fecha[3]."-".$fecha[2]."-".$fecha[1];
								if(substr($_POST['ddescuentos'][$x],0,1)=='I')
		  						{
		  							$sqlr="select tesorecaudos_det.ingreso, tesorecaudos_det.valor, tesoreciboscaja.cuentabanco,tesoreciboscaja.cuentacaja, tesoreciboscaja.id_recibos, tesorecaudos.id_recaudo, tesorecaudos.tercero from tesorecaudos, tesorecaudos_det,tesoreciboscaja where tesorecaudos.estado='P' and tesoreciboscaja.fecha BETWEEN '$f1' AND '$f2' and tesorecaudos_det.ingreso='".substr($_POST['ddescuentos'][$x],2,$tm-2)."' and tesorecaudos.id_recaudo=tesorecaudos_det.id_recaudo and tesorecaudos.id_recaudo=tesoreciboscaja.id_recaudo and tesoreciboscaja.tipo=3";
		 			 				//$sqlr="select tesoreciboscaja_det.ingreso, tesoreciboscaja_det.valor, tesoreciboscaja.cuentabanco,tesoreciboscaja.cuentacaja, tesoreciboscaja.valor,tesoreciboscaja.ID_RECIBOS from  tesoreciboscaja_det,tesoreciboscaja where tesoreciboscaja.estado='S' and MONTH(tesoreciboscaja.fecha)='$_POST[mes]' and YEAR(tesoreciboscaja.fecha)='".$_POST[vigencias]."' and tesoreciboscaja_det.ingreso='".substr($_POST[ddescuentos][$x],2,$tm-2)."' and tesoreciboscaja_det.id_recibos=tesoreciboscaja.id_recibos AND tesoreciboscaja.ESTADO='S'";
		 							// $sqlr="";
		 							$res=mysqli_query($linkbd, $sqlr);
									//echo "<br> H2: ".$sqlr;
									while ($row =mysqli_fetch_row($res))
	    							{
		 								$sqlr="select *from  tesoingresos_det where codigo='$row[0]' and vigencia=(SELECT MAX(vigencia) FROM tesoingresos_det WHERE codigo = '$row[0]')";
		  								$res2=mysqli_query($linkbd, $sqlr);
		  								//echo "$row[0] - ".$sqlr;
		  								while($row2 =mysqli_fetch_row($res2))
		   								{
											$sq="select fechainicial from conceptoscontables_det where codigo='$row2[2]' and modulo='4' and tipo='C' and fechainicial<'$fechaf1' and cuenta!='' order by fechainicial asc";
											//echo $sq;
											$re=mysqli_query($linkbd, $sq);
											while($ro=mysqli_fetch_assoc($re))
											{
												$_POST['fechacausa']=$ro["fechainicial"];
											}
		   									$sqlr="select * from  conceptoscontables_det where codigo='$row2[2]' and modulo='4' and tipo='C' and fechainicial='".$_POST['fechacausa']."'";
		  									$res3=mysqli_query($linkbd, $sqlr);
		 									// echo "$row2[1] - ".$sqlr;
		   									while($row3 =mysqli_fetch_row($res3))
		   									{
												if(substr($row3[4],0,1)=='2')
		    									{
													$vpor=$row2[5];
													$_POST['mtdescuentos'][]='I';
													$_POST['mddesvalores'][]=$row[1]*($vpor/100);
													$_POST['mddesvalores2'][]=$row[1]*($vpor/100);
													$_POST['mddescuentos'][]=$row[0];
													$_POST['mdctas'][]=$row[1];
													$_POST['mdndescuentos'][]=buscaingreso($row[0]);
													if($row[2]!='')
													{
		  												// $_POST[mnbancos][]=buscatercero(buscabanco($row[2]));
														$_POST['mnbancos'][]=$row[4];
														$_POST['mctanbancos'][]=$row[5];
													}
													if($row[2]=='')
													{
														//$_POST[mnbancos][]=buscacuenta($row[3]);
		    											$_POST['mnbancos'][]=$row[4];
														$_POST['mctanbancos'][]=$row[5];
													}
		   											$totalpagar+=$row[1]*($vpor/100);

                                                    $_POST['mdtercero'][]=$row[6];
                                                    $_POST['mdnomtercero'][]=buscatercero($row[6]);
													//$nv=buscaingreso($row[0]);
													//echo "ing:$nv";
												}
		   									}
		  								}
		 							}
		 							//*****INGRESOS PROPIOS
		 							$sqlr="select distinct tesosinreciboscaja_det.ingreso, sum(tesosinreciboscaja_det.valor), tesosinreciboscaja.cuentabanco,tesosinreciboscaja.cuentacaja, tesosinreciboscaja.valor ,tesosinreciboscaja.id_recibos, tesosinrecaudos.tercero from  tesosinreciboscaja_det,tesosinreciboscaja, tesosinrecaudos where tesosinreciboscaja.estado='S' and tesosinreciboscaja.fecha BETWEEN '$f1' AND '$f2' and tesosinreciboscaja_det.ingreso='".substr($_POST['ddescuentos'][$x],2,$tm-2)."' and tesosinreciboscaja_det.id_recibos=tesosinreciboscaja.id_recibos and tesosinreciboscaja.id_recaudo=tesosinrecaudos.id_recaudo GROUP BY tesosinreciboscaja.id_recibos";
		 							$res=mysqli_query($linkbd, $sqlr);
									//echo "<br> H3: ".$sqlr;
									while ($row =mysqli_fetch_row($res))
									{
		 								$sqlr="select *from  tesoingresos_det where codigo='$row[0]' and vigencia=(SELECT MAX(vigencia) FROM tesoingresos_det WHERE codigo = '$row[0]')";
		  								$res2=mysqli_query($linkbd, $sqlr);
		 	 							//echo "$row[0] - ".$sqlr;
		  								while($row2 =mysqli_fetch_row($res2))
		   								{
											$sq="select fechainicial from conceptoscontables_det where codigo='$row2[2]' and modulo='4' and tipo='C' and fechainicial<'$fechaf1' and cuenta!='' order by fechainicial asc";
											//echo $sq;
											$re=mysqli_query($linkbd, $sq);
											while($ro=mysqli_fetch_assoc($re))
											{
												$_POST['fechacausa']=$ro["fechainicial"];
											}
		   									$sqlr="select *from  conceptoscontables_det where codigo='$row2[2]' and modulo='4' and tipo='C' and fechainicial='".$_POST['fechacausa']."' group by cuenta";
		  									$res3=mysqli_query($linkbd, $sqlr);
		 									// echo "$row2[1] - ".$sqlr;
		   									while($row3 =mysqli_fetch_row($res3))
		   									{
												if(substr($row3[4],0,1)=='2' || substr($row3[4],0,1)=='4' || substr($row3[4],0,1) == 3 || substr($row3[4],0,1) == 1)
												{
													$vpor=$row2[5];
													$_POST['mtdescuentos'][]='I';
													$_POST['mddesvalores'][]=$row[1]*($vpor/100);
													$_POST['mddesvalores2'][]=$row[1]*($vpor/100);
													$_POST['mddescuentos'][]=$row[0];
													$_POST['mdctas'][]=$row[4];
													$_POST['mdndescuentos'][]=buscaingreso($row[0]);
                                                    $_POST['mnbancos'][]=$row[5];
                                                    $_POST['mdtercero'][]=$row[6];
                                                    $_POST['mdnomtercero'][]=buscatercero($row[6]);
													$totalpagar+=$row[1]*($vpor/100);
													break;
													//$nv=buscaingreso($row[0]);
													//echo "ing:$nv";
												}
		   									}
		  								}
		 							}
		 							//****** RECAUDO TRANSFERENCIAS *******
									$sqlr="select distinct tesorecaudotransferencia_det.ingreso, sum(tesorecaudotransferencia_det.valor), tesorecaudotransferencia.banco,tesorecaudotransferencia.ncuentaban,tesorecaudotransferencia.valortotal,tesorecaudotransferencia.id_recaudo, tesorecaudotransferencia.tercero from  tesorecaudotransferencia_det,tesorecaudotransferencia where tesorecaudotransferencia.estado='S' and tesorecaudotransferencia.fecha BETWEEN '$f1' AND '$f2' and tesorecaudotransferencia_det.ingreso='".substr($_POST['ddescuentos'][$x],2,$tm-2)."' and tesorecaudotransferencia_det.id_recaudo=tesorecaudotransferencia.id_recaudo group by tesorecaudotransferencia_det.ingreso";
		 							$res=mysqli_query($linkbd, $sqlr);
									//echo "<br> H4: ".$sqlr;
									while ($row =mysqli_fetch_row($res))
	    							{
		 								$sqlr="select *from  tesoingresos_det where codigo='$row[0]' and vigencia=(SELECT MAX(vigencia) FROM tesoingresos_det WHERE codigo = '$row[0]')";
										$res2=mysqli_query($linkbd, $sqlr);
										//echo "$row[0] - ".$sqlr;
										while($row2 =mysqli_fetch_row($res2))
										{
											$sq="select fechainicial from conceptoscontables_det where codigo='$row2[2]' and modulo='4' and tipo='C' and fechainicial<'$fechaf1' and cuenta!='' order by fechainicial asc";
											//echo $sq;
											$re=mysqli_query($linkbd, $sq);
											while($ro=mysqli_fetch_assoc($re))
											{
												$_POST['fechacausa']=$ro["fechainicial"];
											}
											$sqlr="select *from  conceptoscontables_det where codigo='$row2[2]' and modulo='4' and tipo='C' and fechainicial='".$_POST['fechacausa']."'";
											$res3=mysqli_query($linkbd, $sqlr);
											// echo "$row2[1] - ".$sqlr;
		   									while($row3 =mysqli_fetch_row($res3))
		   									{
												if(substr($row3[4],0,1)=='2')
												{
													$vpor=$row2[5];
													$_POST['mtdescuentos'][]='I';
													$_POST['mddesvalores'][]=$row[1]*($vpor/100);
													$_POST['mddesvalores2'][]=$row[1]*($vpor/100);
													$_POST['mddescuentos'][]=$row[0];
													$_POST['mdctas'][]=$row[4];
													$_POST['mnbancos'][]=$row[5];
													$_POST['mdndescuentos'][]=buscaingreso($row[0]);
													$totalpagar+=$row[1]*($vpor/100);
                                                    $_POST['mdtercero'][]=$row[6];
                                                    $_POST['mdnomtercero'][]=buscatercero($row[6]);
													//$nv=buscaingreso($row[0]);
													//echo "ing:$nv";
												}
											}
		  								}
		 							}
								}
								//********************************
							}
							// echo "<br>c...".count($_POST[mddescuentos]);
							$iter="zebra1";
							$iter2="zebra2";
							$retencion = $_POST['mdndescuentos'][0];
							$acumulador = 0;
                            $baseTotalRete = 0;
							for ($x=0;$x<count($_POST['mddescuentos']);$x++)
		 					{
								// echo "<br>".count($_POST[mddescuentos]);
								if($retencion != $_POST['mdndescuentos'][$x]){
									$ncheck='check'.$x;
									echo "<tr style = 'background-color: #03fccf; font-size: 14px; padding: 4px 6px;'>
											<td colspan = '5' >
												Total de retencion/ingreso ".$retencion."
											</td>
                                            <td>
                                                ".number_format(round($baseTotalRete,0),0)."
                                            </td>
											<td>
												".number_format(round($acumulador,0),0)."
											</td>
											<td></td>
										</tr>";
										$acumulador = 0;
                                        $baseTotalRete = 0;
								}

								echo "
									<tr class=$iter>
										<td >
											<input name='mdndescuentos[]' value='".$_POST['mdndescuentos'][$x]."' type='text' size='60' readonly>
											<input name='mddescuentos[]' value='".$_POST['mddescuentos'][$x]."' type='hidden'>
											<input name='mtdescuentos[]' value='".$_POST['mtdescuentos'][$x]."' type='hidden'>


										</td>
                                        <td>
                                            <input name='mdtercero[]' value='".$_POST['mdtercero'][$x]."'  type='text' size='10' readonly>
                                        </td>
                                        <td>
                                            <input name='mdnomtercero[]' value='".$_POST['mdnomtercero'][$x]."'  type='text' size='45' readonly>
                                        </td>
										<td><input name='mnbancos[]' value='".$_POST['mnbancos'][$x]."' type='text' size='10' readonly></td>

										<td><input name='mctanbancos[]' value='".$_POST['mctanbancos'][$x]."' type='text' size='35' readonly></td>
										<td>
											<input name='mdctas[]' value='".$_POST['mdctas'][$x]."' type='hidden' size='15' readonly class='inpnovisibles'>
											<input name='mdctas2[]' value='".number_format($_POST['mdctas'][$x],0)."' type='text' size='15' readonly class='inpnovisibles'>
										</td>
										<td>
											<input name='mddesvalores[]' value='".round($_POST['mddesvalores'][$x],0)."' type='hidden'>
											<input name='mddesvalores2[]' value='".number_format($_POST['mddesvalores2'][$x],0)."' type='text' size='15' readonly>
										</td>
									</tr>";
                                $retencion = $_POST['mdndescuentos'][$x];
								$acumulador += round($_POST['mddesvalores'][$x],0);
                                $baseTotalRete += round($_POST['mdctas'][$x],0);
								if($x == count($_POST['mdndescuentos']) - 1){
									$ncheck='check'.$x;
									echo "<tr style = 'background-color: #03fccf; font-size: 14px; padding: 4px 6px;'>
											<td colspan = '5' >
												Total de retencion/ingreso ".$retencion."
											</td>
                                            <td>
                                                ".number_format(round($baseTotalRete,0),0)."
                                            </td>
											<td>
												".number_format(round($acumulador,0),0)."
											</td>
											<td></td>
										</tr>";
										$acumulador = 0;
                                        $baseTotalRete = 0;
								}

								$aux=$iter;
								$iter=$iter2;
								$iter2=$aux;
		 					}
							$resultado = convertir($totalpagar);
							$_POST['letras']=$resultado." PESOS M/CTE";
		 					echo "<tr><td></td><td></td><td></td><td></td><td>Total:</td><td><input type='text' name='totalpagob' value='".number_format(array_sum($_POST['mdctas']),0)."' size='15' readonly class='inpnovisibles'></td><td><input type='hidden' name='totalpago2' value='".round($totalpagar,0)."' ><input type='text' name='totalpago' value='".number_format($totalpagar,0)."' size='15' readonly></td></tr>";
		 					echo "<tr><td colspan='3'><input name='letras' type='text' value='$_POST[letras]' size='150' ></td>";
							?>
        					<script>
        						document.form2.valorpagar.value=<?php echo round($totalpagar,0);?>;
								//calcularpago();
							</script>
        				</table>
	   				</div>
					<?php
					//echo "oculto".$_POST[oculto];
					if($_POST['oculto']=='2')
					{
						//**verificacion de guardado anteriormente *****
						$sqlr="select count(*) from tesopagoterceros where id_pago=$_POST[idcomp] ";
						$res=mysqli_query($linkbd, $sqlr);
						//echo $sqlr;
						while($r=mysqli_fetch_row($res))
						{
							$numerorecaudos=$r[0];
						}
						if($numerorecaudos==0)
	 					{
							preg_match("/([0-9]{2})\/([0-9]{2})\/([0-9]{4})/", $_POST["fecha"], $fecha);
							$fechaf=$fecha[3]."-".$fecha[2]."-".$fecha[1];
							//************CREACION DEL COMPROBANTE CONTABLE ************************
							$sqlr="insert into tesopagoterceros (`id_pago`, `tercero`, `banco`, `cheque`, `transferencia`, `valor`, `mes`, `concepto`, `cc`, `estado`,fecha) values ($_POST[idcomp],'$_POST[tercero]','$_POST[banco]', '$_POST[ncheque]','$_POST[ntransfe]',$totalpagar,'$_POST[mes]' , '$_POST[concepto]','$_POST[cc]','S','$fechaf')";
							mysqli_query($linkbd, $sqlr);
							//echo "$sqlr <br>";
							//***busca el consecutivo del comprobante contable
							$sqlr="insert into comprobante_cab (numerotipo,tipo_comp,fecha,concepto,total,total_debito,total_credito,diferencia,estado) values ($_POST[idcomp] ,12,'$fechaf','$_POST[concepto]',0,$totalpagar,$totalpagar,0,'1')";
							mysqli_query($linkbd, $sqlr);
							//echo "<br>C:".count($_POST[mddescuentos]);
							for ($x=0;$x<count($_POST['mddescuentos']);$x++)
							{
								$sqlr="insert into comprobante_det (id_comp,cuenta,tercero,centrocosto,detalle,cheque,valdebito,valcredito,estado,vigencia) values ('12 $_POST[idcomp]','".$_POST['mdctas'][$x]."','".$_POST['tercero']."','".$_POST['cc']."','PAGO RECAUDO A TERCERO MES ".$meses[$_POST['mes']]."','$_POST[ncheque]$_POST[ntransfe]',".$_POST['mddesvalores'][$x].",0,'1','".$vigusu."')";
								//	echo "$sqlr <br>";
								mysqli_query($linkbd, $sqlr);

								//*** Cuenta BANCO **
								$sqlr="insert into comprobante_det (id_comp,cuenta,tercero,centrocosto,detalle,cheque,valdebito,valcredito,estado,vigencia) values ('12 $_POST[idcomp]','".$_POST['banco']."','".$_POST['tercero']."','".$_POST['cc']."','PAGO RECAUDO A TERCERO MES ".$meses[$_POST['mes']]."','$_POST[ncheque]$_POST[ntransfe]',0,".$_POST['mddesvalores'][$x].",'1','".$vigusu."')";
								mysqli_query($linkbd, $sqlr);
								//echo "$sqlr <br>";

								$sqlr="insert into tesopagoterceros_det(`id_pago`, `movimiento`, `tipo`, `valor`, `cuenta`, `estado`) values ($_POST[idcomp],'".$_POST['mddescuentos'][$x]."','".$_POST['mtdescuentos'][$x]."',".$_POST['mddesvalores'][$x].",'".$_POST['mdctas'][$x]."','S')";
								mysqli_query($linkbd, $sqlr);
							}
							echo "<table class='inicio'><tr><td class='saludo1'><center>Se ha almacenado el Recaudo a Terceros con Exito <img src='imagenes/confirm.png'><script>pdf()</script></center></td></tr></table>";
	 					}//*** if de guardado
	 					else
	  					{
							echo "<table class='inicio'><tr><td class='saludo1'><center><img src='imagenes/alert.png'>Ya Se ha almacenado un documento con ese consecutivo</center></td></tr></table>";
	  					}
					}
					?>
				</form>
 			</td>
		</tr>
	</body>
</html>
