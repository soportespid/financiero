<?php

    header("Cache-control: no-cache, no-store, must-revalidate");
    header("Content-Type: text/html;charset=utf8");

    require 'comun.inc';
    require 'funciones.inc';

    session_start();
    date_default_timezone_set("America/Bogota");

?>
<html lang="es">
    <head>
        <meta charset="utf-8"/>
		<meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1"/>
		<meta name="viewport" content="user-scalable=no">
        <title>:: IDEAL 10 - Presupuesto</title>
        <link href="favicon.ico" rel="shortcut icon"/>
        <link href="css/css2.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
        <link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
        <link href="css/css4.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
        <script type="text/javascript" src="css/programas.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
        <script type="text/javascript" src="css/calendario.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script type="text/javascript" src="JQuery/jquery-2.1.4.min.js"></script>
        <link href="css/style.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />

        <script>
            function ocultarTabla(){
                app.mostrarEjecucion = false;
            }
        </script>
        <style>
            [v-cloak]{
                display : none;
            }

            /* label{
                font-size:13px;
            }

            input{
                height: calc(1em + 0.6rem + 0.5px) !important;
                font-size: 14px !important;
                margin-top: 4px !important;
            }



            .ancho{
                width: 60px;
            }

             */

            label{
                font-size:13px;
            }

            input{
                height: calc(1em + 0.6rem + 0.5px) !important;
                font-size: 14px !important;
                margin-top: 4px !important;
            }

            .captura{
                font-weight: normal;
                cursor: pointer !important;
            }

            .captura:hover{
                background: linear-gradient(#40f3ff , #40b3ff 70%, #40f3ff );
            }

            .colorFila{
                background-color: #F0FFFD !important;
            }

            .colorFila1{
                background-color: #FFF02A !important;
                font-family: -apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, "Helvetica Neue", Arial, "Noto Sans", sans-serif, "Apple Color Emoji", "Segoe UI Emoji", "Segoe UI Symbol", "Noto Color Emoji";
                font-size:10px;
                font-weight:normal;
                border:#eeeeee 1px solid;
                padding-left:3px;
                padding-right:3px;
                margin-bottom:1px;
                margin-top:1px;
                height:23.5px;
                border-radius: 1px;
            }

            .colorFila1:hover{
                background: linear-gradient(#40f3ff , #40b3ff 70%, #40f3ff );
            }

            .agregado{
                font-weight: 700;
            }

            .dobleclickCelda:hover{
                color: brown;
            }

            .sumTotal{
                /* background-color: #8bd1a9; */
                background-color: #31ada1;
                font-weight: 700;
                color: black;
            }
            .sumTotalFuncionamiento{
                background-color: #8bd1a9;
                /* background-color: #59d999; */
                font-weight: 700;
                color: black;
            }

        </style>
    </head>
    <body>
        <IFRAME src="alertas.php" name="alertas" id="alertas" style="display:none"></IFRAME>
        <span id="todastablas2"></span>
        <header>
			<table>
				<tr><script>barra_imagenes("ccpet");</script><?php cuadro_titulos();?></tr>
			</table>
		</header>
        <form name="form2" method="post" action="">
            <section id="myapp" v-cloak >
                <nav>
                    <table>
                        <tr><?php menu_desplegable("ccpet");?></tr>
                    </table>
                    <div class="bg-white group-btn p-1" id="newNavStyle">
                        <button type="button" class="btn btn-white btn-primary-hover d-flex justify-between align-items-center" onclick="mypop=window.open('ccp-principal.php','',''); mypop.focus();">
                            <span>Nueva ventana</span>
                            <svg class="fill-black group-hover:fill-white w-5 h-5" xmlns="http://www.w3.org/2000/svg"  viewBox="0 -960 960 960" ><path d="M320-120v-80h80v-80H160q-33 0-56.5-23.5T80-360v-400q0-33 23.5-56.5T160-840h640q33 0 56.5 23.5T880-760v400q0 33-23.5 56.5T800-280H560v80h80v80H320ZM160-360h640v-400H160v400Zm0 0v-400 400Z"/></svg>
                        </button>
                        <button type="button" @click="downloadExl" class="btn btn-success btn-success-hover d-flex justify-between align-items-center">
                            <span>Exportar Excel</span>
                            <svg class="fill-black group-hover:fill-white w-4 h-4" xmlns="http://www.w3.org/2000/svg"  viewBox="0 0 384 512"><!--!Font Awesome Free 6.5.1 by @fontawesome - https://fontawesome.com License - https://fontawesome.com/license/free Copyright 2024 Fonticons, Inc. --><path d="M48 448V64c0-8.8 7.2-16 16-16H224v80c0 17.7 14.3 32 32 32h80V448c0 8.8-7.2 16-16 16H64c-8.8 0-16-7.2-16-16zM64 0C28.7 0 0 28.7 0 64V448c0 35.3 28.7 64 64 64H320c35.3 0 64-28.7 64-64V154.5c0-17-6.7-33.3-18.7-45.3L274.7 18.7C262.7 6.7 246.5 0 229.5 0H64zm90.9 233.3c-8.1-10.5-23.2-12.3-33.7-4.2s-12.3 23.2-4.2 33.7L161.6 320l-44.5 57.3c-8.1 10.5-6.3 25.5 4.2 33.7s25.5 6.3 33.7-4.2L192 359.1l37.1 47.6c8.1 10.5 23.2 12.3 33.7 4.2s12.3-23.2 4.2-33.7L222.4 320l44.5-57.3c8.1-10.5 6.3-25.5-4.2-33.7s-25.5-6.3-33.7 4.2L192 280.9l-37.1-47.6z"/></svg>
                        </button>
                        <button type="button" onclick="window.location.href='ccp-ejecucionpresupuestal.php'" class="btn btn-success btn-success-hover d-flex justify-between align-items-center">
                            <span>Atras</span>
                            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 -960 960 960"><path d="M760-200v-160q0-50-35-85t-85-35H273l144 144-57 56-240-240 240-240 57 56-144 144h367q83 0 141.5 58.5T840-360v160h-80Z"/></svg>
                        </button>
                    </div>
                </nav>
                <article>
                    <table class="inicio ancho">
                        <tr>
                            <td class="titulos" colspan="10" style="text-align:center; font-weight: 600;">.: Ejecuci&oacute;n presupuestal de gastos - General</td>
                            <td class="cerrar" style="width:7%" onClick="location.href='ccp-principal.php'">Cerrar</td>
                        </tr>
                        <tr>
                            <td style = "width: 10%;">
                                <label class="labelR">Sección</label>
                            </td>
                            <td style = "width: 10%;">
                                <select style = "width: 90%;" v-model="selectUnidad" v-on:change="seccionPresupuestal">
                                    <option value="-1">Todas</option>
                                    <option v-for="unidad in unidadesEjecutoras" v-bind:value="unidad[0]">
                                        {{ unidad[1] }} - {{ unidad[3] }}
                                    </option>
                                </select>
                            </td>
                            <td style = "width: 10%;">
                                <label class="labelR">Dependencia:</label>
                            </td>
                            <td style = "width: 10%;">
                                <select style = "width: 90%;" v-model="selectSeccion" v-on:change="cambiaCriteriosBusqueda">
                                    <option value="-1">Todas</option>
                                    <option v-for="seccion in seccionesPresupuestales" v-bind:value="seccion[0]">
                                        {{ seccion[0] }} - {{ seccion[1] }}
                                    </option>
                                </select>
                            </td>
                            <td style = "width: 10%;">
                                <label class="labelR">Situción/Fondo:</label>
                            </td>
                            <td style = "width: 10%;">
                                <select style = "width: 90%;" v-model="selectMedioPago" v-on:change="cambiaCriteriosBusqueda">
                                    <option v-for="option in optionsMediosPagos" v-bind:value="option.value">
                                        {{ option.text }}
                                    </option>
                                </select>
                            </td>

                            <td style = "width: 8%;">
                                <label class="labelR" for="">Fuente:</label>
                            </td>
                            <td colspan="4">
                                <div>
                                    <multiselect
                                        v-model="valueFuentes"
                                        placeholder="Seleccione una o varias fuentes"
                                        label="fuente" track-by="fuente"
                                        :custom-label="fuenteConNombre"
                                        :options="optionsFuentes"
                                        :multiple="true"
                                        :taggable="false"
                                        @input = "cambiaCriteriosBusqueda"
                                        @open="cambiaCriteriosBusqueda"
                                    ></multiselect>
                                </div>
                                <!-- <select style="width:90%" v-model="vigencia" v-on:Change="cambiaCriteriosBusqueda">
                                    <option v-for="year in years" :value="year[0]">{{ year[0] }}</option>
                                </select> -->
                            </td>

                        </tr>
                        <tr>
                            <td>
                                <label class="labelR">
                                    Fecha inicial:
                                </label>
                            </td>
                            <td>
                                <input type="text" style = "width: 90%;" name="fechaini" value="<?php echo $_POST['fechaini']?>" onKeyUp="return tabular(event,this)" id="fc_1198971545" title="DD/MM/YYYY" onDblClick="displayCalendarFor('fc_1198971545');" class="colordobleclik" autocomplete="off" onChange="" readonly>
                            </td>
                            <td>
                                <label class="labelR">
                                    Fecha final:
                                </label>
                            </td>
                            <td>
                                <input type="text" style = "width: 90%;" name="fechafin" value="<?php echo $_POST['fechafin']?>" onKeyUp="return tabular(event,this)" id="fc_1198971546" title="DD/MM/YYYY" onDblClick="displayCalendarFor('fc_1198971546');" class="colordobleclik" autocomplete="off" onChange="" readonly>
                            </td>

                            <td>
                                <label class="labelR">Vig. gasto:</label>
                            </td>
                            <td>
                                <select style = "width: 90%;" v-model="selectVigenciaGasto" v-on:change="cambiaCriteriosBusqueda">
                                    <option value="-1">Todas</option>
                                    <option v-for="vigenciaDeGasto in vigenciasdelgasto" v-bind:value="vigenciaDeGasto[0]">
                                        {{ vigenciaDeGasto[1] }} - {{ vigenciaDeGasto[2] }}
                                    </option>
                                </select>
                            </td>

                            <td>
                                <label class="labelR">Clasificador:</label>
                            </td>
                            <td>
                                <select style = "width: 90%;" v-model="selectClasificador" v-on:change="cuentasDelClasificador">
                                    <option value="-1">Ninguno...</option>
                                    <option v-for="clasificador in clasificadores" v-bind:value="clasificador[0]">
                                        {{ clasificador[1] }}
                                    </option>
                                </select>
                            </td>

                            <td>
                                <label class="labelR">Tipo:</label>
                            </td>
                            <td>
                                <select style = "width: 90%;" v-model="tipoGasto" v-on:change="cambiaCriteriosBusqueda">
                                    <option value="-1">Todos</option>
                                    <option v-for="tipoDeGasto in tiposDeGasto" v-bind:value="tipoDeGasto[2]">
                                        {{ tipoDeGasto[1] }}
                                    </option>
                                </select>
                            </td>

                            <td>
                                <button type="button" class="botonflechaverde" v-on:click="generarEjecucion">Generar</button>
                            </td>
                        </tr>
                    </table>
                    <div class='subpantalla estilos-scroll' v-show="mostrarEjecucion" style='height:54vh; margin-top:0px; resize: vertical;'>
                        <table class='tabla-ejecucion' id="tableId">
                            <thead>
                                <tr style="text-align:Center;">
                                    <th class="titulosnew00" style="width:2%;">Sección</th>
                                    <th class="titulosnew00" style="width:2%;">Dependencia</th>
                                    <th class="titulosnew00" style="width:2%;">Vig. gasto</th>
                                    <th class="titulosnew00" style="width:5%;">Rubro</th>
                                    <th class="titulosnew00">Nombre</th>
                                    <th class="titulosnew00" style="width:2%;">Tipo</th>
                                    <th class="titulosnew00" style="width:4%;">Fuente</th>
                                    <th class="titulosnew00" style="width:8%;">Nombre Fuente</th>
                                    <th class="titulosnew00" style="width:3%;">CSF</th>
                                    <th class="titulosnew00" style="width:6%;">Inicial</th>
                                    <th class="titulosnew00" style="width:6%;">Adici&oacute;n</th>
                                    <th class="titulosnew00" style="width:6%;">Reducci&oacute;n</th>
                                    <th class="titulosnew00" style="width:6%;">Credito</th>
                                    <th class="titulosnew00" style="width:6%;">Contracredito</th>
                                    <th class="titulosnew00" style="width:6%;">Definitivo</th>
                                    <th class="titulosnew00" style="width:6%;">Disponibilidad</th>
                                    <th class="titulosnew00" style="width:6%;">Compromisos</th>
                                    <th class="titulosnew00" style="width:6%;">Oblicaci&oacute;n</th>
                                    <th class="titulosnew00" style="width:6%;">Egreso</th>
                                    <th class="titulosnew00" style="width:6%;">Saldo</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr v-for="(row,index) in sumaTotalGastos" :key="index" v-bind:class="[ 'contenidonew01', 'sumTotal', 'agregado']">

                                    <td style="width:2%;">{{ row[1] }}</td>
                                    <td style="width:2%;">{{ row[1] }}</td>
                                    <td style="width:2%;">{{ row[0] }}</td>
                                    <td style="width:5%;">{{ row[2] }}</td>
                                    <td>{{ row[3] }}</td>
                                    <td style="width:2%;">{{ row[4] }}</td>
                                    <td style="width:4%;">{{ row[5] }}</td>
                                    <td style="width:8%;"></td>
                                    <td style="width:3%;">{{ row[6] }}</td>
                                    <td style="width:6%; text-align: right">{{ formatonumero(row[7]) }}</td>
                                    <td style="width:6%; text-align: right">{{ formatonumero(row[8]) }}</td>
                                    <td style="width:6%; text-align: right">{{ formatonumero(row[9]) }}</td>
                                    <td style="width:6%; text-align: right">{{ formatonumero(row[10]) }}</td>
                                    <td style="width:6%; text-align: right">{{ formatonumero(row[11]) }}</td>
                                    <td style="width:6%; text-align: right">{{ formatonumero(row[12]) }}</td>
                                    <td style="width:6%; text-align: right">{{ formatonumero(row[13]) }}</td>
                                    <td style="width:6%; text-align: right">{{ formatonumero(row[14]) }}</td>
                                    <td style="width:6%; text-align: right">{{ formatonumero(row[15]) }}</td>
                                    <td style="width:6%; text-align: right">{{ formatonumero(row[16]) }}</td>
                                    <td style="width:6%; text-align: right">{{ formatonumero(row[17]) }}</td>

                                    <input type='hidden' name='vigencia_gastoTot[]' v-model="row[0]">
                                    <input type='hidden' name='sec_presupuestalTot[]' v-model="row[1]">
                                    <input type='hidden' name='rubroTot[]' v-model="row[2]">
                                    <input type='hidden' name='nombreRubroTot[]' v-model="row[3]">
                                    <input type='hidden' name='tipoTot[]' v-model="row[4]">
                                    <input type='hidden' name='fuenteTot[]' v-model="row[5]">
                                    <!-- <input type='hidden' name='nombreFuente[]' v-model=""> -->
                                    <input type='hidden' name='csfTot[]' v-model="row[6]">
                                    <input type='hidden' name='inicialTot[]' v-model="row[7]">
                                    <input type='hidden' name='adicionTot[]' v-model="row[8]">
                                    <input type='hidden' name='reduccionTot[]' v-model="row[9]">
                                    <input type='hidden' name='creditoTot[]' v-model="row[10]">
                                    <input type='hidden' name='contraCreditoTot[]' v-model="row[11]">
                                    <input type='hidden' name='definitivoTot[]' v-model="row[12]">
                                    <input type='hidden' name='disponibilidadTot[]' v-model="row[13]">
                                    <input type='hidden' name='compromisoTot[]' v-model="row[14]">
                                    <input type='hidden' name='obligacionTot[]' v-model="row[15]">
                                    <input type='hidden' name='egresoTot[]' v-model="row[16]">
                                    <input type='hidden' name='saldoTot[]' v-model="row[17]">

                                </tr>

                                <tr v-if="existeTotalGastosFun" v-for="(row,index) in sumaTotalGastosFuncionamiento" :key="index" v-bind:class="[ 'contenidonew01', 'sumTotalFuncionamiento', 'agregado']">

                                    <td style="width:2%;">{{ row[1] }}</td>
                                    <td style="width:2%;">{{ row[1] }}</td>
                                    <td style="width:2%;">{{ row[0] }}</td>
                                    <td style="width:5%;">{{ row[2] }}</td>
                                    <td>{{ row[3] }}</td>
                                    <td style="width:2%;">{{ row[4] }}</td>
                                    <td style="width:4%;">{{ row[5] }}</td>
                                    <td style="width:8%;"></td>
                                    <td style="width:3%;">{{ row[6] }}</td>
                                    <td style="width:6%; text-align: right;">{{ formatonumero(row[7]) }}</td>
                                    <td style="width:6%; text-align: right;">{{ formatonumero(row[8]) }}</td>
                                    <td style="width:6%; text-align: right;">{{ formatonumero(row[9]) }}</td>
                                    <td style="width:6%; text-align: right;">{{ formatonumero(row[10]) }}</td>
                                    <td style="width:6%; text-align: right;">{{ formatonumero(row[11]) }}</td>
                                    <td style="width:6%; text-align: right;">{{ formatonumero(row[12]) }}</td>
                                    <td style="width:6%; text-align: right;">{{ formatonumero(row[13]) }}</td>
                                    <td style="width:6%; text-align: right;">{{ formatonumero(row[14]) }}</td>
                                    <td style="width:6%; text-align: right;">{{ formatonumero(row[15]) }}</td>
                                    <td style="width:6%; text-align: right;">{{ formatonumero(row[16]) }}</td>
                                    <td style="width:6%; text-align: right;">{{ formatonumero(row[17]) }}</td>

                                    <input type='hidden' name='vigencia_gastoFun[]' v-model="row[0]">
                                    <input type='hidden' name='sec_presupuestalFun[]' v-model="row[1]">
                                    <input type='hidden' name='rubroFun[]' v-model="row[2]">
                                    <input type='hidden' name='nombreRubroFun[]' v-model="row[3]">
                                    <input type='hidden' name='tipoFun[]' v-model="row[4]">
                                    <input type='hidden' name='fuenteFun[]' v-model="row[5]">
                                    <!-- <input type='hidden' name='nombreFuente[]' v-model=""> -->
                                    <input type='hidden' name='csfFun[]' v-model="row[6]">
                                    <input type='hidden' name='inicialFun[]' v-model="row[7]">
                                    <input type='hidden' name='adicionFun[]' v-model="row[8]">
                                    <input type='hidden' name='reduccionFun[]' v-model="row[9]">
                                    <input type='hidden' name='creditoFun[]' v-model="row[10]">
                                    <input type='hidden' name='contraCreditoFun[]' v-model="row[11]">
                                    <input type='hidden' name='definitivoFun[]' v-model="row[12]">
                                    <input type='hidden' name='disponibilidadFun[]' v-model="row[13]">
                                    <input type='hidden' name='compromisoFun[]' v-model="row[14]">
                                    <input type='hidden' name='obligacionFun[]' v-model="row[15]">
                                    <input type='hidden' name='egresoFun[]' v-model="row[16]">
                                    <input type='hidden' name='saldoFun[]' v-model="row[17]">

                                </tr>

                                <tr v-if="existeTotalGastosInv" v-for="(row,index) in sumaTotalGastosInversion" :key="index" v-bind:class="[ 'contenidonew01', 'sumTotalFuncionamiento', 'agregado']">

                                    <td style="width:2%;">{{ row[1] }}</td>
                                    <td style="width:2%;">{{ row[1] }}</td>
                                    <td style="width:2%;">{{ row[0] }}</td>
                                    <td style="width:5%;">{{ row[2] }}</td>

                                    <td>{{ row[3] }}</td>
                                    <td style="width:2%;">{{ row[4] }}</td>
                                    <td style="width:4%;">{{ row[5] }}</td>
                                    <td style="width:8%;"></td>
                                    <td style="width:3%;">{{ row[6] }}</td>
                                    <td style="width:6%; text-align: right;">{{ formatonumero(row[7]) }}</td>
                                    <td style="width:6%; text-align: right;">{{ formatonumero(row[8]) }}</td>
                                    <td style="width:6%; text-align: right;">{{ formatonumero(row[9]) }}</td>
                                    <td style="width:6%; text-align: right;">{{ formatonumero(row[10]) }}</td>
                                    <td style="width:6%; text-align: right;">{{ formatonumero(row[11]) }}</td>
                                    <td style="width:6%; text-align: right;">{{ formatonumero(row[12]) }}</td>
                                    <td style="width:6%; text-align: right;">{{ formatonumero(row[13]) }}</td>
                                    <td style="width:6%; text-align: right;">{{ formatonumero(row[14]) }}</td>
                                    <td style="width:6%; text-align: right;">{{ formatonumero(row[15]) }}</td>
                                    <td style="width:6%; text-align: right;">{{ formatonumero(row[16]) }}</td>
                                    <td style="width:6%; text-align: right;">{{ formatonumero(row[17]) }}</td>

                                    <input type='hidden' name='vigencia_gastoInv[]' v-model="row[0]">
                                    <input type='hidden' name='sec_presupuestalInv[]' v-model="row[1]">
                                    <input type='hidden' name='rubroInv[]' v-model="row[2]">
                                    <input type='hidden' name='nombreRubroInv[]' v-model="row[3]">
                                    <input type='hidden' name='tipoInv[]' v-model="row[4]">
                                    <input type='hidden' name='fuenteInv[]' v-model="row[5]">
                                    <!-- <input type='hidden' name='nombreFuente[]' v-model=""> -->
                                    <input type='hidden' name='csfInv[]' v-model="row[6]">
                                    <input type='hidden' name='inicialInv[]' v-model="row[7]">
                                    <input type='hidden' name='adicionInv[]' v-model="row[8]">
                                    <input type='hidden' name='reduccionInv[]' v-model="row[9]">
                                    <input type='hidden' name='creditoInv[]' v-model="row[10]">
                                    <input type='hidden' name='contraCreditoInv[]' v-model="row[11]">
                                    <input type='hidden' name='definitivoInv[]' v-model="row[12]">
                                    <input type='hidden' name='disponibilidadInv[]' v-model="row[13]">
                                    <input type='hidden' name='compromisoInv[]' v-model="row[14]">
                                    <input type='hidden' name='obligacionInv[]' v-model="row[15]">
                                    <input type='hidden' name='egresoInv[]' v-model="row[16]">
                                    <input type='hidden' name='saldoInv[]' v-model="row[17]">

                                </tr>

                                <tr v-for="(row,index) in arbol" :key="index" v-bind:class="[row[2] === 'C' ? 'captura' : 'agregado' , Math.round(row[19]) < 0 ? 'colorFila1' : (Math.trunc(row[15]) < Math.trunc(row[16]) ? 'colorFila1' : (Math.trunc(row[16]) < Math.trunc(row[17]) ? 'colorFila1' : (Math.trunc(row[17]) < Math.trunc(row[18]) ? 'colorFila1' : (index % 2 ? 'contenidonew00' : 'contenidonew01'))))]">

                                    <td style="width:2%;">{{ row[7] }} {{ row[8] }}</td>
                                    <td style="width:2%;">{{ row[4] }} {{ nombreSeccion(row[4]) }}</td>
                                    <td style="width:2%;">{{ row[5] }}</td>
                                    <td style="width:5%;" v-on:dblclick="auxiliarDetalle(row)" v-bind:class="row[2] === 'C' ? 'dobleclickCelda' : ''" title="Doble click, genera auxiliar">{{ row[0] }}</td>
                                    <td>{{ row[1] }}</td>
                                    <td style="width:2%;">{{ row[2] }}</td>
                                    <td style="width:4%;">{{ row[3] }}</td>
                                    <td style="width:8%;">{{ buscarFuente(row[3]) }}</td>
                                    <td style="width:3%;">{{ row[6] }}</td>
                                    <td style="width:6%; text-align: right;">{{ formatonumero(row[9]) }}</td>
                                    <td style="width:6%; text-align: right;">{{ formatonumero(row[10]) }}</td>
                                    <td style="width:6%; text-align: right;">{{ formatonumero(row[11]) }}</td>
                                    <td style="width:6%; text-align: right;">{{ formatonumero(row[12]) }}</td>
                                    <td style="width:6%; text-align: right;">{{ formatonumero(row[13]) }}</td>
                                    <td style="width:6%; text-align: right;">{{ formatonumero(row[14]) }}</td>
                                    <td style="width:6%; text-align: right;" v-on:dblclick="auxiliarPorComprobante('disponibilidad', row[3], row[0])" v-bind:class="row[2] === 'C' ? 'dobleclickCelda' : ''" title="Doble click, genera auxiliar">{{ formatonumero(row[15]) }}</td>
                                    <td style="width:6%; text-align: right;" v-on:dblclick="auxiliarPorComprobante('compromiso', row[3], row[0])" v-bind:class="row[2] === 'C' ? 'dobleclickCelda' : ''" title="Doble click, genera auxiliar">{{ formatonumero(row[16]) }}</td>
                                    <td style="width:6%; text-align: right;" v-on:dblclick="auxiliarPorComprobante('obligacion', row[3], row[0])" v-bind:class="row[2] === 'C' ? 'dobleclickCelda' : ''" title="Doble click, genera auxiliar">{{ formatonumero(row[17]) }}</td>
                                    <td style="width:6%; text-align: right;" v-on:dblclick="auxiliarPorComprobante('egreso', row[3], row[0])" v-bind:class="row[2] === 'C' ? 'dobleclickCelda' : ''" title="Doble click, genera auxiliar">{{ formatonumero(row[18]) }}</td>
                                    <td style="width:6%; text-align: right;">{{ formatonumero(row[19]) }}</td>

                                    <input type='hidden' name='unida_ejecutora[]' v-model="row[7]">
                                    <input type='hidden' name='unida_ejecutora_nombre[]' v-model="row[8]">
                                    <input type='hidden' name='vigencia_gasto[]' v-model="row[5]">
                                    <input type='hidden' name='sec_presupuestal[]' v-model="row[4]">
                                    <input type='hidden' name='nombreSec_presupuestal[]' v-model="nombreSeccion(row[4])">
                                    <input type='hidden' name='rubro[]' v-model="row[0]">
                                    <input type='hidden' name='nombreRubro[]' v-model="row[1]">
                                    <input type='hidden' name='tipo[]' v-model="row[2]">
                                    <input type='hidden' name='fuente[]' v-model="row[3]">
                                    <input type='hidden' name='nombreFuente[]' v-model="buscarFuente(row[3])">
                                    <input type='hidden' name='csf[]' v-model="row[6]">
                                    <input type='hidden' name='inicial[]' v-model="row[9]">
                                    <input type='hidden' name='adicion[]' v-model="row[10]">
                                    <input type='hidden' name='reduccion[]' v-model="row[11]">
                                    <input type='hidden' name='credito[]' v-model="row[12]">
                                    <input type='hidden' name='contraCredito[]' v-model="row[13]">
                                    <input type='hidden' name='definitivo[]' v-model="row[14]">
                                    <input type='hidden' name='disponibilidad[]' v-model="row[15]">
                                    <input type='hidden' name='compromiso[]' v-model="row[16]">
                                    <input type='hidden' name='obligacion[]' v-model="row[17]">
                                    <input type='hidden' name='egreso[]' v-model="row[18]">
                                    <input type='hidden' name='saldo[]' v-model="row[19]">
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <div id="cargando" v-if="loading" class="loading">
                        <span>Cargando...</span>
                    </div>
                </article>
                <!-- <button @click = "downloadExl"> Exportar </button> -->
            </section>
        </form>

        <script src="https://cdn.jsdelivr.net/npm/vue@2.5.16/dist/vue.js"></script>
        <script src="https://unpkg.com/vue-multiselect@2.1.0"></script>
        <!-- <link rel="stylesheet" href="https://unpkg.com/vue-multiselect@2.1.0/dist/vue-multiselect.min.css"> -->
        <link rel="stylesheet" href="multiselect.css">

        <!-- <script src="Librerias/vue/vue.min.js"></script> -->
        <script src="Librerias/vue/axios.min.js"></script>
		<script src="xlsx/dist/xlsx.min.js"></script>
		<script src="file-saver/dist/FileSaver.min.js"></script>
		<script src="vue/presupuesto_ccp/ccp-ejecucionpresupuestal-vue.js"></script>
        <script src="sweetalert2/dist/sweetalert2.min.js"></script>
        <link rel="stylesheet" href="sweetalert2/dist/sweetalert2.min.css">




    </body>
</html>