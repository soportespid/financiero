<?php
	header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=utf8");
	require "comun.inc";
	require "funciones.inc";
	sesion();
	$linkbd=conectar_v7();
	$linkbd -> set_charset("utf8");
	cargarcodigopag($_GET[codpag],$_SESSION["nivel"]);
	date_default_timezone_set("America/Bogota");
	ini_set('max_execution_time', 1800);
?>
<!doctype >
<html lang="es">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=9">
		<title>:: Ideal - Presupuesto</title>
		<link href="css/css2.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css">
		<link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css">
		<link href="css/css4.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css">
		<script type="text/javascript" src="css/calendario.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script type="text/javascript" src="css/programas.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script type='text/javascript' src='JQuery/jquery-2.1.4.min.js?<?php echo date('d_m_Y_h_i_s');?>'></script>
		<script>
			function pdf()
			{
				document.form2.action = "pdfejecuciongastos.php";
				document.form2.target = "_BLANK";
				document.form2.submit();
				document.form2.action = "";
				document.form2.target = "";
			}
			function excell()
			{
				document.form2.action = "presu-ejecuciongastosexcel.php";
				document.form2.target = "_BLANK";
				document.form2.submit();
				document.form2.action = "";
				document.form2.target = "";
			}
			function callprogress(vValor)
			{
				document.getElementById("getprogress").innerHTML = vValor;
				document.getElementById("getProgressBarFill").innerHTML = '<div class="ProgressBarFill" style="width: '+vValor+'%;"></div>';
				//document.getElementById("titulog1").style.display='block';
				document.getElementById("progreso").style.display='block';
				document.getElementById("getProgressBarFill").style.display='block';
			}
			function callprogress2(vValor)
			{
				document.getElementById("getprogress2").innerHTML = vValor;
				document.getElementById("getProgressBarFill2").innerHTML = '<div class="ProgressBarFill2" style="width: '+vValor+'%;"></div>';
				document.getElementById("progreso2").style.display='block';
				document.getElementById("getProgressBarFill2").style.display='block';
			}
			function detallea(id, cuenta)
			{
				if($('#detalle'+id).css('display')=='none')
				{
					$('#detalle'+id).css('display','block');
					$('#img'+id).attr('src','imagenes/minus.gif');
				}
				else
				{
					$('#detalle'+id).css('display','none');
					$('#img'+id).attr('src','imagenes/plus.gif');
				}
				var toLoad= 'cpp-reportescuipodetalle1.php';
				$.post(toLoad,{cuenta:cuenta},function (data){
					$('#detalle'+id).html(data.detalle);
					return false;
				},'json');
			}
			function detalleb(id, cuenta, cpc, fuente)
			{
				if($('#detalle'+id).css('display')=='none')
				{
					$('#detalle'+id).css('display','block');
					$('#img'+id).attr('src','imagenes/minus.gif');
				}
				else
				{
					$('#detalle'+id).css('display','none');
					$('#img'+id).attr('src','imagenes/plus.gif');
				}
				var toLoad= 'cpp-reportescuipodetalle2.php';
				$.post(toLoad,{cuenta:cuenta,cpc:cpc,fuente:fuente},function (data){
					$('#detalle'+id).html(data.detalle);
					return false;
				},'json');
			}
			function detallec(id, cuenta, vigengasto, seccpresu, progrmga, bpin)
			{
				if($('#detalle'+id).css('display')=='none')
				{
					$('#detalle'+id).css('display','block');
					$('#img'+id).attr('src','imagenes/minus.gif');
				}
				else
				{
					$('#detalle'+id).css('display','none');
					$('#img'+id).attr('src','imagenes/plus.gif');
				}
				var toLoad= 'cpp-reportescuipodetalle3.php';
				$.post(toLoad,{cuenta:cuenta, vigengasto:vigengasto, vigengasto:vigengasto, seccpresu:seccpresu, progrmga:progrmga, bpin:bpin},function (data){
					$('#detalle'+id).html(data.detalle);
					return false;
				},'json');
			}
			function detalled(id, cuenta, seccpresupuestal, programatico, cpc, fuente, bpin,vigengasto)
			{
				if($('#detalle'+id).css('display')=='none')
				{
					$('#detalle'+id).css('display','block');
					$('#img'+id).attr('src','imagenes/minus.gif');
				}
				else
				{
					$('#detalle'+id).css('display','none');
					$('#img'+id).attr('src','imagenes/plus.gif');
				}
				var toLoad= 'cpp-reportescuipodetalle4.php';
				$.post(toLoad,{cuenta:cuenta,seccpresupuestal:seccpresupuestal,programatico:programatico,cpc:cpc,fuente:fuente,bpin:bpin, vigengasto:vigengasto},function (data){
					$('#detalle'+id).html(data.detalle);
					return false;
				},'json');
			}
		</script>
		<?php
			function buscarfuentecuipo($codigo)
			{
				$linkbd = conectar_v7();
				$linkbd -> set_charset("utf8");
				$sqlmax = "SELECT MAX(version) FROM ccpet_fuentes_cuipo";
				$resmax = mysqli_query($linkbd, $sqlmax);
				$rowmax = mysqli_fetch_row($resmax);
				$sql = "SELECT concuipocpc FROM ccpet_fuentes_cuipo WHERE codigo_fuente = '$codigo' AND version = '$rowmax[0]'";
				$res = mysqli_query($linkbd,$sql);
				$totalcli=mysqli_num_rows($res);
				$row = mysqli_fetch_row($res);
				return($row[0]);
			}
			function buscarcpccuipo($codigo)//0-4
			{
				$linkbd=conectar_v7();
				$linkbd -> set_charset("utf8");
				if(substr($codigo,0,1) <= 4)
				{
					$sqlmax = "SELECT MAX(version) FROM ccpetbienestransportables";
					$resmax = mysqli_query($linkbd, $sqlmax);
					$rowmax = mysqli_fetch_row($resmax);
					$sql = "SELECT concuipocpc FROM ccpetbienestransportables WHERE cpc = '$codigo' AND version = '$rowmax[0]'";
				}
				else
				{
					$sqlmax = "SELECT MAX(version) FROM ccpetservicios";
					$resmax = mysqli_query($linkbd, $sqlmax);
					$rowmax = mysqli_fetch_row($resmax);
					$sql = "SELECT concuipocpc FROM ccpetservicios WHERE cpc = '$codigo' AND version = '$rowmax[0]'";
				}
				$res = mysqli_query($linkbd,$sql);
				$row = mysqli_fetch_row($res);
				return($row[0]);
			}
			function buscarprogramatico($codigo,$tipo)
			{
				$linkbd=conectar_v7();
				$linkbd -> set_charset("utf8");
				//$codm1=substr($codigo,0,7);
				//if($tipo == '1'){$sqlproducto = "SELECT concuipom1 FROM ccpetproductos WHERE cod_producto LIKE '$codm1%'";}
				//else {$sqlproducto = "SELECT concuipom2 FROM ccpetproductos WHERE cod_producto LIKE '$codm1%'";}
				if($tipo == '1'){$sqlproducto = "SELECT concuipom1 FROM ccpetproductos WHERE codigo_indicador LIKE '$codigo'";}
				else {$sqlproducto = "SELECT concuipom2 FROM ccpetproductos WHERE codigo_indicador LIKE '$codigo'";}
				$resproducto = mysqli_query($linkbd,$sqlproducto);
				$rowproducto = mysqli_fetch_row($resproducto);
				return($rowproducto[0]);
			}
			function fcolorlinea($valor1,$valor2,$valor3)
			{
				if($valor1 == $valor2 && $valor2 == $valor3){$color='';}
				else if($valor1 >= $valor2 && $valor2 >= $valor3){$color="style='background-color:rgba(25,151,255,1.00) !important;'";}
				else {$color="style='background-color:rgba(246,17,21,0.93) !important;'";}
				return $color;
			}
			titlepag();
		?>
</head>
	<body>
		<IFRAME src="alertas.php" name="alertas" id="alertas" style="display:none"></IFRAME>
		<span id="todastablas2"></span>
		<table>
			<tr>
				<script>barra_imagenes("ccpet");</script><?php cuadro_titulos(); ?></tr>
			<tr><?php menu_desplegable("ccpet"); ?></tr>
			<tr>
				<?php
					$informes = [];
					$informes[1] = "A_PROGRAMACION_DE_INGRESOS";
					$informes[2] = "B_EJECUCION_DE_INGRESOS";
					$informes[3] = "C_PROGRAMACION_DE_GASTOS";
					$informes[4] = "D_EJECUCION_DE_GASTOS";
					$informes[5] = "E_ID_SECCIONES_PRESUPUESTALES_ADICIONALES";
				?>
				<td colspan="3" class="cinta"><img src='imagenes/add.png' title='Nuevo' onClick="location.href='ccp-reportescuipo.php'" class="mgbt"><img src="imagenes/guardad.png" title="Guardar" class="mgbt1"><img src="imagenes/busca.png" title="Buscar" onClick="document.form2.submit()" class="mgbt"><img src="imagenes/nv.png" title="Nueva Ventana" onClick="mypop=window.open('ccp-principal.php','','');mypop.focus();" class="mgbt"><img src="imagenes/print.png" title="imprimir" onClick="pdf()" class="mgbt">
					<a href="<?php echo "archivos/" . $_SESSION['usuario'] . "informecgr" . $fec . ".csv"; ?>"
							target="_blank" class="mgbt"><img src="imagenes/csv.png" title="cs"></a>
					<a href="descargartxt.php?id=<?php echo $informes[$_POST['reporte']] . ".txt"; ?>&dire=archivos" target="_blank" class="mgbt"><img src="imagenes/contraloria.png" title="contraloria"></a>
					<a href="descargartxt.php?id=<?php echo $informes[$_POST['reporte']] . "b.txt"; ?>&dire=archivos" target="_blank" class="mgbt"><img src="imagenes/contraloria.png" title="Sin Fuente CUIPO"></a>
				</td>
			</tr>
		</table>
		<form name="form2" method="post" action="">
			<?php
				
				$sqlr = "SELECT * FROM configbasica WHERE estado='S'";
				$res = mysqli_query($linkbd,$sqlr);
				while ($row = mysqli_fetch_row($res))
				{
					$_POST['nitentidad'] = $row[0];
					$_POST['entidad'] = $row[1];
					$_POST['codent'] = $row[8];
				}
				if($_POST['oculto']=='')
				{
					$_POST['vigeactual'] = vigencia_usuarios($_SESSION['cedulausu']);
				}
				$vigusu = $_POST['vigeactual'];
				
			?>
			<table align="center" class="inicio">
				<tr>
					<td class="titulos" colspan="9"><p class="neon_titulos">.: Reportes CUIPO</p></td>
					<td class="cerrar" style="width:7%" onClick="location.href='ccp-principal.php'">Cerrar</td>
				</tr>
				<tr>
					<td class="tamano01" style="width:2.5cm;">Reporte:</td>
					<td>
						<select name="reporte" id="reporte" style="width:98%;height:30px;">
							<option value="-1">Seleccione ....</option>
							<option value="1" <?php if ($_POST['reporte'] == '1') echo "selected" ?>>A. PROGRAMACI&Oacute;N DE INGRESOS </option>
							<option value="2" <?php if ($_POST['reporte'] == '2') echo "selected" ?>>B. EJECUCI&Oacute;N DE INGRESOS </option>
							<option value="3" <?php if ($_POST['reporte'] == '3') echo "selected" ?>>C. PROGRAMACI&Oacute;N DE GASTOS </option>
							<option value="4" <?php if ($_POST['reporte'] == '4') echo "selected" ?>>D. EJECUCI&Oacute;N DE GASTOS </option>
							<option value="5" <?php if ($_POST['reporte'] == '5') echo "selected" ?>>E. ID SECCIONES PRESUPUESTALES ADICIONALES </option>
						</select>
					</td>
					<td class="tamano01" style="width:2.5cm;">Vigencia:</td>
					<td>
						<select name="vigeactual" style="width:98%;height:30px;">
							<option value="0">Año</option>
							<?php
								for($i=2050;$i>=1950;$i--)
								{
									echo "<option value='$i'";
									if ($_POST['vigeactual'] == $i) echo "selected";
									echo">$i</option>";
								}
							?>
						</select>
					</td>
					<td class="tamano01" style="width:2.5cm;">Periodos:</td>
					<td>
						<select name="periodos" id="periodos" onChange="validar()" style="width:98%;height:30px;">
							<option value="">Sel..</option>
							<?php
								$sqlr = "SELECT * FROM periodos_cuipo WHERE estado='S' ORDER BY id";
								$resp = mysqli_query($linkbd,$sqlr);
								while ($row = mysqli_fetch_row($resp))
								{
									if ($row[0] == $_POST['periodos'])
									{
										echo "<option value=$row[0] SELECTED>$row[2]</option>";
										$_POST['periodo'] = $row[1];
										$_POST['cperiodo'] = $row[2];
									}
									else {echo "<option value=$row[0]>$row[2]</option>";}
								}
							?>
						</select>
						<input type="hidden" name="periodo" value="<?php echo $_POST['periodo'] ?>">
						<input type="hidden" name="cperiodo" value="<?php echo $_POST['cperiodo'] ?>">
					</td>
					<td class="tamano01" style="width:2.5cm;">Codigo Entidad:</td>
					<td style="width:16%;"><input name="codent" type="text" value="<?php echo $_POST['codent'] ?>" style="width:98%;height:30px;"></td>
					<td style="padding-bottom:0px"><em class="botonflecha" onClick="document.form2.submit()">Generar</em></td>
					<input type="hidden" value="1" name="oculto">
				</tr>
			</table>
			<?php
				$oculto = $_POST['oculto'];
				if ($_POST['oculto'])
				{
					$acumulado = 0;
					switch ($_POST['reporte'])
					{
						case 1: //PROGRAMACIÓN DE INGRESOS
						{
							$numid = $c = 0;
							$maxVersion = ultimaVersionIngresosCCPET();
							$namearch = "archivos/" . $_SESSION['usuario'] . "informecgr" . $fec . ".csv";
							$Descriptor1 = fopen($namearch, "w+");
							$namearch2 = "archivos/" . $informes[$_POST['reporte']] . ".txt";
							$Descriptor2 = fopen($namearch2, "w+");
							echo "
							<div class='subpantallap' style='height:66.5%; width:99.6%;overflow-x:hidden'>
								<table class='inicio' align='center'>
									<tr>
										<td colspan='5' class='titulos'><p class='neon_titulos'>.: A. PROGRAMACI&Oacute;N DE INGRESOS:</p></td>
									</tr>
									<tr>
										<td style='width:20%;' id='totalcuentas' colspan='2'>Resultados Encontrados:</td>
										<td colspan='3'>
											<div id='progreso' class='ProgressBar' style='display:none; float:left'>
												<div class='ProgressBarText'><span id='getprogress'></span>&nbsp;% </div>
												<div id='getProgressBarFill'></div>
											</div>
										</td>
									</tr>
									<tr style='text-align:center;'>
										<td class='titulos2' style='width:2px;'><img src='imagenes/plus.gif'></td>
										<td class='titulos2' colspan='2'  style='font-size:14px;'>&Aacute;RBOL DE CONCEPTOS</td>
										<td class='titulos2' style='width:15%;font-size:14px;'>PRESUPUESTO INICIAL</td>
										<td class='titulos2'style='width:15%;font-size:14px;'>PRESUPUESTO DEFINITIVO</td>
									</tr>";
							$mes2 = substr($_POST['periodo'], 3, 2);
							$_POST['fecha'] = "01/01/$vigusu";
							$_POST['fecha2'] = intval(date("t", $mes2)) . '/' . $mes2 . '/' . $vigusu;
							preg_match("/([0-9]{2})\/([0-9]{2})\/([0-9]{4})/", $_POST['fecha'],$fecha);
							$fechai="$fecha[3]-$fecha[2]-$fecha[1]";
							preg_match("/([0-9]{2})\/([0-9]{2})\/([0-9]{4})/", $_POST['fecha2'],$fecha);
							$fechaf = "$fecha[3]-$fecha[2]-$fecha[1]";
							fputs($Descriptor1, "S;".$_POST['codent'].";".$_POST['periodo'].";".$vigusu.";A_PROGRAMACION_DE_INGRESOS\r\n");
							fputs($Descriptor1, "S;ARBOL DE CONCEPTOS; PRESUPUESTO INICIAL; PRESUPUESTO DEFINITIVO\r\n");
							fputs($Descriptor2, "S\t" . $_POST['codent'] . "\t" . $_POST['periodo'] . "\t" . $vigusu . "\tA_PROGRAMACION_DE_INGRESOS\r\n");
							if($_SESSION['tablatemporal'] != '' && $_SESSION['tablatemporal'] != null)
							{
								$sqlr = "DROP TABLE ".$_SESSION['tablatemporal'];
								mysqli_query($linkbd,$sqlr);
								$sqlr = "CREATE TABLE ".$_SESSION['tablatemporal']." (id int(11),codigocuenta varchar(100), nombrecuenta varchar(200), initransp double, inicuin double, iniservicios double, iniingresos double, adicion double, reduccion double, adicion2 double, reduccion2 double) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci";
								mysqli_query($linkbd,$sqlr);
							}
							$sqlcta ="SELECT codigo, nombre FROM cuentasingresosccpet WHERE municipio=1 AND version = '$maxVersion' AND tipo = 'C' AND codigo <> '1.2.10.01' ORDER BY id ASC";
							$rescta = mysqli_query($linkbd,$sqlcta);
							$totalcli=mysqli_num_rows($rescta);
							while ($rowcta = mysqli_fetch_row($rescta))
							{
								$numid++;
								$c++;
								$sqlinitransp = "SELECT SUM(valor) FROM ccpetinicialingresosbienestransportables WHERE cuenta = '$rowcta[0]' AND vigencia = '$vigusu'";
								$resinitransp = mysqli_query($linkbd,$sqlinitransp);
								$rowinitransp = mysqli_fetch_row($resinitransp);
								if($rowinitransp[0] != ''){$valinitransp = round($rowinitransp[0]);}
								else {$valinitransp = 0;}
								
								$sqlinicuin = "SELECT SUM(valor) FROM ccpetinicialingresoscuin WHERE cuenta = '$rowcta[0]' AND vigencia = '$vigusu'";
								$resinicuin = mysqli_query($linkbd,$sqlinicuin);
								$rowinicuin = mysqli_fetch_row($resinicuin);
								if($rowinicuin[0] != ''){$valinicuin=round($rowinicuin[0]);}
								else {$valinicuin = 0;}
								
								$sqliniserv = "SELECT SUM(valor) FROM ccpetinicialserviciosingresos WHERE cuenta = '$rowcta[0]' AND vigencia = '$vigusu'";
								$resiniserv = mysqli_query($linkbd,$sqliniserv);
								$rowiniserv = mysqli_fetch_row($resiniserv);
								if($rowiniserv[0] != ''){$valiniserv = round($rowiniserv[0]);}
								else {$valiniserv = 0;}
								
								$sqliniingre = "SELECT SUM(valor) FROM ccpetinicialvaloringresos WHERE cuenta = '$rowcta[0]' AND vigencia = '$vigusu'";
								$resiniingre = mysqli_query($linkbd,$sqliniingre);
								$rowiniingre = mysqli_fetch_row($resiniingre);
								if($rowiniingre[0] != ''){$valiniingre = round($rowiniingre[0]);}
								else {$valiniingre = 0;}
								
								$sqladicion = "
								SELECT SUM(T1.valorcsf + T1.valorssf) FROM ccpetadicion_inversion_detalles AS T1 
								INNER JOIN ccpetadicion_inversion AS T2 
								ON T1.codproyecto = T2.id 
								INNER JOIN ccpetacuerdos AS T3
								ON T2.id_acuerdo = T3.id_acuerdo
								WHERE T1.rubro= '$rowcta[0]' AND T3.vigencia = '$vigusu' AND T3.fecha BETWEEN '$fechai' AND '$fechaf'
								GROUP BY T1.rubro";
								$resadicion = mysqli_query($linkbd,$sqladicion);
								$rowadicion = mysqli_fetch_row($resadicion);
								if($rowadicion[0] != ''){$valadicion = round($rowadicion[0]);}
								else {$valadicion = 0;}
								
								$sqladicion2 = "SELECT SUM(valor) FROM ccpetadiciones WHERE cuenta = '$rowcta[0]' AND vigencia = '$vigusu' AND estado != 'N' AND fecha BETWEEN '$fechai' AND '$fechaf' GROUP BY cuenta";
								$resadicion2 = mysqli_query($linkbd, $sqladicion2);
								$rowadicion2 = mysqli_fetch_row($resadicion2);
								if($rowadicion2[0] != ''){$valadicion2 = round($rowadicion2[0]);}
								else {$valadicion2 = 0;}
								
								$sqlreduccion = "SELECT SUM(valor) FROM ccpetreducciones WHERE cuenta = '$rowcta[0]' AND vigencia = '$vigusu' AND fecha BETWEEN '$fechai' AND '$fechaf'";
								$resreduccion = mysqli_query($linkbd,$sqlreduccion);
								$rowreduccion = mysqli_fetch_row($resreduccion);
								if($rowreduccion[0] != ''){$valreduccion = round($rowreduccion[0]);}
								else {$valreduccion = 0;}
								
								$sqlreduccion2 = "
								SELECT SUM(T1.valor) 
								FROM ccpetreduccion AS T1 
								INNER JOIN ccpetacuerdos AS T2
								ON T1.acuerdo = T2.id_acuerdo AND T1.vigencia = T2.vigencia
								WHERE T1.rubro = '$rowcta[0]' AND T1.vigencia = '$vigusu' AND T2.fecha BETWEEN '$fechai' AND '$fechaf'";
								$resreduccion2 = mysqli_query($linkbd,$sqlreduccion2);
								$rowreduccion2 = mysqli_fetch_row($resreduccion2);
								if($rowreduccion2[0] != ''){$valreduccion = round($rowreduccion2[0]);}
								else {$valreduccion2 = 0;}
								
								$sqltabla = "INSERT INTO ".$_SESSION['tablatemporal']." (id, codigocuenta, nombrecuenta, initransp, inicuin, iniservicios, iniingresos, adicion, reduccion, adicion2, reduccion2) VALUES ('$numid', '$rowcta[0]', '$rowcta[1]', '$valinitransp', '$valinicuin', '$valiniserv', '$valiniingre', '$valadicion', '$valreduccion', '$valadicion2', '$valreduccion2')";
								mysqli_query($linkbd,$sqltabla);
								$porcentaje = $c * 100 / $totalcli;
								echo"
								<script>
									progres='".round($porcentaje)."';callprogress(progres);
									document.getElementById('totalcuentas').innerHTML='Resultados Encontrados: $numid / $totalcli';
								</script>";
								flush();
								ob_flush();
								usleep(5);
							}
							$xy=1;
							$iter = "filas01";
							$iter2 = "filas02";
							$sqlver="SELECT * FROM ".$_SESSION['tablatemporal']." ORDER BY id ASC";
							$resver = mysqli_query($linkbd,$sqlver);
							while ($rowver = mysqli_fetch_row($resver))
							{
								$sumaini = $rowver[3] + $rowver[4] + $rowver[5] + $rowver[6];
								$sumatotal = $sumaini + $rowver[7] - $rowver[8] + $rowver[9] + $rowver[10];
								if(($sumaini > 0)||($sumatotal > 0))
								{
									echo "
									<tr class=$iter>
										<td class='titulos2'>
											<a onClick=\"detallea('$xy','$rowver[1]')\" style='cursor:pointer;'>
											<img id='img$xy' src='imagenes/plus.gif'>
											</a>
										</td>
										<td>$rowver[1]</td>
										<td>$rowver[2]</td>
										<td style='text-align:right;'>$".number_format($sumaini,0,',','.')."</td>
										<td style='text-align:right;'>$".number_format($sumatotal,0,',','.')."</td>
									</tr>
									<tr>
									<td align='center'></td>
										<td colspan='16'>
											<div id='detalle$xy' style='display:none'></div>
										</td>
									</tr>";
									$aux=$iter;
									$iter=$iter2;
									$iter2=$aux;
									fputs($Descriptor1,"D;".$rowver[1].";".$sumaini.";".$sumatotal."\r\n");
									fputs($Descriptor2,"D\t".$rowver[1]."\t".$sumaini."\t".$sumatotal."\r\n");
									$xy++;
								}
							}
							echo "
								</table>
							</div>";
							fclose($Descriptor1);
							fclose($Descriptor2);
							echo "<script>document.getElementById('progreso').style.display='none';</script>";
							
						}break;
						case 2: //EJECUCION INGRESOS
						{
							$maxVersion = ultimaVersionIngresosCCPET();
							unset ($numerocuenta,$nombrecuenta,$varcpc,$varnorma1,$varnorma2,$varnorma3,$varnorma4,$valdestiesp, $varfuentes,$vartercerochip,$varpolpublicas,$valrecactualscf,$valrecactualccf,$valrecanteriorscf, $valrecanteriorccf,$valtotalcuenta,$tablasinfo,$varfuentes2);
							$numid=$c=$idtemporal=0;
							$namearch = "archivos/".$_SESSION['usuario']."informecgr".$fec.".csv";
							$Descriptor1 = fopen($namearch, "w+");
							$namearch2 = "archivos/".$informes[$_POST['reporte']].".txt";
							$Descriptor2 = fopen($namearch2, "w+");
							$namearch3 = "archivos/".$informes[$_POST['reporte']]."b.txt";
							$Descriptor3 = fopen($namearch3, "w+");
							echo "
							<div class='subpantallap' style='height:66.5%; width:99.6%;'>
								<table class='inicio' align='center'>
									<tr>
										<td colspan='17' class='titulos'><p class='neon_titulos'>.: B. EJECUCI&Oacute;N DE INGRESOS:</p></td>
									</tr>
									<tr>
										<td style='width:20%;' colspan='3' id='totalcuentas'></td>
										<td colspan='3'>
											<div id='progreso' class='ProgressBar' style='display:none; float:left'>
												<div class='ProgressBarText'><span id='getprogress'></span>&nbsp;% </div>
												<div id='getProgressBarFill'></div>
											</div>
										</td>
										<td colspan='4'>
											<div id='progreso2' class='ProgressBar2' style='display:none; float:left'>
												<div class='ProgressBarText2'><span id='getprogress2'></span>&nbsp;% </div>
												<div id='getProgressBarFill2'></div>
											</div>
										</td>
										<td colspan='5' id='titulosubproceso'></td>
									</tr>
									<tr>
										<td class='titulos2' style='width:2px;'><img src='imagenes/plus.gif'></td>
										<td class='titulos2' colspan='2'>&Aacute;RBOL DE CONCEPTOS</td>
										<td class='titulos2'>CPC</td>
										<td class='titulos2'>APLICA DESTINACI&Oacute;N ESPECIFICA</td>
										<td class='titulos2'>TIPO DE NORMA QUE DEFINE LA DESTINACI&Oacute;N</td>
										<td class='titulos2'>NUMERO DE LA NORMA</td>
										<td class='titulos2'>FECHA DE LA NORMA</td>
										<td class='titulos2'>VALOR DESTINACI&Oacute;N ESPECIFICA</td>
										<td class='titulos2'>FUENTES DE FINANCIACION</td>
										<td class='titulos2'>TERCEROS (C&Oacute;DIGO CHIP)</td>
										<td class='titulos2'>POLITICA PUBLICA</td>
										<td class='titulos2'>RECAUDO DE VIGENCIA ACTUAL SIN SITUACI&Oacute;N DE FONDOS</td>
										<td class='titulos2'>RECAUDO DE VIGENCIA ACTUAL CON SITUACI&Oacute;N DE FONDOS</td>
										<td class='titulos2'>RECAUDO DE VIGENCIA ANTERIOR SIN SITUACI&Oacute;N DE FONDOS</td>
										<td class='titulos2'>RECAUDO DE VIGENCIA ANTERIOR CON SITUACI&Oacute;N DE FONDOS</td>
										<td class='titulos2'>TOTAL RECAUDO</td>
									</tr>";
							$mes2 = substr($_POST['periodo'], 3, 2);
							$_POST['fecha'] = "01/01/$vigusu";
							$_POST['fecha2'] = intval(date("t", $mes2))."/$mes2/$vigusu";
							preg_match("/([0-9]{2})\/([0-9]{2})\/([0-9]{4})/", $_POST['fecha'],$fecha);
							$fechai = "$fecha[3]-$fecha[2]-$fecha[1]";
							preg_match("/([0-9]{2})\/([0-9]{2})\/([0-9]{4})/", $_POST['fecha2'],$fecha);
							$fechaf = "$fecha[3]-$fecha[2]-$fecha[1]";

							fputs($Descriptor1, "S;".$_POST['codent'].";".$_POST['periodo'].";".$vigusu.";B_EJECUCION_DE_INGRESOS\r\n");
							fputs($Descriptor1, "S;ARBOL DE CONCEPTOS;CPC;FUENTES DE FINANCIACION;APLICA DESTINACIÓN ESPECIFICA;TIPO DE NORMA QUE DEFINE LA DESTINACION;NUMERO DE LA NORMA;FECHA DE LA NORMA;VALOR DESTINACION ESPECIFICA;TERCEROS (CODIGO CHIP);POLITICA PUBLICA;RECAUDO DE VIGENCIA ACTUAL SIN SITUACION DE FONDOS;RECAUDO DE VIGENCIA ACTUAL CON SITUACIÓN DE FONDOS;RECAUDO DE VIGENCIA ANTERIOR SIN SITUACION DE FONDOS;RECAUDO DE VIGENCIA ANTERIOR CON SITUACION DE FONDOS;TOTAL RECAUDO\r\n");
							fputs($Descriptor2, "S\t".$_POST['codent']."\t".$_POST['periodo']."\t".$vigusu. "\tB_EJECUCION_DE_INGRESOS\r\n");
							fputs($Descriptor3, "S\t".$_POST['codent']."\t".$_POST['periodo']."\t".$vigusu. "\tB_EJECUCION_DE_INGRESOS\r\n");
							if($_SESSION['tablatemporal'] != '' && $_SESSION['tablatemporal'] != null)
							{
								$sqlr = "DROP TABLE ".$_SESSION['tablatemporal'];
								mysqli_query($linkbd,$sqlr);
								$sqlr = "CREATE TABLE ".$_SESSION['tablatemporal']." (id int(11), codigocuenta varchar(100), nombrecuenta varchar(200), cpc varchar(100), aplicadestinacion varchar(2),tiponorma int(11), numeronorma varchar(50), fechanorma date, valordestinacionesp double, fuente varchar(10), tercerochip varchar(10), politicapublica varchar(10), recaudoactualscf double, recaudoactualccf double, recaudoanteriorscf double, recaudoanteriorccf double, totalrecaudo double, tipotabla varchar(100), fuente2 varchar(10)) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;";
								mysqli_query($linkbd,$sqlr);
							}
							
							$sqlcta ="SELECT codigo, nombre FROM cuentasingresosccpet WHERE municipio=1 AND version = '$maxVersion' AND tipo = 'C' ORDER BY id ASC";
							$rescta = mysqli_query($linkbd,$sqlcta);
							$totalcli=mysqli_num_rows($rescta);
							while ($rowcta = mysqli_fetch_row($rescta))
							{
								$numid++;
								$c++;
								$sqlnormas = "SELECT aplicade,tiponorma,numeronorma,fechanorma FROM ccpecuentastiponormas WHERE cuenta = '$rowcta[0]' AND estado='S'";
								$resnormas = mysqli_query($linkbd,$sqlnormas);
								$rownormas = mysqli_fetch_row($resnormas);
								//RECIBOS DE CAJA
								{
									$cx = 0;
									$sqlreccaja="
									SELECT prc.valor, prc.fuente, prc.productoservicio, prc.idrecibo
									FROM pptorecibocajappto AS prc
									INNER JOIN tesoreciboscaja AS trc
									ON trc.id_recibos = prc.idrecibo AND prc.cuenta = '$rowcta[0]'
									WHERE prc.vigencia = '$vigusu' AND NOT(trc.estado = 'N' OR trc.estado = 'R') AND NOT(prc.tipo = 'R' OR prc.tipo = 'P') AND trc.fecha BETWEEN '$fechai' AND '$fechaf'";//1
									$resreccaja = mysqli_query($linkbd, $sqlreccaja);
									$totalcx = mysqli_num_rows($resreccaja);
									while ($rowreccaja = mysqli_fetch_row($resreccaja))
									{
										$cx++;
										$numerocuenta[] = $rowcta[0];
										$nombrecuenta[] = $rowcta[1];
										if($rowreccaja[2] != '')
										{
											//$auxcpc = buscarcpccuipo($rowreccaja[2]);
											$auxcpc = $rowreccaja[2];
											if($auxcpc != ''){$varcpc[] = $auxcpc;}
											else {$varcpc[] = 'NO APLICA';}
										}
										else {$varcpc[] = 'NO APLICA';}
										$varnorma1[] = $rownormas[0];
										$varnorma2[] = $rownormas[1];
										$varnorma3[] = $rownormas[2];
										$varnorma4[] = $rownormas[3];
										if($rownormas[0] == 'SI'){$valdestiesp[] = round($rowreccaja[0]);}
										else {$valdestiesp[] = 0;}
										if($rowreccaja[1] != '')
										{
											$varfuentes2[] = $rowreccaja[1];
											//$auxfuente = buscarfuentecuipo($rowreccaja[1]);
											$auxfuente = $rowreccaja[1];
											if($auxfuente != ''){$varfuentes[] = $auxfuente;}
											else {$varfuentes[] = 'NO APLICA';}
										}
										else 
										{
											$varfuentes[] = 'NO APLICA';
											$varfuentes2[] = 'NO APLICA';
										}
										$vartercerochip[] = '';
										$varpolpublicas[] = '';
										$valrecactualscf[] = 0;
										$valrecactualccf[] = round($rowreccaja[0]);
										$valrecanteriorscf[] = 0;
										$valrecanteriorccf[] = 0;
										$valtotalcuenta[] = round($rowreccaja[0]);
										$tablasinfo[] = "Recibo de Caja No: $rowreccaja[3]";
										$porcentaje2 = $cx * 100 / $totalcx;
										echo"
										<script>
											progres2 = '".round($porcentaje2)."';
											callprogress2(progres2);
											document.getElementById('titulosubproceso').innerHTML = 'Recibos de caja No: $rowreccaja[3] ($rowcta[0])';
										</script>";
										flush();
										ob_flush();
										usleep(1);
									}
									echo "
									<script>
										document.getElementById('progreso2').style.display='none';
										document.getElementById('titulosubproceso').innerHTML='';
									</script>";
								}
								//SIN RECIBOS DE CAJA
								{
									$cx = 0;
									$sqlsinreccaja="
									SELECT psrc.valor, psrc.fuente, psrc.productoservicio, psrc.idrecibo
									FROM pptosinrecibocajappto AS psrc
									INNER JOIN tesosinreciboscaja AS tsrc
									ON tsrc.id_recibos = psrc.idrecibo AND psrc.cuenta = '$rowcta[0]'
									WHERE psrc.vigencia = '$vigusu' AND NOT(tsrc.estado = 'N' OR tsrc.estado = 'R') AND tsrc.fecha BETWEEN '$fechai' AND '$fechaf'";//2
									$ressinreccaja = mysqli_query($linkbd, $ressinreccaja);
									$totalcx = mysqli_num_rows($ressinreccaja);
									while ($rowsinreccaja = mysqli_fetch_row($ressinreccaja))
									{
										$numerocuenta[] = $rowcta[0];
										$nombrecuenta[] = $rowcta[1];
										if($rowsinreccaja[2] != '')
										{
											//$auxcpc = buscarcpccuipo($rowsinreccaja[2]);
											$auxcpc = $rowsinreccaja[2];
											if($auxcpc != ''){$varcpc[] = $auxcpc;}
											else {$varcpc[] = 'NO APLICA';}
										}
										else {$varcpc[]='NO APLICA';}
										$varnorma1[] = $rownormas[0];
										$varnorma2[] = $rownormas[1];
										$varnorma3[] = $rownormas[2];
										$varnorma4[] = $rownormas[3];
										if($rownormas[0] == 'SI'){$valdestiesp[] = round($rowsinreccaja[0]);}
										else {$valdestiesp[] = 0;}
										if($rowsinreccaja[1] != '')
										{
											$varfuentes2[] = $rowsinreccaja[1];
											//$auxfuente = buscarfuentecuipo($rowsinreccaja[1]);
											$auxfuente = $rowsinreccaja[1];
											if($auxfuente != ''){$varfuentes[] = $auxfuente;}
											else {$varfuentes[] = 'NO APLICA';}
										}
										else 
										{
											$varfuentes[] = 'NO APLICA';
											$varfuentes2[] = 'NO APLICA';
										}
										$vartercerochip[] = '';
										$varpolpublicas[] = '';
										$valrecactualscf[] = 0;
										$valrecactualccf[] = round($rowsinreccaja[0]);
										$valrecanteriorscf[] = 0;
										$valrecanteriorccf[] = 0;
										$valtotalcuenta[] = round($rowsinreccaja[0]);
										$tablasinfo[] = "Sin Recibo de Caja No: $rowsinreccaja[3]";
										$porcentaje2 = $cx * 100 / $totalcx;
										echo"
										<script>
											progres2 = '".round($porcentaje2)."';
											callprogress2(progres2);
											document.getElementById('titulosubproceso').innerHTML = 'Sin Recibo de Caja No: $rowsinreccaja[3] ($rowcta[0])';
										</script>";
										flush();
										ob_flush();
										usleep(1);
									}
									echo "
									<script>
										document.getElementById('progreso2').style.display='none';
										document.getElementById('titulosubproceso').innerHTML='';
									</script>";
								}
								//INGRESOS SSF
								{
									$cx = 0;
									$sqlingssf="
									SELECT pissf.valor, pissf.idrecibo
									FROM pptoingssf AS pissf
									INNER JOIN tesossfingreso_cab AS tissf
									ON pissf.idrecibo = tissf.id_recaudo AND pissf.cuenta = '$rowcta[0]'
									WHERE pissf.vigencia = '$vigusu' AND NOT(tissf.estado = 'N' OR tissf.estado = 'R') AND tissf.vigencia = '$vigusu' AND tissf.fecha BETWEEN '$fechai' AND '$fechaf'";//3
									$resingssf = mysqli_query($linkbd, $sqlingssf);
									$totalcx = mysqli_num_rows($resingssf);
									while ($rowingssf = mysqli_fetch_row($resingssf))
									{
										$numerocuenta[] = $rowcta[0];
										$nombrecuenta[] = $rowcta[1];
										$varcpc[] = 'NO APLICA';
										$varnorma1[] = $rownormas[0];
										$varnorma2[] = $rownormas[1];
										$varnorma3[] = $rownormas[2];
										$varnorma4[] = $rownormas[3];
										if($rownormas[0] == 'SI'){$valdestiesp[] = round($rowingssf[0]);}
										else {$valdestiesp[] = 0;}
										$varfuentes[] = 'NO APLICA';
										$varfuentes2[] = 'NO APLICA';
										$vartercerochip[] = '';
										$varpolpublicas[] = '';
										$valrecactualscf[] = 0;
										$valrecactualccf[] = round($rowingssf[0]);
										$valrecanteriorscf[] = 0;
										$valrecanteriorccf[] = 0;
										$valtotalcuenta[] = round($rowingssf[0]);
										$tablasinfo[] = "Ingreso SSF No: $rowingssf[1]";
										$porcentaje2 = $cx * 100 / $totalcx;
										echo"
										<script>
											progres2 = '".round($porcentaje2)."';
											callprogress2(progres2);
											document.getElementById('titulosubproceso').innerHTML = 'Ingreso SSF No: $rowingssf[1] ($rowcta[0])';
										</script>";
										flush();
										ob_flush();
										usleep(1);
									}
									echo "
									<script>
										document.getElementById('progreso2').style.display='none';
										document.getElementById('titulosubproceso').innerHTML='';
									</script>";
								}
								//NOTAS BANCARIAS
								{
									$cx = 0;
									$sqlnotasban="
									SELECT pnb.valor, pnb.idrecibo
									FROM pptonotasbanppto AS pnb
									INNER JOIN tesonotasbancarias_cab AS tnp
									ON tnp.id_comp = pnb.idrecibo AND pnb.cuenta = '$rowcta[0]'
									WHERE pnb.vigencia = '$vigusu' AND NOT(tnp.estado = 'R' OR tnp.estado = 'N') AND tnp.vigencia = '$vigusu' AND tnp.fecha BETWEEN '$fechai' AND '$fechaf'";//4
									$resnotasban = mysqli_query($linkbd, $sqlnotasban);
									$totalcx = mysqli_num_rows($resnotasban);
									while ($rownotasban = mysqli_fetch_row($resnotasban))
									{
										$numerocuenta[] = $rowcta[0];
										$nombrecuenta[] = $rowcta[1];
										$varcpc[] = 'NO APLICA';
										$varnorma1[] = $rownormas[0];
										$varnorma2[] = $rownormas[1];
										$varnorma3[] = $rownormas[2];
										$varnorma4[] = $rownormas[3];
										if($rownormas[0] == 'SI'){$valdestiesp[] = round($rownotasban[0]);}
										else {$valdestiesp[] = 0;}
										$varfuentes[] = 'NO APLICA';
										$varfuentes2[] = 'NO APLICA';
										$vartercerochip[] = '';
										$varpolpublicas[] = '';
										$valrecactualscf[] = 0;
										$valrecactualccf[] = round($rownotasban[0]);
										$valrecanteriorscf[] = 0;
										$valrecanteriorccf[] = 0;
										$valtotalcuenta[] = round($rownotasban[0]);
										$tablasinfo[] = "Nota Bancaria No: $rownotasban[1]";
										$porcentaje2 = $cx * 100 / $totalcx;
										echo"
										<script>
											progres2 = '".round($porcentaje2)."';
											callprogress2(progres2);
											document.getElementById('titulosubproceso').innerHTML = 'Nota Bancaria No: $rownotasban[1] ($rowcta[0])';
										</script>";
										flush();
										ob_flush();
										usleep(1);
									}
									echo "
									<script>
										document.getElementById('progreso2').style.display='none';
										document.getElementById('titulosubproceso').innerHTML='';
									</script>";
								}
								//RECAUDO TRANSFERENCIA
								{
									$cx = 0;
									$sqlrecatrans="
									SELECT pitp.valor, pitp.fuente, pitp.productoservicio, pitp.idrecibo
									FROM pptoingtranppto AS pitp
									INNER JOIN tesorecaudotransferencia AS titp
									ON pitp.idrecibo = titp.id_recaudo AND pitp.cuenta = '$rowcta[0]'
									WHERE pitp.vigencia = '$vigusu' AND NOT(titp.estado = 'N' OR titp.estado = 'R') AND titp.fecha BETWEEN '$fechai' AND '$fechaf' ";//5
									$resrecatrans = mysqli_query($linkbd, $sqlrecatrans);
									$totalcx = mysqli_num_rows($resrecatrans);
									while ($rowrecatrans=mysqli_fetch_row($resrecatrans))
									{
										$numerocuenta[] = $rowcta[0];
										$nombrecuenta[] = $rowcta[1];
										if($rowrecatrans[2] != '')
										{
											//$auxcpc = buscarcpccuipo($rowrecatrans[2]);
											$auxcpc = $rowrecatrans[2];
											if($auxcpc != ''){$varcpc[] = $auxcpc;}
											else {$varcpc[] = 'NO APLICA';}
										}
										else {$varcpc[] = 'NO APLICA';}
										$varnorma1[] = $rownormas[0];
										$varnorma2[] = $rownormas[1];
										$varnorma3[] = $rownormas[2];
										$varnorma4[] = $rownormas[3];
										if($rownormas[0] == 'SI'){$valdestiesp[] = round($rowrecatrans[0]);}
										else {$valdestiesp[] = 0;}
										if($rowrecatrans[1] != '')
										{
											$varfuentes2[] = $rowrecatrans[1];
											//$auxfuente = buscarfuentecuipo($rowrecatrans[1]);
											$auxfuente = $rowrecatrans[1];
											if($auxfuente != ''){$varfuentes[] = $auxfuente;}
											else {$varfuentes[] = 'NO APLICA';}
										}
										else 
										{
											$varfuentes[] = 'NO APLICA';
											$varfuentes2[] = 'NO APLICA';
										}
										$vartercerochip[] = '';
										$varpolpublicas[] = '';
										$valrecactualscf[] = 0;
										$valrecactualccf[] = round($rowrecatrans[0]);
										$valrecanteriorscf[] = 0;
										$valrecanteriorccf[] = 0;
										$valtotalcuenta[] = round($rowrecatrans[0]);
										$tablasinfo[] = "Recaudo Transferencia No: $rowrecatrans[3]";
										$porcentaje2 = $cx * 100 / $totalcx;
										echo"
										<script>
											progres2 = '".round($porcentaje2)."';
											callprogress2(progres2);
											document.getElementById('titulosubproceso').innerHTML = 'Recaudo Transferencia No: $rowrecatrans[3] ($rowcta[0])';
										</script>";
										flush();
										ob_flush();
										usleep(1);
									}
									echo "
									<script>
										document.getElementById('progreso2').style.display='none';
										document.getElementById('titulosubproceso').innerHTML='';
									</script>";
								}
								//RECAUDO TRANSFERENCIA SGR
								{
									/*$sqlrecatransgr="
									SELECT pitp.valor,
									FROM pptoingtranpptosgr AS pitp
									INNER JOIN tesorecaudotransferenciasgr AS titp
									ON pitp.idrecibo = titp.id_recaudo AND pitp.cuenta = '$rowcta[0]'
									WHERE pitp.vigencia = '$vigusu' AND NOT(titp.estado = 'N' OR titp.estado = 'R') AND titp.fecha BETWEEN '$fechai' AND '$fechaf' ";//6
									$resrecatransgr=mysqli_query($linkbd, $sqlrecatransgr);
									while ($rowrecatransgr=mysqli_fetch_row($resrecatransgr))
									{
										$numerocuenta[]=$rowcta[0];
										$nombrecuenta[]=$rowcta[1];
										$varcpc[]='NO APLICA';
										$varnorma1[]=$rownormas[0];
										$varnorma2[]=$rownormas[1];
										$varnorma3[]=$rownormas[2];
										$varnorma4[]=$rownormas[3];
										$valdestiesp[]='';
										$varfuentes[]='NO APLICA';
										$vartercerochip[]='';
										$varpolpublicas[]='';
										$valrecactualscf[]=0;
										$valrecactualccf[]=$rowrecatransgr[0];
										$valrecanteriorscf[]=0;
										$valrecanteriorccf[]=0;
										$valtotalcuenta[]=$rowrecatransgr[0];
										$tablasinfo[]='RECAUDO TRANSFERENCIA SGR';
									}*/
								}
								//RETENCIONES DE PAGO EGRESOS
								{
									$cx = 0;
									$sqlretencionE="
									SELECT prc.valor, prc.fuente, prc.productoservicio, prc.idrecibo
									FROM pptoretencionpago AS prc
									INNER JOIN tesoegresos AS trc 
									ON trc.id_egreso = prc.idrecibo AND prc.cuenta = '$rowcta[0]'
									WHERE prc.vigencia = '$vigusu' AND NOT(trc.estado='N') AND trc.fecha BETWEEN '$fechai' AND '$fechaf' AND trc.tipo_mov = '201' AND prc.tipo = 'egreso' AND NOT EXISTS (SELECT 1 FROM tesoegresos tra WHERE tra.id_egreso = trc.id_egreso AND tra.tipo_mov = '401') ";//7
									$resretencionE = mysqli_query($linkbd, $sqlretencionE);
									$totalcx = mysqli_num_rows($resretencionE);
									while ($rowretencionE = mysqli_fetch_row($resretencionE))
									{
										$numerocuenta[] = $rowcta[0];
										$nombrecuenta[] = $rowcta[1];
										if($rowretencionE[2] != '')
										{
											//$auxcpc = buscarcpccuipo($rowretencionE[2]);
											$auxcpc = $rowretencionE[2];
											if($auxcpc!=''){$varcpc[] = $auxcpc;}
											else {$varcpc[]='NO APLICA';}
										}
										else {$varcpc[] = 'NO APLICA';}
										$varnorma1[] = $rownormas[0];
										$varnorma2[] = $rownormas[1];
										$varnorma3[] = $rownormas[2];
										$varnorma4[] = $rownormas[3];
										if($rownormas[0] == 'SI'){$valdestiesp[] = round($rowretencionE[0]);}
										else {$valdestiesp[] = 0;}
										if($rowretencionE[1] != '')
										{
											$varfuentes2[] = $rowretencionE[1];
											//$auxfuente = buscarfuentecuipo($rowretencionE[1]);
											$auxfuente = $rowretencionE[1];
											if($auxfuente!=''){$varfuentes[] = $auxfuente;}
											else {$varfuentes[] = 'NO APLICA';}
										}
										else 
										{
											$varfuentes[] = 'NO APLICA';
											$varfuentes2[] = 'NO APLICA';
										}
										$vartercerochip[] = '';
										$varpolpublicas[] = '';
										$valrecactualscf[] = 0;
										$valrecactualccf[] = round($rowretencionE[0]);
										$valrecanteriorscf[] = 0;
										$valrecanteriorccf[]= 0;
										$valtotalcuenta[] = round($rowretencionE[0]);
										$tablasinfo[] = "Retencion de Pago Egreso No: $rowretencionE[3]";
										$porcentaje2 = $cx * 100 / $totalcx;
										echo"
										<script>
											progres2 = '".round($porcentaje2)."';
											callprogress2(progres2);
											document.getElementById('titulosubproceso').innerHTML = 'Retencion de Pago Egreso No: $rowretencionE[3] ($rowcta[0])';
										</script>";
										flush();
										ob_flush();
										usleep(1);
									}
									echo "
									<script>
										document.getElementById('progreso2').style.display='none';
										document.getElementById('titulosubproceso').innerHTML='';
									</script>";
								}
								//RETENCIONES DE PAGO ORDEN DE PAGO
								{
									$cx = 0;
									$sqlretencionO="
									SELECT prc.valor, prc.fuente, prc.productoservicio, prc.idrecibo, trc.medio.pago
									FROM pptoretencionpago AS prc
									INNER JOIN tesoordenpago AS trc 
									ON trc.id_orden = prc.idrecibo AND prc.cuenta = '$rowcta[0]'
									WHERE AND prc.vigencia = '$vigusu' AND NOT(trc.estado='N') AND trc.fecha BETWEEN '$fechai' AND '$fechaf' AND trc.tipo_mov = '201' AND prc.tipo = 'orden' AND NOT EXISTS (SELECT 1 FROM tesoordenpago tca WHERE tca.id_orden = trc.id_orden  AND tca.tipo_mov = '401') ";//8
									$resretencionO = mysqli_query($linkbd, $sqlretencionO);
									$totalcx = mysqli_num_rows($resretencionO);
									while ($rowretencionO = mysqli_fetch_row($resretencionO))
									{
										$numerocuenta[] = $rowcta[0];
										$nombrecuenta[] = $rowcta[1];
										if($rowretencionO[2] != '')
										{
											//$auxcpc = buscarcpccuipo($rowretencionO[2]);
											$auxcpc = $rowretencionO[2];
											if($auxcpc!=''){$varcpc[] = $auxcpc;}
											else {$varcpc[]='NO APLICA';}
										}
										{$varcpc[] = 'NO APLICA';}
										$varnorma1[] = $rownormas[0];
										$varnorma2[] = $rownormas[1];
										$varnorma3[] = $rownormas[2];
										$varnorma4[] = $rownormas[3];
										if($rownormas[0] == 'SI'){$valdestiesp[] = round($rowretencionO[0]);}
										else {$valdestiesp[] = 0;}
										if($rowretencionO[1] != '')
										{
											$varfuentes2[] = $rowretencionO[1];
											//$auxfuente = buscarfuentecuipo($rowretencionO[1]);
											$auxfuente = $rowretencionO[1];
											if($auxfuente != ''){$varfuentes[] = $auxfuente;}
											else {$varfuentes[] = 'NO APLICA';}
										}
										else 
										{
											$varfuentes[] = 'NO APLICA';
											$varfuentes2[] = 'NO APLICA';
										}
										$vartercerochip[] = '';
										$varpolpublicas[] = '';
										if($resretencionO[4] == '2')
										{
											$valrecactualscf[] = round($rowretencionO[0]);
											$valrecactualccf[] = 0;
										}
										else
										{
											$valrecactualscf[] = 0;
											$valrecactualccf[] = round($rowretencionO[0]);
										}
										$valrecanteriorscf[] = 0;
										$valrecanteriorccf[] = 0;
										$valtotalcuenta[] = round($rowretencionO[0]);
										$tablasinfo[] = "Retencion de Orden de Pago  No: $rowretencionO[3]";
										$porcentaje2 = $cx * 100 / $totalcx;
										echo"
										<script>
											progres2 = '".round($porcentaje2)."';
											callprogress2(progres2);
											document.getElementById('titulosubproceso').innerHTML = 'Retencion de Orden de Pago  No: $rowretencionO[3] ($rowcta[0])';
										</script>";
										flush();
										ob_flush();
										usleep(1);
									}
									echo "
									<script>
										document.getElementById('progreso2').style.display='none';
										document.getElementById('titulosubproceso').innerHTML='';
									</script>";
								}
								//SUPERAVIT PPTO
								{
									$cx = 0;
									$sqlsuperavit="
									SELECT psd.valor, psd.consvigencia
									FROM pptosuperavit AS ps
									INNER JOIN pptosuperavit_detalle AS psd
									ON ps.consvigencia = psd.consvigencia AND psd.cuenta = '$rowcta[0]';
									WHERE psd.vigencia = '$vigusu' AND NOT(ps.estado = 'N' OR psd.estado = 'R') AND ps.fecha BETWEEN '$fechai' AND '$fechaf' ";//9
									$ressuperavit = mysqli_query($linkbd, $sqlsuperavit);
									$totalcx = mysqli_num_rows($ressuperavit);
									while ($rowsuperavit = mysqli_fetch_row($ressuperavit))
									{
										$numerocuenta[] = $rowcta[0];
										$nombrecuenta[] = $rowcta[1];
										$varcpc[] = 'NO APLICA';
										$varnorma1[] = $rownormas[0];
										$varnorma2[] = $rownormas[1];
										$varnorma3[] = $rownormas[2];
										$varnorma4[] = $rownormas[3];
										if($rownormas[0] == 'SI'){$valdestiesp[] = round($rowsuperavit[0]);}
										else {$valdestiesp[] = 0;}
										$varfuentes[] = 'NO APLICA';
										$varfuentes2[] = 'NO APLICA';
										$vartercerochip[] = '';
										$varpolpublicas[] = '';
										$valrecactualscf[] = 0;
										$valrecactualccf[] = round($rowsuperavit[0]);
										$valrecanteriorscf[] = 0;
										$valrecanteriorccf[] = 0;
										$valtotalcuenta[] = round($rowsuperavit[0]);
										$tablasinfo[] = "Superavit PPTO No: $rowsuperavit[1]";
										$porcentaje2 = $cx * 100 / $totalcx;
										echo"
										<script>
											progres2 = '".round($porcentaje2)."';
											callprogress2(progres2);
											document.getElementById('titulosubproceso').innerHTML = 'Superavit PPTO No: $rowsuperavit[1] ($rowcta[0])';
										</script>";
										flush();
										ob_flush();
										usleep(1);
									}
									echo "
									<script>
										document.getElementById('progreso2').style.display='none';
										document.getElementById('titulosubproceso').innerHTML='';
									</script>";
								}
								//RESERVAS PPTO
								{
									$cx = 0;
									$sqlreservas="
									SELECT psd.valor, psd.consvigencia
									FROM pptoreservas ps
									INNER JOIN pptoreservas_det psd
									ON ps.consvigencia = psd.consvigencia AND psd.cuenta = '$rowcta[0]'
									WHERE psd.vigencia = '$vigusu' AND NOT(ps.estado = 'N' OR psd.estado = 'R') AND ps.fecha BETWEEN '$fechai' AND '$fechaf'";//10
									$resreservas = mysqli_query($linkbd, $sqlreservas);
									$totalcx = mysqli_num_rows($resreservas);
									while ($rowreservas = mysqli_fetch_row($resreservas))
									{
										$numerocuenta[] = $rowcta[0];
										$nombrecuenta[] = $rowcta[1];
										$varcpc[] = 'NO APLICA';
										$varnorma1[] = $rownormas[0];
										$varnorma2[] = $rownormas[1];
										$varnorma3[] = $rownormas[2];
										$varnorma4[] = $rownormas[3];
										if($rownormas[0] == 'SI'){$valdestiesp[] = round($rowreservas[0]);}
										else {$valdestiesp[] = 0;}
										$varfuentes[] = 'NO APLICA';
										$varfuentes2[] = 'NO APLICA';
										$vartercerochip[] = '';
										$varpolpublicas[] = '';
										$valrecactualscf[] = 0;
										$valrecactualccf[] = round($rowreservas[0]);
										$valrecanteriorscf[] = 0;
										$valrecanteriorccf[] = 0;
										$valtotalcuenta[] = round($rowreservas[0]);
										$tablasinfo[] = "Reservas PPTO No: $rowreservas[1]";
										$porcentaje2 = $cx * 100 / $totalcx;
										echo"
										<script>
											progres2 = '".round($porcentaje2)."';
											callprogress2(progres2);
											document.getElementById('titulosubproceso').innerHTML = 'Reservas PPTO No: $rowreservas[1] ($rowcta[0])';
										</script>";
										flush();
										ob_flush();
										usleep(1);
									}
									echo "
									<script>
										document.getElementById('progreso2').style.display='none';
										document.getElementById('titulosubproceso').innerHTML='';
									</script>";
								}
								//INGRESOS PRESUPUESTO
								{
									/*$sqlingpresu="
									SELECT psd.valor
									FROM pptoingresopresupuesto AS ps
									INNER JOIN pptoingresopresupuesto_det AS psd
									ON ps.consvigencia = psd.consvigencia AND psd.cuenta = '$rowcta[0]'
									WHERE psd.vigencia = '$vigusu' AND NOT(ps.estado = 'N' OR psd.estado='R') AND ps.fecha BETWEEN '$fechai' AND '$fechaf'";//11
									$resingpresu=mysqli_query($linkbd, $sqlingpresu);
									while ($rowingpresu=mysqli_fetch_row($resingpresu))
									{
										$numerocuenta[]=$rowcta[0];
										$nombrecuenta[]=$rowcta[1];
										$varcpc[]='NO APLICA';
										$varnorma1[]=$rownormas[0];
										$varnorma2[]=$rownormas[1];
										$varnorma3[]=$rownormas[2];
										$varnorma4[]=$rownormas[3];
										$valdestiesp[]='';
										$varfuentes[]='NO APLICA';
										$vartercerochip[]='';
										$varpolpublicas[]='';
										$valrecactualscf[]=0;
										$valrecactualccf[]=$rowingpresu[0];
										$valrecanteriorscf[]=0;
										$valrecanteriorccf[]=0;
										$valtotalcuenta[]=$rowingpresu[0];
										$tablasinfo[]='INGRESOS PRESUPUESTO';
									}*/
								}
								//SIN RECIBO DE CAJA SP
								{
									$cx = 0;
									$sqlsp="
									SELECT TB2.valor, TB2.idrecibo, TB2.fuente, TB2.productoservicio, TB2.medio_pago
									FROM tesosinreciboscajasp AS TB1
									INNER JOIN pptosinrecibocajaspppto AS TB2
									ON TB1.id_recibos = TB2.idrecibo AND TB1.vigencia = TB2.vigencia AND TB2.cuenta = '$rowcta[0]'
									WHERE TB1.estado = 'S' AND TB2.vigencia = '$vigusu' AND TB1.fecha BETWEEN '$fechai' AND '$fechaf' ";//12
									$ressp = mysqli_query($linkbd, $sqlsp);
									$totalcx = mysqli_num_rows($ressp);
									while ($rowsp = mysqli_fetch_row($ressp))
									{
										$numerocuenta[] = $rowcta[0];
										$nombrecuenta[] = $rowcta[1];
										if($rowsp[3] != '')
										{
											//$auxcpc = buscarcpccuipo($rowsp[3]);
											$auxcpc = $rowsp[3];
											if($auxcpc !=''){$varcpc[] = $auxcpc;}
											else {$varcpc[] = 'NO APLICA';}
										}
										$varnorma1[] = $rownormas[0];
										$varnorma2[] = $rownormas[1];
										$varnorma3[] = $rownormas[2];
										$varnorma4[] = $rownormas[3];
										if($rownormas[0] == 'SI'){$valdestiesp[] = round($rowsp[0]);}
										else {$valdestiesp[] = 0;}
										if($rowsp[2] != '')
										{
											$varfuentes2[] = $rowsp[2];
											//$auxfuente = buscarfuentecuipo($rowsp[2]);
											$auxfuente = $rowsp[2];
											if($auxfuente != ''){$varfuentes[] = $auxfuente;}
											else {$varfuentes[] = 'NO APLICA';}
											$varfuentes[] = $auxfuente;
										}
										else 
										{
											$varfuentes[] = 'NO APLICA';
											$varfuentes2[] = 'NO APLICA';
										}
										$vartercerochip[] = '';
										$varpolpublicas[] = '';
										if($rowsp[4] == "SSF")
										{
											$valrecactualscf[] = round($rowsp[0]);
											$valrecactualccf[] = 0;
										}
										else
										{
											$valrecactualscf[] = 0;
											$valrecactualccf[] = round($rowsp[0]);
										}
										$valrecanteriorscf[] = 0;
										$valrecanteriorccf[] = 0;
										$valtotalcuenta[] = round($rowsp[0]);
										$tablasinfo[] = "Sin Recibo de Caja SP No: $rowsp[1]";
										$porcentaje2 = $cx * 100 / $totalcx;
										echo"
										<script>
											progres2 = '".round($porcentaje2)."';
											callprogress2(progres2);
											document.getElementById('titulosubproceso').innerHTML = 'Sin Recibo de Caja SP No: $rowsp[1] ($rowcta[0])';
										</script>";
										flush();
										ob_flush();
										usleep(1);
									}
									echo "
									<script>
										document.getElementById('progreso2').style.display='none';
										document.getElementById('titulosubproceso').innerHTML='';
									</script>";
								}
								//RECIBOS DE CAJA SP
								{
									$cx = 0;
									$sqlrsp="
									SELECT T1.valor, T1.id_recibos
									FROM servreciboscaja_det AS T1
									INNER JOIN servreciboscaja AS T2
									ON T2.id_recibos = T1.id_recibos AND T1.cuentapres = '$rowcta[0]'
									WHERE T2.estado = 'S' AND T2.vigencia = '$vigusu' AND T2.fecha BETWEEN '$fechai' AND '$fechaf'";//13
									$resrsp = mysqli_query($linkbd, $sqlrsp);
									$totalcx = mysqli_num_rows($resrsp);
									while ($rowrsp = mysqli_fetch_row($resrsp))
									{
										$numerocuenta[] = $rowcta[0];
										$nombrecuenta[] = $rowcta[1];
										$varcpc[] = 'NO APLICA';
										$varnorma1[] = $rownormas[0];
										$varnorma2[] = $rownormas[1];
										$varnorma3[] = $rownormas[2];
										$varnorma4[] = $rownormas[3];
										if($rownormas[0] == 'SI'){$valdestiesp[] = round($rowrsp[0]);}
										else {$valdestiesp[] = 0;}
										$varfuentes[] = 'NO APLICA';
										$varfuentes2[] = 'NO APLICA';
										$vartercerochip[] = '';
										$varpolpublicas[] = '';
										$valrecactualscf[] = 0;
										$valrecactualccf[] = round($rowrsp[0]);
										$valrecanteriorscf[] = 0;
										$valrecanteriorccf[] = 0;
										$valtotalcuenta[] = round($rowrsp[0]);
										$tablasinfo[] = "Recibo de Caja SP No: $rowrsp[1]";
										$porcentaje2 = $cx * 100 / $totalcx;
										echo"
										<script>
											progres2 = '".round($porcentaje2)."';
											callprogress2(progres2);
											document.getElementById('titulosubproceso').innerHTML = 'Recibo de Caja SP No: $rowrsp[1] ($rowcta[0])';
										</script>";
										flush();
										ob_flush();
										usleep(1);
									}
									echo "
									<script>
										document.getElementById('progreso2').style.display='none';
										document.getElementById('titulosubproceso').innerHTML='';
									</script>";
								}
								//SUPERAVIT TESO
								{
									$cx = 0;
									$sqlavit = "
									SELECT tsad.valor, tsad.fuente, tsad.id_tesosuperavit
									FROM tesosuperavit AS tsa
									INNER JOIN tesosuperavit_det AS tsad
									ON tsa.id = tsad.id_tesosuperavit AND tsad.rubro = '$rowcta[0]'
									WHERE tsa.estado = 'S' AND tsa.vigencia = '$vigusu' AND tsa.fecha BETWEEN '$fechai' AND '$fechaf'";//14
									$resavit = mysqli_query($linkbd, $sqlavit);
									$totalcx = mysqli_num_rows($resavit);
									while ($rowavit = mysqli_fetch_row($resavit))
									{
										$numerocuenta[] = $rowcta[0];
										$nombrecuenta[] = $rowcta[1];
										$varcpc[] = 'NO APLICA';
										$varnorma1[] = $rownormas[0];
										$varnorma2[] = $rownormas[1];
										$varnorma3[] = $rownormas[2];
										$varnorma4[] = $rownormas[3];
										if($rownormas[0] == 'SI'){$valdestiesp[] = round($rowavit[0]);}
										else {$valdestiesp[] = 0;}
										if($rowavit[1] != '')
										{
											$varfuentes2[] = $rowavit[1];
											//$auxfuente = buscarfuentecuipo($rowavit[1]);
											$auxfuente = $rowavit[1];
											if($auxfuente != ''){$varfuentes[] = $auxfuente;}
											else {$varfuentes[] = 'NO APLICA';}
										}
										else 
										{
											$varfuentes[] = 'NO APLICA';
											$varfuentes2[] = 'NO APLICA';
										}
										$vartercerochip[] = '';
										$varpolpublicas[] = '';
										$valrecactualscf[] = 0;
										$valrecactualccf[] = round($rowavit[0]);
										$valrecanteriorscf[] = 0;
										$valrecanteriorccf[] = 0;
										$valtotalcuenta[] = round($rowavit[0]);
										$tablasinfo[] = "Superavit TESO No: $rowavit[2]";
										$porcentaje2 = $cx * 100 / $totalcx;
										echo"
										<script>
											progres2 = '".round($porcentaje2)."';
											callprogress2(progres2);
											document.getElementById('titulosubproceso').innerHTML = 'Superavit TESO No: $rowavit[2] ($rowcta[0])';
										</script>";
										flush();
										ob_flush();
										usleep(1);
									}
									echo "
									<script>
										document.getElementById('progreso2').style.display='none';
										document.getElementById('titulosubproceso').innerHTML='';
									</script>";
								}
								//RESERVAS TESO
								{
									$cx = 0;
									$sqlReservas = "
									SELECT tsad.valor,tsad.fuente, tsa.id
									FROM tesoreservas AS tsa
									INNER JOIN tesoreservas_det AS tsad
									ON tsa.id = tsad.id_reserva AND tsad.rubro = '$rowcta[0]'
									WHERE tsa.estado = 'S' AND tsa.vigencia = '$vigusu' AND tsa.fecha BETWEEN '$fechai' AND '$fechaf'";//15
									$resReservas = mysqli_query($linkbd, $sqlReservas);
									$totalcx = mysqli_num_rows($resReservas);
									while ($rowReservas = mysqli_fetch_row($resReservas))
									{
										$numerocuenta[] = $rowcta[0];
										$nombrecuenta[] = $rowcta[1];
										$varcpc[] = 'NO APLICA';
										$varnorma1[] = $rownormas[0];
										$varnorma2[] = $rownormas[1];
										$varnorma3[] = $rownormas[2];
										$varnorma4[] = $rownormas[3];
										if($rownormas[0] == 'SI'){$valdestiesp[] = round($rowReservas[0]);}
										else {$valdestiesp[] = 0;}
										if($rowReservas[1] != '')
										{
											$varfuentes2[] = $rowReservas[1];
											//$auxfuente = buscarfuentecuipo($rowReservas[1]);
											$auxfuente = $rowReservas[1];
											if($auxfuente != ''){$varfuentes[] = $auxfuente;}
											else {$varfuentes[] = 'NO APLICA';}
										}
										else 
										{
											$varfuentes[] = 'NO APLICA';
											$varfuentes2[] = 'NO APLICA';
										}
										$vartercerochip[] = '';
										$varpolpublicas[] = '';
										$valrecactualscf[] = 0;
										$valrecactualccf[] = round($rowReservas[0]);
										$valrecanteriorscf[] = 0;
										$valrecanteriorccf[] = 0;
										$valtotalcuenta[] = round($rowReservas[0]);
										$tablasinfo[] = 'Reservas Teso. N: '.$rowReservas[2];
										$porcentaje2 = $cx * 100 / $totalcx;
										echo"
										<script>
											progres2 = '".round($porcentaje2)."';
											callprogress2(progres2);
											document.getElementById('titulosubproceso').innerHTML = 'Reservas Teso. N: $rowReservas[2] ($rowcta[0])';
										</script>";
										flush();
										ob_flush();
										usleep(1);
									}
									echo "
									<script>
										document.getElementById('progreso2').style.display='none';
										document.getElementById('titulosubproceso').innerHTML='';
									</script>";
								}
								$porcentaje = $c * 100 / $totalcli;
								echo"
								<script>
									progres='".round($porcentaje)."';callprogress(progres);
									document.getElementById('totalcuentas').innerHTML='Recolectando Información: $numid / $totalcli';
								</script>";
								flush();
								ob_flush();
								usleep(5);
							}
							$iter = "zebra1";
							$iter2 = "zebra2";
							$c=0;
							$totalcli=count($numerocuenta);
							for($xx=0;$xx<$totalcli;$xx++)
							{
								$c++;
								$idtemporal++;
								if($varnorma4[$xx]==0 || $varnorma4[$xx]=="0000-00-00"){$fechacott="1900-01-01";}
								else {$fechacott = $varnorma4[$xx];}
								$sqltabla2 = "INSERT INTO ".$_SESSION['tablatemporal']." (id, codigocuenta, nombrecuenta, cpc, aplicadestinacion, tiponorma, numeronorma, fechanorma, valordestinacionesp, fuente, tercerochip, politicapublica, recaudoactualscf, recaudoactualccf, recaudoanteriorscf, recaudoanteriorccf, totalrecaudo, tipotabla, fuente2) VALUES ('$idtemporal', '$numerocuenta[$xx]', '$nombrecuenta[$xx]', '$varcpc[$xx]', '$varnorma1[$xx]', '$varnorma2[$xx]', '$varnorma3[$xx]', '$fechacott', '$valdestiesp[$xx]', '$varfuentes[$xx]', '$vartercerochip[$xx]', '$varpolpublicas[$xx]', '$valrecactualscf[$xx]', '$valrecactualccf[$xx]', '$valrecanteriorscf[$xx]', '$valrecanteriorccf[$xx]', '$valtotalcuenta[$xx]', '$tablasinfo[$xx]','$varfuentes2[$xx]')";
								mysqli_query($linkbd,$sqltabla2);
								$porcentaje = $c * 100 / $totalcli;
								echo"
								<script>
									progres='".round($porcentaje)."';callprogress(progres);
									document.getElementById('totalcuentas').innerHTML='Procesando Información $c / $totalcli';
								</script>";
								flush();
								ob_flush();
								usleep(1);
							}
							$c=0;
							$sqlver = "SELECT id, codigocuenta, nombrecuenta, cpc, aplicadestinacion, tiponorma, numeronorma, fechanorma, SUM(valordestinacionesp), fuente, tercerochip, politicapublica, SUM(recaudoactualscf), SUM(recaudoactualccf), SUM(recaudoanteriorscf), SUM(recaudoanteriorccf), SUM(totalrecaudo),fuente2 FROM ".$_SESSION['tablatemporal']." GROUP BY codigocuenta, cpc, fuente2 ORDER BY id ASC";
							$resver = mysqli_query($linkbd,$sqlver);
							$totalcli = mysqli_num_rows($resver);
							while ($rowver = mysqli_fetch_row($resver))
							{
								$c++;
								$porcentaje = $c * 100 / $totalcli;
								switch ($rowver[5])
								{
									case "0":	$tiponorma = "No Aplica"; break;
									case "1":	$tiponorma = "Decreto"; break;
									case "2":	$tiponorma = "Acuerdo"; break;
									case "3":	$tiponorma = "Ordenanza"; break;
									case "4":	$tiponorma = "Resolución";
								}
								$fechapartes = explode('-', $rowver[7]);
								$fechainforme = "$fechapartes[2]-$fechapartes[1]-$fechapartes[0]";
								echo "
								<tr class=$iter>
									<td class='titulos2'>
										<a onClick=\"detalleb('$c','$rowver[1]','$rowver[3]','$rowver[17]')\" style='cursor:pointer;'>
											<img id='img$c' src='imagenes/plus.gif'>
										</a>
									</td>
									<td style='width:10%;'>$rowver[1]</td>
									<td>$rowver[2]</td>
									<td>$rowver[3]</td>
									<td>$rowver[4]</td>
									<td>$tiponorma</td>
									<td>$rowver[6]</td>
									<td>$fechainforme</td>
									<td style='text-align:right;'>$".number_format($rowver[8],0,',','.')."</td>
									<td>$rowver[17]</td>
									<td>$rowver[10]</td>
									<td>$rowver[11]</td>
									<td style='text-align:right;'>$".number_format($rowver[12],0,',','.')."</td>
									<td style='text-align:right;'>$".number_format($rowver[13],0,',','.')."</td>
									<td style='text-align:right;'>$".number_format($rowver[14],0,',','.')."</td>
									<td style='text-align:right;'>$".number_format($rowver[15],0,',','.')."</td>
									<td style='text-align:right;'>$".number_format($rowver[16],0,',','.')."</td>
								</tr>
								<tr>
									<td align='center'></td>
									<td colspan='16'>
										<div id='detalle$c' style='display:none'></div>
									</td>
								</tr>
								<script>
									progres='".round($porcentaje)."';callprogress(progres);
									document.getElementById('totalcuentas').innerHTML='Visualizar Información $c / $totalcli';
								</script>";
								flush();
								ob_flush();
								usleep(1);
								$aux=$iter;
								$iter=$iter2;
								$iter2=$aux;
								if($rowver[3]=='NO APLICA'){$valorcpc=0;}
								else {$valorcpc=$rowver[3];}
								if($rowver[9]=='NO APLICA'){$valorfuente=0;}
								else{$valorfuente=$rowver[9];}
								if($rowver[4]=='SI'){$valordesesp='true';}
								else {$valordesesp='false';}
								if($rowver[6]==''){$valornumnorma=0;}
								else{$valornumnorma=$rowver[6];}
								if($rowver[10]==''){$valortercerochip=0;}
								else {$valortercerochip=$rowver[10];}
								if($rowver[11]==''){$valorpolitica=0;}
								else {$valorpolitica=$rowver[11];}
								fputs($Descriptor1, "D;".$rowver[1].";".$valorcpc.";".$valorfuente.";".$valordesesp.";".$rowver[5].";".$valornumnorma.";".$fechainforme.";".$rowver[8].";".$valortercerochip.";".$valorpolitica.";".$rowver[12].";".$rowver[13].";".$rowver[14].";".$rowver[15].";".$rowver[16]."\r\n");
								//fputs($Descriptor2, "D\t".$rowver[1]."\t".$valorcpc."\t".$valorfuente."\t".$valordesesp."\t".$rowver[5]."\t".$valornumnorma."\t".$fechainforme."\t".$rowver[8]."\t".$valortercerochip."\t".$valorpolitica."\t".$rowver[12]."\t". $rowver[13]."\t".$rowver[14]."\t".$rowver[15]."\t".$rowver[16]."\r\n");
								
								fputs($Descriptor2, "D\t".$rowver[1]."\t".$valorcpc."\t".'0'."\t".$valorfuente."\t".$valortercerochip."\t".$valorpolitica."\t".$fechainforme."\t".$tiponorma."\t".$rowver[12]."\t".$rowver[13]."\t".$rowver[14]."\t".$rowver[15]."\t".$rowver[16]."\r\n");
								fputs($Descriptor3,"D\t".$rowver[1]."\t".$valorcpc."\t".$rowver[17]."\t".$valordesesp."\t".$rowver[5]."\t".$valornumnorma."\t".$fechainforme."\t".$rowver[8]."\t".$valortercerochip."\t".$valorpolitica."\t".$rowver[12]."\t".$rowver[13]."\t".$rowver[14]."\t".$rowver[15]."\t".$rowver[16]."\r\n");
							}
							echo "
								</table>
							</div>";
							fclose($Descriptor1);
							fclose($Descriptor2);
							fclose($Descriptor3);
							echo "
							<script>document.getElementById('progreso').style.display='none';</script>";
							
						}break;
						case 3: //PROGRAMACION DE GASTOS
						{
							unset ($numerocuenta,$nombrecuenta,$secpresupuestal,$vigenciagasto,$sector,$programatico,$bpin,$apropiaini, $apropiadef,$registroinfo,$numerocuentaaux);
							$numid = $c = $idtemporal = 0;
							$namearch = "archivos/".$_SESSION['usuario']."informecgr".$fec.".csv";
							$Descriptor1 = fopen($namearch, "w+");
							$namearch2 = "archivos/".$informes[$_POST['reporte']].".txt";
							$Descriptor2 = fopen($namearch2, "w+");
							echo "
							<div class='subpantallap' style='height:66.5%; width:99.6%;'>
								<table class='inicio' align='center'>
									<tr>
										<td class='titulos' colspan='16' ><p class='neon_titulos' id='titulogento'></p></td>
									</tr>
									<tr>
										<td colspan='4' id='totalcuentas'></td>
										<td colspan='4'>
											<div id='progreso' class='ProgressBar' style='display:none; float:left'>
												<div class='ProgressBarText'><span id='getprogress'></span>&nbsp;% </div>
												<div id='getProgressBarFill'></div>
											</div>
										</td>
									</tr>
									<tr>
										<td class='titulos2' style='width:2px;'><img src='imagenes/plus.gif'></td>
										<td class='titulos2' colspan='2'>&Aacute;RBOL DE CONCEPTOS</td>
										<td class='titulos2'>VIGENCIA DEL GASTO</td>
										<td class='titulos2'>SECCIÓN PRESUPUESTAL</td>
										<td class='titulos2'>PROGRAMATICO MGA </td>
										<td class='titulos2'>BPIN</td>
										<td class='titulos2'>APROPIACI&Oacute;N INICIAL</td>
										<td class='titulos2'>APROPIACI&Oacute;N DEFINITIVA</td>
									</tr>";
							$mes2 = substr($_POST['periodo'], 3, 2);
							$_POST['fecha'] = "01/01/$vigusu";
							$_POST['fecha2'] = intval(date("t", $mes2))."/$mes2/$vigusu";
							preg_match("/([0-9]{2})\/([0-9]{2})\/([0-9]{4})/", $_POST['fecha'],$fecha);
							$fechai = "$fecha[3]-$fecha[2]-$fecha[1]";
							preg_match("/([0-9]{2})\/([0-9]{2})\/([0-9]{4})/", $_POST['fecha2'],$fecha);
							$fechaf = "$fecha[3]-$fecha[2]-$fecha[1]";

							fputs($Descriptor1, "S;".$_POST['codent'].";".$_POST['periodo'].";".$vigusu.";C_PROGRAMACION_DE_GASTOS\r\n");
							fputs($Descriptor1, "S;ARBOL DE CONCEPTOS;VIGENCIA DEL GASTO;SECCION PRESUPUESTAL;PROGRAMATICO MGA; BPIN;APROPIACION INICIAL;APROPIACION DEFINITIVA\r\n");
							fputs($Descriptor2, "S\t".$_POST['codent']."\t".$_POST['periodo']."\t".$vigusu. "\tC_PROGRAMACION_DE_GASTOS\r\n");
							
							if($_SESSION['tablatemporal'] != '' && $_SESSION['tablatemporal'] != null)
							{
								$sqlr = "DROP TABLE ".$_SESSION['tablatemporal'];
								mysqli_query($linkbd,$sqlr);
								$sqlr = "CREATE TABLE ".$_SESSION['tablatemporal']." (id int(11), codigocuenta varchar(100), nombrecuenta varchar(200), seccpresupuestal varchar(10), vigenciagastos varchar(10), sector varchar(10), programatico varchar(50), bpin varchar(20),apropiacionini double, apropiaciondef double, registro varchar(100), codigocuentaaux varchar(100))";
								mysqli_query($linkbd,$sqlr);
							}
							
							$sqlredglobal ="SELECT codigo,base,nombre FROM redglobal ORDER BY id ASC";
							$resredglobal = mysqli_query($linkbd,$sqlredglobal);
							while ($rowredglobal = mysqli_fetch_row($resredglobal))
							{
								$c = $numid = 0;
								echo"
								<script>document.getElementById('titulogento').innerHTML='.: C. PROGRAMACI&Oacute;N DE GASTOS: $rowredglobal[2]';</script>";
								$linkmulti = conectar_Multi($rowredglobal[1]);
								$linkmulti -> set_charset("utf8");
								$maxVersion = ultimaVersionGastosCCPETmulti($rowredglobal[1]);
								$sqlcta ="SELECT codigo,nombre FROM cuentasccpet WHERE version = '$maxVersion' AND municipio = '1' AND tipo = 'C' ORDER BY id ASC";
								$rescta = mysqli_query($linkmulti,$sqlcta);
								$totalcli= mysqli_num_rows($rescta);
								while ($rowcta = mysqli_fetch_row($rescta))
								{
									$numid++;
									$c++;
									//INICIAL GASTOS BIENES TRANSPORTABLES
									{
										$sqlBienesTrans = "SELECT valor, id FROM ccpetinicialigastosbienestransportables WHERE cuenta = '$rowcta[0]' AND vigencia = '$vigusu'";
										$resBienesTrans = mysqli_query($linkmulti, $sqlBienesTrans);
										while ($rowBienesTrans = mysqli_fetch_row($resBienesTrans))
										{
											if( substr($rowcta[0],0,4) != '2.3.' )
											{
												$numerocuenta[] = $rowcta[0];
												$nombrecuenta[] = $rowcta[1];
											}
											elseif($rowredglobal[1] == 'regalias')
											{
												$numerocuenta[] = $rowcta[0];
												$nombrecuenta[] = $rowcta[1];
											}
											else
											{
												$numerocuenta[] = '2.99';
												$nombrecuenta[] = 'CONSOLIDADO';
											}
											$numerocuentaaux[] = $rowcta[0];
											$secpresupuestal[] = $rowredglobal[0];
											$vigenciagasto[] = 1;
											$sector[] = 0;
											$programatico[] = 0;
											$bpin[] = 0;
											$apropiaini[] = round($rowBienesTrans[0]);
											$apropiadef[] = round($rowBienesTrans[0]);
											$registroinfo[] = "Bienes Trasportables No: ".$rowBienesTrans[1];
										}
									}
									//INICIAL SERVICIOS
									{
										$sqlServicios = "SELECT valor, id FROM ccpetinicialservicios WHERE cuenta = '$rowcta[0]' AND vigencia = '$vigusu'";
										$resServicios = mysqli_query($linkmulti, $sqlServicios);
										while ($rowServicios = mysqli_fetch_row($resServicios))
										{
											if( substr($rowcta[0],0,4) != '2.3.' )
											{
												$numerocuenta[] = $rowcta[0];
												$nombrecuenta[] = $rowcta[1];
											}
											elseif($rowredglobal[1] == 'regalias')
											{
												$numerocuenta[] = $rowcta[0];
												$nombrecuenta[] = $rowcta[1];
											}
											else
											{
												$numerocuenta[] = '2.99';
												$nombrecuenta[] = 'CONSOLIDADO';
											}
											$numerocuentaaux[] = $rowcta[0];
											$secpresupuestal[] = $rowredglobal[0];
											$vigenciagasto[] = 1;
											$sector[] = 0;
											$programatico[] = 0;
											$bpin[] = 0;
											$apropiaini[] = round($rowServicios[0]);
											$apropiadef[] = round($rowServicios[0]);
											$registroinfo[] = "Servicios No: ".$rowServicios[1];
										}
									}
									//INICIAL VALOR GASTOS
									{
										$sqlValorGastos = "SELECT valor, id FROM ccpetinicialvalorgastos WHERE cuenta = '$rowcta[0]' AND vigencia = '$vigusu'";
										$resValorGastos = mysqli_query($linkmulti, $sqlValorGastos);
										while ($rowValorGastos = mysqli_fetch_row($resValorGastos))
										{
											if( substr($rowcta[0],0,4) != '2.3.' )
											{
												$numerocuenta[] = $rowcta[0];
												$nombrecuenta[] = $rowcta[1];
											}
											elseif($rowredglobal[1] == 'regalias')
											{
												$numerocuenta[] = $rowcta[0];
												$nombrecuenta[] = $rowcta[1];
											}
											else
											{
												$numerocuenta[] = '2.99';
												$nombrecuenta[] = 'CONSOLIDADO';
											}
											$numerocuentaaux[] = $rowcta[0];
											$secpresupuestal[] = $rowredglobal[0];
											$vigenciagasto[] = 1;
											$sector[] = 0;
											$programatico[] = 0;
											$bpin[] = 0;
											$apropiaini[] = round($rowValorGastos[0]);
											$apropiadef[] = round($rowValorGastos[0]);
											$registroinfo[] = "Gastos No: ".$rowValorGastos[1];
										}
									}
									//PROYECTOS PRESUPUESTO
									{
										$sqlrProyectos = "
										SELECT PD.valorcsf, PD.valorssf, PD.indicador_producto, P.codigo, P.id
										FROM ccpproyectospresupuesto AS P
										INNER JOIN ccpproyectospresupuesto_presupuesto AS PD
										ON P.id = PD.codproyecto AND PD.rubro = '$rowcta[0]'
										WHERE P.vigencia = '$vigusu'";
										$resProyectos = mysqli_query($linkmulti, $sqlrProyectos);
										while ($rowProyectos = mysqli_fetch_row($resProyectos))
										{
											if( substr($rowcta[0],0,4) != '2.3.' )
											{
												$numerocuenta[] = $rowcta[0];
												$nombrecuenta[] = $rowcta[1];
											}
											elseif($rowredglobal[1] == 'regalias')
											{
												$numerocuenta[] = $rowcta[0];
												$nombrecuenta[] = $rowcta[1];
											}
											else
											{
												$numerocuenta[] = '2.99';
												$nombrecuenta[] = 'CONSOLIDADO';
											}
											$numerocuentaaux[] = $rowcta[0];
											$sector[] = substr($rowProyectos[2],0, 2);
											$codm1 = substr($rowProyectos[2],0,4);
											/*$sqlproducto = "SELECT concuipom1 FROM ccpetproductos WHERE codigo_indicador = '$codm1'";
											$resproducto = mysqli_query($linkbd, $sqlproducto);
											$rowproducto = mysqli_fetch_row($resproducto);
											if($rowproducto[0] == ''){$programatico[] = 0;}
											else {$programatico[] = $rowproducto[0];}*/
											if($codm1 == ''){$programatico[] = 0;}
											else {$programatico[] = $codm1;}
											$secpresupuestal[] = $rowredglobal[0];
											$vigenciagasto[] = 1;
											if($rowProyectos[3] == ''){$bpin[] = 0;}
											else {$bpin[] = $rowProyectos[3];}
											$apropiaini[] = round($rowProyectos[0]+$rowProyectos[1]);
											$apropiadef[] = round($rowProyectos[0]+$rowProyectos[1]);
											$registroinfo[] = "Proyectos No: ".$rowProyectos[3];
										}
									}
									//ADICIONES
									{
										$sqlAdicion = "SELECT valor, id_adicion, vigencia_gasto FROM ccpetadiciones WHERE cuenta = '$rowcta[0]' AND vigencia = '$vigusu' AND estado != 'N' AND fecha BETWEEN '$fechai' AND '$fechaf'";
										$resAdicion = mysqli_query($linkmulti, $sqlAdicion);
										while ($rowAdicion = mysqli_fetch_row($resAdicion))
										{
											if( substr($rowcta[0],0,4) != '2.3.' )
											{
												$numerocuenta[] = $rowcta[0];
												$nombrecuenta[] = $rowcta[1];
											}
											elseif($rowredglobal[1] == 'regalias')
											{
												$numerocuenta[] = $rowcta[0];
												$nombrecuenta[] = $rowcta[1];
											}
											else
											{
												$numerocuenta[] = '2.99';
												$nombrecuenta[] = 'CONSOLIDADO';
											}
											$numerocuentaaux[] = $rowcta[0];
											$secpresupuestal[] = $rowredglobal[0];
											if($rowAdicion[1] != ''){$vigenciagasto[] = $rowAdicion[2];}
											else {$vigenciagasto[] = 1;}
											$sector[] = 0;
											$programatico[] = 0;
											$bpin[] = 0;
											$apropiaini[] = 0;
											$apropiadef[] = round($rowAdicion[0]);
											$registroinfo[] = "Adiciones No: ".$rowAdicion[1];
										}
									}
									//ADICION INVERSION
									{
										$sqlAdicionInversion = "SELECT PD.valorcsf, PD.vigencia_gasto, P.id, PD.indicador_producto, P.codigo FROM ccpetadicion_inversion AS P, ccpetadicion_inversion_detalles AS PD, ccpetacuerdos AS CA WHERE P.id = PD.codproyecto AND P.id_acuerdo = CA.id_acuerdo AND CA.fecha BETWEEN '$fechai' AND '$fechaf' AND PD.rubro = '$rowcta[0]' AND P.vigencia = '$vigusu'";
										$resAdicionInversion = mysqli_query($linkmulti, $sqlAdicionInversion);
										while ($rowAdicionInversion = mysqli_fetch_row($resAdicionInversion))
										{
											if( substr($rowcta[0],0,4) != '2.3.' )
											{
												$numerocuenta[] = $rowcta[0];
												$nombrecuenta[] = $rowcta[1];
											}
											elseif($rowredglobal[1] == 'regalias')
											{
												$numerocuenta[] = $rowcta[0];
												$nombrecuenta[] = $rowcta[1];
											}
											else
											{
												$numerocuenta[] = '2.99';
												$nombrecuenta[] = 'CONSOLIDADO';
											}
											$numerocuentaaux[] = $rowcta[0];
											$secpresupuestal[] = $rowredglobal[0];
											if($rowAdicionInversion[1]!=''){$vigenciagasto[] = $rowAdicionInversion[1];}
											else {$vigenciagasto[] = 1;}
											$sector[] = 0;
											//$codm1=substr($rowAdicionInversion[3],0,4);
											/*$sqlproducto = "SELECT concuipom1 FROM ccpetproductos WHERE codigo_indicador = '$rowAdicionInversion[3]'";
											$resproducto = mysqli_query($linkbd, $sqlproducto);
											$rowproducto = mysqli_fetch_row($resproducto);
											if($rowproducto[0] == ''){$programatico[] = 0;}
											else {$programatico[] = $rowproducto[0];}*/
											if($rowAdicionInversion[3] == ''){$programatico[] = 0;}
											else {$programatico[] = substr($rowAdicionInversion[3],0,4);}
											if($rowAdicionInversion[4] == ''){$bpin[] = 0;}
											else {$bpin[] = $rowAdicionInversion[4];}
											$apropiaini[] = 0;
											$apropiadef[] = round($rowAdicionInversion[0]);
											$registroinfo[] = "Adici&oacute;n Inversion No: ".$rowAdicionInversion[2];
										}
									}
									//REDUCCIONES
									{
										$sqlReduccion = "SELECT valor, id_reduccion FROM ccpetreducciones WHERE cuenta = '$rowcta[0]' AND fecha BETWEEN '$fechai' AND '$fechaf' AND vigencia = '$vigusu' AND estado != 'N'";
										$resReduccion = mysqli_query($linkmulti, $sqlReduccion);
										while ($rowReduccion = mysqli_fetch_row($resReduccion))
										{
											if( substr($rowcta[0],0,4) != '2.3.' )
											{
												$numerocuenta[] = $rowcta[0];
												$nombrecuenta[] = $rowcta[1];
											}
											elseif($rowredglobal[1] == 'regalias')
											{
												$numerocuenta[] = $rowcta[0];
												$nombrecuenta[] = $rowcta[1];
											}
											else
											{
												$numerocuenta[] = '2.99';
												$nombrecuenta[] = 'CONSOLIDADO';
											}
											$numerocuentaaux[] = $rowcta[0];
											$secpresupuestal[] = $rowredglobal[0];
											$vigenciagasto[] = 1;
											$sector[] = 0;
											$programatico[] = 0;
											$bpin[] = 0;
											$apropiaini[] = 0;
											$apropiadef[] = -1 * round($rowReduccion[0]);
											$registroinfo[] = "Reducciones No: ".$rowReduccion[1];
										}
									}
									//REDUCCIONES 2
									{
										$sqlReduccion = "
										SELECT T1.valor, T1.id, T1.vigencia_gasto, T1.indicador_producto, T1.bpin
										FROM ccpetreduccion AS T1 
										INNER JOIN ccpetacuerdos AS T2
										ON T1.acuerdo = T2.id_acuerdo AND T1.vigencia = T2.vigencia
										WHERE T1.rubro = '$rowcta[0]' AND T1.vigencia = '$vigusu' AND T2.fecha BETWEEN '$fechai' AND '$fechaf'";
										$resReduccion = mysqli_query($linkmulti, $sqlReduccion);
										while ($rowReduccion = mysqli_fetch_row($resReduccion))
										{
											if( substr($rowcta[0],0,4) != '2.3.' )
											{
												$numerocuenta[] = $rowcta[0];
												$nombrecuenta[] = $rowcta[1];
											}
											elseif($rowredglobal[1] == 'regalias')
											{
												$numerocuenta[] = $rowcta[0];
												$nombrecuenta[] = $rowcta[1];
											}
											else
											{
												$numerocuenta[] = '2.99';
												$nombrecuenta[] = 'CONSOLIDADO';
											}
											$numerocuentaaux[] = $rowcta[0];
											$secpresupuestal[] = $rowredglobal[0];
											if($rowReduccion[2]!=''){$vigenciagasto[] = $rowReduccion[2];}
											else {$vigenciagasto[] = 1;}
											$sector[] = 0;
											$codm1=substr($rowReduccion[3],0,4);
											//$sqlproducto = "SELECT concuipom1 FROM ccpetproductos WHERE cod_producto LIKE '$codm1%'";
											/*$sqlproducto = "SELECT concuipom1 FROM ccpetproductos WHERE codigo_indicador = '$rowReduccion[3]'";
											$resproducto = mysqli_query($linkbd, $sqlproducto);
											$rowproducto = mysqli_fetch_row($resproducto);
											if($rowproducto[0] == ''){$programatico[] = 0;}
											else {$programatico[] = $rowproducto[0];}*/
											if($codm1 == ''){$programatico[] = 0;}
											else {$programatico[] = $codm1;}
											if($rowReduccion[4] == ''){$bpin[] = 0;}
											else {$bpin[] = $rowReduccion[4];}
											$apropiaini[] = 0;
											$apropiadef[] = -1 * round($rowReduccion[0]);
											$registroinfo[] = "Reducciones2 No: ".$rowReduccion[1];
										}
									}
									//TRASLADOS CREDITO
									{
										$sqlCredito = "SELECT valor, id FROM ccpettraslados WHERE cuenta = '$rowcta[0]' AND fecha BETWEEN '$fechai' AND '$fechaf' AND vigencia = '$vigusu' AND estado != 'N' AND tipo = 'C'";
										$resCredito = mysqli_query($linkmulti, $sqlCredito);
										while ($rowCredito = mysqli_fetch_row($resCredito))
										{
											if( substr($rowcta[0],0,4) != '2.3.' )
											{
												$numerocuenta[] = $rowcta[0];
												$nombrecuenta[] = $rowcta[1];
											}
											elseif($rowredglobal[1] == 'regalias')
											{
												$numerocuenta[] = $rowcta[0];
												$nombrecuenta[] = $rowcta[1];
											}
											else
											{
												$numerocuenta[] = '2.99';
												$nombrecuenta[] = 'CONSOLIDADO';
											}
											$numerocuentaaux[] = $rowcta[0];
											$secpresupuestal[] = $rowredglobal[0];
											$vigenciagasto[] = 1;
											$sector[] = 0;
											$programatico[] = 0;
											$bpin[] = 0;
											$apropiaini[] = 0;
											$apropiadef[] = round($rowCredito[0]);
											$registroinfo[] = "Traslados Credito No: ".$rowCredito[1];
										}
									}
									//TRASLADOS CONTRACREDITO
									{
										$sqlContracreditoCredito = "SELECT valor, id FROM ccpettraslados WHERE cuenta = '$rowcta[0]' AND fecha BETWEEN '$fechai' AND '$fechaf' AND vigencia = '$vigusu' AND estado != 'N' AND tipo = 'R'";
										$resContracreditoCredito = mysqli_query($linkmulti, $sqlContracreditoCredito);
										while ($rowContracreditoCredito = mysqli_fetch_row($resContracreditoCredito))
										{
											if( substr($rowcta[0],0,4) != '2.3.' )
											{
												$numerocuenta[] = $rowcta[0];
												$nombrecuenta[] = $rowcta[1];
											}
											elseif($rowredglobal[1] == 'regalias')
											{
												$numerocuenta[] = $rowcta[0];
												$nombrecuenta[] = $rowcta[1];
											}
											else
											{
												$numerocuenta[] = '2.99';
												$nombrecuenta[] = 'CONSOLIDADO';
											}
											$numerocuentaaux[] = $rowcta[0];
											$secpresupuestal[] = $rowredglobal[0];
											$vigenciagasto[] = 1;
											$sector[] = 0;
											$programatico[] = 0;
											$bpin[] = 0;
											$apropiaini[] = 0;
											$apropiadef[] = -1 * round($rowContracreditoCredito[0]);
											$registroinfo[] = "Traslados Contracredito No: ".$rowContracreditoCredito[1];
										}
									}
									$porcentaje = $c * 100 / $totalcli;
									echo"
									<script>
										progres='".round($porcentaje)."';callprogress(progres);
										document.getElementById('totalcuentas').innerHTML='Recolectando Información: $numid / $totalcli';
									</script>";
									flush();
									ob_flush();
									usleep(5);
								}
							}
							$iter = "zebra1";
							$iter2 = "zebra2";
							$c=0;
							$totalcli=count($numerocuenta);
							for($xx=0;$xx<$totalcli;$xx++)
							{
								$c++;
								$idtemporal++;
								$sqltabla2 = "INSERT INTO ".$_SESSION['tablatemporal']." (id, codigocuenta, nombrecuenta, seccpresupuestal, vigenciagastos, sector, programatico, bpin, apropiacionini, apropiaciondef, registro, codigocuentaaux) VALUES ('$idtemporal', '$numerocuenta[$xx]', '$nombrecuenta[$xx]', '$secpresupuestal[$xx]', '$vigenciagasto[$xx]', '$sector[$xx]', '$programatico[$xx]', '$bpin[$xx]', '$apropiaini[$xx]', '$apropiadef[$xx]','$registroinfo[$xx]','$numerocuentaaux[$xx]')";
								mysqli_query($linkbd,$sqltabla2);
								$porcentaje = $c * 100 / $totalcli;
								echo"
								<script>
									progres='".round($porcentaje)."';callprogress(progres);
									document.getElementById('totalcuentas').innerHTML='Procesando Información $c / $totalcli';
								</script>";
								flush();
								ob_flush();
								usleep(1);
							}
							$c=0;
							$sqlver="SELECT id, codigocuenta, nombrecuenta, seccpresupuestal, vigenciagastos, sector, programatico, bpin, SUM(apropiacionini), SUM(apropiaciondef) FROM ".$_SESSION['tablatemporal']." GROUP BY codigocuenta, seccpresupuestal, programatico,bpin,vigenciagastos ORDER BY seccpresupuestal,vigenciagastos,id ASC";
							$resver = mysqli_query($linkbd,$sqlver);
							$totalcli=mysqli_num_rows($resver);
							while ($rowver = mysqli_fetch_row($resver))
							{
								$c++;
								$porcentaje = $c * 100 / $totalcli;
								echo "
								<tr class=$iter>
									<td class='titulos2'>
										<a onClick=\"detallec('$c','$rowver[1]','$rowver[4]','$rowver[3]','$rowver[6]','$rowver[7]')\" style='cursor:pointer;'>
										<img id='img$c' src='imagenes/plus.gif'>
										</a>
									</td>
									<td style='width:10%;'>$rowver[1]</td>
									<td>$rowver[2]</td>
									<td>$rowver[4]</td>
									<td>$rowver[3]</td>
									<td>$rowver[6]</td>
									<td>$rowver[7]</td>
									<td style='text-align:right;'>$".number_format($rowver[8],0,',','.')."</td>
									<td style='text-align:right;'>$".number_format($rowver[9],0,',','.')."</td>
								</tr>
								<tr>
									<td align='center'></td>
										<td colspan='16'>
											<div id='detalle$c' style='display:none'></div>
										</td>
									</tr>
								<script>
									progres='".round($porcentaje)."';callprogress(progres);
									document.getElementById('totalcuentas').innerHTML='Visualizar Información $c / $totalcli';
								</script>";
								flush();
								ob_flush();
								usleep(1);
								$aux=$iter;
								$iter=$iter2;
								$iter2=$aux;
								if(($rowver[8] == 0) && ($rowver[9] == 0)){}
								else
								{
									fputs($Descriptor1,"D;".$rowver[1].";".$rowver[4].";".$rowver[3].";".$rowver[6].";".$rowver[7].";".$rowver[8].";".$rowver[9]."\r\n");
									fputs($Descriptor2,"D\t".$rowver[1]."\t".$rowver[4]."\t".$rowver[3]."\t".$rowver[6]."\t".$rowver[7]."\t".$rowver[8]."\t".$rowver[9]."\r\n");
								}
							}
							echo "
								</table>
							</div>";
							fclose($Descriptor1);
							fclose($Descriptor2);
							echo "
							<script>document.getElementById('progreso').style.display='none';</script>";
							
						}break;
						case 4: //EJECUCION DE GASTOS - RESERVAS
						{
							unset ($numerocuenta, $nombrecuenta, $secpresupuestal, $vigenciagasto, $programatico, $varbpin, $varcpc, $varfuentes, $situaciondondos, $politicapublica, $terceroschip, $compromisos, $obligaciones, $pagos, $tablasinfo, $varfuentes2, $varcpc2, $programatico2);
							$numid = $c = $idtemporal = 0;
							$namearch = "archivos/".$_SESSION['usuario']."informecgr".$fec.".csv";
							$Descriptor1 = fopen($namearch, "w+");
							$namearch2 = "archivos/".$informes[$_POST['reporte']].".txt";
							$Descriptor2 = fopen($namearch2, "w+");
							$namearch3 = "archivos/".$informes[$_POST['reporte']]."B.txt";
							$Descriptor3 = fopen($namearch3, "w+");
							echo "
							<div class='subpantallap' style='height:66.5%; width:99.6%;'>
								<table class='inicio' align='center'>
									<tr>
										<td class='titulos' colspan='15'><p class='neon_titulos' id='titulogento'></p></td>
									</tr>
									<tr>
										<td colspan='3' id='totalcuentas'></td>
										<td colspan='3'>
											<div id='progreso' class='ProgressBar' style='display:none; float:left'>
												<div class='ProgressBarText'><span id='getprogress'></span>&nbsp;% </div>
												<div id='getProgressBarFill'></div>
											</div>
										</td>
										<td colspan='4'>
											<div id='progreso2' class='ProgressBar2' style='display:none; float:left'>
												<div class='ProgressBarText2'><span id='getprogress2'></span>&nbsp;% </div>
												<div id='getProgressBarFill2'></div>
											</div>
										</td>
										<td colspan='5' id='titulosubproceso'></td>
									</tr>
									<tr>
										<td class='titulos2' style='width:2px;'><img src='imagenes/plus.gif'></td>
										<td class='titulos2' colspan='2'>&Aacute;RBOL DE CONCEPTOS</td>
										<td class='titulos2'>VIGENCIA DEL GASTO</td>
										<td class='titulos2'>SECCIÓN PRESUPUESTAL</td>
										<td class='titulos2'>PROGRAMATICO MGA </td>
										<td class='titulos2'>CPC</td>
										<td class='titulos2'>FUENTES DE FINANCIACI&Oacute;N</td>
										<td class='titulos2'>BPIN</td>
										<td class='titulos2'>SITUACION DE FONDOS</td>
										<td class='titulos2'>POLITICA PUBLICA</td>
										<td class='titulos2'>TERCEROS CHIP</td>
										<td class='titulos2'>COMPROMISOS</td>
										<td class='titulos2'>OBLIGACIONES</td>
										<td class='titulos2'>PAGOS</td>
									</tr>";
							$mes2 = substr($_POST['periodo'], 3, 2);
							$_POST['fecha'] = "01/01/$vigusu";
							$_POST['fecha2'] = intval(date("t", $mes2))."/$mes2/$vigusu";
							preg_match("/([0-9]{2})\/([0-9]{2})\/([0-9]{4})/", $_POST['fecha'],$fecha);
							$fechai = "$fecha[3]-$fecha[2]-$fecha[1]";
							preg_match("/([0-9]{2})\/([0-9]{2})\/([0-9]{4})/", $_POST['fecha2'],$fecha);
							$fechaf = "$fecha[3]-$fecha[2]-$fecha[1]";

							fputs($Descriptor1, "S;".$_POST['codent'].";".$_POST['periodo'].";".$vigusu.";D_EJECUCION_DE_GASTOS\r\n");
							fputs($Descriptor1, "S;ARBOL DE CONCEPTOS;VIGENCIA DEL GASTO;SECCION PRESUPUESTAL;PROGRAMATICO MGA;CPC;FUENTES DE FINANCIACIÓN;BPIN;SITUACION DE FONDOS;POLITICA PUBLICA;TERCEROS CHIP;COMPROMISOS; OBLIGACIONES;PAGOS\r\n");
							fputs($Descriptor2, "S\t".$_POST['codent']."\t".$_POST['periodo']."\t".$vigusu. "\tD_EJECUCION_DE_GASTOS\r\n");
							
							if($_SESSION['tablatemporal'] != '' && $_SESSION['tablatemporal'] != null)
							{
								$sqlr = "DROP TABLE ".$_SESSION['tablatemporal'];
								mysqli_query($linkbd,$sqlr);
								$sqlr = "CREATE TABLE ".$_SESSION['tablatemporal']." (id int(11), codigocuenta varchar(100), nombrecuenta varchar(200), seccpresupuestal varchar(10), vigenciagastos varchar(10), programatico varchar(50), bpin varchar(20), cpc varchar(100), fuente varchar(10), situacionfondo varchar(1), politicapublica varchar(10), tercerochip varchar(10), compromisos double, obligaciones double, pagos double, tipotabla varchar(100), fuente2 varchar(10), cpc2 varchar(100), programatico2 varchar(50)) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;";
								mysqli_query($linkbd,$sqlr);
							}
							$sqlredglobal ="SELECT codigo, base, nombre FROM redglobal ORDER BY id ASC";
							$resredglobal = mysqli_query($linkbd,$sqlredglobal);
							while ($rowredglobal = mysqli_fetch_row($resredglobal))
							{
								$c = $numid = 0;
								echo"
								<script>document.getElementById('titulogento').innerHTML='.: C. EJECUCI&Oacute;N DE GASTOS: $rowredglobal[2]';</script>";
								$linkmulti = conectar_Multi($rowredglobal[1]);
								$linkmulti -> set_charset("utf8");
								$maxVersion = ultimaVersionGastosCCPETmulti($rowredglobal[1]);
								$sqlcta ="SELECT codigo,nombre FROM cuentasccpet WHERE version = '$maxVersion' AND municipio=1 AND tipo = 'C' ORDER BY id ASC";
								$rescta = mysqli_query($linkmulti,$sqlcta);
								$totalcli=mysqli_num_rows($rescta);
								while ($rowcta = mysqli_fetch_row($rescta))
								{
									$numid++;
									$c++;
									//COMPROMISOS (RP)
									{
										$cx = 0;
										$sqlrp = "
										SELECT T1.consvigencia, T1.productoservicio, T1.fuente, T1.valor, T1.medio_pago,T1.codigo_politicap, T1.bpim, T1.indicador_producto, T1.tipo_mov, T1.codigo_vigenciag
										FROM ccpetrp_detalle AS T1
										INNER JOIN ccpetrp AS T2
										ON T1.consvigencia = T2.consvigencia AND T1.vigencia = T2.vigencia AND T1.tipo_mov = T2.tipo_mov AND T1.cuenta = '$rowcta[0]'
										WHERE T1.vigencia = '$vigusu' AND NOT(T2.estado = 'N') AND T2.fecha BETWEEN '$fechai' AND '$fechaf' 
										ORDER BY consvigencia";
										$resrp = mysqli_query($linkmulti, $sqlrp);
										$totalcx = mysqli_num_rows($resrp);
										while($rowrp = mysqli_fetch_row($resrp))
										{
											$cx++;
											$numerocuenta[] = $rowcta[0];
											$nombrecuenta[] = $rowcta[1];
											$secpresupuestal[] = $rowredglobal[0];
											if($rowrp[9] == ''){$vigenciagasto[] = 1;}
											else {$vigenciagasto[] = $rowrp[9];}
											if($rowrp[7] != '' && $rowrp[7] != null && $rowrp[7] != 0)
											{
												$programatico2[] = substr($rowrp[7],0,7);
												//$auxprogramatico = buscarprogramatico($rowrp[7],'2');
												$auxprogramatico = substr($rowrp[7],0,7);
												if ($auxprogramatico != ''){$programatico[] = $auxprogramatico;}
												else {$programatico[] = 'NO APLICA';}
											}
											else 
											{
												$programatico[] = 'NO APLICA';
												$programatico2[] = 'NO APLICA';
											}
											if($rowrp[6]!=''){$varbpin[] = $rowrp[6];}
											else {$varbpin[] = 0;}
											if($rowrp[1] != '' && $rowrp[1] != null && $rowrp[1] != 0)
											{
												$varcpc2[] = $rowrp[1];
												//$auxcpc = buscarcpccuipo($rowrp[1]);
												$auxcpc = $rowrp[1];
												if($auxcpc != ''){$varcpc[] = $auxcpc;}
												else {$varcpc[] = 'NO APLICA';}
											}
											else 
											{
												$varcpc[] = 'NO APLICA';
												$varcpc2[] = 'NO APLICA';
											}
											if($rowrp[2] != '' && $rowrp[2] != null && $rowrp[2] != 0)
											{
												$varfuentes2[] = $rowrp[2];
												//$auxfuente = buscarfuentecuipo($rowrp[2]);
												$auxfuente = $rowrp[2];
												if($auxfuente != '' && $auxfuente != null){$varfuentes[] = $auxfuente;}
												else {$varfuentes[] = 'NO APLICA';}
											}
											else 
											{
												$varfuentes[] = 'NO APLICA';
												$varfuentes2[] = 'NO APLICA';
											}
											if($rowrp[4] == 'CSF'){$situaciondondos[] = 'C';}
											else {$situaciondondos[] = 'S';}
											//if($rowrp[5]!=''){$politicapublica[] = $rowrp[5];}
											//else {$politicapublica[] = 'NO APLICA';}
											$politicapublica[] = 'NO APLICA';
											$terceroschip=0;
											if(substr($rowrp[8],0, 1) == '2'){$compromisos[] = round($rowrp[3]);}
											else {$compromisos[] = -1 * round($rowrp[3]);}
											$obligaciones[] = 0;
											$pagos[] = 0;
											$tablasinfo[] = "RP No: $rowrp[0] (Mov: $rowrp[8])";
											$porcentaje2 = $cx * 100 / $totalcx;
											echo"
											<script>
												progres2 = '".round($porcentaje2)."';
												callprogress2(progres2);
												document.getElementById('titulosubproceso').innerHTML = 'RP No: $rowrp[0] ($rowcta[0])';
											</script>";
											flush();
											ob_flush();
											usleep(1);
										}
										echo "
										<script>
											document.getElementById('progreso2').style.display='none';
											document.getElementById('titulosubproceso').innerHTML='';
										</script>";
									}
									//OBLIGACIONES CXP
									{
										$cx = 0;
										$sqlcxp = "
										SELECT T1.id_orden, T1.indicador_producto, T1.bpim, T1.productoservicio , T1.fuente, T1.medio_pago, T1.codigo_politicap, T1.tipo_mov, T1.valor, T2.id_rp, T1.codigo_vigenciag
										FROM tesoordenpago_det AS T1
										INNER JOIN tesoordenpago AS T2
										ON T1.id_orden = T2.id_orden AND T1.tipo_mov = T2.tipo_mov AND T1.cuentap = '$rowcta[0]'
										WHERE T2.vigencia='$vigusu' AND NOT(T2.estado='N') AND T2.fecha BETWEEN '$fechai' AND '$fechaf'
										ORDER BY T1.id_orden";
										$rescxp = mysqli_query($linkmulti, $sqlcxp);
										$totalcx = mysqli_num_rows($rescxp);
										while($rowcxp = mysqli_fetch_row($rescxp))
										{
											$cx++;
											$numerocuenta[] = $rowcta[0];
											$nombrecuenta[] = $rowcta[1];
											$secpresupuestal[] = $rowredglobal[0];
											if($rowcxp[10] == ''){$vigenciagasto[] = 1;}
											else {$vigenciagasto[] = $rowcxp[10];}
											if($rowcxp[1] != '' && $rowcxp[1] != null && $rowcxp[1] != 0)
											{
												$programatico2[] = substr($rowcxp[1],0,7);
												//$auxprogramatico = buscarprogramatico($rowcxp[1],'2');
												$auxprogramatico = substr($rowcxp[1],0,7);
												if ($auxprogramatico != ''){$programatico[]= $auxprogramatico;}
												else {$programatico[] = 'NO APLICA';}
											}
											else 
											{
												$programatico[] = 'NO APLICA';
												$programatico2[] = 'NO APLICA';
											}
											if($rowcxp[2] != ''){$varbpin[] = $rowcxp[2];}
											else {$varbpin[] = 0;}
											if($rowcxp[3] != '' && $rowcxp[3] != null && $rowcxp[3] != 0)
											{
												$varcpc2[] = $rowcxp[3];
												//$auxcpc = buscarcpccuipo($rowcxp[3]);
												$auxcpc = $rowcxp[3];
												if($auxcpc != ''){$varcpc[] = $auxcpc;}
												else {$varcpc[] = 'NO APLICA';}
											}
											else 
											{
												$varcpc[] = 'NO APLICA';
												$varcpc2[] = 'NO APLICA';
											}
											if($rowcxp[4] != '' && $rowcxp[4] != null && $rowcxp[4] != 0)
											{
												$varfuentes2[] = $rowcxp[4];
												//$auxfuente = buscarfuentecuipo($rowcxp[4]);
												$auxfuente = $rowcxp[4];
												if($auxfuente != '' && $auxfuente != null){$varfuentes[] = $auxfuente;}
												else {$varfuentes[] = 'NO APLICA';}
											}
											else 
											{
												$varfuentes[] = 'NO APLICA';
												$varfuentes2[]= 'NO APLICA';
											}
											if($rowcxp[5] == 'CSF'){$situaciondondos[] = 'C';}
											else {$situaciondondos[] = 'S';}
											//if($rowcxp[6]!=''){$politicapublica[] = $rowcxp[6];}
											//else {$politicapublica[] = 'NO APLICA';}
											$politicapublica[] = 'NO APLICA';
											$terceroschip=0;
											$compromisos[]=0;
											if(substr($rowcxp[7],0, 1) == '2'){$obligaciones[] = round($rowcxp[8]);}
											else {$obligaciones[] = -1 * round($rowcxp[8]);}
											$pagos[] = 0;
											$tablasinfo[] = "CXP No: $rowcxp[0] (Mov: $rowcxp[7], RP: $rowcxp[9])";
											$porcentaje2 = $cx * 100 / $totalcx;
											echo"
											<script>
												progres2 = '".round($porcentaje2)."';
												callprogress2(progres2);
												document.getElementById('titulosubproceso').innerHTML = 'CXP No: $rowcxp[0] ($rowcta[0])';
											</script>";
											flush();
											ob_flush();
											usleep(1);
										}
										echo "
										<script>
											document.getElementById('progreso2').style.display='none';
											document.getElementById('titulosubproceso').innerHTML='';
										</script>";
									}
									//OBLIGACIONES NOMINA
									{
										$cx = 0;
										$sqlcxpnom = "
										SELECT T1.nomina, T2.indicador, T2.bpin, T2.producto, T2.fuente, T2.medio_pago, T2.valor, T1.rp
										FROM hum_nom_cdp_rp AS T1
										INNER JOIN humnom_presupuestal AS T2 ON T1.nomina = T2.id_nom
										INNER JOIN humnomina AS T3 ON T1.nomina = T3.id_nom
										WHERE T2.cuenta = '$rowcta[0]' AND T1.vigencia = '$vigusu' AND NOT(T1.estado='N' OR T1.estado='R') AND T2.estado = 'P' AND T3.fecha BETWEEN '$fechai' AND '$fechaf'
										ORDER BY T1.nomina";
										$rescxpnom = mysqli_query($linkmulti, $sqlcxpnom);
										$totalcx = mysqli_num_rows($rescxpnom);
										while($rowcxpnom = mysqli_fetch_row($rescxpnom))
										{
											$cx++;
											$numerocuenta[] = $rowcta[0];
											$nombrecuenta[] = $rowcta[1];
											$secpresupuestal[] = $rowredglobal[0];
											$vigenciagasto[] = 1;
											if($rowcxpnom[1] != '' && $rowcxpnom[1] != null && $rowcxpnom[1] != 0)
											{
												$programatico2[] = substr($rowcxpnom[1],0,7);
												//$auxprogramatico = buscarprogramatico($rowcxpnom[1],'2');
												$auxprogramatico = substr($rowcxpnom[1],0,7);
												if ($auxprogramatico != ''){$programatico[]= $auxprogramatico;}
												else {$programatico[] = 'NO APLICA';}
											}
											else 
											{
												$programatico[] = 'NO APLICA';
												$programatico2[] = 'NO APLICA';
											}
											if($rowcxpnom[2] != ''){$varbpin[] = $rowcxpnom[2];}
											else {$varbpin[] = 0;}
											if($rowcxpnom[3] != '' && $rowcxpnom[3] != null && $rowcxpnom[3] != 0)
											{
												$varcpc2[] = $rowcxpnom[3];
												//$auxcpc = buscarcpccuipo($rowcxpnom[3]);
												$auxcpc = $rowcxpnom[3];
												if($auxcpc != ''){$varcpc[] = $auxcpc;}
												else {$varcpc[] = 'NO APLICA';}
											}
											else 
											{
												$varcpc[] = 'NO APLICA';
												$varcpc2[] = 'NO APLICA';
											}
											if($rowcxpnom[4] != '' && $rowcxpnom[4] != null && $rowcxpnom[4] != 0)
											{
												$varfuentes2[] = $rowcxpnom[4];
												//$auxfuente = buscarfuentecuipo($rowcxpnom[4]);
												$auxfuente = $rowcxpnom[4];
												if($auxfuente != '' && $auxfuente != null){$varfuentes[] = $auxfuente;}
												else {$varfuentes[] = 'NO APLICA';}
											}
											else 
											{
												$varfuentes[] = 'NO APLICA';
												$varfuentes2[] = 'NO APLICA';
											}
											if($rowcxpnom[5] == 'CSF'){$situaciondondos[] = 'C';}
											else {$situaciondondos[] = 'S';}
											$politicapublica[] = 'NO APLICA';
											$terceroschip = 0;
											$compromisos[] = 0;
											$obligaciones[] = round($rowcxpnom[6]);
											$pagos[] = 0;
											$tablasinfo[] = "CXPN No: $rowcxpnom[0] (Mov: 201, RP: $rowcxpnom[7])";
											$porcentaje2 = $cx * 100 / $totalcx;
											echo"
											<script>
												progres2 = '".round($porcentaje2)."';
												callprogress2(progres2);
												document.getElementById('titulosubproceso').innerHTML = 'CXPN No: $rowcxpnom[0] ($rowcta[0])';
											</script>";
											flush();
											ob_flush();
											usleep(1);
										}
										echo "
										<script>
											document.getElementById('progreso2').style.display='none';
											document.getElementById('titulosubproceso').innerHTML='';
										</script>";
									}
									//PAGOS EGRESOS
									{
										$cx = 0;
										$sqlegreso ="
										SELECT T2.id_egreso, T1.id_orden, T1.indicador_producto, T1.bpim, T1.productoservicio, T1.fuente, T1.medio_pago, T1.codigo_politicap, T2.tipo_mov, T1.valor, T3.id_rp, T1.codigo_vigenciag
										FROM tesoordenpago_det AS T1
										INNER JOIN tesoegresos AS T2 ON T1.id_orden = T2.id_orden
										INNER JOIN tesoordenpago AS T3 ON T1.id_orden = T3.id_orden
										WHERE T1.cuentap = '$rowcta[0]' AND T3.vigencia='$vigusu' AND NOT(T1.estado='N') AND T2.fecha BETWEEN '$fechai' AND '$fechaf'
										ORDER BY T1.id_orden";
										$resegreso = mysqli_query($linkmulti,$sqlegreso);
										$totalcx = mysqli_num_rows($resegreso);
										while($rowegreso = mysqli_fetch_row($resegreso))
										{
											$cx++;
											$numerocuenta[] = $rowcta[0];
											$nombrecuenta[] = $rowcta[1];
											$secpresupuestal[] = $rowredglobal[0];
											if($rowegreso[11] == ''){$vigenciagasto[] = 1;}
											else {$vigenciagasto[] = $rowegreso[11];}
											if($rowegreso[2] != '' && $rowegreso[2] != null && $rowegreso[2] != 0)
											{
												$programatico2[] = substr($rowegreso[2],0,7);
												//$auxprogramatico = buscarprogramatico($rowegreso[2],'2');
												$auxprogramatico = substr($rowegreso[2],0,7);
												if ($auxprogramatico != ''){$programatico[]= $auxprogramatico;}
												else {$programatico[] = 'NO APLICA';}
											}
											else 
											{
												$programatico[] = 'NO APLICA';
												$programatico2[] = 'NO APLICA';
											}
											if($rowegreso[3] != ''){$varbpin[] = $rowegreso[3];}
											else {$varbpin[] = 0;}
											if($rowegreso[4] != '' && $rowegreso[4] != null && $rowegreso[4] != 0)
											{
												$varcpc2[] = $rowegreso[4];
												//$auxcpc = buscarcpccuipo($rowegreso[4]);
												$auxcpc = $rowegreso[4];
												if($auxcpc != ''){$varcpc[] = $auxcpc;}
												else {$varcpc[] = 'NO APLICA';}
											}
											else 
											{
												$varcpc[] = 'NO APLICA';
												$varcpc2[] = 'NO APLICA';
											}
											if($rowegreso[5] != '' && $rowegreso[5] != null && $rowegreso[5] != 0)
											{
												$varfuentes2[] = $rowegreso[5];
												//$auxfuente = buscarfuentecuipo($rowegreso[5]);
												$auxfuente = $rowegreso[5];
												if($auxfuente != '' && $auxfuente != null){$varfuentes[] = $auxfuente;}
												else {$varfuentes[] = 'NO APLICA';}
											}
											else 
											{
												$varfuentes[] = 'NO APLICA';
												$varfuentes2[] = 'NO APLICA';
											}
											if($rowegreso[6] == 'CSF'){$situaciondondos[] = 'C';}
											else {$situaciondondos[] = 'S';}
											//if($rowegreso[7]!=''){$politicapublica[] = $rowegreso[7];}
											//else {$politicapublica[] = 'NO APLICA';}
											$politicapublica[] = 'NO APLICA';
											$terceroschip = 0;
											$compromisos[] = 0;
											$obligaciones[] = 0;
											if(substr($rowegreso[8],0, 1) == '2'){$pagos[] = round($rowegreso[9]);}
											else {$pagos[] = -1 * round($rowegreso[9]);}
											$tablasinfo[] = "Egreso No: $rowegreso[0] (Mov: $rowegreso[8], CXP: $rowegreso[1], RP: $rowegreso[10])";
											$porcentaje2 = $cx * 100 / $totalcx;
											echo"
											<script>
												progres2 = '".round($porcentaje2)."';
												callprogress2(progres2);
												document.getElementById('titulosubproceso').innerHTML = 'Egreso No: $rowegreso[0] ($rowcta[0])';
											</script>";
											flush();
											ob_flush();
											usleep(1);
										}
										echo "
										<script>
											document.getElementById('progreso2').style.display='none';
											document.getElementById('titulosubproceso').innerHTML='';
										</script>";
									}
									//PAGOS EGRESOS NOMINA
									{
										$cx = 0;
										$sqlegresonom ="
										SELECT T1.id_egreso, T1.id_orden, T2.indicador_producto, T2.bpin, T2.productoservicio, T2.fuente, T2.medio_pago, T2.codigo_politicap, T2.tipo_mov, T2.valordevengado
										FROM tesoegresosnomina AS T1
										INNER JOIN tesoegresosnomina_det AS T2
										ON T1.id_egreso = T2.id_egreso
										WHERE T2.cuentap = '$rowcta[0]' AND T1.vigencia = '$vigusu' AND NOT(T1.estado='N' OR T1.estado='R') AND NOT(T2.tipo='SE' OR T2.tipo='PE' OR T2.tipo='DS' OR T2.tipo='RE' OR T2.tipo='FS') AND T1.fecha BETWEEN '$fechai' AND '$fechaf'";
										$resegresonom = mysqli_query($linkmulti,$sqlegresonom);
										$totalcx = mysqli_num_rows($resegresonom);
										while($rowegresonom = mysqli_fetch_row($resegresonom))
										{
											$cx++;
											$numerocuenta[] = $rowcta[0];
											$nombrecuenta[] = $rowcta[1];
											$secpresupuestal[] = $rowredglobal[0];
											$vigenciagasto[] = 1;
											if($rowegresonom[2] != '' && $rowegresonom[2] != null && $rowegresonom[2] != 0)
											{
												$programatico2[] = substr($rowegresonom[2],0,7);
												//$auxprogramatico = buscarprogramatico($rowegresonom[2],'2');
												$auxprogramatico = substr($rowegresonom[2],0,7);
												if ($auxprogramatico != ''){$programatico[]= $auxprogramatico;}
												else {$programatico[] = 'NO APLICA';}
											}
											else 
											{
												$programatico[] = 'NO APLICA';
												$programatico2[] = 'NO APLICA';
											}
											if($rowegresonom[3] != ''){$varbpin[] = $rowegresonom[3];}
											else {$varbpin[] = 0;}
											if($rowegresonom[4] != '' && $rowegresonom[4] != null && $rowegresonom[4] != 0)
											{
												$varcpc2[] = $rowegresonom[4];
												//$auxcpc = buscarcpccuipo($rowegresonom[4]);
												$auxcpc = $rowegresonom[4];
												if($auxcpc != ''){$varcpc[] = $auxcpc;}
												else {$varcpc[] = 'NO APLICA';}
											}
											else 
											{
												$varcpc[] = 'NO APLICA';
												$varcpc2[] = 'NO APLICA';
											}
											if($rowegresonom[5] != '' && $rowegresonom[5] != null && $rowegresonom[5] != 0)
											{
												$varfuentes2[] = $rowegresonom[5];
												//$auxfuente = buscarfuentecuipo($rowegresonom[5]);
												$auxfuente = $rowegresonom[5];
												if($auxfuente != '' && $auxfuente != null){$varfuentes[] = $auxfuente;}
												else {$varfuentes[] = 'NO APLICA';}
											}
											else 
											{
												$varfuentes[] = 'NO APLICA';
												$varfuentes2[] = 'NO APLICA';
											}
											if($rowegresonom[6] == 'CSF'){$situaciondondos[] = 'C';}
											else {$situaciondondos[] = 'S';}
											//if($rowegresonom[7]!=''){$politicapublica[] = $rowegresonom[7];}
											//else {$politicapublica[] = 'NO APLICA';}
											$politicapublica[] = 'NO APLICA';
											$terceroschip = 0;
											$compromisos[] = 0;
											$obligaciones[] = 0;
											if(substr($rowegresonom[8],0, 1) == '2'){$pagos[] = round($rowegresonom[9]);}
											else {$pagos[] = -1 * round($rowegresonom[9]);}
											$sqlrpnom = "SELECT rp FROM hum_nom_cdp_rp WHERE nomina = '$rowegresonom[1]'";
											$resrpnom = mysqli_query($linkmulti, $sqlrpnom);
											$rowrpnom = mysqli_fetch_row($resrpnom);
											$tablasinfo[] = "Egreso Nomina No: $rowegresonom[0] (Mov: $rowegresonom[8], CXPN: $rowegresonom[1], RP: $rowrpnom[0])";
											$porcentaje2 = $cx * 100 / $totalcx;
											echo"
											<script>
												progres2 = '".round($porcentaje2)."';
												callprogress2(progres2);
												document.getElementById('titulosubproceso').innerHTML = 'Egreso Nomina No: $rowegresonom[0] ($rowcta[0])';
											</script>";
											flush();
											ob_flush();
											usleep(1);
										}
										echo "
										<script>
											document.getElementById('progreso2').style.display='none';
											document.getElementById('titulosubproceso').innerHTML='';
										</script>";
									}
									$porcentaje = $c * 100 / $totalcli;
									echo"
									<script>
										progres='".round($porcentaje)."';callprogress(progres);
										document.getElementById('totalcuentas').innerHTML='Recolectando Información: $numid / $totalcli';
									</script>";
									flush();
									ob_flush();
									usleep(5);
								}
							}
							echo "<script>document.getElementById('titulogento').innerHTML='.: C. EJECUCI&Oacute;N DE GASTOS';</script>";
							$c=0;
							$totalcli=count($numerocuenta);
							for($xx=0;$xx<$totalcli;$xx++)
							{
								$c++;
								$idtemporal++;
								$sqltabla2 = "INSERT INTO ".$_SESSION['tablatemporal']." (id, codigocuenta, nombrecuenta, seccpresupuestal, vigenciagastos, programatico, bpin, cpc, fuente, situacionfondo, politicapublica, tercerochip, compromisos, obligaciones, pagos, tipotabla, fuente2, cpc2, programatico2) VALUES ('$idtemporal', '$numerocuenta[$xx]', '$nombrecuenta[$xx]', '$secpresupuestal[$xx]', '$vigenciagasto[$xx]', '$programatico[$xx]', '$varbpin[$xx]', '$varcpc[$xx]', '$varfuentes[$xx]', '$situaciondondos[$xx]', '$politicapublica[$xx]', '$terceroschip', '$compromisos[$xx]', '$obligaciones[$xx]','$pagos[$xx]', '$tablasinfo[$xx]', '$varfuentes2[$xx]', '$varcpc2[$xx]', '$programatico2[$xx]')";
								mysqli_query($linkbd,$sqltabla2);
								$porcentaje = $c * 100 / $totalcli;
								echo"
								<script>
									progres='".round($porcentaje)."';callprogress(progres);
									document.getElementById('totalcuentas').innerHTML='Procesando Información $c / $totalcli';
								</script>";
								flush();
								ob_flush();
								usleep(1);
							}
							$iter = "zebra1";
							$iter2 = "zebra2";
							$c=0;
							$sqlver="
							SELECT id, codigocuenta, nombrecuenta, seccpresupuestal, vigenciagastos, programatico, bpin, cpc, fuente, situacionfondo, politicapublica, tercerochip, SUM(compromisos), SUM(obligaciones), SUM(pagos), fuente2, cpc2, programatico2
							FROM ".$_SESSION['tablatemporal']." 
							GROUP BY codigocuenta, seccpresupuestal, programatico2, cpc2, fuente2, bpin, vigenciagastos
							ORDER BY seccpresupuestal, vigenciagastos, id ASC";
							$resver = mysqli_query($linkbd,$sqlver);
							$totalcli = mysqli_num_rows($resver);
							while ($rowver = mysqli_fetch_row($resver))
							{
								$c++;
								$porcentaje = $c * 100 / $totalcli;
								$colorlinea=fcolorlinea($rowver[12],$rowver[13],$rowver[14]);
								echo "
								<tr class=$iter $colorlinea>
									<td class='titulos2'>
										<a onClick=\"detalled('$c','$rowver[1]','$rowver[3]','$rowver[17]','$rowver[16]','$rowver[15]', '$rowver[6]','$rowver[4]')\" style='cursor:pointer;'>
											<img id='img$c' src='imagenes/plus.gif'>
										</a>
									</td>
									<td style='width:10%;'>$rowver[1]</td>
									<td>$rowver[2]</td>
									<td>$rowver[4]</td>
									<td>$rowver[3]</td>
									<td>$rowver[17]</td>
									<td>$rowver[16]</td>
									<td>$rowver[15]</td>
									<td>$rowver[6]</td>
									<td>$rowver[9]</td>
									<td>$rowver[10]</td>
									<td>$rowver[11]</td>
									<td style='text-align:right;'>$".number_format($rowver[12],0,',','.')."</td>
									<td style='text-align:right;'>$".number_format($rowver[13],0,',','.')."</td>
									<td style='text-align:right;'>$".number_format($rowver[14],0,',','.')."</td>
								</tr>
								<tr>
									<td align='center'></td>
									<td colspan='14'>
										<div id='detalle$c' style='display:none'></div>
									</td>
								</tr>
								<script>
									progres='".round($porcentaje)."';callprogress(progres);
									document.getElementById('totalcuentas').innerHTML='Visualizar Información $c / $totalcli';
								</script>";
								flush();
								ob_flush();
								usleep(1);
								$aux=$iter;
								$iter=$iter2;
								$iter2=$aux;
								if(($rowver[12] == 0) && ($rowver[13] == 0) && ($rowver[14] == 0)){}
								else
								{
									if($rowver[5]=='NO APLICA'){$valpromga=0;}
									else {$valpromga=$rowver[5];}
									if($rowver[7]=='NO APLICA'){$valcpcd=0;}
									else {$valcpcd=$rowver[7];}
									if($rowver[8]=='NO APLICA'){$valfuented=0;}
									else {$valfuented=$rowver[8];}
									if($rowver[10]=='NO APLICA'){$valppd=0;}
									else {$valppd=$rowver[10];}
									fputs($Descriptor1, "D;".$rowver[1].";".$rowver[4].";".$rowver[3].";".$valpromga.";".$valcpcd."; ".$valfuented.";".$rowver[6].";".$rowver[9].";".$valppd.";".$rowver[11].";".$rowver[12].";".$rowver[13].";".$rowver[14]."\r\n");
									fputs($Descriptor2, "D\t".$rowver[1]."\t".$rowver[4]."\t".$rowver[3]."\t".$valpromga."\t".$valcpcd."\t".$valfuented."\t".$rowver[6]."\t".$rowver[9]."\t".$valppd."\t".$rowver[11]."\t".$rowver[12]."\t".$rowver[13]."\t".$rowver[14]."\r\n");
									fputs($Descriptor3, "D\t".$rowver[1]."\t".$rowver[4]."\t".$rowver[3]."\t".$valpromga."\t".$valcpcd."\t".$rowver[15]."\t".$rowver[6]."\t".$rowver[9]."\t".$valppd."\t".$rowver[11]."\t".$rowver[12]."\t".$rowver[13]."\t".$rowver[14]."\r\n");
								}
							}
							echo "
								</table>
							</div>";
							fclose($Descriptor1);
							fclose($Descriptor2);
							fclose($Descriptor3);
							echo "
							<script>document.getElementById('progreso').style.display='none';</script>";
						}break;
						case 5: //**EJECUCION DE GASTOS
						{
							
						}break;
					}
				}
			?>
		</form>
	</body>
</html>
