<?php
	require_once("tcpdf/tcpdf_include.php");
	require('comun.inc');
	require "funciones.inc";
	session_start();
	class MYPDF extends TCPDF 
	{
		public function Header() 
		{
			$linkbd=conectar_v7();
			$linkbd -> set_charset("utf8");
			$sqlr="SELECT * FROM configbasica WHERE estado='S'";
			$res=mysqli_query($linkbd,$sqlr);
			while($row=mysqli_fetch_row($res))
			{
				$nit=$row[0];
				$rs=$row[1];
			}
			$this->Image('imagenes/escudo.jpg', 22, 12, 25, 23.9, 'JPG', '', 'T', true, 300, '', false, false, 0, false, false, false);// 
			$this->SetFont('helvetica','B',7);
			$this->SetY(10);
			$this->RoundedRect(10, 10, 199, 31, 2,'' );
			$this->Cell(0.1);
			$this->Cell(50,31,'','R',0,'L'); 
			$this->SetY(10);
			$this->SetX(60);
			$this->SetFont('helvetica','B',12);
			$this->Cell(148.5,15,"$rs",0,0,'C'); 
			$this->SetY(16);
			$this->SetX(60);
			$this->SetFont('helvetica','B',11);
			$this->Cell(148.5,10,"$nit",0,0,'C');
			
			//*****************************************************************************************************************************
			$this->SetFont('helvetica','B',14);
			$this->SetY(10);
			$this->Cell(50.1);
			$this->Cell(149,31,'',0,1,'C'); 
			$this->SetY(8);
			$this->Cell(50.1);
			//************************************
			$this->SetFont('helvetica','B',10);
			$this->SetY(27);
			$this->Cell(50.2);
			$this->Cell(148.5,15,'CERTIFICADO DE EXISTENCIA','T',0,'C'); 
		}
		public function Footer() 
		{
			$linkbd=conectar_v7();
			$linkbd -> set_charset("utf8");
			$sqlr="SELECT direccion,telefono,web,email FROM configbasica WHERE estado='S'";
			$resp=mysqli_query($linkbd,$sqlr);
			while($row=mysqli_fetch_row($resp))
			{
				$direcc= strtoupper($row[0]);
				$telefonos=$row[1];
				$dirweb=strtoupper($row[3]);
				$coemail=strtoupper($row[2]);
			}
			if($direcc!=''){$vardirec="Dirección: $direcc, ";}
			else {$vardirec="";}
			if($telefonos!=''){$vartelef="Telefonos: $telefonos";}
			else{$vartelef="";}
			if($dirweb!=''){$varemail="Email: $dirweb, ";}
			else {$varemail="";}
			if($coemail!=''){$varpagiw="Pagina Web: $coemail";}
			else{$varpagiw="";}
			//$this->SetY(-16);
			$this->SetFont('helvetica', 'I', 8);
			$txt = <<<EOD
$vardirec $vartelef
$varemail $varpagiw
EOD;
			$this->Write(0, $txt, '', 0, 'C', true, 0, false, false, 0);
			//$this->SetY(-13);
			$this->Cell(0, 10, 'Page '.$this->getAliasNumPage().'/'.$this->getAliasNbPages(), 0, false, 'C', 0, '', 0, false, 'T', 'M');
			
		}
	}
	$pdf = new MYPDF('P','mm','Letter', true, 'iso-8859-1', false);// create new PDF document
	$pdf->SetDocInfoUnicode (true); 
	// set document information
	$pdf->SetCreator(PDF_CREATOR);
	$pdf->SetAuthor('G&CSAS');
	$pdf->SetTitle('Certificados');
	$pdf->SetSubject('Certificado de Disponibilidad');
	$pdf->SetKeywords('TCPDF, PDF, example, test, guide');
	$pdf->SetMargins(10, 50, 10);// set margins
	$pdf->SetHeaderMargin(50);// set margins
	$pdf->SetFooterMargin(20);// set margins
	$pdf->SetAutoPageBreak(TRUE, 20);// set auto page breaks
	// set some language-dependent strings (optional)
	if (@file_exists(dirname(__FILE__).'/lang/spa.php')) 
	{
		require_once(dirname(__FILE__).'/lang/spa.php');
		$pdf->setLanguageArray($l);
	}
	// ---------------------------------------------------------
	$pdf->AddPage();
	$pdf->SetFont('helvetica','B',12);
	$pdf->Cell(199,4,"C E R T I F I C A",0,1,'C',false,0,0,false,'T','C');
	$pdf->SetFont('helvetica','',11);
	$linkbd=conectar_v7();
	$linkbd -> set_charset("utf8");
	$sqlr="SELECT * FROM configbasica WHERE estado='S'";
	$res=mysqli_query($linkbd,$sqlr);
	while($row=mysqli_fetch_row($res)){
		$nit = $row[0];
		$rs = $row[1];
	}
	$sqlr="SELECT nombrepropietario, documento, direccion, ha, met2, areacon, tipopredio, avaluo, vigencia FROM tesopredios WHERE cedulacatastral = '".$_GET['codig']."' AND ord = '".$_GET['ord']."' AND tot = '".$_GET['tot']."'";
	$res=mysqli_query($linkbd,$sqlr);
	while($row=mysqli_fetch_row($res)){
		$nombrepropietario = $row[0];
		$numdocumento = $row[1];
		$direccpredio = $row[2];
		$hectarias = $row[3];
		$metroscua = $row[4];
		$areacons = $row[5];
		$tipopredio = $row[6];
		$avaluo = $row[7];
		$vigencia = $row[8];
	}
	$textodes = "Que en el listado catastral del Instituto Geográfico Agustin Codazzi para el $rs, aparece el predio ".$_GET['codig']." a nombre de: ";
	$pdf->ln(5);
	$pdf->MultiCell(199,10,$textodes."\n",0,'J',false,1,'','',true,1,true,true,10);
	$pdf->SetFont('helvetica','B',11);
	
	$pdf->Cell(40,8,$numdocumento,0,0,'L',false,0,0,false,'T','C');
	//$pdf->Cell(159,8,$nombrepropietario,0,1,'L',false,0,0,false,'T','C');
	$pdf->MultiCell(159,8,$nombrepropietario,0,'J',false,1,'','',true,1,true,true,10);
	$pdf->SetFont('helvetica','',10);
	$pdf->Cell(159,7,'Con las siguientes especificaciones:',0,1,'L',false,0,0,false,'T','C');
	$pdf->ln(4);
	$pdf->Cell(50,15,'DIRECCIÓN DEL PREDIO',1,0,'C',false,0,0,false,'T','C');
	$pdf->Cell(30,15,'UBICACIÓN',1,0,'C',false,0,0,false,'T','C');
	$posx = $pdf->getx();
	$pdf->Cell(60,7.5,'ÁREA',1,0,'C',false,0,0,false,'T','C');
	$posy = $pdf->gety();
	$pdf->Cell(40,15,'AVALUO CATASTRAL',1,0,'C',false,0,0,false,'T','C');
	$pdf->MultiCell(19,15,"AÑO AVALUO",1,'C', 0, 1, '', '', true, 0, false, true, 15, 'M');
	$pdf->SetFont('helvetica','B',9);
	$pdf->MultiCell(50, 20, $direccpredio."\n", 1, 'J', 0, 0, '' ,'', true, 0, false, true, 20, 'M');
	$pdf->MultiCell(30, 20, $tipopredio."\n", 1, 'J', 0, 0, '' ,'', true, 0, false, true, 20, 'M');
	$pdf->MultiCell(20, 20, $hectarias, 1, 'C', 0, 0, '' ,'', true, 0, false, true, 20, 'M');
	$pdf->MultiCell(20, 20, $metroscua, 1, 'C', 0, 0, '' ,'', true, 0, false, true, 20, 'M');
	$pdf->MultiCell(20, 20, $areacons, 1, 'C', 0, 0, '' ,'', true, 0, false, true, 20, 'M');
	$pdf->MultiCell(40, 20, $avaluo, 1, 'C', 0, 0, '' ,'', true, 0, false, true, 20, 'M');
	$pdf->MultiCell(19, 20, $vigencia, 1, 'C', 0, 0, '' ,'', true, 0, false, true, 20, 'M');
	$posy2 = $pdf->gety();
	$pdf->SetFont('helvetica','',10);
	$pdf->SetY($posy + 7.5);
	$pdf->Setx($posx);
	$pdf->Cell(20,7.5,'HA',1,0,'C',false,0,0,false,'T','C');
	$pdf->Cell(20,7.5,'M²',1,0,'C',false,0,0,false,'T','C');
	$pdf->Cell(20,7.5,'Cons',1,1,'C',false,0,0,false,'T','C');
	$pdf->SetY($posy2);
	$dia = date('d');
	$mes = date('m');
	$anu = date('Y');
	$fecha = date('Y-m-d');
	if($dia == '01'){
		$textdia = "el primer dia del mes de $mes de $anu.";
	}else{
		$textdia = "a los $dia dias del mes de $mes de $anu.";
	}
	$textodes2 = "Se expide en $rs $textdia" ;
	$pdf->ln(25);
	$pdf->MultiCell(199, 10, $textodes2."\n", 0, 'J', 0, 0, '' ,'', true, 0, false, true, 20);
	$pdf->ln(60);
	$sqlr="SELECT funcionario, nomcargo FROM firmaspdf_det WHERE idfirmas='7' AND estado ='S' AND fecha < '$fecha' ORDER BY orden";
	$res=mysqli_query($linkbd,$sqlr);
	while($row=mysqli_fetch_row($res))
	{
		$_POST['ppto'][]=$row[0];
		$_POST['nomcargo'][]=$row[1];
	}
	for($x=0;$x<count($_POST['ppto']);$x++)
	{
		$pdf->ln(14);
		$v=$pdf->gety();
		if($v>=251){
			$pdf->AddPage();
			$pdf->ln(20);
			$v=$pdf->gety();
		}
		$pdf->setFont('times','B',8);
		if (($x%2)==0) 
		{
			if(isset($_POST['ppto'][$x+1]))
			{
				$pdf->Line(17,$v,107,$v);
				$pdf->Line(112,$v,202,$v);
				$v2=$pdf->gety();
				$pdf->Cell(104,4,''.$_POST['ppto'][$x],0,1,'C',false,0,0,false,'T','C');
				$pdf->Cell(104,4,''.$_POST['nomcargo'][$x],0,1,'C',false,0,0,false,'T','C');
				$pdf->SetY($v2);
				$pdf->Cell(295,4,''.$_POST['ppto'][$x+1],0,1,'C',false,0,0,false,'T','C');
				$pdf->Cell(295,4,''.$_POST['nomcargo'][$x+1],0,1,'C',false,0,0,false,'T','C');
			}
			else
			{
				$pdf->Line(50,$v,160,$v);
				$pdf->Cell(190,4,''.$_POST['ppto'][$x],0,1,'C',false,0,0,false,'T','C');
				$pdf->Cell(190,4,''.$_POST['nomcargo'][$x],0,0,'C',false,0,0,false,'T','C');
			}
			$v3=$pdf->gety();
		}
		$pdf->SetY($v3);
		$pdf->SetFont('helvetica','',7);
	}
	$pdf->Output('solicitudcdp.pdf', 'I');
?>


