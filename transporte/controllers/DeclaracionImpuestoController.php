<?php
    require_once '../../Librerias/core/Helpers.php';
    require_once '../models/DeclaracionImpuestoModel.php';

    session_start();
    class DeclaracionImpuestoController extends DeclaracionImpuestoModel{

        public function getData(){
            if(!empty($_SESSION)){
                $arrResponse = [
                    "vigencias"=>$this->selectVigencias(),
                ];
                echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
            }
            die();
        }
        public function save(){
            if(!empty($_SESSION)){
                $arrImpuestos = json_decode($_POST['impuestos'],true);
                $arrAuto = json_decode($_POST['automotor'],true);
                $arrTotales = json_decode($_POST['totales'],true);
                if(empty($arrImpuestos)){
                    $arrResponse = array("status"=>false,"msg"=>"Error de datos.");
                }else{
                    $strFecha = strClean($_POST['fecha']);
                    $request = $this->insertData($strFecha,$arrAuto,$arrTotales);
                    if($request > 0){
                        $requestDet = $this->insertDet($request,$arrImpuestos);
                        if($requestDet > 0){
                            insertAuditoria("trans_auditoria","trans_funciones_id",41,"Crear",$request,"Impuesto vehicular","trans_funciones");
                            $arrResponse = array("status"=>true,"msg"=>"Datos guardados correctamente.","id"=>$request);
                        }else{
                            $arrResponse = array("status"=>false,"msg"=>"Error al guardar vigencias.");
                        }
                    }else{
                        $arrResponse = array("status"=>false,"msg"=>"No se ha podido guardar, inténte de nuevo.");
                    }
                }
                echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
            }
            die();
        }
        public function getEdit(){
            if(!empty($_SESSION)){
                $intId = intval($_POST['codigo']);
                $request = $this->selectEdit($intId);
                if(!empty($request)){
                    $arrResponse = array("status"=>true,"data"=>$request,"consecutivos"=>getConsecutivos("trans_impuesto_cab","id"));
                }else{
                    $arrResponse = array("status"=>false,"consecutivo"=>searchConsec("trans_impuesto_cab","id")-1);
                }
                echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
            }
            die();
        }
        public function getSearch(){
            if(!empty($_SESSION)){
                $intPaginas = intval($_POST['paginas']);
                $intPaginaNow = intval($_POST['pagina']);
                $strTipo = strtolower(replaceChar(strClean($_POST['tipo'])));
                $strBuscar = strtolower(replaceChar(strClean($_POST['buscar'])));
                $strFechaInicial = strClean($_POST['fecha_inicial']);
                $strFechaFinal = strClean($_POST['fecha_final']);
                $strCodigo = strClean($_POST['codigo']);
                $strPlaca = strtoupper(replaceChar(strClean($_POST['placa'])));
                $strDocumento = strtoupper(replaceChar(strClean($_POST['documento'])));
                if($strTipo=="placas"){
                    $request = $this->selectPlacas($intPaginas,$intPaginaNow,$strBuscar);
                }else if($strTipo=="buscar"){
                    $request = $this->selectSearch($intPaginas,$intPaginaNow,$strFechaInicial,$strFechaFinal,$strCodigo,$strPlaca,$strDocumento);
                }
                $arrData = $request['data'];
                $totalPaginas = $request['total'];
                $startPage = max(1, $intPaginaNow - floor(CANT_BTNS / 2));
                if ($startPage + CANT_BTNS - 1 > $totalPaginas) {
                    $startPage = max(1, $totalPaginas - CANT_BTNS + 1);
                }
                $limitPages = min($startPage + CANT_BTNS, $totalPaginas+1);
                $arrResponse = array("data"=>$arrData,"start_page"=>$startPage,"limit_page"=>$limitPages,"total_pages"=>$totalPaginas);
                echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
            }
            die();
        }
        public function changeData(){
            if(!empty($_SESSION)){
                $id = intval($_POST['id']);
                $request = $this->updateStatus($id);
                if($request == 1){
                    insertAuditoria("trans_auditoria","trans_funciones_id",41,"Anular",$id,"Impuesto vehicular","trans_funciones");
                    $arrResponse = array("status"=>true);
                }else{
                    $arrResponse = array("status"=>false,"msg"=>"Error");
                }
                echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
            }
            die();
        }
        public function getImpuesto(){
            if(!empty($_SESSION)){
                $objFechaActual = new DateTime("now");
                $intVigenciaActual = date_format($objFechaActual,"Y");
                $intMesActual = date_format($objFechaActual,"n");
                $arrData = json_decode($_POST['data'],true);
                $request = $this->selectPlaca($arrData['placa']);
                $strFecha = strClean($_POST['fecha']);
                $objFechaPago = new DateTime(strClean($strFecha));
                if(!empty($request)){
                    if($request['fecha_tramite'] != "" ){
                        $objFechaTramite = new DateTime($request['fecha_tramite']);
                        $intDiasMora = $objFechaTramite->diff($objFechaPago)->days;
                        $intVigenciaTramite = date_format(new DateTime($request['fecha_tramite']),"Y");
                        $arrVigencias = [];
                        for ($i=$intVigenciaTramite; $i <= $intVigenciaActual; $i++) {
                            array_unshift($arrVigencias,$i);
                        }
                        $arrCartera = $this->selectCartera($request['placa']);
                        $arrImpuestos = [];
                        foreach ($arrVigencias as $det) {
                            $intVigencia = $det;
                            $objFechaMora = new DateTime($intVigencia."-01-01");
                            $arrPagas = array_filter($arrCartera,function($e)use($intVigencia){return $intVigencia == $e;});
                            if(empty($arrPagas)){

                                $intAvaluo = $this->selectAvaluo($arrData['clase_id'],$arrData['marca_id'],$arrData['linea_id'],$arrData['modelo'],$arrData['cilindraje_id'],$intVigencia,$request['avaluo']);
                                $intAvaluo = $arrData['antiguo'] == "S" ? $intAvaluo/2 : $intAvaluo;
                                $intAvaluo = $arrData['blindaje'] == "S" ? $intAvaluo*1.10 : $intAvaluo;;
                                $intAvaluo = round($intAvaluo);
                                $intTarifa = $this->selectTarifa($arrData['tipo_id'],$intAvaluo);
                                $intDescuento = 0;
                                $intImpuesto = round($intAvaluo*($intTarifa/100));
                                $intDiasImpuesto = 1;

                                $intImpuestoDesc = 0;
                                $intSancion = 0;
                                //Calculo de mora
                                $intSemaforizacion = 0;
                                $intDiasMora = 0;
                                $intInteresMora = 0;

                                if($intVigenciaTramite == $intVigencia){
                                    $diff = $objFechaTramite->diff(new DateTime($intVigencia."-12-31"));
                                    $intDiasImpuesto = $diff->days;
                                    $intImpuesto = round($intImpuesto/365);
                                    $intImpuesto = $intImpuesto*$intDiasImpuesto;
                                }
                                if($intVigenciaActual != $intVigencia){
                                    if($intVigenciaTramite == $intVigencia){
                                        $objFechaMora = new DateTime(($intVigencia+1)."-01-01");
                                        $intDiasMora = $objFechaMora->diff($objFechaPago)->days;
                                    }else{
                                        $intDiasMora = $objFechaMora->diff($objFechaPago)->days;
                                    }
                                    $intInteresMora = $this->selectInteres($intMesActual,$intVigenciaActual,$intDiasMora,$intImpuesto);
                                    $intSancion = $this->selectSancion($intVigencia,$intImpuesto);
                                }
                                if($intVigenciaActual == $intVigencia){
                                    $intDescuento = $this->selectDescuento($strFecha);
                                    $intImpuestoDesc = round($intImpuesto*$intDescuento);
                                }

                                $intTotal = $intInteresMora+$intSancion+$intImpuesto-$intImpuestoDesc;
                                $data = array(
                                    "vigencia"=>$intVigencia,
                                    "avaluo"=>$intAvaluo,
                                    "tarifa"=>$intTarifa,
                                    "sancion"=>$intSancion,
                                    "descuento"=>$intDescuento,
                                    "impuesto"=>$intImpuesto,
                                    "impuesto_descuento"=>$intImpuestoDesc,
                                    "semaforizacion"=>$intSemaforizacion,
                                    "interes_mora"=>$intInteresMora,
                                    "dias_mora"=>$intDiasMora,
                                    "total"=>$intTotal,
                                    "selected"=>1
                                );
                                array_push($arrImpuestos,$data);
                            }
                        }
                        $arrResponse = array("status"=>true,"data"=>$arrImpuestos);
                    }else{
                        $arrResponse = array("status"=>false,"msg"=>"La fecha de matrícula es incorrecta.");
                    }
                }else{
                    $arrResponse = array("status"=>false,"msg"=>"La placa no existe, inténte de nuevo.");
                }
                echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
            }
            die();
        }
    }

    if($_POST){
        $obj = new DeclaracionImpuestoController();
        if($_POST['action'] == "get"){
            $obj->getData();
        }else if($_POST['action'] == "search"){
            $obj->getSearch();
        }else if($_POST['action']=="save"){
            $obj->save();
        }else if($_POST['action']=="edit"){
            $obj->getEdit();
        }else if($_POST['action']=="anular"){
            $obj->changeData();
        }else if($_POST['action']=="impuesto"){
            $obj->getImpuesto();
        }

    }

?>
