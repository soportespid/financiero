<?php
    require_once '../../Librerias/core/Helpers.php';
    require_once '../../PHPExcel/Classes/PHPExcel.php';
    require_once '../models/InteresesModel.php';

    session_start();
    class InteresesController extends InteresesModel{

        public function getData(){
            if(!empty($_SESSION)){
                $arrResponse = array(
                    "vigencias"=>$this->selectVigencias()
                );
                echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
            }
            die();
        }
        public function save(){
            if(!empty($_SESSION)){
                if(empty($_POST['mes']) || empty($_POST['vigencia'])){
                    $arrResponse = array("status"=>false,"msg"=>"Error de datos");
                }else{
                    $intConsecutivo = intval($_POST['consecutivo']);
                    $intMes = intval($_POST['mes']);
                    $intVigencia = intval($_POST['vigencia']);
                    $floPorcentajeMoratorio = floatval($_POST['porcentaje_moratorio']);
                    $floPorcentajeCorriente = floatval($_POST['porcentaje_corriente']);
                    $opcion="";
                    if($intConsecutivo == 0){
                        $opcion = 1;
                        $request = $this->insertData($intMes,$intVigencia,$floPorcentajeMoratorio,$floPorcentajeCorriente);
                    }else{
                        $opcion = 2;
                        $request = $this->updateData($intConsecutivo,$intMes,$intVigencia,$floPorcentajeMoratorio,$floPorcentajeCorriente);
                    }
                    if($request > 0){
                        if($opcion == 1){
                            insertAuditoria("trans_auditoria","trans_funciones_id",13,"Crear",$request,"Intereses","trans_funciones");
                            $arrResponse = array("status"=>true,"msg"=>"Datos guardados correctamente.","id"=>$request);
                        }else{
                            insertAuditoria("trans_auditoria","trans_funciones_id",13,"Editar",$intConsecutivo,"Intereses","trans_funciones");
                            $arrResponse = array("status"=>true,"msg"=>"Datos actualizados correctamente.");
                        }
                    }else if($request == "existe"){
                        $arrResponse = array("status"=>false,"msg"=>"Ya existe el interés para el mes y vigencia seleccionada.");
                    }else{
                        $arrResponse = array("status"=>false,"msg"=>"No se ha podido guardar, inténte de nuevo.");
                    }
                }
                echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
            }
            die();
        }
        public function getEdit(){
            if(!empty($_SESSION)){
                $intId = intval($_POST['codigo']);
                $request = $this->selectEdit($intId);
                if(!empty($request)){
                    $arrResponse = array("status"=>true,"data"=>$request,"consecutivos"=>getConsecutivos("interes","id"));
                }else{
                    $arrResponse = array("status"=>false,"consecutivo"=>searchConsec("interes","id")-1);
                }
                echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
            }
            die();
        }
        public function getSearch(){
            if(!empty($_SESSION)){
                $intPaginas = intval($_POST['paginas']);
                $intPaginaNow = intval($_POST['pagina']);
                $intVigencia = strtolower(replaceChar(strClean($_POST['vigencia'])));
                $request = $this->selectSearch($intPaginas,$intPaginaNow, $intVigencia);
                $arrData = $request['data'];
                $totalPaginas = $request['total'];
                $startPage = max(1, $intPaginaNow - floor(CANT_BTNS / 2));
                if ($startPage + CANT_BTNS - 1 > $totalPaginas) {
                    $startPage = max(1, $totalPaginas - CANT_BTNS + 1);
                }
                $limitPages = min($startPage + CANT_BTNS, $totalPaginas+1);
                $arrResponse = array("data"=>$arrData,"start_page"=>$startPage,"limit_page"=>$limitPages,"total_pages"=>$totalPaginas);
                echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
            }
            die();
        }
    }

    if($_POST){
        $obj = new InteresesController();
        if($_POST['action'] == "get"){
            $obj->getData();
        }else if($_POST['action'] == "search"){
            $obj->getSearch();
        }else if($_POST['action']=="save"){
            $obj->save();
        }else if($_POST['action']=="edit"){
            $obj->getEdit();
        }
    }

?>
