<?php
    require_once '../../Librerias/core/Helpers.php';
    require_once '../../PHPExcel/Classes/PHPExcel.php';
    require_once '../models/TarifasModel.php';

    session_start();
    class TarifasController extends TarifasModel{

        public function getData(){
            if(!empty($_SESSION)){
                $arrResponse = array(
                    "tipos"=>$this->selectTipos()
                );
                echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
            }
            die();
        }
        public function save(){
            if(!empty($_SESSION)){
                $arrRangos = json_decode($_POST['rangos'],true);
                $arrVehiculos = json_decode($_POST['vehiculos'],true);
                if(empty($_POST['nombre']) || empty($arrVehiculos) || empty($_POST['vigencia'])){
                    $arrResponse = array("status"=>false,"msg"=>"Error de datos");
                }else{
                    $intConsecutivo = intval($_POST['consecutivo']);
                    $intTipo = intval($_POST['tipo']);
                    $intVigencia = intval($_POST['vigencia']);
                    $intPorcentaje = floatval($_POST['porcentaje']);
                    $strNombre = ucfirst(strtolower(replaceChar(strClean($_POST['nombre']))));
                    $opcion="";
                    if($intConsecutivo == 0){
                        $opcion = 1;
                        $request = $this->insertData($intTipo,$intPorcentaje,$strNombre,$arrVehiculos,$intVigencia);
                    }else{
                        $opcion = 2;
                        $request = $this->updateData($intConsecutivo,$intTipo,$intPorcentaje,$strNombre,$arrVehiculos,$intVigencia);
                    }
                    if($request > 0){
                        if($intTipo == 2){
                            $this->insertDet($request,$arrRangos);
                        }
                        if($opcion == 1){
                            insertAuditoria("trans_auditoria","trans_funciones_id",14,"Crear",$request,"Tarifas legales","trans_funciones");
                            $arrResponse = array("status"=>true,"msg"=>"Datos guardados correctamente.","id"=>$request);
                        }else{
                            insertAuditoria("trans_auditoria","trans_funciones_id",14,"Editar",$intConsecutivo,"Tarifas legales","trans_funciones");
                            $arrResponse = array("status"=>true,"msg"=>"Datos actualizados correctamente.");
                        }
                    }else if($request == "existe"){
                        $arrResponse = array("status"=>false,"msg"=>"El nombre o la vigencia ya existe, inténte con otro.");
                    }else if($request =="existe_tipo"){
                        $arrResponse = array("status"=>false,"msg"=>"Algunos de los tipos de vehiculos seleccionados ya se encuentran asignados en otra tarifa.");
                    }else{
                        $arrResponse = array("status"=>false,"msg"=>"No se ha podido guardar, inténte de nuevo.");
                    }
                }
                echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
            }
            die();
        }
        public function getEdit(){
            if(!empty($_SESSION)){
                $intId = intval($_POST['codigo']);
                $request = $this->selectEdit($intId);
                if(!empty($request)){
                    $arrResponse = array("status"=>true,"data"=>$request,"consecutivos"=>getConsecutivos("trans_tarifas_cab","id"));
                }else{
                    $arrResponse = array("status"=>false,"consecutivo"=>searchConsec("trans_tarifas_cab","id")-1);
                }
                echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
            }
            die();
        }
        public function getSearch(){
            if(!empty($_SESSION)){
                $intPaginas = intval($_POST['paginas']);
                $intPaginaNow = intval($_POST['pagina']);
                $request = $this->selectSearch($intPaginas,$intPaginaNow);
                $arrData = $request['data'];
                $totalPaginas = $request['total'];
                $startPage = max(1, $intPaginaNow - floor(CANT_BTNS / 2));
                if ($startPage + CANT_BTNS - 1 > $totalPaginas) {
                    $startPage = max(1, $totalPaginas - CANT_BTNS + 1);
                }
                $limitPages = min($startPage + CANT_BTNS, $totalPaginas+1);
                $arrResponse = array("data"=>$arrData,"start_page"=>$startPage,"limit_page"=>$limitPages,"total_pages"=>$totalPaginas);
                echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
            }
            die();
        }
    }

    if($_POST){
        $obj = new TarifasController();
        if($_POST['action'] == "get"){
            $obj->getData();
        }else if($_POST['action'] == "search"){
            $obj->getSearch();
        }else if($_POST['action']=="save"){
            $obj->save();
        }else if($_POST['action']=="edit"){
            $obj->getEdit();
        }
    }

?>
