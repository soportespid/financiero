<?php
    require_once '../../Librerias/core/Helpers.php';
    require_once '../../PHPExcel/Classes/PHPExcel.php';
    require_once '../models/ReciboImpuestoModel.php';

    session_start();
    class ReciboImpuestoController extends ReciboImpuestoModel{

        public function getInitialDataCreate(){
            if(!empty($_SESSION)){
                $arrResponse = array(
                    "cuentas_bancarias" => $this->getCuentasBancoTransito(),
                    "fecha_min" => getFechaBlock($_SESSION["cedulausu"])
                );
                echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
            }
            die();
        }

        public function getDataLiquidaciones() {
            if(!empty($_SESSION)){
                $txt_search = $_POST["txt_search"];
                $limite_pagina = $_POST["limite_pagina"];
                $pagina_actual = $_POST["pagina_actual"];
                $request = $this->getAllLiquidacionImpuestos($txt_search, $pagina_actual, $limite_pagina);
                $total_paginas = $request['total'];
                $startPage = max(1, $pagina_actual - floor(CANT_BTNS / 2));
                if ($startPage + CANT_BTNS - 1 > $total_paginas) {
                    $startPage = max(1, $total_paginas - CANT_BTNS + 1);
                }
                $limitPages = min($startPage + CANT_BTNS, $total_paginas+1);
                $arrResponse = array(
                    "liquidaciones" => $request["liquidaciones"],
                    "start_page"=>$startPage,"limit_page"=>$limitPages,"total_pages"=>$total_paginas
                );
                echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
            }
            die();
        }

        public function saveCreate() {
            if(!empty($_SESSION)) {
                $data = [
                    "fecha" => (string) $_POST["fecha"],
                    "liquidacion_id" => (int) $_POST["liquidacion_id"],
                    "medio_pago" => (string) $_POST["medio_pago"],
                    "cuenta_bancaria" => (string) $_POST["cuenta_bancaria"],
                    "cuenta_contable" => (string) $_POST["cuenta_contable"],
                    "valor_recaudo" => (float) $_POST["valor"],
                    "detalle" => (string) $_POST["detalle"],
                    "estado" => "S"
                ];
                $request = $this->insertRecaudo($data);
                if ($request > 0) {
                    $arrResponse = array(
                        "status" => true,
                        "msg" => "Recaudo impuesto guardado con código $request",
                        "id" => $request
                    );
                } else {
                    $arrResponse = array(
                        "status" => false,
                        "msg" => "Error en guardado de recaudo impuesto"
                    );
                }
                echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
            }
        }

        public function getInitialDataSearch() {
            if(!empty($_SESSION)){
                $txt_search = $_POST["txt_search"];
                $limite_pagina = $_POST["limite_pagina"];
                $pagina_actual = $_POST["pagina_actual"];
                $request = $this->getAllRecaudos($txt_search, $pagina_actual, $limite_pagina);
                $total_paginas = $request['total'];
                $startPage = max(1, $pagina_actual - floor(CANT_BTNS / 2));
                if ($startPage + CANT_BTNS - 1 > $total_paginas) {
                    $startPage = max(1, $total_paginas - CANT_BTNS + 1);
                }
                $limitPages = min($startPage + CANT_BTNS, $total_paginas+1);
                $arrResponse = array(
                    "recaudos" => $request["recaudos"],
                    "start_page"=>$startPage,"limit_page"=>$limitPages,"total_pages"=>$total_paginas,
                    "fecha_min" => getFechaBlock($_SESSION["cedulausu"])
                );
                echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
            }
            die();
        }

        public function anulaRecaudo() {
            if(!empty($_SESSION)){
                $id = $_POST["id"];
                $request = $this->anularRecaudo($id);
                if ($request > 0) {
                    $arrResponse = array(
                        "status" => true,
                        "msg" => "Recaudo $id anulado con exito",
                    );
                } else {
                    $arrResponse = array(
                        "status" => false,
                        "msg" => "Error en anular"
                    );
                }
                echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
            }
            die();
        }

        public function getInitialDataEdit() {
            if(!empty($_SESSION)){
                $intId = intval($_POST['id']);
                $request = $this->selectEdit($intId);
                if(!empty($request)){
                    $arrResponse = array("status"=>true,"data"=>$request,"consecutivos"=>getConsecutivos("trans_recaudo_impuesto","id"));
                }else{
                    $arrResponse = array("status"=>false,"consecutivo"=>searchConsec("trans_recaudo_impuesto","id")-1);
                }
                echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
            }
            die();
        }
    }

    if($_POST){
        $obj = new ReciboImpuestoController();
        if($_POST['action'] == "getDataCreate"){
            $obj->getInitialDataCreate();
        } else if ($_POST["action"] == "getDataSearch") {
            $obj->getInitialDataSearch();
        } else if ($_POST["action"] == "getDataEdit") {
            $obj->getInitialDataEdit();
        } else if ($_POST["action"] == "getDataLiquidaciones") {
            $obj->getDataLiquidaciones();
        } else if ($_POST["action"] == "save") {
            $obj->saveCreate();
        } else if ($_POST["action"] == "anulaRecaudo") {
            $obj->anulaRecaudo();
        }
    }

?>
