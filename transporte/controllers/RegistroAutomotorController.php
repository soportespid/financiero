<?php
    require_once '../../Librerias/core/Helpers.php';
    require_once '../../PHPExcel/Classes/PHPExcel.php';
    require_once '../models/RegistroAutomotorModel.php';

    session_start();
    class RegistroAutomotorController extends RegistroAutomotorModel{

        public function getInitialDataCreate(){
            if(!empty($_SESSION)){
                $arrResponse = array(
                    "departamentos" => $this->getAllDepartamentos(),
                    "municipios" => $this->getAllMunicipios(),
                    "tipos" => $this->getAllTipos(),
                    "combustibles" => $this->getAllCombustibles(),
                    "cilindrajes" => $this->getAllCilindrajes(),
                    "potencias" => $this->getAllPotencias(),
                    "tipo_publicos" => $this->getAllTipoPublicos(),
                    "carrocerias" => $this->getAllCarrocerias(),
                    "servicios" => $this->getAllServicios()
                );
                echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
            }
            die();
        }

        public function getDataClases() {
            if(!empty($_SESSION)){
                $tipo_id = $_POST["tipo_id"];
                $arrResponse = array(
                    "clases" => $this->getAllClases($tipo_id),
                );
                echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
            }
            die();
        }

        public function getDataMarcas() {
            if(!empty($_SESSION)){
                $clase_id = $_POST["clase_id"];
                $arrResponse = array(
                    "marcas" => $this->getAllMarcas($clase_id),
                );
                echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
            }
            die();
        }

        public function getDataLineas() {
            if(!empty($_SESSION)){
                $marcaId = $_POST["marcaId"];
                $arrResponse = array(
                    "lineas" => $this->getAllLineas($marcaId),
                );
                echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
            }
            die();
        }

        public function getDataTerceros() {
            if(!empty($_SESSION)){
                $txt_search = $_POST["txt_search"];
                $arrResponse = array(
                    "terceros" => getAllTerceros($txt_search),
                );
                echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
            }
            die();
        }

        public function checkPlaca() {
            if(!empty($_SESSION)){
                $placa = strtoupper($_POST["placa"]);
                $arrResponse = array(
                    "status" => $this->existePlaca($placa),
                );
                echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
            }
            die();
        }

        public function saveCreate() {
            if(!empty($_SESSION)){
                $data = [
                    "fecha_tramite" => $_POST["fecha_tramite"],
                    "placa" => strtoupper($_POST["placa"]),
                    "departamento_cod" => $_POST["departamento_cod"],
                    "municipio_cod" => $_POST["municipio_cod"],
                    "propiertario" => $_POST["propietario_document"],
                    "tipo_id" => $_POST["tipo_id"],
                    "clase_id" => $_POST["clase_id"],
                    "marca_id" => $_POST["marca_id"],
                    "linea_id" => $_POST["linea_id"],
                    "combustible_id" => $_POST["combustible_id"],
                    "color" => ucfirst(strtolower($_POST["color"])),
                    "modelo" => $_POST["modelo"],
                    "cilindraje_id" => $_POST["cilindraje_id"],
                    "capacidad_carga" => round($_POST["capacidad_carga"], 2),
                    "ocupantes" => (int) $_POST["ocupantes"],
                    "potencia_hp" => round($_POST["potencia_hp"], 2),
                    "tipo_publico_id" => $_POST["tipo_publico_id"],
                    "carroceria_id" => $_POST["carroceria_id"],
                    "vehiculo_antiguo" => $_POST["vehiculo_antiguo"],
                    "blindaje" => $_POST["blindaje"],
                    "fecha_blindaje" => $_POST["fecha_blindaje"],
                    "desmonte_blindaje" => $_POST["desmonte_blindaje"],
                    "fecha_desmonte" => $_POST["fecha_desmonte"],
                    "numero_motor" => $_POST["numero_motor"],
                    "numero_chasis" => $_POST["numero_chasis"],
                    "numero_serie" => $_POST["numero_serie"],
                    "numero_vin" => $_POST["numero_vin"],
                    "servicio_id" => $_POST["servicio_id"],
                    "empresa_vinculadora" => $_POST["empresa_vinculadora"],
                    "avaluo"=>doubleval($_POST['avaluo'])
                ];

                $resp = $this->insertRegistroAutomotor($data);

                if ($resp > 0) {
                    $arrResponse = array(
                        "status" => true,
                        "msg" => "Registro automotor guardado con exito!",
                        "id" => $resp
                    );
                } else {
                    $arrResponse = array(
                        "status" => false,
                        "msg" => "Error en guardado de registro automotor"
                    );
                }


                echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
            }
            die();
        }

        public function getInitialDataSearch() {
            if(!empty($_SESSION)){
                $clase = $_POST["clase"];
                $marca = $_POST["marca"];
                $linea = $_POST["linea"];
                $modelo = $_POST["modelo"];
                $txt_search = $_POST["txt_search"];
                $limite_pagina = $_POST["limite_pagina"];
                $pagina_actual = $_POST["pagina_actual"];
                $request = $this->getAllAutomotores($clase, $marca, $linea, $modelo, $txt_search, $limite_pagina, $pagina_actual);
                $total_paginas = $request['total'];
                $startPage = max(1, $pagina_actual - floor(CANT_BTNS / 2));
                if ($startPage + CANT_BTNS - 1 > $total_paginas) {
                    $startPage = max(1, $total_paginas - CANT_BTNS + 1);
                }
                $limitPages = min($startPage + CANT_BTNS, $total_paginas+1);
                $arrResponse = array(
                    "clases" => $this->getClasesAutomotores(),
                    "marcas" => $this->getMarcasAutomotores(),
                    "lineas" => $this->getLineasAutomotores(),
                    "modelos" => $this->getModelosAutomotores(),
                    "automotores" => $request["data"],
                    "start_page"=>$startPage,"limit_page"=>$limitPages,"total_pages"=>$total_paginas
                );
                echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
            }
            die();
        }

        public function getInitialDataEdit() {
            if(!empty($_SESSION)){
                $id = $_POST["id"];
                $data = $this->getAutomotorEdit($id);
                $arrResponse = array(
                    "data" => $data,
                    "historial" => $this->getHistorialAutomotor($data["placa"])
                );
                echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
            }
            die();
        }

        public function saveEdit() {
            if(!empty($_SESSION)){
                $data = [
                    "id" => $_POST["id"],
                    "fecha_tramite" => $_POST["fecha_tramite"],
                    "placa" => strtoupper($_POST["placa"]),
                    "dpto" => $_POST["departamento_cod"],
                    "mnpio" => $_POST["municipio_cod"],
                    "propietario" => $_POST["propietario_document"],
                    "tipo_id" => $_POST["tipo_id"],
                    "clase_id" => $_POST["clase_id"],
                    "marca_id" => $_POST["marca_id"],
                    "linea_id" => $_POST["linea_id"],
                    "combustible_id" => $_POST["combustible_id"],
                    "color" => ucfirst(strtolower($_POST["color"])),
                    "modelo" => $_POST["modelo"],
                    "cilindraje_id" => $_POST["cilindraje_id"],
                    "capacidad_carga" => round($_POST["capacidad_carga"], 2),
                    "cantidad_pasajeros" => (int) $_POST["ocupantes"],
                    "potencia_hp" => round($_POST["potencia_hp"], 2),
                    "publico_id" => $_POST["tipo_publico_id"],
                    "carroceria_id" => $_POST["carroceria_id"],
                    "vehiculo_antiguo" => $_POST["vehiculo_antiguo"],
                    "blindaje" => $_POST["blindaje"],
                    "fecha_blindaje" => $_POST["fecha_blindaje"],
                    "desmonte_blindaje" => $_POST["desmonte_blindaje"],
                    "fecha_desmonte" => $_POST["fecha_desmonte"],
                    "numero_motor" => $_POST["numero_motor"],
                    "numero_chasis" => $_POST["numero_chasis"],
                    "numero_serie" => $_POST["numero_serie"],
                    "numero_vin" => $_POST["numero_vin"],
                    "tipo_servicio_id" => $_POST["servicio_id"],
                    "empresa_vinculadora" => $_POST["empresa_vinculadora"],
                    "avaluo"=>doubleval($_POST['avaluo'])
                ];

                $id = $this->InsertEditRegistroAutomotor($data);

                if ($id > 0) {
                    $arrResponse = array(
                        "status" => true,
                        "msg" => "Registro automotor actualizado con exito!",
                    );
                } else {
                    $arrResponse = array(
                        "status" => false,
                        "msg" => "Error en guardado de registro automotor"
                    );
                }
                echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
            }
            die();
        }
    }

    if($_POST){
        $obj = new RegistroAutomotorController();
        if($_POST['action'] == "getDataCreate"){
            $obj->getInitialDataCreate();
        } else if ($_POST["action"] == "getTerceros") {
            $obj->getDataTerceros();
        } else if ($_POST["action"] == "getClases") {
            $obj->getDataClases();
        } else if ($_POST["action"] == "getLineas") {
            $obj->getDataLineas();
        } else if ($_POST["action"] == "getMarcas") {
            $obj->getDataMarcas();
        } else if ($_POST["action"] == "checkPlaca") {
            $obj->checkPlaca();
        } else if ($_POST["action"] == "save") {
            $obj->saveCreate();
        } else if ($_POST["action"] == "getDataSearch") {
            $obj->getInitialDataSearch();
        } else if ($_POST["action"] == "getDataEdit") {
            $obj->getInitialDataEdit();
        } else if ($_POST["action"] == "saveEdit") {
            $obj->saveEdit();
        }
    }

?>
