<?php
    require_once '../../Librerias/core/Helpers.php';
    require_once '../../PHPExcel/Classes/PHPExcel.php';
    require_once '../models/BaseGravableModel.php';

    session_start();
    class BaseGravableController extends BaseGravableModel{
        public function upload(){
            if(!empty($_SESSION)){
                //$this->resetTables();exit;

                $intVigencia = intval($_POST['vigencia']);
                $tmpfname = $_FILES['file']['tmp_name'];
                $excelReader = PHPExcel_IOFactory::createReaderForFile($tmpfname);
                $excelObj = $excelReader->load($tmpfname);
                $sheet = $excelObj->getSheet(0);
                $lastRow = $sheet->getHighestRow();
                $lastColumnString = $sheet->getHighestColumn();
                $startColumnInt = PHPExcel_Cell::columnIndexFromString("I");
                $lastColumnInt = PHPExcel_Cell::columnIndexFromString($lastColumnString);
                $arrData = [];
                $strTipo = ucfirst(strtolower(replaceChar(strClean($sheet->getCell("B4")->getValue()))));
                $tipo = $this->select("SELECT id FROM trans_caracterizacion_tipo WHERE nombre = '$strTipo'");
                if(empty($tipo)){
                    $tipo = $this->insert("INSERT INTO trans_caracterizacion_tipo (nombre) VALUES(?)",[$strTipo]);
                }else{
                    $tipo = $tipo['id'];
                }
                insertAuditoria("trans_auditoria","trans_funciones_id",100,"Crear",$intVigencia,"Base gravable","trans_funciones");
                $this->delete("DELETE FROM trans_base_gravable WHERE tipo = $tipo AND vigencia = $intVigencia");
                //Formatos
                for ($i=4; $i <= $lastRow ; $i++) {
                    $id = strClean($sheet->getCell("A$i")->getValue());
                    $tipo = ucfirst(strtolower(replaceChar(strClean($sheet->getCell("B$i")->getValue()))));
                    $clase = ucfirst(strtolower(replaceChar(strClean($sheet->getCell("C$i")->getValue()))));
                    $marca = ucfirst(strtolower(replaceChar(strClean($sheet->getCell("D$i")->getValue()))));
                    $linea = ucfirst(strtolower(replaceChar(strClean($sheet->getCell("E$i")->getValue()))));
                    $cilindraje = ucfirst(strtolower(replaceChar(strClean($sheet->getCell("F$i")->getValue()))));
                    $capacidad = ucfirst(strtolower(replaceChar(strClean($sheet->getCell("G$i")->getValue()))));
                    $pasajeros = ucfirst(strtolower(replaceChar(strClean($sheet->getCell("H$i")->getValue()))));


                    $arrTablas = $this->checkCaracterizacion($tipo,$clase,$cilindraje,$pasajeros,$capacidad,$marca,$linea);
                    $colModelo = "I";
                    for ($j=$startColumnInt; $j <= $lastColumnInt; $j++) {
                        $modelo = $sheet->getCell($colModelo."3")->getValue();
                        $valor = doubleval(str_replace([",","$","."],"",$sheet->getCell($colModelo.$i)->getValue()));
                        if($modelo!="" && $valor!=""){
                            $data = array(
                                "id"=>$id,
                                "tipo"=>$arrTablas[0]['id'],
                                "clase"=>$arrTablas[1]['id'],
                                "marca"=>$arrTablas[5]['id'],
                                "linea"=>$arrTablas[6]['id'],
                                "cilindraje"=>$arrTablas[2]['id'],
                                "capacidad"=>$arrTablas[4]['id'],
                                "pasajeros"=>$arrTablas[3]['id'],
                                "modelo"=>$modelo,
                                "valor"=>$valor,
                                "vigencia"=>$intVigencia
                            );
                            array_push($arrData,$data);
                        }
                        $colModelo++;
                    }

                }
                $request = $this->insertData($arrData);
                echo json_encode($request,JSON_UNESCAPED_UNICODE);
            }
            die();
        }

        public function getData(){
            if(!empty($_SESSION)){
                $arrResponse = [
                    "tipos"=>$this->selectTipos(),
                    "clases"=>$this->selectClases(),
                    "marcas"=>$this->selectMarcas(),
                    "lineas"=>$this->selectLineas(),
                    "modelos"=>$this->selectModelos(),
                    "vigencias"=>$this->selectVigencias(),
                ];
                echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
            }
            die();
        }

        public function getSearch(){
            if(!empty($_SESSION)){
                $intPaginas = intval($_POST['paginas']);
                $intPaginaNow = intval($_POST['pagina']);
                $strTipo = strtolower(replaceChar(strClean($_POST['tipo'])));
                $strClase = strtolower(replaceChar(strClean($_POST['clase'])));
                $strMarca = strtolower(replaceChar(strClean($_POST['marca'])));
                $strLinea = strtolower(replaceChar(strClean($_POST['linea'])));
                $strModelo = strtolower(replaceChar(strClean($_POST['modelo'])));
                $strVigencia = strtolower(replaceChar(strClean($_POST['vigencia'])));
                $strBuscar = ucfirst(strtolower(replaceChar(strClean($_POST['buscar']))));
                $request = $this->selectSearch($intPaginas,$strVigencia,$intPaginaNow, $strTipo, $strClase, $strMarca, $strLinea, $strModelo,$strBuscar);
                $arrData = $request['data'];
                $totalPaginas = $request['total'];
                $startPage = max(1, $intPaginaNow - floor(CANT_BTNS / 2));
                if ($startPage + CANT_BTNS - 1 > $totalPaginas) {
                    $startPage = max(1, $totalPaginas - CANT_BTNS + 1);
                }
                $limitPages = min($startPage + CANT_BTNS, $totalPaginas+1);
                $arrResponse = array("data"=>$arrData,"start_page"=>$startPage,"limit_page"=>$limitPages,"total_pages"=>$totalPaginas);
                echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
            }
            die();
        }

        public function getPlantilla(){
            if(!empty($_SESSION)){
                $objPHPExcel = new PHPExcel();
                $objPHPExcel->getActiveSheet()->getStyle('A:AG')->applyFromArray(
                    array(
                        'alignment' => array(
                            'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
                            'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER
                        )
                    )
                );
                $objPHPExcel->getProperties()
                ->setCreator("IDEAL 10")
                ->setLastModifiedBy("IDEAL 10")
                ->setTitle("Exportar Excel con PHP")
                ->setSubject("Documento de prueba")
                ->setDescription("Documento generado con PHPExcel")
                ->setKeywords("usuarios phpexcel")
                ->setCategory("reportes");

                $borders = array(
                    'borders' => array(
                        'allborders' => array(
                            'style' => PHPExcel_Style_Border::BORDER_THIN,
                        'color' => array('argb' => 'FF000000'),
                        )
                    ),
                );

                $objPHPExcel->setActiveSheetIndex(0)
                    ->mergeCells('A1:AG1')
                    ->mergeCells('A2:H2')
                    ->mergeCells('I2:AG2')
                    ->setCellValue('A1', 'BASE GRAVABLE DE VEHICULOS')
                    ->setCellValue('I2', "AÑO MODELO")
                    ->setCellValue('A3', 'ID')
                    ->setCellValue('B3', 'TIPO')
                    ->setCellValue('C3', 'CLASE')
                    ->setCellValue('D3', 'MARCA')
                    ->setCellValue('E3', 'LINEA')
                    ->setCellValue('F3', "CILINDRAJE")
                    ->setCellValue('G3', "TONELAJE")
                    ->setCellValue('H3', "PASAJEROS")
                    ->setCellValue('I3', "")
                    ->setCellValue('J3', "")
                    ->setCellValue('K3', "")
                    ->setCellValue('L3', "")
                    ->setCellValue('M3', "")
                    ->setCellValue('N3', "")
                    ->setCellValue('O3', "")
                    ->setCellValue('P3', "")
                    ->setCellValue('Q3', "")
                    ->setCellValue('R3', "")
                    ->setCellValue('S3', "")
                    ->setCellValue('T3', "")
                    ->setCellValue('U3', "")
                    ->setCellValue('V3', "")
                    ->setCellValue('X3', "")
                    ->setCellValue('Y3', "")
                    ->setCellValue('Z3', "")
                    ->setCellValue('AA3', "")
                    ->setCellValue('AB3', "")
                    ->setCellValue('AC3', "")
                    ->setCellValue('AD3', "")
                    ->setCellValue('AE3', "")
                    ->setCellValue('AF3', "")
                    ->setCellValue('AG3', "");

                $objPHPExcel->setActiveSheetIndex(0)
                -> getStyle ('A1:AG1')
                -> getAlignment ()
                -> applyFromArray (array ( 'horizontal'  =>  PHPExcel_Style_Alignment :: HORIZONTAL_CENTER ,) );

                $objPHPExcel->setActiveSheetIndex(0)
                -> getStyle ('I2:AG2')
                -> getAlignment ()
                -> applyFromArray (array ( 'horizontal'  =>  PHPExcel_Style_Alignment :: HORIZONTAL_CENTER ,) );

                $objPHPExcel->setActiveSheetIndex(0)
                -> getStyle ('A3:AG3')
                -> getAlignment ()
                -> applyFromArray (array ( 'horizontal'  =>  PHPExcel_Style_Alignment :: HORIZONTAL_CENTER ,) );

                $objPHPExcel-> getActiveSheet ()
                -> getStyle ("A1:AG1")
                -> getFill ()
                -> setFillType (PHPExcel_Style_Fill :: FILL_SOLID)
                -> getStartColor ()
                -> setRGB ('3399cc');


                $objPHPExcel-> getActiveSheet ()
                -> getStyle ("A3:AG3")
                -> getFill ()
                -> setFillType (PHPExcel_Style_Fill :: FILL_SOLID)
                -> getStartColor ()
                -> setRGB ('99ddff');

                $objPHPExcel->getActiveSheet()->getStyle('A1:AG1')->applyFromArray($borders);
                $objPHPExcel->getActiveSheet()->getStyle('A2:AG2')->applyFromArray($borders);
                $objPHPExcel->getActiveSheet()->getStyle('A3:AG3')->applyFromArray($borders);

                $objPHPExcel->getActiveSheet()->getStyle("A1:AG1")->getFont()->setBold(true);
                $objPHPExcel->getActiveSheet()->getStyle("A1")->getFont()->getColor()->setRGB("ffffff");

                $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
                $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
                $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
                $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
                $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
                $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
                $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);

                //----Guardar documento----
                header('Content-Type: application/vnd.ms-excel');
                header('Content-Disposition: attachment;filename="plantilla_base_gravable.xlsx"');
                header('Cache-Control: max-age=0');
                $objWriter=PHPExcel_IOFactory::createWriter($objPHPExcel,'Excel2007');
                $objWriter->save('php://output');
            }
            die();
        }
    }

    if($_POST){
        $obj = new BaseGravableController();
        if($_POST['action'] == "upload"){
            $obj->upload();
        }else if($_POST['action'] == "get"){
            $obj->getData();
        }else if($_POST['action'] == "search"){
            $obj->getSearch();
        }else if($_POST['action']=="plantilla"){
            $obj->getPlantilla();
        }
    }

?>
