<?php
    require_once '../../Librerias/core/Helpers.php';
    require_once '../../PHPExcel/Classes/PHPExcel.php';
    require_once '../models/BaseCarteraModel.php';

    session_start();
    class BaseCarteraController extends BaseCarteraModel{
        public function upload(){
            if(!empty($_SESSION)){
                $tmpfname = $_FILES['file']['tmp_name'];
                $excelReader = PHPExcel_IOFactory::createReaderForFile($tmpfname);
                $excelObj = $excelReader->load($tmpfname);
                $sheet = $excelObj->getSheet(0);
                $intVigencia = intval($_POST['vigencia']);
                $lastRow = $sheet->getHighestRow();
                insertAuditoria("trans_auditoria","trans_funciones_id",101,"Crear",$intVigencia,"Base cartera","trans_funciones");
                $this->delete("DELETE FROM trans_cartera WHERE vigencia = $intVigencia");
                //Formatos
                for ($i=2; $i < $lastRow ; $i++) {
                    $strVigencia = $intVigencia;
                    $strPlaca = strtoupper(replaceChar(strClean($sheet->getCell("B$i")->getValue())));
                    $intAvaluo = intval(str_replace([",","$"],"",$sheet->getCell("C$i")->getValue()));
                    $intValorDescuento = intval(str_replace([",","$"],"",$sheet->getCell("D$i")->getValue()));
                    $intValorInteres = intval(str_replace([",","$"],"",$sheet->getCell("E$i")->getValue()));
                    $intValorPago = intval(str_replace([",","$"],"",$sheet->getCell("F$i")->getValue()));
                    $intTarifa = floatval($sheet->getCell("G$i")->getValue());
                    $strEstado = strtolower(replaceChar(strClean($sheet->getCell("H$i")->getValue())));
                    $strFechaTransaccion = replaceChar(strClean($sheet->getCell("I$i")->getValue()));
                    $intValorSinDescuento = intval(str_replace([",","$"],"",$sheet->getCell("J$i")->getValue()));
                    $intValorSancion = intval(str_replace([",","$"],"",$sheet->getCell("K$i")->getValue()));
                    $intValorSaldoPagar = intval(str_replace([",","$"],"",$sheet->getCell("L$i")->getValue()));
                    $intSemaforizacion = intval($sheet->getCell("M$i")->getValue());
                    $intValorDepartamento =intval($sheet->getCell("N$i")->getValue());
                    $intValorMunicipio = intval($sheet->getCell("O$i")->getValue());
                    $strPeriodo = ucfirst(strtolower(replaceChar(strClean($sheet->getCell("P$i")->getValue()))));
                    $intValorDescMatricula = 0;
                    $intValorInteresTotal = intval(str_replace([",","$"],"",$sheet->getCell("Q$i")->getValue()));
                    $intValorSancionTotal = intval(str_replace([",","$"],"",$sheet->getCell("R$i")->getValue()));
                    $intValorAnterior = 0;
                    $intValorSaldoFavor = 0;
                    $strDocumento = strtoupper(replaceChar(strClean($sheet->getCell("S$i")->getValue())));
                    $strNombre = ucfirst(strtolower(replaceChar(strClean($sheet->getCell("T$i")->getValue()))));
                    $intPorcentajeDescuento = floatval($sheet->getCell("U$i")->getValue());
                    $intPorcentajeInteres = floatval($sheet->getCell("V$i")->getValue());
                    $intModelo = intval($sheet->getCell("W$i")->getValue());
                    $strAntiguo = strtoupper(replaceChar(strClean($sheet->getCell("X$i")->getValue())));
                    $strBlindado = strtoupper(replaceChar(strClean($sheet->getCell("Y$i")->getValue())));
                    $strCilindraje = ucfirst(strtolower(replaceChar(strClean($sheet->getCell("Z$i")->getValue()))));
                    $intDiasInteres = 0;
                    $strFechaRadicacion = replaceChar(strClean($sheet->getCell("AA$i")->getValue()));
                    $arrFechaRadicacion = explode("/",$strFechaRadicacion);
                    $arrFechaTransaccion = explode("/",$strFechaTransaccion);
                    $strFechaTransaccion = $arrFechaTransaccion[2]."-".$arrFechaTransaccion[1]."-".$arrFechaTransaccion[0];
                    $strFechaRadicacion = $arrFechaRadicacion[2]."-".$arrFechaRadicacion[1]."-".$arrFechaRadicacion[0];
                    $strAntiguo = $strAntiguo == "SI" ? "S" : "N";
                    $strBlindado = $strBlindado == "SI" ? "S" : "N";
                    $strEstado = $strEstado == "cancelado" ? 1 : 0;
                    $request = $this->insertData(
                        $strPlaca,
                        $strVigencia,
                        $strPeriodo,
                        $intAvaluo,
                        $intValorDescuento,
                        $intValorInteres,
                        $intValorPago,
                        $intValorSinDescuento,
                        $intValorDescMatricula,
                        $intValorSancion,
                        $intValorDepartamento,
                        $intValorMunicipio,
                        $intValorInteresTotal,
                        $intValorSancionTotal,
                        $intValorAnterior,
                        $intValorSaldoPagar,
                        $intValorSaldoFavor,
                        $intSemaforizacion,
                        $strDocumento,
                        $strNombre,
                        $intPorcentajeDescuento,
                        $intPorcentajeInteres,
                        $intModelo,
                        $strAntiguo,
                        $strBlindado,
                        $strCilindraje,
                        $intTarifa,
                        $intDiasInteres,
                        $strEstado,
                        $strFechaTransaccion,
                        $strFechaRadicacion
                    );
                    if($request > 0){
                        $arrResponse = array("status"=>true,"msg"=>"Cartera cargada correctamente");
                    }else{
                        $arrResponse = array("status"=>false,"msg"=>"No se ha podido cargar, inténte de nuevo.");
                        break;
                    }

                }
                echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
            }
            die();
        }
        public function getPlantilla(){
            if(!empty($_SESSION)){
                $objPHPExcel = new PHPExcel();
                $objPHPExcel->getActiveSheet()->getStyle('A:AA')->applyFromArray(
                    array(
                        'alignment' => array(
                            'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
                            'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER
                        )
                    )
                );
                $objPHPExcel->getProperties()
                ->setCreator("IDEAL 10")
                ->setLastModifiedBy("IDEAL 10")
                ->setTitle("Exportar Excel con PHP")
                ->setSubject("Documento de prueba")
                ->setDescription("Documento generado con PHPExcel")
                ->setKeywords("usuarios phpexcel")
                ->setCategory("reportes");

                $borders = array(
                    'borders' => array(
                        'allborders' => array(
                            'style' => PHPExcel_Style_Border::BORDER_THIN,
                        'color' => array('argb' => 'FF000000'),
                        )
                    ),
                );
                $objPHPExcel->setActiveSheetIndex(0)
                    ->mergeCells('A1:AA1')
                    ->setCellValue('A1', 'BASE CARTERA')
                    ->setCellValue('A2', "VIGENCIA")
                    ->setCellValue('B2', 'PLACA')
                    ->setCellValue('C2', 'AVALUO')
                    ->setCellValue('D2', 'VALOR DESCUENTO')
                    ->setCellValue('E2', 'VALOR INTERES')
                    ->setCellValue('F2', 'VALOR PAGO')
                    ->setCellValue('G2', "TARIFA")
                    ->setCellValue('H2', "ESTADO")
                    ->setCellValue('I2', "FECHA TRANSACCION")
                    ->setCellValue('J2', "VALOR SIN DESCUENTO")
                    ->setCellValue('K2', "VALOR SANCION")
                    ->setCellValue('L2', "SALDO A PAGAR")
                    ->setCellValue('M2', "SEMAFORIZACION")
                    ->setCellValue('N2', "VALOR DEPARTAMENTO")
                    ->setCellValue('O2', "VALOR MUNICIPIO")
                    ->setCellValue('P2', "PERIODO")
                    ->setCellValue('Q2', "VALOR INTERES TOTAL")
                    ->setCellValue('R2', "VALOR SANCION TOTAL")
                    ->setCellValue('S2', "DOCUMENTO")
                    ->setCellValue('T2', "NOMBRE")
                    ->setCellValue('U2', "PORCENTAJE DESCUENTO")
                    ->setCellValue('V2', "PORCENTAJE INTERES")
                    ->setCellValue('W2', "MODELO")
                    ->setCellValue('X2', "ANTIGUO")
                    ->setCellValue('Y2', "BLINDADO")
                    ->setCellValue('Z2', "CILINDRAJE")
                    ->setCellValue('AA2', "FECHA RADICACION");

                $objPHPExcel->setActiveSheetIndex(0)
                -> getStyle ('A1:AA1')
                -> getAlignment ()
                -> applyFromArray (array ( 'horizontal'  =>  PHPExcel_Style_Alignment :: HORIZONTAL_CENTER ,) );

                $objPHPExcel->setActiveSheetIndex(0)
                -> getStyle ('A2:AA2')
                -> getAlignment ()
                -> applyFromArray (array ( 'horizontal'  =>  PHPExcel_Style_Alignment :: HORIZONTAL_CENTER ,) );

                $objPHPExcel-> getActiveSheet ()
                -> getStyle ("A1:AA1")
                -> getFill ()
                -> setFillType (PHPExcel_Style_Fill :: FILL_SOLID)
                -> getStartColor ()
                -> setRGB ('3399cc');


                $objPHPExcel-> getActiveSheet ()
                -> getStyle ("A2:AA2")
                -> getFill ()
                -> setFillType (PHPExcel_Style_Fill :: FILL_SOLID)
                -> getStartColor ()
                -> setRGB ('99ddff');

                $objPHPExcel->getActiveSheet()->getStyle('A1:AA1')->applyFromArray($borders);
                $objPHPExcel->getActiveSheet()->getStyle('A2:AA2')->applyFromArray($borders);

                $objPHPExcel->getActiveSheet()->getStyle("A1:AA1")->getFont()->setBold(true);
                $objPHPExcel->getActiveSheet()->getStyle("A1")->getFont()->getColor()->setRGB("ffffff");

                $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
                $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
                $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
                $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
                $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
                $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
                $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);
                $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setAutoSize(true);
                $objPHPExcel->getActiveSheet()->getColumnDimension('I')->setAutoSize(true);
                $objPHPExcel->getActiveSheet()->getColumnDimension('J')->setAutoSize(true);
                $objPHPExcel->getActiveSheet()->getColumnDimension('K')->setAutoSize(true);
                $objPHPExcel->getActiveSheet()->getColumnDimension('L')->setAutoSize(true);
                $objPHPExcel->getActiveSheet()->getColumnDimension('M')->setAutoSize(true);
                $objPHPExcel->getActiveSheet()->getColumnDimension('N')->setAutoSize(true);
                $objPHPExcel->getActiveSheet()->getColumnDimension('O')->setAutoSize(true);
                $objPHPExcel->getActiveSheet()->getColumnDimension('P')->setAutoSize(true);
                $objPHPExcel->getActiveSheet()->getColumnDimension('Q')->setAutoSize(true);
                $objPHPExcel->getActiveSheet()->getColumnDimension('R')->setAutoSize(true);
                $objPHPExcel->getActiveSheet()->getColumnDimension('S')->setAutoSize(true);
                $objPHPExcel->getActiveSheet()->getColumnDimension('T')->setAutoSize(true);
                $objPHPExcel->getActiveSheet()->getColumnDimension('U')->setAutoSize(true);
                $objPHPExcel->getActiveSheet()->getColumnDimension('V')->setAutoSize(true);
                $objPHPExcel->getActiveSheet()->getColumnDimension('W')->setAutoSize(true);
                $objPHPExcel->getActiveSheet()->getColumnDimension('X')->setAutoSize(true);
                $objPHPExcel->getActiveSheet()->getColumnDimension('Y')->setAutoSize(true);
                $objPHPExcel->getActiveSheet()->getColumnDimension('Z')->setAutoSize(true);
                $objPHPExcel->getActiveSheet()->getColumnDimension('AA')->setAutoSize(true);

                //----Guardar documento----
                header('Content-Type: application/vnd.ms-excel');
                header('Content-Disposition: attachment;filename="plantilla_base_cartera.xlsx"');
                header('Cache-Control: max-age=0');
                $objWriter=PHPExcel_IOFactory::createWriter($objPHPExcel,'Excel2007');
                $objWriter->save('php://output');
            }
            die();
        }
        public function getData(){
            if(!empty($_SESSION)){
                $arrResponse = [
                    "periodos"=>$this->selectPeriodos(),
                    "vigencias"=>$this->selectVigencias(),
                ];
                echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
            }
            die();
        }

        public function getSearch(){
            if(!empty($_SESSION)){
                $intPaginas = intval($_POST['paginas']);
                $intPaginaNow = intval($_POST['pagina']);
                $strPeriodo = strtolower(replaceChar(strClean($_POST['periodo'])));
                $strPlaca = strtoupper(replaceChar(strClean($_POST['placa'])));
                $strVigencia = strtolower(replaceChar(strClean($_POST['vigencia'])));
                $strBuscar = ucfirst(strtolower(replaceChar(strClean($_POST['buscar']))));
                $request = $this->selectSearch($intPaginas,$strVigencia,$intPaginaNow, $strPeriodo,$strPlaca, $strBuscar);
                $arrData = $request['data'];
                $totalPaginas = $request['total'];
                $startPage = max(1, $intPaginaNow - floor(CANT_BTNS / 2));
                if ($startPage + CANT_BTNS - 1 > $totalPaginas) {
                    $startPage = max(1, $totalPaginas - CANT_BTNS + 1);
                }
                $limitPages = min($startPage + CANT_BTNS, $totalPaginas+1);
                $arrResponse = array("data"=>$arrData,"start_page"=>$startPage,"limit_page"=>$limitPages,"total_pages"=>$totalPaginas);
                echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
            }
            die();
        }
    }

    if($_POST){
        $obj = new BaseCarteraController();
        if($_POST['action'] == "upload"){
            $obj->upload();
        }else if($_POST['action'] == "get"){
            $obj->getData();
        }else if($_POST['action'] == "search"){
            $obj->getSearch();
        }else if($_POST['action']=="plantilla"){
            $obj->getPlantilla();
        }
    }

?>
