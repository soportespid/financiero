<?php
    require_once '../../Librerias/core/Helpers.php';
    require_once '../models/CaracterizacionVehicularModel.php';
    session_start();

    class CaracterizacionVehicularController extends CaracterizacionVehicularModel{
        private $arrOpciones = [
            1=>["nombre"=>"Caracterizacion de vehiculo - tipo de servicio","tabla"=>"trans_caracterizacion_servicio"],
            2=>['nombre'=>"Caracterizacion de vehiculo - clase","tabla"=>"trans_caracterizacion_clase"],
            3=>['nombre'=>"Caracterizacion de vehiculo - tipo de combustible","tabla"=>"trans_caracterizacion_combustible"],
            4=>["nombre"=>"Caracterizacion de vehiculo - cilindraje","tabla"=>"trans_caracterizacion_cilindraje"],
            5=>["nombre"=>"Caracterizacion de vehiculo - publico","tabla"=>"trans_caracterizacion_publico"],
            6=>["nombre"=>"Caracterizacion de vehiculo - ocupantes","tabla"=>"trans_caracterizacion_ocupantes"],
            7=>["nombre"=>"Caracterizacion de vehiculo - capacidad","tabla"=>"trans_caracterizacion_capacidad"],
            8=>["nombre"=>"Caracterizacion de vehiculo - potencia HP","tabla"=>"trans_caracterizacion_potencia"],
            9=>["nombre"=>"Caracterizacion de vehiculo - marca","tabla"=>"trans_caracterizacion_marca"],
            10=>["nombre"=>"Caracterizacion de vehiculo - color","tabla"=>"trans_caracterizacion_color"],
            11=>["nombre"=>"Caracterizacion de vehiculo - linea","tabla"=>"trans_caracterizacion_linea"],
            12=>["nombre"=>"Caracterizacion de vehiculo - linea","tabla"=>"trans_caracterizacion_carroceria"],
        ];
        public function getData(){
            if(!empty($_SESSION)){
                $request = $this->selectMarcas();
                echo json_encode($request,JSON_UNESCAPED_UNICODE);
            }
        }
        /***********************Tipo de servicio**********************/
        public function save(){
            if(!empty($_SESSION)){
                if(empty($_POST['nombre'])){
                    $arrResponse = array("status"=>false,"msg"=>"Error de datos");
                }else{
                    $strNombre = ucfirst(strtolower(strClean(replaceChar($_POST['nombre']))));
                    $intTipoOpcion = intval($_POST['opcion']);
                    $intConsecutivo = intval($_POST['consecutivo']);
                    $intMarca = strClean($_POST['marca']);
                    $opcion="";
                    if($intConsecutivo == 0){
                        $opcion = 1;
                        $request = $this->insertData($strNombre,$this->arrOpciones[$intTipoOpcion]['tabla'],$intMarca);
                    }else{
                        $opcion = 2;
                        $request = $this->updateData($intConsecutivo,$strNombre,$this->arrOpciones[$intTipoOpcion]['tabla'],$intMarca);
                    }
                    if($request > 0){
                        if($opcion == 1){
                            insertAuditoria("trans_auditoria","trans_funciones_id",$intTipoOpcion,"Crear",$request,$this->arrOpciones[$intTipoOpcion]['nombre'],"trans_funciones");
                            $arrResponse = array("status"=>true,"msg"=>"Datos guardados correctamente.","id"=>$request);
                        }else{
                            insertAuditoria("trans_auditoria","trans_funciones_id",$intTipoOpcion,"Editar",$intConsecutivo,$this->arrOpciones[$intTipoOpcion]['nombre'],"trans_funciones");
                            $arrResponse = array("status"=>true,"msg"=>"Datos actualizados correctamente.");
                        }
                    }else if($request =="existe"){
                        $arrResponse = array("status"=>false,"msg"=>"Este nombre ya existe, pruebe con otro.");
                    }else{
                        $arrResponse = array("status"=>false,"msg"=>"No se ha podido guardar, inténte de nuevo.");
                    }
                }
                echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
            }
            die();
        }
        public function getEdit(){
            if(!empty($_SESSION)){
                $intId = intval($_POST['codigo']);
                $intTipoOpcion = intval($_POST['opcion']);
                $request = $this->selectEdit($intId,$this->arrOpciones[$intTipoOpcion]['tabla'],$intTipoOpcion);
                if(!empty($request)){
                    $arrResponse = array("status"=>true,"data"=>$request,"consecutivos"=>getConsecutivos($this->arrOpciones[$intTipoOpcion]['tabla'],"id"));
                }else{
                    $arrResponse = array("status"=>false,"consecutivo"=>searchConsec($this->arrOpciones[$intTipoOpcion]['tabla'],"id")-1);
                }
                echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
            }
            die();
        }
        public function getSearch(){
            if(!empty($_SESSION)){
                $intTipoOpcion = intval($_POST['opcion']);
                $intMarca = strClean($_POST['marca']);
                $request = $this->selectData($this->arrOpciones[$intTipoOpcion]['tabla'],$intTipoOpcion);
                echo json_encode($request,JSON_UNESCAPED_UNICODE);
            }
            die();
        }
        public function changeData(){
            if(!empty($_SESSION)){
                $id = intval($_POST['id']);
                $estado = $_POST['estado'];
                $intTipoOpcion = intval($_POST['opcion']);
                $request = $this->updateStatus($id,$estado,$this->arrOpciones[$intTipoOpcion]['tabla']);
                if($request == 1){
                    insertAuditoria("trans_auditoria","trans_funciones_id",$intTipoOpcion,"Editar estado",$id,$this->arrOpciones[$intTipoOpcion]['nombre'],"trans_funciones");
                    $arrResponse = array("status"=>true);
                }else{
                    $arrResponse = array("status"=>false,"msg"=>"Error");
                }
                echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
            }
            die();
        }
    }

    if($_POST){
        $obj = new CaracterizacionVehicularController();
        if($_POST['action'] == "save"){
            $obj->save();
        }else if($_POST['action']=="edit"){
            $obj->getEdit();
        }else if($_POST['action']=="search"){
            $obj->getSearch();
        }else if($_POST['action']=="change"){
            $obj->changeData();
        }else if($_POST['action']=="get"){
            $obj->getData();
        }
    }

?>
