<?php
    require_once '../../tcpdf/tcpdf_include.php';
    require_once '../../tcpdf/tcpdf.php';
    require_once '../../Librerias/core/Helpers.php';

    session_start();
    class MYPDF extends TCPDF {

		public function Header()
		{
			$request = configBasica();
            $strNit = $request['nit'];
            $strRazon = $request['razonsocial'];

			//Parte Izquierda
			$this->Image('../../imagenes/escudo.jpg',  13, 13, 20, 20, 'JPG', '', 'T', true, 300, '', false, false, 0, false, false, false);// Logo
			$this->SetFont('helvetica','B',8);
			$this->SetY(10);
			$this->RoundedRect(10, 10, 190, 25, 1,'');
			$this->Cell(0.1);
			$this->Cell(26,25,'','R',0,'L');
			$this->SetY(8);
			$this->SetX(40);
			$this->SetFont('helvetica','B',9);
			$this->Cell(140,15,strtoupper("$strRazon"),0,0,'C');
			$this->SetFont('helvetica','B',7);
			$this->SetY(12);
			$this->SetX(40);
			$this->Cell(140,15,'NIT: '.$strNit,0,0,'C');
			//*****************************************************************************************************************************
			$this->SetFont('helvetica','B',9);
			$this->SetY(23);
			$this->SetX(36);
            $this->Cell(164,12,"RECIBO DE IMPUESTO VEHICULAR",'T',0,'C');


            $this->SetFont('helvetica','B',7);
			$this->SetY(10);
			$this->SetX(167);
			$this->Cell(30,7," FECHA: ". date("d/m/Y"),"L",0,'L');
			$this->SetY(17);
			$this->SetX(167);
            $this->Cell(35,6,"","L",0,'L');

            $this->SetFont('helvetica','B',7);
			$this->SetY(15);
			$this->SetX(167);
			$this->Cell(30,7," No. Recibo: ". ID,"L",0,'L');
			$this->SetY(25);
			$this->SetX(167);
            $this->Cell(35,6,"","",0,'L');

			//**********************************************************
			$this->SetFont('times','B',10);
			$this->ln(12);
			//**********************************************************
            if(MOV=="401"){
                $img_file = './assets/img/reversado.png';
                $this->Image($img_file, 0, 20, 250, 280, '', '', '', false, 300, '', false, false, 0);
            }
		}
		public function Footer(){

			$request = configBasica();
            $strDireccion = $request['direccion'] != "" ? "Dirección: ".strtoupper($request['direccion']) : "";
            $strWeb = $request['web'] != "" ? "Pagina web: ".strtoupper($request['web']) :"";
            $strEmail = $request['email'] !="" ? "Email: ".strtoupper($request['email']) :"";
            $strTelefono = $request['telefono'] != "" ? "Telefonos: ".$request['telefono'] : "";
            $strUsuario = searchUser($_SESSION['cedulausu'])['nom_usu'];
			$strNick = $_SESSION['nickusu'];
			$strFecha = date("d/m/Y H:i:s");
			$strIp = $_SERVER['REMOTE_ADDR'];


            $this->setY(230);
            $this->setX(60);
            $this->SetFont('helvetica','B',9);
            $this->SetFillColor(51, 153, 204);
            $this->SetTextColor(255,255,255);
            $this->cell(85,4,'Recibido y sello','LRTB',0,'C',1);
            $this->ln();
            $this->setX(60);
            $this->SetFont('helvetica','',7);
            $this->SetTextColor(0,0,0);
            $this->SetFillColor(255,255,255);
            $this->cell(85,20,'','LR',0,'L',1);
            $this->ln();
            $this->setX(60);
            $this->cell(85,4,"",'LR',0,'L',1);
            $this->ln();
            $this->setX(60);
            $this->cell(85,4,"",'LRB',0,'L',1);

            $this->ln();
            $this->ln();
            $this->ln();

			$this->SetFont('helvetica', 'I', 8);
			$txt = <<<EOD
			$strDireccion $strTelefono
			$strEmail $strWeb
			EOD;
			$this->SetFont('helvetica', 'I', 6);
			$this->Cell(190,10,'','T',0,'T');
			$this->ln(2);
			$this->Write(0, $txt , '', 0, 'C', true, 0, false, false, 0);

			$this->Cell(50, 10, 'Hecho por: '.$strUsuario, 00, false, 'C', 0, '', 0, false, 'T', 'M');
			$this->Cell(30, 10, 'Impreso por: '.$strNick, 0, false, 'C', 0, '', 0, false, 'T', 'M');
			$this->Cell(30, 10, 'IP: '.$strIp, 0, false, 'C',0, '', 0, false, 'T', 'M');
			$this->Cell(30, 10, 'Fecha: '.$strFecha, 0, false, 'C', 0, '', 0, false, 'T', 'M');
			$this->Cell(30, 10, 'IDEAL.10 S.A.S', 0, false, 'C', 0, '', 0, false, 'T', 'M');
            $this->Cell(20, 10, 'Pagina '.$this->getAliasNumPage().'/'.$this->getAliasNbPages(), 0, false, 'C', 0, '', 0, false, 'T', 'M');
		}
	}
    class ReciboImpuestoExportController{

        public function exportPdf(){
            if(!empty($_SESSION)){
                $arrData = json_decode($_POST['data'],true);
                if(!empty($arrData)){
                    define("ID",$arrData['id']);
                    define("FECHA",$arrData['fecha_format']);

                    $pdf = new MYPDF('P','mm','Letter', true, 'iso-8859-1', false);
                    $pdf->SetDocInfoUnicode (true);
                    // set document information
                    $pdf->SetCreator(PDF_CREATOR);
                    $pdf->SetAuthor('IDEALSAS');
                    $pdf->SetTitle('RECIBO IMPUESTO VEHICULAR');
                    $pdf->SetSubject("RECIBO IMPUESTO VEHICULAR");
                    $pdf->SetKeywords('TCPDF, PDF, example, test, guide');
                    $pdf->SetMargins(10, 38, 10);// set margins
                    $pdf->SetHeaderMargin(38);// set margins
                    $pdf->SetFooterMargin(17);// set margins
                    $pdf->SetAutoPageBreak(TRUE, 20);// set auto page breaks
                    // set some language-dependent strings (optional)
                    if (@file_exists(dirname(__FILE__).'/lang/spa.php'))
                    {
                        require_once(dirname(__FILE__).'/lang/spa.php');
                        $pdf->setLanguageArray($l);
                    }
                    $intAlturaTexto = 12;
                    $intAlturaCelda = 5;
                    $pdf->AddPage();
                    $pdf->SetFillColor(51, 153, 204);
                    $pdf->SetTextColor(255,255,255);
                    $pdf->SetFont('Helvetica','b',$intAlturaTexto);
                    $pdf->MultiCell(190,$intAlturaCelda,"Información","LRBT",'C',true,0,'','',true,0,false,true,0,'M',true);
                    $pdf->ln();

                    $pdf->SetFillColor(153,221,255);
                    $pdf->SetTextColor(0,0,0);
                    $pdf->SetFont('Helvetica','b',$intAlturaTexto);
                    $pdf->MultiCell(30,$intAlturaCelda,"Nombre","LRBT",'L',true,0,'','',true,0,false,true,0,'M',true);
                    $pdf->SetFillColor(255,255,255);
                    $pdf->SetTextColor(0,0,0);
                    $pdf->SetFont('Helvetica','',$intAlturaTexto);
                    $pdf->MultiCell(160,$intAlturaCelda,$arrData['nombre_propietario'],"LRBT",'L',true,0,'','',true,0,false,true,0,'M',true);
                    $pdf->ln();
                    $pdf->SetFillColor(153,221,255);
                    $pdf->SetTextColor(0,0,0);
                    $pdf->SetFont('Helvetica','b',$intAlturaTexto);
                    $pdf->MultiCell(30,$intAlturaCelda,"Placa","LRBT",'L',true,0,'','',true,0,false,true,0,'M',true);
                    $pdf->SetFillColor(255,255,255);
                    $pdf->SetTextColor(0,0,0);
                    $pdf->SetFont('Helvetica','',$intAlturaTexto);
                    $pdf->MultiCell(160,$intAlturaCelda,$arrData['placa'],"LRBT",'L',true,0,'','',true,0,false,true,0,'M',true);
                    $pdf->ln();
                    $pdf->SetFillColor(153,221,255);
                    $pdf->SetTextColor(0,0,0);
                    $pdf->SetFont('Helvetica','b',$intAlturaTexto);
                    $pdf->MultiCell(30,$intAlturaCelda,"CC/NIT","LRBT",'L',true,0,'','',true,0,false,true,0,'M',true);
                    $pdf->SetFillColor(255,255,255);
                    $pdf->SetTextColor(0,0,0);
                    $pdf->SetFont('Helvetica','',$intAlturaTexto);
                    $pdf->MultiCell(160,$intAlturaCelda,$arrData['propietario'],"LRBT",'L',true,0,'','',true,0,false,true,0,'M',true);
                    $pdf->ln();

                    $pdf->SetFillColor(153,221,255);
                    $pdf->SetTextColor(0,0,0);
                    $pdf->SetFont('Helvetica','b',$intAlturaTexto);
                    $pdf->MultiCell(30,$intAlturaCelda,"Medio de pago","LRBT",'L',true,0,'','',true,0,false,true,0,'M',true);
                    $pdf->SetFillColor(255,255,255);
                    $pdf->SetTextColor(0,0,0);
                    $pdf->SetFont('Helvetica','',$intAlturaTexto);
                    $pdf->MultiCell(160,$intAlturaCelda,$arrData['medio_pago'],"LRBT",'L',true,0,'','',true,0,false,true,0,'M',true);
                    $pdf->ln();
                    $pdf->SetFillColor(153,221,255);
                    $pdf->SetTextColor(0,0,0);
                    $pdf->SetFont('Helvetica','b',$intAlturaTexto);
                    $pdf->MultiCell(30,$intAlturaCelda,"Cuenta bancaria","LRBT",'L',true,0,'','',true,0,false,true,0,'M',true);
                    $pdf->SetFillColor(255,255,255);
                    $pdf->SetTextColor(0,0,0);
                    $pdf->SetFont('Helvetica','',$intAlturaTexto);
                    $pdf->MultiCell(160,$intAlturaCelda,$arrData['cuenta_bancaria'],"LRBT",'L',true,0,'','',true,0,false,true,0,'M',true);
                    $pdf->ln();

                    $pdf->SetFillColor(153,221,255);
                    $pdf->SetTextColor(0,0,0);
                    $pdf->SetFont('Helvetica','b',$intAlturaTexto);
                    $pdf->MultiCell(30,$intAlturaCelda,"Valor","LRBT",'L',true,0,'','',true,0,false,true,0,'M',true);
                    $pdf->SetFillColor(255,255,255);
                    $pdf->SetTextColor(0,0,0);
                    $pdf->SetFont('Helvetica','',$intAlturaTexto);
                    $pdf->MultiCell(160,$intAlturaCelda,formatNum($arrData['valor_recaudo']),"LRBT",'L',true,0,'','',true,0,false,true,0,'M',true);
                    $pdf->ln();
                    $pdf->ln();
                    $pdf->ln();
                    $pdf->SetFillColor(51, 153, 204);
                    $pdf->SetTextColor(255,255,255);
                    $pdf->SetFont('Helvetica','b',$intAlturaTexto);
                    $pdf->MultiCell(190,$intAlturaCelda,"Detalle","LRBT",'C',true,0,'','',true,0,false,true,0,'M',true);
                    $pdf->ln();
                    $pdf->SetFillColor(255,255,255);
                    $pdf->SetTextColor(0,0,0);
                    $pdf->SetFont('Helvetica','',$intAlturaTexto);
                    $pdf->MultiCell(190,$intAlturaCelda,$arrData['detalle'],"LRBT",'L',true,0,'','',true,0,false,true,0,'M',true);



                    $pdf->Output('recibo_impuesto_vehicular.pdf', 'I');
                }
            }
            die();
        }
    }

    if($_POST){
        $obj = new ReciboImpuestoExportController();
        if($_POST['action']=="pdf"){
            $obj->exportPdf();
        }

    }

?>
