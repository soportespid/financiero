<?php
    require_once '../../Librerias/core/Helpers.php';
    require_once '../models/TramitesModel.php';
    session_start();

    class tramitesController extends TramitesModel{
        public function getData(){
            if(!empty($_SESSION)){
                //$request['consecutivo'] = $this->selectConsecutivo();
                $request['conceptos'] = $this->selectConceptos();
                $request['modal_cuentas'] = $this->selectCuentas($_POST['search']);
                $request['modal_servicios'] = $this->selectServicios($_POST['search']);
                $request['modal_bienes'] = $this->selectBienes($_POST['search']);
                $request['modal_cuin'] = $this->selectCuin($_POST['search']);
                $request['modal_fuente'] = $this->selectFuentes($_POST['search']);
                $request['secciones']=$this->selectSeccionesPresupuestales();
                echo json_encode($request,JSON_UNESCAPED_UNICODE);
            }
            die();
        }
        public function search(){
            if(!empty($_SESSION)){
                $request['modal_cuentas'] = $this->selectCuentas($_POST['search']);
                $request['modal_servicios'] = $this->selectServicios($_POST['search']);
                $request['modal_bienes'] = $this->selectBienes($_POST['search']);
                $request['modal_cuin'] = $this->selectCuin($_POST['search']);
                $request['modal_fuente'] = $this->selectFuentes($_POST['search']);
                echo json_encode($request,JSON_UNESCAPED_UNICODE);
            }
            die();
        }
        public function save(){
            if(!empty($_SESSION)){

                $arrDataHead = json_decode($_POST['dataHead'],true);
                $arrDataDetail = json_decode($_POST['dataDetail'],true);

                if(empty($arrDataHead) || empty($arrDataDetail)){
                    $arrResponse = array("status"=>false,"msg"=>"Error de datos");
                }else{
                    $arrCabecera = $arrDataHead;
                    $arrDetalle = $arrDataDetail;
                    $opcion = "";
                    if($_POST['opciones'] == 0){
                        $opcion = 1;
                        $request = $this->insertData($arrCabecera);
                        $id = $request;
                        if(is_numeric($request) && $request > 0){
                            $request = $this->insertDet($id,$arrDetalle);
                            $arrResponse = array("status"=>true,"msg"=>"Datos guardados","consecutivo"=>$id);
                        }
                    }else{
                        $opcion = 2;
                        $request = $this->updateData($arrCabecera);
                        $id = $arrCabecera['codigo'];
                    }
                   /*  if(is_numeric($request) && $request > 0){
                        $request = $this->insertDet($id,$arrCabecera,$arrDetalle);
                        if(is_numeric($request) && $request > 0){
                            if($opcion == 1){
                                insertAuditoria("trans_auditoria","trans_funciones_id",16,"Crear",$id,"Tramites servicios tyt","trans_funciones");
                                $arrResponse = array("status"=>true,"msg"=>"Datos guardados","consecutivo"=>$id);
                            }else{
                                insertAuditoria("trans_auditoria","trans_funciones_id",16,"Editar",$id,"Tramites servicios tyt","trans_funciones");
                                $arrResponse = array("status"=>true,"msg"=>"Datos actualizados");
                            }
                            if($arrCabecera['terceros'] == 2 && $arrCabecera['is_presupuesto'] == 2){
                                $request = $this->insertFuentes($id,$arrCabecera,$arrDetalle);
                                if(is_numeric($request) && $request > 0){
                                    if($opcion == 1){
                                        insertAuditoria("trans_auditoria","trans_funciones_id",16,"Crear",$id,"Tramites servicios tyt","trans_funciones");
                                        $arrResponse = array("status"=>true,"msg"=>"Datos guardados","consecutivo"=>$id);
                                    }else{
                                        insertAuditoria("trans_auditoria","trans_funciones_id",16,"Editar",$id,"Tramites servicios tyt","trans_funciones");
                                        $arrResponse = array("status"=>true,"msg"=>"Datos actualizados");
                                    }
                                }else{
                                    if($opcion == 1){
                                        insertAuditoria("trans_auditoria","trans_funciones_id",16,"Crear",$id,"Tramites servicios tyt","trans_funciones");
                                        $arrResponse = array("status"=>true,"msg"=>"Datos guardados","consecutivo"=>$id);
                                    }else{
                                        insertAuditoria("trans_auditoria","trans_funciones_id",16,"Editar",$id,"Tramites servicios tyt","trans_funciones");
                                        $arrResponse = array("status"=>true,"msg"=>"Datos actualizados");
                                    }
                                }
                            }else{
                                $arrResponse = array("status"=>true,"msg"=>"Datos actualizados");
                            }
                        }else{
                            $arrResponse = array("status"=>false,"msg"=>"Error en ingreso detalle");
                        }
                    }else{
                        $arrResponse = array("status"=>false,"msg"=>"Error en ingreso cabecera");
                    } */
                }
                echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
            }
            die();
        }
        public function getEdit(){
            if(!empty($_SESSION)){
                $codigo = strClean($_POST['codigo']);
                $arrData = array(
					"status"=>true,
					"infoCabeceras"=>$this->selectIngreso($codigo),
                    "infoDetalles"=>$this->selectDet($codigo,true)
				);
                echo json_encode($arrData,JSON_UNESCAPED_UNICODE);
                //echo json_encode($request,JSON_UNESCAPED_UNICODE);
            }
            die();
        }
        public function getSearch(){
            if(!empty($_SESSION)){
                $intPaginas = intval($_POST['paginas']);
                $intPaginaNow = intval($_POST['pagina']);
                $strSearch = ucfirst(strtolower(replaceChar(strClean($_POST['buscar']))));
                $request = $this->selectSearch($intPaginas,$intPaginaNow,$strSearch);
                $arrData = $request['data'];
                $totalPaginas = $request['total'];
                $startPage = max(1, $intPaginaNow - floor(CANT_BTNS / 2));
                if ($startPage + CANT_BTNS - 1 > $totalPaginas) {
                    $startPage = max(1, $totalPaginas - CANT_BTNS + 1);
                }
                $limitPages = min($startPage + CANT_BTNS, $totalPaginas+1);
                $arrResponse = array("data"=>$arrData,"start_page"=>$startPage,"limit_page"=>$limitPages,"total_pages"=>$totalPaginas);
                echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
            }
            die();
        }
        public function getClasificador(){
            if(!empty($_SESSION)){
                $arrResponse = $this->selectClasificador($_POST['cuenta']);
                echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
            }
            die();
        }
    }

    if($_POST){
        $obj = new tramitesController();
        switch ($_POST['action']){
            case 'search':
                $obj->getSearch();break;
            case 'get':
                $obj->getData();break;
            case 'clasificador':
                $obj->getClasificador();break;
            case 'save':
                $obj->save();break;
            case 'edit':
                $obj->getEdit();break;
        }
    }

?>
