<?php
    require_once '../../Librerias/core/Helpers.php';
    require_once '../../PHPExcel/Classes/PHPExcel.php';
    require_once '../models/DescuentosModel.php';

    session_start();
    class DescuentosController extends DescuentosModel{

        public function getData(){
            if(!empty($_SESSION)){

            }
            die();
        }
        public function save(){
            if(!empty($_SESSION)){
                if(empty($_POST['tipo']) || empty($_POST['porcentaje'])){
                    $arrResponse = array("status"=>false,"msg"=>"Error de datos");
                }else{
                    $intConsecutivo = intval($_POST['consecutivo']);
                    $strFecha = ucfirst(strtolower(replaceChar(strClean($_POST['fecha']))));
                    $intTipo = intval($_POST['tipo']);
                    $floPorcentaje = floatval($_POST['porcentaje']);
                    $opcion="";
                    if($intConsecutivo == 0){
                        $opcion = 1;
                        $request = $this->insertData($strFecha,$intTipo,$floPorcentaje);
                    }else{
                        $opcion = 2;
                        $request = $this->updateData($intConsecutivo,$strFecha,$intTipo,$floPorcentaje);
                    }
                    if($request > 0){
                        if($opcion == 1){
                            insertAuditoria("trans_auditoria","trans_funciones_id",15,"Crear",$request,"Descuentos","trans_funciones");
                            $arrResponse = array("status"=>true,"msg"=>"Datos guardados correctamente.","id"=>$request);
                        }else{
                            insertAuditoria("trans_auditoria","trans_funciones_id",15,"Editar",$intConsecutivo,"Descuentos","trans_funciones");
                            $arrResponse = array("status"=>true,"msg"=>"Datos actualizados correctamente.");
                        }
                    }else{
                        $arrResponse = array("status"=>false,"msg"=>"No se ha podido guardar, inténte de nuevo.");
                    }
                }
                echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
            }
            die();
        }
        public function getEdit(){
            if(!empty($_SESSION)){
                $intId = intval($_POST['codigo']);
                $request = $this->selectEdit($intId);
                if(!empty($request)){
                    $arrResponse = array("status"=>true,"data"=>$request,"consecutivos"=>getConsecutivos("trans_descuentos","id"));
                }else{
                    $arrResponse = array("status"=>false,"consecutivo"=>searchConsec("trans_descuentos","id")-1);
                }
                echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
            }
            die();
        }
        public function getSearch(){
            if(!empty($_SESSION)){
                $intPaginas = intval($_POST['paginas']);
                $intPaginaNow = intval($_POST['pagina']);
                $strTipo = strtolower(replaceChar(strClean($_POST['tipo'])));
                $request = $this->selectSearch($intPaginas,$intPaginaNow, $strTipo);
                $arrData = $request['data'];
                $totalPaginas = $request['total'];
                $startPage = max(1, $intPaginaNow - floor(CANT_BTNS / 2));
                if ($startPage + CANT_BTNS - 1 > $totalPaginas) {
                    $startPage = max(1, $totalPaginas - CANT_BTNS + 1);
                }
                $limitPages = min($startPage + CANT_BTNS, $totalPaginas+1);
                $arrResponse = array("data"=>$arrData,"start_page"=>$startPage,"limit_page"=>$limitPages,"total_pages"=>$totalPaginas);
                echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
            }
            die();
        }
        public function changeData(){
            if(!empty($_SESSION)){
                $id = intval($_POST['id']);
                $estado = strClean($_POST['estado']);
                $request = $this->updateStatus($id,$estado);
                if($request == 1){
                    insertAuditoria("trans_auditoria","trans_funciones_id",15,"Editar estado",$id,"Descuentos","trans_funciones");
                    $arrResponse = array("status"=>true);
                }else{
                    $arrResponse = array("status"=>false,"msg"=>"Error");
                }
                echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
            }
            die();
        }
    }

    if($_POST){
        $obj = new DescuentosController();
        if($_POST['action'] == "get"){
            $obj->getData();
        }else if($_POST['action'] == "search"){
            $obj->getSearch();
        }else if($_POST['action']=="save"){
            $obj->save();
        }else if($_POST['action']=="edit"){
            $obj->getEdit();
        }else if($_POST['action']=="change"){
            $obj->changeData();
        }
    }

?>
