<?php
    require_once '../../Librerias/core/Helpers.php';
    require_once '../../PHPExcel/Classes/PHPExcel.php';
    require_once '../models/LiquidacionIngresoModel.php';
    session_start();

    class LiquidacionIngresoController extends LiquidacionIngresoModel{
        public function initialData(){
            if(!empty($_SESSION)){
                $arrResponse = array(
                    "terceros"=>$this->selectTerceros(),
                    "centros"=>$this->selectCentroCostos(),
                    "ingresos"=>$this->selectIngresos(),
                    "liquidaciones"=>$this->selectLiquidaciones(),
                    "secciones"=>$this->selectSeccionesPresupuestales()
                );
                echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
            }
            die();
        }
        public function selectIngreso(){
            if(!empty($_SESSION)){
                if($_POST){
                    $intCausacion = intval($_POST['codigoSearch']);
                    $request = $this->searchIngreso($intCausacion);
                    echo json_encode($request,JSON_UNESCAPED_UNICODE);
                }
            }
            die();
        }
        public function save(){
            if(!empty($_SESSION)){
                if($_POST){
                    $strMov = strClean($_POST['mov']);
                    if($strMov == "101"){
                        if(checkBlock($_SESSION['cedulausu'],$_POST['fecha'])){
                            if(empty($_POST['concepto']) || empty($_POST['tercero'])){
                                $arrResponse = array("status"=>false,"msg"=>"Error de datos");
                            }else{
                                $arrData = json_decode($_POST['data'],true);
                                if(!is_array($arrData) || empty($arrData)){
                                    $arrResponse = array("status"=>false,"msg"=>"No hay ingresos para guardar.");
                                }else{
                                    $intCausacion = intval($_POST['causacion']);
                                    $strFecha = strClean($_POST['fecha']);
                                    $strConcepto = strClean(replaceChar($_POST['concepto']));
                                    $strConcepto = $intCausacion == 2 ? "ESTE DOCUMENTO NO REQUIERE CAUSACION CONTABLE - ".$strConcepto : $strConcepto;
                                    $strConcepto = strtoupper($strConcepto);
                                    $strTercero = strClean($_POST['tercero']);
                                    $strMedioPago = strClean($_POST['mediopago']) == "CSF" ? 1 : 2;
                                    $floatTotal = floatval($_POST['total']);
                                    $intConsecutivo = searchConsec("trans_sin_recaudos_cab", "id_recaudo");
                                    $request = $this->insertComprobanteCab($intConsecutivo,$strFecha,$strConcepto,$floatTotal);
                                    if($request > 0){
                                        $request = $this->insertLiquidacionCab($request,$strFecha,$floatTotal,$strConcepto,$strTercero, $strMedioPago);
                                        if($request > 0){
                                            $request = $this->insertLiquidacionDet($request,$arrData,$intCausacion,$intConsecutivo,$strTercero);
                                            if($request > 0){
                                                $arrResponse = array("status"=>true,"msg"=>"Datos guardados","id"=>$intConsecutivo);
                                            }else{
                                                $arrResponse = array("status"=>false,"msg"=>"Error en el detalle");
                                            }
                                        }else{
                                            $arrResponse =  array("status"=>false,"msg"=>"Error en la cabecera del recaudo");
                                        }
                                    }else{
                                        $arrResponse = array("status"=>false,"msg"=>"Error en la cabecera del comprobante");
                                    }
                                }
                            }
                        }else{
                            $arrResponse = array("status"=>false,"msg"=>"Usuario, esta fecha se encuentra bloqueda.");
                        }
                    }else{
                        if(checkBlock($_SESSION['cedulausu'],$_POST['fecha_rev'])){
                            if(empty($_POST['fecha_rev']) || empty($_POST['concepto_rev']) || empty($_POST['id_rev'])){
                                $arrResponse = array("status"=>false,"msg"=>"Error de datos");
                            }else{
                                $strFechaRev = strClean($_POST['fecha_rev']);
                                $strConceptoRev = strtoupper(strClean(replaceChar($_POST['concepto_rev'])));
                                $intId = floatval($_POST['id_rev']);
                                $request = $this->selectRecaudo($intId);
                                if(!empty($request)){
                                    $requestRev = $this->insertReversar($intId,$strFechaRev,$strConceptoRev,$request);
                                    if($requestRev > 0){
                                        $arrResponse = array("status"=>true,"msg"=>"Datos guardados correctamente","id"=>$intId);
                                    }else if($request == "recaudo"){
                                        $arrResponse = array("status"=>false,"msg"=>"Error al actualizar cabecera de liquidacion");
                                    }else if($request == "recaudo_cab"){
                                        $arrResponse = array("status"=>false,"msg"=>"Error al guardar cabecera de liquidacion");
                                    }else if($request == "comp_det"){
                                        $arrResponse = array("status"=>false,"msg"=>"Error al guardar detalle de comprobante de ingreso reversado");
                                    }else if($request == "comp"){
                                        $arrResponse = array("status"=>false,"msg"=>"Error al guardar cabecera de comprobante de ingreso reversado");
                                    }else if($request == "existe"){
                                        $arrResponse = array("status"=>false,"msg"=>"Esta liquidación ya se ha reversado, intente con otro.");
                                    }
                                }
                            }

                        }else{
                            $arrResponse = array("status"=>false,"msg"=>"Usuario, esta fecha se encuentra bloqueda.");
                        }
                    }
                    echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
                }
            }
            die();
        }
        public function getEdit(){
            if(!empty($_SESSION)){
                if($_POST){
                    $intId = intval($_POST['codigo']);
                    $request = $this->selectEdit($intId);
                    if(!empty($request)){
                        $arrData['recibo'] = array_values(array_filter($request,function($e){return $e['tipo_mov'] == 101;}))[0];
                        if(count($request) > 1){
                            $arrData['reversado'] = array_values(array_filter($request,function($e){return $e['tipo_mov'] == 301;}))[0];
                        }
                        $arrResponse = array("status"=>true,"data"=>$arrData,"consecutivos"=>getConsecutivos("trans_sin_recaudos_cab","id_recaudo"));
                    }else{
                        $arrResponse = array("status"=>false,"consecutivo"=>searchConsec("trans_sin_recaudos_cab ","id_recaudo")-1);
                    }
                    echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
                }
            }
            die();
        }
        public function getSearch(){
            if(!empty($_SESSION)){
                if($_POST){
                    $strFechaInicial = strClean($_POST['fecha_inicial']);
                    $strFechaFinal = strClean($_POST['fecha_final']);
                    $strSearch = strClean($_POST['search']);
                    $request = $this->selectSearch($strFechaInicial,$strFechaFinal,$strSearch);
                    echo json_encode($request,JSON_UNESCAPED_UNICODE);
                }
            }
            die();
        }
    }

    if($_POST){
        $obj = new LiquidacionIngresoController();
        if($_POST['action'] == "get"){
            $obj->initialData();
        }else if ($_POST['action'] == "selectIng"){
            $obj->selectIngreso();
        }else if($_POST['action']=="save"){
            $obj->save();
        }else if($_POST['action']=="edit"){
            $obj->getEdit();
        }else if($_POST['action']=="gen"){
            $obj->getSearch();
        }
    }

?>
