<?php
    require_once '../../tcpdf/tcpdf_include.php';
    require_once '../../tcpdf/tcpdf.php';
    require_once '../../Librerias/core/Helpers.php';

    session_start();
    class MYPDF extends TCPDF {

		public function Header()
		{
			$request = configBasica();
            $strNit = $request['nit'];
            $strRazon = $request['razonsocial'];

			//Parte Izquierda
			$this->Image('../../imagenes/escudo.jpg',  13, 13, 20, 20, 'JPG', '', 'T', true, 300, '', false, false, 0, false, false, false);// Logo
			$this->SetFont('helvetica','B',8);
			$this->SetY(10);
			$this->RoundedRect(10, 10, 190, 25, 1,'');
			$this->Cell(0.1);
			$this->Cell(26,25,'','R',0,'L');
			$this->SetY(8);
			$this->SetX(40);
			$this->SetFont('helvetica','B',9);
			$this->Cell(140,15,strtoupper("$strRazon"),0,0,'C');
			$this->SetFont('helvetica','B',7);
			$this->SetY(12);
			$this->SetX(40);
			$this->Cell(140,15,'NIT: '.$strNit,0,0,'C');
			//*****************************************************************************************************************************
			$this->SetFont('helvetica','B',9);
			$this->SetY(23);
			$this->SetX(36);
            $this->Cell(164,12,"DECLARACIÓN DE IMPUESTO VEHICULAR",'T',0,'C');


            $this->SetFont('helvetica','B',7);
			$this->SetY(10);
			$this->SetX(167);
			$this->Cell(30,7," FECHA: ". date("d/m/Y"),"L",0,'L');
			$this->SetY(17);
			$this->SetX(167);
            $this->Cell(35,6,"","L",0,'L');

            $this->SetFont('helvetica','B',7);
			$this->SetY(15);
			$this->SetX(167);
			$this->Cell(30,7," DECLARACION: ". ID,"L",0,'L');
			$this->SetY(25);
			$this->SetX(167);
            $this->Cell(35,6,"","",0,'L');

			//**********************************************************
			$this->SetFont('times','B',10);
			$this->ln(12);
			//**********************************************************
            if(ESTADO=="N"){
                $img_file = '../../assets/img/anulado.png';
                $this->SetAlpha(0.35);
                $this->Image($img_file, 0, 20, 250, 280, '', '', '', false, 300, '', false, false, 300);
                $this->SetAlpha(1);
            }
		}
		public function Footer(){

			$request = configBasica();
            $strDireccion = $request['direccion'] != "" ? "Dirección: ".strtoupper($request['direccion']) : "";
            $strWeb = $request['web'] != "" ? "Pagina web: ".strtoupper($request['web']) :"";
            $strEmail = $request['email'] !="" ? "Email: ".strtoupper($request['email']) :"";
            $strTelefono = $request['telefono'] != "" ? "Telefonos: ".$request['telefono'] : "";
            $strUsuario = searchUser($_SESSION['cedulausu'])['nom_usu'];
			$strNick = $_SESSION['nickusu'];
			$strFecha = date("d/m/Y H:i:s");
			$strIp = $_SERVER['REMOTE_ADDR'];

            $this->setY(230);
            $this->setX(10);
            $this->SetFont('helvetica','B',9);
            $this->SetFillColor(51, 153, 204);
            $this->SetTextColor(255,255,255);
            $this->cell(85,4,'Firma','LRTB',0,'C',1);
            $this->ln();
            $this->setX(10);
            $this->SetFont('helvetica','',7);
            $this->SetTextColor(0,0,0);
            $this->SetFillColor(255,255,255);
            $this->cell(85,20,'','LRTB',0,'L',1);
            $this->ln();
            $this->setX(10);
            $this->cell(85,4," Nombre: ".strtoupper( NOMBRE),'LRTB',0,'L',1);
            $this->ln();
            $this->setX(10);
            $this->cell(85,4," CC/NIT: ".strtoupper( CC),'LRTB',0,'L',1);

            $this->setY(230);
            $this->setX(115);
            $this->SetFont('helvetica','B',9);
            $this->SetFillColor(51, 153, 204);
            $this->SetTextColor(255,255,255);
            $this->cell(85,4,'Timbre y sello del banco','LRTB',0,'C',1);
            $this->ln();
            $this->setX(115);
            $this->SetFont('helvetica','',7);
            $this->SetTextColor(0,0,0);
            $this->SetFillColor(255,255,255);
            $this->cell(85,20,'','LR',0,'L',1);
            $this->ln();
            $this->setX(115);
            $this->cell(85,4,"",'LR',0,'L',1);
            $this->ln();
            $this->setX(115);
            $this->cell(85,4,"",'LRB',0,'L',1);

            $this->ln();
            $this->ln();
            $this->ln();

			$this->SetFont('helvetica', 'I', 8);
			$txt = <<<EOD
			$strDireccion $strTelefono
			$strEmail $strWeb
			EOD;
			$this->SetFont('helvetica', 'I', 6);
			$this->Cell(190,10,'','T',0,'T');
			$this->ln(2);
			$this->Write(0, $txt , '', 0, 'C', true, 0, false, false, 0);

			$this->Cell(50, 10, 'Hecho por: '.$strUsuario, 00, false, 'C', 0, '', 0, false, 'T', 'M');
			$this->Cell(30, 10, 'Impreso por: '.$strNick, 0, false, 'C', 0, '', 0, false, 'T', 'M');
			$this->Cell(30, 10, 'IP: '.$strIp, 0, false, 'C',0, '', 0, false, 'T', 'M');
			$this->Cell(30, 10, 'Fecha: '.$strFecha, 0, false, 'C', 0, '', 0, false, 'T', 'M');
			$this->Cell(30, 10, 'IDEAL.10 S.A.S', 0, false, 'C', 0, '', 0, false, 'T', 'M');
            $this->Cell(20, 10, 'Pagina '.$this->getAliasNumPage().'/'.$this->getAliasNbPages(), 0, false, 'C', 0, '', 0, false, 'T', 'M');
		}
	}
    class DeclaracionImpuestoExportController{

        public function exportPdf(){
            if(!empty($_SESSION)){
                $arrData = json_decode($_POST['data'],true);
                if(!empty($arrData)){
                    $arrDetalle = $arrData['detalle'];
                    $arrAutomotor = $arrData['automotor'];
                    define("ID",$arrData['id']);
                    define("FECHA",$arrData['fecha_format']);
                    define("NOMBRE",$arrAutomotor['nombre_tercero']);
                    define("CC",$arrData['propietario']);
                    define("ESTADO",$arrData['estado']);

                    $pdf = new MYPDF('P','mm','Letter', true, 'iso-8859-1', false);
                    $pdf->SetDocInfoUnicode (true);
                    // set document information
                    $pdf->SetCreator(PDF_CREATOR);
                    $pdf->SetAuthor('IDEALSAS');
                    $pdf->SetTitle('DECLARACIÓN IMPUESTO VEHICULAR');
                    $pdf->SetSubject("DECLARACIÓN IMPUESTO VEHICULAR");
                    $pdf->SetKeywords('TCPDF, PDF, example, test, guide');
                    $pdf->SetMargins(10, 38, 10);// set margins
                    $pdf->SetHeaderMargin(38);// set margins
                    $pdf->SetFooterMargin(17);// set margins
                    $pdf->SetAutoPageBreak(TRUE, 20);// set auto page breaks
                    // set some language-dependent strings (optional)
                    if (@file_exists(dirname(__FILE__).'/lang/spa.php'))
                    {
                        require_once(dirname(__FILE__).'/lang/spa.php');
                        $pdf->setLanguageArray($l);
                    }
                    $pdf->AddPage();
                    $pdf->SetFillColor(51, 153, 204);
                    $pdf->SetTextColor(255,255,255);
                    $pdf->SetFont('Helvetica','b',9);
                    $pdf->MultiCell(190,4,"Declarante","LRBT",'C',true,0,'','',true,0,false,true,0,'M',true);
                    $pdf->ln();

                    $pdf->SetFillColor(153,221,255);
                    $pdf->SetTextColor(0,0,0);
                    $pdf->SetFont('Helvetica','b',9);
                    $pdf->MultiCell(30,4,"Nombre","LRBT",'L',true,0,'','',true,0,false,true,0,'M',true);
                    $pdf->SetFillColor(255,255,255);
                    $pdf->SetTextColor(0,0,0);
                    $pdf->SetFont('Helvetica','',9);
                    $pdf->MultiCell(70,4,$arrAutomotor['nombre_tercero'],"LRBT",'L',true,0,'','',true,0,false,true,0,'M',true);
                    $pdf->SetFillColor(153,221,255);
                    $pdf->SetTextColor(0,0,0);
                    $pdf->SetFont('Helvetica','b',9);
                    $pdf->MultiCell(30,4,"CC/NIT","LRBT",'L',true,0,'','',true,0,false,true,0,'M',true);
                    $pdf->SetFillColor(255,255,255);
                    $pdf->SetTextColor(0,0,0);
                    $pdf->SetFont('Helvetica','',9);
                    $pdf->MultiCell(60,4,$arrAutomotor['propietario'],"LRBT",'L',true,0,'','',true,0,false,true,0,'M',true);
                    $pdf->ln();

                    $pdf->SetFillColor(153,221,255);
                    $pdf->SetTextColor(0,0,0);
                    $pdf->SetFont('Helvetica','b',9);
                    $pdf->MultiCell(30,4,"Departamento","LRBT",'L',true,0,'','',true,0,false,true,0,'M',true);
                    $pdf->SetFillColor(255,255,255);
                    $pdf->SetTextColor(0,0,0);
                    $pdf->SetFont('Helvetica','',9);
                    $pdf->MultiCell(70,4,$arrAutomotor['departamento_tercero'],"LRBT",'L',true,0,'','',true,0,false,true,0,'M',true);
                    $pdf->SetFillColor(153,221,255);
                    $pdf->SetTextColor(0,0,0);
                    $pdf->SetFont('Helvetica','b',9);
                    $pdf->MultiCell(30,4,"Municipio","LRBT",'L',true,0,'','',true,0,false,true,0,'M',true);
                    $pdf->SetFillColor(255,255,255);
                    $pdf->SetTextColor(0,0,0);
                    $pdf->SetFont('Helvetica','',9);
                    $pdf->MultiCell(60,4,$arrAutomotor['municipio_tercero'],"LRBT",'L',true,0,'','',true,0,false,true,0,'M',true);
                    $pdf->ln();

                    $pdf->SetFillColor(153,221,255);
                    $pdf->SetTextColor(0,0,0);
                    $pdf->SetFont('Helvetica','b',9);
                    $pdf->MultiCell(30,4,"Dirección","LRBT",'L',true,0,'','',true,0,false,true,0,'M',true);
                    $pdf->SetFillColor(255,255,255);
                    $pdf->SetTextColor(0,0,0);
                    $pdf->SetFont('Helvetica','',9);
                    $pdf->MultiCell(70,4,$arrAutomotor['direccion'],"LRBT",'L',true,0,'','',true,0,false,true,0,'M',true);
                    $pdf->SetFillColor(153,221,255);
                    $pdf->SetTextColor(0,0,0);
                    $pdf->SetFont('Helvetica','b',9);
                    $pdf->MultiCell(30,4,"Teléfono","LRBT",'L',true,0,'','',true,0,false,true,0,'M',true);
                    $pdf->SetFillColor(255,255,255);
                    $pdf->SetTextColor(0,0,0);
                    $pdf->SetFont('Helvetica','',9);
                    $pdf->MultiCell(60,4,$arrAutomotor['celular'],"LRBT",'L',true,0,'','',true,0,false,true,0,'M',true);
                    $pdf->ln();

                    $pdf->SetFillColor(51, 153, 204);
                    $pdf->SetTextColor(255,255,255);
                    $pdf->SetFont('Helvetica','b',9);
                    $pdf->MultiCell(190,4,"Datos del vehículo","LRBT",'C',true,0,'','',true,0,false,true,0,'M',true);
                    $pdf->ln();

                    $pdf->SetFillColor(153,221,255);
                    $pdf->SetTextColor(0,0,0);
                    $pdf->SetFont('Helvetica','b',9);
                    $pdf->MultiCell(20,4,"Placa","LRBT",'L',true,0,'','',true,0,false,true,0,'M',true);
                    $pdf->SetFillColor(255,255,255);
                    $pdf->SetTextColor(0,0,0);
                    $pdf->SetFont('Helvetica','',9);
                    $pdf->MultiCell(20,4,$arrAutomotor['placa'],"LRBT",'L',true,0,'','',true,0,false,true,0,'M',true);
                    $pdf->SetFillColor(153,221,255);
                    $pdf->SetTextColor(0,0,0);
                    $pdf->SetFont('Helvetica','b',9);
                    $pdf->MultiCell(20,4,"Fecha matrícula","LRBT",'L',true,0,'','',true,0,false,true,0,'M',true);
                    $pdf->SetFillColor(255,255,255);
                    $pdf->SetTextColor(0,0,0);
                    $pdf->SetFont('Helvetica','',9);
                    $pdf->MultiCell(20,4,$arrAutomotor['fecha_format'],"LRBT",'L',true,0,'','',true,0,false,true,0,'M',true);
                    $pdf->SetFillColor(153,221,255);
                    $pdf->SetTextColor(0,0,0);
                    $pdf->SetFont('Helvetica','b',9);
                    $pdf->MultiCell(20,4,"Departamento","LRBT",'L',true,0,'','',true,0,false,true,0,'M',true);
                    $pdf->SetFillColor(255,255,255);
                    $pdf->SetTextColor(0,0,0);
                    $pdf->SetFont('Helvetica','',9);
                    $pdf->MultiCell(30,4,$arrAutomotor['departamento'],"LRBT",'L',true,0,'','',true,0,false,true,0,'M',true);
                    $pdf->SetFillColor(153,221,255);
                    $pdf->SetTextColor(0,0,0);
                    $pdf->SetFont('Helvetica','b',9);
                    $pdf->MultiCell(30,4,"Municipio","LRBT",'L',true,0,'','',true,0,false,true,0,'M',true);
                    $pdf->SetFillColor(255,255,255);
                    $pdf->SetTextColor(0,0,0);
                    $pdf->SetFont('Helvetica','',9);
                    $pdf->MultiCell(30,4,$arrAutomotor['municipio'],"LRBT",'L',true,0,'','',true,0,false,true,0,'M',true);
                    $pdf->ln();

                    $pdf->SetFillColor(153,221,255);
                    $pdf->SetTextColor(0,0,0);
                    $pdf->SetFont('Helvetica','b',9);
                    $pdf->MultiCell(20,4,"Clase","LRBT",'L',true,0,'','',true,0,false,true,0,'M',true);
                    $pdf->SetFillColor(255,255,255);
                    $pdf->SetTextColor(0,0,0);
                    $pdf->SetFont('Helvetica','',9);
                    $pdf->MultiCell(20,4,$arrAutomotor['clase'],"LRBT",'L',true,0,'','',true,0,false,true,0,'M',true);
                    $pdf->SetFillColor(153,221,255);
                    $pdf->SetTextColor(0,0,0);
                    $pdf->SetFont('Helvetica','b',9);
                    $pdf->MultiCell(20,4,"Marca","LRBT",'L',true,0,'','',true,0,false,true,0,'M',true);
                    $pdf->SetFillColor(255,255,255);
                    $pdf->SetTextColor(0,0,0);
                    $pdf->SetFont('Helvetica','',9);
                    $pdf->MultiCell(20,4,$arrAutomotor['marca'],"LRBT",'L',true,0,'','',true,0,false,true,0,'M',true);
                    $pdf->SetFillColor(153,221,255);
                    $pdf->SetTextColor(0,0,0);
                    $pdf->SetFont('Helvetica','b',9);
                    $pdf->MultiCell(20,4,"Línea","LRBT",'L',true,0,'','',true,0,false,true,0,'M',true);
                    $pdf->SetFillColor(255,255,255);
                    $pdf->SetTextColor(0,0,0);
                    $pdf->SetFont('Helvetica','',9);
                    $pdf->MultiCell(90,4,$arrAutomotor['linea'],"LRBT",'L',true,0,'','',true,0,false,true,0,'M',true);
                    $pdf->ln();

                    $pdf->SetFillColor(153,221,255);
                    $pdf->SetTextColor(0,0,0);
                    $pdf->SetFont('Helvetica','b',9);
                    $pdf->MultiCell(20,4,"Modelo","LRBT",'L',true,0,'','',true,0,false,true,0,'M',true);
                    $pdf->SetFillColor(255,255,255);
                    $pdf->SetTextColor(0,0,0);
                    $pdf->SetFont('Helvetica','',9);
                    $pdf->MultiCell(20,4,$arrAutomotor['modelo'],"LRBT",'L',true,0,'','',true,0,false,true,0,'M',true);
                    $pdf->SetFillColor(153,221,255);
                    $pdf->SetTextColor(0,0,0);
                    $pdf->SetFont('Helvetica','b',9);
                    $pdf->MultiCell(20,4,"Color","LRBT",'L',true,0,'','',true,0,false,true,0,'M',true);
                    $pdf->SetFillColor(255,255,255);
                    $pdf->SetTextColor(0,0,0);
                    $pdf->SetFont('Helvetica','',9);
                    $pdf->MultiCell(20,4,$arrAutomotor['color'],"LRBT",'L',true,0,'','',true,0,false,true,0,'M',true);
                    $pdf->SetFillColor(153,221,255);
                    $pdf->SetTextColor(0,0,0);
                    $pdf->SetFont('Helvetica','b',9);
                    $pdf->MultiCell(20,4,"Carrocería","LRBT",'L',true,0,'','',true,0,false,true,0,'M',true);
                    $pdf->SetFillColor(255,255,255);
                    $pdf->SetTextColor(0,0,0);
                    $pdf->SetFont('Helvetica','',9);
                    $pdf->MultiCell(90,4,$arrAutomotor['carroceria'],"LRBT",'L',true,0,'','',true,0,false,true,0,'M',true);
                    $pdf->ln();

                    $pdf->SetFillColor(153,221,255);
                    $pdf->SetTextColor(0,0,0);
                    $pdf->SetFont('Helvetica','b',9);
                    $pdf->MultiCell(20,4,"Antiguo","LRBT",'L',true,0,'','',true,0,false,true,0,'M',true);
                    $pdf->SetFillColor(255,255,255);
                    $pdf->SetTextColor(0,0,0);
                    $pdf->SetFont('Helvetica','',9);
                    $pdf->MultiCell(20,4,$arrAutomotor['antiguo'] == "N" ? "No" : "Si","LRBT",'L',true,0,'','',true,0,false,true,0,'M',true);
                    $pdf->SetFillColor(153,221,255);
                    $pdf->SetTextColor(0,0,0);
                    $pdf->SetFont('Helvetica','b',9);
                    $pdf->MultiCell(20,4,"Blindado","LRBT",'L',true,0,'','',true,0,false,true,0,'M',true);
                    $pdf->SetFillColor(255,255,255);
                    $pdf->SetTextColor(0,0,0);
                    $pdf->SetFont('Helvetica','',9);
                    $pdf->MultiCell(20,4,$arrAutomotor['antiguo'] == "N" ? "No" : "Si","LRBT",'L',true,0,'','',true,0,false,true,0,'M',true);
                    $pdf->SetFillColor(153,221,255);
                    $pdf->SetTextColor(0,0,0);
                    $pdf->SetFont('Helvetica','b',9);
                    $pdf->MultiCell(20,4,"Cilindraje","LRBT",'L',true,0,'','',true,0,false,true,0,'M',true);
                    $pdf->SetFillColor(255,255,255);
                    $pdf->SetTextColor(0,0,0);
                    $pdf->SetFont('Helvetica','',9);
                    $pdf->MultiCell(90,4,$arrAutomotor['cilindraje'],"LRBT",'L',true,0,'','',true,0,false,true,0,'M',true);
                    $pdf->ln();

                    $pdf->SetFillColor(153,221,255);
                    $pdf->SetTextColor(0,0,0);
                    $pdf->SetFont('Helvetica','b',9);
                    $pdf->MultiCell(20,4,"Pasajeros","LRBT",'L',true,0,'','',true,0,false,true,0,'M',true);
                    $pdf->SetFillColor(255,255,255);
                    $pdf->SetTextColor(0,0,0);
                    $pdf->SetFont('Helvetica','',9);
                    $pdf->MultiCell(20,4,$arrAutomotor['pasajeros'],"LRBT",'L',true,0,'','',true,0,false,true,0,'M',true);
                    $pdf->SetFillColor(153,221,255);
                    $pdf->SetTextColor(0,0,0);
                    $pdf->SetFont('Helvetica','b',9);
                    $pdf->MultiCell(20,4,"Tonelaje","LRBT",'L',true,0,'','',true,0,false,true,0,'M',true);
                    $pdf->SetFillColor(255,255,255);
                    $pdf->SetTextColor(0,0,0);
                    $pdf->SetFont('Helvetica','',9);
                    $pdf->MultiCell(20,4,$arrAutomotor['carga'],"LRBT",'L',true,0,'','',true,0,false,true,0,'M',true);
                    $pdf->SetFillColor(153,221,255);
                    $pdf->SetTextColor(0,0,0);
                    $pdf->SetFont('Helvetica','b',9);
                    $pdf->MultiCell(20,4,"Tipo de servicio","LRBT",'L',true,0,'','',true,0,false,true,0,'M',true);
                    $pdf->SetFillColor(255,255,255);
                    $pdf->SetTextColor(0,0,0);
                    $pdf->SetFont('Helvetica','',9);
                    $pdf->MultiCell(90,3.8,$arrAutomotor['servicio'],"LRBT",'L',true,0,'','',true,0,false,true,0,'M',true);
                    $pdf->ln();
                    $pdf->ln();
                    $pdf->SetFillColor(51, 153, 204);
                    $pdf->SetTextColor(255,255,255);
                    $pdf->SetFont('Helvetica','b',9);
                    $pdf->MultiCell(190,4,"Detalle","LRBT",'C',true,0,'','',true,0,false,true,0,'M',true);
                    $pdf->ln();

                    $pdf->SetFillColor(153,221,255);
                    $pdf->SetTextColor(0,0,0);
                    $pdf->SetFont('Helvetica','b',9);

                    $pdf->MultiCell(20,4,"Vigencia","LRBT",'C',true,0,'','',true,0,false,true,0,'M',true);
                    $pdf->MultiCell(25,4,"Avalúo","LRBT",'C',true,0,'','',true,0,false,true,0,'M',true);
                    $pdf->MultiCell(20,4,"Tarifa","LRBT",'C',true,0,'','',true,0,false,true,0,'M',true);
                    $pdf->MultiCell(20,4,"Impuesto","LRBT",'C',true,0,'','',true,0,false,true,0,'M',true);
                    $pdf->MultiCell(20,4,"Descuento","LRBT",'C',true,0,'','',true,0,false,true,0,'M',true);
                    $pdf->MultiCell(20,4,"Sancion","LRBT",'C',true,0,'','',true,0,false,true,0,'M',true);
                    $pdf->MultiCell(20,4,"Dias mora","LRBT",'C',true,0,'','',true,0,false,true,0,'M',true);
                    $pdf->MultiCell(20,4,"Interés mora","LRBT",'C',true,0,'','',true,0,false,true,0,'M',true);
                    $pdf->MultiCell(25,4,"Total a pagar","LRBT",'C',true,0,'','',true,0,false,true,0,'M',true);

                    $pdf->ln();
                    $pdf->SetFillColor(255,255,255);
                    $pdf->SetTextColor(0,0,0);
                    $pdf->SetFont('Helvetica','',9);
                    foreach ($arrDetalle as $data) {
                        $pdf->MultiCell(20,4,$data['vigencia'],"LRBT",'C',true,0,'','',true,0,false,true,0,'M',true);
                        $pdf->MultiCell(25,4,formatNum($data['avaluo']),"LRBT",'R',true,0,'','',true,0,false,true,0,'M',true);
                        $pdf->MultiCell(20,4,$data['tarifa']."%","LRBT",'C',true,0,'','',true,0,false,true,0,'M',true);
                        $pdf->MultiCell(20,4,formatNum($data['impuesto']),"LRBT",'R',true,0,'','',true,0,false,true,0,'M',true);
                        $pdf->MultiCell(20,4,formatNum($data['impuesto_descuento']),"LRBT",'R',true,0,'','',true,0,false,true,0,'M',true);
                        $pdf->MultiCell(20,4,formatNum($data['sancion']),"LRBT",'R',true,0,'','',true,0,false,true,0,'M',true);
                        $pdf->MultiCell(20,4,$data['dias_mora'],"LRBT",'C',true,0,'','',true,0,false,true,0,'M',true);
                        $pdf->MultiCell(20,4,formatNum($data['interes_mora']),"LRBT",'R',true,0,'','',true,0,false,true,0,'M',true);
                        $pdf->MultiCell(25,4,formatNum($data['total']),"LRBT",'R',true,0,'','',true,0,false,true,0,'M',true);
                        $pdf->ln();
                    }
                    $pdf->SetFillColor(153,221,255);
                    $pdf->SetTextColor(0,0,0);
                    $pdf->SetFont('Helvetica','b',9);

                    $pdf->MultiCell(20,4,"Totales:","LRBT",'R',true,0,'','',true,0,false,true,0,'M',true);
                    $pdf->SetFillColor(255,255,255);
                    $pdf->SetTextColor(0,0,0);
                    $pdf->SetFont('Helvetica','',9);
                    $pdf->MultiCell(25,4,formatNum($arrData['total_avaluo']),"LRBT",'R',true,0,'','',true,0,false,true,0,'M',true);
                    $pdf->MultiCell(20,4,"","LRBT",'C',true,0,'','',true,0,false,true,0,'M',true);
                    $pdf->MultiCell(20,4,formatNum($arrData['total_impuesto']),"LRBT",'R',true,0,'','',true,0,false,true,0,'M',true);
                    $pdf->MultiCell(20,4,formatNum($arrData['total_descuento']),"LRBT",'R',true,0,'','',true,0,false,true,0,'M',true);
                    $pdf->MultiCell(20,4,formatNum($arrData['total_sancion']),"LRBT",'R',true,0,'','',true,0,false,true,0,'M',true);
                    $pdf->MultiCell(20,4,"","LRBT",'C',true,0,'','',true,0,false,true,0,'M',true);
                    $pdf->MultiCell(20,4,formatNum($arrData['total_interes_mora']),"LRBT",'R',true,0,'','',true,0,false,true,0,'M',true);
                    $pdf->SetFont('Helvetica','b',9);
                    $pdf->MultiCell(25,4,formatNum($arrData['total_pago']),"LRBT",'R',true,0,'','',true,0,false,true,0,'M',true);
                    $pdf->ln();
                    $pdf->ln();

                    $pdf->SetFillColor(51, 153, 204);
                    $pdf->SetTextColor(255,255,255);
                    $pdf->SetFont('Helvetica','b',9);
                    $pdf->MultiCell(190,4,"Distribución de recaudo","LRBT",'C',true,0,'','',true,0,false,true,0,'M',true);
                    $pdf->ln();

                    $pdf->SetFillColor(153,221,255);
                    $pdf->SetTextColor(0,0,0);
                    $pdf->SetFont('Helvetica','b',9);
                    $pdf->MultiCell(47.5,4,"Municipio 20% - Ahorro 735-001234","LRBT",'L',true,0,'','',true,0,false,true,0,'M',true);
                    $pdf->SetFillColor(255,255,255);
                    $pdf->SetTextColor(0,0,0);
                    $pdf->SetFont('Helvetica','',9);
                    $pdf->MultiCell(47.5,3.8,formatNum($arrData['total_municipio']),"LRBT",'R',true,0,'','',true,0,false,true,0,'M',true);
                    $pdf->SetFillColor(153,221,255);
                    $pdf->SetTextColor(0,0,0);
                    $pdf->SetFont('Helvetica','b',9);
                    $pdf->MultiCell(47.5,4,"Departamento 80% convenio 22570","LRBT",'L',true,0,'','',true,0,false,true,0,'M',true);
                    $pdf->SetFillColor(255,255,255);
                    $pdf->SetTextColor(0,0,0);
                    $pdf->SetFont('Helvetica','',9);
                    $pdf->MultiCell(47.5,4,formatNum($arrData['total_departamento']),"LRBT",'R',true,0,'','',true,0,false,true,0,'M',true);
                    $pdf->ln();
                    $pdf->ln();

                    $pdf->SetFont('Helvetica','B',9);
                    $pdf->MultiCell(150,4,"PRESENTAR FORMULARIO EN BBVA","",'L',true,0,'','',true,0,false,true,0,'M',true);
                    $pdf->ln();
                    $pdf->MultiCell(150,4,"DEPARTAMENTO: CONVENIO 22570","",'L',true,0,'','',true,0,false,true,0,'M',true);
                    $pdf->ln();
                    $pdf->MultiCell(150,4,"MUNICIPIO: CUENTA DE AHORRO 735-001234 TRÁNSITO MUNICIPAL","",'L',true,0,'','',true,0,false,true,0,'M',true);


                    $pdf->Output('declaracion_impuesto_vehicular.pdf', 'I');
                }
            }
            die();
        }
    }

    if($_POST){
        $obj = new DeclaracionImpuestoExportController();
        if($_POST['action']=="pdf"){
            $obj->exportPdf();
        }

    }

?>
