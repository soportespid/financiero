<?php
    require_once '../../Librerias/core/Helpers.php';
    require_once '../../Librerias/core/Mysql.php';
    session_start();
    ini_set('display_errors', '1');
    ini_set('display_startup_errors', '1');
    error_reporting(E_ERROR);
    class RecaudosModel extends Mysql{
        private $intConsecutivo;
        private $intPaginas;
        private $intPaginaNow;
        private $intInicio;
        function __construct(){
            parent::__construct();
        }
        public function insertPrecio($codigo,$precio){
            $fecha=date('Y-m-d h:i:s');
			//Desactiva cualquier precio antiguo con este mismo código
            $sql="UPDATE trans_servicios_precios SET estado=? WHERE ingreso='$codigo'";
            $request = $this->update($sql,['N']);

            //Ingresa valores del nuevo precio con este código
            $sql="INSERT INTO trans_servicios_precios (ingreso,precio,fecha,estado) VALUES (?,?,?,?)";
            $request = $this->insert($sql,[$codigo,$precio,$fecha,'S']);
            return $request;
        }
        public function insertFuentes($id,$cabecera,$detalle){
            $fuentes = $detalle['fuentes'];
            $totalFuentes = count($fuentes);
            $sql = "UPDATE trans_servicios_fuentes SET estado=? WHERE codigo = '$id'";
			$request = $this->update($sql,['N']);
            for ($i=0; $i < $totalFuentes; $i++) {
                $codigo_fuente = $fuentes[$i]['codigo'];
                $sql = "INSERT INTO trans_servicios_fuentes (codigo, fuente, estado) VALUES (?,?,?)";
                $request = $this->update($sql,[$id,$codigo_fuente,'S']);
            }
            return intval($request);
        }
        public function insertDet($id,$cabecera,$detalle){
            $clasificador ="";
            $cuenta = "";
            $this->delete("DELETE FROM trans_servicios_det WHERE codigo ='$cabecera[codigo]'");
            if($cabecera['terceros'] == 2){
                $cuenta= $detalle['cuenta'];
                $clasificador = $detalle['clasificador'];
            }
            if($cabecera['tipo']=="C"){
                $cuentas = $detalle['cuentas'];
                $totalCuentas = count($cuentas);
                for ($i=0; $i < $totalCuentas ; $i++) {
                    $idConcepto = $cuentas[$i]['id_concepto'];
                    $tipoConcepto = $cuentas[$i]['tipo_concepto'];
                    $porcentaje = $cuentas[$i]['porcentaje'];
                    $nroCuenta = $cuentas[$i]['cuenta'];
                    $clasificador = $cuentas[$i]['cuenta_clasificadora'];
                    $fuente = $cuentas[$i]['fuente'];
                    $seccion = $cuentas[$i]['seccion'];
                    $sql="INSERT INTO trans_servicios_det (codigo,concepto,modulo,tipoconce,porcentaje,cuentapres,cuenta_clasificadora,estado,fuente,seccion)
                    VALUES (?,?,?,?,?,?,?,?,?,?)";
                    $arrData = [
                        $id,
                        $idConcepto."-",
                        '14',
                        $tipoConcepto,
                        $porcentaje,
                        $nroCuenta,
                        $clasificador,
                        'S',
                        $fuente,
                        $seccion
                    ];
                    $request = intval($this->insert($sql,$arrData));
                }
            }else{
                $sql="INSERT INTO trans_servicios_det (codigo,concepto,modulo,tipoconce,porcentaje,cuentapres,cuenta_clasificadora,estado)
                VALUES (?,?,?,?,?,?,?,?)";
                $arrData = [$id,$detalle['concepto'],'14', $detalle['tipo_concepto'], $detalle['porcentaje'],$cuenta,$clasificador,'S'];
                $request = intval($this->insert($sql,$arrData));
            }
            return $request;
        }
        public function insertData($arrCabecera){
            $sql = "INSERT INTO trans_servicios (nombre,tipo,estado,terceros,tipoIngreso,gravado,is_tercero,is_cuenta) VALUES(?,?,?,?,?,?,?,?)";
            $arrData = [
                ucfirst(strtolower(strClean(replaceChar($arrCabecera['nombre'])))),
                $arrCabecera['tipo'],
                'S',
                $arrCabecera['destino'],
                $arrCabecera['tipo_ingreso'],
                $arrCabecera['contabilizar'],
                $arrCabecera['terceros'],
                $arrCabecera['is_presupuesto']
            ];
            $request = $this->insert($sql,$arrData);
            return $request;
        }
        public function updateData($arrCabecera){
            $sql = "UPDATE trans_servicios SET nombre=?,tipo=?,estado=?,terceros=?,is_tercero=?,
            tipoIngreso=?,gravado=?, is_cuenta=?
            WHERE codigo = '$arrCabecera[codigo]'";
            $arrData = [
                $arrCabecera['nombre'],
                $arrCabecera['tipo'],
                'S',
                $arrCabecera['destino'],
                $arrCabecera['terceros'],
                $arrCabecera['tipo_ingreso'],
                $arrCabecera['contabilizar'],
                $arrCabecera['is_presupuesto']
            ];
            $request = intval($this->update($sql,$arrData));
            return $request;
        }
        public function selectPrecio($codigo){
            $sql ="SELECT precio FROM trans_servicios_precios WHERE estado='S' AND ingreso='$codigo'";
            $request = $this->select($sql)['precio'];
            return $request;
        }
        public function selectConcepto($codigo){
            $string = $codigo."-";
            $sql = "SELECT nombre FROM conceptoscontables WHERE modulo='14' AND tipo='TI' AND codigo='$codigo'";
            $string .= $this->select($sql)['nombre'];
            return $string;
        }
        public function selectNombreClasificador($clasificador,$codigo){
            $sql ="";
            if($clasificador == 1){
                $sql  = "SELECT nombre FROM ccpet_cuin WHERE codigo_cuin = '$codigo'";
            }else if($clasificador == 2){
                $sql="SELECT titulo as nombre FROM ccpetbienestransportables WHERE grupo = '$codigo'";

            }else if($clasificador == 3){
                $sql="SELECT titulo as nombre FROM ccpetservicios WHERE grupo = '$codigo'";
            }
            $request = $this->select($sql)['nombre'];
            return $request;
        }
        public function selectDet($codigo,$flag=false){
            $sql="SELECT
            d.codigo,
            d.concepto,
            d.tipoconce,
            d.porcentaje,
            d.cuentapres,
            d.cuenta_clasificadora,
            d.seccion,
            c.nombre,
            d.fuente
            FROM trans_servicios_det d
            LEFT JOIN cuentasingresosccpetseleccionadas c
            ON d.cuentapres = c.codigo COLLATE utf8mb4_general_ci
            WHERE d.codigo = '$codigo'";
            if($flag){
                $request = $this->select_all($sql);
            }else{
                $request = $this->select($sql);
            }
            return $request;
        }
        public function selectFuentesDet($codigo){
            $sql = "SELECT f.fuente as codigo, c.nombre
            FROM trans_servicios_fuentes f
            INNER JOIN ccpet_fuentes_cuipo c
            ON c.codigo_fuente = f.fuente
            WHERE f.codigo = '$codigo' AND estado ='S'";
            $request = $this->select_all($sql);
            return $request;
        }
        public function selectIngreso($codigo){
            $sql = "SELECT codigo,nombre,tipo,terceros,tipoIngreso,gravado,is_tercero,is_cuenta FROM trans_servicios WHERE codigo = '$codigo'";
            $request = $this->select($sql);
            return $request;
        }
        public function selectSeccionesPresupuestales(){
            $sql = "SELECT id_seccion_presupuestal as codigo, nombre FROM pptoseccion_presupuestal WHERE estado='S'";
            $request = $this->select_all($sql);
            return $request;
        }
        public function selectServicios($search){
            $sql = "SELECT grupo as codigo, titulo as nombre
                    FROM ccpetservicios
                    WHERE version = (SELECT MAX(version) FROM ccpetbienestransportables) AND LENGTH(grupo) = 5 AND (grupo LIKE '$search%' OR titulo LIKE '%$search%')
                    ORDER BY grupo ASC"
            ;
            $request['data'] = $this->select_all($sql);
            $request['results'] = $this->select("SELECT count(*) as total
                FROM ccpetservicios
                WHERE version = (SELECT MAX(version) FROM ccpetbienestransportables) AND LENGTH(grupo) = 5 AND (grupo LIKE '$search%' OR titulo LIKE '%$search%')"
                )['total'];
            return $request;

        }
        public function selectBienes($search){
            $sql = "SELECT grupo as codigo, titulo as nombre
                    FROM ccpetbienestransportables
                    WHERE version = (SELECT MAX(version) FROM ccpetbienestransportables) AND LENGTH(grupo) = 7 AND (grupo LIKE '$search%' OR titulo LIKE '%$search%')
                    ORDER BY grupo ASC"
            ;
            $request['data'] = $this->select_all($sql);
            $request['results'] = $this->select("SELECT count(*) as total
                FROM ccpetbienestransportables
                WHERE version = (SELECT MAX(version) FROM ccpetbienestransportables) AND LENGTH(grupo) = 7 AND (grupo LIKE '$search%' OR titulo LIKE '%$search%')"
                )['total'];
            return $request;

        }

        public function selectFuentes($search){
            $sql="SELECT codigo_fuente as codigo, nombre
            FROM ccpet_fuentes_cuipo
            WHERE version = '1' AND
            (codigo_fuente LIKE '$search%' OR nombre LIKE '%$search%')
            ORDER BY codigo_fuente";
            $request['data'] = $this->select_all($sql);
            $request['results'] = $this->select("SELECT count(*) as total
                FROM ccpet_fuentes_cuipo
                WHERE version = '1' AND
                (codigo_fuente LIKE '$search%' OR nombre LIKE '%$search%')"
                )['total'];
            return $request;
        }
        public function selectCuin($search){
            $sql = "SELECT id_entidad,
            nit,
            nombre,
            sector,
            subsector,
            tipo,
            supra_regional,
            nivel_territorial,
            depto,municipio,
            consecutivo,
            codigo_cuin as codigo
            FROM ccpet_cuin
            WHERE version=(SELECT MAX(version) FROM ccpet_cuin) AND
            (nit LIKE '%$search%' OR codigo_cuin LIKE '$search%' OR nombre LIKE '%$search%') ORDER BY id"
            ;
            $request['data'] = $this->select_all($sql);
            $request['results'] = $this->select("SELECT count(*) as total
                FROM ccpet_cuin
                WHERE version=(SELECT MAX(version) FROM ccpet_cuin) AND
                (nit LIKE '%$search%' OR codigo_cuin LIKE '$search%' OR nombre LIKE '%$search%')"
                )['total'];
            return $request;
        }
        public function selectClasificador($cuenta,$flag=false){
            $sql = "SELECT clasificadores FROM ccpetprogramarclasificadores WHERE cuenta = '$cuenta'";
            $request = $this->select($sql)['clasificadores'];
            if($flag){
                return $request;
            }else{
                echo json_encode($request,JSON_UNESCAPED_UNICODE);
                die();
            }
        }
        public function selectConceptos(){
            $sql = "SELECT codigo,nombre,tipo FROM conceptoscontables WHERE modulo='14' AND tipo='TI' ORDER BY codigo";
            $request = $this->select_all($sql);
            return $request;
        }
        public function selectCuentas($search){
            $sql = "SELECT codigo, nombre, tipo, nivel
            FROM cuentasingresosccpetseleccionadas
            WHERE nombre like '$search%' OR codigo like '$search%'";
            $request['data'] = $this->select_all($sql);
            $request['results'] = $this->select("SELECT count(*) as total
                FROM cuentasingresosccpetseleccionadas
                WHERE nombre like '$search%' OR codigo like '$search%'")['total'];
            return $request;
        }
        public function selectSearch(string $intPaginas,string $intPaginaNow,string $strSearch){
            $opcionTercero = [
                '' => 'OTROS',
                'D' => 'DEPARTAMENTAL',
                'M' => 'MUNICIPAL',
                'N' => 'NACIONAL',
            ];
            $this->intPaginas = $intPaginas;
            $this->intPaginaNow = $intPaginaNow;
            $limit ="";
            $this->intInicio = ($this->intPaginaNow-1)*$this->intPaginas;
            if($this->intPaginas != 0){
                $limit = " LIMIT $this->intInicio,$this->intPaginas";
            }
            $sql="SELECT i.codigo, i.tipo,i.nombre,d.concepto,UPPER(i.tipoIngreso) as tipoingreso,i.terceros,d.cuentapres,i.is_tercero,is_cuenta
            FROM trans_servicios i
            INNER JOIN trans_servicios_det d
            ON i.codigo = d.codigo
            WHERE i.estado<> '' AND d.modulo = '14'
            AND (i.nombre LIKE '$strSearch%' OR i.codigo LIKE '$strSearch%')
            GROUP BY i.codigo ORDER BY i.codigo  ASC $limit";

            $sqlTotal = "SELECT count(*) as total
            FROM trans_servicios i
            INNER JOIN trans_servicios_det d
            ON i.codigo = d.codigo
            WHERE i.estado<> '' AND d.modulo = '14'
            AND (i.nombre LIKE '$strSearch%' OR i.codigo LIKE '$strSearch%')
            GROUP BY i.codigo";
            $totalRecords = $this->select($sqlTotal)['total'];
            $totalPages = intval($totalRecords > 0 ? ceil($totalRecords/$this->intPaginas) : 0);
            $totalPages = $totalPages == 0 ? 1 : $totalPages;
            $request = $this->select_all($sql);
            $total =count($request);
            for ($i=0; $i < $total ; $i++) {
                $isTercero = $request[$i]['is_tercero'] == 1 ? "TERCEROS" : "PROPIO";
                $request[$i]['terceros'] = $opcionTercero[$request[$i]['terceros']]."-".$isTercero;
                if($request[$i]['tipo'] == "S"){
                    $request[$i]['estado_cuenta'] = $this->validCuenta($request[$i]['cuentapres']);
                    $request[$i]['concepto'] = $this->selectConcepto($request[$i]['concepto']);
                }else{
                    $request[$i]['concepto'] = "-";
                }
            }

            return array("data"=>$request,"total"=>$totalPages);
        }
        public function validCuenta($cuenta){
            $sql = "SELECT * FROM cuentasingresosccpetseleccionadas WHERE codigo = '$cuenta'";
            $request = $this->select_all($sql);
            $estado = 1;
            if(empty($request)) $estado = 2;
            return $estado;
        }

    }
?>
