<?php
    require_once '../../Librerias/core/Helpers.php';
    require_once '../../Librerias/core/Mysql.php';
    session_start();
    ini_set('display_errors', '1');
    ini_set('display_startup_errors', '1');
    error_reporting(E_ERROR);
    class TarifasModel extends Mysql{
        private $intConsecutivo;
        private $intTipo;
        private $intPorcentaje;
        private $strNombre;
        private $intPaginas;
        private $intPaginaNow;
        private $intInicio;
        private $intVigencia;
        function __construct(){
            parent::__construct();
        }

        public function insertData(int $intTipo,float $intPorcentaje,string $strNombre,array $arrVehiculos, int $intVigencia){
            $this->intTipo = $intTipo;
            $this->intPorcentaje = $intPorcentaje;
            $this->strNombre = $strNombre;
            $this->intVigencia = $intVigencia;
            $strVehiculos = implode(",",$arrVehiculos);
            $return = "";
            $sql = "SELECT * FROM trans_tarifas_cab WHERE nombre = '$strNombre' AND vigencia = $this->intVigencia";
            $request = $this->select_all($sql);
            if(empty($request)){
                $sql = "SELECT * FROM trans_tarifas_tipos WHERE tipo IN ($strVehiculos) AND vigencia = $this->intVigencia";
                $request = $this->select_all($sql);
                if(empty($request)){
                    $sql = "INSERT INTO trans_tarifas_cab (nombre,tipo,porcentaje,estado,vigencia) VALUES(?,?,?,?,?)";
                    $return = $this->insert($sql,[$this->strNombre,$this->intTipo,$this->intPorcentaje,"S",$this->intVigencia]);
                    $this->intConsecutivo = $return;
                    foreach ($arrVehiculos as $det) {
                        $sql = "INSERT INTO trans_tarifas_tipos (tarifa_id,tipo,vigencia) VALUES(?,?,?)";
                        $this->insert($sql,[$this->intConsecutivo,$det,$this->intVigencia]);
                    }
                }else{
                    $return ="existe_tipo";
                }
            }else{
                $return ="existe";
            }
            return $return;
        }
        public function insertDet(int $intConsecutivo,array $arrRangos){
            $this->intConsecutivo = $intConsecutivo;
            $this->delete("DELETE FROM trans_tarifas_det WHERE tarifa_id = $this->intConsecutivo");
            foreach ($arrRangos as $det) {
                $sql = "INSERT INTO trans_tarifas_det (tarifa_id,tipo,porcentaje,desde,hasta) VALUES(?,?,?,?,?)";
                $this->insert($sql,[$this->intConsecutivo,$det['limite'],$det['porcentaje'],$det['desde'],$det['hasta']]);
            }
        }
        public function updateData(int $intConsecutivo,int $intTipo,float $intPorcentaje,string $strNombre,array $arrVehiculos,int $intVigencia){
            $this->intConsecutivo = $intConsecutivo;
            $this->intTipo = $intTipo;
            $this->intPorcentaje = $intPorcentaje;
            $this->strNombre = $strNombre;
            $this->intVigencia = $intVigencia;
            $strVehiculos = implode(",",$arrVehiculos);
            $sql = "SELECT * FROM trans_tarifas_cab WHERE nombre = '$strNombre' AND vigencia = $this->intVigencia AND id != $this->intConsecutivo";
            $request = $this->select($sql);
            $return = "";
            if(empty($request)){
                $sql = "SELECT * FROM trans_tarifas_tipos WHERE tipo IN ($strVehiculos) AND tarifa_id != $this->intConsecutivo AND vigencia = $this->intVigencia";
                $request = $this->select_all($sql);
                if(empty($request)){
                    $this->delete("DELETE FROM trans_tarifas_tipos WHERE tarifa_id = $this->intConsecutivo");
                    $sql = "UPDATE trans_tarifas_cab SET nombre=?,tipo=?,porcentaje=?,vigencia=? WHERE id = $this->intConsecutivo";
                    $return = $this->update($sql,[$this->strNombre,$this->intTipo,$this->intPorcentaje,$this->intVigencia]);
                    foreach ($arrVehiculos as $det) {
                        $sql = "INSERT INTO trans_tarifas_tipos (tarifa_id,tipo,vigencia) VALUES(?,?,?)";
                        $this->insert($sql,[$this->intConsecutivo,$det,$this->intVigencia]);
                    }
                }else{
                    $return ="existe_tipo";
                }
            }else{
                $return = "existe";
            }
            return $return;
        }
        public function selectEdit(int $intConsecutivo){
            $this->intConsecutivo = $intConsecutivo;
            $sql = "SELECT * FROM trans_tarifas_cab WHERE id = $this->intConsecutivo";
            $request = $this->select($sql);
            if(!empty($request)){
                $request['vehiculos'] = $this->select_all("SELECT * FROM trans_tarifas_tipos WHERE tarifa_id = $this->intConsecutivo");
                $request['detalle'] = $this->select_all("SELECT *,tipo as limite FROM trans_tarifas_det WHERE tarifa_id = $this->intConsecutivo");
            }
            return $request;
        }
        public function selectSearch(string $intPaginas,string $intPaginaNow){
            $this->intPaginas = $intPaginas;
            $this->intPaginaNow = $intPaginaNow;
            $limit ="";
            $this->intInicio = ($this->intPaginaNow-1)*$this->intPaginas;
            if($this->intPaginas != 0){
                $limit = " LIMIT $this->intInicio,$this->intPaginas";
            }
            $sql = "SELECT * FROM trans_tarifas_cab ORDER BY id DESC $limit";
            $sqlTotal = "SELECT count(*) as total FROM trans_tarifas_cab  ORDER BY id DESC";
            $totalRecords = $this->select($sqlTotal)['total'];
            $totalPages = intval($totalRecords > 0 ? ceil($totalRecords/$this->intPaginas) : 0);
            $totalPages = $totalPages == 0 ? 1 : $totalPages;
            $request = $this->select_all($sql);
            $total =count($request);
            for ($i=0; $i < $total; $i++) {
                $this->intConsecutivo = $request[$i]['id'];
                $request[$i]['is_status'] = $request[$i]['estado'] =="S"? 1 : 0;
                $sqlDet = "SELECT concat(ti.id,'-',ti.nombre) as nombre
                FROM trans_tarifas_tipos det
                INNER JOIN trans_caracterizacion_tipo ti ON ti.id = det.tipo
                WHERE det.tarifa_id = $this->intConsecutivo";
                $det = $this->select_all($sqlDet);
                $arrTempDet = [];
                foreach ($det as $d) {array_push($arrTempDet,$d['nombre']);}
                $request[$i]['nombre_tipo'] = $request[$i]['tipo'] == 1 ? "Porcentaje" : "Rango y porcentaje";
                $request[$i]['vehiculos'] = implode(",",$arrTempDet);

            }
            return array("data"=>$request,"total"=>$totalPages);
        }
        public function selectTipos(){
            $sql = "SELECT * FROM trans_caracterizacion_tipo ORDER BY nombre ASC";
            $request = $this->select_all($sql);
            return $request;
        }
    }
?>
