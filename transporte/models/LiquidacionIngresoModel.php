<?php
    require_once '../../Librerias/core/Helpers.php';
    require_once '../../Librerias/core/Mysql.php';
    session_start();
    ini_set('display_errors', '1');
    ini_set('display_startup_errors', '1');
    error_reporting(E_ERROR);
    class LiquidacionIngresoModel extends Mysql{
        private $intConsecutivo;
        private $intIdComp;
        private $strFecha;
        private $strFechaFinal;
        private $strTercero;
        private $strConcepto;
        private $floatTotal;
        private $intCausacion;
        function __construct(){
            parent::__construct();
        }
        public function selectSeccionesPresupuestales(){
            $sql = "SELECT id_seccion_presupuestal as codigo, nombre FROM pptoseccion_presupuestal WHERE estado='S'";
            $request = $this->select_all($sql);
            return $request;
        }
        public function selectTerceros(){
            $sql="SELECT cedulanit as codigo,
            CASE WHEN razonsocial IS NULL OR razonsocial = ''
            THEN CONCAT(nombre1,' ',nombre2,' ',apellido1,' ',apellido2)
            ELSE razonsocial END AS nombre FROM terceros ORDER BY id_tercero";
            $request = $this->select_all($sql);
            return $request;
        }
        public function selectCentroCostos(){
            $sql = "SELECT id_cc as codigo, nombre FROM centrocosto WHERE estado='S'";
            $request = $this->select_all($sql);
            return $request;
        }
        public function selectLiquidaciones(){
            $sql = "SELECT *,id_recaudo as codigo, valortotal as valor FROM trans_sin_recaudos_cab WHERE estado = 'S' ORDER BY id_recaudo DESC";
            $request = $this->select_all($sql);
            return $request;
        }
        public function selectIngresos(){
            $sql = "SELECT * FROM trans_tramites_runt WHERE estado='S'";
            $request = $this->select_all($sql);
            /* if(!empty($request)){
                $total = count($request);
                for ($i=0; $i < $total ; $i++) {
                    $isStatus = false;
                    $codigo = $request[$i]['codigo'];
                    $arrFuentes = [];


                    
                    $sqlTotalCuenta = "SELECT COALESCE(COUNT(cuentapres),0) as total FROM trans_servicios_transito_det WHERE codigo = '$codigo'";

                    $sqlTotalFuente = "SELECT COALESCE(COUNT(*),0) as total FROM trans_servicios_transito_det WHERE codigo = '$codigo' AND estado = 'S'";

                    $sqlFuentes = "SELECT f.codigo_fuente as codigo, f.nombre,tf.porcentaje,
                    tf.seccion,
                    tf.concepto,
                    tf.cuentapres,
                    cuenta_clasificadora
                    FROM trans_servicios_transito_det tf
                    INNER JOIN ccpet_fuentes_cuipo f ON tf.fuente = f.codigo_fuente
                    WHERE codigo = '$codigo' AND estado = 'S' ";
                    

                    $totalCuentas = $this->select($sqlTotalCuenta)['total'];
                    $totalFuentes = $this->select($sqlTotalFuente)['total'];

                    $arrFuentes = $this->select_all($sqlFuentes);
                    if($request[$i]['tipo']=="S"){
                        $sqlCuenta = "SELECT cuentapres,cuenta_clasificadora,porcentaje,seccion,concepto FROM trans_servicios_transito_det WHERE codigo = '$codigo'";
                        $cuenta = $this->select($sqlCuenta);
                        $arrFuentes[0]['cuentapres'] = $cuenta['cuentapres'];
                        $arrFuentes[0]['porcentaje'] = $cuenta['porcentaje'];
                        $arrFuentes[0]['seccion'] = $cuenta['seccion'];
                        $arrFuentes[0]['concepto'] = $cuenta['concepto'];
                        $arrFuentes[0]['cuenta_clasificadora'] = $cuenta['cuenta_clasificadora'];
                    }
                    if($totalCuentas > 0 && $totalFuentes > 0){
                        $isStatus = true;

                    }
                    
                    $request[$i]['is_status'] = $isStatus;
                    $request[$i]['fuentes'] = $arrFuentes;
                }
            } */
            return $request;
        }

        public function searchIngreso($codigo){
            $sql = "SELECT cab.*, det.concepto FROM trans_tramites_runt_cab as cab, trans_tramites_runt_det as det  WHERE cab.codigo = '$codigo' AND cab.estado = 'S' AND cab.numeral = det.numeral group by cab.numeral";
            $request = $this->select_all($sql);
            /* if(!empty($request)){
                
                $total = count($request);
                $arrFuentes = [];
                for ($i=0; $i < $total ; $i++) {

                    $codigoNum = $request[$i]['numeral'];
                    $sqlConcepto = "SELECT concepto FROM trans_tramites_runt_det WHERE numeral = '$codigoNum' limit 1";
                    $request['concepto'] = $this->select($sqlConcepto)['concepto'];

                    $isStatus = false;
                    
                    
                    $sqlFuentes = "SELECT f.codigo_fuente as codigo, f.nombre,tf.porcentaje,
                    tf.seccion,
                    tf.concepto,
                    tf.cuentapres,
                    tf.cuenta_clasificadora,
                    tf.numeral
                    FROM trans_tramites_runt_det tf
                    INNER JOIN ccpet_fuentes_cuipo f ON tf.fuente = f.codigo_fuente
                    WHERE numeral = '$codigo' AND estado = 'S' ";
                    $arrFuentes[$i] = $this->select_all($sqlFuentes);

                }
                $request['fuentes'] = $arrFuentes;
            } */
            
            return $request;
        }
        public function insertComprobanteCab(int $intConsecutivo,string $strFecha,string $strConcepto,float $floatTotal,$comp =86,$estado = 1){
            $this->intConsecutivo = $intConsecutivo;
            $this->strFecha = $strFecha;
            $this->strConcepto = $strConcepto;
            $this->floatTotal = $floatTotal;
            $this->intIdComp = $comp;
            $sql = "INSERT INTO comprobante_cab(numerotipo,tipo_comp,fecha,concepto,total,total_debito,total_credito,diferencia,estado)
            VALUES(?,?,?,?,?,?,?,?,?)";
            $arrData = [$this->intConsecutivo,$this->intIdComp,$this->strFecha,$this->strConcepto,0,$this->floatTotal,$this->floatTotal,0,$estado];
            $request = $this->insert($sql,$arrData);
            return $request;
        }
        public function insertLiquidacionCab(int $intConsecutivo,string $strFecha,float $floatTotal,string $strConcepto,string $strTercero, string $strMedioPago){
            //insertAuditoria("trans_auditoria","trans_funciones_id",40,"Crear",$intConsecutivo,"Liquidacion ingresos internos","trans_funciones");
            $this->intConsecutivo = $intConsecutivo;
            $this->strFecha = $strFecha;
            $this->strConcepto = $strConcepto;
            $this->floatTotal = $floatTotal;
            $this->strTercero = $strTercero;
            $strYear = explode("-",$strFecha)[0];
            $sql="INSERT INTO trans_sin_recaudos_cab (id_comp,fecha,vigencia,tercero,valortotal,concepto,estado,medio_pago)
            VALUES(?,?,?,?,?,?,?,?)";
            $arrData = [$this->intConsecutivo,$this->strFecha,$strYear,$this->strTercero,$this->floatTotal,$this->strConcepto,"S",$strMedioPago];
            $request = $this->insert($sql,$arrData);
            return $request;
        }
        public function insertLiquidacionDet(int $intConsecutivo,array $arrData,int $intCausacion,int $intConsecLiquid,string $strTercero){
            $this->intConsecutivo = $intConsecutivo;
            $this->intCausacion = $intCausacion;
            $this->strTercero = $strTercero;
            $vigencia = date("Y");
            $request = 0;
            foreach ($arrData as $data) {
                //$centroCosto = getCentroCosto($data['centro']['codigo'])['id_cc'];
                $centroCosto = '01';
                $ingreso = $data['numeral'];
                if($intCausacion == 1){
                    
                    $idConcepto = $data['concepto'];
                    $arrConcepto = getConceptoDet($idConcepto,14,"TI",$centroCosto);
                    foreach($arrConcepto as $concepto){
                        $debito = $concepto['debito'] == "S" ?$data['valor'] : 0 ;
                        $credito = $concepto['debito'] != "S" ?$data['valor'] : 0 ;
                        $sql="INSERT INTO comprobante_det (id_comp,cuenta,tercero,centrocosto,detalle,valdebito,valcredito,estado,vigencia)
                        VALUES (?,?,?,?,?,?,?,?,?)";
                        $arrValuesDet = [
                            '86 '.$intConsecLiquid,
                            $concepto['cuenta'],
                            $this->strTercero,
                            $centroCosto,
                            'Causacion '.$data['nombre'],
                            $debito,
                            $credito,
                            1,
                            $vigencia
                        ];
                        $this->insert($sql,$arrValuesDet);
                    }
                }
                $sql="INSERT INTO trans_sin_recaudos_det (id_recaudo,ingreso,valor,estado,cc,fuente,seccion,cuentapres,cuenta_clasificadora)
                VALUES(?,?,?,?,?,?,?,?,?)";
                $arrValues = [
                    $this->intConsecutivo,
                    $ingreso,
                    $data['valor'],'S',
                    $centroCosto,'',
                    '',
                    '',
                    ''
                ];
                $request = $this->insert($sql,$arrValues);
            }
            return $request;
        }
        public function insertReversar(int $intConsecutivo,string $strFecha,string $strConcepto,array $data){
            $this->intConsecutivo = $intConsecutivo;
            $this->strFecha = $strFecha;
            $this->strConcepto = $strConcepto;
            $strYear = explode("-",$strFecha)[0];
            $sqlValid  = "SELECT * FROM trans_sin_recaudos_cab WHERE tipo_mov = 101 AND estado = 'R' AND id_recaudo='{$this->intConsecutivo}'";
            $requestValid = $this->select($sqlValid);
            if(empty($requestValid)){
                $this->intIdComp = $this->insertComprobanteCab(
                    $this->intConsecutivo,
                    $this->strFecha,
                    $this->strConcepto,
                    $data['valortotal'],
                    2086
                );
                if($this->intIdComp > 0){
                    $request = $this->update("UPDATE trans_sin_recaudos_cab SET estado=? WHERE id_recaudo='{$this->intConsecutivo}'",['R']);
                    if($request == 0){
                        $return = "recaudo";
                        return $return;
                    }
                    $sql="INSERT INTO trans_sin_recaudos_cab (id_recaudo,id_comp,fecha,vigencia,tercero,valortotal,concepto,estado,tipo_mov)
                    VALUES(?,?,?,?,?,?,?,?,?)";
                    $arrData = [$this->intConsecutivo,$this->intIdComp,$this->strFecha,$strYear,$data['tercero'],$data['valortotal'],$this->strConcepto,"R",301];
                    $request = $this->insert($sql,$arrData);
                    if($request == 0){
                        $return = "recaudo_cab";
                        return $return;
                    }else{
                        insertAuditoria("trans_auditoria","trans_funciones_id",40,"Reversar",$this->intConsecutivo,"Liquidacion ingresos internos","trans_funciones");
                        $arrDetComp = $data['comprobante']['det'];
                        if(count($arrDetComp) > 0){
                            foreach ($arrDetComp as $det) {
                                $debito = $det['valcredito'];
                                $credito = $det['valdebito'];
                                $numeroTipo = $det['numerotipo'];
                                $tipoComp = "20".$det['tipo_comp'];
                                $idComp = $tipoComp." ".$numeroTipo;
                                $detalle = "Reversion ".$det['detalle'];
                                $sqlInsertDet = "INSERT INTO comprobante_det(id_comp,cuenta,tercero,centrocosto,detalle,valdebito,valcredito,estado,vigencia,tipo_comp,numerotipo)
                                VALUES(?,?,?,?,?,?,?,?,?,?,?)";
                                $arrValues = [
                                    $idComp,
                                    $det['cuenta'],
                                    $det['tercero'],
                                    $det['centrocosto'],
                                    $detalle,
                                    $debito,
                                    $credito,
                                    1,
                                    $strYear,
                                    $tipoComp,
                                    $numeroTipo
                                ];
                                $requestInsertDet = $this->insert($sqlInsertDet,$arrValues);
                                if($requestInsertDet == 0){
                                    $return = "comp_det";
                                    return $return;
                                }
                            }
                        }
                        $return = $request;
                    }
                }else{
                    $return = "comp";
                }
            }else{
                $return = "existe";
            }
            return $return;
        }
        public function selectRecaudo(int $intConsecutivo){
            $this->intConsecutivo = $intConsecutivo;
            $sql = "SELECT * FROM trans_sin_recaudos_cab WHERE id_recaudo = $this->intConsecutivo";
            $request = $this->select($sql);
            if(!empty($request)){
                $sqlCompCab = "SELECT * FROM comprobante_cab WHERE id_comp = {$request['id_comp']}";
                $arrComp = $this->select($sqlCompCab);
                $sqlCompDet = "SELECT * FROM comprobante_det WHERE numerotipo = {$arrComp['numerotipo']} AND tipo_comp = {$arrComp['tipo_comp']}";
                $arrCompDet = $this->select_all($sqlCompDet);
                $request['comprobante']['cab'] = $arrComp;
                $request['comprobante']['det'] = $arrCompDet;
            }
            return $request;
        }
        public function selectSearch(string $strFecha, string $strFechaFinal,string $search){
            $this->strFecha = $strFecha;
            $this->strFechaFinal = $strFechaFinal;
            $sql = "SELECT *,DATE_FORMAT(fecha,'%d/%m/%Y') as fecha FROM trans_sin_recaudos_cab
            WHERE fecha BETWEEN '$this->strFecha' AND '$this->strFechaFinal' AND tipo_mov = '101'
            AND (tercero like '$search%' OR concepto like '$search%' OR id_recaudo like '$search%')
            ORDER BY id_recaudo DESC";
            $request = $this->select_all($sql);
            if(!empty($request)){
                $total = count($request);
                for ($i=0; $i < $total; $i++) {
                    $request[$i]['nombre'] = getNombreTercero($request[$i]['tercero']);
                    $request[$i]['total_format'] = formatNum($request[$i]['valortotal']);
                }
            }
            return $request;
        }
        public function selectEdit(int $intConsecutivo){
            $this->intConsecutivo = $intConsecutivo;
            $sql = "SELECT * FROM trans_sin_recaudos_cab WHERE id_recaudo = $this->intConsecutivo";
            $request = $this->select_all($sql);
            if(!empty($request)){
                $total = count($request);
                for ($i=0; $i < $total; $i++) {
                    $sqlDet = "SELECT
                    det.ingreso as codigo_ingreso,
                    det.valor,
                    det.estado,
                    det.seccion as codigo_cc,
                    TT.nombre
                    FROM trans_sin_recaudos_det det
                    LEFT JOIN trans_tramites_runt_cab TT ON TT.numeral = det.ingreso
                    WHERE id_recaudo = $this->intConsecutivo";

                    if($request[$i]['estado'] == "P"){
                        $sql = "SELECT id_recibos FROM trans_sin_recibos_cab WHERE estado = 'S' AND id_recaudo = $this->intConsecutivo";
                        $request[$i]['ingreso_interno'] = $this->select($sql)['id_recibos'];
                    }
                    $request[$i]['det'] = $this->select_all($sqlDet);
                    $request[$i]['nombre_tercero'] = getNombreTercero($request[$i]['tercero']);

                }
            }
            return $request;
        }
    }
?>
