<?php
    require_once '../../Librerias/core/Helpers.php';
    require_once '../../Librerias/core/Mysql.php';
    session_start();
    ini_set('display_errors', '1');
    ini_set('display_startup_errors', '1');
    error_reporting(E_ERROR);
    class DeclaracionImpuestoModel extends Mysql{
        private $intConsecutivo;
        private $strFecha;
        private $intTipo;
        private $floPorcentaje;
        private $intPaginas;
        private $intPaginaNow;
        private $intInicio;
        private $strPlaca;
        function __construct(){
            parent::__construct();
        }

        public function insertData(string $strFecha,array $arrAuto,array $arrTotales){
            $this->strFecha = $strFecha;
            $intTotalMunicipio = $arrTotales['total']*0.2;
            $intTotalDepartamento = $arrTotales['total']*0.8;
            $sql = "INSERT INTO trans_impuesto_cab (fecha,id_placa,placa,propietario,total_avaluo,total_impuesto,total_descuento,total_sancion,total_interes_mora,total_pago,total_municipio,total_departamento)
            VALUES(?,?,?,?,?,?,?,?,?,?,?,?)";
            $arrData = [
                $strFecha,
                $arrAuto['id'],
                $arrAuto['placa'],
                $arrAuto['propietario'],
                $arrTotales['avaluo'],
                $arrTotales['impuesto'],
                $arrTotales['impuesto_descuento'],
                $arrTotales['sancion'],
                $arrTotales['interes_mora'],
                $arrTotales['total'],
                $intTotalMunicipio,
                $intTotalDepartamento
            ];
            $request = $this->insert($sql,$arrData);
            return $request;
        }
        public function insertDet(int $intConsecutivo,array $arrImpuestos ){
            $this->intConsecutivo = $intConsecutivo;
            $request = 0;
            foreach ($arrImpuestos as $data) {
                $sql = "INSERT INTO trans_impuesto_det (impuesto_id,vigencia,avaluo,tarifa,sancion,impuesto,impuesto_descuento,semaforizacion,interes_mora,dias_mora,total)
                VALUES (?,?,?,?,?,?,?,?,?,?,?)";
                $arrData = [
                    $this->intConsecutivo,
                    $data['vigencia'],
                    $data['avaluo'],
                    $data['tarifa'],
                    $data['sancion'],
                    $data['impuesto'],
                    $data['impuesto_descuento'],
                    $data['semaforizacion'],
                    $data['interes_mora'],
                    $data['dias_mora'],
                    $data['total'],
                ];
                $request = $this->insert($sql,$arrData);
                if($request == 0){
                    break;
                }
            }
            return $request;
        }
        public function updateData(int $intConsecutivo,string $strFecha,int $intTipo,float $floPorcentaje){
            $this->intConsecutivo = $intConsecutivo;
            $this->strFecha = $strFecha;
            $this->intTipo = $intTipo;
            $this->floPorcentaje = $floPorcentaje;
            $sql = "UPDATE trans_descuentos SET tipo=?,fecha=?,porcentaje=? WHERE id = $this->intConsecutivo";
            $arrData = [$this->intTipo,$this->strFecha,$this->floPorcentaje];
            $request = $this->update($sql,$arrData);
            return $request;
        }
        public function selectEdit($intConsecutivo){
            $this->intConsecutivo = $intConsecutivo;
            $sql = "SELECT *,date_format(fecha,'%d/%m/%Y') as fecha_format FROM trans_impuesto_cab WHERE id = $this->intConsecutivo";
            $request = $this->select($sql);
            if(!empty($request)){
                $sqlDet = "SELECT * FROM trans_impuesto_det WHERE impuesto_id = $this->intConsecutivo ORDER BY vigencia DESC";
                $request['detalle'] = $this->select_all($sqlDet);
                $sqlAutomotor = "SELECT au.placa,
                au.fecha_tramite as fecha,
                date_format(au.fecha_tramite,'%d/%m/%Y') as fecha_format,
                au.id,
                au.clase_id,
                au.marca_id,
                au.linea_id,
                au.cilindraje_id,
                au.propietario,
                cl.nombre as clase,
                cl.tipo_id,
                ma.nombre as marca,
                li.nombre as linea,
                au.color,
                au.modelo,
                ci.nombre as cilindraje,
                au.capacidad_carga as carga,
                au.cantidad_pasajeros as pasajeros,
                co.nombre as combustible,
                au.potencia_hp as potencia,
                pu.nombre as publico,
                ca.nombre as carroceria,
                au.vehiculo_antiguo as antiguo,
                au.blindaje,
                se.nombre as servicio,
                au.dpto,
                au.mnpio
                FROM trans_automotores au
                LEFT JOIN trans_caracterizacion_clase cl ON cl.id = au.clase_id
                LEFT JOIN trans_caracterizacion_marca ma ON ma.id = au.marca_id
                LEFT JOIN trans_caracterizacion_linea li ON li.id = au.linea_id
                LEFT JOIN trans_caracterizacion_combustible co ON co.id = au.combustible_id
                LEFT JOIN trans_caracterizacion_cilindraje ci ON ci.id = au.cilindraje_id
                LEFT JOIN trans_caracterizacion_publico pu ON pu.id = au.publico_id
                LEFT JOIN trans_caracterizacion_carroceria ca ON ca.id = au.carroceria_id
                LEFT JOIN trans_caracterizacion_servicio se ON se.id = au.tipo_servicio_id
                WHERE au.placa = '{$request['placa']}'";
                $arrAutomotor = $this->select($sqlAutomotor);
                $arrTerceros = getTerceros();
                $tercero = array_values(array_filter($arrTerceros,function($e)use($request){return $e['cedulanit'] == $request['propietario'];}))[0];
                $arrAutomotor['departamento'] = $this->select("SELECT nombredpto as nombre FROM danedpto WHERE danedpto = '{$arrAutomotor['dpto']}'")['nombre'];
                $arrAutomotor['municipio'] = $this->select("SELECT nom_mnpio as nombre FROM danemnpio WHERE danemnpio = '{$arrAutomotor['mnpio']}' and danedpto = '{$arrAutomotor['dpto']}'")['nombre'];
                $arrAutomotor['departamento_tercero'] = $this->select("SELECT nombredpto as nombre FROM danedpto WHERE danedpto = '{$tercero['depto']}'")['nombre'];
                $arrAutomotor['municipio_tercero'] = $this->select("SELECT nom_mnpio as nombre FROM danemnpio WHERE danemnpio = '{$tercero['mnpio']}' and danedpto = '{$tercero['depto']}'")['nombre'];
                $arrAutomotor['nombre_tercero'] = $tercero['nombre'];
                $arrAutomotor['celular'] = $tercero['celular'];
                $request['automotor'] = $arrAutomotor;
            }
            return $request;
        }
        public function selectPlaca(string $strPlaca){
            $this->strPlaca = $strPlaca;
            $sql = "SELECT * FROM trans_automotores WHERE placa = '$this->strPlaca'";
            $request = $this->select($sql);
            $fecha = substr($request['fecha_tramite'],0,4);
            if($fecha == "0000"){
                $request['fecha_tramite'] ="";
            }
            return $request;
        }
        public function selectAvaluo($intClase,$intMarca,$intLinea,$intModelo,$intCilindraje,$intVigencia,$intAvaluo){
            $sql = "SELECT coalesce(valor,0) as valor FROM trans_base_gravable
            WHERE clase_id = $intClase AND marca_id = $intMarca AND linea_id = $intLinea
            AND modelo = $intModelo  AND vigencia = $intVigencia";
            $request = $this->select($sql);
            if(empty($request)){
                $intVigencia +=1;
                $sql = "SELECT coalesce(valor,0) as valor FROM trans_base_gravable
                WHERE clase_id = $intClase AND marca_id = $intMarca AND linea_id = $intLinea
                AND modelo = $intModelo  AND vigencia = $intVigencia";
                $request = $this->select($sql);
                if(empty($request)){
                    $request['valor'] = $intAvaluo/1000;
                }
            }
            return $request['valor']*1000;
        }
        public function selectTarifa($intTipo,$intAvaluo){
            $sql = "SELECT tarifa_id FROM trans_tarifas_tipos WHERE tipo = $intTipo";
            $request = $this->select($sql);
            $intPorcentaje =0;
            if(!empty($request)){
                $sql = "SELECT * FROM trans_tarifas_cab WHERE id = {$request['tarifa_id']}";
                $request = $this->select($sql);
                if($request['tipo'] == "2"){
                    $sql = "SELECT * FROM trans_tarifas_det WHERE tarifa_id = {$request['id']}";
                    $arrRangos = $this->select_all($sql);
                    foreach ($arrRangos as $rango) {
                        if($intAvaluo >= $rango['desde'] && $intAvaluo <=$rango['hasta'] && $rango['tipo'] == 1){
                            $intPorcentaje = $rango['porcentaje'];
                            break;
                        }else if($intAvaluo >=$rango['desde'] && $rango['tipo'] == 2){
                            $intPorcentaje = $rango['porcentaje'];
                            break;
                        }
                    }
                }else{
                    $intPorcentaje = $request['porcentaje'];
                }
            }
            return $intPorcentaje;
        }
        public function selectDescuento($strFecha){
            $arrFecha = explode("-",$strFecha);
            $sql="SELECT * FROM trans_descuentos WHERE estado = 'S' AND fecha >= '$strFecha' AND YEAR(fecha) = $arrFecha[0]";
            $request = $this->select($sql);
            $valor = 0;
            if(!empty($request)){
                $valor = $request['porcentaje']/100;
            }
            return $valor;
        }

        public function updateStatus(int $intConsecutivo){
            $this->intConsecutivo = $intConsecutivo;
            $sql = "UPDATE trans_impuesto_cab SET estado = ? WHERE id = $this->intConsecutivo";
            $request = $this->update($sql,["N"]);
            return $request;
        }
        public function selectVigencias(){
            $sql = "SELECT vigencia as nombre FROM trans_base_gravable GROUP BY vigencia ORDER BY vigencia DESC";
            $request = $this->select_all($sql);
            return $request;
        }
        public function selectSancion($intVigencia,$intImpuesto){
            $sql = "SELECT salario FROM admfiscales WHERE vigencia = $intVigencia";
            $request = $this->select($sql)['salario'];
            $sancion = round($request*8);
            if($intImpuesto < $request){
                $sancion = $intImpuesto;
            }
            return $sancion;
        }
        public function selectInteres($intMesActual,$intVigencia,$intDiasMora,$intImpuesto){
            $sql = "SELECT moratorio FROM interes WHERE mes = $intMesActual AND vigencia = $intVigencia";
            $request = $this->select($sql);
            $interes = 0;
            if(!empty($request)){
                $interesDiario = ($request['moratorio']/365)/100;
                $interes = $interesDiario*$intImpuesto;
                $interes = ceil($interes*$intDiasMora);
            }
            return $interes;
        }
        public function selectCartera($placa){
            $sql = "SELECT periodo FROM trans_cartera WHERE placa ='$placa'";
            $request = $this->select_all($sql);
            if(!empty($request)){
                $sqlLiquidado = "SELECT det.vigencia FROM trans_impuesto_det det
                INNER JOIN trans_impuesto_cab cab ON det.impuesto_id = cab.id  WHERE cab.placa ='$placa' AND det.estado = 'P'";
                $arrLiquidado = $this->select_all($sqlLiquidado);
                $request = array_values(array_column($request,"periodo"));
                foreach ($arrLiquidado as $data) {
                    array_push($request,$data['vigencia']);
                }
            }else{
                $sql = "SELECT det.vigencia FROM trans_impuesto_det det
                INNER JOIN trans_impuesto_cab cab ON det.impuesto_id = cab.id  WHERE cab.placa ='$placa' AND det.estado = 'P'";
                $request = array_values(array_column($request,"vigencia"));
            }
            return $request;
        }
        /**************************************TIPOS DE BUSQUEDA******************************************/
        public function selectPlacas(string $intPaginas,string $intPaginaNow,string $strBuscar){
            $this->intPaginas = $intPaginas;
            $this->intPaginaNow = $intPaginaNow;
            $limit ="";

            $this->intInicio = ($this->intPaginaNow-1)*$this->intPaginas;
            if($this->intPaginas != 0){
                $limit = " LIMIT $this->intInicio,$this->intPaginas";
            }

                $sql = "SELECT au.placa,
                au.fecha_tramite as fecha,
                date_format(au.fecha_tramite,'%d/%m/%Y') as fecha_format,
                au.id,
                au.clase_id,
                au.marca_id,
                au.linea_id,
                au.cilindraje_id,
                au.propietario,
                cl.nombre as clase,
                cl.tipo_id,
                ma.nombre as marca,
                li.nombre as linea,
                au.color,
                au.modelo,
                ci.nombre as cilindraje,
                au.capacidad_carga as carga,
                au.cantidad_pasajeros as pasajeros,
                co.nombre as combustible,
                au.potencia_hp as potencia,
                pu.nombre as publico,
                ca.nombre as carroceria,
                au.vehiculo_antiguo as antiguo,
                au.blindaje,
                se.nombre as servicio,
                au.dpto,
                au.mnpio
                FROM trans_automotores au
                LEFT JOIN trans_caracterizacion_clase cl ON cl.id = au.clase_id
                LEFT JOIN trans_caracterizacion_marca ma ON ma.id = au.marca_id
                LEFT JOIN trans_caracterizacion_linea li ON li.id = au.linea_id
                LEFT JOIN trans_caracterizacion_combustible co ON co.id = au.combustible_id
                LEFT JOIN trans_caracterizacion_cilindraje ci ON ci.id = au.cilindraje_id
                LEFT JOIN trans_caracterizacion_publico pu ON pu.id = au.publico_id
                LEFT JOIN trans_caracterizacion_carroceria ca ON ca.id = au.carroceria_id
                LEFT JOIN trans_caracterizacion_servicio se ON se.id = au.tipo_servicio_id
                WHERE au.placa LIKE '$strBuscar%' OR au.propietario LIKE '$strBuscar%' AND au.estado = 'S'
                ORDER BY au.id $limit";

                $sqlTotal = "SELECT count(*) as total
                FROM trans_automotores au
                LEFT JOIN trans_caracterizacion_clase cl ON cl.id = au.clase_id
                LEFT JOIN trans_caracterizacion_marca ma ON ma.id = au.marca_id
                LEFT JOIN trans_caracterizacion_linea li ON li.id = au.linea_id
                LEFT JOIN trans_caracterizacion_combustible co ON co.id = au.combustible_id
                LEFT JOIN trans_caracterizacion_cilindraje ci ON ci.id = au.cilindraje_id
                LEFT JOIN trans_caracterizacion_publico pu ON pu.id = au.publico_id
                LEFT JOIN trans_caracterizacion_carroceria ca ON ca.id = au.carroceria_id
                LEFT JOIN trans_caracterizacion_servicio se ON se.id = au.tipo_servicio_id
                WHERE au.placa LIKE '$strBuscar%' OR au.propietario LIKE '$strBuscar%' AND au.estado = 'S'
                ORDER BY au.id";


            $totalRecords = $this->select($sqlTotal)['total'];
            $totalPages = intval($totalRecords > 0 ? ceil($totalRecords/$this->intPaginas) : 0);
            $totalPages = $totalPages == 0 ? 1 : $totalPages;
            $request = $this->select_all($sql);
            $total = count($request);
            $arrTerceros = getTerceros();
            for ($i=0; $i < $total ; $i++) {
                $data = $request[$i];
                $fecha = substr($data['fecha'],0,4);
                if($fecha == "0000"){
                    $request[$i]['fecha'] ="";
                }
                $tercero = array_values(array_filter($arrTerceros,function($e)use($data){return $e['cedulanit'] == $data['propietario'];}))[0];
                $request[$i]['departamento'] = $this->select("SELECT nombredpto as nombre FROM danedpto WHERE danedpto = '{$data['dpto']}'")['nombre'];
                $request[$i]['municipio'] = $this->select("SELECT nom_mnpio as nombre FROM danemnpio WHERE danemnpio = '{$data['mnpio']}' AND danedpto = '{$data['dpto']}'")['nombre'];
                $request[$i]['departamento_tercero'] = $this->select("SELECT nombredpto as nombre FROM danedpto WHERE danedpto = '{$tercero['depto']}'")['nombre'];
                $request[$i]['municipio_tercero'] = $this->select("SELECT nom_mnpio as nombre FROM danemnpio WHERE danemnpio = '{$tercero['mnpio']}' AND danedpto = '{$tercero['depto']}'")['nombre'];
                $request[$i]['nombre_tercero'] = $tercero['nombre'];
                $request[$i]['celular'] = $tercero['celular'];
            }
            return array("data"=>$request,"total"=>$totalPages);
        }
        public function selectSearch(string $intPaginas,string $intPaginaNow,string $strFechaInicial,string $strFechaFinal,string $strCodigo,string $strPlaca,string $strDocumento){
            $this->intPaginas = $intPaginas;
            $this->intPaginaNow = $intPaginaNow;
            $limit ="";

            $this->intInicio = ($this->intPaginaNow-1)*$this->intPaginas;
            if($this->intPaginas != 0){
                $limit = " LIMIT $this->intInicio,$this->intPaginas";
            }
            $sql = "SELECT cab.id,
            DATE_FORMAT(cab.fecha,'%d/%m/%Y') as fecha,
            cab.placa,
            cab.total_pago as total,
            cab.propietario,
            cab.estado,
            CASE WHEN t.razonsocial IS NULL OR t.razonsocial = ''
            THEN CONCAT(t.nombre1,' ',t.nombre2,' ',t.apellido1,' ',t.apellido2)
            ELSE t.razonsocial END AS nombre
            FROM trans_impuesto_cab cab
            LEFT JOIN terceros t ON cab.propietario = t.cedulanit
            WHERE cab.fecha BETWEEN '$strFechaInicial' AND '$strFechaFinal'
            AND cab.id LIKE '$strCodigo%' AND cab.placa LIKE '$strPlaca%' AND cab.propietario LIKE '$strDocumento%'
            ORDER BY cab.id DESC $limit";

            $sqlTotal = "SELECT count(*) as total
                FROM trans_impuesto_cab cab
                LEFT JOIN terceros t ON cab.propietario = t.cedulanit
                WHERE cab.fecha BETWEEN '$strFechaInicial' AND '$strFechaFinal'
                AND cab.id LIKE '$strCodigo%' AND cab.placa LIKE '$strPlaca%' AND cab.propietario LIKE '$strDocumento%'
                ORDER BY cab.id DESC
            ";
            $totalRecords = $this->select($sqlTotal)['total'];
            $totalPages = intval($totalRecords > 0 ? ceil($totalRecords/$this->intPaginas) : 0);
            $totalPages = $totalPages == 0 ? 1 : $totalPages;
            $request = $this->select_all($sql);
            return array("data"=>$request,"total"=>$totalPages);
        }
    }
?>
