<?php
    require_once '../../Librerias/core/Helpers.php';
    require_once '../../Librerias/core/Mysql.php';
    session_start();
    ini_set('display_errors', '1');
    ini_set('display_startup_errors', '1');
    error_reporting(E_ERROR);
    class BaseGravableModel extends Mysql{

        private $arrTablas;
        private $intPaginas;
        private $intPaginaNow;
        private $strTipo;
        private $strClase;
        private $strMarca;
        private $strLinea;
        private $strModelo;
        private $strBuscar;
        private $intInicio;
        private $intVigencia;
        function __construct(){
            parent::__construct();
            $this->arrTablas = [
                ['nombre'=>"","tabla"=>"trans_caracterizacion_tipo"],
                ['nombre'=>"","tabla"=>"trans_caracterizacion_clase","tipo"=>""],
                ["nombre"=>"","tabla"=>"trans_caracterizacion_cilindraje"],
                ["nombre"=>"","tabla"=>"trans_caracterizacion_ocupantes"],
                ["nombre"=>"","tabla"=>"trans_caracterizacion_capacidad"],
                ["nombre"=>"","tabla"=>"trans_caracterizacion_marca","clase"=>"","tipo_marca"=>""],
                ["nombre"=>"","tabla"=>"trans_caracterizacion_linea","marca"=>"","clase_linea"=>"","tipo_linea"=>""],

            ];
        }
        public function selectClases(){
            $sql = "SELECT * FROM trans_caracterizacion_clase ORDER BY nombre";
            $request = $this->select_all($sql);
            return $request;
        }
        public function selectMarcas(){
            $sql = "SELECT * FROM trans_caracterizacion_marca ORDER BY nombre";
            $request = $this->select_all($sql);
            return $request;
        }
        public function selectLineas(){
            $sql = "SELECT * FROM trans_caracterizacion_linea ORDER BY nombre";
            $request = $this->select_all($sql);
            return $request;
        }
        public function selectTipos(){
            $sql = "SELECT * FROM trans_caracterizacion_tipo ORDER BY nombre ASC";
            $request = $this->select_all($sql);
            return $request;
        }
        public function selectModelos(){
            $sql = "SELECT modelo as nombre FROM trans_base_gravable GROUP BY modelo ORDER BY modelo DESC";
            $request = $this->select_all($sql);
            return $request;
        }
        public function selectVigencias(){
            $sql = "SELECT vigencia as nombre FROM trans_base_gravable GROUP BY vigencia ORDER BY vigencia DESC";
            $request = $this->select_all($sql);
            return $request;
        }
        public function selectSearch(string $intPaginas,string $intVigencia,string $intPaginaNow,string $strTipo,string $strClase,string $strMarca,string $strLinea,string $strModelo,string $strBuscar){

            $this->intPaginas = $intPaginas;
            $this->intPaginaNow = $intPaginaNow;
            $this->strTipo = $strTipo;
            $this->strClase = $strClase;
            $this->strMarca = $strMarca;
            $this->strLinea = $strLinea;
            $this->strModelo = $strModelo;
            $this->intVigencia = $intVigencia;
            $limit ="";


            $this->intInicio = ($this->intPaginaNow-1)*$this->intPaginas;
            $this->strBuscar = $strBuscar;
            if($this->intPaginas != 0){
                $limit = " LIMIT $this->intInicio,$this->intPaginas";
            }
            $sql = "SELECT
            ba.id,
            ba.codigo,
            ba.valor,
            ba.modelo,
            ba.vigencia,
            ti.nombre as tipo,
            cl.nombre as clase,
            ci.nombre as cilindraje,
            oc.nombre as ocupantes,
            ca.nombre as capacidad,
            ma.nombre as marca,
            li.nombre as linea
            FROM trans_base_gravable ba
            INNER JOIN trans_caracterizacion_tipo ti ON ti.id = ba.tipo
            INNER JOIN trans_caracterizacion_clase cl ON cl.id = ba.clase_id AND ti.id = cl.tipo_id
            INNER JOIN trans_caracterizacion_cilindraje ci ON ci.id = ba.cilindraje_id
            INNER JOIN trans_caracterizacion_ocupantes oc ON oc.id = ba.ocupantes_id
            INNER JOIN trans_caracterizacion_capacidad ca ON ca.id = ba.capacidad_id
            INNER JOIN trans_caracterizacion_marca ma ON ma.id = ba.marca_id AND cl.id = ma.clase_id
            INNER JOIN trans_caracterizacion_linea li ON li.id = ba.linea_id AND li.marca_id = ma.id
            WHERE (cl.nombre LIKE '$this->strBuscar%' OR ci.id LIKE '$this->strBuscar%'
            OR ca.nombre LIKE '$this->strBuscar%' OR ma.nombre LIKE '$this->strBuscar%'
            OR li.nombre LIKE '$this->strBuscar%' OR ba.modelo LIKE '$this->strBuscar%'
            OR ti.nombre LIKE '$this->strBuscar%' OR ba.vigencia LIKE '$this->strBuscar%'
            OR oc.nombre LIKE '$this->strBuscar%')
            AND cl.id LIKE '$this->strClase%'  AND ma.id LIKE '$this->strMarca%' AND li.id LIKE '$this->strLinea%'
            AND ba.modelo LIKE '$this->strModelo%' AND ti.id LIKE '$this->strTipo%' AND ba.vigencia LIKE '$this->intVigencia%'
            ORDER BY ba.id $limit";


            $sqlTotal = "SELECT COUNT(*) as total
            FROM trans_base_gravable ba
            INNER JOIN trans_caracterizacion_tipo ti ON ti.id = ba.tipo
            INNER JOIN trans_caracterizacion_clase cl ON cl.id = ba.clase_id
            INNER JOIN trans_caracterizacion_cilindraje ci ON ci.id = ba.cilindraje_id
            INNER JOIN trans_caracterizacion_ocupantes oc ON oc.id = ba.ocupantes_id
            INNER JOIN trans_caracterizacion_capacidad ca ON ca.id = ba.capacidad_id
            INNER JOIN trans_caracterizacion_marca ma ON ma.id = ba.marca_id
            INNER JOIN trans_caracterizacion_linea li ON li.id = ba.linea_id
            WHERE (cl.nombre LIKE '$this->strBuscar%' OR ci.id LIKE '$this->strBuscar%'
            OR ca.nombre LIKE '$this->strBuscar%' OR ma.nombre LIKE '$this->strBuscar%'
            OR li.nombre LIKE '$this->strBuscar%' OR ba.modelo LIKE '$this->strBuscar%'
            OR ti.nombre LIKE '$this->strBuscar%' OR ba.vigencia LIKE '$this->strBuscar%'
            OR oc.nombre LIKE '$this->strBuscar%')
            AND cl.id LIKE '$this->strClase%'  AND ma.id LIKE '$this->strMarca%' AND li.id LIKE '$this->strLinea%'
            AND ba.modelo LIKE '$this->strModelo%' AND ti.id LIKE '$this->strTipo%' AND ba.vigencia LIKE '$this->intVigencia%'
            ORDER BY ba.id";
            $totalRecords = $this->select($sqlTotal)['total'];
            $totalPages = intval($totalRecords > 0 ? ceil($totalRecords/$this->intPaginas) : 0);
            $totalPages = $totalPages == 0 ? 1 : $totalPages;
            $request = $this->select_all($sql);
            return array("data"=>$request,"total"=>$totalPages);
        }
        public function checkCaracterizacion($tipo,$clase,$cilindraje,$pasajeros,$capacidad,$marca,$linea){
            $this->arrTablas[0]['nombre'] = $tipo;
            $this->arrTablas[1]['nombre'] = $clase;
            $this->arrTablas[1]['tipo'] = $tipo;
            $this->arrTablas[2]['nombre'] = $cilindraje;
            $this->arrTablas[3]['nombre'] = $pasajeros;
            $this->arrTablas[4]['nombre'] = $capacidad;
            $this->arrTablas[5]['nombre'] = $marca;
            $this->arrTablas[5]['clase'] = $clase;
            $this->arrTablas[5]['tipo_marca'] = $tipo;
            $this->arrTablas[6]['nombre'] = $linea;
            $this->arrTablas[6]['marca'] = $marca;
            $this->arrTablas[6]['clase_linea'] = $clase;
            $this->arrTablas[6]['tipo_linea'] = $tipo;
            $total = count($this->arrTablas);
            for ($i=0; $i < $total; $i++) {
                $tabla = $this->arrTablas[$i];
                $nombreTabla = $tabla['tabla'];
                if(isset($tabla['marca'])){
                    $sql = "SELECT li.id FROM $nombreTabla li
                    INNER JOIN trans_caracterizacion_marca ma ON li.marca_id = ma.id
                    INNER JOIN trans_caracterizacion_clase cl ON cl.id = ma.clase_id
                    INNER JOIN trans_caracterizacion_tipo ti ON cl.tipo_id = ti.id
                    WHERE ma.nombre = '{$tabla['marca']}' AND li.nombre = '{$tabla['nombre']}'
                    AND cl.nombre = '{$tabla['clase_linea']}' AND ti.nombre = '{$tabla['tipo_linea']}'";
                }else if(isset($tabla['tipo'])){
                    $sql = "SELECT ma.id FROM $nombreTabla ma INNER JOIN trans_caracterizacion_tipo ti ON ma.tipo_id = ti.id
                    WHERE ma.nombre = '{$tabla['nombre']}' AND ti.nombre = '{$tabla['tipo']}'";
                }else if(isset($tabla['clase'])){
                    $sql = "SELECT ma.id FROM $nombreTabla ma
                    INNER JOIN trans_caracterizacion_clase cl ON ma.clase_id = cl.id
                    INNER JOIN trans_caracterizacion_tipo ti ON cl.tipo_id = ti.id
                    WHERE ma.nombre = '{$tabla['nombre']}' AND cl.nombre = '{$tabla['clase']}' AND ti.nombre = '{$tabla['tipo_marca']}'";
                }else{
                    $sql = "SELECT id FROM $nombreTabla WHERE nombre = '{$tabla['nombre']}'";
                }
                $request = $this->select($sql);
                $dato = 0;
                if(empty($request)){
                    if(isset($tabla['marca'])){
                        $sql = "SELECT ma.id FROM trans_caracterizacion_marca ma
                        INNER JOIN trans_caracterizacion_clase cl ON cl.id =  ma.clase_id
                        INNER JOIN trans_caracterizacion_tipo ti ON cl.tipo_id = ti.id
                        WHERE ma.nombre = '{$tabla['marca']}' AND cl.nombre = '{$tabla['clase_linea']}' AND ti.nombre = '{$tabla['tipo_linea']}'";
                        $id = $this->select($sql)['id'];

                        $sql="INSERT INTO $nombreTabla (nombre,estado,marca_id) VALUES(?,?,?)";
                        $dato = $this->insert($sql,[$tabla['nombre'],"S",$id]);
                    }else if(isset($tabla['tipo'])){
                        $sql = "SELECT id FROM trans_caracterizacion_tipo WHERE nombre = '{$tabla['tipo']}'";
                        $id = $this->select($sql)['id'];

                        $sql="INSERT INTO $nombreTabla (nombre,estado,tipo_id) VALUES(?,?,?)";
                        $dato = $this->insert($sql,[$tabla['nombre'],"S",$id]);
                    }else if(isset($tabla['clase'])){
                        $sql = "SELECT cl.id FROM trans_caracterizacion_clase cl
                        INNER JOIN trans_caracterizacion_tipo ti ON cl.tipo_id = ti.id
                        WHERE cl.nombre = '{$tabla['clase']}' AND ti.nombre = '{$tabla['tipo_marca']}'";
                        $id = $this->select($sql)['id'];

                        $sql="INSERT INTO $nombreTabla (nombre,estado,clase_id) VALUES(?,?,?)";
                        $dato = $this->insert($sql,[$tabla['nombre'],"S",$id]);
                    }else{
                        $sql="INSERT INTO $nombreTabla (nombre,estado) VALUES(?,?)";
                        $dato = $this->insert($sql,[$tabla['nombre'],"S"]);
                    }
                }else{
                    $dato = $this->select($sql)['id'];
                }
                $this->arrTablas[$i]['id']=$dato;
            }
            return $this->arrTablas;
        }
        public function resetTables(){
            $this->select("TRUNCATE TABLE trans_caracterizacion_tipo");
            $this->select("TRUNCATE TABLE trans_caracterizacion_clase");
            $this->select("TRUNCATE TABLE trans_caracterizacion_cilindraje");
            $this->select("TRUNCATE TABLE trans_caracterizacion_ocupantes");
            $this->select("TRUNCATE TABLE trans_caracterizacion_capacidad");
            $this->select("TRUNCATE TABLE trans_caracterizacion_marca");
            $this->select("TRUNCATE TABLE trans_caracterizacion_linea");
            $this->select("TRUNCATE TABLE trans_base_gravable");
        }
        public function insertData($arrData){
            $arrResponse = [];
            if(!empty($arrData)){
                foreach ($arrData as $data) {
                    $sql = "INSERT INTO trans_base_gravable (codigo,tipo,clase_id,marca_id,linea_id,cilindraje_id,
                    capacidad_id,ocupantes_id,valor,modelo,vigencia) VALUES (?,?,?,?,?,?,?,?,?,?,?)";
                    $arrValues = [
                        $data['id'],
                        $data['tipo'],
                        $data['clase'],
                        $data['marca'],
                        $data['linea'],
                        $data['cilindraje'],
                        $data['capacidad'],
                        $data['pasajeros'],
                        $data['valor'],
                        $data['modelo'],
                        $data['vigencia']
                    ];
                    $request = $this->insert($sql,$arrValues);
                    if($request>0){
                        $arrResponse = ["status"=>true,"msg"=>"Datos guardados correctamente."];
                    }else{
                        $arrResponse = ["status"=>false,"msg"=>"No se ha completado el guardado total de los datos, intente de nuevo."];
                        break;
                    }
                }
            }
            return $arrResponse;
        }
    }
?>
