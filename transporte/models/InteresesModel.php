<?php
    require_once '../../Librerias/core/Helpers.php';
    require_once '../../Librerias/core/Mysql.php';
    session_start();
    ini_set('display_errors', '1');
    ini_set('display_startup_errors', '1');
    error_reporting(E_ERROR);
    class InteresesModel extends Mysql{
        private $intConsecutivo;
        private $intMes;
        private $intVigencia;
        private $floPorcentajeMoratorio;
        private $floPorcentajeCorriente;
        private $intPaginas;
        private $intPaginaNow;
        private $intInicio;
        private $arrMeses;
        function __construct(){
            parent::__construct();
            $this->arrMeses = [
                1=>"Enero",
                2=>"Febrero",
                3=>"Marzo",
                4=>"Abril",
                5=>"Mayo",
                6=>"Junio",
                7=>"Julio",
                8=>"Agosto",
                9=>"Septiembre",
                10=>"Octubre",
                11=>"Noviembre",
                12=>"Diciembre",
            ];
        }

        public function insertData(int $intMes,int $intVigencia,float $floPorcentajeMoratorio,float $floPorcentajeCorriente){
            $this->intMes = $intMes;
            $this->intVigencia = $intVigencia;
            $this->floPorcentajeMoratorio = $floPorcentajeMoratorio;
            $this->floPorcentajeCorriente = $floPorcentajeCorriente;
            $sql = "SELECT * FROM interes WHERE vigencia = $this->intVigencia AND mes = $this->intMes";
            $request = $this->select($sql);
            $return ="";
            if(empty($request)){
                $sql = "INSERT INTO interes (vigencia,mes,moratorio,corriente) VALUES(?,?,?,?)";
                $arrData = [$this->intVigencia,$this->intMes,$this->floPorcentajeMoratorio,$this->floPorcentajeCorriente];
                $return = $this->insert($sql,$arrData);
            }else{
                $return = "existe";
            }
            return $return;
        }
        public function updateData(int $intConsecutivo,int $intMes,int $intVigencia,float $floPorcentajeMoratorio,float $floPorcentajeCorriente){
            $this->intConsecutivo = $intConsecutivo;
            $this->intMes = $intMes;
            $this->intVigencia = $intVigencia;
            $this->floPorcentajeMoratorio = $floPorcentajeMoratorio;
            $this->floPorcentajeCorriente = $floPorcentajeCorriente;
            $sql = "SELECT * FROM interes WHERE vigencia = $this->intVigencia AND mes = $this->intMes AND id != $this->intConsecutivo";
            $request = $this->select($sql);
            $return = "";
            if(empty($request)){
                $sql = "UPDATE interes SET vigencia=?,mes=?,moratorio=?, corriente=? WHERE id = $this->intConsecutivo";
                $arrData = [$this->intVigencia,$this->intMes,$this->floPorcentajeMoratorio,$this->floPorcentajeCorriente];
                $return = $this->update($sql,$arrData);
            }else{
                $return = "existe";
            }
            return $return;
        }
        public function selectEdit(int $intConsecutivo){
            $this->intConsecutivo = $intConsecutivo;
            $sql = "SELECT * FROM interes WHERE id = $this->intConsecutivo";
            $request = $this->select($sql);
            return $request;
        }
        public function selectSearch(string $intPaginas,string $intPaginaNow,string $intVigencia){
            $this->intPaginas = $intPaginas;
            $this->intPaginaNow = $intPaginaNow;
            $this->intVigencia = $intVigencia;

            $limit ="";

            $this->intInicio = ($this->intPaginaNow-1)*$this->intPaginas;
            if($this->intPaginas != 0){
                $limit = " LIMIT $this->intInicio,$this->intPaginas";
            }
            $sql = "SELECT * FROM interes WHERE vigencia LIKE '%$this->intVigencia' ORDER BY id DESC $limit";
            $sqlTotal = "SELECT count(*) as total FROM interes WHERE vigencia LIKE '%$this->intVigencia' ORDER BY id DESC";
            $totalRecords = $this->select($sqlTotal)['total'];
            $totalPages = intval($totalRecords > 0 ? ceil($totalRecords/$this->intPaginas) : 0);
            $totalPages = $totalPages == 0 ? 1 : $totalPages;
            $request = $this->select_all($sql);
            $total =count($request);
            for ($i=0; $i < $total; $i++) {
                $request[$i]['is_status'] = $request[$i]['estado'] =="S"? 1 : 0;
                $request[$i]['mes'] = $this->arrMeses[$request[$i]['mes']];
            }
            return array("data"=>$request,"total"=>$totalPages);
        }
        public function selectVigencias(){
            $sql = "SELECT vigencia FROM interes GROUP BY vigencia ORDER BY vigencia DESC";
            $request = $this->select_all($sql);
            return $request;
        }
    }
?>
