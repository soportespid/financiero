<?php
    require_once '../../Librerias/core/Helpers.php';
    require_once '../../Librerias/core/Mysql.php';
    session_start();
    ini_set('display_errors', '1');
    ini_set('display_startup_errors', '1');
    error_reporting(E_ERROR);
    class BaseCarteraModel extends Mysql{

        private $intPaginas;
        private $intPaginaNow;
        private $intPeriodo;
        private $strPlaca;
        private $strBuscar;
        private $intInicio;
        private $intVigencia;
        function __construct(){
            parent::__construct();
        }
        public function selectVigencias(){
            $sql = "SELECT * FROM trans_cartera GROUP BY vigencia ORDER BY vigencia DESC";
            $request = $this->select_all($sql);
            return $request;
        }
        public function selectPeriodos(){
            $sql = "SELECT * FROM trans_cartera GROUP BY periodo ORDER BY periodo DESC";
            $request = $this->select_all($sql);
            return $request;
        }
        public function selectSearch(string $intPaginas,string $intVigencia,string $intPaginaNow,string $intPeriodo,$strPlaca,$strBuscar){

            $this->intPaginas = $intPaginas;
            $this->intPaginaNow = $intPaginaNow;
            $this->intPeriodo = $intPeriodo;
            $this->strPlaca = $strPlaca;
            $this->intVigencia = $intVigencia;
            $limit ="";

            $this->intInicio = ($this->intPaginaNow-1)*$this->intPaginas;
            $this->strBuscar = $strBuscar;
            if($this->intPaginas != 0){
                $limit = " LIMIT $this->intInicio,$this->intPaginas";
            }
            $sql = "SELECT *,DATE_FORMAT(fecha_transaccion,'%d/%m/%Y') as fecha_transaccion,
            DATE_FORMAT(fecha_radicacion,'%d/%m/%Y') as fecha_radicacion
            FROM trans_cartera
            WHERE (placa LIKE '$this->strBuscar%' OR documento LIKE '$this->strBuscar%'
            OR nombre LIKE '$this->strBuscar%' OR periodo LIKE '$this->strBuscar%'
            OR modelo LIKE '$this->strBuscar%')
            AND placa LIKE '$this->strPlaca%' AND vigencia LIKE '$this->intVigencia%'
            AND periodo LIKE '$this->intPeriodo%' ORDER BY id $limit";


            $sqlTotal = "SELECT count(*) as total FROM trans_cartera
            WHERE (placa LIKE '$this->strBuscar%' OR documento LIKE '$this->strBuscar%'
            OR nombre LIKE '$this->strBuscar%' OR periodo LIKE '$this->strBuscar%'
            OR modelo LIKE '$this->strBuscar%')
            AND placa LIKE '$this->strPlaca%' AND vigencia LIKE '$this->intVigencia%'
            AND periodo LIKE '$this->intPeriodo%' ORDER BY id";

            $totalRecords = $this->select($sqlTotal)['total'];
            $totalPages = intval($totalRecords > 0 ? ceil($totalRecords/$this->intPaginas) : 0);
            $totalPages = $totalPages == 0 ? 1 : $totalPages;
            $request = $this->select_all($sql);
            return array("data"=>$request,"total"=>$totalPages);
        }
        public function insertCarroceria($intIdCarroceria,$strCarroceria){
            $sql = "SELECT * FROM trans_caracterizacion_carroceria WHERE id = $intIdCarroceria";
            $request = $this->select_all($sql);
            if(empty($request)){
                $sql = "INSERT INTO trans_caracterizacion_carroceria (id,nombre) VALUES (?,?)";
                $request = $this->insert($sql,[$intIdCarroceria,$strCarroceria]);
            }
        }
        public function insertData($strPlaca,
        $strVigencia,
        $strPeriodo,
        $intAvaluo,
        $intValorDescuento,
        $intValorInteres,
        $intValorPago,
        $intValorSinDescuento,
        $intValorDescMatricula,
        $intValorSancion,
        $intValorDepartamento,
        $intValorMunicipio,
        $intValorInteresTotal,
        $intValorSancionTotal,
        $intValorAnterior,
        $intValorSaldoPagar,
        $intValorSaldoFavor,
        $intSemaforizacion,
        $strDocumento,
        $strNombre,
        $intPorcentajeDescuento,
        $intPorcentajeInteres,
        $intModelo,
        $strAntiguo,
        $strBlindado,
        $strCilindraje,
        $intTarifa,
        $intDiasInteres,
        $strEstado,
        $strFechaTransaccion,
        $strFechaRadicacion){
            $sql = "INSERT INTO trans_cartera (placa,vigencia,periodo,valor_avaluo,valor_descuento,
            valor_interes,valor_pago,valor_sin_descuento,valor_desc_matricula,valor_sancion,valor_departamento,valor_municipio,valor_interes_total,valor_sancion_total,
            valor_anterior,valor_saldo_pagar,valor_saldo_favor,semaforizacion,documento,nombre,porcentaje_descuento,porcentaje_interes,modelo,antiguo,blindado,cilindraje,
            tarifa,dias_interes,estado,fecha_transaccion,fecha_radicacion) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
            $request = $this->insert($sql,[
                $strPlaca,
                $strVigencia,
                $strPeriodo,
                $intAvaluo,
                $intValorDescuento,
                $intValorInteres,
                $intValorPago,
                $intValorSinDescuento,
                $intValorDescMatricula,
                $intValorSancion,
                $intValorDepartamento,
                $intValorMunicipio,
                $intValorInteresTotal,
                $intValorSancionTotal,
                $intValorAnterior,
                $intValorSaldoPagar,
                $intValorSaldoFavor,
                $intSemaforizacion,
                $strDocumento,
                $strNombre,
                $intPorcentajeDescuento,
                $intPorcentajeInteres,
                $intModelo,
                $strAntiguo,
                $strBlindado,
                $strCilindraje,
                $intTarifa,
                $intDiasInteres,
                $strEstado,
                $strFechaTransaccion,
                $strFechaRadicacion
            ]);
            return $request ;
        }
    }
?>
