<?php
    require_once '../../Librerias/core/Helpers.php';
    require_once '../../Librerias/core/Mysql.php';
    session_start();
    ini_set('display_errors', '1');
    ini_set('display_startup_errors', '1');
    error_reporting(E_ERROR);
    class ReciboImpuestoModel extends Mysql{
        public function getAllLiquidacionImpuestos($txt_search, $pagina_actual, $paginacion) {
            $limit = $search = "";

            if ($paginacion <> "") {
                $inicio = ($pagina_actual - 1) * $paginacion;
                $limit = "LIMIT $inicio, $paginacion";
            }

            if ($txt_search <> "") {
                $search = "AND id LIKE '$txt_search' OR placa LIKE '$txt_search%' OR propietario LIKE '$txt_search%' ";
            }

            $sql_liquidaciones = "SELECT id, fecha, placa, propietario, total_departamento AS total_pago FROM trans_impuesto_cab WHERE estado = 'S' $search ORDER BY id DESC $limit";
            $row_liquidaciones = $this->select_all($sql_liquidaciones);

            foreach ($row_liquidaciones as $i => $liquidacion) {
                $row_liquidaciones[$i]["nombre"] = getNombreTercero($liquidacion["propietario"]);
            }

            $sql_total = "SELECT COUNT(*) AS total FROM trans_impuesto_cab WHERE estado = 'S' $search";
            $row_total = $this->select($sql_total)["total"];

            return array("liquidaciones"=>$row_liquidaciones,"total"=>$row_total);
        }

        public function getAllRecaudos($txt_search, $pagina_actual, $paginacion) {
            $limit = $search = "";

            if ($paginacion <> "") {
                $inicio = ($pagina_actual - 1) * $paginacion;
                $limit = "LIMIT $inicio, $paginacion";
            }

            if ($txt_search <> "") {
                $search = " AND recaudo.id LIKE '$txt_search' OR liquidacion.placa LIKE '$txt_search%' OR liquidacion.propietario LIKE '$txt_search%' ";
            }

            $sql_recaudos = "SELECT recaudo.id, recaudo.fecha, recaudo.liquidacion_id, liquidacion.placa,
            liquidacion.propietario, recaudo.medio_pago, recaudo.cuenta_bancaria, recaudo.valor_recaudo, recaudo.detalle, recaudo.estado
            FROM trans_recaudo_impuesto AS recaudo INNER JOIN trans_impuesto_cab AS liquidacion ON recaudo.liquidacion_id = liquidacion.id
            WHERE recaudo.id <> 0 $search
            ORDER BY recaudo.id DESC $limit";
            $row_recaudos = $this->select_all($sql_recaudos);

            foreach ($row_recaudos as $i => $recaudo) {
                $row_recaudos[$i]["nombre_propietario"] = getNombreTercero($recaudo["propietario"]);
            }

            $sql_total = "SELECT COUNT(*) AS total
            FROM trans_recaudo_impuesto AS recaudo INNER JOIN trans_impuesto_cab AS liquidacion ON recaudo.liquidacion_id = liquidacion.id
            WHERE recaudo.id <> 0 $search";
            $row_total = $this->select($sql_total)["total"];

            return array("recaudos"=>$row_recaudos,"total"=>$row_total);
        }
        public function selectEdit(int $intConsecutivo){
            $sql_recaudos = "SELECT recaudo.id, recaudo.fecha, date_format(recaudo.fecha,'%d/%m/%Y') as fecha_format, recaudo.liquidacion_id, liquidacion.placa,
            liquidacion.propietario, recaudo.medio_pago, recaudo.cuenta_bancaria, recaudo.valor_recaudo, recaudo.detalle, recaudo.estado
            FROM trans_recaudo_impuesto AS recaudo INNER JOIN trans_impuesto_cab AS liquidacion ON recaudo.liquidacion_id = liquidacion.id
            WHERE recaudo.id <> 0  AND recaudo.id = $intConsecutivo";
            $row_recaudos = $this->select($sql_recaudos);
            $row_recaudos["nombre_propietario"] = getNombreTercero($row_recaudos["propietario"]);

            $sqlCuenta = "SELECT TB2.razonsocial as nombre,TB1.cuenta,TB1.ncuentaban as cuenta_banco,TB1.tipo, TB1.tercero
            FROM tesobancosctas TB1
            INNER JOIN terceros TB2
            ON TB1.tercero = TB2.cedulanit
            INNER JOIN cuentasnicsp TB3
            ON TB1.cuenta=TB3.cuenta
            WHERE TB1.estado='S' AND TB1.ncuentaban = '{$row_recaudos['cuenta_bancaria']}'";
            $nombreCuenta = $this->select($sqlCuenta)['nombre'];
            $row_recaudos["nombre_cuenta"] = $nombreCuenta;
            return $row_recaudos;
        }
        public function getCuentasBancoTransito(){
            $sql = "SELECT TB2.razonsocial as nombre,TB1.cuenta,TB1.ncuentaban as cuenta_banco,TB1.tipo, TB1.tercero
            FROM tesobancosctas TB1
            INNER JOIN terceros TB2
            ON TB1.tercero = TB2.cedulanit
            INNER JOIN cuentasnicsp TB3
            ON TB1.cuenta=TB3.cuenta
            WHERE TB1.estado='S' AND TB1.ncuentaban = '735001234'";
            $request = $this->select_all($sql);
            return $request;
        }

        public function insertRecaudo($data) {
            $sql_insert_recaudo = "INSERT INTO trans_recaudo_impuesto (fecha, liquidacion_id, medio_pago, cuenta_bancaria, cuenta_contable, valor_recaudo, detalle, estado) VALUES (?, ?, ?, ?, ?, ?, ?, ?)";

            $data_recaudo = [
                $data["fecha"],
                $data["liquidacion_id"],
                $data["medio_pago"],
                $data["cuenta_bancaria"],
                $data["cuenta_contable"],
                $data["valor_recaudo"],
                $data["detalle"],
                $data["estado"]
            ];

            $id = $this->insert($sql_insert_recaudo, $data_recaudo);
            if ($id > 0) {
                $this->cambioEstadoPago($data["liquidacion_id"]);
                $this->create_auditoria("Crear", $id);
            }
            return $id;
        }

        private function cambioEstadoPago($liquidacion_id) {
            $sql_cab = "UPDATE trans_impuesto_cab SET estado = ? WHERE id = $liquidacion_id";
            $sql_det = "UPDATE trans_impuesto_det SET estado = ? WHERE impuesto_id = $liquidacion_id";
            $this->update($sql_cab, ["P"]);
            $this->update($sql_det, ["P"]);
        }

        private function create_auditoria(string $accion, int $consecutivo) {
            $sql_funcion = "SELECT id FROM trans_funciones WHERE nombre = 'Recaudo impuesto'";
            $funcion = $this->select($sql_funcion);

            insertAuditoria("trans_auditoria","trans_funciones_id",$funcion["id"],$accion,$consecutivo);
        }

        public function anularRecaudo($id) {
            $sql_anular = "UPDATE trans_recaudo_impuesto SET estado = ? WHERE id = $id";
            $row_anular = $this->update($sql_anular, ["N"]);

            if ($row_anular == 1) {
                $this->create_auditoria("Anular", $id);
            }

            return $row_anular;
        }
    }
?>
