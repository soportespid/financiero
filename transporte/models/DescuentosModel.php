<?php
    require_once '../../Librerias/core/Helpers.php';
    require_once '../../Librerias/core/Mysql.php';
    session_start();
    ini_set('display_errors', '1');
    ini_set('display_startup_errors', '1');
    error_reporting(E_ERROR);
    class DescuentosModel extends Mysql{
        private $intConsecutivo;
        private $strFecha;
        private $intTipo;
        private $floPorcentaje;
        private $intPaginas;
        private $intPaginaNow;
        private $intInicio;
        private $arrTipos;
        function __construct(){
            parent::__construct();
            $this->arrTipos = [1=>"Por pronto pago"];
        }

        public function insertData(string $strFecha,int $intTipo,float $floPorcentaje){
            $this->strFecha = $strFecha;
            $this->intTipo = $intTipo;
            $this->floPorcentaje = $floPorcentaje;
            $sql = "INSERT INTO trans_descuentos (tipo,fecha,porcentaje) VALUES(?,?,?)";
            $arrData = [$this->intTipo,$this->strFecha,$this->floPorcentaje];
            $request = $this->insert($sql,$arrData);
            return $request;
        }
        public function updateData(int $intConsecutivo,string $strFecha,int $intTipo,float $floPorcentaje){
            $this->intConsecutivo = $intConsecutivo;
            $this->strFecha = $strFecha;
            $this->intTipo = $intTipo;
            $this->floPorcentaje = $floPorcentaje;
            $sql = "UPDATE trans_descuentos SET tipo=?,fecha=?,porcentaje=? WHERE id = $this->intConsecutivo";
            $arrData = [$this->intTipo,$this->strFecha,$this->floPorcentaje];
            $request = $this->update($sql,$arrData);
            return $request;
        }
        public function selectEdit(int $intConsecutivo){
            $this->intConsecutivo = $intConsecutivo;
            $sql = "SELECT * FROM trans_descuentos WHERE id = $this->intConsecutivo";
            $request = $this->select($sql);
            return $request;
        }
        public function selectSearch(string $intPaginas,string $intPaginaNow,string $strTipo){
            $this->intPaginas = $intPaginas;
            $this->intPaginaNow = $intPaginaNow;
            $this->intTipo = $strTipo;
            $limit ="";

            $this->intInicio = ($this->intPaginaNow-1)*$this->intPaginas;
            if($this->intPaginas != 0){
                $limit = " LIMIT $this->intInicio,$this->intPaginas";
            }
            $sql = "SELECT *,DATE_FORMAT(fecha,'%d/%m/%Y') as fecha FROM trans_descuentos WHERE tipo LIKE '$this->intTipo%' ORDER BY id DESC $limit";
            $sqlTotal = "SELECT count(*) as total FROM trans_descuentos WHERE tipo LIKE '$this->intTipo%' ORDER BY id DESC";
            $totalRecords = $this->select($sqlTotal)['total'];
            $totalPages = intval($totalRecords > 0 ? ceil($totalRecords/$this->intPaginas) : 0);
            $totalPages = $totalPages == 0 ? 1 : $totalPages;
            $request = $this->select_all($sql);
            $total =count($request);
            for ($i=0; $i < $total; $i++) {
                $request[$i]['is_status'] = $request[$i]['estado'] =="S"? 1 : 0;
                $request[$i]['tipo'] = $this->arrTipos[$request[$i]['tipo']];
            }
            return array("data"=>$request,"total"=>$totalPages);
        }
        public function updateStatus(int $intConsecutivo,string $estado){
            $this->intConsecutivo = $intConsecutivo;
            $sql = "UPDATE trans_descuentos SET estado = '$estado' WHERE id = $this->intConsecutivo";
            $request = $this->update($sql,[$estado]);
            return $request;
        }
    }
?>
