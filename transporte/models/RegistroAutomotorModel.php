<?php
    require_once '../../Librerias/core/Helpers.php';
    require_once '../../Librerias/core/Mysql.php';
    session_start();
    ini_set('display_errors', '1');
    ini_set('display_startup_errors', '1');
    error_reporting(E_ERROR);
    class RegistroAutomotorModel extends Mysql{
        public function getAllDepartamentos() {
            $sql_departamentos = "SELECT danedpto AS codigo, nombredpto AS nombre FROM danedpto ORDER BY id_dpto ASC";
            $row_departamentos = $this->select_all($sql_departamentos);
            return $row_departamentos;
        }

        public function getAllMunicipios() {
            $sql_municipios = "SELECT danedpto AS codigo_departamento, danemnpio AS codigo, nom_mnpio AS nombre FROM danemnpio ORDER BY id_mnpio ASC";
            $row_municipios = $this->select_all($sql_municipios);
            return $row_municipios;
        }

        public function getAllTipos() {
            $sql_tipos = "SELECT id, nombre FROM trans_caracterizacion_tipo WHERE estado = 'S' ORDER BY id ASC";
            $row_tipos = $this->select_all($sql_tipos);
            return $row_tipos;
        }

        public function getAllClases($tipo_id) {
            $sql_clases = "SELECT id, nombre FROM trans_caracterizacion_clase WHERE estado = 'S' AND tipo_id = $tipo_id ORDER BY id ASC";
            $row_clases = $this->select_all($sql_clases);
            return $row_clases;
        }

        public function getAllMarcas($clase_id) {
            $sql_marcas = "SELECT id, nombre FROM trans_caracterizacion_marca WHERE estado = 'S' AND clase_id = $clase_id ORDER BY id ASC";
            $row_marcas = $this->select_all($sql_marcas);
            return $row_marcas;
        }

        public function getAllLineas($marcaId) {
            $sql_lineas = "SELECT id, nombre FROM trans_caracterizacion_linea WHERE estado = 'S' AND marca_id = $marcaId ORDER BY id ASC";
            $row_lineas = $this->select_all($sql_lineas);
            return $row_lineas;
        }

        public function getAllCombustibles() {
            $sql_combustibles = "SELECT id, nombre FROM trans_caracterizacion_combustible WHERE estado = 'S' ORDER BY id ASC";
            $row_combustibles = $this->select_all($sql_combustibles);
            return $row_combustibles;
        }

        public function getAllCilindrajes() {
            $sql_cilindrajes = "SELECT id, nombre FROM trans_caracterizacion_cilindraje WHERE estado = 'S' ORDER BY id ASC";
            $row_cilindrajes = $this->select_all($sql_cilindrajes);
            return $row_cilindrajes;
        }

        public function getAllPotencias() {
            $sql_potencias = "SELECT id, nombre FROM trans_caracterizacion_potencia WHERE estado = 'S' ORDER BY id ASC";
            $row_potencias = $this->select_all($sql_potencias);
            return $row_potencias;
        }

        public function getAllTipoPublicos() {
            $sql_publicos = "SELECT id, nombre FROM trans_caracterizacion_publico WHERE estado = 'S' ORDER BY id ASC";
            $row_publicos = $this->select_all($sql_publicos);
            return $row_publicos;
        }

        public function getAllCarrocerias() {
            $sql_carrocerias = "SELECT id, nombre FROM trans_caracterizacion_carroceria WHERE estado = 'S' ORDER BY id ASC";
            $row_carrocerias = $this->select_all($sql_carrocerias);
            return $row_carrocerias;
        }

        public function getAllServicios() {
            $sql_servicios = "SELECT id, nombre FROM trans_caracterizacion_servicio WHERE estado = 'S' ORDER BY id ASC";
            $row_servicios = $this->select_all($sql_servicios);
            return $row_servicios;
        }

        public function existePlaca($placa) {
            $sql_automotor = "SELECT COUNT(*) AS total_row FROM trans_automotores WHERE placa = '$placa'";
            $row_automotor = $this->select($sql_automotor);
            $status = $row_automotor["total_row"] > 0 ? true : false;
            return $status;
        }

        public function getClasesAutomotores() {
            $sql = "SELECT DISTINCT automotor.clase_id, c.nombre FROM trans_automotores AS automotor INNER JOIN trans_caracterizacion_clase AS c ON c.id = automotor.clase_id";
            $row = $this->select_all($sql);
            return $row;
        }

        public function getMarcasAutomotores() {
            $sql = "SELECT DISTINCT automotor.marca_id, m.nombre FROM trans_automotores AS automotor INNER JOIN trans_caracterizacion_marca AS m ON m.id = automotor.marca_id";
            $row = $this->select_all($sql);
            return $row;
        }

        public function getLineasAutomotores() {
            $sql = "SELECT DISTINCT automotor.linea_id, l.nombre FROM trans_automotores AS automotor INNER JOIN trans_caracterizacion_linea as l ON l.id = automotor.linea_id";
            $row = $this->select_all($sql);
            return $row;
        }

        public function getModelosAutomotores() {
            $sql = "SELECT DISTINCT modelo FROM trans_automotores ORDER BY modelo DESC";
            $row = $this->select_all($sql);
            return $row;
        }

        public function getAllAutomotores($clase_id, $marca_id, $linea_id, $modelo_id, $txt_search, $limite_pagina, $pagina_actual) {
            $limit = $clase = $marca = $linea = $modelo = $search = $inicio = "";

            $inicio = ($pagina_actual - 1) * $limite_pagina;

            if ($limite_pagina != "") {
                $limit = "LIMIT $inicio, $limite_pagina";
            }

            if ($clase_id != "") {
                $clase = "AND automotor.clase_id = $clase_id ";
            }

            if ($marca_id != "") {
                $marca = "AND automotor.marca_id = $marca_id ";
            }

            if ($linea_id != "") {
                $linea = "AND automotor.linea_id = $linea_id ";
            }

            if ($modelo_id != "") {
                $modelo = "AND automotor.modelo = $modelo_id ";
            }

            if ($txt_search != "") {
                $search = "AND automotor.propietario LIKE '%$txt_search%' OR automotor.placa LIKE '%$txt_search%'";
            }

            $sql_automotores = "SELECT automotor.id,
            automotor.placa,
            automotor.fecha_tramite,
            automotor.propietario,
            automotor.dpto,
            automotor.mnpio,
            dpto.nombredpto AS nombre_departamento,
            c.nombre AS nombre_clase,
            m.nombre AS nombre_marca,
            l.nombre AS nombre_linea,
            automotor.modelo,
            cd.nombre AS nombre_cilindraje,
            automotor.capacidad_carga,
            automotor.cantidad_pasajeros,
            p.nombre AS nombre_publico,
            ca.nombre AS nombre_carroceria
            FROM trans_automotores AS automotor
            INNER JOIN danedpto AS dpto ON dpto.danedpto = automotor.dpto
            INNER JOIN trans_caracterizacion_clase AS c ON c.id = automotor.clase_id
            INNER JOIN trans_caracterizacion_marca AS m ON m.id = automotor.marca_id
            LEFT JOIN trans_caracterizacion_linea as l ON l.id = automotor.linea_id
            LEFT JOIN trans_caracterizacion_cilindraje AS cd ON cd.id = automotor.cilindraje_id
            LEFT JOIN trans_caracterizacion_publico AS p ON p.id = automotor.publico_id
            LEFT JOIN trans_caracterizacion_carroceria AS ca ON ca.id = automotor.carroceria_id
            WHERE automotor.id != '' $clase $marca $linea $modelo $search
            ORDER BY automotor.id
            $limit";
            $row_automotores = $this->select_all($sql_automotores);

            foreach ($row_automotores as $i => $data) {
                $row_automotores[$i]["nombre"] = getNombreTercero($data["propietario"]);

                $sql_municipio = "SELECT nom_mnpio FROM danemnpio WHERE danedpto = '$data[dpto]' AND danemnpio = '$data[mnpio]'";
                $row_municipio = $this->select($sql_municipio);
                $row_automotores[$i]["nombre_municipio"] = $row_municipio["nom_mnpio"];

                $row_automotores[$i]["nombre_linea"] = $row_automotores[$i]["nombre_linea"] == NULL ? "NA" : $row_automotores[$i]["nombre_linea"];
                $row_automotores[$i]["nombre_cilindraje"] = $row_automotores[$i]["nombre_cilindraje"] == NULL ? "NA" : $row_automotores[$i]["nombre_cilindraje"];
                $row_automotores[$i]["nombre_publico"] = $row_automotores[$i]["nombre_publico"] == NULL ? "NA" : $row_automotores[$i]["nombre_publico"];
                $row_automotores[$i]["nombre_carroceria"] = $row_automotores[$i]["nombre_carroceria"] == NULL ? "NA" : $row_automotores[$i]["nombre_carroceria"];
            }

            $sql_total = "SELECT COUNT(*) AS total
            FROM trans_automotores AS automotor
            INNER JOIN danedpto AS dpto ON dpto.danedpto = automotor.dpto
            INNER JOIN trans_caracterizacion_clase AS c ON c.id = automotor.clase_id
            INNER JOIN trans_caracterizacion_marca AS m ON m.id = automotor.marca_id
            LEFT JOIN trans_caracterizacion_linea as l ON l.id = automotor.linea_id
            LEFT JOIN trans_caracterizacion_cilindraje AS cd ON cd.id = automotor.cilindraje_id
            LEFT JOIN trans_caracterizacion_publico AS p ON p.id = automotor.publico_id
            LEFT JOIN trans_caracterizacion_carroceria AS ca ON ca.id = automotor.carroceria_id
            WHERE automotor.id != '' $clase $marca $linea $modelo $search
            ORDER BY automotor.id";
            // dep($sql_total);
            $total_registros = $this->select($sql_total)["total"];
            $totalPages = intval($total_registros > 0 ? ceil($total_registros/$limite_pagina) : 0);
            $totalPages = $totalPages == 0 ? 1 : $totalPages;

            return array("data"=>$row_automotores,"total"=>$totalPages);
        }

        public function getAutomotorEdit($id) {
            $sql_automotor = "SELECT automotor.fecha_tramite,
            automotor.placa,
            automotor.dpto,
            automotor.mnpio,
            automotor.propietario,
            CASE WHEN t.razonsocial IS NULL OR t.razonsocial = ''
            THEN CONCAT(t.nombre1,' ',t.nombre2,' ',t.apellido1,' ',t.apellido2)
            ELSE t.razonsocial END AS nombre,
            automotor.tipo_id,
            automotor.clase_id,
            clase.nombre AS nombre_clase,
            automotor.marca_id,
            marca.nombre AS nombre_marca,
            automotor.linea_id,
            linea.nombre AS nombre_linea,
            automotor.combustible_id,
            combustible.nombre AS nombre_combustible,
            automotor.color,
            automotor.modelo,
            automotor.cilindraje_id,
            cilindraje.nombre AS nombre_cilindraje,
            automotor.capacidad_carga,
            automotor.cantidad_pasajeros,
            automotor.potencia_hp,
            automotor.publico_id,
            publico.nombre AS nombre_publico,
            automotor.carroceria_id,
            carroceria.nombre AS nombre_carroceria,
            automotor.vehiculo_antiguo,
            automotor.blindaje,
            automotor.fecha_blindaje,
            automotor.desmonte_blindaje,
            automotor.fecha_desmonte,
            automotor.numero_motor,
            automotor.numero_chasis,
            automotor.numero_serie,
            automotor.numero_vin,
            automotor.tipo_servicio_id,
            automotor.estado,
            automotor.avaluo
            FROM trans_automotores AS automotor
            INNER JOIN terceros AS t ON t.cedulanit = automotor.propietario
            INNER JOIN trans_caracterizacion_clase AS clase ON clase.id = automotor.clase_id
            LEFT JOIN trans_caracterizacion_marca AS marca ON marca.id = automotor.marca_id
            LEFT JOIN trans_caracterizacion_linea AS linea ON linea.id = automotor.linea_id
            LEFT JOIN trans_caracterizacion_combustible AS combustible ON combustible.id = automotor.combustible_id
            LEFT JOIN trans_caracterizacion_cilindraje AS cilindraje ON cilindraje.id = automotor.cilindraje_id
            LEFT JOIN trans_caracterizacion_publico AS publico ON publico.id = automotor.publico_id
            LEFT JOIN trans_caracterizacion_carroceria AS carroceria ON carroceria.id = automotor.carroceria_id
            WHERE automotor.id = $id";
            $row_automotor = $this->select($sql_automotor);

            $row_automotor["marca_id"] = $row_automotor["marca_id"] == "" ? "" : $row_automotor["marca_id"];
            $row_automotor["nombre_marca"] = $row_automotor["nombre_marca"] == "" ? "" : $row_automotor["nombre_marca"];

            $row_automotor["linea_id"] = $row_automotor["linea_id"] == "" ? "" : $row_automotor["linea_id"];
            $row_automotor["nombre_linea"] = $row_automotor["nombre_linea"] == "" ? "" : $row_automotor["nombre_linea"];

            $row_automotor["combustible_id"] = $row_automotor["combustible_id"] == "" ? "" : $row_automotor["combustible_id"];
            $row_automotor["nombre_combustible"] = $row_automotor["nombre_combustible"] == "" ? "" : $row_automotor["nombre_combustible"];

            $row_automotor["cilindraje_id"] = $row_automotor["cilindraje_id"] == "" ? "" : $row_automotor["cilindraje_id"];
            $row_automotor["nombre_cilindraje"] = $row_automotor["nombre_cilindraje"] == "" ? "" : $row_automotor["nombre_cilindraje"];

            $row_automotor["publico_id"] = $row_automotor["publico_id"] == "" ? "" : $row_automotor["publico_id"];
            $row_automotor["nombre_publico"] = $row_automotor["nombre_publico"] == "" ? "" : $row_automotor["nombre_publico"];

            $row_automotor["carroceria_id"] = $row_automotor["carroceria_id"] == "" ? "" : $row_automotor["carroceria_id"];
            $row_automotor["nombre_carroceria"] = $row_automotor["nombre_carroceria"] == "" ? "" : $row_automotor["nombre_carroceria"];

            $row_automotor["tipo_servicio_id"] = $row_automotor["tipo_servicio_id"] == 0 ? "" : $row_automotor["tipo_servicio_id"];

            return $row_automotor;
        }

        public function getHistorialAutomotor($placa) {
            $sql_automotor = "SELECT automotor.fecha_actualizacion AS fecha_realizado,
            automotor.fecha_cambio,
            automotor.tipo_tramite_id,
            automotor.fecha_tramite,
            automotor.placa,
            automotor.dpto,
            dpto.nombredpto AS nombre_departamento,
            automotor.mnpio,
            automotor.propietario,
            CASE WHEN t.razonsocial IS NULL OR t.razonsocial = ''
            THEN CONCAT(t.nombre1,' ',t.nombre2,' ',t.apellido1,' ',t.apellido2)
            ELSE t.razonsocial END AS nombre,
            clase.nombre AS nombre_clase,
            marca.nombre AS nombre_marca,
            linea.nombre AS nombre_linea,
            combustible.nombre AS nombre_combustible,
            automotor.color,
            automotor.modelo,
            cilindraje.nombre AS nombre_cilindraje,
            automotor.capacidad_carga,
            automotor.cantidad_pasajeros,
            automotor.potencia_hp,
            publico.nombre AS nombre_publico,
            carroceria.nombre AS nombre_carroceria,
            automotor.vehiculo_antiguo,
            automotor.blindaje,
            automotor.fecha_blindaje,
            automotor.desmonte_blindaje,
            automotor.fecha_desmonte,
            automotor.numero_motor,
            automotor.numero_chasis,
            automotor.numero_serie,
            automotor.numero_vin,
            automotor.avaluo,
            servicio.nombre AS nombre_tipo_servicio
            FROM trans_automotores_historial AS automotor
            INNER JOIN danedpto AS dpto ON dpto.danedpto = automotor.dpto
            INNER JOIN terceros AS t ON t.cedulanit = automotor.propietario
            INNER JOIN trans_caracterizacion_clase AS clase ON clase.id = automotor.clase_id
            LEFT JOIN trans_caracterizacion_marca AS marca ON marca.id = automotor.marca_id
            LEFT JOIN trans_caracterizacion_linea AS linea ON linea.id = automotor.linea_id
            LEFT JOIN trans_caracterizacion_combustible AS combustible ON combustible.id = automotor.combustible_id
            LEFT JOIN trans_caracterizacion_cilindraje AS cilindraje ON cilindraje.id = automotor.cilindraje_id
            LEFT JOIN trans_caracterizacion_publico AS publico ON publico.id = automotor.publico_id
            LEFT JOIN trans_caracterizacion_carroceria AS carroceria ON carroceria.id = automotor.carroceria_id
            LEFT JOIN trans_caracterizacion_servicio AS servicio ON servicio.id = automotor.tipo_servicio_id
            WHERE automotor.placa = '$placa'";
            $row_automotor = $this->select_all($sql_automotor);

            if (!empty($row_automotor)) {
                foreach ($row_automotor as $i => $automotor) {
                    $sql_municipio = "SELECT nom_mnpio FROM danemnpio WHERE danedpto = '$automotor[dpto]' AND danemnpio = '$automotor[mnpio]'";
                    $row_municipio = $this->select($sql_municipio);
                    $row_automotor[$i]["nombre_municipio"] = $row_municipio["nom_mnpio"];

                    $row_automotor[$i]["nombre_marca"] = $automotor["nombre_marca"] == "" ? "" : $automotor["nombre_marca"];
                    $row_automotor[$i]["nombre_linea"] = $automotor["nombre_linea"] == "" ? "" : $automotor["nombre_linea"];
                    $row_automotor[$i]["nombre_combustible"] = $automotor["nombre_combustible"] == "" ? "" : $automotor["nombre_combustible"];
                    $row_automotor[$i]["nombre_cilindraje"] = $automotor["nombre_cilindraje"] == "" ? "" : $automotor["nombre_cilindraje"];
                    $row_automotor[$i]["nombre_publico"] = $automotor["nombre_publico"] == "" ? "" : $automotor["nombre_publico"];
                    $row_automotor[$i]["nombre_carroceria"] = $automotor["nombre_carroceria"] == "" ? "" : $automotor["nombre_carroceria"];
                    $row_automotor[$i]["nombre_tipo_servicio"] = $automotor["nombre_tipo_servicio"] == "" ? "" : $automotor["nombre_tipo_servicio"];
                }
            }

            return $row_automotor;
        }

        /* Insertar datos */

        public function insertRegistroAutomotor($data) {
            $sql = "INSERT INTO trans_automotores (placa, dpto, mnpio, fecha_tramite, propietario, tipo_id, clase_id, marca_id, linea_id, combustible_id, color, modelo, cilindraje_id, capacidad_carga, cantidad_pasajeros, potencia_hp, publico_id, carroceria_id, vehiculo_antiguo, blindaje, fecha_blindaje, desmonte_blindaje, fecha_desmonte, numero_motor, numero_chasis, numero_serie, numero_vin, tipo_servicio_id, empresa_vinculadora, estado,avaluo) VALUES (?,?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";

            $values = [
                "$data[placa]",
                "$data[departamento_cod]",
                "$data[municipio_cod]",
                "$data[fecha_tramite]",
                $data["propiertario"],
                $data["tipo_id"],
                $data["clase_id"],
                $data["marca_id"],
                $data["linea_id"],
                $data["combustible_id"],
                "$data[color]",
                "$data[modelo]",
                $data["cilindraje_id"],
                $data["capacidad_carga"],
                $data["ocupantes"],
                $data["potencia_hp"],
                $data["tipo_publico_id"],
                $data["carroceria_id"],
                "$data[vehiculo_antiguo]",
                "$data[blindaje]",
                "$data[fecha_blindaje]",
                "$data[desmonte_blindaje]",
                "$data[fecha_desmonte]",
                "$data[numero_motor]",
                "$data[numero_chasis]",
                "$data[numero_serie]",
                "$data[numero_vin]",
                $data["servicio_id"],
                "$data[empresa_vinculadora]",
                "S",
                "$data[avaluo]",
            ];

            $id = $this->insert($sql, $values);

            $sql_funcion = "SELECT id FROM trans_funciones WHERE nombre = 'Registro automotor'";
            $funcion = $this->select($sql_funcion);

            insertAuditoria("trans_auditoria","trans_funciones_id",$funcion["id"],"Crear",$id);

            return $id;
        }

        public function InsertEditRegistroAutomotor($data) {
            $sql_automotor = "SELECT * FROM trans_automotores WHERE id = $data[id]";
            $row_automotor = $this->select($sql_automotor);

            $sql_insert_historial = "INSERT INTO trans_automotores_historial (placa, tipo_tramite_id, dpto, mnpio, fecha_tramite, propietario, tipo_id, clase_id, marca_id, linea_id, combustible_id, color, modelo, cilindraje_id, capacidad_carga, cantidad_pasajeros, potencia_hp, publico_id, carroceria_id, vehiculo_antiguo, blindaje, fecha_blindaje, desmonte_blindaje, fecha_desmonte, numero_motor, numero_chasis, numero_serie, numero_vin, tipo_servicio_id, empresa_vinculadora, fecha_actualizacion, estado, avaluo) VALUES (?,?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";

            // Datos insert historial
            $data_historial = [
                "$row_automotor[placa]",
                1,
                "$row_automotor[dpto]",
                "$row_automotor[mnpio]",
                "$row_automotor[fecha_tramite]",
                "$row_automotor[propietario]",
                $row_automotor["tipo_id"],
                $row_automotor["clase_id"],
                $row_automotor["marca_id"],
                $row_automotor["linea_id"],
                $row_automotor["combustible_id"],
                $row_automotor["color"],
                $row_automotor["modelo"],
                $row_automotor["cilindraje_id"],
                $row_automotor["capacidad_carga"],
                $row_automotor["cantidad_pasajeros"],
                $row_automotor["potencia_hp"],
                $row_automotor["publico_id"],
                $row_automotor["carroceria_id"],
                "$row_automotor[vehiculo_antiguo]",
                "$row_automotor[blindaje]",
                "$row_automotor[fecha_blindaje]",
                "$row_automotor[desmonte_blindaje]",
                "$row_automotor[fecha_desmonte]",
                "$row_automotor[numero_motor]",
                "$row_automotor[numero_chasis]",
                "$row_automotor[numero_serie]",
                "$row_automotor[numero_vin]",
                $row_automotor["tipo_servicio_id"],
                "$row_automotor[empresa_vinculadora]",
                "$row_automotor[fecha_actualizacion]",
                "$row_automotor[estado]",
                "$row_automotor[avaluo]"
            ];
            $request_historial = $this->insert($sql_insert_historial, $data_historial);

            if ($request_historial == 0) {
                return false;
            }

            $sql_delete = "DELETE FROM trans_automotores WHERE id = $data[id]";
            $this->delete($sql_delete);

            $sql_insert = "INSERT INTO trans_automotores (id, placa, dpto, mnpio, fecha_tramite, propietario, tipo_id, clase_id, marca_id, linea_id, combustible_id, color, modelo, cilindraje_id, capacidad_carga, cantidad_pasajeros, potencia_hp, publico_id, carroceria_id, vehiculo_antiguo, blindaje, fecha_blindaje, desmonte_blindaje, fecha_desmonte, numero_motor, numero_chasis, numero_serie, numero_vin, tipo_servicio_id, empresa_vinculadora, estado,avaluo) VALUES (?,?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";

            $data_insert = [
                $data["id"],
                "$data[placa]",
                "$data[dpto]",
                "$data[mnpio]",
                "$data[fecha_tramite]",
                "$data[propietario]",
                $data["tipo_id"],
                $data["clase_id"],
                $data["marca_id"],
                $data["linea_id"],
                $data["combustible_id"],
                "$data[color]",
                "$data[modelo]",
                $data["cilindraje_id"],
                $data["capacidad_carga"],
                $data["cantidad_pasajeros"],
                $data["potencia_hp"],
                $data["publico_id"],
                $data["carroceria_id"],
                "$data[vehiculo_antiguo]",
                "$data[blindaje]",
                "$data[fecha_blindaje]",
                "$data[desmonte_blindaje]",
                "$data[fecha_desmonte]",
                "$data[numero_motor]",
                "$data[numero_chasis]",
                "$data[numero_serie]",
                "$data[numero_vin]",
                $data["tipo_servicio_id"],
                "$data[empresa_vinculadora]",
                "S",
                "$data[avaluo]",
            ];
            $request = $this->insert($sql_insert, $data_insert);

            $sql_funcion = "SELECT id FROM trans_funciones WHERE nombre = 'Registro automotor'";
            $funcion = $this->select($sql_funcion);

            insertAuditoria("trans_auditoria","trans_funciones_id",$funcion["id"],"Editar",$request);

            return $request;
        }
    }
?>
