<?php
    require_once '../../Librerias/core/Helpers.php';
    require_once '../../Librerias/core/Mysql.php';
    session_start();
    /*ini_set('display_errors', '1');
    ini_set('display_startup_errors', '1');
    error_reporting(E_ALL);*/
    class CaracterizacionVehicularModel extends Mysql{
        private $intConsecutivo;
        private $strNombre;
        private $strTabla;
        private $intVigencia;
        private $intMarca;

        function __construct(){
            parent::__construct();
            $this->intVigencia = date_create(date("Y-m-d"))->format('Y');
        }
        public function selectMarcas(){
            $sql = "SELECT * FROM trans_caracterizacion_marca WHERE estado = 'S' ORDER BY nombre";
            $request = $this->select_all($sql);
            return $request;
        }
        public function selectData($strTabla,$intTipoOpcion){
            $this->strTabla = $strTabla;
            if($intTipoOpcion ==11){
                $sql = "SELECT li.*,ma.nombre as marca
                FROM $this->strTabla li
                INNER JOIN trans_caracterizacion_marca ma
                ON li.marca_id = ma.id
                ORDER BY li.id DESC";
            }else{
                $sql = "SELECT * FROM $this->strTabla ORDER BY id DESC";
            }
            $request = $this->select_all($sql);
            $total = count($request);
            for ($i=0; $i < $total; $i++) {
                $request[$i]['is_status'] = $request[$i]['estado'] =="S"? 1 : 0;
            }
            return $request;
        }
        public function insertData(string $strNombre,string $strTabla,string $intMarca){
            $this->strTabla = $strTabla;
            $this->strNombre = $strNombre;
            $this->intMarca = $intMarca;
            $sqlMarca =$this->intMarca !="" ? " AND marca_id = $this->intMarca" : "";
            $sql = "SELECT * FROM $this->strTabla WHERE nombre = '$this->strNombre' $sqlMarca";
            $request = $this->select_all($sql);
            $return = "";
            if(empty($request)){
                if($this->intMarca !=""){
                    $sql="INSERT INTO $this->strTabla (nombre,estado,marca_id) VALUES(?,?,?)";
                    $return = $this->insert($sql,[$this->strNombre,"S",$this->intMarca]);
                }else{
                    $sql="INSERT INTO $this->strTabla (nombre,estado) VALUES(?,?)";
                    $return = $this->insert($sql,[$this->strNombre,"S"]);
                }
            }else{
                $return = "existe";
            }
            return $return;
        }
        public function updateData(int $intConsecutivo,string $strNombre,string $strTabla,string $intMarca){
            $this->strNombre = $strNombre;
            $this->intConsecutivo = $intConsecutivo;
            $this->strTabla = $strTabla;
            $this->intMarca = $intMarca;
            $sqlMarca =$this->intMarca !="" ? " AND marca_id = $this->intMarca" : "";
            $sql = "SELECT * FROM $this->strTabla WHERE nombre = '$this->strNombre' AND id != $this->intConsecutivo $sqlMarca";
            $request = $this->select_all($sql);
            $return = "";
            if(empty($request)){
                if($this->intMarca !=""){
                    $sql="UPDATE  $this->strTabla SET nombre=?,marca_id=? WHERE id = $this->intConsecutivo";
                    $return = $this->update($sql,[$this->strNombre,$this->intMarca]);
                }else{
                    $sql="UPDATE $this->strTabla SET nombre=? WHERE id = $this->intConsecutivo";
                    $return = $this->update($sql,[$this->strNombre]);
                }
            }else{
                $return = "existe";
            }
            return $return;
        }
        public function selectEdit($intConsecutivo,$strTabla){
            $this->intConsecutivo = $intConsecutivo;
            $this->strTabla = $strTabla;
            $sql = "SELECT * FROM $this->strTabla WHERE id = $this->intConsecutivo";
            $request = $this->select($sql);
            return $request;
        }
        public function updateStatus(int $intConsecutivo,string $estado,string $strTabla){
            $this->intConsecutivo = $intConsecutivo;
            $this->strTabla = $strTabla;
            $sql = "UPDATE $this->strTabla SET estado = '$estado' WHERE id = $this->intConsecutivo";
            $request = $this->update($sql,[$estado]);
            return $request;
        }
    }
?>
