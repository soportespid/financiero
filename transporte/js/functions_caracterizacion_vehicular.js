const URL ='transporte/controllers/CaracterizacionVehicularController.php';
var app = new Vue({
    el:"#myapp",
    data() {
        return {
            isLoading:false,
            intPageVal: "",
            txtConsecutivo:0,
            txtNombre:"",
            arrSearch:[],
            arrSearchCopy:[],
            txtSearch:"",
            txtResultados:0,
            arrConsecutivos:[],
            txtEstado:"",
            opcion:0,
            arrMarcas:[],
            selectMarca:"",
            arrOpciones:{
                1:"trans-caracterizacion-servicio-editar",
                2:"trans-caracterizacion-clase-editar",
                3:"trans-caracterizacion-combustible-editar",
                4:"trans-caracterizacion-cilindraje-editar",
                5:"trans-caracterizacion-publico-editar",
                6:"trans-caracterizacion-ocupantes-editar",
                7:"trans-caracterizacion-capacidad-editar",
                8:"trans-caracterizacion-potencia-editar",
                9:"trans-caracterizacion-marca-editar",
                10:"trans-caracterizacion-color-editar",
                11:"trans-caracterizacion-linea-editar",
                12:"trans-caracterizacion-carroceria-editar",
            }
        }
    },
    mounted() {
        const intPageVal = this.$refs.pageType.value;
        this.opcion = this.$refs.option.value;
        if(intPageVal == 2){
            this.getEdit();
        }else if(intPageVal == 3){
            this.getSearch();
        }
        if(this.opcion == 11){
            this.getData();
        }
    },
    methods: {
        getData:async function(){
            let formData = new FormData();
            formData.append("action","get");
            formData.append("opcion",this.opcion);
            const response = await fetch(URL,{method:"POST",body:formData});
            const objData = await response.json();
            this.arrMarcas = objData;
        },
        getEdit:async function(){
            if(this.opcion == 11){
                await this.getData();
            }
            const codigo = new URLSearchParams(window.location.search).get('id');
            let formData = new FormData();
            formData.append("action","edit");
            formData.append("codigo",codigo);
            formData.append("opcion",this.opcion);
            const response = await fetch(URL,{method:"POST",body:formData});
            const objData = await response.json();
            if(objData.status){
                this.txtConsecutivo = objData.data.id;
                this.txtNombre = objData.data.nombre;
                this.arrConsecutivos = objData.consecutivos;
                this.txtEstado = objData.data.estado;
                this.selectMarca = this.opcion == 11 ? objData.data.marca_id : "";
            }else{
                window.location.href=app.arrOpciones[app.opcion]+'?id='+objData.consecutivo;
            }
        },
        getSearch:async function(){
            const formData = new FormData();
            formData.append("action","search");
            formData.append("opcion",this.opcion);
            const response = await fetch(URL,{method:"POST",body:formData});
            const objData = await response.json();
            this.arrSearch = objData;
            this.arrSearchCopy = objData;
            this.txtResultados = this.arrSearchCopy.length;
        },
        save:async function(){
            if(this.txtNombre==""){
                Swal.fire("Atención!","El campo no puede estar vacio.","warning");
                return false;
            }
            if(this.opcion == 11){
                if(this.selectMarca==""){
                    Swal.fire("Atención!","Debe seleccionar la marca.","warning");
                    return false;
                }
            }
            Swal.fire({
                title:"¿Estás segur@ de guardar?",
                text:"",
                icon: 'warning',
                showCancelButton:true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText:"Sí, guardar",
                cancelButtonText:"No, cancelar"
            }).then(async function(result){
                if(result.isConfirmed){
                    const formData = new FormData();
                    formData.append("action","save");
                    formData.append("nombre",app.txtNombre);
                    formData.append("opcion",app.opcion);
                    formData.append("marca",app.selectMarca);
                    formData.append("consecutivo",app.txtConsecutivo);
                    app.isLoading = true;
                    const response = await fetch(URL,{method:"POST",body:formData});
                    const objData = await response.json();
                    app.isLoading = false;
                    if(objData.status){
                        Swal.fire("Guardado",objData.msg,"success");
                        if(objData.id && app.txtConsecutivo == 0){
                            setTimeout(function(){
                                window.location.href=app.arrOpciones[app.opcion]+'?id='+objData.id;
                            },1500);
                        }else{
                            app.getEdit();
                        }
                    }else{
                        Swal.fire("Error",objData.msg,"error");
                    }
                }
            });
        },
        search:function(){
            let search = this.txtSearch.toLowerCase();
            if(this.opcion == 11){
                this.arrSearchCopy = [...this.arrSearch.filter(e=>e.id.toLowerCase().includes(search) || e.nombre.toLowerCase().includes(search)
                    || e.marca.toLowerCase().includes(search))];
            }else{
                this.arrSearchCopy = [...this.arrSearch.filter(e=>e.id.toLowerCase().includes(search) || e.nombre.toLowerCase().includes(search))];
            }
            this.txtResultados = this.arrSearchCopy.length;
        },
        changeStatus:function(item){
            Swal.fire({
                title:"¿Estás segur@ de cambiar el estado?",
                text:"",
                icon: 'warning',
                showCancelButton:true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText:"Sí",
                cancelButtonText:"No, cancelar"
            }).then(async function(result){
                if(result.isConfirmed){
                    const formData = new FormData();
                    formData.append("action","change");
                    formData.append("id",item.id);
                    formData.append("estado",item.estado =='S' ? 'N' : 'S');
                    formData.append("opcion",app.opcion);
                    const response = await fetch(URL,{method:"POST",body:formData});
                    const objData = await response.json();
                    if(objData.status){
                        let index = app.arrSearch.findIndex(e => e.id == item.id);
                        app.arrSearch[index].estado = item.estado =='S' ? 'N' : 'S';
                        app.arrSearch[index].is_status = app.arrSearch[index].estado =='S' ? 1 : 0;
                        app.search();
                        Swal.fire("Estado actualizado","","success");
                    }else{
                        Swal.fire("Error",objData.msg,"error");
                    }
                }else{
                    app.search();
                }
            });

        },
        nextItem:function(type){
            let id = this.txtConsecutivo;
            let index = this.arrConsecutivos.findIndex(function(e){return e.id == id});
            if(type=="next" && app.arrConsecutivos[++index]){
                id = this.arrConsecutivos[index++].id;
            }else if(type=="prev" && app.arrConsecutivos[--index]){
                id = this.arrConsecutivos[index--].id;
            }
           window.location.href=app.arrOpciones[app.opcion]+'?id='+id;
        },
    },
})
