const URL = "transporte/controllers/ReciboImpuestoController.php";
const URLEXPORT ='transporte/controllers/ReciboImpuestoExportController.php';

var app = new Vue({
    el:"#myapp",
    data() {
        return {
            /* general */
            isLoading: false,
            tabs: '1',
            /* Paginación */
            arrBotones:[],
            paginaActual: 1,
            intInicioPagina:1,
            intTotalPaginas:1,
            intTotalBotones:1,
            selectPorPagina:25,
            /* Datos Basicos */
            fecha: new Date().toISOString().split("T")[0],
            fechaMin: '',
            liquidacion_id: '',
            placa: '',
            propietarioDocument: '',
            propietarioName: '',
            valorLiquidacion: 0,
            medioPago: 'banco',
            cuentaBancaria: '',
            cuentaContable: '',
            cuentaBancariaName: '',
            detalleRecaudo: '',
            /* Modal Liquidaciones */
            modalLiquidacion: false,
            arrLiquidaciones: [],
            txtSearchLiquidacion: '',
            /* Modal Cuentas Bancarias */
            modalCuentasBancarias: false,
            arrCuentasBancarias: [], arrCuentasBancariasCopy: [],
            txtSearchBanco: '',
            /* View Buscar */
            txtSearch: '',
            arrDataSearch: [],
            arrExport:[],
        }
    },
    mounted() {
        this.intPageVal = this.$refs.pageType.value;
        if(this.intPageVal == 1){
            this.getDataCreate();
        }else if(this.intPageVal == 2){
            this.getDataSearch();
        }else if(this.intPageVal == 3){
            this.getDataEdit();
        }
    },
    methods: {
        async getDataCreate() {
            let formData = new FormData();
            formData.append("action", "getDataCreate");
            this.isLoading = true;
            const response = await fetch(URL,{method:"POST",body:formData});
            const objData = await response.json();
            this.arrCuentasBancarias = this.arrCuentasBancariasCopy = objData.cuentas_bancarias;
            this.fechaMin = objData.fecha_min;
            this.isLoading = false;
        },

        async getDataSearch(paginaActual=1) {
            this.paginaActual = paginaActual;
            let formData = new FormData();
            formData.append("action", "getDataSearch");
            formData.append("txt_search", this.txtSearch);
            formData.append("limite_pagina", this.selectPorPagina);
            formData.append("pagina_actual", this.paginaActual);
            this.isLoading = true;
            const response = await fetch(URL,{method:"POST",body:formData});
            const objData = await response.json();
            this.arrDataSearch = objData.recaudos;
            this.intInicioPagina  = objData.start_page;
            this.intTotalBotones = objData.limit_page;
            this.intTotalPaginas = objData.total_pages;
            this.fechaMin = objData.fecha_min;
            this.getBotones();
            this.isLoading = false;
        },

        async getDataEdit() {
            this.id = new URLSearchParams(window.location.search).get('id');
            let formData = new FormData();
            formData.append("action", "getDataEdit");
            formData.append("id", this.id);
            this.isLoading = true;
            this.getDataCreate();
            const response = await fetch(URL,{method:"POST",body:formData});
            const objData = await response.json();
            if(objData.status){
                this.fecha = objData.data.fecha;
                this.liquidacion_id = objData.data.liquidacion_id;
                this.placa = objData.data.placa;
                this.propietarioDocument = objData.data.propietario;
                this.propietarioName = objData.data.nombre_propietario;
                this.valorLiquidacion = objData.data.valor_recaudo;
                this.medioPago = objData.data.medio_pago;
                this.cuentaBancaria = objData.data.cuenta_bancaria;
                this.cuentaBancariaName = objData.data.nombre_cuenta;
                this.detalleRecaudo = objData.data.detalle;
                this.arrExport=objData.data;
            }else{
                window.location.href='trans-recibo-impuesto-visualizar?id='+objData.consecutivo;
            }
            this.isLoading = false;
        },
        exportData:function(){
            const form = document.createElement("form");
            form.method ="post";
            form.target="_blank";
            form.action=URLEXPORT;

            function addField(name,value){
                const input = document.createElement("input");
                input.type="hidden";
                input.name=name;
                input.value = value;
                form.appendChild(input);
            }
            addField("action","pdf");
            addField("data",JSON.stringify(this.arrExport));
            document.body.appendChild(form);
            form.submit();
            document.body.removeChild(form);
        },

        async getLiquidaciones(paginaActual = 1) {
            this.paginaActual = paginaActual;
            let formData = new FormData();
            formData.append("action", "getDataLiquidaciones");
            formData.append("txt_search", this.txtSearchLiquidacion);
            formData.append("limite_pagina", this.selectPorPagina);
            formData.append("pagina_actual", this.paginaActual);
            this.isLoading = true;
            const response = await fetch(URL,{method:"POST",body:formData});
            const objData = await response.json();
            this.arrLiquidaciones = objData.liquidaciones;
            this.intInicioPagina  = objData.start_page;
            this.intTotalBotones = objData.limit_page;
            this.intTotalPaginas = objData.total_pages;
            this.getBotones();
            this.isLoading = false;
        },

        async deployModal(opcion) {
            if (opcion == "Liquidaciones") {
                await this.getLiquidaciones();
                this.modalLiquidacion = true;
            } else if (opcion == "CuentaBancaria") {
                this.modalCuentasBancarias = true;
            }
        },

        async arrayFilter(option) {
            if (option == "CuentaBancaria") {
                search = this.txtSearchBanco.toLowerCase();
                this.arrCuentasBancarias = [...this.arrCuentasBancariasCopy.filter(e=>e.cuenta.toLowerCase().includes(search) || e.nombre.toLowerCase().includes(search))];
            }
        },

        fillForm(option, item) {
            if (option == "Liquidaciones") {
                this.liquidacion_id = item.id;
                this.placa = item.placa;
                this.propietarioDocument = item.propietario;
                this.propietarioName = item.nombre;
                this.valorLiquidacion = item.total_pago;
                this.txtSearchLiquidacion = "";
                this.selectPorPagina = 25;
                this.paginaActual = 1;
                this.detalleRecaudo = "Recaudo de impuesto vehicular del automotor con placa " + this.placa;
                this.modalLiquidacion = false;
            } else if (option == "CuentaBancaria") {
                this.cuentaBancaria = item.cuenta_banco;
                this.cuentaBancariaName = item.nombre;
                this.cuentaContable = item.cuenta;
                this.txtSearchBanco = "";
                this.arrCuentasBancarias = this.arrCuentasBancariasCopy;
                this.modalCuentasBancarias = false;
            }
        },

        getBotones:function(){
            this.arrBotones = [];
            for (let i = this.intInicioPagina; i < this.intTotalBotones; i++) {
                this.arrBotones.push(i);
            }
        },

        async save() {
            if (this.fecha < this.fechaMin) {
                Swal.fire("Atención!","La fecha de recaudo que digistaste esta bloqueada","warning");
                return false;
            }

            if (this.fecha == "" || this.liquidacion_id == "" || this.placa == "" || this.valorLiquidacion == "" || this.medioPago == "" || this.cuentaBancaria == "" || this.cuentaBancariaName == "" || this.detalleRecaudo == "") {
                Swal.fire("Atención!","Todos los campos con (*) son obligatorios!","warning");
                return false;
            }

            const formData = new FormData();
            formData.append("action", "save");
            formData.append("fecha", this.fecha);
            formData.append("liquidacion_id", this.liquidacion_id);
            formData.append("medio_pago", this.medioPago);
            formData.append("cuenta_bancaria", this.cuentaBancaria);
            formData.append("cuenta_contable", this.cuentaContable);
            formData.append("valor", this.valorLiquidacion);
            formData.append("detalle", this.detalleRecaudo);

            Swal.fire({
                title:"¿Estás segur@ de guardar?",
                text:"",
                icon: 'warning',
                showCancelButton:true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText:"Sí, guardar",
                cancelButtonText:"No, cancelar"
            }).then(async function(result) {
                if(result.isConfirmed) {
                    const response = await fetch(URL,{method:"POST",body:formData});
                    const objData = await response.json();

                    if (objData.status) {
                        Swal.fire("Guardado",objData.msg,"success");
                        if(objData.id){
                            setTimeout(function(){
                                // window.location.href="trans-registro-automotor-editar"+'?id='+objData.id;
                                window.location.reload();
                            },1500);
                        }
                    }
                    else {
                        Swal.fire("Error!",objData.msg,"warning");
                    }
                }
            });
        },

        async anulaRecaudo(index, id) {
            const formData = new FormData();
            formData.append("action","anulaRecaudo");
            formData.append("id",id);

            Swal.fire({
                title:"¿Estás segur@ de anular el documento?",
                text:"",
                icon: 'warning',
                showCancelButton:true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText:"Sí, anular",
                cancelButtonText:"No, cancelar"
            }).then(async function(result) {
                if(result.isConfirmed) {
                    const response = await fetch(URL,{method:"POST",body:formData});
                    const objData = await response.json();
                    if (objData.status) {
                        Swal.fire("Anulado",objData.msg,"success");
                        app.arrDataSearch[index]["estado"] = "N";
                    }
                    else {
                        Swal.fire("Error!",objData.msg,"warning");
                    }
                }
            });
        },

        /* Formatos */
        formatFecha: function(fecha) {
            // Separar la fecha en componentes
            const parts = fecha.split('-');
            if (parts.length !== 3) {
                throw new Error('Formato de fecha inválido. Use Y-m-d.');
            }
            const year = parts[0];
            const month = parts[1];
            const day = parts[2];

            // Retornar la fecha en el formato deseado
            return `${day}/${month}/${year}`;
        },

        viewFormatNumber: function(valor){
            return new Intl.NumberFormat("en-US", {style: "currency", currency: "USD"}).format(valor);
        },

    },
    computed:{

    }
})
