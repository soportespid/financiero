const URL ='transporte/controllers/InteresesController.php';
var app = new Vue({
    el:"#myapp",
    data() {
        return {
            isLoading:false,
            intPagina:1,
            intInicioPagina:1,
            intTotalPaginas:1,
            intTotalBotones:1,
            intConsecutivo:0,
            intVigencia:new Date().getFullYear(),
            intPorcentajeM:"0",
            intPorcentajeC:"0",
            intMes:new Date().getMonth()+1,
            strBuscar:"",
            strEstado:"",
            strFecha:new Date().toISOString().split("T")[0],
            selectPorPagina:25,
            selectVigencia:"",
            arrData:[],
            arrVigencias:[],
            arrBotones:[],
            arrConsecutivos:[]
        }
    },
    mounted() {
        const intPageVal = this.$refs.pageType.value;
        if(intPageVal == 2){
            this.getEdit();
        }else if(intPageVal == 3){
            this.getSearch();
        }
    },
    methods: {
        save: async function(){
            if(this.intPorcentajeM == "" || this.intPorcentajeC == "" || this.intVigencia == "" || this.intMes == ""){
                Swal.fire("Atención!","Todos los campos con (*) son obligatorios.","warning");
                return false;
            }
            if(this.intPorcentajeM < 0 || this.intPorcentajeM > 100){
                Swal.fire("Atención!","El porcentaje moratorio no puede ser menor a 0 o mayor a 100.","warning");
                return false;
            }
            if(this.intPorcentajeC < 0 || this.intPorcentajeC > 100){
                Swal.fire("Atención!","El porcentaje corriente no puede ser menor a 0 o mayor a 100.","warning");
                return false;
            }
            Swal.fire({
                title:"¿Estás segur@ de guardar?",
                text:"",
                icon: 'warning',
                showCancelButton:true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText:"Sí, guardar",
                cancelButtonText:"No, cancelar"
            }).then(async function(result){
                if(result.isConfirmed){
                    const formData = new FormData();
                    formData.append("action","save");
                    formData.append("porcentaje_moratorio",app.intPorcentajeM);
                    formData.append("porcentaje_corriente",app.intPorcentajeC);
                    formData.append("mes",app.intMes);
                    formData.append("consecutivo",app.intConsecutivo);
                    formData.append("vigencia",app.intVigencia);
                    app.isLoading = true;
                    const response = await fetch(URL,{method:"POST",body:formData});
                    const objData = await response.json();
                    app.isLoading = false;
                    if(objData.status){
                        Swal.fire("Guardado",objData.msg,"success");
                        if(objData.id && app.txtConsecutivo == 0){
                            setTimeout(function(){
                                window.location.href='trans-intereses-editar?id='+objData.id;
                            },1500);
                        }else{
                            app.getEdit();
                        }
                    }else{
                        Swal.fire("Error",objData.msg,"error");
                    }
                }
            });
        },
        getData:async function (){
            const formData = new FormData();
            formData.append("action","get");
            const response = await fetch(URL,{method:"POST",body:formData});
            const objData = await response.json();
            this.arrVigencias = objData.vigencias;
        },
        getEdit:async function(){
            const codigo = new URLSearchParams(window.location.search).get('id');
            let formData = new FormData();
            formData.append("action","edit");
            formData.append("codigo",codigo);
            const response = await fetch(URL,{method:"POST",body:formData});
            const objData = await response.json();
            if(objData.status){
                this.intConsecutivo = objData.data.id;
                this.arrConsecutivos = objData.consecutivos;
                this.intPorcentajeM = objData.data.moratorio;
                this.intPorcentajeC =  objData.data.corriente;
                this.intMes = objData.data.mes;
                this.intVigencia = objData.data.vigencia;
            }else{
                window.location.href='trans-intereses-editar?id='+objData.consecutivo;
            }
        },
        getSearch:async function (intPagina=1){
            await this.getData();
            this.intPagina = intPagina;
            const formData = new FormData();
            formData.append("action","search");
            formData.append("paginas",this.selectPorPagina);
            formData.append("pagina",this.intPagina);
            formData.append("vigencia",this.selectVigencia);
            this.isLoading = true;
            const response = await fetch(URL,{method:"POST",body:formData});
            const objData = await response.json();
            this.isLoading = false;
            this.arrData = objData.data;
            this.intInicioPagina  = objData.start_page;
            this.intTotalBotones = objData.limit_page;
            this.intTotalPaginas = objData.total_pages;
            this.getBotones();
        },
        getBotones:function(){
            this.arrBotones = [];
            for (let i = this.intInicioPagina; i < this.intTotalBotones; i++) {
                this.arrBotones.push(i);
            }
        },
        nextItem:function(type){
            let id = this.intConsecutivo;
            let index = this.arrConsecutivos.findIndex(function(e){return e.id == id});
            if(type=="next" && app.arrConsecutivos[++index]){
                id = this.arrConsecutivos[index++].id;
            }else if(type=="prev" && app.arrConsecutivos[--index]){
                id = this.arrConsecutivos[index--].id;
            }
           window.location.href='trans-intereses-editar?id='+id;
        },
    },
})
