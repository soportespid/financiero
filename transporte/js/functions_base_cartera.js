const URL ='transporte/controllers/BaseCarteraController.php';
var app = new Vue({
    el:"#myapp",
    data() {
        return {
            isLoading:false,
            isModal:false,
            intPagina:1,
            intInicioPagina:1,
            intTotalPaginas:1,
            intTotalBotones:1,
            inputFile:"",
            intVigencia:new Date().getFullYear(),
            strBuscar:"",
            selectPeriodo:"",
            selectVigencia:"",
            selectPorPagina:25,
            arrData:[],
            arrPeriodos:[],
            arrVigencias:[],
            arrBotones:[],
            strPlaca:"",
        }
    },
    mounted() {
        this.getInitial();
    },
    methods: {
        getInitial:async function (){
            await this.getSearch();
            const formData = new FormData();
            formData.append("action","get");
            const response = await fetch(URL,{method:"POST",body:formData});
            const objData = await response.json();
            this.arrPeriodos = objData.periodos;
            this.arrVigencias = objData.vigencias;
        },
        getSearch:async function (intPagina=1){
            this.intPagina = intPagina;
            const formData = new FormData();
            formData.append("action","search");
            formData.append("paginas",this.selectPorPagina);
            formData.append("pagina",this.intPagina);
            formData.append("buscar",this.strBuscar);
            formData.append("placa",this.strPlaca);
            formData.append("periodo",this.selectPeriodo);
            formData.append("vigencia",this.selectVigencia);
            this.isLoading = true;
            const response = await fetch(URL,{method:"POST",body:formData});
            const objData = await response.json();
            this.isLoading = false;
            this.arrData = objData.data;
            this.intInicioPagina  = objData.start_page;
            this.intTotalBotones = objData.limit_page;
            this.intTotalPaginas = objData.total_pages;
            this.getBotones();
        },
        getPlantilla: async function(){
            const form = document.createElement("form");
            form.method ="post";
            form.target="_blank";
            form.action=URL;

            function addField(name,value){
                const input = document.createElement("input");
                input.type="hidden";
                input.name=name;
                input.value = value;
                form.appendChild(input);
            }

            addField("action","plantilla");
            document.body.appendChild(form);
            form.submit();
            document.body.removeChild(form);
        },
        getBotones:function(){
            this.arrBotones = [];
            for (let i = this.intInicioPagina; i < this.intTotalBotones; i++) {
                this.arrBotones.push(i);
            }
        },
        getFile:function(element){
            this.inputFile = element.event.target.files;
        },
        upload:async function(){
            let inputFile = this.inputFile;
            if(inputFile.length == 0){
                Swal.fire("Error","Debe subir el archivo.","error");
                return false;
            }
            if(this.intVigencia == "" || this.intVigencia <= 0){
                Swal.fire("Error","Debe ingresar una vigencia.","error");
                return false;
            }
            let file = inputFile[0];
            let arrNombre = file.name.split(".");
            let extension = arrNombre[arrNombre.length-1];
            if(extension != "xlsx"){
                Swal.fire("Error","El tipo de archivo es incorrecto.","error");
                return false;
            }

            let formData = new FormData();
            formData.append("action","upload");
            formData.append("file",file);
            formData.append("vigencia",this.intVigencia);
            Swal.fire({
                title:"¿Estás segur@ de cargar?",
                text:"Los datos guardados serán reemplazados por los nuevos.",
                icon: 'warning',
                showCancelButton:true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText:"Sí, guardar",
                cancelButtonText:"No, cancelar"
            }).then(async function(result){
                if(result.isConfirmed){
                    app.isLoading = true;
                    const response = await fetch(URL,{method:"POST",body:formData});
                    const objData = await response.json();
                    app.isLoading = false;
                    if(objData.status){
                        Swal.fire("Guardado",objData.msg,"success");
                        app.getSearch();
                        app.isModal = false;
                    }else{
                        Swal.fire("Error",objData.msg,"error");
                    }
                }
            });
        },
        formatMoney:function(valor){
            valor = new String(valor);
            // Separar la parte entera y decimal
            const [integerPart, decimalPart] = valor.split(",");

            // Formatear la parte entera
            const formattedInteger = integerPart.replace(/\B(?=(\d{3})+(?!\d))/g, ".");
            return decimalPart !== undefined
            ? `$${formattedInteger},${decimalPart}`
            : `$${formattedInteger}`;
        },
    },
})
