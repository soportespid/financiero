const URL ='transporte/controllers/DeclaracionImpuestoController.php';
const URLEXPORT ='transporte/controllers/DeclaracionImpuestoExportController.php';
var app = new Vue({
    el:"#myapp",
    data() {
        return {
            isLoading:false,
            isModal:false,
            isCheckAll:true,
            intPagina:1,
            intInicioPagina:1,
            intTotalPaginas:1,
            intTotalBotones:1,
            intConsecutivo:0,
            intVigencia:new Date().getFullYear(),
            intPorcentaje:"",
            strBuscar:"",
            strEstado:"",
            strFecha:new Date().toISOString().split("T")[0],
            strFechaInicial: new Date(new Date().getFullYear(), 0, 1).toISOString().split("T")[0],
            strFechaFinal: new Date().toISOString().split("T")[0],
            strCodigo:"",
            strPlaca:"",
            strDocumento:"",
            selectPorPagina:25,
            selectVigencia:"",
            arrVigencias:[],
            arrData:[],
            arrBotones:[],
            arrConsecutivos:[],
            arrImpuestos:[],
            objAuto:{
                id:"",
                placa:"",
                departamento:"",
                municipio:"",
                clase:"",
                marca:"",
                linea:"",
                modelo:"",
                combustible:"",
                cilindraje:"",
                carga:"",
                pasajeros:"",
                potencia:"",
                publico:"",
                carroceria:"",
                antiguo:"",
                blindaje:"",
                nombre:"",
                cc:"",
                telefono:"",
                departamento_tercero:"",
                municipio_tercero:"",
                direccion:"",
                celular:"",
                fecha:"",
            },
            objTotal : {
                "avaluo":0,
                "tarifa":0,
                "descuento":0,
                "sancion":0,
                "impuesto":0,
                "impuesto_descuento":0,
                "semaforizacion":0,
                "interes_mora":0,
                "dias_mora":0,
                "total":0
            },
            arrExport:[],
        }
    },
    mounted() {
        const intPageVal = this.$refs.pageType.value;
        if(intPageVal == 2){
            this.getEdit();
        }else if(intPageVal == 3){
            this.getSearch(1,"buscar");
        }else{
            this.getData();
            this.getSearch(1,"placas");
        }
    },
    methods: {
        exportData:function(){
            if(app.arrImpuestos.length == 0){
                Swal.fire("Atención!","No puede exportar sin datos.","warning");
                return false;
            }
            const form = document.createElement("form");
            form.method ="post";
            form.target="_blank";
            form.action=URLEXPORT;

            function addField(name,value){
                const input = document.createElement("input");
                input.type="hidden";
                input.name=name;
                input.value = value;
                form.appendChild(input);
            }
            addField("action","pdf");
            addField("data",JSON.stringify(this.arrExport));
            document.body.appendChild(form);
            form.submit();
            document.body.removeChild(form);
        },
        save: async function(){
            if(this.objAuto.placa == ""){
                Swal.fire("Atención!","Todos los campos con (*) son obligatorios.","warning");
                return false;
            }
            const impuestos = this.arrImpuestos.filter(function(e){return e.selected});
            if(impuestos.length == 0){
                Swal.fire("Atención!","Debe seleccionar al menos una vigencia","warning");
                return false;
            }
            Swal.fire({
                title:"¿Estás segur@ de guardar?",
                text:"",
                icon: 'warning',
                showCancelButton:true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText:"Sí, guardar",
                cancelButtonText:"No, cancelar"
            }).then(async function(result){
                if(result.isConfirmed){
                    const formData = new FormData();
                    formData.append("action","save");
                    formData.append("impuestos",JSON.stringify(impuestos));
                    formData.append("automotor",JSON.stringify(app.objAuto));
                    formData.append("totales",JSON.stringify(app.objTotal));
                    formData.append("fecha",app.strFecha);
                    formData.append("consecutivo",app.intConsecutivo);
                    app.isLoading = true;
                    const response = await fetch(URL,{method:"POST",body:formData});
                    const objData = await response.json();
                    app.isLoading = false;
                    if(objData.status){
                        Swal.fire("Guardado",objData.msg,"success");
                        if(objData.id){
                            setTimeout(function(){
                                window.location.href='trans-declaracion-impuesto-visualizar?id='+objData.id;
                            },1500);
                        }else{
                            app.getEdit();
                        }
                    }else{
                        Swal.fire("Error",objData.msg,"error");
                    }
                }
            });
        },
        getData: async function(){
            let formData = new FormData();
            formData.append("action","get");
            const response = await fetch(URL,{method:"POST",body:formData});
            const objData = await response.json();
            this.arrVigencias = objData.vigencias;
            this.selectVigencia = this.arrVigencias[0].nombre;
        },
        getEdit:async function(){
            const codigo = new URLSearchParams(window.location.search).get('id');
            let formData = new FormData();
            formData.append("action","edit");
            formData.append("codigo",codigo);
            this.isLoading = true;
            const response = await fetch(URL,{method:"POST",body:formData});
            const objData = await response.json();
            this.isLoading = false;
            if(objData.status){
                this.arrExport = objData.data;
                this.intConsecutivo = objData.data.id;
                this.arrConsecutivos = objData.consecutivos;
                this.strEstado = objData.data.estado;
                this.strFecha =  objData.data.fecha;
                this.arrImpuestos = objData.data.detalle;
                this.objAuto = objData.data.automotor;
                this.objTotal ={
                    avaluo: objData.data.total_avaluo,
                    impuesto: objData.data.total_impuesto,
                    impuesto_descuento: objData.data.total_descuento,
                    sancion: objData.data.total_sancion,
                    interes_mora: objData.data.total_interes_mora,
                    total:objData.data.total_pago,
                }
            }else{
                window.location.href='trans-declaracion-impuesto-visualizar?id='+objData.consecutivo;
            }
        },
        getSearch:async function (intPagina=1,tipoBusqueda=""){
            this.intPagina = intPagina;
            const formData = new FormData();
            formData.append("action","search");
            formData.append("paginas",this.selectPorPagina);
            formData.append("pagina",this.intPagina);
            formData.append("tipo",tipoBusqueda);
            formData.append("buscar",this.strBuscar);
            formData.append("fecha_inicial",this.strFechaInicial);
            formData.append("fecha_final",this.strFechaFinal);
            formData.append("codigo",this.strCodigo);
            formData.append("placa",this.strPlaca);
            formData.append("documento",this.strDocumento);
            this.isLoading = true;
            const response = await fetch(URL,{method:"POST",body:formData});
            const objData = await response.json();
            this.isLoading = false;
            this.arrData = objData.data;
            this.intInicioPagina  = objData.start_page;
            this.intTotalBotones = objData.limit_page;
            this.intTotalPaginas = objData.total_pages;
            this.getBotones();
        },
        getBotones:function(){
            this.arrBotones = [];
            for (let i = this.intInicioPagina; i < this.intTotalBotones; i++) {
                this.arrBotones.push(i);
            }
        },
        formatMoney:function(valor){
            valor = new String(valor);
            // Separar la parte entera y decimal
            const [integerPart, decimalPart] = valor.split(",");

            // Formatear la parte entera
            const formattedInteger = integerPart.replace(/\B(?=(\d{3})+(?!\d))/g, ".");
            return decimalPart !== undefined
            ? `$${formattedInteger},${decimalPart}`
            : `$${formattedInteger}`;
        },
        nextItem:function(type){
            let id = this.intConsecutivo;
            let index = this.arrConsecutivos.findIndex(function(e){return e.id == id});
            if(type=="next" && app.arrConsecutivos[++index]){
                id = this.arrConsecutivos[index++].id;
            }else if(type=="prev" && app.arrConsecutivos[--index]){
                id = this.arrConsecutivos[index--].id;
            }
           window.location.href='trans-declaracion-impuesto-visualizar?id='+id;
        },
        getImpuesto: async function(){
            if(this.objAuto.placa !="" && this.selectVigencia != ""){
                const formData = new FormData();
                formData.append("action","impuesto");
                formData.append("vigencia",this.selectVigencia);
                formData.append("fecha",this.strFecha);
                formData.append("data",JSON.stringify(this.objAuto));
                this.isLoading = true;
                const response = await fetch(URL,{method:"post",body:formData});
                const objData = await response.json();
                this.isLoading = false;
                if(objData.status){
                    this.arrImpuestos = objData.data;
                    this.getTotales();
                }else{
                    Swal.fire("Error",objData.msg,"error");
                }
            }
        },
        getTotales:function(){
            this.objTotal ={
                avaluo: 0,
                impuesto: 0,
                impuesto_descuento: 0,
                sancion: 0,
                interes_mora: 0,
                total:0,
            }
            this.arrImpuestos.forEach(e => {
                if(e.selected){
                    this.objTotal.avaluo += e.avaluo;
                    this.objTotal.impuesto += e.impuesto;
                    this.objTotal.impuesto_descuento += e.impuesto_descuento;
                    this.objTotal.sancion += e.sancion;
                    this.objTotal.interes_mora += e.interes_mora;
                    this.objTotal.total += e.total;
                }
            });
        },
        changeStatus:function(index){
            const vigencia = this.arrImpuestos[index];
            vigencia.selected = !vigencia.selected;
            this.arrImpuestos[index] = vigencia;
            const flag = this.arrImpuestos.every((e)=>e.selected == true);
            this.isCheckAll = flag;
            this.getTotales();
        },
        anular:function(item){
            Swal.fire({
                title:"¿Estás segur@ de anular?",
                text:"",
                icon: 'warning',
                showCancelButton:true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText:"Sí",
                cancelButtonText:"No, cancelar"
            }).then(async function(result){
                if(result.isConfirmed){
                    const formData = new FormData();
                    formData.append("action","anular");
                    formData.append("id",item.id);
                    const response = await fetch(URL,{method:"POST",body:formData});
                    const objData = await response.json();
                    if(objData.status){
                        app.getSearch(1,"buscar");
                        Swal.fire("Estado actualizado","","success");
                    }else{
                        Swal.fire("Error",objData.msg,"error");
                    }
                }else{
                    app.getSearch(1,"buscar");
                }
            });

        },
        changeAll:function(){
            this.arrImpuestos.forEach(e => {
                e.selected = this.isCheckAll
            });
            this.getTotales();
        },
        selectItem:function ({...data},type=""){
            if(type=="placas"){
                this.objAuto = data;
                this.getImpuesto();
                this.isModal = false;
            }
        }
    },
})
