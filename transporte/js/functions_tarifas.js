const URL ='transporte/controllers/TarifasController.php';
var app = new Vue({
    el:"#myapp",
    data() {
        return {
            isLoading:false,
            intPagina:1,
            intInicioPagina:1,
            intTotalPaginas:1,
            intTotalBotones:1,
            intConsecutivo:0,
            intTipo:1,
            intRangoDesde:"0",
            intRangoHasta:"0",
            intRangoPorcentaje:"0",
            intVigencia:new Date().getFullYear(),
            intLimite:1,
            intPorcentaje:0,
            strNombre:"",
            strBuscar:"",
            strEstado:"",
            selectPorPagina:25,
            arrData:[],
            arrTipos:[],
            arrBotones:[],
            arrConsecutivos:[],
            arrTiposElegidos:[],
            arrRangos:[],
        }
    },
    mounted() {
        const intPageVal = this.$refs.pageType.value;
        if(intPageVal == 2){
            this.getEdit();
        }else if(intPageVal == 3){
            this.getSearch();
        }else{
            this.getData();
        }
    },
    computed:{
        valorDesde:function() {
            return this.formatMoney(this.intRangoDesde)
        },
        valorHasta:function() {
            return this.formatMoney(this.intRangoHasta)
        },
    },
    methods: {
        save: async function(){
            const selectMultiple = document.querySelector("#selectMultiple1");
            if(selectMultiple.getAttribute("data-values")){
                this.arrTiposElegidos = selectMultiple.getAttribute("data-values").split(",");
                for (let i = 0; i < app.arrTiposElegidos.length; i++) {
                    const e = app.arrTiposElegidos[i];
                    const arr = e.split("-");
                    this.arrTiposElegidos[i] = arr[0];
                }
            }else{
                this.arrTiposElegidos = [];
            }
            if(this.strNombre=="" || this.intVigencia ==""){
                Swal.fire("Atención!","Todos los campos con (*) son obligatorios.","warning");
                return false;
            }
            if(this.intVigencia <=0){
                Swal.fire("Atención!","La vigencia no puede ser menor o igual a cero.","warning");
                return false;
            }
            if(app.arrTiposElegidos.length == 0){
                Swal.fire("Atención!","Debe seleccionar los tipos de vehiculos que aplica la tarifa","warning");
                return false;
            }

            if(this.intTipo == 2){
                if(app.arrRangos.length==0){
                    Swal.fire("Atención!","Debe agregar al menos un rango.","warning");
                    return false;
                }
            }else{
                if(this.intPorcentaje < 0 || this.intPorcentaje > 100){
                    Swal.fire("Atención!","El porcentaje no puede ser menor a 0 o mayor a 100.","warning");
                    return false;
                }
                if(this.intPorcentaje == ""){
                    Swal.fire("Atención!","El porcentaje no puede estar vacio.","warning");
                    return false;
                }
            }

            Swal.fire({
                title:"¿Estás segur@ de guardar?",
                text:"",
                icon: 'warning',
                showCancelButton:true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText:"Sí, guardar",
                cancelButtonText:"No, cancelar"
            }).then(async function(result){
                if(result.isConfirmed){
                    const formData = new FormData();
                    formData.append("action","save");
                    formData.append("porcentaje",app.intPorcentaje);
                    formData.append("vigencia",app.intVigencia);
                    formData.append("tipo",app.intTipo);
                    formData.append("nombre",app.strNombre);
                    formData.append("vehiculos",JSON.stringify(app.arrTiposElegidos));
                    formData.append("rangos",JSON.stringify(app.arrRangos));
                    formData.append("consecutivo",app.intConsecutivo);
                    app.isLoading = true;
                    const response = await fetch(URL,{method:"POST",body:formData});
                    const objData = await response.json();
                    app.isLoading = false;
                    if(objData.status){
                        Swal.fire("Guardado",objData.msg,"success");
                        if(objData.id && app.txtConsecutivo == 0){
                            setTimeout(function(){
                                window.location.href='trans-tarifa-editar?id='+objData.id;
                            },1500);
                        }else{
                            app.getEdit();
                        }
                    }else{
                        Swal.fire("Error",objData.msg,"error");
                    }
                }
            });
        },
        getData:async function (){
            const formData = new FormData();
            formData.append("action","get");
            const response = await fetch(URL,{method:"POST",body:formData});
            const objData = await response.json();
            this.arrTipos = objData.tipos;
        },
        getEdit:async function(){
            await this.getData();
            const codigo = new URLSearchParams(window.location.search).get('id');
            let formData = new FormData();
            formData.append("action","edit");
            formData.append("codigo",codigo);
            const response = await fetch(URL,{method:"POST",body:formData});
            const objData = await response.json();
            if(objData.status){
                const selectMultiple = document.querySelector("#selectMultiple1");
                this.intConsecutivo = objData.data.id;
                this.arrConsecutivos = objData.consecutivos;
                this.intTipo = objData.data.tipo;
                this.strNombre = objData.data.nombre;
                this.intPorcentaje =  objData.data.porcentaje;
                this.intVigencia = objData.data.vigencia;
                const tipos = objData.data.vehiculos;
                let tipoTemp = [];
                this.arrTipos.forEach(e => {
                    const flag = tipos.filter(function(f){return f.tipo == e.id}).length > 0 ? true : false;
                    e.is_checked = flag;
                    if(flag){
                        tipoTemp.push(e.id+"-"+e.nombre);
                    }
                });
                tipoTemp = tipoTemp.toString();
                selectMultiple.setAttribute("data-values",tipoTemp);
                selectMultiple.innerHTML = tipoTemp;
                if(this.intTipo == 2){
                    this.arrRangos = objData.data.detalle;
                }
            }else{
                window.location.href='trans-tarifa-editar?id='+objData.consecutivo;
            }
        },
        getSearch:async function (intPagina=1){
            await this.getData();
            this.intPagina = intPagina;
            const formData = new FormData();
            formData.append("action","search");
            formData.append("paginas",this.selectPorPagina);
            formData.append("pagina",this.intPagina);
            this.isLoading = true;
            const response = await fetch(URL,{method:"POST",body:formData});
            const objData = await response.json();
            this.isLoading = false;
            this.arrData = objData.data;
            this.intInicioPagina  = objData.start_page;
            this.intTotalBotones = objData.limit_page;
            this.intTotalPaginas = objData.total_pages;
            this.getBotones();
        },
        getBotones:function(){
            this.arrBotones = [];
            for (let i = this.intInicioPagina; i < this.intTotalBotones; i++) {
                this.arrBotones.push(i);
            }
        },
        nextItem:function(type){
            let id = this.intConsecutivo;
            let index = this.arrConsecutivos.findIndex(function(e){return e.id == id});
            if(type=="next" && app.arrConsecutivos[++index]){
                id = this.arrConsecutivos[index++].id;
            }else if(type=="prev" && app.arrConsecutivos[--index]){
                id = this.arrConsecutivos[index--].id;
            }
           window.location.href='trans-tarifa-editar?id='+id;
        },
        formatMoney:function(valor){
            valor = new String(valor);
            // Separar la parte entera y decimal
            const [integerPart, decimalPart] = valor.split(",");

            // Formatear la parte entera
            const formattedInteger = integerPart.replace(/\B(?=(\d{3})+(?!\d))/g, ".");
            return decimalPart !== undefined
            ? `$${formattedInteger},${decimalPart}`
            : `$${formattedInteger}`;
        },
        setValue:function(event,tipo){
            const input = event.target.value;
            const numericValue = input.replace(/[^0-9,]/g, "");
            const parts = numericValue.split(",");
            let valor = numericValue;
            if (parts.length > 2) {
                valor = parts[0] + "," + parts[1];
            } else {
                valor = numericValue;
            }
            if(tipo == "desde"){
                this.intRangoDesde = valor;
            }else{
                this.intRangoHasta = valor;
            }
        },
        addItem:function(){
            if(this.intRangoDesde =="" || this.intRangoPorcentaje == ""){
                Swal.fire("Atención!","Debe llenar todos los campos para agregar un rango","warning");
                return false;
            }
            if(this.intLimite == 2){
                if(this.intRangoHasta == ""){
                    Swal.fire("Atención!","Debe llenar todos los campos para agregar un rango","warning");
                    return false;
                }
            }
            this.arrRangos.push(
                {
                    desde:app.intRangoDesde,
                    limite:app.intLimite,
                    hasta:app.intRangoHasta,
                    porcentaje:app.intRangoPorcentaje,
                }
            );
            this.intRangoDesde ="0";
            this.intLimite = 1;
            this.intRangoHasta ="0";
            this.intRangoPorcentaje ="0";
        },
        delItem:function(index=-1){
            if(index >= 0){
                this.arrRangos.splice(index,1);
            }else{
                this.arrRangos = [];
            }
        },
    },
})
