const URL ='transporte/controllers/tramitesController.php';

var app = new Vue({
	el:"#myapp",
	data() {
		return {
			isLoading:false,
			isModal:false,
			isModalServicios:false,
			isModalBienes:false,
			isModalCuin:false,
			isModalFuente:false,
			isModalFuenteComp:false,
			selectTerceros1:2,
			selectTerceros2:2,
			selectTerceros3:2,
			selectSimple1:"C",
			selectSimple2:"C",
			selectSimple3:"C",
			selectConcepto1:"",
			selectConcepto2:"",
			selectConcepto3:"",
			selectDestino1:"",
			selectDestino2:"",
			selectDestino3:"",
			selectTipoIngreso1:"normal",
			selectTipoIngreso2:"normal",
			selectTipoIngreso3:"normal",
			selectContabilizar1:"S",
			selectContabilizar2:"S",
			selectContabilizar3:"S",
			selectIsPresupuesto1:2,
			selectIsPresupuesto2:2,
			selectIsPresupuesto3:2,
			selectSeccion1:"",
			selectSeccion2:"",
			selectSeccion3:"",
			intType1:0,
			intType2:0,
			intType3:0,
			intIdConsecutivo:0,
			intResults:0,
			intResultsServicios:0,
			intResultsBienes:0,
			intResultsCuin:0,
			intPorcentaje1:100,
			intPorcentaje2:100,
			intPorcentaje3:100,
			intResultsFuente:0,
			intPrecio1:0,
			intPrecio2:0,
			intPrecio3:0,
			intTotalPorcentaje1:0,
			intTotalPorcentaje2:0,
			intTotalPorcentaje3:0,
			strSearch:"",
			strNombre1:"",
			strNombre2:"",
			strNombre3:"",
			strBuscar:"",
			arrConceptos:[],
			arrModalCuentas:[],
			arrModalCuentasCopy:[],
			arrModalServicios:[],
			arrModalBienes:[],
			arrModalCuin:[],
			arrModalFuente:[],
			arrModalFuenteCopy:[],
			arrDetalleIngreso1:[],
			arrDetalleIngreso2:[],
			arrDetalleIngreso3:[],
			arrDetalleFuentes:[],
			arrSecciones:[],
			arrData:[],
			objCuenta1:{"codigo":"","nombre":""},
			objCuenta2:{"codigo":"","nombre":""},
			objCuenta3:{"codigo":"","nombre":""},
			objServicio1:{"codigo":"","nombre":""},
			objServicio2:{"codigo":"","nombre":""},
			objServicio3:{"codigo":"","nombre":""},
			objBienes1:{"codigo":"","nombre":""},
			objBienes2:{"codigo":"","nombre":""},
			objBienes3:{"codigo":"","nombre":""},
			objCuin1:{"codigo":"","nombre":""},
			objCuin2:{"codigo":"","nombre":""},
			objCuin3:{"codigo":"","nombre":""},
			objFuente1:{"codigo":"","nombre":""},
			objFuente2:{"codigo":"","nombre":""},
			objFuente3:{"codigo":"","nombre":""},
			objFuenteComp1:{"codigo":"","nombre":""},
			objFuenteComp2:{"codigo":"","nombre":""},
			objFuenteComp3:{"codigo":"","nombre":""},
			fuenteComp:"",
			intPagina:1,
			intInicioPagina:1,
			intTotalPaginas:1,
			intTotalBotones:1,
			selectPorPagina:25,
			tipoCuenta:1,
			txtResults:0,
		}
	},
	mounted() {
		const intPageVal = this.$refs.pageType.value;
		switch (intPageVal){
			case '2':
				this.getEdit();break;
			case '3':
				this.getSearch();break;
			default:
				this.getData();
		}
	},
	methods: {
		getData: async function(){
			let formData = new FormData();
			formData.append("action","get");
			formData.append("search",this.strSearch);
			this.isLoading = true;
			const response = await fetch(URL,{method:"POST",body:formData});
			const objData = await response.json();
			this.arrConceptos = objData.conceptos;
			this.arrSecciones = objData.secciones;
			this.arrModalCuentas = objData.modal_cuentas.data;
			this.arrModalCuentasCopy = objData.modal_cuentas.data;
			this.arrModalServicios = objData.modal_servicios.data;
			this.arrModalBienes = objData.modal_bienes.data;
			this.arrModalCuin = objData.modal_cuin.data;
			this.arrModalFuente = objData.modal_fuente.data;
			this.arrModalFuenteCopy = objData.modal_fuente.data;
			this.intResults = objData.modal_cuentas.results;
			this.intResultsServicios = objData.modal_servicios.results;
			this.intResultsBienes = objData.modal_bienes.results;
			this.intResultsCuin = objData.modal_cuin.results;
			this.intResultsFuente = objData.modal_fuente.results;
			this.selectConcepto1 = this.arrConceptos.length > 0 ? this.arrConceptos[0].codigo : "";
			this.selectConcepto2 = this.arrConceptos.length > 0 ? this.arrConceptos[0].codigo : "";
			this.selectConcepto3 = this.arrConceptos.length > 0 ? this.arrConceptos[0].codigo : "";
			this.selectSeccion1 = this.arrSecciones.length > 0 ? this.arrSecciones[0].codigo : "";
			this.selectSeccion2 = this.arrSecciones.length > 0 ? this.arrSecciones[0].codigo : "";
			this.selectSeccion3 = this.arrSecciones.length > 0 ? this.arrSecciones[0].codigo : "";
			this.isLoading = false;

		},
		getEdit: async function(){
			this.intIdCodigo = new URLSearchParams(window.location.search).get('id');
			this.intIdConsecutivo = this.intIdCodigo;
			let formData = new FormData();
			formData.append("action","edit");
			formData.append("codigo",this.intIdCodigo);
			this.isLoading = true;
			const response = await fetch(URL,{method:"POST",body:formData});
			const objData = await response.json();
			this.isLoading = false;
			if(objData.status){
				app.getData();
				const infoCabeceras = objData.infoCabeceras;
				const infoDetalles = objData.infoDetalles; console.log(infoDetalles);

				infoCabeceras.forEach((item) => {
					let i = item.numeral;
					this[`strNombre${i}`] = item.nombre;
					this[`intPrecio${i}`] = item.precio;
					this[`selectDestino${i}`] = item.destino;
					this[`selectTipoIngreso${i}`] = item.tipoIngreso;
				});

				infoDetalles.forEach((item) => {
					let i = item.numeral;
					let arrDetalleCuentas = this[`arrDetalleIngreso${i}`] || [];
					let concepto = {};
					concepto['codigo'] = item.concepto;
					concepto['nombre'] = item.nombre_concepto;

					arrDetalleCuentas.push({
						"cuenta":item.cuentapres,
						"nombre_cuenta":item.nombre,
						"concepto":concepto,
						"cuenta_clasificadora":'',
						"porcentaje":item.porcentaje,
						"id_concepto":item.concepto,
						"tipo_concepto":item.concepto,
						"fuente":item.fuente,
						"seccion":item.seccion,
						"nombre_seccion":item.nombre_seccion,
						"nombre_fuente":item.nombre_fuente
					});
					this[`arrDetalleIngreso${i}`] = arrDetalleCuentas;
					this.getTotalPorcentajeInicio(item.numeral);
				});
			}
			/* else{
				let consecutivo = objData.consecutivo;
				window.location.href='trans-recaudo-impuesto-editar?id='+consecutivo;
			} */

		},
		getSearch:async function (intPagina=1){
			this.intPagina = intPagina;
			const formData = new FormData();
			formData.append("action","search");
			formData.append("buscar",this.strBuscar);
			formData.append("paginas",this.selectPorPagina);
			formData.append("pagina",this.intPagina);
			this.isLoading = true;
			const response = await fetch(URL,{method:"POST",body:formData});
			const objData = await response.json();
			this.isLoading = false;
			this.arrData = objData.data;
			this.intInicioPagina  = objData.start_page;
			this.intTotalBotones = objData.limit_page;
			this.intTotalPaginas = objData.total_pages;
			this.getBotones();
		},
		getBotones:function(){
			this.arrBotones = [];
			for (let i = this.intInicioPagina; i < this.intTotalBotones; i++) {
				this.arrBotones.push(i);
			}
		},
		selectItem: async function(item,type){
			if(type == "cuenta"){
				this[`objCuenta${this.tipoCuenta}`].codigo = item.codigo;
				this[`objCuenta${this.tipoCuenta}`].nombre = item.nombre;
				let formData = new FormData();
				formData.append("action","clasificador");
				formData.append("cuenta",this[`objCuenta${this.tipoCuenta}`].codigo);
				this.isLoading = true;
				const response = await fetch(URL,{method:"POST",body:formData});
				this[`intType${this.tipoCuenta}`] = await response.json();
				this.strSearch = '';
				this.search('modal_cuenta');
				this.isLoading = false;
			}else if(type =="servicio"){
				this[`objServicio${this.tipoCuenta}`].codigo = item.codigo;
				this[`objServicio${this.tipoCuenta}`].nombre = item.nombre;
			}else if(type =="bienes"){
				this[`objBienes${this.tipoCuenta}`].codigo = item.codigo;
				this[`objBienes${this.tipoCuenta}`].nombre = item.nombre;
			}else if(type =="cuin"){
				this[`objCuin${this.tipoCuenta}`].codigo = item.codigo;
				this[`objCuin${this.tipoCuenta}`].nombre = item.nombre;
			}else if(type =="fuente"){
				this[`objFuente${this.tipoCuenta}`].codigo = item.codigo;
				this[`objFuente${this.tipoCuenta}`].nombre = item.nombre;
				this.strSearch = '';
				this.search('modal_fuente');
			}else if(type =="fuenteComp"){
				this[`objFuenteComp${this.tipoCuenta}`].codigo = item.codigo;
				this[`objFuenteComp${this.tipoCuenta}`].nombre = item.nombre;
				this.strSearch = '';
				this.search('modal_fuente');
			}
		},
		addFuente: function(){
			if(this.objFuente.codigo == ""){
				Swal.fire("Error","Debe elegir una fuente.","error");
				return false;
			}
			const fuente = this.objFuente;
			const arrFuentes = this.arrDetalleFuentes;
			const totalFuente = arrFuentes.length;
			if(totalFuente > 0){
				let flag = true;
				arrFuentes.forEach(element => {
					if(element.codigo == this.objFuente.codigo){
						flag = false;
					}
				});
				if(!flag){
					Swal.fire("Error","Esta fuente ya ha sido agregada, intente con otra.","error");
					return false;
				}
			}
			this.arrDetalleFuentes.push(fuente);
			this.objFuente = {"codigo":"","nombre":""}
		},
		delFuente: function(index=-1){
			if(index>=0){
				this.arrDetalleFuentes.splice(index,1);
			}else{
				this.arrDetalleFuentes = [];
			}
		},
		getTotalPorcentajeInicio:function(i){
			this[`intTotalPorcentaje${i}`] = 0;
			this[`arrDetalleIngreso${i}`].forEach(el=>{app[`intTotalPorcentaje${i}`]+=parseFloat(el.porcentaje)});
		},
		getTotalPorcentaje:function(){
			this[`intTotalPorcentaje${this.tipoCuenta}`] = 0;
			this[`arrDetalleIngreso${this.tipoCuenta}`].forEach(el=>{app[`intTotalPorcentaje${app.tipoCuenta}`]+=parseFloat(el.porcentaje)});
		},
		add: function(){
			let clasificador = "";
			let cuentaCodigo = this[`objCuenta${this.tipoCuenta}`].codigo;
			let cuentaNombre = this[`objCuenta${this.tipoCuenta}`].nombre;
			let concepto = this[`selectConcepto${this.tipoCuenta}`];
			let tipoNombre = "";
			let arrDetalleCuentas = this[`arrDetalleIngreso${this.tipoCuenta}`];
			let porcentaje = parseFloat(this[`intPorcentaje${this.tipoCuenta}`]);
			if(concepto == ""){
				Swal.fire("Error","Debe elegir un concepto contable","error");
				return false;
			}
			if(this[`selectTerceros${this.tipoCuenta}`] == 2){
				if(this[`intType${this.tipoCuenta}`] > 0){
					if(this[`intType${this.tipoCuenta}`] == 1){
						clasificador = this[`objCuin${this.tipoCuenta}`].codigo;
						tipoNombre = "CUIN";
					}else if(this[`intType${this.tipoCuenta}`] == 2){ 
						clasificador = this[`objBienes${this.tipoCuenta}`].codigo;
						tipoNombre = "Bienes transportables";
					}else if(this[`intType${this.tipoCuenta}`] == 3){
						clasificador = this[`objServicio${this.tipoCuenta}`].codigo;
						tipoNombre = "Servicio";
					}
					if(clasificador ==""){
						Swal.fire("Error","Debe seleccionar un "+tipoNombre,"error");
						return false;
					}
				}
				if(cuentaCodigo == ""){
					Swal.fire("Error","Debe elegir una cuenta presupuestal.","error");
					return false;
				}
			}
			let totalPorcentaje = porcentaje; 
			if(arrDetalleCuentas.length > 0){
				arrDetalleCuentas.forEach(el=>{totalPorcentaje+=parseFloat(el.porcentaje)});
				if(totalPorcentaje > 100){
					Swal.fire("Error","La suma de porcentajes no debe ser mayor a 100","error");
					return false;
				}
			}
			let objConcepto = this.arrConceptos.filter(function(e){return e.codigo == app[`selectConcepto${app.tipoCuenta}`]})[0];
			let objSeccion = JSON.parse(JSON.stringify(app.arrSecciones.filter(function(e){return e.codigo == app[`selectSeccion${app.tipoCuenta}`]})[0]));
			arrDetalleCuentas.push({
				"cuenta":this[`selectTerceros${this.tipoCuenta}`] == 2 ? cuentaCodigo : "",
				"nombre_cuenta":this[`selectTerceros${this.tipoCuenta}`] == 2 ? cuentaNombre : "",
				"concepto":objConcepto,
				"cuenta_clasificadora":clasificador,
				"porcentaje":porcentaje,
				"id_concepto":objConcepto.codigo,
				"tipo_concepto":this[`selectSimple${this.tipoCuenta}`],
				"fuente":this[`objFuenteComp${this.tipoCuenta}`].codigo,
				"seccion":objSeccion.codigo,
				"nombre_seccion":objSeccion.nombre,
				"nombre_fuente":this[`objFuenteComp${this.tipoCuenta}`].nombre
			});
			this[`arrDetalleIngreso${this.tipoCuenta}`] = arrDetalleCuentas;
			this.getTotalPorcentaje();
		},
		del: function(index=-1){
			if(index>=0){
				this[`arrDetalleIngreso${this.tipoCuenta}`].splice(index,1);
			}else{
				this[`arrDetalleIngreso${this.tipoCuenta}`] = [];
			}
			this.getTotalPorcentaje();
		},
		afectaPresupuesto:function(){
			const vueContext = this;
			if(this.selectIsPresupuesto1 == 1){
				Swal.fire({
					title:"¿Estás segur@ de que no afecta presupuesto?",
					text:"",
					icon: 'warning',
					showCancelButton:true,
					confirmButtonColor: '#3085d6',
					cancelButtonColor: '#d33',
					confirmButtonText:"Sí",
					cancelButtonText:"No"
				}).then(async function(result){
					if(!result.isConfirmed){
						vueContext.selectIsPresupuesto1 = 2;
					}
				});
			}
		},
		save:async function(){
			const camposValidos = [1, 2, 3].some(i => {
				const nombreValido = this[`strNombre${i}`] !== "";
				const detalleValido = Array.isArray(this[`arrDetalleIngreso${i}`]) && this[`arrDetalleIngreso${i}`].length > 0;
				return nombreValido && detalleValido;
			});
			
			if (!camposValidos) {
				Swal.fire("Error", "Al menos uno de los nombres de los componestes y su detalle deben estar diligenciado", "error");
				return false;
			}

			const objHeadArray = [];
			const objDetailArray = [];

			for (let i=1; i < 4; i++){
				const nombreValido = this[`strNombre${i}`] !== "";
				const detalleValido = Array.isArray(this[`arrDetalleIngreso${i}`]) && this[`arrDetalleIngreso${i}`].length > 0;
				if(nombreValido && detalleValido){
					
					let objHead = {
						"nombre":this[`strNombre${i}`],
						"precio":this[`intPrecio${i}`],
						"terceros":this[`selectTerceros${i}`],
						"tipo":this[`selectSimple${i}`],
						"destino":this[`selectDestino${i}`],
						"tipo_ingreso":this[`selectTipoIngreso${i}`],
						"contabilizar":this[`selectContabilizar${i}`],
						"modulo":14,
						"numeral":i
					};
					objHeadArray.push(objHead);

					const arrDetalleIngreso = this[`arrDetalleIngreso${i}`];
					arrDetalleIngreso.forEach((elemento) => {
						let objDetail = {
							"concepto":elemento.concepto.codigo,
							"modulo":14,
							"tipo_concepto":this[`selectSimple${i}`],
							"porcentaje":elemento.porcentaje,
							"seccion":elemento.seccion,
							"clasificador":elemento.cuenta_clasificadora,
							"cuentaPresu":elemento.cuenta,
							"fuentes":elemento.fuente,
							"numeral":i
						};
						objDetailArray.push(objDetail);
					});
				}
			}
			const vueContext = this;
			let formData = new FormData();
			formData.append("action","save");
			formData.append("opciones",this.intIdConsecutivo);
			formData.append("dataHead",JSON.stringify(objHeadArray));
			formData.append("dataDetail",JSON.stringify(objDetailArray));
			Swal.fire({
				title:"¿Estás segur@ de guardar?",
				text:"",
				icon: 'warning',
				showCancelButton:true,
				confirmButtonColor: '#3085d6',
				cancelButtonColor: '#d33',
				confirmButtonText:"Sí, guardar",
				cancelButtonText:"No, cancelar"
			}).then(async function(result){
				if(result.isConfirmed){
					app.isLoading = true;
					const response = await fetch(URL,{method:"POST",body:formData});
					const objData =  await response.json();
					app.isLoading = false;
					if(objData.status){
						if(objData.id && app.intIdConsecutivo == 0){
							Swal.fire("Guardado",objData.msg,"success");
							vueContext.isLoading = false;
							setTimeout(function(){
								window.location.href="trans-tramites-runt-editar.php?id="+objData.consecutivo;
							},1500);
						}else{
							app.getEdit();
						}
					}else{
						Swal.fire("Error",objData.msg,"error");
					}
				}
			});
		},
		formatNumero: function(valor){
			return new Intl.NumberFormat("en-US", {style: "currency", currency: "USD"}).format(valor);
		},
		validInteger:function(valor){
			valor = new String(valor);
			if(valor.includes(".") || valor.includes(",")){
				Swal.fire("Error","La cantidad debe ser un número entero.","error");
			}
		},
		showTab:function(tab){
			let tabs = this.$refs.rTabs.children;
			let tabsContent = this.$refs.rTabsContent.children;
			for (let i = 0; i < tabs.length; i++) {
				tabs[i].classList.remove("active");
				tabsContent[i].classList.remove("active")
			}
			tabs[tab-1].classList.add("active");
			tabsContent[tab-1].classList.add("active");
			this.tipoCuenta = tab;
		},
		nextItem:function(type){
			let id = this.intIdConsecutivo;
			let index = this.arrConsecutivos.findIndex(function(e){return e.id == id});
			if(type=="next" && app.arrConsecutivos[++index]){
				id = this.arrConsecutivos[index++].id;
			}else if(type=="prev" && app.arrConsecutivos[--index]){
				id = this.arrConsecutivos[index--].id;
			}
			window.location.href='trans-recaudo-impuesto-editar?id='+id;
		},
		search:function(type){
			let search;
			switch (type){
				case 'modal_cuenta':
					search = this.strSearch.toLowerCase();
					this.arrModalCuentasCopy = [...this.arrModalCuentas.filter(e=>e.nombre.toLowerCase().includes(search) || e.codigo.toLowerCase().includes(search))];
					//this.txtResults = this.arrModalCuentasCopy.length;
					break;
				case 'modal_fuente':
					search = this.strSearch.toLowerCase();
					this.arrModalFuenteCopy = [...this.arrModalFuente.filter(e=>e.nombre.toLowerCase().includes(search) || e.codigo.toLowerCase().includes(search))];
					//this.txtResults = this.arrModalFuenteCopy.length;
					break;
				case 'modal_servicio':
					
					break;
				case 'modal_bienes':
					
					break;
				case 'modal_cuin':
					
					break;
			}
			
		},
	},
	computed:{

	}
})
