const URL ='transporte/controllers/BaseGravableController.php';
var app = new Vue({
    el:"#myapp",
    data() {
        return {
            isLoading:false,
            isModal:false,
            intPagina:1,
            intInicioPagina:1,
            intTotalPaginas:1,
            intTotalBotones:1,
            inputFile:"",
            intVigencia:new Date().getFullYear(),
            strBuscar:"",
            selectTipo:"",
            selectClase:"",
            selectMarca:"",
            selectLinea:"",
            selectModelo:"",
            selectVigencia:"",
            selectPorPagina:25,
            arrData:[],
            arrTipos:[],
            arrClases:[],
            arrClasesCopy:[],
            arrMarcas:[],
            arrMarcasCopy:[],
            arrLineas:[],
            arrLineasCopy:[],
            arrModelos:[],
            arrVigencias:[],
            arrBotones:[],
        }
    },
    mounted() {
        this.getInitial();
    },
    methods: {
        getInitial:async function (){
            await this.getSearch();
            const formData = new FormData();
            formData.append("action","get");
            const response = await fetch(URL,{method:"POST",body:formData});
            const objData = await response.json();
            this.arrTipos = objData.tipos;
            this.arrClases = objData.clases;
            this.arrMarcas = objData.marcas;
            this.arrModelos = objData.modelos;
            this.arrLineas = objData.lineas;
            this.arrVigencias = objData.vigencias;
        },
        getSearch:async function (intPagina=1){
            this.intPagina = intPagina;
            const formData = new FormData();
            formData.append("action","search");
            formData.append("paginas",this.selectPorPagina);
            formData.append("pagina",this.intPagina);
            formData.append("buscar",this.strBuscar);
            formData.append("tipo",this.selectTipo);
            formData.append("clase",this.selectClase);
            formData.append("marca",this.selectMarca);
            formData.append("linea",this.selectLinea);
            formData.append("modelo",this.selectModelo);
            formData.append("vigencia",this.selectVigencia);
            this.isLoading = true;
            const response = await fetch(URL,{method:"POST",body:formData});
            const objData = await response.json();
            this.isLoading = false;
            this.arrData = objData.data;
            this.intInicioPagina  = objData.start_page;
            this.intTotalBotones = objData.limit_page;
            this.intTotalPaginas = objData.total_pages;
            this.getBotones();
        },
        getBotones:function(){
            this.arrBotones = [];
            for (let i = this.intInicioPagina; i < this.intTotalBotones; i++) {
                this.arrBotones.push(i);
            }
        },
        getFile:function(element){
            this.inputFile = element.event.target.files;
        },
        getPlantilla: async function(){
            const form = document.createElement("form");
            form.method ="post";
            form.target="_blank";
            form.action=URL;

            function addField(name,value){
                const input = document.createElement("input");
                input.type="hidden";
                input.name=name;
                input.value = value;
                form.appendChild(input);
            }

            addField("action","plantilla");
            document.body.appendChild(form);
            form.submit();
            document.body.removeChild(form);
        },
        changeFiltro:function(tipo="marca"){
            if(tipo=="marca"){
                this.selectLinea ="";
                this.arrLineasCopy = [...this.arrLineas.filter(function(e){return e.marca_id == app.selectMarca})];
            }else if(tipo=="clase"){
                this.selectMarca ="";
                this.selectLinea ="";
                this.arrLineasCopy = [];
                this.arrMarcasCopy = [...this.arrMarcas.filter(function(e){return e.clase_id == app.selectClase})];
            }else if(tipo=="tipo"){
                this.selectClase ="";
                this.selectMarca ="";
                this.selectLinea ="";
                this.arrLineasCopy = [];
                this.arrMarcasCopy = [];
                this.arrClasesCopy = [...this.arrClases.filter(function(e){return e.tipo_id == app.selectTipo})];
            }
        },
        upload:async function(){
            let inputFile = this.inputFile;
            if(inputFile.length == 0){
                Swal.fire("Error","Debe subir el archivo.","error");
                return false;
            }
            if(this.intVigencia == "" || this.intVigencia <= 0){
                Swal.fire("Error","Debe ingresar una vigencia.","error");
                return false;
            }
            let file = inputFile[0];
            let arrNombre = file.name.split(".");
            let extension = arrNombre[arrNombre.length-1];
            if(extension != "xlsx"){
                Swal.fire("Error","El tipo de archivo es incorrecto.","error");
                return false;
            }
            let formData = new FormData();
            formData.append("action","upload");
            formData.append("file",file);
            formData.append("vigencia",this.intVigencia);
            Swal.fire({
                title:"¿Estás segur@ de cargar?",
                text:"Los datos guardados serán reemplazados por los nuevos.",
                icon: 'warning',
                showCancelButton:true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText:"Sí, guardar",
                cancelButtonText:"No, cancelar"
            }).then(async function(result){
                if(result.isConfirmed){
                    app.isLoading = true;
                    const response = await fetch(URL,{method:"POST",body:formData});
                    const objData = await response.json();
                    app.isLoading = false;
                    if(objData.status){
                        Swal.fire("Guardado",objData.msg,"success");
                        await app.getInitial();
                        app.isModal = false;
                    }else{
                        Swal.fire("Error",objData.msg,"error");
                    }
                }
            });
        },
        formatMoney:function(valor){
            valor = new String(valor);
            // Separar la parte entera y decimal
            const [integerPart, decimalPart] = valor.split(",");

            // Formatear la parte entera
            const formattedInteger = integerPart.replace(/\B(?=(\d{3})+(?!\d))/g, ".");
            return decimalPart !== undefined
            ? `$${formattedInteger},${decimalPart}`
            : `$${formattedInteger}`;
        },
    },
})
