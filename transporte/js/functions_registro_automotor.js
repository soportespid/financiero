const URL = "transporte/controllers/RegistroAutomotorController.php";

var app = new Vue({
    el:"#myapp",
    data() {
        return {
            /* general */
            isLoading: false,
            tabs: '1',
            /* Datos Basicos */
            dateTramite: new Date().toISOString().split("T")[0],
            placa: '',
            departamentoCod: '',
            municipioCod: '',
            propietarioDocument: '',
            propietarioName: '',

            /* Caracterizacion de vehiculo */
            tipoId: '0',
            claseName: '',
            claseId: '',
            marcaName: '',
            marcaId: '',
            lineaName: '',
            lineaId: '',
            combustibleName: '',
            combustibleId: '',
            color: '',
            modelo: '',
            cilindrajeName: '',
            cilindrajeId: '',
            capacidad: 0,
            ocupantes: 0,
            potencia: 0,
            tipoPublicoName: '',
            tipoPublicoId: '',
            carroceriaId: '',
            carroceriaName: '',
            vehiculoAntiguo: 'N',
            blindaje: 'N',
            fechaBlindaje: '',
            desmontajeBlindaje: 'N',
            fechaDesmontaje: '',
            numeroMotor: '',
            numeroChasis: '',
            numeroSerie: '',
            numeroVin: '',
            servicioName: '',
            servicioId: '',
            empresaVinculadora: '',
            intAvaluo:0,

            /* Modal terceros */
            modalTerceros: false,
            arrTerceros: [], arrTercerosCopy: [],
            txtSearchTercero: '',

            /* Modal Caracterizacion */
            modalCaracterizacion: false,
            titleCaracterizacion: '',
            accionCaracterizacion: '',
            txtSearchCaracterizacion: '',
            arrCaracterizacion: [],

            /* Arrys caracterización */
            arrDepartamentos: [],
            arrMunicipios: [],
            arrTipos: [],
            arrClases: [],
            arrMarcas: [],
            arrLineas: [],
            arrCombustibles: [],
            arrCilindrajes: [],
            arrPublicos: [],
            arrCarrocerias: [],
            arrServicios: [],
            arrModelos: [],

            /* Array view search */
            arrDataSearch: [],
            arrBotones:[],
            txtSearch: '',
            intPagina:1,
            paginaActual: 1,
            intInicioPagina:1,
            intTotalPaginas:1,
            intTotalBotones:1,
            selectPorPagina:25,

            /* view edit */
            estado: '',
            id: 0,
        }
    },
    mounted() {
        this.intPageVal = this.$refs.pageType.value;
        if(this.intPageVal == 1){
            this.getDataCreate();
        }else if(this.intPageVal == 2){
            this.getDataSearch();
        }else if(this.intPageVal == 3){
            this.getDataEdit();
        }
    },
    methods: {
        async getDataCreate() {
            let formData = new FormData();
            formData.append("action", "getDataCreate");
            this.isLoading = true;
            const response = await fetch(URL,{method:"POST",body:formData});
            const objData = await response.json();
            this.arrDepartamentos = objData.departamentos;
            this.arrMunicipios = objData.municipios;
            this.arrTipos = objData.tipos;
            this.arrCombustibles = objData.combustibles;
            this.arrCilindrajes = objData.cilindrajes;
            this.arrPublicos = objData.tipo_publicos;
            this.arrCarrocerias = objData.carrocerias;
            this.arrServicios = objData.servicios;
            this.isLoading = false;
        },

        async getDataSearch(intPagina=1) {
            this.paginaActual = intPagina;
            let formData = new FormData();
            formData.append("action", "getDataSearch");
            formData.append("clase", this.claseId);
            formData.append("marca", this.marcaId);
            formData.append("linea", this.lineaId);
            formData.append("modelo", this.modelo);
            formData.append("txt_search", this.txtSearch);
            formData.append("limite_pagina", this.selectPorPagina);
            formData.append("pagina_actual", this.paginaActual);
            this.isLoading = true;
            const response = await fetch(URL,{method:"POST",body:formData});
            const objData = await response.json();
            this.arrDataSearch = objData.automotores;
            this.arrClases = objData.clases;
            this.arrMarcas = objData.marcas;
            this.arrLineas = objData.lineas;
            this.arrModelos = objData.modelos;
            this.intInicioPagina  = objData.start_page;
            this.intTotalBotones = objData.limit_page;
            this.intTotalPaginas = objData.total_pages;
            this.getBotones();
            this.isLoading = false;
        },

        async getDataEdit() {
            this.id = new URLSearchParams(window.location.search).get('id');
            let formData = new FormData();
            formData.append("action", "getDataEdit");
            formData.append("id", this.id);
            this.isLoading = true;
            this.getDataCreate();
            const response = await fetch(URL,{method:"POST",body:formData});
            const objData = await response.json();
            this.dateTramite = objData.data.fecha_tramite;
            this.placa = objData.data.placa;
            this.departamentoCod = objData.data.dpto;
            this.municipioCod = objData.data.mnpio;
            this.propietarioDocument = objData.data.propietario;
            this.propietarioName = objData.data.nombre;
            this.tipoId = objData.data.tipo_id;
            this.claseId = objData.data.clase_id;
            this.claseName = objData.data.nombre_clase;
            this.marcaId = objData.data.marca_id;
            this.marcaName = objData.data.nombre_marca;
            this.lineaId = objData.data.linea_id;
            this.lineaName = objData.data.nombre_linea;
            this.combustibleId = objData.data.combustible_id;
            this.combustibleName = objData.data.nombre_combustible;
            this.color = objData.data.color;
            this.modelo = objData.data.modelo;
            this.cilindrajeId = objData.data.cilindraje_id;
            this.cilindrajeName = objData.data.nombre_cilindraje;
            this.capacidad = objData.data.capacidad_carga;
            this.ocupantes = objData.data.cantidad_pasajeros;
            this.potencia = objData.data.potencia_hp;
            this.tipoPublicoId = objData.data.publico_id;
            this.tipoPublicoName = objData.data.nombre_publico;
            this.carroceriaId = objData.data.carroceria_id;
            this.carroceriaName = objData.data.nombre_carroceria;
            this.vehiculoAntiguo = objData.data.vehiculo_antiguo;
            this.blindaje = objData.data.blindaje;
            this.fechaBlindaje = objData.data.fecha_blindaje;
            this.desmontajeBlindaje = objData.data.desmonte_blindaje;
            this.fechaDesmontaje = objData.data.fecha_desmonte;
            this.numeroMotor = objData.data.numero_motor;
            this.numeroChasis = objData.data.numero_chasis;
            this.numeroSerie = objData.data.numero_serie;
            this.numeroVin = objData.data.numero_vin;
            this.servicioId = objData.data.tipo_servicio_id;
            this.estado = objData.data.estado;
            this.intAvaluo = objData.data.avaluo;
        },

        getBotones:function(){
            this.arrBotones = [];
            for (let i = this.intInicioPagina; i < this.intTotalBotones; i++) {
                this.arrBotones.push(i);
            }
        },

        async getDataMarcas(claseId) {
            let formData = new FormData();
            formData.append("action", "getMarcas");
            formData.append("clase_id", claseId);
            const response = await fetch(URL,{method:"POST",body:formData});
            const objData = await response.json();
            this.arrMarcas = objData.marcas;
        },

        async getDataLineas(marcaId) {
            let formData = new FormData();
            formData.append("action", "getLineas");
            formData.append("marcaId", marcaId);
            const response = await fetch(URL,{method:"POST",body:formData});
            const objData = await response.json();
            this.arrLineas = objData.lineas;
        },

        async getDataClases(tipoId) {
            let formData = new FormData();
            formData.append("action", "getClases");
            formData.append("tipo_id", tipoId);
            const response = await fetch(URL,{method:"POST",body:formData});
            const objData = await response.json();
            this.arrClases = objData.clases;
        },

        async getTerceros(txtSearch) {
            if (txtSearch == "") {
                return false;
            }

            let formData = new FormData();
            formData.append("action", "getTerceros");
            formData.append("txt_search", txtSearch);
            this.isLoading = true;
            const response = await fetch(URL,{method:"POST",body:formData});
            const objData = await response.json();
            this.arrTerceros = objData.terceros;
            this.isLoading = false;
        },

        async checkPlaca() {
            let formData = new FormData();
            formData.append("action", "checkPlaca");
            formData.append("placa", this.placa);
            const response = await fetch(URL,{method:"POST",body:formData});
            const objData = await response.json();
            if (objData.status == true) {
                Swal.fire("Atención!","Esta placa ya ha sido creada en el sistema","warning");
                this.placa = "";
                return false;
            }
        },

        async desplegarModal(option) {
            if (option == "Clases") {
                if (this.tipoId == "") {
                    Swal.fire("Atención!","Debe seleccionar primero el tipo","warning");
                    return false;
                }
                await this.getDataClases(this.tipoId);
                this.arrCaracterizacion = this.arrClases;
            } else if (option == "Lineas") {
                if (this.marcaId == "") {
                    Swal.fire("Atención!","Debe seleccionar primero la Marca","warning");
                    return false;
                }
                await this.getDataLineas(this.marcaId);
                this.arrCaracterizacion = this.arrLineas;
            } else if (option == "Marcas") {
                if (this.claseId == "") {
                    Swal.fire("Atención!","Debe seleccionar primero la Clase","warning");
                    return false;
                }
                await this.getDataMarcas(this.claseId);
                this.arrCaracterizacion = this.arrMarcas;
            } else if (option == "Combustibles") {
                this.arrCaracterizacion = this.arrCombustibles;
            } else if (option == "Tipo Publicos") {
                this.arrCaracterizacion = this.arrPublicos;
            } else if (option == "Carrocerias") {
                this.arrCaracterizacion = this.arrCarrocerias;
            }

            this.titleCaracterizacion = option;
            this.accionCaracterizacion = option;
            this.modalCaracterizacion = true;
        },

        async filterArray(option) {
            let search = "";
            let data = [];

            switch (option) {
                case "inputTercero":
                    search = this.propietarioDocument.toLowerCase();
                    await this.getTerceros(search);
                    if (this.arrTerceros.length == 0 || this.arrTerceros.length > 1) {
                        Swal.fire("Atención!","Documento de propietario no encontrado","warning");
                        this.propietarioDocument = this.propietarioName = "";
                        return false;
                    }

                    this.fillForm("tercero", this.arrTerceros[0]);
                    break;

                case "Clases":
                    search = this.txtSearchCaracterizacion.toLowerCase();
                    this.arrCaracterizacion = [...this.arrClases.filter(e=>e.nombre.toLowerCase().includes(search))];
                    break;

                case "Marcas":
                    if (this.txtSearchCaracterizacion != "") {
                        search = this.txtSearchCaracterizacion.toLowerCase();
                        this.arrCaracterizacion = [...this.arrMarcas.filter(e=>e.nombre.toLowerCase().includes(search))];
                    } else {
                        this.txtSearchCaracterizacion = [];
                    }
                    break;

                case "Lineas":
                    search = this.txtSearchCaracterizacion.toLowerCase();
                    this.arrCaracterizacion = [...this.arrLineas.filter(e=>e.nombre.toLowerCase().includes(search))];
                    break;

                case "Combustibles":
                    search = this.txtSearchCaracterizacion.toLowerCase();
                    this.arrCaracterizacion = [...this.arrCombustibles.filter(e=>e.nombre.toLowerCase().includes(search))];
                    break;

                case "Cilindrajes":
                    if (this.txtSearchCaracterizacion != "")  {
                        search = this.txtSearchCaracterizacion.toLowerCase();
                        this.arrCaracterizacion = [...this.arrCilindrajes.filter(e=>e.nombre.toLowerCase().includes(search))];
                    } else {
                        this.arrCaracterizacion = [];
                    }
                    break;

                case "Tipo Publicos":
                    search = this.txtSearchCaracterizacion.toLowerCase();
                    this.arrCaracterizacion = [...this.arrPublicos.filter(e=>e.nombre.toLowerCase().includes(search))];
                    break;

                case "Carrocerias":
                    search = this.txtSearchCaracterizacion.toLowerCase();
                    this.arrCaracterizacion = [...this.arrCarrocerias.filter(e=>e.nombre.toLowerCase().includes(search))];
                    break;

                default:
                    console.log("Error, opción no encontrada");
                    break;
            }
        },

        fillForm(option, item) {
            switch (option) {
                case "tercero":
                    this.propietarioDocument = item.cedulanit;
                    this.propietarioName = item.nombre;
                    this.txtSearchTercero = "";
                    this.modalTerceros = false;
                    this.arrTerceros = [];
                    break;

                case "Clases":
                    this.claseId = item.id;
                    this.claseName = item.nombre;
                    this.txtSearchCaracterizacion = this.marcaId = this.marcaName = this.lineaId = this.lineaName = "";
                    this.modalCaracterizacion = false;
                    this.arrCaracterizacion = [];
                    break;

                case "Marcas":
                    this.marcaId = item.id;
                    this.marcaName = item.nombre;
                    this.txtSearchCaracterizacion = this.lineaId = this.lineaName = "";
                    this.modalCaracterizacion = false;
                    this.arrCaracterizacion = [];
                    break;

                case "Lineas":
                    this.lineaId = item.id;
                    this.lineaName = item.nombre;
                    this.txtSearchCaracterizacion = "";
                    this.modalCaracterizacion = false;
                    this.arrCaracterizacion = [];
                    break;

                case "Combustibles":
                    this.combustibleId = item.id;
                    this.combustibleName = item.nombre;
                    this.txtSearchCaracterizacion = "";
                    this.modalCaracterizacion = false;
                    this.arrCaracterizacion = [];
                    break;

                case "Cilindrajes":
                    this.cilindrajeId = item.id;
                    this.cilindrajeName = item.nombre;
                    this.txtSearchCaracterizacion = "";
                    this.modalCaracterizacion = false;
                    this.arrCaracterizacion = [];
                    break;

                case "Tipo Publicos":
                    this.tipoPublicoId = item.id;
                    this.tipoPublicoName = item.nombre;
                    this.txtSearchCaracterizacion = "";
                    this.modalCaracterizacion = false;
                    this.arrCaracterizacion = [];
                    break;

                case "Carrocerias":
                    this.carroceriaId = item.id;
                    this.carroceriaName = item.nombre;
                    this.txtSearchCaracterizacion = "";
                    this.modalCaracterizacion = false;
                    this.arrCaracterizacion = [];
                    break;

                default:
                    break;
            }
        },

        async save () {
            /* Validacion datos basicos */
            if (this.dateTramite == "" || this.placa == "" || this.departamentoCod == "" || this.municipioCod == "" || this.propietarioDocument == "" || this.propietarioName == "") {
                Swal.fire("Atención!","Todos los campos con (*) son obligatorios en Datos basicos!","warning");
                return false;
            }

            if (this.tipoId == "" || this.claseId == "" || this.marcaId == "" || this.lineaId == "" || this.combustibleName == "" || this.color == "" || this.modelo == "" || this.cilindrajeId == "" || this.capacidad < 0 || this.ocupantes < 0 || this.potencia < 0 || this.carroceriaId == "" || this.vehiculoAntiguo == "" || this.blindaje == "" || this.desmontajeBlindaje == "") {
                Swal.fire("Atención!","Todos los campos con (*) son obligatorios en Caracterización!","warning");
                return false;
            }

            if (this.numeroMotor == "") {
                Swal.fire("Atención!","Todos los campos con (*) son obligatorios en Identificación interna del vehiculo!","warning");
                return false;
            }

            if (this.servicioId == "") {
                Swal.fire("Atención!","Todos los campos con (*) son obligatorios en Tipo de uso!","warning");
                return false;
            }

            const formData = new FormData();
            formData.append("action", "save");
            formData.append("fecha_tramite", this.dateTramite);
            formData.append("placa", this.placa);
            formData.append("departamento_cod", this.departamentoCod);
            formData.append("municipio_cod", this.municipioCod);
            formData.append("propietario_document", this.propietarioDocument);
            formData.append("tipo_id", this.tipoId);
            formData.append("clase_id", this.claseId);
            formData.append("marca_id", this.marcaId);
            formData.append("linea_id", this.lineaId);
            formData.append("combustible_id", this.combustibleId);
            formData.append("color", this.color);
            formData.append("modelo", this.modelo);
            formData.append("cilindraje_id", this.cilindrajeId);
            formData.append("capacidad_carga", this.capacidad);
            formData.append("ocupantes", this.ocupantes);
            formData.append("potencia_hp", this.potencia);
            formData.append("tipo_publico_id", this.tipoPublicoId);
            formData.append("carroceria_id", this.carroceriaId);
            formData.append("vehiculo_antiguo", this.vehiculoAntiguo);
            formData.append("blindaje", this.blindaje);
            formData.append("fecha_blindaje", this.fechaBlindaje);
            formData.append("desmonte_blindaje", this.desmontajeBlindaje);
            formData.append("fecha_desmonte", this.fechaDesmontaje);
            formData.append("numero_motor", this.numeroMotor);
            formData.append("numero_chasis", this.numeroChasis);
            formData.append("numero_serie", this.numeroSerie);
            formData.append("numero_vin", this.numeroVin);
            formData.append("servicio_id", this.servicioId);
            formData.append("empresa_vinculadora", this.empresaVinculadora);
            formData.append("avaluo", this.intAvaluo);


            Swal.fire({
                title:"¿Estás segur@ de guardar?",
                text:"",
                icon: 'warning',
                showCancelButton:true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText:"Sí, guardar",
                cancelButtonText:"No, cancelar"
            }).then(async function(result) {
                if(result.isConfirmed) {
                    const response = await fetch(URL,{method:"POST",body:formData});
                    const objData = await response.json();

                    if (objData.status) {
                        Swal.fire("Guardado",objData.msg,"success");
                        if(objData.id){
                            setTimeout(function(){
                                window.location.href="trans-registro-automotor-editar"+'?id='+objData.id;
                            },1500);
                        }
                    }
                    else {
                        Swal.fire("Error!",objData.msg,"warning");
                    }
                }
            });
        },

        async saveEdit() {
            /* Validacion datos basicos */
            if (this.dateTramite == "" || this.placa == "" || this.departamentoCod == "" || this.municipioCod == "" || this.propietarioDocument == "" || this.propietarioName == "") {
                Swal.fire("Atención!","Todos los campos con (*) son obligatorios en Datos basicos!","warning");
                return false;
            }

            if (this.tipoId == "" || this.claseId == "" || this.marcaId == "" || this.lineaId == "" || this.combustibleName == "" || this.color == "" || this.modelo == "" || this.cilindrajeId == "" || this.capacidad < 0 || this.ocupantes < 0 || this.potencia < 0 || this.carroceriaId == "" || this.vehiculoAntiguo == "" || this.blindaje == "" || this.desmontajeBlindaje == "") {
                Swal.fire("Atención!","Todos los campos con (*) son obligatorios en Caracterización!","warning");
                return false;
            }

            if (this.numeroMotor == "") {
                Swal.fire("Atención!","Todos los campos con (*) son obligatorios en Identificación interna del vehiculo!","warning");
                return false;
            }

            if (this.servicioId == "") {
                Swal.fire("Atención!","Todos los campos con (*) son obligatorios en Tipo de uso!","warning");
                return false;
            }

            const formData = new FormData();
            formData.append("action", "saveEdit");
            formData.append("id", this.id);
            formData.append("fecha_tramite", this.dateTramite);
            formData.append("placa", this.placa);
            formData.append("departamento_cod", this.departamentoCod);
            formData.append("municipio_cod", this.municipioCod);
            formData.append("propietario_document", this.propietarioDocument);
            formData.append("tipo_id", this.tipoId);
            formData.append("clase_id", this.claseId);
            formData.append("marca_id", this.marcaId);
            formData.append("linea_id", this.lineaId);
            formData.append("combustible_id", this.combustibleId);
            formData.append("color", this.color);
            formData.append("modelo", this.modelo);
            formData.append("cilindraje_id", this.cilindrajeId);
            formData.append("capacidad_carga", this.capacidad);
            formData.append("ocupantes", this.ocupantes);
            formData.append("potencia_hp", this.potencia);
            formData.append("tipo_publico_id", this.tipoPublicoId);
            formData.append("carroceria_id", this.carroceriaId);
            formData.append("vehiculo_antiguo", this.vehiculoAntiguo);
            formData.append("blindaje", this.blindaje);
            formData.append("fecha_blindaje", this.fechaBlindaje);
            formData.append("desmonte_blindaje", this.desmontajeBlindaje);
            formData.append("fecha_desmonte", this.fechaDesmontaje);
            formData.append("numero_motor", this.numeroMotor);
            formData.append("numero_chasis", this.numeroChasis);
            formData.append("numero_serie", this.numeroSerie);
            formData.append("numero_vin", this.numeroVin);
            formData.append("servicio_id", this.servicioId);
            formData.append("empresa_vinculadora", this.empresaVinculadora);
            formData.append("avaluo", this.intAvaluo);

            Swal.fire({
                title:"¿Estás segur@ de actualizar?",
                text:"",
                icon: 'warning',
                showCancelButton:true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText:"Sí, actualizar",
                cancelButtonText:"No, cancelar"
            }).then(async function(result) {
                if(result.isConfirmed) {
                    const response = await fetch(URL,{method:"POST",body:formData});
                    const objData = await response.json();

                    if (objData.status) {
                        Swal.fire("Guardado",objData.msg,"success");
                        if(objData.id){
                            setTimeout(function(){
                                window.location.reload();
                            },1000);
                        }
                    }
                    else {
                        Swal.fire("Error!",objData.msg,"warning");
                    }
                }
            });
        },

        clearForm() {
            this.claseId = this.claseName = this.marcaId = this.marcaName = this.lineaId = this.lineaName = "";
        },

        /* Formatos */
        formatFecha: function(fecha) {
            // Separar la fecha en componentes
            const parts = fecha.split('-');
            if (parts.length !== 3) {
                throw new Error('Formato de fecha inválido. Use Y-m-d.');
            }
            const year = parts[0];
            const month = parts[1];
            const day = parts[2];

            // Retornar la fecha en el formato deseado
            return `${day}/${month}/${year}`;
        },
    },
    computed:{

    }
})
