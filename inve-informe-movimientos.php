<?php

    header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=utf8");
	require 'comun.inc';
	require 'funciones.inc';
	session_start();
    if(empty($_SESSION)){
        header("location: index.php");
    }
	$linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");
	cargarcodigopag($_GET['codpag'],$_SESSION["nivel"]);
	date_default_timezone_set("America/Bogota");
?>
<!DOCTYPE >
<html lang=es>
	<head>
        <meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1"/>
		<meta name="viewport" content="user-scalable=no">
		<title>:: IDEAL 10</title>
		<link href="favicon.ico" rel="shortcut icon"/>
		<link href="css/css2z.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
		<link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
		<link href="css/css4.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
        <link href="css/style.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css"/>
        <link rel="stylesheet" href="sweetalert2/dist/sweetalert2.min.css">
        <script src="sweetalert2/dist/sweetalert2.min.js"></script>
		<script src="css/programas.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
        <style>
            [v-cloak]{
                display : none;
            }
        </style>
	</head>
	<body>
		<header>
			<table>
				<tr>
                    <script>barra_imagenes("inve");</script><?php cuadro_titulos();?>
                </tr>
			</table>
		</header>

        <section id="myapp" v-cloak>
            <!-- variables -->
            <!-- <input type="hidden" value = "1" ref="pageType"> -->

            <!-- loading -->
            <div class="loading-container" v-show="isLoading" >
                <p class="text-loading"data-loading="IDEAL 10">IDEAL 10</p>
            </div>

            <nav>
                <table>
                    <tr><?php menu_desplegable("inve");?></tr>
                </table>
                <div class="bg-white group-btn p-1" id="newNavStyle">
                    <button type="button" class="btn btn-white btn-primary-hover d-flex justify-between align-items-center" @click="mypop=window.open('inve-principal','',''); mypop.focus();">
                        <span>Nueva ventana</span>
                        <svg class="fill-black group-hover:fill-white w-5 h-5" viewBox="0 -960 960 960" ><path d="M320-120v-80h80v-80H160q-33 0-56.5-23.5T80-360v-400q0-33 23.5-56.5T160-840h640q33 0 56.5 23.5T880-760v400q0 33-23.5 56.5T800-280H560v80h80v80H320ZM160-360h640v-400H160v400Zm0 0v-400 400Z"/></svg>
                    </button>
                </div>
            </nav>
            <article class="bg-white">
                <div>
                    <div>
                        <h2 class="titulos m-0">Informe de movimientos de almacen</h2>
                        
                        <div class="d-flex w-50">
                            <div class="form-control">
                                <label class="form-label m-0" for="">Fecha inicial<span class="text-danger fw-bolder">*</span></label>
                                <input type="date" class="text-center" v-model="fechaIni">
                            </div>

                            <div class="form-control">
                                <label class="form-label m-0" for="">Fecha final<span class="text-danger fw-bolder">*</span></label>
                                <input type="date" class="text-center" v-model="fechaFin">
                            </div> 

                            <div class="form-control justify-between text-center w-25">
                                <label class="form-label m-0" for=""></label>
                                    <button type="button" @click="getData()" class="btn btn-primary">Buscar</button>
                                </div>
                            </div>
                        </div>
                    </div>                       
                </div>

                <div class="table-responsive overflow-auto" style="height:60vh">
                    <table class="table table-hover fw-normal">
                        <thead>
                            <tr class="text-center">
                                <th>Fecha</th>
                                <th>Tipo movimiento</th>
                                <th>Consecutivo</th>
                                <th>Descripción</th>
                                <th>Valor total</th>
                                <th>Estado</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr v-for="(data,index) in arrData" :key="index" class="text-center" @dblclick="sendToView(data)">
                                <td>{{ formatFecha(data.fecha) }}</td>
                                <td>{{ data.nombre_movimiento }}</td>
                                <td>{{ data.consecutivo }}</td>
                                <td class="text-left">{{ data.detalle }}</td>
                                <td class="text-rigth">{{ viewFormatNumber(data.valor_total) }}</td>
                                <td>
                                    <span :class="data.estado == 'S' ? 'badge-success' : 'badge-danger'" class="badge">
                                        {{ (data.estado == 'S') ? "Activo" : ((data.estado == 'N') ? "Anulado" : "Reversado") }}
                                    </span>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>

            </article>
        </section>

        <script src="Librerias/vue/vue.min.js"></script>
		<script src="Librerias/vue/axios.min.js"></script>
		<script type="module" src="almacen/js/functions_informe_movimientos.js"></script>

	</body>
</html>
