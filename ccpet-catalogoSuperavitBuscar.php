<?php

    header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=utf8");
	require 'comun.inc';
	require 'funciones.inc';
	session_start();
    if(empty($_SESSION)){
        header("location: index.php");
    }
	$linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");
	cargarcodigopag($_GET['codpag'],$_SESSION["nivel"]);
	date_default_timezone_set("America/Bogota");
?>
<!DOCTYPE >
<html lang=es>
	<head>
    <meta charset="utf-8"/>
		<meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1"/>
		<meta name="viewport" content="user-scalable=no">
		<title>:: IDEAL 10 - Tesorería</title>
		<link href="favicon.ico" rel="shortcut icon"/>
		<link href="css/css2z.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
		<link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
		<link href="css/css4.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
        <link href="css/tabs.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css"/>
        <link href="css/style.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css"/>
        <link rel="stylesheet" href="sweetalert2/dist/sweetalert2.min.css">
        <script type="text/javascript" src="css/programas.js"></script>
        <script src="sweetalert2/dist/sweetalert2.min.js"></script>
		<script src="vue/vue.min.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script src="css/programas.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script src="css/calendario.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
	</head>
	<body>
		<header>
			<table>
				<tr><script>barra_imagenes("ccpet");</script><?php cuadro_titulos();?></tr>
			</table>
		</header>

		<form name="form2" method="post" action="">
			<section id="myapp" v-cloak >
                <div id="cargando" v-show="isLoading" class="loading" style="z-index: 10000;">
                    <span>Cargando...</span>
                </div>
				<nav>
					<table>
						<tr><?php menu_desplegable("ccpet");?></tr>
                        <tr>
							<td colspan="3" class="cinta">
                                <img src="imagenes/add.png"  @click="window.location.href='ccpet-catalogoSuperavitCrear.php'" class="mgbt" title="Nuevo">
								<img src="imagenes/guardad.png"   title="Guardar"  class="mgbt">
								<img src="imagenes/busca.png" @click="window.location.href='ccpet-catalogoSuperavitBuscar.php'"   class="mgbt" title="Buscar">
								<img src="imagenes/nv.png" @click="mypop=window.open('ccp-principal.php','',''); mypop.focus();" class="mgbt" title="Nueva Ventana">
                                <a href="ccp-visualizarclasificadorpresupuestal.php"  class="mgbt"><img src="imagenes/iratras.png" title="Atr&aacute;s"></a>
							</td>
						</tr>
					</table>
				</nav>
				<article>
                    <div class="inicio">
                        <div>
                            <h2 class="titulos m-0">.: Buscar catálogos superavit</h2>
                            <div class="form-control w-50">
                                <div class="d-flex">
                                    <input type="search" placeholder="Buscar por código o nombre" @keyup="search()" v-model="txtSearch">
                                </div>
                            </div>
                            <h2 class="titulos m-0">.: Resultados: {{ txtResults}} </h2>
                        </div>
                        <div class="overflow-auto max-vh-50 overflow-x-hidden p-2" >
                            <table class="table table-hover fw-normal">
                                <thead>
                                    <tr>
                                        <th>Codigo</th>
                                        <th>Nombre</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr @dblclick="editItem(data.codigo)" v-for="(data,index) in arrData" :key="data.codigo">
                                        <td>{{ data.codigo }}</td>
                                        <td>{{ data.nombre}}</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div v-if="arrData !=''" class="inicio">
                        <p style="text-align:center">Página {{ intPage }} de {{ intTotalPages }}</p>
                        <ul style="list-style:none; padding:0;display:flex;justify-content: center;align-items:center">
                            <li v-show="intPage > 1" @click="search(intPage = 1)" style="cursor:pointer;padding:4px 10px;background:#fff; border:1px solid #39c"><< </li>
                            <li v-show="intPage > 1" @click="search(--intPage)" style="cursor:pointer;padding:4px 10px;background:#fff; border:1px solid #39c" ><</li>
                            <li v-show="intPage < intTotalPages" @click="search(++intPage)" style="cursor:pointer;padding:4px 10px;background:#fff; border:1px solid #39c" >></li>
                            <li v-show="intPage < intTotalPages" @click="search(intPage = intTotalPages)" style="cursor:pointer;padding:4px 10px;background:#fff; border:1px solid #39c">>></li>
                        </ul>
                    </div>
				</article>
			</section>
		</form>

		<script src="node_modules/read-excel-file/bundle/read-excel-file.min.js"></script>
		<script src="node_modules/xlsx/dist/xlsx.full.min.js"></script>
        <script src="Librerias/vue/vue.min.js"></script>
		<script src="Librerias/vue/axios.min.js"></script>
		<script type="module" src="presupuesto_ccpet/catalogo_superavit/buscar/ccpet-catalogoBuscar.js?<?= date('d_m_Y_h_i_s');?>"></script>

	</body>
</html>
