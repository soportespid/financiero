<?php
	header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=utf8");
	require "comun.inc";
	require "funciones.inc";
	session_start();
	cargarcodigopag($_GET['codpag'],$_SESSION["nivel"]);
	date_default_timezone_set("America/Bogota");
?>
<!DOCTYPE >
<html lang="es">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1">
		<title>:: IDEAL 10 - Gesti&oacute;n humana</title>
		<link href="css/css2.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<script type="text/javascript" src="css/programas.js"></script>
		<?php titlepag();?>
	</head>
	<body>
		<IFRAME src="alertas.php" name="alertas" id="alertas" style="display:none"></IFRAME>
		<span id="todastablas2"></span>
		<table>
			<tr><script>barra_imagenes("hum");</script><?php cuadro_titulos();?></tr>	 
			<tr><?php menu_desplegable("hum");?></tr>
			<tr>
				<td colspan="3" class="cinta">
					<img src="imagenes/add2.png" class="mgbt1">
					<img src="imagenes/guardad.png" class="mgbt1">
					<img src="imagenes/buscad.png" class="mgbt1">
					<img src="imagenes/agenda1.png" title="Agenda" onClick="mypop=window.open('plan-agenda.php','','');mypop.focus()" class="mgbt">
					<img src="imagenes/nv.png" title="Nueva ventana" onClick="mypop=window.open('hum-principal.php','','');mypop.focus();" class="mgbt">
					<img src="imagenes/duplicar_pantalla.png" title="Duplicar pesta&ntilde;a" onClick="mypop=window.open('<?php echo $url2; ?>','','');mypop.focus();" class="tooltip bottom mgbt">
				</td>
			</tr>
		</table>
		<form name="form2" method="post" action="">
			<table class="inicio">
				<tr>
					<td class="titulos" colspan="2">.: Men&uacute; Tramites Nomina Versión hasta 2024</td>
					<td class="cerrar" style="width:7%" onClick="location.href='hum-principal.php'">Cerrar</td>
				</tr>
				<tr>
					<td class="titulos2">&nbsp;Gesti&oacute;n Nomina</td>
					<td class="titulos2">&nbsp;Par&aacute;metros Nomina</td>
				</tr>
				<tr>
					<td class='saludo1' width='45%'>
						<ol id="lista2">
							<li class='mgbt3' onClick="location.href='hum-buscadescuentosnom.php'">Descuentos Nomina</li>
							<li class='mgbt3' onClick="location.href='hum-retencionesbuscar.php'">Ingresar Retenciones</li>
							<li class='mgbt3' onClick="location.href='hum-incapacidadesbuscar.php'">Ingresar Incapacidades</li>
							<li class='mgbt3' onClick="location.href='hum-vacacionesagregarbuscar.php'">Ingresar Vacaciones</li>
							<li class='mgbt3' onClick="location.href='hum-novedadespagosbuscar.php'">Novedad de Pagos Funcionario</li>
							<li class='mgbt3' onClick="location.href='hum-prepararnominabuscar.php'">Preparar Liquidaci&oacute;n</li>
							<li class='mgbt3' onClick="location.href='hum-liquidarnominabuscar.php'">Crear Liquidaci&oacute;n</li>
							<li class='mgbt3' onClick="location.href='hum-aprobarnominabuscar.php'">Aprobar Nomina</li>
						</ol>
					</td>
					<td class='saludo1'>
						<ol id="lista2">
							<li class='mgbt3' onClick="location.href='hum-bancosbuscar.php'">Bancos</li>
							<li class='mgbt3' onClick="location.href='hum-nivelesarlbuscar.php'">Tarifas ARL</li>
							<li class='mgbt3' onClick="location.href='hum-funcionariosbuscar.php'">Funcionarios</li>
						</ol>
					<td>
				</tr>
			</table>
		</form>
	</body>
</html>
