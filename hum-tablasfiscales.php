<?php
	header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=utf8");
	require "comun.inc";
	require "funciones.inc";
	session_start();
	$linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");
	cargarcodigopag($_GET['codpag'],$_SESSION["nivel"]);
	date_default_timezone_set("America/Bogota");
?>
<!DOCTYPE >
<html lang="es">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1">
		<meta name="viewport" content="user-scalable=no">
		<title>:: IDEAL 10 - Parametrización</title>
		<link href="favicon.ico" rel="shortcut icon"/>
        <link href="css/css2.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<link href="css/css4.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<link href="sweetalert2/dist/sweetalert2.min.css" rel="stylesheet">
		<script src="sweetalert2/dist/sweetalert2.min.js"></script>
        <script type="text/javascript" src="css/programas.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script>
			function guardar(){
				if (1==1){
					Swal.fire({
						icon: 'question',
						title: '¿Esta Seguro de guardar?',
						showDenyButton: true,
						confirmButtonText: 'Guardar',
						confirmButtonColor: '#01CC42',
						denyButtonText: 'Cancelar',
						denyButtonColor: '#FF121A',
					}).then(
						(result) => {
							if (result.isConfirmed){
								document.getElementById('oculto').value = "2";
								document.form2.submit();
							}else if (result.isDenied){
								Swal.fire({
									icon: 'info',
									title: 'No se guardo',
									confirmButtonText: 'Continuar',
									confirmButtonColor: '#FF121A',
									timer: 2500
								});
							}
						}
					)
				}else{
					Swal.fire({
						icon: 'error',
						title: 'Error!',
						text: 'Faltan datos para completar el registro',
						confirmButtonText: 'Continuar',
						confirmButtonColor: '#FF121A',
						timer: 2500
					});
				}
			}
			function despliegamodal2(_valor,_nven){
				document.getElementById("bgventanamodal2").style.visibility=_valor;
				if(_valor=="hidden"){
					document.getElementById('ventana2').src="";
				} else {
					switch (_nven) { 
						case "1":	
							document.getElementById('ventana2').src = "tercerosgral-ventana01.php?objeto=icbf&nobjeto=nicbf&nfoco=sena";break;
						case "2":	
							document.getElementById('ventana2').src = "tercerosgral-ventana01.php?objeto=sena&nobjeto=nsena&nfoco=iti";break;
						case "3":	
							document.getElementById('ventana2').src = "tercerosgral-ventana01.php?objeto=iti&nobjeto=niti&nfoco=cajas";break;
						case "4":	
							document.getElementById('ventana2').src = "tercerosgral-ventana01.php?objeto=cajas&nobjeto=ncajas&nfoco=esap";break;
						case "5":	
							document.getElementById('ventana2').src = "tercerosgral-ventana01.php?objeto=esap&nobjeto=nesap&nfoco=indiceinca";break;
					case "6":	
							document.getElementById('ventana2').src = "tercerosgral-ventana01.php?objeto=arp&nobjeto=narp&nfoco=arp";break;	
					}
				}
			}
			function busquedas(_nbus){
				switch(_nbus){
					case "1":	if (document.getElementById('icbf').value!=""){
									document.getElementById('banbus').value="1";
									document.form2.submit();
								}
								break;
					case "2":	if (document.getElementById('sena').value!=""){
									document.getElementById('banbus').value="2";
									document.form2.submit();
								}
								break;
					case "3":	if (document.getElementById('iti').value!=""){
									document.getElementById('banbus').value="3";
									document.form2.submit();
								}
								break;
					case "4":	if (document.getElementById('cajas').value!=""){
									document.getElementById('banbus').value="4";
									document.form2.submit();
								}
								break;
					case "5":	if (document.getElementById('esap').value!="")	{
									document.getElementById('banbus').value="5";
									document.form2.submit();
								}
								break;
					case "6":	if (document.getElementById('arp').value!=""){
									document.getElementById('banbus').value="6";
									document.form2.submit();
								}
								break;			
				}
			}
		</script>
		<?php titlepag();?>
	</head>
	<body>
		<IFRAME src="alertas.php" name="alertas" id="alertas" style="display:none"></IFRAME>
		<span id="todastablas2"></span>
		<table>
			<tr><script>barra_imagenes("para");</script><?php cuadro_titulos();?></tr>	 
			<tr><?php menu_desplegable("para");?></tr>
			<tr>
				<td colspan="3" class="cinta">
					<img src="imagenes/add.png" title="Nuevo" onClick="location.href='hum-tablasfiscales.php'" class="mgbt"/>
					<img src="imagenes/guarda.png" title="Guardar" onClick="guardar()" class="mgbt"/>
					<img src="imagenes/busca.png" title="Buscar" onClick="location.href='hum-buscatablasfiscales.php'" class="mgbt"/>
					<img src="imagenes/agenda1.png" title="Agenda" onClick="mypop=window.open('plan-agenda.php','','');mypop.focus()" class="mgbt"/>
					<img src="imagenes/nv.png" title="Nueva ventana" onClick="mypop=window.open('para-principal.php','','');mypop.focus();" class="mgbt">
					<img src="imagenes/duplicar_pantalla.png" title="Duplicar pesta&ntilde;a" onClick="mypop=window.open('<?php echo $url2; ?>','','');mypop.focus();" class="tooltip bottom mgbt">
					<img src="imagenes/iratras.png" title="Atr&aacute;s" onClick="location.href='hum-buscatablasfiscales.php'" class="mgbt">
				</td>
			</tr>		
		</table>
		<div id="bgventanamodalm" class="bgventanamodalm">
			<div id="ventanamodalm" class="ventanamodalm">
				<IFRAME src="" name="ventanam" marginWidth=0 marginHeight=0 frameBorder=0 id="ventanam" frameSpacing=0 style=" width:700px; height:130px; top:200; overflow:hidden;"> 
				</IFRAME>
			</div>
		</div>
		<form name="form2" method="post">
			<input type="hidden" name="valfocus" id="valfocus" value="0"/>
			<?php
				if($_POST['oculto']==""){	
					$_POST['vigencia'] = selconsecutivo('admfiscales','vigencia');
					if($_POST['vigencia'] != ''){
						$vigeant = $_POST['vigencia'] - 1;
						$sqlr = "SELECT uvt, salario, transporte, alimentacion, bfsol, balimentacion, btransporte, icbf, sena, iti, cajas, esap, indiceinca FROM admfiscales WHERE vigencia = '$vigeant'";
						$resp = mysqli_query($linkbd,$sqlr);
						$row = mysqli_fetch_row($resp);
						$_POST['uvt'] = $row[0];
						$_POST['salario'] = $row[1];
						$_POST['transporte'] = $row[2];
						$_POST['alimentacion'] = $row[3];
						$_POST['bfsol'] = $row[4];
						$_POST['balimentacion'] = $row[5];
						$_POST['btransporte'] = $row[6];

						$_POST['icbf'] = $row[7];
						if($_POST['icbf'] != ''){
							$_POST['nicbf'] = buscatercero($_POST['icbf']);
						} else {
							$_POST['nicbf'] = '';
						}

						$_POST['sena'] = $row[8];
						if($_POST['sena']){
							$_POST['nsena'] = buscatercero($_POST['sena']);
						} else {
							$_POST['nsena'] = '';
						}

						$_POST['iti'] = $row[9];
						if($_POST['iti']){
							$_POST['niti'] = buscatercero($_POST['iti']);
						} else {
							$_POST['niti'] = '';
						}

						$_POST['cajas'] = $row[10];
						if($_POST['cajas']){
							$_POST['ncajas'] = buscatercero($_POST['cajas']);
						} else {
							$_POST['ncajas'] = '';
						}

						$_POST['esap'] = $row[11];
						if($_POST['esap']){
							$_POST['nesap'] = buscatercero($_POST['esap']);
						} else {
							$_POST['nesap'] = '';
						}

						$_POST['arp'] = $row[12];
						if($_POST['arp']){
							$_POST['narp'] = buscatercero($_POST['arp']);
						} else {
							$_POST['narp'] = '';
						}
					}else{
						$_POST['uvt'] = 0;
						$_POST['salario'] = 0;
						$_POST['transporte'] = 0;
						$_POST['alimentacion'] = 0;
						$_POST['bfsol'] = 0;
						$_POST['balimentacion'] = 0;
						$_POST['btransporte'] = 0;
					}
					
				}
			?>
			<table class="inicio ancho">
				<tr>
					<td class="titulos" colspan="8">:: Tablas Fiscales</td>
					<td class="cerrar" style="width:7%" onClick="location.href='para-principal.php'">Cerrar</td>
				</tr>
				<tr>
					<td class="saludo1" style="width:3.5cm">:: Vigencia:</td>
					<td style="width:12%"><input type="text" name="vigencia" value="<?php echo $_POST['vigencia']?>" maxlength="4" onKeyPress="javascript:return solonumeros(event)" style="width:100%"></td>
					<td class="saludo1" style="width:3.5cm">:: UVT:</td>
					<td style="width:12%"><input name="uvt" type="text" value="<?php echo $_POST['uvt']?>"  onKeyPress="javascript:return solonumeros(event)" style="width:100%"></td>
					<td class="saludo1" style="width:3.5cm">:: Salario Minimo:</td>
					<td style="width:12%"><input name="salario" type="text" value="<?php echo $_POST['salario']?>" onKeyPress="javascript:return solonumeros(event)" style="width:100%"></td>
					<td class="saludo1" style="width:3.5cm">:: Base F Solidaridad:</td>
					<td><input name="bfsol" type="text" value="<?php echo $_POST['bfsol']?>"  onKeyPress="javascript:return solonumeros(event)" style="width:100%"></td>
				</tr>
				<tr>
					<td class="saludo1">:: Subsidio Transporte:</td>
					<td><input name="transporte" type="text" value="<?php echo $_POST['transporte']?>" onKeyPress="javascript:return solonumeros(event)" style="width:100%"></td>
					<td class="saludo1">:: Subsidio Alimentacion:</td>
					<td><input name="alimentacion" type="text" value="<?php echo $_POST['alimentacion']?>" onKeyPress="javascript:return solonumeros(event)" style="width:100%"></td>
					<td class="saludo1">:: Base Aux Transporte:</td>
					<td><input name="btransporte" type="text" value="<?php echo $_POST['btransporte']?>" onKeyPress="javascript:return solonumeros(event)" style="width:100%"></td>
					<td class="saludo1">:: Base Aux Alimentacion:</td>
					<td><input name="balimentacion" type="text" value="<?php echo $_POST['balimentacion']?>"  onKeyPress="javascript:return solonumeros(event)" style="width:100%"></td>
				</tr>
				<tr><td class="titulos" colspan="8">.: Empresas prestadoras de servicios</td></tr>
				<tr>
					<td class="saludo1">.: ARL:</td>
					<td><input id="arp" name="arp" type="text" value="<?php echo $_POST['arp']?>" onKeyUp="return tabular(event,this)" style="width:100%" onChange="busquedas('6')" onDblClick="despliegamodal2('visible','6');" class="colordobleclik" autocomplete="off"/></td>
					<td colspan="2"><input id="narp" name="narp" type="text" value="<?php echo $_POST['narp']?>" onKeyUp="return tabular(event,this)" style="width:100%" readonly></td>
					<td class="saludo1">.: ICBF:</td>
					<td><input id="icbf" name="icbf" type="text" value="<?php echo $_POST['icbf']?>" onKeyUp="return tabular(event,this)" style="width:100%" onChange="busquedas('1')" onDblClick="despliegamodal2('visible','1');" class="colordobleclik" autocomplete="off"/></td>
					<td colspan="2"><input id="nicbf" name="nicbf" type="text" value="<?php echo $_POST['nicbf']?>" onKeyUp="return tabular(event,this)" style="width:100%" readonly></td>
				</tr>
				<tr>
					<td class="saludo1">.: SENA:</td>
					<td><input id="sena" name="sena" type="text" value="<?php echo $_POST['sena']?>" onKeyUp="return tabular(event,this)" style="width:100%" onChange="busquedas('2')" onDblClick="despliegamodal2('visible','2');" class="colordobleclik" autocomplete="off" onDblClick="despliegamodal2('visible','2');" class="colordobleclik" autocomplete="off"/></td>
					<td colspan="2"><input id="nsena" name="nsena" type="text" value="<?php echo $_POST['nsena']?>"  onKeyUp="return tabular(event,this)" style="width:100%" readonly> </td>
					<td class="saludo1">.: Institutos Tecnicos:</td>
					<td><input id="iti" name="iti" type="text" value="<?php echo $_POST['iti']?>" onKeyUp="return tabular(event,this)" style="width:100%" onChange="busquedas('3')" onDblClick="despliegamodal2('visible','3');" class="colordobleclik" autocomplete="off"/></td>
					<td colspan="2"><input id="niti" name="niti" type="text" value="<?php echo $_POST['niti']?>" onKeyUp="return tabular(event,this)" style="width:100%" readonly> </td>
				</tr>
				<tr>
					<td class="saludo1">.: Caja de Compensacion:</td>
					<td><input id="cajas" name="cajas" type="text" value="<?php echo $_POST['cajas']?>" onKeyUp="return tabular(event,this)" style="width:100%" onChange="busquedas('4')" onDblClick="despliegamodal2('visible','4');" class="colordobleclik" autocomplete="off"/></td>
					<td colspan="2"><input id="ncajas" name="ncajas" type="text" value="<?php echo $_POST['ncajas']?>" onKeyUp="return tabular(event,this)" style="width:100%" readonly></td>
					<td class="saludo1">.: ESAP:</td>
					<td><input id="esap" name="esap" type="text" value="<?php echo $_POST['esap']?>" onKeyUp="return tabular(event,this)" style="width:100%" onChange="busquedas('5')" onDblClick="despliegamodal2('visible','5');" class="colordobleclik" autocomplete="off"/></td>
					<td colspan="2"><input id="nesap" name="nesap" type="text" value="<?php echo $_POST['nesap']?>" onKeyUp="return tabular(event,this)" style="width:100%" readonly></td>
				</tr>    
			</table>
			<input type="hidden" name="banbus" id="banbus" value=""/>
			<input type="hidden" name="oculto" id="oculto" value="1">
			<?php
				if($_POST['banbus']!=''){
					switch ($_POST['banbus']){ 
						case '1':	
							$nresul = buscatercero($_POST['icbf']);
							if($nresul != ''){
								echo"
								<script>
									document.getElementById('nicbf').value='$nresul';
									document.getElementById('sena').focus();
								</script>";
							} else {
								echo"
								<script>
									document.getElementById('valfocus').value='1';
									document.getElementById('nicbf').value='';
									Swal.fire({
										icon: 'error',
										title: 'Error!',
										text: 'Documento Incorrecto',
										confirmButtonText: 'Continuar',
										confirmButtonColor: '#FF121A',
										timer: 2500
									});
								</script>";
							}break;
						case '2':	
							$nresul = buscatercero($_POST['sena']);
							if($nresul!=''){
								echo"
								<script>
									document.getElementById('nsena').value='$nresul';
									document.getElementById('iti').focus();
								</script>";
							} else {
								echo"
								<script>
									document.getElementById('valfocus').value='2';
									document.getElementById('nsena').value='';
									Swal.fire({
										icon: 'error',
										title: 'Error!',
										text: 'Documento Incorrecto',
										confirmButtonText: 'Continuar',
										confirmButtonColor: '#FF121A',
										timer: 2500
									});
								</script>";
							}break;
						case '3':	
							$nresul = buscatercero($_POST['iti']);
							if($nresul!=''){
								echo"
								<script>
									document.getElementById('niti').value='$nresul';
									document.getElementById('cajas').focus();
								</script>";
							} else {
								echo"
								<script>
									document.getElementById('valfocus').value='3';
									document.getElementById('niti').value='';
									Swal.fire({
										icon: 'error',
										title: 'Error!',
										text: 'Documento Incorrecto',
										confirmButtonText: 'Continuar',
										confirmButtonColor: '#FF121A',
										timer: 2500
									});
								</script>";
							}break;
						case '4':	
							$nresul = buscatercero($_POST['cajas']);
							if($nresul!=''){
								echo"
								<script>
									document.getElementById('ncajas').value='$nresul';
									document.getElementById('esap').focus();
								</script>";
							} else {
								echo"
								<script>
									document.getElementById('valfocus').value='4';
									document.getElementById('ncajas').value='';
									Swal.fire({
										icon: 'error',
										title: 'Error!',
										text: 'Documento Incorrecto',
										confirmButtonText: 'Continuar',
										confirmButtonColor: '#FF121A',
										timer: 2500
									});
								</script>";
							}break;
						case '5':	
							$nresul = buscatercero($_POST['esap']);
							if($nresul!=''){
								echo"
								<script>
									document.getElementById('nesap').value='$nresul';
									document.getElementById('indiceinca').focus();
								</script>";
							} else {
								echo"
								<script>
									document.getElementById('valfocus').value='5';
									document.getElementById('nesap').value='';
									Swal.fire({
										icon: 'error',
										title: 'Error!',
										text: 'Documento Incorrecto',
										confirmButtonText: 'Continuar',
										confirmButtonColor: '#FF121A',
										timer: 2500
									});
								</script>";
							}break;
						case '6':	
							$nresul = buscatercero($_POST['arp']);
							if($nresul!=''){
								echo"
								<script>
									document.getElementById('narp').value='$nresul';
									document.getElementById('narp').focus();
								</script>";
							}
							else
							{
								echo"
								<script>
									document.getElementById('valfocus').value='6';
									document.getElementById('narp').value='';
									Swal.fire({
										icon: 'error',
										title: 'Error!',
										text: 'Documento Incorrecto',
										confirmButtonText: 'Continuar',
										confirmButtonColor: '#FF121A',
										timer: 2500
									});
								</script>";
							}break;
					}
				}
				if($_POST['oculto']=="2"){
					if($_POST['indiceinca']==""){
						$_POST['indiceinca']=0;
					}
					$sqlr = "SELECT count(vigencia) FROM admfiscales WHERE vigencia = '".$_POST['vigencia']."'";
					$resp = mysqli_query($linkbd,$sqlr);
					$row = mysqli_fetch_row($resp);
					if ($row[0]==0 && $row[0]!=""){
						$sqlr = "INSERT INTO admfiscales (vigencia, uvt, salario, transporte, alimentacion, bfsol, balimentacion, btransporte, estado, icbf, sena, iti, cajas, esap, indiceinca) VALUES ('".$_POST['vigencia']."', '".$_POST['uvt']."', '".$_POST['salario']."', '".$_POST['transporte']."', '".$_POST['alimentacion']."', '".$_POST['bfsol']."', '".$_POST['balimentacion']."', '".$_POST['btransporte']."', 'S' ,'".$_POST['icbf']."','".$_POST['sena']."','".$_POST['iti']."','".$_POST['cajas']."','".$_POST['esap']."','".$_POST['arp']."') ";
						if (!mysqli_query($linkbd,$sqlr)){
							echo"
							<script>
								document.getElementById('valfocus').value='6';
								Swal.fire({
									icon: 'error',
									title: 'Error!',
									text: 'Manejador de Errores de la Clase BD admfiscales',
									confirmButtonText: 'Continuar',
									confirmButtonColor: '#FF121A',
									timer: 2500
								});
							</script>";
						} else {
							$sqlv = "SELECT id FROM admfiscales WHERE vigencia = '".$_POST['vigencia']."'";
							$resv = mysqli_query($linkbd,$sqlv);
							$rowv = mysqli_fetch_row($resv);
							echo"
							<script>
								Swal.fire({
									icon: 'success',
									title: 'Se ha Agregado La informacion a la Vigencia ".$_POST['vigencia']."',
									showConfirmButton: true,
									confirmButtonText: 'Continuar',
									confirmButtonColor: '#01CC42',
									timer: 3500
								}).then((response) => {
									location.href='hum-editatablasfiscales.php?idvig=".$rowv[0]."';
								});
							</script>";
						}
					}else {
						echo"
						<script>
							document.getElementById('valfocus').value='6';
							Swal.fire({
								icon: 'error',
								title: 'Error!',
								text: 'Ya Existe datos para esta Vigencia',
								confirmButtonText: 'Continuar',
								confirmButtonColor: '#FF121A',
								timer: 2500
							});
						</script>";
					}
				}
			?>
		</form>
		<div id="bgventanamodal2">
			<div id="ventanamodal2">
				<IFRAME  src="" name="buster" marginWidth=0 marginHeight=0 frameBorder=0 id="ventana2" frameSpacing=0 style="left:500px; width:900px; height:500px; top:200;"> 
				</IFRAME>
			</div>
		</div>
	</body>
</html>
