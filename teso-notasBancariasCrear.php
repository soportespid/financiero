<?php

    header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=utf8");
	require 'comun.inc';
	require 'funciones.inc';
	session_start();
    if(empty($_SESSION)){
        header("location: index.php");
    }
	$linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");
	cargarcodigopag($_GET['codpag'],$_SESSION["nivel"]);
	date_default_timezone_set("America/Bogota");
?>
<!DOCTYPE >
<html lang=es>
	<head>
    <meta charset="utf-8"/>
		<meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1"/>
		<meta name="viewport" content="user-scalable=no">
		<title>:: IDEAL 10 - Parametrización</title>
		<link href="favicon.ico" rel="shortcut icon"/>
		<link href="css/css2z.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
		<link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
		<link href="css/css4.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
        <link href="css/tabs.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css"/>
        <link href="css/style.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css"/>
        <link rel="stylesheet" href="sweetalert2/dist/sweetalert2.min.css">
        <script type="text/javascript" src="css/programas.js"></script>
        <script src="sweetalert2/dist/sweetalert2.min.js"></script>
		<script src="vue/vue.min.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script src="css/programas.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script src="css/calendario.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
	</head>
	<body>
		<header>
			<table>
				<tr><script>barra_imagenes("para");</script><?php cuadro_titulos();?></tr>
			</table>
		</header>

		<form name="form2" method="post" action="">
			<section id="myapp" v-cloak >
                <div id="cargando" v-show="isLoading" class="loading" style="z-index: 10000;">
                    <span>Cargando...</span>
                </div>
				<nav>
					<table>
						<tr><?php menu_desplegable("para");?></tr>
                        <tr>
							<td colspan="3" class="cinta">
                                <img src="imagenes/add.png"  @click="window.location.reload()" class="mgbt" title="Nuevo">
								<img @click="save()" src="imagenes/guarda.png"   title="Guardar"  class="mgbt">
								<img src="imagenes/busca.png" @click="window.location.href='teso-notasBancariasBuscar.php'"   class="mgbt" title="Buscar">
								<img src="imagenes/nv.png" @click="mypop=window.open('para-principal.php','',''); mypop.focus();" class="mgbt" title="Nueva Ventana">
                                <a href="teso-notasBancariasBuscar.php"  class="mgbt"><img src="imagenes/iratras.png" title="Atr&aacute;s"></a>
							</td>
						</tr>
					</table>
				</nav>
				<article>
                    <div class="inicio">
                        <div>
                            <h2 class="titulos m-0">.: Agregar notas bancarias <span class="text-danger fw-bolder">*</span></h2>
                            <div class="d-flex justify-between w-75">
                                <div class="form-control w-25">
                                    <label class="form-label">.: Consecutivo:</label>
                                    <input type="text"  style="text-align:center;" v-model="txtConsecutivo" disabled readonly>
                                </div>
                                <div class="form-control w-25">
                                    <label class="form-label">.: Tipo de nota <span class="text-danger fw-bolder">*</span>:</label>
                                    <select v-model="selectNota">
                                        <option value="I">Ingreso</option>
                                        <option value="G">Gasto</option>
                                    </select>
                                </div>
                                <div class="form-control">
                                    <label class="form-label" for="">.: Nombre <span class="text-danger fw-bolder">*</span>:</label>
                                    <input type="text" v-model="txtNombre">
                                </div>
                            </div>
                        </div>
                        <div>
                            <h2 class="titulos m-0">.: Detalle nota bancaria</h2>
                            <div class="form-control w-75">
                                <label class="form-label" for="labelSelectName2">.: Concepto contable <span class="text-danger fw-bolder">*</span>:</label>
                                <select id="labelSelectName2" v-model="selectConcepto">
                                    <option v-for="data in arrConceptos" :value="data.codigo":key="data.codigo">{{data.codigo+"-"+data.tipo+"-"+data.nombre}}</option>
                                </select>
                            </div>
                            <div v-if="selectNota == 'I'">
                                <div  class="form-control w-75">
                                    <label class="form-label" for="">.: Cuenta presupuestal <span class="text-danger fw-bolder">*</span>:</label>
                                    <div class="d-flex">
                                        <input type="text" v-model="objCuenta.codigo" @change="search('codigo_cuenta')" @dblclick="isModal=true" class="w-25 colordobleclik">
                                        <input type="text" v-model="objCuenta.nombre" disabled readonly>
                                    </div>
                                </div>
                                <div class="form-control w-75">
                                    <label class="form-label" for="">.: Fuente <span class="text-danger fw-bolder">*</span>:</label>
                                    <div class="d-flex">
                                        <input type="text" v-model="objFuente.codigo" @change="search('codigo_fuente')" @dblclick="isModalFuente=true" class="w-25 colordobleclik">
                                        <input type="text" v-model="objFuente.nombre" disabled readonly>
                                    </div>
                                </div>
                                <div  v-if="txtType == 2" class="form-control w-75">
                                    <label class="form-label" for="">.: Bienes <span class="text-danger fw-bolder">*</span>:</label>
                                    <div class="d-flex">
                                        <input type="text" v-model="objBien.codigo" @change="search('codigo_bien')" @dblclick="isModalBienes=true" class="w-25 colordobleclik">
                                        <input type="text" v-model="objBien.nombre" disabled readonly>
                                    </div>
                                </div>
                                <div  v-if="txtType == 3" class="form-control w-75">
                                    <label class="form-label" for="">.: Servicios <span class="text-danger fw-bolder">*</span>:</label>
                                    <div class="d-flex">
                                        <input type="text" v-model="objServicio.codigo" @change="search('codigo_servicio')" @dblclick="isModalServicios=true" class="w-25 colordobleclik">
                                        <input type="text" v-model="objServicio.nombre" disabled readonly>
                                    </div>
                                </div>
                                <div  v-if="txtType == 4" class="form-control w-75">
                                    <label class="form-label" for="">.: Complemento de ingreso <span class="text-danger fw-bolder">*</span>:</label>
                                    <div class="d-flex">
                                        <input type="text" v-model="objComplemento.codigo" @change="search('codigo_complemento')" @dblclick="isModalComplemento=true" class="w-25 colordobleclik">
                                        <input type="text" v-model="objComplemento.nombre" disabled readonly>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
				</article>
                <!--MODALES-->
                <div v-show="isModal">
                    <transition name="modal">
                        <div class="modal-mask">
                            <div class="modal-wrapper">
                                <div class="modal-container">
                                    <table class="inicio ancho">
                                        <tr>
                                            <td class="titulos" colspan="2" >.: Buscar cuenta presupuestal</td>
                                            <td class="cerrar" style="width:7%" @click="isModal = false">Cerrar</td>
                                        </tr>
                                    </table>
                                    <div class="bg-white">
                                        <div class="form-control m-0 p-2 w-inherit">
                                            <input type="search" placeholder="Buscar" @keyup="search('modal_cuenta')" v-model="txtSearch">
                                        </div>
                                        <p class="fw-bolder m-0 p-2">Resultados de búsqueda: {{ txtResultsModalCuentas}} </p>
                                        <div class="overflow-auto max-vh-50 overflow-x-hidden p-2">
                                            <table class="table table-hover fw-normal">
                                                <thead>
                                                    <tr>
                                                        <th>Cuenta</th>
                                                        <th>Descripción</th>
                                                        <th>Tipo</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr @dblclick="selectItemModal(data,'cuenta')" v-for="data in arrModalCuentas" :key="data.codigo">
                                                        <td>{{data.codigo}}</td>
                                                        <td>{{data.nombre}}</td>
                                                        <td>{{data.tipo}}</td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </transition>
                </div>
                <div v-show="isModalFuente">
                    <transition name="modal">
                        <div class="modal-mask">
                            <div class="modal-wrapper">
                                <div class="modal-container">
                                    <table class="inicio ancho">
                                        <tr>
                                            <td class="titulos" colspan="2" >.: Buscar Fuente</td>
                                            <td class="cerrar" style="width:7%" @click="isModalFuente = false">Cerrar</td>
                                        </tr>
                                    </table>
                                    <div class="bg-white">
                                        <div class="form-control m-0 p-2 w-inherit">
                                            <input type="search" placeholder="Buscar" @keyup="search('modal_fuente')" v-model="txtSearchFuente">
                                        </div>
                                        <p class="fw-bolder m-0 p-2">Resultados de búsqueda: {{ txtResultsModalFuentes}} </p>
                                        <div class="overflow-auto max-vh-50 overflow-x-hidden p-2">
                                            <table class="table table-hover fw-normal">
                                                <thead>
                                                    <tr>
                                                        <th>Codigo</th>
                                                        <th>Nombre</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr @dblclick="selectItemModal(data,'fuente')" v-for="data in arrModalFuentes" :key="data.codigo">
                                                        <td>{{data.codigo}}</td>
                                                        <td>{{data.nombre}}</td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </transition>
                </div>
                <div v-show="isModalBienes">
                    <transition name="modal">
                        <div class="modal-mask">
                            <div class="modal-wrapper">
                                <div class="modal-container">
                                    <table class="inicio ancho">
                                        <tr>
                                            <td class="titulos" colspan="2" >.: Buscar bienes</td>
                                            <td class="cerrar" style="width:7%" @click="isModalBienes = false">Cerrar</td>
                                        </tr>
                                    </table>
                                    <div class="bg-white">
                                        <div class="form-control m-0 p-2 w-inherit">
                                            <input type="search" placeholder="Buscar" @keyup="search('modal_bien')" v-model="txtSearchBien">
                                        </div>
                                        <p class="fw-bolder m-0 p-2">Resultados de búsqueda: {{ txtResultsModalBienes}} </p>
                                        <div class="overflow-auto max-vh-50 overflow-x-hidden p-2">
                                            <table class="table table-hover fw-normal">
                                                <thead>
                                                    <tr>
                                                        <th>Codigo</th>
                                                        <th>Nombre</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr @dblclick="selectItemModal(data,'bien')" v-for="data in arrModalBienes" :key="data.codigo">
                                                        <td>{{data.codigo}}</td>
                                                        <td>{{data.nombre}}</td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </transition>
                </div>
                <div v-show="isModalServicios">
                    <transition name="modal">
                        <div class="modal-mask">
                            <div class="modal-wrapper">
                                <div class="modal-container">
                                    <table class="inicio ancho">
                                        <tr>
                                            <td class="titulos" colspan="2" >.: Buscar servicios</td>
                                            <td class="cerrar" style="width:7%" @click="isModalServicios = false">Cerrar</td>
                                        </tr>
                                    </table>
                                    <div class="bg-white">
                                        <div class="form-control m-0 p-2 w-inherit">
                                            <input type="search" placeholder="Buscar" @keyup="search('modal_servicio')" v-model="txtSearchServicio">
                                        </div>
                                        <p class="fw-bolder m-0 p-2">Resultados de búsqueda: {{ txtResultsModalServicios}} </p>
                                        <div class="overflow-auto max-vh-50 overflow-x-hidden p-2">
                                            <table class="table table-hover fw-normal">
                                                <thead>
                                                    <tr>
                                                        <th>Codigo</th>
                                                        <th>Nombre</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr @dblclick="selectItemModal(data,'servicio')" v-for="data in arrModalServicios" :key="data.codigo">
                                                        <td>{{data.codigo}}</td>
                                                        <td>{{data.nombre}}</td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </transition>
                </div>
                <div v-show="isModalComplemento">
                    <transition name="modal">
                        <div class="modal-mask">
                            <div class="modal-wrapper">
                                <div class="modal-container">
                                    <table class="inicio ancho">
                                        <tr>
                                            <td class="titulos" colspan="2" >.: Buscar complemento de ingresos</td>
                                            <td class="cerrar" style="width:7%" @click="isModalComplemento = false">Cerrar</td>
                                        </tr>
                                    </table>
                                    <div class="bg-white">
                                        <div class="form-control m-0 p-2 w-inherit">
                                            <input type="search" placeholder="Buscar" @keyup="search('modal_complemento')" v-model="txtSearchComplemento">
                                        </div>
                                        <p class="fw-bolder m-0 p-2">Resultados de búsqueda: {{ txtResultsModalComplementos}} </p>
                                        <div class="overflow-auto max-vh-50 overflow-x-hidden p-2">
                                            <table class="table table-hover fw-normal">
                                                <thead>
                                                    <tr>
                                                        <th>Codigo</th>
                                                        <th>Nombre</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr @dblclick="selectItemModal(data,'complemento')" v-for="data in arrModalComplementos" :key="data.codigo">
                                                        <td>{{data.codigo}}</td>
                                                        <td>{{data.nombre}}</td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </transition>
                </div>
			</section>
		</form>
		<script src="node_modules/read-excel-file/bundle/read-excel-file.min.js"></script>
		<script src="node_modules/xlsx/dist/xlsx.full.min.js"></script>
        <script src="Librerias/vue/vue.min.js"></script>
		<script src="Librerias/vue/axios.min.js"></script>
		<script type="module" src="tesoreria/notas_bancarias/crear/teso-notasCrear.js?<?= date('d_m_Y_h_i_s');?>"></script>
	</body>
</html>
