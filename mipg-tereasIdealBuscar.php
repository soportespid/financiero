<?php
	header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=utf8");
	require 'comun.inc';
	require 'funciones.inc';
	session_start();
	if(empty($_SESSION)){
		header("location: index.php");
	}
	$linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");
	cargarcodigopag($_GET['codpag'],$_SESSION["nivel"]);
	date_default_timezone_set("America/Bogota");
?>
<!DOCTYPE >
<html lang=es>
	<head>
		<meta charset="utf-8"/>
		<meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1"/>
		<meta name="viewport" content="user-scalable=no">
		<title>:: IDEAL 10 - Herramientas MIPG</title>
		<link href="favicon.ico" rel="shortcut icon"/>
		<link href="css/css2z.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
		<link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
		<link href="css/css4.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
		<link href="css/style.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css"/>
		<link rel="stylesheet" href="sweetalert2/dist/sweetalert2.min.css">
		<script src="sweetalert2/dist/sweetalert2.min.js"></script>
		<script type="text/javascript" src="css/programas.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script src="css/calendario.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script>
			function updateFecha2() { 
				const fecha1 = document.getElementById('fc_1198971545').value
				if (document.getElementById('fc_1198971545').value) { 
					const [day, month, year] = fecha1.split('/').map(Number); 
					if (!isNaN(day) && !isNaN(month) && !isNaN(year)) { 
						const firstDay = new Date(year, month - 1, 1); 
						const lastDay = new Date(year, month, 0);  
						document.getElementById('fc_1198971546').value = lastDay.toLocaleDateString('es-ES', { day: '2-digit', month: '2-digit', year: 'numeric' });
					} 
				} 
			}
		</script>
	</head>
	<body>
		<section id="myapp" v-cloak>
			<div class="loading-container" v-show="isLoading" >
				<p class="text-loading"data-loading="IDEAL 10">IDEAL 10</p>
			</div>
			<div class="main-container">
				<header>
					<table>
						<tr><script>barra_imagenes("meci");</script><?php cuadro_titulos();?></tr>
					</table>
				</header>
				<nav>
					<?php menu_desplegable("meci");?>
					<div class="bg-white group-btn p-1">
						<button type="button" @click="mypop=window.open('meci-principal.php','',''); mypop.focus();" class="btn btn-white btn-primary-hover d-flex justify-between align-items-center">
							<span>Nueva ventana</span>
							<svg class="fill-black group-hover:fill-white w-5 h-5" xmlns="http://www.w3.org/2000/svg" viewBox="0 -960 960 960"><path d="M320-120v-80h80v-80H160q-33 0-56.5-23.5T80-360v-400q0-33 23.5-56.5T160-840h640q33 0 56.5 23.5T880-760v400q0 33-23.5 56.5T800-280H560v80h80v80H320ZM160-360h640v-400H160v400Zm0 0v-400 400Z"></path></svg>
						</button>
						<button type="button" @click="mypop=window.open('mipg-tereasIdealBuscar.php','','');mypop.focus();" class="btn btn-white btn-primary-hover d-flex justify-between align-items-center">
							<span class="group-hover:text-white">Duplicar pantalla</span>
							<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 -960 960 960"><path d="M640-160v-360H160v360h480Zm80-200v-80h80v-360H320v200h-80v-200q0-33 23.5-56.5T320-880h480q33 0 56.5 23.5T880-800v360q0 33-23.5 56.5T800-360h-80ZM160-80q-33 0-56.5-23.5T80-160v-360q0-33 23.5-56.5T160-600h480q33 0 56.5 23.5T720-520v360q0 33-23.5 56.5T640-80H160Zm400-603ZM400-340Z"></path></svg>
						</button>
					</div>
				</nav>
				<article>
					<div class="bg-white">
						<h2 class="titulos m-0">.: Tareas Programadas</h2>
						<div class="w-100">
							<div class="d-flex w-100">
								<div class="form-control w-25">
									<label class="form-label" for="">Fecha Inicial:</label>
									<input type="text" name="fecha" value="<?php echo $_POST['fecha']?>" onKeyUp="return tabular(event,this)" id="fc_1198971545" title="DD/MM/YYYY" onDblClick="displayCalendarFor('fc_1198971545');" class="colordobleclik" autocomplete="off" onchange="updateFecha2()" readonly>
								</div>
								<div class="form-control w-25">
									<label class="form-label" for="">Fecha Final:</label>
									<input type="text" name="fecha2" value="<?php echo $_POST['fecha2']?>" onKeyUp="return tabular(event,this)" id="fc_1198971546" title="DD/MM/YYYY" onDblClick="displayCalendarFor('fc_1198971546');" class="colordobleclik" autocomplete="off" onChange="" readonly>
								</div>
								<div class="form-control w-25"> 
									<label class="form-label" for="">Estado:</label>
									<select id="labelselectEstado" v-model="selectEstado" @change="searchx">
										<option value="0" selected>Todos</option>
										<option value="1" selected>Pendientes</option>
										<option value="2" selected>Contestados</option>
										<option value="3" selected>Anulados</option>
									</select>
								</div>
								<div class="form-control justify-between w-25">
									<label for=""></label>
									<div class="d-flex">
										<button type="button" class="btn btn-primary" @click="cargarInfo()">Buscar</button>
									</div>
								</div>
							</div>
							<div class="d-flex w-100">
								<div class="form-control ">
									<label class="form-label" for="">Radicado - Descripción:</label>
									<input type="search" placeholder="buscar" v-model="txtSearch" @keyup="searchx">
								</div>
								<div class="form-control w-25" >
									<label class="form-label" for="">Tipo:</label>
									<select id="labelselectTipo" v-model="selectTipo" @change="searchx">
										<option value="0" selected>Todos</option>
										<option value="1" selected>Tareas Internas</option>
										<option value="2" selected>Radicados Ventanilla</option>
									</select>
								</div>
								<div class="form-control w-25">
									<label for=""></label>
								</div>
							</div>
						</div>
						<h2 class="titulos m-0">.: Resultados: {{ txtResults }} </h2>
						<div class="overflow-auto max-vh-50 overflow-x-hidden p-2" ref="tableContainer">
							<table class="table table-hover fw-normal">
								<thead>
									<tr>
										<th class="text-center">N° Radicado</th>
										<th class="text-center">Fecha Raricado</th>
										<th class="text-center">Fecha Vencimiento</th>
										<th class="text-center">Fecha Respuesta</th>
										<th class="text-center">Asignado por</th>
                                        <th class="text-center">Descripción</th>
										<th class="text-center">Proceso</th>
										<th class="text-center">Estado</th>
									</tr>
								</thead>
								<tbody>
									<tr v-for="(data, index) in arrDataCopy" :key="index" v-bind:class="{ 'bg-yellow': data.is_yellow == 1 }" ref="highlightedRow" @dblclick="editItem(data.numeror, data.estado, data.estadores, data.tipot )">
										<td class="text-center">{{ data.codradicacion }}</td>
										<td class="text-center">{{ data.fradicado }}</td>
										<td class="text-center">{{ data.fLimite }}</td>
										<td class="text-center">{{ data.frespuesta }}</td>
										<td>{{ data.nomtercero }}</td>
										<td>{{ data.descripcionr }}</td>
										<td class="text-center">
											<span :class="[data.estadores == 1 ? 'badge-success' : 'badge-primary']" class="badge">{{ data.estadores == 1 ? "Responsable" : "Lector"}}</span>
										</td>
										<td class="text-center">
											<span :class="[data.colorEstado === '0' ? 'badge-warning' : '', data.colorEstado === '1' ? 'badge-success' : '', data.colorEstado === '2' ? 'badge-primary' : '']" class="badge">{{ data.estado === "LN" ? "Sin Revisar" : (data.estado === "LS" ? "Revisada" : (data.estado === "AN" ? "Sin Responder" : (data.estado  === "AC" ? "Contestada" : (data.estado === "AR" ? "Redirigido" : "Desconocido")))) }}
											</span>
										</td>
										<td class="text-center">{{ data.estado }}</td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
				</article>
			</div>
		</section>
		<script src="node_modules/read-excel-file/bundle/read-excel-file.min.js"></script>
		<script src="node_modules/xlsx/dist/xlsx.full.min.js"></script>
		<script src="Librerias/vue/vue.min.js"></script>
		<script src="Librerias/vue/axios.min.js"></script>
		<script type="module" src="vue/herramientas_mipg/tareas_ideal/buscar/mipg-tereasIdealBuscar.js?<?= date('d_m_Y_h_i_s');?>"></script>
	</body>
</html>