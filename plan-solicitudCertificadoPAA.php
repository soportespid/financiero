<?php
	header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=utf8");
	require 'comun.inc';
	require 'funciones.inc';
	session_start();
	$linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");
	cargarcodigopag($_GET['codpag'],$_SESSION["nivel"]);
	date_default_timezone_set("America/Bogota");
?>
<!DOCTYPE >
<html lang=es>
	<head>
    <meta charset="utf-8"/>
		<meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1"/>
		<meta name="viewport" content="user-scalable=no">
		<title>:: IDEAL 10 - Planeación</title>
		<link href="favicon.ico" rel="shortcut icon"/>
		<link href="css/css2z.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
		<link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
		<link href="css/css4.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
        <link href="css/tabs.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css"/>
        <link rel="stylesheet" href="sweetalert2/dist/sweetalert2.min.css">
        <script type="text/javascript" src="css/programas.js"></script>
        <script src="sweetalert2/dist/sweetalert2.min.js"></script>
		<script src="vue/vue.min.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script src="css/programas.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script src="css/calendario.js?<?php echo date('d_m_Y_h_i_s');?>"></script>

        <style>
            [v-cloak]{
                display : none;
            }
        </style>
	</head>
	<body>
		<header>
			<table>
				<tr><script>barra_imagenes("plan");</script><?php cuadro_titulos();?></tr>
			</table>
		</header>

		<form name="form2" method="post" action="">
			<section id="myapp" v-cloak>
				<nav>
					<table>
						<tr><?php menu_desplegable("plan");?></tr>
						<tr>
							<td colspan="3" class="cinta">
								<img src="imagenes/add.png" v-on:click="location.href='plan-solicitudCertificadoPAA'" class="mgbt" title="Nuevo">
								<img src="imagenes/guarda.png" @click="guardar" title="Guardar"  class="mgbt">
								<img src="imagenes/busca.png" v-on:click="location.href='plan-buscaCertificadoPAA'" class="mgbt" title="Buscar">
								<img src="imagenes/nv.png" onClick="mypop=window.open('plan-principal','',''); mypop.focus();" class="mgbt" title="Nueva Ventana">
							</td>
						</tr>
					</table>
				</nav>

				<article>
                    <div>
                        <table class="inicio">
                            <tr>
                                <td class="titulos" colspan="6">.: Datos de plan de adquisición</td>
                            </tr>
                            
                            <tr>
                                <td class="textonew01" style="width:10%;">.: Fecha solicitud:</td>
                                <td style="width: 11%;">
                                    <input type="text" name="fechaSolicitud"  value="<?php echo $_POST['fechaSolicitud']?>" onKeyUp="return tabular(event,this)" id="fechaSolicitud" title="DD/MM/YYYY" onDblClick="displayCalendarFor('fechaSolicitud');" class="colordobleclik" autocomplete="off" onChange=""  readonly>
                                </td>
                                
                                <td class="textonew01" style="width:10%;">.: Código PAA:</td>
                                <td style="width: 11%;">
                                    <input type="text" v-model="codPaa" @dblclick="despliegaModalPlan" class="colordobleclik">
                                </td>

                                <td class="textonew01" style="width:10%;">.: Descripción:</td>
                                <td>
                                    <textarea v-model="descripcionPaa" style="width:100%" readonly></textarea>
                                </td>
                            <tr>

                            <tr>
                                <td class="textonew01" style="width:10%;">.: Observaciones:</td>
                                <td colspan="5">
                                    <textarea v-model="observacion" style="width:100%"></textarea>
                                </td>
                            </tr>
                        </table>

                        <div class='subpantalla' style='height:50vh; width:100%; margin-top:0px;  overflow-x:hidden'>
                            <table class='inicio'>        
                                <thead>
                                    <tr>
                                        <th class="titulosnew00" style="width: 20%;"> <input type="checkbox" v-model="select_all" @click="seleccionaTodos"> Solicitar todos</th>
                                        <th class="titulosnew00" style="width: 20%;">Código Producto</th>
                                        <th class="titulosnew00">Nombre</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr v-for="(produc,index) in productos" :key="produc.id" v-bind:class="index % 2 ? 'saludo1a' : 'saludo2'">
                                        <td style="width:5%; text-align:center;">
                                            <input type="checkbox" :value="produc.id" v-model="selected">
                                        </td>
                                        <td style="width:5%; text-align:center;"> {{ produc.id }} </td>
                                        <td style="text-align:center;"> {{ produc.nombre }} </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>

                    <div v-show="showpaa">
						<transition name="modal">
							<div class="modal-mask">
								<div class="modal-wrapper">
									<div class="modal-container2">
										<table class="inicio ancho">
											<tr>
												<td class="titulos" colspan="2" >Planes de adquisición anual</td>
												<td class="cerrar" style="width:7%" @click="showpaa = false">Cerrar</td>
											</tr>
                                            <tr>
                                                <td>
                                                    <input type="text" v-model="searchPlan" v-on:keyup="filtroPaa" placeholder="Buscar por código o descripcion" style="width: 100%;">
                                                </td>
                                            </tr>
										</table>
										<table class='tablamv'>
											<thead>
												<tr style="text-align:center;">
													<th class="titulosnew00" style="width: 5%;">Código</th>
													<th class="titulosnew00">Descripción</th>
                                                    <th class="titulosnew00" style="width: 25%;">Códigos UNSPSCS</th>
                                                    <th class="titulosnew00" style="width: 25%;">Clasificadores</th>
                                                    <th style="width: 1%;"></th>
												</tr>
											</thead>
											<tbody>
												<tr v-for="(plan,index) in planes" v-bind:class="index % 2 ? 'saludo1a' : 'saludo2'" v-on:click="selecPaa(plan)">
													<td style="width:5%; text-align:center;"> {{ plan[0] }} </td>
													<td style="text-align:left;"> {{ plan[1] }} </td>
                                                    <td style="width:25%; text-align:center;"> {{ plan[2] }} </td>
                                                    <td style="width:25%; text-align:center;"> {{ plan[3] }} </td>
												</tr>
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</transition>
					</div> 


                    <div id="cargando" v-if="loading" class="loading">
						<span>Cargando...</span>
					</div>
				</article>
			</section>
		</form>

		<script src="node_modules/read-excel-file/bundle/read-excel-file.min.js"></script>
		<script src="node_modules/xlsx/dist/xlsx.full.min.js"></script>
        <script src="Librerias/vue/vue.min.js"></script>
		<script src="Librerias/vue/axios.min.js"></script>
		<script type="module" src="planeacion/solicitudCertificadoPAA/crear/plan-solicitudCertificadoPAA.js"></script>
        
	</body>
</html>