<?php
	header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=utf8");
	require "comun.inc";
	require "funciones.inc";
	session_start();
	$linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");
	cargarcodigopag($_GET['codpag'],$_SESSION["nivel"]);
	date_default_timezone_set("America/Bogota");
?>
<!DOCTYPE >
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="es">
	<head>
		<meta http-equiv="Content-Type" content="text/html" charset="utf8"/>
		<meta http-equiv="X-UA-Compatible" content="IE=9"/>
		<title>:: IDEAL 10 - Gesti&oacute;n humana</title>
		<link href="css/css2.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<link href="css/css4.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<script type="text/javascript" src="css/programas.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script>
			function agregardetalle()
			{
				if(document.form2.funcionamiento.value != '-1' && document.form2.funtemporal.value != '-1' && document.form2.inversion.value != '-1' && document.form2.invtemporal.value != '-1')
				{
					document.form2.agregadet.value = 1;
					document.form2.submit();
				}
				else{despliegamodalm('visible','2','Faltan datos para completar el registro');}
			}
			function eliminar(variable)
			{
					document.form2.elimina.value = ariable;
					despliegamodalm('visible','4','Esta Seguro de Eliminar Detalle','1');
			}
			function guardar()
			{
				var validacion01=document.getElementById('nombre').value;
				if (document.form2.codigo.value != '' && validacion01.trim() != '' && document.form2.condeta.value > 0)
				{despliegamodalm('visible','4','Esta Seguro de Guardar','2')}
				else{despliegamodalm('visible','2','Faltan datos para completar el registro');}
			}
			function despliegamodal2(_valor)
			{
				document.getElementById("bgventanamodal2").style.visibility = _valor;
				if(_valor == "hidden"){document.getElementById('ventana2').src = "";}
				else 
				{document.getElementById('ventana2').src = "scuentasppto-ventana01.php?ti=2";}
			}
			function despliegamodalm(_valor,_tip,mensa,pregunta)
			{
				document.getElementById("bgventanamodalm").style.visibility = _valor;
				if(_valor == "hidden")
				{
					document.getElementById('ventanam').src = "";
					if (document.getElementById('valfocus').value != "0")
					{
						document.getElementById('valfocus').value = '0';
						document.getElementById('cuentap').focus();
						document.getElementById('cuentap').select();
					}
				}
				else
				{
					switch(_tip)
					{
						case "1":	document.getElementById('ventanam').src = "ventana-mensaje1.php?titulos="+mensa;break;
						case "2":	document.getElementById('ventanam').src = "ventana-mensaje3.php?titulos="+mensa;break;
						case "3":	document.getElementById('ventanam').src = "ventana-mensaje2.php?titulos="+mensa;break;
						case "4":	document.getElementById('ventanam').src = "ventana-consulta1.php?titulos=" + mensa + "&idresp=" + pregunta;break;
					}
				}
			}
			function funcionmensaje(){document.location.href = "hum-tablasparafiscalesccpet.php";}
			function respuestaconsulta(pregunta)
			{
				switch(pregunta)
				{
					case "1":	if(document.getElementById('condeta').value == '1'){document.getElementById('bloqueo01').value = '0';}
								document.getElementById('oculto').value = "6";
								document.form2.submit();break;
					case "2":	document.form2.oculto.value = "2";
								document.form2.submit();break;
				}
			}
		</script>
		<?php titlepag();?>
	</head>
	<body>
		<IFRAME src="alertas.php" name="alertas" id="alertas" style="display:none"></IFRAME>
		<span id="todastablas2"></span>
		<table>
			<tr><script>barra_imagenes("hum");</script><?php cuadro_titulos();?></tr>
			<tr><?php menu_desplegable("hum");?></tr>
			<tr>
				<td colspan="3" class="cinta"><img src="imagenes/add.png" title="Nuevo" onClick="location.href='hum-tablasparafiscalesccpet.php'" class="mgbt"><img src="imagenes/guarda.png" title="Guardar" onClick="guardar();" class="mgbt"><img src="imagenes/busca.png" title="Buscar" onClick="location.href='hum-buscaparafiscalesccpet.php'" class="mgbt"><img src="imagenes/agenda1.png" title="Agenda" onClick="mypop=window.open('plan-agenda.php','','');mypop.focus()" class="mgbt"><img src="imagenes/nv.png" title="Nueva ventana" onClick="mypop=window.open('hum-principal.php','','');mypop.focus();" class="mgbt"><img src="imagenes/duplicar_pantalla.png" title="Duplicar pesta&ntilde;a" onClick="mypop=window.open('<?php echo $url2; ?>','','');mypop.focus();" class="tooltip bottom mgbt"><img src="imagenes/iratras.png" title="Atr&aacute;s" onClick="location.href='hum-buscaparafiscalesccpet.php'" class="mgbt"></td>
			</tr>
		</table>
		<div id="bgventanamodalm" class="bgventanamodalm">
			<div id="ventanamodalm" class="ventanamodalm">
				<IFRAME src="" name="ventanam" marginWidth=0 marginHeight=0 frameBorder=0 id="ventanam" frameSpacing=0 style=" width:700px; height:130px; top:200; overflow:hidden;"> 
				</IFRAME>
			</div>
		</div>
		<form name="form2" method="post" action="">
			<input type="hidden" name="valfocus" id="valfocus" value="1"/>
			<?php
				$vigencia = date(Y);
				$vigusu = vigencia_usuarios($_SESSION['cedulausu']);
				$vigencia = $vigusu;
				if(!$_POST['oculto'])
				{
					$fec = date("d/m/Y");
					$_POST['fecha'] = $fec;
					$_POST['tipo']='S';
					$_POST['valoradicion'] = 0;
					$_POST['valorreduccion'] = 0;
					$_POST['valortraslados'] = 0;
					$_POST['valor'] = 0;
					$sqlr = "SELECT MAX(RIGHT(codigo,2)) FROM humparafiscalesccpet ORDER BY codigo DESC";
					$res = mysqli_query($linkbd,$sqlr);
					$row = mysqli_fetch_row($res);
					$_POST['codigo'] = $row[0] + 1;
					if(strlen($_POST['codigo']) == 1){$_POST['codigo'] = '0'.$_POST['codigo'];}
				}
			?>
			<table class="inicio ancho">
				<tr>
					<td class="titulos" colspan="8">.: Agregar Parafiscal</td>
					<td class="cerrar" style="width:7%;"><a href="hum-principal.php">&nbsp;Cerrar</a></td>
				</tr>
				<tr>
					<td class="saludo1" style="width:7%;">C&oacute;digo:</td>
					<td style="width:6%;"><input type="text" name="codigo" id="codigo" value="<?php echo @ $_POST['codigo']?>" maxlength="2"  onKeyPress="javascript:return solonumeros(event)" onKeyUp="return tabular(event,this)" style="width:98%;"></td>
					<td class="saludo1" style="width:7%;">Nombre:</td>
					<td style="width:35%;"><input type="text" name="nombre" id="nombre" value="<?php echo @ $_POST['nombre']?>" onKeyUp="return tabular(event,this)" style="width:99%;"></td>
					<td class="saludo1" style="width:2%;">%</td>
					<td style="width:8%;"><input type="text" name="porcentaje" id="porcentaje" value="<?php echo @ $_POST['porcentaje']?>" onKeyUp="return tabular(event,this)" onKeyPress="javascript:return solonumeros(event)" style="width:90%;"></td>
					<td class="saludo1" style="width:7%;">Tipo:</td>
					<td>
						<select name="tipo" id="tipo" >
							<option value=''>Seleccione</option>
							<option value='D' <?php if($_POST['tipo'] == 'D') echo 'SELECTED'; ?>>Descuento</option>
							<option value='A' <?php if($_POST['tipo'] == 'A') echo 'SELECTED'; ?>>Aporte</option>
						</select>
					</td>
				</tr>
			</table>
			<input type="hidden" name="oculto" id="oculto" value="1">
			<table class="inicio ancho">
				<tr><td colspan='8' class='titulos'>Agregar detalle variable de pago</td></tr>
				<tr>
					<td class="tamano01" style="width: 4cm;">Funcionamiento: </td>
					<td style="width: 25%;">
						<select name="funcionamiento" id="funcionamiento" onChange="validar();" style="width:100%;" class="tamano02">
							<option value="-1">Seleccione...</option>
							<?php
								$sqlr="SELECT codigo,nombre,tipo FROM conceptoscontables WHERE modulo = '2' AND tipo = 'F' ORDER BY codigo";
								$resp = mysqli_query($linkbd,$sqlr);
								while ($row = mysqli_fetch_row($resp))
								{
									if($row[0] == $_POST['funcionamiento'])
									{
										echo "<option value='$row[0]' SELECTED>$row[0] - $row[2] - $row[1]</option>";
										$_POST['funcionamientonom'] = "$row[0] - $row[2] - $row[1]";
									}
									else{echo "<option value='$row[0]'>$row[0] - $row[2] - $row[1]</option>";}
								}
							?>
						</select>
						<input type="hidden" name="funcionamientonom" id="funcionamientonom" value="<?php echo $_POST['funcionamientonom']?>">
					</td>
					<td class="tamano01" style="width: 4.5cm;">Funcionamiento Temporal: </td>
					<td style="width: 25%;">
						<select name="funtemporal" id="funtemporal" onChange="validar();" style="width:100%;" class="tamano02">
							<option value="-1">Seleccione...</option>
							<?php
								$sqlr="SELECT codigo,nombre,tipo FROM conceptoscontables WHERE modulo = '2' AND tipo = 'FT' ORDER BY codigo";
								$resp = mysqli_query($linkbd,$sqlr);
								while ($row = mysqli_fetch_row($resp))
								{
									if($row[0] == $_POST['funtemporal'])
									{
										echo "<option value='$row[0]' SELECTED>$row[0] - $row[2] - $row[1]</option>";
										$_POST['funtemporalnom'] = "$row[0] - $row[2] - $row[1]";
									}
									else{echo "<option value='$row[0]'>$row[0] - $row[2] - $row[1]</option>";}
								}
							?>
						</select>
						<input type="hidden" name="funtemporalnom" id="funtemporalnom" value="<?php echo $_POST['funtemporalnom']?>">
					</td>
					<td class="tamano01" style="width:6%;">Sector:</td>
					<td>
						<select name='sector' id="sector">
							<option value='N/A' <?php if(@ $_POST['sector'] == 'N/A') echo 'SELECTED'; ?>>N/A</option>
							<option value='publico' <?php if(@ $_POST['sector'] == 'publico') echo 'SELECTED'; ?>>Publico</option>
							<option value='privado' <?php if(@ $_POST['sector'] == 'privado') echo 'SELECTED'; ?>>Privado</option>
						</select>
					</td>
				</tr>
				<tr>
					<td class="tamano01" >Inversi&oacute;n: </td>
					<td>
						<select name="inversion" id="inversion" onChange="validar();" style="width:100%;" class="tamano02">
							<option value="-1">Seleccione...</option>
							<?php
								$sqlr="SELECT codigo,nombre,tipo FROM conceptoscontables WHERE modulo = '2' AND tipo = 'IN' ORDER BY codigo";
								$resp = mysqli_query($linkbd,$sqlr);
								while ($row = mysqli_fetch_row($resp))
								{
									if($row[0] == $_POST['inversion'])
									{
										echo "<option value='$row[0]' SELECTED>$row[0] - $row[2] - $row[1]</option>";
										$_POST['inversionnom'] = "$row[0] - $row[2] - $row[1]";
									}
									else{echo "<option value='$row[0]'>$row[0] - $row[2] - $row[1]</option>";}
								}
							?>
						</select>
						<input type="hidden" name="inversionnom" id="inversionnom" value="<?php echo @ $_POST['inversionnom']?>">
					</td>
					<td class="tamano01">Inversi&oacute;n Temporal: </td>
					<td>
						<select name="invtemporal" id="invtemporal" onChange="validar();" style="width:100%;" class="tamano02">
							<option value="-1">Seleccione...</option>
							<?php
								$sqlr="SELECT codigo,nombre,tipo FROM conceptoscontables WHERE modulo = '2' AND tipo = 'IT' ORDER BY codigo";
								$resp = mysqli_query($linkbd,$sqlr);
								while ($row = mysqli_fetch_row($resp))
								{
									if($row[0] == $_POST['invtemporal'])
									{
										echo "<option value='$row[0]' SELECTED>$row[0] - $row[2] - $row[1]</option>";
										$_POST['invtemporalnom'] = "$row[0] - $row[2] - $row[1]";
									}
									else{echo "<option value='$row[0]'>$row[0] - $row[2] - $row[1]</option>";}
								}
							?>
						</select>
						<input type="hidden" name="invtemporalnom" id="invtemporalnom" value="<?php echo @ $_POST['invtemporalnom']?>">
					</td>
				</tr>
				<tr>
					<td class="tamano01" >Gastos Comercialización </td>
					<td>
						<select name="gastoscomer" id="gastoscomer" onChange="validar();" style="width:100%;" class="tamano02">
							<option value="-1">Seleccione...</option>
							<?php
								$sqlr = "SELECT codigo, nombre, tipo FROM conceptoscontables WHERE modulo = '2' AND tipo = 'CP' ORDER BY codigo";
								$resp = mysqli_query($linkbd,$sqlr);
								while ($row = mysqli_fetch_row($resp))
								{
									if($row[0] == $_POST['gastoscomer'])
									{
										echo "<option value='$row[0]' SELECTED>$row[0] - $row[2] - $row[1]</option>";
										$_POST['gastoscomernom'] = "$row[0] - $row[2] - $row[1]";
									}
									else{echo "<option value='$row[0]'>$row[0] - $row[2] - $row[1]</option>";}
								}
							?>
						</select>
						<input type="hidden" name="gastoscomernom" id="gastoscomernom" value="<?php echo @ $_POST['gastoscomernom']?>">
					</td>
					<td class="tamano01">Gastos Comercialización Temporal: </td>
					<td>
						<select name="gastoscomertem" id="gastoscomertem" onChange="validar();" style="width:100%;" class="tamano02">
							<option value="-1">Seleccione...</option>
							<?php
								$sqlr="SELECT codigo, nombre, tipo FROM conceptoscontables WHERE modulo = '2' AND tipo = 'CT' ORDER BY codigo";
								$resp = mysqli_query($linkbd,$sqlr);
								while ($row = mysqli_fetch_row($resp))
								{
									if($row[0]==$_POST['gastoscomertem'])
									{
										echo "<option value='$row[0]' SELECTED>$row[0] - $row[2] - $row[1]</option>";
										$_POST['gastoscomertemnom'] = "$row[0] - $row[2] - $row[1]";
									}
									else{echo "<option value='$row[0]'>$row[0] - $row[2] - $row[1]</option>";}
								}
							?>
						</select>
						<input type="hidden" name="gastoscomertemnom" id="gastoscomertemnom" value="<?php echo @ $_POST['gastoscomertemnom']?>">
					</td>
					<td style="padding-bottom:0px" colspan="2"><em class="botonflecha" onClick="agregardetalle();">Agregar</em></td>
				</tr>
			</table>
			<div class="subpantalla" style="height:43.5%; width:99.6%; overflow-x:hidden;">
				<table  class="inicio ancho">
					<tr><td class="titulos" colspan="10">Detalle variable - Parametrizaci&oacute;n</td></tr>
					<tr class="titulos2">
						<td style='width:15%;'>Funcionamiento</td>
						<td style='width:15%;'>Funcionamiento Temporal</td>
						<td style='width:15%;'>Inversi&oacute;n</td>
						<td style='width:15%;'>Inversi&oacute;n Temporal</td>
						<td style='width:15%;'>Gastos Comercialización</td>
						<td style='width:15%;'>Gastos Comercialización Temporal</td>
						<td>Sector</td>
						<td style='width:5%;'>Eliminar<input type='hidden' name='elimina' id='elimina'></td>
					</tr>
					<?php
						if ($_POST['oculto']=='6')
						{ 
							$posi=$_POST['elimina'];
							unset($_POST['idfuncionamiento'][$posi]);
							unset($_POST['nomfuncionamiento'][$posi]);
							unset($_POST['idfuncionamientotemp'][$posi]);
							unset($_POST['nomfuncionamientotemp'][$posi]);
							unset($_POST['idinversion'][$posi]);
							unset($_POST['nominversion'][$posi]);
							unset($_POST['idinversiontemp'][$posi]);
							unset($_POST['nominversiontemp'][$posi]);
							unset($_POST['idgastoscomer'][$posi]);
							unset($_POST['nomgastoscomer'][$posi]);
							unset($_POST['idgastoscomertem'][$posi]);
							unset($_POST['nomgastoscomertem'][$posi]);
							unset($_POST['idsector'][$posi]);
							$_POST['idfuncionamiento'] = array_values($_POST['idfuncionamiento']);
							$_POST['nomfuncionamiento'] = array_values($_POST['nomfuncionamiento']);
							$_POST['idfuncionamientotemp'] = array_values($_POST['idfuncionamientotemp']);
							$_POST['nomfuncionamientotemp'] = array_values($_POST['nomfuncionamientotemp']);
							$_POST['idinversion'] = array_values($_POST['idinversion']);
							$_POST['nominversion'] = array_values($_POST['nominversion']);
							$_POST['idinversiontemp'] = array_values($_POST['idinversiontemp']); 
							$_POST['nominversiontemp'] = array_values($_POST['nominversiontemp']);
							$_POST['idgastoscomer'] = array_values($_POST['idgastoscomer']);
							$_POST['nomgastoscomer'] = array_values($_POST['nomgastoscomer']);
							$_POST['idgastoscomertem'] = array_values($_POST['idgastoscomertem']); 
							$_POST['nomgastoscomertem'] = array_values($_POST['nomgastoscomertem']);
						}
						if ($_POST['agregadet']=='1')
						{
							$_POST['idfuncionamiento'][]=$_POST['funcionamiento'];
							$_POST['nomfuncionamiento'][]=$_POST['funcionamientonom'];
							$_POST['idfuncionamientotemp'][]=$_POST['funtemporal'];
							$_POST['nomfuncionamientotemp'][]=$_POST['funtemporalnom'];
							$_POST['idinversion'][]=$_POST['inversion'];
							$_POST['nominversion'][]=$_POST['inversionnom'];
							$_POST['idinversiontemp'][]=$_POST['invtemporal'];
							$_POST['nominversiontemp'][]=$_POST['invtemporalnom'];
							$_POST['idgastoscomer'][]=$_POST['gastoscomer'];
							$_POST['nomgastoscomer'][]=$_POST['gastoscomernom'];
							$_POST['idgastoscomertem'][]=$_POST['gastoscomertem'];
							$_POST['nomgastoscomertem'][]=$_POST['gastoscomertemnom'];
							$_POST['idsector'][]=$_POST['sector'];
							echo"
							<script>
								document.form2.funcionamiento.value='-1';
								document.form2.funcionamientonom.value='';
								document.form2.funtemporal.value='-1';
								document.form2.funtemporalnom.value='';
								document.form2.inversion.value='-1';
								document.form2.inversionnom.value='';
								document.form2.invtemporal.value='-1';
								document.form2.invtemporalnom.value='';
								document.form2.gastoscomer.value='-1';
								document.form2.gastoscomernom.value='';
								document.form2.gastoscomertem.value='-1';
								document.form2.gastoscomertemnom.value='';
								document.form2.sector.value='N/A';
							</script>";
						}
						$iter='saludo1a';
						$iter2='saludo2';
						$_POST['condeta']=$cdtll=count($_POST['idfuncionamiento']);
						for ($x=0;$x< $cdtll;$x++)
						{
							echo"
							<input type='hidden' name='idfuncionamiento[]' value='".$_POST['idfuncionamiento'][$x]."'/>
							<input type='hidden' name='nomfuncionamiento[]' value='".$_POST['nomfuncionamiento'][$x]."'/>
							<input type='hidden' name='idfuncionamientotemp[]' value='".$_POST['idfuncionamientotemp'][$x]."'/>
							<input type='hidden' name='nomfuncionamientotemp[]' value='".$_POST['nomfuncionamientotemp'][$x]."'/>
							<input type='hidden' name='idinversion[]' value='".$_POST['idinversion'][$x]."'/>
							<input type='hidden' name='nominversion[]' value='".$_POST['nominversion'][$x]."'/>
							<input type='hidden' name='idinversiontemp[]' value='".$_POST['idinversiontemp'][$x]."'/>
							<input type='hidden' name='nominversiontemp[]' value='".$_POST['nominversiontemp'][$x]."'/>
							<input type='hidden' name='idgastoscomer[]' value='".$_POST['idgastoscomer'][$x]."'/>
							<input type='hidden' name='nomgastoscomer[]' value='".$_POST['nomgastoscomer'][$x]."'/>
							<input type='hidden' name='idgastoscomertem[]' value='".$_POST['idgastoscomertem'][$x]."'/>
							<input type='hidden' name='nomgastoscomertem[]' value='".$_POST['nomgastoscomertem'][$x]."'/>
							<input type='hidden' name='idsector[]' value='".$_POST['idsector'][$x]."'/>
							<tr class='$iter'>
								<td>".$_POST['nomfuncionamiento'][$x]."</td>
								<td>".$_POST['nomfuncionamientotemp'][$x]."</td>
								<td>".$_POST['nominversion'][$x]."</td>
								<td>".$_POST['nominversiontemp'][$x]."</td>
								<td>".$_POST['nomgastoscomer'][$x]."</td>
								<td>".$_POST['nomgastoscomertem'][$x]."</td>
								<td>".$_POST['idsector'][$x]."</td>
								<td><a href='#' onclick='eliminar($x)'><img src='imagenes/del.png'></a></td>
							</tr>";
							$aux=$iter;
							$iter=$iter2;
							$iter2=$aux;
						}
					?>
				</table>
			</div>
			<input type="hidden" name="agregadet" value="0">
			<input type="hidden" name="condeta" id="condeta" value="<?php echo $_POST['condeta'];?>"/>
			<?php
				if($_POST['oculto']=='2')
				{
					$numid = selconsecutivo('humparafiscalesccpet','id');
					$sqlr = "INSERT INTO humparafiscalesccpet (id, codigo, nombre, tipo, porcentaje, estado)VALUES ('$numid', '".$_POST['codigo']."', '".$_POST['nombre']."', '".$_POST['tipo']."', '".$_POST['porcentaje']."', 'S')";
					if (!mysqli_query($linkbd,$sqlr))
					{
						echo "<script>despliegamodalm('visible','2','Manejador de errores BD, no se pudo ejecutar la petición humparafiscales');</script>";
					}
					else
					{
						//****COMPUESTO
						for($x=0; $x < $_POST['condeta']; $x++)
						{
							$numid_det = selconsecutivo('humparafiscalesccpet_det','id_det');
							$sqlr = "INSERT INTO humparafiscalesccpet_det (id_det, codigo, funcionamiento, funcionamientotemp, inversion, inversiontemp, gastoscomerc, gastoscomerctemp, sector, estado)VALUES ('$numid_det', '".$_POST['codigo']."', '".$_POST['idfuncionamiento'][$x]."', '".$_POST['idfuncionamientotemp'][$x]."', '".$_POST['idinversion'][$x]."', '".$_POST['idinversiontemp'][$x]."', '".$_POST['idgastoscomer'][$x]."', '".$_POST['idgastoscomertem'][$x]."', '".$_POST['idsector'][$x]."', 'S')";
							mysqli_query($linkbd,$sqlr);
						}//***** fin del for	
						echo "<script>despliegamodalm('visible','1','Se ha almacenado el detalle de la variable con éxito');</script>";
					}
				}
			?>
			<div id="bgventanamodal2">
				<div id="ventanamodal2">
					<IFRAME  src="" name="buster" marginWidth=0 marginHeight=0 frameBorder=0 id="ventana2" frameSpacing=0 style="left:500px; width:900px; height:500px; top:200;"> 
					</IFRAME>
				</div>
			</div>
		</form>
	</body>
</html>
