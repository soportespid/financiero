<?php

    header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=utf8");
	require 'comun.inc';
	require 'funciones.inc';
	session_start();
    if(empty($_SESSION)){
        header("location: index.php");
    }
	$linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");
	cargarcodigopag($_GET['codpag'],$_SESSION["nivel"]);
	date_default_timezone_set("America/Bogota");
?>
<!DOCTYPE >
<html lang=es>
	<head>
    <meta charset="utf-8"/>
		<meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1"/>
		<meta name="viewport" content="user-scalable=no">
		<title>:: IDEAL 10 - Tesorería</title>
		<link href="favicon.ico" rel="shortcut icon"/>
		<link href="css/css2z.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
		<link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
		<link href="css/css4.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
        <link href="css/tabs.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css"/>
        <link href="css/style.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css"/>
        <link rel="stylesheet" href="sweetalert2/dist/sweetalert2.min.css">
        <script type="text/javascript" src="css/programas.js"></script>
        <script src="sweetalert2/dist/sweetalert2.min.js"></script>
		<script src="vue/vue.min.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script src="css/programas.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script src="css/calendario.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
	</head>
	<body>
		<header>
			<table>
				<tr><script>barra_imagenes("teso");</script><?php cuadro_titulos();?></tr>
			</table>
		</header>

		<form name="form2" method="post" action="">
			<section id="myapp" v-cloak >
                <div id="cargando" v-show="isLoading" class="loading" style="z-index: 10000;">
                    <span>Cargando...</span>
                </div>
				<nav>
					<table>
						<tr><?php menu_desplegable("teso");?></tr>
                        <tr>
							<td colspan="3" class="cinta">
                                <img src="imagenes/add2.png"  class="mgbt" title="Nuevo">
								<img @click="save()" src="imagenes/guarda.png"   title="Guardar"  class="mgbt">
								<img src="imagenes/busca.png" @click="window.location.href='teso-modificarBancoRecaudoBuscar.php'"   class="mgbt" title="Buscar">
								<img src="imagenes/nv.png" @click="mypop=window.open('teso-principal.php','',''); mypop.focus();" class="mgbt" title="Nueva Ventana">
                                <a href="teso-modificarBancoRecaudoBuscar.php"  class="mgbt"><img src="imagenes/iratras.png" title="Atr&aacute;s"></a>
							</td>
						</tr>
					</table>
				</nav>
				<article>
                    <div class="inicio">
                        <div>
                            <h2 class="titulos m-0">.: Cambio de banco para recaudo por transferencia</h2>
                            <div class="d-flex justify-between w-75">
                                <div class="form-control w-25">
                                    <label class="form-label">.: No Recaudo:</label>
                                    <div class="d-flex">
                                        <button v-show="txtConsecutivo > 1" type="button" class="btn btn-primary" @click="editItem(--objDato.id_recaudo)"><</button>
                                        <input type="text"  style="text-align:center;" v-model="objDato.id_recaudo" @change="editItem(objDato.id_recaudo)">
                                        <button v-show="txtConsecutivo < txtMax" type="button" class="btn btn-primary" @click="editItem(++objDato.id_recaudo)">></button>
                                    </div>
                                </div>
                                <div class="form-control w-25">
                                    <label class="form-label">.: No liquidación:</label>
                                    <input type="text"  style="text-align:center;" v-model="objDato.idcomp" disabled readonly>
                                </div>
                                <div class="form-control w-25">
                                    <label class="form-label">.: Estado:</label>
                                    <input type="text"  style="text-align:center;" v-model="objDato.estado" disabled readonly>
                                </div>
                                <div class="form-control w-25">
                                    <label class="form-label">.: Documento:</label>
                                    <input type="text"  v-model="objDato.tercero.documento" disabled readonly>
                                </div>
                                <div class="form-control">
                                    <label class="form-label">.: Nombre:</label>
                                    <input type="text"  v-model="objDato.tercero.nombre" disabled readonly>
                                </div>
                            </div>
                            <div class="d-flex w-75">
                                <div class="form-control w-25">
                                    <label class="form-label" for="">.: Fecha:</label>
                                    <input type="text" v-model="objDato.fecha" disabled readonly>
                                </div>
                                <div class="form-control">
                                    <label class="form-label" for="">.: Concepto:</label>
                                    <input type="text" v-model="objDato.concepto" disabled readonly>
                                </div>
                            </div>
                            <div class="d-flex w-75">
                                <div class="form-control">
                                    <label class="form-label" for="">.: Cuenta bancaria <span class="text-danger fw-bolder">*</span>:</label>
                                    <div class="d-flex">
                                        <input type="text" class="w-25 colordobleclik" @dblclick="isModal=true" @change="search('codigo_cuenta')" v-model="objCuenta.ncuentaban">
                                        <input type="text" class="w-25"  :value="objCuenta.tipo" disabled readonly>
                                        <input type="text" :value="objCuenta.razonsocial" disabled readonly>
                                    </div>
                                </div>
                                <div class="form-control w-25">
                                    <label class="form-label" for="">.: Valor:</label>
                                    <input type="text" v-model="objDato.valor" disabled readonly>
                                </div>
                            </div>
                            <div class="d-flex w-75">
                                <div class="form-control">
                                    <label class="form-label" for="">.: Causa de cambio <span class="text-danger fw-bolder">*</span>:</label>
                                    <input type="text" v-model="strCausaCambio">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="inicio">
                        <h2 class="titulos m-0">.: Detalle recibo de caja</h2>
                        <div class="overflow-auto max-vh-50 overflow-x-hidden p-2" >
                            <table class="table text-center fw-normal">
                                <thead>
                                    <tr>
                                        <th>Código</th>
                                        <th>Ingreso</th>
                                        <th>Valor</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr v-for="(data,index) in arrDetalle" :key="index">
                                        <td>{{data.codigo}}</td>
                                        <td class="text-left">{{data.nombre}}</td>
                                        <td class="text-right">{{data.valor}}</td>
                                    </tr>
                                    <tr>
                                        <td colspan="2" class="text-right fw-bold">Total:</td>
                                        <td class="text-right fw-bold">{{objDato.valor}}</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
				</article>
                <!--MODALES-->
                <div v-show="isModal">
                    <transition name="modal">
                        <div class="modal-mask">
                            <div class="modal-wrapper">
                                <div class="modal-container">
                                    <table class="inicio ancho">
                                        <tr>
                                            <td class="titulos" colspan="2" >.: Buscar cuenta bancaria</td>
                                            <td class="cerrar" style="width:7%" @click="isModal = false">Cerrar</td>
                                        </tr>
                                    </table>
                                    <div class="bg-white">
                                        <div class="form-control m-0 p-2 w-inherit">
                                            <input type="search" placeholder="Buscar" @keyup="search('modal_cuenta')" v-model="txtSearch">
                                        </div>
                                        <p class="fw-bolder m-0 p-2">Resultados de búsqueda: {{ txtResults}} </p>
                                        <div class="overflow-auto max-vh-50 overflow-x-hidden p-2">
                                            <table class="table table-hover fw-normal">
                                                <thead>
                                                    <tr>
                                                        <th>Nombre</th>
                                                        <th>Cuenta</th>
                                                        <th>Cuenta bancaria</th>
                                                        <th>Tipo</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr @dblclick="selectItemModal(data,'cuenta')" v-for="(data,index) in arrModalCuentas" :key="index">
                                                        <td>{{data.razonsocial}}</td>
                                                        <td>{{data.cuenta}}</td>
                                                        <td>{{data.ncuentaban}}</td>
                                                        <td>{{data.tipo}}</td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </transition>
                </div>
			</section>
		</form>
		<script src="node_modules/read-excel-file/bundle/read-excel-file.min.js"></script>
		<script src="node_modules/xlsx/dist/xlsx.full.min.js"></script>
        <script src="Librerias/vue/vue.min.js"></script>
		<script src="Librerias/vue/axios.min.js"></script>
		<script type="module" src="tesoreria/modificarbanco_transferencia/editar/teso-transferenciaEditar.js?<?= date('d_m_Y_h_i_s');?>"></script>
	</body>
</html>
