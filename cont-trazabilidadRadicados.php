<?php
		require 'comun.inc';
		require 'funciones.inc';
		session_start();
		cargarcodigopag($_GET['codpag'],$_SESSION['nivel']);
		header('Cache-control: private'); // Arregla IE 6
		header("Cache-control: no-cache, no-store, must-revalidate");
		header("Content-Type: text/html;charset=utf8");
		date_default_timezone_set('America/Bogota');
        $_POST['fechaIni'] = isset($_POST['fechaIni']) && $_POST['fechaIni'] != "" ? $_POST['fechaIni'] : "01/01/".date("Y");
        $_POST['fechaFin'] = isset($_POST['fechaFin']) && $_POST['fechaFin'] != "" ? $_POST['fechaFin'] : date("d/m/Y");
		titlepag();
?>
<!DOCTYPE >
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="es">
	<head>
		<meta http-equiv="Content-type" content="text/html; charset=utf8"/>
		<meta http-equiv="X-UA-Compatible" content="IE=9"/>
		<meta name="viewport" content="user-scalable=no">
		<title>:: IDEAL 10 - Almacen</title>
		<link href="css/css2.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<link href="css/css4.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<link href="css/cssSP.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<link href="css/tabs.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css"/>
        <script type="text/javascript" src="css/calendario.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script src="vue/vue.js"></script>
		<script type="text/javascript" src="css/programas.js"></script>

		<!-- sweetalert2 -->
		<script src="sweetalert2/dist/sweetalert2.min.js"></script>
		<link rel="stylesheet" href="sweetalert2/dist/sweetalert2.min.css">

		<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">

		<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
		<script type="text/javascript" src="bootstrap/fontawesome.5.11.2/js/all.js"></script>

		<script>
            function cambiaFecha(){
                var fecha = document.getElementById('fecha').value;
                fecArray = fecha.split('/');
                document.getElementById('vigencia').value = fecArray[2];
            }
        </script>

		<style>
			.modal-mask {
			position: fixed;
			z-index: 9998;
			top: 0;
			left: 0;
			width: 100%;
			height: 100%;
			background-color: rgba(0, 0, 0, .5);
			display: table;
			transition: opacity .3s ease;
			}

			.modal-wrapper {
			display: table-cell;
			vertical-align: middle;
			}
			.modal-body{
				max-height: 500px;
				overflow-y: scroll;
			}
			.modal-intermetio{
				margin: 0 15px;
				font-family: helvética !important;
				font-size: 26px !important;
				padding: 10px 0;
			}
			.modal-intermedio-agregar{
				text-align:right;
				padding: 4px;
				margin-top: 6px;
				margin-right: 20px
			}
			.modal-body_1{
				padding-top: 15px;
				height: 40px;
			}
			.loader-table{
				/* background-color: #dff9fb;
				opacity: .5; */
				display: flex;
				align-items: center;
				justify-content: center;
				height: 75%;
			}
			.spinner{
				border: 4px solid rgba(0, 0, 0, 0.2);
				border-left-color: #39C;
				border-radius: 50%;
				width: 50px;
				height: 50px;
				animation: spin .9s linear infinite;
			}
			@keyframes spin {
				to { transform: rotate(360deg); }
			}
			.modal-container1
			{
				width: 50%;
				margin: 0px auto;
				padding: 20px 30px;
				text-align: left;
				background:linear-gradient(#99bbcc, #B6CEDA);
				border-radius: 10px;
				box-shadow: 0 2px 8px rgba(0, 0, 0, .33);
				transition: all .3s ease;
			}

            [v-cloak]{
                display : none;
            }

            label{
                font-size:13px;
            }

            input{
                height: calc(1em + 0.6rem + 0.5px) !important;
                font-size: 14px !important;
                margin-top: 4px !important;
            }

        </style>
	</head>
	<body>
		<div class="subpantalla" style="height:640px; width:99.6%; overflow:hidden; height: 95%;">
			<div id="myapp" style="height:inherit;" v-cloak>
				<div>
					<table>
						<tr>
							<script>barra_imagenes("cont");</script>
							<?php cuadro_titulos();?>
						</tr>

						<tr>
							<?php menu_desplegable("cont");?>
						</tr>

						<tr>
							<td colspan="3" class="cinta">
								<a href="#" class="mgbt"><img src="imagenes/add.png"/></a>
								<a href="#" class="mgbt"><img src="imagenes/guarda.png" title="Guardar"/></a>
								<a href="#" class="mgbt"><img src="imagenes/busca.png" title="Buscar"/></a>
								<a onclick="mypop=window.open('plan-agenda.php','','');mypop.focus()" class="mgbt"><img src="imagenes/agenda1.png" title="Agenda" /></a>
								<a onclick="mypop=window.open('cont-principal.php','','');mypop.focus();" class="mgbt"><img src="imagenes/nv.png" title="Nueva ventana"></a>
								<a onclick="mypop=window.open('<?php echo $url2; ?>','','');mypop.focus();" class="mgbt"><img src="imagenes/duplicar_pantalla.png" title="Duplicar pesta&ntilde;a"></a>
								<a href="cont-estadoComprobantesTrazabilidad.php"><img src="imagenes/iratras.png" title="Atrás"></a>
							</td>
						</tr>
					</table>
				</div>

				<table class="inicio grande">
					<tr>
						<td class="titulos" colspan="6">.: Trazabilidad documentos radicados stock de almacen</td>
						<td class="cerrar" style="width:4%" onClick="location.href='cont-principal.php'">Cerrar</td>
					</tr>

					<tr>
						<td class="tamano01" style="width: 8%;">Fecha Inicial:</td>
						<td style="width:10%;">
							<input type="text" name="fechaIni" id="fechaIni" value="<?=$_POST['fechaIni']?>" onchange="" maxlength="10" onKeyUp="return tabular(event,this)"  onKeyDown="mascara(this,'/',patron,true)" title="DD/MM/YYYY" style="height:30px;text-align:center;width:100%;" onClick="displayCalendarFor('fechaIni');" class="colordobleclik" readonly/>
						</td>

						<td class="tamano01" style="width: 8%;">Fecha Final:</td>
						<td style="width:10%;">
							<input type="text" name="fechaFin" id="fechaFin" value="<?=$_POST['fechaFin']?>" onchange="" maxlength="10" onKeyUp="return tabular(event,this)"  onKeyDown="mascara(this,'/',patron,true)" title="DD/MM/YYYY" style="height:30px;text-align:center;width:100%;" onClick="displayCalendarFor('fechaFin');" class="colordobleclik" readonly/>
						</td>
						<td></td>
						<td colspan='2'>
							<em class='botonflechaverde' v-on:click="buscarInformacion" style='float:rigth;'>Buscar Información</em>
						</td>
					</tr>
				</table>


				<div id="cargando" v-if="loading" class="loading">
					<span>Cargando...</span>
				</div>

                <div class="subpantalla" style="height: 60%; overflow-x:hidden;">
					<table>
						<thead>
							<tr class='titulos table-bordered' style="text-align: center;">
								<th colspan='4'scope="col">Destino compra:</th>
								<th colspan='3'scope="col">Orden de pago</th>
								<th colspan='3'scope="col">Almacen</th>
							</tr>

							<tr class="titulos2 table-bordered" style="text-align: center;">
								<td style="width: 5%;">Codigo destino compra</td>
								<td style="width: 10%;">Fecha</td>
								<td style="width: 5%;">Registro</td>
								<td style="width: 10%;">Valor RP</td>
								<td style="width: 5%;">Cod orden pago</td>
								<td style="width: 10%;">Fecha</td>
								<td style="width: 10%;">Valor</td>
								<td style="width: 5%;">Cod Entrada</td>
								<td style="width: 10%;">Fecha</td>
								<td style="width: 10%;">Valor</td>
							</tr>
						</thead>


						<tbody>
							<tr v-for="(destino,index) in destinosCompra" v-bind:class="index % 2 ? 'saludo1a' : 'saludo2'" style='text-rendering: optimizeLegibility; text-align:center;'>
								<td style="font: 110% sans-serif; padding-left:10px">{{ destino[0] }}</td>
								<td style="font: 110% sans-serif; padding-left:10px">{{ destino[1] }}</td>
								<td style="font: 110% sans-serif; padding-left:10px">{{ destino[2] }}</td>
								<td style="font: 110% sans-serif; padding-left:10px">{{ destino[3] }}</td>
								<td>
									<template v-for="(orden,index) in ordenPagos">
										<tr v-if="orden[0] == destino[2]" style="text-align: center;">
											<td style="font: 110% sans-serif; padding-left:10px">{{ orden[1] }}</td>
										</tr>
									</template>
								</td>
								<td>
									<template v-for="(orden,index) in ordenPagos">
										<tr v-if="orden[0] == destino[2]">
											<td style="font: 110% sans-serif; padding-left:10px">{{ orden[2] }}</td>
										</tr>
									</template>
								</td>
								<td>
									<template v-for="(orden,index) in ordenPagos">
										<tr v-if="orden[0] == destino[2]">
											<td style="font: 110% sans-serif; padding-left:10px">{{ orden[3] }}</td>
										</tr>
									</template>
								</td>

								<td>
									<template v-for="(entrada,index) in entradas">
										<tr v-if="entrada[0] == destino[2]">
											<td style="font: 110% sans-serif; padding-left:10px">{{ entrada[1] }}</td>
										</tr>
									</template>
								</td>

								<td>
									<template v-for="(entrada,index) in entradas">
										<tr v-if="entrada[0] == destino[2]">
											<td style="font: 110% sans-serif; padding-left:10px">{{ entrada[2] }}</td>
										</tr>
									</template>
								</td>

								<td>
									<template v-for="(entrada,index) in entradas">
										<tr v-if="entrada[0] == destino[2]">
											<td style="font: 110% sans-serif; padding-left:10px">{{ entrada[3] }}</td>
										</tr>
									</template>
								</td>
							</tr>
						</tbody>


					</table>
				</div>

			</div>
		</div>


		<script src="Librerias/vue/vue.min.js"></script>
		<script src="Librerias/vue/axios.min.js"></script>
		<script src="vue/cont-trazabilidadRadicados.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
	</body>
</html>

</html>
