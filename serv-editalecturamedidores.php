<?php
	header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=iso-8859-1");
	require "comun.inc";
	$linkbd=conectar_v7();
	require "funciones.inc";
	session_start();
	date_default_timezone_set("America/Bogota");

	//**niveles menu: Administracion (0) - Consultas (1) - Herramientas (2) - Reportes (3)
?>
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
		<meta http-equiv="X-UA-Compatible" content="IE=9" />
		<title>:: IDEAL 10 - Servicios P&uacute;blicos</title>
		<style>
			form input[type='text'] {
				width:100%;
			}
		</style>
		<script>

			function guardar()
			{
				if (document.form2.fecha.value!='' && document.form2.servicio.value!=''  && document.form2.cliente.value!=''  && document.form2.lectura.value>0)
				{
					despliegamodalm('visible', '4', '¿Esta Seguro de Guardar?', '1');
                }
				else {
                    despliegamodalm('visible', '2', 'Falta información para agregar la lectura');
                }
			}

			function despliegamodalm(_valor,_tip,mensa,pregunta)
			{
				document.getElementById("bgventanamodalm").style.visibility=_valor;
                if(_valor == "hidden") 
                {
                    document.getElementById('ventanam').src = "";
                }
				else
				{
					switch(_tip)
					{
                        case "1":	
                            document.getElementById('ventanam').src = "ventana-mensaje1.php?titulos=" + mensa;
                            break;
                        case "2":	
                            document.getElementById('ventanam').src = "ventana-mensaje3.php?titulos=" + mensa;
                            break;
                        case "3":	
                            document.getElementById('ventanam').src = "ventana-mensaje2.php?titulos=" + mensa;
                            break;
                        case "4":	
                            document.getElementById('ventanam').src = "ventana-consulta1.php?titulos=" + mensa + "&idresp=" + pregunta;
                            break;	
					}
				}
            }
			
			function despliegamodal2(_valor, _table) 
			{
				document.getElementById('bgventanamodal2').style.visibility = _valor;

				if (_table == 'srvmedidores')
				{
					document.getElementById('ventana2').src = 'medidores-ventana.php?table=' + _table;
				} 
				else if(_table == 'srvclientes') 
				{
					document.getElementById('ventana2').src = 'ventana-clienteservicio.php?table=' + _table;
				}
			}

			function respuestaModalBusqueda2(tabla, id, nombre, serial, referencia)
			{
				switch(tabla)
				{
					case 'srvclientes': 
						document.getElementById('cliente').value = id;
						document.getElementById('ntercero').value = nombre;
						document.form2.submit();
					break;
                }
            }

			function funcionmensaje()
			{
				
            }

			function respuestaconsulta(pregunta)
			{
				switch(pregunta)
				{
                    case "1":	
                        document.form2.oculto.value = '2';
                        document.form2.submit();
                        break;
				}
			}
		</script>
		<script type="text/javascript" src="css/calendario.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script type="text/javascript" src="css/programas.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<link href="css/css2.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<link href="css/css4.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<?php titlepag();?>
	</head>
	<body>
		<IFRAME src="alertas.php" name="alertas" id="alertas" style="display:none"></IFRAME>
		<span id="todastablas2"></span>
		<table>
			<tr><script>barra_imagenes("serv");</script><?php cuadro_titulos();?></tr>	 
			<tr><?php menu_desplegable("serv");?></tr>
			<tr>
				<td colspan="3" class="cinta">
					<a href="serv-lecturamedidores.php" class="tooltip right mgbt">
						<img src="imagenes/add.png"/>
						<span class="tiptext">Nuevo</span>
					</a> 
					<a onClick="guardar();" class="tooltip bottom mgbt">
						<img src="imagenes/guarda.png"/>
						<span class="tiptext">Guardar</span>
					</a>
					<a href="serv-buscalecturamedidores.php" class="tooltip bottom mgbt"> 
						<img src="imagenes/busca.png"/>
						<span class="tiptext">Buscar</span>
					</a> 
					<a onClick="mypop=window.open('serv-principal.php','','');mypop.focus();" class="tooltip bottom mgbt">
						<img src="imagenes/nv.png">
						<span class="tiptext">Nueva Ventana</span>
					</a>
					<a onClick="mypop=window.open('<?php echo $url2; ?>','','');mypop.focus();" class="tooltip bottom mgbt">
						<img src="imagenes/duplicar_pantalla.png">
						<span class="tiptext">Duplicar Pesta&ntilde;a</span>
					</a>
				</td>
			</tr>		  
		</table>

		<?php
			if(!$_POST['oculto'])
			{	
				
			}
		?>
		<div id="bgventanamodalm" class="bgventanamodalm">
			<div id="ventanamodalm" class="ventanamodalm">
				<IFRAME  name="ventanam" marginWidth=0 marginHeight=0 frameBorder=0 id="ventanam" frameSpacing=0 style=" width:700px; height:130px; top:200; overflow:hidden;"></IFRAME>
			</div>
        </div>
		<form name="form2" method="post" action="">

		<?php 
				if(@ $_POST['oculto']=="")
				{
					$sqlr = "SELECT * FROM srvlectura WHERE id = '".$_GET['idban']."' ";
					$resp = mysqli_query($linkbd, $sqlr);
					$row  = mysqli_fetch_row($resp);
					$_POST['codigo'] = $row[0];
					$_POST['servicio'] = $row[2];
					$_POST['fecha'] = date('d/m/Y',strtotime($row[4]));
					$_POST['lectura'] = $row[5];
					$_POST['observacion'] = $row[6];
					$_POST['cliente'] = $row[1];
					$_POST['id_medidor'] = $row[3];

					$sqlr1 = "SELECT id_tercero FROM srvclientes WHERE id = $row[1]";
					$resp1 = mysqli_query($linkbd, $sqlr1);
					$row1  = mysqli_fetch_row($resp1);

					$sqlr2 = "SELECT nombre1,nombre2,apellido1,apellido2 FROM terceros WHERE id_tercero = $row1[0] ";
					$resp2 = mysqli_query($linkbd, $sqlr2);
					$row2  = mysqli_fetch_assoc($resp2);
					$_POST['ntercero'] = $row2['nombre1'] .' '. $row2['nombre2'] .' '. $row2['apellido1'] .' '. $row2['apellido2'];

					$sqlr3 = "SELECT serial FROM srvmedidores WHERE id =  $row[3]";
					$resp3 = mysqli_query($linkbd, $sqlr3);
					$row3 = mysqli_fetch_row($resp3);
					$_POST['serial'] = $row3[0];
					
					

				}
			?>
	
			<table class="inicio ancho" align="center" >
				<tr>
					<td class="titulos" colspan="10">.: Modificar Lectura Medidores</td>
					<td class="cerrar" onclick="location.href='serv-principal.php'">Cerrar</td>
				</tr>
				<tr>
					<td  class="saludo1">Codigo Lectura:</td>
					<td >
						<input name="codigo" type="text" value="<?php echo $_POST['codigo']?>" onKeyPress="javascript:return solonumeros(event)" onKeyUp="return tabular(event,this)" style="text-align:center" readonly>
					</td>
					<td class="saludo1">Fecha:</td>
					<td>
                        <input type="text" name="fecha" id="fecha" value="<?php echo @ $_POST['fecha']?>" maxlength="10" onKeyUp="return tabular(event,this)"  id="fc_1198971545" onKeyDown="mascara(this,'/',patron,true)" title="DD/MM/YYYY" style="height:30px;width:85%" readonly/>

						<a href="#" onClick="displayCalendarFor('fecha');" title="Calendario">
							<img src="imagenes/calendario04.png" style="width:25px;">
						</a>
					</td>
					<td class="saludo1">Cliente:</td>
					<td>
						<input type="text" name='cliente' id='cliente'  value="<?php echo @$_POST['cliente']?>" style="width: 100%; height: 30px; text-align:center;" onclick = "despliegamodal2('visible', 'srvclientes');"  class="colordobleclik" readonly>
                    </td>
					<td colspan="2">
						<input type="text" name="ntercero" id="ntercero" value="<?php echo @$_POST['ntercero']?>" style="width:100%;height:30px;" readonly>
					</td>
					
				</tr>
				<tr>
					<td class="saludo1">Medidor:</td>
					<td>
						<input type="text" name='serial' id='serial' value="<?php echo @$_POST['serial']?>" style="width: 100%; height: 30px;" onclick="despliegamodal2('visible', 'srvmedidores');" class="colordobleclik" readonly>
						<input type="hidden" name='id_medidor' id='id_medidor' value="<?php echo $_POST['id_medidor']?>">
					</td>

					<td class="tamano01">Servicio:</td>
					<td>
						<select name="servicio" id="servicio" style="width:100%;" onchange='actualizar()'>
							<option value="-1">Seleccione ...</option>
							<?php
								$sqlr="SELECT * FROM srvservicios";
								$resp = mysqli_query($linkbd,$sqlr);
								while ($row =mysqli_fetch_row($resp))
								{
									if(@ $_POST['servicio']==$row[0])
									{
										echo "<option value='$row[0]' SELECTED>$row[0] - $row[1]</option>";
									}
									else{echo "<option value='$row[0]'>$row[0] - $row[1]</option>";}
								}
							?>
						</select>
					</td>
				</tr>
				<tr>
					<td class="saludo1">Valor Lectura:</td>
					<td>
						<input id="lectura" type="text" name="lectura" value="<?php echo $_POST['lectura']?>" onKeyPress="javascript:return solonumeros(event)" onKeyUp="return tabular(event,this)" min=0 onBlur="">
					</td>
					<td  class="saludo1">Observaci&oacute;n:</td>
					<td  colspan="3">
						<input name="observacion" type="text" value="<?php echo $_POST['observacion']?>"  onKeyUp="return tabular(event,this)">
						<input name="oculto" type="hidden" value="1">	
					</td>
				</tr>
			</table>
		</form>
		<?php
			if($_POST['oculto']=='2')
			{	  
				preg_match("/([0-9]{2})\/([0-9]{2})\/([0-9]{4})/", $_POST['fecha'],$f);
				$fecha="$f[3]-$f[2]-$f[1]";	 

				$nr="1";
				$sqlr = "UPDATE srvlectura SET fecha = '$fecha', id_cliente='$_POST[cliente]', id_servicio='$_POST[servicio]', id_medidor='$_POST[id_medidor]', valormedicion='$_POST[lectura]', observacion='$_POST[observacion]' WHERE id = '$_POST[codigo]' ";

				if (!mysqli_query($linkbd, $sqlr))
				{
					$e = mysqli_error($linkbd);
					
					echo "
						<script> 
							despliegamodalm('visible', '2', 'No se pudo ejecutar la peticion');
						</script>";
				}
				else
				{
					echo "
						<script>
							despliegamodalm('visible', '1', 'Se ha almacenado con Exito');
						</script>";
				}
				
			}
		?>
		<div id="bgventanamodal2">
			<div id="ventanamodal2">
				<IFRAME  src="" name="buster" marginWidth=0 marginHeight=0 frameBorder=0 id="ventana2" frameSpacing=0 style="left:500px; width:900px; height:500px; top:200;"></IFRAME>
			</div>
		</div>
	</body>
</html>