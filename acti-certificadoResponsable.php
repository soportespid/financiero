<?php
	header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=utf8");
	require 'comun.inc';
	require 'funciones.inc';
	session_start();
	$linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");
	cargarcodigopag($_GET['codpag'],$_SESSION["nivel"]);
	date_default_timezone_set("America/Bogota");
?>
<!DOCTYPE >
<html lang=es>
	<head>
		<meta charset="utf-8"/>
		<meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1"/>
		<meta name="viewport" content="user-scalable=no">
		<title>:: IDEAL 10 - Activos Fijos</title>
		<link href="favicon.ico" rel="shortcut icon"/>
		<link href="css/css2z.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
		<link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
		<link href="css/css4.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
        <link href="css/tabs.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css"/>
        <link rel="stylesheet" href="sweetalert2/dist/sweetalert2.min.css">
        <script src="sweetalert2/dist/sweetalert2.min.js"></script>
		<script src="vue/vue.min.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script src="css/programas.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script src="css/calendario.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
        <script>
            function pdf(){
				document.form2.action = "acti-certificadoResponsablePDF.php";
				document.form2.target = "_BLANK";
				document.form2.submit(); 
				document.form2.action = "";
				document.form2.target = "";
			}
        </script>
		<style>
			input[type=number]::-webkit-inner-spin-button,
			input[type=number]::-webkit-outer-spin-button { 
				-webkit-appearance: none; 
				margin: 0; 
			}
			input[type=number]{
				text-align:right;
				box-sizing: border-box;
				font: 120% sans-serif;
				/* width: 100% !important; */
			}
			[v-cloak]{display : none;}

			.centrarSelect {

				height: 30px !important;
				text-align-last:center !important;
			}

			.aumentarTamaño {

				font-size:15px;
			}

            .tamano01 {
                text-align: center !important;
            }
		</style>
	</head>
	<body>
		<header>
			<table>
				<tr><script>barra_imagenes("acti");</script><?php cuadro_titulos();?></tr>
			</table>
		</header>

		<form name="form2" method="post" action="">
			<section id="myapp" v-cloak >
				<nav>
					<table>
						<tr><?php menu_desplegable("acti");?></tr>
						<tr>
							<td colspan="3" class="cinta">
								<img src="imagenes/add2.png" class="mgbt1" title="Nuevo">
								<img src="imagenes/guardad.png" title="Guardar" class="mgbt1">
								<img src="imagenes/buscad.png" v-on:click="location.href=''" class="mgbt1" title="Buscar">
								<img src="imagenes/nv.png" onClick="mypop=window.open('acti-principal.php','',''); mypop.focus();" class="mgbt" title="Nueva Ventana">
								<a onclick="pdf()"><img src="imagenes/print.png" title="Imprimir" class="mgbt"></a>
								<img src='imagenes/iratras.png' title="Men&uacute; Nomina" onclick="location.href='acti-menuCertificados'" class='mgbt'>
							</td>
						</tr>
					</table>
				</nav>

				<article>
                    <div>
                        <table class="inicio">
                            <tr>
                                <td class="titulos" colspan="12">.: Certificado de activos</td>
								<td class="cerrar" style="width:4%" onClick="location.href='acti-principal.php'">Cerrar</td>
                            </tr>

                            <tr>
                                <td class="textonew01" style="width:3.5cm;">.: Responsable:</td>
                                <td style="width: 11%;">
                                    <input type="text" name="responsable" v-model="responsable" style="text-align: center;" @dblclick="despliegoTerceros" class="colordobleclik" readonly>
                                </td>
                                <td>
                                    <input type="text" name="nomResponsable" v-model="nomResponsable" style="width: 100%;" readonly>
                                </td>

								<td class="textonew01" style="width:3.5cm;">.: Dependencia:</td>
								<td style="width: 11%;">
									<select v-model="dependencia" style="width: 90%; font-size: 10px; margin-top:4px">
										<option value="">Seleccionar dependencia</option>
										<option v-for="depen in dependencias" v-bind:value="depen[0]">
											{{ depen[0] }} - {{ depen[1] }}
										</option>
									</select>
								</td>

                                <td>
									<button type="button" @click="buscarActivos" class="botonflechaverde">Buscar activos</button>
								</td>
                            </tr>
                        </table>

						<div class='subpantalla' style='height:64vh; width:99.2%; margin-top:0px; resize: vertical;'>
                            <table class=''>
                                <thead>
                                    <tr>
										<th class="titulosnew00" style="width:10%;">Placa</th>
										<th class="titulosnew00">Nombre</th>
										<th class="titulosnew00" style="width:10%;">Fecha compra</th>
                                        <th class="titulosnew00" style="width:10%;">Estado de activo</th>
                                        <th class="titulosnew00" style="width:15%;">Ubicación</th>
                                        <th class="titulosnew00" style="width:15%;">Dependencia</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr v-for="(activo,index) in listadoActivos" v-bind:class="(index % 2 ? 'saludo1a' : 'saludo2')" style="font: 100% sans-serif;">
										<td style="width:10%; text-align:center;">{{ activo[0] }}</td>
										<td style="text-align:center;">{{ activo[1] }}</td>
										<td style="width:10%; text-align:center;">{{ activo[2] }}</td>
										<td style="width:10%; text-align:center;">{{ activo[3] }}</td>
										<td style="width:15%; text-align:center;">{{ activo[4] }}</td>
										<td style="width:15%; text-align:center;">{{ activo[5] }}</td>

										<input type='hidden' name='placa[]' v-model="activo[0]">
										<input type='hidden' name='nombre[]' v-model="activo[1]">
										<input type='hidden' name='fecha[]' v-model="activo[2]">
										<input type='hidden' name='estado[]' v-model="activo[3]">
										<input type='hidden' name='ubicacion[]' v-model="activo[4]">
										<input type='hidden' name='dependencia[]' v-model="activo[5]">
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div> 

					<div id="cargando" v-if="loading" class="loading">
						<span>Cargando...</span>
					</div>

                    <div v-show="showModalResponsables">
						<transition name="modal">
							<div class="modal-mask">
								<div class="modal-wrapper">
									<div class="modal-container">
										<table class="inicio ancho">
											<tr>
												<td class="titulos" colspan="2" >Listado terceros</td>
												<td class="cerrar" style="width:7%" @click="showModalResponsables = false">Cerrar</td>
											</tr>

                                            <tr>
                                                <td class="textonew01" style="text-align:center;">Cedula:</td>
                                                <td>
                                                    <input type="text" v-model="searchCedula" v-on:keyup="filtroResponsable" placeholder="Buscar por número de cedula" style="width: 100%;">
                                                </td>
                                            </tr>
										</table>
										<table class='tablamv'>
											<thead>
												<tr style="text-align:center;">
													<th class="titulosnew00" style="width: 20%;">Documento</th>
													<th class="titulosnew00">Nombre o razón social</th>
                                                    <th style="width: 1%;"></th>
												</tr>
											</thead>
											<tbody>
												<tr v-for="(tercero,index) in terceros" v-bind:class="index % 2 ? 'saludo1a' : 'saludo2'" v-on:click="seleccionaResponsable(tercero[0], tercero[1])">
													<td style="width:20%; text-align:center;"> {{ tercero[0] }} </td>
													<td style="text-align:left;"> {{ tercero[1] }} </td>
												</tr>
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</transition>
					</div>  
				</article>
			</section>
		</form>
        
		<script src="node_modules/read-excel-file/bundle/read-excel-file.min.js"></script>
		<script src="node_modules/xlsx/dist/xlsx.full.min.js"></script>
		<script src="Librerias/vue/vue.min.js"></script>
		<script src="Librerias/vue/axios.min.js"></script>
		<script type="module" src="activos_fijos/certificadoResponsable/acti-certificadoResponsable.js"></script>
        
	</body>
</html>