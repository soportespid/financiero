<?php
	// Include the main TCPDF library (search for installation path). PDF_PAGE_FORMAT
	require_once('barras/tcpdf_include.php');
	date_default_timezone_set('America/Bogota'); //puedes cambiar Guayaquil por tu Ciudad
	setlocale(LC_ALL, 'es_ES');
	
	// create new PDF document
	$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, 'letter', true, 'UTF-8', false);
	// set document information
	$pdf->SetCreator('');
	$pdf->SetAuthor('');
	$pdf->SetTitle('');
	$pdf->SetSubject('');
	$pdf->SetKeywords('');
	// set default header data
	// set header and footer fonts
	$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
	$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
	// set default monospaced font
	$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
	// set margins
	$pdf->SetMargins(0, 0, 0);
	$pdf->SetHeaderMargin(0);
	$pdf->SetFooterMargin(0);
	// set auto page breaks
	$pdf->SetAutoPageBreak(TRUE,0);
	// set image scale factor
	$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
	// set some language-dependent strings (optional)
	if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
		require_once(dirname(__FILE__).'/lang/eng.php');
		$pdf->setLanguageArray($l);
	}
	// ---------------------------------------------------------
	// set a barcode on the page footer
	// set font
	$pdf->SetFont('helvetica', '', 11);
	
	// add a page
	ini_set('max_execution_time', 7200);
	ereg( "([0-9]{2,4})-([0-9]{1,2})-([0-9]{1,2})", $_POST[fecha],$fecha);
	$fechaf=$fecha[3]."-".$fecha[2]."-".$fecha[1];
	 $fechaimp=mktime(0,0,0,$fecha[2],$fecha[3],$fecha[1]);
	 $_POST[fecha]=$fechaimp;
	 $fechaini=mktime(0,0,0,$_POST[periodo],"01",$_POST[vigencias]);
	 $ultdia=date("d",mktime(0,0,0,$_POST[periodo2]+1,0,$_POST[vigencias]));
	 $fechafin=mktime(0,0,0,$_POST[periodo2],$ultdia,$_POST[vigencias]);
	 $fechapagop=$fechaimp+($_POST[diaplazo]*24*60*60);
	$totalrec=count($_POST[totalfact]);
	$tam=$_POST[facfin];
	$tmin=$_POST[facini];
	for($yx=$tmin;$yx<=$tam;$yx++)
	{	
	$pdf->AddPage();	
	$pdf->SetFont('helvetica', '', 10);
	// define barcode style
	$style = array(
		'position' => '',
		'align' => 'C',
		'stretch' => false,
		'fitwidth' => true,
		'cellfitalign' => '',
		'border' => false,
		'hpadding' => 'auto',
		'vpadding' => 'auto',
		'fgcolor' => array(0,0,0),
		'bgcolor' => false, //array(255,255,255),
		'text' => false,
		'font' => 'helvetica',
		'fontsize' => 8,
		'stretchtext' => 1);
	
	$sumafactura=0;
	$sumaintereses=0;
	$sumasubsidios=0;
	$sumasaldoant=0;
	$sumacontri=0;
	$sumafactura=array_sum($_POST[totalfact][$yx]);
	$sumasubsidios=array_sum($_POST[totalsubs][$yx]);
	$sumasaldoant=array_sum($_POST[totalsaldoant][$yx]);	
	$sumacontri=array_sum($_POST[totalcontri][$yx]);	
	$sumaintereses=$_POST[fintereses][$yx];
	$mxcor=0;//margen de correccion horizontal
	$mycor=1.5;//margen de correccion veritcal
	$nfactura=substr("000000".$_POST[facturas][$yx],-10);//Ingresar Numero de Factura
	$estadofac=$_POST[estfac][$yx];
 	$ruta=$_POST[frutas][$yx]; //Ingresar Ruta
	$cedcatastral=$_POST[codigocat][$yx];//Ingresar Cedula catastral
	$codsuscriptor=$_POST[codigousu][$yx];//Ingresar el Codigo del Suscriptor
	$ciclo=$_POST[liquigen];//ingresar Ciclo
	$nomsuscriptor=utf8_decode($_POST[fnomter][$yx]);//Ingresar el Nombre del Suscriptor
	$direccion=$_POST[fdire][$yx];//ingresar Direccio
	$barrio=$_POST[fbarrio][$yx];//Ingresar Barrio
	$uso=$_POST[fuso][$yx];//Ingresar Barrio
	$tes=strlen(strrchr($_POST[festrato][$yx],"-"));
	$estrato=substr(strrchr($_POST[festrato][$yx],"-"),2,$tes);
	//Ingresar Estrato
	$nmedidor="";//Ingresar el Numero de Medidor
	$feciperiodo=date("d-m-Y",$fechaini);//teIngresar Fecha Inicial Periodo Facturado
	$fecfperiodo=date("d-m-Y",$fechafin);//Ingresar Fecha Final Periodo Facturado
	$lecactual="";//Ingresar Lectura Actual
	$lecanterior="";//Ingresar Lectura Anteriror
	$consumo=$lecactual-$lecanterior;//Ingresar El Consumo
	$justificacion="";//Ingresar La Justificacion
	$u6consumo1="";//Ultimo Seis consumos, Consumo 1
	$u6consumo2="";//Ultimo Seis consumos, Consumo 2
	$u6consumo3="";//Ultimo Seis consumos, Consumo 3
	$u6consumo4="";//Ultimo Seis consumos, Consumo 4
	$u6consumo5="";//Ultimo Seis consumos, Consumo 5
	$u6consumo6="";//Ultimo Seis consumos, Consumo 6
	$promediocom=(($u6consumo1+$u6consumo2+$u6consumo3+$u6consumo4+$u6consumo5+$u6consumo6)/6);
	$fecimpresion=date("d-m-Y",$_POST[fecha]);//Ingresar Fecha de Impresion
	$fecpagoopt=date("d-m-Y",$fechapagop);//Ingresar Fecha de Pago Oportuno
	$pagorecargo=date("d-m-Y",$fechapagop);//Ingresar Pago con Recargo
	$totalacueducto=$_POST[totalfact][$yx]["01"];//Ingresar total acueducto
	$totalalcanta=$_POST[totalfact][$yx]["02"];//Ingresar Total Alcantarillado
	$totalaseo=$_POST[totalfact][$yx]["03"];//Ingresar Total Aseo
	$totalotros="0";//Ingresar Tolal Otros pagos
	$totalsubsidios=$sumasubsidios;//Ingresar Total Subsidios
	$totalsobrecos=$sumacontri;//Ingresar Total Sobre Costos
	$totaldeudant=$sumasaldoant;//sin Intereses
	$comentarios=$_POST[obs];
	$pageen="CAJA";//PAGUESE EN 
	$cfacueducto="2000";//Ingresar Cargo Fijo Acueducto
	$cfsubsidio="2000";//Ingresar Cargo Fijo Subsidio
	$cfsobrecosto="2000";//Ingresar Cargo Fijo Sobrecosto
	$totalpago=$sumafactura-$sumasubsidios+$totaldeudant+$totalsobrecos;//Ingresar Total a Pagar
	$totalpagor=$sumafactura-$sumasubsidios+$totaldeudant+$totalsobrecos;//Ingresar Total a Pagar con Recargo
	//Ingresar Informacion Estado de Cuenta y Financiacion
	$limecf=4;
	$_POST[comceptoecf]=array();
	$_POST[valorecf]=array();
	for($x=0;$x<$limecf;$x++)
	{
		$_POST[comceptoecf][]="123456789012345678901234";
		$_POST[valorecf][]="15000";
	}
	//Ingresar Informacion Liquidacion Servicio Acueducto
	$limlsa=3;
	$_POST[consumolsa]=array();
	$_POST[acueductolsa]=array();
	$_POST[subsidiolsa]=array();
	$_POST[sobrecostolsa]=array();
	//Ingresar Informacion de Otros Cobros
	$limlsa=5;
	$cser=count($_POST[servcods]);
	$_POST[occodigo]=array();
		$_POST[occomcepto]=array();
		$_POST[ocvalor]=array();
		$_POST[ocsubsidio]=array();
		$_POST[ocsobrecosto]=array();
		$_POST[ocvalorreal]=array();
	for($s=0;$s<$cser;$s++)
	{
	if($_POST[servcods][$s]!="")
		{
		$servi=$_POST[servcods][$s];
		if($_POST[totalfact][$yx][$servi]>0 || $_POST[totalfact][$yx+1][$servi]!="")
		{
		$_POST[occodigo][]=$_POST[servcods][$s];
		$_POST[occomcepto][]=$_POST[servnoms][$s];
		$_POST[ocvalor][]=$_POST[totalfact][$yx][$servi];
		$_POST[ocsubsidio][]=$_POST[totalsubs][$yx][$servi];
		$_POST[ocsobrecosto][]=$_POST[totalcontri][$yx][$servi];
		$_POST[ocvalorreal][]=$_POST[totalfact][$yx][$servi]-$_POST[totalsubs][$yx][$servi]+$_POST[totalcontri][$yx][$servi];
		}
		}
	}
	$fecdi=explode('-', $_POST[fecha]);
	$cod01="7709998328310";	//codigo GS1 Asignado
	$cod02=str_pad($_POST[facturas][$yx],10,"0", STR_PAD_LEFT);//Numero Liquidacion	
	$cod03=str_pad($totalpago,10,"0", STR_PAD_LEFT);//total a pagar
	$cod04=date("Ymd",$fechapagop);//fecha limite
	$codtotn="(450)$cod01(8020)$cod02(3900)$cod03(96)$cod04";
	$codtot="450".$cod01."8020".$cod02."3900".$cod03."96".$cod04;
	//***************Bloque Informacion Inicial********************
	//ingresar Numero factura
	$pdf->SetFont('helvetica', '', 10);
	$y=$pdf->GetY();
	$y=$y+1;	
	$pdf->SetY($y+6.1+$mycor);
    $pdf->Cell(164+$mxcor);
	$pdf->Cell(30,0,$nfactura,0,0,'R',FALSE);
	$pdf->write1DBarcode(''.$codtot, 'C128', 100, ''.$y+14,100, 18, 0.4, $style, 'N');
	$pdf->SetFont('helvetica', '', 8);
	$pdf->SetY($y+30.1+$mycor);
    $pdf->Cell(102+$mxcor);
	$pdf->Cell(95.5,0,$codtotn,0,0,'C',FALSE);
	$pdf->SetFont('helvetica', '', 10);
	//***estado fac
	if ($estadofac=='V')
	$estexto='VENCIDA';
	if ($estadofac=='P')
	$estexto='PAGO';
	if ($estadofac=='S')
	$estexto='';
	$pdf->SetY($y+11.5+$mycor);
    $pdf->Cell(104+$mxcor);
	$pdf->Cell(30,0,''.$estexto,0,0,'R',FALSE);
	//ingresar La Ruta
	$pdf->SetY($y+40+$mycor);
    $pdf->Cell(36+$mxcor);
	$pdf->Cell(21.5,0,$ruta,0,0,'C',FALSE);
	//ingresar El Codigo Catastral
	$pdf->SetFont('helvetica', '', 7);
	$pdf->Cell(0.1);
	$pdf->Cell(37,0,$cedcatastral,0,0,'C',FALSE);	
	//ingresar El Codigo del Suscriptor
	$pdf->SetFont('helvetica', '', 10);
	$pdf->Cell(0.1);
	$pdf->Cell(36,0,$codsuscriptor,0,0,'C',FALSE);
	//ingresar El Ciclo
	$pdf->Cell(0.1);
	$pdf->Cell(19.5,0,$ciclo,0,0,'C',FALSE);
	//***************Bloque Informacion General********************
	//ingresar El Nombre del Suscriptor
	$pdf->SetFont('helvetica', '', 7);
	$pdf->SetY($y+53.5+$mycor);
	$pdf->Cell(17+$mxcor);
	$pdf->Cell(62,0,$nomsuscriptor,0,0,'L',FALSE);
	//ingresar La Direccion
	$pdf->SetY($y+60+$mycor);
	$pdf->Cell(17+$mxcor);
	$pdf->Cell(62,0,substr($direccion,0,28),0,0,'L',FALSE);
	//ingresar El Barrio
	$pdf->SetY($y+67+$mycor);
	$pdf->Cell(17+$mxcor);
	$pdf->Cell(62,0,substr($barrio,0,28),0,0,'L',FALSE);
	//ingresar El Uso
	$pdf->SetY($y+74+$mycor);
	$pdf->Cell(17+$mxcor);
	$pdf->Cell(40,0,substr($uso,0,21),0,0,'L',FALSE);
	//ingresar El Estrato
	$pdf->Cell(1);
	$pdf->Cell(20,0,$estrato,0,0,'L',FALSE);
	//ingresar El Numero de Medidor
	$pdf->SetY($y+80+$mycor);
	$pdf->Cell(17+$mxcor);
	$pdf->Cell(62,0,$nmedidor,0,0,'L',FALSE);
	//***************Bloque Periodo Facturado********************
	//Ingresar Fecha Inicial Periodo Facturado
	$pdf->SetFont('helvetica', '', 10);
	$pdf->SetY($y+52+$mycor);
	$pdf->Cell(81+$mxcor);
	//$pdf->Cell(37,0,$feciperiodo,0,0,'C',FALSE);
	$lastday = mktime (0,0,0,substr($feciperiodo,3,2),1,substr($feciperiodo,7,4));
	$vmes=ucwords(strftime('%B',$lastday));	
	$pdf->Cell(37,0,$vmes." - ".substr($feciperiodo,6,4),0,0,'C',FALSE);
	//Ingresar Fecha Final Periodo Facturado
	$pdf->Cell(1);
	$lastday = mktime (0,0,0,substr($fecfperiodo,3,2),1,substr($fecfperiodo,7,4));
	$vmesf=ucwords(strftime('%B',$lastday));	
	//$pdf->Cell(37,0,$fecfperiodo,0,0,'C',FALSE);
	$pdf->Cell(37,0,$vmesf." - ".substr($fecfperiodo,6,4),0,0,'C',FALSE);
	//Ingresar Lectura Actual
	$pdf->SetFont('helvetica', '', 8);
	$pdf->SetY($y+61.5+$mycor);
	$pdf->Cell(78+$mxcor);
	$pdf->Cell(17,0,$lecactual,0,0,'R',FALSE);
	//Ingresar Lectura Anterior
	$pdf->Cell(1);
	$pdf->Cell(16.5,0,$lecanterior,0,0,'R',FALSE);
	//Ingresar El Consumo
	$pdf->Cell(1);
	$pdf->Cell(20,0,$consumo,0,0,'R',FALSE);
	//Ingresar La Justificacion
	$pdf->Cell(0.1);
	$pdf->Cell(18,0,$justificacion,0,0,'L',FALSE);
	//Ultimo Seis Consumos, Ingresar Consumo 1
	$pdf->SetY($y+68.5+$mycor);
	$pdf->Cell(79+$mxcor);
	$pdf->Cell(12.5,0,$u6consumo1,0,0,'R',FALSE);
	//Ultimo Seis Consumos, Ingresar Consumo 2
	$pdf->Cell(1);
	$pdf->Cell(9.5,0,$u6consumo2,0,0,'R',FALSE);	
	//Ultimo Seis Consumos, Ingresar Consumo 3
	$pdf->Cell(1);
	$pdf->Cell(7.5,0,$u6consumo3,0,0,'R',FALSE);		
	//Ultimo Seis Consumos, Ingresar Consumo 4
	$pdf->Cell(1);
	$pdf->Cell(9,0,$u6consumo4,0,0,'R',FALSE);
	//Ultimo Seis Consumos, Ingresar Consumo 5
	$pdf->Cell(1);
	$pdf->Cell(9,0,$u6consumo5,0,0,'R',FALSE);
	//Ultimo Seis Consumos, Ingresar Consumo 6
	$pdf->Cell(1);
	$pdf->Cell(8.5,0,$u6consumo6,0,0,'R',FALSE);
	//Ultimo Seis Consumos, Ingresar promedio
	$pdf->Cell(1);
	$pdf->Cell(11,0,$promediocom,0,0,'R',FALSE);	
	//***************Bloque Informacion Estado de Cuenta y Financiacion********************
	$pdf->SetFont('helvetica', '', 7);
	$con=0;
	$saltoy=0;
	//***************Bloque Informacion Liquidacion Servicio Acueducto  ********************
	$pdf->SetFont('helvetica', '', 8);
	$con=0;
	$saltoy=0;
	$subtotalacue=0;
	$subtotalsubs=0;
	$subtotalsobr=0;
	//***************Bloque Informacion Otros Cobros  ********************
	$pdf->SetFont('helvetica', '', 8);
	$con=0;
	$saltoy=1;
	//otros cobros
	while ($con<count($_POST[occodigo]))
	{	
		$pdf->SetY($y+115+$mycor+$saltoy);
		$pdf->Cell(17+$mxcor);
		$pdf->Cell(21,0,"".$_POST[occodigo][$con],0,0,'L',FALSE);
		$pdf->Cell(0.1);
		$pdf->Cell(35,0,$_POST[occomcepto][$con],0,0,'L',FALSE);
		$pdf->Cell(0.1);
		$pdf->Cell(19,0,number_format($_POST[ocvalor][$con],0,".",","),0,0,'R',FALSE);
		$pdf->Cell(0.1);
		$pdf->Cell(19,0,number_format($_POST[ocsubsidio][$con],0,".",","),0,0,'R',FALSE);
		$pdf->Cell(0.1);
		$pdf->Cell(19,0,number_format($_POST[ocsobrecosto][$con],0,".",","),0,0,'R',FALSE);
		$pdf->Cell(0.1);
		$pdf->Cell(19,0,number_format($_POST[ocvalorreal][$con],0,".",","),0,0,'R',FALSE);
		$saltoy=$saltoy+3.5;
		$con=$con+1;
	}  
	$octotal=array_sum($_POST[ocvalorreal]);
	$oclugar=$pageen;
	$pdf->SetFont('helvetica', '', 10);
	$pdf->SetY($y+138+$mycor);
	$pdf->Cell(58+$mxcor);
	$pdf->Cell(30,0,"$".number_format($octotal,0,".",","),0,0,'L',FALSE);
	$pdf->Cell(22);
	$pdf->Cell(19.5,0,$oclugar,0,0,'L',FALSE);
	//***************Bloque Informacion Resumen Factura  ********************
	$pdf->SetFont('helvetica', '', 10);
	$mycor+=8;
	//ingresar Datos Acueducto
	$pdf->SetY($y+98+$mycor);
	$pdf->Cell(160+$mxcor);
	$pdf->Cell(37.5,0,'$'.number_format($totalacueducto,0,".",","),0,0,'R',FALSE);
	//ingresar Datos Alcantarillado
	$pdf->SetY($y+105+$mycor);
	$pdf->Cell(160+$mxcor);
	$pdf->Cell(37.5,0,'$'.number_format($totalalcanta,0,".",","),0,0,'R',FALSE);
	//ingresar Datos Aseo
	$pdf->SetY($y+112+$mycor);
	$pdf->Cell(160+$mxcor);
	$pdf->Cell(37.5,0,'$'.number_format($totalaseo,0,".",","),0,0,'R',FALSE);
	//ingresar Datos Total Otros Pagos
	$pdf->SetY($y+118.5+$mycor);
	$pdf->Cell(160+$mxcor);
	$pdf->Cell(37.5,0,'$'.number_format($totalotros,0,".",","),0,0,'R',FALSE);
	//ingresar Datos Total Subsidios
	$pdf->SetY($y+125.5+$mycor);
	$pdf->Cell(160+$mxcor);
	$pdf->Cell(37.5,0,'-$'.number_format($totalsubsidios,0,".",","),0,0,'R',FALSE);
	//ingresar Datos Total Sobrecostos
	$pdf->SetY($y+132.5+$mycor);
	$pdf->Cell(160+$mxcor);
	$pdf->Cell(37.5,0,'$'.number_format($totalsobrecos,0,".",","),0,0,'R',FALSE);
	//ingresar Datos Deuda Anterior
	$pdf->SetY($y+139.5+$mycor);
	$pdf->Cell(160+$mxcor);
	$pdf->Cell(37.5,0,'$'.number_format($totaldeudant,0,".",","),0,0,'R',FALSE);
	//ingresar Datos Total Mes
	$totalmes=($totalacueducto+$totalalcanta+$totalaseo+$totalotros-$totalsubsidios+$totalsobrecos);
	$pdf->SetY($y+146.5+$mycor);
	$pdf->Cell(160+$mxcor);
	$pdf->Cell(37.5,0,'$'.number_format($totalmes,0,".",","),0,0,'R',FALSE);
	//ingresar Datos Total A Pagar
	//$totalpago=($totalmes+$totaldeudant);
	$pdf->SetY($y+153.5+$mycor);
	$pdf->Cell(160+$mxcor);
	$pdf->Cell(37.5,0,'$'.number_format($totalpago,0,".",","),0,0,'R',FALSE);
	//***************Bloque Informacion Dia y Costo de Pagos********************
	$pdf->SetFont('helvetica', '', 10);
	$mycor-=8;
	//ingresar La Fecha de Impresion
	$pdf->SetY($y+41.5+$mycor);
	$pdf->Cell(161+$mxcor);
	$pdf->Cell(37.5,0,$fecimpresion,0,0,'C',FALSE);
	//ingresar La Fecha de Pago Oportuno
	$pdf->SetY($y+51+$mycor);
	$pdf->Cell(161+$mxcor);
	$pdf->Cell(37.5,0,$fecpagoopt,0,0,'C',FALSE);
	//ingresar Total a Pagar
	$pdf->SetY($y+60+$mycor);
	$pdf->Cell(161+$mxcor);
	$pdf->Cell(37.5,0,"$".number_format($totalpago,0,".",","),0,0,'C',FALSE);
	//ingresar Pago con Recargo
	$pdf->SetY($y+69+$mycor);
	$pdf->Cell(161+$mxcor);
	$pdf->Cell(37.5,0,$pagorecargo,0,0,'C',FALSE);
	//ingresar Total a Pagar con Recargo
	$pdf->SetY($y+78+$mycor);
	$pdf->Cell(161+$mxcor);
	$pdf->Cell(37.5,0,"$".number_format($totalpagor,0,".",","),0,0,'C',FALSE);
	//***************Bloque Informacion Adicional********************
	//ingresar Comentarios
	$pdf->SetFont('helvetica', '', 8);
	$pdf->SetY($y+142+$mycor);
	$pdf->Cell(16.2+$mxcor);
	$pdf->Cell(135,0,$comentarios,0,0,'L',FALSE);
	//***************Bloque Informacion Tirilla de pago 1********************
	//ingresar Fecha Impesion
	$pdf->SetFont('helvetica', '', 10);
	$pdf->SetY($y+191.5+$mycor);
	$pdf->Cell(53.5+$mxcor);
	$pdf->Cell(27,0,$fecimpresion,0,0,'C',FALSE);
	//ingresar Numero de Factura
	$pdf->Cell(2);
	$pdf->Cell(26,0,$nfactura,0,0,'C',FALSE);
	//ingresar Codigo Suscriptor
	$pdf->SetY($y+201.5+$mycor);
	$pdf->Cell(19+$mxcor);
	$pdf->Cell(44,0,$codsuscriptor,0,0,'L',FALSE);
	//ingresar Page En
	$pdf->Cell(3);
	$pdf->Cell(42,0,$pageen,0,0,'L',FALSE);
	//ingresar Nombre Suscriptor
	$pdf->SetFont('helvetica', '', 7);
	$pdf->SetY($y+211.5+$mycor);
	$pdf->Cell(19+$mxcor);
	$pdf->Cell(44,0,substr($nomsuscriptor,0,35),0,0,'L',FALSE);
	//ingresar Periodo Facturado
	$pdf->Cell(3);
	$pdf->Cell(42,0,$vmes." - ".$vmesf,0,0,'C',FALSE);
	//ingresar Pago Oportuno
	$pdf->SetFont('helvetica', '', 10);
	$pdf->SetY($y+224+$mycor);
	$pdf->Cell(19+$mxcor);
	$pdf->Cell(44,0,substr($fecpagoopt,0,23),0,0,'C',FALSE);
	//ingresar Pago con Recargo
	$pdf->Cell(3);
	$pdf->Cell(42,0,$pagorecargo,0,0,'C',FALSE);
	//ingresar Valor pago oportuno
	$pdf->SetY($y+233+$mycor);
	$pdf->Cell(19+$mxcor);
	$pdf->Cell(44,0,"$".number_format($totalpago,0,".",","),0,0,'C',FALSE);
	//ingresar Valor Pago con Recargo
	$pdf->Cell(3);
	$pdf->Cell(42,0,"$".number_format($totalpagor,0,".",","),0,0,'C',FALSE);
	//***************Bloque Informacion Tirilla de pago 2********************
	//ingresar Fecha Impesion
	$pdf->SetFont('helvetica', '', 10);
	$pdf->SetY($y+190+$mycor);
	$pdf->Cell(148+$mxcor);
	$pdf->Cell(27,0,$fecimpresion,0,0,'C',FALSE);
	//ingresar Numero de Factura
	$pdf->Cell(1);
	$pdf->Cell(26,0,$nfactura,0,0,'C',FALSE);
	//ingresar Codigo Suscriptor
	$pdf->SetY($y+201.5+$mycor);
	$pdf->Cell(112.5+$mxcor);
	$pdf->Cell(44,0,$codsuscriptor,0,0,'L',FALSE);
	//ingresar Page En
	$pdf->Cell(3);
	$pdf->Cell(42,0,$pageen,0,0,'L',FALSE);
	//ingresar Nombre Suscriptor
	$pdf->SetFont('helvetica', '', 7);
	$pdf->SetY($y+211.5+$mycor);
	$pdf->Cell(112.5+$mxcor);
	$pdf->Cell(40,0,substr($nomsuscriptor,0,35),0,0,'L',FALSE);
	//ingresar Periodo Facturado
	$pdf->Cell(7);
	$pdf->Cell(42,0,$vmes." - ".$vmesf,0,0,'C',FALSE);
	//ingresar Pago Oportuno
	$pdf->SetFont('helvetica', '', 10);
	$pdf->SetY($y+224+$mycor);
	$pdf->Cell(112.5+$mxcor);
	$pdf->Cell(42,0,substr($fecpagoopt,0,50),0,0,'C',FALSE);
	//ingresar Pago con Recargo
	$pdf->Cell(3);
	$pdf->Cell(44,0,$pagorecargo,0,0,'C',FALSE);
	//ingresar Valor pago oportuno
	$pdf->SetY($y+233+$mycor);
	$pdf->Cell(112.5+$mxcor);
	$pdf->Cell(44,0,"$".number_format($totalpago,0,".",","),0,0,'C',FALSE);
	//ingresar Valor Pago con Recargo
	$pdf->Cell(3);
	$pdf->Cell(44,0,"$".number_format($totalpagor,0,".",","),0,0,'C',FALSE);
	// Standard 2 of 5
//$pdf->Cell(0, 0, 'Standard 2 of 5', 0, 1);
$pdf->Ln();
$posy=$pdf->GetY();
$pdf->write1DBarcode(''.$codtot, 'C128', 5, ''.$posy+15, 100, 18, 0.4, $style, 'N');
$pdf->write1DBarcode(''.$codtot, 'C128', 106, ''.$posy+15, 100, 18, 0.4, $style, 'N');
$pdf->SetFont('helvetica', '', 8);
	$pdf->SetY($posy+32);
    $pdf->Cell(7.5+$mxcor);
	$pdf->Cell(95.5,0,$codtotn,0,0,'C',FALSE);
	$pdf->Cell(5+$mxcor);
	$pdf->Cell(95.5,0,$codtotn,0,0,'C',FALSE);
  }



// ---------------------------------------------------------

//Close and output PDF document
$pdf->Output('facturas.pdf', 'I');

//============================================================+
// END OF FILE
//============================================================+
?>