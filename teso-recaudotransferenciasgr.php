<?php
	header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=utf8");
	require "comun.inc";
	require "funciones.inc";
	require "conversor.php";
	sesion();
	$linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");
	cargarcodigopag($_GET['codpag'],$_SESSION["nivel"]);
	date_default_timezone_set("America/Bogota");
?>
<!DOCTYPE >
<html lang=es>
	<head>
		<meta charset="utf-8"/>
		<meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1"/>
		<meta name="viewport" content="user-scalable=no">
		<title>:: SPID - Tesoreria</title>
		<link href="css/css2.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet">
		<link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet">
		<link href="css/css4.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet">
		<link href="css/tabs.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet">
		<link href="sweetalert2/dist/sweetalert2.min.css" rel="stylesheet">
		<script src="sweetalert2/dist/sweetalert2.min.js"></script>
		<script type="text/javascript" src="css/calendario.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script type="text/javascript" src="css/programas.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script>
			function validar(){
				document.form2.submit();
			}
			function buscater(e){
				if (document.form2.tercero.value != ""){
					document.form2.bt.value = '1';
					document.form2.submit();
				}
			}
			function agregardetalle(){
				if(document.form2.codingreso.value != "" &&  document.form2.valor.value > 0   && document.form2.tiporet.value != ''){ 
					document.form2.agregadet.value = 1;
					document.form2.submit();
				}else{
					Swal.fire(
						'Error!',
						'Falta informacion para poder Agregar',
						'error'
					);
				}
			}
			function agregardetalled(){
				if(document.form2.retencion.value != "" &&  document.form2.vporcentaje.value != ""){ 
					document.form2.agregadetdes.value = 1;
					document.form2.submit();
				}else{
					Swal.fire(
						'Error!',
						'Falta informacion para poder Agregar',
						'error'
					);
				}
			}
			function eliminar(variable){
				if (confirm("Esta Seguro de Eliminar")){
					document.form2.elimina.value = variable;
					vvend = document.getElementById('elimina');
					vvend.value = variable;
					document.form2.submit();
				}
			}
			function eliminard(variable){
				Swal.fire({
						icon: 'question',
						title: '¿Esta Seguro de Eliminar?',
						showDenyButton: true,
						confirmButtonText: 'Eliminar',
						denyButtonText: 'Cancelar',
					}).then(
						(result) => {
							if (result.isConfirmed){
								document.form2.eliminad.value = variable;
								vvend = document.getElementById('eliminad');
								vvend.value = variable;
								document.form2.submit();
							}
						}
					)
			}
			function guardar(){
				if(document.form2.tipomovimiento.value=='101'){
					ingresos2 = document.form2.codingreso.value;
					banco = document.form2.nbanco.value;
					destino = document.form2.tiporet.value;
					if(destino == 'M'){
						retencionIngresos = document.getElementsByName('dndescuentos[]');
						if(retencionIngresos.length > 0){
							totalRecaudo = document.form2.totalcf.value;
							totalRetenciones = document.form2.totaldes.value;
							totalRecaudo = totalRecaudo.replace(",","");
							totalRecaudo = totalRecaudo.replace(",","");
							totalRecaudo = totalRecaudo.replace(",","");
							totalRecaudo = totalRecaudo.replace(",","");
							if(parseInt(totalRecaudo) == parseInt(totalRetenciones)){
								if (document.form2.fecha.value != '' && document.form2.concepto.value != '' && banco != ''){
									despliegamodalm('visible','4','Esta Seguro de Guardar','1');
								}else{
									despliegamodalm('visible','2','Faltan datos para completar el registro');
									document.form2.fecha.focus();
									document.form2.fecha.select();
								}
							}else{
								despliegamodalm('visible','2','El valor del recaudo y el total de retenciones ingresos no coinciden.');
								document.form2.fecha.focus();
								document.form2.fecha.select();
							}
						}else{
							despliegamodalm('visible','2','Faltan agregar retencion ingresos propios');
							document.form2.fecha.focus();
							document.form2.fecha.select();
						}
					}else{
						if (document.form2.fecha.value != '' && document.form2.concepto.value != '' && banco != ''){
							despliegamodalm('visible','4','Esta Seguro de Guardar','1');
						}else{
							despliegamodalm('visible','2','Faltan datos para completar el registro');
							document.form2.fecha.focus();
							document.form2.fecha.select();
						}
					}
				}else{
					numeroDeRecaudoAReversar = document.form2.ncomp.value;
					descripcionDeReversion = document.form2.descripcion.value;
					if (numeroDeRecaudoAReversar != '' && descripcionDeReversion != ''){
						despliegamodalm('visible','4','Esta Seguro de Guardar','1');
					}else{
						despliegamodalm('visible','2','Faltan datos para completar el registro');
					}
				}
			}
			function despliegamodalm(_valor,_tip,mensa,pregunta){
				document.getElementById("bgventanamodalm").style.visibility=_valor;
				if(_valor=="hidden"){
					document.getElementById('ventanam').src="";
				}else{
					switch(_tip){
						case "1":	document.getElementById('ventanam').src="ventana-mensaje1.php?titulos="+mensa;break;
						case "2":	document.getElementById('ventanam').src="ventana-mensaje3.php?titulos="+mensa;break;
						case "3":	document.getElementById('ventanam').src="ventana-mensaje2.php?titulos="+mensa;break;
						case "4":	document.getElementById('ventanam').src="ventana-consulta1.php?titulos="+mensa+"&idresp="+pregunta;break;	
					}
				}
			}
			function funcionmensaje(){
				location.href="teso-editarecaudotransferenciasgr.php?idrecaudo="+document.form2.idcomp.value;
			}
			function respuestaconsulta(pregunta){
				switch(pregunta){
					case "1":	
						document.form2.oculto.value='2';
						document.form2.submit();break;
				}
			}
			function pdf(){
				document.form2.action="teso-pdfrecaudostrans.php";
				document.form2.target="_BLANK";
				document.form2.submit(); 
				document.form2.action="";
				document.form2.target="";
			}
			function buscater(e){
				if (document.form2.tercero.value!=""){
					document.form2.bt.value='1';
					document.form2.submit();
				}
			}
			
			function despliegamodal2(_valor,_num){
				document.getElementById("bgventanamodal2").style.visibility=_valor;
				if(_valor=="hidden"){
					document.getElementById('ventana2').src="";
				}else{
					switch(_num){
						case '1': document.getElementById('ventana2').src="cuentasbancarias-ventana02.php?tipoc=D&obj01=banco&obj02=nbanco&obj03=&obj04=cb&obj05=ter";break;
						case '2': document.getElementById('ventana2').src="ventana-recaudosgrreversar.php";break;
						case '3': document.getElementById('ventana2').src="ingresosgral-ventana02.php?objeto=codingreso&nobjeto=ningreso";break;
						case '4': document.getElementById('ventana2').src="tercerosgral-ventana01.php?objeto=tercero&nobjeto=ntercero&nfoco=codingreso";break;
					}
				}
			}
			function respuestamensaje(){
				if(document.form2.tipomovimiento.value=='101'){
					location.href="teso-editarecaudotransferenciasgr.php?idrecaudo="+document.form2.idcomp.value;
				}else{
					location.href="teso-editarecaudotransferenciasgr.php?idrecaudo="+document.form2.ncomp.value;
				}
			}
			function validafinalizar(e){
				var id=e.id;
				var check=e.checked;
				if(id=='retencionesPropias'){
					document.getElementById('retencionesPropias').value=1;
				}else{
					document.form2.retencionesPropias.checked=false;
				}
				document.form2.submit();
			}
		</script>
		<?php titlepag();?>
	</head>
	<body>
		<IFRAME src="alertas.php" name="alertas" id="alertas" style="display:none"></IFRAME>
		<span id="todastablas2"></span>
		<table>
			<tr><script>barra_imagenes("teso");</script><?php cuadro_titulos();?></tr>	 
			<tr><?php menu_desplegable("teso");?></tr>
			<tr>
				<td colspan="3" class="cinta">
					<img src="imagenes/add.png" title="Nuevo" onClick="location.href='teso-recaudotransferenciasgr.php'" class="mgbt">
					<img src="imagenes/guarda.png" title="Guardar" onClick="guardar()" class="mgbt"> 
					<img src="imagenes/busca.png" title="Buscar" onClick="location.href='teso-buscarecaudotransferenciasgr.php'" class="mgbt">
					<img src="imagenes/agenda1.png" title="Agenda" onClick="mypop=window.open('plan-agenda.php','','');mypop.focus()" class="mgbt">
					<img src="imagenes/nv.png" title="Nueva Ventana" onClick="mypop=window.open('teso-principal.php','','');mypop.focus();" class="mgbt">
				</td>
			</tr>		  
		</table>
		<div id="bgventanamodalm" class="bgventanamodalm">
			<div id="ventanamodalm" class="ventanamodalm">
				<IFRAME src="" name="ventanam" marginWidth=0 marginHeight=0 frameBorder=0 id="ventanam" frameSpacing=0 style=" width:700px; height:130px; top:200; "> 
				</IFRAME>
			</div>
		</div>
		<?php
			$_POST['vigencia'] = $vigencia = $vigusu = vigencia_usuarios($_SESSION['cedulausu']);
			//*********** 11050501	CAJA PRINCIPAL esta es la cuenta que va a credito en todas las consignacones
			if(!$_POST['oculto']){
				$_POST['tipomovimiento'] = "101";
				$check1 = "checked";
				$fec = date("d/m/Y");
				$sqlr = "select valor_inicial from dominios where nombre_dominio='CUENTA_CAJA'";
				$res = mysqli_query($linkbd,$sqlr);
				while ($row = mysqli_fetch_row($res)){
					$_POST['cuentacaja'] = $row[0];
				}
				$fec = date("d/m/Y");
				$_POST['fecha'] = $fec; 		 		  			 
				$_POST['valor'] = 0;		 
			}
			if(!$_POST['tabgroup1']){
				$check1 = 'checked';
			}
			switch($_POST['tabgroup1']){
				case 1: $check1='checked';break;
				case 2: $check2='checked';break;
				case 3: $check3='checked';
			}
			if($_POST['tipomovimiento'] == '101')
			{
				$sqlr = "select max(id_recaudo) from tesorecaudotransferenciasgr ";
				$res = mysqli_query($linkbd,$sqlr);
				$consec = 0;
				while($r = mysqli_fetch_row($res)){
					$consec = $r[0];	  
				}
				$consec+=1;
				$_POST['idcomp'] = $consec;	
			}
		?>
		<form name="form2" method="post" action=""> 
			<?php
				//***** busca tercero
				if($_POST['bt']=='1')
				{
					$nresul=buscatercero($_POST['tercero']);
					if($nresul!=''){
						$_POST['ntercero']=$nresul;
					}else{
						$_POST['ntercero']="";
					}
				}				
				if($_POST['oculto']=='3'){
					$_POST['dcoding'] = array(); 		 
					$_POST['dncoding'] = array(); 		 
					$_POST['dvalores'] = array();
					$sqlr = "SELECT ingreso, valor FROM tesorecaudotransferenciasgr_det WHERE id_recaudo='$_POST[ncomp]'";
					$res = mysqli_query($linkbd,$sqlr);
					while($row = mysqli_fetch_row($res)){
						$_POST['dcoding'][] = $row[0];
						$_POST['dncoding'][] = buscaingreso($row[0]);			 		
						$_POST['dvalores'][] = $row[1];
					}
				}
			?>
			<table class="inicio">
				<tr>
					<td class="titulos" style="width:100%;">.: Tipo de Movimiento 
						<select name="tipomovimiento" id="tipomovimiento" onKeyUp="return tabular(event,this)" onChange="document.form2.submit();" style="width:20%;" >
							<?php 
								$user = $_SESSION['cedulausu'];
								$sql = "SELECT * from permisos_movimientos WHERE usuario='$user' AND estado='T' ";
								$res = mysqli_query($linkbd,$sql);
								$num = mysqli_num_rows($res);
								if($num == 1){
									$sqlr = "select * from tipo_movdocumentos where estado='S' and modulo=3 AND (id='1' OR id='3')";
									$resp = mysqli_query($linkbd,$sqlr);
									while ($row = mysqli_fetch_row($resp)){
										if($_POST['tipomovimiento']==$row[0].$row[1]){
											echo "<option value='$row[0]$row[1]' SELECTED >$row[0]$row[1]-$row[2]</option>";
										}else{
											echo "<option value='$row[0]$row[1]'>$row[0]$row[1]-$row[2]</option>";
										}
									}
								}
								else
								{
									$sql = "SELECT codmov,tipomov from permisos_movimientos WHERE usuario='$user' AND estado='S' AND modulo='3' AND transaccion='PIC' ";
									$res = mysqli_query($linkbd,$sql);
									while($row = mysqli_fetch_row($res)){
										if($_POST['tipomovimiento'] == $row[0]){
											echo "<option value='$row[0]' SELECTED>$row[0] - $row[1]</option>";
										}else{
											echo "<option value='$row[0]'>$row[0] - $row[1]</option>";
										}
									}
								}
							?>
						</select>
					</td>
					<td style="width:80%;"></td>
				</tr>
			</table>
			<input type="hidden" name="oculto" id="oculto" value="1" >
			<?php
				if($_POST['tipomovimiento'] == "101")
				{
			?>
			<div class="tabsmeci" style='min-height:31% !important;'>
				<div class="tab">
					<input type="radio" id="tab-1" name="tabgroup1" value="1" <?php echo $check1;?> >
					<label for="tab-1">Recaudo transferencia</label>
					<div class="content" style="overflow-x:hidden;" id="divdet">
						<table class="inicio ancho">
							<tr >
								<td class="titulos" colspan="9"> Recaudos Transferencias SGR</td>
								<td class="cerrar" style="width:7%" onClick="location.href='teso-principal.php'">Cerrar</td>
							</tr>
							<tr>
								<td style="width:12%;" class="saludo1" >No Recaudo:</td>
								<td style="width:10%;">
									<input name="idcomp" type="text" value="<?php echo $_POST['idcomp']?>" style="width:100%;" onKeyUp="return tabular(event,this) "  readonly>
								</td>
								<td style="width:5%;" class="saludo1">Fecha:</td>
								<td style="width:15%;">
									<input name="fecha" type="text" id="fc_1198971545" title="DD/MM/YYYY" value="<?php echo $_POST['fecha']; ?>" onKeyUp="return tabular(event,this) " onKeyDown="mascara(this,'/',patron,true)"  maxlength="10" style="width:100%;" class="colordobleclik" onDblClick="displayCalendarFor('fc_1198971545');" autocomplete="off" onChange=""> 
								</td>
								<td style="width:12%;" class="saludo1">Vigencia:</td>
								<td style="width:5%;">
									<input type="text" id="vigencia" name="vigencia" style="width:100%;" onKeyPress="javas cript:return solonumeros(event)" onKeyUp="return tabular(event,this)"  value="<?php echo $_POST['vigencia']?>" onClick="document.getElementById('tipocta').focus(); document.getElementById('tipocta').select();" readonly>
								</td>
								<td style="width:10%;" class="saludo1">Destino:</td>
								<td  colspan="2">
									<select name="tiporet" id="tiporet" style="width:100%;" onChange="validar()" >
										<option value="" >Seleccione...</option>
										<option value="N" <?php if($_POST['tiporet'] == 'N') echo "SELECTED"?>>Nacional</option>
										<option value="D" <?php if($_POST['tiporet'] == 'D') echo "SELECTED"?>>Departamental</option>
										<option value="M" <?php if($_POST['tiporet'] == 'M') echo "SELECTED"?>>Municipal</option>            
									</select>        
								</td>
								<td></td>
							</tr>
							<tr>
								<td class="saludo1">Concepto Recaudo:</td>
								<td colspan="3">
									<input name="concepto" type="text" value="<?php echo $_POST['concepto']?>" style="width:100%;" onKeyUp="return tabular(event,this)" >
								</td>
								<td class="saludo1">Fuente de Recurso:</td>
								<td>
									<input type="text" id="codingreso" name="codingreso" value="<?php echo $_POST['codingreso']?>"  onKeyUp="return tabular(event,this)" style="width:100%;" class="colordobleclik" onDblClick="despliegamodal2('visible','3');" title="Listado de Ingresos" autocomplete="off" readonly>
									<input type="hidden" value="0" name="bin">
								</td> 
								<td colspan="3"><input type="text" name="ningreso" id="ningreso" value="<?php echo $_POST['ningreso']?>" style="width:100%;" readonly></td>
							</tr>
							<tr>
								<td class="saludo1">Recaudado:</td>
								<td>
									<?php
										$sqlr = "select tesobancosctas.estado, tesobancosctas.cuenta, tesobancosctas.ncuentaban, tesobancosctas.tipo, terceros.razonsocial, tesobancosctas.tercero from tesobancosctas,terceros where tesobancosctas.tercero=terceros.cedulanit and tesobancosctas.estado='S' ";
										$res = mysqli_query($linkbd,$sqlr);
										while ($row = mysqli_fetch_row($res)){
											if($row[1] == $_POST['banco']){
												$_POST['nbanco'] = $row[4];
												$_POST['ter'] = $row[5];
												$_POST['cb'] = $row[2];
											}
										}	 	
									?>
									<input type="text" name="cb" id="cb" value="<?php echo $_POST['cb']; ?>" style="width:100%;" onDblClick="despliegamodal2('visible','1');" class="colordobleclik" title="Listado Cuentas Bancarias" autocomplete="off"/>
									<input name="banco" id="banco" type="hidden" value="<?php echo $_POST['banco']?>" >
									<input type="hidden" id="ter" name="ter" value="<?php echo $_POST['ter']?>" >           
								</td>
								<td colspan="4"> 
									<input type="text" id="nbanco" name="nbanco" value="<?php echo $_POST['nbanco']?>" style="width:100%;" readonly>
								</td>
								<td class="saludo1">Centro Costo:</td>
								<td colspan="2">
									<select name="cc"  onChange="validar()" style="width:100%;" onKeyUp="return tabular(event,this)">
										<?php
											$sqlr = "select * from centrocosto where estado='S'";
											$res = mysqli_query($linkbd,$sqlr);
											while ($row = mysqli_fetch_row($res)){
												if($row[0] == $_POST['cc']){
													echo "<option value='$row[0]' SELECTED>$row[0] - $row[1]</option>";
												}else{
													echo "<option value='$row[0]'>$row[0] - $row[1]</option>";
												}
											
											}	 	
										?>
									</select>
								</td>
							</tr>
							<tr>
								<td class="saludo1">NIT: </td>
								<td><input type="text" name="tercero" id="tercero" value="<?php echo $_POST['tercero']?>" onKeyUp="return tabular(event,this)" onBlur="buscater(event)" style="width:100%;" class="colordobleclik" autocomplete="off" onDblClick="despliegamodal2('visible','4');" title="Listado Contribuyentes"/></td>
								<td class="saludo1">Contribuyente:</td>
								<td colspan="3">
									<input type="text" id="ntercero" name="ntercero" value="<?php echo $_POST['ntercero']?>" onKeyUp="return tabular(event,this)" style="width:100%;" readonly>
									<input type="hidden" value="0" name="bt">
									<input type="hidden" id="cb" name="cb" value="<?php echo $_POST['cb']?>" >
									<input type="hidden" id="ct" name="ct" value="<?php echo $_POST['ct']?>" >
									
								</td>
								<td class="saludo1" >Valor:</td>
								<td>
									<input type="hidden" id="valor" name="valor" value="<?php echo $_POST['valor']?>"/>
									<input type="text" name="valorvl" id="valorvl" data-a-sign="$" data-a-dec="<?php echo $_SESSION["spdecimal"];?>" data-a-sep="<?php echo $_SESSION["spmillares"];?>" data-v-min='0' onKeyUp="sinpuntitos2('valor','valorvl');return tabular(event,this);" onkeypress="return validanumeros(event);" value="<?php echo $_POST['valorvl']; ?>" style='text-align:right;' />
								</td>
								<td ><em class="botonflechaverde" onClick="agregardetalle()">Agregar</em></td>
								<input type="hidden" name="agregadet"/>
							</tr>
						</table>
					</div>
				</div>

				<div class="tab">
					<input type="radio" id="tab-2" name="tabgroup1" value="2" <?php echo $check2;?> >
					<label for="tab-2">Retencion ingresos propios</label>
					
					<div class="content" style="overflow:hidden;" id="divdet">
						<table class="inicio ancho" style="width:99.5%">
							<tr >
								<td class="titulos" colspan="9">Retenciones</td>
								<td class="cerrar" style="width:7%" onClick="location.href='teso-principal.php'">Cerrar</td>
							</tr>
							<tr>
								<td class="saludo1" style="width:6%">Retencion y Descuento:</td>
								<td style="width:18%">
									<select name="retencion"  onKeyUp="return tabular(event,this)" style="width: 100%;">
										<option value="">Seleccione ...</option>
										<?php
											$sqlr="SELECT * FROM tesoretenciones WHERE estado='S' AND destino = '$_POST[tiporet]' AND terceros='1' AND tipo='S' ORDER BY codigo ASC";
											$res = mysqli_query($linkbd,$sqlr);
											while ($row = mysqli_fetch_row($res)){
												if("$row[0]" == $_POST['retencion']){
													echo "<option value='$row[0]' SELECTED>$row[1] - $row[2]</option>";
													$_POST['porcentaje'] = $row[5];
													$_POST['nretencion'] = $row[1]." - ".$row[2];
												}else{
													echo "<option value='$row[0]'>$row[1] - $row[2]</option>";
												} 	 
											}	 	
										?>
									</select>
									<input type="hidden" value="<?php echo $_POST['nretencion']?>" name="nretencion">
								</td>
								<td class="saludo1" style="width:6%">Valor:</td>
								<td style="width:6%"><input type="text" class='inputnum' id="vporcentaje" name="vporcentaje" value="<?php echo $_POST['vporcentaje']?>" ></td>
								<td class="saludo1" style="width:6%">Total Descuentos:</td>
								<td style="width:6%">
									<input type="text" class='inputnum' id="totaldes" name="totaldes"  value="<?php echo $_POST['totaldes']?>" readonly>
								</td>
								<input type="hidden" value="0" name="agregadetdes">
								<td colspan="2"><em class="botonflechaverde" onClick="agregardetalled()">Agregar</em></td>
							</tr>
							<?php 	
								$_POST['valoregreso'] = $_POST['valor'];
								$_POST['valorretencion'] = $_POST['totaldes'];
								$_POST['valorcheque'] = $_POST['valoregreso']-$_POST['valorretencion'];
								if ($_POST['eliminad']!='')
								{ 
									$posi=$_POST['eliminad'];
									unset($_POST['ddescuentos'][$posi]);
									unset($_POST['dndescuentos'][$posi]);
									unset($_POST['ddesvalores'][$posi]);
									$_POST['ddescuentos'] = array_values($_POST['ddescuentos']); 
									$_POST['dndescuentos'] = array_values($_POST['dndescuentos']);
									$_POST['ddesvalores'] = array_values($_POST['ddesvalores']); 		 
								}	 
								if ($_POST['agregadetdes'] == '1')
								{
									$_POST['ddescuentos'][] = $_POST['retencion'];
									$_POST['dndescuentos'][] = $_POST['nretencion'];
									$_POST['ddesvalores'][] = $_POST['vporcentaje'];
									$_POST['agregadetdes'] = '0';
									echo"
									<script>
										document.form2.vporcentaje.value=0;	
										document.form2.retencion.value='';	
									</script>";
								}
							?>
						</table>
						<table class='tablamv2' style='width:99.7%'>
							<thead>
								<tr>
									<th class="titulos" style='text-align:left;'>Descuento</th>
									<th class="titulos" style='width:20%'>Valor</th>
									<th class="titulos2" style='width:8%'><img src="imagenes/del.png"></th>
								</tr>
								<input type='hidden' name='eliminad' id='eliminad'>
							</thead>
							<tbody style='max-height: 10vh;'>
								<?php
									$totaldes=0;
									for ($x=0;$x<count($_POST['ddescuentos']);$x++)
									{
										if ($x % 2 == 0){
											$iter='saludo1a';
										}else{
											$iter='saludo2';
										}
										echo "
										<input type='hidden' name='dndescuentos[]' value='".$_POST['dndescuentos'][$x]."'>
										<input type='hidden' name='ddescuentos[]' value='".$_POST['ddescuentos'][$x]."' >
										<input type='hidden' name='ddesvalores[]' value='".($_POST['ddesvalores'][$x])."'>
										<tr class='$iter'>
											<td>".$_POST['dndescuentos'][$x]."</td>
											<td style='width:20%;text-align:right;''>$".number_format($_POST['ddesvalores'][$x],0,',','.')."</td>
											<td style='width:8%;text-align:center;'><img src='imagenes/del.png' class='icoop' onclick='eliminard($x)'></td>
										</tr>";
										$totaldes = $totaldes + ($_POST['ddesvalores'][$x]);
									}		 
									$_POST['valorretencion'] = $totaldes;
									echo"
									<script>
										document.form2.totaldes.value='$totaldes';		
										document.form2.valorretencion.value='$totaldes';
									</script>";
									$_POST['valorcheque'] = $_POST['valoregreso'] - $_POST['valorretencion'];
								?>
							</tbody>
						</table>
					</div>
				
				</div>
			</div>
			<?php
				}else if($_POST['tipomovimiento']=='301'){
			?>
			<div class="subpantalla" style="height:110px;" style="overflow-x:hidden !important;">
				<table class="inicio">
					<tr>
						<td class="titulos" colspan="6">.: Reversion de Recaudos Transferencias SGR</td>
					</tr>
					<tr>
						<td style="width:10%;" class="saludo1" >No Recaudo:</td>
						<td style="width:10%">
							<input type="text" name="ncomp" id="ncomp" style="width:80%;" onKeyPress="javascript:return solonumeros(event)" value="<?php echo $_POST['ncomp']?>" onKeyUp="return tabular(event,this) " onBlur="validar2()" readonly >&nbsp;<img src="imagenes/find02.png" style="width:20px;" onClick="despliegamodal2('visible','2');" title="Buscar REGLIAS" class="icobut" />  
						</td>
						<td class="saludo1" style="width:5%;">Fecha:</td>
						<td style="width:10%;">
							<input name="fecha" type="text" value="<?php echo $_POST['fecha']?>" maxlength="10"  onKeyPress="javascript:return solonumeros(event)" onKeyUp="return tabular(event,this)"  id="fc_1198971545" onKeyDown="mascara(this,'/',patron,true)" style="width:80%;" title="DD/MM/YYYY"/>&nbsp;<a href="#" onClick="displayCalendarFor('fc_1198971545');" title="Calendario"><img src="imagenes/calendario04.png" style="width:20px;"/></a>
						</td>
						<td class="saludo1" style="width:5%;">Descripcion: </td>
						<td style="width:60%;" colspan="3">
							<input type="text" name="descripcion" id="descripcion" value="<?php echo $_POST['descripcion']?>" style="width:100%;">
						</td>
					</tr>
					<tr>
						<td style="width:10%;" class="saludo1" >Tercero</td>
						<td>
							<input type="text" id="terceroAReversar" name="terceroAReversar" value="<?php echo $_POST['terceroAReversar'] ?>" style="width:80%;" readonly>
						</td>
						<td colspan="4">
							<input type="text" id="nombreTerceroAReversar" name="nombreTerceroAReversar" value="<?php echo $_POST['nombreTerceroAReversar'] ?>" style="width:100%;" readonly>
						</td>
					</tr>
				</table>
			</div>
			<?php
				}
          		 //***** busca tercero
				if($_POST['bt']=='1'){
					$nresul = buscatercero($_POST['tercero']);
					if($nresul != ''){
						$_POST['ntercero'] = $nresul;
						echo"
						<script>
							document.getElementById('codingreso').focus();
							document.getElementById('codingreso').select();
						</script>";
					}else{
						$_POST['ntercero']="";
						echo"
						<script>
							alert('Tercero Incorrecto o no Existe')				   		  	
							document.form2.tercero.focus();	
						</script>";
					}
				}
			?>
			<div class="subpantalla" style='height:40%;'>
				<table class="inicio">
					<tr>
						<td colspan="4" class="titulos">Detalle  Recaudos Transferencia</td>
					</tr>                  
					<tr>
						<td class="titulos2">Codigo</td>
						<td class="titulos2">Ingreso</td>
						<td class="titulos2">Valor</td>
						<td class="titulos2"><img src="imagenes/del.png" ><input type='hidden' name='elimina' id='elimina'></td>
					</tr>
					<?php 		
						if ($_POST['elimina']!='')
						{ 
							$posi=$_POST['elimina'];
							unset($_POST['dcoding'][$posi]);	
							unset($_POST['dncoding'][$posi]);			 
							unset($_POST['dvalores'][$posi]);			  		 
							$_POST['dcoding']= array_values($_POST['dcoding']); 		 
							$_POST['dncoding']= array_values($_POST['dncoding']); 		 		 
							$_POST['dvalores']= array_values($_POST['dvalores']); 		 		 		 		 		 
						}	 
						if ($_POST['agregadet']=='1')
						{
							$_POST['dcoding'][] = $_POST['codingreso'];
							$_POST['dncoding'][] = $_POST['ningreso'];			 		
							$_POST['dvalores'][] = $_POST['valor'];
							$_POST['agregadet'] = 0;
							echo"
							<script>
								
								document.form2.valor.value='';
								document.form2.valorvl.value='';
								document.form2.codingreso.value='';
								document.form2.ningreso.value='';
							</script>";
						}
						$_POST['totalc'] = 0;
						for ($x=0;$x<count($_POST['dcoding']);$x++){
							echo "
							<tr class='saludo1'>
								<td style='width:5%;'>
									<input name='dcoding[]' value='".$_POST['dcoding'][$x]."' type='text' style='width:100%;' readonly>
								</td>
								<td style='width:80%;'>
									<input name='dncoding[]' value='".$_POST['dncoding'][$x]."' type='text' style='width:100%;' readonly>
								</td>
								<td style='width:15%;'>
									<input name='dvalores[]' value='".$_POST['dvalores'][$x]."' type='text' style='width:100%;' readonly>
								</td>
								<td >
									<a href='#' onclick='eliminar($x)'>
										<img src='imagenes/del.png'>
									</a>
								</td>
							</tr>";
							$_POST['totalc'] = $_POST['totalc']+$_POST['dvalores'][$x];
							$_POST['totalcf'] = number_format($_POST['totalc'],2);
						}
						$resultado = convertir($_POST['totalc']);
						$_POST['letras'] = $resultado." Pesos";
						echo "
						<tr class='saludo1'>
							<td style='width:5%;'>
							</td>
							<td style='width:80%;'>Total</td>
							<td style='width:15%;'>
								<input name='totalcf' type='text' value='$_POST[totalcf]' style='width:100%;' readonly>
								<input name='totalc' type='hidden' value='$_POST[totalc]'>
							</td>
						</tr>
						<tr>
							<td style='width:5%;' class='saludo1'>Son:</td>
							<td style='width:80%;'>
								<input name='letras' type='text' value='$_POST[letras]' style='width:100%;'>
							</td>
						</tr>";
					?> 
				</table>
			</div>
			<?php
				if($_POST['oculto']=='2'){
					preg_match("/([0-9]{2})\/([0-9]{2})\/([0-9]{4})/", $_POST['fecha'],$fecha);
					$fechaf = "$fecha[3]-$fecha[2]-$fecha[1]";
					$fechamax = "$fecha[3]$fecha[2]$fecha[1]";
					$vigencia = $fecha[3];
					$bloq=bloqueos($_SESSION['cedulausu'],$fechaf);
					if($bloq >= 1){
						$sqlblfecha = "SELECT fecha FROM tesorecaudotransferenciasgr WHERE tipo_mov = '101' AND estado = 'S' ORDER BY id_recaudo DESC LIMIT 1";
						$resblfecha = mysqli_query($linkbd,$sqlblfecha);
						$rowblfecha = mysqli_fetch_row($resblfecha);
						$fechamin = date('Ymd',strtotime($rowblfecha[0]));
						if ((int)$fechamax >= (int)$fechamin){
							if($_POST['tipomovimiento'] == '101'){
								$sqlr = "select count(*) from tesorecaudotransferenciasgr where id_recaudo=$_POST[idcomp]";
								$res = mysqli_query($linkbd,$sqlr);
								while($r = mysqli_fetch_row($res)){
									$numerorecaudos = $r[0];
								}
								if($numerorecaudos == 0){
									//*********************CREACION DEL COMPROBANTE CONTABLE ***************************
									//***busca el consecutivo del comprobante contable
									//************ insercion de cabecera recaudos ************
									$sqlr = "insert into tesorecaudotransferenciasgr (id_recaudo,fecha,vigencia,banco,ncuentaban,concepto,detalleretencion,tercero,cc,valortotal,estado,tipo_mov,destino) values('".$_POST['idcomp']."','$fechaf','".$_POST['vigencia']."','".$_POST['ter']."','".$_POST['cb']."','".strtoupper($_POST['concepto'])."','".strtoupper($_POST['detalleretencion'])."','$_POST[tercero]','$_POST[cc]','$_POST[totalc]','S','$_POST[tipomovimiento]','$_POST[tiporet]')";
									mysqli_query($linkbd,$sqlr);
									$idrec = $_POST['idcomp'];
									$consec = 0;
									$consec = $idrec;
									//***cabecera comprobante
									$sqlr = "insert into comprobante_cab (numerotipo,tipo_comp,fecha,concepto,total,total_debito,total_credito,diferencia,estado) values ($consec,46,'$fechaf','".strtoupper($_POST['concepto'])."',0,'".$_POST['totalc']."','".$_POST['totalc']."',0,'1')";
									mysqli_query($linkbd,$sqlr);
									// DETALLE DEL COMPROBANTE CONTABLE
												
									//Banco $_POST['banco']
									//Cuenta 1
									//Cuenta 3
									//Fuente recurso		

									for($x=0;$x<count($_POST['dndescuentos']);$x++){
										$dd = $_POST['ddescuentos'][$x];
										$sqlr = "select * from tesoretenciones_det,tesoretenciones where tesoretenciones_det.codigo=tesoretenciones.id and tesoretenciones.id='".$dd."'";
										$rowdes = mysqli_fetch_row(mysqli_query($linkbd, $sqlr));

										$sq = "SELECT cuenta from conceptoscontables_det where codigo='$rowdes[3]' and modulo='4' and tipo='RE' and fechainicial<'$fechaf' and cuenta!='' order by fechainicial asc";
										$ro = mysqli_fetch_row(mysqli_query($linkbd, $sq));

										$sq2 = "SELECT cuenta from conceptoscontables_det where codigo='$rowdes[9]' and modulo='4' and tipo='SR' and fechainicial<'$fechaf' and cuenta!='' order by fechainicial asc";
										$ro2 = mysqli_fetch_row(mysqli_query($linkbd, $sq2));

										$sq3 = "SELECT concepto FROM tesoingresos_det WHERE codigo = '".$_POST['dcoding'][0]."' ORDER BY vigencia DESC LIMIT 1";
										$ro3 = mysqli_fetch_row(mysqli_query($linkbd, $sq3));
										
										$sq4 = "SELECT cuenta from conceptoscontables_det where codigo='$ro3[0]' and modulo='4' and tipo='C' and fechainicial<'$fechaf' and cuenta!='' and debito = 'S' order by fechainicial asc";
										$ro4 = mysqli_fetch_row(mysqli_query($linkbd, $sq4));

										$banco = $_POST['banco'];
										$fuenteRecurso = $ro4[0];
										$cuenta01 = $ro[0];
										$cuenta03 = $ro2[0];

										$nomDescuento = $_POST['dndescuentos'][$x];
										$valordes = $_POST['ddesvalores'][$x];

										$sqlr = "INSERT INTO comprobante_det (id_comp,cuenta,tercero,centrocosto,detalle, cheque,valdebito, valcredito, estado, vigencia) values ('46 $consec','$banco','$_POST[tercero]','$_POST[cc]','Descuento . $nomDescuento','','$valordes',0,'1' ,'$_POST[vigencia]')";
										mysqli_query($linkbd, $sqlr);
										
										$sqlr = "INSERT INTO comprobante_det(id_comp,cuenta,tercero,centrocosto,detalle, cheque,valdebito, valcredito, estado, vigencia) values ('46 $consec','$cuenta01','$_POST[tercero]','$_POST[cc]','Descuento . $nomDescuento','',0,'$valordes','1','$_POST[vigencia]')";
										mysqli_query($linkbd, $sqlr);

										$sqlr = "INSERT INTO comprobante_det (id_comp,cuenta,tercero,centrocosto,detalle, cheque,valdebito, valcredito, estado, vigencia) values ('46 $consec','$cuenta03','$_POST[tercero]','$_POST[cc]','Descuento . $nomDescuento','','$valordes',0,'1' ,'$_POST[vigencia]')";
										mysqli_query($linkbd, $sqlr);

										$sqlr = "INSERT INTO comprobante_det(id_comp,cuenta,tercero,centrocosto,detalle, cheque,valdebito, valcredito, estado, vigencia) values ('46 $consec','$fuenteRecurso','$_POST[tercero]','$_POST[cc]','Descuento . $nomDescuento','',0,'$valordes','1','$_POST[vigencia]')";
										mysqli_query($linkbd, $sqlr);
									}

									$noGuarda = 0;
									//************** insercion de consignaciones **************
									for($x=0;$x<count($_POST['dndescuentos']);$x++){
										$sqlr="insert into tesorecaudotransferenciaretencionsgr (id_recaudo,descuento,valor,estado) values($idrec,'".$_POST['ddescuentos'][$x]."',".$_POST['ddesvalores'][$x].",'S')";
										if (!mysqli_query($linkbd,$sqlr)){
											echo "<table ><tr><td class='saludo1'><center><font color=blue><img src='imagenes/alert.png'> Manejador de Errores de la Clase BD<br><font size=1></font></font><br><p align=center>No se pudo ejecutar la petici�n: <br><font color=red><b>$sqlr</b></font></p>";
											echo "Ocurrió el siguiente problema:<br>";
											echo "<pre>";
											echo "</pre></center></td></tr></table>";
											$noGuarda += 1;
										}
									}
									for($x=0;$x<count($_POST['dcoding']);$x++){
										$sqlr="insert into tesorecaudotransferenciasgr_det (id_recaudo,ingreso,valor,estado,tipo_mov) values($idrec,'".$_POST['dcoding'][$x]."',".$_POST['dvalores'][$x].",'S','".$_POST['tipomovimiento']."')";
										if (!mysqli_query($linkbd,$sqlr)){
											echo "<table ><tr><td class='saludo1'><center><font color=blue><img src='imagenes/alert.png'> Manejador de Errores de la Clase BD<br><font size=1></font></font><br><p align=center>No se pudo ejecutar la petici�n: <br><font color=red><b>$sqlr</b></font></p>";
											echo "Ocurrió el siguiente problema:<br>";
											echo "<pre>";
											echo "</pre></center></td></tr></table>";
											$noGuarda += 1;
										}else{
											$sqlri = "Select * from tesoingresos_det where codigo='".$_POST['dcoding'][$x]."' and vigencia=$vigusu";
											$resi = mysqli_query($linkbd,$sqlri);
											while($rowi = mysqli_fetch_row($resi)){
												$vi = $_POST['dvalores'][$x];
											}
										}
									}
									if($noGuarda == 0){
										echo "<table  class='inicio'>
											<tr>
												<td class='saludo1'>
													<center>Se ha almacenado el Recaudo con Exito <img src='imagenes/confirm.png'></center>
												</td>
											</tr>
										</table>
										<script>respuestamensaje();</script>
										<script>
											document.form2.numero.value='';
											document.form2.valor.value=0;
										</script>";
									}
								}
								else{
									echo "<table class='inicio'><tr><td class='saludo1'><center>Ya Existe un Recibo con este numero <img src='imagenes/alert.png'></center></td></tr></table>";
								}
							}
							else{
								//*********************CREACION DEL COMPROBANTE CONTABLE ***************************
								//***busca el consecutivo del comprobante contable
								//************ insercion de cabecera recaudos ************
								$sqlr = "UPDATE tesorecaudotransferenciasgr SET estado = 'R' WHERE id_recaudo='$_POST[ncomp]'";
								mysqli_query($linkbd,$sqlr);
								$sqlr="insert into tesorecaudotransferenciasgr (id_recaudo,fecha,vigencia,banco,ncuentaban,concepto,detalleretencion,tercero,cc,valortotal,estado,tipo_mov) values('".$_POST['ncomp']."','$fechaf','".$vigencia."','','','".strtoupper($_POST['descripcion'])."','','".$_POST['terceroAReversar']."','','".$_POST['totalc']."','R','".$_POST['tipomovimiento']."')";
								mysqli_query($linkbd,$sqlr);
								$sqlr = "SELECT * FROM comprobante_det WHERE tipo_comp='46' AND numerotipo='".$_POST['ncomp']."'";
								$res = mysqli_query($linkbd,$sqlr);
								while($row = mysqli_fetch_row($res)){
									$sqlr = "insert into comprobante_det (id_comp,cuenta,tercero,centrocosto,detalle, cheque,valdebito, valcredito, estado, vigencia) values ('46 ".$_POST['ncomp']."','".$row[2]."','".$row[3]."' ,'".$row[4]."' , 'Reversion ".$row[5]."','',".$row[8].",".$row[7].",'2' ,'".".$row[10]."."')";
									mysqli_query($linkbd,$sqlr);
								}
								echo "<table  class='inicio'>
									<tr>
										<td class='saludo1'>
											<center>Se ha almacenado el Recaudo con Exito <img src='imagenes/confirm.png'></center>
										</td>
									</tr>
								</table>
								<script>respuestamensaje();</script>
								";
							}
						}else{
							echo"
							<script>
								Swal.fire({
									icon: 'error',
									title: 'Error!',
									text: 'La fecha actual es menor a la fecha del recaudo transferencia anteriror',
									confirmButtonText: 'Continuar',
									confirmButtonColor: '#FF121A',
									timer: 3000
								});
							</script>";
						}
					}else{
						echo"
						<script>
							Swal.fire({
								icon: 'error',
								title: 'Error!',
								text: 'No Tiene los Permisos para Modificar este Documento',
								confirmButtonText: 'Continuar',
								confirmButtonColor: '#FF121A',
								timer: 3000
							});
						</script>";
					}
				}
			?>
			<div id="bgventanamodal2">
				<div id="ventanamodal2">
					<IFRAME  src="" name="buster" marginWidth=0 marginHeight=0 frameBorder=0 id="ventana2" frameSpacing=0 style="left:500px; width:900px; height:500px; top:200;"> 
					</IFRAME>
				</div>
			</div>	
		</form>
	</body>
</html> 		