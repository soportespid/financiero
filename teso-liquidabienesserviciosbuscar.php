<?php
	header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=utf8");
	require "comun.inc";
	require "funciones.inc";
	session_start();
	$linkbd=conectar_v7();
	$linkbd -> set_charset("utf8");	
	cargarcodigopag($_GET['codpag'],$_SESSION["nivel"]);
	date_default_timezone_set("America/Bogota");
?>
<!DOCTYPE >
<html lang="es">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1">
		<meta name="viewport" content="user-scalable=no">
		<title>:: IDEAL 10 - Tesorer&iacute;a</title>
		<link href="favicon.ico" rel="shortcut icon"/>
		<link href="css/css2.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<link href="css/css4.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<link href="sweetalert2/dist/sweetalert2.min.css" rel="stylesheet">
		<script src="sweetalert2/dist/sweetalert2.min.js"></script>
		<script type="text/javascript" src="css/programas.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script type="text/javascript" src="css/calendario.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script type="text/javascript" src="jquery-1.11.0.min.js"></script>
		<script>
			function verUltimaPos(idcta){
				location.href="teso-liquidabienesserviciosver.php?idrecaudo=" + idcta;
			}
			function eliminar(idr){
				Swal.fire({
					icon: 'question',
					title: '¿Esta Seguro de Anular?',
					showDenyButton: true,
					confirmButtonText: 'Anular',
					confirmButtonColor: '#01CC42',
					denyButtonText: 'Cancelar',
					denyButtonColor: '#FF121A',
				}).then(
					(result) => {
						if (result.isConfirmed){
							document.form2.anular.value = idr;
							document.form2.oculto.value = '3';
							document.form2.submit();
						}else if (result.isDenied){
							Swal.fire({
								icon: 'info',
								title: 'No se Anulo',
								confirmButtonText: 'Continuar',
								confirmButtonColor: '#FF121A',
								timer: 2500
							});
						}
					}
				)
			}
			function buscarbotonfiltro(){
				if((document.form2.fechaini.value != "" && document.form2.fechafin.value != "")){
					document.form2.oculto.value = '3';
					document.form2.submit();
				}else{
					Swal.fire({
						icon: 'error',
						title: 'Error!',
						text: 'Falta digitar fecha',
						confirmButtonText: 'Continuar',
						confirmButtonColor: '#FF121A',
						timer: 2500
					});
				}
			}  
		</script>
	</head>
	<body>
		<IFRAME src="alertas.php" name="alertas" id="alertas" style="display:none"></IFRAME>
		<span id="todastablas2"></span>
		<table>
			<tr><script>barra_imagenes("teso");</script><?php cuadro_titulos();?></tr>	 
			<tr><?php menu_desplegable("teso");?></tr>
			<tr>
				<td colspan="3" class="cinta">
					<img src="imagenes/add.png" title="Nuevo" class="mgbt" onClick="location.href='teso-liquidabienesservicios.php'"/>
					<img src="imagenes/guardad.png" class="mgbt"/>
					<img src="imagenes/buscad.png" class="mgbt"/>
					<img src="imagenes/agenda1.png" title="Agenda" onClick="mypop=window.open('plan-agenda.php','','');mypop.focus()" class="mgbt"/>
					<img src="imagenes/nv.png" title="Nueva ventana"  onClick="mypop=window.open('teso-principal.php','','');mypop.focus();" class="mgbt"/>
					<img src="imagenes/duplicar_pantalla.png" title="Duplicar pesta&ntilde;a" onClick="mypop=window.open('<?php echo $url2; ?>','','');mypop.focus();" class="tooltip bottom mgbt">
					<img src="imagenes/excel.png" title="Excel" onclick="crearexcel()" class="mgbt">
				</td>
			</tr>
		</table>
		<form name="form2" method="post" action="teso-liquidabienesserviciosbuscar.php">
			<div class="loading" id="divcarga"><span>Cargando...</span></div>
			<?php
				if($_POST['oculto']!=''){
					echo"<script>document.getElementById('divcarga').style.display='none';</script>";
				}
				if($_POST['oculto']==''){
					$_POST['fechaini'] = date("01/m/Y");
					$_POST['fechafin'] = date("d/m/Y");
				}
			?>
			<table class='tablamv3 ancho'>
				<thead>
					<tr>
						<th class="titulos" style='text-align:left;'>:. Buscar Recaudos </th>
						<th class="cerrar" style="width:7%" onClick="location.href='teso-principal.php'">Cerrar</th>
					</tr>
					<tr>
						<th style="width:2cm" class="tamano01">Código: </th>
						<th style="width:8%"><input type="text" name="numero" id="numero" value="<?php echo $_POST['numero'];?>" style="width:100%" autocomplete="off"></th>
						<th style="width:2cm" class="tamano01">Concepto: </th>
						<th><input type="text" name="concepto" id="concepto" value="<?php echo $_POST['concepto'];?>" style="width:100%" autocomplete="off"></th>
						<th class="tamano01" style="width:2cm">Fecha inicial: </th>
						<th style="width:10%"><input type="search" name="fechaini" id="fc_1198971545" title="DD/MM/YYYY" placeholder="DD/MM/YYYY" value="<?php echo $_POST['fechaini'];?>" onKeyUp="return tabular(event,this)" onKeyDown="mascara(this,'/',patron,true)" maxlength="10" style="width:100%" class="colordobleclik" autocomplete="off" onChange="" onDblClick="displayCalendarFor('fc_1198971545');"></th>
						<th class="tamano01" style="width:2cm">Fecha final: </th>
						<th style="width:10%;"><input type="search" name="fechafin" id="fc_1198971546" title="DD/MM/YYYY" placeholder="DD/MM/YYYY" value="<?php echo $_POST['fechafin'];?>" onKeyUp="return tabular(event,this)" onKeyDown="mascara(this,'/',patron,true)" maxlength="10" style="width:100%" class="colordobleclik" autocomplete="off" onChange="" onDblClick="displayCalendarFor('fc_1198971546');"></th>
						<th style="padding-bottom:1px" ><em class="botonflechaverde" onClick="buscarbotonfiltro();">Buscar</em></th>
						<th style="width:7%" ></th>
					</tr>
					<input type="hidden" name="oculto" id="oculto" value="1"/>
					<input type="hidden" name="anular" id="anular" value = "<?php echo $_POST['anular'];?>" />
					<?php
						$ntr = 0;
						if($_POST['anular'] != ''){
							$sql = "UPDATE tesobienesservicios SET estado = 'N' WHERE id_recaudo = '".$_POST['anular']."'";
							mysqli_query($linkbd,$sql);
							$sql = "UPDATE comprobante_cab SET estado = '0' WHERE tipo_comp = '41', numerotipo = '".$_POST['anular']."'";
							mysqli_query($linkbd,$sql);
							echo "
							<script>
								document.form2.anular.value = '';
							</script";
						}
						if($_POST['oculto'] == '3'){
							if($_POST['numero'] != ''){
								$crit1 = "id_recaudo LIKE '".$_POST['numero']."'";
							}else{
								$crit1 = "";
							}
							if ($_POST['concepto'] != ''){
								$crit2 = "concepto LIKE '".$_POST['concepto']."'";
							}else{
								$crit2 = "";
							}
							preg_match("/([0-9]{2})\/([0-9]{2})\/([0-9]{4})/",$_POST['fechaini'],$fecha);
							$fechai = "$fecha[3]-$fecha[2]-$fecha[1]";
							preg_match("/([0-9]{2})\/([0-9]{2})\/([0-9]{4})/",$_POST['fechafin'],$fecha);
							$fechaf = "$fecha[3]-$fecha[2]-$fecha[1]";
							if($crit1 != '' || $crit2 != ''){
								$critm = "$crit1 $crit2";
							}else{
								$critm = "fecha BETWEEN '$fechai' AND '$fechaf'";
							}
							$sqlr = "SELECT * FROM tesobienesservicios WHERE $critm ORDER BY id_recaudo DESC";
							$resp = mysqli_query($linkbd,$sqlr);
							$ntr = mysqli_num_rows($resp);
						}
						echo "
							<tr><th class='titulos' style='text-align:left;'>.: Resultados Busqueda: $ntr</th></tr>
							<tr>
								<th class='titulosnew00' style='width:6.2%;'>Codigo</th>
								<th class='titulosnew00' style='width:25.7%;'>Nombre</th>
								<th class='titulosnew00' style='width:9.9%;'>Fecha</th>
								<th class='titulosnew00' style='width:37.6%;'>Concepto</th>
								<th class='titulosnew00' style='width:7%;'>N° Recaudo</th>
								<th class='titulosnew00' style='width:5%;'>Estado</th>
								<th class='titulosnew00' >Anular</th>
							</tr>";	
						$iter = 'saludo1a';
						$iter2 = 'saludo2';
						$filas = 1;
					?>
				</thead>
				<tbody style='height:54vh;'>
					<?php
						if($ntr == 0){
							echo "
							<tr>
								<td class='saludo1' style='text-align:center;width:100%;font-size:25px'>No hay resultados de su busqueda.</td>
							</tr>";
						}else{
							while ($row = mysqli_fetch_row($resp)){
								$nombreTercero = buscatercero($row[4]);
								$sqlrt = "SELECT id_recibos, estado FROM teso_ingreso_bienes_servicios WHERE id_recaudo = '$row[0]'";
								$resrt = mysqli_query($linkbd,$sqlrt);
								$rowrt = mysqli_fetch_row($resrt);
								if($row[7] == 'S'){
									$estclass = 'luz_verde';
									$estdescr = 'Activa';
								}else{
									$estclass = 'luz_roja';
									$estdescr = 'Anulada';
								}
								if($rowrt[0] != '' && $rowrt[1] == 'S'){
									$numrecaudo = $rowrt[0];
									$nomclass = 'garbageY';
									if($rowrt[1] == 'S'){
										$nomtitle = 'Ya tiene recaudo';
										$nomfuncion = '';
									}else{
										if($row[7] == 'S'){
											$nomclass = 'garbageX';
											$nomtitle = 'Anular Liquidación';
											$nomfuncion = "eliminar($row[0])";
										}else{
											$nomclass = 'garbageY';
											$nomtitle = 'Ya esta Anulada';
											$nomfuncion = '';
										}
									}
								}else{
									if($rowrt[0] == ''){
										$numrecaudo = 0;
									}else{
										$numrecaudo = $rowrt[0];
									}
									if($row[7] == 'S'){
										$nomclass = 'garbageX';
										$nomtitle = 'Anular Liquidación';
										$nomfuncion = "eliminar($row[0])";
									}else{
										$nomclass = 'garbageY';
										$nomtitle = 'Ya esta Anulada';
										$nomfuncion = '';
									}
								}
								
								echo"
								<tr class='$iter' onDblClick=\"verUltimaPos($row[0])\" style='text-transform:uppercase;'>
									<td style='width:6.3%;'>$row[0]</td>
									<td style='width:26%;text-align:left;'>$nombreTercero</td>
									<td style='width:10%;text-align:center;'>$row[2]</td>
									<td style='width:39%;text-align:left;'>$row[6]</td>
									<td style='width:7%;'>$numrecaudo</td>
									<td style='width:5%;'><div class='$estclass' title='$estdescr'></div></td>
									<td style='display: flex;justify-content: center;'><div class='$nomclass' title='$nomtitle' onClick='$nomfuncion'></div></td>
								</tr>";
								$con+=1;
								$aux=$iter;
								$iter=$iter2;
								$iter2=$aux;
								$filas++;
							}
						}
						echo "<script>document.getElementById('divcarga').style.display='none';</script>";
					?>
				</tbody>
			</table>
		</form> 
	</body>
</html>