<?php
	header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=utf8");

	require "comun.inc";
	require "funciones.inc";
	require "conversor.php";
	require "validaciones.inc";

	$linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");

	session_start();
	date_default_timezone_set("America/Bogota");
?>
<!DOCTYPE > 
<html lang="es">
	<head>
		<meta charset="utf-8"/>
		<meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1"/>
		<meta name="viewport" content="user-scalable=no">
		<title>:: IDEAL 10 - Tesorer&iacute;a</title>
		<link href="favicon.ico" rel="shortcut icon"/>
        <link href="css/css2.css" rel="stylesheet" type="text/css" />
        <link href="css/css3.css" rel="stylesheet" type="text/css" />
		<script type="text/javascript" src="css/programas.js"></script>
        <script type="text/javascript" src="css/calendario.js"></script>
		<script>
			function crearexcel(){
				document.form2.action="teso-buscapagoterceros-vigante-excel.php";
				document.form2.target="_BLANK";
				document.form2.submit();
				document.form2.action="";
				document.form2.target="";
			}
			function verUltimaPos(idcta, filas, filtro)
			{
				var scrtop=$('#divdet').scrollTop();
				var altura=$('#divdet').height();
				var numpag=$('#nummul').val();
				var limreg=$('#numres').val();
				if((numpag<=0)||(numpag=="")){numpag=0;}
				if((limreg==0)||(limreg=="")){limreg=10;}
				numpag++;
				location.href="cont-editarplancuenta.php?idtipocom="+idcta+"&scrtop="+scrtop+"&totreg="+filas+"&altura="+altura+"&numpag="+numpag+"&limreg="+limreg+"&filtro="+filtro;
			}
			function buscarbotonfiltro()
            {
                if((document.form2.fechaini.value != "" && document.form2.fechafin.value == "") || (document.form2.fechaini.value == "" && document.form2.fechafin.value != "")){
                    alert("Falta digitar fecha");
                }else{
                    document.getElementById('numpos').value=0;
                    document.getElementById('nummul').value=0;
                    document.form2.submit();
                }   
            }
			function cambionumero()
			{
				document.getElementById('numres').value=document.getElementById('renumres').value;
				document.getElementById('nummul').value=0;
				document.getElementById('numpos').value=0;
				document.getElementById('oculto').value=1;
				document.form2.submit(); 
			}
			function numsig()
			{
				document.getElementById('nummul').value=parseInt(document.getElementById('nummul').value)+1;
				document.getElementById('numpos').value=parseInt(document.getElementById('numres').value)*parseInt(document.getElementById('nummul').value);
				document.getElementById('oculto').value=1;
				document.form2.submit(); 
			}
			function numant()
			{
				document.getElementById('nummul').value=parseInt(document.getElementById('nummul').value)-1;
				document.getElementById('numpos').value=parseInt(document.getElementById('numres').value)*parseInt(document.getElementById('nummul').value);
				document.getElementById('oculto').value=1;
				document.form2.submit(); 
			}
			function saltopag(pag)
			{
				document.getElementById('nummul').value=parseInt(pag)-1;
				document.getElementById('numpos').value=parseInt(document.getElementById('numres').value)*parseInt(document.getElementById('nummul').value);
				document.getElementById('oculto').value=1;
				document.form2.submit();
			}
		</script>
		
		<script>
			//************* ver reporte ************
			//***************************************
			function verep(idfac)
			{
				document.form1.oculto.value=idfac;
				document.form1.submit();
			}
			//************* genera reporte ************
			//***************************************
			function genrep(idfac)
			{
				document.form2.oculto.value=idfac;
				document.form2.submit();
			}
			function buscacta(e)
			{
				if (document.form2.cuenta.value!="")
				{
					document.form2.bc.value='1';
					document.form2.submit();
				}
			}
			function validar()
			{
				document.form2.submit();
			}
			function buscater(e)
			{
				if (document.form2.tercero.value!="")
				{
					document.form2.bt.value='1';
					document.form2.submit();
				}
			}
			function agregardetalle()
			{
				if(document.form2.numero.value!="" &&  document.form2.valor.value>0 &&  document.form2.banco.value!=""  )
				{ 
								document.form2.agregadet.value=1;
					//			document.form2.chacuerdo.value=2;
								document.form2.submit();
				}
				else {
					alert("Falta informacion para poder Agregar");
				}
			}
			//************* genera reporte ************
			//***************************************
			function eliminar(idr)
			{
				if (confirm("Esta Seguro de Eliminar el Recibo de Caja"))
				{
				document.form2.oculto.value=2;
				document.form2.var1.value=idr;
				document.form2.submit();
				}
			}
			//************* genera reporte ************
			//***************************************
			function guardar()
			{
				if (document.form2.fecha.value!='')
				{
					if (confirm("Esta Seguro de Guardar"))
					{
					document.form2.oculto.value=2;
					document.form2.submit();
					}
				}
				else
				{
					alert('Faltan datos para completar el registro');
					document.form2.fecha.focus();
					document.form2.fecha.select();
				}
			}
			function pdf()
			{
				document.form2.action="teso-pdfconsignaciones.php";
				document.form2.target="_BLANK";
				document.form2.submit(); 
				document.form2.action="";
				document.form2.target="";
			}
			function verUltimaPos(idcta)
			{
				location.href="teso-editapagotercerosvigant.php?idpago="+idcta;
			}
						
		</script>
		<?php 
			$gidcta=$_GET['idpago'];

			if(isset($_GET['fini']) && isset($_GET['ffin'])){
				if(!empty($_GET['fini']) && !empty($_GET['ffin'])){
					$_POST['fecha']=$_GET['fini'];
					$_POST['fecha2']=$_GET['ffin'];
				}
			}
			$fech1=explode("/",$_POST['fechaini']);
			$fech2=explode("/",$_POST['fechafin']);
			$f1=$fech1[2]."-".$fech1[1]."-".$fech1[0];
			$f2=$fech2[2]."-".$fech2[1]."-".$fech2[0];

		?>
	</head>
	<body>
		<IFRAME src="alertas.php" name="alertas" id="alertas" style="display:none"></IFRAME>
		<span id="todastablas2"></span>
		<table>
			<tr><script>barra_imagenes("teso");</script><?php cuadro_titulos();?></tr>	 
			<tr><?php menu_desplegable("teso");?></tr>
			<tr>
				<td colspan="3" class="cinta">
					<a href="teso-pagoterceros-vigant.php" class="mgbt"><img src="imagenes/add.png" title="Nuevo"/></a>
					<a class="mgbt"><img src="imagenes/guardad.png"/></a>
					<a class="mgbt"><img src="imagenes/buscad.png"/></a>
					<a onClick="mypop=window.open('plan-agenda.php','','');mypop.focus()" class="mgbt"><img src="imagenes/agenda1.png" title="Agenda"/></a>
					<a onClick="mypop=window.open('teso-principal.php','','');mypop.focus();" class="mgbt"><img src="imagenes/nv.png" title="Nueva ventana"></a>
					<a onClick="mypop=window.open('<?php echo $url2; ?>','','');mypop.focus();" class="mgbt"><img src="imagenes/duplicar_pantalla.png" title="Duplicar pesta&ntilde;a"></a>
					<a onclick="crearexcel()" class="mgbt"><img src="imagenes/excel.png" title="Excell"></a>
				</td>
			</tr>	
		</table>

 		<form name="form2" method="post" action="teso-buscapagoterceros-vigant.php">
			<?php 
				if($_GET['numpag']!="")
				{
					$oculto=$_POST['oculto'];
					if($oculto!=2)
					{
					$_POST['numres']=$_GET['limreg'];
					$_POST['numpos']=$_GET['limreg']*($_GET['numpag']-1);
					$_POST['nummul']=$_GET['numpag']-1;
					}
				}
				else
				{
					if($_POST['nummul']=="")
					{
						$_POST['numres']=10;
						$_POST['numpos']=0;
						$_POST['nummul']=0;
					}
				}
			?>

			<table  class="inicio" align="center" >
				<tr >
					<td class="titulos" colspan="16">:. Buscar Pago Terceros Vigencias Anteriores</td>
					<td width="70" class="cerrar" ><a href="teso-principal.php"> Cerrar</a></td>
				</tr>
				<tr >
					<td class="saludo1" style="width:4cm;">No de pago: </td>
                    <td style="width:15%;"><input type="search" name="numero" id="numero"  value="<?php echo $_POST['numero'];?>" style="width:98%;"/></td> 
					<td class="saludo1" style="width:4cm;">Concepto de pago: </td>
                    <td style="width:15%;"><input type="search" name="nombre" id="nombre"  value="<?php echo $_POST['nombre'];?>" style="width:98%;"/></td> 
					<td class="tamano01" style="width:2cm;">Fecha inicial: </td>
					<td><input type="search" name="fechaini" id="fc_1198971545" title="DD/MM/YYYY" placeholder="DD/MM/YYYY" value="<?php echo @$_POST['fechaini'];?>" onKeyUp="return tabular(event,this)" onchange="" onKeyDown="mascara(this,'/',patron,true)" maxlength="10" style="width:75%;height:30px;">&nbsp;<img src="imagenes/calendario04.png" onClick="displayCalendarFor('fc_1198971545');" class="icobut" title="Calendario"></td>
                    <td  class="tamano01" style="width:2cm;">Fecha final: </td>
                    <td><input type="search" name="fechafin"  id="fc_1198971546" title="DD/MM/YYYY" placeholder="DD/MM/YYYY" value="<?php echo @$_POST['fechafin'];?>" onKeyUp="return tabular(event,this) " onchange="" onKeyDown="mascara(this,'/',patron,true)" maxlength="10" style="width:75%;height:30px;"/>&nbsp;<img src="imagenes/calendario04.png" onClick="displayCalendarFor('fc_1198971546');"  class="icobut" title="Calendario"></td>
					<td><input type="button" name="bboton" onClick="buscarbotonfiltro();" value="&nbsp;&nbsp;Buscar&nbsp;&nbsp;" /></td>  
				</tr>                     
			</table>  

			<input id="oculto" name="oculto" type="hidden" value="1">
			<input name="var1" type="hidden" value=<?php echo $_POST['var1'];?> >  
			<input type="hidden" name="fecham1"  id="fecham1" value="<?php echo $_POST['fecham1']; ?>"/>
			<input type="hidden" name="fecham2" id="fecham2" value="<?php echo $_POST['fecham2']; ?>"/>
			<input type="hidden" name="numres" id="numres" value="<?php echo $_POST['numres'];?>"/>
			<input type="hidden" name="numpos" id="numpos" value="<?php echo $_POST['numpos'];?>"/>
			<input type="hidden" name="nummul" id="nummul" value="<?php echo $_POST['nummul'];?>"/>

			<div class="subpantallap" style="height:68.5%; width:99.6%; overflow-x:hidden;">
				<?php
					$oculto=$_POST['oculto'];
					if($_POST['oculto']==2)
					{
						$sqlr="select * from tesopagotercerosvigant where id_pago	=$_POST[var1]";
						$resp = mysqli_query($linkbd,$sqlr);
						$row=mysqli_fetch_row($resp);
						//********Comprobante contable en 000000000000
						$sqlr="update comprobante_cab set estado='0' where tipo_comp='15' and numerotipo='$_POST[var1]'";
						mysqli_query($linkbd,$sqlr);
						/*$sqlr="update comprobante_det set valdebito=0,valcredito=0 where id_comp='15 $row[0]'";
						mysql_query($sqlr,$linkbd);*/
						
						//******** RECIBO DE CAJA ANULAR 'N'	 
						$sqlr="update tesopagotercerosvigant set estado='N' where id_pago=$row[0]";
						mysqli_query($linkbd,$sqlr);	  
					}
				?>
		
				<?php
					$oculto=$_POST['oculto'];
				

					$crit1=" ";
					$crit2=" ";

					if ($_POST['numero']!="")
					{
						$crit1=" AND tesopagotercerosvigant.id_pago LIKE '".$_POST['numero']."' ";
					}
					
					if ($_POST['nombre']!="")
					{
						$crit2=" AND tesopagotercerosvigant.concepto LIKE '".$_POST['nombre']."' ";
					}
					
					if($_POST['fechaini']!='')
					{
						preg_match("/([0-9]{2})\/([0-9]{2})\/([0-9]{4})/",$_POST['fechaini'],$fecha);
						$fechai="$fecha[3]-$fecha[2]-$fecha[1]";
						if($_POST['fechafin']!='')
						{
							preg_match("/([0-9]{2})\/([0-9]{2})\/([0-9]{4})/",$_POST['fechafin'],$fecha);
							$fechaf="$fecha[3]-$fecha[2]-$fecha[1]";
							$crit3 = "AND fecha BETWEEN '$fechai' AND '$fechaf'";
						}
						else
						{
							$fechaf=date("Y-m-d");
							$crit3 = "AND fecha BETWEEN '$fechai' AND '$fechaf'";
						}
					}

					//sacar el consecutivo 
					//$sqlr="select *from pptosideforigen where".$crit1.$crit2." order by pptosideforigen.codigo";
					$sqlr="SELECT * FROM tesopagotercerosvigant WHERE tesopagotercerosvigant.id_pago>-1 ".$crit1.$crit2.$crit3." ORDER BY tesopagotercerosvigant.id_pago DESC";

					// echo "<div><div>sqlr:".$sqlr."</div></div>";
					$resp = mysqli_query($linkbd,$sqlr);
					$ntr = mysqli_num_rows($resp);

					$_POST['numtop']=$ntr;

					$nuncilumnas=ceil($_POST['numtop']/$_POST['numres']);
					
					$cond2="";

					if ($_POST['numres'] != "-1"){ $cond2="LIMIT $_POST[numpos], $_POST[numres]";}

					$sqlr="SELECT * FROM tesopagotercerosvigant WHERE tesopagotercerosvigant.id_pago>-1 ".$crit1.$crit2.$crit3." ORDER BY tesopagotercerosvigant.id_pago DESC $cond2";
					$resp = mysqli_query($linkbd,$sqlr);
					
					$numcontrol=$_POST['nummul']+1;
					
					if(($nuncilumnas==$numcontrol)||($_POST['numres']=="-1")){
						$imagenforward="<img src='imagenes/forward02.png' style='width:17px'>";
						$imagensforward="<img src='imagenes/skip_forward02.png' style='width:16px' >";
					}
					else{
						$imagenforward="<img src='imagenes/forward01.png' style='width:17px' title='Siguiente' onClick='numsig()'>";
						$imagensforward="<img src='imagenes/skip_forward01.png' style='width:16px' title='Fin' onClick='saltopag(\"$nuncilumnas\")'>";
					}
					if(($_POST['numpos']==0)||($_POST['numres']=="-1")){
						$imagenback="<img src='imagenes/back02.png' style='width:17px'>";
						$imagensback="<img src='imagenes/skip_back02.png' style='width:16px'>";
					}
					else{
						$imagenback="<img src='imagenes/back01.png' style='width:17px' title='Anterior' onClick='numant();'>";
						$imagensback="<img src='imagenes/skip_back01.png' style='width:16px' title='Inicio' onClick='saltopag(\"1\")'>";
					}


					$con=1;

					echo "
					<table class='inicio' align='center' >
					<tr>
						<td colspan='7' class='titulos'>.: Resultados Busqueda:</td>
						<td class='submenu'>
							<select name='renumres' id='renumres' onChange='cambionumero();' style='width:100%'>
								<option value='10'"; if ($_POST['renumres']=='10'){echo 'selected';} echo ">10</option>
								<option value='20'"; if ($_POST['renumres']=='20'){echo 'selected';} echo ">20</option>
								<option value='30'"; if ($_POST['renumres']=='30'){echo 'selected';} echo ">30</option>
								<option value='50'"; if ($_POST['renumres']=='50'){echo 'selected';} echo ">50</option>
								<option value='100'"; if ($_POST['renumres']=='100'){echo 'selected';} echo ">100</option>
								<option value='-1'"; if ($_POST['renumres']=='-1'){echo 'selected';} echo ">Todos</option>
							</select>
						</td>
					</tr>
					<tr>
						<td colspan='2' id='RecEnc'>Recaudos Encontrados: $ntr2</td>
					</tr>
					<tr>
						<td class='titulos2'>Codigo</td>
						<td class='titulos2'>Concepto</td>
						<td class='titulos2'>Fecha</td>
						<td class='titulos2'>Beneficiario</td>
						<td class='titulos2'>Valor</td>
						<td class='titulos2'>Estado</td>
						<td class='titulos2' width='5%'><center>Anular</td>
						<td class='titulos2' width='5%'><center>Ver</td>
					</tr>";	
					
					$iter='zebra1';
					$iter2='zebra2';

					if($_POST['fechaini'] == '' && $_POST['fechafin'] == '' && $_POST['numero'] == '' && $_POST['nombre'] == '')
					{
						echo "
						<table class='inicio'>
							<tr>
								<td class='saludo1' style='text-align:center;width:100%;font-size:25px'>Utilice el filtro de busqueda</td>
							</tr>
						</table>";
						$nuncilumnas = 0;
					}
					elseif(mysqli_num_rows($resp) == 0 || mysqli_num_rows($resp) == '0')
					{
						echo "
						<table class='inicio'>
							<tr>
								<td class='saludo1' style='text-align:center;width:100%;font-size:25px'>No hay resultados de su busqueda.</td>
							</tr>
						</table>";
					}
					else
					{

						while ($row =mysqli_fetch_row($resp)) 
						{
							$ntr2 = $ntr1;

							echo "<script>document.getElementById('RecEnc').innerHTML = 'Recaudos encontrados: $ntr2'</script>";

							$nter=buscatercero($row[1]);
							if($row[9]=='S')
								$imgsem="src='imagenes/sema_verdeON.jpg' title='Activo'"; 	 				  
							if($row[9]=='N')
								$imgsem="src='imagenes/sema_rojoON.jpg' title='Inactivo'"; 
							if($gidcta!="")
							{
								if($gidcta==$row[0])
								{
									$estilo='background-color:#FF9';
								}
								else
								{
									$estilo="";
								}
							}
							else
							{
								$estilo="";
							}
							$idcta="'".$row[0]."'";
							echo "
							<tr class='$iter' onMouseOver=\"anterior=this.style.backgroundColor;this.style.backgroundColor='#40b3ff';\"
									onMouseOut=\"this.style.backgroundColor=anterior\" onDblClick=\"verUltimaPos($idcta)\" style='text-transform:uppercase; $estilo'>
								<td>$row[0]</td>
								<td>$row[7]</td>
								<td>$row[10]</td>
								<td>$nter</td>
								<td>".number_format($row[5],2)."</td>
								<td style='text-align:center;'><img $imgsem style='width:18px'></td>";
								if($row[9]=='S')
								echo "<td ><a href='#'  onClick=eliminar($row[0])><center><img src='imagenes/anular.png'></center></a></td>";		 
								if($row[9]=='N')
								echo "<td></td>";	 
							echo "<td ><a href='teso-editapagotercerosvigant.php?idpago=$row[0]'><center><img src='imagenes/lupa02.png' style='width:18px'></center></a></td></tr>";
							$con+=1;
							$aux=$iter;
							$iter=$iter2;
							$iter2=$aux;
				?>
						<input type="hidden" name="code[]" id="code[]" value="<?php echo $row[0];?>"/>
						<input type="hidden" name="cont[]" id="cont[]" value="<?php echo $row[7];?>"/>
						<input type="hidden" name="fecha[]" id="fecha[]" value="<?php echo $row[10];?>"/>
						<input type="hidden" name="ben[]" id="ben[]" value="<?php echo $nter;?>"/>
						<input type="hidden" name="num[]" id="num[]" value="<?php echo number_format($row[5],2);?>"/>
					
				<?php
						}
					}
					echo"</table>
					<table class='inicio'>
						<tr>
							<td style='text-align:center;'>
								<a href='#'>$imagensback</a>&nbsp;
								<a href='#'>$imagenback</a>&nbsp;&nbsp;";
								if($nuncilumnas<=9){$numfin=$nuncilumnas;}
								else{$numfin=9;}
								for($xx = 1; $xx <= $numfin; $xx++)
								{
									if($numcontrol<=9){$numx=$xx;}
									else{$numx=$xx+($numcontrol-9);}
									if($numcontrol==$numx){echo"<a href='#' onClick='saltopag(\"$numx\")'; style='color:#24D915'> $numx </a>";}
									else {echo"<a href='#' onClick='saltopag(\"$numx\")'; style='color:#000000'> $numx </a>";}
								}
								echo"&nbsp;&nbsp;<a href='#'>$imagenforward</a>
									&nbsp;<a href='#'>$imagensforward</a>
							</td>
						</tr>
					</table>";
				?>

			</div>
		</form>
	</body>
</html>