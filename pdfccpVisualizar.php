<?php
//V 1000 12/12/16  
	require_once("tcpdf/tcpdf_include.php");
	require('comun.inc');
	require "funciones.inc";
	session_start();
	$linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");
	date_default_timezone_set("America/New_York");

	$sqlr = "SELECT id_cargo, id_comprobante FROM pptofirmas WHERE id_comprobante='6' AND vigencia='".$_POST['vigencia']."' ORDER BY id_cargo DESC";
	$res = mysqli_query($linkbd,$sqlr);
	while($row=mysqli_fetch_assoc($res))
	{
		if($row["id_cargo"] == '0')
		{
			$_POST['ppto'][] = buscatercero($_POST['tercero']);
			$_POST['nomcargo'][]='BENEFICIARIO';
		}
		else
		{
			$sqlr1="SELECT cedulanit,(SELECT nombrecargo FROM planaccargos WHERE codcargo='".$row["id_cargo"]."') FROM planestructura_terceros WHERE codcargo='".$row["id_cargo"]."' AND estado='S'";
			$res1 = mysqli_query($linkbd,$sqlr1);
			$row1 = mysqli_fetch_row($res1);
			$_POST['ppto'][] = buscatercero($row1[0]);
			$_POST['nomcargo'][] = $row1[1];
		}
	}
	//var_dump($_POST['nomcargo']);
	class MYPDF extends TCPDF 
	{
		public function Header() 
		{
			$linkbd = conectar_v7();
			$linkbd -> set_charset("utf8");
			$sqlr="SELECT nit, razonsocial FROM configbasica WHERE estado='S'";
			$resp=mysqli_query($linkbd,$sqlr);
			while($row=mysqli_fetch_row($resp)){$nit=$row[0];$rs=$row[1];}

            //-------------- RECUADRO DEL ENCABEZADO  --------------------------------
			$this->Image('imagenes/escudo.jpg', 15, 14, 25, 23.9, 'JPG', '', 'T', true, 300, '', false, false, 0, false, false, false);// Logo
			$this->SetFont('helvetica','B',8);
			$this->SetY(10);
			$this->RoundedRect(10, 10, 280, 31, 2.5,''); //Borde del encabezado - Recuadro del encabezado
			$this->Cell(35,31,'','R',0,'L'); //Linea que separa el encabazado verticalmente
			$this->SetY(10);
			$this->SetFont('helvetica','B',12);

			if(strlen($rs)<40)
			{
				$this->SetX(40);
				$this->Cell(230,15,$rs,0,0,'C');  //Posicion Municipio
				$this->SetY(16);
			}
			else
			{
				$this->Cell(61);
				$this->MultiCell(200,15,$rs,0,'C',false,1,'','',true,4,false,true,19,'T',false);
				$this->SetY(18);
			}
			
            //--------------------- CONTENIDO DENTRO DEL RECUADRO DEL ENCABEZADO -----------------------------------------

			$this->SetX(40);  //Posicion Numero Nit
			$this->SetFont('helvetica','B',8);
			$this->Cell(230,12,"NIT: $nit",0,0,'C'); // Posicion Numero Nit
			$this->SetY(27);                    // Posicion Certificado
			$this->SetX(45);                    // Posicion Certificado
			$this->SetFont('helvetica','B',11);
			$this->Cell(192,14,"CERTIFICADO DE DISPONIBILIDAD PRESUPUESTAL ",1,0,'C');        // Recuadro del certificado
			$this->SetFont('helvetica','I',10);
			$this->SetY(27);
			$this->SetX(62);
			$mov='';
			if(isset($_POST['movimiento']))
			{
				if($_POST['movimiento']=='401' || $_POST['movimiento']=='402'){
					$mov="DOCUMENTO DE REVERSION";
				}else if($_POST['estadoc'] == 'ANULADO'){
					$mov = "DOCUMENTO ANULADO";
				}
			}
			$this->SetFont('helvetica','B',10);
			$this->Cell(228,7,$mov,'T',0,'C',false,0,1); 
			$this->SetFont('helvetica','B',9);
			$this->SetY(27);
			$this->SetX(243);                                      // Posicion Numero  
			$this->Cell(37,5," NUMERO: ".$_POST['numero'],'T',0,'L');
			$this->SetY(31);
			$this->SetX(243);                                      // Posicion Fecha     
			$this->Cell(35,6," FECHA: ".$_POST['fecha'],0,0,'L');
			$this->SetY(36);
			$this->SetX(243);                                      //  Posicion Vigencia 
			$this->Cell(35,5," VIGENCIA: ".$_POST['vigencia'],0,0,'L');

			//------------------ Informacion TRD -----------------------------------
			if($_POST['codigoTRD'] != ''){
				$this->SetY(41);
				$this->SetX(10);
				$this->Cell(35,5,$_POST['codigoTRD'],0,0,'L');
				$this->SetY(18);
				$this->SetX(253);
				$this->Cell(37,5," Versión : ".$_POST['versionTRD'],0,0,'L');
				$this->SetY(21);
				$this->SetX(253);
				$this->Cell(35,6," Fecha : ".$_POST['fechaTRD'],0,0,'L');
			}
			//------------------ META PDM -----------------------------------
			
			
		}

		public function Footer() 
		{
			$linkbd = conectar_v7();
			$linkbd -> set_charset("utf8");

            $sqlrLema = "SELECT lema FROM interfaz01";
            $respLema = mysqli_query($linkbd,$sqlrLema);
            $rowLema = mysqli_fetch_row($respLema);

			$sqlr="SELECT direccion,telefono,web,email FROM configbasica WHERE estado='S'";
			$resp=mysqli_query($linkbd,$sqlr);
			$user = $_SESSION['nickusu'];	
			$fecha = date("Y-m-d H:i:s");
			$ip = $_SERVER['REMOTE_ADDR'];
			$useri = $_POST['user'];
			while($row=mysqli_fetch_row($resp))
			{
				$direcc=strtoupper($row[0]);
				$telefonos=$row[1];
				$dirweb=strtoupper($row[3]);
				$coemail=$row[2];
			}

			if($direcc!=''){$vardirec="Dirección: $direcc, ";}
			else {$vardirec="";}
			if($telefonos!=''){$vartelef="Telefonos: $telefonos";}
			else{$vartelef="";}
			if($dirweb!=''){$varemail="Email: $dirweb, ";}
			else {$varemail="";}
			if($coemail!=''){$varpagiw="Pagina Web: $coemail";}
			else{$varpagiw="";}
			//$this->SetY(-16);
			$this->SetFont('helvetica', 'I', 8);
			$txt = <<<EOD
			$vardirec $vartelef
			$varemail $varpagiw
			EOD;
			$this->Cell(277,10,'','T',0,'T');
            $this->ln(2);
            //$this->Cell(277,10,$rowLema[0],'T',0,'T');
            $this->Write(0, $rowLema[0] , '', 0, 'C', true, 0, false, false, 0);
			//$this->ln(2);
			$this->Write(0, $txt , '', 0, 'C', true, 0, false, false, 0);
			$this->Cell(25, 10, 'Hecho por: '.$useri, 0, false, 'C', 0, '', 0, false, 'T', 'M');
			$this->Cell(100, 10, 'Impreso por: '.$user, 0, false, 'C', 0, '', 0, false, 'T', 'M');
			$this->Cell(30, 10, 'IP: '.$ip, 0, false, 'C', 0, '', 0, false, 'T', 'M');
			$this->Cell(102);
			//$this->Cell(102, 10, 'Fecha: '.$fecha, 0, false, 'C', 0, '', 0, false, 'T', 'M');
			$this->Cell(5, 10, 'IDEAL.10 S.A.S    Pagina '.$this->getAliasNumPage().'/'.$this->getAliasNbPages(), 0, false, 'C', 0, '', 0, false, 'T', 'M');
			
		}
	}
	
    //----------------------------------------------------------------------

	$pdf = new MYPDF('L','mm','Letter', true, 'iso-8859-1', false);// create new PDF document // Orientacion del pdf
	$pdf->SetDocInfoUnicode (true); 
	// set document information
	$pdf->SetCreator(PDF_CREATOR);
	$pdf->SetAuthor('Ideal10sas');
	$pdf->SetTitle('Certificados');
	$pdf->SetSubject('Certificado de Disponibilidad');
	$pdf->SetKeywords('TCPDF, PDF, example, test, guide');
	$pdf->SetMargins(10, 60, 10);// set margins // Posicion de los valores de cada columna
	$pdf->SetHeaderMargin(90);// set margins
	$pdf->SetFooterMargin(20);// set margins
	$pdf->SetAutoPageBreak(TRUE, 20);// set auto page breaks
	// set some language-dependent strings (optional)
	if (@file_exists(dirname(__FILE__).'/lang/spa.php')) 
	{
		require_once(dirname(__FILE__).'/lang/spa.php');
		$pdf->setLanguageArray($l);
	}

	// ---------------------------------------------------------
	$pdf->AddPage();
	$pdf->SetFont('helvetica','I',9);
	
	$crit1=" ";
	$crit2=" ";
	$crit3=" ";
	

	$vig=vigencia_usuarios($_SESSION['cedulausu']);
	if ($_POST['vigencia'] != ""){$crit1 = " AND TB1.vigencia ='".$_POST['vigencia']."' ";}
	else {$crit1 = " AND TB1.vigencia ='$vig' ";}
	if ($_POST['numero'] != ""){$crit2=" AND TB1.consvigencia like '%".$_POST['numero']."%' ";}
	if ($_POST['fechaini'] != "" and $_POST['fechafin'] != "" )
	{
		preg_match("/([0-9]{2})\/([0-9]{2})\/([0-9]{4})/", $_POST['fechaini'], $fecha);
		$fechai = $fecha[3]."-".$fecha[2]."-".$fecha[1];
		preg_match("/([0-9]{2})\/([0-9]{2})\/([0-9]{4})/", $_POST['fechafin'], $fecha);
		$fechaf = $fecha[3]."-".$fecha[2]."-".$fecha[1];
		$crit3=" AND TB1.fecha between '$fechai' and '$fechaf'  ";
	}

	/* $this->Cell(25,5,'tipo gasto',1,0,'C',false,'T','C');
	$this->Cell(20,5,'sec. presu.',1,0,'C',false,0,0,false,'T','C');
	$this->Cell(20,5,'Medio pago',1,0,'C',false,0,0,false,'T','C');
	$this->Cell(25,5,'Vig gasto',1,0,'C',false,0,0,false,'T','C');
	$this->Cell(20,5,'Proyecto',1,0,'C',false,0,0,false,'T','C');
	$this->Cell(20,5,'Programatico',1,0,'C',false,0,0,false,'T','C');
	$this->Cell(30,5,'Cuenta',1,0,'C',false,0,0,false,'T','C');
	$this->Cell(30,5,'Fuente',1,0,'C',false,0,0,false,'T','C');
	$this->Cell(20,5,'CPC',1,0,'C',false,0,0,false,'T','C');
	$this->Cell(20,5,'Divipola',1,0,'C',false,0,0,false,'T','C');
	$this->Cell(20,5,'CHIP',1,0,'C',false,0,0,false,'T','C');
	$this->Cell(25,5,'Valor',1,1,'C',false,0,0,false,'T','C'); */

	$pdf->SetFont('times','B',12);
	$pdf->SetY(44);
	$pdf->MultiCell(280,5,'EL SUSCRITO '.$_POST['nomcargo'][0],'','C');
	$pdf->Cell(280,12,'CERTIFICA:',0,0,'C');
	$pdf->SetY(60);
	$pdf->SetFont('times','',10);
	$pdf->cell(0.1);
	$pdf->SetY(65);
	$pdf->MultiCell(280,6,'Que de acuerdo con el Presupuesto General de Ingresos y Gastos, para la vigencia fiscal de '.$_POST['vigencia'].', existe saldo disponible y no comprometido, para amparar el compromiso que se pretende adquirir a continuación:',0,'L',false,1,'','',true,0,false,true,0,'T',false);
	$pdf->ln(4);
	$pdf->SetFont('times','B',10);
	$pdf->Cell(30,10,"SOLICITANTE:",0,0,'L');
	$pdf->SetFont('times','',10);
	
	$pdf->Cell(250,10,$_POST['solicita'],0,1,'L',false,0,0,false,'T','C');

	$pdf->SetFont('times','B',10);
	$pdf->Cell(17,5,"OBJETO:",0,0,'L');
	$pdf->SetFont('times','',10);
	// MultiCell($w, $h, $txt, $border=0, $align='J', $fill=0, $ln=1, $x='', $y='', $reseth=true, $stretch=0, $ishtml=false, $autopadding=true, $maxh=0)
	$pdf->MultiCell(250,16.6,$_POST['objeto'],0,'L',false,1,40,'',true,0,false,true,19,'T',false);
	/* $pdf->SetFont('helvetica','B',7);
	$pdf->MultiCell(169,4,$_POST['meta'].' '.$_POST['nmeta'],0,'L',false,1,'','',true,0,false,true,0,'T',false);
	$pdf->ln(2); */

	$pdf->SetFillColor(200, 200, 200);
	$pdf->SetFont('helvetica','B',6);
	$posy=$pdf->GetY();
	$pdf->SetY($posy+1);

	$pdf->Cell(25,5,'Rubro',1,0,'C',true,0,0,false,'C','C');
	$pdf->Cell(30,5,'Sec. presu.',1,0,'C',true,0,0,false,'C','C');
	$pdf->Cell(20,5,'Medio pago',1,0,'C',true,0,0,false,'C','C');
	$pdf->Cell(25,5,'Vig gasto',1,0,'C',true,0,0,false,'C','C');
	$pdf->Cell(40,5,'Proyecto',1,0,'C',true,0,0,false,'C','C');
	$pdf->Cell(25,5,'Programatico',1,0,'C',true,0,0,false,'C','C');
	$pdf->Cell(30,5,'CCPET',1,0,'C',true,0,0,false,'C','C');
	$pdf->Cell(30,5,'Fuente',1,0,'C',true,0,0,false,'C','C');
	$pdf->Cell(25,5,'CPC',1,0,'C',true,0,0,false,'C','C');
	$pdf->Cell(30,5,'Valor',1,1,'C',true,0,0,false,'C','C');
	$con=0;
	while ($con<count($_POST['dcuenta']))
	{
		$altura=4;
		$altini=4;
		$ancini=20;
		$altaux=0;
		$colst01=strlen($_POST['dcuenta'][$con]);
		$colst02=strlen($_POST['dfuente'][$con]);
		if($colst01>$colst02){$cantidad_lineas= $colst01;}
		else{$cantidad_lineas= $colst02;}
		if($cantidad_lineas > $ancini)
		{
			$cant_espacios = $cantidad_lineas/$ancini;
			$rendondear=ceil($cant_espacios);
			$altaux=$altini*$rendondear;
		}
		if($altaux>$altura){$altura=$altaux;}
		/* if ($concolor==0){$pdf->SetFillColor(200,200,200);$concolor=1;}
		else {$pdf->SetFillColor(255,255,255);$concolor=0;} */
		
		
		/* $this->Cell(20,5,'tipo gasto',1,0,'C',false,'T','C');
			$this->Cell(20,5,'sec. presu.',1,0,'C',false,0,0,false,'T','C');
			$this->Cell(20,5,'Medio pago',1,0,'C',false,0,0,false,'T','C');
			$this->Cell(25,5,'Vig gasto',1,0,'C',false,0,0,false,'T','C');
			$this->Cell(20,5,'Proyecto',1,0,'C',false,0,0,false,'T','C');
			$this->Cell(20,5,'Programatico',1,0,'C',false,0,0,false,'T','C');
            $this->Cell(30,5,'Cuenta',1,0,'C',false,0,0,false,'T','C');
            $this->Cell(30,5,'Fuente',1,0,'C',false,0,0,false,'T','C');
            $this->Cell(20,5,'CPC',1,0,'C',false,0,0,false,'T','C');
            $this->Cell(20,5,'Divipola',1,0,'C',false,0,0,false,'T','C');
            $this->Cell(20,5,'CHIP',1,0,'C',false,0,0,false,'T','C');
			$this->Cell(20,5,'Valor',1,1,'C',false,0,0,false,'T','C'); */

		// MultiCell($w, $h, $txt, $border=0, $align='J', $fill=0, $ln=1, $x='', $y='', $reseth=true, $stretch=0, $ishtml=false, $autopadding=true, $maxh=0)
		$v=$pdf->gety();
		if($v>=155){ 
			$pdf->AddPage();
			$pdf->SetFillColor(200, 200, 200);
			$pdf->SetFont('helvetica','B',6);
			$posy=$pdf->GetY();
			$pdf->SetY($posy+1);

			$pdf->Cell(25,5,'Rubro',1,0,'C',true,0,0,false,'C','C');
			$pdf->Cell(30,5,'Sec. presu.',1,0,'C',true,0,0,false,'C','C');
			$pdf->Cell(20,5,'Medio pago',1,0,'C',true,0,0,false,'C','C');
			$pdf->Cell(25,5,'Vig gasto',1,0,'C',true,0,0,false,'C','C');
			$pdf->Cell(40,5,'Proyecto',1,0,'C',true,0,0,false,'C','C');
			$pdf->Cell(25,5,'Programatico',1,0,'C',true,0,0,false,'C','C');
			$pdf->Cell(30,5,'CCPET',1,0,'C',true,0,0,false,'C','C');
			$pdf->Cell(30,5,'Fuente',1,0,'C',true,0,0,false,'C','C');
			$pdf->Cell(25,5,'CPC',1,0,'C',true,0,0,false,'C','C');
			$pdf->Cell(30,5,'Valor',1,1,'C',true,0,0,false,'C','C');
		}

		$rubro = '';
		$partsFuente = explode('-', $_POST['dfuente'][$con]);
		if($_POST['dbpim'][$con] != ''){
			
			$rubro = $_POST['dprogramatico'][$con]."-".$_POST['dbpim'][$con]."-".$partsFuente[0];
		}else{
			$partsCuenta = explode("-", $_POST['dcuenta'][$con]);
			$rubro = $partsCuenta[0]."-".$partsFuente[0];
		}
		
		$pdf->SetFillColor(255,255,255);
		$pdf->SetFont('times','',9);
		$pdf->MultiCell(25,$altura,$rubro,1,'C',true,0,'','',true,0,false,true,$altura,'M',false);
		$pdf->MultiCell(30,$altura,strtolower($_POST['dsecPresupuestal'][$con]),1,'C',true,0,'','',true,0,false,true,$altura,'M',false);
        //$pdf->Cell(25,$altura,"  ".$_POST[dfuentes][$con],1,0,'L',true,0,0,false,'T','C');
		$pdf->MultiCell(20,$altura,$_POST['dmedioPago'][$con],1,'C',true,0,'','',true,0,false,true,$altura,'M',false);
		$pdf->MultiCell(25,$altura,$_POST['dvigGasto'][$con],1,'C',true,0,'','',true,0,false,true,$altura,'M',false);
		$pdf->SetFont('times','',8);
		$pdf->MultiCell(40,$altura,$_POST['dbpim'][$con].' - '.strtolower($_POST['nombreProyecto'][$con]),1,'C',true,0,'','',true,0,false,true,$altura,'M',false);
		$pdf->MultiCell(25,$altura,$_POST['dprogramatico'][$con].' - '.$_POST['indpro'][$con],1,'C',true,0,'','',true,0,false,true,$altura,'M',false);
		$pdf->SetFont('times','',9);
		$pdf->MultiCell(30,$altura,$_POST['dcuenta'][$con],1,'C',true,0,'','',true,0,false,true,$altura,'M',false);
		$pdf->MultiCell(30,$altura,$_POST['dfuente'][$con],1,'C',true,0,'','',true,0,false,true,$altura,'M',false);
		$pdf->MultiCell(25,$altura,$_POST['dproductoservicio'][$con]. ' - '. $_POST['nomCpc'][$con],1,'C',true,0,'','',true,0,false,true,$altura,'M',false);
        //$pdf->MultiCell(50,$altura,iconv($_SESSION["VERCARPDFINI"], $_SESSION["VERCARPDFFIN"]."//TRANSLIT",$_POST[dServProduct][$con].' - '.$_POST[dnServProduct][$con]),1,'L',true,0,'','',true,0,false,true,$altura,'M',false);
		$pdf->Cell(30,$altura,"$ ".number_format($_POST['dvalor'][$con],2,$_SESSION["spdecimal"],$_SESSION["spmillares"])."   ",1,1,'C',true,0,0,false,'T','C');
		$con++;
	}

    //-------------------  Nombre/ Cargo / Valor total ----------------
	$sql="SELECT user FROM pptocdp WHERE consvigencia='".$_POST['numero']."' AND vigencia='".$_POST['vigencia']."' ";
	$res=mysqli_query($linkbd,$sql);
	$row = mysqli_fetch_row($res);
	$pdf->Cell(250,6,'TOTAL:',0,0,'R',false,0,0,false,'T','C');
	$pdf->setFont('times','B',9);
	$pdf->Cell(30,6,"$ ".number_format($_POST['cuentagas2'],2,$_SESSION["spdecimal"],$_SESSION["spmillares"])."  ",1,1,'R',false,0,0,false,'T','C');
	$pdf->ln(2);
	$v=$pdf->gety();
	if($v>=180){
		$pdf->AddPage();
		$v=$pdf->gety();
	}
	$pdf->MultiCell(280,8,'SON: '.strtoupper($_POST['letras']." M/CTE"),1,'L',false,1,'','',true,0,false,true,8,'M',false); // Valor letra
	$pdf->ln(8);
	for($x=0;$x<count($_POST['ppto']);$x++)
	{
		/* $pdf->ln(6);
		$v=$pdf->gety();
		if($v>=180){
			$pdf->AddPage();
			$pdf->ln(15);
			$v=$pdf->gety();
		} */
		$pdf->setFont('times','B',8);
		$pdf->ln(6);
		$v=$pdf->gety();
		if($v>=180)
		{
			$pdf->AddPage();
			$pdf->ln(15);
			$v=$pdf->gety();
		}
		$pdf->setFont('times','B',7);
		
		if (($x%2)==0) 
		{
			if(isset($_POST['ppto'][$x+1]))
			{
				$pdf->Line(40,$v,130,$v);
				$pdf->Line(170,$v,260,$v);
				$v2=$pdf->gety();
				$pdf->Cell(150,4,''.$_POST['ppto'][$x],0,1,'C',false,0,0,false,'T','C');
				$pdf->Cell(150,4,''.$_POST['nomcargo'][$x],0,1,'C',false,0,0,false,'T','C');
				$pdf->SetY($v2);
				$pdf->Cell(410,4,''.$_POST['ppto'][$x+1],0,1,'C',false,0,0,false,'T','C');
				$pdf->Cell(410,4,''.$_POST['nomcargo'][$x+1],0,1,'C',false,0,0,false,'T','C');
				
			}
			else
			{
				$v = $v + 5;
				$pdf->ln(5);
				
				$pdf->Line(105,$v,195,$v);
				$pdf->Cell(280,4,''.$_POST['ppto'][$x],0,1,'C',false,0,0,false,'T','C');
				$pdf->Cell(280,4,''.$_POST['nomcargo'][$x],0,0,'C',false,0,0,false,'T','C');
			}
			$v3=$pdf->gety();
		}
		$pdf->SetY($v3);
		$pdf->SetFont('helvetica','',7);
	}
	// ---------------------------------------------------------
	$pdf->Output('reportecdp.pdf', 'I');//Close and output PDF document
?>
