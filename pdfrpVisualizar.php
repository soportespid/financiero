<?php
	require_once("tcpdf/tcpdf_include.php");
	require('comun.inc');
	require"funciones.inc";
	session_start();
	$desplaza = 0;
	date_default_timezone_set("America/New_York");
	class MYPDF extends TCPDF 
	{
		public function Header() 
		{	
			if ($_POST['estadoc']=='ANULADO'){$this->Image('imagenes/anulado.jpg',30,15,150,80);}
			$linkbd=conectar_v7();
			$linkbd -> set_charset("utf8");
			$sqlr="SELECT * FROM configbasica WHERE estado='S'";
			$res=mysqli_query($linkbd,$sqlr);
			while($row=mysqli_fetch_row($res))
			{
				$nit=$row[0];
				$rs=$row[1];
			}
			$this->Image('imagenes/escudo.jpg', 15, 14, 25, 23.9, 'JPG', '', 'T', true, 300, '', false, false, 0, false, false, false);// Logo
			$this->SetFont('helvetica','B',8);
			$this->SetY(10);
			$this->RoundedRect(10, 10, 280, 31, 2.5,''); //Borde del encabezado - Recuadro del encabezado
			$this->Cell(0.1);
			$this->Cell(35,31,'','R',0,'L'); //Linea que separa el encabazado verticalmente
			$this->SetY(10);
			$mov='';
			if(isset($_POST['movimiento']))
			{
				if($_POST['movimiento']=='401' || $_POST['movimiento']=='402'){$mov="DOCUMENTO DE REVERSION";}
			}
			$this->Cell(149,31,$mov,0,1,'C'); 
			$this->SetY(10);
			
			$this->SetFont('helvetica','B',14);
			if(strlen($rs)<80)
			{
				$this->Cell(50.1);
				$this->Cell(180,15,"$rs",0,0,'C');
				$this->SetY(10);
			}
			else
			{
				$this->Cell(74.1);
				$this->MultiCell(200,15,$rs,0,'C',false,1,'','',true,4,false,true,19,'T',false);
				$this->SetY(12);
			}
			//$this->MultiCell(149,15,$rs,0,'C',false,1,'','',true,4,false,true,19,'T',false);
			
			$this->Cell(40);
			$this->SetFont('helvetica','B',10);
			$this->Cell(200	,25,"$nit",0,0,'C'); 
			$this->SetY(27);
			$this->Cell(242.1);
			$this->Cell(38,14,'','TL',0,'C');
			$this->SetY(27.5);
			$this->Cell(243);
			$this->Cell(35,5,'No RP : '.$_POST['numero'],0,0,'L');
			$this->SetY(31.5);
			$this->Cell(243);
			$this->Cell(35,5,'VIGENCIA F.: '.$_POST['vigencia'],0,0,'L');
			$this->SetY(35.5);
			$this->Cell(243);
			$this->Cell(35,5,'FECHA: '.$_POST['fecha'],0,0,'L');
			$this->SetY(27);
			$this->Cell(35);
			$this->SetFont('helvetica','B',12);
			$this->Cell(207,8,'REGISTRO PRESUPUESTAL DE COMPROMISO',1,0,'C'); 
			$this->SetFont('helvetica','B',10);
			$this->SetY(36);
			$this->Cell(75);
			$this->Cell(25,5,'CDP No: '.$_POST['numerocdp'],0,0,'L');
			$this->SetY(36);
			$this->Cell(117);
			$this->Cell(82,5,'EXPEDIDO EL: '.$_POST['fechacdp'],0,0,'L');
			$this->SetY(36);
			$this->Cell(180);
			$this->Cell(34.1,5,'Contrato: '.$_POST['ncontrato'],'',0,'L');

			//------------------ Informacion TRD -----------------------------------
			if($_POST['codigoTRD'] != ''){
				$this->SetY(36);
				$this->SetX(180);
				$this->Cell(35,5,$_POST['codigoTRD'],0,0,'L');
				$this->SetY(18);
				$this->SetX(252);
				$this->SetFont('helvetica','B',9);
				$this->Cell(37,5," Versión : ".$_POST['versionTRD'],0,0,'L');
				$this->SetY(21);
				$this->SetX(252);   
				$this->Cell(35,6," Fecha : ".$_POST['fechaTRD'],0,0,'L');
			}
		}
		function Footer()
		{
			$linkbd=conectar_v7();
			$linkbd -> set_charset("utf8");
			$sqlr="SELECT direccion,telefono,web,email FROM configbasica WHERE estado='S'";
			$resp=mysqli_query($linkbd,$sqlr);
			$user = $_SESSION['nickusu'];	
			$fecha = date("Y-m-d H:i:s");
			$ip = $_SERVER['REMOTE_ADDR'];
			$useri = $_POST['user'];
			while($row=mysqli_fetch_row($resp))
			{
				$direcc=strtoupper($row[0]);
				$telefonos=$row[1];
				$dirweb=strtoupper($row[3]);
				$coemail=strtoupper($row[2]);
			}
			if($direcc!=''){$vardirec="Dirección: $direcc, ";}
			else {$vardirec="";}
			if($telefonos!=''){$vartelef="Telefonos: $telefonos";}
			else{$vartelef="";}
			if($dirweb!=''){$varemail="Email: $dirweb, ";}
			else {$varemail="";}
			if($coemail!=''){$varpagiw="Pagina Web: $coemail";}
			else{$varpagiw="";}
			//$this->SetY(-16);
			$this->SetFont('helvetica', 'I', 8);
			$txt = <<<EOD
			$vardirec $vartelef
			$varemail $varpagiw
			EOD;
			$this->Cell(277,10,'','T',0,'T');
			$this->ln(2);
			$this->Write(0, $txt , '', 0, 'C', true, 0, false, false, 0);
			$this->Cell(25, 10, 'Hecho por: '.$useri, 0, false, 'C', 0, '', 0, false, 'T', 'M');
			$this->Cell(100, 10, 'Impreso por: '.$user, 0, false, 'C', 0, '', 0, false, 'T', 'M');
			$this->Cell(30, 10, 'IP: '.$ip, 0, false, 'C', 0, '', 0, false, 'T', 'M');
            $this->Cell(102);
			/* $this->Cell(102, 10, 'Fecha: '.$fecha, 0, false, 'C', 0, '', 0, false, 'T', 'M'); */
			$this->Cell(5, 10, 'IDEAL.10 S.A.S    Pagina '.$this->getAliasNumPage().'/'.$this->getAliasNbPages(), 0, false, 'C', 0, '', 0, false, 'T', 'M');
			
		}
	}
	$linkbd=conectar_v7();
	$linkbd -> set_charset("utf8");
	$sqlr="select id_cargo,id_comprobante from pptofirmas where id_comprobante='7' and vigencia='".$_POST['vigencia']."'";
	$res=mysqli_query($linkbd,$sqlr);
	
	while($row=mysqli_fetch_assoc($res))
	{
		if($row["id_cargo"]=='0')
		{
			$_POST['ppto'][]=buscatercero($_POST['tercero']);
			$_POST['nomcargo'][]='BENEFICIARIO';
		}
		else
		{
			$sqlr1="select cedulanit,(select nombrecargo from planaccargos where codcargo='".$row["id_cargo"]."') from planestructura_terceros where codcargo='".$row["id_cargo"]."' and estado='S'";
			$res1=mysqli_query($linkbd,$sqlr1);
			$row1=mysqli_fetch_row($res1);
			$_POST['ppto'][]=buscar_empleado($row1[0]);
			$_POST['nomcargo'][]=$row1[1];
		}
	}
	$pdf = new MYPDF('L','mm','Letter', true, 'iso-8859-1', false);
	$pdf->SetDocInfoUnicode (true); 
	$pdf->SetCreator(PDF_CREATOR);
	$pdf->SetAuthor('IDELA10SAS');
	$pdf->SetTitle('Registro');
	$pdf->SetSubject('Registro de Disponibilidad');
	$pdf->SetKeywords('TCPDF, PDF, example, test, guide');
	$pdf->SetMargins(10, 45, 10);// set margins
	$pdf->SetHeaderMargin(45);// set margins
	$pdf->SetFooterMargin(20);// set margins
	$pdf->SetAutoPageBreak(TRUE, 20);// set auto page breaks
	// set some language-dependent strings (optional)
	if (@file_exists(dirname(__FILE__).'/lang/spa.php')) 
	{
		require_once(dirname(__FILE__).'/lang/spa.php');
		$pdf->setLanguageArray($l);
	}
	$pdf->AddPage();
	$pdf->SetFont('times','B',10);
	$pdf->Cell(28,5,'BENEFICIARIO:',0,0,'L');
	$pdf->SetFont('times','',10);
	$pdf->Cell(252,5,''.$_POST['ntercero'],0,1,'L');
	$pdf->SetFont('times','B',10);
	$pdf->Cell(28,5,'CEDULA/NIT:',0,0,'L');
	$pdf->SetFont('times','',10);
	$pdf->Cell(252,5,''.$_POST['tercero'],0,1,'L');
	$pdf->SetFont('times','B',10);
	$pdf->Cell(18,5,'OBJETO: ',0,'L');
	$pdf->SetFont('times','',10);
	$pdf->MultiCell(252,5,''.$_POST['objeto'],0,'L',false,1,38,'',true,4,false,true,60,'T',false);
    
	if(isset($_POST['detaller']))
	{
		if(!empty($_POST['detaller']))
		{
			$pdf->SetFont('helvetica','B',10);  //Nuevo
			$pdf->Cell(18,5,'REVERSION:',0,0,'L');	 //Nuevo
			$pdf->SetFont('helvetica','',10); //Nuevo
			$pdf->MultiCell(262,2,''.$_POST['detaller'],0,'L',false,1,'','',true,4,false,true,14,'T',false);
		}
	}
    
    $pdf->ln(2);
	$posy=$pdf->GetY();
	$pdf->line(10,$posy+1,290,$posy);
	$pdf->ln(2);
	
	$pdf->SetFillColor(200, 200, 200);

	$pdf->SetFont('helvetica','B',10);
	//$pdf->Cell(181,5,''.$_POST['beneficiario'],0,0,'L');
	$posy=$pdf->GetY();
	//$pdf->RoundedRect(10,$posy+1, 280, 5, 1.2,'' );
	$pdf->SetFont('helvetica','B',6);
	
	$posy=$pdf->GetY();
	$pdf->SetY($posy+1);
	/* $pdf->Cell(0.1); */
    /* $pdf->Cell(25,5,"VIGENCIA DEL GASTO",1,0,'C'); 
	$pdf->Cell(25,5,"CODIGO",1,0,'C'); 
	$pdf->Cell(52,5,'RUBRO',1,0,'C');
	$pdf->Cell(36,5,'FUENTE',1,0,'C');
	$pdf->Cell(40,5,'PRODUCTO/SERVICIO',1,0,'C');
	$pdf->Cell(36,5,'INDICADOR PRODUCTO',1,0,'C');
    $pdf->Cell(36,5,'POLÍTICA PÚBLICA',1,0,'C');
	$pdf->Cell(30,5,'INGRESOS',1,1,'C'); */
	
	$pdf->Cell(25,5,'Rubro',1,0,'C',true,0,0,false,'T','C');
	$pdf->Cell(30,5,'sec. presu.',1,0,'C',true,0,0,false,'T','C');
	$pdf->Cell(20,5,'Medio pago',1,0,'C',true,0,0,false,'T','C');
	$pdf->Cell(25,5,'Vig gasto',1,0,'C',true,0,0,false,'T','C');
	$pdf->Cell(40,5,'Proyecto',1,0,'C',true,0,0,false,'T','C');
	$pdf->Cell(25,5,'Programatico',1,0,'C',true,0,0,false,'T','C');
	$pdf->Cell(30,5,'CCPET',1,0,'C',true,0,0,false,'T','C');
	$pdf->Cell(30,5,'Fuente',1,0,'C',true,0,0,false,'T','C');
	$pdf->Cell(25,5,'CPC',1,0,'C',true,0,0,false,'T','C');
	/*$pdf->Cell(20,5,'Divipola',1,0,'C',true,0,0,false,'T','C');
	$pdf->Cell(20,5,'CHIP',1,0,'C',true,0,0,false,'T','C');*/
	$pdf->Cell(30,5,'Valor',1,1,'C',true,0,0,false,'T','C');
    /* $pdf->SetFont('helvetica','I',9);
    $pdf->ln(0); */
	//$posy=$pdf->GetY()+$_POST[desplaza];
	//$pdf->SetY(5+$posy);   
	$con=0;
	while ($con<count($_POST['dcuenta']))
	{	
		/* $altura=6;
		$altini=6;
		$ancini=60;
		$altaux=0;

        
		$colst01=strlen(iconv($_SESSION["VERCARPDFINI"], $_SESSION["VERCARPDFFIN"]."//TRANSLIT",$_POST[nomPoliticap][$con]));
		$colst02=strlen(iconv($_SESSION["VERCARPDFINI"], $_SESSION["VERCARPDFFIN"]."//TRANSLIT",$_POST[dnfuentes][$con]));

		if($colst01>$colst02){$cantidad_lineas= $colst01;}
		else{$cantidad_lineas= $colst02;}
		if($cantidad_lineas > $ancini)
		{
			$cant_espacios = $cantidad_lineas/$ancini;
			$rendondear=ceil($cant_espacios);
			$altaux=$altini*$rendondear;
		}
		if($altaux>$altura){$altura=$altaux;}
		if ($concolor==0){$pdf->SetFillColor(200,200,200);$concolor=1;}
		else {$pdf->SetFillColor(255,255,255);$concolor=0;}
		$pdf->SetFont('times','',8);

        $pdf->MultiCell(25,$altura,iconv($_SESSION["VERCARPDFINI"], $_SESSION["VERCARPDFFIN"]."//TRANSLIT",$_POST[codVigenciag][$con].' - '.$_POST[nomVigenciag][$con]),1,'L',true,0,'','',true,0,false,true,$altura,'M',false);
		$pdf->Cell(25,$altura,"  ".$_POST[dcuentas][$con],1,0,'L',true,0,0,false,'T','C');
        //$pdf->Cell(25,$altura,"  ".$_POST[dfuentes][$con],1,0,'L',true,0,0,false,'T','C');
        $pdf->MultiCell(52,$altura,iconv($_SESSION["VERCARPDFINI"], $_SESSION["VERCARPDFFIN"]."//TRANSLIT",$_POST[dncuentas][$con]),1,'L',true,0,'','',true,0,false,true,$altura,'M',false);
		$pdf->MultiCell(36,$altura,iconv($_SESSION["VERCARPDFINI"], $_SESSION["VERCARPDFFIN"]."//TRANSLIT",$_POST[dfuentes][$con].' - '.$_POST[dnfuentes][$con]),1,'L',true,0,'','',true,0,false,true,$altura,'M',false);
		$pdf->MultiCell(40,$altura,iconv($_SESSION["VERCARPDFINI"], $_SESSION["VERCARPDFFIN"]."//TRANSLIT",$_POST[dServProduct][$con].' - '.$_POST[dnServProduct][$con]),1,'L',true,0,'','',true,0,false,true,$altura,'M',false);
		$pdf->MultiCell(36,$altura,iconv($_SESSION["VERCARPDFINI"], $_SESSION["VERCARPDFFIN"]."//TRANSLIT",$_POST[dindicador][$con].' - '.$_POST[dnindicador][$con]),1,'L',true,0,'','',true,0,false,true,$altura,'M',false);
        $pdf->MultiCell(36,$altura,iconv($_SESSION["VERCARPDFINI"], $_SESSION["VERCARPDFFIN"]."//TRANSLIT",$_POST[codPoliticap][$con].' - '.$_POST[nomPoliticap][$con]),1,'L',true,0,'','',true,0,false,true,$altura,'M',false);
		$pdf->Cell(30,$altura,"$ ".number_format($_POST[dgastos][$con],2,$_SESSION["spdecimal"],$_SESSION["spmillares"])."   ",1,1,'R',true,0,0,false,'T','C');
		$con++; */

        $altura=4;
		$altini=6;
		$ancini=30;
		$altaux=0;
		$colst01=strlen($_POST['dcuenta'][$con]);
		$colst02=strlen($_POST['dfuente'][$con]);
		if($colst01>$colst02){$cantidad_lineas= $colst01;}
		else{$cantidad_lineas= $colst02;}
		if($cantidad_lineas > $ancini)
		{
			$cant_espacios = $cantidad_lineas/$ancini;
			$rendondear=ceil($cant_espacios);
			$altaux=$altini*$rendondear;
		}
		if($altaux>$altura){$altura=$altaux;}
		/* if ($concolor==0){$pdf->SetFillColor(200,200,200);$concolor=1;}
		else {$pdf->SetFillColor(255,255,255);$concolor=0;}*/
		$pdf->SetFont('times','',9);
		/* $this->Cell(20,5,'tipo gasto',1,0,'C',false,'T','C');
			$this->Cell(20,5,'sec. presu.',1,0,'C',false,0,0,false,'T','C');
			$this->Cell(20,5,'Medio pago',1,0,'C',false,0,0,false,'T','C');
			$this->Cell(25,5,'Vig gasto',1,0,'C',false,0,0,false,'T','C');
			$this->Cell(20,5,'Proyecto',1,0,'C',false,0,0,false,'T','C');
			$this->Cell(20,5,'Programatico',1,0,'C',false,0,0,false,'T','C');
            $this->Cell(30,5,'Cuenta',1,0,'C',false,0,0,false,'T','C');
            $this->Cell(30,5,'Fuente',1,0,'C',false,0,0,false,'T','C');
            $this->Cell(20,5,'CPC',1,0,'C',false,0,0,false,'T','C');
            $this->Cell(20,5,'Divipola',1,0,'C',false,0,0,false,'T','C');
            $this->Cell(20,5,'CHIP',1,0,'C',false,0,0,false,'T','C');
			$this->Cell(20,5,'Valor',1,1,'C',false,0,0,false,'T','C'); */

		$v=$pdf->gety();
		if($v>=150){ 
			$pdf->AddPage();
			$pdf->SetFillColor(200, 200, 200);
			$pdf->SetFont('helvetica','B',6);
			$pdf->Cell(25,5,'Rubro',1,0,'C',true,0,0,false,'T','C');
			$pdf->Cell(30,5,'sec. presu.',1,0,'C',true,0,0,false,'T','C');
			$pdf->Cell(20,5,'Medio pago',1,0,'C',true,0,0,false,'T','C');
			$pdf->Cell(25,5,'Vig gasto',1,0,'C',true,0,0,false,'T','C');
			$pdf->Cell(40,5,'Proyecto',1,0,'C',true,0,0,false,'T','C');
			$pdf->Cell(25,5,'Programatico',1,0,'C',true,0,0,false,'T','C');
			$pdf->Cell(30,5,'CCPET',1,0,'C',true,0,0,false,'T','C');
			$pdf->Cell(30,5,'Fuente',1,0,'C',true,0,0,false,'T','C');
			$pdf->Cell(25,5,'CPC',1,0,'C',true,0,0,false,'T','C');
			/*$pdf->Cell(20,5,'Divipola',1,0,'C',true,0,0,false,'T','C');
			$pdf->Cell(20,5,'CHIP',1,0,'C',true,0,0,false,'T','C');*/
			$pdf->Cell(30,5,'Valor',1,1,'C',true,0,0,false,'T','C');
		}
		$rubro = '';
		$partsFuente = explode('-', $_POST['dfuente'][$con]);
		if($_POST['dbpim'][$con] != ''){
			
			$rubro = $_POST['dprogramatico'][$con]."-".$_POST['dbpim'][$con]."-".$partsFuente[0];
		}else{
			$partsCuenta = explode("-", $_POST['dcuenta'][$con]);
			$rubro = $partsCuenta[0]."-".$partsFuente[0];
		}
		$pdf->SetFillColor(250,250,250);
		$pdf->MultiCell(25,$altura,$rubro,1,'C',true,0,'','',true,0,false,true,$altura,'M',false);
		$pdf->MultiCell(30,$altura,strtolower($_POST['dsecPresupuestal'][$con]),1,'C',true,0,'','',true,0,false,true,$altura,'M',false);
        //$pdf->Cell(25,$altura,"  ".$_POST[dfuentes][$con],1,0,'L',true,0,0,false,'T','C');
		$pdf->MultiCell(20,$altura,$_POST['dmedioPago'][$con],1,'C',true,0,'','',true,0,false,true,$altura,'M',false);
		$pdf->MultiCell(25,$altura,$_POST['dvigGasto'][$con],1,'C',true,0,'','',true,0,false,true,$altura,'M',false);
		$pdf->SetFont('times','',8);
		$pdf->MultiCell(40,$altura,$_POST['dbpim'][$con].' - '.strtolower($_POST['nombreProyecto'][$con]),1,'C',true,0,'','',true,0,false,true,$altura,'M',false);
		$pdf->SetFont('times','',9);
		$pdf->MultiCell(25,$altura,$_POST['dprogramatico'][$con].' - '.$_POST['indpro'][$con],1,'C',true,0,'','',true,0,false,true,$altura,'M',false);
		$pdf->MultiCell(30,$altura,$_POST['dcuenta'][$con],1,'C',true,0,'','',true,0,false,true,$altura,'M',false);
		$pdf->MultiCell(30,$altura,$_POST['dfuente'][$con],1,'C',true,0,'','',true,0,false,true,$altura,'M',false);
		$pdf->MultiCell(25,$altura,$_POST['dproductoservicio'][$con]. ' - '. $_POST['nomCpc'][$con],1,'C',true,0,'','',true,0,false,true,$altura,'M',false);
		/*$pdf->MultiCell(20,$altura,$_POST['ddivipola'][$con],1,'C',true,0,'','',true,0,false,true,$altura,'M',false);
		$pdf->MultiCell(20,$altura,$_POST['dchip'][$con],1,'C',true,0,'','',true,0,false,true,$altura,'M',false);*/	
        //$pdf->MultiCell(50,$altura,iconv($_SESSION["VERCARPDFINI"], $_SESSION["VERCARPDFFIN"]."//TRANSLIT",$_POST[dServProduct][$con].' - '.$_POST[dnServProduct][$con]),1,'L',true,0,'','',true,0,false,true,$altura,'M',false);
		$pdf->Cell(30,$altura,"$ ".number_format($_POST['dvalor'][$con],2,$_SESSION["spdecimal"],$_SESSION["spmillares"])."   ",1,1,'C',true,0,0,false,'T','C');
		$con++;

	}	
	$pdf->SetFont('helvetica','B',10);
	$pdf->SetFillColor(200, 200, 200);
	$pdf->ln(2);
	$pdf->SetLineWidth(0.5);
	$pdf->cell(186.8,5,'','T',0,'R');
	$pdf->cell(57,5,'Total','T',0,0,'R');
	$pdf->cell(35,5,'$'.number_format($_POST['cuentagas2'],2,$_SESSION["spdecimal"],$_SESSION["spmillares"]),'T',0,'R');
	$pdf->SetLineWidth(0.2);
	$pdf->ln(10);

	$v=$pdf->gety();
	
		
	
	$pdf->RoundedRect(8, $v-1, 282, 8, 1.2,'' );
	$pdf->MultiCell(199,4,'SON: '.$_POST['letras'],0,'L');
    $pdf->ln(8);
	for($x=0;$x<count($_POST['ppto']);$x++)
	{
		/* $pdf->ln(6);
		$v=$pdf->gety();
		if($v>=180){
			$pdf->AddPage();
			$pdf->ln(15);
			$v=$pdf->gety();
		} */
		$pdf->setFont('times','B',8);
		$pdf->ln(6);
		$v=$pdf->gety();
		if($v>=180)
		{
			$pdf->AddPage();
			$pdf->ln(15);
			$v=$pdf->gety();
		}
		$pdf->setFont('times','B',7);
		
		if (($x%2)==0) 
		{
			if(isset($_POST['ppto'][$x+1]))
			{
				$pdf->Line(40,$v,130,$v);
				$pdf->Line(170,$v,260,$v);
				$v2=$pdf->gety();
				$pdf->Cell(150,4,''.$_POST['ppto'][$x],0,1,'C',false,0,0,false,'T','C');
				$pdf->Cell(150,4,''.$_POST['nomcargo'][$x],0,1,'C',false,0,0,false,'T','C');
				$pdf->SetY($v2);
				$pdf->Cell(410,4,''.$_POST['ppto'][$x+1],0,1,'C',false,0,0,false,'T','C');
				$pdf->Cell(410,4,''.$_POST['nomcargo'][$x+1],0,1,'C',false,0,0,false,'T','C');
				
			}
			else
			{
				$v = $v + 5;
				$pdf->ln(5);
				
				$pdf->Line(105,$v,195,$v);
				$pdf->Cell(280,4,''.$_POST['ppto'][$x],0,1,'C',false,0,0,false,'T','C');
				$pdf->Cell(280,4,''.$_POST['nomcargo'][$x],0,0,'C',false,0,0,false,'T','C');
			}
			$v3=$pdf->gety();
		}
		$pdf->SetY($v3);
		$pdf->SetFont('helvetica','',7);
	}
	
$pdf->Output('reporterp.pdf', 'I');
?>
