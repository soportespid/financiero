<?php
	header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=utf8");
	require 'comun.inc';
	require 'funciones.inc';
	session_start();
	$linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");
	cargarcodigopag($_GET['codpag'],$_SESSION["nivel"]);
	date_default_timezone_set("America/Bogota");
	titlepag();
?>
<!DOCTYPE >
<html lang=es>
	<head>
		<meta charset="utf-8"/>
		<meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1"/>
		<meta name="viewport" content="user-scalable=no">
		<title>:: IDEAL 10 - Servicios Públicos</title>
		<link href="css/css2z.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
		<link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
		<link href="css/css4.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
		<link href="css/tabs.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css"/>
		<link href="css/style.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css"/>
		<script src="vue/vue.min.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script src="css/programas.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script src="css/calendario.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<style>
			input[type=number]::-webkit-inner-spin-button,
			input[type=number]::-webkit-outer-spin-button { 
				-webkit-appearance: none; 
				margin: 0; 
			}
			input[type=number]{
				text-align:right;
				box-sizing: border-box;
				font: 120% sans-serif;
				width: 100% !important;
			}
			[v-cloak]{display : none;}
		</style>
		<script src="sweetalert2/dist/sweetalert2.min.js"></script>
		<link rel="stylesheet" href="sweetalert2/dist/sweetalert2.min.css">
	</head>
	<body>
		<header>
			<table>
				<tr><script>barra_imagenes("serv");</script><?php cuadro_titulos();?></tr>
			</table>
		</header>
		<form name="form2" method="post" action="">
			<section id="myapp" v-cloak >
				<nav>
					<table>
						<tr><?php menu_desplegable("serv");?></tr>
					</table>

					<div class="bg-white group-btn p-1" id="newNavStyle">
						<button type="button" class="btn btn-white btn-primary-hover d-flex justify-between align-items-center" @click="window.location.href='serv-crearBarrio.php'">
							<span>Nuevo</span>
							<svg viewBox="0 -960 960 960" ><path d="M440-440H200v-80h240v-240h80v240h240v80H520v240h-80v-240Z"/></svg>
						</button>
						<button type="button" class="btn btn-white btn-primary-hover d-flex justify-between align-items-center" @click="window.location.reload()">
							<span>Buscar</span>
							<svg class="fill-black group-hover:fill-white w-5 h-5" viewBox="0 -960 960 960" ><path d="M784-120 532-372q-30 24-69 38t-83 14q-109 0-184.5-75.5T120-580q0-109 75.5-184.5T380-840q109 0 184.5 75.5T640-580q0 44-14 83t-38 69l252 252-56 56ZM380-400q75 0 127.5-52.5T560-580q0-75-52.5-127.5T380-760q-75 0-127.5 52.5T200-580q0 75 52.5 127.5T380-400Z"/></svg>
						</button>
						<button type="button" class="btn btn-white btn-primary-hover d-flex justify-between align-items-center" @click="mypop=window.open('serv-principal.php','',''); mypop.focus();">
							<span>Nueva ventana</span>
							<svg class="fill-black group-hover:fill-white w-5 h-5" viewBox="0 -960 960 960" ><path d="M320-120v-80h80v-80H160q-33 0-56.5-23.5T80-360v-400q0-33 23.5-56.5T160-840h640q33 0 56.5 23.5T880-760v400q0 33-23.5 56.5T800-280H560v80h80v80H320ZM160-360h640v-400H160v400Zm0 0v-400 400Z"/></svg>
						</button>
					</div>
				</nav>
				<article>
					<table class="inicio ancho">
						<tr>
							<td class="titulos" colspan="6" >Buscar Barrios</td>
							<td class="cerrar" style="width:7%" onClick="location.href='serv-principal.php'">Cerrar</td>
						</tr>

                        <tr>
                            <td class="tamano01" style="width: 10%;">Barrio:</td>
                            <td>
                                <input type="text" v-model="buscarBarrio" style="width: 98%;" autocomplete="off">
                            </td>
                            
                            <td style="padding-bottom:0px">
                                <em class="botonflechaverde" v-on:Click="filtrarBarrios">Buscar</em>
                            </td>
                        </tr>
					</table>

					<div class='subpantalla' style='height:66vh; width:99.2%; margin-top:0px; overflow:hidden'>
						<table class='tablamv'>
							<thead>
								<tr style="text-align:Center;">
									<th class="titulosnew00" style="width:6%;">Código barrio</th>
									<th class="titulosnew00">Nombre barrio</th>
									<th class="titulosnew00" style="width:15%;">Departamento</th>
                                    <th class="titulosnew00" style="width:15%;">Municipio</th>
                                    <th class="titulosnew00" style="width:15%;">Centro Poblado</th>
									<th class="titulosnew00" style="width:15%;">Ubicación</th>
									<th class="titulosnew00" style="width:8%;">Eliminar</th>
								</tr>
							</thead>
                            
							<tbody>
								<tr v-for="(barrio, index) in barrios" v-on:dblclick="seleccionaBarrio(barrio)" v-bind:class="index % 2 ? 'saludo1a' : 'saludo2'" style='text-rendering: optimizeLegibility; cursor: pointer !important;'>
                                    <td style="width:6%; font: 120% sans-serif; padding-left:5px; text-align:center;">{{ barrio[0] }}</td>
                                    <td style="font: 120% sans-serif; padding-left:5px;">{{ barrio[1] }}</td>
                                    <td style="width:15%; font: 120% sans-serif; padding-left:5px; text-align:center;">{{ barrio[2] }}</td>
                                    <td style="width:15%; font: 120% sans-serif; padding-left:5px; text-align:center;">{{ barrio[3] }}</td>
                                    <td style="width:15%; font: 120% sans-serif; padding-left:5px; text-align:center;">{{ barrio[4] }}</td>
									<td style="width:15%; font: 120% sans-serif; padding-left:5px; text-align:center;">{{ barrio[5] }}</td>
									<td style="width:8%; text-align: center;">
										<a v-on:click="eliminarBarrio(barrio)" style='cursor:pointer;'>
											<img src="imagenes/newdelete.png" style="height:30px;" title="Eliminar">
										</a>
									</td>
								</tr>
							</tbody>
						</table>
					</div>

					<div id="cargando" v-if="loading" class="loading">
						<span>Cargando...</span>
					</div>

				</article>
			</section>
		</form>
		
		<script src="Librerias/vue/vue.min.js"></script>
		<script src="Librerias/vue/axios.min.js"></script>
		<script src="servicios_publicos/barrios/crear_buscar/serv-barrios.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
        
	</body>
</html>