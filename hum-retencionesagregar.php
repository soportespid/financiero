<?php
	header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=utf8");
	require 'comun.inc';
	require 'funciones.inc';
	require 'conversor.php';
	sesion();
	$linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");
	cargarcodigopag($_GET['codpag'],$_SESSION["nivel"]);
	date_default_timezone_set("America/Bogota");
?>
<!DOCTYPE >
<html lang=es>
	<head>
		<meta charset="utf-8"/>
		<meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1"/>
		<meta name="viewport" content="user-scalable=no">
		<title>:: IDEAL 10 - Gesti&oacute;n humana</title>
		<link href="css/css2z.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
		<link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
		<link href="css/css4.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
		<link href="favicon.ico" rel="shortcut icon" />
		<link rel="stylesheet" href="sweetalert2/dist/sweetalert2.min.css">
		<script src="sweetalert2/dist/sweetalert2.min.js"></script>
		<script type="text/javascript" src="css/programas.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script type="text/javascript" src="css/calendario.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script>
			function guardar(){
				valord = document.getElementsByName('tfecha[]');
				if(valord.length > 0){
					Swal.fire({
						icon: 'question',
						title: '¿Seguro que quieres guardar la retención?',
						showDenyButton: true,
						confirmButtonText: 'Guardar',
						confirmButtonColor: '#01CC42',
						denyButtonText: 'Cancelar',
						denyButtonColor: '#FF121A',
					}).then(
						(result) => {
							if (result.isConfirmed){
								document.form2.oculto.value = "2";
								document.form2.submit();
							}
							else if (result.isDenied){
								Swal.fire({
									icon: 'info',
									title: 'No se guardo la retención',
									confirmButtonText: 'Continuar',
									confirmButtonColor: '#FF121A',
									timer: 2500
								});
							}
						}
					)
				}else{
					Swal.fire({
						icon: 'error',
						title: 'Error!',
						text: 'Falta asignar retenciones a un funcionario', 
						confirmButtonText: 'Continuar',
						confirmButtonColor: '#FF121A',
						timer: 3500
					});
				}
			}
			function validar(formulario){document.form2.submit();}
			function buscater(e){
				if (document.form2.tercero.value != ""){
					document.form2.bt.value = '1';
					document.form2.submit();
				}
			}
			function despliegamodalm(_valor,_tip,mensa,pregunta){
				document.getElementById("bgventanamodalm").style.visibility = _valor;
				if(_valor == "hidden"){
					document.getElementById('ventanam').src = "";
				}else{
					switch(_tip){
						case "1":
							document.getElementById('ventanam').src = "ventana-mensaje1.php?titulos=" + mensa; break;
						case "2":
							document.getElementById('ventanam').src = "ventana-mensaje3.php?titulos=" + mensa; break;
						case "3":
							document.getElementById('ventanam').src = "ventana-mensaje2.php?titulos=" + mensa; break;
						case "4":
							document.getElementById('ventanam').src = "ventana-consulta1.php?titulos=" + mensa + "&idresp=" + pregunta; break;
					}
				}
			}
			function funcionmensaje(){document.location.href = "hum-retencionesbuscar.php";}
			function respuestaconsulta(pregunta){
				switch(pregunta){
					case "1":
						document.form2.oculto.value = "2";
						document.form2.submit();
						break;
					case "2": 
						document.form2.oculto.value = "3";
						document.form2.submit();
						break;
				}
			}
			function despliegamodal2(_valor,_num){
				document.getElementById("bgventanamodal2").style.visibility = _valor;
				if(_valor == "hidden"){
					document.getElementById('ventana2').src = "";
				}else{
					document.getElementById('ventana2').src = "ventanacargafuncionariosretenciones.php?objeto=tercero";
				}
			}
			function fagregar(){
				var vvigencia = document.form2.vigencia.value;
				if(document.form2.tiporete.value != "-1"){
					if(document.form2.ntercero.value != ""){
						if(document.form2.valrete.value != ""){
							if(vvigencia.length >= "4"){
								if(document.form2.variablepago.value != "-1"){
									document.form2.oculto.value = "4";
									document.form2.submit();
								}else{
									Swal.fire({
										icon: 'error',
										title: 'Error!',
										text: 'Falta aignar el tipo de pago', 
										confirmButtonText: 'Continuar',
										confirmButtonColor: '#FF121A',
										timer: 3500
									});
								}	
							}else{
								Swal.fire({
									icon: 'error',
									title: 'Error!',
									text: 'Falta asignar la vigencia', 
									confirmButtonText: 'Continuar',
									confirmButtonColor: '#FF121A',
									timer: 3500
								});
							}
						}else{
							Swal.fire({
								icon: 'error',
								title: 'Error!',
								text: 'Falta asignar el valor de la retención', 
								confirmButtonText: 'Continuar',
								confirmButtonColor: '#FF121A',
								timer: 3500
							});
						}
					}else{
						Swal.fire({
							icon: 'error',
							title: 'Error!',
							text: 'Falta asignar un funcionario', 
							confirmButtonText: 'Continuar',
							confirmButtonColor: '#FF121A',
							timer: 3500
						});
					}
				}else{
					Swal.fire({
							icon: 'error',
							title: 'Error!',
							text: 'Falta seleccionar el tipo de retención', 
							confirmButtonText: 'Continuar',
							confirmButtonColor: '#FF121A',
							timer: 3500
						});
				}
			}
			function eliminar(variable){
				Swal.fire({
					icon: 'question',
					title: '¿Esta seguro de eliminar la retención?',
					showDenyButton: true,
					confirmButtonText: 'Eliminar',
					confirmButtonColor: '#01CC42',
					denyButtonText: 'Cancelar',
					denyButtonColor: '#FF121A',
				}).then(
					(result) => {
						if (result.isConfirmed){
							document.getElementById('elimina').value = variable;
							document.form2.oculto.value = "3";
							document.form2.submit();
						}
					}
				)
			}
		</script>
	</head>
	<body>
		<IFRAME src="alertas.php" name="alertas" id="alertas" style="display:none"></IFRAME>
		<span id="todastablas2"></span>
		<table>
			<tr><script>barra_imagenes("hum");</script><?php cuadro_titulos();?></tr> 
			<tr><?php menu_desplegable("hum");?></tr>
			<tr>
				<td colspan="3" class="cinta">
					<img src="imagenes/add.png" title="Nuevo" onClick="location.href='hum-retencionesagregar.php'" class="mgbt">
					<img src="imagenes/guarda.png" title="Guardar" onClick="guardar()" class="mgbt">
					<img src="imagenes/busca.png" title="Buscar" onClick="location.href='hum-retencionesbuscar.php'" class="mgbt">
					<img src="imagenes/agenda1.png" title="Agenda" onClick="mypop=window.open('plan-agenda.php','','');mypop.focus()" class="mgbt">
					<img src="imagenes/nv.png" title="Nueva ventana" class="mgbt" onClick="mypop=window.open('hum-principal.php','','');mypop.focus();">
					<img src="imagenes/duplicar_pantalla.png" title="Duplicar pesta&ntilde;a" onClick="mypop=window.open('<?php echo $url2; ?>','','');mypop.focus();" class="tooltip bottom mgbt">
					<img src='imagenes/iratras.png' title="Atr&aacute;s" onClick="location.href='hum-menunomina.php'" class="mgbt">
				</td>
			</tr>
		</table>
		<div id="bgventanamodalm" class="bgventanamodalm">
			<div id="ventanamodalm" class="ventanamodalm">
				<IFRAME src="" name="ventanam" marginWidth=0 marginHeight=0 frameBorder=0 id="ventanam" frameSpacing=0 style=" width:700px; height:130px; top:200; overflow:hidden;"></IFRAME>
			</div>
		</div>
		<form name="form2" method="post" action="">
			<?php 
				if($_POST['oculto'] == ''){
					$_POST['fecha'] = date("d/m/Y");
					$_POST['periodo'] = date("m");
					
				}
				preg_match("/([0-9]{2})\/([0-9]{2})\/([0-9]{4})/",$_POST['fecha'],$fecha);
				$_POST['vigencia'] = $fecha[3];
				if($_POST['bt'] == '1'){
					$nresul = buscatercero($_POST['tercero']);
					if($nresul != ''){
						$_POST['ntercero'] = $nresul;
						$sqlr = "
						SELECT codfun, GROUP_CONCAT(descripcion ORDER BY CONVERT(valor, SIGNED INTEGER) SEPARATOR '<->')
						FROM hum_funcionarios
						WHERE (item = 'VALESCALA') AND codfun=(SELECT codfun FROM hum_funcionarios WHERE descripcion='".$_POST['tercero']."' AND estado='S') AND estado='S'
						GROUP BY codfun";
						$resp2 = mysqli_query($linkbd,$sqlr);
						$row2 = mysqli_fetch_row($resp2);
						$_POST['salbas'] = $row2[1];
					}else{
						$_POST['ntercero'] = "";
					}
				}
			?>
			<table class="inicio ancho" style="width:99.7%;">
				<tr>
					<td class="titulos" colspan="10">:: Retenciones funcionarios</td>
					<td class="cerrar" style="width:7%" onClick="location.href='hum-principal.php'">Cerrar</td>
				</tr>
				<tr>
					<td class="tamano01" style="width:4cm;">Fecha registro:</td>
					<td style="width:12%"><input type="text" name="fecha" id="fc_1198971545" title="DD/MM/YYYY" value="<?php echo $_POST['fecha']; ?>" onKeyUp="return tabular(event,this)" onKeyDown="mascara(this,'/',patron,true)"  maxlength="10" style="width:100%;" class="colordobleclik" onDblClick="displayCalendarFor('fc_1198971545');" autocomplete="off" onChange=""></td>
					<td class="tamano01" style="width:3.5cm;">Tipo Retenci&oacute;n:</td>
					<td colspan="4">
						<select name="tiporete" id="tiporete" style="width:100%;" >
							<option value="-1">Seleccione ....</option>
							<?php 
								$sqlr="SELECT id, codigo, nombre FROM tesoretenciones WHERE estado = 'S' AND nomina = '1' ORDER BY codigo";
								$resp = mysqli_query($linkbd,$sqlr);
								while ($row = mysqli_fetch_row($resp)){
									if($_POST['tiporete'] == $row[0]){
										echo "<option value='$row[0]' SELECTED>$row[1] - $row[2]</option>";
										$_POST['ntiporete'] = $row[1];	
									}else {echo "<option value='$row[0]'>$row[1] - $row[2]</option>";}
								}
							?>
						</select>
						<input type="hidden" name="ntiporete" id="ntiporete" value="<?php echo $_POST['ntiporete']?>" />
					</td>
				</tr>
				<tr>
					<td class="tamano01" >Vigencia:</td>
					<td><input type="text" name="vigencia" id="vigencia" value="<?php echo $_POST['vigencia']?>" onKeyUp="return tabular(event,this)" readonly></td>
					<td class="tamano01">Tipo de Pago:</td>
					<td colspan="4">
						<select name="variablepago" id="variablepago" class="tamano02" style="width:100%;">
							<option value="-1">Seleccione ....</option>
							<?php
								$sqlr="SELECT codigo,nombre FROM ccpethumvariables WHERE estado='S'";
								$resp = mysqli_query($linkbd,$sqlr);
								while ($row =mysqli_fetch_row($resp)){
									if(in_array($row[0], $vtiponum)){$vartip = "S";}
									else{$vartip = "N";}
									if($_POST['variablepago'] == $row[0]){
										if($vartip == "N"){echo "<option value='$row[0]' SELECTED>$row[0] - $row[1]</option>";}
									}else{
										if($vartip == "N"){echo "<option value='$row[0]' >$row[0] - $row[1]</option>";}
									}
								}
							?>
						</select>
					</td>
				</tr>
				<tr>
					<td class="tamano01">Funcionario:</td>
					<td><input type="text" id="tercero" name="tercero" onKeyPress="javascript:return solonumeros(event)" onKeyUp="return tabular(event,this)" onChange="buscater(event)" value="<?php echo $_POST['tercero'];?>" style="width:100%;" class="colordobleclik" autocomplete="off" onDblClick="despliegamodal2('visible');"></td>
					<td colspan="5"><input type="text" name="ntercero" id="ntercero" value="<?php echo $_POST['ntercero'];?>" style="width: 100%;" readonly/></td> 
				</tr>
				<tr>
					<td class="tamano01">Salario B&aacute;sico:</td>
					<td><input type="text" name="salbas" id="salbas" value="<?php echo $_POST['salbas'];?>" style="width:100%;" readonly/></td>
					<td class="tamano01">Valor Retenci&oacute;n:</td>
					<td style="width:12%;"><input type="text" name="valrete" id="valrete" value="<?php echo $_POST['valrete']?>" style="width:100%;" onKeyPress="javascript:return solonumeros(event)" onKeyUp="return tabular(event,this)"/></td>
					<td class="tamano01" style="width:2.5cm;">Mes:</td>
					<td style="width:12%;">
						<select name="periodo" id="periodo" style="width:100%">
							<option value="-1">Seleccione ....</option>
							<?php
								$sqlr="SELECT id, nombre FROM meses ORDER BY id";
								$resp = mysqli_query($linkbd,$sqlr);
								while ($row = mysqli_fetch_row($resp)){
									if($_POST['periodo'] == $row[0]){
										echo "<option value='$row[0]' SELECTED>$row[1]</option>";
										$_POST['nmes'] = $row[1];
									}
									else{
										echo "<option value='$row[0]'>$row[1]</option>";
									}
								}
							?>
						</select>
						<input type="hidden" name="nmes" id="nmes" value="<?php echo $_POST['nmes']?>">
					</td>
					<td colspan="2" >&nbsp;&nbsp;<em class="botonflechaverde" onClick="fagregar();">Agregar</em></td>
				</tr>
			</table>
			<div class="subpantalla" style="height:47%; width:99.5%;overflow-x:hidden;">
				<table class="inicio">
					<tr><td class="titulos" colspan="10">Retenciones Asignadas a Funcionarios</td></tr>
					<tr>
						<td class="titulos2" style="width:5%;">Item</td>
						<td class="titulos2" style="width:5%;">T.R.</td>
						<td class="titulos2" style="width:5%;">T.P.</td>
						<td class="titulos2" style="width:10%;">Documento</td>
						<td class="titulos2">Nombre funcionario</td>
						<td class="titulos2" style="width:10%;">Salario b&aacute;sico</td>
						<td class="titulos2" style="width:10%;">Valor retenci&oacute;n</td>
						<td class="titulos2" style="width:10%;">Mes</td>
						<td class="titulos2" style="width:5%;">Vigencia</td>
						<td class="titulos2" style="width:3%;text-align:center;"><img src="imagenes/del.png"/></td>
					</tr>
				<?php
					if ($_POST['oculto'] == '3'){
						$posi=$_POST['elimina'];
						unset($_POST['tfecha'][$posi]);
						unset($_POST['ttreste'][$posi]);
						unset($_POST['tntreste'][$posi]);
						unset($_POST['ttercero'][$posi]);
						unset($_POST['tntercero'][$posi]);
						unset($_POST['tsalbas'][$posi]);
						unset($_POST['tvalrete'][$posi]);
						unset($_POST['tperiodo'][$posi]);
						unset($_POST['tnmes'][$posi]);
						unset($_POST['tvigencia'][$posi]);
						unset($_POST['tvpago'][$posi]);
						$_POST['tfecha'] = array_values($_POST['tfecha']); 
						$_POST['ttreste'] = array_values($_POST['ttreste']);
						$_POST['tntreste'] = array_values($_POST['tntreste']);
						$_POST['ttercero'] = array_values($_POST['ttercero']);
						$_POST['tntercero'] = array_values($_POST['tntercero']);
						$_POST['tsalbas'] = array_values($_POST['tsalbas']);
						$_POST['tvalrete'] = array_values($_POST['tvalrete']);
						$_POST['tperiodo'] = array_values($_POST['tperiodo']);
						$_POST['tnmes'] = array_values($_POST['tnmes']);
						$_POST['tvigencia'] = array_values($_POST['tvigencia']);
						$_POST['tvpago'] = array_values($_POST['tvpago']);
						$_POST['elimina'] = '';
					}
					if ($_POST['oculto'] == '4'){
						$_POST['tfecha'][] = $_POST['fecha'];
						$_POST['ttreste'][] = $_POST['tiporete'];
						$_POST['tntreste'][] = $_POST['ntiporete'];
						$_POST['ttercero'][] = $_POST['tercero'];
						$_POST['tntercero'][] = $_POST['ntercero'];
						$_POST['tsalbas'][] = $_POST['salbas'];
						$_POST['tvalrete'][] = $_POST['valrete'];
						$_POST['tperiodo'][] = $_POST['periodo'];
						$_POST['tnmes'][] = $_POST['nmes'];
						$_POST['tvigencia'][] = $_POST['vigencia'];
						$_POST['tvpago'][] = $_POST['variablepago'];
						echo"
						<script>
							document.form2.tercero.value='';
							document.form2.ntercero.value='';
							document.form2.salbas.value='';
							document.form2.valrete.value='';
							document.form2.tercero.select();
						</script>";
						
					}
					$co = "saludo1a";
					$co2 = "saludo2";
					for ($x = 0; $x < count($_POST['tfecha']); $x++){
						echo "
						<input type='hidden' name='tfecha[]' value='".$_POST['tfecha'][$x]."'/>
						<input type='hidden' name='tvpago[]' value='".$_POST['tvpago'][$x]."'/>
						<input type='hidden' name='ttreste[]' value='".$_POST['ttreste'][$x]."'/>
						<input type='hidden' name='tntreste[]' value='".$_POST['tntreste'][$x]."'/>
						<input type='hidden' name='ttercero[]' value='".$_POST['ttercero'][$x]."'/>
						<input type='hidden' name='tntercero[]' value='".$_POST['tntercero'][$x]."'/>
						<input type='hidden' name='tsalbas[]' value='".$_POST['tsalbas'][$x]."'/>
						<input type='hidden' name='tvalrete[]' value='".$_POST['tvalrete'][$x]."'/>
						<input type='hidden' name='tperiodo[]' value='".$_POST['tperiodo'][$x]."'/>
						<input type='hidden' name='tnmes[]' value='".$_POST['tnmes'][$x]."'/>
						<input type='hidden' name='tvigencia[]' value='".$_POST['tvigencia'][$x]."'/>
						<tr class='$co'>
							<td class='icoop' style='text-align:right;' >".($x+1)."&nbsp;</td>
							<td class='icoop' style='text-align:right;'>".$_POST['tntreste'][$x]."&nbsp;</td>
							<td class='icoop' style='text-align:right;'>".$_POST['tvpago'][$x]."&nbsp;</td>
							<td class='icoop' style='text-align:right;'>".number_format($_POST['ttercero'][$x],0)."&nbsp;</td>
							<td class='icoop' >".$_POST['tntercero'][$x]."&nbsp;</td>
							<td class='icoop' style='text-align:right;'>$ ".number_format($_POST['tsalbas'][$x],0)."&nbsp;</td>
							<td class='icoop' style='text-align:right;'>$ ".number_format($_POST['tvalrete'][$x],0)."&nbsp;</td>
							<td class='icoop' >".$_POST['tnmes'][$x]."&nbsp;</td>
							<td class='icoop' style='text-align:right;'>".$_POST['tvigencia'][$x]."&nbsp;</td>
							<td class='icoop' style='text-align:center;'><img src='imagenes/del.png' onclick='eliminar($x)'></td>
						</tr>";
						$aux = $co;
						$co = $co2;
						$co2 = $aux;
					}
				?>
				</table>
			</div>
			<?php
				if($_POST['oculto'] == "2"){
					$conval=0;
					for ($x = 0; $x < count($_POST['tfecha']); $x++){
						$sqlr = "SELECT MAX(id) FROM hum_retencionesfun";
						$res = mysqli_query($linkbd,$sqlr);
						$row = mysqli_fetch_row($res);
						$numid = $row[0] + 1;
						preg_match("/([0-9]{2})\/([0-9]{2})\/([0-9]{4})/",$_POST['tfecha'][$x],$fecha);
						$fechaf = "$fecha[3]-$fecha[2]-$fecha[1]";
						$sqlr="INSERT INTO hum_retencionesfun(id, fecha, tiporetencion, docfuncionario, nomfuncionario, salbasico, valorretencion, mes, vigencia, estadopago, estado, tipopago) VALUES ('$numid ', '$fechaf', '". $_POST['ttreste'][$x]."', '".$_POST['ttercero'][$x]."', '".$_POST['tntercero'][$x]."', '".$_POST['tsalbas'][$x]."', '".$_POST['tvalrete'][$x]."', '".$_POST['tperiodo'][$x]."', '".$_POST['tvigencia'][$x]."', 'N', 'S', '".$_POST['tvpago'][$x]."')";
						if(!mysqli_query($linkbd,$sqlr)){$conval++;}
					}
					if($conval > 0){
						echo"
						<script>
							Swal.fire({
								icon: 'error',
								title: 'Error!',
								text: 'Error al Almacenar Retenciones', 
								confirmButtonText: 'Continuar',
								confirmButtonColor: '#FF121A',
								timer: 3500
							});
						</script>";
					}else{
						echo "
						<script>
							Swal.fire({
								icon: 'success',
								title: 'Se Almacenaron todas las Retenciones Exitosamente',
								showConfirmButton: true,
								confirmButtonText: 'Continuar',
								confirmButtonColor: '#01CC42',
								timer: 3500
							}).then((response) => {
								document.location.href = 'hum-retencionesbuscar.php';
							});
						</script>";
					}
				}
			?>
			<input type="hidden" name="bt" id="bt" value="0"/>
			<input type='hidden' name='elimina' id='elimina'/>
			<input type="hidden" name="oculto" id="oculto" value="1"/>
		</form>
		<div id="bgventanamodal2">
			<div id="ventanamodal2">
				<IFRAME  src="" name="buster" marginWidth=0 marginHeight=0 frameBorder=0 id="ventana2" frameSpacing=0 style="left:500px; width:900px; height:500px; top:200;"></IFRAME>
			</div>
		</div>
	</body>
</html>
