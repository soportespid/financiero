<?php 
	header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=utf8");
	require "comun.inc";
	require "funciones.inc";
	$linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");
	date_default_timezone_set("America/Bogota");
?>
<!DOCTYPE >
<html lang="es">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=9">
		<title>:: Spid - Gestion Humana</title>
		<link href="css/css2.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<link href="css/css4.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<script type="text/javascript" src="css/programas.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script>
			var anterior;
			function ponprefijo(codproy,nomproy){
				parent.document.form2.idfuente.value = codproy;
				parent.document.form2.nomfuente.value = codproy+" - "+nomproy;
				parent.despliegamodal2("hidden");
			}
		</script> 
		<?php titlepag();?>
	</head>
	<body>
		<form action="" method="post" name="form2">
			<?php 
				if($_POST['oculto'] == ""){
					$_POST['numpos'] = 0;
					$_POST['numres'] = 10;
					$_POST['nummul'] = 0;
					$_POST['idproyecto'] = $_GET['idproy'];
					$_POST['idprogramatico'] = $_GET['idprogr'];
					$_POST['vigencia'] = date('Y');
				}
			?>
			<table class="inicio ancho">
				<tr>
					<td class="titulos" colspan="5">:: FUENTES</td>
					<td class="cerrar" style="width:7%" onClick="parent.despliegamodal2('hidden');">Cerrar</td>
				</tr>
			</table>
			<input type="hidden" name="oculto" id="oculto" value="1">
			<input type="hidden" name="idproyecto'" id="idproyecto'" value="<?php echo $_POST['idproyecto'];?>">
			<input type="hidden" name="idproyecto'" id="idproyecto'" value="<?php echo $_POST['idprogramatico'];?>">
			<input type="hidden" name="numres" id="numres" value="<?php echo @ $_POST['numres'];?>"/>
			<input type="hidden" name="numpos" id="numpos" value="<?php echo @ $_POST['numpos'];?>"/>
			<input type="hidden" name="nummul" id="nummul" value="<?php echo @ $_POST['nummul'];?>"/>
			<div class="subpantalla" style="height:84%; width:99.6%; overflow-x:hidden;">
				<?php
					$sqlr="
					SELECT T1.id_fuente, T2.nombre
					FROM ccpproyectospresupuesto_presupuesto AS T1
					INNER JOIN ccpet_fuentes_cuipo AS T2
					ON T1.id_fuente = T2.codigo_fuente
					WHERE T1.codproyecto = '".$_POST['idproyecto']."' AND T1.indicador_producto = '".$_POST['idprogramatico']."' ORDER BY T1.id DESC";
					$resp = mysqli_query($linkbd,$sqlr);
					$con=1;
					echo "
					<table class='inicio' align='center' width='99%'>
					<tr>
						<td class='titulos2' width='5%'>Item</td>
						<td class='titulos2'>Nombre</td>
						<td class='titulos2' width='8%'>Origen</td>
					</tr>";	
					$iter='saludo1a';
					$iter2='saludo2';
					while ($row = mysqli_fetch_row($resp))
					{
						$ncc=$row[1];
						$con2=$con+ $_POST['numpos'];
						echo" 
						<tr class='$iter' onClick=\"javascript:ponprefijo('$row[0]','$row[1]')\" >
							<td>$con2</td>
							<td style='text-transform:uppercase'>$row[0] - $row[1]</td>
							<td>Inicial</td>
						</tr>";
						$con+=1;
						$aux=$iter;
						$iter=$iter2;
						$iter2=$aux;
					}
					$sqlr="
					SELECT T1.codigo_fuente, T1.nombre 
					FROM ccpet_fuentes_cuipo AS T1 
					INNER JOIN ccpet_adiciones AS T2 
					ON T2.fuente = T1.codigo_fuente 
					INNER JOIN ccpproyectospresupuesto AS T3 
					ON T2.bpim = T3.codigo 
					WHERE T3.id = '".$_POST['idproyecto']."' AND T2.programatico = '".$_POST['idprogramatico']."'";
					$resp = mysqli_query($linkbd,$sqlr);
					while ($row = mysqli_fetch_row($resp))
					{
						$ncc=$row[1];
						$con2=$con+ $_POST['numpos'];
						echo" 
						<tr class='$iter'  onClick=\"javascript:ponprefijo('$row[0]','$row[1]')\" >
							<td>$con2</td>
							<td style='text-transform:uppercase'>$row[0] - $row[1]</td>
							<td>Adiciones</td>
						</tr>";
						$con+=1;
						$aux=$iter;
						$iter=$iter2;
						$iter2=$aux;
					}


					echo"
						</table>";
				?>
			</div>
			<input type="hidden" name="numtop" id="numtop" value="<?php echo @ $_POST['numtop'];?>" />
		</form>
	</body>
</html>
