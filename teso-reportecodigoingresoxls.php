<?php  
require_once 'PHPExcel/Classes/PHPExcel.php';
include 'PHPExcel/Classes/PHPExcel/IOFactory.php';// PHPExcel_IOFactory
require "comun.inc";
require "funciones.inc";
session_start();
$linkbd = conectar_v7();
$linkbd -> set_charset("utf8"); 
$objPHPExcel = new PHPExcel();

//----Propiedades----
$objPHPExcel->getProperties()
        ->setCreator("SPID")
        ->setLastModifiedBy("SPID")
        ->setTitle("Exportar Excel con PHP")
        ->setSubject("Reporte de Codigos Ingresos")
        ->setDescription("Reporte de Codigos Ingresos")
        ->setKeywords("Reporte")
        ->setCategory("reportes");
$objPHPExcel-> getActiveSheet ()
        -> getStyle ("A1")
        -> getFill ()
        -> setFillType (PHPExcel_Style_Fill :: FILL_SOLID)
        -> getStartColor ()
        -> setRGB ('C8C8C8');
$objPHPExcel-> getActiveSheet ()
        -> getStyle ("A2:I2")
        -> getFill ()
        -> setFillType (PHPExcel_Style_Fill :: FILL_SOLID)
        -> getStartColor ()
        -> setRGB ('A6E5F3');
//----Cuerpo de Documento----
$objPHPExcel->getActiveSheet()->mergeCells('A1:I1');
$objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue('A1', 'Reporte de Codigos Ingresos');

$objFont=$objPHPExcel->getActiveSheet()->getStyle('A1')->getFont();
$objFont->setName('Courier New'); 
$objFont->setSize(15); 
$objFont->setBold(true); 
$objFont->setUnderline(PHPExcel_Style_Font::UNDERLINE_SINGLE);
$objFont->getColor()->setARGB( PHPExcel_Style_Color::COLOR_BLACK);
$borders = array(
      'borders' => array(
        'allborders' => array(
          'style' => PHPExcel_Style_Border::BORDER_THIN,
          'color' => array('argb' => 'FF000000'),
        )
      ),
    );
    $objPHPExcel->getActiveSheet()->getStyle('A2:I2')->applyFromArray($borders);

$objAlign=$objPHPExcel->getActiveSheet()->getStyle('A1')->getAlignment(); 
$objAlign->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER); 
$objAlign->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
$objPHPExcel-> getActiveSheet()->getColumnDimension('A')->setWidth(15);
$objPHPExcel-> getActiveSheet()->getColumnDimension('B')->setWidth(15);
$objPHPExcel-> getActiveSheet()->getColumnDimension('C')->setWidth(25);
$objPHPExcel-> getActiveSheet()->getColumnDimension('D')->setWidth(25);
$objPHPExcel-> getActiveSheet()->getColumnDimension('E')->setWidth(25);
$objPHPExcel-> getActiveSheet()->getColumnDimension('F')->setWidth(50);
$objPHPExcel-> getActiveSheet()-> getStyle('F')->getAlignment()-> applyFromArray (array ( 'horizontal'  =>  PHPExcel_Style_Alignment :: VERTICAL_JUSTIFY ,) );
$objPHPExcel-> getActiveSheet()->getColumnDimension('G')->setWidth(20);
$objPHPExcel-> getActiveSheet()->getColumnDimension('H')->setWidth(20);
$objPHPExcel-> getActiveSheet()->getColumnDimension('I')->setWidth(20);
$objWorksheet = $objPHPExcel->getActiveSheet();
$objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue('A2', 'Item')
            ->setCellValue('B2', 'N° Recibo')
            ->setCellValue('C2', 'Fecha')
            ->setCellValue('D2', 'Tercero')
            ->setCellValue('E2', 'Nombre Tercero')
            ->setCellValue('F2','Descripcion')
            ->setCellValue('G2', 'N° Liquidacion')
            ->setCellValue('H2', 'Valor')
            ->setCellValue('I2', 'Estado');


$i=3;

for($x=0;$x<count($_POST['item']);$x++)
{               
    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0,$i,$_POST['item'][$x]);
    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(1,$i,$_POST['id_recibos'][$x]);
    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(2,$i,$_POST['fecha'][$x]);
    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(3,$i,$_POST['tercero'][$x]);
    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(4,$i,$_POST['nombreTercero'][$x]);
    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(5,$i,iconv($_SESSION["VERCARPDFINI"], $_SESSION["VERCARPDFFIN"]."//TRANSLIT",$_POST['descripcion'][$x]));
    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(6,$i,$_POST['liquidacion'][$x]);
    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(7,$i,$_POST['valor'][$x]);
    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(8,$i,$_POST['estado'][$x]);
    
    //$objPHPExcel->getActiveSheet()->getStyle("A$i:P$i")->applyFromArray($borders);
    $objPHPExcel->getActiveSheet()->getStyle("A$i:I$i")->getAlignment()->applyFromArray(array('vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,));
    $objPHPExcel->getActiveSheet()->getStyle("H$i")->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);
    $i+=1;
}
$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(6,$i,'Total: ');
$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(7,$i,$_POST['valtotal']);
$objPHPExcel->getActiveSheet()->getStyle("H$i")->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);
$objPHPExcel-> getActiveSheet ()
        -> getStyle ("G$i")
        -> getFill ()
        -> setFillType (PHPExcel_Style_Fill :: FILL_SOLID)
        -> getStartColor ()
        -> setRGB ('C8C8C8');


    
              
            
//----Propiedades de la hoja

//$objPHPExcel->getActiveSheet()->getStyle("A$i:P$i")->getAlignment()->applyFromArray(array('horizontal' => PHPExcel_Style_Alignment::VERTICAL_CENTER,));
$objPHPExcel->getActiveSheet()->getStyle('A2:I2')->getAlignment()->applyFromArray(array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,));
$objPHPExcel->getActiveSheet()->setTitle('Teso-Reporte-Ingresos');
$objPHPExcel->setActiveSheetIndex(0);

//----Guardar documento----
header('Content-Type: application/vnd.ms-excel');
header('Content-Disposition: attachment;filename="Reporte-Cod-Ingresos.xls"');
header('Cache-Control: max-age=0');
 
$objWriter=PHPExcel_IOFactory::createWriter($objPHPExcel,'Excel5');
$objWriter->save('php://output');
exit;

?>