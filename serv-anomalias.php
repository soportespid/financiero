<?php
	header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=iso-8859-1");
	require "comun.inc";
	require "funciones.inc";
	session_start();
	$linkbd = conectar_v7();
	cargarcodigopag($_GET['codpag'],$_SESSION["nivel"]);
	date_default_timezone_set("America/Bogota");
?>
<!DOCTYPE >
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="es">
	<head>
		<meta http-equiv="Content-type" content="text/html; charset=iso-8859-1"/>
		<meta http-equiv="X-UA-Compatible" content="IE=9"/>
		<title>:: IDEAL 10 - Servicios P&uacute;blicos</title>
		<link href="css/css2.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<link href="css/cssSP.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<script type="text/javascript" src="css/programas.js"></script>
		<style></style>
		<script>

			function despliegamodalm(_valor,_tip,mensa,pregunta)
			{
				document.getElementById("bgventanamodalm").style.visibility=_valor;
				if(_valor == "hidden")
				{
					document.getElementById('ventanam').src = "";
				}
				else
				{
					switch(_tip)
					{
						case "1":	document.getElementById('ventanam').src="ventana-mensaje1.php?titulos="+mensa;break;
						case "2":	document.getElementById('ventanam').src="ventana-mensaje3.php?titulos="+mensa;break;
						case "3":	document.getElementById('ventanam').src="ventana-mensaje2.php?titulos="+mensa;break;
						case "4":	document.getElementById('ventanam').src="ventana-consulta1.php?titulos="+mensa+"&idresp="+pregunta;break;	
					}
				}
			}

			function funcionmensaje()
			{
				document.location.href = "serv-anomalias.php";
			}

			function respuestaconsulta(pregunta)
			{
				switch(pregunta)
				{
					case "1":	
						document.form2.oculto.value = '2';
						document.form2.submit();
					break;
				}
			}

			function guardar()
			{
				var codigo = document.getElementById('codban').value;
				var nombre = document.getElementById('nomban').value;
				var servicio = document.getElementById('codser').value

				if (codigo.trim() != '' && nombre.trim() != '' && servicio != '-1') 
				{
					despliegamodalm('visible','4','Esta Seguro de Guardar','1');
				}
				else 
				{
					despliegamodalm('visible','2','Falta informaci&oacute;n para crear la Anomalia');
				}
			}

			function cambiocheck()
			{
				if(document.getElementById('myonoffswitch').value == 'S')
				{
					document.getElementById('myonoffswitch').value = 'N';
				}
				else
				{
					document.getElementById('myonoffswitch').value = 'S';
				}

				document.form2.submit();
			}

			function despliegamodal2(_valor,_num)
			{
				document.getElementById("bgventanamodal2").style.visibility=_valor;

				if(_valor=="hidden")
				{
					document.getElementById('ventana2').src="";
				}
				else
				{
					document.getElementById('ventana2').src="serv-servicios_ventana01.php?idserv=codser&nomserv=nomser";
				}
			}

			function buscaservicio()
			{
 				document.form2.bser.value='1';
 				document.form2.submit();
 			}
		</script>

		<?php titlepag();?>

	</head>

	<body>
		<IFRAME src="alertas.php" name="alertas" id="alertas" style="display:none"></IFRAME>
		<span id="todastablas2"></span>
		<table>
			<tr><script>barra_imagenes("serv");</script><?php cuadro_titulos();?></tr>

			<tr><?php menu_desplegable("serv");?></tr>

			<tr>
				<td colspan="3" class="cinta">
					<a href="serv-anomalias.php" class="mgbt"><img src="imagenes/add.png"/></a>

					<a onclick="guardar()" class="mgbt"><img src="imagenes/guarda.png" title="Guardar"/></a>

					<a href="serv-anomaliasbuscar.php" class="mgbt"><img src="imagenes/busca.png" title="Buscar"/></a>

					<a onclick="mypop=window.open('plan-agenda.php','','');mypop.focus()" class="mgbt"><img src="imagenes/agenda1.png" title="Agenda" /></a>

					<a onclick="mypop=window.open('serv-principal.php','','');mypop.focus();" class="mgbt"><img src="imagenes/nv.png" title="Nueva ventana"></a>

					<a onclick="mypop=window.open('<?php echo $url2; ?>','','');mypop.focus();" class="mgbt"><img src="imagenes/duplicar_pantalla.png" title="Duplicar pesta&ntilde;a"></a>
				</td>
			</tr>
		</table>

		<div id="bgventanamodalm" class="bgventanamodalm">
			<div id="ventanamodalm" class="ventanamodalm">
				<IFRAME src="" name="ventanam" marginWidth=0 marginHeight=0 frameBorder=0 id="ventanam" frameSpacing=0 style=" width:700px; height:130px; top:200; overflow:hidden;"></IFRAME>
			</div>
		</div>

		<form name="form2" method="post" action="">

			<?php 
				if(@$_POST['oculto'] == "")
				{
					$_POST['onoffswitch'] = "S";
					$_POST['codban'] = selconsecutivo('srvanomalias','id');
				}

				if(@$_POST['bser'] == '1')
				{
					$sqlr = "SELECT nombre FROM srvservicios WHERE id='".$_POST['codser']."' ORDER BY id";
					$resp = mysqli_query($linkbd,$sqlr);
					$row  = mysqli_fetch_row($resp);

					if($row[0]!='')
					{
						$_POST['nomser']=$row[0];
					}
					else 
					{
						$_POST['nomser']="";
					}
				}
			?>

			<table class="inicio">
				<tr>
					<td class="titulos" colspan="6">.: Ingresar Anomalia</td>

					<td class="cerrar" style="width:7%" onClick="location.href='serv-principal.php'">Cerrar</td>
				</tr>

				<tr>
					<td class="tamano01" style="width:3cm;">C&oacute;digo:</td>

					<td style="width:15%;">
						<input type="text" name="codban" id="codban" onKeyUp="return tabular(event,this)" value="<?php echo @$_POST['codban'];?>" style="width:100%;height:30px;text-align:center" readonly/>
					</td>

					<td class="tamano01" style="width:3cm;">Nombre:</td>

					<td>
						<input type="text" name="nomban" id="nomban" value="<?php echo @$_POST['nomban'];?>" style="width:100%;height:30px;text-transform:uppercase"/>
					</td>
				</tr>

				<tr>
					<td class="tamano01" style="width:3cm;">Servicio:</td>

					<td style="width:15%;">
						<input type="text" name="codser" id="codser" onKeyUp="return tabular(event,this)" value="<?php echo @ $_POST['codser'];?>" onChange="buscaservicio();" style="width:100%;height: 30px; text-align:center;" onDblClick="despliegamodal2('visible');" class="colordobleclik" autocomplete="off"/>
					</td>

					<td colspan="2">
						<input type="text" name="nomser" id="nomser" value="<?php echo @ $_POST['nomser'];?>" style="width:100%;height:30px;text-transform:uppercase" readonly/>
					</td>

					<input type="hidden" name="bser" id="bser" value="0"/>

					<td class="tamano01">Estado:</td>

					<td>
						<div class="onoffswitch">
							<input type="checkbox" name="onoffswitch" class="onoffswitch-checkbox" id="myonoffswitch" value="<?php echo @ $_POST['onoffswitch'];?>" <?php if(@ $_POST['onoffswitch']=='S'){echo "checked";}?> onChange="cambiocheck();"/>
							<label class="onoffswitch-label" for="myonoffswitch">
								<span class="onoffswitch-inner"></span>
								<span class="onoffswitch-switch"></span>
							</label>
						</div>
					</td>
				</tr>
			</table>

			<input type="hidden" name="oculto" id="oculto" value="1"/>

			<?php 
				if(@$_POST['oculto'] == "2")
				{
					if (@$_POST['onoffswitch'] != 'S')
					{
						$valest = 'N';
					}
					else 
					{
						$valest = 'S';
					}

					$_POST['codban'] = selconsecutivo('srvanomalias','id');

					$sqlr = " INSERT INTO srvanomalias (id,nombre,estado,id_servicio) VALUES ('".$_POST['codban']."','".$_POST['nomban']."','$valest', '".$_POST['codser']."')";

					if (!mysqli_query($linkbd,$sqlr))
					{
						echo"
						<script>
							despliegamodalm('visible','2','No se pudo ejecutar la peticion');
						</script>";
					}
					else 
					{
						echo "
						<script>
							despliegamodalm('visible','1','Se ha almacenado con Exito');
						</script>";
					}
				}
			?>
			
		</form>

		<div id="bgventanamodal2">
			<div id="ventanamodal2">
				<IFRAME  src="" name="buster" marginWidth=0 marginHeight=0 frameBorder=0 id="ventana2" frameSpacing=0 style="left:500px; width:900px; height:500px; top:200;"></IFRAME>
			</div>
		</div>

	</body>
</html>