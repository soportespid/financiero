<?php

    header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=utf8");
	require 'comun.inc';
	require 'funciones.inc';
	session_start();
    if(empty($_SESSION)){
        header("location: index.php");
    }
	$linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");
	cargarcodigopag($_GET['codpag'],$_SESSION["nivel"]);
	date_default_timezone_set("America/Bogota");
?>
<!DOCTYPE >
<html lang=es>
	<head>
    <meta charset="utf-8"/>
		<meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1"/>
		<meta name="viewport" content="user-scalable=no">
		<title>:: IDEAL 10 - Planeación estrategica</title>
		<link href="favicon.ico" rel="shortcut icon"/>
		<link href="css/css2z.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
		<link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
		<link href="css/css4.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
        <link href="css/tabs.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css"/>
        <link href="css/style.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css"/>
        <link rel="stylesheet" href="sweetalert2/dist/sweetalert2.min.css">
        <script type="text/javascript" src="css/programas.js"></script>
        <script src="sweetalert2/dist/sweetalert2.min.js"></script>
		<script src="vue/vue.min.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script src="css/programas.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script src="css/calendario.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
        <style>
			[v-cloak]{display : none;}
		</style>
	</head>
	<body>
		<header>
			<table>
				<tr><script>barra_imagenes("plan");</script><?php cuadro_titulos();?></tr>
			</table>
		</header>

        <section id="myapp" v-cloak>
            <input type="hidden" value = "1" ref="pageType">
            <div class="loading-container" v-show="isLoading" >
                <p class="text-loading"data-loading="IDEAL 10">IDEAL 10</p>
            </div>
            <nav>
                <table>
                    <tr><?php menu_desplegable("plan");?></tr>
                </table>
                <div class="bg-white group-btn p-1" id="newNavStyle">
                    <button type="button" class="btn btn-white btn-primary-hover d-flex justify-between align-items-center" @click="window.location.reload()">
                        <span>Nuevo</span>
                        <svg viewBox="0 -960 960 960" ><path d="M440-440H200v-80h240v-240h80v240h240v80H520v240h-80v-240Z"/></svg>
                    </button>
                    <button type="button" class="btn btn-white btn-primary-hover d-flex justify-between align-items-center" @click="save()">
                        <span>Guardar</span>
                        <svg viewBox="0 -960 960 960" ><path d="M840-680v480q0 33-23.5 56.5T760-120H200q-33 0-56.5-23.5T120-200v-560q0-33 23.5-56.5T200-840h480l160 160Zm-80 34L646-760H200v560h560v-446ZM480-240q50 0 85-35t35-85q0-50-35-85t-85-35q-50 0-85 35t-35 85q0 50 35 85t85 35ZM240-560h360v-160H240v160Zm-40-86v446-560 114Z"/></svg>
                    </button>
                    <button type="button" class="btn btn-white btn-primary-hover d-flex justify-between align-items-center" @click="location.href='plan-buscaIndicadorProducto'">
                        <span>Buscar</span>
                        <svg class="fill-black group-hover:fill-white w-5 h-5" viewBox="0 -960 960 960" ><path d="M784-120 532-372q-30 24-69 38t-83 14q-109 0-184.5-75.5T120-580q0-109 75.5-184.5T380-840q109 0 184.5 75.5T640-580q0 44-14 83t-38 69l252 252-56 56ZM380-400q75 0 127.5-52.5T560-580q0-75-52.5-127.5T380-760q-75 0-127.5 52.5T200-580q0 75 52.5 127.5T380-400Z"/></svg>
                    </button>
                    <button type="button" class="btn btn-white btn-primary-hover d-flex justify-between align-items-center" @click="mypop=window.open('plan-principal.php','',''); mypop.focus();">
                        <span>Nueva ventana</span>
                        <svg class="fill-black group-hover:fill-white w-5 h-5" viewBox="0 -960 960 960" ><path d="M320-120v-80h80v-80H160q-33 0-56.5-23.5T80-360v-400q0-33 23.5-56.5T160-840h640q33 0 56.5 23.5T880-760v400q0 33-23.5 56.5T800-280H560v80h80v80H320ZM160-360h640v-400H160v400Zm0 0v-400 400Z"/></svg>
                    </button>
                    <button type="button" class="btn btn-success d-flex justify-between align-items-center" @click="window.location.href='plan-menuPlanEstrategico'">
                        <span>Atrás</span>
                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 -960 960 960"><path d="M760-200v-160q0-50-35-85t-85-35H273l144 144-57 56-240-240 240-240 57 56-144 144h367q83 0 141.5 58.5T840-360v160h-80Z"/></svg>
                    </button>
                </div>
            </nav>
            <article>
                <div class="bg-white">
                    <div>
                        <h2 class="titulos m-0">Crear indicador de producto</h2>
                        <p class="m-0 ms-2">Todos los campos con (<span class="text-danger fw-bolder">*</span>) son obligatorios</p>

                        <div class="form-control w-25">
                            <label class="form-label m-0" for="">Plan de desarrollo territorial<span class="text-danger fw-bolder">*</span></label>
                            <select v-model="pdtId" @change="changeDataPdt()">
                                <option value="" selected>Seleccione</option>
                                <option v-for="(pdt, index) in planes" :value="pdt.id">{{pdt.nombre_pdt}}</option>
                            </select>
                        </div>

                        <div class="d-flex">
                            <div class="form-control w-25">
                                <label class="form-label m-0" for="">Sector<span class="text-danger fw-bolder">*</span></label>
                                <input type="text" v-model="codSector" @dblclick="deployModal('sector')" class="colordobleclik" readonly>
                            </div>

                            <div class="form-control justify-between">
                                <label class="form-label m-0" for=""></label>
                                <input type="text" v-model="nomSector" readonly>
                            </div>

                            <div class="form-control w-25">
                                <label class="form-label m-0" for="">Programa<span class="text-danger fw-bolder">*</span></label>
                                <input type="text" v-model="codPrograma" @dblclick="deployModal('programa')" class="colordobleclik" readonly>
                            </div>

                            <div class="form-control justify-between">
                                <label class="form-label m-0" for=""></label>
                                <input type="text" v-model="nomPrograma" readonly>
                            </div>
                        </div>

                        <div>
                            <div class="d-flex">
                                <div class="form-control w-25">
                                    <label class="form-label m-0" for="">Producto<span class="text-danger fw-bolder">*</span></label>
                                    <input type="text" v-model="codProducto" @dblclick="deployModal('producto')" class="colordobleclik" readonly>
                                </div>

                                <div class="form-control justify-between">
                                    <label class="form-label m-0" for=""></label>
                                    <input type="text" v-model="nomProducto" readonly>
                                </div>

                                <div class="form-control w-25">
                                    <label class="form-label m-0" for="">Indicador<span class="text-danger fw-bolder">*</span></label>
                                    <input type="text" v-model="codIndicador" @dblclick="deployModal('indicador')" class="colordobleclik" readonly>
                                </div>

                                <div class="form-control justify-between">
                                    <label class="form-label m-0" for=""></label>
                                    <input type="text" v-model="nombreIndicador" readonly>
                                </div>
                            </div>

                            <div class="d-flex">
                                <div class="form-control">
                                    <label class="form-label m-0" for="">Unidad de medida<span class="text-danger fw-bolder">*</span></label>
                                    <input type="text" v-model="unidadMedida" readonly>
                                </div>

                                <div class="form-control">
                                    <label class="form-label m-0" for="">Valor cuatrienio (pesos)<span class="text-danger fw-bolder">*</span></label>
                                    <div class="form-number number-primary">
                                        <span></span>
                                        <input type="text" v-model="valorFormat" @input="formatInputNumber('valorFormat', 'valor')" class="text-right">
                                    </div>
                                </div>

                                <div class="form-control">
                                    <label class="form-label m-0" for="">Meta cuatrienio<span class="text-danger fw-bolder">*</span></label>
                                    <input type="number" v-model="metaCuatrienio" class="text-right">
                                </div>

                                <div class="form-control">
                                    <label class="form-label m-0" for="">Linea base<span class="text-danger fw-bolder">*</span></label>
                                    <input type="number" v-model="lineaBase">
                                </div>
                            </div>

                            <div class="d-flex">
                                <div class="form-control">
                                    <label class="form-label m-0" for="">Nombre personalizado<span class="text-danger fw-bolder">*</span></label>
                                    <input type="text" v-model="nombrePersonalizado">
                                </div>

                                <div class="form-control w-25">
                                    <label class="form-label m-0" for="">Indicador resultado<span class="text-danger fw-bolder">*</span></label>
                                    <input type="text" v-model="codResultado" @dblclick="modalResultado=true;" class="colordobleclik" readonly>
                                </div>

                                <div class="form-control justify-between">
                                    <label class="form-label m-0" for=""></label>
                                    <input type="text" v-model="nomResultado" readonly>
                                </div>
                            </div>

                            <div class="d-flex">
                                <div class="form-control">
                                    <label class="form-label m-0" for="">Lineas estrategicas<span class="text-danger fw-bolder">*</span></label>
                                    <div>
                                        <multiselect
											v-model="ejesSelected"
											placeholder="Seleccione uno o varios ejes estrategicos"
											label="nombre" track-by="nombre"
											:options="ejesEstrategicos"
											:multiple="true"
                                            :searchable="false"
										></multiselect>
                                    </div>
                                </div>

                                <div class="form-control w-50">
                                    <label class="form-label m-0" for="">Funcionario responsable<span class="text-danger fw-bolder">*</span></label>
                                    <input type="text" v-model="documento" @dblclick="modalFuncionarios=true;" class="text-center colordobleclik" readonly>
                                </div>

                                <div class="form-control">
                                    <label class="form-label m-0" for=""><span class="text-danger fw-bolder">*</span></label>
                                    <input type="text" v-model="funcionario" readonly>
                                </div>
                            </div>
                        </div>
                    </div>                       
                </div>

                <!-- modales -->
                <div v-show="modalSectores" class="modal">
                    <div class="modal-dialog modal-sm">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title">Sectores</h5>
                                <button type="button" @click="modalSectores=false;" class="btn btn-close"><div></div><div></div></button>
                            </div>
                            <div class="modal-body">
                                <div class="d-flex flex-column">
                                    <div class="form-control m-0 mb-3">
                                        <input type="search" v-model="txtSearch" @keyup="searchData('modalSector')" placeholder="Busca código o nombre">
                                    </div>
                                </div>
                                <div class="overflow-auto max-vh-50 overflow-x-hidden" >
                                    <table class="table table-hover fw-normal">
                                        <thead>
                                            <tr>
                                                <th class="text-center">Código</th>
                                                <th>Nombre</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr v-for="(sec,index) in sectores" :key="index" @click="selectItem('selectSector', sec)">
                                                <td class="text-center">{{sec.codigo}}</td>
                                                <td>{{sec.nombre}}</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div v-show="modalPrograma" class="modal">
                    <div class="modal-dialog modal-lg">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title">Programas</h5>
                                <button type="button" @click="modalPrograma=false;" class="btn btn-close"><div></div><div></div></button>
                            </div>
                            <div class="modal-body">
                                <div class="d-flex flex-column">
                                    <div class="form-control m-0 mb-3">
                                        <input type="search" v-model="txtSearch" @keyup="searchData('modalPrograma')" placeholder="Busca código o nombre">
                                    </div>
                                </div>
                                <div class="overflow-auto max-vh-50 overflow-x-hidden" >
                                    <table class="table table-hover fw-normal">
                                        <thead>
                                            <tr>
                                                <th class="text-center">Código</th>
                                                <th>Nombre</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr v-for="(pro,index) in programas" :key="index" @click="selectItem('selectPrograma', pro)">
                                                <td class="text-center">{{pro.codigo}}</td>
                                                <td>{{pro.nombre}}</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div v-show="modalProducto" class="modal">
                    <div class="modal-dialog modal-lg">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title">Productos</h5>
                                <button type="button" @click="modalProducto=false;" class="btn btn-close"><div></div><div></div></button>
                            </div>
                            <div class="modal-body">
                                <div class="d-flex flex-column">
                                    <div class="form-control m-0 mb-3">
                                        <input type="search" v-model="txtSearch" @keyup="searchData('modalProducto')" placeholder="Busca código o nombre">
                                    </div>
                                </div>
                                <div class="overflow-auto max-vh-50 overflow-x-hidden" >
                                    <table class="table table-hover fw-normal">
                                        <thead>
                                            <tr>
                                                <th class="text-center">Código</th>
                                                <th>Nombre</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr v-for="(produ,index) in productos" :key="index" @click="selectItem('selectProducto', produ)">
                                                <td class="text-center">{{produ.codigo}}</td>
                                                <td>{{produ.nombre}}</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div v-show="modalIndicadores" class="modal">
                    <div class="modal-dialog modal-xl" >
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title">Indicadores</h5>
                                <button type="button" @click="modalIndicadores=false;" class="btn btn-close"><div></div><div></div></button>
                            </div>
                            <div class="modal-body">
                                <div class="d-flex flex-column">
                                    <div class="form-control m-0 mb-3">
                                        <input type="search" v-model="txtSearch" @keyup="searchData('modalIndicador')" placeholder="Busca código indicador o nombre">
                                    </div>
                                </div>
                                <div class="overflow-auto max-vh-50 overflow-x-hidden" >
                                    <table class="table table-hover fw-normal">
                                        <thead>
                                            <tr>
                                                <th>Código indicador</th>
                                                <th>Nombre indicador</th>
                                                <th>Unidad de medida</th>
                                                <th>Indicador principal</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr v-for="(ind,index) in indicadores" :key="index" @click="selectItem('selectIndicador', ind)">
                                                <td>{{ind.codigo_indicador}}</td>
                                                <td>{{ind.nombre_indicador}}</td>
                                                <td>{{ind.unidad_medida}}</td>
                                                <td>{{ind.indicador_principal}}</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div v-show="modalResultado" class="modal">
                    <div class="modal-dialog modal-lg">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title">Indicadores de resultado</h5>
                                <button type="button" @click="modalResultado=false;" class="btn btn-close"><div></div><div></div></button>
                            </div>
                            <div class="modal-body">
                                <div class="d-flex flex-column">
                                    <div class="form-control m-0 mb-3">
                                        <input type="search" v-model="txtSearch" @keyup="searchData('modalResultado')" placeholder="Busca código o nombre">
                                    </div>
                                </div>
                                <div class="overflow-auto max-vh-50 overflow-x-hidden" >
                                    <table class="table table-hover fw-normal">
                                        <thead>
                                            <tr>
                                                <th class="text-center">Código</th>
                                                <th>Nombre</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr v-for="(resultado,index) in indicadoresResultados" :key="index" @click="selectItem('selectResultado', resultado)">
                                                <td class="text-center">{{resultado.codigo_indicador}}</td>
                                                <td>{{resultado.nombre_indicador}}</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div v-show="modalFuncionarios" class="modal">
                    <div class="modal-dialog modal-xl" >
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title">Funcionarios</h5>
                                <button type="button" @click="modalFuncionarios=false;" class="btn btn-close"><div></div><div></div></button>
                            </div>
                            <div class="modal-body">
                                <div class="d-flex flex-column">
                                    <div class="form-control m-0 mb-3">
                                        <input type="search" v-model="txtSearch" @keyup="searchData('modalFuncionario')" placeholder="Busca por documento, nombre o cargo de funcionario">
                                    </div>
                                </div>
                                <div class="overflow-auto max-vh-50 overflow-x-hidden" >
                                    <table class="table table-hover fw-normal">
                                        <thead>
                                            <tr>
                                                <th class="text-center">Documento</th>
                                                <th>Nombre funcionario</th>
                                                <th>Cargo</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr v-for="(fun,index) in funcionarios" :key="index" @click="selectItem('selectFuncionario', fun)">
                                                <td class="text-center">{{fun.cedulanit}}</td>
                                                <td>{{fun.nombre}}</td>
                                                <td>{{fun.nombre_cargo}}</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </article>
        </section>

        <script src="Librerias/vue/vue.min.js"></script>
		<script src="Librerias/vue/axios.min.js"></script>
        <script src="https://unpkg.com/vue-multiselect@2.1.0"></script>
        <link rel="stylesheet" href="multiselect.css">
		<script type="module" src="planeacion/planDesarrollo/js/indicadorProductos_functions.js"></script>
	</body>
</html>
