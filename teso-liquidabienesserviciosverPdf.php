<?php
	require_once("tcpdf/tcpdf_include.php");
	require('comun.inc');
	require "funciones.inc";
    require "funcionesSP.inc.php";

	date_default_timezone_set("America/Bogota");
	session_start();

	class MYPDF extends TCPDF{
		public function Header(){
			if ($_POST['estado']=='R'){
				$this->Image('imagenes/reversado02.png',75,41.5,50,15);
			}
			$linkbd = conectar_v7();
			$linkbd -> set_charset("utf8");
			$sqlr = "SELECT * FROM configbasica WHERE estado='S' ";
			$res = mysqli_query($linkbd, $sqlr);
			while($row = mysqli_fetch_row($res)){
				$nit = $row[0];
				$rs = $row[1];
				$nalca = $row[6];
			}
			$detallegreso = $_POST['detallegreso'];
			//Parte Izquierda
			$this->Image('imagenes/escudo.jpg',  13, 13, 20, 20, 'JPG', '', 'T', true, 300, '', false, false, 0, false, false, false);// Logo
			$this->SetFont('helvetica','B',8);
			$this->SetY(10);
			$this->RoundedRect(10, 10, 277, 25, 1,'');
			$this->Cell(0.1);
			$this->Cell(26,25,'','R',0,'L'); 
			$this->SetY(8);
			$this->SetX(80);
			$this->SetFont('helvetica','B',9);
			$this->Cell(160,15,strtoupper("$rs"),0,0,'C'); 
			$this->SetFont('helvetica','B',7);
			$this->SetY(12);
			$this->SetX(80);
			$this->Cell(160,15,'NIT: '.$nit,0,0,'C');
			//*****************************************************************************************************************************
			$this->SetFont('helvetica','B',9);			 
			$this->SetY(23);
			$this->SetX(36);
			$this->Cell(251,12,"LIQUIDACIÓN BIENES Y SERVICIOS",'T',0,'C'); 
			$mov='';
			if(isset($_POST['movimiento'])){
				if(!empty($_POST['movimiento'])){
					if($_POST['movimiento']=='401'){$mov="DOCUMENTO DE REVERSION";}
				}
			}

			$this->SetFont('helvetica','B',6);
			$this->SetY(10);
			$this->SetX(257);
			$this->Cell(30,5," NUMERO: ".$_POST['idcomp'],"L",0,'L');
			$this->SetY(13.5);
			$this->SetX(257);
			$this->Cell(35,6," FECHA: ".$_POST['fecha'],"L",0,'L');
			$this->SetY(18);
			$this->SetX(257);
			$this->Cell(35,5," VIGENCIA: ".$_POST['vigencia'],"L",0,'L');

			$this->SetFont('helvetica','B',8);
			$this->SetY(25);
			$this->cell(248,8,'TOTAL:   ',0,0,'R');
			$this->RoundedRect(257.5, 26 ,27, 6, 1,'');
			$this->cell(26,8,'$'.number_format($_POST['valorcheque'],2,",","."),0,0,'R');
			//**********************************************************
			$this->SetFont('times','B',10);
			$this->ln(12);
			//**********************************************************
		}
		public function Footer() 
		{
			$linkbd = conectar_v7();
			$linkbd -> set_charset("utf8");
			$sqlr = "SELECT direccion,telefono,web,email FROM configbasica WHERE estado='S'";
			$resp=mysqli_query($linkbd, $sqlr);
			$user = $_SESSION['nickusu'];	
			$fecha = date("Y-m-d H:i:s");
			$ip = $_SERVER['REMOTE_ADDR'];
			$useri = $_POST['user'];
			while($row = mysqli_fetch_row($resp)){
				$direcc = strtoupper($row[0]);
				$telefonos = $row[1];
				$dirweb = strtoupper($row[3]);
				$coemail = strtoupper($row[2]);
			}
			if($direcc!=''){
				$vardirec = "Dirección: $direcc, ";
			}else {
				$vardirec = "";
			}
			if($telefonos!=''){
				$vartelef = "Telefonos: $telefonos";
			}else{ 
				$vartelef = "";
			}
			if($dirweb!=''){$varemail="Email: $dirweb, ";}
			else {$varemail="";}
			if($coemail!=''){$varpagiw="Pagina Web: $coemail";}
			else{$varpagiw="";}
			$this->SetFont('helvetica', 'I', 8);
			$txt = <<<EOD
			$vardirec $vartelef
			$varemail $varpagiw
			EOD;
			$this->SetFont('helvetica', 'I', 6);
			$this->Cell(277,10,'','T',0,'T');
			$this->ln(2);
			$this->Write(0, $txt , '', 0, 'C', true, 0, false, false, 0);
			$this->Cell(25, 10, 'Hecho por: '.$useri, 0, false, 'C', 0, '', 0, false, 'T', 'M');
			$this->Cell(100, 10, 'Impreso por: '.$user, 0, false, 'C', 0, '', 0, false, 'T', 'M');
			$this->Cell(30, 10, 'IP: '.$ip, 0, false, 'C', 0, '', 0, false, 'T', 'M');
			$this->Cell(102, 10, 'Fecha: '.$fecha, 0, false, 'C', 0, '', 0, false, 'T', 'M');
			$this->Cell(5, 10, 'IDEAL.10 S.A.S    Pagina '.$this->getAliasNumPage().'/'.$this->getAliasNbPages(), 0, false, 'C', 0, '', 0, false, 'T', 'M');
		}
	}
	$pdf = new MYPDF('L','mm','Letter', true, 'iso-8859-1', false);
	$pdf->SetDocInfoUnicode (true); 
	// set document information
	$pdf->SetCreator(PDF_CREATOR);
	$pdf->SetAuthor('IDEALSAS');
	$pdf->SetTitle('Liquidacion bienes y servicios');
	$pdf->SetSubject('Certificado de Disponibilidad');
	$pdf->SetKeywords('TCPDF, PDF, example, test, guide');
	$pdf->SetMargins(10, 38, 10);// set margins
	$pdf->SetHeaderMargin(38);// set margins
	$pdf->SetFooterMargin(17);// set margins
	$pdf->SetAutoPageBreak(TRUE, 20);// set auto page breaks
	// set some language-dependent strings (optional)
	if (@file_exists(dirname(__FILE__).'/lang/spa.php')) 
	{
		require_once(dirname(__FILE__).'/lang/spa.php');
		$pdf->setLanguageArray($l);
	}	
	$pdf->AddPage();

    $pdf->SetFont('helvetica','B',7);
	$pdf->cell(25,4,'Concepto: ','LT',0,'L',0);
	$pdf->SetFont('helvetica','',7);	
	$pdf->cell(115,4,''.$_POST['concepto'],'T',0,'L',0);

    $pdf->SetFillColor(255,255,255);
	$pdf->SetFont('helvetica','B',7);
	$pdf->cell(25,4,'C.C. o NIT: ','T',0,'L',0);
	$pdf->SetFont('helvetica','',7);
	$pdf->cell(112,4,''.$_POST['tercero'],'TR',1,'L',0);
	$pdf->cell(0.2);

	$pdf->SetFillColor(245,245,245);
	$detalle = ucfirst(strtolower($_POST['ntercero']));
	$lineas = $pdf->getNumLines($detalle, 174);
	$alturadt=(4*$lineas);

	$pdf->SetFont('helvetica','B',7);
	$pdf->MultiCell(25,$alturadt,'Contribuyente: ', 'LB', 'J', 0, 0, '', '', true, 0, false, true, $alturadt, 'T');
	$pdf->SetFont('helvetica','',7);
	$pdf->MultiCell(252,$alturadt,"$detalle",'RB','L',false,1,'','',true,0,false,true,$alturadt,'T',false);
    $pdf->ln(2);	
	$y=$pdf->GetY();	
	$pdf->SetY($y);

	$y=$pdf->GetY();	
	$pdf->SetFillColor(222,222,222);
	$pdf->SetFont('helvetica','B',7);
    $pdf->Cell(0.1);
    $pdf->Cell(53.8,5,'Código',0,0,'C',1); 
	$pdf->SetY($y);
	$pdf->Cell(60);
	$pdf->Cell(53.8,5,'Ingreso',0,0,'C',1);
	$pdf->SetY($y);
	$pdf->Cell(120);
	$pdf->SetFont('helvetica','B',7);
	$pdf->Cell(53.8,5,'Centro de costo',0,0,'C',1);
	$pdf->SetY($y);
	$pdf->SetFont('helvetica','B',7);
	$pdf->Cell(180);
	$pdf->Cell(53.8,5,'Fuente',0,0,'C',1);
	$pdf->SetY($y);
	$pdf->Cell(240);
	$pdf->Cell(38,5,'Valor',0,0,'C',1);
	$pdf->SetFont('helvetica','',6);
	$cont=0;
	$pdf->ln(6);
    $arrDetalle = $_POST['detalle'];
    $rows = count($arrDetalle);
    $total = '$'.number_format($_POST['totalcf']);
    $totalLetras = $_POST['letras'];
	for($i=0;$i<$rows;$i++){
			$vv=$pdf->gety();//echo $vv."<br>";
			if($vv>=170)
			{ 
				$pdf->AddPage();
				$vv=$pdf->gety();
			}
			if ($con%2==0){$pdf->SetFillColor(255,255,255);}
			else {$pdf->SetFillColor(245,245,245);}
            $strCodigo = $arrDetalle[$i]['codigo'];
            $strIngreso = $arrDetalle[$i]['ingreso'];
            $strCentroCosto = $arrDetalle[$i]['centro_costo'];
            $strFuente = $arrDetalle[$i]['fuente'];
            $intValor = $arrDetalle[$i]['valor'];

			$lnIngreso = $pdf-> getNumLines($strIngreso, 40);
			$lnCentroCosto = $pdf->getNumLines($strCentroCosto, 40);


			$valorMax = max($lnIngreso,$lnCentroCosto);
			$altura = $valorMax * 3;
			
			//echo $altura."<br>";
			$pdf->MultiCell(53.8,$altura,$strCodigo,0,'C',true,0,'','',true,0,false,true,0,'L',false);
			$pdf->MultiCell(40.8,$altura,strtoupper($strIngreso),0,'C',true,0,80,'',true,0,false,true,0,'L',false);
			$pdf->MultiCell(40.8,$altura,strtoupper($strCentroCosto),0,'C',true,0,140,'',true,0,false,true,0,'',false);
			$pdf->MultiCell(53.8,$altura,$strFuente,0,'L',true,0,211,'',true,0,false,true,0,'L',false);
			$pdf->MultiCell(38,$altura,"$".number_format($intValor,2,",","."),0,'R',true,0,250,'',true,0,false,true,0,'L',false);
	}

    
    $pdf->ln(6);
    $y = $pdf->GetY();
    
    $pdf->setY($y);
    $pdf->SetFont('helvetica','B',7);
	$pdf->cell(25,4,'Son: ','LTB',0,'L',0);
	$pdf->SetFont('helvetica','',7);
	$pdf->cell(215,4,''.$totalLetras,'RTB',1,'L',0);
    $pdf->setY($y);
    $pdf->setX(250);
    $pdf->cell(38,4,'Total: '.$total,'LTBR',0,'R',0);
    $pdf->ln(6);

    if(count($_POST['ddescuentos'])!= 0)
	{
		$y=$pdf->GetY();
		$pdf->SetY($y);

		$y=$pdf->GetY();
		$pdf->SetFillColor(222,222,222);
		$pdf->SetFont('helvetica','B',7);
		$pdf->SetY($y);
		$pdf->Cell(0.1);
		$pdf->Cell(40,4,'Codigo ',0,0,'C',1);
		$pdf->SetY($y);
		$pdf->Cell(41);
		$pdf->Cell(140,4,'Retencion',0,0,'C',1);
		$pdf->SetY($y);
		$pdf->Cell(182);
		$pdf->Cell(35,4,'Porcentaje',0,0,'C',1);
		$pdf->SetY($y);
		$pdf->Cell(218);
		$pdf->Cell(59,4,'Valor',0,0,'C',1);
		$pdf->SetFont('helvetica','',6);
		$cont=0;
		$pdf->ln(1);
		for($x=0;$x<count($_POST['ddescuentos']);$x++)
		{
			$pdf->ln(3.5);
			if ($con%2==0){$pdf->SetFillColor(255,255,255);}
			else {$pdf->SetFillColor(245,245,245);}
			$pdf->Cell(41,3,''.$_POST['ddescuentos'][$x],'',0,'C',1);
			$pdf->Cell(141,3,''.$_POST['dndescuentos'][$x],'',0,'L',1);
			$pdf->Cell(35,3,''.$_POST['dporcentajes'][$x].'%','',0,'C',1);
			$pdf->Cell(60,3,'$'.number_format($_POST['ddesvalores'][$x],2),'',0,'R',1);
			$con=$con+1;
		}
	}
	$pdf->Output();
?> 


