<?php
require "comun.inc";
require "funciones.inc";
$linkbd=conectar_v7();
session_start();
cargarcodigopag($_GET['codpag'],$_SESSION["nivel"]);
//**niveles menu: Administracion (0) - Consultas (1) - Herramientas (2) - Reportes (3)
?>
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html" />
        <meta http-equiv="X-UA-Compatible" content="IE=9" />
        <title>:: IDEAL 10 - Tesoreria</title>

        <script>
            //************* ver reporte ************
            //***************************************
            function verep(idfac)
            {
                document.form1.oculto.value=idfac;
                document.form1.submit();
            }
            //************* genera reporte ************
            //***************************************
            function genrep(idfac)
            {
                document.form2.oculto.value=idfac;
                document.form2.submit();
            }

            function buscacta(e)
            {
                if (document.form2.cuenta.value!="")
                {
                    document.form2.bc.value='1';
                    document.form2.submit();
                }
            }

            function buscactap(e)
            {
                if (document.form2.cuentap.value!="")
                {
                    document.form2.bcp.value='1';
                    document.form2.submit();
                }
            }

            function validar()
            {
                document.form2.submit();
            }

            function guardar()
            {
                //   alert("Balance Descuadrado");
                //valor=document.form2.codigo.value;
                if (document.form2.cb.value!='' && document.form2.nbanco.value!='')
                {
                    if (confirm("Esta Seguro de Guardar"))
                    {
                        document.form2.oculto.value=2;
                        document.form2.action="teso-bancopredial.php";
                        document.form2.submit();
                    }
                }
                else 
                {
                    alert("Comprobante descuadrado o faltan informacion");
                }
            }

            function despliegamodal2(_valor,_nomve)
            {
                document.getElementById("bgventanamodal2").style.visibility=_valor;
                if(_valor=="hidden")
                {
                    document.getElementById('ventana2').src="";
                }
                else 
                {
                    //document.getElementById('ventana2').src="cuentas-ventana01.php";
                    switch(_nomve)
                    {
                        case 1:	document.getElementById('ventana2').src="cuentasbancarias-ventana02.php?tipoc=D&obj01=banco&obj02=nbanco&obj03=&obj04=cb&obj05=ter";break;
                        case 2:	document.getElementById('ventana2').src="tercerosgral-ventana01.php?objeto=tercero&nobjeto=ntercero&tnfoco=detallegreso";break;
                    }
                }
            }
        </script>
        <script src="css/programas.js"></script>
        <link href="css/css2.css" rel="stylesheet" type="text/css" />
        <link href="css/css3.css" rel="stylesheet" type="text/css" />

        <?php titlepag();?>

    </head>
    <body>
	    <IFRAME src="alertas.php" name="alertas" id="alertas" style="display:none"></IFRAME>
	    <span id="todastablas2"></span>
        <table>
            <tr><script>barra_imagenes("teso");</script><?php cuadro_titulos();?></tr>	 
            <tr><?php menu_desplegable("teso");?></tr>
            <tr>
                <td colspan="3" class="cinta">
                    <a href="teso-bancopredial.php" class="mgbt"><img src="imagenes/add.png" alt="Nuevo"  border="0"  title="Nuevo"/></a> 
                    <a href="#"  onClick="guardar();" class="mgbt"><img src="imagenes/guarda.png"  alt="Guardar" title="Guardar"/></a>
                    <a href="#" class="mgbt" > <img src="imagenes/busca.png"  alt="Buscar"  title="Buscar"/></a> 
                    <a href="#" onClick="mypop=window.open('teso-principal.php','','');mypop.focus();" class="mgbt" ><img src="imagenes/nv.png" alt="nueva ventana" title="Nueva ventana"></a> 
                </td>
            </tr>		  
        </table>
        <?php
            $vigencia=date('Y');
            if(!$_POST['oculto'])
            {		  			 
                $sqlr = "SELECT cuenta, nombre FROM bancopredial";
                $res = mysqli_query($linkbd, $sqlr);
                $row = mysqli_fetch_row($res);
                $_POST['cb'] = $row[0];
                $_POST['nbanco'] = $row[1];
            }
        ?>
        <form name="form2" method="post" action="">
            <table class="inicio" align="center" >
                <tr>
                    <td class="titulos" colspan="9">Crear Banco predial </td>
                    <td style="width:10%;" class="cerrar" ><a href="teso-principal.php"> Cerrar</a></td>
                </tr>
                <tr>
                    <td style="width:10%;" class="saludo1">Cuenta:  </td>
                    <td style="width:15%;">
						<input type="text" name="cb" id="cb" value="<?php echo $_POST['cb']; ?>" style="width:80%;" readonly/>&nbsp;
                        <a onClick="despliegamodal2('visible',1);"  style='cursor:pointer;' title='Listado Cuentas Bancarias'>	
                            <img src='imagenes/find02.png' style='width:20px;'/>
                        </a>
					</td>
					<td style="width:70%;">
						<input type='text' id='nbanco' name='nbanco' style='width:80%;' value="<?php echo $_POST['nbanco']; ?>" readonly>
					</td>
                    <input type='hidden' name='banco' id='banco' value="<?php echo $_POST['banco']; ?>"/>
                    <input type='hidden' id='ter' name='ter' value="<?php echo $_POST['ter']; ?>"/>
                    <input type='hidden' id='oculto' name='oculto' value="1"/></td>

                </tr> 
            </table>
            <div id="bgventanamodal2">
                <div id="ventanamodal2">
                    <IFRAME  src="" name="buster" marginWidth=0 marginHeight=0 frameBorder=0 id="ventana2" frameSpacing=0 style="left:500px; width:900px; height:500px; top:200;"> 
                    </IFRAME>
                </div>
            </div>
        </form>
        <?php
        $oculto=$_POST['oculto'];
        if($_POST['oculto']=='2')
        {
            
            if ($_POST['cb']!="" && $_POST['nbanco']!="" )
            {
                $sqlrD = "DELETE FROM bancopredial";
                mysqli_query($linkbd, $sqlrD);
                $sqlr = "INSERT INTO bancopredial (cuenta,nombre)VALUES ('$_POST[cb]','".utf8_decode($_POST['nbanco'])."')";
                if (!mysqli_query($linkbd, $sqlr))
                {
                    echo "<table><tr><td class='saludo1'><center><font color=blue>Manejador de Errores de la Clase BD<br><font size=1></font></font><br><p align=center>No se pudo ejecutar la petici�n: <br><font color=red><b>$sqlr</b></font></p>";
                    //	 $e =mysql_error($respquery);
                    echo "Ocurri� el siguiente problema:<br>";
                    //echo htmlentities($e['message']);
                    echo "<pre>";
                    ///echo htmlentities($e['sqltext']);
                    // printf("\n%".($e['offset']+1)."s", "^");
                    echo "</pre></center></td></tr></table>";
                }
                else
                {
                    echo "<table><tr><td class='saludo1'><center><h2>Se ha almacenado con Exito</h2></center></td></tr></table>";
                }
            }
            else
            {
                echo "<table><tr><td class='saludo1'><center><H2>Falta informacion para Crear el banco predial</H2></center></td></tr></table>";
            }
        }
	    ?> 
    </body>
</html>