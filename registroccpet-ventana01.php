<?php
	header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=utf8");

	require "comun.inc";
	require "funciones.inc";
	require "conversor.php";
	require "validaciones.inc";

	$linkbd_V7 = conectar_v7();
	$linkbd_V7 -> set_charset("utf8");

	session_start();
	date_default_timezone_set("America/Bogota");
?>
<!DOCTYPE >
<html lang="es">
	<head>
		<meta charset="UTF-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
    	<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<title>:: IDEAL 10 - Tesorer&iacute;a</title>
		<link href="favicon.ico" rel="shortcut icon"/>
        <link href="css/css2.css" rel="stylesheet" type="text/css" />
        <link href="css/css3.css" rel="stylesheet" type="text/css" />
		<script type="text/javascript" src="css/programas.js"></script>
		<script type="text/javascript" src="css/calendario.js"></script>
		<script> 
			function ponprefijo(idrp,idcdp,det,valor,saldo,tercero,vigencia)
			{ 
				parent.document.form2.rp.value =idrp  ;
				parent.document.form2.cdp.value =idcdp ;
				parent.document.form2.detallecdp.value =det ;	
				parent.document.form2.valorrp.value =valor ;	
				parent.document.form2.saldorp.value =saldo ;
				parent.document.form2.valor.value =saldo ;	
				parent.document.form2.tercero.value =tercero ;	
				parent.document.form2.vigencia.value =vigencia ;
				parent.document.form2.rp.focus();	
				parent.document.form2.cdp.focus();	
				parent.despliegamodal2("hidden");
			} 
		</script> 
	</head>
	<body >
  		<form name="form2" action="" method="post">
        	<?php 
				if ($_POST['oculto']=='')
				{
					$_POST['vigencia']=$_GET['vigencia'];
					$_POST['numpos']=0;$_POST['numres']=10;$_POST['nummul']=0;
				}
			?>
			<table  class="inicio" style="width:99.4%;">
            
      			<tr>
        			<td class="titulos" colspan="4">:: Buscar Registro</td>
                 	<td class="cerrar"><a onClick="parent.despliegamodal2('hidden');">&nbsp;Cerrar</a></td>
      			</tr>
      			<tr>
        			<td class="saludo1" style="width:3.5cm;">:: Numero Registro:</td>
        			<td>
                    	<input type="search" name="numero"  value="<?php echo $_POST['numero'];?>" />
						<input type="button" name="bboton" onClick="limbusquedas();" value="&nbsp;&nbsp;Buscar&nbsp;&nbsp;"/>
                    </td>
       			</tr>                       
    		</table> 
     		<input type="hidden" name="vigencia" value="<?php echo $_POST['vigencia']?>"/>
          	<input type="hidden" name="oculto" id="oculto"  value="1">
            <input type="hidden" name="numres" id="numres" value="<?php echo $_POST['numres'];?>"/>
    		<input type="hidden" name="numpos" id="numpos" value="<?php echo $_POST['numpos'];?>"/>
       		<input type="hidden" name="nummul" id="nummul" value="<?php echo $_POST['nummul'];?>"/>
    		<div class="subpantalla" style="height:84.5%; width:99%; overflow-x:hidden;">
				<?php
                    $crit1="";
                    $crit2="";
                    if ($_POST['numero']!=""){$crit1=" and (ccpetrp.consvigencia like '%$_POST[numero]%') ";}
                    $crit2=" and  ccpetrp.vigencia=$_POST[vigencia] and  ccpetcdp.vigencia=$_POST[vigencia]";
					$fecha = $_POST['vigencia'].'-12-31';
					
					$sqlr = "SELECT DC.vigencia, DC.consvigencia, DC.tercero, RP.detalle, RP.idcdp, RP.valor  FROM ccpetdc AS DC, ccpetrp AS RP WHERE DC.vigencia = '$_POST[vigencia]' AND DC.vigencia = RP.vigencia AND DC.consvigencia = RP.consvigencia";
					
                    $resp = mysqli_query($linkbd_V7, $sqlr);
                    $con=1;
                    echo "
					<table class='inicio' align='center' width='99%'>
						
						<tr>
							<td class='titulos2' style='width:3%'>Item</td>
							<td class='titulos2' style='width:4%'>Vigencia</td>
							<td class='titulos2' style='width:8%'>No RP</td>
							<td class='titulos2' colspan='2'>Detalle</td>
						</tr>";	
                    $iter='saludo1a';
                    $iter2='saludo2'; 
                    while ($row =mysqli_fetch_row($resp)) 
                    {
                    	$saldoRP = generaSaldoRPccpet($row[1],$row[0]);
                    	if($saldoRP!=0)
						{
                    		$detalle=$row[3];
							$con2 = $con+ $_POST['numpos'];
							echo"
							<tr class='$iter' onClick=\"javascript:ponprefijo('$row[1]','$row[4]','$detalle','$row[5]','$saldoRP','$row[2]','$row[0]')\" onMouseOver=\"anterior=this.style.backgroundColor;this.style.backgroundColor='#40b3ff';\" onMouseOut=\"this.style.backgroundColor=anterior\" style='text-transform:uppercase'>
								<td>$con2</td>
								<td>$row[0]</td>
								<td>$row[1]</td>
								<td colspan='2'>$detalle</td>
							</tr>";
							$con+=1;
							$aux=$iter;
							$iter=$iter2;
							$iter2=$aux;
                    	}
                       
                    } 
                    echo"</table>";
                ?>
			</div>
		</form>
	</body>
</html>
