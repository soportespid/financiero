<?php
	header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=utf8");
	require "comun.inc";
	require "funciones.inc";
	session_start();
	$linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");
	cargarcodigopag($_GET['codpag'],$_SESSION["nivel"]);
	date_default_timezone_set("America/Bogota");
	$scroll=$_GET['scrtop'];
	$totreg=$_GET['totreg'];
	$idcta=$_GET['idcta'];
	$altura=$_GET['altura'];
	$filtro="'".$_GET['filtro']."'";
?>
<!DOCTYPE >
<html lang="es">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1">
		<meta name="viewport" content="user-scalable=no">
		<title>:: IDEAL 10 - Tesorer&iacute;a</title>
		<link href="favicon.ico" rel="shortcut icon"/>
		<link href="css/css2z.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet"/>
		<link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet"/>
		<link href="css/css4.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet"/>
		<link href="sweetalert2/dist/sweetalert2.min.css" rel="stylesheet">
		<script src="sweetalert2/dist/sweetalert2.min.js"></script>
		<script type="text/javascript" src="css/calendario.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script type="text/javascript" src="css/programas.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script>
			function buscacta(e){
				if (document.form2.cuenta.value!=""){
					document.form2.bc.value='1';
					document.getElementById('oculto').value='7';
					document.form2.submit();
				}
			}
			function buscactac(e){
				if (document.form2.cuentac.value!=""){
					document.form2.bcc.value='1';
					document.getElementById('oculto').value='7';
					document.form2.submit();
				}
			}
			function buscactap(e){
				if (document.form2.cuentap.value!=""){
					document.form2.bcp.value='1';
					document.getElementById('oculto').value='7';
					document.form2.submit();
				}else{
					document.form2.bcp1.value='1';
					document.form2.submit();
				}
			}
			function validar(){
				document.getElementById('oculto').value='7';
				document.form2.submit();
			}
			//GUARDAR, ADICIONAR Y ELIMINAR
			function guardar(){
				if (document.form2.nombre.value != '' && document.form2.tipo.value != ''  && document.form2.codigo.value != ''){
					Swal.fire({
						icon: 'question',
						title: '¿Esta seguro de guardar los cambios?',
						showDenyButton: true,
						confirmButtonText: 'Guardar',
						confirmButtonColor: '#01CC42',
						denyButtonText: 'Cancelar',
						denyButtonColor: '#FF121A',
					}).then(
						(result) => {
							if (result.isConfirmed){
								document.form2.oculto.value = 2;
								document.form2.submit();
							}
							else if (result.isDenied){
								Swal.fire({
									icon: 'info',
									title: 'No se realizaron modificaciones',
									confirmButtonText: 'Continuar',
									confirmButtonColor: '#FF121A',
									timer: 2500
								});
							}
						}
					)
				}else{
					Swal.fire({
						icon: 'error',
						title: 'Error!',
						text: 'Faltan información para Modificar los Datos',
						confirmButtonText: 'Continuar',
						confirmButtonColor: '#FF121A',
						timer: 2500
					});
				}
			}
			function agregardetalle(){
				if(document.form2.valor.value != ""){
					document.form2.agregadet.value=1;
					document.getElementById('oculto').value = '7';
					document.form2.submit();
				}else{
					Swal.fire({
						icon: 'error',
						title: 'Error!',
						text: 'Faltan datos para agregar el registro',
						confirmButtonText: 'Continuar',
						confirmButtonColor: '#FF121A',
						timer: 2500
					});
				}
			}
			function eliminar(variable){
				Swal.fire({
					icon: 'question',
					title: '¿Esta seguro de eliminar el registro?',
					showDenyButton: true,
					confirmButtonText: 'Guardar',
					confirmButtonColor: '#01CC42',
					denyButtonText: 'Cancelar',
					denyButtonColor: '#FF121A',
				}).then(
					(result) => {
						if (result.isConfirmed){
							document.getElementById('elimina').value = variable;
							document.getElementById('oculto').value='6';
							document.form2.submit();
						}
						else if (result.isDenied){
							Swal.fire({
								icon: 'info',
								title: 'No se elimino el registro',
								confirmButtonText: 'Continuar',
								confirmButtonColor: '#FF121A',
								timer: 2500
							});
						}
					}
				)
			}
			function despliegamodalm(_valor,_tip,mensa,pregunta){
				document.getElementById("bgventanamodalm").style.visibility=_valor;
				if(_valor=="hidden"){document.getElementById('ventanam').src="";}
				else{
					switch(_tip){
						case "1":
							document.getElementById('ventanam').src="ventana-mensaje1.php?titulos="+mensa;break;
						case "2":
							document.getElementById('ventanam').src="ventana-mensaje3.php?titulos="+mensa;break;
						case "3":
							document.getElementById('ventanam').src="ventana-mensaje2.php?titulos="+mensa;break;
						case "4":
							document.getElementById('ventanam').src="ventana-consulta1.php?titulos="+mensa+"&idresp="+pregunta;break;
					document.getElementById('ventanam').src="ventana-consulta1.php?titulos="+mensa+"&idresp="+pregunta;break;
							document.getElementById('ventanam').src="ventana-consulta1.php?titulos="+mensa+"&idresp="+pregunta;break;
					}
				}
			}
			function despliegamodal2(_valor){
				document.getElementById("bgventanamodal2").style.visibility=_valor;
				if(_valor=="hidden"){
					document.getElementById('ventana2').src="";
				}else{
					document.getElementById('ventana2').src="cuentasppto-ventana2.php";
				}
			}
			function funcionmensaje(){}
			function respuestaconsulta(pregunta){
				switch(pregunta){
					case "1":	document.getElementById('oculto').value='2';document.form2.submit();break;
					case "2":	document.getElementById('oculto').value='6';
								document.form2.submit();
								break;
				}
			}
			function buscacuentap(){
				document.form2.buscap.value='1';
				document.form2.oculto.value='7';
				document.form2.submit();
			}
			function adelante(scrtop, numpag, limreg, filtro, next){
				var maximo=document.getElementById('maximo').value;
				var actual=document.getElementById('ids').value;
				if(parseFloat(maximo)>parseFloat(actual)){
					document.getElementById('oculto').value='1';
					document.getElementById('ids').value=next;
					var idcta=document.getElementById('codigo').value;
					document.form2.action="teso-editaretenciones.php?idcta="+idcta+"&scrtop="+scrtop+"&numpag="+numpag+"&limreg="+limreg+"&filtro="+filtro;
					document.form2.submit();
				}
			}
			function atrasc(scrtop, numpag, limreg, filtro, prev){
				var minimo = document.getElementById('minimo').value;
				var actual = document.getElementById('ids').value;
				if(parseFloat(minimo)<parseFloat(actual)){
					document.getElementById('oculto').value = '1';
					document.getElementById('ids').value = prev;
					var idcta=document.getElementById('codigo').value;
					document.form2.action="teso-editaretenciones.php?idcta="+idcta+"&scrtop="+scrtop+"&numpag="+numpag+"&limreg="+limreg+"&filtro="+filtro;
					document.form2.submit();
				}
			}
			function iratras(scrtop, numpag, limreg, filtro){
				var idcta = document.getElementById('codigo').value;
				location.href="teso-buscaretenciones.php?idcta="+idcta+"&scrtop="+scrtop+"&numpag="+numpag+"&limreg="+limreg+"&filtro="+filtro;
			}
		</script>
	</head>
	<body>
		<IFRAME src="alertas.php" name="alertas" id="alertas" style="display:none"></IFRAME>
		<span id="todastablas2"></span>
        <?php
			$numpag = $_GET['numpag'];
			$limreg = $_GET['limreg'];
			$scrtop = 26*$totreg;
			$vigusu = $vigencia = date('Y');
		?>
		<table>
			<tr><script>barra_imagenes("teso");</script><?php cuadro_titulos();?></tr>
			<tr><?php menu_desplegable("teso");?></tr>
			<tr>
				<td colspan="3" class="cinta">
					<img src="imagenes/add.png" title="Nuevo" onClick="location.href='teso-retenciones.php'" class="mgbt">
					<img src="imagenes/guarda.png" title="Guardar" onClick="guardar()" class="mgbt">
					<img src="imagenes/busca.png" title="Buscar" onClick="location.href='teso-buscaretenciones.php'" class="mgbt">
					<img src="imagenes/agenda1.png" title="Agenda" onClick="mypop=window.open('plan-agenda.php','','');mypop.focus()" class="mgbt">
					<img src="imagenes/nv.png" class="mgbt" title="Nueva ventana" onClick="mypop=window.open('teso-principal.php','','');mypop.focus();">
					<img src="imagenes/duplicar_pantalla.png" title="Duplicar pesta&ntilde;a" onClick="mypop=window.open('<?php echo $url2; ?>','','');mypop.focus();" class="tooltip bottom mgbt">
					<img src="imagenes/iratras.png" title="Atr&aacute;s" onClick="iratras(<?php echo $scrtop; ?>, <?php echo $numpag; ?>, <?php echo $limreg; ?>, <?php echo $filtro; ?>)" class="mgbt">
				</td>
			</tr>
		</tr>
			</tr>
		</table>
		<form name="form2" method="post" action="">
			<?php
				if ($_GET['is']!=""){
					echo "<script>document.getElementById('codrec').value=".$_GET['is'].";</script>";
				}
				$sqlr = "select MIN(id), MAX(id) from tesoretenciones ORDER BY id";
				$res = mysqli_query($linkbd,$sqlr);
				$r = mysqli_fetch_row($res);
				$_POST['minimo'] = $r[0];
				$_POST['maximo'] = $r[1];
				if($_POST['oculto']==""){
					if ($_POST['codrec']!="" || $_GET['is']!=""){
						if($_POST['codrec']!=""){
							$sqlr="select *from tesoretenciones where id='".$_POST['codrec']."'";
						}else {
							$sqlr="select *from tesoretenciones where id ='".$_GET['is']."'";
						}
					}else{
						$sqlr="select * from  tesoretenciones ORDER BY id DESC";
					}
					$res = mysqli_query($linkbd,$sqlr);
					$row = mysqli_fetch_row($res);
					$_POST['ids'] = $row[0];
				}
				function buscaDestino($destino){
					switch($destino){
						case "N":{
							return 'Nacional';
						}
						case 'M':{
							return 'Municipal';
						}
						case 'D':{
							return 'Departamental';
						}
					}
				}
				if(($_POST['oculto']!="2") && ($_POST['oculto']!="6") && ($_POST['oculto']!="7")){
					unset($_POST['dconceptos']);
					unset($_POST['dnconceptos']);
					$_POST['dconceptos']=array();
					$_POST['dnconceptos']=array();
					$_POST['conceptocausa']="";
					$_POST['nconceptocausa']="";
					$_POST['conceptoingreso']="";
					$_POST['nconceptoingreso']="";
					$_POST['conceptosgr']="";
					$_POST['nconceptosgr']="";
					$_POST['cuentap']="";
					$_POST['ncuentap']="";
					$sqlr="select *from tesoretenciones,tesoretenciones_det where  tesoretenciones.id=tesoretenciones_det.codigo and tesoretenciones.id=".$_POST['ids']."";
					$cont=0;
					$resp = mysqli_query($linkbd,$sqlr);
					while ($row = mysqli_fetch_assoc($resp)){
						$sqlr3 = "SELECT *FROM tesoretenciones_det_presu WHERE cod_presu='".$row["cod_presu"]."' AND vigencia='$vigusu'";
						$res3 = mysqli_query($linkbd,$sqlr3);
						$row3 = mysqli_fetch_assoc($res3);
						$_POST['codigo'] = $row["codigo"];
						$_POST['nombre'] = $row["nombre"];
						$_POST['nombre'] = $row["nombre"];
						$_POST['nombre'] = $row["nombre"];
						$_POST['tipo'] = $row["tipo"];
						$_POST['retencion'] = $row["retencion"];
						$_POST['terceros'] = $row["terceros"];
						$_POST['iva'] = $row["iva"];
						$_POST['tiporet'] = $row["destino"];
						$_POST['nomina'] = $row["nomina"];
						$sqlr1="Select * from conceptoscontables  where modulo='4' and tipo='RE' and codigo ='".$row["conceptocausa"]."' ";
						$resp1 = mysqli_query($linkbd,$sqlr1);
						$row1 = mysqli_fetch_assoc($resp1);
						$sqlr2 = "Select * from conceptoscontables  where modulo='4' and tipo='RI' and codigo ='".$row["conceptoingreso"]."' ";
						$resp2 = mysqli_query($linkbd,$sqlr2);
						$row2 = mysqli_fetch_assoc($resp2);
						$sqlr4 = "Select * from conceptoscontables  where modulo='4' and tipo='SR' and codigo ='".$row["conceptosgr"]."' ";
						$resp4 = mysqli_query($linkbd,$sqlr4);
						$row4 = mysqli_fetch_assoc($resp4);
						if($row["tipo"]=='C'){
							$_POST['dconceptocausa'][$cont]=$row["conceptocausa"];
							$_POST['dnconceptocausa'][$cont]=$row1["codigo"]." ".$row1["nombre"];
							$_POST['dconceptoingreso'][$cont]=$row["conceptoingreso"];
							$_POST['dnconceptoingreso'][$cont]=$row2["codigo"]." ".$row2["nombre"];
							$_POST['dconceptosgr'][$cont]=$row["conceptosgr"];
							$_POST['dnconceptosgr'][$cont]=$row4["codigo"]." ".$row4["nombre"];
							$_POST['dvalores'][$cont]=$row["porcentaje"];
							$_POST['tcuentas'][$cont]='N';
							$_POST['dcuentasp'][$cont]=$row3["cuentapres"];
							$_POST['dncuentasp'][$cont]=buscaNombreCuentaCCPET($row3["cuentapres"],1);
							$_POST['destinoRetencion'][$cont]=buscaDestino($row["destino"]);
						}
						if($row["tipo"]=='S')
						{
							$_POST['conceptocausa']=$row["conceptocausa"];
							$_POST['nconceptocausa']=$row1["codigo"]." ".$row1["nombre"];
							$_POST['conceptoingreso']=$row["conceptoingreso"];
							$_POST['nconceptoingreso']=$row2["codigo"]." ".$row2["nombre"];
							$_POST['cuentap']=$row3["cuentapres"];
							$_POST['ncuentap']=buscaNombreCuentaCCPET($row3["cuentapres"],1);
							$_POST['conceptosgr']=$row["conceptosgr"];
							$_POST['nconceptosgr']=$row4["codigo"]." ".$row4["nombre"];
						}
						$cont=$cont+1;
					}
					$sqlr = "SELECT * FROM tesoretenciones WHERE id='".$_POST['ids']."'";
					$cont=0;
					$resp = mysqli_query($linkbd, $sqlr);
					while ($row = mysqli_fetch_row($resp)){
						$_POST['codigo'] = $row[1];
						$_POST['nombre'] = $row[2];
						$_POST['tipo'] = $row[3];
						$_POST['retencion'] = $row[5];
						$_POST['terceros'] = $row[6];
						$_POST['iva'] = $row[7];
						$_POST['tiporet'] = $row[8];
						$_POST['nomina'] = $row[9];
						$_POST['rec_participa'] = $row[10];
					}
				}
				//NEXT
				$sqln = "SELECT * FROM tesoretenciones WHERE codigo > '".$_POST['codigo']."' ORDER BY codigo ASC LIMIT 1";
				$resn = mysqli_query($linkbd,$sqln);
				$row = mysqli_fetch_row($resn);
				$next = "'".$row[0]."'";
				//PREV
				$sqlp="SELECT * FROM tesoretenciones WHERE codigo < '".$_POST['codigo']."' ORDER BY codigo DESC LIMIT 1";
				$resp=mysqli_query($linkbd,$sqlp);
				$row=mysqli_fetch_row($resp);
				$prev="'".$row[0]."'";
			?>
			<div id="bgventanamodalm" class="bgventanamodalm">
				<div id="ventanamodalm" class="ventanamodalm">
					<IFRAME src="" name="ventanam" marginWidth=0 marginHeight=0 frameBorder=0 id="ventanam" frameSpacing=0 style=" width:700px; height:130px; top:200; overflow:hidden;">
					</IFRAME>
				</div>
			</div>
			<?php //**** busca cuentas
				if($_POST['bcp']!=''){
					$nresul = buscaNombreCuentaCCPET($_POST['cuentap'],1);
					if($nresul!=''){
						$_POST['ncuentap']=$nresul;
					}else{
						$_POST['ncuentap']="";
					}
				}
				if($_POST['bcp1']!=''){
					$_POST['ncuentap']="";
					$_POST['cuentap']="";
				}
				if($_POST['bc']!=''){
					$nresul=buscacuenta($_POST['cuenta']);
					if($nresul!=''){
						$_POST['ncuenta']=$nresul;
					}else{
						$_POST['ncuenta']="";
					}
				}
				if($_POST['bcc']!=''){
					$nresul=buscacuenta($_POST['cuentac']);
					if($nresul!='')	{
						$_POST['ncuentac']=$nresul;
					}else{
						$_POST['ncuentac']="";
					}
				}
			?>
			<table class="inicio ancho">
				<tr >
					<td class="titulos" colspan="14">Editar Retenciones Pagos</td>
					<td class="cerrar" style="width:7%" onClick="location.href='teso-principal.php'">Cerrar</td>
				</tr>
				<tr>
					<td style="width:1.7cm" class="textonew01">C&oacute;digo:</td>
					<td style="width:6%" >
							<img src="imagenes/back.png" class="icobut" title="Anterior" onClick="atrasc(<?php echo $scrtop; ?>, <?php echo $numpag; ?>, <?php echo $limreg; ?>, <?php echo $filtro; ?>, <?php echo $prev; ?>)">&nbsp;<input name="codigo" id="codigo" type="text" value="<?php echo $_POST['codigo']?>" maxlength="2" style="width:30%" onKeyPress="javascript:return solonumeros(event)" onKeyUp="return tabular(event,this)">&nbsp;<img src="imagenes/next.png" onClick="adelante(<?php echo $scrtop; ?>, <?php echo $numpag; ?>, <?php echo $limreg; ?>, <?php echo $filtro; ?>, <?php echo $next; ?>)" class="icobut" title="Sigiente">
						<input type="hidden" value="<?php echo $_POST['maximo']?>" name="maximo" id="maximo">
						<input type="hidden" value="<?php echo $_POST['minimo']?>" name="minimo" id="minimo">
						<input type="hidden" value="<?php echo $_POST['codrec']?>" name="codrec" id="codrec">
						<input type='hidden' name='ids' id='ids' value= "<?php echo $_POST['ids']?>">
					</td>
					<td style="width:2cm" class="textonew01">&nbsp;&nbsp;Nombre:</td>
					<td colspan="9"><input name="nombre" type="text" value="<?php echo $_POST['nombre']?>" style="width:100%" onKeyUp="return tabular(event,this)"/></td>
					<td style="width:3.3cm" class="textonew01">&nbsp;&nbsp;Valor Retenci&oacute;n:</td>
					<td style="width:10%">
						<input name="retencion" type="text" id="retencion" onKeyPress="javascript:return solonumeros(event)" onKeyUp="return tabular(event,this)" value="<?php echo $_POST['retencion']?>" style="width:70%" maxlength="5">%
					</td>
				</tr>
				<tr>
					<td class="textonew01">Tipo:</td>
					<td>
						<select name="tipo" id="tipo" onChange="validar()" >
							<option value="S" <?php if($_POST['tipo']=='S') echo "SELECTED"?>>Simple</option>
							<option value="C" <?php if($_POST['tipo']=='C') echo "SELECTED"?>>Compuesto</option>
						</select>
						<input name="oculto" id="oculto" type="hidden" value="1">
					</td>
					<td class="textonew01">&nbsp;&nbsp;Terceros:</td>
					<td>
						<?php
							if ($_POST['terceros']=='1'){$chk='checked';}
							else {$chk='';}
						?>
						<input name="terceros" type="checkbox" value="1" onClick="validar();" <?php echo $chk ?> >
					</td>
					<td class="textonew01" style="width:1cm">IVA:</td>
					<td>
						<?php
							if ($_POST['iva']=='1'){$chk2='checked';}
							else {$chk2='';}
						?>
						<input name="iva" type="checkbox" value="1" <?php echo $chk2 ?>>
					</td>
					<td class="textonew01" style="width:1.7cm">N&oacute;mina:</td>
					<td>
						<?php
							if ($_POST['nomina']=='1'){$chk3='checked';}
							else {$chk3='';}
						?>
						<input type="checkbox" name="nomina" value="1" <?php echo $chk3 ?>>
					</td>
					<td class="textonew01" style="width:1.7cm">Destino:</td>
					<td style="width: 10%;">
						<select name="tiporet" id="tiporet" onChange="validar()" style="width: 100%;">
							<option value="" >Otros</option>
							<option value="N" <?php if($_POST['tiporet']=='N') echo "SELECTED"?>>Nacional</option>
							<option value="D" <?php if($_POST['tiporet']=='D') echo "SELECTED"?>>Departamental</option>
							<option value="M" <?php if($_POST['tiporet']=='M') echo "SELECTED"?>>Municipal</option>
						</select>
					</td>
					<?php
						if($_POST['tiporet']=='D') {
							echo "
							<td class='textonew01' style='width:4.8cm'>&nbsp;&nbsp;&nbsp;Reconoce Participaci&oacute;n:</td>
							<td>
								<select name='rec_participa' id='rec_participa' onChange='validar()'>
									<option value='' >Seleccione...</option>
									<option value='S'"; if($_POST['rec_participa']=='S') echo "SELECTED"; echo ">SI</option>
									<option value='N'"; if($_POST['rec_participa']=='N') echo "SELECTED"; echo ">NO</option>
								</select>
							</td>";
						}else{
							echo "
							<td style='width:4.8cm'></td>
							<td></td>";
						}
					?>
				</tr>
			</table>
			<?php
				if($_POST['tipo']=='S') //***** SIMPLE
				{
					?>
					<table class='inicio ancho'>
						<tr><td colspan='6' class='titulos'>Detalle Retencion Pago</td></tr>
						<tr>
							<td style='width:18%;'class='textonew01'>Concepto Contable Causaci&oacute;n:</td>
							<td style='width:40%;' colspan="2">
								<select name='conceptocausa' id='conceptocausa' style='width:100%;'>
									<option value='-1'>Seleccione ....</option>
									<?php
										$sqlr="Select * from conceptoscontables where modulo='4' and tipo='RE' order by codigo";
										$resp = mysqli_query($linkbd,$sqlr);
										while ($row =mysqli_fetch_row($resp)){
											if($row[0] == $_POST['conceptocausa']){
												echo "<option value='$row[0]' SELECTED>$row[0] - $row[3] - $row[1]</option>";
												$_POST['nconceptocausa'] = $row[0]." - ".$row[3]." - ".$row[1];
											}else{
												echo "<option value='$row[0]'>$row[0] - $row[3] - $row[1]</option>";
											}
										}
									?>
								</select>
								<input type="hidden" id="nconceptocausa" name="nconceptocausa" value="<?php echo $_POST['nconceptocausa']?>">
							</td>
							<td></td>
							<td></td>
						</tr>
						<tr>
							<td class="textonew01">Concepto Contable Ingresos:</td>
							<td colspan="2">
								<select name="conceptoingreso" id="conceptoingreso" style="width:100%;">
									<option value="-1">Seleccione ....</option>
									<?php
										$sqlr="Select * from conceptoscontables where modulo='4' and tipo='RI' order by codigo";
										$resp = mysqli_query($linkbd,$sqlr);
										while ($row = mysqli_fetch_row($resp)){
											if($row[0] == $_POST['conceptoingreso']){
												echo "<option value='$row[0]' SELECTED>$row[0] - $row[3] - $row[1]</option>";
												$_POST['nconceptoingreso']=$row[0]." - ".$row[3]." - ".$row[1];
											}else{
												echo "<option value='$row[0]'>$row[0] - $row[3] - $row[1]</option>";
											}
										}
									?>
								</select>
								<input type="hidden" id="nconceptoingreso" name="nconceptoingreso" value="<?php echo $_POST['nconceptoingreso']?>" >
							</td>
							<td></td>
							<td></td>
						</tr>
						<tr>
							<td class="textonew01">Concepto Contable SGR:</td>
							<td colspan="2">
								<select name="conceptosgr" id="conceptosgr" style="width:100%;">
									<option value="-1">Seleccione ....</option>
										<?php
											$sqlr="Select * from conceptoscontables where modulo='4' and tipo='SR' order by codigo";
											$resp = mysqli_query($linkbd,$sqlr);
											while ($row =mysqli_fetch_row($resp)){
												if($row[0] == $_POST['conceptosgr']){
													echo "<option value='$row[0]' SELECTED>$row[0] - $row[3] - $row[1]</option>";
													$_POST['nconceptosgr'] = $row[0]." - ".$row[3]." - ".$row[1];
												}else{
													echo "<option value='$row[0]'>$row[0] - $row[3] - $row[1]</option>";
												}
											}
										?>
								</select>
								<input id="nconceptosgr" name="nconceptosgr" type="hidden" value="<?php echo $_POST['nconceptosgr']?>" >
							</td>
							<td></td>
							<td></td>
						</tr>
						<tr>
							<td style="width:10%;" class='textonew01'>Cuenta presupuestal: </td>
							<td style="width:10%;" >
								<input type="text" id="cuentap" name="cuentap" style="width:100%" onKeyPress="javascript:return solonumeros(event)" onKeyUp="return tabular(event,this)" onChange="buscactap(event)" value="<?php echo $_POST['cuentap']?>" class='colordobleclik' autocomplete='off' onDblClick="despliegamodal2('visible');" title="Cuentas presupuestales">
							</td>
							<input type="hidden" value="" name="bcp" id="bcp">
							<input type="hidden" value="" name="bcp1" id="bcp1">
							<td ><input name="ncuentap" type="text" style="width:100%;" value="<?php echo $_POST['ncuentap']?>" readonly></td>
							<td></td>
						</tr>
					</table>
					<?php
				}
				if($_POST['tipo']=='C') //**** COMPUESTO
				{
					?>
					<table class='inicio ancho'>
						<tr><td colspan="4" class="titulos">Agregar Detalle Retencion Pago</td></tr>
						<tr>
							<td style="width:18%;" class="textonew01">Concepto Contable Causacion:</td>
							<td style="width:40%;" colspan="2">
								<select name="conceptocausa" id="conceptocausa" style="width:100%;">
									<option value="-1">Seleccione ....</option>
										<?php
											$sqlr="Select * from conceptoscontables where modulo='4' and tipo='RE' order by codigo";
											$resp = mysqli_query($linkbd,$sqlr);
											while ($row = mysqli_fetch_row($resp))
											{
												echo "";
												if($row[0]==$_POST['conceptocausa'])
												{
													echo "<option value='$row[0]' SELECTED>$row[0] - $row[3] - $row[1]</option>";
													$_POST['nconceptocausa']=$row[0]." - ".$row[3]." - ".$row[1];
												}else{
													echo "<option value='$row[0]'>$row[0] - $row[3] - $row[1]</option>";
												}
											}
										?>
								</select>
								<input id="nconceptocausa" name="nconceptocausa" type="hidden" value="<?php echo $_POST['nconceptocausa']?>" >
							</td>
							<td></td>
						</tr>
						<tr>
							<td class="textonew01">Concepto Contable Ingresos:</td>
							<td style="width:40%;" colspan="2">
								<select name="conceptoingreso" id="conceptoingreso" style="width:100%;">
									<option value="-1">Seleccione ....</option>
										<?php
											$sqlr="Select * from conceptoscontables where modulo='4' and tipo='RI' order by codigo";
											$resp = mysqli_query($linkbd,$sqlr);
											while ($row = mysqli_fetch_row($resp)){
												if($row[0]==$_POST['conceptoingreso'])
												{
													echo "<option value='$row[0]' SELECTED>$row[0] - $row[3] - $row[1]</option>";
													$_POST['nconceptoingreso']=$row[0]." - ".$row[3]." - ".$row[1];
												}else{
													echo "<option value='$row[0]'>$row[0] - $row[3] - $row[1]</option>";
												}
											}
										?>
								</select>
								<input id="nconceptoingreso" name="nconceptoingreso" type="hidden" value="<?php echo $_POST['nconceptoingreso']?>">
							</td>
							<td></td>
						</tr>
						<tr>
							<td class="textonew01">Concepto Contable SGR:</td>
							<td style="width:40%;" colspan="2">
								<select name="conceptosgr" id="conceptosgr" style="width:100%;">
									<option value="-1">Seleccione ....</option>
										<?php
											$sqlr="Select * from conceptoscontables where modulo='4' and tipo='SR' order by codigo";
											$resp = mysqli_query($linkbd,$sqlr);
											while ($row = mysqli_fetch_row($resp)){
												if($row[0] == $_POST['conceptosgr']){
													echo "<option value='$row[0]' SELECTED>$row[0] - $row[3] - $row[1]</option>";
													$_POST['nconceptosgr']=$row[0]." - ".$row[3]." - ".$row[1];
												}else{
													echo "<option value='$row[0]'>$row[0] - $row[3] - $row[1]</option>";
												}
											}
										?>
								</select>
								<input id="nconceptosgr" name="nconceptosgr" type="hidden" value="<?php echo $_POST['nconceptosgr']?>" >
							</td>
							<td></td>
						</tr>
						<tr>
							<td class="textonew01">Cuenta presupuestal: </td>
							<td style="width:10%;">
								<input type="text" style="width:100%;" id="cuentap" name="cuentap" onKeyPress="javascript:return solonumeros(event)" onKeyUp="return tabular(event,this)" onchange="buscactap(event)" value="<?php echo $_POST['cuentap']?>" class='colordobleclik' autocomplete='off' onDblClick="despliegamodal2('visible');" title="Cuentas presupuestales">
								<input type="hidden" value="" name="bcp" id="bcp">
								<input type="hidden" value="" name="bcp1" id="bcp1">
							</td>
							<td>
								<input name="ncuentap" style="width:100%;" type="text" value="<?php echo $_POST['ncuentap']?>" readonly>
							</td>
						</tr>
						<tr>
							<td class="textonew01">Division Retencion:</td>
							<td style="width:10%;">
								<input id="valor" style="width:80%;" name="valor" type="text" value="<?php echo $_POST['valor']?>" onKeyUp="return tabular(event,this)" onKeyPress="javascript:return solonumeros(event)" > %

								<input type="hidden" value="0" name="agregadet">
								<input type="hidden" value="<?php echo $_POST['id']?>" name="id">
								<input type="hidden" value="0" name="buscap">
							</td>
							<td>
								<select name="tiporet2" id="tiporet2" onChange="validar()" >
									<option value="" >Seleccione...</option>
									<option value="N" <?php if($_POST['tiporet2']=='N') echo "SELECTED"?>>Nacional</option>
									<option value="D" <?php if($_POST['tiporet2']=='D') echo "SELECTED"?>>Departamental</option>
									<option value="M" <?php if($_POST['tiporet2']=='M') echo "SELECTED"?>>Municipal</option>
								</select>
								<em class="botonflechaverde" onClick="agregardetalle();">Agregar</em>
							</td>
						</tr>
					</table>
					<?php
						//**** busca cuenta
						if($_POST['bc']!=''){
							$nresul = buscacuenta($_POST['cuenta']);
							if($nresul != ''){
								$_POST['ncuenta'] = $nresul;
								echo"
								<script>
									document.getElementById('cuentap').focus();
									document.getElementById('cuentap').select();
									document.getElementById('bc').value='';
								</script>";
							}else{
								$_POST['ncuenta']="";
								echo"<script>alert('Cuenta Incorrecta');document.form2.cuenta.focus();</script>";
							}
						}
						if($_POST['bcc'] != ''){
							$nresul = buscacuenta($_POST['cuentac']);
							if($nresul != ''){
								$_POST['ncuentac'] = $nresul;
								echo"
								<script>
									document.getElementById('cuenta').focus();
									document.getElementById('cuenta').select();
									document.getElementById('bcc').value='';
								</script>";
							}else{
								$_POST['ncuentac']="";
								echo"<script>alert('Cuenta Incorrecta');document.form2.cuentac.focus();</script>";
							}
						}
						//**** busca cuenta
						if($_POST['bcp']!=''){
							$nresul=buscaNombreCuentaCCPET($_POST['cuentap'],1);
							if($nresul!=''){
								$_POST['ncuentap']=$nresul;
								echo"
								<script>
									document.getElementById('codigo').focus();
									document.getElementById('codigo').select();
									document.getElementById('bcp').value='';
								</script>";
							}else{
								$_POST['ncuentap']="";
								echo"
								<script>
									alert('Cuenta Incorrecta');
									document.form2.cuentap.focus();
									document.form2.cuentap.select();
								</script>";
							}
						}
					?>
					<table class='tablamv'>
						<thead>
							<tr>
								<th class="titulos" colspan="8">Detalle Retencion Pago</th>
							</tr>
							<tr>
								<th class="titulos2" style='width:20%;'>Concepto Contable Causacion</th>
								<th class="titulos2" style='width:20%;'>Concepto Contable Ingreso</th>
								<th class="titulos2" style='width:19%;'>Concepto Contable SGR:</th>
								<th class="titulos2" style='width:8%;'>Cta Pres</th>
								<th class="titulos2" style='width:17%;'>Nombre Cta Pres</th>
								<th class="titulos2" style='width:10%;'>Destino</th>
								<th class="titulos2" style='width:3%;'>%</th>
								<th class="titulos2" >
									<img src="imagenes/del.png" >
									<input type='hidden' name='elimina' id='elimina'>
								</th>
							</tr>
						</thead>
						<tbody style="max-height: 17vh;">
							<?php
								if ($_POST['elimina']!=''){
									$posi=$_POST['elimina'];
									unset($_POST['tcuentas'][$posi]);
									unset($_POST['dconceptocausa'][$posi]);
									unset($_POST['dnconceptocausa'][$posi]);
									unset($_POST['dconceptoingreso'][$posi]);
									unset($_POST['dnconceptoingreso'][$posi]);
									unset($_POST['dconceptosgr'][$posi]);
									unset($_POST['dnconceptosgr'][$posi]);
									unset($_POST['dcuentasp'][$posi]);
									unset($_POST['dncuentasp'][$posi]);
									unset($_POST['dvalores'][$posi]);
									unset($_POST['destinoRetencion'][$posi]);
									$_POST['tcuentas']= array_values($_POST['tcuentas']);
									$_POST['dconceptocausa']= array_values($_POST['dconceptocausa']);
									$_POST['dnconceptocausa']= array_values($_POST['dnconceptocausa']);
									$_POST['dconceptoingreso']= array_values($_POST['dconceptoingreso']);
									$_POST['dnconceptoingreso']= array_values($_POST['dnconceptoingreso']);
									$_POST['dconceptosgr']= array_values($_POST['dconceptosgr']);
									$_POST['dnconceptosgr']= array_values($_POST['dnconceptosgr']);
									$_POST['dcuentasp']= array_values($_POST['dcuentasp']);
									$_POST['dncuentasp']= array_values($_POST['dncuentasp']);
									$_POST['dvalores']= array_values($_POST['dvalores']);
									$_POST['destinoRetencion']= array_values($_POST['destinoRetencion']);
								}
								if ($_POST['agregadet']=='1'){
									$cuentacred=0;
									$cuentadeb=0;
									$diferencia=0;
									$_POST['tcuentas'][] = 'N';
									$_POST['dconceptocausa'][] = $_POST['conceptocausa'];
									$_POST['dnconceptocausa'][] = $_POST['nconceptocausa'];
									$_POST['dconceptoingreso'][]= $_POST['conceptoingreso'];
									$_POST['dnconceptoingreso'][] = $_POST['nconceptoingreso'];
									$_POST['dconceptosgr'][] = $_POST['conceptosgr'];
									$_POST['dnconceptosgr'][]= $_POST['nconceptosgr'];
									$_POST['dcuentasp'][] = $_POST['cuentap'];
									$_POST['dncuentasp'][] = $_POST['ncuentap'];
									$_POST['dvalores'][] = $_POST['valor'];
									$_POST['destinoRetencion'][] = buscaDestino($_POST['tiporet2']);
									$_POST['agregadet'] = 0;
									echo"
									<script>
										document.form2.conceptocausa.value='';
										document.form2.nconceptocausa.value='';
										document.form2.conceptoingreso.value='';
										document.form2.nconceptoingreso.value='';
										document.form2.conceptosgr.value='';
										document.form2.nconceptosgr.value='';
										document.form2.ncuentac.value='';
										document.form2.ncuentap.value='';
										document.form2.valor.value='';
										document.form2.tiporet.value='';
										document.form2.submit();
									</script>";
								}
								if($_POST['buscap']=='1'){
									for($x=0;$x<count($_POST['dcuentasp']);$x++){
										$sqlr = "SELECT nombre FROM pptocuentas WHERE cuenta='".$_POST['dcuentasp'][$x]."' AND vigencia='$vigusu'";
										$res = mysqli_query($linkbd,$sqlr);
										$row = mysqli_fetch_row($res);
										$_POST['dncuentasp'][$x] = $row[0];
									}
								}
								for ($x=0;$x< count($_POST['dconceptocausa']);$x++){
									echo "
										<input type='hidden' name='dconceptocausa[]' value='".$_POST['dconceptocausa'][$x]."'>
										<input type='hidden' name='dnconceptocausa[]' value='".$_POST['dnconceptocausa'][$x]."'>
										<input type='hidden' name='dconceptoingreso[]' value='".$_POST['dconceptoingreso'][$x]."'>
										<input type='hidden' name='dconceptosgr[]' value='".$_POST['dconceptosgr'][$x]."'>
										<input type='hidden' name='dnconceptoingreso[]' value='".$_POST['dnconceptoingreso'][$x]."'>
										<input type='hidden' name='dnconceptosgr[]' value='".$_POST['dnconceptosgr'][$x]."'>
										<input type='hidden' name='dcuentasp[]' value='".$_POST['dcuentasp'][$x]."'>
										<input type='hidden' name='dncuentasp[]' value='".$_POST['dncuentasp'][$x]."'>
										<input type='hidden' name='destinoRetencion[]' value='".$_POST['destinoRetencion'][$x]."'>
										<input type='hidden' name='dvalores[]' value='".$_POST['dvalores'][$x]."'>
										<tr class='saludo2'>
											<td style='width:20%;'>".$_POST['dnconceptocausa'][$x]."</td>
											<td style='width:20%;'>".$_POST['dnconceptoingreso'][$x]."</td>
											<td style='width:20%;'>".$_POST['dnconceptosgr'][$x]."</td>
											<td style='width:8%;'>".$_POST['dcuentasp'][$x]."</td>
											<td style='width:17%;'>".$_POST['dncuentasp'][$x]."</td>
											<td style='width:10%;'>".$_POST['destinoRetencion'][$x]."</td>
											<td style='width:3%;'>".$_POST['dvalores'][$x]."</td>
											<td>
												<a href='#' onclick='eliminar($x)'>
													<img src='imagenes/del.png'>
												</a>
											</td>
										</tr>";
								}
							?>
						</tbody>
					</table>
					<?php
				}
				if($_POST['oculto'] == '2'){
					$sumaTotal = 0;
					$evaluar = 0;
					for ($x=0;$x< count($_POST['dconceptocausa']);$x++){
						$sumaCausa = 0;
						$sumaSgr = 0;
						$valorRete = $_POST['dvalores'][$x];
						if($_POST['dconceptocausa'][$x]!='-1'){
							$sumaCausa += $valorRete;
							$valorRete=0;
						}
						if($_POST['dconceptosgr'][$x]!='-1'){
							$sumaSgr += $valorRete;
							$valorRete=0;
							$evaluar=1;
						}
						$sumaTotal += $sumaCausa + $valorRete;
						$sumaTotalSgr += $sumaSgr + $valorRete;
					}
					$sumaT = 0;
					if($evaluar==1){
						$sumaT = $sumaTotalSgr;
					}else{
						$sumaT = 100;
					}
					//**** simple
					if($_POST['tipo']=='S'){
						$sumaTotal='100';
						$sumaT='100';
					}

					$nr="1";
					$errores = 0;
					if($_POST['tiporet']=='D'){
						$siparticipacion = $_POST['rec_participa'];
					}else{
						$siparticipacion = "";
					}
                    if($_POST['nomina'] == ''){
                        $_POST['nomina'] = '0';
                    }
					$sqlr = "UPDATE tesoretenciones set codigo = '".$_POST['codigo']."', nombre = '".$_POST['nombre']."', tipo = '".$_POST['tipo']."', estado = 'S', retencion = '".$_POST['retencion']."', terceros = '".$_POST['terceros']."', iva = '".$_POST['iva']."',destino = '".$_POST['tiporet']."', nomina = '".$_POST['nomina']."', participacion = '$siparticipacion' WHERE id = '".$_POST['ids']."'";
					if (!mysqli_query($linkbd,$sqlr)){
						echo"
						<script>
							Swal.fire({
								icon: 'error',
								title: 'Error!',
								text: 'No se pudo almacenar la información en la tabla principal:',
								confirmButtonText: 'Continuar',
								confirmButtonColor: '#FF121A',
								timer: 2500
							});
						</script";
					}else{
						$sq="SELECT max(cod_presu) FROM tesoretenciones_det";
						$rs=mysqli_query($linkbd,$sq);
						$rw=mysqli_fetch_row($rs);
						//**** simple
						if($_POST['tipo']=='S'){
							$rw[0]+=1;
							$sqlr="delete from tesoretenciones_det where tesoretenciones_det.codigo='$_POST[ids]'";
							mysqli_query($linkbd,$sqlr);
							$sqlr="delete from tesoretenciones_det_presu where tesoretenciones_det_presu.codigo='$_POST[ids]' and vigencia='$vigusu'";
							mysqli_query($linkbd,$sqlr);
							$sqlr="INSERT INTO tesoretenciones_det (codigo,cod_presu,conceptocausa,conceptoingreso,modulo,tipoconce,porcentaje,estado,conceptosgr)VALUES ('$_POST[ids]','$rw[0]','".$_POST['conceptocausa']."','".$_POST['conceptoingreso']."','4','RE-RI','100','S','".$_POST['conceptosgr']."')";
							if (!mysqli_query($linkbd,$sqlr)){
								$errores++;
							}
							$id = mysqli_insert_id($linkbd);
							if($_POST['cuentap']!=""){
								$sqlr="INSERT INTO tesoretenciones_det_presu (id_retencion,codigo,cod_presu,cuentapres,vigencia)VALUES ('$id','$_POST[ids]','$rw[0]','".$_POST['cuentap']."','$vigusu')";
								if (!mysqli_query($linkbd,$sqlr)){
									$errores++;
								}
							}
						}
						//****COMPUESTO
						if($_POST['tipo']=='C'){
							$sqlr="delete from tesoretenciones_det where tesoretenciones_det.codigo='$_POST[ids]'";
							if (!mysqli_query($linkbd,$sqlr)){
								$errores++;
							}
							$sqlr="delete from tesoretenciones_det_presu where tesoretenciones_det_presu.codigo='$_POST[ids]' and vigencia='$vigusu'";
							if (!mysqli_query($linkbd,$sqlr)){
								$errores++;
							}else{
								for($x=0;$x<count($_POST['dconceptocausa']);$x++)
								{
									$rw[0]+=1;
									$sqlr = "INSERT INTO tesoretenciones_det (codigo,cod_presu,conceptocausa,conceptoingreso,modulo,tipoconce,porcentaje,estado,conceptosgr,destino)VALUES ('$_POST[ids]','$rw[0]','".$_POST['dconceptocausa'][$x]."','".$_POST['dconceptoingreso'][$x]."','4','RE-RI','".$_POST['dvalores'][$x]."','S','".$_POST['dconceptosgr'][$x]."','".$_POST['destinoRetencion'][$x]."')";
									if (!mysqli_query($linkbd,$sqlr)){
										$errores++;
									}
									$id = mysqli_insert_id($linkbd);
									if($_POST['dcuentasp'][$x]!=""){
										$sqlr = "INSERT INTO tesoretenciones_det_presu (id_retencion,codigo,cod_presu,cuentapres,vigencia)VALUES ('$id','$_POST[ids]','$rw[0]','".$_POST['dcuentasp'][$x]."','$vigusu')";
										if (!mysqli_query($linkbd,$sqlr)){
											$errores++;
										}
									}
								}//***** fin del for
							}
						}
						if ($errores == 1){
							$desupdate = "Se actualizo la información de la retención con un error";
						}else{
							$desupdate = "Se actualizo la información de la retención con $errores errores";
						}
						echo"
						<script>
							Swal.fire({
								icon: 'success',
								title: '$desupdate',
								confirmButtonText: 'Continuar',
								confirmButtonColor: '#FF121A',
								timer: 3000
							});
						</script>";
					}
				}
			?>
			<div id="bgventanamodal2">
				<div id="ventanamodal2">
					<IFRAME  src="" name="buster" marginWidth=0 marginHeight=0 frameBorder=0 id="ventana2" frameSpacing=0 style="left:500px; width:900px; height:500px; top:200;">
					</IFRAME>
				</div>
			</div>
    	</form>
	</body>
</html>
