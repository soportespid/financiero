<?php
	header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=utf8");
	require "comun.inc";
	require "funciones.inc";
	session_start();
	$linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");
	cargarcodigopag($_GET['codpag'],$_SESSION["nivel"]);
	date_default_timezone_set("America/Bogota");
	$scroll=$_GET['scrtop'];
	$totreg=$_GET['totreg'];
	$idcta=$_GET['idcta'];
	$altura=$_GET['altura'];
	$filtro="'".$_GET['filtro']."'";
?>
<!DOCTYPE >
<html lang="es">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1">
		<meta name="viewport" content="user-scalable=no">
		<title>:: IDEAL 10 - Parametrización</title>
		<link href="favicon.ico" rel="shortcut icon"/>
		<link href="css/css2z.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet"/>
		<link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet"/>
		<link href="css/css4.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet"/>
		<link href="sweetalert2/dist/sweetalert2.min.css" rel="stylesheet">
		<script src="sweetalert2/dist/sweetalert2.min.js"></script>
		<script type="text/javascript" src="css/programas.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script type="text/javascript" src="JQuery/jquery-2.1.4.min.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script>
			function validar(){
				document.getElementById('oculto').value='7';
				document.form2.submit();
			}

			function guardar(){
				if (document.form2.codigo.value!='' && document.form2.nombre.value!=''){
					Swal.fire({
						icon: 'question',
						title: '¿Esta seguro de modificar los cambios?',
						showDenyButton: true,
						confirmButtonText: 'Guardar',
						confirmButtonColor: '#01CC42',
						denyButtonText: 'Cancelar',
						denyButtonColor: '#FF121A',
					}).then(
						(result) => {
							if (result.isConfirmed){
								document.getElementById('oculto').value = '2';
								document.form2.submit();
							}
							else if (result.isDenied){
								Swal.fire({
									icon: 'info',
									title: 'No se modifico la información',
									confirmButtonText: 'Continuar',
									confirmButtonColor: '#FF121A',
									timer: 2500
								});
							}
						}
					)
				}
				else{
					Swal.fire({
						icon: 'error',
						title: 'Error!',
						text: 'Faltan información para dejar modificar',
						confirmButtonText: 'Continuar',
						confirmButtonColor: '#FF121A',
						timer: 2500
					});
				}
			}

			function adelante(scrtop, numpag, limreg, filtro, next){
				var maximo = document.getElementById('maximo').value;
				var actual = document.getElementById('codigo').value;
				if(parseFloat(maximo) > parseFloat(actual)){
					document.getElementById('oculto').value = '1';
					document.getElementById('codigo').value = next;
					var idcta = document.getElementById('codigo').value;
					document.form2.action = "teso-editasanciones.php?idcta="+idcta+"&scrtop="+scrtop+"&numpag="+numpag+"&limreg="+limreg+"&filtro="+filtro;
					document.form2.submit();
				}
			}
		
			function atrasc(scrtop, numpag, limreg, filtro, prev){
				var minimo = document.getElementById('minimo').value;
				var actual = document.getElementById('codigo').value;
				if(parseFloat(minimo) < parseFloat(actual)){
					document.getElementById('oculto').value = '1';
					document.getElementById('codigo').value = prev;
					var idcta = document.getElementById('codigo').value;
					document.form2.action = "teso-editasanciones.php?idcta="+idcta+"&scrtop="+scrtop+"&numpag="+numpag+"&limreg="+limreg+"&filtro="+filtro;
					document.form2.submit();
				}
			}
		
			function iratras(scrtop, numpag, limreg, filtro){
				var idcta = document.getElementById('codigo').value;
				location.href = "teso-buscasanciones.php?idcta="+idcta+"&scrtop="+scrtop+"&numpag="+numpag+"&limreg="+limreg+"&filtro="+filtro;
			}
		</script>
	</head>
	<body>
		<IFRAME src="alertas.php" name="alertas" id="alertas" style="display:none"></IFRAME>
		<span id="todastablas2"></span>
		<?php
			$numpag = $_GET['numpag'];
			$limreg = $_GET['limreg'];
			$scrtop = 26*$totreg;
			$vigencia = date('Y');
		?>
		<table>
			<tr><script>barra_imagenes("para");</script><?php cuadro_titulos();?></tr>	 
			<tr><?php menu_desplegable("para");?></tr>
			<tr>
				<td colspan="3" class="cinta">
				<a href="teso-sanciones.php" class="mgbt"><img src="imagenes/add.png" title="Nuevo"/></a> 
				<a onClick="guardar();" class="mgbt"><img src="imagenes/guarda.png"  title="Guardar"/></a>
				<a href="teso-buscasanciones.php" class="mgbt"> <img src="imagenes/busca.png"  title="Buscar" /></a>
				<a onClick="mypop=window.open('plan-agenda.php','','');mypop.focus()" class="mgbt"><img src="imagenes/agenda1.png" title="Agenda"/></a>
				<a onClick="mypop=window.open('para-principal.php','','');mypop.focus();" class="mgbt"><img src="imagenes/nv.png" title="Nueva ventana"></a>
				<a onClick="mypop=window.open('<?php echo $url2; ?>','','');mypop.focus();" class="tooltip bottom mgbt"><img src="imagenes/duplicar_pantalla.png" title="Duplicar pesta&ntilde;a"></a>
				<a onClick="iratras(<?php echo $scrtop; ?>, <?php echo $numpag; ?>, <?php echo $limreg; ?>, <?php echo $filtro; ?>)" class="mgbt"><img src="imagenes/iratras.png" title="Atr&aacute;s"></a></td>
			</tr>		  
		</table>
		<form name="form2" method="post" action="">
			<?php
				if ($_GET['idr']!=""){
					echo "<script>document.getElementById('codrec').value = $_GET[idr];</script>";
				}
				$sqlr = "SELECT  MIN(CONVERT(id_sancion, SIGNED INTEGER)), MAX(CONVERT(id_sancion, SIGNED INTEGER)) FROM tesosanciones ORDER BY CONVERT(id_sancion, SIGNED INTEGER)";
				$res = mysqli_query($linkbd,$sqlr);
				$r = mysqli_fetch_row($res);
				$_POST['minimo'] = $r[0];
				$_POST['maximo'] = $r[1];
				if($_POST['oculto'] == ""){
					if ($_POST['codrec'] != "" || $_GET['idr'] != ""){
						if($_POST['codrec'] != ""){
							$sqlr="SELECT * FROM tesosanciones WHERE id_sancion = '".$_POST['codrec']."'";
						}
						else{
							$sqlr = "SELECT * FROM tesosanciones WHERE id_sancion = '".$_GET['idr']."'";
						}
					}
					else{
						$sqlr="SELECT * FROM  tesosanciones ORDER BY CONVERT(id_sancion, SIGNED INTEGER) DESC";
					}
					$res = mysqli_query($linkbd,$sqlr);
					$row = mysqli_fetch_row($res);
					$_POST['codigo'] = $row[0];
				}
				if(($_POST['oculto'] != "2")&&($_POST['oculto']!="7")){	
					$fec = date("d/m/Y");
					$_POST['fecha'] = $fec; 	
					$_POST['valoradicion'] = 0;
					$_POST['valorreduccion'] = 0;
					$_POST['valortraslados'] = 0;		 		  			 
					$_POST['valor'] = 0;
					$sqlr="SELECT * FROM tesosanciones WHERE id_sancion=".$_POST['codigo'];
					$res = mysqli_query($linkbd,$sqlr);
					$row = mysqli_fetch_row($res);
					$_POST['codigo'] = $row[0];
					$_POST['nombre'] = $row[1];
					$_POST['valor'] = $row[4];	
				}
				//NEXT
				$sqln = "SELECT * FROM tesosanciones WHERE id_sancion > '".$_POST['codigo']."' ORDER BY id_sancion ASC LIMIT 1";
				$resn = mysqli_query($linkbd,$sqln);
				$row = mysqli_fetch_row($resn);
				$next = "'".$row[0]."'";
				//PREV
				$sqlp = "SELECT * FROM tesosanciones WHERE id_sancion < '".$_POST['codigo']."' ORDER BY id_sancion DESC LIMIT 1";
				$resp = mysqli_query($linkbd,$sqlp);
				$row = mysqli_fetch_row($resp);
				$prev = "'".$row[0]."'";
			?>
			<div id="bgventanamodalm" class="bgventanamodalm">
				<div id="ventanamodalm" class="ventanamodalm">
					<IFRAME src="" name="ventanam" marginWidth=0 marginHeight=0 frameBorder=0 id="ventanam" frameSpacing=0 style=" width:700px; height:130px; top:200; overflow:hidden;"> 
					</IFRAME>
				</div>
			</div>
			<table class="inicio ancho">
				<tr >
					<td class="titulos" colspan="6">Edita Sanción </td>
					<td class="cerrar" style="width:7%" onClick="location.href='teso-principal.php'">Cerrar</td>
				</tr>
				<tr >
					<td class="tamano01" style="width:2cm">Código:</td>
					<td style="width:10%">
						<a href="#" onClick="atrasc(<?php echo $scrtop; ?>, <?php echo $numpag; ?>, <?php echo $limreg; ?>, <?php echo $filtro; ?>, <?php echo $prev; ?>)"><img src="imagenes/back.png" title="anterior" ></a> 
						<input name="codigo" id='codigo' type="text" value="<?php echo $_POST['codigo']?>" style="width:70%" onKeyUp="return tabular(event,this)" readonly>        
						<a href="#" onClick="adelante(<?php echo $scrtop; ?>, <?php echo $numpag; ?>, <?php echo $limreg; ?>, <?php echo $filtro; ?>, <?php echo $next; ?>)"><img src="imagenes/next.png" title="siguiente"></a> 
						<input type="hidden" value="<?php echo $_POST['maximo']?>" name="maximo" id="maximo">
						<input type="hidden" value="<?php echo $_POST['minimo']?>" name="minimo" id="minimo">
						<input type="hidden" value="<?php echo $_POST['codrec']?>" name="codrec" id="codrec">
					</td>
					<td class="tamano01" style="width:2cm">Nombre:</td>
					<td style="width:40%"><input name="nombre" type="text" value="<?php echo $_POST['nombre']?>" style="width:100%" onKeyUp="return tabular(event,this)"> </td>
					<td class="tamano01" style="width:2cm">Porcentaje:</td>
					<td><input id="valor" name="valor" type="text" value="<?php echo $_POST['valor']?>" onKeyUp="return tabular(event,this)"  onKeyPress="javascript:return solonumeros(event)" style="width:20%"> %</td>
				</tr> 
			</table>
			<input type="hidden" name="oculto" id="oculto" value="1">
			<?php
				$oculto=$_POST['oculto'];
				if($_POST['oculto']=='2'){
					if ($_POST['nombre'] != "" && $_POST['codigo'] != "" ){  
						$nr = "1";
						$sqlr="UPDATE tesosanciones SET nombre = '".$_POST['nombre']."', porcentaje = '".$_POST['valor']."', estado = 'S' WHERE id_sancion = '".$_POST['codigo']."'";
						if (!mysqli_query($linkbd,$sqlr)){
							echo"
							<script>
								Swal.fire({
									icon: 'error',
									title: 'Error!',
									text: 'No se pudo modificar la sanción',
									confirmButtonText: 'Continuar',
									confirmButtonColor: '#FF121A',
									timer: 2500
								});
							</script>";
						}else{
							echo"
							<script>
								Swal.fire({
									icon: 'success',
									title: 'Se ha modifico con Exito la sanció N°: ".$_POST['codigo']."',
									confirmButtonText: 'Continuar',
									confirmButtonColor: '#FF121A',
									timer: 3000
								});
							</script>";
						}
					}else{
						echo"
						<script>
							Swal.fire({
								icon: 'error',
								title: 'Error!',
								text: 'Falta informacion para modificar la sanción',
								confirmButtonText: 'Continuar',
								confirmButtonColor: '#FF121A',
								timer: 2500
							});
						</script>";
					}
				}
			?>
		</form>
	</body>
</html>