<?php
	header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=utf8");
	require "comun.inc";
	require "funciones.inc";
	require "conversor.php";
	require "validaciones.inc";
	session_start();
	$linkbd=conectar_v7();
	$linkbd -> set_charset("utf8");
	cargarcodigopag($_GET['codpag'],$_SESSION["nivel"]);
	date_default_timezone_set("America/Bogota");
?>
<!DOCTYPE >
<html lang="es">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=9">
		<title>:: IDEAL 10 - Contabilidad</title>
		<link href="css/css2.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css">
		<link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css">
		<link href="css/css4.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css">
		<link href="css/tabs2.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css">
		<script type="text/javascript" src="css/programas.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script type="text/javascript" src="css/calendario.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script type="text/javascript" src="JQuery/jquery-2.1.4.min.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script>
			function guardar()
			{
				var rp = document.form2.rp.value;
				var tercero = document.form2.ntercero.value;
				
				if(rp != "" && tercero != "" ){despliegamodalm('visible','4','Â¿Esta seguro que desea guardar?','1');}
				else {despliegamodalm('visible','2','Faltan datos por ingresar');}
			}
			function despliegamodalm(_valor, _tip, msj, ask )
			{
				document.getElementById("bgventanamodalm").style.visibility=_valor;
				if(_valor == "hidden"){document.getElementById('ventanam').src = "";}
				else
				{
					switch(_tip)
					{
						case "1":	document.getElementById('ventanam').src="ventana-mensaje1.php?titulos="+msj;break;
						case "2":	document.getElementById('ventanam').src="ventana-mensaje3.php?titulos="+msj;break;
						case "3":	document.getElementById('ventanam').src="ventana-mensaje2.php?titulos="+msj;break;
						case "4":	document.getElementById('ventanam').src="ventana-consulta1.php?titulos="+msj+"&idresp="+ask;break;	
					}
				}
			}
			function funcionmensaje()
			{
				var actual = parseFloat(document.form2.numero.value);
				document.location.href="ccp-visualizardestinodecompra.php?idnumero=" +actual;
			}
			function respuestaconsulta(ask)
			{
				switch(ask)
				{
					case "1":	document.form2.oculto.value='2';
								document.form2.submit();
								break;
				}
			}
			function despliegamodal2(_valor,v)
			{
				document.getElementById("bgventanamodal2").style.visibility=_valor;
				if(_valor=="hidden"){document.getElementById('ventana2').src="";}
				else
				{
					if(v==1){document.getElementById('ventana2').src="ventana-rp.php?";}
				}
			}
			function buscarp(e)
			{
				if (document.form2.rp.value!="")
				{
					document.form2.brp.value = '1';
					document.form2.submit();
				}
			}
			function adelante()
			{
				var actual = parseFloat(document.form2.numero.value);
				var maximo = parseFloat(document.form2.maximo.value);
				var editablead = parseFloat(document.form2.editablead.value);
				if(maximo > actual)
				{
					actual = parseFloat(actual)+1;
					if(editablead == 0){location.href="ccp-editardestinodecompra.php?idnumero=" +actual;}
					else{location.href="ccp-visualizardestinodecompra.php?idnumero=" +actual;}
				}
			}
			function atras()
			{
				var actual = parseFloat(document.form2.numero.value);
				var minimo = parseFloat(document.form2.minimo.value);
				var editableat = parseFloat(document.form2.editableat.value);
				if(actual > minimo)
				{
					actual = parseFloat(actual)-1;
					if(editableat == 0){location.href="ccp-editardestinodecompra.php?idnumero=" +actual;}
					else
					{location.href="ccp-visualizardestinodecompra.php?idnumero=" +actual;}
				}
			}
		</script>
		<?php titlepag();?>
	</head>
	<body>
		<IFRAME src="alertas.php" name="alertas" id="alertas" style="display:none"></IFRAME>
		<span id="todastablas2"></span>
		<table>
			<tr><script>barra_imagenes("cont");</script><?php cuadro_titulos();?></tr>	 
			<tr><?php menu_desplegable("cont");?></tr>
			<tr>
				<td colspan="3" class="cinta"><img src="imagenes/add.png" title="Nuevo" onClick="location.href='ccp-destinodecompra.php'" class="mgbt"><img src="imagenes/guarda.png" title="Guardar" onClick="guardar();" class="mgbt"><img src="imagenes/busca.png" title="Buscar" onClick="location.href='ccp-buscardestinodecompra.php'" class="mgbt"><img src="imagenes/agenda1.png" title="Agenda" onClick="mypop=window.open('plan-agenda.php','','');mypop.focus()" class="mgbt"><img src="imagenes/nv.png" title="Nueva ventana" onClick="<?php echo paginasnuevas("cont");?>" class="mgbt"><img src="imagenes/duplicar_pantalla.png" title="Duplicar pesta&ntilde;a" onClick="mypop=window.open('<?php echo $url2; ?>','','');mypop.focus();" class="tooltip bottom mgbt"><img src="imagenes/iratras.png" title="Atr&aacute;s" onClick="location.href='ccp-buscardestinodecompra.php'" class="mgbt"></td>
			</tr>
		</table>
		<div id="bgventanamodalm" class="bgventanamodalm">
			<div id="ventanamodalm" class="ventanamodalm">
				<IFRAME src="" name="ventanam" marginWidth=0 marginHeight=0 frameBorder=0 id="ventanam" frameSpacing=0 style=" width:700px; height:130px; top:200; overflow:hidden;"></IFRAME>
			</div>
		</div>
		<form name="form2" method="post" >
			<?php 
				if($_POST['oculto']=="")
				{
					$check1="checked";
					$_POST['numero']= $_GET['idnumero'];
					$sqlr="SELECT MIN(id), MAX(id), editable FROM ccpetdc";
					$res=mysqli_query($linkbd,$sqlr);
					$r=mysqli_fetch_row($res);
					$_POST['minimo']=$r[0];
					$_POST['maximo']=$r[1];
					$_POST['editable']=$r[2];
					$sqlediat = "SELECT editable FROM ccpetdc WHERE id = ".$_POST['numero']."-1" ;
					$resediat = mysqli_query($linkbd,$sqlediat);
					$rowediat = mysqli_fetch_row($resediat);
					$_POST['editableat']=$rowediat[0];
					$sqlediad = "SELECT editable FROM ccpetdc WHERE id = ".$_POST['numero']."+1";
					$resediad = mysqli_query($linkbd,$sqlediad);
					$rowediad = mysqli_fetch_row($resediad);
					$_POST['editablead']=$rowediad[0];
					$sqlEdiCab = "SELECT * FROM ccpetdc WHERE id = ".$_POST['numero'];
					$resEdiCab = mysqli_query($linkbd,$sqlEdiCab);
					$rowEdiCab = mysqli_fetch_row($resEdiCab);
					$_POST['rp'] = $rowEdiCab[2];
					$_POST['fecha'] = $rowEdiCab[4];
					$_POST['vigencia'] = $rowEdiCab[1];
					$_POST['contrato'] = $rowEdiCab[8];
					$_POST['idcdp'] = $rowEdiCab[9];
					$_POST['ntercero'] = $rowEdiCab[5];
					$_POST['valorrp'] = $rowEdiCab[6];
					$_POST['saldo'] = $rowEdiCab[7]; 
					$_POST['tipomov'] = $rowEdiCab[10];
					$sqlrcdp = "SELECT solicita, objeto FROM ccpetcdp WHERE id_cdp = $rowEdiCab[9]";
					$rescdp = mysqli_query($linkbd,$sqlrcdp);
					$rowcdp = mysqli_fetch_row($rescdp);
					$_POST['solicita'] = $rowcdp[0];
					$_POST['objeto'] = $rowcdp[1];
					$sqlrter = "SELECT nombre1, nombre2, apellido1, apellido2, razonsocial FROM terceros WHERE cedulanit = '".$_POST['ntercero']."'";
					$rester = mysqli_query($linkbd,$sqlrter);
					$rowter = mysqli_fetch_row($rester); 
					if($rowter[4] != ""){$_POST['nomtercero']= $rowter[4];}
					else {$_POST['nomtercero']= "$rowter[0] $rowter[1] $rowter[2] $rowter[3]";}
					switch($rowEdiCab[3])
					{
						case 'S':	$_POST['estado']='ACTIVO';
									$color=" style='background-color:#0CD02A; width:98%;height:30px; color:#fff'";
									break;
						case 'C':	$_POST['estado']='COMPLETO';
									$color=" style='background-color:#00CCFF; width:98%;height:30px; color:#fff'";
									break;
						case 'N':	$_POST['estado']='ANULADO';
									$color=" style='background-color:#aa0000; width:98%;height:30px; color:#fff'";
									break;
						case 'R':	$_POST['estado']='REVERSADO';
									$color=" style='background-color:#aa0000; width:98%;height:30px; color:#fff'";
									break;
					}
					$d = 0;
					$_POST['codVigenciag'] = array();
					$_POST['nomVigenciag'] = array();
					$_POST['codCuenta'] = array();
					$_POST['nomCuenta'] = array();
					$_POST['codFuente'] = array();
					$_POST['nomFuente'] = array();
					$_POST['codProductos'] = array();
					$_POST['nomProductos'] = array();
					$_POST['codIndicador'] = array();
					$_POST['nomIndicador'] = array();
					$_POST['codPoliticap'] = array();
					$_POST['nomPoliticap'] = array();
					$_POST['medioPago'] = array();
					$_POST['valor'] = array();
					$_POST['iddestcompra'] = array();
					$sqlEdiDet = "SELECT * FROM ccpetdc_detalle WHERE consvigencia = '".$_POST['numero']."'";
					$resEdiDet = mysqli_query($linkbd, $sqlEdiDet);
					while($rowEdiDet = mysqli_fetch_row($resEdiDet))
					{
						$sqlcodVigenciag = "SELECT nombre FROM ccpet_vigenciadelgasto WHERE codigo = '".$rowEdiDet[10]."'";
						$rescodVigenciag = mysqli_query($linkbd,$sqlcodVigenciag);
						$rowcodVigenciag = mysqli_fetch_row($rescodVigenciag);
						$_POST['nomVigenciag'][] = $rowcodVigenciag[0]; 
						$_POST['codVigenciag'][] = $rowEdiDet[10];
						$_POST['codCuenta'][] = $rowEdiDet[3];
						$nomCuentares = buscacuentaccpetgastos($rowEdiDet[3]);
						$_POST['nomCuenta'][] = $nomCuentares;
						$_POST['codFuente'][] = $rowEdiDet[5];
						$nomFuenteres = buscafuenteccpet($rowEdiDet[5]);
						$_POST['nomFuente'][] = $nomFuenteres;
						$_POST['codIndicador'][] = $rowEdiDet[8];
						$nomIndicadorres = buscaindicadorccpet($rowEdiDet[8]);
						$_POST['nomIndicador'][] = $nomIndicadorres;
						$_POST['codProductos'][] = $rowEdiDet[4];
						$nomProductosres = buscaservicioccpetgastos($rowEdiDet[4]);
						if($nomProductosres == ''){$nomProductosres = buscaproductoccpetgastos($rowEdiDet[4]);}
						$_POST['nomProductos'][] = $nomProductosres;
						$sqlcodPoliticap = "SELECT nombre FROM ccpet_politicapublica WHERE codigo = '$rowEdiDet[9]'";
						$rescodPoliticap = mysqli_query($linkbd,$sqlcodPoliticap);
						$rowcodPoliticap = mysqli_fetch_row($rescodPoliticap);
						$_POST['nomPoliticap'][] = $rowcodPoliticap[0];
						$_POST['codPoliticap'][] = $rowEdiDet[9];
						$_POST['medioPago'][] = $rowEdiDet[14];
						$_POST['valor'][] = $rowEdiDet[11];
						$_POST["destcompra$d"] = $rowEdiDet[16];
						$_POST['iddestcompra'][] = $rowEdiDet[0];
						$d++;
					}
				}
				$co="zebra1";
				$co2="zebra2";
				switch($_POST['tabgroup1'])
				{
					case 1:	$check1='checked';break;
					case 2:	$check2='checked';break;
					case 3:	$check3='checked';break;
				}
			?>
			<div class="tabs" style="height:74%; width:99.7%">
				<div class="tab">
					<input type="radio" id="tab-1" name="tabgroup1" value="1" <?php echo $check1;?>/>
					<label id="clabel" for="tab-1">Radicar Destino de Compra</label>
					<div class="content" style="overflow:hidden;">
						<table class="inicio" width="99.6%">
							<tr>
								<td class="titulos" colspan="10">.: Radicar Destino de Compra </td>
								<td class="cerrar" style='width:7%' onClick="location.href='cont-principal.php'">Cerrar</td>
							<tr>
								<td class="tamano01" style="width:3.5cm;">N&uacute;mero:</td>
								<td style="width:9%"><img src="imagenes/back.png" title="Anterior" onClick="atras()" class="icobut">&nbsp;<input type="text" name="numero" id="numero" value="<?php echo $_POST['numero'] ?>" style="text-align:center;width:55%;height:30px;" onKeyUp="return tabular(event,this)" readonly>&nbsp;<img src="imagenes/next.png" title="Siguiente" onClick="adelante()" class="icobut"></td>
								<td class="tamano01">RP:</td>
								<td><input type="text" name="rp" id="rp" onKeyUp="return tabular(event,this)" value="<?php echo $_POST['rp'];?>" style="width:98%;height:30px;" readonly></td>
								<td class="tamano01" >Fecha:</td>
								<td style="width:15%;"><input name="fecha" type="text" id="fc_1198971545" value="<?php echo $_POST['fecha']; ?>" style="width:98%;height:30px;" readonly></td>
								<td class="tamano01">Vigencia:</td>
								<td style="width:3cm;"><input type="text" id="vigencia" name="vigencia" value="<?php echo $_POST['vigencia'] ?>" style="width:98%;height:30px;" readonly></td>
								<td class="tamano01">Contrato:</td>
								<td><input id="contrato" type="text" name="contrato" onKeyUp="return tabular(event,this)"  onKeyPress="javascript:return solonumeros(event)" value="<?php echo $_POST['contrato'] ?>" style="width:100%;height:30px;" readonly></td>
							</tr>
							<tr>
								<td class="tamano01">Numero CDP:</td>
								<td><input name="idcdp" type="text" id="idcdp" onKeyUp="return tabular(event,this)" value="<?php echo $_POST['idcdp'] ?>" style="width:98%;height:30px;" readonly></td>
								<td class="saludo1">Tercero:</td>
								<td>
									<input type="text" name="ntercero" id="ntercero" value="<?php echo $_POST['ntercero'] ?>" style="width:98%;height:30px;" readonly>
								</td>
								<td colspan="6">
									<input type="text" name="nomtercero" id="nomtercero" value="<?php echo $_POST['nomtercero'] ?>" style="width:100%;height:30px;" readonly>
								</td>
							</tr>
							<tr>
								<td class="tamano01">Solicita:</td>
								<td colspan="3"><input type="text" name="solicita" id="solicita" onKeyUp="return tabular(event,this)" value="<?php echo $_POST['solicita'] ?>" style="width:99%;height:30px;" readonly></td>
								<td class="tamano01">Objeto:</td>
								<td colspan='5'><input type="text" name="objeto"  id="objeto" onKeyUp="return tabular(event,this)" value="<?php echo $_POST['objeto'] ?>" style="width:100%;height:30px;" readonly> </td>
							</tr>
							<tr>
								<td class="tamano01">Valor RP:</td>
								<td><input name="valorrp" type="text" value="<?php echo $_POST['valorrp'] ?>" style="width:98%;height:30px;" readonly></td>
								<td class="tamano01">Saldo:</td>
								<td><input name="saldo" type="text" value="<?php echo $_POST['saldo'] ?>" style="width:98%;height:30px;" readonly></td>
								<td class="tamano01">Estado:</td>
								<td ><input name="estado" type="text" id="estado" value="<?php echo $_POST['estado'] ?>" readonly <?php echo $color; ?> style="width:98%;height:30px;"/></td>
								<td class="tamano01">Tipo movimiento:</td>
								<td><input name="tipomov" type="text" value="<?php echo $_POST['tipomov'] ?>" style="width:98%;height:30px;" readonly></td>
						</table>
						<div class="subpantalla" style="height:62%; width:99.6%; overflow-x:hidden;">
							<?php
								echo"
								<table class='inicio' width='99%'>
									<tr><td class='titulos' colspan='10'>.: Detalle destino de comprar</td></tr>
									<tr class='titulos2'>
										<td><center>Vigencia del Gasto</center></td>
										<td style='width:10%'><center>Cuenta</center></td>
										<td><center>Nombre Cuenta</center></td>
										<td><center>Fuente</center></td>
										<td><center>Producto/Servicio</center></td>
										<td style='width:10%'><center>Indicador Producto</center></td>
										<td style='width:10%'><center>Pol&iacute;tica P&uacute;plica</center></td>
										<td><center>Medio de Pago</center></td>
										<td style='width:10%'><center>Valor</center></td>
										<td style='width:5%'><center>Destino de compra</center></td>
									</tr>";
								$_POST['cuentagas'] = 0;
								for($x = 0; $x < count($_POST['nomCuenta']); $x++)
								{
									echo"
									<tr class='$co'>
										<input type='hidden' name='codVigenciag[]' value='".$_POST['codVigenciag'][$x]."'/>
										<input type='hidden' name='nomVigenciag[]' value='".$_POST['nomVigenciag'][$x]."'/>
										<input type='hidden' name='codCuenta[]' value='".$_POST['codCuenta'][$x]."' />
										<input type='hidden' name='nomCuenta[]' value='".$_POST['nomCuenta'][$x]."'/>
										<input type='hidden' name='codFuente[]' value='".$_POST['codFuente'][$x]."'/>
										<input type='hidden' name='nomFuente[]' value='".$_POST['nomFuente'][$x]."'/>
										<input type='hidden' name='codProductos[]' value='".$_POST['codProductos'][$x]."'/>
										<input type='hidden' name='nomProductos[]' value='".$_POST['nomProductos'][$x]."'/>
										<input type='hidden' name='codIndicador[]' value='".$_POST['codIndicador'][$x]."'/>
										<input type='hidden' name='nomIndicador[]' value='".$_POST['nomIndicador'][$x]."'/>
										<input type='hidden' name='codPoliticap[]' value='".$_POST['codPoliticap'][$x]."'/>
										<input type='hidden' name='nomPoliticap[]' value='".$_POST['nomPoliticap'][$x]."'/>
										<input type='hidden' name='medioPago[]' value='".$_POST['medioPago'][$x]."'/>
										<input type='hidden' name='valor[]' value='".$_POST['valor'][$x]."'>
										<input type='hidden' name='codsector[]' value='".$_POST['codsector'][$x]."'>
										<input type='hidden' name='nomsector[]' value='".$_POST['nomsector'][$x]."'>
										<td style='width=10%'>".$_POST['codVigenciag'][$x]." - ".$_POST['nomVigenciag'][$x]."</td>
										<td style='width=10%'>".$_POST['codCuenta'][$x]."</td>
										<td style='width=10%'>".$_POST['nomCuenta'][$x]."</td>
										<td style='width=10%'>".$_POST['codFuente'][$x]." - ".$_POST['nomFuente'][$x]."</td>
										<td style='width=10%'>".$_POST['codProductos'][$x]." - ".$_POST['nomProductos'][$x]."</td>
										<td style='width=10%'>".$_POST['codIndicador'][$x]." - ".$_POST['nomIndicador'][$x]."</td>
										<td style='width=10%'>".$_POST['codPoliticap'][$x]." - ".$_POST['nomPoliticap'][$x]."</td>
										<td style='width=10%'>".$_POST['medioPago'][$x]."</td>
										<td style='text-align:right;'>$ ".number_format($_POST['valor'][$x],2,$_SESSION["spdecimal"],$_SESSION["spmillares"])."</td>
										<td>
											<select name='destcompra$x' onChange='document.form2.submit();' >
												<option value=''>Seleccionar Destino de Compra</option>
									";
									$sqldest="SELECT * FROM almdestinocompra WHERE estado='S' ORDER BY codigo";
									$resdest=mysqli_query($linkbd,$sqldest);
									while($rowdest = mysqli_fetch_row($resdest))
									{
										if($_POST["destcompra$x"]==$rowdest[0]){echo "<option value='$rowdest[0]' SELECTED>$rowdest[1]</option>";}
										else {echo "<option value='$rowdest[0]'>$rowdest[1]</option>";}
									}
									echo "
											</select>
										</td>
									</tr>";
									$gas=$_POST['valor'][$x];
									$_POST['cuentagas']=$_POST['cuentagas']+$gas;
									$resultado = convertir($_POST['cuentagas']);
									$_POST['letras']=$resultado." PESOS";
									$aux=$co;
									$co=$co2;
									$co2=$aux;
								}
								echo "
									<input type='hidden' name='cuentagas' id='cuentagas' value='".$_POST['cuentagas']."'/>
									<input type='hidden' name='cuentagas2' id='cuentagas2' value='".$_POST['cuentagas2']."'/>
									<input type='hidden' name='letras' id='letras' value='".$_POST['letras']."'/>
									<tr class='$co' style='text-align:right;'>
										<td colspan='8'>Total:</td>
										<td>$ ".number_format($_POST['cuentagas'],2,$_SESSION["spdecimal"],$_SESSION["spmillares"])."</td>
									</tr>
									<tr>
										<td class='saludo1'>Son:</td>
										<td class='saludo1' colspan= '9'>".$_POST['letras']."</td>
									</tr>
								</table>";
							?>
						</div>
						<input type="hidden" name="oculto" id="oculto" value="1">
					</div>
				</div>
				
				
				
			</div>
			<input type="hidden" name="maximo" id="maximo" value="<?php echo $_POST['maximo']?>"/>
			<input type="hidden" name="minimo" id="minimo" value="<?php echo $_POST['minimo']?>"/>
			<input type="hidden" name="editable" id="editable" value="<?php echo $_POST['editable']?>"/>
			<input type="hidden" name="editableat" id="editableat" value="<?php echo $_POST['editableat']?>"/>
			<input type="hidden" name="editablead" id="editablead" value="<?php echo $_POST['editablead']?>"/>

				<?php
					if($_POST['oculto'] =='2')
					{
						switch($_POST['estado'])
						{
							case 'ACTIVO':	
								$estado = 'S';
							break;

							case 'COMPLETO':
								$estado = 'C';
							break;

							case 'ANULADO':
								$estado = 'N'; 
							break;

							case 'REVERSADO':
								$estado = 'R';
							break;
						}

	 					$sqlCabecera = "UPDATE ccpetdc SET editable = '1' WHERE id = '".$_POST['numero']."' ";
						mysqli_query($linkbd,$sqlCabecera);
						
						for($x=0;$x<count($_POST['nomCuenta']);$x++)
						{
							$sqlDetalle="UPDATE ccpetdc_detalle SET destino_compra = '".$_POST["destcompra$x"]."' WHERE id = '".$_POST['iddestcompra'][$x]."' ";
							
							if(!mysqli_query($linkbd,$sqlDetalle))
							{
								echo"
								<script>
									despliegamodalm('visible','2','No se pudo ejecutar la accion: ');
								</script>";
							}
							else 
							{
								echo "
								<script>
									despliegamodalm('visible','1','Se ha almacenado con exito');
								</script>";
							}
						}
					}
            	?>
            </div>
        </form>
        <div id="bgventanamodal2">
            <div id="ventanamodal2">
                <IFRAME  src="" name="buster" marginWidth=0 marginHeight=0 frameBorder=0 id="ventana2" frameSpacing=0 style="left:500px; width:900px; height:500px; top:200;"></IFRAME>
            </div>
        </div>
    </body>
</html>
