const URL ='contabilidad_vue/exogena_columnas/editar/cont-columnasEditar.php';

var app = new Vue({
    el:"#myapp",
    data() {
        return {
            isLoading:false,
            txtNombre:"",
            txtConsecutivo:0,
            txtMax:0
        }
    },
    mounted() {
        this.getData();
    },
    methods: {
        getData:async function(){
            let codigo = new URLSearchParams(window.location.search).get('id');
            const formData = new FormData();
            formData.append("action","get");
            formData.append("codigo",codigo);
            this.isLoading = true;
            const response = await fetch(URL,{method:"POST",body:formData});
            const objData = await response.json();
            if(objData.status){
                this.txtCodigo = objData.data.columna.formato;
                this.txtNombre = objData.data.columna.nombre;
                this.txtConsecutivo = objData.data.consecutivo;
                this.txtMax = objData.data.max;
            }else{
                window.location.href='cont-columnasExogenaEditar.php?id='+objData.max;
            }
            this.isLoading = false;

        },
        editItem:function(id){
            window.location.href='cont-columnasExogenaEditar.php?id='+id;
        },
        save: async function(){
            const vueContext = this;
            if(this.txtNombre ==""){
                Swal.fire("Error","Todos los campos con (*) son obligatorios","error");
                return false;
            }
            let formData = new FormData();
            formData.append("action","save");
            formData.append("nombre",this.txtNombre);
            formData.append("id",this.txtConsecutivo);
            Swal.fire({
                title:"¿Estás segur@ de guardar?",
                text:"",
                icon: 'warning',
                showCancelButton:true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText:"Sí, guardar",
                cancelButtonText:"No, cancelar"
            }).then(async function(result){
                if(result.isConfirmed){
                    let response = await fetch(URL,{method:"POST",body:formData});
                    let objData = await response.json();
                    if(objData.status){
                        Swal.fire("Guardado",objData.msg,"success");
                    }else{
                        Swal.fire("Error",objData.msg,"error");
                    }
                }
            });
        },
    },
    computed:{

    }
})
