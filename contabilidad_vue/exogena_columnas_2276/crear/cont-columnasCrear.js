const URL ='contabilidad_vue/exogena_columnas_2276/crear/cont-columnasCrear.php';

var app = new Vue({
    el:"#myapp",
    data() {
        return {
            isLoading:false,
            isModal:false,
            txtSearch:"",
            txtNombre:"",
            txtCodigo:"",
            txtResults:0,
            arrVariables:[],
            arrVariablesCopy:[],
            arrVariablesDetalle:[],
            objVariable:{"codigo":"","nombre":"","concepto":""}
        }
    },
    mounted() {
        this.getData();
    },
    methods: {
        getData:async function(){
            const formData = new FormData();
            formData.append("action","get");
            this.isLoading = true;
            const response = await fetch(URL,{method:"POST",body:formData});
            const objData = await response.json();
            this.arrVariables = objData;
            this.arrVariablesCopy = objData;
            this.txtResults = this.arrVariablesCopy.length;
            this.isLoading = false;
        },
        search:function(type=""){
            let search = this.txtSearch.toLowerCase();
            if(type=="modal"){
                this.arrVariablesCopy = [...this.arrVariables.filter(e=>e.nombre.toLowerCase().includes(search)
                    || e.codigo.toLowerCase().includes(search))];
                this.txtResults = this.arrVariablesCopy.length;
            }else{
                let arr = [...this.arrVariables.filter(e=>e.codigo.toLowerCase() == this.objVariable.codigo)];
                this.objVariable = arr.length > 0 ? arr[0] : {"codigo":"","nombre":"No existe","concepto":""};
            }
        },
        save:async function(){
            const vueContext = this;
            if(this.txtCodigo == "" || this.txtNombre == ""){
                Swal.fire("Error","Todos los campos con (*) son obligatorios.","error");
                return false;
            }
            let data = {
                "cabecera":{
                    "codigo":this.txtCodigo,
                    "nombre":this.txtNombre
                },
                "detalle":this.arrVariablesDetalle
            }
            const formData = new FormData();
            formData.append("action","save");
            formData.append("data",JSON.stringify(data));
            Swal.fire({
                title:"¿Estás segur@ de guardar?",
                text:"",
                icon: 'warning',
                showCancelButton:true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText:"Sí, guardar",
                cancelButtonText:"No, cancelar"
            }).then(async function(result){
                if(result.isConfirmed){
                    vueContext.isLoading = true;
                    const response = await fetch(URL,{method:"POST",body:formData});
                    const objData = await response.json();
                    if(objData.status){
                        Swal.fire("Guardado",objData.msg,"success");
                        setTimeout(function(){
                            window.location.reload();
                        },2000);
                    }else{
                        Swal.fire("Error",objData.msg,"error");
                    }
                    vueContext.isLoading = false;
                }
            });
        },
        selectItem:function({...item}){
            this.objVariable = item;
            this.isModal = false;
        },
        addItem:function(){
            let obj = Object.assign({},this.objVariable);
            if(obj.codigo != ""){
                let flag = this.arrVariablesDetalle.filter(e=>e.codigo == obj.codigo).length > 0  ? true : false;
                if(!flag){
                    this.arrVariablesDetalle.push(obj);
                    this.objVariable={"codigo":"","nombre":"","concepto":""};
                }else{
                    Swal.fire("Error","Esta variable ya fue agregada, pruebe con otra.","error");
                }
            }else{
                Swal.fire("Error","La variable está vacía o no es válida, pruebe con otra.","error");
            }
        },
        delItem:function({...item}){
            let index = this.arrVariablesDetalle.findIndex(e => e.codigo == item.codigo);
            this.arrVariablesDetalle.splice(index,1);
        }
    },
    computed:{

    }
})
