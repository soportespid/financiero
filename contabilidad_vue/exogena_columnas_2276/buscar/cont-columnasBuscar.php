<?php
    require_once '../../../comun.inc';
    require '../../../funciones.inc';
    require '../../../funcionesSP.inc.php';
    /*ini_set('display_errors', '1');
    ini_set('display_startup_errors', '1');
    error_reporting(E_ALL);*/
    session_start();
    //dep($_POST);exit;
    if($_POST){
        $obj = new Plantilla();
        if($_POST['action']=="get"){
            $obj->getData();
        }else if($_POST['action']=="change"){
            $obj->changeData();
        }else if($_POST['action'] == "del"){
            $obj->delData();
        }
    }

    class Plantilla{
        private $linkbd;
        private $arrData;
        public function __construct() {
            $this->linkbd = conectar_v7();
            $this->linkbd->set_charset("utf8");
        }
        public function getData(){
            if(!empty($_SESSION)){
                $request = $this->selectData();
                echo json_encode($request,JSON_UNESCAPED_UNICODE);
            }
            die();
        }
        public function changeData(){
            if(!empty($_SESSION)){
                $id = intval($_POST['id']);
                $estado = $_POST['estado'];
                $sql = "UPDATE contcodigosinternos SET estado = '$estado' WHERE idnum = $id";
                $request = mysqli_query($this->linkbd,$sql);
                if($request == 1){
                    $arrResponse = array("status"=>true);
                }else{
                    $arrResponse = array("status"=>false,"msg"=>"Error");
                }
                echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
            }
            die();
        }
        public function delData(){
            if(!empty($_SESSION)){
                $id = intval($_POST['id']);
                $sql = "DELETE FROM contcodigosinternos WHERE idnum = $id";
                $request = mysqli_query($this->linkbd,$sql);
                if($request == 1){
                    $sql = "DELETE FROM contcodigosinternos_det WHERE idnum = $id";
                    mysqli_query($this->linkbd,$sql);

                    $arrResponse = array("status"=>true);
                }else{
                    $arrResponse = array("status"=>false,"msg"=>"Error");
                }
                echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
            }
            die();
        }
        public function selectData(){
            $sql = "SELECT *, idnum as id FROM contcodigosinternos ORDER BY idnum";
            $arrData = mysqli_fetch_all(mysqli_query($this->linkbd,$sql),MYSQLI_ASSOC);
            $total = count($arrData);
            $arrResponse = array("status"=>true,"data"=>$arrData,"total"=>$total);
            return $arrResponse;
        }
    }
?>
