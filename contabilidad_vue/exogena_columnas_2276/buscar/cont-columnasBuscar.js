const URL ='contabilidad_vue/exogena_columnas_2276/buscar/cont-columnasBuscar.php';

var app = new Vue({
    el:"#myapp",
    data() {
        return {
            isLoading:false,
            arrData:[],
            arrDataCopy:[],
            txtResults:0,
            txtSearch:"",
        }
    },
    mounted() {
        this.getData();
    },
    methods: {
        getData: async function(){
            const formData = new FormData();
            formData.append("action","get");
            this.isLoading=true;
            const response = await fetch(URL,{method:"POST", body:formData});
            const objData = await response.json();
            this.arrData = objData.data;
            this.arrDataCopy = objData.data;
            this.txtResults = objData.total;
            this.isLoading=false;
        },
        search:function(){
            let search = this.txtSearch.toLowerCase();
            this.arrDataCopy = [...this.arrData.filter(e=>e.nombre.toLowerCase().includes(search)
            || e.codigo.toLowerCase().includes(search))];
            this.txtResults = this.arrDataCopy.length;
        },
        editItem:function(id){
            window.location.href='cont-columnasExogena2276Editar.php?id='+id;
        },
        changeStatus:function(item){
            const vueThis = this;
            Swal.fire({
                title:"¿Estás segur@ de cambiar el estado?",
                text:"",
                icon: 'warning',
                showCancelButton:true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText:"Sí",
                cancelButtonText:"No, cancelar"
            }).then(async function(result){
                if(result.isConfirmed){
                    const formData = new FormData();
                    formData.append("action","change");
                    formData.append("id",item.id);
                    formData.append("estado",item.estado =='S' ? 'N' : 'S');
                    const response = await fetch(URL,{method:"POST",body:formData});
                    const objData = await response.json();
                    if(objData.status){
                        let index = vueThis.arrData.findIndex(e => e.id == item.id);
                        vueThis.arrData[index].estado = item.estado =='S' ? 'N' : 'S';
                        vueThis.search();
                        Swal.fire("Estado actualizado","","success");
                    }else{
                        Swal.fire("Error",objData.msg,"error");
                    }
                }
            });

        },
        delItem:function(item){
            const vueThis = this;
            Swal.fire({
                title:"¿Estás segur@ de eliminar la columna?",
                text:"Se eliminará para siempre...",
                icon: 'warning',
                showCancelButton:true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText:"Sí, eliminar",
                cancelButtonText:"No, cancelar"
            }).then(async function(result){
                if(result.isConfirmed){
                    const formData = new FormData();
                    formData.append("action","del");
                    formData.append("id",item.id);
                    const response = await fetch(URL,{method:"POST",body:formData});
                    const objData = await response.json();
                    if(objData.status){
                        let index = vueThis.arrData.findIndex(e => e.id == item.id);
                        vueThis.arrData.splice(index,1);
                        vueThis.search();
                        Swal.fire("Columna eliminada","","success");
                    }else{
                        Swal.fire("Error",objData.msg,"error");
                    }
                }
            });

        },
    },
    computed:{

    }
})
