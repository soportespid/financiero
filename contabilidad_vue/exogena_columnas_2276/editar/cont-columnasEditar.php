<?php
    require_once '../../../PHPExcel/Classes/PHPExcel.php';
    require_once '../../../comun.inc';
    require '../../../funciones.inc';
    require '../../../funcionesSP.inc.php';
    /*ini_set('display_errors', '1');
    ini_set('display_startup_errors', '1');
    error_reporting(E_ALL);*/
    session_start();
    if($_POST){
        $obj = new Plantilla();
        if($_POST['action']=="get"){
            $obj->getData();
        }else if($_POST['action']=="save"){
            $obj->save();
        }
    }

    class Plantilla{
        private $linkbd;
        private $arrData;
        private $strVigencia;
        public function __construct() {
            $this->linkbd = conectar_v7();
            $this->linkbd->set_charset("utf8");
        }
        public function getData(){
            if(!empty($_SESSION)){
                $id = intval($_POST['codigo']);
                $info = $this->selectColumna($id);
                $max = $this->selectMax();
                if(!empty($info)){
                    $request['variables'] = $this->selectData();
                    $request['columna'] = $info;
                    $request['consecutivo'] = $id;
                    $request['max'] = $max;
                    $arrResponse = array("status"=>true,"data"=>$request);
                }else{
                    $arrResponse = array("status"=>false,"max"=>$max);
                }
                echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
            }
            die();
        }
        public function save(){
            if(!empty($_SESSION)){
                $arrData = json_decode($_POST['data'],true);
                if(empty($arrData)){
                    $arrResponse = array("status"=>false,"msg"=>"Error de datos");
                }else{
                    $request = $this->updateColumna($arrData['cabecera']);
                    if(is_numeric($request) && $request > 0){
                        if(!empty($arrData['detalle'])){
                            $this->insertColumnaDet($request,$arrData['detalle']);
                        }
                        $arrResponse = array("status"=>true,"msg"=>"Datos actualizados correctamente.");
                    }else if($request =="existe"){
                        $arrResponse = array("status"=>false,"msg"=>"La columna ya existe, pruebe con otra.");
                    }else{
                        $arrResponse = array("status"=>false,"msg"=>"Error en cabecera");
                    }
                }
                echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
            }
            die();
        }
        public function selectColumna($id){
            $sql = "SELECT * FROM contcodigosinternos WHERE idnum = $id";
            $request = mysqli_query($this->linkbd,$sql)->fetch_assoc();
            if(!empty($request)){
                $sql = "SELECT * FROM contcodigosinternos_det WHERE idnum = $id";
                $request['variables'] = mysqli_fetch_all(mysqli_query($this->linkbd,$sql),MYSQLI_ASSOC);
            }
            return $request;
        }
        public function selectMax(){
            $sql = "SELECT MAX(idnum) as max FROM contcodigosinternos";
            $request = mysqli_query($this->linkbd,$sql)->fetch_assoc()['max'];
            return $request;
        }
        public function updateColumna($data){
            $id = $data['id'];
            $nombre = ucwords($data['nombre']);
            $sql = "SELECT * FROM contcodigosinternos WHERE (codigo= '{$data['codigo']}' OR nombre= '$nombre') AND idnum != $id";
            $request = mysqli_query($this->linkbd,$sql)->fetch_assoc();
            if(empty($request)){
                $sql = "UPDATE contcodigosinternos SET codigo = '$data[codigo]',nombre='$nombre' WHERE idnum = $id";
                mysqli_query($this->linkbd,$sql);
                $request = intval($id);
            }else{
                $request = "existe";
            }
            return $request;
        }
        public function insertColumnaDet($id,$data){
            $sql = "DELETE FROM contcodigosinternos_det WHERE idnum = $id";
            mysqli_query($this->linkbd,$sql);
            $request ="";
            foreach ($data as $var) {
                $consecutivo = selconsecutivo('contcodigosinternos_det','iddet');
                $sql ="INSERT INTO contcodigosinternos_det (iddet,idnum,codigo,nombre,concepto,estado)VALUES(
                    $consecutivo,
                    $id,
                    '$var[codigo]',
                    '$var[nombre]',
                    '$var[concepto]',
                    'S'
                )";
                mysqli_query($this->linkbd,$sql);
                $request = intval($consecutivo);
            }
            return $request;
        }
        public function selectData(){
            $sql = "SELECT codigo,nombre, codigo as concepto FROM ccpethumvariables ORDER BY codigo" ;
            $request = mysqli_fetch_all(mysqli_query($this->linkbd,$sql),MYSQLI_ASSOC);
            array_push($request,array("codigo"=>"SR","nombre"=>"SALUD EMPLEADOR","concepto"=>"07"));
            array_push($request,array("codigo"=>"SE","nombre"=>"SALUD EMPLEADO","concepto"=>"08"));
            array_push($request,array("codigo"=>"PR","nombre"=>"PENSION EMPLEADOR","concepto"=>"09"));
            array_push($request,array("codigo"=>"PE","nombre"=>"PENSION EMPLEADO","concepto"=>"10"));
            return $request;
        }
    }
?>
