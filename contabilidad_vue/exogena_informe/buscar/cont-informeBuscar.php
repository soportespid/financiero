<?php
    require_once '../../../comun.inc';
    require '../../../funciones.inc';
    require '../../../funcionesSP.inc.php';
    /*ini_set('display_errors', '1');
    ini_set('display_startup_errors', '1');
    error_reporting(E_ALL);*/
    session_start();
    //dep($_POST);exit;
    if($_POST){
        $obj = new Plantilla();
        if($_POST['action']=="get"){
            $obj->getData();
        }else if($_POST['action'] == "exogena"){
            $obj->getExogena();
        }
    }

    class Plantilla{
        private $linkbd;
        private $arrData;
        public function __construct() {
            $this->linkbd = conectar_v7();
            $this->linkbd->set_charset("utf8");
        }
        public function getData(){
            if(!empty($_SESSION)){
                $request = $this->selectData();
                echo json_encode($request,JSON_UNESCAPED_UNICODE);
            }
            die();
        }
        public function getExogena(){
            if(!empty($_SESSION)){
                if($_POST){
                    if(empty($_POST['formato']) || empty($_POST['fecha'])){
                        $arrResponse = array("status"=>false,"msg"=>"Error de datos");
                    }else{
                        $formato = $_POST['formato'];
                        $fecha = $_POST['fecha'];
                        $columnas = $this->selectColumnas($formato);
                        $filas = $this->selectFilas($formato,$fecha,$columnas);
                        $arrResponse = array("status"=>true,"columnas"=>$columnas,"filas"=>$filas);
                    }
                    echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
                }
            }
            die();
        }
        public function selectFilas($formato,$fecha,$columnas){
            $arrColumnas = array_values(array_filter($columnas,function($e){return $e['tipo'] == 2;}));
            $arrMovimientosCuenta = $this->selectMovimientosCuenta($fecha,$formato);
            $totalColumnas = count($arrColumnas);
            $arrFilas = $this->filtrarMovimientosTercero($arrMovimientosCuenta);
            //dep($arrFilas);exit;
            $totalFilas = count($arrFilas);
            //Itero filas a mostrar
            for ($i=0; $i < $totalFilas; $i++) {
                $arrNewColumnas = $arrColumnas;
                $columnasInfo = $arrFilas[$i]['columnas_info'];
                $totalColumnasInfo = count($columnasInfo);
                //Ahora, itero las columnas que tiene cada fila.
                for ($k=0; $k < $totalColumnasInfo; $k++) {
                    for ($j=0; $j< $totalColumnas; $j++) {
                        /*
                        Aquí, asigno el valor de columnas info en las columnas originales para posteriormente guardarlas en las filas
                        Ya que las columnas originales son las que se muestran en la vista.
                        */
                        if($columnasInfo[$k]['columna'] == $arrNewColumnas[$j]['columna'] && $columnasInfo[$k]['valor'] >= $arrNewColumnas[$j]['valor']){
                            $arrNewColumnas[$j]['valor_cuenta'] += $columnasInfo[$k]['valor'];
                            break;
                        }
                    }
                }
                //filtro las columnas que tiene valor mínimo que actuarán como principales
                $arrMainColumnas = array_values(array_filter($arrNewColumnas,function($e){return $e['valor']>0;}));
                $totalValorColumna = 0;
                foreach ($arrMainColumnas as $col) {
                    $totalValorColumna+=$col['valor_cuenta'];
                }
                //Si la suma del valor_cuenta asignado a las columnas principales es igual a cero, entonces toda la fila queda en ceros.
                if($totalValorColumna == 0){
                    for ($j=0; $j < count($arrNewColumnas) ; $j++) {
                        $arrNewColumnas[$j]['valor_cuenta'] = 0;
                    }
                }
                $arrFilas[$i]['columnas'] = $arrNewColumnas;
            }
            //Filtro las columnas que no están en ceros
            $arrNewFilas=[];
            foreach ($arrFilas as $fila) {
                $totalValores = 0;
                foreach ($fila['columnas'] as $columna) {
                    $totalValores+=$columna['valor_cuenta'];
                }
                if($totalValores > 0){
                    array_push($arrNewFilas,$fila);
                }
            }
            $arrFilas = $arrNewFilas;
            usort($arrFilas,function($a,$b){
                return strcmp($a['cedulanit'],$b['cedulanit']);
            });
            return $arrFilas;
        }
        public function filtrarMovimientosTercero($arrMovimientosCuenta){
            $arrFilas = [];
            //Filtro los movimientos por tercero y los agrego a un nuevo array
            for ($i=0; $i < count($arrMovimientosCuenta); $i++) {
                $totalFilas = count($arrFilas);
                if($totalFilas>0){
                    $flag = true;
                    for ($j=0; $j < count($arrFilas); $j++) {
                        //Si el tercero ya existe, agrego nuevas columnas
                            if(
                                $arrFilas[$j]['cedulanit'] == $arrMovimientosCuenta[$i]['cedulanit'] &&
                                $arrFilas[$j]['concepto'] == $arrMovimientosCuenta[$i]['concepto']
                            ){

                                $arrColumnas = $arrFilas[$j]['columnas_info'];
                                $nuevaCol = $arrMovimientosCuenta[$i]['columnas_info'][0];
                                array_push($arrColumnas,$nuevaCol);
                                $arrFilas[$j]['columnas_info'] = $arrColumnas;
                                $flag = false;
                                break;
                            }
                    }
                    if($flag){
                        array_push($arrFilas,$arrMovimientosCuenta[$i]);
                    }
                }else{
                    array_push($arrFilas,$arrMovimientosCuenta[$i]);
                }
            }
            //Iteración para reasignar columnas a la fila correspondiente
            for ($i=0; $i < count($arrFilas); $i++) {
                //ordeno las columnas por id de columna que va en cada fila o movimiento
                usort($arrFilas[$i]['columnas_info'],function($a,$b){
                    return strcmp($a['columna'],$b['columna']);
                });
                $columnas = $arrFilas[$i]['columnas_info'];
                $newColumnas = [];
                $moverColumnas = [];
                $moverAtercero = "";
                $cuentaPadre ="";
                for ($j=0; $j < count($columnas) ; $j++) {
                    $flag = false;
                    $col = $columnas[$j];
                    //Filtro las columnas que siguen en la misma fila y las que deben ir en otra.
                    //Las que son principales
                    if($columnas[$j]['is_main']){//Las que son principales
                        $flag = true;
                        array_push($newColumnas,$col);
                    }
                    if($arrFilas[$i]['is_main']){
                        //Las que son secundarias y no coincide con el comprobante columna y comprobante fila pero se mantienen en la misma fila
                        if(!$columnas[$j]['is_main'] && $columnas[$j]['comprobante'] != $arrFilas[$i]['numerotipo']){
                            $col = $columnas[$j];
                            $flag = true;
                            array_push($newColumnas,$col);
                        }
                    }
                    //Las restantes son las que deben ir en otra fila.
                    if(!$flag){
                        $moverAtercero = $arrFilas[$i]['cedulanit'];
                        array_push($moverColumnas,$col);
                    }
                }


                //Identifico la cuenta padre donde deben ir las filas que no corresponden a la fila original
                if(!empty($newColumnas)){
                    $cuentaPadre = array_values(array_filter($newColumnas,function($e){return !$e['is_main'];}))[0]['cuenta_padre'];
                }else if(!empty($moverColumnas)){
                    $cuentaPadre = $moverColumnas[0]['cuenta_padre'];
                }
                 /*
                    Itero nuevamente las filas para buscar y encontrar la fila donde corresponden las columnas que se deben reasignar
                    a la fila correspondiente y esto se logra con la cuenta padre y cedula del tercero.
                */
                if($cuentaPadre!=""){
                    for ($j=0; $j < count($arrFilas) ; $j++) {
                        if($arrFilas[$j]['cuenta'] == $cuentaPadre && $moverAtercero == $arrFilas[$j]['cedulanit']){
                            $updateColumnas = array_merge($arrFilas[$j]['columnas_info'],$moverColumnas);
                            $arrFilas[$j]['columnas_info'] = $updateColumnas;
                            break;
                        }
                    }
                }
                $arrFilas[$i]['columnas_info'] = $newColumnas;
            }
            return $arrFilas;
        }
        public function selectMovimientosCuenta($fecha,$formato){
            $sqlConceptos = "SELECT  det.concepto,det.formato,
            det.columna_id,det.cuenta,det.tipo, cab.valor
            FROM contexogenaparametros_det det
            INNER JOIN contexogenaparametros_cab cab
            ON cab.columna = det.columna_id
            WHERE det.formato = '$formato'";
            $arrConceptos = mysqli_fetch_all(mysqli_query($this->linkbd,$sqlConceptos),MYSQLI_ASSOC);
            $arrMovimientosCuenta = [];
            $totalConceptos = count($arrConceptos);
            //Itero conceptos configurados
            for ($i=0; $i < $totalConceptos ; $i++) {
                /*
                Obtengo los movimientos por cada cuenta asignada en cada concepto teniendo en cuenta los valores configurados en cada
                columna para identificar a qué columnas principales están configuradas las cuentas.
                */
                $movimientos= $this->selectMovimientos($arrConceptos[$i]['cuenta'],$fecha,$arrConceptos[$i]['valor']);
                $totalMov = count($movimientos);
                /*
                Itero todos los movimientos de cada cuenta configurada en su respectivo concepto para almacenarlos todos juntos
                en un solo arreglo
                */
                for ($j=0; $j < $totalMov ; $j++) {
                    array_push($arrMovimientosCuenta,$movimientos[$j]);
                }
            }
            //En esta parte, discrimino los movimientos principales y secundarios con el estado "is_main"
            $arrMovPrincipal = array_values(array_filter($arrMovimientosCuenta,function($e){return $e['is_main'];}));
            $arrMovSecundarios = array_values(array_filter($arrMovimientosCuenta,function($e){return !$e['is_main'];}));
            $arrMovimientosCuenta = [];
            /*
                En esta iteración anidada comparo los comprobantes principales y secundarios con el propósito
                de decirle a cada movimiento que si el principal y secundario coincide con el mismo comprobante,
                entonces, el movimiento secundario se agrega al arreglo donde se almacenan todos los movimientos. Además,
                al movimiento secundario se le asigna un campo cuenta_padre que servirá en otro proceso.
            */
            for ($i=0; $i < count($arrMovPrincipal) ; $i++) {
                $comprobanteP = $arrMovPrincipal[$i]['numerotipo'];//Comprobante principal
                for ($j=0; $j < count($arrMovSecundarios); $j++) {
                    $comprobanteS = $arrMovSecundarios[$j]['numerotipo']; // Comprobante secundario
                    if($comprobanteS ==$comprobanteP){
                        $arrMovSecundarios[$j]['cuenta_padre'] = $arrMovPrincipal[$i]['cuenta'];
                        array_push($arrMovimientosCuenta,$arrMovSecundarios[$j]);
                        break;
                    }
                }
                array_push($arrMovimientosCuenta,$arrMovPrincipal[$i]);
            }

            $movimientos = $arrMovimientosCuenta;
            $arrMovimientosCuenta = [];
            /*
                En esta iteración anidada, lo que hace es iterar nuevamente los conceptos y movimientos para asignar
                los terceros de cada movimiento, el valor según el tipo de cuenta, y las columnas que maneja cada movimiento
            */
            for ($i=0; $i < count($arrConceptos); $i++) {
                $lengthConceptoCuenta = strlen($arrConceptos[$i]['cuenta']);
                //Itero movimientos de las cuentas configuradas
                for ($j=0; $j < count($movimientos); $j++) {
                    $cuentaMov = substr($movimientos[$j]['cuenta'],0,$lengthConceptoCuenta);
                    if($cuentaMov == $arrConceptos[$i]['cuenta']){
                        $tercero = $this->selectTercero($movimientos[$j]['tercero']);
                        if(!empty($tercero)){
                            $movimientos[$j]['cedulanit'] = $movimientos[$j]['tercero'];
                            $movimientos[$j]['tercero'] = $tercero;
                            $movimientos[$j]['concepto'] = $arrConceptos[$i]['concepto'];
                            $movimientos[$j]['saldo_anterior'] = $this->selectSaldoAnterior($fecha,$movimientos[$j]['cuenta'],$tercero['cedulanit']);

                            $valor = 0;
                            //Filtro el valor según el tipo de la cuenta, si es debito,creidto, saldo o debito-credito
                            if($arrConceptos[$i]['tipo'] == 1){
                                $valor = $movimientos[$j]['debito'];
                            }else if($arrConceptos[$i]['tipo'] == 2 ){
                                $valor = $movimientos[$j]['credito'];
                            }else if($arrConceptos[$i]['tipo'] == 3){
                                $valor = $movimientos[$j]['saldo_anterior']+abs($movimientos[$j]['debito']-$movimientos[$j]['credito']);
                            }else if($arrConceptos[$i]['tipo'] == 4){
                                $valor = abs($movimientos[$j]['debito']-$movimientos[$j]['credito']);
                            }
                            //Asigno el valor, tipo y la columna correspondiente
                            $movimientos[$j]['columnas_info'] = [array(
                                "columna"=>$arrConceptos[$i]['columna_id'],
                                "tipo"=>$arrConceptos[$i]['tipo'],
                                "valor"=>$valor,
                                "comprobante"=>$movimientos[$j]['numerotipo'],
                                "cuenta"=>$movimientos[$j]['cuenta'],
                                "cuenta_padre"=>$movimientos[$j]['cuenta_padre'],
                                "is_main"=>$movimientos[$j]['is_main']
                            )];
                            array_push($arrMovimientosCuenta,$movimientos[$j]);
                        }
                    }
                }
            }
            return $arrMovimientosCuenta;
        }
        public function selectMovimientos($cuenta,$fecha,$valor){
            $sql = "SELECT distinct
            comprobante_det.cuenta,
            comprobante_det.tercero,
            sum(comprobante_det.valdebito) as debito,
            sum(comprobante_det.valcredito) as credito,
            comprobante_det.numerotipo
            from comprobante_cab,comprobante_det
            where comprobante_det.cuenta LIKE '$cuenta%'
            and comprobante_cab.fecha between '{$fecha}-01-01' and '{$fecha}-12-31' and comprobante_det.tipo_comp=comprobante_cab.tipo_comp
            and comprobante_det.numerotipo=comprobante_cab.numerotipo and comprobante_cab.estado='1' and comprobante_cab.tipo_comp<>'7'
            and comprobante_cab.tipo_comp <> '102' and comprobante_cab.tipo_comp <> '100' and comprobante_cab.tipo_comp <> '101'
            and comprobante_cab.tipo_comp <> '103' and comprobante_cab.tipo_comp <> '104' and comprobante_det.tipo_comp <> 19
            and comprobante_det.tipo_comp <> 13
            group by comprobante_det.cuenta, comprobante_det.tercero,comprobante_det.numerotipo
            order by comprobante_det.cuenta, comprobante_cab.fecha,
            comprobante_cab.tipo_comp, comprobante_cab.numerotipo,comprobante_det.id_det;";
            $movimientos = mysqli_fetch_all(mysqli_query($this->linkbd,$sql),MYSQLI_ASSOC);
            $total = count($movimientos);
            for ($i=0; $i < $total ; $i++) {
                /*
                    Agrego dos estados a cada movimiento, "is_main" si la cuenta que tiene este movimiento pertenece a una
                    cuenta que está asignada a una columna principal; es columna principal cuando el valor > 0.
                */
                $movimientos[$i]['is_main'] = 0;
                if($valor > 0){
                    $movimientos[$i]['is_main'] = 1;
                }
            }
            return $movimientos;
        }
        public function selectSaldoAnterior($fecha,$cuenta,$cedula){
            //Saldo anterior
            $sqlPeriodoAnterior = "SELECT
            comprobante_det.cuenta,(sum(comprobante_det.valdebito)-sum(comprobante_det.valcredito)) as saldof
            from comprobante_cab,comprobante_det
            where comprobante_det.cuenta = '$cuenta' and  comprobante_cab.fecha
            between '{$fecha}-01-01' and '{$fecha}-12-31'
            and comprobante_det.tipo_comp=comprobante_cab.tipo_comp
            and comprobante_det.numerotipo=comprobante_cab.numerotipo
            and comprobante_cab.estado='1' and comprobante_cab.tipo_comp<>'7'
            group by comprobante_det.cuenta order by comprobante_cab.tipo_comp, comprobante_cab.numerotipo,comprobante_det.id_det";
            $periodoAnterior = mysqli_query($this->linkbd,$sqlPeriodoAnterior)->fetch_assoc();
            $saldoAnterior = !empty($periodoAnterior) ? $periodoAnterior['saldof'] : 0;

            //Busco saldo comprobante inicial
            $sqlComprobanteInicial = "SELECT distinct comprobante_det.cuenta,(sum(comprobante_det.valdebito)-sum(comprobante_det.valcredito)) as saldof
            from comprobante_cab,comprobante_det
            where comprobante_det.tercero='$cedula' and comprobante_det.cuenta = '{$saldoAnterior['cuenta']}'
            and comprobante_det.tipo_comp=comprobante_cab.tipo_comp and  comprobante_det.numerotipo=comprobante_cab.numerotipo
            and comprobante_cab.estado='1' and comprobante_cab.tipo_comp='7'
            AND YEAR(comprobante_cab.fecha) = '$fecha' AND comprobante_cab.tipo_comp <> '7'
            group by comprobante_det.cuenta order by comprobante_cab.tipo_comp, comprobante_cab.numerotipo,comprobante_det.id_det";

            $saldoComprobanteInicial = mysqli_query($this->linkbd,$sqlComprobanteInicial)->fetch_assoc();
            $saldoComprobanteInicial = !empty($saldoComprobanteInicial) ? $saldoComprobanteInicial['saldof'] : 0;
            $saldoAnterior = round($saldoAnterior+$saldoComprobanteInicial,2);
            return $saldoAnterior;
        }
        public function selectTercero($tercero){
            $sql="SELECT * FROM terceros WHERE cedulanit = '$tercero'";
            $request = mysqli_query($this->linkbd,$sql)->fetch_assoc();
            return $request;
        }
        public function selectColumnas($formato){
            $sql ="SELECT cab.valor,cab.columna,cab.visible,cab.tipo,col.nombre
            FROM contexogenaparametros_cab cab
            INNER JOIN contexogenacolumnas col
            ON cab.columna = col.id
            WHERE formato = '$formato'";
            $columnas = mysqli_fetch_all(mysqli_query($this->linkbd,$sql),MYSQLI_ASSOC);
            $totalCol = count($columnas);
            for ($i=0; $i < $totalCol ; $i++) {
                $columnas[$i]['check'] = boolval($columnas[$i]['visible']);
                $columnas[$i]['valor_cuenta'] = 0;
            }
            return $columnas;
        }
        public function selectData(){
            $sql = "SELECT f.formato,f.nombre
            FROM contexogenaparametros_cab cab
            INNER JOIN contexogenaformatos f
            ON f.formato = cab.formato
            GROUP BY cab.formato ORDER BY cab.formato";
            $request = mysqli_fetch_all(mysqli_query($this->linkbd,$sql),MYSQLI_ASSOC);
            return $request;
        }
    }
?>
