const URL ='contabilidad_vue/exogena_informe/buscar/cont-informeBuscar.php';

var app = new Vue({
    el:"#myapp",
    data() {
        return {
            isLoading:false,
            arrData:[],
            txtAnio:new Date().getFullYear(),
            selectFormato:0,
            arrColumnas:[],
            arrFilas:[],
            arrTotalColumnas:[]
        }
    },
    mounted() {
        this.getData();
    },
    methods: {
        getData: async function(){
            const formData = new FormData();
            formData.append("action","get");
            this.isLoading=true;
            const response = await fetch(URL,{method:"POST", body:formData});
            const objData = await response.json();
            this.arrData = objData;
            this.isLoading=false;
        },
        getExogena: async function(){
            if(this.selectFormato == 0 || this.txtAnio == "" || this.txtAnio <= 0){
                Swal.fire("Error","Todos los campos con (*) son obligatorios.","error");
                return false;
            }
            const formData = new FormData();
            const vueContext = this;
            formData.append("action","exogena");
            formData.append("formato",this.selectFormato);
            formData.append("fecha",this.txtAnio);
            this.isLoading = true;
            const response = await fetch(URL,{method:"POST",body:formData});
            const objData = await response.json();
            if(objData.status){
                this.arrColumnas = objData.columnas;
                this.arrFilas = objData.filas;
                for (let i = 0; i < vueContext.arrFilas.length; i++) {
                    let columnas = this.arrFilas[i].columnas;
                    this.arrFilas[i].columnas = columnas.filter(e=>e.check);
                }
                let arrColumnasFijas = [...this.arrColumnas.filter(e=>e.check && e.tipo==1)];
                let arrColumnasCalculadas = [...this.arrColumnas.filter(e=>e.check && e.tipo==2)];
                this.arrTotalColumnas = arrColumnasFijas.concat(arrColumnasCalculadas);
            }else{
                Swal.fire("Error",objData.msg,"error");
            }
            this.isLoading = false;
        },
        printExcel:function(){
            if(this.selectFormato == 0 || this.txtAnio == "" || this.txtAnio <= 0){
                Swal.fire("Error","Todos los campos con (*) son obligatorios.","error");
                return false;
            }
            window.open("cont-generarExogenaExcel.php?formato="+this.selectFormato+"&fecha="+this.txtAnio,"__blank");
        },
        formatNumero: function(valor){
            return new Intl.NumberFormat("en-US", {style: "currency", currency: "USD"}).format(valor);
        },
    },
    computed:{

    }
})
