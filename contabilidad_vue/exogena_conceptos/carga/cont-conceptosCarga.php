<?php
    require_once '../../../PHPExcel/Classes/PHPExcel.php';
    require_once '../../../comun.inc';
    require '../../../funciones.inc';
    require '../../../funcionesSP.inc.php';
    /*ini_set('display_errors', '1');
    ini_set('display_startup_errors', '1');
    error_reporting(E_ALL);*/
    session_start();
    if($_POST){
        $obj = new Plantilla();
        if($_POST['action']=="upload"){
            $obj->upload();
        }
    }

    class Plantilla{
        private $linkbd;
        private $arrData;
        private $strVigencia;
        public function __construct() {
            $this->linkbd = conectar_v7();
            $this->linkbd->set_charset("utf8");
        }
        public function upload(){
            if(!empty($_SESSION)){
                $tmpfname = $_FILES['file']['tmp_name'];
                $excelReader = PHPExcel_IOFactory::createReaderForFile($tmpfname);
                $excelObj = $excelReader->load($tmpfname);

                $hojaFormatos = $excelObj->getSheet(0);
                $hojaConceptos = $excelObj->getSheet(1);
                $hojaColumnas = $excelObj->getSheet(2);

                $lastRowFormatos = $hojaFormatos->getHighestRow();
                $lastRowConceptos = $hojaConceptos->getHighestRow();
                $lastRowColumnas = $hojaColumnas->getHighestRow();

                $sql = "TRUNCATE TABLE contexogenaconceptos";
                mysqli_query($this->linkbd, $sql);

                $sql = "TRUNCATE TABLE contexogenacolumnas";
                mysqli_query($this->linkbd, $sql);

                $sql = "TRUNCATE TABLE contexogenaformatos";
                mysqli_query($this->linkbd, $sql);

                //Formatos
                for ($i=4; $i <= $lastRowFormatos ; $i++) {
                    $formato = $hojaFormatos->getCell("A$i")->getValue();
                    $nombre = $hojaFormatos->getCell("B$i")->getValue();
                    $this->insertFormatoExogena($formato,$nombre);
                }
                //Conceptos
                for ($i=4; $i <= $lastRowConceptos ; $i++) {
                    $codigo = $hojaConceptos->getCell("B$i")->getValue();
                    $concepto = $hojaConceptos->getCell("C$i")->getValue();
                    $formato = $hojaConceptos->getCell("A$i")->getValue();
                    $this->insertConceptoExogena($codigo,$formato,$concepto);
                }
                //Columnas
                for ($i=4; $i <= $lastRowColumnas ; $i++) {
                    $nombre = $hojaColumnas->getCell("B$i")->getValue();
                    $tipo = $hojaColumnas->getCell("A$i")->getValue();
                    $this->insertColumnaExogena($nombre,$tipo);
                }
                $arrResponse = array("status"=>true,"msg"=>"El archivo ha sido cargado correctamente correctamente.");
                echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
            }
            die();
        }
        public function insertConceptoExogena($codigo,$formato,$concepto){
            $año = date('Y');
            $sql = "INSERT INTO contexogenaconceptos(nombre,estado,vigencia,formato,codigo)
            VALUES('$concepto','S','$año','$formato','$codigo')";
            $request = mysqli_query($this->linkbd,$sql);
            return $request;
        }
        public function insertColumnaExogena($nombre,$tipo){
            $sql = "INSERT INTO contexogenacolumnas(nombre,tipo,estado)
            VALUES('$nombre',$tipo,'S')";
            $request = mysqli_query($this->linkbd,$sql);
            return $request;
        }
        public function insertFormatoExogena($formato,$nombre){
            $sql = "INSERT INTO contexogenaformatos(formato,nombre,estado)
            VALUES('$formato','$nombre','S')";
            $request = mysqli_query($this->linkbd,$sql);
            return $request;
        }

    }
?>
