const URL ='contabilidad_vue/exogena_conceptos/carga/cont-conceptosCarga.php';

var app = new Vue({
    el:"#myapp",
    data() {
        return {
            isLoading:false,
            isModal:false,
            inputFile:""
        }
    },
    mounted() {

    },
    methods: {
        upload:async function(){
            let inputFile = this.inputFile;
            if(inputFile.length == 0){
                Swal.fire("Error","Debe subir la plantilla.","error");
                return false;
            }
            let file = inputFile[0];
            let extension = file.name.split(".")[1];
            if(extension != "xlsx"){
                Swal.fire("Error","El archivo es incorrecto; por favor, utiliza nuestra plantilla.","error");
                return false;
            }
            let formData = new FormData();
            formData.append("action","upload");
            formData.append("file",file);
            Swal.fire({
                title:"¿Estás segur@ de cargar?",
                text:"",
                icon: 'warning',
                showCancelButton:true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText:"Sí, guardar",
                cancelButtonText:"No, cancelar"
            }).then(async function(result){
                if(result.isConfirmed){
                    const response = await fetch(URL,{method:"POST",body:formData});
                    const objData = await response.json();
                    if(objData.status){
                        Swal.fire("Guardado",objData.msg,"success");
                    }else{
                        Swal.fire("Error",objData.msg,"error");
                    }
                }
            });
        },
        getFile:function(element){
            this.inputFile = element.event.target.files;
        },
        printExcel:function(){
            window.open("cont-conceptosExogenaCargaExcel.php");
        },
    },
    computed:{

    }
})
