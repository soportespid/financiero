const URL ='contabilidad_vue/exogena_conceptos/editar/cont-conceptosEditar.php';

var app = new Vue({
    el:"#myapp",
    data() {
        return {
            isLoading:false,
            isModal:false,
            txtSearch:"",
            txtResults:0,
            txtConsecutivo:0,
            txtMax:0,
            arrCuentas:[],
            arrCuentasCopy:[],
            arrFormatos:[],
            arrConceptos:[],
            arrConceptosFormato:[],
            arrCuentasDetalle:[],
            selectFormato:0,
            selectConcepto:0,
            selectTipo:1,
            arrTipoNombre:{1:"Debito",2:"Credito",3:"Saldo",4:"Debito/Credito"},
            objConcepto:{"codigo":0,"nombre":""},
            objCuenta:{"cuenta":"","nombre":""},
            inputFile:""
        }
    },
    mounted() {
        this.getData();
    },
    methods: {
        getData:async function(){
            let codigo = new URLSearchParams(window.location.search).get('id');
            const formData = new FormData();
            formData.append("action","get");
            formData.append("codigo",codigo);
            this.isLoading = true;
            const response = await fetch(URL,{method:"POST",body:formData});
            const objData = await response.json();
            if(objData.status){
                this.arrFormatos = objData.data.formatos;
                this.arrConceptos = objData.data.conceptos;
                this.arrCuentas = objData.data.cuentas.data;
                this.arrCuentasCopy = objData.data.cuentas.data;
                this.txtResults = objData.data.cuentas.total;
                this.txtMax = objData.data.max;
                const cabecera = objData.data.concepto.cabecera;
                const detalle = objData.data.concepto.detalle;
                this.txtConsecutivo = cabecera.id;
                this.selectFormato = cabecera.formato;
                this.arrConceptosFormato = [...this.arrConceptos.filter(e => e.formato == this.selectFormato)];
                this.selectConcepto = cabecera.codigo;
                this.objConcepto = this.arrConceptosFormato.filter(e => e.codigo == this.selectConcepto)[0];

                detalle.forEach(e => {
                    e['tipo'] = this.arrTipoNombre[e.tipo_id];
                    this.arrCuentasDetalle.push(e);
                });
                this.isLoading = false;
            }else{
                window.location.href='cont-conceptosExogenaEditar.php?id='+obj.data;
            }

        },
        getConceptos:function(){
            this.arrConceptosFormato = [...this.arrConceptos.filter(e => e.formato == this.selectFormato)];
            this.selectConcepto = 0;
            this.objConcepto={"codigo":0,"nombre":""};
        },
        getConceptoNombre:function(){
            this.objConcepto = this.arrConceptosFormato.filter(e => e.codigo == this.selectConcepto)[0];
        },
        search:function(type=""){
            let search = this.txtSearch.toLowerCase();
            if(type=="modal_cuenta"){
                this.arrCuentasCopy = [...this.arrCuentas.filter(e=>e.cuenta.toLowerCase().includes(search)
                    || e.nombre.toLowerCase().includes(search))];
                this.txtResults = this.arrCuentasCopy.length;
            }else{
                let arr = [...this.arrCuentas.filter(e=>e.cuenta.toLowerCase() == this.objCuenta.cuenta)];
                this.objCuenta = arr.length > 0 ? arr[0] : {"cuenta":"","nombre":"No existe"};
            }
        },
        save:async function(){
            let total = this.arrCuentasDetalle.length;
            if(this.selectConcepto == 0 || this.selectFormato == 0){
                Swal.fire("Error","Todos los campos con (*) son obligatorios.","error");
                return false;
            }
            if(total == 0){
                Swal.fire("Error","Debe asignar al menos una cuenta.","error");
                return false;
            }
            let data = {
                "cabecera":{
                    "id":this.txtConsecutivo,
                    "codigo":this.selectConcepto,
                    "formato":this.selectFormato
                },
                "detalle":this.arrCuentasDetalle
            }
            const formData = new FormData();
            formData.append("action","save");
            formData.append("data",JSON.stringify(data));
            Swal.fire({
                title:"¿Estás segur@ de guardar?",
                text:"",
                icon: 'warning',
                showCancelButton:true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText:"Sí, guardar",
                cancelButtonText:"No, cancelar"
            }).then(async function(result){
                if(result.isConfirmed){
                    const response = await fetch(URL,{method:"POST",body:formData});
                    const objData = await response.json();
                    if(objData.status){
                        Swal.fire("Guardado",objData.msg,"success");
                    }else{
                        Swal.fire("Error",objData.msg,"error");
                    }
                }
            });
        },
        editItem:function(id){
            window.location.href='cont-conceptosExogenaEditar.php?id='+id;
        },
        selectItem:function({...item}){
            this.objCuenta = item;
            this.isModal = false;
        },
        addItem:function(){
            let obj = Object.assign({},this.objCuenta);
            if(obj.cuenta != ""){
                let flag = this.arrCuentasDetalle.filter(e=>e.cuenta == obj.cuenta).length > 0  ? true : false;
                if(!flag){
                    obj['tipo'] = this.arrTipoNombre[this.selectTipo];
                    obj['tipo_id'] = this.selectTipo;
                    this.arrCuentasDetalle.push(obj);
                    this.objCuenta={"cuenta":"","nombre":""};
                }else{
                    Swal.fire("Error","Esta cuenta ya fue agregada, pruebe con otra.","error");
                }
            }else{
                Swal.fire("Error","La cuenta está vacía o no es válida, pruebe con otra.","error");
            }
        },
        delItem:function({...item}){
            let index = this.arrCuentasDetalle.findIndex(e => e.cuenta == item.cuenta);
            this.arrCuentasDetalle.splice(index,1);
        }
    },
    computed:{

    }
})
