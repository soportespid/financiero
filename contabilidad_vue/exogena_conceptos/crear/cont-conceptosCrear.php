<?php
    require_once '../../../PHPExcel/Classes/PHPExcel.php';
    require_once '../../../comun.inc';
    require '../../../funciones.inc';
    require '../../../funcionesSP.inc.php';
    /*ini_set('display_errors', '1');
    ini_set('display_startup_errors', '1');
    error_reporting(E_ALL);*/
    session_start();
    if($_POST){
        $obj = new Plantilla();
        if($_POST['action']=="get"){
            $obj->getData();
        }else if($_POST['action']=="save"){
            $obj->save();
        }
    }

    class Plantilla{
        private $linkbd;
        private $arrData;
        private $strVigencia;
        public function __construct() {
            $this->linkbd = conectar_v7();
            $this->linkbd->set_charset("utf8");
        }
        public function getData(){
            if(!empty($_SESSION)){
                $request['conceptos'] = $this->selectConceptos();
                $request['formatos'] = $this->selectFormatos();
                $request['cuentas'] = $this->selectCuentas();
                echo json_encode($request,JSON_UNESCAPED_UNICODE);
            }
            die();
        }
        public function save(){
            if(!empty($_SESSION)){
                $arrData = json_decode($_POST['data'],true);
                if(empty($arrData)){
                    $arrResponse = array("status"=>false,"msg"=>"Error de datos");
                }else{
                    $request = $this->insertConcepto($arrData['cabecera']);
                    if(is_numeric($request) && $request > 0){
                        $request = $this->insertConceptoDet($arrData['detalle']);
                        if(is_numeric($request) && $request > 0){
                            $arrResponse = array("status"=>true,"msg"=>"Datos guardados correctamente.");
                        }else{
                            $arrResponse = array("status"=>false,"msg"=>"Error en detalle");
                        }
                    }else if($request =="existe"){
                        $arrResponse = array("status"=>false,"msg"=>"El concepto ya existe, pruebe con otro.");
                    }else{
                        $arrResponse = array("status"=>false,"msg"=>"Error en cabecera");
                    }
                }
                echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
            }
            die();
        }
        public function insertConcepto($data){
            $sql = "SELECT * FROM contexogenaconce_cab WHERE codigo = '$data[codigo]' AND formato = '$data[formato]'";
            $request = mysqli_fetch_all(mysqli_query($this->linkbd,$sql),MYSQLI_ASSOC);
            if(empty($request)){
                $sql ="INSERT INTO contexogenaconce_cab(codigo,formato,usuario,estado)
                VALUES('$data[codigo]','$data[formato]','$_SESSION[usuario]','S')";
                $request = intval(mysqli_query($this->linkbd,$sql));
            }else{
                $request = "existe";
            }
            return $request;
        }
        public function insertConceptoDet($data){
            $id = mysqli_query($this->linkbd,"SELECT MAX(id) as id FROM contexogenaconce_cab")->fetch_assoc()['id'];
            foreach ($data as $d) {
                $sql ="INSERT INTO contexogenaconce_det(codigo,cuenta,nombre,tipo)
                VALUES($id,'$d[cuenta]','$d[nombre]','$d[tipo_id]')";
                $request = intval(mysqli_query($this->linkbd,$sql));
            }
            return $request;
        }
        public function selectFormatos(){
            $sql = "SELECT * FROM contexogenaformatos WHERE estado ='S' ORDER BY formato";
            $request = mysqli_fetch_all(mysqli_query($this->linkbd,$sql),MYSQLI_ASSOC);
            return $request;
        }
        public function selectConceptos(){
            $sql = "SELECT * FROM contexogenaconceptos WHERE estado ='S' ORDER BY codigo";
            $request = mysqli_fetch_all(mysqli_query($this->linkbd,$sql),MYSQLI_ASSOC);
            return $request;
        }
        public function selectCuentas(){
            $sql ="SELECT * FROM cuentasnicsp WHERE tipo = 'Mayor' AND LENGTH(cuenta) <= 6 AND estado = 'S'";
            $request = mysqli_fetch_all(mysqli_query($this->linkbd,$sql),MYSQLI_ASSOC);
            return array("data"=>$request,"total"=>count($request));
        }
    }
?>
