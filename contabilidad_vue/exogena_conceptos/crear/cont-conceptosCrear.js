const URL ='contabilidad_vue/exogena_conceptos/crear/cont-conceptosCrear.php';

var app = new Vue({
    el:"#myapp",
    data() {
        return {
            isLoading:false,
            isModal:false,
            txtSearch:"",
            txtResults:0,
            arrCuentas:[],
            arrCuentasCopy:[],
            arrFormatos:[],
            arrConceptos:[],
            arrConceptosFormato:[],
            arrCuentasDetalle:[],
            selectFormato:0,
            selectConcepto:0,
            selectTipo:1,
            arrTipoNombre:{1:"Debito",2:"Credito",3:"Saldo",4:"Debito/Credito"},
            objConcepto:{"codigo":0,"nombre":""},
            objCuenta:{"cuenta":"","nombre":""},
            inputFile:""
        }
    },
    mounted() {
        this.getData();
    },
    methods: {
        getData:async function(){
            const formData = new FormData();
            formData.append("action","get");
            this.isLoading = true;
            const response = await fetch(URL,{method:"POST",body:formData});
            const objData = await response.json();
            this.arrFormatos = objData.formatos;
            this.arrConceptos = objData.conceptos;
            this.arrCuentas = objData.cuentas.data;
            this.arrCuentasCopy = objData.cuentas.data;
            this.txtResults = objData.cuentas.total;
            this.isLoading = false;
        },
        getConceptos:function(){
            this.arrConceptosFormato = [...this.arrConceptos.filter(e => e.formato == this.selectFormato)];
            this.selectConcepto = 0;
            this.objConcepto={"codigo":0,"nombre":""};
        },
        getConceptoNombre:function(){
            this.objConcepto = this.arrConceptosFormato.filter(e => e.codigo == this.selectConcepto)[0];
        },
        search:function(type=""){
            let search = this.txtSearch.toLowerCase();
            if(type=="modal_cuenta"){
                this.arrCuentasCopy = [...this.arrCuentas.filter(e=>e.cuenta.toLowerCase().includes(search)
                    || e.nombre.toLowerCase().includes(search))];
                this.txtResults = this.arrCuentasCopy.length;
            }else{
                let arr = [...this.arrCuentas.filter(e=>e.cuenta.toLowerCase() == this.objCuenta.cuenta)];
                this.objCuenta = arr.length > 0 ? arr[0] : {"cuenta":"","nombre":"No existe"};
            }
        },
        save:async function(){
            let total = this.arrCuentasDetalle.length;
            if(this.selectConcepto == 0 || this.selectFormato == 0){
                Swal.fire("Error","Todos los campos con (*) son obligatorios.","error");
                return false;
            }
            if(total == 0){
                Swal.fire("Error","Debe asignar al menos una cuenta.","error");
                return false;
            }
            let data = {
                "cabecera":{
                    "codigo":this.selectConcepto,
                    "formato":this.selectFormato
                },
                "detalle":this.arrCuentasDetalle
            }
            const formData = new FormData();
            formData.append("action","save");
            formData.append("data",JSON.stringify(data));
            Swal.fire({
                title:"¿Estás segur@ de guardar?",
                text:"",
                icon: 'warning',
                showCancelButton:true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText:"Sí, guardar",
                cancelButtonText:"No, cancelar"
            }).then(async function(result){
                if(result.isConfirmed){
                    const response = await fetch(URL,{method:"POST",body:formData});
                    const objData = await response.json();
                    if(objData.status){
                        Swal.fire("Guardado",objData.msg,"success");
                        setTimeout(function(){
                            window.location.reload();
                        },2000);
                    }else{
                        Swal.fire("Error",objData.msg,"error");
                    }
                }
            });
        },
        selectItem:function({...item}){
            this.objCuenta = item;
            this.isModal = false;
        },
        addItem:function(){
            let obj = Object.assign({},this.objCuenta);
            if(obj.cuenta != ""){
                let flag = this.arrCuentasDetalle.filter(e=>e.cuenta == obj.cuenta).length > 0  ? true : false;
                if(!flag){
                    obj['tipo'] = this.arrTipoNombre[this.selectTipo];
                    obj['tipo_id'] = this.selectTipo;
                    this.arrCuentasDetalle.push(obj);
                    this.objCuenta={"cuenta":"","nombre":""};
                }else{
                    Swal.fire("Error","Esta cuenta ya fue agregada, pruebe con otra.","error");
                }
            }else{
                Swal.fire("Error","La cuenta está vacía o no es válida, pruebe con otra.","error");
            }
        },
        delItem:function({...item}){
            let index = this.arrCuentasDetalle.findIndex(e => e.cuenta == item.cuenta);
            this.arrCuentasDetalle.splice(index,1);
        }
    },
    computed:{

    }
})
