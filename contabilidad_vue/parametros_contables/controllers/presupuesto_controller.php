<?php
    require_once '../../../Librerias/core/Helpers.php';
    require_once '../models/presupuesto_model.php';
    session_start();
    header('Content-Type: application/json');
    class presupuestoController extends presupuestoModel {
        /* crear */
        public function get() {
            if (!empty($_SESSION)) {
                $tipo = $_POST["tipo"];
                $modulo = $_POST["modulo"];
                $arrResponse = array(
                    "consecutivo" => $this->get_consec($tipo, $modulo),
                    "cuentasCredito" => $this->get_cuentas_credito(),
                    "destinosCompras" => $this->get_destinos_compra(),
                    "centrosCostos" => $this->get_centros_costos(),
                );
                echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);    
            }
            die();
        }

        public function get_debito() {
            if (!empty($_SESSION)) {
                $destinoCompra = $_POST["codDestino"];
                $arrResponse = array(
                    "cuentasDebito" => $this->get_cuentas_debito($destinoCompra)
                );
                echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);    
            }
            die();
        }

        public function save() {
            if (!empty($_SESSION)) {
                $tipo = $_POST["tipo"];
                $modulo = $_POST["modulo"];
                $nombre = $_POST["nombre"];
                $arrCredito = json_decode($_POST["arrCredito"], true);
                $arrDebito = json_decode($_POST["arrDebito"], true);

                $this->create_programacion($nombre, $tipo, $arrCredito, $arrDebito, $modulo);

                
                $arrResponse = array(
                    "status" => true,
                    "msg" => "Guardado realizado con exito" 
                );
                echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);    
            }
            die();
        }

        /* buscar */

        public function getDataSearch() {
            if (!empty($_SESSION)) {
                $tipo = $_POST["tipo"];
                $modulo = $_POST["modulo"];
                $arrResponse = array(
                    "data" => $this->get_search($tipo, $modulo)
                );
                echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);    
            }
            die();
        }

        /* editar */
        public function getEdit() {
            if (!empty($_SESSION)) {
                $tipo = $_POST["tipo"];
                $modulo = $_POST["modulo"];
                $cod = $_POST["cod"];
                $arrResponse = array(
                    "nombre" => $this->get_data_cab($tipo, $cod, $modulo),
                    "arrCredito" => $this->get_arr_credito($tipo, $cod, $modulo),
                    "arrDebito" => $this->get_arr_debito($tipo, $cod, $modulo),
                    "consecutivos" => $this->get_consecutivos($tipo, $modulo),
                    "cuentasCredito" => $this->get_cuentas_credito(),
                    "destinosCompras" => $this->get_destinos_compra(),
                    "centrosCostos" => $this->get_centros_costos(),
                );
                echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);    
            }
            die();
        }

        public function saveEdit() {
            if (!empty($_SESSION)) {
                $codigo = $_POST["codigo"];
                $tipo = $_POST["tipo"];
                $modulo = $_POST["modulo"];
                $nombre = $_POST["nombre"];
                $arrCredito = json_decode($_POST["arrCredito"], true);
                $arrDebito = json_decode($_POST["arrDebito"], true);

                $this->update_programacion($codigo, $nombre, $tipo, $arrCredito, $arrDebito, $modulo);

                $arrResponse = array(
                    "status" => true,
                    "msg" => "Guardado realizado con exito" 
                );
                echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);    
            }
            die();
        }
    }

    if($_POST){
        $obj = new presupuestoController();
        $accion = $_POST["action"];
        
        if ($accion == "get") {
            $obj->get();
        } else if ($accion=="get_debito") {
            $obj->get_debito();   
        } else if ($accion == "save") {
            $obj->save();
        } else if ($accion == "getSearch") {
            $obj->getDataSearch();
        } else if ($accion == "getEdit") {
            $obj->getEdit();
        } else if ($accion == "update") {
            $obj->saveEdit();
        }
    }
?>