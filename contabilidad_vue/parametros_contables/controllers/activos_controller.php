<?php
    require_once '../../../Librerias/core/Helpers.php';
    require_once '../models/activos_model.php';
    session_start();

    class ActivosController extends ActivosModel{

        public function initialData(){
            if(!empty($_SESSION)){
                $arrResponse = array(
                    "cuentas"=>$this->selectCuentas(),
                    "centros"=>$this->selectCentroCostos(),
                    "disposiciones"=>$this->selectDisposiciones(),
                    "activos"=>$this->selectActivos()
                );
                echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
            }
            die();
        }
        public function setData(){
            if(!empty($_SESSION)){
                if($_POST){
                    if(empty($_POST['nombre']) || empty($_POST['data'])){
                        $arrResponse = array("status"=>false,"msg"=>"Error de datos");
                    }else{
                        $intId = strClean($_POST['consecutivo']);
                        $strClase = strClean($_POST['clase']);
                        $strNombre = strClean($_POST['nombre']);
                        $strGrupo = strClean($_POST['grupo']);
                        $strTipo = strClean($_POST['tipo']);
                        $strTipoDet = $strClase.$strGrupo.$strTipo;
                        $arrData = json_decode($_POST['data'],true);

                        if(count($arrData) > 0){
                            if($intId == ""){
                                $option = 1;
                                $request = $this->insertData($strNombre);
                            }else{
                                $option = 2;
                                $request = $this->updateData($intId,$strNombre);
                                if($request > 0){
                                    $request = $intId;
                                }
                            }
                            if($request > 0){
                                $request_det = $this->insertDataDetalle($request,$arrData,$strTipoDet);
                                if($request_det > 0){
                                    if($option == 1){
                                        $arrResponse = array("status"=>true,"msg"=>"Los datos se han guardado correctamente.","id"=>$request);
                                    }else{
                                        $arrResponse = array("status"=>true,"msg"=>"Los datos se han actualizado correctamente.");
                                    }
                                }else{
                                    $arrResponse = array("status"=>false,"msg"=>"Ha ocurrido un error en el detalle.");
                                }
                            }else if($request=="existe"){
                                $arrResponse = array("status"=>false,"msg"=>"El activo ya existe, pruebe con otro.");
                            }else{
                                $arrResponse = array("status"=>false,"msg"=>"Ha ocurrido un error en la cabecera, intente de nuevo.");
                            }
                        }else{
                            $arrResponse = array("status"=>false,"msg"=>"El concepto no puede guardarse sin cuentas contables.");
                        }
                    }
                    echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
                }
            }
            die();
        }
        public function getSearch(){
            if(!empty($_SESSION)){
                $arrActivos = $this->selectActivos();
                $request = $this->selectSearch();
                if(!empty($request)){
                    $total = count($request);
                    for ($i=0; $i < $total ; $i++) {
                        $element = $request[$i];
                        $nombreClase = strtolower(array_values(array_filter($arrActivos,function($e) use($element){
                            return $e['codigo']==$element['clase'] && $e['niveluno'] == 0;
                        }))[0]['nombre']);
                        $nombreGrupo = array_values(array_filter($arrActivos,function($e) use($element){
                            return $e['codigo']==$element['grupo'] && $e['niveluno'] == $element['clase'];
                        }))[0]['nombre'];
                        $nombreTipo = array_values(array_filter($arrActivos,function($e) use($element){
                            return $e['codigo']==$element['tipo'] && $e['niveluno'] == $element['grupo'] && $e['niveldos'] == $element['clase'];
                        }))[0]['nombre'];
                        $request[$i]['clase_nombre'] = ucfirst(strtolower($nombreClase));
                        $request[$i]['grupo_nombre'] = ucfirst(strtolower($nombreGrupo));
                        $request[$i]['tipo_nombre'] = ucfirst(strtolower($nombreTipo));
                    }
                }
                echo json_encode($request,JSON_UNESCAPED_UNICODE);
            }
            die();
        }
        public function getInfoEdit(){
            if(!empty($_SESSION)){
                $id = strClean($_POST['codigo']);
                $request = $this->selectInfo($id);
                if(!empty($request)){
                    $arrResponse = array(
                        "status"=>true,
                        "data"=>$request,
                        "consecutivos"=>$this->selectConsecutivos(),
                        "cuentas"=>$this->selectCuentas(),
                        "centros"=>$this->selectCentroCostos(),
                        "disposiciones"=>$this->selectDisposiciones(),
                        "activos"=>$this->selectActivos()
                    );
                }else{
                    $arrResponse = array("status"=>false,"consecutivo"=>$this->selectConsecutivo());
                }
                echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
            }
            die();
        }
        public function changeData(){
            if(!empty($_SESSION)){
                $id = strClean($_POST['id']);
                $estado = strClean($_POST['estado']);
                $request = $this->updateStatus($id,$estado);
                if($request == 1){
                    $arrResponse = array("status"=>true);
                }else{
                    $arrResponse = array("status"=>false,"msg"=>"Error");
                }
                echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
            }
            die();
        }
    }
    if($_POST){
        $obj = new ActivosController();
        if($_POST['action'] == "get"){
            $obj->initialData();
        }else if($_POST['action']=="set"){
            $obj->setData();
        }else if($_POST['action']=="search"){
            $obj->getSearch();
        }else if($_POST['action']=="edit"){
            $obj->getInfoEdit();
        }else if($_POST['action']=="change"){
            $obj->changeData();
        }
    }

?>
