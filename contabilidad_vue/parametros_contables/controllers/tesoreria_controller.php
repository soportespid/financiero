<?php
    require_once '../../../Librerias/core/Helpers.php';
    require_once '../models/tesoreria_model.php';
    session_start();

    class TesoreriaController extends TesoreriaModel{

        public function initialData(){
            if(!empty($_SESSION)){
                $arrResponse = array(
                    "cuentas"=>$this->selectCuentas(),
                    "centros"=>$this->selectCentroCostos()
                );
                echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
            }
            die();
        }
        public function setData(){
            if(!empty($_SESSION)){
                if($_POST){
                    if(empty($_POST['nombre']) || empty($_POST['data'])){
                        $arrResponse = array("status"=>false,"msg"=>"Error de datos");
                    }else{
                        $intId = strClean($_POST['consecutivo']);
                        $intModulo = intval(strClean($_POST['modulo']));
                        $strNombre = strtoupper(strClean(replaceChar($_POST['nombre'])));
                        $strTipo = strtoupper(strClean(replaceChar($_POST['tipo'])));
                        $arrData = json_decode($_POST['data'],true);
                        if(count($arrData) > 0){
                            if($intId == ""){
                                $option = 1;
                                $request = $this->insertData($strNombre,$strTipo,$intModulo);
                            }else{
                                $option = 2;
                                $request = $this->updateData($intId,$strNombre,$strTipo,$intModulo);
                                if($request > 0){
                                    $request = $intId;
                                }
                            }
                            if($request > 0 || $request != ""){
                                $request_det = $this->insertDataDetalle($request,$strTipo,$arrData,$intModulo);
                                if($request_det > 0){
                                    if($option == 1){
                                        $arrResponse = array("status"=>true,"msg"=>"Los datos se han guardado correctamente.","id"=>$request);
                                    }else{
                                        $arrResponse = array("status"=>true,"msg"=>"Los datos se han actualizado correctamente.");
                                    }
                                }else{
                                    $arrResponse = array("status"=>false,"msg"=>"Ha ocurrido un error en el detalle.");
                                }
                            }else if($request=="existe"){
                                $arrResponse = array("status"=>false,"msg"=>"El nombre ya existe, pruebe con otro.");
                            }else{
                                $arrResponse = array("status"=>false,"msg"=>"Ha ocurrido un error en la cabecera, intente de nuevo.");
                            }
                        }else{
                            $arrResponse = array("status"=>false,"msg"=>"El concepto no puede guardarse sin cuentas contables.");
                        }
                    }
                    echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
                }
            }
            die();
        }
        public function getSearch(){
            if(!empty($_SESSION)){
                $strTipo = strtoupper(strClean(replaceChar($_POST['tipo'])));
                $intModulo = intval(strClean($_POST['modulo']));
                $request = $this->selectSearch($strTipo,$intModulo);
                echo json_encode($request,JSON_UNESCAPED_UNICODE);
            }
            die();
        }
        public function getInfoEdit(){
            if(!empty($_SESSION)){
                $id = strClean($_POST['codigo']);
                $strTipo = strtoupper(strClean(replaceChar($_POST['tipo'])));
                $intModulo = intval(strClean($_POST['modulo']));
                $request = $this->selectInfo($id,$strTipo,$intModulo);
                if(!empty($request)){
                    $arrResponse = array(
                        "status"=>true,
                        "data"=>$request,
                        "consecutivos"=>$this->selectConsecutivos($strTipo,$intModulo),
                        "cuentas"=>$this->selectCuentas(),
                        "centros"=>$this->selectCentroCostos()
                    );
                }else{
                    $arrResponse = array("status"=>false,"consecutivo"=>$this->selectConsecutivo($strTipo,$intModulo));
                }
                echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
            }
            die();
        }
    }
    if($_POST){
        $obj = new TesoreriaController();
        if($_POST['action'] == "get"){
            $obj->initialData();
        }else if($_POST['action']=="set"){
            $obj->setData();
        }else if($_POST['action']=="search"){
            $obj->getSearch();
        }else if($_POST['action']=="edit"){
            $obj->getInfoEdit();
        }
    }

?>
