<?php
    require_once '../../../Librerias/core/Helpers.php';
    require_once '../models/humana_terceros_model.php';
    session_start();

    class HumanaTercerosController extends HumanaTercerosModel{

        public function initialData(){
            if(!empty($_SESSION)){
                $arrResponse = array(
                    "cuentas"=>$this->selectCuentas(),
                    "terceros"=>$this->selectTerceros()
                );
                echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
            }
            die();
        }
        public function setData(){
            if(!empty($_SESSION)){
                if($_POST){
                    if(empty($_POST['nombre']) || empty($_POST['data']) || empty($_POST['tercero'])){
                        $arrResponse = array("status"=>false,"msg"=>"Error de datos");
                    }else{
                        $intId = strClean($_POST['consecutivo']);
                        $strNombre = strtoupper(strClean(replaceChar($_POST['nombre'])));
                        $strTercero = strClean($_POST['tercero']);
                        $arrData = json_decode($_POST['data'],true);
                        if(count($arrData) > 0){
                            if($intId == ""){
                                $option = 1;
                                $request = $this->insertData($strNombre,$strTercero);
                            }else{
                                $option = 2;
                                $request = $this->updateData($intId,$strNombre,$strTercero);
                                if($request > 0){
                                    $request = $intId;
                                }
                            }
                            if($request > 0 || $request != ""){
                                $request_det = $this->insertDataDetalle($request,$arrData);
                                if($request_det > 0){
                                    if($option == 1){
                                        $arrResponse = array("status"=>true,"msg"=>"Los datos se han guardado correctamente.","id"=>$request);
                                    }else{
                                        $arrResponse = array("status"=>true,"msg"=>"Los datos se han actualizado correctamente.");
                                    }
                                }else{
                                    $arrResponse = array("status"=>false,"msg"=>"Ha ocurrido un error en el detalle.");
                                }
                            }else if($request=="existe"){
                                $arrResponse = array("status"=>false,"msg"=>"El nombre ya existe, pruebe con otro.");
                            }else{
                                $arrResponse = array("status"=>false,"msg"=>"Ha ocurrido un error en la cabecera, intente de nuevo.");
                            }
                        }else{
                            $arrResponse = array("status"=>false,"msg"=>"El concepto no puede guardarse sin cuentas contables.");
                        }
                    }
                    echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
                }
            }
            die();
        }
        public function getSearch(){
            if(!empty($_SESSION)){
                $request = $this->selectSearch();
                echo json_encode($request,JSON_UNESCAPED_UNICODE);
            }
            die();
        }
        public function getInfoEdit(){
            if(!empty($_SESSION)){
                $id = strClean($_POST['codigo']);
                $request = $this->selectInfo($id);
                if(!empty($request)){
                    $arrResponse = array(
                        "status"=>true,
                        "data"=>$request,
                        "consecutivos"=>$this->selectConsecutivos(),
                        "cuentas"=>$this->selectCuentas(),
                        "terceros"=>$this->selectTerceros()
                    );
                }else{
                    $arrResponse = array("status"=>false,"consecutivo"=>$this->selectConsecutivo());
                }
                echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
            }
            die();
        }
        public function changeData(){
            if(!empty($_SESSION)){
                $id = strClean($_POST['id']);
                $estado = strClean($_POST['estado']);
                $request = $this->updateStatus($id,$estado);
                if($request == 1){
                    $arrResponse = array("status"=>true);
                }else{
                    $arrResponse = array("status"=>false,"msg"=>"Error");
                }
                echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
            }
            die();
        }
    }
    if($_POST){
        $obj = new HumanaTercerosController();
        if($_POST['action'] == "get"){
            $obj->initialData();
        }else if($_POST['action']=="set"){
            $obj->setData();
        }else if($_POST['action']=="search"){
            $obj->getSearch();
        }else if($_POST['action']=="edit"){
            $obj->getInfoEdit();
        }else if($_POST['action']=="change"){
            $obj->changeData();
        }
    }

?>
