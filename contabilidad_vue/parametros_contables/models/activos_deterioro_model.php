<?php
    require_once '../../../Librerias/core/Helpers.php';
    require_once '../../../Librerias/core/Mysql.php';
    session_start();
    /*ini_set('display_errors', '1');
    ini_set('display_startup_errors', '1');
    error_reporting(E_ALL);*/
    class ActivosDeterioroModel extends Mysql{
        private $strNombre;
        private $strTipo;
        private $strId;
        private $intModulo;
        function __construct(){
            parent::__construct();
        }
        public function selectCuentas(){
            $sql ="SELECT cuenta as codigo,nombre,naturaleza FROM cuentasnicsp WHERE tipo = 'Auxiliar' AND LENGTH(cuenta) >= 9 AND estado = 'S'
            ORDER BY cuenta";
            $request = $this->select_all($sql);
            return $request;
        }
        public function selectCentroCostos(){
            $sql = "SELECT id_cc as codigo, nombre FROM centrocosto WHERE estado='S'";
            $request = $this->select_all($sql);
            return $request;
        }
        public function selectActivos(){
            $sql="SELECT * FROM actipo WHERE estado='S'";
            $request = $this->select_all($sql);
            return $request;
        }
        public function insertData(string $strNombre){
            $this->strNombre = $strNombre;;
            $this->strId = $this->selectConsecutivo()+1;
            $return = "";
            $sql = "SELECT * FROM acti_deterioro_cab WHERE nombre = '$this->strNombre'";
            $request = $this->select_all($sql);
            if(empty($request)){
                $sql = "INSERT INTO acti_deterioro_cab(id,nombre,estado) VALUES(?,?,?)";
                $arrData = array($this->strId,$this->strNombre,"S");
                $this->insert($sql,$arrData);
                $return =$this->strId;
                insertAuditoria("cont_auditoria","cont_funciones_id",4,"Crear correccion por deterioro",$return);
            }else{
                $return = "existe";
            }
            return $return;
        }
        public function updateData(string $strId,string $strNombre){
            $this->strNombre = $strNombre;
            $this->strId = $strId;
            $return = "";
            $sql = "SELECT * FROM acti_deterioro_cab
            WHERE nombre = '$this->strNombre' AND id != '$this->strId'";
            $request = $this->select_all($sql);
            if(empty($request)){
                $sql = "UPDATE acti_deterioro_cab SET nombre =?
                WHERE id = '$this->strId'";
                $arrData = array($this->strNombre);
                $return = $this->update($sql,$arrData);
                insertAuditoria("cont_auditoria","cont_funciones_id",4,"Editar correccion por deterioro",$strId);
            }else{
                $return = "existe";
            }
            return $return;
        }
        public function insertDataDetalle(string $strId,array $data,string $strTipo){
            $this->strId = $strId;
            $this->strTipo = $strTipo;
            $sql = "DELETE FROM acti_deterioro_det WHERE id_cab = '$this->strId'";
            $this->delete($sql);
            $request = "";
            foreach ($data as $d) {
                $sql = "INSERT INTO acti_deterioro_det(id_cab,fechainicial,cuenta_debito,cuenta_credito,
                disposicion_activos,centro_costos,estado,tipo)
                VALUES(?,?,?,?,?,?,?,?)";
                $arrData = array(
                    $this->strId,
                    $d['fecha'],
                    $d['cuenta_debito']['codigo'],
                    $d['cuenta_credito']['codigo'],
                    0,
                    $d['centro']['codigo'],
                    'S',
                    $this->strTipo
                );
                $this->insert($sql,$arrData);
                $request = 1;
            }
            return $request;
        }
        public function selectConsecutivo(){
            $sql = "SELECT MAX(id) as codigo FROM acti_deterioro_cab";
            $request = $this->select($sql)['codigo'];
            return $request;
        }
        public function selectConsecutivos(){
            $sql = "SELECT id FROM acti_deterioro_cab";
            $request = $this->select_all($sql);
            return $request;
        }
        public function selectSearch(){
            $sql = "SELECT id as codigo, nombre,estado FROM acti_deterioro_cab";
            $request = $this->select_all($sql);
            if(!empty($request)){
                $total = count($request);
                for ($i=0; $i < $total; $i++) {
                    $id = $request[$i]['codigo'];
                    $sql = "SELECT tipo FROM acti_deterioro_det WHERE id_cab=$id GROUP BY tipo";
                    $request_det = $this->select($sql);
                    $clase ="";
                    $grupo ="";
                    $tipo ="";
                    if(!empty($request_det)){
                        $res = $request_det['tipo'];
                        $clase = substr($res,0,1);
                        $grupo = substr($res,1,2);
                        $tipo = substr($res,3,3);
                    }
                    $request[$i]['clase'] = $clase;
                    $request[$i]['grupo'] = $grupo;
                    $request[$i]['tipo'] = $tipo;
                    $request[$i]['is_status'] = $request[$i]['estado'] =="S"? 1 : 0;
                }
            }
            return $request;
        }
        public function selectInfo(string $strId){
            $this->strId = $strId;
            $sql = "SELECT id as codigo, nombre,estado FROM acti_deterioro_cab WHERE id = $this->strId";
            $request = $this->select($sql);
            if(!empty($request)){
                $sql = "SELECT * FROM acti_deterioro_det WHERE id_cab=$this->strId GROUP BY tipo ORDER BY DATE(fechainicial) DESC";
                $detalle = $this->select_all($sql);
                $clase ="";
                $grupo ="";
                $tipo ="";
                if(!empty($detalle)){
                    $res = $detalle[0]['tipo'];
                    $clase = substr($res,0,1);
                    $grupo = substr($res,1,2);
                    $tipo = substr($res,3,3);
                }
                $request['clase'] = $clase;
                $request['grupo'] = $grupo;
                $request['tipo'] = $tipo;
                $request['detalle'] = $detalle;
            }
            return $request;
        }
        public function updateStatus($id,$estado){
            insertAuditoria("cont_auditoria","cont_funciones_id",4,"Editar estado de correccion por deterioro",$id);
            $sql = "UPDATE acti_activos_cab SET estado = ? WHERE id = $id";
            $request = $this->update($sql,array($estado));
            return $request;
        }
    }
?>
