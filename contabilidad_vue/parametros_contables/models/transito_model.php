<?php
    require_once '../../../Librerias/core/Helpers.php';
    require_once '../../../Librerias/core/Mysql.php';
    session_start();
    /* ini_set('display_errors', '1');
    ini_set('display_startup_errors', '1');
    error_reporting(E_ERROR); */
    class TransitoModel extends Mysql{
        private $strNombre;
        private $strTipo;
        private $strId;
        private $intModulo;
        private $arrTipo;
        function __construct(){
            parent::__construct();
            $this->arrTipo = array(
                "TI"=>"impuestos",
                "TS"=>"sanciones",
                "TT"=>"tramites",
            );
        }
        public function selectCuentas(){
            $sql ="SELECT cuenta as codigo,nombre,naturaleza FROM cuentasnicsp WHERE tipo = 'Auxiliar' AND estado = 'S'
            ORDER BY cuenta";
            $request = $this->select_all($sql);
            return $request;
        }
        public function selectCentroCostos(){
            $sql = "SELECT id_cc as codigo, nombre FROM centrocosto WHERE estado='S'";
            $request = $this->select_all($sql);
            return $request;
        }
        public function insertData(string $strNombre, string $strTipo,int $intModulo){
            $this->intModulo = $intModulo;
            $this->strNombre = $strNombre;
            $this->strTipo = $strTipo;
            $this->strId = $this->selectConsecutivo($this->strTipo,$this->intModulo)+1;
            $return = "";
            $sql = "SELECT * FROM conceptoscontables WHERE nombre = '$this->strNombre' AND modulo = '$this->intModulo' AND tipo = '$this->strTipo'";
            $request = $this->select_all($sql);
            if(empty($request)){
                $sql = "INSERT INTO conceptoscontables(codigo,nombre,modulo,tipo) VALUES(?,?,?,?)";
                $arrData = array($this->strId,$this->strNombre,$this->intModulo,$this->strTipo);
                $this->insert($sql,$arrData);
                $return = $this->strId;
                insertAuditoria("cont_auditoria","cont_funciones_id",1,"Crear ".$this->arrTipo[$this->strTipo],$return);
            }else{
                $return = "existe";
            }
            return $return;
        }
        public function updateData(string $strId,string $strNombre,string $strTipo,int $intModulo){
            $this->intModulo = $intModulo;
            $this->strNombre = $strNombre;
            $this->strId = $strId;
            $this->strTipo = $strTipo;
            $return = "";
            $sql = "SELECT * FROM conceptoscontables
            WHERE nombre = '$this->strNombre' AND tipo = '$this->strTipo' AND modulo = '$this->intModulo' AND codigo != '$this->strId'";
            $request = $this->select_all($sql);
            if(empty($request)){
                $sql = "UPDATE conceptoscontables SET nombre =?
                WHERE  tipo = '$this->strTipo'  AND modulo = '$this->intModulo' AND codigo = '$this->strId'";
                $arrData = array($this->strNombre);
                $return = $this->update($sql,$arrData);
                insertAuditoria("cont_auditoria","cont_funciones_id",1,"Editar ".$this->arrTipo[$this->strTipo],$strId);
            }else{
                $return = "existe";
            }
            return $return;
        }
        public function insertDataDetalle(string $strId,string $strTipo,array $data,int $intModulo){
            $this->intModulo = $intModulo;
            $this->strId = $strId;
            $this->strTipo = $strTipo;
            $sql = "DELETE FROM conceptoscontables_det
            WHERE codigo = '$this->strId' AND modulo = '$this->intModulo' AND tipo = '$this->strTipo'";
            $this->delete($sql);
            $request = "";
            foreach ($data as $d) {
                $sql = "INSERT INTO conceptoscontables_det(codigo,tipo,tipocuenta,cuenta,cc,debito,credito,estado,modulo,fechainicial)
                VALUES(?,?,?,?,?,?,?,?,?,?)";
                $arrData = array(
                    $this->strId,
                    $this->strTipo,
                    "N",
                    $d['cuenta']['codigo'],
                    $d['centro']['codigo'],
                    $d['debito'],
                    $d['credito'],
                    "S",
                    $this->intModulo,
                    $d['fecha']
                );
                $request = $this->insert($sql,$arrData);
            }
            return $request;
        }
        public function selectConsecutivo(string $strTipo,int $intModulo){
            $this->strTipo = $strTipo;
            $this->intModulo = $intModulo;
            $sql = "SELECT MAX(CAST(codigo AS UNSIGNED)) as codigo FROM conceptoscontables WHERE tipo = '$this->strTipo' AND modulo = '$this->intModulo'";
            $request = $this->select($sql)['codigo'];
            return $request;
        }
        public function selectConsecutivos(string $strTipo,int $intModulo){
            $this->strTipo = $strTipo;
            $this->intModulo = $intModulo;
            $sql = "SELECT codigo as id FROM conceptoscontables WHERE tipo = '$this->strTipo' AND modulo = '$this->intModulo'";
            $request = $this->select_all($sql);
            return $request;
        }
        public function selectSearch($strTipo,int $intModulo){
            $this->strTipo = $strTipo;
            $this->intModulo = $intModulo;
            $sql = "SELECT codigo,nombre FROM conceptoscontables WHERE tipo = '$this->strTipo' AND modulo = '$this->intModulo' ORDER BY CAST(codigo AS INT) ASC";
            $request = $this->select_all($sql);
            return $request;
        }
        public function selectInfo(string $strId,string $strTipo,int $intModulo){
            $this->strId = $strId;
            $this->strTipo = $strTipo;
            $this->intModulo = $intModulo;
            $sql = "SELECT nombre,codigo FROM conceptoscontables WHERE codigo = '$strId' AND tipo = '$this->strTipo' AND modulo = '$this->intModulo'";
            $request = $this->select($sql);
            if(!empty($request)){
                $sql = "SELECT codigo,tipo,tipocuenta,cuenta,cc,debito,credito,estado,modulo,fechainicial
                FROM conceptoscontables_det
                WHERE codigo = '$strId' AND tipo = '$this->strTipo' AND estado = 'S' AND modulo = '$this->intModulo' AND cuenta != ''
                ORDER BY DATE(fechainicial) DESC";
                $request['detalle'] = $this->select_all($sql);
            }
            return $request;
        }
    }
?>
