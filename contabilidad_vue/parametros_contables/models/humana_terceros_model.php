<?php
    require_once '../../../Librerias/core/Helpers.php';
    require_once '../../../Librerias/core/Mysql.php';
    /*ini_set('display_errors', '1');
    ini_set('display_startup_errors', '1');
    error_reporting(E_ALL);*/
    session_start();
    class HumanaTercerosModel extends Mysql{
        private $strNombre;
        private $strTercero;
        private $strTipo;
        private $strId;
        private $intModulo;
        private $arrTipo;
        function __construct(){
            parent::__construct();
        }
        public function selectCuentas(){
            $sql ="SELECT cuenta as codigo,nombre,naturaleza FROM cuentasnicsp WHERE tipo = 'Auxiliar' AND estado = 'S'
            ORDER BY cuenta";
            $request = $this->select_all($sql);
            return $request;
        }
        public function insertData(string $strNombre, string $strTercero){
            $this->strNombre = $strNombre;
            $this->strTercero = $strTercero;
            $this->strId = $this->selectConsecutivo()+1;
            $return = "";
            $sql = "SELECT * FROM humvariablesretenciones WHERE nombre = '$this->strNombre'";
            $request = $this->select_all($sql);
            if(empty($request)){
                $sql = "INSERT INTO humvariablesretenciones(codigo,nombre,beneficiario,estado) VALUES(?,?,?,?)";
                $arrData = array($this->strId,$this->strNombre,$this->strTercero,"S");
                $this->insert($sql,$arrData);
                $return = $this->strId;
                insertAuditoria("cont_auditoria","cont_funciones_id",2,"Crear concepto contable retencion - tercero",$return);
            }else{
                $return = "existe";
            }
            return $return;
        }
        public function updateData(string $strId,string $strNombre,string $strTercero){
            $this->strNombre = $strNombre;
            $this->strId = $strId;
            $this->strTercero = $strTercero;
            $return = "";
            $sql = "SELECT * FROM humvariablesretenciones
            WHERE nombre = '$this->strNombre' AND codigo != '$this->strId'";
            $request = $this->select_all($sql);
            if(empty($request)){
                $sql = "UPDATE humvariablesretenciones SET nombre =?, beneficiario=?
                WHERE codigo = '$this->strId'";
                $return = $this->update($sql,array($this->strNombre,$this->strTercero));
                insertAuditoria("cont_auditoria","cont_funciones_id",2,"Editar concepto contable retencion - tercero",$strId);
            }else{
                $return = "existe";
            }
            return $return;
        }
        public function insertDataDetalle(string $strId,array $data){
            $this->strId = $strId;
            $sql = "DELETE FROM humvariablesretenciones_det
            WHERE codigo = '$this->strId' AND modulo = '2' AND tipo = 'R'";
            $this->delete($sql);
            $request = "";
            foreach ($data as $d) {
                $sql = "INSERT INTO humvariablesretenciones_det(codigo,tipo,tipocuenta,cuenta,debito,credito,estado,modulo,fechainicial)
                VALUES(?,?,?,?,?,?,?,?,?)";
                $arrData = array(
                    $this->strId,
                    "R",
                    "N",
                    $d['cuenta']['codigo'],
                    $d['debito'],
                    $d['credito'],
                    "S",
                    "2",
                    $d['fecha']
                );
                $request = $this->insert($sql,$arrData);
            }
            return $request;
        }
        public function selectConsecutivo(){
            $sql = "SELECT MAX(CAST(codigo AS UNSIGNED)) as codigo FROM humvariablesretenciones";
            $request = $this->select($sql)['codigo'];
            return $request;
        }
        public function selectConsecutivos(){
            $sql = "SELECT codigo as id FROM humvariablesretenciones";
            $request = $this->select_all($sql);
            return $request;
        }
        public function selectSearch(){
            $sql = "SELECT h.codigo,h.nombre,
            CONCAT(t.razonsocial,' ',t.nombre1,' ',t.nombre2,' ',t.apellido1,' ',t.apellido2) as nombre_tercero,
            h.beneficiario,
            h.estado
            FROM humvariablesretenciones h
            INNER JOIN terceros t ON h.beneficiario = t.cedulanit";
            $request = $this->select_all($sql);
            $total = count($request);
            for ($i=0; $i < $total; $i++) {
                $request[$i]['is_status'] = $request[$i]['estado'] =="S"? 1 : 0;
            }
            return $request;
        }
        public function selectInfo(string $strId){
            $this->strId = $strId;
            $sql = "SELECT nombre,codigo,beneficiario FROM humvariablesretenciones WHERE codigo = '$strId'";
            $request = $this->select($sql);
            if(!empty($request)){
                $sql = "SELECT * FROM humvariablesretenciones_det WHERE codigo = '$strId' AND estado = 'S' AND cuenta != '' ORDER BY DATE(fechainicial) DESC";
                $request['detalle'] = $this->select_all($sql);
            }
            return $request;
        }
        public function selectTerceros(){
            $sql = "SELECT CONCAT(razonsocial,' ',nombre1,' ',nombre2,' ',apellido1,' ',apellido2) as nombre,
                cedulanit as codigo FROM terceros";
            $request = $this->select_all($sql);
            return $request;
        }
        public function updateStatus($id,$estado){
            insertAuditoria("cont_auditoria","cont_funciones_id",2,"Editar estado de concepto contable retencion - tercero",$id);
            $sql = "UPDATE humvariablesretenciones SET estado = ? WHERE codigo = $id";
            $request = $this->update($sql,array($estado));
            return $request;
        }
    }
?>
