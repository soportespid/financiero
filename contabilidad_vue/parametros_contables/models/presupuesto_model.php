<?php
    require_once '../../../Librerias/core/Helpers.php';
    require_once '../../../Librerias/core/Mysql.php';
    require_once '../../../funciones.inc';
    session_start();
    
    class presupuestoModel extends Mysql {
        private $cedula;
        private $vigencia;
        public $digitosAuxiliar;
        public $modulo = 3;

        function __construct(){
            parent::__construct();
            $this->cedula = $_SESSION["cedulausu"];
            $this->vigencia = getVigenciaUsuario($this->cedula);
            $this->digitosAuxiliar = $this->nivelCuentas();
        }

        /* Read data */
        public function get_consec($tipo, $moduloRef = 0) {
            $this->modulo = $moduloRef == 0 ? 3 : $moduloRef;
            $sql = "SELECT MAX(codigo) AS consecutivo FROM conceptoscontables WHERE modulo = $this->modulo AND tipo = '$tipo'";
            $data = $this->select($sql);
            $consecutivo = $data["consecutivo"] + 1;

            if ($consecutivo <= 9) {
                $consecutivo = "0".$consecutivo;
            }

            return $consecutivo;
        }

        private function nivelCuentas() {
            $sql = "SELECT posiciones FROM nivelesctas WHERE id_nivel = 5";
            $request = $this->select($sql);
            return $request["posiciones"];
        }

        public function get_cuentas_credito() {
            $sql = "SELECT cuenta, nombre, tipo FROM (SELECT cn1.cuenta,cn1.nombre,cn1.naturaleza,cn1.centrocosto,cn1.tercero,cn1.tipo,cn1.estado FROM cuentasnicsp AS cn1 INNER JOIN cuentasnicsp AS cn2 ON cn2.tipo='Auxiliar' AND cn2.naturaleza='CREDITO' AND cn2.cuenta LIKE CONCAT(cn1.cuenta, '%' ) WHERE cn1.tipo='Mayor' AND cn1.naturaleza='CREDITO' GROUP BY cn1.cuenta UNION SELECT cuenta,nombre,naturaleza,centrocosto,tercero,tipo,estado FROM cuentasnicsp WHERE tipo='Auxiliar' AND naturaleza='CREDITO') AS tabla ORDER BY 1";
            $data = $this->select_all($sql);
            return $data;
        }

        public function get_destinos_compra() {
            $sql = "SELECT codigo, nombre FROM almdestinocompra WHERE estado = 'S' ORDER BY codigo"; 
            $data = $this->select_all($sql);
            return $data;
        }

        public function get_centros_costos() {
            $sql = "SELECT id_cc AS codigo, nombre FROM centrocosto WHERE estado='S' ORDER BY id_cc";
            $data = $this->select_all($sql);
            return $data;
        }

        public function get_cuentas_debito($destinoCompra) {
            $cuentas = array();
            $sql = "SELECT cuenta_inicial, cuenta_final FROM almdestinocompra_det WHERE codigo = '$destinoCompra'";
            $destinos = $this->select_all($sql);

            foreach ($destinos as $destino) {
                $sql = "SELECT * 
                FROM 
                (SELECT cn1.cuenta,cn1.nombre,cn1.naturaleza,cn1.centrocosto,cn1.tercero,cn1.tipo,cn1.estado 
                FROM cuentasnicsp AS cn1 
                INNER JOIN cuentasnicsp AS cn2 
                ON cn2.tipo='Auxiliar' 
                AND cn2.cuenta LIKE CONCAT( cn1.cuenta, '%' )
                WHERE cn1.tipo='Mayor' 
                AND (cn1.cuenta BETWEEN '$destino[cuenta_inicial]' AND '$destino[cuenta_final]')
                OR cn1.cuenta LIKE '$destino[cuenta_final]%'
                GROUP BY cn1.cuenta 
                UNION SELECT cuenta,nombre,naturaleza,centrocosto,tercero,tipo,estado 
                FROM cuentasnicsp
                WHERE tipo='Auxiliar' 
                AND (cuenta BETWEEN '$destino[cuenta_inicial]' AND '$destino[cuenta_final]')
                OR cuenta LIKE '$destino[cuenta_final]%') AS tabla ORDER BY 1";

                $data = $this->select_all($sql);

                $cuentas = array_merge($cuentas, $data);
            }

            return $cuentas;
        }

        public function get_search($tipo, $moduloRef = 0) {
            $this->modulo = $moduloRef == 0 ? $this->modulo : $moduloRef;
            $sql = "SELECT codigo, nombre FROM conceptoscontables WHERE modulo = $this->modulo AND tipo = '$tipo' ORDER BY codigo DESC";
            $data = $this->select_all($sql);
            return $data;
        }

        public function get_data_cab($tipo, $codigo, $moduloRef = 0) {
            $this->modulo = $moduloRef == 0 ? $this->modulo : $moduloRef;
            $sql = "SELECT nombre FROM conceptoscontables WHERE modulo = $this->modulo AND tipo = '$tipo' AND codigo = '$codigo' ORDER BY codigo DESC";
            $data = $this->select($sql);
            return $data["nombre"];
        }

        public function get_arr_credito($tipo, $codigo, $moduloRef = 0) {
            $this->modulo = $moduloRef == 0 ? $this->modulo : $moduloRef;
            $sql = "SELECT concepto.fechainicial AS fecha, concepto.cuenta, cuenta.nombre
            FROM conceptoscontables_det AS concepto 
            INNER JOIN cuentasnicsp AS cuenta
            ON concepto.cuenta = cuenta.cuenta
            WHERE concepto.modulo = $this->modulo 
            AND concepto.tipo = '$tipo' 
            AND concepto.codigo = '$codigo'
            AND concepto.credito = 'S'
            ORDER BY concepto.id_det ASC";
            $data = $this->select_all($sql);
            return $data;
        }

        private function get_name_cuenta($cuenta) {
            $sql = "SELECT nombre FROM cuentasnicsp WHERE cuenta = '$cuenta'";
            $data = $this->select($sql);
            return $data["nombre"];
        }

        public function get_arr_debito($tipo, $codigo, $moduloRef = 0) {
            $this->modulo = $moduloRef == 0 ? $this->modulo : $moduloRef;
            $sql = "SELECT concepto.fechainicial AS fecha, concepto.cuenta, cuenta.nombre, concepto.cc AS centroCosto, concepto.destino_compra AS destinoCompra, concepto.asociado_a AS cuentaAsociada
            FROM conceptoscontables_det AS concepto 
            INNER JOIN cuentasnicsp AS cuenta
            ON concepto.cuenta = cuenta.cuenta
            WHERE concepto.modulo = $this->modulo 
            AND concepto.tipo = '$tipo' 
            AND concepto.codigo = '$codigo'
            AND concepto.debito = 'S'
            ORDER BY concepto.id_det ASC";
            $data = $this->select_all($sql);

            foreach ($data as &$d) {
                $d["nombreAsociada"] = $this->get_name_cuenta($d["cuentaAsociada"]);
            }
            return $data;
        }

        public function get_consecutivos($tipo, $moduloRef = 0) {
            $this->modulo = $moduloRef == 0 ? $this->modulo : $moduloRef;
            $sql = "SELECT codigo FROM conceptoscontables WHERE modulo = $this->modulo AND tipo = '$tipo'";
            $data = $this->select_all($sql);
            return $data;
        }

        /* Create data */
        public function create_programacion($nombre, $tipo, $arrCredito, $arrDebito, $moduloRef = 0) {
            $this->modulo = $moduloRef == 0 ? $this->modulo : $moduloRef;
            $consecutivo = $this->get_consec($tipo, $this->modulo);
            $sql_principal = "INSERT INTO conceptoscontables (codigo, nombre, modulo, tipo, almacen) VALUES (?, ?, ?, ?, ?)";
            $values_principal = [$consecutivo, strClean(replaceChar($nombre)), $this->modulo, $tipo, "N"];
            $this->insert($sql_principal, $values_principal);

            $sql_det = "INSERT INTO conceptoscontables_det (codigo, tipo, tipocuenta, cuenta, cc, debito, credito, estado, modulo, fechainicial, asociado_a, destino_compra) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";

            foreach ($arrCredito as $credito) {
                $values_credito = [
                    $consecutivo,
                    $tipo, 
                    "N",
                    $credito["cuenta"],
                    "01", /* centro costo */
                    "N", /* debito */
                    "S", /* credito */
                    "S", /* estado */
                    $this->modulo,
                    $credito["fecha"],
                    "", /* asociado */
                    "", /* destino compra */
                ];

                $this->insert($sql_det, $values_credito);
            } 

            foreach ($arrDebito as $debito) {
                $values_debito = [
                    $consecutivo, 
                    $tipo,
                    "N",
                    $debito["cuenta"],
                    $debito["centroCosto"],
                    "S", /* debito */
                    "N", /* credito */
                    "S", /* estado */
                    $this->modulo,
                    $debito["fecha"],
                    $debito["cuentaAsociada"],
                    $debito["destinoCompra"]
                ];
                $this->insert($sql_det, $values_debito);
            }

            return true;
        }

        /* Update data */
        public function update_programacion($codigo, $nombre, $tipo, $arrCredito, $arrDebito, $moduloRef = 0) {
            $this->modulo = $moduloRef == 0 ? $this->modulo : $moduloRef;
            $sql_principal = "UPDATE conceptoscontables SET nombre = ? WHERE codigo = '$codigo' AND modulo = $this->modulo AND tipo = '$tipo'";
            $values_principal = [strClean(replaceChar($nombre))];
            $this->update($sql_principal, $values_principal);

            $sql_delete = "DELETE FROM conceptoscontables_det WHERE codigo = '$codigo' AND tipo = '$tipo' AND modulo = $this->modulo";
            $this->delete($sql_delete);

            $sql_det = "INSERT INTO conceptoscontables_det (codigo, tipo, tipocuenta, cuenta, cc, debito, credito, estado, modulo, fechainicial, asociado_a, destino_compra) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";

            foreach ($arrCredito as $credito) {
                $values_credito = [
                    $codigo,
                    $tipo, 
                    "N",
                    $credito["cuenta"],
                    "01", /* centro costo */
                    "N", /* debito */
                    "S", /* credito */
                    "S", /* estado */
                    $this->modulo,
                    $credito["fecha"],
                    "", /* asociado */
                    "", /* destino compra */
                ];

                $this->insert($sql_det, $values_credito);
            } 

            foreach ($arrDebito as $debito) {
                $values_debito = [
                    $codigo, 
                    $tipo,
                    "N",
                    $debito["cuenta"],
                    $debito["centroCosto"],
                    "S", /* debito */
                    "N", /* credito */
                    "S", /* estado */
                    $this->modulo,
                    $debito["fecha"],
                    $debito["cuentaAsociada"],
                    $debito["destinoCompra"]
                ];
                $this->insert($sql_det, $values_debito);
            }

            return true;
        }
        /* Delate data */
       
    }
?>