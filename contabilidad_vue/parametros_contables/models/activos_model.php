<?php
    require_once '../../../Librerias/core/Helpers.php';
    require_once '../../../Librerias/core/Mysql.php';
    session_start();
    /*ini_set('display_errors', '1');
    ini_set('display_startup_errors', '1');
    error_reporting(E_ALL);*/
    class ActivosModel extends Mysql{
        private $strNombre;
        private $strTipo;
        private $strId;
        private $intModulo;
        function __construct(){
            parent::__construct();
        }
        public function selectCuentas(){
            $sql ="SELECT cuenta as codigo,nombre,naturaleza
            FROM cuentasnicsp WHERE tipo = 'Auxiliar' AND LENGTH(cuenta) >= 9 AND estado = 'S'
            ORDER BY cuenta";
            $request = $this->select_all($sql);
            return $request;
        }
        public function selectCentroCostos(){
            $sql = "SELECT id_cc as codigo, nombre FROM centrocosto WHERE estado='S'";
            $request = $this->select_all($sql);
            return $request;
        }
        public function selectActivos(){
            $sql="SELECT * FROM actipo WHERE estado='S'";
            $request = $this->select_all($sql);
            return $request;
        }
        public function selectDisposiciones(){
            $sql="SELECT id as codigo,nombre FROM acti_disposicionactivos WHERE estado='S'";
            $request = $this->select_all($sql);
            return $request;
        }
        public function insertData(string $strNombre){
            $this->strNombre = $strNombre;;
            $return = "";
            $sql = "SELECT * FROM acti_activos_cab WHERE nombre = '$this->strNombre'";
            $request = $this->select_all($sql);
            if(empty($request)){
                $sql = "INSERT INTO acti_activos_cab(nombre,estado) VALUES(?,?)";
                $arrData = array($this->strNombre,"S");
                $return = $this->insert($sql,$arrData);
                insertAuditoria("cont_auditoria","cont_funciones_id",4,"Crear activo",$return);
            }else{
                $return = "existe";
            }
            return $return;
        }
        public function updateData(string $strId,string $strNombre){
            $this->strNombre = $strNombre;
            $this->strId = $strId;
            $return = "";
            $sql = "SELECT * FROM acti_activos_cab
            WHERE nombre = '$this->strNombre' AND id != '$this->strId'";
            $request = $this->select_all($sql);
            if(empty($request)){
                $sql = "UPDATE acti_activos_cab SET nombre =?
                WHERE id = '$this->strId'";
                $arrData = array($this->strNombre);
                $return = $this->update($sql,$arrData);
                insertAuditoria("cont_auditoria","cont_funciones_id",4,"Editar activo",$strId);
            }else{
                $return = "existe";
            }
            return $return;
        }
        public function insertDataDetalle(string $strId,array $data,string $strTipo){
            $this->strId = $strId;
            $this->strTipo = $strTipo;
            $sql = "DELETE FROM acti_activos_det WHERE id_cab = '$this->strId'";
            $this->delete($sql);
            $request = "";
            foreach ($data as $d) {
                $sql = "INSERT INTO acti_activos_det(id_cab,fechainicial,cuenta_activo,cuenta_perdida,
                cuenta_recuperacion,cuenta_retiro,disposicion_activos,centro_costos,estado,tipo)
                VALUES(?,?,?,?,?,?,?,?,?,?)";
                $arrData = array(
                    $this->strId,
                    $d['fecha'],
                    $d['cuenta_activo']['codigo'],
                    $d['cuenta_perdida']['codigo'],
                    $d['cuenta_recuperacion']['codigo'],
                    $d['cuenta_retiro']['codigo'],
                    $d['disposicion']['codigo'],
                    $d['centro']['codigo'],
                    'S',
                    $this->strTipo
                );
                $request = $this->insert($sql,$arrData);
            }
            return $request;
        }
        public function selectConsecutivo(){
            $sql = "SELECT MAX(id) as codigo FROM acti_activos_cab";
            $request = $this->select($sql)['codigo'];
            return $request;
        }
        public function selectConsecutivos(){
            $sql = "SELECT id FROM acti_activos_cab";
            $request = $this->select_all($sql);
            return $request;
        }
        public function selectSearch(){
            $sql = "SELECT id as codigo, nombre,estado FROM acti_activos_cab";
            $request = $this->select_all($sql);
            if(!empty($request)){
                $total = count($request);
                for ($i=0; $i < $total; $i++) {
                    $id = $request[$i]['codigo'];
                    $sql = "SELECT tipo FROM acti_activos_det WHERE id_cab=$id GROUP BY tipo";
                    $request_det = $this->select($sql);
                    $clase ="";
                    $grupo ="";
                    $tipo ="";
                    if(!empty($request_det)){
                        $res = $request_det['tipo'];
                        $clase = substr($res,0,1);
                        $grupo = substr($res,1,2);
                        $tipo = substr($res,3,3);
                    }
                    $request[$i]['clase'] = $clase;
                    $request[$i]['grupo'] = $grupo;
                    $request[$i]['tipo'] = $tipo;
                    $request[$i]['is_status'] = $request[$i]['estado'] =="S"? 1 : 0;
                }
            }
            return $request;
        }
        public function selectInfo(string $strId){
            $this->strId = $strId;
            $sql = "SELECT id as codigo, nombre,estado FROM acti_activos_cab WHERE id = $this->strId";
            $request = $this->select($sql);
            if(!empty($request)){
                $sql = "SELECT * FROM acti_activos_det WHERE id_cab=$this->strId GROUP BY tipo ORDER BY DATE(fechainicial) DESC";
                $detalle = $this->select_all($sql);
                $clase ="";
                $grupo ="";
                $tipo ="";
                if(!empty($detalle)){
                    $res = $detalle[0]['tipo'];
                    $clase = substr($res,0,1);
                    $grupo = substr($res,1,2);
                    $tipo = substr($res,3,3);
                }
                $request['clase'] = $clase;
                $request['grupo'] = $grupo;
                $request['tipo'] = $tipo;
                $request['detalle'] = $detalle;
            }
            return $request;
        }
        public function updateStatus($id,$estado){
            insertAuditoria("cont_auditoria","cont_funciones_id",4,"Editar estado de activos",$id);
            $sql = "UPDATE acti_activos_cab SET estado = ? WHERE id = $id";
            $request = $this->update($sql,array($estado));
            return $request;
        }
    }
?>
