const URL ='contabilidad_vue/parametros_contables/controllers/almacen_controller.php';

var app = new Vue({
    el:"#myapp",
    data() {
        return {
            isLoading:false,
            isModal:false,
            txtSearch:"",
            txtResultados:0,
            txtNombre:"",
            txtFecha:new Date(new Date().getFullYear(),0,1).toISOString().split("T")[0],
            arrCentros:[],
            arrCuentas: [],
            arrCuentasCopy:[],
            arrDetalles:[],
            arrSearch:[],
            arrSearchCopy:[],
            arrConsecutivos:[],
            objCuenta:{codigo:"",nombre:""},
            selectCentro:"",
            selectTipo: 1,
            intPageVal:0,
            txtTipo:"",
            txtConsecutivo:"",
            txtRuta:"",
            txtModulo:0,
        }
    },
    mounted() {
        this.intPageVal = this.$refs.pageType.value;
        this.txtTipo = this.$refs.txtType.value;
        this.txtRuta = this.$refs.txtRoute ? this.$refs.txtRoute.value : "";
        this.txtModulo = this.$refs.txtModule ? this.$refs.txtModule.value : "";
        if(this.intPageVal == 1){
            this.getData();
        }else if(this.intPageVal == 2){
            this.getSearchData();
        }else if(this.intPageVal == 3){
            this.getEdit();
        }
    },
    methods: {
        save: async function(){
            if(this.txtNombre =="" || app.arrDetalles.length == 0){
                Swal.fire("Atención!","Todos los campos con (*) son obligatorios.","warning");
                return false;
            }
            Swal.fire({
                title:"¿Estás segur@ de guardar?",
                text:"",
                icon: 'warning',
                showCancelButton:true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText:"Sí, guardar",
                cancelButtonText:"No, cancelar"
            }).then(async function(result){
                if(result.isConfirmed){
                    const formData = new FormData();
                    formData.append("action","set");
                    formData.append("nombre",app.txtNombre);
                    formData.append("data",JSON.stringify(app.arrDetalles));
                    formData.append("consecutivo",app.txtConsecutivo);
                    formData.append("tipo",app.txtTipo);
                    formData.append("modulo",app.txtModulo);
                    app.isLoading = true;
                    const response = await fetch(URL,{method:"POST",body:formData});
                    const objData = await response.json();
                    app.isLoading = false;
                    if(objData.status){
                        Swal.fire("Guardado",objData.msg,"success");
                        if(objData.id){
                            setTimeout(function(){
                                window.location.href=app.txtRuta+'?id='+objData.id;
                            },1500);
                        }
                    }else{
                        Swal.fire("Error",objData.msg,"error");
                    }
                }
            });
        },
        getSearchData:async function(){
            const formData = new FormData();
            formData.append("action","search");
            formData.append("tipo",this.txtTipo);
            formData.append("modulo",this.txtModulo);
            const response = await fetch(URL,{method:"POST",body:formData});
            const objData = await response.json();
            this.arrSearch = objData;
            this.arrSearchCopy = objData;
            this.txtResultados = this.arrSearchCopy.length;
        },
        getData: async function(){
            const formData = new FormData();
            formData.append("action","get");
            const response = await fetch(URL,{method:"POST",body:formData});
            const objData = await response.json();
            this.arrCentros = objData.centros;
            this.arrCuentas = objData.cuentas;
            this.arrCuentasCopy = objData.cuentas;
            this.txtResultados = this.arrCuentasCopy.length;
            this.selectCentro = this.arrCentros[0].codigo;
        },
        search:function(type=""){
            let search = "";
            if(type == "modal")search = this.txtSearch.toLowerCase();
            if(type == "search")search = this.txtSearch.toLowerCase();
            if(type == "cod_cuenta") search = this.objCuenta.codigo;

            if(type=="modal"){
                this.arrCuentasCopy = [...this.arrCuentas.filter(e=>e.codigo.toLowerCase().includes(search) || e.nombre.toLowerCase().includes(search))];
                this.txtResultados = this.arrCuentasCopy.length;
            }else if(type == "cod_cuenta"){
                this.objCuenta = {};
                this.objCuenta = JSON.parse(JSON.stringify(this.arrCuentasCopy.filter(e=>e.codigo==search)[0]));
                this.selectTipo = this.objCuenta.naturaleza == "DEBITO" ? 1 : 2;
            }else if(type == "search"){
                this.arrSearchCopy = [...this.arrSearch.filter(e=>e.codigo.toLowerCase().includes(search) || e.nombre.toLowerCase().includes(search))];
                this.txtResultados = this.arrSearchCopy.length;
            }
        },
        getEdit:async function(){
            const codigo = new URLSearchParams(window.location.search).get('id');
            let formData = new FormData();
            formData.append("action","edit");
            formData.append("codigo",codigo);
            formData.append("tipo",this.txtTipo);
            formData.append("modulo",this.txtModulo);
            this.isLoading = true;
            const response = await fetch(URL,{method:"POST",body:formData});
            const objData = await response.json();
            if(objData.status){
                this.arrCentros = objData.centros;
                this.arrCuentas = objData.cuentas;
                this.arrCuentasCopy = objData.cuentas;
                this.txtResultados = this.arrCuentasCopy.length;
                this.selectCentro = this.arrCentros[0].codigo;
                this.txtConsecutivo = objData.data.codigo;
                this.txtNombre = objData.data.nombre;
                this.arrConsecutivos = objData.consecutivos;
                const detalle = objData.data.detalle;
                if(detalle.length > 0){
                    detalle.forEach(det => {
                        let cuentas = app.arrCuentas.filter(e=>e.codigo==det.cuenta);
                        let cuenta ={codigo:"",nombre:""};
                        if(cuentas.length > 0){
                            cuenta = cuentas[0];
                        }else if(det.cuenta != ""){
                            cuenta.codigo = det.cuenta;
                        }
                        this.arrDetalles.push({
                            fecha:det.fechainicial,
                            cuenta: cuenta,
                            centro:app.arrCentros.filter(e=>e.codigo == det.cc)[0],
                            debito: det.debito,
                            credito: det.credito
                        });
                    });
                }
            }else{
                const id = objData.consecutivo;
                window.location.href=this.txtRuta+'?id='+id;
            }
            this.isLoading = false;
        },
        add:function(){
            if(app.objCuenta.codigo =="" || app.selectCentro == 0){
                Swal.fire("Atención!","Debe seleccionar la cuenta y centro de costos","warning");
                return false;
            }
            if(app.arrDetalles.length>0){
                let flag = false;
                for (let i = 0; i < app.arrDetalles.length; i++) {
                    const element = app.arrDetalles[i];
                    if(element.centro.codigo == app.selectCentro && element.cuenta.codigo == app.objCuenta.codigo){
                        flag = true;
                        break;
                    }
                }
                if(!flag){
                    this.arrDetalles.push({
                        fecha:app.txtFecha,
                        cuenta:JSON.parse(JSON.stringify(app.objCuenta)),
                        centro:app.arrCentros.filter(function(e){return app.selectCentro == e.codigo})[0],
                        debito: app.selectTipo == 1 ? "S" : "N",
                        credito: app.selectTipo == 2 ? "S" : "N"
                    });
                }else{
                    Swal.fire("Atención!","Este detalle ya fue agregado","warning");
                    return false;
                }
            }else{
                this.arrDetalles.push({
                    fecha:app.txtFecha,
                    cuenta:JSON.parse(JSON.stringify(app.objCuenta)),
                    centro:app.arrCentros.filter(function(e){return app.selectCentro == e.codigo})[0],
                    debito: app.selectTipo == 1 ? "S" : "N",
                    credito: app.selectTipo == 2 ? "S" : "N"
                });
            }
        },
        del:function(index){
            this.arrDetalles.splice(index,1);
        },
        selectItem:function({...item}){
            this.objCuenta = item;
            this.selectTipo = item.naturaleza == "DEBITO" ? 1 : 2;
            this.isModal = false;
        },
        nextItem:function(type){
            let vueContext = this;
            let id = this.txtConsecutivo
            let index = this.arrConsecutivos.findIndex(function(e){return e.id == id});
            if(type=="next" && vueContext.arrConsecutivos[++index]){
                id = this.arrConsecutivos[index++].id;
            }else if(type=="prev" && vueContext.arrConsecutivos[--index]){
                id = this.arrConsecutivos[index--].id;
            }
            window.location.href=this.txtRuta+'?id='+id;
        },

    },
})
