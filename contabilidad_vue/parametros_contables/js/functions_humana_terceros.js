const URL ='contabilidad_vue/parametros_contables/controllers/humana_terceros_controller.php';

var app = new Vue({
    el:"#myapp",
    data() {
        return {
            isLoading:false,
            isModal:false,
            isModalTercero:false,
            txtSearch:"",
            txtSearchTerceros:"",
            txtResultadosTerceros:0,
            txtResultados:0,
            txtNombre:"",
            txtFecha:new Date(new Date().getFullYear(),0,1).toISOString().split("T")[0],
            arrTerceros:[],
            arrTercerosCopy:[],
            arrCuentas: [],
            arrCuentasCopy:[],
            arrDetalles:[],
            arrSearch:[],
            arrSearchCopy:[],
            arrConsecutivos:[],
            objCuenta:{codigo:"",nombre:""},
            objTercero:{codigo:"",nombre:""},
            selectCentro:"",
            selectTipo: 1,
            intPageVal:0,
            txtTipo:"",
            txtConsecutivo:"",
            txtRuta:"",
            txtModulo:0,
        }
    },
    mounted() {
        this.intPageVal = this.$refs.pageType.value;
        this.txtRuta = this.$refs.txtRoute ? this.$refs.txtRoute.value : "";
        this.txtModulo = this.$refs.txtModule ? this.$refs.txtModule.value : "";
        if(this.intPageVal == 1){
            this.getData();
        }else if(this.intPageVal == 2){
            this.getSearchData();
        }else if(this.intPageVal == 3){
            this.getEdit();
        }
    },
    methods: {
        save: async function(){
            if(this.txtNombre =="" || app.arrDetalles.length == 0 || app.objTercero.codigo == ""){
                Swal.fire("Atención!","Todos los campos con (*) son obligatorios.","warning");
                return false;
            }
            Swal.fire({
                title:"¿Estás segur@ de guardar?",
                text:"",
                icon: 'warning',
                showCancelButton:true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText:"Sí, guardar",
                cancelButtonText:"No, cancelar"
            }).then(async function(result){
                if(result.isConfirmed){
                    const formData = new FormData();
                    formData.append("action","set");
                    formData.append("nombre",app.txtNombre);
                    formData.append("tercero",app.objTercero.codigo);
                    formData.append("data",JSON.stringify(app.arrDetalles));
                    formData.append("consecutivo",app.txtConsecutivo);
                    formData.append("modulo",app.txtModulo);
                    app.isLoading = true;
                    const response = await fetch(URL,{method:"POST",body:formData});
                    const objData = await response.json();
                    app.isLoading = false;
                    if(objData.status){
                        Swal.fire("Guardado",objData.msg,"success");
                        if(objData.id){
                            setTimeout(function(){
                                window.location.href=app.txtRuta+'?id='+objData.id;
                            },1500);
                        }
                    }else{
                        Swal.fire("Error",objData.msg,"error");
                    }
                }
            });
        },
        getSearchData:async function(){
            const formData = new FormData();
            formData.append("action","search");
            const response = await fetch(URL,{method:"POST",body:formData});
            const objData = await response.json();
            this.arrSearch = objData;
            this.arrSearchCopy = objData;
            this.txtResultados = this.arrSearchCopy.length;
        },
        getData: async function(){
            const formData = new FormData();
            formData.append("action","get");
            const response = await fetch(URL,{method:"POST",body:formData});
            const objData = await response.json();
            this.arrTerceros = objData.terceros;
            this.arrTercerosCopy = objData.terceros;
            this.arrCuentas = objData.cuentas;
            this.arrCuentasCopy = objData.cuentas;
            this.txtResultados = this.arrCuentasCopy.length;
            this.txtResultadosTerceros = this.arrTercerosCopy.length;
        },
        search:function(type=""){
            let search = "";
            if(type == "modal")search = this.txtSearch.toLowerCase();
            if(type == "modal_tercero")search = this.txtSearchTerceros.toLowerCase();
            if(type == "search")search = this.txtSearch.toLowerCase();
            if(type == "cod_cuenta") search = this.objCuenta.codigo;
            if(type == "cod_tercero") search = this.objTercero.codigo;

            if(type=="modal"){
                this.arrCuentasCopy = [...this.arrCuentas.filter(e=>e.codigo.toLowerCase().includes(search) || e.nombre.toLowerCase().includes(search))];
                this.txtResultados = this.arrCuentasCopy.length;
            }else if(type == "modal_tercero"){
                this.arrTercerosCopy = [...this.arrTerceros.filter(e=>e.codigo.toLowerCase().includes(search) || e.nombre.toLowerCase().includes(search))];
                this.txtResultadosTerceros = this.arrTercerosCopy.length;
            }else if(type == "cod_cuenta"){
                this.objCuenta = {};
                this.objCuenta = JSON.parse(JSON.stringify(this.arrCuentasCopy.filter(e=>e.codigo==search)[0]));
                this.selectTipo = this.objCuenta.naturaleza == "DEBITO" ? 1 : 2;
            }else if(type == "cod_tercero"){
                this.objTercero = {};
                this.objTercero = JSON.parse(JSON.stringify(this.arrTercerosCopy.filter(e=>e.codigo==search)[0]));
            }else if(type == "search"){
                this.arrSearchCopy = [...this.arrSearch.filter(e=>e.codigo.toLowerCase().includes(search) || e.nombre.toLowerCase().includes(search))];
                this.txtResultados = this.arrSearchCopy.length;
            }
        },
        getEdit:async function(){
            const codigo = new URLSearchParams(window.location.search).get('id');
            let formData = new FormData();
            formData.append("action","edit");
            formData.append("codigo",codigo);
            this.isLoading = true;
            const response = await fetch(URL,{method:"POST",body:formData});
            const objData = await response.json();
            if(objData.status){

                this.arrTerceros = objData.terceros;
                this.arrTercerosCopy = objData.terceros;
                this.arrCuentas = objData.cuentas;
                this.arrCuentasCopy = objData.cuentas;
                this.txtResultados = this.arrCuentasCopy.length;
                this.txtResultadosTerceros = this.arrTercerosCopy.length;
                this.txtConsecutivo = objData.data.codigo;
                this.txtNombre = objData.data.nombre;
                this.arrConsecutivos = objData.consecutivos;
                this.objTercero = app.arrTercerosCopy.filter(e=>e.codigo==objData.data.beneficiario)[0];
                const detalle = objData.data.detalle;
                if(detalle.length > 0){
                    detalle.forEach(det => {
                        let cuentas = app.arrCuentas.filter(e=>e.codigo==det.cuenta);
                        let cuenta ={codigo:"",nombre:""};
                        if(cuentas.length > 0){
                            cuenta = cuentas[0];
                        }else if(det.cuenta != ""){
                            cuenta.codigo = det.cuenta;
                        }
                        this.arrDetalles.push({
                            fecha:det.fechainicial,
                            cuenta: cuenta,
                            debito: det.debito,
                            credito: det.credito
                        });
                    });
                }
            }else{
                const id = objData.consecutivo;
                window.location.href=this.txtRuta+'?id='+id;
            }
            this.isLoading = false;
        },
        add:function(){
            if(app.objCuenta.codigo ==""){
                Swal.fire("Atención!","Debe seleccionar la cuenta contable","warning");
                return false;
            }
            if(app.arrDetalles.length>0){
                let flag = false;
                for (let i = 0; i < app.arrDetalles.length; i++) {
                    const element = app.arrDetalles[i];
                    if(element.cuenta.codigo == app.objCuenta.codigo){
                        flag = true;
                        break;
                    }
                }
                if(!flag){
                    this.arrDetalles.push({
                        fecha:app.txtFecha,
                        cuenta:JSON.parse(JSON.stringify(app.objCuenta)),
                        debito: app.selectTipo == 1 ? "S" : "N",
                        credito: app.selectTipo == 2 ? "S" : "N"
                    });
                }else{
                    Swal.fire("Atención!","Este detalle ya fue agregado","warning");
                    return false;
                }
            }else{
                this.arrDetalles.push({
                    fecha:app.txtFecha,
                    cuenta:JSON.parse(JSON.stringify(app.objCuenta)),
                    debito: app.selectTipo == 1 ? "S" : "N",
                    credito: app.selectTipo == 2 ? "S" : "N"
                });
            }
        },
        del:function(index){
            this.arrDetalles.splice(index,1);
        },
        changeStatus:function(item){
            const vueThis = this;
            Swal.fire({
                title:"¿Estás segur@ de cambiar el estado?",
                text:"",
                icon: 'warning',
                showCancelButton:true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText:"Sí",
                cancelButtonText:"No, cancelar"
            }).then(async function(result){
                if(result.isConfirmed){
                    const formData = new FormData();
                    formData.append("action","change");
                    formData.append("id",item.codigo);
                    formData.append("estado",item.estado =='S' ? 'N' : 'S');
                    const response = await fetch(URL,{method:"POST",body:formData});
                    const objData = await response.json();
                    if(objData.status){
                        let index = vueThis.arrSearch.findIndex(e => e.codigo == item.codigo);
                        vueThis.arrSearch[index].estado = item.estado =='S' ? 'N' : 'S';
                        vueThis.arrSearch[index].is_status = vueThis.arrSearch[index].estado =='S' ? 1 : 0;
                        vueThis.search("search");
                        Swal.fire("Estado actualizado","","success");
                    }else{
                        Swal.fire("Error",objData.msg,"error");
                    }
                }else{
                    vueThis.search("search");
                }
            });

        },
        selectItem:function({...item},type=""){
            if(type == "tercero"){
                this.objTercero = item;
                this.isModalTercero = false;
            }else{
                this.objCuenta = item;
                this.selectTipo = item.naturaleza == "DEBITO" ? 1 : 2;
                this.isModal = false;
            }
        },
        nextItem:function(type){
            let vueContext = this;
            let id = this.txtConsecutivo
            let index = this.arrConsecutivos.findIndex(function(e){return e.id == id});
            if(type=="next" && vueContext.arrConsecutivos[++index]){
                id = this.arrConsecutivos[index++].id;
            }else if(type=="prev" && vueContext.arrConsecutivos[--index]){
                id = this.arrConsecutivos[index--].id;
            }
            window.location.href=this.txtRuta+'?id='+id;
        },

    },
})
