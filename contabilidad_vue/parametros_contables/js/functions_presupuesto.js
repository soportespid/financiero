const URL = "contabilidad_vue/parametros_contables/controllers/presupuesto_controller.php";

var app = new Vue({
    el:"#myapp",
    data() {
        return {
            /* general */
            isLoading: false,
            txtSearch: '',
            id: 0,
            tabs: '1',
            tipo: '',
            modulo: '',

            /* cabacera */
            consecutivo: '',
            nombre: '',

            /* modal cuentas credito */
            modalCredito: false,
            arrCuentasCredito: [], arrCuentasCreditoCopy: [],
            txtCredito: '',

            /* credito */
            fechaCredito: '',       
            cuentaCredito: '',
            nombreCredito: '',

            /* array credito */
            arrCredito: [],

            /* modal cuentas debito */
            modalDebito: false,
            arrCuentasDebito: [], arrCuentasDebitoCopy: [],
            txtDebito: '',

            /* debito */
            fechaDebito: '',
            destinosCompras: [],
            codDestino: '',
            cuentaDebito: '',
            nombreDebito: '',
            centroCostos: [],
            cc: '',
            cuentaAsociada: [],

            /* array debito */
            arrDebito: [],

            /* buscar */
            arrData: [], arrDataCopy: [],
            txtSearch: '',

            /* editar */
            url: "",
            arrConsecutivos: [],
        }
    },
    mounted() {

        
        this.intPageVal = this.$refs.pageType.value;
        this.tipo = this.$refs.tipo.value;
        this.modulo = this.$refs.modulo?.value ?? 0;

        if (this.intPageVal == 1) {
            this.get();
        } else if (this.intPageVal == 2) {
            this.getSearch();
        } else if (this.intPageVal == 3) {
            this.url = this.$refs.url.value;
            this.getEdit();
        }
    },
    methods: {
        /* metodos para traer información */
        async get() {
            const formData = new FormData();
            formData.append("action","get");
            formData.append("tipo",this.tipo);
            formData.append("modulo",this.modulo);
            const response = await fetch(URL,{method:"POST",body:formData});
            const objData = await response.json();     
            this.consecutivo = objData.consecutivo; 
            this.arrCuentasCredito = this.arrCuentasCreditoCopy = objData.cuentasCredito;
            this.destinosCompras = objData.destinosCompras;
            this.centroCostos = objData.centrosCostos;
        },

        async getSearch() {
            const formData = new FormData();
            formData.append("action","getSearch");
            formData.append("tipo",this.tipo);
            formData.append("modulo", this.modulo);
            const response = await fetch(URL,{method:"POST",body:formData});
            const objData = await response.json();    
            this.arrData = this.arrDataCopy = objData.data; 
        },

        async getEdit() {
            this.id = new URLSearchParams(window.location.search).get('cod');
            const formData = new FormData();
            formData.append("action","getEdit");
            formData.append("cod",this.id);
            formData.append("tipo",this.tipo);
            formData.append("modulo", this.modulo);
            const response = await fetch(URL,{method:"POST",body:formData});
            const objData = await response.json();    
            this.consecutivo = this.id;
            this.nombre = objData.nombre;
            this.arrCredito = objData.arrCredito;
            this.arrDebito = objData.arrDebito;
            this.arrConsecutivos = objData.consecutivos;
            this.arrCuentasCredito = this.arrCuentasCreditoCopy = objData.cuentasCredito;
            this.destinosCompras = objData.destinosCompras;
            this.centroCostos = objData.centrosCostos;
        },

        async get_cuentas_debito() {
            if (this.codDestino == "") {
                return false;
            }

            const formData = new FormData();
            formData.append("action","get_debito");
            formData.append("codDestino",this.codDestino);
            const response = await fetch(URL,{method:"POST",body:formData});
            const objData = await response.json();    
            this.arrCuentasDebito = this.arrCuentasDebitoCopy = objData.cuentasDebito; 
        },

        /* metodos para procesar información */
        filter(type="") {
            let search;
            switch (type) {
                case "modalCredito":
                    search = this.txtCredito.toLowerCase();
                    this.arrCuentasCredito = [...this.arrCuentasCreditoCopy.filter(e=>e.cuenta.toLowerCase().includes(search) || e.nombre.toLowerCase().includes(search))];    
                    break;

                case "inputCredito":
                    search = this.cuentaCredito;
                    const data = [...this.arrCuentasCredito.filter(e=>e.cuenta == search)];
                    if (data.length > 0) {
                        if (data[0].tipo == 'Auxiliar') {
                            this.nombreCredito = data[0].nombre;
                        } else {
                            this.cuentaCredito = this.nombreCredito = "";
                            Swal.fire("Atención!", "Cuenta contable es Mayor","warning");
                        }
                    } else {
                        this.cuentaCredito = this.nombreCredito = "";
                        Swal.fire("Atención!", "Cuenta contable no encontrada","warning");
                    }
                    break;
            
                case "modalDebito":
                    search = this.txtDebito.toLowerCase();
                    this.arrCuentasDebito = [...this.arrCuentasDebitoCopy.filter(e=>e.cuenta.toLowerCase().includes(search) || e.nombre.toLowerCase().includes(search))];
                    break;

                case "inputDebito":
                    search = this.cuentaDebito;
                    const data2 = [...this.arrCuentasDebito.filter(e=>e.cuenta == search)];
                    if (data2.length > 0) {
                        if (data2[0].tipo == 'Auxiliar') {
                            this.nombreDebito = data2[0].nombre;
                        } else {
                            this.cuentaDebito = this.nombreDebito = "";
                            Swal.fire("Atención!", "Cuenta contable es Mayor","warning");
                        }
                    } else {
                        this.cuentaDebito = this.nombreDebito = "";
                        Swal.fire("Atención!", "Cuenta contable no encontrada","warning");
                    }
                    break;

                case "inputSearch":
                    search = this.txtSearch.toLowerCase();
                    this.arrData = [...this.arrDataCopy.filter(e=>e.codigo.toLowerCase().includes(search) || e.nombre.toLowerCase().includes(search))];
                    break;
                default:
                    break;
            }
        },

        selectItem(type="", item) {
            if (type=="modalCredito") {
                if (item.tipo == 'Auxiliar') {
                    this.cuentaCredito = item.cuenta;
                    this.nombreCredito = item.nombre;
                    this.modalCredito = false;
                    this.txtCredito = "";
                    this.arrCuentasCredito = this.arrCuentasCreditoCopy;
                } else {
                    Swal.fire("Atención!", "Cuenta contable es Mayor","warning");
                }
            } else if (type="modalDebito") {
                if (item.tipo == 'Auxiliar') {
                    this.cuentaDebito = item.cuenta;
                    this.nombreDebito = item.nombre;
                    this.modalDebito = false;
                    this.txtDebito = "";
                    this.arrCuentasDebito = this.arrCuentasDebitoCopy;  
                } else {
                    Swal.fire("Atención!", "Cuenta contable es Mayor","warning");
                }
            }
        },

        addArrCredito() {
            if (this.fechaCredito == "") {
                Swal.fire("Atención!", "El campo de fecha es obligatorio","warning");
                return false;
            }

            if (this.cuentaCredito == "" || this.nombreCredito == "") {
                Swal.fire("Atención!", "El campo de cuenta credito es obligatorio","warning");
                return false;
            }

            this.arrCredito.push({
                "fecha": this.fechaCredito,
                "cuenta": this.cuentaCredito,
                "nombre": this.nombreCredito,
            });

            this.cuentaCredito = this.nombreCredito = "";
        },

        delCredito(index, cuenta) {
            // Verificar si la cuenta existe en this.arrDebito
            const cuentaExiste = this.arrDebito.some(debito => debito.cuentaAsociada === cuenta);

            if (cuentaExiste) {
                Swal.fire("Atención!", "La cuenta existe en el listado de DEBITOS y no se puede eliminar", "warning");
                return false;
            }

            // Eliminar el elemento del array arrCredito
            this.arrCredito.splice(index, 1);
        },

        addArrDebito() {
            if (this.fechaDebito == "") {
                Swal.fire("Atención!", "El campo de fecha es obligatorio","warning");
                return false;
            }

            if (this.codDestino == "") {
                Swal.fire("Atención!", "El campo de Destino compra es obligatorio","warning");
                return false;
            }

            if (this.cuentaDebito == "" || this.nombreDebito == "") {
                Swal.fire("Atención!", "El campo de cuenta debito es obligatorio","warning");
                return false;
            }

            if (this.cc == "") {
                Swal.fire("Atención!", "El campo de Centro costo es obligatorio","warning");
                return false;
            }

            if (this.cuentaAsociada == "") {
                Swal.fire("Atención!", "El campo de Cuenta asociada es obligatorio","warning");
                return false;
            }

            this.arrDebito.push({
                "fecha": this.fechaDebito,
                "cuenta": this.cuentaDebito,
                "nombre": this.nombreDebito,
                "centroCosto": this.cc,
                "destinoCompra": this.codDestino,
                "cuentaAsociada": this.cuentaAsociada.cuenta,
                "nombreAsociada": this.cuentaAsociada.nombre
            });

            this.cuentaDebito = this.nombreDebito = "";
            this.cuentaAsociada = [];
        },

        delDebito(index) {
            this.arrDebito.splice(index, 1);
        },
        
        nextItem:function(type){
            let codigo = this.id;
            let index = this.arrConsecutivos.findIndex(function(e){return e.codigo == codigo});
            if(type=="next" && this.arrConsecutivos[++index]){
                codigo = this.arrConsecutivos[index++].codigo;
            }else if(type=="prev" && this.arrConsecutivos[--index]){
                codigo = this.arrConsecutivos[index--].codigo;
            }
            window.location.href=this.url+'?cod='+codigo;
        },

        /* Metodos para guardar o actualizar información */
        async save() {
            if (this.nombre == "") {
                Swal.fire("Atención!", "El campo de Nombre es obligatorio","warning");
                return false;
            }

            if (this.arrCredito.length == 0 || this.arrDebito.length == 0) {
                Swal.fire("Atención!", "El listado de Creditos y Debitos es obligatorio","warning");
                return false;
            }

            const formData = new FormData();
            formData.append("action","save");
            formData.append("tipo",this.tipo);
            formData.append("modulo", this.modulo);
            formData.append("nombre",this.nombre);
            formData.append("arrCredito", JSON.stringify(this.arrCredito));
            formData.append("arrDebito", JSON.stringify(this.arrDebito));

            Swal.fire({
                title:"¿Estás segur@ de guardar?",
                text:"",
                icon: 'warning',
                showCancelButton:true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText:"Sí, guardar",
                cancelButtonText:"No, cancelar"
            }).then(async function(result) {
                if(result.isConfirmed) {
                    const response = await fetch(URL,{method:"POST",body:formData});
                    const objData = await response.json();

                    if (objData.status) {
                        Swal.fire("Guardado",objData.msg,"success");
                        setTimeout(function(){
                            window.location.reload();
                        },1500);
                    }
                    else {
                        Swal.fire("Error!",objData.msg,"warning");
                    }
                }
            });
        },
       
        async update() {
            if (this.nombre == "") {
                Swal.fire("Atención!", "El campo de Nombre es obligatorio","warning");
                return false;
            }

            if (this.arrCredito.length == 0 || this.arrDebito.length == 0) {
                Swal.fire("Atención!", "El listado de Creditos y Debitos es obligatorio","warning");
                return false;
            }

            const formData = new FormData();
            formData.append("action","update");
            formData.append("codigo",this.id);
            formData.append("tipo",this.tipo);
            formData.append("modulo", this.modulo);
            formData.append("nombre",this.nombre);
            formData.append("arrCredito", JSON.stringify(this.arrCredito));
            formData.append("arrDebito", JSON.stringify(this.arrDebito));

            Swal.fire({
                title:"¿Estás segur@ de actualizar?",
                text:"",
                icon: 'warning',
                showCancelButton:true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText:"Sí, guardar",
                cancelButtonText:"No, cancelar"
            }).then(async function(result) {
                if(result.isConfirmed) {
                    const response = await fetch(URL,{method:"POST",body:formData});
                    const objData = await response.json();

                    if (objData.status) {
                        Swal.fire("Actualizado",objData.msg,"success");
                        setTimeout(function(){
                            window.location.reload();
                        },1500);
                    }
                    else {
                        Swal.fire("Error!",objData.msg,"warning");
                    }
                }
            });
        },

        /* Formatos para mostrar información */
        formatFecha: function(fecha) {
            // Separar la fecha en componentes
            const parts = fecha.split('-');
            if (parts.length !== 3) {
                throw new Error('Formato de fecha inválido. Use Y-m-d.');
            }
            const year = parts[0];
            const month = parts[1];
            const day = parts[2];
        
            // Retornar la fecha en el formato deseado
            return `${day}/${month}/${year}`;
        },

        /* enviar informacion excel o pdf */
    
    },
    computed:{

    }
})
