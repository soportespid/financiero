const URL ='contabilidad_vue/parametros_contables/controllers/activos_deterioro_controller.php';

var app = new Vue({
    el:"#myapp",
    data() {
        return {
            isLoading:false,
            isModal:false,
            txtSearch:"",
            txtResultados:0,
            txtNombre:"",
            txtFecha:new Date(new Date().getFullYear(),0,1).toISOString().split("T")[0],
            arrCentros:[],
            arrCuentas: [],
            arrCuentasCopy:[],
            arrActivos:[],
            arrClases:[],
            arrGrupos:[],
            arrTipos:[],
            arrDetalles:[],
            arrSearch:[],
            arrSearchCopy:[],
            arrConsecutivos:[],
            objCuentaDebito:{codigo:"",nombre:""},
            objCuentaCredito:{codigo:"",nombre:""},
            selectCentro:"",
            selectClase:"",
            selectTipoActivo:"",
            selectGrupo:"",
            intPageVal:0,
            txtTipo:"",
            txtConsecutivo:"",
            txtRuta:"",
            txtModulo:0,
            tipoCuenta:1,
        }
    },
    mounted() {
        this.intPageVal = this.$refs.pageType.value;
        this.txtRuta = this.$refs.txtRoute ? this.$refs.txtRoute.value : "";
        if(this.intPageVal == 1){
            this.getData();
        }else if(this.intPageVal == 2){
            this.getSearchData();
        }else if(this.intPageVal == 3){
            this.getEdit();
        }
    },
    methods: {
        save: async function(){
            if(app.arrDetalles.length == 0){
                Swal.fire("Atención!","Todos los campos con (*) son obligatorios.","warning");
                return false;
            }
            Swal.fire({
                title:"¿Estás segur@ de guardar?",
                text:"",
                icon: 'warning',
                showCancelButton:true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText:"Sí, guardar",
                cancelButtonText:"No, cancelar"
            }).then(async function(result){
                if(result.isConfirmed){
                    const formData = new FormData();
                    formData.append("action","set");
                    formData.append("data",JSON.stringify(app.arrDetalles));
                    formData.append("consecutivo",app.txtConsecutivo);
                    formData.append("nombre",app.arrTipos.filter(e=>e.codigo==app.selectTipoActivo)[0].nombre);
                    formData.append("clase",app.selectClase);
                    formData.append("grupo",app.selectGrupo);
                    formData.append("tipo",app.selectTipoActivo);
                    app.isLoading = true;
                    const response = await fetch(URL,{method:"POST",body:formData});
                    const objData = await response.json();
                    app.isLoading = false;
                    if(objData.status){
                        Swal.fire("Guardado",objData.msg,"success");
                        if(objData.id){
                            setTimeout(function(){
                                window.location.href=app.txtRuta+'?id='+objData.id;
                            },1500);
                        }
                    }else{
                        Swal.fire("Error",objData.msg,"error");
                    }
                }
            });
        },
        getSearchData:async function(){
            const formData = new FormData();
            formData.append("action","search");
            const response = await fetch(URL,{method:"POST",body:formData});
            const objData = await response.json();
            this.arrSearch = objData;
            this.arrSearchCopy = objData;
            this.txtResultados = this.arrSearchCopy.length;
        },
        getData: async function(){
            const formData = new FormData();
            formData.append("action","get");
            const response = await fetch(URL,{method:"POST",body:formData});
            const objData = await response.json();
            this.arrCentros = objData.centros;
            this.arrCuentas = objData.cuentas;
            this.arrCuentasCopy = objData.cuentas;
            this.arrActivos = objData.activos;
            this.arrClases = objData.activos.filter(e=>e.niveluno ==0);
            this.selectClase = this.arrClases[0].codigo;
            this.arrGrupos = this.arrActivos.filter(e=>e.niveluno == this.selectClase);
            this.selectGrupo = this.arrGrupos[0].codigo;
            this.arrTipos = this.arrActivos.filter(e=>e.niveluno == this.selectGrupo && e.niveldos == this.selectClase);
            this.selectTipoActivo = this.arrTipos[0].codigo;
            this.txtResultados = this.arrCuentasCopy.length;
            this.selectCentro = this.arrCentros[0].codigo;
        },
        search:function(type=""){
            let search = "";
            if(type == "modal")search = this.txtSearch.toLowerCase();
            if(type == "search")search = this.txtSearch.toLowerCase();
            if(type == "cod_debito") search = this.objCuentaDebito.codigo;
            if(type == "cod_credito") search = this.objCuentaCredito.codigo;

            if(type=="modal"){
                this.arrCuentasCopy = [...this.arrCuentas.filter(e=>e.codigo.toLowerCase().includes(search) || e.nombre.toLowerCase().includes(search))];
                this.txtResultados = this.arrCuentasCopy.length;
            }else if(type == "cod_debito"){
                this.objCuentaDebito = {};
                this.objCuentaDebito = JSON.parse(JSON.stringify(this.arrCuentasCopy.filter(e=>e.codigo==search)[0]));
            }else if(type == "cod_credito"){
                this.objCuentaCredito = {};
                this.objCuentaCredito = JSON.parse(JSON.stringify(this.arrCuentasCopy.filter(e=>e.codigo==search)[0]));
            }else if(type == "search"){
                this.arrSearchCopy = [...this.arrSearch.filter(e=>e.codigo.toLowerCase().includes(search) || e.nombre.toLowerCase().includes(search))];
                this.txtResultados = this.arrSearchCopy.length;
            }
        },
        getEdit:async function(){
            const codigo = new URLSearchParams(window.location.search).get('id');
            let formData = new FormData();
            formData.append("action","edit");
            formData.append("codigo",codigo);
            this.isLoading = true;
            const response = await fetch(URL,{method:"POST",body:formData});
            const objData = await response.json();
            if(objData.status){
                this.arrCentros = objData.centros;
                this.arrCuentas = objData.cuentas;
                this.arrCuentasCopy = objData.cuentas;
                this.arrActivos = objData.activos;
                this.arrClases = objData.activos.filter(e=>e.niveluno ==0);
                this.selectClase = objData.data.clase;
                this.arrGrupos = this.arrActivos.filter(e=>e.niveluno == this.selectClase);
                this.selectGrupo = objData.data.grupo
                this.arrTipos = this.arrActivos.filter(e=>e.niveluno == this.selectGrupo && e.niveldos == this.selectClase);
                this.selectTipoActivo = objData.data.tipo;
                this.txtResultados = this.arrCuentasCopy.length;
                this.selectCentro = this.arrCentros[0].codigo;
                this.txtConsecutivo = objData.data.codigo;
                this.arrConsecutivos = objData.consecutivos;
                const detalle = objData.data.detalle;
                if(detalle.length > 0){
                    detalle.forEach(det => {
                        let cuentaDebito = app.arrCuentas.filter(function(e){return det.cuenta_debito == e.codigo});
                        let cuentaCredito = app.arrCuentas.filter(function(e){return det.cuenta_credito == e.codigo});
                        this.arrDetalles.push({
                            fecha:det.fechainicial,
                            cuenta_debito:cuentaDebito.length > 0 ? cuentaDebito[0] : {codigo:"",nombre:""},
                            cuenta_credito:cuentaCredito.length > 0 ? cuentaCredito[0] : {codigo:"",nombre:""},
                            centro:app.arrCentros.filter(function(e){return det.centro_costos == e.codigo})[0],
                        });
                    });
                }
            }else{
                const id = objData.consecutivo;
                window.location.href=this.txtRuta+'?id='+id;
            }
            this.isLoading = false;
        },
        add:function(){
            if(app.objCuentaDebito.codigo =="" || app.objCuentaCredito.codigo ==""){
                Swal.fire("Atención!","Debe seleccionar las cuentas contables","warning");
                return false;
            }
            if(app.arrDetalles.length>0){
                let flag = false;
                for (let i = 0; i < app.arrDetalles.length; i++) {
                    const element = app.arrDetalles[i];
                    if(element.centro.codigo == app.selectCentro
                        && element.cuenta_debito.codigo == app.objCuentaDebito.codigo
                        && element.cuenta_credito.codigo == app.objCuentaCredito.codigo){
                        flag = true;
                        break;
                    }
                }
                if(!flag){
                    this.arrDetalles.push({
                        fecha:app.txtFecha,
                        cuenta_debito:JSON.parse(JSON.stringify(app.objCuentaDebito)),
                        cuenta_credito:JSON.parse(JSON.stringify(app.objCuentaCredito)),
                        centro:app.arrCentros.filter(function(e){return app.selectCentro == e.codigo})[0],
                    });
                }else{
                    Swal.fire("Atención!","Este detalle ya fue agregado","warning");
                    return false;
                }
            }else{
                this.arrDetalles.push({
                    fecha:app.txtFecha,
                    cuenta_debito:JSON.parse(JSON.stringify(app.objCuentaDebito)),
                    cuenta_credito:JSON.parse(JSON.stringify(app.objCuentaCredito)),
                    centro:app.arrCentros.filter(function(e){return app.selectCentro == e.codigo})[0],
                });
            }
        },
        del:function(index){
            this.arrDetalles.splice(index,1);
        },
        selectItem:function({...item}){
            if(this.tipoCuenta == 1){
                this.objCuentaDebito = item;
            }else if(this.tipoCuenta == 2){
                this.objCuentaCredito = item;
            }
            this.isModal = false;
        },
        nextItem:function(type){
            let vueContext = this;
            let id = this.txtConsecutivo
            let index = this.arrConsecutivos.findIndex(function(e){return e.id == id});
            if(type=="next" && vueContext.arrConsecutivos[++index]){
                id = this.arrConsecutivos[index++].id;
            }else if(type=="prev" && vueContext.arrConsecutivos[--index]){
                id = this.arrConsecutivos[index--].id;
            }
            window.location.href=this.txtRuta+'?id='+id;
        },
        changeActivo:function(type=1){
            if(type == 1){
                this.arrGrupos = this.arrActivos.filter(e=>e.niveluno == this.selectClase);
                this.arrTipos = this.arrActivos.filter(e=>e.niveluno == this.selectGrupo && e.niveldos == this.selectClase);
                if(app.arrGrupos.length > 0){
                    this.selectGrupo = this.arrGrupos[0].codigo;
                    this.selectTipoActivo = this.arrTipos[0].codigo;
                }
            }else if(type == 2){
                this.arrTipos = this.arrActivos.filter(e=>e.niveluno == this.selectGrupo && e.niveldos == this.selectClase);
            }
        },
        changeStatus:function(item){
            const vueThis = this;
            Swal.fire({
                title:"¿Estás segur@ de cambiar el estado?",
                text:"",
                icon: 'warning',
                showCancelButton:true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText:"Sí",
                cancelButtonText:"No, cancelar"
            }).then(async function(result){
                if(result.isConfirmed){
                    const formData = new FormData();
                    formData.append("action","change");
                    formData.append("id",item.codigo);
                    formData.append("estado",item.estado =='S' ? 'N' : 'S');
                    const response = await fetch(URL,{method:"POST",body:formData});
                    const objData = await response.json();
                    if(objData.status){
                        let index = vueThis.arrSearch.findIndex(e => e.codigo == item.codigo);
                        vueThis.arrSearch[index].estado = item.estado =='S' ? 'N' : 'S';
                        vueThis.arrSearch[index].is_status = vueThis.arrSearch[index].estado =='S' ? 1 : 0;
                        vueThis.search("search");
                        Swal.fire("Estado actualizado","","success");
                    }else{
                        Swal.fire("Error",objData.msg,"error");
                    }
                }else{
                    vueThis.search("search");
                }
            });

        },

    },
})
