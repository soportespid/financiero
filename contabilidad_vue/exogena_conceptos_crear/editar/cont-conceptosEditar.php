<?php
    require_once '../../../PHPExcel/Classes/PHPExcel.php';
    require_once '../../../comun.inc';
    require '../../../funciones.inc';
    require '../../../funcionesSP.inc.php';
    /*ini_set('display_errors', '1');
    ini_set('display_startup_errors', '1');
    error_reporting(E_ALL);*/
    session_start();
    if($_POST){
        $obj = new Plantilla();
        if($_POST['action']=="get"){
            $obj->getData();
        }else if($_POST['action']=="save"){
            $obj->save();
        }
    }

    class Plantilla{
        private $linkbd;
        private $arrData;
        private $strVigencia;
        public function __construct() {
            $this->linkbd = conectar_v7();
            $this->linkbd->set_charset("utf8");
        }
        public function getData(){
            if(!empty($_SESSION)){
                $id = intval($_POST['codigo']);
                $data = $this->selectConcepto($id);
                $max = $this->selectMax();
                if(!empty($data)){
                    $request['concepto'] = $data;
                    $request['consecutivo'] = $id;
                    $request['max'] = $max;
                    $request['formatos'] = $this->selectFormatos();
                    $arrResponse = array("status"=>true,"data"=>$request);
                }else{
                    $arrResponse = array("status"=>false,"max"=>$max);
                }
                echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
            }
            die();
        }
        public function save(){
            if(!empty($_SESSION)){
                if(empty($_POST['nombre']) || empty($_POST['codigo']) || empty($_POST['formato'])){
                    $arrResponse = array("status"=>false,"msg"=>"Error de datos");
                }else{
                    $request = $this->insertData($_POST['id'],$_POST['formato'],$_POST['codigo'],$_POST['nombre']);
                    if(is_numeric($request) && $request > 0){
                        $arrResponse = array("status"=>true,"msg"=>"Datos guardados.");
                    }else if($request =="existe"){
                        $arrResponse = array("status"=>false,"msg"=>"El concepto ya existe, pruebe con otro.");
                    }else{
                        $arrResponse = array("status"=>false,"msg"=>"Error");
                    }
                }
                echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
            }
            die();
        }
        public function selectConcepto($id){
            $sql = "SELECT * FROM contexogenaconceptos WHERE id = $id";
            $request = mysqli_query($this->linkbd,$sql)->fetch_assoc();
            return $request;
        }
        public function selectMax(){
            $sql = "SELECT MAX(id) as max FROM contexogenaconceptos";
            $request = mysqli_query($this->linkbd,$sql)->fetch_assoc()['max'];
            return $request;
        }
        public function selectFormatos(){
            $sql = "SELECT * FROM contexogenaformatos WHERE estado = 'S' ORDER BY formato";
            $arrData = mysqli_fetch_all(mysqli_query($this->linkbd,$sql),MYSQLI_ASSOC);
            return $arrData;
        }
        public function insertData($id,$formato,$codigo,$nombre){
            $sql = "SELECT * FROM contexogenaconceptos WHERE formato = '$formato' AND codigo = '$codigo' AND id!=$id";
            $request = mysqli_fetch_all(mysqli_query($this->linkbd,$sql),MYSQLI_ASSOC);
            if(empty($request)){
                $sql ="UPDATE contexogenaconceptos SET formato='$formato',codigo='$codigo',nombre='$nombre'
                WHERE id = $id";
                $request = intval(mysqli_query($this->linkbd,$sql));
            }else{
                $request = "existe";
            }
            return $request;
        }
    }
?>
