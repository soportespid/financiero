const URL ='contabilidad_vue/exogena_conceptos_crear/editar/cont-conceptosEditar.php';

var app = new Vue({
    el:"#myapp",
    data() {
        return {
            isLoading:false,
            txtCodigo:"",
            txtNombre:"",
            arrFormatos:[],
            selectFormato:0,
            txtMax:0,
            txtConsecutivo:0,
        }
    },
    mounted() {
        this.getData();
    },
    methods: {
        getData: async function(){
            let codigo = new URLSearchParams(window.location.search).get('id');
            const formData = new FormData();
            formData.append("action","get");
            formData.append("codigo",codigo);
            this.isLoading = true;
            const response = await fetch(URL,{method:"POST",body:formData});
            const objData = await response.json();
            if(objData.status){
                this.selectFormato = objData.data.concepto.formato
                this.txtCodigo = objData.data.concepto.codigo;
                this.txtNombre = objData.data.concepto.nombre;
                this.txtConsecutivo = objData.data.consecutivo;
                this.txtMax = objData.data.max;
                this.arrFormatos = objData.data.formatos;
            }else{
                window.location.href='cont-conceptosExogenaCEditar.php?id='+objData.max;
            }
            this.isLoading = false;
        },
        save: async function(){
            const vueContext = this;
            if(this.txtCodigo =="" || this.txtNombre =="" || this.selectFormato == 0){
                Swal.fire("Error","Todos los campos con (*) son obligatorios","error");
                return false;
            }
            let formData = new FormData();
            formData.append("action","save");
            formData.append("id",this.txtConsecutivo);
            formData.append("codigo",this.txtCodigo);
            formData.append("nombre",this.txtNombre);
            formData.append("formato",this.selectFormato);

            Swal.fire({
                title:"¿Estás segur@ de guardar?",
                text:"",
                icon: 'warning',
                showCancelButton:true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText:"Sí, guardar",
                cancelButtonText:"No, cancelar"
            }).then(async function(result){
                if(result.isConfirmed){
                    let response = await fetch(URL,{method:"POST",body:formData});
                    let objData = await response.json();
                    if(objData.status){
                        Swal.fire("Guardado",objData.msg,"success");
                    }else{
                        Swal.fire("Error",objData.msg,"error");
                    }
                }
            });
        },
        editItem:function(id){
            window.location.href='cont-conceptosExogenaCEditar.php?id='+id;
        },
    },
    computed:{

    }
})
