<?php
    //$URL_BBDD = true;
	require_once '../../../comun.inc';
    require '../../../funciones.inc';
    //require_once '../../../vue/presupuesto_ccp/funcionesccp.inc.php';
    session_start();

	$linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");


    $out = array('error' => false);

    $action = "show";

    if(isset($_GET['action'])){
        $action = $_GET['action'];
    }
    
    if($action == 'buscarCierres'){

        /* $out['errorS'] = var_dump('envuarl'); */
        //echo "shola";
        $centroCostos = array();

        $critFecha = '';
        $critNumAcuerdo = '';

        if($_POST['fechaInicial'] != '' && $_POST['fechaFinal'] != ''){
            $critFecha = " AND fecha BETWEEN '".$_POST['fechaInicial']."' AND '".$_POST['fechaFinal']."'";
        }

        $sqlr = "SELECT numerotipo, fecha, concepto, estado FROM comprobante_cab WHERE tipo_comp = 31 $critFecha ORDER BY numerotipo DESC";
        $res = mysqli_query($linkbd, $sqlr);
        while($row = mysqli_fetch_row($res)){
            array_push($centroCostos, $row);
        }
        $out['centroCostos'] = $centroCostos;
    }

    if($action == 'borrarCierre'){

        $consecutivo = $_GET['consecutivo'];
        $fechaCierre = $_GET['fechaCierre'];
        $tienePermiso = '';

        /* preg_match("/([0-9]{2})\/([0-9]{2})\/([0-9]{4})/", $fechaCierre,$fecha);
        $fechaf = "$fecha[3]-$fecha[2]-$fecha[1]"; */
        $bloq = bloqueos($_SESSION['cedulausu'],$fechaCierre);
        $vigegreso=$fecha[3];
        if($bloq >= 1){

            $tienePermiso = 'Si';

            $sqlrD_cab = "DELETE FROM comprobante_cab WHERE tipo_comp = 31 AND numerotipo = '$consecutivo'";
            mysqli_query($linkbd, $sqlrD_cab);

            cargartrazas('conta', 'comprobante_cab', 'borrar', $consecutivo, '31', $consecutivo, '', '', $sqlrD_cab);

            $sqlrD_det = "DELETE FROM comprobante_det WHERE tipo_comp = 31 AND numerotipo = '$consecutivo'";
            mysqli_query($linkbd, $sqlrD_det);
            cargartrazas('conta', 'comprobante_det', 'borrar', $consecutivo, '31', $consecutivo, '', '', $sqlrD_det);


        }else{

            $tienePermiso = 'No';

        }

        $out['tienePermiso'] = $tienePermiso;
    }

   
    header("Content-type: application/json");
    echo json_encode($out);
    die();