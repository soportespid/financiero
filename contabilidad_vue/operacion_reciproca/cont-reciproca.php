<?php

    /*ini_set('display_errors', '1');
    ini_set('display_startup_errors', '1');
    error_reporting(E_ALL);*/
    date_default_timezone_set("America/Bogota");
    require_once '../../comun.inc';
    require '../../funciones.inc';
    require '../../funcionesSP.inc.php';
    require '../../conversor.php';
    require_once '../../PHPExcel/Classes/PHPExcel.php';
    session_start();


    class Plantilla{
        private $linkbd;
        public function __construct() {
            $this->linkbd = conectar_v7();
            $this->linkbd->set_charset("utf8");
        }
        public function getData(){
            if(!empty($_SESSION)){
                $arrData = array(
                    "periodos"=>$this->selectPeriodos(),
                    "entidad"=>$this->selectCodigoEntidad()
                );
                echo json_encode($arrData,JSON_UNESCAPED_UNICODE);
            }
            die();
        }
        public function getBalance($vigencia=null,$periodo=null){
            if(!empty($_SESSION)){
                $strVigencia = $vigencia != null ? $vigencia : $_POST['vigencia'];
                $strMes1 = $periodo != null ? substr($periodo,1,2) : substr($_POST['periodo'],1,2);
                $strMes2 = $periodo != null ? substr($periodo,3,2) : substr($_POST['periodo'],3,2);
                $strFechaInicial=$strVigencia."-".$strMes1."-01";
			    $strFechaFinal=$strVigencia."-".$strMes2."-".intval(date("t",$strMes2));
                $arrReciprocas = $this->selectReciprocas();
                $arrNiveles = $this->selectNiveles();
                $intNivel = $arrNiveles[count($arrNiveles)-2]['posiciones'];
                $arrData = $this->selectMovimientosPeriodos(
                    $intNivel,
                    $strFechaInicial,
                    $strFechaFinal,
                    $arrReciprocas['sin_valor']
                );
                foreach ($arrReciprocas['con_valor'] as $data) {
                    $data['comprobante'] = $data['cuenta'];
                    $data['saldo'] = $data['valor'];
                    array_push($arrData,$data);
                }
                $arrData = $this->setNiveles($arrData,$arrNiveles,$intNivel);
                $arrResponse = $this->orderData($arrData,$intNivel);
                if($_POST){
                    echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
                }else{
                    return $arrResponse;
                }
            }
            die();
        }
        public function orderData($data,$nivel){
            $arrComp = array_column($data,"comprobante");
            $arrChip = [2,3,4,9];
            $arrNoCorriente = [3,4,5,6,7,8,9];
            $arrNoCorriente2 = [16,17,18,27];
            $arrData = [];
            usort($arrComp, 'strcmp');
            $arrComp = array_values(array_unique($arrComp));
            $totalComp = count($arrComp);
            $totalData = count($data);
            $totalSaldo = 0;
            for ($i=0; $i < $totalComp ; $i++) {
                for ($j=0; $j < $totalData; $j++) {
                    $arrInfo = $data[$j];
                    if($arrComp[$i] == $data[$j]['comprobante']){
                        $comprobante = $arrComp[$i];
                        $saldo = round($arrInfo['saldo'],2);
                        $saldoAbs = abs($saldo);
                        $arrInfo['saldo_chip'] = $saldo;
                        $arrInfo['tipo'] = 0;
                        if(in_array(substr($comprobante,0,1),$arrChip) && $arrInfo['reciproca']==0){
                            $arrInfo['saldo_chip'] = $saldo * -1;
                        }
                        if(in_array(substr($comprobante,0,1),$arrNoCorriente) || in_array(substr($comprobante,0,2),$arrNoCorriente2)){
                            $arrInfo['tipo'] = 1;
                        }
                        if(strlen($comprobante) >=$nivel){
                            $totalSaldo+=$saldo;
                        }
                        $arrInfo['saldo'] = $saldoAbs;
                        $sql = "SELECT nombre FROM cuentasnicsp WHERE cuenta='$comprobante'";
                        $arrNombre = mysqli_query($this->linkbd,$sql)->fetch_assoc();
                        if(!empty($arrNombre)){$arrInfo['nombre'] = strtoupper($arrNombre['nombre']);}
                        array_push($arrData,$arrInfo);
                        break;
                    }
                }
            }
            $totalSaldo = intval(abs($totalSaldo));
            $arrData = array_values(array_filter($arrData,function($e){return $e['saldo'] > 0;}));
            return array("data"=>$arrData,"total"=>$totalSaldo);
        }
        public function setNiveles($arrData,$arrNiveles,$intNivel){
            $arrNewData = [];
            $totalData = count($arrData);
            for ($i=0; $i < $intNivel ; $i++) {
                for ($j=0; $j < $totalData; $j++) {
                    $data = $arrData[$j];
                    $data['nombre'] = "";
                    $data['comprobante'] = (string)$data['comprobante'];
                    if(strlen($data['comprobante']) < $intNivel){
                        $data['tercero']="";
                    }
                    if(strlen($data['comprobante']>=$arrNiveles[$i-1]['posiciones'])){
                        $strComprobante = substr($data['comprobante'],0,$arrNiveles[$i-2]['posiciones']);
                        if($strComprobante!=""){
                            $totalDataTemp = count($arrNewData);
                            $flag = false;
                            for ($k=0; $k < $totalDataTemp; $k++) {
                                if($strComprobante == $arrNewData[$k]['comprobante']){
                                    $arrNewData[$k]['saldo']+=$data['saldo'];
                                    $arrNewData[$k]['debito']+=$data['debito'];
                                    $arrNewData[$k]['credito']+=$data['credito'];
                                    $flag = true;
                                    break;
                                }
                            }
                            if(!$flag){
                                $data['comprobante'] = $strComprobante;
                                array_push($arrNewData,$data);
                            }
                        }
                    }
                }
            }
            return $arrNewData;
        }
        public function selectMovimientosPeriodos($intNivel,$strFechaInicial,$strFechaFinal,$arrReciprocas){
            $arrCriterios = $this->selectCriterios();
            $strCriterioCostos ="";
            if(!empty($arrCriterios['data'])){
                $strCriterioCostos = "AND comprobante_det.centrocosto NOT IN (".implode(",",$arrCriterios['data']).")";
            }
            $arrBases = $this->selectBases();
            $arrData = [];
            $arrConexiones = [];
            $arrTempData=[];
            foreach($arrBases as $base){
                $linkmulti = conectar_Multi($base['base'], $base['usuario']);
                $linkmulti -> set_charset("utf8");
                $arrConexiones[] = $linkmulti;
            }
            foreach ($arrReciprocas as $reciproca) {
                //Movimientos del periodo
                $sql="SELECT DISTINCT
                SUBSTR(comprobante_det.cuenta,1,$intNivel) as comprobante,
                comprobante_det.valdebito as debito,
                comprobante_det.valcredito as credito,
                COALESCE((sum(comprobante_det.valdebito)- sum(comprobante_det.valcredito)),0) as saldo
                FROM comprobante_det
                RIGHT JOIN comprobante_cab ON comprobante_cab.tipo_comp = comprobante_det.tipo_comp
				AND comprobante_det.numerotipo = comprobante_cab.numerotipo
				WHERE comprobante_cab.estado = 1
                AND (comprobante_det.valdebito > 0 OR comprobante_det.valcredito > 0)
                AND comprobante_cab.fecha BETWEEN '$strFechaInicial' AND '$strFechaFinal'
                AND comprobante_det.tipo_comp NOT IN(7,100,102,101,103,19,104)
                AND comprobante_det.cuenta LIKE '$reciproca[cuenta]%'
                GROUP BY SUBSTR(comprobante_det.cuenta,1,$intNivel)
                ORDER BY comprobante_det.cuenta";


                //Saldo inicial
                $sqlSaldo="SELECT DISTINCT
                SUBSTR(comprobante_det.cuenta,1,$intNivel) as comprobante,
                comprobante_det.valdebito as debito,
                comprobante_det.valcredito as credito,
                COALESCE((sum(comprobante_det.valdebito)- sum(comprobante_det.valcredito)),0) as saldo
                FROM comprobante_det
                RIGHT JOIN comprobante_cab ON comprobante_cab.tipo_comp = comprobante_det.tipo_comp
				AND comprobante_det.numerotipo = comprobante_cab.numerotipo
				WHERE comprobante_cab.estado = 1
                AND (comprobante_det.valdebito > 0 OR comprobante_det.valcredito > 0) AND comprobante_det.tipo_comp = 102
                AND comprobante_det.cuenta LIKE '$reciproca[cuenta]%'  $strCriterioCostos
                GROUP BY SUBSTR(comprobante_det.cuenta,1,$intNivel) ORDER BY comprobante_det.cuenta";

                //Periodos
                $sqlPeriodo="SELECT DISTINCT
                SUBSTR(comprobante_det.cuenta,1,$intNivel) as comprobante,
                comprobante_det.valdebito as debito,
                comprobante_det.valcredito as credito,
                COALESCE((sum(comprobante_det.valdebito)- sum(comprobante_det.valcredito)),0) as saldo
                FROM comprobante_det
                RIGHT JOIN comprobante_cab ON comprobante_cab.tipo_comp = comprobante_det.tipo_comp
				AND comprobante_det.numerotipo = comprobante_cab.numerotipo
                WHERE comprobante_cab.estado = 1
                AND (comprobante_det.valdebito > 0 OR comprobante_det.valcredito > 0)
                AND comprobante_det.tipo_comp NOT IN(100,101,103,104,102,7,19) $strCriterioCostos
                AND comprobante_cab.fecha BETWEEN '2018-01-01' AND '$strFechaFinal' AND comprobante_det.cuenta LIKE '$reciproca[cuenta]%'
                GROUP BY SUBSTR(comprobante_det.cuenta,1,$intNivel)
                ORDER BY comprobante_det.cuenta";
                foreach($arrConexiones as $linkmulti){
                    $movimiento = mysqli_query($linkmulti,$sqlSaldo)->fetch_assoc();
                    if(!empty($movimiento)){
                        $movimiento['tercero'] = $reciproca['tercero'];
                        array_push($arrTempData,$movimiento);
                    }
                    if($strFechaFinal>='2018-01-01'){
                        $movimiento = mysqli_query($linkmulti,$sqlPeriodo)->fetch_assoc();
                        if(!empty($movimiento)){
                            $movimiento['tercero'] = $reciproca['tercero'];
                            array_push($arrTempData,$movimiento);
                        }
                    }else{
                        $movimiento = mysqli_query($linkmulti,$sql)->fetch_assoc();
                        if(!empty($movimiento)){
                            $movimiento['tercero'] = $reciproca['tercero'];
                            array_push($arrTempData,$movimiento);
                        }
                    }
                }
            }
            foreach ($arrTempData as $data) {
                $totalData = count($arrData);
                if($totalData > 0){
                    $flag = false;
                    for ($i=0; $i < $totalData ; $i++) {
                        if($data['comprobante'] == $arrData[$i]['comprobante']){
                            $arrData[$i]['saldo']+=$data['saldo'];
                            $arrData[$i]['debito']+=$data['debito'];
                            $arrData[$i]['credito']+=$data['credito'];
                            $flag = true;
                            break;
                        }
                    }
                    if(!$flag){
                        array_push($arrData,$data);
                    }
                }else{
                    array_push($arrData,$data);
                }
            }
            return $arrData;
        }
        public function selectPeriodos(){
            $sql = "SELECT * FROM chip_periodos WHERE estado='S' ORDER BY id";
            $request = mysqli_fetch_all(mysqli_query($this->linkbd,$sql),MYSQLI_ASSOC);
            return $request;
        }
        public function selectBases(){
            $sql = "SELECT base, usuario FROM redglobal";
            $request = mysqli_fetch_all(mysqli_query($this->linkbd,$sql),MYSQLI_ASSOC);
            return $request;
        }
        public function selectNiveles(){
            $sql = "SELECT * FROM nivelesctas  WHERE estado='S' ORDER BY id_nivel";
            $request = mysqli_fetch_all(mysqli_query($this->linkbd,$sql),MYSQLI_ASSOC);
            return $request;
        }
        public function selectReciprocas(){
            $sql = "SELECT DISTINCT cuenta, tercero, sum(valor) as valor FROM cuentasreciprocas
            WHERE estado ='S' GROUP BY cuenta ORDER BY cuenta";
            $request = mysqli_fetch_all(mysqli_query($this->linkbd,$sql),MYSQLI_ASSOC);
            $arrData['sin_valor'] = array_values(array_filter($request,function($e){$e['reciproca'] = 0; return $e['valor']==0;}));
            $arrData['con_valor'] = array_values(array_filter($request,function($e){$e['reciproca'] = 1; return  $e['valor']!=0;}));
            for ($i=0; $i < count($arrData['sin_valor']); $i++) { $arrData['sin_valor'][$i]['reciproca'] = 0;}
            for ($i=0; $i < count($arrData['con_valor']); $i++) { $arrData['con_valor'][$i]['reciproca'] = 1;}
            return $arrData;
        }
        public function selectCriterios(){
            $arrCriterio = [];
            $strCriterio ="";
            $sql = "SELECT id_cc FROM centrocosto WHERE entidad='N'";
            $request = mysqli_fetch_all(mysqli_query($this->linkbd,$sql),MYSQLI_ASSOC);
            foreach ($request as $r) {
                $strCriterio.="AND comprobante_det.centrocosto <> ".$r['id_cc'];
                array_push($arrCriterio,$r['id_cc']);
            }
            return array("data"=>$arrCriterio,"str_data"=>$strCriterio);
        }
        public function selectCodigoEntidad(){
            $sql="SELECT *FROM configbasica WHERE estado='S'";
            $request = mysqli_query($this->linkbd,$sql)->fetch_assoc()['codcontaduria'];
            return $request;
        }
    }
    if($_POST){
        $obj = new Plantilla();
        if($_POST['action']=="get"){
            $obj->getData();
        }else if($_POST['action']=="balance"){
            $obj->getBalance();
        }
    }
    if($_GET){
        $obj = new Plantilla();
        $request = $obj->getBalance($_GET['vigencia'],$_GET['periodo']);
        $arrDet = $request['data'];
        $objPHPExcel = new PHPExcel();
        $objPHPExcel->getActiveSheet()->getStyle('A:J')->applyFromArray(
            array(
                'alignment' => array(
                    'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
                    'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER
                )
            )
        );
        $objPHPExcel->getProperties()
        ->setCreator("IDEAL 10")
        ->setLastModifiedBy("IDEAL 10")
        ->setTitle("Exportar Excel con PHP")
        ->setSubject("Documento de prueba")
        ->setDescription("Documento generado con PHPExcel")
        ->setKeywords("usuarios phpexcel")
        ->setCategory("reportes");

        //----Cuerpo de Documento----
        $objPHPExcel->setActiveSheetIndex(0)
        ->mergeCells('A1:F1')
        ->mergeCells('A2:F2')
        ->setCellValue('A1', 'CONTABILIDAD')
        ->setCellValue('A2', 'OPERACIONES RECÍPROCAS');

        $objPHPExcel-> getActiveSheet ()
        -> getStyle ("A1")
        -> getFill ()
        -> setFillType (PHPExcel_Style_Fill :: FILL_SOLID)
        -> getStartColor ()
        -> setRGB ('C8C8C8');
        $objPHPExcel-> getActiveSheet ()
        -> getStyle ("A1:A2")
        -> getFont ()
        -> setBold ( true )
        -> setName ( 'Verdana' )
        -> setSize ( 10 )
        -> getColor ()
        -> setRGB ('000000');
        $objPHPExcel-> getActiveSheet ()
        -> getStyle ('A1:A2')
        -> getAlignment ()
        -> applyFromArray (array ( 'horizontal'  =>  PHPExcel_Style_Alignment :: HORIZONTAL_CENTER ,) );
        $objPHPExcel-> getActiveSheet ()
        -> getStyle ('A3:F3')
        -> getAlignment ()
        -> applyFromArray (array ( 'horizontal'  =>  PHPExcel_Style_Alignment :: VERTICAL_JUSTIFY ,) );
        $objPHPExcel-> getActiveSheet ()
        -> getStyle ("A2")
        -> getFill ()
        -> setFillType (PHPExcel_Style_Fill :: FILL_SOLID);

        $borders = array(
            'borders' => array(
                'allborders' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN,
                'color' => array('argb' => 'FF000000'),
                )
            ),
        );
        $objPHPExcel->setActiveSheetIndex(0)
        ->setCellValue('A3', 'Código')
        ->setCellValue('B3', "Cuenta")
        ->setCellValue('C3', "Código entidad")
        ->setCellValue('D3', "Saldo final")
        ->setCellValue('E3', "Corriente")
        ->setCellValue('F3', "No corriente");
        $objPHPExcel-> getActiveSheet ()
            -> getStyle ("A3:F3")
            -> getFill ()
            -> setFillType (PHPExcel_Style_Fill :: FILL_SOLID)
            -> getStartColor ()
            -> setRGB ('99ddff');
        $objPHPExcel->getActiveSheet()->getStyle('A1:F1')->applyFromArray($borders);
        $objPHPExcel->getActiveSheet()->getStyle('A2:F2')->applyFromArray($borders);
        $objPHPExcel->getActiveSheet()->getStyle('A3:F3')->applyFromArray($borders);

        $objPHPExcel->getActiveSheet()->getStyle("A3")->getFont()->setBold(true);
        $objPHPExcel->getActiveSheet()->getStyle("B3")->getFont()->setBold(true);
        $objPHPExcel->getActiveSheet()->getStyle("C3")->getFont()->setBold(true);
        $objPHPExcel->getActiveSheet()->getStyle("D3")->getFont()->setBold(true);
        $objPHPExcel->getActiveSheet()->getStyle("E3")->getFont()->setBold(true);
        $objPHPExcel->getActiveSheet()->getStyle("F3")->getFont()->setBold(true);

        $row = 4;
        $totalData = count($arrDet);
        foreach ($arrDet as $data) {
            $objPHPExcel->setActiveSheetIndex(0)
            ->setCellValueExplicit ("A$row", $data['comprobante'], PHPExcel_Cell_DataType :: TYPE_STRING)
            ->setCellValueExplicit ("B$row", $data['nombre'], PHPExcel_Cell_DataType :: TYPE_STRING)
            ->setCellValueExplicit ("C$row", $data['tercero'], PHPExcel_Cell_DataType :: TYPE_STRING)
            ->setCellValueExplicit ("D$row", $data['saldo'], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
            ->setCellValueExplicit ("E$row", $data['tipo'] == 0 ? $data['saldo_chip'] : 0, PHPExcel_Cell_DataType :: TYPE_NUMERIC)
            ->setCellValueExplicit ("F$row", $data['tipo'] == 1 ? $data['saldo_chip'] : 0, PHPExcel_Cell_DataType :: TYPE_NUMERIC);
            $objPHPExcel->getActiveSheet()->getStyle("A$row:F$row")->applyFromArray($borders);
            if(strlen($data['comprobante'])<6){
                $objPHPExcel-> getActiveSheet ()
                -> getStyle ("A$row:F$row")
                -> getFill ()
                -> setFillType (PHPExcel_Style_Fill :: FILL_SOLID)
                -> getStartColor ()
                -> setRGB ('3399cc');
                $objPHPExcel->getActiveSheet()->getStyle("A$row")->getFont()->getColor()->setRGB("ffffff");
                $objPHPExcel->getActiveSheet()->getStyle("B$row")->getFont()->getColor()->setRGB("ffffff");
                $objPHPExcel->getActiveSheet()->getStyle("C$row")->getFont()->getColor()->setRGB("ffffff");
                $objPHPExcel->getActiveSheet()->getStyle("D$row")->getFont()->getColor()->setRGB("ffffff");
                $objPHPExcel->getActiveSheet()->getStyle("E$row")->getFont()->getColor()->setRGB("ffffff");
                $objPHPExcel->getActiveSheet()->getStyle("F$row")->getFont()->getColor()->setRGB("ffffff");
                $objPHPExcel->getActiveSheet()->getStyle("A$row")->getFont()->setBold(true);
                $objPHPExcel->getActiveSheet()->getStyle("B$row")->getFont()->setBold(true);
                $objPHPExcel->getActiveSheet()->getStyle("C$row")->getFont()->setBold(true);
                $objPHPExcel->getActiveSheet()->getStyle("D$row")->getFont()->setBold(true);
                $objPHPExcel->getActiveSheet()->getStyle("E$row")->getFont()->setBold(true);
                $objPHPExcel->getActiveSheet()->getStyle("F$row")->getFont()->setBold(true);
            }
            $row++;
        }
        $objPHPExcel->setActiveSheetIndex(0)
        ->mergeCells("A$row:B$row")
        ->setCellValue("C$row", 'TOTAL ')
        ->setCellValueExplicit ("D$row", $request['total'], PHPExcel_Cell_DataType :: TYPE_NUMERIC);
        $objPHPExcel-> getActiveSheet ()
                -> getStyle ("A$row:D$row")
                -> getFill ()
                -> setFillType (PHPExcel_Style_Fill :: FILL_SOLID)
                -> getStartColor ()
                -> setRGB ('3399cc');
        $objPHPExcel->getActiveSheet()->getStyle("C$row")->getFont()->getColor()->setRGB("ffffff");
        $objPHPExcel->getActiveSheet()->getStyle("D$row")->getFont()->getColor()->setRGB("ffffff");
        $objPHPExcel->getActiveSheet()->getStyle("C$row")->getFont()->setBold(true);
        $objPHPExcel->getActiveSheet()->getStyle("D$row")->getFont()->setBold(true);
        $objPHPExcel->getActiveSheet()->getStyle("D$row")->getFont()->setBold(true);

        $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);

        $objPHPExcel->getActiveSheet()->getStyle("A$row:F$row")->applyFromArray($borders);

        //----Guardar documento----
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="CGN2015_002_OPERACIONES_RECIPROCAS_CONVERGENCIA.xlsx"');
        header('Cache-Control: max-age=0');
        $objWriter=PHPExcel_IOFactory::createWriter($objPHPExcel,'Excel2007');
        $objWriter->save('php://output');
        die();
    }
?>
