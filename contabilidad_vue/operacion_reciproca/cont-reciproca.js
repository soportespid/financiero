const URL ='contabilidad_vue/operacion_reciproca/cont-reciproca.php';

var app = new Vue({
    el:"#myapp",
    data() {
        return {
            isLoading:false,
            selectVigencia: new Date().getFullYear(),
            selectPeriodo:1,
            arrPeriodos:[],
            arrVigencias:[],
            arrData:[],
            total:0,
            txtEntidad:"",
        }
    },
    mounted() {
        this.getData();
    },
    methods: {
        setVigencias:function(){
            for (let i = this.selectVigencia-2; i <= this.selectVigencia; i++) {
                this.arrVigencias.push(i);
            }
            this.arrVigencias.sort(function(a,b){return b-a});
        },
        getData: async function(){
            const formData = new FormData();
            formData.append("action","get");
            this.isLoading = true;
            const response = await fetch(URL,{method:"POST",body:formData});
            const objData = await response.json();
            this.isLoading = false;
            this.arrPeriodos = objData.periodos;
            this.txtEntidad = objData.entidad;
            this.setVigencias();
        },
        getBalance: async function(){
            let vueContext = this;
            let periodo = this.arrPeriodos.filter(function(e){return e.id == vueContext.selectPeriodo})[0].periodo;
            const formData = new FormData();
            formData.append("action","balance");
            formData.append("vigencia",this.selectVigencia);
            formData.append("periodo",periodo);
            this.isLoading = true;
            const response = await fetch(URL,{method:"POST",body:formData});
            const objData = await response.json();
            this.isLoading = false;
            this.arrData = objData.data;
            this.total =objData.total;
        },
        exportExcel:function(){
            const vueContext = this;
            const periodo = this.arrPeriodos.filter(function(e){return e.id == vueContext.selectPeriodo})[0].periodo;
            const data = this.arrData.filter(function(e){return e.comprobante.length >=6});
            const totalRows = data.length;
            const params = "?vigencia="+this.selectVigencia+"&periodo="+periodo;
            if(totalRows == 0){
                Swal.fire("Error","Debe generar datos antes de exportar.","error");
                return false;
            }
            window.open(URL+params,"_blank");
        },
        exportData:function(){
            const vueContext = this;
            const periodo = this.arrPeriodos.filter(function(e){return e.id == vueContext.selectPeriodo })[0]['periodo'];
            const data = this.arrData.filter(function(e){return e.comprobante.length >=6});
            const totalRows = data.length
            if(totalRows == 0){
                Swal.fire("Error","Debe generar datos antes de exportar.","error");
                return false;
            }
            const delimiter = "\t";

            const headers = [
                "S",
                this.txtEntidad,
                periodo+"\t"+this.selectVigencia,
                "CGN2015_002_OPERACIONES_RECIPROCAS_CONVERGENCIA",
            ];
            let content = headers.join(delimiter)+"\n";

            for (let i = 0; i < totalRows; i++) {
                const e = data[i];
                const parte1 = e.comprobante.slice(0,1);
                const parte2 = e.comprobante.slice(1,2);
                const parte3 = e.comprobante.slice(2,4);
                const parte4 = e.comprobante.slice(4,6);
                const comprobante = parte1+"."+parte2+"."+parte3+"."+parte4

                const row = [
                    "D",
                    comprobante,
                    e.tercero,
                    e.tipo == 0 ? e.saldo_chip : 0,
                    e.tipo == 1 ? e.saldo_chip : 0,
                ];
                content+=row.join(delimiter)+"\n";

            }
            const link = document.createElement('a');
            link.href = 'data:text/txt;charset=utf-8,' + encodeURIComponent(content);
            link.target = '_blank';
            link.download = `CGN2015_002_OPERACIONES_RECIPROCAS_CONVERGENCIA.txt`;
            link.click();
        },
        formatNumero: function(valor){
            return new Intl.NumberFormat("en-US", {style: "currency", currency: "USD"}).format(valor);
        },
    },
    computed:{

    }
})
