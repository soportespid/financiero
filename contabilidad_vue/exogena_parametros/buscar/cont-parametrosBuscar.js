const URL ='contabilidad_vue/exogena_parametros/buscar/cont-parametrosBuscar.php';

var app = new Vue({
    el:"#myapp",
    data() {
        return {
            isLoading:false,
            arrData:[],
            txtResults:0,
            txtSearch:""
        }
    },
    mounted() {
        this.getData();
    },
    methods: {

        getData: async function(){

            const formData = new FormData();
            formData.append("action","get");
            this.isLoading=true;
            const response = await fetch(URL,{method:"POST", body:formData});
            const objData = await response.json();
            this.arrData = objData;
            console.log(this.arrData);
            this.isLoading=false;
        },
        editItem:function(id){
            window.location.href="cont-parametrosExogenaEditar.php?id="+id;
        }
    },
    computed:{

    }
})
