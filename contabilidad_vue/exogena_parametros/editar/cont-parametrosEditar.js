const URL ='contabilidad_vue/exogena_parametros/editar/cont-parametrosEditar.php';

var app = new Vue({
    el:"#myapp",
    data() {
        return {
            isLoading:false,
            isModal:false,
            arrFormatos:[],
            arrFormato:[],
            arrFormatoDetalle:[],
            arrColumnas:[],
            arrColumnasFijas:[],
            arrColumnasCalculadas:[],
            arrConceptos:[],
            arrConceptosFormato:[],
            selectFormato:0,
            checkColumnaFija:true,
            checkColumnaCalculada:true,
        }
    },
    mounted() {
        this.getData();
    },
    methods: {
        getData:async function(){
            let codigo = new URLSearchParams(window.location.search).get('id');
            let vueContext = this;
            const formData = new FormData();
            formData.append("action","get");
            formData.append("codigo",codigo);
            const response = await fetch(URL,{method:"POST",body:formData});
            const objData = await response.json();
            this.arrFormatos = objData.formatos;
            this.arrFormato = objData.cabecera;
            this.arrFormatoDetalle = objData.detalle;
            this.arrColumnas = objData.columnas;
            this.arrConceptos = objData.conceptos;
            this.arrColumnas.forEach(e => {
                e['check'] = false;
                e['valor']=0;
            });
            this.arrColumnasFijas = this.arrColumnas.filter(e =>e.tipo == 1);
            this.arrColumnasCalculadas = this.arrColumnas.filter(e => e.tipo == 2);

            this.selectFormato = this.arrFormato[0].formato;


            //Asigno el valor guardado en cada columna calculada
            for (let i = 0; i < vueContext.arrColumnas.length; i++) {
                for (let j = 0; j < vueContext.arrFormato.length; j++) {
                    if(this.arrColumnas[i].id == this.arrFormato[j].columna){
                        this.arrColumnas[i].check = this.arrFormato[j].visible == "1" ? true : false;
                        this.arrColumnas[i].valor = this.arrFormato[j].valor;
                    }

                }
            }
            this.arrConceptosFormato.forEach(e=>{e['selected_col'] = 0
                e['columnas'] = this.arrColumnasCalculadas

            });

            //Asigno las columnas calculadas a cada cuenta
            this.getConceptos();
            for (let i = 0; i < vueContext.arrConceptosFormato.length; i++) {
                this.arrConceptosFormato[i]['selected_col'] = this.arrFormatoDetalle[i].columna_id;
            }
        },
        getConceptos:function(){
            this.arrConceptosFormato = [...this.arrConceptos.filter(e => e.formato == this.selectFormato)];
            this.arrConceptosFormato.forEach(e=>{
                e['columnas'] = [...this.arrColumnasCalculadas.filter(e => e.check == true)];
                if(e.selected_col){
                    let objCol = e.columnas.filter(col=> col.id == e.selected_col)[0];
                    if(typeof objCol != "undefined"){
                        e.selected_col = objCol.id;
                    }else{
                        e.selected_col = 0;
                    }
                }else{
                    e['selected_col'] = 0;
                }
            });
        },
        save:async function(){
            if(this.selectFormato == 0){
                Swal.fire("Error","Debe seleccionar un formato","error");
                return false;
            }
            let arrDetalle = [];
            let arrColumnasCalculadas = [...this.arrColumnasCalculadas.filter(e=>e.check)];
            let totalConceptos = this.arrConceptosFormato.length;
            let totalColumnas = arrColumnasCalculadas.length;
            for (let i = 0; i < totalConceptos; i++) {
                let concepto = this.arrConceptosFormato[i];
                arrDetalle.push({
                    "concepto":concepto.codigo_concepto,
                    "cuenta":concepto.cuenta,
                    "columna":concepto.selected_col,
                    "tipo":concepto.tipo
                });
            }
            let arrDetalleTotal = [];
            for (let i = 0; i < totalColumnas; i++) {
                let columna = arrColumnasCalculadas[i];
                for (let j = 0; j < arrDetalle.length; j++) {
                    if(arrDetalle[j].columna == columna.id){
                        arrDetalleTotal.push(arrDetalle[i]);
                        break;
                    }
                }
            }
            if(arrDetalleTotal.length == 0){
                Swal.fire("Error","Al menos una columna calculada debe tener una cuenta","error");
                return false;
            }
            let obj = {
                "formato":this.selectFormato,
                "cabecera":this.arrColumnas,
                "detalle":arrDetalle
            }
            const formData = new FormData();
            const vueContext = this;
            formData.append("action","save");
            formData.append("data",JSON.stringify(obj));
            Swal.fire({
                title:"¿Estás segur@ de guardar?",
                text:"",
                icon: 'warning',
                showCancelButton:true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText:"Sí, guardar",
                cancelButtonText:"No, cancelar"
            }).then(async function(result){
                if(result.isConfirmed){
                    let response = await fetch(URL,{method:"POST",body:formData});
                    let objData = await response.json();
                    if(objData.status){
                        Swal.fire("Guardado",objData.msg,"success");
                        vueContext.getData();
                    }else{
                        Swal.fire("Error",objData.msg,"error");
                    }
                }
            });
        },
        showTab:function(tab){
            let tabs = this.$refs.rTabs.children;
            let tabsContent = this.$refs.rTabsContent.children;
            for (let i = 0; i < tabs.length; i++) {
                tabs[i].classList.remove("active");
                tabsContent[i].classList.remove("active")
            }
            tabs[tab-1].classList.add("active");
            tabsContent[tab-1].classList.add("active")
        }
    },
    computed:{

    }
})
