<?php
    require_once '../../../PHPExcel/Classes/PHPExcel.php';
    require_once '../../../comun.inc';
    require '../../../funciones.inc';
    require '../../../funcionesSP.inc.php';
    /*ini_set('display_errors', '1');
    ini_set('display_startup_errors', '1');
    error_reporting(E_ALL);*/
    session_start();
    if($_POST){
        $obj = new Plantilla();
        if($_POST['action']=="get"){
            $obj->getData();
        }else if($_POST['action']=="save"){
            $obj->save();
        }
    }

    class Plantilla{
        private $linkbd;
        private $arrData;
        private $strVigencia;
        public function __construct() {
            $this->linkbd = conectar_v7();
            $this->linkbd->set_charset("utf8");
        }
        public function getData(){
            if(!empty($_SESSION)){
                $formato = $this->selectFormato($_POST['codigo']);
                if(!empty($formato)){
                    $request['cabecera'] = $formato['cabecera'];
                    $request['detalle'] = $formato['detalle'];
                    $request['formatos'] = $this->selectFormatos();
                    $request['columnas'] = $this->selectColumnas();
                    $request['conceptos'] = $this->selectConceptos();
                    echo json_encode($request,JSON_UNESCAPED_UNICODE);
                }
            }
            die();
        }
        public function save(){
            if(!empty($_SESSION)){
                $arrData = json_decode($_POST['data'],true);
                //dep($arrData);exit;
                if(empty($arrData)){
                    $arrResponse = array("status"=>false,"msg"=>"Error de datos");
                }else{
                    $request = $this->insertParametros($arrData);
                    if($request > 0){
                        $arrResponse = array("status"=>true,"msg"=>"Parámetros actualizados para el formato ".$arrData['formato']);
                    }else{
                        $arrResponse = array("status"=>false,"msg"=>"Hubo un error, intente de nuevo");
                    }

                }
                echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
            }
            die();
        }
        public function selectFormato($codigo){

            $sql = "SELECT * FROM contexogenaparametros_cab WHERE formato = '$codigo'";
            $arrData = mysqli_fetch_all(mysqli_query($this->linkbd,$sql),MYSQLI_ASSOC);
            $arrDetalle = [];
            if(!empty($arrData)){
                $sql = "SELECT * FROM contexogenaparametros_det WHERE formato = '$codigo'";
                $arrDetalle = mysqli_fetch_all(mysqli_query($this->linkbd,$sql),MYSQLI_ASSOC);
            }
            return array("cabecera"=>$arrData,"detalle"=>$arrDetalle);
        }
        public function insertParametros(array $arrData){
            $formato = $arrData['formato'];
            $arrCab = $arrData['cabecera'];
            $arrDet = $arrData['detalle'];
            $sql = "DELETE FROM contexogenaparametros_cab WHERE formato = '$formato'";
            mysqli_query($this->linkbd, $sql);

            $sql = "DELETE FROM contexogenaparametros_det WHERE formato = '$formato'";
            mysqli_query($this->linkbd, $sql);

            $totalCab = count($arrCab);
            $totalDet = count($arrDet);
            for ($i=0; $i < $totalCab; $i++) {
                $cab = $arrCab[$i];
                $cab['check'] = $cab['check'] == "" ? 0 : 1;
                $cab['valor'] = $cab['check'] == 1 ? $cab['valor']:0;
                $sql = "INSERT INTO contexogenaparametros_cab(formato,columna,valor,tipo,visible)
                VALUES('$formato',$cab[id],$cab[valor],$cab[tipo],$cab[check])";
                $request= mysqli_query($this->linkbd,$sql);
            }
            for ($j=0; $j < $totalDet ; $j++) {
                $det = $arrDet[$j];
                $sql ="INSERT INTO contexogenaparametros_det(formato,concepto,columna_id,cuenta,tipo)
                VALUES('$formato','$det[concepto]',$det[columna],'$det[cuenta]',$det[tipo])";
                mysqli_query($this->linkbd,$sql);
            }
            return $request;
        }
        public function selectFormatos(){
            $sql = "SELECT * FROM contexogenaformatos ORDER BY formato";
            $request = mysqli_fetch_all(mysqli_query($this->linkbd,$sql),MYSQLI_ASSOC);
            return $request;
        }
        public function selectColumnas(){
            $sql = "SELECT * FROM contexogenacolumnas ORDER BY id";
            $request = mysqli_fetch_all(mysqli_query($this->linkbd,$sql),MYSQLI_ASSOC);
            return $request;
        }
        public function selectConceptos(){
            $sql = "SELECT DISTINCT cab.formato,cab.id,cab.codigo,co.nombre
            FROM contexogenaconce_cab cab
            INNER JOIN contexogenaconceptos co
            ON cab.codigo = co.codigo
            INNER JOIN contexogenaformatos f
            ON f.formato = cab.formato
            WHERE cab.estado = 'S'
            GROUP BY cab.codigo";
            $arrConceptos = mysqli_fetch_all(mysqli_query($this->linkbd,$sql),MYSQLI_ASSOC);
            $arrDetalle = [];
            if(!empty($arrConceptos)){
                $total = count($arrConceptos);
                for ($i=0; $i < $total ; $i++) {
                    $sql = "SELECT cuenta,nombre,tipo FROM contexogenaconce_det WHERE codigo = {$arrConceptos[$i]['id']} ORDER BY id";
                    $detalles = mysqli_fetch_all(mysqli_query($this->linkbd,$sql),MYSQLI_ASSOC);
                    $totalDet = count($detalles);
                    for ($j=0; $j < $totalDet ; $j++) {
                        $det['codigo_concepto'] = $arrConceptos[$i]['codigo'];
                        $det['nombre_concepto'] = $arrConceptos[$i]['nombre'];
                        $det['formato'] = $arrConceptos[$i]['formato'];
                        $det['nombre'] = $detalles[$j]['nombre'];
                        $det['cuenta'] = $detalles[$j]['cuenta'];
                        $det['tipo'] = $detalles[$j]['tipo'];
                        array_push($arrDetalle,$det);
                    }
                }
            }
            return $arrDetalle;
        }
    }
?>
