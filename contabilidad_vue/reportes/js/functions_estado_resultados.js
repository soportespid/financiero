const URL ='contabilidad_vue/reportes/controllers/EstadoResultadosController.php';
const URLEXPORT = 'contabilidad_vue/reportes/controllers/EstadoResultadosExportController.php';
var app = new Vue({
    el:"#myapp",
    data() {
        return {
            isLoading:false,
            isModal:false,
            txtSearch:"",
            txtResultados:0,
            txtFechaInicial:new Date(new Date().getFullYear(), 0, 1).toISOString().split("T")[0],
            txtFechaFinal:new Date().toISOString().split("T")[0],
            objCuentaInicial:{codigo:"",nombre:""},
            objCuentaFinal:{codigo:"",nombre:""},
            arrCuentas:[],
            arrCentros:[],
            arrCentrosExterno:[],
            arrNiveles:[],
            arrCuentasCopy:[],
            arrData:{
                data:[]
            },
            arrExportData:[],
            txtIngresoActual:0,
            txtGastoActual:0,
            txtUtilidadActual:0,
            txtIngresoAnterior:0,
            txtGastoAnterior:0,
            txtUtilidadAnterior:0,
            selectNivel:1,
            selectCentro:-1,
            typeCuenta:1,
            checkAgregado:false,
            checkCierre:false,
            checkExterno:false,
        }
    },
    mounted() {

    },
    methods: {
        exportData:function(type=1){
            if(app.arrData.data.length == 0){
                Swal.fire("Atención!","Debe generar el informe.","warning");
                return false;
            }
            const form = document.createElement("form");
            form.method ="post";
            form.target="_blank";
            form.action=URLEXPORT;

            function addField(name,value){
                const input = document.createElement("input");
                input.type="hidden";
                input.name=name;
                input.value = value;
                form.appendChild(input);
            }
            addField("action",type == 1 ? "pdf" : "excel");
            addField("fecha_inicial",this.txtFechaInicial);
            addField("fecha_final",this.txtFechaFinal);
            addField("data",JSON.stringify(this.arrExportData));
            addField("type",app.isExport);
            document.body.appendChild(form);
            form.submit();
            document.body.removeChild(form);
        },
        generate:async function(){
            const formData = new FormData();
            formData.append("action","gen");
            formData.append("fecha_inicial",this.txtFechaInicial);
            formData.append("fecha_final",this.txtFechaFinal);
            this.isLoading = true;
            const response = await fetch(URL,{method:"POST",body:formData});
            const objData = await response.json();
            this.arrData = objData.data;
            this.arrExportData = objData;
            this.txtIngresoActual = objData.utilidad_actual.ingresos;
            this.txtGastoActual = objData.utilidad_actual.gastos;
            this.txtUtilidadActual = objData.utilidad_actual.utilidad;
            this.txtIngresoAnterior = objData.utilidad_anterior.ingresos;
            this.txtGastoAnterior = objData.utilidad_anterior.gastos;
            this.txtUtilidadAnterior = objData.utilidad_anterior.utilidad;
            this.isLoading = false;
        },
        selectItem:function({...item}){
            if(this.typeCuenta == 1){
                this.objCuentaInicial = item;
            }else{
                this.objCuentaFinal = item;
            }
            this.isModal = false;
        },
        formatNumero: function(valor){
            return new Intl.NumberFormat("en-US", {style: "currency", currency: "USD"}).format(valor);
        },
    },
    computed:{

    }
})
