const URL ='contabilidad_vue/reportes/controllers/BalanceController.php';
const URLEXPORT = 'contabilidad_vue/reportes/controllers/BalanceExportController.php';
var app = new Vue({
    el:"#myapp",
    data() {
        return {
            isLoading:false,
            isModal:false,
            txtSearch:"",
            txtResultados:0,
            txtFechaInicial:new Date(new Date().getFullYear(), 0, 1).toISOString().split("T")[0],
            txtFechaFinal:new Date().toISOString().split("T")[0],
            objCuentaInicial:{codigo:"",nombre:""},
            objCuentaFinal:{codigo:"",nombre:""},
            arrCuentas:[],
            arrCentros:[],
            arrCentrosExterno:[],
            arrNiveles:[],
            arrCuentasCopy:[],
            arrData:{
                saldo:0,
                debito:0,
                credito:0,
                saldo_final:0,
                data:[]
            },
            selectNivel:1,
            selectCentro:-1,
            typeCuenta:1,
            checkAgregado:false,
            checkCierre:false,
            checkExterno:false,
        }
    },
    mounted() {
        this.getData();
    },
    methods: {
        getData:async function(){
            const formData = new FormData();
            formData.append("action","get");
            const response = await fetch(URL,{method:"POST",body:formData});
            const objData = await response.json();
            this.arrCuentas = objData.cuentas;
            this.arrCuentasCopy = objData.cuentas;
            this.arrNiveles = objData.niveles;
            this.arrCentros = objData.centros;
            this.arrCentrosExterno = objData.centros_externo;
            this.txtResultados = objData.cuentas.length;
            this.selectNivel = this.arrNiveles[0].id_nivel;
        },
        search: function(type=""){
            let search = "";
            if(type == "modal")search = this.txtSearch.toLowerCase();
            if(type == "cod_cuenta") search = this.objCuentaInicial.codigo;
            if(type=="cod_cuenta_final")search = this.objCuentaFinal.codigo;

            if(type=="modal"){
                this.arrCuentasCopy = [...this.arrCuentas.filter(e=>e.codigo.toLowerCase().includes(search) || e.nombre.toLowerCase().includes(search))];
                this.txtResultados = this.arrCuentasCopy.length;
            }else if(type == "cod_cuenta"){
                const data = [...this.arrCuentas.filter(e=>e.codigo == search)];
                const cuenta = data.length > 0 ? JSON.parse(JSON.stringify(data[0])) : {codigo:this.objCuentaInicial.codigo,nombre:""};
                this.objCuentaInicial = cuenta;
            }else if(type == "cod_cuenta_final"){
                const data = [...this.arrCuentas.filter(e=>e.codigo == search)];
                const cuenta = data.length > 0 ? JSON.parse(JSON.stringify(data[0])) : {codigo:search,nombre:""};
                this.objCuentaFinal = cuenta;

            }
        },
        exportData:function(type=1){
            if(app.arrData.data.length == 0){
                Swal.fire("Atención!","debe generar el balance.","warning");
                return false;
            }
            const form = document.createElement("form");
            form.method ="post";
            form.target="_blank";
            form.action=URLEXPORT;

            function addField(name,value){
                const input = document.createElement("input");
                input.type="hidden";
                input.name=name;
                input.value = value;
                form.appendChild(input);
            }
            addField("action",type == 1 ? "pdf" : "excel");
            addField("fecha_inicial",this.txtFechaInicial);
            addField("fecha_final",this.txtFechaFinal);
            addField("cuenta_inicial",this.objCuentaInicial.codigo);
            addField("cuenta_final",this.objCuentaFinal.codigo);
            addField("data",JSON.stringify(this.arrData));
            addField("type",app.isExport);
            document.body.appendChild(form);
            form.submit();
            document.body.removeChild(form);
        },
        generate:async function(){
            let dataCentros ="";
            if(document.querySelector("#selectMultiple1")){
                dataCentros = document.querySelector("#selectMultiple1").getAttribute("data-values") != null ? document.querySelector("#selectMultiple1").getAttribute("data-values") :"";
            }
            const formData = new FormData();
            formData.append("action","gen");
            formData.append("fecha_inicial",this.txtFechaInicial);
            formData.append("fecha_final",this.txtFechaFinal);
            formData.append("cuenta_inicial",this.objCuentaInicial.codigo);
            formData.append("cuenta_final",this.objCuentaFinal.codigo);
            formData.append("is_agregado",this.checkAgregado? 1 : 0);
            formData.append("is_cierre",this.checkCierre ? 1 : 0);
            formData.append("is_externo",this.checkExterno ? 1 : 0);
            formData.append("nivel",JSON.stringify(this.arrNiveles.filter(function(e){return e.posiciones == app.selectNivel})[0]));
            formData.append("centro",this.selectCentro);
            formData.append("data_centros",this.checkExterno  || this.checkAgregado  ? "" : dataCentros);
            this.isLoading = true;
            const response = await fetch(URL,{method:"POST",body:formData});
            const objData = await response.json();
            this.arrData = objData;
            this.isLoading = false;
        },
        selectItem:function({...item}){
            if(this.typeCuenta == 1){
                this.objCuentaInicial = item;
            }else{
                this.objCuentaFinal = item;
            }
            this.isModal = false;
        },
        changeChecks:function(type){
            if(type==1){
                if(this.checkAgregado){
                    this.checkExterno = false;
                }
            }else if(type==2){
                if(this.checkExterno){
                    this.checkAgregado = false;
                }
            }
        },
        getAuxiliar:function(data){
            if(data.comprobante.length > 6){
                const arrFechaInicial = this.txtFechaInicial.split("-");
                const arrFechaFinal = this.txtFechaFinal.split("-");
                const strFechaInicial = arrFechaInicial[2]+"/"+arrFechaInicial[1]+"/"+arrFechaInicial[0];
                const strFechaFinal = arrFechaFinal[2]+"/"+arrFechaFinal[1]+"/"+arrFechaFinal[0];
                window.open('cont-auxiliarcuenta.php?cod='+data.comprobante+"&fec=" + strFechaInicial + "&fec1="+strFechaFinal);mypop.focus();
            }
        },
        formatNumero: function(valor){
            return new Intl.NumberFormat("en-US", {style: "currency", currency: "USD"}).format(valor);
        },
    },
    computed:{

    }
})
