<?php
    require_once '../../../Librerias/core/Helpers.php';
    require_once '../../../PHPExcel/Classes/PHPExcel.php';
    require_once '../models/BalanceModel.php';
    session_start();

    class BalanceController extends BalanceModel{
        public function getData(){
            if(!empty($_SESSION)){
                $arrData = array(
                    "niveles"=>$this->selectNiveles(),
                    "centros"=>$this->selectCentros(),
                    "cuentas"=>$this->selectCuentas(),
                    "centros_externo"=>$this->selectCentroExterno()
                );
                echo json_encode($arrData,JSON_UNESCAPED_UNICODE);
            }
            die();
        }
        public function generate(){
            if(!empty($_SESSION)){
                $strFechaInicial = strClean($_POST['fecha_inicial']);
                $strFechaFinal = strClean($_POST['fecha_final']);
                $strCuentaInicial = !empty($_POST['cuenta_inicial']) ? strClean($_POST['cuenta_inicial']) : "1";
                $strCuentaFinal = !empty($_POST['cuenta_final']) ? strClean($_POST['cuenta_final']) : "99999999999";
                $intAgregado = intval($_POST['is_agregado']);
                $intCierre = intval($_POST['is_cierre']);
                $intExterno = intval($_POST['is_externo']);
                $arrNivel = json_decode($_POST['nivel'],true);
                $intNivel = $arrNivel['id_nivel'];
                $strCentro = strClean($_POST['centro']);
                $arrCentros = $_POST['data_centros'] != "" ?explode(",",$_POST['data_centros']) : [];
                $arrCuentas = $this->selectBuscarCuentas($arrNivel['posiciones'],$strCuentaInicial,$strCuentaFinal);
                $arrNiveles = $this->selectNiveles();
                $arrData = $this->selectMovimientosPeriodos(
                    $arrNivel['posiciones'],
                    $strFechaInicial,
                    $strFechaFinal,
                    $intAgregado,
                    $intCierre,
                    $intExterno,
                    $strCentro,
                    $arrCentros,
                    $arrCuentas,
                );
                $arrData = $this->setNiveles($arrData,$arrNiveles,$intNivel);
                $arrResponse = $this->orderData($arrData);
                $arrResponse['nivel'] = $arrNivel['posiciones'];
                echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
            }
            die();
        }
        public function setNiveles($arrData,$arrNiveles,$intNivel){
            $totalData = count($arrData);
            $arrNewData = [];
            for ($i=0; $i < $intNivel ; $i++) {
                for ($j=0; $j < $totalData; $j++) {
                    $data = $arrData[$j];
                    if(strlen($data['comprobante'])>=$arrNiveles[$i-1]['posiciones']){
                        $strComprobante = substr($data['comprobante'],0,$arrNiveles[$i-1]['posiciones']);
                        if($strComprobante!=""){
                            $totalDataTemp = count($arrNewData);
                            $flag = false;
                            for ($k=0; $k < $totalDataTemp; $k++) {
                                if($strComprobante == $arrNewData[$k]['comprobante']){
                                    $arrNewData[$k]['saldo']+=$data['saldo'];
                                    $arrNewData[$k]['debito']+=$data['debito'];
                                    $arrNewData[$k]['credito']+=$data['credito'];
                                    $arrNewData[$k]['saldo_final'] = $arrNewData[$k]['saldo']+$arrNewData[$k]['debito']-$arrNewData[$k]['credito'];
                                    $flag = true;
                                    break;
                                }
                            }
                            if(!$flag){
                                $data['comprobante'] = $strComprobante;
                                $data['saldo_final'] = $data['saldo']+$data['debito']-$data['credito'];
                                array_push($arrNewData,$data);
                            }
                        }else{
                            $data['saldo_final'] = $data['saldo']+$data['debito']-$data['credito'];
                            array_push($arrNewData,$data);
                        }
                    }
                }
            }
            return $arrNewData;
        }

        public function orderData($data){
            $arrNombresCuentas = getNombresCuentaNicsp();
            $arrComp = array_column($data,"comprobante");
            $arrData = [];
            usort($arrComp, 'strcmp');
            $arrComp = array_values(array_unique($arrComp));
            $totalComp = count($arrComp);
            $totalData = count($data);
            $totalCredito = 0;
            $totalDebito = 0;
            $totalSaldo = 0;
            $totalSaldoFinal = 0;
            for ($i=0; $i < $totalComp ; $i++) {
                for ($j=0; $j < $totalData; $j++) {
                    $arrInfo = $data[$j];
                    if($arrComp[$i] == $data[$j]['comprobante']){
                        $comprobante = $arrComp[$i];
                        $arrNombre = array_values(array_filter($arrNombresCuentas,function($e)use($comprobante){return $comprobante==$e['cuenta'];}))[0];
                        if(!empty($arrNombre)){$arrInfo['nombre'] = strtoupper($arrNombre['nombre']);}
                        array_push($arrData,$arrInfo);
                        break;
                    }
                }
            }
            $arrDataPrincipales = array_filter($arrData,function($e){return strlen($e['comprobante'])==1;});
            foreach ($arrDataPrincipales as $data) {
                $totalSaldo += round($data['saldo'],2);
                $totalCredito += round($data['credito'],2);
                $totalDebito += round($data['debito'],2);
            }
            $totalSaldoFinal = round($totalSaldo+$totalDebito-$totalCredito,2);
            $totalSaldo = round($totalSaldo,2);
            $totalCredito = round($totalCredito,2);
            $totalDebito = round($totalDebito,2);
            return array("data"=>$arrData,"saldo"=>$totalSaldo,"debito"=>$totalDebito,"credito"=>$totalCredito,"saldo_final"=>$totalSaldoFinal);
        }
    }
    if($_POST){
        $obj = new BalanceController();
        if($_POST['action']=="get"){
            $obj->getData();
        }else if($_POST['action']=="gen"){
            $obj->generate();
        }
    }

?>
