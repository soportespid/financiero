<?php
    require_once '../../../Librerias/core/Helpers.php';
    require_once '../../../PHPExcel/Classes/PHPExcel.php';
    require_once '../../../tcpdf/tcpdf_include.php';
    require_once '../../../tcpdf/tcpdf.php';
    session_start();
    /*ini_set('display_errors', '1');
    ini_set('display_startup_errors', '1');
    error_reporting(E_ALL);*/
    class EstadoResultadosExportController {
        public function exportExcel(){
            if(!empty($_SESSION)){
                $arrData = json_decode($_POST['data'],true);
                if(!empty($arrData)){
                    $request = configBasica();
                    $strNit = $request['nit'];
                    $strRazon = $request['razonsocial'];
                    $fileName = 'estado_resultados_'.strtolower($strRazon)."_".$strNit.'';
                    $arrData = json_decode($_POST['data'],true);
                    $arrFechaInicial = explode("-",$_POST['fecha_inicial']);
                    $arrFechaFinal = explode("-",$_POST['fecha_final']);
                    $strFechaInicial = $arrFechaInicial[2]."/".$arrFechaInicial[1]."/".$arrFechaInicial[0];
                    $strFechaFinal = $arrFechaFinal[2]."/".$arrFechaFinal[1]."/".$arrFechaFinal[0];
                    $arrDet = $arrData['data']['data'];

                    $objPHPExcel = new PHPExcel();
                    $objPHPExcel->getActiveSheet()->getStyle('A:D')->applyFromArray(
                        array(
                            'alignment' => array(
                                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
                                'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER
                            )
                        )
                    );
                    $objPHPExcel->getProperties()
                    ->setCreator("IDEAL 10")
                    ->setLastModifiedBy("IDEAL 10")
                    ->setTitle("Exportar Excel con PHP")
                    ->setSubject("Documento de prueba")
                    ->setDescription("Documento generado con PHPExcel")
                    ->setKeywords("usuarios phpexcel")
                    ->setCategory("reportes");

                    //----Cuerpo de Documento----
                    $objPHPExcel->setActiveSheetIndex(0)
                    ->mergeCells('A1:D1')
                    ->mergeCells('A2:D2')
                    ->mergeCells('B3:D3')
                    ->setCellValue('A1', $strRazon.' - CONTABILIDAD')
                    ->setCellValue('A2', 'ESTADO DE RESULTADOS')
                    ->setCellValue('A3', 'Fecha')
                    ->setCellValue('A4', 'Código')
                    ->setCellValue('B4', 'Nombre')
                    ->setCellValue('C4', 'Periodo actual')
                    ->setCellValue('D4', 'Periodo anterior')
                    ->setCellValue('B3', "Desde ".$strFechaInicial." Hasta ".$strFechaFinal);
                    $objPHPExcel-> getActiveSheet ()
                    -> getStyle ("A1")
                    -> getFill ()
                    -> setFillType (PHPExcel_Style_Fill :: FILL_SOLID)
                    -> getStartColor ()
                    -> setRGB ('3399cc');
                    $objPHPExcel-> getActiveSheet ()
                    -> getStyle ("A3")
                    -> getFill ()
                    -> setFillType (PHPExcel_Style_Fill :: FILL_SOLID)
                    -> getStartColor ()
                    -> setRGB ('3399cc');
                    $objPHPExcel-> getActiveSheet ()
                    -> getStyle ("A4:D4")
                    -> getFill ()
                    -> setFillType (PHPExcel_Style_Fill :: FILL_SOLID)
                    -> getStartColor ()
                    -> setRGB ('3399cc');
                    $objPHPExcel-> getActiveSheet ()
                    -> getStyle ('A1:A2')
                    -> getAlignment ()
                    -> applyFromArray (array ( 'horizontal'  =>  PHPExcel_Style_Alignment :: HORIZONTAL_CENTER ,) );
                    $objPHPExcel-> getActiveSheet ()
                    -> getStyle ('A4:D4')
                    -> getAlignment ()
                    -> applyFromArray (array ( 'horizontal'  =>  PHPExcel_Style_Alignment :: HORIZONTAL_CENTER ,) );

                    $borders = array(
                        'borders' => array(
                            'allborders' => array(
                                'style' => PHPExcel_Style_Border::BORDER_THIN,
                            'color' => array('argb' => 'FF000000'),
                            )
                        ),
                    );

                    $objPHPExcel->getActiveSheet()->getStyle("A1")->getFont()->setBold(true);
                    $objPHPExcel->getActiveSheet()->getStyle("A3:D3")->getFont()->setBold(true);
                    $objPHPExcel->getActiveSheet()->getStyle('A1:D1')->applyFromArray($borders);
                    $objPHPExcel->getActiveSheet()->getStyle('A2:D2')->applyFromArray($borders);
                    $objPHPExcel->getActiveSheet()->getStyle('A3:D3')->applyFromArray($borders);

                    $objPHPExcel->getActiveSheet()->getStyle("A1")->getFont()->getColor()->setRGB("ffffff");
                    $objPHPExcel->getActiveSheet()->getStyle("A3")->getFont()->getColor()->setRGB("ffffff");
                    $objPHPExcel->getActiveSheet()->getStyle("A4:D4")->getFont()->getColor()->setRGB("ffffff");

                    $row = 5;
                    foreach ($arrDet as $data) {
                        if(strlen($data['comprobante']) == 1){
                            $objPHPExcel-> getActiveSheet ()
                                -> getStyle ("A$row:D$row")
                                -> getFill ()
                                -> setFillType (PHPExcel_Style_Fill :: FILL_SOLID)
                                -> getStartColor ()
                                -> setRGB ('99ddff');
                        }
                        $objPHPExcel->setActiveSheetIndex(0)
                        ->setCellValueExplicit ("A$row", $data['comprobante'] )
                        ->setCellValueExplicit ("B$row", $data['nombre'] )
                        ->setCellValueExplicit ("C$row", formatNum($data['saldo_final']) )
                        ->setCellValueExplicit ("D$row", !empty($data['anterior'])? formatNum($data['anterior']['saldo_final']) : formatNum(0));
                        $objPHPExcel->getActiveSheet()->getStyle("A$row:D$row")->applyFromArray($borders);
                        $row++;
                    }
                    $objPHPExcel-> getActiveSheet ()
                                -> getStyle ("A$row:D$row")
                                -> getFill ()
                                -> setFillType (PHPExcel_Style_Fill :: FILL_SOLID)
                                -> getStartColor ()
                                -> setRGB ('99ddff');
                    $objPHPExcel->getActiveSheet()->getStyle("A$row:D$row")->getFont()->setBold(true);
                    $objPHPExcel->setActiveSheetIndex(0)
                    ->mergeCells("A$row:B$row")
                    ->setCellValue("A$row", 'UTILIDAD')
                    ->setCellValue("C$row", formatNum($arrData['utilidad_actual']['utilidad']))
                    ->setCellValue("D$row", formatNum($arrData['utilidad_anterior']['utilidad']));
                    $objPHPExcel->getActiveSheet()->getStyle("A$row:D$row")->applyFromArray($borders);

                    $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
                    $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(50);
                    $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
                    $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);

                    //----Guardar documento----
                    header('Content-Type: application/vnd.ms-excel');
                    header('Content-Disposition: attachment;filename="'.$fileName.'.xlsx"');
                    header('Cache-Control: max-age=0');
                    $objWriter=PHPExcel_IOFactory::createWriter($objPHPExcel,'Excel2007');
                    $objWriter->save('php://output');
                }
            }
            die();
        }
        public function exportPdf(){
            if(!empty($_SESSION)){
                $request = configBasica();
                $strNit = $request['nit'];
                $strRazon = $request['razonsocial'];
                $fileName = 'estado_resultados_'.strtolower($strRazon)."_".$strNit.'';
                $arrData = json_decode($_POST['data'],true);
                $arrFechaInicial = explode("-",$_POST['fecha_inicial']);
                $arrFechaFinal = explode("-",$_POST['fecha_final']);
                $strFechaInicial = $arrFechaInicial[2]."/".$arrFechaInicial[1]."/".$arrFechaInicial[0];
                $strFechaFinal = $arrFechaFinal[2]."/".$arrFechaFinal[1]."/".$arrFechaFinal[0];
                $arrDet = $arrData['data']['data'];


                $pdf = new MYPDF('P','mm','Letter', true, 'iso-8859-1', false);
                $pdf->SetDocInfoUnicode (true);
                // set document information
                $pdf->SetCreator(PDF_CREATOR);
                $pdf->SetAuthor('IDEALSAS');
                $pdf->SetTitle('ESTADO DE RESULTADOS');
                $pdf->SetSubject('ESTADO DE RESULTADOS');
                $pdf->SetKeywords('TCPDF, PDF, example, test, guide');
                $pdf->SetMargins(10, 38, 10);// set margins
                $pdf->SetHeaderMargin(38);// set margins
                $pdf->SetFooterMargin(17);// set margins
                $pdf->SetAutoPageBreak(TRUE, 20);// set auto page breaks
                // set some language-dependent strings (optional)
                if (@file_exists(dirname(__FILE__).'/lang/spa.php'))
                {
                    require_once(dirname(__FILE__).'/lang/spa.php');
                    $pdf->setLanguageArray($l);
                }
                $pdf->AddPage();
                $pdf->SetFillColor(51, 153, 204);
                $pdf->SetTextColor(255,255,255);
                $pdf->SetFont('Helvetica','b',9);
                $pdf->MultiCell(35,4,"Fecha","LRBT",'P',true,0,'','',true,0,false,true,0,'M',true);
                $pdf->SetFillColor(255,255,255);
                $pdf->SetTextColor(0,0,0);
                $pdf->SetFont('Helvetica','',9);
                $pdf->MultiCell(155,4,"Desde ".$strFechaInicial." Hasta ".$strFechaFinal,"RBTL",'L',true,0,'','',true,0,false,true,0,'M',true);
                $pdf->ln();
                $pdf->ln();
                $pdf->SetFillColor(51, 153, 204);
                $pdf->SetTextColor(255,255,255);
                $pdf->SetFont('helvetica','B',7);

                $pdf->MultiCell(47.5,6,"Codigo","LRBT",'C',true,0,'','',true,0,false,true,0,'M',true);
                $pdf->MultiCell(47.5,6,"Cuenta","LRBT",'C',true,0,'','',true,0,false,true,0,'M',true);
                $pdf->MultiCell(47.5,6,"Periodo actual","LRBT",'C',true,0,'','',true,0,false,true,0,'M',true);
                $pdf->MultiCell(47.5,6,"Periodo anterior","LRBT",'C',true,0,'','',true,0,false,true,0,'M',true);
                $pdf->ln();
                $pdf->SetTextColor(0,0,0);
                $fill = true;
                $height = 5;
                foreach ($arrDet as $data) {
                    $pdf->SetFillColor(245,245,245);
                    $pdf->SetFont('helvetica','',7);
                    if(strlen($data['comprobante']) == 1){
                        $pdf->SetFont('helvetica','B',7);
                        $pdf->SetFillColor(153,221,255);
                        $fill = true;
                    }
                    $pdf->MultiCell(47.5,$height,$data['comprobante'],"",'L',$fill,0,'','',true,0,false,true,0,'M',true);
                    $pdf->MultiCell(47.5,$height,$data['nombre'],"",'L',$fill,0,'','',true,0,false,true,0,'M',true);
                    $pdf->MultiCell(47.5,$height,formatNum($data['saldo_final']),"",'R',$fill,0,'','',true,0,false,true,0,'M',true);
                    $pdf->MultiCell(47.5,$height,!empty($data['anterior'])? formatNum($data['anterior']['saldo_final']) : formatNum(0),"",'R',$fill,0,'','',true,0,false,true,0,'M',true);
                    $pdf->ln();
                    $fill = !$fill;
                }
                $pdf->SetFont('helvetica','B',7);
                $pdf->SetFillColor(153,221,255);
                $pdf->MultiCell(95,$height,"Utilidad","",'R',$fill,0,'','',true,0,false,true,0,'M',true);
                $pdf->MultiCell(47.5,$height,formatNum($arrData['utilidad_actual']['utilidad']),"",'R',$fill,0,'','',true,0,false,true,0,'M',true);
                $pdf->MultiCell(47.5,$height,formatNum($arrData['utilidad_anterior']['utilidad']),"",'R',$fill,0,'','',true,0,false,true,0,'M',true);

                $pdf->ln();
                $pdf->Output($fileName.'.pdf', 'I');
            }
            die();
        }

    }
    class MYPDF extends TCPDF {

		public function Header()
		{
			$request = configBasica();
            $strNit = $request['nit'];
            $strRazon = $request['razonsocial'];

			//Parte Izquierda
			$this->Image('../../../imagenes/escudo.jpg',  13, 13, 20, 20, 'JPG', '', 'T', true, 300, '', false, false, 0, false, false, false);// Logo
			$this->SetFont('helvetica','B',8);
			$this->SetY(10);
			$this->RoundedRect(10, 10, 190, 25, 1,'');
			$this->Cell(0.1);
			$this->Cell(26,25,'','R',0,'L');
			$this->SetY(8);
			$this->SetX(40);
			$this->SetFont('helvetica','B',9);
			$this->Cell(140,15,strtoupper("$strRazon"),0,0,'C');
			$this->SetFont('helvetica','B',7);
			$this->SetY(12);
			$this->SetX(40);
			$this->Cell(140,15,'NIT: '.$strNit,0,0,'C');
			//*****************************************************************************************************************************
			$this->SetFont('helvetica','B',9);
			$this->SetY(23);
			$this->SetX(36);
            $this->Cell(164,12,"ESTADO DE RESULTADOS",'T',0,'C');


            $this->SetFont('helvetica','B',7);
			$this->SetY(10);
			$this->SetX(167);
			$this->Cell(30,7," FECHA: ". date("d/m/Y"),"L",0,'L');
			$this->SetY(17);
			$this->SetX(167);
            $this->Cell(35,6,"","L",0,'L');

			//**********************************************************
			$this->SetFont('times','B',10);
			$this->ln(12);
		}
		public function Footer(){

			$request = configBasica();
            $strDireccion = $request['direccion'] != "" ? "Dirección: ".strtoupper($request['direccion']) : "";
            $strWeb = $request['web'] != "" ? "Pagina web: ".strtoupper($request['web']) :"";
            $strEmail = $request['email'] !="" ? "Email: ".strtoupper($request['email']) :"";
            $strTelefono = $request['telefono'] != "" ? "Telefonos: ".$request['telefono'] : "";
            $strUsuario = searchUser($_SESSION['cedulausu'])['nom_usu'];
			$strNick = $_SESSION['nickusu'];
			$strFecha = date("d/m/Y H:i:s");
			$strIp = $_SERVER['REMOTE_ADDR'];

			$this->SetFont('helvetica', 'I', 8);
			$txt = <<<EOD
			$strDireccion $strTelefono
			$strEmail $strWeb
			EOD;
			$this->SetFont('helvetica', 'I', 6);
			$this->Cell(277,10,'','T',0,'T');
			$this->ln(2);
			$this->Write(0, $txt , '', 0, 'C', true, 0, false, false, 0);

			$this->Cell(50, 10, 'Hecho por: '.$strUsuario, 00, false, 'C', 0, '', 0, false, 'T', 'M');
			$this->Cell(30, 10, 'Impreso por: '.$strNick, 0, false, 'C', 0, '', 0, false, 'T', 'M');
			$this->Cell(30, 10, 'IP: '.$strIp, 0, false, 'C',0, '', 0, false, 'T', 'M');
			$this->Cell(30, 10, 'Fecha: '.$strFecha, 0, false, 'C', 0, '', 0, false, 'T', 'M');
			$this->Cell(30, 10, 'IDEAL.10 S.A.S', 0, false, 'C', 0, '', 0, false, 'T', 'M');
            $this->Cell(20, 10, 'Pagina '.$this->getAliasNumPage().'/'.$this->getAliasNbPages(), 0, false, 'C', 0, '', 0, false, 'T', 'M');
		}
	}

    if($_POST){
        $obj = new EstadoResultadosExportController();
        if($_POST['action'] == "excel"){
            $obj->exportExcel();
        }else if($_POST['action'] == "pdf"){
            $obj->exportPdf();
        }
    }

?>
