<?php
    require_once '../../../Librerias/core/Helpers.php';
    require_once '../../../PHPExcel/Classes/PHPExcel.php';
    require_once '../../../tcpdf/tcpdf_include.php';
    require_once '../../../tcpdf/tcpdf.php';
    session_start();
    /*ini_set('display_errors', '1');
    ini_set('display_startup_errors', '1');
    error_reporting(E_ALL);*/
    class EstadoFinancieroExportController {
        public function exportExcel(){
            if(!empty($_SESSION)){
                $arrData = json_decode($_POST['data'],true);
                if(!empty($arrData)){
                    $request = configBasica();
                    $strNit = $request['nit'];
                    $strRazon = $request['razonsocial'];
                    $fileName = 'estado_financiero_'.strtolower($strRazon)."_".$strNit.'';
                    $arrData = json_decode($_POST['data'],true);
                    $arrFechaInicial = explode("-",$_POST['fecha_inicial']);
                    $arrFechaFinal = explode("-",$_POST['fecha_final']);
                    $strFechaInicial = $arrFechaInicial[2]."/".$arrFechaInicial[1]."/".$arrFechaInicial[0];
                    $strFechaFinal = $arrFechaFinal[2]."/".$arrFechaFinal[1]."/".$arrFechaFinal[0];
                    $intActivo = $arrData['activo'];
                    $intActivoAnterior = $arrData['activo_anterior'];
                    $arrDet = $arrData['data']['data'];

                    $objPHPExcel = new PHPExcel();
                    $objPHPExcel->getActiveSheet()->getStyle('A:D')->applyFromArray(
                        array(
                            'alignment' => array(
                                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
                                'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER
                            )
                        )
                    );
                    $objPHPExcel->getProperties()
                    ->setCreator("IDEAL 10")
                    ->setLastModifiedBy("IDEAL 10")
                    ->setTitle("Exportar Excel con PHP")
                    ->setSubject("Documento de prueba")
                    ->setDescription("Documento generado con PHPExcel")
                    ->setKeywords("usuarios phpexcel")
                    ->setCategory("reportes");

                    //----Cuerpo de Documento----
                    $objPHPExcel->setActiveSheetIndex(0)
                    ->mergeCells('A1:D1')
                    ->mergeCells('A2:D2')
                    ->mergeCells('B3:D3')
                    ->setCellValue('A1', $strRazon.' - CONTABILIDAD')
                    ->setCellValue('A2', 'ESTADO DE SITUACIÓN FINANCIERA')
                    ->setCellValue('A3', 'Fecha')
                    ->setCellValue('A4', 'Código')
                    ->setCellValue('B4', 'Nombre')
                    ->setCellValue('C4', 'Periodo actual')
                    ->setCellValue('D4', 'Periodo anterior')
                    ->setCellValue('B3', "Desde ".$strFechaInicial." Hasta ".$strFechaFinal);
                    $objPHPExcel-> getActiveSheet ()
                    -> getStyle ("A1")
                    -> getFill ()
                    -> setFillType (PHPExcel_Style_Fill :: FILL_SOLID)
                    -> getStartColor ()
                    -> setRGB ('3399cc');
                    $objPHPExcel-> getActiveSheet ()
                    -> getStyle ("A3")
                    -> getFill ()
                    -> setFillType (PHPExcel_Style_Fill :: FILL_SOLID)
                    -> getStartColor ()
                    -> setRGB ('3399cc');
                    $objPHPExcel-> getActiveSheet ()
                    -> getStyle ("A4:D4")
                    -> getFill ()
                    -> setFillType (PHPExcel_Style_Fill :: FILL_SOLID)
                    -> getStartColor ()
                    -> setRGB ('3399cc');
                    $objPHPExcel-> getActiveSheet ()
                    -> getStyle ('A1:A2')
                    -> getAlignment ()
                    -> applyFromArray (array ( 'horizontal'  =>  PHPExcel_Style_Alignment :: HORIZONTAL_CENTER ,) );
                    $objPHPExcel-> getActiveSheet ()
                    -> getStyle ('A4:D4')
                    -> getAlignment ()
                    -> applyFromArray (array ( 'horizontal'  =>  PHPExcel_Style_Alignment :: HORIZONTAL_CENTER ,) );

                    $borders = array(
                        'borders' => array(
                            'allborders' => array(
                                'style' => PHPExcel_Style_Border::BORDER_THIN,
                            'color' => array('argb' => 'FF000000'),
                            )
                        ),
                    );

                    $objPHPExcel->getActiveSheet()->getStyle("A1")->getFont()->setBold(true);
                    $objPHPExcel->getActiveSheet()->getStyle("A3:D3")->getFont()->setBold(true);
                    $objPHPExcel->getActiveSheet()->getStyle('A1:D1')->applyFromArray($borders);
                    $objPHPExcel->getActiveSheet()->getStyle('A2:D2')->applyFromArray($borders);
                    $objPHPExcel->getActiveSheet()->getStyle('A3:D3')->applyFromArray($borders);

                    $objPHPExcel->getActiveSheet()->getStyle("A1")->getFont()->getColor()->setRGB("ffffff");
                    $objPHPExcel->getActiveSheet()->getStyle("A3")->getFont()->getColor()->setRGB("ffffff");
                    $objPHPExcel->getActiveSheet()->getStyle("A4:D4")->getFont()->getColor()->setRGB("ffffff");

                    $totalActivoCorriente = 0;
                    $totalActivoNoCorriente = 0;
                    $totalPasivoCorriente = 0;
                    $totalPasivoNoCorriente = 0;
                    $totalActivoAnteriorCorriente = 0;
                    $totalActivoAnteriorNoCorriente = 0;
                    $totalPasivoAnteriorCorriente = 0;
                    $totalPasivoAnteriorNoCorriente = 0;

                    $arrActivosCorrientes = array_filter($arrDet,function($e){return strlen($e['comprobante']) == 2 &&
                        substr($e['comprobante'],0,1) == 1 && $e['tipo'] == 0;});
                    $arrActivosNoCorrientes = array_filter($arrDet,function($e){return strlen($e['comprobante']) == 2 &&
                        substr($e['comprobante'],0,1) == 1 && $e['tipo'] == 1;});

                    $arrPasivosCorrientes = array_filter($arrDet,function($e){return strlen($e['comprobante']) == 2 &&
                        substr($e['comprobante'],0,1) == 2 && $e['tipo'] == 0;});
                    $arrPasivosNoCorrientes = array_filter($arrDet,function($e){return strlen($e['comprobante']) == 2 &&
                        substr($e['comprobante'],0,1) == 2 && $e['tipo'] == 1;});
                    $arrMayoresTotales = array_values(array_filter($arrDet,function($e){return strlen($e['comprobante']) == 1;}));

                    foreach ($arrDet as $e) {
                        if(strlen($e['comprobante']) == 2 && substr($e['comprobante'],0,1) == 1 && $e['tipo'] == 0){
                            $totalActivoCorriente+= $e['saldo_final'];
                            $totalActivoAnteriorCorriente+= $e['anterior']['saldo_final'];
                        }else if(strlen($e['comprobante']) == 2 && substr($e['comprobante'],0,1) == 1 && $e['tipo'] == 1){
                            $totalActivoNoCorriente+= $e['saldo_final'];
                            $totalActivoAnteriorNoCorriente+= $e['anterior']['saldo_final'];
                        }else if(strlen($e['comprobante']) == 2 && substr($e['comprobante'],0,1) == 2 && $e['tipo'] == 0){
                            $totalPasivoCorriente+= $e['saldo_final'];
                            $totalPasivoAnteriorCorriente+= $e['anterior']['saldo_final'];
                        }else if(strlen($e['comprobante']) == 2 && substr($e['comprobante'],0,1) == 2 && $e['tipo'] == 1){
                            $totalPasivoNoCorriente+= $e['saldo_final'];
                            $totalPasivoAnteriorNoCorriente+= $e['anterior']['saldo_final'];
                        }

                    }
                    $row = 5;
                    $objPHPExcel-> getActiveSheet ()
                    -> getStyle ("A$row:D$row")
                    -> getFill ()
                    -> setFillType (PHPExcel_Style_Fill :: FILL_SOLID)
                    -> getStartColor ()
                    -> setRGB ('99ddff');

                    //Activos
                    $objPHPExcel->setActiveSheetIndex(0)
                    ->setCellValueExplicit ("A$row", "1" )
                    ->setCellValueExplicit ("B$row", "Activos" )
                    ->setCellValueExplicit ("C$row", formatNum($arrMayoresTotales[0]['saldo_final']) )
                    ->setCellValueExplicit ("D$row", !empty($arrMayoresTotales[0]['anterior'])? formatNum($arrMayoresTotales[0]['anterior']['saldo_final']) : formatNum(0));
                    $objPHPExcel->getActiveSheet()->getStyle("A$row:D$row")->applyFromArray($borders);
                    $objPHPExcel->getActiveSheet()->getStyle("A$row:D$row")->getFont()->setBold(true);
                    $row++;
                    //Corriente
                    $objPHPExcel->setActiveSheetIndex(0)
                    ->setCellValueExplicit ("A$row", "" )
                    ->setCellValueExplicit ("B$row", "Corriente" )
                    ->setCellValueExplicit ("C$row", formatNum($totalActivoCorriente) )
                    ->setCellValueExplicit ("D$row", formatNum($totalActivoAnteriorCorriente));
                    $objPHPExcel->getActiveSheet()->getStyle("A$row:D$row")->applyFromArray($borders);
                    $objPHPExcel->getActiveSheet()->getStyle("A$row:D$row")->getFont()->setBold(true);
                    $row++;
                    foreach ($arrActivosCorrientes as $data) {
                        $objPHPExcel->setActiveSheetIndex(0)
                            ->setCellValueExplicit ("A$row", $data['comprobante'] )
                            ->setCellValueExplicit ("B$row", $data['nombre'] )
                            ->setCellValueExplicit ("C$row", formatNum($data['saldo_final']) )
                            ->setCellValueExplicit ("D$row", !empty($data['anterior'])? formatNum($data['anterior']['saldo_final']) : formatNum(0));
                            $objPHPExcel->getActiveSheet()->getStyle("A$row:D$row")->applyFromArray($borders);
                            $row++;
                    }
                    //No Corriente
                    $objPHPExcel->setActiveSheetIndex(0)
                    ->setCellValueExplicit ("A$row", "" )
                    ->setCellValueExplicit ("B$row", "No corriente" )
                    ->setCellValueExplicit ("C$row", formatNum($totalActivoNoCorriente) )
                    ->setCellValueExplicit ("D$row", formatNum($totalActivoAnteriorNoCorriente));
                    $objPHPExcel->getActiveSheet()->getStyle("A$row:D$row")->applyFromArray($borders);
                    $objPHPExcel->getActiveSheet()->getStyle("A$row:D$row")->getFont()->setBold(true);
                    $row++;

                    foreach ($arrActivosNoCorrientes as $data) {
                        $objPHPExcel->setActiveSheetIndex(0)
                            ->setCellValueExplicit ("A$row", $data['comprobante'] )
                            ->setCellValueExplicit ("B$row", $data['nombre'] )
                            ->setCellValueExplicit ("C$row", formatNum($data['saldo_final']) )
                            ->setCellValueExplicit ("D$row", !empty($data['anterior'])? formatNum($data['anterior']['saldo_final']) : formatNum(0));
                            $objPHPExcel->getActiveSheet()->getStyle("A$row:D$row")->applyFromArray($borders);
                            $row++;
                    }

                    $objPHPExcel-> getActiveSheet ()
                    -> getStyle ("A$row:D$row")
                    -> getFill ()
                    -> setFillType (PHPExcel_Style_Fill :: FILL_SOLID)
                    -> getStartColor ()
                    -> setRGB ('99ddff');
                    //Pasivos
                    $objPHPExcel->setActiveSheetIndex(0)
                    ->setCellValueExplicit ("A$row", "2" )
                    ->setCellValueExplicit ("B$row", "Pasivos" )
                    ->setCellValueExplicit ("C$row", formatNum(abs($arrMayoresTotales[1]['saldo_final'])) )
                    ->setCellValueExplicit ("D$row", !empty($arrMayoresTotales[1]['anterior'])? formatNum(abs($arrMayoresTotales[0]['anterior']['saldo_final'])) : formatNum(0));
                    $objPHPExcel->getActiveSheet()->getStyle("A$row:D$row")->applyFromArray($borders);
                    $objPHPExcel->getActiveSheet()->getStyle("A$row:D$row")->getFont()->setBold(true);
                    $row++;

                    //Corriente
                    $objPHPExcel->setActiveSheetIndex(0)
                    ->setCellValueExplicit ("A$row", "" )
                    ->setCellValueExplicit ("B$row", "Corriente" )
                    ->setCellValueExplicit ("C$row", formatNum(abs($totalPasivoCorriente)) )
                    ->setCellValueExplicit ("D$row", formatNum(abs($totalPasivoAnteriorCorriente)));
                    $objPHPExcel->getActiveSheet()->getStyle("A$row:D$row")->applyFromArray($borders);
                    $objPHPExcel->getActiveSheet()->getStyle("A$row:D$row")->getFont()->setBold(true);
                    $row++;

                    foreach ($arrPasivosCorrientes as $data) {
                        $intSaldoFinalActual = $data['saldo_final'] < 0 ? -1*$data['saldo_final'] : $data['saldo_final'];
                        $intSaldoFinalAnterior = !empty($data['anterior']) ? $data['anterior']['saldo_final'] : 0;
                        $intSaldoFinalAnterior = $intSaldoFinalAnterior < 0 ? -1*$intSaldoFinalAnterior  : $intSaldoFinalAnterior;
                        $objPHPExcel->setActiveSheetIndex(0)
                            ->setCellValueExplicit ("A$row", $data['comprobante'] )
                            ->setCellValueExplicit ("B$row", $data['nombre'] )
                            ->setCellValueExplicit ("C$row", formatNum($intSaldoFinalActual) )
                            ->setCellValueExplicit ("D$row", formatNum($intSaldoFinalAnterior));
                            $objPHPExcel->getActiveSheet()->getStyle("A$row:D$row")->applyFromArray($borders);
                            $row++;
                    }
                    //No Corriente
                    $objPHPExcel->setActiveSheetIndex(0)
                    ->setCellValueExplicit ("A$row", "" )
                    ->setCellValueExplicit ("B$row", "No corriente" )
                    ->setCellValueExplicit ("C$row", formatNum(abs($totalPasivoNoCorriente)) )
                    ->setCellValueExplicit ("D$row", formatNum(abs($totalPasivoAnteriorNoCorriente)));
                    $objPHPExcel->getActiveSheet()->getStyle("A$row:D$row")->applyFromArray($borders);
                    $objPHPExcel->getActiveSheet()->getStyle("A$row:D$row")->getFont()->setBold(true);
                    $row++;

                    foreach ($arrPasivosNoCorrientes as $data) {
                        $intSaldoFinalActual = $data['saldo_final'] < 0 ? -1*$data['saldo_final'] : $data['saldo_final'];
                        $intSaldoFinalAnterior = !empty($data['anterior']) ? $data['anterior']['saldo_final'] : 0;
                        $intSaldoFinalAnterior = $intSaldoFinalAnterior < 0 ? -1*$intSaldoFinalAnterior  : $intSaldoFinalAnterior;
                        $objPHPExcel->setActiveSheetIndex(0)
                            ->setCellValueExplicit ("A$row", $data['comprobante'] )
                            ->setCellValueExplicit ("B$row", $data['nombre'] )
                            ->setCellValueExplicit ("C$row", formatNum($intSaldoFinalActual) )
                            ->setCellValueExplicit ("D$row", formatNum($intSaldoFinalAnterior));
                            $objPHPExcel->getActiveSheet()->getStyle("A$row:D$row")->applyFromArray($borders);
                            $row++;
                    }
                    //Patrimonio
                    foreach ($arrDet as $data) {
                        if(substr($data['comprobante'],0,1) == 3){
                            if(strlen($data['comprobante']) == 1){
                                $objPHPExcel-> getActiveSheet ()
                                    -> getStyle ("A$row:D$row")
                                    -> getFill ()
                                    -> setFillType (PHPExcel_Style_Fill :: FILL_SOLID)
                                    -> getStartColor ()
                                    -> setRGB ('99ddff');
                            }
                            $objPHPExcel->setActiveSheetIndex(0)
                            ->setCellValueExplicit ("A$row", $data['comprobante'] )
                            ->setCellValueExplicit ("B$row", $data['nombre'] )
                            ->setCellValueExplicit ("C$row", formatNum(abs($data['saldo_final'])) )
                            ->setCellValueExplicit ("D$row", !empty($data['anterior'])? formatNum(abs($data['anterior']['saldo_final'])) : formatNum(0));
                            $objPHPExcel->getActiveSheet()->getStyle("A$row:D$row")->applyFromArray($borders);
                            $row++;
                        }
                    }

                    $utilidad = -1*$arrData['utilidad'];
                    $utilidadAnterior = -1*$arrData['utilidad_anterior'];

                    $objPHPExcel->setActiveSheetIndex(0)
                    ->setCellValueExplicit ("A$row", "31" )
                    ->setCellValueExplicit ("B$row", "RESULTADO DEL EJERCICIO" )
                    ->setCellValueExplicit ("C$row", formatNum($utilidad) )
                    ->setCellValueExplicit ("D$row", formatNum($utilidadAnterior));
                    $objPHPExcel->getActiveSheet()->getStyle("A$row:D$row")->applyFromArray($borders);
                    $row++;

                    //PASIVO + PATRIMONIO
                    $objPHPExcel-> getActiveSheet ()
                    -> getStyle ("A$row:D$row")
                    -> getFill ()
                    -> setFillType (PHPExcel_Style_Fill :: FILL_SOLID)
                    -> getStartColor ()
                    -> setRGB ('99ddff');
                    $objPHPExcel->setActiveSheetIndex(0)
                    ->setCellValueExplicit ("A$row", "" )
                    ->setCellValueExplicit ("B$row", "PASIVO + PATRIMONIO" )
                    ->setCellValueExplicit ("C$row", formatNum(abs($intActivo)) )
                    ->setCellValueExplicit ("D$row", formatNum(abs($intActivoAnterior)));
                    $objPHPExcel->getActiveSheet()->getStyle("A$row:D$row")->applyFromArray($borders);
                    $objPHPExcel->getActiveSheet()->getStyle("A$row:D$row")->getFont()->setBold(true);
                    $row++;

                    //UTILIDAD
                    $objPHPExcel-> getActiveSheet ()
                    -> getStyle ("A$row:D$row")
                    -> getFill ()
                    -> setFillType (PHPExcel_Style_Fill :: FILL_SOLID)
                    -> getStartColor ()
                    -> setRGB ('99ddff');
                    $objPHPExcel->setActiveSheetIndex(0)
                    ->setCellValueExplicit ("A$row", "" )
                    ->setCellValueExplicit ("B$row", "TOTAL" )
                    ->setCellValueExplicit ("C$row", formatNum($arrData['total']) )
                    ->setCellValueExplicit ("D$row", formatNum($arrData['total_anterior']));
                    $objPHPExcel->getActiveSheet()->getStyle("A$row:D$row")->applyFromArray($borders);
                    $objPHPExcel->getActiveSheet()->getStyle("A$row:D$row")->getFont()->setBold(true);
                    $row++;

                    foreach ($arrDet as $data) {
                        if(substr($data['comprobante'],0,1) == 8 || substr($data['comprobante'],0,1) == 9){
                            if(strlen($data['comprobante']) == 1){
                                $objPHPExcel-> getActiveSheet ()
                                    -> getStyle ("A$row:D$row")
                                    -> getFill ()
                                    -> setFillType (PHPExcel_Style_Fill :: FILL_SOLID)
                                    -> getStartColor ()
                                    -> setRGB ('99ddff');
                            }
                            $objPHPExcel->setActiveSheetIndex(0)
                            ->setCellValueExplicit ("A$row", $data['comprobante'] )
                            ->setCellValueExplicit ("B$row", $data['nombre'] )
                            ->setCellValueExplicit ("C$row", formatNum($data['saldo_final']) )
                            ->setCellValueExplicit ("D$row", !empty($data['anterior'])? formatNum($data['anterior']['saldo_final']) : formatNum(0));
                            $objPHPExcel->getActiveSheet()->getStyle("A$row:D$row")->applyFromArray($borders);
                            $row++;
                        }
                    }
                    $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
                    $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(50);
                    $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
                    $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);

                    //----Guardar documento----
                    header('Content-Type: application/vnd.ms-excel');
                    header('Content-Disposition: attachment;filename="'.$fileName.'.xlsx"');
                    header('Cache-Control: max-age=0');
                    $objWriter=PHPExcel_IOFactory::createWriter($objPHPExcel,'Excel2007');
                    $objWriter->save('php://output');
                }
            }
            die();
        }
        public function exportPdf(){
            if(!empty($_SESSION)){
                $request = configBasica();
                $strNit = $request['nit'];
                $strRazon = $request['razonsocial'];
                $fileName = 'estado_financiero_'.strtolower($strRazon)."_".$strNit.'';
                $arrData = json_decode($_POST['data'],true);
                $arrFechaInicial = explode("-",$_POST['fecha_inicial']);
                $arrFechaFinal = explode("-",$_POST['fecha_final']);
                $strFechaInicial = $arrFechaInicial[2]."/".$arrFechaInicial[1]."/".$arrFechaInicial[0];
                $strFechaFinal = $arrFechaFinal[2]."/".$arrFechaFinal[1]."/".$arrFechaFinal[0];
                $arrDet = $arrData['data']['data'];


                $pdf = new MYPDF('P','mm','Letter', true, 'iso-8859-1', false);
                $pdf->SetDocInfoUnicode (true);
                // set document information
                $pdf->SetCreator(PDF_CREATOR);
                $pdf->SetAuthor('IDEALSAS');
                $pdf->SetTitle('ESTADO DE SITUACIÓN FINANCIERA');
                $pdf->SetSubject('ESTADO DE SITUACIÓN FINANCIERA');
                $pdf->SetKeywords('TCPDF, PDF, example, test, guide');
                $pdf->SetMargins(10, 38, 10);// set margins
                $pdf->SetHeaderMargin(38);// set margins
                $pdf->SetFooterMargin(17);// set margins
                $pdf->SetAutoPageBreak(TRUE, 20);// set auto page breaks
                // set some language-dependent strings (optional)
                if (@file_exists(dirname(__FILE__).'/lang/spa.php'))
                {
                    require_once(dirname(__FILE__).'/lang/spa.php');
                    $pdf->setLanguageArray($l);
                }
                $pdf->AddPage();
                $pdf->SetFillColor(51, 153, 204);
                $pdf->SetTextColor(255,255,255);
                $pdf->SetFont('Helvetica','b',9);
                $pdf->MultiCell(35,4,"Fecha","LRBT",'P',true,0,'','',true,0,false,true,0,'M',true);
                $pdf->SetFillColor(255,255,255);
                $pdf->SetTextColor(0,0,0);
                $pdf->SetFont('Helvetica','',9);
                $pdf->MultiCell(155,4,"Desde ".$strFechaInicial." Hasta ".$strFechaFinal,"RBTL",'L',true,0,'','',true,0,false,true,0,'M',true);
                $pdf->ln();
                $pdf->ln();
                $pdf->SetFillColor(51, 153, 204);
                $pdf->SetTextColor(255,255,255);
                $pdf->SetFont('helvetica','B',7);

                $pdf->MultiCell(47.5,6,"Codigo","LRBT",'C',true,0,'','',true,0,false,true,0,'M',true);
                $pdf->MultiCell(47.5,6,"Cuenta","LRBT",'C',true,0,'','',true,0,false,true,0,'M',true);
                $pdf->MultiCell(47.5,6,"Periodo actual","LRBT",'C',true,0,'','',true,0,false,true,0,'M',true);
                $pdf->MultiCell(47.5,6,"Periodo anterior","LRBT",'C',true,0,'','',true,0,false,true,0,'M',true);
                $pdf->ln();
                $pdf->SetTextColor(0,0,0);
                $fill = true;
                $height = 5;
                $totalActivoCorriente = 0;
                $totalActivoNoCorriente = 0;
                $totalPasivoCorriente = 0;
                $totalPasivoNoCorriente = 0;
                $totalActivoAnteriorCorriente = 0;
                $totalActivoAnteriorNoCorriente = 0;
                $totalPasivoAnteriorCorriente = 0;
                $totalPasivoAnteriorNoCorriente = 0;

                $arrActivosCorrientes = array_filter($arrDet,function($e){return strlen($e['comprobante']) == 2 &&
                    substr($e['comprobante'],0,1) == 1 && $e['tipo'] == 0;});
                $arrActivosNoCorrientes = array_filter($arrDet,function($e){return strlen($e['comprobante']) == 2 &&
                    substr($e['comprobante'],0,1) == 1 && $e['tipo'] == 1;});

                $arrPasivosCorrientes = array_filter($arrDet,function($e){return strlen($e['comprobante']) == 2 &&
                    substr($e['comprobante'],0,1) == 2 && $e['tipo'] == 0;});
                $arrPasivosNoCorrientes = array_filter($arrDet,function($e){return strlen($e['comprobante']) == 2 &&
                    substr($e['comprobante'],0,1) == 2 && $e['tipo'] == 1;});
                $arrMayoresTotales = array_values(array_filter($arrDet,function($e){return strlen($e['comprobante']) == 1;}));

                foreach ($arrDet as $e) {
                    if(strlen($e['comprobante']) == 2 && substr($e['comprobante'],0,1) == 1 && $e['tipo'] == 0){
                        $totalActivoCorriente+= $e['saldo_final'];
                        $totalActivoAnteriorCorriente+= $e['anterior']['saldo_final'];
                    }else if(strlen($e['comprobante']) == 2 && substr($e['comprobante'],0,1) == 1 && $e['tipo'] == 1){
                        $totalActivoNoCorriente+= $e['saldo_final'];
                        $totalActivoAnteriorNoCorriente+= $e['anterior']['saldo_final'];
                    }else if(strlen($e['comprobante']) == 2 && substr($e['comprobante'],0,1) == 2 && $e['tipo'] == 0){
                        $totalPasivoCorriente+= $e['saldo_final'];
                        $totalPasivoAnteriorCorriente+= $e['anterior']['saldo_final'];
                    }else if(strlen($e['comprobante']) == 2 && substr($e['comprobante'],0,1) == 2 && $e['tipo'] == 1){
                        $totalPasivoNoCorriente+= $e['saldo_final'];
                        $totalPasivoAnteriorNoCorriente+= $e['anterior']['saldo_final'];
                    }
                }
                //Activos
                $pdf->SetFont('helvetica','B',7);
                $pdf->SetFillColor(153,221,255);
                $pdf->MultiCell(47.5,$height,"1","",'L',true,0,'','',true,0,false,true,0,'M',true);
                $pdf->MultiCell(47.5,$height,"ACTIVOS","",'L',true,0,'','',true,0,false,true,0,'M',true);
                $pdf->MultiCell(47.5,$height,formatNum($arrMayoresTotales[0]['saldo_final']),"",'R',true,0,'','',true,0,false,true,0,'M',true);
                $pdf->MultiCell(47.5,$height,!empty($arrMayoresTotales[0]['anterior'])? formatNum($arrMayoresTotales[0]['anterior']['saldo_final']) : formatNum(0),"",'R',true,0,'','',true,0,false,true,0,'M',true);
                $pdf->ln();

                //Corriente
                $pdf->SetFillColor(245,245,245);
                $pdf->SetFont('helvetica','B',7);

                $pdf->MultiCell(47.5,$height,"","",'L',$fill,0,'','',true,0,false,true,0,'M',true);
                $pdf->MultiCell(47.5,$height,"CORRIENTE","",'L',$fill,0,'','',true,0,false,true,0,'M',true);
                $pdf->MultiCell(47.5,$height,formatNum($totalActivoCorriente),"",'R',$fill,0,'','',true,0,false,true,0,'M',true);
                $pdf->MultiCell(47.5,$height,formatNum($totalActivoAnteriorCorriente),"",'R',$fill,0,'','',true,0,false,true,0,'M',true);
                $pdf->ln();

                foreach ($arrActivosCorrientes as $data) {
                    $pdf->SetFillColor(245,245,245);
                    $pdf->SetFont('helvetica','',7);
                    $pdf->MultiCell(47.5,$height,$data['comprobante'],"",'L',$fill,0,'','',true,0,false,true,0,'M',true);
                    $pdf->MultiCell(47.5,$height,$data['nombre'],"",'L',$fill,0,'','',true,0,false,true,0,'M',true);
                    $pdf->MultiCell(47.5,$height,formatNum($data['saldo_final']),"",'R',$fill,0,'','',true,0,false,true,0,'M',true);
                    $pdf->MultiCell(47.5,$height,!empty($data['anterior'])? formatNum($data['anterior']['saldo_final']) : formatNum(0),"",'R',$fill,0,'','',true,0,false,true,0,'M',true);
                    $pdf->ln();
                    $fill = !$fill;
                }

                //No corriente
                $pdf->SetFillColor(245,245,245);
                $pdf->SetFont('helvetica','B',7);

                $pdf->MultiCell(47.5,$height,"","",'L',$fill,0,'','',true,0,false,true,0,'M',true);
                $pdf->MultiCell(47.5,$height,"NO CORRIENTE","",'L',$fill,0,'','',true,0,false,true,0,'M',true);
                $pdf->MultiCell(47.5,$height,formatNum($totalActivoNoCorriente),"",'R',$fill,0,'','',true,0,false,true,0,'M',true);
                $pdf->MultiCell(47.5,$height,formatNum($totalActivoAnteriorNoCorriente),"",'R',$fill,0,'','',true,0,false,true,0,'M',true);
                $pdf->ln();

                foreach ($arrActivosNoCorrientes as $data) {
                    $pdf->SetFillColor(245,245,245);
                    $pdf->SetFont('helvetica','',7);
                    $pdf->MultiCell(47.5,$height,$data['comprobante'],"",'L',$fill,0,'','',true,0,false,true,0,'M',true);
                    $pdf->MultiCell(47.5,$height,$data['nombre'],"",'L',$fill,0,'','',true,0,false,true,0,'M',true);
                    $pdf->MultiCell(47.5,$height,formatNum($data['saldo_final']),"",'R',$fill,0,'','',true,0,false,true,0,'M',true);
                    $pdf->MultiCell(47.5,$height,!empty($data['anterior'])? formatNum($data['anterior']['saldo_final']) : formatNum(0),"",'R',$fill,0,'','',true,0,false,true,0,'M',true);
                    $pdf->ln();
                    $fill = !$fill;
                }

                //Pasivos
                $pdf->SetFont('helvetica','B',7);
                $pdf->SetFillColor(153,221,255);
                $pdf->MultiCell(47.5,$height,"2","",'L',true,0,'','',true,0,false,true,0,'M',true);
                $pdf->MultiCell(47.5,$height,"PASIVOS","",'L',true,0,'','',true,0,false,true,0,'M',true);
                $pdf->MultiCell(47.5,$height,formatNum(abs($arrMayoresTotales[1]['saldo_final'])),"",'R',true,0,'','',true,0,false,true,0,'M',true);
                $pdf->MultiCell(47.5,$height,!empty($arrMayoresTotales[1]['anterior'])? formatNum(abs($arrMayoresTotales[1]['anterior']['saldo_final'])) : formatNum(0),"",'R',true,0,'','',true,0,false,true,0,'M',true);
                $pdf->ln();

                //Corriente
                $pdf->SetFillColor(245,245,245);
                $pdf->SetFont('helvetica','B',7);

                $pdf->MultiCell(47.5,$height,"","",'L',$fill,0,'','',true,0,false,true,0,'M',true);
                $pdf->MultiCell(47.5,$height,"CORRIENTE","",'L',$fill,0,'','',true,0,false,true,0,'M',true);
                $pdf->MultiCell(47.5,$height,formatNum(abs($totalPasivoCorriente)),"",'R',$fill,0,'','',true,0,false,true,0,'M',true);
                $pdf->MultiCell(47.5,$height,formatNum(abs($totalPasivoAnteriorCorriente)),"",'R',$fill,0,'','',true,0,false,true,0,'M',true);
                $pdf->ln();

                foreach ($arrPasivosCorrientes as $data) {
                    $intSaldoFinalActual = $data['saldo_final'] < 0 ? -1*$data['saldo_final'] : $data['saldo_final'];
                    $intSaldoFinalAnterior = !empty($data['anterior']) ? $data['anterior']['saldo_final'] : 0;
                    $intSaldoFinalAnterior = $intSaldoFinalAnterior < 0 ? -1*$intSaldoFinalAnterior  : $intSaldoFinalAnterior;
                    $pdf->SetFillColor(245,245,245);
                    $pdf->SetFont('helvetica','',7);
                    $pdf->MultiCell(47.5,$height,$data['comprobante'],"",'L',$fill,0,'','',true,0,false,true,0,'M',true);
                    $pdf->MultiCell(47.5,$height,$data['nombre'],"",'L',$fill,0,'','',true,0,false,true,0,'M',true);
                    $pdf->MultiCell(47.5,$height,formatNum($intSaldoFinalActual),"",'R',$fill,0,'','',true,0,false,true,0,'M',true);
                    $pdf->MultiCell(47.5,$height,formatNum($intSaldoFinalAnterior),"",'R',$fill,0,'','',true,0,false,true,0,'M',true);
                    $pdf->ln();
                    $fill = !$fill;
                }

                //No corriente
                $pdf->SetFillColor(245,245,245);
                $pdf->SetFont('helvetica','B',7);

                $pdf->MultiCell(47.5,$height,"","",'L',$fill,0,'','',true,0,false,true,0,'M',true);
                $pdf->MultiCell(47.5,$height,"NO CORRIENTE","",'L',$fill,0,'','',true,0,false,true,0,'M',true);
                $pdf->MultiCell(47.5,$height,formatNum(abs($totalPasivoNoCorriente)),"",'R',$fill,0,'','',true,0,false,true,0,'M',true);
                $pdf->MultiCell(47.5,$height,formatNum(abs($totalPasivoAnteriorNoCorriente)),"",'R',$fill,0,'','',true,0,false,true,0,'M',true);
                $pdf->ln();

                foreach ($arrPasivosNoCorrientes as $data) {
                    $intSaldoFinalActual = $data['saldo_final'] < 0 ? -1*$data['saldo_final'] : $data['saldo_final'];
                    $intSaldoFinalAnterior = !empty($data['anterior']) ? $data['anterior']['saldo_final'] : 0;
                    $intSaldoFinalAnterior = $intSaldoFinalAnterior < 0 ? -1*$intSaldoFinalAnterior  : $intSaldoFinalAnterior;
                    $pdf->SetFillColor(245,245,245);
                    $pdf->SetFont('helvetica','',7);
                    $pdf->MultiCell(47.5,$height,$data['comprobante'],"",'L',$fill,0,'','',true,0,false,true,0,'M',true);
                    $pdf->MultiCell(47.5,$height,$data['nombre'],"",'L',$fill,0,'','',true,0,false,true,0,'M',true);
                    $pdf->MultiCell(47.5,$height,formatNum($intSaldoFinalActual),"",'R',$fill,0,'','',true,0,false,true,0,'M',true);
                    $pdf->MultiCell(47.5,$height,formatNum($intSaldoFinalAnterior),"",'R',$fill,0,'','',true,0,false,true,0,'M',true);
                    $pdf->ln();
                    $fill = !$fill;
                }

                //Patrimonio
                foreach ($arrDet as $data) {

                    if(substr($data['comprobante'],0,1) == 3){
                        $pdf->SetFillColor(245,245,245);
                        $pdf->SetFont('helvetica','',7);
                        if(strlen($data['comprobante']) == 1){
                            $pdf->SetFont('helvetica','B',7);
                            $pdf->SetFillColor(153,221,255);
                            $fill = true;
                        }
                        $pdf->MultiCell(47.5,$height,$data['comprobante'],"",'L',$fill,0,'','',true,0,false,true,0,'M',true);
                        $pdf->MultiCell(47.5,$height,$data['nombre'],"",'L',$fill,0,'','',true,0,false,true,0,'M',true);
                        $pdf->MultiCell(47.5,$height,formatNum(abs($data['saldo_final'])),"",'R',$fill,0,'','',true,0,false,true,0,'M',true);
                        $pdf->MultiCell(47.5,$height,!empty($data['anterior'])? formatNum(abs($data['anterior']['saldo_final'])) : formatNum(0),"",'R',$fill,0,'','',true,0,false,true,0,'M',true);
                        $pdf->ln();
                        $fill = !$fill;
                    }
                }
                $pdf->SetFillColor(245,245,245);
                $pdf->SetFont('helvetica','',7);

                $utilidad = -1*$arrData['utilidad'];
                $utilidadAnterior = -1*$arrData['utilidad_anterior'];

                $pdf->MultiCell(47.5,$height,"31","",'L',true,0,'','',true,0,false,true,0,'M',true);
                $pdf->MultiCell(47.5,$height,"RESULTADO DEL EJERCICIO","",'L',true,0,'','',true,0,false,true,0,'M',true);
                $pdf->MultiCell(47.5,$height,formatNum($utilidad),"",'R',true,0,'','',true,0,false,true,0,'M',true);
                $pdf->MultiCell(47.5,$height,formatNum($utilidadAnterior),"",'R',true,0,'','',true,0,false,true,0,'M',true);
                $pdf->ln();

                //PATRIMONIO + PASIVO
                $pdf->SetFont('helvetica','B',7);
                $pdf->SetFillColor(153,221,255);
                $pdf->MultiCell(47.5,$height,"","",'L',true,0,'','',true,0,false,true,0,'M',true);
                $pdf->MultiCell(47.5,$height,"ACTIVO = PASIVO + PATRIMONIO","",'L',true,0,'','',true,0,false,true,0,'M',true);
                $pdf->MultiCell(47.5,$height,formatNum(-1*$arrData['activo']),"",'R',true,0,'','',true,0,false,true,0,'M',true);
                $pdf->MultiCell(47.5,$height,formatNum(-1*$arrData['activo_anterior']),"",'R',true,0,'','',true,0,false,true,0,'M',true);
                $pdf->ln();

                //UTILIDAD
                $pdf->SetFont('helvetica','B',7);
                $pdf->SetFillColor(153,221,255);
                $pdf->MultiCell(47.5,$height,"","",'L',true,0,'','',true,0,false,true,0,'M',true);
                $pdf->MultiCell(47.5,$height,"TOTAL","",'L',true,0,'','',true,0,false,true,0,'M',true);
                $pdf->MultiCell(47.5,$height,formatNum($arrData['total']),"",'R',true,0,'','',true,0,false,true,0,'M',true);
                $pdf->MultiCell(47.5,$height,formatNum($arrData['total_anterior']),"",'R',true,0,'','',true,0,false,true,0,'M',true);
                $pdf->ln();

                //Otros
                foreach ($arrDet as $data) {
                    if(substr($data['comprobante'],0,1) > 6){
                        $pdf->SetFillColor(245,245,245);
                        $pdf->SetFont('helvetica','',7);
                        if(strlen($data['comprobante']) == 1){
                            $pdf->SetFont('helvetica','B',7);
                            $pdf->SetFillColor(153,221,255);
                            $fill = true;
                        }
                        $pdf->MultiCell(47.5,$height,$data['comprobante'],"",'L',$fill,0,'','',true,0,false,true,0,'M',true);
                        $pdf->MultiCell(47.5,$height,$data['nombre'],"",'L',$fill,0,'','',true,0,false,true,0,'M',true);
                        $pdf->MultiCell(47.5,$height,formatNum($data['saldo_final']),"",'R',$fill,0,'','',true,0,false,true,0,'M',true);
                        $pdf->MultiCell(47.5,$height,!empty($data['anterior'])? formatNum($data['anterior']['saldo_final']) : formatNum(0),"",'R',$fill,0,'','',true,0,false,true,0,'M',true);
                        $pdf->ln();
                        $fill = !$fill;
                    }
                }

                $pdf->ln();
                $pdf->Output($fileName.'.pdf', 'I');

            }
            die();
        }

    }
    class MYPDF extends TCPDF {

		public function Header()
		{
			$request = configBasica();
            $strNit = $request['nit'];
            $strRazon = $request['razonsocial'];

			//Parte Izquierda
			$this->Image('../../../imagenes/escudo.jpg',  13, 13, 20, 20, 'JPG', '', 'T', true, 300, '', false, false, 0, false, false, false);// Logo
			$this->SetFont('helvetica','B',8);
			$this->SetY(10);
			$this->RoundedRect(10, 10, 190, 25, 1,'');
			$this->Cell(0.1);
			$this->Cell(26,25,'','R',0,'L');
			$this->SetY(8);
			$this->SetX(40);
			$this->SetFont('helvetica','B',9);
			$this->Cell(140,15,strtoupper("$strRazon"),0,0,'C');
			$this->SetFont('helvetica','B',7);
			$this->SetY(12);
			$this->SetX(40);
			$this->Cell(140,15,'NIT: '.$strNit,0,0,'C');
			//*****************************************************************************************************************************
			$this->SetFont('helvetica','B',9);
			$this->SetY(23);
			$this->SetX(36);
            $this->Cell(164,12,"ESTADO DE SITUACIÓN FINANCIERA",'T',0,'C');


            $this->SetFont('helvetica','B',7);
			$this->SetY(10);
			$this->SetX(167);
			$this->Cell(30,7," FECHA: ". date("d/m/Y"),"L",0,'L');
			$this->SetY(17);
			$this->SetX(167);
            $this->Cell(35,6,"","L",0,'L');

			//**********************************************************
			$this->SetFont('times','B',10);
			$this->ln(12);
		}
		public function Footer(){

			$request = configBasica();
            $strDireccion = $request['direccion'] != "" ? "Dirección: ".strtoupper($request['direccion']) : "";
            $strWeb = $request['web'] != "" ? "Pagina web: ".strtoupper($request['web']) :"";
            $strEmail = $request['email'] !="" ? "Email: ".strtoupper($request['email']) :"";
            $strTelefono = $request['telefono'] != "" ? "Telefonos: ".$request['telefono'] : "";
            $strUsuario = searchUser($_SESSION['cedulausu'])['nom_usu'];
			$strNick = $_SESSION['nickusu'];
			$strFecha = date("d/m/Y H:i:s");
			$strIp = $_SERVER['REMOTE_ADDR'];

			$this->SetFont('helvetica', 'I', 8);
			$txt = <<<EOD
			$strDireccion $strTelefono
			$strEmail $strWeb
			EOD;
			$this->SetFont('helvetica', 'I', 6);
			$this->Cell(277,10,'','T',0,'T');
			$this->ln(2);
			$this->Write(0, $txt , '', 0, 'C', true, 0, false, false, 0);

			$this->Cell(50, 10, 'Hecho por: '.$strUsuario, 00, false, 'C', 0, '', 0, false, 'T', 'M');
			$this->Cell(30, 10, 'Impreso por: '.$strNick, 0, false, 'C', 0, '', 0, false, 'T', 'M');
			$this->Cell(30, 10, 'IP: '.$strIp, 0, false, 'C',0, '', 0, false, 'T', 'M');
			$this->Cell(30, 10, 'Fecha: '.$strFecha, 0, false, 'C', 0, '', 0, false, 'T', 'M');
			$this->Cell(30, 10, 'IDEAL.10 S.A.S', 0, false, 'C', 0, '', 0, false, 'T', 'M');
            $this->Cell(20, 10, 'Pagina '.$this->getAliasNumPage().'/'.$this->getAliasNbPages(), 0, false, 'C', 0, '', 0, false, 'T', 'M');
		}
	}

    if($_POST){
        $obj = new EstadoFinancieroExportController();
        if($_POST['action'] == "excel"){
            $obj->exportExcel();
        }else if($_POST['action'] == "pdf"){
            $obj->exportPdf();
        }
    }

?>
