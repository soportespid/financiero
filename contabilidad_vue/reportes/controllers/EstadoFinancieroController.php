<?php
    require_once '../../../Librerias/core/Helpers.php';
    require_once '../../../PHPExcel/Classes/PHPExcel.php';
    require_once '../models/EstadoFinancieroModel.php';
    session_start();
    class EstadoFinancieroController extends EstadoFinancieroModel{
        public function generate(){
            if(!empty($_SESSION)){
                $strFechaInicialActual = strClean($_POST['fecha_inicial']);
                $strFechaFinalActual = strClean($_POST['fecha_final']);
                $arrFechaInicial = explode("-",$strFechaInicialActual);
                $arrFechaFinal = explode("-",$strFechaFinalActual);
                $strFechaInicialAnterior = ($arrFechaInicial[0]-1)."-".$arrFechaInicial[1]."-".$arrFechaInicial[2];
                $strFechaFinalAnterior = ($arrFechaFinal[0]-1)."-".$arrFechaFinal[1]."-".$arrFechaFinal[2];
                $arrCuentas = $this->selectBuscarCuentas();
                $arrNiveles = $this->selectNiveles();
                $arrDataActual = $this->selectMovimientosPeriodos(2,$strFechaInicialActual,$strFechaFinalActual,$arrCuentas);
                $arrDataAnterior = $this->selectMovimientosPeriodos(2,$strFechaInicialAnterior,$strFechaFinalAnterior,$arrCuentas);

                $arrDataActual = $this->orderData($this->setNiveles($arrDataActual,$arrNiveles,2));
                $arrDataAnterior = $this->orderData($this->setNiveles($arrDataAnterior,$arrNiveles,2));
                if(!empty($arrDataActual['data'])){
                    $totalActual = count($arrDataActual['data']);
                    for ($i=0; $i < $totalActual; $i++) {
                        $data = $arrDataActual['data'][$i];
                        $anterior = array_values(array_filter($arrDataAnterior['data'],function($e) use($data){return $data['comprobante'] == $e['comprobante'];}));
                        $arrDataActual['data'][$i]['anterior']= [] ;
                        if(!empty($anterior)){
                            $arrDataActual['data'][$i]['anterior']= $anterior[0] ;
                        }
                    }
                    $totalActivo = 0;
                    $totalPasivo = 0;
                    $totalPatrimonio = 0;
                    $totalResultado = 0;
                    $totalActivoAnterior = 0;
                    $totalPasivoAnterior = 0;
                    $totalPatrimonioAnterior = 0;
                    $totalResultadoAnterior = 0;
                    $totalIngresosActual = 0;
                    $totalGastosActual = 0;
                    $totalIngresosAnterior = 0;
                    $totalGastosAnterior = 0;
                    foreach ($arrDataActual['data'] as $data) {
                        if($data['comprobante'] == 1){
                            $totalActivo+=$data['saldo_final'];
                            if(!empty($data['anterior'])){
                                $totalActivoAnterior+=$data['anterior']['saldo_final'];
                            }
                        }
                        if($data['comprobante'] == 2){
                            $totalPasivo+= $data['saldo_final'];
                            if(!empty($data['anterior'])){
                                $totalPasivoAnterior+=$data['anterior']['saldo_final'];
                            }
                        }
                        if($data['comprobante'] == 3){
                            $totalPatrimonio+= $data['saldo_final'];
                            if(!empty($data['anterior'])){
                                $totalPatrimonioAnterior+=$data['anterior']['saldo_final'];
                            }
                        }
                        if($data['comprobante'] == 4){
                            $totalIngresosActual+=$data['saldo_final'];
                            if(!empty($data['anterior'])){
                                $totalIngresosAnterior+=$data['anterior']['saldo_final'];
                            }
                        }
                        if($data['comprobante'] == 5 || $data['comprobante'] == 6){
                            $totalGastosActual+= $data['saldo_final'];
                            if(!empty($data['anterior'])){
                                $totalGastosAnterior+=$data['anterior']['saldo_final'];
                            }
                        }
                    }

                    $totalUtilidadActual = $totalIngresosActual+$totalGastosActual;
                    $totalUtilidadAnterior = $totalIngresosAnterior+$totalGastosAnterior;
                    $totalResultado = $totalPasivo+$totalPatrimonio+$totalUtilidadActual;
                    $totalResultadoAnterior =$totalPasivoAnterior+$totalPatrimonioAnterior+$totalUtilidadAnterior;
                    $arrResponse = array(
                        "data"=>$arrDataActual,
                        "activo"=>$totalResultado,
                        "activo_anterior"=>$totalResultadoAnterior,
                        "total"=>$totalPasivo+$totalActivo+$totalPatrimonio+$totalIngresosActual+$totalGastosActual,
                        "total_anterior"=>$totalPasivoAnterior+$totalActivoAnterior+$totalPatrimonioAnterior+$totalIngresosAnterior+$totalGastosAnterior,
                        "utilidad"=>$totalUtilidadActual,
                        "utilidad_anterior"=>$totalUtilidadAnterior
                    );
                }
                echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
            }
            die();
        }
        public function setNiveles($arrData,$arrNiveles,$intNivel){
            $totalData = count($arrData);
            $arrNewData = [];
            for ($i=0; $i < $intNivel ; $i++) {
                for ($j=0; $j < $totalData; $j++) {
                    $data = $arrData[$j];
                    if(strlen($data['comprobante'])>=$arrNiveles[$i-1]['posiciones']){
                        $strComprobante = substr($data['comprobante'],0,$arrNiveles[$i-1]['posiciones']);
                        if($strComprobante!=""){
                            $totalDataTemp = count($arrNewData);
                            $flag = false;
                            for ($k=0; $k < $totalDataTemp; $k++) {
                                if($strComprobante == $arrNewData[$k]['comprobante']){
                                    $arrNewData[$k]['saldo']+=$data['saldo'];
                                    $arrNewData[$k]['debito']+=$data['debito'];
                                    $arrNewData[$k]['credito']+=$data['credito'];
                                    $arrNewData[$k]['saldo_final'] = $arrNewData[$k]['saldo']+$arrNewData[$k]['debito']-$arrNewData[$k]['credito'];
                                    $flag = true;
                                    break;
                                }
                            }
                            if(!$flag){
                                $data['comprobante'] = $strComprobante;
                                $data['saldo_final'] = $data['saldo']+$data['debito']-$data['credito'];
                                array_push($arrNewData,$data);
                            }
                        }else{
                            $data['saldo_final'] = $data['saldo']+$data['debito']-$data['credito'];
                            array_push($arrNewData,$data);
                        }
                    }
                }
            }
            return $arrNewData;
        }

        public function orderData($data){
            $arrNombresCuentas = getNombresCuentaNicsp();
            $arrComp = array_column($data,"comprobante");
            $arrData = [];
            $arrCorriente = [11,13,15,19,24,25,29];
            usort($arrComp, 'strcmp');
            $arrComp = array_values(array_unique($arrComp));
            $totalComp = count($arrComp);
            $totalData = count($data);
            $totalCredito = 0;
            $totalDebito = 0;
            $totalSaldo = 0;
            $totalSaldoFinal = 0;
            for ($i=0; $i < $totalComp ; $i++) {
                for ($j=0; $j < $totalData; $j++) {
                    $arrInfo = $data[$j];
                    if($arrComp[$i] == $data[$j]['comprobante']){
                        $comprobante = $arrComp[$i];
                        $arrNombre = array_values(array_filter($arrNombresCuentas,function($e)use($comprobante){return $comprobante==$e['cuenta'];}))[0];
                        if(!empty($arrNombre)){$arrInfo['nombre'] = strtoupper($arrNombre['nombre']);}
                        $arrInfo['tipo'] = 1;
                        if(in_array(substr($comprobante,0,2),$arrCorriente)){
                            $arrInfo['tipo'] = 0;
                        }
                        array_push($arrData,$arrInfo);
                        break;
                    }
                }
            }
            $arrDataPrincipales = array_filter($arrData,function($e){return strlen($e['comprobante'])==1;});
            foreach ($arrDataPrincipales as $data) {
                $totalSaldo += round($data['saldo'],2);
                $totalCredito += round($data['credito'],2);
                $totalDebito += round($data['debito'],2);
            }
            $totalSaldoFinal = round($totalSaldo+$totalDebito-$totalCredito,2);
            $totalSaldo = round($totalSaldo,2);
            $totalCredito = round($totalCredito,2);
            $totalDebito = round($totalDebito,2);
            return array("data"=>$arrData,"saldo"=>$totalSaldo,"debito"=>$totalDebito,"credito"=>$totalCredito,"saldo_final"=>$totalSaldoFinal);
        }
    }
    if($_POST){
        $obj = new EstadoFinancieroController();
        if($_POST['action']=="gen"){
            $obj->generate();
        }
    }

?>
