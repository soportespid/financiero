<?php
    require_once '../../../Librerias/core/Helpers.php';
    require_once '../../../PHPExcel/Classes/PHPExcel.php';
    require_once '../../../tcpdf/tcpdf_include.php';
    require_once '../../../tcpdf/tcpdf.php';
    session_start();
    /*ini_set('display_errors', '1');
    ini_set('display_startup_errors', '1');
    error_reporting(E_ALL);*/
    class BalanceExportController {
        public function exportExcel(){
            if(!empty($_SESSION)){
                $arrData = json_decode($_POST['data'],true);
                if(!empty($arrData)){
                    $request = configBasica();
                    $strNit = $request['nit'];
                    $strRazon = $request['razonsocial'];
                    $fileName = 'balance_general_'.strtolower($strRazon)."_".$strNit.'';
                    $arrData = json_decode($_POST['data'],true);
                    $arrFechaInicial = explode("-",$_POST['fecha_inicial']);
                    $arrFechaFinal = explode("-",$_POST['fecha_final']);
                    $strFechaInicial = $arrFechaInicial[2]."/".$arrFechaInicial[1]."/".$arrFechaInicial[0];
                    $strFechaFinal = $arrFechaFinal[2]."/".$arrFechaFinal[1]."/".$arrFechaFinal[0];
                    $strCuentaInicial = strClean($_POST['cuenta_inicial']);
                    $strCuentaFinal = strClean($_POST['cuenta_final']);
                    $arrDet = $arrData['data'];
                    $objPHPExcel = new PHPExcel();
                    $objPHPExcel->getActiveSheet()->getStyle('A:F')->applyFromArray(
                        array(
                            'alignment' => array(
                                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
                                'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER
                            )
                        )
                    );
                    $objPHPExcel->getProperties()
                    ->setCreator("IDEAL 10")
                    ->setLastModifiedBy("IDEAL 10")
                    ->setTitle("Exportar Excel con PHP")
                    ->setSubject("Documento de prueba")
                    ->setDescription("Documento generado con PHPExcel")
                    ->setKeywords("usuarios phpexcel")
                    ->setCategory("reportes");

                    //----Cuerpo de Documento----
                    $objPHPExcel->setActiveSheetIndex(0)
                    ->mergeCells('A1:F1')
                    ->mergeCells('A2:F2')
                    ->mergeCells('B3:F3')
                    ->mergeCells('B4:F4')
                    ->setCellValue('A1', $strRazon.' - CONTABILIDAD')
                    ->setCellValue('A2', 'BALANCE DE PRUEBA NICSP')
                    ->setCellValue('A3', 'Fecha')
                    ->setCellValue('A4', 'Cuentas')
                    ->setCellValue('B3', "Desde ".$strFechaInicial." Hasta ".$strFechaFinal)
                    ->setCellValue('B4', "Desde ".$strCuentaInicial." Hasta ".$strCuentaFinal)
                    ->setCellValue('A5', 'Codigo')
                    ->setCellValue('B5', 'Cuenta')
                    ->setCellValue('C5', 'Saldo anterior')
                    ->setCellValue('D5', 'Debito')
                    ->setCellValue('E5', 'Credito')
                    ->setCellValue('F5', 'Saldo final');
                    $objPHPExcel-> getActiveSheet ()
                    -> getStyle ("A3")
                    -> getFill ()
                    -> setFillType (PHPExcel_Style_Fill :: FILL_SOLID)
                    -> getStartColor ()
                    -> setRGB ('3399cc');
                    $objPHPExcel-> getActiveSheet ()
                    -> getStyle ("A4")
                    -> getFill ()
                    -> setFillType (PHPExcel_Style_Fill :: FILL_SOLID)
                    -> getStartColor ()
                    -> setRGB ('3399cc');
                    $objPHPExcel-> getActiveSheet ()
                    -> getStyle ("A1:F1")
                    -> getFill ()
                    -> setFillType (PHPExcel_Style_Fill :: FILL_SOLID)
                    -> getStartColor ()
                    -> setRGB ('3399cc');
                    $objPHPExcel-> getActiveSheet ()
                    -> getStyle ("A5:F5")
                    -> getFill ()
                    -> setFillType (PHPExcel_Style_Fill :: FILL_SOLID)
                    -> getStartColor ()
                    -> setRGB ('3399cc');

                    $objPHPExcel-> getActiveSheet ()
                    -> getStyle ('A1:A2')
                    -> getAlignment ()
                    -> applyFromArray (array ( 'horizontal'  =>  PHPExcel_Style_Alignment :: HORIZONTAL_CENTER ,) );
                    $objPHPExcel-> getActiveSheet ()
                    -> getStyle ('A5:F5')
                    -> getAlignment ()
                    -> applyFromArray (array ( 'horizontal'  =>  PHPExcel_Style_Alignment :: HORIZONTAL_CENTER ,) );

                    $borders = array(
                        'borders' => array(
                            'allborders' => array(
                                'style' => PHPExcel_Style_Border::BORDER_THIN,
                            'color' => array('argb' => 'FF000000'),
                            )
                        ),
                    );





                    $objPHPExcel->getActiveSheet()->getStyle("A1:F1")->applyFromArray($borders);
                    $objPHPExcel->getActiveSheet()->getStyle("A2:F2")->applyFromArray($borders);
                    $objPHPExcel->getActiveSheet()->getStyle("A3:F3")->applyFromArray($borders);
                    $objPHPExcel->getActiveSheet()->getStyle("A4:F4")->applyFromArray($borders);
                    $objPHPExcel->getActiveSheet()->getStyle("A5:F5")->applyFromArray($borders);


                    $objPHPExcel->getActiveSheet()->getStyle("A1")->getFont()->getColor()->setRGB("ffffff");
                    $objPHPExcel->getActiveSheet()->getStyle("A3")->getFont()->getColor()->setRGB("ffffff");
                    $objPHPExcel->getActiveSheet()->getStyle("A4")->getFont()->getColor()->setRGB("ffffff");
                    $objPHPExcel->getActiveSheet()->getStyle("A5")->getFont()->getColor()->setRGB("ffffff");
                    $objPHPExcel->getActiveSheet()->getStyle("B5")->getFont()->getColor()->setRGB("ffffff");
                    $objPHPExcel->getActiveSheet()->getStyle("C5")->getFont()->getColor()->setRGB("ffffff");
                    $objPHPExcel->getActiveSheet()->getStyle("D5")->getFont()->getColor()->setRGB("ffffff");
                    $objPHPExcel->getActiveSheet()->getStyle("E5")->getFont()->getColor()->setRGB("ffffff");
                    $objPHPExcel->getActiveSheet()->getStyle("F5")->getFont()->getColor()->setRGB("ffffff");

                    $objPHPExcel->getActiveSheet()->getStyle("A1:F1")->getFont()->setBold(true);
                    $objPHPExcel->getActiveSheet()->getStyle("A3")->getFont()->setBold(true);
                    $objPHPExcel->getActiveSheet()->getStyle("A3")->getFont()->setBold(true);
                    $objPHPExcel->getActiveSheet()->getStyle("A4")->getFont()->setBold(true);
                    $objPHPExcel->getActiveSheet()->getStyle("A5")->getFont()->setBold(true);
                    $objPHPExcel->getActiveSheet()->getStyle("B5")->getFont()->setBold(true);
                    $objPHPExcel->getActiveSheet()->getStyle("C5")->getFont()->setBold(true);
                    $objPHPExcel->getActiveSheet()->getStyle("D5")->getFont()->setBold(true);
                    $objPHPExcel->getActiveSheet()->getStyle("E5")->getFont()->setBold(true);
                    $objPHPExcel->getActiveSheet()->getStyle("F5")->getFont()->setBold(true);

                    $row = 6;

                    foreach ($arrDet as $data) {
                        if(strlen($data['comprobante']) < $arrData['nivel']){
                            $objPHPExcel-> getActiveSheet ()
                                -> getStyle ("A$row:F$row")
                                -> getFill ()
                                -> setFillType (PHPExcel_Style_Fill :: FILL_SOLID)
                                -> getStartColor ()
                                -> setRGB ('99ddff');
                            $objPHPExcel->getActiveSheet()->getStyle("A$row:F$row")->getFont()->setBold(true);
                        }
                        $objPHPExcel->setActiveSheetIndex(0)
                        ->setCellValueExplicit ("A$row", $data['comprobante'] )
                        ->setCellValueExplicit ("B$row", $data['nombre'] )
                        ->setCellValueExplicit ("C$row", formatNum($data['saldo']))
                        ->setCellValueExplicit ("D$row", formatNum($data['debito']))
                        ->setCellValueExplicit ("E$row", formatNum($data['credito']))
                        ->setCellValueExplicit ("F$row", formatNum($data['saldo_final']));
                        $objPHPExcel->getActiveSheet()->getStyle("A$row:F$row")->applyFromArray($borders);
                        $row++;
                    }
                    $objPHPExcel-> getActiveSheet ()
                    -> getStyle ("A$row:F$row")
                    -> getFill ()
                    -> setFillType (PHPExcel_Style_Fill :: FILL_SOLID)
                    -> getStartColor ()
                    -> setRGB ('99ddff');

                    $objPHPExcel->setActiveSheetIndex(0)
                    ->mergeCells("A$row:B$row")
                    ->setCellValueExplicit ("A$row", "TOTAL")
                    ->setCellValueExplicit ("C$row", formatNum($arrData['saldo']))
                    ->setCellValueExplicit ("D$row", formatNum($arrData['debito']))
                    ->setCellValueExplicit ("E$row", formatNum($arrData['credito']))
                    ->setCellValueExplicit ("F$row", formatNum($arrData['saldo_final']));
                    $objPHPExcel->getActiveSheet()->getStyle("A$row:F$row")->getFont()->setBold(true);
                    $objPHPExcel->getActiveSheet()->getStyle("A$row:F$row")->applyFromArray($borders);

                    $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
                    $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(50);
                    $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
                    $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
                    $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
                    $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);

                    //----Guardar documento----
                    header('Content-Type: application/vnd.ms-excel');
                    header('Content-Disposition: attachment;filename="'.$fileName.'.xlsx"');
                    header('Cache-Control: max-age=0');
                    $objWriter=PHPExcel_IOFactory::createWriter($objPHPExcel,'Excel2007');
                    $objWriter->save('php://output');
                }
            }
            die();
        }
        public function exportPdf(){
            if(!empty($_SESSION)){

                $request = configBasica();
                $strNit = $request['nit'];
                $strRazon = $request['razonsocial'];
                $fileName = 'balance_general_'.strtolower($strRazon)."_".$strNit.'';
                $arrData = json_decode($_POST['data'],true);
                $arrFechaInicial = explode("-",$_POST['fecha_inicial']);
                $arrFechaFinal = explode("-",$_POST['fecha_final']);
                $strFechaInicial = $arrFechaInicial[2]."/".$arrFechaInicial[1]."/".$arrFechaInicial[0];
                $strFechaFinal = $arrFechaFinal[2]."/".$arrFechaFinal[1]."/".$arrFechaFinal[0];
                $strCuentaInicial = strClean($_POST['cuenta_inicial']);
                $strCuentaFinal = strClean($_POST['cuenta_final']);
                $arrDet = $arrData['data'];



                $pdf = new MYPDF('P','mm','Letter', true, 'iso-8859-1', false);
                $pdf->SetDocInfoUnicode (true);
                // set document information
                $pdf->SetCreator(PDF_CREATOR);
                $pdf->SetAuthor('IDEALSAS');
                $pdf->SetTitle('BALANCE DE PRUEBA NICSP');
                $pdf->SetSubject('BALANCE DE PRUEBA NICSP');
                $pdf->SetKeywords('TCPDF, PDF, example, test, guide');
                $pdf->SetMargins(10, 38, 10);// set margins
                $pdf->SetHeaderMargin(38);// set margins
                $pdf->SetFooterMargin(17);// set margins
                $pdf->SetAutoPageBreak(TRUE, 20);// set auto page breaks
                // set some language-dependent strings (optional)
                if (@file_exists(dirname(__FILE__).'/lang/spa.php'))
                {
                    require_once(dirname(__FILE__).'/lang/spa.php');
                    $pdf->setLanguageArray($l);
                }

                $pdf->AddPage();
                $pdf->SetFillColor(51, 153, 204);
                $pdf->SetTextColor(255,255,255);
                $pdf->SetFont('Helvetica','b',9);
                $pdf->MultiCell(35,4,"Fecha","LRBT",'P',true,0,'','',true,0,false,true,0,'M',true);
                $pdf->SetFillColor(255,255,255);
                $pdf->SetTextColor(0,0,0);
                $pdf->SetFont('Helvetica','',9);
                $pdf->MultiCell(155,4,"Desde ".$strFechaInicial." Hasta ".$strFechaFinal,"RBTL",'L',true,0,'','',true,0,false,true,0,'M',true);
                $pdf->ln();
                $pdf->SetFillColor(51, 153, 204);
                $pdf->SetTextColor(255,255,255);
                $pdf->SetFont('Helvetica','b',9);
                $pdf->MultiCell(35,4,"Cuentas","LRBT",'P',true,0,'','',true,0,false,true,0,'M',true);
                $pdf->SetFillColor(255,255,255);
                $pdf->SetTextColor(0,0,0);
                $pdf->SetFont('Helvetica','',9);
                $pdf->MultiCell(155,4,"Desde ".$strCuentaInicial." Hasta ".$strCuentaFinal,"RBTL",'L',true,0,'','',true,0,false,true,0,'M',true);
                $pdf->ln();
                $pdf->ln();
                $pdf->SetFillColor(51, 153, 204);
                $pdf->SetTextColor(255,255,255);
                $pdf->SetFont('helvetica','B',7);

                $pdf->MultiCell(31.6,6,"Codigo","LRBT",'C',true,0,'','',true,0,false,true,0,'M',true);
                $pdf->MultiCell(31.6,6,"Cuenta","LRBT",'C',true,0,'','',true,0,false,true,0,'M',true);
                $pdf->MultiCell(31.6,6,"Saldo anterior","LRBT",'C',true,0,'','',true,0,false,true,0,'M',true);
                $pdf->MultiCell(31.6,6,"Debito","LRBT",'C',true,0,'','',true,0,false,true,0,'M',true);
                $pdf->MultiCell(31.6,6,"Credito","LRBT",'C',true,0,'','',true,0,false,true,0,'M',true);
                $pdf->MultiCell(31.6,6,"Saldo final","LRBT",'C',true,0,'','',true,0,false,true,0,'M',true);
                $pdf->ln();
                $pdf->SetTextColor(0,0,0);
                $fill = true;
                $height = 10;
                foreach ($arrDet as $data) {

                    $pdf->SetFillColor(245,245,245);
                    $pdf->SetFont('helvetica','',7);
                    if(strlen($data['comprobante']) < $arrData['nivel']){
                        $pdf->SetFont('helvetica','B',7);
                        $pdf->SetFillColor(153,221,255);
                        $fill = true;
                    }
                    $pdf->MultiCell(31.6,$height,$data['comprobante'],"",'L',$fill,0,'','',true,0,false,true,0,'M',true);
                    $pdf->MultiCell(62,$height,$data['nombre'],"",'L',$fill,0,'','',true,0,false,true,0,'M',true);
                    $pdf->MultiCell(24.1,$height,formatNum($data['saldo']),"",'R',$fill,0,'','',true,0,false,true,0,'M',true);
                    $pdf->MultiCell(24.1,$height,formatNum($data['debito']),"",'R',$fill,0,'','',true,0,false,true,0,'M',true);
                    $pdf->MultiCell(24.1,$height,formatNum($data['credito']),"",'R',$fill,0,'','',true,0,false,true,0,'M',true);
                    $pdf->MultiCell(24.1,$height,formatNum($data['saldo_final']),"",'R',$fill,0,'','',true,0,false,true,0,'M',true);
                    $pdf->ln();
                    if($pdf->getY() > 260){
                        $pdf->addPage();
                    }
                    $fill = !$fill;
                }
                $pdf->SetFont('helvetica','B',7);
                $pdf->SetFillColor(153,221,255);
                $pdf->MultiCell(94,$height,"TOTAL","",'R',true,0,'','',true,0,false,true,0,'M',true);
                $pdf->MultiCell(24.1,$height,formatNum($arrData['saldo']),"",'R',true,0,'','',true,0,false,true,0,'M',true);
                $pdf->MultiCell(24.1,$height,formatNum($arrData['debito']),"",'R',true,0,'','',true,0,false,true,0,'M',true);
                $pdf->MultiCell(24.1,$height,formatNum($arrData['credito']),"",'R',true,0,'','',true,0,false,true,0,'M',true);
                $pdf->MultiCell(24.1,$height,formatNum($arrData['saldo_final']),"",'R',true,0,'','',true,0,false,true,0,'M',true);
                $pdf->ln();
                $pdf->Output($fileName.'.pdf', 'I');

            }
            die();
        }

    }
    class MYPDF extends TCPDF {

		public function Header()
		{
			$request = configBasica();
            $strNit = $request['nit'];
            $strRazon = $request['razonsocial'];

			//Parte Izquierda
			$this->Image('../../../imagenes/escudo.jpg',  13, 13, 20, 20, 'JPG', '', 'T', true, 300, '', false, false, 0, false, false, false);// Logo
			$this->SetFont('helvetica','B',8);
			$this->SetY(10);
			$this->RoundedRect(10, 10, 190, 25, 1,'');
			$this->Cell(0.1);
			$this->Cell(26,25,'','R',0,'L');
			$this->SetY(8);
			$this->SetX(40);
			$this->SetFont('helvetica','B',9);
			$this->Cell(140,15,strtoupper("$strRazon"),0,0,'C');
			$this->SetFont('helvetica','B',7);
			$this->SetY(12);
			$this->SetX(40);
			$this->Cell(140,15,'NIT: '.$strNit,0,0,'C');
			//*****************************************************************************************************************************
			$this->SetFont('helvetica','B',9);
			$this->SetY(23);
			$this->SetX(36);
            $this->Cell(164,12,"BALANCE DE PRUEBA NICSP",'T',0,'C');


            $this->SetFont('helvetica','B',7);
			$this->SetY(10);
			$this->SetX(167);
			$this->Cell(30,7," FECHA: ". date("d/m/Y"),"L",0,'L');
			$this->SetY(17);
			$this->SetX(167);
            $this->Cell(35,6,"","L",0,'L');

			//**********************************************************
			$this->SetFont('times','B',10);
			$this->ln(12);
			//**********************************************************
            if(MOV=="401"){
                $img_file = './assets/img/reversado.png';
                $this->Image($img_file, 0, 20, 250, 280, '', '', '', false, 300, '', false, false, 0);
            }
		}
		public function Footer(){

			$request = configBasica();
            $strDireccion = $request['direccion'] != "" ? "Dirección: ".strtoupper($request['direccion']) : "";
            $strWeb = $request['web'] != "" ? "Pagina web: ".strtoupper($request['web']) :"";
            $strEmail = $request['email'] !="" ? "Email: ".strtoupper($request['email']) :"";
            $strTelefono = $request['telefono'] != "" ? "Telefonos: ".$request['telefono'] : "";
            $strUsuario = searchUser($_SESSION['cedulausu'])['nom_usu'];
			$strNick = $_SESSION['nickusu'];
			$strFecha = date("d/m/Y H:i:s");
			$strIp = $_SERVER['REMOTE_ADDR'];

			$this->SetFont('helvetica', 'I', 8);
			$txt = <<<EOD
			$strDireccion $strTelefono
			$strEmail $strWeb
			EOD;
			$this->SetFont('helvetica', 'I', 6);
			$this->Cell(277,10,'','T',0,'T');
			$this->ln(2);
			$this->Write(0, $txt , '', 0, 'C', true, 0, false, false, 0);

			$this->Cell(50, 10, 'Hecho por: '.$strUsuario, 00, false, 'C', 0, '', 0, false, 'T', 'M');
			$this->Cell(30, 10, 'Impreso por: '.$strNick, 0, false, 'C', 0, '', 0, false, 'T', 'M');
			$this->Cell(30, 10, 'IP: '.$strIp, 0, false, 'C',0, '', 0, false, 'T', 'M');
			$this->Cell(30, 10, 'Fecha: '.$strFecha, 0, false, 'C', 0, '', 0, false, 'T', 'M');
			$this->Cell(30, 10, 'IDEAL.10 S.A.S', 0, false, 'C', 0, '', 0, false, 'T', 'M');
            $this->Cell(20, 10, 'Pagina '.$this->getAliasNumPage().'/'.$this->getAliasNbPages(), 0, false, 'C', 0, '', 0, false, 'T', 'M');
		}
	}

    if($_POST){
        $obj = new BalanceExportController();
        if($_POST['action'] == "excel"){
            $obj->exportExcel();
        }else if($_POST['action'] == "pdf"){
            $obj->exportPdf();
        }
    }

?>
