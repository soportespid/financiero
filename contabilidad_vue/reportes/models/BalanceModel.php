<?php
    require_once '../../../Librerias/core/Helpers.php';
    require_once '../../../Librerias/core/Mysql.php';
    session_start();
    /*ini_set('display_errors', '1');
    ini_set('display_startup_errors', '1');
    error_reporting(E_ALL);*/
    class BalanceModel extends Mysql{
        private $strFechaInicial;
        private $strFechaFinal;

        function __construct(){
            parent::__construct();
        }
        public function selectNiveles(){
            $sql = "SELECT * FROM nivelesctas WHERE estado='S' ORDER BY id_nivel";
            $request = $this->select_all($sql);
            return $request;
        }
        public function selectCentros(){
            $sql = "SELECT id_cc as codigo,nombre FROM centrocosto WHERE estado='S' AND  entidad='S'";
            $request = $this->select_all($sql);
            return $request;
        }
        public function selectCuentas(){
            $sql ="SELECT cuenta as codigo,nombre FROM cuentasnicsp WHERE  estado = 'S'";
            $request = $this->select_all($sql);
            return $request;
        }
        public function selectCentroExterno(){
            $sql = "SELECT codigo, base, nombre, usuario FROM redglobal WHERE tipo = 'EX' ORDER BY id";
            $request = $this->select_all($sql);
            return $request;
        }
        public function selectCriterios(){
            $arrCriterio = [];
            $strCriterio ="";
            $sql = "SELECT id_cc FROM centrocosto WHERE entidad='N'";
            $request = $this->select_all($sql);
            foreach ($request as $r) {
                $strCriterio.="AND comprobante_det.centrocosto <> ".$r['id_cc'];
                array_push($arrCriterio,$r['id_cc']);
            }
            return array("data"=>$arrCriterio,"str_data"=>$strCriterio);
        }
        public function genCentros($arrCentros){
            $result = "";
            if(count($arrCentros) > 0){
                $searchCentro = "";
                foreach ($arrCentros as $centro) {
                    $searchCentro.= "comprobante_det.centrocosto like '$centro'";
                }
                $result = "AND (".$searchCentro.")";
            }
            return $result;
        }
        public function selectBuscarCuentas($intNivel,$strCuentaInicial,$strCuentaFinal){
            $crit1 = " and left(cuenta,$intNivel)>='$strCuentaInicial' and left(cuenta,$intNivel)<='$strCuentaFinal' ";
            $sql = "SELECT DISTINCT cuenta,tipo FROM cuentasnicsp WHERE estado ='S'  AND length(cuenta)=$intNivel " . $crit1 . " group by cuenta,tipo order by cuenta ";
            $request = $this->select_all($sql);
            return $request;
        }
        public function selectMovimientosPeriodos($intNivel, $strFechaInicial, $strFechaFinal, $intAgregado, $intCierre, $intExterno,
        $strCentro,$arrCentros, $arrCuentas){
            $arrCriterios = $this->selectCriterios();
            $strGenCentros = $this->genCentros($arrCentros);
            $strCriterioCostos ="";
            $strCriterioCierre = $intCierre == 1 ? "" : ",13";
            $strFechaFinalAnterior = date("Y-m-d",strtotime($strFechaInicial)-(24 * 60 * 60));
            if(!empty($arrCriterios['data'])){
                $strCriterioCostos = "AND comprobante_det.centrocosto NOT IN (".implode(",",$arrCriterios['data']).")";
            }
            $arrBases = getBases();
            $arrConexiones = [];
            $arrData = [];
            $arrTempData=[];
            if($intAgregado != 1 && $intExterno != 1){
                $arrBases = [$arrBases[0]];
            }else if($intExterno == 1){
                $arrBases = array_filter($arrBases,function($e) use($strCentro){ return $e['codigo'] == $strCentro; });
            }
            foreach($arrBases as $base){
                $con = connectBase($base['base'], $base['usuario']);
                $arrConexiones[] = $con;
            }
            foreach ($arrCuentas as $cuenta) {
                //Movimientos del periodo
                $sql ="SELECT DISTINCT
                SUBSTR(comprobante_det.cuenta,1,$intNivel) as comprobante,
                sum(comprobante_det.valdebito) as debito,
                sum(comprobante_det.valcredito) as credito
                FROM comprobante_det
                RIGHT JOIN comprobante_cab ON comprobante_cab.tipo_comp = comprobante_det.tipo_comp
                AND comprobante_det.numerotipo = comprobante_cab.numerotipo
                WHERE
                comprobante_cab.estado = 1
                AND (comprobante_det.valdebito > 0 OR comprobante_det.valcredito > 0)
                AND comprobante_cab.fecha BETWEEN '$strFechaInicial' AND '$strFechaFinal'
                AND comprobante_det.tipo_comp NOT IN(7,100,102,101,103,19,104$strCriterioCierre)
                AND comprobante_det.cuenta LIKE '$cuenta[cuenta]%'
                GROUP BY SUBSTR(comprobante_det.cuenta,1,$intNivel)
                ORDER BY comprobante_det.cuenta
                ";

                //Saldo inicial
                $sqlSaldo="SELECT DISTINCT
                SUBSTR(comprobante_det.cuenta,1,$intNivel) as comprobante,
                COALESCE((sum(comprobante_det.valdebito)- sum(comprobante_det.valcredito)),0) as saldo
                FROM comprobante_det
                RIGHT JOIN comprobante_cab ON comprobante_cab.tipo_comp = comprobante_det.tipo_comp
				AND comprobante_det.numerotipo = comprobante_cab.numerotipo
				WHERE comprobante_cab.estado = 1
                AND (comprobante_det.valdebito > 0 OR comprobante_det.valcredito > 0) AND comprobante_det.tipo_comp = 102
                AND comprobante_det.tipo_comp <> 19
                AND comprobante_det.cuenta LIKE '$cuenta[cuenta]%'  $strCriterioCostos $strGenCentros
                GROUP BY SUBSTR(comprobante_det.cuenta,1,$intNivel) ORDER BY comprobante_det.cuenta";

                //Periodos
                $sqlPeriodo="SELECT DISTINCT
                SUBSTR(comprobante_det.cuenta,1,$intNivel) as comprobante,
                COALESCE((sum(comprobante_det.valdebito)- sum(comprobante_det.valcredito)),0) as saldo
                FROM comprobante_det
                RIGHT JOIN comprobante_cab ON comprobante_cab.tipo_comp = comprobante_det.tipo_comp
				AND comprobante_det.numerotipo = comprobante_cab.numerotipo
                WHERE comprobante_cab.estado = 1
                AND (comprobante_det.valdebito > 0 OR comprobante_det.valcredito > 0)
                AND comprobante_det.tipo_comp NOT IN(100,101,103,104,102,7,19) $strCriterioCostos $strGenCentros
                AND comprobante_cab.fecha BETWEEN '2018-01-01' AND '$strFechaFinalAnterior' AND comprobante_det.cuenta LIKE '$cuenta[cuenta]%'
                GROUP BY SUBSTR(comprobante_det.cuenta,1,$intNivel)
                ORDER BY comprobante_det.cuenta";

                foreach($arrConexiones as $con){
                    $movimiento = $con->select($sql);
                    if(!empty($movimiento)){
                        $movimiento['saldo'] = 0;
                        array_push($arrTempData,$movimiento);
                    }
                    $movimiento = $con->select($sqlSaldo);
                    if(!empty($movimiento)){
                        $movimiento['debito'] = 0;
                        $movimiento['credito'] = 0;
                        array_push($arrTempData,$movimiento);
                    }
                    if($strFechaFinalAnterior>='2018-01-01'){
                        $movimiento = $con->select($sqlPeriodo);
                        if(!empty($movimiento)){
                            $movimiento['debito'] = 0;
                            $movimiento['credito'] = 0;
                            array_push($arrTempData,$movimiento);
                        }
                    }
                }
            }

            foreach ($arrTempData as $data) {
                $totalData = count($arrData);
                if($totalData > 0){
                    $flag = false;
                    for ($i=0; $i < $totalData ; $i++) {
                        if($data['comprobante'] == $arrData[$i]['comprobante']){
                            $arrData[$i]['saldo']+=$data['saldo'];
                            $arrData[$i]['debito']+=$data['debito'];
                            $arrData[$i]['credito']+=$data['credito'];
                            $flag = true;
                            break;
                        }
                    }
                    if(!$flag){
                        array_push($arrData,$data);
                    }
                }else{
                    array_push($arrData,$data);
                }
            }
            return $arrData;
        }
    }
?>
