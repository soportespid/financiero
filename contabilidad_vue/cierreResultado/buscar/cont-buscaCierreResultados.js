/* import { createApp } from '../../../Librerias/vue3/dist/vue.esm-browser.js'

const DIR_ORIGIN = window.location.origin + '/';
const DIR_PATHNAME = window.location.pathname.split("/", 2)[1] + '/';
const DIR_FULL = DIR_ORIGIN + '/' + DIR_PATHNAME; */

const app = Vue.createApp({
    data() {
      return {
        detalles: [],
        numActo: '',
        loading: false,
      }
    },

    mounted() {
		  this.loading = false;
	  },

    computed: {
      existeInformacion(){
        const det = JSON.parse(JSON.stringify(this.detalles));
        return det.length > 0 ? true : false
      },

    },

    methods: {

      async buscarCierreResultados() {
        //console.log(JSON.parse(JSON.stringify(this.detallesAd)));
        this.loading = true;
        let fechaInicial = document.getElementById('fc_1198971545').value;
        let fechaFinal = document.getElementById('fc_1198971546').value;

        this.detalles = [];

        if(fechaInicial && fechaFinal){

          if(fechaInicial && fechaFinal){
            let arrFechaInicial = fechaInicial.split("/");
            fechaInicial = `${arrFechaInicial[2]}-${arrFechaInicial[1]}-${arrFechaInicial[0]}`;
            let arrFechaFinal = fechaFinal.split("/");
            fechaFinal = `${arrFechaFinal[2]}-${arrFechaFinal[1]}-${arrFechaFinal[0]}`;
          }else{
            fechaInicial = '';
            fechaFinal = '';
          }

          let formData = new FormData();
          formData.append("fechaInicial", fechaInicial);
          formData.append("fechaFinal", fechaFinal);

          const URL = `contabilidad_vue/cierreResultado/buscar/cont-buscarCierreResultados.php?action=buscarCierres`;

          await axios.post(URL, formData)
            .then((response) => {
              
              this.detalles = response.data.cierreResultados;
              
            }).finally(() => {
              this.loading =  false;
            });
            
        }else{

          Swal.fire(
              'Falta informaci&oacute;n para generar reporte.',
              'Verifique que la fecha inicial y final tengan informaci&oacute;n para realizar la busqueda.',
              'warning'
          );

        }

      },

      async deshacer(consecutivo, fechaCierre){

        Swal.fire({
          icon: 'question',
          title: '¿Seguro que quieres borrar el cierre?',
          showDenyButton: true,
          confirmButtonText: 'Borrar',
          confirmButtonColor: '#01CC42',
          denyButtonText: 'Cancelar',
          denyButtonColor: '#FF121A',
        }).then(
          (result) => {
            if (result.isConfirmed){

              const URL = `contabilidad_vue/cierreResultado/buscar/cont-buscarCierreResultados.php?action=borrarCierre&consecutivo=${consecutivo}&fechaCierre=${fechaCierre}`;

              axios.post(URL)
              .then((response) => {
                if(response.data.tienePermiso == 'Si'){
                  Swal.fire({
                    icon: 'success',
                    title: 'Se borro con exito el comprobante de cierre '+consecutivo,
                    confirmButtonText: 'Continuar',
                    confirmButtonColor: '#FF121A',
                    timer: 3000
                  }).then(
                    (rsp)=>{
                      if (rsp.isConfirmed){
                        this.buscarCierreResultados();
                      }
                      
                    }
                  );
                }else{
                  Swal.fire({
                    icon: 'info',
                    title: 'No Tiene los Permisos para Modificar este Documento.',
                    confirmButtonText: 'Continuar',
                    confirmButtonColor: '#FF121A',
                    timer: 2500
                  });
                }
                
              });
            }
            else if (result.isDenied){
              Swal.fire({
                icon: 'info',
                title: 'No se borró el cierre',
                confirmButtonText: 'Continuar',
                confirmButtonColor: '#FF121A',
                timer: 2500
              });
            }
          }
        )

      },

      toFormData(obj){
        var form_data = new FormData();
        for(var key in obj){
            form_data.append(key, obj[key]);
        }
        return form_data;
      },

      comprobarEstado(estado){
        switch(estado){
          case '1':
            return 'Activo'
          break;
          case '0':
            return 'Anulado'
          break;
          case '2':
            return 'Reversado'
          break;
        }
      },

      formatonumero: function(valor){
        return new Intl.NumberFormat("en-US", {style: "currency", currency: "USD"}).format(valor);
      },

    }
})
  
app.mount('#myapp')