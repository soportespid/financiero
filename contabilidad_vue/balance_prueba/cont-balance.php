<?php
    /*ini_set('display_errors', '1');
    ini_set('display_startup_errors', '1');
    error_reporting(E_ALL);*/
    date_default_timezone_set("America/Bogota");
    require_once '../../comun.inc';
    require '../../funciones.inc';
    require '../../funcionesSP.inc.php';
    require '../../conversor.php';

    session_start();
    if($_POST){
        $obj = new Plantilla();
        if($_POST['action']=="get"){
            $obj->getData();
        }else if($_POST['action']=="calcular"){
            $obj->calcular();
        }
    }

    class Plantilla{
        private $linkbd;
        public function __construct() {
            $this->linkbd = conectar_v7();
            $this->linkbd->set_charset("utf8");
        }
        public function getData(){
            if(!empty($_SESSION)){
                $request =$this->selectData();
                echo json_encode($request,JSON_UNESCAPED_UNICODE);
            }
            die();
        }
        public function calcular(){
            if(!empty($_SESSION)){
                $arrInfo = [];
                $arrData = json_decode($_POST['data'],true);
                $strFechaInicial = $_POST['fecha_inicial'];
                $strFechaFinal = $_POST['fecha_final'];
                $strDestino = $_POST['destino'];
                $parametros = $this->selectParametros();
                $totalData= count($arrData);
                for ($i=0; $i < $totalData ; $i++) {
                    $data = $arrData[$i];
                    if($data['sigla']=="R"){
                        if($data['id']=="34"){
                            $arrRete34 = $this->selectRete34($strFechaInicial,$strFechaFinal,$data);
                            array_push($arrInfo,$arrRete34);
                        }
                        $arrOrden = $this->selectOrdenPago($strFechaInicial,$strFechaFinal,$data,$strDestino,$parametros);
                        $arrPagoTerceros = $this->selectPagoTerceros($strFechaInicial,$strFechaFinal,$data,$strDestino);
                        if(!empty($arrOrden)){
                            array_push($arrInfo,$arrOrden);
                        }
                        if(!empty($arrPagoTerceros)){
                            array_push($arrInfo,$arrPagoTerceros);
                        }
                    }else if($data['sigla']=="I"){
                        $arrRecaudo = $this->selectRecaudos($strFechaInicial,$strFechaFinal,$data);
                        $arrRecaudoPropio = $this->selectRecaudosPropios($strFechaInicial,$strFechaFinal,$data);
                        $arrRecaudoTransferencia = $this->selectRecaudosTransferencias($strFechaInicial,$strFechaFinal,$data);
                        if(!empty($arrRecaudo)){
                            array_push($arrInfo,$arrRecaudo);
                        }
                        if(!empty($arrRecaudoPropio)){
                            array_push($arrInfo,$arrRecaudoPropio);
                        }
                        if(!empty($arrRecaudoTransferencia)){
                            array_push($arrInfo,$arrRecaudoTransferencia);
                        }
                    }
                }
                $arrInfo = $this->fixArrayInfo($arrInfo);
                $arrBanks = $this->filterByBanks($arrInfo['data']);
                $arrBanksDet = $this->filterByBanksDet($arrInfo['data']);
                $arrResponse = array("data"=>$arrInfo,"html"=>$this->getHtmlDetail($arrBanks),"html_bancos"=>$this->getHtmlBancos($arrBanksDet));
                echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
            }
            die();
        }
        public function filterByBanks($data){
            $arrData = [];
            $total = count($data);
            for ($i=0; $i < $total ; $i++) {
                $totalData = count($arrData);
                if($totalData > 0){
                    $flag = false;
                    for ($j=0; $j < $totalData ; $j++) {
                        if($data[$i]['cuenta_banco'] == $arrData[$j]['cuenta_banco'] &&
                        $data[$i]['nombre'] == $arrData[$j]['nombre']){
                            $arrData[$j]['cuentas']+=$data[$i]['cuentas'];
                            $arrData[$j]['valor']+=$data[$i]['valor'];
                            $flag = true;
                            break;
                        }
                    }
                    if(!$flag){
                        array_push($arrData,$data[$i]);
                    }
                }else{
                    array_push($arrData,$data[$i]);
                }
            }
            return $arrData;
        }
        public function filterByBanksDet($data){
            $arrData = [];
            $total = count($data);
            for ($i=0; $i < $total ; $i++) {
                $totalData = count($arrData);
                if($totalData > 0){
                    $flag = false;
                    for ($j=0; $j < $totalData ; $j++) {
                        if($data[$i]['cuenta_banco'] == $arrData[$j]['cuenta_banco']){
                            $arrData[$j]['cuentas']+=$data[$i]['cuentas'];
                            $arrData[$j]['valor']+=$data[$i]['valor'];
                            $flag = true;
                            break;
                        }
                    }
                    if(!$flag){
                        array_push($arrData,$data[$i]);
                    }
                }else{
                    array_push($arrData,$data[$i]);
                }
            }
            usort($arrData,function($a,$b){return $a['tercero_banco'] > $b['tercero_banco'];});
            return $arrData;
        }
        public function fixArrayInfo(array $arrInfo){
            $arrData = [];
            $total = 0;
            $cuentas = 0;
            foreach ($arrInfo as $cab) {
                foreach ($cab as $det) {
                    if($det['cuentas']>0){
                        $total+=$det['valor'];
                        $cuentas+=$det['cuentas'];
                        array_push($arrData,$det);
                    }
                }
            }
            return array(
                "total_cuentas"=>$cuentas,
                "total_valor"=>$total,
                "total_valor_letras"=>convertir($total)." PESOS M/CTE",
                "data"=>$arrData
            );
        }
        public function getHtmlBancos($data){
            $total = count($data);
            $retencion = !empty($data) ? $data[0]['tercero_banco'] : "";
            $acumulador = 0;
            $baseTotalRete = 0;
            $html ="";
            for ($i=0; $i < $total ; $i++) {
                $row = $data[$i];
                if($retencion != $row['tercero_banco']){
                    $html.= '
                    <tr class = "bg-success text-white">
                        <td colspan = "2" > Total de '.$retencion.'</td>
                        <td class="text-right">'.number_format(round($baseTotalRete,0),0).'</td>
                        <td class="text-right">'.number_format(round($acumulador,0),0).'</td>
                    </tr>';
                    $acumulador = 0;
                    $baseTotalRete = 0;
                }
                $html.='
                    <tr>
                        <td>'.$row['cuenta_banco'].'</td>
                        <td>'.$row['tercero_banco'].'</td>
                        <td class="text-right">'.number_format($row['cuentas']).'</td>
                        <td class="text-right">'.number_format($row['valor']).'</td>
                    </tr>';
                $retencion = $row['tercero_banco'];
                $acumulador += round($row['valor'],0);
                $baseTotalRete += round($row['cuentas'],0);
                if($i == $total - 1){
                    $html.= '
                    <tr class = "bg-success text-white" >
                        <td colspan = "2" >Total de '.$retencion.'</td>
                        <td class="text-right">'.number_format(round($baseTotalRete,0),0).'</td>
                        <td class="text-right">'.number_format(round($acumulador,0),0).'</td>
                    </tr>';
                    $acumulador = 0;
                    $baseTotalRete = 0;
                }
            }
            return $html;
        }
        public function getHtmlDetail($data){
            $total = count($data);
            $retencion = !empty($data) ? $data[0]['nombre_descuentos'] : "";
            $acumulador = 0;
            $baseTotalRete = 0;
            $html ="";
            for ($i=0; $i < $total ; $i++) {
                $row = $data[$i];
                if($retencion != $row['nombre_descuentos']){
                    $html.= '
                    <tr class = "bg-success text-white">
                        <td colspan = "3" > Total de retencion/ingreso '.$retencion.'</td>
                        <td>'.number_format(round($baseTotalRete,0),0).'</td>
                        <td>'.number_format(round($acumulador,0),0).'</td>
                    </tr>';
                    $acumulador = 0;
                    $baseTotalRete = 0;
                }
                $html.='
                    <tr>
                        <td>'.$row['nombre'].'</td>
                        <td>'.$row['cuenta_banco'].'</td>
                        <td>'.$row['tercero_banco'].'</td>
                        <td>'.number_format($row['cuentas']).'</td>
                        <td>'.number_format($row['valor']).'</td>
                    </tr>';
                $retencion = $row['nombre_descuentos'];
                $acumulador += round($row['valor'],0);
                $baseTotalRete += round($row['cuentas'],0);
                if($i == $total - 1){
                    $html.= '<tr class = "bg-success text-white" >
                            <td colspan = "3" >
                                Total de retencion/ingreso '.$retencion.'
                            </td>
                            <td>
                                '.number_format(round($baseTotalRete,0),0).'
                            </td>
                            <td>
                                '.number_format(round($acumulador,0),0).'
                            </td>
                        </tr>';
                    $acumulador = 0;
                    $baseTotalRete = 0;
                }
            }
            return $html;
        }
        public function selectRecaudosTransferencias($strFechaInicial,$strFechaFinal,$data){
            $arrData = [];
            $sql="SELECT DISTINCT td.ingreso,
            sum(td.valor) as valor,
            tr.banco as tercero_banco,
            tr.ncuentaban as cuenta_banco,
            tr.valortotal,
            tr.id_recaudo,
            tr.tercero
            FROM tesorecaudotransferencia_det td
            LEFT JOIN tesorecaudotransferencia tr ON td.id_recaudo=tr.id_recaudo
            WHERE tr.estado='S' AND tr.fecha BETWEEN '$strFechaInicial' AND '$strFechaFinal'
            AND td.ingreso='$data[codigo]' GROUP BY td.ingreso";
            $request = mysqli_fetch_all(mysqli_query($this->linkbd,$sql),MYSQLI_ASSOC);
            $total = count($request);
            for ($i=0; $i < $total; $i++) {
                $element = [];
                $arrRecaudo = $request[$i];
                $sql_ingreso="SELECT * FROM  tesoingresos_det WHERE codigo='$arrRecaudo[ingreso]'
                AND vigencia=(SELECT MAX(vigencia) FROM tesoingresos_det WHERE codigo = '$arrRecaudo[ingreso]')";
                $request_ingreso = mysqli_fetch_all(mysqli_query($this->linkbd,$sql_ingreso),MYSQLI_ASSOC);
                $totalRecaudo = count($request_ingreso);
                for ($j=0; $j < $totalRecaudo ; $j++) {
                    $arrIngreso = $request_ingreso[$j];
                    $sql_concepto = "SELECT * FROM conceptoscontables_det WHERE codigo='$arrIngreso[concepto]'
                    AND modulo='4' AND tipo='C' AND fechainicial < '$strFechaFinal' ORDER BY fechainicial DESC";
                    $arrConcepto = mysqli_fetch_all(mysqli_query($this->linkbd,$sql_concepto),MYSQLI_ASSOC)[0];
                    if(substr($arrConcepto['cuenta'],0,1)=='2'){
                        $porcentaje = $arrIngreso['porcentaje'];
                        $element['nombre'] = $data['nombre'];
                        $element['valor'] = $arrRecaudo['valor']*($porcentaje/100);
                        $element['cuentas'] = $arrRecaudo['valor'];
                        $element['bancos'] = $arrRecaudo['id_recibos'];
                        $element['cuenta_bancos'] = $arrRecaudo['id_recaudo'];
                        $element['tercero'] = $arrRecaudo['tercero'];
                        $element['nombre_tercero'] = $this->selectTercero($arrRecaudo['tercero']);
                        $element['descuentos'] = $arrRecaudo['ingreso'];
                        $element['nombre_descuentos'] = buscaingreso($arrRecaudo['ingreso']);
                        $element['cuenta_banco'] = $arrRecaudo['cuenta_banco'];
                        $element['tercero_banco'] = $this->selectTercero($arrRecaudo['tercero_banco']);
                        array_push($arrData,$element);
                    }
                }
            }
            return $arrData;
        }
        public function selectRecaudosPropios($strFechaInicial,$strFechaFinal,$data){
            $arrData = [];
            $sql="SELECT DISTINCT td.ingreso,
            sum(td.valor) as valor,
            trc.cuentabanco,
            trc.cuentacaja,
            trc.id_recibos,
            tr.tercero,
            tc.ncuentaban as cuenta_banco,
            tc.tercero as tercero_banco
            FROM tesosinreciboscaja_det td
            LEFT JOIN tesosinreciboscaja trc ON td.id_recibos=trc.id_recibos
            LEFT JOIN tesosinrecaudos tr ON trc.id_recaudo=tr.id_recaudo
            LEFT JOIN tesobancosctas as tc ON tc.cuenta = trc.cuentabanco
            WHERE trc.estado='S' AND trc.fecha BETWEEN '$strFechaInicial' AND '$strFechaFinal'
            AND td.ingreso='$data[codigo]' GROUP BY trc.id_recibos";
            $request = mysqli_fetch_all(mysqli_query($this->linkbd,$sql),MYSQLI_ASSOC);
            $total = count($request);
            for ($i=0; $i < $total; $i++) {
                $element = [];
                $arrRecaudo = $request[$i];
                $sql_ingreso="SELECT * FROM  tesoingresos_det WHERE codigo='$arrRecaudo[ingreso]'
                AND vigencia=(SELECT MAX(vigencia) FROM tesoingresos_det WHERE codigo = '$arrRecaudo[ingreso]')";
                $request_ingreso = mysqli_fetch_all(mysqli_query($this->linkbd,$sql_ingreso),MYSQLI_ASSOC);
                $totalRecaudo = count($request_ingreso);
                for ($j=0; $j < $totalRecaudo ; $j++) {
                    $arrIngreso = $request_ingreso[$j];
                    $sql_concepto = "SELECT * FROM conceptoscontables_det WHERE codigo='$arrIngreso[concepto]'
                    AND modulo='4' AND tipo='C' AND fechainicial < '$strFechaFinal' ORDER BY fechainicial DESC";
                    $arrConcepto = mysqli_fetch_all(mysqli_query($this->linkbd,$sql_concepto),MYSQLI_ASSOC)[0];
                    if(substr($arrConcepto['cuenta'],0,1)=='2' || substr($arrConcepto['cuenta'],0,1)=='4' ||
                    substr($arrConcepto['cuenta'],0,1) == 3 || substr($arrConcepto['cuenta'],0,1) == 1){
                        $porcentaje = $arrIngreso['porcentaje'];
                        $element['nombre'] = $data['nombre'];
                        $element['valor'] = $arrRecaudo['valor']*($porcentaje/100);
                        $element['cuentas'] = $arrRecaudo['valor'];
                        $element['bancos'] = $arrRecaudo['id_recibos'];
                        $element['cuenta_bancos'] = $arrRecaudo['id_recaudo'];
                        $element['tercero'] = $arrRecaudo['tercero'];
                        $element['nombre_tercero'] = $this->selectTercero($arrRecaudo['tercero']);
                        $element['descuentos'] = $arrRecaudo['ingreso'];
                        $element['nombre_descuentos'] = buscaingreso($arrRecaudo['ingreso']);
                        $element['cuenta_banco'] = $arrRecaudo['cuenta_banco'];
                        $element['tercero_banco'] = $this->selectTercero($arrRecaudo['tercero_banco']);
                        array_push($arrData,$element);
                        break;
                    }
                }
            }
            return $arrData;
        }
        public function selectRecaudos($strFechaInicial,$strFechaFinal,$data){
            $arrData = [];
            $sql="SELECT trd.ingreso,
            trd.valor,
            trc.cuentabanco,
            trc.cuentacaja,
            trc.id_recibos,
            tr.id_recaudo,
            tr.tercero,
            tc.ncuentaban as cuenta_banco,
            tc.tercero as tercero_banco
            FROM tesorecaudos tr
            LEFT JOIN tesorecaudos_det trd ON tr.id_recaudo=trd.id_recaudo
            LEFT JOIN tesoreciboscaja trc ON tr.id_recaudo=trc.id_recaudo
            LEFT JOIN tesobancosctas as tc ON tc.cuenta = trc.cuentabanco
            WHERE tr.estado='P' AND trc.fecha BETWEEN '$strFechaInicial' AND '$strFechaFinal'
            AND trd.ingreso='$data[codigo]' AND trc.tipo=3";
            $request = mysqli_fetch_all(mysqli_query($this->linkbd,$sql),MYSQLI_ASSOC);
            $total = count($request);
            for ($i=0; $i < $total; $i++) {
                $element = [];
                $arrRecaudo = $request[$i];
                $sql_ingreso="SELECT * FROM  tesoingresos_det WHERE codigo='$arrRecaudo[ingreso]'
                AND vigencia=(SELECT MAX(vigencia) FROM tesoingresos_det WHERE codigo = '$arrRecaudo[ingreso]')";
                $request_ingreso = mysqli_fetch_all(mysqli_query($this->linkbd,$sql_ingreso),MYSQLI_ASSOC);
                $totalRecaudo = count($request_ingreso);
                for ($j=0; $j < $totalRecaudo ; $j++) {
                    $arrIngreso = $request_ingreso[$j];
                    $sql_concepto = "SELECT * FROM conceptoscontables_det WHERE codigo='$arrIngreso[concepto]'
                    AND modulo='4' AND tipo='C' AND fechainicial < '$strFechaFinal' ORDER BY fechainicial DESC";
                    $arrConcepto = mysqli_fetch_all(mysqli_query($this->linkbd,$sql_concepto),MYSQLI_ASSOC)[0];
                    if(substr($arrConcepto['cuenta'],0,1)=='2'){
                        $porcentaje = $arrIngreso['porcentaje'];
                        $element['nombre'] = $data['nombre'];
                        $element['valor'] = $arrRecaudo['valor']*($porcentaje/100);
                        $element['cuentas'] = $arrRecaudo['valor'];
                        $element['bancos'] = $arrRecaudo['id_recibos'];
                        $element['cuenta_bancos'] = $arrRecaudo['id_recaudo'];
                        $element['tercero'] = $arrRecaudo['tercero'];
                        $element['nombre_tercero'] = $this->selectTercero($arrRecaudo['tercero']);
                        $element['cuenta_banco'] = $arrRecaudo['cuenta_banco'];
                        $element['tercero_banco'] = $this->selectTercero($arrRecaudo['tercero_banco']);
                        $element['descuentos'] = $arrRecaudo['ingreso'];
                        $element['nombre_descuentos'] = buscaingreso($arrRecaudo['ingreso']);
                        array_push($arrData,$element);
                    }
                }
            }
            return $arrData;
        }
        public function selectPagoTerceros($strFechaInicial,$strFechaFinal,$data,$strDestino){
            $arrData = [];
            $sql="SELECT tr.id_retencion,
            tr.valor,
            tp.banco,
            tp.id_pago,
            tr.valor,
            tp.tercero,
            tc.ncuentaban as cuenta_banco,
            tc.tercero as tercero_banco
            FROM tesopagotercerosvigant_retenciones AS tr
            LEFT JOIN tesopagotercerosvigant AS tp ON tp.id_pago=tr.id_egreso
            LEFT JOIN tesobancosctas as tc ON tp.cuenta = tp.banco
            WHERE  tp.estado='S' AND tp.fecha BETWEEN '$strFechaInicial' AND '$strFechaFinal'
            AND tr.id_retencion='$data[id]' ORDER BY tp.id_pago";
            $request = mysqli_fetch_all(mysqli_query($this->linkbd,$sql),MYSQLI_ASSOC);
            $condicion = $data['tipo'] == "C" ? "AND destino='$strDestino' AND conceptoingreso!= '-1'" : "";
            $total = count($request);
            for ($i=0; $i < $total ; $i++) {
                $element = [];
                $arrOrden = $request[$i];
                $sql_ret="SELECT * FROM tesoretenciones_det WHERE codigo='$arrOrden[id_retencion]' $condicion ORDER BY porcentaje DESC";
                $request_ret = mysqli_fetch_all(mysqli_query($this->linkbd,$sql_ret),MYSQLI_ASSOC);
                $totalRet = count($request_ret);
                for ($j=0; $j < $totalRet ; $j++) {
                    $arrRet = $request_ret[$j];
                    if($arrRet['tipo'] == 'C' && $arrRet['destino'] != $strDestino){
                        continue;
                    }
                    $porcentaje = $arrRet['porcentaje'];
                    $element['nombre'] = $data['nombre'];
                    $element['valor'] = $arrOrden['valor'];
                    $element['cuentas'] = $arrOrden['base'];
                    $element['bancos'] = $arrOrden['id_orden'];
                    $element['cuenta_bancos'] = "CXP";
                    $element['cuenta_banco'] = $arrOrden['cuenta_banco'];
                    $element['tercero_banco'] = $this->selectTercero($arrOrden['tercero_banco']);
                    $element['tercero'] = $arrOrden['tercero'];
                    $element['nombre_tercero'] = $this->selectTercero($arrOrden['tercero']);
                    $element['descuentos'] = $arrOrden['id_retencion'];
                    $element['nombre_descuentos'] = buscaretencion($arrOrden['id_retencion']);
                    if($data['tipo'] == "C"){
                        if($porcentaje <= 100){
                            $element['valor'] = $arrOrden['valor']*($porcentaje/100);
                        }
                    }
                    array_push($arrData,$element);
                }
            }
            return $arrData;
        }
        public function selectOrdenPago($strFechaInicial,$strFechaFinal,$data,$strDestino,$parametros){
            $arrData = [];
            if($parametros == 1){
                $sql="SELECT tr.id_retencion,
                tr.valor,
                tp.base,
                tp.iva,
                tp.id_orden,
                tp.tercero,
                tc.tercero as tercero_banco,
                te.cuentabanco as cuenta_banco
                FROM tesoordenpago as tp
                LEFT JOIN tesoordenpago_retenciones as tr ON tp.id_orden=tr.id_orden
                LEFT JOIN tesoegresos as te ON tp.id_orden = te.id_orden
                LEFT JOIN tesobancosctas as tc ON tc.ncuentaban = te.cuentabanco
                WHERE tp.fecha BETWEEN '$strFechaInicial' AND '$strFechaFinal' AND tr.id_retencion='$data[id]'
                AND tp.estado!='R' AND tp.estado!='N' AND tp.tipo_mov='201' AND te.estado='S' AND te.tipo_mov='201' ORDER BY tp.id_orden";

            }else{
                $sql="SELECT tr.id_retencion,
                tr.valor,
                te.banco,
                te.cuentabanco as cuenta_banco,
                te.id_egreso,
                tp.base,
                tp.iva,
                te.tercero,
                tc.tercero as tercero_banco
                FROM tesoordenpago AS tp
                LEFT JOIN tesoordenpago_retenciones as tr ON tp.id_orden=tr.id_orden
                LEFT JOIN tesoegresos as te ON tp.id_orden = te.id_orden
                LEFT JOIN tesobancosctas as tc ON tc.ncuentaban = te.cuentabanco
                WHERE te.estado='S' AND tp.estado!='N' AND te.fecha BETWEEN '$strFechaInicial' AND '$strFechaFinal'
                AND tr.id_retencion='$data[id]' AND te.estado='S' AND te.tipo_mov='201' AND tp.tipo_mov='201' ORDER BY te.id_egreso";
            }
            //dep($sql);exit;
            $request = mysqli_fetch_all(mysqli_query($this->linkbd,$sql),MYSQLI_ASSOC);
            $total = count($request);
            for ($i=0; $i < $total ; $i++) {
                $element = [];
                $arrOrden = $request[$i];
                if($parametros == 1){
                    $condicion = $data['tipo'] == "C" ? "AND destino='$strDestino' AND conceptoingreso!= '-1'" : "";
                    $sql_ret="SELECT * FROM tesoretenciones_det WHERE codigo='$arrOrden[id_retencion]' $condicion ORDER BY porcentaje DESC";
                }else{
                    $condicion = $data['tipo'] == "C" ? "AND tesoretenciones_det.destino='$strDestino'" : "";
                    $sql_ret="SELECT * FROM tesoretenciones, tesoretenciones_det  WHERE tesoretenciones_det.codigo='$arrOrden[id_retencion]'
                    AND tesoretenciones.id=tesoretenciones_det.codigo $condicion ORDER BY porcentaje DESC";
                }

                $request_ret = mysqli_fetch_all(mysqli_query($this->linkbd,$sql_ret),MYSQLI_ASSOC);
                $totalRet = count($request_ret);
                for ($j=0; $j < $totalRet ; $j++) {
                    $arrRet = $request_ret[$j];
                    if($parametros !=1 && $arrRet['tipo'] == 'C' && $arrRet['destino'] != $strDestino){
                        continue;
                    }
                    $porcentaje = $arrRet['porcentaje'];
                    $element['nombre'] = $data['nombre'];
                    $element['valor'] = $arrOrden['valor'];
                    $element['cuentas'] = $arrOrden['base'];
                    $element['bancos'] = $arrOrden['id_orden'];
                    $element['cuenta_bancos'] = "CXP";
                    $element['tercero'] = $arrOrden['tercero'];
                    $element['cuenta_banco'] = $arrOrden['cuenta_banco'];
                    $element['tercero_banco'] = $this->selectTercero($arrOrden['tercero_banco']);
                    $element['nombre_tercero'] = $this->selectTercero($arrOrden['tercero']);
                    $element['descuentos'] = $arrOrden['id_retencion'];
                    $element['nombre_descuentos'] = buscaretencion($arrOrden['id_retencion']);
                    if($data['tipo'] == "C"){
                        if($porcentaje <= 100){
                            $element['valor'] = $arrOrden['valor']*($porcentaje/100);
                        }
                    }
                    array_push($arrData,$element);
                }
            }
            return $arrData;
        }
        public function selectRete34($strFechaInicial,$strFechaFinal,$data){
            $arrData = [];
            $arrFechaInicial = explode("-",$strFechaInicial);
            $arrFechaFinal = explode("-",$strFechaFinal);
            $strFechaInicial = $arrFechaInicial[0]."-".$arrFechaInicial[1]."-01";
            $strFechaFinal = $arrFechaFinal[0]."-".$arrFechaFinal[1]."-31";
            $sql="SELECT COALESCE(SUM(valorretencion),0) as total
            FROM hum_retencionesfun
            WHERE tiporetencion='34' AND fecha BETWEEN '$strFechaInicial' AND '$strFechaFinal' AND estadopago='N'";
            $request = mysqli_query($this->linkbd,$sql)->fetch_assoc()['total'];
            $arrData['nombre'] = $data['nombre'];
            $arrData['valor'] = $request;
            $arrData['cuentas'] = 0;
            $arrData['bancos'] = "";
            $arrData['cuenta_bancos'] = "EGRESO NOMINA";
            $arrData['tercero'] = "";
            $arrData['tercero_banco'] = "";
            $arrData['cuenta_banco'] = "";
            $arrData['nombre_tercero'] = "";
            $arrData['descuentos'] = 35;
            $arrData['nombre_descuentos'] = buscaretencion(34);
            return [$arrData];
        }
        public function selectParametros(){
            $sql="SELECT conta_pago FROM tesoparametros";
            $request = mysqli_query($this->linkbd,$sql)->fetch_assoc()['conta_pago'];
            return $request;
        }
        public function selectData(){
            $arrRetenciones = $this->selectRetenciones();
            $arrIngresos = $this->selectIngresos();
            $arrData = array_merge($arrRetenciones,$arrIngresos);
            return $arrData;
        }
        public function selectRetenciones(){
            $sql = "SELECT id,codigo,nombre,tipo,destino FROM tesoretenciones WHERE estado = 'S' ORDER BY codigo";
            $request = mysqli_fetch_all(mysqli_query($this->linkbd,$sql),MYSQLI_ASSOC);
            $total = count($request);
            if($total > 0){
                for ($i=0; $i < $total ; $i++) {
                    $request[$i]['opcion_nombre'] = "Retención";
                    $request[$i]['sigla'] = "R";
                    $request[$i]['todo'] = "T";
                }
            }
            return $request;
        }
        public function selectIngresos(){
            $sql = "SELECT codigo,nombre,tipo,terceros as destino FROM tesoingresos WHERE estado = 'S' ORDER BY codigo";
            $request = mysqli_fetch_all(mysqli_query($this->linkbd,$sql),MYSQLI_ASSOC);
            $total = count($request);
            if($total > 0){
                for ($i=0; $i < $total ; $i++) {
                    $request[$i]['opcion_nombre'] = "Ingreso";
                    $request[$i]['sigla'] = "I";
                    $request[$i]['todo'] = "T";
                }
            }
            return $request;
        }
        public function selectTercero($tercero){
            $sql="SELECT id_tercero,cedulanit as documento,direccion,tipodoc as tipo_documento,telefono,celular,email as correo,persona as tipo, regimen,
            CASE WHEN razonsocial IS NULL OR razonsocial = ''
            THEN CONCAT(nombre1,' ',nombre2,' ',apellido1,' ',apellido2)
            ELSE razonsocial END AS nombre FROM terceros WHERE cedulanit = '$tercero'";
            $request = mysqli_query($this->linkbd,$sql)->fetch_assoc();
            $nombre ="";
            if(!empty($request)){
                $nombre = $request['nombre'];
            }
            return $nombre;
        }
    }
?>
