<?php
    require_once '../../../PHPExcel/Classes/PHPExcel.php';
    require_once '../../../comun.inc';
    require '../../../funciones.inc';
    require '../../../funcionesSP.inc.php';
    /*ini_set('display_errors', '1');
    ini_set('display_startup_errors', '1');
    error_reporting(E_ALL);*/
    session_start();
    if($_POST){
        $obj = new Plantilla();
        if($_POST['action']=="get"){
            $obj->getData();
        }else if($_POST['action']=="save"){
            $obj->save();
        }
    }

    class Plantilla{
        private $linkbd;
        private $arrData;
        private $strVigencia;
        public function __construct() {
            $this->linkbd = conectar_v7();
            $this->linkbd->set_charset("utf8");
        }
        public function save(){
            if(!empty($_SESSION)){
                if(empty($_POST['nombre']) || empty($_POST['codigo'])){
                    $arrResponse = array("status"=>false,"msg"=>"Error de datos");
                }else{
                    $request = $this->insertData($_POST['codigo'],$_POST['nombre']);
                    if(is_numeric($request) && $request > 0){
                        $arrResponse = array("status"=>true,"msg"=>"Datos guardados.");
                    }else if($request =="existe"){
                        $arrResponse = array("status"=>false,"msg"=>"El formato ya existe, pruebe con otro.");
                    }else{
                        $arrResponse = array("status"=>false,"msg"=>"Error");
                    }
                }
                echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
            }
            die();
        }
        public function insertData($formato,$nombre){
            $sql = "SELECT * FROM contexogenaformatos WHERE formato = '$formato'";
            $request = mysqli_fetch_all(mysqli_query($this->linkbd,$sql),MYSQLI_ASSOC);
            if(empty($request)){
                $sql ="INSERT INTO contexogenaformatos(formato,nombre)
                VALUES('$formato','$nombre')";
                $request = intval(mysqli_query($this->linkbd,$sql));
            }else{
                $request = "existe";
            }
            return $request;
        }
    }
?>
