const URL ='contabilidad_vue/exogena_formatos/crear/cont-formatosCrear.php';

var app = new Vue({
    el:"#myapp",
    data() {
        return {
            isLoading:false,
            txtCodigo:"",
            txtNombre:"",
        }
    },
    mounted() {
    },
    methods: {
        save: async function(){
            const vueContext = this;
            if(this.txtCodigo =="" || this.txtNombre ==""){
                Swal.fire("Error","Todos los campos con (*) son obligatorios","error");
                return false;
            }
            let formData = new FormData();
            formData.append("action","save");
            formData.append("codigo",this.txtCodigo);
            formData.append("nombre",this.txtNombre);

            Swal.fire({
                title:"¿Estás segur@ de guardar?",
                text:"",
                icon: 'warning',
                showCancelButton:true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText:"Sí, guardar",
                cancelButtonText:"No, cancelar"
            }).then(async function(result){
                if(result.isConfirmed){
                    let response = await fetch(URL,{method:"POST",body:formData});
                    let objData = await response.json();
                    if(objData.status){
                        vueContext.txtCodigo = "";
                        vueContext.txtNombre = "";
                        Swal.fire("Guardado",objData.msg,"success");
                    }else{
                        Swal.fire("Error",objData.msg,"error");
                    }
                }
            });
        },
    },
    computed:{

    }
})
