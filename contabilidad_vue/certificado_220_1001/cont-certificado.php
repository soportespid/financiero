<?php
    require_once '../../comun.inc';
    require '../../funciones.inc';
    require '../../funcionesSP.inc.php';
    /*ini_set('display_errors', '1');
    ini_set('display_startup_errors', '1');
    error_reporting(E_ALL);*/
    session_start();
    //dep($_POST);exit;
    if($_POST){
        $obj = new Plantilla();
        if($_POST['action']=="get"){
            $obj->getData();
        }
    }

    class Plantilla{
        private $linkbd;
        private $arrData;
        public function __construct() {
            $this->linkbd = conectar_v7();
            $this->linkbd->set_charset("utf8");
        }
        public function getData(){
            if(!empty($_SESSION)){
                $request['exogenas'] = $this->selectExogenas();
                $request['retenedor'] = $this->selectRetenedor();
                echo json_encode($request,JSON_UNESCAPED_UNICODE);
            }
            die();
        }
        public function selectRetenedor(){
            $sql="SELECT * FROM configbasica WHERE estado='S'";
            $request = mysqli_query($this->linkbd,$sql)->fetch_assoc();
            return $request;
        }
        public function selectExogenas(){
            $sql = "SELECT id_exo as id, vigencia,descripcion FROM exogena_cab WHERE estado='S' ORDER BY id_exo DESC";
            $request = mysqli_fetch_all(mysqli_query($this->linkbd,$sql),MYSQLI_ASSOC);
            $total = count($request);
            for ($i=0; $i < $total ; $i++) {
                $sql_det = "SELECT
                det.concepto,
                det.tercero as documento,
                t.nombre1 as primer_nombre,
                t.nombre2 as segundo_nombre,
                t.apellido1 as primer_apellido,
                t.apellido2 as segundo_apellido,
                t.tipodoc as tipo_documento,
                t.depto as cod_dpto,
                t.mnpio as cod_mnpo,
                SUM(det.valor) as valor,
                CONCAT(t.nombre1,' ',t.nombre2,' ',t.apellido1,' ',t.apellido2) as nombre
                FROM exogena_det det
                INNER JOIN terceros t
                ON t.cedulanit = det.tercero
                WHERE det.id_exo = '{$request[$i]['id']}'
                GROUP BY det.tercero,det.concepto";
                $terceros =mysqli_fetch_all(mysqli_query($this->linkbd,$sql_det),MYSQLI_ASSOC);
                $request[$i]['terceros'] = $this->configTerceros($terceros,$request[$i]['vigencia']);
            }
            return $request;
        }
        public function configTerceros($terceros,$vigencia){
            $arrNewTerceros = [];
            $arrNeedles = ["aporte","aportes","valor","tipo","identificacion","apoyo","apoyos","salud","obligatorios","fondos","pensiones"];
            $arrConceptos = array_values(array_unique(array_filter(array_column($terceros,"concepto"),function($e){return !empty($e);})));
            $arrConceptosInternos = array_values(array_filter($arrConceptos,function($e){return strlen($e)==3;}));
            $strConceptoInterno = implode(",",$arrConceptosInternos);
            $strConcepto = implode(",",$arrConceptos);
            $sqlColumnas = "SELECT codigo, nombre
            FROM contexogenaconceptos
            WHERE estado='S' AND formato='1001' AND codigo IN ($strConcepto)";

            $columnas = mysqli_fetch_all(mysqli_query($this->linkbd,$sqlColumnas),MYSQLI_ASSOC);
            array_push($columnas,array("codigo"=>"2276","nombre"=>"Otros pagos"));

            //Códigos internos
            $sqlCodigosInternos = "SELECT codigo, nombre FROM contcodigosinternos WHERE estado='S' AND codigo IN ($strConceptoInterno)";
            $arrCodigosInternos = mysqli_fetch_all(mysqli_query($this->linkbd,$sqlCodigosInternos),MYSQLI_ASSOC);
            foreach ($arrCodigosInternos as $data) {
                array_push($columnas,$data);
            }
            $totalColumnas = count($columnas);
            $totalTerceros = count($terceros);
            //Aquí se asigna las columnas que pertenece a cada tercero
            for ($i=0; $i < $totalTerceros ; $i++) {
                $terceros[$i]['columnas'] = [];
                $totalNewTerceros = count($arrNewTerceros);
                if($totalNewTerceros > 0){
                    $flag = false;
                    for ($j=0; $j < $totalNewTerceros; $j++) {
                        if($arrNewTerceros[$j]['documento'] == $terceros[$i]['documento']){
                            $arrCols = $arrNewTerceros[$j]['columnas'];
                            $arrNewCol = array("concepto"=>$terceros[$i]['concepto'],"valor"=>$terceros[$i]['valor']);
                            array_push($arrCols,$arrNewCol);
                            $arrNewTerceros[$j]['columnas'] = $arrCols;
                            $flag = true;
                            break;
                        }
                    }
                    if(!$flag){
                        $arrNewCol = array("concepto"=>$terceros[$i]['concepto'],"valor"=>$terceros[$i]['valor']);
                        $arrCols = $terceros[$i]['columnas'];
                        array_push($arrCols,$arrNewCol);
                        $terceros[$i]['columnas'] = $arrCols;
                        array_push($arrNewTerceros,$terceros[$i]);
                    }
                }else{
                    $arrNewCol = array("concepto"=>$terceros[$i]['concepto'],"valor"=>$terceros[$i]['valor']);
                    $arrCols = $terceros[$i]['columnas'];
                    array_push($arrCols,$arrNewCol);
                    $terceros[$i]['columnas'] = $arrCols;
                    array_push($arrNewTerceros,$terceros[$i]);
                }
            }
            /*
                Se itera el nuevo arreglo de los terceros para llenar las columnas originales con las
                columnas que ya estan asignadas a cada tercero en el proceso anterior

            */
            $totalNewTerceros = count($arrNewTerceros);
            for ($i=0; $i < $totalNewTerceros ; $i++) {
                $arrAportes = $this->selectSaludPensionTercero($vigencia,$arrNewTerceros[$i]['documento']);
                $salud = $arrAportes['salud'];
                $pension = $arrAportes['pension'];
                $renglones = 36;
                $columnasTercero = $arrNewTerceros[$i]['columnas'];
                for ($l=0; $l < $totalColumnas ; $l++) {
                    $nombre = $columnas[$l]['nombre'];
                    $hayStack = strtolower(replaceChar($nombre));
                    $columnas[$l]['valor'] = 0;
                    $columnas[$l]['nombre'] = ucfirst(strtolower(replaceChar($nombre)));
                    $columnas[$l]['tipo'] = 1;
                    $columnas[$l]['renglon'] = $renglones++;
                    for ($k=0; $k < count($arrNeedles); $k++) {
                        $flag = strstr($hayStack,$arrNeedles[$k]) != "" ? true : false;
                        if($flag){
                            $columnas[$l]['tipo'] = 2;
                            break;
                        }
                    }
                    for ($k=0; $k < count($columnasTercero); $k++) {
                        $is_salud = strstr($hayStack,"salud") != "" ? true : false;
                        $is_pension = strstr($hayStack,"pensiones") && strstr($hayStack,"obligatorios") != "" ? true : false;
                        if($columnas[$l]['codigo'] == $columnasTercero[$k]['concepto']){
                            $columnas[$l]['valor'] = $columnasTercero[$k]['valor'];
                            if($is_salud){
                                $columnas[$l]['valor'] = $salud;
                            }else if($is_pension){
                                $columnas[$l]['valor'] = $pension;
                            }
                            break;
                        }else if($is_salud){
                            $columnas[$l]['valor'] = $salud;
                            break;
                        }else if($is_pension){
                            $columnas[$l]['valor'] = $pension;
                            break;
                        }
                    }
                }
                $filtroColumnas = $this->filtrarColumnas($columnas);
                $arrNewTerceros[$i]['columnas_ingresos'] = $filtroColumnas['columnas_ingresos'];
                $arrNewTerceros[$i]['columnas_aportes'] = $filtroColumnas['columnas_aportes'];
                $arrNewTerceros[$i]['total_ingresos'] = $filtroColumnas['total_ingresos'];
                $arrNewTerceros[$i]['renglon_ingreso'] = $filtroColumnas['renglon_ingreso'];
                $arrNewTerceros[$i]['renglon_aporte'] = $filtroColumnas['renglon_aporte'];
            }
            return $arrNewTerceros;
        }
        public function selectSaludPensionTercero($vigencia,$tercero){
            $sql = "SELECT
            SUM(det.salud) as salud,
            (SUM(det.pension)+ SUM(det.fondosolid)) as pension
            FROM humnomina_det det
            INNER JOIN humnomina cab
            ON cab.id_nom = det.id_nom AND cab.vigencia = '$vigencia'
            WHERE det.cedulanit = '$tercero'";
            return mysqli_query($this->linkbd,$sql)->fetch_assoc();
        }
        public function filtrarColumnas($columnas){

            $totalIngresos = 0;
            $arrIngresos = array_values(array_filter($columnas,function($e){return $e['tipo']==1;}));
            $arrAportes = array_values(array_filter($columnas,function($e){return $e['tipo']==2;}));
            usort($arrAportes,function($a,$b){
                return strcmp($a['nombre'],$b['nombre']);
            });
            $renglonIngreso =  $arrIngresos[count($arrIngresos)-1]['renglon']+1;
            foreach ($arrIngresos as $col) {
                $totalIngresos+= $col['valor'];
            }
            $renglonAporte = $renglonIngreso+1;
            for ($l=0; $l < count($arrAportes) ; $l++) {
                $arrAportes[$l]['renglon'] = $renglonAporte;
                $renglonAporte++;
            }
            return array(
                "columnas_ingresos"=>$arrIngresos,
                "columnas_aportes"=>$arrAportes,
                "total_ingresos"=>$totalIngresos,
                "renglon_ingreso"=>$renglonIngreso,
                "renglon_aporte"=>$renglonAporte-1
            );
        }
    }
?>
