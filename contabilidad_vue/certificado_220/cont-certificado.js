const URL ='contabilidad_vue/certificado_220/cont-certificado.php';
const URLPDF = 'cont-certificadoIngresosRet220Pdf';

var app = new Vue({
    el:"#myapp",
    data() {
        return {
            isLoading:false,
            isModal:false,
            isTable:false,
            txtResults:0,
            txtVigencia:0,
            txtFechaAct:new Date().toISOString().slice(0, 10),
            txtSearch:"",
            arrExogenas:[],
            arrTerceros:[],
            arrTercerosCopy:[],
            arrCamposEmpleado:[],
            objTercero:{documento:"",nombre:""},
            objRetenedor:{razonsocial:""},
            selectExogena:0
        }
    },
    mounted() {
        this.getData();
    },
    methods: {
        getData: async function(){
            const formData = new FormData();
            formData.append("action","get");
            this.isLoading = true;
            const response = await fetch(URL,{method:"POST",body:formData});
            const objData = await response.json();
            this.arrExogenas = objData.exogenas;
            this.objRetenedor = objData.retenedor;
            this.isLoading = false;
            console.log(objData);
        },
        getTerceros:function(){
            this.arrTerceros = this.arrExogenas.filter(e => e.id == this.selectExogena)[0].terceros;
            this.arrTercerosCopy = [...this.arrTerceros];
            this.txtVigencia = [...this.arrExogenas.filter(e=>e.id == this.selectExogena)][0].vigencia;
            this.txtResults = this.arrTercerosCopy.length;
        },
        selectItem:function({...item}){
            this.arrCamposEmpleado = [];
            this.objTercero = item;
            this.isModal = false;
            this.isTable = true;
            const renglon = this.objTercero.renglon_aporte+1;
            const aumentoRenglon = 8;
            const arrNombres = ["Arrendamientos","Honorarios, comisiones y servicios",
            "Intereses y rendimientos financieros","Enajenación de activos fijos","Loterías, rifas, apuestas y similares","Otros"];

            for (let i = 0; i < 8; i++) {
                this.arrCamposEmpleado.push({
                    nombre:arrNombres[i],
                    renglon1:renglon+i,
                    renglon2:(renglon+i)+aumentoRenglon
                })
            }
            this.arrCamposEmpleado[7].renglon1 = "";
            this.arrCamposEmpleado[7].nombre = "Total retenciones año gravable 2022 "+"(Sume " +this.objTercero.renglon_aporte+" + "+
            this.arrCamposEmpleado[6].renglon2+")";
            this.arrCamposEmpleado[6].nombre =  "Totales (Valor recibido: Sume "+this.arrCamposEmpleado[0].renglon1+" a "+
            this.arrCamposEmpleado[5].renglon1+"),"+"(Valor retenido: "+"Sume "+this.arrCamposEmpleado[0].renglon2+" a "+
            this.arrCamposEmpleado[5].renglon2+")";
        },
        search:function(type=""){
            let search = this.txtSearch.toLowerCase();
            if(type=="modal_tercero"){
                this.arrTercerosCopy = [...this.arrTerceros.filter(e=>e.nombre.toLowerCase().includes(search)
                    || e.documento.toLowerCase().includes(search))];
                this.txtResults = this.arrTercerosCopy.length;
            }else{
                let arr = [...this.arrTerceros.filter(e=>e.documento.toLowerCase() == this.objTercero.documento)];
                this.objTercero = arr.length > 0 ? arr[0] : {"documento":"","nombre":"No existe"};
                this.isModal = false;
                this.isTable = false;
            }
        },
        printPDF:function(){
            if(this.objTercero.documento == "" || this.selectExogena == 0){
                Swal.fire("Error","Todos los campos con (*) son obligatorios","error");
                return false;
            }
            const data = {
                vigencia:this.txtVigencia,
                retenedor:this.objRetenedor,
                tercero:this.objTercero,
                empleado:this.arrCamposEmpleado
            }
            const form = document.createElement("form");
            form.method ="post";
            form.target="_blank";
            form.action=URLPDF;
            function addField(name,value){
                const input = document.createElement("input");
                input.type="hidden";
                input.name=name;
                input.value = value;
                form.appendChild(input);
            }
            addField("data",JSON.stringify(data));
            document.body.appendChild(form);
            form.submit();
            document.body.removeChild(form);

        },
        formatNumero: function(valor){
            return new Intl.NumberFormat("en-US", {style: "currency", currency: "USD"}).format(valor);
        },
    },
    computed:{

    }
})
