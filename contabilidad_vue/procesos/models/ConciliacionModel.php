<?php
	require_once '../../../Librerias/core/Helpers.php';
	require_once '../../../Librerias/core/Mysql.php';
	session_start();
	/*ini_set('display_errors', '1');
	ini_set('display_startup_errors', '1');
	error_reporting(E_ALL);*/
	class ConciliacionModel extends Mysql{
		private $strFechaInicial;
		private $strFechaFinal;
		private $floatExtractoBanco;
		private $floatExtractoCalculado;
		private $floatDiferencia;
		private $strCuenta;
		function __construct(){
			parent::__construct();
		}
		public function selectCuentas(){
			$sql ="SELECT nic.cuenta as codigo,
			nic.nombre,
			ban.ncuentaban as cuenta_banco
			FROM cuentasnicsp nic
			INNER JOIN tesobancosctas AS ban ON ban.cuenta = nic.cuenta
			WHERE nic.tipo = 'Auxiliar' AND nic.estado = 'S'";
			$request = $this->select_all($sql);
			return $request;
		}
		public function selectNotas($cuenta = '', $tipoComp = '', $numComp = '', $fechaf = '', $tipoNota = ''){
			if ($tipoNota == 'D'){
				$sql = "SELECT  id, nota, debito, credito, fechaaux FROM conciliacion_registros WHERE cuenta = :cuenta AND num_comprobante = :numComp AND tipo_comprobante = :tipoComp AND tiponota = 'D'"; 
				$params = array(
					':cuenta' => $cuenta,
					':numComp' => $numComp,
					':tipoComp' => $tipoComp
				);
			} else {
				$sql = "SELECT id, nota, debito, credito, fechaaux FROM conciliacion_registros WHERE cuenta = :cuenta AND fecha = :fechaf AND tiponota = 'G'";
				$params = array(
					':cuenta' => $cuenta,
					':fechaf' => $fechaf
				);
			}
			$request = $this->select_all_conParametros($sql, $params);
			return $request;
			
		}
		public function selectNotas_all($cuenta, $fechai, $fechaf){
			$sql = "SELECT num_comprobante, nom_comprobante, nota FROM conciliacion_registros WHERE cuenta = '$cuenta' AND fecha BETWEEN '$fechai' AND '$fechaf'";
			$request = $this->select_all($sql);
			return $request;
		}
		public function selectConciliacionCab(string $strFechaInicial,string $strFechaFinal,string $strCuenta){
			$this->strFechaInicial = $strFechaInicial;
			$this->strFechaFinal = $strFechaFinal;
			$this->strCuenta = $strCuenta;
			$sql = "SELECT extractobanc as saldo_extracto,diferencia FROM conciliacion_cab
			WHERE cuenta ='$this->strCuenta' AND periodo1 = '$this->strFechaInicial' AND periodo2 = '$this->strFechaFinal'";
			$request = $this->select($sql);
			return $request;
		}
		public function selectSaldoInicial(string $strFechaInicial,string $strCuenta){
			$arrFecha = explode("-",$strFechaInicial);
			$this->strFechaInicial = date("Y-m-d",mktime(0,0,0,$arrFecha[1],$arrFecha[2],$arrFecha[0])-(24*60*60));
			$this->strCuenta = $strCuenta;
			$sql = "SELECT coalesce((sum(T2.valdebito) - sum(T2.valcredito)),0) AS saldo
			FROM comprobante_cab AS T1
			INNER JOIN comprobante_det AS T2 ON T2.tipo_comp = T1.tipo_comp AND T2.numerotipo = T1.numerotipo
			WHERE T2.cuenta = '$this->strCuenta' AND T1.estado = '1' AND T2.cuenta != '' AND T1.fecha BETWEEN ''
			AND '$this->strFechaInicial' ORDER BY T1.tipo_comp, T1.numerotipo, T2.id_det, T1.fecha";
			$request = $this->select($sql)['saldo'];
			return $request;
		}
		public function selectSaldoFinal(string $strFechaInicial,string $strFechaFinal,string $strCuenta,float $intSaldoFinal){
			$this->strFechaInicial = $strFechaInicial;
			$this->strFechaFinal = $strFechaFinal;
			$this->strCuenta = $strCuenta;
			$sql = "SELECT coalesce(sum(T2.valdebito),0) as debito,
			coalesce(sum(T2.valcredito),0) as credito
			FROM comprobante_cab AS T1
			INNER JOIN comprobante_det AS T2 ON T2.tipo_comp = T1.tipo_comp
			AND T2.numerotipo = T1.numerotipo
			WHERE T2.cuenta = '$this->strCuenta' AND T1.estado = '1' AND T1.tipo_comp NOT IN(7,102)
			AND T2.cuenta != '' AND T1.fecha BETWEEN '$this->strFechaInicial' AND '$this->strFechaFinal'
			GROUP BY T1.tipo_comp, T1.numerotipo,T2.id_det";
			$request = $this->select_all($sql);
			$debito = 0;
			$credito = 0;
			if(!empty($request)){
				foreach ($request as $e) {
					$debito+=$e['debito'];
					$credito+=$e['credito'];
				}
			}
			return ($debito-$credito)+$intSaldoFinal;
		}
		public function selectComprobantes(string $strFechaInicial,string $strFechaFinal,string $strCuenta){
			$this->strFechaInicial = $strFechaInicial;
			$this->strFechaFinal = $strFechaFinal;
			$this->strCuenta = $strCuenta;
			$condicion ="";
			if(strtotime("2018-01-01")< strtotime($this->strFechaInicial)){
				$condicion = ",'104'";
			}

			$sql = "SELECT cab.id_comp,
			det.id_comp as id_comp_det,
			cab.numerotipo,
			cab.tipo_comp,
			cab.fecha,
			DATE_FORMAT(cab.fecha,'%d/%m/%Y') as fecha_format,
			cab.concepto,
			det.id_det,
			det.cuenta,
			det.tercero,
			det.centrocosto,
			det.cheque,
			coalesce(sum(det.valdebito),0) as debito,
			coalesce(sum(det.valcredito),0) as credito,
			tipo.nombre as nombre_comp,
			CASE WHEN t.razonsocial IS NULL OR t.razonsocial = ''
			THEN CONCAT(t.nombre1,' ',t.nombre2,' ',t.apellido1,' ',t.apellido2)
			ELSE t.razonsocial END AS nombre_tercero
			FROM comprobante_cab cab
			INNER JOIN comprobante_det  det ON det.tipo_comp = cab.tipo_comp AND det.numerotipo = cab.numerotipo
			LEFT JOIN tipo_comprobante  tipo ON tipo.codigo = cab.tipo_comp
			LEFT JOIN terceros t ON t.cedulanit = det.tercero
			WHERE det.cuenta = '$this->strCuenta' AND cab.estado = '1' AND cab.tipo_comp NOT IN('7','102','103'$condicion)
			AND cab.fecha BETWEEN '' AND '$this->strFechaFinal' GROUP BY det.tipo_comp, det.numerotipo  ORDER BY cab.fecha";
			$request = $this->select_all($sql);
			return $request;
		}
		public function selectConciliado(string $strFechaInicial,string $strFechaFinal,string $strCuenta){
			$this->strFechaInicial = $strFechaInicial;
			$this->strFechaFinal = $strFechaFinal;
			$this->strCuenta = $strCuenta;
			$sql = "SELECT id_comp,periodo2,periodo1,DATE_FORMAT(periodo2,'%d/%m/%Y') as periodo_format
			FROM conciliacion
			WHERE cuenta = '$this->strCuenta' AND periodo2 BETWEEN '$this->strFechaInicial' AND '$this->strFechaFinal'";
			$sql2 = "SELECT id_comp,periodo2,periodo1,DATE_FORMAT(periodo2,'%d/%m/%Y') as periodo_format
			FROM conciliacion
			WHERE cuenta = '$this->strCuenta' AND periodo2 <='$this->strFechaInicial'";
			return array("periodo_actual"=>$this->select_all($sql),"periodo_anterior"=>$this->select_all($sql2));
		}
		public function insertCab(string $strCuenta,float $floatExtractoBanco,float $floatExtractoCalculado,float $floatDiferencia,string $strFechaInicial,string $strFechaFinal){
			$this->floatExtractoBanco = $floatExtractoBanco;
			$this->floatExtractoCalculado = $floatExtractoCalculado;
			$this->floatDiferencia = $floatDiferencia;
			$this->strFechaInicial = $strFechaInicial;
			$this->strFechaFinal = $strFechaFinal;
			$this->strCuenta = $strCuenta;
			$sql = "DELETE FROM conciliacion_cab WHERE cuenta = '$this->strCuenta' AND periodo1 = '$this->strFechaInicial' AND periodo2 = '$this->strFechaFinal'";
			$this->delete($sql);
			$sql = "INSERT INTO conciliacion_cab(cuenta,extractobanc,extractocalc,diferencia,fecha,vigencia,periodo1,periodo2) VALUES(?,?,?,?,?,?,?,?)";
			$arrData = array(
				$this->strCuenta,
				$this->floatExtractoBanco,
				$this->floatExtractoCalculado,
				$this->floatDiferencia,
				date("Y-m-d"),
				date("Y"),
				$this->strFechaInicial,
				$this->strFechaFinal
			);
			$request = $this->insert($sql,$arrData);
			insertAuditoria("cont_auditoria","cont_funciones_id",7,"Crear",$request);
			return $request;
		}
		public function insertDet(array $arrData,string $strFechaInicial,string $strFechaFinal){
			$this->strFechaInicial = $strFechaInicial;
			$this->strFechaFinal = $strFechaFinal;
			if(!empty($arrData)){
				foreach ($arrData as $data) {
					if($data['is_show']){
						$sql = "DELETE FROM conciliacion
						WHERE id_comp = '$data[id_comp_det]' AND cuenta = '$this->strCuenta'
						AND periodo2 BETWEEN '$this->strFechaInicial' AND '$this->strFechaFinal'";
						$this->delete($sql);
						if($data['is_checked']){
							$sql="INSERT INTO conciliacion (id_comp, cuenta, id_det, valordeb, valorcred, fecha, vigencia, periodo1, periodo2)
							VALUES (?,?,?,?,?,?,?,?,?)";
							$arrValues = array(
								$data['id_comp_det'],
								$data['cuenta'],
								$data['id_det'],
								$data['debito'],
								$data['credito'],
								date('Y-m-d'),
								date('Y'),
								$strFechaInicial,
								$data['periodo_final'] == "" ? $strFechaFinal : $data['periodo_final']
							);
							$this->insert($sql,$arrValues);
						}
					}
				}
			}else{
				$sql = "DELETE FROM conciliacion WHERE cuenta = '$this->strCuenta' AND periodo2 BETWEEN '$this->strFechaInicial' AND '$this->strFechaFinal'";
				$this->delete($sql);
			}
		}
		public function selectConciliacionFecha(string $strCuenta,string $strFechaInicial,string $strFechaFinal){
			$arrFecha = explode("-",$strFechaInicial);
			$this->strFechaInicial = $arrFecha[0]."-01-01";
			$this->strFechaFinal = $strFechaFinal;
			$this->strCuenta = $strCuenta;
			$sql="SELECT id_comp,periodo2,id_det, DATE_FORMAT(periodo2,'%d/%m/%Y') as periodo_format FROM conciliacion
			WHERE cuenta='$this->strCuenta' AND periodo2 NOT BETWEEN '$this->strFechaInicial' AND '$this->strFechaFinal'";
			$request = $this->select_all($sql);
			return $request;
		}
		public function insertNotas($tipoComp = 0, $fechaf, $cuenta, $numComp = 0, $nomComp = '', $registro = '',  $tipoNota, $tipomov, $valor, $fechaaux){
			if($tipomov == 'debito'){
				$valdebito = $valor;
				$valcredito = 0;
			}else {
				$valdebito = 0;
				$valcredito = $valor;
			}
			$sql = "INSERT INTO conciliacion_registros(tipo_comprobante, fecha, cuenta, num_comprobante, nom_comprobante, nota, estado, tiponota, debito, credito, fechaaux) VALUES(?,?,?,?,?,?,?,?,?,?,?)";
			$arrData = array(
				$tipoComp,
				$fechaf,
				$cuenta,
				$numComp,
				$nomComp,
				$registro,
				'S',
				$tipoNota,
				$valdebito,
				$valcredito,
				$fechaaux
			);
			$request = $this->insert($sql,$arrData);
			return $request;
		}
		public function delNotas($numID){
			$sql = "DELETE FROM conciliacion_registros WHERE id = :numid";
			$params = [
				':numid' => $numID,
			];
			$request = $this->delete_conParametros($sql, $params);
			return $request;
		}
	}
?>
