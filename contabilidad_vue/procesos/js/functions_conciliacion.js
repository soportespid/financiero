const URL ='contabilidad_vue/procesos/controllers/ConciliacionController.php';
const URLEXPORT = 'contabilidad_vue/procesos/controllers/ConciliacionExportController.php';
var app = new Vue({
	el:"#myapp",
	data() {
		return {
			isLoading:false,
			isModal:false,
			isModal2:false,
			isCheckAll:false,
			isExport:0,
			txtSearch:"",
			txtResultados:0,
			txtFechaInicial:new Date(new Date().setDate(1)).toISOString().split("T")[0],
			txtFechaFinal:new Date(new Date().getFullYear(), new Date().getMonth() + 1, 0).toISOString().split("T")[0],
			objCuenta:{codigo:"",nombre:""},
			arrCuentas:[],
			arrCuentasCopy:[],
			arrData:[],
			txtSaldoExtracto:0,
			txtDiferencia:0,
			txtSaldoInicial:0,
			txtSaldoFinal:0,
			txtDebitoConciliado:0,
			txtDebitoNoConciliado:0,
			txtCreditoConciliado:0,
			txtCreditoNoConciliado:0,
			txtTotalDebito:0,
			txtTotalCredito:0,
			txtSaldoExtractoCalculo:0,
			objNota:{numCuenta:"", tipoComprobante: "", numComprobante:"", fecha:"", nomComprobante:"", tipoNota:""},
			notaNueva:{registro:'', valor:0, fecha:'', tipoMovimiento:''},
			arrNotas:[],
		}
	},
	mounted() {
		const codigo = new URLSearchParams(window.location.search).get('id');
		this.getData();
	},
	methods: {
		getData:async function(){
			const formData = new FormData();
			formData.append("action","get");
			const response = await fetch(URL,{method:"POST",body:formData});
			const objData = await response.json();
			this.arrCuentas = objData.cuentas;
			this.arrCuentasCopy = objData.cuentas;
			this.txtResultados = this.arrCuentasCopy.length;
			if(new URLSearchParams(window.location.search).get('cod')){
				const codigo = new URLSearchParams(window.location.search).get('cod');
				this.txtFechaInicial = new URLSearchParams(window.location.search).get('fec');
				this.txtFechaFinal = new URLSearchParams(window.location.search).get('fec1');
				const data = [...this.arrCuentas.filter(e=>e.codigo == codigo)];
				this.objCuenta = JSON.parse(JSON.stringify(data[0]));
				this.generate();
			}
		},
		search: function(type=""){
			let search = "";
			if(type == "modal")search = this.txtSearch.toLowerCase();
			if(type == "cod_cuenta") search = this.objCuenta.codigo;

			if(type=="modal"){
				this.arrCuentasCopy = [...this.arrCuentas.filter(e=>e.codigo.toLowerCase().includes(search) || e.nombre.toLowerCase().includes(search))];
				this.txtResultados = this.arrCuentasCopy.length;
			}else if(type == "cod_cuenta"){
				const data = [...this.arrCuentas.filter(e=>e.codigo == search)];
				this.objCuenta = data.length > 0 ? JSON.parse(JSON.stringify(data[0])) : {codigo:"",nombre:""};
			}
		},
		save:async function(){
			if(this.objCuenta.codigo == ""){
				Swal.fire("Atención!","debe seleccionar la cuenta a conciliar.","warning");
				return false;
			}
			Swal.fire({
				title:"¿Estás segur@ de guardar?",
				text:"",
				icon: 'warning',
				showCancelButton:true,
				confirmButtonColor: '#3085d6',
				cancelButtonColor: '#d33',
				confirmButtonText:"Sí, guardar",
				cancelButtonText:"No, cancelar"
			}).then(async function(result){
				if(result.isConfirmed){
					const formData = new FormData();
					formData.append("action","save");
					formData.append("fecha_inicial",app.txtFechaInicial);
					formData.append("fecha_final",app.txtFechaFinal);
					formData.append("extracto_calculado",app.txtSaldoExtractoCalculo);
					formData.append("extracto_banco",app.txtSaldoExtracto);
					formData.append("diferencia",app.txtDiferencia);
					formData.append("cuenta",app.objCuenta.codigo);
					formData.append("data",JSON.stringify(app.arrData));
					app.isLoading = true;
					const response = await fetch(URL,{method:"POST",body:formData});
					const objData = await response.json();
					app.isLoading = false;
					if(objData.status){
						let arrFecha = app.txtFechaFinal.split("-");
						const arrData = [...app.arrData];
						arrData.forEach(function(e){
							if(e.is_checked){
								e.periodo_format = arrFecha[2]+"/"+arrFecha[1]+"/"+arrFecha[0];
							}else{
								e.periodo_format = "";
							}
						});
						app.arrData = arrData;
						Swal.fire("Guardado",objData.msg,"success");
					}else{
						Swal.fire("Error",objData.msg,"error");
					}
				}
			});
		},
		exportData:function(type=1){
			if(this.objCuenta.codigo == ""){
				Swal.fire("Atención!","debe seleccionar la cuenta a conciliar.","warning");
				return false;
			}
			const form = document.createElement("form");
			form.method ="post";
			form.target="_blank";
			form.action=URLEXPORT;

			function addField(name,value){
				const input = document.createElement("input");
				input.type="hidden";
				input.name=name;
				input.value = value;
				form.appendChild(input);
			}
			addField("action",type == 1 ? "pdf" : "excel");
			addField("fecha_inicial",this.txtFechaInicial);
			addField("fecha_final",this.txtFechaFinal);
			addField("data",JSON.stringify(this.arrData));
			addField("extracto_calculado",this.txtSaldoExtractoCalculo);
			addField("extracto_banco",this.txtSaldoExtracto);
			addField("diferencia",this.txtDiferencia);
			addField("cuenta",this.objCuenta.codigo);
			addField("nombre",this.objCuenta.nombre);
			addField("saldo_inicial",this.txtSaldoInicial);
			addField("saldo_final",this.txtSaldoFinal);
			addField("credito_conciliado",this.txtCreditoConciliado);
			addField("credito_noconciliado",this.txtCreditoNoConciliado);
			addField("debito_conciliado",this.txtDebitoConciliado);
			addField("debito_noconciliado",this.txtDebitoNoConciliado);
			addField("type",app.isExport);
			document.body.appendChild(form);
			form.submit();
			document.body.removeChild(form);
		},
		generate:async function(){
			if(this.objCuenta.codigo ==""){
				Swal.fire("Atención","Debe seleccionar la cuenta que quiere conciliar.","warning");
				return false;
			}
			const formData = new FormData();
			formData.append("action","gen");
			formData.append("fecha_inicial",this.txtFechaInicial);
			formData.append("fecha_final",this.txtFechaFinal);
			formData.append("cuenta",this.objCuenta.codigo);
			this.isLoading = true;
			const response = await fetch(URL,{method:"POST",body:formData});
			const objData = await response.json();
			this.isLoading = false;
			if(objData.status){
				this.txtDiferencia = objData.data.diferencia;
				this.txtSaldoExtracto = objData.data.saldo_extracto;
				this.txtSaldoInicial = objData.data.saldo_inicial;
				this.txtSaldoFinal = objData.data.saldo_final;
				this.txtCreditoConciliado = objData.data.credito_conciliado;
				this.txtCreditoNoConciliado = objData.data.credito_noconciliado;
				this.txtDebitoConciliado = objData.data.debito_conciliado;
				this.txtDebitoNoConciliado = objData.data.debito_noconciliado;
				this.txtTotalDebito = objData.data.total_debito;
				this.txtTotalCredito = objData.data.total_credito;
				this.txtSaldoExtractoCalculo = objData.data.saldo_extracto_calculado;
				this.arrData = objData.data.data;
				this.arrData.forEach(function(e){
					e.concepto = e.concepto != "" ? e.concepto.toLowerCase() : e.concepto;
					e.nombre_comp = e.nombre_comp != "" ? e.nombre_comp.toLowerCase() : e.nombre_comp;
					e.nombre_tercero = e.nombre_tercero != "" ? e.nombre_tercero.toLowerCase() : e.nombre_tercero;
				});
				const flag = this.arrData.every((e)=>e.is_checked && e.is_show);
				this.isCheckAll = flag;
			}else{
				Swal.fire("Error",objData.msg,"error");
			}
		},
		redirect:function(){
			if(this.objCuenta.codigo ==""){
				Swal.fire("Atención","Debe seleccionar la cuenta que quiere conciliar.","warning");
				return false;
			}
			const arrFechaInicial = this.txtFechaInicial.split("-");
			const arrFechaFinal = this.txtFechaFinal.split("-");
			const fechaInicial = arrFechaInicial[2]+"/"+arrFechaInicial[1]+"/"+arrFechaInicial[0];
			const fechaFinal = arrFechaFinal[2]+"/"+arrFechaFinal[1]+"/"+arrFechaFinal[0];
			window.open("cont-auxiliarcuenta.php?cod="+this.objCuenta.codigo+"&fec="+fechaInicial+"&fec1="+fechaFinal);
		},
		changeStatus:function(index){
			const data = this.arrData[index];
			data.is_checked=!data.is_checked;
			data.debito = parseFloat(data.debito);
			data.credito = parseFloat(data.credito);
			if(data.is_checked){
				this.txtCreditoNoConciliado -=data.credito;
				this.txtCreditoConciliado +=data.credito;
				this.txtDebitoNoConciliado -=data.debito;
				this.txtDebitoConciliado +=data.debito;
				if(data.debito > 0){
					this.txtDiferencia-=data.debito;
				}else{
					this.txtDiferencia+=data.credito;
				}
			}else{
				this.txtCreditoNoConciliado +=data.credito;
				this.txtCreditoConciliado -=data.credito;
				this.txtDebitoNoConciliado +=data.debito;
				this.txtDebitoConciliado -=data.debito;
				if(data.debito > 0){
					this.txtDiferencia+=data.debito;
				}else{
					this.txtDiferencia-=data.credito;
				}
			}
			this.txtSaldoExtractoCalculo = this.txtSaldoFinal+this.txtCreditoNoConciliado-this.txtDebitoNoConciliado;
			this.arrData[index] = data;
			const flag = this.arrData.every((e)=>e.is_checked && e.is_show);
			this.isCheckAll = flag;
			this.changeDiferencia();
		},
		changeAll:function(){
			this.txtCreditoNoConciliado = 0;
			this.txtCreditoConciliado = 0;
			this.txtDebitoNoConciliado = 0;
			this.txtDebitoConciliado = 0;
			this.txtDiferencia = 0;
			this.arrData.forEach(e => {
				e.is_checked = this.isCheckAll;
				e.debito = parseFloat(e.debito);
				e.credito = parseFloat(e.credito);
				if(e.is_checked){
					this.txtCreditoConciliado +=e.credito;
					this.txtDebitoConciliado +=e.debito;
				}else{
					this.txtCreditoNoConciliado +=e.credito;
					this.txtDebitoNoConciliado +=e.debito;
				}
			});
			this.txtSaldoExtractoCalculo = this.txtSaldoFinal+this.txtCreditoNoConciliado-this.txtDebitoNoConciliado;
			this.changeDiferencia();
		},
		changeDiferencia:function(){
			this.txtDiferencia = this.txtSaldoExtracto - this.txtSaldoExtractoCalculo;
		},
		selectItem:function({...item}){
			this.objCuenta = item;
			this.isModal = false;
		},
		formatNum: function(valor){
			return new Intl.NumberFormat("en-US", {style: "currency", currency: "USD"}).format(valor);
		},
		abrirRegistro: async function(tipoNota, tipoComp = '', fecha = '', nomComp = '', numComp = ''){
			let cuenta = this.objNota.numCuenta = this.objCuenta.codigo;
			this.objNota.tipoComprobante = tipoComp;
			this.objNota.numComprobante = numComp;
			this.objNota.fecha = fecha;
			this.objNota.nomComprobante = nomComp;
			this.objNota.tipoNota = tipoNota;
			if(tipoNota == 'G'){
				this.notaNueva.fecha = fecha;
			} else {
				const [dia, mes, anio] = fecha.split('/');
				this.notaNueva.fecha = `${anio}-${mes}-${dia}`;
			}
			const formData = new FormData();
			formData.append("action","cargaNota");
			formData.append("numCuenta",cuenta);
			formData.append("tipoComp",tipoComp);
			formData.append("numComp",numComp);
			formData.append("fecha",fecha);
			formData.append("tipoNota",tipoNota);
			this.isLoading = true;
			const response = await fetch(URL,{method:"POST",body:formData});
			const objData = await response.json();
			this.isLoading = false;
			if(objData.status){
				this.arrNotas = objData.notas;
				this.isModal2 = true;
			}else{
				Swal.fire("Error",objData.msg,"error");
			}
		},
		guardarNotas: async function(){
			Swal.fire({
				title:"¿Estás segur@ de guardar la nota?",
				text:"",
				icon: 'warning',
				showCancelButton:true,
				confirmButtonColor: '#3085d6',
				cancelButtonColor: '#d33',
				confirmButtonText:"Sí, guardar",
				cancelButtonText:"No, cancelar"
			}).then(async function(result){
				if(result.isConfirmed){
					const formData = new FormData();
					formData.append("action","guardarNota");
					formData.append("numCuenta", app.objNota.numCuenta);
					formData.append("tipoComp", app.objNota.tipoComprobante);
					formData.append("numComp", app.objNota.numComprobante);
					formData.append("fecha", app.objNota.fecha);
					formData.append("nomComp", app.objNota.nomComprobante);
					formData.append("tipoNota",app.objNota.tipoNota);

					formData.append("tipomovimiento",app.notaNueva.tipoMovimiento);
					formData.append("registro", app.notaNueva.registro);
					formData.append("valor", app.notaNueva.valor);
					formData.append("fechaaux", app.notaNueva.fecha);

					app.isLoading = true;
					const response = await fetch(URL,{method:"POST",body:formData});
					const objData = await response.json();
					app.isLoading = false;
					if(objData.status){
						Swal.fire("Guardado",objData.msg,"success");
						app.limpiarNotas();
						app.abrirRegistro(app.objNota.tipoNota, app.objNota.tipoComprobante, app.objNota.fecha, app.objNota.nomComprobante, app.objNota.numComprobante);
					}else{
						Swal.fire("Error",objData.msg,"error");
					}
				}
			});
		},
		eliminarNotas: async function(numID){
			Swal.fire({
				title:"¿Estás segur@ de eliminar la nota?",
				text:"",
				icon: 'warning',
				showCancelButton:true,
				confirmButtonColor: '#3085d6',
				cancelButtonColor: '#d33',
				confirmButtonText:"Sí, Eliminar",
				cancelButtonText:"No, cancelar"
			}).then(async function(result){
				if(result.isConfirmed){
					const formData = new FormData();
					formData.append("action","eliminarNota");
					formData.append("numID", numID);
					app.isLoading = true;
					const response = await fetch(URL,{method:"POST",body:formData});
					const objData = await response.json();
					app.isLoading = false;
					if(objData.status){
						Swal.fire("Eliminado",objData.msg,"success");
						app.limpiarNotas();
						app.abrirRegistro(app.objNota.tipoNota, app.objNota.tipoComprobante, app.objNota.fecha, app.objNota.nomComprobante, app.objNota.numComprobante);
					}else{
						Swal.fire("Error",objData.msg,"error");
					}
				}
			});
		},
		cerrarNotas: function(){
			app.objNota.numCuenta = '';
			app.objNota.tipoComprobante = '';
			app.objNota.numComprobante = '';
			app.objNota.fecha = '';
			app.objNota.nomComprobante = '';
			app.objNota.tipoNota = '';
			app.limpiarNotas();
			this.isModal2 = false;
		},
		limpiarNotas: function(){
			app.notaNueva.tipoMovimiento = '';
			app.notaNueva.registro = '';
			app.notaNueva.valor = 0;
			if(app.objNota.tipoNota == 'G'){
				this.notaNueva.fecha = app.objNota.fecha;
			} else {
				const [dia, mes, anio] = app.objNota.fecha.split('/');
				this.notaNueva.fecha = `${anio}-${mes}-${dia}`;
			}
		},
	},
	computed:{

	}
})
