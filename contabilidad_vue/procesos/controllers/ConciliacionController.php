<?php
	require_once '../../../Librerias/core/Helpers.php';
	require_once '../../../PHPExcel/Classes/PHPExcel.php';
	require_once '../models/ConciliacionModel.php';
	session_start();

	class ConciliacionController extends ConciliacionModel{
		public function getData(){
			if(!empty($_SESSION)){
				$arrData = array(
					"cuentas"=>$this->selectCuentas()
				);
				echo json_encode($arrData,JSON_UNESCAPED_UNICODE);
			}
			die();
		}
		public function generate(){
			if(!empty($_SESSION)){
				if(empty($_POST['fecha_inicial']) || empty($_POST['fecha_final']) || empty($_POST['cuenta'])){
					$arrResponse = array("status"=>false,"msg"=>"Error de datos");
				}else{
					$strFechaInicial = strClean($_POST['fecha_inicial']);
					$strFechaFinal = strClean($_POST['fecha_final']);
					$intFechaInicial = strtotime($strFechaInicial);
					$strFechaFinalCompare = new DateTime($strFechaFinal);
					$strFechaFinalCompare->modify('last day of this month');
					$intFechaFinalCompare = strtotime($strFechaFinalCompare->format("Y-m-d"));
					$arrFilterData = [];
					$debitoConciliado = 0;
					$debitoNoConciliado = 0;
					$creditoConciliado = 0;
					$creditoNoConciliado = 0;
					$totalDebito = 0;
					$totalCredito = 0;
					$strCuenta = strClean($_POST['cuenta']);
					$arrConciliacion = $this->selectConciliacionCab($strFechaInicial,$strFechaFinal,$strCuenta);
					$intSaldoInicial = $this->selectSaldoInicial($strFechaInicial,$strCuenta);
					$intSaldoFinal = $this->selectSaldoFinal($strFechaInicial,$strFechaFinal,$strCuenta,$intSaldoInicial);
					$arrComprobantes = $this->selectComprobantes($strFechaInicial,$strFechaFinal,$strCuenta);
					$arrConciliado = $this->selectConciliado($strFechaInicial,$strFechaFinal,$strCuenta);
					$arrConciliadoFecha = $this->selectConciliacionFecha($strCuenta,$strFechaInicial,$strFechaFinal);

					$arrConcilioActual = array_values(array_column($arrConciliado['periodo_actual'],"id_comp"));
					$arrConcilioAnterior = array_values(array_column($arrConciliado['periodo_anterior'],"id_comp"));
					$arrConcilioFechas = array_values(array_column($arrConciliadoFecha,"id_comp"));
					foreach ($arrComprobantes as $e) {
						$e['is_status'] = 0;
						$e['is_checked'] = 0;
						$e['is_show'] = 0;
						$e['periodo_final']="";
						$e['periodo_inicial']="";
						$e['periodo_format']="";

						$intFechaComprobante = strtotime($e['fecha']);
						$flagActual = in_array($e['id_comp_det'],$arrConcilioActual);

						if($flagActual){
							$e['is_status'] = 1;
							$e['is_checked'] = 1;
							$data = [];
							foreach ($arrConciliado['periodo_actual'] as $d) {
								if($d['id_comp'] == $e['id_comp_det']){$data=$d;break;}
							}
							//$data = array_values(array_filter($arrConciliado['periodo_actual'],function($f) use($e){return $f['id_comp']==$e['id_comp_det'];}))[0];
							$e['periodo_final'] = $data['periodo2'];
							$e['periodo_format']=$data['periodo_format'];
							$e['periodo_inicial'] = $data['periodo1'];
						}else {
							if ($intFechaComprobante > $intFechaInicial){
								$e['is_status'] = 1;
							}else{
								$e['is_status'] =  in_array($e['id_comp_det'],$arrConcilioAnterior) ? 0 : 1;
							}
						}
						if($e['is_status']){
							$totalCredito +=$e['credito'];
							$totalDebito +=$e['debito'];
							if($e['is_checked']){
								$debitoConciliado+=$e['debito'];
								$creditoConciliado+=$e['credito'];
							}else{
								$debitoNoConciliado+=$e['debito'];
								$creditoNoConciliado+=$e['credito'];
							}
							if($e['periodo_final'] != ""){
								if($intFechaFinalCompare >=  strtotime($e['periodo_final'])){
									$e['is_show'] = 1;
								}
							}
							if(!$e['is_show'] && in_array($e['id_comp_det'],$arrConcilioFechas)){
								foreach ($arrConciliadoFecha as $d) {
									if($d['id_comp'] == $e['id_comp_det']){$data=$d;break;}
								}
								//$data = array_values(array_filter($arrConciliadoFecha,function($f) use($e){return $f['id_comp']==$e['id_comp_det'];}))[0];
								$e['periodo_final'] = $data['periodo2'];
								$e['periodo_format'] = $data['periodo_format'];
							}else{
								$e['is_show'] = 1;
							}
							array_push($arrFilterData,$e);
						}
					}
					$saldoExtracto = round($arrConciliacion['saldo_extracto'],2);
					$intSaldoInicial = round($intSaldoInicial,2);
					$intSaldoFinal = round($intSaldoFinal,2);
					$intSaldoCalculado = $intSaldoFinal-$debitoNoConciliado+$creditoNoConciliado;
					$diferencia = $saldoExtracto - $intSaldoCalculado;
					$diferencia = $diferencia >= 0 && $diferencia <= 1 ? 0 : $diferencia;
					usort($arrFilterData,function($a,$b){
						$strFecha1 = strtotime($a['fecha']);
						$strFecha2 = strtotime($b['fecha']);
						return $strFecha1 > $strFecha2;
					});
					$arrData = array (
						"saldo_extracto"=>$saldoExtracto,
						"diferencia"=>$diferencia,
						"saldo_inicial"=>$intSaldoInicial,
						"saldo_final"=>$intSaldoFinal,
						"debito_conciliado"=>$debitoConciliado,
						"credito_conciliado"=>$creditoConciliado,
						"debito_noconciliado"=>$debitoNoConciliado,
						"credito_noconciliado"=>$creditoNoConciliado,
						"total_debito"=>$totalDebito,
						"total_credito"=>$totalCredito,
						"saldo_extracto_calculado"=>$intSaldoCalculado,
						"data"=>$arrFilterData
					);
					$arrResponse = array("status"=>true,"data"=>$arrData);
				}
				echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
			}
			die();
		}
		public function save(){
			if(!empty($_SESSION)){

				if(empty($_POST['fecha_inicial']) || empty($_POST['fecha_final']) || empty($_POST['cuenta']) ){
					$arrResponse = array("status"=>false,"msg"=>"Error de datos");
				}else{
					$arrData = json_decode($_POST['data'],true);
					if(!is_array($arrData)){
						$arrResponse = array("status"=>false,"msg"=>"Los datos no son válidos.");
					}else{
						$strFechaInicial = strClean($_POST['fecha_inicial']);
						$arrFechaInicial = explode("-",$strFechaInicial);
						$strFechaInicial = $arrFechaInicial[0]."-".$arrFechaInicial[1]."-01";
						$strFechaFinal = strClean($_POST['fecha_final']);
						$strFechaFinal = new DateTime($strFechaFinal);
						$strFechaFinal->modify('last day of this month');
						$strFechaFinal = $strFechaFinal->format("Y-m-d");
						$strCuenta = strClean($_POST['cuenta']);
						$floatExtractoCalculado = floatval($_POST['extracto_calculado']);
						$floatExtractoBanco = floatval($_POST['extracto_banco']);
						$floatDiferencia = floatval($_POST['diferencia']);
						$request = $this->insertCab($strCuenta,$floatExtractoBanco,$floatExtractoCalculado,$floatDiferencia,$strFechaInicial,$strFechaFinal);
						if($request > 0){
							if(!empty($arrData)){
								$this->insertDet($arrData,$strFechaInicial,$strFechaFinal);
							}
							$arrResponse = array("status"=>true,"msg"=>"Datos guardados");

						}else{
							$arrResponse = array("status"=>false,"msg"=>"Error en la cabecera");
						}
					}
					echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
				}
			}
			die();
		}
		public function cargaNota(){
			if(!empty($_SESSION)){
				$cuenta = $_POST["numCuenta"];
				$tipoComp = $_POST["tipoComp"];
				$numComp = $_POST["numComp"];
				$fechaf = $_POST["fecha"];
				$tipoNota = $_POST["tipoNota"];
				$arrData = array(
					"status"=>true,
					"notas"=>$this->selectNotas($cuenta, $tipoComp, $numComp, $fechaf, $tipoNota)
				);
			} else {
				$arrData = array("status"=>false,"msg"=>"Error con la Sesión");
			}
			echo json_encode($arrData,JSON_UNESCAPED_UNICODE);
			die();
		}
		public function guardarNota(){
			if(!empty($_SESSION)){
				$cuenta = $_POST["numCuenta"];
				$tipoComp = $_POST["tipoComp"];
				$numComp = $_POST["numComp"];
				$nomComp = $_POST["nomComp"];
				$tipoNota = $_POST["tipoNota"];

				$tipomov = $_POST["tipomovimiento"];
				$valor = $_POST["valor"];
				$fechaaux = $_POST["fechaaux"];
				$registro = $_POST["registro"];

				if($tipoNota == 'D'){
					preg_match("/([0-9]{2})\/([0-9]{2})\/([0-9]{4})/", $_POST['fecha'],$fecha);
					$fechaf = "$fecha[3]-$fecha[2]-$fecha[1]";
				} else {
					$fechaf = $_POST['fecha'];
				}
				$request = $this->insertNotas($tipoComp, $fechaf, $cuenta, $numComp, $nomComp, $registro, $tipoNota, $tipomov, $valor, $fechaaux);
				if($request > 0){
					$arrData = array("status"=>true,"msg"=>"Nota guardada");
				}else{
					$arrData = array("status"=>false,"msg"=>"Error al guardar Nota");
				}
			} else {
				$arrData = array("status"=>false,"msg"=>"Error con la Sesión");
			}
			echo json_encode($arrData,JSON_UNESCAPED_UNICODE);
			die();
		}
		public function eliminarNota(){
			if(!empty($_SESSION)){
				$numID = $_POST["numID"];
				$request = $this->delNotas($numID);
				if($request > 0){
					$arrData = array("status"=>true,"msg"=>"Nota guardada");
				}else{
					$arrData = array("status"=>false,"msg"=>"Error al guardar Nota");
				}
			} else {
				$arrData = array("status"=>false,"msg"=>"Error con la Sesión");
			}
			echo json_encode($arrData,JSON_UNESCAPED_UNICODE);
			die();
		}
	}
	if($_POST){
		$obj = new ConciliacionController();
		$accion = $_POST["action"];
		switch ($accion){
			case 'get':
				$obj->getData();
				break;
			case 'gen':
				$obj->generate();
				break;
			case 'cargaNota':
				$obj->cargaNota();
				break;
			case 'guardarNota':
				$obj->guardarNota();
				break;
			case 'eliminarNota':
				$obj->eliminarNota();
				break;
			case 'save':
				$obj->save();
				break;
			
		}
	}
?>
