<?php
	require_once '../../../Librerias/core/Helpers.php';
	require_once '../../../PHPExcel/Classes/PHPExcel.php';
	require_once '../../../tcpdf/tcpdf_include.php';
	require_once '../../../tcpdf/tcpdf.php';
	session_start();
	/*ini_set('display_errors', '1');
	ini_set('display_startup_errors', '1');
	error_reporting(E_ALL);*/
	class ConciliacionExportControler {
		public function exportExcel(){
			if(!empty($_SESSION)){
				$arrData = json_decode($_POST['data'],true);
				if(!empty($arrData)){
					$strType = $_POST['type'];
					$arrData = json_decode($_POST['data'],true);
					$arrConciliados = array_filter($arrData,function($e){return $e['periodo_format'] != "" && $e['is_show'];});
					$arrNoConciliados = array_filter($arrData,function($e){return $e['periodo_format'] == "" && $e['is_show'];});
					$arrFechaInicial = explode("-",$_POST['fecha_inicial']);
					$arrFechaFinal = explode("-",$_POST['fecha_final']);
					$strFechaInicial = $arrFechaInicial[2]."/".$arrFechaInicial[1]."/".$arrFechaInicial[0];
					$strFechaFinal = $arrFechaFinal[2]."/".$arrFechaFinal[1]."/".$arrFechaFinal[0];
					$strNombre = strClean($_POST['nombre']);
					$strCuenta = strClean($_POST['cuenta']);
					$floatExtractoCalculado = floatval($_POST['extracto_calculado']);
					$floatExtractBanco = floatval($_POST['extracto_banco']);
					$floatDiferencia = floatval($_POST['diferencia']);
					$floatSaldoInicial = floatval($_POST['saldo_inicial']);
					$floatSaldoFinal = floatval($_POST['saldo_final']);
					$floatCreditoConciliado = floatval($_POST['credito_conciliado']);
					$floatCreditoNoConciliado = floatval($_POST['credito_noconciliado']);
					$floatDebitoConciliado = floatval($_POST['debito_conciliado']);
					$floatDebitoNoConciliado = floatval($_POST['debito_noconciliado']);
					$floatTotalDebito = floatval($_POST['total_debito']);
					$floatTotalCredito = floatval($_POST['total_credito']);

					$objPHPExcel = new PHPExcel();
					$objPHPExcel->getActiveSheet()->getStyle('A:J')->applyFromArray(
						array(
							'alignment' => array(
								'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
								'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER
							)
						)
					);
					$objPHPExcel->getProperties()
					->setCreator("IDEAL 10")
					->setLastModifiedBy("IDEAL 10")
					->setTitle("Exportar Excel con PHP")
					->setSubject("Documento de prueba")
					->setDescription("Documento generado con PHPExcel")
					->setKeywords("usuarios phpexcel")
					->setCategory("reportes");

					//----Cuerpo de Documento----
					$objPHPExcel->setActiveSheetIndex(0)
					->mergeCells('A1:J1')
					->mergeCells('A2:J2')
					->mergeCells('A3:D3')
					->mergeCells('E3:J3')
					->mergeCells('A4:D4')
					->mergeCells('E4:J4')
					->mergeCells('A5:D5')
					->mergeCells('H5:I5')
					->mergeCells('A6:E6')
					->mergeCells('F6:J6')
					->mergeCells('A7:B7')
					->mergeCells('H7:I7')
					->mergeCells('A8:D8')
					->mergeCells('H8:I8')
					->setCellValue('A1', 'CONTABILIDAD')
					->setCellValue('A2', 'CONCILIACIÓN BANCARIA')
					->setCellValue('A3', 'Fecha')
					->setCellValue('A4', 'Cuenta')
					->setCellValue('A5', 'Saldo extracto bancario')
					->setCellValue('F5', 'Saldo I')
					->setCellValue('H5', 'Saldo F')
					->setCellValue('A6', "No conciliados")
					->setCellValue('F6', "Conciliados")
					->setCellValue('A7', "Débito")
					->setCellValue('D7', "Crédito")
					->setCellValue('F7', "Débito")
					->setCellValue('H7', "Crédito")
					->setCellValue('A8', "Saldo extracto calculado")
					->setCellValue('F8', "Diferencia")
					->setCellValue('H8', "Estado")
					->setCellValue('E3', "Desde ".$strFechaInicial." Hasta ".$strFechaFinal)
					->setCellValue('E4', $strCuenta."-".$strNombre)
					->setCellValue('E5', $floatExtractBanco)
					->setCellValue('G5', $floatSaldoInicial)
					->setCellValue('J5', $floatSaldoFinal)
					->setCellValue('C7', $floatDebitoNoConciliado)
					->setCellValue('E7', $floatCreditoNoConciliado)
					->setCellValue('G7', $floatDebitoConciliado)
					->setCellValue('J7', $floatCreditoConciliado)
					->setCellValue('E8', $floatExtractoCalculado)
					->setCellValue('G8', $floatDiferencia)
					->setCellValue('J8', $floatDiferencia >= 0 && $floatDiferencia <= 1  ? "Conciliado" : "No conciliado");
					$objPHPExcel-> getActiveSheet ()
					-> getStyle ("A3")
					-> getFill ()
					-> setFillType (PHPExcel_Style_Fill :: FILL_SOLID)
					-> getStartColor ()
					-> setRGB ('3399cc');
					$objPHPExcel-> getActiveSheet ()
					-> getStyle ("A4")
					-> getFill ()
					-> setFillType (PHPExcel_Style_Fill :: FILL_SOLID)
					-> getStartColor ()
					-> setRGB ('3399cc');
					$objPHPExcel-> getActiveSheet ()
					-> getStyle ("A5")
					-> getFill ()
					-> setFillType (PHPExcel_Style_Fill :: FILL_SOLID)
					-> getStartColor ()
					-> setRGB ('3399cc');
					$objPHPExcel-> getActiveSheet ()
					-> getStyle ("A6:J6")
					-> getFill ()
					-> setFillType (PHPExcel_Style_Fill :: FILL_SOLID)
					-> getStartColor ()
					-> setRGB ('3399cc');
					$objPHPExcel-> getActiveSheet ()
					-> getStyle ("A7")
					-> getFill ()
					-> setFillType (PHPExcel_Style_Fill :: FILL_SOLID)
					-> getStartColor ()
					-> setRGB ('3399cc');
					$objPHPExcel-> getActiveSheet ()
					-> getStyle ("D7")
					-> getFill ()
					-> setFillType (PHPExcel_Style_Fill :: FILL_SOLID)
					-> getStartColor ()
					-> setRGB ('3399cc');
					$objPHPExcel-> getActiveSheet ()
					-> getStyle ("H7")
					-> getFill ()
					-> setFillType (PHPExcel_Style_Fill :: FILL_SOLID)
					-> getStartColor ()
					-> setRGB ('3399cc');
					$objPHPExcel-> getActiveSheet ()
					-> getStyle ("H5")
					-> getFill ()
					-> setFillType (PHPExcel_Style_Fill :: FILL_SOLID)
					-> getStartColor ()
					-> setRGB ('3399cc');
					$objPHPExcel-> getActiveSheet ()
					-> getStyle ("F3")
					-> getFill ()
					-> setFillType (PHPExcel_Style_Fill :: FILL_SOLID)
					-> getStartColor ()
					-> setRGB ('3399cc');
					$objPHPExcel-> getActiveSheet ()
					-> getStyle ("F4")
					-> getFill ()
					-> setFillType (PHPExcel_Style_Fill :: FILL_SOLID)
					-> getStartColor ()
					-> setRGB ('3399cc');
					$objPHPExcel-> getActiveSheet ()
					-> getStyle ("F5")
					-> getFill ()
					-> setFillType (PHPExcel_Style_Fill :: FILL_SOLID)
					-> getStartColor ()
					-> setRGB ('3399cc');
					$objPHPExcel-> getActiveSheet ()
					-> getStyle ("F6")
					-> getFill ()
					-> setFillType (PHPExcel_Style_Fill :: FILL_SOLID)
					-> getStartColor ()
					-> setRGB ('3399cc');
					$objPHPExcel-> getActiveSheet ()
					-> getStyle ("F7")
					-> getFill ()
					-> setFillType (PHPExcel_Style_Fill :: FILL_SOLID)
					-> getStartColor ()
					-> setRGB ('3399cc');
					$objPHPExcel-> getActiveSheet ()
					-> getStyle ("A8")
					-> getFill ()
					-> setFillType (PHPExcel_Style_Fill :: FILL_SOLID)
					-> getStartColor ()
					-> setRGB ('3399cc');
					$objPHPExcel-> getActiveSheet ()
					-> getStyle ("F8")
					-> getFill ()
					-> setFillType (PHPExcel_Style_Fill :: FILL_SOLID)
					-> getStartColor ()
					-> setRGB ('3399cc');
					$objPHPExcel-> getActiveSheet ()
					-> getStyle ("H8")
					-> getFill ()
					-> setFillType (PHPExcel_Style_Fill :: FILL_SOLID)
					-> getStartColor ()
					-> setRGB ('3399cc');
					$objPHPExcel-> getActiveSheet ()
					-> getStyle ("A1")
					-> getFill ()
					-> setFillType (PHPExcel_Style_Fill :: FILL_SOLID)
					-> getStartColor ()
					-> setRGB ('C8C8C8');
					$objPHPExcel-> getActiveSheet ()
					-> getStyle ("A1:A2")
					-> getFont ()
					-> setBold ( true )
					-> setName ( 'Verdana' )
					-> setSize ( 10 )
					-> getColor ()
					-> setRGB ('000000');
					$objPHPExcel-> getActiveSheet ()
					-> getStyle ('A1:A2')
					-> getAlignment ()
					-> applyFromArray (array ( 'horizontal'  =>  PHPExcel_Style_Alignment :: HORIZONTAL_CENTER ,) );
					$objPHPExcel-> getActiveSheet ()
					-> getStyle ('A6')
					-> getAlignment ()
					-> applyFromArray (array ( 'horizontal'  =>  PHPExcel_Style_Alignment :: HORIZONTAL_CENTER ,) );
					$objPHPExcel-> getActiveSheet ()
					-> getStyle ('F6')
					-> getAlignment ()
					-> applyFromArray (array ( 'horizontal'  =>  PHPExcel_Style_Alignment :: HORIZONTAL_CENTER ,) );

					$objPHPExcel-> getActiveSheet ()
					-> getStyle ("A2")
					-> getFill ()
					-> setFillType (PHPExcel_Style_Fill :: FILL_SOLID);

					$borders = array(
						'borders' => array(
							'allborders' => array(
								'style' => PHPExcel_Style_Border::BORDER_THIN,
							'color' => array('argb' => 'FF000000'),
							)
						),
					);




					$objPHPExcel->getActiveSheet()->getStyle("A10:J10")->getFont()->setBold(true);
					$objPHPExcel->getActiveSheet()->getStyle('A1:J1')->applyFromArray($borders);
					$objPHPExcel->getActiveSheet()->getStyle('A2:J2')->applyFromArray($borders);
					$objPHPExcel->getActiveSheet()->getStyle('A3:J3')->applyFromArray($borders);
					$objPHPExcel->getActiveSheet()->getStyle('A4:J4')->applyFromArray($borders);
					$objPHPExcel->getActiveSheet()->getStyle('A5:J5')->applyFromArray($borders);
					$objPHPExcel->getActiveSheet()->getStyle('A6:J6')->applyFromArray($borders);
					$objPHPExcel->getActiveSheet()->getStyle('A7:J7')->applyFromArray($borders);
					$objPHPExcel->getActiveSheet()->getStyle('A8:J8')->applyFromArray($borders);


					$objPHPExcel->getActiveSheet()->getStyle("A3")->getFont()->getColor()->setRGB("ffffff");
					$objPHPExcel->getActiveSheet()->getStyle("A4")->getFont()->getColor()->setRGB("ffffff");
					$objPHPExcel->getActiveSheet()->getStyle("A5")->getFont()->getColor()->setRGB("ffffff");
					$objPHPExcel->getActiveSheet()->getStyle("A6")->getFont()->getColor()->setRGB("ffffff");
					$objPHPExcel->getActiveSheet()->getStyle("A7")->getFont()->getColor()->setRGB("ffffff");
					$objPHPExcel->getActiveSheet()->getStyle("A8")->getFont()->getColor()->setRGB("ffffff");
					$objPHPExcel->getActiveSheet()->getStyle("A9")->getFont()->getColor()->setRGB("ffffff");
					$objPHPExcel->getActiveSheet()->getStyle("F3")->getFont()->getColor()->setRGB("ffffff");
					$objPHPExcel->getActiveSheet()->getStyle("F4")->getFont()->getColor()->setRGB("ffffff");
					$objPHPExcel->getActiveSheet()->getStyle("F5")->getFont()->getColor()->setRGB("ffffff");
					$objPHPExcel->getActiveSheet()->getStyle("F6")->getFont()->getColor()->setRGB("ffffff");
					$objPHPExcel->getActiveSheet()->getStyle("F7")->getFont()->getColor()->setRGB("ffffff");
					$objPHPExcel->getActiveSheet()->getStyle("F8")->getFont()->getColor()->setRGB("ffffff");
					$objPHPExcel->getActiveSheet()->getStyle("H5")->getFont()->getColor()->setRGB("ffffff");
					$objPHPExcel->getActiveSheet()->getStyle("H7")->getFont()->getColor()->setRGB("ffffff");
					$objPHPExcel->getActiveSheet()->getStyle("H8")->getFont()->getColor()->setRGB("ffffff");
					$objPHPExcel->getActiveSheet()->getStyle("D7")->getFont()->getColor()->setRGB("ffffff");

					$objPHPExcel->getActiveSheet()->getStyle("A3")->getFont()->setBold(true);
					$objPHPExcel->getActiveSheet()->getStyle("A4")->getFont()->setBold(true);
					$objPHPExcel->getActiveSheet()->getStyle("A5")->getFont()->setBold(true);
					$objPHPExcel->getActiveSheet()->getStyle("A6")->getFont()->setBold(true);
					$objPHPExcel->getActiveSheet()->getStyle("A7")->getFont()->setBold(true);
					$objPHPExcel->getActiveSheet()->getStyle("A8")->getFont()->setBold(true);
					$objPHPExcel->getActiveSheet()->getStyle("A9")->getFont()->setBold(true);
					$objPHPExcel->getActiveSheet()->getStyle("F3")->getFont()->setBold(true);
					$objPHPExcel->getActiveSheet()->getStyle("F4")->getFont()->setBold(true);
					$objPHPExcel->getActiveSheet()->getStyle("F5")->getFont()->setBold(true);
					$objPHPExcel->getActiveSheet()->getStyle("F6")->getFont()->setBold(true);
					$objPHPExcel->getActiveSheet()->getStyle("F7")->getFont()->setBold(true);
					$objPHPExcel->getActiveSheet()->getStyle("F8")->getFont()->setBold(true);
					$objPHPExcel->getActiveSheet()->getStyle("H5")->getFont()->setBold(true);
					$objPHPExcel->getActiveSheet()->getStyle("H7")->getFont()->setBold(true);
					$objPHPExcel->getActiveSheet()->getStyle("H8")->getFont()->setBold(true);
					$objPHPExcel->getActiveSheet()->getStyle("D7")->getFont()->setBold(true);
					$row = 11;
					if(!empty($arrNoConciliados)){
						$objPHPExcel->setActiveSheetIndex(0)
						->mergeCells('A9:J9')
						->setCellValue("A9","No conciliados");
						$objPHPExcel-> getActiveSheet ()
						-> getStyle ("A9")
						-> getFill ()
						-> setFillType (PHPExcel_Style_Fill :: FILL_SOLID)
						-> getStartColor ()
						-> setRGB ('3399cc');
						$objPHPExcel-> getActiveSheet ()
						-> getStyle ("A10:K10")
						-> getFill ()
						-> setFillType (PHPExcel_Style_Fill :: FILL_SOLID)
						-> getStartColor ()
						-> setRGB ('99ddff');
						$objPHPExcel-> getActiveSheet ()
						-> getStyle ('A10:K10')
						-> getAlignment ()
						-> applyFromArray (array ( 'horizontal'  =>  PHPExcel_Style_Alignment :: HORIZONTAL_CENTER ,) );
						$objPHPExcel-> getActiveSheet ()
						-> getStyle ('A9')
						-> getAlignment ()
						-> applyFromArray (array ( 'horizontal'  =>  PHPExcel_Style_Alignment :: HORIZONTAL_CENTER ,) );
						$objPHPExcel->getActiveSheet()->getStyle('A9:J9')->applyFromArray($borders);
						$objPHPExcel->getActiveSheet()->getStyle('A10:J10')->applyFromArray($borders);
						$objPHPExcel->getActiveSheet()->getStyle("A9")->getFont()->getColor()->setRGB("ffffff");
						$objPHPExcel->getActiveSheet()->getStyle("A7")->getFont()->setBold(true);
						$objPHPExcel->setActiveSheetIndex(0)
						->setCellValue('A10', 'Id')
						->setCellValue('B10', "Fecha")
						->setCellValue('C10', "Nombre comprobante")
						->setCellValue('D10', "Nro")
						->setCellValue('E10', "C.C")
						->setCellValue('F10', "Tercero")
						->setCellValue('G10', "Detalle")
						->setCellValue('H10', "Fecha de conciliación")
						->setCellValue('I10', "Débito")
						->setCellValue('J10', "Crédito");
						$floatTotalDebito = 0;
						$floatTotalCredito = 0;
						foreach ($arrNoConciliados as $data) {
							$floatTotalDebito += $data['debito'];
							$floatTotalCredito += $data['credito'];
							$objPHPExcel->setActiveSheetIndex(0)
							->setCellValueExplicit ("A$row", $data['id_det'], PHPExcel_Cell_DataType :: TYPE_STRING)
							->setCellValueExplicit ("B$row", $data['fecha_format'], PHPExcel_Cell_DataType :: TYPE_STRING)
							->setCellValueExplicit ("C$row", $data['nombre_comp'], PHPExcel_Cell_DataType :: TYPE_STRING)
							->setCellValueExplicit ("D$row", $data['numerotipo'], PHPExcel_Cell_DataType :: TYPE_STRING)
							->setCellValueExplicit ("E$row", $data['centrocosto'], PHPExcel_Cell_DataType :: TYPE_STRING)
							->setCellValueExplicit ("F$row", $data['nombre_tercero'], PHPExcel_Cell_DataType :: TYPE_STRING)
							->setCellValueExplicit ("G$row", $data['concepto'], PHPExcel_Cell_DataType :: TYPE_STRING)
							->setCellValueExplicit ("H$row", $data['periodo_format'], PHPExcel_Cell_DataType :: TYPE_STRING)
							->setCellValueExplicit ("I$row", $data['debito'], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
							->setCellValueExplicit ("J$row", $data['credito'], PHPExcel_Cell_DataType :: TYPE_NUMERIC);
							$objPHPExcel->getActiveSheet()->getStyle("A$row:J$row")->applyFromArray($borders);
							$row++;
						}
						$objPHPExcel->setActiveSheetIndex(0)
						->mergeCells("A$row:H$row")
						->setCellValue("A$row", "Total")
						->setCellValue("I$row", $floatTotalDebito)
						->setCellValue("J$row", $floatTotalCredito);
						$objPHPExcel-> getActiveSheet ()
						-> getStyle ("A$row:G$row")
						-> getFill ()
						-> setFillType (PHPExcel_Style_Fill :: FILL_SOLID)
						-> getStartColor ()
						-> setRGB ('99ddff');
						$objPHPExcel->getActiveSheet()->getStyle("A$row:J$row")->applyFromArray($borders);
						$row++;
					}
					if(!empty($arrConciliados) && $strType == "true"){
						$objPHPExcel->setActiveSheetIndex(0)
						->mergeCells("A$row:J$row")
						->setCellValue("A$row", 'Conciliados');
						$objPHPExcel-> getActiveSheet ()
						-> getStyle ("A$row:J$row")
						-> getAlignment ()
						-> applyFromArray (array ( 'horizontal'  =>  PHPExcel_Style_Alignment :: HORIZONTAL_CENTER ,) );
						$objPHPExcel-> getActiveSheet ()
						-> getStyle ("A$row")
						-> getFill ()
						-> setFillType (PHPExcel_Style_Fill :: FILL_SOLID)
						-> getStartColor ()
						-> setRGB ('3399cc');

						$objPHPExcel->getActiveSheet()->getStyle("A$row:J$row")->applyFromArray($borders);
						$objPHPExcel->getActiveSheet()->getStyle("A$row")->getFont()->getColor()->setRGB("ffffff");
						$objPHPExcel->getActiveSheet()->getStyle("A$row")->getFont()->setBold(true);
						$row++;
						$objPHPExcel-> getActiveSheet ()
						-> getStyle ("A$row:J$row")
						-> getFill ()
						-> setFillType (PHPExcel_Style_Fill :: FILL_SOLID)
						-> getStartColor ()
						-> setRGB ('99ddff');
						$objPHPExcel-> getActiveSheet ()
						-> getStyle ("A$row:J$row")
						-> getAlignment ()
						-> applyFromArray (array ( 'horizontal'  =>  PHPExcel_Style_Alignment :: HORIZONTAL_CENTER ,) );
						$objPHPExcel->setActiveSheetIndex(0)
						->setCellValue("A$row", 'Id')
						->setCellValue("B$row", "Fecha")
						->setCellValue("C$row", "Nombre comprobante")
						->setCellValue("D$row", "Nro")
						->setCellValue("E$row", "C.C")
						->setCellValue("F$row", "Tercero")
						->setCellValue("G$row", "Detalle")
						->setCellValue("H$row", "Fecha de conciliación")
						->setCellValue("I$row", "Débito")
						->setCellValue("J$row", "Crédito");
						$row++;
						$floatTotalDebito = 0;
						$floatTotalCredito = 0;
						foreach ($arrConciliados as $data) {
							$floatTotalDebito += $data['debito'];
							$floatTotalCredito += $data['credito'];
							$objPHPExcel->setActiveSheetIndex(0)
							->setCellValueExplicit ("A$row", $data['id_det'], PHPExcel_Cell_DataType :: TYPE_STRING)
							->setCellValueExplicit ("B$row", $data['fecha_format'], PHPExcel_Cell_DataType :: TYPE_STRING)
							->setCellValueExplicit ("C$row", $data['nombre_comp'], PHPExcel_Cell_DataType :: TYPE_STRING)
							->setCellValueExplicit ("D$row", $data['numerotipo'], PHPExcel_Cell_DataType :: TYPE_STRING)
							->setCellValueExplicit ("E$row", $data['centrocosto'], PHPExcel_Cell_DataType :: TYPE_STRING)
							->setCellValueExplicit ("F$row", $data['nombre_tercero'], PHPExcel_Cell_DataType :: TYPE_STRING)
							->setCellValueExplicit ("G$row", $data['concepto'], PHPExcel_Cell_DataType :: TYPE_STRING)
							->setCellValueExplicit ("H$row", $data['periodo_format'], PHPExcel_Cell_DataType :: TYPE_STRING)
							->setCellValueExplicit ("I$row", $data['debito'], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
							->setCellValueExplicit ("J$row", $data['credito'], PHPExcel_Cell_DataType :: TYPE_NUMERIC);
							$objPHPExcel->getActiveSheet()->getStyle("A$row:J$row")->applyFromArray($borders);
							$row++;
						}
						$objPHPExcel->setActiveSheetIndex(0)
						->mergeCells("A$row:H$row")
						->setCellValue("A$row", "Total")
						->setCellValue("I$row", $floatTotalDebito)
						->setCellValue("J$row", $floatTotalCredito);
						$objPHPExcel-> getActiveSheet ()
						-> getStyle ("A$row:G$row")
						-> getFill ()
						-> setFillType (PHPExcel_Style_Fill :: FILL_SOLID)
						-> getStartColor ()
						-> setRGB ('99ddff');
						$objPHPExcel->getActiveSheet()->getStyle("A$row:J$row")->applyFromArray($borders);
					}
					$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(20);
					$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(20);
					$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(50);
					$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(50);
					$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(50);
					$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(50);

					//----Guardar documento----
					header('Content-Type: application/vnd.ms-excel');
					header('Content-Disposition: attachment;filename="conciliacion_bancaria.xlsx"');
					header('Cache-Control: max-age=0');
					$objWriter=PHPExcel_IOFactory::createWriter($objPHPExcel,'Excel2007');
					$objWriter->save('php://output');
				}
			}
			die();
		}
		public function exportPdf(){
			if(!empty($_SESSION)){

				$strType = $_POST['type'];
				$arrData = json_decode($_POST['data'],true);
				$arrConciliados = array_filter($arrData,function($e){return $e['periodo_format'] != "" && $e['is_show'];});
				$arrNoConciliados = array_filter($arrData,function($e){return $e['periodo_format'] == "" && $e['is_show'];});
				$arrFechaInicial = explode("-",$_POST['fecha_inicial']);
				$arrFechaFinal = explode("-",$_POST['fecha_final']);
				$strFechaInicial = $arrFechaInicial[2]."/".$arrFechaInicial[1]."/".$arrFechaInicial[0];
				$strFechaFinal = $arrFechaFinal[2]."/".$arrFechaFinal[1]."/".$arrFechaFinal[0];
				$strNombre = strClean($_POST['nombre']);
				$strCuenta = strClean($_POST['cuenta']);
				$floatExtractoCalculado = floatval($_POST['extracto_calculado']);
				$floatExtractBanco = floatval($_POST['extracto_banco']);
				$floatDiferencia = floatval($_POST['diferencia']);
				$floatSaldoInicial = floatval($_POST['saldo_inicial']);
				$floatSaldoFinal = floatval($_POST['saldo_final']);
				$floatCreditoConciliado = floatval($_POST['credito_conciliado']);
				$floatCreditoNoConciliado = floatval($_POST['credito_noconciliado']);
				$floatDebitoConciliado = floatval($_POST['debito_conciliado']);
				$floatDebitoNoConciliado = floatval($_POST['debito_noconciliado']);
				$arrNotas = selectNotas_all($strCuenta, $_POST['fecha_inicial'], $_POST['fecha_final']);
				
				$pdf = new MYPDF('P','mm','Letter', true, 'iso-8859-1', false);
				$pdf->SetDocInfoUnicode (true);
				// set document information
				$pdf->SetCreator(PDF_CREATOR);
				$pdf->SetAuthor('IDEALSAS');
				$pdf->SetTitle('CONCILIACIÓN BANCARIA');
				$pdf->SetSubject('CONCILIACIÓN BANCARIA');
				$pdf->SetKeywords('TCPDF, PDF, example, test, guide');
				$pdf->SetMargins(10, 38, 10);// set margins
				$pdf->SetHeaderMargin(38);// set margins
				$pdf->SetFooterMargin(17);// set margins
				$pdf->SetAutoPageBreak(TRUE, 20);// set auto page breaks
				// set some language-dependent strings (optional)
				if (@file_exists(dirname(__FILE__).'/lang/spa.php'))
				{
					require_once(dirname(__FILE__).'/lang/spa.php');
					$pdf->setLanguageArray($l);
				}
				$pdf->AddPage();
				$pdf->SetFillColor(51, 153, 204);
				$pdf->SetTextColor(255,255,255);
				$pdf->SetFont('Helvetica','b',9);
				$pdf->MultiCell(35,4,"Fecha","LRBT",'L',true,0,'','',true,0,false,true,0,'M',true);
				$pdf->SetFillColor(255,255,255);
				$pdf->SetTextColor(0,0,0);
				$pdf->SetFont('Helvetica','',9);
				$pdf->MultiCell(155,4,"Desde ".$strFechaInicial." Hasta ".$strFechaFinal,"RBTL",'L',true,0,'','',true,0,false,true,0,'M',true);
				$pdf->ln();
				$pdf->SetFillColor(51, 153, 204);
				$pdf->SetTextColor(255,255,255);
				$pdf->SetFont('Helvetica','b',9);
				$pdf->MultiCell(35,4,"Cuenta","LRBT",'L',true,0,'','',true,0,false,true,0,'M',true);
				$pdf->SetFillColor(255,255,255);
				$pdf->SetTextColor(0,0,0);
				$pdf->SetFont('Helvetica','',9);
				$pdf->MultiCell(155,4,$strCuenta."-".$strNombre,"RBTL",'L',true,0,'','',true,0,false,true,0,'M',true);
				$pdf->ln();
				$pdf->SetFillColor(51, 153, 204);
				$pdf->SetTextColor(255,255,255);
				$pdf->SetFont('Helvetica','b',9);
				$pdf->MultiCell(35,4,"Saldo extracto bancario","LRBT",'L',true,0,'','',true,0,false,true,0,'M',true);
				$pdf->SetFillColor(255,255,255);
				$pdf->SetTextColor(0,0,0);
				$pdf->SetFont('Helvetica','',9);
				$pdf->MultiCell(60,4,formatNum($floatExtractBanco, 2),"RBTL",'R',true,0,'','',true,0,false,true,0,'M',true);
				$pdf->SetFillColor(51, 153, 204);
				$pdf->SetTextColor(255,255,255);
				$pdf->SetFont('Helvetica','b',9);
				$pdf->MultiCell(23,4,"Saldo I ","LRBT",'L',true,0,'','',true,0,false,true,0,'M',true);
				$pdf->SetFillColor(255,255,255);
				$pdf->SetTextColor(0,0,0);
				$pdf->SetFont('Helvetica','',9);
				$pdf->MultiCell(25,4,formatNum($floatSaldoInicial, 2),"RBTL",'R',true,0,'','',true,0,false,true,0,'M',true);
				$pdf->SetFillColor(51, 153, 204);
				$pdf->SetTextColor(255,255,255);
				$pdf->SetFont('Helvetica','b',9);
				$pdf->MultiCell(22,4,"Saldo F ","LRBT",'L',true,0,'','',true,0,false,true,0,'M',true);
				$pdf->SetFillColor(255,255,255);
				$pdf->SetTextColor(0,0,0);
				$pdf->SetFont('Helvetica','',9);
				$pdf->MultiCell(25 ,4,formatNum($floatSaldoFinal, 2),"RBTL",'R',true,0,'','',true,0,false,true,0,'M',true);
				$pdf->ln();
				$pdf->SetFillColor(51, 153, 204);
				$pdf->SetTextColor(255,255,255);
				$pdf->SetFont('Helvetica','b',9);
				$pdf->MultiCell(95,4,"No conciliados","LRBT",'C',true,0,'','',true,0,false,true,0,'M',true);
				$pdf->MultiCell(95,4,"Conciliados","LRBT",'C',true,0,'','',true,0,false,true,0,'M',true);
				$pdf->ln();
				$pdf->SetFillColor(153,221,255);
				$pdf->SetFont('helvetica','B',9);
				$pdf->SetTextColor(0,0,0);
				$pdf->MultiCell(35,4,"Débito","LRBT",'L',true,0,'','',true,0,false,true,0,'M',true);
				$pdf->SetFillColor(255,255,255);
				$pdf->SetTextColor(0,0,0);
				$pdf->SetFont('Helvetica','',9);
				$pdf->MultiCell(22,4,formatNum($floatDebitoNoConciliado, 2),"RBTL",'R',true,0,'','',true,0,false,true,0,'M',true);
				$pdf->SetFillColor(153,221,255);
				$pdf->SetFont('helvetica','B',9);
				$pdf->SetTextColor(0,0,0);
				$pdf->MultiCell(16,4,"Crédito","LRBT",'L',true,0,'','',true,0,false,true,0,'M',true);
				$pdf->SetFillColor(255,255,255);
				$pdf->SetTextColor(0,0,0);
				$pdf->SetFont('Helvetica','',9);
				$pdf->MultiCell(22,4,formatNum($floatCreditoNoConciliado, 2),"RBTL",'R',true,0,'','',true,0,false,true,0,'M',true);
				$pdf->SetFillColor(153,221,255);
				$pdf->SetFont('helvetica','B',9);
				$pdf->SetTextColor(0,0,0);
				$pdf->MultiCell(23,4,"Débito","LRBT",'L',true,0,'','',true,0,false,true,0,'M',true);
				$pdf->SetFillColor(255,255,255);
				$pdf->SetTextColor(0,0,0);
				$pdf->SetFont('Helvetica','',9);
				$pdf->MultiCell(25,4,formatNum($floatDebitoConciliado, 2),"RBTL",'R',true,0,'','',true,0,false,true,0,'M',true);
				$pdf->SetFillColor(153,221,255);
				$pdf->SetFont('helvetica','B',9);
				$pdf->SetTextColor(0,0,0);
				$pdf->MultiCell(22,4,"Crédito","LRBT",'L',true,0,'','',true,0,false,true,0,'M',true);
				$pdf->SetFillColor(255,255,255);
				$pdf->SetTextColor(0,0,0);
				$pdf->SetFont('Helvetica','',9);
				$pdf->MultiCell(25,4,formatNum($floatCreditoConciliado, 2),"RBTL",'R',true,0,'','',true,0,false,true,0,'M',true);
				$pdf->ln();
				$pdf->SetFillColor(51, 153, 204);
				$pdf->SetTextColor(255,255,255);
				$pdf->SetFont('Helvetica','b',9);
				$pdf->MultiCell(35,4,"Saldo extracto calculado	","LRBT",'L',true,0,'','',true,0,false,true,0,'M',true);
				$pdf->SetFillColor(255,255,255);
				$pdf->SetTextColor(0,0,0);
				$pdf->SetFont('Helvetica','',9);
				$pdf->MultiCell(60,4,formatNum($floatExtractoCalculado, 2),"RBTL",'R',true,0,'','',true,0,false,true,0,'M',true);
				$pdf->SetFillColor(51, 153, 204);
				$pdf->SetTextColor(255,255,255);
				$pdf->SetFont('Helvetica','b',9);
				$pdf->MultiCell(23,4,"Diferencia","LRBT",'L',true,0,'','',true,0,false,true,0,'M',true);
				$pdf->SetFillColor(255,255,255);
				$pdf->SetTextColor(0,0,0);
				$pdf->SetFont('Helvetica','',9);
				$pdf->MultiCell(25,4,formatNum($floatDiferencia, 2),"RBTL",'R',true,0,'','',true,0,false,true,0,'M',true);
				$pdf->SetFillColor(51, 153, 204);
				$pdf->SetTextColor(255,255,255);
				$pdf->SetFont('Helvetica','b',9);
				$pdf->MultiCell(22,4,"Estado","LRBT",'L',true,0,'','',true,0,false,true,0,'M',true);
				$pdf->SetFillColor(255,255,255);
				$pdf->SetTextColor(0,0,0);
				$pdf->SetFont('Helvetica','',9);
				$pdf->MultiCell(25 ,4,$floatDiferencia >= 0 && $floatDiferencia <= 1 ? "Conciliado" : "No conciliado","RBTL",'R',true,0,'','',true,0,false,true,0,'M',true);
				$pdf->ln();
				if(!empty($arrNoConciliados)){
					$pdf->MultiCell(190,4,"No conciliados","LRBT",'C',true,0,'','',true,0,false,true,0,'M',true);
					$pdf->ln();
					$floatTotalDebito = 0;
					$floatTotalCredito = 0;
					$pdf->SetFillColor(153,221,255);
					$pdf->SetFont('helvetica','B',6);
					$pdf->SetTextColor(0,0,0);
					$pdf->MultiCell(10,6,"Id","LRBT",'C',true,0,'','',true,0,false,true,0,'M',true);
					$pdf->MultiCell(15,6,"Fecha","LRBT",'C',true,0,'','',true,0,false,true,0,'M',true);
					$pdf->MultiCell(30,6,"Nombre comp","LRBT",'C',true,0,'','',true,0,false,true,0,'M',true);
					$pdf->MultiCell(10,6,"Nro","LRBT",'C',true,0,'','',true,0,false,true,0,'M',true);
					$pdf->MultiCell(10,6,"C.C","LRBT",'C',true,0,'','',true,0,false,true,0,'M',true);
					$pdf->MultiCell(25,6,"Tercero","LRBT",'C',true,0,'','',true,0,false,true,0,'M',true);
					$pdf->MultiCell(35,6,"Detalle","LRBT",'C',true,0,'','',true,0,false,true,0,'M',true);
					$pdf->MultiCell(15,6,"Fecha conciliación","LRBT",'C',true,0,'','',true,0,false,true,0,'M',true);
					$pdf->MultiCell(20,6,"Débito","LRBT",'C',true,0,'','',true,0,false,true,0,'M',true);
					$pdf->MultiCell(20,6,"Crédito","LRBT",'C',true,0,'','',true,0,false,true,0,'M',true);
					$pdf->ln();
					$fill = true;
					foreach ($arrNoConciliados as $data) {
						$floatTotalDebito += $data['debito'];
						$floatTotalCredito += $data['credito'];
						$pdf->SetFillColor(245,245,245);
						$pdf->SetFont('helvetica','',6);
						$nombre = $pdf->getNumLines($data['nombre_comp'],30);
						$concepto = $pdf->getNumLines($data['concepto'],25);
						$nombreTercero = $pdf->getNumLines($data['nombre_tercero'],25);
						$max = max($nombre,$nombreTercero,$concepto);
						$height = 3*$max;
						$pdf->MultiCell(10,$height,$data['id_det'],"",'L',$fill,0,'','',true,0,false,true,0,'M',true);
						$pdf->MultiCell(15,$height,$data['fecha_format'],"",'L',$fill,0,'','',true,0,false,true,0,'M',true);
						$pdf->MultiCell(30,$height,$data['nombre_comp'],"",'L',$fill,0,'','',true,0,false,true,0,'M',true);
						$pdf->MultiCell(10,$height,$data['numerotipo'],"",'C',$fill,0,'','',true,0,false,true,0,'M',true);
						$pdf->MultiCell(10,$height,$data['centrocosto'],"",'C',$fill,0,'','',true,0,false,true,0,'M',true);
						$pdf->MultiCell(25,$height,$data['nombre_tercero'],"",'L',$fill,0,'','',true,0,false,true,0,'M',true);
						$pdf->MultiCell(35,$height,$data['concepto'],"",'L',$fill,0,'','',true,0,false,true,0,'M',true);
						$pdf->MultiCell(15,$height,$data['periodo_format'],"",'C',$fill,0,'','',true,0,false,true,0,'M',true);
						$pdf->MultiCell(20,$height,formatNum($data['debito'], 2),"",'R',$fill,0,'','',true,0,false,true,0,'M',true);
						$pdf->MultiCell(20,$height,formatNum($data['credito'], 2),"",'R',$fill,0,'','',true,0,false,true,0,'M',true);
						$pdf->ln();
						$fill = !$fill;
						if($pdf->GetY()>230){
							$pdf->AddPage();
						}
					}
					$pdf->SetFillColor(153,221,255);
					$pdf->SetFont('helvetica','B',6);
					$pdf->SetTextColor(0,0,0);
					$pdf->MultiCell(150,4,"Total","",'R',true,0,'','',true,0,false,true,0,'M',true);

					$pdf->SetFillColor(255,255,255);
					$pdf->SetTextColor(0,0,0);
					$pdf->MultiCell(20,4,formatNum($floatTotalDebito, 2),"",'R',0,0,'','',true,0,false,true,0,'M',true);
					$pdf->MultiCell(20,4,formatNum($floatTotalCredito, 2),"",'R',0,0,'','',true,0,false,true,0,'M',true);
				}
				if(!empty($arrConciliados) && $strType =="true"){
					$floatTotalDebito = 0;
					$floatTotalCredito = 0;
					$pdf->ln();
					$pdf->ln();
					$pdf->SetFillColor(51, 153, 204);
					$pdf->SetTextColor(255,255,255);
					$pdf->SetFont('Helvetica','b',9);
					$pdf->MultiCell(190,4,"Conciliados","LRBT",'C',true,0,'','',true,0,false,true,0,'M',true);
					$pdf->ln();
					$pdf->SetFillColor(153,221,255);
					$pdf->SetFont('helvetica','B',6);
					$pdf->SetTextColor(0,0,0);
					$pdf->MultiCell(10,6,"Id","LRBT",'C',true,0,'','',true,0,false,true,0,'M',true);
					$pdf->MultiCell(15,6,"Fecha","LRBT",'C',true,0,'','',true,0,false,true,0,'M',true);
					$pdf->MultiCell(30,6,"Nombre comp","LRBT",'C',true,0,'','',true,0,false,true,0,'M',true);
					$pdf->MultiCell(10,6,"Nro","LRBT",'C',true,0,'','',true,0,false,true,0,'M',true);
					$pdf->MultiCell(10,6,"C.C","LRBT",'C',true,0,'','',true,0,false,true,0,'M',true);
					$pdf->MultiCell(25,6,"Tercero","LRBT",'C',true,0,'','',true,0,false,true,0,'M',true);
					$pdf->MultiCell(35,6,"Detalle","LRBT",'C',true,0,'','',true,0,false,true,0,'M',true);
					$pdf->MultiCell(15,6,"Fecha conciliación","LRBT",'C',true,0,'','',true,0,false,true,0,'M',true);
					$pdf->MultiCell(20,6,"Débito","LRBT",'C',true,0,'','',true,0,false,true,0,'M',true);
					$pdf->MultiCell(20,6,"Crédito","LRBT",'C',true,0,'','',true,0,false,true,0,'M',true);
					$pdf->ln();
					$fill = true;
					foreach ($arrConciliados as $data) {
						$floatTotalDebito += $data['debito'];
						$floatTotalCredito += $data['credito'];
						$pdf->SetFillColor(245,245,245);
						$pdf->SetFont('helvetica','',6);
						$nombre = $pdf->getNumLines($data['nombre_comp'],30);
						$concepto = $pdf->getNumLines($data['concepto'],25);
						$nombreTercero = $pdf->getNumLines($data['nombre_tercero'],25);
						$max = max($nombre,$nombreTercero,$concepto);
						$height = 3*$max;
						$pdf->MultiCell(10,$height,$data['id_det'],"",'L',$fill,0,'','',true,0,false,true,0,'M',true);
						$pdf->MultiCell(15,$height,$data['fecha_format'],"",'L',$fill,0,'','',true,0,false,true,0,'M',true);
						$pdf->MultiCell(30,$height,$data['nombre_comp'],"",'L',$fill,0,'','',true,0,false,true,0,'M',true);
						$pdf->MultiCell(10,$height,$data['numerotipo'],"",'C',$fill,0,'','',true,0,false,true,0,'M',true);
						$pdf->MultiCell(10,$height,$data['centrocosto'],"",'C',$fill,0,'','',true,0,false,true,0,'M',true);
						$pdf->MultiCell(25,$height,$data['nombre_tercero'],"",'L',$fill,0,'','',true,0,false,true,0,'M',true);
						$pdf->MultiCell(35,$height,$data['concepto'],"",'L',$fill,0,'','',true,0,false,true,0,'M',true);
						$pdf->MultiCell(15,$height,$data['periodo_format'],"",'C',$fill,0,'','',true,0,false,true,0,'M',true);
						$pdf->MultiCell(20,$height,formatNum($data['debito'], 2),"",'R',$fill,0,'','',true,0,false,true,0,'M',true);
						$pdf->MultiCell(20,$height,formatNum($data['credito'], 2),"",'R',$fill,0,'','',true,0,false,true,0,'M',true);
						$pdf->ln();
						$fill = !$fill;
						if($pdf->GetY()>230){
							$pdf->AddPage();
						}
					}
					$pdf->SetFillColor(153,221,255);
					$pdf->SetFont('helvetica','B',6);
					$pdf->SetTextColor(0,0,0);
					$pdf->MultiCell(150,4,"Total","",'R',true,0,'','',true,0,false,true,0,'M',true);

					$pdf->SetFillColor(255,255,255);
					$pdf->SetTextColor(0,0,0);
					$pdf->MultiCell(20,4,formatNum($floatTotalDebito, 2),"",'R',0,0,'','',true,0,false,true,0,'M',true);
					$pdf->MultiCell(20,4,formatNum($floatTotalCredito, 2),"",'R',0,0,'','',true,0,false,true,0,'M',true);
				}
				$pdf->ln();
				if (!empty($arrNotas)) {
					$pdf->MultiCell(190,4,"Observaciones","LRBT",'C',true,0,'','',true,0,false,true,0,'M',true);
					$pdf->ln();
					$pdf->SetFillColor(153,221,255);
					$pdf->SetFont('helvetica','B',6);
					$pdf->SetTextColor(0,0,0);
					$pdf->MultiCell(10,6,"No","LRBT",'C',true,0,'','',true,0,false,true,0,'M',true);
					$pdf->MultiCell(15,6,"Fecha","LRBT",'C',true,0,'','',true,0,false,true,0,'M',true);
					$pdf->MultiCell(30,6,"Nombre comp","LRBT",'C',true,0,'','',true,0,false,true,0,'M',true);
					$pdf->MultiCell(10,6,"Número","LRBT",'C',true,0,'','',true,0,false,true,0,'M',true);
					$pdf->MultiCell(75,6,"Descripción","LRBT",'C',true,0,'','',true,0,false,true,0,'M',true);
					$pdf->MultiCell(25,6,"Debito","LRBT",'C',true,0,'','',true,0,false,true,0,'M',true);
					$pdf->MultiCell(25,6,"credito","LRBT",'C',true,0,'','',true,0,false,true,0,'M',true);
					$pdf->ln();
					$conta = 1;
					$fill = true;
					foreach ($arrNotas as $nota) {
						$pdf->SetFillColor(245,245,245);
						$pdf->SetFont('helvetica','',6);
						$nomComp = $pdf->getNumLines($nota['nom_comprobante'],30);
						$numComp = $pdf->getNumLines($nota['num_comprobante'],25);
						$descripcion = $pdf->getNumLines($nota['nota'],75);
						$max = max($nomComp,$numComp,$descripcion);
						$height = 3*$max;
						$pdf->MultiCell(10,$height,$conta,"",'L',$fill,0,'','',true,0,false,true,0,'M',true);
						$pdf->MultiCell(15,$height,$nota['fechaaux'],"",'L',$fill,0,'','',true,0,false,true,0,'M',true);
						$pdf->MultiCell(30,$height,$nota['tiponota'] == 'G'  ? "Observación General" : $nota['nom_comprobante'],"",'L',$fill,0,'','',true,0,false,true,0,'M',true);
						$pdf->MultiCell(10,$height,$nota['num_comprobante'],"",'C',$fill,0,'','',true,0,false,true,0,'M',true);
						$pdf->MultiCell(75,$height,$nota['nota'],"",'L',$fill,0,'','',true,0,false,true,0,'M',true);
						$pdf->MultiCell(25,$height,formatNum($nota['debito'], 2),"",'R',$fill,0,'','',true,0,false,true,0,'M',true);
						$pdf->MultiCell(25,$height,formatNum($nota['credito'], 2),"",'R',$fill,0,'','',true,0,false,true,0,'M',true);
						$conta++;
						$pdf->ln();
						$fill = !$fill;
						if($pdf->GetY()>230){
							$pdf->AddPage();
						}
					}
				}
				$pdf->ln();
				$pdf->Output('declaracion_retenciones.pdf', 'I');
			}
			die();
		}

	}
	class MYPDF extends TCPDF {

		public function Header()
		{
			$request = configBasica();
			$strNit = $request['nit'];
			$strRazon = $request['razonsocial'];

			//Parte Izquierda
			$this->Image('../../../imagenes/escudo.jpg',  13, 13, 20, 20, 'JPG', '', 'T', true, 300, '', false, false, 0, false, false, false);// Logo
			$this->SetFont('helvetica','B',8);
			$this->SetY(10);
			$this->RoundedRect(10, 10, 190, 25, 1,'');
			$this->Cell(0.1);
			$this->Cell(26,25,'','R',0,'L');
			$this->SetY(8);
			$this->SetX(40);
			$this->SetFont('helvetica','B',9);
			$this->Cell(140,15,strtoupper("$strRazon"),0,0,'C');
			$this->SetFont('helvetica','B',7);
			$this->SetY(12);
			$this->SetX(40);
			$this->Cell(140,15,'NIT: '.$strNit,0,0,'C');
			//*****************************************************************************************************************************
			$this->SetFont('helvetica','B',9);
			$this->SetY(23);
			$this->SetX(36);
			$this->Cell(164,12,"CONCILIACIÓN BANCARIA",'T',0,'C');


			$this->SetFont('helvetica','B',7);
			$this->SetY(10);
			$this->SetX(167);
			$this->Cell(30,7," FECHA: ". date("d/m/Y"),"L",0,'L');
			$this->SetY(17);
			$this->SetX(167);
			$this->Cell(35,6,"","L",0,'L');

			//**********************************************************
			$this->SetFont('times','B',10);
			$this->ln(12);
			//**********************************************************
			if(MOV=="401"){
				$img_file = './assets/img/reversado.png';
				$this->Image($img_file, 0, 20, 250, 280, '', '', '', false, 300, '', false, false, 0);
			}
		}
		public function Footer(){

			$request = configBasica();
			$strDireccion = $request['direccion'] != "" ? "Dirección: ".strtoupper($request['direccion']) : "";
			$strWeb = $request['web'] != "" ? "Pagina web: ".strtoupper($request['web']) :"";
			$strEmail = $request['email'] !="" ? "Email: ".strtoupper($request['email']) :"";
			$strTelefono = $request['telefono'] != "" ? "Telefonos: ".$request['telefono'] : "";
			$strUsuario = searchUser($_SESSION['cedulausu'])['nom_usu'];
			$strNick = $_SESSION['nickusu'];
			$strFecha = date("d/m/Y H:i:s");
			$strIp = $_SERVER['REMOTE_ADDR'];

			$this->SetFont('helvetica', 'I', 8);
			$txt = <<<EOD
			$strDireccion $strTelefono
			$strEmail $strWeb
			EOD;
			$this->SetFont('helvetica', 'I', 6);
			$this->Cell(277,10,'','T',0,'T');
			$this->ln(2);
			$this->Write(0, $txt , '', 0, 'C', true, 0, false, false, 0);

			$this->Cell(50, 10, 'Hecho por: '.$strUsuario, 00, false, 'C', 0, '', 0, false, 'T', 'M');
			$this->Cell(30, 10, 'Impreso por: '.$strNick, 0, false, 'C', 0, '', 0, false, 'T', 'M');
			$this->Cell(30, 10, 'IP: '.$strIp, 0, false, 'C',0, '', 0, false, 'T', 'M');
			$this->Cell(30, 10, 'Fecha: '.$strFecha, 0, false, 'C', 0, '', 0, false, 'T', 'M');
			$this->Cell(30, 10, 'IDEAL.10 S.A.S', 0, false, 'C', 0, '', 0, false, 'T', 'M');
			$this->Cell(20, 10, 'Pagina '.$this->getAliasNumPage().'/'.$this->getAliasNbPages(), 0, false, 'C', 0, '', 0, false, 'T', 'M');
		}
	}

	if($_POST){
		$obj = new ConciliacionExportControler();
		if($_POST['action'] == "excel"){
			$obj->exportExcel();
		}else if($_POST['action'] == "pdf"){
			$obj->exportPdf();
		}
	}

?>
