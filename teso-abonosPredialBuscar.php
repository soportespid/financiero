<?php

    header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=utf8");
	require 'comun.inc';
	require 'funciones.inc';
	session_start();
    if(empty($_SESSION)){
        header("location: index.php");
    }
	$linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");
	cargarcodigopag($_GET['codpag'],$_SESSION["nivel"]);
	date_default_timezone_set("America/Bogota");
?>
<!DOCTYPE >
<html lang=es>
	<head>
    <meta charset="utf-8"/>
		<meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1"/>
		<meta name="viewport" content="user-scalable=no">
		<title>:: IDEAL 10 - Tesoreria</title>
		<link href="favicon.ico" rel="shortcut icon"/>
		<link href="css/css2z.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
		<link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
		<link href="css/css4.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
        <link href="css/tabs.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css"/>
        <link href="css/style.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css"/>
        <link rel="stylesheet" href="sweetalert2/dist/sweetalert2.min.css">
        <script type="text/javascript" src="css/programas.js"></script>
        <script src="sweetalert2/dist/sweetalert2.min.js"></script>
		<script src="vue/vue.min.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script src="css/programas.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script src="css/calendario.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
        <style>
            .marcaRecibo{
                background-color: #ffc107 !important;
                color:#000;
                font-weight: bold;
            }
        </style>
	</head>
	<body>
		<header>
			<table>
				<tr><script>barra_imagenes("teso");</script><?php cuadro_titulos();?></tr>
			</table>
		</header>

		<form name="form2" method="post" action="">
			<section id="myapp" v-cloak >
                <div class="loading-container" v-show="isLoading" >
                    <p class="text-loading"data-loading="IDEAL 10">IDEAL 10</p>
                </div>
				<nav>
					<table>
						<tr><?php menu_desplegable("teso");?></tr>
					</table>
                    <div class="bg-white group-btn p-1" id="newNavStyle">
                        <button type="button" class="btn btn-white btn-primary-hover d-flex justify-between align-items-center" @click="window.location.href='teso-abonosPredialCrear.php'">
                            <span>Nuevo</span>
                            <svg  xmlns="http://www.w3.org/2000/svg"  viewBox="0 -960 960 960" ><path d="M440-440H200v-80h240v-240h80v240h240v80H520v240h-80v-240Z"/></svg>
                        </button>
                        <button type="button" class="btn btn-white btn-primary-hover d-flex justify-between align-items-center" @click="mypop=window.open('teso-principal.php','',''); mypop.focus();">
                            <span>Nueva ventana</span>
                            <svg class="fill-black group-hover:fill-white w-5 h-5" xmlns="http://www.w3.org/2000/svg"  viewBox="0 -960 960 960" ><path d="M320-120v-80h80v-80H160q-33 0-56.5-23.5T80-360v-400q0-33 23.5-56.5T160-840h640q33 0 56.5 23.5T880-760v400q0 33-23.5 56.5T800-280H560v80h80v80H320ZM160-360h640v-400H160v400Zm0 0v-400 400Z"/></svg>
                        </button>
                        <button type="button" class="btn btn-success btn-success-hover d-flex justify-between align-items-center" @click="printExcel">
                            <span>Exportar Excel</span>
                            <svg class="fill-black group-hover:fill-white w-4 h-4" xmlns="http://www.w3.org/2000/svg"  viewBox="0 0 384 512"><!--!Font Awesome Free 6.5.1 by @fontawesome - https://fontawesome.com License - https://fontawesome.com/license/free Copyright 2024 Fonticons, Inc. --><path d="M48 448V64c0-8.8 7.2-16 16-16H224v80c0 17.7 14.3 32 32 32h80V448c0 8.8-7.2 16-16 16H64c-8.8 0-16-7.2-16-16zM64 0C28.7 0 0 28.7 0 64V448c0 35.3 28.7 64 64 64H320c35.3 0 64-28.7 64-64V154.5c0-17-6.7-33.3-18.7-45.3L274.7 18.7C262.7 6.7 246.5 0 229.5 0H64zm90.9 233.3c-8.1-10.5-23.2-12.3-33.7-4.2s-12.3 23.2-4.2 33.7L161.6 320l-44.5 57.3c-8.1 10.5-6.3 25.5 4.2 33.7s25.5 6.3 33.7-4.2L192 359.1l37.1 47.6c8.1 10.5 23.2 12.3 33.7 4.2s12.3-23.2 4.2-33.7L222.4 320l44.5-57.3c8.1-10.5 6.3-25.5-4.2-33.7s-25.5-6.3-33.7 4.2L192 280.9l-37.1-47.6z"/></svg>
                        </button>
                        <button type="button" class="btn btn-success d-flex justify-between align-items-center" @click="window.location.href='teso-gestioncobroporcuotas.php'">
                            <span>Atrás</span>
                            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 -960 960 960"><path d="M760-200v-160q0-50-35-85t-85-35H273l144 144-57 56-240-240 240-240 57 56-144 144h367q83 0 141.5 58.5T840-360v160h-80Z"/></svg>
                        </button>
                    </div>
				</nav>
				<article>
                    <div class="bg-white">
                        <div>
                            <h2 class="titulos m-0">Buscar abonos acuerdo prediales</h2>
                            <div class="d-flex">
                                <div class="form-control w-50">
                                    <label class="form-label">Buscar:</label>
                                    <input type="search" placeholder="buscar por abono, acuerdo o contribuyente" v-model="txtSearch" @keyup="search">
                                </div>
                                <div class="form-control w-50">
                                    <label class="form-label">Convenciones:</label>
                                    <span class="badge badge-warning w-25">Liquidaciones sin recibos de caja</span>
                                </div>
                            </div>
                            <h2 class="titulos m-0">Resultados: {{ txtResults }} </h2>
                        </div>
                        <div class="overflow-auto max-vh-50 overflow-x-hidden p-2" >
                            <table class="table table-hover fw-normal">
                                <thead>
                                    <tr>
                                        <th class="text-center">No. Abono</th>
                                        <th class="text-center">No. Acuerdo</th>
                                        <th class="text-center">No. Liquidacion</th>
                                        <th class="text-center">No. Recibo de caja</th>
                                        <th>Fecha</th>
                                        <th>Concepto</th>
                                        <th>Documento</th>
                                        <th>Nombre</th>
                                        <th class="text-right">Valor</th>
                                        <th class="text-center">Estado</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr v-for="(data,index) in arrDataCopy" :key="index" @dblclick="editItem(data.idabono)"
                                    :class="[data.cierre > 0 && data.recibo=='N/A' ? 'marcaRecibo' : '']">
                                        <td class="text-center">{{data.idabono}}</td>
                                        <td class="text-center">{{data.idacuerdo}}</td>
                                        <td class="text-center">{{data.cierre}}</td>
                                        <td class="text-center">{{data.recibo}}</td>
                                        <td>{{data.fecha}}</td>
                                        <td>{{data.concepto}}</td>
                                        <td>{{data.documento}}</td>
                                        <td>{{data.nombre}}</td>
                                        <td class="text-right">{{formatNumero(data.total)}}</td>
                                        <td class="text-center"><span :class="[data.tipomovimiento == '201' ? 'badge-success' : 'badge-danger']"class="badge">{{ data.tipomovimiento == "201" ? "Activo" : "Reversado"}}</span></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
				</article>
			</section>
		</form>

		<script src="node_modules/read-excel-file/bundle/read-excel-file.min.js"></script>
		<script src="node_modules/xlsx/dist/xlsx.full.min.js"></script>
        <script src="Librerias/vue/vue.min.js"></script>
		<script src="Librerias/vue/axios.min.js"></script>
		<script type="module" src="tesoreria/predial_abonos/buscar/teso-abonosPredialBuscar.js?<?= date('d_m_Y_h_i_s');?>"></script>

	</body>
</html>
