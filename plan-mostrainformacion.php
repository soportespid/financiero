<?php 
	header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=utf8");
	require 'comun.inc';
	require 'funciones.inc';
	sesion();
	$linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");
	cargarcodigopag($_GET['codpag'],$_SESSION['nivel']);
	date_default_timezone_set("America/Bogota");
?>
<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <title>Planeación Estratégica - IDEAL 10</title>

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@400;500;700&display=swap" rel="stylesheet">

    <!-- Estilos Modernos -->
    <style>
        body {
            margin: 0;
            padding: 0;
            font-family: 'Roboto', sans-serif;
            background-color: #f0f2f5;
            color: #333;
            display: flex;
            justify-content: center;
            align-items: center;
            min-height: 100vh;
        }

        .container {
            background: #ffffff;
            border-radius: 8px;
            box-shadow: 0 4px 12px rgba(0, 0, 0, 0.1);
            width: 100%;
            max-width: 90%;
            padding: 20px;
        }

        .header {
            display: flex;
            justify-content: space-between;
            align-items: center;
            margin-bottom: 20px;
            border-bottom: 2px solid #e5e5e5;
            padding-bottom: 10px;
        }

        .header .title {
            font-size: 24px;
            font-weight: 500;
            color: #555;
        }

        .content {
            display: flex;
            flex-direction: column;
        }

        .title-section {
            background-color: #f7f7f7;
            padding: 15px;
            border-radius: 5px;
            margin-bottom: 20px;
            box-shadow: 0 2px 8px rgba(0, 0, 0, 0.05);
        }

        .title-section h2 {
            margin: 0;
            font-size: 22px;
            font-weight: 600;
            color: #444;
            text-align: center;
        }

        .text-image-container {
            display: flex;
            gap: 20px;
            align-items: flex-start;
        }

        .text-area {
            flex: 2;
            padding: 10px;
            border: 1px solid #ddd;
            border-radius: 5px;
            background-color: #fafafa;
            resize: none;
            height: 300px;
            min-width: 300px;
            box-shadow: inset 0 2px 8px rgba(0, 0, 0, 0.03);
            transition: box-shadow 0.3s ease;
            overflow-y: scroll;
            text-align: justify; 
            white-space: pre-wrap;
            overflow-y: auto;
        }
        .text-area:hover {
            box-shadow: inset 0 2px 12px rgba(0, 0, 0, 0.1);
        }

        .image-container {
            flex: 1;
            background-size: contain;
            background-position: center;
            background-repeat: no-repeat;
            border-radius: 5px;
            min-width: 300px;
            height: 300px;
            box-shadow: 0 4px 12px rgba(0, 0, 0, 0.1);
        }

        .links {
            margin-top: 20px;
            text-align: center;
        }

        .links p {
            margin: 0;
            font-size: 16px;
            font-weight: 500;
        }

        .links a {
            color: #007bff;
            text-decoration: none;
            font-weight: 500;
        }

        .links a:hover {
            text-decoration: underline;
        }

        @media (max-width: 768px) {
            .text-image-container {
                flex-direction: column;
            }

            .image-container {
                min-width: 100%;
            }
        }
    </style>
</head>

<body>
    <form name="formulario">
        <?php 
            $_POST['oculto'] = $_GET['idinfo'];
            $sqlr = "SELECT * FROM infor_interes WHERE indices='".$_POST['oculto']."'";
            $res = mysqli_query($linkbd,$sqlr);
            $rowEmp = mysqli_fetch_assoc($res);
            $titulos = strtoupper($rowEmp['titulos']);
            $imagenes = $rowEmp['imgnombre'] ? "informacion/imagenes/".$rowEmp['imgnombre'] : "";
            $txtarchivo = "informacion/archivos/".$rowEmp['texnombre'];
            $arcadjunto = $rowEmp['adjunto'] ? $rowEmp['adjunto'] : "";
            $nombreLink = $rowEmp['link'];

            $tipoletratitulo = $rowEmp['tipoletrat'];
            $formatoletratitulo = $rowEmp['formatoletrat'];
            $tamanoletratitulo = $rowEmp['tamanoletrat'];
            $colorletratitulo = $rowEmp['colorletrat'];
            $colorfondotitulo = $rowEmp['colorfondot'];

            $tipoletratex = $rowEmp['tipoletrad'];
            $formatoletratx = $rowEmp['formatoletrad'];
            $tamanoletratx = $rowEmp['tamanoletrad'];
            $colorletratx = $rowEmp['colorletrad'];
            $colorfondotx = $rowEmp['colorfondod'];

            $sicaptura = 0;

            try {
                $ar = fopen($txtarchivo, "r");
                if (!$ar) {
                    throw new Exception("");
                }

                while (!feof($ar)) {
                    $linea = fgets($ar);
                    $_POST['gradescr'] .= str_replace('<br />', '', nl2br($linea));
                }
                fclose($ar);
            } catch (Exception $e) {
                $_POST['gradescr'] = "No se pudo abrir el archivo";
            }
        ?>

        <div class="container">
            <div class="content">
                <div class="title-section" style="background-color: <?php echo $colorfondotitulo; ?>">
                    <h2 style="font-family: <?php echo $tipoletratitulo; ?>; font-style: <?php echo $formatoletratitulo; ?>; font-size: <?php echo $tamanoletratitulo; ?>; color: <?php echo $colorletratitulo; ?>">
                        <?php echo $titulos; ?>
                    </h2>
                </div>

                <div class="text-image-container">
                    <textarea id="gradescr" name="gradescr" class="text-area" readonly style="font-family: <?php echo $tipoletratex; ?>; font-style: <?php echo $formatoletratx; ?>; font-size: <?php echo $tamanoletratx; ?>; color: <?php echo $colorletratx; ?>; background-color: <?php echo $colorfondotx; ?>; text-align: justify; white-space: pre-wrap;"><?php echo $_POST['gradescr']; ?></textarea>
                    

                    <?php if ($imagenes): ?>
                        <div class="image-container" style="background-image: url('<?php echo $imagenes; ?>');"></div>
                    <?php endif; ?>
                </div>

                <div class="links">
                    <?php if($nombreLink): ?>
                        <p>Link: <a href="<?php echo $nombreLink; ?>" target="_blank">Revisar aquí</a></p>
                    <?php endif; ?>
                    <?php if($arcadjunto): ?>
                        <p>Archivo Adjunto: <a href="informacion/adjuntos/<?php echo $arcadjunto; ?>" download><?php echo $arcadjunto; ?> <img src="imagenes/descargar.png"></a></p>
                    <?php endif; ?>
                </div>
            </div>
        </div>

        <input id="oculto" name="oculto" type="hidden" value="<?php echo $_POST['oculto']; ?>">
    </form>
</body>

</html>
