<<?php
	header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=utf8");
	require "comun.inc";
	require "funciones.inc";
	session_start();
	$linkbd=conectar_v7();	
	$linkbd -> set_charset("utf8");
	cargarcodigopag($_GET['codpag'],$_SESSION["nivel"]);
	date_default_timezone_set("America/Bogota");
?>
<!DOCTYPE >
<html lang="es">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1">
		<meta name="viewport" content="user-scalable=no">
		<title>:: IDEAL 10 - Tesorer&iacute;a</title>
		<link href="favicon.ico" rel="shortcut icon"/>
		<link href="css/css2z.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet"/>
		<link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet"/>
		<link href="css/css4.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet"/>
		<link href="sweetalert2/dist/sweetalert2.min.css" rel="stylesheet">
		<script src="sweetalert2/dist/sweetalert2.min.js"></script>
		<script type="text/javascript" src="jquery-1.11.0.min.js"></script> 
		<script type="text/javascript" src="css/programas.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script type="text/javascript" src="css/calendario.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script>
			function verUltimaPos(idcta, filas, filtro){
				var scrtop = $('#divdet').scrollTop();
				var altura = $('#divdet').height();
				var numpag = $('#nummul').val();
				var limreg = $('#numres').val();
				if((numpag <= 0) || (numpag == "")){
					numpag=0;
				}
				if((limreg==0)||(limreg==""))
					limreg=10;
				numpag++;
				//location.href="teso-sinrecibocajaver.php?idrecibo="+idcta+"&scrtop="+scrtop+"&totreg="+filas+"&altura="+altura+"&numpag="+numpag+"&limreg="+limreg+"&filtro="+filtro;
			}
			function buscarbotonfiltro(){
                if((document.form2.fechaini.value == "" && document.form2.fechafin.value == "") ){
                    Swal.fire({
						icon: 'error',
						title: 'Error!',
						text: 'Faltan ingresar fechas',
						confirmButtonText: 'Continuar',
						confirmButtonColor: '#FF121A',
						timer: 2500
					});
                }else{
                    document.getElementById('numpos').value=0;
                    document.getElementById('nummul').value=0;
                    document.form2.submit();
                }
                
            }
			function crearexcel(){
				document.form2.action="teso-ingresosinternosexcel.php";
				document.form2.target="_BLANK";
				document.form2.submit();
				document.form2.action="";
				document.form2.target="";
			}
			function verUltimaPos(pos)
			{
				location.href="teso-visualizarIngresosBienesServicios.php?pos="+pos;
			}
		</script>
        <?php
			if(isset($_GET['fini']) && isset($_GET['ffin'])){
				if(!empty($_GET['fini']) && !empty($_GET['ffin'])){
					$_POST['fecha'] = $_GET['fini'];
					$_POST['fecha2'] = $_GET['ffin'];
				}
			}
			preg_match("/([0-9]{2})\/([0-9]{2})\/([0-9]{4})/", $_POST["fechaini"], $fech1);
			preg_match("/([0-9]{2})\/([0-9]{2})\/([0-9]{4})/", $_POST["fechafin"], $fech2);
			$f1=$fech1[3]."-".$fech1[2]."-".$fech1[1];
			$f2=$fech2[3]."-".$fech2[2]."-".$fech2[1];

			$scrtop=$_GET['scrtop'];
			if($scrtop=="") $scrtop=0;
			echo"<script>
				window.onload=function(){
					$('#divdet').scrollTop(".$scrtop.")
				}
			</script>";
			$gidcta=$_GET['idcta'];
			if(isset($_GET['filtro'])){
				$_POST['nombre']=$_GET['filtro'];
			}
		?>
	</head>
	<body>
		<IFRAME src="alertas.php" name="alertas" id="alertas" style="display:none"></IFRAME>
		<span id="todastablas2"></span>
		<table>
			<tr><script>barra_imagenes("teso");</script><?php cuadro_titulos();?></tr>	 
			<tr><?php menu_desplegable("teso");?></tr>
			<tr>
				<td colspan="3" class="cinta">
					<img src="imagenes/add.png" title="Nuevo" onClick="location.href='teso-ingresoBienesServicios.php'" class="mgbt"/>
					<img src="imagenes/guardad.png" class="mgbt1"/>
					<img src="imagenes/buscad.png" class="mgbt"/>
					<img src="imagenes/agenda1.png" title="Agenda"  onClick="mypop=window.open('plan-agenda.php','','');mypop.focus()" class="mgbt"/>
					<img src="imagenes/nv.png" title="Nueva ventana" class="mgbt" onClick="mypop=window.open('teso-principal.php','','');mypop.focus();">
					<img src="imagenes/duplicar_pantalla.png" title="Duplicar pesta&ntilde;a" onClick="mypop=window.open('<?php echo $url2; ?>','','');mypop.focus();" class="tooltip bottom mgbt">
					<img src="imagenes/excel.png" title="Excel" onclick="crearexcel()" class="mgbt">
				</td>
			</tr>	
		</table>
		<?php
			if($_POST['oculto']==''){
				
				$_POST['fechaini'] = date("01/m/Y");
				$_POST['fechafin'] =  date("d/m/Y");
			}
			if($_GET['numpag']!=""){
				if($_POST['oculto'] != 2){
					$_POST['numres']=$_GET['limreg'];
					$_POST['numpos']=$_GET['limreg']*($_GET['numpag']-1);
					$_POST['nummul']=$_GET['numpag']-1;
				}
			}
			else{
				if($_POST['nummul']==""){
					$_POST['numres']=10;
					$_POST['numpos']=0;
					$_POST['nummul']=0;
				}
			}
		?>
		<form name="form2" method="post" action="">
			<table class='tablamv3 ancho' >
			<thead>
				<tr>
					<td class="titulos" colspan = "16" style="text-align:left;" >:. Buscar ingresos propios</td>
					<td class="cerrar" style="width:7%" onClick="location.href='teso-principal.php'">Cerrar</td>
				</tr>
				<tr>
					<td class="tamano01" style="width:3cm;">Numero recibo:</td>
					<td colspan="4"><input type="search" name="numero" id="numero" value="<?php echo $_POST['numero'];?>" style="width:100%;"/></td>

					<td class="tamano01" style="width:2.2cm;">Concepto:</td>
					<td colspan="4"><input type="search" name="nombre" id="nombre" value="<?php echo $_POST['nombre'];?>" style="width:100%;"/></td>

					<td class="tamano01" style="width:2.2cm;">Documento:</td>
					<td colspan="4"><input type="search" name="documento" id="documento" value="<?php echo $_POST['documento'];?>" style="width:100%;"/></td>

					<td class="tamano01" style="width:2.2cm;">Fecha inicial: </td>
					<td style="width:10%;"><input type="search" name="fechaini" id="fc_1198971545" title="DD/MM/YYYY" placeholder="DD/MM/YYYY" value="<?php echo $_POST['fechaini'];?>" onchange="" onKeyUp="return tabular(event,this)" onKeyDown="mascara(this,'/',patron,true)" maxlength="10" style="width:100%" onDblClick="displayCalendarFor('fc_1198971545');" class="colordobleclik" autocomplete="off" onchange="cambiovigencia()" title="Calendario"></td>

					<td class="tamano01" style="width:2.2cm;">Fecha final: </td>
					<td style="width:10%;"><input type="search" name="fechafin"  id="fc_1198971546" title="DD/MM/YYYY" placeholder="DD/MM/YYYY" value="<?php echo $_POST['fechafin'];?>" onchange="" onKeyUp="return tabular(event,this) " onKeyDown="mascara(this,'/',patron,true)" maxlength="10" style="width:100%"  onDblClick="displayCalendarFor('fc_1198971546');" class="colordobleclik" autocomplete="off" onchange="cambiovigencia()" title="Calendario"/></td>  

					<td style="padding-bottom:1px"><em class="botonflechaverde" onClick="buscarbotonfiltro();">Buscar</em></td>
				</tr>  
			
					<input id="oculto" name="oculto" type="hidden" value="1">
					<input name="var1" type="hidden" id="var1" value=<?php echo $_POST['var1'];?> >
					<input type="hidden" name="numres" id="numres" value="<?php echo $_POST['numres'];?>"/>
					<input type="hidden" name="numpos" id="numpos" value="<?php echo $_POST['numpos'];?>"/>
					<input type="hidden" name="nummul" id="nummul" value="<?php echo $_POST['nummul'];?>"/>

					<?php
						$_POST['fecham1'] = $f1;
						$_POST['fecham2'] = $f2;
						if ($_POST['numero'] != ""){
							$crit1 = "T1.id_recibos LIKE '".$_POST['numero']."'";
						}else{
							$crit1="";
						}
						if ($_POST['nombre']!=""){
							$crit2 = "T1.concepto LIKE '%".$_POST['nombre']."%'";
						}else{
							$crit2="";
						}
						if ($_POST['documento'] != ""){
							$crit3 = "T1.tercero LIKE {$_POST['documento']}";
						}else{
							$crit3="";
						}
						if($crit1 != '' || $crit2 != '' || $crit3 != ''){
							$critm = "$crit1 $crit2 $crit3";
						}else{
							$critm = "T1.fecha BETWEEN '$f1' AND '$f2'";
						}
						$sqlr="SELECT DISTINCT T1.id_recibos, T1.id_recaudo, T1.fecha, T1.concepto, T1.tercero, T1.valor, T1.estado FROM teso_ingreso_bienes_servicios AS T1 INNER JOIN teso_ingreso_bienes_servicios_det AS T2 ON T1.id_recibos = T2.id_recibos WHERE $critm ORDER BY T1.id_recibos DESC ";
						$resp = mysqli_query($linkbd,$sqlr);
						$con = 1;
						$ntr = mysqli_num_rows($resp);
						echo"
							<tr>
								<td colspan='8' class='titulos'>.: Resultados Busqueda:</td>
							</tr>
							<tr>
								<td colspan='9' id='RecEnc'>Recibos encontrados: $ntr2</td>
							</tr>
							<tr>
								<td width='150' class='titulos2'>No Recibo</td>
								<td class='titulos2'>No Liquid.</td>
								<td class='titulos2'>Fecha</td>
								<td class='titulos2'>Concepto</td>
								<td class='titulos2'>Contribuyente</td>
								<td class='titulos2'>Valor</td>
								
								<td class='titulos2'>ESTADO</td>
							</tr>";
							?>
							</thead>
							<tbody style='height:54vh;' >
							
							<?php
							$iter='saludo1a';
							$iter2='saludo2';
							$filas=1;

							if($_POST['fechaini'] == '' && $_POST['fechafin'] == ''){
								echo "
									<tr>
										<td class='saludo1' style='text-align:center;width:100%;font-size:25px'>Se debe ingresar una fecha inicial y una fecha final</td>
									</tr>
								";
								$nuncilumnas = 0;
							}elseif(mysqli_num_rows($resp) == 0 || mysqli_num_rows($resp) == '0'){
								echo "
									<tr>
										<td class='saludo1' style='text-align:center;width:100%;font-size:25px'>No hay resultados de su busqueda.</td>
									</tr>
								";
							}else{
								while ($row =mysqli_fetch_row($resp)){
									$ntr2 = $ntr;
									echo "<script>document.getElementById('RecEnc').innerHTML = 'Recibos encontrados: $ntr2'</script>";
									$ntercero=buscatercero($row[4]);
									if($gidcta!=""){
										if($gidcta==$row[0]){
											$estilo='background-color:yellow';
										}else{
											$estilo="";
										}
									}else{
										$estilo="";
									}	
									$idcta="'".$row[0]."'";
									$numfil="'".$filas."'";
									$filtro="'".$_POST['nombre']."'";
									echo"
										<input type='hidden' name='nreciboE[]' value='".$row[0]."'>
										<input type='hidden' name='conceptoE[]' value='".$rowt[6]."'>
										<input type='hidden' name='fechaE[]' value='".$row[2]."'>
										<input type='hidden' name='nomContribuyenteE[]' value='".$row[15]," - ", $ntercero."'>
										<input type='hidden' name='valorE[]' value='".number_format($row[8],2)."'>
										<input type='hidden' name='nliquiE[]' value='".$row[4]."'>
										<input type='hidden' name='tipoE[]' value='".$tipos[$row[10]-1]."'>
									";
									if ($row[9]=='S'){
										echo"<input type='hidden' name='estadoE[]' value='ACTIVO'>";
									}
									if ($row[9]=='N'){
										echo"<input type='hidden' name='estadoE[]' value='ANULADO'>";
									}
									if ($row[9]=='P'){
										echo"<input type='hidden' name='estadoE[]' value='PAGO'>";
									}
									echo"
									<tr class='$iter' onDblClick=\"verUltimaPos($idcta)\" style='text-transform:uppercase; $estilo' >
										<td style='width:10%;text-align:center;'>$row[0]</td>
										<td style='width:17%;text-align:center;'>$row[1]</td>
										<td style='width:10%;text-align:center;'>$row[2]</td>
										<td style='width:16%;text-align:left;'>$row[3]</td>
										<td style='width:22%;text-align:left;'>$ntercero</td>
										<td style='width:10%;text-align:right;'>".number_format($row[5],2)."</td>";//$
									if ($row[6]=='S'){
										echo "<td style='width:12%;'><center><img src='imagenes/sema_verdeON.jpg' style='width:18px;'></center></td>";
									}
									if ($row[6]=='N'){
										echo "<td style='width:12%;'><center><img src='imagenes/sema_rojoON.jpg' style='width:18px;'></center></td>";
									}
									
									
					$con+=1;
					$aux=$iter;
					$iter=$iter2;
					$iter2=$aux;
					$filas++;
				}
			}

?></table> </div>
</form> 
</body>
</html>