<?php

require __DIR__ . '/vendor/autoload.php';

    header("Cache-control: no-cache, no-store, must-revalidate");
    header("Content-Type: text/html;charset=utf8");

    require 'comun.inc';
    require 'funciones.inc';
    require "conversor.php";

    session_start();
    if(empty($_SESSION)){
        header("location: index.php");
    }

    $linkbd = conectar_v7();
    $linkbd -> set_charset("utf8");

    date_default_timezone_set("America/Bogota");

?>
<!DOCTYPE >
<html lang="es">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1">
        <meta name="viewport" content="user-scalable=no">
        <title>:: IDEAL 10 - Tesorer&iacute;a</title>
        <link href="favicon.ico" rel="shortcut icon"/>
        <link href="css/css2.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
        <link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
        <link href="css/tabs.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
        <link href="sweetalert2/dist/sweetalert2.min.css" rel="stylesheet">
        <script src="sweetalert2/dist/sweetalert2.min.js"></script>
        <script type="text/javascript" src="css/programas.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
        <script type="text/javascript" src="css/calendario.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
        <script>
            function validar(){document.form2.submit();}
            function guardar(){
                let locali = "<?php echo $_SESSION;?>";
                if(!locali){
                    Swal.fire({
                        icon: 'success',
                        title: 'Se ha cerrado sesión. Serás redirigido a la página de inicio.',
                        showConfirmButton: true,
                        confirmButtonText: 'Continuar',
                        confirmButtonColor: '#01CC42',
                        timer: 3500
                    }).then((response) => {
                        location.href='index.php';
                        //document.location.href = "index.php";
                    });
                } else {
                    var vg="<?php
                        $sqlr = "SELECT * FROM tesoreciboscaja WHERE id_recibos='$_POST[idcomp]'";
                        $res = mysqli_query($linkbd,$sqlr);
                        $r = mysqli_fetch_row($res);
                        if($r[0]==""){
                            echo "S";
                        }else{
                            echo "N";
                        }
                        ?>";
                    if(vg=="S"){
                        if(!document.form2.modorec){var banco = '';}
                        else {var banco = document.form2.modorec.value;}

                        if(!document.form2.cuentaPuente){var CPuente = '';}
                        else {var CPuente = document.form2.cuentaPuente.value;}
                        if(banco == 'banco'){
                            var cuentaBancoCaja = document.form2.cb.value;
                        }else if(banco == 'caja'){
                            var cuentaBancoCaja = 'cuentaCaja';
                        }else{
                            var cuentaBancoCaja = 'cuentaPuente';
                        }
                        if (document.form2.fecha.value!='' && (banco!='' || CPuente!='') && cuentaBancoCaja !='' && document.form2.tiporec.value!='' )
                        {despliegamodalm('visible','4','Esta Seguro de Guardar','1');}
                        else{
                            despliegamodalm('visible','2','Faltan datos para completar el registro');
                            document.form2.fecha.focus();
                            document.form2.fecha.select();
                        }
                    }else{
                        var id="<?php
                            $sqlr="select id_recibos from tesoreciboscaja order by id_recibos desc";
                            $res=mysqli_query($linkbd,$sqlr);
                            $r=mysqli_fetch_row($res);
                            $_POST['idcomp']=$r[0]+1;
                            echo $r[0];
                            ?>"
                        document.form2.idcomp.value=id;
                    }
                }
            }
            function pdf(){
                document.form2.action="teso-pdfrecaja.php";
                document.form2.target="_BLANK";
                document.form2.submit();
                document.form2.action="";
                document.form2.target="";
            }
            function despliegamodalm(_valor,_tip,mensa,pregunta){
                document.getElementById("bgventanamodalm").style.visibility=_valor;
                if(_valor=="hidden"){document.getElementById('ventanam').src="";}
                else
                {
                    switch(_tip)
                    {
                        case "1":
                            document.getElementById('ventanam').src="ventana-mensaje1.php?titulos="+mensa;break;
                        case "2":
                            document.getElementById('ventanam').src="ventana-mensaje3.php?titulos="+mensa;break;
                        case "3":
                            document.getElementById('ventanam').src="ventana-mensaje2.php?titulos="+mensa;break;
                        case "4":
                            document.getElementById('ventanam').src="ventana-consulta2.php?titulos="+mensa+"&idresp="+pregunta;break;
                    }
                }
            }
            function funcionmensaje(){
                var numdocar=document.getElementById('idcomp').value;
                document.location.href = "teso-recibocajaver.php?idrecibo="+numdocar;
            }
            function respuestaconsulta(estado,pregunta){
                if(estado=="S")
                {
                    switch(pregunta)
                    {
                        case "1":	document.form2.oculto.value=2;
                                    document.form2.submit();
                                    break;
                        case "2":	var fechast=document.form2.fecha.value.split('/');
                                    document.form2.vigencia.value=document.form2.vigesistem.value=fechast[2];
                                    break;
                    }
                }
                else
                {
                    switch(pregunta)
                    {
                        case "1":	break;
                        case "2":	var fechast=document.form2.fecha.value.split('/');
                                    document.form2.vigesistem.value=fechast[2];
                                    break;
                    }
                }
                document.form2.submit();
            }
            function despliegamodal2(_valor,v){
                document.getElementById("bgventanamodal2").style.visibility=_valor;
                if(_valor=="hidden"){document.getElementById('ventana2').src="";}
                else
                {
                    if(v==1){
                        document.getElementById('ventana2').src="cuentasbancarias-ventana02.php?tipoc=D&obj01=banco&obj02=nbanco&obj03=&obj04=cb&obj05=ter";
                    }else{
                        document.getElementById('ventana2').src="notaspararevelacion.php?nota="+document.form2.notaf.value;
                    }
                }
            }
            function verificarfecha(){
                despliegamodalm('visible','4','La fecha asignada tiene una vigencia diferente a la del sistema, desea trabajar con esta vigencia?','2');
            }
        </script>
    </head>
    <body>
        <IFRAME src="alertas.php" name="alertas" id="alertas" style="display:none"></IFRAME>
        <span id="todastablas2"></span>
        <table>
            <tr><script>barra_imagenes("teso");</script><?php cuadro_titulos();?></tr>
            <tr><?php menu_desplegable("teso");?></tr>
            <tr>
                <td colspan="3" class="cinta">
                    <a onClick="location.href='teso-recibocaja.php'" class="mgbt"><img src="imagenes/add.png" title="Nuevo"/></a>
                    <a onClick="guardar()" class="mgbt"><img src="imagenes/guarda.png" title="Guardar"/></a>
                    <a class="mgbt" onClick="location.href='teso-buscarecibocaja.php'"><img src="imagenes/busca.png" title="Buscar"/></a>
                    <a onClick="mypop=window.open('plan-agenda.php','','');mypop.focus()" class="mgbt"><img src="imagenes/agenda1.png" title="Agenda"/></a>
                    <a class="mgbt" onClick="<?php echo paginasnuevas("teso");?>"><img src="imagenes/nv.png" title="Nueva ventana"/></a>
                    <a onClick="mypop=window.open('<?php echo $url2; ?>','','');mypop.focus();" class="tooltip bottom mgbt"><img src="imagenes/duplicar_pantalla.png" title="Duplicar pesta&ntilde;a"></a>
                    <a class="mgbt1"><img src="imagenes/printd.png" style="width:29px;height:25px;"  /></a>
                </td>
            </tr>
        </table>
        <div id="bgventanamodalm" class="bgventanamodalm">
            <div id="ventanamodalm" class="ventanamodalm">
                <IFRAME src="" name="ventanam" marginWidth=0 marginHeight=0 frameBorder=0 id="ventanam" frameSpacing=0 style=" width:700px; height:130px; top:200; overflow:hidden;">
                </IFRAME>
            </div>
        </div>
        <?php
            function obtenerTipoPredio($catastral){
                $tipo="";
                $digitos=substr($catastral,5,2);
                if($digitos=="00"){$tipo="rural";}
                else{$tipo="urbano";}
                return $tipo;
            }
            $sqlr="select cuentacaja, cobro_ambiental from tesoparametros";
            $res=mysqli_query($linkbd,$sqlr);
            while ($row = mysqli_fetch_row($res))
            {
                $_POST['cuentacaja']=$row[0];
                $_POST['cobro_ambiental'] = $row[1];
            }
            //*********** 11050501	CAJA PRINCIPAL esta es la cuenta que va a credito en todas las consignacones
            if(!$_POST['oculto'])
            {
                $check1="checked";
                $fec=date("d/m/Y");
                $_POST['fecha']=$fec;
                $_POST['vigesistem']=$_POST['vigencia']=date("Y");
                $sqlr="select cuentacaja from tesoparametros";
                $res=mysqli_query($linkbd,$sqlr);
                while ($row =mysqli_fetch_row($res)) {$_POST['cuentacaja']=$row[0];}
                $sqlr="SELECT valor_inicial,valor_final,tipo FROM dominios WHERE nombre_dominio='COBRO_RECIBOS' AND descripcion_valor= '".$_POST['vigencia']."' AND  tipo='S'";
                $res=mysqli_query($linkbd,$sqlr);
                while ($row =mysqli_fetch_row($res))
                {
                    $_POST['cobrorecibo']=$row[0];
                    $_POST['vcobrorecibo']=$row[1];
                    $_POST['tcobrorecibo']=$row[2];
                }
                $consec=selconsecutivo("tesoreciboscaja","id_recibos");
                $_POST['idcomp']=$consec;
                $_POST['valor']=0;
                $sqlr="SELECT tmindustria,desindustria,desavisos,desbomberil,intindustria,intavisos,intbomberil FROM tesoparametros";
                $res=mysqli_query($linkbd,$sqlr);
                while ($row =mysqli_fetch_row($res))
                {
                    $_POST['salariomin']=$row[0];
                    $_POST['descunidos']="$row[1]$row[2]$row[3]";
                    $_POST['intecunidos']="$row[4]$row[5]$row[6]";
                }
            }
            preg_match("/([0-9]{2})\/([0-9]{2})\/([0-9]{4})/", $_POST['fecha'],$vigensistem);
            if($vigensistem[3]!=$_POST['vigesistem'])
            {
                echo "<script>verificarfecha();</script>";
            }
            $vigencia=$vigusu=$_POST['vigencia'];
        ?>
        <form name="form2" method="post" action="">
            <input type="hidden" name="vguardar" id="vguardar" value="">
            <input name="encontro" type="hidden" value="<?php echo @ $_POST['encontro']?>" >
            <input name="cobrorecibo" type="hidden" value="<?php echo @ $_POST['cobrorecibo']?>" >
            <input name="vcobrorecibo" type="hidden" value="<?php echo @ $_POST['vcobrorecibo']?>" >
            <input name="tcobrorecibo" type="hidden" value="<?php echo @ $_POST['tcobrorecibo']?>" >
            <input name="codcatastral" type="hidden" value="<?php echo @ $_POST['codcatastral']?>" >

            <input name="cobro_ambiental" type="hidden" value="<?php echo @ $_POST['cobro_ambiental']?>" >
            <?php
                if(@ $_POST['oculto']){
                    switch($_POST['tiporec']){
                        case 1:	{ //RECAUDO PREDIAL
                            if($_POST['tipo']=='1'){
                                $sqlr="SELECT T1.vigencia FROM tesoacuerdopredial_det T1,tesoacuerdopredial T2 WHERE T1.idacuerdo = '$_POST[idrecaudo]' AND T2.idacuerdo = T1.idacuerdo AND T2.estado = 'S' ";
                                $res=mysqli_query($linkbd,$sqlr);
                                $vigencias="";
                                while($row = mysqli_fetch_row($res)){$vigencias.=($row[0]."-");}
                                $vigencias= "Años liquidados: ".substr($vigencias,0,-1);
                                $sql="SELECT * FROM tesoacuerdopredial WHERE idacuerdo = '".$_POST['idrecaudo']."' AND estado='S' AND (cuotas-cuota_pagada) > 0";
                                $result=mysqli_query($linkbd,$sql);
                                $_POST['encontro']="";
                                while($row = mysqli_fetch_row($result))
                                {
                                    $_POST['cuotas']=$row[10]+1;
                                    $_POST['tcuotas']=$row[4];
                                    $_POST['codcatastral']=$row[1];
                                    if($_POST['concepto']==""){$_POST['concepto']="$vigencias Cod Catastral No $row[1]";}
                                    $_POST['valorecaudo']=$row[7];
                                    $_POST['totalc']=$row[7];
                                    $_POST['tercero']=$row[13];

                                    $sqlrIdPredio = "SELECT id FROM predios WHERE codigo_catastro = '$row[1]'";
                                    $resIdPredio = mysqli_query($linkbd,$sqlrIdPredio);
                                    $rowIdPredio = mysqli_fetch_row($resIdPredio);

                                    $sqlrNom = "SELECT nombre_propietario FROM predio_propietarios WHERE predio_id = '$rowIdPredio[0]'";
                                    $resNom = mysqli_query($linkbd,$sqlrNom);
                                    $rowNom = mysqli_fetch_row($resNom);
                                    $_POST['ntercero'] = $rowNom[0];
                                    /* $sqlr1="SELECT nombrepropietario FROM tesopredios WHERE cedulacatastral= '".$_POST['codcatastral']."' AND estado='S'";
                                    $resul=mysqli_query($linkbd,$sqlr1);
                                    $row1 =mysqli_fetch_row($resul);
                                    $_POST['ntercero']=$row1[0];
                                    if ($_POST['ntercero']=='')
                                    {
                                        $sqlr2="SELECT * FROM tesopredios WHERE cedulacatastral='$row[1]' AND ord='$row[11]' AND tot='$row[12]'";
                                        $resc=mysqli_query($linkbd,$sqlr2);
                                        $rowc =mysqli_fetch_row($resc);
                                        $_POST['ntercero']=$rowc[6];
                                    } */
                                    $_POST['encontro']=1;
                                }
                            }
                            else
                            {
                                $_POST['cuotas']="1";
                                $_POST['tcuotas']="1";
                                $sqlr="SELECT * FROM tesoliquidapredial WHERE idpredial='".$_POST['idrecaudo']."' AND estado ='S' AND 1=".$_POST['tiporec'];
                                $_POST['encontro']="";
                                $res=mysqli_query($linkbd,$sqlr);
                                while ($row =mysqli_fetch_row($res)){
                                    $_POST['codcatastral']=$row[1];
                                    if($_POST['concepto']==""){$_POST['concepto']="$row[17] Cod Catastral No $row[1] $row[19] $row[20]";}
                                    $_POST['valorecaudo']=$row[8];
                                    $_POST['totalc']=$row[8];
                                    $_POST['tercero']=$row[4];

                                    $sqlrIdPredio = "SELECT id FROM predios WHERE codigo_catastro = '$row[1]'";
                                    $resIdPredio = mysqli_query($linkbd,$sqlrIdPredio);
                                    $rowIdPredio = mysqli_fetch_row($resIdPredio);

                                    $sqlrNom = "SELECT nombre_propietario FROM predio_propietarios WHERE predio_id = '$rowIdPredio[0]'";
                                    $resNom = mysqli_query($linkbd,$sqlrNom);
                                    $rowNom = mysqli_fetch_row($resNom);
                                    $_POST['ntercero'] = $rowNom[0];

                                    /* $sqlr1="select nombrepropietario from tesopredios where cedulacatastral = '".$_POST['codcatastral']."' and estado='S'";
                                    $resul=mysqli_query($linkbd,$sqlr1);
                                    $row1 =mysqli_fetch_row($resul);
                                    $_POST['ntercero']=$row1[0];
                                    if ($_POST['ntercero']=='')
                                    {
                                        $sqlr2="select *from tesopredios where cedulacatastral='$row[1]' and ord='$row[19]' and tot='$row[20]'";
                                        $resc=mysqli_query($linkbd,$sqlr2);
                                        $rowc =mysqli_fetch_row($resc);
                                        $_POST['ntercero']=$rowc[6];
                                    }	 */
                                    $_POST['encontro']=1;
                                }
                            }
                        }break;
                        case 2:	{ //RECAUDO INDUSTRIA Y COMERCIO
                            $sqlr = "SELECT * FROM tesoindustria WHERE id_industria='$_POST[idrecaudo]' AND estado ='S' AND 2='$_POST[tiporec]'";
                            $_POST['encontro'] = "";
                            $res = mysqli_query($linkbd,$sqlr);
                            while ($row =mysqli_fetch_row($res)) {
                                if($_POST['concepto']==""){
                                    $_POST['concepto'] = "Liquidacion Industria y Comercio avisos y tableros - $row[3]";
                                }
                                $_POST['valorecaudo'] = $row[6];
                                $_POST['totalc'] = $row[6];
                                $_POST['tercero'] = $row[5];
                                $_POST['ntercero'] = buscatercero($row[5]);
                                $_POST['encontro'] = 1;
                                $_POST['cuotas'] = $row[9]+1;
                                $_POST['tcuotas'] = $row[8];
                                $_POST['tipo_impuesto'] = $row[16];
                            }
                        }break;
                        case 3:	{ //OTROS RECAUDOS
                            $sqlr="select *from tesorecaudos where tesorecaudos.id_recaudo=$_POST[idrecaudo] and estado ='S' and 3=$_POST[tiporec]";
                            $_POST['encontro']="";
                            $res=mysqli_query($linkbd,$sqlr);
                            while ($row =mysqli_fetch_row($res))
                            {
                                if($_POST['concepto']==""){$_POST['concepto']=$row[6];	}
                                $_POST['valorecaudo']=$row[5];
                                $_POST['totalc']=$row[5];
                                $_POST['tercero']=$row[4];
                                $_POST['ntercero']=buscatercero($row[4]);
                                $_POST['encontro']=1;
                            }
                        }break;
                    }
                }
            ?>
            <table class="inicio" style="width:99.7%;">
                <tr >
                    <td class="titulos" colspan="9">Recibo de Caja</td>
                    <td class="cerrar" style="width:7%;"><a onClick="location.href='teso-principal.php'">&nbsp;Cerrar</a></td>
                </tr>
                <tr>
                    <td class="saludo1" style="width:2cm;" >No Recibo:</td>
                    <td style="width:20%;" colspan="<?php if(@ $_POST['tiporec'] == '1'){echo '3'; }else{echo '1';}?>" >
                        <input type="hidden" name="cuentacaja"  value="<?php echo @ $_POST['cuentacaja']?>"/>
                        <input type="text" name="idcomp" id="idcomp" value="<?php echo @ $_POST['idcomp']?>" onKeyUp="return tabular(event,this)" style="width:100%;" readonly/>
                    </td>
                    <td class="saludo1" style="width:2.3cm;">Fecha: </td>
                    <td style="width:18%;"><input type="text" name="fecha" value="<?php echo @ $_POST['fecha']?>" maxlength="10"  onKeyPress="javascript:return solonumeros(event)" onKeyUp="return tabular(event,this)" id="fc_1198971545" onKeyDown="mascara(this,'/',patron,true)" title="DD/MM/YYYY" style="width:80%;" onChange="verificarfecha();"/>&nbsp;<img src="imagenes/calendario04.png"  class="icobut" onClick="displayCalendarFor('fc_1198971545');" title="Calendario"/></td>
                    <td class="saludo1" style="width:2.5cm;">Vigencia:</td>
                    <td style="width:12%;"><input type="text" id="vigencia" name="vigencia" onKeyPress="javascript:return solonumeros(event)" onKeyUp="return tabular(event,this)" value="<?php echo @ $_POST['vigencia']?>" onClick="document.getElementById('tipocta').focus(); document.getElementById('tipocta').select();" style="width:100%;" readonly></td>
                    <input type="hidden" name="vigesistem" id="vigesistem" value="<?php echo $_POST['vigesistem']?>"/>
                    <td rowspan="6" colspan="2" style="background:url(imagenes/LOGOIDEAL10c.png); background-repeat:no-repeat; background-position:right; background-size: 100% 100%;"></td>
                </tr>
                <tr>
                    <td class="saludo1"> Recaudo:</td>
                    <td>
                        <select name="tiporec" id="tiporec" onKeyUp="return tabular(event,this)" onChange="validar()" style="width:100%;">
                            <option value=""> Seleccione ...</option>
                            <option value="1" <?php if($_POST['tiporec']=='1') echo "SELECTED"; ?>>Predial</option>
                            <option value="2" <?php if($_POST['tiporec']=='2') echo "SELECTED"; ?>>Industria y Comercio</option>
                            <option value="3" <?php if($_POST['tiporec']=='3') echo "SELECTED"; ?>>Otros Recaudos</option>
                        </select>
                    </td>
                    <?php
                        if($_POST['tiporec']=='1'){
                            $_POST['tipo']=='2';
                            echo "
                                <td class='saludo1'> Tipo:</td>
                                <td>
                                    <select name='tipo' id='tipo' onKeyUp='return tabular(event,this)' style='width:100%;' disabled>
                                        <option value='2' ";
                            if($_POST['tipo']=='2'){echo 'SELECTED';}
                            echo">Por Liquidacion</option>
                                    </select>
                                </td>";
                        }
                    ?>
                    <td class="saludo1"><?php if($_POST['tipo']=='1') {echo 'No. Acuerdo:'; }else{echo 'No Liquidaci&oacute;n:'; } ?></td>
                    <td><input type="text" id="idrecaudo" name="idrecaudo" value="<?php echo @ $_POST['idrecaudo']?>"  onKeyUp="return tabular(event,this)" onChange="validar()" style="width:80%;"></td>
                    <?php
                        if($_POST['tipo']=='2'){
                            $sqlrAbono = "SELECT * FROM tesoabono WHERE cierre='".$_POST['idrecaudo']."'";
                            $rowAbono = view($sqlrAbono);
                        }
                        if($rowAbono==NULL){
                    ?>
                            <td class="saludo1">Recaudado en:</td>
                            <td>
                                <select name="modorec" id="modorec" onKeyUp="return tabular(event,this)" onChange="validar()" style="width:100%;">
                                    <option value="">Seleccione ...</option>
                                    <option value="banco" <?php if($_POST['modorec']=='banco') echo "SELECTED"; ?>>Banco</option>
                                    <option value="caja" <?php if($_POST['modorec']=='caja') echo "SELECTED"; ?>>Caja</option>
                                </select>
                            </td>
                        <?php
                        }
                        else
                        {
                            if($_POST['idrecaudo']!='')
                            {
                                $sqCuentaPuente = "select cuentapuente from tesoparametros";
                                $rowCuentaPuente = view($sqCuentaPuente);
                                ?>
                                <td class="saludo1">Cuenta Puente:</td>
                                    <td>
                                        <input type="text" name="cuentaPuente" id="cuentaPuente" value="<?php echo $rowCuentaPuente[0]['cuentapuente'] ?>" readonly>
                                    </td>
                                <?php
                            }
                        }
                        ?>
                </tr>
                <?php
                    if ($_POST['modorec']=='banco')
                    {
                        echo"
                        <tr>
                            <td class='saludo1'>Cuenta :</td>
                            <td>
                                <input type='text' name='cb' id='cb' value='".$_POST['cb']."' style='width:80%;'/>&nbsp;
                                <a onClick=\"despliegamodal2('visible',1);\"  style='cursor:pointer;' title='Listado Cuentas Bancarias'>
                                    <img src='imagenes/find02.png' style='width:20px;'/>
                                </a>
                            </td>
                            <td colspan='4'>
                                    <input type='text' id='nbanco' name='nbanco' style='width:100%;' value='".$_POST['nbanco']."' readonly>
                            </td>
                                    <input type='hidden' name='banco' id='banco' value='".$_POST['banco']."'/>
                                    <input type='hidden' id='ter' name='ter' value='".$_POST['ter']."'/>
                            </td>
                        </tr>";
                    }
                ?>
                <tr>
                    <td class="saludo1">Concepto:</td>
                    <td colspan="<?php if($_POST['tiporec']==2 ){echo '3';}else{echo'5';}?>">
                        <input type="text" name="concepto" value="<?php echo $_POST['concepto'] ?>" onKeyUp="return tabular(event,this)" style="width:95%;">
                        <input type="hidden" name="notaf" id="notaf" value="<?php echo $_POST['notaf']?>" >&nbsp;<img src="imagenes/notaf.png" class="icobut" onClick="despliegamodal2('visible',2);" title="Notas"/>
                    </td>
                    <?php
                        if(($_POST['tiporec']==2) || ($_POST['tiporec']==1)){
                            echo"
                            <td class='saludo1'>No Cuota:</td>
                            <td><input type='text' name='cuotas' size='1' value='".$_POST['cuotas']."' readonly>/<input type='text' id='tcuotas' name='tcuotas' value='".$_POST['tcuotas']."' size='1' readonly ></td>";
                        }
                    ?>
                </tr>
                <tr>
                    <td  class="saludo1">Documento: </td>
                    <td colspan="<?php if($_POST['tiporec']=='1'){echo '3'; }else{echo '1';}?>" ><input type="text" name="tercero" value="<?php echo $_POST['tercero']?>" onKeyUp="return tabular(event,this)" style="width:100%;" readonly></td>
                    <td class="saludo1">Contribuyente:</td>
                    <td colspan="3"><input type="text" id="ntercero" name="ntercero" value="<?php echo $_POST['ntercero']?>" onKeyUp="return tabular(event,this)" style="width:100%;" readonly></td>
                </tr>
                <tr>
                    <td class="saludo1">Valor:</td>
                    <td colspan="<?php if($_POST['tiporec']=='1'){echo '3'; }else{echo '1';}?>">
                        <input type="text" id="valorecaudo" name="valorecaudo" value="<?php echo $_POST['valorecaudo']?>" onKeyUp="return tabular(event,this)" style="width:90%;" readonly >
                    </td>
                </tr>
                <?php if ($_POST['modorec']!='banco'){echo"<tr style='height:20;'><tr>";}?>
            </table>
            <input type="hidden" name="oculto" id="oculto" value="1"/>
            <input type="hidden" name="trec" value="<?php echo $_POST['trec']?>"/>
            <input type="hidden" name="tipo_impuesto" id='tipo_impuesto' value="<?php echo $_POST['tipo_impuesto']?>"/>
            <div class="subpantallac7"  style="height:49.3%; width:99.6%; overflow-x:hidden;" id="divdet">
            <?php
                if($_POST['oculto'] && $_POST['encontro']=='1'){
                    switch($_POST['tiporec']) {
                        case 1: {//********PREDIAL
                            $_POST['dcoding']= array();
                            $_POST['dncoding']= array();
                            $_POST['dvalores']= array();
                            if($_POST['tcobrorecibo']=='S'){
                                $_POST['dcoding'][]=$_POST['cobrorecibo'];
                                $_POST['dncoding'][]=buscaingreso($_POST['cobrorecibo'])." ".$vigusu;
                                $_POST['dvalores'][]=$_POST['vcobrorecibo'];
                            }
                            $_POST['trec']='PREDIAL';
                                $sqlr="select tb1.vigliquidada, tb1.totaliquidavig, tb2.codigocatastral from tesoliquidapredial_det AS tb1, tesoliquidapredial AS tb2 where tb1.idpredial = '".$_POST['idrecaudo']."' AND tb1.idpredial = tb2.idpredial and tb1.estado ='S' and 1=".$_POST['tiporec'];
                                $res=mysqli_query($linkbd,$sqlr);
                                //*******************CREANDO EL RECIBO DE CAJA DE PREDIAL ***********************
                                while ($row =mysqli_fetch_row($res)){
                                    $vig=$row[0];
                                    if(substr($row[2],0,2) == '00'){
                                        if($vig==$vigusu){
                                            $sqlr2 = "select * from tesoingresos_predial where codigo='03'";
                                            $res2 = mysqli_query($linkbd,$sqlr2);
                                            $row2 = mysqli_fetch_row($res2);
                                            $_POST['dcoding'][] = $row2[0];
                                            $_POST['dncoding'][] = $row2[1]." ".$vig;
                                            $_POST['dvalores'][] = $row[1];
                                        }else {
                                            $sqlr2 = "select * from tesoingresos_predial where codigo='04'";
                                            $res2 = mysqli_query($linkbd,$sqlr2);
                                            $row2 = mysqli_fetch_row($res2);
                                            $_POST['dcoding'][] = $row2[0];
                                            $_POST['dncoding'][] = $row2[1]." ".$vig;
                                            $_POST['dvalores'][] = $row[1];
                                        }
                                    }else{
                                        if($vig==$vigusu){
                                            $sqlr2 = "select * from tesoingresos_predial where codigo='01'";
                                            $res2=mysqli_query($linkbd,$sqlr2);
                                            $row2 =mysqli_fetch_row($res2);
                                            $_POST['dcoding'][]=$row2[0];
                                            $_POST['dncoding'][]=$row2[1]." ".$vig;
                                            $_POST['dvalores'][]=$row[1];
                                        }else{
                                            $sqlr2="select * from tesoingresos_predial where codigo='02'";
                                            $res2=mysqli_query($linkbd,$sqlr2);
                                            $row2 =mysqli_fetch_row($res2);
                                            $_POST['dcoding'][]=$row2[0];
                                            $_POST['dncoding'][]=$row2[1]." ".$vig;
                                            $_POST['dvalores'][]=$row[1];
                                        }
                                    }

                                }

                        }break;
                        case 2: {//***********INDUSTRIA Y COMERCIO
                            $_POST['dcoding'] = array();
                            $_POST['dncoding'] = array();
                            $_POST['dvalores'] = array();
                            $_POST['trec'] = 'INDUSTRIA Y COMERCIO';
                            if($_POST['tcobrorecibo']=='S'){
                                $_POST['dcoding'][] = $_POST['cobrorecibo'];
                                $_POST['dncoding'][] = buscaingreso($_POST['cobrorecibo'])." ".$vigusu;
                                $_POST['dvalores'][] = $_POST['vcobrorecibo'];
                            }
                            $sqlr = "SELECT * FROM tesoindustria WHERE id_industria = ".$_POST['idrecaudo']." AND estado = 'S' AND 2=".$_POST['tiporec'];
                            $res = mysqli_query($linkbd,$sqlr);
                            while ($row = mysqli_fetch_row($res)){
                                $sqlr2 = "SELECT * FROM tesoingresos_ica WHERE codigo = '".$_POST['tipo_impuesto']."' ";
                                $res2 = mysqli_query($linkbd,$sqlr2);
                                $row2 = mysqli_fetch_row($res2);
                                $_POST['dcoding'][] = $row2[0];
                                $_POST['dncoding'][] = $row2[1];
                                $_POST['dvalores'][] = $row[6]/$_POST['tcuotas'];
                            }
                        }break;
                        case 3: {///*****************otros recaudos *******************
                            $_POST['trec']='OTROS RECAUDOS';
                            $_POST['dcoding']= array();
                            $_POST['dncoding']= array();
                            $_POST['dvalores']= array();
                            if(@ $_POST['tcobrorecibo']=='S')
                            {
                                $_POST['dcoding'][]=$_POST['cobrorecibo'];
                                $_POST['dncoding'][]=buscaingreso($_POST['cobrorecibo'])." ".$vigusu;
                                $_POST['dvalores'][]=$_POST['vcobrorecibo'];
                            }
                            $sqlr="select *from tesorecaudos_det where tesorecaudos_det.id_recaudo=".$_POST['idrecaudo']." and estado ='S'  and 3=".$_POST['tiporec'];
                            $res=mysqli_query($linkbd,$sqlr);
                            while ($row =mysqli_fetch_row($res))
                            {
                                $_POST['dcoding'][]=$row[2];
                                $sqlr2="select nombre from tesoingresos where codigo='".$row[2]."'";
                                $res2=mysqli_query($linkbd,$sqlr2);
                                $row2 =mysqli_fetch_row($res2);
                                $_POST['dncoding'][]=$row2[0];
                                $_POST['dvalores'][]=$row[3];
                            }
                        }break;
                    }
                }
                ?>
                <table class="inicio">
                    <tr><td colspan="4" class="titulos">Detalle Recibo de Caja</td></tr>
                    <tr>
                        <td class="titulos2" style="width:15%;">Codigo</td>
                        <td class="titulos2">Ingreso</td>
                        <td class="titulos2" style="width:20%;">Valor</td>
                    </tr>
                    <?php
                        $_POST['totalc']=0;
                        $co="saludo1a";
                        $co2="saludo2";
                        if(isset($_POST['dcoding'])){
                            for ($x=0;$x<count($_POST['dcoding']);$x++)
                            {
                                echo "
                                <input type='hidden' name='dcoding[]' value='".$_POST['dcoding'][$x]."'>
                                <input type='hidden' name='dncoding[]' value='".$_POST['dncoding'][$x]."'>
                                <input type='hidden' name='dvalores[]' value='".$_POST['dvalores'][$x]."'>
                                <tr class='$co' onMouseOver=\"anterior=this.style.backgroundColor;this.style.backgroundColor='#40b3ff';\" onMouseOut=\"this.style.backgroundColor=anterior\">
                                    <td>".$_POST['dcoding'][$x]."</td>
                                    <td>".$_POST['dncoding'][$x]."</td>
                                    <td style='text-align:right;'>$ ".number_format($_POST['dvalores'][$x],2)."</td>
                                </tr>";
                                $_POST['totalc']=$_POST['totalc']+$_POST['dvalores'][$x];
                                $_POST['totalcf']=number_format($_POST['totalc'],2);
                                $totalg=number_format($_POST['totalc'],2,'.','');
                                $aux=$co;
                                $co=$co2;
                                $co2=$aux;
                            }
                        }

                        if ($_POST['totalc']!='' && $_POST['totalc']!=0){$_POST['letras'] = convertirdecimal($totalg,'.');}
                        else{$_POST['letras']=''; $_POST['totalcf']=0;}
                        echo "
                        <input type='hidden' name='totalcf' value='".$_POST['totalcf']."'>
                        <input type='hidden' name='totalc' value='".$_POST['totalc']."'>
                        <input type='hidden' name='letras' value='".$_POST['letras']."'>
                        <tr class='$co' style='text-align:right;'>
                            <td colspan='2'>Total</td>
                            <td>$ ".$_POST['totalcf']."</td>
                        </tr>
                        <tr class='titulos2'>
                            <td>Son:</td>
                            <td colspan='2'>".$_POST['letras']."</td>
                        </tr>";
                    ?>
                </table>
            </div>
            <?php
                if($_POST['oculto']=='2'){
                    preg_match("/([0-9]{2})\/([0-9]{2})\/([0-9]{4})/", $_POST['fecha'],$fecha);
                    $fechaf="$fecha[3]-$fecha[2]-$fecha[1]";
                    $bloq=bloqueos($_SESSION['cedulausu'],$fechaf);
                    if($_POST['modorec']=='caja'){
                        $cuentacb = $_POST['cuentacaja'];
                        $cajas = $_POST['cuentacaja'];
                        $cbancos = "";
                    }
                    if($_POST['modorec']=='banco')
                    {
                        $cuentacb = $_POST['banco'];
                        $cajas = "";
                        $cbancos = $_POST['banco'];
                    }
                    if($bloq>=1){
                        //************VALIDAR SI YA FUE GUARDADO ************************
                        switch($_POST['tiporec']){
                            //***** PREDIAL *****************************************
                            case 1:{
                                $var01=$var02=0;
                                $sqlr="select count(*) from tesoreciboscaja where id_recaudo=".$_POST['idrecaudo']." and tipo='1' AND estado='S'";
                                $res=mysqli_query($linkbd,$sqlr);
                                while($r=mysqli_fetch_row($res)){$numerorecaudos=$r[0];}
                                if($numerorecaudos==0)
                                {
                                    if(@ $_POST['modorec']=='caja')
                                    {
                                        $cuentacb=$_POST['cuentacaja'];
                                        $cajas=$_POST['cuentacaja'];
                                        $cbancos="";
                                    }
                                    if(@ $_POST['modorec']=='banco')
                                    {
                                        $cuentacb=$_POST['banco'];
                                        $cajas="";
                                        $cbancos=$_POST['banco'];
                                    }
                                    if(@ $_POST['cuentaPuente']!='')
                                    {
                                        $cuentacb=$_POST['cuentaPuente'];
                                        $cajas="";
                                        $cbancos=$_POST['bacuentaPuentenco'];
                                    }
                                    $user=$_SESSION['nickusu'];
                                    $sqlr="insert into tesoreciboscaja (id_comp,fecha,vigencia,id_recaudo,recaudado,cuentacaja,cuentabanco, valor,estado,tipo,descripcion,usuario) values('0','$fechaf','$vigusu', '".$_POST['idrecaudo']."','".$_POST['modorec']."','$cajas','$cbancos', '".$_POST['totalc']."','S','".$_POST['tiporec']."','".$_POST['concepto']."','$user')";
                                    if (!mysqli_query($linkbd,$sqlr))
                                    {
                                        $e =mysqli_error($linkbd);
                                        echo"<script>despliegamodalm('visible','2','No se pudo ejecutar la peticion: $e');</script>";
                                    }
                                    else
                                    {
                                        $concecc=mysqli_insert_id($linkbd);
                                    }
                                    //************ insercion de cabecera recaudos ************
                                    echo "<input type='hidden' name='concec' value='$concecc'>";
                                    //marca;
                                    echo "<script>
                                        despliegamodalm('visible','1','>Se ha almacenado el Recibo de Caja con Exito');
                                        document.form2.vguardar.value='1';

                                    </script>";

                                    $sqlr="update tesoliquidapredial set estado='P' WHERE idpredial=".$_POST['idrecaudo'];
                                    mysqli_query($linkbd,$sqlr);

                                    $sqlr="Select * from tesoliquidapredial_det where idpredial=".$_POST['idrecaudo'];
                                    $resq=mysqli_query($linkbd,$sqlr);

                                    $tenant_db = \Laravel\DB::connectWithDomain(\Laravel\Route::currentDomain());

                                    $stmt = $tenant_db->prepare('SELECT `id` FROM `predios` WHERE `codigo_catastro` = ? LIMIT 1');
                                    $stmt->bind_param('s', $_POST['codcatastral']);
                                    if (! $stmt->execute()) {
                                        echo($tenant_db->error);
                                    }
                                    $result = $stmt->get_result();
                                    $predio_id = $result->fetch_array(MYSQLI_NUM)[0];

                                    $stmt = $tenant_db->prepare('UPDATE `factura_predials` SET `data` = (SELECT JSON_REPLACE(data, \'$.factura_pagada\', true) FROM `factura_predials` WHERE `id` = ? LIMIT 1) WHERE `id` = ?');
                                    $stmt->bind_param('ii', $_POST['idrecaudo'], $_POST['idrecaudo']);
                                    if (! $stmt->execute()) {
                                        echo($tenant_db->error);
                                    }

                                    while($rq=mysqli_fetch_row($resq))
                                    {
                                        $sqlr2="update tesoprediosavaluos set pago='S' where codigocatastral=(select codigocatastral from tesoliquidapredial WHERE idpredial=".$_POST['idrecaudo'].") AND vigencia=$rq[1]";
                                        mysqli_query($linkbd,$sqlr2);

                                        if ($predio_id != null) {
                                            $stmt = $tenant_db->prepare('UPDATE `predio_avaluos` SET `pagado` = 1 WHERE `predio_id` = ? AND `vigencia` = ?');
                                            $stmt->bind_param('ii', $predio_id, $rq[1]);
                                            if (! $stmt->execute()) {
                                                echo($tenant_db->error);
                                            }
                                        }
                                    }

                                    $tenant_db->close();

                                    echo"
                                    <script>
                                        document.form2.numero.value='';
                                        document.form2.valor.value=0;
                                    </script>";
                                    //**********************CREANDO COMPROBANTE CONTABLE ********************************
                                    $sqlr="insert into comprobante_cab (numerotipo,tipo_comp,fecha,concepto,total,total_debito, total_credito, diferencia,estado) values ($concecc,5,'$fechaf','".$_POST['concepto']."',0, ".$_POST['totalc'].",".$_POST['totalc'].",0,'1')";
                                    mysqli_query($linkbd,$sqlr);
                                    //******parte para el recaudo del cobro por recibo de caja
                                    for($x=0;$x<count($_POST['dcoding']);$x++)
                                    {
                                        if($_POST['dcoding'][$x]==$_POST['cobrorecibo'])
                                        {
                                            //***** BUSQUEDA INGRESO ********
                                            $sqlri="Select * from tesoingresos_predial_det where codigo='".$_POST['dcoding'][$x]."' and vigencia=(SELECT MAX(vigencia) FROM tesoingresos_predial_det WHERE codigo = '".$_POST['dcoding'][$x]."')";
                                            $resi=mysqli_query($linkbd,$sqlri);
                                            while($rowi=mysqli_fetch_row($resi))
                                            {
                                                //**** busqueda cuenta presupuestal*****
                                                //busqueda concepto contable
                                                $sq="select fechainicial from conceptoscontables_det where codigo='$rowi[2]' and modulo='4' and tipo='C' and fechainicial<'$fechaf' and cuenta!='' order by fechainicial asc";
                                                $re=mysqli_query($linkbd,$sq);
                                                while($ro=mysqli_fetch_assoc($re)){$_POST['fechacausa']=$ro["fechainicial"];}
                                                $sqlrc="Select * from conceptoscontables_det where estado='S' and modulo='4' AND codigo='$rowi[2]' and tipo='C' and fechainicial='".$_POST['fechacausa']."'";
                                                $resc=mysqli_query($linkbd,$sqlrc);
                                                while($rowc=mysqli_fetch_row($resc))
                                                {
                                                    $porce=$rowi[5];
                                                    if($rowc[7]=='S')
                                                    {
                                                        $valorcred=$_POST['dvalores'][$x]*($porce/100);
                                                        $valordeb=0;
                                                        if($rowc[3]=='N')
                                                        {
                                                            //*****inserta del concepto contable
                                                            //***********MODIFICAR CUENTA PPTAL DE INGRESO AGREGARLE EL RECAUDO

                                                            $vi=$_POST['dvalores'][$x]*($porce/100);
                                                            $sqlr="update pptocuentaspptoinicial  set ingresos=ingresos+".$vi." where cuenta ='$rowi[6]' and vigencia='$vigusu'";
                                                            mysqli_query($linkbd,$sqlr);
                                                            //************ FIN MODIFICACION PPTAL
                                                            $sqlr="insert into comprobante_det (id_comp,cuenta,tercero,centrocosto,detalle,cheque,valdebito,valcredito,estado,vigencia) values ('5 $concecc','$rowc[4]', '".$_POST['tercero']."','$rowc[5]','Ingreso ".strtoupper($_POST['dncoding'][$x])."','','".round($valordeb,0)."','".round($valorcred,0)."','1','".$_POST['vigencia']."')";
                                                            mysqli_query($linkbd,$sqlr);
                                                            //***cuenta caja o banco
                                                            if(@ $_POST['modorec']=='caja')
                                                            {
                                                                $cuentacb=$_POST['cuentacaja'];
                                                                $cajas=$_POST['cuentacaja'];
                                                                $cbancos="";
                                                            }
                                                            if(@ $_POST['modorec']=='banco')
                                                            {
                                                                $cuentacb=$_POST['banco'];
                                                                $cajas="";
                                                                $cbancos=$_POST['banco'];
                                                            }
                                                            $sqlr="insert into comprobante_det (id_comp,cuenta,tercero,centrocosto, detalle,cheque,valdebito,valcredito,estado,vigencia) values ('5 $concecc','$cuentacb','".$_POST['tercero']."', '$rowc[5]','Ingreso ".strtoupper($_POST['dncoding'][$x])."','','".round($valorcred,0)."','0','1','".$_POST['vigencia']."')";
                                                            mysqli_query($linkbd,$sqlr);
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }

                                     //*************** fin de cobro de recibo
                                    $sql="select codigocatastral from tesoliquidapredial WHERE idpredial=".$_POST['idrecaudo']." and estado !='N'";
                                    $resul=mysqli_query($linkbd,$sql);
                                    $rowcod=mysqli_fetch_row($resul);
                                    $cedulaCatastral = $rowcod[0];
                                    $tipopre=obtenerTipoPredio($rowcod[0]);
                                    //die();
                                    $sqlr="select *from tesoliquidapredial_det where idpredial=".$_POST['idrecaudo']." and estado ='S'  and 1=".$_POST['tiporec'];
                                    $res=mysqli_query($linkbd,$sqlr);
                                    //*******************CREANDO EL RECIBO DE CAJA DE PREDIAL ***********************
                                    while ($row =mysqli_fetch_row($res))
                                    {

                                        $vig=$row[1];
                                        $vlrdesc=$row[10];
                                        $cuentaAmb = '';

                                        if($vig==$vigusu) //*************VIGENCIA ACTUAL *****************
                                        {
                                            $idcomp=mysqli_insert_id($linkbd);
                                            echo "<input type='hidden' id='ncomp' name='ncomp' value='$idcomp'>";
                                            $codigoIng = '';
                                            if(substr($cedulaCatastral,0,2) == '00'){
                                                $codigoIng = '03';
                                            }else{
                                                $codigoIng = '01';
                                            }

                                            $sqlr2="SELECT * FROM tesoingresos_predial_det WHERE codigo='$codigoIng' AND vigencia=(SELECT MAX(vigencia) FROM tesoingresos_predial_det WHERE codigo = '$codigoIng') GROUP BY concepto ORDER BY concepto ASC";
                                            $res2=mysqli_query($linkbd,$sqlr2);
                                            //****** $cuentacb   ES LA CUENTA CAJA O BANCO
                                            while($rowi =mysqli_fetch_row($res2))
                                            {
                                                switch($rowi[2])
                                                {
                                                    //Impuesto Predial Vigente
                                                    case '21': case '20':

                                                        $descpredial = $row[10];
                                                        $valorcred=$row[4]+$descpredial;
                                                        $valordeb=$row[4];


                                                        if($valorcred>0)
                                                        {
                                                            if($_POST['cobro_ambiental'] == 'S'){
                                                                $vi=$valorcred-$row[8];
                                                            }else{
                                                                $vi=$valorcred;
                                                            }

                                                            $sqlrPresu="insert into pptorecibocajappto (cuenta,idrecibo,valor,vigencia,tipo,ingreso,fuente, seccion_presupuestal, medio_pago, vigencia_gasto) values('$rowi[6]',$concecc,".round($vi,0).",'$vigusu','N','$codigoIng', '$rowi[10]', '16', 'CSF', '1')";
                                                            mysqli_query($linkbd,$sqlrPresu);
                                                            $sqlrPresu="";
                                                        }

                                                        $sq="SELECT fechainicial FROM conceptoscontables_det WHERE codigo='$rowi[2]' AND modulo='4' AND tipo='PR' AND fechainicial<'$fechaf' AND cuenta!='' ORDER BY fechainicial ASC";
                                                        $re=mysqli_query($linkbd,$sq);
                                                        while($ro=mysqli_fetch_assoc($re))
                                                        {$_POST['fechacausa']=$ro["fechainicial"];}

                                                         $sqlrc="Select * from conceptoscontables_det where estado='S' and modulo='4' AND codigo = '$rowi[2]' and tipo='PR' and fechainicial='".$_POST['fechacausa']."'";
                                                        $resc=mysqli_query($linkbd,$sqlrc);
                                                        while($rowc=mysqli_fetch_row($resc))
                                                        {
                                                            $centroCostosCeros = str_pad($rowc[5], 2, "0", STR_PAD_LEFT);
                                                            $valordeb=0;
                                                            $porce=$rowi[5];
                                                            if($rowc[6]=='S')
                                                            {
                                                                if($rowc[3]=='N')
                                                                {
                                                                    if($valorcred>0)
                                                                    {
                                                                        $sqlr="insert into comprobante_det (id_comp,cuenta,tercero,centrocosto,detalle, cheque,valdebito,valcredito,estado,vigencia) values ('5 $concecc','$rowc[4]','$_POST[tercero]','$centroCostosCeros','Ingreso Impuesto Predial Vigente $vig','','0','".round($valorcred,0)."','1','$_POST[vigencia]')";
                                                                        mysqli_query($linkbd,$sqlr);
                                                                        //$valordeb=round($valorcred-$descpredial,2);
                                                                        $valordeb=round($row[4],0);
                                                                        $sqlr="insert into comprobante_det (id_comp, cuenta, tercero, centrocosto, detalle, cheque, valdebito, valcredito, estado, vigencia) values ('5 $concecc','$cuentacb','$_POST[tercero]','$centroCostosCeros','Ingreso Impuesto Predial Vigente $vig','','".round($valordeb,0)."','0','1','$_POST[vigencia]')";
                                                                        mysqli_query($linkbd,$sqlr);
                                                                        //******MODIFICAR CUENTA PPTAL DE INGRESO AGREGARLE EL RECAUDO ******
                                                                    }
                                                                }
                                                            }
                                                            else
                                                            {
                                                                $cuentaAmb = $rowc[4];
                                                            }
                                                        }
                                                    break;
                                                    //Ingreso Sobretasa Ambiental
                                                    case '24': case '25': //***
                                                    {
                                                        $valorcred=$row[8];
                                                        $valordeb=$row[8];

                                                        if($valorcred>0)
                                                        {
                                                            $vi=$row[8];

                                                            $sqlrPresu="insert into pptorecibocajappto (cuenta,idrecibo,valor,vigencia,tipo,ingreso,fuente, seccion_presupuestal, medio_pago, vigencia_gasto) values('$rowi[6]',$concecc,".round($vi,0).",'$vigusu','N','$codigoIng', '$rowi[10]', '16', 'CSF', '1')";
                                                            mysqli_query($linkbd,$sqlrPresu);
                                                            $sqlrPresu="";
                                                        }

                                                        $sq="select fechainicial from conceptoscontables_det where codigo='$rowi[2]' and modulo='4' and tipo='PR' and fechainicial<'$fechaf' and cuenta!='' order by fechainicial asc";
                                                        $re=mysqli_query($linkbd,$sq);
                                                        while($ro=mysqli_fetch_assoc($re)){$_POST['fechacausa']=$ro["fechainicial"];}

                                                        $sqlrc="Select * from conceptoscontables_det where estado='S' and modulo='4' AND codigo='$rowi[2]' and tipo='PR' and fechainicial='$_POST[fechacausa]' ORDER BY cuenta DESC";
                                                        $resc=mysqli_query($linkbd,$sqlrc);
                                                        while($rowc=mysqli_fetch_row($resc))
                                                        {
                                                            $centroCostosCeros = str_pad($rowc[5], 2, "0", STR_PAD_LEFT);

                                                            $valordeb=0;

                                                            $inicioCuenta = substr($rowc[4], 0, 1);

                                                            if($inicioCuenta == '2'){

                                                                if($rowc[7] == 'S'){

                                                                    if($rowc[3]=='N'){

                                                                        if($valorcred>0){

                                                                            $sqlr = "insert into comprobante_det (id_comp,cuenta,tercero,centrocosto,detalle, cheque,valdebito,valcredito,estado,vigencia) values ('5 $concecc','$rowc[4]','$_POST[tercero]','$centroCostosCeros','Ingreso Sobretasa Ambiental $vig','','0','".round($valorcred,0)."','1','$_POST[vigencia]')";
                                                                            mysqli_query($linkbd, $sqlr);

                                                                            $valordeb = $valorcred;

                                                                            $sqlr="insert into comprobante_det (id_comp, cuenta, tercero, centrocosto, detalle, cheque, valdebito, valcredito, estado, vigencia) values ('5 $concecc', '$cuentacb','$_POST[tercero]', '$centroCostosCeros','Ingreso Sobretasa Ambiental $vig','','".round($valordeb,0)."','0','1','$_POST[vigencia]')";
                                                                            mysqli_query($linkbd, $sqlr);

                                                                        }
                                                                    }
                                                                }

                                                            }else{

                                                                switch ($rowc[7]){

                                                                    case 'S':

                                                                        if($rowc[3]=='N'){

                                                                            if($valorcred>0){

                                                                                $sqlr = "insert into comprobante_det (id_comp,cuenta,tercero,centrocosto,detalle, cheque,valdebito,valcredito,estado,vigencia) values ('5 $concecc','$rowc[4]','$_POST[tercero]','$centroCostosCeros','Reconocimiento de Ingreso Sobretasa Ambiental $vig','','0','".round($valorcred,0)."','1','$_POST[vigencia]')";
                                                                                mysqli_query($linkbd, $sqlr);

                                                                            }

                                                                        }

                                                                        break;

                                                                    default:

                                                                        if($rowc[3]=='N'){

                                                                            if($valorcred>0){

                                                                                $sqlr = "insert into comprobante_det (id_comp,cuenta,tercero,centrocosto,detalle, cheque,valdebito,valcredito,estado,vigencia) values ('5 $concecc','$rowc[4]','$_POST[tercero]','$centroCostosCeros','Reconocimiento de Ingreso Sobretasa Ambiental $vig','','".round($valorcred,0)."','0','1','$_POST[vigencia]')";
                                                                                mysqli_query($linkbd, $sqlr);

                                                                                $sqlr = "insert into comprobante_det (id_comp,cuenta,tercero,centrocosto,detalle, cheque,valdebito,valcredito,estado,vigencia) values ('5 $concecc','$rowc[4]','$_POST[tercero]','$centroCostosCeros','Ingreso Sobretasa Ambiental $vig','','".round($valordeb,0)."','".round($valorcred,0)."','1','$_POST[vigencia]')";
                                                                                mysqli_query($linkbd, $sqlr);

                                                                                $valordeb = $valorcred;

                                                                                $sqlr="insert into comprobante_det (id_comp, cuenta, tercero, centrocosto, detalle, cheque, valdebito, valcredito, estado, vigencia) values ('5 $concecc', '$cuentacb','$_POST[tercero]', '$centroCostosCeros','Ingreso Sobretasa Ambiental $vig','','".round($valordeb,0)."','0','1','$_POST[vigencia]')";
                                                                                mysqli_query($linkbd, $sqlr);

                                                                            }


                                                                        }

                                                                        break;

                                                                }

                                                            }


                                                        }
                                                    }break;
                                                    //Ingreso Sobretasa Bomberil
                                                    case '26': case '27':
                                                    {
                                                        $valorcred=$row[6];
                                                        $valordeb=$row[6];

                                                        if($valorcred>0)
                                                        {
                                                            $vi=$row[6];

                                                            $sqlrPresu="insert into pptorecibocajappto (cuenta,idrecibo,valor,vigencia,tipo,ingreso,fuente, seccion_presupuestal, medio_pago, vigencia_gasto) values('$rowi[6]',$concecc,".round($vi,0).",'$vigusu','N','$codigoIng', '$rowi[10]', '16', 'CSF', '1')";
                                                            mysqli_query($linkbd,$sqlrPresu);
                                                            $sqlrPresu="";
                                                        }

                                                        $sq="select fechainicial from conceptoscontables_det where codigo='$rowi[2]' and modulo='4' and tipo='PR' and fechainicial<'$fechaf' and cuenta!='' order by fechainicial asc";
                                                        $re=mysqli_query($linkbd,$sq);
                                                        while($ro=mysqli_fetch_assoc($re)){$_POST['fechacausa']=$ro["fechainicial"];}


                                                        $sqlrc="Select * from conceptoscontables_det where estado='S' and modulo='4' AND codigo='$rowi[2]' and tipo='PR' and fechainicial='$_POST[fechacausa]' order by cuenta desc";
                                                        $resc=mysqli_query($linkbd,$sqlrc);
                                                        while($rowc=mysqli_fetch_row($resc))
                                                        {
                                                            $centroCostosCeros = str_pad($rowc[5], 2, "0", STR_PAD_LEFT);

                                                            $valordeb=0;

                                                            switch($rowc[7]){

                                                                case 'S':

                                                                    if($rowc[3]=='N'){

                                                                        if($valorcred>0){

                                                                            $sqlr="insert into comprobante_det (id_comp,cuenta,tercero,centrocosto,detalle, cheque,valdebito,valcredito,estado,vigencia) values ('5 $concecc','$rowc[4]','$_POST[tercero]', '$centroCostosCeros','Reconocimiento de Ingreso Sobretasa Bomberil $vig','', '0','".round($valorcred,0)."','1','$_POST[vigencia]')";
                                                                            mysqli_query($linkbd,$sqlr);

                                                                        }

                                                                    }

                                                                    break;

                                                                default:

                                                                    if($rowc[3]=='N'){

                                                                        if($valorcred>0){

                                                                            $sqlr="insert into comprobante_det (id_comp,cuenta,tercero,centrocosto,detalle, cheque,valdebito,valcredito,estado,vigencia) values ('5 $concecc','$rowc[4]','$_POST[tercero]', '$centroCostosCeros','Reconocimiento de Ingreso Sobretasa Bomberil $vig','', '".round($valorcred,0)."','0','1','$_POST[vigencia]')";
                                                                            mysqli_query($linkbd,$sqlr);

                                                                            $sqlr="insert into comprobante_det (id_comp,cuenta,tercero,centrocosto,detalle, cheque,valdebito,valcredito,estado,vigencia) values ('5 $concecc','$rowc[4]','$_POST[tercero]', '$centroCostosCeros','Ingreso Sobretasa Bomberil $vig','', '".round($valordeb,0)."','".round($valorcred,0)."','1','$_POST[vigencia]')";
                                                                            mysqli_query($linkbd,$sqlr);

                                                                            $valordeb=round($valorcred,0);

                                                                            $sqlr="insert into comprobante_det (id_comp,cuenta,tercero,centrocosto, detalle, cheque,valdebito,valcredito,estado,vigencia) values ('5 $concecc','$cuentacb','$_POST[tercero]','$centroCostosCeros','Ingreso Sobretasa Bomberil $vig','', '".round($valordeb,0)."','0','1','$_POST[vigencia]')";
                                                                            mysqli_query($linkbd,$sqlr);

                                                                        }

                                                                    }

                                                                    break;

                                                            }

                                                        }
                                                    }break;
                                                    //Descuento Pronto Pago Predial
                                                    case '31':
                                                    {
                                                        $sq="select fechainicial from conceptoscontables_det where codigo='$rowi[2]' and modulo='4' and tipo='PR' and fechainicial<'$fechaf' and cuenta!='' order by fechainicial asc";
                                                        $re=mysqli_query($linkbd,$sq);
                                                        while($ro=mysqli_fetch_assoc($re)){$_POST['fechacausa']=$ro["fechainicial"];}

                                                        $sqlrc="Select * from conceptoscontables_det where estado='S' and modulo='4' AND codigo='$rowi[2]' and tipo='PR' and fechainicial='".$_POST['fechacausa']."'";
                                                        $resc = mysqli_query($linkbd, $sqlrc);
                                                        while($rowc=mysqli_fetch_row($resc))
                                                        {
                                                            $valordeb=round($row[10],0);
                                                            $centroCostosCeros = str_pad($rowc[5], 2, "0", STR_PAD_LEFT);

                                                            if($rowc[7]=='S')
                                                            {
                                                                $valorcred=0;
                                                                if($rowc[3]=='N')
                                                                {
                                                                    if($valordeb>0)
                                                                    {
                                                                        $sqlr="insert into comprobante_det (id_comp,cuenta,tercero,centrocosto,detalle, cheque,valdebito,valcredito,estado, vigencia) values ('5 $concecc', '$rowc[4]','$_POST[tercero]', '$centroCostosCeros','Descuento Pronto Pago Predial $vig','','".round($valordeb,0)."','$valorcred','1','$_POST[vigencia]')";
                                                                        mysqli_query($linkbd,$sqlr);
                                                                    }
                                                                }
                                                            }
                                                        }

                                                    }break;
                                                    //Intereses Predial
                                                    case '28':
                                                    {
                                                        $sqlrDescIntPredial = "SELECT descuentointpredial FROM tesoliquidapredial_desc WHERE id_predial='".$row[0]."' AND vigencia = '$vig'";
                                                        $rescDescIntPredial = mysqli_query($linkbd, $sqlrDescIntPredial);
                                                        $rowcDescIntPredial = mysqli_fetch_row($rescDescIntPredial);

                                                        $valorcred=$row[5]-$rowcDescIntPredial[0];
                                                        $valordeb=$valorcred;

                                                        if($valorcred>0)
                                                        {
                                                            $vi=$valorcred;

                                                            $sqlrPresu="insert into pptorecibocajappto (cuenta,idrecibo,valor,vigencia,tipo,ingreso,fuente, seccion_presupuestal, medio_pago, vigencia_gasto) values('$rowi[6]',$concecc,".round($vi,0).",'$vigusu','N','$codigoIng', '$rowi[10]', '16', 'CSF', '1')";
                                                            mysqli_query($linkbd,$sqlrPresu);
                                                            $sqlrPresu="";
                                                        }

                                                        $sq="select fechainicial from conceptoscontables_det where codigo='$rowi[2]' and modulo='4' and tipo='PR' and fechainicial<'$fechaf' and cuenta!='' order by fechainicial asc";
                                                        $re=mysqli_query($linkbd,$sq);
                                                        while($ro=mysqli_fetch_assoc($re)){$_POST['fechacausa']=$ro["fechainicial"];}

                                                        $sqlrc="Select * from conceptoscontables_det where estado='S' and modulo='4' AND codigo = '$rowi[2]' and tipo='PR' and fechainicial='$_POST[fechacausa]' ORDER BY cuenta DESC";
                                                        $resc=mysqli_query($linkbd,$sqlrc);
                                                        while($rowc=mysqli_fetch_row($resc))
                                                        {
                                                            $centroCostosCeros = str_pad($rowc[5], 2, "0", STR_PAD_LEFT);

                                                            $valordeb=0;

                                                            switch($rowc[7]){

                                                                case 'S':

                                                                    if($rowc[3]=='N'){

                                                                        if($valorcred > 0){

                                                                            $sqlr = "insert into comprobante_det (id_comp, cuenta, tercero, centrocosto, detalle, cheque, valdebito, valcredito, estado, vigencia) values ('5 $concecc', '$rowc[4]','$_POST[tercero]', '$centroCostosCeros','Reconocimiento de Intereses Predial $vig','','0','".round($valorcred,0)."','1','$_POST[vigencia]')";
                                                                            mysqli_query($linkbd, $sqlr);

                                                                        }

                                                                    }

                                                                    break;

                                                                default:

                                                                    if($rowc[3]=='N'){

                                                                        if($valorcred > 0){

                                                                            $sqlr = "insert into comprobante_det (id_comp, cuenta, tercero, centrocosto, detalle, cheque, valdebito, valcredito, estado, vigencia) values ('5 $concecc', '$rowc[4]','$_POST[tercero]', '$centroCostosCeros','Reconocimiento de Intereses Predial $vig','','".round($valorcred,0)."','0','1','$_POST[vigencia]')";
                                                                            mysqli_query($linkbd, $sqlr);

                                                                            $sqlr = "insert into comprobante_det (id_comp,cuenta,tercero,centrocosto,detalle, cheque,valdebito,valcredito,estado,vigencia) values ('5 $concecc','$rowc[4]','$_POST[tercero]','$centroCostosCeros','Intereses Predial $vig','','".round($valordeb,0)."', '".round($valorcred,0)."','1','$_POST[vigencia]')";
                                                                            mysqli_query($linkbd, $sqlr);

                                                                            $valordeb = $valorcred;
                                                                            $sqlr = "insert into comprobante_det (id_comp,cuenta,tercero,centrocosto,detalle, cheque,valdebito,valcredito,estado,vigencia) values ('5 $concecc','$cuentacb','$_POST[tercero]','$centroCostosCeros','Intereses Predial $vig','','".round($valordeb,0)."', '0','1','$_POST[vigencia]')";
                                                                            mysqli_query($linkbd, $sqlr);

                                                                        }

                                                                    }

                                                                    break;

                                                            }


                                                        }
                                                    }break;
                                                    //Intereses Bomberil
                                                    case '30':
                                                    {
                                                        $sqlrDescIntBomberil = "SELECT descuentointbomberil FROM tesoliquidapredial_desc WHERE id_predial='".$row[0]."' AND vigencia = '$vig'";
                                                        $rescDescIntBomberil = mysqli_query($linkbd, $sqlrDescIntBomberil);
                                                        $rowcDescIntBomberil = mysqli_fetch_row($rescDescIntBomberil);

                                                        $valorcred=$row[7]-$rowcDescIntBomberil[0];
                                                        $valordeb=$valorcred;

                                                        if($valorcred>0)
                                                        {
                                                            $vi=$valorcred;

                                                            $sqlrPresu="insert into pptorecibocajappto (cuenta,idrecibo,valor,vigencia,tipo,ingreso,fuente, seccion_presupuestal, medio_pago, vigencia_gasto) values('$rowi[6]',$concecc,".round($vi,0).",'$vigusu','N','$codigoIng', '$rowi[10]', '16', 'CSF', '1')";
                                                            mysqli_query($linkbd,$sqlrPresu);
                                                            $sqlrPresu="";
                                                        }

                                                        $sq="select fechainicial from conceptoscontables_det where codigo='$rowi[2]' and modulo='4' and tipo='PR' and fechainicial<'$fechaf' and cuenta!='' order by fechainicial asc";
                                                        $re=mysqli_query($linkbd,$sq);
                                                        while($ro=mysqli_fetch_assoc($re)){$_POST['fechacausa']=$ro["fechainicial"];}

                                                        $sqlrc="Select * from conceptoscontables_det where estado='S' and modulo='4' AND codigo='$rowi[2]' and tipo='PR' and fechainicial='$_POST[fechacausa]' order by cuenta desc";
                                                        $resc=mysqli_query($linkbd,$sqlrc);
                                                        while($rowc=mysqli_fetch_row($resc))
                                                        {
                                                            $centroCostosCeros = str_pad($rowc[5], 2, "0", STR_PAD_LEFT);

                                                            $valordeb = 0;

                                                            switch ($rowc[7]){

                                                                case 'S':

                                                                    if($rowc[3]=='N'){

                                                                        if($valorcred > 0){

                                                                            $sqlr="insert into comprobante_det (id_comp, cuenta, tercero, centrocosto, detalle, cheque, valdebito, valcredito, estado, vigencia) values ('5 $concecc', '$rowc[4]','$_POST[tercero]', '$centroCostosCeros','Reconocimiento de Intereses Sobretasa Bomberil $vig','','0','".round($valorcred,0)."','1','$_POST[vigencia]')";
                                                                            mysqli_query($linkbd,$sqlr);

                                                                        }

                                                                    }

                                                                    break;

                                                                default:

                                                                    if($rowc[3]=='N'){

                                                                        if($valorcred > 0){

                                                                            $sqlr="insert into comprobante_det (id_comp, cuenta, tercero, centrocosto, detalle, cheque, valdebito, valcredito, estado, vigencia) values ('5 $concecc', '$rowc[4]','$_POST[tercero]', '$centroCostosCeros','Reconocimiento de Intereses Sobretasa Bomberil $vig','','".round($valorcred,0)."','0','1','$_POST[vigencia]')";
                                                                            mysqli_query($linkbd, $sqlr);

                                                                            $sqlr="insert into comprobante_det (id_comp,cuenta,tercero,centrocosto,detalle, cheque,valdebito,valcredito,estado,vigencia) values ('5 $concecc','$rowc[4]','$_POST[tercero]','$centroCostosCeros','Intereses Sobretasa Bomberil $vig','', '".round($valordeb,0)."','".round($valorcred,0)."','1','$_POST[vigencia]')";
                                                                            mysqli_query($linkbd, $sqlr);

                                                                            $valordeb = $valorcred;

                                                                            $sqlr = "insert into comprobante_det (id_comp, cuenta, tercero, centrocosto, detalle,cheque,valdebito,valcredito,estado,vigencia) values ('5 $concecc','$cuentacb','$_POST[tercero]','$centroCostosCeros','Intereses Sobretasa Bomberil $vig','','".round($valordeb,0)."','0','1','$_POST[vigencia]')";
                                                                            mysqli_query($linkbd, $sqlr);

                                                                        }

                                                                    }

                                                                    break;

                                                            }
                                                        }
                                                    }break;

                                                    //Intereses Sobtretasa Ambiental

                                                    case '29':
                                                    {
                                                        $sqlrDescIntAmbiental = "SELECT descuentointambiental FROM tesoliquidapredial_desc WHERE id_predial='".$row[0]."' AND vigencia = '$vig'";
                                                        $rescDescIntAmbiental = mysqli_query($linkbd, $sqlrDescIntAmbiental);
                                                        $rowcDescIntAmbiental = mysqli_fetch_row($rescDescIntAmbiental);

                                                        $valorcred=$row[9]-$rowcDescIntAmbiental[0];
                                                        $valordeb=$valorcred;

                                                        if($valorcred>0)
                                                        {
                                                            $vi=$valorcred;

                                                            $sqlrPresu="insert into pptorecibocajappto (cuenta,idrecibo,valor,vigencia,tipo,ingreso,fuente, seccion_presupuestal, medio_pago, vigencia_gasto) values('$rowi[6]',$concecc,".round($vi,0).",'$vigusu','N','$codigoIng', '$rowi[10]', '16', 'CSF', '1')";
                                                            mysqli_query($linkbd,$sqlrPresu);
                                                            $sqlrPresu="";
                                                        }

                                                        $sq="select fechainicial from conceptoscontables_det where codigo='$rowi[2]' and modulo='4' and tipo='PR' and fechainicial<'$fechaf' and cuenta!='' order by fechainicial asc";
                                                        $re=mysqli_query($linkbd,$sq);
                                                        while($ro=mysqli_fetch_assoc($re)){$_POST['fechacausa']=$ro["fechainicial"];}

                                                        $sqlrc="Select * from conceptoscontables_det where estado='S' and modulo='4' AND codigo='$rowi[2]' and tipo='PR' and fechainicial='$_POST[fechacausa]' order by cuenta desc";
                                                        $resc=mysqli_query($linkbd,$sqlrc);
                                                        while($rowc=mysqli_fetch_row($resc))
                                                        {
                                                            $centroCostosCeros = str_pad($rowc[5], 2, "0", STR_PAD_LEFT);

                                                            $valordeb=0;

                                                            $inicioCuenta = substr($rowc[4], 0, 1);

                                                            if($inicioCuenta == '2'){

                                                                if($rowc[7] == 'S'){

                                                                    if($rowc[3]=='N'){

                                                                        if($valorcred>0){

                                                                            $sqlr = "insert into comprobante_det (id_comp,cuenta,tercero,centrocosto,detalle, cheque,valdebito,valcredito,estado,vigencia) values ('5 $concecc','$rowc[4]','$_POST[tercero]', '$centroCostosCeros','Intereses Sobtretasa Ambiental $vig','','0','".round($valorcred,0)."','1','$_POST[vigencia]')";
                                                                            mysqli_query($linkbd, $sqlr);

                                                                            $valordeb = $valorcred;

                                                                            $sqlr = "insert into comprobante_det (id_comp, cuenta, tercero, centrocosto, detalle, cheque, valdebito, valcredito, estado, vigencia) values ('5 $concecc', '$cuentacb','$_POST[tercero]', '$centroCostosCeros','Intereses Sobtretasa Ambiental $vig','','".round($valordeb,0)."','0','1','$_POST[vigencia]')";
                                                                            mysqli_query($linkbd, $sqlr);

                                                                        }
                                                                    }
                                                                }

                                                            }else{

                                                                switch($rowc[7]){

                                                                    case 'S':

                                                                        if($rowc[3]=='N'){

                                                                            if($valorcred > 0){

                                                                                $sqlr="insert into comprobante_det (id_comp,cuenta,tercero,centrocosto,detalle, cheque,valdebito,valcredito,estado,vigencia) values ('5 $concecc','$rowc[4]','$_POST[tercero]', '$centroCostosCeros','Reconocimiento de Intereses Sobtretasa Ambiental $vig','','0','".round($valorcred,0)."','1','$_POST[vigencia]')";
                                                                                mysqli_query($linkbd, $sqlr);

                                                                            }

                                                                        }

                                                                        break;

                                                                    default:

                                                                        if($rowc[3]=='N'){

                                                                            if($valorcred > 0){

                                                                                $sqlr = "insert into comprobante_det (id_comp,cuenta,tercero,centrocosto,detalle, cheque,valdebito,valcredito,estado,vigencia) values ('5 $concecc','$rowc[4]','$_POST[tercero]', '$centroCostosCeros','Reconocimiento de Intereses Sobtretasa Ambiental $vig','','".round($valorcred,0)."','0','1','$_POST[vigencia]')";
                                                                                mysqli_query($linkbd,$sqlr);

                                                                                $sqlr = "insert into comprobante_det (id_comp,cuenta,tercero,centrocosto,detalle, cheque,valdebito,valcredito,estado,vigencia) values ('5 $concecc','$rowc[4]','$_POST[tercero]', '$centroCostosCeros','Intereses Sobtretasa Ambiental $vig','','".round($valordeb,0)."','".round($valorcred,0)."','1','$_POST[vigencia]')";
                                                                                mysqli_query($linkbd,$sqlr);

                                                                                $valordeb = $valorcred;

                                                                                $sqlr = "insert into comprobante_det (id_comp, cuenta, tercero, centrocosto, detalle, cheque, valdebito, valcredito, estado, vigencia) values ('5 $concecc', '$cuentacb','$_POST[tercero]', '$centroCostosCeros','Intereses Sobtretasa Ambiental $vig','','".round($valordeb,0)."','0','1','$_POST[vigencia]')";
                                                                                mysqli_query($linkbd,$sqlr);

                                                                            }

                                                                        }

                                                                        break;
                                                                }
                                                            }
                                                        }
                                                    }break;

                                                    //Alumbrado
                                                    case '34': //***
                                                    {
                                                        $siAlumbrado=0;
                                                        $sqlrDesc = "SELECT val_alumbrado FROM tesoliquidapredial_desc WHERE id_predial='$row[0]' AND vigencia=$vig ORDER BY vigencia ASC";
                                                        $resDesc = mysqli_query($linkbd,$sqlrDesc);
                                                        while ($rowDesc =mysqli_fetch_assoc($resDesc))
                                                        {
                                                            $siAlumbrado = $rowDesc['val_alumbrado'];
                                                        }
                                                        if($siAlumbrado>0)
                                                        {
                                                            $valorAlumbrado=round($siAlumbrado,0);
                                                            $valorcred=$valorAlumbrado;
                                                            $valordeb=0;

                                                            if($valorcred>0)
                                                            {
                                                                $sqlrPresu="insert into pptorecibocajappto (cuenta,idrecibo,valor,vigencia,tipo,ingreso,fuente, seccion_presupuestal, medio_pago, vigencia_gasto) values('$rowi[6]',$concecc,".round($valorcred).",'$vigusu','N','$codigoIng', '$rowi[10]', '16', 'CSF', '1')";
                                                                mysqli_query($linkbd,$sqlrPresu);
                                                                $sqlrPresu="";
                                                            }

                                                            //$valorAlumbrado=round($row[2]*($vcobroalumbrado/1000),0);
                                                            $sq="select fechainicial from conceptoscontables_det where codigo='$rowi[2]' and modulo='4' and tipo='PR' and fechainicial<'$fechaf' and cuenta!='' order by fechainicial asc";
                                                            $re=mysqli_query($linkbd,$sq);
                                                            while($ro=mysqli_fetch_assoc($re)){$_POST['fechacausa']=$ro["fechainicial"];}


                                                            $sqlrc="Select * from conceptoscontables_det where estado='S' and modulo='4' AND codigo='$rowi[2]' and tipo='PR' and fechainicial='".$_POST['fechacausa']."' order by cuenta desc";
                                                            $resc=mysqli_query($linkbd,$sqlrc);
                                                            while($rowc=mysqli_fetch_row($resc))
                                                            {
                                                                $centroCostosCeros = str_pad($rowc[5], 2, "0", STR_PAD_LEFT);

                                                                switch ($rowc[7]){

                                                                    case 'S':

                                                                        if($rowc[3]=='N'){

                                                                            if($valorcred > 0){

                                                                                $sqlr="insert into comprobante_det (id_comp,cuenta,tercero,centrocosto,detalle, cheque,valdebito,valcredito,estado,vigencia) values ('5 $concecc','$rowc[4]','$_POST[tercero]','$centroCostosCeros','Reconocimiento de Ingreso Impuesto sobre el Servicio de Alumbrado Publico $vig','','0','".round($valorcred,0)."','1','$_POST[vigencia]')";
                                                                                mysqli_query($linkbd, $sqlr);

                                                                            }

                                                                        }

                                                                        break;

                                                                    default:

                                                                        if($rowc[3]=='N'){

                                                                            if($valorcred > 0){

                                                                                $sqlr = "insert into comprobante_det (id_comp,cuenta,tercero,centrocosto,detalle, cheque,valdebito,valcredito,estado,vigencia) values ('5 $concecc','$rowc[4]','$_POST[tercero]','$centroCostosCeros','Reconocimiento de Ingreso Impuesto sobre el Servicio de Alumbrado Publico $vig','','".round($valorcred,0)."','0','1','$_POST[vigencia]')";
                                                                                mysqli_query($linkbd, $sqlr);

                                                                                $sqlr = "insert into comprobante_det (id_comp,cuenta,tercero,centrocosto,detalle, cheque,valdebito,valcredito,estado,vigencia) values ('5 $concecc','$rowc[4]','$_POST[tercero]','$centroCostosCeros','Ingreso Impuesto sobre el Servicio de Alumbrado Publico $vig','','".round($valordeb,0)."','".round($valorcred,0)."','1','$_POST[vigencia]')";
                                                                                mysqli_query($linkbd, $sqlr);

                                                                                $valordeb = $valorcred;

                                                                                $sqlr = "insert into comprobante_det (id_comp, cuenta, tercero, centrocosto, detalle, cheque, valdebito, valcredito, estado, vigencia) values ('5 $concecc', '$cuentacb','".$_POST['tercero']."', '$centroCostosCeros','Ingreso Impuesto sobre el Servicio de Alumbrado Publico $vig','','".round($valordeb,0)."','0','1','".$_POST['vigencia']."')";
                                                                                mysqli_query($linkbd, $sqlr);

                                                                            }

                                                                        }

                                                                        break;

                                                                }


                                                            }
                                                        }
                                                    }break;
                                                }
                                            }
                                            if($_POST['tipo']=='1')
                                            {
                                                $_POST['dcoding'][]=$row2[0]['codigo'];
                                                $_POST['dncoding'][]=$row2[0]['nombre']." ".$vig;
                                                $_POST['dvalores'][]=$dvalor;
                                            }
                                            else
                                            {
                                                $_POST['dcoding'][]=$row2[0];
                                                $_POST['dncoding'][]=$row2[1]." ".$vig;
                                                $_POST['dvalores'][]=$row[11];
                                            }
                                        }
                                        else  ///***********OTRAS VIGENCIAS ***********
                                        {
                                            //$tasadesc=$row[10]/($row[4]+$row[6]);
                                            $tasadesc=$row[10];
                                            $idcomp=mysqli_insert_id($linkbd);
                                            echo "<input type='hidden' name='ncomp' value='$idcomp'>";
                                            $sqlr="update tesoreciboscaja set id_comp=$idcomp WHERE id_recaudo=$_POST[idrecaudo] and tipo='1'";
                                            mysqli_query($linkbd,$sqlr);

                                            $codigoIng = '';
                                            if(substr($cedulaCatastral,0,2) == '00'){
                                                $codigoIng = '04';
                                            }else{
                                                $codigoIng = '02';
                                            }

                                            $sqlr2="SELECT * FROM tesoingresos_predial_det WHERE codigo='$codigoIng' AND vigencia=(SELECT MAX(vigencia) FROM tesoingresos_predial_det WHERE codigo = '$codigoIng') GROUP BY concepto ORDER BY concepto ASC";

                                            $res2=mysqli_query($linkbd,$sqlr2);
                                            //****** $cuentacb   ES LA CUENTA CAJA O BANCO
                                            while($rowi =mysqli_fetch_row($res2))
                                            {
                                                switch($rowi[2])
                                                {
                                                    //Ingreso Impuesto Predial Otras Vigencias
                                                    case '22': case '23': //***

                                                        $valorcred=$row[4];
                                                        $valordeb=$valorcred;

                                                        if($valorcred>0)
                                                        {
                                                            if($_POST['cobro_ambiental'] == 'S'){
                                                                $vi=$row[4]-$tasadesc-$row[8];
                                                            }else{
                                                                $vi=$row[4]-$tasadesc;
                                                            }


                                                            $sqlrPresu="insert into pptorecibocajappto (cuenta,idrecibo,valor,vigencia,tipo,ingreso,fuente, seccion_presupuestal, medio_pago, vigencia_gasto) values('$rowi[6]',$concecc,".round($vi,0).",'$vigusu','N','$codigoIng', '$rowi[10]', '16', 'CSF', '1')";
                                                            mysqli_query($linkbd,$sqlrPresu);
                                                            $sqlrPresu="";
                                                        }

                                                        $sq="select fechainicial from conceptoscontables_det where codigo='$rowi[2]' and modulo='4' and tipo='PR' and fechainicial<'$fechaf' and cuenta!='' order by fechainicial asc";
                                                        $re=mysqli_query($linkbd,$sq);
                                                        while($ro=mysqli_fetch_assoc($re))
                                                        {
                                                            $_POST['fechacausa']=$ro["fechainicial"];
                                                        }
                                                        $sqlrc="Select * from conceptoscontables_det where estado='S' and modulo='4' AND codigo='$rowi[2]' and tipo='PR' and fechainicial='".$_POST['fechacausa']."'";
                                                        $resc=mysqli_query($linkbd,$sqlrc);
                                                        while($rowc=mysqli_fetch_row($resc))
                                                        {
                                                            $centroCostosCeros = str_pad($rowc[5], 2, "0", STR_PAD_LEFT);
                                                            $porce=$rowi[5];
                                                            if($rowc[6]=='S')
                                                            {
                                                                $valordeb=0;
                                                                if($rowc[3]=='N')
                                                                {
                                                                    if($valorcred>0)
                                                                    {
                                                                        $sqlr="insert into comprobante_det (id_comp, cuenta, tercero, centrocosto, detalle, cheque, valdebito, valcredito, estado, vigencia) values ('5 $concecc', '$rowc[4]','".$_POST['tercero']."','$centroCostosCeros','Ingreso Impuesto Predial Otras Vigencias $vig','','".round($valordeb,0)."','".round($valorcred,0)."','1','".$_POST['vigencia']."')";
                                                                        mysqli_query($linkbd,$sqlr);
                                                                        //$valordeb=$valorcred-$tasadesc*$valorcred;
                                                                        $valordeb=$valorcred-$tasadesc;
                                                                        $sqlr="insert into comprobante_det (id_comp, cuenta, tercero, centrocosto, detalle, cheque, valdebito, valcredito, estado, vigencia) values ('5 $concecc', '$cuentacb','".$_POST['tercero']."', '$centroCostosCeros','Ingreso Impuesto Predial Otras Vigencias $vig','','".round($valordeb,0)."',0,'1','".$_POST['vigencia']."')";
                                                                        mysqli_query($linkbd,$sqlr);
                                                                    }
                                                                }
                                                            }
                                                            else
                                                            {
                                                                $cuentaAmb = $rowc[4];
                                                            }
                                                        }
                                                    break;
                                                    //Ingreso Sobretasa Ambiental
                                                    case '24': case '25': //***
                                                    {
                                                        $valorcred=$row[8];
                                                        $valordeb=$valorcred;

                                                        if($valorcred>0)
                                                        {
                                                            $vi=$row[8];

                                                            $sqlrPresu = "insert into pptorecibocajappto (cuenta,idrecibo,valor,vigencia,tipo,ingreso,fuente, seccion_presupuestal, medio_pago, vigencia_gasto) values('$rowi[6]',$concecc,".round($vi,0).",'$vigusu','N','$codigoIng', '$rowi[10]', '16', 'CSF', '1')";
                                                            mysqli_query($linkbd,$sqlrPresu);
                                                            $sqlrPresu = "";
                                                        }

                                                        $sq="select fechainicial from conceptoscontables_det where codigo='$rowi[2]' and modulo='4' and tipo='PR' and fechainicial<'$fechaf' and cuenta!='' order by fechainicial asc";
                                                        $re=mysqli_query($linkbd,$sq);
                                                        while($ro=mysqli_fetch_assoc($re)){$_POST['fechacausa']=$ro["fechainicial"];}

                                                        $sqlrc="Select * from conceptoscontables_det where estado='S' and modulo='4' AND codigo='$rowi[2]' and tipo='PR' and fechainicial='$_POST[fechacausa]' ORDER BY cuenta DESC";
                                                        $resc=mysqli_query($linkbd,$sqlrc);
                                                        while($rowc=mysqli_fetch_row($resc))
                                                        {
                                                            $centroCostosCeros = str_pad($rowc[5], 2, "0", STR_PAD_LEFT);

                                                            $valordeb=0;

                                                            $inicioCuenta = substr($rowc[4], 0, 1);

                                                            if($inicioCuenta == '2'){

                                                                if($rowc[7] == 'S'){

                                                                    if($rowc[3]=='N'){

                                                                        if($valorcred>0){

                                                                            $sqlr = "insert into comprobante_det (id_comp,cuenta,tercero,centrocosto,detalle, cheque,valdebito,valcredito,estado,vigencia) values ('5 $concecc','$rowc[4]','$_POST[tercero]','$centroCostosCeros','Ingreso Sobretasa Ambiental $vig','', '0','".round($valorcred,0)."','1','$_POST[vigencia]')";
                                                                            mysqli_query($linkbd, $sqlr);

                                                                            $valordeb = $valorcred;

                                                                            $sqlr = "insert into comprobante_det (id_comp,cuenta,tercero,centrocosto,detalle, cheque,valdebito,valcredito,estado,vigencia) values ('5 $concecc','$cuentacb','$_POST[tercero]','$centroCostosCeros','Ingreso Sobretasa Ambiental $vig','', '".round($valordeb,0)."',0,'1','$_POST[vigencia]')";
                                                                            mysqli_query($linkbd, $sqlr);

                                                                        }
                                                                    }

                                                                }

                                                            }else{

                                                                switch ($rowc[7]) {

                                                                    case 'S':

                                                                        if($rowc[3] == 'N'){

                                                                            if($valorcred > 0){

                                                                                $sqlr="insert into comprobante_det (id_comp,cuenta,tercero,centrocosto,detalle, cheque,valdebito,valcredito,estado,vigencia) values ('5 $concecc','$rowc[4]','$_POST[tercero]','$centroCostosCeros','Reconocimiento de Ingreso Sobretasa Ambiental $vig','', '0','".round($valorcred,0)."','1','$_POST[vigencia]')";
                                                                                mysqli_query($linkbd, $sqlr);

                                                                            }

                                                                        }

                                                                        break;

                                                                    default:

                                                                        if($rowc[3] == 'N'){

                                                                            if($valorcred > 0){

                                                                                $sqlr = "insert into comprobante_det (id_comp,cuenta,tercero,centrocosto,detalle, cheque,valdebito,valcredito,estado,vigencia) values ('5 $concecc','$rowc[4]','$_POST[tercero]','$centroCostosCeros','Reconocimiento de Ingreso Sobretasa Ambiental $vig','', '".round($valorcred,0)."','0','1','$_POST[vigencia]')";
                                                                                mysqli_query($linkbd, $sqlr);

                                                                                $sqlr = "insert into comprobante_det (id_comp,cuenta,tercero,centrocosto,detalle, cheque,valdebito,valcredito,estado,vigencia) values ('5 $concecc','$rowc[4]','$_POST[tercero]','$centroCostosCeros','Ingreso Sobretasa Ambiental $vig','', '".round($valordeb,0)."','".round($valorcred,0)."','1','$_POST[vigencia]')";
                                                                                mysqli_query($linkbd, $sqlr);

                                                                                $valordeb = $valorcred;

                                                                                $sqlr = "insert into comprobante_det (id_comp,cuenta,tercero,centrocosto,detalle, cheque,valdebito,valcredito,estado,vigencia) values ('5 $concecc','$cuentacb','$_POST[tercero]','$centroCostosCeros','Ingreso Sobretasa Ambiental $vig','', '".round($valordeb,0)."',0,'1','$_POST[vigencia]')";
                                                                                mysqli_query($linkbd, $sqlr);

                                                                            }

                                                                        }

                                                                        break;

                                                                }
                                                            }
                                                        }
                                                    }break;
                                                    //Ingreso Sobretasa Bomberil
                                                    case '26': case '27':
                                                    {
                                                        $valorcred=$row[6];
                                                        $valordeb=$valorcred;

                                                        if($valorcred>0)
                                                        {
                                                            $vi=$row[6];

                                                            $sqlrPresu="insert into pptorecibocajappto (cuenta,idrecibo,valor,vigencia,tipo,ingreso,fuente, seccion_presupuestal, medio_pago, vigencia_gasto) values('$rowi[6]',$concecc,".round($vi,0).",'$vigusu','N','$codigoIng', '$rowi[10]', '16', 'CSF', '1')";
                                                            mysqli_query($linkbd,$sqlrPresu);
                                                            $sqlrPresu="";
                                                        }
                                                        $sq="select fechainicial from conceptoscontables_det where codigo='$rowi[2]' and modulo='4' and tipo='PR' and fechainicial<'$fechaf' and cuenta!='' order by fechainicial asc";
                                                        $re=mysqli_query($linkbd,$sq);
                                                        while($ro=mysqli_fetch_assoc($re)){$_POST['fechacausa']=$ro["fechainicial"];}

                                                        $sqlrc="Select * from conceptoscontables_det where estado='S' and modulo='4' AND codigo='$rowi[2]' and tipo='PR' and fechainicial='$_POST[fechacausa]' ORDER BY cuenta DESC";
                                                        $resc=mysqli_query($linkbd,$sqlrc);
                                                        while($rowc=mysqli_fetch_row($resc))
                                                        {
                                                            $centroCostosCeros = str_pad($rowc[5], 2, "0", STR_PAD_LEFT);

                                                            $valordeb = 0;

                                                            switch ($rowc[7]) {

                                                                case 'S':

                                                                    if($rowc[3] == 'N'){

                                                                        if($valorcred > 0){

                                                                            $sqlr="insert into comprobante_det (id_comp,cuenta,tercero,centrocosto,detalle, cheque,valdebito,valcredito,estado,vigencia) values ('5 $concecc','$rowc[4]','$_POST[tercero]','$centroCostosCeros','Reconocimiento de Ingreso Sobretasa Bomberil $vig','', '0','".round($valorcred,0)."','1','$_POST[vigencia]')";
                                                                            mysqli_query($linkbd, $sqlr);

                                                                        }

                                                                    }

                                                                    break;

                                                                default:

                                                                    if($rowc[3] == 'N'){

                                                                        if($valorcred > 0){

                                                                            $sqlr="insert into comprobante_det (id_comp,cuenta,tercero,centrocosto,detalle, cheque,valdebito,valcredito,estado,vigencia) values ('5 $concecc','$rowc[4]','$_POST[tercero]','$centroCostosCeros','Reconocimiento de Ingreso Sobretasa Bomberil $vig','', '".round($valorcred,0)."','0','1','$_POST[vigencia]')";
                                                                            mysqli_query($linkbd,$sqlr);

                                                                            $sqlr="insert into comprobante_det (id_comp,cuenta,tercero,centrocosto,detalle, cheque,valdebito,valcredito,estado,vigencia) values ('5 $concecc','$rowc[4]','$_POST[tercero]','$centroCostosCeros','Ingreso Sobretasa Bomberil $vig','', '".round($valordeb,0)."','".round($valorcred,0)."','1','$_POST[vigencia]')";
                                                                            mysqli_query($linkbd,$sqlr);

                                                                            $valordeb = $valorcred;

                                                                            $sqlr="insert into comprobante_det (id_comp,cuenta,tercero,centrocosto,detalle, cheque,valdebito,valcredito,estado,vigencia) values ('5 $concecc','$cuentacb','$_POST[tercero]','$centroCostosCeros','Ingreso Sobretasa Bomberil $vig','','".round($valordeb,0)."',0,'1','$_POST[vigencia]')";
                                                                            mysqli_query($linkbd,$sqlr);

                                                                        }

                                                                    }

                                                                    break;

                                                            }

                                                        }
                                                    }break;
                                                    //Descuento por pronto pago predial.
                                                    case '31':
                                                    {
                                                        $sq="select fechainicial from conceptoscontables_det where codigo='$rowi[2]' and modulo='4' and tipo='PR' and fechainicial<'$fechaf' and cuenta!='' order by fechainicial asc";
                                                        $re=mysqli_query($linkbd,$sq);
                                                        while($ro=mysqli_fetch_assoc($re)){$_POST['fechacausa']=$ro["fechainicial"];}
                                                        $sqlrc="Select * from conceptoscontables_det where estado='S' and modulo='4' AND codigo='$rowi[2]' and tipo='PR' and fechainicial='$_POST[fechacausa]'";
                                                        $resc=mysqli_query($linkbd,$sqlrc);
                                                        while($rowc=mysqli_fetch_row($resc))
                                                        {
                                                            $centroCostosCeros = str_pad($rowc[5], 2, "0", STR_PAD_LEFT);
                                                            if($rowc[7]=='S')
                                                            {
                                                                $valordeb=$row[10];
                                                                $valorcred=0;
                                                                if($rowc[3]=='N')
                                                                {
                                                                    if($valordeb>0)
                                                                    {
                                                                        $sqlr="insert into comprobante_det (id_comp, cuenta, tercero, centrocosto, detalle, cheque, valdebito, valcredito, estado, vigencia) values ('5 $concecc', '$rowc[4]','$_POST[tercero]', '$centroCostosCeros','Descuento Pronto Pago $vig','', '".round($valordeb,0)."','".round($valorcred,2)."','1','$_POST[vigencia]')";
                                                                        mysqli_query($linkbd,$sqlr);
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }break;
                                                    case '28': //Intereses Predial
                                                    {
                                                        $sqlrDescIntPredial = "SELECT descuentointpredial FROM tesoliquidapredial_desc WHERE id_predial='".$row[0]."' AND vigencia = '$vig'";
                                                        $rescDescIntPredial = mysqli_query($linkbd, $sqlrDescIntPredial);
                                                        $rowcDescIntPredial = mysqli_fetch_row($rescDescIntPredial);

                                                        $valorcred=$row[5]-$rowcDescIntPredial[0];
                                                        $valordeb=$valorcred;

                                                        if($valorcred>0)
                                                        {
                                                            $vi=$valorcred;

                                                            $sqlrPresu="insert into pptorecibocajappto (cuenta,idrecibo,valor,vigencia,tipo,ingreso,fuente, seccion_presupuestal, medio_pago, vigencia_gasto) values('$rowi[6]',$concecc,".round($vi,0).",'$vigusu','N','$codigoIng', '$rowi[10]', '16', 'CSF', '1')";
                                                            mysqli_query($linkbd,$sqlrPresu);
                                                            $sqlrPresu="";
                                                        }

                                                        $sq="select fechainicial from conceptoscontables_det where codigo='".$rowi[2]."' and modulo='4' and tipo='PR' and fechainicial<'$fechaf' and cuenta!='' order by fechainicial asc";
                                                        $re=mysqli_query($linkbd,$sq);
                                                        while($ro=mysqli_fetch_assoc($re)){$_POST['fechacausa']=$ro["fechainicial"];}

                                                        $sqlrc="Select * from conceptoscontables_det where estado='S' and modulo='4' AND codigo='".$rowi[2]."' and tipo='PR' and fechainicial='$_POST[fechacausa]' ORDER BY cuenta DESC";
                                                        $resc=mysqli_query($linkbd,$sqlrc);
                                                        while($rowc=mysqli_fetch_row($resc))
                                                        {
                                                            $centroCostosCeros = str_pad($rowc[5], 2, "0", STR_PAD_LEFT);

                                                            $valordeb=0;

                                                            switch ($rowc[7]) {

                                                                case 'S':

                                                                    if($rowc[3]=='N'){

                                                                        if($valorcred>0){

                                                                            $sqlr = "insert into comprobante_det (id_comp, cuenta, tercero, centrocosto, detalle, cheque, valdebito, valcredito, estado, vigencia) values ('5 $concecc', '$rowc[4]','$_POST[tercero]', '$centroCostosCeros','Reconocimiento de Intereses Predial $vig','','0','".round($valorcred,0)."','1','$_POST[vigencia]')";
                                                                            mysqli_query($linkbd, $sqlr);

                                                                        }

                                                                    }

                                                                    break;

                                                                default:

                                                                    if($rowc[3]=='N'){

                                                                        if($valorcred>0){

                                                                            $sqlr="insert into comprobante_det (id_comp, cuenta, tercero, centrocosto, detalle, cheque, valdebito, valcredito, estado, vigencia) values ('5 $concecc', '$rowc[4]','$_POST[tercero]', '$centroCostosCeros','Reconocimiento de Intereses Predial $vig','','".round($valorcred,0)."','0','1','$_POST[vigencia]')";
                                                                            mysqli_query($linkbd,$sqlr);

                                                                            $sqlr="insert into comprobante_det (id_comp,cuenta,tercero,centrocosto,detalle, cheque,valdebito,valcredito,estado,vigencia) values ('5 $concecc','$rowc[4]','$_POST[tercero]','$centroCostosCeros','Intereses Predial $vig','','".round($valordeb,0)."', '".round($valorcred,0)."','1','$_POST[vigencia]')";
                                                                            mysqli_query($linkbd,$sqlr);

                                                                            $valordeb = $valorcred;
                                                                            $sqlr = "insert into comprobante_det (id_comp,cuenta,tercero,centrocosto,detalle, cheque,valdebito,valcredito,estado,vigencia) values ('5 $concecc','$cuentacb','$_POST[tercero]','$centroCostosCeros','Intereses Predial $vig','','".round($valordeb,0)."', 0,'1','$_POST[vigencia]')";
                                                                            mysqli_query($linkbd, $sqlr);

                                                                        }

                                                                    }

                                                                    break;

                                                            }

                                                        }
                                                    }break;
                                                    //Intereses Sobretasa Bomberil
                                                    case '30':
                                                    {
                                                        $sqlrDescIntBomberil = "SELECT descuentointbomberil FROM tesoliquidapredial_desc WHERE id_predial='".$row[0]."' AND vigencia = '$vig'";
                                                        $rescDescIntBomberil = mysqli_query($linkbd, $sqlrDescIntBomberil);
                                                        $rowcDescIntBomberil = mysqli_fetch_row($rescDescIntBomberil);

                                                        $valorcred=$row[7]-$rowcDescIntBomberil[0];
                                                        $valordeb=$valorcred;
                                                        if($valorcred>0)
                                                        {
                                                            $vi=$valorcred;

                                                            $sqlrPresu="insert into pptorecibocajappto (cuenta,idrecibo,valor,vigencia,tipo,ingreso,fuente, seccion_presupuestal, medio_pago, vigencia_gasto) values('$rowi[6]',$concecc,".round($vi,0).",'$vigusu','N','$codigoIng', '$rowi[10]', '16', 'CSF', '1')";
                                                            mysqli_query($linkbd,$sqlrPresu);
                                                            $sqlrPresu="";
                                                        }

                                                        $sq="select fechainicial from conceptoscontables_det where codigo='$rowi[2]' and modulo='4' and tipo='PR' and fechainicial<'$fechaf' and cuenta!='' order by fechainicial asc";
                                                        $re=mysqli_query($linkbd,$sq);
                                                        while($ro=mysqli_fetch_assoc($re)){$_POST['fechacausa']=$ro["fechainicial"];}

                                                        $sqlrc="Select * from conceptoscontables_det where estado='S' and modulo='4' AND codigo='$rowi[2]' and tipo='PR' and fechainicial='".$_POST['fechacausa']."' order by cuenta desc";
                                                        $resc=mysqli_query($linkbd,$sqlrc);
                                                        while($rowc=mysqli_fetch_row($resc))
                                                        {
                                                            $centroCostosCeros = str_pad($rowc[5], 2, "0", STR_PAD_LEFT);

                                                            $valordeb=0;

                                                            switch ($rowc[7]) {

                                                                case 'S':

                                                                    if($rowc[3]=='N'){

                                                                        if($valorcred>0){

                                                                            $sqlr = "insert into comprobante_det (id_comp, cuenta, tercero, centrocosto, detalle, cheque, valdebito, valcredito, estado, vigencia) values ('5 $concecc', '$rowc[4]','$_POST[tercero]', '$centroCostosCeros','Reconocimiento deIntereses Sobretasa Bomberil $vig','','0','".round($valorcred,0)."','1','$_POST[vigencia]')";
                                                                            mysqli_query($linkbd, $sqlr);

                                                                        }

                                                                    }

                                                                    break;

                                                                default:

                                                                    if($rowc[3]=='N'){

                                                                        if($valorcred>0){

                                                                            $sqlr="insert into comprobante_det (id_comp, cuenta, tercero, centrocosto, detalle, cheque, valdebito, valcredito, estado, vigencia) values ('5 $concecc', '$rowc[4]','$_POST[tercero]', '$centroCostosCeros','Reconocimiento de Intereses Sobretasa Bomberil $vig','','".round($valorcred,0)."','0','1','$_POST[vigencia]')";
                                                                            mysqli_query($linkbd,$sqlr);

                                                                            $sqlr="insert into comprobante_det (id_comp,cuenta,tercero,centrocosto,detalle, cheque,valdebito,valcredito,estado,vigencia) values ('5 $concecc','$rowc[4]','$_POST[tercero]','$centroCostosCeros','Intereses Sobretasa Bomberil $vig','','".round($valordeb,0)."','".round($valorcred,0)."','1','$_POST[vigencia]')";
                                                                            mysqli_query($linkbd,$sqlr);

                                                                            $valordeb=$valorcred;
                                                                            $sqlr="insert into comprobante_det (id_comp, cuenta, tercero, centrocosto, detalle, cheque, valdebito, valcredito, estado, vigencia) values ('5 $concecc','$cuentacb','$_POST[tercero]','$centroCostosCeros','Intereses Sobretasa Bomberil $vig','','".round($valordeb,0)."',0,'1','$_POST[vigencia]')";
                                                                            mysqli_query($linkbd,$sqlr);

                                                                        }

                                                                    }

                                                                    break;

                                                            }

                                                        }
                                                    }break;
                                                    //Intereses Sobtretasa Ambiental
                                                    case '29':
                                                    {
                                                        $sqlrDescIntAmbiental = "SELECT descuentointambiental FROM tesoliquidapredial_desc WHERE id_predial='".$row[0]."' AND vigencia = '$vig'";
                                                        $rescDescIntAmbiental = mysqli_query($linkbd, $sqlrDescIntAmbiental);
                                                        $rowcDescIntAmbiental = mysqli_fetch_row($rescDescIntAmbiental);

                                                        $valorcred=$row[9]-$rowcDescIntAmbiental[0];
                                                        $valordeb=$valorcred;
                                                        if($valorcred>0)
                                                        {
                                                            $vi=$valorcred;

                                                            $sqlrPresu="insert into pptorecibocajappto (cuenta,idrecibo,valor,vigencia,tipo,ingreso,fuente, seccion_presupuestal, medio_pago, vigencia_gasto) values('$rowi[6]',$concecc,".round($vi,0).",'$vigusu','N','$codigoIng', '$rowi[10]', '16', 'CSF', '1')";
                                                            mysqli_query($linkbd,$sqlrPresu);
                                                            $sqlrPresu="";
                                                        }

                                                        $sq="select fechainicial from conceptoscontables_det where codigo='".$rowi[2]."' and modulo='4' and tipo='PR' and fechainicial<'$fechaf' and cuenta!='' order by fechainicial asc";
                                                        $re=mysqli_query($linkbd,$sq);
                                                        while($ro=mysqli_fetch_assoc($re)){$_POST['fechacausa']=$ro["fechainicial"];}

                                                        $sqlrc="Select * from conceptoscontables_det where estado='S' and modulo='4' AND codigo='$rowi[2]' and tipo='PR' and fechainicial='$_POST[fechacausa]' order by cuenta desc";
                                                        $resc=mysqli_query($linkbd,$sqlrc);
                                                        while($rowc=mysqli_fetch_row($resc))
                                                        {
                                                            $centroCostosCeros = str_pad($rowc[5], 2, "0", STR_PAD_LEFT);

                                                            $valordeb=0;

                                                            $inicioCuenta = substr($rowc[4], 0, 1);

                                                            if($inicioCuenta == '2'){

                                                                if($rowc[7] == 'S'){

                                                                    if($rowc[3]=='N'){

                                                                        if($valorcred>0){

                                                                            $sqlr="insert into comprobante_det (id_comp, cuenta, tercero, centrocosto, detalle, cheque, valdebito, valcredito, estado, vigencia) values ('5 $concecc', '".$rowc[4]."','".$_POST['tercero']."', '".$centroCostosCeros."','Intereses Sobtretasa Ambiental $vig','','0','".round($valorcred,0)."','1','".$_POST['vigencia']."')";
                                                                            mysqli_query($linkbd, $sqlr);

                                                                            $valordeb = $valorcred;

                                                                            $sqlr="insert into comprobante_det (id_comp, cuenta, tercero, centrocosto, detalle, cheque, valdebito, valcredito, estado, vigencia) values ('5 $concecc', '".$cuentacb."','".$_POST['tercero']."', '".$centroCostosCeros."','Intereses Sobtretasa Ambiental $vig','','".round($valordeb,0)."',0,'1','".$_POST['vigencia']."')";
                                                                            mysqli_query($linkbd, $sqlr);

                                                                        }
                                                                    }
                                                                }

                                                            }else{

                                                                switch ($rowc[7]) {

                                                                    case 'S':

                                                                        if($rowc[3]=='N'){

                                                                            if($valorcred>0){

                                                                                $sqlr="insert into comprobante_det (id_comp, cuenta, tercero, centrocosto, detalle, cheque, valdebito, valcredito, estado, vigencia) values ('5 $concecc', '".$rowc[4]."','".$_POST['tercero']."', '".$centroCostosCeros."','Reconocimiento de Intereses Sobtretasa Ambiental $vig','','0','".round($valorcred,0)."','1','".$_POST['vigencia']."')";
                                                                                mysqli_query($linkbd,$sqlr);

                                                                            }

                                                                        }

                                                                        break;

                                                                    default:

                                                                        if($rowc[3]=='N'){

                                                                            if($valorcred>0){

                                                                                $sqlr="insert into comprobante_det (id_comp, cuenta, tercero, centrocosto, detalle, cheque, valdebito, valcredito, estado, vigencia) values ('5 $concecc', '".$rowc[4]."','".$_POST['tercero']."', '".$centroCostosCeros."','Reconocimiento de Intereses Sobtretasa Ambiental $vig','','".round($valorcred,0)."','0','1','".$_POST['vigencia']."')";
                                                                                mysqli_query($linkbd, $sqlr);

                                                                                $sqlr="insert into comprobante_det (id_comp, cuenta, tercero, centrocosto, detalle, cheque, valdebito, valcredito, estado, vigencia) values ('5 $concecc', '".$rowc[4]."','".$_POST['tercero']."', '".$centroCostosCeros."','Intereses Sobtretasa Ambiental $vig','','".round($valordeb,0)."','".round($valorcred,0)."','1','".$_POST['vigencia']."')";
                                                                                mysqli_query($linkbd, $sqlr);

                                                                                $valordeb = $valorcred;

                                                                                $sqlr="insert into comprobante_det (id_comp, cuenta, tercero, centrocosto, detalle, cheque, valdebito, valcredito, estado, vigencia) values ('5 $concecc', '".$cuentacb."','".$_POST['tercero']."', '".$centroCostosCeros."','Intereses Sobtretasa Ambiental $vig','','".round($valordeb,0)."',0,'1','".$_POST['vigencia']."')";
                                                                                mysqli_query($linkbd, $sqlr);

                                                                            }

                                                                        }

                                                                        break;
                                                                }
                                                            }
                                                        }
                                                    }break;
                                                    //ALUMBRADO
                                                    case '34': //***
                                                    {
                                                        $siAlumbrado=0;
                                                        $sqlrDesc = "SELECT val_alumbrado FROM tesoliquidapredial_desc WHERE id_predial='$row[0]' AND vigencia=$vig ORDER BY vigencia ASC";
                                                        $resDesc = mysqli_query($linkbd,$sqlrDesc);
                                                        while ($rowDesc =mysqli_fetch_assoc($resDesc))
                                                        {
                                                            $siAlumbrado = $rowDesc['val_alumbrado'];
                                                        }
                                                        if($siAlumbrado>0)
                                                        {
                                                            $valorAlumbrado=round($siAlumbrado,0);
                                                            $valorcred=$valorAlumbrado;
                                                            if($valorcred>0)
                                                            {
                                                                $sqlrPresu="insert into pptorecibocajappto (cuenta,idrecibo,valor,vigencia,tipo,ingreso,fuente, seccion_presupuestal, medio_pago, vigencia_gasto) values('$rowi[6]',$concecc,".round($valorcred,0).",'$vigusu','N','$codigoIng', '$rowi[10]', '16', 'CSF', '1')";
                                                                mysqli_query($linkbd,$sqlrPresu);
                                                                $sqlrPresu="";
                                                            }
                                                            //$valorAlumbrado=round($row[2]*($vcobroalumbrado/1000),0);
                                                            $sq="select fechainicial from conceptoscontables_det where codigo='$rowi[2]' and modulo='4' and tipo='PR' and fechainicial<'$fechaf' and cuenta!='' order by fechainicial asc";
                                                            $re=mysqli_query($linkbd,$sq);
                                                            while($ro=mysqli_fetch_assoc($re)){$_POST['fechacausa']=$ro["fechainicial"];}

                                                            $valordeb=0;

                                                            $sqlrc="Select * from conceptoscontables_det where estado='S' and modulo='4' AND codigo='$rowi[2]' and tipo='PR' and fechainicial='".$_POST['fechacausa']."' order by cuenta desc";
                                                            $resc=mysqli_query($linkbd,$sqlrc);
                                                            while($rowc=mysqli_fetch_row($resc))
                                                            {
                                                                $centroCostosCeros = str_pad($rowc[5], 2, "0", STR_PAD_LEFT);

                                                                switch($rowc[7]){

                                                                    case 'S':

                                                                        if($rowc[3]=='N')
                                                                        {
                                                                            if($valorcred>0)
                                                                            {
                                                                                $sqlr="insert into comprobante_det (id_comp,cuenta,tercero,centrocosto,detalle, cheque,valdebito,valcredito,estado,vigencia) values ('5 $concecc','$rowc[4]','$_POST[tercero]','$centroCostosCeros','Reconocimiento de Ingreso Impuesto sobre el Servicio de Alumbrado Publico $vig','','0','".round($valorcred,0)."','1','$_POST[vigencia]')";
                                                                                mysqli_query($linkbd, $sqlr);
                                                                            }
                                                                        }

                                                                        break;

                                                                    default:

                                                                        if($rowc[3]=='N')
                                                                        {
                                                                            if($valorcred>0)
                                                                            {
                                                                                $sqlr="insert into comprobante_det (id_comp,cuenta,tercero,centrocosto,detalle, cheque,valdebito,valcredito,estado,vigencia) values ('5 $concecc','$rowc[4]','$_POST[tercero]','$centroCostosCeros','Reconocimiento de Ingreso Impuesto sobre el Servicio de Alumbrado Publico $vig','','".round($valorcred,2)."','0','1','$_POST[vigencia]')";
                                                                                mysqli_query($linkbd, $sqlr);

                                                                                $sqlr="insert into comprobante_det (id_comp,cuenta,tercero,centrocosto,detalle, cheque,valdebito,valcredito,estado,vigencia) values ('5 $concecc','$rowc[4]','$_POST[tercero]','$centroCostosCeros','Ingreso Impuesto sobre el Servicio de Alumbrado Publico $vig','','".round($valordeb,2)."','".round($valorcred,0)."','1','$_POST[vigencia]')";
                                                                                mysqli_query($linkbd, $sqlr);

                                                                                $valordeb=$valorcred;

                                                                                $sqlr="insert into comprobante_det (id_comp, cuenta, tercero, centrocosto, detalle, cheque, valdebito, valcredito, estado, vigencia) values ('5 $concecc', '$cuentacb','".$_POST['tercero']."', '$centroCostosCeros','Ingreso Impuesto sobre el Servicio de Alumbrado Publico $vig','','".round($valordeb,0)."','0','1','".$_POST['vigencia']."')";
                                                                                mysqli_query($linkbd, $sqlr);
                                                                            }
                                                                        }

                                                                        break;
                                                                }
                                                            }
                                                        }
                                                    }break;
                                                }
                                            }
                                        }
                                    }

                                    $sqlr="Select *from tesoliquidapredial_det where idpredial=$_POST[idrecaudo]";
                                    $resp=mysqli_query($linkbd,$sqlr);
                                    while($row=mysqli_fetch_row($resp))
                                    {
                                        $sqlr2="update tesoprediosavaluos set pago='S' where codigocatastral=(select codigocatastral from tesoliquidapredial WHERE idpredial=$_POST[idrecaudo]) AND vigencia=$row[1]";
                                        mysqli_query($linkbd,$sqlr2);
                                    }

                                } //fin de la verificacion
                                else
                                {
                                    echo"<script>despliegamodalm('visible','2','Ya Existe un Recibo de Caja para esta Liquidacion Predial');</script>";
                                }//***FIN DE LA VERIFICACION
                            }break; //**FIN PREDIAL***********
                            case 2: { //********** INDUSTRIA Y COMERCIO
                                $entra = true;
                                $sqlr = "SELECT count(*) FROM tesoreciboscaja WHERE id_recaudo = '".$_POST['idrecaudo']."' AND tipo = '2'";
                                $res = mysqli_query($linkbd,$sqlr);
                                while($r = mysqli_fetch_row($res)){
                                    $numerorecaudos = $r[0];
                                }
                                if($numerorecaudos >= 0){
                                    if($_POST['modorec'] == 'caja'){
                                        $cuentacb = $_POST['cuentacaja'];
                                        $cajas = $_POST['cuentacaja'];
                                        $cbancos = "";
                                    }
                                    if($_POST['modorec']=='banco'){
                                        $cuentacb = $_POST['banco'];
                                        $cajas = "";
                                        $cbancos = $_POST['banco'];
                                    }
                                    $user = $_SESSION['nickusu'];
                                    $sqlr = "INSERT INTO tesoreciboscaja (id_comp, fecha, vigencia, id_recaudo, recaudado, cuentacaja, cuentabanco, valor, estado, tipo, descripcion, usuario) VALUES (0, '$fechaf', '$vigusu', ".$_POST['idrecaudo'].", '".$_POST['modorec']."', '$cajas', '$cbancos', '".$_POST['totalc']."', 'S', '".$_POST['tiporec']."','".$_POST['concepto']."', '$user')";
                                    if (!mysqli_query($linkbd,$sqlr)){
                                        echo "
                                        <script>
                                            Swal.fire({
                                                icon: 'error',
                                                title: 'Error!',
                                                text: 'Manejador de Errores de la Clase BD, No se pudo ejecutar la petición: $sqlr',
                                                confirmButtonText: 'Continuar',
                                                confirmButtonColor: '#FF121A',
                                                timer: 2500
                                            });
                                        </script>";
                                    } else {
                                        $valorcuentabanco = 0;
                                        $concecc = $_POST['idcomp'];
                                         //*************COMPROBANTE CONTABLE INDUSTRIA
                                        $sqlr="INSERT INTO comprobante_cab (numerotipo, tipo_comp, fecha, concepto, total, total_debito,total_credito, diferencia, estado) VALUES ($concecc, 5, '$fechaf', '".$_POST['concepto']."', 0, ".$_POST['totalc'].", ".$_POST['totalc'].", 0, '1')";
                                        mysqli_query($linkbd,$sqlr);
                                        $idcomp = mysqli_insert_id($linkbd);
                                        $sqlr = "UPDATE tesoreciboscaja SET id_comp = '$idcomp' WHERE id_recaudo = '".$_POST['idrecaudo']."' AND tipo = '2'";
                                        mysqli_query($linkbd,$sqlr);
                                        if($_POST['cuotas'] == $_POST['tcuotas']){
                                            $sqlr = "UPDATE tesoindustria SET estado = 'P', pagos = '".$_POST['cuotas']."'  WHERE id_industria = '".$_POST['idrecaudo']."'";
                                        }else{
                                            $sqlr = "UPDATE tesoindustria SET pagos = '".$_POST['cuotas']."'  WHERE id_industria = '".$_POST['idrecaudo']."'";
                                        }
                                        mysqli_query($linkbd,$sqlr);

                                        for($x=0;$x<count($_POST['dcoding']);$x++){
                                            $sqlr = "SELECT ncuotas FROM tesoindustria WHERE id_industria='".$_POST['idrecaudo']."'";
                                            $res = mysqli_query($linkbd,$sqlr);
                                            $row = mysqli_fetch_row($res);
                                            $ncuotas = $row[0];
                                            $_POST['descunidos']="$row[1]$row[2]$row[3]";
                                            //***** BUSQUEDA INGRESO ********
                                            $sqlr = "SELECT * FROM tesoindustria_det WHERE id_industria='".$_POST['idrecaudo']."'";
                                            $res = mysqli_query($linkbd,$sqlr);
                                            $row = mysqli_fetch_row($res);
                                            $industria = (float)$row[1] + (float)$row[31];
                                            $avisos = $row[2];
                                            $bomberil = $row[3];
                                            $retenciones = $row[4];
                                            $sanciones = $row[5];
                                            $saldoafavor = $row[20];
                                            $intereses = $row[25];
                                            $interesesind = $row[26];
                                            $interesesavi = $row[27];
                                            $interesesbom = $row[28];
                                            $antivigact = $row[11];
                                            $antivigant = $row[10];
                                            $saldopagar = $row[8];
                                            $exoneracion = $row[18];
                                            $autoretencion = $row[19];

                                            if((float)$intereses > 0){
                                                $intetodos = (float)$interesesind + (float)$interesesavi + (float)$interesesbom;
                                                if($intetodos>0){
                                                    $indinteres = (float)$interesesind;
                                                    $aviinteres = (float)$interesesavi;
                                                    $bominteres = (float)$interesesbom;
                                                } else {
                                                    $indinteres = (float)$intereses;
                                                    $aviinteres = 0;
                                                    $bominteres = 0;
                                                }
                                            }
                                            if(($row[21]>0)|| ($row[13]>0)){//descuentos
                                                if(($row[22]+$row[23]+$row[24])>0){
                                                    $descuenindus = $row[22];//descuento industria
                                                    $descuenaviso = $row[23];//descuento avisos
                                                    $descuenbombe = $row[24];//descuento bomberil
                                                } else {
                                                    if(substr($_POST['descunidos'], -3, 1)=='S'){//descuento industria
                                                        $descuenindus = ($row[1] + $row[31])*($row[13]/100);
                                                    } else {
                                                        $descuenindus = 0;
                                                    }
                                                    if(substr($_POST['descunidos'], -2, 1)=='S'){//descuento avisos
                                                        $descuenaviso = $row[2]*($row[13]/100);
                                                    } else {
                                                        $descuenaviso = 0;
                                                    }
                                                    if(substr($_POST['descunidos'], -1, 1)=='S'){//descuento bomberil
                                                        $descuenbombe = $row[3]*($row[13]/100);
                                                    } else {
                                                        $descuenbombe = 0;
                                                    }
                                                }
                                            }
                                            /*if(((float)$row[21]+(float)$row[22]+(float)$row[23])>0){
                                                $descuenindus = $row[21];//descuento industria
                                                $descuenaviso = $row[22];//descuento avisos
                                                $descuenbombe = $row[23];//descuento bomberil
                                            } elseif($row[13]>0) {
                                                if(substr($_POST['descunidos'], -3, 1)=='S'){//descuento industria
                                                    $descuenindus = $row[1]*($row[13]/100);
                                                } else {
                                                    $descuenindus = 0;
                                                }
                                                if(substr($_POST['descunidos'], -2, 1)=='S'){//descuento avisos
                                                    $descuenaviso = $row[2]*($row[13]/100);
                                                } else {
                                                    $descuenaviso=0;
                                                }
                                                if(substr($_POST['descunidos'], -1, 1)=='S'){//descuento bomberil
                                                    $descuenbombe = $row[3]*($row[13]/100);
                                                }else{
                                                    $descuenbombe = 0;
                                                }
                                            }*/

                                            $totalantivigact = $antivigact - $retenciones - $antivigant - $saldoafavor - $exoneracion - $autoretencion;

                                            if($totalantivigact < 0){
                                                $totalica = $industria - $descuenindus + $totalantivigact;
                                            }else {
                                                $totalica = $industria - $descuenindus;
                                            }
                                            if ($totalica < 0){
                                                $totalsanciones = $sanciones + $totalica;
                                            } else {
                                                $totalsanciones = $sanciones;
                                            }
                                            if ($totalsanciones < 0){
                                                $totalavisos = $avisos + $interesesavi - $descuenaviso + $totalsanciones;
                                            } else {
                                                $totalavisos = $avisos + $interesesavi - $descuenaviso;
                                            }
                                            if($totalavisos < 0){
                                                $totalbombe = $bomberil + $interesesbom - $descuenbombe + $totalavisos;
                                            } else {
                                                $totalbombe = $bomberil + $interesesbom - $descuenbombe;
                                            }

                                            if($ncuotas > 1){
                                                $totalica = $totalica/$ncuotas;
                                                $totalsanciones = $totalsanciones/$ncuotas;
                                                $totalbombe = $totalbombe/$ncuotas;
                                                $totalavisos = $totalavisos/$ncuotas;
                                            }
                                            $codingreso = $_POST['dcoding'][$x];
                                            $codtercero = $_POST['tercero'];
                                            $codanoliqu = $_POST['ageliquida'];
                                            $codvigenci = $_POST['vigencia'];

                                            $_POST['cuentabanco'] = '';
                                            $sql_cuen ="SELECT cuenta FROM tesobancosctas WHERE ncuentaban = '".$_POST['cb']."' AND estado='S' ";
                                            $res_cuen = mysqli_query($linkbd,$sql_cuen);
                                            $row_cuen = mysqli_fetch_row($res_cuen);
                                            $_POST['cuentabanco'] = $row_cuen[0];

                                            $sqlri = "SELECT concepto, cuentapres, fuente FROM tesoingresos_ica_det WHERE (codigo = '$codingreso' OR codigo = '04') AND vigencia = (SELECT MAX(vigencia) FROM tesoingresos_ica_det WHERE codigo = '$codingreso' OR codigo = '04') ORDER BY concepto";
                                            $res = mysqli_query($linkbd,$sqlri);
                                            while($row = mysqli_fetch_row($res)){
                                                switch($row[0]){
                                                    case '00': {//*****Sanciones
                                                        if(($totalsanciones > 0) || ($_POST['valorecaudo'] == 0)){
                                                            $sqlr2 = "SELECT * FROM conceptoscontables_det WHERE estado='S' AND modulo='4' AND codigo='00' AND tipo='C' AND tipocuenta='N' AND debito='S' AND fechainicial=(SELECT MAX(fechainicial) FROM conceptoscontables_det WHERE estado='S' AND modulo='4' AND codigo='00' AND tipo='C' AND tipocuenta='N' AND debito='S' AND fechainicial<='$fechaf')";
                                                            $res2 = mysqli_query($linkbd,$sqlr2);
                                                            while($row2 = mysqli_fetch_row($res2)){
                                                                if($numcc==''){
                                                                    $numcc = $row2[5];
                                                                }
                                                                if($row2[3]=='N'){
                                                                    if($row2[6]=='S'){
                                                                        $valordeb = 0;
                                                                        $valorcred = $totalsanciones;
                                                                        $sqlr = "INSERT INTO comprobante_det (id_comp, cuenta, tercero, centrocosto,detalle, cheque, valdebito, valcredito, estado, vigencia) VALUES ('5 $concecc', '$row2[4]', '".$_POST['tercero']."', '$row2[5]', 'Sanciones ICA ".$_POST['ageliquida']."', '', '0', '$valorcred', '1', '".$_POST['vigencia']."')";
                                                                        mysqli_query($linkbd,$sqlr);
                                                                        if($_POST['cuentabanco']==$row2[4]){
                                                                            $valorcuentabanco = $valorcuentabanco + (0-$valorcred);
                                                                        }
                                                                        //********** CAJA O BANCO
                                                                        $valordeb = $totalsanciones;
                                                                        $cuentacbr = $cuentacb;
                                                                        $sqlr = "INSERT INTO comprobante_det (id_comp,cuenta,tercero,centrocosto, detalle,cheque,valdebito,valcredito,estado,vigencia) VALUES ('5 $concecc','$cuentacb','$_POST[tercero]','$row2[5]','Sanciones ICA $_POST[modorec]','', '$valordeb','0','1','$_POST[vigencia]')";
                                                                        mysqli_query($linkbd,$sqlr);
                                                                        if($_POST['cuentabanco']==$cuentacbr){
                                                                            $valorcuentabanco = $valorcuentabanco + ($valordeb-0);
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                            $sqlt = "INSERT INTO pptorecibocajappto (cuenta, idrecibo, valor, vigencia, tipo,ingreso, fuente, seccion_presupuestal, medio_pago, vigencia_gasto) VALUES ('$row[1]', '$concecc','$sanciones','$vigusu', 'N', '00', '$row[2]', '16', 'CSF', '1')";
                                                            mysqli_query($linkbd, $sqlt);
                                                        }
                                                    }break;
                                                    case '04': {//*****industria
                                                        if (($totalica > 0) || ($_POST['valorecaudo'] == 0) ){
                                                            if($_POST['valorecaudo'] == 0){
                                                                $totalica = 0;
                                                            }
                                                            $sqlr2 = "SELECT * FROM conceptoscontables_det WHERE estado='S' AND modulo='4' AND codigo='04' AND tipo='C' AND fechainicial = (SELECT MAX(fechainicial) FROM conceptoscontables_det WHERE estado='S' AND modulo='4' AND codigo='04' AND tipo='C' AND fechainicial<='$fechaf')";
                                                            $res2 = mysqli_query($linkbd,$sqlr2);
                                                            while($row2 = mysqli_fetch_row($res2)){
                                                                if($row2[3]=='N'){
                                                                    if($row2[6]=='S'){
                                                                        $valorcred = $totalica;
                                                                        $cuentaica = $row2[4];
                                                                        $cuentaindustria = $row2[4];
                                                                        $sqlr = "INSERT INTO comprobante_det (id_comp, cuenta, tercero,centrocosto, detalle, cheque, valdebito, valcredito, estado, vigencia) VALUES ('5 $concecc', '$row2[4]', '$codtercero', '$row2[5]', 'Industria y Comercio $codanoliqu', '', 0, '$valorcred', '1', '$codvigenci')";
                                                                        mysqli_query($linkbd,$sqlr);
                                                                        if($_POST['cuentabanco']==$row2[4]){
                                                                            $valorcuentabanco = $valorcuentabanco + (0-$valorcred);
                                                                        }
                                                                        //********** CAJA O BANCO
                                                                        $valordeb =  $totalica;
                                                                        $cuentacbr = $cuentacb;
                                                                        $sqlr = "INSERT INTO comprobante_det (id_comp,cuenta,tercero,centrocosto,detalle, cheque, valdebito,valcredito,estado,vigencia) VALUES ('5 $concecc','$cuentacbr','$_POST[tercero]','$row2[5]','Industria y Comercio $_POST[modorec]','','$valordeb','0', '1','$_POST[vigencia]')";
                                                                        mysqli_query($linkbd,$sqlr);
                                                                        if($_POST['cuentabanco']==$cuentacbr){
                                                                            $valorcuentabanco = $valorcuentabanco + ($valordeb-0);
                                                                        }

                                                                        $sqlr = "INSERT INTO pptorecibocajappto (cuenta,idrecibo,valor,vigencia,tipo, ingreso,fuente, seccion_presupuestal, medio_pago, vigencia_gasto) VALUES ('$row[1]','$concecc','$industria','$vigusu','N','04', '$row[2]', '16', 'CSF', '1')";
                                                                        mysqli_query($linkbd,$sqlr);
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }break;
                                                    case '05':{//************avisos
                                                        if(($totalavisos > 0) && ($_POST['valorecaudo'] != 0)){
                                                            $sqlr2="SELECT * FROM conceptoscontables_det WHERE estado='S' AND modulo='4' AND codigo='05' AND tipo='C' AND fechainicial=(SELECT MAX(fechainicial) FROM conceptoscontables_det WHERE estado='S' AND modulo='4' AND codigo='05' AND tipo='C' AND fechainicial<='$fechaf')";
                                                            $res2 = mysqli_query($linkbd,$sqlr2);
                                                            while($row2 = mysqli_fetch_row($res2)){
                                                                if($row2[3]=='N'){
                                                                    if($row2[6]=='S'){
                                                                        $valordeb = 0;
                                                                        $valorcred = $totalavisos;
                                                                        $sqlr = "INSERT INTO comprobante_det (id_comp, cuenta, tercero,centrocosto, detalle, cheque, valdebito, valcredito, estado, vigencia) VALUES ('5 $concecc','$row2[4]','$_POST[tercero]','$row2[5]','Avisos y Tableros $_POST[ageliquida]','','0', '$valorcred','1', '$_POST[vigencia]')";
                                                                        mysqli_query($linkbd,$sqlr);
                                                                        if($_POST['cuentabanco']==$row2[4]){
                                                                            $valorcuentabanco = $valorcuentabanco + (0-$valorcred);
                                                                        }
                                                                        //********** CAJA O BANCO
                                                                        $valordeb = $totalavisos;
                                                                        $cuentacbr = $cuentacb;
                                                                        $sqlr = "INSERT INTO comprobante_det (id_comp,cuenta,tercero,centrocosto, detalle,cheque,valdebito,valcredito,estado,vigencia) VALUES ('5 $concecc','$cuentacb','$_POST[tercero]','$row2[5]','Avisos y Tableros $_POST[modorec]','', '$valordeb','0','1','$_POST[vigencia]')";
                                                                        mysqli_query($linkbd,$sqlr);
                                                                        if($_POST['cuentabanco']==$cuentacbr){
                                                                            $valorcuentabanco = $valorcuentabanco + ($valordeb-0);
                                                                        }

                                                                        $sqlr="INSERT INTO pptorecibocajappto (cuenta,idrecibo,valor,vigencia, tipo,ingreso,fuente, seccion_presupuestal, medio_pago, vigencia_gasto) VALUES ('$row[1]',$concecc,$avisos, '$vigusu','N','05', '$row[2]', '16', 'CSF', '1')";
                                                                        mysqli_query($linkbd,$sqlr);
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }break;
                                                     case '06':{//*********bomberil ********
                                                        if(($totalbombe > 0) && ($_POST['valorecaudo'] != 0)){
                                                            $sqlr2 = "SELECT * FROM conceptoscontables_det WHERE estado='S' AND modulo='4' AND codigo='06' AND tipo='C' AND fechainicial=(SELECT MAX(fechainicial) FROM conceptoscontables_det WHERE estado='S' AND modulo='4' AND codigo='06' AND tipo='C' AND fechainicial<='$fechaf')";
                                                            $res2 = mysqli_query($linkbd,$sqlr2);
                                                            while($row2=mysqli_fetch_row($res2)){
                                                                if($row2[3]=='N'){
                                                                    if($row2[6]=='S'){
                                                                        $valordeb = 0;
                                                                        $valorcred = $totalbombe;
                                                                        $sqlr="INSERT INTO comprobante_det (id_comp,cuenta,tercero,centrocosto, detalle,cheque,valdebito,valcredito,estado,vigencia) VALUES ('5 $concecc','$row2[4]','$_POST[tercero]','$row2[5]','Bomberil $_POST[ageliquida]', '','$valordeb', '$valorcred','1', '$_POST[vigencia]')";
                                                                        mysqli_query($linkbd,$sqlr);
                                                                        if($_POST['cuentabanco']==$row2[4]){
                                                                            $valorcuentabanco = $valorcuentabanco + (0-$valorcred);
                                                                        }
                                                                        //********** CAJA O BANCO
                                                                        $valordeb = $totalbombe;
                                                                        $cuentacbr = $cuentacb;
                                                                        $sqlr = "INSERT INTO comprobante_det (id_comp,cuenta,tercero,centrocosto, detalle,cheque,valdebito,valcredito, estado,vigencia) VALUES ('5 $concecc','$cuentacb','$_POST[tercero]','$row2[5]','Bomberil $_POST[modorec]', '','$valordeb','0', '1','$_POST[vigencia]' )";
                                                                        mysqli_query($linkbd,$sqlr);
                                                                        if($_POST['cuentabanco']==$cuentacbr){
                                                                            $valorcuentabanco = $valorcuentabanco + ($valordeb-0);
                                                                        }

                                                                        $sqlr = "INSERT INTO pptorecibocajappto (cuenta,idrecibo,valor,vigencia, tipo,ingreso,fuente, seccion_presupuestal, medio_pago, vigencia_gasto) values('$row[1]','$concecc','$bomberil','$vigusu','N','06', '$row[2]', '16', 'CSF', '1')";
                                                                        mysqli_query($linkbd,$sqlr);
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }break;
                                                    case 'P12':{//Anticipo vigencia Actual
                                                        if(($totalantivigact > 0) && ($_POST['valorecaudo'] != 0)){
                                                            $sqlr2 = "SELECT * FROM conceptoscontables_det WHERE estado='S' AND modulo='4' AND codigo='P12' AND tipo='C' AND fechainicial=(SELECT MAX(fechainicial) FROM conceptoscontables_det WHERE estado='S' AND modulo='4' AND codigo='P12' AND tipo='C' AND fechainicial<='$fechaf')";
                                                            $res2 = mysqli_query($linkbd,$sqlr2);
                                                            while($row2 = mysqli_fetch_row($res2)){
                                                                if($row2[3]=='N'){
                                                                    if($row2[6]=='N'){
                                                                        $valorcred = $totalantivigact;
                                                                        $cuentaica = $row2[4];
                                                                        $sqlr = "INSERT INTO comprobante_det (id_comp,cuenta,tercero,centrocosto,detalle, cheque,valdebito,valcredito,estado,vigencia) VALUES ('5 $concecc','$cuentaica','$_POST[tercero]','$row2[5]','Anticipo vigencia Actual $_POST[ageliquida]','',0, '$valorcred','1', '$_POST[vigencia]')";
                                                                        mysqli_query($linkbd,$sqlr);
                                                                        if($_POST['cuentabanco']==$cuentaica){
                                                                            $valorcuentabanco = $valorcuentabanco + (0-$valorcred);
                                                                        }
                                                                        //********** CAJA O BANCO
                                                                        $valordeb = $totalantivigact;
                                                                        $cuentacbr = $cuentacb;
                                                                        $sqlr="insert into comprobante_det (id_comp,cuenta,tercero,centrocosto, detalle,cheque,valdebito,valcredito,estado,vigencia) values ('5 $concecc','$cuentacb','$_POST[tercero]','$row2[5]','Anticipo vigencia Actual $_POST[modorec]','', '$valordeb','0','1','$_POST[vigencia]')";
                                                                        mysqli_query($linkbd,$sqlr);
                                                                        if($_POST['cuentabanco']==$cuentacbr){
                                                                            $valorcuentabanco = $valorcuentabanco + ($valordeb-0);
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }break;
                                                    case 'P16':{//*****INTERESES INDUSTRIA
                                                        if(($indinteres > 0) && ($_POST['valorecaudo'] != 0)){
                                                            $sqlr2 = "SELECT * FROM conceptoscontables_det WHERE estado='S' AND modulo='4' AND codigo='P16' AND tipo='C' AND debito='S' AND fechainicial=(SELECT MAX(fechainicial) FROM conceptoscontables_det WHERE estado='S' AND modulo='4' AND codigo='P16' AND tipo='C' AND debito='S' AND fechainicial<='$fechaf')";
                                                            $res2 = mysqli_query($linkbd,$sqlr2);
                                                            while($row2 = mysqli_fetch_row($res2)){
                                                                if($row2[3]=='N'){
                                                                    $valordeb = 0;
                                                                    $valorcred = $indinteres;
                                                                    $cuentacbr = $cuentacb;
                                                                    $sqlr = "insert into comprobante_det (id_comp, cuenta, tercero, centrocosto, detalle, cheque, valdebito, valcredito, estado, vigencia) values ('5 $concecc', '$row2[4]','".$_POST['tercero']."', '$row2[5]','Intereses Industria y Comercio ".$_POST['ageliquida']."','','$valordeb','$valorcred','1', '".$_POST['vigencia']."')";
                                                                    mysqli_query($linkbd,$sqlr);
                                                                    $valorcred = 0;
                                                                    $valordeb = $indinteres;
                                                                    $sqlr = "INSERT INTO comprobante_det (id_comp, cuenta, tercero, centrocosto, detalle, cheque, valdebito, valcredito, estado, vigencia) VALUES ('5 $concecc','$cuentacbr','".$_POST['tercero']."', '$row2[5]','Intereses Industria y Comercio ".$_POST['ageliquida']."','','$valordeb','$valorcred','1', '".$_POST['vigencia']."')";
                                                                    mysqli_query($linkbd,$sqlr);
                                                                    if($_POST['cuentabanco']==$cuentacbr){
                                                                        $valorcuentabanco = $valorcuentabanco + ($valordeb-0);
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }break;
                                                }
                                            }
                                            //**************Ajuste de redondeo******************
                                            $diferenciatotal = ($_POST['valorecaudo']/$ncuotas) - $valorcuentabanco;
                                            if($diferenciatotal!=0){
                                                $sqlr = "SELECT valor_inicial from dominios where nombre_dominio='CUENTA_MILES'";
                                                $res = mysqli_query($linkbd,$sqlr);
                                                while ($row = mysqli_fetch_row($res)){
                                                    $cuentaredondeos = $row[0];
                                                }
                                                if($diferenciatotal>0){
                                                    $sqlr = "INSERT INTO comprobante_det (id_comp,cuenta,tercero,centrocosto,detalle, cheque, valdebito,valcredito,estado,vigencia) VALUES ('5 $concecc','".$_POST['cuentabanco']."','".$_POST['tercero']."','$numcc','AJUSTE DE REDONDEO','0',$diferenciatotal,'0','1', '".$_POST['vigencia']."')";
                                                    mysqli_query($linkbd,$sqlr);
                                                    $sqlr = "INSERT INTO comprobante_det (id_comp,cuenta,tercero,centrocosto,detalle, cheque, valdebito,valcredito,estado,vigencia) VALUES ('5 $concecc','$cuentaredondeos','".$_POST['tercero']."','$numcc','AJUSTE DE REDONDEO','0','0',$diferenciatotal,'1', '".$_POST['vigencia']."')";
                                                    mysqli_query($linkbd,$sqlr);
                                                } else {
                                                    $sqlr = "INSERT INTO comprobante_det (id_comp,cuenta,tercero,centrocosto,detalle, cheque, valdebito,valcredito,estado,vigencia) VALUES ('5 $concecc','".$_POST['cuentabanco']."','".$_POST['tercero']."','$numcc','AJUSTE DE REDONDEO','0','0',".abs($diferenciatotal).",'1', '".$_POST['vigencia']."')";
                                                    mysqli_query($linkbd,$sqlr);
                                                    $sqlr = "INSERT INTO comprobante_det (id_comp,cuenta,tercero,centrocosto,detalle, cheque, valdebito,valcredito,estado,vigencia) VALUES ('5 $concecc','$cuentaredondeos','".$_POST['tercero']."','$numcc','AJUSTE DE REDONDEO','0',".abs($diferenciatotal).",'0','1', '".$_POST['vigencia']."')";
                                                    mysqli_query($linkbd,$sqlr);
                                                }
                                            }
                                            echo "
                                            <script>
                                                Swal.fire({
                                                    icon: 'success',
                                                    title: 'Se ha almacenado el Recibo de Caja con Exito',
                                                    showConfirmButton: true,
                                                    confirmButtonText: 'Continuar',
                                                    confirmButtonColor: '#01CC42',
                                                    timer: 3500
                                                }).then((response) => {
                                                    location.href='teso-recibocajaver.php?idrecibo=$concecc';
                                                });
                                            </script>";
                                        }
                                    }
                                }
                                else
                                {
                                    echo"
                                    <script>
                                        Swal.fire({
                                            icon: 'success',
                                            title: 'Ya Existe un Recibo de Caja para esta Liquidacion',
                                            showConfirmButton: true,
                                            confirmButtonText: 'Continuar',
                                            confirmButtonColor: '#01CC42',
                                            timer: 3500
                                        });
                                    </script>";
                                }
                            }break;
                            case 3: //**************OTROS RECAUDOS
                            {
                                $sqlr="select count(*) from tesoreciboscaja where id_recaudo=$_POST[idrecaudo] and tipo='3' AND ESTADO='S'";
                                $res=mysqli_query($linkbd,$sqlr);
                                while($r=mysqli_fetch_row($res)){$numerorecaudos=$r[0];}
                                if($numerorecaudos==0)
                                {
                                    //*********************CREACION DEL COMPROBANTE CONTABLE ***************************
                                    //***busca el consecutivo del comprobante contable
                                    $concecc=0;
                                    $sqlr="select max(id_recibos ) from tesoreciboscaja  ";
                                    $res=mysqli_query($linkbd,$sqlr);
                                    while($r=mysqli_fetch_row($res)){$concecc=$r[0];}
                                    $concecc+=1;
                                    // $consec=$concecc;
                                    //***cabecera comprobante
                                    $sqlr="insert into comprobante_cab (numerotipo,tipo_comp,fecha,concepto,total,total_debito,total_credito,diferencia,estado) values ($concecc,5,'$fechaf','$_POST[concepto]',0,$_POST[totalc],$_POST[totalc],0,'1')";
                                    mysqli_query($linkbd,$sqlr);
                                    $idcomp=mysqli_insert_id($linkbd);
                                    echo "<input type='hidden' name='ncomp' value='$idcomp'>";

                                    //******************* DETALLE DEL COMPROBANTE CONTABLE *********************
                                    for($x=0;$x<count($_POST['dcoding']);$x++)
                                    {
                                        //***** BUSQUEDA INGRESO ********
                                        $sqlri="Select * from tesoingresos_det where codigo='".$_POST['dcoding'][$x]."' and vigencia=(SELECT MAX(vigencia) FROM tesoingresos_det WHERE codigo = '".$_POST['dcoding'][$x]."')";//echo "$sqlri <br>";
                                        $resi=mysqli_query($linkbd,$sqlri);
                                        while($rowi=mysqli_fetch_row($resi))
                                        {
                                            $porce=$rowi[5];
                                            if($rowi[6]!="")
                                            {
                                                $vi=$_POST['dvalores'][$x]*($porce/100);
                                                //****creacion documento presupuesto ingresos
                                                $sql="SELECT terceros FROM tesoingresos WHERE codigo=".$_POST['dcoding'][$x] ;
                                                $res=mysqli_query($linkbd,$sql);
                                                $row= mysqli_fetch_row($res);

                                                $sqlrFuente = "SELECT fuente FROM tesoingresos_fuentes WHERE codigo = '".$_POST['dcoding'][$x]."' AND estado = 'S'";
                                                $respFuente = mysqli_query($linkbd, $sqlrFuente);
                                                $rowFuente = mysqli_fetch_row($respFuente);

                                                $tipoIng = $row[0] == 'R' ? 'R' : 'N';

                                                $sqlr="insert into pptorecibocajappto (cuenta,idrecibo,valor,vigencia,tipo,ingreso,fuente, productoservicio, seccion_presupuestal, medio_pago, vigencia_gasto) values('$rowi[6]',$concecc,$vi,'".$vigusu."','".$tipoIng."','".$_POST['dcoding'][$x]."', '$rowFuente[0]', '$rowi[7]', '16', 'CSF', '1')";//echo "$sqlr";
                                                mysqli_query($linkbd,$sqlr);
                                            }
                                            //**** busqueda cuenta presupuestal*****
                                            //busqueda concepto contable
                                            $sq="select fechainicial from conceptoscontables_det where codigo='$rowi[2]' and modulo='4' and tipo='C' and fechainicial<'$fechaf' and cuenta!='' order by fechainicial asc";
                                            $re=mysqli_query($linkbd,$sq);
                                            while($ro=mysqli_fetch_assoc($re))
                                            {
                                                $_POST['fechacausa']=$ro["fechainicial"];
                                            }
                                            $sqlrc="Select * from conceptoscontables_det where estado='S' and modulo='4' AND codigo='$rowi[2]' and tipo='C' and fechainicial='".$_POST['fechacausa']."'";
                                            $resc=mysqli_query($linkbd,$sqlrc);
                                            while($rowc=mysqli_fetch_row($resc))
                                            {

                                                if($_POST['dcoding'][$x]==$_POST['cobrorecibo'])
                                                {
                                                    if($rowc[7]=='S'){$columna= $rowc[7];}
                                                    else{$columna= 'N';}
                                                    $cuentacont=$rowc[4];
                                                }
                                                else
                                                {
                                                    $columna= $rowc[6];
                                                    $cuentacont=$rowc[4];
                                                }
                                                if($columna=='S')
                                                {
                                                    $valorcred=$_POST['dvalores'][$x]*($porce/100);
                                                    $valordeb=0;
                                                    if($rowc[3]=='N')
                                                    {
                                                        //*****inserta del concepto contable
                                                        //***********MODIFICAR CUENTA PPTAL DE INGRESO AGREGARLE EL RECAUDO *********
                                                        $sqlrpto="Select * from pptocuentas where estado='S' and cuenta='$rowi[6]' and vigencia='$vigusu'";
                                                        $respto=mysqli_query($linkbd,$sqlrpto);
                                                        $rowpto=mysqli_fetch_row($respto);

                                                        $sqlr="update pptocuentaspptoinicial  set ingresos=ingresos+".$vi." where cuenta ='".$rowi[6]."' and vigencia='$vigusu'";
                                                        mysqli_query($linkbd,$sqlr);

                                                        //************ FIN MODIFICACION PPTAL
                                                        $sqlr="insert into comprobante_det (id_comp,cuenta,tercero,centrocosto,detalle,cheque, valdebito,valcredito, estado,vigencia) values ('5 $concecc','".$cuentacont."','".$_POST['tercero']."','".$rowc[5]."','Ingreso ".strtoupper($_POST['dncoding'][$x])."','',".$valordeb.",".$valorcred.",'1','".$_POST['vigencia']."')";
                                                        mysqli_query($linkbd,$sqlr);
                                                        //***cuenta caja o banco
                                                        if($_POST['modorec']=='caja')
                                                        {
                                                            $cuentacb=$_POST['cuentacaja'];
                                                            $cajas=$_POST['cuentacaja'];
                                                            $cbancos="";
                                                        }
                                                        if($_POST['modorec']=='banco')
                                                        {
                                                            $cuentacb=$_POST['banco'];
                                                            $cajas="";
                                                            $cbancos=$_POST['banco'];
                                                        }
                                                        //$valordeb=$_POST[dvalores][$x]*($porce/100);
                                                        //$valorcred=0;
                                                        $sqlr="insert into comprobante_det (id_comp,cuenta,tercero,centrocosto,detalle,cheque, valdebito,valcredito,estado,vigencia) values ('5 $concecc','".$cuentacb."','".$_POST['tercero']."','".$rowc[5]."','Ingreso ".strtoupper($_POST['dncoding'][$x])."','',".$valorcred.",0,'1','".$_POST['vigencia']."')";
                                                        mysqli_query($linkbd,$sqlr);
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    $user=$_SESSION['nickusu'];
                                    //************ insercion de cabecera recaudos ************
                                    $sqlr="insert into tesoreciboscaja (id_comp,fecha,vigencia,id_recaudo,recaudado,cuentacaja,cuentabanco,valor, estado,tipo, descripcion,usuario,tipo_mov) values($idcomp,'$fechaf','$vigusu',$_POST[idrecaudo],'$_POST[modorec]','$cajas','$cbancos', '$_POST[totalc]', 'S','$_POST[tiporec]', '$_POST[concepto]','$user', '101')";
                                    if (!mysqli_query($linkbd,$sqlr))
                                    {
                                        $e =mysqli_error($linkbd);
                                        echo"<script>despliegamodalm('visible','2','No se pudo ejecutar la peticion: $e');</script>";
                                    }
                                    else
                                    {
                                        $sqlr="update tesorecaudos set estado='P' WHERE ID_RECAUDO=$_POST[idrecaudo]";
                                        mysqli_query($linkbd,$sqlr);
                                        echo"<script>despliegamodalm('visible','1','Se ha almacenado el Recibo de Caja con Exito');</script>";
                                    }
                                } //fin de la verificacion
                                else { echo"<script>despliegamodalm('visible','2','Ya Existe un Recibo de Caja para esta Liquidación');</script>";}
                            }break;
                            //********************* INDUSTRIA Y COMERCIO
                        } //*****fin del switch
                        $_POST['ncomp']=$concecc;
                        //******* GUARDAR DETALLE DEL RECIBO DE CAJA ******
                        for($x=0;$x<count($_POST['dcoding']);$x++)
                        {
                            $sqlr="insert into tesoreciboscaja_det (id_recibos,ingreso,valor,estado) values($concecc,'".$_POST['dcoding'][$x]."',".$_POST['dvalores'][$x].",'S')";
                            mysqli_query($linkbd,$sqlr);
                        }
                        //***** FIN DETALLE RECIBO DE CAJA ***************
                        if($_POST['notaf']!='')
                        {
                            $date=getdate();
                            $h=$date["hours"];
                            $min=$date["minutes"];
                            $hora=$h.":".$min;
                            $sqlr="insert into teso_notasrevelaciones (hora,fecha,usuario,modulo,tipo_documento,numero_documento,valor_total_transaccion,nota) values('$hora','$fechaf','".$_SESSION["usuario"]."','teso','5','$concecc','".$_POST['dvalores'][$x]."','$_POST[notaf]')";
                            mysqli_query($linkbd,$sqlr);
                        }
                    }
                    else {echo"<script>despliegamodalm('visible','2','No Tiene los Permisos para Modificar este Documento');</script>";}
                    //****fin if bloqueo
                }//**fin del oculto
            ?>
            <div id="bgventanamodal2">
                <div id="ventanamodal2">
                    <IFRAME  src="" name="buster" marginWidth=0 marginHeight=0 frameBorder=0 id="ventana2" frameSpacing=0 style="left:500px; width:900px; height:500px; top:200;">
                    </IFRAME>
                </div>
            </div>
        </form>
    </body>
</html>
