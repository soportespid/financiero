<?php
	header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=utf8");
	require 'comun.inc';
	require 'funciones.inc';
	session_start();
	$linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");
	cargarcodigopag($_GET['codpag'],$_SESSION["nivel"]);
	date_default_timezone_set("America/Bogota");
?>
<!DOCTYPE >
<html lang=es>
	<head>
		<meta charset="utf-8"/>
		<meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1"/>
		<meta name="viewport" content="user-scalable=no">
		<title>:: IDEAL 10 - Presupuesto CCPET</title>
		<link href="favicon.ico" rel="shortcut icon"/>
		<link href="css/css2z.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
		<link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
		<link href="css/css4.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
        <link href="css/tabs.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css"/>
        <link href="css/style.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css"/>
        <link rel="stylesheet" href="sweetalert2/dist/sweetalert2.min.css">
        <script src="sweetalert2/dist/sweetalert2.min.js"></script>
		<script src="vue/vue.min.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script src="css/programas.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script src="css/calendario.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
        <script>

        </script>
	</head>
	<body>
		<header>
			<table>
				<tr><script>barra_imagenes("ccpet");</script><?php cuadro_titulos();?></tr>
			</table>
		</header>

		<form name="form2" method="post" action="">
			<section id="myapp" v-cloak >
				<nav>
					<table>
						<tr><?php menu_desplegable("ccpet");?></tr>
						<!-- <tr>
							<td colspan="3" class="cinta">
								<img src="imagenes/add.png" v-on:click="location.href=''" class="mgbt" title="Nuevo">
								<img src="imagenes/guarda.png" title="Guardar" v-on:click="" class="mgbt">
								<img src="imagenes/busca.png" v-on:click="location.href=''" class="mgbt" title="Buscar">
								<img src="imagenes/nv.png" onClick="mypop=window.open('ccp-principal.php','',''); mypop.focus();" class="mgbt" title="Nueva Ventana">
                                <img src="imagenes/excel.png" title="Excel" class="mgbt" v-on:click="excel">
							</td>
						</tr> -->
					</table>
                    <div class="bg-white group-btn p-1" id="newNavStyle">
                        <button type="button" class="btn btn-white btn-primary-hover d-flex justify-between align-items-center" @click="window.location.href='ccp-informeCDP.php'">
                            <span>Nuevo</span>
                            <svg  xmlns="http://www.w3.org/2000/svg"  viewBox="0 -960 960 960" ><path d="M440-440H200v-80h240v-240h80v240h240v80H520v240h-80v-240Z"/></svg>
                        </button>
                        <!-- <button type="button" class="btn btn-white btn-primary-hover d-flex justify-between align-items-center" v-on:Click="guardarCdp">
                            <span>Guardar</span>
                            <svg xmlns="http://www.w3.org/2000/svg"  viewBox="0 -960 960 960" ><path d="M840-680v480q0 33-23.5 56.5T760-120H200q-33 0-56.5-23.5T120-200v-560q0-33 23.5-56.5T200-840h480l160 160Zm-80 34L646-760H200v560h560v-446ZM480-240q50 0 85-35t35-85q0-50-35-85t-85-35q-50 0-85 35t-35 85q0 50 35 85t85 35ZM240-560h360v-160H240v160Zm-40-86v446-560 114Z"/></svg>
                        </button> -->
                        <button type="button" class="btn btn-white btn-primary-hover d-flex justify-between align-items-center" @click="window.location.href='ccp-informeCDP.php'">
                            <span>Buscar</span>
                            <svg class="fill-black group-hover:fill-white w-5 h-5" xmlns="http://www.w3.org/2000/svg"  viewBox="0 -960 960 960" ><path d="M784-120 532-372q-30 24-69 38t-83 14q-109 0-184.5-75.5T120-580q0-109 75.5-184.5T380-840q109 0 184.5 75.5T640-580q0 44-14 83t-38 69l252 252-56 56ZM380-400q75 0 127.5-52.5T560-580q0-75-52.5-127.5T380-760q-75 0-127.5 52.5T200-580q0 75 52.5 127.5T380-400Z"/></svg>
                        </button>
                        <button type="button" class="btn btn-white btn-primary-hover d-flex justify-between align-items-center" @click="mypop=window.open('ccp-principal.php','',''); mypop.focus();">
                            <span>Nueva ventana</span>
                            <svg class="fill-black group-hover:fill-white w-5 h-5" xmlns="http://www.w3.org/2000/svg"  viewBox="0 -960 960 960" ><path d="M320-120v-80h80v-80H160q-33 0-56.5-23.5T80-360v-400q0-33 23.5-56.5T160-840h640q33 0 56.5 23.5T880-760v400q0 33-23.5 56.5T800-280H560v80h80v80H320ZM160-360h640v-400H160v400Zm0 0v-400 400Z"/></svg>
                        </button>
                        <button type="button" class="btn btn-white btn-primary-hover d-flex justify-between align-items-center" @click="mypop=window.open('ccp-informeCDP.php','','');mypop.focus();">
                            <span class="group-hover:text-white">Duplicar pantalla</span>
                            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 -960 960 960"><path d="M640-160v-360H160v360h480Zm80-200v-80h80v-360H320v200h-80v-200q0-33 23.5-56.5T320-880h480q33 0 56.5 23.5T880-800v360q0 33-23.5 56.5T800-360h-80ZM160-80q-33 0-56.5-23.5T80-160v-360q0-33 23.5-56.5T160-600h480q33 0 56.5 23.5T720-520v360q0 33-23.5 56.5T640-80H160Zm400-603ZM400-340Z"></path></svg>
                        </button>
                        <button type="button" @click="excel()"
                            class="btn btn-success btn-success-hover d-flex justify-between align-items-center">
                            <span>Exportar Excel</span>
                            <svg class="fill-black group-hover:fill-white w-4 h-4" xmlns="http://www.w3.org/2000/svg"
                                viewBox="0 0 384 512"><!--!Font Awesome Free 6.5.1 by @fontawesome - https://fontawesome.com License - https://fontawesome.com/license/free Copyright 2024 Fonticons, Inc. -->
                                <path
                                    d="M48 448V64c0-8.8 7.2-16 16-16H224v80c0 17.7 14.3 32 32 32h80V448c0 8.8-7.2 16-16 16H64c-8.8 0-16-7.2-16-16zM64 0C28.7 0 0 28.7 0 64V448c0 35.3 28.7 64 64 64H320c35.3 0 64-28.7 64-64V154.5c0-17-6.7-33.3-18.7-45.3L274.7 18.7C262.7 6.7 246.5 0 229.5 0H64zm90.9 233.3c-8.1-10.5-23.2-12.3-33.7-4.2s-12.3 23.2-4.2 33.7L161.6 320l-44.5 57.3c-8.1 10.5-6.3 25.5 4.2 33.7s25.5 6.3 33.7-4.2L192 359.1l37.1 47.6c8.1 10.5 23.2 12.3 33.7 4.2s12.3-23.2 4.2-33.7L222.4 320l44.5-57.3c8.1-10.5 6.3-25.5-4.2-33.7s-25.5-6.3-33.7 4.2L192 280.9l-37.1-47.6z">
                                </path>
                            </svg>
                        </button>
                        <button type="button" class="btn btn-success d-flex justify-between align-items-center" @click="window.location.href='ccp-librosppto.php'">
                            <span>Atrás</span>
                            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 -960 960 960"><path d="M760-200v-160q0-50-35-85t-85-35H273l144 144-57 56-240-240 240-240 57 56-144 144h367q83 0 141.5 58.5T840-360v160h-80Z"/></svg>
                        </button>
                    </div>
				</nav>

				<article class="bg-white">
                    <h2 class="titulos m-0">Reporte CDP</h2>
                    <div>
                        <div class="d-flex w-75">
                            <div class="form-control w-25">
                                <label class="form-label">Fecha inicial:</label>
                                <input type="text" name="fecha"  value="<?php echo $_POST['fechaIni']?>" onKeyUp="return tabular(event,this)" id="fechaIni" title="DD/MM/YYYY" onDblClick="displayCalendarFor('fechaIni');" class="colordobleclik" autocomplete="off" onChange="" readonly>
                            </div>
                            <div class="form-control w-25">
                                <label class="form-label">Fecha final:</label>
                                <input type="text" name="fechaFin" value="<?php echo $_POST['fecha2']?>" onKeyUp="return tabular(event,this)" id="fechaFin" title="DD/MM/YYYY" onDblClick="displayCalendarFor('fechaFin');" class="colordobleclik" autocomplete="off" onChange="" readonly>
                            </div>
                            <div class="d-flex">
                                <div class="form-control">
                                    <label class="form-label" for="">Filtro:</label>
                                    <div class="d-flex">
                                        <select id="labelSelectName2" v-model="selectFiltro">
                                            <option value="0" selected>Todo</option>
                                            <option value="1">Inversión</option>
                                            <option value="2">Servicio</option>
                                            <option value="3">Funcionamiento</option>
                                        </select>
                                        <button type="button" class="btn btn-primary" v-on:click="traeDatosFormulario">Buscar</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <h2 class="titulos m-0">Detalles informe</h2>
                    <div class="overflow-auto max-vh-50 overflow-x-hidden p-2" >
                        <table class="table fw-normal">
                            <thead>
                                <tr>
                                    <th >Vigencia</th>
                                    <th >Consvigencia</th>
                                    <th >Fecha</th>
                                    <th >Objeto</th>
                                    <th >Rubro</th>
                                    <th >Cuenta CCPET</th>
                                    <th >producto servicio</th>
                                    <th >fuente</th>
                                    <th >bpim</th>
                                    <th >indicador producto</th>
                                    <th >medio pago</th>
                                    <th >Vigencia gasto</th>
                                    <th >Valor</th>
                                    <th >Saldo</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr v-for="(det,index) in detalles">
                                    <td >{{ det[0] }}</td>
                                    <td >{{ det[1] }}</td>
                                    <td class="text-nowrap">{{ det[2] }}</td>
                                    <td >{{ det[3] }}</td>
                                    <td >{{ det[13] }}</td>
                                    <td >{{ det[4] }}</td>
                                    <td >{{ det[5] }}</td>
                                    <td >{{ det[6] }}</td>
                                    <td >{{ det[7] }}</td>
                                    <td >{{ det[8] }}</td>
                                    <td >{{ det[9] }}</td>
                                    <td >{{ det[10] }}</td>
                                    <td > {{ formatonumero(det[11]) }}</td>
                                    <td > {{ formatonumero(det[12]) }}</td>

                                    <input type='hidden' name='vigencia[]' v-model="det[0]">
                                    <input type='hidden' name='consvigencia[]' v-model="det[1]">
                                    <input type='hidden' name='fecha[]' v-model="det[2]">
                                    <input type='hidden' name='objeto[]' v-model="det[3]">
                                    <input type='hidden' name='rubro[]' v-model="det[13]">
                                    <input type='hidden' name='cuenta[]' v-model="det[4]">
                                    <input type='hidden' name='productoServicio[]' v-model="det[5]">
                                    <input type='hidden' name='fuente[]' v-model="det[6]">
                                    <input type='hidden' name='bpim[]' v-model="det[7]">
                                    <input type='hidden' name='indicadorProducto[]' v-model="det[8]">
                                    <input type='hidden' name='medioPago[]' v-model="det[9]">
                                    <input type='hidden' name='vigenciaGasto[]' v-model="det[10]">
                                    <input type='hidden' name='valor[]' v-model="det[11]">
                                    <input type='hidden' name='saldo[]' v-model="det[12]">
                                </tr>
                            </tbody>
                        </table>
                    </div>

					<div class="loading-container" v-show="loading" >
                        <p class="text-loading"data-loading="IDEAL 10">IDEAL 10</p>
                    </div>
				</article>
			</section>
		</form>

		<script src="node_modules/read-excel-file/bundle/read-excel-file.min.js"></script>
		<script src="node_modules/xlsx/dist/xlsx.full.min.js"></script>
		<script src="Librerias/vue/vue.min.js"></script>
		<script src="Librerias/vue/axios.min.js"></script>
		<script src="presupuesto_ccpet/reporteCDP/ccp-informeCDP.js?"></script>

	</body>
</html>
