<?php
	header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=utf8");
	require "comun.inc";
	require "funciones.inc";
	sesion();
	$linkbd=conectar_v7();
	$linkbd -> set_charset("utf8");
	cargarcodigopag($_GET[codpag],$_SESSION["nivel"]);
	date_default_timezone_set("America/Bogota");
	$cuenta = $_POST['cuenta'];
	$vigengasto = $_POST['vigengasto'];
	$seccpresu = $_POST['seccpresu'];
	$progrmga = $_POST['progrmga'];
	$bpin = $_POST['bpin'];
	$tabla = "<table class='inicio'>
	<tr style='text-align:center;'>
		<td class='titulos2'>Cuenta</td>
		<td class='titulos2'>Descripción</td>
		<td class='titulos2'>Vigencia del Gasto</td>
		<td class='titulos2'>Secci&oacute;n Presupuestal</td>
		<td class='titulos2'>Programatico MGA</td>
		<td class='titulos2'>BPIN</td>
		<td class='titulos2'>Registro</td>
		<td class='titulos2'>Apropiaci&oacute;n Inicial</td>
		<td class='titulos2'>Apropiaci&oacute;n Definitiva</td>
	</tr>";
	if($cuenta != '2.99')
	{
		$sqlver="
		SELECT id, codigocuenta, nombrecuenta, seccpresupuestal, vigenciagastos, sector, programatico, bpin, apropiacionini, apropiaciondef, registro
		FROM ".$_SESSION['tablatemporal']."
		WHERE codigocuenta = '$cuenta' AND seccpresupuestal = '$seccpresu' AND vigenciagastos = '$vigengasto' AND programatico = '$progrmga' AND bpin = '$bpin'
		ORDER BY id ASC";
	}
	else
	{
		$sqlver="
		SELECT id, codigocuentaaux, nombrecuenta, seccpresupuestal, vigenciagastos, sector, programatico, bpin, apropiacionini, apropiaciondef, registro
		FROM ".$_SESSION['tablatemporal']."
		WHERE codigocuentaaux LIKE '2.3.%' AND seccpresupuestal = '$seccpresu' AND vigenciagastos = '$vigengasto' AND programatico = '$progrmga' AND bpin = '$bpin'
		ORDER BY id ASC";
	}
	$resver = mysqli_query($linkbd,$sqlver);
	while ($rowver = mysqli_fetch_row($resver))
	{
		$tabla.="<tr class='cssdeta' onMouseOver=\"anterior=this.style.backgroundColor;this.style.backgroundColor='#40b3ff';\"
onMouseOut=\"this.style.backgroundColor=anterior\">
			<td>$rowver[1]</td>
			<td>$rowver[2]</td>
			<td>$rowver[4]</td>
			<td>$rowver[3]</td>
			<td>$rowver[6]</td>
			<td>$rowver[7]</td>
			<td>$rowver[10]</td>
			<td style='text-align:right;'>$".number_format($rowver[8],0,',','.')."</td>
			<td style='text-align:right;'>$".number_format($rowver[9],0,',','.')."</td>
		</tr>";
	}
$tabla.='</table>';
$data_row = array('detalle'=>$tabla);
header('Content-Type: application/json');
echo json_encode($data_row);
?>