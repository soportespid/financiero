<?php
    header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=utf8");

	require 'comun.inc';
	require 'funciones.inc';

	session_start();
    date_default_timezone_set("America/Bogota");

?>
<!DOCTYPE >
<html lang="es">
	<head>
		<meta charset="utf-8"/>
		<meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1"/>
		<meta name="viewport" content="user-scalable=no">
		<title>:: Ideal - Presupuesto</title>
		<link href="favicon.ico" rel="shortcut icon"/>
        
		<link href="css/css2.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<link href="css/css2z.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
        <link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
        <link href="css/css4.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
        <script type="text/javascript" src="css/programas.js"></script>
        <script type="text/javascript" src="css/calendario.js"></script>
        <script type="text/javascript" src="JQuery/jquery-2.1.4.min.js"></script>

		<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">

        <script>
            function cambiaFecha(){
                var fecha = document.getElementById('fecha').value;
                fecArray = fecha.split('/');
                document.getElementById('vigencia').value = fecArray[2];
            }
        </script>

		<style>
			.inicio--no-shadow{
				box-shadow: none;
			}

			[v-cloak]{
				display : none;
			}
			
			.seleccionarTodos {
				padding: 4px 0px 4px 10px;
				background: linear-gradient(#337CCF, #1450A3);
				border: 1px solid;
				border-top-color: #0A6EBD;
				border-right-color: #0A6EBD;
				border-bottom-color: #337CCF;
				border-left-color: #337CCF;
				box-shadow: 4px 4px 2px #368eb880;
				border-radius: 5px;
				display: flex;
				align-items: center;
				flex-direction: row;
				text-align: center;
				gap: 5px;
				color: #fff;
				cursor: pointer;
				font-size: 0.8rem;
			}

			.span-check{
				margin-right: 0.5em;
				text-align: center;
				width: 1.5rem;
				height: 1.5rem;
				background-color: rgb(12, 176, 252);
				border: 2px solid;
				border-color: rgb(46, 136, 238);
				border-radius: 5px;
				cursor: pointer;
			}

			.span-check-completed {
				margin-right: 0.5em;
				text-align: center;
				width: 1.5rem;
				height: 1.5rem;
				background-color: rgb(12, 176, 252);
				border: 2px solid;
				border-color: rgb(46, 136, 238);
				border-radius: 5px;
				cursor: pointer;
				background-image: url("data:image/svg+xml,%3csvg viewBox='0 0 16 16' fill='white' xmlns='http://www.w3.org/2000/svg'%3e%3cpath d='M12.207 4.793a1 1 0 010 1.414l-5 5a1 1 0 01-1.414 0l-2-2a1 1 0 011.414-1.414L6.5 9.086l4.293-4.293a1 1 0 011.414 0z'/%3e%3c/svg%3e");
				background-size:auto;
			}
		</style>
    </head>
    <body>
        <IFRAME src="alertas.php" name="alertas" id="alertas" style="display:none"></IFRAME>
        <span id="todastablas2"></span>
        <table>
            <tr><script>barra_imagenes("contra");</script><?php cuadro_titulos();?></tr>	 
            <tr><?php menu_desplegable("contra");?></tr>
        	<tr>
          		<td colspan="3" class="cinta">
					<a><img src="imagenes/add.png" title="Nuevo" onClick="location.href='ccp-homologarfuentesf05.php'" class="mgbt"/></a>
					<a class="mgbt"><img src="imagenes/guardad.png"/></a>
					<a><img src="imagenes/buscad.png" title="Buscar" class="mgbt"/></a>
					<a href="#" onClick="mypop=window.open('contra-principal.php','','');mypop.focus();" class="mgbt"><img src="imagenes/nv.png" title="Nueva Ventana"></a>
					<img src="imagenes/iratras.png" title="Atr&aacute;s" onClick="location.href='ccp-ejecucionpresupuestal.php'" class="mgbt"/>
				</td>
        	</tr>
		</table>
		<div class="subpantalla" style="height:78%; width:99.6%; overflow:hidden; resize: vertical;">
			<div id="myapp" class="padding: 50px" v-cloak >
				<div class="row">
					<div class="col-12">
						<h5 style="padding-left:50px; padding-top:2px; padding-bottom:2px; background-color: #0FB0D4; text-align: center;">Solicitud certificado de disponibilidad.</h5>
					</div>
				</div>
				<div style="height:100%;">
                <div class="row" style="border-radius:2px; background-color: #F6F6F6; padding: 5px;">

                    <div class="col-md-1" style="display: grid; align-content:center;">
                        <label for="" style="margin-bottom: 0;">Fecha:</label>
                    </div>
                    <div class="col-md-2" style="padding: 1px">
                        <div class="row">
                            <div class="col-md-8" style="padding: 1px; margin-left: 15px;">
                                <input type="text" id="fecha" name="fecha" class="form-control input" title="DD/MM/YYYY" placeholder="DD/MM/YYYY" onchange="cambiaFecha();">
                            </div>
                            <div class="col-md-2" style="padding: 2px; padding-top: 8px">
                                <a href="#" onClick="displayCalendarFor('fecha');" tabindex="3" title="Calendario"><img src="imagenes/calendario04.png" align="absmiddle" style="width:20px;"></a>
                            </div>
                        </div>
                        
                    </div>

                    </div>
					
					<div class="row" style="margin: 0px 50px 0; border-radius: 2px; background-color: #E1E2E2; ">
						<div class="col-md-10" style="padding: 12px 0px 0px 30px; font: 140% sans-serif;">
                            <label for="">Buscar Fuente. <span style="font-size: 12px; font-weight: bold;"><i>(filtre por fuente o nombre fuente.)</i></span></label>
						</div>
						<div class="col-md-2" style="padding: 4px" v-show="puedeGuardar">
							<button class="col btn btn-success" @click = "guardarFuentesf04">Guardar Cambios</button>
						</div>
					</div>
					<div class="row" style="margin:0px 50px 0; border-radius: 2px; background-color: #E1E2E2;">
						<div class="col-md-10" style="padding: 4px">
							<input v-on:keyup="buscarFuente" v-model="buscar_fuente" type="text" class="form-control" style="height: auto; border-radius:4px;" placeholder="Buscar Fuente.">
						</div>
						<div class="col-md-2" style="padding: 4px">
							<select v-model="vigencia" style="width:100%" v-on:Change="cargarParametros" class="form-control" style = "font-size: 10px; margin-top:4px">
								<option v-for="year in years" :value="year[0]">{{ year[0] }}</option>
							</select>
						</div>	
					</div>

					<div style="margin: 10px 50px 0">
						<table>
							<thead>
								<tr>
									<td class='titulos' width="20%"  style="font: 160% sans-serif; border-radius: 5px 0px 0px 0px;">Fuente CUIPO</td>
									<td class='titulos' style="font: 160% sans-serif;">
										Nombre
									</td>
                                    <td class='titulos' width = "20%" style="text-align: center;">
										Fuente F05
									</td>
									
									<td class='titulos' width = "1%"></td>
								</tr>
							</thead>
						</table>
					</div>
					<div class="estilos-scroll" style="margin: 0px 50px 20px; border-radius: 0 0 0 5px; height: 62%; overflow: scroll; overflow-x: hidden; background: white;">
						<table class='inicio inicio--no-shadow'>
							<tbody v-if="show_resultados">
								
								<tr v-for="(result, index) in results" :style="estaEnArray(result) == true ? myStyle : ''" v-bind:class="index % 2 ? 'contenidonew00' : 'contenidonew01'" >
									<td width="20%" style="font: 160% sans-serif; padding: 5px;">{{ result[0].split('-')[0] }}</td>
									<td colspan="2" style="font: 160% sans-serif;">{{ result[1] }}</td>

                                    <td width="20%" style="text-align: center;">
										<div v-on:dblclick="agregarFuentef05(result[0],result[1])" title="Doble click para desplegar ventana emergente" style='text-rendering: optimizeLegibility; text-align:center; background-color: #FBE71B; cursor: pointer !important; border-radius: 5px; height: 5vh; display: flex; flex-direction: column; justify-content: center'>
											{{ selFuenteHomologar && selFuenteHomologar[result[0]] }} 
										</div>
									</td>
									
								</tr>
							</tbody>
							<tbody v-else>
								<tr>
									<td width="20%"style="font: 120% sans-serif; padding-left:10px; text-align:center;" colspan="3">Sin resultados</td>
								</tr>
							</tbody>
						</table>
					</div>
					
				</div>
				

				<div v-show="showModal_fuentesf05">
                    <transition name="modal">
                        <div class="modal-mask">
                            <div class="modal-wrapper">
                                <div class="modal-dialog modal-lg" style = "max-width: 1200px !important;" role="document">
                                    <div class="modal-content"  style = "width: 1200px !important;" scrollable>
                                        <div class="modal-header">
                                            <h5 class="modal-title">{{ rubroVentanaEmergente }}</h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true" @click="showModal_fuentesf05 = false">&times;</span>
                                            </button>
                                        </div>
                                        <div class="modal-body">
                                            <div style="margin: 2px 0 0 0;">
                                                <div class="row" style="margin: 4px; border-radius:4px; background-color: #F6F6F6; ">
                                                    <div class="col-md-2" style="padding: 12px 0px 0px 30px; font: 140% sans-serif;">
                                                        <label for="">Fuente F05:</label>
                                                    </div>
                                                    
                                                    <div class="col-md-6 col-md-offset-6" style="padding: 4px">
                                                        <input type="text" class="form-control" placeholder="Buscar por nombre" style="font: sans-serif; " v-on:keyup="searchMonitorFuentef05" v-model="searchFuentef05.keywordFuentef05">
                                                    </div>
                                                </div>
                                                <table>
                                                    <thead>
                                                        <tr>
                                                            <td width="20%" class='titulos' style="font: 120% sans-serif; padding-left:10px; border-radius: 5px 0 0 0;">Fuente</td>
                                                            <td class='titulos' style="font: 120% sans-serif; ">Nombre</td>

                                                        </tr>
                                                    </thead>
                                                </table>
                                            </div>
                                            <div style="margin: 2px 0 0 0; border-radius: 0 0 0 6px; height: 200px; overflow: scroll; overflow-x: hidden; background: white; ">
                                                <table class='inicio inicio--no-shadow'>
                                                    <tbody>
                                                        <?php
                                                            $co ='zebra1';
                                                            $co2='zebra2';
                                                        ?>
                                                        <tr v-for="fuentef05 in fuentesf05" v-on:click="seleccionarFuentef05(fuentef05)" class='<?php echo $co; ?>' style='text-rendering: optimizeLegibility; cursor: pointer !important; style=\"cursor: hand\"'>
                                                            <td width="20%" style="font: 120% sans-serif; padding-left:10px">{{ fuentef05[0] }}</td>
                                                            <td  style="font: 120% sans-serif; padding-left:10px">{{ fuentef05[1] }}</td>
                                                            <?php
                                                            $aux=$co;
                                                            $co=$co2;
                                                            $co2=$aux;
                                                            ?>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-secondary" @click="showModal_fuentesf05 = false">Cerrar</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </transition>
                </div>

				

                <div id="cargando" v-show="loading" class="loading">
                    <span>Cargando...</span>
                </div>

			</div>	
		</div>

        <script type="module" src="./contratacion/solicitud/contra-solicitudcdp.js"></script>
        <script src="Librerias/vue/axios.min.js"></script>
        <script src="sweetalert2/dist/sweetalert2.min.js"></script>
        <link rel="stylesheet" href="sweetalert2/dist/sweetalert2.min.css">
        <script src="https://unpkg.com/vue@3/dist/vue.global.js"></script>
	</body>
</html>