<?php
	header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=utf8");
	require "comun.inc";
	require "funciones.inc";
	require "funcionesSP.inc.php";
	session_start();
	$linkbd = conectar_v7();
	$linkbd -> set_charset("utf8"); 
	cargarcodigopag($_GET['codpag'],$_SESSION['nivel']);
    date_default_timezone_set("America/Bogota");
	titlepag();
			
?>

<!DOCTYPE >

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="es">
	<head>
		<meta http-equiv="Content-type" content="text/html; charset=utf8"/>
		<meta http-equiv="X-UA-Compatible" content="IE=9"/>
		<title>:: IDEAL 10 - Servicios P&uacute;blicos</title>
		<link href="css/css2.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<link href="css/css4.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<link href="css/cssSP.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<link href="css/tabs.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
        <link href="css/style.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css"/>
		<script type="text/javascript" src="css/calendario.js"></script>
        <script type="text/javascript" src="css/programas.js"></script>
		<script type="text/javascript" src="jquery-1.11.0.min.js"></script> 
        
		<script>
            $(window).load(function () {
				$('#cargando').hide();
			});

			function despliegamodalm(_valor,_tip,mensa,pregunta)
			{
				document.getElementById("bgventanamodalm").style.visibility=_valor;
                if(_valor == "hidden")
                {
                    document.getElementById('ventanam').src="";
                }
				else
				{
					switch(_tip)
					{
						case "1":	document.getElementById('ventanam').src="ventana-mensaje1.php?titulos="+mensa;break;
						case "2":	document.getElementById('ventanam').src="ventana-mensaje3.php?titulos="+mensa;break;
						case "3":	document.getElementById('ventanam').src="ventana-mensaje2.php?titulos="+mensa;break;
						case "4":	document.getElementById('ventanam').src="ventana-consulta1.php?titulos="+mensa+"&idresp="+pregunta;break;	
					}
				}
            }

			function funcionmensaje()
			{
				var x = window.location.search;
				location.href = "serv-editaUsuario.php"+x;
            }

			function guardar()
			{
                despliegamodalm('visible','4','Esta Seguro de Modificar','1');   
            }

			function respuestaconsulta(pregunta)
			{
				switch(pregunta)
				{
					case "1":	
						document.form2.oculto.value='2';
						document.form2.submit();
					break;
				}
				document.form2.submit();
            }
            
            function buscaLecturas()
            {
                var corteValidacion = document.getElementById('corte').value;
                var servicioValidacion = document.getElementById('servicio').value;
                var rutaValidacion = document.getElementById('ruta').value;

                if(corteValidacion != '-1')
                {
                    if(servicioValidacion != '-1')
                    {
                        if(rutaValidacion != '-1')
                        {
						    document.form2.submit();
                        }
                        else
                        {
                            despliegamodalm('visible', '2', 'Seleccione una ruta');
                        }
                    }
                    else
                    {
                        despliegamodalm('visible', '2', 'Seleccione un servicio');
                    }
                }
                else
                {
                    despliegamodalm('visible', '2', 'Seleccione un corte');
                }
            }

            function excell()
			{
				document.form2.action="serv-reporteLecturasEXCEL.php";
				document.form2.target="_BLANK";
				document.form2.submit(); 
				document.form2.action="";
				document.form2.target="";
			}
        </script>
                
	</head>
	<body>
		<IFRAME src="alertas.php" name="alertas" id="alertas" style="display:none"></IFRAME>
		<span id="todastablas2"></span>
		<table>
			<tr>
                <script>barra_imagenes("serv");</script><?php cuadro_titulos();?>
            </tr>

			<tr><?php menu_desplegable("serv");?></tr>
			<tr>
				<td colspan="3" class="cinta">
					<a href="serv-lecturas.php" class="mgbt"><img src="imagenes/add.png"/></a>
					<a onclick="guardar();" class="mgbt"><img src="imagenes/guarda.png" title="Guardar"/></a>
					<a href="serv-buscaLecturas.php" class="mgbt"><img src="imagenes/busca.png" title="Buscar"/></a>
					<a onclick="mypop=window.open('plan-agenda.php','','');mypop.focus()" class="mgbt"><img src="imagenes/agenda1.png" title="Agenda" /></a>
					<a onclick="mypop=window.open('serv-principal.php','','');mypop.focus();" class="mgbt"><img src="imagenes/nv.png" title="Nueva ventana"></a>
					<a onclick="mypop=window.open('<?php echo $url2; ?>','','');mypop.focus();" class="mgbt"><img src="imagenes/duplicar_pantalla.png" title="Duplicar pesta&ntilde;a"></a>
                    <a href=""><img src="imagenes/excel.png" title="Excel" onClick="excell()" class="mgbt"></td></a>
                </td>
			</tr>
        </table>
        
		<div id="bgventanamodalm" class="bgventanamodalm">
			<div id="ventanamodalm" class="ventanamodalm">
				<IFRAME src="" name="ventanam" marginWidth=0 marginHeight=0 frameBorder=0 id="ventanam" frameSpacing=0 style=" width:700px; height:130px; top:200; overflow:hidden;"></IFRAME>
			</div>
		</div>

		<form name="form2" method="post" action="">
			<?php
				if(@ $_POST['oculto'] == '')
				{
					
				}
				
                if(@$_POST['busca'] == '2')
                {
                   
                }
            ?>

			<div class="container">
				<table class="inicio" style="width: 99%;">
					<tr>
						<td class="titulos" colspan="8">Busca Lecturas</td>
						<td class="cerrar" style="width:7%" onClick="location.href='serv-principal.php'">Cerrar</td>
					</tr>

					<tr>
                        <td class="tamano01" style="width: 3cm;">Corte Liquidado:</td>
                        <td style="width: 20%;">
                            <select name="corte" id="corte" class="centrarSelect" style="width: 100%;">
                                <option value="-1" class="aumentarTamaño">SELECCIONE CORTE</option>
                                <?php
                                    $sql = "SET lc_time_names = 'es_ES'";
                                    mysqli_query($linkbd,$sql);

                                    $sqlr = "SELECT numero_corte, UPPER(MONTHNAME(fecha_inicial)), UPPER(MONTHNAME(fecha_final)), YEAR(fecha_inicial), YEAR(fecha_final) FROM srvcortes WHERE numero_corte > 0";
                                    $resp = mysqli_query($linkbd,$sqlr);
                                    while ($row = mysqli_fetch_row($resp))
                                    {
                                        if(@ $_POST['corte'] == $row[0])
                                        {
                                            echo "<option class='aumentarTamaño' value='$row[0]' SELECTED>$row[1] $row[3] - $row[2] $row[4]</option>";
                                        }
                                        else{echo "<option class='aumentarTamaño' value='$row[0]'>$row[1] $row[3] - $row[2] $row[4]</option>";}
                                    }
                                ?>
                            </select>
                        </td>

                        <td class="tamano01" style="width: 3cm;">Servicio:</td>
                        <td style="width: 20%;">
                            <select name="servicio" id="servicio" class="centrarSelect" style="width: 100%;">
                                <option value="-1" class="aumentarTamaño">SELECCIONE SERVICIO</option>
                                <?php
                                    $sql = "SET lc_time_names = 'es_ES'";
                                    mysqli_query($linkbd,$sql);

                                    $sqlServicios = "SELECT id, nombre FROM srvservicios WHERE estado = 'S' AND unidad_medida <> 'GASTOS DE OPERACION' ORDER BY id ASC";
                                    $resServicios = mysqli_query($linkbd,$sqlServicios);
                                    while($rowServicios = mysqli_fetch_row($resServicios))
                                    {
                                        if(@ $_POST['servicio'] == $rowServicios[0])
                                        {
                                            echo "<option class='aumentarTamaño' value='$rowServicios[0]' SELECTED>$rowServicios[1]</option>";
                                        }
                                        else{echo "<option class='aumentarTamaño' value='$rowServicios[0]'>$rowServicios[1]</option>";}
                                    }
                                ?>
                            </select>
                        </td>

                        <td class="tamano01" style="width: 3cm;">Rutas:</td>
                        <td style="width: 20%;">
                            <select name="ruta" id="ruta" class="centrarSelect" style="width: 100%;">
                                <option value="0" class="aumentarTamaño">TODAS</option>
                                <?php
                                    $sql = "SET lc_time_names = 'es_ES'";
                                    mysqli_query($linkbd,$sql);

                                    $sqlRutas = "SELECT id, nombre FROM srvrutas WHERE estado = 'S'";
                                    $resRutas = mysqli_query($linkbd,$sqlRutas);
                                    while($rowRutas = mysqli_fetch_row($resRutas))
                                    {
                                        if(@ $_POST['ruta'] == $rowRutas[0])
                                        {
                                            echo "<option class='aumentarTamaño' value='$rowRutas[0]' SELECTED>$rowRutas[1]</option>";
                                        }
                                        else{echo "<option class='aumentarTamaño' value='$rowRutas[0]'>$rowRutas[1]</option>";}
                                    }
                                ?>
                            </select>
                        </td>

						<td style="padding-bottom:0px">
							<em class="botonflecha" id="filtro" onclick="buscaLecturas();">Buscar</em>
						</td>
					</tr> 
				</table>

                <div id="cargando" style=" position:absolute;left: 46%; bottom: 45%">
                    <img src="imagenes/loading.gif" style=" width: 80px; height: 80px"/>
                </div>
                
				<div class="subpantalla" style="height:70%; width:99%; overflow-x:hidden; float:left;">
                    
                    <?php
                        $crit1 = '';

                        if($_POST['ruta'] != '0')
                        {
                            $crit1 = "AND C.id_ruta = $_POST[ruta]";
                        }
    
                        $sqlServicios = "SELECT nombre FROM srvservicios WHERE id = $_POST[servicio]";
                        $resServicios = mysqli_query($linkbd,$sqlServicios);
                        $rowServicios = mysqli_fetch_row($resServicios);
    
                        $corteAnt = $_POST['corte'] - 1;
                        $x = 0;

                        $sqlLectura = "SELECT DISTINCT L.id_cliente, L.lectura_medidor, L.consumo, L.observacion, C.id_tercero, C.id_ruta, C.codigo_ruta, C.cod_usuario FROM srvlectura AS L INNER JOIN srvclientes AS C ON L.id_cliente = C.id WHERE corte = $_POST[corte] AND id_servicio = $_POST[servicio] $crit1 ORDER BY C.id_ruta ASC, C.codigo_ruta ASC";
                        $resLectura = mysqli_query($linkbd,$sqlLectura);
                        $encontrados = mysqli_num_rows($resLectura);
                        while ($rowLectura = mysqli_fetch_row($resLectura)) 
                        {
                            $sqlLecturaAnt = "SELECT lectura_medidor FROM srvlectura WHERE corte = $_POST[corte] AND id_servicio = $_POST[servicio] AND id_cliente = $rowLectura[0]";
                            $resLecturaAnt = mysqli_query($linkbd,$sqlLecturaAnt);
                            $rowLecturaAnt = mysqli_fetch_row($resLecturaAnt);

                            $sqlDireccion = "SELECT direccion FROM srvdireccion_cliente WHERE id_cliente = $rowLectura[0]";
                            $resDireccion = mysqli_query($linkbd,$sqlDireccion);
                            $rowDireccion = mysqli_fetch_row($resDireccion);
    
                            $sqlTercero = "SELECT nombre1, nombre2, apellido1, apellido2, razonsocial FROM terceros WHERE id_tercero = '$rowLectura[4]' ";
                            $resTercero = mysqli_query($linkbd,$sqlTercero);
                            $rowTercero = mysqli_fetch_row($resTercero);
    
                            if ($rowTercero != '')
                            {
                                if($rowTercero[4] == ''){$nombreCompleto = $rowTercero[0]. ' ' .$rowTercero[1]. ' ' .$rowTercero[2]. ' ' .$rowTercero[3];}
                                else{$nombreCompleto = $rowTercero[4];}
                            }
    
                            if($rowLectura[7] != ''){$_POST['codUsuarios'][$x] = $rowLectura[7];}
                            else {$_POST['codUsuarios'][$x] = '';}
    
                            if($nombreCompleto != ''){$_POST['nombres'][$x] = $nombreCompleto;}
                            else {$_POST['nombres'][$x] = '';}
    
                            if($rowLectura[5] != ''){$_POST['rutas'][$x] = $rowLectura[5];}
                            else {$_POST['rutas'][$x] = '';}
    
                            if($rowLectura[6] != ''){$_POST['codigoRutas'][$x] = $rowLectura[6];}
                            else {$_POST['codigoRutas'][$x] = '';}
    
                            if($rowDireccion[0] != ''){$_POST['direcciones'][$x] = $rowDireccion[0];}
                            else {$_POST['direcciones'][$x] = '';}
    
                            $_POST['servicios'][$x] = $rowServicios[0];
    
                            if($rowLecturaAnt[0] != ''){$_POST['lecturasAnt'][$x] = $rowLecturaAnt[0];}
                            else {$_POST['lecturasAnt'][$x] = '';}
    
                            if($rowLectura[1] != ''){$_POST['lecturasAct'][$x] = $rowLectura[1];}
                            else {$_POST['lecturasAct'][$x] = '';}
    
                            if($rowLectura[2] != ''){$_POST['consumos'][$x] = $rowLectura[2];}
                            else {$_POST['consumos'][$x] = '';}
    
                            if($rowLectura[3] != ''){$_POST['observaciones'][$x] = $rowLectura[3];}
                            else {$_POST['observaciones'][$x] = '';}
    
                            echo"
                            <input type='hidden' name='codUsuarios[]' value='".$_POST['codUsuarios'][$x]."'>
                            <input type='hidden' name='nombres[]' value='".$_POST['nombres'][$x]."'>
                            <input type='hidden' name='rutas[]' value='".$_POST['rutas'][$x]."'>
                            <input type='hidden' name='codigoRutas[]' value='".$_POST['codigoRutas'][$x]."'>
                            <input type='hidden' name='direcciones[]' value='".$_POST['direcciones'][$x]."'>
                            <input type='hidden' name='servicios[]' value='".$_POST['servicios'][$x]."'>
                            <input type='hidden' name='lecturasAnt[]' value='".$_POST['lecturasAnt'][$x]."'>
                            <input type='hidden' name='lecturasAct[]' value='".$_POST['lecturasAct'][$x]."'>
                            <input type='hidden' name='consumos[]' value='".$_POST['consumos'][$x]."'>
                            <input type='hidden' name='observaciones[]' value='".$_POST['observaciones'][$x]."'>
                        ";
     

                            $x++;
                        }
                    ?>

					<table class="inicio grande">

                        <tr>
                            <td class="titulos" colspan="11">.: Lecturas Usuario</td>
                        </tr>

                        <tr>
							<td colspan='4'>Barrios Encontrados: <?php echo $encontrados ?></td>
						</tr>

                        <tr class="titulos2" style='text-align:center;'>
                            <td style="width: 7%">Código usuario</td>
                            <td style="width: 20%">Nombre Usuario</td>
                            <td style="width: 3%">Ruta</td>
                            <td style="width: 7%">Código de ruta</td>
                            <td style="width: 9%">Dirección</td>
                            <td style="width: 7%">Servicio</td>
                            <td style="width: 7%">Lectura Anterior</td>
                            <td style="width: 7%">Lectura Actual</td>
                            <td style="width: 7%">Consumo</td>
                            <td>Observación</td>
					    </tr>

                    <?php
                        $iter = 'saludo1a';
                        $iter2 = 'saludo2';

                        for ($x=0; $x < count($_POST['codUsuarios']); $x++) 
                        { 
                    ?>
                            <tr class="<?php echo $iter ?>" style='text-transform:uppercase; text-align:center;'>
                                <td><?php echo $_POST['codUsuarios'][$x] ?></td>
                                <td><?php echo $_POST['nombres'][$x] ?></td>
                                <td><?php echo $_POST['rutas'][$x] ?></td>
                                <td><?php echo $_POST['codigoRutas'][$x] ?></td>
                                <td><?php echo $_POST['direcciones'][$x] ?></td>
                                <td><?php echo $_POST['servicios'][$x] ?></td>
                                <td><?php echo $_POST['lecturasAnt'][$x] ?></td>
                                <td><?php echo $_POST['lecturasAct'][$x] ?></td>
                                <td><?php echo $_POST['consumos'][$x] ?></td>
                                <td><?php echo $_POST['observaciones'][$x] ?></td>
                            </tr>
                    <?php
                            $aux = $iter;
                            $iter = $iter2;
                            $iter2 = $aux;	
                        }
                    ?>
					</table>
				</div>
			</div>

			<?php
				if($_POST['oculto'] == '2')
				{

                }
			?>

            <input type="hidden" name="busca" id="busca" value="1"/>    
			<input type="hidden" name="oculto" id="oculto" value="1"/>
			
			<div id="bgventanamodal2">
				<div id="ventanamodal2">
					<IFRAME  src="" name="buster" marginWidth=0 marginHeight=0 frameBorder=0 id="ventana2" frameSpacing=0 style="left:500px; width:900px; height:500px; top:200;"></IFRAME>
				</div>
			</div>
			
			
        </form>                          
    </body>   
</html>
