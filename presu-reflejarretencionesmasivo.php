<?php
	ini_set('max_execution_time',3600);
	header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=utf8");

	require 'comun.inc';
	require 'funciones.inc';

	$linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");

	session_start();
    date_default_timezone_set("America/Bogota");
?>
<!DOCTYPE >
<html lang="es">
	<head>
		<meta charset="UTF-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
    	<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<title>:: IDEAL 10 - Administraci&oacute;n</title>
		<link href="favicon.ico" rel="shortcut icon"/>
        <link href="css/css2.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<link href="css/css4.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<script type="text/javascript" src="jquery-1.11.0.min.js"></script> 
        <script type="text/javascript" src="css/programas.js"></script>
        <script type="text/javascript" src="css/calendario.js"></script>
        <script>
			$(window).load(function () { $('#cargando').hide();});
			function buscar()
			{
				var fechaini = document.getElementById("fechaini").value;
				var fechafin = document.getElementById("fechafin").value;
				
				if(fechaini!='' && fechafin!=''){
					document.form2.oculto.value='3';
					document.form2.submit(); 
				}else{
					despliegamodalm('visible','2','Debe existir una fecha inicial y una fecha final');
				}
				
			}
			
			function reflejar(){
				var numrecaudos = document.getElementsByName("recaudocc[]");
				if(numrecaudos.length >0){
					document.form2.oculto.value='2';
					document.form2.submit(); 
				}else{
					despliegamodalm('visible','2','No existen recibos para reflejar');
				}
			}
			
			function despliegamodalm(_valor,_tip,mensa,pregunta)
			{
				document.getElementById("bgventanamodalm").style.visibility=_valor;
				if(_valor=="hidden"){document.getElementById('ventanam').src="";}
				else
				{
					switch(_tip)
					{
						case "1":
							document.getElementById('ventanam').src="ventana-mensaje1.php?titulos="+mensa;break;
						case "2":
							document.getElementById('ventanam').src="ventana-mensaje3.php?titulos="+mensa;break;
						case "3":
							document.getElementById('ventanam').src="ventana-mensaje2.php?titulos="+mensa;break;
						case "4":
							document.getElementById('ventanam').src="ventana-consulta1.php?titulos="+mensa+"&idresp="+pregunta;break;	
					}
				}
			}
			
			function funcionmensaje()
			{
				
			}
			
			function respuestaconsulta(pregunta)
			{
				switch(pregunta)
				{
					case "1":	document.form2.oculto.value=2;
								document.form2.submit();
								break;
				}
			}
			
        </script>
		 
	</head>
	<body>
		
		<IFRAME src="alertas.php" name="alertas" id="alertas" style="display:none"></IFRAME>
        <span id="todastablas2"></span>
        <table>
            <tr><script>barra_imagenes("adm");</script><?php cuadro_titulos();?></tr>	 
            <tr><?php menu_desplegable("adm");?></tr>
        	<tr>
  				<td colspan="3" class="cinta">
				<a href="presu-reflejarretencionesmasivo.php" class="mgbt"><img src="imagenes/add.png" title="Nuevo"/></a>
				<a onClick="document.form2.submit();" href="#" class="mgbt"><img src="imagenes/busca.png" title="Buscar" /></a>
				<a href="#" class="mgbt" onClick="mypop=window.open('adm-principal.php','',''); mypop.focus();"><img src="imagenes/nv.png" title="Nueva Ventana"></a> 
				<a href="#" onclick="crearexcel()" class="mgbt"><img src="imagenes/excel.png" title="Excell"></a><a href="adm-comparacomprobantes-presu.php" class="mgbt"><img src="imagenes/iratras.png" title="Atr&aacute;s"></a></td>
         	</tr>	
		</table>
		
		 <div id="bgventanamodalm" class="bgventanamodalm">
            <div id="ventanamodalm" class="ventanamodalm">
                <IFRAME src="" name="ventanam" marginWidth=0 marginHeight=0 frameBorder=0 id="ventanam" frameSpacing=0 style=" width:700px; height:130px; top:200; overflow:hidden;"> 
                </IFRAME>
            </div>
        </div>
		
 		<form name="form2" method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>"> 
		 	<div class="loading" id="divcarga"><span>Cargando...</span></div> 
			
			<?php
				$iter='saludo1b';
				$iter2='saludo2b';
			?>
			<table width="100%" align="center"  class="inicio" >
                <tr>
                    <td class="titulos" colspan="9">:: Buscar .: Retenciones </td>
                    <td class="cerrar" style='width:7%' onClick="location.href='cont-principal.php'">Cerrar</td>
                    <input type="hidden" name="oculto" id="oculto" value="<?php echo $_POST['oculto']; ?>">
                    <input type="hidden" name="iddeshff" id="iddeshff" value="<?php echo $_POST['iddeshff'];?>">	 
                </tr>                       
                <tr>
                    <td  class="saludo1" >Fecha Inicial: </td>
                    <td><input type="search" name="fechaini" id="fechaini" title="YYYY/MM/DD" onchange="" value="<?php echo $_POST['fechaini']; ?>" onKeyUp="return tabular(event,this)" onKeyDown="mascara(this,'/',patron,true)"  maxlength="10">&nbsp;<img src="imagenes/calendario04.png" style="width:20px" onClick="displayCalendarFor('fechaini');" class="icobut" title="Calendario"></td>
                    <td  class="saludo1" >Fecha Final: </td>
                    <td ><input type="search" name="fechafin" id="fechafin" title="YYYY/MM/DD" onchange="" value="<?php echo $_POST['fechafin']; ?>" onKeyUp="return tabular(event,this) " onKeyDown="mascara(this,'/',patron,true)"  maxlength="10">&nbsp;<img src="imagenes/calendario04.png" style="width:20px" onClick="displayCalendarFor('fechafin');"  class="icobut" title="Calendario"></td>  
                    <td style=" padding-bottom: 0em"><em class="botonflecha" onClick="buscar()">Buscar</em></td>
					<td style=" padding-bottom: 0em"><em class="botonflecha" onClick="reflejar()">Reflejar</em></td>
                </tr>
			</table>
			
			<?php
				
			if($_POST['oculto']==3){
				//Variables ocultas para información de tablas
				unset($_POST['recaudocc']);
				unset($_POST['recaudocc1']);
				unset($_POST['conceptocc']);
				unset($_POST['valtotaltescc']);
				unset($_POST['valtotalcontcc']);
				unset($_POST['diferenciacc']);
				unset($_POST['vigenciaComp']);
				
				$_POST['recaudocc']= array_values($_POST['recaudocc']);
				$_POST['recaudocc1']= array_values($_POST['recaudocc1']);
				$_POST['conceptocc']= array_values($_POST['conceptocc']);
				$_POST['valtotaltescc']= array_values($_POST['valtotaltescc']);
				$_POST['valtotalcontcc']= array_values($_POST['valtotalcontcc']);
				$_POST['diferenciacc']= array_values($_POST['diferenciacc']);
				$_POST['vigenciaComp']= array_values($_POST['vigenciaComp']);
			
				function calcularRetenciones($numCxp)
				{
					$linkbd = conectar_v7();
					$linkbd -> set_charset("utf8");

					$totalRetenciones = 0;
					$sqlrRetenciones = "SELECT TB1.id_retencion, TB1.valor FROM tesoordenpago_retenciones AS TB1, tesoretenciones AS TB2 WHERE TB1.id_orden='$numCxp' AND TB1.id_retencion = TB2.id AND TB2.terceros = ''";
					$resRetenciones=mysqli_query($linkbd, $sqlrRetenciones);

					while ($rowRetenciones = mysqli_fetch_row($resRetenciones))
					{
						$sqlrPorcentaje = "SELECT porcentaje FROM tesoretenciones_det WHERE codigo='$rowRetenciones[0]' AND conceptocausa!='-1'";
						$resPorcentaje = mysqli_query($linkbd, $sqlrPorcentaje);
						$rowPorcentaje = mysqli_fetch_row($resPorcentaje);
						$totalRetenciones += (($rowPorcentaje[0]/100) * $rowRetenciones[1]);
					}
					return $totalRetenciones;
				}

				$queryDate="";
				if(isset($_POST['fechafin']) and isset($_POST['fechaini'])){

					if(!empty($_POST['fechaini']) and !empty($_POST['fechafin'])){
						$fechaInicial=date('Y-m-d',strtotime($_POST['fechaini']));
						$fechaFinal=date('Y-m-d',strtotime($_POST['fechafin']));
						$queryDate="AND TE.fecha>='".$fechaInicial."' and TE.fecha<='".$fechaFinal."'";
					}
				}
				$sqlr = "SELECT TE.id_egreso, TE.id_orden, TE.concepto, TE.vigencia FROM tesoegresos TE, tesoordenpago TEO WHERE TE.estado = 'S' AND TE.id_orden=TEO.id_orden AND TEO.medio_pago!='2' $queryDate ORDER BY TE.id_egreso DESC";
				$resp=mysqli_query($linkbd, $sqlr);
				while ($row =mysqli_fetch_row($resp))
				{
					$estilo="";
					$stado="";
					$dif_retencion = 0;
					$dif_retencion = calcularRetenciones($row[1]);
					$sql = "select C.idrecibo,sum(C.valor) FROM pptoretencionpago C  where C.idrecibo=$row[0]";
					$rs=mysqli_query($linkbd, $sql);
					$rw=mysqli_fetch_row($rs);
					if($dif_retencion > 0)
					{
						if($rw[0] != null){
							$difround = round($dif_retencion-$rw[1]);
							if ($difround!=0)
							{
								$_POST['recaudocc'][] = $row[0];
								$_POST['recaudocc1'][] = $row[1];
								$_POST['conceptocc'][] = $row[2];
								$_POST['valtotaltescc'][] = $dif_retencion;
								$_POST['valtotalcontcc'][] = $rw[1];
								$_POST['diferenciacc'][] = $difround;
								$_POST['vigenciaComp'][] = $row[3];
							}
						}else{
							$_POST['recaudocc'][] = $row[0];
							$_POST['recaudocc1'][] = $row[1];
							$_POST['conceptocc'][] = $row[2];
							$_POST['valtotaltescc'][] = $dif_retencion;
							$_POST['valtotalcontcc'][] = 0;
							$_POST['diferenciacc'][] = $dif_retencion;
							$_POST['vigenciaComp'][] = $row[3];
						}
					}
				} 				
			}
			
			echo "<div class='subpantallac5' style='height:55%; width:99.6%; margin-top:0px; overflow-x:hidden' id='divdet'>
				<table class='inicio' align='center' id='valores' >
				<tbody>";
				echo "<tr class='titulos ' style='text-align:center;'>
							<td ></td>
							<td ></td>
							<td >Tesoreria</td>
							<td >Presupuesto</td>
							<td ></td>
						</tr>
						<tr class='titulos' style='text-align:center;'>
							<td id='col1'>Egreso</td>
							<td id='col2'>Concepto</td>
							<td id='col3'>Valor Total</td>
							<td id='col6'>Valor Total</td>
							<td id='col7'>Diferencia</td>
						</tr>";
						
			for($k=0; $k<count($_POST['recaudocc']);$k++){
				
				echo "<input type='hidden' name='recaudocc[]' value='".$_POST['recaudocc'][$k]."'/>";
				echo "<input type='hidden' name='recaudocc1[]' value='".$_POST['recaudocc1'][$k]."'/>";
				echo "<input type='hidden' name='conceptocc[]' value='".$_POST['conceptocc'][$k]."'/>";
				echo "<input type='hidden' name='valtotaltescc[]' value='".$_POST['valtotaltescc'][$k]."'/>";
				echo "<input type='hidden' name='valtotalcontcc[]' value='".$_POST['valtotalcontcc'][$k]."'/>";
				echo "<input type='hidden' name='diferenciacc[]' value='".$_POST['diferenciacc'][$k]."'/>";
				echo "<input type='hidden' name='estilocc[]' value='".$_POST['estilocc'][$k]."'/>";
				echo "<input type='hidden' name='vigenciaComp[]' value='".$_POST['vigenciaComp'][$k]."'/>";
				
				echo"<tr class='$iter' style='text-transform:uppercase;background-color:yellow; ' >
					<td style='width:7%;' id='1'>".$_POST['recaudocc'][$k]."</td>
					<td style='width:32%;' id='2'>".$_POST['conceptocc'][$k]."</td>
					<td style='text-align:right;width:3%;' id='3'>$".number_format($_POST['valtotaltescc'][$k],2,',','.')."</td>
					<td  style='text-align:right;width:4.5%;' id='6'>$".number_format($_POST['valtotalcontcc'][$k],2,',','.')."</td>
					<td  style='text-align:right;width:4.5%;' id='7'>$".number_format($_POST['diferenciacc'][$k],2,',','.')."</td></tr>";
				$aux=$iter;
				$iter=$iter2;
				$iter2=$aux;
				$resultadoSuma=0.0;
				
			}

			echo "</table></tbody></div><script>document.getElementById('divcarga').style.display='none';</script>";
			
			if($_POST['oculto'] == 2){
				$recibos = "";
				$recibosfallidos = "";
				
				$tam = count($_POST['recaudocc1']);
				for($n = 0; $n < $tam; $n++)
				{
					$elimina = "DELETE FROM pptoretencionpago WHERE idrecibo='".$_POST['recaudocc'][$n]."'";
					mysqli_query($linkbd, $elimina);
					$sqlrRetencion = "SELECT TB1.* FROM tesoordenpago_retenciones AS TB1, tesoretenciones AS TB2 WHERE TB1.id_orden='".$_POST['recaudocc1'][$n]."' AND TB1.id_retencion = TB2.id AND TB2.terceros = ''";
					$resRetencion=mysqli_query($linkbd, $sqlrRetencion);
					while ($rowRetencion = mysqli_fetch_row($resRetencion))
					{
						$sqlr="SELECT * FROM tesoretenciones_det,tesoretenciones_det_presu WHERE tesoretenciones_det.id=tesoretenciones_det_presu.id_retencion AND tesoretenciones_det.codigo='".$rowRetencion[0]."'";
						$resdes = mysqli_query($linkbd, $sqlr);
						$valordes = 0;
						while($rowdes = mysqli_fetch_assoc($resdes))
						{
							if($_POST['iva'] > 0 && $rowdes['tercero']==1)
							{
								$valordes=round($rowRetencion[3]*($rowdes['porcentaje']/100),0);
							}
							else
							{
								$valordes=round(($rowRetencion[3])*($rowdes['porcentaje']/100),0);
							}
							if($rowdes['cuentapres']!="")
							{
								$sqlrFuente = "SELECT fuente FROM ccpetinicialing WHERE cuenta = '".$rowdes['cuentapres']."' ORDER BY vigencia DESC LIMIT 1";
								$respFuente = mysqli_query($linkbd, $sqlrFuente);
								$rowFuente = mysqli_fetch_row($respFuente);
								$sql = "insert into pptoretencionpago(cuenta, idrecibo, valor, vigencia, tipo, fuente, seccion_presupuestal, medio_pago, vigencia_gasto) values ('".$rowdes['cuentapres']."', '".$_POST['recaudocc'][$n]."', $valordes,'".$_POST['vigenciaComp'][$n]."', 'egreso', '".$rowFuente[0]."', '16', 'CSF', '1')";
								mysqli_query($linkbd, $sql); 	
							}
						}
					}
					$recibos.=($_POST['recaudocc1'][$n])." ";
				}
				
				echo "<table class='inicio'><tr><td class='saludo1'><center>Se han reflejado los Recibos de Caja $recibos con Exito <img src='imagenes/confirm.png'><script></script></center></td></tr></table>"; 
				echo "<table class='inicio'><tr><td class='saludo1'><center>No se pudieron reflejar los Recibos de Caja $recibosfallidos <img src='imagenes/del.png'><script></script></center></td></tr></table>";					
			}
			?>
        </form> 
	</body>
</html>