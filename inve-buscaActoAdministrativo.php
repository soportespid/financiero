<?php
	header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=utf8");
	require 'comun.inc';
	require 'funciones.inc';
	session_start();
	$linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");
	cargarcodigopag($_GET['codpag'],$_SESSION["nivel"]);
	date_default_timezone_set("America/Bogota");
?>
<!DOCTYPE >
<html lang=es>
	<head>
    <meta charset="utf-8"/>
		<meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1"/>
		<meta name="viewport" content="user-scalable=no">
		<title>:: IDEAL 10 - Almacen</title>
		<link href="favicon.ico" rel="shortcut icon"/>
		<link href="css/css2z.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
		<link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
		<link href="css/css4.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
        <link href="css/tabs.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css"/>
        <link rel="stylesheet" href="sweetalert2/dist/sweetalert2.min.css">
        <script type="text/javascript" src="css/programas.js"></script>
        <script src="sweetalert2/dist/sweetalert2.min.js"></script>
		<script src="vue/vue.min.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script src="css/programas.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script src="css/calendario.js?<?php echo date('d_m_Y_h_i_s');?>"></script>

		<style>
			input[type=number]::-webkit-inner-spin-button,
			input[type=number]::-webkit-outer-spin-button {
				-webkit-appearance: none;
				margin: 0;
			}
			input[type=number]{
				text-align:right;
				box-sizing: border-box;
				font: 120% sans-serif;
				/* width: 100% !important; */
			}
			[v-cloak]{display : none;}

			.centrarSelect {

				height: 30px !important;
				text-align-last:center !important;
			}

			.aumentarTamaño {

				font-size:15px;
			}

            .tamano01 {
                text-align: center !important;
            }

            SELECT{
                font-size:11px;
                font-family: -apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, "Helvetica Neue", Arial, "Noto Sans", sans-serif, "Apple Color Emoji", "Segoe UI Emoji", "Segoe UI Symbol", "Noto Color Emoji";
                font-weight:bold;
                outline:none;
                border-radius:3px;
                border:1px solid rgba(0,0,0, 0.2);
                color:222;
                /* background-color:#B2BFD9; */
            }
            option{
                font-family: -apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, "Helvetica Neue", Arial, "Noto Sans", sans-serif, "Apple Color Emoji", "Segoe UI Emoji", "Segoe UI Symbol", "Noto Color Emoji";
                font-size: 10px;
                color: 333;
                /* background-color:#eee; */
                color:#222;
            }
		</style>
	</head>
	<body>
		<header>
			<table>
				<tr><script>barra_imagenes("inve");</script><?php cuadro_titulos();?></tr>
			</table>
		</header>

		<form name="form2" method="post" action="">
			<section id="myapp" v-cloak >
				<nav>
					<table>
						<tr><?php menu_desplegable("inve");?></tr>
					</table>
                    <div class="bg-white group-btn p-1">
                        <button type="button" v-on:click="location.href='inve-actoAdministrativo'" class="btn btn-white btn-primary-hover d-flex justify-between align-items-center">
                            <span>Nuevo</span>
                            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 -960 960 960">
                                <path d="M440-440H200v-80h240v-240h80v240h240v80H520v240h-80v-240Z"></path>
                            </svg>
                        </button>
                        <button type="button" class="btn btn-white btn-primary-hover d-flex justify-between align-items-center">
                            <span>Guardar</span>
                            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 -960 960 960">
                                <path
                                    d="M840-680v480q0 33-23.5 56.5T760-120H200q-33 0-56.5-23.5T120-200v-560q0-33 23.5-56.5T200-840h480l160 160Zm-80 34L646-760H200v560h560v-446ZM480-240q50 0 85-35t35-85q0-50-35-85t-85-35q-50 0-85 35t-35 85q0 50 35 85t85 35ZM240-560h360v-160H240v160Zm-40-86v446-560 114Z">
                                </path>
                            </svg>
                        </button>
                        <button type="button" v-on:click="location.href='inve-buscaActoAdministrativo'" class="btn btn-white btn-primary-hover d-flex justify-between align-items-center">
                            <span>Buscar</span>
                            <svg class="fill-black group-hover:fill-white w-5 h-5" xmlns="http://www.w3.org/2000/svg" viewBox="0 -960 960 960">
                                <path d="M784-120 532-372q-30 24-69 38t-83 14q-109 0-184.5-75.5T120-580q0-109 75.5-184.5T380-840q109 0 184.5 75.5T640-580q0 44-14 83t-38 69l252 252-56 56ZM380-400q75 0 127.5-52.5T560-580q0-75-52.5-127.5T380-760q-75 0-127.5 52.5T200-580q0 75 52.5 127.5T380-400Z">
                                </path>
                            </svg>
                        </button>
                        <button type="button" @click="mypop=window.open('inve-principal','',''); mypop.focus();" class="btn btn-white btn-primary-hover d-flex justify-between align-items-center">
                            <span>Nueva ventana</span>
                            <svg class="fill-black group-hover:fill-white w-5 h-5" xmlns="http://www.w3.org/2000/svg"
                                viewBox="0 -960 960 960">
                                <path
                                    d="M320-120v-80h80v-80H160q-33 0-56.5-23.5T80-360v-400q0-33 23.5-56.5T160-840h640q33 0 56.5 23.5T880-760v400q0 33-23.5 56.5T800-280H560v80h80v80H320ZM160-360h640v-400H160v400Zm0 0v-400 400Z">
                                </path>
                            </svg>
                        </button>
                        <button type="button" onclick="location.href='inve-menuactos'" class="btn btn-success btn-success-hover d-flex justify-between align-items-center">
                            <span>Atras</span>
                            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 -960 960 960">
                                <path
                                    d="M760-200v-160q0-50-35-85t-85-35H273l144 144-57 56-240-240 240-240 57 56-144 144h367q83 0 141.5 58.5T840-360v160h-80Z">
                                </path>
                            </svg>
                        </button>
                    </div>
				</nav>

				<article>
					<div>
						<table class="inicio ancho">
							<tr>
								<td class="titulos" colspan="8">.: Buscar actos administrativo</td>
                            	<td class="cerrar" style="width:4%" onClick="location.href='inve-principal.php'">Cerrar</td>
							</tr>

                            <tr>
                                <td class="textonew01" style="width:2.5cm;">.: Fecha inicio:</td>
                                <td style="width: 11%;">
                                    <input type="text" name="fechaIni"  value="<?php echo $_POST['fechaIni']?>" onKeyUp="return tabular(event,this)" id="fechaIni" title="DD/MM/YYYY" onclick="displayCalendarFor('fechaIni');" class="colordobleclik" autocomplete="off" onChange=""  readonly>
                                </td>

                                <td class="textonew01" style="width:2.5cm;">.: Fecha final:</td>
                                <td style="width: 11%;">
                                    <input type="text" name="fechaFin"  value="<?php echo $_POST['fechaFin']?>" onKeyUp="return tabular(event,this)" id="fechaFin" title="DD/MM/YYYY" onclick="displayCalendarFor('fechaFin');" class="colordobleclik" autocomplete="off" onChange=""  readonly>
                                </td>

                                <td class="textonew01" style="width:3.5cm;">.: Tipo de acta:</td>
                                <td style="width: 15%;">
                                    <select v-model="tipoActo" style="width: 100%; font-size: 10px; margin-top:4px">
                                        <option value="">Seleccionar tipo de acta adminsitrativa</option>
                                        <option value="104">104 - Acta administrativa por ajuste</option>
                                        <option value="107">107 - Acta administrativa por donación</option>
                                    </select>
                                </td>
                                <td></td>
                                <td style="padding-bottom:0px">
                                    <em class="botonflechaverde" @click="buscarActos">Buscar</em>
                                </td>
                            </tr>
                        </table>

                        <div class="subpantalla" style='height:63vh; width:100%; overflow-x:hidden; resize: vertical;'>
                            <table>
                                <thead>
                                    <tr>
                                        <th width="6%" class="titulosnew00">Tipo de acto</th>
                                        <th width="6%" class="titulosnew00">Consecutivo</th>
                                        <th width="10%" class="titulosnew00">Fecha</th>
                                        <th width="25%" class="titulosnew00">Motivo</th>
                                        <th width="10%" class="titulosnew00">Valor</th>
                                        <th width="6%" class="titulosnew00">Eliminar</th>
                                    </tr>
                                </thead>

                                <tbody>
                                    <tr v-if="listadoActos == 0">
                                        <td colspan="7">
                                            <div style="text-align: center; color:#222; font-size:large" class="h4 text-primary text-center">
                                                Utilice los filtros para buscar información.
                                            </div>
                                        </td>
                                    </tr>

                                    <tr v-for="(actos,index) in listadoActos" v-on:dblclick="visualizar(actos)" v-bind:class="(index % 2 ? 'saludo1a' : 'saludo2')" style="font: 100% sans-serif;">
                                        <td style="text-align:center; width:6%;">{{ actos[0] }}</td>
                                        <td style="text-align:center; width:6%;">{{ actos[1] }}</td>
                                        <td style="text-align:center; width:10%;">{{ actos[2] }}</td>
                                        <td style="text-align:center; width:25%;">{{ actos[3] }}</td>
                                        <td style="text-align:center; width:15%;">{{ formatonumero(actos[4]) }}</td>
                                        <td style="text-align:center; width:6%;"><img src="imagenes/eliminar.png" width="40px" height="40px" @click="eliminar(actos)"></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>

					<div id="cargando" v-if="loading" class="loading">
						<span>Cargando...</span>
					</div>
				</article>
			</section>
		</form>

		<script src="node_modules/read-excel-file/bundle/read-excel-file.min.js"></script>
		<script src="node_modules/xlsx/dist/xlsx.full.min.js"></script>
        <script src="Librerias/vue/vue.min.js"></script>
		<script src="Librerias/vue/axios.min.js"></script>
		<script type="module" src="almacen/actoAdministrativo/buscar/inve-buscaActoAdministrativo.js"></script>

	</body>
</html>
