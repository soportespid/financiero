<?php
header("Cache-control: no-cache, no-store, must-revalidate");
header("Content-Type: text/html;charset=utf8");
require "comun.inc";
require "funciones.inc";
sesion();
$linkbd = conectar_v7();
$linkbd->set_charset("utf8");
cargarcodigopag($_GET["codpag"], $_SESSION["nivel"]);
date_default_timezone_set("America/Bogota");
$scroll = $_GET['scrtop'];
$totreg = $_GET['totreg'];
$idcta = $_GET['idcta'];
$altura = $_GET['altura'];
$filtro = "'" . $_GET['filtro'] . "'";
?>
<!DOCTYPE>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="es">

<head>
    <meta http-equiv="Content-type" content="text/html; charset=utf8" />
    <meta http-equiv="X-UA-Compatible" content="IE=9" />
    <title>:: IDEAL 10 - Parametrización</title>
    <link href="css/css2.css?<?php echo date('d_m_Y_h_i_s'); ?>" rel="stylesheet" type="text/css" />
    <link href="css/css3.css?<?php echo date('d_m_Y_h_i_s'); ?>" rel="stylesheet" type="text/css" />
    <link href="css/css4.css?<?php echo date('d_m_Y_h_i_s'); ?>" rel="stylesheet" type="text/css" />
    <link href="css/style.css?<?php echo date('d_m_Y_h_i_s'); ?>" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="css/programas.js?<?php echo date('d_m_Y_h_i_s'); ?>"></script>
    <script type='text/javascript' src='JQuery/jquery-2.1.4.min.js'></script>
    <script type="text/javascript" src="JQuery/alphanum/jquery.alphanum.js"></script>
    <script>
        function guardar() {
            var validacion01 = document.getElementById('nombre').value;
            if (validacion01.trim() != '') { despliegamodalm('visible', '4', 'Esta Seguro de Modificar', '1'); }
            else { despliegamodalm('visible', '2', 'Falta informacion para modificar el Centro Costo'); }
        }
        function despliegamodalm(_valor, _tip, mensa, pregunta) {
            document.getElementById("bgventanamodalm").style.visibility = _valor;
            if (_valor == "hidden") { document.getElementById('ventanam').src = ""; }
            else {
                switch (_tip) {
                    case "1":
                        document.getElementById('ventanam').src = "ventana-mensaje1.php?titulos=" + mensa; break;
                    case "2":
                        document.getElementById('ventanam').src = "ventana-mensaje3.php?titulos=" + mensa; break;
                    case "3":
                        document.getElementById('ventanam').src = "ventana-mensaje2.php?titulos=" + mensa; break;
                    case "4":
                        document.getElementById('ventanam').src = "ventana-consulta1.php?titulos=" + mensa + "&idresp=" + pregunta; break;
                }
            }
        }
        function funcionmensaje() { }
        function respuestaconsulta(pregunta) {
            switch (pregunta) {
                case "1":
                    document.getElementById('oculto').value = '2';
                    document.form2.submit();
                    break;
            }
        }
        function adelante(scrtop, numpag, limreg, filtro, next) {
            if (parseFloat(document.form2.idcomp.value) < parseFloat(document.form2.maximo.value)) {
                document.getElementById('oculto').value = '1';
                document.getElementById('idcomp').value = next;
                document.getElementById('codigo').value = next;
                var idcta = document.getElementById('codigo').value;
                document.form2.action = "ccp-editaruniejecutora.php?idcta=" + idcta + "&scrtop=" + scrtop + "&numpag=" + numpag + "&limreg=" + limreg + "&filtro=" + filtro;
                document.form2.submit();
            }
        }
        function atrasc(scrtop, numpag, limreg, filtro, prev) {
            document.getElementById('oculto').value = '1';
            document.getElementById('idcomp').value = prev;
            document.getElementById('codigo').value = prev;
            var idcta = document.getElementById('codigo').value;
            document.form2.action = "ccp-editaruniejecutora.php?idcta=" + idcta + "&scrtop=" + scrtop + "&numpag=" + numpag + "&limreg=" + limreg + "&filtro=" + filtro;
            document.form2.submit();
        }
        function iratras(scrtop, numpag, limreg, filtro) {
            var idcta = document.getElementById('codigo').value;
            location.href = "ccp-buscauniejecutora.php?idcta=" + idcta + "&scrtop=" + scrtop + "&numpag=" + numpag + "&limreg=" + limreg + "&filtro=" + filtro;
        }
    </script>
</head>

<body>
    <IFRAME src="alertas.php" name="alertas" id="alertas" style="display:none"></IFRAME>
    <span id="todastablas2"></span>
    <?php
    $numpag = $_GET['numpag'];
    $limreg = $_GET['limreg'];
    $scrtop = 26 * $totreg;
    ?>
    <table>
        <tr>
            <script>barra_imagenes("para");</script><?php cuadro_titulos(); ?>
        </tr>
        <tr><?php menu_desplegable("para"); ?></tr>
    </table>
    <div class="bg-white group-btn p-1"><button type="button" onclick="location.href='ccp-uniejecutora.php'"
            class="btn btn-white btn-primary-hover d-flex justify-between align-items-center">
            <span>Nuevo</span>
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 -960 960 960">
                <path d="M440-440H200v-80h240v-240h80v240h240v80H520v240h-80v-240Z"></path>
            </svg>
        </button><button type="button" onclick="guardar();"
            class="btn btn-white btn-primary-hover d-flex justify-between align-items-center">
            <span>Guardar</span>
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 -960 960 960">
                <path
                    d="M840-680v480q0 33-23.5 56.5T760-120H200q-33 0-56.5-23.5T120-200v-560q0-33 23.5-56.5T200-840h480l160 160Zm-80 34L646-760H200v560h560v-446ZM480-240q50 0 85-35t35-85q0-50-35-85t-85-35q-50 0-85 35t-35 85q0 50 35 85t85 35ZM240-560h360v-160H240v160Zm-40-86v446-560 114Z">
                </path>
            </svg>
        </button><button type="button" onclick="location.href='ccp-buscauniejecutora.php'"
            class="btn btn-white btn-primary-hover d-flex justify-between align-items-center">
            <span>Buscar</span>
            <svg class="fill-black group-hover:fill-white w-5 h-5" xmlns="http://www.w3.org/2000/svg"
                viewBox="0 -960 960 960">
                <path
                    d="M784-120 532-372q-30 24-69 38t-83 14q-109 0-184.5-75.5T120-580q0-109 75.5-184.5T380-840q109 0 184.5 75.5T640-580q0 44-14 83t-38 69l252 252-56 56ZM380-400q75 0 127.5-52.5T560-580q0-75-52.5-127.5T380-760q-75 0-127.5 52.5T200-580q0 75 52.5 127.5T380-400Z">
                </path>
            </svg>
        </button><button type="button" onclick="window.open('para-principal');"
            class="btn btn-white btn-primary-hover d-flex justify-between align-items-center">
            <span>Nueva ventana</span>
            <svg class="fill-black group-hover:fill-white w-5 h-5" xmlns="http://www.w3.org/2000/svg"
                viewBox="0 -960 960 960">
                <path
                    d="M320-120v-80h80v-80H160q-33 0-56.5-23.5T80-360v-400q0-33 23.5-56.5T160-840h640q33 0 56.5 23.5T880-760v400q0 33-23.5 56.5T800-280H560v80h80v80H320ZM160-360h640v-400H160v400Zm0 0v-400 400Z">
                </path>
            </svg>
        </button><button type="button" onclick="iratras(26, 1, 10, '')"
            class="btn btn-success btn-success-hover d-flex justify-between align-items-center">
            <span>Atras</span>
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 -960 960 960">
                <path
                    d="M760-200v-160q0-50-35-85t-85-35H273l144 144-57 56-240-240 240-240 57 56-144 144h367q83 0 141.5 58.5T840-360v160h-80Z">
                </path>
            </svg>
        </button></div>
    <div id="bgventanamodalm" class="bgventanamodalm">
        <div id="ventanamodalm" class="ventanamodalm">
            <IFRAME src="" name="ventanam" marginWidth=0 marginHeight=0 frameBorder=0 id="ventanam" frameSpacing=0
                style=" width:700px; height:130px; top:200; overflow:hidden;">
            </IFRAME>
        </div>
    </div>
    <form name="form2" method="post" action="">
        <?php
        if ($_GET['idtipocom'] != "") {
            echo "<script>document.getElementById('codrec').value='" . $_GET['idtipocom'] . "';</script>";
        }
        $sqlr = "SELECT * FROM pptouniejecu ORDER BY id_cc DESC";
        $res = mysqli_query($linkbd, $sqlr);
        $r = mysqli_fetch_row($res);
        $_POST['maximo'] = $r[0];
        if (!$_POST['oculto']) {
            if ($_POST['codrec'] != "" || $_GET['idtipocom'] != "") {
                if ($_POST['codrec'] != "") {
                    $sqlr = "SELECT * FROM pptouniejecu WHERE id_cc='" . $_POST['codrec'] . "'";
                } else {
                    $sqlr = "SELECT * FROM pptouniejecu WHERE id_cc ='" . $_GET['idtipocom'] . "'";
                }
            } else {
                $sqlr = "SELECT * FROM pptouniejecu ORDER BY id_cc DESC";
            }
            $res = mysqli_query($linkbd, $sqlr);
            $row = mysqli_fetch_row($res);
            $_POST['idcomp'] = $row[0];
        }
        if ($_POST['oculto'] != "2") {
            $sqlr = "SELECT * FROM pptouniejecu WHERE id_cc='" . $_POST['idcomp'] . "'";
            $res = mysqli_query($linkbd, $sqlr);
            while ($row = mysqli_fetch_row($res)) {
                $_POST['idcomp'] = $row[0];
                $_POST['nombre'] = $row[1];
                $_POST['estado'] = $row[2];
                $_POST['tipo'] = $row[3];
                $_POST['codigo'] = $row[0];
            }
        }
        //NEXT
        $sqln = "SELECT * FROM pptouniejecu WHERE id_cc > '" . $_POST['idcomp'] . "' ORDER BY id_cc ASC LIMIT 1";
        $resn = mysqli_query($linkbd, $sqln);
        $row = mysqli_fetch_row($resn);
        $next = $row[0];
        //PREV
        $sqlp = "SELECT * FROM pptouniejecu WHERE id_cc < '" . $_POST['idcomp'] . "' ORDER BY id_cc DESC LIMIT 1";
        $resp = mysqli_query($linkbd, $sqlp);
        $row = mysqli_fetch_row($resp);
        $prev = $row[0];
        ?>
        <table class="inicio" align="center">
            <tr>
                <td class="titulos" colspan="6">.: Editar Unidad Ejecutora</td>
                <td class="cerrar" style="width:7%" onClick="location.href='ccp-principal.php'">Cerrar</td>
            </tr>
            <tr>
                <td class="tamano01" style="width:8%;">C&oacute;digo:</td>
                <td style="width:13%;">
                    <img src="imagenes/back.png" title="anterior"
                        onClick="atrasc(<?php echo "$scrtop,$numpag,$limreg,$filtro,$prev"; ?>)" class="icobut" />
                    &nbsp;<input type="text" name="codigo" id="codigo" value="<?php echo $_POST['codigo'] ?>"
                        style="width:50%; height:30px;" maxlength="2" readonly />&nbsp;<img src="imagenes/next.png"
                        title="siguiente" onClick="adelante(<?php echo "$scrtop,$numpag,$limreg,$filtro,$next"; ?>)"
                        class="icobut" />
                    <input type="hidden" value="<?php echo $_POST['maximo'] ?>" name="maximo">
                    <input type="hidden" value="<?php echo $_POST['codrec'] ?>" name="codrec" id="codrec">
                </td>
                <td class="tamano01" style="width:8%;">Nombre:</td>
                <td><input type="text" name="nombre" id="nombre" value="<?php echo $_POST['nombre'] ?>"
                        style="width:80%;height:30px;" onKeyUp="return tabular(event,this)" /></td>
            </tr>
            <tr>
                <td class="tamano01">Ingresos :</td>
                <td>
                    <select name="tipo" id="tipo" style="width:50%;height:30px;">
                        <option value="S" <?php if ($_POST['tipo'] == 'S')
                            echo "selected" ?>>SI</option>
                            <option value="N" <?php if ($_POST['tipo'] == 'N')
                            echo "selected" ?>>NO</option>
                        </select>
                    </td>
                    <td class="tamano01">Activo:</td>
                    <td>
                        <select name="estado" id="estado" style="width:8%;height:30px;">
                            <option value="S" <?php if ($_POST['estado'] == 'S')
                            echo "selected" ?>>SI</option>
                            <option value="N" <?php if ($_POST['estado'] == 'N')
                            echo "selected" ?>>NO</option>
                        </select>
                    </td>
                </tr>
            </table>
            <input type="hidden" name="oculto" id="oculto" value="1" />
            <input type="hidden" name="idcomp" id="idcomp" value="<?php echo $_POST['idcomp'] ?>" />
        <?php
        if ($_POST['oculto'] == "2") {
            $sqlr = "UPDATE pptouniejecu SET nombre='" . $_POST['nombre'] . "',estado='" . $_POST['estado'] . "',entidad='" . $_POST['tipo'] . "' WHERE id_cc='" . $_POST['idcomp'] . "'";
            if (!mysqli_query($linkbd, $sqlr)) {
                echo "<script>despliegamodalm('visible','3','No se pudo ejecutar la petici�n');</script>";
            } else {
                echo "<script>despliegamodalm('visible','3','Se ha modificado con Exito la Unidad Ejecutora');</script>";
            }
        }
        ?>
        <script type="text/javascript">$('#nombre').alphanum({ allow: '' });</script>
        <script
            type="text/javascript">$('#codigo').numeric({ allowThouSep: false, allowDecSep: false, allowMinus: false, maxDigits: 2 });</script>
    </form>
</body>

</html>
