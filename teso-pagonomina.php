<?php
	header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=utf8");
	require "comun.inc";
	require "funciones.inc";
	require "conversor.php";
	require "validaciones.inc";
	require 'funcionesnomima.inc.php';
	session_start();
	$linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");
	cargarcodigopag($_GET['codpag'],$_SESSION["nivel"]);
	date_default_timezone_set("America/Bogota");
?>
<!DOCTYPE >
<html lang="es">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1">
		<meta name="viewport" content="user-scalable=no">
		<title>:: IDEAL 10 - Tesorer&iacute;a</title>
		<link href="favicon.ico" rel="shortcut icon"/>
		<link href="css/css2.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css">
		<link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css">
		<link href="css/css4.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css">
		<link href="css/tabs.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css">
		<link href="sweetalert2/dist/sweetalert2.min.css" rel="stylesheet">
		<script src="sweetalert2/dist/sweetalert2.min.js"></script>
		<script type="text/javascript" src="css/calendario.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script type="text/javascript" src="css/programas.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script>
			function buscacta(e){
				if (document.form2.cuenta.value!=""){
					document.form2.bc.value='1';document.form2.submit();
				}
			}
			function validar(){
				document.form2.submit();
			}
			function buscaop(e){
				if (document.form2.orden.value!=""){
					document.form2.bop.value='1';
					document.form2.submit();
				}
			}
			function agregardetalle(){
				if(document.form2.numero.value!="" &&  document.form2.valor.value>0 &&  document.form2.banco.value!=""  ){ 
					document.form2.agregadet.value=1;
					document.form2.submit();
				}
				else{
					alert("Falta informacion para poder Agregar");
				}
			}
			function agregardetalled(){
				if(document.form2.retencion.value!="" &&  document.form2.vporcentaje.value!=""){ 
					document.form2.agregadetdes.value=1;
					document.form2.submit();
				}
				else{
					alert("Falta informacion para poder Agregar");
				}
			}
			function marcar(indice,posicion){
				varcheck=document.getElementsByName('conciliados[]');
				vartipo=document.getElementsByName('tdet[]');
				tipo=vartipo.item(posicion-1).value;
				if(tipo=="N" || tipo=="PN"){
					document.getElementById('tipoegreso').value="Nomina";
					for (x=0;x<varcheck.length;x++){
						if((x+1)!=posicion){
							varcheck.item(x).checked=false;
						}
					}
				}
				else{
					document.getElementById('tipoegreso').value="Otros";
					for (x=0;x<varcheck.length;x++){
						if(vartipo.item(x).value=="N"){
							varcheck.item(x).checked=false;
						}
					}
				}
				if(tipo=="D"){
					for (x=0;x<varcheck.length;x++){
						if(vartipo.item(x).value!="D"){
							varcheck.item(x).checked=false;
						}
					}
				}
				else{
					for (x=0;x<varcheck.length;x++){
						if(vartipo.item(x).value=="D"){
							varcheck.item(x).checked=false;
						}
					}
				}
				vtabla=document.getElementById('filas'+indice);
				clase=vtabla.className;
				if(varcheck.item(posicion).checked){
					vtabla.style.backgroundColor='#3399bb';
				}
				else{
					e=varcheck.item(posicion).value;
					document.getElementById('filas'+posicion).style.backgroundColor='#ffffff';
				}
				sumarconc();
			}
			function seleplanilla(){
				varcheck=document.getElementsByName('conciliados[]');
				vartipo=document.getElementsByName('tdet[]');
				for (x=0;x<varcheck.length;x++){
					if(vartipo.item(x).value=="F" || vartipo.item(x).value=="SR" || vartipo.item(x).value=="SE" || vartipo.item(x).value=="PR" || vartipo.item(x).value=="PE" || vartipo.item(x).value=="FS"){
						varcheck.item(x).checked=true;
					}
					else{
						varcheck.item(x).checked=false;
					}
				}
				sumarconc();
			}
			function limpiarsele(){
				varcheck=document.getElementsByName('conciliados[]');
				for (x=0;x<varcheck.length;x++){
					varcheck.item(x).checked=false;
				}
			}
			function buscater(e){
				if (document.form2.tercero.value!=""){
					document.form2.bt.value='1';document.form2.submit();
				}
			}
			function sumarconc(){
				alert(' este '+indice+' ch '+indice);
				vvigencias=document.getElementsByName('conciliados[]');
				valores=document.getElementsByName('dvalores[]');
				alert(' este '+indice+' ch '+posicion);
				sumacd=0;
				sumancd=0;
				sumacc=0;
				sumancc=0;
				sumach=0;
				for (x=0;x<vvigencias.length;x++){if(vvigencias.item(x).checked){
					sumacd=sumacd+parseFloat(valores.item(x).value);
					sumach=sumach+1;}
				}
				saldofinal=parseFloat(document.form2.valororden.value); 
				document.form2.valorpagar.value=sumacd;
				//document.form2.saldopagar.value=saldofinal-sumacd;
				if(sumach>1){
					document.form2.tercero.value='';document.form2.ntercero.value='';
				}
			}
			function eliminar(variable){
				if (confirm("Esta Seguro de Eliminar")){
					document.form2.elimina.value=variable;
					vvend=document.getElementById('elimina');
					vvend.value=variable;
					document.form2.submit();
				}
			}
			function eliminard(variable){
				if (confirm("Esta Seguro de Eliminar")){
					document.form2.eliminad.value=variable;
					vvend=document.getElementById('eliminad');
					vvend.value=variable;
					document.form2.submit();
				}
			}
			function guardar(){
				if(document.form2.modopago.value == 'CSF'){
					if (document.form2.fecha.value!='' && document.form2.tipop.value!='' && document.form2.banco.value!='' && document.form2.tercero.value!=''){
						despliegamodalm('visible','4','Esta Seguro de Guardar','1');
					}else {
						despliegamodalm('visible','2','Faltan datos para completar el registro');
					}
				}else{
					if (document.form2.fecha.value!='' && document.form2.tercero.value!=''){
						despliegamodalm('visible','4','Esta Seguro de Guardar','1');
					}else {
						despliegamodalm('visible','2','Faltan datos para completar el registro');
					}
				}
				
			}
			function calcularpago(){
				sumacd=0;
				valores=document.getElementsByName('devalores[]');
				for (x=0;x<valores.length;x++){sumacd=sumacd+parseFloat(valores.item(x).value);}
				document.form2.valorpagar.value=sumacd;
			}
			function pdf(){
				if(document.getElementById('tipoegreso').value=="Nomina") {document.form2.action="pdfegresonominaemp.php";}
				else {document.form2.action="pdfegresonomina.php";}
				document.form2.target="_BLANK";
				document.form2.submit(); 
				document.form2.action="";
				document.form2.target="";
			}
			function agregaregreso(){
				document.form2.agregadetegre.value=1;
				document.form2.tabgroup1.value=3;
				document.form2.submit();
			}
			function despliegamodal2(_valor,scr){
				let url = '';
				switch(scr){
					case "1": url = "cuentasbancarias-ventana02.php?tipoc=D&obj01=banco&obj02=nbanco&obj03=&obj04=cb&obj05=ter";break;
					case "2": url = "cuentasbancarias-ventana02.php?tipoc=C&obj01=banco&obj02=nbanco&obj03=&obj04=cb&obj05=ter";break;
					case "3": url = "tercerosgral-ventana01.php?objeto=tercero&nobjeto=ntercero&nfoco=cc";break;
					case "4": url = "mediosdepago-ventana01.php?tipo=CSF";break;
					case "5": url = "mediosdepago-ventana01.php?tipo=SSF";break;
				}
				document.getElementById("bgventanamodal2").style.visibility=_valor;
				if(_valor=="hidden"){document.getElementById('ventana2').src="";}
				else {document.getElementById('ventana2').src=url;}
			}
			function despliegamodalm(_valor,_tip,mensa,pregunta){
				document.getElementById("bgventanamodalm").style.visibility=_valor;
				if(_valor=="hidden") {document.getElementById('ventanam').src="";}
				else{
					switch(_tip){
						case "1":	document.getElementById('ventanam').src="ventana-mensaje1.php?titulos="+mensa;break;
						case "2":	document.getElementById('ventanam').src="ventana-mensaje3.php?titulos="+mensa;break;
						case "3":	document.getElementById('ventanam').src="ventana-mensaje2.php?titulos="+mensa;break;
						case "4":	document.getElementById('ventanam').src="ventana-consulta1.php?titulos="+mensa+"&idresp="+pregunta;break;	
					}
				}
			}
			function respuestaconsulta(pregunta){
				switch(pregunta){
					case "1":	document.form2.oculto.value=2;document.form2.submit();break;
					case "2":	break
				}
			}
			function funcionmensaje(){
				//var codi = document.form2.egreso.value;
				var codi =document.getElementById('egreso').value;
				document.location.href = "teso-pagonominaver.php?idegre=" + codi;
			}			
		</script>
		<?php 
			titlepag();
			function buscacuentaprespn($cuenta,$vigencia){
				$linkbd = conectar_v7();
				$linkbd -> set_charset("utf8");
				$sqlr = "SELECT * FROM pptocuentas WHERE cuenta = '$cuenta' AND (vigencia = '$vigencia' or vigenciaf = '$vigencia')";
				$res = mysqli_query($linkbd,$sqlr);
				while($r = mysqli_fetch_row($res)){
					$valor2 = $r[1];
				}
				return $valor2;
			}
		?>
	</head>
	<body>
		<IFRAME src="alertas.php" name="alertas" id="alertas" style="display:none"></IFRAME>
		<span id="todastablas2"></span>
		<table>
			<tr><script>barra_imagenes("teso");</script><?php cuadro_titulos();?></tr>
			<tr><?php menu_desplegable("teso");?></tr>
			<tr>
				<td colspan="3" class="cinta">
					<img src="imagenes/add.png" title="Nuevo" onClick="location.href='teso-pagonomina.php'" class="mgbt">
					<img src="imagenes/guarda.png" title="Guardar" onClick="guardar()" class="mgbt">
					<img src="imagenes/busca.png" title="Buscar" onClick="location.href='teso-buscapagonomina.php'" class="mgbt">
					<img src="imagenes/agenda1.png" title="Agenda" onClick="mypop=window.open('plan-agenda.php','','');mypop.focus()" class="mgbt">
					<img src="imagenes/nv.png" title="Nueva ventana" onClick="mypop=window.open('teso-principal.php','','');mypop.focus();" class="mgbt">
					<img src="imagenes/duplicar_pantalla.png" title="Duplicar pesta&ntilde;a" onClick="mypop=window.open('<?php echo $url2; ?>','','');mypop.focus();" class="mgbt">
				</td>
			</tr>
		</table>
		<div id="bgventanamodalm" class="bgventanamodalm">
			<div id="ventanamodalm" class="ventanamodalm">
				<IFRAME src="" name="ventanam" marginWidth=0 marginHeight=0 frameBorder=0 id="ventanam" frameSpacing=0 style=" width:700px; height:130px; top:200; overflow:hidden;">
				</IFRAME>
			</div>
		</div>
		<form name="form2" method="post" action="">
			<?php
				$sqlr="SELECT sueldo, cajacompensacion, icbf, sena, iti, esap, arp, salud_empleador, salud_empleado, pension_empleador, pension_empleado, sub_alimentacion, aux_transporte, prima_navidad FROM humparametrosliquida";
				$resp = mysqli_query($linkbd,$sqlr);
				while ($row =mysqli_fetch_row($resp)){
					$_POST['salmin'] = $row[0];
					$_POST['tprimanav'] = $row[13];
					$_POST['cajacomp'] = $row[1];
					$_POST['icbf'] = $row[2];
					$_POST['sena'] = $row[3];
					$_POST['iti'] = $row[4];
					$_POST['esap'] = $row[5];
					$_POST['arp'] = $row[6];
					$_POST['salud_empleador'] = $row[7];
					$_POST['salud_empleado'] = $row[8];
					$_POST['pension_empleador'] = $row[9];
					$_POST['pension_empleado'] = $row[10];
					$_POST['auxtran'] = $row[12];
					$_POST['auxalim'] = $row[11];
				}
				//*********** cuenta origen va al credito y la destino al debito
				if(!$_POST['oculto']){
					$check1 = "checked";
					$fec = date("d/m/Y");
					$_POST['fecha'] = $fec;
					$_POST['vigencia'] = '';
					$_POST['egreso'] = selconsecutivo('tesoegresosnomina','id_egreso');
					$_POST['tempdoc'] = "";
					$_POST['contadoc'] = 0;
					$_POST['tipoegreso'] = "";
					$_POST['valtipos'] = 0;
					if($_GET['nomina'] != ''){
						$_POST['orden'] = $_GET['nomina'];
						$_POST['rp'] = $_GET['varp'];
						$_POST['tabgroup1'] = 2;
						$sqlr = "SELECT vigencia FROM hum_nom_cdp_rp WHERE nomina='".$_GET['nomina']."'";
						$res = mysqli_query($linkbd,$sqlr);
						$row = mysqli_fetch_row($res);
						$_POST['vigencia'] = $row[0];
					}
					$vigencia = $vigusu = $_POST['vigencia'];
					$_POST['modopago'] = 'CSF';
				}			
				switch($_POST['tabgroup1']){
					case 1:	$check1='checked';break;
					case 2:	$check2='checked';break;
					case 3:	$check3='checked';
				}
			?>
			<input type="hidden" id="cajacomp" name="cajacomp" value="<?php echo $_POST['cajacomp']?>"/>
			<input type="hidden" id="icbf" name="icbf" value="<?php echo $_POST['icbf']?>"/>
			<input type="hidden" id="sena" name="sena" value="<?php echo $_POST['sena']?>"/>
			<input type="hidden" id="esap" name="esap" value="<?php echo $_POST['esap']?>"/>
			<input type="hidden" id="iti" name="iti" value="<?php echo $_POST['iti']?>"/>
			<input type="hidden" id="arp" name="arp" value="<?php echo $_POST['arp']?>"/>
			<input type="hidden" id="salud_empleador" name="salud_empleador" value="<?php echo $_POST['salud_empleador']?>"/>
			<input type="hidden" id="salud_empleado" name="salud_empleado" value="<?php echo $_POST['salud_empleado']?>"/>
			<input type="hidden" id="transp" name="transp" value="<?php echo $_POST['transp']?>"/>
			<input type="hidden" id="pension_empleador" name="pension_empleador" value="<?php echo $_POST['pension_empleador']?>"/>
			<input type="hidden" id="pension_empleado" name="pension_empleado" value="<?php echo $_POST['pension_empleado']?>"/>
			<input type="hidden" id="salmin" name="salmin" value="<?php echo $_POST['salmin']?>"/> 
			<input type="hidden" id="tprimanav" name="tprimanav" value="<?php echo $_POST['tprimanav']?>"/>
			<input type="hidden" id="auxalim" name="auxalim" value="<?php echo $_POST['auxalim']?>"/>
			<input type="hidden" id="auxtran" name="auxtran" value="<?php echo $_POST['auxtran']?>"/>
			<!-- Variables tipos procesos adicionales --> 
			<input type="hidden" name="tipo01" id="tipo01" value="<?php echo $_POST['tipo01'];?>"/>
			<input type="hidden" name="tipo02" id="tipo02" value="<?php echo $_POST['tipo02'];?>"/>
			<?php
				//***** busca tercero
				if($_POST['bt'] == '1'){
					$nresul = buscatercero($_POST['tercero']);
					if($nresul != ''){
						$_POST['ntercero'] = $nresul;
					}
					else{
						$_POST['ntercero'] = "";
					}
				}
				//Inicio Guardar
				if($_POST['oculto'] == '2'){
					preg_match("/([0-9]{2})\/([0-9]{2})\/([0-9]{4})/", $_POST['fecha'],$fecha);
					$fechaf = "$fecha[3]-$fecha[2]-$fecha[1]";
					$fechamax = "$fecha[3]$fecha[2]$fecha[1]";
					$vigencia = $fecha[3];
					$bloq = bloqueos($_SESSION['cedulausu'],$fechaf);
					if($bloq >= 1){
						$sqlblfecha = "SELECT fecha FROM tesoegresosnomina WHERE tipo_mov = '201' AND estado = 'S' ORDER BY id_egreso DESC LIMIT 1";
						$resblfecha = mysqli_query($linkbd,$sqlblfecha);
						$rowblfecha = mysqli_fetch_row($resblfecha);
						$fechamin = date('Ymd',strtotime($rowblfecha[0]));
						if ((int)$fechamax >= (int)$fechamin){
							//************CREACION DEL COMPROBANTE CONTABLE ************************
							$sqlr = "SELECT count(*) FROM tesoegresosnomina WHERE id_egreso = ".$_POST['egreso']." AND estado = 'S'";
							$res = mysqli_query($linkbd,$sqlr );
							$row = mysqli_fetch_row($res);
							$nreg = $row[0];
							if ($nreg == 0){
								$debitos = array_sum($_POST['devalores']);
								if($_POST['tipop'] == 'cheque'){
									$sqlr="INSERT INTO tesoegresosnomina (id_orden, fecha, vigencia, valortotal, retenciones, valorpago, concepto, banco, cheque, tercero, cuentabanco, estado, pago) VALUES ('".$_POST['orden']."', '$fechaf', '".$_POST['vigencia']."', '".$_POST['valorpagar']."', 0, '".$_POST['valorpagar']."', '".$_POST['concepto']."', '".$_POST['banco']."', '".$_POST['ncheque']."', '".$_POST['tercero']."', '".$_POST['cb']."', 'S', '".$_POST['tipop']."')";
								}else{
									$sqlr="INSERT INTO tesoegresosnomina (id_orden, fecha, vigencia, valortotal, retenciones, valorpago, concepto, banco, cheque, tercero, cuentabanco, estado, pago) VALUES ('".$_POST['orden']."', '$fechaf', '".$_POST['vigencia']."', '".$_POST['valorpagar']."', 0, '".$_POST['valorpagar']."', '".$_POST['concepto']."', '".$_POST['banco']."', '".$_POST['ntransfe']."', '".$_POST['tercero']."', '".$_POST['cb']."', 'S', '".$_POST['tipop']."')";
								}
								if (!mysqli_query($linkbd,$sqlr)){
									echo "<table class='inicio'><tr><td class='saludo1'><center><font color=blue><img src='imagenes\alert.png'>Manejador de Errores de la Clase BD<br><font size=1></font></font><br><p align=center>No se pudo ejecutar la petición: <br><font color=red><b>$sqlr</b></font></p>";
									echo "Ocurrió el siguiente problema:<br>";
									echo "<pre>";
									echo "</pre></center></td></tr></table>";
								}else{
									$ideg = $_POST['egreso'];
									$sqlr = "SELECT fecha FROM humnomina WHERE id_nom='".$_POST['orden']."'";
									$resp = mysqli_query($linkbd,$sqlr);
									$row = mysqli_fetch_row($resp);
									$fechacuantas = $row[0];
									$sqlr="UPDATE tesoegresosnominachequeras SET consecutivo = consecutivo + 1 WHERE cuentabancaria = '".$_POST['cb']."' AND estado = 'S'";
									mysqli_query($linkbd,$sqlr);
									
									$sqlr="INSERT INTO tesoegresosnomina_cheque (id_cheque, id_egreso, estado, motivo) VALUES ('".$_POST['ncheque']."', $ideg, 'S', '')";
									mysqli_query($linkbd,$sqlr);
									
									for($y=0;$y<count($_POST['decuentas']);$y++){
										$sqlr = "INSERT INTO tesoegresosnomina_det (id_egreso, id_orden, tipo, tercero, ntercero_det, cuentap, cc, valor, estado, ndes, valordevengado, fuente, indicador_producto, bpin, medio_pago, codigo_politicap, tipo_mov, proyecto, idborrar, cod_fun, seccion_presupuestal, vigencia_gasto) VALUES ('$ideg', '".$_POST['orden']."', '".$_POST['tedet'][$y]."', '".$_POST['decuentas'][$y]."', '".$_POST['dencuentas'][$y]."', '".$_POST['derecursos'][$y]."', '".$_POST['deccs'][$y]."', '".$_POST['devalores'][$y]."', 'S', '".$_POST['idedescuento'][$y]."', '".$_POST['salariodevengado'][$y]."', '".$_POST['codfuente'][$y]."', '".$_POST['codprogramatico'][$y]."', '".$_POST['codbpin'][$y]."', 'CSF', '1', '201', '".$_POST['codproyecto'][$y]."', '".$_POST['codborrar'][$y]."','".$_POST['codigofuncionario'][$y]."','".$_POST['codsecpresupuestal'][$y]."','1')";
										mysqli_query($linkbd,$sqlr);
									}
									//***********crear el contabilidad
									
									$sqlr="INSERT INTO comprobante_cab (numerotipo, tipo_comp, fecha, concepto, total, total_debito, total_credito, diferencia, estado) VALUES ('$ideg', '17', '$fechaf', '".$_POST['concepto']."',0, '".$_POST['valorpagar']."','".$_POST['valorpagar']."',0,'1')";
									mysqli_query($linkbd,$sqlr);
									$idcomp = mysqli_insert_id($linkbd);
									for($y=0;$y<count($_POST['decuentas']);$y++){
										switch($_POST['tedet'][$y]){
											case '':	break;
											//**** Tipo SR: Pago Salud Empresa ****
											case 'SR':{
												$ctaconcepto = cuentascontablesccpet::cuentadebito_egreso($_POST['derecursos'][$y], $_POST['deccs'][$y], $fechaf, '');
												//Cuanta Origen
												$sqlr = "INSERT INTO comprobante_det (id_comp, cuenta, tercero, centrocosto, detalle, cheque, valdebito, valcredito, estado, vigencia) VALUES ('17 $ideg', '$ctaconcepto', '".$_POST['tercero']."', '".$_POST['deccs'][$y]."', 'Pago Salud Empleador', '', ".$_POST['devalores'][$y].", 0, '1', '".$_POST['vigencia']."')";
												mysqli_query($linkbd,$sqlr);
												//**Cuenta Banco
												$sqlr = "INSERT INTO comprobante_det (id_comp, cuenta, tercero, centrocosto, detalle, cheque, valdebito, valcredito, estado, vigencia) VALUES ('17 $ideg', '".$_POST['banco']."', '".$_POST['tercero']."', '".$_POST['deccs'][$y]."', 'Pago Salud Empleador Banco', '".$_POST['cheque']." ".$_POST['ntransfe']."', 0,".$_POST['devalores'][$y].", '1', '".$_POST['vigencia']."')";
												mysqli_query($linkbd,$sqlr);
												//Desactivar Pago
												$sqlr = "UPDATE humnomina_saludpension SET estado = 'P' WHERE tercero = '".$_POST['decuentas'][$y]."' AND id_nom = '".$_POST['orden']."' AND cc = '".$_POST['deccs'][$y]."' AND tipo = 'SR'";
												mysqli_query($linkbd,$sqlr);
											}break;
											//**** Tipo SE: Pago Salud Empleado ****
											case 'SE':{
												$ctaconcepto = cuentascontablesccpet::cuentadebito_egreso($_POST['derecursos'][$y], $_POST['deccs'][$y], $fechaf, $_POST['salud_empleado']);
												//Cuenta Origen
												$sqlr = "INSERT INTO comprobante_det (id_comp, cuenta, tercero, centrocosto, detalle, cheque, valdebito, valcredito, estado, vigencia) VALUES ('17 $ideg', '$ctaconcepto', '".$_POST['tercero']."', '".$_POST['deccs'][$y]."', 'Pago Salud Empleado', '', ".$_POST['devalores'][$y].", 0, '1', '".$_POST['vigencia']."')";
												mysqli_query($linkbd,$sqlr);
												//Cuenta Banco
												$sqlr = "INSERT INTO comprobante_det (id_comp, cuenta, tercero, centrocosto, detalle, cheque, valdebito, valcredito, estado, vigencia) VALUES ('17 $ideg', '".$_POST['banco']."', '".$_POST['tercero']."', '".$_POST['deccs'][$y]."', 'Pago Salud Empleado Banco', '".$_POST['cheque']." ".$_POST['ntransfe']."', 0, ".$_POST['devalores'][$y].", '1' ,'".$_POST['vigencia']."')";
												mysqli_query($linkbd,$sqlr);
												//Desactivar Pago
												$sqlr = "UPDATE humnomina_saludpension SET estado = 'P' WHERE tercero = '".$_POST['decuentas'][$y]."' AND id_nom = '".$_POST['orden']."' AND cc = '".$_POST['deccs'][$y]."' AND tipo = 'SE'";
												mysqli_query($linkbd,$sqlr);
											}break;
											//**** Tipo PR: Pago Pension Empresa ****
											case 'PR':{
												$ctaconcepto = cuentascontablesccpet::cuentadebito_egreso($_POST['derecursos'][$y],$_POST['deccs'][$y],$fechaf, $_POST['pension_empleador']);
												//Cuenta Origen
												$sqlr = "INSERT INTO comprobante_det (id_comp, cuenta, tercero, centrocosto, detalle, cheque, valdebito, valcredito, estado, vigencia) VALUES ('17 $ideg', '$ctaconcepto', '".$_POST['tercero']."', '".$_POST['deccs'][$y]."', 'Pago Pension Empleador', '', ".$_POST['devalores'][$y].", 0, '1', '".$_POST['vigencia']."')";
												mysqli_query($linkbd,$sqlr);
												//Cuenta Banco
												$sqlr = "INSERT INTO comprobante_det (id_comp, cuenta, tercero, centrocosto, detalle, cheque, valdebito, valcredito, estado, vigencia) VALUES ('17 $ideg', '".$_POST['banco']."', '".$_POST['tercero']."' ,'".$_POST['deccs'][$y]."','Pago Pension Empleador Banco', '".$_POST['cheque']." ".$_POST['ntransfe']."', 0, ".$_POST['devalores'][$y].", '1' ,'".$_POST['vigencia']."')";
												mysqli_query($linkbd,$sqlr);
												//Desactivar Pago
												$sqlr = "UPDATE humnomina_saludpension SET estado = 'P' WHERE tercero = '".$_POST['decuentas'][$y]."' AND id_nom = '".$_POST['orden']."' AND cc = '".$_POST['deccs'][$y]."' AND tipo = 'PR'";
												mysqli_query($linkbd,$sqlr);
											}break;
											//**** Tipo PE: Pago Pension Empleado ****
											case 'PE':{ 
												$ctaconcepto = cuentascontablesccpet::cuentadebito_egreso($_POST['derecursos'][$y],$_POST['deccs'][$y],$fechaf,$_POST['pension_empleado']);
												//Cuenta Origen
												$sqlr = "INSERT INTO comprobante_det (id_comp, cuenta, tercero, centrocosto, detalle, cheque, valdebito, valcredito, estado, vigencia) VALUES ('17 $ideg', '$ctaconcepto', '".$_POST['tercero']."', '".$_POST['deccs'][$y]."', 'Pago Pension Empleado','', ".$_POST['devalores'][$y].", 0, '1', '".$_POST['vigencia']."')";
												mysqli_query($linkbd,$sqlr);
												//Cuenta Banco
												$sqlr = "INSERT INTO comprobante_det (id_comp, cuenta, tercero, centrocosto, detalle, cheque, valdebito, valcredito, estado, vigencia) VALUES ('17 $ideg', '".$_POST['banco']."','".$_POST['tercero']."' ,'".$_POST['deccs'][$y]."','Pago Pension Empleado Banco', '".$_POST['cheque']." ".$_POST['ntransfe']."', 0, ".$_POST['devalores'][$y].", '1', '".$_POST['vigencia']."')";
												mysqli_query($linkbd,$sqlr);
												//Desactivar Pago
												$sqlr = "UPDATE humnomina_saludpension SET estado = 'P' WHERE tercero = '".$_POST['decuentas'][$y]."' AND id_nom ='".$_POST['orden']."' AND cc = '".$_POST['deccs'][$y]."' AND tipo = 'PE'";
												mysqli_query($linkbd,$sqlr);
											}break;
											//**** Tipo FS: Pago Fondo de Solidaridad ****
											case 'FS':{
												$ctaconcepto = cuentascontablesccpet::cuentadebito_egreso($_POST['derecursos'][$y],$_POST['deccs'][$y],$fechaf, $_POST['pension_empleado']);
												//Cuenta Origen
												$sqlr = "INSERT INTO comprobante_det (id_comp, cuenta, tercero, centrocosto, detalle, cheque, valdebito, valcredito, estado, vigencia) VALUES ('17 $ideg', '$ctaconcepto', '".$_POST['tercero']."', '".$_POST['deccs'][$y]."', 'Pago Fondo de Solidaridad','', ".$_POST['devalores'][$y].",0,'1' ,'".$_POST['vigencia']."')";
												mysqli_query($linkbd,$sqlr);
												//Cuenta Banco
												$sqlr = "INSERT INTO comprobante_det (id_comp, cuenta, tercero, centrocosto, detalle, cheque, valdebito, valcredito, estado, vigencia) VALUES ('17 $ideg', '".$_POST['banco']."', '".$_POST['tercero']."', '".$_POST['deccs'][$y]."', 'Pago Fondo de solidaridad Banco', '".$_POST['cheque']." ".$_POST['ntransfe']."', 0, ".$_POST['devalores'][$y].", '1', '".$_POST['vigencia']."')";
												mysqli_query($linkbd,$sqlr);
												//Desactivar Pago
												$sqlr = "UPDATE humnomina_saludpension SET estado='P' WHERE tercero = '".$_POST['decuentas'][$y]."' AND id_nom = '".$_POST['orden']."' AND cc = '".$_POST['deccs'][$y]."' AND tipo = 'FS'";
												mysqli_query($linkbd,$sqlr);
											}break;
											//**** Tipo F: Pago Parafiscales ****
											case 'F':{
												$ctaconcepto = cuentascontablesccpet::cuentadebito_egreso($_POST['derecursos'][$y],$_POST['deccs'][$y], $fechaf,'');
												$ncppto = buscarnombreparafiscal($_POST['tedet'][$y]);
												//Cuenta Origen
												$sqlr = "INSERT INTO comprobante_det (id_comp,cuenta,tercero,centrocosto,detalle,cheque, valdebito,valcredito,estado,vigencia) VALUES ('17 $ideg','$ctaconcepto','$_POST[tercero]','".$_POST['deccs'][$y]."','Pago parafiscales $ncppto', '',".$_POST['devalores'][$y].",0,'1','".$_POST['vigencia']."')";
												mysqli_query($linkbd,$sqlr);
												//Cuenta Banco
												$valorp = $_POST['devalores'][$y];
												$sqlr = "INSERT INTO comprobante_det (id_comp,cuenta,tercero,centrocosto,detalle,cheque, valdebito,valcredito,estado,vigencia) VALUES ('17 $ideg','$_POST[banco]','$_POST[tercero]','".$_POST['deccs'][$y]."','Pago parafiscales Banco $ncppto','$_POST[cheque]  $_POST[ntransfe]',0,'$valorp','1', '".$_POST['vigencia']."')";
												mysqli_query($linkbd,$sqlr);
												//Desactivar Pago
												$sqlr = "UPDATE humnomina_parafiscales SET estado='P' WHERE id_nom = '$_POST[orden]' AND cc = '".$_POST['deccs'][$y]."' AND id_parafiscal = '".$_POST['idedescuento'][$y]."'";
												mysqli_query($linkbd,$sqlr);
											}break;
											//***+ Tipo DS: Pago Descuentos Empleado ****
											case 'DS':{
												$vnom = $_POST['devalores'][$y];
												if($vnom>0)
												{
													$sqlr="SELECT descripcion, id_retencion, modo_pago, tipopago FROM humretenempleados WHERE id = '".$_POST['idedescuento'][$y]."'";
													$respr = mysqli_query($linkbd,$sqlr);
													$rret = mysqli_fetch_row($respr);
													if($rret[2] == 'CSF'){
														$sqlrcu = "SELECT DISTINCT T1.nombre, T1.beneficiario, T2.cuenta FROM humvariablesretenciones T1, humvariablesretenciones_det T2 WHERE T1.codigo = '$rret[1]' AND T1.codigo = T2.codigo AND T2.credito = 'S' AND fechainicial = (SELECT MAX(T3.fechainicial) FROM humvariablesretenciones_det T3 WHERE T3.codigo = T2.codigo AND T3.credito = 'S' AND T3.fechainicial <= '$fechaf')";
														$respcu = mysqli_query($linkbd,$sqlrcu);
														while ($rowcu = mysqli_fetch_row($respcu)){
															$ctaconcepto = $rowcu[2];
															$docbenefi = $rowcu[1];
															$nomdescu = strtolower($rowcu[0]);
														}
														$ctabanco = $_POST['banco'];
														$desdecuento = "Pago Banco $rowdes[0] $nomdescu";
														$deschequetransfe = $_POST['cheque']." ".$_POST['ntransfe'];
													}else{
														$sqlrcu = "SELECT nombre, cuentacontable FROM tesomediodepagossf WHERE id = '$rret[1]'";
														$respcu = mysqli_query($linkbd,$sqlrcu);
														$rowcu = mysqli_fetch_row($respcu);
														$ctabanco = $rowcu[1];
														$nomdescu = strtolower($rowcu[0]);
														//$ctabanco = cuentascontablesccpet::cuentadebito_egreso($_POST['derecursos'][$y],$_POST['deccs'][$y],$_POST['fechafi'],'');
														$sqlrcu = "SELECT DISTINCT T1.nombre,T1.beneficiario,T2.cuenta FROM humvariablesretenciones T1, humvariablesretenciones_det T2 WHERE T1.codigo = '01' AND T1.codigo = T2.codigo AND T2.credito = 'S' AND fechainicial=(SELECT MAX(T3.fechainicial) FROM humvariablesretenciones_det T3 WHERE T3.codigo=T2.codigo AND T3.credito = 'S' AND T3.fechainicial<='".$_POST['fechafi']."')";
														$respcu = mysqli_query($linkbd,$sqlrcu);
														while ($rowcu = mysqli_fetch_row($respcu)){
															$ctaconcepto = $rowcu[2];
														}
														$desdecuento = "Pago Sin SF";
														$deschequetransfe = '';
													}
													$ccosto = $_POST['deccs'][$y];
													$ncppto = $rret[0];
													$sqlr = "INSERT INTO comprobante_det (id_comp,cuenta,tercero,centrocosto,detalle,cheque,valdebito,valcredito,estado,vigencia, numacti,cantarticulo) values ('17 $ideg','$ctaconcepto','".$_POST['tercero']."','$ccosto','Pago $rowdes[0] $nomdescu','','$vnom','0','1','".$_POST['vigencia']."','0','0')";
													mysqli_query($linkbd,$sqlr);

													$sqlr = "INSERT INTO comprobante_det (id_comp, cuenta, tercero, centrocosto, detalle, cheque, valdebito, valcredito, estado, vigencia, numacti, cantarticulo) VALUES ('17 $ideg', '$ctabanco', '".$_POST['tercero']."', '$ccosto', '$desdecuento', '$deschequetransfe', '0', '$vnom', '1', '".$_POST['vigencia']."','0','0')";
													mysqli_query($linkbd,$sqlr);
													
													$sqlr = "UPDATE humnominaretenemp SET estado = 'P' WHERE id = '".$_POST['idedescuento'][$y]."' AND id_nom = '$_POST[orden]'";
													$respr = mysqli_query($linkbd,$sqlr);
												}

												
											}break;
											//***+ Tipo DR: Pago Retenciones Empleado ****
											case 'RE':{
												$valorp = $_POST['devalores'][$y];
												$sqlr = "SELECT tiporetencion FROM hum_retencionesfun WHERE id = '".$_POST['idedescuento'][$y]."'";
												$respr = mysqli_query($linkbd,$sqlr);
												$rret = mysqli_fetch_row($respr);
												$sq = "SELECT fechainicial FROM humvariablesretenciones_det WHERE codigo = '$rret[0]' AND fechainicial < '$fechaf' AND cuenta != '' ORDER BY fechainicial ASC";
												$re = mysqli_query($linkbd,$sq);
												while($ro = mysqli_fetch_assoc($re)){
													$_POST['fechacausa'] = $ro["fechainicial"];
												}
												$sqlr = "SELECT distinct * FROM humvariablesretenciones AS T1,humvariablesretenciones_det AS T2 WHERE T1.codigo = '$rret[0]' AND T1.codigo = T2.codigo  AND T2.fechainicial = '$_POST[fechacausa]'";
												$respr = mysqli_query($linkbd,$sqlr);
												while ($rowr = mysqli_fetch_row($respr)){
													$ctacont = $rowr[8];
													if('S' == $rowr[10]){	
														$debito = $valorlibranza;
														$credito = 0;
														$ncppto = buscacuentapres($_POST['derecursos'][$y],2);
														$sqlr="INSERT INTO comprobante_det (id_comp, cuenta, tercero, centrocosto, detalle, cheque,valdebito, valcredito, estado, vigencia) VALUES ('17 $ideg', '$rowr[8]', '".$_POST['tercero']."', '".$_POST['deccs'][$y]."', 'Pago $ncppto', '', ".$_POST['devalores'][$y].", 0, '1', '".$_POST['vigencia']."')";
														mysqli_query($linkbd,$sqlr);
													}
													if('S' == $rowr[9]){
														$valorp = $_POST['devalores'][$y];
														$sqlr="INSERT INTO comprobante_det (id_comp, cuenta, tercero, centrocosto, detalle, cheque,valdebito, valcredito, estado, vigencia) VALUES ('17 $ideg', '".$_POST['banco']."', '".$_POST['tercero']."', '".$_POST['deccs'][$y]."', 'Pago Banco $ncppto', '".$_POST['cheque']."  ".$_POST['ntransfe']."', 0, '$valorp', '1', '".$_POST['vigencia']."')";
														mysqli_query($linkbd,$sqlr);
													}
												}
												$sqlr="UPDATE humnominaretenemp SET estado = 'P' WHERE `id`='".$_POST['idedescuento'][$y]."' AND id_nom = '$_POST[orden]'";
												mysqli_query($linkbd,$sqlr);
											}break;
											//**** Tipo N: Pago Salarios y otros pagos****
											default:{
												$vnom = $_POST['devalores'][$y];
												$ctaconcepto = cuentascontablesccpet::cuentadebito_egreso($_POST['derecursos'][$y],$_POST['deccs'][$y],$fechaf,'');
												$ncppto = nombrevariblespagonomina($_POST['tedet'][$y]);
												$sqlr = "INSERT INTO comprobante_det (id_comp, cuenta, tercero, centrocosto, detalle, cheque, valdebito, valcredito, estado, vigencia, tipo_comp, numerotipo) VALUES ('17 $ideg','$ctaconcepto','".$_POST['tercero']."','".$_POST['deccs'][$y]."','Pago $ncppto','', '$vnom',0,'1' ,'$vigusu','17','$ideg')";
												mysqli_query($linkbd,$sqlr);
														//*** banco ***
												$sqlr="INSERT INTO comprobante_det (id_comp, cuenta, tercero, centrocosto, detalle, cheque, valdebito,valcredito, estado, vigencia, tipo_comp, numerotipo) VALUES ('17 $ideg', '$_POST[banco]', '".$_POST['tercero']."', '".$_POST['deccs'][$y]."', 'Pago Banco $ncppto', '".$_POST['cheque']."  ".$_POST['ntransfe']."', '0', '$vnom', '1', '$vigusu', '17', '$ideg')";
												mysqli_query($linkbd,$sqlr);
												
												$sqlr = "UPDATE humnomina_det SET estado = 'P' WHERE cedulanit='".$_POST['decuentas'][$y]."' AND id_nom = '".$_POST['orden']."'";
												mysqli_query($linkbd,$sqlr);
											}
										}
									}
									echo "<script>despliegamodalm('visible','1','Se ha almacenado el Egreso con Exito');</script>";
								}
							}else{
								echo "<table class='inicio'><tr><td class='saludo1'><center>No se puede almacenar, ya existe un egreso para esta orden <img src='imagenes\alert.png'></center></td></tr></table>";
							}
						}else{
							echo"
							<script>
								Swal.fire({
									icon: 'error',
									title: 'Error!',
									text: 'La fecha actual es menor a la fecha del egreso de nomina anteriror',
									confirmButtonText: 'Continuar',
									confirmButtonColor: '#FF121A',
									timer: 3000
								});
							</script>";
						}
					}else{
						echo"
						<script>
							Swal.fire({
								icon: 'error',
								title: 'Error!',
								text: 'La fecha pertenece a un periodo bloqueado',
								confirmButtonText: 'Continuar',
								confirmButtonColor: '#FF121A',
								timer: 3000
							});
						</script>";
					}
				}
				if($_POST['bop'] == '1'){
					if($_POST['orden']!=''){
						//carga tipos procesos adicionales
						$sqlr="SELECT rp, vigencia FROM hum_nom_cdp_rp WHERE nomina = '".$_POST['orden']."' AND estado = 'S'";
						$resp = mysqli_query($linkbd,$sqlr);
						$row = mysqli_fetch_row($resp);
						$_POST['bop'] = "";
						$_POST['rp'] = $row[0];
						$vigencia = $vigusu = $_POST['vigencia'] = $row[1];
					}else{
						$_POST['cdp'] = "";
						$_POST['detallecdp'] = "";
						$_POST['tercero'] = "";
						$_POST['ntercero'] = "";
						$_POST['bop'] = "";
					}
				}
			?>
			<div class="tabsctas" style="height:70.5%; width:99.6%;">
				<div class="tab">
					<input type="radio" id="tab-1" name="tabgroup1" value="1" <?php echo $check1;?> >
					<label for="tab-1">Nomina</label>
					<div class="content" style="overflow-x:hidden;">
						<table class="inicio ancho">
							<tr>
								<td colspan="8" class="titulos">Comprobante de Egreso</td>
								<td class="cerrar" style="width:7%" onClick="location.href='teso-principal.php'">Cerrar</td>
							</tr>
							<tr>
								<td class="saludo1" style="width:3.2cm;">No Egreso:</td>
								<td style="width:15%;">
									<input type="hidden" name="vigencia" value="<?php echo $_POST['vigencia']?>">
									<input type="hidden" name="cuentapagar" value="<?php echo $_POST['cuentapagar']?>">
									<input type="text" name="egreso" id="egreso" value="<?php echo $_POST['egreso']?>" style="width:100%;" onKeyUp="return tabular(event,this)" onKeyDown="mascara(this,'/',patron,true)" onBlur="buscarp(event)" readonly>
								</td>
								<td class="saludo1" style="width:3.2cm;">Liquidacion Nomina:</td>
								<td style="width:15%;">
									<input type="text" name="orden" value="<?php echo $_POST['orden']?>"  onKeyUp="return tabular(event,this)" onBlur="buscaop(event)" class="colordobleclik" onDblClick = "mypop=window.open('nomina-ventana.php?vigencia=<?php echo $_POST['vigencia']?>','','menubar=0,scrollbars=yes, toolbar=no, location=no, width=900,height=500px'); mypop.focus();" autocomplete="off" style="width:100%;">
									<input type="hidden" value="0" name="bop">
								</td>
								<td class="saludo1" style="width:3.2cm;">RP:</td>
								<td style="width:15%;"><input name="rp" type="text" id="rp" onKeyUp="return tabular(event,this)" value="<?php echo $_POST['rp']?>" style="width:100%;" readonly></td>
								<td></td>
							</tr>
						</table>
						<input type="hidden" value="1" name="oculto">
						
						<!--Evaluar variables para eliminarlas-->
						<input type="hidden" value="0" name="agregadetegre">
						<input type="hidden" name="saldopagar" id="saldopagar" value="<?php echo $_POST['saldopagar']?>">
						<input type="hidden"  name="valororden" id="valororden" value="<?php echo $_POST['valororden']?>">
						
						<?php
							//***MOSTRAR REGISTRO PRESUPUESTAL
							$_POST['brp'] = 0;

							//*** busca contenido del rp
							$_POST['drcuentas'] = array();
							$_POST['drncuentas'] = array();
							$_POST['drvalores'] = array();
							$_POST['drsaldos'] = array();
							$_POST['drubros'] = array();
							$_POST['drfuentes'] = array();
							$_POST['drprogramatico'] = array();
							$_POST['drbpim'] = array();

							$sqlr="SELECT * FROM ccpetrp_detalle WHERE consvigencia = '".$_POST['rp']."' AND vigencia = '".$_POST['vigencia']."' AND tipo_mov = '201' AND consvigencia != 0";
							$res = mysqli_query($linkbd,$sqlr);
							while($r = mysqli_fetch_row($res))
							{
								$consec = $r[0];
								$_POST['drcuentas'][] = $r[3];
								$_POST['drvalores'][] = $r[6];
								$_POST['drsaldos'][] = $r[8];
								$_POST['drncuentas'][] = nombrecuentapresu($r[3]);
								$_POST['drubros'][] = $r[3];
								$_POST['drfuentes'][] = $r[5];
								$_POST['drprogramatico'][] = $r[11];
								$_POST['drbpim'][] = $r[16];
							}
						?>
						<table class="inicio">
							<tr><td colspan="7" class="titulos">Detalle Registro Presupuestal</td></tr>
							<?php
								if($_POST['todos'] == 1){$checkt = 'checked';}
								else {$checkt = '';}
								echo"
								<tr>
									<td class='titulos2'>Cuenta</td>
									<td class='titulos2'>Nombre Cuenta</td>
									<td class='titulos2'>Fuente</td>
									<td class='titulos2'>Programatico</td>
									<td class='titulos2'>Bpim</td>
									<td class='titulos2'>Valor</td>
									<td class='titulos2'>Cancelado</td>
								</tr>";
								$_POST['totalv'] = 0;
								$_POST['totals'] = 0;
								$totalpagos = 0;
								$iter = "zebra1";
								$iter2 = "zebra2";
								for ($x = 0; $x < count($_POST['drcuentas']); $x++)
								{
									$chk = '';
									$ch = esta_en_array($_POST['drubros'],$_POST['drcuentas'][$x]);
									if($ch == '1')
									{
										$chk = "checked";
										$_POST['totalv'] = $_POST['totalv'] + $_POST['drvalores'][$x];
									}
									$sqlpago = "SELECT SUM(valor) FROM tesoegresosnomina_det WHERE id_orden = '".$_POST['orden']."' AND cuentap = '".$_POST['drcuentas'][$x]."' AND fuente = '".$_POST['drfuentes'][$x]."' AND indicador_producto = '".$_POST['drprogramatico'][$x]."' AND bpin = '".$_POST['drbpim'][$x]."'";
									$respago = mysqli_query($linkbd,$sqlpago);
									$rowpago = mysqli_fetch_row($respago);
									$totalpagos += $rowpago[0];
									echo "
									<input type='hidden' name='drcuentas[]' value='".$_POST['drcuentas'][$x]."'/>
									<input type='hidden' name='drncuentas[]' value='".$_POST['drncuentas'][$x]."'/>
									<input type='hidden' name='drvalores[]' value='".$_POST['drvalores'][$x]."'/>
									<input type='hidden' name='drsaldos[]' value='".$_POST['drsaldos'][$x]."'/>
									<input type='hidden' name='drfuentes[]' value='".$_POST['drfuentes'][$x]."'/>
									<input type='hidden' name='drprogramatico[]' value='".$_POST['drprogramatico'][$x]."'/>
									<input type='hidden' name='drbpim[]' value='".$_POST['drbpim'][$x]."'/>
									<tr class='$iter'>
										<td>".$_POST['drcuentas'][$x]."</td>
										<td style='text-transform:uppercase'>".$_POST['drncuentas'][$x]."</td>
										<td>".$_POST['drfuentes'][$x]."</td>
										<td>".$_POST['drprogramatico'][$x]."</td>
										<td>".$_POST['drbpim'][$x]."</td>
										<td align='right'>".number_format($_POST['drvalores'][$x],2,".",",")."</td>
										<td align='right'>".number_format($rowpago[0],2,".",",")."</td>
									</tr>";
									$aux = $iter2;
									$iter2 = $iter;
									$iter = $aux;
								}
								$resultado = convertir($_POST['totalv']);
								$_POST['letras'] = $resultado." PESOS M/CTE";
								$_POST['totalvf'] = number_format($_POST['totalv'],2,".",",");
								$_POST['totalsf'] = number_format($totalpagos,2,".",",");
								echo "
								<input type='hidden' name='totalvf' value='".$_POST['totalvf']."'/>
								<input type='hidden' name='totalv' value='".$_POST['totalv']."'/>
								<input type='hidden' name='totalsf' value='".$_POST['totalsf']."'/>
								<input type='hidden' name='totals' value='".$_POST['totals']."'/>
								<input type='hidden' name='letras' value='".$_POST['letras']."'/>
								<tr class='$iter'>
									<td ></td>
									<td style='text-align:right;'>TOTAL:</td>
									<td style='text-align:right;'>".$_POST['totalvf']."</td>
									<td style='text-align:right;'>".$_POST['totalsf']."</td>
								</tr>
								<tr>
									<td class='saludo1'>Son:</td> 
									<td colspan='5' class='saludo1'>".$_POST['letras']."</td>
								</tr>";
							?>
						</table>
					</div>
				</div>
				<input type="hidden" id="tipoegreso" name="tipoegreso" value="<?php echo $_POST['tipoegreso']?>"/>
				<div class="tab">
					<input type="radio" id="tab-2" name="tabgroup1" value="2" <?php echo $check2;?>>
					<label for="tab-2">Detalles</label>
					<div class="content" style="overflow:hidden;">
						<table class="inicio">
							<tr>
								<td colspan="5" class="titulos">Detalle Orden de Pago</td>
								<td class="titulos" style="text-align:center;width:5%"><input type="button" name="creaegreso" id="creaegreso" value=" Limpiar" onClick="limpiarsele()"></td>
								<td class="titulos" style="text-align:center;width:7%"><input type="button" name="creaegreso" id="creaegreso" value=" Seleccionar Planilla " onClick="seleplanilla()"></td>
								<td class="titulos" style="text-align:center;width:7%"><input type="button" name="creaegreso" id="creaegreso" value=" Crear Egreso " onClick="agregaregreso()" style="background-color:#36D000 !important;"></td>
								<td class="cerrar" style="width:7%;"><a onClick="location.href='teso-principal.php'">&nbsp;Cerrar</a></td>
							</tr>
						</table>
						<div class="subpantallac5" style="overflow-x:hidden;">
							<table class="inicio">
								<tr>
									<td class="titulos2" style="width:6%">No - Tipo</td>
									<td class="titulos2" style="width:7%">Nit</td>
									<td class="titulos2">Tercero</td>
									<td class="titulos2" style="width:3%">CC</td>
									<td class="titulos2" style="width:20%">Item</td>
									<td class="titulos2" style="width:15%">Cuenta Presupuestal</td>
									<td class="titulos2" style="width:10%">Valor</td>
									<td class="titulos2" style="width:4%">-</td>
								</tr>
								<?php
									if ($_POST['elimina'] != ''){ 
										$posi = $_POST['elimina'];
										unset($_POST['dccs'][$posi]);
										unset($_POST['dvalores'][$posi]);
										$_POST['dccs'] = array_values($_POST['dccs']);
									}
									if ($_POST['agregadet'] == '1'){
										$_POST['dccs'][] = $_POST['cc'];
										$_POST['agregadet'] = '0';
										echo"
										<script>
											document.form2.banco.value='';
											document.form2.nbanco.value='';
											document.form2.banco2.value='';
											document.form2.nbanco2.value='';
											document.form2.cb.value='';
											document.form2.cb2.value='';
											document.form2.valor.value='';
											document.form2.numero.value='';
											document.form2.agregadet.value='0';
											document.form2.numero.select();
											document.form2.numero.focus();
										</script>";
									}
									$iter = 'saludo1a';
									$iter2 = 'saludo2';
									$_POST['totalc'] = 0;
									$veps[] = array();
									$veps2[] = array();
									$vafp[] = array();
									$vafp2[] = array();
									$vasec[] = array();
									$varp[] = array();
									
									//primera parte de los detalles de la orden de pago DATOS EMPLEADOS
									$sqlr = "
									SELECT SUM(netopagar), cc, GROUP_CONCAT(tipopago ORDER BY tipopago SEPARATOR ', '), cedulanit, estado, totaldev, salbas, SUM(arp), SUM(salud), SUM(pension), SUM(fondosolid), SUM(otrasdeduc), idfuncionario, fuente, indicador_producto, bpin, proyecto, seccion_presupuestal, GROUP_CONCAT(cuenta_presu ORDER BY tipopago SEPARATOR '<->'), GROUP_CONCAT(netopagar ORDER BY tipopago SEPARATOR '<->') 
									FROM humnomina_det 
									WHERE id_nom = '".$_POST['orden']."' AND 'P' = (SELECT estado FROM humnomina WHERE id_nom = '".$_POST['orden']."') GROUP BY idfuncionario ORDER BY cedulanit";
									$resp2 = mysqli_query($linkbd,$sqlr);
									$i = 1;
									//busca tipos de pago 
									while($row2 = mysqli_fetch_row($resp2)){
										if($row2[0] > 0){
											$chk = $tipopnom = $ctapresver = $netosver = $dsb = '';
											$ccpersona = $row2[1];
											$doctercero = $row2[3];
											$ntercero = buscatercero($row2[3]);
											$ctapres = $row2[18];
											$netosge = $row2[19];
											$tipop = $row2[2];
											
											$ctapresdiv = explode('<->', $row2[18]);
											$netosdiv = explode('<->', $row2[19]);
											$tipodiv = explode(', ', $row2[2]);
											for($xy = 0; $xy < count($tipodiv); $xy++){
												if($tipopnom == ''){$tipopnom = "($tipodiv[$xy])".nombrevariblespagonomina($tipodiv[$xy]);}
												else {$tipopnom = $tipopnom.'<br>'."($tipodiv[$xy])".nombrevariblespagonomina($tipodiv[$xy]);}
											}
											for($xy = 0; $xy < count($ctapresdiv); $xy++){
												if($ctapresver == ''){
													$ctapresver = "($tipodiv[$xy]) ".$ctapresdiv[$xy];
												}
												else{
													$ctapresver = $ctapresver.'<br>'."($tipodiv[$xy]) ".$ctapresdiv[$xy];
												}
											}
											for($xy = 0; $xy < count($netosdiv); $xy++){
												if($netosver == ''){
													$netosver = "($tipodiv[$xy]) ".'$ '.number_format($netosdiv[$xy], 2, '.', ',');
												}
												else{
													$netosver = $netosver."<br>($tipodiv[$xy]) $ ".number_format($netosdiv[$xy], 2, '.', ',');
												}
											}
											$ch = esta_en_array($_POST['conciliados'], $i);
											if($ch == 1){
												$chk = "checked"; 
												$tipclase = 'saludo1v';
											}
											else{
												$tipclase = $iter;
											}
											$dsb2 = "<img src='imagenes/dinerob1.png' title='NO PAGO' style='width:18px;' />";
											if($row2[4] == 'P')
											{
												$dsb = " disabled";
												$dsb2 = "<img src='imagenes/dinero1.png' title='PAGO' style='width:18px' />";
											}
											$idnomina = $vfuentes = $vprogramatico = $vbpin = $vproyecto = $vproyecto = '';
											echo "
											<input type='hidden' name='idfuncionario[]' value='$row2[12]'/>
											<input type='hidden' name='tdet[]' value='$tipop'/>
											<input type='hidden' name='dcuentas[]' value='$doctercero'/>
											<input type='hidden' name='dncuentas[]' value='$ntercero'/>
											<input type='hidden' name='drecursos[]' value='$ctapres'/>
											<input type='hidden' name='dccs[]' value='$ccpersona'/>
											<input type='hidden' name='dvalores[]' value='".round($row2[0], 0, PHP_ROUND_HALF_UP)."'/>
											
											<input type='hidden' name='detalles[]' value='$row2[6]'/>
											<input type='hidden' name='iddescuento[]' value=''/>
											<input type='hidden' name='codfuncionario[]' value='$row2[13]'>
											<input type='hidden' name='saldevengado[]' value='$row2[5]'/>
											<input type='hidden' name='valarl[]' value='$row2[7]'/>
											<input type='hidden' name='valsalud[]' value='$row2[8]'/>
											<input type='hidden' name='valpension[]' value='$row2[9]'/>
											<input type='hidden' name='valfondosol[]' value='$row2[10]'/>
											<input type='hidden' name='valdeducciones[]' value='$row2[11]'/>
											
											<input type='hidden' name='nvidborrar[]' value='$idnomina'>
											<input type='hidden' name='nvfuente[]' value='$vfuentes'>
											<input type='hidden' name='nvprogramatico[]' value='$vprogramatico'>
											<input type='hidden' name='nvbpin[]' value='$vbpin'>
											<input type='hidden' name='nvproyecto[]' value='$vproyecto'>
											<input type='hidden' name='nvsecpresu[]' value='$vsecpresu'>
											
											<tr class='$tipclase' id='filas$i'>
												<td>$i: $tipop</td>
												<td>$row2[3]</td>
												<td>$ntercero</td>
												<td>$ccpersona</td>
												<td>$tipopnom</td>
												<td>$ctapresver</td>
												<td style='text-align:right;'>$netosver<br>$ ".number_format(round($row2[0], 0, PHP_ROUND_HALF_UP), 2, '.', ',')."</td>
												<td><input type='checkbox' name='conciliados[]' value='$i' $chk onClick='marcar($row2[3],$i);'  $dsb  class='defaultcheckbox'>&nbsp;&nbsp;$dsb2</td>
											</tr>";
											$_POST['totalc'] = $_POST['totalc'] + $row2[0];
											$_POST['totalcf'] = number_format($_POST['totalc'],2,".",",");
											$i += 1;
											$aux=$iter;
											$iter=$iter2;
											$iter2=$aux;
										}
									}
									//Segunda parte de los detalles de la orden de pago DESCUENTOS Y LIBRANZAS
									$sqlrdesc="
									SELECT T1.id_nom, T1.id, T1.cedulanit, T1.valor, T1.estado, T1.fuente, T1.indicador_producto, T1.bpin, T1.proyecto, T1.seccion_presupuestal, T1.cuenta_presu, T1.cod_fun, T1.id_des, T1.descripcion, T3.beneficiario
									FROM humnominaretenemp AS T1
									INNER JOIN humvariablesretenciones AS T3
									ON T1.idtercero = T3.codigo
									WHERE T1.id_nom='".$_POST['orden']."' AND T1.tipo_des='DS'";
									$resp2 = mysqli_query($linkbd,$sqlrdesc);
									while($row2 = mysqli_fetch_row($resp2)){
										$dsb = " ";
										$dsb2 = "<img src='imagenes/dinerob1.png' title='NO PAGO' style='width:18px;' />";
										$chk = "";
										if($row2[4] == 'P'){
											$dsb=" disabled";
											$dsb2="<img src='imagenes/dinero1.png' title='PAGO' style='width:18px' />";	
										}	
										$ch = esta_en_array($_POST['conciliados'], $i);
										if($ch == 1){
											$chk = "checked";
										}
										$ntercero = buscatercero($row2[14]);
										$deudor = buscatercero($row2[2]);
										$valores = $row2[3]; 
										$descripdes = $row2[13];
										$numccdeudor = itemfuncionarios($row2[11],'22');
										$idnomina = $vfuentes = $vprogramatico = $vbpin = $vproyecto = $vproyecto = '';
										$idnomina = $row2[12];
										$vfuentes = $row2[5];
										$vprogramatico = $row2[6];
										$vbpin = $row2[7];
										$vproyecto = $row2[8];
										$vsecpresu = $row2[9];
										echo "
										<input type='hidden' name='tdet[]' value='DS'/>
										<input type='hidden' name='dcuentas[]' value='$row2[14]'/>
										<input type='hidden' name='dncuentas[]' value='$ntercero - $descripdes - $deudor'/>
										<input type='hidden' name='dccs[]' value='$numccdeudor'/>
										<input type='hidden' name='drecursos[]' value='$row2[10]'/>
										<input type='hidden' name='dvalores[]' value='".round($row2[3], 0, PHP_ROUND_HALF_UP)."'>
										<input type='hidden' name='detalles[]' value='$row2[2]'>
										<input type='hidden' name='iddescuento[]' value='$row2[1]'>
										<input type='hidden' name='codfuncionario[]' value='$row2[11]'>
										<input type='hidden' name='nvidborrar[]' value='$idnomina'>
										<input type='hidden' name='nvfuente[]' value='$vfuentes'>
										<input type='hidden' name='nvprogramatico[]' value='$vprogramatico'>
										<input type='hidden' name='nvbpin[]' value='$vbpin'>
										<input type='hidden' name='nvproyecto[]' value='$vproyecto'>
										<input type='hidden' name='nvsecpresu[]' value='$vsecpresu'>
										<tr class='$iter' id='filas$i'>
											<td>$i DS</td>
											<td>$row2[2]</td>
											<td>$ntercero - $descripdes - $deudor</td>
											<td>$numccdeudor</td>
											<td>Descuantos y Libranzas</td>
											<td>$row2[10]</td>
											<td style='text-align:right;'>$ ".number_format(round($row2[3], 0, PHP_ROUND_HALF_UP),2,'.',',')."</td>
											<td><input type='checkbox' name='conciliados[]' value='$i' $chk onClick='marcar($k,$i);' $dsb class='defaultcheckbox'>&nbsp;&nbsp;$dsb2</td>
										</tr>";
										$_POST['totalc'] = $_POST['totalc']+$valores;
										$_POST['totalcf'] = number_format($_POST['totalc'],2,".",",");
										$i += 1;
										$aux = $iter;
										$iter = $iter2;
										$iter2 = $aux;
									}
									//tercera parte de los detalles de la orden de pago RETENCIONES LIBRANZAS
									$sqlrdesc="
									SELECT DISTINCT T1.id_nom,T1.id,T1.cedulanit,T1.valor,T2.id,T2.tiporetencion,T2.valorretencion,T3.beneficiario, T1.estado,T2.nomfuncionario 
									FROM humnominaretenemp T1, hum_retencionesfun T2
									WHERE T1.id=T2.id AND T1.id_nom='".$_POST['orden']."' AND T1.tipo_des='RE'";
									/*$sqlrdesc="
									SELECT DISTINCT T1.id_nom,T1.id,T1.cedulanit,T1.valor,T2.id,T2.tiporetencion,T2.valorretencion,T3.beneficiario, T1.estado,T2.nomfuncionario 
									FROM humnominaretenemp T1, hum_retencionesfun T2, humvariablesretenciones T3
									WHERE T1.id=T2.id AND T2.tiporetencion=T3.codigo AND T1.id_nom='".$_POST['orden']."' AND T1.tipo_des='RE'";*/
									$resp2 = mysqli_query($linkbd,$sqlrdesc);
									while($row2 = mysqli_fetch_row($resp2)){
										$dsb = " ";
										$dsb2 = "<img src='imagenes/dinerob1.png' title='NO PAGO' style='width:18px;' />";
										$chk = "";
										if($row2[8] == 'P'){
											$dsb = " disabled";
											$dsb2 = "<img src='imagenes/dinero1.png' title='PAGO' style='width:18px' />";	
										}	
										$ch = esta_en_array($_POST['conciliados'], $i);
										if($ch == 1){
											$chk = "checked";
										}	
										$ntercero = buscatercero($row2[7]);
										$deudor = buscatercero($row2[2]);
										$valores = $row2[6]; 
										$descripdes = $row2[9];
										$idnomina = $vfuentes = $vprogramatico = $vbpin = $vproyecto = $vproyecto = '';
										
										echo "
										<input type='hidden' name='tdet[]' value='RE'/>
										<input type='hidden' name='dcuentas[]' value='$row2[7]'/>
										<input type='hidden' name='dncuentas[]' value='$ntercero - $descripdes - $deudor'/>
										<input type='hidden' name='dccs[]' value='".$vccemp[$row2[2]]."'/>
										<input type='hidden' name='drecursos[]' value='".$vcpemp[$row2[2]]."'/>
										<input type='hidden' name='dvalores[]' value='".round($row2[3], 0, PHP_ROUND_HALF_UP)."'>
										<input type='hidden' name='detalles[]' value='$row2[2]'>
										<input type='hidden' name='codfuncionario[]' value='$rowcc[9]'>
										<input type='hidden' name='iddescuento[]' value='$row2[1]'>
										
										<input type='hidden' name='nvidborrar[]' value='$idnomina'>
										<input type='hidden' name='nvfuente[]' value='$vfuentes'>
										<input type='hidden' name='nvprogramatico[]' value='$vprogramatico'>
										<input type='hidden' name='nvbpin[]' value='$vbpin'>
										<input type='hidden' name='nvproyecto[]' value='$vproyecto'>
										<input type='hidden' name='nvsecpresu[]' value='$vsecpresu'>
										
										<tr class='$iter' id='filas$i'>
											<td>$i RE</td>
											<td>$row2[7]</td>
											<td>$ntercero - $descripdes - $deudor</td>
											<td>".$vccemp[$row2[2]]."</td>
											<td>Retenci&oacute;n en la Fuente</td>
											<td>".$vcpemp[$row2[2]]."</td>
											<td style='text-align:right;'>$ ".number_format(round($row2[3], 0, PHP_ROUND_HALF_UP),2,'.',',')."</td>
											<td><input type='checkbox' name='conciliados[]' value='$i' $chk onClick='marcar($k,$i);'   $dsb class='defaultcheckbox'>&nbsp;&nbsp;$dsb2</td>
										</tr>";
										$_POST['totalc']=$_POST['totalc']+$valores;
										$_POST['totalcf']=number_format($_POST['totalc'],2,".",",");
										$i += 1;
										$aux = $iter;
										$iter = $iter2;
										$iter2 = $aux;
									}
									//cuarta parte de los detalles de la orden de pago DATOS SALUD A
									$sqlr = "SELECT * FROM humnomina_saludpension WHERE tipo = 'SR' AND id_nom = '".$_POST['orden']."'";
									$rescc = mysqli_query($linkbd,$sqlr);
									while ($rowcc =mysqli_fetch_row($rescc)){
										$chk = "";
										$dsb = "";
										$dsb2 = "<img src='imagenes/dinerob1.png' title='NO PAGO' style='width:18px;' />";
										$ch = esta_en_array($_POST['conciliados'], $i);
										if($ch == 1){
											$chk="checked";
										}
										$ctapres = $rowcc[10];
										$ntercero = buscatercero($rowcc[3]);
										$nfuncionario = buscatercero($rowcc[2]);
										$estadop = $rowcc[6];
										if($estadop == 'P')
										{
											$dsb = " disabled";
											$dsb2 = "<img src='imagenes/dinero1.png' title='PAGO' style='width:18px' />";	
										}
										$idnomina = $vfuentes = $vprogramatico = $vbpin = $vproyecto = $vproyecto = '';
										$idnomina = $rowcc[8];
										$vfuentes = $rowcc[12];
										$vprogramatico = $rowcc[13];
										$vbpin = $rowcc[14];
										$vproyecto = $rowcc[19];
										$vsecpresu = $rowcc[20];
										echo "
										<input type='hidden' name='tdet[]' value='SR'/>
										<input type='hidden' name='dcuentas[]' value='$rowcc[3]'/>
										<input type='hidden' name='dncuentas[]' value='$ntercero - $nfuncionario'/>
										<input type='hidden' name='dccs[]' value='$rowcc[4]'/>
										<input type='hidden' name='drecursos[]' value='$ctapres'/>
										<input type='hidden' name='dvalores[]' value='".round($rowcc[5], 0, PHP_ROUND_HALF_UP)."'/>
										<input type='hidden' name='detalles[]' value='$row2[2]'>
										<input type='hidden' name='codfuncionario[]' value='$rowcc[9]'>
										<input type='hidden' name='iddescuento[]' value=''>
										
										<input type='hidden' name='nvidborrar[]' value='$idnomina'>
										<input type='hidden' name='nvfuente[]' value='$vfuentes'>
										<input type='hidden' name='nvprogramatico[]' value='$vprogramatico'>
										<input type='hidden' name='nvbpin[]' value='$vbpin'>
										<input type='hidden' name='nvproyecto[]' value='$vproyecto'>
										<input type='hidden' name='nvsecpresu[]' value='$vsecpresu'>
										
										<tr class='$iter' id='filas$i'>
											<td>$i SR</td>
											<td>$rowcc[3]</td>
											<td>$ntercero - $nfuncionario</td>
											<td>$rowcc[4]</td>
											<td>Salud Empresa</td>
											<td>$ctapres</td>
											<td style='text-align:right;'>$ ".number_format(round($rowcc[5], 0, PHP_ROUND_HALF_UP),2,'.',',')."</td>
											<td><input type='checkbox' name='conciliados[]' value='$i' $chk onClick='marcar($k,$i);' $dsb class='defaultcheckbox'>&nbsp;&nbsp;$dsb2</td>
										</tr>";
										$_POST['totalc'] = $_POST['totalc']+$rowcc[5];
										$_POST['totalcf'] = number_format($_POST['totalc'],2,".",",");
										$i+=1;
										$aux=$iter;
										$iter=$iter2;
										$iter2=$aux;
									}
									//quinta parte de los detalles de la orden de pago SALUD EMPLEADO
									$sqlr = "SELECT * FROM humnomina_saludpension WHERE tipo = 'SE' AND id_nom = '".$_POST['orden']."'";
									$rescc = mysqli_query($linkbd,$sqlr);
									while ($rowcc = mysqli_fetch_row($rescc)){
										$chk = "";
										$dsb = "";
										$dsb2 = "<img src='imagenes/dinerob1.png' title='NO PAGO' style='width:18px;'/>";
										$ch = esta_en_array($_POST['conciliados'], $i);
										if($ch == 1){$chk="checked";}
										$ctapres = $rowcc[10];
										$ntercero = buscatercero($rowcc[3]);
										$nfuncionario = buscatercero($rowcc[2]);
										$estadop = $rowcc[6];
										if($estadop=='P'){
											$dsb = " disabled";
											$dsb2 = "<img src='imagenes/dinero1.png' title='PAGO' style='width:18px' />";	
										}
										$idnomina = $vfuentes = $vprogramatico = $vbpin = $vproyecto = $vproyecto = '';
										$idnomina = $rowcc[8];
										$vfuentes = $rowcc[12];
										$vprogramatico = $rowcc[13];
										$vbpin = $rowcc[14];
										$vproyecto = $rowcc[19];
										$vsecpresu = $rowcc[20];
										echo "
										<input type='hidden' name='tdet[]' value='SE'/>
										<input type='hidden' name='dcuentas[]' value='$rowcc[3]'/>
										<input type='hidden' name='dncuentas[]' value='$ntercero - $nfuncionario'/>
										<input type='hidden' name='dccs[]' value='$rowcc[4]'/>
										<input type='hidden' name='drecursos[]' value='$ctapres'/>
										<input type='hidden' name='dvalores[]' value='".round($rowcc[5], 0, PHP_ROUND_HALF_UP)."'/>
										<input type='hidden' name='detalles[]' value='$row2[2]'>
										<input type='hidden' name='codfuncionario[]' value='$rowcc[9]'>
										<input type='hidden' name='iddescuento[]' value=''>
										
										<input type='hidden' name='nvidborrar[]' value='$idnomina'>
										<input type='hidden' name='nvfuente[]' value='$vfuentes'>
										<input type='hidden' name='nvprogramatico[]' value='$vprogramatico'>
										<input type='hidden' name='nvbpin[]' value='$vbpin'>
										<input type='hidden' name='nvproyecto[]' value='$vproyecto'>
										<input type='hidden' name='nvsecpresu[]' value='$vsecpresu'>
										
										<tr class='$iter' id='filas$i'>
											<td>$i SE</td>
											<td>$rowcc[3]</td>
											<td>$ntercero - $nfuncionario</td>
											<td>$rowcc[4]</td>
											<td>Salud Empleados</td>
											<td>$ctapres</td>
											<td style='text-align:right;'>$ ".number_format(round($rowcc[5], 0, PHP_ROUND_HALF_UP),2,'.',',')."</td>
											<td><input type='checkbox' name='conciliados[]' value='$i' $chk onClick='marcar($k,$i);' $dsb class='defaultcheckbox'>&nbsp;&nbsp;$dsb2</td>
										</tr>";
										$_POST['totalc']=$_POST['totalc']+$rowcc[5];
										$_POST['totalcf']=number_format($_POST['totalc'],2,".",",");
										$i+=1;
										$aux=$iter;
										$iter=$iter2;
										$iter2=$aux;
									}
									//sexta parte de los detalles de la orden de pago PENSION EMPRESA
									$sqlr="SELECT * FROM humnomina_saludpension WHERE tipo = 'PR' AND id_nom = '".$_POST['orden']."'";
									$rescc=mysqli_query($linkbd,$sqlr);
									while ($rowcc = mysqli_fetch_row($rescc)){
										$chk = "";
										$dsb = "";
										$dsb2 = "<img src='imagenes/dinerob1.png' title='NO PAGO' style='width:18px;' />";
										$ch = esta_en_array($_POST['conciliados'], $i);
										if($ch==1){
											$chk = "checked";
										}
										$ctapres = $rowcc[10];
										$ntercero = buscatercero($rowcc[3]);
										$nfuncionario = buscatercero($rowcc[2]);
										$estadop = $rowcc[6];
										if($estadop == 'P'){
											$dsb = " disabled";
											$dsb2 = "<img src='imagenes/dinero1.png' title='PAGO' style='width:18px' />";	
										}
										$idnomina = $vfuentes = $vprogramatico = $vbpin = $vproyecto = $vproyecto = '';
										$idnomina = $rowcc[8];
										$vfuentes = $rowcc[12];
										$vprogramatico = $rowcc[13];
										$vbpin = $rowcc[14];
										$vproyecto = $rowcc[19];
										$vsecpresu = $rowcc[20];
										echo "
										<input type='hidden' name='tdet[]' value='PR'/>
										<input type='hidden' name='dcuentas[]' value='$rowcc[3]'/>
										<input type='hidden' name='dncuentas[]' value='$ntercero - $nfuncionario'/>
										<input type='hidden' name='dccs[]' value='$rowcc[4]'/>
										<input type='hidden' name='drecursos[]' value='$ctapres'/>
										<input type='hidden' name='dvalores[]' value='".round($rowcc[5], 0, PHP_ROUND_HALF_UP)."'/>
										<input type='hidden' name='detalles[]' value='$row2[2]'>
										<input type='hidden' name='codfuncionario[]' value='$rowcc[9]'>
										<input type='hidden' name='iddescuento[]' value=''>
										
										<input type='hidden' name='nvidborrar[]' value='$idnomina'>
										<input type='hidden' name='nvfuente[]' value='$vfuentes'>
										<input type='hidden' name='nvprogramatico[]' value='$vprogramatico'>
										<input type='hidden' name='nvbpin[]' value='$vbpin'>
										<input type='hidden' name='nvproyecto[]' value='$vproyecto'>
										<input type='hidden' name='nvsecpresu[]' value='$vsecpresu'>
										
										<tr class='$iter' id='filas$i'>
											<td>$i PR </td>
											<td>$rowcc[3]</td>
											<td>$ntercero - $nfuncionario</td>
											<td>$rowcc[4]</td>
											<td>Pensi&oacute;n Empresa</td>
											<td>$ctapres</td>
											<td style='text-align:right;'>$ ".number_format(round($rowcc[5], 0, PHP_ROUND_HALF_UP),2,'.',',')."</td>
											<td><input type='checkbox' name='conciliados[]' value='$i' $chk onClick='marcar($k,$i);' $dsb class='defaultcheckbox'>&nbsp;&nbsp;$dsb2</td>
										</tr>";
										$_POST['totalc'] = $_POST['totalc'] + $rowcc[5];
										$_POST['totalcf'] = number_format($_POST['totalc'],2,".",",");
										$i += 1;
										$aux = $iter;
										$iter = $iter2;
										$iter2 = $aux;
									}
									//septima parte de los detalles de la orden de pago PENSION EMPLEADO
									$sqlr = "SELECT * FROM humnomina_saludpension WHERE tipo = 'PE' AND id_nom = '".$_POST['orden']."'";
									$rescc = mysqli_query($linkbd,$sqlr);
									while ($rowcc = mysqli_fetch_row($rescc)){
										$chk = "";
										$dsb = "";
										$dsb2 = "<img src='imagenes/dinerob1.png' title='NO PAGO' style='width:18px;' />";
										$ch = esta_en_array($_POST['conciliados'], $i);
										if($ch == 1){
											$chk = "checked";
										}
										$ctapres = $rowcc[10];
										$ntercero = buscatercero($rowcc[3]);
										$nfuncionario = buscatercero($rowcc[2]);
										$estadop = $rowcc[6];
										if($estadop == 'P')
										{
											$dsb = " disabled";
											$dsb2 = "<img src='imagenes/dinero1.png' title='PAGO' style='width:18px' />";	
										}
										$idnomina = $vfuentes = $vprogramatico = $vbpin = $vproyecto = $vproyecto = '';
										$idnomina = $rowcc[8];
										$vfuentes = $rowcc[12];
										$vprogramatico = $rowcc[13];
										$vbpin = $rowcc[14];
										$vproyecto = $rowcc[19];
										$vsecpresu = $rowcc[20];
										echo "
										<input type='hidden' name='tdet[]' value='PE'/>
										<input type='hidden' name='dcuentas[]' value='$rowcc[3]'/>
										<input type='hidden' name='dncuentas[]' value='$ntercero - $nfuncionario'/>
										<input type='hidden' name='dccs[]' value='$rowcc[4]'/>
										<input type='hidden' name='drecursos[]' value='$ctapres'/>
										<input type='hidden' name='dvalores[]' value='".round($rowcc[5], 0, PHP_ROUND_HALF_UP)."'/>
										<input type='hidden' name='detalles[]' value='$row2[2]'>
										<input type='hidden' name='codfuncionario[]' value='$rowcc[9]'>
										<input type='hidden' name='iddescuento[]' value=''>
										
										<input type='hidden' name='nvidborrar[]' value='$idnomina'>
										<input type='hidden' name='nvfuente[]' value='$vfuentes'>
										<input type='hidden' name='nvprogramatico[]' value='$vprogramatico'>
										<input type='hidden' name='nvbpin[]' value='$vbpin'>
										<input type='hidden' name='nvproyecto[]' value='$vproyecto'>
										<input type='hidden' name='nvsecpresu[]' value='$vsecpresu'>
										
										<tr class='$iter' id='filas$i'>
											<td>$i PE</td>
											<td>$rowcc[3]</td>
											<td>$ntercero - $nfuncionario</td>
											<td>$rowcc[4]</td>
											<td>Pensi&oacute;n Empleados</td>
											<td>$ctapres</td>
											<td style='text-align:right;'>$ ".number_format(round($rowcc[5], 0, PHP_ROUND_HALF_UP),2,'.',',')."</td>
											<td><input type='checkbox' name='conciliados[]' value='$i' $chk onClick='marcar($k,$i);' $dsb class='defaultcheckbox'>&nbsp;&nbsp;$dsb2</td>
										</tr>";
										$_POST['totalc']=$_POST['totalc']+$rowcc[5];
										$_POST['totalcf']=number_format($_POST['totalc'],2,".",",");
										$i+=1;
										$aux = $iter;
										$iter = $iter2;
										$iter2 = $aux;
									}
									//octava parte de los detalles de la orden de pago Fondo de solidaridad
									$sqlr = "SELECT * FROM humnomina_saludpension WHERE tipo = 'FS' AND id_nom = '".$_POST['orden']."'";
									$rescc = mysqli_query($linkbd,$sqlr);
									while ($rowcc =mysqli_fetch_row($rescc)){
										$chk = "";
										$dsb = "";
										$dsb2 = "<img src='imagenes/dinerob1.png' title='NO PAGO' style='width:18px;' />";
										$ch = esta_en_array($_POST['conciliados'], $i);
										if($ch == 1){
											$chk="checked";
										}
										$ctapres = $rowcc[10];
										$ntercero = buscatercero($rowcc[3]);
										$nfuncionario = buscatercero($rowcc[2]);
										$estadop = $rowcc[6];
										if($estadop == 'P'){
											$dsb = " disabled";
											$dsb2 = "<img src='imagenes/dinero1.png' title='PAGO' style='width:18px' />";	
										}
										$idnomina = $vfuentes = $vprogramatico = $vbpin = $vproyecto = $vproyecto = '';
										$idnomina = $rowcc[8];
										$vfuentes = $rowcc[12];
										$vprogramatico = $rowcc[13];
										$vbpin = $rowcc[14];
										$vproyecto = $rowcc[19];
										$vsecpresu = $rowcc[20];
										echo "
										<input type='hidden' name='tdet[]' value='FS'/>
										<input type='hidden' name='dcuentas[]' value='$rowcc[3]'/>
										<input type='hidden' name='dncuentas[]' value='$ntercero - $nfuncionario'/>
										<input type='hidden' name='dccs[]' value='$rowcc[4]'/>
										<input type='hidden' name='drecursos[]' value='$ctapres'/>
										<input type='hidden' name='dvalores[]' value='".round($rowcc[5], 0, PHP_ROUND_HALF_UP)."'/>
										<input type='hidden' name='detalles[]' value='$row2[2]'>
										<input type='hidden' name='codfuncionario[]' value='$rowcc[9]'>
										<input type='hidden' name='iddescuento[]' value=''>
										
										<input type='hidden' name='nvidborrar[]' value='$idnomina'>
										<input type='hidden' name='nvfuente[]' value='$vfuentes'>
										<input type='hidden' name='nvprogramatico[]' value='$vprogramatico'>
										<input type='hidden' name='nvbpin[]' value='$vbpin'>
										<input type='hidden' name='nvproyecto[]' value='$vproyecto'>
										<input type='hidden' name='nvsecpresu[]' value='$vsecpresu'>
										
										<tr class='$iter' id='filas$i'>
											<td>$i FS</td>
											<td>$rowcc[3]</td>
											<td>$ntercero - $nfuncionario</td>
											<td>$rowcc[4]</td>
											<td>Fondo solidaridad</td>
											<td>$ctapres</td>
											<td style='text-align:right;'>$ ".number_format(round($rowcc[5], 0, PHP_ROUND_HALF_UP),2,'.',',')."</td>
											<td><input type='checkbox' name='conciliados[]' value='$i' $chk onClick='marcar($k,$i);' $dsb class='defaultcheckbox'>&nbsp;&nbsp;$dsb2</td>
										</tr>";
										$_POST['totalc'] = $_POST['totalc'] + $rowcc[5];
										$_POST['totalcf'] = number_format($_POST['totalc'],2,".",",");
										$i += 1;
										$aux = $iter;
										$iter = $iter2;
										$iter2 = $aux;
									}
									//novena PARTE parte de los detalles de la orden de pago PARAFISCALES 
									$sqlr2="SELECT cajas,icbf,sena,iti,esap,indiceinca FROM admfiscales WHERE estado='S' AND vigencia = '".$_POST['vigencia']."'";
									$resp2 = mysqli_query($linkbd,$sqlr2);
									while($row2=mysqli_fetch_row($resp2)){
										$cajascom = $row2[0];
										$icbf = $row2[1];
										$sena = $row2[2];
										$iti = $row2[3];
										$esap = $row2[4];
										$arp = $row2[5];
									}
									$sqlr2 = "SELECT cajacompensacion,icbf,sena,iti,esap,arp FROM humparametrosliquida";
									$resp2 = mysqli_query($linkbd,$sqlr2);
									while($row2 = mysqli_fetch_row($resp2)){
										$tcajacomp = $row2[0];
										$ticbfb = $row2[1];
										$tsenab = $row2[2];
										$titib = $row2[3];
										$tesapb = $row2[4];
										$tarpb = $row2[5];
									}
									$sqlr="SELECT * FROM humnomina_parafiscales WHERE id_nom='".$_POST['orden']."' ";
									$resp2 = mysqli_query($linkbd,$sqlr);
									$chk = "";
									$dsb = "";
									while($row2=mysqli_fetch_row($resp2)){
										$chk = "";
										$dsb = "";
										$dsb2 = "<img src='imagenes/dinerob1.png' title='NO PAGO' style='width:18px;' />";
										$sqlrpf = "SELECT tipo,porcentaje FROM humparafiscalesccpet WHERE codigo='$row2[1]'";
										$rowpf = mysqli_fetch_row(mysqli_query($linkbd,$sqlrpf));
										if($rowpf[0]='A'){
											$vatipa="Aporte";
										} 
										else{
											$vatipa="Descuento";
										}
										switch ($row2[1]){
											//Caja de Compensación
											case $tcajacomp:{
												if($row2[5]=='P'){
													$dsb=" disabled";
													$dsb2="<img src='imagenes/dinero1.png' title='PAGO' style='width:18px' />";
												}
												$ch=esta_en_array($_POST['conciliados'], $i);
												if($ch==1){$chk="checked";}
												$numcc = $row2[4];
												$ctapres = $row2[6];
												$valores = $row2[3];
												$ntercero = buscatercero($cajascom);
												$idnomina = $vfuentes = $vprogramatico = $vbpin = $vproyecto = $vsecpresu = '';
												$idnomina = $row2[7];
												$vfuentes = $row2[10];
												$vprogramatico = $row2[11];
												$vbpin = $row2[12];
												$vproyecto = $row2[17];
												$vsecpresu = $row2[18];
												
												echo "
													<input type='hidden' name='tdet[]' value='F'/>
													<input type='hidden' name='dcuentas[]' value='$cajascom'/>
													<input type='hidden' name='dncuentas[]' value='$ntercero'/>
													<input type='hidden' name='dccs[]' value='$numcc'/>
													<input type='hidden' name='drecursos[]' value='$ctapres'/>
													<input type='hidden' name='dvalores[]' value='".round($valores, 0, PHP_ROUND_HALF_UP)."'/>
													<input type='hidden' name='detalles[]' value='$row2[2]'/>
													<input type='hidden' name='iddescuento[]' value='$row2[1]'/>
													
													<input type='hidden' name='nvidborrar[]' value='$idnomina'>
													<input type='hidden' name='nvfuente[]' value='$vfuentes'>
													<input type='hidden' name='nvprogramatico[]' value='$vprogramatico'>
													<input type='hidden' name='nvbpin[]' value='$vbpin'>
													<input type='hidden' name='nvproyecto[]' value='$vproyecto'>
													<input type='hidden' name='nvsecpresu[]' value='$vsecpresu'>
													
													<tr class='$iter'  id='filas$i'>
														<td>$i F</td>
														<td>$cajascom</td>
														<td>$ntercero</td>
														<td>$numcc</td>
														<td>$vatipa $rowpf[1]%</td>
														<td>$ctapres</td>
														<td style='text-align:right;'>$ ".number_format(round($valores, 0, PHP_ROUND_HALF_UP),2,'.',',')."</td>
														<td><input type='checkbox' name='conciliados[]' value='$i' $chk onClick='marcar($k,$i);' $dsb class='defaultcheckbox'>&nbsp;&nbsp;$dsb2</td>
													</tr>";
												$_POST['totalc'] = $_POST['totalc'] + $valores;
												$_POST['totalcf'] = number_format($_POST['totalc'],2,".",",");
												$i += 1;
												$aux = $iter;
												$iter = $iter2;
												$iter2 = $aux;
											}break;
											//ICBF
											case $ticbfb:{
												$dsb = "";
												$dsb2 = "<img src='imagenes/dinerob1.png' title='NO PAGO' style='width:18px;' />";
												if($row2[5] == 'P'){
													$dsb = " disabled";
													$dsb2 = "<img src='imagenes/dinero1.png' title='PAGO' style='width:18px' />";	
												}	
												$ch = esta_en_array($_POST['conciliados'], $i);
												if($ch == 1) {$chk="checked";}
												$numcc = $row2[4];
												$ctapres = $row2[6];
												$valores = $row2[3]; 
												$ntercero = buscatercero($icbf);
												$idnomina = $vfuentes = $vprogramatico = $vbpin = $vproyecto = $vproyecto = '';
												$idnomina = $row2[7];
												$vfuentes = $row2[10];
												$vprogramatico = $row2[11];
												$vbpin = $row2[12];
												$vproyecto = $row2[17];
												$vsecpresu = $row2[18];
												echo "
													<input type='hidden' name='tdet[]' value='F'/>
													<input type='hidden' name='dcuentas[]' value='$icbf'/>
													<input type='hidden' name='dncuentas[]' value='$ntercero'/>
													<input type='hidden' name='dccs[]' value='$numcc'/>
													<input type='hidden' name='drecursos[]' value='$ctapres'>
													<input type='hidden' name='dvalores[]' value='".round($valores, 0, PHP_ROUND_HALF_UP)."'/>
													<input type='hidden' name='detalles[]' value='$row2[2]'>
													<input type='hidden' name='iddescuento[]' value='$row2[1]'>
													
													<input type='hidden' name='nvidborrar[]' value='$idnomina'>
													<input type='hidden' name='nvfuente[]' value='$vfuentes'>
													<input type='hidden' name='nvprogramatico[]' value='$vprogramatico'>
													<input type='hidden' name='nvbpin[]' value='$vbpin'>
													<input type='hidden' name='nvproyecto[]' value='$vproyecto'>
													<input type='hidden' name='nvsecpresu[]' value='$vsecpresu'>
													
													<tr class='$iter'  id='filas$i'>
														<td>$i F</td>
														<td>$icbf</td>
														<td>$ntercero</td>
														<td>$numcc</td>
														<td>$vatipa $rowpf[1]%</td>
														<td>$ctapres</td>
														<td style='text-align:right;'>$ ".number_format(round($valores, 0, PHP_ROUND_HALF_UP),2,'.',',')."</td>
														<td><input type='checkbox' name='conciliados[]' value='$i' $chk onClick='marcar($k,$i);' $dsb class='defaultcheckbox'>&nbsp;&nbsp;$dsb2</td>
													</tr>";
												$_POST['totalc'] = $_POST['totalc'] + $valores;
												$_POST['totalcf'] = number_format($_POST['totalc'],2,".",","); 
												$i += 1;
												$aux = $iter;
												$iter = $iter2;
												$iter2 = $aux;
											}break;
											//SENA
											case $tsenab:{
												$dsb="";
												$dsb2="<img src='imagenes/dinerob1.png' title='NO PAGO' style='width:18px;' />";
												if($row2[5] == 'P'){
													$dsb = " disabled";
													$dsb2 = "<img src='imagenes/dinero1.png' title='PAGO' style='width:18px' />";	
												}
												$ch = esta_en_array($_POST['conciliados'], $i);
												if($ch == 1){$chk = "checked"; }
												$numcc = $row2[4];
												$ctapres = $row2[6];
												$valores = $row2[3];
												$ntercero = buscatercero($sena);
												$idnomina = $vfuentes = $vprogramatico = $vbpin = $vproyecto = $vproyecto = '';
												$idnomina = $row2[7];
												$vfuentes = $row2[10];
												$vprogramatico = $row2[11];
												$vbpin = $row2[12];
												$vproyecto = $row2[17];
												$vsecpresu = $row2[18];
												echo "
													<input type='hidden' name='tdet[]' value='F'/>
													<input type='hidden' name='dcuentas[]' value='$sena'/>
													<input type='hidden' name='dncuentas[]' value='$ntercero'/>
													<input type='hidden' name='drecursos[]' value='$ctapres'/>
													<input type='hidden' name='dccs[]' value='$numcc'/>
													<input type='hidden' name='dvalores[]' value='".round($valores, 0, PHP_ROUND_HALF_UP)."'/>
													<input type='hidden' name='detalles[]' value='$row2[2]'>
													<input type='hidden' name='iddescuento[]' value='$row2[1]'>
													
													<input type='hidden' name='nvidborrar[]' value='$idnomina'>
													<input type='hidden' name='nvfuente[]' value='$vfuentes'>
													<input type='hidden' name='nvprogramatico[]' value='$vprogramatico'>
													<input type='hidden' name='nvbpin[]' value='$vbpin'>
													<input type='hidden' name='nvproyecto[]' value='$vproyecto'>
													<input type='hidden' name='nvsecpresu[]' value='$vsecpresu'>
										
													<tr class='$iter' id='filas$i'>
														<td>$i F</td>
														<td>$sena</td>
														<td>$ntercero</td>
														<td>$numcc</td>
														<td>$vatipa $rowpf[1]%</td>
														<td>$ctapres</td>
														<td style='text-align:right;'>$ ".number_format(round($valores, 0, PHP_ROUND_HALF_UP),2,'.',',')."</td>
														<td><input type='checkbox' name='conciliados[]' value='$i' $chk onClick='marcar($k,$i);' $dsb class='defaultcheckbox'>&nbsp;&nbsp;$dsb2</td>
													</tr>";
												$_POST['totalc'] = $_POST['totalc']+$valores;
												$_POST['totalcf']=number_format($_POST['totalc'],2,".",",");
												$i += 1;
												$aux = $iter;
												$iter = $iter2;
												$iter2 = $aux;
											}break;
											//Institutos Tecnicos
											case $titib:{
												$dsb = "";
												$dsb2 = "<img src='imagenes/dinerob1.png' title='NO PAGO' style='width:18px;' />";
												if($row2[5] == 'P'){
													$dsb=" disabled";
													$dsb2="<img src='imagenes/dinero1.png' title='PAGO' style='width:18px' />";	
												}	
												$ch = esta_en_array($_POST['conciliados'], $i);
												if($ch == 1){
													$chk = "checked";
												}
												$numcc = $row2[4];
												$ctapres = $row2[6];
												$valores = $row2[3];
												$ntercero = buscatercero($iti);
												$idnomina = $vfuentes = $vprogramatico = $vbpin = $vproyecto = $vproyecto = '';
												$idnomina = $row2[7];
												$vfuentes = $row2[10];
												$vprogramatico = $row2[11];
												$vbpin = $row2[12];
												$vproyecto = $row2[17];
												$vsecpresu = $row2[18];
												echo "
													<input type='hidden' name='tdet[]' value='F'/>
													<input type='hidden' name='dcuentas[]' value='$iti'/>
													<input type='hidden' name='dncuentas[]' value='$ntercero'/>
													<input type='hidden' name='dccs[]' value='$numcc'/>
													<input type='hidden' name='drecursos[]' value='$ctapres'/>
													<input type='hidden' name='dvalores[]' value='".round($valores, 0, PHP_ROUND_HALF_UP)."'/>
													<input type='hidden' name='detalles[]' value='$row2[2]'/>
													<input type='hidden' name='iddescuento[]' value='$row2[1]'/>
													
													<input type='hidden' name='nvidborrar[]' value='$idnomina'>
													<input type='hidden' name='nvfuente[]' value='$vfuentes'>
													<input type='hidden' name='nvprogramatico[]' value='$vprogramatico'>
													<input type='hidden' name='nvbpin[]' value='$vbpin'>
													<input type='hidden' name='nvproyecto[]' value='$vproyecto'>
													<input type='hidden' name='nvsecpresu[]' value='$vsecpresu'>
										
													<tr class='$iter'  id='filas$i'>
														<td>$i F</td>
														<td>$iti</td>
														<td>$ntercero</td>
														<td>$numcc</td>
														<td>$vatipa $rowpf[1]%</td>
														<td>$ctapres</td>
														<td style='text-align:right;'>$ ".number_format(round($valores, 0, PHP_ROUND_HALF_UP),2,'.',',')."</td>
														<td><input type='checkbox' name='conciliados[]' value='$i' $chk onClick='marcar($k,$i);' $dsb class='defaultcheckbox'>&nbsp;&nbsp;$dsb2</td>
													</tr>";
												$_POST['totalc'] = $_POST['totalc'] + $valores;
												$_POST['totalcf'] = number_format($_POST['totalc'],2,".",",");
												$i += 1;
												$aux = $iter;
												$iter = $iter2;
												$iter2 = $aux;
											}break; 
											//ESAP
											case $tesapb:{
												$dsb = "";
												$dsb2 = "<img src='imagenes/dinerob1.png' title='NO PAGO' style='width:18px;' />";
												if($row2[5] == 'P'){
													$dsb = " disabled";
													$dsb2 = "<img src='imagenes/dinero1.png' title='PAGO' style='width:18px' />";	
												}	
												$ch = esta_en_array($_POST['conciliados'], $i);
												if($ch == 1){
													$chk = "checked";
												}
												$numcc = $row2[4];
												$ctapres = $row2[6];
												$valores = $row2[3];
												$ntercero = buscatercero($esap);
												$idnomina = $vfuentes = $vprogramatico = $vbpin = $vproyecto = $vproyecto = '';
												$idnomina = $row2[7];
												$vfuentes = $row2[10];
												$vprogramatico = $row2[11];
												$vbpin = $row2[12];
												$vproyecto = $row2[17];
												$vsecpresu = $row2[18];
												echo "
													<input type='hidden' name='tdet[]' value='F'/>
													<input type='hidden' name='dcuentas[]' value='$esap'/>
													<input type='hidden' name='dncuentas[]' value='$ntercero'/>
													<input type='hidden' name='dccs[]' value='$numcc'/>
													<input type='hidden' name='drecursos[]' value='$ctapres'/>
													<input type='hidden' name='dvalores[]' value='".round($valores, 0, PHP_ROUND_HALF_UP)."'/>
													<input type='hidden' name='detalles[]' value='$row2[2]'>
													<input type='hidden' name='iddescuento[]' value='$row2[1]'>
													
													<input type='hidden' name='nvidborrar[]' value='$idnomina'>
													<input type='hidden' name='nvfuente[]' value='$vfuentes'>
													<input type='hidden' name='nvprogramatico[]' value='$vprogramatico'>
													<input type='hidden' name='nvbpin[]' value='$vbpin'>
													<input type='hidden' name='nvproyecto[]' value='$vproyecto'>
													<input type='hidden' name='nvsecpresu[]' value='$vsecpresu'>
										
													<tr class='$iter'  id='filas$i'>
														<td>$i F</td>
														<td>$esap</td>
														<td>$ntercero</td>
														<td>$numcc</td>
														<td>$vatipa $rowpf[1]%</td>
														<td>$ctapres</td>
														<td style='text-align:right;'>$ ".number_format(round($valores, 0, PHP_ROUND_HALF_UP),2,'.',',')."</td>
														<td><input type='checkbox' name='conciliados[]' value='$i' $chk onClick='marcar($k,$i);' $dsb class='defaultcheckbox'>&nbsp;&nbsp;$dsb2</td>
													</tr>";
												$_POST['totalc'] = $_POST['totalc'] + $valores;
												$_POST['totalcf'] = number_format($_POST['totalc'],2,".",",");
												$i += 1;
												$aux = $iter;
												$iter = $iter2;
												$iter2 = $aux;
											}break; 
											//ARL
											case $tarpb:{
												$dsb = "";
												$dsb2 = "<img src='imagenes/dinerob1.png' title='NO PAGO' style='width:18px;' />";
												if($row2[5] == 'P')
												{
													$dsb = " disabled";
													$dsb2 = "<img src='imagenes/dinero1.png' title='PAGO' style='width:18px' />";	
												}
												$ch = esta_en_array($_POST['conciliados'], $i);
												if($ch == 1){$chk="checked";}	
												$numcc = $row2[4];
												$ctapres = $row2[6];
												$ntercero = buscatercero($arp);
												$valores = $row2[3];
												$idnomina = $vfuentes = $vprogramatico = $vbpin = $vproyecto = $vproyecto = '';
												$idnomina = $row2[7];
												$vfuentes = $row2[10];
												$vprogramatico = $row2[11];
												$vbpin = $row2[12];
												$vproyecto = $row2[17];
												$vsecpresu = $row2[18];
												echo "
													<input type='hidden' name='tdet[]' value='F'/>
													<input type='hidden' name='dcuentas[]' value='$arp'/>
													<input type='hidden' name='dncuentas[]' value='$ntercero'/>
													<input type='hidden' name='dccs[]' value='$numcc'/>
													<input type='hidden' name='drecursos[]' value='$ctapres'/>
													<input type='hidden' name='dvalores[]' value='".round($valores, 0, PHP_ROUND_HALF_UP)."'/>
													<input type='hidden' name='detalles[]' value='$row2[2]'>
													<input type='hidden' name='iddescuento[]' value='$row2[1]'>
													
													<input type='hidden' name='nvidborrar[]' value='$idnomina'>
													<input type='hidden' name='nvfuente[]' value='$vfuentes'>
													<input type='hidden' name='nvprogramatico[]' value='$vprogramatico'>
													<input type='hidden' name='nvbpin[]' value='$vbpin'>
													<input type='hidden' name='nvproyecto[]' value='$vproyecto'>
													<input type='hidden' name='nvsecpresu[]' value='$vsecpresu'>
													
													<tr class='$iter' id='filas$i'>
														<td>$i F</td>
														<td>$arp</td>
														<td>$ntercero</td>
														<td>$numcc</td>
														<td>$vatipa $rowpf[1]%</td>
														<td>$ctapres</td>
														<td style='text-align:right;'>$ ".number_format(round($valores, 0, PHP_ROUND_HALF_UP),2,'.',',')."</td>
														<td><input type='checkbox' name='conciliados[]' value='$i' $chk onClick='marcar($k,$i);' $dsb class='defaultcheckbox'>&nbsp;&nbsp;$dsb2</td>
													</tr>";
												$_POST['totalc'] = $_POST['totalc'] + $valores;
												$_POST['totalcf'] = number_format($_POST['totalc'],2,".",",");
												$i += 1;
												$aux = $iter;
												$iter = $iter2;
												$iter2 = $aux;
											}break; 
										}
									}
									$resultado = convertir($_POST['totalc']);
									$_POST['letras']=$resultado." PESOS M/CTE";
									echo "
									<tr class='saludo3'>
										<td colspan='4'></td>
										<td style='text-align:right;'>Total:</td>
										<td><input name='totalcf' type='text' value='$_POST[totalcf]' readonly style='text-align:right;'><input name='totalc' type='hidden' value='$_POST[totalc]'></td>
									</tr>
									<tr>
										<td  class='saludo1'>Son:</td>
										<td colspan='5' class='saludo1'><input name='letras' type='text' value='$_POST[letras]' style='width:100%' class='inpnovisibles'></td>
									</tr>";
								?>
								<script>
										document.form2.valor.value=<?php echo $_POST['totalc'];?>;
								</script>
							</table>
						</div>
					</div>	
				</div>
				<div class="tab">
					<input type="radio" id="tab-3" name="tabgroup1" value="3" <?php echo $check3;?>>
					<label for="tab-3">Egreso</label>
					<div class="content" style="overflow-x:hidden;"> 
						<table class="inicio">
							<tr>
								<td colspan="8" class="titulos">Comprobante de Egreso</td>
								<td class="cerrar" style="width:7%;"><a onClick="location.href='teso-principal.php'">&nbsp;Cerrar</a></td>
							</tr>
							<tr>
								<td style="width:10%;" class="saludo1">No Egreso:</td>
								<td style="width:15%;">
									<input type="hidden" name="cuentapagar" value="<?php echo $_POST['cuentapagar']?>"/>
									<input type="text" name="egreso" id="egreso" style="width:35%;" value="<?php echo $_POST['egreso']?>" onKeyUp="return tabular(event,this)" onKeyDown="mascara(this,'/',patron,true)" onBlur="buscarp(event)" readonly ></td>
								<td class="saludo1" style="width:5%;">Fecha: </td>
								<td style="width:15%;">
									<input id="fc_1198971546" style="width:45%;" name="fecha" type="text" value="<?php echo $_POST['fecha']?>" title="DD/MM/YYYY" maxlength="10" onKeyDown="mascara(this,'/',patron,true)" onKeyPress="javascript:return solonumeros(event)" onKeyUp="return tabular(event,this)">&nbsp;<a href="#" onClick="displayCalendarFor('fc_1198971546');"> <img src="imagenes/buscarep.png"></a>
								</td>
								<td style="width:8%;" class="saludo1">Modo de Pago:</td>
								<td style="width:10%;">
									<select name="modopago" onChange="validar();" ="return tabular(event,this)" style="width:100%">
										<?php
											if($_POST['modopago'] == 'CSF'){echo'<option value="CSF" selected>Con SF</option>'; }
											else {echo'<option value="CSF">Con SF</option>'; }
											if($_POST['modopago'] == 'SSF') {echo'<option value="SSF" selected>Sin SF</option>';}
											else {echo'<option value="SSF">Sin SF</option>';}
										?>
									</select>
								</td> 
								<?php
									if($_POST['modopago'] == 'CSF'){
										echo "
											<td style='width:8%;' class='saludo1'>Forma de Pago:</td>
											<td style='width:40%;'>
												<select name='tipop' onChange='validar();' onKeyUp='return tabular(event,this)'>
													<option value=''>Seleccione ...</option>";
										if($_POST['tipop'] == 'cheque'){echo'<option value="cheque" selected>Cheque</option>'; }
										else {echo'<option value="cheque">Cheque</option>'; }
										if($_POST['tipop'] == 'transferencia') {echo'<option value="transferencia" selected>Transferencia</option>';}
										else {echo'<option value="transferencia">Transferencia</option>';}
										if($_POST['tipop'] == 'caja') {echo'<option value="caja" selected>Otros</option>';}
										else {echo'<option value="caja">Otros</option>';}
										echo "	</select>
											</td>";
									}else{
										echo"<input type='hidden' name='tipop' id='tipop' value='caja'>";
									}
								?>   
							</tr>
							<tr>
								<td style="width:10%;" class="saludo1">Tercero:</td>
								<td >
									<input type="text" name="tercero" id="tercero" onKeyUp="return tabular(event,this)" onBlur="buscater(event)" value="<?php echo $_POST['tercero']?>" style="width:80%;">&nbsp;
									<a onClick="despliegamodal2('visible','3');"><img src="imagenes/find02.png" style="width:20px;cursor:pointer;"></a>
								</td>
								<td colspan="4">
									<input type="text" name="ntercero" id="ntercero" value="<?php echo $_POST['ntercero']?>" style="width:59%" readonly>
									<input type="hidden" value="0" name="bt">
								</td>
							</tr>
							<tr>
								<td style="width:10%;" class="saludo1">Concepto:</td>
								<td colspan="3">
									<input type="text"  style="width:100%;" name="concepto" id="concepto" value="<?php echo $_POST['concepto']?>" ></td>
								<td class="saludo1">Valor a Pagar:</td>
								<td><input name="valorpagar" type="text" id="valorpagar" onKeyUp="return tabular(event,this)" value="<?php echo $_POST['valorpagar']?>"  readonly> </td>
							</tr>
							<?php 
								if($_POST['modopago'] == 'CSF'){
									//**** if del cheques
									if($_POST['tipop']=='cheque'){
										echo "
										<tr>
											<td style='width:10%;' class='saludo1'>Cuenta :</td>
											<td>
												<input type='text' name='cb' id='cb' value='".$_POST['cb']."' style='width:80%;'/>&nbsp;
												<a onClick=\"despliegamodal2('visible','2');\"  style='cursor:pointer;' title='Listado Cuentas Bancarias'>
													<img src='imagenes/find02.png' style='width:20px;'/>
												</a>
											</td>
											<td colspan='2'>
													<input type='text' id='nbanco' name='nbanco' style='width:100%;' value='".$_POST['nbanco']."' readonly>
											</td>
											<td class='saludo1'>Cheque:</td>
											<td>
												<input type='text' id='ncheque' name='ncheque' value='".$_POST['ncheque']."' style='width:30%;'>
											</td>
											<input type='hidden' name='banco' id='banco' value='".$_POST['banco']."'/>
											<input type='hidden' id='ter' name='ter' value='".$_POST['ter']."'/>
										</tr>";
										if($_POST['cb'] != ""){
											$sqlc="SELECT cheque FROM tesocheques WHERE cuentabancaria = '".$_POST['cb']."' AND estado = 'S' ORDER BY cheque asc";
											$resc = mysqli_query($linkbd,$sqlc);
											$rowc = mysqli_fetch_row($resc);
											if($rowc[0]!=''){echo "<script>document.form2.ncheque.value='$rowc[0]';</script>";}
										}
									}//cierre del if de cheques
									//**** if del transferencias
									if($_POST['tipop']=='transferencia'){
										
										echo "<tr>
										<td style='width:10%;' class='saludo1'>Cuenta :</td>
										<td>
											<input type='text' name='cb' id='cb' value='$_POST[cb]' style='width:80%;'/>&nbsp;
											<a onClick=\"despliegamodal2('visible','1');\"  style='cursor:pointer;' title='Listado Cuentas Bancarias'>	
												<img src='imagenes/find02.png' style='width:20px;'/>
											</a>
										</td>
										<td colspan='2'>
												<input type='text' id='nbanco' name='nbanco' style='width:100%;' value='".$_POST['nbanco']."' readonly>
										<td class='saludo1'>No Transferencia:</td>
										<td >
											<input type='text' id='ntransfe' name='ntransfe' value='".$_POST['ntransfe']."' style='width:30%;'>
										</td>
											<input type='hidden' name='banco' id='banco' value='".$_POST['banco']."'/>
											<input type='hidden' id='ter' name='ter' value='".$_POST['ter']."'/>
										</tr>";
									}//cierre del if de transferencia
								}else{
									echo "
										<tr>
											<td style='width:10%;' class='saludo1'>Cuenta :</td>
											<td>
												<input type='text' name='cb' id='cb' value='".$_POST['cb']."' style='width:80%;'/>&nbsp;
												<a onClick=\"despliegamodal2('visible','5');\"  style='cursor:pointer;' title='Listado Cuentas Bancarias'>
													<img src='imagenes/find02.png' style='width:20px;'/>
												</a>
											</td>
											<td colspan='2'>
													<input type='text' id='nbanco' name='nbanco' style='width:100%;' value='".$_POST['nbanco']."' readonly>
											</td>
										</tr>";
								}
								if($_POST['bt']=='1')//***** busca tercero
								{
									$nresul = buscatercero($_POST['tercero']);
									if($nresul != ''){$_POST['ntercero'] = $nresul;}
									else
									{
										$_POST['ntercero'] = "";
										echo"
											<script>
												alert('Tercero Incorrecto o no Existe');
												document.form2.tercero.value='';
												document.form2.tercero.focus();
											</script>";
									}
								}
							?>
						</table>
						<table class="inicio">
							<tr class="titulos2">
								<td>No /Id Descuento</td>
								<td>Nit</td>
								<td>Tercero</td>
								<td>CC</td>
								<td>Cta Presupuestal</td>
								<td>Sec. Presupuestal</td>
								<td>Programatico</td>
								<td>fuente</td>
								<td>Bpin</td>
								<td>Proyecto</td>
								<td>Valor<input type='hidden' name='eliminad' id='eliminad'></td>
							</tr>
							<?php
								$totalpagos=0;
								$iter="zebra1";  
								$iter2="zebra2";
								$cutotal=0;
								foreach($_POST['conciliados'] as $jp)
								{
									$nid = $jp;
									$jp-=1;
									
									if ($_POST['tdet'][$jp]=="nn")
									{
										echo "
											<input type='hidden' name='valorarl' value='".$_POST['valarl'][$jp]."'/>
											<input type='hidden' name='valorsalud' value='".$_POST['valsalud'][$jp]."'/>
											<input type='hidden' name='valorpension' value='".$_POST['valpension'][$jp]."'/>
											<input type='hidden' name='valorfondosol' value='".$_POST['valfondosol'][$jp]."'/>
											<input type='hidden' name='valordeducciones' value='".$_POST['valdeducciones'][$jp]."'/>
										";
									}
									if($_POST['tdet'][$jp]!='PR' && $_POST['tdet'][$jp]!='SE' && $_POST['tdet'][$jp]!='SR' && $_POST['tdet'][$jp]!='PE' && $_POST['tdet'][$jp]!='FS' && $_POST['tdet'][$jp]!='F' && $_POST['tdet'][$jp]!='DS' && $_POST['tdet'][$jp]!='DR')
									{
										$sqlr="SELECT * FROM humnomina WHERE id_nom='".$_POST['orden']."'";
										$resp = mysqli_query($linkbd,$sqlr);
										$row =mysqli_fetch_row($resp);
										if ($concepegre == '') {$concepegre="Nomina $_POST[orden] Mes $row[3] Vigencia $row[7]";}
										echo"
											<script>
												document.form2.tercero.value='".$_POST['dcuentas'][$jp]."';
												document.form2.ntercero.value='".$_POST['dncuentas'][$jp]."';
												document.form2.concepto.value='$concepegre';
											</script>";
										
										$sqlrtt="SELECT SUM(netopagar), cc,tipopago, cedulanit, estado, SUM(totaldev), salbas, SUM(arp), SUM(salud), SUM(pension), SUM(fondosolid), SUM(otrasdeduc), id FROM humnomina_det WHERE id_nom='".$_POST['orden']."' AND idfuncionario = '".$_POST['idfuncionario'][$jp]."' GROUP BY tipopago,idfuncionario ORDER BY cedulanit,tipopago";
										$restt= mysqli_query($linkbd,$sqlrtt);
										while($rowtt=mysqli_fetch_row($restt))
										{
											if($rowtt[0]>0)
											{
												$ntercero = buscatercero($rowtt[3]);
												$tipopresupuesto = buscartipovinculacion($_POST['orden'], $rowtt[2], $_POST['idfuncionario'][$jp]);
												$ctapres = buscarcuentanuevocatalogo1($rowtt[2],$tipopresupuesto[0]);
												$numseccionpresu = itemfuncionarios($_POST['idfuncionario'][$jp],'42');
												if($tipopresupuesto[0] == 'IN' || $tipopresupuesto[0] == 'IT')
												{
													$numproyecto = itemfuncionarios($_POST['idfuncionario'][$jp],'31');
													$numprogramatico = itemfuncionarios($_POST['idfuncionario'][$jp],'43');
													$numfuente = itemfuncionarios($_POST['idfuncionario'][$jp],'44');
													$numbpin = nombrebpim($numproyecto);
												}
												else
												{
													$numproyecto = $numprogramatico = $numbpin = '';
													$numfuente = buscarfuentespres($ctapres, $numseccionpresu, $_POST['vigencia'], $tipopresupuesto[0], $_POST['idfuncionario'][$jp], $rowtt[0]);
												}
												$tipopago = nombrevariblespagonomina($rowtt[2]);
												echo "
												<input type='hidden' name='tedet[]' value='$rowtt[2]'/>
												<input type='hidden' name='idedescuento[]' value=''/>
												<input type='hidden' name='decuentas[]' value='$rowtt[3]'/>
												<input type='hidden' name='ddescuentos[]' value='".$_POST['ddescuentos'][$jp]."'/>
												<input type='hidden' name='dencuentas[]' value='$ntercero'/>
												<input type='hidden' name='deccs[]' value='$rowtt[1]'/>
												<input type='hidden' name='derecursos[]' value='$ctapres'/>
												<input type='hidden' name='devalores[]' value='$rowtt[0]'>
												<input type='hidden' name='salariodevengado[]' value='$rowtt[5]'/>
												<input type='hidden' name='codigofuncionario[]' value='".$_POST['idfuncionario'][$jp]."'/>
												<input type='hidden' name='codsecpresupuestal[]' value='$numseccionpresu'/>
												<input type='hidden' name='codprogramatico[]' value='$numprogramatico'/>
												<input type='hidden' name='codproyecto[]' value='$numproyecto'/>
												<input type='hidden' name='codfuente[]' value='$numfuente'/>
												<input type='hidden' name='savbpin[]' value='$numbpin'/>
												<input type='hidden' name='codborrar[]' value=''/>
												
												<tr class='$iter'>
													<td>$nid-$rowtt[2]-$tipopago </td>
													<td>$rowtt[3]</td>
													<td>$ntercero</td>
													<td>$rowtt[1]</td>
													<td>$ctapres</td>
													<td>$numseccionpresu</td>
													<td>$numprogramatico</td>
													<td>$numfuente</td>
													<td>$numbpin</td>
													<td>$numproyecto</td>
													<td style='text-align:right;'>$ ".number_format($rowtt[0],2,'.',',')."</td>
												</tr>";
												$totalpagos=$totalpagos+($rowtt[0]);
												$aux=$iter2;
												$iter2=$iter;
												$iter=$aux;
											}
										}
									}
									else
									{
										$ctapres = $numseccionpresu = $numprogramatico = $numproyecto = $numfuente = $numbpin = $numborrar = '';
										$ctapres = $_POST['drecursos'][$jp];
										$numseccionpresu = $_POST['nvsecpresu'][$jp];
										$numprogramatico = $_POST['nvprogramatico'][$jp];
										$numproyecto = $_POST['nvproyecto'][$jp];
										$numfuente = $_POST['nvfuente'][$jp];
										$numbpin = $_POST['nvbpin'][$jp];
										$numborrar = $_POST['nvidborrar'][$jp];
										echo "
										<input type='hidden' name='tedet[]' value='".$_POST['tdet'][$jp]."'/>
										<input type='hidden' name='idedescuento[]' value='".$_POST['iddescuento'][$jp]."'/>
										<input type='hidden' name='decuentas[]' value='".$_POST['dcuentas'][$jp]."'/>
										<input type='hidden' name='ddescuentos[]' value='".$_POST['ddescuentos'][$jp]."'/>
										<input type='hidden' name='dencuentas[]' value='".$_POST['dncuentas'][$jp]."'/>
										<input type='hidden' name='deccs[]' value='".$_POST['dccs'][$jp]."'/>
										<input type='hidden' name='derecursos[]' value='$ctapres'/>
										<input type='hidden' name='devalores[]' value='".$_POST['dvalores'][$jp]."'>
										<input type='hidden' name='salariodevengado[]' value='".$_POST['dvalores'][$jp]."'/>
										<input type='hidden' name='codigofuncionario[]' value='".$_POST['idfuncionario'][$jp]."'/>
										<input type='hidden' name='codsecpresupuestal[]' value='$numseccionpresu'/>
										<input type='hidden' name='codprogramatico[]' value='$numprogramatico'/>
										<input type='hidden' name='codproyecto[]' value='$numproyecto'/>
										<input type='hidden' name='codfuente[]' value='$numfuente'/>
										<input type='hidden' name='codbpin[]' value='$numbpin'/>
										<input type='hidden' name='codborrar[]' value='$numborrar'/>
										<tr class='$iter'>
											<td>$nid-".$_POST['tdet'][$jp]." ".$_POST['iddescuento'][$jp]."</td>
											<td>".$_POST['dcuentas'][$jp]."</td>
											<td>".$_POST['dncuentas'][$jp]."</td>
											<td>".$_POST['dccs'][$jp]."</td>
											<td>$ctapres</td>
											<td>$numseccionpresu</td>
											<td>$numprogramatico</td>
											<td>$numfuente</td>
											<td>$numbpin</td>
											<td>$numproyecto</td>
											<td style='text-align:right;'>$ ".number_format($_POST['dvalores'][$jp],2,'.',',')."</td>
										</tr>";
										$totalpagos = $totalpagos + ($_POST['dvalores'][$jp]);
										$aux = $iter2;
										$iter2 = $iter;
										$iter = $aux;
									}
								}
								$_POST['valoregreso'] = $totalpagos;
								echo"
									<script>
										document.form2.valorpagar.value='$totalpagos';
										document.form2.valoregreso.value='$totalpagos';
									</script>";
								$_POST['letrasegreso'] = convertir($totalpagos);
								echo "
								<tr class='$iter' style='text-align:right;font-weight:bold;'>
									<td colspan='10'>Total:</td>
									<td>$ ".number_format($totalpagos,2,'.',',')."</td>
								</tr>
								<input type='hidden' name='letrasegreso' value='".$_POST['letrasegreso']."'>
								<tr class='titulos2'>
									<td>Son:</td>
									<td colspan='10'>".$_POST['letrasegreso']."</td>
								</tr>";
							?>
						</table>
					</div>
				</div> 
			</div>
			<div id="bgventanamodal2">
				<div id="ventanamodal2">
					<IFRAME  src="" name="buster" marginWidth=0 marginHeight=0 frameBorder=0 id="ventana2" frameSpacing=0 style="left:500px; width:900px; height:500px; top:200;">
					</IFRAME>
				</div>
			</div>
		</form>
	</body>
</html>