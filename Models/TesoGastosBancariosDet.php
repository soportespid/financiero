<?php
	/**
	 * Modelo de Tabla tesogastosbancarios_det
	 * Almacena datos de la relacion de los detalles de gasto bancarios
	 */
	/* require(VENDOR_PATH.'autoload.php');
	use Illuminate\Database\Eloquent\Model; */

	class TesoGastosBancariosDet{
		/* protected $table = 'tesogastosbancarios_det';
		protected $primaryKey = 'id_det';
		public $timestamps = false; */

		public static function gastosBancarios($condiciones){

			$linkbd = conectar_v7();
			$linkbd -> set_charset("utf8");

			$sqlr = "SELECT concepto, cuentapres, tipo, fuente FROM tesogastosbancarios_det AS TB1, tesogastosbancarios AS TB2 WHERE TB1.codigo = TB2.codigo AND TB1.tipoconce = '$condiciones[tipoconce]' AND TB1.modulo = '$condiciones[modulo]' AND TB1.codigo = '$condiciones[codigo]' AND TB1.estado = '$condiciones[estado]' AND TB1.vigencia = '$condiciones[vigencia]'";
			$res = mysqli_query($linkbd, $sqlr);
			$row = mysqli_fetch_assoc($res);

			return $row;

		}
	}