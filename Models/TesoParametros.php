<?php
	/**
	 * Modelo de Tabla tesoparametros
	 * Almacena datos de la relacion de los parametros estandares de tesoreria
	 */
	/* require(VENDOR_PATH.'autoload.php');
	use Illuminate\Database\Eloquent\Model; */


	class TesoParametros{
		/* protected $table = 'tesoparametros';
		protected $primaryKey = 'id';
		public $timestamps = false; */
		
		public static function parametros($condiciones)
		{
			$linkbd = conectar_v7();
			$linkbd -> set_charset("utf8");
			$parametros = [];

			$sqlr = "SELECT * FROM tesoparametros WHERE estado = '$condiciones[estado]' LIMIT 1";
			$res = mysqli_query($linkbd, $sqlr);
			$row = mysqli_fetch_assoc($res);
			
			return $row;
		}

		public static function findParametro($id)
		{
			$linkbd = conectar_v7();
			$linkbd -> set_charset("utf8");
			$parametros = [];

			$sqlr = "SELECT * FROM tesoparametros WHERE id = '$id'";
			$res = mysqli_query($linkbd, $sqlr);
			$row = mysqli_fetch_assoc($res);
			
			return $row;
		}

		public static function saveParametros($datosSave)
		{
			$linkbd = conectar_v7();
			$linkbd -> set_charset("utf8");

			$tam = count($datosSave);
			$cont = 0;
			$campos = '';
			$data = '';
			foreach($datosSave as $key => $dato){
				$cont++;
				if($cont == $tam){
					$campos .= $key;
					$data .= "'".$dato."'";
				}else{
					$campos .= $key.', ';
					$data .= "'".$dato."', ";
				}
			}
			/* var_dump($data); */
			$sqlrD = "DELETE FROM tesoparametros";
			mysqli_query($linkbd, $sqlrD);

			$sqlr = "INSERT INTO tesoparametros($campos) VALUES ($data)";
			try{
				mysqli_query($linkbd, $sqlr);
				return 1;
			}catch(Exception $e){
				return 0;
			}
			//$sqlr = "INSERT INTO dominios('nombre_dominio', 'valor_inicial', valor_final, tipo) VALUES('$datosSave[nombre_dominio]', '$datosSave[valor_inicial]', '$datosSave[valor_final]', '$datosSave[tipo]' )";
			//
			
		}
	}
