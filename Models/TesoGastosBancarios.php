<?php
	/**
	 * Modelo de Tabla tesogastosbancarios
	 * Almacena datos de la relacion de los gasto bancarios
	 */
	/* require(VENDOR_PATH.'autoload.php');
	use Illuminate\Database\Eloquent\Model; */

	class TesoGastosBancarios{
		/* protected $table = 'tesogastosbancarios';
		protected $primaryKey = 'codigo';
		public $timestamps = false; */

		public static function gastosBancarios($condiciones){
			$linkbd = conectar_v7();
			$linkbd -> set_charset("utf8");
			$gastosBancarios = [];

			$sqlr = "SELECT * FROM tesogastosbancarios WHERE estado = '$condiciones[estado]'";
			$res = mysqli_query($linkbd, $sqlr);
			while($row = mysqli_fetch_assoc($res)){
				array_push($gastosBancarios, $row);
			}
			
			return $gastosBancarios;
		}
	}