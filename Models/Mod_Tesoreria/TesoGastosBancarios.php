<?php
	require(VENDOR_PATH.'autoload.php');
	use Illuminate\Database\Eloquent\Model;

	class TesoGastosBancarios extends Model
	{
		protected $table = 'tesogastosbancarios';
		protected $primaryKey = 'codigo';
		public $timestamps = false;
	}
