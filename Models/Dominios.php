<?php
	/**
	 * Modelo de Tabla dominios
	 * Almacena datos de la relacion de los registros de dominios
	 */
	/* require(VENDOR_PATH.'autoload.php');
	use Illuminate\Database\Eloquent\Model; */

	class Dominios{
		/* protected $table = 'dominios';
		protected $primaryKey = null;
		public $timestamps = false; */

		public static function dominio($condiciones)
		{
			$linkbd = conectar_v7();
			$linkbd -> set_charset("utf8");
			$ingresos = [];
			
			$sqlr = "SELECT valor_inicial, valor_final, tipo, descripcion_valor FROM dominios WHERE nombre_dominio = '$condiciones[nombre_dominio]' ORDER BY descripcion_valor DESC LIMIT 1";
			$res = mysqli_query($linkbd, $sqlr);
			$row = mysqli_fetch_assoc($res);
			
			return $row;
		}

		public static function updateDominio($condiciones, $actualizacion)
		{
			$linkbd = conectar_v7();
			$linkbd -> set_charset("utf8");

			$sqlr = "UPDATE dominios SET valor_inicial = '$actualizacion[valor_inicial]' WHERE nombre_dominio = '$condiciones[nombre_dominio]'";
			mysqli_query($linkbd, $sqlr);
		}

		public static function updateDominio2($condiciones, $actualizacion)
		{
			$linkbd = conectar_v7();
			$linkbd -> set_charset("utf8");

			$sqlr = "UPDATE dominios SET valor_inicial = '$actualizacion[valor_inicial]', descripcion_valor = '$actualizacion[descripcion_valor]' WHERE nombre_dominio = '$condiciones[nombre_dominio]'";
			mysqli_query($linkbd, $sqlr);
		}

		public static function updateDominio3($condiciones, $actualizacion)
		{
			$linkbd = conectar_v7();
			$linkbd -> set_charset("utf8");

			$sqlr = "UPDATE dominios SET valor_inicial = '$actualizacion[valor_inicial]', valor_final = '$actualizacion[valor_final]', tipo = '$actualizacion[tipo]' WHERE nombre_dominio = '$condiciones[nombre_dominio]'";
			mysqli_query($linkbd, $sqlr);
		}

		public static function saveDominio($datosSave)
		{
			$linkbd = conectar_v7();
			$linkbd -> set_charset("utf8");
			$sqlr = "INSERT INTO dominios('valor_inicial', 'nombre_dominio') VALUES('$datosSave[valor_inicial]', '$datosSave[nombre_dominio]' )";
			mysqli_query($linkbd, $sqlr);
			
		}

		public static function saveDominio2($datosSave)
		{
			$linkbd = conectar_v7();
			$linkbd -> set_charset("utf8");
			$sqlr = "INSERT INTO dominios('valor_inicial', 'nombre_dominio', descripcion_valor) VALUES('$datosSave[valor_inicial]', '$datosSave[nombre_dominio]', '$datosSave[descripcion_valor]' )";
			mysqli_query($linkbd, $sqlr);
			
		}

		public static function saveDominio3($datosSave)
		{
			$linkbd = conectar_v7();
			$linkbd -> set_charset("utf8");
			$sqlr = "INSERT INTO dominios('nombre_dominio', 'valor_inicial', valor_final, tipo) VALUES('$datosSave[nombre_dominio]', '$datosSave[valor_inicial]', '$datosSave[valor_final]', '$datosSave[tipo]' )";
			mysqli_query($linkbd, $sqlr);
			
		}
	}