<?php
	/**
	 * Modelo de Tabla tesoingresos
	 * Almacena datos de la relacion de los ingresos de tesoreria
	 */
	/* require(VENDOR_PATH.'autoload.php');
	use Illuminate\Database\Eloquent\Model; */

	class TesoIngresos{
		/* protected $table = 'tesoingresos';
		protected $primaryKey = 'codigo';
		public $timestamps = false; */

		public static function ingresos($condiciones)
		{
			$linkbd = conectar_v7();
			$linkbd -> set_charset("utf8");
			$ingresos = [];

			$sqlr = "SELECT codigo, nombre FROM tesoingresos WHERE estado = '$condiciones[estado]' ORDER BY codigo DESC";
			$res = mysqli_query($linkbd, $sqlr);
			while($row = mysqli_fetch_assoc($res)){
				array_push($ingresos, $row);
			}
			
			return $ingresos;
		}
	}
