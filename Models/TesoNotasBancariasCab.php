<?php
	/**
	 * Modelo de Tabla tesonotasbancarias_cab
	 * Almacena datos de cabecera de la notas bancarias
	 */
	/* require(VENDOR_PATH.'autoload.php');
 */
	class TesoNotasBancariasCab{

		public static function max($id){

			$linkbd = conectar_v7();
			$linkbd -> set_charset("utf8");

			$sqlr = "SELECT MAX($id) FROM tesonotasbancarias_cab";
			$res = mysqli_query($linkbd, $sqlr);
			$row = mysqli_fetch_row($res);
			
			return $row[0];
		}

		public static function min($id){

			$linkbd = conectar_v7();
			$linkbd -> set_charset("utf8");

			$sqlr = "SELECT MIN($id) FROM tesonotasbancarias_cab";
			$res = mysqli_query($linkbd, $sqlr);
			$row = mysqli_fetch_row($res);
			
			return $row[0];
		}

		public static function notaBancaria($condiciones){

			$linkbd = conectar_v7();
			$linkbd -> set_charset("utf8");

			$sqlr = "SELECT * FROM tesonotasbancarias_cab WHERE id_comp = '$condiciones[id_comp]'";
			$res = mysqli_query($linkbd, $sqlr);
			$row = mysqli_fetch_row($res);
			
			return $row;
		}

		public static function saveNotaBancariaCab($data){

			$linkbd = conectar_v7();
			$linkbd -> set_charset("utf8");

			$sqlr = "INSERT INTO tesonotasbancarias_cab ('id_comp', 'fecha', 'vigencia', 'estado', 'concepto', tipo_mov, user) VALUES ($data[id_comp], '$data[fecha]', '$data[vigencia]', '$data[estado]', '$data[concepto]', '$data[tipo_mov]', '$data[user]')";

			try{
				mysqli_query($linkbd, $sqlr);
				return 1;
			}catch(Exception $e){
				return 0;
			}

		}
		
	}
