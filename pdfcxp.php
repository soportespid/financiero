<?php
	require_once("tcpdf/tcpdf_include.php");
	require('comun.inc');
	require "funciones.inc";
	date_default_timezone_set("America/Bogota");
	session_start();
	$val = 0;
	class MYPDF extends TCPDF{
		public function Header(){
			if ($_POST['estado']=='R'){
				$this->Image('imagenes/reversado02.png',200,10,50,15);
			}
			$linkbd = conectar_v7();
			$linkbd -> set_charset("utf8");
			$sqlr="select *from configbasica where estado='S' ";
			$res=mysqli_query($linkbd, $sqlr);
			while($row=mysqli_fetch_row($res)){
				$nit=$row[0];
				$rs=$row[1];
				$nalca=$row[6];
			}
			$detallegreso = $_POST['detallegreso'];
			//Parte Izquierda
			$this->Image('imagenes/escudo.jpg',  13, 13, 20, 20, 'JPG', '', 'T', true, 300, '', false, false, 0, false, false, false);// Logo
			$this->SetFont('helvetica','B',8);
			$this->SetY(10);
			$this->RoundedRect(10, 10, 277, 25, 1,'1111');
			$this->Cell(0.1);
			$this->Cell(26,25,'','R',0,'L');
			$this->SetY(8);
			$this->SetX(80);
			$this->SetFont('helvetica','B',9);
			$this->Cell(160,15,strtoupper("$rs"),0,0,'C');
			$this->SetFont('helvetica','B',7);
			$this->SetY(12);
			$this->SetX(80);
			$this->Cell(160,15,'NIT: '.$nit,0,0,'C');
			//*****************************************************************************************************************************
			$this->SetFont('helvetica','B',9);
			$this->SetY(23);
			$this->SetX(36);
			$this->Cell(251,12,"LIQUIDACIÓN CUENTA POR PAGAR",'T',0,'C');
			$mov='';
			if(isset($_POST['movimiento'])){
				if(!empty($_POST['movimiento'])){
					if($_POST['movimiento']=='401'){
						$mov="DOCUMENTO DE REVERSION";
					}
				}
			}
			$this->SetFont('helvetica','B',6);
			$this->SetY(10);
			$this->SetX(257);
			$this->Cell(30,4," NÚMERO: ".$_POST['idcomp'],"L",0,'L');
			$this->SetY(14);
			$this->SetX(257);
			$this->Cell(35,5," FECHA: ".$_POST['fecha'],"L",0,'L');
			$this->SetY(19);
			$this->SetX(257);
			$this->Cell(35,4," VIGENCIA: ".$_POST['vigencia'],"L",0,'L');

			$this->SetFont('helvetica','B',8);
			$this->SetY(25);
			$this->cell(248,8,'NETO A PAGAR:   ',0,0,'R');
			$this->RoundedRect(257.5, 26 ,27, 6, 1,'');
			$this->cell(26,8,'$'.number_format($_POST['valorcheque'],2),0,0,'R');

			/*
			$this->SetFont('helvetica','B',10);
			$this->SetY(15);
			$this->Cell(50.1);
			$this->Cell(149,20,$mov,0,0,'C');
			//************************************
			$this->SetFont('helvetica','I',7);
			$this->SetY(27);
			$this->Cell(35);
			//$this->multiCell(110.7,3,''.strtoupper($detallegreso),'T','L');
			$this->multiCell(242,3,'','T','L');
			$this->SetFont('helvetica','B',10);
			$this->SetY(27);
			$this->Cell(237);
			$this->Cell(37.8,14,'','TL',0,'L');
			$this->SetY(27);
			$this->Cell(238);
			$this->Cell(35,5,'NUMERO : '.$_POST['idcomp'],0,0,'L');
			$this->SetY(31);
			$this->Cell(238);
			$this->Cell(35,5,'FECHA: '.$_POST['fecha'],0,0,'L');
			$this->SetY(35);
			$this->Cell(238);
			$this->Cell(35,5,'VIGENCIA: '.$_POST['vigencia'],0,0,'L');
			$this->SetY(27);
			$this->Cell(50.2);
			$this->MultiCell(105.7,4,'',0,'L');
			*/
			//**********************************************************
			$this->SetFont('times','B',10);
			$this->ln(12);
			//**********************************************************
		}
		public function Footer(){
			$linkbd = conectar_v7();
			$linkbd -> set_charset("utf8");
			$sqlr="SELECT direccion,telefono,web,email FROM configbasica WHERE estado='S'";
			$resp=mysqli_query($linkbd, $sqlr);
			$user = $_SESSION['nickusu'];
			$fecha = date("Y-m-d H:i:s");
			$ip = $_SERVER['REMOTE_ADDR'];
			$useri = $_POST['user'];
			while($row=mysqli_fetch_row($resp)){
				$direcc=strtoupper($row[0]);
				$telefonos=$row[1];
				$dirweb=strtoupper($row[3]);
				$coemail=strtoupper($row[2]);
			}
			if($direcc!=''){$vardirec="Dirección: $direcc, ";}
			else {$vardirec="";}
			if($telefonos!=''){$vartelef="Telefonos: $telefonos";}
			else{$vartelef="";}
			if($dirweb!=''){$varemail="Email: $dirweb, ";}
			else {$varemail="";}
			if($coemail!=''){$varpagiw="Pagina Web: $coemail";}
			else{$varpagiw="";}
			$this->SetFont('helvetica', 'I', 8);
			$txt = <<<EOD
			$vardirec $vartelef
			$varemail $varpagiw
			EOD;
			$this->SetFont('helvetica', 'I', 6);
			$this->Cell(277,10,'','T',0,'T');
			$this->ln(2);
			$this->Write(0, $txt , '', 0, 'C', true, 0, false, false, 0);
			$this->Cell(25, 10, 'Hecho por: '.$useri, 0, false, 'C', 0, '', 0, false, 'T', 'M');
			$this->Cell(100, 10, 'Impreso por: '.$user, 0, false, 'C', 0, '', 0, false, 'T', 'M');
			$this->Cell(30, 10, 'IP: '.$ip, 0, false, 'C', 0, '', 0, false, 'T', 'M');
			$this->Cell(102, 10, 'Fecha: '.$fecha, 0, false, 'C', 0, '', 0, false, 'T', 'M');
			$this->Cell(5, 10, 'IDEAL.10 S.A.S    Pagina '.$this->getAliasNumPage().'/'.$this->getAliasNbPages(), 0, false, 'C', 0, '', 0, false, 'T', 'M');
		}
	}
	$pdf = new MYPDF('L','mm','Letter', true, 'iso-8859-1', false);
	$pdf->SetDocInfoUnicode (true);
	// set document information
	$pdf->SetCreator(PDF_CREATOR);
	$pdf->SetAuthor('IDEALSAS');
	$pdf->SetTitle('Certificados');
	$pdf->SetSubject('Certificado de Disponibilidad');
	$pdf->SetKeywords('TCPDF, PDF, example, test, guide');
	$pdf->SetMargins(10, 38, 10);// set margins
	$pdf->SetHeaderMargin(38);// set margins
	$pdf->SetFooterMargin(17);// set margins
	$pdf->SetAutoPageBreak(TRUE, 20);// set auto page breaks
	// set some language-dependent strings (optional)
	if (@file_exists(dirname(__FILE__).'/lang/spa.php')){
		require_once(dirname(__FILE__).'/lang/spa.php');
		$pdf->setLanguageArray($l);
	}
	$pdf->AddPage();

	$pdf->cell(0.2);
	$pdf->SetFillColor(255,255,255);
	$pdf->SetFont('helvetica','B',7);
	$pdf->cell(25,4,'Registro:','LT',0,'L',0);
	$pdf->SetFont('helvetica','',7);
	$pdf->cell(15,4,''.$_POST['rp'],'T',0,'L',0);

	$pdf->SetFont('helvetica','B',7);
	$pdf->cell(25,4,'Detalle RP: ','LT',0,'L',0);
	$pdf->SetFont('helvetica','',7);
	$detallecdp = mb_substr($_POST['detallecdp'],0,191,'utf-8');
	$pdf->cell(212,4,''.ucfirst(strtolower($detallecdp)),'RT',1,'L',0);
	$pdf->cell(0.2);

	$pdf->SetFont('helvetica','B',7);
	$pdf->cell(25,4,'Beneficiario: ','LT',0,'L',0);
	$pdf->SetFont('helvetica','',7);
	$ntercero =  $_POST['ntercero'];
	$pdf->cell(115,4,''.$ntercero,'T',0,'L',0);

	$pdf->SetFillColor(255,255,255);
	$pdf->SetFont('helvetica','B',7);
	$pdf->cell(25,4,'C.C. o NIT: ','T',0,'L',0);
	$pdf->SetFont('helvetica','',7);
	$pdf->cell(112,4,''.$_POST['tercero'],'TR',1,'L',0);
	$pdf->cell(0.2);

	$pdf->SetFillColor(245,245,245);
	$detallegreso = ucfirst(strtolower($_POST['detallegreso']));
	$lineas = $pdf->getNumLines($detallegreso, 174);
	$alturadt=(4*$lineas);
	$pdf->SetFont('helvetica','B',7);
	$pdf->MultiCell(25,$alturadt,'Detalle: ', 'LB', 'J', 0, 0, '', '', true, 0, false, true, $alturadt, 'T');
	$pdf->SetFont('helvetica','',7);
	$pdf->MultiCell(252,$alturadt,"$detallegreso",'RB','L',false,1,'','',true,0,false,true,$alturadt,'T',false);

	$pdf->SetFillColor(245,245,245);
	$pdf->SetFont('helvetica','B',7);
	$pdf->cell(0.2);
	$pdf->cell(25,4,'Valor Pago:','LB',0,'L',0);
	$pdf->SetFont('helvetica','',7);
	$pdf->cell(45,4,'$'.number_format($_POST['valor'],2),'BR',0,'L',0);
	$pdf->SetFont('helvetica','B',7);
	$pdf->cell(30,4,'Retenciones: ','B',0,'L',0);
	$pdf->SetFont('helvetica','',7);
	$pdf->cell(40,4,'$'.number_format($_POST['valorretencion'],2),'BR',0,'L',0);
	$pdf->SetFont('helvetica','B',7);
	$pdf->cell(25,4,'Base Ret:','B',0,'L',0);
	$pdf->SetFont('helvetica','',7);
	$pdf->cell(45,4,'$'.number_format($_POST['base'],2),'BR',0,'L',0);
	$pdf->SetFont('helvetica','B',7);
	$pdf->cell(22,4,'Iva:','B',0,'L',0);
	$pdf->SetFont('helvetica','',7);
	$pdf->cell(45,4,'$'.number_format($_POST['iva'],2),'BR',1,'L',0);

	$pdf->ln(2);
	$y=$pdf->GetY();
	$pdf->SetY($y);

	$y=$pdf->GetY();
	$pdf->SetFillColor(222,222,222);
	$pdf->SetFont('helvetica','B',7);
	//$pdf->SetY($y);
    $pdf->Cell(0.1);
    $pdf->Cell(29,5,'Cuenta',0,0,'C',1);
	$pdf->SetY($y);
	$pdf->Cell(30);
	$pdf->Cell(17,5,'Medio pago',0,0,'C',1);
	$pdf->SetY($y);
	$pdf->Cell(48);
	$pdf->SetFont('helvetica','B',6);
	$pdf->Cell(16,5,'Vig. de gasto',0,0,'C',1);
	$pdf->SetY($y);
	$pdf->SetFont('helvetica','B',7);
	$pdf->Cell(65);
	$pdf->Cell(55,5,'Proyecto',0,0,'C',1);
	$pdf->SetY($y);
	$pdf->Cell(121);
	$pdf->Cell(29,5,'Programatico',0,0,'C',1);
	$pdf->SetY($y);
	$pdf->Cell(151);
	$pdf->Cell(29,5,'CCPET',0,0,'C',1);
	$pdf->SetY($y);
	$pdf->Cell(181);
	$pdf->Cell(29,5,'Fuente',0,0,'C',1);
	$pdf->SetY($y);
	$pdf->Cell(211);
	$pdf->Cell(29,5,'CPC',0,0,'C',1);
	$pdf->SetY($y);
	$pdf->Cell(241);
	$pdf->Cell(36,5,'Valor',0,0,'C',1);
	$pdf->SetFont('helvetica','',6);
	$cont=0;
	$pdf->ln(6);
	$tamArray = count($_POST['dcuentas']);
	for($x=0;$x<$tamArray;$x++){
		if($_POST['dvalores'][$x]>0){
			$vv=$pdf->gety();//echo $vv."<br>";
			if($vv>=170){
				$pdf->AddPage();
				$vv=$pdf->gety();
			}
			if ($con%2==0){$pdf->SetFillColor(255,255,255);}
			else {$pdf->SetFillColor(245,245,245);}
			$dvigencia =$_POST['dvigasto'][$x];
			$drecursos =$_POST['dbpim'][$x];
			$dseccionp =$_POST['dseccion'][$x];
			$dmediopago = $_POST['dmediopago'][$x];
			$dprogramatico = $_POST['dprogramatico'][$x];
			$dcuentas = $_POST['dcuentas'][$x];
			$dfuente = $_POST['drecursos'][$x];
			$dCPC = $_POST['dCPC'][$x];
			$lineaspro = $pdf-> getNumLines($drecursos, 50);
			$lineasprog = $pdf->getNumLines($dprogramatico, 30);
			$lineascc = $pdf->getNumLines($dcuentas, 30);
			$lineasfuente = $pdf ->getNumLines($dfuente, 30);
			$lineasCpc = $pdf ->getNumLines($dCPC, 30);
			/* echo $lineaspro."<br>";
			echo $lineasprog."<br>";
			echo $lineascc."<br>";
			echo $lineasfuente."<br>";
			echo "Cambia <br>"; */
			$valorMax = max($lineaspro,$lineasprog,$lineascc,$lineasfuente, $lineasCpc);
			//echo $valorMax." valor max <br>";
			$altura = $valorMax * 3;
			/* if($tamArray === 1){
				$altura = $valorMax * 5;
			} */

			//echo $altura."<br>";
			$pdf->MultiCell(30,$altura,strtolower($dseccionp),0,'C',true,0,'','',true,0,false,true,0,'L',false);
			$pdf->MultiCell(17,$altura,$dmediopago,0,'C',true,0,'','',true,0,false,true,0,'L',false);
			$pdf->MultiCell(17,$altura,$dvigencia,0,'C',true,0,'','',true,0,false,true,0,'',false);
			$pdf->MultiCell(56,$altura,strtolower($drecursos),0,'L',true,0,'','',true,0,false,true,0,'L',false);
			$pdf->MultiCell(30,$altura,$dprogramatico,0,'L',true,0,'','',true,0,false,true,0,'L',false);
			$pdf->MultiCell(31,$altura,strtolower($dcuentas),0,'L',true,0,'','',true,0,false,true,0,'L',false);
			$pdf->MultiCell(30,$altura,$dfuente,0,'L',true,0,'','',true,0,false,true,0,'L',false);
			$pdf->MultiCell(30,$altura,$dCPC,0,'L',true,0,'','',true,0,false,true,0,'B',false);
			//$pdf->MultiCell(36,10,'$'.number_format($_POST['dvalores'][$x],2),'0',1,'R',true,0,false,true,'T','0' );
			$pdf->MultiCell(36,$altura,'$'.number_format($_POST['dvalores'][$x],2),0,'R',true,1,'','',true,0,false,true,0,'M',false);
			$con=$con+1;
		}
	}
	//$pdf->ln(4);
	//echo $pdf->GetY();
	if(count($_POST['ddescuentos'])!= 0){
		$y=$pdf->GetY();
		$pdf->SetY($y);

		$y=$pdf->GetY();
		$pdf->SetFillColor(222,222,222);
		$pdf->SetFont('helvetica','B',7);
		$pdf->SetY($y);
		$pdf->Cell(0.1);
		$pdf->Cell(40,4,'Codigo ',0,0,'C',1);
		$pdf->SetY($y);
		$pdf->Cell(41);
		$pdf->Cell(140,4,'Retencion',0,0,'C',1);
		$pdf->SetY($y);
		$pdf->Cell(182);
		$pdf->Cell(35,4,'Porcentaje',0,0,'C',1);
		$pdf->SetY($y);
		$pdf->Cell(218);
		$pdf->Cell(59,4,'Valor',0,0,'C',1);
		$pdf->SetFont('helvetica','',6);
		$cont=0;
		$pdf->ln(1);
		for($x=0;$x<count($_POST['ddescuentos']);$x++){
			$pdf->ln(3.5);
			if ($con%2==0){$pdf->SetFillColor(255,255,255);}
			else {$pdf->SetFillColor(245,245,245);}
			$pdf->Cell(41,3,''.$_POST['ddescuentos'][$x],'',0,'C',1);
			$pdf->Cell(141,3,''.$_POST['dndescuentos'][$x],'',0,'L',1);
			$pdf->Cell(35,3,''.$_POST['dporcentajes'][$x].'%','',0,'C',1);
			$pdf->Cell(60,3,'$'.number_format($_POST['ddesvalores'][$x],2),'',0,'R',1);
			$con=$con+1;
		}
	}
	if(count($_POST['dcuenta'])>0){
		$pdf->ln(5);
		$pdf->SetFillColor(222,222,222);
		$pdf->SetFont('helvetica','B',10);
		$pdf->Cell(0.1);
		$pdf->Cell(199,5, 'AFECTACIÓN PRESUPUESTAL',0,0,'C',1);
		$pdf->ln(6);
		$pdf->SetFont('helvetica','',10);
		$pdf->Cell(44,6,'CUENTA ',1,0,'C',true,'',0,false,'T','C');
		$pdf->Cell(115,6,'DESCRIPCIÓN ',1,0,'C',true,'',1,false,'T','C');
		$pdf->Cell(40,6,'VALOR ',1,1,'C',true,'',1,false,'T','C');
		$pdf->ln(1);
		$pdf->SetFont('helvetica','I',10);
		$con=0;
		$ltrb = '';
		for($i=0;$i<count($_POST['dcuenta']);$i++){
			$gdcuenta[$_POST['dcuenta'][$i]][0]=$_POST['dcuenta'][$i];
			$gdcuenta[$_POST['dcuenta'][$i]][1]=$_POST['ncuenta'][$i];
			$gdcuenta[$_POST['dcuenta'][$i]][2]=$gdcuenta[$_POST['dcuenta'][$i]][2]+str_replace(',','',$_POST['rvalor'][$i]);
		}
		foreach($gdcuenta as $val){
			if ($con%2==0){$pdf->SetFillColor(245,245,245);}
			else {$pdf->SetFillColor(255,255,255);}
			$niy=$pdf->Gety();
			if($niy >= 269.5) $ltrb = 'B';
			if($niy >= 275.5) $ltrb = 'T';
			$pdf->Cell(44,3,''.$val[0],'L'.$ltrb,0,'C',true,'',1,false,'T','C');
			$pdf->Cell(115,3,''.$val[1],'L'.$ltrb,0,'L',true,'',1,false,'T','C');
			$pdf->Cell(40,3,''.number_format($val[2],2,".",","),'LR'.$ltrb,1,'R',true,'',1,false,'T','C');
			$ltrb = '';
			$con=$con+1;
		}
		if ($con%2==0){$pdf->SetFillColor(245,245,245);}
		else {$pdf->SetFillColor(255,255,255);}
		$pdf->Cell(159,6,'Total: ','TLB',0,'R',true,'',1,false,'T','C');
		$pdf->Cell(40,6,''.$_POST['varto'],'TLRB',1,'R',true,'',0,false,'T','C');
	}
	$pdf->ln(15);
	$linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");
	preg_match("/([0-9]{2})\/([0-9]{2})\/([0-9]{4})/", $_POST['fecha'],$fecha);
	$fechaf = "$fecha[3]-$fecha[2]-$fecha[1]";
	$sql = "SELECT funcionario, nomcargo, documento FROM firmaspdf_det WHERE idfirmas='9' AND estado ='S' AND fecha < '$fechaf' ORDER BY orden";
	$res= mysqli_query($linkbd, $sql);
	while ($row = mysqli_fetch_row($res)) {
		if($row[2] == 'FEP'){
			$useri = $_POST['user'];
			$sqldocu = "SELECT cc_usu FROM usuarios WHERE usu_usu = '$useri'";
			$resdocu = mysqli_query($linkbd, $sqldocu);
			$rowdocu = mysqli_fetch_row($resdocu);
			$docelabora = $rowdocu[0];
			$_POST['ppto'][] = buscar_empleado($docelabora);
			$_POST['nomcargo'][] = $row[1];
		} else if($row[2] == 'BEN') {
			$_POST['ppto'][] = $ntercero;
			$_POST['nomcargo'][] = $row[1];
		} else {
			$_POST['ppto'][] = $row[0];
			$_POST['nomcargo'][] = $row[1];
		}
	}
	for($x=0;$x<count($_POST['ppto']);$x++){
		$pdf->ln(6);
		$v=$pdf->gety();
		if($v>=180){
			$pdf->AddPage();
			$pdf->ln(15);
			$v=$pdf->gety();
		}
		$pdf->setFont('times','B',7);

		if (($x%2)==0){
			if(isset($_POST['ppto'][$x+1])){
				$pdf->Line(40,$v,130,$v);
				$pdf->Line(170,$v,260,$v);
				$v2=$pdf->gety();
				$pdf->Cell(150,4,''.$_POST['ppto'][$x],0,1,'C',false,0,0,false,'T','C');
				$pdf->Cell(150,4,''.$_POST['nomcargo'][$x],0,1,'C',false,0,0,false,'T','C');
				$pdf->SetY($v2);
				$pdf->Cell(410,4,''.$_POST['ppto'][$x+1],0,1,'C',false,0,0,false,'T','C');
				$pdf->Cell(410,4,''.$_POST['nomcargo'][$x+1],0,1,'C',false,0,0,false,'T','C');

			} else {
				$v = $v + 5;
				$pdf->ln(5);

				$pdf->Line(105,$v,195,$v);
				$pdf->Cell(280,4,''.$_POST['ppto'][$x],0,1,'C',false,0,0,false,'T','C');
				$pdf->Cell(280,4,''.$_POST['nomcargo'][$x],0,0,'C',false,0,0,false,'T','C');
			}
			$v3=$pdf->gety();
		}
		$pdf->SetY($v3);
		$pdf->SetFont('helvetica','',7);
	}
	/*$pdf->ln(15);
	$pdf->SetFont('helvetica','',7);
	$pdf->Cell(138.4,4,'Elaborado Por:____________________________________________________                     Revisado Por:____________________________________________________',0,0,'L',false,0,0,false,'T','C');
	*/
	$pdf->Output();
?>


