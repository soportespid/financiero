<?php
	header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=utf8");
	require 'comun.inc';
	require 'funciones.inc';
	session_start();
	$linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");
	cargarcodigopag($_GET['codpag'],$_SESSION["nivel"]);
	date_default_timezone_set("America/Bogota");
	titlepag();
?>
<!DOCTYPE >
<html lang=es>
	<head>
		<meta charset="utf-8"/>
		<meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1"/>
		<meta name="viewport" content="user-scalable=no">
		<title>:: IDEAL 10 - Tesoreria</title>
		<link href="css/css2z.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
		<link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
		<link href="css/css4.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
		<link href="css/tabs2.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css"/>
		<script src="sweetalert2/dist/sweetalert2.min.js"></script>
		<link rel="stylesheet" href="sweetalert2/dist/sweetalert2.min.css">
		<script type="text/javascript" src="css/programas.js"></script>
	<script>
		function buscacta(e)
		{
			if (document.form2.cuenta.value!="")
			{
				document.form2.bc.value='1';
				document.form2.submit();
			}
		}

		function buscarFuente(e)
		{
			if (document.form2.fuente.value!="")
			{		
				document.form2.bf.value='1';
				document.form2.submit();
			}
		}

		function buscacta2(e,tipo)
		{
			if (tipo == 1)
			{
				document.form2.bc.value='2';
				document.form2.submit();
			}
			if(tipo == 2)
			{
				document.form2.bc.value='3';
				document.form2.submit();
			}
			if(tipo == 3)
			{
				document.form2.bc.value='4';
				document.form2.submit();
			}
		}

		function validar()
		{
			document.form2.submit();
		}

		function validar2(){
			document.getElementById('oculto').value='7';
			document.form2.submit();
		}

		function agregardetalle()
		{
			if(document.form2.valor.value!="" &&  document.form2.concecont.value!="")
			{	 
				document.form2.agregadet.value=1;
	//			document.form2.chacuerdo.vavlue=2;
				document.form2.submit();
			}
			else {
				alert("Falta informacion para poder Agregar");
			}
		}

		function agregaFuente() {
		
			var fuente = document.getElementById('fuente').value;
			var nombreFuente = document.getElementById('nfuente').value;

			if (fuente != "" && nombreFuente != "") {

				document.form2.agregaFuente.value = 1;
				document.form2.submit();
			}
			else {
				Swal.fire('Falta información', 'Seleccióna la fuente', 'info');
			}
		}

		function eliminarFuente(pos) {

			Swal.fire({
				icon: 'question',
				title: 'Seguro que quieres eliminar?',
				showDenyButton: true,
				confirmButtonText: 'Eliminar',
				denyButtonText: 'Cancelar',
				}).then((result) => {
					if (result.isConfirmed) {
						document.form2.eliminaFuente.value=pos;
						document.form2.submit();
					}
					else {
						Swal.fire('Eliminar cancelado', '', 'info');
					}
				}
			)
		}

		function eliminar(variable)
		{
			if (confirm("Esta Seguro de Eliminar"))
			{
				document.form2.elimina.value=variable;
				//eli=document.getElementById(elimina);
				vvend=document.getElementById('elimina');
				//eli.value=elimina;
				vvend.value=variable;
				document.form2.submit();
			}
		}

		function guardar()
		{
			var codigo = document.getElementById('codigo').value;
			var nombreIngreso = document.getElementById('nombre').value;
			var precioVenta = document.getElementById('precio').value;


			if (codigo != '' && nombreIngreso != '' && precioVenta >= 0 ) {
				
				Swal.fire({
					icon: 'question',
					title: 'Seguro que quieres guardar?',
					showDenyButton: true,
					confirmButtonText: 'Guardar',
					denyButtonText: 'Cancelar',
					}).then((result) => {
						if (result.isConfirmed) {
							document.form2.oculto.value='2';
							document.form2.submit();
						} else if (result.isDenied) {
							Swal.fire('Guardar cancelado', '', 'info')
						}
					}
				)		
			}
			else {
				Swal.fire('Falta información', 'Información faltante en cabecera', 'info');
			}
		}

		function despliegamodal2(_valor)
		{
			document.getElementById("bgventanamodal2").style.visibility=_valor;
			if(_valor=="hidden")
			{
				document.getElementById('ventana2').src="";
			}
			else 
			{
				document.getElementById('ventana2').src="cuentasppto-ventana1.php?ti=1";
			}
		}

		function despliegamodal3(_valor)
		{
			document.getElementById("bgventanamodal2").style.visibility=_valor;
			if(_valor=="hidden")
			{
				document.getElementById('ventana2').src="";
			}
			else 
			{
				document.getElementById('ventana2').src="bienesTransportables-ventana.php";
			}
		}
		
		function despliegamodal4(_valor)
		{
			document.getElementById("bgventanamodal2").style.visibility=_valor;
			if(_valor=="hidden")
			{
				document.getElementById('ventana2').src="";
			}
			else 
			{
				document.getElementById('ventana2').src="servicios-ventana.php";
			}
		}

		function despliegamodal5(_valor)
		{
			document.getElementById("bgventanamodal2").style.visibility=_valor;
			if(_valor=="hidden")
			{
				document.getElementById('ventana2').src="";
			}
			else 
			{
				document.getElementById('ventana2').src="cuin-ventana.php";
			}
		}

		function despliegamodalm(_valor,_tip,mensa,pregunta,variable)
		{
			document.getElementById("bgventanamodalm").style.visibility=_valor;
			if(_valor=="hidden"){document.getElementById('ventanam').src="";}
			else
			{
				switch(_tip)
				{
					case "1":	document.getElementById('ventanam').src="ventana-mensaje1.php?titulos="+mensa;break;
					case "2":	document.getElementById('ventanam').src="ventana-mensaje3.php?titulos="+mensa;break;
					case "3":	document.getElementById('ventanam').src="ventana-mensaje2.php?titulos="+mensa;break;
					case "4":	document.getElementById('ventanam').src="ventana-consulta1.php?titulos="+mensa+"&idresp="+pregunta;break;	
					case "5":
					document.getElementById('ventanam').src="ventana-elimina1.php?titulos="+mensa+"&idresp="+pregunta+"&variable="+variable;break;	
				}
			}
		}

		function respuestaconsulta(pregunta, variable)
		{
			switch(pregunta)
			{
				case "1":	document.getElementById('oculto').value="2";
							document.form2.submit();break;
				case "2":
					document.form2.elimina.value=variable;
					//eli=document.getElementById(elimina);
					vvend=document.getElementById('elimina');
					//eli.value=elimina;
					vvend.value=variable;
					document.form2.submit();
					break;
			}
		}

		function despliegamodal7(_valor)
		{	
			document.getElementById("bgventanamodal2").style.visibility=_valor;
			if(_valor=="hidden")
			{
				document.getElementById('ventana2').src="";
			}
			else 
			{
				document.getElementById('ventana2').src="fuentes-ventana1.php";
			}
		}

		function funcionmensaje(){document.location.href = "teso-ingresosVentas.php?idr="+document.getElementById('codigo').value}
		function validafinalizar(e)
		{
			var id=e.id;
			var check=e.checked;
			if(id=='terceros'){
				document.getElementById('terceros').value=1;
			}else{
				document.form2.terceros.checked=false;
			}
			document.getElementById('oculto').value='6';
			document.form2.submit();
		}

		function iratras()
		{
			var oldURL = document.referrer;
			location.href = oldURL;
		}
	</script>
</head>
<body>
	<IFRAME src="alertas.php" name="alertas" id="alertas" style="display:none"></IFRAME>
	<span id="todastablas2"></span>
	<table>
		<tr><script>barra_imagenes("teso");</script><?php cuadro_titulos();?></tr>	 
		<tr><?php menu_desplegable("teso");?></tr>
		<tr>
			<td colspan="3" class="cinta">
				<a href="teso-ingresosVentas.php" class="mgbt"><img src="imagenes/add.png" title="Nuevo"/></a> 
				<a onClick="guardar();" class="mgbt"><img src="imagenes/guarda.png" title="Guardar"/></a> 
				<a href="teso-buscaingresosVentas.php" class="mgbt"><img src="imagenes/busca.png" title="Buscar"/></a> 
				<a onClick="mypop=window.open('plan-agenda.php','','');mypop.focus()" class="mgbt"><img src="imagenes/agenda1.png" title="Agenda" /></a>
				<a onClick="mypop=window.open('teso-principal.php','','');mypop.focus();" class="mgbt"><img src="imagenes/nv.png" title="Nueva ventana"></a>
				<a onClick="mypop=window.open('<?php echo $url2; ?>','','');mypop.focus();" class="tooltip bottom mgbt"><img src="imagenes/duplicar_pantalla.png" title="Duplicar pesta&ntilde;a"></a>
				<a onClick="iratras();" class="mgbt"><img src="imagenes/iratras.png" title="Atr&aacute;s"></a>
			</td>
		</tr>		  
	</table>

	<?php
		$vigencia=date('Y');
		$linkbd=conectar_v7();
		$vigusu=vigencia_usuarios($_SESSION['cedulausu']); 
		$bienesTransportables = '';
		$servicios = '';

		if($_POST['oculto'] == "")
		{
			$fec=date("d/m/Y");
			$_POST['fecha']=$fec; 		
			$_POST['tabgroup1']=1;
			$_POST['valoradicion']=0;
			$_POST['valorreduccion']=0;
			$_POST['valortraslados']=0;		 		  			 
			$_POST['valor']=0;	
			$_POST['precio']=0;	
			$sqlr="SELECT MAX(CAST(codigo AS INT)) FROM tesoingresos_ventas WHERE codigo NOT REGEXP '^[a-z]' ORDER BY codigo DESC";
			$res=mysqli_query($linkbd, $sqlr);
			$row = mysqli_fetch_row($res);
			$_POST['codigo']=$row[0]+1;
			if(strlen($_POST['codigo'])==1)
			{
			$_POST['codigo']='0'.$_POST['codigo'];
			}	 
		}

		if ($_POST['oculto'] == '6' && $_POST['terceros'] == 'on') {
			$_POST['terceros']=1;
		}

		if ($_POST['agregaFuente'] == 1) {
			
			$validacion = 0;

			for ($i=0; $i < count($_POST['codigoFuentes']); $i++) { 
				
				if ($_POST['codigoFuentes'][$i] == $_POST['fuente']) {
					$validacion++;
				}			
			}

			if ($validacion == 0) {
				$_POST['codigoFuentes'][]=$_POST['fuente'];
				$_POST['nombreFuentes'][]=$_POST['nfuente'];
				$_POST['fuente'] = "";
				$_POST['nfuente'] = "";
				echo"
					<script>
						document.getElementById('fuente').value='';
						document.getElementById('nfuente').value='';
					</script>";
			}
			else {
				echo "
					<script>
						Swal.fire({
							icon: 'error',
							title: 'Código repetido'
						})
					</script>
				";
			}
		}

		if($_POST['terceros']=="on")
		{
			$_POST['terceros']=1;
		}

		if ($_POST['eliminaFuente'] != '') {

			$posi=$_POST['eliminaFuente'];
			unset($_POST['codigoFuentes'][$posi]);
			unset($_POST['nombreFuentes'][$posi]);
			$_POST['codigoFuentes']= array_values($_POST['codigoFuentes']); 
			$_POST['nombreFuentes']= array_values($_POST['nombreFuentes']); 
		}

		if ($_POST['bf'] == '1') {

			$sqlFuente = "SELECT codigo_fuente, nombre FROM ccpet_fuentes_cuipo WHERE codigo_fuente = '$_POST[fuente]' AND version = '1'";
			$resFuente = mysqli_query($linkbd, $sqlFuente);
			$rowFuente = mysqli_fetch_row($resFuente);

			if ($rowFuente[0] != '') {

				$_POST['nfuente'] = $rowFuente[1];
			}
			else {

				echo"
					<script>
						Swal.fire({
							icon: 'error',
							title: 'Fuente no encontrada'
						})
						
						document.getElementById('fuente').value='';
						document.getElementById('nfuente').value='';
			
					</script>";

				$_POST['fuente'] = '';
				$_POST['nfuente'] = '';
			}
		}
	?>

    <div id="bgventanamodalm" class="bgventanamodalm">
        <div id="ventanamodalm" class="ventanamodalm">
            <IFRAME src="" name="ventanam" marginWidth=0 marginHeight=0 frameBorder=0 id="ventanam" frameSpacing=0 style=" width:700px; height:130px; top:200; overflow:hidden;"> 
            </IFRAME>
        </div>
    </div>	  

	<form name="form2" method="post" action="">
		<?php //**** busca cuenta
			if($_POST['bc']!='')
			{
				$nresul=buscaNombreCuentaCCPET($_POST['cuenta'], '1');			
				if($nresul!='')
				{
					$_POST['ncuenta']=$nresul;
				}
				else
				{
					$_POST['ncuenta']="";	
				}
			}

			if($_POST['bc'] == '2')
			{
				$nresul = buscaNombreClasificadorCCPET($_POST['cuin'], '1');

				if($nresul!='')
				{
					$_POST['ncuin']=$nresul;
				}
				else
				{
					$_POST['ncuin']="";	
				}
			}

			if($_POST['bc'] == '3')
			{
				$nresul = buscaNombreClasificadorCCPET($_POST['clasificador'], '2');

				if($nresul!='')
				{
					$_POST['nclasificador']=$nresul;
				}
				else
				{
					$_POST['nclasificador']="";	
				}
			}

			if($_POST['bc'] == '4')
			{
				$nresul = buscaNombreClasificadorCCPET($_POST['clasificadorServicios'], '3');

				if($nresul!='')
				{
					$_POST['nclasificadorServicios']=$nresul;
				}
				else
				{
					$_POST['nclasificadorServicios']="";	
				}
			}

			switch($_POST['tabgroup1'])
			{
				case 1:
					$check1='checked';break;
				case 2:
					$check2='checked';break;
				case 3:
					$check3='checked';break;
				case 4:
					$check4='checked';break;
				case 5:
					$check5='checked';break;
			}
		?>
	<div class="tabs" style="height:77%; width:99.6%">
		<div class="tab">
			<input type="radio" id="tab-1" name="tabgroup1" value="1" <?php echo $check1;?>/>
			<label id="clabel" for="tab-1">Ingresos</label>
			<div class="content" style="overflow:hidden;">
				<table class="inicio ancho">
					<tr>
						<td class="titulos" colspan="12">.: Agregar venta de bienes y servicios</td>
						<td  class="cerrar" style="width:7%"><a href="teso-principal.php">Cerrar</a></td>
					</tr>
					
					<tr>
						<td class="textoNewR" style="width:7%">
							<label class="labelR">Código:</label>
						</td>

						<td style="width:7%">
							<input style="text-align:center;" name="codigo" id="codigo" type="text" value="<?php echo $_POST['codigo']?>" maxlength="4" onKeyPress="javascript:return solonumeros(event)" onKeyUp="return tabular(event,this)" readonly> 
							<input id="oculto" name="oculto" type="hidden" value="1">		
						</td>

						<td class="textoNewR" style="width:10%">
							<label class="labelR">Nombre Ingreso:</label>
						</td>
						<td style="width:40%" colspan="5">
							<input name="nombre" id="nombre" type="text" value="<?php echo $_POST['nombre']?>" onKeyUp="return tabular(event,this)" style="width: 98%;"> 
						</td>
				
						<td class="textoNewR" style="width:10%">
							<label class="labelR">Precio Venta:</label>
						</td>
						<td>
							<input id="precio" name="precio" type="text" value="<?php echo $_POST['precio']?>" onKeyUp="return tabular(event,this)"  onKeyPress="javascript:return solonumeros(event)" >
						</td>
					</tr> 

					<tr>					
						<td class="textoNewR" style="width:5%">
							<label class="labelR">IVA:</label>
						</td>
						<td colspan="3" style="width:70%;">
							<select name="iva" id="iva" style="width:100%;" >
								<option value="-1">Seleccione ....</option>
								<?php
								$sqlr="Select * from conceptoscontables  where modulo='4' and tipo='IV' order by codigo";
								$resp = mysqli_query($linkbd, $sqlr);
								while ($row =mysqli_fetch_row($resp)) 
								{
									$i=$row[0];
									echo "<option value=$row[0] ";
									if($i==$_POST['iva'])
									{
										echo "SELECTED";
										$_POST['iva']=$row[0];
									}
									echo " >".$row[0]." - ".$row[3]." - ".$row[1]."</option>";	  
								}			
								?>
							</select>
						</td>

						<td class="textoNewR" style="width:15%">
							<label class="labelR">Contabiliza liquidación:</label>
						</td>
						<td>
							<select name="contLiquidacion" id="contLiquidacion">
								<option value="S" <?php if($_POST['contLiquidacion']=='S') echo "SELECTED"?>>Si</option>
								<option value="N" <?php if($_POST['contLiquidacion']=='N') echo "SELECTED"?>>No</option>
							</select>
						</td>						
					</tr>
				</table>
			
				<table class="inicio ancho">
					<tr>
						<td colspan="12" class="titulos">Agregar Detalle Ingreso</td>
					</tr>                  
					<tr>
						<td class="textoNewR" style="width:10%">
							<label class="labelR">Concepto contable:</label>
						</td>
						<td style="width:50%;">
							<select name="concecont" id="concecont" style="width:45%;" >
								<option value="-1">Seleccione ....</option>
								<?php
								$sqlr="Select * from conceptoscontables  where modulo='4' and tipo='C' order by codigo";
								$resp = mysqli_query($linkbd, $sqlr);
								while ($row =mysqli_fetch_row($resp)) 
								{
									$i=$row[0];
									echo "<option value=$row[0] ";
									if($i==$_POST['concecont'])
									{
										echo "SELECTED";
										$_POST['concecontnom']=$row[0]." - ".$row[3]." - ".$row[1];
									}
									echo " >".$row[0]." - ".$row[3]." - ".$row[1]."</option>";	  
								}			
								?>
							</select>
							<input id="concecontnom" name="concecontnom" type="hidden" value="<?php echo $_POST['concecontnom']?>" >
						</td>
					</tr>
					<tr>
						<?php
							if($_POST['terceros']!="1") {
						?>
								<td class="textoNewR" style="width:10%">
									<label class="labelR">Cuenta presupuestal:</label>
								</td>
								<td style="width:50%;" valign="middle" >
									<input type="text" id="cuenta" name="cuenta" style="width:10%" onKeyPress="javascript:return solonumeros(event)" onKeyUp="return tabular(event,this)" onBlur="buscacta(event)" value="<?php echo $_POST['cuenta']?>" onClick="document.getElementById('cuenta').focus();document.getElementById('cuenta').select();">
									<input type="hidden" value="" name="bc" id="bc">
									<a title="Cuentas presupuestales" onClick="despliegamodal2('visible');" style="cursor:pointer;"><img src="imagenes/find02.png" style="width:20px;"/></a>
									<input type="text" name="ncuenta" id="ncuenta" style="width:33%;" value="<?php echo $_POST['ncuenta']?>"  readonly>
								</td>
						<?php
							}
						?>
					</tr>
					<?php
					$buscarClasificador = '';

					$sqlr = "SELECT clasificadores FROM ccpetprogramarclasificadores WHERE cuenta = '".$_POST['cuenta']."' ";
					
					$resp = mysqli_query($linkbd, $sqlr);
				
					$row = mysqli_fetch_row($resp);

					if($row[0] == '1')
					{
						//showmodal CUIN
						$buscarClasificador = '1';
					}
				
					if($row[0] == '2')
					{
						//showmodal bienes transportables
						$buscarClasificador = '2';
						
					}
					elseif($row[0] == '3')
					{
						//showmodal servicios
						
						$buscarClasificador = '3';
					}

					?>

					<?php
					if($buscarClasificador == '1')
					{
					?>

					<tr>
						<td class="textoNewR" style="width:10%">
							<label class="labelR">CUIN:</label>
						</td>
						<td style="width:50%;" valign="middle">
							<input type="text" id="cuin" name="cuin" style="width:10%" onKeyPress="javascript:return solonumeros(event)" onKeyUp="return tabular(event,this)" onBlur="buscacta2(event,1)" value="<?php echo $_POST['cuin']?>" onClick="document.getElementById('cuin').focus();document.getElementById('cuin').select();">
							<a title="CUIN" onClick="despliegamodal5('visible');" style="cursor:pointer;"><img src="imagenes/find02.png" style="width:20px;"/></a>
							<input type="text" name="ncuin" id="ncuin" style="width:33%;" value="<?php echo $_POST['ncuin']?>"  readonly>
						</td>
					</tr>
					
					<?php
					}

					if($buscarClasificador == '2')
					{
					?>
					<tr>
						<td class="textoNewR" style="width:10%">
							<label class="labelR">Bienes transportables:</label>
						</td>
						<td style="width:50%;" valign="middle">
							<input type="text" id="clasificador" name="clasificador" style="width:10%" onKeyPress="javascript:return solonumeros(event)" onKeyUp="return tabular(event,this)" onBlur="buscacta2(event,2)" value="<?php echo $_POST['clasificador']?>" onClick="document.getElementById('clasificador').focus();document.getElementById('clasificador').select();">
							<a title="Cuentas presupuestales" onClick="despliegamodal3('visible');" style="cursor:pointer;"><img src="imagenes/find02.png" style="width:20px;"/></a>
							<input type="text" name="nclasificador" id="nclasificador" style="width:33%;" value="<?php echo $_POST['nclasificador']?>"  readonly>
						</td>
					</tr>
					<?php
					}

					if($buscarClasificador == '3')
					{
					?>
					<tr>
						<td class="textoNewR" style="width:10%">
							<label class="labelR">Servicios:</label>
						</td>
						<td style="width:50%;" valign="middle">
							<input type="text" id="clasificadorServicios" name="clasificadorServicios" style="width:10%" onKeyPress="javascript:return solonumeros(event)" onKeyUp="return tabular(event,this)" onBlur="buscacta2(event,3)" value="<?php echo $_POST['clasificadorServicios']?>" onClick="document.getElementById('clasificadorServicios').focus();document.getElementById('clasificadorServicios').select();">
							<a title="Clasificador Servicios" onClick="despliegamodal4('visible');" style="cursor:pointer;"><img src="imagenes/find02.png" style="width:20px;"/></a>
							<input type="text" name="nclasificadorServicios" id="nclasificadorServicios" style="width:33%;" value="<?php echo $_POST['nclasificadorServicios']?>"  readonly>
						</td>
					</tr>
					<?php
					}
					?>
					<tr>
						<td class="textoNewR" style="width:10%">
							<label class="labelR">Porcentaje:</label>
						</td>
						<td>
							<input id="valor" name="valor" type="text" value="<?php echo $_POST['valor']; ?>" style="width:10%;" onKeyUp="return tabular(event,this)"  onKeyPress="javascript:return solonumeros(event)" > %
						</td>
					</tr>
				</table>
				<?php

					//Saber si se va agregar CUIN, Bienes Transportable o Servicios
					$cuentaClasificadora;
					if(isset($_POST['cuin']))
					{
						$cuentaClasificadora = $_POST['cuin'];
					}
					elseif(isset($_POST['clasificador']))
					{
						$cuentaClasificadora = $_POST['clasificador'];
					}
					elseif(isset($_POST['clasificadorServicios']))
					{
						$cuentaClasificadora = $_POST['clasificadorServicios'];
					}
					else
					{
						$cuentaClasificadora = '';
					}
					//var_dump($cuentaClasificadora);
				?>
			</div>
		</div>	

		<div class="tab">
			<input type="radio" id="tab-2" name="tabgroup1" value="2" <?php echo $check2;?>/>
			<label id="clabel" for="tab-2">Fuentes</label>
			<div class="content" style="overflow:hidden;">
				<table class="inicio ancho">
					<tr>
						<td class="titulos" colspan="12">.: Agregar Fuentes</td>
						<td class="cerrar" style="width:7%"><a href="teso-principal.php">Cerrar</a></td>
					</tr>
					
					<tr>
						<td class="textoNewR" style="width:10%">
							<label class="labelR">Fuente:</label>
						</td>
						<td style="width: 15%;">
							<input type="text" id="fuente" name="fuente" style="width:80%" onKeyPress="javascript:return solonumeros(event)" onKeyUp="return tabular(event,this)" onBlur="buscarFuente(event)" value="<?php echo $_POST['fuente']?>" autocomplete="off">
							
							<input type="hidden" name="bf" id="bf" value="0">

							<a title="Fuentes presupuestales" onClick="despliegamodal7('visible');" style="cursor:pointer;"><img src="imagenes/find02.png" style="width:20px;"/></a>
						</td>

						<td colspan="4">
							<input type="text" name="nfuente" id="nfuente" style="width:100%;" value="<?php echo $_POST['nfuente']?>"  readonly>
							<input type="hidden" name="agregaFuente" id="agregaFuente" value="0">
						</td>

						<td>
							<em class="botonflechaverde" onclick="agregaFuente()">Agregar fuente</em>
						</td>
					</tr>
				</table>

				<table class="inicio ancho">
					<tr>
						<td class="titulos" colspan="6">Fuentes: </td>
					</tr>
					<tr>
						<td class="titulos2" style="text-align: center; width: 20%;">Código fuente</td>
						<td class="titulos2" style="text-align: center;">Nombre fuente</td>	
						<td class="titulos2" style="text-align: center;">
							Eliminar
							<input type='hidden' name='eliminaFuente' id='eliminaFuente'>
						</td>
					</tr>

					<?php 
						for ($x=0;$x< count($_POST['codigoFuentes']);$x++)
						{
							echo "
							<tr>
								<td class='saludo2'>
									<input name='codigoFuentes[]' value='".$_POST['codigoFuentes'][$x]."' type='text' readonly style='width: 100%; text-align: center;'>
								</td>
								<td class='saludo2'>
									<input name='nombreFuentes[]' value='".$_POST['nombreFuentes'][$x]."' type='text' readonly style='width: 100%;'>
								</td>
								<td class='saludo2' style='text-align: center;'>
									<a onclick='eliminarFuente($x)'>
									<img src='imagenes/del.png'></a>
								</td>
							</tr>";
						}
					?>
				</table>
			</div>
		</div>
	</div>
	<div id="bgventanamodal2">
		<div id="ventanamodal2">
			<IFRAME  src="" name="buster" marginWidth=0 marginHeight=0 frameBorder=0 id="ventana2" frameSpacing=0 style="left:500px; width:900px; height:500px; top:200;"> 
			</IFRAME>
		</div>
    </div>
						
    </form>
  		<?php
			if($_POST['oculto']=='2')
			{			
				$sqlr="INSERT INTO tesoingresos_ventas (codigo,nombre,tipo,estado,terceros,tipoIngreso,gravado,concepto_iva)VALUES ('$_POST[codigo]', '$_POST[nombre]', 'S', 'S', '', 'normal', '$_POST[contLiquidacion]','$_POST[iva]')";
				
				if (!mysqli_query($linkbd, $sqlr))
				{
					echo "
							<script>
								Swal.fire({
									icon: 'error',
									title: 'Error en guardado de cabecera'
								})
							</script>
						";
				}
				else
				{
					$fecha=date('Y-m-d h:i:s');

					//Desactiva cualquier precio antiguo con este mismo código
					$sqlr="UPDATE tesoingresos_ventas_precios SET estado='N' WHERE ingreso='$_POST[codigo]'";
					mysqli_query($linkbd, $sqlr);

					//Ingresa valores del nuevo precio con este código
					$sqlr="INSERT INTO tesoingresos_ventas_precios (ingreso,precio,fecha,estado) VALUES ('$_POST[codigo]',$_POST[precio],'$fecha','S')";
					mysqli_query($linkbd, $sqlr);
		
					$_POST['valor']=100;

					if($cuentaClasificadora != '')
					{
						$sqlr="INSERT INTO tesoingresos_ventas_det (codigo,concepto,modulo,tipoconce,porcentaje,cuentapres,cuenta_clasificadora,estado)VALUES ('$_POST[codigo]','$_POST[concecont]','4', 'S', '$_POST[valor]', '$_POST[cuenta]',$cuentaClasificadora,'S')";
					}
					else
					{
						$sqlr="INSERT INTO tesoingresos_ventas_det (codigo,concepto,modulo,tipoconce,porcentaje,cuentapres,estado)VALUES ('$_POST[codigo]','$_POST[concecont]','4', 'S', '$_POST[valor]', '$_POST[cuenta]','S')";
					}

					if (!mysqli_query($linkbd, $sqlr))
					{
						echo "
							<script>
								Swal.fire({
									icon: 'error',
									title: 'Error en guardado de detalle'
								})
							</script>
						";
					}
					else
					{
						$sql = "UPDATE tesoingresos_fuentes SET estado='N' WHERE codigo = '$_POST[codigo]'";
						mysqli_query($linkbd, $sql);

						for ($x=0;$x< count($_POST['codigoFuentes']);$x++) {

							$sqlr = "INSERT INTO tesoingresos_ventas_fuentes (codigo, fuente, estado) VALUES ('$_POST[codigo]', '".$_POST['codigoFuentes'][$x]."', 'S')";
							mysqli_query($linkbd, $sqlr);
						}	
						
						echo "
							<script>
								Swal.fire({
									icon: 'success',
									title: 'Guardado exitoso!',
									}).then((result) => {
									if (result.value) {
										document.location.href = 'teso-ingresosVentas.php';
									} 
								})
							</script>";
					}
				}
			}
		?> 
		</td>
	</tr>
</table>
</body>
</html>