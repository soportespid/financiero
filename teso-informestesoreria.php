<?php
	header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=utf8");
	require "comun.inc";
	require "funciones.inc";
	session_start();
	cargarcodigopag($_GET['codpag'],$_SESSION["nivel"]);
	date_default_timezone_set("America/Bogota");
?>
<!DOCTYPE >
<html lang="es">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1">
		<meta name="viewport" content="user-scalable=no">
		<title>:: Spid - Tesoreria</title>
		<link href="favicon.ico" rel="shortcut icon"/>
        <link href="css/css2z.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet"/>
		<link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet"/>
		<script type="text/javascript" src="css/programas.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
    </head>
    <body>
        <IFRAME src="alertas.php" name="alertas" id="alertas" style="display:none"></IFRAME>
        <span id="todastablas2"></span>
		<table>
			<tr><script>barra_imagenes("teso");</script><?php cuadro_titulos();?></tr>
			<tr><?php menu_desplegable("teso");?></tr>
			<tr>
				<td colspan="3" class="cinta">
					<a class="mgbt"><img src="imagenes/add2.png" /></a>
					<a class="mgbt"><img src="imagenes/guardad.png" style="width:24px;"/></a>
					<a class="mgbt"><img src="imagenes/buscad.png"/></a>
					<a onClick="mypop=window.open('plan-agenda.php','','');mypop.focus()" class="mgbt"><img src="imagenes/agenda1.png" title="Agenda" /></a>
					<a href="#" onClick="mypop=window.open('teso-principal.php','','');mypop.focus();" class="mgbt"><img src="imagenes/nv.png" title="Nueva Ventana"></a>
				</td>
			</tr>
		</table>
		<form name="form2" method="post" action="">
			<div style="overflow-y: scroll; height: 78%">
				<table class="inicio">
					<tr>
						<td class="titulos" colspan="2">.: Informes de tesoreria </td>
						<td class="cerrar" style="width:7%;" ><a href="teso-principal.php">&nbsp;Cerrar</a></td>
					</tr>
                    <tr>
                        <td>
                            <ol id="lista2">
								<table>
									<tr>
										<td>
                                            <li onClick="location.href='teso-saldobancos.php'" style="cursor:pointer;">Saldos bancos</li>
                                            <li onClick="location.href='teso-reporingresos.php'" style="cursor:pointer;">Reporte de ingresos</li>
                                            <li onClick="location.href='teso-reporegresos.php'" style="cursor:pointer;">Comprobante de egresos</li>
                                            <li onClick="location.href='teso-reporegresosnomina.php'" style="cursor:pointer;">Egreso n&oacute;mina</li>
                                            <li onClick="location.href='teso-reporcontribuyente.php'" style="cursor:pointer;">Reporte pago terceros</li>
                                            <li onClick="location.href='teso-reportenotasbancarias.php'" style="cursor:pointer;">Reporte Notas Bancarias</li>
                                            <li onClick="location.href='teso-reportecodigoingreso.php'" style="cursor:pointer;">Reporte de Codigos Ingresos</li>
                                            <li onClick="location.href='teso-reporteliquidacionpredial.php'" style="cursor:pointer;">Reporte liquidacion predial</li>
                                            <li onClick="location.href='teso-reporteliquidacionpredial1.php'" style="cursor:pointer;">Reporte liquidacion predial 1.0 </li>
                                            <li onClick="location.href='teso-reportecxp.php'" style="cursor:pointer;">Reporte Cuentas Por Pagar</li>
                                            <li onClick="location.href='teso-reporteidentificados.php'" style="cursor:pointer;">Reporte de recaudos por Clasificar</li>
                                            <li onClick="location.href='teso-reportebancos.php'" style="cursor:pointer;">Reporte de recaudo por Bancos</li>
                                            <li onClick="location.href='teso-comparapresu-conta.php'" style="cursor:pointer;">Reporte para comparar el ingreso ambiental presupuesto - contabilidad</li>
                                            <li onClick="location.href='teso-comparaconta-presu.php'" style="cursor:pointer;">Reporte para comparar el ingreso ambiental contabilidad - presupuesto</li>
                                            <li onClick="location.href='teso-saldosrp.php'" style="cursor:pointer;">Saldo rp</li>
                                            <li onClick="location.href='teso-cxpreporte.php'" style="cursor:pointer;">Saldos Cxp</li>
                                            <li onClick="location.href='teso-reporteprescripcion.php'" style="cursor:pointer;">Reporte de prescripcion predial</li>
                                            <li onClick="location.href='teso-cuentas_xp_nomina.php'" style="cursor:pointer;">Saldos Nomina</li>
										</td>
										<td style="vertical-align:top">
                                            <li onClick="location.href='teso-reporteEgresos'" style="cursor:pointer;">Reporte de egresos</li>
										</td>
									</tr>
								</table>
							</ol>
                        </td>
					</tr>
				</table>
			</div>
		</form>
	</body>
</html>
