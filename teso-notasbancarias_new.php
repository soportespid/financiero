<?php
header("Cache-control: no-cache, no-store, must-revalidate");
header("Content-Type: text/html;charset=utf8");
require "comun.inc";
require "funciones.inc";
session_start();
$linkbd = conectar_v7();
$linkbd->set_charset("utf8");
cargarcodigopag($_GET['codpag'], $_SESSION["nivel"]);
date_default_timezone_set("America/Bogota");
?>
<!DOCTYPE>
<html lang="es">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="ie=edge" />
    <meta name="viewport" content="user-scalable=no">
    <title>IDEAL 10 - Tesoreria</title>
    <link href="favicon.ico" rel="shortcut icon" />
    <link href="css/css2z.css?<?php echo date('d_m_Y_h_i_s'); ?>" rel="stylesheet" type="text/css" />
    <link href="css/css3.css?<?php echo date('d_m_Y_h_i_s'); ?>" rel="stylesheet" type="text/css" />
    <link href="css/css4.css?<?php echo date('d_m_Y_h_i_s'); ?>" rel="stylesheet" type="text/css" />
    <link href="css/tabs.css?<?php echo date('d_m_Y_h_i_s'); ?>" rel="stylesheet" type="text/css" />
    <link href="sweetalert2/dist/sweetalert2.min.css" rel="stylesheet">
    <script src="sweetalert2/dist/sweetalert2.min.js"></script>
    <script src="vue/vue.js"></script>
    <script type="text/javascript" src="css/programas.js?<?php echo date('d_m_Y_h_i_s'); ?>"> </script>
    <script type="text/javascript" src="css/calendario.js?<?php echo date('d_m_Y_h_i_s'); ?>"> </script>
</head>

<body>
    <header>
        <table>
            <tr>
                <script>barra_imagenes("teso");</script><?php cuadro_titulos(); ?>
            </tr>
        </table>
    </header>
    <section id="myapp" v-cloak>
        <nav>
            <table>
                <tr><?php menu_desplegable("teso"); ?></tr>
            </table>
            <div class="bg-white group-btn p-1">
                <button type="button" onclick="window.location.href='teso-notasbancarias_new.php'"
                    class="btn btn-white btn-primary-hover d-flex justify-between align-items-center">
                    <span>Nuevo</span>
                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 -960 960 960">
                        <path d="M440-440H200v-80h240v-240h80v240h240v80H520v240h-80v-240Z"></path>
                    </svg>
                </button>
                <button type="button" v-on:click="validarGuardar();" class="btn btn-white btn-primary-hover d-flex justify-between align-items-center">
                    <span>Guardar</span>
                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 -960 960 960">
                        <path
                            d="M840-680v480q0 33-23.5 56.5T760-120H200q-33 0-56.5-23.5T120-200v-560q0-33 23.5-56.5T200-840h480l160 160Zm-80 34L646-760H200v560h560v-446ZM480-240q50 0 85-35t35-85q0-50-35-85t-85-35q-50 0-85 35t-35 85q0 50 35 85t85 35ZM240-560h360v-160H240v160Zm-40-86v446-560 114Z">
                        </path>
                    </svg>
                </button>
                <button type="button" @click="window.location.href='teso-buscanotasbancarias_new.php'"
                    class="btn btn-white btn-primary-hover d-flex justify-between align-items-center">
                    <span>Buscar</span>
                    <svg class="fill-black group-hover:fill-white w-5 h-5" xmlns="http://www.w3.org/2000/svg"
                        viewBox="0 -960 960 960">
                        <path
                            d="M784-120 532-372q-30 24-69 38t-83 14q-109 0-184.5-75.5T120-580q0-109 75.5-184.5T380-840q109 0 184.5 75.5T640-580q0 44-14 83t-38 69l252 252-56 56ZM380-400q75 0 127.5-52.5T560-580q0-75-52.5-127.5T380-760q-75 0-127.5 52.5T200-580q0 75 52.5 127.5T380-400Z">
                        </path>
                    </svg>
                </button>
                <button type="button" onclick="mypop=window.open('plan-agenda.php','','');mypop.focus()"
                    class="btn btn-white btn-primary-hover d-flex justify-between align-items-center">
                    <span class="group-hover:text-white">Agenda</span>
                    <svg class="fill-black group-hover:fill-white w-5 h-5" xmlns="http://www.w3.org/2000/svg"
                        viewBox="0 -960 960 960">
                        <path
                            d="M560-564v-68q33-14 67.5-21t72.5-7q26 0 51 4t49 10v64q-24-9-48.5-13.5T700-600q-38 0-73 9.5T560-564Zm0 220v-68q33-14 67.5-21t72.5-7q26 0 51 4t49 10v64q-24-9-48.5-13.5T700-380q-38 0-73 9t-67 27Zm0-110v-68q33-14 67.5-21t72.5-7q26 0 51 4t49 10v64q-24-9-48.5-13.5T700-490q-38 0-73 9.5T560-454ZM260-320q47 0 91.5 10.5T440-278v-394q-41-24-87-36t-93-12q-36 0-71.5 7T120-692v396q35-12 69.5-18t70.5-6Zm260 42q44-21 88.5-31.5T700-320q36 0 70.5 6t69.5 18v-396q-33-14-68.5-21t-71.5-7q-47 0-93 12t-87 36v394Zm-40 118q-48-38-104-59t-116-21q-42 0-82.5 11T100-198q-21 11-40.5-1T40-234v-482q0-11 5.5-21T62-752q46-24 96-36t102-12q58 0 113.5 15T480-740q51-30 106.5-45T700-800q52 0 102 12t96 36q11 5 16.5 15t5.5 21v482q0 23-19.5 35t-40.5 1q-37-20-77.5-31T700-240q-60 0-116 21t-104 59ZM280-494Z">
                        </path>
                    </svg>
                </button>
                <button type="button" onclick="mypop=window.open('teso-principal.php','','');mypop.focus();"
                    class="btn btn-white btn-primary-hover d-flex justify-between align-items-center">
                    <span>Nueva ventana</span>
                    <svg class="fill-black group-hover:fill-white w-5 h-5" xmlns="http://www.w3.org/2000/svg"
                        viewBox="0 -960 960 960">
                        <path
                            d="M320-120v-80h80v-80H160q-33 0-56.5-23.5T80-360v-400q0-33 23.5-56.5T160-840h640q33 0 56.5 23.5T880-760v400q0 33-23.5 56.5T800-280H560v80h80v80H320ZM160-360h640v-400H160v400Zm0 0v-400 400Z">
                        </path>
                    </svg>
                </button>

            </div>
        </nav>
        <article>
            <table class="inicio ancho">
                <tr>
                    <td class="titulos" colspan="8">Notas Bancarias</td>
                    <td class="cerrar" style="width:7%" onClick="location.href='teso-principal.php'">Cerrar</td>
                </tr>
                <tr>
                    <td class="tamano01" style="width:3cm">Tipo Movimiento:</td>
                    <td colspan="3">
                        <select v-model="tipoMovimiento" style="width:100%;text-transform: uppercase;" v-on:click="">
                            <option v-for="vcTipoMivimiento in selectTipoMovimiento" :value="vcTipoMivimiento[3]">{{
                                vcTipoMivimiento[0] }}{{ vcTipoMivimiento[1] }} - {{ vcTipoMivimiento[2] }}</option>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td class="tamano01" style="width:3cm">:&middot; N° Comprobante:</td>
                    <td><input type="text" v-model="numeroNota" style="width:100%" readonly /></td>
                    <td class="tamano01" style="width:3cm;">:&middot; Fecha:</td>
                    <td style="width:10%;"><input type="text" name="fecha" value="<?php echo $_POST['fecha'] ?>"
                            onKeyUp="return tabular(event,this)" id="fc_1198971545" title="DD/MM/YYYY"
                            onDblClick="displayCalendarFor('fc_1198971545');" class="colordobleclik" autocomplete="off"
                            onChange="" readonly></td>
                    <td class="tamano01" style="width:3cm">:&middot;Centro de Costo:</td>
                    <td colspan="2">
                        <select v-model="centroCosto" style="width:100%;text-transform: uppercase;" v-on:click=""
                            v-on:change="buscarGastoBancario();">
                            <option v-for="vcCentroCosto in selectCentroCosto" :value="vcCentroCosto[0]">{{
                                vcCentroCosto[0] }} - {{ vcCentroCosto[1] }}</option>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td class="tamano01">:&middot; Concepto:</td>
                    <td colspan="3"><input type="text" v-model="concepto" style="width:100%"
                            placeholder="Objeto de la nota bancaria" /></td>
                    <td class="tamano01">:&middot; Doc. Banco:</td>
                    <td colspan="2"><input type="text" v-model="docBanco" style="width:100%" /></td>
                </tr>
                <tr>
                    <td class="tamano01">:&middot; Cuenta:</td>
                    <td><input type="text" v-model="cuentaBancaria" style="width:100%" class="colordobleclik"
                            v-on:change="validarcuenta()" v-on:dblclick='toggleModal2' /></td>
                    <td colspan="5"><input type="text" v-model="nomBanco" style="width:100%" readonly /></td>
                    <input type="hidden" v-model="cuentaBancariaH" />
                    <input type="hidden" v-model="numBanco" />
                </tr>
                <tr>
                    <td class="tamano01">:&middot; Gasto Bancario:</td>
                    <td colspan="3">
                        <select v-model="gastoBancario" style="width:100%;text-transform: uppercase;" v-on:click="">
                            <option v-for="vcGastoBancario in selectGastoBancario" :value="vcGastoBancario[3]"
                                :disabled="vcGastoBancario[4] === ''"
                                v-bind:class="(vcGastoBancario[4] == '' ? 'contenidonew05': '')">{{ vcGastoBancario[2]
                                }} - {{ vcGastoBancario[0] }} - {{ vcGastoBancario[1] }}</option>
                        </select>
                    </td>
                    <td class="tamano01">:&middot; Valor Nota:</td>
                    <td><input type="text" v-model="valorNota" style="width:100%"
                            onKeyPress="javascript:return solonumeros(event)" /></td>
                    <td style="height: 30px;"><em class="botonflechaverde" v-on:click="agregarListaNotas()">Agregar</em>
                    </td>
                </tr>
            </table>
            <table class='tablamv'>
                <thead>
                    <tr class="titulos">
                        <th style="width:5%; font: 110% sans-serif;">Item</th>
                        <th style="width:10%; font: 110% sans-serif;">Centro Costo</th>
                        <th style="width:15%; font: 110% sans-serif;">Doc. banco</th>
                        <th style="width:43%; font: 110% sans-serif;">Banco</th>
                        <th style="width:10%; font: 110% sans-serif;">Gasto Banco</th>
                        <th style="width:10%; font: 110% sans-serif;">valor</th>
                        <th style="font: 110% sans-serif;">Eliminar</th>
                    </tr>
                </thead>
                <tbody style="max-height: 32vh !important;">
                    <tr v-for="(vcListaNotas, index) in selectListaNotas"
                        v-bind:class="index % 2 ? 'saludo1a' : 'saludo2'"
                        style='text-rendering: optimizeLegibility; cursor: pointer !important;'>
                        <td style="font: 120% sans-serif; padding-left:10px; width:5%; text-align:center;">{{ index +
                            1}}</td>
                        <td style="font: 120% sans-serif; padding-left:10px; width:10%; text-align:center;">{{
                            vcListaNotas[0] }}</td>
                        <td style="font: 120% sans-serif; padding-left:10px; width:15%; text-align:center;">{{
                            vcListaNotas[2] }}</td>
                        <td style="font: 120% sans-serif; padding-left:10px; width:42%;">{{ vcListaNotas[3] }} - {{
                            vcListaNotas[4] }}</td>
                        <td style="font: 120% sans-serif; padding-left:10px; width:10%; text-align:center;">{{
                            vcListaNotas[7] }}</td>
                        <td style="font: 120% sans-serif; padding-left:10px; width:13%; text-align:right;">{{
                            formatonumero(vcListaNotas[8]) }}</td>
                        <td style='text-align:center;' v-on:click="eliminaListaNotas(index,vcListaNotas[8])"><img
                                src='imagenes/del.png'></td>
                    </tr>
                </tbody>
                <tbody style="overflow:hidden !important;">
                    <tr>
                        <td style='text-align:right; font: 120% sans-serif; width:79%;'>Total:</td>
                        <td style='text-align:right; font: 120% sans-serif; width:15%;'>{{
                            formatonumero(valorListaNotas) }}</td>
                        <td></td>
                    </tr>
                </tbody>
            </table>
        </article>
        <div v-show="showModal2">
            <transition name="modal">
                <div class="modal-mask">
                    <div class="modal-wrapper">
                        <div class="modal-container2">
                            <table class='tablamv'>
                                <tbody style="overflow:hidden !important;">
                                    <tr>
                                        <td class="titulos" colspan="2">SELECCIONAR CUENTA</td>
                                        <td class="cerrar" style="width:7%;" @click="showModal2 = false">Cerrar</td>
                                    </tr>
                                    <tr>
                                        <td class="tamano01" style="width:3cm;">Cuenta:</td>
                                        <td style="width: 60%;"><input type="text" class="form-control"
                                                placeholder="Buscar por nombre del programa"
                                                v-on:keyup="buscaCuentaVentana" v-model="searchCuenta"
                                                style="width:100%;  height: 30px !important; padding: 5px;" /></td>
                                        <td></td>
                                    </tr>
                                </tbody>
                                <thead>
                                    <tr>
                                        <th class='titulos'
                                            style="width:5%; font: 110% sans-serif; border-radius: 5px 0 0 0;">N°</th>
                                        <th class='titulos' style="width:30%; font: 110% sans-serif;">Razón Social</th>
                                        <th class='titulos' style="width:35%; font: 110% sans-serif;">Descripción</th>
                                        <th class='titulos' style="width:11%; font: 110% sans-serif;">Cuenta Contable
                                        </th>
                                        <th class='titulos' style="width:11%; font: 110% sans-serif;">Cuenta Bancaria
                                        </th>
                                        <th class='titulos' style="font: 110% sans-serif; border-radius: 0 5px 0 0;">
                                            Tipo Cuenta</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr v-for="(vcCuentas,index) in selectCuentas"
                                        v-on:click="cargaInfoCuenta( vcCuentas[5],vcCuentas[0], vcCuentas[2], vcCuentas[3])"
                                        v-bind:class="index % 2 ? 'saludo1a' : 'saludo2'"
                                        style="text-rendering: optimizeLegibility; cursor: pointer !important;">
                                        <td style="width:5%; font: 110% sans-serif; padding-left:10px">{{ index }}</td>
                                        <td style="width:30%; font: 110% sans-serif; padding-left:10px">{{ vcCuentas[0]
                                            }}</td>
                                        <td style="width:36%;font: 110% sans-serif; padding-left:10px">{{ vcCuentas[1]
                                            }}</td>
                                        <td style="width:11%; font: 110% sans-serif; padding-left:10px">{{ vcCuentas[2]
                                            }}</td>
                                        <td style="width:11%; font: 110% sans-serif; padding-left:10px">{{ vcCuentas[3]
                                            }}</td>
                                        <td style="font: 110% sans-serif; padding-left:10px">{{ vcCuentas[4] }}</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </transition>
        </div>
        <div id="cargando" v-if="loading" class="loading">
            <span>Cargando...</span>
        </div>
    </section>
    <script src="Librerias/vue/vue.min.js"></script>
    <script src="Librerias/vue/axios.min.js"></script>
    <script src="vue/tesoreria/teso-notasbancarias_new.js?<?php echo date('d_m_Y_h_i_s'); ?>"></script>
</body>

</html>
