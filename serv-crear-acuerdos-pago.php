<?php
	header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=utf8");
	require "comun.inc";
	require "funciones.inc";
	require "funcionesSP.inc.php";
	session_start();
	$linkbd = conectar_v7();
	$linkbd -> set_charset("utf8"); 
	cargarcodigopag($_GET['codpag'],$_SESSION['nivel']);
    date_default_timezone_set("America/Bogota");
	titlepag();
			
?>

<!DOCTYPE >

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="es">
	<head>
		<meta http-equiv="Content-type" content="text/html; charset=utf8"/>
		<meta http-equiv="X-UA-Compatible" content="IE=9"/>
		<title>:: IDEAL 10 - Servicios P&uacute;blicos</title>
		<link href="css/css2.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<link href="css/css4.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<link href="css/cssSP.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<link href="css/tabs.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<script type="text/javascript" src="css/calendario.js"></script>
        <script type="text/javascript" src="css/programas.js"></script>
		<script type="text/javascript" src="jquery-1.11.0.min.js"></script> 
		<script type="text/javascript" src="JQuery/autoNumeric-master/autoNumeric-min.js"></script>
		<script src="sweetalert2/dist/sweetalert2.min.js"></script>
		<link rel="stylesheet" href="sweetalert2/dist/sweetalert2.min.css">
        
		<script>
			function despliegamodalm(_valor,_tip,mensa,pregunta)
			{
				document.getElementById("bgventanamodalm").style.visibility=_valor;
                if(_valor == "hidden")
                {
                    document.getElementById('ventanam').src="";
                }
				else
				{
					switch(_tip)
					{
						case "1":	document.getElementById('ventanam').src="ventana-mensaje1.php?titulos="+mensa;break;
						case "2":	document.getElementById('ventanam').src="ventana-mensaje3.php?titulos="+mensa;break;
						case "3":	document.getElementById('ventanam').src="ventana-mensaje2.php?titulos="+mensa;break;
						case "4":	document.getElementById('ventanam').src="ventana-consulta1.php?titulos="+mensa+"&idresp="+pregunta;break;	
					}
				}
            }

			function despliegamodal(estado, modal)
			{
				document.getElementById("bgventanamodal2").style.visibility=estado;

				if(estado == "hidden")
				{
					document.getElementById('ventana2').src="";
				}
				else
				{
					switch (modal) {
						case 'usuarios':
							document.getElementById('ventana2').src = 'serv-modalUsuariosAcuerdoPago.php';
						break;

						default:
							break;
					}
				}
			}

			function funcionmensaje()
			{
				var x = window.location.search;
				location.href = "serv-crear-acuerdos-pago.php";
            }

			function guardar()
			{
				var numeroAcuerdo = document.getElementById('numeroAcuerdo').value;

				if(numeroAcuerdo != '')
				{
					var codUsuario = document.getElementById('codUsuario').value;

					if(codUsuario != '')
					{
						var fecha = document.getElementById('fecha').value;

						if(fecha != '')
						{
							var medioRecaudo = document.getElementById('modoRecaudo').value;

							if(medioRecaudo != '-1')
							{
								if(medioRecaudo == 'banco')
								{
									var cuentaBancaria = document.getElementById('cuentaBancaria').value;

									if(cuentaBancaria != '')
									{
										var abono = document.getElementById('valorAbono').value;

										if(abono != '')
										{
											var cuotas = document.getElementById('numeroCuotas').value;

											if(cuotas != '')
											{
												Swal.fire({
													icon: 'question',
													title: 'Seguro que quieres guardar?',
													showDenyButton: true,
													confirmButtonText: 'Guardar',
													denyButtonText: 'Cancelar',
													}).then((result) => {
													if (result.isConfirmed) {
														document.form2.oculto.value='2';
														document.form2.submit();
													} else if (result.isDenied) {
														Swal.fire('Guardar cancelado', '', 'info')
													}
												})
											}
											else
											{
												Swal.fire({
													icon: 'warning',
													title: 'Ingrese el número de cuotas para el acuerdo de pago'
												})	
											}
										}
										else
										{
											Swal.fire({
												icon: 'warning',
												title: 'Se debe ingresar un abono para realizar el acuerdo de pago'
											})	
										}
									}
									else
									{
										Swal.fire({
											icon: 'warning',
											title: 'Cuenta bancaría vacía'
										})	
									}
								}
								
								if(medioRecaudo == 'caja')
								{
									var abono = document.getElementById('valorAbono').value;

									if(abono != '')
									{
										var cuotas = document.getElementById('numeroCuotas').value;

										if(cuotas != '')
										{
											Swal.fire({
												icon: 'question',
												title: 'Seguro que quieres guardar?',
												showDenyButton: true,
												confirmButtonText: 'Guardar',
												denyButtonText: 'Cancelar',
												}).then((result) => {
												if (result.isConfirmed) {
													document.form2.oculto.value='2';
													document.form2.submit();
												} else if (result.isDenied) {
													Swal.fire('Guardar cancelado', '', 'info')
												}
											})
										}
										else
										{
											Swal.fire({
												icon: 'warning',
												title: 'Ingrese el número de cuotas para el acuerdo de pago'
											})	
										}
									}
									else
									{
										Swal.fire({
											icon: 'warning',
											title: 'Se debe ingresar un abono para realizar el acuerdo de pago'
										})	
									}
								}
							}
							else
							{
								Swal.fire({
									icon: 'warning',
									title: 'Selecciona el medio de recaudo'
								})	
							}
						}
						else
						{
							Swal.fire({
								icon: 'warning',
								title: 'La fecha esta vacío'
							})	
						}
					}
					else
					{
						Swal.fire({
							icon: 'warning',
							title: 'Código de usuario vacío'
						})
					}
				}
				else
				{
					Swal.fire({
						icon: 'warning',
						title: 'No hay número de acuerdo, contacte a soporte'
					})
				}
            }

			function respuestaconsulta(pregunta)
			{
				switch(pregunta)
				{
					case "1":	
						document.form2.oculto.value='2';
						document.form2.submit();
					break;
				}
				document.form2.submit();
            }

			function validaAbono()
			{
				var validacion01 = document.getElementById('valorAbono').value;

				if(validacion01 != '' && validacion01 != 0)
				{
					document.form2.oculto.value='3';
					document.form2.submit();
				}
			}

			function calculaCuota() 
			{
				var validacion01 = document.getElementById('numeroCuotas').value;

				if(validacion01 != '' && validacion01 != 0)
				{
					document.form2.oculto.value = '4';
					document.form2.submit();
				}
			}

			function actualizar()
			{
				document.form2.submit();
			}

			function despliegamodal2(_valor, _table)
			{
                document.getElementById('bgventanamodal2').style.visibility = _valor;
                
                
					if(_table == 'srvcuentasbancarias')
				{
					document.getElementById('ventana2').src = 'cuentasBancarias-ventana.php';
				}
            }

			function buscaLiquidacion()
			{
				document.form2.oculto.value = '5';
				document.form2.submit();
			}

			function pdf()
			{
				var fecha = document.getElementById("fecha").value;
				var codUsuario = document.getElementById("codUsuario").value;
				var nombre = document.getElementById("nombreSuscriptor").value;
				var documento = document.getElementById("documento").value;
				var medioPago = document.getElementById("modoRecaudo").value;
				var valorFactura = document.getElementById("valorFactura").value;
				var valorAbono = document.getElementById("valorAbono").value;

				if (codUsuario != "" && nombre != "" && documento != "" && medioPago != "" && valorFactura != "" && valorAbono != "") {
					if (medioPago == "banco") {
						var banco = document.getElementById("banco").value;
						if (banco != "") {
							document.form2.action="serv-acuerdoPagoCobroPDF.php";
							document.form2.target="_BLANK";
							document.form2.submit(); 
							document.form2.action="";
							document.form2.target="";	
						}
						else {
							alert("Faltan datos por ingresar al formulario");	
						}
					}
					else if (medioPago == "caja") {
						document.form2.action="serv-acuerdoPagoCobroPDF.php";
						document.form2.target="_BLANK";
						document.form2.submit(); 
						document.form2.action="";
						document.form2.target="";	
					}
				}
				else {
					alert("Faltan datos por ingresar al formulario");
				}
			}


			jQuery(function($){ $('#valorvl').autoNumeric('init');});
        </script>
                
	</head>
	<body>
		<IFRAME src="alertas.php" name="alertas" id="alertas" style="display:none"></IFRAME>
		<span id="todastablas2"></span>
		<table>
			<tr>
                <script>barra_imagenes("serv");</script><?php cuadro_titulos();?>
            </tr>

			<tr><?php menu_desplegable("serv");?></tr>
			<tr>
				<td colspan="3" class="cinta">
					<a href="serv-crear-acuerdos-pago.php" class="mgbt"><img src="imagenes/add.png"/></a>
					<a onclick="guardar();" class="mgbt"><img src="imagenes/guarda.png" title="Guardar"/></a>
					<a href="serv-buscar-acuerdosPago.php" class="mgbt"><img src="imagenes/busca.png" title="Buscar"/></a>
					<a onclick="mypop=window.open('plan-agenda.php','','');mypop.focus()" class="mgbt"><img src="imagenes/agenda1.png" title="Agenda" /></a>
					<a onclick="mypop=window.open('serv-principal.php','','');mypop.focus();" class="mgbt"><img src="imagenes/nv.png" title="Nueva ventana"></a>
					<a onClick="pdf()" class="mgbt"><img src="imagenes/print.png" style="width:29px;height:25px;" title="Imprimir" /></a>
					<a onclick="mypop=window.open('<?php echo $url2; ?>','','');mypop.focus();" class="mgbt"><img src="imagenes/duplicar_pantalla.png" title="Duplicar pesta&ntilde;a"></a>
                </td>
			</tr>
        </table>
        
		<div id="bgventanamodalm" class="bgventanamodalm">
			<div id="ventanamodalm" class="ventanamodalm">
				<IFRAME src="" name="ventanam" marginWidth=0 marginHeight=0 frameBorder=0 id="ventanam" frameSpacing=0 style=" width:700px; height:130px; top:200; overflow:hidden;"></IFRAME>
			</div>
		</div>

		<form name="form2" method="post" action="">
			<?php
				if(@ $_POST['oculto'] == '')
				{
					$_POST['numeroAcuerdo'] = selconsecutivo('srv_acuerdo_cab', 'codigo_acuerdo');

					$sqlParametros = "SELECT porcentaje_acuerdo_pago FROM srvparametros WHERE id = 1";
					$rowParametros = mysqli_fetch_row(mysqli_query($linkbd, $sqlParametros));

					$_POST['porcentaje_abono'] = $rowParametros[0];
				}
				
				if(@ $_POST['oculto'] == '3')
				{
					$porcentajeAbono = $_POST['porcentaje_abono'] / 100;
					$valorFactura = $_POST['valorFactura'];
					$valorMinimo = $valorFactura * $porcentajeAbono;
					$valorAbono = $_POST['valorAbono'];

					if($valorAbono < $valorMinimo)
					{
						echo "
							<script>
								Swal.fire({
									icon: 'warning',
									title: 'Error en Abono',
									text: 'El valor del abono es menor al requerido'
								})
							</script>
						";

						$_POST['valorAbono'] = '';
						$_POST['valorvl'] = '';
					}
					else
					{
						$valorAcuerdo = $valorFactura - $valorAbono;
						$_POST['valorAcuerdo'] = $valorAcuerdo;
						$valorAcuerdo = number_format($valorAcuerdo, 2, '.', ',');
						$_POST['valorAcuerdoVisible'] = $valorAcuerdo;	
					}
				}

				if(@$_POST['oculto'] == '4')
				{
					$valorAcuerdo = $_POST['valorAcuerdo'];
					$numeroDeCuotas = $_POST['numeroCuotas'];

					$valorPorCuotas = $valorAcuerdo / $numeroDeCuotas;
					$valorPorCuotas = round($valorPorCuotas, 2);

					$_POST['valorCuota'] = $valorPorCuotas;
					$valorPorCuotas = number_format($valorPorCuotas, 2, '.', ',');
					$_POST['valorCuotaVisible'] = $valorPorCuotas;
				}

				if(@$_POST['oculto'] == '5')
				{
					$codUsuario = $_POST['codUsuario'];

					$sqlCliente = "SELECT id, id_tercero FROM srvclientes WHERE cod_usuario = '$codUsuario'";
					$rowCliente = mysqli_fetch_row(mysqli_query($linkbd, $sqlCliente));

					if($rowCliente[0] != '')
					{
						$corteActivo = buscaCorteActivo();

						$sqlEstadoFactura = "SELECT estado_pago, numero_facturacion FROM srvcortes_detalle WHERE id_cliente = $rowCliente[0] AND id_corte = $corteActivo";
						$rowEstadoFactura = mysqli_fetch_row(mysqli_query($linkbd, $sqlEstadoFactura));

						if($rowEstadoFactura[0] == 'S')
						{
							$acuerdoPago = "N";

							$sqlBuscaAcuerdo = "SELECT estado_acuerdo FROM srv_acuerdo_cab WHERE id_cliente = $rowCliente[0] AND estado_acuerdo = 'Activo'";
							$rowBuscaAcuerdo = mysqli_num_rows(mysqli_query($linkbd, $sqlBuscaAcuerdo));

							if($rowBuscaAcuerdo != 0)
							{
								$acuerdoPago = "S";
							}

							if($acuerdoPago == 'N')
							{
								$idCliente = $rowCliente[0];
								$idTercero = $rowCliente[1];
								$numeroFactura = $rowEstadoFactura[1];
								$nombre = buscaNombreTerceroConId($idTercero);
								$documento = buscaDocumentoTerceroConId($idTercero);
								$valorFactura = consultaValorTotalSRVFACTURAS($numeroFactura);
								$valorFactura = ($valorFactura / 100);
								$valorFactura = ceil($valorFactura);
								$valorFactura = $valorFactura * 100;
	
								$_POST['id_cliente'] = $idCliente;
								$_POST['nombreSuscriptor'] = $nombre;
								$_POST['documento'] = $documento;
								$_POST['numeroFactura'] = $numeroFactura;
								$_POST['valorFactura'] = $valorFactura;
								$valorFactura = number_format($valorFactura, 2, '.', ',');
								$_POST['valorFacturaVisible'] = $valorFactura;
							}
							else
							{
								echo "
									<script>
										Swal.fire({
											icon: 'warning',
											title: 'El usuario tiene un acuerdo de pago activo',
										})
									</script>
								";
								$_POST['codUsuario'] = '';
							}
							
						}
						if($rowEstadoFactura[0] == 'P')
						{
							echo "
								<script>
									Swal.fire({
										icon: 'warning',
										title: 'Error factura',
										text: 'La factura actual de este usuario tiene pago realizado'
									})
								</script>
							";

							$_POST['codUsuario'] = '';
						}

						if($rowEstadoFactura[0] == 'A')
						{
							echo "
								<script>
									Swal.fire({
										icon: 'warning',
										title: 'Error factura',
										text: 'La factura actual de ese usuario tiene un acuerdo de pago'
									})
								</script>
							";

							$_POST['codUsuario'] = '';
						}
					}
					else
					{
						echo "
							<script>
								Swal.fire({
									icon: 'warning',
									title: 'Error en código de usuario',
									text: 'Código de usuario no encontrado'
								})
							</script>
						";

						$_POST['codUsuario'] = '';
					}
				}
            ?>

			<div>
				<table class="inicio">
					<tr>
						<td class="titulos" colspan="7">Crear Acuerdo de Pago</td>
						<td class="cerrar" style="width:7%" onClick="location.href='serv-principal.php'">Cerrar</td>
					</tr>
				</table>

				<div class="subpantalla" style="height:40%; width:99.5%; float:left; overflow: hidden;">
					<table class="inicio grande">
                        <tr>
                            <td class="titulos" colspan="9">Datos Acuerdo: </td>
                        </tr>
						<tr>
                            <td class="tamano01" style="width: 10%;">Código de Acuerdo: </td>
                            <td style="width: 15%">
                                <input type="text" name="numeroAcuerdo" id="numeroAcuerdo" value="<?php echo $_POST['numeroAcuerdo'] ?>" style="text-align:center; width: 100%;" readonly/>
								<input type="hidden" name="porcentaje_abono" id="porcentaje_abono" value="<?php echo $_POST['porcentaje_abono'] ?>">
                            </td>

							<td class="tamano01" style="width: 10%;">Código Usuario:</td> 
							<td style="width: 15%">
								<input type="text" name='codUsuario' id='codUsuario' value="<?php echo @$_POST['codUsuario'];?>" style="text-align:center; width: 100%;" class="colordobleclik" ondblclick="despliegamodal('visible', 'usuarios')" onchange="buscaLiquidacion();"/>

								<input type="hidden" name="id_cliente" id="id_cliente" value="<?php echo $_POST['id_cliente'] ?>">
							</td>

							<td class="tamano01" style="width: 10%;">Nombre del Suscriptor: </td>
							<td style="width: 19%">
								<input type="text" name="nombreSuscriptor" id="nombreSuscriptor" value="<?php echo @ $_POST['nombreSuscriptor'];?>" style="text-align: center; width: 100%;" readonly/>
							</td>

							<td class="tamano01" style="width: 10%;">Documento Suscriptor: </td>
							<td>
								<input type="text" name="documento" id="documento" value="<?php echo @ $_POST['documento'];?>" style="text-align: center;" readonly/>
							</td>
						</tr>

						<tr>
							<td class="tamano01" style="width:10%;">Fecha:</td>
							<td style="width:15%;">
								<input type="text" name="fecha" id="fecha" value="<?php echo @ $_POST['fecha']?>" onchange="" maxlength="10" onKeyUp="return tabular(event,this)" onKeyDown="mascara(this,'/',patron,true)" title="DD/MM/YYYY" placeholder="DD/MM/YYYY" style="height:30px;text-align:center;width:87%;"/>
								
								<a href="#" onClick="displayCalendarFor('fecha');" title="Calendario">
									<img src="imagenes/calendario04.png" style="width:25px;">
								</a>
							</td>

							<td class="tamano01">Recaudo En:</td>
							<td>
								<select  name="modoRecaudo" id="modoRecaudo" style="width:100%;" onChange="actualizar()" class="centrarSelect">
									<option class="aumentarTamaño" value="-1">:: SELECCIONE TIPO DE RECAUDO ::</option>
									<option class="aumentarTamaño" value="caja" <?php if(@$_POST['modoRecaudo']=='caja') echo "SELECTED"; ?>>Caja</option>
									<option class="aumentarTamaño" value="banco" <?php if(@$_POST['modoRecaudo']=='banco') echo "SELECTED"; ?>>Banco</option>
								</select>
							</td>
							
							<?php
								if (@$_POST['modoRecaudo'] == 'banco')
								{
							?>
									<td class="tamano01" style="width: 3cm;">Cuenta Banco: </td> 
									<td>
										<input type="text" id="banco" name="banco" style="text-align:center; width: 90%;" onKeyPress="javascript:return solonumeros(event)" onKeyUp="return tabular(event,this)" value="<?php echo $_POST['banco']?>" onClick="document.getElementById('banco').focus();document.getElementById('banco').select();" readonly>
										<input type="hidden" name="cuentaBancaria" id="cuentaBancaria" value="<?php echo $_POST['cuentaBancaria']; ?>">
										<a title="Bancos" onClick="despliegamodal2('visible','srvcuentasbancarias');" style="cursor:pointer;"><img src="imagenes/find02.png" style="width:20px;"/></a>
									</td>	

									<td colspan="2">
										<input type="text" name="nbanco" id="nbanco" style="text-align:center; width:100%;" value="<?php echo $_POST['nbanco']?>"  readonly>
									</td>	
							<?php
								}
							?>
						</tr>

						<tr>
							<td class="tamano01">Numero Factura: </td>
							<td>
								<input type="text" name="numeroFactura" id="numeroFactura" value="<?php echo $_POST['numeroFactura'] ?>" style="text-align:center; width: 100%;" readonly>	
							</td>

							<td class="tamano01">Valor Factura: </td>
							<td>
								<input type="text" name="valorFacturaVisible" id="valorFacturaVisible" value="<?php echo $_POST['valorFacturaVisible'] ?>" style="text-align:center; width: 100%;" readonly>
								<input type="hidden" name="valorFactura" id="valorFactura" value="<?php echo $_POST['valorFactura'] ?>" >	
							</td>
						</tr>

						<tr>
							<td class="tamano01">Valor Abono: </td>
							<td>
								<input type="hidden" id="valorAbono" name="valorAbono" value="<?php echo $_POST['valorAbono']?>">
                            	<input type="text" name="valorvl" id="valorvl" data-a-sign="$" data-a-dec="<?php echo $_SESSION["spdecimal"];?>" data-a-sep="<?php echo $_SESSION["spmillares"];?>" data-v-min='0' onKeyUp="sinpuntitos2('valorAbono','valorvl');return tabular(event,this);" value="<?php echo $_POST['valorvl']; ?>" style='text-align:center; width: 100%;' onblur="validaAbono();" />
							</td>

							<td class="tamano01">Valor Acuerdo: </td>
							<td>
								<input type="text" name="valorAcuerdoVisible" id="valorAcuerdoVisible" value="<?php echo $_POST['valorAcuerdoVisible'] ?>" style="text-align:center; width: 100%;" readonly>
								<input type="hidden" name="valorAcuerdo" id="valorAcuerdo" value="<?php echo $_POST['valorAcuerdo'] ?>" >
							</td>
						</tr>

						<tr>
							<td class="tamano01">Numero de cuotas: </td>
							<td>
								<input type="text" name="numeroCuotas" id="numeroCuotas" value="<?php echo $_POST['numeroCuotas'] ?>" style="text-align:center; width: 100%;" onblur="calculaCuota();">
							</td>

							<td class="tamano01">Valor de cuota: </td>
							<td>
								<input type="text" name="valorCuotaVisible" id="valorCuotaVisible" value="<?php echo $_POST['valorCuotaVisible'] ?>" style="text-align:center; width: 100%;" readonly>
								<input type="hidden" name="valorCuota" id="valorCuota" value="<?php echo $_POST['valorCuota'] ?>">
							</td>
						</tr>
					</table>
				</div>	
			</div>	
			<?php

				if($_POST['oculto'] == '2')
				{
					$codAcuerdo = $_POST['numeroAcuerdo'];
					$idCliente = $_POST['id_cliente'];
					$codUsuario = $_POST['codUsuario'];
					$nombre = $_POST['nombreSuscriptor'];
					$documento = $_POST['documento'];
					$fec = explode("/", $_POST['fecha']);
                	$fecha = $fec[2].'-'.$fec[1].'-'.$fec[0];
					$año = $fec[2];
					$numeroFactura = $_POST['numeroFactura'];
					$valorFactura = $_POST['valorFactura'];
					$valorAbono = $_POST['valorAbono'];
					$valorAcuerdo = $_POST['valorAcuerdo'];
					$numeroCuotas = $_POST['numeroCuotas'];
					$valorCuota = $_POST['valorCuota'];
					$medioPago = $_POST['modoRecaudo'];
					$codigoComprobante = '39';

					$acuerdoPago = concepto_cuentasn2('01','AP',10,'01',$fecha);

					for ($i=0; $i < count($acuerdoPago); $i++) 
					{ 
						if($acuerdoPago[$i][2] == 'S')
						{
							$cuentaAcuerdoPago = $acuerdoPago[$i][0];
						}
					}

					if ($medioPago == 'caja') 
					{
						$sqlCuentaCaja = "SELECT cuentacaja FROM tesoparametros";
						$resCuentaCaja = mysqli_query($linkbd,$sqlCuentaCaja);
						$rowCuentaCaja = mysqli_fetch_row($resCuentaCaja);
						$numeroCuenta = $rowCuentaCaja[0];
					} 
					else {$numeroCuenta = $_POST['cuentaBancaria'];}

					$sqlAcuerdoCab = "INSERT INTO srv_acuerdo_cab (codigo_acuerdo, id_cliente, cod_usuario, nombre_suscriptor, documento, fecha_acuerdo, codigo_factura, valor_factura, valor_abonado, valor_acuerdo, numero_cuotas, valor_cuota, cuotas_liquidadas, valor_liquidado, medio_pago, numero_cuenta, estado_acuerdo) VALUES ($codAcuerdo, $idCliente, '$codUsuario', '$nombre', '$documento', '$fecha', $numeroFactura, $valorFactura, $valorAbono, $valorAcuerdo, $numeroCuotas, $valorCuota, 0, 0, '$medioPago', '$numeroCuenta', 'Activo')";
					mysqli_query($linkbd, $sqlAcuerdoCab);

					$sqlCliente = "SELECT zona_uso FROM srvclientes WHERE id = $idCliente ";
					$resCliente = mysqli_query($linkbd,$sqlCliente);
					$rowCliente = mysqli_fetch_row($resCliente);

					if($rowCliente[0] == 1) {

						$cargoFijo = "cargo_fijo_u";
						$consumo = "consumo_u";
						$subsidioCF = "subsidio_cf_u";
						$subsidioCS = "subsidio_cs_u";
						$contribucion = "contribucion_u";
					} else {
						$cargoFijo = "cargo_fijo_r";
						$consumo = "consumo_r";
						$subsidioCF = "subsidio_cf_r";
						$subsidioCS = "subsidio_cs_r";
						$contribucion = "contribucion_r";
					}
					
					$sql = "SELECT * FROM srvcortes_detalle WHERE estado_pago = 'P' AND id_cliente = $idCliente ORDER BY id DESC LIMIT 1"; //buscamos el ultimo corte que se pago
					$res = mysqli_query($linkbd,$sql);
					$row = mysqli_fetch_row($res);

					if($row[0] != '')//si el cliente ya tiene pagos solo tomara las facturas que tenga vencida y aún no ha pagado
					{
						$sql = "SELECT * FROM srvcortes_detalle WHERE estado_pago = 'V' AND id_cliente = '$idCliente' AND id > $row[0] ";//buscamos los cortes vencidos luego del que se pago
						$res = mysqli_query($linkbd,$sql);
						while($row = mysqli_fetch_row($res))
						{
							$sqlDetalles = "SELECT id_tipo_cobro, credito, debito FROM srvdetalles_facturacion WHERE numero_facturacion = $row[3] ";
							$resDetalles = mysqli_query($linkbd,$sqlDetalles);
							while($rowDetalles = mysqli_fetch_row($resDetalles))
							{
								//var_dump($rowDetalles);
								switch($rowDetalles[0]) 
								{
									case 1:
										$saldoCargoFijo = $saldoCargoFijo + $rowDetalles[1];
										break;
									case 2:
										$saldoConsumo = $saldoConsumo + $rowDetalles[1];
										break;
									case 5:
										$saldoContribucion = $saldoContribucion + $rowDetalles[1];
										break;
									case 11:
										$saldoInteres = $saldoInteres + $rowDetalles[1];
										break;
									case 12:
										$saldoAlumbrado = $saldoAlumbrado + $rowDetalles[1];
										break;
								}
							}
						}
					}
					else //Si el cliente no ha tenido ningun pago el programa tomara todas las facturas vencidas
					{
						$sql = "SELECT * FROM srvcortes_detalle WHERE estado_pago = 'V' AND id_cliente = $idCliente"; //buscamos los cortes vencidos luego del que se pago
						$res = mysqli_query($linkbd,$sql);
						while($row = mysqli_fetch_row($res))
						{
							$sqlDetalles = "SELECT id_tipo_cobro, credito, debito FROM srvdetalles_facturacion WHERE numero_facturacion = $row[3] ";
							$resDetalles = mysqli_query($linkbd,$sqlDetalles);
							while($rowDetalles = mysqli_fetch_row($resDetalles))
							{
								switch($rowDetalles[0]) 
								{
									case 1:
										$saldoCargoFijo = $saldoCargoFijo + $rowDetalles[1];
										break;
									case 2:
										$saldoConsumo = $saldoConsumo + $rowDetalles[1];
										break;
									case 5:
										$saldoContribucion = $saldoContribucion + $rowDetalles[1];
										break;
									case 11:
										$saldoInteres = $saldoInteres + $rowDetalles[1];
										break;
									case 12:
										$saldoAlumbrado = $saldoAlumbrado + $rowDetalles[1];
										break;
								}
							}
						}
					}

					$sqlServicios = "SELECT id FROM srvservicios";
					$resServicios = mysqli_query($linkbd, $sqlServicios);
					while($rowServicios = mysqli_fetch_row($resServicios))
					{
						$sqlValorXServicio = "SELECT SUM(credito), SUM(debito), corte FROM srvdetalles_facturacion WHERE numero_facturacion = $numeroFactura AND id_servicio = $rowServicios[0] AND (credito > 0 OR debito > 0) AND tipo_movimiento = '101'";
						$sqlValorXServicio = mysqli_fetch_row(mysqli_query($linkbd, $sqlValorXServicio));

						if(($sqlValorXServicio[0] != '' AND $sqlValorXServicio[0] != NULL) || ($sqlValorXServicio[1] != '' AND $sqlValorXServicio[1] != NULL))
						{
							$valorServicio = $sqlValorXServicio[0] - $sqlValorXServicio[1];

							$valorServicio = round($valorServicio, 2, PHP_ROUND_HALF_UP);
	
							$abonoValorServicio = ($valorServicio / $valorFactura) * $valorAbono;
	
							$valorServicioMenosAbono = $valorServicio - $abonoValorServicio;
	
							$valorServicioMenosAbono = round($valorServicioMenosAbono, 2, PHP_ROUND_HALF_UP);
	
							$valorCuotaServicio = $valorServicioMenosAbono / $numeroCuotas;
	
							$valorCuotaServicio = round($valorCuotaServicio, 2, PHP_ROUND_HALF_UP);
	
							$sqlAcuerdoDet = "INSERT INTO srv_acuerdo_det (codigo_acuerdo, id_servicio, valor_cuota_servicio) VALUES ($codAcuerdo, $rowServicios[0], $valorCuotaServicio)";
							mysqli_query($linkbd, $sqlAcuerdoDet);
	
							$sqlSaldaFactura = "INSERT INTO srvdetalles_facturacion (corte, id_cliente, numero_facturacion, tipo_movimiento, fecha_movimiento, id_servicio, id_tipo_cobro, credito, debito, estado) VALUES ($sqlValorXServicio[2], $idCliente, $numeroFactura, '202', '$fecha', $rowServicios[0], 12, 0, $valorServicio, 'S')";
							mysqli_query($linkbd, $sqlSaldaFactura);
						}	
					}	

					$usuario = $_SESSION["usuario"];
					$cedula = $_SESSION["cedulausu"];
					$ip = getRealIP();
					$hoy = date("Y-m-d H:i:s");

					$sqlAuditoria = "INSERT INTO srv_auditoria (id_srv_funciones, accion, cedula, usuario, ip, fecha_hora, consecutivo) VALUES (5, 'Crear', '$cedula', '$usuario', '$ip', '$hoy', $codAcuerdo)";
					mysqli_query($linkbd, $sqlAuditoria);
					
					$sqlCambiaEstado = "UPDATE srvcortes_detalle SET estado_pago = 'A' WHERE numero_facturacion = $numeroFactura";
					if (mysqli_query($linkbd,$sqlCambiaEstado))
					{
						echo "
							<script>
								Swal.fire({
									icon: 'success',
									title: 'Guardado exitoso!',
									}).then((result) => {
									if (result.value) {
										document.location.href = 'serv-crear-acuerdos-pago.php';
									} 
								})
							</script>";
					}
					else 
					{
						echo"
							<script>
								Swal.fire('Error en el guardar', '', 'error');
							</script>";
					}
				}
			?>

			<input type="hidden" name="cambio" id="cambio" value="1"/>
			<input type="hidden" name="oculto" id="oculto" value="1"/>
			
			<div id="bgventanamodal2">
				<div id="ventanamodal2">
					<IFRAME  src="" name="buster" marginWidth=0 marginHeight=0 frameBorder=0 id="ventana2" frameSpacing=0 style="left:500px; width:900px; height:500px; top:200;"></IFRAME>
				</div>
			</div>
			
			
        </form>                          
    </body>   
</html>
