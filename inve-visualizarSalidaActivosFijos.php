<?php
	header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=utf8");
	require 'comun.inc';
	require 'funciones.inc';
	session_start();
	$linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");
	cargarcodigopag($_GET['codpag'],$_SESSION["nivel"]);
	date_default_timezone_set("America/Bogota");
?>
<!DOCTYPE >
<html lang=es>
	<head>
    <meta charset="utf-8"/>
		<meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1"/>
		<meta name="viewport" content="user-scalable=no">
		<title>:: IDEAL 10 - Activos fijos</title>
		<link href="favicon.ico" rel="shortcut icon"/>
		<link href="css/css2z.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
		<link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
		<link href="css/css4.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
        <link href="css/tabs.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css"/>
        <link rel="stylesheet" href="sweetalert2/dist/sweetalert2.min.css">
        <script type="text/javascript" src="css/programas.js"></script>
        <script src="sweetalert2/dist/sweetalert2.min.js"></script>
		<script src="vue/vue.min.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script src="css/programas.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script src="css/calendario.js?<?php echo date('d_m_Y_h_i_s');?>"></script>

        <script>
            function pdf(){
				document.form2.action = "inve-salidaActivosFijosPDF.php";
				document.form2.target = "_BLANK";
				document.form2.submit(); 
				document.form2.action = "";
				document.form2.target = "";
			}
		</script>

		<style>
			input[type=number]::-webkit-inner-spin-button,
			input[type=number]::-webkit-outer-spin-button { 
				-webkit-appearance: none; 
				margin: 0; 
			}
			input[type=number]{
				text-align:right;
				box-sizing: border-box;
				font: 120% sans-serif;
				/* width: 100% !important; */
			}
			[v-cloak]{display : none;}

			.centrarSelect {

				height: 30px !important;
				text-align-last:center !important;
			}

			.aumentarTamaño {

				font-size:15px;
			}

            .tamano01 {
                text-align: center !important;
            }

            SELECT{
                font-size:11px;
                font-family: -apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, "Helvetica Neue", Arial, "Noto Sans", sans-serif, "Apple Color Emoji", "Segoe UI Emoji", "Segoe UI Symbol", "Noto Color Emoji";
                font-weight:bold;
                outline:none;
                border-radius:3px;
                border:1px solid rgba(0,0,0, 0.2);
                color:222;
                /* background-color:#B2BFD9; */
            }
            option{
                font-family: -apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, "Helvetica Neue", Arial, "Noto Sans", sans-serif, "Apple Color Emoji", "Segoe UI Emoji", "Segoe UI Symbol", "Noto Color Emoji"; 
                font-size: 10px; 
                color: 333;
                /* background-color:#eee; */
                color:#222;
            }
		</style>
	</head>
	<body>
		<header>
			<table>
				<tr><script>barra_imagenes("inve");</script><?php cuadro_titulos();?></tr>
			</table>
		</header>

		<form name="form2" method="post" action="">
			<section id="myapp" v-cloak >
				<nav>
					<table>
						<tr><?php menu_desplegable("inve");?></tr>
						<tr>
							<td colspan="3" class="cinta">
								<img src="imagenes/add.png" v-on:click="location.href='inve-salidaActivosFijos.php'" class="mgbt" title="Nuevo">
								<img src="imagenes/guardad.png" title="Guardar"  class="mgbt">
								<img src="imagenes/busca.png" v-on:click="location.href='inve-buscarSalidaActivosFijos'" class="mgbt" title="Buscar">
								<img src="imagenes/nv.png" onClick="mypop=window.open('inve-principal','',''); mypop.focus();" class="mgbt" title="Nueva Ventana">
                                <a onclick="pdf()"><img src="imagenes/print.png" title="Imprimir" class="mgbt">
                                <a href="inve-menuSalidas"><img src="imagenes/iratras.png" class="mgbt" alt="Atrás"></a>
							</td>
						</tr>
					</table>
				</nav>

				<article>    
                    <table class="inicio ancho">
                        <tr>
                            <td class="titulos" colspan="12">.: Gestión de activo</td>
                        </tr>
                        
                        <tr>
                            <td class="textonew01" style="width:3.5cm;">.: Consecutivo:</td>
                            <td style="width: 11%;">
                                <input type="text" v-model="consecutivo" name="consecutivo" style="text-align: center;" readonly>
                            </td>

                            <td class="textonew01" style="width:2.5cm;">.: Fecha:</td>
                            <td style="width: 11%;">
                                <input type="text" v-model="fecha" name="fecha" style="text-align: center;" readonly>
                            </td>

                            <td class="textonew01" style="width:3.5cm;">Descripción:</td>
                            <td>
                                <input type="text" v-model="descripcion" name="descripcion" style="width:100%" readonly>
                            </td>

                            <td class="textonew01" style="width:3.5cm;">Total:</td>
                            <td>
                                <input type="text" v-model="formatonumero(totalValores)" name="totalValores" style="text-align: center;" readonly>
                            </td>

                            <td>
                                <input type="text" v-model="estado" style="text-align: center;" readonly>
                            </td>
                        </tr>
                    </table>

                    <div class="subpantalla" style='height:40vh; width:100%; overflow-x:hidden; resize: vertical;'>
                            <table>        
                                <thead>
                                    <tr>
                                        <th width="6%" class="titulosnew00">Bodega</th>
                                        <th width="6%" class="titulosnew00">Centro costos</th>
                                        <th width="10%" class="titulosnew00">Articulo</th>
                                        <th width="25%" class="titulosnew00">Nombre articulo</th>
                                        <th width="10%" class="titulosnew00">Unidad medida</th>
                                        <th width="10%"class="titulosnew00">Cantidad</th>
                                        <th width="10%" class="titulosnew00">Valor unitario</th>
                                        <th width="10%" class="titulosnew00">Valor total</th>
                                    </tr>
                                </thead>

                                <tbody>
                                    <tr v-for="(articulo,index) in listaArticulos" v-bind:class="(index % 2 ? 'saludo1a' : 'saludo2')" style="font: 100% sans-serif;">
                                        <td style="text-align:center; width:6%;">{{ articulo[0] }}</td>
                                        <td style="text-align:center; width:6%;">{{ articulo[1] }}</td>
                                        <td style="text-align:center; width:10%;">{{ articulo[2] }}</td>
                                        <td style="text-align:center; width:25%;">{{ articulo[3] }}</td>
                                        <td style="text-align:center; width:10%;">{{ articulo[4] }}</td>
                                        <td style="text-align:center; width:10%;">{{ articulo[5] }}</td>
                                        <td style="text-align:center; width:10%;">{{ formatonumero(articulo[6]) }}</td>
                                        <td style="text-align:center; width:10%;">{{ formatonumero(articulo[7]) }}</td>

                                        
                                        <input type='hidden' name='codArticulo[]' v-model="articulo[2]">
                                        <input type='hidden' name='nomArticulo[]' v-model="articulo[3]">
                                        <input type='hidden' name='canSalida[]' v-model="articulo[5]">
                                        <input type='hidden' name='unidadMedida[]' v-model="articulo[4]">
                                        <input type='hidden' name='valorUnidad[]' v-model="articulo[6]">
                                        <input type='hidden' name='valorTotal[]' v-model="articulo[7]">
                                    </tr>
                                    <tr>
                                        <td class="titulosnew00" style="text-align:right;" colspan="7">Total: </td>
                                        <td style="text-align:center;">{{ formatonumero(totalValores) }}</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>

					<div id="cargando" v-if="loading" class="loading">
						<span>Cargando...</span>
					</div>

				</article>
			</section>
		</form>

		<script src="node_modules/read-excel-file/bundle/read-excel-file.min.js"></script>
		<script src="node_modules/xlsx/dist/xlsx.full.min.js"></script>
        <script src="Librerias/vue/vue.min.js"></script>
		<script src="Librerias/vue/axios.min.js"></script>
		<script type="module" src="almacen/salidaTraslado/visualizar/inve-visualizarSalidaActivosFijos.js"></script>
        
	</body>
</html>