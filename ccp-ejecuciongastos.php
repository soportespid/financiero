<?php
    ini_set('max_execution_time',3600);
	require "comun.inc";
    require "funciones.inc"; 
    $linkbd = conectar_v7();
	session_start();
	cargarcodigopag(@$_GET['codpag'], @$_SESSION['nivel']);
	header("Cache-control: private"); // Arregla IE 6 
    date_default_timezone_set("America/Bogota");

    $maxVersion = ultimaVersionGastosCCPET();
?>

<!DOCTYPE >
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="es"> 
	<head>
	 	<meta http-equiv="Content-Type" content="text/html" charset="iso-8859-1"/>
        <meta http-equiv="X-UA-Compatible" content="IE=9"/>
		<title>:: Ideal - Presupuesto</title>
        <link href="css/css2.css" rel="stylesheet" type="text/css" />
        <link href="css/css3.css" rel="stylesheet" type="text/css" />
        <link href="css/css4.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="css/programas.js"></script>
        <script type="text/javascript" src="css/funciones.js"></script>
        <script type="text/javascript" src="jquery-1.11.0.min.js"></script> 
        <script type="text/javascript" src="JQuery/jquery-1.12.0.min.js"></script> 
        <script src="css/calendario.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<link rel="stylesheet" href="bootstrap/css/bootstrap.css">
        <link rel="stylesheet" href="bootstrap/css/estilos.css">
		<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">

        <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
        <script type="text/javascript" src="bootstrap/fontawesome.5.11.2/js/all.js"></script>
        <?php titlepag();?> 

        <script>

            $(window).load(function () { $('#cargando').hide();});

            function excell()
            {
                document.form2.action="ccp-ejecuciongastosexcel.php";
                document.form2.target="_BLANK";
                document.form2.submit(); 
                document.form2.action="";
                document.form2.target="";
            }
            function excell1()
            {
                document.form2.action="ccp-ejecuciongastosexcel1.php";
                document.form2.target="_BLANK";
                document.form2.submit(); 
                document.form2.action="";
                document.form2.target="";
            }
            function validar()
            {
                document.form2.submit();
            }

            function generarEjecucion()
            {
                document.form2.oculto.value = '3';
                document.form2.submit();
            }
        </script>
    </head>
    <body>
        <IFRAME src="alertas.php" name="alertas" id="alertas" style="display:none"></IFRAME>
        <span id="todastablas2"></span>
        <table>
            <tr><script>barra_imagenes("ccpet");</script><?php cuadro_titulos();?></tr>	 
            <tr><?php menu_desplegable("ccpet");?></tr>
        	<tr>
          		<td colspan="3" class="cinta">
					<a><img src="imagenes/add.png" title="Nuevo" onClick="location.href='#'" class="mgbt"/></a>
					<a class="mgbt"><img src="imagenes/guardad.png"/></a>
					<a><img src="imagenes/busca.png" title="Buscar"  onClick="location.href='#'" class="mgbt"/></a>
					<a href="#" onClick="mypop=window.open('ccp-principal.php','','');mypop.focus();" class="mgbt"><img src="imagenes/nv.png" title="Nueva Ventana"></a>
                    <img src="imagenes/excel.png" title="Excel" onClick='excell()' class="mgbt"/>
                    <img src="imagenes/excel.png" title="Excel Acumulado" onClick='excell1()' class="mgbt"/>
					<img src="imagenes/iratras.png" title="Atr&aacute;s" onClick="location.href='ccp-ejecucionpresupuestal.php'" class="mgbt"/>
				</td>
        	</tr>
		</table>
        <div class="row" style="margin: 5px 4px 0px">
            <div class="col-12">
                <form name="form2" method="post">
                    <div class="loading" id="divcarga"><span>Cargando...</span></div>
                    <input type="hidden" name="oculto" value="<?php echo $_POST['oculto'] ?>">

                <div class="row" style="border-radius:2px; background-color: #E1E2E2; ">

                    <div class="col-md-1" style="display: grid; align-content:center;">
                        <label for="" style="margin-bottom: 0; font-weight: bold">Fecha ini: </label>
                    </div>
                    <div class="col-2 container--crear__datos container row">
                        <div class="col-9">
                            <input type="text" name="fecha" value="<?php echo @$_POST['fecha']; ?>" class="form-control imput--fecha" aria-describedby="basic-addon1" onKeyPress="javascript:return solonumeros(event)" onKeyUp="return tabular(event,this)"  id="fc_1198971545" onKeyDown="mascara(this,'/',patron,true)" title="DD/MM/YYYY" placeholder="DD/MM/YYYY" style = "margin-top:5px;">
                        </div>
                        <div class="col-2">
                            <a href="#" onClick="displayCalendarFor('fc_1198971545');" title="Calendario">
                            <img src="imagenes/calendario04.png" style="width:30px; padding-top:5px;padding-left: -5px;"/></a>
                        </div>
                    </div>
                    
                    <div class="col-md-1" style="display: grid; align-content:center;">
                        <label for="" style="margin-bottom: 0; font-weight: bold">Fecha fin: </label>
                    </div>
                    <div class="col-2 container--crear__datos container row">
                        <div class="col-9">
                            <input type="text" name="fechafin" value="<?php echo @$_POST['fechafin']; ?>" class="form-control imput--fecha" aria-describedby="basic-addon1" onKeyPress="javascript:return solonumeros(event)" onKeyUp="return tabular(event,this)"  id="fc_1198971546" onKeyDown="mascara(this,'/',patron,true)" title="DD/MM/YYYY" placeholder="DD/MM/YYYY" style = "margin-top:5px;">
                        </div>
                        <div class="col-2">
                            <a href="#" onClick="displayCalendarFor('fc_1198971546');" title="Calendario">
                            <img src="imagenes/calendario04.png" style="width:30px; padding-top:5px;padding-left: -5px;"/></a>
                        </div>
                    </div>


                    <div class="col-md-1" style="display: grid; align-content:center;">
                        <label for="" style="margin-bottom: 0; font-weight: bold">Unidad: </label>
                    </div>
                    <div class="col-md-1" style="padding: 4px">
                        <select name="unidadEjecutora" id="unidadEjecutora" style="width:85%;" class="form-control select" onChange="validar()" onKeyUp="return tabular(event,this)">
                            <option value=''>Todos</option>
				            <?php
                                $sqlr = "select *from pptouniejecu where estado='S'";
                                $res = mysqli_query($linkbd, $sqlr);
                                while ($row = mysqli_fetch_row($res))
                                {
                                    echo "<option value=$row[0] ";
                                    $i=$row[0];
                                    if($i==$_POST['unidadEjecutora'])
                                    {
                                        echo "SELECTED";
                                    }
                                    echo ">".$row[0]." - ".$row[1]."</option>";	 	 
                                }
				            ?>
			            </select>
                    </div>
                    <div class="col-md-2" style="display: grid; align-content:center;">
                        <label for="" style="margin-bottom: 0; font-weight: bold">Medio de pago: </label>
                    </div>
                    <div class="col-md-1" style="padding: 4px">
                        <select name="medioDePago" id="medioDePago" style="width:85%;" class="form-control select" onChange="validar()" onKeyUp="return tabular(event,this)">
                            <option value=''>Todos</option>
                            <option value='CSF' <?php if($_POST['medioDePago']=='CSF') echo "SELECTED"; ?>>CSF</option>
                            <option value='SSF' <?php if($_POST['medioDePago']=='SSF') echo "SELECTED"; ?>>SSF</option>
			            </select>
                    </div>

                    <div class="col-md-1" style="display: grid; align-content:center;">
                        <button type="button" class="btn btn-primary" name="Generar" id="Generar" onClick="generarEjecucion()">
                        Generar
                        </button>
                    </div>
                </div>
            </div>
        </div>
        <div class="subpantalla" style="height:450px; width:99.6%; padding:10px !important; background-color: white;">
            <table v-if="show_table_search" class="table table-hover">
                <thead >
                    <tr style="font-size: 90%; background-color: #2ECCFA; ">
                        <th style="border-radius: 5px 0px 0px 0px !important;">Detalle</th>
                        <th>C&oacute;digo</th>
                        <th>Nombre</th>
                        <th>Inicial</th>
                        <th>Adici&oacute;n</th>
                        <th>Reducci&oacute;n</th>
                        <th>Credito</th>
                        <th>Contracredito</th>
                        <th>Definitivo</th>
                        <th>Disponibilidad</th>
                        <th>Compromiso</th>
                        <th>Obligacion</th>
                        <th>Pago</th>
                        <th style="border-radius: 0px 5px 0px 0px !important;">Saldo</th>
                    </tr>
                </thead>
                <tbody>
                    <?php

                        if($_POST['oculto']==""){echo"<script>document.getElementById('divcarga').style.display='none';</script>";}

                        if($_POST['oculto'] == '3')
                        {
                            $crit2 = '';
                            $vigencia = '';
                            $critinv2 = '';

                            preg_match("/([0-9]{2})\/([0-9]{2})\/([0-9]{4})/", $_POST['fecha'],$fecha);
					        $fechaIni=''.$fecha[3].'-'.$fecha[2].'-'.$fecha[1].'';

                            preg_match("/([0-9]{2})\/([0-9]{2})\/([0-9]{4})/", $_POST['fechafin'],$fechaf);
					        $fechaFin=''.$fechaf[3].'-'.$fechaf[2].'-'.$fechaf[1].'';

                            $vigencia = $fecha[3];

                            $sql = "SELECT codigo, nombre, tipo FROM cuentasccpet WHERE municipio=1 AND version='$maxVersion' ORDER BY id ASC";
                            $res = mysqli_query($linkbd, $sql);
                            $i = 0;
                
                            while($row = mysqli_fetch_row($res))
                            {
                                $valorTotal = 0;
                                $valorCxp = 0;
                                $valorEgreso = 0;
                                $rubro = '';
                                $negrilla = 'font-weight: normal;';
                                $rubro = $row[0];

                                if($row[2] == 'A')
                                {
                                    $rubro = $row[0].'%';
                                    $negrilla = 'font-weight: bold;';
                                }

                                if($_POST['unidadEjecutora'] == '')
                                {
                                    $_POST['unidadEjecutora'] = '%';
                                }

                                $arregloGastos = reporteGastosCcpet($rubro, $_POST['unidadEjecutora'], $_POST['medioDePago'], $vigencia, $fechaIni, $fechaFin);

                                if($arregloGastos[0] > 0 || $arregloGastos[1] > 0 || $arregloGastos[3] > 0)
                                {
                                    $nombreCuenta = buscaNombreCuentaCCPET($row[0]);
                                    $cuentas[$row[0]]["numCuenta"]=$row[0];
                                    $cuentas[$row[0]]["nomCuenta"]="$nombreCuenta";
                                    $cuentas[$row[0]]["tipo"]=$row[2];
                                    $cuentas[$row[0]]["presuInicial"]=$arregloGastos[0];
                                    $cuentas[$row[0]]["adicion"]=$arregloGastos[1];
                                    $cuentas[$row[0]]["reduccion"]=$arregloGastos[2];
                                    $cuentas[$row[0]]["credito"]=$arregloGastos[3];
                                    $cuentas[$row[0]]["conCredito"]=$arregloGastos[4];
                                    $cuentas[$row[0]]["presuDefinitivo"]=$arregloGastos[5];
                                    $cuentas[$row[0]]["cdp"]=$arregloGastos[6];
                                    $cuentas[$row[0]]["rp"]=$arregloGastos[7];
                                    $cuentas[$row[0]]["cxp"]=$arregloGastos[8];
                                    $cuentas[$row[0]]["egreso"]=$arregloGastos[9];
                                    $cuentas[$row[0]]["saldo"]=$arregloGastos[10];
                                }
                                
                            }

                            $i = 0;

                            foreach ($cuentas as $key => $value) 
					        {
                                $numeroCuenta=$cuentas[$key]['numCuenta'];
                                $nombreCuenta=$cuentas[$key]['nomCuenta'];
                                $tipo=$cuentas[$key]['tipo'];
                                $presupuestoInicial=$cuentas[$key]['presuInicial'];
                                $adicion=$cuentas[$key]['adicion'];
                                $reduccion=$cuentas[$key]['reduccion'];
                                $credito=$cuentas[$key]['credito'];
                                $contracredito=$cuentas[$key]['conCredito'];
                                $presupuestoDefinitivo=$cuentas[$key]['presuDefinitivo'];
                                $cdp=$cuentas[$key]['cdp'];
                                $rp=$cuentas[$key]['rp'];
                                $cxp=$cuentas[$key]['cxp'];
                                $egreso=$cuentas[$key]['egreso'];
                                $saldo=$cuentas[$key]['saldo']; 
                                 $style='';
                                if(round($saldo,0)<0 || round($rp,0)>round($cdp,0) || round($egreso,0)>round($cxp,0) || round($cxp,0)>round($rp,0))
                                {$style='background: yellow';} 
                                $negrilla = 'font-weight: normal;';
                                if($tipo == 'A')
                                {
                                    $negrilla = 'font-weight: bold;';
                                }
                                if($_POST['unidadEjecutora'] == '%')
                                {
                                    $_POST['unidadEjecutora'] = '';
                                }

                                $compromisoEnEjecucion = $rp - $cxp;
                                $cuentasPorPagar = $cxp - $egreso;


                                echo "
                                <tr style='$negrilla font-size: 90%; $style'>
                                    <td style='padding-left: 40px; padding-top: 15px'>";
                                    if($tipo == 'C'){
                                        echo "<a onClick='verDetalleGastos($i, \"$numeroCuenta\",\"$_POST[unidadEjecutora]\",\"$_POST[medioDePago]\",\"$vigencia\",\"\",\"$fechaIni\",\"$fechaFin\")' style='cursor:pointer;'>
                                        <img id='img".$i."' style='width: 15px;' src='imagenes/plus.gif'>
                                        </a> 
                                        ";
                                        }
                                    echo "
                                    </td>
                                    <td>$numeroCuenta</td>
                                    <td>$nombreCuenta</td>
                                    <td style='width:300px% !important;'>".number_format($presupuestoInicial, 2, ',', '.')."</td>
                                    <td style='width:300px% !important;'>".number_format($adicion, 2, ',', '.')."</td>
                                    <td style='width:300px% !important;'>".number_format($reduccion, 2, ',', '.')."</td>
                                    <td style='width:300px% !important;'>".number_format($credito, 2, ',', '.')."</td>
                                    <td style='width:300px% !important;'>".number_format($contracredito, 2, ',', '.')."</td>
                                    <td style='width:300px% !important;'>".number_format($presupuestoDefinitivo, 2, ',', '.')."</td>
                                    <td style='width:300px% !important;'>".number_format($cdp, 2, ',', '.')."</td>
                                    <td style='width:300px% !important;'>".number_format($rp, 2, ',', '.')."</td>
                                    <td style='width:300px% !important;'>".number_format($cxp, 2, ',', '.')."</td>
                                    <td style='width:300px% !important;'>".number_format($egreso, 2, ',', '.')."</td>
                                    <td style='width:300px% !important;'>".number_format($saldo, 2, ',', '.')."</td>
                                    
                                </tr>
                                <input type='hidden' name='codigo[]' value='$numeroCuenta'>
                                <input type='hidden' name='nombre[]' value='$nombreCuenta'>
                                <input type='hidden' name='tipo[]' value='$tipo'>
                                <input type='hidden' name='presupuestoInicial[]' value='$presupuestoInicial'>
                                <input type='hidden' name='adicion[]' value='$adicion'>
                                <input type='hidden' name='reduccion[]' value='$reduccion'>
                                <input type='hidden' name='credito[]' value='$credito'>
                                <input type='hidden' name='contracredito[]' value='$contracredito'>
                                <input type='hidden' name='presupuestoDefinitivo[]' value='$presupuestoDefinitivo'>
                                <input type='hidden' name='cdp[]' value='$cdp'>
                                <input type='hidden' name='rp[]' value='$rp'>
                                <input type='hidden' name='cxp[]' value='$cxp'>
                                <input type='hidden' name='egreso[]' value='$egreso'>
                                <input type='hidden' name='saldo[]' value='$saldo'>
                                <input type='hidden' name='compromisoEnEjecucion[]' value='$compromisoEnEjecucion'>
                                <input type='hidden' name='cuentasPorPagar[]' value='$cuentasPorPagar'>
                                
                                <tr cellspacing='0' cellpadding='0' style = ''>
                                    <td align='center' style='padding: 0px !important; border: 0px'></td>
                                    <td colspan='13' align='right' style='padding: 0px !important; border: 0px'>
                                        <div id='detalle".$i."' style='display:none;'></div>
                                    </td>
                                </tr>";

                                $i++;
                            }

                            echo"<script>document.getElementById('divcarga').style.display='none';</script>";
                           
                        }
                    ?>
                    </form>
                </tbody>
            </table>
        </div>
    </body>
</html>