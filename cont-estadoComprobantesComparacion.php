<?php
	header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=utf8");

	require "comun.inc";
	require "funciones.inc";

	$linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");

	session_start();
    date_default_timezone_set("America/Bogota");
?>
<!DOCTYPE html>
<html lang=es>
	<head>
		<meta charset="utf-8"/>
		<meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1"/>
		<meta name="viewport" content="user-scalable=no">
		<title>:: IDEAL 10 - Contabilidad</title>
		<link href="favicon.ico" rel="shortcut icon"/>
		<link href="css/css2z.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
		<link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
		<link href="css/css4.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
		<script type="text/javascript" src="css/programas.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
	</head>
	<body>
		<header>
			<table>
				<tr><script>barra_imagenes("cont");</script><?php cuadro_titulos();?></tr>	
			</table>
			<nav>
				<table>
					<tr><?php menu_desplegable("cont");?></tr>
					<tr>
						<td colspan="3" class="cinta">
							<img src="imagenes/add2.png" class="mgbt1"/>
							<img src="imagenes/guardad.png" class="mgbt1" style="width:24px;"/>
							<img src="imagenes/buscad.png" class="mgbt1"/>
							<img src="imagenes/nv.png" title="Nueva Ventana" onClick="mypop=window.open('cont-principal.php','','');mypop.focus();" class="mgbt"/>
							<img src="imagenes/iratras.png" class="mgbt" onClick="location.href='cont-estadocomprobantes.php'" title="Atrás"/>
						</td>
					</tr>
				</table>
			</nav>
			<IFRAME src="alertas.php" name="alertas" id="alertas" style="display:none"></IFRAME>
			<span id="todastablas2"></span>
		</header>
		<section>
				<div style="height:79vh; width:99.9%; overflow-x:hidden;">
				<div class="subpantalla" style="height:93%; width:49%; overflow-x: hidden; float:left;">
					<table class="inicio">
						<tr>
							<td class="titulos" colspan="4">.: Revisión bancos</td>
						</tr>

						<tr style="vertical-align: top;text-align: left;" id="listamenus">
							<td width='45%'>
								<ol>
									<li onClick="location.href='cont-comparabancoegreso.php'" class='mgbt3'>Comparar Valor Banco - Egreso Neto</li>
									<li onClick="location.href='cont-comparabancoegresonomina.php'" class='mgbt3'>Comparar Valor Banco - Egreso Nomina Neto</li>
									<li onClick="location.href='adm-reciboscaja-cont.php'" class='mgbt3'>Comparar Recibo de ingreso (Recibo de caja) - Contabilidad</li>
									<li onClick="location.href='cont-comparacion-ingresosInternos-contabilidad.php'" class='mgbt3'>Comparar Ingresos internos - Contabilidad</li>
									<li onClick="location.href='cont-comparacion-ingresosClasificar-contabilidad.php'" class='mgbt3'>Comparar Recaudo por clasificar - Contabilidad</li>
									<li onClick="location.href='cont-comparacion-ingresosTransferencias-contabilidad.php'" class='mgbt3'>Comparar Recaudo por transferencias - Contabilidad</li>
									<li onClick="location.href='cont-recibocajasp-cont.php'" class='mgbt3'>Comparar recaudo servicios publicos - Contabilidad</li>
									<li onClick="location.href='serv-comparacion-cont-serv.php'" class='mgbt3'>Comparar recaudo completo servicios publicos nuevo - Contabilidad</li>
									<li onClick="location.href='cont-comparartrasladosbancos.php'" class='mgbt3'>Comparar traslados bancarios - Contabilidad</li>
									<li onClick="location.href='cont-compararrecaudoterceros.php'" class='mgbt3'>Comparar pagos recaudos terceros - Contabilidad</li>
									<li onClick="location.href='cont-compararotrosegresos.php'" class='mgbt3'>Comparar pagos otros egresos - Contabilidad</li>
								</ol>
							</td>
						</tr>						
					</table>
				</div>	

				
				<div class="subpantalla" style="height:93%; width:50%; overflow-x: hidden; float:left;">
					<table class="inicio">
						<tr>
							<td class="titulos" colspan="4">.: Revisión otros documentos</td>
						</tr>

						<tr style="vertical-align: top;text-align: left;" id="listamenus">
							<td width='45%'>
								<ol>
									<li onClick="location.href='cont-comparaegresocxp.php'" class='mgbt3'>Comparar cuentas de egreso y cxp en contabilidad </li>
									<li onClick="location.href='cont-reportenom_liquidacion_egresos.php'" class='mgbt3'>Comparar Nomina - Egresos </li>
									<li onClick="location.href='cont-comparasalidasalmacencontabilidad.php'"class='mgbt3'>Comparar salida por reservas de almacen - contabilidad </li>
									<li onClick="location.href='cont-comparacxp.php'" class='mgbt3'>Comparar Valor CXP contabilidad - CXP Tesoreria</li>
									<li onClick="location.href='cont-comparasalidasdirectacontabilidad.php'" class='mgbt3'>Comparar salida directa de almacen - contabilidad </li>
									<li onClick="location.href='cont-comparaentradacontabilidad.php'" class='mgbt3'>Comparar entrada de almacen - contabilidad </li>
									<li onClick="location.href='cont-comparaordenentrada.php'" class='mgbt3'>Comparar Orden de activaci&oacute;n - contabilidad </li>
								</ol>
							</td>
						</tr>
					</table>
				</div>
            </div>
		</section>
	</body>
</html>