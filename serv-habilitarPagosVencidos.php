<?php
    header("Cache-control: no-cache, no-store, must-revalidate");
    header("Content-Type: text/html;charset=utf8");
    require 'comun.inc';
    require 'funciones.inc';
    session_start();
    if(empty($_SESSION)){
        header("location: index.php");
    }
    $linkbd = conectar_v7();
    $linkbd -> set_charset("utf8");
    cargarcodigopag($_GET['codpag'],$_SESSION["nivel"]);
    date_default_timezone_set("America/Bogota");
?>
<!DOCTYPE >
<html lang=es>
	<head>
		<meta charset="utf-8"/>
		<meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1"/>
		<meta name="viewport" content="user-scalable=no">
		<title>:: IDEAL 10 - Servicios Públicos</title>
		<link href="favicon.ico" rel="shortcut icon"/>
		<link href="css/css2z.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
		<link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
		<link href="css/css4.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
        <link href="css/tabs.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css"/>
        <link href="css/style.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css"/>
        <link rel="stylesheet" href="sweetalert2/dist/sweetalert2.min.css">
        <script type="text/javascript" src="css/programas.js"></script>
        <script src="sweetalert2/dist/sweetalert2.min.js"></script>
		<script src="vue/vue.min.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script src="css/programas.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script src="css/calendario.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
	</head>
	<body>
		<header>
			<table>
				<tr><script>barra_imagenes("serv");</script><?php cuadro_titulos();?></tr>
			</table>
		</header>

		<form name="form2" method="post" action="">
			<section id="myapp" v-cloak>

                <div id="cargando" v-show="loading" class="loading" style="z-index: 10000;">
                    <span>Cargando...</span>
                </div>

				<nav>
					<table>
						<tr><?php menu_desplegable("serv");?></tr>
						<tr>
							<td colspan="3" class="cinta">
								<img src="imagenes/add2.png"  class="mgbt1" title="Nuevo">
								<img src="imagenes/guardad.png" title="Guardar"  class="mgbt1">
								<img src="imagenes/buscad.png" class="mgbt1" title="Buscar">
								<img src="imagenes/nv.png" onClick="mypop=window.open('serv-principal.php','',''); mypop.focus();" class="mgbt" title="Nueva Ventana">
							</td>
						</tr>
					</table>
				</nav>
                
				<article>
                    <table class="inicio ancho">
						<tr>
							<td class="titulos" colspan="6">Habilitar factura vencida</td>
						</tr>

						<tr>
							<td class="textonew01" style="width: 10%;">Número de factura vencida: *</td>
							<td style="width: 15%;">
								<input type="number" v-model="facturaVencida" style="width: 100%;" autocomplete="off">
							</td>
							<td colspan="2" style="padding-bottom:0px">
                                <button type="button" class="btn btn-primary" @click="habilitarFactura">Habilitar</button>
							</td>
						</tr>
					</table>

                    <table class="inicio ancho">
						<tr>
							<td class="titulos" colspan="6">Eliminar deuda e intereses</td>
						</tr>

						<tr>
							<td class="textonew01" style="width: 10%;">Número de factura con deuda: *</td>
							<td style="width: 15%;">
								<input type="number" v-model="facturaDeuda" style="width: 100%;" autocomplete="off">
							</td>
							<td colspan="2" style="padding-bottom:0px">
                                <button type="button" @click="eliminarDeuda" class="btn btn-primary">Quitar deuda</button>
							</td>
						</tr>
					</table>
				</article>
			</section>
		</form>

		<script src="Librerias/vue/vue.min.js"></script>
		<script src="Librerias/vue/axios.min.js"></script>
		<script type="module" src="servicios_publicos/pagosVencidos/serv-habilitarPagosVencidos.js"></script>
        
	</body>
</html>