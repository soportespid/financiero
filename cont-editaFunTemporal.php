<?php

	header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=utf8");
	require 'comun.inc';
	require 'funciones.inc';
	session_start();
	if(empty($_SESSION)){
		header("location: index.php");
	}
	$linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");
	cargarcodigopag($_GET['codpag'],$_SESSION["nivel"]);
	date_default_timezone_set("America/Bogota");
?>
<!DOCTYPE >
<html lang=es>
	<head>
		<meta charset="utf-8"/>
		<meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1"/>
		<meta name="viewport" content="user-scalable=no">
		<title>:: IDEAL 10 - Parametrización</title>
		<link href="favicon.ico" rel="shortcut icon"/>
		<link href="css/css2z.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
		<link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
		<link href="css/css4.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
		<link href="css/style.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css"/>
		<link rel="stylesheet" href="sweetalert2/dist/sweetalert2.min.css">
		<script src="sweetalert2/dist/sweetalert2.min.js"></script>
		<script type="text/javascript" src="css/programas.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<style>
			[v-cloak]{display : none;}
		</style>
	</head>
	<body>
		<section id="myapp" v-cloak>
			<input type="hidden" value = "3" ref="pageType">
			<input type="hidden" value = "2" ref="modulo">
			<input type="hidden" value = "FT" ref="tipo">
			<input type="hidden" value = "cont-editaFunTemporal.php" ref="url">
            <div class="loading-container" v-show="isLoading" >
                <p class="text-loading"data-loading="IDEAL 10">IDEAL 10</p>
            </div>
			<div class="main-container">
				<header>
					<table>
						<tr><script>barra_imagenes("para");</script><?php cuadro_titulos();?></tr>
					</table>
				</header>
				<nav>
					<table>
						<tr><?php menu_desplegable("para");?></tr>
					</table>
					<div class="bg-white group-btn p-1">
						<button type="button" class="btn btn-white btn-primary-hover d-flex justify-between align-items-center" @click="location.href='cont-creaFunTemporal.php'">
							<span>Nuevo</span>
							<svg viewBox="0 -960 960 960" ><path d="M440-440H200v-80h240v-240h80v240h240v80H520v240h-80v-240Z"/></svg>
						</button>
						<button type="button" @click="update()" class="btn btn-white btn-primary-hover d-flex justify-between align-items-center">
							<span>Guardar</span>
							<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 -960 960 960"><path d="M840-680v480q0 33-23.5 56.5T760-120H200q-33 0-56.5-23.5T120-200v-560q0-33 23.5-56.5T200-840h480l160 160Zm-80 34L646-760H200v560h560v-446ZM480-240q50 0 85-35t35-85q0-50-35-85t-85-35q-50 0-85 35t-35 85q0 50 35 85t85 35ZM240-560h360v-160H240v160Zm-40-86v446-560 114Z"></path></svg>
						</button>
						<button type="button" @click="location.href='cont-buscaFunTemporal.php'" class="btn btn-white btn-primary-hover d-flex justify-between align-items-center">
							<span>Buscar</span>
							<svg class="fill-black group-hover:fill-white w-5 h-5" xmlns="http://www.w3.org/2000/svg" viewBox="0 -960 960 960"><path d="M784-120 532-372q-30 24-69 38t-83 14q-109 0-184.5-75.5T120-580q0-109 75.5-184.5T380-840q109 0 184.5 75.5T640-580q0 44-14 83t-38 69l252 252-56 56ZM380-400q75 0 127.5-52.5T560-580q0-75-52.5-127.5T380-760q-75 0-127.5 52.5T200-580q0 75 52.5 127.5T380-400Z"></path></svg>
						</button>
						<button type="button" @click="mypop=window.open('para-principal.php','',''); mypop.focus();" class="btn btn-white btn-primary-hover d-flex justify-between align-items-center">
							<span>Nueva ventana</span>
							<svg class="fill-black group-hover:fill-white w-5 h-5" xmlns="http://www.w3.org/2000/svg" viewBox="0 -960 960 960"><path d="M320-120v-80h80v-80H160q-33 0-56.5-23.5T80-360v-400q0-33 23.5-56.5T160-840h640q33 0 56.5 23.5T880-760v400q0 33-23.5 56.5T800-280H560v80h80v80H320ZM160-360h640v-400H160v400Zm0 0v-400 400Z"></path></svg>
						</button>
						<button type="button" @click="window.location.href='cont-menuFuncionamiento.php'" class="btn btn-success btn-success-hover d-flex justify-between align-items-center">
                            <span>Atras</span>
                            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 -960 960 960"><path d="M760-200v-160q0-50-35-85t-85-35H273l144 144-57 56-240-240 240-240 57 56-144 144h367q83 0 141.5 58.5T840-360v160h-80Z"></path></svg>
                        </button>
					</div>
				</nav>
				<article>		
					<h2 class="titulos m-0">Funcionamiento Temporal</h2>
					<div class="d-flex">
						<div class="form-control w-15">
							<label class="form-label">Código<span class="text-danger fw-bolder">*</span></label>
							<div class="d-flex">
								<button type="button" class="btn btn-primary" @click="nextItem('prev')"><</button>
								<input type="text" style="text-align:center;" v-model="consecutivo" @change="nextItem()" readonly>
								<button type="button" class="btn btn-primary" @click="nextItem('next')">></button>
							</div>
						</div>

						<div class="form-control">
							<label class="form-label">Nombre<span class="text-danger fw-bolder">*</span></label>
							<input type="text" v-model="nombre">
						</div>	

						<div class="form-control w-15">
							<label class="form-label">Tipo</label>
							<input type="text" value="FT" class="text-center" readonly>
						</div>
					</div>

					<div ref="rTabs" class="nav-tabs bg-white p-1">
						<div class="nav-item" :class="tabs == '1' ? 'active' : ''" @click="tabs='1';">Cuentas Credito</div>
						<div class="nav-item" :class="tabs == '2' ? 'active' : ''" @click="tabs='2';">Cuentas Debito</div>
					</div>

					<div>
						<div class="bg-white" v-show="tabs == 1">
							<div class="d-flex">
								<div class="form-control w-15">
									<label class="form-label">Fecha<span class="text-danger fw-bolder">*</span></label>
									<input type="date" v-model="fechaCredito" class="text-center">
								</div>

								<div class="form-control w-25">
									<label class="form-label">Cuenta credito<span class="text-danger fw-bolder">*</span></label>
									<input type="text" class="colordobleclik" v-model="cuentaCredito" @dblclick="modalCredito=true" @change="filter('inputCredito')">
								</div>

								<div class="form-control justify-between">
									<label class="form-label"></label>
									<input type="text" v-model="nombreCredito" readonly>
								</div>

								<div class="form-control justify-between w-25">
									<label for=""></label>
									<button type="button" class="btn btn-primary" @click="addArrCredito()">Agregar</button>
								</div>
							</div>

							<div class="table-responsive">
								<table class="table table-hover fw-normal">
									<thead>
										<tr class="text-center">
											<th class="w-15">Fecha</th>
											<th class="w-25">Cuenta</th>
											<th>Nombre</th>
											<th class="w-15">Opción</th>
										</tr>
									</thead>
									<tbody>
										<tr v-for="(credito, index) in arrCredito" :key="index">
											<td class="text-center">{{ formatFecha(credito.fecha) }}</td>
											<td class="text-center">{{ credito.cuenta }}</td>
											<td>{{ credito.nombre }}</td>
											<td>
												<div class="d-flex justify-center">
													<button type="button" @click="delCredito(index, credito.cuenta)" class="btn btn-danger">x</button>
												</div>
											</td>
										</tr>
									</tbody>
								</table>
							</div>
						</div>

						<div class="bg-white" v-show="tabs == 2">
							<div class="d-flex">
								<div class="form-control w-15">
									<label class="form-label">Fecha<span class="text-danger fw-bolder">*</span></label>
									<input type="date" class="text-center" v-model="fechaDebito">
								</div>

								<div class="form-control w-50">
									<label class="form-label">Destino compra<span class="text-danger fw-bolder">*</span></label>
									<select v-model="codDestino" @change="get_cuentas_debito()">
										<option value="">Seleccione</option>
										<option v-for="destino in destinosCompras" :value="destino.codigo">{{ destino.codigo }} - {{ destino.nombre }}</option>
									</select>
								</div>

								<div class="form-control w-25">
									<label class="form-label">Cuenta debito<span class="text-danger fw-bolder">*</span></label>
									<input type="text" class="colordobleclik" v-model="cuentaDebito" @dblclick="modalDebito=true" @change="filter('inputDebito')">
								</div>

								<div class="form-control justify-between">
									<label class="form-label"></label>
									<input type="text" v-model="nombreDebito">
								</div>
							</div>

							<div class="d-flex">
								<div class="form-control w-50">
									<label class="form-label">Centro costo<span class="text-danger fw-bolder">*</span></label>
									<select v-model="cc">
										<option value="">Seleccione</option>
										<option v-for="cc in centroCostos" :value="cc.codigo">{{ cc.codigo }} - {{ cc.nombre }}</option>
									</select>
								</div>

								<div class="form-control">
									<label class="form-label">Cuenta asociada<span class="text-danger fw-bolder">*</span></label>
									<select v-model="cuentaAsociada">
										<option value="">Seleccione</option>
										<option v-for="credito in arrCredito" :value="credito">{{ credito.cuenta }} - {{ credito.nombre }}</option>
									</select>
								</div>

								<div class="form-control justify-between w-25">
									<label for=""></label>
									<button type="button" class="btn btn-primary" @click="addArrDebito()">Agregar</button>
								</div>
							</div>
							
							<div class="table-responsive">
								<table class="table table-hover fw-normal">
									<thead>
										<tr class="text-center">
											<th>Fecha</th>
											<th>Cuenta</th>
											<th>Nombre</th>
											<th>Centro costo</th>
											<th>Destino compra</th>
											<th>Cuenta asociada</th>
											<th>Nombre cuenta asociada</th>
											<th>Opción</th>
										</tr>
									</thead>
									<tbody>
										<tr v-for="(debito, index) in arrDebito" :key="index">
											<td class="text-center">{{ formatFecha(debito.fecha) }}</td>
											<td class="text-center">{{ debito.cuenta }}</td>
											<td>{{ debito.nombre }}</td>
											<td class="text-center">{{ debito.centroCosto }}</td>
											<td class="text-center">{{ debito.destinoCompra }}</td>
											<td class="text-center">{{ debito.cuentaAsociada }}</td>
											<td>{{ debito.nombreAsociada }}</td>
											<td>
												<div class="d-flex justify-center">
													<button type="button" @click="delDebito(index)" class="btn btn-danger">x</button>
												</div>
											</td>
										</tr>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</article>

				<!-- MODALES -->
				<div v-show="modalCredito" class="modal">
                    <div class="modal-dialog modal-xl" >
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title">Cuentas contables credito</h5>
                                <button type="button" @click="modalCredito=false;" class="btn btn-close"><div></div><div></div></button>
                            </div>
                            <div class="modal-body">
                                <div class="d-flex flex-column">
                                    <div class="form-control m-0 mb-3">
                                        <input type="search" v-model="txtCredito" @keyup="filter('modalCredito')" placeholder="Busca por cuenta o nombre">
                                    </div>
                                </div>
                                <div class="overflow-auto max-vh-50 overflow-x-hidden" >
                                    <table class="table table-hover fw-normal">
                                        <thead>
                                            <tr>
                                                <th class="text-center">Cuenta</th>
                                                <th>Nombre</th>
												<th class="text-center">Tipo</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr v-for="(credito,index) in arrCuentasCredito" :key="index" @click="selectItem('modalCredito', credito)">
                                                <td>{{credito.cuenta}}</td>
                                                <td>{{credito.nombre}}</td>
												<td class="text-center">{{credito.tipo}}</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
				
				<div v-show="modalDebito" class="modal">
                    <div class="modal-dialog modal-xl" >
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title">Cuentas contables debito</h5>
                                <button type="button" @click="modalDebito=false;" class="btn btn-close"><div></div><div></div></button>
                            </div>
                            <div class="modal-body">
                                <div class="d-flex flex-column">
                                    <div class="form-control m-0 mb-3">
                                        <input type="search" v-model="txtDebito" @keyup="filter('modalDebito')" placeholder="Busca por cuenta o nombre">
                                    </div>
                                </div>
                                <div class="overflow-auto max-vh-50 overflow-x-hidden" >
                                    <table class="table table-hover fw-normal">
                                        <thead>
                                            <tr>
                                                <th class="text-center">Cuenta</th>
                                                <th>Nombre</th>
												<th class="text-center">Tipo</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr v-for="(debito,index) in arrCuentasDebito" :key="index" @click="selectItem('modalDebito', debito)">
                                                <td>{{debito.cuenta}}</td>
                                                <td>{{debito.nombre}}</td>
												<td class="text-center">{{debito.tipo}}</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
			</div>
		</section>
		<script src="Librerias/vue/vue.min.js"></script>
		<script src="Librerias/vue/axios.min.js"></script>
		<script type="module" src="contabilidad_vue/parametros_contables/js/functions_presupuesto.js?"<?= date('d_m_Y_h_i_s');?>></script>
	</body>
</html>