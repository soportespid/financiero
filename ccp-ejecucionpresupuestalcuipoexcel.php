<?php
	header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=utf8");
	require_once 'PHPExcel/Classes/PHPExcel.php';
	require "comun.inc";
	require "funciones.inc";
	sesion();
	$linkbd=conectar_v7();
	$linkbd -> set_charset("utf8");
	date_default_timezone_set("America/Bogota");
	
	$objPHPExcel = new PHPExcel();
	//----Propiedades----
	$objPHPExcel->getProperties()
		->setCreator("SPID")
		->setLastModifiedBy("SPID")
		->setTitle("Liquidacion de Nomina")
		->setSubject("Nomina")
		->setDescription("Nomina")
		->setKeywords("Nomina")
		->setCategory("Gestion Humana");
	//define('FORMAT_CURRENCY_PLN_1', '$#,0.00_-');
	define('FORMAT_CURRENCY_PLN_1', '$ #,0_-');
	//----Cuerpo de Documento----
	$objPHPExcel->getActiveSheet()->mergeCells('A1:V1');
	$objPHPExcel->setActiveSheetIndex(0)->setCellValue('A1', 'EJECUCION PRESUPUESTAL CUIPO PERIODO: '.$_POST['fechai'].' Al '.$_POST['fechaf']);
	$objFont=$objPHPExcel->getActiveSheet()->getStyle('A1')->getFont();
	$objFont->setName('Courier New');
	$objFont->setSize(15);
	$objFont->setBold(true);
	$objFont->setUnderline(PHPExcel_Style_Font::UNDERLINE_SINGLE);
	$objFont->getColor()->setARGB( PHPExcel_Style_Color::COLOR_BLACK);
	$objAlign=$objPHPExcel->getActiveSheet()->getStyle('A1')->getAlignment(); 
	$objAlign->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
	$objAlign->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
	$objPHPExcel-> getActiveSheet ()
		-> getStyle ("A2:V2")
		-> getFill ()
		-> setFillType (PHPExcel_Style_Fill :: FILL_SOLID)
		-> getStartColor ()
		-> setRGB ('A6E5F3');
	$objPHPExcel-> getActiveSheet ()
		-> getStyle ("A1")	
		-> getFill ()
		-> setFillType (PHPExcel_Style_Fill :: FILL_SOLID)
		-> getStartColor ()
		-> setRGB ('A6E5F3');
	$borders = array(
		'borders' => array(
			'allborders' => array(
				'style' => PHPExcel_Style_Border::BORDER_THIN,
				'color' => array('argb' => 'FF000000'),
			)
		),
	);
	$objPHPExcel->getActiveSheet()->getStyle('A2:V2')->applyFromArray($borders);
	$objPHPExcel->setActiveSheetIndex(0)
			->setCellValue('A2', 'CUENTA')
			->setCellValue('B2', utf8_encode('DESCRIPCI�N'))
			->setCellValue('C2', 'VIGENCIA DEL GASTO')
			->setCellValue('D2', utf8_encode('SECCI�N PRESUPUESTAL'))
			->setCellValue('E2', 'PROGRAMATICO MGA')
			->setCellValue('F2', 'CPC')
			->setCellValue('G2', utf8_encode('FUENTES DE FINANCIACI�N'))
			->setCellValue('H2', 'BPIN')
			->setCellValue('I2', 'SITUACION DE FONDOS')
			->setCellValue('J2', 'POLITICA PUBLICA')
			->setCellValue('K2', 'TERCEROS CHIP')
			->setCellValue('L2', 'INICIAL')
			->setCellValue('M2', utf8_encode('ADICI�N'))
			->setCellValue('N2', utf8_encode('REDUCCI�N'))
			->setCellValue('O2', 'CREDITO')
			->setCellValue('P2', 'CONTRACREDITO')
			->setCellValue('Q2', 'DEFINITIVO')
			->setCellValue('R2', 'DISPONIBILIDAD')
			->setCellValue('S2', 'COMPROMISOS')
			->setCellValue('T2', 'OBLIGACIONES')
			->setCellValue('U2', 'PAGOS')
			->setCellValue('V2', 'SALDO');
	$i=3;
	$nombretablatemporal = $_SESSION['tablatemporal']."EP";
	
	if($_POST['secpresupuestal']=='0'){$sqlredglobal ="SELECT codigo, base, nombre FROM redglobal ORDER BY id ASC";}
	else {$sqlredglobal ="SELECT codigo, base, nombre FROM redglobal WHERE codigo = '".$_POST['secpresupuestal']."'";}
	$resredglobal = mysqli_query($linkbd,$sqlredglobal);
	while ($rowredglobal = mysqli_fetch_row($resredglobal))
	{
		$linkmulti = conectar_Multi($rowredglobal[1]);
		$linkmulti -> set_charset("utf8");
		$sqlmax = "SELECT MAX(version) FROM cuentasccpet";
		$resmax = mysqli_query($linkmulti, $sqlmax);
		$rowmax = mysqli_fetch_row($resmax);
		$maxVersion = $rowmax[0];
		//$sqlty="SELECT DISTINCT vigenciagastos FROM $nombretablatemporal WHERE codigocuenta LIKE '$rowcta[0]%' AND seccpresupuestal = '$rowredglobal[0]'";
		//$resty = mysqli_query($linkbd,$sqlty);
		//while ($rowty = mysqli_fetch_row($resty))
		$rowty[0]="-";
		{
			$sqlcta ="SELECT codigo, tipo, nombre FROM cuentasccpet WHERE version='$maxVersion' AND municipio=1 ORDER BY id ASC";
			$rescta = mysqli_query($linkmulti,$sqlcta);
			while ($rowcta = mysqli_fetch_row($rescta))
			{
				if($rowcta[1]=='A')
				{
					$sqlver="
					SELECT SUM(apropiacionini), SUM(adiciones), SUM(reducciones), SUM(trascredito), SUM(trascontracredito), SUM(apropiaciondef), SUM(disponibilidad), SUM(compromisos), SUM(obligaciones), SUM(pagos)
					FROM $nombretablatemporal
					WHERE codigocuenta LIKE '$rowcta[0]%' AND seccpresupuestal = '$rowredglobal[0]'";
					$resver = mysqli_query($linkbd,$sqlver);
					$rowver = mysqli_fetch_row($resver);
					if(($rowver[0] > 0) || ($rowver[1] > 0) || ($rowver[2] > 0) || ($rowver[3] > 0) || ($rowver[4] > 0) || ($rowver[5] > 0) || ($rowver[6] > 0) || ($rowver[7] > 0) || ($rowver[8] > 0) || ($rowver[9] > 0))
					{
						$saldopresu = $rowver[5] - $rowver[6];
						$objPHPExcel->setActiveSheetIndex(0)
						->setCellValueExplicit ("A$i", $rowcta[0], PHPExcel_Cell_DataType :: TYPE_STRING)
						->setCellValueExplicit ("B$i", $rowcta[2], PHPExcel_Cell_DataType :: TYPE_STRING)
						->setCellValueExplicit ("C$i", $rowty[0], PHPExcel_Cell_DataType :: TYPE_STRING)
						->setCellValueExplicit ("D$i", $rowredglobal[0], PHPExcel_Cell_DataType :: TYPE_STRING)
						->setCellValueExplicit ("E$i","-", PHPExcel_Cell_DataType :: TYPE_STRING)
						->setCellValueExplicit ("F$i","-", PHPExcel_Cell_DataType :: TYPE_STRING)
						->setCellValueExplicit ("G$i","-", PHPExcel_Cell_DataType :: TYPE_STRING)
						->setCellValueExplicit ("H$i", "-", PHPExcel_Cell_DataType :: TYPE_STRING)
						->setCellValueExplicit ("I$i", "-", PHPExcel_Cell_DataType :: TYPE_STRING)
						->setCellValueExplicit ("J$i", "-", PHPExcel_Cell_DataType :: TYPE_STRING)
						->setCellValueExplicit ("K$i", "-", PHPExcel_Cell_DataType :: TYPE_STRING)
						->setCellValueExplicit ("L$i", $rowver[0], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
						->setCellValueExplicit ("M$i", $rowver[1], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
						->setCellValueExplicit ("N$i", $rowver[2], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
						->setCellValueExplicit ("O$i", $rowver[3], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
						->setCellValueExplicit ("P$i", $rowver[4], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
						->setCellValueExplicit ("Q$i", $rowver[5], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
						->setCellValueExplicit ("R$i", $rowver[6], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
						->setCellValueExplicit ("S$i", $rowver[7], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
						->setCellValueExplicit ("T$i", $rowver[8], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
						->setCellValueExplicit ("U$i", $rowver[9], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
						->setCellValueExplicit ("V$i", $saldopresu, PHPExcel_Cell_DataType :: TYPE_NUMERIC);
						$objPHPExcel->getActiveSheet()->getStyle("L$i:V$i")->getNumberFormat()->setFormatCode(FORMAT_CURRENCY_PLN_1);
						$objPHPExcel->getActiveSheet()->getStyle("A$i:V$i")->applyFromArray($borders);
						$objPHPExcel->getActiveSheet()->getStyle("A$i:V$i")->getFont()->setBold(true);
						$objPHPExcel->getActiveSheet()->getStyle ("A$i:V$i")->getFill()->setFillType(PHPExcel_Style_Fill :: FILL_SOLID)-> getStartColor()->setRGB ('B4BEC8');
						if($saldopresu < 0)
						{
							$objPHPExcel->getActiveSheet()->getStyle ("Q$i:R$i")->getFill()->setFillType(PHPExcel_Style_Fill :: FILL_SOLID)-> getStartColor()->setRGB ('FA6666');
							$objPHPExcel->getActiveSheet()->getStyle ("V$i:V$i")->getFill()->setFillType(PHPExcel_Style_Fill :: FILL_SOLID)-> getStartColor()->setRGB ('FA6666');
						}
						$i++;
					}
				}
				else
				{
					$sqlver="
					SELECT id, codigocuenta, nombrecuenta, seccpresupuestal, vigenciagastos, programatico, bpin, cpc, fuente, situacionfondo, politicapublica, tercerochip, SUM(compromisos), SUM(obligaciones), SUM(pagos), fuente2, cpc2, programatico2, SUM(disponibilidad), SUM(apropiacionini), SUM(apropiaciondef), SUM(adiciones), SUM(reducciones), SUM(trascredito), SUM(trascontracredito)
					FROM $nombretablatemporal
					WHERE codigocuenta = '$rowcta[0]' AND seccpresupuestal = '$rowredglobal[0]'
					GROUP BY codigocuenta, seccpresupuestal, programatico2, cpc2, fuente2, bpin, vigenciagastos
					ORDER BY seccpresupuestal, vigenciagastos, id ASC";
					$resver = mysqli_query($linkbd,$sqlver);
					while ($rowver = mysqli_fetch_row($resver))
					{
						$saldopresu = $rowver[20] - $rowver[18];
						$objPHPExcel->setActiveSheetIndex(0)
						->setCellValueExplicit ("A$i", $rowver[1], PHPExcel_Cell_DataType :: TYPE_STRING)
						->setCellValueExplicit ("B$i", $rowver[2], PHPExcel_Cell_DataType :: TYPE_STRING)
						->setCellValueExplicit ("C$i", $rowver[4], PHPExcel_Cell_DataType :: TYPE_STRING)
						->setCellValueExplicit ("D$i", $rowver[3], PHPExcel_Cell_DataType :: TYPE_STRING)
						->setCellValueExplicit ("E$i","$rowver[17] ($rowver[5])", PHPExcel_Cell_DataType :: TYPE_STRING)
						->setCellValueExplicit ("F$i","$rowver[16] ($rowver[7])", PHPExcel_Cell_DataType :: TYPE_STRING)
						->setCellValueExplicit ("G$i","$rowver[15] ($rowver[8])", PHPExcel_Cell_DataType :: TYPE_STRING)
						->setCellValueExplicit ("H$i", $rowver[6], PHPExcel_Cell_DataType :: TYPE_STRING)
						->setCellValueExplicit ("I$i", $rowver[9], PHPExcel_Cell_DataType :: TYPE_STRING)
						->setCellValueExplicit ("J$i", $rowver[10], PHPExcel_Cell_DataType :: TYPE_STRING)
						->setCellValueExplicit ("K$i", $rowver[11], PHPExcel_Cell_DataType :: TYPE_STRING)
						->setCellValueExplicit ("L$i", $rowver[19], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
						->setCellValueExplicit ("M$i", $rowver[21], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
						->setCellValueExplicit ("N$i", $rowver[22], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
						->setCellValueExplicit ("O$i", $rowver[23], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
						->setCellValueExplicit ("P$i", $rowver[24], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
						->setCellValueExplicit ("Q$i", $rowver[20], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
						->setCellValueExplicit ("R$i", $rowver[18], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
						->setCellValueExplicit ("S$i", $rowver[12], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
						->setCellValueExplicit ("T$i", $rowver[13], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
						->setCellValueExplicit ("U$i", $rowver[14], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
						->setCellValueExplicit ("V$i", $saldopresu, PHPExcel_Cell_DataType :: TYPE_NUMERIC);
						$objPHPExcel->getActiveSheet()->getStyle("A$i:V$i")->applyFromArray($borders);
						$objPHPExcel->getActiveSheet()->getStyle("L$i:V$i")->getNumberFormat()->setFormatCode(FORMAT_CURRENCY_PLN_1);
						if($saldopresu < 0)
						{
							$objPHPExcel->getActiveSheet()->getStyle ("Q$i:R$i")->getFill()->setFillType(PHPExcel_Style_Fill :: FILL_SOLID)-> getStartColor()->setRGB ('FA6666');
							$objPHPExcel->getActiveSheet()->getStyle ("V$i:V$i")->getFill()->setFillType(PHPExcel_Style_Fill :: FILL_SOLID)-> getStartColor()->setRGB ('FA6666');
						}
						$i++;
					}
				}
			}
		}
	}
	//----Propiedades de la hoja 1
	$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->getColumnDimension('K')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->getColumnDimension('L')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->getColumnDimension('M')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->getColumnDimension('N')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->getColumnDimension('O')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->getColumnDimension('P')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->getColumnDimension('Q')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->getColumnDimension('R')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->getColumnDimension('S')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->getColumnDimension('T')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->getColumnDimension('U')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->getColumnDimension('v')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->setTitle('General');
	$objPHPExcel->setActiveSheetIndex(0);

//----Guardar documento----
header('Content-Type: application/vnd.ms-excel');
header('Content-Disposition: attachment;filename="Ejecucionpresupuestal.xlsx"');
header('Cache-Control: max-age=0');
 
$objWriter=PHPExcel_IOFactory::createWriter($objPHPExcel,'Excel2007');
$objWriter->save('php://output');
exit;

?>