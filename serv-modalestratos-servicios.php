<?php
	header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=utf8");
	require 'comun.inc';
	require 'funciones.inc';
	$linkbd = conectar_v7();
    $linkbd -> set_charset("utf8"); 
	date_default_timezone_set("America/Bogota");
?>
<!DOCTYPE >
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="es">
	<head>
		<meta http-equiv="Content-Type" content="text/html" charset="utf8"/>
		<meta http-equiv="X-UA-Compatible" content="IE=9"/>
		<title>:: Estratos</title>
		<link href="css/css2.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<link href="css/css4.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
        <link href="css/cssSP.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<script type="text/javascript" src="css/programas.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script>
			function agregar(codigo, nombre, posicion)
			{
				parent.document.getElementsByName('estrato2[]').item(posicion).value = nombre;
				parent.document.getElementsByName('id_estrato2[]').item(posicion).value = codigo;
				parent.despliegamodal("hidden");
			}
		</script> 
		<?php titlepag();?>
	</head>
	<body>
		<?php
			$_POST['posicion'] = $_GET['pos'];
		?>
		
		<form name="form2" method="post">
			<table class="inicio ancho" style="width:99.5%">
				<tr>
					<td class="titulos" colspan="3">Clases de uso - Servicios Publicos</td>
					<td class="cerrar" style="width:7%" onClick="parent.despliegamodal('hidden');">Cerrar</td>
				</tr>
			</table>

			<div class="subpantalla" style="height:82%; width:99.2%; overflow-x:hidden;">
				<table class='inicio' align='center' width='99%'>			
					<tr class='titulos2' style='text-align:center;'>
						<td style="width: 10%;">Código</td>
						<td>Clase de uso</td>
						<td style="width: 5%">Estado</td>
					</tr>

					<?php
						$sqlEstrato = "SELECT * FROM srv_clases WHERE estado = 'S'";
						$resEstrato = mysqli_query($linkbd,$sqlEstrato);
						$encontrados = mysqli_num_rows($resEstrato);

						$iter  = 'saludo1a';
						$iter2 = 'saludo2';
						$conta = 1;

						while ($rowEstrato = mysqli_fetch_row($resEstrato))
						{
					?>
							<tr class='<?php echo $iter ?>' style='text-align:center; text-transform:uppercase;' onclick="agregar('<?php echo $rowEstrato[0]; ?>', '<?php echo $rowEstrato[1]; ?>', '<?php echo $_POST['posicion']; ?>')">
								<td><?php echo $rowEstrato[0] ?></td>
								<td><?php echo $rowEstrato[1] ?></td>
								<td><?php echo $rowEstrato[2] ?></td>
							</tr>
					<?php
							$aux=$iter;
							$iter=$iter2;
							$iter2=$aux;
							$conta++;
						}

						if ($encontrados == 0)
						{
					?>
							<table class='inicio'>
								<tr>
									<td class='saludo1' style='text-align:center;'>
										<img src='imagenes\alert.png' style='width:25px; height: 30px;'> No se han encontrado registros <img src='imagenes\alert.png' style='width:25px; height: 30px;'>
									</td>
								</tr>
							</table>
					<?php
						}
					?>
				</table>
			</div>
		</form>
	</body>
</html>
