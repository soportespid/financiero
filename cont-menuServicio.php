<?php
	require 'comun.inc';
	require 'funciones.inc'; 
	session_start();
	cargarcodigopag(@$_GET['codpag'], @$_SESSION['nivel']);
	header('Cache-control: private'); 
	date_default_timezone_set('America/Bogota')
?>
<!DOCTYPE >
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="es">
	<head>
	 	<meta http-equiv="Content-Type" content="text/html" charset="iso-8859-1"/>
        <meta http-equiv="X-UA-Compatible" content="IE=9"/>
		<title>:: IDEAL 10 - Parametrización</title>
        <link href="css/css2.css" rel="stylesheet" type="text/css" />
		<link href="css/css3.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="css/programas.js"></script>
		<script type="text/javascript" src="css/calendario.js"></script>
        
		<?php titlepag();?>
    </head>
    <body>
        <IFRAME src="alertas.php" name="alertas" id="alertas" style="display:none"></IFRAME>
        <span id="todastablas2"></span>
        <table>
            <tr><script>barra_imagenes("para");</script><?php cuadro_titulos();?></tr>	 
            <tr><?php menu_desplegable("para");?></tr>
       		<tr>
          		<td colspan="3" class="cinta">
                    <a class="mgbt"><img src="imagenes/add2.png"/></a>
                    <a class="mgbt"><img src="imagenes/guardad.png" title="Guardar" /></a>
                    <a class="mgbt"><img src="imagenes/buscad.png"/></a>
                    <a onClick="mypop=window.open('plan-agenda.php','','');mypop.focus()" class="mgbt"><img src="imagenes/agenda1.png" title="Agenda"/></a>
                    <a class="mgbt" onClick="mypop=window.open('para-principal.php','','');mypop.focus();"><img src="imagenes/nv.png" title="Nueva ventana"></a>
                    <a onClick="mypop=window.open('<?php echo $url2; ?>','','');mypop.focus();" class="tooltip bottom mgbt"><img src="imagenes/duplicar_pantalla.png" title="Duplicar pesta&ntilde;a">
                    <a href="cont-menupresupuesto.php" class="mgbt"><img src="imagenes/iratras.png" title="Atr&aacute;s"></a>
                </td>
       	 	</tr>
        </table>
        <form name="form2" method="post" action="">
			<table class="inicio">
      			<tr>
        			<td class="titulos" colspan="2">.: Gesti&oacute;n Servicio de la Deuda P&uacute;blica </td>
        			<td class="cerrar" style="width:7%;" ><a href="para-principal.php">&nbsp;Cerrar</a></td>
      			</tr>
      			
				<tr>
                    <td>
                        <ol id="lista2">
                            <li onClick="location.href='cont-servicios-buscaServicios.php'" style="cursor:pointer;">2.2.1 - Servicio de la deuda p&uacute;blica externa</li>
                            <li onClick="location.href='cont-servicios-buscaCorrientes.php'" style="cursor:pointer;">2.2.2 - Servicio de la deuda p&uacute;blica interna</li>
                        </ol>
                    </td>
				</tr>							
    		</table>
		</form>
    </body>
</html>
