<?php
	require_once '../../../comun.inc';
    require '../../../funciones.inc';
    require '../../../conversor.php';
    session_start();

	$linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");

    $out = array('error' => false);

    $action = "show";

    if(isset($_GET['action'])){
        $action = $_GET['action'];
    }

    if ($action == 'datosIniciales') {
          
        $consecutivo = $_GET['consecutivo'];
        $listaArticulos = [];

        $sqlCab = "SELECT consecutivo, date_format(fecha, '%d/%m/%Y'), descripcion, estado, valor_total FROM alm_salida_activos_cab WHERE consecutivo = '$consecutivo'";
        $resCab = mysqli_query($linkbd, $sqlCab);
        $rowCab = mysqli_fetch_row($resCab);

        $conse = $rowCab[0];
        $fecha = $rowCab[1];
        $descripcion = $rowCab[2];
        $total = $rowCab[4];

        switch ($rowCab[3]) {
            case 'S':
                $estado = "Activo";
            break;
            
            case 'R':
                $estado = "Reversado";
            break;
        }

        $sqlDet = "SELECT bodega, cc, cod_articulo, cantidad, valor_unitario, valor_total FROM alm_salida_activos_det WHERE consecutivo = '$consecutivo'";
        $resDet = mysqli_query($linkbd, $sqlDet);
        while ($rowDet = mysqli_fetch_row($resDet)) {
            
            $datosTemp = [];

            $sqlBodegas = "SELECT nombre FROM almbodegas WHERE id_cc = '$rowDet[0]'";
            $resBodegas = mysqli_query($linkbd, $sqlBodegas);   
            $rowBodegas = mysqli_fetch_row($resBodegas);

            $bodega = $rowDet[0] . " - " . $rowBodegas[0];

            $sqlcc = "SELECT nombre FROM centrocosto WHERE id_cc = '$rowDet[1]'";
            $rescc = mysqli_query($linkbd, $sqlcc);
            $rowcc = mysqli_fetch_row($rescc);

            $cc = $rowDet[1] . " - " . $rowcc[0];

            $grupo = substr($rowDet[2], 0, -5);
            $codigo = substr($rowDet[2], 4);

            $sqlArticulo = "SELECT nombre FROM almarticulos WHERE codigo = '$codigo' AND grupoinven = '$grupo'";
            $resArticulo = mysqli_query($linkbd, $sqlArticulo);
            $rowArticulo = mysqli_fetch_row($resArticulo);

            $articulo = $rowDet[2];
            $nameArticulo = $rowArticulo[0];

            $sqlArticulosDet = "SELECT unidad FROM almarticulos_det WHERE articulo = '$rowDet[2]'";
            $resArticulosDet = mysqli_query($linkbd, $sqlArticulosDet);
            $rowArticulosDet = mysqli_fetch_row($resArticulosDet);

            $unidad = $rowArticulosDet[0];
            $cantidad = $rowDet[3];
            $valorUnitario = $rowDet[4];
            $valorTotal = $rowDet[5];

            $datosTemp[] = $bodega;
            $datosTemp[] = $cc;
            $datosTemp[] = $articulo;
            $datosTemp[] = $nameArticulo;
            $datosTemp[] = $unidad;
            $datosTemp[] = $cantidad;
            $datosTemp[] = $valorUnitario;
            $datosTemp[] = $valorTotal;
            array_push($listaArticulos, $datosTemp);
        }

        $out['conse'] = $conse;
        $out['fecha'] = $fecha;
        $out['descripcion'] = $descripcion;
        $out['estado'] = $estado;
        $out['total'] = $total;
        $out['listaArticulos'] = $listaArticulos;
    }

    header("Content-type: application/json");
    echo json_encode($out);
    die();