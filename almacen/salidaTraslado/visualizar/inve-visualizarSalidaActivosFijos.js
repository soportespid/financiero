const URL = 'almacen/salidaTraslado/visualizar/inve-visualizarSalidaActivosFijos.php';

var app = new Vue({
    el: '#myapp',
	data:{

        error: '',
        loading: false,
        consecutivo: '',
        fecha: '',
        descripcion: '',
        listaArticulos: [],
        estado: '',
        totalValores: 0,
    },

    mounted: function(){

        this.consultaInformacion();
    },

    methods: 
    {
        //Desglosar array
        toFormData: function(obj){
            var form_data = new FormData();
            for(var key in obj){
                form_data.append(key, obj[key]);
            }
            return form_data;
        },

        formatonumero: function(valor){
            return new Intl.NumberFormat("en-US", {style: "currency", currency: "USD"}).format(valor);
        },

        traeParametros: function(name){
            //Captura parametros
            name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
            var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
            results = regex.exec(location.search);
            return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
        },


        consultaInformacion: async function() {

            const consecutivo = this.traeParametros("consecutivo");

            if (consecutivo != "") {

                await axios.post(URL+'?action=datosIniciales&consecutivo='+consecutivo)
                .then((response) => {
                    
                    this.consecutivo = response.data.conse;
                    this.fecha = response.data.fecha;
                    this.descripcion = response.data.descripcion;
                    this.totalValores = response.data.total;
                    this.estado = response.data.estado;
                    this.listaArticulos = response.data.listaArticulos;
                });
            }
        },
    },
});