const URL = 'almacen/salidaTraslado/buscar/inve-buscarSalidaActivosFijos.php';

var app = new Vue({
    el: '#myapp',
	data:{

        error: '',
        loading: false,
        
        consecutivo: '',
        salidas: [],
    },

    mounted: function(){


    },

    methods: 
    {
        //Desglosar array
        toFormData: function(obj){
            var form_data = new FormData();
            for(var key in obj){
                form_data.append(key, obj[key]);
            }
            return form_data;
        },

        formatonumero: function(valor){
            return new Intl.NumberFormat("en-US", {style: "currency", currency: "USD"}).format(valor);
        },

        traeParametros: function(name){
            //Captura parametros
            name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
            var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
            results = regex.exec(location.search);
            return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
        },

        buscarDatos: async function() {
            
            const fechaIni = document.getElementById('fechaIni').value;
            const fechaFin = document.getElementById('fechaFin').value;
            
            if (this.consecutivo != "" || (fechaFin != "" && fechaIni != "")) {

                var formData = new FormData();

                formData.append("consecutivo", this.consecutivo);
                formData.append("fechaIni", fechaIni);
                formData.append("fechaFin", fechaFin);

                await axios.post(URL+'?action=datosIniciales', formData)
                .then((response) => {
                    
                    this.salidas = response.data.salidas;
                });
            }
            else {
                
                Swal.fire("Error", "Debes ingresar fechas o consecutivo", "error");
            }
        },

        visualizar: function(salida) {

            const x = "inve-visualizarSalidaActivosFijos?consecutivo="+salida[0];
            window.open(x, '_blank');
            window.focus();
        },
    },
});