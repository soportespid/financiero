<?php

	require_once '../../../comun.inc';
    require '../../../funciones.inc';
    require '../../../conversor.php';
    session_start();

	$linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");

    $out = array('error' => false);

    $action = "show";

    if(isset($_GET['action'])){
        $action = $_GET['action'];
    }

    if ($action == 'datosIniciales') {
          
        $datos = [];

        if ($_POST['fechaIni'] != "" && $_POST['fechaFin'] != "") {
            preg_match("/([0-9]{2})\/([0-9]{2})\/([0-9]{4})/", $_POST["fechaIni"], $f);
            $fechaIni = $f[3]."-".$f[2]."-".$f[1];

            preg_match("/([0-9]{2})\/([0-9]{2})\/([0-9]{4})/", $_POST["fechaFin"], $f);
            $fechaFin = $f[3]."-".$f[2]."-".$f[1];

            $filtro01 = "AND fecha BETWEEN '$fechaIni' AND '$fechaFin' ";
        }
        if ($_POST['consecutivo'] != "") {
            
            $consecutivo = $_POST['consecutivo'];

            $filtro02 = "AND consec = '$consecutivo' ";
        }

        $sql = "SELECT consec, date_format(fecha, '%d-%m-%Y'), nombre, bodega1, usuario, valortotal, estado FROM almginventario WHERE estado = 'S' AND tipomov = '2' AND tiporeg = '03' $filtro01 $filtro02";
        $result = mysqli_query($linkbd, $sql);
        while ($row = mysqli_fetch_row($result)) {

            $sqlUsuarios = "SELECT usu_usu FROM usuarios WHERE cc_usu = '$row[4]'";
            $rowUsuarios = mysqli_fetch_row(mysqli_query($linkbd, $sqlUsuarios));
            
            $sqlBodegas = "SELECT nombre FROM almbodegas WHERE id_cc = '$row[3]'";
            $rowBodegas = mysqli_fetch_row(mysqli_query($linkbd, $sqlBodegas));

            switch ($row[6]) {
                case 'S':
                    $estado = 'Activo';
                break;

                case 'N':
                    $estado = 'Anulado';
                break;

                case 'R':
                    $estado = 'Reversado';
                break;
                
                default:
                    $estado = '';
                break;
            }

            $datosTemp = [];

            $datosTemp[] = $row[0];
            $datosTemp[] = $row[1];
            $datosTemp[] = $row[2];
            $datosTemp[] = $rowBodegas[0];
            $datosTemp[] = $rowUsuarios[0];
            $datosTemp[] = $row[5];
            $datosTemp[] = $estado;
            array_push($datos, $datosTemp);
        }
        
        $out['salidas'] = $datos;
    }

    header("Content-type: application/json");
    echo json_encode($out);
    die();