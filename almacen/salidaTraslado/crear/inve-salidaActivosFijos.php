<?php

	require_once '../../../comun.inc';
    require '../../../funciones.inc';
    header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=utf8");
    session_start();

	$linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");

    $out = array('error' => false);

    $action = "show";

    if(isset($_GET['action'])){
        $action = $_GET['action'];
    }

    if ($action == "datosIniciales") {

        $bodegas = [];
        $cc = [];

        $sqlBodegas = "SELECT id_cc, nombre FROM almbodegas WHERE estado = 'S'";
        $resBodegas = mysqli_query($linkbd, $sqlBodegas);
        while ($rowBodegas = mysqli_fetch_row($resBodegas)) {
            
            array_push($bodegas, $rowBodegas);
        }

        $sqlCentroCosto = "SELECT id_cc, nombre FROM centrocosto WHERE estado='S' AND entidad='S' ORDER BY id_cc";
        $resCentroCosto = mysqli_query($linkbd, $sqlCentroCosto);
        while ($rowCentroCosto = mysqli_fetch_row($resCentroCosto)) {
            
            array_push($cc, $rowCentroCosto);
        }

        $sqlConsecutivo = "SELECT consec FROM almginventario WHERE tipomov = '2' AND tiporeg = '03' ORDER BY consec DESC";
        $rowConsecutivo = mysqli_fetch_row(mysqli_query($linkbd, $sqlConsecutivo));

        $consecutivo = $rowConsecutivo[0] + 1;

        $out['consecutivo'] = $consecutivo;
        $out['bodegas'] = $bodegas;
        $out['cc'] = $cc;
    }

    if ($action == "guardar") {
        $consecutivo = $_POST['consecutivo'];
        $descripcion = $_POST['descripcion'];
        $total = $_POST['total'];
        preg_match("/([0-9]{2})\/([0-9]{2})\/([0-9]{4})/", $_POST["fecha"], $f);
        $fecha = $f[3]."-".$f[2]."-".$f[1];
        $vigencia = $f[3];
        $tipoComprobante = "62";
       
        $sqlr = "SELECT count(*) From dominios dom  where dom.nombre_dominio = 'PERMISO_MODIFICA_DOC'  and dom.valor_final <= '$fecha'  AND dom.valor_inicial =  '$_SESSION[cedulausu]' ";
        $res = mysqli_query($linkbd,$sqlr);
        $row = mysqli_fetch_row($res);

        if ($row[0] >= 1) 
        {
            $user = $_SESSION['nickusu'];
            $sqlEntidad = "SELECT nit FROM configbasica";
            $rowEntidad = mysqli_fetch_row(mysqli_query($linkbd, $sqlEntidad));

            $nitEntidad = $rowEntidad[0];

            date_default_timezone_set('America/Bogota');

            $sqlInventario = "INSERT INTO almginventario (fecha, tipomov, tiporeg, valortotal, usuario, estado, nombre, consec, vigenciadoc) VALUES ('$fecha', '2', '03', $total, '$_SESSION[cedulausu]', 'S', '$descripcion', $consecutivo, '$vigencia') ";
            mysqli_query($linkbd, $sqlInventario);

            $sqlSalida= "INSERT INTO alm_salida_activos_cab (consecutivo, tipo_movimiento, fecha, descripcion, valor_total, estado) VALUES ($consecutivo, '203', '$fecha', '$descripcion', $total, 'S')";
            mysqli_query($linkbd, $sqlSalida);

            $sqlComprobante = "INSERT INTO comprobante_cab (numerotipo, tipo_comp, fecha, concepto, total_debito, total_credito, estado) VALUES ('$consecutivo', '$tipoComprobante', '$fecha', '$descripcion', $total, $total, '1') ";
            mysqli_query($linkbd, $sqlComprobante);

            for ($i=0; $i < count($_POST['listaArticulos']); $i++) { 
                
                $bodega = $_POST['listaArticulos'][$i][0];
                $cc = $_POST['listaArticulos'][$i][1];
                $codArticulo = $_POST['listaArticulos'][$i][2];
                $cantidadSalida = $_POST['listaArticulos'][$i][5];
                $unidadMedida = $_POST['listaArticulos'][$i][4];
                $valorUnitario = $_POST['listaArticulos'][$i][6];
                $valorTotal = $_POST['listaArticulos'][$i][7];
                $grupo = substr($codArticulo, 0, -5);
                $codigo = substr($codArticulo, 4);

                $sqlConceptosContables = "SELECT CC.codigo, CC.nombre, CCD.cuenta FROM conceptoscontables AS CC INNER JOIN conceptoscontables_det AS CCD ON CCD.codigo = CC.codigo WHERE CC.modulo = 5 AND CC.tipo = 'CT' AND CCD.tipo = 'CT' AND CCD.cc = '$cc' ORDER BY CCD.fechainicial DESC";
                $rowConceptosContables = mysqli_fetch_row(mysqli_query($linkbd, $sqlConceptosContables));

                $cuentaDebito = $rowConceptosContables[2];

                $sqlGrupoInventario = "SELECT nombre, concepent FROM almgrupoinv WHERE codigo = '$grupo'";
                $rowGrupoInventario = mysqli_fetch_row(mysqli_query($linkbd, $sqlGrupoInventario));

                $sqlCuentaContableArticulo = "SELECT cuenta FROM conceptoscontables_det WHERE codigo = '$rowGrupoInventario[1]' AND tipo = 'AE' AND debito = 'S' ";
                $rowCuentaContableArticulo = mysqli_fetch_row(mysqli_query($linkbd, $sqlCuentaContableArticulo));
                
                $cuentaCredito = $rowCuentaContableArticulo[0];

                $sqlArticulo = "SELECT codunspsc FROM almarticulos WHERE codigo = '$codigo' AND grupoinven = '$grupo' AND estado = 'S'";
                $rowArticulo =  mysqli_fetch_row(mysqli_query($linkbd, $sqlArticulo));
                
                $unspsc = $rowArticulo[0];

                $sqlInventarioDet = "INSERT INTO almginventario_det (codigo, unspsc, codart, cantidad_entrada, cantidad_salida, valorunit, valortotal, unidad, tipomov, tiporeg, bodega, codcuentacre, cc, concepto) VALUES ($consecutivo, '$unspsc', '$codArticulo', 0, $cantidadSalida, $valorUnitario, $valorTotal, '$unidadMedida', '2', '03', '$bodega', '$cuentaCredito', '$cc', '$cuentaDebito') ";
                mysqli_query($linkbd, $sqlInventarioDet);
                
                $sqlSalidaDet = "INSERT INTO alm_salida_activos_det (consecutivo, bodega, cc, cod_articulo, cantidad, valor_unitario, valor_total, estado) VALUES ($consecutivo, '$bodega', '$cc', '$codArticulo', '$cantidadSalida', $valorUnitario, $valorTotal, 'S')";
                mysqli_query($linkbd, $sqlSalidaDet);
    
                $sqlComprobanteDeb = "INSERT INTO comprobante_det (id_comp, cuenta, tercero, centrocosto, detalle, cheque, valdebito, valcredito, estado, vigencia, tipo_comp, numerotipo, numacti, cantarticulo, bodega_ubicacion) VALUES ('$tipoComprobante $consecutivo', '$cuentaDebito', '$nitEntidad', '$cc', 'Salida por traslado a activos fijos', '', $valorTotal, 0, '1', '$vigencia', '$tipoComprobante', '$consecutivo', '$codArticulo', $cantidadSalida, '$bodega')";
                mysqli_query($linkbd, $sqlComprobanteDeb);
    
                $sqlComprobanteCred = "INSERT INTO comprobante_det (id_comp, cuenta, tercero, centrocosto, detalle, cheque, valdebito, valcredito, estado, vigencia, tipo_comp, numerotipo, numacti, cantarticulo, bodega_ubicacion) VALUES ('$tipoComprobante $consecutivo', '$cuentaCredito', '$nitEntidad', '$cc', 'Salida por traslado a activos fijos', '', 0, $valorTotal, '1', '$vigencia', '$tipoComprobante', '$consecutivo', '$codArticulo', $cantidadSalida, '$bodega')";
                mysqli_query($linkbd, $sqlComprobanteCred);
    
                $out['insertaBien'] = true;
            }
        }
        else {
            //Fecha no disponible
            $out['insertaBien'] = false;
        }
    }

    header("Content-type: application/json");
    echo json_encode($out);
    die();