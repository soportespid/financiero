const URL = 'almacen/salidaTraslado/crear/inve-salidaActivosFijos.php';
import { filtroEnArrayDeObjetos } from './../../../funciones.js';

var app = new Vue({
    el: '#myapp',
	data:{

        error: '',
        loading: false,
        movimiento: 1,

        consecutivo: '',
        descripcion: '',
        bodega: '',
        bodegas: [],
        cc: '',
        centrocostos: [],
        codArticulo: '',
        nomArticulo: '',
        canBodega: '',
        canSalida: '',
        unidadMedida: '',
        valorUnidad: '',
        valorTotal: '',
        showArticulos: false,
        articulos: [],
        articuloscopy: [],
        searchArticulo: '',
        listaArticulos: [],
        totalValores: 0,
    },

    mounted: function(){

        this.datosIniciales();
    },

    methods: 
    {
        //Desglosar array
        toFormData: function(obj){
            var form_data = new FormData();
            for(var key in obj){
                form_data.append(key, obj[key]);
            }
            return form_data;
        },

        formatonumero: function(valor){
            return new Intl.NumberFormat("en-US", {style: "currency", currency: "USD"}).format(valor);
        },

        traeParametros: function(name){
            //Captura parametros
            name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
            var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
            results = regex.exec(location.search);
            return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
        },

        datosIniciales: async function() {
            
            await axios.post(URL+'?action=datosIniciales')
            .then((response) => {
                
                this.centrocostos = response.data.cc;
                this.bodegas = response.data.bodegas;
                this.consecutivo = response.data.consecutivo;
            });
        },

        ventanaArticulo: async function() {

            if (this.bodega != '' && this.cc != '') {
                this.loading = true;
                await axios.post('vue/inve-salidaDirecta.php?action=listaArticulos&bodega='+this.bodega+'&cc='+this.cc)
                .then((response) => {
                    this.articulos = response.data.articulos;
                    this.articuloscopy = response.data.articulos;
                    this.loading = false;
                    this.showArticulos = true;
                });     
            }
            else {
                Swal.fire("Error", "Debes seleccionar la bodega y centro de costos", "error");
            }
        },

        filtroArticulos: async function() {

            const data = this.articuloscopy;
            var text = this.searchArticulo;
            var resultado = [];
            
            resultado = await filtroEnArrayDeObjetos({'data': data, 'text': text});

            this.articulos = resultado;
        },

        seleccionarArticulo: async function(articulo) {

            this.searchArticulo = "";
            this.valorTotal = "";
            this.canSalida = "";
            this.codArticulo = articulo[0];
            this.nomArticulo = articulo[1];
            this.canBodega = articulo[3];
            this.unidadMedida = articulo[4];
            this.valorUnidad = articulo[7];
            this.showArticulos = false;

            for (let index = 0; index < this.listaArticulos.length; index++) {
                
                if (this.listaArticulos[index][2] == this.codArticulo) {

                    this.canBodega = this.canBodega - this.listaArticulos[index][5];
                }
            }
        },

        validaCantidadSalida: function(disponible, numero) {

            const cantidadDisponible = disponible;
            const cantidadSalida = numero;

            if ((cantidadSalida > cantidadDisponible) || (cantidadSalida <= 0)) {
                Swal.fire("Error", "Cantidad ingresada erronea", "error");
                this.canSalida = "";
                this.valorTotal = "";
            }
            else {
                this.valorTotal = this.canSalida * this.valorUnidad;
            }
        },

        agregarArticulo: function() {

            var validacionArticulo = false;
            
            if (this.codArticulo != "" && this.nomArticulo != "" && this.canBodega != "" && this.canSalida != "" && this.unidadMedida != "" && this.valorUnidad != "" && this.valorTotal != "") {

                for (let index = 0; index < this.listaArticulos.length; index++) {
                
                    if (this.listaArticulos[index][2] == this.codArticulo) {
    
                        validacionArticulo = true;
                    }
                }

                if (validacionArticulo == false) {
                    
                    var articulos = [];

                    articulos.push(this.bodega);
                    articulos.push(this.cc);
                    articulos.push(this.codArticulo);
                    articulos.push(this.nomArticulo);
                    articulos.push(this.unidadMedida);
                    articulos.push(this.canSalida);
                    articulos.push(this.valorUnidad);
                    articulos.push(this.valorTotal);
                    this.listaArticulos.push(articulos);                
                    this.totalValores += this.valorTotal;
                    this.bodega = this.cc = this.codArticulo = this.nomArticulo = this.canSalida = this.unidadMedida = this.valorUnidad = this.valorTotal = this.canBodega = "";
                }
                else {
                    Swal.fire({
                        icon: 'warning',
                        title: 'Error',
                        text: 'Articulo ya fue agregado'
                    })
                }
                
            }
            else {
                Swal.fire({
                    icon: 'warning',
                    title: 'Falta información',
                    text: 'Faltan datos para agregar articulo'
                })
            }
        },

        eliminarArticuloAgregado: function(articulo) {


            Swal.fire({
                icon: 'question',
                title: 'Esta seguro que quiere eliminar?',
                showDenyButton: true,
                confirmButtonText: 'Eliminar!',
                denyButtonText: 'Cancelar',
                }).then((result) => {
                if (result.isConfirmed) {

                    this.totalValores = parseFloat(this.totalValores) - parseFloat(articulo[7]);

                    var i = this.listaArticulos.indexOf( articulo );
                    
                    if ( i !== -1 ) {
                        this.listaArticulos.splice( i, 1 );
                    }   
                } 
            })  
        },

        guardar: function() {

            var fecha = document.getElementById('fechaRegistro').value;

            if (this.listaArticulos.length != 0 && this.totalValores != "" && this.consecutivo != "" && this.descripcion != "" && fecha != "") {

                Swal.fire({
                    icon: 'question',
                    title: 'Esta seguro que quiere guardar?',
                    showDenyButton: true,
                    confirmButtonText: 'Guardar!',
                    denyButtonText: 'Cancelar',
                    }).then((result) => {
                    if (result.isConfirmed) {

                        var formData = new FormData();

                        formData.append("consecutivo", this.consecutivo);
                        formData.append("fecha", fecha);
                        formData.append("descripcion", this.descripcion);
                        formData.append("total", this.totalValores);

                        for(let i=0; i <= this.listaArticulos.length-1; i++) {

                            const val = Object.values(this.listaArticulos[i]).length-1;
    
                            for(let x = 0; x <= val; x++) {
                                formData.append("listaArticulos["+i+"][]", Object.values(this.listaArticulos[i])[x]);
                            }
                        }

                        axios.post(URL + '?action=guardar', formData)
                        .then((response) => {
                            
                            // console.log(response.data);
                            if(response.data.insertaBien == 1){
                                Swal.fire({
                                    position: 'top-end',
                                    icon: 'success',
                                    title: 'La salida ha sido guardada con exito',
                                    showConfirmButton: false,
                                    timer: 1500
                                }).then((response) => {
                                    this.redireccionar();
                                });
                            }
                            else{
                            
                                Swal.fire(
                                    'Error!',
                                    'No se pudo guardar.',
                                    'error'
                                );
                            }
                        });
                    } 
                })  
            }
            else {
                Swal.fire({
                    icon: 'warning',
                    title: 'Falta información',
                    text: 'Falta información para poder guardar'
                })
            }
        },

        redireccionar: function () {

            location.href = "inve-salidaActivosFijos";
        },
    },
});