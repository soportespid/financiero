<?php
    require_once '../../../comun.inc';
    require '../../../funciones.inc';
    require '../../../funcionesSP.inc.php';
    session_start();
    if($_POST){
        $obj = new Articulo();
        if($_POST['action']=="get"){
            $obj->getData();
        }else if($_POST['action']=="search"){
            $currentPage = intval($_POST['page']);
            $obj->search($_POST['search'],$currentPage,$_POST['option']);
        }else if($_POST['action'] == "search_mov"){
            $obj->searchMov($_POST['articulo'],$_POST['bodega'],$_POST['fecha_inicial'],$_POST['fecha_final']);
        }
    }

    class Articulo{
        private $linkbd;
        private $strCodigoArticulo;
        public function __construct() {
            $this->linkbd = conectar_v7();
            $this->linkbd->set_charset("utf8");
        }
        public function search(string $search,int $currentPage,string $type){
            if(!empty($_SESSION)){
                $perPage = 200;
                $startRows = ($currentPage-1) * $perPage;
                $totalRows = 0;
                if($type == "cod_articulo"){
                    $sql = "SELECT
                    a.nombre,
                    CONCAT(a.grupoinven,a.codigo) as codigo_articulo,
                    d.unidad
                    FROM almarticulos a
                    INNER JOIN almarticulos_det d
                    ON d.articulo = CONCAT(a.grupoinven,a.codigo)
                    WHERE CONCAT(a.grupoinven,a.codigo) = '$search'";
                    $request = mysqli_query($this->linkbd,$sql)->fetch_assoc();
                    $arrResponse = array("status"=>true,"data"=>$request);
                }else{
                    $sql = "SELECT a.nombre, CONCAT(a.grupoinven,a.codigo) as codigo_articulo
                    FROM almarticulos a
                    INNER JOIN almarticulos_det d
                    ON d.articulo = CONCAT(a.grupoinven,a.codigo)
                    WHERE a.nombre like '$search%' OR CONCAT(a.grupoinven,a.codigo) like '$search%'
                    ORDER BY length(a.codigo),a.codigo DESC LIMIT $startRows, $perPage";
                    $request = mysqli_fetch_all(mysqli_query($this->linkbd,$sql),MYSQLI_ASSOC);
                    $totalRows = intval(mysqli_query($this->linkbd,
                        "SELECT count(*) as total
                        FROM almarticulos
                        WHERE nombre like '$search%' OR CONCAT(grupoinven,codigo) like '$search%' ")->fetch_assoc()['total']);

                    $totalPages = $totalRows > 0 ? ceil($totalRows/$perPage) : 1;
                    $arrResponse = array("status"=>true,"data"=>$request,"total"=>$totalRows,"total_pages"=>$totalPages);
                }
                echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
            }
            die();
        }
        public function searchMov($codigo,$bodega,$fechaInicial,$fechaFinal){
            if($fechaInicial !="" && $fechaFinal ==""){
                $arrResponse = array("status"=>false,"msg"=>"Para buscar por fechas, debe llenar ambos campos. Inténtelo de nuevo.");
            }elseif($fechaInicial =="" && $fechaFinal !=""){
                $arrResponse = array("status"=>false,"msg"=>"Para buscar por fechas, debe llenar ambos campos. Inténtelo de nuevo.");
            }else{
                $request = $this->selectMovimientos($codigo,$bodega,$fechaInicial,$fechaFinal);
                $arrResponse = array("status"=>true,"data"=>$request);
            }
            echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
        }
        public function getData(){
            if(!empty($_SESSION)){
                $arrArticulos = $this->selectArticulos();
                $request['articulos'] = $arrArticulos['articulos'];
                $request['articulos_resultados'] = $arrArticulos['resultados'];
                $request['articulos_paginas'] =$arrArticulos['paginas'];
                $request['movimientos'] = $this->selectMovimientos("","01","","");
                $request['bodegas'] = $this->selectBodegas();
                echo json_encode($request,JSON_UNESCAPED_UNICODE);
            }
            die();
        }

        public function selectMovimientos($codigo,$bodega,$fechaInicial,$fechaFinal){
            $dateRange ="";
            if($fechaInicial !="" && $fechaFinal !=""){
                $arrFechaInicio = explode("/",$fechaInicial);
                $arrFechaFinal = explode("/",$fechaFinal);

                $dateInicio = date_format(date_create($arrFechaInicio[2]."-".$arrFechaInicio[1]."-".$arrFechaInicio[0]),"Y-m-d");
                $dateFinal = date_format(date_create($arrFechaFinal[2]."-".$arrFechaFinal[1]."-".$arrFechaFinal[0]),"Y-m-d");
                $dateRange = " AND i.fecha >= '$dateInicio' AND i.fecha <= '$dateFinal'";
            }
            $arrData = [];
            $articulos = mysqli_fetch_all(mysqli_query($this->linkbd,
                "SELECT CONCAT(grupoinven,codigo) as codigo,
                nombre FROM almarticulos WHERE CONCAT(grupoinven,codigo) like '$codigo%'"),MYSQLI_ASSOC);
            if(count($articulos)>0){
                $total = count($articulos);
                for ($i=0; $i < $total ; $i++) {
                    $cantidad = 0;
                    $valorTotal = 0;
                    $idArt = $articulos[$i]['codigo'];
                    $movimientos = mysqli_fetch_all(mysqli_query(
                        $this->linkbd,
                        "SELECT DATE_FORMAT(i.fecha,'%d/%m/%Y') as fecha,
                        DATE(i.fecha) as fechaorden ,
                        d.codart,
                        d.codigo as documento,
                        d.valortotal as total,
                        d.valorunit as valor,
                        d.cantidad_entrada as entrada,
                        d.cantidad_salida as salida,
                        d.unidad,
                        CONCAT(d.tipomov,d.tiporeg) as movimiento,
                        d.tipomov,
                        c.nombre as centro,
                        t.nombre as mov_nom
                        FROM almginventario_det d
                        INNER JOIN almginventario i ON i.consec = d.codigo AND i.estado = 'S' AND CONCAT(d.tipomov,d.tiporeg) = CONCAT(i.tipomov,i.tiporeg)
                        INNER JOIN centrocosto c ON c.id_cc = d.cc
                        INNER JOIN almtipomov t ON CONCAT(d.tipomov,d.tiporeg) = CONCAT(t.tipom,t.codigo)
                        WHERE d.bodega='$bodega' AND d.codart = $idArt $dateRange
                        ORDER BY fechaorden ASC;"),MYSQLI_ASSOC
                    );
                    $totalMov = count($movimientos);
                    if($totalMov > 0){
                        $arrComprobantes = array(
                            "101"=>50,
                            "104"=>53,
                            "106"=>54,
                            "107"=>51,
                            "201"=>60,
                            "202"=>61,
                            "203"=>62,
                            "204"=>63,
                            "205"=>64,
                            "206"=>65,
                            "301"=>2050,
                            "304"=>2053,
                            "306"=>2054,
                            "307"=>2051,
                            "403"=>2062,
                            "406"=>2065
                        );
                        for ($j=0; $j < $totalMov ; $j++) {
                            $cantidad += $movimientos[$j]['entrada'];
                            if($movimientos[$j]['salida'] > 0)$cantidad-=$movimientos[$j]['salida'];
                            $valorTotal = $cantidad * $movimientos[$j]['valor'];
                            $tipoComprobante = $arrComprobantes[$movimientos[$j]['movimiento']];
                            $validCant = $movimientos[$j]['entrada'] > 0 ? $movimientos[$j]['entrada'] : $movimientos[$j]['salida'];
                            $movimientos[$j]['cantidad_saldo'] = $cantidad;
                            $movimientos[$j]['total_saldo'] = $valorTotal;
                            $movimientos[$j]['tipo'] = $tipoComprobante;
                            $movimientos[$j]['total_entrada'] = $movimientos[$j]['valor'] * $movimientos[$j]['entrada'];
                            $movimientos[$j]['total_salida'] = $movimientos[$j]['valor'] * $movimientos[$j]['salida'];
                            $movimientos[$j]['estado_contable'] = $this->validContabilidad($movimientos[$j]['codart'],$tipoComprobante,$validCant,$movimientos[$j]['documento']);
                        }
                        $data['movimientos'] = $movimientos;
                        $data['nombre'] = $articulos[$i]['codigo']." - ".$articulos[$i]['nombre'];
                        $data['valor_total'] = $valorTotal;
                        $data['cantidad_total'] = $cantidad;
                        array_push($arrData,$data);
                    }


                }
            }
            return $arrData;
        }
        public function validContabilidad($codigo,$comprobante,$cantidad,$documento){
            $sql="SELECT cantarticulo FROM comprobante_det
            WHERE numacti = '$codigo' AND tipo_comp = '$comprobante' AND numerotipo = '$documento'";
            $request = mysqli_query($this->linkbd,$sql)->fetch_assoc()['cantarticulo'];
            $result = 0;
            if($request > 0){
                $result = $request == $cantidad ? 1 : 2;
            }else{
                $result = 3;
            }
            return $result;
        }
        public function selectBodegas(){
            $sql = "SELECT * FROM almbodegas WHERE estado='S' ORDER BY id_cc";
            $request = mysqli_fetch_all(mysqli_query($this->linkbd,$sql ),MYSQLI_ASSOC);
            return $request;
        }
        public function selectArticulos(){
            $perPage = 200;
            $totalRows = intval(mysqli_query($this->linkbd,
                "SELECT count(*) as total
                FROM almarticulos")->fetch_assoc()['total']);
            $totalPages = $totalRows > 0 ? ceil($totalRows/$perPage) : 1;

            $sql = "SELECT a.nombre,
                    a.codunspsc,
                    CONCAT(a.grupoinven,a.codigo) as codigo_articulo,
                    d.unidad
                    FROM almarticulos a
                    INNER JOIN almarticulos_det d
                    ON CONCAT(a.grupoinven,a.codigo) = d.articulo
                    WHERE a.nombre !=''
                    ORDER BY length(a.codigo),a.codigo DESC LIMIT 0, $perPage";
            $articulos = mysqli_fetch_all(mysqli_query($this->linkbd,$sql),MYSQLI_ASSOC);
            $arrData = array("articulos"=>$articulos,"resultados"=>$totalRows,"paginas"=>$totalPages);
            return $arrData;
        }
    }
?>
