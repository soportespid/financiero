const URL ='almacen/Kardex/buscar/inve-gestionKardex.php';

var app = new Vue({
    el:"#myapp",
    data() {
        return {
            isLoading:false,
            isModalArticulos:false,
            strSearch:"",
            strBuscarCodArticulo:"",
            intResults:0,
            intPage:1,
            intTotalPages:1,
            arrMovimientos:[],
            arrBodegas:[],
            arrArticulos:[],
            objArticulo:{"codigo_articulo":"","nombre":"","unidad":"","codunspsc":""},
            selectBodega:"01"
        }
    },
    mounted() {
        this.getData();
    },
    methods: {
        getData: async function(){
            this.isLoading = true;
            let formData = new FormData();
            formData.append("action","get");

            const response = await fetch(URL,{method:"POST",body:formData});
            const objData = await response.json();

            this.arrMovimientos = objData.movimientos;
            this.arrBodegas = objData.bodegas;
            this.arrArticulos = objData.articulos;
            this.intResults = objData.articulos_resultados;
            this.intTotalPages = objData.articulos_paginas;
            this.isLoading = false;
            console.log(objData);
        },
        search: async function(page=1,option=""){
            let search =""
            if(page <= 0){
                page = 1;
                this.intPage = page;
            }else if(page > this.intTotalPages){
                page = this.intTotalPages;
                this.intPage = page;
            }
            if(option=="cod_articulo") search = this.strBuscarCodArticulo;
            if(option=="articulo") search = this.strSearch;
            this.isLoading = true;
            const formData = new FormData();
            formData.append("action","search");
            formData.append("search",search);
            formData.append("option",option);
            formData.append("page",page);
            const response = await fetch(URL,{method:"POST",body:formData});
            const objData = await response.json();
            if(option=="cod_articulo"){
                if(objData.data !=null){
                    this.objArticulo = objData.data;
                }else{
                    this.objArticulo = {"codigo_articulo":"","nombre":"","unidad":"","codunspsc":""}
                    this.strBuscarCodArticulo = "";
                }
                this.strArticuloNombre = this.objArticulo.nombre;
            }else{
                this.arrArticulos = objData.data;
                this.intResults = objData.total;
                this.intTotalPages = objData.total_pages;
            }
            this.isLoading = false;
        },
        searchMovs:async function(){
            let formData = new FormData();
            let fechaInicial = document.querySelector("#fechaInicial").value;
            let fechaFinal = document.querySelector("#fechaFinal").value;

            if(fechaFinal != "" && fechaInicial ==""){
                Swal.fire("Error","Para buscar por fechas, ambos campos deben estar llenos","error");
                return false;
            }
            if(fechaFinal == "" && fechaInicial !=""){
                Swal.fire("Error","Para buscar por fechas, ambos campos deben estar llenos","error");
                return false;
            }
            formData.append("action","search_mov");
            formData.append("bodega",this.selectBodega);
            formData.append("articulo",this.objArticulo.codigo_articulo);
            formData.append("fecha_inicial",fechaInicial);
            formData.append("fecha_final",fechaFinal);
            const response = await fetch(URL,{method:"POST",body:formData});
            const objData = await response.json();
            if(objData.status){
                this.arrMovimientos = objData.data;
            }else{
                Swal.fire("Error",objData.msg,"error");
            }
        },
        selectItem: async function(item,type){
            this.objArticulo = item;
            this.strBuscarCodArticulo = this.objArticulo.codigo_articulo;
        },
        formatNumero: function(valor){
            return new Intl.NumberFormat("en-US", {style: "currency", currency: "USD"}).format(valor);
        },
        printPDF:function(){
            let fechaInicial = document.querySelector("#fechaInicial").value;
            let fechaFinal = document.querySelector("#fechaFinal").value;

            window.open('inve-pdfGestionKardex.php?bodega='
            +this.selectBodega
            +'&articulo='+this.objArticulo.codigo_articulo
            +'&fecha_inicial='+fechaInicial
            +'&fecha_final='+fechaFinal,"_blank");
        },
        printExcel:function(){
            let fechaInicial = document.querySelector("#fechaInicial").value;
            let fechaFinal = document.querySelector("#fechaFinal").value;

            window.open('inve-excelGestionKardex.php?bodega='
            +this.selectBodega
            +'&articulo='+this.objArticulo.codigo_articulo
            +'&fecha_inicial='+fechaInicial
            +'&fecha_final='+fechaFinal,"_blank");
        }
    },
})
