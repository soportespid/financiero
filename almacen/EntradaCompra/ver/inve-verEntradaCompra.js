const URL ='almacen/EntradaCompra/ver/inve-verEntradaCompra.php';

var app = new Vue({
    el:"#myapp",
    data() {
        return {
            isLoading:false,
            isModalArticulos:false,
            isModalRegistro:false,
            isModalDocumento:false,
            isSelectAll:true,
            strDescripcion:"",
            strSearch:"",
            strBuscarCodArticulo:"",
            strMedida:"",
            strModelo:"",
            strMarca:"",
            strSerie:"",
            intValor:"",
            intCantidad:"",
            intResults:0,
            intPage:1,
            intTotalPages:1,
            intConsecutivo:0,
            intTotalValor:0,
            intTotalReversion:0,
            intIdCodigo:0,
            arrArticulos:[],
            arrCentros:[],
            arrBodegas:[],
            arrProductos:[],
            arrRegistros:[],
            arrDocumentos:[],
            arrRevProductos:[],
            objArticulo:{"codigo":"","nombre":"","unidad":"","codunspsc":""},
            objRegistro:{"consvigencia":"","detalle":"","valor":"","saldo":0,"credito":"","debito":""},
            objDocumento:{},
            selectMovimiento:1,
            selectEntrada:"01",
            selectCostos:"",
            selectBodegas:"",
            intRevConsecutivo:0,
            intRevDocumento:"",
            strRevDescripcion:"",
            strFecha:"",
        }
    },
    mounted() {
        this.getData();
    },
    methods: {  
        getData: async function(){
            this.intIdCodigo = new URLSearchParams(window.location.search).get('id');
            if(this.intIdCodigo > 0){
                const formData = new FormData();
                formData.append("action","get");
                formData.append("codigo",this.intIdCodigo);
                const response = await fetch(URL,{method:"POST",body:formData});
                const objData = await response.json();
                console.log(objData);
                if(objData.status){
                    let data = objData.data;
                    let documento = data.documento;
                    this.objDocumento = documento.cabecera;
                    this.intConsecutivo = this.objDocumento.consec;
                    this.strDescripcion = this.objDocumento.nombre;
                    this.selectBodegas = documento.articulos[0]['bodega']+" - "+documento.articulos[0]['bodega_nombre'];
                    this.selectCostos = documento.articulos[0]['cc']+" - "+documento.articulos[0]['cc_nombre'];
                    this.arrProductos = documento.articulos;
                    this.strFecha = this.objDocumento.fecha;
                    this.intTotalValor = this.objDocumento.valortotal;
                }else{
                    Swal.fire("Error",objData.msg,"error");
                }

            }else{
                window.location.href="inve-buscarGestionEntradaCompra.php";
            }
        },
        save:async function(){
            const formData = new FormData();
            let obj;
            if(this.selectMovimiento == 1){
                if(this.intConsecutivo == "" || this.strDescripcion == "" || document.querySelector("#fechaInicial").value==""){
                    Swal.fire("Error","Todos los campos de la cabecera documento son obligatorios.","error");
                    return false;
                }
                if(this.objRegistro.consvigencia ==""){
                    Swal.fire("Error","Por favor, seleccione un registro presupuestal.","error");
                    return false;
                }
                if(this.arrProductos.length <= 0){
                    Swal.fire("Error","Debe agregar al menos un artículo.","error");
                    return false;
                }
                obj = {
                    "movimiento":1,
                    "cabecera":{
                        "consecutivo":this.intConsecutivo,
                        "fecha":document.querySelector("#fechaInicial").value,
                        "descripcion":this.strDescripcion,
                        "registro":{
                            "idrp":this.objRegistro.consvigencia,
                            "centro":this.selectCostos.id,
                            "bodega":this.selectBodegas.id,
                            "total":this.intTotalValor
                        }
                    },
                    "detalle":this.arrProductos
                }
            }else{
                if(this.strRevDescripcion == ""){
                    Swal.fire("Error","Por favor, escribe el motivo o razón de reversión","error");
                    return false;
                }
                if(this.objDocumento.consec == ""){
                    Swal.fire("Error","Por favor, elige el documento a reversar","error");
                    return false;
                }
                obj = {
                    "movimiento":3,
                    "cabecera":{
                        "consecutivo":this.intRevConsecutivo,
                        "fecha":document.querySelector("#fechaR").value,
                        "descripcion":this.strRevDescripcion,
                        "registro":{
                            "idrp":this.objDocumento.codmov,
                            "centro":this.objDocumento.cc,
                            "bodega":this.objDocumento.bodega1,
                            "total":this.intTotalReversion,
                            "entrada_consec":this.objDocumento.consec
                        }
                    },
                    "detalle":this.arrRevProductos
                }
            }
            this.isLoading = true;
            formData.append("entrada",JSON.stringify(obj));
            formData.append("action","save");
            const response = await fetch(URL,{method:"POST",body:formData});
            const objData = await response.json();
            if(!objData)window.location.reload();
            if(objData.status && objData.status_comp){
                this.arrProductos = [];
                this.strMedida = "";
                this.strArticuloNombre ="";
                this.strDescripcion = "";
                this.selectCostos = "";
                this.selectBodegas = "";
                this.intTotalValor="";
                this.objArticulo={"codigo":"","nombre":"","unidad":"","codunspsc":""},
                this.objRegistro={"consvigencia":"","detalle":"","valor":"","saldo":0,"credito":"","debito":""}
                this.intTotalReversion = 0;
                this.objDocumento.consec = "";
                this.arrRevProductos = [];
                this.objDocumento.nombre = "";
                this.getData();
                Swal.fire("Guardado",objData.msg,"success");
            }else if(!objData.status_comp){
                this.arrProductos = [];
                this.strMedida = "";
                this.strArticuloNombre ="";
                this.strDescripcion = "";
                this.selectCostos = "";
                this.selectBodegas = "";
                this.intTotalValor="";
                this.objArticulo={"codigo":"","nombre":"","unidad":"","codunspsc":""},
                this.objRegistro={"consvigencia":"","detalle":"","valor":"","saldo":0,"credito":"","debito":""}
                this.getData();
                Swal.fire("Error",objData.msg,"error");
            }else if(!objData.status){
                Swal.fire("Error",objData.msg,"error");
            }
            this.isLoading = false;
        },
        formatNumero: function(valor){
            return new Intl.NumberFormat("en-US", {style: "currency", currency: "USD"}).format(valor);
        },
        validInteger:function(valor){
            valor = new String(valor);
            if(valor.includes(".") || valor.includes(",")){
                Swal.fire("Error","La cantidad debe ser un número entero.","error");
            }
        },
        printPDF:function(){
            window.open('inve-pdfEntradaCompra.php?id='+this.intIdCodigo,"_blank");
        }
    }
})