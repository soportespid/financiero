<?php
    require_once '../../../comun.inc';
    require '../../../funciones.inc';
    require '../../../funcionesSP.inc.php';
    session_start();
    if($_POST){
        $obj = new Articulo();
        if($_POST['action']=="get"){
            $obj->getData($_POST['codigo']);
        }
    }

    class Articulo{
        private $linkbd;
        private $strCodigoGrupo;
        private $strCodigoCentro;
        private $strVigencia;
        private $intIdCodigo;
        public function __construct() {
            $this->linkbd = conectar_v7();
            $this->linkbd->set_charset("utf8");
            $this->strVigencia = vigencia_usuarios($_SESSION['cedulausu']);
        }
        public function getData($id){
            if(!empty($_SESSION)){
                if($id > 0){
                    $request['documento'] = $this->selectDatosEntrada($id);
                    $arrResponse = array("status"=>true,"data"=>$request);
                }else{
                    $arrResponse = array("status"=>false,"msg"=>"Error de datos");
                }
                echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
            }
            die();
        }

        public function selectDatosEntrada($id){
            $this->intIdCodigo = $id;
            $sql = "SELECT *,DATE_FORMAT(fecha,'%d/%m/%Y') as fecha FROM almginventario WHERE codigo = $this->intIdCodigo";
            $request['cabecera'] = mysqli_query($this->linkbd,$sql)->fetch_assoc();

            $idrp = $request['cabecera']['codmov']; 
            $idConsec = $request['cabecera']['consec'];
            $sql = "SELECT cuenta_debito as credito, LEFT(productoservicio, 1) as tipo_producto
            FROM ccpetdc_detalle 
            WHERE vigencia='$this->strVigencia' AND LEFT(productoservicio, 1) <= 4 AND destino_compra='01' AND idrp = $idrp";

            $credito = mysqli_query($this->linkbd,$sql)->fetch_assoc()['credito'];

            $sql = "SELECT DATE_FORMAT(fecha,'%d/%m/%Y') as fecha,detalle,valor,consvigencia 
            FROM ccpetrp 
            WHERE consvigencia='$idrp' and vigencia='$this->strVigencia'";
            $request['registro'] = mysqli_query($this->linkbd,$sql)->fetch_assoc();
            $request['registro']['credito'] = $credito;
            $request['registro']['saldo'] =$request['registro']['valor'] - mysqli_query(
                $this->linkbd,
                "SELECT sum(valortotal) as total FROM `almginventario` WHERE estado = 'S' 
                AND codmov = '$idrp' AND tipomov = '1' AND tiporeg ='01'"
            )->fetch_assoc()['total'];
            $sqlAr = "SELECT *,unspsc as codunspsc,
                    codart as codigo,valorunit as valor,
                    valortotal as total, codcuentacre as credito,
                    cc as centro, concepto as debito, cantidad_entrada as cantidad
                    FROM almginventario_det WHERE tipomov = 1 AND tiporeg='01' AND codigo = $idConsec ORDER BY id_det DESC";
            $articulos = mysqli_fetch_all(mysqli_query($this->linkbd,$sqlAr),MYSQLI_ASSOC);
            $totalArt = count($articulos);
            for ($j=0; $j < $totalArt ; $j++) { 
                $idBodega = $articulos[$j]['bodega'];
                $idCentro = $articulos[$j]['centro'];
                $idArticulo = $articulos[$j]['codart'];
                $articulos[$j]['bodega_nombre'] = mysqli_query($this->linkbd,"SELECT nombre FROM almbodegas WHERE id_cc='$idBodega'")->fetch_assoc()['nombre'];
                $articulos[$j]['cc_nombre'] = mysqli_query($this->linkbd,"SELECT nombre FROM centrocosto WHERE id_cc='$idCentro'")->fetch_assoc()['nombre'];
                $articulos[$j]['nombre'] = mysqli_query($this->linkbd,"SELECT nombre FROM almarticulos WHERE CONCAT(grupoinven,codigo) = '$idArticulo'")->fetch_assoc()['nombre'];
            }
            $request['articulos'] = $articulos;
            return $request;
        }
    }
?>