<?php
    require_once '../../../comun.inc';
    require '../../../funciones.inc';
    require '../../../funcionesSP.inc.php';
    session_start();
    if($_POST){
        $obj = new Articulo();
        if($_POST['action']=="search"){
            $currentPage = intval($_POST['page']);
            $obj->search($_POST['search'],$currentPage,$_POST['option']);
        }else if($_POST['action']=="get"){
            $obj->getData();
        }else if($_POST['action'] =="getDebt"){
            $obj->getDebt($_POST['id'],$_POST['centro']);
        }else if($_POST['action'] == "save"){
            $obj->save($_POST['entrada']);
        }else if($_POST['action'] == "registros"){
            echo json_encode($obj->selectRegistros($_POST['vigencia']),JSON_UNESCAPED_UNICODE);
        }
    }

    class Articulo{
        private $linkbd;
        private $strCodigoArticulo;
        private $strCodigoGrupo;
        private $strCodigoCentro;
        private $strVigencia;
        public function __construct() {
            $this->linkbd = conectar_v7();
            $this->linkbd->set_charset("utf8");
            $this->strVigencia = date("Y");
        }
        public function search(string $search,int $currentPage,string $type){
            if(!empty($_SESSION)){
                $perPage = 200;
                $startRows = ($currentPage-1) * $perPage;
                $totalRows = 0;
                if($type == "cod_articulo"){
                    $sql = "SELECT nombre, CONCAT(grupoinven,codigo) as codigo_articulo
                    FROM almarticulos WHERE CONCAT(grupoinven,codigo) = '$search'";
                    $request = mysqli_query($this->linkbd,$sql)->fetch_assoc();
                    $strArticuloCod = $request['codigo_articulo'];

                    $request['unidad'] = mysqli_query(
                        $this->linkbd,
                        "SELECT unidad FROM almarticulos_det WHERE articulo = '$strArticuloCod'"
                    )->fetch_assoc()['unidad'];
                    $arrResponse = array("status"=>true,"data"=>$request);
                }else{
                    $sql = "SELECT nombre, CONCAT(grupoinven,codigo) as codigo_articulo
                    FROM almarticulos
                    WHERE nombre like '$search%' OR CONCAT(grupoinven,codigo) like '$search%'
                    ORDER BY length(codigo),codigo DESC LIMIT $startRows, $perPage";
                    $request = mysqli_fetch_all(mysqli_query($this->linkbd,$sql),MYSQLI_ASSOC);
                    $totalArticulos = count($request);
                    for ($i=0; $i < $totalArticulos; $i++) {
                        $strArticuloCod = $request[$i]['codigo_articulo'];
                        $request[$i]['unidad'] = mysqli_query(
                            $this->linkbd,
                            "SELECT unidad FROM almarticulos_det WHERE articulo = '$strArticuloCod'"
                        )->fetch_assoc()['unidad'];
                    }
                    $totalRows = intval(mysqli_query($this->linkbd,
                        "SELECT count(*) as total
                        FROM almarticulos
                        WHERE nombre like '$search%' OR CONCAT(grupoinven,codigo) like '$search%' ")->fetch_assoc()['total']);

                    $totalPages = $totalRows > 0 ? ceil($totalRows/$perPage) : 1;
                    $arrResponse = array("status"=>true,"data"=>$request,"total"=>$totalRows,"total_pages"=>$totalPages);
                }
                echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
            }
            die();
        }
        public function getData(){
            if(!empty($_SESSION)){
                $arrArticulos = $this->selectArticulos();
                $request['articulos'] = $arrArticulos['articulos'];
                $request['articulos_resultados'] = $arrArticulos['resultados'];
                $request['articulos_paginas'] =$arrArticulos['paginas'];
                $request['centros'] = $this->selectCentros();
                $request['bodegas'] = $this->selectBodegas();
                $request['consecutivo'] = $this->selectConsecutivo();
                $request['rev_consecutivo'] = $this->selectRevConsecutivo();
                $request['documentos_entradas'] = $this->selectDocumentos();
                $request['registros'] = $this->selectRegistros();
                echo json_encode($request,JSON_UNESCAPED_UNICODE);
            }
            die();
        }
        public function getDebt($id,$centro){
            $this->strCodigoCentro = $centro;
            $this->strCodigoGrupo = substr($id,0,4);
            $request = mysqli_query(
                $this->linkbd,
                "SELECT t2.cuenta, MAX(t2.fechainicial) as fecha
                FROM conceptoscontables_det t2
                INNER JOIN almgrupoinv t1
                ON t1.concepent = t2.codigo
                WHERE t1.codigo ='$this->strCodigoGrupo' AND t2.cc = '$this->strCodigoCentro'
                AND t2.debito = 'S' AND t2.modulo = '5' AND t2.tipo='AE'"
            )->fetch_assoc()['cuenta'];
            echo json_encode($request,JSON_UNESCAPED_UNICODE);
            die();
        }

        public function save($data){
            if(!empty($_SESSION)){
                $arrData = json_decode($data,true);
                //dep($arrData);exit;
                if(empty($arrData['cabecera']) || empty($arrData['detalle'])){
                    $arrResponse = array("status"=>false,"msg"=>"Error de datos");
                }else{
                    $arrCabecera = $arrData['cabecera'];
                    $arrDetalle = $arrData['detalle'];
                    $arrRegistro = $arrCabecera['registro'];
                    if(empty($arrRegistro['idrp']) || empty($arrRegistro['centro'])
                    || empty($arrRegistro['bodega']) || empty($arrRegistro['total'])){
                        $arrResponse = array("status"=>false,"msg"=>"Error de información registro.");
                    }else if(empty($arrCabecera['consecutivo']) || empty($arrCabecera['fecha'])
                    || empty($arrCabecera['descripcion'])){
                        $arrResponse = array("status"=>false,"msg"=>"Error de cabecera.");
                    }else if(empty($arrDetalle)){
                        $arrResponse = array("status"=>false,"msg"=>"Error de artículos.");
                    }else{
                        /**********CABECERA***********/
                        $arrDate = explode("/",$arrCabecera['fecha']);
                        $strFecha = date_format(date_create($arrDate[2]."-".$arrDate[1]."-".$arrDate[0]),"Y-m-d");
                        $strVigencia = $arrDate[2];
                        $strDescripcion = $arrCabecera['descripcion'];
                        $intConsecutivo = $arrCabecera['consecutivo'];
                        $intIdrp = $arrRegistro['idrp'];
                        $strCentro = $arrRegistro['centro'];
                        $strBodega = $arrRegistro['bodega'];
                        $strTipoRegistro = "01";
                        $intTotalUsado = $arrRegistro['total'];
                        $intMov = $arrData['movimiento'];
                        $strUsuario = $_SESSION['cedulausu'];
                        $intTipoComprobante = $intMov == 1 ? 50:2050;
                        $intEstado = 1;
                        $strTercero = $this->selectTercero();
                        $totalArticulos = count($arrDetalle);
                        $intConsecEntrada = 0;
                        if(isset($arrRegistro['entrada_consec'])){
                            $intConsecEntrada = $arrRegistro['entrada_consec'];
                        }
                        $validarBloqueo = $this->validBloqueo($strFecha,$strUsuario);
                        if($validarBloqueo >= 1){
                            $request = $this->insertCompraAlmacen($strFecha,$intMov,$strTipoRegistro,$intIdrp,
                            $intTotalUsado,$strUsuario,$strDescripcion,$intConsecutivo,$strBodega,$strVigencia,$strCentro,$intConsecEntrada);
                            if($request > 0){
                                for ($i=0; $i < $totalArticulos ; $i++) {
                                    $codigo = $intConsecutivo;
                                    $unspsc = $arrDetalle[$i]['codunspsc'];
                                    $codart = $arrDetalle[$i]['codigo'];
                                    $solicitud = $intIdrp;
                                    $cantidad_entrada = $intMov == 1 ? $arrDetalle[$i]['cantidad'] : 0;
                                    $cantidad_salida = $intMov == 3 ? $arrDetalle[$i]['cantidad'] : 0;
                                    $valorunit = $arrDetalle[$i]['valor'];
                                    $valortotal = $arrDetalle[$i]['total'];
                                    $unidad = $arrDetalle[$i]['unidad'];
                                    $tipomov = $intMov;
                                    $tiporeg = $strTipoRegistro;
                                    $bodega = $arrDetalle[$i]['bodega'];
                                    $codcuentacre = $intMov == 1 ? $arrDetalle[$i]['credito'] : $arrDetalle[$i]['debito'];
                                    $cc = $arrDetalle[$i]['centro'];
                                    $concepto = $intMov == 1 ? $arrDetalle[$i]['debito'] : $arrDetalle[$i]['credito'];
                                    $marca = $arrDetalle[$i]['marca'];
                                    $modelo = $arrDetalle[$i]['modelo'];
                                    $serie = $arrDetalle[$i]['serie'];

                                    $request = $this->insertArticuloAlmacen($codigo,$unspsc,$codart,$solicitud,$cantidad_entrada,$cantidad_salida,
                                    $valorunit,$valortotal,$unidad,$tipomov,$tiporeg,$bodega,$codcuentacre,
                                    $cc,$concepto,$marca,$modelo,$serie);
                                }
                                $request_comp = $this->insertComprobante($intConsecutivo,$intTipoComprobante,$strFecha,$strDescripcion,$intTotalUsado,$intEstado,$intConsecEntrada);
                                if($request_comp>0){
                                    $strIdComprobante = $intTipoComprobante." ".$intConsecutivo;
                                    $intEstadoDetalle = 1;
                                    $intNumeroTipo = 1;

                                    for ($i=0; $i < $totalArticulos ; $i++) {
                                        $strDetalle = "Entrada por compra del articulo ".$arrDetalle[$i]['codigo']."-".$arrDetalle[$i]['nombre'];
                                        $this->insertCompDetalleDeb(
                                            $strIdComprobante,
                                            $arrDetalle[$i]['debito'],
                                            $strTercero,
                                            $arrDetalle[$i]['centro'],
                                            $strDetalle,
                                            $intMov == 1 ? $arrDetalle[$i]['total'] : 0,
                                            $intMov == 1 ? 0 : $arrDetalle[$i]['total'],
                                            $intEstadoDetalle,
                                            $strVigencia,
                                            $intTipoComprobante,
                                            $intNumeroTipo,
                                            $arrDetalle[$i]['codigo'],
                                            $arrDetalle[$i]['cantidad'],
                                            $arrDetalle[$i]['bodega'],
                                        );
                                        $this->insertCompDetalleCred(
                                            $strIdComprobante,
                                            $arrDetalle[$i]['credito'],
                                            $strTercero,
                                            $arrDetalle[$i]['centro'],
                                            $strDetalle,
                                            $intMov == 1 ? 0 : $arrDetalle[$i]['total'],
                                            $intMov == 1 ? $arrDetalle[$i]['total'] : 0,
                                            $intEstadoDetalle,
                                            $strVigencia,
                                            $intTipoComprobante,
                                            $intNumeroTipo,
                                            $arrDetalle[$i]['codigo'],
                                            $arrDetalle[$i]['cantidad'],
                                            $arrDetalle[$i]['bodega'],
                                        );
                                    }
                                    if($intMov == 1){
                                        $arrResponse = array("status"=>true,"status_comp"=>true,"msg"=>"Datos guardados correctamente");
                                    }else{
                                        $arrResponse = array("status"=>true,"status_comp"=>true,"msg"=>"Datos reversados correctamente");
                                    }
                                }else{
                                    $arrResponse = array("status"=>true,"status_comp"=>false,"msg"=>"Error, datos guardados en almacén pero no en contabilidad.");
                                }
                            }else{
                                $arrResponse = array("status"=>false,"msg"=>"No se ha podido guardar, inténtelo de nuevo.");
                            }
                        }else{
                            $arrResponse = array("status"=>false,"msg"=>"Fecha bloqueada.");
                        }
                    }
                }
            }else{
                $arrResponse = false;
            }
            echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
            die();
        }
        public function validBloqueo($strFecha,$strUsuario){
            $sql = "SELECT count(*) as valor FROM dominios dom  WHERE dom.nombre_dominio = 'PERMISO_MODIFICA_DOC'  AND dom.valor_final <= '$strFecha'  AND dom.valor_inicial =  '$strUsuario'";
            $request = mysqli_query($this->linkbd,$sql)->fetch_assoc()['valor'];
            return $request;
        }
        public function selectArticulos(){
            $perPage = 200;
            $totalRows = intval(mysqli_query($this->linkbd,
                "SELECT count(*) as total
                FROM almarticulos")->fetch_assoc()['total']);
            $totalPages = $totalRows > 0 ? ceil($totalRows/$perPage) : 1;

            $sql = "SELECT nombre, codunspsc, CONCAT(grupoinven,codigo) as codigo_articulo
            FROM almarticulos WHERE nombre !='' ORDER BY length(codigo),codigo DESC LIMIT 0, $perPage";
            $articulos = mysqli_fetch_all(mysqli_query($this->linkbd,$sql),MYSQLI_ASSOC);
            $totalArticulos = count($articulos);
            for ($i=0; $i < $totalArticulos; $i++) {
                $strArticuloCod = $articulos[$i]['codigo_articulo'];
                $articulos[$i]['unidad'] = mysqli_query(
                    $this->linkbd,
                    "SELECT unidad FROM almarticulos_det WHERE articulo = '$strArticuloCod'"
                )->fetch_assoc()['unidad'];
            }
            $arrData = array("articulos"=>$articulos,"resultados"=>$totalRows,"paginas"=>$totalPages);
            return $arrData;
        }
        public function selectRegistros($vigencia=""){

            if($vigencia!=""){
                $this->strVigencia = $vigencia;
            }

            $arrRegistros = [];
            $requestRD = mysqli_fetch_all(mysqli_query(
                $this->linkbd,
                "SELECT idrp, SUM(valor) as total, vigencia,cuenta_debito as credito
                FROM ccpetdc_detalle
                WHERE vigencia='$this->strVigencia' AND destino_compra='01' GROUP BY idrp"),MYSQLI_ASSOC
            );
            $totalResult = count($requestRD);

            for ($i=0; $i < $totalResult ; $i++) {
                $idRp = $requestRD[$i]['idrp'];
                $strVigencia = $requestRD[$i]['vigencia'];
                $totalRp = 0;
                $requestInve = mysqli_fetch_all(mysqli_query($this->linkbd,
                "SELECT consec, tiporeg FROM almginventario
                WHERE codmov = '".$idRp."' AND fecha LIKE '%".$strVigencia."%' AND tipomov = '1'")
                ,MYSQLI_ASSOC);
                $totalInve = count($requestInve);
                for ($j=0; $j < $totalInve ; $j++) {
                    $intConsecutivo = $requestInve[$j]['consec'];
                    $intTipoRegistro = $requestInve[$j]['tiporeg'];
                    $totalRp+= mysqli_query($this->linkbd,
                        "SELECT SUM(valortotal) as total
                        FROM almginventario_det
                        WHERE codigo = '$intConsecutivo' AND tipomov = '1' AND tiporeg = '$intTipoRegistro' GROUP BY codigo, tiporeg"
                    )->fetch_assoc()['total'];
                }
                if($totalRp < $requestRD[$i]['total']){
                    $registro = mysqli_query($this->linkbd,
                    "SELECT DATE_FORMAT(fecha,'%d/%m/%Y') as fecha,detalle,valor,consvigencia
                    FROM ccpetrp
                    WHERE consvigencia='$idRp' and vigencia='$this->strVigencia'")->fetch_assoc();
                    $registro['credito'] = $requestRD[$i]['credito'];
                    $registro['saldo'] =$registro['valor'] - mysqli_query(
                        $this->linkbd,
                        "SELECT sum(valortotal) as total FROM `almginventario` WHERE estado = 'S' AND codmov = '$idRp' AND tipomov = '1' AND tiporeg ='01' AND vigenciadoc = '$strVigencia'"
                    )->fetch_assoc()['total'];
                    array_push($arrRegistros,$registro);
                }
            }
            return $arrRegistros;
        }
        public function selectDocumentos(){
            $sql = "SELECT * ,DATE_FORMAT(fecha,'%d/%m/%Y') as fecha FROM almginventario WHERE tipomov = 1 AND tiporeg ='01' AND estado='S' ORDER BY consec DESC";
            $request = mysqli_fetch_all(mysqli_query($this->linkbd,$sql),MYSQLI_ASSOC);
            if(count($request)){
                $totalDocs = count($request);
                for ($i=0; $i < $totalDocs ; $i++) {
                    $idCodigo = $request[$i]['consec'];
                    $sqlAr = "SELECT *,unspsc as codunspsc,
                    codart as codigo,valorunit as valor,
                    valortotal as total, codcuentacre as credito,
                    cc as centro, concepto as debito, cantidad_entrada as cantidad
                    FROM almginventario_det WHERE tipomov = 1 AND tiporeg='01' AND codigo = $idCodigo ORDER BY id_det DESC";
                    $articulos = mysqli_fetch_all(mysqli_query($this->linkbd,$sqlAr),MYSQLI_ASSOC);
                    $totalArt = count($articulos);
                    for ($j=0; $j < $totalArt ; $j++) {
                        $idBodega = $articulos[$j]['bodega'];
                        $idCentro = $articulos[$j]['centro'];
                        $idArticulo = $articulos[$j]['codart'];
                        $articulos[$j]['bodega_nombre'] = mysqli_query($this->linkbd,"SELECT nombre FROM almbodegas WHERE id_cc='$idBodega'")->fetch_assoc()['nombre'];
                        $articulos[$j]['cc_nombre'] = mysqli_query($this->linkbd,"SELECT nombre FROM centrocosto WHERE id_cc='$idCentro'")->fetch_assoc()['nombre'];
                        $articulos[$j]['nombre'] = mysqli_query($this->linkbd,"SELECT nombre FROM almarticulos WHERE CONCAT(grupoinven,codigo) = '$idArticulo'")->fetch_assoc()['nombre'];
                        $articulos[$j]['isSelected'] = true;
                    }
                    $request[$i]['articulos'] = $articulos;
                }
            }
            return $request;
        }
        public function selectBodegas(){
            $sql = "SELECT * FROM almbodegas WHERE estado='S' ORDER BY id_cc";
            $request = mysqli_fetch_all(mysqli_query($this->linkbd,$sql ),MYSQLI_ASSOC);
            return $request;
        }
        public function selectCentros(){
            $sql = "SELECT * FROM centrocosto WHERE estado='S' ORDER BY id_cc";
            $request = mysqli_fetch_all(mysqli_query($this->linkbd,$sql),MYSQLI_ASSOC);
            return $request;
        }
        public function selectTercero(){
            $sql = "SELECT nit FROM configbasica WHERE estado = 'S'";
            $request = mysqli_query($this->linkbd,$sql)->fetch_assoc()['nit'];
            return $request;
        }
        public function selectRevConsecutivo(){
            $sql = "SELECT consec FROM almginventario WHERE tipomov='3' AND tiporeg='01'
            ORDER BY consec DESC";
            $request = mysqli_query($this->linkbd,$sql,MYSQLI_ASSOC)->fetch_assoc()['consec']+1;
            return $request;
        }
        public function selectConsecutivo(){
            $sql = "SELECT consec FROM almginventario
            WHERE tipomov='1'
            AND tiporeg='01'
            ORDER BY consec DESC";
            $request = mysqli_query($this->linkbd,
                $sql,MYSQLI_ASSOC)->fetch_assoc()['consec']+1;
            return $request;
        }
        public function insertCompraAlmacen($strFecha,$intMov,$strTipoRegistro,$intIdrp,
            $intTotalUsado,$strUsuario,$strDescripcion,$intConsecutivo,$strBodega,$strVigencia,$strCentro,$intConsecEntrada){
            if($intConsecEntrada > 0){
                mysqli_query($this->linkbd,"UPDATE almginventario SET estado = 'R' WHERE consec = $intConsecEntrada");
            }
            $chrEstado = "S";
            $sql = "INSERT INTO almginventario (fecha,tipomov,tiporeg,codmov,valortotal,usuario,nombre,consec,bodega1,bodega2,vigenciadoc,cc,estado)
                    VALUES('$strFecha','$intMov','$strTipoRegistro','$intIdrp','$intTotalUsado','$strUsuario','$strDescripcion','$intConsecutivo','$strBodega','$strBodega','$strVigencia','$strCentro','$chrEstado')";
            $request = mysqli_query($this->linkbd,$sql);
            return $request;
        }
        public function insertArticuloAlmacen($codigo,$unspsc,$codart,$solicitud,$cantidad_entrada,$cantidad_salida,
        $valorunit,$valortotal,$unidad,$tipomov,$tiporeg,$bodega,$codcuentacre,
        $cc,$concepto,$marca,$modelo,$serie){
            $sql = "INSERT INTO almginventario_det (codigo,unspsc,codart,solicitud,cantidad_entrada,cantidad_salida,
                    valorunit,valortotal,unidad,tipomov,tiporeg,bodega,codcuentacre,
                    cc,concepto,marca,modelo,serie)
                    VALUES ('$codigo','$unspsc','$codart','$solicitud','$cantidad_entrada','$cantidad_salida',
                    '$valorunit','$valortotal','$unidad','$tipomov','$tiporeg','$bodega','$codcuentacre',
                    '$cc','$concepto','$marca','$modelo','$serie')";
            $request = mysqli_query($this->linkbd,$sql);
            return $request;
        }
        public function insertComprobante($intConsecutivo,$intTipoComprobante,$strFecha,$strDescripcion,$intTotalUsado,$intEstado,$intConsecEntrada){
            if($intConsecEntrada > 0){
                mysqli_query($this->linkbd,"UPDATE comprobante_cab SET estado = 0 WHERE tipo_comp=50 AND numerotipo = $intConsecEntrada");
            }
            $sql = "INSERT INTO comprobante_cab (numerotipo,tipo_comp,fecha,concepto,total,total_debito,total_credito,estado)
            VALUES($intConsecutivo,$intTipoComprobante,'$strFecha','$strDescripcion',$intTotalUsado,$intTotalUsado,$intTotalUsado,$intEstado)";
            $request = mysqli_query($this->linkbd,$sql);
            return $request;
        }
        public function insertCompDetalleDeb($idComprobante,$cuenta,$tercero,$centro,$detalle,
        $valorDeb,$valorCre,$estado,$vigencia,$tipoComp,$numeroTipo,$codigo,$cantidad,$bodega){
            $sql = "INSERT INTO comprobante_det (id_comp,cuenta,tercero,centrocosto,detalle,valdebito,valcredito,estado,
            vigencia,tipo_comp,numerotipo,numacti,cantarticulo,bodega_ubicacion)
            VALUES('$idComprobante','$cuenta','$tercero','$centro','$detalle',$valorDeb,$valorCre,$estado,'$vigencia',$tipoComp,$numeroTipo,'$codigo',$cantidad,'$bodega')";
            $request = mysqli_query($this->linkbd,$sql);
            return $request;
        }
        public function insertCompDetalleCred($idComprobante,$cuenta,$tercero,$centro,$detalle,
        $valorDeb,$valorCre,$estado,$vigencia,$tipoComp,$numeroTipo,$codigo,$cantidad,$bodega){
            $sql = "INSERT INTO comprobante_det (id_comp,cuenta,tercero,centrocosto,detalle,valdebito,valcredito,estado,
            vigencia,tipo_comp,numerotipo,numacti,cantarticulo,bodega_ubicacion)
            VALUES('$idComprobante','$cuenta','$tercero','$centro','$detalle',$valorDeb,$valorCre,$estado,'$vigencia',$tipoComp,$numeroTipo,'$codigo',$cantidad,'$bodega')";
            $request = mysqli_query($this->linkbd,$sql);
            return $request;
        }
    }
?>
