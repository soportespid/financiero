const URL ='almacen/EntradaCompra/crear/inve-gestionEntradaCompra.php';

var app = new Vue({
    el:"#myapp",
    data() {
        return {
            isLoading:false,
            isModalArticulos:false,
            isModalRegistro:false,
            isModalDocumento:false,
            isSelectAll:true,
            strDescripcion:"",
            strSearch:"",
            strBuscarCodArticulo:"",
            strMedida:"",
            strModelo:"",
            strMarca:"",
            strSerie:"",
            intValor:"",
            intCantidad:"",
            intResults:0,
            intPage:1,
            intTotalPages:1,
            intConsecutivo:0,
            intTotalValor:0,
            intTotalReversion:0,
            arrArticulos:[],
            arrCentros:[],
            arrBodegas:[],
            arrArticulos:[],
            arrProductos:[],
            arrRegistros:[],
            arrDocumentos:[],
            arrRevProductos:[],
            objArticulo:{"codigo":"","nombre":"","unidad":"","codunspsc":""},
            objRegistro:{"consvigencia":"","detalle":"","valor":"","saldo":0,"credito":"","debito":""},
            objDocumento:{"consec":"","nombre":"","valortotal":0},
            selectMovimiento:1,
            selectEntrada:"01",
            selectCostos:"",
            selectBodegas:"",
            intRevConsecutivo:0,
            intRevDocumento:"",
            strRevDescripcion:"",
            selectVigencia :new Date().getFullYear(),
            arrVigencias:[]
        }
    },
    mounted() {
        this.getData();
    },
    methods: {
        getData: async function(){
            const date = new Date();
            const formData = new FormData();
            formData.append("action","get");
            const response = await fetch(URL,{method:"POST",body:formData});
            const objData = await response.json();
            this.arrCentros = objData.centros;
            this.arrBodegas = objData.bodegas;
            this.arrArticulos = objData.articulos;
            this.intResults = objData.articulos_resultados;
            this.intTotalPages = objData.articulos_paginas;
            this.intConsecutivo = objData.consecutivo;
            this.arrRegistros = objData.registros;

            this.arrVigencias[0] = date.getFullYear()-1;
            this.arrVigencias[1] = date.getFullYear();
            this.selectVigencia = new Date().getFullYear(),
            //Datos reversar
            this.intRevConsecutivo = objData.rev_consecutivo;
            this.arrDocumentos = objData.documentos_entradas;

            document.querySelector("#selectCosto").removeAttribute("disabled");
            document.querySelector("#selectBodega").removeAttribute("disabled");
        },
        search: async function(page=1,option=""){
            let search =""
            if(page <= 0){
                page = 1;
                this.intPage = page;
            }else if(page > this.intTotalPages){
                page = this.intTotalPages;
                this.intPage = page;
            }
            if(option=="cod_articulo") search = this.strBuscarCodArticulo;
            if(option=="articulo") search = this.strSearch;
            this.isLoading = true;
            const formData = new FormData();
            formData.append("action","search");
            formData.append("search",search);
            formData.append("option",option);
            formData.append("page",page);
            const response = await fetch(URL,{method:"POST",body:formData});
            const objData = await response.json();
            if(option=="cod_articulo"){
                this.objArticulo = objData.data;
                this.strArticuloNombre = this.objArticulo.nombre;
                this.strMedida = this.objArticulo.unidad;
            }else{
                this.arrArticulos = objData.data;
                this.intResults = objData.total;
                this.intTotalPages = objData.total_pages;
            }
            this.isLoading = false;
        },
        selectItem: async function(item,type){
            if(type=="articulo"){
                this.objArticulo = item;
                this.strBuscarCodArticulo = this.objArticulo.codigo_articulo;
                this.strArticuloNombre = this.objArticulo.nombre;
                this.strMedida = this.objArticulo.unidad;
            }else if(type=="documento"){
                this.objDocumento = item;
                this.arrRevProductos = this.objDocumento.articulos;
                this.intTotalReversion = this.objDocumento.valortotal;
                this.isSelectAll = true;
            }else{
                this.objRegistro = item;
                this.intTotalValor=0;
                this.arrProductos = [];
            }
        },
        addProducto: async function(){
            if(this.objRegistro.consvigencia ==""){
                Swal.fire("Error","Por favor, seleccione un registro presupuestal.","error");
                return false;
            }

            if(this.selectCostos == "" || this.selectBodegas == "" ){
                Swal.fire("Error","El centro de costo y bodega deben ser seleccionados.","error");
                return false;
            }

            if(this.objArticulo.codigo == ""){
                Swal.fire("Error","Debe seleccionar un artículo.","error");
                return false;
            }
            if(this.intCantidad== "" || this.intValor== "" ){
                Swal.fire("Error","La cantidad y el valor del artículo son obligatorios.","error");
                return false;
            }
            if(this.intCantidad < 1 || this.intValor < 1){
                Swal.fire("Error","La cantidad o el valor del artículo es incorrecto.","error");
                return false;
            }
            let cantidadArt = this.intCantidad;
            let valorArt = this.intValor;
            let totalArt = parseFloat((cantidadArt * valorArt).toFixed(2));
            let objProduct = {
                "codigo":this.objArticulo.codigo_articulo,
                "codunspsc":this.objArticulo.codunspsc,
                "nombre":this.objArticulo.nombre,
                "unidad":this.objArticulo.unidad,
                "cantidad":cantidadArt,
                "valor":valorArt,
                "total":totalArt,
                "modelo":this.strModelo,
                "marca":this.strMarca,
                "serie":this.strSerie,
                "obj_bodega":this.selectBodegas,
                "obj_centro":this.selectCostos,
                "bodega":this.selectBodegas.id,
                "centro":this.selectCostos.id,
                "credito":this.objRegistro.credito
            }

            let total = 0;
            let flag = false;

            if(this.arrProductos.length > 0){
                this.arrProductos.forEach(element => {
                    total +=element.total;
                });
                total = total+objProduct.total;
                if(total > this.objRegistro.saldo){
                    Swal.fire("Error","El valor total no debe superar el saldo presupuestal.","error");
                    return false;
                }
                let index = this.arrProductos.map(el=>el.codigo).indexOf(objProduct.codigo);
                if(index > -1 && this.arrProductos[index]['codigo'] == objProduct.codigo){
                    Swal.fire("Error","Este artículo ya se agregó, intente con otro.","error");
                    return false;
                }

                flag = true;
            }else{
                if(objProduct['total'] > this.objRegistro.saldo){
                    Swal.fire("Error","El valor total no debe superar el saldo presupuestal.","error");
                    return false;
                }
                flag = true;
                total = objProduct.total
            }

            if(flag){
                this.isLoading = true;
                const formData = new FormData();
                formData.append("id",objProduct.codigo);
                formData.append("centro",objProduct.centro);
                formData.append("action","getDebt");
                const response = await fetch(URL,{method:"POST",body:formData});
                objProduct['debito'] = await response.json();
                this.arrProductos.push(objProduct);
                this.strBuscarCodArticulo = "";
                this.objArticulo.nombre = "";
                this.strMedida = "";
                this.intCantidad = "";
                this.intValor = "";
                this.strModelo = "";
                this.strSerie = "";
                this.strMarca = "";
                this.intTotalValor = total;
                this.isLoading = false;
                let totalProductos = this.arrProductos.length;
                if(totalProductos > 0){
                    document.querySelector("#selectCosto").setAttribute("disabled","disabled");
                    document.querySelector("#selectBodega").setAttribute("disabled","disabled");
                }else{
                    document.querySelector("#selectCosto").removeAttribute("disabled");
                    document.querySelector("#selectBodega").removeAttribute("disabled");
                }
            }
        },
        delProducto: function(item){
            let total = 0;
            let index = this.arrProductos.map(el=>el.codigo).indexOf(item.codigo);
            this.arrProductos.splice(index,1);
            this.arrProductos.forEach(element => {
                total +=element.total;
            });
            this.intTotalValor = total;
            let totalProductos = this.arrProductos.length;
            if(totalProductos == 0){
                document.querySelector("#selectCosto").removeAttribute("disabled");
                document.querySelector("#selectBodega").removeAttribute("disabled");
            }
        },
        save:async function(){
            const formData = new FormData();
            let obj;
            if(this.selectMovimiento == 1){
                if(this.intConsecutivo == "" || this.strDescripcion == "" || document.querySelector("#fechaInicial").value==""){
                    Swal.fire("Error","Todos los campos de la cabecera documento son obligatorios.","error");
                    return false;
                }
                if(this.objRegistro.consvigencia ==""){
                    Swal.fire("Error","Por favor, seleccione un registro presupuestal.","error");
                    return false;
                }
                if(this.arrProductos.length <= 0){
                    Swal.fire("Error","Debe agregar al menos un artículo.","error");
                    return false;
                }
                obj = {
                    "movimiento":1,
                    "cabecera":{
                        "consecutivo":this.intConsecutivo,
                        "fecha":document.querySelector("#fechaInicial").value,
                        "descripcion":this.strDescripcion,
                        "registro":{
                            "idrp":this.objRegistro.consvigencia,
                            "centro":this.selectCostos.id,
                            "bodega":this.selectBodegas.id,
                            "total":this.intTotalValor
                        }
                    },
                    "detalle":this.arrProductos
                }
            }else{
                if(this.strRevDescripcion == ""){
                    Swal.fire("Error","Por favor, escribe el motivo o razón de reversión","error");
                    return false;
                }
                if(this.objDocumento.consec == ""){
                    Swal.fire("Error","Por favor, elige el documento a reversar","error");
                    return false;
                }
                obj = {
                    "movimiento":3,
                    "cabecera":{
                        "consecutivo":this.intRevConsecutivo,
                        "fecha":document.querySelector("#fechaR").value,
                        "descripcion":this.strRevDescripcion,
                        "registro":{
                            "idrp":this.objDocumento.codmov,
                            "centro":this.objDocumento.cc,
                            "bodega":this.objDocumento.bodega1,
                            "total":this.intTotalReversion,
                            "entrada_consec":this.objDocumento.consec
                        }
                    },
                    "detalle":this.arrRevProductos
                }
            }
            this.isLoading = true;
            formData.append("entrada",JSON.stringify(obj));
            formData.append("action","save");
            const response = await fetch(URL,{method:"POST",body:formData});
            const objData = await response.json();
            if(!objData)window.location.reload();
            if(objData.status && objData.status_comp){
                this.arrProductos = [];
                this.strMedida = "";
                this.strArticuloNombre ="";
                this.strDescripcion = "";
                this.selectCostos = "";
                this.selectBodegas = "";
                this.strRevDescripcion= "";
                this.intTotalValor="";
                this.objArticulo={"codigo":"","nombre":"","unidad":"","codunspsc":""},
                this.objRegistro={"consvigencia":"","detalle":"","valor":"","saldo":0,"credito":"","debito":""}
                this.intTotalReversion = 0;
                this.objDocumento.consec = "";
                this.arrRevProductos = [];
                this.objDocumento.nombre = "";
                this.getData();
                Swal.fire("Guardado",objData.msg,"success");
            }else if(!objData.status_comp){
                this.arrProductos = [];
                this.strMedida = "";
                this.strArticuloNombre ="";
                this.strDescripcion = "";
                this.selectCostos = "";
                this.selectBodegas = "";
                this.intTotalValor="";
                this.objArticulo={"codigo":"","nombre":"","unidad":"","codunspsc":""},
                this.objRegistro={"consvigencia":"","detalle":"","valor":"","saldo":0,"credito":"","debito":""}
                this.getData();
                Swal.fire("Error",objData.msg,"error");
            }else if(!objData.status){
                Swal.fire("Error",objData.msg,"error");
            }
            this.isLoading = false;
        },
        formatNumero: function(valor){
            return new Intl.NumberFormat("en-US", {style: "currency", currency: "USD"}).format(valor);
        },
        validInteger:function(valor){
            valor = new String(valor);
            if(valor.includes(".") || valor.includes(",")){
                Swal.fire("Error","La cantidad debe ser un número entero.","error");
            }
        },
        showTab:function(tab){
            let tabs = this.$refs.rTabs.children;
            let tabsContent = this.$refs.rTabsContent.children;
            for (let i = 0; i < tabs.length; i++) {
                tabs[i].classList.remove("active");
                tabsContent[i].classList.remove("active")
            }
            tabs[tab-1].classList.add("active");
            tabsContent[tab-1].classList.add("active")
        },
        getRegistros: async function(){
            const formData = new FormData();
            formData.append("action","registros");
            formData.append("vigencia",this.selectVigencia);
            this.isLoading = true;
            const response = await fetch(URL,{method:"POST",body:formData});
            this.arrRegistros = await response.json();
            this.isLoading = false;
        }

    },
    computed:{
        total: function(){
            return this.intTotalValor;
        },
        saldo:function(){
            return this.objRegistro.saldo-this.intTotalValor;
        },
    }
})
