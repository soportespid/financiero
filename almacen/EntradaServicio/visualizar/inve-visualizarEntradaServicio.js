var app = new Vue({
    el: '#myapp',
	data:{

        error: '',
        loading: false,
        tabgroup1:'1',

        //cabecera
        consecutivo: '',
        descripcion: '',
        fecha: '',
        estilo: '',
        certificaAlm: 0,

        //gestion rp
        rp: '',
        fechaRP: '',
        descripcionRP: '',
        valorRP: '',
        valorGuardado: '',
        estado: '',
        detallesRP: [],
        datosCab: [],
        //articulos
        detalleArticulo: [],
        detalleArticuloManual: [],
        cantidadArticulos: 0,
        codigoArticulos: [],
        nombreArticulos: [],
        cantidad: [],
        valorArticulos: [],
        marca: [],
        modelo: [],
        serial: [],
        formato: [
            {codigo: '', nombre: '', cantidad: '', valor: '', marca: '', modelo: '', serial: ''},
        ],

        articulos: [],
        showArticulos: false,
        searchArt : {keywordArt: ''},
        posicionArticulo: '',
    },

    mounted: function(){

        this.traeDatosFormulario();
        this.traeArticulos();
    },

    methods: 
    {
        //Desglosar array
        toFormData: function(obj){
            var form_data = new FormData();
            for(var key in obj){
                form_data.append(key, obj[key]);
            }
            return form_data;
        },

        formatonumero: function(valor){
            return new Intl.NumberFormat("en-US", {style: "currency", currency: "USD"}).format(valor);
        },

        traeParametros: function(name){
            //Captura parametros
            name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
            var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
            results = regex.exec(location.search);
            return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
        },

        traeDatosFormulario: async function() {

            const consecutivo = this.traeParametros('cod');
            
            await axios.post('almacen/EntradaServicio/visualizar/inve-visualizarEntradaServicio.php?action=datosFormulario&conse='+consecutivo)
            .then((response) => {
                console.log(response.data.datosCab);
                app.datosCab = response.data.datosCab;
                app.detallesRP = response.data.datosDet;
                app.detalleArticulo = response.data.articulos;
                
            }).catch((error) => {
                this.error = true;
                console.log(error)
            }).finally(() => {
                
                this.consecutivo = consecutivo;
                this.fecha = this.datosCab[0];
                this.descripcion = this.datosCab[1];
                this.rp = this.datosCab[2];
                this.descripcionRP = this.datosCab[6];
                this.fechaRP = this.datosCab[5];
                this.valorRP = this.datosCab[7];
                this.valorGuardado = this.datosCab[3];
                this.certificaAlm = this.datosCab[8];

                if (this.datosCab[4] == 'S') {
                    this.estado = 'ACTIVO';
                    this.estilo = "background-color:#009900; color:#fff;";
                }
                else if (this.datosCab[4] == 'R') {
                    this.estado = 'REVERSADO';
                    this.estilo = "background-color:#aa0000; color:#fff;";
                }
            });
        },

        subirExcel: async function() {
            
            const input = document.getElementById("archivoExcel");
            await readXlsxFile(input.files[0]).then((rows) => {

                this.detalleArticulo = rows;
            })
            
            this.elimina();
        },

        elimina: function() {
            this.detalleArticulo.splice(0, 1);
        },

        bajarExcel: function() {

            const workbook = XLSX.utils.book_new();
            const worksheet = XLSX.utils.json_to_sheet(this.formato);
            XLSX.utils.book_append_sheet(workbook, worksheet, "formato");
            XLSX.writeFile(workbook, "formato.xlsx");
        },

        llenarCantidadArticulos: function() {

            if (parseInt(this.cantidadArticulos) >= 0) {
                if (this.cantidadArticulos < this.detalleArticuloManual.length) {
                    this.detalleArticuloManual = [];    
                }
                
                for (let i = 0; i < this.cantidadArticulos; i++) {
                    this.detalleArticuloManual[i] = 0;
                }
                this.detalleArticuloManual = Object.values(this.detalleArticuloManual);
            }
            else {
                Swal.fire("Error", "Cantidad mayor de articulos debe ser mayor a cero", "warning");
                this.cantidadArticulos = '';
                this.detalleArticuloManual = [];
            }
            
        },

        validaCantidad: function(index) {

            if(this.cantidad[index] <= 0) {
                Swal.fire("Error", "Cantidad menor a cero", "warning");
                this.cantidad[index] = '';
                this.cantidad = Object.values(this.cantidad);
            } 
        },

        validaValor: function(index) {

            if (this.valorArticulos[index] <= 0) {
                Swal.fire("Error", "Valor articulo menor a cero", "warning");
                this.valorArticulos[index] = '';
                this.valorArticulos = Object.values(this.valorArticulos);
            }
        },

        guardar: function() {

            Swal.fire({
                icon: 'question',
                title: 'Seguro que quieres guardar?',
                showDenyButton: true,
                confirmButtonText: 'Guardar',
                denyButtonText: 'Cancelar',
                }).then((result) => {
                if (result.isConfirmed) 
                {
                    if (result.isConfirmed) {
                        this.loading = true;
                        var formData = new FormData();

                        for(let i=0; i <= this.detalleArticulo.length-1; i++)
                        {
                            const val = Object.values(this.detalleArticulo[i]).length;

                            for(let x = 0; x <= val-1; x++) {

                                formData.append("detalleArticulo["+i+"][]", Object.values(this.detalleArticulo[i])[x]);
                            } 
                        }

                        for (let i = 0; i < this.codigoArticulos.length; i++) {
                            
                            formData.append('codigoArticulos[]', this.codigoArticulos[i]);   
                            formData.append('nombreArticulos[]', this.nombreArticulos[i]);   
                            formData.append('cantidad[]', this.cantidad[i]);   
                            formData.append('valorArticulos[]', this.valorArticulos[i]);
                            formData.append('marca[]', this.marca[i]);
                            formData.append('modelo[]', this.modelo[i]);
                            formData.append('serial[]', this.serial[i]);   
                        }

                        formData.append("consecutivo", this.consecutivo);
                        
                        axios.post('almacen/EntradaServicio/visualizar/inve-visualizarEntradaServicio.php?action=guardar', formData)
                        .then((response) => {
                            
                            if(response.data.insertaBien){
                                this.loading = false;
                                Swal.fire({
                                    icon: 'success',
                                    title: 'Se ha guardado con exito',
                                    showConfirmButton: false,
                                    timer: 1500
                                  }).then((response) => {
                                        app.redireccionar();
                                    });
                            }
                            else {
                                Swal.fire(
                                    'Error!',
                                    'No se pudo guardar.',
                                    'error'
                                );
                            }
                        });
                        
                    }
                } 
                else if (result.isDenied) 
                {
                    Swal.fire('Guardar cancelado', '', 'info');
                }
            })   
        },

        traeArticulos: async function() {

            await axios.post('almacen/EntradaServicio/visualizar/inve-visualizarEntradaServicio.php?action=articulos')
            .then((response) => {
                
                app.articulos = response.data.articulos;
            }).catch((error) => {
                this.error = true;
                console.log(error)
            }).finally(() => {
                
            });
        },

        despliegaArticulos: function(posicion) {

            this.showArticulos = true;
            this.posicionArticulo = posicion;
        },

        searchMonitoArticulo: async function() {

            var keywordArt = app.toFormData(this.searchArt);
            
            await axios.post('almacen/EntradaServicio/visualizar/inve-visualizarEntradaServicio.php?action=filtraArticulo', keywordArt)
            .then((response) => {
                app.articulos = response.data.articulos;
            }).catch((error) => {
                this.error = true;
                console.log(error)
            }).finally(() => {
        
            });  
        },

        seleccionaArticulo: function(articulo) {

            this.codigoArticulos[this.posicionArticulo] = articulo[0];
            this.nombreArticulos[this.posicionArticulo] = articulo[1];
            this.posicionArticulo = '';
            this.showArticulos = false;
        },

        redireccionar: function()
        {
            location.reload();
        },
    }
});