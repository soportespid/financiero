<?php

use Illuminate\Contracts\Auth\Guard;

    require '../../../comun.inc';
    require '../../../funciones.inc';

    $linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");

    $out = array('error' => false);

    $action = "show";

    if(isset($_GET['action'])){
        $action = $_GET['action'];
    }

    if ($action == 'datosFormulario') {
        
        $consecutivo = $_GET['conse'];
        $datosCab =  array();


        //Cabecera
        $sqlCab = "SELECT fecha, descripcion, rp, valor, estado, user, YEAR(fecha), certifica_alm FROM alm_entrada_servicio_cab WHERE consecutivo = $consecutivo";
        $rowCab = mysqli_fetch_row(mysqli_query($linkbd, $sqlCab));

        $certificaAlm = (int) $rowCab[7];

        $sqlrp="SELECT fecha,detalle,valor FROM ccpetrp WHERE consvigencia='$rowCab[2]' AND vigencia='$rowCab[6]'";
        $rowrp = mysqli_fetch_row(mysqli_query($linkbd, $sqlrp));

        array_push($datosCab, $rowCab[0]);
        array_push($datosCab, $rowCab[1]);
        array_push($datosCab, $rowCab[2]);
        array_push($datosCab, $rowCab[3]);
        array_push($datosCab, $rowCab[4]);
        array_push($datosCab, $rowrp[0]);
        array_push($datosCab, $rowrp[1]);
        array_push($datosCab, $rowrp[2]);
        array_push($datosCab, $certificaAlm);
        
        $out['datosCab'] = $datosCab;


        $datosDet = array();
        $maxVersion = ultimaVersionGastosCCPET();

        $sqlDet = "SELECT cod_vigenciag, seccion_presupuestal, bpim, cuenta, fuente, productoservicio, indicador_producto, cod_politicap, medio_pago, destino_compra, cuenta_debito, valor FROM alm_entrada_servicio_det WHERE consecutivo = '$consecutivo'";
        $resDet = mysqli_query($linkbd, $sqlDet);
        while ($rowDet = mysqli_fetch_row($resDet)) {

            $datos = array();

            $sqlVigenciaGasto = "SELECT nombre FROM ccpet_vigenciadelgasto WHERE codigo = $rowDet[0]";
            $rowVigenciaGasto = mysqli_fetch_row(mysqli_query($linkbd, $sqlVigenciaGasto));

            $sqlSeccionPresupuestal = "SELECT nombre FROM pptoseccion_presupuestal WHERE id_seccion_presupuestal = $rowDet[1]";
            $rowSeccionPresupuestal = mysqli_fetch_row(mysqli_query($linkbd, $sqlSeccionPresupuestal));


            $sqlCuentaCCPET = "SELECT nombre FROM cuentasccpet WHERE codigo = '$rowDet[3]' AND version = '$maxVersion'";
            $rowCuentaCCPET = mysqli_fetch_row(mysqli_query($linkbd, $sqlCuentaCCPET));

            $cuenta = $rowDet[3] . " - " . $rowCuentaCCPET[0];

            $sqlFuenteCUIPO = "SELECT nombre FROM ccpet_fuentes_cuipo WHERE codigo_fuente = '$rowDet[4]'";
            $rowFuenteCUIPO = mysqli_fetch_row(mysqli_query($linkbd, $sqlFuenteCUIPO));

            $fuente = $rowDet[4] . " - " . $rowFuenteCUIPO[0];

            $nomProductosres = buscaservicioccpetgastos($rowDet[5]);
            if($nomProductosres == ''){$nomProductosres = buscaproductoccpetgastos($rowDet[5]);}

            $nomProductosres = $rowDet[5] . " - " . $nomProductosres;

            $indicadorProducto = buscaindicadorccpet($rowDet[6]);
            $indicadorProducto = $rowDet[6] . " - " . $indicadorProducto;

            $sqlPoliticaPublica = "SELECT nombre FROM ccpet_politicapublica WHERE codigo = '$rowDet[7]'";
            $rowPoliticaPublica = mysqli_fetch_row(mysqli_query($linkbd, $sqlPoliticaPublica));

            $politicaPublica = $rowDet[7] . " - " . $rowPoliticaPublica[0];

            $sqlDestinoCompra = "SELECT nombre FROM almdestinocompra WHERE codigo = '$rowDet[9]'";
            $rowDestinoCompra = mysqli_fetch_row(mysqli_query($linkbd, $sqlDestinoCompra));

            $destinoCompra = $rowDet[9] . " - " . $rowDestinoCompra[0];

            $nombreCuentaContable = buscacuenta($rowDet[10]);

            array_push($datos, $rowVigenciaGasto[0]);
            array_push($datos, $rowSeccionPresupuestal[0]);
            array_push($datos, $rowDet[2]);
            array_push($datos, $cuenta);
            array_push($datos, $fuente);
            array_push($datos, $nomProductosres);
            array_push($datos, $indicadorProducto);
            array_push($datos, $politicaPublica);
            array_push($datos, $rowDet[8]);
            array_push($datos, $destinoCompra);
            array_push($datos, $nombreCuentaContable);
            array_push($datos, $rowDet[11]);
            array_push($datosDet, $datos);
        }

        $out['datosDet'] = $datosDet;

        $articulos = array();

        $sqlArticulos = "SELECT cod_articulo, nom_articulo, cantidad, valor, marca, modelo, serial FROM alm_entrada_servicio_articulos WHERE consecutivo = '$consecutivo' ORDER BY  CONVERT(cod_articulo, INT)";
        $resArticulos = mysqli_query($linkbd, $sqlArticulos);
        while ($rowArticulos = mysqli_fetch_row($resArticulos)) {
            
            array_push($articulos, $rowArticulos);
        }

        $out['articulos'] = $articulos;
    }

    if ($action == 'articulos') {
        
        $articulos = array();

        $sqlArt = "SELECT codigo, nombre, codunspsc, grupoinven FROM almarticulos WHERE estado = 'S' ORDER BY length(grupoinven),grupoinven ASC, length(codigo),codigo";
        $resArt = mysqli_query($linkbd, $sqlArt);
        while ($rowArt = mysqli_fetch_row($resArt)) {
            
            //crear array que se va limpiando
            $datos = array();

            //concatena el codigo del articulo
            $cod = $rowArt[3].$rowArt[0];

            //carga datos al array
            array_push($datos, $cod);
            array_push($datos, $rowArt[1]);
            array_push($datos, $rowArt[2]);

            //carga array al objeto
            array_push($articulos, $datos);
        }

        $out['articulos'] = $articulos;
    }

    if ($action == 'filtraArticulo') {

        $keywordArt = $_POST['keywordArt'];
        $articulos = array();

        $sqlArt = "SELECT codigo, nombre, codunspsc, grupoinven FROM almarticulos WHERE estado = 'S' AND concat_ws(' ', nombre, concat_ws('', grupoinven, codigo)) LIKE '%$keywordArt%' ORDER BY length(grupoinven),grupoinven ASC, length(codigo),codigo ASC";
        $resArt = mysqli_query($linkbd, $sqlArt);
        while ($rowArt = mysqli_fetch_row($resArt)) {
            
            //crear array que se va limpiando
            $datos = array();

            //concatena el codigo del articulo
            $cod = $rowArt[3].$rowArt[0];

            //carga datos al array
            array_push($datos, $cod);
            array_push($datos, $rowArt[1]);
            array_push($datos, $rowArt[2]);

            //carga array al objeto
            array_push($articulos, $datos);
        }

        $out['articulos'] = $articulos;
    }

    if ($action == 'guardar') {
        
        $consecutivo = "";
        $consecutivo = $_POST['consecutivo'];

        $sqlDelete = "DELETE FROM alm_entrada_servicio_articulos WHERE consecutivo = $consecutivo";
        mysqli_query($linkbd, $sqlDelete);

        for ($i=0; $i < count($_POST['detalleArticulo']); $i++) { 
            
            $codigo = $_POST['detalleArticulo'][$i][0];
            $nombre = $_POST['detalleArticulo'][$i][1];
            $cantidad = $_POST['detalleArticulo'][$i][2];
            $valor = $_POST['detalleArticulo'][$i][3];
            $marca = $_POST['detalleArticulo'][$i][4];
            $modelo = $_POST['detalleArticulo'][$i][5];
            $serial = $_POST['detalleArticulo'][$i][6];

            if ($codigo != '' && $nombre != '' && $cantidad != '' && $valor != '') {

                $consecutivoDet3 = selconsecutivo('alm_entrada_servicio_articulos', 'id');

                $sqlArticulos = "INSERT INTO alm_entrada_servicio_articulos VALUES ($consecutivoDet3, $consecutivo, '$codigo', '$nombre', $cantidad, $valor, '$marca', '$modelo', '$serial')";
                mysqli_query($linkbd, $sqlArticulos);
            }
        }

        for ($i=0; $i < count($_POST['codigoArticulos']); $i++) { 
            
            $codigo = $_POST['codigoArticulos'][$i];
            $nombre = $_POST['nombreArticulos'][$i];
            $cantidad = $_POST['cantidad'][$i];
            $valor = $_POST['valorArticulos'][$i];
            $marca = $_POST['marca'][$i];
            $modelo = $_POST['modelo'][$i];
            $serial = $_POST['serial'][$i];

            if ($codigo != '' && $nombre != '' && $cantidad != '' && $valor != '') {

                $consecutivoDet3 = selconsecutivo('alm_entrada_servicio_articulos', 'id');
                $sqlArticulos = "INSERT INTO alm_entrada_servicio_articulos VALUES ($consecutivoDet3, $consecutivo, '$codigo', '$nombre', $cantidad, $valor, '$marca', '$modelo', '$serial')";
                mysqli_query($linkbd, $sqlArticulos);
            }
        }

        $out['insertaBien'] = true;
    }

    header("Content-type: application/json");
    echo json_encode($out);
    die();