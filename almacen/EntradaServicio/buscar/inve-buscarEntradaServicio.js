const URL = "almacen/EntradaServicio/buscar/inve-buscarEntradaServicio.php";

var app = new Vue({
    el: '#myapp',
	data:{

        error: '',
        loading: false,
        tabgroup1:'1',

        codigoEntrada: '',
        datos: [],
        estilos: [],
        existeInformacion: true,
    },

    mounted: function(){

    },

    computed: {
  
    },

    methods: 
    {
        formatonumero: function(valor){
            return new Intl.NumberFormat("en-US", {style: "currency", currency: "USD"}).format(valor);
        },

        buscar: async function() {

            var fechaIni = document.getElementById('fechaInicial').value;
            var fechaFin = document.getElementById('fechaFinal').value;

            if (this.codigoEntrada != '' || (fechaIni != '' && fechaFin != '')) { 
                
                if (fechaIni <= fechaFin) {
                     
                    await axios.post(URL+'?action=buscar&consecutivo='+this.codigoEntrada+'&fechaIni='+fechaIni+'&fechaFin='+fechaFin)
                    .then((response) => {
                        
                        app.datos = response.data.datos;
                    }).catch((error) => {
                        this.error = true;
                        console.log(error)
                    }).finally(() => {

                        if (this.datos.length >= 1) {
                            this.existeInformacion = false;
                        }
                        else {
                            this.existeInformacion = true;
                        }
                    });     
                }
                else {
                    Swal.fire("Error", "Fecha inicial debe ser menor a fecha final", "warning");
                }
            }
            else {
                Swal.fire("Error", "Cantidad mayor de articulos debe ser mayor a cero", "warning");
            }
        },

        comprobarEstado: function(estado) {
            switch(estado){
                case 'S':
                    return 'Activo'
                break;

                case 'N':
                    return 'Anulado'
                break;

                case 'R':
                  return 'Reversado'
                break;
            }
        },


        eliminarEntrada: function(datos) {

            Swal.fire({
                icon: 'warning',
                title: 'Esta seguro que quiere eliminar?',
                showDenyButton: true,
                confirmButtonText: 'Eliminar!',
                denyButtonText: 'Cancelar',
                }).then((result) => {
                if (result.isConfirmed) {

                    var formData = new FormData();
                    formData.append("consecutivo", datos[0]);

                    var i = this.datos.indexOf( datos );
                    
                    if ( i !== -1 ) {
                        this.datos.splice( i, 1 );
                    }   

                    axios.post(URL + '?action=eliminar', formData)
                    .then((response) => {
                       
                        if(response.data.insertaBien){
                            Swal.fire({
                                title: "Eliminado!",
                                text: "La entrada por servicio ha sido eliminada!",
                                icon: "success"
                            });
                        }                       
                    });
                } 
            })  
        },

        seleccionaEntrada: function(entrada) {

            var x = "inve-visualizarEntradaServicio.php?cod="+entrada[0];
            window.open(x, '_blank');
            window.focus();
        },
    }
});