<?php

    use Illuminate\Contracts\Auth\Guard;

    require '../../../comun.inc';
    require '../../../funciones.inc';

    $linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");

    $out = array('error' => false);

    $action = "show";

    if(isset($_GET['action'])){
        $action = $_GET['action'];
    }


    if ($action == 'buscar') {

        $datos = array();

        if ($_GET['fechaIni'] != '' && $_GET['fechaFin'] != '') {

            preg_match("/([0-9]{2})\/([0-9]{2})\/([0-9]{4})/", $_GET["fechaIni"], $fi);
            $fechaIni = $fi[3]."-".$fi[2]."-".$fi[1];

            preg_match("/([0-9]{2})\/([0-9]{2})\/([0-9]{4})/", $_GET["fechaFin"], $ff);
            $fechaFin = $ff[3]."-".$ff[2]."-".$ff[1];
        } 

        $codigo = $_GET['consecutivo'];
        $condicion = "";

        if ($codigo != '' && $fechaIni != '') {
            $condicion = "WHERE consecutivo = $codigo AND fecha BETWEEN '$fechaIni' AND '$fechaFin'";
        }

        if ($codigo == '' && $fechaIni != '') {
            $condicion = "WHERE fecha BETWEEN '$fechaIni' AND '$fechaFin'";
        }

        if ($codigo != '' && $fechaIni == '') {
            $condicion = "WHERE consecutivo = $codigo";
        }

        $sqlEntrada = "SELECT consecutivo, fecha, descripcion, rp, valor, estado FROM alm_entrada_servicio_cab $condicion AND tipo_mov = '101' ORDER BY consecutivo DESC";
        $resEntrada = mysqli_query($linkbd, $sqlEntrada);
        while ($rowEntrada = mysqli_fetch_row($resEntrada)) {
            
            array_push($datos, $rowEntrada);
        }

        $out['datos'] = $datos;
    }

    if ($action == 'eliminar') {

        $consecutivo = $_POST['consecutivo'];

        $sqlCab = "DELETE FROM alm_entrada_servicio_cab WHERE consecutivo = $consecutivo";
        mysqli_query($linkbd, $sqlCab);

        $sqlDet = "DELETE FROM alm_entrada_servicio_det WHERE consecutivo = $consecutivo";
        mysqli_query($linkbd, $sqlDet);

        $sqlArticulos = "DELETE FROM alm_entrada_servicio_articulos WHERE consecutivo = $consecutivo";
        mysqli_query($linkbd, $sqlArticulos);

        $out['insertaBien'] = true;
    }

    header("Content-type: application/json");
    echo json_encode($out);
    die();