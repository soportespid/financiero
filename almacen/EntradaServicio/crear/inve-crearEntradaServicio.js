var app = new Vue({
    el: '#myapp',
	data:{

        error: '',
        loading: false,
        tabgroup1:'1',

        //cabecera
        consecutivo: '',
        descripcion: '',

        //gestion rp
        rp: '',
        fechaRP: '',
        descripcionRP: '',
        valorRP: '',
        saldoRP: '',
        showRP: false,
        cabeceraRP: [],
        searchRP : {keywordRP: ''},
        detallesRP: [],
        valorEntrada: [],
        posicionActual: '',
        tercero: '',
        certificaAlm: 1,

        //articulos
        detalleArticulo: [],
        formato: [
            {codigo: '', nombre: '', cantidad: '', valor: '', marca: '', modelo: '', serial: ''},
        ],
        cantidadArticulos: 0,
        codigoArticulos: [],
        nombreArticulos: [],
        cantidad: [],
        valorArticulos: [],
        marca: [],
        modelo: [],
        serial: [],
        detalleArticuloManual: [],

        articulos: [],
        showArticulos: false,
        searchArt : {keywordArt: ''},
        posicionArticulo: '',
    },

    mounted: function(){

        this.datosIniciales();
        this.traeArticulos();
    },

    methods: 
    {
        //Desglosar array
        toFormData: function(obj){
            var form_data = new FormData();
            for(var key in obj){
                form_data.append(key, obj[key]);
            }
            return form_data;
        },

        formatonumero: function(valor){
            return new Intl.NumberFormat("en-US", {style: "currency", currency: "USD"}).format(valor);
        },

        datosIniciales: async function() {
            
            await axios.post('almacen/EntradaServicio/crear/inve-crearEntradaServicio.php?action=datosIniciales')
            .then((response) => {
                
                app.consecutivo = response.data.consecutivo;
            });
        },

        datosRP: async function() {
            
            var fecha = document.getElementById('fecha').value;

            await axios.post('almacen/EntradaServicio/crear/inve-crearEntradaServicio.php?action=listadoRP&fecha='+fecha)
            .then((response) => {
                app.cabeceraRP = response.data.cabecera;
            }).catch((error) => {
                this.error = true;
                console.log(error)
            }).finally(() => {
        
            });
        },

        despliegaModalRP: function() {
          
            var fecha = document.getElementById('fecha').value;

            if (fecha != '') {
                this.datosRP();
                this.showRP = true;
            }
            else {
                Swal.fire(
                    'Error!',
                    'Selecciona la fecha primero.',
                    'error'
                );
            }
            
        },

        searchMonitorRP: async function() {

            var keywordRP = app.toFormData(this.searchRP);
            var fecha = document.getElementById('fecha').value;
            
            await axios.post('almacen/EntradaServicio/crear/inve-crearEntradaServicio.php?action=filtraRP&fecha='+fecha, keywordRP)
            .then((response) => {
                app.cabeceraRP = response.data.cabecera;
            }).catch((error) => {
                this.error = true;
                console.log(error)
            }).finally(() => {
        
            });  
        },

        seleccionaRP: async function(rp) {

            var fecha = document.getElementById('fecha').value;

            this.rp = '';
            this.descripcionRP = '';
            this.valorRP = '';
            this.saldoRP = '';
            this.fechaRP = '';

            this.rp = rp[0];
            this.descripcionRP = rp[2];
            this.valorRP = rp[3];
            this.saldoRP = rp[4];
            this.fechaRP = rp[1];
            this.tercero = rp[5];

            await axios.post('almacen/EntradaServicio/crear/inve-crearEntradaServicio.php?action=detalleRP&rp='+rp[0]+'&fecha='+fecha)
            .then((response) => {
                app.detallesRP = response.data.detalleRP;
            }).catch((error) => {
                this.error = true;
                console.log(error)
            }).finally(() => {
                this.showRP = false;
                app.llenadoValores();
            });
        },

        llenadoValores: function() {

            for (let i = 0; i < app.detallesRP.length; i++) {
                
                this.valorEntrada[i] = app.detallesRP[i][17];
            }
        },

        validaValorMax: function(posicion) {

            if (this.detallesRP[posicion][17] >= this.valorEntrada[posicion] && this.valorEntrada[posicion] >= 0) {
                
                
            }
            else {
                Swal.fire('Valor mayor al saldo disponible', '', 'info');
                this.valorEntrada[posicion] = this.detallesRP[posicion][17];
                this.valorEntrada = Object.values(this.valorEntrada);
            }
        },

        subirExcel: async function() {
            
            const input = document.getElementById("archivoExcel");
            await readXlsxFile(input.files[0]).then((rows) => {

                this.detalleArticulo = rows;
            })
            
            this.elimina();
        },

        elimina: function() {
            this.detalleArticulo.splice(0, 1);
        },

        bajarExcel: function() {

            const workbook = XLSX.utils.book_new();
            const worksheet = XLSX.utils.json_to_sheet(this.formato);
            XLSX.utils.book_append_sheet(workbook, worksheet, "formato");
            XLSX.writeFile(workbook, "formato.xlsx");
        },

        llenarCantidadArticulos: function() {

            if (parseInt(this.cantidadArticulos) >= 0) {
                if (this.cantidadArticulos < this.detalleArticuloManual.length) {
                    this.detalleArticuloManual = [];    
                }
                
                for (let i = 0; i < this.cantidadArticulos; i++) {
                    this.detalleArticuloManual[i] = 0;
                }
                this.detalleArticuloManual = Object.values(this.detalleArticuloManual);
            }
            else {
                Swal.fire("Error", "Cantidad mayor de articulos debe ser mayor a cero", "warning");
                this.cantidadArticulos = '';
                this.detalleArticuloManual = [];
            }
            
        },

        validaCantidad: function(index) {

            if(this.cantidad[index] <= 0) {
                Swal.fire("Error", "Cantidad menor a cero", "warning");
                this.cantidad[index] = '';
                this.cantidad = Object.values(this.cantidad);
            } 
        },

        validaValor: function(index) {

            if (this.valorArticulos[index] <= 0) {
                Swal.fire("Error", "Valor articulo menor a cero", "warning");
                this.valorArticulos[index] = '';
                this.valorArticulos = Object.values(this.valorArticulos);
            }
        },

        guardar: function() {

            var fecha = document.getElementById('fecha').value;

            if (this.detallesRP != '' && this.valorEntrada != '' && fecha != '' && this.descripcion != '') {

                Swal.fire({
                    icon: 'question',
                    title: 'Seguro que quieres guardar?',
                    showDenyButton: true,
                    confirmButtonText: 'Guardar',
                    denyButtonText: 'Cancelar',
                    }).then((result) => {
                    if (result.isConfirmed) 
                    {
                        if (result.isConfirmed) {
                            this.loading = true;
                            var formData = new FormData();
    
                            for(let i=0; i <= this.detallesRP.length-1; i++)
                            {
                                const val = Object.values(this.detallesRP[i]).length;

                                for(let x = 0; x <= val; x++) {

                                    formData.append("detallesRP["+i+"][]", Object.values(this.detallesRP[i])[x]);
                                }

                                formData.append('valorEntrada[]', this.valorEntrada[i]);    
                            }

                            for(let i=0; i <= this.detalleArticulo.length-1; i++)
                            {
                                const val = Object.values(this.detalleArticulo[i]).length;

                                for(let x = 0; x <= val; x++) {

                                    formData.append("detalleArticulo["+i+"][]", Object.values(this.detalleArticulo[i])[x]);
                                } 
                            }

                            for (let i = 0; i < this.codigoArticulos.length; i++) {
                                
                                formData.append('codigoArticulos[]', this.codigoArticulos[i]);   
                                formData.append('nombreArticulos[]', this.nombreArticulos[i]);   
                                formData.append('cantidad[]', this.cantidad[i]);   
                                formData.append('valorArticulos[]', this.valorArticulos[i]);   
                                formData.append('marca[]', this.marca[i]);   
                                formData.append('modelo[]', this.modelo[i]);   
                                formData.append('serial[]', this.serial[i]);   
                            }

                            formData.append("rp", this.rp);
                            formData.append("fecha", fecha);
                            formData.append("descripcion", this.descripcion);
                            formData.append("tercero", this.tercero);
                            formData.append("certificaAlm", this.certificaAlm);
                            
                            axios.post('almacen/EntradaServicio/crear/inve-crearEntradaServicio.php?action=guardar', formData)
                            .then((response) => {
                                // console.log(response.data);
                                this.loading = false;
                    
                                if(response.data.insertaBien){
                                    Swal.fire({
                                        icon: 'success',
                                        title: 'Se ha guardado con exito',
                                        showConfirmButton: false,
                                        timer: 1500
                                        }).then((response) => {
                                            app.redireccionar();
                                        });
                                }
                                else {
                                    Swal.fire(
                                        'Error!',
                                        'No se pudo guardar.',
                                        'error'
                                    );
                                }
                            });
                            
                        }
                    } 
                    else if (result.isDenied) 
                    {
                        Swal.fire('Guardar cancelado', '', 'info');
                    }
                })   
            }
            else {
                Swal.fire('Falta información por llenar', '', 'info');
            }
        },

        redireccionar: function()
        {
            location.href = "inve-crearEntradaServicio.php";
        },

        traeArticulos: async function() {

            await axios.post('almacen/EntradaServicio/visualizar/inve-visualizarEntradaServicio.php?action=articulos')
            .then((response) => {
                
                app.articulos = response.data.articulos;
            }).catch((error) => {
                this.error = true;
                console.log(error)
            }).finally(() => {
                
            });
        },

        despliegaArticulos: function(posicion) {

            this.showArticulos = true;
            this.posicionArticulo = posicion;
        },

        searchMonitoArticulo: async function() {

            var keywordArt = app.toFormData(this.searchArt);
            
            await axios.post('almacen/EntradaServicio/visualizar/inve-visualizarEntradaServicio.php?action=filtraArticulo', keywordArt)
            .then((response) => {
                //console.log(response.data);
                app.articulos = response.data.articulos;
            }).catch((error) => {
                this.error = true;
                console.log(error)
            }).finally(() => {
        
            });  
        },

        seleccionaArticulo: function(articulo) {

            this.codigoArticulos[this.posicionArticulo] = articulo[0];
            this.nombreArticulos[this.posicionArticulo] = articulo[1];
            this.posicionArticulo = '';
            this.showArticulos = false;
        },
    }
});