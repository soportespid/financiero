<?php

    require '../../../comun.inc';
    require '../../../funciones.inc';
    require '../../../funcionesSP.inc.php';

    session_start();
    $linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");

    $out = array('error' => false);

    $action = "show";

    if(isset($_GET['action'])){
        $action = $_GET['action'];
    }

    if ($action == 'datosIniciales') {

        $consecutivo = "";
        $consecutivo = selconsecutivo('alm_entrada_servicio_cab', 'consecutivo');
        $out['consecutivo'] = $consecutivo;
    }

    if ($action == 'listadoRP') {

        preg_match("/([0-9]{2})\/([0-9]{2})\/([0-9]{4})/", $_GET['fecha'], $fecha);
		$vigencia = vigencia_usuarios($_SESSION['cedulausu']); 
        $datosCabecera = array();

        $sqlDetalle = "SELECT idrp, SUM(valor), vigencia, LEFT(productoservicio, 1), destino_compra FROM ccpetdc_detalle WHERE vigencia='$vigencia' GROUP BY idrp";
		$resDetalle = mysqli_query($linkbd, $sqlDetalle);
        while ($rowDetalle = mysqli_fetch_row($resDetalle)) {
            
            if ($rowDetalle[4] == '01') {

                if ($rowDetalle[3] >= 5) {
                    
                    $sqlEntradaServicio = "SELECT SUM(valor) FROM alm_entrada_servicio_cab WHERE rp = $rowDetalle[0] AND fecha LIKE '%".$rowDetalle[2]."%' ";
                    $rowEntradaServicio = mysqli_fetch_row(mysqli_query($linkbd, $sqlEntradaServicio));
        
                    
                    if ($rowDetalle[1] >= $rowEntradaServicio[0]) {
        
                        $saldoDisponible = 0;
                        $datos = array();
        
                        $saldoDisponible = $rowDetalle[1] - $rowEntradaServicio[0];
        
                        $sqlrp="SELECT fecha,detalle,valor,vigencia,tercero FROM ccpetrp WHERE consvigencia='$rowDetalle[0]' and vigencia='$vigencia'";
                        $rowrp = mysqli_fetch_row(mysqli_query($linkbd, $sqlrp));
        
                        array_push($datos, $rowDetalle[0]);
                        array_push($datos, $rowrp[0]);
                        array_push($datos, $rowrp[1]);
                        array_push($datos, $rowrp[2]);
                        array_push($datos, $saldoDisponible);
                        array_push($datos, $rowrp[4]);
                        array_push($datosCabecera, $datos);
                    }    
                }
            }
            else {
                $sqlEntradaServicio = "SELECT SUM(valor) FROM alm_entrada_servicio_cab WHERE rp = $rowDetalle[0] AND fecha LIKE '%".$rowDetalle[2]."%' ";
                $rowEntradaServicio = mysqli_fetch_row(mysqli_query($linkbd, $sqlEntradaServicio));
    
                
                if ($rowDetalle[1] >= $rowEntradaServicio[0]) {
    
                    $saldoDisponible = 0;
                    $datos = array();
    
                    $saldoDisponible = $rowDetalle[1] - $rowEntradaServicio[0];
    
                    $sqlrp="SELECT fecha,detalle,valor,vigencia,tercero FROM ccpetrp WHERE consvigencia='$rowDetalle[0]' and vigencia='$vigencia'";
                    $rowrp = mysqli_fetch_row(mysqli_query($linkbd, $sqlrp));
    
                    array_push($datos, $rowDetalle[0]);
                    array_push($datos, $rowrp[0]);
                    array_push($datos, $rowrp[1]);
                    array_push($datos, $rowrp[2]);
                    array_push($datos, $saldoDisponible);
                    array_push($datos, $rowrp[4]);
                    array_push($datosCabecera, $datos);
                }    
            }

            

            // if ($rowDetalle[4] == '01') {
            //     if ($rowDetalle[3] >= 5) {

            //         $sqlEntradaServicio = "SELECT SUM(valor) FROM alm_entrada_servicio_cab WHERE rp = $rowDetalle[0] AND fecha LIKE '%".$rowDetalle[2]."%' ";
            //         $rowEntradaServicio = mysqli_fetch_row(mysqli_query($linkbd, $sqlEntradaServicio));
        
                    
            //         if ($rowDetalle[1] >= $rowEntradaServicio[0]) {
        
            //             $saldoDisponible = 0;
            //             $datos = array();
    
            //             $saldoDisponible = $rowDetalle[1] - $rowEntradaServicio[0];
        
            //             $sqlrp="SELECT fecha,detalle,valor,vigencia,tercero FROM ccpetrp WHERE consvigencia='$rowDetalle[0]' and vigencia='$vigusu'";
            //             $rowrp = mysqli_fetch_row(mysqli_query($linkbd, $sqlrp));
        
            //             array_push($datos, $rowDetalle[0]);
            //             array_push($datos, $rowrp[0]);
            //             array_push($datos, $rowrp[1]);
            //             array_push($datos, $rowrp[2]);
            //             array_push($datos, $saldoDisponible);
            //             array_push($datos, $rowrp[4]);
            //             array_push($datosCabecera, $datos);
            //         }    
            //     }
            // }
            // else {
            //     $sqlEntradaServicio = "SELECT SUM(valor) FROM alm_entrada_servicio_cab WHERE rp = $rowDetalle[0] AND fecha LIKE '%".$rowDetalle[2]."%' ";
            //     $rowEntradaServicio = mysqli_fetch_row(mysqli_query($linkbd, $sqlEntradaServicio));
    
                
            //     if ($rowDetalle[1] >= $rowEntradaServicio[0]) {
    
            //         $saldoDisponible = 0;
            //         $datos = array();

            //         $saldoDisponible = $rowDetalle[1] - $rowEntradaServicio[0];
    
            //         $sqlrp="SELECT fecha,detalle,valor,vigencia,tercero FROM ccpetrp WHERE consvigencia='$rowDetalle[0]' and vigencia='$vigusu'";
            //         $rowrp = mysqli_fetch_row(mysqli_query($linkbd, $sqlrp));
    
            //         array_push($datos, $rowDetalle[0]);
            //         array_push($datos, $rowrp[0]);
            //         array_push($datos, $rowrp[1]);
            //         array_push($datos, $rowrp[2]);
            //         array_push($datos, $saldoDisponible);
            //         array_push($datos, $rowrp[4]);
            //         array_push($datosCabecera, $datos);
            //     }    
            // }
        }

        $out['cabecera'] = $datosCabecera;
    }

    if ($action == 'filtraRP') {

        $keywordRP = $_POST['keywordRP'];
        preg_match("/([0-9]{2})\/([0-9]{2})\/([0-9]{4})/", $_GET['fecha'], $fecha);
		$vigencia = vigencia_usuarios($_SESSION['cedulausu']); 
        $datosCabecera = array();

        $sqlDetalle = "SELECT idrp, SUM(valor), vigencia, LEFT(productoservicio, 1), destino_compra FROM ccpetdc_detalle WHERE vigencia='$vigencia' AND CONCAT_WS(' ', idrp) LIKE '%$keywordRP%' GROUP BY idrp";
		$resDetalle = mysqli_query($linkbd, $sqlDetalle);
        while ($rowDetalle = mysqli_fetch_row($resDetalle)) {
            
            if ($rowDetalle[4] == '01') {
                if ($rowDetalle[3] >= 5) {

                    $sqlEntradaServicio = "SELECT SUM(valor) FROM alm_entrada_servicio_cab WHERE rp = $rowDetalle[0] AND fecha = LIKE '%".$rowDetalle[2]."%' ";
                    $rowEntradaServicio = mysqli_fetch_row(mysqli_query($linkbd, $sqlEntradaServicio));
        
                    
                    if ($rowDetalle[1] >= $rowEntradaServicio[0]) {
        
                        $saldoDisponible = 0;
                        $datos = array();
    
                        $saldoDisponible = $rowDetalle[1] - $rowEntradaServicio[0];
        
                        $sqlrp="SELECT fecha,detalle,valor,vigencia FROM ccpetrp WHERE consvigencia='$rowDetalle[0]' AND vigencia='$vigencia'";
                        $rowrp = mysqli_fetch_row(mysqli_query($linkbd, $sqlrp));
        
                        array_push($datos, $rowDetalle[0]);
                        array_push($datos, $rowrp[0]);
                        array_push($datos, $rowrp[1]);
                        array_push($datos, $rowrp[2]);
                        array_push($datos, $saldoDisponible);
                        array_push($datosCabecera, $datos);
                    }    
                }
            }
            else {
                $sqlEntradaServicio = "SELECT SUM(valor) FROM alm_entrada_servicio_cab WHERE rp = $rowDetalle[0] AND fecha = LIKE '%".$rowDetalle[2]."%' ";
                $rowEntradaServicio = mysqli_fetch_row(mysqli_query($linkbd, $sqlEntradaServicio));
    
                
                if ($rowDetalle[1] >= $rowEntradaServicio[0]) {
    
                    $saldoDisponible = 0;
                    $datos = array();

                    $saldoDisponible = $rowDetalle[1] - $rowEntradaServicio[0];
    
                    $sqlrp="SELECT fecha,detalle,valor,vigencia FROM ccpetrp WHERE consvigencia='$rowDetalle[0]' AND vigencia='$vigencia'";
                    $rowrp = mysqli_fetch_row(mysqli_query($linkbd, $sqlrp));
    
                    array_push($datos, $rowDetalle[0]);
                    array_push($datos, $rowrp[0]);
                    array_push($datos, $rowrp[1]);
                    array_push($datos, $rowrp[2]);
                    array_push($datos, $saldoDisponible);
                    array_push($datosCabecera, $datos);
                }   
            }
        }

        $out['cabecera'] = $datosCabecera;
    }

    if ($action == 'detalleRP') {

        $rp = $_GET['rp'];
        preg_match("/([0-9]{2})\/([0-9]{2})\/([0-9]{4})/", $_GET['fecha'], $fecha);
		$vigencia = vigencia_usuarios($_SESSION['cedulausu']); 
        $detalleRP = array();
        $maxVersion = ultimaVersionGastosCCPET();

        $sqlDetalleCDP = "SELECT cod_vigenciag, seccion_presupuestal, bpim, cuenta, fuente, productoservicio, indicador_producto, cod_politicap, medio_pago, valor, sector, cuenta_debito, destino_compra, LEFT(productoservicio, 1) FROM ccpetdc_detalle WHERE vigencia='$vigencia' AND idrp = $rp";
        $resDetalleCDP = mysqli_query($linkbd, $sqlDetalleCDP);
        while ($rowDetalleCDP = mysqli_fetch_row($resDetalleCDP)) {

            if ($rowDetalleCDP[12] == '01') {

                if ($rowDetalleCDP[13] >= 5) {

                    $datos = array();

                    $sqlVigenciaGasto = "SELECT nombre FROM ccpet_vigenciadelgasto WHERE codigo = $rowDetalleCDP[0]";
                    $rowVigenciaGasto = mysqli_fetch_row(mysqli_query($linkbd, $sqlVigenciaGasto));

                    $sqlSeccionPresupuestal = "SELECT nombre FROM pptoseccion_presupuestal WHERE id_seccion_presupuestal = $rowDetalleCDP[1]";
                    $rowSeccionPresupuestal = mysqli_fetch_row(mysqli_query($linkbd, $sqlSeccionPresupuestal));

                    $sqlCuentaCCPET = "SELECT nombre FROM cuentasccpet WHERE codigo = '$rowDetalleCDP[3]' AND version = '$maxVersion'";
                    $rowCuentaCCPET = mysqli_fetch_row(mysqli_query($linkbd, $sqlCuentaCCPET));

                    $cuenta = $rowDetalleCDP[3] . " - " . $rowCuentaCCPET[0];

                    $sqlFuenteCUIPO = "SELECT nombre FROM ccpet_fuentes_cuipo WHERE codigo_fuente = '$rowDetalleCDP[4]'";
                    $rowFuenteCUIPO = mysqli_fetch_row(mysqli_query($linkbd, $sqlFuenteCUIPO));

                    $fuente = $rowDetalleCDP[4] . " - " . $rowFuenteCUIPO[0];

                    $nomProductosres = buscaservicioccpetgastos($rowDetalleCDP[5]);
                    if($nomProductosres == ''){$nomProductosres = buscaproductoccpetgastos($rowDetalleCDP[5]);}

                    $nomProductosres = $rowDetalleCDP[5] . " - " . $nomProductosres;

                    $indicadorProducto = buscaindicadorccpet($rowDetalleCDP[6]);
                    $indicadorProducto = $rowDetalleCDP[6] . " - " . $indicadorProducto;

                    $sqlPoliticaPublica = "SELECT nombre FROM ccpet_politicapublica WHERE codigo = '$rowDetalleCDP[7]'";
                    $rowPoliticaPublica = mysqli_fetch_row(mysqli_query($linkbd, $sqlPoliticaPublica));

                    $politicaPublica = $rowDetalleCDP[7] . " - " . $rowPoliticaPublica[0];

                    $sqlEntradaServicioDet = "SELECT SUM(valor) FROM alm_entrada_servicio_det WHERE rp = $rp AND cuenta = '$rowDetalleCDP[3]' AND productoservicio = '$rowDetalleCDP[5]' AND fuente = '$rowDetalleCDP[4]' AND indicador_producto = '$rowDetalleCDP[6]' AND cod_politicap = '$rowDetalleCDP[7]' AND cod_vigenciag = '$rowDetalleCDP[0]' AND medio_pago = '$rowDetalleCDP[8]' AND sector = '$rowDetalleCDP[10]' AND seccion_presupuestal = '$rowDetalleCDP[1]' AND bpim = '$rowDetalleCDP[2]'";
                    $rowEntradaservicioDet = mysqli_fetch_row(mysqli_query($linkbd, $sqlEntradaServicioDet));

                    $saldo = $rowDetalleCDP[9] - $rowEntradaservicioDet[0];

                    array_push($datos, $rowDetalleCDP[0]);
                    array_push($datos, $rowVigenciaGasto[0]);
                    array_push($datos, $rowDetalleCDP[1]);
                    array_push($datos, $rowSeccionPresupuestal[0]);
                    array_push($datos, $rowDetalleCDP[2]);
                    array_push($datos, $rowDetalleCDP[3]);
                    array_push($datos, $cuenta);
                    array_push($datos, $rowDetalleCDP[4]);
                    array_push($datos, $fuente);
                    array_push($datos, $rowDetalleCDP[5]);
                    array_push($datos, $nomProductosres);
                    array_push($datos, $rowDetalleCDP[6]);
                    array_push($datos, $indicadorProducto);
                    array_push($datos, $rowDetalleCDP[7]);
                    array_push($datos, $politicaPublica);
                    array_push($datos, $rowDetalleCDP[8]);
                    array_push($datos, $rowDetalleCDP[9]);
                    array_push($datos, $saldo);
                    array_push($datos, $rowDetalleCDP[10]);
                    array_push($datos, $rowDetalleCDP[11]);
                    array_push($detalleRP, $datos);
                }
            }
            else {
                $datos = array();

                $sqlVigenciaGasto = "SELECT nombre FROM ccpet_vigenciadelgasto WHERE codigo = $rowDetalleCDP[0]";
                $rowVigenciaGasto = mysqli_fetch_row(mysqli_query($linkbd, $sqlVigenciaGasto));

                $sqlSeccionPresupuestal = "SELECT nombre FROM pptoseccion_presupuestal WHERE id_seccion_presupuestal = $rowDetalleCDP[1]";
                $rowSeccionPresupuestal = mysqli_fetch_row(mysqli_query($linkbd, $sqlSeccionPresupuestal));

                $sqlCuentaCCPET = "SELECT nombre FROM cuentasccpet WHERE codigo = '$rowDetalleCDP[3]' AND version = '$maxVersion'";
                $rowCuentaCCPET = mysqli_fetch_row(mysqli_query($linkbd, $sqlCuentaCCPET));

                $cuenta = $rowDetalleCDP[3] . " - " . $rowCuentaCCPET[0];

                $sqlFuenteCUIPO = "SELECT nombre FROM ccpet_fuentes_cuipo WHERE codigo_fuente = '$rowDetalleCDP[4]'";
                $rowFuenteCUIPO = mysqli_fetch_row(mysqli_query($linkbd, $sqlFuenteCUIPO));

                $fuente = $rowDetalleCDP[4] . " - " . $rowFuenteCUIPO[0];

                $nomProductosres = buscaservicioccpetgastos($rowDetalleCDP[5]);
                if($nomProductosres == ''){$nomProductosres = buscaproductoccpetgastos($rowDetalleCDP[5]);}

                $nomProductosres = $rowDetalleCDP[5] . " - " . $nomProductosres;

                $indicadorProducto = buscaindicadorccpet($rowDetalleCDP[6]);
                $indicadorProducto = $rowDetalleCDP[6] . " - " . $indicadorProducto;

                $sqlPoliticaPublica = "SELECT nombre FROM ccpet_politicapublica WHERE codigo = '$rowDetalleCDP[7]'";
                $rowPoliticaPublica = mysqli_fetch_row(mysqli_query($linkbd, $sqlPoliticaPublica));

                $politicaPublica = $rowDetalleCDP[7] . " - " . $rowPoliticaPublica[0];

                $sqlEntradaServicioDet = "SELECT SUM(valor) FROM alm_entrada_servicio_det WHERE rp = $rp AND cuenta = '$rowDetalleCDP[3]' AND productoservicio = '$rowDetalleCDP[5]' AND fuente = '$rowDetalleCDP[4]' AND indicador_producto = '$rowDetalleCDP[6]' AND cod_politicap = '$rowDetalleCDP[7]' AND cod_vigenciag = '$rowDetalleCDP[0]' AND medio_pago = '$rowDetalleCDP[8]' AND sector = '$rowDetalleCDP[10]' AND seccion_presupuestal = '$rowDetalleCDP[1]' AND bpim = '$rowDetalleCDP[2]'";
                $rowEntradaservicioDet = mysqli_fetch_row(mysqli_query($linkbd, $sqlEntradaServicioDet));

                $saldo = $rowDetalleCDP[9] - $rowEntradaservicioDet[0];

                array_push($datos, $rowDetalleCDP[0]);
                array_push($datos, $rowVigenciaGasto[0]);
                array_push($datos, $rowDetalleCDP[1]);
                array_push($datos, $rowSeccionPresupuestal[0]);
                array_push($datos, $rowDetalleCDP[2]);
                array_push($datos, $rowDetalleCDP[3]);
                array_push($datos, $cuenta);
                array_push($datos, $rowDetalleCDP[4]);
                array_push($datos, $fuente);
                array_push($datos, $rowDetalleCDP[5]);
                array_push($datos, $nomProductosres);
                array_push($datos, $rowDetalleCDP[6]);
                array_push($datos, $indicadorProducto);
                array_push($datos, $rowDetalleCDP[7]);
                array_push($datos, $politicaPublica);
                array_push($datos, $rowDetalleCDP[8]);
                array_push($datos, $rowDetalleCDP[9]);
                array_push($datos, $saldo);
                array_push($datos, $rowDetalleCDP[10]);
                array_push($datos, $rowDetalleCDP[11]);
                array_push($detalleRP, $datos);
            }
            
        }

        $out['detalleRP'] = $detalleRP;
    }

    if ($action == 'guardar') {
        
        preg_match("/([0-9]{2})\/([0-9]{2})\/([0-9]{4})/", $_POST["fecha"], $fecha);
        $fechaf = $fecha[3]."-".$fecha[2]."-".$fecha[1];

        $consecutivo = selconsecutivo('alm_entrada_servicio_cab', 'consecutivo');
        $vigusu = vigencia_usuarios($_SESSION['cedulausu']);

        $descripcion = strClean(replaceChar($_POST['descripcion']));
        $rp = $_POST['rp'];
        $user = $_SESSION['cedulausu'];
        $tercero = $_POST['tercero'];
        $certificaAlm = $_POST["certificaAlm"];
        $sumaTotal = 0;

        for ($i=0; $i < count($_POST["valorEntrada"]); $i++) { 
            
            $sumaTotal += $_POST['valorEntrada'][$i];
        }

        $sqlEntradaServicioCab = "INSERT INTO alm_entrada_servicio_cab (consecutivo, fecha, descripcion, tipo_mov, rp, valor, estado, user, certifica_alm) VALUES ($consecutivo, '$fechaf', '$descripcion', '101', $rp, $sumaTotal, 'S', '$user', $certificaAlm)";
        mysqli_query($linkbd, $sqlEntradaServicioCab);

        for ($i=0; $i < count($_POST['detallesRP']); $i++) { 
            
            $cuenta = $_POST['detallesRP'][$i][5];
            $producto_servicio = $_POST['detallesRP'][$i][9];
            $fuente = $_POST['detallesRP'][$i][7];
            $indicador_producto = $_POST['detallesRP'][$i][11];
            $politica_publica =  $_POST['detallesRP'][$i][13];
            $vigencia_gasto = $_POST['detallesRP'][$i][0];
            $medio_pago = $_POST['detallesRP'][$i][15];
            $sector = $_POST['detallesRP'][$i][18];
            $seccion_presupuestal = $_POST['detallesRP'][$i][2];
            $bpim = $_POST['detallesRP'][$i][4];
            $valor = $_POST['valorEntrada'][$i];
            $cuenta_credito = $_POST['detallesRP'][$i][19];
        
            $sqlCentroCostos = "SELECT id_cc FROM centrocostos_seccionpresupuestal WHERE id_sp = '$seccion_presupuestal'";
            $rowCentroCostos = mysqli_fetch_row(mysqli_query($linkbd, $sqlCentroCostos));

            $cc = $rowCentroCostos[0];

            $consecutivoDet = selconsecutivo('alm_entrada_servicio_det', 'id');

            $sqlEntradaServicioDet = "INSERT INTO alm_entrada_servicio_det VALUES ($consecutivoDet, $consecutivo, $rp, '$cuenta', '$producto_servicio', '$fuente', '$indicador_producto', '$politica_publica', '$vigencia_gasto', '$medio_pago', '$sector', '$seccion_presupuestal', '$bpim', $valor, '', '')";
            mysqli_query($linkbd, $sqlEntradaServicioDet);            
        }

        for ($i=0; $i < count($_POST['detalleArticulo']); $i++) { 
            
            $codigo = $_POST['detalleArticulo'][$i][0];
            $nombre = $_POST['detalleArticulo'][$i][1];
            $cantidad = $_POST['detalleArticulo'][$i][2];
            $valor = $_POST['detalleArticulo'][$i][3];
            $marca = $_POST['detalleArticulo'][$i][4];
            $modelo = $_POST['detalleArticulo'][$i][5];
            $serial = $_POST['detalleArticulo'][$i][6];

            if ($codigo != '' && $nombre != '' && $cantidad != '' && $valor != '') {

                $consecutivoDet3 = selconsecutivo('alm_entrada_servicio_articulos', 'id');

                $sqlArticulos = "INSERT INTO alm_entrada_servicio_articulos VALUES ($consecutivoDet3, $consecutivo, '$codigo', '$nombre', $cantidad, $valor, '$marca', '$modelo', '$serial')";
                mysqli_query($linkbd, $sqlArticulos);
            }
        }

        for ($i=0; $i < count($_POST['codigoArticulos']); $i++) { 
            
            $codigo = $_POST['codigoArticulos'][$i];
            $nombre = $_POST['nombreArticulos'][$i];
            $cantidad = $_POST['cantidad'][$i];
            $valor = $_POST['valorArticulos'][$i];
            $marca = $_POST['marca'][$i];
            $modelo = $_POST['modelo'][$i];
            $serial = $_POST['serial'][$i];

            if ($codigo != '' && $nombre != '' && $cantidad != '' && $valor != '') {

                $consecutivoDet3 = selconsecutivo('alm_entrada_servicio_articulos', 'id');
                $sqlArticulos = "INSERT INTO alm_entrada_servicio_articulos VALUES ($consecutivoDet3, $consecutivo, '$codigo', '$nombre', $cantidad, $valor, '$marca', '$modelo', '$serial')";
                mysqli_query($linkbd, $sqlArticulos);
            }
        }

        $out['insertaBien'] = true;
    }

    header("Content-type: application/json");
    echo json_encode($out);
    die();