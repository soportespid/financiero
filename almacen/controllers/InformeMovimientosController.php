<?php
    require_once '../../Librerias/core/Helpers.php';
    require_once '../../PHPExcel/Classes/PHPExcel.php';
    require_once '../models/InformeMovimientosModel.php';
    session_start();
    
    class InformeMovimientosController extends InformeMovimientosModel{

        public function getInitialData(){
            if(!empty($_SESSION)){
                $fecha_inicial = $_POST["fecha_ini"];
                $fecha_final = $_POST["fecha_fin"];
                $arrResponse = array(
                    "movimientos" => $this->getAllMovimientos($fecha_inicial, $fecha_final)
                );
        
                echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
            }
            die();
        }
    }

    if($_POST){
        $obj = new InformeMovimientosController();
        if($_POST['action'] == "getData"){
            $obj->getInitialData();
        }
    }

?>
