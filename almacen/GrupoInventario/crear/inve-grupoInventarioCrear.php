<?php
    require_once '../../../comun.inc';
    require '../../../funciones.inc';
    require '../../../funcionesSP.inc.php';

    if($_POST){
        $obj = new Plantilla();
        if($_POST['action']=="get"){
            $obj->getData();
        }else if($_POST['action']=="save"){
            $obj->save($_POST['data']);
        }
    }
    class Plantilla{
        private $linkbd;
        private $strGrupoCodigo;
        private $strCodigoArticulo;
        private $strCodigoBien;
        private $strUnidadMedida;
        private $strNombre;
        private $chrEstado;
        public function __construct() {
            $this->linkbd = conectar_v7();
            $this->linkbd->set_charset("utf8");
        }
        public function getData (){
            if(!empty($_SESSION)){
                $request['conceptos'] = $this->selectConceptos();
                $request['consecutivo'] = $this->selectConsecutivo();
                echo json_encode($request,JSON_UNESCAPED_UNICODE);
            }
            die();
        }
        public function save($data){
            if(!empty($_SESSION)){
                if($data !="" ){
                    $arrData = json_decode($data,true);
                    $request = $this->insertGrupo($arrData);
                    if(is_numeric($request) && $request>0){
                        $arrResponse = array("status"=>true,"msg"=>"Grupo de inventario guardado.");
                    }else{
                        $arrResponse = array("status"=>false,"msg"=>"No se pudo guardar, intentelo de nuevo.");
                    }
                }else{
                    $arrResponse = array("status"=>false,"msg"=>"Error de datos");
                }
                echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
            }
            die();
        }
        public function insertGrupo(array $arrData){
            $arrData['nombre'] = strtoupper($arrData['nombre']);
            $sql="INSERT INTO almgrupoinv (codigo,nombre,estado,concepent) VALUES ('$arrData[codigo]','$arrData[nombre]','$arrData[estado]', '$arrData[concepto]')";
            $request = intval(mysqli_query($this->linkbd,$sql));
            return $request;
        }
        public function selectConsecutivo(){
            $sql="SELECT MAX(CONVERT(codigo, SIGNED INTEGER)) as codigo FROM almgrupoinv";
            $request = mysqli_query($this->linkbd,$sql)->fetch_assoc()['codigo']+1;
            return $request;
        }
        public function selectConceptos(){
            $sql ="SELECT * FROM conceptoscontables WHERE modulo='5' AND tipo='AE' ORDER BY codigo";
            $request = mysqli_fetch_all(mysqli_query($this->linkbd,$sql),MYSQLI_ASSOC);
            return $request;
        }

    }
?>
