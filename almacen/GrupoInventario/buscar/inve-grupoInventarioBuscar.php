<?php
    require_once '../../../comun.inc';
    require '../../../funciones.inc';
    require '../../../funcionesSP.inc.php';

    if($_POST){
        $obj = new Articulo();
        if($_POST['action']=="search"){
            $currentPage = intval($_POST['page']);
            $obj->search($_POST['search'],$currentPage);
        }
    }

    class Articulo{
        private $linkbd;
        private $strCodigoArticulo;
        private $strCodigoGrupo;
        private $strCodigoBien;
        public function __construct() {
            $this->linkbd = conectar_v7();
            $this->linkbd->set_charset("utf8");
        }
        public function search(string $search,int $currentPage){
            $perPage = 200;
            $startRows = ($currentPage-1) * $perPage;
            $totalRows = 0;

            $sql = "SELECT codigo,nombre,estado,concepent
            FROM almgrupoinv
            WHERE (nombre LIKE '$search%' OR codigo LIKE '$search%') AND estado ='S' ORDER BY codigo DESC LIMIT $startRows, $perPage";
            $request = mysqli_fetch_all(mysqli_query($this->linkbd,$sql),MYSQLI_ASSOC);
            $totalRows = intval(mysqli_query($this->linkbd,
                "SELECT count(*) as total
                FROM almgrupoinv
            WHERE (nombre LIKE '$search%' OR codigo LIKE '$search%') AND estado ='S' ORDER BY codigo DESC LIMIT $startRows, $perPage")->fetch_assoc()['total']);
            $totalPages = $totalRows > 0 ? ceil($totalRows/$perPage) : 1;
            $arrResponse = array("status"=>true,"data"=>$request,"total"=>$totalRows,"total_pages"=>$totalPages);
            echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
            die();
        }
    }
?>
