const URL ='almacen/GrupoInventario/editar/inve-grupoInventarioEditar.php';

var app = new Vue({
    el:"#myapp",
    data() {
        return {
            isLoading:false,
            strNombre:"",
            intConsecutivo:0,
            selectEstado:"S",
            selectConcepto:"01",
            arrConceptos:[]
        }
    },
    mounted() {
        this.getData();
    },
    methods: {
        getData: async function(){
            this.intConsecutivo = new URLSearchParams(window.location.search).get('id');
            if(this.intConsecutivo > 0){
                const formData = new FormData();
                formData.append("action","get");
                formData.append("codigo",this.intConsecutivo);
                const response = await fetch(URL,{method:"POST",body:formData});
                const objData = await response.json();
                this.arrConceptos = objData.conceptos;
                this.intConsecutivo = objData.data.codigo;
                this.strNombre = objData.data.nombre;
                this.selectEstado = objData.data.estado;
                this.selectConcepto = objData.data.concepent;
            }else{
                window.location.href="inve-grupoInventarioBuscar.php";
            }
        },
        save: async function(){
            if(this.strNombre == "" || this.selectConcepto ==""){
                Swal.fire("Error","Todos los campos son obligatorios, intente de nuevo.","error");
                return false;
            }
            const arrData = {
                "codigo":this.intConsecutivo,
                "nombre":this.strNombre,
                "concepto":this.selectConcepto,
                "estado":this.selectEstado
            }
            this.isLoading = true;
            let formData = new FormData();
            formData.append("action","save");
            formData.append("data",JSON.stringify(arrData));
            const response = await fetch(URL,{method:"POST",body:formData});
            const objData = await response.json();
            if(objData.status){
                this.strNombre = "";
                this.getData();
                Swal.fire("Guardado",objData.msg,"success");
            }else{
                Swal.fire("Error",objData.msg,"error");
            }
            this.isLoading = false;
        }
    },
})
