<?php
    require_once '../../../comun.inc';
    require '../../../funciones.inc';
    require '../../../funcionesSP.inc.php';

    if($_POST){
        $obj = new Plantilla();
        if($_POST['action']=="get"){
            $obj->getData($_POST['codigo']);
        }else if($_POST['action']=="save"){
            $obj->save($_POST['data']);
        }
    }
    class Plantilla{
        private $linkbd;
        private $strGrupoCodigo;
        private $strCodigoArticulo;
        private $strCodigoBien;
        private $strUnidadMedida;
        private $strNombre;
        private $chrEstado;
        public function __construct() {
            $this->linkbd = conectar_v7();
            $this->linkbd->set_charset("utf8");
        }
        public function getData ($codigo){
            if(!empty($_SESSION)){
                $request['conceptos'] = $this->selectConceptos();
                $request['data'] = $this->selectData($codigo);
                echo json_encode($request,JSON_UNESCAPED_UNICODE);
            }
            die();
        }
        public function save($data){
            if(!empty($_SESSION)){
                if($data !="" ){
                    $arrData = json_decode($data,true);
                    $request = $this->updateGrupo($arrData);
                    if(is_numeric($request) && $request>0){
                        $arrResponse = array("status"=>true,"msg"=>"Grupo de inventario actualizado.");
                    }else{
                        $arrResponse = array("status"=>false,"msg"=>"No se pudo actualizar, intentelo de nuevo.");
                    }
                }else{
                    $arrResponse = array("status"=>false,"msg"=>"Error de datos");
                }
                echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
            }
            die();
        }
        public function selectData($codigo){
            $sql = "SELECT codigo,nombre,concepent,estado FROM almgrupoinv WHERE codigo = '$codigo'";
            $request = mysqli_query($this->linkbd,$sql)->fetch_assoc();
            return $request;
        }
        public function updateGrupo(array $arrData){
            $arrData['nombre'] = strtoupper($arrData['nombre']);
            $sql="UPDATE almgrupoinv SET nombre='$arrData[nombre]',estado = '$arrData[estado]',concepent='$arrData[concepto]'
            WHERE codigo='$arrData[codigo]'";
            $request = intval(mysqli_query($this->linkbd,$sql));
            return $request;
        }
        public function selectConceptos(){
            $sql ="SELECT * FROM conceptoscontables WHERE modulo='5' AND tipo='AE' ORDER BY codigo";
            $request = mysqli_fetch_all(mysqli_query($this->linkbd,$sql),MYSQLI_ASSOC);
            return $request;
        }

    }
?>
