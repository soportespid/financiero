<?php
    require_once '../../../comun.inc';
    require '../../../funciones.inc';
    require '../../../funcionesSP.inc.php';

    if($_POST){
        $obj = new Articulo();
        if($_POST['action']=="search"){
            $currentPage = intval($_POST['page']); 
            $obj->search($_POST['search'],$currentPage);
        }
    }

    class Articulo{
        private $linkbd;
        private $strCodigoArticulo;
        private $strCodigoGrupo;
        private $strCodigoBien;
        public function __construct() {
            $this->linkbd = conectar_v7();
            $this->linkbd->set_charset("utf8");
        }
        public function search(string $search,int $currentPage){
            $perPage = 200;
            $startRows = ($currentPage-1) * $perPage;
            $totalRows = 0;

            $sql = "SELECT codigo,nombre,codunspsc,grupoinven,estado, CONCAT(grupoinven,codigo) as codigo_articulo
            FROM almarticulos 
            WHERE nombre like '$search%' OR CONCAT(grupoinven,codigo) like '$search%' 
            ORDER BY length(codigo),codigo LIMIT $startRows, $perPage";
            $request = mysqli_fetch_all(mysqli_query($this->linkbd,$sql),MYSQLI_ASSOC);
            $totalRows = intval(mysqli_query($this->linkbd,
                "SELECT count(*) as total 
                FROM almarticulos 
                WHERE nombre like '$search%' OR CONCAT(grupoinven,codigo) like '$search%' ")->fetch_assoc()['total']);
            if(!empty($request)){
                $total = count($request);   
                for ($i=0; $i < $total ; $i++) { 
                    $this->strCodigoGrupo = $request[$i]['grupoinven'];
                    $this->strCodigoArticulo = $this->strCodigoGrupo.$request[$i]['codigo'];
                    $this->strCodigoBien = $request[$i]['codunspsc'];
                    $sql = "SELECT unidad FROM almarticulos_det WHERE articulo = '$this->strCodigoArticulo'";
                    $request[$i]['unidad'] = mysqli_query($this->linkbd,$sql)->fetch_assoc()['unidad'];
                    $request[$i]['grupoinven'] = $this->strCodigoGrupo." - ".mysqli_query(
                        $this->linkbd,"SELECT nombre FROM almgrupoinv WHERE codigo ='$this->strCodigoGrupo'"
                    )->fetch_assoc()['nombre'];
                    $request[$i]['codunspsc'] = $this->strCodigoBien." - ".mysqli_query(
                        $this->linkbd,
                        "SELECT titulo FROM ccpetbienestransportables 
                        WHERE version = (SELECT MAX(version) FROM ccpetbienestransportables)  AND LENGTH(grupo) = 7 AND grupo ='$this->strCodigoBien'"
                    )->fetch_assoc()['titulo'];
                }
            }
            $totalPages = $totalRows > 0 ? ceil($totalRows/$perPage) : 1;
            $arrResponse = array("status"=>true,"data"=>$request,"total"=>$totalRows,"total_pages"=>$totalPages);
            echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
            die();
        }
    }
?>