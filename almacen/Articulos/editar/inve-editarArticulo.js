const URL ='almacen/Articulos/editar/inve-editarArticulo.php';

var app = new Vue({
    el:"#myapp",
    data() {
        return {
            isLoading:false,
            isModalBienes:false,
            arrGrupos:[],
            arrUnidades:[],
            arrModalBienes:[],
            intSelectGrupo:0,
            intIdCodigo:0,
            strCodigo:"",
            strBuscarBienCodigo:"",
            strBuscarBienModal:"",
            strBienTitulo:"",
            strNombre:"",
            strUnidad:0,
            objBien:{"grupo":"","titulo":""}
        }
    },
    mounted() {
        this.getData();
    },
    methods: {
        getData: async function(){
            this.intIdCodigo = new URLSearchParams(window.location.search).get('id');
            if(this.intIdCodigo > 0){
                const formData = new FormData();
                formData.append("action","get");
                formData.append("codigo",this.intIdCodigo);
                const response = await fetch(URL,{method:"POST",body:formData});
                const objData = await response.json();
                console.log(objData);
                if(objData.status){
                    const data = objData.data; 
                    const articulo = data.articulo;
                    this.arrGrupos = data.grupos;
                    this.arrUnidades = data.unidades;
                    this.arrModalBienes = data.bienes;
                    this.intSelectGrupo = articulo.grupoinven;
                    this.strUnidad = articulo.unidad;
                    this.strCodigo = articulo.codigo;
                    this.strNombre = articulo.nombre
                    this.strUnidad = articulo.unidad;
                    this.strBuscarBienCodigo = articulo.codunspsc;
                    this.objBien.titulo = articulo.nombre_cpc;
                }else{
                    window.location.href="inve-buscarArticulos.php";
                }
                
            }else{
                window.location.href="inve-buscarArticulos.php";
            }
        },
        search: async function(option){
            let search = "";
            
            if(option=="cod_bien") search = this.strBuscarBienCodigo;
            if(option=="clasificacion") search = this.strBuscarBienModal;

            this.isLoading = true;
            let formData = new FormData()
            formData.append("type",this.intCodeType);
            formData.append("search",search);
            formData.append("action","search");
            formData.append("option",option);
            
            const response = await fetch(URL,{method:'POST',body:formData});
            const objData = await response.json();
            if(objData.status){
                if(option=="cod_bien") this.objBien = objData.data;
                if(option=="clasificacion") this.arrModalBienes = objData.data;
            }else{
                this.arrModalBienes = [];
            }
            this.isLoading = false;
        },
        selectItem: async function(item){
            this.objBien = item;
            this.strBuscarBienCodigo = this.objBien.grupo;
            this.strBienTitulo = this.objBien.titulo;
        },
        save: async function(){
            this.intIdCodigo = new URLSearchParams(window.location.search).get('id');
            if(this.intIdCodigo > 0){
                if(this.strBuscarBienCodigo =="" || this.objBien.titulo == "" || this.strNombre =="" || this.strMedida==""){
                    Swal.fire("Error","Todos los campos son obligatorios, inténte de nuevo.","error");
                    return false;
                }
                this.isLoading = true;
                let formData = new FormData();
                formData.append("cod_bien",this.strBuscarBienCodigo);
                formData.append("cod_articulo",this.intIdCodigo);
                formData.append("nombre",this.strNombre);
                formData.append("unidad",this.strUnidad);
                formData.append("action","save");
    
                const response = await fetch(URL,{method:"POST",body:formData});
                const objData = await response.json();
                if(objData.status){
                    Swal.fire("Actualizado",objData.msg,"success");
                }else{
                    Swal.fire("Error",objData.msg,"error");
                }
                this.isLoading = false;
            }else{
                window.location.href="inve-buscarArticulos.php";
            }
        }
    },
})