<?php
    require_once '../../../comun.inc';
    require '../../../funciones.inc';
    require '../../../funcionesSP.inc.php';
    if($_POST){
        $obj = new Articulo();
        if($_POST['action']=="get"){
            $obj->getData($_POST['codigo']);
        }elseif($_POST['action'] == 'search'){
            $obj->search($_POST['option'],$_POST['search']);
        }elseif($_POST['action']=="save"){
            $obj->save(
                $_POST['cod_articulo'],
                $_POST['nombre'],
                $_POST['cod_bien'],
                $_POST['unidad']
            );
        }
    }

    class Articulo{
        private $linkbd;
        private $strGrupoCodigo;
        private $strCodigoArticulo;
        private $strCodigoBien;
        private $strUnidadMedida;
        private $strNombre;
        private $chrEstado;
        public function __construct() {
            $this->linkbd = conectar_v7();
            $this->linkbd->set_charset("utf8");
        }
        public function save(string $codigo,string $nombre, string $codunspsc,string $medida){
            //dep($_POST);exit;
            if(empty($nombre) || empty($codunspsc)){
                $arrResponse = array("status"=>false,"msg"=>"Error de datos");
            }else{
                $this->strCodigoArticulo = $codigo;
                $this->strNombre = $nombre;
                $this->strCodigoBien = $codunspsc;
                $this->strUnidadMedida = $medida;
                $request = mysqli_query($this->linkbd,"SELECT * FROM almarticulos WHERE CONCAT(grupoinven,codigo) = $this->strCodigoArticulo")->fetch_assoc();
                if(!empty($request)){
                    $sql="UPDATE almarticulos SET nombre='$this->strNombre',codunspsc='$this->strCodigoBien' 
                    WHERE CONCAT(grupoinven,codigo) = $this->strCodigoArticulo";
                    $request = mysqli_query($this->linkbd,$sql);
                    if($request > 0){
                        $request = mysqli_query($this->linkbd,"SELECT articulo FROM almarticulos_det WHERE articulo = '$this->strCodigoArticulo'")->fetch_assoc();
                        if(!empty($request)){
                            $request = mysqli_query($this->linkbd,"UPDATE almarticulos_det SET unidad='$this->strUnidadMedida' 
                            WHERE articulo = '$this->strCodigoArticulo'");
                        }else{
                            $strConsecutivo=selconsecutivo('almarticulos_det','id_det');
                            $sql="INSERT INTO almarticulos_det (id_det,articulo,unidad,factor,principal) 
                            VALUES ('$strConsecutivo','$this->strCodigoArticulo','$this->strUnidadMedida', 1, 1)";
                            
                            $request = mysqli_query($this->linkbd,$sql);
                        }
                        $arrResponse = array("status"=>true,"msg"=>"Datos actualizados");
                    }else{
                        $arrResponse = array("status"=>false,"msg"=>"No se ha podido guardar, intente de nuevo");
                    }
                }else{
                    $arrResponse = array("status"=>false,"msg"=>"Error de datos");
                }
            }
            echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
            die();
        }
        public function getData ($strCodigoArticulo){
            $this->strCodigoArticulo = $strCodigoArticulo;
            $request['articulo'] = mysqli_query(
                $this->linkbd,
                "SELECT codigo,nombre,codunspsc,grupoinven,estado FROM almarticulos WHERE CONCAT(grupoinven,codigo) = $this->strCodigoArticulo"
            )->fetch_assoc();
            if(!empty($request['articulo'])){
                $this->strCodigoBien = $request['articulo']['codunspsc'];

                $request['articulo']['unidad'] = mysqli_query(
                    $this->linkbd,
                    "SELECT unidad FROM almarticulos_det WHERE articulo = '$this->strCodigoArticulo'"
                )->fetch_assoc()['unidad'];
                $request['articulo']['nombre_cpc'] = mysqli_query(
                    $this->linkbd,
                    "SELECT titulo FROM ccpetbienestransportables 
                    WHERE grupo ='$this->strCodigoBien'"
                )->fetch_assoc()['titulo'];
                $request['grupos'] = mysqli_fetch_all(mysqli_query($this->linkbd,"SELECT * FROM almgrupoinv WHERE estado='S' ORDER BY codigo"),MYSQLI_ASSOC);
                $request['unidades'] = mysqli_fetch_all(mysqli_query($this->linkbd,"SELECT * FROM dominios WHERE nombre_dominio='unidad_medida' AND tipo='S' ORDER BY descripcion_valor"),MYSQLI_ASSOC);
                $request['bienes'] = mysqli_fetch_all(mysqli_query($this->linkbd,
                        "SELECT grupo, titulo 
                        FROM ccpetbienestransportables 
                        WHERE version = (SELECT MAX(version) FROM ccpetbienestransportables)  AND LENGTH(grupo) = 7 ORDER BY grupo ASC"),MYSQLI_ASSOC);
                $arrResponse = array("status"=>true,"data"=>$request);
            }else{
                $arrResponse = array("status"=>false);
            }
            echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
            die();
        }
        public function search(string $option,string $search){
            if($search!=""){
                if($option == "clasificacion"){
                    $request = mysqli_fetch_all(mysqli_query($this->linkbd,
                    "SELECT grupo, titulo 
                    FROM ccpetbienestransportables 
                    WHERE version = (SELECT MAX(version) FROM ccpetbienestransportables)  AND LENGTH(grupo) = 7 AND (grupo LIKE '$search%' OR titulo LIKE '%$search%') 
                    ORDER BY grupo ASC"),MYSQLI_ASSOC);
                }else if($option=="cod_bien"){
                    $request = mysqli_query($this->linkbd,
                    "SELECT grupo, titulo 
                    FROM ccpetbienestransportables 
                    WHERE version = (SELECT MAX(version) FROM ccpetbienestransportables) AND LENGTH(grupo) = 7 AND grupo='$search'"
                    )->fetch_assoc();
                }
                if(!empty($request)){
                    $arrResponse = array("status"=>true,"msg"=>"Datos encontrados","data"=>$request);
                }else{
                    $arrResponse = array("status"=>false,"msg"=>"Datos no encontrados");
                }
            }else{
                $arrResponse = array("status"=>false,"msg"=>"Error de datos");
            }
            echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
            die();
        }
    }
?>