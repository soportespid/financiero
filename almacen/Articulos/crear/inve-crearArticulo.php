<?php
    require_once '../../../comun.inc';
    require '../../../funciones.inc';
    require '../../../funcionesSP.inc.php';

    if($_POST){
        $obj = new Articulo();
        if($_POST['action']=="grupo_inventario"){
            $obj->getData();
        }elseif($_POST['action']=="codigo"){
            $obj->getCodigo($_POST['codigo']);
        }elseif($_POST['action'] == 'search'){
            $obj->search($_POST['option'],$_POST['search']);
        }elseif($_POST['action']=="save"){
            $obj->save(
                $_POST['cod_articulo'],
                $_POST['nombre'],
                $_POST['cod_bien'],
                $_POST['cod_grupo'],
                "S",
                $_POST['unidad']
            );
        }
    }

    class Articulo{
        private $linkbd;
        private $strGrupoCodigo;
        private $strCodigoArticulo;
        private $strCodigoBien;
        private $strUnidadMedida;
        private $strNombre;
        private $chrEstado;
        public function __construct() {
            $this->linkbd = conectar_v7();
            $this->linkbd->set_charset("utf8");
        }
        public function save(string $codigo,string $nombre, string $codunspsc,string $grupoinven,string $estado,string $medida){
            if(empty($codigo) || empty($nombre) || empty($grupoinven) || empty($estado) || empty($estado)){
                $arrResponse = array("status"=>false,"msg"=>"Error de datos");
            }else{

                $this->strCodigoArticulo = $codigo;
                $this->strNombre = $nombre;
                $this->strCodigoBien = $codunspsc;
                $this->strGrupoCodigo = $grupoinven;
                $this->chrEstado = $estado;
                $this->strUnidadMedida = $medida;
                $sql="INSERT INTO almarticulos (codigo,nombre,codunspsc,grupoinven,estado)
                VALUES ('$this->strCodigoArticulo','$this->strNombre','$this->strCodigoBien','$this->strGrupoCodigo','$this->chrEstado')";
                $request = mysqli_query($this->linkbd,$sql);
                if($request > 0){
                    $strCodigo = $this->strGrupoCodigo.$this->strCodigoArticulo;;
                    $strConsecutivo=selconsecutivo('almarticulos_det','id_det');
                    $sql="INSERT INTO almarticulos_det (id_det,articulo,unidad,factor,principal)  
                    VALUES ('$strConsecutivo','$strCodigo','$this->strUnidadMedida', 1, 1)";
                    $request = mysqli_query($this->linkbd,$sql);
                    $arrResponse = array("status"=>true,"msg"=>"Datos guardados");
                }else{
                    $arrResponse = array("status"=>false,"msg"=>"No se ha podido guardar, intente de nuevo");
                }
            }
            echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
            die();
        }
        public function getData (){
            $request['grupos'] = mysqli_fetch_all(mysqli_query($this->linkbd,"SELECT * FROM almgrupoinv WHERE estado='S' ORDER BY codigo"),MYSQLI_ASSOC);
            $request['unidades'] = mysqli_fetch_all(mysqli_query($this->linkbd,"SELECT * FROM dominios WHERE nombre_dominio='unidad_medida' AND tipo='S' ORDER BY descripcion_valor"),MYSQLI_ASSOC);
            $request['bienes'] = mysqli_fetch_all(mysqli_query($this->linkbd,
                    "SELECT grupo, titulo 
                    FROM ccpetbienestransportables 
                    WHERE version = (SELECT MAX(version) FROM ccpetbienestransportables)  AND LENGTH(grupo) = 7 ORDER BY grupo ASC"),MYSQLI_ASSOC);
            echo json_encode($request,JSON_UNESCAPED_UNICODE);
            die();
        }
        public function getCodigo(string $strCodigo){
            $this->strGrupoCodigo = $strCodigo;
            $request = mysqli_query(
                $this->linkbd,
                "SELECT MAX(CONVERT(codigo, SIGNED INTEGER)) FROM almarticulos WHERE grupoinven='$this->strGrupoCodigo'")->fetch_all()[0];
            $codigo='0000'.($request[0]+1);
            $longitud=strlen($codigo);
            $this->strCodigoArticulo = substr($codigo,$longitud-5,$longitud);
            echo json_encode($this->strCodigoArticulo,JSON_UNESCAPED_UNICODE);
            die();
        }
        public function search(string $option,string $search){
            if($search!=""){
                if($option == "clasificacion"){
                    $request = mysqli_fetch_all(mysqli_query($this->linkbd,
                    "SELECT grupo, titulo 
                    FROM ccpetbienestransportables 
                    WHERE version = (SELECT MAX(version) FROM ccpetbienestransportables)  AND LENGTH(grupo) >= 5 AND (grupo LIKE '$search%' OR titulo LIKE '%$search%') 
                    ORDER BY grupo ASC"),MYSQLI_ASSOC);
                }else if($option=="cod_bien"){
                    $request = mysqli_query($this->linkbd,
                    "SELECT grupo, titulo 
                    FROM ccpetbienestransportables 
                    WHERE version = (SELECT MAX(version) FROM ccpetbienestransportables) AND LENGTH(grupo) >= 5 AND grupo='$search'"
                    )->fetch_assoc();
                }
                if(!empty($request)){
                    $arrResponse = array("status"=>true,"msg"=>"Datos encontrados","data"=>$request);
                }else{
                    $arrResponse = array("status"=>false,"msg"=>"Datos no encontrados");
                }
            }else{
                $arrResponse = array("status"=>false,"msg"=>"Error de datos");
            }
            echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
            die();
        }
    }
?>