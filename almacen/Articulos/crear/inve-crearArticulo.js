const URL ='almacen/Articulos/crear/inve-crearArticulo.php';

var app = new Vue({
    el:"#myapp",
    data() {
        return {
            isLoading:false,
            isModalBienes:false,
            arrGrupos:[],
            arrUnidades:[],
            arrModalBienes:[],
            intSelectGrupo:0,
            strCodigo:"",
            strBuscarBienCodigo:"",
            strBuscarBienModal:"",
            strBienTitulo:"",
            strNombre:"",
            strUnidad:0,
            objBien:{"grupo":"","titulo":""}
        }
    },
    mounted() {
        this.getData();
    },
    methods: {
        getData: async function(){
            const formData = new FormData();
            formData.append("action","grupo_inventario");
            const response = await fetch(URL,{method:"POST",body:formData});
            const objData = await response.json();
            this.arrGrupos = objData.grupos;
            this.arrUnidades = objData.unidades;
            this.arrModalBienes = objData.bienes;
        },
        getCodigo: async function(){
            const formData = new FormData();
            formData.append("action","codigo");
            formData.append("codigo",this.intSelectGrupo);
            const response = await fetch(URL,{method:"POST",body:formData});
            this.strCodigo = await response.json();
        },
        search: async function(option){
            let search = "";
            
            if(option=="cod_bien") search = this.strBuscarBienCodigo;
            if(option=="clasificacion") search = this.strBuscarBienModal;

            this.isLoading = true;
            let formData = new FormData()
            formData.append("type",this.intCodeType);
            formData.append("search",search);
            formData.append("action","search");
            formData.append("option",option);
            
            const response = await fetch(URL,{method:'POST',body:formData});
            const objData = await response.json();
            if(objData.status){
                if(option=="cod_bien") this.objBien = objData.data;
                if(option=="clasificacion") this.arrModalBienes = objData.data;
            }else{
                this.arrModalBienes = [];
            }
            this.isLoading = false;
        },
        selectItem: async function(item){
            this.objBien = item;
            this.strBuscarBienCodigo = this.objBien.grupo;
            this.strBienTitulo = this.objBien.titulo;
        },
        save: async function(){
            if(this.intSelectGrupo < 0 || this.strCodigo =="" || this.strNombre =="" || this.strMedida==""){
                Swal.fire("Error","Todos los campos son obligatorios, inténte de nuevo.","error");
                return false;
            }
            this.isLoading = true;
            let formData = new FormData();
            formData.append("cod_grupo",this.intSelectGrupo);
            formData.append("cod_bien",this.strBuscarBienCodigo);
            formData.append("cod_articulo",this.strCodigo);
            formData.append("nombre",this.strNombre);
            formData.append("unidad",this.strUnidad);
            formData.append("action","save");

            const response = await fetch(URL,{method:"POST",body:formData});
            const objData = await response.json();
            if(objData.status){
                this.intSelectGrupo = 0;
                this.strBuscarBienCodigo = "";
                this.strCodigo = "";
                this.strUnidad = 0;
                this.strNombre = "";
                this.objBien.titulo = "";

                Swal.fire("Guardado",objData.msg,"success");
            }else{
                Swal.fire("Error",objData.msg,"error");
            }
            this.isLoading = false;
        }
    },
})