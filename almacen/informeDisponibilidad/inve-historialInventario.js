const URL = 'almacen/informeDisponibilidad/inve-informeDisponiblidad.php';

var app = new Vue({
    el: '#myapp',
	data:{

        error: '',
        loading: false,

        codArticulo: '',
        nombreArticulo: '',
        centroCosto: '',
        bodeg: '',
        historial: [],
    },

    mounted: function(){

        this.datosIniciales();
    },

    methods: 
    {
        //Desglosar array
        toFormData: function(obj){
            var form_data = new FormData();
            for(var key in obj){
                form_data.append(key, obj[key]);
            }
            return form_data;
        },

        formatonumero: function(valor){
            return new Intl.NumberFormat("en-US", {style: "currency", currency: "USD"}).format(valor);
        },

        traeParametros: function(name){
            //Captura parametros
            name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
            var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
            results = regex.exec(location.search);
            return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
        },

        datosIniciales: async function() {

            this.codArticulo = this.traeParametros("cod");
            this.bodeg = this.traeParametros("bod");
            this.centroCosto = this.traeParametros('cc');

            var formData = new FormData();

            formData.append("bodega", this.bodeg);
            formData.append("cc", this.centroCosto);
            formData.append("codArticulo", this.codArticulo);

            await axios.post(URL+'?action=buscaDatos', formData)
            .then((response) => {
                
                this.nombreArticulo = response.data.nombre;
                this.historial = response.data.historial;
            });
        },

        excel: function() {

            if (this.historial != '') {

                document.form2.action="inve-historialInventarioExcel.php";
				document.form2.target="_BLANK";
				document.form2.submit(); 
            }
            else {
                Swal.fire("Faltan datos", "Genera el informe primero", "warning");
            }
        },
    },
});