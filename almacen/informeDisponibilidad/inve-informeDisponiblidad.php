<?php
	require_once '../../comun.inc';
    require '../../funciones.inc';
    session_start();

	$linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");

    $out = array('error' => false);

    $action = "show";

    if(isset($_GET['action'])){
        $action = $_GET['action'];
    }

    function array_sort_by(&$arrIni, $col, $order)
    {
        $arrAux = array();
        foreach ($arrIni as $key=> $row)
        {
            $arrAux[$key] = is_object($row) ? $arrAux[$key] = $row->$col : $row[$col];
            $arrAux[$key] = strtolower($arrAux[$key]);
        }
        array_multisort($arrAux, $order, $arrIni);
    }

    if ($action == "datosIniciales") {

        $bodegas = [];
        $centroCostos = [];
        $grupos = [];

        $sqlBodegas = "SELECT id_cc, nombre FROM almbodegas WHERE estado = 'S'";
        $resBodegas = mysqli_query($linkbd, $sqlBodegas);
        while ($rowBodegas = mysqli_fetch_row($resBodegas)) {
            
            array_push($bodegas, $rowBodegas);
        }

        $sqlCentroCosto = "SELECT id_cc, nombre FROM centrocosto WHERE estado='S' AND entidad='S' ORDER BY id_cc";
        $resCentroCosto = mysqli_query($linkbd, $sqlCentroCosto);
        while ($rowCentroCosto = mysqli_fetch_row($resCentroCosto)) {
            
            array_push($centroCostos, $rowCentroCosto);
        }

        $sqlGrupos = "SELECT codigo, nombre FROM almgrupoinv WHERE estado = 'S'";
        $resGrupos = mysqli_query($linkbd, $sqlGrupos);
        while ($rowGrupos = mysqli_fetch_row($resGrupos)) {
            
            array_push($grupos, $rowGrupos);
        }

        $out['bodegas'] = $bodegas;
        $out['centroCostos'] = $centroCostos;
        $out['grupos'] = $grupos;
    }

    if ($action == "buscarInventario") {

        //Inicializa variables
        $bodega = $_POST['bodega'];
        $cc = $_POST['cc'];
        $grupo = $_POST['grupo'];
        $modulo = 3;
        $listadoArticulos = array();

        $codigosEntrada = array();
        $codigosSalida = array();

        $articulosEntrada = array();
        $articulosSalida = array();

        if ($grupo != "") {

            $filtroGrupo = "AND numacti LIKE '$grupo%' ";
        }
        else {
            $filtroGrupo = "";
        }

        //Tipo de comprobantes
        
        //Entrada
        $sqlComprobanteEntrada = "SELECT codigo FROM tipo_comprobante WHERE id_cat = $modulo AND tipo_movimiento = 1";
        $resComprobanteEntrada = mysqli_query($linkbd, $sqlComprobanteEntrada);
        while ($rowComprobanteEntrada = mysqli_fetch_row($resComprobanteEntrada)) {
            $codigosEntrada[] = $rowComprobanteEntrada[0];
        }

        //En grupa todos los tipos de comprobantes de entrada
        $entrada = implode(",", $codigosEntrada);

        //Busca todos los movimientos de entrada con la cantidad de articulos y su valor total
        $sqlComprobantesEntrada = "SELECT numacti, SUM(cantarticulo), SUM(valdebito) FROM comprobante_det WHERE tipo_comp IN ($entrada) AND centrocosto = '$cc' AND bodega_ubicacion = '$bodega' AND valdebito != 0 AND numacti != '' $filtroGrupo GROUP BY (numacti)";
        $resComprobantesEntrada = mysqli_query($linkbd, $sqlComprobantesEntrada);
        while ($rowComprobantesEntrada = mysqli_fetch_row($resComprobantesEntrada)) {
            
            array_push($articulosEntrada, $rowComprobantesEntrada);
        }

        //Salida
        $sqlComprobanteSalida = "SELECT codigo FROM tipo_comprobante WHERE id_cat = $modulo AND tipo_movimiento = 2";
        $resComprobanteSalida = mysqli_query($linkbd, $sqlComprobanteSalida);
        while ($rowComprobanteSalida = mysqli_fetch_row($resComprobanteSalida)) {
            $codigosSalida[] = $rowComprobanteSalida[0];
        }

        //En grupa todos los tipos de comprobantes de salida
        $salida = implode(",", $codigosSalida);

        $sqlComprobantesSalida = "SELECT numacti, SUM(cantarticulo), SUM(valcredito) FROM comprobante_det WHERE tipo_comp IN ($salida) AND centrocosto = '$cc' AND bodega_ubicacion = '$bodega' AND valcredito != 0 AND numacti != '' $filtroGrupo GROUP BY (numacti)";
        $resComprobantesSalida = mysqli_query($linkbd, $sqlComprobantesSalida);
        while ($rowComprobantesSalida = mysqli_fetch_row($resComprobantesSalida)) {
            
            array_push($articulosSalida, $rowComprobantesSalida);
        }
        
        //Recorre el array de articulos de entrada
        foreach ($articulosEntrada as $key => $articulo) {

            $cantidadSalida = 0;
            $valorSalida = 0;
            $saldoArticulo = 0;
            $codigoArticulo2 = "";
            $codigoArticulo2 = $articulo[0];
            $posicion = "";
            
            //Busca el codigo de articulo en el array de salidas en la columna codigo y obtiene su  posicion
            $posicion = array_search($codigoArticulo2, array_column($articulosSalida, 0));

            //Si encuentra la posicion va y busca en el array de salidas y toma la cantidad de articulos de salida y el valor total de esas salidas

            $a = gettype($posicion);
            //echo $codigoArticulo2 . " - " . $a . "\n"; 

            if ($a != 'boolean') { 
                
                $cantidadSalida = $articulosSalida[$posicion][1];
                $valorSalida = $articulosSalida[$posicion][2];
                //echo "Encontro " . $codigoArticulo2 . " - " . $posicion . "\n";
            }   
            else {
                //echo "No encontro " . $codigoArticulo2 . " - " . $posicion .  "\n";
            }
            
            $saldoArticulo = $articulo[1] - $cantidadSalida;
            
            if ($saldoArticulo > 0) {
                
                $datos = array();
                $codigoarticulo = "";
                $codigo = "";
                $saldoValorTotal = 0;
                $valorUnitario = 0;
                $grupo = "";

                $saldoValorTotal = $articulo[2] - $valorSalida;
                $valorUnitario = $saldoValorTotal / $saldoArticulo;

                $codigoarticulo = $articulo[0];
                $grupo = substr($codigoarticulo, 0, -5);
                $codigo = substr($codigoarticulo, 4);
                
                $sqlGrupoInventario = "SELECT nombre, concepent FROM almgrupoinv WHERE codigo = '$grupo'";
                $rowGrupoInventario = mysqli_fetch_row(mysqli_query($linkbd, $sqlGrupoInventario));

                $sqlCuentaContableArticulo = "SELECT cuenta FROM conceptoscontables_det WHERE codigo = '$rowGrupoInventario[1]' AND tipo = 'AE' AND debito = 'S' ";
                $rowCuentaContableArticulo = mysqli_fetch_row(mysqli_query($linkbd, $sqlCuentaContableArticulo));

                $sqlArticulos = "SELECT nombre, codunspsc FROM almarticulos WHERE codigo = '$codigo' AND grupoinven = '$grupo'";
                $rowArticulos = mysqli_fetch_row(mysqli_query($linkbd, $sqlArticulos));

                $sqlArticulosDet = "SELECT unidad FROM almarticulos_det WHERE articulo = '$codigoarticulo'";
                $rowArticulosDet = mysqli_fetch_row(mysqli_query($linkbd, $sqlArticulosDet));

                array_push($datos, $codigoarticulo);
                array_push($datos, $rowArticulos[0]);
                array_push($datos, $rowGrupoInventario[0]);
                array_push($datos, $saldoArticulo);
                array_push($datos, $rowArticulosDet[0]);
                array_push($datos, $bodega);
                array_push($datos, $cc);
                array_push($datos, round($valorUnitario,2));
                array_push($datos, $rowArticulos[1]);
                array_push($datos, $rowCuentaContableArticulo[0]);
                array_push($datos, round($valorUnitario,2) * $saldoArticulo);
                array_push($listadoArticulos, $datos);
            }
        }

        $out['articulos'] = $listadoArticulos;
    }

    if ($action = "buscaDatos") {

        $historial = [];
        $codArticulo = $_POST['codArticulo'];
        $bodega = $_POST['bodega'];
        $cc = $_POST['cc'];
        $modulo = 3;

        $grupo = substr($codArticulo, 0, -5);
        $codigo = substr($codArticulo, 4);

        $sqlArticulo = "SELECT nombre FROM almarticulos WHERE codigo = '$codigo' AND grupoinven = '$grupo' AND estado = 'S'";
        $rowArticulo = mysqli_fetch_row(mysqli_query($linkbd, $sqlArticulo));

        $nombre = $rowArticulo[0];

        $out['nombre'] = $nombre;

        //Entrada
        $sqlComprobanteEntrada = "SELECT codigo FROM tipo_comprobante WHERE id_cat = $modulo AND tipo_movimiento = 1";
        $resComprobanteEntrada = mysqli_query($linkbd, $sqlComprobanteEntrada);
        while ($rowComprobanteEntrada = mysqli_fetch_row($resComprobanteEntrada)) {
            $codigosEntrada[] = $rowComprobanteEntrada[0];
        }

        //En grupa todos los tipos de comprobantes de entrada
        $entrada = implode(",", $codigosEntrada);

        //Busca todos los movimientos de entrada con la cantidad de articulos y su valor total
        $sqlComprobantesEntrada = "SELECT D.numerotipo, D.tipo_comp, D.numacti, D.cantarticulo, D.valdebito, D.cuenta FROM comprobante_det AS D WHERE D.tipo_comp IN ($entrada) AND D.centrocosto = '$cc' AND D.bodega_ubicacion = '$bodega' AND D.valdebito != 0 AND D.numacti = '$codArticulo'";
        $resComprobantesEntrada = mysqli_query($linkbd, $sqlComprobantesEntrada);
        while ($rowComprobantesEntrada = mysqli_fetch_row($resComprobantesEntrada)) {
            
            /*
                0. Articulo
                1. Tipo comprobante
                2. número comprobante
                3. Fecha
                4. Valor
                5. Entrada
                6. Salida
                7. Saldo
                8. Cuenta contable 
            */

            $sqlCab = "SELECT fecha FROM comprobante_cab WHERE numerotipo = $rowComprobantesEntrada[0] AND tipo_comp = $rowComprobantesEntrada[1]";
            $rowCab = mysqli_fetch_row(mysqli_query($linkbd, $sqlCab));

            $fecha = $rowCab[0];

            $sqlTipoComprobante = "SELECT nombre FROM tipo_comprobante WHERE codigo = '$rowComprobantesEntrada[1]'";
            $rowTipoComprobante = mysqli_fetch_row(mysqli_query($linkbd, $sqlTipoComprobante));

            $tipoComprobante = $rowComprobantesEntrada[1] . " - " . $rowTipoComprobante[0];

            $temp = [];

            $temp[] = $codArticulo;
            $temp[] = $tipoComprobante;
            $temp[] = $rowComprobantesEntrada[0];
            $temp[] = $fecha;
            $temp[] = $rowComprobantesEntrada[4];
            $temp[] = $rowComprobantesEntrada[3];
            $temp[] = "0";
            $temp[] = "0";
            $temp[] = $rowComprobantesEntrada[5];
            array_push($historial, $temp);
        }

        //Salida
        $sqlComprobanteSalida = "SELECT codigo FROM tipo_comprobante WHERE id_cat = $modulo AND tipo_movimiento = 2";
        $resComprobanteSalida = mysqli_query($linkbd, $sqlComprobanteSalida);
        while ($rowComprobanteSalida = mysqli_fetch_row($resComprobanteSalida)) {
            $codigosSalida[] = $rowComprobanteSalida[0];
        }

        //En grupa todos los tipos de comprobantes de salida
        $salida = implode(",", $codigosSalida);

        $sqlComprobantesSalida = "SELECT D.numerotipo, D.tipo_comp, D.numacti, D.cantarticulo, D.valcredito, D.cuenta FROM comprobante_det AS D WHERE D.tipo_comp IN ($salida) AND D.centrocosto = '$cc' AND D.bodega_ubicacion = '$bodega' AND D.valcredito != 0 AND D.numacti = '$codArticulo'";
        $resComprobantesSalida = mysqli_query($linkbd, $sqlComprobantesSalida);
        while ($rowComprobantesSalida = mysqli_fetch_row($resComprobantesSalida)) {
            
            $sqlCab = "SELECT fecha FROM comprobante_cab WHERE numerotipo = $rowComprobantesSalida[0] AND tipo_comp = $rowComprobantesSalida[1]";
            $rowCab = mysqli_fetch_row(mysqli_query($linkbd, $sqlCab));

            $fecha = $rowCab[0];

            $sqlTipoComprobante = "SELECT nombre FROM tipo_comprobante WHERE codigo = '$rowComprobantesSalida[1]'";
            $rowTipoComprobante = mysqli_fetch_row(mysqli_query($linkbd, $sqlTipoComprobante));

            $tipoComprobante = $rowComprobantesSalida[1] . " - " . $rowTipoComprobante[0];

            $temp = [];

            $temp[] = $codArticulo;
            $temp[] = $tipoComprobante;
            $temp[] = $rowComprobantesSalida[0];
            $temp[] = $fecha;
            $temp[] = $rowComprobantesSalida[4];
            $temp[] = "0";
            $temp[] = $rowComprobantesSalida[3];
            $temp[] = "0";
            $temp[] = $rowComprobantesSalida[5];
            array_push($historial, $temp);
        }

        array_sort_by($historial, 3, $order = SORT_ASC);

        foreach ($historial as $key => $value) {

            if ($key == 0) {

                $saldo = 0;
            }
            else {
                $saldo = $saldoAnterior;
            }

            if (intval($historial[$key][5]) != 0) {
                $historial[$key][7] = $saldo + intval($historial[$key][5]);
            }
            else if (intval($historial[$key][6]) != 0) {
                $historial[$key][7] = $saldo - intval($historial[$key][6]);
            }

            $saldoAnterior = intval($historial[$key][7]);
        }

        $out['historial'] = $historial; 
    }

    header("Content-type: application/json");
    echo json_encode($out);
    die();