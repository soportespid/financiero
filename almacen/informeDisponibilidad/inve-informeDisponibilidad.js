const URL = 'almacen/informeDisponibilidad/inve-informeDisponiblidad.php';

var app = new Vue({
    el: '#myapp',
	data:{

        error: '',
        loading: false,

        listadoArticulos: [],
        centroCostos: [],
        cc: '',
        bodegas: [],
        bodega: '',
        grupos: [],
        grupo: '',


        codArticulo: '',
        nombreArticulo: '',
        centroCosto: '',
        bodeg: '',
    },

    mounted: function(){

        this.datosIniciales();
    },

    methods: 
    {
        //Desglosar array
        toFormData: function(obj){
            var form_data = new FormData();
            for(var key in obj){
                form_data.append(key, obj[key]);
            }
            return form_data;
        },

        formatonumero: function(valor){
            return new Intl.NumberFormat("en-US", {style: "currency", currency: "USD"}).format(valor);
        },

        datosIniciales: async function() {

            await axios.post(URL+'?action=datosIniciales')
            .then((response) => {
                
                this.centroCostos = response.data.centroCostos;
                this.bodegas = response.data.bodegas;
                this.grupos = response.data.grupos;

            });
        },

        buscarDatos: async function() {
   
            if (this.bodega != "" && this.cc != "") {

                this.loading = true;

                var formData = new FormData();

                formData.append("bodega", this.bodega);
                formData.append("cc", this.cc);
                formData.append("grupo", this.grupo);

                await axios.post(URL+'?action=buscarInventario', formData)
                .then((response) => {
                    
                    this.listadoArticulos = response.data.articulos;
                });

                this.loading = false;
            }  
            else {
                Swal.fire("Error", "Debes seleccionar la bodega y centro de costos", "error");
            }
        },

        visualizar: function(articulo) {

            var x = "inve-historialInventario?cod="+articulo[0]+"&bod="+articulo[5]+"&cc="+articulo[6];
            window.open(x, '_blank');
            window.focus();
        },

        excel: function() {

            if (this.listadoArticulos != '') {

                document.form2.action="inve-disponibilidadInventarioExcel.php";
				document.form2.target="_BLANK";
				document.form2.submit(); 
            }
            else {
                Swal.fire("Faltan datos", "Genera el informe primero", "warning");
            }
        },
    },
});