const URL ='almacen/EntradaAjuste/ver/inve-verEntradaAjuste.php';

var app = new Vue({
    el:"#myapp",
    data() {
        return {
            isLoading:false,
            isModalArticulos:false,
            isModalRegistro:false,
            isModalDocumento:false,
            isSelectAll:true,
            strDescripcion:"",
            strSearch:"",
            strBuscarCodArticulo:"",
            strMedida:"",
            strModelo:"",
            strMarca:"",
            strSerie:"",
            intValor:"",
            intCantidad:"",
            intResults:0,
            intPage:1,
            intTotalPages:1,
            intConsecutivo:0,
            intTotalValor:0,
            intTotalReversion:0,
            intIdCodigo:0,
            arrArticulos:[],
            arrCentros:[],
            arrBodegas:[],
            arrProductos:[],
            arrRegistros:[],
            arrDocumentos:[],
            arrRevProductos:[],
            objArticulo:{"codigo":"","nombre":"","unidad":"","codunspsc":""},
            objRegistro:{"consvigencia":"","detalle":"","valor":"","saldo":0,"credito":"","debito":""},
            objDocumento:{},
            selectMovimiento:1,
            selectEntrada:"01",
            selectCostos:"",
            selectBodegas:"",
            intRevConsecutivo:0,
            intRevDocumento:"",
            strRevDescripcion:"",
            strFecha:"",
        }
    },
    mounted() {
        this.getData();
    },
    methods: {  
        getData: async function(){
            this.intIdCodigo = new URLSearchParams(window.location.search).get('id');
            if(this.intIdCodigo > 0){
                const formData = new FormData();
                formData.append("action","get");
                formData.append("codigo",this.intIdCodigo);
                const response = await fetch(URL,{method:"POST",body:formData});
                const objData = await response.json();
                console.log(objData);
                if(objData.status){
                    let data = objData.data;
                    let documento = data.documento;
                    this.objDocumento = documento.cabecera;
                    this.intConsecutivo = this.objDocumento.consec;
                    this.strDescripcion = this.objDocumento.nombre;
                    this.selectBodegas = documento.articulos[0]['bodega']+" - "+documento.articulos[0]['bodega_nombre'];
                    this.selectCostos = documento.articulos[0]['cc']+" - "+documento.articulos[0]['cc_nombre'];
                    this.arrProductos = documento.articulos;
                    this.strFecha = this.objDocumento.fecha;
                    this.intTotalValor = this.objDocumento.valortotal;
                }else{
                    Swal.fire("Error",objData.msg,"error");
                }

            }else{
                window.location.href="inve-buscarGestionEntradaAjuste.php";
            }
        },
        formatNumero: function(valor){
            return new Intl.NumberFormat("en-US", {style: "currency", currency: "USD"}).format(valor);
        },
        printPDF:function(){
            window.open('inve-pdfEntradaAjuste.php?id='+this.intIdCodigo,"_blank");
        }
    }
})