<?php
    require_once '../../../comun.inc';
    require '../../../funciones.inc';
    require '../../../funcionesSP.inc.php';

    if($_POST){
        $obj = new Articulo();
        if($_POST['action']=="search"){
            $currentPage = intval($_POST['page']); 
            $obj->search($_POST['search'],$currentPage);
        }
    }

    class Articulo{
        private $linkbd;
        private $strCodigoArticulo;
        private $strCodigoGrupo;
        private $strCodigoBien;
        public function __construct() {
            $this->linkbd = conectar_v7();
            $this->linkbd->set_charset("utf8");
        }
        public function search(string $search,int $currentPage){
            $perPage = 200;
            $startRows = ($currentPage-1) * $perPage;
            $totalRows = 0;

            $sql = "SELECT codigo,tipomov,nombre,consec,DATE(fecha) as fechaorden, DATE_FORMAT(fecha,'%d/%m/%Y') as fecha
            FROM almginventario 
            WHERE (nombre LIKE '$search%' OR consec LIKE '$search%') AND tiporeg = '04' AND estado ='S' ORDER BY fechaorden DESC LIMIT $startRows, $perPage";
            $request = mysqli_fetch_all(mysqli_query($this->linkbd,$sql),MYSQLI_ASSOC);
            $totalRows = intval(mysqli_query($this->linkbd,
                "SELECT count(*) as total 
                FROM almginventario 
                WHERE (nombre LIKE '$search%' OR consec LIKE '$search%') AND tiporeg = '04' AND estado ='S'")->fetch_assoc()['total']);
            $totalPages = $totalRows > 0 ? ceil($totalRows/$perPage) : 1;
            $arrResponse = array("status"=>true,"data"=>$request,"total"=>$totalRows,"total_pages"=>$totalPages);
            echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
            die();
        }
    }
?>