<?php
    require_once '../../../comun.inc';
    require '../../../funciones.inc';
    require '../../../funcionesSP.inc.php';
    session_start();
    if($_POST){
        $obj = new Articulo();
        if($_POST['action']=="search"){
            $currentPage = intval($_POST['page']);
            $obj->search($_POST['search'],$currentPage,$_POST['option']);
        }else if($_POST['action']=="get"){
            $obj->getData();
        }else if($_POST['action'] =="getDebt"){
            $obj->getDebt($_POST['id'],$_POST['centro']);
        }else if($_POST['action'] == "save"){
            $obj->save($_POST['entrada']);
        }else if($_POST['action']=="concepto"){
            $obj->getConcepto($_POST['id']);
        }
    }

    class Articulo{
        private $linkbd;
        private $strCodigoArticulo;
        private $strCodigoGrupo;
        private $strCodigoCentro;
        private $strVigencia;
        public function __construct() {
            $this->linkbd = conectar_v7();
            $this->linkbd->set_charset("utf8");
            $this->strVigencia = vigencia_usuarios($_SESSION['cedulausu']);
        }
        public function getData(){
            if(!empty($_SESSION)){
                $request['consecutivo'] = $this->selectConsecutivo();
                $request['rev_consecutivo'] = $this->selectRevConsecutivo();
                $request['documentos_entradas'] = $this->selectDocumentos();
                $request['registros'] = $this->selectRegistros();
                $request['conceptos'] = $this->selectConceptos();
                echo json_encode($request,JSON_UNESCAPED_UNICODE);
            }
            die();
        }

        public function save($data){
            if(!empty($_SESSION)){
                $arrData = json_decode($data,true);
                //dep($arrData);exit;
                if(empty($arrData['cabecera']) || empty($arrData['detalle'])){
                    $arrResponse = array("status"=>false,"msg"=>"Error de datos");
                }else{
                    $arrCabecera = $arrData['cabecera'];
                    $arrDetalle = $arrData['detalle'];
                    $arrRegistro = $arrCabecera['registro'];
                    if(empty($arrRegistro['idrp']) || empty($arrRegistro['centro'])
                    || empty($arrRegistro['bodega']) || empty($arrRegistro['total'])){
                        $arrResponse = array("status"=>false,"msg"=>"Error de información acto.");
                    }else if(empty($arrCabecera['consecutivo']) || empty($arrCabecera['fecha'])
                    || empty($arrCabecera['descripcion'])){
                        $arrResponse = array("status"=>false,"msg"=>"Error de cabecera.");
                    }else if(empty($arrDetalle)){
                        $arrResponse = array("status"=>false,"msg"=>"Error de artículos.");
                    }else{
                        /**********CABECERA***********/
                        $arrDate = explode("/",$arrCabecera['fecha']);
                        $strFecha = date_format(date_create($arrDate[2]."-".$arrDate[1]."-".$arrDate[0]),"Y-m-d");
                        $strVigencia = $arrDate[2];
                        $strDescripcion = $arrCabecera['descripcion'];
                        $intConsecutivo = $arrCabecera['consecutivo'];
                        $intIdrp = $arrRegistro['idrp'];
                        $strCentro = $arrRegistro['centro'];
                        $strBodega = $arrRegistro['bodega'];
                        $strTipoRegistro = "04";
                        $intTotalUsado = $arrRegistro['total'];
                        $intMov = $arrData['movimiento'];
                        $strUsuario = $_SESSION['cedulausu'];
                        $intTipoComprobante = $intMov == 1 ? 53:2053;
                        $intEstado = 1;
                        $strTercero = $this->selectTercero();
                        $totalArticulos = count($arrDetalle);
                        $intConsecEntrada = 0;
                        $intSaldo = $arrRegistro['saldo'] - $arrRegistro['total'];
                        if(isset($arrRegistro['entrada_consec'])){
                            $intConsecEntrada = $arrRegistro['entrada_consec'];
                        }
                        $validarBloqueo = $this->validBloqueo($strFecha,$strUsuario);
                        if($validarBloqueo >= 1){
                            $request = $this->insertCompraAlmacen($strFecha,$intMov,$strTipoRegistro,$intIdrp,
                            $intTotalUsado,$strUsuario,$strDescripcion,$intConsecutivo,$strBodega,$strVigencia,$strCentro,$intConsecEntrada);
                            if($request > 0){
                                for ($i=0; $i < $totalArticulos ; $i++) {
                                    $codigo = $intConsecutivo;
                                    $unspsc = $arrDetalle[$i]['codunspsc'];
                                    $codart = $arrDetalle[$i]['codigo'];
                                    $solicitud = $intIdrp;
                                    $cantidad_entrada = $intMov == 1 ? $arrDetalle[$i]['cantidad'] : 0;
                                    $cantidad_salida = $intMov == 3 ? $arrDetalle[$i]['cantidad'] : 0;
                                    $valorunit = $arrDetalle[$i]['valor'];
                                    $valortotal = $arrDetalle[$i]['valortotal'];
                                    $unidad = $arrDetalle[$i]['unidad'];
                                    $tipomov = $intMov;
                                    $tiporeg = $strTipoRegistro;
                                    $bodega = $arrDetalle[$i]['bodega'];
                                    $codcuentacre = $intMov == 1 ? $arrDetalle[$i]['credito'] : $arrDetalle[$i]['debito'];
                                    $cc = $arrDetalle[$i]['cc'];
                                    $concepto = $intMov == 1 ? $arrDetalle[$i]['debito'] : $arrDetalle[$i]['credito'];
                                    $marca = $arrDetalle[$i]['marca'];
                                    $modelo = $arrDetalle[$i]['modelo'];
                                    $serie = $arrDetalle[$i]['serie'];

                                    $request = $this->insertArticuloAlmacen($codigo,$unspsc,$codart,$solicitud,$cantidad_entrada,$cantidad_salida,
                                    $valorunit,$valortotal,$unidad,$tipomov,$tiporeg,$bodega,$codcuentacre,
                                    $cc,$concepto,$marca,$modelo,$serie);
                                }
                                $request_comp = $this->insertComprobante($intConsecutivo,$intTipoComprobante,$strFecha,$strDescripcion,$intTotalUsado,$intEstado,$intConsecEntrada);
                                if($request_comp>0){
                                    $strIdComprobante = $intTipoComprobante." ".$intConsecutivo;
                                    $intEstadoDetalle = 1;
                                    $intNumeroTipo = 1;

                                    for ($i=0; $i < $totalArticulos ; $i++) {
                                        $strDetalle = "Entrada por ajuste del articulo ".$arrDetalle[$i]['codigo']."-".$arrDetalle[$i]['nombre'];
                                        $this->insertCompDetalleDeb(
                                            $strIdComprobante,
                                            $arrDetalle[$i]['debito'],
                                            $strTercero,
                                            $arrRegistro['centro'],
                                            $strDetalle,
                                            $intMov == 1 ? $arrDetalle[$i]['valortotal'] : 0,
                                            $intMov == 1 ? 0 : $arrDetalle[$i]['valortotal'],
                                            $intEstadoDetalle,
                                            $strVigencia,
                                            $intTipoComprobante,
                                            $intNumeroTipo,
                                            $arrDetalle[$i]['codigo'],
                                            $arrDetalle[$i]['cantidad'],
                                            $arrDetalle[$i]['bodega'],
                                        );
                                        $this->insertCompDetalleCred(
                                            $strIdComprobante,
                                            $arrDetalle[$i]['credito'],
                                            $strTercero,
                                            $arrRegistro['centro'],
                                            $strDetalle,
                                            $intMov == 1 ? 0 : $arrDetalle[$i]['valortotal'],
                                            $intMov == 1 ? $arrDetalle[$i]['valortotal'] : 0,
                                            $intEstadoDetalle,
                                            $strVigencia,
                                            $intTipoComprobante,
                                            $intNumeroTipo,
                                            $arrDetalle[$i]['codigo'],
                                            $arrDetalle[$i]['cantidad'],
                                            $arrDetalle[$i]['bodega'],
                                        );
                                    }
                                    if($intMov == 1){
                                        $request = mysqli_query(
                                            $this->linkbd,
                                            "UPDATE almactoajusteent SET valorsaldo = $intSaldo WHERE consecutivo = $intIdrp");
                                        $arrResponse = array("status"=>true,"status_comp"=>true,"msg"=>"Datos guardados correctamente");
                                    }else{
                                        $total = $arrRegistro['total'];
                                        $request = mysqli_query(
                                            $this->linkbd,
                                            "UPDATE almactoajusteent SET valorsaldo = $total WHERE consecutivo = $intIdrp");

                                        $arrResponse = array("status"=>true,"status_comp"=>true,"msg"=>"Datos reversados correctamente");
                                    }
                                }else{
                                    $arrResponse = array("status"=>true,"status_comp"=>false,"msg"=>"Error, datos guardados en almacén pero no en contabilidad.");
                                }
                            }else{
                                $arrResponse = array("status"=>false,"msg"=>"No se ha podido guardar, inténtelo de nuevo.");
                            }
                        }else{
                            $arrResponse = array("status"=>false,"msg"=>"Fecha bloqueada.");
                        }
                    }
                }
            }else{
                $arrResponse = false;
            }
            echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
            die();
        }
        public function validBloqueo($strFecha,$strUsuario){
            $sql = "SELECT count(*) as valor FROM dominios dom  WHERE dom.nombre_dominio = 'PERMISO_MODIFICA_DOC'  AND dom.valor_final <= '$strFecha'  AND dom.valor_inicial =  '$strUsuario'";
            $request = mysqli_query($this->linkbd,$sql)->fetch_assoc()['valor'];
            return $request;
        }
        public function selectConceptos(){
            $request = mysqli_fetch_all(mysqli_query(
                $this->linkbd,
                "SELECT * FROM conceptoscontables WHERE tipo='EA' and modulo='5' ORDER BY codigo"),MYSQLI_ASSOC
            );
            return $request;
        }
        public function selectRegistros(){
            $request = mysqli_fetch_all(mysqli_query(
                $this->linkbd,
                "SELECT *,DATE_FORMAT(fecha,'%d/%m/%Y') as fecha FROM almactoajusteent WHERE valorsaldo > 0 AND tipo_mov='104' ORDER BY id DESC"),MYSQLI_ASSOC
            );
            if(count($request)>0){
                $totalRows = count($request);
                for ($i=0; $i < $totalRows; $i++) {
                    $id = $request[$i]['consecutivo'];
                    $articulos = mysqli_fetch_all(mysqli_query(
                        $this->linkbd,
                        "SELECT codigo,descripcion,unumedida as unidad,cantidad,valor,saldo,bodega,cc,descripcion as nombre
                        FROM almactoajusteentarticu
                        WHERE idacto= $id AND tipo_mov=104 AND estado='S' ")
                    ,MYSQLI_ASSOC);
                    $totalArt = count($articulos);
                    for ($j=0; $j < $totalArt ; $j++) {
                        $idBodega = $articulos[$j]['bodega'];
                        $idCentro = $articulos[$j]['cc'];
                        $articulos[$j]['valortotal'] = $articulos[$j]['cantidad'] * $articulos[$j]['valor'];
                        $articulos[$j]['bodega_nombre'] = $this->getBodegaNombre($idBodega);
                        $articulos[$j]['cc_nombre'] =$this->getCentroNombre($idCentro);
                        $articulos[$j]['debito'] = $this->getdebt($articulos[$j]['codigo'],$idCentro);
                        $articulos[$j]['codunspsc'] = $this->getUnspsc($articulos[$j]['codigo']);
                        $articulos[$j]['modelo'] ="";
                        $articulos[$j]['serie'] ="";
                        $articulos[$j]['marca'] ="";
                    }
                    $request[$i]['articulos'] = $articulos;
                }
            }

            return $request;
        }
        public function getConcepto($id){
            $sql = "SELECT cuenta FROM conceptoscontables_det WHERE modulo = '5' AND codigo = '$id' AND tipo = 'EA'";
            $request = mysqli_query($this->linkbd,$sql)->fetch_assoc()['cuenta'];
            echo json_encode($request,JSON_UNESCAPED_UNICODE);
            die();
        }
        public function getDebt($id,$centro){
            $this->strCodigoCentro = $centro;
            $this->strCodigoGrupo = substr($id,0,4);
            $sql = "SELECT t2.cuenta, MAX(t2.fechainicial) as fecha
            FROM conceptoscontables_det t2
            INNER JOIN almgrupoinv t1
            ON t1.concepent = t2.codigo
            WHERE t1.codigo ='$this->strCodigoGrupo' AND t2.cc = '$this->strCodigoCentro'
            AND t2.debito = 'S' AND t2.modulo = '5' AND t2.tipo='AE'";
            $request = mysqli_query(
                $this->linkbd,
                $sql
            )->fetch_assoc()['cuenta'];
            return $request;
        }
        public function getBodegaNombre($codigo){
            $sql="SELECT nombre FROM almbodegas WHERE id_cc ='$codigo'";
            $request = mysqli_query($this->linkbd,$sql)->fetch_assoc()['nombre'];
            return $request;
        }
        public function getCentroNombre($codigo){
            $sql="SELECT nombre FROM centrocosto WHERE id_cc ='$codigo'";
            $request = mysqli_query($this->linkbd,$sql)->fetch_assoc()['nombre'];
            return $request;
        }
        public function getUnspsc($codigo){
            $sql = "SELECT codunspsc FROM almarticulos WHERE CONCAT(grupoinven,codigo) = '$codigo'";
            $request = mysqli_query($this->linkbd,$sql)->fetch_assoc()['codunspsc'];
            return $request;
        }
        public function selectDocumentos(){
            $sql = "SELECT * ,DATE_FORMAT(fecha,'%d/%m/%Y') as fecha FROM almginventario WHERE tipomov = 1 AND tiporeg ='04' AND estado='S' ORDER BY consec DESC";
            $request = mysqli_fetch_all(mysqli_query($this->linkbd,$sql),MYSQLI_ASSOC);
            if(count($request)){
                $totalDocs = count($request);
                for ($i=0; $i < $totalDocs ; $i++) {
                    $idCodigo = $request[$i]['consec'];
                    $sqlAr = "SELECT *,unspsc as codunspsc,
                    codart as codigo,valorunit as valor,
                    valortotal as total, codcuentacre as credito,
                    cc as centro, concepto as debito, cantidad_entrada as cantidad
                    FROM almginventario_det WHERE tipomov = 1 AND tiporeg='04' AND codigo = $idCodigo ORDER BY id_det DESC";
                    $articulos = mysqli_fetch_all(mysqli_query($this->linkbd,$sqlAr),MYSQLI_ASSOC);
                    $totalArt = count($articulos);
                    for ($j=0; $j < $totalArt ; $j++) {
                        $idBodega = $articulos[$j]['bodega'];
                        $idCentro = $articulos[$j]['centro'];
                        $idArticulo = $articulos[$j]['codart'];
                        $articulos[$j]['bodega_nombre'] = mysqli_query($this->linkbd,"SELECT nombre FROM almbodegas WHERE id_cc='$idBodega'")->fetch_assoc()['nombre'];
                        $articulos[$j]['cc_nombre'] = mysqli_query($this->linkbd,"SELECT nombre FROM centrocosto WHERE id_cc='$idCentro'")->fetch_assoc()['nombre'];
                        $articulos[$j]['nombre'] = mysqli_query($this->linkbd,"SELECT nombre FROM almarticulos WHERE CONCAT(grupoinven,codigo) = '$idArticulo'")->fetch_assoc()['nombre'];
                        $articulos[$j]['isSelected'] = true;
                    }
                    $request[$i]['articulos'] = $articulos;
                }
            }
            return $request;
        }
        public function selectTercero(){
            $sql = "SELECT nit FROM configbasica WHERE estado = 'S'";
            $request = mysqli_query($this->linkbd,$sql)->fetch_assoc()['nit'];
            return $request;
        }
        public function selectRevConsecutivo(){
            $sql = "SELECT consec FROM almginventario WHERE tipomov='3' AND tiporeg='04'
            ORDER BY consec DESC";
            $request = mysqli_query($this->linkbd,$sql,MYSQLI_ASSOC)->fetch_assoc()['consec']+1;
            return $request;
        }
        public function selectConsecutivo(){
            $sql = "SELECT consec FROM almginventario
            WHERE tipomov='1'
            AND tiporeg='04'
            ORDER BY consec DESC";
            $request = mysqli_query($this->linkbd,
                $sql,MYSQLI_ASSOC)->fetch_assoc()['consec']+1;
            return $request;
        }
        public function insertCompraAlmacen($strFecha,$intMov,$strTipoRegistro,$intIdrp,
            $intTotalUsado,$strUsuario,$strDescripcion,$intConsecutivo,$strBodega,$strVigencia,$strCentro,$intConsecEntrada){
            if($intConsecEntrada > 0){
                mysqli_query($this->linkbd,"UPDATE almginventario SET estado = 'R' WHERE tipomov=1 AND tiporeg='04' AND consec = $intConsecEntrada");
            }
            $chrEstado = "S";
            $sql = "INSERT INTO almginventario (fecha,tipomov,tiporeg,codmov,valortotal,usuario,nombre,consec,bodega1,bodega2,vigenciadoc,cc,estado)
                    VALUES('$strFecha','$intMov','$strTipoRegistro','$intIdrp','$intTotalUsado','$strUsuario','$strDescripcion','$intConsecutivo','$strBodega','$strBodega','$strVigencia','$strCentro','$chrEstado')";
            $request = mysqli_query($this->linkbd,$sql);
            return $request;
        }
        public function insertArticuloAlmacen($codigo,$unspsc,$codart,$solicitud,$cantidad_entrada,$cantidad_salida,
        $valorunit,$valortotal,$unidad,$tipomov,$tiporeg,$bodega,$codcuentacre,
        $cc,$concepto,$marca,$modelo,$serie){
            $sql = "INSERT INTO almginventario_det (codigo,unspsc,codart,solicitud,cantidad_entrada,cantidad_salida,
                    valorunit,valortotal,unidad,tipomov,tiporeg,bodega,codcuentacre,
                    cc,concepto,marca,modelo,serie)
                    VALUES ('$codigo','$unspsc','$codart','$solicitud','$cantidad_entrada','$cantidad_salida',
                    '$valorunit','$valortotal','$unidad','$tipomov','$tiporeg','$bodega','$codcuentacre',
                    '$cc','$concepto','$marca','$modelo','$serie')";
            $request = mysqli_query($this->linkbd,$sql);
            return $request;
        }
        public function insertComprobante($intConsecutivo,$intTipoComprobante,$strFecha,$strDescripcion,$intTotalUsado,$intEstado,$intConsecEntrada){
            if($intConsecEntrada > 0){
                mysqli_query($this->linkbd,"UPDATE comprobante_cab SET estado = 0 WHERE tipo_comp=53 AND numerotipo = $intConsecEntrada");
            }
            $sql = "INSERT INTO comprobante_cab (numerotipo,tipo_comp,fecha,concepto,total,total_debito,total_credito,estado)
            VALUES($intConsecutivo,$intTipoComprobante,'$strFecha','$strDescripcion',$intTotalUsado,$intTotalUsado,$intTotalUsado,$intEstado)";
            $request = mysqli_query($this->linkbd,$sql);
            return $request;
        }
        public function insertCompDetalleDeb($idComprobante,$cuenta,$tercero,$centro,$detalle,
        $valorDeb,$valorCre,$estado,$vigencia,$tipoComp,$numeroTipo,$codigo,$cantidad,$bodega){
            $sql = "INSERT INTO comprobante_det (id_comp,cuenta,tercero,centrocosto,detalle,valdebito,valcredito,estado,
            vigencia,tipo_comp,numerotipo,numacti,cantarticulo,bodega_ubicacion)
            VALUES('$idComprobante','$cuenta','$tercero','$centro','$detalle',$valorDeb,$valorCre,$estado,'$vigencia',$tipoComp,$numeroTipo,'$codigo',$cantidad,'$bodega')";
            $request = mysqli_query($this->linkbd,$sql);
            return $request;
        }
        public function insertCompDetalleCred($idComprobante,$cuenta,$tercero,$centro,$detalle,
        $valorDeb,$valorCre,$estado,$vigencia,$tipoComp,$numeroTipo,$codigo,$cantidad,$bodega){
            $sql = "INSERT INTO comprobante_det (id_comp,cuenta,tercero,centrocosto,detalle,valdebito,valcredito,estado,
            vigencia,tipo_comp,numerotipo,numacti,cantarticulo,bodega_ubicacion)
            VALUES('$idComprobante','$cuenta','$tercero','$centro','$detalle',$valorDeb,$valorCre,$estado,'$vigencia',$tipoComp,$numeroTipo,'$codigo',$cantidad,'$bodega')";
            $request = mysqli_query($this->linkbd,$sql);
            return $request;
        }
    }
?>
