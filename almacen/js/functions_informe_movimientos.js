const URL = "almacen/controllers/InformeMovimientosController.php";

var app = new Vue({
    el:"#myapp",
    data() {
        return {
            /* general */
            isLoading: false,
            fechaIni: new Date(new Date().setDate(1)).toISOString().split("T")[0],
            fechaFin:  new Date(new Date().getFullYear(), new Date().getMonth() + 1, 0).toISOString().split("T")[0],
            arrData: [],
        }
    },
    mounted() {        
    },
    methods: {
        async getData() {
            if (this.fechaIni > this.fechaFin) {
                Swal.fire("Atención!","La fecha final no puede ser superior a la fecha inicial","warning");
            }

            let formData = new FormData();
            formData.append("action", "getData");
            formData.append("fecha_ini", this.fechaIni);
            formData.append("fecha_fin", this.fechaFin);
            this.isLoading = true;
            const response = await fetch(URL,{method:"POST",body:formData});
            const objData = await response.json();
            this.arrData = objData.movimientos;
            this.isLoading = false;
        },

        async sendToView(item) {
            if (item.tipo_movimiento == "101" || item.tipo_movimiento == "104" || item.tipo_movimiento == "107") {
                window.open(item.url + "?id="+item.codigo);
            } else if (item.tipo_movimiento == "203") {
                window.open(item.url + "?consecutivo="+item.consecutivo);
            } else if (item.tipo_movimiento == "206") {
                window.open(item.url + "?consec="+item.consecutivo+"&mov="+item.tipomov);
            }
        },

        /* Formatos */
        formatFecha: function(fecha) {
            // Separar la fecha en componentes
            const parts = fecha.split('-');
            if (parts.length !== 3) {
                throw new Error('Formato de fecha inválido. Use Y-m-d.');
            }
            const year = parts[0];
            const month = parts[1];
            const day = parts[2];
        
            // Retornar la fecha en el formato deseado
            return `${day}/${month}/${year}`;
        },

        viewFormatNumber: function(valor){
            return new Intl.NumberFormat("en-US", {style: "currency", currency: "USD"}).format(valor);  
        },
    },
    computed:{

    }
})
