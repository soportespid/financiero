const URL = 'almacen/inventarioExtracontable/editar/inve-editarInventarioExtracontable.php';
import { filtroEnArrayDeObjetos } from './../../../funciones.js';

var app = new Vue({
    el: '#myapp',
	data:{

        error: '',
        loading: false,
        codArticulo: '',
        nomArticulo: '',
        cantidad: 0,
        unidadMedida: '',
        valorUnidad: 0,
        valorTotal: 0,
        showArticulos: false,
        articulos: [],
        articuloscopy: [],
        searchArticulo: '',
        listaArticulos: [],
        listaArticulosGuardar: [],
        totalValores: 0,
        terceros: [],
        terceroscopy: [],
        tercero: '',
        nomTercero: '',
        showModalResponsables: false,
        searchCedula: '',
        dependencias: [],
        dependencia: '',
        id: '',
    },

    mounted: function(){

        this.traerInformacion();
        this.datosIniciales();
    },

    methods: 
    {
        //Desglosar array
        toFormData: function(obj){
            var form_data = new FormData();
            for(var key in obj){
                form_data.append(key, obj[key]);
            }
            return form_data;
        },

        formatonumero: function(valor){
            return new Intl.NumberFormat("en-US", {style: "currency", currency: "USD"}).format(valor);
        },

        traeParametros: function(name){
            //Captura parametros
            name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
            var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
            results = regex.exec(location.search);
            return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
        },

        traerInformacion: async function() {

            this.id = this.traeParametros("id");

            var formData = new FormData();
            formData.append("id", this.id);

            await axios.post(URL+'?action=consultaInformacion', formData).then((response) => {
            
                this.codArticulo = response.data.codArticulo;
                this.nomArticulo = response.data.nomArticulo;
                this.unidadMedida = response.data.unidadMedida;
                this.dependencia = response.data.dependencia;
                document.getElementById('fechaCompra').value = response.data.fechaCompra;
                this.tercero = response.data.tercero;
                this.nomTercero = response.data.nomTercero;
                this.cantidad = response.data.cantidad;
                this.valorUnidad = response.data.valorUnidad;
                this.valorTotal = response.data.valorTotal;
            });
        },

        datosIniciales: async function() {
            
            await axios.post(URL+'?action=datosIniciales')
            .then((response) => {
            
                this.articulos = response.data.articulos;
                this.articuloscopy = response.data.articulos;
                this.terceros = response.data.terceros;
                this.terceroscopy = response.data.terceros;
                this.dependencias = response.data.dependencias;
            });
        },

        despliegaModalTerceros: function() {

            this.showModalResponsables = true;
        },

        filtroCedulaResponsable: async function () {

            const data = this.terceroscopy;
            var text = this.searchCedula;
            var resultado = [];

            resultado = await filtroEnArrayDeObjetos({'data': data, 'text': text});

            this.terceros = resultado;
        },

        seleccionaResponsable: function(documento, nombre) {

            if ((documento != "" && documento != null) && (nombre != "" && nombre != null)) {

                this.tercero = documento;
                this.nomTercero = nombre;
                this.showModalResponsables = false;
            }
        },

        despliegaModalArticulos: function() {

            this.showArticulos = true;
        },

        filtroArticulos: async function() {

            const data = this.articuloscopy;
            var text = this.searchArticulo;
            var resultado = [];

            resultado = await filtroEnArrayDeObjetos({'data': data, 'text': text});

            this.articulos = resultado;
        },

        seleccionarArticulo: function(cod, nom, unidad) {

            if ((cod != "" && cod != null) && (nom != "" && nom != null) && (unidad != "" && unidad != null)) {

                this.codArticulo = cod;
                this.nomArticulo = nom;
                this.unidadMedida = unidad;
                this.showArticulos = false;
            } 
        },

        validaCantidad: function(cantidad) {
            
            if (cantidad >= 1) {

                if(Number.isInteger(parseFloat(cantidad))) {
                    this.valorTotal = this.cantidad * this.valorUnidad;
                }
                else {
                    Swal.fire({
                        icon: 'info',
                        title: 'Formato no valido',
                        text: 'La cantidad debe ser un número entero.'
                    })     
                    this.cantidad = 0;
                }
            }
            else {
                Swal.fire({
                    icon: 'info',
                    title: 'Cantidad no valida',
                    text: 'La cantidad debe ser mayor o igual a uno.'
                })     
                this.cantidad = 0;
            }
        },

        validaValorUnidad: function(valor) {

            if (this.cantidad >= 1) {
                if (valor >= 1) {

                    this.valorTotal = this.cantidad * this.valorUnidad;
                }
                else {
                    Swal.fire({
                        icon: 'info',
                        title: 'Valor no valido',
                        text: 'El valor debe ser mayor o igual a uno.'
                    })     
                    this.valorUnidad = 0;
                    this.valorTotal = 0;
                }
            }
            else {
                Swal.fire({
                    icon: 'info',
                    title: 'Cantidad no ingresada',
                    text: 'Ingrese primero la cantidad de articulos.'
                })     
                this.valorUnidad = 0;
                this.valorTotal = 0;
            }
        },

        guardar: async function() {
 
            const fechaCompra = document.getElementById('fechaCompra').value;

            if (this.codArticulo != "" && this.nomArticulo != "" && this.unidad != "") {

                if (this.dependencia != "" && fechaCompra != "" && this.tercero && this.nomTercero != "") {

                    if (this.cantidad > 0 && this.valorUnidad > 0 && this.valorTotal > 0) {
                        Swal.fire({
                            icon: 'question',
                            title: 'Esta seguro que quiere editar?',
                            showDenyButton: true,
                            confirmButtonText: 'Guardar!',
                            denyButtonText: 'Cancelar',
                            }).then((result) => {
                            if (result.isConfirmed) {

                                var formData = new FormData();

                                formData.append("id", this.id);
                                formData.append("fechaCompra", fechaCompra);
                                formData.append("dependencia", this.dependencia);
                                formData.append("responsable", this.tercero);
                                formData.append("cantidad", this.cantidad);
                                formData.append("valorUnitario", this.valorUnidad);
                                formData.append("valorTotal", this.valorTotal);

                                axios.post(URL+'?action=guardar', formData).then((response) => {

                                    if(response.data.result == true){
                                        Swal.fire({
                                            position: 'top-end',
                                            icon: 'success',
                                            title: 'Se ha actualizado con exito',
                                            showConfirmButton: false,
                                            timer: 1500
                                        });
                                    }
                                    else{
                                        Swal.fire({
                                            icon: 'error',
                                            title: 'Error!',
                                            text: 'Contacte con soporte.'
                                        })  
                                    }
                                });
                            } 
                        })  
                    }
                    else {
                        Swal.fire({
                            icon: 'info',
                            title: 'Falta información',
                            text: 'Cantidad, valor unidad y valor total'
                        })          
                    }
                }
                else {
                    Swal.fire({
                        icon: 'info',
                        title: 'Falta información',
                        text: 'Dependencia, fecha de compra, tercero o nombre de tercero'
                    })      
                }
            }
            else {
                Swal.fire({
                    icon: 'info',
                    title: 'Falta información',
                    text: 'Código articulo, nombre articulo y unidad'
                })        
            }
        },
    },
});