<?php
    header("Cache-control: no-cache, no-store, must-revalidate");
    header("Content-Type: text/html;charset=utf8");
	require_once '../../../comun.inc';
    require '../../../funciones.inc';
    require '../../../conversor.php';
    session_start();

	$linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");

    $out = array('error' => false);

    $action = "show";

    if(isset($_GET['action'])){
        $action = $_GET['action'];
    }

    if ($action == "consultaInformacion") {

        $id = "";
        $id = $_POST['id'];

        $sqlSearch = "SELECT cod_articulo, fecha_compra, dependencia, responsable, cantidad, valor_unitario, valor_total FROM alm_extra_contable WHERE id = $id";
        $resSearch = mysqli_query($linkbd, $sqlSearch);
        $rowSearch = mysqli_fetch_row($resSearch);

        $codArticulo = $rowSearch[0];
        $grupo = substr($codArticulo, 0, -5);
        $codigo = substr($codArticulo, 4);

        $sqlArticulo = "SELECT nombre FROM almarticulos WHERE codigo = '$codigo' AND grupoinven = '$grupo'";
        $resArticulo = mysqli_query($linkbd, $sqlArticulo);
        $rowArticulo = mysqli_fetch_row($resArticulo);

        $nomArticulo = $rowArticulo[0];

        $sqlUnidad = "SELECT unidad FROM almarticulos_det WHERE articulo = '$codArticulo'";
        $resUnidad = mysqli_query($linkbd, $sqlUnidad);
        $rowUnidad = mysqli_fetch_row($resUnidad);

        $unidad = $rowUnidad[0];

        $fechaCompra = date("d/m/Y", strtotime($rowSearch[1]));
        $dependencia = $rowSearch[2];
        $tercero = $rowSearch[3];
        $nomTercero = buscatercero($tercero);
        $cantidad = $rowSearch[4];
        $valorUnitario = $rowSearch[5];
        $valorTotal = $rowSearch[6];


        $out['codArticulo'] = $codArticulo;
        $out['nomArticulo'] = $nomArticulo;
        $out['unidadMedida'] = $unidad;
        $out['dependencia'] = $dependencia;
        $out['fechaCompra'] = $fechaCompra;
        $out['tercero'] = $tercero;
        $out['nomTercero'] = $nomTercero;
        $out['cantidad'] = $cantidad;
        $out['valorUnidad'] = $valorUnitario;
        $out['valorTotal'] = $valorTotal;
    }

    if ($action == "datosIniciales") {

        $terceros = [];
        $articulos = [];
        $dependencias = [];

        $sqlTercero = "SELECT nombre1, nombre2, apellido1, apellido2, cedulanit, razonsocial, tipodoc FROM terceros WHERE estado = 'S'";
        $resTercero = mysqli_query($linkbd, $sqlTercero);
        while ($rowTerceros = mysqli_fetch_row($resTercero)) {
            
            $temporal = array();

            if ($rowTerceros[6] == "31") {

                $nombre = $rowTerceros[5];
            }
            else {

                $nombre = $rowTerceros[0] . " " . $rowTerceros[1] . " " . $rowTerceros[2] . " " . $rowTerceros[3];
            }

            array_push($temporal, $rowTerceros[4]);
            array_push($temporal, $nombre);
            array_push($terceros, $temporal);
        }

        $sqlArticulos = "SELECT codigo, grupoinven, nombre FROM almarticulos WHERE estado = 'S'";
        $resArticulos = mysqli_query($linkbd, $sqlArticulos);
        while ($rowArticulos = mysqli_fetch_row($resArticulos)) {
            
            $datosTemp = [];
            $codArticulo = $nombre = $unidad = "";

            $codArticulo = $rowArticulos[1].$rowArticulos[0];
            $nombre = $rowArticulos[2];

            $sqlUnidad = "SELECT unidad FROM almarticulos_det WHERE articulo = $codArticulo";
            $resUnidad = mysqli_query($linkbd, $sqlUnidad);
            $rowUnidad = mysqli_fetch_row($resUnidad);

            if ($rowUnidad[0] != null && $rowUnidad != "") {
                $unidad = $rowUnidad[0];
            }
            else {
                $unidad = "";
            }

            $datosTemp[] = $codArticulo;
            $datosTemp[] = $nombre;
            $datosTemp[] = $unidad;
            array_push($articulos, $datosTemp);
        }
        
        $sqlDependencia = "SELECT codarea, nombrearea FROM planacareas WHERE estado = 'S'";
        $resDependencia = mysqli_query($linkbd, $sqlDependencia);
        while ($rowDependencia = mysqli_fetch_row($resDependencia)) {
            
            array_push($dependencias, $rowDependencia);
        }

        $out['dependencias'] = $dependencias;
        $out['terceros'] = $terceros;
        $out['articulos'] = $articulos;
    }

    if ($action == "guardar") {

        $id = $codArticulo = $fechaCompra = $dependencia = $responsable = "";
        $cantidad = $valorUnitario = $valorTotal = 0;

        $id = isset($_POST['id']);
        preg_match("/([0-9]{2})\/([0-9]{2})\/([0-9]{4})/", $_POST['fechaCompra'], $f);
        $fechaCompra = $f[3]."-".$f[2]."-".$f[1];
        $dependencia = $_POST['dependencia'];
        $responsable = $_POST['responsable'];
        $cantidad = $_POST['cantidad'];
        $valorUnitario = $_POST['valorUnitario'];
        $valorTotal = $_POST['valorTotal'];

        $sqlUpdate = "UPDATE alm_extra_contable SET fecha_compra = '$fechaCompra', dependencia = '$dependencia', responsable = '$responsable', cantidad = $cantidad, valor_unitario = $valorUnitario, valor_total = $valorTotal WHERE id = $id";
        if(mysqli_query($linkbd, $sqlUpdate) !== False){
            $out['result'] = true;
        }else{
            $out['result'] = false;
        }
    }

    header("Content-type: application/json");
    echo json_encode($out);
    die();