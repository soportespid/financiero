<?php
    header("Cache-control: no-cache, no-store, must-revalidate");
    header("Content-Type: text/html;charset=utf8");
	require_once '../../../comun.inc';
    require '../../../funciones.inc';
    require '../../../conversor.php';
    session_start();

	$linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");

    $action = "show";

    if(isset($_GET['action'])){
        $action = $_GET['action'];
    }

    if ($action == "datosIniciales") {

        $terceros = [];
        $dependencias = [];

        $sqlTercero = "SELECT nombre1, nombre2, apellido1, apellido2, cedulanit, razonsocial, tipodoc FROM terceros WHERE estado = 'S'";
        $resTercero = mysqli_query($linkbd, $sqlTercero);
        while ($rowTerceros = mysqli_fetch_row($resTercero)) {
            
            $temporal = array();

            if ($rowTerceros[6] == "31") {

                $nombre = $rowTerceros[5];
            }
            else {

                $nombre = $rowTerceros[0] . " " . $rowTerceros[1] . " " . $rowTerceros[2] . " " . $rowTerceros[3];
            }

            array_push($temporal, $rowTerceros[4]);
            array_push($temporal, $nombre);
            array_push($terceros, $temporal);
        }
        
        $sqlDependencia = "SELECT codarea, nombrearea FROM planacareas WHERE estado = 'S'";
        $resDependencia = mysqli_query($linkbd, $sqlDependencia);
        while ($rowDependencia = mysqli_fetch_row($resDependencia)) {
            
            array_push($dependencias, $rowDependencia);
        }

        $out['dependencias'] = $dependencias;
        $out['terceros'] = $terceros;
    }

    if ($action == 'buscaInventario') {

        $documento = $_POST['documento'];
        $codDepen = $_POST['dependencia'];
        $fechaIni = $_POST["fechaIni"];
        $fechaFin = $_POST["fechaFin"];
        $filtro01 = $filtro02 = $filtro03 = "";

        if ($documento != "") {

            $filtro01 = "AND responsable = '$documento' ";
        }

        if ($codDepen != "") {
            $filtro02 = "AND dependencia = '$codDepen' ";
        }
    
        if ($fechaIni != "" && $fechaFin != "") {
            $filtro03 = "AND fecha_compra BETWEEN '$fechaIni' AND '$fechaFin'";
        }

        $articulos = [];

        $sqlSearch = "SELECT id, cod_articulo, fecha_compra, dependencia, responsable, cantidad, valor_unitario, valor_total FROM alm_extra_contable WHERE valor_unitario != 0 $filtro01 $filtro02 $filtro03 ORDER BY id ASC";
        $resSearch = mysqli_query($linkbd, $sqlSearch);
        while ($rowSearch = mysqli_fetch_row($resSearch)) {
            
            $datosTemp = [];

            $id = $rowSearch[0];
            $grupo = substr($rowSearch[1], 0, -5);
            $codigo = substr($rowSearch[1], 4);
            $fecha = $rowSearch[2];
            $codArticulo = $rowSearch[1];
            $responsable = $rowSearch[4];
            $dependencia = $rowSearch[3];
            $cantidad = $rowSearch[5];
            $valorUnitario = $rowSearch[6];
            $valorTotal = $rowSearch[7];

            $sqlArticulos = "SELECT nombre, codunspsc FROM almarticulos WHERE codigo = '$codigo' AND grupoinven = '$grupo'";
            $rowArticulos = mysqli_fetch_row(mysqli_query($linkbd, $sqlArticulos));

            $nomArticulo = $rowArticulos[0];

            $sqlArticulosDet = "SELECT unidad FROM almarticulos_det WHERE articulo = '$codArticulo'";
            $rowArticulosDet = mysqli_fetch_row(mysqli_query($linkbd, $sqlArticulosDet));

            $unidad = $rowArticulosDet[0];
            $nomResponsable = buscatercero($responsable);
            
            $sqlDependencia = "SELECT nombrearea FROM planacareas WHERE codarea = $dependencia";
            $resDependencia = mysqli_query($linkbd, $sqlDependencia);
            $rowDependencia = mysqli_fetch_row($resDependencia);
            
            $nomDependencia = $rowDependencia[0];

            $datosTemp[] = $id;
            $datosTemp[] = $fecha;
            $datosTemp[] = $codArticulo;
            $datosTemp[] = $nomArticulo;
            $datosTemp[] = $unidad;
            $datosTemp[] = $responsable . " - " . $nomResponsable;
            $datosTemp[] = $dependencia . " - " . $nomDependencia;
            $datosTemp[] = $cantidad;
            $datosTemp[] = $valorUnitario;
            $datosTemp[] = $valorTotal;
            $datosTemp[] = $nomDependencia;
            $datosTemp[] = $nomResponsable;
            array_push($articulos, $datosTemp);
        }

        $out['articulos'] = $articulos;
    }

    if ($_POST["action"] == "eliminar") {
        $id = (int) $_POST["id"];
       
        $sql = "DELETE FROM alm_extra_contable WHERE id = $id";
        mysqli_query($linkbd, $sql);

        $arrResponse = array("status"=>true);
        echo json_encode($arrResponse);
    }

    header("Content-type: application/json");
    echo json_encode($out);
    die();