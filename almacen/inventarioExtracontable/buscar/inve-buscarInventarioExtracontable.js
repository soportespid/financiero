const URL = 'almacen/inventarioExtracontable/buscar/inve-buscarInventarioExtracontable.php';
const URLPDF = "inve-inventarioExtracontablePDF.php";
import { filtroEnArrayDeObjetos } from './../../../funciones.js';

var app = new Vue({
    el: '#myapp',
	data:{

        error: '',
        loading: false,
        showArticulos: false,
        listaArticulos: [],
        totalValores: 0,
        terceros: [],
        terceroscopy: [],
        tercero: '',
        showModalResponsables: false,
        searchCedula: '',
        dependencias: [],
        dependencia: '',
        nomTercero: '',
        
        fechaIni: '',
        fechaFin: '',
    },

    mounted: function(){

        this.datosIniciales();
    },

    methods: 
    {
        //Desglosar array
        toFormData: function(obj){
            var form_data = new FormData();
            for(var key in obj){
                form_data.append(key, obj[key]);
            }
            return form_data;
        },

        formatonumero: function(valor){
            return new Intl.NumberFormat("en-US", {style: "currency", currency: "USD"}).format(valor);
        },

        traeParametros: function(name){
            //Captura parametros
            name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
            var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
            results = regex.exec(location.search);
            return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
        },

        datosIniciales: async function() {
            
            await axios.post(URL+'?action=datosIniciales')
            .then((response) => {
            
                this.terceros = response.data.terceros;
                this.terceroscopy = response.data.terceros;
                this.dependencias = response.data.dependencias;
            });
        },

        filtroCedulaResponsable: async function () {

            const data = this.terceroscopy;
            var text = this.searchCedula;
            var resultado = [];

            resultado = await filtroEnArrayDeObjetos({'data': data, 'text': text});

            this.terceros = resultado;
        },

        seleccionaResponsable: function(documento, nombre) {

            if ((documento != "" && documento != null) && (nombre != "" && nombre != null)) {

                this.tercero = documento;
                this.nomTercero = nombre;
                this.showModalResponsables = false;
            }
        },

        buscarInventario: async function() {

            var formData = new FormData();
            formData.append("documento", this.tercero);
            formData.append("dependencia", this.dependencia);
            formData.append("fechaIni", this.fechaIni);
            formData.append("fechaFin", this.fechaFin);

            await axios.post(URL+'?action=buscaInventario', formData).then((response) => {
                
                this.listaArticulos = response.data.articulos;
            });
        },

        deleteArticulo: async function(item, index) {
            Swal.fire({
                title:"¿Estás segur@ de eliminar?",
                text:"",
                icon: 'warning',
                showCancelButton:true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText:"Sí, eliminar",
                cancelButtonText:"No, cancelar"
            }).then(async function(result){
                if(result.isConfirmed) {

                    const formData = new FormData();
                    formData.append("action","eliminar");
                    formData.append("id",item[0]);
                    const response = await fetch(URL,{method:"POST",body:formData});
                    // const objData = await response.json();

                    if (response.status == 200) {
                        app.listaArticulos.splice(index, 1);
                    }
                    else {
                        Swal.fire("Error",objData.msg,"warning");
                    }
                }
            });
        },

        pdf: function() {
            const form = document.createElement("form");
            form.method = "post";
            form.target = "_blank";
            form.action = URLPDF;       
            
            function addField(name,value){
                const input = document.createElement("input");
                input.type="hidden";
                input.name=name;
                input.value = value;
                form.appendChild(input);
            }
         
            addField("data", JSON.stringify(this.listaArticulos));
        
            document.body.appendChild(form);
            form.submit();
            document.body.removeChild(form);
        },

        visualizar: function(articulo) {

            const x = "inve-editarInventarioExtracontable?id="+articulo[0];
            window.open(x, '_blank');
            window.focus();
        },
    },
});