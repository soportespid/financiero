<?php
    header("Cache-control: no-cache, no-store, must-revalidate");
    header("Content-Type: text/html;charset=utf8");
	require_once '../../../comun.inc';
    require '../../../funciones.inc';
    require '../../../conversor.php';
    session_start();

	$linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");

    $out = array('error' => false);

    $action = "show";

    if(isset($_GET['action'])){
        $action = $_GET['action'];
    }

    if ($action == "datosIniciales") {

        $terceros = [];
        $articulos = [];
        $dependencias = [];

        $sqlTercero = "SELECT nombre1, nombre2, apellido1, apellido2, cedulanit, razonsocial, tipodoc FROM terceros WHERE estado = 'S'";
        $resTercero = mysqli_query($linkbd, $sqlTercero);
        while ($rowTerceros = mysqli_fetch_row($resTercero)) {
            
            $temporal = array();

            if ($rowTerceros[6] == "31") {

                $nombre = $rowTerceros[5];
            }
            else {

                $nombre = $rowTerceros[0] . " " . $rowTerceros[1] . " " . $rowTerceros[2] . " " . $rowTerceros[3];
            }

            array_push($temporal, $rowTerceros[4]);
            array_push($temporal, $nombre);
            array_push($terceros, $temporal);
        }

        $sqlArticulos = "SELECT codigo, grupoinven, nombre FROM almarticulos WHERE estado = 'S'";
        $resArticulos = mysqli_query($linkbd, $sqlArticulos);
        while ($rowArticulos = mysqli_fetch_row($resArticulos)) {
            
            $datosTemp = [];
            $codArticulo = $nombre = $unidad = "";

            $codArticulo = $rowArticulos[1].$rowArticulos[0];
            $nombre = $rowArticulos[2];

            $sqlUnidad = "SELECT unidad FROM almarticulos_det WHERE articulo = $codArticulo";
            $resUnidad = mysqli_query($linkbd, $sqlUnidad);
            $rowUnidad = mysqli_fetch_row($resUnidad);

            if ($rowUnidad[0] != null && $rowUnidad != "") {
                $unidad = $rowUnidad[0];
            }
            else {
                $unidad = "";
            }

            if ($unidad != "") {
                $datosTemp[] = $codArticulo;
                $datosTemp[] = $nombre;
                $datosTemp[] = $unidad;
                array_push($articulos, $datosTemp);
            }
        }
        
        $sqlDependencia = "SELECT codarea, nombrearea FROM planacareas WHERE estado = 'S'";
        $resDependencia = mysqli_query($linkbd, $sqlDependencia);
        while ($rowDependencia = mysqli_fetch_row($resDependencia)) {
            
            array_push($dependencias, $rowDependencia);
        }

        $out['dependencias'] = $dependencias;
        $out['terceros'] = $terceros;
        $out['articulos'] = $articulos;
    }

    if ($action == "guardar") {

        $articulos = [];
        $articulos = $_POST['listaArticulosGuardar'];

        foreach ($articulos as $key => $articulo) {
            
            preg_match("/([0-9]{2})\/([0-9]{2})\/([0-9]{4})/", $articulo[0], $f);
            $fecha = $f[3]."-".$f[2]."-".$f[1];
            $codArticulo = $articulo[1];
            $nomArticulo = $articulo[2];
            $unidadMedida = $articulo[3];
            $tercero = $articulo[4];
            $dependencia = $articulo[5];
            $cantidad = $articulo[6];
            $valorUnidad = $articulo[7];
            $valorTotal = $articulo[8];
            $id = selconsecutivo('alm_extra_contable', 'id');

            $sqlInsert = "INSERT INTO alm_extra_contable (id, cod_articulo, fecha_compra, dependencia, responsable, cantidad, valor_unitario, valor_total) VALUES ($id, '$codArticulo', '$fecha', '$dependencia', '$tercero', $cantidad, $valorUnidad, $valorTotal)";
            mysqli_query($linkbd, $sqlInsert);
        }

        $out['insertaBien'] = true;
    }

    header("Content-type: application/json");
    echo json_encode($out);
    die();