<?php
    header("Cache-control: no-cache, no-store, must-revalidate");
    header("Content-Type: text/html;charset=utf8");
	require_once '../../../comun.inc';
    require '../../../funciones.inc';
    require '../../../conversor.php';
    session_start();

	$linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");

    $out = array('error' => false);

    $action = "show";

    if(isset($_GET['action'])){
        $action = $_GET['action'];
    }

    if ($action == "buscaActas") {

        $fechaIni = $fechaFin = $tipoActa = $filtroFechas = $filtroActa = "";
        $datos = [];

        if ($_POST['fechaIni'] != "" && $_POST['fechaFin'] != "") {
            preg_match("/([0-9]{2})\/([0-9]{2})\/([0-9]{4})/", $_POST["fechaIni"], $fecha);
            $fechaIni = $fecha[3]."-".$fecha[2]."-".$fecha[1];
            
            preg_match("/([0-9]{2})\/([0-9]{2})\/([0-9]{4})/", $_POST["fechaFin"], $fecha);
            $fechaFin = $fecha[3]."-".$fecha[2]."-".$fecha[1];

            $filtroFechas = "AND fecha BETWEEN '$fechaIni' AND '$fechaFin' ";
        }

        if ($_POST['tipoActa'] != "") {

            $tipoActa = $_POST['tipoActa'];

            $filtroActa = "AND tipo_mov LIKE '$tipoActa' ";
        }

        $sqlSearch = "SELECT tipo_mov, consecutivo, fecha, motivo, valortotal, valorsaldo FROM almactoajusteent WHERE estado = 'A' $filtroActa $filtroFechas ORDER BY fecha DESC, consecutivo DESC  ";
        $resSearch = mysqli_query($linkbd, $sqlSearch);
        while ($rowSearch = mysqli_fetch_row($resSearch)) {
            
            array_push($datos, $rowSearch);
        }

        $out['datos'] = $datos;
    }

    if ($action == "eliminaActas") {

        $tipoMov = $consecutivo = "";

        $tipoMov = $_POST['tipoMov'];
        $consecutivo = $_POST['consecutivo'];

        $sqlDeteleCab = "DELETE FROM almactoajusteent WHERE tipo_mov = '$tipoMov' AND consecutivo = '$consecutivo'";
        mysqli_query($linkbd, $sqlDeteleCab);

        $sqlDeleteDet = "DELETE FROM almactoajusteentarticu WHERE idacto = '$consecutivo' AND tipo_mov = '$tipoMov'";
        mysqli_query($linkbd, $sqlDeleteDet);

        $sqlDeleteFirma = "DELETE FROM almactoajusteentpartici WHERE idacto = '$consecutivo' AND tipo_mov = '$tipoMov'";
        mysqli_query($linkbd, $sqlDeleteFirma);
    }

    header("Content-type: application/json");
    echo json_encode($out);
    die();