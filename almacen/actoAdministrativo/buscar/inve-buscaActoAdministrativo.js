const URL = 'almacen/actoAdministrativo/buscar/inve-buscaActoAdministrativo.php';
import { filtroEnArrayDeObjetos } from './../../../funciones.js';

var app = new Vue({
    el: '#myapp',
	data:{

        error: '',
        loading: false,
        tipoActo: '',
        listadoActos: [],
    },

    mounted: function(){

    },

    methods: 
    {
        //Desglosar array
        toFormData: function(obj){
            var form_data = new FormData();
            for(var key in obj){
                form_data.append(key, obj[key]);
            }
            return form_data;
        },

        formatonumero: function(valor){
            return new Intl.NumberFormat("en-US", {style: "currency", currency: "USD"}).format(valor);
        },

        traeParametros: function(name){
            //Captura parametros
            name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
            var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
            results = regex.exec(location.search);
            return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
        },

        buscarActos: function() {

            const fechaIni = document.getElementById('fechaIni').value;
            const fechaFin = document.getElementById('fechaFin').value;

            if ((this.tipoActo != "") || (fechaIni != "" && fechaFin != "")) {

                var formData = new FormData();
                formData.append("fechaIni", fechaIni);
                formData.append("fechaFin", fechaFin);
                formData.append("tipoActa", this.tipoActo);

                axios.post(URL+'?action=buscaActas', formData).then((response) => {

                    console.log(response.data);
                    this.listadoActos = response.data.datos;
                });
            }
            else {
                Swal.fire({
                    icon: 'warning',
                    title: 'Falta información',
                    text: 'Debes buscar por fechas y por tipo de acto'
                })     
            }
        },

        eliminar: function(actos) {

           if (actos[5] > 0) {

                Swal.fire({
                    icon: 'question',
                    title: 'Esta seguro que quiere eliminar?',
                    showDenyButton: true,
                    confirmButtonText: 'Eliminar!',
                    denyButtonText: 'Cancelar',
                    }).then((result) => {
                    if (result.isConfirmed) {

                        var formData = new FormData();
                        formData.append("tipoMov", actos[0]);
                        formData.append("consecutivo", actos[1]);
        
                        axios.post(URL+'?action=eliminaActas', formData);

                        var i = this.listadoActos.indexOf( actos );
                    
                        if ( i !== -1 ) {
                            this.listadoActos.splice( i, 1 );
                        }   

                        Swal.fire({
                            icon: 'success',
                            title: 'Eliminado con exito',
                            showConfirmButton: false,
                            timer: 1500
                        });
                    } 
                })  
           }
           else {
                Swal.fire({
                    icon: 'warning',
                    title: 'Error',
                    text: 'Este acto ya se le realizo una entrada'
                })    
           }
        },

        visualizar: function(actos) {

            const x = "inve-visualizarActoAdministrativo?tipoMov="+actos[0]+"&conse="+actos[1];
            window.open(x, '_blank');
            window.focus();
        },
    },
});