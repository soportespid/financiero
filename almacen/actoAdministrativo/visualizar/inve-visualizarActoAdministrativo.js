const URL = 'almacen/actoAdministrativo/visualizar/inve-visualizarActoAdministrativo.php';
import { filtroEnArrayDeObjetos } from './../../../funciones.js';

var app = new Vue({
    el: '#myapp',
	data:{

        error: '',
        loading: false,
        error: '',
        loading: false,
        movimiento: '',
        motivo: '',
        consecutivo: '',
        descripcion: '',
        listaArticulos: [],
        totalValores: 0,
        ciudad: '',
        lugar: '',
        tercero: '',
        nomTercero: '',
        fechaRegistro: '',
    },

    mounted: function(){

        this.traeInformacion();
    },

    methods: 
    {
        //Desglosar array
        toFormData: function(obj){
            var form_data = new FormData();
            for(var key in obj){
                form_data.append(key, obj[key]);
            }
            return form_data;
        },

        formatonumero: function(valor){
            return new Intl.NumberFormat("en-US", {style: "currency", currency: "USD"}).format(valor);
        },

        traeParametros: function(name){
            //Captura parametros
            name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
            var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
            results = regex.exec(location.search);
            return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
        },

        traeInformacion: function() {

            const tipoMov = this.traeParametros('tipoMov');
            const consecutivo = this.traeParametros('conse');

            var formData = new FormData();
            formData.append("tipoMov", tipoMov);
            formData.append("consecutivo", consecutivo);

            axios.post(URL+'?action=buscaInformacion', formData).then((response) => {

                console.log(response.data);
                this.movimiento = response.data.movimiento;
                this.consecutivo = response.data.consecutivo;
                this.fechaRegistro = response.data.fechaRegistro;
                this.motivo = response.data.motivo;
                this.ciudad = response.data.ciudad;
                this.lugar = response.data.lugar;
                this.tercero = response.data.tercero;
                this.nomTercero = response.data.nomTercero;
                this.descripcion = response.data.descripcion;
                this.totalValores = response.data.total;
                this.listaArticulos = response.data.datos;
            });
        },
    },
});