<?php
    header("Cache-control: no-cache, no-store, must-revalidate");
    header("Content-Type: text/html;charset=utf8");
	require_once '../../../comun.inc';
    require '../../../funciones.inc';
    require '../../../conversor.php';
    session_start();

	$linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");

    $out = array('error' => false);

    $action = "show";

    if(isset($_GET['action'])){
        $action = $_GET['action'];
    }

    if ($action == "buscaInformacion") {

        $tipoMov = $consecutivo = "";
        $datos = [];

        $tipoMov = $_POST['tipoMov'];
        $consecutivo = $_POST['consecutivo'];

        $sqlCab = "SELECT fecha, doctercero, nomtercero, valortotal, ciudad, lugarfisico, motivo, otrosdetalles, tipo_mov, consecutivo FROM almactoajusteent WHERE tipo_mov = '$tipoMov' AND consecutivo = '$consecutivo'";
        $resCab = mysqli_query($linkbd, $sqlCab);
        $rowCab = mysqli_fetch_row($resCab);
            
        $fechaRegistro = date("d/m/Y", strtotime($rowCab[0]));
        $tercero = $rowCab[1];
        $nomTercero = $rowCab[2];
        $valorTotal = $rowCab[3];
        $ciudad = $rowCab[4];
        $lugar = $rowCab[5];
        $motivo = $rowCab[6];
        $descripcion = $rowCab[7];
        $total = $rowCab[3];
        
        if ($rowCab[8] == '104') {
            $mov = "104 - Acto administrativo por ajuste";
        }
        else if ($rowCab[8] == '107') {
            $mov = "107 - Acto administrativo por donación";
        }

        $conse = $rowCab[9];

        $sqlDet = "SELECT codigo, descripcion, unumedida, estadou, cantidad, valor FROM almactoajusteentarticu WHERE tipo_mov = '$tipoMov' AND idacto = '$consecutivo' ORDER BY id ASC";
        $resDet = mysqli_query($linkbd, $sqlDet);
        while ($rowDet = mysqli_fetch_row($resDet)) {
            
            $temporal = [];

            array_push($temporal, $rowDet[0]);
            array_push($temporal, $rowDet[1]);
            array_push($temporal, $rowDet[2]);
            if($rowDet[3] == 'N') {
                array_push($temporal, "Nuevo");
            }
            else if ($rowDet[3] == 'U') {
                array_push($temporal, "Usado");    
            }
            array_push($temporal, $rowDet[4]);
            array_push($temporal, $rowDet[5]);
            $valorTotal = round($rowDet[4] * $rowDet[5], 2);
            array_push($temporal, $valorTotal);
            
            array_push($datos, $temporal);
        }

        $out['movimiento'] = $mov;
        $out['consecutivo'] = $conse;
        $out['fechaRegistro'] = $fechaRegistro;
        $out['motivo'] = $motivo;
        $out['ciudad'] = $ciudad;
        $out['lugar'] = $lugar;
        $out['tercero'] = $tercero;
        $out['nomTercero'] = $nomTercero;
        $out['descripcion'] = $descripcion;
        $out['total'] = $total;
        $out['datos'] = $datos;
    }

    header("Content-type: application/json");
    echo json_encode($out);
    die();