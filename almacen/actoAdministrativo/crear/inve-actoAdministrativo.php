<?php
    header("Cache-control: no-cache, no-store, must-revalidate");
    header("Content-Type: text/html;charset=utf8");
	require_once '../../../comun.inc';
    require '../../../funciones.inc';
    require '../../../funcionesSP.inc.php';
    require '../../../conversor.php';
    session_start();

	$linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");

    $out = array('error' => false);

    $action = "show";

    if(isset($_GET['action'])){
        $action = $_GET['action'];
    }

    if ($action == "datosIniciales") {

        $terceros = [];
        $articulos = [];
        $bodegas = [];
        $cc = [];

        $sqlBodegas = "SELECT id_cc, nombre FROM almbodegas WHERE estado = 'S'";
        $resBodegas = mysqli_query($linkbd, $sqlBodegas);
        while ($rowBodegas = mysqli_fetch_row($resBodegas)) {
            
            array_push($bodegas, $rowBodegas);
        }

        $sqlCentroCosto = "SELECT id_cc, nombre FROM centrocosto WHERE estado='S' AND entidad='S' ORDER BY id_cc";
        $resCentroCosto = mysqli_query($linkbd, $sqlCentroCosto);
        while ($rowCentroCosto = mysqli_fetch_row($resCentroCosto)) {
            
            array_push($cc, $rowCentroCosto);
        }

        $sqlTercero = "SELECT nombre1, nombre2, apellido1, apellido2, cedulanit, razonsocial, tipodoc FROM terceros WHERE estado = 'S'";
        $resTercero = mysqli_query($linkbd, $sqlTercero);
        while ($rowTerceros = mysqli_fetch_row($resTercero)) {
            
            $temporal = array();

            if ($rowTerceros[6] == "31") {

                $nombre = $rowTerceros[5];
            }
            else {

                $nombre = $rowTerceros[0] . " " . $rowTerceros[1] . " " . $rowTerceros[2] . " " . $rowTerceros[3];
            }

            array_push($temporal, $rowTerceros[4]);
            array_push($temporal, $nombre);
            array_push($terceros, $temporal);
        }

        $sqlArticulos = "SELECT codigo, grupoinven, nombre FROM almarticulos WHERE estado = 'S'";
        $resArticulos = mysqli_query($linkbd, $sqlArticulos);
        while ($rowArticulos = mysqli_fetch_row($resArticulos)) {
            
            $datosTemp = [];
            $codArticulo = $nombre = $unidad = "";

            $codArticulo = $rowArticulos[1].$rowArticulos[0];
            $nombre = $rowArticulos[2];

            $sqlUnidad = "SELECT unidad FROM almarticulos_det WHERE articulo = $codArticulo";
            $resUnidad = mysqli_query($linkbd, $sqlUnidad);
            $rowUnidad = mysqli_fetch_row($resUnidad);

            if ($rowUnidad[0] != null && $rowUnidad != "") {
                $unidad = $rowUnidad[0];
                $datosTemp[] = $codArticulo;
                $datosTemp[] = $nombre;
                $datosTemp[] = $unidad;
                array_push($articulos, $datosTemp);
            }
        }

        $out['terceros'] = $terceros;
        $out['articulos'] = $articulos;
        $out['cc'] = $cc;
        $out['bodegas'] = $bodegas;
    }

    if ($action == "buscaConse") {

        $tipoMov = $consecutivo = "";
        $tipoMov = $_POST['movimiento'];

        $sqlCab = "SELECT COALESCE(MAX(consecutivo),0) FROM almactoajusteent WHERE tipo_mov = '$tipoMov'";
        $resCab = mysqli_query($linkbd, $sqlCab);
        $rowCab = mysqli_fetch_row($resCab);

        $consecutivo = $rowCab[0] + 1;

        $out['conse'] = $consecutivo;
    }

    if ($action == "guardar") {

        $tipoMov = $consecutivo = $fecha = $motivo = $ciudad = $lugar = $tercero = $nomTercero = $descripcion = $usuario = "";
        $total = 0;
        $listadoArticulos = [];

        $tipoMov = $_POST['movimiento'];
        $consecutivo = $_POST['consecutivo'];
        preg_match("/([0-9]{2})\/([0-9]{2})\/([0-9]{4})/", $_POST["fecha"], $fecha);
        $fecha = $fecha[3]."-".$fecha[2]."-".$fecha[1];
        $motivo = strClean(replaceChar($_POST['motivo']));
        $ciudad = strClean(replaceChar($_POST['ciudad']));
        $lugar = strClean(replaceChar($_POST['lugar']));
        $tercero = $_POST['tercero'];
        $nomTercero = $_POST['nomTercero'];
        $descripcion = strClean(replaceChar($_POST['descripcion']));
        $total = $_POST['total'];
        $listadoArticulos = $_POST['listaArticulos'];
        $subtotal = 0;

        $id = selconsecutivo('almactoajusteent', 'id');
        
        foreach ($listadoArticulos as $key => $articulos) {
            
            $bodega = $cc = $codArticulo = $nomArticulo = $unidadMedida = $estadoArticulo = $cantidad = $valorUnidad = $valorTotal = "";

            $bodega = $articulos[0];
            $cc = $articulos[1];
            $codArticulo = $articulos[2];
            $nomArticulo = $articulos[3];
            $unidadMedida = $articulos[4];
            if ($articulos[5] == 'Nuevo') {
                $estadoArticulo = "N";
            } else {
                $estadoArticulo = "U";
            }
            $cantidad = $articulos[6];
            $valorUnidad = $articulos[7];
            $valorTotal = $articulos[8];

            $idDet = selconsecutivo('almactoajusteentarticu', 'id');

            $sqlActoArticulos = "INSERT INTO almactoajusteentarticu (id, idacto, codigo, descripcion, unumedida, cantidad, valor, estadou, estado, tipo_mov, bodega, cc) VALUES ($idDet, '$consecutivo', '$codArticulo', '$nomArticulo', '$unidadMedida', $cantidad, $valorUnidad, '$estadoArticulo', 'S', '$tipoMov', '$bodega', '$cc')";
            mysqli_query($linkbd, $sqlActoArticulos);

            $subtotal += $valorTotal;
        }

        $sqlActoCab = "INSERT INTO almactoajusteent (id, fecha, doctercero, nomtercero, valortotal, ciudad, lugarfisico, motivo, otrosdetalles, estado, tipo_mov, consecutivo, valorsaldo) VALUES ($id, '$fecha', '$tercero', '$nomTercero', $total, '$ciudad', '$lugar', '$motivo', '$descripcion', 'A', '$tipoMov', '$consecutivo', $subtotal)";
        mysqli_query($linkbd, $sqlActoCab);

        $sqlr="SELECT documento, funcionario, nomcargo FROM firmaspdf_det WHERE idfirmas='6' AND estado ='S'";
        $res=mysqli_query($linkbd,$sqlr);
        $rowCargo=mysqli_fetch_row($res);

        $documento = $rowCargo[0];
        $funcionario = $rowCargo[1];
        $cargo = $rowCargo[2];
        $idFirma = selconsecutivo('almactoajusteentpartici', 'id');

        $sqlFirma = "INSERT INTO almactoajusteentpartici (id, idacto, documento, nombre, cargo, estado, tipo_mov) VALUES ($idFirma, '$consecutivo', '$documento', '$funcionario', '$cargo', 'S', '$tipoMov')";
        mysqli_query($linkbd, $sqlFirma);

        $out['resultado'] = true;
    }

    header("Content-type: application/json");
    echo json_encode($out);
    die();