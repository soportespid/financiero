const URL = 'almacen/actoAdministrativo/crear/inve-actoAdministrativo.php';
import { filtroEnArrayDeObjetos } from './../../../funciones.js';

var app = new Vue({
    el: '#myapp',
	data:{

        error: '',
        loading: false,
        movimiento: '',
        motivo: '',
        consecutivo: '',
        descripcion: '',
        codArticulo: '',
        nomArticulo: '',
        cantidad: 0,
        unidadMedida: '',
        valorUnidad: 0,
        valorTotal: 0,
        showArticulos: false,
        articulos: [],
        articuloscopy: [],
        searchArticulo: '',
        listaArticulos: [],
        totalValores: 0,
        bodega: '',
        bodegas: [],
        cc: '',
        centrocostos: [],
        terceros: [],
        terceroscopy: [],
        ciudad: '',
        lugar: '',
        tercero: '',
        nomTercero: '',
        showModalResponsables: false,
        searchCedula: '',
        estadoArticulo: '',
    },

    mounted: function(){

        this.datosIniciales();
    },

    methods: 
    {
        //Desglosar array
        toFormData: function(obj){
            var form_data = new FormData();
            for(var key in obj){
                form_data.append(key, obj[key]);
            }
            return form_data;
        },

        formatonumero: function(valor){
            return new Intl.NumberFormat("en-US", {style: "currency", currency: "USD"}).format(valor);
        },

        traeParametros: function(name){
            //Captura parametros
            name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
            var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
            results = regex.exec(location.search);
            return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
        },

        datosIniciales: async function() {
            
            await axios.post(URL+'?action=datosIniciales')
            .then((response) => {
                    
                this.bodegas = response.data.bodegas;
                this.centrocostos = response.data.cc;
                this.articulos = response.data.articulos;
                this.articuloscopy = response.data.articulos;
                this.terceros = response.data.terceros;
                this.terceroscopy = response.data.terceros;
            });
        },

        buscaConsecutivo: async function(movimiento) {

            if (movimiento != "") {

                var formData = new FormData();
                formData.append("movimiento", movimiento);

                await axios.post(URL+'?action=buscaConse', formData).then((response) => {

                    this.consecutivo = response.data.conse;
                });
            }
            else {
                this.consecutivo = "";
            }
        },

        despliegaModalTerceros: function() {

            this.showModalResponsables = true;
        },

        filtroCedulaResponsable: async function () {

            const data = this.terceroscopy;
            var text = this.searchCedula;
            var resultado = [];

            resultado = await filtroEnArrayDeObjetos({'data': data, 'text': text});

            this.terceros = resultado;
        },

        seleccionaResponsable: function(documento, nombre) {

            if ((documento != "" && documento != null) && (nombre != "" && nombre != null)) {

                this.tercero = documento;
                this.nomTercero = nombre;
                this.showModalResponsables = false;
            }
        },

        despliegaModalArticulos: function() {

            this.showArticulos = true;
        },

        filtroArticulos: async function() {

            const data = this.articuloscopy;
            var text = this.searchArticulo;
            var resultado = [];

            resultado = await filtroEnArrayDeObjetos({'data': data, 'text': text});

            this.articulos = resultado;
        },

        seleccionarArticulo: function(cod, nom, unidad) {

            if ((cod != "" && cod != null) && (nom != "" && nom != null) && (unidad != "" && unidad != null)) {

                this.codArticulo = cod;
                this.nomArticulo = nom;
                this.unidadMedida = unidad;
                this.showArticulos = false;
            } 
        },

        validaCantidad: function(cantidad) {
            
            if (cantidad >= 1) {

                if(Number.isInteger(parseFloat(cantidad))) {
                    this.valorTotal = this.cantidad * this.valorUnidad;
                }
                else {
                    Swal.fire({
                        icon: 'info',
                        title: 'Formato no valido',
                        text: 'La cantidad debe ser un número entero.'
                    })     
                    this.cantidad = 0;
                }
            }
            else {
                Swal.fire({
                    icon: 'info',
                    title: 'Cantidad no valida',
                    text: 'La cantidad debe ser mayor o igual a uno.'
                })     
                this.cantidad = 0;
            }
        },

        validaValorUnidad: function(valor) {

            if (this.cantidad >= 1) {
                if (valor >= 1) {

                    this.valorTotal = this.cantidad * this.valorUnidad;
                }
                else {
                    Swal.fire({
                        icon: 'info',
                        title: 'Valor no valido',
                        text: 'El valor debe ser mayor o igual a uno.'
                    })     
                    this.valorUnidad = 0;
                    this.valorTotal = 0;
                }
            }
            else {
                Swal.fire({
                    icon: 'info',
                    title: 'Cantidad no ingresada',
                    text: 'Ingrese primero la cantidad de articulos.'
                })     
                this.valorUnidad = 0;
                this.valorTotal = 0;
            }
        },

        agregaArticulo: async function(){

            if (this.codArticulo != "" && this.nomArticulo != "" && this.unidadMedida != "") {
                if (this.bodega != "" && this.cc != "") {
                    if (this.cantidad >= 1 && this.valorUnidad >= 1 && this.valorTotal >= 1) {

                        const data = this.listaArticulos;
                        var text = this.codArticulo;
                        var resultado = [];
            
                        resultado = await filtroEnArrayDeObjetos({'data': data, 'text': text});

                        if (resultado.length == 0) {

                            var temporal = [];
                            temporal.push(this.bodega);
                            temporal.push(this.cc);
                            temporal.push(this.codArticulo);
                            temporal.push(this.nomArticulo);
                            temporal.push(this.unidadMedida);
                            temporal.push(this.estadoArticulo);
                            temporal.push(this.cantidad);
                            temporal.push(this.valorUnidad);
                            temporal.push(this.valorTotal);
                            this.totalValores += this.valorTotal;
                            this.listaArticulos.push(temporal);

                            this.bodega = this.cc = this.codArticulo = this.nomArticulo = this.unidadMedida = "";
                            this.cantidad = this.valorUnidad = this.valorTotal = 0;
                        }
                        else {
                            Swal.fire({
                                icon: 'info',
                                title: 'Articulo ya ingresado',
                                text: 'Este articulo ya fue ingresado, si quiere modificarlo debes eliminarlo y volverlo a agregar.'
                            })   
                        }
                    }
                    else {
                        Swal.fire({
                            icon: 'info',
                            title: 'Falta información',
                            text: 'Cantidad, valor unidad o valor total'
                        })     
                    }
                }
                else {
                    Swal.fire({
                        icon: 'info',
                        title: 'Falta información',
                        text: 'Bodega o centros de costos'
                    })         
                }
            }
            else {
                Swal.fire({
                    icon: 'info',
                    title: 'Falta información',
                    text: 'Código de articulo, nombre o unidad de medida'
                })     
            }
        },

        eliminarArticuloAgregado: function(articulo) {

            Swal.fire({
                icon: 'question',
                title: 'Esta seguro que quiere eliminar?',
                showDenyButton: true,
                confirmButtonText: 'Eliminar!',
                denyButtonText: 'Cancelar',
                }).then((result) => {
                if (result.isConfirmed) {

                    this.totalValores = parseFloat(this.totalValores) - parseFloat(articulo[8]);

                    var i = this.listaArticulos.indexOf( articulo );
                    
                    if ( i !== -1 ) {
                        this.listaArticulos.splice( i, 1 );
                    }   
                } 
            })  
        },

        guardar: async function() {

            var fecha = document.getElementById('fechaRegistro').value;
 
            if (this.movimiento != "" && this.consecutivo != "" && fecha != "" && this.motivo != "" && this.ciudad != "" && this.lugar != "" && this.tercero != "" && this.nomTercero != "" && this.descripcion != "") {

                if (this.listaArticulos.length >= 1) {

                    Swal.fire({
                        icon: 'question',
                        title: 'Esta seguro que quiere guardar?',
                        showDenyButton: true,
                        confirmButtonText: 'Guardar!',
                        denyButtonText: 'Cancelar',
                        }).then((result) => {
                        if (result.isConfirmed) {

                            var formData = new FormData();

                            formData.append("movimiento", this.movimiento);
                            formData.append("consecutivo", this.consecutivo);
                            formData.append("fecha", fecha);
                            formData.append("motivo", this.motivo);
                            formData.append("ciudad", this.ciudad);
                            formData.append("lugar", this.lugar);
                            formData.append("tercero", this.tercero);
                            formData.append("nomTercero", this.nomTercero);
                            formData.append("descripcion", this.descripcion);
                            formData.append("total", this.totalValores);

                            for(let i=0; i <= this.listaArticulos.length-1; i++) {

                                const val = Object.values(this.listaArticulos[i]).length-1;

                                for(let x = 0; x <= val; x++) {
                                    formData.append("listaArticulos["+i+"][]", Object.values(this.listaArticulos[i])[x]);
                                }
                            }

                            axios.post(URL+'?action=guardar', formData).then((response) => {

                                if (response.data.resultado == true) {

                                    Swal.fire({
                                        position: 'top-end',
                                        icon: 'success',
                                        title: 'Guardado con exito',
                                        showConfirmButton: false,
                                        timer: 1500
                                    }).then((response) => {
                                        this.redireccionar();
                                    });
                                }
                            });
                        }
                    })
                }
                else {
                    Swal.fire({
                        icon: 'info',
                        title: 'Falta información',
                        text: 'Verifica haber agregado articulos al detalles del documento.'
                    })        
                }
            }
            else {
                Swal.fire({
                    icon: 'info',
                    title: 'Falta información',
                    text: 'Verifique que no falten datos en la cabecera del documento.'
                })    
            }
        },

        redireccionar: function () {

            location.href = "inve-actoAdministrativo";
        },
    },
});