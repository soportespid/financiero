const URL ='almacen/EntradaDonacion/buscar/inve-buscarEntradaDonacion.php';

var app = new Vue({
    el:"#myapp",
    data() {
        return {
            isLoading:false,
            intResults:0,
            intTotalPages:1,
            intPage:1,
            strSearch:"",
            arrData:[]

        }
    },
    mounted() {
        this.search();
    },
    methods: {
        search: async function(page=1){
            if(page <= 0){
                page = 1;
                this.intPage = page;
            }else if(page > this.intTotalPages){
                page = this.intTotalPages;
                this.intPage = page;
            }

            this.isLoading = true;
            const formData = new FormData();
            formData.append("action","search");
            formData.append("search",this.strSearch);
            formData.append("page",page);
            const response = await fetch(URL,{method:"POST",body:formData});
            const objData = await response.json();
            this.arrData = objData.data;
            this.intResults = objData.total;
            this.intTotalPages = objData.total_pages;
            this.isLoading = false;
        },
        editItem: function(codigo,tipo){
            if(tipo == 1){
                window.location.href='inve-verEntradaDonacion.php?id='+codigo
            }
        }
    },
})