const URL ='almacen/EntradaDonacion/crear/inve-gestionEntradaDonacion.php';

var app = new Vue({
	el:"#myapp",
	data() {
		return {
			isLoading:false,
			isModalArticulos:false,
			isModalRegistro:false,
			isModalDocumento:false,
			isSelectAll:true,
			strConcepto:"",
			strDescripcion:"",
			strSearch:"",
			strBuscarCodArticulo:"",
			strMedida:"",
			strModelo:"",
			strMarca:"",
			strSerie:"",
			intValor:"",
			intCantidad:"",
			intResults:0,
			intPage:1,
			intTotalPages:1,
			intConsecutivo:0,
			intTotalValor:0,
			intTotalReversion:0,
			arrProductos:[],
			arrRegistros:[],
			arrDocumentos:[],
			arrRevProductos:[],
			arrConceptos:[],
			objArticulo:{"codigo":"","nombre":"","unidad":"","codunspsc":""},
			objRegistro:{"consecutivo":"","motivo":"","valortotal":"","valorsaldo":0},
			objDocumento:{"consec":"","nombre":"","valortotal":0},
			selectMovimiento:1,
			selectEntrada:"01",
			selectConcepto:0,
			intRevConsecutivo:0,
			intRevDocumento:"",
			strRevDescripcion:"",
		}
	},
	mounted() {
		this.getData();
	},
	methods: {  
		getData: async function(){
			const formData = new FormData();
			formData.append("action","get");
			const response = await fetch(URL,{method:"POST",body:formData});
			const objData = await response.json();
			this.intConsecutivo = objData.consecutivo;
			this.arrRegistros = objData.registros;
			this.arrConceptos = objData.conceptos;
			console.log(objData);
			//Datos reversar
			this.intRevConsecutivo = objData.rev_consecutivo;
			this.arrDocumentos = objData.documentos_entradas;
		},
		selectItem: function(item,type){
			if(type=="documento"){
				this.objDocumento = item;
				this.arrRevProductos = this.objDocumento.articulos;
				this.intTotalReversion = this.objDocumento.valortotal;
			}else{
				if(this.selectConcepto == ""){
					Swal.fire("Error","Por favor, elija el concepto contable","error");
					return false;
				}
				this.objRegistro = item;
				let total = 0;
                const arrProductos = this.objRegistro.articulos;
                arrProductos.forEach(pro => {
                    pro['credito'] = this.strConcepto;
                    total+=pro['valortotal'];
                });
				this.arrProductos = arrProductos;
                this.intTotalValor=total;
			}
		},
		save:async function(){
			const formData = new FormData();
			let obj;
			if(this.selectMovimiento == 1){
				if(this.intConsecutivo == "" || this.strDescripcion == "" || document.querySelector("#fechaInicial").value==""){
					Swal.fire("Error","Todos los campos de la cabecera documento son obligatorios.","error");
					return false;
				}
				if(this.objRegistro.consecutivo ==""){
					Swal.fire("Error","Por favor, seleccione un acto administrativo.","error");
					return false;
				}
				if(this.selectConcepto == ""){
					Swal.fire("Error","Debe elegir un concepto contable","error");
					return false;
				}
				obj = {
					"movimiento":1,
					"cabecera":{
						"consecutivo":this.intConsecutivo,
						"fecha":document.querySelector("#fechaInicial").value,
						"descripcion":this.strDescripcion,
						"registro":{
							"idrp":this.objRegistro.consecutivo,
							"centro":this.arrProductos[0].cc,
							"bodega":this.arrProductos[0].bodega,
							"total":this.intTotalValor,
                            "saldo":this.objRegistro.valorsaldo
						}
					},
					"detalle":this.arrProductos
				}
			}else{
				if(this.strRevDescripcion == ""){
					Swal.fire("Error","Por favor, escribe el motivo o razón de reversión","error");
					return false;
				}
				if(this.objDocumento.consec == ""){
					Swal.fire("Error","Por favor, elige el documento a reversar","error");
					return false;
				}
				obj = {
					"movimiento":3,
					"cabecera":{
						"consecutivo":this.intRevConsecutivo,
						"fecha":document.querySelector("#fechaR").value,
						"descripcion":this.strRevDescripcion,
						"registro":{
							"idrp":this.objDocumento.codmov,
							"centro":this.objDocumento.cc,
							"bodega":this.objDocumento.bodega1,
							"total":this.intTotalReversion,
							"entrada_consec":this.objDocumento.consec
						}
					},
					"detalle":this.arrRevProductos
				}
			}
			this.isLoading = true;
			formData.append("entrada",JSON.stringify(obj));
			formData.append("action","save");
			const response = await fetch(URL,{method:"POST",body:formData});
			const objData = await response.json();
			if(!objData)window.location.reload();
			if(objData.status && objData.status_comp){
				this.arrProductos = [];
				this.strDescripcion = "";
				this.intTotalValor=0;
				this.objRegistro ={"consecutivo":"","motivo":"","valortotal":"","valorsaldo":0},
				this.intTotalReversion = 0;
				this.objDocumento.consec = "";
				this.arrRevProductos = [];
				this.strRevDescripcion= "";
				this.objDocumento.nombre = "";
                this.selectConcepto=0;
				this.getData();
				Swal.fire("Guardado",objData.msg,"success");
			}else if(!objData.status_comp){
				this.arrProductos = [];
				this.strDescripcion = "";
				this.intTotalValor=0;
				this.objRegistro ={"consecutivo":"","motivo":"","valortotal":"","valorsaldo":0},
				this.intTotalReversion = 0;
				this.objDocumento.consec = "";
				this.arrRevProductos = [];
				this.objDocumento.nombre = "";
                this.selectConcepto=0;
				this.getData();
				Swal.fire("Error",objData.msg,"error");
			}else if(!objData.status){
				Swal.fire("Error",objData.msg,"error");
			}
			this.isLoading = false;
		},
        getConcepto: async function (){
            let formData = new FormData();
			formData.append("action","concepto");
            formData.append("id",this.selectConcepto);
            this.isLoading = true;
            const response = await fetch(URL,{method:"POST",body:formData});
            this.strConcepto = await response.json();
            if(this.arrProductos != ""){
				const arrProductos = this.objRegistro.articulos;
				arrProductos.forEach(pro => {pro['credito'] = this.strConcepto;});
				this.arrProductos = arrProductos;
			}
            this.isLoading = false;

        },
		formatNumero: function(valor){
			return new Intl.NumberFormat("en-US", {style: "currency", currency: "USD"}).format(valor);
		},
		validInteger:function(valor){
			valor = new String(valor);
			if(valor.includes(".") || valor.includes(",")){
				Swal.fire("Error","La cantidad debe ser un número entero.","error");
			}
		},
        addEsp:function(e,codigo){
            const element = e.target;
            const type = element.getAttribute("data-type");
            const index = this.arrProductos.map(el=>el.codigo).indexOf(codigo);

            if(type == "modelo"){
                this.arrProductos[index].modelo = element.value;
            }else if(type == "marca"){
                this.arrProductos[index].marca = element.value;
            }else{
                this.arrProductos[index].serie = element.value;
            }
        }
	},
})