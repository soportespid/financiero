<?php
    require_once '../../Librerias/core/Helpers.php';
    require_once '../../Librerias/core/Mysql.php';
    session_start();
    ini_set('display_errors', '1');
    ini_set('display_startup_errors', '1');
    error_reporting(E_ERROR);
    class InformeMovimientosModel extends Mysql{
        public function getAllMovimientos($fecha_inicial, $fecha_final) {
            $sql_movimientos = "SELECT codigo, 
            fecha, 
            CONCAT(tipomov, tiporeg) AS tipo_movimiento,
            tipomov,
            consec AS consecutivo, 
            nombre AS detalle, 
            valortotal AS valor_total, 
            estado 
            FROM almginventario 
            WHERE fecha BETWEEN '$fecha_inicial' AND '$fecha_final' 
            ORDER BY fecha ASC, codigo ASC";
            $row_movimientos = $this->select_all($sql_movimientos);

            foreach ($row_movimientos as $key => $movimiento) {
                if ($movimiento["tipo_movimiento"] == "101") {
                    $row_movimientos[$key]["nombre_movimiento"] = "Entrada por compra";
                    $row_movimientos[$key]["url"] = "inve-verEntradaCompra";
                } else if ($movimiento["tipo_movimiento"] == "104") {
                    $row_movimientos[$key]["nombre_movimiento"] = "Entrada por ajuste";
                    $row_movimientos[$key]["url"] = "inve-verEntradaAjuste";
                } else if ($movimiento["tipo_movimiento"] == "107") {
                    $row_movimientos[$key]["nombre_movimiento"] = "Entrada por donación";
                    $row_movimientos[$key]["url"] = "inve-verEntradaDonacion";
                } else if ($movimiento["tipo_movimiento"] == "203") {
                    $row_movimientos[$key]["nombre_movimiento"] = "Salida por traslado activos fijos";
                    $row_movimientos[$key]["url"] = "inve-visualizarSalidaActivosFijos";
                } else if ($movimiento["tipo_movimiento"] == "206") {
                    $row_movimientos[$key]["nombre_movimiento"] = "Salida directa";
                    $row_movimientos[$key]["url"] = "inve-visualizar-salidaDirecta";
                } else {
                    $row_movimientos[$key]["nombre_movimiento"] = "";
                    $row_movimientos[$key]["url"] = "";
                }

                $detalle = ucfirst(strtolower(replaceChar(strClean($movimiento["detalle"]))));
                $row_movimientos[$key]["detalle"] = $detalle;
            }
            return $row_movimientos;
        }
    }
?>
