<?php 
require_once 'comun.inc';
require 'funciones.inc';
require 'funcionesSP.inc.php';
header("Cache-control: no-cache, no-store, must-revalidate");
header("Content-Type: text/html;charset=utf8");
session_start();

    $linkbd = conectar_v7();
    $linkbd -> set_charset("utf8");

    $sqlCorteDet = "SELECT id_corte, id_cliente AS clienteId, numero_facturacion AS factura FROM srvcortes_detalle WHERE estado_pago = 'S' AND id_corte = 15 ORDER BY numero_facturacion";
    $resCorteDet = mysqli_query($linkbd, $sqlCorteDet);
    while ($rowCorteDet = mysqli_fetch_assoc($resCorteDet)) {

        $factura = $rowCorteDet["factura"];
        $corteAnt = $rowCorteDet["id_corte"] - 1;
    
        $sqlCorteDet2 = "SELECT numero_facturacion AS facturaAnterior, estado_pago FROM srvcortes_detalle WHERE id_corte = $corteAnt AND id_cliente = $rowCorteDet[clienteId]";
        $rowCorteDet2 = mysqli_fetch_assoc(mysqli_query($linkbd, $sqlCorteDet2));
    
        $facturaAnt = $rowCorteDet2["facturaAnterior"];
        $estadoPago = $rowCorteDet2["estado_pago"];

        if ($estadoPago == 'V') {
            $sqlFactura = "SELECT id_servicio FROM srvfacturas WHERE num_factura = $factura ORDER BY id_servicio ASC";
            $resFactura = mysqli_query($linkbd, $sqlFactura);
            while ($rowFactura = mysqli_fetch_assoc($resFactura)) {
                
                $valorDeudaServicio = $valorIntereses = $interesMes = 0;
        
                $sqlValue = "SELECT credito AS valueCredito FROM srvdetalles_facturacion WHERE numero_facturacion = $facturaAnt AND id_servicio = $rowFactura[id_servicio] AND tipo_movimiento = '101' AND id_tipo_cobro = 10";
                $rowValue = mysqli_fetch_assoc(mysqli_query($linkbd, $sqlValue));
                $valorIntereses = $rowValue["valueCredito"];
        
                $sqlValueDeuda = "SELECT SUM(credito) AS valueCredito, SUM(debito) AS valueDebito FROM srvdetalles_facturacion WHERE numero_facturacion = $facturaAnt AND id_servicio = $rowFactura[id_servicio] AND tipo_movimiento = '101' AND id_tipo_cobro != 10";
                $rowValueDeuda = mysqli_fetch_assoc(mysqli_query($linkbd, $sqlValueDeuda));
                $valorDeudaServicio = $rowValueDeuda["valueCredito"] - $rowValueDeuda["valueDebito"];
                
                $interesMes = $valorDeudaServicio * 0.05;
                $valorIntereses = $valorIntereses + $interesMes;
        
                $valorDeudaServicio = round($valorDeudaServicio, 2);
                $valorIntereses = round($valorIntereses, 2);
        
                $update = "UPDATE srvfacturas SET deuda_anterior = $valorDeudaServicio, interes_mora = $valorIntereses WHERE num_factura = $factura AND id_servicio = $rowFactura[id_servicio]";
                mysqli_query($linkbd, $update);
        
                $update2 = "UPDATE srvdetalles_facturacion SET credito = $valorDeudaServicio WHERE numero_facturacion = $factura AND id_servicio = $rowFactura[id_servicio] AND id_tipo_cobro = 8";
                mysqli_query($linkbd, $update2);
        
                $update3 = "UPDATE srvdetalles_facturacion SET credito = $valorIntereses WHERE numero_facturacion = $factura AND id_servicio = $rowFactura[id_servicio] AND id_tipo_cobro = 10";
                mysqli_query($linkbd, $update3);
            }
        }
    }

    echo "Arreglo con exito! :)";
?>