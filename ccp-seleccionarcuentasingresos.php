<?php
    header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=utf8");

	require 'comun.inc';
	require 'funciones.inc';

	session_start();
    date_default_timezone_set("America/Bogota");

?>

<html lang="en">
    <head>
        <meta charset="utf-8"/>
        <meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1"/>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>:: IDEAL 10 - Presupuesto CCPET</title>
        <link href="favicon.ico" rel="shortcut icon"/>

        <link href="css/css2.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
        <link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
        <link href="css/css4.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
        <script type="text/javascript" src="css/programas.js"></script>
        <script type="text/javascript" src="css/calendario.js"></script>
        <script type="text/javascript" src="JQuery/jquery-2.1.4.min.js"></script>

        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
        <link href="css/style.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css"/>
        <style>
            [v-cloak]{
                display : none;
            }

            label{
                font-size:13px;
            }

            input{
                height: calc(1em + 0.6rem + 0.5px) !important;
                font-size: 14px !important;
                /* margin-top: 4px !important; */
            }
        </style>

    </head>
    <body>
        <IFRAME src="alertas.php" name="alertas" id="alertas" style="display:none"></IFRAME>
        <span id="todastablas2"></span>

        <header>
            <table>
                <tr><script>barra_imagenes("ccpet");</script><?php cuadro_titulos();?></tr>
            </table>
        </header>

        <form name="form2" method="post" action="">
            <section id="myapp" v-cloak>
                <nav>
                    <table>
                        <tr><?php menu_desplegable("ccpet");?></tr>
                        <tr>
                            <td colspan="3" class="cinta">
                                <img src="imagenes/add.png" onclick="location.href='ccp-seleccionarcuentasingresos.php'" class="mgbt" title="Nuevo" >

                                <img src="imagenes/guardad.png" title="Guardar" class="mgbt">

                                <img src="imagenes/buscad.png" class="mgbt" title="Buscar" onclick="location.href='#'">

                                <a onClick="mypop=window.open('ccp-principal.php','','');mypop.focus();" class="mgbt"><img src="imagenes/nv.png" title="Nueva ventana"></a>

                                <img src="imagenes/iratras.png" title="Atr&aacute;s" onClick="location.href='ccp-visualizarclasificadorpresupuestal.php'" class="mgbt">
                            </td>
                        </tr>
                    </table>
                </nav>
                <article>
                    <table class="inicio ancho">
                        <tr>
                            <td class="titulos" colspan="9" >Marcar cuentas de ingresos para recaudar. </td>
                            <td class="cerrar" style="width:7%" onClick="location.href='teso-principal.php'">Cerrar</td>
                        </tr>
                        <tr>

                            <td class = "textoNewR" style = "width: 25%;">
                                <label class="labelR">
                                    Clasificador de cuentas Ingresos CCPET:
                                </label>
                            </td>
                            <td v-show = "mostrarGuardarRubros"  width="15%">
                                <button type="button" class="btn btn-success btn-sm" @click="guardarRubros" style="margin-bottom: 4px;">Guardar Cuentas Seleccionadas</button>
                            </td>
                            <td v-show = "mostrarBotonVisuaizarRubros" width="15%">
                                <button type="button" class="btn btn-primary btn-sm" @click="visualizarCuentas" style="margin-bottom: 4px;">Visualizar Cuentas Guardadas</button>
                            </td>
                            <td></td>
                        </tr>
                    </table>

                    <div class='subpantalla estilos-scroll' style='height:54vh; margin-top:0px; resize: vertical;'>
                        <table class = "tabla-ejecucion">
                            <thead>
                                <tr style="text-align:Center;">
                                    <th class="titulosnew00" style="width:20%; border-right: 1px solid black; width: 10%;">Rubro</th>
                                    <th class="titulosnew00" style="border-right: 1px solid black">Nombre</th>
                                    <!-- <th class="titulosnew00" style="width:10%;">Tipo</th> -->
                                    <td class="titulosnew00" style="width: 15%; border-right: 1px solid black">Acciones</td>
                                    <td class="titulosnew00" style="width: 15%; border-right: 1px solid black">Programar Clasificadores</td>
                                    <td class="titulosnew00" style="width: 10%;">Clasificadores</td>
                                    
                                </tr>
                            </thead>
                            <tbody>
                                <tr v-for="(det,index) in rubrosIngresos" style="font-size: large;" :style = "[(det[2] === 'A' ? 'font-weight: bold' : myStyleAgregados), estaEnArray(det[0]) ? myStyleSeleccionados : ''] " v-bind:class="[(index % 2 ? 'contenidonew00-01' : contenidonew01-01), det[4] != null ? 'contenidoAuxiliar' : '']" v-on:click="selRubro(det)">
                                    <td style="width:10%; padding-left: 10px; border-right: 1px solid black">{{ det[0] }}</td>
                                    <td style="border-right: 1px solid black; padding-left: 10px;">{{ det[1] }}</td>
                                    <!-- <td style="width:10%; text-align:center;">{{ det[2] }}</td> -->
                                    <td style="text-align: center;border-right: 1px solid black">
                                        <button type="button" v-if="det[4] == null && det[2] == 'C'" class="btn btn-primary btn-sm" @click="agregarAxuliar(det)">Agregar o Eliminar Auxiliar</button>

                                        
                                        
                                        <!-- <button type="button" v-if="det[4] != null && det[2] == 'C'" class="btn btn-danger btn-sm" @click="eliminarAuxiliar(det)">Eliminar Auxiliar</button>
                                        </button> -->
                                    </td>
                                    <td style="text-align: center; border-right: 1px solid black">
                                        <button type="button" v-if="det[4] == null && det[2] == 'C'" class="btn btn-secondary btn-sm" @click="programarCuentas(det[0])">Programar Clasificadores</button>
                                    </td>
                                    <td style="text-align: center;">
                                        {{ nomClasificador[det[0]] }}
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <div id="cargando" v-if="loading" class="loading">
                        <span>Cargando...</span>
                    </div>

                </article>

                <div v-show="mostarRubrosSeleccionados">
                    <transition name="modal">
                        <div class="modal-mask">
                            <div class="modal-wrapper">
                                <div class="modal-dialog modal-lg" style = "max-width: 1200px !important; height: 600px !important;" role="document">
                                    <div class="modal-content"  style = "width: 100% !important; height: 600px !important;" scrollable>
                                        <div class="modal-header" style="background-color: #E1E2E2; ">

                                            <h5 class="modal-title">Rubros seleccionados</h5>

                                            <div class="close" style = "opacity: 1; display: flex; gap: 10px;">
                                                <h6>Exportar a excel: </h6>
                                                <img src="imagenes/excel.png" title="Excel" class="mgbt" @click="excel" style="cursor: pointer;">
                                            </div>

                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true" @click="mostarRubrosSeleccionados = false">&times;</span>
                                            </button>
                                        </div>

                                        <div class="modal-body">
                                            <div style="margin: 2px 0 0 0;">
                                                <div class="row" style="margin: 4px; border-radius:4px; background-color: #E1E2E2; ">

                                                </div>
                                                <table>
                                                    <thead>
                                                        <tr>
                                                            <td width="20%" class='titulos' style="font: 120% sans-serif; padding-left:10px; border-radius: 5px 0 0 0;">Rubros</td>
                                                            <td class='titulos' style="font: 120% sans-serif; ">Nombre</td>
                                                            <td width="10%" class='titulos' style="font: 120% sans-serif; ">Tipo</td>
                                                        </tr>
                                                    </thead>
                                                </table>
                                            </div>
                                            <div style="margin: 2px 0 0 0; border-radius: 0 0 0 6px; height: 400px; overflow: scroll; overflow-x: hidden; background: white; ">
                                                <table class='inicio inicio--no-shadow'>
                                                    <tbody>

                                                        <tr v-for="(rubroSel, index) in rubrosSeleccionadosModal" style="font-size: large;" :style = "rubroSel[2] === 'A' ? 'font-weight: bold' : 'cursor: pointer'" v-bind:class="[(index % 2 ? 'contenidonew00' : 'contenidonew01')]">
                                                            <td style="width:20%; padding-left: 10px; border-right: 1px solid black">{{ rubroSel[0] }}</td>
                                                            <td style="border-right: 1px solid black; padding-left: 10px;">{{ rubroSel[1] }}</td>
                                                            <td style="width:10%; text-align:center;">{{ rubroSel[2] }}</td>

                                                            <input type='hidden' name='rubro[]' v-model="rubroSel[0]">
                                                            <input type='hidden' name='nombreRubro[]' v-model="rubroSel[1]">
                                                            <input type='hidden' name='tipo[]' v-model="rubroSel[2]">

                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-secondary" @click="mostarRubrosSeleccionados = false">Cerrar</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </transition>
                </div>

                <div v-show="mostarAgregarAuxiliar">
                    <transition name="modal">
                        <div class="modal-mask">
                            <div class="modal-wrapper">
                                <div class="modal-dialog modal-lg" style = "max-width: 1200px !important; height: 600px !important;" role="document">
                                    <div class="modal-content"  style = "width: 100% !important; height: 600px !important;" scrollable>
                                        <div class="modal-header" style="background-color: #E1E2E2; ">

                                            <h5 class="modal-title">Agregar Auxiliar</h5>

                                            <div class="close" style = "opacity: 1; display: flex; gap: 10px;">
                                                <h6>{{ rubroSeleccionado }} </h6>
                                            </div>

                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true" @click="mostarAgregarAuxiliar = false">&times;</span>
                                            </button>
                                        </div>

                                        <div class="modal-body">
                                            <div style="margin: 2px 0 0 0;">
                                                <div class="row d-flex justify-content-end" 
                                                    style="margin: 4px; border-radius: 4px; background-color: #E1E2E2;">
                                                    <button 
                                                        type="button" 
                                                        class="btn btn-primary btn-sm" 
                                                        @click="agregarOtroAuxiliar" 
                                                        >
                                                        Agregar Auxiliar
                                                    </button>
                                                </div>
                                                <table>
                                                    <thead>
                                                        <tr>
                                                            <td width="20%" class='titulos' style=" width:20%; font: 120% sans-serif; padding-left:10px; border-radius: 5px 0 0 0;">Codigo Auxiliar</td>
                                                            <td class='titulos' style="font: 120% sans-serif; ">NombreDel Clasificador</td>
                                                            <td class="titulos" style=" width:10%; font: 120% sans-serif; ">Quitar</td>
                                                        </tr>
                                                    </thead>
                                                </table>
                                            </div>
                                            <div style="margin: 2px 0 0 0; border-radius: 0 0 0 6px; height: 400px; overflow: scroll; overflow-x: hidden; background: white; ">
                                                <table class='inicio inicio--no-shadow'>
                                                    <tbody>

                                                        <tr v-for="(rubroSel, index) in auxiliaresDelRubro" style="font-size: large;" v-bind:class="[(index % 2 ? 'contenidonew00' : 'contenidonew01')]">
                                                            <td style="width:20%; padding-left: 10px; border-right: 1px solid black">{{ rubroSel[0] }}</td>
                                                            <td style="border-right: 1px solid black; padding-left: 10px;">
                                                                <input type="text" v-model="rubroSel[5]" style="width: 100%;" onchange="agregarNombreAuxiliar(rubroSel[0], rubroSel[5])">
                                                            </td>
                                                            <td style="width:10%; text-align:center;">
                                                                <button type="button" class="btn btn-danger btn-sm" @click="quitarAuxiliar(rubroSel)">Quitar</button>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-primary" @click="guardarAuxiliares">Guardar</button>
                                            <button type="button" class="btn btn-secondary" @click="mostarAgregarAuxiliar = false">Cerrar</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </transition>
                </div>

                <div v-show="showModal">
					<transition name="modal">
						<div class="modal-mask">
							<div class="modal-wrapper">
								<div class="modal-dialog modal-lg" role="document">
									<div class="modal-content" scrollable>
									<div class="modal-header">
										<h5 class="modal-title">Programar Clasificadores</h5>
										<button type="button" class="close" data-dismiss="modal" aria-label="Close">
										<span aria-hidden="true" @click="showModal = false">&times;</span>
										</button>
									</div>
									<div class="modal-body">
										<!-- Default unchecked -->
										<div v-for="clasificador in listClasi" class="custom-control custom-checkbox">
											<input type="checkbox" class="custom-control-input" :value="clasificador[0]" v-model="checkedClasificadores" style="z-index: 2000;">
											<label class="custom-control-label" for="ingresos">{{ clasificador[1] }}</label>
										</div>
										
									</div>
									<div class="modal-footer">
										<button type="button" class="btn btn-primary" v-on:click="guardarClasificadores()">Guardar</button>
										<button type="button" class="btn btn-secondary" @click="showModal = false">Cerrar</button>
									</div>
									</div>
								</div>
							</div>
						</div>
					</transition>
				</div>

                <div v-show="showModal_respuesta">
					<transition name="modal">
						<div class="modal-mask">
							<div class="modal-wrapper">
								<div class="modal-dialog" role="document">
									<div class="modal-content">
									<div class="modal-header">
										<h5 class="modal-title"></h5>
										<button type="button" class="close" data-dismiss="modal" aria-label="Close">
										<span aria-hidden="true" @click="showModal_respuesta = false">&times;</span>
										</button>
									</div>
									<div class="modal-body_1">
										<!-- Default unchecked -->
										<h3 style="font: 120% sans-serif; text-align:center">Se guardo exitosamente!!</h3>
									</div>
									<div class="modal-footer">
										<button type="button" class="btn btn-secondary" @click="showModal_respuesta = false">Cerrar</button>
									</div>
									</div>
								</div>
							</div>
						</div>
					</transition>
				</div>

            </section>
        </form>

        <script type="module" src="./presupuesto_ccpet/clasificadores/ccp-seleccionarcuentasingresos.js"></script>
        <script src="Librerias/vue/axios.min.js"></script>
        <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/vue/3.2.37/vue.global.min.js"></script> -->
        <!-- <script src="https://unpkg.com/vue@3"></script> -->
        <script src="sweetalert2/dist/sweetalert2.min.js"></script>
        <link rel="stylesheet" href="sweetalert2/dist/sweetalert2.min.css">
        <script src="https://unpkg.com/vue@3/dist/vue.global.js"></script>

    </body>
</html>
