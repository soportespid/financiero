<?php
  require"comun.inc";
  require"funciones.inc";
  session_start();
?>
<html xmlns="http://www.w3.org/1999/xhtml">
 	 <head>
		<meta http-equiv="Content-Type" content="text/html" />
		<meta http-equiv="X-UA-Compatible" content="IE=9" />
		<title>:: IDEAL.10 - Servicios Publicos</title>
		<script>
			function verUltimaPos(idcta, filas, filtro)
			{
				var scrtop = $('#divdet').scrollTop();
				var altura = $('#divdet').height();
				var numpag = $('#nummul').val();
				var limreg = $('#numres').val();

                if((numpag <= 0)||(numpag == ""))
                {
                    numpag = 0;
                }
                if((limreg == 0)||(limreg == ""))
                {
                    limreg = 10;
                }
				numpag++;
				location.href="serv-editalecturamedidores.php?idban="+idcta+"&scrtop="+scrtop+"&totreg="+filas+"&altura="+altura+"&numpag="+numpag+"&limreg="+limreg+"&filtro="+filtro;
            }

		</script>
		<script type="text/javascript" src="css/programas.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script type="text/javascript" src="css/calendario.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<link href="css/css2.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<link href="css/css4.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<script type="text/javascript" src="jquery-1.11.0.min.js?<?php echo date('d_m_Y_h_i_s');?>"></script> 
		<?php titlepag();?>
	</head>
  	<body>
		<IFRAME src="alertas.php" name="alertas" id="alertas" style="display:none"></IFRAME>
		<span id="todastablas2"></span>
		<table>
			<tr><script>barra_imagenes("serv");</script><?php cuadro_titulos();?></tr>	 
			<tr><?php menu_desplegable("serv");?></tr>
			<tr>
				<td colspan="3" class="cinta">
					<a href="serv-lecturamedidores.php" class="tooltip right mgbt">
						<img src="imagenes/add.png"/>
						<span class="tiptext">Nuevo</span>
					</a> 
					<a class="tooltip bottom mgbt">
						<img src="imagenes/guardad.png"/>
						<span class="tiptext">Guardar</span>
					</a>
					<a href="serv-buscalecturamedidores.php" class="tooltip bottom mgbt"> 
						<img src="imagenes/busca.png"/>
						<span class="tiptext">Buscar</span>
					</a> 
					<a onClick="mypop=window.open('serv-principal.php','','');mypop.focus();" class="tooltip bottom mgbt">
						<img src="imagenes/nv.png">
						<span class="tiptext">Nueva Ventana</span>
					</a>
					<a onClick="mypop=window.open('<?php echo $url2; ?>','','');mypop.focus();" class="tooltip bottom mgbt">
						<img src="imagenes/duplicar_pantalla.png">
						<span class="tiptext">Duplicar Pesta&ntilde;a</span>
					</a>
				</td>
			</tr>		
		</table>
		<form name="form2" method="post" action="serv-buscalecturamedidores.php">
			<table  class="inicio ancho" align="center" >
				<tr>
					<td class="titulos" colspan="8">:. Buscar Lecturas de Medidores</td>
					<td  class="cerrar" ><a href="serv-principal.php">Cerrar</a></td>
				</tr>
				<tr>
					<td  class="saludo1">No Lectura:</td>
					<td >
						<input name="numero" type="text" value="" size="20" >
					</td>
					<td  class="saludo1">Nombre Cliente: </td>
					<td>
						<input name="nombre" type="text" value="" size="50" >
					</td>
					<td  class="saludo1">Rango Fechas: </td>
					<td>
						<input type="text" name="fecha1" value="<?php echo @ $_POST['fecha1']?>" maxlength="10" onKeyPress="javascript:return solonumeros(event)" onKeyUp="return tabular(event,this)" id="fc_1198971545" onKeyDown="mascara(this,'/',patron,true)" title="DD/MM/YYYY" style="width:80%;height:30px;" readonly>&nbsp;
						<img src="imagenes/calendario04.png" onClick="displayCalendarFor('fc_1198971545');" title="Calendario" class="icobut"/>
					</td>
					<td>
						<input type="text" name="fecha2" value="<?php echo @ $_POST['fecha2']?>" maxlength="10" onKeyPress="javascript:return solonumeros(event)" onKeyUp="return tabular(event,this)" id="fc_1198971546" onKeyDown="mascara(this,'/',patron,true)" title="DD/MM/YYYY" style="width:80%;height:30px;" readonly>&nbsp;
						<img src="imagenes/calendario04.png" onClick="displayCalendarFor('fc_1198971546');" title="Calendario" class="icobut"/>
					</td>
					<td style="padding-bottom:0px">
                        <em class="botonflecha" onClick="limbusquedas();">Buscar</em>
                    </td>	
						<input type="hidden" name="numres" id="numres" value="<?php echo @ $_POST['numres'];?>"/>
						<input type="hidden" name="numpos" id="numpos" value="<?php echo @ $_POST['numpos'];?>"/>
						<input type="hidden" name="nummul" id="nummul" value="<?php echo @ $_POST['nummul'];?>"/>
						<input name="oculto" type="hidden" value="1">
				</tr>                       
			</table> 
			<div class="subpantallac5" style="height:69%; width:99.6%; margin-top:0px; overflow-x:hidden" id="divdet">
			<?php
				$linkbd=conectar_bd();
				$crit1=" ";
				$crit2=" ";
				$crit3=" ";
				if ($_POST[nombre]!=""){
					$crit1=" and concat_ws(t.nombre1, t.nombre2, t.apellido1, t.apellido2, t.razonsocial) like '%".$_POST[nombre]."%' ";
				}
				if ($_POST[numero]!=""){
					$crit2=" and l.id like '%".$_POST[numero]."%'  ";
				}
				if ($_POST[fecha1]!="" or $_POST[fecha2]!=""){
					preg_match("/([0-9]{2})\/([0-9]{2})\/([0-9]{4})/", $_POST['fecha1'],$f1);
					$fecha1="$f1[3]-$f1[2]-$f1[1]";
					preg_match("/([0-9]{2})\/([0-9]{2})\/([0-9]{4})/", $_POST['fecha2'],$f2);
					$fecha2="$f2[3]-$f2[2]-$f2[1]";
					$crit3=" and l.fecha between '$fecha1' and '$fecha2'";
				}
					

				$sqlr = "SELECT l.id, l.fecha, l.valormedicion, l.observacion, c.id, t.nombre1, t.nombre2, t.apellido1, t.apellido2, t.razonsocial, s.nombre  FROM terceros t INNER JOIN srvclientes c ON t.id_tercero= c.id_tercero INNER JOIN srvlectura l ON l.id_cliente = c.id INNER JOIN srvservicios s ON l.id_servicio = s.id WHERE c.estado = 'S' $crit1 $crit2 $crit3 ORDER BY CONVERT(c.id, SIGNED INTEGER) ASC";

				$resp = mysql_query($sqlr,$linkbd);
				$_POST['numtop'] = mysql_num_rows($resp);
				$nuncilumnas = ceil($_POST['numtop']/$_POST['numres']);
                $numcontrol = $_POST['nummul'] + 1;
                
				if(($nuncilumnas == $numcontrol) || (@ $_POST['numres'] == "-1"))
				{
					$imagenforward = " <img src='imagenes/forward02.png' style='width:17px;cursor:default;'> ";
					$imagensforward = " <img src='imagenes/skip_forward02.png' style='width:16px;cursor:default;'> ";
				}
				else 
				{
					$imagenforward = " <img src='imagenes/forward01.png' style='width:17px;cursor:pointer;' title='Siguiente' onClick='numsiguiente()'> ";
					$imagensforward = " <img src='imagenes/skip_forward01.png' style='width:16px;cursor:pointer;' title='Fin' onClick='saltocol(\"$nuncilumnas\")'> ";
                }
                
				if((@ $_POST['numpos'] == 0) || (@ $_POST['numres'] == '-1'))
				{
					$imagenback = " <img src='imagenes/back02.png' style='width:17px;cursor:default;'> ";
					$imagensback = "<img src='imagenes/skip_back02.png' style='width:16px;cursor:default;'> ";
				}
				else
				{
					$imagenback = " <img src='imagenes/back01.png' style='width:17px;cursor:pointer;' title='Anterior' onClick='numanterior();'> ";
					$imagensback = " <img src='imagenes/skip_back01.png' style='width:16px;cursor:pointer;' title='Inicio' onClick='saltocol(\"1\")'> ";
                }
				$con=1;

				echo "<table class='inicio' align='center' >
						<tr>
							<td colspan='6' class='titulos'>.: Resultados Busqueda:</td>
							<td class='submenu'>
                                <select name='renumres' id='renumres' onChange='cambionum();' style='width:100%'>
                                    <option value='10'"; if (@ $_POST['renumres'] == '10'){echo 'selected';} echo ">10</option>
                                    <option value='20'"; if (@ $_POST['renumres'] == '20'){echo 'selected';} echo ">20</option>
                                    <option value='30'"; if (@ $_POST['renumres'] == '30'){echo 'selected';} echo ">30</option>
                                    <option value='50'"; if (@ $_POST['renumres'] == '50'){echo 'selected';} echo ">50</option>
                                    <option value='100'"; if (@ $_POST['renumres'] == '100'){echo 'selected';} echo ">100</option>
                                    <option value='-1'"; if (@ $_POST['renumres'] == '-1'){echo 'selected';} echo ">Todos</option>
                                </select>
						    </td>
						</tr>
						<tr>
							<td colspan='8' class='saludo1'>Resultados Encontradas: ".$_POST['numtop']."</td>
						</tr>
						<tr>
							<td class='titulos2'>No Lectura</td>
							<td class='titulos2'>Fecha</td>
							<td class='titulos2'>Codigo Cliente</td>
							<td class='titulos2'>Nombre Cliente</td>
							<td class='titulos2'>Servicio</td>
							<td class='titulos2'>Valor Medicion</td>
							<td class='titulos2'>Observaciones</td>
						</tr>";	
				//echo "nr:".$nr;<td class='titulos2'>Fecha</td>
				$iter='saludo1a';
				$iter2='saludo2';
				$filas = 1;
				while ($row =mysql_fetch_array($resp)) 
				{
					$con2 = $con + $_POST['numpos'];
					$idcta = "'$row[0]'";
					$numfil = "'$filas'";

					$matricula = str_pad($row['id'], 10, "0", STR_PAD_LEFT);
					$nombreCompleto = $row['nombre1'].' '.$row['nombre2'].' '.$row['apellido1'].' '.$row['apellido2'];
					if ($row['razonsocial']!='') {
						$nombreCompleto = $row['razonsocial'];
					}
					echo "<tr class='$iter' onDblClick=\"verUltimaPos($idcta, $numfil, $filtro)\" style='text-transform:uppercase;'>
							<td class='icoop' style='width:auto;'>$row[0]</td>
							<td class='icoop' style='width:auto;'>$row[1]</td>
							<td class='icoop' style='width:auto;'>$matricula</td>
							<td class='icoop' style='width:auto;'>$nombreCompleto</td>
							<td class='icoop' style='width:auto;'>$row[nombre]</td>
							<td class='icoop' style='width:auto;'>$row[valormedicion]</td>
							<td class='icoop' style='width:auto;'>$row[observacion]</td>
						</tr>";
					$con+=1;
					$aux=$iter;
					$iter=$iter2;
					$iter2=$aux;
				}
				if (@ $_POST['numtop'] == 0)
				{
					echo "
					<table class='inicio'>
						<tr>
							<td class='saludo1' style='text-align:center;width:100%'>
								<img src='imagenes\alert.png' style='width:25px'>No hay coincidencias en la b&uacute;squeda $tibusqueda<img src='imagenes\alert.png' style='width:25px'>
							</td>
						</tr>
					</table>";
				}
                        
				echo"
                    </table>
                    
					<table class='inicio'>
						<tr>
							<td style='text-align:center;'>
								<a>$imagensback</a>&nbsp;
                                <a>$imagenback</a>&nbsp;&nbsp;";
                                
                                if($nuncilumnas <= 9)
                                {
                                    $numfin = $nuncilumnas;
                                }
                                else
                                {
                                    $numfin = 9;
                                }
                                
                                for($xx = 1; $xx <= $numfin; $xx++)
				                {
                                    if($numcontrol <= 9)
                                    {
                                        $numx = $xx;
                                    }
                                    else
                                    {
                                        $numx = $xx + ($numcontrol - 9);
                                    }

                                    if($numcontrol == $numx)
                                    {
                                        echo"<a onClick='saltocol(\"$numx\")'; style='color:#24D915;cursor:pointer;'> $numx </a>";
                                    }
                                    else 
                                    {
                                        echo"<a onClick='saltocol(\"$numx\")'; style='color:#000000;cursor:pointer;'> $numx </a>";
                                    }
                                }
                                
                            echo"			
                                &nbsp;&nbsp;<a>$imagenforward</a>
                                &nbsp;<a>$imagensforward</a>

                            </td>
                        </tr>
                    </table> ";

            ?>
            
            <input type="hidden" name="ocules" id="ocules" value="<?php echo @ $_POST['ocules'];?>">
            
            <input type="hidden" name="actdes" id="actdes" value="<?php echo @ $_POST['actdes'];?>">
            
            <input type="hidden" name="numtop" id="numtop" value="<?php echo @ $_POST['numtop'];?>">
            
			</div>
		</form> 
  	</body>
</html>