<?php
header("Cache-control: no-cache, no-store, must-revalidate");
header("Content-Type: text/html;charset=utf8");

require "comun.inc";
require "funciones.inc";
require "conversor.php";
require "validaciones.inc";

$linkbd_V7 = conectar_v7();
$linkbd_V7->set_charset("utf8");

session_start();
date_default_timezone_set("America/Bogota");
?>
<!DOCTYPE>
<html lang="es">

<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1" />
    <meta name="viewport" content="user-scalable=no">
    <title>:: IDEAL 10 - Presupuesto</title>
    <link href="favicon.ico" rel="shortcut icon" />
    <link href="css/css2.css" rel="stylesheet" type="text/css" />
    <link href="css/css3.css" rel="stylesheet" type="text/css" />
    <link href="css/style.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="css/programas.js"></script>

</head>
<style>
</style>

<body>
    <IFRAME src="alertas.php" name="alertas" id="alertas" style="display:none"></IFRAME>
    <span id="todastablas2"></span>
    <table>
        <tr>
            <script>barra_imagenes("ccpet");</script><?php cuadro_titulos(); ?>
        </tr>
        <tr><?php menu_desplegable("ccpet"); ?></tr>
    </table>
    <div class="bg-white group-btn p-1">
        <button type="button" onclick="location.href='ccp-buscarcdp.php'" class="btn btn-white btn-primary-hover d-flex justify-between align-items-center">
            <span>Buscar</span>
            <svg class="fill-black group-hover:fill-white w-5 h-5" xmlns="http://www.w3.org/2000/svg"
                viewBox="0 -960 960 960">
                <path d="M784-120 532-372q-30 24-69 38t-83 14q-109 0-184.5-75.5T120-580q0-109 75.5-184.5T380-840q109 0 184.5 75.5T640-580q0 44-14 83t-38 69l252 252-56 56ZM380-400q75 0 127.5-52.5T560-580q0-75-52.5-127.5T380-760q-75 0-127.5 52.5T200-580q0 75 52.5 127.5T380-400Z">
                </path>
            </svg>
        </button>
        <button type="button" onclick="window.open('ccp-principal');"
            class="btn btn-white btn-primary-hover d-flex justify-between align-items-center">
            <span>Nueva ventana</span>
            <svg class="fill-black group-hover:fill-white w-5 h-5" xmlns="http://www.w3.org/2000/svg"
                viewBox="0 -960 960 960">
                <path
                    d="M320-120v-80h80v-80H160q-33 0-56.5-23.5T80-360v-400q0-33 23.5-56.5T160-840h640q33 0 56.5 23.5T880-760v400q0 33-23.5 56.5T800-280H560v80h80v80H320ZM160-360h640v-400H160v400Zm0 0v-400 400Z">
                </path>
            </svg>
        </button>
    </div>
    <form name="form2" method="post" action="">
        <table class="inicio">
            <tr>
                <td class="titulos" colspan="2">.: Gesti&oacute;n CDP</td>
                <td class="cerrar" style='width:7%' onClick="location.href='ccp-principal.php'">Cerrar</td>
            </tr>
            <tr>
                <td class='saludo1' width='70%'>
                    <ol id="lista2">

                        <li onClick="location.href='ccp-cdpbasico.php'" style="cursor:pointer">CDP B&aacute;sico</li>
                        <li onClick="location.href='ccp-solicitudcdp.php'" style="cursor:pointer">Certificado de Disponibilidad Presupuestal</li>
                        <li onClick="location.href='ccp-cdpnomina2022.php'" style="cursor:pointer">CDP N&oacute;mina
                        </li>

                    </ol>
            </tr>
        </table>
    </form>
</body>

</html>
