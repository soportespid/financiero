<?php
	require "comun.inc";
	require "funciones.inc";
	require "conversor.php";
	session_start();
	$linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");
?>
<html lang="es">
	<head>
		<meta charset="UTF-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
    	<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<title>:: IDEAL 10 - Presupuesto</title>
        <link href="css/css2.css" rel="stylesheet" type="text/css" />
		<link href="css/css3.css" rel="stylesheet" type="text/css" />
        <script type='text/javascript' src='css/programas.js'></script>
		<script>
			function guardar(idfac)
			{
				var validacion01=document.getElementById('vigenciai').value;
				var validacion02=document.getElementById('vigenciaf').value;
				if (validacion01.trim()!='' && validacion02.trim()!='')
				{despliegamodalm('visible','4','Esta Seguro de Guardar','1');}
 				else{despliegamodalm('visible','2','Faltan datos para completar el registro');}
			}
			function despliegamodalm(_valor,_tip,mensa,pregunta)
			{
				document.getElementById("bgventanamodalm").style.visibility=_valor;
				if(_valor=="hidden"){document.getElementById('ventanam').src="";}
				else
				{
					switch(_tip)
					{
						case "1":
							document.getElementById('ventanam').src="ventana-mensaje1.php?titulos="+mensa;break;
						case "2":
							document.getElementById('ventanam').src="ventana-mensaje3.php?titulos="+mensa;break;
						case "3":
							document.getElementById('ventanam').src="ventana-mensaje2.php?titulos="+mensa;break;
						case "4":
							document.getElementById('ventanam').src="ventana-consulta1.php?titulos="+mensa+"&idresp="+pregunta;break;
					}
				}
			}
			function funcionmensaje(){document.location.href = "ccp-vigencia.php";}
			function respuestaconsulta(pregunta)
			{
				switch(pregunta)
				{
					case "1":	document.form2.oculto.value="2";document.form2.submit();break;
				}
			}
		</script>
		<?php titlepag();?>
	</head>
	<body>
		<IFRAME src="alertas.php" name="alertas" id="alertas" style="display:none"></IFRAME>
		<span id="todastablas2"></span>
		<table>
    		<tr><script>barra_imagenes("ccpet");</script><?php cuadro_titulos();?></tr>
   			<tr><?php menu_desplegable("ccpet");?></tr>
			<tr>
  				<td colspan="3" class="cinta">
					<a href="ccp-vigencia.php" class="mgbt"><img src="imagenes/add.png" title="Nuevo"/></a>
					<a href="#"  onClick="guardar();" class="mgbt"><img src="imagenes/guarda.png" title="Guardar" /></a>
					<a href="ccp-buscavigencia.php" class="mgbt"><img src="imagenes/busca.png" title="Buscar" /></a>
					<a onClick="mypop=window.open('plan-agenda.php','','');mypop.focus()" class="mgbt"><img src="imagenes/agenda1.png" title="Agenda" /></a>
					<a href="#" onClick="mypop=window.open('ccp-principal.php','','');mypop.focus();" class="mgbt"><img src="imagenes/nv.png" title="Nueva Ventana"></a>
				</td>
          	</tr>
  		</table>
        <div id="bgventanamodalm" class="bgventanamodalm">
            <div id="ventanamodalm" class="ventanamodalm">
                <IFRAME src="" name="ventanam" marginWidth=0 marginHeight=0 frameBorder=0 id="ventanam" frameSpacing=0 style=" width:700px; height:130px; top:200; overflow:hidden;">
                </IFRAME>
            </div>
        </div>
        <form name="form2" method="post" action="">
			<?php
                if(!$_POST['oculto'])
                {
                    $sqlr="select *from dominios where nombre_dominio='VIGENCIA_RG' and tipo='S'";
                    $res=mysqli_query($linkbd, $sqlr);
                    while($r=mysqli_fetch_row($res))
                    {
                        $_POST['vigenciaacti']=$r[0];
                        $_POST['vigenciaactf']=$r[1];
                    }
                }
            ?>
  			<table class="inicio" align="center" >
    			<tr>
      				<td class="titulos" colspan="4">Crear Vigencia  PD</td>
                    <td class="cerrar" style="width:7%;" ><a href="ccp-principal.php">&nbsp;Cerrar</a></td>
    			</tr>
      			<tr>
      				<td class="saludo1" style="width:5cm;">Vigencia Activa:</td>
      				<td colspan="3"><input name="vigenciaacti" type="text" id="vigenciaacti" size="5" value="<?php echo $_POST['vigenciaacti']?>" readonly>&nbsp;-&nbsp;<input name="vigenciaactf" type="text" id="vigenciaactf" size="5" value="<?php echo $_POST['vigenciaactf']?>" readonly></td>
    			</tr>
    			<tr>
      				<td class="saludo1" style="width:5cm;">Nueva Vigencia Inicial:</td>
      				<td style="width:15%;"><input name="vigenciai" type="text" id="vigenciai" size="5" value="<?php echo $_POST['vigenciai']?>" onKeyPress="javascript:return solonumeros(event)"></td>
      				<td  class="saludo1" style="width:5cm;">Nueva Vigencia Final:</td>
      				<td><input name="vigenciaf" type="text" id="vigenciaf" size="5" value="<?php echo $_POST['vigenciaf']?>" onKeyPress="javascript:return solonumeros(event)"></td>
    			</tr>
 			</table>
  			<input type="hidden" name="oculto" id="oculto" value="1">
      		<?php
				if($_POST['oculto']==2)
				{
					$sqlr="select count(*) from dominios where nombre_dominio='VIGENCIA_RG' and  ('$_POST[vigenciai]' between valor_inicial and valor_final or '$_POST[vigenciaf]' between valor_inicial and valor_final)";
					$resp=mysqli_query($linkbd, $sqlr);
					$nreg=mysqli_fetch_row($resp);
					if($nreg[0]<=0)
					{
						$sqlr="update dominios set tipo='N' where nombre_dominio='VIGENCIA_RG' and tipo='S'";
						mysqli_query($linkbd, $sqlr);
						$sqlr="insert into dominios (valor_inicial,valor_final,descripcion_valor, nombre_dominio,tipo,descripcion_dominio) values ('$_POST[vigenciai]','$_POST[vigenciaf]','VIGENCIAS','VIGENCIA_RG','S','VIGENCIAS REGALIAS')";
						if(!mysqli_query($linkbd, $sqlr))
  							{echo"<script>despliegamodalm('visible','2','Error no se pudo actualizar $sqlr');</script>";}
  						else
  							{echo"<script>despliegamodalm('visible','1','Se ha Actualizado con Exito: Vigencia $_POST[vigenciai] -  $_POST[vigenciaf] ');</script>";}
					}
					else
						{echo"<script>despliegamodalm('visible','2','Error, ya existe una vigencia con estos valores');</script>";}

				}
			?>
		</form>
	</body>
</html>
