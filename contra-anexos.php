<?php
	require "comun.inc";
	require "funciones.inc";
	require "conversor.php";
	session_start();
	$linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");
	header("Cache-control: private"); // Arregla IE 6
	date_default_timezone_set("America/Bogota");
?>
<!DOCTYPE >
<html lang="es">
	<head>
		<meta charset="UTF-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
    	<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<title>:: IDEAL 10 - Parametrización</title>
        <link href="css/css2.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="css/programas.js"></script>
		<script>
			function guardar()
			{
				var validacion01=document.getElementById('codigo').value
				var validacion02=document.getElementById('nombre').value
				if (validacion01.trim()!='' && validacion02.trim()!=''){despliegamodalm('visible','4','Esta Seguro de Guardar','1');}
				else
				{
					despliegamodalm('visible','2','Faltan datos para completar el registro');
					document.form2.nombre.focus();
					document.form2.nombre.select();
				}
			}
			function despliegamodalm(_valor,_tip,mensa,pregunta)
			{
				document.getElementById("bgventanamodalm").style.visibility=_valor;
				if(_valor=="hidden"){document.getElementById('ventanam').src="";}
				else
				{
					switch(_tip)
					{
						case "1":
							document.getElementById('ventanam').src="ventana-mensaje1.php?titulos="+mensa;break;
						case "2":
							document.getElementById('ventanam').src="ventana-mensaje3.php?titulos="+mensa;break;
						case "3":
							document.getElementById('ventanam').src="ventana-mensaje2.php?titulos="+mensa;break;
						case "4":
							document.getElementById('ventanam').src="ventana-consulta1.php?titulos="+mensa+"&idresp="+pregunta;break;
					}
				}
			}
			function funcionmensaje(){document.location.href = "contra-anexos.php";}
			function respuestaconsulta(pregunta)
			{
				switch(pregunta)
				{
					case "1":	document.form2.oculto.value="2";document.form2.submit();break;
				}
			}
		</script>
		<?php titlepag();?>
	</head>
	<body>
        <IFRAME src="alertas.php" name="alertas" id="alertas" style="display:none"></IFRAME>
        <span id="todastablas2"></span>
        <table>
            <tr><script>barra_imagenes("para");</script><?php cuadro_titulos();?></tr>
            <tr><?php menu_desplegable("para");?></tr>
            <tr>
  				<td colspan="3" class="cinta">
					<a href="contra-anexos.php" class="mgbt"><img src="imagenes/add.png" title="Nuevo" border="0" /></a>
					<a href="#"  onClick="guardar()"class="mgbt"><img src="imagenes/guarda.png" title="Guardar" /></a>
					<a href="contra-buscaanexos.php" class="mgbt"><img src="imagenes/busca.png" title="Buscar" border="0" /></a>
					<a onClick="mypop=window.open('plan-agenda.php','','');mypop.focus()" class="mgbt"><img src="imagenes/agenda1.png" title="Agenda" /></a>
					<a href="#" onClick="mypop=window.open('para-principal.php','','');mypop.focus();" class="mgbt"><img src="imagenes/nv.png" title="Nueva ventana"></a>
				</td>
      		</tr>
   		</table>
        <div id="bgventanamodalm" class="bgventanamodalm">
            <div id="ventanamodalm" class="ventanamodalm">
                <IFRAME src="" name="ventanam" marginWidth=0 marginHeight=0 frameBorder=0 id="ventanam" frameSpacing=0 style=" width:700px; height:130px; top:200; overflow:hidden;">
                </IFRAME>
            </div>
        </div>
 		<form name="form2" method="post">
 		<?php
 			if($_POST['oculto']=="")
			{
				$mxa=selconsecutivo('contraanexos','id');
				$_POST['codigo']=$mxa;
			}
 		?>
            <table class="inicio" >
                <tr>
                    <td class="titulos" colspan="4">Crear Anexos</td>
                    <td class="cerrar" style='width:7%'><a href="para-principal.php">Cerrar</a></td>
                </tr>
                <tr>
                    <td class="saludo1" style='width:10%'>Codigo:</td>
                    <td style='width:30%'><input type="text" name="codigo" id="codigo" value="<?php echo $_POST['codigo']?>" style='width:15%'></td>
                    <td class="saludo1" style='width:10%'>Nombre:</td>
                    <td><input type="text" name="nombre" id="nombre" value="<?php echo $_POST['nombre']?>" style='width:70%'></td>
                </tr>
                <tr>
                    <td class="saludo1">Tipo:</td>
                    <td>
                        <select name="tipo" id="tipo" onKeyUp="return tabular(event,this)">
                            <option  value="" >Seleccione....</option>
                            <?php
                                $sqlr="SELECT * FROM dominios where nombre_dominio='CONTRATACION_ANEXOS' ";
                                $res=mysqli_query($linkbd, $sqlr);
                                while ($rowEmp = mysqli_fetch_row($res))
                                {
                                    echo "<option value=$rowEmp[0]";
                                    if($rowEmp[0]==$_POST['tipo']){echo "  SELECTED";}
                                    echo ">$rowEmp[0] - $rowEmp[1]</option>";
                                }
                            ?>
                        </select>
                    </td>
                    <td class="saludo1">Estado:</td>
                    <td>
                        <select name="estado" id="estado" onKeyUp="return tabular(event,this)" >
                            <option value="S" <?php if($_POST['estado']=='S') echo "SELECTED"; ?>>Activo</option>
                            <option value="N" <?php if($_POST['estado']=='N') echo "SELECTED"; ?>>Inactivo</option>
                        </select>
                    </td>
                </tr>
            </table>
			<input type="hidden" name="oculto" value="1">
			<?php
				if($_POST['oculto']=="2")//********guardar
				{
					$mxa=selconsecutivo('contraanexos','id');
					$sqlr="insert into contraanexos (id,nombre,estado,fijo,fase) values ('$mxa','$_POST[nombre]','$_POST[estado]','N','$_POST[tipo]') ";
					if (!mysqli_query($linkbd, $sqlr)){echo"<script>despliegamodalm('visible','2','ERROR EN LA CREACION DEL ANEXO');</script>";}
					else {echo"<script>despliegamodalm('visible','1','Se ha almacenado el Anexo con Exito');</script>";}
				}
			?>
		</form>
	</body>
</html>
