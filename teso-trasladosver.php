<?php
	header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=utf8");
	require "comun.inc";
	require "funciones.inc";
	require "conversor.php";
	sesion();
	$linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");
	cargarcodigopag($_GET[codpag],$_SESSION["nivel"]);
	date_default_timezone_set("America/Bogota");
?>
<html lang="es">
	<head>
		<meta charset="utf-8">
		<title>:: IDEAL 10 - Tesorer&iacute;a</title>
		<link href="css/css2.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css">
		<link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css">
		<link href="css/css4.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css">
		<script type="text/javascript" src="css/calendario.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script type="text/javascript" src="css/programas.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script>
			function buscacta(e)
			{
				if (document.form2.cuenta.value != "")
				{
					document.form2.bc.value = '1';
					document.form2.submit();
				}
			}
			function validar()
			{
				document.form2.submit();
			}
			function buscater(e)
			{
				if (document.form2.tercero.value != "")
				{
					document.form2.bt.value = '1';
					document.form2.submit();
				}
			}
			function agregardetalle()
			{
				if(document.form2.numero.value != "" &&  document.form2.valor.value > 0 &&  document.form2.banco.value != "")
				{
					document.form2.agregadet.value = 1;
					document.form2.submit();
				}
				else {despliegamodalm('visible','2','Falta informacion para poder Agregar');}
			}
			function eliminar(variable)
			{
				despliegamodalm('visible','4','Esta Seguro de Eliminar','2')
				document.form2.elimina.value = variable;
			}
			function guardar()
			{
				var ntraslados = document.getElementsByName('dbancos[]').length;
				if ((document.form2.fecha.value != '') && (ntraslados > 0))
				{
					despliegamodalm('visible','4','Esta seguro de guardar','1')
				}
				else
				{
					despliegamodalm('visible','2','Faltan datos para completar el registro');
					document.form2.fecha.focus();
					document.form2.fecha.select();
				}
			}
			function pdf()
			{
				document.form2.action = "teso-pdftraslados.php";
				document.form2.target = "_BLANK";
				document.form2.submit();
				document.form2.action = "";
				document.form2.target = "";
			}
			function despliegamodalm(_valor,_tip,mensa,pregunta)
			{
				document.getElementById("bgventanamodalm").style.visibility=_valor;
				if(_valor=="hidden"){document.getElementById('ventanam').src="";}
				else
				{
					switch(_tip)
					{
						case "1":	document.getElementById('ventanam').src="ventana-mensaje1.php?titulos="+mensa;break;
						case "2":	document.getElementById('ventanam').src="ventana-mensaje3.php?titulos="+mensa;break;
						case "3":	document.getElementById('ventanam').src="ventana-mensaje2.php?titulos="+mensa;break;
						case "4":	document.getElementById('ventanam').src="ventana-consulta1.php?titulos="+mensa+"&idresp="+pregunta;break;	
					}
				}
			}
			function funcionmensaje()
			{
				//document.location.href = "hum-liquidarnominamirar.php?idnomi="+document.form2.idcomp.value;
			}
			function respuestaconsulta(pregunta)
			{
				switch(pregunta)
				{
					case "1":
						document.form2.oculto.value = 2;
						document.form2.submit();
						break;
					case "2":
						document.form2.oculto.value = 9;
						document.form2.submit();
						break;
				}
			}
		</script>
		<?php titlepag();?>
	</head>
	<body>
		<IFRAME src="alertas.php" name="alertas" id="alertas" style="display:none"></IFRAME>
		<span id="todastablas2"></span>
		<table>
			<tr><script>barra_imagenes("teso");</script><?php cuadro_titulos();?></tr>
			<tr><?php menu_desplegable("teso");?></tr>
			<tr class="cinta">
				<td colspan="3" class="cinta"><img src="imagenes/add.png" title="Nuevo" onClick="location.href='teso-traslados.php'" class="mgbt"><img src="imagenes/guarda.png" title="Guardar" onClick="guardar()" class="mgbt"><img src="imagenes/busca.png" title="Buscar" onClick="location.href='teso-buscatraslados.php'" class="mgbt"><img src="imagenes/agenda1.png" title="Agenda" onClick="mypop=window.open('plan-agenda.php','','');mypop.focus()" class="mgbt"><img src="imagenes/nv.png" title="Nueva ventana" onClick="mypop=window.open('teso-principal.php','','');mypop.focus();" class="mgbt"><img src="imagenes/duplicar_pantalla.png" title="Duplicar pesta&ntilde;a" onClick="mypop=window.open('<?php echo $url2; ?>','','');mypop.focus();" class="mgbt"><img src="imagenes/print.png"  title="Imprimir" onClick="pdf()" class="mgbt"></td>
			</tr>
		</table>
			<div id="bgventanamodalm" class="bgventanamodalm">
				<div id="ventanamodalm" class="ventanamodalm">
					<IFRAME src="" name="ventanam" marginWidth=0 marginHeight=0 frameBorder=0 id="ventanam" frameSpacing=0 style=" width:700px; height:130px; top:200; overflow:hidden;"></IFRAME>
				</div>
		</div>
		<?php
			if(!$_POST['oculto'])
			{
				$_POST['tipotras'] = 1;
				$fec = date("d/m/Y");
				$sqlr = "SELECT cuentatraslado FROM tesoparametros WHERE estado = 'S'";
				$res = mysqli_query($linkbd,$sqlr);
				while ($row = mysqli_fetch_row($res)){$_POST['cuentatraslado'] = $row[0];}
				$_POST['fecha'] = $fec;
				$_POST['valor'] = 0;
				$_POST['idcomp'] = selconsecutivo('tesotraslados_cab','id_consignacion');
			}
			ereg( "([0-9]{1,2})/([0-9]{1,2})/([0-9]{2,4})", $_POST['fecha'],$fecha);
			$_POST['vigencia'] = $fecha[3];
		?>
		<form name="form2" method="post" action="">
			<table class="inicio" align="center">
				<tr>
					<td class="titulos" colspan="8">.: Agregar Traslados</td>
					<td class="cerrar" style="width:7%" onClick="location.href='teso-principal.php'">Cerrar</td>
				</tr>
				<tr>
					<td class="tamano01" style="width:2.5cm;">Numero Comp:</td>
					<td style="width:20%"><input type="text" name="idcomp" value="<?php echo $_POST['idcomp']?>" style="width:98%;height:30px;" readonly></td>
					<input type="hidden" name="cuentatraslado" value="<?php echo $_POST['cuentatraslado']?>">
					<td class="tamano01" style="width:3cm;">Fecha:</td>
					<td style="width:12%"><input type="text" id="fc_1198971545" title="DD/MM/YYYY" name="fecha" value="<?php echo $_POST['fecha']?>" maxlength="10" onKeyPress="javascript:return solonumeros(event)" onKeyUp="return tabular(event,this)" onKeyDown="mascara(this,'/',patron,true)" style="width:80%;height:30px;">&nbsp;<img src="imagenes/calendario04.png" onClick="displayCalendarFor('fc_1198971545');" title="Calendario" class="icobut"></td>
					<td class="tamano01" style='width:2.5cm;'>Tipo Traslado</td>
					<td style="width:12%">
						<select name="tipotras" id="tipotras" onKeyUp="return tabular(event,this)" onChange="validar()" style="width:98%;height:30px;">
							<option value="1" <?php if($_POST['tipotras'] == '1') echo "SELECTED"; ?>>Interno</option>
							<option value="2" <?php if($_POST['tipotras'] == '2') echo "SELECTED"; ?>>Externo</option>
						</select>
					</td>
					<?php
						if($_POST['tipotras'] == 2)
						{
							echo"
							<td class='tamano01' style='width:2.5cm;'>Destino:</td>
							<td>
								<select name='tipotrasext' id='tipotrasext' onKeyUp=\"return tabular(event,this)\" onChange=\"validar()\" style='width:100%;height:30px;'>
								<option value=''>Seleccione....</option>";
							$sqlr = "SELECT codigo, base, nombre FROM redglobal WHERE tipo = 'EX' ORDER BY id";
							$res = mysqli_query($linkbd,$sqlr);
							while ($row = mysqli_fetch_row($res))
							{
								if($_POST['tipotrasext'] == $row[0])
								{
									echo "<option value='$row[0]' SELECTED> $row[0] - $row[2]</option>";
									$_POST['baseext'] = $row[1];
									$_POST['ndestino'] = $row[2];
								}
								else {echo "<option value='$row[0]'> $row[0] - $row[2]</option>";}
							}
							echo"</select>
							</td>
							<input type='hidden' name='ndestino' id='ndestino' value='".$_POST['ndestino']."'>
							<input type='hidden' name='baseext' id='baseext' value='".$_POST['baseext']."'>";
						}
						else
						{
							echo"
							<td style='width:2.5cm;'></td>
							<td></td>";
						}
					?>
				</tr>
				<tr>
					<td class="tamano01">Numero Transacci&oacute;n:</td>
					<td><input type="text" name="numero" value="<?php echo $_POST['numero']?>" onKeyUp="return tabular(event,this)" style="width:98%;height:30px;"></td>
					<td  class="tamano01">Concepto Traslado:</td>
					<td colspan="5"><input type="text" name="concepto" value="<?php echo $_POST['concepto']?>"onKeyUp="return tabular(event,this)" style="width:100%;height:30px;"></td>
					<td></td>
				</tr>
				<tr>
					<td class="tamano01">CC Origen :</td>
					<td>
						<select name="cc" onChange="validar()" onKeyUp="return tabular(event,this)" style="width:98%;height:30px;">
							<option value="">Seleccione....</option>
							<?php
								$sqlr = "SELECT * FROM centrocosto WHERE estado='S'";
								$res = mysqli_query($linkbd,$sqlr);
								while ($row = mysqli_fetch_row($res)) 
								{
									if($_POST['cc'] == $row[0])
									{
										echo "<option value='$row[0]' SELECTED> $row[0] - $row[1]</option>";
										$_POST['ccnombre'] = $row[1];
									}
									else {echo "<option value='$row[0]'> $row[0] - $row[1]</option>";}
								}
							?>
						</select>
					</td>
					<input type="hidden" id="ccnombre" name="ccnombre" value="<?php echo $_POST['ccnombre']?>">
					<td class="tamano01">Cuenta Origen:</td>
					<td colspan="2">
						<select id="banco" name="banco" onChange="validar()" onKeyUp="return tabular(event,this)" style="width:98%;height:30px;">
							<option value="">Seleccione....</option>
							<?php
								$sqlr = "SELECT T1.estado, T1.cuenta, T1.ncuentaban, T1.tipo, T2.razonsocial, T1.tercero FROM tesobancosctas AS T1, terceros AS T2 WHERE T1.tercero = T2.cedulanit AND T1.estado = 'S'";
								$res = mysqli_query($linkbd,$sqlr);
								while ($row = mysqli_fetch_row($res))
								{
									if($_POST['banco'] == $row[1])
									{
										echo "<option value='$row[1]' SELECTED>$row[2] - Cuenta $row[3]</option>";
										$_POST['nbanco'] = $row[4];
										$_POST['ter'] = $row[5];
										$_POST['cb'] = $row[2];
									}
									else {echo "<option value='$row[1]'>$row[2] - Cuenta $row[3]</option>";}
								}
							?>
						</select>
					</td>
					<td colspan="3"><input type="text" id="nbanco" name="nbanco" value="<?php echo $_POST['nbanco']?>" style="width:100%;height:30px;" readonly></td>
					<input type="hidden" id="cb" name="cb" value="<?php echo $_POST['cb']?>">
					<input type="hidden" id="ter" name="ter" value="<?php echo $_POST['ter']?>">
				</tr>
				<tr>
					<td class='tamano01'>CC Destino:</td>
					<td>
						<select name='cc2' onChange="validar()" onKeyUp="return tabular(event,this)" style='width:98%;height:30px;'>
							<option value="">Seleccione....</option>
							<?php
								if($_POST['tipotras'] == 1)
								{
									$sqlr = "SELECT * FROM centrocosto WHERE estado = 'S'";
									$res = mysqli_query($linkbd,$sqlr);
									while ($row = mysqli_fetch_row($res))
									{
										if($_POST['cc2'] == $row[0])
										{
											echo "<option value='$row[0]' SELECTED>$row[0] - $row[1]</option>";
											$_POST['ccnombre2'] = $row[1];
										}
										else {echo "<option value='$row[0]'>$row[0] - $row[1]</option>";}
									}
								}
								else
								{
									$linkmulti = conectar_Multi($_POST['baseext']);
									$linkmulti -> set_charset("utf8");
									$sqlr = "SELECT * FROM centrocosto WHERE estado = 'S'";
									$res = mysqli_query($linkmulti,$sqlr);
									while ($row = mysqli_fetch_row($res))
									{
										if($_POST['cc2'] == $row[0])
										{
											echo "<option value='$row[0]' SELECTED>$row[0] - $row[1]</option>";
											$_POST['ccnombre2'] = $row[1];
										}
										else {echo "<option value='$row[0]'>$row[0] - $row[1]</option>";}
									}
								}
							?>
						</select>
					</td>
					<input type="hidden" id="ccnombre2" name="ccnombre2" value="<?php echo $_POST['ccnombre2']?>">
					<td class="tamano01">Cuenta Destino:</td>
					<td colspan="2">
						<select id="banco2" name="banco2"  onChange="validar()" onKeyUp="return tabular(event,this)" style="width:98%;height:30px;">
							<option value="">Seleccione....</option>
							<?php
								if($_POST['tipotras'] == 1)
								{
									$sqlr="SELECT T1.estado, T1.cuenta, T1.ncuentaban, T1.tipo, T2.razonsocial, T1.tercero FROM tesobancosctas AS T1 INNER JOIN terceros AS T2 ON T1.tercero = T2.cedulanit WHERE T1.estado = 'S' ";
									$res = mysqli_query($linkbd,$sqlr);
									while ($row = mysqli_fetch_row($res))
									{
										if($_POST['banco2'] == $row[1])
										{
											echo "<option value='$row[1]' SELECTED>$row[2] - Cuenta $row[3]</option>";
											$_POST['nbanco2'] = $row[4];
											$_POST['cb2'] = $row[2];
											$_POST['ter2'] = $row[5];
										}
										else{echo "<option value='$row[1]'>$row[2] - Cuenta $row[3]</option>";}
									}
								}
								else
								{
									$linkmulti = conectar_Multi($_POST['baseext']);
									$linkmulti -> set_charset("utf8");
									$sqlr="SELECT T1.estado, T1.cuenta, T1.ncuentaban, T1.tipo, T2.razonsocial, T1.tercero FROM tesobancosctas AS T1 INNER JOIN terceros AS T2 ON T1.tercero = T2.cedulanit WHERE T1.estado = 'S' ";
									$res = mysqli_query($linkmulti,$sqlr);
									while ($row = mysqli_fetch_row($res))
									{
										if($_POST['banco2'] == $row[1])
										{
											echo "<option value='$row[1]' SELECTED>$row[2] - Cuenta $row[3]</option>";
											$_POST['nbanco2'] = $row[4];
											$_POST['cb2'] = $row[2];
											$_POST['ter2'] = $row[5];
										}
										else{echo "<option value='$row[1]'>$row[2] - Cuenta $row[3]</option>";}
									}
								}
							?>
						</select>
					</td>
					<td colspan="3"><input type="text" id="nbanco2" name="nbanco2" value="<?php echo $_POST['nbanco2']?>" style="width:100%;height:30px;" readonly></td>
					<input type="hidden" id="cb2" name="cb2" value="<?php echo $_POST['cb2']?>">
					<input type="hidden" id="ter2" name="ter2" value="<?php echo $_POST['ter2']?>">
				</tr>
				<tr>
					<td class="saludo1">Valor:</td>
					<td><input type="text" id="valor" name="valor" value="<?php echo $_POST['valor']?>" onKeyUp="return tabular(event,this)"  style='width:98%;height:30px;'></td>
					<td colspan="5"><em class="botonflecha" onClick="agregardetalle()">Agregar</em></td>
				</tr>
			</table>
			<input type="hidden" name="agregadet" value="0">
			<input type="hidden" name="oculto"value="1">
			<input type="hidden" name="vigencia" value="<?php echo $_POST['vigencia'];?>">
			<script>
				document.form2.valor.focus();
				document.form2.valor.select();
			</script>
			<div class="subpantalla">
				<table class="inicio">
					<tr><td colspan="9" class="titulos">Detalle Traslados</td></tr>
					<tr class="titulos2">
						<td>Tipo</td>
						<td>Destino</td>
						<td>No Transacci&oacute;n</td>
						<td>CC-1</td>
						<td>Cuenta Bancaria 1 </td>
						<td>CC-2</td>
						<td>Cuenta Bancaria 2 </td>
						<td>Valor</td>
						<td><img src="imagenes/del.png"><input type='hidden' name='elimina' id='elimina'></td>
					</tr>
					<?php
						if ($_POST['oculto'] == '9')
						{
							$posi = $_POST['elimina'];
							unset($_POST['dccs'][$posi]);
							unset($_POST['dccs2'][$posi]);
							unset($_POST['dconsig'][$posi]);
							unset($_POST['dbancos'][$posi]);
							unset($_POST['dnbancos'][$posi]);
							unset($_POST['dbancos2'][$posi]);
							unset($_POST['dnbancos2'][$posi]);
							unset($_POST['dcbs'][$posi]);
							unset($_POST['dcbs2'][$posi]);
							unset($_POST['dcts'][$posi]);
							unset($_POST['dcts2'][$posi]);
							unset($_POST['dvalores'][$posi]);
							unset($_POST['dtipotraslado'][$posi]);
							unset($_POST['ddestino'][$posi]);
							unset($_POST['dndestino'][$posi]);
							unset($_POST['dbase'][$posi]);
							unset($_POST['dccnombre'][$posi]);
							unset($_POST['dccnombre2'][$posi]);
							$_POST['dccs'] = array_values($_POST['dccs']);
							$_POST['dccs2'] = array_values($_POST['dccs2']);
							$_POST['dconsig'] = array_values($_POST['dconsig']);
							$_POST['dbancos'] = array_values($_POST['dbancos']);
							$_POST['dnbancos'] = array_values($_POST['dnbancos']);
							$_POST['dbancos2'] = array_values($_POST['dbancos2']);
							$_POST['dnbancos2'] = array_values($_POST['dnbancos2']);
							$_POST['dcbs'] = array_values($_POST['dcbs']);
							$_POST['dcbs2'] = array_values($_POST['dcbs2']);
							$_POST['dcts'] = array_values($_POST['dcts']);
							$_POST['dcts2'] = array_values($_POST['dcts2']);
							$_POST['dvalores'] = array_values($_POST['dvalores']);
							$_POST['dtipotraslado'] = array_values($_POST['dtipotraslado']);
							$_POST['ddestino'] = array_values($_POST['ddestino']);
							$_POST['dndestino'] = array_values($_POST['ddnestino']);
							$_POST['dbase'] = array_values($_POST['dbase']);
							$_POST['dccnombre'] = array_values($_POST['dccnombre']);
							$_POST['dccnombre2'] = array_values($_POST['dccnombre2']);
						}
						if ($_POST['agregadet'] == '1')
						{
							$_POST['dccs'][] = $_POST['cc'];
							$_POST['dccs2'][] = $_POST['cc2'];
							$_POST['dconsig'][] = $_POST['numero'];
							$_POST['dbancos'][] = $_POST['banco'];
							$_POST['dnbancos'][] = $_POST['nbanco'];
							$_POST['dbancos2'][] = $_POST['banco2'];
							$_POST['dnbancos2'][] = $_POST['nbanco2'];
							$_POST['dcbs'][] = $_POST['cb'];
							$_POST['dcbs2'][] = $_POST['cb2'];
							$_POST['dcts'][] = $_POST['ter'];
							$_POST['dcts2'][] = $_POST['ter2'];
							$_POST['dvalores'][] = $_POST['valor'];
							$_POST['dtipotraslado'][] = $_POST['tipotras'];
							$_POST['ddestino'][] = $_POST['tipotrasext'];
							$_POST['dndestino'][] = $_POST['ndestino'];
							$_POST['dbase'][] = $_POST['baseext'];
							$_POST['dccnombre'][] = $_POST['ccnombre'];
							$_POST['dccnombre2'][] = $_POST['ccnombre2'];
							$_POST['agregadet'] = '0';
							echo"
							<script>
								document.form2.cc.value = '';
								document.form2.cc2.value = '';
								document.form2.banco.value = '';
								document.form2.nbanco.value = '';
								document.form2.banco2.value = '';
								document.form2.nbanco2.value = '';
								document.form2.cb.value = '';
								document.form2.cb2.value = '';
								document.form2.ter.value = '';
								document.form2.ter2.value = '';
								document.form2.valor.value = '';
								document.form2.numero.value = '';
								document.form2.ccnombre.value = '';
								document.form2.ccnombre2.value = '';
								document.form2.agregadet.value = '0';
								document.form2.numero.select();
								document.form2.numero.focus();
							</script>";
						}
						$_POST['totalc'] = 0;
						for ($x = 0; $x < count($_POST['dbancos']); $x++)
						{
							if($_POST['dtipotraslado'][$x] == 1)
							{
								$tipotraslado = 'Interno';
								$nomdestino = '--';
							}
							else
							{
								$tipotraslado = 'Externo';
								$nomdestino = $_POST['ddestino'][$x]." - ".$_POST['dndestino'][$x];
							}
							echo "
								<input type='hidden' name='dnbancos[]' value='".$_POST['dnbancos'][$x]."'>
								<input type='hidden' name='dconsig[]' value='".$_POST['dconsig'][$x]."'>
								<input type='hidden' name='dccs[]' value='".$_POST['dccs'][$x]."'>
								<input type='hidden' name='dccnombre[]' value='".$_POST['dccnombre'][$x]."'>
								<input type='hidden' name='dcbs[]' value='".$_POST['dcbs'][$x]."'>
								<input type='hidden' name='dbancos[]' value='".$_POST['dbancos'][$x]."'>
								<input type='hidden' name='dcts[]' value='".$_POST['dcts'][$x]."'>
								<input type='hidden' name='dccs2[]' value='".$_POST['dccs2'][$x]."'>
								<input type='hidden' name='dccnombre2[]' value='".$_POST['dccnombre2'][$x]."'>
								<input type='hidden' name='dcts2[]' value='".$_POST['dcts2'][$x]."'>
								<input type='hidden' name='dnbancos2[]' value='".$_POST['dnbancos2'][$x]."'>
								<input type='hidden' name='dbancos2[]' value='".$_POST['dbancos2'][$x]."'>
								<input type='hidden' name='dcbs2[]' value='".$_POST['dcbs2'][$x]."'>
								<input type='hidden' name='dvalores[]' value='".$_POST['dvalores'][$x]."'>
								<input type='hidden' name='dtipotraslado[]' value='".$_POST['dtipotraslado'][$x]."'>
								<input type='hidden' name='ddestino[]' value='".$_POST['ddestino'][$x]."'>
								<input type='hidden' name='dndestino[]' value='".$_POST['dndestino'][$x]."'>
								<input type='hidden' name='dbase[]' value='".$_POST['dbase'][$x]."'>
							<tr>
								<td class='saludo2'>$tipotraslado</td>
								<td class='saludo2'>$nomdestino</td>
								<td class='saludo2'>".$_POST['dconsig'][$x]."</td>
								<td class='saludo2'>".$_POST['dccs'][$x]." - ".$_POST['dccnombre'][$x]."</td>
								<td class='saludo2'>".$_POST['dcbs'][$x]." - ".$_POST['dnbancos'][$x]."</td>
								<td class='saludo2'>".$_POST['dccs2'][$x]." - ".$_POST['dccnombre2'][$x]."</td>
								<td class='saludo2'>".$_POST['dcbs2'][$x]." - ".$_POST['dnbancos2'][$x]."</td>
								<td class='saludo2'>".$_POST['dvalores'][$x]."</td>
								<td class='saludo2'><a href='#' onclick='eliminar($x)'><img src='imagenes/del.png'></a></td>
							</tr>";
							$_POST['totalc'] = $_POST['totalc'] + $_POST['dvalores'][$x];
							$_POST['totalcf'] = number_format($_POST['totalc'],2,".",",");
						}
						$resultado = convertir($_POST['totalc']);
						$_POST['letras'] = $resultado." Pesos";
						echo "
						<input type='hidden' name='totalcf' type='text' value='".$_POST['totalcf']."'>
						<input type='hidden' name='totalc' value='".$_POST['totalc']."'>
						<input type='hidden' name='letras' value='".$_POST['letras']."'>
						<tr>
							<td colspan='5'></td>
							<td class='saludo2'>Total</td>
							<td class='saludo2'>".$_POST['totalcf']."</td>
						</tr>
						<tr><td colspan='5'>Son: ".$_POST['letras']."</td></tr>";
					?>
				</table>
			</div>
			<?php
				if($_POST['oculto']=='2')
				{
					$sqlr="select count(*) from tesotraslados_cab where id_consignacion = '".$_POST['idcomp']."'";
					$res = mysqli_query($linkbd,$sqlr);
					while($r = mysql_fetch_row($res))
					{
						$numerorecaudos = $r[0];
					}
					if($numerorecaudos == 0)
					{
						ereg( "([0-9]{1,2})/([0-9]{1,2})/([0-9]{2,4})", $_POST['fecha'],$fecha);
						$fechaf = "$fecha[3]-$fecha[2]-$fecha[1]";
						$consec = 0;
						$consec = $_POST['idcomp'];
						$sqlr = "INSERT INTO comprobante_cab (numerotipo, tipo_comp, fecha, concepto, total, total_debito, total_credito, diferencia, estado) VALUES ('$consec','10', '$fechaf', '".$_POST['concepto']."', '0', '".$_POST['totalc']."', '".$_POST['totalc']."', '0', '1')";
						mysqli_query($linkbd,$sqlr);
						$idcomp = mysqli_insert_id();
						$sqlr = "INSERT INTO tesotraslados_cab (id_comp, fecha, vigencia, estado, concepto) VALUES ($idcomp, '$fechaf', '".$_POST['vigencia']."', 'S', '".$_POST['concepto']."')";
						mysqli_query($linkbd,$sqlr);
						$idtraslados = mysqli_insert_id();
						echo "<input type='hidden' name='ncomp' value='$idcomp'>";
						if($_POST['tipotras'] == '1')
						{
							for($x = 0; $x < count($_POST['dbancos']); $x++)
							{
								//**** consignacion  BANCARIA*****
								$sqlr="INSERT INTO comprobante_det (id_comp, cuenta, tercero, centrocosto, detalle, cheque, valdebito, valcredito, estado, vigencia) VALUES ('10 $consec', '".$_POST['dbancos'][$x]."', '".$_POST['dcts'][$x]."', '".$_POST['dccs'][$x]."', 'Traslado ".$_POST['dconsig'][$x]." ".$_POST['dnbancos'][$x]."', '', 0, ".$_POST['dvalores'][$x].", '1', '".$_POST['vigencia']."')";
								mysqli_query($linkbd,$sqlr);
								//*** Cuenta CAJA **
								$sqlr="INSERT INTO comprobante_det (id_comp, cuenta, tercero, centrocosto, detalle, cheque, valdebito, valcredito, estado, vigencia) VALUES ('10 $consec', '".$_POST['dbancos2'][$x]."', '".$_POST['dcts2'][$x]."', '".$_POST['dccs2'][$x]."', 'Traslado ".$_POST['dconsig'][$x]." ".$_POST['dnbancos2'][$x]."', '', ".$_POST['dvalores'][$x].", 0, '1', '".$_POST['vigencia']."')";
								mysqli_query($linkbd,$sqlr);
							}
							for($x = 0; $x < count($_POST['dbancos']); $x++)
							{
								$sqlr="INSERT INTO tesotraslados (id_trasladocab, fecha, ntransaccion, cco, ncuentaban1, tercero1, ccd, ncuentaban2, tercero2, valor, estado, tipo_traslado, destino_ext, tipo_mov) VALUES ('$idtraslados', '$fechaf','".$_POST['dconsig'][$x]."', '".$_POST['dccs'][$x]."', '".$_POST['dcbs'][$x]."', '".$_POST['dcts'][$x]."', '".$_POST['dccs2'][$x]."', '".$_POST['dcbs2'][$x]."', '".$_POST['dcts2'][$x]."', ".$_POST['dvalores'][$x].", 'S','INT','','201')";
								if (!mysqli_query($linkbd,$sqlr))
								{
									echo "<script>despliegamodalm('visible','2','No se pudo ejecutar la petición');</script>";
								}
								else
								{
									echo "
									<script>
										document.form2.numero.value = '';
										document.form2.valor.value = 0;
										document.form2.oculto.value = 1;
										despliegamodalm('visible','3','Se ha almacenado los Traslados con Exito');
									</script>";
								}
							}
						}
						else
						{
							$linkmulti = conectar_Multi($_POST['baseext']);
							$linkmulti -> set_charset("utf8");
							$consecsal = selconsecutivomulti('tesotraslados_cab','id_consignacion',$_POST['baseext']);
							$sqlr = "INSERT INTO comprobante_cab (numerotipo, tipo_comp, fecha, concepto, total, total_debito, total_credito, diferencia, estado) VALUES ('$consec','10', '$fechaf', '".$_POST['concepto']."', '0', '".$_POST['totalc']."', '".$_POST['totalc']."', '0', '1')";
							mysqli_query($linkmulti,$sqlr);
							$idcompsal = mysqli_insert_id();
							$sqlr = "INSERT INTO tesotraslados_cab (id_comp, fecha, vigencia, estado, concepto) VALUES ($idcompsal, '$fechaf', '".$_POST['vigencia']."', 'S', '".$_POST['concepto']."')";
							mysqli_query($linkmulti ,$sqlr);
							$idtrasladossal = mysqli_insert_id();
							for($x = 0; $x < count($_POST['dbancos']); $x++)
							{
								$debito = 0;
								$credito = $_POST['dvalores'][$x];
								$sqlr = "INSERT INTO comprobante_det (id_comp, cuenta, tercero, centrocosto, detalle, cheque, valdebito, valcredito, estado, vigencia) VALUES ('10 $consec', '".$_POST['dbancos'][$x]."', '".$_POST['dcts'][$x]."', '".$_POST['dccs'][$x]."', 'Traslado ".$_POST['dconsig'][$x]." ".$_POST['dnbancos'][$x]."', '',$debito, $credito, '1', '".$_POST['vigencia']."')";
								mysqli_query($linkbd,$sqlr);
								$sqlr="INSERT INTO comprobante_det (id_comp, cuenta, tercero, centrocosto, detalle, cheque, valdebito, valcredito, estado, vigencia) VALUES ('10 $consec', '".$_POST['cuentatraslado']."', '".$_POST['dcts'][$x]."', '".$_POST['dccs'][$x]."', 'Traslado ".$_POST['dconsig'][$x]." ".$_POST['dnbancos'][$x]."', '', $credito, $debito, '1', '".$_POST['vigencia']."')";
								mysqli_query($linkbd,$sqlr);
								$debito = $_POST['dvalores'][$x];
								$credito = 0;
								$sqlr = "INSERT INTO comprobante_det (id_comp, cuenta, tercero, centrocosto, detalle, cheque, valdebito, valcredito, estado, vigencia) VALUES ('10 $consecsal', '".$_POST['dbancos'][$x]."', '".$_POST['dcts'][$x]."', '".$_POST['dccs'][$x]."', 'Traslado ".$_POST['dconsig'][$x]." ".$_POST['dnbancos'][$x]."', '',$debito, $credito, '1', '".$_POST['vigencia']."')";
								mysqli_query($linkmulti,$sqlr);
								$sqlr="INSERT INTO comprobante_det (id_comp, cuenta, tercero, centrocosto, detalle, cheque, valdebito, valcredito, estado, vigencia) VALUES ('10 $consecsal', '".$_POST['cuentatraslado']."', '".$_POST['dcts'][$x]."', '".$_POST['dccs'][$x]."', 'Traslado ".$_POST['dconsig'][$x]." ".$_POST['dnbancos'][$x]."', '', $credito, $debito, '1', '".$_POST['vigencia']."')";
								mysqli_query($linkmulti,$sqlr);
							}
							//***************CREACION DE LA TABLA EN CTRASLADOS *********************
							for($x=0; $x < count($_POST['dbancos']); $x++)
							{
								$sqlr="INSERT INTO tesotraslados (id_trasladocab, fecha, ntransaccion, cco, ncuentaban1, tercero1, ccd, ncuentaban2, tercero2, valor, estado, tipo_traslado, destino_ext, tipo_mov) VALUES ('$idtraslados', '$fechaf', '".$_POST['dconsig'][$x]."', '".$_POST['dccs'][$x]."', '".$_POST['dcbs'][$x]."', '".$_POST['dcts'][$x]."', '".$_POST['dccs2'][$x]."', '".$_POST['dcbs2'][$x]."', '".$_POST['dcts2'][$x]."', ".$_POST['dvalores'][$x].", 'S', 'EXT','','201')";
								mysqli_query($linkbd,$sqlr);
								$sqlr="INSERT INTO tesotraslados (id_trasladocab, fecha, ntransaccion, cco, ncuentaban1, tercero1, ccd, ncuentaban2, tercero2, valor, estado, tipo_traslado, destino_ext, tipo_mov) VALUES ('$idtraslados', '$fechaf', '".$_POST['dconsig'][$x]."', '".$_POST['dccs'][$x]."', '".$_POST['dcbs'][$x]."', '".$_POST['dcts'][$x]."', '".$_POST['dccs2'][$x]."', '".$_POST['dcbs2'][$x]."', '".$_POST['dcts2'][$x]."', ".$_POST['dvalores'][$x].", 'S', 'EXT','','201')";
								mysqli_query($linkmulti,$sqlr);
							}
								echo"<script>despliegamodalm('visible','3','Se ha almacenado los Traslados con Exito');</script>";
						}
					}
					else
					{
						echo "<script>despliegamodalm('visible','2','Ya existe un traslado con este consecutivo ');</script>";
					}
				}
			?>
		</form>
	</body>
</html>