<?php

if (!isset($_GET['input'])) {
    http_response_code(400);
    die('Falta argumento "input"!');
}

if (!isset($_GET['date'])) {
    http_response_code(400);
    die('Falta argumento "date"!');
}

if (!file_exists($_GET['input'])) {
    http_response_code(400);
    die('El archivo csv no existe! ' . $_GET['input']);
}

require __DIR__ . '/vendor/autoload.php';
require 'fpdf.php';
require 'comun.inc';

use Carbon\Carbon;

date_default_timezone_set('America/Bogota');

class PDF extends FPDF
{
    function addImageIfExists($imagePath, $x, $y, $w = 0, $h = 0, $type = 'PNG') {
        if (file_exists($imagePath)) {
            $this->Image($imagePath, $x, $y, $w, $h, $type);
        } else {
            echo '';
        }
    }
    function Header()
    {
        $linkdb = conectar_v7();

        $result = mysqli_query($linkdb, "SELECT * FROM configbasica WHERE estado = 'S'");
        $row = mysqli_fetch_row($result);

        $nit = $row[0];
        $razon_social = $row[1];

        $this->Image('imagenes/escudo.jpg', 23, 13, 25, 25);
        $this->SetFont('Arial', 'B', 10);
        $this->SetY(10);
        $this->RoundedRect(10, 10, 199, 31, 1.5, '');
        $this->Cell(0.1);
        $this->Cell(50, 31, '', 'R', 0, 'L');

        $this->SetFont('Arial', 'B', 14);
        $this->SetY(10);
        $this->Cell(40);
        $this->Cell(149, 31, '', 0, 1, '');
        $this->SetY(8);
        $this->Cell(40);
        $this->Cell(110, 20, mb_convert_encoding('ALCALDÍA MUNICIPAL', 'ISO-8859-1'), 0, 0, 'C');
        $this->SetY(10);
        $this->Cell(40);
        $this->Cell(110, 25, $razon_social, 0, 0, 'C');
        $this->SetY(10);
        $this->Cell(40);
        $this->SetFont('Arial', 'B', 12);
        $this->Cell(110, 38, 'NIT: ' . $nit, 0, 0, 'C');

        #lineas verticales
        $this->SetY(10);
        $this->Cell(135.1);
        $this->Cell(5, 31, '', 'R', 0, 'L');

        $this->SetY(10);
        $this->Cell(165.1);
        $this->Cell(5, 31, '', 'R', 0, 'L');

        #lineas Horizontales
        $this->SetY(20.75);
        $this->Cell(140.5);
        $this->Cell(58.5, 31, '', 'T', 0, 'L');

        $this->SetY(31.5);
        $this->Cell(140.5);
        $this->Cell(58.5, 31, '', 'T', 0, 'L');

        #Textos Horizontales 1
        $this->SetFont('Arial', '', 10);

        $this->SetY(13);
        $this->Cell(135);
        $this->Cell(38, 5, mb_convert_encoding('Código', 'ISO-8859-1'), 0, 0, 'C');

        $this->SetY(24);
        $this->Cell(135);
        $this->Cell(38, 5, 'TRD', 0, 0, 'C');

        $this->SetY(33.75);
        $this->Cell(135);
        $this->Cell(38, 5, mb_convert_encoding('Versión', 'ISO-8859-1'), 0, 0, 'C');

        //Textos Horizontales 2
        $this->SetY(13);
        $this->Cell(165);
        $this->Cell(38, 5, 'D.A 2000', 0, 0, 'C');

        $this->SetY(24);
        $this->Cell(165);
        $this->Cell(38, 5, '', 0, 0, 'C');

        $this->SetY(33.75);
        $this->Cell(165);
        $this->Cell(38, 5, '01', 0, 0, 'C');

        $this->ln(5);

        $this->SetY(27);
        $this->Cell(50.2);

        $this->MultiCell(105.7, 4, '', 0, 'L');

        $this->SetFont('times', 'B', 10);

        $this->ln(2);
    }

    function Footer()
    {
        $linkdb = conectar_v7();

        $sql = "SELECT planacareas_info.correo FROM planacareas_info, planacareas WHERE planacareas_info.codarea = planacareas.codarea AND planacareas.nombrearea LIKE '%SECRETARI_ DE HACIENDA%' AND planacareas.estado='S'";
        $correo = mysqli_fetch_row(mysqli_query($linkdb, $sql));

        if ($correo) {
            $correo = $correo[0];
        }

        $datBasicos = mysqli_fetch_row(mysqli_query($linkdb, 'SELECT direccion, telefono, web FROM configbasica'));
        $lema = mysqli_fetch_row(mysqli_query($linkdb, 'SELECT lema FROM interfaz01'))[0];

        $this->SetY(-35);
        $this->SetFont('Arial', 'BI', 14);
        $this->Cell(0, 10, mb_convert_encoding('"' . $lema . '"', 'ISO-8859-1'), 0, 0, 'C');

        $this->SetFont('Arial', 'I', 10);

        if ($datBasicos[0] !== null && $datBasicos[0] !== '') {
            $this->ln(5);
            $this->Cell(0, 10, mb_convert_encoding($datBasicos[0], 'ISO-8859-1'), 0, 0, 'C');
        }

        if ($datBasicos[1] !== null && $datBasicos[1] !== '') {
            $this->ln(5);
            $this->Cell(0, 10, mb_convert_encoding('Teléfono: ' . $datBasicos[1], 'ISO-8859-1'), 0, 0, 'C');
        }

        if ($datBasicos[2] !== null && $datBasicos[2] !== '') {
            $this->ln(5);
            $this->Cell(0, 10, mb_convert_encoding('Página Web: ' . mb_strtolower($datBasicos[2]), 'ISO-8859-1'), 0, 0, 'C');
        }

        if ($correo !== null) {
            $this->ln(5);
            $this->Cell(0, 10, 'E-mail: ' . $correo, 0, 0, 'C');
        }
    }
}

$pdf = new PDF('P', 'mm', 'Legal');

$predio_ids = [];
$resoluciones = [];
$vigencias = [];
$valor_avaluos = [];
$tasa_por_mils = [];
$predials = [];
$bomberils = [];
$ambientals = [];
$total_liquidaciones = [];
$predial_intereses = [];
$predial_descuento_intereses = [];
$bomberil_intereses = [];
$ambiental_intereses = [];
$descuento_intereses = [];
$codigo_catastros = [];
$vig_ant = [];
$dedua_ant = [];

$csv = fopen($_GET['input'], 'r');

/* if (fgetcsv($csv) !== [
    'resolucion','predio_id','vigencia','valor_avaluo','tasa_por_mil','predial','predial_descuento','bomberil','ambiental','alumbrado','total_liquidacion','dias_mora','predial_intereses','predial_descuento_intereses','bomberil_intereses','bomberil_descuento_intereses','ambiental_intereses','ambiental_descuento_intereses','descuento_intereses','total_interesesresolucion','predio_id','vigencia','valor_avaluo','tasa_por_mil','predial','predial_descuento','bomberil','ambiental','alumbrado','total_liquidacion','dias_mora','predial_intereses','predial_descuento_intereses','bomberil_intereses','bomberil_descuento_intereses','ambiental_intereses','ambiental_descuento_intereses','descuento_intereses','total_intereses'
]) {
    die('El archivo csv no tiene el formato esperado: ' . print_r($header, true));
} */

$linkdb = conectar_v7();

while ($line = fgetcsv($csv)) {
    $result = mysqli_query($linkdb, 'SELECT codigo_catastro FROM predios WHERE id = ' . $line[1]);

    $codigo_catastro = mysqli_fetch_row($result)[0];

    array_push($predio_ids, $line[1]); // predio_id
    array_push($resoluciones, $line[0]); // resolucion
    array_push($vigencias, $line[2]); // vigencia
    array_push($valor_avaluos, $line[3]); // valor_avaluo
    array_push($tasa_por_mils, $line[4]); // tasa_por_mil
    array_push($predials, $line[5]); // predial
    array_push($bomberils, $line[7]); // bomberil
    array_push($ambientals, $line[8]); // ambiental
    array_push($total_liquidaciones, $line[10]); // total_liquidacion
    array_push($predial_intereses, $line[12]); // predial_intereses
    array_push($predial_descuento_intereses, $line[13]); // predial_descuento_intereses
    array_push($bomberil_intereses, $line[14]); // bomberil_intereses
    array_push($ambiental_intereses, $line[16]); // ambiental_intereses
    array_push($descuento_intereses, $line[18]); // descuento_intereses
    array_push($codigo_catastros, $codigo_catastro); // codigo_catastro
    array_push($vig_ant, $line[20]);//deuda anterior vigencas
    array_push($dedua_ant, $line[21]);//Total deuda anterior
}

$result = mysqli_query($linkdb, "SELECT * FROM configbasica WHERE estado = 'S'");
$row = mysqli_fetch_row($result);

$nit = $row[0];
$razon_social = $row[1];
$art_liquidacion = $row[17];
$art_notificacion = $row[18];

$number_formatter = NumberFormatter::create('es', NumberFormatter::SPELLOUT);
$actual = '';

$now = new Carbon;

for ($v = 0; $v < count($codigo_catastros); $v++) {
    if (isset($_GET['resolucion']) && $_GET['resolucion'] != $resoluciones[$v]) {
        continue;
    }

    if ($codigo_catastros[$v] != $actual) {
        $actual = $codigo_catastros[$v];

        $pdf->AddPage();

        $result = mysqli_query($linkdb, 'SELECT direccion, hectareas, metros_cuadrados, area_construida FROM predio_informacions WHERE predio_id = ' . $predio_ids[$v] . ' ORDER BY created_at DESC LIMIT 1');
        $row = mysqli_fetch_row($result);

        $direccion = $row[0];
        $ha = $row[1];
        $m2 = $row[2];
        $ac = $row[3];

        $result = mysqli_query($linkdb, 'SELECT documento, nombre_propietario FROM predio_propietarios WHERE predio_id = ' . $predio_ids[$v] . ' ORDER BY created_at DESC LIMIT 1');
        $row = mysqli_fetch_row($result);

        $tercero = $row[0];
        $ntercero = $row[1];

        $total_liquidacion = 0;

        for ($i = $v; $codigo_catastros[$i] === $codigo_catastros[$v]; $i++) {
            $total_liquidacion += $total_liquidaciones[$i];
        }

        $posy = $pdf->GetY();

        $tipoPredio = substr($codigo_catastros[$v], 0, 2) === '00' ? 'Rural' : 'Urbano';

        $pdf->SetY($posy + 10);
        $pdf->Cell(0.5);
        $pdf->SetFont('Arial', 'B', 10);
        $pdf->MultiCell(195, 7, 'FACTURA No. ' . $resoluciones[$v] . ' - ' . date('Y'), 0, 'C');
        $pdf->MultiCell(195, 8, mb_convert_encoding('DETERMINACIÓN DEL IMPUESTO PREDIAL UNIFICADO - ACTO DE LIQUIDACIÓN OFICIAL SECRETARÍA DE HACIENDA ' . mb_strtoupper($razon_social), 'ISO-8859-1'), 0, 'C');
        $pdf->SetFont('Arial', '', 10);
        $pdf->MultiCell(195, 4, mb_convert_encoding('La Secretaría de Hacienda del ' . $razon_social . ' , en uso de sus facultades legales y especialmente las conferidas en el Art.66 Ley 383 de 1997; Art.59 Ley 788 de 2002; ' . $art_liquidacion . ' Estatuto Tributario Municipal profiere el presente acto de liquidación del Impuesto Predial Unificado, tasas y sobretasas, con relación al predio identificado con cedula catastral No.' . $codigo_catastros[$v] . ' denominado y/o ubicado en ' . mb_strtoupper($direccion) . ' zona '.$tipoPredio.', del ' . $razon_social . ', y con cargo al contribuyente ' . $ntercero . ' identificado con la C.C./Nit. No.' . $tercero . ' y/o actual propietario o poseedor del predio referido, deuda que equivale a la suma de ' . $number_formatter->format($total_liquidacion) . ' PESOS ($ ' . number_format($total_liquidacion, 2) . '), discriminados en los siguientes periodos gravables y conceptos que permiten calcular el monto de la obligación, así:', 'ISO-8859-1'), 0, 'J');

        //1º cuadro

        $pdf->ln(5);
        $pdf->RoundedRect(10, 100, 199, 24, 0.5, '1111');
        $pdf->SetFont('Arial', '', 8);
        $pdf->SetY(102);
        $pdf->Cell(50, 4, mb_convert_encoding('CÓDIGO CATASTRAL', 'ISO-8859-1'), 0, 0, 'L');
        $pdf->Line(58, 100, 58, 112);
        $pdf->Cell(140, 4, mb_convert_encoding('DIRECCIÓN', 'ISO-8859-1'), 0, 0, 'L');
        $pdf->Line(148, 112, 148, 124);

        $pdf->SetY(107);
        $pdf->Cell(50, 4, $codigo_catastros[$v], 0, 0, 'L');
        $pdf->Cell(140, 4, mb_substr(mb_strtoupper($direccion), 0, 80), 0, 0, 'L');
        $pdf->Line(10, 112, 209, 112);

        $pdf->SetY(113);
        $pdf->Cell(80, 4, 'NOMBRE', 0, 0, 'L');
        $pdf->Line(88, 112, 88, 124);
        $pdf->Cell(30, 4, mb_convert_encoding('CÉDULA / NIT', 'ISO-8859-1'), 0, 0, 'L');
        $pdf->Line(118, 112, 118, 124);
        $pdf->Cell(10, 4, 'HA', 0, 0, 'L');
        $pdf->Line(128, 112, 128, 124);
        $pdf->Cell(10, 4, 'M2', 0, 0, 'L');
        $pdf->Line(138, 112, 138, 124);
        $pdf->Cell(10, 4, 'AC', 0, 0, 'L');
        $pdf->Cell(50, 4, mb_convert_encoding('FECHA DE LIQUIDACIÓN', 'ISO-8859-1'), 0, 0, 'L');

        $pdf->SetY(119);
        $pdf->Cell(80, 4, mb_convert_encoding(substr(strtoupper($ntercero), 0, 50), 'ISO-8859-1'), 0, 0, 'L');
        $pdf->Cell(30, 4, $tercero, 0, 0, 'L');
        $pdf->Cell(9, 4, (int) $ha, 0, 0, 'L');
        $pdf->Cell(10, 4, (int) $m2, 0, 0, 'L');
        $pdf->Cell(10, 4, (int) $ac, 0, 0, 'L');
        $pdf->Cell(50, 4, $_GET['date'], 0, 0, 'L');

        // 2º Tabla

        $pdf->SetFont('Arial', '', 6);

        $pdf->SetY(125);
        $pdf->SetX(10.6);
        $pdf->SetFillColor(150, 150, 150);
        $pdf->SetTextColor(255, 255, 255);
        $pdf->Cell(197.7, 5, '', 0, 0, 'C', 1);

        $pdf->SetY(125.5);
        $pdf->Cell(10, 4, mb_convert_encoding('AÑO', 'ISO-8859-1'), '', false, 'C');
        $pdf->Cell(25, 4, mb_convert_encoding('AVALÚO', 'ISO-8859-1'), '', false, 'C');
        $pdf->Cell(10, 4, 'TASA', '', false, 'C');
        $pdf->Cell(25, 4, 'PREDIAL', '', false, 'C');
        $pdf->Cell(16, 4, mb_convert_encoding('INTERÉS', 'ISO-8859-1'), '', false, 'C');
        $pdf->Cell(16, 4, 'BOMBERIL', '', false, 'C');
        $pdf->Cell(16, 4, mb_convert_encoding('INTERÉS', 'ISO-8859-1'), '', false, 'C');
        $pdf->Cell(16, 4, 'AMBIENTAL', '', false, 'C');
        $pdf->Cell(16, 4, mb_convert_encoding('INTERÉS', 'ISO-8859-1'), '', false, 'C');
        $pdf->Cell(16, 4, 'DESCUENTOS', '', false, 'C');
        $pdf->Cell(25, 4, 'VALOR TOTAL', '', false, 'C');
        $pdf->SetTextColor(0, 0, 0);
        $pdf->RoundedRect(10, 124, 199, 6.5, 0.5, '1111');

        $posy = $pdf->GetY();
        $pdf->SetY($posy + 5);

        for ($i = $v; $codigo_catastros[$i] === $codigo_catastros[$v]; $i++) {
            $predial_interes = $predial_intereses[$i] + $predial_descuento_intereses[$i];

            $pdf->Cell(10, 4, $vigencias[$i], 'RL', false, 'C');
            $pdf->Cell(25, 4, number_format($valor_avaluos[$i], 2), 'R', false, 'C');
            $pdf->Cell(10, 4, $tasa_por_mils[$i] . ' x mil', 'R', false, 'C');
            $pdf->Cell(24, 4, number_format($predials[$i], 2), 'R', false, 'C');
            $pdf->Cell(18, 4, number_format($predial_interes, 2), 'R', false, 'C');
            $pdf->Cell(14, 4, number_format($bomberils[$i], 2), 'R', false, 'C');
            $pdf->Cell(18, 4, number_format($bomberil_intereses[$i], 2), 'R', false, 'C');
            $pdf->Cell(14, 4, number_format($ambientals[$i], 2), 'R', false, 'C');
            $pdf->Cell(18, 4, number_format($ambiental_intereses[$i], 2), 'R', false, 'C');
            $pdf->Cell(17, 4, number_format($descuento_intereses[$i], 2), 'R', false, 'C');
            $pdf->Cell(31, 4, number_format($total_liquidaciones[$i], 2), 'R', true, 'C');
        }

        $posy2 = $pdf->GetY();
        $pdf->Line(10, $posy2, 209, $posy2);

        $posy = $pdf->GetY();
        $pdf->SetY($posy+0.4);

        if($vig_ant[$v] != ''){
            $pdf->Cell(10, 4, $vig_ant[$v], 'RL', false, 'C');
            $pdf->Cell(158, 4, 'IMPUESTO PREDIAL UNIFICADO VIGENCIAS ANTERIORES (FACTURACION 2023)', 'R', false, 'C');
            $pdf->Cell(31, 4, number_format($dedua_ant[$v], 2), 'R', true, 'C');
        }

        $posy2 = $pdf->GetY();
        $pdf->Line(10, $posy2, 209, $posy2);

        $sql = "SELECT * FROM tesoparametros WHERE estado = 'S'";
        $res = mysqli_query($linkdb, $sql);

        while ($row = mysqli_fetch_row($res)) {
            $teso = $row[4];
        }

        $web = mysqli_fetch_row(mysqli_query($linkdb, 'SELECT web FROM configbasica'))[0];

        $posy2 = $pdf->GetY();
        $pdf->SetY($posy2 + 5);
        $pdf->SetFont('Arial', 'B', 10);
        $pdf->MultiCell(195, 8, 'RECURSOS', 0, 'C');
        $pdf->SetFont('Arial', '', 10);
        $pdf->MultiCell(195, 4, mb_convert_encoding('Contra la presente Liquidación Oficial procede el Recurso de Reconsideración, el cual deberá interponerse ante el Alcalde Municipal, dentro de los dos (2) meses siguientes a la notificación del presente acto, en virtud de los ' . $art_notificacion . ' . Una vez en firme el presente acto administrativo presta merito ejecutivo.', 'ISO-8859-1'), 0, 'J');
        $pdf->SetFont('Arial', 'B', 10);
        $pdf->MultiCell(195, 8, mb_convert_encoding('CONSTANCIA DE NOTIFICACIÓN', 'ISO-8859-1'), 0, 'C');
        $pdf->SetFont('Arial', '', 10);
        $pdf->MultiCell(195, 4, mb_convert_encoding('La notificación de la factura se realizará mediante inserción en la página web de la Alcaldía del ' . $razon_social . ' ' . mb_strtolower($web) . ' y, simultáneamente con la publicación en medios físicos en el registro, cartelera o lugar visible de la Secretaría de Hacienda de la Alcaldía conforme al artículo No.354 Ley 1819 de 2016.', 'ISO-8859-1'), 0, 'J');
        $pdf->MultiCell(195, 4, '', 0, 0, '');
        $pdf->MultiCell(195, 4, mb_convert_encoding('Los intereses de mora se liquidarán hasta el momento del pago total, calculados de conformidad con las normas legales vigentes.', 'ISO-8859-1'), 0, 'J');
        $pdf->MultiCell(195, 4, '', 0, 0, '');
        $pdf->MultiCell(195, 4, mb_convert_encoding('De conformidad con el decreto 2150 de 1995, la firma mecánica aquí plasmada tiene validez para todos los efectos legales.', 'ISO-8859-1'), 0, 'J');
        $pdf->MultiCell(195, 4, '', 0, 0, '');
        $pdf->MultiCell(195, 4, mb_convert_encoding('Dada en el ' . mb_convert_case($razon_social, MB_CASE_TITLE) . ' a los ' . mb_strtoupper($number_formatter->format($now->day)) . ' ' . $now->locale('es')->isoFormat('(D) \d\í\a\s \d\e MMMM') . ' del ' . mb_strtoupper($number_formatter->format($now->year)) . ' (' . $now->year . ').', 'ISO-8859-1'), 0, 'J');
        $pdf->SetFont('Arial', 'B', 10);
        $pdf->MultiCell(200, 4, mb_convert_encoding("\n\nNOTIFÍQUESE Y CÚMPLASE\n", 'ISO-8859-1'), 0, 'C');
        $pdf->SetFont('Times', 'B', 9);




		//$pdf->Image('imagenes/firma_factura_masiva.jpg' , 80 ,$posy3-40, 60 ,30,'jpg');

        //$pdf->addImageIfExists( $imagePath, 10, $y + 10, 60, 40);


        $pdf->ln(8);
        $pdf->MultiCell(200, 4, "\n" . mb_convert_encoding(mb_strtoupper($teso) . "\nSECRETARÍA DE HACIENDA", 'ISO-8859-1'), 0, 'C');

        $posy3=$pdf->GetY();
        $imagePath = 'imagenes/firma_factura_masiva.png';

        $posyImage = $vig_ant[$v] != '' ? $posy3 - 35 : $posy3 - 25;

        $pdf->addImageIfExists($imagePath, 80, $posyImage, 60, 30);
        //$pdf->ln(4);
        $pdf->SetFont('times', '', 8);
        //$pdf->multicell(199,4,'* Contra la presente liquidaci'.mb_convert_encoding(ó).'n procede el recurso de reconsideraci'.mb_convert_encoding(ó).'n dentro de los dos (2) meses siguientes a su notificaci'.mb_convert_encoding(ó).'n',0);
    }

    if (isset($_GET['resolucion']) && $_GET['resolucion'] == $resoluciones[$v]) {
        break;
    }
}

$pdf->Output();
