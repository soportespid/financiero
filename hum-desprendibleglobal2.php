<?php
	header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=utf8");
	require "comun.inc";
	require "funciones.inc";
	require 'funcionesnomima.inc';
	session_start();
	$linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");
	cargarcodigopag($_GET['codpag'],$_SESSION["nivel"]);
	date_default_timezone_set("America/Bogota");
?>
<!DOCTYPE >
<html lang="es">
	<head>
		<meta http-equiv="Content-Type" content="text/html" charset="utf-8"/>
		<meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1">
		<title>:: IDEAL 10 - Gesti&oacute;n humana</title>
		<link href="css/css2.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<script type="text/javascript" src="css/calendario.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script type="text/javascript" src="css/programas.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script>
			function despliegamodalm(_valor,_tip,mensa,pregunta, variable)
			{
				document.getElementById("bgventanamodalm").style.visibility=_valor;
				if(_valor=="hidden")
				{
					document.getElementById('ventanam').src="";
					switch(document.getElementById('valfocus').value)
					{
						case "1":	document.getElementById('docum').focus();
									document.getElementById('docum').select();
									break;
					}
					document.getElementById('valfocus').value='0';
				}
				else
				{
					switch(_tip)
					{
						case "1":	document.getElementById('ventanam').src="ventana-mensaje1.php?titulos="+mensa;break;
						case "2":	document.getElementById('ventanam').src="ventana-mensaje3.php?titulos="+mensa;break;
						case "3":	document.getElementById('ventanam').src="ventana-mensaje2.php?titulos="+mensa;break;
						case "4":	document.getElementById('ventanam').src="ventana-consulta1.php?titulos="+mensa+"&idresp="+pregunta;break;	
						case "5":	document.getElementById('ventanam').src="ventana-elimina1.php?titulos="+mensa+"&idresp="+pregunta+"&variable="+variable;break;	
					}
				}
			}
			function despliegamodal2(_valor,_pag)
			{
				document.getElementById("bgventanamodal2").style.visibility=_valor;
				if(_valor=="hidden"){document.getElementById('ventana2').src="";}
				else if(_pag=="1"){document.getElementById('ventana2').src="cargafuncionarios-ventana03.php?objeto=tercero&vcodfun=idusuario";}
			}
			function respuestaconsulta(pregunta, variable)
			{
				switch(pregunta)
				{
					case "1":	document.getElementById('oculto').value="2";
								document.form2.submit();break;
					case "2":	document.form2.elimina.value=variable;
								vvend=document.getElementById('elimina');
								vvend.value=variable;
								document.form2.sw.value=document.getElementById('tipomov').value ;
								document.form2.submit();break;
				}
			}
			function funcionmensaje(){}
			function validar()
			{
				document.form2.oculto.value=2;document.form2.submit();
			}
			
			function buscater(e)
			{
				if (document.form2.tercero.value!=""){document.form2.bt.value='1';document.form2.submit();}
				else{document.form2.ntercero.value=""}
			}
			function excell()
			{
				document.form2.action="hum-desprendible2excel.php";
				document.form2.target="_BLANK";
				document.form2.submit(); 
				document.form2.action="";
				document.form2.target="";
			}
		</script>
		<?php titlepag();?>
	</head>
	<body>
		<IFRAME src="alertas.php" name="alertas" id="alertas" style="display:none"></IFRAME>
		<span id="todastablas2"></span>
		<table>
			<tr><script>barra_imagenes("hum");</script><?php cuadro_titulos();?></tr>	 
			<tr><?php menu_desplegable("hum");?></tr>
			<tr>
				<td colspan="3" class="cinta">
					<a class="mgbt"><img src="imagenes/add2.png"/></a>
					<a class="mgbt"><img src="imagenes/guardad.png"/></a>
					<a class="mgbt"><img src="imagenes/buscad.png"/></a>
					<a onClick="mypop=window.open('plan-agenda.php','','');mypop.focus()" class="mgbt"><img src="imagenes/agenda1.png" title="Agenda"/></a>
					<a onClick="mypop=window.open('hum-principal.php','','');mypop.focus();" class="mgbt"><img src="imagenes/nv.png" title="Nueva ventana"/></a>
					<a onClick="mypop=window.open('<?php echo $url2; ?>','','');mypop.focus();" class="tooltip bottom mgbt"><img src="imagenes/duplicar_pantalla.png" title="Duplicar pesta&ntilde;a"></a>
					<a class="mgbt" onClick='excell()'><img src="imagenes/excel.png"></a>
				</td>
			</tr>
		</table>
		<div id="bgventanamodalm" class="bgventanamodalm">
			<div id="ventanamodalm" class="ventanamodalm">
				<IFRAME src="" name="ventanam" marginWidth=0 marginHeight=0 frameBorder=0 id="ventanam" frameSpacing=0 style=" width:700px; height:130px; top:200; overflow:hidden;"> 
				</IFRAME>
			</div>
		</div>		  
		<form name="form2" method="post" action="">
			<table class="inicio">
				<tr>
					<td colspan="10" class="titulos">Detalle desprendible de nomina</td>
					<td class="cerrar" style="width:7%" onClick="location.href='hum-principal.php'">Cerrar</td>
				</tr>
				<tr>
					<td class="saludo1" style="width:2.2cm;">Fecha inicial:</td>
					<td style="width:9%;"><input name="fecha"  type="text" value="<?php echo @$_POST['fecha']?>" maxlength="10"  onKeyPress="javascript:return solonumeros(event)" onKeyUp="return tabular(event,this)"  id="fc_1198971545" onKeyDown="mascara(this,'/',patron,true)" style="width:75%;" title="DD/MM/YYYY" onchange=''/>&nbsp;<a href="#" onClick="displayCalendarFor('fc_1198971545');" title="Calendario"><img src="imagenes/calendario04.png" style="width:20px;"/></a></td>
					<td class="saludo1" style="width:2.2cm;">Fecha final:</td>
					<td style="width:9%;"><input name="fecha2" type="text" value="<?php echo @$_POST['fecha2']?>" maxlength="10"  onKeyPress="javascript:return solonumeros(event)" onKeyUp="return tabular(event,this)"  id="fc_1198971546" onKeyDown="mascara(this,'/',patron,true)" style="width:75%;" title="DD/MM/YYYY" onchange=''/>&nbsp;<a href="#" onClick="displayCalendarFor('fc_1198971546');" title="Calendario"><img src="imagenes/calendario04.png" onChange='' style="width:20px;"/></a></td>
					
					<td style='width:5%'><input type="button" name="buscar" value="Buscar " onClick="validar()"/></td>
					<td></td>
				</tr>
				<input type="hidden" name="oculto" id="oculto" value="1"/>
			</table>
			<div class="subpantalla" style="height:68.5%; width:99.6%; ">
				<?php
					if($_POST['oculto']==2){
						echo "<table class='inicio'>
						<tr><td colspan='25' class='titulos'>Nominas liquidadas</td></tr>
						<tr>
							<td class='titulos2'>UNIDAD EJECUTORA</td>
							<td class='titulos2'>CARGO</td>
							<td class='titulos2'>DOCUMENTO</td>
							<td class='titulos2'>FUNCIONARIO</td>
							<td class='titulos2'>ASIGNACIÓN BÁSICA ANUAL</td>
							<td class='titulos2'>GASTOS DE REPRESENTACIÓN ANUAL</td>
							<td class='titulos2'>PRIMA TÉCNICA ANUAL</td>
							<td class='titulos2'>PRIMA DE GESTIÓN ANUAL</td>
							<td class='titulos2'>PRIMA DE LOCALIZACIÓN ANUAL</td>
							<td class='titulos2'>PRIMA DE COORDINACIÓN ANUAL</td>
							<td class='titulos2'>PRIMA DE RIESGO ANUAL</td>
							<td class='titulos2'>PRIMA EXTRAORDINARIA ANUAL</td>
							<td class='titulos2'>PRIMA O SUBSID ALIMENTAC ANUAL</td>
							<td class='titulos2'>AUXILIO DE TRANSPORTE ANUAL</td>
							<td class='titulos2'>PRIMA DE ANTIGÜEDAD ANUAL</td>
							<td class='titulos2'>BONIFICACIÓN DIRECCIÓN ANUAL</td>
							<td class='titulos2'>PRIMA DE SERVICIOS ANUAL</td>
							<td class='titulos2'>PRIMA DE NAVIDAD ANUAL</td>
							<td class='titulos2'>BONIFIC POR SERVICIOS ANUAL</td>
							<td class='titulos2'>BONIFIC DE RECREACIÓN ANUAL</td>
							<td class='titulos2'>BONIFIC GESTIÓN TERRITORIA ANUAL</td>
							<td class='titulos2'>PRIMA DE VACACIONES ANUAL</td>
							<td class='titulos2'>OTRAS PRIMAS ANUAL</td>
							<td class='titulos2'>CESANTÍAS ANUAL</td>
							<td class='titulos2'>INTERESES SOBRE CESANTÍAS ANUAL</td>
						</tr>";
						preg_match("/([0-9]{2})\/([0-9]{2})\/([0-9]{4})/", $_POST['fecha'],$fecha);
						$fechai = "$fecha[3]-$fecha[2]-$fecha[1]";
						preg_match("/([0-9]{2})\/([0-9]{2})\/([0-9]{4})/", $_POST['fecha2'],$fecha);
						$fechaf = "$fecha[3]-$fecha[2]-$fecha[1]";

						$sqlr = "SELECT T1.codfun, GROUP_CONCAT(T1.descripcion ORDER BY CONVERT(T1.valor, SIGNED INTEGER) SEPARATOR '<->')
						FROM hum_funcionarios T1
						WHERE (T1.item = 'NOMCARGO' OR T1.item = 'DOCTERCERO' OR T1.item = 'NOMTERCERO' OR T1.item = 'UNIEJECUTORA') AND T1.estado = 'S'
						GROUP BY T1.codfun
						ORDER BY CONVERT(T1.codfun, SIGNED INTEGER) DESC";
						$resp = mysqli_query($linkbd,$sqlr);
						while ($row = mysqli_fetch_row($resp)){

							$datos = explode('<->',$row[1]);
							$nomcar[$row[0]] = $datos[0];
							$docfun[$row[0]] = $datos[1];
							$nomfun[$row[0]] = $datos[2];
							$numuni[$row[0]] = $datos[3];

							$totalfun[] = $row[0];
							
							//SUELDO PERSONAL DE NOMINA
							$sqlr01 = "SELECT SUM(ht.totaldev) FROM humnomina AS hm, humnomina_det AS ht WHERE hm.id_nom=ht.id_nom AND hm.fecha BETWEEN CAST('$fechai' AS DATE) AND CAST('$fechaf' AS DATE) AND ht.idfuncionario = '$row[0]' AND hm.estado = 'P' AND (ht.tipopago = '01' OR ht.tipopago = '45') ORDER BY hm.id_nom DESC";
							$resp01 = mysqli_query($linkbd,$sqlr01);
							$row01 = mysqli_fetch_row($resp01);
							$sum01[$row[0]] = $row01[0];

							//GASTOS DE REPRESENTACION
							$sqlr03 = "SELECT SUM(ht.totaldev) FROM humnomina AS hm, humnomina_det AS ht WHERE hm.id_nom=ht.id_nom AND hm.fecha BETWEEN CAST('$fechai' AS DATE) AND CAST('$fechaf' AS DATE) AND ht.idfuncionario = '$row[0]' AND hm.estado = 'P' AND ht.tipopago = '03' ORDER BY hm.id_nom DESC";
							$resp03 = mysqli_query($linkbd,$sqlr03);
							$row03 = mysqli_fetch_row($resp03);
							$sum03[$row[0]] = $row03[0];

							//SUBSIDIO DE ALIMENTACION
							$sqlr04 = "SELECT SUM(ht.totaldev) FROM humnomina AS hm, humnomina_det AS ht WHERE hm.id_nom=ht.id_nom AND hm.fecha BETWEEN CAST('$fechai' AS DATE) AND CAST('$fechaf' AS DATE) AND ht.idfuncionario = '$row[0]' AND hm.estado = 'P' AND ht.tipopago = '04' ORDER BY hm.id_nom DESC";
							$resp04 = mysqli_query($linkbd,$sqlr04);
							$row04 = mysqli_fetch_row($resp04);
							$sum04[$row[0]] = $row04[0];

							//AUXILIO DE TRANSPORTE
							$sqlr05 = "SELECT SUM(ht.totaldev) FROM humnomina AS hm, humnomina_det AS ht WHERE hm.id_nom=ht.id_nom AND hm.fecha BETWEEN CAST('$fechai' AS DATE) AND CAST('$fechaf' AS DATE) AND ht.idfuncionario = '$row[0]' AND hm.estado = 'P' AND ht.tipopago = '05' ORDER BY hm.id_nom DESC";
							$resp05 = mysqli_query($linkbd,$sqlr05);
							$row05 = mysqli_fetch_row($resp05);
							$sum05[$row[0]] = $row05[0];

							//PRIMA DE SERVICIO
							$sqlr06 = "SELECT SUM(ht.totaldev) FROM humnomina AS hm, humnomina_det AS ht WHERE hm.id_nom=ht.id_nom AND hm.fecha BETWEEN CAST('$fechai' AS DATE) AND CAST('$fechaf' AS DATE) AND ht.idfuncionario = '$row[0]' AND hm.estado = 'P' AND ht.tipopago = '06' ORDER BY hm.id_nom DESC";
							$resp06 = mysqli_query($linkbd,$sqlr06);
							$row06 = mysqli_fetch_row($resp06);
							$sum06[$row[0]] = $row06[0];

							//BONIFICACION POR SERVICIOS PRESTADOS
							$sqlr07 = "SELECT SUM(ht.totaldev) FROM humnomina AS hm, humnomina_det AS ht WHERE hm.id_nom=ht.id_nom AND hm.fecha BETWEEN CAST('$fechai' AS DATE) AND CAST('$fechaf' AS DATE) AND ht.idfuncionario = '$row[0]' AND hm.estado = 'P' AND ht.tipopago = '07' ORDER BY hm.id_nom DESC";
							$resp07 = mysqli_query($linkbd,$sqlr07);
							$row07 = mysqli_fetch_row($resp07);
							$sum07[$row[0]] = $row07[0];

							//PRIMA DE NAVIDAD
							$sqlr15 = "SELECT SUM(ht.totaldev) FROM humnomina AS hm, humnomina_det AS ht WHERE hm.id_nom=ht.id_nom AND hm.fecha BETWEEN CAST('$fechai' AS DATE) AND CAST('$fechaf' AS DATE) AND ht.idfuncionario = '$row[0]' AND hm.estado = 'P' AND ht.tipopago = '15' ORDER BY hm.id_nom DESC";
							$resp15 = mysqli_query($linkbd,$sqlr15);
							$row15 = mysqli_fetch_row($resp15);
							$sum15[$row[0]] = $row15[0];

							//PRIMA DE VACACIONES
							$sqlr16 = "SELECT SUM(ht.totaldev) FROM humnomina AS hm, humnomina_det AS ht WHERE hm.id_nom=ht.id_nom AND hm.fecha BETWEEN CAST('$fechai' AS DATE) AND CAST('$fechaf' AS DATE) AND ht.idfuncionario = '$row[0]' AND hm.estado = 'P' AND ht.tipopago = '16' ORDER BY hm.id_nom DESC";
							$resp16 = mysqli_query($linkbd,$sqlr16);
							$row16 = mysqli_fetch_row($resp16);
							$sum16[$row[0]] = $row16[0];
							
							//APORTES DE CESANTIAS
							$sqlr27 = "SELECT SUM(ht.totaldev) FROM humnomina AS hm, humnomina_det AS ht WHERE hm.id_nom=ht.id_nom AND hm.fecha BETWEEN CAST('$fechai' AS DATE) AND CAST('$fechaf' AS DATE) AND ht.idfuncionario = '$row[0]' AND hm.estado = 'P' AND (ht.tipopago = '27' OR ht.tipopago = '47') ORDER BY hm.id_nom DESC";
							$resp27 = mysqli_query($linkbd,$sqlr27);
							$row27 = mysqli_fetch_row($resp27);
							$sum27[$row[0]] = $row27[0];

							//VACACIONES
							$sqlr40 = "SELECT SUM(ht.totaldev) FROM humnomina AS hm, humnomina_det AS ht WHERE hm.id_nom=ht.id_nom AND hm.fecha BETWEEN CAST('$fechai' AS DATE) AND CAST('$fechaf' AS DATE) AND ht.idfuncionario = '$row[0]' AND hm.estado = 'P' AND ht.tipopago = '40' ORDER BY hm.id_nom DESC";
							$resp40 = mysqli_query($linkbd,$sqlr40);
							$row40 = mysqli_fetch_row($resp40);
							$sum40[$row[0]] = $row40[0];
							
							//INDEMNIZACION POR VACACIONES
							$sqlr41 = "SELECT SUM(ht.totaldev) FROM humnomina AS hm, humnomina_det AS ht WHERE hm.id_nom=ht.id_nom AND hm.fecha BETWEEN CAST('$fechai' AS DATE) AND CAST('$fechaf' AS DATE) AND ht.idfuncionario = '$row[0]' AND hm.estado = 'P' AND ht.tipopago = '41' ORDER BY hm.id_nom DESC";
							$resp41 = mysqli_query($linkbd,$sqlr41);
							$row41 = mysqli_fetch_row($resp41);
							$sum41[$row[0]] = $row41[0];

							//BONIFICACION ESPECIAL DE RECREACION
							$sqlr42 = "SELECT SUM(ht.totaldev) FROM humnomina AS hm, humnomina_det AS ht WHERE hm.id_nom=ht.id_nom AND hm.fecha BETWEEN CAST('$fechai' AS DATE) AND CAST('$fechaf' AS DATE) AND ht.idfuncionario = '$row[0]' AND hm.estado = 'P' AND ht.tipopago = '42' ORDER BY hm.id_nom DESC";
							$resp42 = mysqli_query($linkbd,$sqlr42);
							$row42 = mysqli_fetch_row($resp42);
							$sum42[$row[0]] = $row42[0];

							//BONIFICACION DE GESTION TERRITORIAL PARA ALCALDES
							$sqlr43 = "SELECT SUM(ht.totaldev) FROM humnomina AS hm, humnomina_det AS ht WHERE hm.id_nom=ht.id_nom AND hm.fecha BETWEEN CAST('$fechai' AS DATE) AND CAST('$fechaf' AS DATE) AND ht.idfuncionario = '$row[0]' AND hm.estado = 'P' AND ht.tipopago = '43' ORDER BY hm.id_nom DESC";
							$resp43 = mysqli_query($linkbd,$sqlr42);
							$row43 = mysqli_fetch_row($resp42);
							$sum43[$row[0]] = $row43[0];
							$sumveri = $row01[0] + $row03[0] + $row04[0] + $row05[0] + $row06[0] + $row15[0] + $row07[0] + $row42[0] + $row43[0] + $row16[0] + $row27[0];
							$co="zebra1";
							$co2="zebra2";
							if($sumveri > 0){
								echo "
								<tr class='$co' style='text-transform:uppercase'>
									<td >$datos[3]</td>
									<td >$datos[0]</td>
									<td >$datos[1]</td>
									<td >$datos[2]</td>
									<td style='text-align:right;'>".number_format($row01[0],2,",",".")."</td>
									<td style='text-align:right;'>".number_format($row03[0],2,",",".")."</td>
									<td style='text-align:right;'>".number_format(0,2,",",".")."</td>
									<td style='text-align:right;'>".number_format(0,2,",",".")."</td>
									<td style='text-align:right;'>".number_format(0,2,",",".")."</td>
									<td style='text-align:right;'>".number_format(0,2,",",".")."</td>
									<td style='text-align:right;'>".number_format(0,2,",",".")."</td>
									<td style='text-align:right;'>".number_format(0,2,",",".")."</td>
									<td style='text-align:right;'>".number_format($row04[0],2,",",".")."</td>
									<td style='text-align:right;'>".number_format($row05[0],2,",",".")."</td>
									<td style='text-align:right;'>".number_format(0,2,",",".")."</td>
									<td style='text-align:right;'>".number_format(0,2,",",".")."</td>
									<td style='text-align:right;'>".number_format($row06[0],2,",",".")."</td>
									<td style='text-align:right;'>".number_format($row15[0],2,",",".")."</td>
									<td style='text-align:right;'>".number_format($row07[0],2,",",".")."</td>
									<td style='text-align:right;'>".number_format($row42[0],2,",",".")."</td>
									<td style='text-align:right;'>".number_format($row43[0],2,",",".")."</td>
									<td style='text-align:right;'>".number_format($row16[0],2,",",".")."</td>
									<td style='text-align:right;'>".number_format(0,2,",",".")."</td>
									<td style='text-align:right;'>".number_format($row27[0],2,",",".")."</td>
									<td style='text-align:right;'>".number_format(0,2,",",".")."</td>
								</tr>
								<input type='hidden' name='nom01[]' value='$datos[3]'/>
								<input type='hidden' name='nom02[]' value='$datos[0]'/>
								<input type='hidden' name='nom03[]' value='$datos[1]'/>
								<input type='hidden' name='nom04[]' value='$datos[2]'/>
								<input type='hidden' name='nom05[]' value='$row01[0]'/>
								<input type='hidden' name='nom06[]' value='$row03[0]'/>
								<input type='hidden' name='nom07[]' value='$row04[0]'/>
								<input type='hidden' name='nom08[]' value='$row05[0]'/>
								<input type='hidden' name='nom09[]' value='$row06[0]'/>
								<input type='hidden' name='nom10[]' value='$row15[0]'/>
								<input type='hidden' name='nom11[]' value='$row07[0]'/>
								<input type='hidden' name='nom12[]' value='$row42[0]'/>
								<input type='hidden' name='nom13[]' value='$row43[0]'/>
								<input type='hidden' name='nom14[]' value='$row16[0]'/>
								<input type='hidden' name='nom15[]' value='$row27[0]'/>
								";
								$aux=$co;
								$co=$co2;
								$co2=$aux;
							}
						}
						echo "</table>";
					}
				?>
			</div>
			<div id="bgventanamodal2">
				<div id="ventanamodal2">
					<IFRAME  src="" name="buster" marginWidth=0 marginHeight=0 frameBorder=0 id="ventana2" frameSpacing=0 style="left:500px; width:900px; height:500px; top:200;"></IFRAME>
				</div>
			</div>
		</form>
	</body>
</html>
