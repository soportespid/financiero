<?php
	header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=utf8");
	require "comun.inc";
	require "funciones.inc";
	require "funcionesSP.inc.php";
	session_start();
	$linkbd = conectar_v7();
    $linkbd -> set_charset("utf8");
	cargarcodigopag($_GET['codpag'],$_SESSION["nivel"]);
	date_default_timezone_set("America/Bogota");
	//error_reporting(E_ALL);
?>
<!DOCTYPE >
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="es">
	<head>
		<meta http-equiv="Content-type" content="text/html; charset=utf8"/>
		<meta http-equiv="X-UA-Compatible" content="IE=9"/>
		<title>:: IDEAL 10 - Servicios P&uacute;blicos</title>
		<link href="css/css2.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<link href="css/css4.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
        <link href="css/cssSP.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<script type="text/javascript" src="css/programas.js"></script>
        <script type="text/javascript" src="css/calendario.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script src="sweetalert2/dist/sweetalert2.min.js"></script>
		<link rel="stylesheet" href="sweetalert2/dist/sweetalert2.min.css">

		<script>
			function despliegamodalm(_valor,_tip,mensa,pregunta)
			{
				document.getElementById("bgventanamodalm").style.visibility=_valor;
				if(_valor=="hidden"){document.getElementById('ventanam').src="";}
				else
				{
					switch(_tip)
					{
						case "1":	document.getElementById('ventanam').src="ventana-mensaje1.php?titulos="+mensa;break;
						case "2":	document.getElementById('ventanam').src="ventana-mensaje3.php?titulos="+mensa;break;
						case "3":	document.getElementById('ventanam').src="ventana-mensaje2.php?titulos="+mensa;break;
						case "4":	document.getElementById('ventanam').src="ventana-consulta1.php?titulos="+mensa+"&idresp="+pregunta;break;	
					}
				}
			}

            function despliegamodal(estado, modal)
			{
				document.getElementById("bgventanamodal2").style.visibility=estado;

				if(estado == "hidden")
				{
					document.getElementById('ventana2').src="";
				}
				else
				{
					switch (modal) {
						case 'usuarios':
							document.getElementById('ventana2').src = 'serv-modalUsuariosRecaudo.php';
						break;

						default:
							break;
					}
				}
			}

            function despliegamodal2(_valor, _table)
			{
                document.getElementById('bgventanamodal2').style.visibility = _valor;
                
                
					if(_table == 'srvcuentasbancarias')
				{
					document.getElementById('ventana2').src = 'cuentasBancarias-ventana.php';
				}
            }

			function funcionmensaje()
			{
                var consecutivo = document.getElementById('consecutivo').value;
				
			}

			function actualizar()
			{
				document.form2.submit();
			}

			function guardar()
			{	
                var numeroRecaudo = document.getElementById('numeroRecaudo').value;

				if(numeroRecaudo != '')
				{
					var numeroFactura = document.getElementById('numeroFactura').value;
					
					if(numeroFactura != '')
					{
						var codigoUsuario = document.getElementById('codUsuario').value;

						if(codigoUsuario != '')
						{
							var fecha = document.getElementById('fecha').value;

							if(fecha != '')
							{
								var tipoPago = document.getElementById('modoRecaudo').value;

								if(tipoPago != '-1')
								{
									if(tipoPago == 'banco')
									{
										var cuentaBancaria = document.getElementById('cuentaBancaria').value;

										if(cuentaBancaria != '')
										{
											Swal.fire({
												icon: 'question',
												title: 'Seguro que quieres guardar?',
												showDenyButton: true,
												confirmButtonText: 'Guardar',
												denyButtonText: 'Cancelar',
												}).then((result) => {
												if (result.isConfirmed) {
													document.form2.oculto.value='2';
													document.form2.submit();
												} else if (result.isDenied) {
													Swal.fire('Guardar cancelado', '', 'info')
												}
											})
										}
										else
										{
											Swal.fire({
												icon: 'warning',
												title: 'Debe seleccionar una cuenta de banco'
											})
										}
									}
									
									if(tipoPago == 'caja')
									{
										Swal.fire({
											icon: 'question',
											title: 'Seguro que quieres guardar?',
											showDenyButton: true,
											confirmButtonText: 'Guardar',
											denyButtonText: 'Cancelar',
											}).then((result) => {
											if (result.isConfirmed) {
												document.form2.oculto.value='2';
												document.form2.submit();
											} else if (result.isDenied) {
												Swal.fire('Guardar cancelado', '', 'info')
											}
										})
									}
								}
								else
								{
									Swal.fire({
										icon: 'warning',
										title: 'Seleccione el medio de recaudo'
									})
								}
							}
							else
							{
								Swal.fire({
									icon: 'warning',
									title: 'La fecha no puede estar vacía'
								})
							}
						}
						else
						{
							Swal.fire({
								icon: 'warning',
								title: 'Código de usuario vacío'
							})
						}
					}
					else
					{
						Swal.fire({
							icon: 'warning',
							title: 'Número de factura vacío'
						})
					}
				}
				else
				{
					Swal.fire({
						icon: 'warning',
						title: 'Número de recaudo no se encuentra, contacte a soporte'
					})
				}
			}

            function respuestaconsulta(pregunta)
			{
				switch(pregunta)
				{
					case "1":	
						document.form2.oculto.value = '2';
						document.form2.submit();
					break;
				}
			}

            function pulsar(e) 
            {
                if (e.keyCode === 13 && !e.shiftKey) 
                {
                    e.preventDefault();
                    
                    var numeroFactura = document.getElementById('numeroFactura').value;

                    if(numeroFactura.trim() != '')
                    {
                        document.form2.bc.value = '2';
						document.form2.submit();
                    }
					else
					{
						Swal.fire({
							icon: 'warning',
							title: 'Numero de factura vacio'
						})
					}
                }
            }

		</script>
		<?php titlepag();?>
	</head>
	<body>
		<IFRAME src="alertas.php" name="alertas" id="alertas" style="display:none"></IFRAME>
		<span id="todastablas2"></span>
		<table>
			<tr><script>barra_imagenes("serv");</script><?php cuadro_titulos();?></tr>

			<tr><?php menu_desplegable("serv");?></tr>

			<tr>
				<td colspan="3" class="cinta">
                    <a href="serv-crear-recaudo.php" class="mgbt"><img src="imagenes/add.png"/></a>
					<a onclick="guardar()" class="mgbt"><img src="imagenes/guarda.png" title="Guardar"/></a>
					<a href="serv-buscar-recaudo.php" class="mgbt"><img src="imagenes/busca.png" title="Buscar"/></a>
					<a onclick="mypop=window.open('plan-agenda.php','','');mypop.focus()" class="mgbt"><img src="imagenes/agenda1.png" title="Agenda" /></a>
					<a onclick="mypop=window.open('serv-principal.php','','');mypop.focus();" class="mgbt"><img src="imagenes/nv.png" title="Nueva ventana"></a>
					<a onclick="mypop=window.open('<?php echo $url2; ?>','','');mypop.focus();" class="mgbt"><img src="imagenes/duplicar_pantalla.png" title="Duplicar pesta&ntilde;a"></a>
                </td>
			</tr>
		</table>

		<div id="bgventanamodalm" class="bgventanamodalm">
			<div id="ventanamodalm" class="ventanamodalm">
				<IFRAME src="" name="ventanam" marginWidth=0 marginHeight=0 frameBorder=0 id="ventanam" frameSpacing=0 style=" width:700px; height:130px; top:200; overflow:hidden;"></IFRAME>
			</div>
		</div>

		<form name="form2" method="post" action="">
			<?php 
                $vigusu=vigencia_usuarios($_SESSION['cedulausu']);

				if(@$_POST['oculto']=="")
				{
                    $_POST['fecha'] = date("d/m/Y");
                    $_POST['numeroRecaudo'] = selconsecutivo('srv_recaudo_factura','codigo_recaudo');
				}
                
				if(@$_POST['bc'] == '2')
				{
                    $numeroFactura = "";
					$numeroFactura = $_POST['numeroFactura'];
					
					$sqlCorteDetalles = "SELECT estado_pago FROM srvcortes_detalle WHERE numero_facturacion = $numeroFactura AND estado_pago = 'S'";
					$rowCorteDetalles = mysqli_fetch_row(mysqli_query($linkbd, $sqlCorteDetalles));

					if($rowCorteDetalles[0] != '')
					{
						if($rowCorteDetalles['0'] == 'S')
						{
							$sqlFacturas = "SELECT cliente, cod_usuario, nombre_suscriptor, documento FROM srvfacturas WHERE num_factura = $numeroFactura AND estado = 'ACTIVO' LIMIT 1";
							$rowFacturas = mysqli_fetch_row(mysqli_query($linkbd, $sqlFacturas));

							$_POST['id_cliente'] = $rowFacturas[0];
							$_POST['codUsuario'] = $rowFacturas[1];
							$_POST['nombreSuscriptor'] = $rowFacturas[2];
							$_POST['documento'] = $rowFacturas[3];
							$valorFactura = consultaValorTotalSRVFACTURAS($numeroFactura);
							$valorFactura = ($valorFactura / 100);
                            $valorFactura = ceil($valorFactura);
                            $valorFactura = $valorFactura * 100;
                            $_POST['valorFactura'] = $valorFactura;
                            $valorFactura = number_format($valorFactura, 2, ',', '.');	
							$_POST['valorFacturaVisible'] = $valorFactura;
							$_POST['concepto'] = "Pago completo de la factura numero ".$numeroFactura." del codigo de usuario ".$rowFacturas[1];
						}
						else
						{
							echo "
								<script>
									Swal.fire({
										icon: 'error',
										title: 'Factura no pendiente de pago'
									})
								</script>
							";

							$_POST['numeroFactura'] = '';
						}
					}
					else
					{
						echo "
							<script>
								Swal.fire({
									icon: 'error',
									title: 'No se encontro número de factura'
								})
							</script>
						";

						$_POST['numeroFactura'] = '';
					}
				}	
				
			?>
            <div>
                <table class="inicio">
                    <tr>
                        <td class="titulos" colspan="6">.: Recaudo Facturación</td>
                        <td class="cerrar" style="width:7%" onClick="location.href='serv-principal.php'">Cerrar</td>
                    </tr>
                </table>

                <div class="subpantalla" style="height:30%; width:99.5%; float:left; overflow: hidden;">
					<table class="inicio">
                        <tr>
                            <td class="titulos" colspan="9">Datos Recaudo: </td>
                        </tr>

                        <tr>
                            <td class="tamano01" style="width:10%;">Número de Recaudo:</td>
                            <td style="width:10%;">
                                <input  type="text" name="numeroRecaudo" id="numeroRecaudo" value="<?php echo @ $_POST['numeroRecaudo'];?>" style="width:100%;text-align:center;" readonly/>
                            </td>

                            <td class="tamano01" style="width:10%;">N° de Factura:</td>

                            <td style="width:15%;">
                                <input type="text" name="numeroFactura" id="numeroFactura" value="<?php echo @ $_POST['numeroFactura'];?>" style="text-align:center; width:100%;" class="colordobleclik" ondblclick="despliegamodal('visible', 'usuarios')" onkeypress="pulsar(event)"/>
                            </td>

                            <td class="tamano01" style="width: 10%;">Código Usuario:</td> 
                            <td style="width: 15%">
                                <input type="text" name='codUsuario' id='codUsuario' value="<?php echo @$_POST['codUsuario'];?>" style="text-align:center; width: 100%;" readonly/>

                                <input type="hidden" name="id_cliente" id="id_cliente" value="<?php echo $_POST['id_cliente'] ?>">
                            </td>
                        </tr>

                        <tr>
                            <td class="tamano01" style="width: 10%;">Documento Suscriptor: </td>
							<td>
								<input type="text" name="documento" id="documento" value="<?php echo @ $_POST['documento'];?>" style="text-align: center; width: 100%;" readonly/>
							</td>

                            <td class="tamano01" style="width: 10%;">Suscriptor: </td>
							<td>
								<input type="text" name="nombreSuscriptor" id="nombreSuscriptor" value="<?php echo @ $_POST['nombreSuscriptor'];?>" style="text-align: center; width: 100%;" readonly/>
							</td>

                            <td class="tamano01" style="width:10%;">Fecha:</td>
							<td style="width:15%;">
								<input type="text" name="fecha" id="fecha" value="<?php echo @ $_POST['fecha']?>" onchange="" maxlength="10" onKeyUp="return tabular(event,this)" onKeyDown="mascara(this,'/',patron,true)" title="DD/MM/YYYY" placeholder="DD/MM/YYYY" style="height:30px;text-align:center;width:85%;" readonly/>
								
								<a href="#" onClick="displayCalendarFor('fecha');" title="Calendario">
									<img src="imagenes/calendario04.png" style="width:25px;">
								</a>
							</td>
                        </tr>

                        <tr>
                            <td class="tamano01">Valor Factura: </td>
							<td>
                                <input type="text" name="valorFacturaVisible" id="valorFacturaVisible" value="<?php echo $_POST['valorFacturaVisible'] ?>" style="text-align:center; width: 100%;" readonly>	
								<input type="hidden" name="valorFactura" id="valorFactura" value="<?php echo $_POST['valorFactura'] ?>">	
							</td>

                            <td class="tamano01">Concepto de pago:</td>
                            <td colspan="3">
                                <input  type="text" name="concepto" id="concepto" value="<?php echo @ $_POST['concepto'];?>" style="width:100%;height:30px;" />
                            </td>
                        </tr>

                        <tr>
                            <td class="tamano01">Recaudo En:</td>
							<td>
								<select  name="modoRecaudo" id="modoRecaudo" style="width:100%;" onChange="actualizar()" class="">
									<option class="aumentarTamaño" value="-1">:: SELECCIONE TIPO DE RECAUDO ::</option>
									<option class="aumentarTamaño" value="caja" <?php if(@$_POST['modoRecaudo']=='caja') echo "SELECTED"; ?>>Caja</option>
									<option class="aumentarTamaño" value="banco" <?php if(@$_POST['modoRecaudo']=='banco') echo "SELECTED"; ?>>Banco</option>
								</select>
							</td>

                            <?php
								if (@$_POST['modoRecaudo'] == 'banco')
								{
							?>
									<td class="tamano01" style="width: 3cm;">Cuenta Banco: </td> 
									<td>
										<input type="text" id="banco" name="banco" style="text-align:center; width: 90%;" onKeyPress="javascript:return solonumeros(event)" onKeyUp="return tabular(event,this)" value="<?php echo $_POST['banco']?>" onClick="document.getElementById('banco').focus();document.getElementById('banco').select();" readonly>
										<input type="hidden" name="cuentaBancaria" id="cuentaBancaria" value="">
										<a title="Bancos" onClick="despliegamodal2('visible','srvcuentasbancarias');" style="cursor:pointer;"><img src="imagenes/find02.png" style="width:20px;"/></a>
									</td>	

									<td colspan="2">
										<input type="text" name="nbanco" id="nbanco" style="text-align:center; width:100%;" value="<?php echo $_POST['nbanco']?>"  readonly>
									</td>	
							<?php
								}
							?>
                        </tr>
                    </table>
                </div>
            </div>

			<input type="hidden" name="oculto" id="oculto" value="1"/>
            <input type="hidden" name="bc" id="bc" value="1"/>

			<?php 
				if(@$_POST['oculto']=="2")
				{
                    $codRecaudo = selconsecutivo('srv_recaudo_factura','codigo_recaudo');
					$idCliente = $_POST['id_cliente'];
					$codUsuario = $_POST['codUsuario'];
					$suscriptor = $_POST['nombreSuscriptor'];
					$documento = $_POST['documento'];
					$numeroFactura = $_POST['numeroFactura'];
					$fec = explode("/", $_POST['fecha']);
                	$fecha = $fec[2].'-'.$fec[1].'-'.$fec[0];
					$año = $fec[2];
					$medioPago = $_POST['modoRecaudo'];
					$corte = buscaCorteActivo();
					$codigoComprobante = "30";

					if ($medioPago == 'caja') 
					{
						$sqlCuentaCaja = "SELECT cuentacaja FROM tesoparametros";
						$resCuentaCaja = mysqli_query($linkbd,$sqlCuentaCaja);
						$rowCuentaCaja = mysqli_fetch_row($resCuentaCaja);
						$numeroCuenta = $rowCuentaCaja[0];
					} 
					else {$numeroCuenta = $_POST['cuentaBancaria'];}
					
					$conceptoDePago = $_POST['concepto'];
					$valor = $_POST['valorFactura'];
					$usuario = $_SESSION['usuario'];
					$estado = "ACTIVO";

					$sqlZonaUso = "SELECT zona_uso FROM srvclientes WHERE id = $idCliente";
					$rowZonaUso = mysqli_fetch_row(mysqli_query($linkbd, $sqlZonaUso));

					if($rowZonaUso[0] == 1) {

						$conceptoCargoFijo = "cargo_fijo_u";
						$conceptoConsumo = "consumo_u";
						$conceptoSubsidioCF = "subsidio_cf_u";
						$conceptoSubsidioCS = "subsidio_cs_u";
						$conceptoContribucion = "contribucion_u";
					} else {
						$conceptoCargoFijo = "cargo_fijo_r";
						$conceptoConsumo = "consumo_r";
						$conceptoSubsidioCF = "subsidio_cf_r";
						$conceptoSubsidioCS = "subsidio_cs_r";
						$conceptoContribucion = "contribucion_r";
					}

					$sqlRecaudoFactura = "INSERT INTO srv_recaudo_factura (codigo_recaudo, id_cliente, cod_usuario, suscriptor, documento, numero_factura, fecha_recaudo, medio_pago, numero_cuenta, concepto, valor_pago, usuario, estado) VALUES ($codRecaudo, $idCliente, '$codUsuario', '$suscriptor', '$documento', $numeroFactura, '$fecha', '$medioPago', '$numeroCuenta', '$conceptoDePago', $valor, '$usuario', 'ACTIVO')";
					
					if(mysqli_query($linkbd, $sqlRecaudoFactura))
					{
						$sqlServicios = "SELECT id FROM srvservicios ORDER BY id ASC";
						$resServicios = mysqli_query($linkbd, $sqlServicios);
						while($rowServicios = mysqli_fetch_row($resServicios))
						{
							$credito = 0;
							$debito = 0;

							$sqlDetallesFactura = "SELECT credito, debito FROM srvdetalles_facturacion WHERE numero_facturacion = $numeroFactura AND id_servicio = $rowServicios[0] AND (credito > 0 OR debito > 0) ORDER BY id_tipo_cobro ASC";
							$resDetallesFactura = mysqli_query($linkbd, $sqlDetallesFactura);
							while ($rowDetallesFactura = mysqli_fetch_row($resDetallesFactura))
							{
								$credito += $rowDetallesFactura[0];
								$debito += $rowDetallesFactura[1];
							}

							$valor = $credito - $debito;

							if($valor > 0)
							{
								$sqlPagoKardex = "INSERT INTO srvdetalles_facturacion (corte, id_cliente, numero_facturacion, tipo_movimiento, fecha_movimiento, id_servicio, id_tipo_cobro, credito, debito, saldo, estado) VALUES ($corte, $idCliente, $numeroFactura, '201', '$fecha', $rowServicios[0], 12, 0, $valor, 0, 'S')";				
								$validacionGuardado1 = mysqli_query($linkbd, $sqlPagoKardex);
							}		
						}

						if($validacionGuardado1 == TRUE)
						{
							$sqlComprobanteCabecera = "INSERT INTO comprobante_cab (numerotipo, tipo_comp, fecha, concepto, total_debito, total_credito, estado) VALUES ($codRecaudo, '$codigoComprobante', '$fecha', '$conceptoDePago', $valor, $valor, '1')";

							if (mysqli_query($linkbd, $sqlComprobanteCabecera)) {
								
								$sqlServicios = "SELECT id FROM srvservicios ORDER BY id ASC";
								$resServicios = mysqli_query($linkbd, $sqlServicios);
								while ($rowServicios = mysqli_fetch_assoc($resServicios)) {

									$deudaAnterior = "";

									$sqlDeudaAnterior = "SELECT credito FROM srvdetalles_facturacion WHERE numero_facturacion = $numeroFactura AND id_servicio = $rowServicios[id] AND id_tipo_cobro = 8 AND credito > 0";
									$rowDedudaAnterior = mysqli_fetch_assoc(mysqli_query($linkbd, $sqlDeudaAnterior));

									if($rowDedudaAnterior['credito'] != 0) {
										$deudaAnterior = true;
									}else {
										$deudaAnterior = false;
									}

									if ($deudaAnterior == false) {
										
										$cargoFijo = 0;
										$consumo = 0;
										$subsidioCF = 0;
										$subsidioCS = 0;
										$contribucion = 0;
										$acuerdoPago = 0;
										$valorCF = 0;
										$valorCS = 0;
										$saldoInicial = 0;

										$sqlDetallesFactura = "SELECT id_tipo_cobro, credito, debito FROM srvdetalles_facturacion WHERE numero_facturacion = $numeroFactura AND tipo_movimiento = '101' AND id_servicio = $rowServicios[id] AND (credito > 0 OR debito > 0) ORDER BY id_tipo_cobro ASC";
										$resDetallesFactura = mysqli_query($linkbd, $sqlDetallesFactura);
										while ($rowDetallesFactura = mysqli_fetch_assoc($resDetallesFactura)) {

											switch ($rowDetallesFactura['id_tipo_cobro']) {
												case 1:
													$cargoFijo = $rowDetallesFactura['credito'];
													break;
						
												case 2:
													$consumo = $rowDetallesFactura['credito'];
													break;
						
												case 3:
													$subsidioCF = $rowDetallesFactura['debito'];
													break;
						
												case 4:
													$subsidioCS = $rowDetallesFactura['debito'];
													break;
						
												case 5:
													$contribucion = $rowDetallesFactura['credito'];
													break;
						
												case 7:
													$acuerdoPago = $rowDetallesFactura['credito'];
													break;
											}
										}

										if ($cargoFijo > 0) {

											$valorCF = $cargoFijo - $subsidioCF;
						
											$sql = "SELECT cargo_fijo_u, cc, nombre FROM srvservicios WHERE id = $rowServicios[id] ";
											$res = mysqli_query($linkbd,$sql);
											$row = mysqli_fetch_row($res);
						
											$concepto = concepto_cuentasn2($row[0],'SS',10,$row[1],$fecha);
						
											for ($i=0; $i < count($concepto); $i++) 
											{ 
												if($concepto[$i][2] == 'S')
												{
													$cuentaDebito = $concepto[$i][0];
												}
											}
						
											$sqlr="INSERT INTO comprobante_det (id_comp,cuenta,tercero,centrocosto,detalle,cheque,valdebito, valcredito,estado,vigencia) VALUES ('$codigoComprobante $codRecaudo', '$numeroCuenta', '$documento', '$row[1]', 'Pago completo del cargo fijo de $row[2] de la factura $numeroFactura', '', $valorCF, 0, '1', '$año')";
											mysqli_query($linkbd,$sqlr);
						
											$sqlr="INSERT INTO comprobante_det (id_comp,cuenta,tercero,centrocosto,detalle,cheque,valdebito, valcredito,estado,vigencia) VALUES ('$codigoComprobante $codRecaudo', '$cuentaDebito', '$documento', '$row[1]', 'Pago completo del cargo fijo de $row[2] de la factura $numeroFactura', '', 0, $valorCF, '1', '$año')";
											mysqli_query($linkbd,$sqlr);
										}

										if ($consumo > 0) {

											$valorCS = $consumo - $subsidioCS;
						
											$sql = "SELECT consumo_u, cc, nombre FROM srvservicios WHERE id = $rowServicios[id] ";
											$res = mysqli_query($linkbd,$sql);
											$row = mysqli_fetch_row($res);
						
											$concepto = concepto_cuentasn2($row[0],'CL',10,$row[1],$fecha);
						
											for ($i=0; $i < count($concepto); $i++) 
											{ 
												if($concepto[$i][2] == 'S')
												{
													$cuentaDebito = $concepto[$i][0];
												}
											}
						
											$sqlr="INSERT INTO comprobante_det (id_comp,cuenta,tercero,centrocosto,detalle,cheque,valdebito, valcredito,estado,vigencia) VALUES ('$codigoComprobante $codRecaudo', '$numeroCuenta', '$documento', '$row[1]', 'Pago completo del consumo de $row[2] de la factura $numeroFactura', '', $valorCS, 0, '1', '$año')";
											mysqli_query($linkbd,$sqlr);
						
											$sqlr="INSERT INTO comprobante_det (id_comp,cuenta,tercero,centrocosto,detalle,cheque,valdebito, valcredito,estado,vigencia) VALUES ('$codigoComprobante $codRecaudo', '$cuentaDebito', '$documento', '$row[1]', 'Pago completo del consumo de $row[2] de la factura $numeroFactura', '', 0, $valorCS, '1', '$año')";
											mysqli_query($linkbd,$sqlr);
										}

										if ($contribucion > 0) {

											$sqlServicio = "SELECT contribucion_u, cc, nombre FROM srvservicios WHERE id = $rowServicios[id] ";
											$resServicio = mysqli_query($linkbd,$sqlServicio);
											$rowServicio = mysqli_fetch_row($resServicio);
						
											$concepto = concepto_cuentasn2($rowServicio[0],'SC',10,$rowServicio[1],$fecha);
						
											for ($i=0; $i < count($concepto); $i++) 
											{ 
												if($concepto[$i][2] == 'S')
												{
													$cuentaDebito = $concepto[$i][0];
												}
											}
						
											$sqlr="INSERT INTO comprobante_det (id_comp,cuenta,tercero,centrocosto,detalle,cheque,valdebito, valcredito,estado,vigencia) VALUES ('$codigoComprobante $codRecaudo', '$numeroCuenta', '$documento', '$row[1]', 'Pago completo de la contribucion de $row[2] de la factura $numeroFactura', '', $contribucion, 0, '1', '$año')";
											mysqli_query($linkbd,$sqlr);
						
											$sqlr="INSERT INTO comprobante_det (id_comp,cuenta,tercero,centrocosto,detalle,cheque,valdebito, valcredito,estado,vigencia) VALUES ('$codigoComprobante $codRecaudo', '$cuentaDebito', '$documento', '$row[1]', 'Pago completo de la contribucion de $row[2] de la factura $numeroFactura', '', 0, $contribucion, '1', '$año')";
											mysqli_query($linkbd,$sqlr);
										}

										if ($acuerdoPago > 0) {

											$acuerdoPago = concepto_cuentasn2('01','AP',10,'02',$fecha);
						
											for ($i=0; $i < count($acuerdoPago); $i++) 
											{ 
												if($acuerdoPago[$i][2] == 'S')
												{
													$cuentaAcuerdoPago = $acuerdoPago[$i][0];
												}
											}
						
											$sqlr="INSERT INTO comprobante_det (id_comp,cuenta,tercero,centrocosto,detalle,cheque,valdebito, valcredito,estado,vigencia) VALUES ('$codigoComprobante $codRecaudo', '$numeroCuenta', '$documento', '$row[1]', 'Pago completo del acuerdo de pago de $row[2] de la factura $numeroFactura', '', $acuerdoPago, 0, '1', '$año')";
											mysqli_query($linkbd,$sqlr);
						
											$sqlr="INSERT INTO comprobante_det (id_comp,cuenta,tercero,centrocosto,detalle,cheque,valdebito, valcredito,estado,vigencia) VALUES ('$codigoComprobante $codRecaudo', '$cuentaDebito', '$documento', '$row[1]', 'Pago completo del acuerdo de pago de $row[2] de la factura $numeroFactura', '', 0, $acuerdoPago, '1', '$año')";
											mysqli_query($linkbd,$sqlr);
										}
									}
									else {

										$sqlBuscaPagos = "SELECT numero_facturacion FROM srvcortes_detalle WHERE id_cliente = $idCliente AND id_corte < $corte AND (estado_pago = 'P' OR estado_pago = 'A') ORDER BY id_corte DESC LIMIT 1";
										$rowBuscaPagos = mysqli_fetch_assoc(mysqli_query($linkbd, $sqlBuscaPagos));

										if(isset($rowBuscaPagos['numero_facturacion']) != '') {

											$cargoFijo = 0;
											$consumo = 0;
											$subsidioCF = 0;
											$subsidioCS = 0;
											$contribucion = 0;
											$acuerdoPago = 0;
											$valorCF = 0;
											$valorCS = 0;
											$saldoInicial = 0;

											$sqlDetallesFactura = "SELECT id_tipo_cobro, credito, debito FROM srvdetalles_facturacion WHERE numero_facturacion = $numeroFactura AND tipo_movimiento = '101' AND id_servicio = $rowServicios[id] AND (credito > 0 OR debito > 0) ORDER BY id_tipo_cobro ASC";
											$resDetallesFactura = mysqli_query($linkbd, $sqlDetallesFactura);
											while ($rowDetallesFactura = mysqli_fetch_assoc($resDetallesFactura)) {

												switch ($rowDetallesFactura['id_tipo_cobro']) {
													case 1:
														$cargoFijo += $rowDetallesFactura['credito'];
														break;

													case 2:
														$consumo += $rowDetallesFactura['credito'];
														break;

													case 3:
														$subsidioCF += $rowDetallesFactura['debito'];
														break;

													case 4:
														$subsidioCS += $rowDetallesFactura['debito'];
														break;

													case 5:
														$contribucion += $rowDetallesFactura['credito'];
														break;

													case 7:
														$acuerdoPago += $rowDetallesFactura['credito'];
														break;

													case 10: 
														$saldoInicial += $rowDetallesFactura['credito'];
														break;
												}
											}

											$sqlFacturasVencidas = "SELECT numero_facturacion FROM srvcortes_detalle WHERE id_cliente = $idCliente AND numero_facturacion > $rowBuscaPagos[numero_facturacion] AND numero_facturacion < $numeroFactura AND estado_pago = 'V'";
											$resFacturasVencidas = mysqli_query($linkbd, $sqlFacturasVencidas);
											while ($rowFacturasVencidas = mysqli_fetch_assoc($resFacturasVencidas)) {
												
												$sqlDetallesFactura = "SELECT id_tipo_cobro, credito, debito FROM srvdetalles_facturacion WHERE numero_facturacion = $rowFacturasVencidas[numero_facturacion] AND id_servicio = $rowServicios[id] AND (credito > 0 OR debito > 0)";
												$resDetallesFactura = mysqli_query($linkbd, $sqlDetallesFactura);
												while ($rowDetallesFactura = mysqli_fetch_assoc($resDetallesFactura)) {
													
													switch ($rowDetallesFactura['id_tipo_cobro']) {
														case 1:
															$cargoFijo += $rowDetallesFactura['credito'];
															break;

														case 2:
															$consumo += $rowDetallesFactura['credito'];
															break;

														case 3:
															$subsidioCF += $rowDetallesFactura['debito'];
															break;

														case 4:
															$subsidioCS += $rowDetallesFactura['debito'];
															break;

														case 5:
															$contribucion += $rowDetallesFactura['credito'];
															break;

														case 7:
															$acuerdoPago += $rowDetallesFactura['credito'];
															break;   
														
														case 10:
															$saldoInicial += $rowDetallesFactura['credito'];
															break;
													}
												}
											}

											if ($cargoFijo > 0) {

												$valorCF = $cargoFijo - $subsidioCF;
						
												$sql = "SELECT cargo_fijo_u, cc, nombre FROM srvservicios WHERE id = $rowServicios[id] ";
												$res = mysqli_query($linkbd,$sql);
												$row = mysqli_fetch_row($res);
						
												$concepto = concepto_cuentasn2($row[0],'SS',10,$row[1],$fecha);
						
												for ($i=0; $i < count($concepto); $i++) 
												{ 
													if($concepto[$i][2] == 'S')
													{
														$cuentaDebito = $concepto[$i][0];
													}
												}
						
												$sqlr="INSERT INTO comprobante_det (id_comp,cuenta,tercero,centrocosto,detalle,cheque,valdebito, valcredito,estado,vigencia) VALUES ('$codigoComprobante $codRecaudo', '$numeroCuenta', '$documento', '$row[1]', 'Pago completo del cargo fijo de $row[2] de la factura $numeroFactura', '', $valorCF, 0, '1', '$año')";
												mysqli_query($linkbd,$sqlr);
						
												$sqlr="INSERT INTO comprobante_det (id_comp,cuenta,tercero,centrocosto,detalle,cheque,valdebito, valcredito,estado,vigencia) VALUES ('$codigoComprobante $codRecaudo', '$cuentaDebito', '$documento', '$row[1]', 'Pago completo del cargo fijo de $row[2] de la factura $numeroFactura', '', 0, $valorCF, '1', '$año')";
												mysqli_query($linkbd,$sqlr);
											}

											if ($consumo > 0) {

												$valorCS = $consumo - $subsidioCS;
						
												$sql = "SELECT consumo_u, cc, nombre FROM srvservicios WHERE id = $rowServicios[id] ";
												$res = mysqli_query($linkbd,$sql);
												$row = mysqli_fetch_row($res);
						
												$concepto = concepto_cuentasn2($row[0],'CL',10,$row[1],$fecha);
						
												for ($i=0; $i < count($concepto); $i++) 
												{ 
													if($concepto[$i][2] == 'S')
													{
														$cuentaDebito = $concepto[$i][0];
													}
												}
						
												$sqlr="INSERT INTO comprobante_det (id_comp,cuenta,tercero,centrocosto,detalle,cheque,valdebito, valcredito,estado,vigencia) VALUES ('$codigoComprobante $codRecaudo', '$numeroCuenta', '$documento', '$row[1]', 'Pago completo del consumo de $row[2] de la factura $numeroFactura', '', $valorCS, 0, '1', '$año')";
												mysqli_query($linkbd,$sqlr);
						
												$sqlr="INSERT INTO comprobante_det (id_comp,cuenta,tercero,centrocosto,detalle,cheque,valdebito, valcredito,estado,vigencia) VALUES ('$codigoComprobante $codRecaudo', '$cuentaDebito', '$documento', '$row[1]', 'Pago completo del consumo de $row[2] de la factura $numeroFactura', '', 0, $valorCS, '1', '$año')";
												mysqli_query($linkbd,$sqlr);
											}

											if ($contribucion > 0) {

												$sqlServicio = "SELECT contribucion_u, cc, nombre FROM srvservicios WHERE id = $rowServicios[id] ";
												$resServicio = mysqli_query($linkbd,$sqlServicio);
												$rowServicio = mysqli_fetch_row($resServicio);
						
												$concepto = concepto_cuentasn2($rowServicio[0],'SC',10,$rowServicio[1],$fecha);
						
												for ($i=0; $i < count($concepto); $i++) 
												{ 
													if($concepto[$i][2] == 'S')
													{
														$cuentaDebito = $concepto[$i][0];
													}
												}
						
												$sqlr="INSERT INTO comprobante_det (id_comp,cuenta,tercero,centrocosto,detalle,cheque,valdebito, valcredito,estado,vigencia) VALUES ('$codigoComprobante $codRecaudo', '$numeroCuenta', '$documento', '$row[1]', 'Pago completo de la contribucion de $row[2] de la factura $numeroFactura', '', $contribucion, 0, '1', '$año')";
												mysqli_query($linkbd,$sqlr);
						
												$sqlr="INSERT INTO comprobante_det (id_comp,cuenta,tercero,centrocosto,detalle,cheque,valdebito, valcredito,estado,vigencia) VALUES ('$codigoComprobante $codRecaudo', '$cuentaDebito', '$documento', '$row[1]', 'Pago completo de la contribucion de $row[2] de la factura $numeroFactura', '', 0, $contribucion, '1', '$año')";
												mysqli_query($linkbd,$sqlr);
											}

											if ($acuerdoPago > 0) {

												$acuerdoPago = concepto_cuentasn2('01','AP',10,'01',$fecha);
						
												for ($i=0; $i < count($acuerdoPago); $i++) 
												{ 
													if($acuerdoPago[$i][2] == 'S')
													{
														$cuentaAcuerdoPago = $acuerdoPago[$i][0];
													}
												}
						
												$sqlr="INSERT INTO comprobante_det (id_comp,cuenta,tercero,centrocosto,detalle,cheque,valdebito, valcredito,estado,vigencia) VALUES ('$codigoComprobante $codRecaudo', '$numeroCuenta', '$documento', '$row[1]', 'Pago completo del acuerdo de pago de $row[2] de la factura $documento', '', $acuerdoPago, 0, '1', '$año')";
												mysqli_query($linkbd,$sqlr);
						
												$sqlr="INSERT INTO comprobante_det (id_comp,cuenta,tercero,centrocosto,detalle,cheque,valdebito, valcredito,estado,vigencia) VALUES ('$codigoComprobante $codRecaudo', '$cuentaDebito', '$documento', '$row[1]', 'Pago completo del acuerdo de pago de $row[2] de la factura $documento', '', 0, $acuerdoPago, '1', '$año')";
												mysqli_query($linkbd,$sqlr);
											}

											if($saldoInicial > 0)
											{
												$sql = "SELECT cargo_fijo_u, cc, nombre FROM srvservicios WHERE id = $rowServicios[id] ";
												$res = mysqli_query($linkbd,$sql);
												$row = mysqli_fetch_row($res);

												$concepto = concepto_cuentasn2($row[0],'SS',10,$row[1],$rowRecaudoFactura['fecha_recaudo']);

												for ($i=0; $i < count($concepto); $i++) 
												{ 
													if($concepto[$i][2] == 'S')
													{
														$cuentaDebito = $concepto[$i][0];
													}
												}

												$sqlr="INSERT INTO comprobante_det (id_comp,cuenta,tercero,centrocosto,detalle,cheque,valdebito, valcredito,estado,vigencia) VALUES ('$codigoComprobante $codRecaudo', '$numeroCuenta', '$documento', '$row[1]', 'Pago completo saldo inicial de $row[2] de la factura $numeroFactura', '', $saldoInicial, 0, '1', '$año')";
												mysqli_query($linkbd,$sqlr);

												$sqlr="INSERT INTO comprobante_det (id_comp,cuenta,tercero,centrocosto,detalle,cheque,valdebito, valcredito,estado,vigencia) VALUES ('$codigoComprobante $codRecaudo', '$cuentaDebito', '$documento', '$row[1]', 'Pago completo saldo inicial de $row[2] de la factura $numeroFactura', '', 0, $saldoInicial, '1', '$año')";
												mysqli_query($linkbd,$sqlr);
											}
										}	
										else {

											$cargoFijo = 0;
											$consumo = 0;
											$subsidioCF = 0;
											$subsidioCS = 0;
											$contribucion = 0;
											$acuerdoPago = 0;
											$valorCF = 0;
											$valorCS = 0;
											$saldoInicial = 0;

											$sqlDetallesFactura = "SELECT id_tipo_cobro, credito, debito FROM srvdetalles_facturacion WHERE numero_facturacion = $numeroFactura AND tipo_movimiento = '101' AND id_servicio = $rowServicios[id] AND (credito > 0 OR debito > 0) ORDER BY id_tipo_cobro ASC";
											$resDetallesFactura = mysqli_query($linkbd, $sqlDetallesFactura);
											while ($rowDetallesFactura = mysqli_fetch_assoc($resDetallesFactura)) {

												switch ($rowDetallesFactura['id_tipo_cobro']) {
													case 1:
														$cargoFijo += $rowDetallesFactura['credito'];
														break;

													case 2:
														$consumo += $rowDetallesFactura['credito'];
														break;

													case 3:
														$subsidioCF += $rowDetallesFactura['debito'];
														break;

													case 4:
														$subsidioCS += $rowDetallesFactura['debito'];
														break;

													case 5:
														$contribucion += $rowDetallesFactura['credito'];
														break;

													case 7:
														$acuerdoPago += $rowDetallesFactura['credito'];
														break;
													case 10:
														$saldoInicial += $rowDedudaAnterior['credito'];
														break;
												}
											}

											$sqlFacturasVencidas = "SELECT numero_facturacion FROM srvcortes_detalle WHERE id_cliente = $idCliente AND numero_facturacion < $numeroFactura AND estado_pago = 'V'";
											$resFacturasVencidas = mysqli_query($linkbd, $sqlFacturasVencidas);
											while ($rowFacturasVencidas = mysqli_fetch_assoc($resFacturasVencidas)) {

												$sqlDetallesFactura = "SELECT id_tipo_cobro, credito, debito FROM srvdetalles_facturacion WHERE numero_facturacion = $rowFacturasVencidas[numero_facturacion] AND id_servicio = $rowServicios[id] AND (credito > 0 OR debito > 0)";
												$resDetallesFactura = mysqli_query($linkbd, $sqlDetallesFactura);
												while ($rowDetallesFactura = mysqli_fetch_assoc($resDetallesFactura)) {
													
													switch ($rowDetallesFactura['id_tipo_cobro']) {
														case 1:
															$cargoFijo += $rowDetallesFactura['credito'];
															break;

														case 2:
															$consumo += $rowDetallesFactura['credito'];
															break;

														case 3:
															$subsidioCF += $rowDetallesFactura['debito'];
															break;

														case 4:
															$subsidioCS += $rowDetallesFactura['debito'];
															break;

														case 5:
															$contribucion += $rowDetallesFactura['credito'];
															break;

														case 7:
															$acuerdoPago += $rowDetallesFactura['credito'];
															break; 
														
														case 10:
															$saldoInicial += $rowDetallesFactura['credito'];
															break;
													}
												}
											}

											if ($cargoFijo > 0) {

												$valorCF = $cargoFijo - $subsidioCF;
						
												$sql = "SELECT cargo_fijo_u, cc, nombre FROM srvservicios WHERE id = $rowServicios[id] ";
												$res = mysqli_query($linkbd,$sql);
												$row = mysqli_fetch_row($res);
						
												$concepto = concepto_cuentasn2($row[0],'SS',10,$row[1],$fecha);
						
												for ($i=0; $i < count($concepto); $i++) 
												{ 
													if($concepto[$i][2] == 'S')
													{
														$cuentaDebito = $concepto[$i][0];
													}
												}
						
												$sqlr="INSERT INTO comprobante_det (id_comp,cuenta,tercero,centrocosto,detalle,cheque,valdebito, valcredito,estado,vigencia) VALUES ('$codigoComprobante $codRecaudo', '$numeroCuenta', '$documento', '$row[1]', 'Pago completo del cargo fijo de $row[2] de la factura $numeroFactura', '', $valorCF, 0, '1', '$año')";
												mysqli_query($linkbd,$sqlr);
						
												$sqlr="INSERT INTO comprobante_det (id_comp,cuenta,tercero,centrocosto,detalle,cheque,valdebito, valcredito,estado,vigencia) VALUES ('$codigoComprobante $codRecaudo', '$cuentaDebito', '$documento', '$row[1]', 'Pago completo del cargo fijo de $row[2] de la factura $numeroFactura', '', 0, $valorCF, '1', '$año')";
												mysqli_query($linkbd,$sqlr);
											}
						
											if ($consumo > 0) {
						
												$valorCS = $consumo - $subsidioCS;
						
												$sql = "SELECT consumo_u, cc, nombre FROM srvservicios WHERE id = $rowServicios[id] ";
												$res = mysqli_query($linkbd,$sql);
												$row = mysqli_fetch_row($res);
						
												$concepto = concepto_cuentasn2($row[0],'CL',10,$row[1],$fecha);
						
												for ($i=0; $i < count($concepto); $i++) 
												{ 
													if($concepto[$i][2] == 'S')
													{
														$cuentaDebito = $concepto[$i][0];
													}
												}
						
												$sqlr="INSERT INTO comprobante_det (id_comp,cuenta,tercero,centrocosto,detalle,cheque,valdebito, valcredito,estado,vigencia) VALUES ('$codigoComprobante $codRecaudo', '$numeroCuenta', '$documento', '$row[1]', 'Pago completo del consumo de $row[2] de la factura $numeroFactura', '', $valorCS, 0, '1', '$año')";
												mysqli_query($linkbd,$sqlr);
						
												$sqlr="INSERT INTO comprobante_det (id_comp,cuenta,tercero,centrocosto,detalle,cheque,valdebito, valcredito,estado,vigencia) VALUES ('$codigoComprobante $codRecaudo', '$cuentaDebito', '$documento', '$row[1]', 'Pago completo del consumo de $row[2] de la factura $numeroFactura', '', 0, $valorCS, '1', '$año')";
												mysqli_query($linkbd,$sqlr);
											}
						
											if ($contribucion > 0) {
						
												$sqlServicio = "SELECT contribucion_u, cc, nombre FROM srvservicios WHERE id = $rowServicios[id] ";
												$resServicio = mysqli_query($linkbd,$sqlServicio);
												$rowServicio = mysqli_fetch_row($resServicio);
						
												$concepto = concepto_cuentasn2($rowServicio[0],'SC',10,$rowServicio[1],$fecha);
						
												for ($i=0; $i < count($concepto); $i++) 
												{ 
													if($concepto[$i][2] == 'S')
													{
														$cuentaDebito = $concepto[$i][0];
													}
												}
						
												$sqlr="INSERT INTO comprobante_det (id_comp,cuenta,tercero,centrocosto,detalle,cheque,valdebito, valcredito,estado,vigencia) VALUES ('$codigoComprobante $codRecaudo', '$numeroCuenta', '$documento', '$row[1]', 'Pago completo de la contribucion de $row[2] de la factura $numeroFactura', '', $contribucion, 0, '1', '$año')";
												mysqli_query($linkbd,$sqlr);
						
												$sqlr="INSERT INTO comprobante_det (id_comp,cuenta,tercero,centrocosto,detalle,cheque,valdebito, valcredito,estado,vigencia) VALUES ('$codigoComprobante $codRecaudo', '$cuentaDebito', '$documento', '$row[1]', 'Pago completo de la contribucion de $row[2] de la factura $numeroFactura', '', 0, $contribucion, '1', '$año')";
												mysqli_query($linkbd,$sqlr);
											}
						
											if ($acuerdoPago > 0) {
						
												$acuerdoPago = concepto_cuentasn2('01','AP',10,'01',$fecha);
						
												for ($i=0; $i < count($acuerdoPago); $i++) 
												{ 
													if($acuerdoPago[$i][2] == 'S')
													{
														$cuentaAcuerdoPago = $acuerdoPago[$i][0];
													}
												}
						
												$sqlr="INSERT INTO comprobante_det (id_comp,cuenta,tercero,centrocosto,detalle,cheque,valdebito, valcredito,estado,vigencia) VALUES ('$codigoComprobante $codRecaudo', '$numeroCuenta', '$documento', '$row[1]', 'Pago completo del acuerdo de pago de $row[2] de la factura $numeroFactura', '', $acuerdoPago, 0, '1', '$año')";
												mysqli_query($linkbd,$sqlr);
						
												$sqlr="INSERT INTO comprobante_det (id_comp,cuenta,tercero,centrocosto,detalle,cheque,valdebito, valcredito,estado,vigencia) VALUES ('$codigoComprobante $codRecaudo', '$cuentaDebito', '$documento', '$row[1]', 'Pago completo del acuerdo de pago de $row[2] de la factura $numeroFactura', '', 0, $acuerdoPago, '1', '$año')";
												mysqli_query($linkbd,$sqlr);
											}
						
											if ($saldoInicial > 0) {
						
												$sql = "SELECT cargo_fijo_u, cc, nombre FROM srvservicios WHERE id = $rowServicios[id] ";
												$res = mysqli_query($linkbd,$sql);
												$row = mysqli_fetch_row($res);
						
												$concepto = concepto_cuentasn2($row[0],'SS',10,$row[1],$fecha);
						
												for ($i=0; $i < count($concepto); $i++) 
												{ 
													if($concepto[$i][2] == 'S')
													{
														$cuentaDebito = $concepto[$i][0];
													}
												}
						
												$sqlr="INSERT INTO comprobante_det (id_comp,cuenta,tercero,centrocosto,detalle,cheque,valdebito, valcredito,estado,vigencia) VALUES ('$codigoComprobante $codRecaudo', '$numeroCuenta', '$documento', '$row[1]', 'Pago completo del saldo inicial de $row[2] de la factura $numeroFactura', '', $saldoInicial, 0, '1', '$año')";
												mysqli_query($linkbd,$sqlr);
						
												$sqlr="INSERT INTO comprobante_det (id_comp,cuenta,tercero,centrocosto,detalle,cheque,valdebito, valcredito,estado,vigencia) VALUES ('$codigoComprobante $codRecaudo', '$cuentaDebito', '$documento', '$row[1]', 'Pago completo del saldo inicial de $row[2] de la factura $numeroFactura', '', 0, $saldoInicial, '1', '$año')";
												mysqli_query($linkbd,$sqlr);
											}
										}
									}
								}

								$sqlEstadoFactura = "UPDATE srvcortes_detalle SET estado_pago = 'P' WHERE numero_facturacion = $numeroFactura";
								if(mysqli_query($linkbd, $sqlEstadoFactura));
								{
									echo "
										<script>
											Swal.fire({
												icon: 'success',
												title: 'Guardado exitoso!',
												}).then((result) => {
												if (result.value) {
													document.location.href = 'serv-crear-recaudo.php';
												} 
											})
										</script>";
								}
							}
						}
						else
						{
							echo "
									<script>
										Swal.fire({
											icon: 'error',
											title: 'Error en guardado paso 2'
										})
									</script>
								";
						}

					}
					else
					{
						echo "
							<script>
								Swal.fire({
									icon: 'error',
									title: 'Error en guardado paso 1'
								})
							</script>
						";
					}
				}
			?>
		</form>
		<div id="bgventanamodal2">
			<div id="ventanamodal2">
				<IFRAME  src="" name="buster" marginWidth=0 marginHeight=0 frameBorder=0 id="ventana2" frameSpacing=0 style="left:500px; width:900px; height:500px; top:200;"></IFRAME>
			</div>
		</div>
	</body>
</html>