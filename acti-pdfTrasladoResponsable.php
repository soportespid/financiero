<?php
//V 1000 12/12/16
	require_once("tcpdf/tcpdf_include.php");
	require('comun.inc');
	require "funciones.inc";
	session_start();
	$linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");
	date_default_timezone_set("America/New_York");

    $sqlr="SELECT funcionario, nomcargo FROM firmaspdf_det WHERE idfirmas='6' AND estado ='S'";
	$res=mysqli_query($linkbd,$sqlr);
	$rowCargo=mysqli_fetch_row($res);

    $_POST['ppto'][] = $rowCargo[0];
    $_POST['nomcargo'][] = $rowCargo[1];

	class MYPDF extends TCPDF
	{
		public function Header()
		{
			$linkbd = conectar_v7();
			$linkbd -> set_charset("utf8");
			$sqlr="SELECT nit, razonsocial FROM configbasica WHERE estado='S'";
			$resp=mysqli_query($linkbd,$sqlr);
			while($row=mysqli_fetch_row($resp)){$nit=$row[0];$rs=$row[1];}

            //-------------- RECUADRO DEL ENCABEZADO  --------------------------------
			$this->Image('imagenes/escudo.jpg', 15, 14, 25, 23.9, 'JPG', '', 'T', true, 300, '', false, false, 0, false, false, false);// Logo
			$this->SetFont('helvetica','B',8);
			$this->SetY(10);
			$this->RoundedRect(10, 10, 280, 31, 2.5,''); //Borde del encabezado - Recuadro del encabezado
			$this->Cell(35,31,'','R',0,'L'); //Linea que separa el encabazado verticalmente
			$this->SetY(10);
			$this->SetFont('helvetica','B',12);

			if(strlen($rs)<40)
			{
				$this->SetX(40);
				$this->Cell(230,15,$rs,0,0,'C');  //Posicion Municipio
				$this->SetY(16);
			}
			else
			{
				$this->Cell(61);
				$this->MultiCell(200,15,$rs,0,'C',false,1,'','',true,4,false,true,19,'T',false);
				$this->SetY(18);
			}

            //--------------------- CONTENIDO DENTRO DEL RECUADRO DEL ENCABEZADO -----------------------------------------

			$this->SetX(40);  //Posicion Numero Nit
			$this->SetFont('helvetica','B',8);
			$this->Cell(230,12,"NIT: $nit",0,0,'C'); // Posicion Numero Nit
			$this->SetY(27);                    // Posicion Certificado
			$this->SetX(45);                    // Posicion Certificado
			$this->SetFont('helvetica','B',11);
			$this->Cell(192,14,"TRASLADO DE RESPONSABLE DE ACTIVO",1,0,'C');        // Recuadro del certificado
			$this->SetFont('helvetica','I',10);
			$this->SetY(27);
			$this->SetX(62);
			$this->SetFont('helvetica','B',10);
			$this->Cell(228,7,"",'T',0,'C',false,0,1);
			$this->SetFont('helvetica','B',9);
			$this->SetY(27);
			$this->SetX(243);
            preg_match("/([0-9]{2})\/([0-9]{2})\/([0-9]{4})/", $_POST['fechaRegistro'],$f);
            $vigencia = "$f[3]";              // Posicion Fecha
			$this->Cell(35,6," FECHA: ".$_POST['fechaRegistro'],0,0,'L');
			$this->SetY(33);
			$this->SetX(243);                                      //  Posicion Vigencia
			$this->Cell(35,5," VIGENCIA: ".$vigencia,0,0,'L');
		}

		public function Footer()
		{
			$linkbd = conectar_v7();
			$linkbd -> set_charset("utf8");
			$sqlr="SELECT direccion,telefono,web,email FROM configbasica WHERE estado='S'";
			$resp=mysqli_query($linkbd,$sqlr);
			$user = $_SESSION['nickusu'];
			$fecha = date("Y-m-d H:i:s");
			$ip = $_SERVER['REMOTE_ADDR'];
			$useri = $_POST['user'];
			while($row=mysqli_fetch_row($resp))
			{
				$direcc=strtoupper($row[0]);
				$telefonos=$row[1];
				$dirweb=strtoupper($row[3]);
				$coemail=$row[2];
			}
			if($direcc!=''){$vardirec="Dirección: $direcc, ";}
			else {$vardirec="";}
			if($telefonos!=''){$vartelef="Telefonos: $telefonos";}
			else{$vartelef="";}
			if($dirweb!=''){$varemail="Email: $dirweb, ";}
			else {$varemail="";}
			if($coemail!=''){$varpagiw="Pagina Web: $coemail";}
			else{$varpagiw="";}
			//$this->SetY(-16);
			$this->SetFont('helvetica', 'I', 8);
			$txt = <<<EOD
			$vardirec $vartelef
			$varemail $varpagiw
			EOD;
			$this->Cell(277,10,'','T',0,'T');
			$this->ln(2);
			$this->Write(0, $txt , '', 0, 'C', true, 0, false, false, 0);
			$this->Cell(25, 10, 'Hecho por: '.$useri, 0, false, 'C', 0, '', 0, false, 'T', 'M');
			$this->Cell(100, 10, 'Impreso por: '.$user, 0, false, 'C', 0, '', 0, false, 'T', 'M');
			$this->Cell(30, 10, 'IP: '.$ip, 0, false, 'C', 0, '', 0, false, 'T', 'M');
			$this->Cell(102, 10, 'Fecha: '.$fecha, 0, false, 'C', 0, '', 0, false, 'T', 'M');
			$this->Cell(5, 10, 'IDEAL.10 S.A.S    Pagina '.$this->getAliasNumPage().'/'.$this->getAliasNbPages(), 0, false, 'C', 0, '', 0, false, 'T', 'M');

		}
	}

    //----------------------------------------------------------------------

	$pdf = new MYPDF('L','mm','Letter', true, 'iso-8859-1', false);// create new PDF document // Orientacion del pdf
	$pdf->SetDocInfoUnicode (true);
	// set document information
	$pdf->SetCreator(PDF_CREATOR);
	$pdf->SetAuthor('Ideal10sas');
	$pdf->SetTitle('Traslado Responsable');
	$pdf->SetSubject('Traslado Responsable');
	$pdf->SetKeywords('TCPDF, PDF, example, test, guide');
	$pdf->SetMargins(10, 60, 10);// set margins // Posicion de los valores de cada columna
	$pdf->SetHeaderMargin(90);// set margins
	$pdf->SetFooterMargin(20);// set margins
	$pdf->SetAutoPageBreak(TRUE, 20);// set auto page breaks
	// set some language-dependent strings (optional)
	if (@file_exists(dirname(__FILE__).'/lang/spa.php'))
	{
		require_once(dirname(__FILE__).'/lang/spa.php');
		$pdf->setLanguageArray($l);
	}

	// ---------------------------------------------------------
	$pdf->AddPage();
	$pdf->SetFont('helvetica','I',9);

    $infoinicio=strtoupper("Se realiaza el traslado de responsable del numero de activo $_POST[numeroActivo] - $_POST[nomActivo]");

	$pdf->SetFont('times','B',12);
	$pdf->SetY(44);
	$pdf->MultiCell(280,5,'EL SUSCRITO '.$_POST['nomcargo'][0],'','C');
	$pdf->Cell(280,12,'CERTIFICA:',0,0,'C');
	$pdf->SetY(60);
	$pdf->SetFont('times','',10);
	$pdf->cell(0.1);
	$pdf->SetY(65);
	$pdf->MultiCell(280,6,$infoinicio,0,'L',false,1,'','',true,0,false,true,0,'T',false);
	$pdf->ln(4);
	$pdf->SetFont('times','',10);
    $pdf->Cell(20,4,'El MOTIVO:',0,0,'L');
	$pdf->MultiCell(280,6,strtoupper($_POST['motivo']),0,'L',false,1,'','',true,0,false,true,0,'T',false);
	$pdf->SetFont('times','',10);
	$pdf->Cell(35,10,$_POST['solicita'],0,1,'C',false,0,0,false,'T','C');
	$pdf->SetFont('times','B',10);
	$pdf->SetFillColor(200, 200, 200);
	$pdf->SetFont('helvetica','B',6);
	$posy=$pdf->GetY();
	$pdf->SetY($posy+1);



    //-------------------  Nombre/ Cargo / Valor total ----------------
	$pdf->ln(15);
	$v=$pdf->gety();
	if($v>=180){
		$pdf->AddPage();
		$v=$pdf->gety();
	}

    $pdf->setFont('times','B',8);

    $pdf->Line(37,$v,137,$v);
	$pdf->Line(142,$v,232,$v);

    $pdf->Cell(144,4,''.$_POST['actualResponsable'],0,1,'C',false,0,0,false,'T','C');
    $pdf->Cell(144,4,''.$_POST['actualNomResponsable'],0,1,'C',false,0,0,false,'T','C');
    $pdf->SetY($v);
    $pdf->Cell(350,4,''.$_POST['responsable'],0,1,'C',false,0,0,false,'T','C');
    $pdf->Cell(350,4,''.$_POST['nomResponsable'],0,1,'C',false,0,0,false,'T','C');

	for($x=0;$x<count($_POST['ppto']);$x++)
	{
		$pdf->ln(20);
		$v=$pdf->gety();
		if($v>=180){
			$pdf->AddPage();
			$pdf->ln(20);
			$v=$pdf->gety();
		}
		$pdf->setFont('times','B',8);
		if (($x%2)==0) {
			if(isset($_POST['ppto'][$x+1])){
				$pdf->Line(17,$v,107,$v);
				$pdf->Line(112,$v,202,$v);
				$v2=$pdf->gety();
				$pdf->Cell(104,4,''.$_POST['ppto'][$x],0,1,'C',false,0,0,false,'T','C');
				$pdf->Cell(200,4,''.$_POST['nomcargo'][$x],0,1,'C',false,0,0,false,'T','C');
				$pdf->SetY($v2);
				$pdf->Cell(295,4,''.$_POST['ppto'][$x+1],0,1,'C',false,0,0,false,'T','C');
				$pdf->Cell(200,4,''.$_POST['nomcargo'][$x+1],0,1,'C',false,0,0,false,'T','C');
			}else{
				$pdf->Line(100,$v,200,$v);
				$pdf->Cell(280,4,''.$_POST['ppto'][$x],0,1,'C',false,0,0,false,'T','C');
				$pdf->Cell(280,4,''.$_POST['nomcargo'][$x],0,0,'C',false,0,0,false,'T','C');
			}
			$v3=$pdf->gety();
		}
		$pdf->SetY($v3);
		$pdf->SetFont('helvetica','',7);
	}
	// ---------------------------------------------------------
	$pdf->Output('reportecdp.pdf', 'I');//Close and output PDF document
?>
