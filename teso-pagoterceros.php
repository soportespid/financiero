<?php
	require "comun.inc";
	require "funciones.inc";
	require "conversor.php";
	session_start();
	$linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");
?>
<html lang="es">
	<head>
		<meta charset="UTF-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
    	<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<title>:: IDEAL 10 - Tesorer&iacute;a</title>
		<link href="favicon.ico" rel="shortcut icon"/>
        <link href="css/css2.css" rel="stylesheet" type="text/css" />
		<link href="css/css3.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="css/programas.js"></script>
        <script type="text/javascript" src="css/calendario.js"></script>
		<script>
			function validar(){document.form2.submit();}
			function agregardetalled()
			{
				if(document.form2.retencion.value!="" )
				{
					var tipoRetencion = document.form2.tipoRetencion.value;
					if(tipoRetencion == 'C')
					{
						var tipoRete = document.form2.tiporet.value;
						if(tipoRete != '')
						{
							document.form2.agregadetdes.value=1;
							//document.form2.chacuerdo.value=2;
							document.form2.submit();
						}
						else {despliegamodalm('visible','2','Falta Seleccionar el tipo de retencion');}
					}
					else
					{
						document.form2.agregadetdes.value=1;
						//document.form2.chacuerdo.value=2;
						document.form2.submit();
					}
				}
				else {despliegamodalm('visible','2','Falta informacion para poder Agregar');}
			}
			function eliminard(variable)
			{
				document.form2.eliminad.value=variable;
				document.form2.submit();
			}
			function guardar()
			{
				var tipo_pago = document.form2.tipop.value;
				if(tipo_pago == 'transferencia'){
					if (document.form2.fecha.value!='' && document.form2.tercero.value!='' && document.form2.nbanco.value!='' && document.form2.ntransfe.value != '')
					{
						despliegamodalm('visible','4','Esta Seguro de Guardar','2');
					}
					else
					{
						despliegamodalm('visible','2','Faltan datos para completar el registro');
						document.form2.fecha.focus();
						document.form2.fecha.select();
					}
				}else{
					if (document.form2.fecha.value!='' && document.form2.tercero.value!='')
					{
						despliegamodalm('visible','4','Esta Seguro de Guardar','2');
					}
					else
					{
						despliegamodalm('visible','2','Faltan datos para completar el registro');
						document.form2.fecha.focus();
						document.form2.fecha.select();
					}
				}
			}
			function calcularpago()
 			{
				valorp=document.form2.valor.value;
				descuentos=document.form2.totaldes.value;
				valorc=valorp-descuentos;
				document.form2.valorcheque.value=valorc;
				document.form2.valoregreso.value=valorp;
				document.form2.valorretencion.value=descuentos;
 			}
			function pdf()
			{
				document.form2.action="pdfpagoterceros.php";
				document.form2.target="_BLANK";
				document.form2.submit();
				document.form2.action="";
				document.form2.target="";
			}
			function buscater(e)
 			{
				if (document.form2.tercero.value!="")
				{
 					document.form2.bt.value='1';
 					document.form2.submit();
 				}
 			}
			function despliegamodal2(_valor,scr)
			{
				//alert("Hola"+scr);
				if(scr=="1"){
					var url="cuentasbancarias-ventana02.php?tipoc=D&obj01=banco&obj02=nbanco&obj03=&obj04=cb&obj05=ter";
				}
				if(scr=="2"){
					var url="cuentasbancarias-ventana02.php?tipoc=C&obj01=banco&obj02=nbanco&obj03=&obj04=cb&obj05=ter";
				}
				if(scr=="3"){
					var url="tercerosgral-ventana01.php?objeto=tercero&nobjeto=ntercero&nfoco=cc";
				}
				document.getElementById("bgventanamodal2").style.visibility=_valor;
				if(_valor=="hidden")
				{
					document.getElementById('ventana2').src="";
				}
				else
				{
					document.getElementById('ventana2').src=url;
				}
			}
			function despliegamodalm(_valor,_tip,mensa,pregunta)
			{
				document.getElementById("bgventanamodalm").style.visibility=_valor;
				if(_valor=="hidden")
				{
					document.getElementById('ventanam').src="";
					switch(document.getElementById('valfocus').value)
					{
						case "1":	document.getElementById('valfocus').value='';
									document.getElementById('tercero').focus();
									document.getElementById('tercero').select();
									break;
						case "2":	document.getElementById('valfocus').value='';
									document.getElementById('banco').value='';
									document.getElementById('banco').focus();
									document.getElementById('banco').select();
					}
				}
				else
				{
					switch(_tip)
					{
						case "1":
							document.getElementById('ventanam').src="ventana-mensaje1.php?titulos="+mensa;break;
						case "2":
							document.getElementById('ventanam').src="ventana-mensaje3.php?titulos="+mensa;break;
						case "3":
							document.getElementById('ventanam').src="ventana-mensaje2.php?titulos="+mensa;break;
						case "4":
							document.getElementById('ventanam').src="ventana-consulta1.php?titulos="+mensa+"&idresp="+pregunta;break;
					}
				}
			}
			function funcionmensaje()
			{
				var numdocar=document.getElementById('idcomp').value;
				document.location.href = "teso-editapagoterceros.php?idpago="+numdocar;
			}
			function respuestaconsulta(pregunta)
			{
				switch(pregunta)
				{
					case "1":	document.form2.oculto.value='3';
								document.form2.submit();
								break;
					case "2":	document.form2.oculto.value='2';
								document.form2.submit();
								break;
				}
			}
		</script>
	</head>
	<body>
		<IFRAME src="alertas.php" name="alertas" id="alertas" style="display:none"></IFRAME>
		<span id="todastablas2"></span>
		<table>
            <tr><script>barra_imagenes("teso");</script><?php cuadro_titulos();?></tr>
			<tr><?php menu_desplegable("teso");?></tr>
			<tr>
  				<td colspan="3" class="cinta">
					<a onClick="location.href='teso-pagoterceros.php'" class="mgbt"><img src="imagenes/add.png" title="Nuevo"/></a>
					<a onClick="guardar()" class="mgbt"><img src="imagenes/guarda.png" title="Guardar" /></a>
					<a class="mgbt" onClick="location.href='teso-buscapagoterceros.php'" ><img src="imagenes/busca.png" title="Buscar"/></a>
					<a onClick="mypop=window.open('plan-agenda.php','','');mypop.focus()" class="mgbt"><img src="imagenes/agenda1.png" title="Agenda"/></a>
					<a onClick="<?php echo paginasnuevas("teso");?>" class="mgbt"><img src="imagenes/nv.png" title="Nueva ventana"></a>
					<a onClick="mypop=window.open('<?php echo $url2; ?>','','');mypop.focus();" class="mgbt"><img src="imagenes/duplicar_pantalla.png" title="Duplicar pesta&ntilde;a"></a>
					<a class="mgbt1"><img src="imagenes/printd.png" style="width:29px;height:25px;"/></a>
				</td>
			</tr>
		</table>
        <div id="bgventanamodalm" class="bgventanamodalm">
            <div id="ventanamodalm" class="ventanamodalm">
                <IFRAME src="" name="ventanam" marginWidth=0 marginHeight=0 frameBorder=0 id="ventanam" frameSpacing=0 style=" width:700px; height:130px; top:200; overflow:hidden;">
                </IFRAME>
            </div>
        </div>
		<form name="form2" method="post" action="">
        	<input type="hidden" name="valfocus" id="valfocus" value=""/>
			<?php
				$vigusu=vigencia_usuarios($_SESSION['cedulausu']);
				//$_POST[vigencia]=$vigusu;

				$fec=date("d/m/Y");
				$fechaVig = explode("/", $fec);
  				$vact=$fechaVig[2];
				$vigencia=$fechaVig[2];
	 			$sqlr="select max(id_pago) from tesopagoterceros";
				$res=mysqli_query($linkbd,$sqlr);
				$consec=0;
				while($r=mysqli_fetch_row($res)){$consec=$r[0];}
	 			$consec+=1;
	 			$_POST['idcomp']=$consec;
	  			//*********** cuenta origen va al credito y la destino al debito
				if(!$_POST['oculto'])
				{
					$sqlr="select *from cuentapagar where estado='S' ";
					$res=mysqli_query($linkbd,$sqlr);
					while ($row =mysqli_fetch_row($res)){$_POST['cuentapagar']=$row[1];}
					$sqlr="select valor_inicial from dominios where nombre_dominio='CUENTA_MILES'";
					$res=mysqli_query($linkbd,$sqlr);
					while ($row =mysqli_fetch_row($res)){$_POST['cuentamiles']=$row[0];}
					$check1="checked";
 		 			$fec=date("d/m/Y");
		 			$_POST['fecha']=$fec;
					//$_POST[valor]=0;
					//$_POST[valorcheque]=0;
					//$_POST[valorretencion]=0;
					//$_POST[valoregreso]=0;
					//$_POST[totaldes]=0;
		 			$_POST['vigencia']=$_POST['vigencias'];
		 			$sqlr="select max(id_pago) from tesopagoterceros";
					$res=mysqli_query($linkbd,$sqlr);
					$consec=0;
					while($r=mysqli_fetch_row($res)){$consec=$r[0];}
	 				$consec+=1;
	 				$_POST['idcomp']=$consec;
				}
 				$meses=array('','Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre');
  				if($_POST['bt']=='1')
			 	{
			  		$nresul=buscatercero($_POST['tercero']);
			 		if($nresul!=''){$_POST['ntercero']=$nresul;}
			 		else {$_POST['ntercero']="";}
			 	}
 			?>
	   		<table class="inicio" style="width:99.6%;">
	   			<tr>
	     			<td colspan="2"  style="width:95%;"class="titulos">Pago Terceros - Otros Pagos</td>
                    <td class="cerrar" style="width:5%;"><a onClick="location.href='teso-principal.php'">&nbsp;Cerrar</a></td>
             	</tr>
       			<tr>
       				<td style="width:80%;">
       					<table>
       						<tr>
			                	<td class="saludo1" style="width:15%;">Numero Pago:</td>
			        			<td style="width:16%;">
			                    	<input type="hidden" name="cuentamiles" value="<?php echo $_POST['cuentamiles']?>" readonly>
			                        <input type="text" name="idcomp" id="idcomp" value="<?php echo $_POST['idcomp']?>" readonly>
			                  	</td>
				  				<td class="saludo1" style="width:2cm;">Fecha: </td>
			        			<td style="width:25%;">
								<input name="fecha" type="text" value="<?php echo $_POST['fecha']?>" maxlength="10"  onKeyPress="javascript:return solonumeros(event)" onKeyUp="return tabular(event,this)"  id="fc_1198971545" onKeyDown="mascara(this,'/',patron,true)" title="DD/MM/YYYY" style="width:60%;">&nbsp;<a onClick="displayCalendarFor('fc_1198971545');" title="Calendario"><img src="imagenes/calendario04.png" style="width:20px; cursor:pointer"></a></td>
				  				<td class="saludo1" style="width:10%;">Vigencia: </td>
			        			<td style="width:25%;"><input name="vigencia" type="text" style="width:20%;" value="<?php echo $_POST['vigencia']?>" maxlength="2"  onKeyPress="javascript:return solonumeros(event)" onKeyUp="return tabular(event,this)" readonly> </td>

			              	</tr>
			       			<tr>
			                	<td style="width:15%;" class="saludo1">Forma de Pago:</td>
			       				<td >
			       					<select name="tipop" onChange="validar();" style="width:100%;">
			       						<option value="">Seleccione ...</option>
							 			<option value="cheque" <?php if($_POST['tipop']=='cheque') echo "SELECTED"?>>Cheque</option>
			  				  			<option value="transferencia" <?php if($_POST['tipop']=='transferencia') echo "SELECTED"?>>Transferencia</option>
										<option value="caja" <?php if($_POST['tipop']=='caja') echo "SELECTED"?>>Caja</option>
							  		</select>
			          			</td>
			       				<td class="saludo1">Mes:</td>
			       				<td style="width:25%;">
			       					<select name="mes" onChange="validar()" style="width:55%;">
			       						<option value="">Seleccione ...</option>
			         					<?php
					   						for($x=1;$x<=12;$x++)
					    					{
					 							echo"<option value='$x'";
												if($_POST['mes']==$x) {echo " SELECTED>";}
												else {echo">";}
												echo "$meses[$x]</option>";
											}
					  					 ?>
			          				</select>
									<select name="vigencias" id="vigencias" onChange="validar()" style="width:40%;">
			      						<option value="">Sel..</option>
				  						<?php
											$sqlr="SELECT anio FROM admbloqueoanio WHERE bloqueado='N' ORDER BY anio DESC";
											$res = mysqli_query($linkbd,$sqlr);

											while($row=mysqli_fetch_row($res))
											{
												echo "<option  value=$row[0] ";
												if($row[0]==$_POST['vigencias'])
												{
													echo " SELECTED";
												}
												echo " >".$row[0]."</option>";
											}
				  						?>
			      					</select>
			          			</td>
			       			</tr>
			         		<?php
								//**** if del cheques
								if($_POST['tipop']=='cheque')
								{
									// echo"
									// <tr>
										// <td class='saludo1'>Cuenta Bancaria:</td>
										// <td >
											// <select id='banco' name='banco' onChange='validar()' onKeyUp='return tabular(event,this)' style='width:100%;'>
											// <option value=''>Seleccione....</option>";
									// $sqlr="select tesobancosctas.estado,tesobancosctas.cuenta,tesobancosctas.ncuentaban,tesobancosctas.tipo,terceros.razonsocial, tesobancosctas.tercero from tesobancosctas,terceros where tesobancosctas.tercero=terceros.cedulanit and tesobancosctas.estado='S' ";
									// $res=mysql_query($sqlr,$linkbd);
									// while ($row =mysql_fetch_row($res))
									// {
										// if("$row[1]"==$_POST[banco])
										// {
											// echo "<option value='$row[1]' SELECTED>$row[2] - Cuenta $row[3]</option>";
											// $_POST[nbanco]=$row[4];
											// $_POST[ter]=$row[5];
											// $_POST[cb]=$row[2];
										 // }
									  // echo "<option value='$row[1]'>$row[2] - Cuenta $row[3]</option>";
									// }
									// echo"</select>";
									echo "<tr>
					  				<td class='saludo1'>Cuenta :</td>
				                    <td>
				                    	<input type='readonly' name='cb' id='cb' value='$_POST[cb]' style='width:80%;'/>&nbsp;
				                    	<a onClick=\"despliegamodal2('visible','2');\"  style='cursor:pointer;' title='Listado Cuentas Bancarias'>
				                    		<img src='imagenes/find02.png' style='width:20px;'/>
				                    	</a>
				                    </td>
				                    <td colspan='2'>
				        					<input type='text' id='nbanco' name='nbanco' style='width:100%;' value='$_POST[nbanco]'  readonly>
				      				</td>
									<td class='saludo1'>Cheque:</td>
									<td>
										<input type='text' id='ncheque' name='ncheque' value='$_POST[ncheque]' style='width:100%;'>
									</td>
										<input type='hidden' name='banco' id='banco' value='$_POST[banco]'/>
										<input type='hidden' id='ter' name='ter' value='$_POST[ter]'/>
									</tr>";
									$sqlr="select count(*) from tesochequeras where banco='$_POST[ter]' and cuentabancaria='$_POST[cb]' and estado='S' ";
									$res2=mysqli_query($linkbd,$sqlr);
									$row2 =mysqli_fetch_row($res2);
									if($row2[0]<=0 && $_POST['oculto']!='' && $_POST['banco']!='')
						  			{
						  				// echo "
									 	// <script>
											// document.getElementById('valfocus').value='2';
											// despliegamodalm('visible','2','No existe una chequera activa para esta Cuenta');
										// </script>";
						  				// $_POST[nbanco]="";
						  				// $_POST[ncheque]="";
						  			}
						 			else
						   			{
						    			$sqlr="select * from tesochequeras where banco='$_POST[ter]' and cuentabancaria='$_POST[cb]' and estado='S' ";
										$res2=mysqli_query($linkbd,$sqlr);
										$row2 =mysqli_fetch_row($res2);
										//$_POST[ncheque]=$row2[6];
						   			}
									// echo"
											// <input name='cb' type='hidden' value='$_POST[cb]'>
											// <input type='hidden' id='ter' name='ter' value='$_POST[ter]'>
										// </td>
										// <td colspan='2'><input type='text' id='nbanco' name='nbanco' value='$_POST[nbanco]' style='width:100%;' readonly></td>
										// <td class='saludo1'>Cheque:</td>
										// <td><input type='text' id='ncheque' name='ncheque' value='$_POST[ncheque]' style='width:100%;'></td>
					  				// </tr>";
									if($_POST['cb']!=''){
										$sqlc="select cheque from tesocheques where cuentabancaria='$_POST[cb]' and estado='S' order by cheque asc";
										//echo $sqlc;
										$resc = mysqli_query($linkbd,$sqlc);
										$rowc =mysqli_fetch_row($resc);
										//echo "cheque: ".$rowc[0];
										if($rowc[0]==''){
											// echo "<script>
													// document.form2.ncheque.value='';
													// despliegamodalm('visible','2','Esta cuenta no tiene chequera registrada');
													// document.form2.ncheque.disabled=true;
												// </script>";
										}else{
											echo "<script>document.form2.ncheque.value='".$rowc[0]."';</script>";
										}
									}
				    			}//cierre del if de cheques
				  				//**** if del transferencias
				  				if($_POST['tipop']=='transferencia')
				    			{
				  					// echo"
			      					// <tr>
				  						// <td class='saludo1'>Cuenta Bancaria:</td>
				  						// <td>
				     						// <select id='banco' name='banco' onChange='validar()' onKeyUp='return tabular(event,this)' style='width:100%;'>
				      						// <option value=''>Seleccione....</option>";
									// $sqlr="select tesobancosctas.estado,tesobancosctas.cuenta,tesobancosctas.ncuentaban,tesobancosctas.tipo, terceros.razonsocial,tesobancosctas.tercero from tesobancosctas,terceros where tesobancosctas.tercero=terceros.cedulanit and tesobancosctas.estado='S' ";
									// $res=mysql_query($sqlr,$linkbd);
									// while ($row =mysql_fetch_row($res))
							    	// {
								 		// if("$row[1]"==$_POST[banco])
						 				// {
											// echo "<option value='$row[1]' SELECTED>$row[2]- Cuenta $row[3]</option>";
									 		// $_POST[nbanco]=$row[4];
									  		// $_POST[ter]=$row[5];
									 		// $_POST[cb]=$row[2];
									 	// }
								  		// else {echo "<option value='$row[1]'>$row[2]- Cuenta $row[3]</option>";}
									// }
			            			// echo"</select>";
									echo "<tr>
					  				<td class='saludo1'>Cuenta :</td>
				                    <td>
				                    	<input type='readonly' name='cb' id='cb' value='$_POST[cb]' style='width:80%;'/>&nbsp;
				                    	<a onClick=\"despliegamodal2('visible','1');\"  style='cursor:pointer;' title='Listado Cuentas Bancarias'>
				                    		<img src='imagenes/find02.png' style='width:20px;'/>
				                    	</a>
				                    </td>
				                    <td colspan='2'>
				        					<input type='text' id='nbanco' name='nbanco' style='width:100%;' value='$_POST[nbanco]'  readonly>
				      				<td class='saludo1'>No Transferencia:</td>
									<td >
										<input type='text' id='ntransfe' name='ntransfe' value='$_POST[ntransfe]' style='width:100%;'>
									</td>
										<input type='hidden' name='banco' id='banco' value='$_POST[banco]'/>
										<input type='hidden' id='ter' name='ter' value='$_POST[ter]'/>
									</tr>";
									/*$sqlr="select count(*) from tesochequeras where banco=$_POST[ter] and cuentabancaria='$_POST[cb]' and estado='S' ";
									$res2=mysql_query($sqlr,$linkbd);
									$row2 =mysql_fetch_row($res2);
									if($row2[0]<=0 && $_POST[oculto]!='')
						  			{
						   				echo "<script>alert('No existe una chequera activa para esta Cuenta');document.form2.banco.value=''; document.form2.banco.focus();</script>";
						  				$_POST[nbanco]="";
						  				$_POST[ncheque]="";
						  			}
						  			else
						   			{
						    			$sqlr="select * from tesochequeras where banco=$_POST[ter] and cuentabancaria='$_POST[cb]' and estado='S' ";
										$res2=mysql_query($sqlr,$linkbd);
										$row2 =mysql_fetch_row($res2);
						   				$_POST[ncheque]=$row2[6];
						   			}*/
									// echo"
											// <input name='cb' type='hidden' value='$_POST[cb]'>
											// <input type='hidden' id='ter' name='ter' value='$_POST[ter]'>
										// </td>
										// <td colspan='2'><input type='text' id='nbanco' name='nbanco' value='$_POST[nbanco]' style='width:100%;' readonly></td>
										// <td class='saludo1'>No Transferencia:</td>
										// <td ><input type='text' id='ntransfe' name='ntransfe' value='$_POST[ntransfe]' style='width:100%;'></td>
				  					// </tr>";
				     			}//cierre del if de cheques
								if($_POST['tipop']=='caja')
				    			{
				  					$sqlr="select cuentacaja from tesoparametros where estado='S'";
									$res=mysqli_query($linkbd,$sqlr);
									$row =mysqli_fetch_row($res);
									$_POST['banco']=$row[0];
									// echo $_POST[banco];

									echo "
										<input type='hidden' name='cb' id='cb' value='$_POST[cb]' style='width:80%;'/>
										<input type='hidden' id='nbanco' name='nbanco' style='width:100%;' value='$_POST[nbanco]'  readonly>
										<input type='hidden' id='ntransfe' name='ntransfe' value='$_POST[ntransfe]' style='width:100%;'>
										<input type='hidden' name='banco' id='banco' value='$_POST[banco]'/>
										<input type='hidden' id='ter' name='ter' value='$_POST[ter]'/>";

				     			}//cierre del if de cheques

			      			?>
							<tr>
			      				<td style="width:15%;" class="saludo1">Tercero:</td>
			          			<td >
									<input type="text" name="tercero" id="tercero" onKeyUp="return tabular(event,this)" onBlur="buscater(event)" value="<?php echo $_POST['tercero']?>" style="width:80%;">&nbsp;
									<a onClick="despliegamodal2('visible','3');"><img src="imagenes/find02.png" style="width:20px;cursor:pointer;"></a>
								</td>
			          			<td colspan="4">
			                    	<input type="text" name="ntercero" id="ntercero" value="<?php echo $_POST['ntercero']?>" style="width:100%" readonly>
			                        <input type="hidden" value="0" name="bt">
			                   	</td>
			                 	<!-- <td style="width:10%;"  class="saludo1">Centro Costo:</td> -->
								<?php
								$slqrCentroCosto = "SELECT id_cc FROM centrocosto WHERE estado = 'S' LIMIT 1";
								$resCentroCosto = mysqli_query($linkbd,$slqrCentroCosto);
								$rowCentroCosto = mysqli_fetch_row($resCentroCosto);
								$_POST['cc'] = $rowCentroCosto[0];

								?>
			                	<td style="width:20%;">
									<input type="hidden" name="cc" id = "cc" value = "<?php echo $_POST['cc']; ?>" />
				 				</td>
			     			</tr>
			          		<tr>
			        			<td style="width:15%;" class="saludo1">Concepto</td>
			                    <td colspan="3"><input type="text" name="concepto" value="<?php echo $_POST['concepto']?>" style="width:100%;"></td>
			          	  		<td style="width:10%;" class="saludo1">Valor a Pagar:</td>
			                    <td style="width:20%;"><input name="valorpagar" type="text" id="valorpagar" onKeyUp="return tabular(event,this)" value="<?php echo round($_POST['valorpagar'],0) ?>" style='text-align:right; width: 100%;' readonly></td>
			            	</tr>
			      			<tr>
			                	<td style="width:15%;" class="saludo1">Retenciones e Ingresos:</td>
								<td colspan="3">
									<select name="retencion"  onChange="validar()" onKeyUp="return tabular(event,this)" style="width:60%;">
										<option value="">Seleccione ...</option>
										<?php
											//PARA LA PARTE CONTABLE SE TOMA DEL DETALLE DE LA PARAMETRIZACION LAS CUENTAS QUE INICIAN EN 2********************
											// $sqlr="SELECT TB3.* FROM tesoretenciones TB3 WHERE TB3.estado='S' AND TB3.terceros='1' AND TB3.id NOT IN(SELECT TB1.movimiento FROM tesopagoterceros_det TB1, tesopagoterceros TB2 WHERE TB1.id_pago=TB2.id_pago AND TB1.tipo='R' AND TB2.anos='$_POST[vigencias]' AND TB2.mes='$_POST[mes]')";
											// $res=mysql_query($sqlr,$linkbd);
											// while ($row =mysql_fetch_row($res))
							    			// {
								 				// if('R-'."$row[0]"==$_POST[retencion])
						 						// {
									 				// echo "<option value='R-$row[0]' SELECTED>R - $row[1] - $row[2]</option>";
									  				// $_POST[nretencion]='R - '.$row[1]." - ".$row[2];
									 			// }
												// else{echo "<option value='R-$row[0]'>R - $row[1] - $row[2]</option>";}
											// }
											// $sqlr="SELECT TB3.* FROM tesoingresos TB3 WHERE TB3.estado='S' AND TB3.terceros='1' AND TB3.codigo NOT IN(SELECT TB1.movimiento FROM tesopagoterceros_det TB1, tesopagoterceros TB2 WHERE TB1.id_pago=TB2.id_pago AND TB1.tipo='I' AND TB2.anos='$_POST[vigencias]' AND TB2.mes='$_POST[mes]')";
											// //$sqlr="select *from tesoingresos where estado='S' and terceros='1'";
											// $res=mysql_query($sqlr,$linkbd);
											// while ($row =mysql_fetch_row($res))
							   				// {
								 				// if('I-'."$row[0]"==$_POST[retencion])
						 						// {
									 				// echo "<option value='I-$row[0]' SELECTED>I - $row[1] - $row[2]</option>";
									 				// $_POST[nretencion]='I - '.$row[1]." - ".$row[2];
												// }
								 				// else{echo "<option value='I-$row[0]'>I - $row[1] - $row[2]</option>";}
											// }
											//PARA LA PARTE CONTABLE SE TOMA DEL DETALLE DE LA PARAMETRIZACION LAS CUENTAS QUE INICIAN EN 2**********************

											$sqlr="select *from tesoretenciones where estado='S' order by codigo";
											$res=mysqli_query($linkbd,$sqlr);
											while ($row =mysqli_fetch_row($res))
											{
												echo "<option value='R-$row[0]' ";
												$i=$row[0];
												if('R-'.$i==$_POST['retencion'])
												{
													echo "SELECTED";
													$_POST['nretencion']='R - '.$row[1]." - ".$row[2];
												}
												echo ">R - ".$row[1]." - ".$row[2]."</option>";
												$reten[]= 'R-'.$row[0];
												$nreten[]= 'R - '.$row[1]." - ".$row[2];
											}
											$sqlr="select *from tesoingresos where estado='S' AND (terceros != '' OR is_tercero = 1) order by codigo";
											$res=mysqli_query($linkbd,$sqlr);
											while ($row =mysqli_fetch_row($res))
											{
												echo "<option value='I-$row[0]' ";
												$i=$row[0];
												if('I-'.$i==$_POST['retencion'])
												{
													echo "SELECTED";
													$_POST['nretencion']='I - '.$row[1]." - ".$row[2];
												}
												echo ">I - $row[0] - ".$row[1]." - ".$row[2]."</option>";
												$reten[]= 'I-'.$row[0];
												$nreten[]= 'I - '.$row[1]." - ".$row[2];
											}
											$sqlrAlumbrado = "SELECT ";

										?>
			   						</select>&nbsp;&nbsp;
			                    	<input type="hidden" value="<?php echo $_POST['nretencion']?>" name="nretencion">
			                        <input type="hidden" value="1" name="oculto" id="oculto">
									<?php
									$tipoRetencion ='';
									$tipoRetencion = buscaTipoDeRetencion(substr($_POST['retencion'],-2));
									if($tipoRetencion == 'C')
									{
										?>
										<select name="tiporet" id="tiporet">
											<option value="" >Seleccione...</option>
											<option value="N" <?php if($_POST['tiporet']=='N') echo "SELECTED"?>>Nacional</option>
											<option value="D" <?php if($_POST['tiporet']=='D') echo "SELECTED"?>>Departamental</option>
											<option value="M" <?php if($_POST['tiporet']=='M') echo "SELECTED"?>>Municipal</option>
										</select>
										<?php
									}
									?>
									<input type='hidden' name='tipoRetencion' id='tipoRetencion' value='<?php echo $tipoRetencion ?>'>
									<input type="button" name="agregard" id="agregard" value="   Agregar   " onClick="agregardetalled()" >
			                        <input type="hidden" value="0" name="agregadetdes"></td>
			        			<td style="width:10%;" class="saludo1">Ajuste Miles:&nbsp;
			        				<input type="checkbox" name="ajuste" id="ajuste"  value="1" onClick="document.form2.submit()" <?php  if($_POST['ajuste']==1) echo "checked"; ?> class="defaultcheckbox">
			        			</td>
			                    <td style="width:25%;">
			                    	<input name="valorpagarmil" type="text" id="valorpagarmil" onKeyUp="return tabular(event,this)" value="<?php echo round($_POST['valorpagarmil'],0) ?>" style='text-align:right; width: 49%;' readonly>
			                    	<input name="diferencia" type="text" id="diferencia" onKeyUp="return tabular(event,this)" value="<?php echo round($_POST['diferencia'],0) ?>" style='text-align:right; width: 49%;' readonly></td>
			          		</tr>
			                <?php if ($_POST['tipop']==''){
			                //echo"<tr style='height:20;'><tr>";
			                	}?>
       					</table>
       				</td>
       				<td colspan="3" style="width:20%; background:url(imagenes/siglasideal.png); background-repeat:no-repeat; background-position:center; background-size: 50% 100%;" ></td>
       			</tr>
      		</table>
			<div class="subpantallac3" style="height:22.5%; width:99.4%; overflow-x:hidden;">
       			<table class="inicio" style="overflow:scroll">
       				<?php
						if ($_POST['eliminad']!='')
		 				{
		 					$posi=$_POST['eliminad'];
		 					unset($_POST['ddescuentos'][$posi]);
							unset($_POST['dndescuentos'][$posi]);
							unset($_POST['dmes'][$posi]);
							unset($_POST['dntipoRetencion'][$posi]);
							unset($_POST['dntipoRet'][$posi]);
		 					$_POST['ddescuentos']= array_values($_POST['ddescuentos']);
							$_POST['dndescuentos']= array_values($_POST['dndescuentos']);
							$_POST['dmes']= array_values($_POST['dmes']);
							$_POST['dntipoRetencion']= array_values($_POST['dntipoRetencion']);
							$_POST['dntipoRet']= array_values($_POST['dntipoRet']);
		 				}
		 				if ($_POST['agregadetdes']=='1')
		 				{
							if (!(in_array($_POST['retencion'], $_POST['ddescuentos']) && in_array($_POST['mes'], $_POST['dmes'])))
							{
								$_POST['ddescuentos'][]=$_POST['retencion'];
								$_POST['dmes'][]=$_POST['mes'];
								$_POST['dndescuentos'][]=$_POST['nretencion'];
								$_POST['dntipoRetencion'][]=$_POST['tiporet'];
								$_POST['dntipoRet'][]=$_POST['tipoRetencion'];
								$_POST['agregadetdes']='0';
							}
							else {echo"<script>despliegamodalm('visible','2','La Retenci�n o Ingreso ya esta en la Lista');</script>";}
							echo"
							<script>
								document.form2.porcentaje.value=0;
								//document.form2.vporcentaje.value=0;
								document.form2.retencion.value='';
								document.form2.tiporet.value='';
								document.form2.tipoRetencion.value='';
							</script>";
		 				}
		  			?>
        			<tr>
                    	<td class="titulos">Retenciones e Ingresos</td>
                        <td class="titulos2" style="width:8%;text-align:center;">Eliminar</td>
                  	</tr>
                    <input type='hidden' name='eliminad' id='eliminad'>
      				<?php
						$totaldes=0;
						$co="saludo1a";
		  				$co2="saludo2";
						for ($x=0;$x<count($_POST['ddescuentos']);$x++)
		 				{
		 					echo "
							<input type='hidden' name='dndescuentos[]' value='".$_POST['dndescuentos'][$x]."'/>
							<input type='hidden' name='ddescuentos[]' value='".$_POST['ddescuentos'][$x]."'/>
							<input type='hidden' name='dmes[]' value='".$_POST['dmes'][$x]."'/>
							<input name='dntipoRetencion[]' value='".$_POST['dntipoRetencion'][$x]."' type='hidden'>
							<input name='dntipoRet[]' value='".$_POST['dntipoRet'][$x]."' type='hidden'>
							<tr class='$co' onMouseOver=\"anterior=this.style.backgroundColor;this.style.backgroundColor='#40b3ff';\" onMouseOut=\"this.style.backgroundColor=anterior\">
								<td>".$_POST['dndescuentos'][$x]."</td>
								<td style='text-align:center;'><a onclick='eliminard($x)'><img src='imagenes/del.png' style='cursor:pointer;'></a></td>
							</tr>";
							$aux=$co;
							$co=$co2;
							$co2=$aux;
		 				}
						$_POST['valorretencion']=$totaldes;
						echo"
        				<script>
        					document.form2.totaldes.value='totaldes';
       						document.form2.valorretencion.value='$totaldes';
        				</script>";
						$_POST['valorcheque']=$_POST['valoregreso']-$_POST['valorretencion'];
					?>
        		</table>
         		<?php
           			//***** busca tercero
			 		if($_POST['bt']=='1')
			 		{
			 			$nresul=buscatercero($_POST['tercero']);
			  			if($nresul!='')
			   			{
			  				$_POST['ntercero']=$nresul;
  							echo"
							<script>
			  					document.getElementById('retencion').focus();
								document.getElementById('retencion').select();
							</script>";
			  			}
			 			else
			 			{
			  				$_POST['ntercero']="";
			 				echo"
			  				<script>
								document.getElementById('valfocus').value='1';
								despliegamodalm('visible','2','Tercero Incorrecto o no Existe');
			  				</script>";
			 			}
			 		}
			 	?>
			</div>
	  		<div class="subpantallac" style="height:22.5%; width:99.4%; overflow-x:hidden;">
       			<table class="inicio" >
        			<tr>
						<td class="titulos">Consecutivo</td>
                    	<td class="titulos">Retenciones / Ingresos</td>
                        <td class="titulos">Contabilidad</td>
                        <td class="titulos">Centro Costo</td>
                        <td class="titulos">Valor</td>
                  	</tr>
      				<?php
						$_POST['mddescuentos']=array();
						$_POST['mtdescuentos']=array();
						$_POST['mddesvalores']=array();
						$_POST['mddesvalores2']=array();
						$_POST['mdndescuentos']=array();
						$_POST['mdctas']=array();
						$_POST['consecutivo']=array();
						$_POST['centroCosto']=array();
						$totalpagar=0;
						preg_match("/([0-9]{2})\/([0-9]{2})\/([0-9]{4})/", $_POST["fecha"], $fecha);
						$fechaf1=$fecha[3]."-".$fecha[2]."-".$fecha[1];
						//**** buscar movimientos
						for ($x=0;$x<count($_POST['ddescuentos']);$x++)
		 				{
		 					$tm=strlen($_POST['ddescuentos'][$x]);
							//********** RETENCIONES *********
							if(substr($_POST['ddescuentos'][$x],0,1)=='R')
		  					{
								$query="SELECT conta_pago FROM tesoparametros";
								$resultado=mysqli_query($linkbd,$query);
								$arreglo=mysqli_fetch_row($resultado);
								if(substr($_POST['ddescuentos'][$x],2,$tm-2)=='34')
								{
									$sqlrete="SELECT SUM(valorretencion) FROM hum_retencionesfun WHERE tiporetencion='34' AND vigencia='$_POST[vigencias]' AND mes='$_POST[mes]' AND estadopago='N'";
									$resrete = mysqli_query($linkbd,$sqlrete);
									$rowrete =mysqli_fetch_row($resrete);
									$vpor=100;
									$_POST['mtdescuentos'][]='R';
									$_POST['mddesvalores'][]=$rowrete[0]*($vpor/100);
									$_POST['mddesvalores2'][]=$rowrete[0]*($vpor/100);
									$_POST['mddescuentos'][]=35;
									$_POST['mdctas'][]=243615001;
									$_POST['consecutivo'][]=0;
									$_POST['centroCosto'][]='01';
									$_POST['mdndescuentos'][]=buscaretencion(34);
									$totalpagar+=$rowrete[0]*($vpor/100);
								}
								$query="SELECT conta_pago FROM tesoparametros";
								$resultado=mysqli_query($linkbd,$query);
								$arreglo=mysqli_fetch_row($resultado);
								$opcion=$arreglo[0];
								if($opcion=='1')
								{
									$sqlr="select tesoordenpago_retenciones.id_retencion, sum(tesoordenpago_retenciones.valor),tesoordenpago.base,tesoordenpago.iva,tesoordenpago.id_orden, tesoordenpago.valorpagar from tesoordenpago, tesoordenpago_retenciones where MONTH(tesoordenpago.fecha)='".$_POST['dmes'][$x]."' AND YEAR(tesoordenpago.fecha)='".$_POST['vigencias']."' and tesoordenpago_retenciones.id_retencion='".substr($_POST['ddescuentos'][$x],2,$tm-2)."' and tesoordenpago.id_orden=tesoordenpago_retenciones.id_orden AND tesoordenpago.estado!='R' AND tesoordenpago.estado!='N' AND tesoordenpago.tipo_mov='201' GROUP BY tesoordenpago.id_orden  ORDER BY tesoordenpago.id_orden";
									$res=mysqli_query($linkbd,$sqlr);
									while ($row =mysqli_fetch_row($res))
									{
										$crit ='';
										if($_POST['dntipoRet'][$x] == 'C')
										{
											$crit = "AND destino='".$_POST['dntipoRetencion'][$x]."' AND conceptoingreso!= '-1'";
										}

                                        $sqlrOrdenSum = "SELECT SUM(valor) FROM tesoordenpago_det WHERE id_orden='".$row[4]."' AND valor > 0";
                                        $resOrdenSum = mysqli_query($linkbd, $sqlrOrdenSum);
                                        $rowOrdenSum = mysqli_fetch_row($resOrdenSum);
                                        $valorTotal = 0;
                                        if($rowOrdenSum[0] > 0)
                                        {
                                            $valorTotal = $rowOrdenSum[0];
                                        }else{
                                            continue;
                                        }

										$sqlr="select *from tesoretenciones_det where codigo='$row[0]' $crit";
										$rowRete = view($sqlr);
										//var_dump(count($rowRete));
										for($xx = 0; $xx<count($rowRete); $xx++)
										{
											$sqlrOrden = "SELECT valor, cc FROM tesoordenpago_det WHERE id_orden='".$row[4]."' AND valor > 0";
											$resOrden = mysqli_query($linkbd,$sqlrOrden);
											while($rowOrden = mysqli_fetch_row($resOrden)){

												$rest = substr($rowRete[$xx]['tipoconce'],-2);
												$sq="select fechainicial from conceptoscontables_det where codigo='".$rowRete[$xx]['conceptoingreso']."' and modulo='".$rowRete[$xx]['modulo']."' and tipo='$rest' and cc = '".$rowOrden[1]."' and fechainicial<'$fechaf1' and cuenta!='' order by fechainicial asc";
												$re=mysqli_query($linkbd,$sq);
												while($ro=mysqli_fetch_assoc($re))
												{
													$_POST['fechacausa']=$ro["fechainicial"];
												}
												$sqlr="select * from conceptoscontables_det where codigo='".$rowRete[$xx]['conceptoingreso']."' and modulo='".$rowRete[$xx]['modulo']."' and cc = '".$rowOrden[1]."' and tipo='$rest' and fechainicial='".$_POST['fechacausa']."'";
												$rst=mysqli_query($linkbd,$sqlr);
												$row1=mysqli_fetch_assoc($rst);

												$retPocentaje = $rowOrden[0]/$valorTotal;
												if($_POST['dntipoRet'][$x] == 'C')
												{

													$_POST['mtdescuentos'][]='R';
													$_POST['mddesvalores'][]=($row[1]*($rowRete[$xx]['porcentaje']/100))*$retPocentaje;
													$_POST['mddesvalores2'][]=($row[1]*($rowRete[$xx]['porcentaje']/100))*$retPocentaje;
													$_POST['mddescuentos'][]=$row[0];
													$_POST['mdctas'][]=$row1['cuenta'];
													$_POST['consecutivo'][]=$row[4];
													$_POST['centroCosto'][]=$rowOrden[1];
													$_POST['mdndescuentos'][]=buscaretencion($row[0]);
													$totalpagar+=($row[1]*($rowRete[$xx]['porcentaje']/100))*$retPocentaje;
												}
												else
												{
													$_POST['mtdescuentos'][]='R';
													$_POST['mddesvalores'][]=$row[1]*$retPocentaje;
													$_POST['mddesvalores2'][]=$row[1]*$retPocentaje;
													$_POST['mddescuentos'][]=$row[0];
													$_POST['mdctas'][]=$row1['cuenta'];
													$_POST['consecutivo'][]=$row[4];
													$_POST['centroCosto'][]=$rowOrden[1];
													$_POST['mdndescuentos'][]=buscaretencion($row[0]);
													$totalpagar+=$row[1]*$retPocentaje;
												}
											}
										}

										/*$sqlr="select *from tesoretenciones_det where codigo='$row[0]'";
										//echo $sqlr;
										$res2=mysql_query($sqlr,$linkbd);
										while($row2 =mysql_fetch_row($res2))
										{
											if($row2[7]=='9')
												continue;
											$rest=substr($row2[6],-2);
											$sq="select fechainicial from conceptoscontables_det where codigo='$row2[4]' and modulo='$row2[5]' and tipo='$rest' and fechainicial<'$fechaf1' and cuenta!='' order by fechainicial asc";
											$re=mysql_query($sq,$linkbd);
											while($ro=mysql_fetch_assoc($re))
											{
												$_POST[fechacausa]=$ro["fechainicial"];
											}
											$sqlr="select * from conceptoscontables_det where codigo='$row2[4]' and modulo='$row2[5]' and tipo='$rest' and fechainicial='".$_POST[fechacausa]."'";
											$rst=mysql_query($sqlr,$linkbd);
											$row1=mysql_fetch_assoc($rst);
											if(substr($row1['cuenta'],0,1)==2)
											{
												$vpor=$row2[7];
												$_POST[mtdescuentos][]='R';
												$_POST[mddesvalores][]=$row[1]*($vpor/100);
												$_POST[mddesvalores2][]=$row[1]*($vpor/100);
												$_POST[mddescuentos][]=$row[0];
												$_POST[mdctas][]=$row1['cuenta'];
												$_POST[mdndescuentos][]=buscaretencion($row[0]);
												$totalpagar+=$row[1]*($vpor/100);
											}
										}*/
									}
								}
								else
								{
									$sqlr="select tesoordenpago_retenciones.id_retencion, sum(tesoordenpago_retenciones.valor), tesoordenpago.id_orden, tesoordenpago.valorpagar from tesoordenpago, tesoordenpago_retenciones,tesoegresos where tesoegresos.id_orden=tesoordenpago.id_orden  and tesoegresos.estado='S' and MONTH(tesoegresos.fecha)='$_POST[mes]' and YEAR(tesoegresos.fecha)='".$_POST['vigencias']."' and tesoordenpago_retenciones.id_retencion='".substr($_POST['ddescuentos'][$x],2,$tm-2)."' and tesoordenpago.id_orden=tesoordenpago_retenciones.id_orden AND tesoordenpago.estado!='N' AND tesoordenpago.tipo_mov='201' group by tesoordenpago_retenciones.id_retencion, tesoordenpago.id_orden";
									//echo $sqlr;
									$res=mysqli_query($linkbd,$sqlr);
									while ($row =mysqli_fetch_row($res))
									{
										$crit ='';
										if($_POST['dntipoRet'][$x] == 'C')
										{
											$crit = "AND destino='".$_POST['dntipoRetencion'][$x]."' AND conceptoingreso!= '-1'";
										}

                                        $sqlrOrdenSum = "SELECT SUM(valor) FROM tesoordenpago_det WHERE id_orden='".$row[2]."' AND valor > 0";
                                        $resOrdenSum = mysqli_query($linkbd, $sqlrOrdenSum);
                                        $rowOrdenSum = mysqli_fetch_row($resOrdenSum);
                                        $valorTotal = 0;
                                        if($rowOrdenSum[0] > 0)
                                        {
                                            $valorTotal = $rowOrdenSum[0];
                                        }else{
                                            continue;
                                        }

										$sqlr="select *from tesoretenciones_det where codigo='$row[0]' $crit";
										$rowRete = view($sqlr);
										//var_dump(count($rowRete));
										for($xx = 0; $xx<count($rowRete); $xx++)
										{
											$sqlrOrden = "SELECT valor, cc FROM tesoordenpago_det WHERE id_orden='".$row[2]."' AND valor > 0";
											$resOrden = mysqli_query($linkbd,$sqlrOrden);
											while($rowOrden = mysqli_fetch_row($resOrden)){

												$rest = substr($rowRete[$xx]['tipoconce'],-2);
												$sq="select fechainicial from conceptoscontables_det where codigo='".$rowRete[$xx]['conceptoingreso']."' and modulo='".$rowRete[$xx]['modulo']."' and tipo='$rest' and fechainicial<'$fechaf1' and cuenta!='' order by fechainicial asc";
												$re=mysqli_query($linkbd,$sq);
												while($ro=mysqli_fetch_assoc($re))
												{
													$_POST['fechacausa']=$ro["fechainicial"];
												}
												$sqlr="select * from conceptoscontables_det where codigo='".$rowRete[$xx]['conceptoingreso']."' and modulo='".$rowRete[$xx]['modulo']."' and tipo='$rest' and fechainicial='".$_POST['fechacausa']."'";
												$rst=mysqli_query($linkbd,$sqlr);
												$row1=mysqli_fetch_assoc($rst);

												$retPocentaje = $rowOrden[0]/$valorTotal;

												if($_POST['dntipoRet'][$x] == 'C')
												{
													$_POST['mtdescuentos'][]='R';
													$_POST['mddesvalores'][]=($row[1]*($rowRete[$xx]['porcentaje']/100))*$retPocentaje;
													$_POST['mddesvalores2'][]=($row[1]*($rowRete[$xx]['porcentaje']/100))*$retPocentaje;
													$_POST['mddescuentos'][]=$row[0];
													$_POST['mdctas'][]=$row1['cuenta'];
													$_POST['consecutivo'][]=$row[2];
													$_POST['centroCosto'][]=$rowOrden[1];
													$_POST['mdndescuentos'][]=buscaretencion($row[0]);
													$totalpagar+=($row[1]*($rowRete[$xx]['porcentaje']/100))*$retPocentaje;
												}
												else
												{
													$_POST['mtdescuentos'][]='R';
													$_POST['mddesvalores'][]=$row[1]*$retPocentaje;
													$_POST['mddesvalores2'][]=$row[1]*$retPocentaje;
													$_POST['mddescuentos'][]=$row[0];
													$_POST['mdctas'][]=$row1['cuenta'];
													$_POST['consecutivo'][]=$row[2];
													$_POST['centroCosto'][]=$rowOrden[1];
													$_POST['mdndescuentos'][]=buscaretencion($row[0]);
													$totalpagar+=$row[1]*$retPocentaje;
												}
											}
										}
									}
									/*while ($row =mysql_fetch_row($res))
									{

										$sqlr="select *from tesoretenciones_det where codigo='$row[0]'";
										//echo $sqlr;
										$res2=mysql_query($sqlr,$linkbd);
										while($row2 =mysql_fetch_row($res2))
										{
											$rest=substr($row2[6],-2);
											$sq="select fechainicial from conceptoscontables_det where codigo='$row2[4]' and modulo='$row2[5]' and tipo='$rest' and fechainicial<'$fechaf1' and cuenta!='' order by fechainicial asc";
											$re=mysql_query($sq,$linkbd);
											while($ro=mysql_fetch_assoc($re))
											{
												$_POST[fechacausa]=$ro["fechainicial"];
											}
											$sqlr="select * from conceptoscontables_det where codigo='$row2[4]' and modulo='$row2[5]' and tipo='$rest' and fechainicial='".$_POST[fechacausa]."'";
											$rst=mysql_query($sqlr,$linkbd);
											$row1=mysql_fetch_assoc($rst);
											if(substr($row1['cuenta'],0,1)==2)
											{
												$vpor=$row2[7];
												$_POST[mtdescuentos][]='R';
												$_POST[mddesvalores][]=$row[1]*($vpor/100);
												$_POST[mddesvalores2][]=$row[1]*($vpor/100);
												$_POST[mddescuentos][]=$row[0];
												$_POST[mdctas][]=$row1['cuenta'];
												$_POST[mdndescuentos][]=buscaretencion($row[0]);
												$totalpagar+=$row[1]*($vpor/100);
											}
										}
									}*/
								}

								$sqlr="SELECT tesopagotercerosvigant_retenciones.id_retencion, sum(tesopagotercerosvigant_retenciones.valor), tesopagotercerosvigant_retenciones.id_egreso, tesopagotercerosvigant_retenciones.valor FROM tesopagotercerosvigant_retenciones,tesopagotercerosvigant where tesopagotercerosvigant.id_pago = tesopagotercerosvigant_retenciones.id_egreso AND tesopagotercerosvigant.estado='S' and MONTH(tesopagotercerosvigant.fecha)='$_POST[mes]' AND YEAR(tesopagotercerosvigant.fecha)='".$_POST['vigencias']."' AND tesopagotercerosvigant_retenciones.id_retencion='".substr($_POST['ddescuentos'][$x],2,$tm-2)."'  GROUP BY tesopagotercerosvigant_retenciones.id_retencion";

								//$sqlr="SELECT tesoordenpago_retenciones.id_retencion, sum(tesoordenpago_retenciones.valor), tesoordenpago.id_orden, tesoordenpago.valorpagar FROM tesoordenpago, tesoordenpago_retenciones,tesoegresos WHERE tesoegresos.id_orden=tesoordenpago.id_orden  AND tesoegresos.estado='S' AND MONTH(tesoegresos.fecha)='$_POST[mes]' AND YEAR(tesoegresos.fecha)='".$_POST['vigencias']."' AND tesoordenpago_retenciones.id_retencion='".substr($_POST['ddescuentos'][$x],2,$tm-2)."' AND tesoordenpago.id_orden=tesoordenpago_retenciones.id_orden AND tesoordenpago.estado!='N' AND tesoordenpago.tipo_mov='201' group by tesoordenpago_retenciones.id_retencion";
								//echo $sqlr;
								$res=mysqli_query($linkbd,$sqlr);
								while ($row =mysqli_fetch_row($res))
								{
									$crit ='';
                                    if($_POST['dntipoRet'][$x] == 'C')
                                    {
                                        $crit = "AND tesoretenciones_det.destino='".$_POST['dntipoRetencion'][$x]."'";
                                    }
                                    $sqlr="select *from tesoretenciones, tesoretenciones_det  where tesoretenciones_det.codigo='$row[0]' AND tesoretenciones.id=tesoretenciones_det.codigo $crit ORDER BY porcentaje DESC";
                                    $rowRete = view($sqlr);
                                    $valorProcentaje = 0;
                                    $idRetencion = '';

									for($xx = 0; $xx<count($rowRete); $xx++)
									{
										if($idRetencion == $rowRete[$xx]['codigo']){
                                            $valorProcentaje = $val2 + $rowRete[$xx]['porcentaje'];
                                        }else{
                                            $valorProcentaje = $rowRete[$xx]['porcentaje'];
                                            $idRetencion = $rowRete[$xx]['codigo'];
                                        }

                                        $val2 = 0;
                                        $val2 = $rowRete[$xx]['porcentaje'];

                                        if($rowRete[$xx]['tipo'] == 'C' && $rowRete[$xx]['destino'] != $_POST['dntipoRetencion'][$x])
                                        {
                                            continue;
                                        }

                                        $rest = substr($rowRete[$xx]['tipoconce'],-2);
                                        $sq="select fechainicial from conceptoscontables_det where codigo='".$rowRete[$xx]['conceptoingreso']."' and modulo='".$rowRete[$xx]['modulo']."' and tipo='$rest' and fechainicial<'$fechaf1' and cuenta!='' order by fechainicial asc";
                                        $re=mysqli_query($linkbd,$sq);
                                        while($ro=mysqli_fetch_assoc($re))
                                        {
                                            $_POST['fechacausa']=$ro["fechainicial"];
                                        }
                                        $sqlr="select * from conceptoscontables_det where codigo='".$rowRete[$xx]['conceptoingreso']."' and modulo='".$rowRete[$xx]['modulo']."' and tipo='$rest' and fechainicial='".$_POST['fechacausa']."'";
                                        $rst=mysqli_query($linkbd,$sqlr);
                                        $row1=mysqli_fetch_assoc($rst);

                                        $retPocentaje = $rowOrden[0]/$valorTotal;

                                        switch ($_POST['dntipoRet'][$x]) {
                                            case 'C':
                                                $_POST['mtdescuentos'][] = 'R';
                                                $_POST['mddesvalores'][] = ($row[1]*($rowRete[$xx]['porcentaje']/100));
                                                $_POST['mddesvalores2'][] = ($row[1]*($rowRete[$xx]['porcentaje']/100));
                                                $_POST['mddescuentos'][] = $row[0];
                                                $_POST['mdctas'][] = $row1['cuenta'];
                                                $_POST['consecutivo'][] = $row[2];
                                                $_POST['centroCosto'][] = '01';
                                                $_POST['mdndescuentos'][] = buscaretencion($row[0]);
                                                $totalpagar += ($row[1]*($rowRete[$xx]['porcentaje']/100));
                                                break;
                                            default:
                                                $_POST['mtdescuentos'][] = 'R';
                                                $_POST['mddesvalores'][] = $row[1];
                                                $_POST['mddesvalores2'][] = $row[1];
                                                $_POST['mddescuentos'][] = $row[0];
                                                $_POST['mdctas'][] = $row1['cuenta'];
                                                $_POST['consecutivo'][] = $row[2];
                                                $_POST['centroCosto'][] = '01';
                                                $_POST['mdndescuentos'][] = buscaretencion($row[0]);
                                                $totalpagar += $row[1];
                                        }
									}
								}

							}
							//****** INGRESOS *******

							if(substr($_POST['ddescuentos'][$x],0,1)=='I')
		  					{

								//	$sqlr="select  tesorecaudos_det.ingreso, sum(tesorecaudos_det.valor), tesoreciboscaja.cuentabanco, tesoreciboscaja.cuentacaja from tesorecaudos, tesorecaudos_det,tesoreciboscaja where tesorecaudos.estado='P' and MONTH(tesoreciboscaja.fecha)='$_POST[mes]' and YEAR(tesoreciboscaja.fecha)='".$_POST[vigencias]."' and tesorecaudos_det.ingreso='".substr($_POST[ddescuentos][$x],2,$tm-2)."' and tesorecaudos.id_recaudo=tesorecaudos_det.id_recaudo and tesorecaudos.id_recaudo=tesoreciboscaja.id_recaudo and tesoreciboscaja.tipo=3";
								$sqlr="select tesoreciboscaja_det.ingreso, sum(tesoreciboscaja_det.valor), tesoreciboscaja.cuentabanco, tesoreciboscaja.cuentacaja, tesoreciboscaja.id_recibos from  tesoreciboscaja_det,tesoreciboscaja where tesoreciboscaja.estado='S' AND tesoreciboscaja.tipo=3 and MONTH(tesoreciboscaja.fecha)='$_POST[mes]' and YEAR(tesoreciboscaja.fecha)='".$_POST['vigencias']."' and tesoreciboscaja_det.ingreso='".substr($_POST['ddescuentos'][$x],2,$tm-2)."' and tesoreciboscaja_det.id_recibos=tesoreciboscaja.id_recibos group by tesoreciboscaja_det.ingreso";
		                  		$res=mysqli_query($linkbd,$sqlr);
								while ($row =mysqli_fetch_row($res))
	    						{
		 							$sqlr="select * from  tesoingresos_det where codigo='$row[0]' and vigencia=(SELECT MAX(vigencia) FROM tesoingresos_det WHERE codigo = '$row[0]')";
		  							$res2=mysqli_query($linkbd,$sqlr);
		  							while($row2 =mysqli_fetch_row($res2))
		   							{
										$sq="select fechainicial from conceptoscontables_det where codigo='$row2[2]' and modulo='4' and tipo='C' and fechainicial<'$fechaf1' and cuenta!='' order by fechainicial asc";
										//echo $sq;
										$re=mysqli_query($linkbd,$sq);
										while($ro=mysqli_fetch_assoc($re))
										{
											$_POST['fechacausa']=$ro["fechainicial"];
										}
		   								$sqlr_cont="select *from  conceptoscontables_det where codigo='$row2[2]' and modulo='4' and tipo='C' and fechainicial='".$_POST['fechacausa']."' ORDER BY cuenta DESC";
		  								$res3=mysqli_query($linkbd,$sqlr_cont);
		   								while($row3 =mysqli_fetch_row($res3))
		   								{
		   									if(substr($row3[4],0,1)=='2' || substr($row3[4],0,1) == 3 || substr($row3[4],0,1) == 1)
		    								{
		   										$vpor=$row2[5];
		   										$_POST['mtdescuentos'][]='I';
	   	   										$_POST['mddesvalores'][]=$row[1]*($vpor/100);
		   										$_POST['mddesvalores2'][]=$row[1]*($vpor/100);
												$_POST['mddescuentos'][]=$row[0];
											   	$_POST['mdctas'][]=$row3[4];
												$_POST['consecutivo'][]=$row[4];
												$_POST['centroCosto'][]=$row3[5];
											   	$_POST['mdndescuentos'][]=buscaingreso($row[0]);
											   	$totalpagar+=$row[1]*($vpor/100);
												break;
											   	//$nv=buscaingreso($row[0]);
											   	//echo "ing:$nv";
											}
		   								}
		  							}
		 						}
								 //****** RECAUDO TRANSFERENCIAS *******
							$sqlr="select tesorecaudotransferencia_det.ingreso, sum(tesorecaudotransferencia_det.valor), tesorecaudotransferencia.banco,tesorecaudotransferencia.ncuentaban, tesorecaudotransferencia.id_recaudo from  tesorecaudotransferencia_det,tesorecaudotransferencia where tesorecaudotransferencia.estado='S' and MONTH(tesorecaudotransferencia.fecha)='$_POST[mes]' and YEAR(tesorecaudotransferencia.fecha)='".$_POST['vigencias']."' and tesorecaudotransferencia_det.ingreso='".substr($_POST['ddescuentos'][$x],2,$tm-2)."' and tesorecaudotransferencia_det.id_recaudo=tesorecaudotransferencia.id_recaudo group by tesorecaudotransferencia_det.ingreso";

							$res=mysqli_query($linkbd,$sqlr);
						   while ($row =mysqli_fetch_row($res))
						   {
								$sqlr="select *from  tesoingresos_det where codigo='$row[0]' and vigencia = (SELECT MAX(vigencia) FROM tesoingresos_det WHERE codigo = '$row[0]')";
								 $res2=mysqli_query($linkbd,$sqlr);
								 while($row2 =mysqli_fetch_row($res2))
								  {
								   $sq="select fechainicial from conceptoscontables_det where codigo='$row2[2]' and modulo='4' and tipo='C' and fechainicial<'$fechaf1' and cuenta!='' order by fechainicial asc";
								   //echo $sq;
								   $re=mysqli_query($linkbd,$sq);
								   while($ro=mysqli_fetch_assoc($re))
								   {
									   $_POST['fechacausa']=$ro["fechainicial"];
								   }
									 $sqlr="select *from  conceptoscontables_det where codigo='$row2[2]' and modulo='4' and tipo='C' and fechainicial='".$_POST['fechacausa']."' ORDER BY cuenta DESC";
									 $res3=mysqli_query($linkbd,$sqlr);
									  while($row3 =mysqli_fetch_row($res3))
									  {
										  if(substr($row3[4],0,1)=='2' || substr($row3[4],0,1) == 3 || substr($row3[4],0,1) == 1)
									   {
											 $vpor=$row2[5];
											  $_POST['mtdescuentos'][]='I';
											  $_POST['mddesvalores'][]=$row[1]*($vpor/100);
											  $_POST['mddesvalores2'][]=$row[1]*($vpor/100);
											  $_POST['mddescuentos'][]=$row[0];
											  $_POST['mdctas'][]=$row3[4];
											  $_POST['consecutivo'][]=$row[4];
											  $_POST['centroCosto'][]=$row3[5];
											  $_POST['mdndescuentos'][]=buscaingreso($row[0]);
											  $totalpagar+=$row[1]*($vpor/100);
											  //$nv=buscaingreso($row[0]);
											  //echo "ing:$nv";
											  break;
									   }
									 }
								 }
							}
							//*****INGRESOS PROPIOS
							$sqlr="select tesosinreciboscaja_det.ingreso, sum(tesosinreciboscaja_det.valor),tesosinreciboscaja.cuentabanco, tesosinreciboscaja.cuentacaja, tesosinreciboscaja.id_recibos from  tesosinreciboscaja_det,tesosinreciboscaja where tesosinreciboscaja.estado='S' and MONTH(tesosinreciboscaja.fecha)='$_POST[mes]' and YEAR(tesosinreciboscaja.fecha)='".$_POST['vigencias']."' and tesosinreciboscaja_det.ingreso='".substr($_POST['ddescuentos'][$x],2,$tm-2)."' and tesosinreciboscaja_det.id_recibos=tesosinreciboscaja.id_recibos group by  tesosinreciboscaja_det.ingreso ";
							$res=mysqli_query($linkbd,$sqlr);
							while ($row =mysqli_fetch_row($res))
							{

									$sqlr="select *from  tesoingresos_det where codigo='$row[0]' and vigencia=(SELECT MAX(vigencia) FROM tesoingresos_det WHERE codigo = '$row[0]')";
									$res2=mysqli_query($linkbd,$sqlr);
									//echo "$row[0] - ".$sqlr;
									while($row2 =mysqli_fetch_row($res2))
									{
									$sq="select fechainicial from conceptoscontables_det where codigo='$row2[2]' and modulo='4' and tipo='C' and fechainicial<'$fechaf1' and cuenta!='' order by fechainicial asc";
									//echo $sq;
									$re=mysqli_query($linkbd,$sq);
									while($ro=mysqli_fetch_assoc($re))
									{
										$_POST['fechacausa']=$ro["fechainicial"];
									}
										$sqlr="select *from  conceptoscontables_det where codigo='$row2[2]' and modulo='4' and tipo='C' and fechainicial='".$_POST['fechacausa']."' group by cuenta ORDER BY cuenta DESC";
										$res3=mysqli_query($linkbd,$sqlr);
										while($row3 =mysqli_fetch_row($res3))
										{
											if(substr($row3[4],0,1)=='2' || substr($row3[4],0,1)=='4' || substr($row3[4],0,1) == 3 || substr($row3[4],0,1) == 1)
											{
												$vpor=$row2[5];
												$_POST['mtdescuentos'][]='I';
													$_POST['mddesvalores'][]=$row[1]*($vpor/100);
												$_POST['mddesvalores2'][]=$row[1]*($vpor/100);
												$_POST['mddescuentos'][]=$row[0];
												$_POST['mdctas'][]=$row3[4];
												$_POST['consecutivo'][]=$row[4];
											    $_POST['centroCosto'][]=$row3[5];
												$_POST['mdndescuentos'][]=buscaingreso($row[0]);
												$totalpagar+=$row[1]*($vpor/100);
												break;
											}
										}
									}
								}
							}

						}//********************************
						$co="saludo1a";
		  				$co2="saludo2";
						for ($x=0;$x<count($_POST['mddescuentos']);$x++)
		 				{
		 					echo "
							<input type='hidden' name='mdndescuentos[]' value='".$_POST['mdndescuentos'][$x]."'/>
							<input type='hidden' name='mddescuentos[]' value='".$_POST['mddescuentos'][$x]."'/>
							<input type='hidden' name='mtdescuentos[]' value='".$_POST['mtdescuentos'][$x]."'/>
							<input type='hidden' name='mdctas[]' value='".$_POST['mdctas'][$x]."'/>
							<input type='hidden' name='centroCosto[]' value='".$_POST['centroCosto'][$x]."'/>
							<input type='hidden' name='mddesvalores[]' value='".round($_POST['mddesvalores'][$x],0)."'/>
							<input type='hidden' name='mddesvalores2[]' value='".number_format($_POST['mddesvalores2'][$x],0)."'/>
							<tr class='$co' onMouseOver=\"anterior=this.style.backgroundColor;this.style.backgroundColor='#40b3ff';\" onMouseOut=\"this.style.backgroundColor=anterior\">
								<td>".$_POST['consecutivo'][$x]."</td>
								<td>".$_POST['mdndescuentos'][$x]."</td>
								<td>".$_POST['mdctas'][$x]."</td>
								<td>".$_POST['centroCosto'][$x]."</td>
								<td style='text-align:right;'>$ ".number_format($_POST['mddesvalores2'][$x],0)."</td>
							</tr>";
							$aux=$co;
							$co=$co2;
							$co2=$aux;
		 				}
		 				$vmil=0;
						if($_POST['ajuste']=='1'){$vmil=round($totalpagar,-3);}
		  				else {$vmil=$totalpagar;}
						$resultado = convertir(round($vmil,0));
						$_POST['letras']=$resultado." PESOS M/CTE";
		 				echo "
						<input type='hidden' name='totalpago2' value='".round($totalpagar,0)."'/>
						<input type='hidden' name='totalpago' value='".number_format($totalpagar,0)."'/>
						<input type='hidden' name='letras' value='$_POST[letras]'/>
						<tr class='$co' style='text-align:right;'>
							<td colspan='4'>Total:</td>
							<td>$ ".number_format($totalpagar,0)."</td>
						</tr>
						<tr class='titulos2'><td colspan='5'>Son: $_POST[letras]</td>";
						$dif=$vmil-$totalpagar;
						echo"
        				<script>
       						document.form2.valorpagar.value='".round($totalpagar,0)."';
        					document.form2.valorpagarmil.value='$vmil';
							document.form2.diferencia.value='".round($dif,0)."';//calcularpago();
        				</script>";
					?>
        		</table>
	   		</div>
        	<?php
				if($_POST['oculto']=='2')
				{
					$sqlr="select max(tesopagoterceros.id_pago) from tesopagoterceros" ;
					$res=mysqli_query($linkbd,$sqlr);
					while($r=mysqli_fetch_row($res)){$consec=$r[0];}
	 				$consec+=1;
					$_POST['idcomp']=$consec;
					//**verificacion de guardado anteriormente *****
					$sqlr="select count(*) from tesopagoterceros where id_pago=$_POST[idcomp] ";
					$res=mysqli_query($linkbd,$sqlr);
					$numerorecaudos=0;
					while($r=mysqli_fetch_row($res)){$numerorecaudos=$r[0];}
					if($numerorecaudos==0)
	 				{
						$sqlr="update tesocheques set estado='P', destino='PAGO_TERCERO', idcomp='$_POST[idcomp]' where cuentabancaria='$_POST[cb]' and cheque='$_POST[ncheque]'";
		 				mysqli_query($linkbd,$sqlr);
 						preg_match("/([0-9]{2})\/([0-9]{2})\/([0-9]{4})/", $_POST["fecha"], $fecha);
						$fechaf=$fecha[3]."-".$fecha[2]."-".$fecha[1];
	 					$sqlr="delete from comprobante_cab where tipo_comp=12 and numerotipo=$_POST[idcomp] ";
						mysqli_query($linkbd,$sqlr);
						//************CREACION DEL COMPROBANTE CONTABLE ************************
						$sqlr="insert into tesopagoterceros (id_pago,tercero,banco,cheque,transferencia,valor,mes,concepto,cc,estado,fecha,ajuste, anos) values ($_POST[idcomp],'$_POST[tercero]','$_POST[banco]', '$_POST[ncheque]','$_POST[ntransfe]',$totalpagar,'$_POST[mes]','$_POST[concepto]', '$_POST[cc]','S','$fechaf', '$_POST[ajuste]','$_POST[vigencias]')";
						mysqli_query($linkbd,$sqlr);
						//***busca el consecutivo del comprobante contable
						$sqlr="insert into comprobante_cab (numerotipo,tipo_comp,fecha,concepto,total,total_debito,total_credito,diferencia,estado) values ($_POST[idcomp] ,12,'$fechaf','$_POST[concepto]',0,$totalpagar,$totalpagar,0,'1')";
						mysqli_query($linkbd,$sqlr);
						for ($x=0;$x<count($_POST['mddescuentos']);$x++)
	 					{
							$sqlr="insert into comprobante_det (id_comp,cuenta,tercero,centrocosto,detalle,cheque,valdebito,valcredito,estado, vigencia) values ('12 $_POST[idcomp]','".$_POST['mdctas'][$x]."','".$_POST['tercero']."','".$_POST['centroCosto'][$x]."','PAGO RECAUDO A TERCERO MES ".$meses[$_POST['mes']]."','$_POST[ncheque]$_POST[ntransfe]',".round($_POST['mddesvalores'][$x], 2).",0,'1','".$_POST['vigencias']."')";
							mysqli_query($linkbd,$sqlr);
		 					//*** Cuenta BANCO **
							$sqlr="insert into comprobante_det (id_comp,cuenta,tercero,centrocosto,detalle,cheque,valdebito,valcredito,estado, vigencia) values ('12 $_POST[idcomp]','".$_POST['banco']."','".$_POST['tercero']."','".$_POST['centroCosto'][$x]."','PAGO RECAUDO A TERCERO MES ".$meses[$_POST['mes']]."','$_POST[ncheque]$_POST[ntransfe]',0,".round($_POST['mddesvalores'][$x], 2).",'1','".$_POST['vigencias']."')";
							mysqli_query($linkbd,$sqlr);

							$sqlr="insert into tesopagoterceros_det(`id_pago`, `movimiento`, `tipo`, `valor`, `cuenta`, `estado`, `mes`, `anio`, `cc`) values ($_POST[idcomp],'".$_POST['mddescuentos'][$x]."','".$_POST['mtdescuentos'][$x]."',".round($_POST['mddesvalores'][$x], 2).",'".$_POST['mdctas'][$x]."','S','".$meses[$_POST['mes']]."','".$_POST['vigencias']."','".$_POST['centroCosto'][$x]."')";
							mysqli_query($linkbd,$sqlr);
	  					}
	  					if($_POST['diferencia']<>0)
	 					{
		 					if($_POST['diferencia']>0)
		 					{
			 					$sqlr="insert into comprobante_det (id_comp,cuenta,tercero,centrocosto,detalle,cheque,valdebito,valcredito,estado, vigencia) values ('12 $_POST[idcomp]','".$_POST['cuentamiles']."','".$_POST['tercero']."','".$_POST['cc']."','AJUSTE MIL PAGO RECAUDO A TERCERO MES ".$meses[$_POST['mes']]."','$_POST[ncheque]$_POST[ntransfe]',".$_POST['diferencia'].",0,'1','".$_POST['vigencias']."')";
								mysqli_query($linkbd,$sqlr);
		 						//*** Cuenta BANCO **
								$sqlr="insert into comprobante_det (id_comp,cuenta,tercero,centrocosto,detalle,cheque,valdebito,valcredito,estado, vigencia) values ('12 $_POST[idcomp]','".$_POST['banco']."','".$_POST['tercero']."','".$_POST['cc']."','AJUSTE MIL PAGO RECAUDO A TERCERO MES ".$meses[$_POST['mes']]."','$_POST[ncheque]$_POST[ntransfe]',0,".abs($_POST['diferencia']).",'1','".$_POST['vigencias']."')";
								mysqli_query($linkbd,$sqlr);
		 					}
		 					if($_POST['diferencia']<0)
		 					{
								$sqlr="insert into comprobante_det (id_comp,cuenta,tercero,centrocosto,detalle,cheque,valdebito,valcredito,estado, vigencia) values ('12 $_POST[idcomp]','".$_POST['cuentamiles']."','".$_POST['tercero']."','".$_POST['cc']."','AJUSTE MIL PAGO RECAUDO A TERCERO MES ".$meses[$_POST['mes']]."','$_POST[ncheque]$_POST[ntransfe]',0,".abs($_POST['diferencia']).",'1','".$_POST['vigencias']."')";
								mysqli_query($linkbd,$sqlr);
		 						//*** Cuenta BANCO **
								$sqlr="insert into comprobante_det (id_comp,cuenta,tercero,centrocosto,detalle,cheque,valdebito,valcredito,estado, vigencia) values ('12 $_POST[idcomp]','".$_POST['banco']."','".$_POST['tercero']."','".$_POST['cc']."','AJUSTE MIL PAGO RECAUDO A TERCERO MES ".$meses[$_POST['mes']]."','$_POST[ncheque]$_POST[ntransfe]',".abs($_POST['diferencia']).",0,'1','".$_POST['vigencias']."')";
								mysqli_query($linkbd,$sqlr);
		 					}
						}
						echo"<script>despliegamodalm('visible','1','Se ha almacenado el Recaudo a Terceros con Exito');</script>";
	 				}//*** if de guardado
	 				else {echo"<script>despliegamodalm('visible','2',Ya Se ha almacenado un documento con ese consecutivo');</script>";}
				}
			?>
 			<div id="bgventanamodal2">
                <div id="ventanamodal2">
                    <IFRAME  src="" name="buster" marginWidth=0 marginHeight=0 frameBorder=0 id="ventana2" frameSpacing=0 style="left:500px; width:900px; height:500px; top:200;">
                    </IFRAME>
                </div>
       	 	</div>
		</form>
	</body>
</html>
