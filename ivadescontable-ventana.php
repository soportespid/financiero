<?php
	header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=utf8");
	require "comun.inc";
	require "funciones.inc";
	require "validaciones.inc";
	session_start();
	$linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");
	cargarcodigopag($_GET['codpag'],$_SESSION["nivel"]);
	date_default_timezone_set("America/Bogota");
?>
<!doctype html>
<html lang="es">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=9">
		<title>:: Ideal</title>
		<link href="css/css2.css" rel="stylesheet" type="text/css">
		<link href="css/css3.css" rel="stylesheet" type="text/css">
		<script type="text/javascript" src="css/programas.js"></script>
		<script type="text/javascript" src="css/calendario.js"></script>
		<script> 
			function ponprefijo(pref)
			{
				parent.document.getElementById('cuentaIvaDescontable').value = pref;
				parent.despliegamodal2("hidden");
			}
		</script>
	</head>
	<body>
		<form name="form2" action="" method="post">
			<?php
				if ($_POST['oculto']=='')
				{
					
				}
			?>
			<table class="inicio" style="width:99.4%;">
				<tr>
					<td class="titulos" colspan="4">:: Buscar Cuenta contable</td>
					<td class="cerrar" style="width:7%" onClick="parent.despliegamodal2('hidden');">Cerrar</td>
				</tr>
			</table>
			<input type="hidden" name="oculto" id="oculto" value="1">
			<input type="hidden" name="numres" id="numres" value="<?php echo $_POST['numres'];?>">
			<input type="hidden" name="numpos" id="numpos" value="<?php echo $_POST['numpos'];?>">
			<input type="hidden" name="nummul" id="nummul" value="<?php echo $_POST['nummul'];?>">
		    <div class="subpantalla" style="height:84.5%; width:99%; overflow-x:hidden;">
				<?php
					$sqlrCuentaContable = "SELECT cuenta, debito, credito, cc FROM conceptoscontables_det WHERE tipo = 'IV'";
                    $resCuentaContable = mysqli_query($linkbd, $sqlrCuentaContable);
                    $con=1;
                    echo "
                    <table class='inicio' align='center' width='99%'>
                        <tr>
                            <td class='titulos2' style='width:3%'>Item</td>
                            <td class='titulos2' style='width:20%'>Cuenta contable</td>
                            <td class='titulos2' style='width:60'>Nombre cuenta</td>
                            <td class='titulos2' style='width:10/'>Centro costo</td>
                        </tr>";
                    $iter='saludo1a';
                    $iter2='saludo2'; 
                    $cuentasDebito = array();
                    $cuentasCredito = array();
                    $cc = array();
                    while ($rowCuentaContable = mysqli_fetch_row($resCuentaContable))
                    {
                        if($rowCuentaContable[1] == 'S'){$cuentasDebito[] = $rowCuentaContable[0];}
                        else {$cuentasCredito[] = $rowCuentaContable[0];}
                        $cc[] = $rowCuentaContable[3];
                    }
                    for($x = 0; $x < count($cuentasDebito); $x++)
                    {
                        $nombreCuentaContable = buscacuenta($cuentasDebito[0]);

                        $con2 = $con + $_POST['numpos'];
                        echo"
                        <tr class='$iter' onClick=\"javascript:ponprefijo('$cuentasDebito[$x]')\" style='text-transform:uppercase'>
                            <td>$con2</td>
                            <td>$cuentasDebito[$x]</td>
                            <td>$nombreCuentaContable</td>
                            <td>$cc[$x]</td>
                        </tr>";
                        $con += 1;
                        $aux = $iter;
                        $iter = $iter2;
                        $iter2 = $aux;
                    } 
                    echo"</table>";
				?>
			</div>
		</form>
	</body>
</html>
