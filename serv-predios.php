<?php
	header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=iso-8859-1");
	require"comun.inc";
	require"funciones.inc";
	require 'funcionessp.inc';
	session_start();
	$linkbd=conectar_v7();
?>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="es">
	<head>
		<meta http-equiv="Content-type" content="text/html; charset=iso-8859-1"/>
		<meta http-equiv="X-UA-Compatible" content="IE=9"/>
		<title>:: SPID - Servicios Publicos</title>
		<link href="css/css2.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<script type="text/javascript" src="css/programas.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script type="text/javascript" src="css/calendario.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script>
			function agregardetalle()
			{
				if(document.form2.medidor.value!=""  )
				{
					document.form2.agregadet.value=1;
					document.form2.submit();
				}
				else {alert("Falta informacion para poder Agregar");}
			}
			function eliminar(variable)
			{
				if (confirm("Esta Seguro de Eliminar"))
				{
					document.form2.elimina.value=variable;
					vvend=document.getElementById('elimina');
					vvend.value=variable;
					document.form2.submit();
				}
			}
			function guardar()
			{
				if (document.form2.tercero.value!='' )
				{
					if (confirm("Esta Seguro de Guardar"))
					{
						document.form2.oculto.value=2;
						document.form2.submit();
					}
				}
				else
				{
					alert('Faltan datos para completar el registro');
					document.form2.tercero.focus();
					document.form2.tercero.select();
				}
			}
			function buscater(e)
			{
				if (document.form2.tercero.value!="")
				{
					document.form2.bt.value='1';
					document.form2.submit();
				}
			}
			function buscar()
			{
				document.form2.buscav.value='1';
				document.form2.submit();
			}
		</script>
		<?php titlepag();?>
	</head>
	<body>
		<IFRAME src="alertas.php" name="alertas" id="alertas" style="display:none"></IFRAME>
		<span id="todastablas2"></span>
		<table>
			<tr><script>barra_imagenes("serv");</script><?php cuadro_titulos();?></tr>	 
			<tr><?php menu_desplegable("serv");?></tr>
			<tr>
				<td colspan="3" class="cinta"><a href="serv-predios.php" ><img src="imagenes/add.png" alt="Nuevo"  border="0" /></a> <a href="#"  onClick="guardar();"><img src="imagenes/guarda.png"  alt="Guardar" /></a><a href="serv-buscapredios.php" > <img src="imagenes/busca.png"  alt="Buscar" /></a> <a href="#" onClick="mypop=window.open('serv-principal.php','','');mypop.focus();"><img src="imagenes/nv.png" alt="nueva ventana"></a></td>
			</tr>
		</table>
		<form  name="form2" method="post" action="">
			<?php
				$vigencia=date('Y');
				$sqlr="SELECT MAX(codigo) FROM servclientes ";
				$resp=mysqli_query($linkbd,$sqlr);
				while($r=mysqli_fetch_row($resp))
				{
					$_POST['consecadd']=$r[0];
				}
				$_POST['consecadd']=$_POST['consecadd']+1;
				$_POST['consecadd']="00000000".$_POST['consecadd'];
				$_POST['consecadd']=substr($_POST['consecadd'],-10);
				$sqlr="SELECT depto,mnpio FROM configbasica ";
				$resp=mysqli_query($linkbd,$sqlr);
				while($r=mysqli_fetch_row($resp))
				{
					$_POST['prefijo']=$r[0]."".$r[1];
				}
				if(!@$_POST['oculto'])
				{
					$fec=date("d/m/Y");
					$_POST['fecha']=$fec;
					$_POST['valoradicion']=0;
					$_POST['valorreduccion']=0;
					$_POST['valortraslados']=0;
					$_POST['valor']=0;
				}
				if (@$_POST['chacuerdo']=='2')
				{
					$_POST['dcuentas']=array();
					$_POST['dncuetas']=array();
					$_POST['dingresos']=array();
					$_POST['dgastos']=array();
					$_POST['diferencia']=0;
					$_POST['cuentagas']=0;
					$_POST['cuentaing']=0;
				}
				if(@$_POST['bt']=='1')
				{
					$nresul=buscatercero($_POST['tercero']);
					if($nresul!=''){$_POST['ntercero']=$nresul;}
					else{$_POST['ntercero']="";}
				}
				if(@$_POST['buscav']=='1')
				{
					$_POST['prediocon']=array();
					$_POST['terceros']=array();
					$_POST['fechain']=array();
					$_POST['fechafin']=array();
					$_POST['descrips']=array();
					$_POST['estados']=array();
					$_POST['modificar']=array();
					$sqlr="SELECT * FROM tesopredios WHERE cedulacatastral='".$_POST['codcat']."'";
					$res=mysqli_query($linkbd,$sqlr);
					while($row=mysqli_fetch_row($res))
					{
						$_POST['catastral']=$row[0];
						$_POST['ntercerop']=$row[6];
						$_POST['tercerop']=$row[5];
						$_POST['direccion']=$row[7];
						$_POST['ha']=$row[8];
						$_POST['mt2']=$row[9];
						$_POST['areac']=$row[10];
						$_POST['avaluo']=number_format($row[11],2);
						$_POST['tipop']=$row[14];
						if($_POST['tipop']=='urbano'){$_POST['estrato']=$row[15];}
						else{$_POST['rangos']=$row[15];}
						$_POST['dtcuentas'][]=$row[1];
						$_POST['dvalores'][]=$row[5];
						$_POST['buscav']="";
					}
					$sqlr="SELECT * FROM terceros_servicios WHERE codcatastral='".$_POST['codcat']."' ORDER BY consecutivo ASC";
					$res=mysqli_query($linkbd,$sqlr);
					$_POST['ncons']=-1;
					while($row=mysqli_fetch_row($res))
					{
						$nter=buscatercero($row[0]);
						$_POST['prediocon'][]=$row[1]."-".$row[2];
						$_POST['terceros'][]=$row[0];
						$_POST['fechain'][]=$row[3];
						$_POST['fechafin'][]=$row[4];
						$_POST['descrips'][]="REGISTRADO";
						$_POST['estados'][]=$row[5];
						$_POST['ncons']=$row[2];
					}
				}
				if(@$_POST['bt']=='1')
				{
					$nresul=buscatercero($_POST['tercero']);
					if($nresul!='')
					{
						$_POST['ntercero']=$nresul;
						echo"<script>document.getElementById('agrega').focus();document.getElementById('agrega').select();</script>";
					}
					else
					{
						$_POST[ntercero]="";
						echo"<script>alert('Tercero Incorrecto o no Existe');document.form2.tercero.focus();</script>";
					}
				}
			?>
			<table class="inicio" align="center" >
				<tr >
					<td class="titulos" colspan="7">.: Crear Clientes</td>
					<td  class="cerrar" ><a href="serv-principal.php">Cerrar</a></td>
				</tr>
				<tr>
					<td class="saludo1">Codigo Cliente:</td>
					<td>
						<input type="text" name="prefijo" value="<?php echo @$_POST['prefijo']; ?>" size="5" readonly/>
						<input type="text" name="consecadd" value="<?php echo @$_POST['consecadd']; ?>" size="20" readonly/>
					</td>
					<td class="saludo1">Cod Anterior:</td>
					<td><input name="codanterior" type="text" value="<?php echo @$_POST['codanterior']; ?>" size="20" ></td>
					<td class="saludo1">Tercero:</td>
					<td >
						<input type="text" id="tercero" name="tercero" onKeyUp="return tabular(event,this)" value="<?php echo @$_POST['tercero']?>" size="10" onBlur="buscater(event)" onClick="document.getElementById('tercero').focus(); document.getElementById('tercero').select();" />
						<input type="hidden" value="0" name="bt"> 
						<input type="hidden" name="ncons" value="<?php @$_POST['ncons']?>" >
						<a href="#" onClick="mypop=window.open('terceros-ventana.php','','menubar=0,scrollbars=yes, toolbar=no, location=no, width=900,height=500px');mypop.focus();"><img src="imagenes/buscarep.png" align="absmiddle" border="0"></a>
						<input type="text" name="ntercero" value="<?php echo @$_POST['ntercero']?>" size="40" readonly/>
					</td>
				</tr>
				<tr>
					<td  class="saludo1">Buscar Cod Catastral:</td>
					<td  colspan="2"><input type="text" id="codcat" name="codcat" size="20" onKeyUp="return tabular(event,this)" onBlur="buscar(event)" value="<?php echo@ $_POST['codcat']?>" onClick="document.getElementById('codcat').focus(); document.getElementById('codcat').select();"> <a href="#" onClick="mypop=window.open('catastral-ventana.php','','menubar=0,scrollbars=yes, toolbar=no, location=no, width=900,height=500px');mypop.focus();"><img src="imagenes/buscarep.png" align="absmiddle" border="0"/></a> <input type="hidden" name="chacuerdo" value="1"><input type="hidden" value="1" name="oculto"> <input type="hidden" value="<?php echo @$_POST['buscav']?>" name="buscav"></td>
				</tr>
				<tr>
					<td class="saludo1">Codigo Catastral:</td>
					<td><input type="hidden" value="<?php echo @$_POST['nbanco']?>" name="nbanco"> <input type="text" name="catastral"  id="catastral"  onClick="document.getElementById('tercero').focus(); document.getElementById('tercero').select();" onKeyUp="return tabular(event,this)" value="<?php echo @$_POST['catastral']?>" onBlur="buscater(event)" size="20" readonly/></td>
					<td class="saludo1">Direccion:</td>
					<td colspan="3"  ><input name="direccion" type="text" id="direccion" onKeyUp="return tabular(event,this)" value="<?php echo @$_POST['direccion']?>" size="80"></td>
				</tr>
				<tr>
					<td class="saludo1">Barrio:</td>
					<td>
						<select name="barrios" >
							<option value="">Seleccione ...</option>
							<?php
								$sqlr="select *from servbarrios where estado='S'";
								$res=mysqli_query($linkbd,$sqlr);
								while ($row =mysqli_fetch_row($res)) 
								{
									if($row[0]==@$_POST['barrios'])
									{echo "<option value='$row[0]' SELECTED>".strtoupper($row[0]." ".$row[1])."</option>";}
									else {echo "<option value='$row[0]'>".strtoupper($row[0]." ".$row[1])."</option>";}
								}
							?>
						</select>
					</td>
					<td class="saludo1">Zona:</td>
					<td>
						<select name="zona" >
							<option value="">Seleccione ...</option>
							<?php
								$sqlr="select *from servzonas where estado='S'";
								$res=mysqli_query($linkbd,$sqlr);
								while ($row =mysqli_fetch_row($res))
								{
									if($row[0]==@$_POST['zona'])
									{echo "<option value='$row[0]' SELECTED>".strtoupper($row[0]." ".$row[1])."</option>";}
									else {echo "<option value='$row[0]'>".strtoupper($row[0]." ".$row[1])."</option>";}
								}
							?>
						</select>
					</td>
					<td class="saludo1">Lado:</td>
					<td>
						<select name="lado" >
							<option value="">Seleccione ...</option>
							<option value="A" <?php if(@$_POST['lado']=='A') echo "Selected"?>>Lado A</option>
							<option value="B" <?php if(@$_POST['lado']=='B') echo "Selected"?>>Lado B</option>
							<option value="C" <?php if(@$_POST['lado']=='C') echo "Selected"?>>Lado C</option>
							<option value="D" <?php if(@$_POST['lado']=='D') echo "Selected"?>>Lado D</option>
						</select>
					</td>
				</tr>
				<tr>
					<td class="saludo1">Estratos:</td>
					<td>
						<select name="estrato" >
							<option value="">Seleccione ...</option>
							<?php
								$sqlr="select *from servestratos where estado='S'";
								$res=mysqli_query($linkbd,$sqlr);
								while ($row =mysqli_fetch_row($res)) 
								{
									if($row[0]==@$_POST['estrato'])
									{
										echo "<option value='$row[0]' SELECTED>".strtoupper($row[2])." - ".$row[1]."</option>";
										$_POST['nestrato']=$row[1];
									}
									else {echo "<option value='$row[0]'>".strtoupper($row[2])." - ".$row[1]."</option>";}
								}
							?>
						</select>
						<input type="hidden" name="nestrato" value="<?php echo $_POST['nestrato']?>"/>
					</td>
					<td class="saludo1">Fecha Activacion:</td>
					<td><input name="fecha" type="text" value="<?php echo @$_POST['fecha']?>" maxlength="10" size="10" onKeyPress="javascript:return solonumeros(event)" onKeyUp="return tabular(event,this)" id="fc_1198971545" onKeyDown="mascara(this,'/',patron,true)" title="DD/MM/YYYY">   <a href="#" onClick="displayCalendarFor('fc_1198971545');"><img src="imagenes/buscarep.png" align="absmiddle" border="0"></a></td> 
				</tr>
				<tr>
					<td class="saludo1" >Medidor:</td>
					<td><input type="text" size="30" name="medidor" value="<?php echo @$_POST['medidor'] ?>" readonly> <a href="#" onClick="mypop=window.open('medidores-ventana.php','','menubar=0,scrollbars=yes, toolbar=no, location=no, width=900,height=500px');mypop.focus();"><img src="imagenes/buscarep.png" align="absmiddle" border="0"></a></td>
					<td> <input type="button" name="agregamedidor" value="Agregar Medidor" onClick="agregardetalle()"><input type="hidden" value="0" name="agregadet"></td>
				</tr>
			</table>
			<table class="iniciop">
				<tr>
					<td class="titulos">Codigo Medidor</td>
					<td class="titulos">Estado Medidor</td>
					<td class="titulos">Servicios</td>
					<td class="titulos"><img src="imagenes/del.png" ><input type='hidden' name='elimina' id='elimina'></td>
				</tr>
				<?php
					if (@$_POST['elimina']!='')
					{
						$posi=$_POST['elimina'];
						unset($_POST['dcodmedidor'][$posi]);
						unset($_POST['ddestmed'][$posi]);
						unset($_POST['dservmed'][$posi]);
						unset($_POST['ddestmedc'][$posi]);
						$_POST['dcodmedidor']= array_values($_POST['dcodmedidor']);
						$_POST['ddestmed']= array_values($_POST['ddestmed']);
						$_POST['dservmed']= array_values($_POST['dservmed']);
						$_POST['ddestmedc']= array_values($_POST['ddestmedc']);
					}
					if (@$_POST['agregadet']=='1')
					{
						$_POST['dcodmedidor'][]=$_POST['medidor'];
						$_POST['ddestmed'][]=$_POST['estadomed'];
						$_POST['dservmed'][]=buscarmedidor_servicios($_POST['medidor']);
						$_POST['ddestmedc'][]=$_POST['estadomedoc'];
						$_POST['agregadet']=0;
					}
					for ($x=0;$x<count(@$_POST['dcodmedidor']);$x++)
					{
						echo "
							<tr>
								<td class='saludo1'><input type='text' name='dcodmedidor[]' value='".@$_POST['dcodmedidor'][$x]."' size='10' readonly></td>
								<td class='saludo1'><input type='text' name='ddestmed[]' value='".@$_POST['ddestmed'][$x]."' size='20' readonly><input type='hidden' name='ddestmedc[]' value='".@$_POST['ddestmedc'][$x]."' size='4' readonly></td>
								<td class='saludo1'><input type='text' name='dservmed[]' value='".@$_POST['dservmed'][$x]."' size='15' readonly></td>
								<td class='saludo1'><a href='#' onclick='eliminar($x)'><img src='imagenes/del.png'></a></td>
							</tr>";
					}
				?>
			</table>
			<table class="iniciop">
				<tr><td class="titulos" colspan="5">Servicios:</td></tr>
				<tr>
					<td class="titulos2">Cod Servicio</td>
					<td class="titulos2">Servicio</td>
					<td class="titulos2">Promedio Lectura</td>
					<td class="titulos2">Cargue Inicial</td>
					<td class="titulos2">Activar</td>
				</tr>
				<?php
					$co="saludo1";
					$co2="saludo2";
					$x=0;
					$sqlr="select *from servservicios ";
					$res=mysqli_query($linkbd,$sqlr);
					
					while($row=mysqli_fetch_row($res))
					{
						$chks="";
						if(esta_en_array(@$_POST['servicios'],$row[0])){$chks=" checked";}
						if(@$_POST['promval'][$x] == NULL or @$_POST['promval'][$x] == ""){$_POST['promval'][$x]=0;}
						if(@$_POST['saldoval'][$x] == NULL or @$_POST['saldoval'][$x] == ""){$_POST['saldoval'][$x]=0;}
						echo "
						<tr class='$co'>
							<td colspan='1' >$row[0]</td>
							<td>$row[1]</td>
							<td><input name='promval[]' type='text'  value='".@$_POST['promval'][$x]."' size='5'></td>
							<td><input name='saldoval[]' type='text'  value='".@$_POST['saldoval'][$x]."' size='5'></td>
							<td><input name='servicios[]' value='".$row[0]."' type='checkbox'  $chks></td>
						</tr> ";
						$aux=$co;
						$co=$co2;
						$co2=$aux;
						$x+=1;
					}
				?>
			</table>
			<?php
				if(@$_POST['oculto']=='2')
				{
					preg_match("/([0-9]{2})\/([0-9]{2})\/([0-9]{4})/", $_POST['fecha'],$fecha);
					$fechaf="$fecha[3]-$fecha[2]-$fecha[1]";
					$sqlr="insert into servclientes (codigo,prefijo,codcatastral,estrato,direccion, terceroactual, codigo_anterior, fechacreacion, barrio, estado,estadomedidor,intereses,nombretercero,zona,lado) values ('".$_POST['consecadd']."','".$_POST['prefijo']."','".$_POST['catastral']."','".$_POST['estrato']."','".$_POST['direccion']."','".$_POST['tercero']."','".$_POST['codanterior']."','$fechaf','".$_POST['barrios']."','S','1',0,'".$_POST['ntercero']."','".$_POST['zona']."','".$_POST['lado']."')";
					if (!mysqli_query($linkbd,$sqlr))
					{
						echo "<table class='inicio'><tr><td class='saludo1'><center><font color=blue><img src='imagenes\alert.png'> Manejador de Errores de la Clase BD<br><font size=1></font></font><br><p align=center>No se pudo ejecutar la petici�n: <br><font color=red><b>$sqlr</b></font></p>";	
					}
					else
					{
						for($x=0;$x<count(@$_POST['dcodmedidor']);$x++)
						{
							$sqlr="update servmedidores set cliente='".$_POST['consecadd']."' where codigo='".$_POST['dcodmedidor'][$x]."'";
							mysql_query($linkbd,$sqlr);
						}
						echo "<table class='inicio'><tr><td class='saludo1'><center>Se ha Creado el Cliente  ".$_POST['consecadd']." con Exito <img src='imagenes\confirm.png'></center></td></tr></table>";
						$tam=count(@$_POST['servicios']);
						for($x=0;$x<$tam;$x++)
						{
							//************** modificacion del presupuesto **************
							$sqlr="insert into terceros_servicios (cedulanit, codcatastral, consecutivo, servicio, fechainicial, fechafinal, estado,promedio,saldo,interes) values ('".$_POST['tercero']."','".$_POST['catastral']."', '".$_POST['consecadd']."','".$_POST['servicios'][$x]."','$fechaf','','S','".$_POST['promval'][$x]."', ".$_POST['saldoval'][$x].",0)";
							if (!mysqli_query($linkbd,$sqlr))
							{
								echo "<table class='inicio'><tr><td class='saludo1'><center><font color=blue><img src='imagenes\alert.png'> Manejador de Errores de la Clase BD<br><font size=1></font></font><br><p align=center>No se pudo ejecutar la petici�n: <br><font color=red><b>$sqlr</b></font></p>";
								echo "Ocurri� el siguiente problema:<br>";
								echo "<pre>";
								echo "</pre></center></td></tr></table>";
							}
							else
							{
								echo "<table class='inicio'><tr><td class='saludo1'><center>Se ha Creado el Cliente - Servicio: ".$_POST['servicios'][$x]."  con Exito <img src='imagenes\confirm.png'></center></td></tr></table>";
								echo"<script>document.form2.tercero.value='';document.form2.ntercero.value='';</script>";
							}
						}		//**** fin del ciclo
					}
				}
			?>
		</form>
	</body>
</html>