<?php
	header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=iso-8859-1");
	require 'comun.inc';
	require 'funciones.inc';
	require 'validaciones.inc';
	require 'funcionesnomima.inc';
	session_start();
	$linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");
	date_default_timezone_set("America/Bogota");
?>
<!DOCTYPE >
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="es">
	<head>
		<meta http-equiv="Content-type" content="text/html; charset=iso-8859-1"/>
		<meta http-equiv="X-UA-Compatible" content="IE=9"/>
		<title>:: Spid - Tesoreria</title>
		<link href="css/css2.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<style>
			.example1
			{
				font-size: 3em;
				color: limegreen;
				position: absolute;
				width: 100%;
				height: 100%;
				margin: 0;
				line-height: 50px;
				text-align: center;
				transform:translateX(100%);/* Starting position */
				animation: example1 15s linear infinite;/* Apply animation to this element */
			}
			@keyframes example1 /* Move it (define the animation) */
			{
				0% {transform: translateX(100%);}
				100% {transform: translateX(-100%);}
			}
		</style>
		<script type="text/javascript" src="css/programas.js"></script>
		<script>
			function procesos(){document.form2.oculto.value='2';document.form2.submit();}
		</script>
		<?php titlepag();?>
	</head>
	<body>
		<IFRAME src="alertas.php" name="alertas" id="alertas" style="display:none"></IFRAME>
		<span id="todastablas2"></span>
		<table>
			<tr><script>barra_imagenes("hum");</script><?php cuadro_titulos();?></tr>
			<tr><?php menu_desplegable("hum");?></tr>
		</table>
		<form name="form2" method="post" action="">
			<table class="inicio">
				<tr>
					<td class="titulos" colspan="7">ARREGLO CACHARROS VARIOS</td>
					<td class="cerrar" style="width:7%" onClick="location.href='teso-principal.php'">Cerrar</td>
				</tr>
				<tr>
					<td class="saludo1" style="width:2.5cm;">.: Inicio:</td>
					<td style="width:10%;"><input type="text" name="varini" id="varini" value="<?php echo $_POST['varini']?>" style="width:100%"/></td>
					<td class="saludo1" style="width:2.5cm;">.: Fin:</td>
					<td style="width:10%;"><input type="text" name="varfin" id="varfin" value="<?php echo $_POST['varfin']?>" style="width:100%"/></td>
					<td></td>
				</tr>
				<tr>
					<td class="saludo1">.: Proceso:</td>
					<td colspan="4" style='height:28px;'>
						<select name="selecob" id="selecob" style='text-transform:uppercase; width:100%; height:22px;'>
							<option value="">....</option>
							<option value='1' <?php if($_POST['selecob']=='1'){echo "SELECTED";}?>>1: Pasar detalles contabilidad a tesoreria</option>
							
						</select>
					<td style="padding-bottom:5px" colspan="2"><em class="botonflecha" onClick="procesos();">Correr</em></td>
					<td></td>
				</tr>
			</table>
			<input type="hidden" name="oculto" id="oculto" value="1"/>
			<div class="subpantalla" style="height:68.5%; width:99.6%;overflow-x:hidden" id="divdet">
			<?php
				if($_POST['oculto'] == 2){
					switch ($_POST['selecob']){
						case '1':{//Pasar detalles contabilidad a tesoreria
							if($_POST['varini'] != ''){
								for ($x = $_POST['varini']; $x <= $_POST['varfin']; $x++){
									$tercero = '';
									$valor1 = 0;
									$ingreso = '';
									//$sql = "SELECT tercero, valcredito  FROM comprobante_det WHERE tipo_comp = '41' AND numerotipo = '$x' AND cuenta = '41553001'";
									$sql = "SELECT tercero, valcredito  FROM comprobante_det WHERE tipo_comp = '41' AND numerotipo = '$x' AND cuenta = '439090002'";
									$res = mysqli_query($linkbd,$sql);
									$row = mysqli_fetch_row($res);
									if($row[0] != ''){
										$tercero = $row[0];
										$valor1 = $row[1];
										$ingreso = '01';
									}else{
										//$sql = "SELECT tercero, valcredito  FROM comprobante_det WHERE tipo_comp = '41' AND numerotipo = '$x' AND cuenta = '41553002'";
										$sql = "SELECT tercero, valcredito  FROM comprobante_det WHERE tipo_comp = '41' AND numerotipo = '$x' AND cuenta = '480890002'";
										$res = mysqli_query($linkbd,$sql);
										$row = mysqli_fetch_row($res);
										$tercero = $row[0];
										$valor1 = $row[1];
										$ingreso = '02';
									}
									$valoriva = 0;
									//$sql = "SELECT valcredito  FROM comprobante_det WHERE tipo_comp = '41' AND numerotipo = '$x' AND cuenta = '24080501'";
									$sql = "SELECT valcredito  FROM comprobante_det WHERE tipo_comp = '41' AND numerotipo = '$x' AND cuenta = '244502001'";
									$res = mysqli_query($linkbd,$sql);
									$row = mysqli_fetch_row($res);
									$valoriva = $row[0];
									if($valor1 == ''){
										$valor1 = 0;
									}
									if($valoriva == ''){
										$valoriva = 0;
									}
									$sqlr = "INSERT INTO tesobienesservicios_det (id_recaudo,ingreso, valor, estado, cc, fuente, iva) VALUES ('$x', '$ingreso', '$valor1', 'S', '02', '1.2.1.0.00', '$valoriva')";	
									mysqli_query($linkbd,$sqlr);
								}
							}
							else{echo "<h3 class='example1'>Error agrega numero de nomina en inicio</h3>";}
						}break;
					}
				}
			?> 
			</div>
		</form>
	</body>
</html>