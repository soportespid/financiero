<?php
	date_default_timezone_set("America/Bogota");
	require_once("tcpdf/tcpdf_include.php");
	require('comun.inc');
	require "funciones.inc";
	require 'funcionesnomima.inc';
    require 'funcionesSP.inc.php';
    /*ini_set('display_errors', '1');
    ini_set('display_startup_errors', '1');
    error_reporting(E_ALL);*/
	session_start();
	class MYPDF extends TCPDF
	{
        public function Header()
		{
			$linkbd = conectar_v7();
			$linkbd -> set_charset("utf8");
			$sqlr="select *from configbasica where estado='S' ";
			//echo $sqlr;
			$res=mysqli_query($linkbd, $sqlr);
			while($row=mysqli_fetch_row($res))
			{
				$nit=$row[0];
				$rs=$row[1];

			}
			//Parte Izquierda
			$this->Image('imagenes/escudo.jpg',  13, 13, 20, 20, 'JPG', '', 'T', true, 300, '', false, false, 0, false, false, false);// Logo
			$this->SetFont('helvetica','B',8);
			$this->SetY(10);
			$this->RoundedRect(10, 10, 277, 25, 1,'');
			$this->Cell(0.1);
			$this->Cell(26,25,'','R',0,'L');
			$this->SetY(8);
			$this->SetX(80);
			$this->SetFont('helvetica','B',9);
			$this->Cell(160,15,strtoupper("$rs"),0,0,'C');
			$this->SetFont('helvetica','B',7);
			$this->SetY(12);
			$this->SetX(80);
			$this->Cell(160,15,'NIT: '.$nit,0,0,'C');
			//*****************************************************************************************************************************
			$this->SetFont('helvetica','B',9);
			$this->SetY(23);
			$this->SetX(36);
            $this->Cell(251,12,"CERTIFICADO PAZ Y SALVO - SERVICIOS PÚBLICOS",'T',0,'C');


            $this->SetFont('helvetica','B',7);
			$this->SetY(10);
			$this->SetX(255);
			$this->Cell(30,13," FECHA: ".date("d/m/Y"),"L",0,'L');
			$this->SetY(17);
			$this->SetX(257);
			// $this->Cell(35,6," FECHA: ".date("d/m/Y"),"L",0,'L');

			//**********************************************************
			$this->SetFont('times','B',10);
			$this->ln(12);
		}
        public function ColoredTable($data) {
            // Colors, line width and bold font
            $this->SetFillColor(222, 222, 222);
            $this->SetTextColor(000);
            //$this->SetDrawColor(128, 0, 0);
            $this->SetLineWidth(0.3);
            $this->SetFont('helvetica','B',8);
            // Header
            $this->SetFont('helvetica','',8);
            $this->Ln();
            // Color and font restoration
            $this->SetFillColor(245,245,245);
            $this->SetTextColor(0);
            $this->SetFont('');
            // Data
        }
		public function Footer()
		{
			$linkbd = conectar_v7();
			$linkbd -> set_charset("utf8");
			$sql1="SELECT lema FROM interfaz01 ";
			$res1 = mysqli_query($linkbd,$sql1);
			$row1 = mysqli_fetch_row($res1);
			$lema = $row1[0];

            $user = $_SESSION['nickusu'];
			$fecha = date("Y-m-d H:i:s");
			$ip = $_SERVER['REMOTE_ADDR'];


			$sqlr = "SELECT direccion,telefono,web,email FROM configbasica WHERE estado='S'";
			$resp = mysqli_query($linkbd,$sqlr);
			$row = mysqli_fetch_row($resp);
			$direcc = $row[0];
			$telefonos = $row[1];
			$dirweb = $row[3];
			$coemail = $row[2];
			if($direcc!=''){$vardirec="Dirección: $direcc, ";}
			else {$vardirec="";}
			if($telefonos!=''){$vartelef="Telefonos: $telefonos";}
			else{$vartelef="";}
			if($dirweb!=''){$varemail="Email: $dirweb, ";}
			else {$varemail="";}
			if($coemail!=''){$varpagiw="Pagina Web: $coemail";}
			else{$varpagiw="";}
			//$this->SetY(-16);
			$this->SetFont('helvetica', 'I', 8);
			$txt = <<<EOD
            $lema
            $vardirec $vartelef
            $varemail $varpagiw
            EOD;
            $this->SetFont('helvetica', 'I', 6);
			$this->Cell(277,10,'','T',0,'T');
			$this->ln(2);
			$this->Write(0, $txt , '', 0, 'C', true, 0, false, false, 0);

            $sqlrcc="SELECT * FROM `usuarios` WHERE `cc_usu` = '$_SESSION[cedulausu]'";
            $respcc=mysqli_query($linkbd, $sqlrcc);
            $rowcc=mysqli_fetch_row($respcc);
            //echo $rowcc[1];

			$this->Cell(50, 10, 'Hecho por: '.$user, 00, false, 'C', 0, '', 0, false, 'T', 'M');
			$this->Cell(60, 10, 'Impreso por: '.$user, 0, false, 'C', 0, '', 0, false, 'T', 'M');
			$this->Cell(50, 10, 'IP: '.$ip, 0, false, 'C',0, '', 0, false, 'T', 'M');
			$this->Cell(40, 10, 'Fecha: '.$fecha, 0, false, 'C', 0, '', 0, false, 'T', 'M');
			$this->Cell(30, 10, 'IDEAL.10 S.A.S', 0, false, 'C', 0, '', 0, false, 'T', 'M');
            $this->Cell(30, 10, 'Pagina '.$this->getAliasNumPage().'/'.$this->getAliasNbPages(), 0, false, 'C', 0, '', 0, false, 'T', 'M');
		}
	}
	$pdf = new MYPDF('L','mm','LETTER', true, 'UTF-8', false);// create new PDF document
	$pdf->SetDocInfoUnicode (true);
	// set document information
	$pdf->SetCreator(PDF_CREATOR);
	$pdf->SetAuthor('G&CSAS');
	$pdf->SetTitle('Certificados');
	$pdf->SetSubject('Paz y salvo');
	$pdf->SetKeywords('TCPDF, PDF, example, test, guide');
	$pdf->SetMargins(10, 45, 10);// set margins
	$pdf->SetHeaderMargin(45);// set margins
	$pdf->SetFooterMargin(20);// set margins
	$pdf->SetAutoPageBreak(TRUE, 20);// set auto page breaks
	// set some language-dependent strings (optional)
	if (@file_exists(dirname(__FILE__).'/lang/spa.php'))
	{
		require_once(dirname(__FILE__).'/lang/spa.php');
		$pdf->setLanguageArray($l);
	}
	$linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");

	$codUsuario = $_GET["cod"];
	$sqlUsuario = "SELECT id_tercero, id FROM srvclientes WHERE cod_usuario = '$codUsuario'";
	$rowUsuario = mysqli_fetch_assoc(mysqli_query($linkbd, $sqlUsuario));

	$sqlDireccion = "SELECT direccion FROM srvdireccion_cliente WHERE id_cliente = $rowUsuario[id]";
	$rowDireccion = mysqli_fetch_assoc(mysqli_query($linkbd, $sqlDireccion));
	
	$documento = buscaDocumentoTerceroConId($rowUsuario["id_tercero"]);
	$nombre = buscaNombreTerceroConId($rowUsuario["id_tercero"]);
	$direccion = $rowDireccion["direccion"];

	$sqlNameAlm = "SELECT funcionario, nomcargo FROM firmaspdf_det WHERE idfirmas='7' AND estado ='S'";
	$rowNameAlm = mysqli_fetch_assoc(mysqli_query($linkbd, $sqlNameAlm));
	$funcionario = $rowNameAlm["funcionario"];
	$cargo = $rowNameAlm["nomcargo"];

	// ---------------------------------------------------------
	$pdf->AddPage();
    $pdf->Cell(254,2,"LA ENTIDAD DE SERVICIOS PÚBLICOS CERTIFICA",'U',0,'C');
	$pdf->ln();$pdf->ln();
	$html = "
	<table>
		<tr>
			<td  style=\"width: 100%; font-size:12px; font-weight:normal; text-align: justify; text-justify: inter-word; \"><label>Que, revisada la base de datos de servicios púbicos de este Municipio, La persona $nombre, identificado con el número de documento No.$documento, con el predio ubicado en la dirección $direccion se encuentra a paz y salvo por concepto del servicio de acueducto, alcantarillado y aseo.</label>
			</td>
		</tr>
	</table>";
	$pdf->writeHTML($html, true, false, true, false,'');

	
	$pdf->ln();
	$v = $pdf->gety();
	
	$pdf->SetFont('helvetica','',12);

	$rete = "el día " . date("d");
    $mes = date("m");
    $año = date("Y");
    $dmesl1 = mesletras((int)$mes);

	$pdf->Cell(80,5,"Lo anterior se expide $rete del mes de $dmesl1 de $año",0,1,'L',0,'',0);

	$pdf->ln();$pdf->ln();$pdf->ln();$pdf->ln();
	
    $pdf->SetFont('helvetica','',8);
    $pdf->Cell(250,5,"QUIEN CERTIFICA",0,0,'C',0,'',0);
    $pdf->ln(20);
    $v = $pdf->gety();
    $pdf->Line(85,$v,180,$v);

    $pdf->SetFont('helvetica','B',8);
    $pdf->Cell(250,4,$funcionario,0,1,'C',false,0,0,false,'T','C');
    $pdf->SetFont('helvetica','',8);
    $pdf->Cell(250,4,$cargo,0,0,'C',false,0,0,false,'T','C');


	$pdf->Output('Acta de Recibo.pdf', 'I');
?>


