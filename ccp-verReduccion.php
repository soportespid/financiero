<?php
	header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=iso-8859-1");
	require 'comun.inc';
	require 'funciones.inc';
	require 'validaciones.inc';
	require 'conversor.php';
	session_start();
	$linkbd = conectar_v7();
	cargarcodigopag($_GET['codpag'],$_SESSION["nivel"]);
	date_default_timezone_set("America/Bogota");
?>
<!DOCTYPE >
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="es">
	<head>
		<meta http-equiv="Content-type" content="text/html; charset=iso-8859-1"/>
		<meta http-equiv="X-UA-Compatible" content="IE=9"/>
		<title>IDEAL 10 - Planeaci&oacute;n Estrat&eacute;gica</title>
		<link href="css/css2.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<link href="css/css4.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<link href="css/tabs.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css"/>
		<script src="vue/vue.js"></script>
		<script type="text/javascript" src="css/programas.js?<?php echo date('d_m_Y_h_i_s');?>"> </script>
		<style>
			.modal-mask
			{
				position: fixed;
				z-index: 9998;
				top: 0;
				left: 0;
				width: 100%;
				height: 100%;
				background-color: rgba(0, 0, 0, .5);
				display: table;
				transition: opacity .3s ease;
			}
			.modal-wrapper 
			{
				display: table-cell;
				vertical-align: middle;
			}
			.modal-container
			{
				width: 60%;
				margin: 0px auto;
				padding: 20px 30px;
				text-align: left;
				background:linear-gradient(#99bbcc, #B6CEDA);
				border-radius: 2px;
				box-shadow: 0 2px 8px rgba(0, 0, 0, .33);
				transition: all .3s ease;
			}
			.modal-container1
			{
				width: 50%;
				margin: 0px auto;
				padding: 20px 30px;
				text-align: left;
				background:linear-gradient(#99bbcc, #B6CEDA);
				border-radius: 10px;
				box-shadow: 0 2px 8px rgba(0, 0, 0, .33);
				transition: all .3s ease;
			}
			.modal-container2
			{
				width: 80%;
				margin: 0px auto;
				padding: 20px 30px;
				text-align: left;
				background:linear-gradient(#99bbcc, #B6CEDA);
				border-radius: 2px;
				box-shadow: 0 2px 8px rgba(0, 0, 0, .33);
				transition: all .3s ease;
			}
			.modal-container3
			{
				width: 90%;
				margin: 0px auto;
				padding: 20px 30px;
				text-align: left;
				background:linear-gradient(#99bbcc, #B6CEDA);
				border-radius: 2px;
				box-shadow: 0 2px 8px rgba(0, 0, 0, .33);
				transition: all .3s ease;
			}
			footer
			{
				text-align: right;
			}

			/* Agrandar checkbox */
			input[type=checkbox]
			{
				/*Navegadores para soportar*/
				-ms-transform: scale(2); /* IE */
				-moz-transform: scale(2); /* FF */
				-webkit-transform: scale(2); /* Safari y Chrome */
				-o-transform: scale(2); /* Opera */
				padding: 10px;
			}
		</style>
		<?php titlepag();?>
	</head>
	<body>
		<div id="myapp">
			<table>
				<tr><script>barra_imagenes("ccpet");</script><?php cuadro_titulos();?></tr>
				<tr><?php menu_desplegable("ccpet");?></tr>
				<tr>
					<td colspan="3" class="cinta">
                        <img src="imagenes/add.png" onClick="location.href='ccp-reduccion.php'" class="mgbt" title="Nuevo" />
                        <img src="imagenes/guardad.png" title="Guardar" v-on:click="" class="mgbt"/>
                        <img src="imagenes/busca.png" onClick="location.href='ccp-buscaReduccion.php'" class="mgbt" title="Buscar"/>
                        <img src="imagenes/nv.png" onClick="mypop=window.open('ccp-principal.php','',''); mypop.focus();" class="mgbt" title="Nueva Ventana">
					</td>
				</tr>
			</table>
				<?php
					if(@ $_POST['oculto']=="")
					{
						$_POST['tabgroup1']=1;
					}
					switch($_POST['tabgroup1'])
					{
						case 1:
							$check1='checked';break;
						case 2:
							$check2='checked';break;
						case 3:
							$check3='checked';break;
						case 4:
							$check4='checked';break;
						case 5:
							$check5='checked';break;
					}
                    if(@ $_POST['oculto']=="")
					{
						$_POST['tabgroup3']=1;
					}
                    switch($_POST['tabgroup3'])
					{
						case 1:
							$checked1='checked';break;
						case 2:
							$checked2='checked';break;
						case 3:
							$checked3='checked';break;
						case 4:
							$checked4='checked';break;
					}
					switch($_POST['tabgroup4'])
					{
						case 1:
							$chek1='checked';break;
						case 2:
							$chek2='checked';break;
						case 3:
							$chek3='checked';break;
						case 4:
							$chek4='checked';break;
					}
				?>
                <div class="tabsmeci" style="height:77%; width:99.6%" >
                    <div class="tab">
                        <input type="radio" id="tab-1" name="tabgroup1" value="1" <?php echo $check1;?> >
                        <label for="tab-1">Reducciones</label>
                        <div class="content" style="overflow:hidden;">
                            <table class="inicio ancho">
                                <tr>
                                    <td class="titulos" colspan="6">Reducciones</td>
                                    <td class="cerrar" style="width:7%" onClick="location.href='plan-principal.php'">Cerrar</td>
                                </tr>
                                
                                <tr style="color: red;">
									<td style="width: 100px;" class="tamano01">Reducci&oacute;n Finalizada</td>
                                </tr>
                            </table>
                            
                            <div class="tabsmeci" style="width:99.8%" >
                                <div class="tab" >
                                    <input type="radio" id="tab-1g" name="tabgroupMostrar" v-model="tabgroupMostrar" v-bind:value="1" >
                                    <label for="tab-1g">Presupuesto Gastos</label>
                                    <div class="content" style="overflow-x:hidden;" v-bind:style="{ height: tabheight10 }">
                                        <table class='inicio inicio--no-shadow'>
                                            <tbody>
                                                <tr class="titulos">
                                                    <td>Bpin</td>
                                                    <td>Rubro</td>
                                                    <td>Fuente</td>
                                                    <td>Vigencia del Gasto</td>
                                                    <td>Politica Publica</td>
                                                    <td>Meta</td>
                                                    <td>Medio de Pago</td>
                                                    <td>Cuin/Bienes/Servicios</td>
                                                    <td>Valor Gasto</td>
                                                </tr>
                                                <tr v-for="(vccuenta, index) in presupuestoTotalGastos" v-bind:class="index % 2 ? 'saludo1a' : 'saludo2'" style='text-rendering: optimizeLegibility; cursor: pointer !important; style=\"cursor: hand\"' >
                                                    <td style="font: 120% sans-serif; padding-left:10px;">{{ vccuenta[0] }}</td>
                                                    <td style="font: 120% sans-serif; padding-left:10px;">{{ vccuenta[1] }}</td>
                                                    <td style="font: 120% sans-serif; padding-left:10px;">{{ vccuenta[2] }}</td>
                                                    <td style="font: 120% sans-serif; padding-left:10px;">{{ vccuenta[3] }}</td>
                                                    <td style="font: 120% sans-serif; padding-left:10px;">{{ vccuenta[4] }}</td>
                                                    <td style="font: 120% sans-serif; padding-left:10px;">{{ vccuenta[5] }}</td>
                                                    <td style="font: 120% sans-serif; padding-left:10px;">{{ vccuenta[6] }}</td>
                                                    <td style="font: 120% sans-serif; padding-left:10px;">{{ vccuenta[7] }}</td>
                                                    <td style="font: 120% sans-serif; padding-left:10px;">{{ vccuenta[8] }}</td>
                                                </tr>		
												<tr style="background-color: #76FDA6">
													<td colspan='8' style="font:150% sans-serif; padding-left:30px; font-weight: bold">Total Ingresos</td>
													<td style="font: 120% sans-serif; padding-left:10px;">{{ valorTotalGastos }}</td>
												</tr>							
                                            </tbody>
                                        </table>
                                    </div>	
                                </div>
                                <div class="tab" >
                                    <input type="radio" id="tab-2g" name="tabgroupMostrar" v-model="tabgroupMostrar" v-bind:value="2" >
                                    <label for="tab-2g">Presupuesto Ingresos</label>
                                    <div class="content" style="overflow-x:hidden;" v-bind:style="{ height: tabheight10 }">
                                        <table class='inicio inicio--no-shadow'>
                                            <tbody>
                                                <tr class="titulos">
                                                    
                                                    <td>Rubro</td>
                                                    <td>Fuente</td>
                                                    <td>Vigencia del Gasto</td>
													<td>Politica Publica</td>
                                                    <td>Cuin/Bienes/Servicios</td>
                                                    <td>Valor Ingresos</td>
                                                </tr>
                                                <tr v-for="(vccuenta, index) in presupuestoTotalIngresos" v-bind:class="index % 2 ? 'saludo1a' : 'saludo2'" style='text-rendering: optimizeLegibility; cursor: pointer !important; style=\"cursor: hand\"' >
                                                    
                                                    <td style="font: 120% sans-serif; padding-left:10px;">{{ vccuenta[0] }}</td>
                                                    <td style="font: 120% sans-serif; padding-left:10px;">{{ vccuenta[1] }}</td>
                                                    <td style="font: 120% sans-serif; padding-left:10px;">{{ vccuenta[2] }}</td>
													<td style="font: 120% sans-serif; padding-left:10px;">{{ vccuenta[3] }}</td>
                                                    <td style="font: 120% sans-serif; padding-left:10px;">{{ vccuenta[4] }}</td>
                                                    <td style="font: 120% sans-serif; padding-left:10px;">{{ vccuenta[5] }}</td>
                                                </tr>									
												<tr style="background-color: #76FDA6">
													<td colspan='5' style="font:150% sans-serif; padding-left:30px; font-weight: bold">Total Ingresos</td>
													<td style="font: 120% sans-serif; padding-left:10px;">{{ valorTotalIngresos }}</td>
												</tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- termina -->

				
				<input type="hidden" v-model="acuerdo" :value="acuerdo=<?php echo $_GET['is']; ?>"/>
			</div>
		</div>
		<script src="Librerias/vue/vue.min.js"></script>
		<script src="Librerias/vue/axios.min.js"></script>
		<script src="vue/presupuesto_ccp/ccp-verReduccion.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
	</body>
</html>