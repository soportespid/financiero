<?php
header("Cache-control: no-cache, no-store, must-revalidate");
header("Content-Type: text/html;charset=utf8");

require 'comun.inc';
require 'funciones.inc';

$linkbd = conectar_v7();
$linkbd->set_charset("utf8");

session_start();
date_default_timezone_set("America/Bogota");

$scroll = $_GET['scrtop'];
$totreg = $_GET['totreg'];
$idcta = $_GET['idcta'];
$altura = $_GET['altura'];
?>
<!DOCTYPE>
<html lang="es">

<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1" />
    <meta name="viewport" content="user-scalable=no">
    <title>:: IDEAL 10 - Parametrización</title>
    <link href="favicon.ico" rel="shortcut icon" />
    <link href="css/css2.css" rel="stylesheet" type="text/css" />
    <link href="css/css3.css" rel="stylesheet" type="text/css" />
    <link href="css/style.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="css/programas.js"></script>
    <script>
        function guardar() {
            var validacion01 = document.getElementById('nombre').value;
            if (validacion01.trim() != '') { despliegamodalm('visible', '4', 'Esta Seguro de Modificar', '1'); }
            else { despliegamodalm('visible', '2', 'Falta informacion para Crear el Comprobante'); }
        }
        function despliegamodalm(_valor, _tip, mensa, pregunta) {
            document.getElementById("bgventanamodalm").style.visibility = _valor;
            if (_valor == "hidden") { document.getElementById('ventanam').src = ""; }
            else {
                switch (_tip) {
                    case "1":
                        document.getElementById('ventanam').src = "ventana-mensaje1.php?titulos=" + mensa; break;
                    case "2":
                        document.getElementById('ventanam').src = "ventana-mensaje3.php?titulos=" + mensa; break;
                    case "3":
                        document.getElementById('ventanam').src = "ventana-mensaje2.php?titulos=" + mensa; break;
                    case "4":
                        document.getElementById('ventanam').src = "ventana-consulta1.php?titulos=" + mensa + "&idresp=" + pregunta; break;
                }
            }
        }
        function funcionmensaje() { }
        function respuestaconsulta(pregunta) {
            switch (pregunta) {
                case "1":
                    document.form2.oculto.value = "2";
                    document.form2.submit();
                    break;
            }
        }
    </script>
    <script>
        function iratras(scrtop, numpag, limreg) {
            var idcta = document.getElementById('idcomp').value;
            location.href = "cont-buscatipodoc.php?idcta=" + idcta + "&scrtop=" + scrtop + "&numpag=" + numpag + "&limreg=" + limreg;
        }
    </script>
</head>

<body>
    <IFRAME src="alertas.php" name="alertas" id="alertas" style="display:none"></IFRAME>
    <span id="todastablas2"></span>
    <?php
    $numpag = $_GET['numpag'];
    $limreg = $_GET['limreg'];
    $scrtop = 26 * $totreg;
    ?>
    <table>
        <tr>
            <script>barra_imagenes("para");</script><?php cuadro_titulos(); ?>
        </tr>
        <tr>
            <?php menu_desplegable("para"); ?>
        </tr>
    </table>
    <div class="bg-white group-btn p-1"><button type="button" onclick="window.location.href='cont-tipodoc.php'"
            class="btn btn-white btn-primary-hover d-flex justify-between align-items-center">
            <span>Nuevo</span>
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 -960 960 960">
                <path d="M440-440H200v-80h240v-240h80v240h240v80H520v240h-80v-240Z"></path>
            </svg>
        </button><button type="button" onclick="guardar();"
            class="btn btn-white btn-primary-hover d-flex justify-between align-items-center">
            <span>Guardar</span>
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 -960 960 960">
                <path
                    d="M840-680v480q0 33-23.5 56.5T760-120H200q-33 0-56.5-23.5T120-200v-560q0-33 23.5-56.5T200-840h480l160 160Zm-80 34L646-760H200v560h560v-446ZM480-240q50 0 85-35t35-85q0-50-35-85t-85-35q-50 0-85 35t-35 85q0 50 35 85t85 35ZM240-560h360v-160H240v160Zm-40-86v446-560 114Z">
                </path>
            </svg>
        </button><button type="button" onclick="window.location.href='cont-buscatipodoc.php'"
            class="btn btn-white btn-primary-hover d-flex justify-between align-items-center">
            <span>Buscar</span>
            <svg class="fill-black group-hover:fill-white w-5 h-5" xmlns="http://www.w3.org/2000/svg"
                viewBox="0 -960 960 960">
                <path
                    d="M784-120 532-372q-30 24-69 38t-83 14q-109 0-184.5-75.5T120-580q0-109 75.5-184.5T380-840q109 0 184.5 75.5T640-580q0 44-14 83t-38 69l252 252-56 56ZM380-400q75 0 127.5-52.5T560-580q0-75-52.5-127.5T380-760q-75 0-127.5 52.5T200-580q0 75 52.5 127.5T380-400Z">
                </path>
            </svg>
        </button><button type="button" onclick="mypop=window.open('para-principal.php','','');mypop.focus();"
            class="btn btn-white btn-primary-hover d-flex justify-between align-items-center">
            <span>Nueva ventana</span>
            <svg class="fill-black group-hover:fill-white w-5 h-5" xmlns="http://www.w3.org/2000/svg"
                viewBox="0 -960 960 960">
                <path
                    d="M320-120v-80h80v-80H160q-33 0-56.5-23.5T80-360v-400q0-33 23.5-56.5T160-840h640q33 0 56.5 23.5T880-760v400q0 33-23.5 56.5T800-280H560v80h80v80H320ZM160-360h640v-400H160v400Zm0 0v-400 400Z">
                </path>
            </svg>
        </button><button type="button" onclick="mypop=window.open('plan-agenda.php','','');mypop.focus()"
            class="btn btn-white btn-primary-hover d-flex justify-between align-items-center">
            <span class="group-hover:text-white">Agenda</span>
            <svg class="fill-black group-hover:fill-white w-5 h-5" xmlns="http://www.w3.org/2000/svg"
                viewBox="0 -960 960 960">
                <path
                    d="M560-564v-68q33-14 67.5-21t72.5-7q26 0 51 4t49 10v64q-24-9-48.5-13.5T700-600q-38 0-73 9.5T560-564Zm0 220v-68q33-14 67.5-21t72.5-7q26 0 51 4t49 10v64q-24-9-48.5-13.5T700-380q-38 0-73 9t-67 27Zm0-110v-68q33-14 67.5-21t72.5-7q26 0 51 4t49 10v64q-24-9-48.5-13.5T700-490q-38 0-73 9.5T560-454ZM260-320q47 0 91.5 10.5T440-278v-394q-41-24-87-36t-93-12q-36 0-71.5 7T120-692v396q35-12 69.5-18t70.5-6Zm260 42q44-21 88.5-31.5T700-320q36 0 70.5 6t69.5 18v-396q-33-14-68.5-21t-71.5-7q-47 0-93 12t-87 36v394Zm-40 118q-48-38-104-59t-116-21q-42 0-82.5 11T100-198q-21 11-40.5-1T40-234v-482q0-11 5.5-21T62-752q46-24 96-36t102-12q58 0 113.5 15T480-740q51-30 106.5-45T700-800q52 0 102 12t96 36q11 5 16.5 15t5.5 21v482q0 23-19.5 35t-40.5 1q-37-20-77.5-31T700-240q-60 0-116 21t-104 59ZM280-494Z">
                </path>
            </svg>
        </button><button type="button" onclick="iratras(104, 1, 10)"
            class="btn btn-success btn-success-hover d-flex justify-between align-items-center">
            <span>Atras</span>
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 -960 960 960">
                <path
                    d="M760-200v-160q0-50-35-85t-85-35H273l144 144-57 56-240-240 240-240 57 56-144 144h367q83 0 141.5 58.5T840-360v160h-80Z">
                </path>
            </svg>
        </button></div>
    <div id="bgventanamodalm" class="bgventanamodalm">
        <div id="ventanamodalm" class="ventanamodalm">
            <IFRAME src="" name="ventanam" marginWidth=0 marginHeight=0 frameBorder=0 id="ventanam" frameSpacing=0
                style=" width:700px; height:130px; top:200; overflow:hidden;">
            </IFRAME>
        </div>
    </div>
    <?php
    if (!$_POST['oculto'] == "2") {
        $sqlr = "Select *from tipo_comprobante where id_tipo=$_GET[idtipocom]";
        $res = mysqli_query($linkbd, $sqlr);
        while ($row = mysqli_fetch_row($res)) {
            $_POST['idcomp'] = $row[0];
            $_POST['nombre'] = $row[1];
            $_POST['estado'] = $row[2];
            $_POST['codigo'] = $row[3];
            $_POST['fijo'] = $row[4];
            $_POST['categoria'] = $row[5];
            $_POST['tipomov'] = $row[6];
        }
    }
    ?>
    <form name="form2" method="post" action="cont-editartipodoc.php">
        <table class="inicio" align="center">
            <tr>
                <td class="titulos" colspan="6">.: Agregar Tipo Comprobante</td>
                <td class="cerrar"><a href="cont-principal.php">Cerrar</a></td>
            </tr>
            <tr>
                <td class="saludo1" style="width:8%;">.: C&oacute;digo:</td>
                <td style="width:10%;"><input type="text" name="codigo" id="codigo"
                        value="<?php echo $_POST['codigo'] ?>" style="width:99%;" onBlur="valcodigo();" readonly></td>
                <td class="saludo1" style="width:17%;">.: Nombre Comprobante:</td>
                <td><input type="text" name="nombre" id="nombre" value="<?php echo $_POST['nombre'] ?>"
                        style="width:99.5%"></td>
                <td class="saludo1" style="width:8%;">.: Activo:</td>
                <td style="width:15%;">
                    <select name="estado" id="estado" style="width:100%;">
                        <option value="S" <?php if ($_POST['estado'] == 'S') {
                            echo "SELECTED";
                        } ?>>SI</option>
                        <option value="N" <?php if ($_POST['estado'] == 'N') {
                            echo "SELECTED";
                        } ?>>NO</option>
                    </select>
                </td>
            </tr>
            <tr>
                <td class="saludo1" colspan="4" style="background-color: white"></td>
                <td class="saludo1" style="width:8%;">.: Categoria:</td>
                <td style="width:15%;">
                    <select name="categoria" id="categoria" style="width:100%;">
                        <option value="">.: Seleccione la categoria</option>
                        <?php
                        $sql = "select * from categoria_compro WHERE estado='S' ORDER BY id";
                        $result = mysqli_query($linkbd, $sql);
                        while ($row = mysqli_fetch_array($result)) {
                            if ($_POST['categoria'] == $row[0]) {
                                echo "<option value='$row[0]' selected>$row[1]</option>";
                            } else {
                                echo "<option value='$row[0]'>$row[1]</option>";
                            }

                        }
                        ?>

                    </select>
                </td>
            </tr>
            <tr>
                <td class="saludo1" colspan="4" style="background-color: white"></td>

                <td class="saludo1" style="width:8%;">.: Tipo Movimiento:</td>

                <td style="width:15%;">
                    <select name="tipomov" id="tipomov" style="width:100%;">
                        <option value="1" <?php if ($_POST['tipomov'] == '0')
                            echo "SELECTED"; ?>>Seleccione movimiento
                        </option>
                        <option value="1" <?php if ($_POST['tipomov'] == '1')
                            echo "SELECTED"; ?>>1 - Entrada </option>
                        <option value="2" <?php if ($_POST['tipomov'] == '2')
                            echo "SELECTED"; ?>>2 - Salida </option>
                    </select>
                </td>
            </tr>
        </table>
        <input name="oculto" id="oculto" type="hidden" value="1">
        <input name="idcomp" id="idcomp" type="hidden" value="<?php echo $_POST['idcomp'] ?>">
        <?php
        if ($_POST['oculto']) {
            $sqlr = "update tipo_comprobante set nombre='$_POST[nombre]',estado='$_POST[estado]',codigo='$_POST[codigo]',id_cat=$_POST[categoria], tipo_movimiento = '$_POST[tipomov]' where id_tipo=$_POST[idcomp]";
            if (!mysqli_query($linkbd, $sqlr)) {
                echo "<script>despliegamodalm('visible','2','No se pudo ejecutar la petici�n');</script>";
            } else {
                echo "<script>despliegamodalm('visible','3','Se ha actualizado con Exito');</script>";
            }
        }
        ?>
    </form>
</body>

</html>
