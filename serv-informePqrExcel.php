<?php
	require_once 'PHPExcel/Classes/PHPExcel.php';
    include '/PHPExcel/Classes/PHPExcel/IOFactory.php';
	require "comun.inc";
	require "funciones.inc";
    require "Librerias/core/Helpers.php";
    ini_set('max_execution_time',99999999);
    header("Content-type: application/json");
	session_start();
	$linkbd=conectar_v7();

	$servicio = $_POST["servicio"];

	$objPHPExcel = new PHPExcel();
	
	$objPHPExcel->getProperties()
		->setCreator("IDEAL10")
		->setLastModifiedBy("IDEAL10")
		->setTitle("Reporte de respuestas PQR")
		->setSubject("SP")
		->setDescription("SP")
		->setKeywords("SP")
		->setCategory("SERVICIOS PUBLICOS");
	$objPHPExcel->getActiveSheet()->mergeCells('A1:P1');
	$objPHPExcel->setActiveSheetIndex(0)->setCellValue('A1', "Reporte PQR $servicio");
	$objFont=$objPHPExcel->getActiveSheet()->getStyle('A1')->getFont();
	$objFont->setName('Courier New');
	$objFont->setSize(15);
	$objFont->setBold(true);
	$objFont->setUnderline(PHPExcel_Style_Font::UNDERLINE_SINGLE);
	$objFont->getColor()->setARGB( PHPExcel_Style_Color::COLOR_BLACK);
	$objAlign=$objPHPExcel->getActiveSheet()->getStyle('A1')->getAlignment(); 
	$objAlign->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
	$objAlign->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
	$objPHPExcel-> getActiveSheet ()
		-> getStyle ("A2:P2")
		-> getFill ()
		-> setFillType (PHPExcel_Style_Fill :: FILL_SOLID)
		-> getStartColor ()
		-> setRGB ('A6E5F3');
	$objPHPExcel-> getActiveSheet ()
		-> getStyle ("A1")	
		-> getFill ()
		-> setFillType (PHPExcel_Style_Fill :: FILL_SOLID)
		-> getStartColor ()
		-> setRGB ('A6E5F3');
	$borders = array(
		'borders' => array(
			'allborders' => array(
				'style' => PHPExcel_Style_Border::BORDER_THIN,
				'color' => array('argb' => 'FF000000'),
			)
		),
	);

    $data = json_decode($_POST['data'], true);
	$objPHPExcel->getActiveSheet()->getStyle('A2:P2')->applyFromArray($borders);
	$objPHPExcel->setActiveSheetIndex(0)
        ->setCellValue('A2', 'Dane dpto')
        ->setCellValue('B2', 'Dane mnpio')
        ->setCellValue('C2', 'Centro poblado')
        ->setCellValue('D2', 'Radicado recibido')
        ->setCellValue('E2', 'Fecha radicación')
        ->setCellValue('F2', 'Tipo tramite')
		->setCellValue('G2', 'Causal')
		->setCellValue('H2', 'Detalle causal')
        ->setCellValue('I2', 'NUID')
        ->setCellValue('J2', 'Número factura')
        ->setCellValue('K2', 'Tipo respuesta')
        ->setCellValue('L2', 'Fecha respuesta')
        ->setCellValue('M2', 'Radicación respuesta')
        ->setCellValue('N2', 'Fecha notificación')
        ->setCellValue('O2', 'Tipo notificación')
        ->setCellValue('P2', 'Fecha traslado SSPD');
        
	$i=3;
    
	foreach ($data as $key => $d) {
        $estado = $d["estado"] == 'S' ? "Activo" : "Anulado";
		$objPHPExcel->setActiveSheetIndex(0)
		->setCellValueExplicit ("A$i", $d["dpto"], PHPExcel_Cell_DataType :: TYPE_STRING)
		->setCellValueExplicit ("B$i", $d["mnpio"], PHPExcel_Cell_DataType :: TYPE_STRING)
		->setCellValueExplicit ("C$i", $d["centroPoblado"], PHPExcel_Cell_DataType :: TYPE_STRING)
		->setCellValueExplicit ("D$i", $d["consecutivoRadicado"], PHPExcel_Cell_DataType :: TYPE_STRING)
		->setCellValueExplicit ("E$i", $d["fechaRadicado"], PHPExcel_Cell_DataType :: TYPE_STRING)
		->setCellValueExplicit ("F$i", $d["tipoTramite"], PHPExcel_Cell_DataType :: TYPE_STRING)
		->setCellValueExplicit ("G$i", $d["causal"], PHPExcel_Cell_DataType :: TYPE_STRING)
		->setCellValueExplicit ("H$i", $d["detalleCausal"], PHPExcel_Cell_DataType :: TYPE_STRING)
		->setCellValueExplicit ("I$i", $d["NUID"], PHPExcel_Cell_DataType :: TYPE_STRING)
		->setCellValueExplicit ("J$i", $d["numFactura"], PHPExcel_Cell_DataType :: TYPE_STRING)
		->setCellValueExplicit ("K$i", $d["tipoRespuesta"], PHPExcel_Cell_DataType :: TYPE_STRING)
		->setCellValueExplicit ("L$i", $d["fechaRespuesta"], PHPExcel_Cell_DataType :: TYPE_STRING)
		->setCellValueExplicit ("M$i", $d["consecutivoRespuesta"], PHPExcel_Cell_DataType :: TYPE_STRING)
		->setCellValueExplicit ("N$i", $d["fechaRespuesta"], PHPExcel_Cell_DataType :: TYPE_STRING)
		->setCellValueExplicit ("O$i", $d["tipoNotificacion"], PHPExcel_Cell_DataType :: TYPE_STRING)
		->setCellValueExplicit ("P$i", "", PHPExcel_Cell_DataType :: TYPE_STRING)
		;

        $objPHPExcel->getActiveSheet()->getStyle("A$i:P$i")->applyFromArray($borders);

        $i++;
	}


	$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
    $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
    $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
    $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
    $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);
    $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setAutoSize(true);
    $objPHPExcel->getActiveSheet()->getColumnDimension('I')->setAutoSize(true);
    $objPHPExcel->getActiveSheet()->getColumnDimension('J')->setAutoSize(true);
    $objPHPExcel->getActiveSheet()->getColumnDimension('K')->setAutoSize(true);
    $objPHPExcel->getActiveSheet()->getColumnDimension('L')->setAutoSize(true);
    $objPHPExcel->getActiveSheet()->getColumnDimension('M')->setAutoSize(true);
    $objPHPExcel->getActiveSheet()->getColumnDimension('N')->setAutoSize(true);
    $objPHPExcel->getActiveSheet()->getColumnDimension('O')->setAutoSize(true);
    $objPHPExcel->getActiveSheet()->getColumnDimension('P')->setAutoSize(true);

	$objPHPExcel->getActiveSheet()->setTitle('SP');
	$name = "reporte_pqr_$servicio.xlsx";
	//header('Content-Type: application/vnd.ms-excel');
    header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
	header('Content-Disposition: attachment;filename="'.$name.'"');
	header('Cache-Control: max-age=0');
	$objWriter=PHPExcel_IOFactory::createWriter($objPHPExcel,'Excel2007');
	$objWriter->save('php://output');
	exit;
?>