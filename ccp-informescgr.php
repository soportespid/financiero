<?php //V 1000 12/12/16 ?> 
<?php
	ini_set('max_execution_time',3600);
	require "comun.inc";
	require "funciones.inc";
	require "validaciones.inc";
	session_start();
		
	$linkbd_V7 = conectar_v7();	
	cargarcodigopag($_GET['codpag'],$_SESSION["nivel"]);
	header("Cache-control: private"); // Arregla IE 6
	date_default_timezone_set("America/Bogota");

	$maxVersion = ultimaVersionIngresosCCPET();
	$maxVersionGastos = ultimaVersionGastosCCPET();
?>
<html lang="es">
	<head>
		<meta charset="UTF-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
    	<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<title>:: IDEAL 10 - Presupuesto</title>

		<link href="css/css2.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<link href="css/css4.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />

        <script type="text/javascript" src="css/calendario.js"></script>
		<script type="text/javascript" src="css/programas.js"></script>
        <script>
			$(window).load(function () { $('#cargando').hide();});

        </script>
		<?php titlepag();?>
    </head>
    <body>
        <IFRAME src="alertas.php" name="alertas" id="alertas" style="display:none"></IFRAME>
        <span id="todastablas2"></span>
        <table>
            <tr><script>barra_imagenes("ccpet");</script><?php cuadro_titulos();?></tr>	 
            <tr><?php menu_desplegable("ccpet");?></tr>
        	<tr>
				<?php
					$informes=array();
					$informes[1]="Relacion_de_CDPs";
					$informes[2]="Relacion_de_compromisos";
					$informes[3]="Relacion_de_obligaciones";
					$informes[4]="Relacion_de_pagos";
					$informes[5]="Ejecucion_presupuestal_de_ingresos";
					$informes[6]="Ejecucion_presupuestal_de_gastos";
					$informes[7]="Ejecucion_presupuestal_de_gastos_2018_2019_2020";
					$informes[8]="Relacion_de_ingresos";
					$informes[9]="Ejecucion_presupuestal_de_gastos_2022";
					

					$vigenciasDelGasto = array();
					$vigenciasDelGasto[1] = "Actual";
					$vigenciasDelGasto[2] = "Reservas";
					$vigenciasDelGasto[3] = "CxP";

					$tipoCuenta = array();
					$tipoCuenta['2.1'] = "Funcionamiento";
					$tipoCuenta['2.2'] = "Servicio a la deuda publica";
					$tipoCuenta['2.3'] = "Inversion";

					$tipoCuentaPpto = array();
					$tipoCuentaPpto['funcionamiento'] = "Funcionamiento";
					$tipoCuentaPpto['deuda'] = "Servicio a la deuda publica";
					$tipoCuentaPpto['inversion'] = "Inversion";

				?>
				<td colspan="3" class="cinta">
					<a href="#" class="mgbt"><img src="imagenes/add.png" title="Nuevo" /></a>
					<a href="#" class="mgbt" onClick="document.form2.submit();">
						<img src="imagenes/guarda.png" title="Guardar"/>
					</a>
					<a href="#" onClick="document.form2.submit()" class="mgbt">
						<img src="imagenes/busca.png" title="Buscar" />
					</a>
					<a href="#" onClick="mypop=window.open('ccp-principal.php','','');mypop.focus();" class="mgbt">
						<img src="imagenes/nv.png" title="Nueva Ventana">
					</a>
					<a href="#" onClick="pdf()" class="mgbt">
						<img src="imagenes/print.png" title="imprimir">
					</a>
					<a href="<?php echo "archivos/FORMATO_".$informes[$_POST['reporte']].".csv"; ?>" download = "<?php echo "archivos/FORMATO_".$informes[$_POST['reporte']].".csv"; ?>" target="_blank" class="mgbt">
						<img src="imagenes/csv.png" title="csv">
					</a>
				</td>
			</tr>
		</table>
        <form name="form2" method="post" action="ccp-informescgr.php">
            <div class="loading" id="divcarga"><span>Cargando...</span></div>
            <table  align="center" class="inicio">
                <tr >
                    <td class="titulos" colspan="7">.: Reportes CGR</td>
                    <td width="74" class="cerrar"><a href="presu-principal.php">Cerrar</a></td>
                </tr>
                <tr>
                    <td class="saludo1">Reporte</td>
                    <td>
                        <select name="reporte" id="reporte">
                            <option value="-1">Seleccione ....</option>
                            <option value="1" <?php if($_POST['reporte']=='1') echo "selected" ?>>Relaci&oacute;n de CDPs</option>
                            <option value="2" <?php if($_POST['reporte']=='2') echo "selected" ?>>Relaci&oacute;n de Compromisos</option>
                            <option value="3" <?php if($_POST['reporte']=='3') echo "selected" ?>>Relaci&oacute;n de Obligaciones</option>
                            <option value="4" <?php if($_POST['reporte']=='4') echo "selected" ?>>Relaci&oacute;n de Pagos</option>
                            <option value="5" <?php if($_POST['reporte']=='5') echo "selected" ?>>Ejecuci&oacute;n presupuestal de ingresos</option>
                            <option value="6" <?php if($_POST['reporte']=='6') echo "selected" ?>>Ejecuci&oacute;n presupuestal de gastos</option>
                            <option value="7" <?php if($_POST['reporte']=='7') echo "selected" ?>>Ejecuci&oacute;n presupuestal de gastos 2018, 2019 y 2020</option>
							<option value="8" <?php if($_POST['reporte']=='8') echo "selected" ?>>Relaci&oacute;n de ingresos</option>
							<option value="9" <?php if($_POST['reporte']=='9') echo "selected" ?>>Ejecuci&oacute;n presupuestal de gastos</option>
                            
                        </select>
                    </td> 
                    <td class="saludo1" style="width:2.5cm;">Fecha inicial:</td>
       				<td style="width:10%;">
                       <input name="fecha"  type="text" value="<?php echo $_POST['fecha']?>" maxlength="10"  onKeyPress="javascript:return solonumeros(event)" onchange="" onKeyUp="return tabular(event,this)"  id="fc_1198971545" onKeyDown="mascara(this,'/',patron,true)" style="width:75%;height:30px;" title="DD/MM/YYYY" placeholder="DD/MM/YYYY"/>&nbsp;<img src="imagenes/calendario04.png" onClick="displayCalendarFor('fc_1198971545');" title="Calendario" class="icobut"/>
                    </td>
       				<td class="saludo1" style="width:2.5cm;">Fecha final:</td>
       				<td style="width:10%;">
                       <input name="fecha2" type="text" value="<?php echo $_POST['fecha2']?>" maxlength="10"  onKeyPress="javascript:return solonumeros(event)" onchange="" onKeyUp="return tabular(event,this)"  id="fc_1198971546" onKeyDown="mascara(this,'/',patron,true)" style="width:75%;height:30px;" title="DD/MM/YYYY" placeholder="DD/MM/YYYY"/>&nbsp;<img src="imagenes/calendario04.png" onClick="displayCalendarFor('fc_1198971546');" title="Calendario" class="icobut"/>
                    </td>

					<td style=" padding-bottom:0px">
						<em class="botonflecha" onClick="document.form2.submit()" >Buscar</em>
						<input type='hidden' name = "oculto" id = "oculto" value = "1">
					</td>

                </tr>      
            </table>
            
	        <div class="subpantallap" style="height:65.5%; width:99.6%;">
                <?php
				if($_POST['oculto']==""){echo"<script>document.getElementById('divcarga').style.display='none';</script>";}
                //**** para sacar la consulta del balance se necesitan estos datos ********

                $oculto = $_POST['oculto'];
                if($_POST['oculto'])
                {
                    $iter = 'zebra1';
                    $iter2 = 'zebra2';
                    switch($_POST['reporte'])
                    {
	                    case 1: //Relación de CDPs
                            
                            $crit1=" ";
                            $crit2=" ";
                            $crit3=" ";
                            $crit4=" ";
                            $crit5=" ";

                            $_POST['nominforme'] = "Relacion_de_CDPs";
                            $namearch = "archivos/FORMATO_".$informes[$_POST['reporte']].".csv";
                            $_POST['nombrearchivo'] = $namearch;
                            
                            $Descriptor1 = fopen($namearch,"w+"); 
                            fputs($Descriptor1,"Anio| Vigencia| NIT_Entidad| Nombre_Entidad| Cod_Cuenta| Fuente_Financiacion| Nombre_Cuenta| Macro_campo_nivel_agregado| Numero_CDP| Fecha_CDP| Valor_CDP| Detalle_CDP\r\n");
                            
                            $sqlr = '';
                            if ($_POST['fecha'] != "" && $_POST['fecha2'] != "" )
                            {
								preg_match("/([0-9]{2})\/([0-9]{2})\/([0-9]{4})/", $_POST['fecha'], $fecha);
								$fechai = $fecha[3]."-".$fecha[2]."-".$fecha[1];

								preg_match("/([0-9]{2})\/([0-9]{2})\/([0-9]{4})/", $_POST['fecha2'], $fecha);
								$fechaf = $fecha[3]."-".$fecha[2]."-".$fecha[1];


                            	$crit3 = " AND CDP.fecha BETWEEN '$fechai' AND '$fechaf' ";

								$sqlr = "SELECT CDP.vigencia, CDP_DET.cuenta, CDP_DET.fuente, CDP.consvigencia, CDP.fecha, SUM(CDP_DET.valor), CDP.objeto, CDP_DET.codigo_vigenciag FROM ccpetcdp AS CDP, ccpetcdp_detalle AS CDP_DET WHERE CDP.vigencia = CDP_DET.vigencia AND CDP.consvigencia = CDP_DET.consvigencia AND CDP.estado <> 'N' AND CDP.tipo_mov = '201' AND CDP.tipo_mov = CDP_DET.tipo_mov $crit3 GROUP BY CDP.vigencia, CDP_DET.cuenta, CDP_DET.fuente, CDP.consvigencia";
								
								
                            }
                            $resp = mysqli_query($linkbd_V7, $sqlr);
                            $ntr = mysqli_num_rows($resp);
                            $con = 1;

							$sqlrEnt = "SELECT nit, razonsocial FROM configbasica";
							$respEnt = mysqli_query($linkbd_V7, $sqlrEnt);
							$rowEnt = mysqli_fetch_row($respEnt);

                            echo "<table class='inicio' align='center' width='80%'>
									<tr>
										<td colspan='12' class='titulos'>.: Relaci&oacute;n de CDPs:</td>
									</tr>
									<tr>
										<td colspan='5'>CDPs Encontrados: $ntr</td>
									</tr>
									<tr>
										<td class='titulos2'>Anio</td>
										<td class='titulos2'>Vigencia</td>
										<td class='titulos2'>NIT_Entidad</td>
										<td class='titulos2'>Nombre_Entidad</td>
										<td class='titulos2'>Cod_Cuenta</td>
										<td class='titulos2'>Fuente_Financiacion</td>
										<td class='titulos2'>Nombre_Cuenta</td>
										<td class='titulos2'>Macro_campo_nivel_agregado</td>
										<td class='titulos2'>Numero_CDP</td>
										<td class='titulos2'>Fecha_CDP</td>
										<td class='titulos2'>Valor_CDP</td>
										<td class='titulos2'>Detalle_CDP</td>
									</tr>";	
		
							while ($row = mysqli_fetch_row($resp)) 
		 					{
								$vigenciaGasto = '';
								$nomFuente = '';

								if($row[7] == ''){
									$row[7] = 1;
								}
								$vigenciaGasto = $vigenciasDelGasto[$row[7]];
								

								$consecutivo = $row[0]."".str_pad($row[3], 5, '0', STR_PAD_LEFT);
								
								/* ereg( "([0-9]{2,4})-([0-9]{1,2})-([0-9]{1,2})", $row[4],$fecha);
								$fechaf = $fecha[1]."/".$fecha[2]."/".$fecha[3];  */

								
									
								$nrub = str_replace(","," ",buscacuentaccpetgastos($row[1], $maxVersionGastos));
								$nrub = str_replace(";", " ", $nrub);

								$nomFuente = str_replace(","," ",buscafuenteccpet($row[2]));
								$nomFuente = str_replace(";", " ", $nomFuente);


								$sqlrRev = "SELECT SUM(CDP_DET.valor) FROM ccpetcdp AS CDP, ccpetcdp_detalle AS CDP_DET WHERE CDP.vigencia = CDP_DET.vigencia AND CDP.consvigencia = CDP_DET.consvigencia AND CDP.estado <> 'N' AND CDP.tipo_mov LIKE '4%' AND CDP.tipo_mov = CDP_DET.tipo_mov AND CDP.vigencia = '$row[0]' AND CDP_DET.cuenta = '$row[1]' AND CDP_DET.fuente = '$row[2]' AND CDP.consvigencia = '$row[3]' $crit3 GROUP BY CDP.vigencia, CDP_DET.cuenta, CDP_DET.fuente, CDP.consvigencia";

								$total = 0;
								
								$respRev = mysqli_query($linkbd_V7, $sqlrRev);
								$rowRev = mysqli_fetch_row($respRev);

								$total = intVal($row[5]) - intVal($rowRev[0]);

								echo "<tr class='$iter'>
										<td>$row[0]</td>
										<td>$vigenciaGasto</td>
										<td>$rowEnt[0]</td>
										<td>$rowEnt[1]</td>
										<td>$row[1]</td>
										<td>$nomFuente</td>
										<td>".str_replace(","," ",str_replace(";", " ",$nrub))."</td>
										<td>DA</td>
										<td>$consecutivo</td>
										<td>$row[4]</td>
										<td>$total</td>
										<td>$row[6]</td>
									</tr>";

								fputs($Descriptor1,$row[0]."|".$vigenciaGasto."|".$rowEnt[0]."|".$rowEnt[1]."|".$row[1]."|".$nomFuente."|".str_replace(","," ",str_replace(";", " ",$nrub))."|DA|".$consecutivo."|".$row[4]."|".round($total, 2)."|".str_replace(","," ",str_replace(";", " ",$row[6]))."\r\n");
			
								$con+=1;
								$aux=$iter;
								$iter=$iter2;
								$iter2=$aux;
 							}
							echo "<script>document.getElementById('divcarga').style.display='none';</script>";
							echo"</table>";
							fclose($Descriptor1);
						break;
	
						//------------------------------------------------------------------------------------------------
	
						case 2:	//Relación de Compromisos

							$crit3 = " ";
							$crit4 = " ";	
							$namearch = "archivos/FORMATO_".$informes[$_POST['reporte']].".csv";
							$_POST['nombrearchivo'] = $namearch;

							$Descriptor1 = fopen($namearch,"w+"); 
                            fputs($Descriptor1,"Anio| Vigencia| NIT_Entidad| Nombre_Entidad| Cod_Cuenta| Fuente_Financiacion| Nombre_Cuenta| Numero_Compromiso| Fecha_Compromiso| Numero_CDP| Fecha_CDP| Valor_Compromiso|NIT_Beneficiario|Nombre_Beneficiario|Detalle_compromiso|Macro_campo_nivel_agregado|Tipo_compromiso|Valor_obligaciones|Valor_pagado_neto|Tipo_Gasto\r\n");
                            
                            $sqlr = '';

							if ($_POST['fecha'] != "" && $_POST['fecha2'] != "" )
                            {
								preg_match("/([0-9]{2})\/([0-9]{2})\/([0-9]{4})/", $_POST['fecha'], $fecha);
								$fechai = $fecha[3]."-".$fecha[2]."-".$fecha[1];

								preg_match("/([0-9]{2})\/([0-9]{2})\/([0-9]{4})/", $_POST['fecha2'], $fecha);
								$fechaf = $fecha[3]."-".$fecha[2]."-".$fecha[1];

                            	$crit3 = " AND RP.fecha BETWEEN '$fechai' AND '$fechaf' ";

								$sqlr = "SELECT RP.vigencia, RP_DET.cuenta, RP_DET.fuente, RP.consvigencia, RP.fecha, RP.idcdp, SUM(RP_DET.valor), RP.tercero, RP.detalle, RP_DET.codigo_vigenciag, RP.tipo_mov 
								FROM ccpetrp AS RP, ccpetrp_detalle AS RP_DET 
								WHERE 
									RP.vigencia = RP_DET.vigencia 
									AND RP.consvigencia = RP_DET.consvigencia 
									AND RP.estado <> 'N' 
									AND RP_DET.tipo_mov='201'
									AND RP.tipo_mov='201'
									AND RP.tipo_mov = RP_DET.tipo_mov
									$crit3 
									GROUP BY RP.vigencia, RP_DET.cuenta, RP_DET.fuente, RP.consvigencia
								";
								
								
                            }
							
							$resp = mysqli_query($linkbd_V7, $sqlr);
                            $ntr = mysqli_num_rows($resp);
                            $con = 1;

							$sqlrEnt = "SELECT nit, razonsocial FROM configbasica";
							$respEnt = mysqli_query($linkbd_V7, $sqlrEnt);
							$rowEnt = mysqli_fetch_row($respEnt);

                            echo "<table class='inicio' align='center' width='80%'>
									<tr>
										<td colspan='20' class='titulos'>.: Relaci&oacute;n de Compromisos:</td>
									</tr>
									<tr>
										<td colspan='5'>Compromisos Encontrados: $ntr</td>
									</tr>
									<tr>
										<td class='titulos2'>Anio</td>
										<td class='titulos2'>Vigencia</td>
										<td class='titulos2'>NIT_Entidad</td>
										<td class='titulos2'>Nombre_Entidad</td>
										<td class='titulos2'>Cod_Cuenta</td>
										<td class='titulos2'>Fuente_Financiacion</td>
										<td class='titulos2'>Nombre_Cuenta</td>
										<td class='titulos2'>Numero_Compromiso</td>
										<td class='titulos2'>Fecha_Compromiso</td>
										<td class='titulos2'>Numero_CDP</td>
										<td class='titulos2'>Fecha_CDP</td>
										<td class='titulos2'>Valor_Compromiso</td>
										<td class='titulos2'>NIT_Beneficiario</td>
										<td class='titulos2'>Nombre_Beneficiario</td>
										<td class='titulos2'>Detalle_compromiso</td>
										<td class='titulos2'>Macro_campo_nivel_agregado</td>
										<td class='titulos2'>Tipo_compromiso</td>
										<td class='titulos2'>Valor_obligaciones</td>
										<td class='titulos2'>Valor_pagado_neto</td>
										<td class='titulos2'>Tipo_Gasto</td>
									</tr>";	
		
							while ($row = mysqli_fetch_row($resp)) 
		 					{
								$vigenciaGasto = '';
								$nomFuente = '';
								$tipoDeCuenta = '';

								if($row[9] == ''){
									$row[9] = 1;
								}
								
								$vigenciaGasto = $vigenciasDelGasto[$row[9]];
								
								$nrub = str_replace(","," ",buscacuentaccpetgastos($row[1], $maxVersionGastos));
								$nrub = str_replace(";", " ", $nrub);

								$nomFuente = str_replace(","," ",buscafuenteccpet($row[2]));
								$nomFuente = str_replace(";", " ", $nomFuente);

								$sqlrCdp = "SELECT fecha FROM ccpetcdp WHERE vigencia = '$row[0]' AND consvigencia = $row[5]";

								$rest = substr($row[1], 0, 3);
								$tipoDeCuenta = $tipoCuenta[$rest];

								

								
								$respCdp = mysqli_query($linkbd_V7, $sqlrCdp);
								$rowCdp = mysqli_fetch_row($respCdp);

								/* $tercero = $row[10]; */
								$tercero = buscatercero($row[7]);
								/* if($tercero == ''){
									$tercero = buscatercero($row[7]);
								} */

								$consecutivo = $row[0]."".str_pad($row[3], 5, '0', STR_PAD_LEFT);
								
								/* ereg( "([0-9]{2,4})-([0-9]{1,2})-([0-9]{1,2})", $row[4],$fecha);
								$fechaf = $fecha[1]."/".$fecha[2]."/".$fecha[3];  */

								$sqlrCxpNom = "SELECT SUM(TB2.valor) FROM hum_nom_cdp_rp AS TB1, humnom_presupuestal AS TB2 WHERE TB1.rp = '$row[3]' AND TB1.vigencia = '$row[0]' AND TB1.nomina = TB2.id_nom AND TB1.estado != 'N' AND TB2.cuenta = '$row[1]'";

								$respCxpNom = mysqli_query($linkbd_V7, $sqlrCxpNom);
								$rowCxp = mysqli_fetch_row($respCxpNom);

								//echo $cantCxpNom."<br>";
								if($rowCxp[0] != null ){
									
									$respCxpNom = mysqli_query($linkbd_V7, $sqlrCxpNom);
									$rowCxp = mysqli_fetch_row($respCxpNom);
									/* echo $rowCxp[0]."<br>";
									echo $sqlrCxpNom."<br>"; */
									$respCxpNom = mysqli_query($linkbd_V7, $sqlrCxpNom);
									$rowEgreso = mysqli_fetch_row($respCxpNom);
								}else{
									$sqlrCxp = "SELECT SUM(TB2.valor) FROM tesoordenpago AS TB1, tesoordenpago_det AS TB2 WHERE TB1.id_rp = '$row[3]' AND TB1.vigencia = '$row[0]' AND TB1.id_orden = TB2.id_orden AND TB1.estado != 'N' AND TB2.cuentap = '$row[1]' AND TB2.fuente = '$row[2]' ";
									$respCxp = mysqli_query($linkbd_V7, $sqlrCxp);
									$rowCxp = mysqli_fetch_row($respCxp);

									$sqlrEgreso = "SELECT SUM(TB2.valor) FROM tesoordenpago AS TB1, tesoordenpago_det AS TB2 WHERE TB1.id_rp = '$row[3]' AND TB1.vigencia = '$row[0]' AND TB1.id_orden = TB2.id_orden AND TB1.estado != 'N' AND TB2.cuentap = '$row[1]' AND TB1.estado = 'P' AND TB2.fuente = '$row[2]' ";
									$respEgreso = mysqli_query($linkbd_V7, $sqlrEgreso);
									$rowEgreso = mysqli_fetch_row($respEgreso);
								}

								//echo $rowCxp[0]."<br>";

								$sqlrSumDet = "SELECT SUM(RP_DET.valor), RP.tipo_mov 
									FROM ccpetrp AS RP, ccpetrp_detalle AS RP_DET 
									WHERE 
										RP.vigencia = RP_DET.vigencia 
										AND RP.consvigencia = RP_DET.consvigencia 
										AND RP.estado <> 'N' 
										AND RP_DET.tipo_mov='201'
										AND RP.tipo_mov='201'
										AND RP_DET.cuenta = '$row[1]'
										AND RP_DET.fuente = '$row[2]'
										AND RP.consvigencia = '$row[3]'
										AND RP.tipo_mov = RP_DET.tipo_mov
										$crit3 
										GROUP BY RP.vigencia, RP_DET.cuenta, RP_DET.fuente, RP.consvigencia
									UNION SELECT SUM(RP_DET.valor), RP.tipo_mov 
									FROM ccpetrp AS RP, ccpetrp_detalle AS RP_DET 
									WHERE 
										RP.vigencia = RP_DET.vigencia 
										AND RP.consvigencia = RP_DET.consvigencia 
										AND RP.estado <> 'N'
										AND (RP_DET.tipo_mov='401' OR RP_DET.tipo_mov='402') 
										AND (RP.tipo_mov='401' OR RP.tipo_mov='402') 
										AND RP_DET.cuenta = '$row[1]'
										AND RP_DET.fuente = '$row[2]'
										AND RP.consvigencia = '$row[3]'
										AND RP.tipo_mov = RP_DET.tipo_mov
										$crit3 
										GROUP BY RP.vigencia, RP_DET.cuenta, RP_DET.fuente, RP.consvigencia";
								$sumaRp = 0;
								$respSumDet = mysqli_query($linkbd_V7, $sqlrSumDet);
								while($rowSumDet = mysqli_fetch_row($respSumDet)){

									if($rowSumDet[1]=='201')
									{
										$sumaRp += round($rowSumDet[0],2);
									}
									else if( substr($rowSumDet[1],0,1) == '4' )
									{
										$sumaRp -= round($rowSumDet[0],2);
									}

								}
								
								$nrub = str_replace(";"," ",$nrub);

								echo "<tr class='$iter'>
										<td>$row[0]</td>
										<td>$vigenciaGasto</td>
										<td>$rowEnt[0]</td>
										<td>$rowEnt[1]</td>
										<td>$row[1]</td>
										<td>$nomFuente</td>
										<td>".str_replace(","," ",str_replace(";", " ",$nrub))."</td>
										<td>$consecutivo</td>
										<td>$row[4]</td>
										<td>$row[5]</td>
										<td>$rowCdp[0]</td>
										<td>$sumaRp</td>
										<td>$row[7]</td>
										<td>$tercero</td>
										<td>$row[8]</td>
										<td>DA</td>
										<td>Contrato</td>
										<td>$rowCxp[0]</td>
										<td>$rowEgreso[0]</td>
										<td>$tipoDeCuenta</td>
									</tr>";

								fputs($Descriptor1,$row[0]."|".$vigenciaGasto."|".$rowEnt[0]."|".$rowEnt[1]."|".$row[1]."|".$nomFuente."|".str_replace(","," ",str_replace(";", " ",$nrub))."|".$consecutivo."|".$row[4]."|".$row[5]."|".$rowCdp[0]."|".round($sumaRp,2)."|".$row[7]."|".$tercero."|".$row[8]."|DA|Contrato|".round($rowCxp[0],2)."|".round($rowEgreso[0],2)."|".$tipoDeCuenta."\r\n");
			
								$con+=1;
								$aux=$iter;
								$iter=$iter2;
								$iter2=$aux;
 							}
							echo "<script>document.getElementById('divcarga').style.display='none';</script>";
							echo"</table>";
							fclose($Descriptor1);


						break;
						//----------------------------------------------------------------------------------------------
						case 3: //Relacion de obligaciones

							$crit3 = " ";
							$crit4 = " ";	
							$namearch = "archivos/FORMATO_".$informes[$_POST['reporte']].".csv";
							$_POST['nombrearchivo'] = $namearch;

							$Descriptor1 = fopen($namearch,"w+"); 
                            fputs($Descriptor1,"Anio_reporte| Vigencia| NIT_Entidad_reportante| Nombre_Entidad| Codigo_Presupuestal| Macro_campo_nivel_agregado| Nombre_Rubro| Numero_de_obligacion| Fecha_de_Obligacion| Numero_Compromiso| Fecha_Compromiso| Valor_obligacion|NIT_Beneficiario|Nombre_Beneficiario|Detalle_compromiso\r\n");
                            
                            $sqlr = '';

							if ($_POST['fecha'] != "" && $_POST['fecha2'] != "" )
                            {
								preg_match("/([0-9]{2})\/([0-9]{2})\/([0-9]{4})/", $_POST['fecha'], $fecha);
								$fechai = $fecha[3]."-".$fecha[2]."-".$fecha[1];

								preg_match("/([0-9]{2})\/([0-9]{2})\/([0-9]{4})/", $_POST['fecha2'], $fecha);
								$fechaf = $fecha[3]."-".$fecha[2]."-".$fecha[1];

                            	$crit3 = " AND TB1.fecha BETWEEN '$fechai' AND '$fechaf' ";


								$sqlr = "SELECT TB1.vigencia, TB2.cuentap, TB1.id_orden, TB1.fecha, TB1.id_rp, SUM(TB2.valor), TB1.tercero, TB1.conceptorden, TB2.codigo_vigenciag FROM tesoordenpago AS TB1, tesoordenpago_det AS TB2 WHERE  TB1.id_orden = TB2.id_orden AND TB1.estado != 'N' $crit3 GROUP BY TB1.vigencia, TB2.cuentap, TB1.id_orden";
                            }
							
							$resp = mysqli_query($linkbd_V7, $sqlr);
                            $ntr = mysqli_num_rows($resp);
                            $con = 1;

							$sqlrEnt = "SELECT nit, razonsocial FROM configbasica";
							$respEnt = mysqli_query($linkbd_V7, $sqlrEnt);
							$rowEnt = mysqli_fetch_row($respEnt);

                            echo "<table class='inicio' align='center' width='80%'>
									<tr>
										<td colspan='15' class='titulos'>.: Relaci&oacute;n de Compromisos:</td>
									</tr>
									<tr>
										<td colspan='5'>Compromisos Encontrados: $ntr</td>
									</tr>
									<tr>
										<td class='titulos2'>Anio_reporte</td>
										<td class='titulos2'>Vigencia</td>
										<td class='titulos2'>NIT_Entidad_reportante</td>
										<td class='titulos2'>Nombre_Entidad</td>
										<td class='titulos2'>Codigo_Presupuestal</td>
										<td class='titulos2'>Macro_campo_nivel_agregado</td>
										<td class='titulos2'>Nombre_Rubro</td>
										<td class='titulos2'>Numero_de_obligacion</td>
										<td class='titulos2'>Fecha_de_Obligacion</td>
										<td class='titulos2'>Numero_Compromiso</td>
										<td class='titulos2'>Fecha_Compromiso</td>
										<td class='titulos2'>Valor_obligacion</td>
										<td class='titulos2'>NIT_Beneficiario</td>
										<td class='titulos2'>Nombre_Beneficiario</td>
										<td class='titulos2'>Detalle_compromiso</td>
									</tr>";	
		
							while ($row = mysqli_fetch_row($resp)) 
		 					{
								if($row[5] > 0){
									$vigenciaGasto = '';
									$tipoDeCuenta = '';
									$nrub = '';

									if($row[8] == ''){
										$row[8] = 1;
									}
									$vigenciaGasto = $vigenciasDelGasto[$row[8]];
									

									$tercero = buscatercero($row[6]);

									$consecutivo = $row[0]."".str_pad($row[4], 5, '0', STR_PAD_LEFT);
									
									if($row[0] >= 2021){
										$sqlrRp = "SELECT fecha, detalle FROM ccpetrp WHERE vigencia = '$row[0]' AND consvigencia = $row[4]";
										$respRp = mysqli_query($linkbd_V7, $sqlrRp);
										$rowRp = mysqli_fetch_row($respRp);

										$nrub = str_replace(","," ",buscacuentaccpetgastos($row[1], $maxVersionGastos));
										$nrub = str_replace(";", " ", $nrub);

									}else{
										$sqlrRp = "SELECT fecha, detalle FROM pptorp WHERE vigencia = '$row[0]' AND consvigencia = $row[4]";
										$respRp = mysqli_query($linkbd_V7, $sqlrRp);
										$rowRp = mysqli_fetch_row($respRp);

										$nrub = str_replace(","," ",buscaNombreCuenta($row[1], $row[0]));
										$nrub = str_replace(";", " ", $nrub);
									}
									


									echo "<tr class='$iter'>
											<td>$row[0]</td>
											<td>$vigenciaGasto</td>
											<td>$rowEnt[0]</td>
											<td>$rowEnt[1]</td>
											<td>$row[1]</td>
											<td>DA</td>
											<td>".str_replace(","," ",$nrub)."</td>
											<td>$row[2]</td>
											<td>$row[3]</td>
											<td>$consecutivo</td>
											<td>$rowRp[0]</td>
											<td>$row[5]</td>
											<td>$row[6]</td>
											<td>$tercero</td>
											<td>$rowRp[1]</td>
										</tr>";

									fputs($Descriptor1,$row[0]."|".$vigenciaGasto."|".$rowEnt[0]."|".$rowEnt[1]."|".$row[1]."|DA|".str_replace(","," ",$nrub)."|".$row[2]."|".$row[3]."|".$consecutivo."|".$rowRp[0]."|".round($row[5],2)."|".$row[6]."|".$tercero."|".$rowRp[1]."\r\n");
				
									$con+=1;
									$aux=$iter;
									$iter=$iter2;
									$iter2=$aux;
								}
								
 							}

							if ($_POST['fecha'] != "" && $_POST['fecha2'] != "" )
							{
								preg_match("/([0-9]{2})\/([0-9]{2})\/([0-9]{4})/", $_POST['fecha'], $fecha);
								$fechai = $fecha[3]."-".$fecha[2]."-".$fecha[1];

								preg_match("/([0-9]{2})\/([0-9]{2})\/([0-9]{4})/", $_POST['fecha2'], $fecha);
								$fechaf = $fecha[3]."-".$fecha[2]."-".$fecha[1];

								$crit3 = " AND HN.fecha BETWEEN '$fechai' AND '$fechaf' ";


								/* $sqlr = "SELECT TB1.vigencia, TB2.cuentap, TB1.id_orden, TB1.fecha, TB1.id_rp, SUM(TB2.valor), TB1.tercero, TB1.conceptorden, TB2.codigo_vigenciag FROM tesoordenpago AS TB1, tesoordenpago_det AS TB2 WHERE  TB1.id_orden = TB2.id_orden AND TB1.estado != 'N' $crit3 GROUP BY TB1.vigencia, TB2.cuentap, TB1.id_orden"; */

								$sqlr = "SELECT HN.vigencia, HN.id_nom, HNP.cuenta, HN.periodo, SUM(HNP.valor), HN.fecha, HNR.rp FROM hum_nom_cdp_rp HNR,humnom_presupuestal HNP,humnomina HN WHERE  HNR.nomina=HNP.id_nom AND  NOT(HNR.estado='N' OR HNR.estado='R') AND HNP.valor>0 AND HN.id_nom=HNR.nomina $crit3 GROUP BY HN.id_nom, HNP.cuenta";
							}

							$resp = mysqli_query($linkbd_V7, $sqlr);
                            
							$sqlrEnt = "SELECT nit, razonsocial FROM configbasica";
							$respEnt = mysqli_query($linkbd_V7, $sqlrEnt);
							$rowEnt = mysqli_fetch_row($respEnt);

                            
							while ($row = mysqli_fetch_row($resp)) 
		 					{
								$vigenciaGasto = '';
								$tipoDeCuenta = '';
								$nrub = '';

								$vigenciaGasto = $vigenciasDelGasto[1];
								

								$consecutivo = $row[0]."".str_pad($row[6], 5, '0', STR_PAD_LEFT);
								
								if($row[0] >= 2021){
									$sqlrRp = "SELECT fecha, detalle FROM ccpetrp WHERE vigencia = '$row[0]' AND consvigencia = $row[6]";
									$respRp = mysqli_query($linkbd_V7, $sqlrRp);
									$rowRp = mysqli_fetch_row($respRp);

									$nrub = str_replace(","," ",buscacuentaccpetgastos($row[2], $maxVersionGastos));
									$nrub = str_replace(";", " ", $nrub);

								}else{
									$sqlrRp = "SELECT fecha, detalle FROM pptorp WHERE vigencia = '$row[0]' AND consvigencia = $row[1]";
									$respRp = mysqli_query($linkbd_V7, $sqlrRp);
									$rowRp = mysqli_fetch_row($respRp);

									$nrub = str_replace(","," ",buscaNombreCuenta($row[2], $row[0]));
									$nrub = str_replace(";", " ", $nrub);
								}
								


								echo "<tr class='$iter'>
										<td>$row[0]</td>
										<td>$vigenciaGasto</td>
										<td>$rowEnt[0]</td>
										<td>$rowEnt[1]</td>
										<td>$row[2]</td>
										<td>DA</td>
										<td>".str_replace(","," ",$nrub)."</td>
										<td>$row[1]</td>
										<td>$row[5]</td>
										<td>$consecutivo</td>
										<td>$rowRp[0]</td>
										<td>$row[4]</td>
										<td>$rowEnt[0]</td>
										<td>$rowEnt[1]</td>
										<td>$rowRp[1]</td>
									</tr>";

								fputs($Descriptor1,$row[0]."|".$vigenciaGasto."|".$rowEnt[0]."|".$rowEnt[1]."|".$row[2]."|DA|".str_replace(","," ",$nrub)."|".$row[1]."|".$row[5]."|".$consecutivo."|".$rowRp[0]."|".round($row[4],2)."|".$rowEnt[0]."|".$rowEnt[1]."|".$rowRp[1]."\r\n");
			
								$con+=1;
								$aux=$iter;
								$iter=$iter2;
								$iter2=$aux;
								
 							}


							echo "<script>document.getElementById('divcarga').style.display='none';</script>";
							echo"</table>";
							fclose($Descriptor1);
							
						break;
						case 4:  //Relación de pagos

							
							
							$crit3 = " ";
							$crit4 = " ";	
							$namearch = "archivos/FORMATO_".$informes[$_POST['reporte']].".csv";
							$_POST['nombrearchivo'] = $namearch;

							$Descriptor1 = fopen($namearch,"w+"); 
                            fputs($Descriptor1,"Anio_reporte| Vigencia| NIT_Entidad| Nombre_Entidad| Cod_Cuenta| Fuente_Financiacion| Nombre_Cuenta| Macro_campo_nivel_agregado| Situacion_de_Fondos| Numero_Egreso| Fecha_Egreso| Numero_Obligacion|Fecha_obligación|Valor_Egreso_Presupuestal|Descuentos|Neto_Pagado|Banco|No_Cuenta|NIT_Beneficiario|Nombre_Beneficiario|Detalle_Egreso\r\n");
                            
                            $sqlr = '';

							if ($_POST['fecha'] != "" && $_POST['fecha2'] != "" )
                            {
								preg_match("/([0-9]{2})\/([0-9]{2})\/([0-9]{4})/", $_POST['fecha'], $fecha);
								$fechai = $fecha[3]."-".$fecha[2]."-".$fecha[1];

								preg_match("/([0-9]{2})\/([0-9]{2})\/([0-9]{4})/", $_POST['fecha2'], $fecha);
								$fechaf = $fecha[3]."-".$fecha[2]."-".$fecha[1];

                            	$crit3 = " AND TB1.fecha BETWEEN '$fechai' AND '$fechaf' ";

								$sqlr = "SELECT TB2.vigencia, TB3.cuentap, TB3.fuente, TB2.medio_pago, TB1.id_egreso, TB1.fecha, TB1.id_orden, TB2.fecha, SUM(TB3.valor), TB1.valortotal, TB1.retenciones, TB1.banco, TB1.tercero, TB1.concepto , TB3.codigo_vigenciag FROM tesoegresos AS TB1, tesoordenpago AS TB2, tesoordenpago_det AS TB3 WHERE TB1.id_orden = TB2.id_orden AND TB2.id_orden = TB3.id_orden AND TB1.estado != 'N' AND TB1.estado != 'R' AND TB1.estado != '' $crit3 GROUP BY TB1.vigencia, TB3.cuentap, TB3.fuente, TB2.medio_pago, TB1.id_egreso";
                            }
							
							$resp = mysqli_query($linkbd_V7, $sqlr);
                            $ntr = mysqli_num_rows($resp);
                            $con = 1;

							$sqlrEnt = "SELECT nit, razonsocial FROM configbasica";
							$respEnt = mysqli_query($linkbd_V7, $sqlrEnt);
							$rowEnt = mysqli_fetch_row($respEnt);

                            echo "<table class='inicio' align='center' width='80%'>
									<tr>
										<td colspan='21' class='titulos'>.: Relaci&oacute;n de Compromisos:</td>
									</tr>
									<tr>
										<td colspan='5'>Compromisos Encontrados: $ntr</td>
									</tr>
									<tr>
										<td class='titulos2'>Anio_reporte</td>
										<td class='titulos2'>Vigencia</td>
										<td class='titulos2'>NIT_Entidad</td>
										<td class='titulos2'>Nombre_Entidad</td>
										<td class='titulos2'>Cod_Cuenta</td>
										<td class='titulos2'>Fuente_Financiacion</td>
										<td class='titulos2'>Nombre_Cuenta</td>
										<td class='titulos2'>Macro_campo_nivel_agregado</td>
										<td class='titulos2'>Situacion_de_Fondos</td>
										<td class='titulos2'>Numero_Egreso</td>
										<td class='titulos2'>Fecha_Egreso</td>
										<td class='titulos2'>Numero_Obligacion</td>
										<td class='titulos2'>Fecha_obligacion</td>
										<td class='titulos2'>Valor_Egreso_Presupuestal</td>
										<td class='titulos2'>Descuentos</td>
										<td class='titulos2'>Neto_Pagado</td>
										<td class='titulos2'>Banco</td>
										<td class='titulos2'>No_Cuenta</td>
										<td class='titulos2'>NIT_Beneficiario</td>
										<td class='titulos2'>Nombre_Beneficiario</td>
										<td class='titulos2'>Detalle_Egreso</td>
									</tr>";	
		
							while ($row = mysqli_fetch_row($resp)) 
		 					{
								if($row[8] > 0){
									$vigenciaGasto = '';
									$tipoDeCuenta = '';
									$nrub = '';
									$nomFuente = '';
									$nomSituacionFondos = '';

									if($row[3] == ''){
										$row[3] = 1;
									}

									if($row[3] == 1){
										$nomSituacionFondos = 'C';
									}else{
										$nomSituacionFondos = 'S';
									}
							

									if($row[14] == ''){
										$row[14] = 1;
									}
									$vigenciaGasto = $vigenciasDelGasto[$row[14]];
									

									$tercero = buscatercero($row[12]);

									$consecutivo = $row[0]."".str_pad($row[4], 5, '0', STR_PAD_LEFT);

									$consecutivoCxp = $row[0]."".str_pad($row[6], 5, '0', STR_PAD_LEFT);
									
									if($row[0] >= 2021){

										$nrub = str_replace(","," ",buscacuentaccpetgastos($row[1], $maxVersionGastos));
										$nrub = str_replace(";", " ", $nrub);

										$nomFuente = str_replace(","," ",buscafuenteccpet($row[2]));
										$nomFuente = str_replace(";", " ", $nomFuente);

									}else{

										$nrub = str_replace(","," ",buscaNombreCuenta($row[1], $row[0]));
										$nrub = str_replace(";", " ", $nrub);

										$nomFuente = str_replace(","," ",buscafuenteppto($row[1], $row[0]));
										$nomFuente = str_replace(";", " ", $nomFuente);
									}
									
									$descuento = 0;

									$descuento = round(($row[10]/$row[9] * $row[8]),2);
									$netoPagado = $row[8] - $descuento;

									$sqlrBanco = "SELECT tercero, ncuentaban FROM tesobancosctas WHERE cuenta = '$row[11]'";
									$respBanco = mysqli_query($linkbd_V7, $sqlrBanco);
									$rowBanco = mysqli_fetch_row($respBanco);

									$banco = buscatercero($rowBanco[0]);

									echo "<tr class='$iter'>
											<td>$row[0]</td>						
											<td>$vigenciaGasto</td	>				
											<td>$rowEnt[0]</td>						
											<td>$rowEnt[1]</td>						
											<td>$row[1]</td>						
											<td>$nomFuente</td>						
											<td>".str_replace(","," ",$nrub)."</td>	
											<td>DA</td>
											<td>$nomSituacionFondos</td>
											<td>$consecutivo</td>					
											<td>$row[5]</td>
											<td>$consecutivoCxp</td>						
											<td>$row[7]</td>	 					
											<td>$row[8]</td>
											<td>$descuento</td>						
											<td>$netoPagado</td>					
											<td>$banco</td>
											<td>$rowBanco[1]</td>
											<td>$row[12]</td>
											<td>$tercero</td>
											<td>$row[13]</td>
										</tr>";

									fputs($Descriptor1,$row[0]."|".$vigenciaGasto."|".$rowEnt[0]."|".$rowEnt[1]."|".$row[1]."|".$nomFuente."|".str_replace("|"," ",$nrub)."|DA|".$nomSituacionFondos."|".$consecutivo."|".$row[5]."|".$consecutivoCxp."|".$row[7]."|".round($row[8],2)."|".round($descuento,2)."|".round($netoPagado,2)."|".$banco."|".$rowBanco[1]."|".$row[12]."|".$tercero."|".str_replace("|"," ",$row[13])."\r\n");
				
									$con+=1;
									$aux=$iter;
									$iter=$iter2;
									$iter2=$aux;
								}
								
 							}

							if ($_POST['fecha'] != "" && $_POST['fecha2'] != "" )
                            {
								preg_match("/([0-9]{2})\/([0-9]{2})\/([0-9]{4})/", $_POST['fecha'], $fecha);
								$fechai = $fecha[3]."-".$fecha[2]."-".$fecha[1];

								preg_match("/([0-9]{2})\/([0-9]{2})\/([0-9]{4})/", $_POST['fecha2'], $fecha);
								$fechaf = $fecha[3]."-".$fecha[2]."-".$fecha[1];

                            	$crit3 = " AND TEN.fecha BETWEEN '$fechai' AND '$fechaf' ";
/* 
								$sqlr = "SELECT TB1.vigencia, TB3.cuentap, TB3.fuente, TB2.medio_pago, TB1.id_egreso, TB1.fecha, TB1.id_orden, TB2.fecha, SUM(TB3.valor), TB1.valortotal, TB1.retenciones, TB1.banco, TB1.tercero, TB1.concepto , TB3.codigo_vigenciag FROM tesoegresos AS TB1, tesoordenpago AS TB2, tesoordenpago_det AS TB3 WHERE TB1.id_orden = TB2.id_orden AND TB2.id_orden = TB3.id_orden AND TB1.estado != 'N' AND TB1.estado != 'R' AND TB1.estado != '' $crit3 GROUP BY TB1.vigencia, TB3.cuentap, TB3.fuente, TB2.medio_pago, TB1.id_egreso"; */

								$sqlr = "SELECT TEN.vigencia, TEND.cuentap, TEND.fuente, TEN.id_egreso, TEN.fecha, TEN.id_orden, SUM(TEND.valordevengado), TEND.tercero, TEN.banco, TEN.concepto FROM tesoegresosnomina TEN,tesoegresosnomina_det TEND WHERE   NOT(TEN.estado='N' OR TEN.estado='R') AND TEN.id_egreso=TEND.id_egreso AND NOT(TEND.tipo='SE' OR TEND.tipo='PE' OR TEND.tipo='DS' OR TEND.tipo='RE') $crit3 GROUP BY TEN.vigencia, TEND.cuentap, TEND.fuente,TEN.id_egreso";

                            }

							$resp = mysqli_query($linkbd_V7, $sqlr);
                            

							$sqlrEnt = "SELECT nit, razonsocial FROM configbasica";
							$respEnt = mysqli_query($linkbd_V7, $sqlrEnt);
							$rowEnt = mysqli_fetch_row($respEnt);

							while ($row = mysqli_fetch_row($resp)) 
		 					{
								$vigenciaGasto = '';
								$tipoDeCuenta = '';
								$nrub = '';
								$nomFuente = '';
								$nomSituacionFondos = '';

								
								$nomSituacionFondos = 'C';
								
								$vigenciaGasto = $vigenciasDelGasto[1];
								
								$tercero = buscatercero($row[7]);

								$consecutivo = $row[0]."".str_pad($row[3], 5, '0', STR_PAD_LEFT);

								$consecutivoCxp = $row[0]."".str_pad($row[5], 5, '0', STR_PAD_LEFT);
								
								if($row[0] >= 2021){


									$nrub = str_replace(","," ",buscacuentaccpetgastos($row[1], $maxVersionGastos));
									$nrub = str_replace(";", " ", $nrub);

									$nomFuente = str_replace(","," ",buscafuenteccpet($row[2]));
									$nomFuente = str_replace(";", " ", $nomFuente);

								}else{

									$nrub = str_replace(","," ",buscaNombreCuenta($row[1], $row[0]));
									$nrub = str_replace(";", " ", $nrub);

									$nomFuente = str_replace(","," ",buscafuenteppto($row[1], $row[0]));
									$nomFuente = str_replace(";", " ", $nomFuente);
								}
								
								$descuento = 0;

								//$descuento = round(($row[10]/$row[9] * $row[8]),2);
								$netoPagado = $row[6] - $descuento;

								$sqlrBanco = "SELECT tercero, ncuentaban FROM tesobancosctas WHERE cuenta = '$row[8]'";
								$respBanco = mysqli_query($linkbd_V7, $sqlrBanco);
								$rowBanco = mysqli_fetch_row($respBanco);

								$banco = buscatercero($rowBanco[0]);

								echo "<tr class='$iter'>
										<td>$row[0]</td>						
										<td>$vigenciaGasto</td	>				
										<td>$rowEnt[0]</td>						
										<td>$rowEnt[1]</td>						
										<td>$row[1]</td>						
										<td>$nomFuente</td>						
										<td>".str_replace(","," ",$nrub)."</td>	
										<td>DA</td>
										<td>$nomSituacionFondos</td>
										<td>$consecutivo</td>					
										<td>$row[4]</td>
										<td>$consecutivoCxp</td>						
										<td>$row[4]</td>	 					
										<td>$row[6]</td>
										<td>$descuento</td>						
										<td>$netoPagado</td>					
										<td>$banco</td>
										<td>$rowBanco[1]</td>
										<td>$row[7]</td>
										<td>$tercero</td>
										<td>$row[9]</td>
									</tr>";

								fputs($Descriptor1,$row[0]."|".$vigenciaGasto."|".$rowEnt[0]."|".$rowEnt[1]."|".$row[1]."|".$nomFuente."|".str_replace("|"," ",$nrub)."|DA|".$nomSituacionFondos."|".$consecutivo."|".$row[4]."|".$consecutivoCxp."|".$row[4]."|".round($row[6],2)."|".round($descuento,2)."|".round($netoPagado,2)."|".$banco."|".$rowBanco[1]."|".$row[7]."|".$tercero."|".str_replace("|"," ",$row[9])."\r\n");
			
								$con+=1;
								$aux=$iter;
								$iter=$iter2;
								$iter2=$aux;
								
 							}


							echo "<script>document.getElementById('divcarga').style.display='none';</script>";
							echo"</table>";
							fclose($Descriptor1);

						break;	

						case 5: //Ejecucion presupuestal de ingresos

							$crit3 = " ";
							$crit4 = " ";	
							$namearch = "archivos/FORMATO_".$informes[$_POST['reporte']].".csv";
							$_POST['nombrearchivo'] = $namearch;

							$Descriptor1 = fopen($namearch,"w+"); 
                            fputs($Descriptor1,"Anio_reporte| Vigencia| NIT_Entidad_reportante| Nombre_Entidad_reportante| Codigo_Presupuestal| Macro_campo_nivel_agregado| Nombre_Rubro| Presupuesto_Inicial| Adiciones_del_periodo| Adiciones_Acumuladas| Reducciones_del_periodo| Reducciones_Acumuladas|Apropiacion_definitiva|Reconocimientos_del_periodo|Reconocimientos_acumulados|Recaudos_del_periodo|Recaudos_Acumulados|Fecha_reporte\r\n");

							echo "<table class='inicio' align='center' width='80%'>
									<tr>
										<td colspan='21' class='titulos'>.: Relaci&oacute;n de Compromisos:</td>
									</tr>
									<tr>
										<td class='titulos2'>Anio_reporte</td>
										<td class='titulos2'>Vigencia</td>
										<td class='titulos2'>NIT_Entidad_reportante</td>
										<td class='titulos2'>Nombre_Entidad_reportante</td>
										<td class='titulos2'>Codigo_Presupuestal</td>
										<td class='titulos2'>Macro_campo_nivel_agregado</td>
										<td class='titulos2'>Nombre_Rubro</td>
										<td class='titulos2'>Presupuesto_Inicial</td>
										<td class='titulos2'>Adiciones_del_periodo</td>
										<td class='titulos2'>Adiciones_Acumuladas</td>
										<td class='titulos2'>Reducciones_del_periodo</td>
										<td class='titulos2'>Reducciones_Acumuladas</td>
										<td class='titulos2'>Apropiacion_definitiva</td>
										<td class='titulos2'>Reconocimientos_del_periodo</td>
										<td class='titulos2'>Reconocimientos_acumulados</td>
										<td class='titulos2'>Recaudos_del_periodo</td>
										<td class='titulos2'>Recaudos_Acumulados</td>
										<td class='titulos2'>Fecha_reporte</td>
									</tr>";	
                            
                            $sqlr = '';
							$vigencia = '';

							if ($_POST['fecha'] != "" && $_POST['fecha2'] != "" )
                            {
								preg_match("/([0-9]{2})\/([0-9]{2})\/([0-9]{4})/", $_POST['fecha'], $fecha);
								$fechai = $fecha[3]."-".$fecha[2]."-".$fecha[1];

								preg_match("/([0-9]{2})\/([0-9]{2})\/([0-9]{4})/", $_POST['fecha2'], $fecha);
								$fechaf = $fecha[3]."-".$fecha[2]."-".$fecha[1];

								$vigencia = $fecha[3];

                            	$crit3 = " AND TB1.fecha BETWEEN '$fechai' AND '$fechaf' ";
                            }

							$sql = "SELECT codigo, nombre, tipo FROM cuentasingresosccpet WHERE municipio=1 AND version='$maxVersion' AND tipo = 'C' ORDER BY id ASC";
							
							
                            $res = mysqli_query($linkbd_V7, $sql);
                            $i = 0;
                
                            while($row = mysqli_fetch_row($res))
                            {
                                $recaudoPorCuenta = 0;
                                $totalRecaudoCuenta = 0;
                                $saldoPorRecaudar = 0;
                                $rubro = '';
                                $negrilla = 'font-weight: normal;';
                                $rubro = $row[0];

								$linkbd = conectar_v7();
								$arregloIngresos = reporteIngresosCcpet($rubro, '%', '', $vigencia, $fechai, $fechaf);
								
                                
								$linkbd = conectar_v7();
								$recaudoPorCuenta = generaRecaudoCcpet($row[0], $vigencia, $vigencia, $fechai, $fechaf);
								
                                
								/* var_dump($recaudoPorCuenta); echo "<br> <br>";
								echo $row[0]." --> ".$recaudoPorCuenta."<br>"; */
                                $totalRecaudoCuenta = $recaudoPorCuenta;

                                $saldoPorRecaudar = $arregloIngresos[3] - $totalRecaudoCuenta;

                                $enEjecucion = round((($totalRecaudoCuenta/$arregloIngresos[3])*100),2);

                                if($arregloIngresos[0] > 0 || $arregloIngresos[1] > 0 || $recaudoPorCuenta > 0)
                                {
									$nombreCuenta = buscaNombreCuentaCCPET($row[0], 1);
                                    
                                    $cuentas[$row[0]]["numCuenta"]=$row[0];
                                    $cuentas[$row[0]]["nomCuenta"]="$nombreCuenta";
                                    $cuentas[$row[0]]["presuInicial"]=$arregloIngresos[0];
                                    $cuentas[$row[0]]["adicion"]=$arregloIngresos[1];
                                    $cuentas[$row[0]]["reduccion"]=$arregloIngresos[2];
                                    $cuentas[$row[0]]["presuDefinitivo"]=$arregloIngresos[3];
                                    $cuentas[$row[0]]["RecaudoPorCuenta"]=$recaudoPorCuenta;
                                    $cuentas[$row[0]]["TotalRecaudo"]=$totalRecaudoCuenta;
                                    $cuentas[$row[0]]["saldo"]=$saldoPorRecaudar;
                                    $cuentas[$row[0]]["enEjecucion"]=$enEjecucion;
                                }
                                
                            }

                            $i = 0;

							$con = 1;

							$vigenciaGasto = $vigenciasDelGasto[1];

							$sqlrEnt = "SELECT nit, razonsocial FROM configbasica";
							$respEnt = mysqli_query($linkbd_V7, $sqlrEnt);
							$rowEnt = mysqli_fetch_row($respEnt);


                            foreach ($cuentas as $key => $value) 
					        {
                                $numeroCuenta=$cuentas[$key]['numCuenta'];
                                $nombreCuenta=$cuentas[$key]['nomCuenta'];
                                $presupuestoInicial=$cuentas[$key]['presuInicial'];
                                $adicion=$cuentas[$key]['adicion'];
                                $reduccion=$cuentas[$key]['reduccion'];
                                $presupuestoDefinitivo=$cuentas[$key]['presuDefinitivo'];
                                $recaudoPorCuenta=$cuentas[$key]['RecaudoPorCuenta'];
                                $totalRecaudo=$cuentas[$key]['TotalRecaudo'];
                                $saldo=$cuentas[$key]['saldo']; 
                                $enEjecucion=$cuentas[$key]['enEjecucion']; 
                                $style='';


								echo "<tr class='$iter'>
											<td>$vigencia</td>								
											<td>$vigenciaGasto</td	>						
											<td>$rowEnt[0]</td>								
											<td>$rowEnt[1]</td>											
											<td>$numeroCuenta</td>								
											<td>DA</td>								
											<td>".str_replace(","," ",$nombreCuenta)."</td>			
											<td>".str_replace(".", ",", round($presupuestoInicial,2))."</td>										
											<td>".str_replace(".", ",", round($adicion,2))."</td>					
											<td>".str_replace(".", ",", round($adicion,2))."</td>							
											<td>".str_replace(".", ",", round($reduccion,2))."</td>								
											<td>".str_replace(".", ",", round($reduccion,2))."</td>							
											<td>".str_replace(".", ",", round($presupuestoDefinitivo,2))."</td>								
											<td>".str_replace(".", ",", round($recaudoPorCuenta,2))."</td>							
											<td>".str_replace(".", ",", round($recaudoPorCuenta,2))."</td>									
											<td>".str_replace(".", ",", round($recaudoPorCuenta,2))."</td>							
											<td>".str_replace(".", ",", round($totalRecaudo,2))."</td>						
											<td>$fechaf</td>								
										</tr>";

									fputs($Descriptor1,$vigencia."|".$vigenciaGasto."|".$rowEnt[0]."|".$rowEnt[1]."|".$numeroCuenta."|DA|".str_replace("|"," ",$nombreCuenta)."|".str_replace(".", ",", round($presupuestoInicial,2))."|".str_replace(".", ",", round($adicion,2))."|".str_replace(".", ",", round($adicion,2))."|".str_replace(".", ",", round($reduccion,2))."|".str_replace(".", ",", round($reduccion,2))."|".str_replace(".", ",", round($presupuestoDefinitivo,2))."|".str_replace(".", ",", round($recaudoPorCuenta,2))."|".str_replace(".", ",", round($recaudoPorCuenta,2))."|".str_replace(".", ",", round($recaudoPorCuenta,2))."|".str_replace(".", ",", round($totalRecaudo,2))."|".$fechaf."\r\n");
				
									$con+=1;
									$aux=$iter;
									$iter=$iter2;
									$iter2=$aux;
                                
                                

                                $i++;
                            }
                            
							echo "<script>document.getElementById('divcarga').style.display='none';</script>";
							echo"</table>";
							fclose($Descriptor1);

						break;

						case 6: //Ejecucion presupuestal de gastos

							$crit3 = " ";
							$crit4 = " ";	
							$namearch = "archivos/FORMATO_".$informes[$_POST['reporte']].".csv";
							$_POST['nombrearchivo'] = $namearch;

							$Descriptor1 = fopen($namearch,"w+"); 
                            fputs($Descriptor1,"Anio| Vigencia| NIT_Entidad| Nombre_Entidad| Cod_Cuenta| Fuente_Financiacion| Nombre_Cuenta| Macro_campo_nivel_agregado| Apropiacion_Inicial| Adiciones_periodo| Adiciones_Acumuladas| Reducciones_Periodo|Reducciones_Acumuladas|Creditos_Periodo|Creditos_Acumulados|Contracreditos_Periodo|Contracreditos_Acumulados|Apropiacion_Definitiva|Aplazamientos|Desaplazamientos|CDPs_Periodo|CDPs_Acumulado|Compromisos_Periodo|Compromisos_Acumulado|Obligaciones_Periodo|Obligaciones_Acumulado|Pagos_Periodo|Pagos_Acumulado|Saldo_Disponible|Porcentaje_Ejecucion|Fecha_Reporte\r\n");

							echo "<table class='inicio' align='center' width='80%'>
									<tr>
										<td colspan='21' class='titulos'>.: Relaci&oacute;n de Compromisos:</td>
									</tr>
									<tr>
										<td class='titulos2'>Anio</td>
										<td class='titulos2'>Vigencia</td>
										<td class='titulos2'>NIT_Entidad</td>
										<td class='titulos2'>Nombre_Entidad</td>
										<td class='titulos2'>Cod_Cuenta</td>
										<td class='titulos2'>Fuente_Financiacion</td>
										<td class='titulos2'>Nombre_Cuenta</td>
										<td class='titulos2'>Macro_campo_nivel_agregado</td>
										<td class='titulos2'>Apropiacion_Inicial</td>
										<td class='titulos2'>Adiciones_periodo</td>
										<td class='titulos2'>Adiciones_Acumuladas</td>
										<td class='titulos2'>Reducciones_Periodo</td>
										<td class='titulos2'>Reducciones_Acumuladas</td>
										<td class='titulos2'>Creditos_Periodo</td>
										<td class='titulos2'>Creditos_Acumulados</td>
										<td class='titulos2'>Contracreditos_Periodo</td>
										<td class='titulos2'>Contracreditos_Acumulados</td>
										<td class='titulos2'>Apropiacion_Definitiva</td>
										<td class='titulos2'>Aplazamientos</td>
										<td class='titulos2'>Desaplazamientos</td>
										<td class='titulos2'>CDPs_Periodo</td>
										<td class='titulos2'>CDPs_Acumulado</td>
										<td class='titulos2'>Compromisos_Periodo</td>
										<td class='titulos2'>Compromisos_Acumulado</td>
										<td class='titulos2'>Obligaciones_Periodo</td>
										<td class='titulos2'>Obligaciones_Acumulado</td>
										<td class='titulos2'>Pagos_Periodo</td>
										<td class='titulos2'>Pagos_Acumulado</td>
										<td class='titulos2'>Saldo_Disponible</td>
										<td class='titulos2'>Porcentaje_Ejecucion</td>
										<td class='titulos2'>Fecha_Reporte</td>
									</tr>";	
                            
                            $sqlr = '';
							$vigencia = '';

							if ($_POST['fecha'] != "" && $_POST['fecha2'] != "" )
                            {
								preg_match("/([0-9]{2})\/([0-9]{2})\/([0-9]{4})/", $_POST['fecha'], $fecha);
								$fechai = $fecha[3]."-".$fecha[2]."-".$fecha[1];

								preg_match("/([0-9]{2})\/([0-9]{2})\/([0-9]{4})/", $_POST['fecha2'], $fecha);
								$fechaf = $fecha[3]."-".$fecha[2]."-".$fecha[1];

								$vigencia = $fecha[3];

                            	$crit3 = " AND TB1.fecha BETWEEN '$fechai' AND '$fechaf' ";
                            }

							if($vigencia >= 2021){
								$sql = "SELECT codigo, nombre, tipo FROM cuentasccpet WHERE municipio=1 AND version = '$maxVersionGastos' AND tipo = 'C' ORDER BY id ASC";
							}else{
								$sql = "SELECT cuenta, nombre, tipo FROM pptocuentas WHERE  clasificacion != 'ingresos' AND vigencia = '$vigencia' AND tipo = 'Auxiliar' ORDER BY cuenta ASC";
							}
							
                            $res = mysqli_query($linkbd_V7, $sql);
                            $i = 0;
                
                            while($row = mysqli_fetch_row($res))
                            {
								$valorTotal = 0;
                                $valorCxp = 0;
                                $valorEgreso = 0;
                                $rubro = '';
                                $rubro = $row[0];

								
                                if($vigencia >= 2021){
									$linkbd = conectar_v7();
									$arregloGastos = reporteGastosCcpetCgr($rubro, '%', '', $vigencia, $fechai, $fechaf);
								}else{
									$linkbd = conectar_bd();
									$arregloGastos = generaReporteGastos($rubro, $vigencia, $fechai, $fechaf);
								}

								

								foreach ($arregloGastos as $key => $value) 
					        	{
									
									foreach($value as $k => $valor)
									{
										/* echo "<br><br>";
										var_dump($value);
										echo "<br><br>";
										var_dump($k); */

										/* echo "<br><br> $key <br><br>";
										
										var_dump($value);
										echo "<br><br> $k <br><br>"; */

										if($vigencia >= 2021){
											$nombreCuenta = buscaNombreCuentaCCPET($row[0]);
											$nomFuente = str_replace(","," ",buscafuenteccpet($k));
											$nomFuente = str_replace(";", " ", $nomFuente);

										}else{
											$nombreCuenta = str_replace(","," ",buscaNombreCuenta($row[0], $vigencia));

											$nomFuente = str_replace(","," ",buscafuenteppto($row[0], $vigencia));
											$nomFuente = str_replace(";", " ", $nomFuente);
										}
										
										$cuentas[$row[0]."".$k]["numCuenta"]=$row[0];
										$cuentas[$row[0]."".$k]["nomCuenta"]="$nombreCuenta";
										$cuentas[$row[0]."".$k]["presuInicial"]=$arregloGastos[0][$k];
										$cuentas[$row[0]."".$k]["adicion"]=$arregloGastos[1][$k];
										$cuentas[$row[0]."".$k]["reduccion"]=$arregloGastos[2][$k];
										$cuentas[$row[0]."".$k]["credito"]=$arregloGastos[3][$k];
										$cuentas[$row[0]."".$k]["conCredito"]=$arregloGastos[4][$k];
										$cuentas[$row[0]."".$k]["cdp"]=$arregloGastos[5][$k];
										$cuentas[$row[0]."".$k]["rp"]=$arregloGastos[6][$k];
										$cuentas[$row[0]."".$k]["cxp"]=$arregloGastos[7][$k];
										$cuentas[$row[0]."".$k]["egreso"]=$arregloGastos[8][$k];
										$cuentas[$row[0]."".$k]["fuente"]=$k;
									}
								}
								
							}
							
							
                            $i = 0;

							$con = 1;

							$vigenciaGasto = $vigenciasDelGasto[1];

							$sqlrEnt = "SELECT nit, razonsocial FROM configbasica";
							$respEnt = mysqli_query($linkbd_V7, $sqlrEnt);
							$rowEnt = mysqli_fetch_row($respEnt);


                            foreach ($cuentas as $key => $value) 
					        {

								$presupuestoDefinitivo = 0;
								$saldo = 0;
								$numeroCuenta=$cuentas[$key]['numCuenta'];
                                $nombreCuenta=$cuentas[$key]['nomCuenta'];
                                $presupuestoInicial=$cuentas[$key]['presuInicial'];
                                $adicion=$cuentas[$key]['adicion'];
                                $reduccion=$cuentas[$key]['reduccion'];
                                $credito=$cuentas[$key]['credito'];
                                $contracredito=$cuentas[$key]['conCredito'];

								$presupuestoDefinitivo = $presupuestoInicial + $adicion - $reduccion + $credito - $contracredito;
                                //$presupuestoDefinitivo=$cuentas[$key]['presuDefinitivo'];
                                $cdp=$cuentas[$key]['cdp'];
                                $rp=$cuentas[$key]['rp'];
                                $cxp=$cuentas[$key]['cxp'];
                                $egreso=$cuentas[$key]['egreso'];

								$saldo = $presupuestoDefinitivo - $cdp;
                                //$saldo=$cuentas[$key]['saldo']; 
                                $fuente=$cuentas[$key]['fuente']; 
                                $style='';

								$compromisoEnEjecucion = $rp - $cxp;
								$enEjecucion = round((($rp/$cxp)*100),-2);
                                $cuentasPorPagar = $cxp - $egreso;

								/* fputs($Descriptor1,"Anio| Vigencia| NIT_Entidad| Nombre_Entidad| Cod_Cuenta| Fuente_Financiacion| Nombre_Cuenta| Macro_campo_nivel_agregado| Apropiacion_Inicial| Adiciones_periodo| Adiciones_Acumuladas| Reducciones_Periodo|Reducciones_Acumuladas|Creditos_Periodo|Creditos_Acumulados|Contracreditos_Periodo|Contracreditos_Acumulados|Apropiacion_Definitiva|Aplazamientos|Desaplazamientos|CDPs_Periodo|CDPs_Acumulado|Compromisos_Periodo|Compromisos_Acumulado|Obligaciones_Periodo|Obligaciones_Acumulado|Pagos_Periodo|Pagos_Acumulado|Saldo_Disponible|Porcentaje_Ejecucion|Fecha_Reporte\r\n"); */

								if($presupuestoDefinitivo > 0){
									$nombreCuenta = str_replace("|"," ",$nombreCuenta);
									echo "<tr class='$iter'>
												<td>$vigencia</td>								
												<td>$vigenciaGasto</td	>						
												<td>$rowEnt[0]</td>								
												<td>$rowEnt[1]</td>											
												<td>$numeroCuenta</td>								
												<td>$fuente</td>														
												<td>".str_replace(";"," ",$nombreCuenta)."</td>			
												<td>DA</td>			
												<td>".round($presupuestoInicial,2)."</td>
												<td>$adicion</td>										
												<td>$adicion</td>										
												<td>$reduccion</td>										
												<td>$reduccion</td>	 											
												<td>$credito</td>												
												<td>$credito</td>							
												<td>$contracredito</td>								
												<td>$contracredito</td>								
												<td>$presupuestoDefinitivo</td>								
												<td>0</td>									
												<td>0</td>									
												<td>$cdp</td>
												<td>$cdp</td>
												<td>$rp</td>
												<td>$rp</td>
												<td>$cxp</td>
												<td>$cxp</td>
												<td>$egreso</td>
												<td>$egreso</td>
												<td>$saldo</td>
												<td>$enEjecucion</td>
												<td>$fechaf</td>
											</tr>";

										fputs($Descriptor1,$vigencia."|".$vigenciaGasto."|".$rowEnt[0]."|".$rowEnt[1]."|".$numeroCuenta."|".$fuente."|".str_replace(";"," ",$nombreCuenta)."|DA|".round($presupuestoInicial,2)."|".round($adicion,2)."|".round($adicion,2)."|".round($reduccion,2)."|".round($reduccion,2)."|".round($credito,2)."|".round($credito,2)."|".round($contracredito,2)."|".round($contracredito,2)."|".round($presupuestoDefinitivo,2)."|0|0|".round($cdp,2)."|".round($cdp,2)."|".round($rp,2)."|".round($rp,2)."|".round($cxp,2)."|".round($cxp,2)."|".round($egreso,2)."|".round($egreso,2)."|".round($saldo,2)."|".$enEjecucion."|".$fechaf."\r\n");
					
										$con+=1;
										$aux=$iter;
										$iter=$iter2;
										$iter2=$aux;
									
									

									$i++;
								}
								
                            } 
                            
							echo "<script>document.getElementById('divcarga').style.display='none';</script>";
							echo"</table>";
							fclose($Descriptor1);

						break;

						case 7: //Ejecucion presupuestal de gastos 2018

							$crit3 = " ";
							$crit4 = " ";	
							$namearch = "archivos/FORMATO_".$informes[$_POST['reporte']].".csv";
							$_POST['nombrearchivo'] = $namearch;

							$Descriptor1 = fopen($namearch,"w+"); 
                            fputs($Descriptor1,"Anio| Vigencia| NIT_Entidad| Nombre_Entidad| Cod_Cuenta| Fuente_Financiacion| Nombre_Cuenta| Macro_campo_nivel_agregado| Apropiacion_Inicial| Adiciones_periodo| Adiciones_Acumuladas| Reducciones_Periodo|Reducciones_Acumuladas|Creditos_Periodo|Creditos_Acumulados|Contracreditos_Periodo|Contracreditos_Acumulados|Apropiacion_Definitiva|Aplazamientos|Desaplazamientos|CDPs_Periodo|CDPs_Acumulado|Compromisos_Periodo|Compromisos_Acumulado|Obligaciones_Periodo|Obligaciones_Acumulado|Pagos_Periodo|Pagos_Acumulado|Saldo_Disponible|Porcentaje_Ejecucion|Fecha_Reporte\r\n");

							echo "<table class='inicio' align='center' width='80%'>
									<tr>
										<td colspan='21' class='titulos'>.: Relaci&oacute;n de Compromisos:</td>
									</tr>
									<tr>
										<td class='titulos2'>Anio</td>
										<td class='titulos2'>Vigencia</td>
										<td class='titulos2'>NIT_Entidad</td>
										<td class='titulos2'>Nombre_Entidad</td>
										<td class='titulos2'>Cod_Cuenta</td>
										<td class='titulos2'>Fuente_Financiacion</td>
										<td class='titulos2'>Nombre_Cuenta</td>
										<td class='titulos2'>Macro_campo_nivel_agregado</td>
										<td class='titulos2'>Apropiacion_Inicial</td>
										<td class='titulos2'>Adiciones_periodo</td>
										<td class='titulos2'>Adiciones_Acumuladas</td>
										<td class='titulos2'>Reducciones_Periodo</td>
										<td class='titulos2'>Reducciones_Acumuladas</td>
										<td class='titulos2'>Creditos_Periodo</td>
										<td class='titulos2'>Creditos_Acumulados</td>
										<td class='titulos2'>Contracreditos_Periodo</td>
										<td class='titulos2'>Contracreditos_Acumulados</td>
										<td class='titulos2'>Apropiacion_Definitiva</td>
										<td class='titulos2'>Aplazamientos</td>
										<td class='titulos2'>Desaplazamientos</td>
										<td class='titulos2'>CDPs_Periodo</td>
										<td class='titulos2'>CDPs_Acumulado</td>
										<td class='titulos2'>Compromisos_Periodo</td>
										<td class='titulos2'>Compromisos_Acumulado</td>
										<td class='titulos2'>Obligaciones_Periodo</td>
										<td class='titulos2'>Obligaciones_Acumulado</td>
										<td class='titulos2'>Pagos_Periodo</td>
										<td class='titulos2'>Pagos_Acumulado</td>
										<td class='titulos2'>Saldo_Disponible</td>
										<td class='titulos2'>Porcentaje_Ejecucion</td>
										<td class='titulos2'>Fecha_Reporte</td>
									</tr>";	
                            
                            $sqlr = '';
							$vigencia = '';

							if ($_POST['fecha'] != "" && $_POST['fecha2'] != "" )
                            {
								preg_match("/([0-9]{2})\/([0-9]{2})\/([0-9]{4})/", $_POST['fecha'], $fecha);
								$fechai = $fecha[3]."-".$fecha[2]."-".$fecha[1];

								preg_match("/([0-9]{2})\/([0-9]{2})\/([0-9]{4})/", $_POST['fecha2'], $fecha);
								$fechaf = $fecha[3]."-".$fecha[2]."-".$fecha[1];

								$vigencia = $fecha[3];

                            	$crit3 = " AND TB1.fecha BETWEEN '$fechai' AND '$fechaf' ";
                            }

							if($vigencia >= 2021){
								$sql = "SELECT codigo, nombre, tipo FROM cuentasccpet WHERE municipio=1 AND version = '$maxVersionGastos' AND tipo = 'C' ORDER BY id ASC";
							}else{
								$sql = "SELECT cuenta, nombre, tipo FROM pptocuentas WHERE  clasificacion != 'ingresos' AND vigencia = '$vigencia' AND tipo = 'Auxiliar' ORDER BY cuenta ASC";
							}
							
                            $res = mysqli_query($linkbd_V7, $sql);
                            $i = 0;
                
                            while($row = mysqli_fetch_row($res))
                            {
								$valorTotal = 0;
                                $valorCxp = 0;
                                $valorEgreso = 0;
                                $rubro = '';
                                $rubro = $row[0];

								
                                if($vigencia >= 2021){
									$linkbd = conectar_v7();
									$arregloGastos = reporteGastosCcpetCgr($rubro, '%', '', $vigencia, $fechai, $fechaf);
								}else{
									$linkbd = conectar_bd();
									$arregloGastos = generaReporteGastos($rubro, $vigencia, $fechai, $fechaf);
								}

								

								foreach ($arregloGastos as $key => $value) 
					        	{
										/* echo "<br><br>";
										var_dump($value);
										echo "<br><br>";
										var_dump($k); */

										/* echo "<br><br> $key <br><br>";
										
										var_dump($value);
										echo "<br><br> $k <br><br>"; */

										if($vigencia >= 2021){
											$nombreCuenta = buscaNombreCuentaCCPET($row[0]);
											$nomFuente = str_replace(","," ",buscafuenteccpet($k));
											$nomFuente = str_replace(";", " ", $nomFuente);

										}else{
											$nombreCuenta = str_replace(","," ",buscaNombreCuenta($row[0], $vigencia));

											$nomFuente = str_replace(","," ",buscafuenteppto($row[0], $vigencia));
											$nomFuente = str_replace(";", " ", $nomFuente);
										}
										
										$cuentas[$row[0]]["numCuenta"]=$row[0];
										$cuentas[$row[0]]["nomCuenta"]="$nombreCuenta";
										$cuentas[$row[0]]["presuInicial"]=$arregloGastos[0];
										$cuentas[$row[0]]["adicion"]=$arregloGastos[1];
										$cuentas[$row[0]]["reduccion"]=$arregloGastos[2];
										$cuentas[$row[0]]["credito"]=$arregloGastos[3];
										$cuentas[$row[0]]["conCredito"]=$arregloGastos[4];
										$cuentas[$row[0]]["cdp"]=$arregloGastos[6];
										$cuentas[$row[0]]["rp"]=$arregloGastos[7];
										$cuentas[$row[0]]["cxp"]=$arregloGastos[8];
										$cuentas[$row[0]]["egreso"]=$arregloGastos[9];
										$cuentas[$row[0]]["fuente"]=$nomFuente;
									
								}
								
							}
							
							
                            $i = 0;

							$con = 1;

							$vigenciaGasto = $vigenciasDelGasto[1];

							$sqlrEnt = "SELECT nit, razonsocial FROM configbasica";
							$respEnt = mysqli_query($linkbd_V7, $sqlrEnt);
							$rowEnt = mysqli_fetch_row($respEnt);


                            foreach ($cuentas as $key => $value) 
					        {

								$presupuestoDefinitivo = 0;
								$saldo = 0;
								$numeroCuenta=$cuentas[$key]['numCuenta'];
                                $nombreCuenta=$cuentas[$key]['nomCuenta'];
                                $presupuestoInicial=$cuentas[$key]['presuInicial'];
                                $adicion=$cuentas[$key]['adicion'];
                                $reduccion=$cuentas[$key]['reduccion'];
                                $credito=$cuentas[$key]['credito'];
                                $contracredito=$cuentas[$key]['conCredito'];

								$presupuestoDefinitivo = $presupuestoInicial + $adicion - $reduccion + $credito - $contracredito;
                                //$presupuestoDefinitivo=$cuentas[$key]['presuDefinitivo'];
                                $cdp=$cuentas[$key]['cdp'];
                                $rp=$cuentas[$key]['rp'];
                                $cxp=$cuentas[$key]['cxp'];
                                $egreso=$cuentas[$key]['egreso'];

								$saldo = $presupuestoDefinitivo - $cdp;
                                //$saldo=$cuentas[$key]['saldo']; 
                                $fuente=$cuentas[$key]['fuente']; 
                                $style='';

								$compromisoEnEjecucion = $rp - $cxp;
								$enEjecucion = round((($rp/$cxp)*100),-2);
                                $cuentasPorPagar = $cxp - $egreso;

								/* fputs($Descriptor1,"Anio| Vigencia| NIT_Entidad| Nombre_Entidad| Cod_Cuenta| Fuente_Financiacion| Nombre_Cuenta| Macro_campo_nivel_agregado| Apropiacion_Inicial| Adiciones_periodo| Adiciones_Acumuladas| Reducciones_Periodo|Reducciones_Acumuladas|Creditos_Periodo|Creditos_Acumulados|Contracreditos_Periodo|Contracreditos_Acumulados|Apropiacion_Definitiva|Aplazamientos|Desaplazamientos|CDPs_Periodo|CDPs_Acumulado|Compromisos_Periodo|Compromisos_Acumulado|Obligaciones_Periodo|Obligaciones_Acumulado|Pagos_Periodo|Pagos_Acumulado|Saldo_Disponible|Porcentaje_Ejecucion|Fecha_Reporte\r\n"); */
								$nombreCuenta = str_replace("|"," ",$nombreCuenta);
								echo "<tr class='$iter'>
											<td>$vigencia</td>								
											<td>$vigenciaGasto</td	>						
											<td>$rowEnt[0]</td>								
											<td>$rowEnt[1]</td>											
											<td>$numeroCuenta</td>								
											<td>$fuente</td>														
											<td>".str_replace(";"," ",$nombreCuenta)."</td>			
											<td>DA</td>			
											<td>".round($presupuestoInicial,2)."</td>
											<td>$adicion</td>										
											<td>$adicion</td>										
											<td>$reduccion</td>										
											<td>$reduccion</td>	 											
											<td>$credito</td>												
											<td>$credito</td>							
											<td>$contracredito</td>								
											<td>$contracredito</td>								
											<td>$presupuestoDefinitivo</td>								
											<td>0</td>									
											<td>0</td>									
											<td>$cdp</td>
											<td>$cdp</td>
											<td>$rp</td>
											<td>$rp</td>
											<td>$cxp</td>
											<td>$cxp</td>
											<td>$egreso</td>
											<td>$egreso</td>
											<td>$saldo</td>
											<td>$enEjecucion</td>
											<td>$fechaf</td>
										</tr>";

									fputs($Descriptor1,$vigencia."|".$vigenciaGasto."|".$rowEnt[0]."|".$rowEnt[1]."|".$numeroCuenta."|".$fuente."|".str_replace(";"," ",$nombreCuenta)."|DA|".round($presupuestoInicial,2)."|".round($adicion,2)."|".round($adicion,2)."|".round($reduccion,2)."|".round($reduccion,2)."|".round($credito,2)."|".round($credito,2)."|".round($contracredito,2)."|".round($contracredito,2)."|".round($presupuestoDefinitivo,2)."|0|0|".round($cdp,2)."|".round($cdp,2)."|".round($rp,2)."|".round($rp,2)."|".round($cxp,2)."|".round($cxp,2)."|".round($egreso,2)."|".round($egreso,2)."|".round($saldo,2)."|".$enEjecucion."|".$fechaf."\r\n");
				
									$con+=1;
									$aux=$iter;
									$iter=$iter2;
									$iter2=$aux;
                                
                                

                                $i++;
                            } 
                            
							echo "<script>document.getElementById('divcarga').style.display='none';</script>";
							echo"</table>";
							fclose($Descriptor1);

						break;

						case 8:

							$crit3 = " ";
							$crit4 = " ";	
							$namearch = "archivos/FORMATO_".$informes[$_POST['reporte']].".csv";
							$_POST['nombrearchivo'] = $namearch;

							$Descriptor1 = fopen($namearch,"w+"); 
                            fputs($Descriptor1,"Anio_reporte| Vigencia| NIT_Entidad_reportante| Nombre_Entidad_reportante| Codigo_Presupuestal| Macro_campo_nivel_agregado| Fecha_de_Recaudo| Numero_Recibo| Numero_reconocimiento| Nit_tercero| Nombre_tercero| Concepto_Recaudo|Valor|Cuenta_Bancaria_Dest|Nombre_banco|No_Cuenta_Banco_Orig|Cta_Contable\r\n");
                            
                            $sqlr = '';

							if ($_POST['fecha'] != "" && $_POST['fecha2'] != "" )
                            {
								preg_match("/([0-9]{2})\/([0-9]{2})\/([0-9]{4})/", $_POST['fecha'], $fecha);
								$fechai = $fecha[3]."-".$fecha[2]."-".$fecha[1];

								preg_match("/([0-9]{2})\/([0-9]{2})\/([0-9]{4})/", $_POST['fecha2'], $fecha);
								$fechaf = $fecha[3]."-".$fecha[2]."-".$fecha[1];

                            	$crit3 = " AND TB1.fecha BETWEEN '$fechai' AND '$fechaf' ";

								$sqlr = "SELECT TB1.vigencia, TB2.cuenta, TB1.fecha, TB1.id_recibos, TB1.id_recaudo, SUM(TB2.valor), TB1.cuentabanco, TB1.tipo  FROM tesoreciboscaja TB1, pptorecibocajappto TB2 WHERE TB1.id_recibos = TB2.idrecibo AND TB1.estado!='N' $crit3 GROUP BY TB2.cuenta, TB1.id_recibos";

								//$sqlr = "SELECT TB1.vigencia, TB2.cuentap, TB1.id_orden, TB1.fecha, TB1.id_rp, SUM(TB2.valor), TB1.tercero, TB1.conceptorden, TB2.codigo_vigenciag FROM tesoordenpago AS TB1, tesoordenpago_det AS TB2 WHERE  TB1.id_orden = TB2.id_orden AND TB1.estado != 'N' $crit3 GROUP BY TB1.vigencia, TB2.cuentap, TB1.id_orden";
                            }
							
							$resp = mysqli_query($linkbd_V7, $sqlr);
                            $ntr = mysqli_num_rows($resp);
                            $con = 1;

							$sqlrEnt = "SELECT nit, razonsocial FROM configbasica";
							$respEnt = mysqli_query($linkbd_V7, $sqlrEnt);
							$rowEnt = mysqli_fetch_row($respEnt);

							

                            echo "<table class='inicio' align='center' width='80%'>
									<tr>
										<td colspan='17' class='titulos'>.: Relaci&oacute;n de Ingresos:</td>
									</tr>
									<tr>
										<td colspan='5'>Ingresos Encontrados: $ntr</td>
									</tr>
									<tr>
										<td class='titulos2'>Anio_reporte</td>
										<td class='titulos2'>Vigencia</td>
										<td class='titulos2'>NIT_Entidad_reportante</td>
										<td class='titulos2'>Nombre_Entidad_reportante</td>
										<td class='titulos2'>Codigo_Presupuestal</td>
										<td class='titulos2'>Macro_campo_nivel_agregado</td>
										<td class='titulos2'>Fecha_de_Recaudo</td>
										<td class='titulos2'>Numero_Recibo</td>
										<td class='titulos2'>Numero_reconocimiento</td>
										<td class='titulos2'>Nit_tercero</td>
										<td class='titulos2'>Nombre_tercero</td>
										<td class='titulos2'>Concepto_Recaudo</td>
										<td class='titulos2'>Valor</td>
										<td class='titulos2'>Cuenta_Bancaria_Dest</td>
										<td class='titulos2'>Nombre_banco</td>
										<td class='titulos2'>No_Cuenta_Banco_Orig</td>
										<td class='titulos2'>Cta_Contable</td>
									</tr>";	
		
							while ($row = mysqli_fetch_row($resp)) 
		 					{
								if($row[5] > 0){
									$vigenciaGasto = '';
									$tipoDeCuenta = '';
									$nrub = '';

									$vigenciaGasto = $vigenciasDelGasto[1];
									
									if($row[7] == 1){
										$sqlr_rec = "SELECT tercero, concepto FROM tesoliquidapredial WHERE idpredial = '$row[4]'";
									}else if($row[7] == 2){
										$sqlr_rec = "SELECT tercero FROM tesoindustria WHERE id_industria = '$row[4]'";
									}else{
										$sqlr_rec = "SELECT tercero, concepto FROM tesorecaudos WHERE id_recaudo = '$row[4]'";
									}

									$resp_rec = mysqli_query($linkbd_V7, $sqlr_rec);
									$row_rec = mysqli_fetch_row($resp_rec);
									
									$tercero = buscatercero($row_rec[0]);
									if($tercero == ''){
										$sqlrTer = "SELECT nombrepropietario FROM tesopredios WHERE documento = '$row_rec[0]'";
										$respTer = mysqli_query($linkbd_V7, $sqlrTer);
										$rowTer = mysqli_fetch_row($respTer);
										$tercero = $rowTer[0];
									}

									$concepto = '';

									$concepto = $row_rec[1];
									if($row[7] == 2){
										$concepto = 'Industria y comercio';
									}


									$sqlrBanco = "SELECT tercero, ncuentaban FROM tesobancosctas WHERE cuenta = '$row[6]'";
									$respBanco = mysqli_query($linkbd_V7, $sqlrBanco);
									$rowBanco = mysqli_fetch_row($respBanco);

									$banco = buscatercero($rowBanco[0]);
									
									echo "<tr style = 'background-color: blue' class='$iter'>
											<td>$row[0]</td>
											<td>$vigenciaGasto</td>
											<td>$rowEnt[0]</td>
											<td>$rowEnt[1]</td>
											<td>$row[1]</td>
											<td>DA</td>
											<td>$row[2]</td>
											<td>$row[3]</td>
											<td>$row[4]</td>
											<td>$row_rec[0]</td>
											<td>$tercero</td>
											<td>$concepto</td>
											<td>$row[5]</td>
											<td>$rowBanco[1]</td>
											<td>$banco</td>
											<td></td>
											<td>$row[6]</td>
										</tr>";

									fputs($Descriptor1,$row[0]."|".$vigenciaGasto."|".$rowEnt[0]."|".$rowEnt[1]."|".$row[1]."|DA|".$row[2]."|".$row[3]."|".$row[4]."|".$row_rec[0]."|".$tercero."|".str_replace(","," ",$concepto)."|".round($row[5],2)."|".$rowBanco[1]."|".$banco."||".$row[6]."\r\n");
				
									$con+=1;
									$aux=$iter;
									$iter=$iter2;
									$iter2=$aux;
								}
								
 							}

							$sqlr = '';

							

							$crit3 = " AND TB1.fecha BETWEEN '$fechai' AND '$fechaf' ";

							$sqlr = "SELECT TB1.vigencia, TB2.cuenta, TB1.fecha, TB1.id_recibos, TB1.id_recaudo, SUM(TB2.valor), TB1.cuentabanco, TB1.tipo  FROM tesosinreciboscaja TB1, pptosinrecibocajappto TB2 WHERE TB1.id_recibos = TB2.idrecibo AND TB1.estado!='N' $crit3 GROUP BY TB2.cuenta, TB1.id_recibos";

                            
							
							$resp = mysqli_query($linkbd_V7, $sqlr);
                            $ntr = mysqli_num_rows($resp);
                            $con = 1;

							$sqlrEnt = "SELECT nit, razonsocial FROM configbasica";
							$respEnt = mysqli_query($linkbd_V7, $sqlrEnt);
							$rowEnt = mysqli_fetch_row($respEnt);

								
		
							while ($row = mysqli_fetch_row($resp)) 
		 					{
								if($row[5] > 0){
									$vigenciaGasto = '';
									$tipoDeCuenta = '';
									$nrub = '';

									$vigenciaGasto = $vigenciasDelGasto[1];
									
									
									$sqlr_rec = "SELECT tercero, concepto FROM tesosinrecaudos WHERE id_recaudo = '$row[4]'";
									

									$resp_rec = mysqli_query($linkbd_V7, $sqlr_rec);
									$row_rec = mysqli_fetch_row($resp_rec);
									
									$tercero = buscatercero($row_rec[0]);

									$concepto = '';

									$concepto = $row_rec[1];
									


									$sqlrBanco = "SELECT tercero, ncuentaban FROM tesobancosctas WHERE cuenta = '$row[6]'";
									$respBanco = mysqli_query($linkbd_V7, $sqlrBanco);
									$rowBanco = mysqli_fetch_row($respBanco);

									$banco = buscatercero($rowBanco[0]);
									



									echo "<tr style = 'background-color: red' class='$iter'>
											<td>$row[0]</td>
											<td>$vigenciaGasto</td>
											<td>$rowEnt[0]</td>
											<td>$rowEnt[1]</td>
											<td>$row[1]</td>
											<td>DA</td>
											<td>$row[2]</td>
											<td>$row[3]</td>
											<td>$row[4]</td>
											<td>$row_rec[0]</td>
											<td>$tercero</td>
											<td>$concepto</td>
											<td>$row[5]</td>
											<td>$rowBanco[1]</td>
											<td>$banco</td>
											<td></td>
											<td>$row[6]</td>
										</tr>";

									fputs($Descriptor1,$row[0]."|".$vigenciaGasto."|".$rowEnt[0]."|".$rowEnt[1]."|".$row[1]."|DA|".$row[2]."|".$row[3]."|".$row[4]."|".$row_rec[0]."|".$tercero."|".str_replace(","," ",$concepto)."|".round($row[5],2)."|".$rowBanco[1]."|".$banco."||".$row[6]."\r\n");
				
									$con+=1;
									$aux=$iter;
									$iter=$iter2;
									$iter2=$aux;
								}
								
 							}

							$sqlr = '';

							

							$crit3 = " AND TB1.fecha BETWEEN '$fechai' AND '$fechaf' ";

							$sqlr = "SELECT TB1.vigencia, TB2.cuenta, TB1.fecha, TB1.id_egreso, TB1.id_orden, SUM(TB2.valor), TB1.banco, TB1.concepto, TB1.tercero  FROM tesoegresos TB1, pptoretencionpago TB2 WHERE TB1.id_egreso = TB2.idrecibo AND TB1.estado!='N' $crit3 GROUP BY TB2.cuenta, TB1.id_egreso";

                            
							
							$resp = mysqli_query($linkbd_V7, $sqlr);
                            $ntr = mysqli_num_rows($resp);
                            $con = 1;

							$sqlrEnt = "SELECT nit, razonsocial FROM configbasica";
							$respEnt = mysqli_query($linkbd_V7, $sqlrEnt);
							$rowEnt = mysqli_fetch_row($respEnt);

								
		
							while ($row = mysqli_fetch_row($resp)) 
		 					{
								if($row[5] > 0){
									$vigenciaGasto = '';
									$tipoDeCuenta = '';
									$nrub = '';

									$vigenciaGasto = $vigenciasDelGasto[1];
									
							
									$tercero = buscatercero($row[8]);

									$concepto = '';

									$concepto = $row[7];
									


									$sqlrBanco = "SELECT tercero, ncuentaban FROM tesobancosctas WHERE cuenta = '$row[6]'";
									$respBanco = mysqli_query($linkbd_V7, $sqlrBanco);
									$rowBanco = mysqli_fetch_row($respBanco);

									$banco = buscatercero($rowBanco[0]);
									


									echo "<tr style = 'background-color: yellow' class='$iter'>
											<td>$row[0]</td>
											<td>$vigenciaGasto</td>
											<td>$rowEnt[0]</td>
											<td>$rowEnt[1]</td>
											<td>$row[1]</td>
											<td>DA</td>
											<td>$row[2]</td>
											<td>$row[3]</td>
											<td>$row[4]</td>
											<td>$row[8]</td>
											<td>$tercero</td>
											<td>$concepto</td>
											<td>$row[5]</td>
											<td>$rowBanco[1]</td>
											<td>$banco</td>
											<td></td>
											<td>$row[6]</td>
										</tr>";

									fputs($Descriptor1,$row[0]."|".$vigenciaGasto."|".$rowEnt[0]."|".$rowEnt[1]."|".$row[1]."|DA|".$row[2]."|".$row[3]."|".$row[4]."|".$row[8]."|".$tercero."|".str_replace(","," ",$concepto)."|".round($row[5],2)."|".$rowBanco[1]."|".$banco."||".$row[6]."\r\n");
				
									$con+=1;
									$aux=$iter;
									$iter=$iter2;
									$iter2=$aux;
								}
								
 							}

							$sqlr = '';

							$crit3 = " AND TB1.fecha BETWEEN '$fechai' AND '$fechaf' ";

							$sqlr = "SELECT TB1.vigencia, TB2.cuenta, TB1.fecha, TB1.consvigencia, TB1.consvigencia, SUM(TB2.valor), TB1.objeto  FROM pptosuperavit TB1, pptosuperavit_detalle TB2 WHERE TB1.consvigencia = TB2.consvigencia AND TB1.vigencia = TB2.vigencia AND TB1.estado!='N' $crit3 GROUP BY TB2.cuenta, TB1.consvigencia, TB1.vigencia";

                            
							
							$resp = mysqli_query($linkbd_V7, $sqlr);
                            $ntr = mysqli_num_rows($resp);
                            $con = 1;

							$sqlrEnt = "SELECT nit, razonsocial FROM configbasica";
							$respEnt = mysqli_query($linkbd_V7, $sqlrEnt);
							$rowEnt = mysqli_fetch_row($respEnt);

								
		
							while ($row = mysqli_fetch_row($resp)) 
		 					{
								if($row[5] > 0){
									$vigenciaGasto = '';
									$tipoDeCuenta = '';
									$nrub = '';

									$vigenciaGasto = $vigenciasDelGasto[1];
									
							
									$tercero = buscatercero($rowEnt[0]);

									$concepto = '';

									$concepto = $row[6];
									

									

									echo "<tr style = 'background-color: green' class='$iter'>
											<td>$row[0]</td>
											<td>$vigenciaGasto</td>
											<td>$rowEnt[0]</td>
											<td>$rowEnt[1]</td>
											<td>$row[1]</td>
											<td>DA</td>
											<td>$row[2]</td>
											<td>$row[3]</td>
											<td>$row[4]</td>
											<td>$rowEnt[0]</td>
											<td>$tercero</td>
											<td>$concepto</td>
											<td>$row[5]</td>
											<td></td>
											<td></td>
											<td></td>
											<td></td>
										</tr>";

									fputs($Descriptor1,$row[0]."|".$vigenciaGasto."|".$rowEnt[0]."|".$rowEnt[1]."|".$row[1]."|DA|".$row[2]."|".$row[3]."|".$row[4]."|".$rowEnt[0]."|".$tercero."|".str_replace(","," ",$concepto)."|".round($row[5],2)."||||\r\n");
				
									$con+=1;
									$aux=$iter;
									$iter=$iter2;
									$iter2=$aux;
								}
								
 							}

							$sqlr = '';

						

							$crit3 = " AND TB1.fecha BETWEEN '$fechai' AND '$fechaf' ";

							$sqlr = "SELECT TB1.vigencia, TB2.cuenta, TB1.fecha, TB1.id_recibos, TB1.id_recaudo, SUM(TB2.valor), TB1.cuentabanco, TB1.tipo  FROM tesosinreciboscajasp TB1, pptosinrecibocajaspppto TB2 WHERE TB1.id_recibos = TB2.idrecibo AND TB1.estado!='N' $crit3 GROUP BY TB2.cuenta, TB1.id_recibos";

							
							
							$resp = mysqli_query($linkbd_V7, $sqlr);
							$ntr = mysqli_num_rows($resp);
							$con = 1;

							$sqlrEnt = "SELECT nit, razonsocial FROM configbasica";
							$respEnt = mysqli_query($linkbd_V7, $sqlrEnt);
							$rowEnt = mysqli_fetch_row($respEnt);

								
		
							while ($row = mysqli_fetch_row($resp)) 
							{
								if($row[5] > 0){
									$vigenciaGasto = '';
									$tipoDeCuenta = '';
									$nrub = '';

									$vigenciaGasto = $vigenciasDelGasto[1];
									
									
									$sqlr_rec = "SELECT tercero, concepto FROM tesosinrecaudossp WHERE id_recaudo = '$row[4]'";
									

									$resp_rec = mysqli_query($linkbd_V7, $sqlr_rec);
									$row_rec = mysqli_fetch_row($resp_rec);
									
									$tercero = buscatercero($row_rec[0]);

									$concepto = '';

									$concepto = $row_rec[1];
									


									$sqlrBanco = "SELECT tercero, ncuentaban FROM tesobancosctas WHERE cuenta = '$row[6]'";
									$respBanco = mysqli_query($linkbd_V7, $sqlrBanco);
									$rowBanco = mysqli_fetch_row($respBanco);

									$banco = buscatercero($rowBanco[0]);
									

									echo "<tr style = 'background-color: pink' class='$iter'>
											<td>$row[0]</td>
											<td>$vigenciaGasto</td>
											<td>$rowEnt[0]</td>
											<td>$rowEnt[1]</td>
											<td>$row[1]</td>
											<td>DA</td>
											<td>$row[2]</td>
											<td>$row[3]</td>
											<td>$row[4]</td>
											<td>$row_rec[0]</td>
											<td>$tercero</td>
											<td>$concepto</td>
											<td>$row[5]</td>
											<td>$rowBanco[1]</td>
											<td>$banco</td>
											<td></td>
											<td>$row[6]</td>
										</tr>";

									fputs($Descriptor1,$row[0]."|".$vigenciaGasto."|".$rowEnt[0]."|".$rowEnt[1]."|".$row[1]."|DA|".$row[2]."|".$row[3]."|".$row[4]."|".$row_rec[0]."|".$tercero."|".str_replace(","," ",$concepto)."|".round($row[5],2)."|".$rowBanco[1]."|".$banco."||".$row[6]."\r\n");
				
									$con+=1;
									$aux=$iter;
									$iter=$iter2;
									$iter2=$aux;
								}
								 
							}

							$sqlr = '';

							$crit3 = " AND TB1.fecha BETWEEN '$fechai' AND '$fechaf' ";

							$sqlr = "SELECT TB1.vigencia, TB2.cuenta, TB1.fecha, TB1.id_recaudo, TB1.id_recaudo, SUM(TB2.valor), TB1.concepto, TB1.tercero  FROM tesossfingreso_cab TB1, pptoingssf TB2 WHERE TB1.id_recaudo = TB2.idrecibo  AND TB1.estado!='N' $crit3 GROUP BY TB2.cuenta, TB1.id_recaudo";
							
							
							$resp = mysqli_query($linkbd_V7, $sqlr);
                            $ntr = mysqli_num_rows($resp);
                            $con = 1;

							$sqlrEnt = "SELECT nit, razonsocial FROM configbasica";
							$respEnt = mysqli_query($linkbd_V7, $sqlrEnt);
							$rowEnt = mysqli_fetch_row($respEnt);

								
		
							while ($row = mysqli_fetch_row($resp)) 
		 					{
								if($row[5] > 0){
									$vigenciaGasto = '';
									$tipoDeCuenta = '';
									$nrub = '';

									$vigenciaGasto = $vigenciasDelGasto[1];
									
							
									$tercero = buscatercero($row[7]);

									$concepto = '';

									$concepto = $row[6];
									

									

									echo "<tr style = 'background-color: tomato' class='$iter'>
											<td>$row[0]</td>
											<td>$vigenciaGasto</td>
											<td>$rowEnt[0]</td>
											<td>$rowEnt[1]</td>
											<td>$row[1]</td>
											<td>DA</td>
											<td>$row[2]</td>
											<td>$row[3]</td>
											<td>$row[4]</td>
											<td>$row[7]</td>
											<td>$tercero</td>
											<td>$concepto</td>
											<td>$row[5]</td>
											<td></td>
											<td></td>
											<td></td>
											<td></td>
										</tr>";

									fputs($Descriptor1,$row[0]."|".$vigenciaGasto."|".$rowEnt[0]."|".$rowEnt[1]."|".$row[1]."|DA|".$row[2]."|".$row[3]."|".$row[4]."|".$row[7]."|".$tercero."|".str_replace(","," ",$concepto)."|".round($row[5],2)."||||\r\n");
				
									$con+=1;
									$aux=$iter;
									$iter=$iter2;
									$iter2=$aux;
								}
								
 							}

							$sqlr = '';

					

							$crit3 = " AND TB1.fecha BETWEEN '$fechai' AND '$fechaf' ";

							$sqlr = "SELECT TB1.vigencia, TB2.cuenta, TB1.fecha, TB1.id_recaudo, TB1.id_recaudo, SUM(TB2.valor), TB1.ncuentaban, TB1.concepto, TB1.tercero  FROM tesorecaudotransferencia TB1, pptoingtranppto TB2 WHERE TB1.id_recaudo = TB2.idrecibo AND TB1.estado!='N' AND TB2.cuenta != '01-TI.B.2.1' $crit3 GROUP BY TB2.cuenta, TB1.id_recaudo";
							
							
							
							$resp = mysqli_query($linkbd_V7, $sqlr);
							$ntr = mysqli_num_rows($resp);
							$con = 1;

							$sqlrEnt = "SELECT nit, razonsocial FROM configbasica";
							$respEnt = mysqli_query($linkbd_V7, $sqlrEnt);
							$rowEnt = mysqli_fetch_row($respEnt);

								
		
							while ($row = mysqli_fetch_row($resp)) 
							{
								if($row[5] > 0){
									$vigenciaGasto = '';
									$tipoDeCuenta = '';
									$nrub = '';

									$vigenciaGasto = $vigenciasDelGasto[1];
									
									
									
									
									$tercero = buscatercero($row[8]);

									$concepto = '';

									$concepto = $row[7];
									


									$sqlrBanco = "SELECT tercero, ncuentaban, cuenta FROM tesobancosctas WHERE ncuentaban = '$row[6]'";
									$respBanco = mysqli_query($linkbd_V7, $sqlrBanco);
									$rowBanco = mysqli_fetch_row($respBanco);

									$banco = buscatercero($rowBanco[0]);
									

									echo "<tr style = 'background-color: aqua' class='$iter'>
											<td>$row[0]</td>
											<td>$vigenciaGasto</td>
											<td>$rowEnt[0]</td>
											<td>$rowEnt[1]</td>
											<td>$row[1]</td>
											<td>DA</td>
											<td>$row[2]</td>
											<td>$row[3]</td>
											<td>$row[4]</td>
											<td>$row[8]</td>
											<td>$tercero</td>
											<td>$concepto</td>
											<td>$row[5]</td>
											<td>$rowBanco[1]</td>
											<td>$banco</td>
											<td></td>
											<td>$row[6]</td>
										</tr>";

									fputs($Descriptor1,$row[0]."|".$vigenciaGasto."|".$rowEnt[0]."|".$rowEnt[1]."|".$row[1]."|DA|".$row[2]."|".$row[3]."|".$row[4]."|".$row[8]."|".$tercero."|".str_replace(","," ",$concepto)."|".round($row[5],2)."|".$rowBanco[1]."|".$banco."||".$rowBanco[3]."\r\n");
				
									$con+=1;
									$aux=$iter;
									$iter=$iter2;
									$iter2=$aux;
								}
								
							}

							echo "<script>document.getElementById('divcarga').style.display='none';</script>";
							echo"</table>";
							fclose($Descriptor1);
						break;

						case 9: //Ejecucion presupuestal de gastos 2022

							$crit3 = " ";
							$crit4 = " ";	
							$namearch = "archivos/FORMATO_".$informes[$_POST['reporte']].".csv";
							$_POST['nombrearchivo'] = $namearch;

							$Descriptor1 = fopen($namearch,"w+"); 
                            fputs($Descriptor1,"Anio| Vigencia| NIT_Entidad| Nombre_Entidad| Cod_Cuenta| Fuente_Financiacion| Nombre_Cuenta| Macro_campo_nivel_agregado| Apropiacion_Inicial| Adiciones_periodo| Adiciones_Acumuladas| Reducciones_Periodo|Reducciones_Acumuladas|Creditos_Periodo|Creditos_Acumulados|Contracreditos_Periodo|Contracreditos_Acumulados|Apropiacion_Definitiva|Aplazamientos|Desaplazamientos|CDPs_Periodo|CDPs_Acumulado|Compromisos_Periodo|Compromisos_Acumulado|Obligaciones_Periodo|Obligaciones_Acumulado|Pagos_Periodo|Pagos_Acumulado|Saldo_Disponible|Porcentaje_Ejecucion|Fecha_Reporte\r\n");

							echo "<table class='inicio' align='center' width='80%'>
									<tr>
										<td colspan='21' class='titulos'>.: Relaci&oacute;n de Compromisos:</td>
									</tr>
									<tr>
										<td class='titulos2'>Anio</td>
										<td class='titulos2'>Vigencia</td>
										<td class='titulos2'>NIT_Entidad</td>
										<td class='titulos2'>Nombre_Entidad</td>
										<td class='titulos2'>Cod_Cuenta</td>
										<td class='titulos2'>Fuente_Financiacion</td>
										<td class='titulos2'>Nombre_Cuenta</td>
										<td class='titulos2'>Macro_campo_nivel_agregado</td>
										<td class='titulos2'>Apropiacion_Inicial</td>
										<td class='titulos2'>Adiciones_periodo</td>
										<td class='titulos2'>Adiciones_Acumuladas</td>
										<td class='titulos2'>Reducciones_Periodo</td>
										<td class='titulos2'>Reducciones_Acumuladas</td>
										<td class='titulos2'>Creditos_Periodo</td>
										<td class='titulos2'>Creditos_Acumulados</td>
										<td class='titulos2'>Contracreditos_Periodo</td>
										<td class='titulos2'>Contracreditos_Acumulados</td>
										<td class='titulos2'>Apropiacion_Definitiva</td>
										<td class='titulos2'>Aplazamientos</td>
										<td class='titulos2'>Desaplazamientos</td>
										<td class='titulos2'>CDPs_Periodo</td>
										<td class='titulos2'>CDPs_Acumulado</td>
										<td class='titulos2'>Compromisos_Periodo</td>
										<td class='titulos2'>Compromisos_Acumulado</td>
										<td class='titulos2'>Obligaciones_Periodo</td>
										<td class='titulos2'>Obligaciones_Acumulado</td>
										<td class='titulos2'>Pagos_Periodo</td>
										<td class='titulos2'>Pagos_Acumulado</td>
										<td class='titulos2'>Saldo_Disponible</td>
										<td class='titulos2'>Porcentaje_Ejecucion</td>
										<td class='titulos2'>Fecha_Reporte</td>
									</tr>";	
                            
                            $sqlr = '';
							$vigencia = '';

							if ($_POST['fecha'] != "" && $_POST['fecha2'] != "" )
                            {
								preg_match("/([0-9]{2})\/([0-9]{2})\/([0-9]{4})/", $_POST['fecha'], $fecha);
								$fechai = $fecha[3]."-".$fecha[2]."-".$fecha[1];

								preg_match("/([0-9]{2})\/([0-9]{2})\/([0-9]{4})/", $_POST['fecha2'], $fecha);
								$fechaf = $fecha[3]."-".$fecha[2]."-".$fecha[1];

								$vigencia = $fecha[3];

                            	$crit3 = " AND TB1.fecha BETWEEN '$fechai' AND '$fechaf' ";
                            }

							$sql = "SELECT codigo, nombre FROM ccpproyectospresupuesto WHERE  vigencia = '$vigencia' ORDER BY id ASC";
                            $res = mysqli_query($linkbd_V7, $sql);
                            $i = 0;
                
                            while($row = mysqli_fetch_row($res))
                            {
								$valorTotal = 0;
                                $valorCxp = 0;
                                $valorEgreso = 0;
                                $rubro = '';
                                $rubro = $row[0];

								
								$linkbd = conectar_v7();
								$linkbd -> set_charset("utf8");
								$arregloGastos = reporteProyectos2022($rubro, '%', '', $vigencia, $fechai, $fechaf);
								

								

								foreach ($arregloGastos as $key => $value) 
					        	{
									
									foreach($value as $k => $valor)
									{
										/* echo "<br><br>";
										var_dump($value);
										echo "<br><br>";
										var_dump($k); */

										/* echo "<br><br> $key <br><br>";
										
										var_dump($value);
										echo "<br><br> $k <br><br>"; */

										$nombreCuenta = $row[1];
										$nomFuente = str_replace(","," ",buscafuenteccpet($k));
										$nomFuente = str_replace(";", " ", $nomFuente);

										
										$cuentas[$row[0]."".$k]["numCuenta"]=$row[0];
										$cuentas[$row[0]."".$k]["nomCuenta"]="$nombreCuenta";
										$cuentas[$row[0]."".$k]["presuInicial"]=$arregloGastos[0][$k];
										$cuentas[$row[0]."".$k]["adicion"]=$arregloGastos[1][$k];
										$cuentas[$row[0]."".$k]["reduccion"]=$arregloGastos[2][$k];
										$cuentas[$row[0]."".$k]["credito"]=$arregloGastos[3][$k];
										$cuentas[$row[0]."".$k]["conCredito"]=$arregloGastos[4][$k];
										$cuentas[$row[0]."".$k]["cdp"]=$arregloGastos[5][$k];
										$cuentas[$row[0]."".$k]["rp"]=$arregloGastos[6][$k];
										$cuentas[$row[0]."".$k]["cxp"]=$arregloGastos[7][$k];
										$cuentas[$row[0]."".$k]["egreso"]=$arregloGastos[8][$k];
										$cuentas[$row[0]."".$k]["fuente"]=$k;
									}
								}
								
							}

							$sql = "SELECT codigo, nombre, tipo FROM cuentasccpet WHERE municipio=1 AND version = '$maxVersionGastos' AND tipo = 'C' ORDER BY id ASC";
                            $res = mysqli_query($linkbd_V7, $sql);
                            $i = 0;
                
                            while($row = mysqli_fetch_row($res))
                            {
								$valorTotal = 0;
                                $valorCxp = 0;
                                $valorEgreso = 0;
                                $rubro = '';
                                $rubro = $row[0];

								
								$linkbd = conectar_v7();
								$linkbd -> set_charset("utf8");
								$arregloGastos = reporteGastosCcpetCgr2022($rubro, '%', '', $vigencia, $fechai, $fechaf);
								

								

								foreach ($arregloGastos as $key => $value) 
					        	{
									
									foreach($value as $k => $valor)
									{
										/* echo "<br><br>";
										var_dump($value);
										echo "<br><br>";
										var_dump($k); */

										/* echo "<br><br> $key <br><br>";
										
										var_dump($value);
										echo "<br><br> $k <br><br>"; */

										$nombreCuenta = buscaNombreCuentaCCPET($row[0]);
										$nomFuente = str_replace(","," ",buscafuenteccpet($k));
										$nomFuente = str_replace(";", " ", $nomFuente);

										
										
										$cuentas[$row[0]."".$k]["numCuenta"]=$row[0];
										$cuentas[$row[0]."".$k]["nomCuenta"]="$nombreCuenta";
										$cuentas[$row[0]."".$k]["presuInicial"]=$arregloGastos[0][$k];
										$cuentas[$row[0]."".$k]["adicion"]=$arregloGastos[1][$k];
										$cuentas[$row[0]."".$k]["reduccion"]=$arregloGastos[2][$k];
										$cuentas[$row[0]."".$k]["credito"]=$arregloGastos[3][$k];
										$cuentas[$row[0]."".$k]["conCredito"]=$arregloGastos[4][$k];
										$cuentas[$row[0]."".$k]["cdp"]=$arregloGastos[5][$k];
										$cuentas[$row[0]."".$k]["rp"]=$arregloGastos[6][$k];
										$cuentas[$row[0]."".$k]["cxp"]=$arregloGastos[7][$k];
										$cuentas[$row[0]."".$k]["egreso"]=$arregloGastos[8][$k];
										$cuentas[$row[0]."".$k]["fuente"]=$k;
									}
								}
								
							}

							
							
							
                            $i = 0;

							$con = 1;

							$vigenciaGasto = $vigenciasDelGasto[1];

							$sqlrEnt = "SELECT nit, razonsocial FROM configbasica";
							$respEnt = mysqli_query($linkbd_V7, $sqlrEnt);
							$rowEnt = mysqli_fetch_row($respEnt);


                            foreach ($cuentas as $key => $value) 
					        {

								$presupuestoDefinitivo = 0;
								$saldo = 0;
								$numeroCuenta=$cuentas[$key]['numCuenta'];
                                $nombreCuenta=$cuentas[$key]['nomCuenta'];
                                $presupuestoInicial=$cuentas[$key]['presuInicial'];
                                $adicion=$cuentas[$key]['adicion'];
                                $reduccion=$cuentas[$key]['reduccion'];
                                $credito=$cuentas[$key]['credito'];
                                $contracredito=$cuentas[$key]['conCredito'];

								$presupuestoDefinitivo = $presupuestoInicial + $adicion - $reduccion + $credito - $contracredito;
                                //$presupuestoDefinitivo=$cuentas[$key]['presuDefinitivo'];
                                $cdp=$cuentas[$key]['cdp'];
                                $rp=$cuentas[$key]['rp'];
                                $cxp=$cuentas[$key]['cxp'];
                                $egreso=$cuentas[$key]['egreso'];

								$saldo = $presupuestoDefinitivo - $cdp;
                                //$saldo=$cuentas[$key]['saldo']; 
                                $fuente=$cuentas[$key]['fuente']; 
                                $style='';

								$compromisoEnEjecucion = $rp - $cxp;
								$enEjecucion = round((($rp/$cxp)*100),-2);
                                $cuentasPorPagar = $cxp - $egreso;

								/* fputs($Descriptor1,"Anio| Vigencia| NIT_Entidad| Nombre_Entidad| Cod_Cuenta| Fuente_Financiacion| Nombre_Cuenta| Macro_campo_nivel_agregado| Apropiacion_Inicial| Adiciones_periodo| Adiciones_Acumuladas| Reducciones_Periodo|Reducciones_Acumuladas|Creditos_Periodo|Creditos_Acumulados|Contracreditos_Periodo|Contracreditos_Acumulados|Apropiacion_Definitiva|Aplazamientos|Desaplazamientos|CDPs_Periodo|CDPs_Acumulado|Compromisos_Periodo|Compromisos_Acumulado|Obligaciones_Periodo|Obligaciones_Acumulado|Pagos_Periodo|Pagos_Acumulado|Saldo_Disponible|Porcentaje_Ejecucion|Fecha_Reporte\r\n"); */

								if($presupuestoDefinitivo > 0){
									$nombreCuenta = str_replace("|"," ",$nombreCuenta);
									echo "<tr class='$iter'>
												<td>$vigencia</td>								
												<td>$vigenciaGasto</td	>						
												<td>$rowEnt[0]</td>								
												<td>$rowEnt[1]</td>											
												<td>$numeroCuenta</td>								
												<td>$fuente</td>														
												<td>".str_replace(";"," ",$nombreCuenta)."</td>			
												<td>DA</td>			
												<td>".round($presupuestoInicial,2)."</td>
												<td>$adicion</td>										
												<td>$adicion</td>										
												<td>$reduccion</td>										
												<td>$reduccion</td>	 											
												<td>$credito</td>												
												<td>$credito</td>							
												<td>$contracredito</td>								
												<td>$contracredito</td>								
												<td>$presupuestoDefinitivo</td>								
												<td>0</td>									
												<td>0</td>									
												<td>$cdp</td>
												<td>$cdp</td>
												<td>$rp</td>
												<td>$rp</td>
												<td>$cxp</td>
												<td>$cxp</td>
												<td>$egreso</td>
												<td>$egreso</td>
												<td>$saldo</td>
												<td>$enEjecucion</td>
												<td>$fechaf</td>
											</tr>";

										fputs($Descriptor1,$vigencia."|".$vigenciaGasto."|".$rowEnt[0]."|".$rowEnt[1]."|".$numeroCuenta."|".$fuente."|".str_replace(";"," ",$nombreCuenta)."|DA|".round($presupuestoInicial,2)."|".round($adicion,2)."|".round($adicion,2)."|".round($reduccion,2)."|".round($reduccion,2)."|".round($credito,2)."|".round($credito,2)."|".round($contracredito,2)."|".round($contracredito,2)."|".round($presupuestoDefinitivo,2)."|0|0|".round($cdp,2)."|".round($cdp,2)."|".round($rp,2)."|".round($rp,2)."|".round($cxp,2)."|".round($cxp,2)."|".round($egreso,2)."|".round($egreso,2)."|".round($saldo,2)."|".$enEjecucion."|".$fechaf."\r\n");
					
										$con+=1;
										$aux=$iter;
										$iter=$iter2;
										$iter2=$aux;
									
									

									$i++;
								}
								
                            } 
                            
							echo "<script>document.getElementById('divcarga').style.display='none';</script>";
							echo"</table>";
							fclose($Descriptor1);

						break;
   					}
				}
				?>
			</div>
		</form>
	</td>
</tr>
</table>
</body>
</html