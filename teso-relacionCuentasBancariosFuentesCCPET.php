<?php
	header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=utf8");
	require 'comun.inc';
	require 'funciones.inc';
	session_start();
	$linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");
	cargarcodigopag($_GET['codpag'],$_SESSION["nivel"]);
	date_default_timezone_set("America/Bogota");
?>
<!DOCTYPE >
<html lang=es>
	<head>
		<meta charset="utf-8"/>
		<meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1"/>
		<meta name="viewport" content="user-scalable=no">
		<title>:: IDEAL 10 - Tesorería</title>
		<link href="favicon.ico" rel="shortcut icon"/>
		<link href="css/css2z.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
		<link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
		<link href="css/css4.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
        <link href="css/tabs.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css"/>
		<script src="vue/vue.min.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script src="css/programas.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script src="css/calendario.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script src="sweetalert2/dist/sweetalert2.min.js"></script>
		<link rel="stylesheet" href="sweetalert2/dist/sweetalert2.min.css">

		<style>
			input[type=number]::-webkit-inner-spin-button,
			input[type=number]::-webkit-outer-spin-button {
				-webkit-appearance: none;
				margin: 0;
			}
			input[type=number]{
				text-align:right;
				box-sizing: border-box;
				font: 120% sans-serif;
				/* width: 100% !important; */
			}
			[v-cloak]{display : none;}

			.centrarSelect {

				height: 30px !important;
				text-align-last:center !important;
			}

			.aumentarTamaño {

				font-size:15px;
				text-align: center !important;
			}

            .tamano01 {
                text-align: center !important;
            }
		</style>
	</head>
	<body>
		<header>
			<table>
				<tr><script>barra_imagenes("teso");</script><?php cuadro_titulos();?></tr>
			</table>
		</header>

		<form name="form2" method="post" action="">
			<section id="myapp" v-cloak >
				<nav>
					<table>
						<tr><?php menu_desplegable("teso");?></tr>
						<tr>
							<td colspan="3" class="cinta">
								<img src="imagenes/add.png" v-on:click="location.href='teso-relacionCuentasBancariosFuentesCCPET.php'" class="mgbt" title="Nuevo">
								<img src="imagenes/guardad.png" title="Guardar" class="mgbt1">
								<img src="imagenes/buscad.png" class="mgbt1" title="Buscar">
								<img src="imagenes/nv.png" onClick="mypop=window.open('teso-principal.php','',''); mypop.focus();" class="mgbt" title="Nueva Ventana">
							</td>
						</tr>
					</table>
				</nav>

				<article>
					<div>
						<table class="inicio grande">
							<tr>
								<td class="titulos" colspan="6">.: Relación bancos y fuentes CCPET</td>
                            	<td class="cerrar" style="width:4%" onClick="location.href='teso-principal.php'">Cerrar</td>
							</tr>

                            <tr>
                                <td>Fuente CUIPO:</td>
                                <td>
                                    <input type="text" v-model="fuenteCuipo" v-on:dblclick='desplegaModalFuentes' style="width: 99%;" class="colordobleclik" readonly>
                                </td>

								<td>Cuenta Bancaria: </td>
                                <td>
                                    <input type="text" v-model="cuentaBancaria" v-on:dblclick='desplegaModalCuentas' style="width: 99%;" class="colordobleclik" readonly>
                                </td>


								<td>
									<em class='botonflechaverde' v-on:click="guardar" style='float:rigth;'>Agregar</em>
								</td>
                            </tr>
						</table>

                        <div class="table-responsive" style="height:60vh">
                            <table class='table'>
                                <thead>
                                    <tr style="text-align:Center;">
                                        <th class="titulosnew00" style="width:15%;">Número cuenta</th>
                                        <th class="titulosnew00" style="width:25%;">Nombre cuenta</th>
                                        <th class="titulosnew00" style="width:15%;">Código fuente</th>
                                        <th class="titulosnew00">Nombre fuente</th>
                                        <th class="titulosnew00" style="width:6%;">Eliminar</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr v-for="(det,index) in relacionDet" v-bind:class="index % 2 ? 'saludo1a' : 'saludo2'" style='text-rendering: optimizeLegibility; cursor: pointer !important;'>
                                        <td style="width:15%; font: 120% sans-serif; padding-left:5px; text-align:center;">{{ det[1] }}</td>
                                        <td style="width:25%; font: 120% sans-serif; padding-left:5px; text-align:center;">{{ det[2] }}</td>
                                        <td style="width:15%; font: 120% sans-serif; padding-left:5px; text-align:center;">{{ det[3] }}</td>
                                        <td style="font: 120% sans-serif; padding-left:5px; text-align:center;">{{ det[4] }}</td>
										<td style="width:6%; text-align: center;">
											<a v-on:click="eliminar(det[0])" style='cursor:pointer;'>
												<img src="imagenes/newdelete.png" style="height:30px;" title="Eliminar">
											</a>
										</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
					</div>

					<div id="cargando" v-if="loading" class="loading">
						<span>Cargando...</span>
					</div>

                    <div v-show="showModalCuentas">
                        <transition name="modal">
                            <div class="modal-mask">
                                <div class="modal-wrapper">
                                    <div class="modal-container2">
                                        <table class="inicio ancho">
                                            <tr>
                                                <td class="titulos" colspan="2" >CUENTA BANCARIAS</td>
                                                <td class="cerrar" style="width:7%" @click="showModalCuentas = false">Cerrar</td>
                                            </tr>
                                            <tr>
                                                <td class="tamano01" style="width:3cm">Cuenta:</td>
                                                <td>
													<input type="text" class="form-control" v-on:keyup="buscarBancos" v-model="searchBanco.keywordBanco" placeholder="Buscar por numero de cuenta bancaria" style="width:100%" />
												</td>
                                            </tr>
                                        </table>
                                        <table class='tablamv'>
                                            <thead>
                                                <tr style="text-align:Center;">
                                                    <th class="titulosnew02" >Resultados Busqueda</th>
                                                </tr>
                                                <tr style="text-align:Center;">
                                                    <th class="titulosnew00" style="width:15%;">Razón social</th>
                                                    <th class="titulosnew00" >Cuenta</th>
                                                    <th class="titulosnew00" style="width:15%;">Cuenta bancaria</th>
                                                    <th class="titulosnew00" style="width:15%;">Tipo Cuenta</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr v-for="(cuenta,index) in cuentasDet" v-on:click="seleccionaBanco(cuenta)" v-bind:class="index % 2 ? 'saludo1a' : 'saludo2'" style='text-rendering: optimizeLegibility; cursor: pointer !important; text-align: center;'>
                                                    <td style="font: 120% sans-serif; padding-left:10px; width:15%;"> {{ cuenta[0] }}</td>
													<td style="font: 120% sans-serif; padding-left:10px;"> {{ cuenta[1] }}</td>
													<td style="font: 120% sans-serif; padding-left:10px; width:15%;"> {{ cuenta[2] }}</td>
													<td style="font: 120% sans-serif; padding-left:10px; width:15%;"> {{ cuenta[3] }}</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </transition>
                    </div>

					<div v-show="showModalFuentes">
                        <transition name="modal">
                            <div class="modal-mask">
                                <div class="modal-wrapper">
                                    <div class="modal-container2">
                                        <table class="inicio ancho">
                                            <tr>
                                                <td class="titulos" colspan="2" >FUENTES CCPET</td>
                                                <td class="cerrar" style="width:7%" @click="showModalFuentes = false">Cerrar</td>
                                            </tr>
                                            <tr>
                                                <td class="tamano01" style="width:3cm">Fuente:</td>
                                                <td>
													<input type="text" class="form-control" v-on:keyup="buscarFuentes" v-model="searchFuente.keywordFuente" placeholder="Buscar fuente" style="width:100%" />
												</td>
                                            </tr>
                                        </table>
                                        <table class='tablamv'>
                                            <thead>
                                                <tr style="text-align:Center;">
                                                    <th class="titulosnew02" >Resultados Busqueda</th>
                                                </tr>
                                                <tr style="text-align:Center;">
                                                    <th class="titulosnew00" style="width:25%;">Código fuente</th>
                                                    <th class="titulosnew00" >Fuente financiacion</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr v-for="(fuente,index) in fuentesDet" v-on:click="seleccionaFuente(fuente)" v-bind:class="index % 2 ? 'saludo1a' : 'saludo2'" style='text-rendering: optimizeLegibility; cursor: pointer !important; text-align: center;'>
                                                    <td style="font: 120% sans-serif; padding-left:10px; width:25%;"> {{ fuente[0] }}</td>
													<td style="font: 120% sans-serif; padding-left:10px;"> {{ fuente[1] }}</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </transition>
                    </div>

				</article>
			</section>
		</form>

		<script src="Librerias/vue/vue.min.js"></script>
		<script src="Librerias/vue/axios.min.js"></script>
		<script src="tesoreria\relacionCuentaBancariaFuenteCCPET\teso-relacionCuentasBancariosFuentesCCPET.js?"></script>

	</body>
</html>
