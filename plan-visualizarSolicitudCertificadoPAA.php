<?php
	header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=utf8");
	require 'comun.inc';
	require 'funciones.inc';
	session_start();
	$linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");
	cargarcodigopag($_GET['codpag'],$_SESSION["nivel"]);
	date_default_timezone_set("America/Bogota");
?>
<!DOCTYPE >
<html lang=es>
	<head>
    <meta charset="utf-8"/>
		<meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1"/>
		<meta name="viewport" content="user-scalable=no">
		<title>:: IDEAL 10 - Planeación</title>
		<link href="favicon.ico" rel="shortcut icon"/>
		<link href="css/css2z.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
		<link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
		<link href="css/css4.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
        <link href="css/tabs.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css"/>
        <link rel="stylesheet" href="sweetalert2/dist/sweetalert2.min.css">
        <script type="text/javascript" src="css/programas.js"></script>
        <script src="sweetalert2/dist/sweetalert2.min.js"></script>
		<script src="vue/vue.min.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script src="css/programas.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script src="css/calendario.js?<?php echo date('d_m_Y_h_i_s');?>"></script>

        <script>
            function pdf(){
				document.form2.action = "pdfn.php";
				document.form2.target = "_BLANK";
				document.form2.submit(); 
				document.form2.action = "";
				document.form2.target = "";
			}
		</script>

        <style>
            [v-cloak]{
                display : none;
            }
        </style>
	</head>
	<body>
		<header>
			<table>
				<tr><script>barra_imagenes("plan");</script><?php cuadro_titulos();?></tr>
			</table>
		</header>

		<form name="form2" method="post" action="">
			<section id="myapp" v-cloak>
				<nav>
					<table>
						<tr><?php menu_desplegable("plan");?></tr>
						<tr>
							<td colspan="3" class="cinta">
								<img src="imagenes/add.png" v-on:click="location.href='plan-solicitudCertificadoPAA'" class="mgbt" title="Nuevo">
								<img src="imagenes/guarda.png" title="Guardar"  class="mgbt">
								<img src="imagenes/busca.png" v-on:click="location.href='plan-buscaCertificadoPAA'" class="mgbt" title="Buscar">
								<img src="imagenes/nv.png" onClick="mypop=window.open('plan-principal','',''); mypop.focus();" class="mgbt" title="Nueva Ventana">
                                <a onclick="pdf()"><img src="imagenes/print.png" title="Imprimir" class="mgbt"></a>
							</td>
						</tr>
					</table>
				</nav>

				<article>
                    <div>
                        <table class="inicio">
                            <tr>
                                <td class="titulos" colspan="6">.: Datos de plan de adquisición</td>
                            </tr>
                            
                            <tr>
                                <td class="textonew01" style="width:10%;">.: Fecha solicitud:</td>
                                <td style="width: 11%;">
                                    <input type="text" name="fecha" v-model="fecha" readonly>
                                </td>
                                
                                <td class="textonew01" style="width:10%;">.: Código PAA:</td>
                                <td style="width: 11%;">
                                    <input type="hideen" name="consecutivo" v-model="consecutivo">
                                    <input type="text" name="codPaa" v-model="codPaa" readonly>
                                </td>

                                <td class="textonew01" style="width:10%;">.: Descripción:</td>
                                <td>
                                    <textarea v-model="descripcionPaa" name="descripcionPaa" style="width:100%" readonly></textarea>
                                </td>
                            <tr>

                            <tr>
                                <td class="textonew01" style="width:10%;">.: Observaciones:</td>
                                <td colspan="5">
                                    <textarea v-model="observacion" name="observacion" style="width:100%" readonly></textarea>
                                </td>
                            </tr>
                        </table>

                        <div class='subpantalla' style='height:50vh; width:100%; margin-top:0px;  overflow-x:hidden'>
                            <table class='inicio'>        
                                <thead>
                                    <tr>
                                        <th class="titulosnew00" style="width: 20%;">Código Producto</th>
                                        <th class="titulosnew00">Nombre</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr v-for="(produc,index) in productos" v-bind:class="index % 2 ? 'saludo1a' : 'saludo2'">
                                        <td style="width:5%; text-align:center;"> {{ produc.id }} </td>
                                        <td style="text-align:center;"> {{ produc.nombre }} </td>

                                        <input type='hidden' name='productos[]' v-model="produc.id">	
                                        <input type='hidden' name='nombres[]' v-model="produc.nombre">	
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>

                    <div id="cargando" v-if="loading" class="loading">
						<span>Cargando...</span>
					</div>
				</article>
			</section>
		</form>

		<script src="node_modules/read-excel-file/bundle/read-excel-file.min.js"></script>
		<script src="node_modules/xlsx/dist/xlsx.full.min.js"></script>
        <script src="Librerias/vue/vue.min.js"></script>
		<script src="Librerias/vue/axios.min.js"></script>
		<script type="module" src="planeacion/solicitudCertificadoPAA/visualizar/plan-visualizarSolicitudCertificadoPAA.js"></script>
        
	</body>
</html>