<?php

	header("Content-Type: text/html;charset=utf-8");
    require_once 'PHPExcel/Classes/PHPExcel.php';
    require "comun.inc";
    require "funciones.inc";
    require 'funcionesSP.inc.php';
    // ini_set('display_errors', '1');
    // ini_set('display_startup_errors', '1');
    // error_reporting(E_ALL);

    session_start();

    class Plantilla{
        private $linkbd;
        public function __construct() {
            $this->linkbd = conectar_v7();
            $this->linkbd->set_charset("utf8");
        }

        public function searchData() {
            $sql = "SELECT 
            PP.nombre_pdt,
            PIP.id,
            PIP.nombre,
            CP.codigo_indicador AS codigo_producto,
            PIP.valor_cuatrienio,
            PIP.meta_cuatrienio,
            PIP.linea_base,
            PIP.indicador_resultado_id,
            CONCAT(T.nombre1, ' ', T.nombre2, ' ', T.apellido1, ' ', T.apellido2) AS funcionario
            FROM plan_indicador_producto AS PIP
            INNER JOIN plan_pdt AS PP
            ON PIP.pdt_id = PP.id
            INNER JOIN ccpetproductos AS CP
            ON PIP.codigo_producto_id = CP.id
            INNER JOIN terceros AS T
            ON PIP.tercero_id = T.id_tercero
            GROUP BY PIP.id";
            $data = mysqli_fetch_all(mysqli_query($this->linkbd,$sql),MYSQLI_ASSOC);
            foreach ($data as $key => $d) {
                $data[$key]["funcionario"] = preg_replace("/\s+/", " ", trim($d["funcionario"]));
            }
            return $data;
        }

        public function ejes() {
            $sql = "SELECT indicador_producto_id, GROUP_CONCAT(eje_estrategico_id ORDER BY eje_estrategico_id ASC) AS ejes_concatenados FROM plan_indicador_producto_det GROUP BY indicador_producto_id";       
            $data = mysqli_fetch_all(mysqli_query($this->linkbd,$sql),MYSQLI_ASSOC);
            return $data;
        }
    }
    $obj = new Plantilla();
    $data = $obj->searchData();
    $ejes = $obj->ejes();
    $objPHPExcel = new PHPExcel();

    foreach ($ejes as $key => $eje) {
        $numeros = explode(',', $eje['ejes_concatenados']);
        $numeros_prefijados = array_map(function($numero) {
            return 'LE' . $numero;
        }, $numeros);
        $ejes[$key]['ejes_concatenados'] = implode(',', $numeros_prefijados);
    }

    $objPHPExcel->getSheet(0)->getStyle('A:J')->applyFromArray(
        array(
            'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
                'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER
            )
        )
    );
    $objPHPExcel->getSheet(0)->setTitle("ISGR");
    $objPHPExcel->getProperties()
    ->setCreator("IDEAL 10")
    ->setLastModifiedBy("IDEAL 10")
    ->setTitle("Exportar Excel con PHP")
    ->setSubject("Documento de prueba")
    ->setDescription("Documento generado con PHPExcel")
    ->setKeywords("usuarios phpexcel")
    ->setCategory("reportes");

    // ----Cuerpo de Documento----
    $objPHPExcel->getSheet(0)
    ->mergeCells('A1:J1')
    ->mergeCells('A2:J2')
    ->setCellValue('A1', 'PLANEACIÓN ESTRATEGICA')
    ->setCellValue('A2', 'INDICADORES DE PRODUCTO CREADOS');
    $objPHPExcel->getSheet(0)
    -> getStyle ("A1")
    -> getFill ()
    -> setFillType (PHPExcel_Style_Fill :: FILL_SOLID)
    -> getStartColor ()
    -> setRGB ('C8C8C8');
    $objPHPExcel->getSheet(0)
    -> getStyle ("A1:J2")
    -> getFont ()
    -> setBold ( true )
    -> setName ( 'Verdana' )
    -> setSize ( 10 )
    -> getColor ()
    -> setRGB ('000000');
    $objPHPExcel->getSheet(0)
    -> getStyle ('A1:J2')
    -> getAlignment ()
    -> applyFromArray (array ( 'horizontal'  =>  PHPExcel_Style_Alignment :: HORIZONTAL_CENTER ,) );
    $objPHPExcel->getSheet(0)
    -> getStyle ('A3:J3')
    -> getAlignment ()
    -> applyFromArray (array ( 'horizontal'  =>  PHPExcel_Style_Alignment :: VERTICAL_JUSTIFY ,) );
    $objPHPExcel->getSheet(0)
    -> getStyle ("A2")
    -> getFill ()
    -> setFillType (PHPExcel_Style_Fill :: FILL_SOLID);

    $borders = array(
        'borders' => array(
            'allborders' => array(
                'style' => PHPExcel_Style_Border::BORDER_THIN,
            'color' => array('argb' => 'FF000000'),
            )
        ),
    );
    $objPHPExcel->getSheet(0)
    ->setCellValue('A3', "Nombre PDT")
    ->setCellValue('B3', "Código")
    ->setCellValue('C3', "Nombre")
    ->setCellValue('D3', "Código indicador")
    ->setCellValue('E3', "Lineas estrategicas")
    ->setCellValue('F3', "Indicador resultado")
    ->setCellValue('G3', "Valor cuatrienio")
    ->setCellValue('H3', "Meta cuatrienio")
    ->setCellValue('I3', "Linea base")
    ->setCellValue('J3', "Funcionario")
    ;
    $objPHPExcel->getSheet(0)
        -> getStyle ("A3:J3")
        -> getFill ()
        -> setFillType (PHPExcel_Style_Fill :: FILL_SOLID)
        -> getStartColor ()
        -> setRGB ('99ddff');
    $objPHPExcel->getSheet(0)->getStyle("A3:J3")->getFont()->setBold(true);
    $objPHPExcel->getSheet(0)->getStyle('A1:J1')->applyFromArray($borders);
    $objPHPExcel->getSheet(0)->getStyle('A2:J2')->applyFromArray($borders);
    $objPHPExcel->getSheet(0)->getStyle('A3:J3')->applyFromArray($borders);


    $objPHPExcel->getSheet(0)->getColumnDimension('A')->setAutoSize(true);
    $objPHPExcel->getSheet(0)->getColumnDimension('B')->setAutoSize(true);
    $objPHPExcel->getSheet(0)->getColumnDimension('C')->setAutoSize(true);
    $objPHPExcel->getSheet(0)->getColumnDimension('D')->setAutoSize(true);
    $objPHPExcel->getSheet(0)->getColumnDimension('E')->setAutoSize(true);
    $objPHPExcel->getSheet(0)->getColumnDimension('F')->setAutoSize(true);
    $objPHPExcel->getSheet(0)->getColumnDimension('G')->setAutoSize(true);
    $objPHPExcel->getSheet(0)->getColumnDimension('H')->setAutoSize(true);
    $objPHPExcel->getSheet(0)->getColumnDimension('I')->setAutoSize(true);
    $objPHPExcel->getSheet(0)->getColumnDimension('J')->setAutoSize(true);

    if(!empty($data)){
        $total = count($data);
        $row = 4;
        for ($i=0; $i < $total ; $i++) {
            $dataEje = "";
            
            foreach ($ejes as $key => $eje) {
                if ($data[$i]["id"] == $eje["indicador_producto_id"]) {
                    $dataEje = $eje["ejes_concatenados"];
                }
            }

            $objPHPExcel->getSheet(0)
            ->setCellValue("A$row", $data[$i]['nombre_pdt'])
            ->setCellValue("B$row", "IP".$data[$i]['id'])
            ->setCellValue("C$row", $data[$i]['nombre'])
            ->setCellValue("D$row", $data[$i]['codigo_producto'])
            ->setCellValue("E$row", $dataEje)
            ->setCellValue("F$row", "IR".$data[$i]['indicador_resultado_id'])
            ->setCellValue("G$row", "$".number_format($data[$i]['valor_cuatrienio'], 2))
            ->setCellValue("H$row", $data[$i]['meta_cuatrienio'])
            ->setCellValue("I$row", $data[$i]['linea_base'])
            ->setCellValue("J$row", $data[$i]['funcionario']);
            $row++;
        }
    }
  
    $objPHPExcel->setActiveSheetIndex(0);
    //----Guardar documento----
    header('Content-Type: application/vnd.ms-excel');
    header('Content-Disposition: attachment;filename="indicador_producto_creados.xlsx"');
    header('Cache-Control: max-age=0');
    $objWriter=PHPExcel_IOFactory::createWriter($objPHPExcel,'Excel2007');
    $objWriter->save('php://output');
    die();

?>
