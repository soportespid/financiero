<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="ejemplo.css">
    <title>GitHub Replica</title>
</head>
<body>
    <header>
        <nav>
            <div class="logo">GitHub</div>
            <ul class="nav-links">
                <li><a href="#">Features</a></li>
                <li><a href="#">Enterprise</a></li>
                <li><a href="#">Explore</a></li>
                <!-- More navigation items -->
            </ul>
            <button class="sign-in">Sign In</button>
        </nav>
    </header>
    <section class="hero">
        <h1>Where the world builds software</h1>
        <p>Millions of developers use GitHub to share and work together on code.</p>
        <button class="get-started">Get Started</button>
    </section>
    <section class="explore-repos">
        <h2>Explore repositories</h2>
        <div class="repo-list">
            <div class="repo-card">
                <img src="https://media.istockphoto.com/id/1358013032/es/foto/concepto-de-desarrollo-web.jpg?s=2048x2048&w=is&k=20&c=bDTO8_AtLMw7P1wTlkT2algxOp5le8ZZKH8IeyaofOo=" alt="Repository 1">
                <h3>Example Repository 1</h3>
                <p>This is an example repository description.</p>
            </div>
            <div class="repo-card">
                <img src="https://cdn.pixabay.com/photo/2016/04/04/14/12/monitor-1307227_1280.jpg" alt="Repository 2">
                <h3>Example Repository 2</h3>
                <p>This is another example repository description.</p>
            </div>
            <div class="repo-card">
                <img src="https://media.istockphoto.com/id/1358013032/es/foto/concepto-de-desarrollo-web.jpg?s=2048x2048&w=is&k=20&c=bDTO8_AtLMw7P1wTlkT2algxOp5le8ZZKH8IeyaofOo=" alt="Repository 1">
                <h3>Example Repository 1</h3>
                <p>This is an example repository description.</p>
            </div>
            <div class="repo-card">
                <img src="https://cdn.pixabay.com/photo/2016/04/04/14/12/monitor-1307227_1280.jpg" alt="Repository 2">
                <h3>Example Repository 2</h3>
                <p>This is another example repository description.</p>
            </div>
            <div class="repo-card">
                <img src="https://media.istockphoto.com/id/1358013032/es/foto/concepto-de-desarrollo-web.jpg?s=2048x2048&w=is&k=20&c=bDTO8_AtLMw7P1wTlkT2algxOp5le8ZZKH8IeyaofOo=" alt="Repository 1">
                <h3>Example Repository 1</h3>
                <p>This is an example repository description.</p>
            </div>
            <div class="repo-card">
                <img src="https://cdn.pixabay.com/photo/2016/04/04/14/12/monitor-1307227_1280.jpg" alt="Repository 2">
                <h3>Example Repository 2</h3>
                <p>This is another example repository description.</p>
            </div>
            <div class="repo-card">
                <img src="https://media.istockphoto.com/id/1358013032/es/foto/concepto-de-desarrollo-web.jpg?s=2048x2048&w=is&k=20&c=bDTO8_AtLMw7P1wTlkT2algxOp5le8ZZKH8IeyaofOo=" alt="Repository 1">
                <h3>Example Repository 1</h3>
                <p>This is an example repository description.</p>
            </div>
            <div class="repo-card">
                <img src="https://cdn.pixabay.com/photo/2016/04/04/14/12/monitor-1307227_1280.jpg" alt="Repository 2">
                <h3>Example Repository 2</h3>
                <p>This is another example repository description.</p>
            </div>
            <div class="repo-card">
                <img src="https://media.istockphoto.com/id/1358013032/es/foto/concepto-de-desarrollo-web.jpg?s=2048x2048&w=is&k=20&c=bDTO8_AtLMw7P1wTlkT2algxOp5le8ZZKH8IeyaofOo=" alt="Repository 1">
                <h3>Example Repository 1</h3>
                <p>This is an example repository description.</p>
            </div>
            <div class="repo-card">
                <img src="https://cdn.pixabay.com/photo/2016/04/04/14/12/monitor-1307227_1280.jpg" alt="Repository 2">
                <h3>Example Repository 2</h3>
                <p>This is another example repository description.</p>
            </div>
            <!-- More repositories -->
        </div>
    </section>
    <!-- More sections and content -->
    <footer>
        <p>&copy; 2023 GitHub Replica. This is not a real GitHub page.</p>
    </footer>
</body>
</html>