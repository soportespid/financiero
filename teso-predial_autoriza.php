<?php
	require "comun.inc";
	require "funciones.inc";
	require "conversor.php";
	sesion();
	$linkbd = conectar_v7();	
	cargarcodigopag($_GET['codpag'], $_SESSION["nivel"]);
	header("Cache-control: private"); // Arregla IE 6
	date_default_timezone_set("America/Bogota");
?>
<!DOCTYPE >
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="es">
	<head>
	 	<meta http-equiv="Content-Type" content="text/html" charset="iso-8859-1"/>
        <meta http-equiv="X-UA-Compatible" content="IE=9"/>
		<title>:: IDEAL 10 - Tesorer&iacute;a</title>
        <link href="css/css2.css" rel="stylesheet" type="text/css" />
        <link href="css/css3.css" rel="stylesheet" type="text/css" />
        <link href="css/tabs.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="css/programas.js"></script>
        <script type="text/javascript" src="css/calendario.js"></script>
		<script>
			function buscacta(e)
 			{
				if (document.form2.cuenta.value!="")
				{
				 	document.form2.bc.value='1';
				 	document.form2.submit();
				}
			}
			function validar() {document.form2.submit();}
			function buscater(e)
			{
				if (document.form2.tercero.value!="")
				{
			 		document.form2.bt.value='1';
			 		document.form2.submit();
			 	}
			 }
			function agregardetalle()
			{
				if(document.form2.numero.value!="" &&  document.form2.valor.value>0 &&  document.form2.banco.value!=""  )
				{ 
					document.form2.agregadet.value=1;
					//			document.form2.chacuerdo.value=2;
					document.form2.submit();
				}
				else {
					alert("Falta informacion para poder Agregar");
				}
			}
			function eliminar(variable)
			{
				if(confirm("Esta Seguro de Eliminar"))
  				{
					document.form2.elimina.value=variable;
					//eli=document.getElementById(elimina);
					vvend=document.getElementById('elimina');
					//eli.value=elimina;
					vvend.value=variable;
					document.form2.submit();
				}
			}


			function guardar()
			{
				if (document.form2.fecha.value!='' && document.form2.autorizacion.value!='' )
				{
					despliegamodalm('visible','4','Esta Seguro de Guardar','1');
				}
				else{
					despliegamodalm('visible','2','Faltan datos para completar el registro');
					document.form2.fecha.focus();
					document.form2.fecha.select();
				}
			}

			function despliegamodalm(_valor,_tip,mensa,pregunta,variable)
			{
				document.getElementById("bgventanamodalm").style.visibility=_valor;
				if(_valor=="hidden"){document.getElementById('ventanam').src="";}
				else
				{
					switch(_tip)
					{
						case "1":	document.getElementById('ventanam').src="ventana-mensaje1.php?titulos="+mensa;break;
						case "2":	document.getElementById('ventanam').src="ventana-mensaje3.php?titulos="+mensa;break;
						case "3":	document.getElementById('ventanam').src="ventana-mensaje2.php?titulos="+mensa;break;
						case "4":	document.getElementById('ventanam').src="ventana-consulta1.php?titulos="+mensa+"&idresp="+pregunta;break;	
						case "5":
						document.getElementById('ventanam').src="ventana-elimina1.php?titulos="+mensa+"&idresp="+pregunta+"&variable="+variable;break;	
					}
				}
			}

			function respuestaconsulta(pregunta, variable)
			{
				switch(pregunta)
				{
					case "1":	document.getElementById('oculto').value="2";
								document.form2.submit();break;
					case "2":
						document.form2.elimina.value=variable;
						//eli=document.getElementById(elimina);
						vvend=document.getElementById('elimina');
						//eli.value=elimina;
						vvend.value=variable;
						document.form2.submit();
						break;
				}
			}
			function funcionmensaje(){document.location.href = "teso-predial_autoriza.php";}

			function pdf()
			{
				document.form2.action="pdfpredial.php";
				document.form2.target="_BLANK";
				document.form2.submit(); 
				document.form2.action="";
				document.form2.target="";
			}

			function buscar()
			{
				// alert("dsdd");
				document.form2.buscav.value='1';
				document.form2.submit();
			}

			function buscavigencias(objeto)
			{
				//document.form2.buscarvig.value='1';
				vvigencias=document.getElementsByName('dselvigencias[]');
				vtotalpred=document.getElementsByName("dpredial[]");
				vtotaliqui=document.getElementsByName("dhavaluos[]"); 	
				vtotalbomb=document.getElementsByName("dimpuesto1[]"); 	
				vtotalmedio=document.getElementsByName("dimpuesto2[]"); 	
				vtotalintp=document.getElementsByName("dipredial[]"); 	
				vtotalintb=document.getElementsByName("dinteres1[]"); 	
				vtotalintma=document.getElementsByName("dinteres2[]"); 	
				vtotaldes=document.getElementsByName("ddescuentos[]"); 	
				sumar=0;
				sumarp=0;
				sumarb=0;
				sumarma=0;
				sumarint=0;
				sumarintp=0;
				sumarintb=0;
				sumarintma=0;
				sumardes=0;
				for(x=0;x<vvigencias.length;x++)
				{
					if(vvigencias.item(x).checked)
					{
					sumar=sumar+parseFloat(vtotaliqui.item(x).value);
					sumarp=sumarp+parseFloat(vtotalpred.item(x).value);
					sumarb=sumarb+parseFloat(vtotalbomb.item(x).value);
					sumarma=sumarma+parseFloat(vtotalmedio.item(x).value);
					sumarint=sumarint+parseFloat(vtotalintp.item(x).value)+parseFloat(vtotalintb.item(x).value)+parseFloat(vtotalintma.item(x).value);
					sumarintp=sumarintp+parseFloat(vtotalintp.item(x).value);
					sumarintb=sumarintb+parseFloat(vtotalintb.item(x).value);
					sumarintma=sumarintma+parseFloat(vtotalintma.item(x).value);	 	 
					sumardes=sumardes+parseFloat(vtotaldes.item(x).value);
					}
				}
				document.form2.totliquida.value=sumar;
				document.form2.totliquida2.value=sumar;
				document.form2.totpredial.value=sumarp;
				document.form2.totbomb.value=sumarb;
				document.form2.totamb.value=sumarma;
				document.form2.totint.value=sumarint;
				document.form2.intpredial.value=sumarintp;
				document.form2.intbomb.value=sumarintb;
				document.form2.intamb.value=sumarintma;
				document.form2.totdesc.value=sumardes;
			}
		</script>
		<?php titlepag();?>
	</head>
	<body>
		<IFRAME src="alertas.php" name="alertas" id="alertas" style="display:none"></IFRAME>
		<span id="todastablas2"></span>
		<table>
			<tr><script>barra_imagenes("teso");</script><?php cuadro_titulos();?></tr>	 
			<tr><?php menu_desplegable("teso");?></tr>
			<tr>
				<td colspan="3" class="cinta">
					<a href="teso-predial_autoriza.php" class="mgbt"><img src="imagenes/add.png" title="Nuevo"/></a> 
					<a onClick="guardar()" class="mgbt"><img src="imagenes/guarda.png" title="Guardar"/></a>
					<a href="teso-buscapredial.php" class="mgbt"> <img src="imagenes/busca.png" title="Buscar"/></a> 
					<a onClick="mypop=window.open('plan-agenda.php','','');mypop.focus()" class="mgbt"><img src="imagenes/agenda1.png" title="Agenda" /></a>
					<a onClick="mypop=window.open('teso-principal.php','','');mypop.focus();" class="mgbt"><img src="imagenes/nv.png" title="Nueva ventana"></a>
					<a onClick="mypop=window.open('<?php echo $url2; ?>','','');mypop.focus();" class="tooltip bottom mgbt"><img src="imagenes/duplicar_pantalla.png" title="Duplicar pesta&ntilde;a"></a> 
					<a <?php if($_POST['oculto']==2) { ?> onClick="pdf()" <?php } ?> class="mgbt"> <img src="imagenes/print.png" title="Imprimir" /></a>
				</td>
			</tr>		  
		</table>
		<tr>
			<td colspan="3" class="tablaprin" align="center"> 
				<?php
				/* $vigusu = vigencia_usuarios($_SESSION[cedulausu]);
				$vigencia = $vigusu; */

				if(!$_POST['oculto']){
					$check1='checked';
					$fec = date("d/m/Y");
					$vig = date("Y");
					$_POST['fecha']=$fec;
					$_POST['vigencia']=$vig;
				}
				
				if($_POST['autorizacion']!="")
				{
					$sqlr = "select *from tesoautorizapredial where estado='S' and id_auto='".$_POST['autorizacion']."'";
					$res = mysqli_query($linkbd, $sqlr);
					while($r = mysqli_fetch_row($res))
					{
						$_POST['fechaav'] = $r[3];
						$_POST['codcat'] = $r[1];
						$_POST['ord'] = $r[11];
						$_POST['tot'] = $r[12];			 
						$_POST['descPago'] = $r[5];			 
						$_POST['autorizaPago'] = $r[6];			 
					}
					preg_match("/([0-9]{4})-([0-9]{2})-([0-9]{2})/", $_POST['fechaav'], $fechaf);
					$_POST['fechaav'] = $fechaf[3]."/".$fechaf[2]."/".$fechaf[1];

					$sqlrPredio = "SELECT * FROM tesopredios WHERE cedulacatastral = '$_POST[codcat]'"; 
					$resPredio = mysqli_query($linkbd, $sqlrPredio);
					$rowPredio = mysqli_fetch_assoc($resPredio);

					$_POST['catastral'] = $_POST['codcat'];
					$_POST['avaluo'] = $rowPredio['avaluo'];
					$_POST['tercero'] = $rowPredio['documento'];
					$_POST['ntercero'] = $rowPredio['nombrepropietario'];
					$_POST['direccion'] = $rowPredio['direccion'];
					$_POST['ha'] = $rowPredio['ha'];
					$_POST['mt2'] = $rowPredio['met2'];
					$_POST['areac'] = $rowPredio['areacon'];
					$_POST['tipop'] = $rowPredio['clasifica'];
				}

 
				switch($_POST['tabgroup1'])
				{
					case 1:
						$check1='checked';
					break;
					case 2:
						$check2='checked';
					break;
					case 3:
						$check3='checked';
				}
				?>
				<div id="bgventanamodalm" class="bgventanamodalm">
					<div id="ventanamodalm" class="ventanamodalm">
						<IFRAME src="" name="ventanam" marginWidth=0 marginHeight=0 frameBorder=0 id="ventanam" frameSpacing=0 style=" width:700px; height:130px; top:200; overflow:hidden;"> 
						</IFRAME>
					</div>
				</div>
 				<form name="form2" method="post" action="">
  					<?php
  						$_POST['numpredial'] = selconsecutivo('tesoliquidapredial','idpredial');			
					?>
					<div class="tabspre">
   						<div class="tab">
							<input type="radio" id="tab-1" name="tabgroup1" value="1" <?php echo $check1;?> >
							<label for="tab-1">Liquidaci&oacute;n Predial</label>
							<div class="content">
								<table class="inicio" align="center" >
									<tr >
										<td style="width:95%;" class="titulos" colspan="3">Liquidar Predial</td>
										<td style="width:5%;" class="cerrar" >
											<a href="teso-principal.php">Cerrar</a>
										</td>
									</tr>
			  						<tr>
			  							<td style="width:80%;">
			  								<table>
			  									<tr>
													<td style="width:10%;" class="saludo1">No Autorizaci&oacute;n</td>
													<td style="width:20%;">
														<select name="autorizacion" onChange="validar();" >
															<option value="">Seleccione ...</option>
															<?php
																$sqlr="select * from tesoautorizapredial where estado='S' and liquidacion=0";
																$res = mysqli_query($linkbd, $sqlr);
																while ($row = mysqli_fetch_row($res)) 
																{
																	echo "<option value=$row[0] ";
																	$i=$row[0];
																	if($i == $_POST['autorizacion'])
																	{
																		echo "SELECTED";
																		//$_POST[nestrato]=$row[1];
																	}
																	echo ">".$row[0]." - Codcat:".$row[1]."</option>";	 
																}
															?>            
														</select> 
							  						</td>
							  						<td style="width:10%" class="saludo1">No Liquidaci&oacute;n:</td>
													<td style="width:10%;">
														<input name="numpredial" type="text" value="<?php echo $_POST['numpredial']?>" style="width:96%;" onKeyPress="javascript:return solonumeros(event)" onKeyUp="return tabular(event,this)" readonly>
													</td>
													<td style="width:5%;"  class="saludo1">Fecha:</td>
													<td style="width:10%;">
														<input name="fecha" type="text" value="<?php echo $_POST['fecha']?>" maxlength="10" style="width:96%;" onKeyPress="javascript:return solonumeros(event)" onKeyUp="return tabular(event,this)" readonly>
													</td>
													<td style="width:5%;"  class="saludo1">Vigencia:</td>
													<td style="width:10%;" >
														<input name="vigencia" type="text" value="<?php echo $_POST['vigencia']?>" maxlength="2" style="width:96%;" onKeyPress="javascript:return solonumeros(event)" onKeyUp="return tabular(event,this)" readonly>
													</td>
												</tr>
												<tr>
													<td style="width:10%;" class="saludo1">C&oacute;digo Catastral:</td>

													<td style="width:21%;">
														<input id="codcat" type="text" name="codcat" onKeyUp="return tabular(event,this)" style="width:65%;" onBlur="buscar(event)" value="<?php echo $_POST['codcat']?>" >
														<input id="ord" type="text" name="ord" style="width:15%;" value="<?php echo $_POST['ord']?>" readonly>
														<input id="tot" type="text" name="tot" style="width:15%;" value="<?php echo $_POST['tot']?>" readonly>
														<input type="hidden" value="0" name="bt">  
														<input type="hidden" name="chacuerdo" value="1">
														<input type="hidden" value="1" name="oculto" id="oculto">
													</td>

													<td class="saludo1">Proy Liquidaci&oacute;n:</td>
													<td style="width:10%;">

														<input name="fechaav" type="text" style="width:96%;" value="<?php echo $_POST['fechaav']?>" maxlength="10" onKeyPress="javascript:return solonumeros(event)" onKeyUp="return tabular(event,this)"  id="fc_1198971545" onKeyDown="mascara(this,'/',patron,true)" title="DD/MM/YYYY" readonly>    
													</td> 

			        							</tr>

			        							<tr>
													<td style="width:10%;" class="saludo1">Descripci&oacute;n pago:</td>
													<td colspan = '3'>
														<input name="descPago" style="width:99%;" value="<?php echo $_POST['descPago']?>" type="text" readonly>
													</td>
													<td style="width:10%;" class="saludo1">Autoriza pago:</td>
													<td colspan = '3'>
														<input name="autorizaPago" style="width:99%;" value="<?php echo $_POST['autorizaPago']?>" type="text" readonly>
													</td>
												</tr>
			  								</table>
			  							</td>
			  						</tr>
	  							</table>
							</div> 
						</div>
    					<div class="tab">
							<input type="radio" id="tab-2" name="tabgroup1" value="2" <?php echo $check2;?>>
							<label for="tab-2">Informaci&oacute;n Predio</label>
							<div class="content"> 
								<table class="inicio">
									<tr>
										<td class="titulos" colspan="9">Informaci&oacute;n Predio</td>
										<td style="width:7%" class="cerrar" ><a href="teso-principal.php">Cerrar</a></td>
									</tr>
									<tr>
										<td style="width:10%;" class="saludo1">C&oacute;digo Catastral:</td>
										<td style="width:10%;">
											<input name="catastral" type="text" id="catastral" onBlur="buscater(event)" style="width:100%;" onKeyUp="return tabular(event,this)" value="<?php echo $_POST['catastral']?>"  readonly>
										</td>
										<td style="width:8%;" class="saludo1">Avaluo:</td>
										<td style="width:5%;">
											<input name="avaluo" type="text" id="avaluo" onKeyUp="return tabular(event,this)" value="<?php echo $_POST['avaluo']?>" style="width:100%;" readonly>
										</td>
										<td style="width:7%;" class="saludo1">Documento:</td>
										<td style="width:5%;">
											<input name="tercero" type="text" id="tercero" onKeyUp="return tabular(event,this)" value="<?php echo $_POST['tercero']?>" style="width:100%;" readonly>
										</td>
										<td style="width:7%;" class="saludo1">Propietario:</td>
										<td  colspan="3">
											<input name="ntercero" type="text" id="propietario" style="width:100%;" onKeyUp="return tabular(event,this)" value="<?php echo $_POST['ntercero']?>"  readonly>
										</td>
		  							</tr>
									<tr>
										<td  class="saludo1">Direccion:</td>
										<td colspan="3">
											<input name="direccion" type="text" id="direccion" style="width:100%;" onKeyUp="return tabular(event,this)" value="<?php echo $_POST['direccion']?>" readonly>
										</td>
										<td class="saludo1">Ha:</td>
										<td style="width:5%;">
											<input name="ha" type="text" id="ha" onKeyUp="return tabular(event,this)" style="width:100%;" value="<?php echo $_POST['ha']?>" readonly>
										</td>
										<td  class="saludo1">Mt2:</td>
										<td style="width:5%;">
											<input name="mt2" type="text" id="mt2" onKeyUp="return tabular(event,this)" value="<?php echo $_POST['mt2']?>" style="width:100%;" readonly>
										</td>
										<td  class="saludo1" style="width:6%;">Area Cons:</td>
										<td style="width:5%;">
											<input name="areac" type="text" id="areac" style="width:100%;" onKeyUp="return tabular(event,this)" value="<?php echo $_POST['areac']?>" readonly>
										</td>
	      							</tr>

		  							<tr>
		     							<td width="119" class="saludo1">Tipo:</td>
                              			<td>
								  			<select name="tipop" onChange="validar();" id="tipop" style="width: 80%">
							   					<option value="">Seleccione ...</option>
													<?php
													if(!empty($_POST['tipop'])){
														
														$sql="SELECT codigo,nombre FROM teso_clasificapredios WHERE vigencia='$_POST[vigencia]' GROUP BY codigo,nombre";
														$result=mysqli_query($linkbd, $sql);
														while($row = mysqli_fetch_array($result)){
															if($row[0]==$_POST['tipop']){
																echo "<option value='$row[0]' SELECTED >$row[1]</option>";
															}else{
																echo "<option value='$row[0]'>$row[1]</option>";
															}
														}
													}
													?>
											</select>
										</td>
										<?php 
										if(!empty($_POST['rangos'])){
											?>
											<td width="119" class="saludo1">Rango Avaluo:</td>
											<td>
												<select name="rangos" onChange="validar();" id="rangos" style="width: 80%">
													<option value="">Seleccione ...</option>
													<?php
													if(!empty($_POST['tipop']) && !empty($_POST['rangos'])){
														$sql="SELECT id_rango,nom_rango FROM teso_clasificapredios WHERE codigo=$_POST[tipop] AND id_rango=$_POST[rangos] AND vigencia='$_POST[vigencia]'";
														$result=mysqli_query($linkbd, $sql);
														while($row = mysqli_fetch_array($result)){
															if($row[0]==$_POST['rangos']){
																echo "<option value='$row[0]' SELECTED >$row[1]</option>";
															}else{
																echo "<option value='$row[0]'>$row[1]</option>";
															}
											
														}
													}
													?>
												</select>
											</td>
											<?php
										}
										?>
	        						</tr> 
      							</table>
							</div> 
						</div>    
					</div>
					<div class="subpantallac" style="width:99.4%;" >
    					<table class="inicio"  style="overflow-x:hidden;">
							<tr>
								<td colspan="14" class="titulos">Periodos a Liquidar  </td>
							</tr>                  
							<tr>
								<td class="titulos2">Vigencia</td>
								<td class="titulos2">Codigo Catastral</td>
								<td class="titulos2">Predial</td>
								<td class="titulos2">Intereses Predial</td>   
								<td class="titulos2">Desc. Intereses</td> 
								<td class="titulos2">Tot. Int Predial</td>                              
								<td class="titulos2">Sobretasa Bombe</td>
								<td class="titulos2">Intereses</td>
								<td class="titulos2">Sobretasa Amb</td>
								<td class="titulos2">Intereses</td>
								<td class="titulos2">Descuentos</td>
								<td class="titulos2">Valor Total</td>
							</tr>
							<?php	
							//echo "hollaaaa";
							/* ereg( "([0-9]{1,2})/([0-9]{1,2})/([0-9]{2,4})", $_POST[fechaav],$fecha);
							$fechaactual=$fecha[3]."-".$fecha[2]."-".$fecha[1];	 */
										
							//$tasaintdiaria=(pow((1+$_POST[tasamora]/100),(1/365))-1); Compuesta
							$_POST['intpredial'] = 0;
							$_POST['totpredial'] = 0;
							$_POST['intbomb'] = 0;
							$_POST['totbomb'] = 0;
							$_POST['intamb'] = 0;
							$_POST['totamb'] = 0;
							$_POST['totint'] = 0;
							$_POST['totdesc'] = 0;
							
							$co="zebra1";
							$co2="zebra2";	
							$sqlr = "select * from  tesoautorizapredial_det where idpredial='$_POST[autorizacion]'";
							//echo $sqlr;
							$res = mysqli_query($linkbd, $sqlr);			  
							while($r = mysqli_fetch_assoc($res))
							{
								echo "<tr class='$co'>

										<input name='dvigencias[]' value='".$r['vigliquidada']."' type='hidden' class='inpnovisibles' readonly>
										<input name='dcodcatas[]' value='".$_POST['codcat']."' type='hidden' class='inpnovisibles' readonly>
										<input name='dvaloravaluo[]' value='".$r['avaluo']."' type='hidden' >
										<input name='dtasavig[]' value='".$r['tasav']."' type='hidden' >
										<input name='dpredial[]' value='".$r['predial']."' type='hidden' class='inpnovisibles' readonly>
										<input name='dipredial[]' value='".$r['intpredial']."' type='hidden' class='inpnovisibles' readonly>
										<input name='ddescipredial[]' value='0' type='hidden' class='inpnovisibles' readonly>
										<input name='ditpredial[]' value='0' type='hidden' class='inpnovisibles' readonly>
										<input name='dimpuesto1[]' value='".$r['bomberil']."' type='hidden' class='inpnovisibles' readonly>
										<input name='dinteres1[]' value='".$r['intbomb']."' type='hidden' class='inpnovisibles' readonly>
										<input name='dimpuesto2[]' value='".$r['medioambiente']."' type='hidden' class='inpnovisibles' readonly>
										<input name='dinteres2[]' value='".$r['intmedioambiente']."' type='hidden' class='inpnovisibles' readonly>
										<input type='hidden' class='inpnovisibles' name='ddescuentos[]' value='".$r['descuentos']."' readonly>
										<input name='davaluos[]' class='inpnovisibles' value='".number_format($r['totaliquidavig'],2)."' type='hidden' readonly>
										<input name='dhavaluos[]'  value='".$r['totaliquidavig']."' type='hidden' >


									<td>".$r['vigliquidada']."</td>
									<td>".$_POST['codcat']."</td>
									<td>".$r['predial']."</td>
									<td>".$r['intpredial']."</td>
									<td>0</td>
									<td>0</td>
									<td>".$r['bomberil']."</td>
									<td>".$r['intbomberil']."</td>
									<td>".$r['medioambiente']."</td>
									<td >".$r['intmedioambiente']."</td>
									<td>".$r['descuentos']."</td>
									<td>".number_format($r['totaliquidavig'],2)."</td>
									
								</tr>";

								$ageliqui=$ageliqui." ".$r['vigliquidada'];
							
								$_POST['totalc'] = $_POST['totalc']+$r['totaliquidavig'];
								$_POST['totalcf'] = number_format($_POST['totalc'],2,".",",");

								$_POST['intpredial'] += $r['intpredial'];
								$_POST['totpredial'] += $r['predial'];
								$_POST['intbomb'] += $r['intbomberil'];
								$_POST['totbomb'] += $r['bomberil'];
								$_POST['intamb'] += $r['intmedioambiente'];
								$_POST['totamb'] += $r['medioambiente'];
								$_POST['totint'] = $_POST['totint'] + $r['intpredial'] + $r['intbomberil'] + $r['intmedioambiente'];
								$_POST['totdesc'] += $r['descuentos'];
								$aux=$co;
								$co=$co2;
								$co2=$aux;
							}
							$_POST['totliquida'] = $_POST['totalc'];
							$resultado = convertir($_POST['totliquida']);
							$_POST['letras']=$resultado." PESOS M/CTE";	
							?> 
						</table>
      				</div>
					<table class="inicio">
						<tr>
							<td class="saludo1">Total:</td>
							<td>
								<input type="text" name="totliquida2" value="<?php echo number_format($_POST['totliquida'],2)?>" size="12"  readonly>
								<input type="hidden" name="totliquida" value="<?php echo $_POST['totliquida']?>" size="12" readonly>
							</td>
							<td class="saludo1">Total Predial:</td>
							<td>
								<input type="hidden" name="intpredial" value="<?php echo $_POST['intpredial']?>">
								<input type="text" name="totpredial" value="<?php echo $_POST['totpredial']?>" size="9" readonly>
							</td>
							<td class="saludo1">Total Sobret Bomberil:</td>
							<td>
								<input type="hidden" name="intbomb" value="<?php echo $_POST['intbomb']?>">
								<input type="text" name="totbomb" value="<?php echo $_POST['totbomb']?>" size="9" readonly>
							</td>
							<td class="saludo1">Total Sobret Ambiental:</td>
							<td>
								<input type="hidden" name="intamb" value="<?php echo $_POST['intamb']?>">
								<input type="text" name="totamb" value="<?php echo $_POST['totamb']?>" size="9" readonly>
							</td>
							<td class="saludo1">Total Intereses:</td>
							<td>
								<input type="text" name="totint" value="<?php echo $_POST['totint']?>" size="9" readonly>
							</td>
							<td class="saludo1">Descuentos:</td>
							<td>
								<input type="text" name="totdesc"  value="<?php echo $_POST['totdesc']?>" size="9" readonly>
							</td>
						</tr>
						<tr>
							<td class="saludo1" >Son:</td>
							<td colspan="8">
								<input type="text" name="letras"  value="<?php echo $_POST['letras']?>" size="155">
							</td>
						</tr>
					</table>
							<?php
							if ($_POST['oculto']=='2')
							{
		 						/* ereg( "([0-9]{1,2})/([0-9]{1,2})/([0-9]{2,4})", $_POST['fecha'],$fecha);
								$fechaf=$fecha[3]."-".$fecha[2]."-".$fecha[1]; */

								preg_match("/([0-9]{2})\/([0-9]{2})\/([0-9]{4})/", $_POST['fecha'], $fecha);
								$fechaf = $fecha[3]."-".$fecha[2]."-".$fecha[1];
								$vigusu = $fecha[3];

								$_POST['numpredial']=selconsecutivo('tesoliquidapredial','idpredial');
								$sqlr="insert into tesoliquidapredial (idpredial,codigocatastral, fecha, vigencia, tercero,tasamora, descuento, tasapredial, totaliquida, totalpredial, totalbomb, totalmedio, totalinteres, intpredial, intbomb, intmedio, totaldescuentos, concepto,estado,ord,tot) values ('$_POST[numpredial]','$_POST[codcat]','$fechaf','".$vigusu."','$_POST[tercero]','$_POST[tasamora]','$_POST[descuento]','$_POST[tasa]','$_POST[totliquida]','$_POST[totpredial]', '$_POST[totbomb]','$_POST[totamb]' ,'$_POST[totint]','$_POST[intpredial]','$_POST[intbomb]','$_POST[intamb]','$_POST[totdesc]','".utf8_decode("Años Liquidados:".$ageliqui)."','S','$_POST[ord]','$_POST[tot]')";
								if(!mysqli_query($linkbd, $sqlr))
	 							{  
	   								echo "
									<script>
										despliegamodalm('visible','2','No Se ha podido Liquidar el Predial');
										document.getElementById('valfocus').value='2';
									</script>";
	 							}
	  							else
	   							{
									$idp = $_POST['numpredial']; 		 
									$sqlr="update tesoautorizapredial set liquidacion='$idp' where id_auto='$_POST[autorizacion]' ";
									mysqli_query($linkbd, $sqlr);
									echo "<input name='idpredial' value='$idp' type='hidden' >";
									echo "<script>despliegamodalm('visible','1','Se ha liquidado el predial');</script>";		 
									
									for($x=0;$x<count($_POST['dvigencias']);$x++)
									{
										$sqlr="insert into tesoliquidapredial_det (`idpredial`, `vigliquidada`, avaluo,tasav,`predial`, `intpredial`, `bomberil`, `intbomb`, `medioambiente`, `intmedioambiente`, `descuentos`, `totaliquidavig`, `estado`) values ($idp,'".$_POST['dvigencias'][$x]."',".$_POST['dvaloravaluo'][$x].",".$_POST['dtasavig'][$x].",".$_POST['dpredial'][$x].",".$_POST['dipredial'][$x].",".$_POST['dimpuesto1'][$x].",".$_POST['dinteres1'][$x].",".$_POST['dimpuesto2'][$x].",".$_POST['dinteres2'][$x].",".$_POST['ddescuentos'][$x].",".$_POST['dhavaluos'][$x].",'S')";
										mysqli_query($linkbd, $sqlr);
											
									}		 
		  							
		   							echo "<table class='inicio'><tr><td class='saludo1'><center>".utf8_decode("Años Liquidados:")." $ageliqui <img src='imagenes\confirm.png'> </center></td></tr></table>";  
		
								}  
	 						}
    						?>
					</form>
				</td>
			</tr>
		</table>
	</body>
</html>