<?php
	header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=utf8");
	require "comun.inc";
	require "funciones.inc";
	require "conversor.php";
	require "validaciones.inc";
	$linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");
	session_start();
    date_default_timezone_set("America/Bogota");
?>
<!DOCTYPE>
<html lang=es>

<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1" />
    <meta name="viewport" content="user-scalable=no">
    <title>:: IDEAL 10 - Contabilidad</title>
    <link href="favicon.ico" rel="shortcut icon" />
    <link href="css/css2.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
    <link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
    <link href="sweetalert2/dist/sweetalert2.min.css" rel="stylesheet">
    <script src="sweetalert2/dist/sweetalert2.min.js"></script>
    <script type="text/javascript" src="css/programas.js"></script>
    <script>
    function guardar() {
        if (document.form2.fecha.value != '' && ((document.form2.modorec.value == 'banco' && document.form2.banco
                .value != '') || (document.form2.modorec.value == 'caja') || (document.form2.cuentaPuente.value !=
                ''))) {
            Swal.fire({
                icon: 'question',
                title: '¿Seguro que quieres reflejar la información?',
                showDenyButton: true,
                confirmButtonText: 'Guardar',
                confirmButtonColor: '#01CC42',
                denyButtonText: 'Cancelar',
                denyButtonColor: '#FF121A',
            }).then(
                (result) => {
                    if (result.isConfirmed) {
                        document.form2.oculto.value = 2;
                        document.form2.ncomp.value = document.form2.idcomp.value;
                        document.form2.submit();
                    } else if (result.isDenied) {
                        Swal.fire({
                            icon: 'info',
                            title: 'No se reflejo la información',
                            confirmButtonText: 'Continuar',
                            confirmButtonColor: '#FF121A',
                            timer: 2500
                        });
                    }
                }
            )
        } else {
            Swal.fire({
                icon: 'error',
                title: 'Error!',
                text: 'Faltan datos para poder reflejar',
                confirmButtonText: 'Continuar',
                confirmButtonColor: '#FF121A',
                timer: 2500
            });
            document.form2.fecha.focus();
            document.form2.fecha.select();
        }
    }

    function verUltimaPos(idcta) {
        window.open("teso-editaingresos.php?idr=" + idcta);
    }

    function pdf() {
        document.form2.action = "teso-pdfrecaja.php";
        document.form2.target = "_BLANK";
        document.form2.submit();
        document.form2.action = "";
        document.form2.target = "";
    }

    function adelante() {
        if (parseFloat(document.form2.ncomp.value) < parseFloat(document.form2.maximo.value)) {
            document.form2.oculto.value = 1;
            if (document.getElementById('codrec').value != "") {
                document.getElementById('codrec').value = ""
            }
            document.form2.ncomp.value = parseFloat(document.form2.ncomp.value) + 1;
            document.form2.idcomp.value = parseFloat(document.form2.idcomp.value) + 1;
            document.form2.action = "cont-recibocaja-reflejar.php";
            document.form2.submit();
        }
    }

    function atrasc() {
        if (document.form2.ncomp.value > 1) {
            document.form2.oculto.value = 1;
            if (document.getElementById('codrec').value != "") {
                document.getElementById('codrec').value = ""
            }
            document.form2.ncomp.value = document.form2.ncomp.value - 1;
            document.form2.idcomp.value = document.form2.idcomp.value - 1;
            document.form2.action = "cont-recibocaja-reflejar.php";
            document.form2.submit();
        }
    }

    function validar2() {
        document.form2.oculto.value = 1;
        document.form2.ncomp.value = document.form2.idcomp.value;
        document.form2.action = "cont-recibocaja-reflejar.php";
        document.form2.submit();
    }
    </script>
    <?php
			function buscanumcuenta($ncod,$fechaf){
				$linkbd = conectar_v7();
				$linkbd -> set_charset("utf8");
				$sqlr="SELECT * FROM conceptoscontables_det WHERE estado='S' AND modulo='4' AND codigo='$ncod' AND tipo='C' AND fechainicial=(SELECT MAX(fechainicial) FROM conceptoscontables_det WHERE estado='S' AND modulo='4' AND codigo='$ncod' AND tipo='C' AND fechainicial<='$fechaf')";
				$res=mysqli_query($linkbd,$sqlr);
				while($row=mysqli_fetch_row($res)){
					if($row[3]=='N'){
						if($row[7]=='N'){
							$cuenta=$row[4];
						}
					}
				}
				return $cuenta;
			}
		?>
</head>

<body>
    <IFRAME src="alertas.php" name="alertas" id="alertas" style="display:none"></IFRAME>
    <span id="todastablas2"></span>
    <table>
        <tr>
            <script>
            barra_imagenes("cont");
            </script><?php cuadro_titulos();?>
        </tr>
        <tr><?php menu_desplegable("cont");?></tr>
        <tr>
            <td colspan="3" class="cinta">
                <a class="mgbt"><img src="imagenes/add2.png" /></a>
                <a class="mgbt"><img src="imagenes/guardad.png" /></a>
                <a href='cont-buscarecibocaja-reflejar.php' class="mgbt"><img src="imagenes/busca.png"
                        title="Buscar" /></a>
                <a onClick="mypop=window.open('plan-agenda.php','','');mypop.focus()" class="mgbt"><img
                        src="imagenes/agenda1.png" title="Agenda" /></a>
                <a onClick="mypop=window.open('cont-principal.php','','');mypop.focus();" class="mgbt"><img
                        src="imagenes/nv.png" title="Nueva ventana" /></a>
                <a onClick="mypop=window.open('<?php echo $url2; ?>','','');mypop.focus();"
                    class="tooltip bottom mgbt"><img src="imagenes/duplicar_pantalla.png"
                        title="Duplicar pesta&ntilde;a">
                    <a onClick="guardar();" class="mgbt"><img src="imagenes/reflejar1.png" title="Reflejar"
                            style="width:24px;" /></a>
                    <a href='cont-reflejardocs.php' class="mgbt"><img src="imagenes/iratras.png"
                            title="Atr&aacute;s" /></a>
            </td>
        </tr>
    </table>
    <form name="form2" method="post" action="">
        <input type="hidden" name="codrec" id="codrec" value="<?php echo $_POST['codrec'];?>" />
        <?php
				$vigencia = $vigusu = vigencia_usuarios($_SESSION['cedulausu']);
				$sqlr = "select cuentacaja, cobro_ambiental from tesoparametros";
				$res = mysqli_query($linkbd,$sqlr);
				while ($row = mysqli_fetch_row($res)){
					$_POST['cuentacaja'] = $row[0];
					$_POST['cobro_ambiental'] = $row[1];
				}
				if(!$_POST['oculto']){
					$sqlr = "select valor_inicial,valor_final, tipo from dominios where nombre_dominio = 'COBRO_RECIBOS' AND descripcion_valor = '$vigusu' and tipo = 'S'";
					$res = mysqli_query($linkbd,$sqlr);
					while ($row =mysqli_fetch_row($res)){
						$_POST['cobrorecibo'] = $row[0];
						$_POST['vcobrorecibo'] = $row[1];
						$_POST['tcobrorecibo'] = $row[2];
					}
				}
				//*********** 11050501	CAJA PRINCIPAL esta es la cuenta que va a credito en todas las consignacones
				if ($_GET['consecutivo']!=""){
					echo "<script>document.getElementById('codrec').value=$_GET[consecutivo];</script>";
				}
				$sqlr = "select id_recibos,id_recaudo from tesoreciboscaja ORDER BY id_recibos DESC";
				$res = mysqli_query($linkbd,$sqlr);
				$r = mysqli_fetch_row($res);
				$_POST['maximo']=$r[0];
				if(!$_POST['oculto']){
					$fec = date("d/m/Y");
					$_POST['vigencia'] = $vigencia;
					if ($_POST['codrec'] != "" || $_GET['consecutivo'] != ""){
						if($_POST['codrec'] != ""){
							$sqlr = "select id_recibos,id_recaudo from  tesoreciboscaja where id_recibos='$_POST[codrec]'";
						}else{
							$sqlr = "select id_recibos,id_recaudo from  tesoreciboscaja where id_recibos='$_GET[consecutivo]'";
						}
					}else{
						$sqlr = "select id_recibos,id_recaudo from  tesoreciboscaja ORDER BY id_recibos DESC";
					}
					$res = mysqli_query($linkbd,$sqlr);
					$r = mysqli_fetch_row($res);
					$_POST['ncomp'] = $r[0];
					$_POST['idcomp'] = $r[0];
					$_POST['idrecaudo'] = $r[1];
				}
				if ($_POST['codrec']!=""){
					$sqlr = "select * from tesoreciboscaja where id_recibos='".$_POST['codrec']."'";
				}else{
					$sqlr="select * from tesoreciboscaja where id_recibos='".$_POST['idcomp']."'";
				}
				$res = mysqli_query($linkbd,$sqlr);
				while($r = mysqli_fetch_row($res)){
					$_POST['tiporec'] = $r[10];
					$_POST['idrecaudo'] = $r[4];
					$_POST['ncomp'] = $r[0];
					$_POST['modorec'] = $r[5];
					$_POST['vigencia'] = $r[3];
					$_POST['cuentabanco'] = $r[7];
				}
			?>
        <input type="hidden" name="cobrorecibo" value="<?php echo $_POST['cobrorecibo']?>">
        <input type="hidden" name="vcobrorecibo" value="<?php echo $_POST['vcobrorecibo']?>">
        <input type="hidden" name="tcobrorecibo" value="<?php echo $_POST['tcobrorecibo']?>">
        <input type="hidden" name="encontro" value="<?php echo $_POST['encontro']?>">
        <input type="hidden" name="codcatastral" value="<?php echo $_POST['codcatastral']?>">
        <input type="hidden" name="cobro_ambiental" value="<?php echo $_POST['cobro_ambiental']?>">
        <?php
				switch($_POST['tiporec']){
					 //Predial
					case 1:{
						$sql="SELECT FIND_IN_SET($_POST[idcomp],recibo),idacuerdo FROM tesoacuerdopredial ";
						$result=mysqli_query($linkbd,$sql);
						$val=0;
						$compro=0;
						while($fila = mysqli_fetch_row($result)){
							if($fila[0]!=0){
								$val=$fila[0];
								$compro=$fila[1];
								break;
							}
						}
						if($val>0){
							$_POST['tipo']="1";
							$_POST['idrecaudo']=$compro;
							$sqlr = "SELECT vigencia FROM tesoacuerdopredial_det WHERE idacuerdo='".$_POST['idrecaudo']."'";
							$res = mysqli_query($linkbd,$sqlr);
							$vigencias="";
							while($row = mysqli_fetch_row($res)){
								$vigencias.=($row[0]."-");
							}
							$vigencias = "Años liquidados: ".substr($vigencias,0,-1);
							$sql= "SELECT * FROM tesoacuerdopredial WHERE idacuerdo='".$_POST['idrecaudo']."' AND (estado='S' or estado='P') ";
							$result = mysqli_query($linkbd,$sql);
							$_POST['encontro']="";
							while($row = mysqli_fetch_row($result)){
									$_POST['cuotas']=$row[10]+1;
									$_POST['tcuotas']=$row[4];
									$_POST['codcatastral']=$row[1];
									$_POST['concepto']=$vigencias.' Cod Catastral No '.$row[1];
									$_POST['valorecaudo']=$row[7];
									$_POST['totalc']=$row[7];
									$_POST['tercero']=$row[13];
									$_POST['encontro']=1;
							}
							$sqlr1="SELECT nombrepropietario FROM tesopredios WHERE cedulacatastral='".$_POST['codcatastral']."' AND estado='S'";
							$resul = mysqli_query($linkbd,$sqlr1);
							$row1 = mysqli_fetch_row($resul);
							$_POST['ntercero']=$row1[0];
							if ($_POST['ntercero']==''){
								$sqlr2="SELECT * from tesopredios WHERE cedulacatastral='$row[1]' ";
								$resc = mysqli_query($linkbd,$sqlr2);
								$rowc = mysqli_fetch_row($resc);
								$_POST['ntercero']=$rowc[6];
							}
						}else{
							$_POST['tipo']="2";
							$sqlr="SELECT * FROM tesoliquidapredial, tesoreciboscaja WHERE tesoliquidapredial.idpredial = tesoreciboscaja.id_recaudo AND tesoreciboscaja.estado !='' AND tesoreciboscaja.id_recibos='".$_POST['idcomp']."'";
							$_POST['encontro']="";
							$res = mysqli_query($linkbd,$sqlr);
							while ($row =mysqli_fetch_row($res)){
								preg_match("/([0-9]{4})\-([0-9]{2})\-([0-9]{2})/",$row[23],$fecha);
								$_POST['fecha']="$fecha[3]/$fecha[2]/$fecha[1]";
								$_POST['codcatastral']=$row[1];
								$_POST['idrecaudo']=$row[25];
								$_POST['vigencia']=$row[3];
								$_POST['concepto']=$row[17].' Cod Catastral No '.$row[1];
								$_POST['valorecaudo']=$row[8];
								$_POST['totalc']=$row[8];
								$_POST['tercero']=$row[4];
								$_POST['modorec']=$row[24];
								$_POST['banco']=$row[25];
								if($row[28]=='S') {$_POST['estadoc']='ACTIVO';}
								if($row[28]=='R') {$_POST['estadoc']='REVERSADO';}
								if($row[28]=='N'){$_POST['estadoc']='ANULADO';}
								$sqlr1="SELECT nombrepropietario FROM tesopredios WHERE cedulacatastral='".$_POST['codcatastral']."' AND estado='S'";
								$resul = mysqli_query($linkbd,$sqlr1);
								$row1 = mysqli_fetch_row($resul);
								$_POST['ntercero']=$row1[0];
								if ($_POST['ntercero']==''){
									$sqlr2="SELECT * FROM tesopredios WHERE cedulacatastral='".$row[1]."' ";
									$resc = mysqli_query($linkbd,$sqlr2);
									$rowc = mysqli_fetch_row($resc);
									$_POST['ntercero']=$rowc[6];
								}
								$_POST['encontro']=1;
							}
						}
						$sqlr="SELECT * FROM tesoreciboscaja WHERE tipo='1' AND id_recaudo=$_POST[idrecaudo] AND id_recibos=$_POST[idcomp]";
						$res = mysqli_query($linkbd,$sqlr);
						$row = mysqli_fetch_row($res);
						$_POST['estadoc']=$row[9];
						if ($_POST['estadoc']=='N') {
							$_POST['estado']="ANULADO";
						}else if($row[9]=='R'){
							$_POST['estado']="REVERSADO";
							$_POST['estadoc']='0';
						}else {
							$_POST['estado']="ACTIVO";
						}
						$_POST['modorec']=$row[5];
						$_POST['banco']=$row[7];
						preg_match("/([0-9]{4})\-([0-9]{2})\-([0-9]{2})/",$row[2],$fecha);
						$_POST['fecha']="$fecha[3]/$fecha[2]/$fecha[1]";
					}break;
					// Industria y Comercio
					case 2:{
						$sqlr="SELECT * FROM tesoindustria WHERE id_industria=$_POST[idrecaudo] AND 2=$_POST[tiporec]";
						$_POST['encontro']="";
						$res = mysqli_query($linkbd,$sqlr);
						while ($row =mysqli_fetch_row($res)){
							$_POST['concepto']="Liquidacion Industria y Comercio avisos y tableros - $row[3]";
							$_POST['valorecaudo']=$row[6];
							$_POST['totalc']=$row[6];
							$_POST['tercero']=$row[5];
							$_POST['ntercero']=buscatercero($row[5]);
							$_POST['encontro']=1;
						}
						$sqlr="SELECT * FROM tesoreciboscaja WHERE id_recibos='".$_POST['idcomp']."' ";
						$res = mysqli_query($linkbd,$sqlr);
						$row = mysqli_fetch_row($res);
						$_POST['estadoc']=$row[9];
						if ($row[9]=='N'){
							$_POST['estado']="ANULADO";
							$_POST['estadoc']='0';
						}else if($row[9]=='R'){
							$_POST['estado']="REVERSADO";
							$_POST['estadoc']='0';
						}else{
							$_POST['estadoc']='1';
							$_POST['estado']="ACTIVO";
						}
						preg_match("/([0-9]{4})\-([0-9]{2})\-([0-9]{2})/",$row[2],$fecha);
						$_POST['fecha']="$fecha[3]/$fecha[2]/$fecha[1]";
						$_POST['modorec']=$row[5];
						$_POST['banco']=$row[7];
					}break;
					//Otros Recaudos
					case 3:{
						$sqlr="SELECT * FROM tesorecaudos where tesorecaudos.id_recaudo='".$_POST['idrecaudo']."' and 3=".$_POST['tiporec'];
						$_POST['encontro']="";
						$res=mysqli_query($linkbd,$sqlr);
						while ($row =mysqli_fetch_row($res)){
							$_POST['concepto']=$row[6];
							$_POST['valorecaudo']=$row[5];
							$_POST['totalc']=$row[5];
							$_POST['tercero']=$row[4];
							$_POST['ntercero']=buscatercero($row[4]);
							$_POST['encontro']=1;
						}
						$sqlr="SELECT * FROM tesoreciboscaja WHERE  id_recibos='".$_POST['idcomp']."'";
						$res = mysqli_query($linkbd,$sqlr);
						$row = mysqli_fetch_row($res);
						preg_match("/([0-9]{4})\-([0-9]{2})\-([0-9]{2})/", $row[2],$fecha);
						$_POST['fecha']="$fecha[3]/$fecha[2]/$fecha[1]";
						$_POST['estadoc']=$row[9];
						if ($row[9]=='N'){
							$_POST['estado']="ANULADO";
							$_POST['estadoc']='0';
						}else if($row[9]=='R'){
							$_POST['estado']="REVERSADO";
							$_POST['estadoc']='0';
						}else{
							$_POST['estadoc']='1';
							$_POST['estado']="ACTIVO";
						}
						$_POST['modorec']=$row[5];
						$_POST['banco']=$row[7];
					}break;
				}
			?>
        <table class="inicio" style="width:99.7%;">
            <tr>
                <td class="titulos" colspan="9">Reflejar Recibo de Caja</td>
                <td class="cerrar" style="width:7%" onClick="location.href='cont-principal.php'">Cerrar</td>
            </tr>
            <tr>
                <td class="saludo1" style="width:2cm;">No Recibo:</td>
                <td style="width:20%;" colspan="<?php if($_POST['tiporec']=='1'){echo '3'; }else{echo '1';}?>">
                    <a href="#" onClick="atrasc()"><img src="imagenes/back.png" title="anterior" /></a>
                    <input type="hidden" name="cuentacaja" value="<?php echo $_POST['cuentacaja']?>" />
                    <input type="text" name="idcomp" value="<?php echo $_POST['idcomp']?>"
                        onKeyUp="return tabular(event,this) " onBlur="validar2()" style="width:50%;" />
                    <input type="hidden" name="ncomp" id="ncomp" value="<?php echo $_POST['ncomp']?>" />
                    <a href="#" onClick="adelante()"><img src="imagenes/next.png" title="siguiente" /></a>
                    <input type="hidden" value="a" name="atras" />
                    <input type="hidden" value="s" name="siguiente" />
                    <input type="hidden" name="maximo" value="<?php echo $_POST['maximo']?>" />
                </td>
                <td class="saludo1" style="width:2.3cm;">Fecha:</td>
                <td style="width:18%;"><input type="text" name="fecha" value="<?php echo $_POST['fecha']?>"
                        onKeyUp="return tabular(event,this)" style="width:45%;" readonly />
                    <?php
							if($_POST['estado']=='ACTIVO'){
								echo "<input name='estado' type='text' value='ACTIVO' size='5' style='width:52%; background-color:#0CD02A; color:white; text-align:center;' readonly >";
							}else if($_POST['estado']=='REVERSADO'){
								echo "<input name='estado' type='text' value='REVERSADO' size='5' style='width:40%; background-color:#FF0000; color:white; text-align:center;' readonly >";
							}else{
								echo "<input name='estado' type='text' value='ANULADO' size='5' style='width:40%; background-color:#FF0000; color:white; text-align:center;' readonly >";
							}
						?>
                </td>
                <td class="saludo1" style="width:2.5cm;">Vigencia:</td>
                <td style="width:12%;">
                    <input type="text" id="vigencia" name="vigencia" onKeyUp="return tabular(event,this)"
                        value="<?php echo $_POST['vigencia']?>" readonly>
                </td>
                <td rowspan="6" colspan="2"
                    style="background:url(imagenes/LOGOIDEAL10c.png); background-repeat:no-repeat; background-position:right; background-size: 100% 100%;">
                </td>
            </tr>
            <tr>
                <td class="saludo1"> Recaudo:</td>
                <td>
                    <select name="tiporec" id="tiporec" onKeyUp="return tabular(event,this)" style="width:100%;">
                        <?php
								switch($_POST['tiporec']){
									case "1":	echo"<option value='1' SELECTED>Predial</option>";break;
									case "2":	echo"<option value='2' SELECTED>Industria y Comercio</option>";break;
									case "3":	echo"<option value='3' SELECTED>Otros Recaudos</option>";break;
								}
							?>
                    </select>
                </td>
                <?php
						if($_POST['tiporec']=='1'){
							echo"
							<td class='saludo1'> Tipo:</td>
							<td>
								<select name='tipo' id='tipo' style='width:100%;'>";
								if($_POST['tipo']==''){echo"<option value='' 'SELECTED'> Sin Especificar</option>";}
								if($_POST['tipo']=='1'){echo "<option value='1' 'SELECTED'>Por Acuerdo</option>";}
								if($_POST['tipo']=='2'){echo "<option value='2' 'SELECTED'>Por Liquidacion</option>";}
								echo "</select>
							</td>";
						}
						if($_POST['tipo']=='1'){
							echo"<td class='saludo1'>No. Acuerdo:</td>";
						}elseif($_POST['tipo']=='2'){
							echo "<td class='saludo1'>No Liquidaci&oacute;n:</td>";
						}
					?>
                <td><input type="text" id="idrecaudo" name="idrecaudo" value="<?php echo $_POST['idrecaudo']?>"
                        onKeyUp="return tabular(event,this)" onChange="validar()" style="width:100%;" readonly></td>
                <?php

						if($_POST['tipo']=='2'){
							$sqlrAbono = "SELECT * FROM tesoabono WHERE cierre='".$_POST['idrecaudo']."'";
							$rowAbono = view($sqlrAbono);
						}
						if($rowAbono==NULL){
							$_POST['cuentaPuente']='';
							echo"
							<td class='saludo1'>Recaudado en:</td>
							<td>
								<select name='modorec' id='modorec' style='width:100%;'>";
							if($_POST['modorec']=='banco'){echo"<option value='banco' SELECTED>Banco</option>";}
							else{echo"<option value='caja' SELECTED>Caja</option>";}
							echo"
								</select>
							</td>";
						}else{
							if($_POST['idrecaudo']!=''){
								$_POST['modorec']=NULL;
								$sqCuentaPuente = "SELECT cuentapuente FROM tesoparametros";
								$rowCuentaPuente = view($sqCuentaPuente);
								echo"
								<td class='saludo1'>Cuenta Puente:</td>
								<td>
									<input type='text' name='cuentaPuente' id='cuentaPuente' value='".$rowCuentaPuente[0]['cuentapuente']."' readonly/>
									<input type='hidden' name='modorec' id='modorec' value='".$_POST['modorec']."'/>
								</td>";
							}
						}
					?>
            </tr>
            <?php
					if ($_POST['modorec']=='banco' && $_POST['cuentaPuente']==''){
						echo"
						<tr>
							<td class='saludo1'>Cuenta:</td>
							<td>
								<select id='banco' name='banco' onChange='validar()' onKeyUp='return tabular(event,this)' style='width:100%'>
									<option value=''>Seleccione....</option>";
						$sqlr="SELECT TB1.estado,TB1.cuenta,TB1.ncuentaban,TB1.tipo,TB2.razonsocial,TB1.tercero FROM tesobancosctas TB1,terceros TB2 WHERE TB1.tercero=TB2.cedulanit AND TB1.estado='S' ";
						$res=mysqli_query($linkbd,$sqlr);
						while ($row =mysqli_fetch_row($res)){
							if("$row[1]"==$_POST['banco']){
								echo "<option value='$row[1]' SELECTED>$row[2] - Cuenta $row[3]</option>";
								$_POST['nbanco']=$row[4];
								$_POST['ter']=$row[5];
								$_POST['cb']=$row[2];
							}else{
								echo "<option value='$row[1]'>$row[2] - Cuenta $row[3]</option>";
							}
						}
						echo"
								</select>
							</td>
							<input type='hidden' name='cb' value='$_POST[cb]'/>
							<input type='hidden' id='ter' name='ter' value='$_POST[ter]'/></td>
							<td class='saludo1'>Banco:</td>
							<td colspan='3'><input type='text' id='nbanco' name='nbanco' value='$_POST[nbanco]' style='width:100%;' readonly></td>
							<input type='hidden' id='cuentabanco' name='cuentabanco' value='".$_POST['cuentabanco']."'/>
						</tr>";
					}

					$sqlr="SELECT nota FROM teso_notasrevelaciones WHERE modulo='teso' AND tipo_documento='5' AND numero_documento='$_POST[idcomp]'";
					$res = mysqli_query($linkbd,$sqlr);
					$row = mysqli_fetch_row($res);
					$_POST['notaf'] = $row[0];
				?>
            <tr>
                <td class="saludo1">Concepto:</td>
                <td colspan="<?php if($_POST['tiporec']==2){echo '3';}else{echo'5';}?>">
                    <input name="concepto" type="text" value="<?php echo $_POST['concepto'] ?>"
                        onKeyUp="return tabular(event,this)" style="width:95%;" readonly>
                    <input type="hidden" name="notaf" id="notaf" value="<?php echo $_POST['notaf']?>">
                    <?php
							if($_POST['notaf']==''){
								echo"<img src='imagenes/notad.png' class='icobut' onClick=\"despliegamodal2('visible',2);\" title='Notas'>";
							}else{
								echo"<img src='imagenes/notaf.png' class='icobut' onClick=\"despliegamodal2('visible',2);\" title='Notas'>";
							}
						?>
                </td>
            </tr>
            <tr>
                <td class="saludo1">Documento: </td>
                <td colspan="<?php if($_POST['tiporec']=='1'){echo '3'; }else{echo '1';}?>"><input name="tercero"
                        type="text" value="<?php echo $_POST['tercero']?>" onKeyUp="return tabular(event,this)"
                        style="width:100%;" readonly></td>
                <td class="saludo1">Contribuyente:</td>
                <td colspan="3">
                    <input type="text" id="ntercero" name="ntercero" value="<?php echo $_POST['ntercero']?>"
                        onKeyUp="return tabular(event,this) " style="width:100%;" readonly>
                    <input type="hidden" id="cb" name="cb" value="<?php echo $_POST['cb']?>">
                    <input type="hidden" id="ct" name="ct" value="<?php echo $_POST['ct']?>">
                </td>
            </tr>
            <tr>
                <td class="saludo1">Valor:</td>
                <td colspan="<?php if($_POST['tiporec']=='1'){echo '3'; }else{echo '1';}?>"><input type="text"
                        id="valorecaudo" name="valorecaudo" value="<?php echo $_POST['valorecaudo']?>"
                        onKeyUp="return tabular(event,this)" style="width:100%;" readonly /></td>

            </tr>
            <?php if ($_POST['modorec']!='banco'){echo"<tr style='height:20;'><tr>";}?>
        </table>
        <input type="hidden" name="oculto" id="oculto" value="1" />
        <input type="hidden" value="<?php echo $_POST['trec']?>" name="trec">
        <input type="hidden" value="0" name="agregadet">
        <div class="subpantalla" style="height:40.2%; width:99.6%; overflow-x:hidden;">
            <?php
					function obtenerTipoPredio($catastral){
						$tipo="";
						$linkbd = conectar_v7();
						$linkbd -> set_charset("utf8");
						$sqlr="SELECT tipopredio FROM tesopredios WHERE cedulacatastral='$catastral'";
						$res=mysqli_query($linkbd,$sqlr);
						$r=mysqli_fetch_row($res);
						$tipo=$r[0];
						return $tipo;
					}
					if(!isset($_POST['oculto'])){
						$_POST['oculto'] = 1;
					}
					if($_POST['oculto'] && $_POST['encontro']=='1'){
						switch($_POST['tiporec']){
							//********PREDIAL
							case 1:{
								unset($_POST['dcoding']);
								unset($_POST['dncoding']);
								unset($_POST['dvalores']);
								$_POST['dcoding']= array();
								$_POST['dncoding']= array();
								$_POST['dvalores']= array();
								if($_POST['tcobrorecibo']=='S'){
									$_POST['dcoding'][]=$_POST['cobrorecibo'];
									$_POST['dncoding'][]=buscaingreso($_POST['cobrorecibo'])." ".$vigusu;
									$_POST['dvalores'][]=$_POST['vcobrorecibo'];
								}
								$_POST['trec']='PREDIAL';
								/* $sqlr="SELECT * FROM tesoliquidapredial_det WHERE idpredial='".$_POST['idrecaudo']."' AND estado ='S' AND 1='".$_POST['tiporec']."'"; */
								$sqlr="select tb1.vigliquidada, tb1.totaliquidavig, tb2.codigocatastral from tesoliquidapredial_det AS tb1, tesoliquidapredial AS tb2 where tb1.idpredial = '".$_POST['idrecaudo']."' AND tb1.idpredial = tb2.idpredial and tb1.estado ='S' and 1=".$_POST['tiporec'];
								$res = mysqli_query($linkbd,$sqlr);
								//*******************CREANDO EL RECIBO DE CAJA DE PREDIAL ***********************
								while ($row = mysqli_fetch_row($res)){
									$vig=$row[0];
									if(substr($row[2],0,2) == '00'){
										if($vig==$vigusu){
											$sqlr2 = "SELECT * FROM tesoingresos_predial WHERE codigo='03'";
											$res2 = mysqli_query($linkbd,$sqlr2);
											$row2 = mysqli_fetch_row($res2);
											$_POST['dcoding'][]=$row2[0];
											$_POST['dncoding'][]=$row2[1]." ".$vig;
											$_POST['dvalores'][]=$row[1];
										}else{
											$sqlr2 = "SELECT * FROM tesoingresos_predial WHERE codigo='04'";
											$res2 = mysqli_query($linkbd,$sqlr2);
											$row2 = mysqli_fetch_row($res2);
											$_POST['dcoding'][]=$row2[0];
											$_POST['dncoding'][]=$row2[1]." ".$vig;
											$_POST['dvalores'][]=$row[1];
										}
									}else{
										if($vig==$vigusu){
											$sqlr2 = "SELECT * FROM tesoingresos_predial WHERE codigo='01'";
											$res2 = mysqli_query($linkbd,$sqlr2);
											$row2 = mysqli_fetch_row($res2);
											$_POST['dcoding'][]=$row2[0];
											$_POST['dncoding'][]=$row2[1]." ".$vig;
											$_POST['dvalores'][]=$row[1];
										}else{
											$sqlr2 = "SELECT * FROM tesoingresos_predial WHERE codigo='02'";
											$res2 = mysqli_query($linkbd,$sqlr2);
											$row2 = mysqli_fetch_row($res2);
											$_POST['dcoding'][]=$row2[0];
											$_POST['dncoding'][]=$row2[1]." ".$vig;
											$_POST['dvalores'][]=$row[1];
										}
									}
								}

							}break;
							//***********INDUSTRIA Y COMERCIO
							case 2:{
								$_POST['dcoding']= array();
								$_POST['dncoding']= array();
								$_POST['dvalores']= array();
								$_POST['trec']='INDUSTRIA Y COMERCIO';
								if($_POST['tcobrorecibo']=='S'){
									$_POST['dcoding'][]=$_POST['cobrorecibo'];
									$_POST['dncoding'][]=buscaingreso($_POST['cobrorecibo'])." ".$vigusu;
									$_POST['dvalores'][]=$_POST['vcobrorecibo'];
								}
								$sqlr = "SELECT * FROM tesoindustria WHERE id_industria='".$_POST['idrecaudo']."' AND  2='$_POST[tiporec]'";
								$res = mysqli_query($linkbd,$sqlr);
								while ($row = mysqli_fetch_row($res)){
									$sqlr2 = "SELECT * FROM tesoingresos_ica WHERE codigo='$row[16]'";
									$res2 = mysqli_query($linkbd,$sqlr2);
									$row2 = mysqli_fetch_row($res2);
									$_POST['dcoding'][]=$row2[0];
									$_POST['dncoding'][]=$row2[1];
									if($row[8]>1){
										$_POST['dvalores'][]=$row[6]/$row[8];
									}else{
										$_POST['dvalores'][]=$row[6];
									}
								}
							}break;
							//*****************otros recaudos *******************
							case 3:{
								$_POST['trec']='OTROS RECAUDOS';
								$sqlr="SELECT * FROM tesorecaudos_det WHERE id_recaudo = $_POST[idrecaudo] AND 3=$_POST[tiporec]";
								$_POST['dcoding']= array();
								$_POST['dncoding']= array();
								$_POST['dvalores']= array();
								if($_POST['tcobrorecibo']=='S'){
									$_POST['dcoding'][] = $_POST['cobrorecibo'];
									$_POST['dncoding'][] = buscaingreso($_POST['cobrorecibo'])." ".$vigusu;
									$_POST['dvalores'][]=$_POST['vcobrorecibo'];
								}
								$res = mysqli_query($linkbd,$sqlr);
								while ($row = mysqli_fetch_row($res)){
									$_POST['dcoding'][]=$row[2];
									$sqlr2="SELECT nombre FROM tesoingresos WHERE codigo='".$row[2]."'";
									$res2 = mysqli_query($linkbd,$sqlr2);
									$row2 = mysqli_fetch_row($res2);
									$_POST['dncoding'][]=$row2[0];
									$_POST['dvalores'][]=$row[3];
								}
							}break;
						}
					}
				?>
            <table class="inicio">
                <tr>
                    <td colspan="4" class="titulos">Detalle Recibo de Caja</td>
                </tr>
                <tr>
                    <td class="titulos2">Codigo</td>
                    <td class="titulos2">Ingreso</td>
                    <td class="titulos2">Valor</td>
                </tr>
                <?php
						$_POST['totalc']=0;
						$iter='saludo1a';
						$iter2='saludo2';
						for ($x=0;$x<count($_POST['dcoding']);$x++){
							$idcta=$_POST['dcoding'][$x];
							echo "
							<tr class='$iter' onDblClick='verUltimaPos($idcta)'>
								<td style='width:10%;'><input type='hidden' name='dcoding[]' value='".$_POST['dcoding'][$x]."'>".$_POST['dcoding'][$x]."</td>
								<td><input type='hidden' name='dncoding[]' value='".$_POST['dncoding'][$x]."'>".$_POST['dncoding'][$x]."</td>
								<td style='width:20%;text-align:right;'><input type='hidden' name='dvalores[]' value='".$_POST['dvalores'][$x]."' style='width:100%;'>$ ".number_format($_POST['dvalores'][$x],2,',','.')."</td>
							</tr>";
							$_POST['totalc']=$_POST['totalc']+$_POST['dvalores'][$x];
							$_POST['totalcf']=number_format($_POST['totalc'],2);
							$aux=$iter;
							$iter=$iter2;
							$iter2=$aux;
						}
						$resultado = convertir($_POST['totalc']);
						$_POST['letras']="$resultado PESOS M/CTE";
						echo "
						<tr class='$iter' >
							<td style='text-align:right;' colspan='2'>Total:</td>
							<td style='text-align:right;'>
								<input type='hidden' name='totalcf' value='".$_POST['totalcf']."'>
								<input name='totalc' type='hidden' value='".$_POST['totalc']."'>$ ".number_format($_POST['totalc'],2,',','.')."
							</td>
						</tr>
						<tr class='titulos2'>
							<td>Son:</td>
							<td colspan='5'><input type='hidden' name='letras' value='".$_POST['letras']."'>".$_POST['letras']."</td>
						</tr>";
					?>
            </table>
        </div>
        <?php
		if($_POST['oculto']=='2'){
			if(strpos($_POST['fecha'],"-")===false){
				preg_match("/([0-9]{2})\/([0-9]{2})\/([0-9]{4})/", $_POST['fecha'],$fecha);
				$fechaf="$fecha[3]-$fecha[2]-$fecha[1]";
			}else{
				$fechaf=$_POST['fecha'];
			}
			$bloq = bloqueos($_SESSION['cedulausu'],$fechaf);
			if($bloq >= 1){
				switch($_POST['tiporec']){//************VALIDAR SI YA FUE GUARDADO ************************
					case 1:{//***** PREDIAL *****************************************
						$sqlr="SELECT count(*) FROM tesoreciboscaja WHERE id_recaudo='".$_POST['idrecaudo']."' AND tipo='1' ";
						$res = mysqli_query($linkbd,$sqlr);
						while($r = mysqli_fetch_row($res)){
							$numerorecaudos = $r[0];
						}
						if($numerorecaudos >= 0){
							$sql="DELETE FROM comprobante_cab WHERE numerotipo = '".$_POST['idcomp']."' AND  tipo_comp='5' ";
							mysqli_query($linkbd,$sql);
							$sql="DELETE FROM comprobante_det WHERE numerotipo = '".$_POST['idcomp']."' AND  tipo_comp='5' ";
							mysqli_query($linkbd,$sql);
							if($_POST['modorec']=='caja'){
								$cuentacb = $_POST['cuentacaja'];
								$cajas = $_POST['cuentacaja'];
								$cbancos = "";
							}
							if($_POST['modorec']=='banco'){
								$cuentacb = $_POST['banco'];
								$cajas = "";
								$cbancos = $_POST['banco'];
							}
							if($_POST['cuentaPuente']!=''){
								$cuentacb = $_POST['cuentaPuente'];
								$cajas = "";
								$cbancos = buscabanco($_POST['cuentaPuente']);
							}
							$concecc = $_POST['idcomp'];
							if(strpos($_POST['fecha'],"-")===false){
								preg_match("/([0-9]{2})\/([0-9]{2})\/([0-9]{4})/", $_POST['fecha'],$fecha);
								$fechaf = "$fecha[3]-$fecha[2]-$fecha[1]";
							}else{
								$fechaf = $_POST['fecha'];
							}
							//************ insercion de cabecera recaudos ************
							echo "
							<input type='hidden' name='concec' value='$concecc'>
							<script>
								despliegamodalm('visible','1','Se ha almacenado el Recibo de Caja con Exito');
								document.form2.vguardar.value='1';
								document.form2.numero.value='';
								document.form2.valor.value=0;
							</script>";
							//**********************CREANDO COMPROBANTE CONTABLE ********************************
							if($_POST['estado']=='ACTIVO' || $_POST['estado']=='REVERSADO'){
								$estado=1;
							}else{
								$estado=0;
							}
							$sqlr="INSERT INTO comprobante_cab (numerotipo, tipo_comp, fecha, concepto, total, total_debito, total_credito, diferencia, estado) values ($concecc,5,'$fechaf','".$_POST['concepto']."',0,".$_POST['totalc'].",".$_POST['totalc'].",0,'$estado')";
							mysqli_query($linkbd,$sqlr);
							//******parte para el recaudo del cobro por recibo de caja
							for($x=0;$x<count($_POST['dcoding']);$x++){
								if($_POST['dcoding'][$x]==$_POST['cobrorecibo']){
									//***** BUSQUEDA INGRESO ********
									$sqlri="SELECT * FROM tesoingresos_det WHERE codigo = '".$_POST['dcoding'][$x]."' AND vigencia=(SELECT MAX(vigencia) FROM tesoingresos_det WHERE codigo = '".$_POST['dcoding'][$x]."')";
									$resi = mysqli_query($linkbd,$sqlri);
									while($rowi=mysqli_fetch_row($resi)){
										//**** busqueda cuenta presupuestal*****
										//busqueda concepto contable
										$sq="SELECT fechainicial FROM conceptoscontables_det WHERE codigo = '$rowi[2]' AND modulo='4' AND tipo='C' AND fechainicial<'$fechaf' AND cuenta!='' ORDER BY fechainicial ASC";
										$re = mysqli_query($linkbd,$sq);
										while($ro=mysqli_fetch_assoc($re)){
											$_POST['fechacausa'] = $ro["fechainicial"];
										}
										$sqlrc="SELECT * FROM conceptoscontables_det WHERE estado='S' AND modulo='4' AND codigo='$rowi[2]' AND tipo='C' AND fechainicial='$_POST[fechacausa]'";
										$resc = mysqli_query($linkbd,$sqlrc);
										while($rowc = mysqli_fetch_row($resc)){
											$porce = $rowi[5];
											if($rowc[7]=='S'){
												$valorcred=$_POST['dvalores'][$x]*($porce/100);
												$valordeb=0;
												if($rowc[3]=='N'){
													//*****inserta del concepto contable
													//***********MODIFICAR CUENTA PPTAL DE INGRESO AGREGARLE EL RECAUDO *********
													$sqlrpto="SELECT * FROM pptocuentas WHERE estado='S' AND cuenta='$rowi[6]' AND vigencia='$vigusu'";
													$respto=mysqli_query($linkbd,$sqlrpto);
													$rowpto=mysqli_fetch_row($respto);
													$vi=$_POST['dvalores'][$x]*($porce/100);
													//************ FIN MODIFICACION PPTAL
													$sqlr="INSERT INTO comprobante_det (id_comp, cuenta, tercero, centrocosto, detalle, cheque,valdebito, valcredito, estado, vigencia) VALUES ('5 $concecc','$rowc[4]','".$_POST['tercero']."', '$rowc[5]','Ingreso ".strtoupper($_POST['dncoding'][$x])."','','".round($valordeb,0)."','".round($valorcred,0)."','1','".$_POST['vigencia']."')";
													mysqli_query($linkbd,$sqlr);
													//***cuenta caja o banco
													if($_POST['modorec']=='caja'){
														$cuentacb=$_POST['cuentacaja'];
														$cajas=$_POST['cuentacaja'];
														$cbancos="";
													}
													if($_POST['modorec']=='banco'){
														$cuentacb=$_POST['banco'];
														$cajas="";
														$cbancos=$_POST['banco'];
													}
													$sqlr="INSERT INTO comprobante_det (id_comp, cuenta, tercero, centrocosto, detalle,c heque, valdebito, valcredito, estado, vigencia) values ('5 $concecc','$cuentacb','".$_POST['tercero']."','$rowc[5]','Ingreso ".strtoupper($_POST['dncoding'][$x])."','','".round($valorcred,0)."','0','1','".$_POST['vigencia']."')";
													mysqli_query($linkbd,$sqlr);
												}
											}
										}
									}
								}
							}
							//*************** fin de cobro de recibo
							$sql="SELECT codigocatastral FROM tesoliquidapredial WHERE idpredial = ".$_POST['idrecaudo'];
							$resul = mysqli_query($linkbd,$sql);
							$rowcod = mysqli_fetch_row($resul);
							$tipopre = obtenerTipoPredio($rowcod[0]);
							$cedulaCatastral = '';
							/* if($_POST['tipo']=='1'){
								$sqlrs = "SELECT * FROM tesoacuerdopredial_det WHERE idacuerdo = ".$_POST['idrecaudo'];
								$res =mysqli_query($linkbd,$sqlrs);
								$rowd=mysqli_fetch_row($res);
								$tasadesc=(($rowd[5]/$_POST['tcuotas'])/100);
								$sqlr="SELECT * FROM tesoacuerdopredial_det WHERE idacuerdo = ".$_POST['idrecaudo'];

								$sqlrC = "SELECT codcatastral FROM tesoacuerdopredial idacuerdo = ".$_POST['idrecaudo']."";
								$resC = mysqli_query($linkbd, $sqlrC);
								$rowdC = mysqli_fetch_row($resC);
								$cedulaCatastral = $rowdC[0];
							}else{ */
								$sqlrs="SELECT * FROM tesoliquidapredial_det WHERE idpredial = ".$_POST['idrecaudo']." AND estado ='S'  AND 1 = ".$_POST['tiporec'];
								$res = mysqli_query($linkbd,$sqlrs);
								$rowd == mysqli_fetch_row($res);
								$tasadesc = ($rowd[6]/100);
								$sqlr="SELECT * FROM tesoliquidapredial_det WHERE idpredial = ".$_POST['idrecaudo']." AND estado ='S' AND 1=".$_POST['tiporec'];

								$sqlrC = "SELECT codigocatastral FROM tesoliquidapredial WHERE idpredial = ".$_POST['idrecaudo']."";
								$resC = mysqli_query($linkbd, $sqlrC);
								$rowdC = mysqli_fetch_row($resC);
								$cedulaCatastral = $rowdC[0];
							/* } */
							$res=mysqli_query($linkbd,$sqlr);
							//*******************CREANDO EL RECIBO DE CAJA DE PREDIAL ***********************
							while ($row =mysqli_fetch_row($res)){
								/* if($_POST['tipo']=='1'){
									$vig=$row[13];
									$vlrdesc=($row[9]/$_POST['tcuotas']);
								}else{ */
									$vig=$row[1];
									$vlrdesc=$row[10];
								/* } */
								$cuentaAmb = '';
								//*************VIGENCIA ACTUAL *****************
								if($vig==$vigusu){
									$idcomp=$_POST['idcomp'];

									$codigoIng = '';
									if(substr($cedulaCatastral,0,2) == '00'){
										$codigoIng = '03';
									}else{
										$codigoIng = '01';
									}

									$sqlr2="SELECT * FROM tesoingresos_predial_det WHERE codigo='$codigoIng' AND vigencia=(SELECT MAX(vigencia) FROM tesoingresos_predial_det WHERE codigo = '$codigoIng') GROUP BY concepto ORDER BY concepto ASC";
									$res2 = mysqli_query($linkbd,$sqlr2);
									//****** $cuentacb   ES LA CUENTA CAJA O BANCO
									while($rowi = mysqli_fetch_row($res2)){
										switch($rowi[2]){
											//Impuesto Predial Vigente
											case '21': case '20':{
												/* $sqlrds="SELECT * FROM tesoingresos_det WHERE codigo='01' AND concepto='P01' AND modulo='4' AND vigencia=(SELECT MAX(vigencia) FROM tesoingresos_det WHERE codigo = '01')";
												$resds=mysqli_query($linkbd,$sqlrds);
												while($rowds =mysqli_fetch_row($resds)){
													$descpredial=round($vlrdesc*(round($rowds[5]/100,2)),2);
												} */
												$sq="SELECT fechainicial FROM conceptoscontables_det WHERE codigo='$rowi[2]' AND modulo='4' AND tipo='PR' AND fechainicial<'$fechaf' AND cuenta!='' ORDER BY fechainicial ASC";
												$re=mysqli_query($linkbd,$sq);
												while($ro=mysqli_fetch_assoc($re)){
													$_POST['fechacausa']=$ro["fechainicial"];
												}
												$sqlrc="SELECT * FROM conceptoscontables_det WHERE estado='S' AND modulo='4' AND codigo = '$rowi[2]' AND tipo='PR' AND fechainicial='$_POST[fechacausa]'";
												$resc=mysqli_query($linkbd,$sqlrc);
												while($rowc=mysqli_fetch_row($resc)){
													$porce=$rowi[5];
													$centroCostosCeros = str_pad($rowc[5], 2, "0", STR_PAD_LEFT);
													if($rowc[6]=='S'){
														$valorcred=$row[4]+round($vlrdesc,0);

														$valordeb=0;
														if($rowc[3]=='N'){
															if($valorcred>0){
																$sqlr="INSERT INTO comprobante_det (id_comp, cuenta, tercero, centrocosto,detalle, cheque, valdebito, valcredito, estado, vigencia) VALUES ('5 $concecc', '$rowc[4]', '$_POST[tercero]', '$centroCostosCeros', 'Ingreso Impuesto Predial Vigente $vig', '', '".round($valordeb,0)."', '".round($valorcred,0)."', '1', '$_POST[vigencia]')";
																mysqli_query($linkbd,$sqlr);
																//$valordeb=round($valorcred,0)-round($vlrdesc,0);
																$valordeb=round($row[4],0);
																$sqlr="INSERT INTO comprobante_det (id_comp, cuenta, tercero, centrocosto, detalle, cheque, valdebito, valcredito, estado, vigencia) VALUES ('5 $concecc','$cuentacb','$_POST[tercero]','$centroCostosCeros','Ingreso Impuesto Predial Vigente $vig','','".round($valordeb,0)."','0','1','$_POST[vigencia]')";
																mysqli_query($linkbd,$sqlr);
																//******MODIFICAR CUENTA PPTAL DE INGRESO AGREGARLE EL RECAUDO ******
																//****creacion documento presupuesto ingresos
																//************ FIN MODIFICACION PPTAL
															}
														}
													}else{
														$cuentaAmb = $rowc[4];
													}
												}
											}break;
											//Ingreso Sobretasa Ambiental
											case '24': case '25':{
												$sq = "SELECT fechainicial FROM conceptoscontables_det WHERE codigo='$rowi[2]' AND modulo='4' AND tipo='PR' AND fechainicial<'$fechaf' AND cuenta!='' ORDER BY fechainicial ASC";
												$re = mysqli_query($linkbd,$sq);
												while($ro=mysqli_fetch_assoc($re)){
													$_POST['fechacausa']=$ro["fechainicial"];
												}
												$sqlrc="SELECT * FROM conceptoscontables_det WHERE estado='S' AND modulo='4' AND codigo='$rowi[2]' AND tipo='PR' AND fechainicial='".$_POST['fechacausa']."' ORDER BY cuenta DESC";
												$resc = mysqli_query($linkbd, $sqlrc);
												while($rowc=mysqli_fetch_row($resc)){
													$centroCostosCeros = str_pad($rowc[5], 2, "0", STR_PAD_LEFT);
													$porce=$rowi[5];
                                                    $valorcred=$row[8];

                                                    $inicioCuenta = substr($rowc[4], 0, 1);

                                                    if($inicioCuenta == '2'){

                                                        if($rowc[7] == 'S'){

                                                            if($rowc[3]=='N'){

                                                                if($valorcred>0){

                                                                    $sqlr="INSERT INTO comprobante_det (id_comp, cuenta, tercero, centrocosto,detalle, cheque, valdebito, valcredito, estado, vigencia) VALUES ('5 $concecc', '$rowc[4]', '$_POST[tercero]', '$centroCostosCeros', 'Ingreso Sobretasa Ambiental $vig', '', '0', '".round($valorcred,0)."', '1','".$_POST['vigencia']."')";
                                                                    mysqli_query($linkbd, $sqlr);

                                                                    $valordeb = $valorcred;

                                                                    $sqlr="INSERT INTO comprobante_det (id_comp, cuenta, tercero, centrocosto, detalle, cheque, valdebito, valcredito, estado, vigencia) VALUES ('5 $concecc', '$cuentacb', '".$_POST['tercero']."', '$centroCostosCeros', 'Ingreso Sobretasa Ambiental $vig', '', '".round($valordeb,0)."', '0', '1', '".$_POST['vigencia']."')";
                                                                    mysqli_query($linkbd, $sqlr);

                                                                }
                                                            }

                                                        }

                                                    }else{

                                                        if($rowc[7]=='S'){
                                                            if($rowc[3]=='N'){
                                                                if($valorcred>0){

                                                                    $sqlr="INSERT INTO comprobante_det (id_comp, cuenta, tercero, centrocosto,detalle, cheque, valdebito, valcredito, estado, vigencia) VALUES ('5 $concecc', '$rowc[4]', '$_POST[tercero]', '$centroCostosCeros', 'Reconocimiento Sobretasa Ambiental $vig', '', '0', '".round($row[8],0)."', '1','".$_POST['vigencia']."')";
                                                                    mysqli_query($linkbd,$sqlr);
                                                                }
                                                            }
                                                        }
                                                        if($rowc[6]=='S'){

                                                            $valordeb=0;
                                                            if($rowc[3]=='N'){
                                                                if($valorcred>0){
                                                                    $cuentaBanco = '';
                                                                    if($_POST['cobro_ambiental'] == 'S'){
                                                                        $cuentaBanco = $cuentaAmb;
                                                                    }else{
                                                                        $cuentaBanco = $cuentacb;
                                                                    }

                                                                    $sqlr="INSERT INTO comprobante_det (id_comp, cuenta, tercero, centrocosto,detalle, cheque, valdebito, valcredito, estado, vigencia) VALUES ('5 $concecc', '$rowc[4]', '$_POST[tercero]', '$centroCostosCeros', 'Reconocimiento Sobretasa Ambiental $vig', '', '".round($row[8],0)."', '0', '1','".$_POST['vigencia']."')";
                                                                    mysqli_query($linkbd,$sqlr);

                                                                    $sqlr="INSERT INTO comprobante_det (id_comp, cuenta, tercero, centrocosto,detalle, cheque, valdebito, valcredito, estado, vigencia) VALUES ('5 $concecc', '$rowc[4]', '$_POST[tercero]', '$centroCostosCeros', 'Ingreso Sobretasa Ambiental $vig', '', '".round($valordeb,0)."', '".round($valorcred,0)."', '1','".$_POST['vigencia']."')";
                                                                    mysqli_query($linkbd,$sqlr);
                                                                    $valordeb = $valorcred;
                                                                    $sqlr="INSERT INTO comprobante_det (id_comp, cuenta, tercero, centrocosto, detalle, cheque, valdebito, valcredito, estado, vigencia) VALUES ('5 $concecc', '$cuentacb', '".$_POST['tercero']."', '$centroCostosCeros', 'Ingreso Sobretasa Ambiental $vig', '', '".round($valordeb,0)."', '0', '1', '".$_POST['vigencia']."')";
                                                                    mysqli_query($linkbd,$sqlr);

                                                                }
                                                            }
                                                        }
                                                    }
												}
											}break;
											//Ingreso Sobretasa Bomberil
											case '26': case '27':{

												$sq="SELECT fechainicial FROM conceptoscontables_det WHERE codigo='$rowi[2]' AND modulo='4' AND tipo='PR' AND fechainicial<'$fechaf' AND cuenta!='' ORDER BY fechainicial ASC";
												$re=mysqli_query($linkbd,$sq);
												while($ro=mysqli_fetch_assoc($re)){
													$_POST['fechacausa']=$ro["fechainicial"];
												}
												$sqlrc="SELECT * FROM conceptoscontables_det WHERE estado='S' AND modulo='4' AND codigo='$rowi[2]' AND tipo='PR' AND fechainicial='".$_POST['fechacausa']."' ORDER BY cuenta DESC ";
												$resc=mysqli_query($linkbd,$sqlrc);

												while($rowc=mysqli_fetch_row($resc)){
													$centroCostosCeros = str_pad($rowc[5], 2, "0", STR_PAD_LEFT);
													$porce=$rowi[5];
                                                    $valorcred=$row[6];

                                                    if($rowc[7]=='S'){
                                                        if($rowc[3]=='N'){
                                                            if($valorcred>0){

                                                                $sqlr="INSERT INTO comprobante_det (id_comp, cuenta, tercero, centrocosto,detalle, cheque, valdebito, valcredito, estado, vigencia) VALUES ('5 $concecc', '$rowc[4]', '".$_POST['tercero']."', '$centroCostosCeros', 'Reconocimiento Sobretasa Bomberil $vig', '', '0', '".round($row[6],0)."', '1', '".$_POST['vigencia']."')";
																mysqli_query($linkbd,$sqlr);
                                                            }
                                                        }
                                                    }

													if($rowc[6]=='S'){

														$valordeb=0;
														if($rowc[3]=='N'){
															if($valorcred>0){
                                                                $sqlr="INSERT INTO comprobante_det (id_comp, cuenta, tercero, centrocosto,detalle, cheque, valdebito, valcredito, estado, vigencia) VALUES ('5 $concecc', '$rowc[4]', '".$_POST['tercero']."', '$centroCostosCeros', 'Reconocimiento Sobretasa Bomberil $vig', '', '".round($row[6],0)."', '0', '1', '".$_POST['vigencia']."')";
																mysqli_query($linkbd,$sqlr);

																$sqlr="INSERT INTO comprobante_det (id_comp, cuenta, tercero, centrocosto,detalle, cheque, valdebito, valcredito, estado, vigencia) VALUES ('5 $concecc', '$rowc[4]', '".$_POST['tercero']."', '$centroCostosCeros', 'Ingreso Sobretasa Bomberil $vig', '', '".round($valordeb,0)."', '".round($valorcred,0)."', '1', '".$_POST['vigencia']."')";
																mysqli_query($linkbd,$sqlr);
																$valordeb=$valorcred;
																$sqlr="INSERT INTO comprobante_det (id_comp,cuenta,tercero,centrocosto, detalle, cheque, valdebito, valcredito, estado, vigencia) VALUES ('5 $concecc', '$cuentacb', '".$_POST['tercero']."', '$centroCostosCeros', 'Ingreso Sobretasa Bomberil $vig', '', '".round($valordeb,0)."', '0', '1', '".$_POST['vigencia']."')";
																mysqli_query($linkbd,$sqlr);

															}
														}
													}
												}
											}break;

											//Descuento Pronto Pago Predial
											case '31':{
												$sq="SELECT fechainicial FROM conceptoscontables_det WHERE codigo='$rowi[2]' AND modulo='4' AND tipo='PR' AND fechainicial<'$fechaf' AND cuenta!='' ORDER BY fechainicial ASC";
												$re=mysqli_query($linkbd,$sq);
												while($ro=mysqli_fetch_assoc($re)){$_POST['fechacausa']=$ro["fechainicial"];}
												$sqlrc="SELECT * FROM conceptoscontables_det WHERE estado='S' AND modulo='4' AND codigo = '$rowi[2]' AND tipo='PR' AND fechainicial='".$_POST['fechacausa']."'";
												$resc=mysqli_query($linkbd,$sqlrc);
												while($rowc=mysqli_fetch_row($resc)){

													$centroCostosCeros = str_pad($rowc[5], 2, "0", STR_PAD_LEFT);

													if($rowc[7]=='S'){

														$valordeb=round($row[10],0);

														$valorcred=0;
														if($rowc[3]=='N'){
															if($valordeb>0){
																$sqlr="INSERT INTO comprobante_det (id_comp,cuenta,tercero,centrocosto,detalle, cheque,valdebito,valcredito,estado,vigencia) VALUES ('5 $concecc', '$rowc[4]', '".$_POST['tercero']."', '$centroCostosCeros', 'Descuento Pronto Pago Predial $vig', '', '".round($valordeb,0)."', '".round($valorcred,0)."','1','$_POST[vigencia]')";
																mysqli_query($linkbd,$sqlr);
															}
														}
													}
												}
											}break;
											//Intereses Predial
											case '28':{
												$sq="SELECT fechainicial FROM conceptoscontables_det WHERE codigo='$rowi[2]' AND modulo='4' AND tipo='PR' AND fechainicial<'$fechaf' AND cuenta!='' ORDER BY fechainicial ASC";
												$re = mysqli_query($linkbd,$sq);
												while($ro = mysqli_fetch_assoc($re)){
													$_POST['fechacausa']=$ro["fechainicial"];
												}
												$sqlrc="SELECT * FROM conceptoscontables_det WHERE estado='S' AND modulo='4' AND codigo = '$rowi[2]' AND tipo='PR' AND fechainicial='".$_POST['fechacausa']."' ORDER BY cuenta DESC";
												$resc = mysqli_query($linkbd,$sqlrc);
												while($rowc = mysqli_fetch_row($resc)){
													$centroCostosCeros = str_pad($rowc[5], 2, "0", STR_PAD_LEFT);
													$porce=$rowi[5];

                                                    $sqlrDescIntPredial = "SELECT descuentointpredial FROM tesoliquidapredial_desc WHERE id_predial='".$_POST['idrecaudo']."' AND vigencia = '$vig'";
													$rescDescIntPredial = mysqli_query($linkbd, $sqlrDescIntPredial);
													$rowcDescIntPredial = mysqli_fetch_row($rescDescIntPredial);

                                                    $valorcred = $row[5]-$rowcDescIntPredial[0];

                                                    switch ($rowc[7]) {
                                                        case 'S':

                                                            if ($valorcred > 0) {
                                                                $sqlr="INSERT INTO comprobante_det (id_comp, cuenta, tercero, centrocosto, detalle, cheque, valdebito, valcredito, estado, vigencia) VALUES ('5 $concecc', '$rowc[4]','".$_POST['tercero']."', '$centroCostosCeros','Reconocimiento de Intereses Predial $vig','','0','".round($valorcred,0)."','1','".$_POST['vigencia']."')";
                                                                mysqli_query($linkbd, $sqlr);
                                                            }

                                                            break;
                                                        default:

                                                            $valordeb = 0;
                                                            if ($rowc[3] == 'N') {
                                                                if ($valorcred > 0) {
                                                                    $sqlr="INSERT INTO comprobante_det (id_comp, cuenta, tercero, centrocosto, detalle, cheque, valdebito, valcredito, estado, vigencia) VALUES ('5 $concecc', '$rowc[4]','".$_POST['tercero']."', '$centroCostosCeros','Reconocimiento de Intereses Predial $vig','','".round($valorcred,0)."','0','1','".$_POST['vigencia']."')";
                                                                    mysqli_query($linkbd, $sqlr);

                                                                    $sqlr="INSERT INTO comprobante_det (id_comp, cuenta, tercero, centrocosto, detalle, cheque, valdebito, valcredito, estado, vigencia) VALUES ('5 $concecc', '$rowc[4]', '".$_POST['tercero']."', '$centroCostosCeros', 'Intereses Predial $vig', '', '".round($valordeb,0)."', '".round($valorcred,0)."', '1', '".$_POST['vigencia']."')";
                                                                    mysqli_query($linkbd, $sqlr);
                                                                    $valordeb = $valorcred;
                                                                    $sqlr="INSERT INTO comprobante_det (id_comp, cuenta, tercero, centrocosto, detalle, cheque, valdebito, valcredito, estado, vigencia) VALUES ('5 $concecc','$cuentacb','".$_POST['tercero']."','$centroCostosCeros','Intereses Predial $vig','','".round($valordeb,2)."', '0','1','".$_POST['vigencia']."')";
                                                                    mysqli_query($linkbd, $sqlr);

                                                                }
                                                            }
                                                            break;
                                                    }

												}
											}break;
                                            //Intereses Sobtretasa Ambiental
											case '29':{

												$sq="SELECT fechainicial FROM conceptoscontables_det WHERE codigo='$rowi[2]' AND modulo='4' AND tipo='PR' AND fechainicial<'$fechaf' AND cuenta!='' ORDER BY fechainicial ASC";
												$re=mysqli_query($linkbd, $sq);
												while($ro=mysqli_fetch_assoc($re)){$_POST['fechacausa']=$ro["fechainicial"];}
												$sqlrc="SELECT * from conceptoscontables_det where estado = 'S' and modulo = '4' AND codigo = '$rowi[2]' AND tipo = 'PR' AND fechainicial = '".$_POST['fechacausa']."' ORDER BY cuenta DESC ";
												$resc = mysqli_query($linkbd,$sqlrc);
												while($rowc = mysqli_fetch_row($resc)){

													$centroCostosCeros = str_pad($rowc[5], 2, "0", STR_PAD_LEFT);

                                                    $sqlrDescIntAmbiental = "SELECT descuentointambiental FROM tesoliquidapredial_desc WHERE id_predial='".$_POST['idrecaudo']."' AND vigencia = '$vig'";
													$rescDescIntAmbiental = mysqli_query($linkbd, $sqlrDescIntAmbiental);
													$rowcDescIntAmbiental = mysqli_fetch_row($rescDescIntAmbiental);

                                                    $valorcred=$row[9]-$rowcDescIntAmbiental[0];

                                                    $inicioCuenta = substr($rowc[4], 0, 1);

                                                    if($inicioCuenta == '2'){

                                                        if($rowc[7] == 'S'){

                                                            if($rowc[3]=='N'){

                                                                if($valorcred>0){

                                                                    $sqlr="INSERT INTO comprobante_det (id_comp,cuenta,tercero,centrocosto,detalle, cheque,valdebito,valcredito,estado,vigencia) VALUES ('5 $concecc','$rowc[4]','".$_POST['tercero']."', '$centroCostosCeros','Intereses Sobtretasa Ambiental $vig','','0','".round($valorcred,0)."','1','".$_POST['vigencia']."')";
                                                                    mysqli_query($linkbd,$sqlr);

                                                                    $valordeb=$valorcred;

                                                                    $sqlr="INSERT INTO comprobante_det (id_comp, cuenta, tercero, centrocosto, detalle, cheque, valdebito, valcredito, estado, vigencia) VALUES ('5 $concecc', '$cuentacb','".$_POST['tercero']."', '$centroCostosCeros','Intereses Sobtretasa Ambiental $vig','','".round($valordeb,0)."','0','1','".$_POST['vigencia']."')";
                                                                    mysqli_query($linkbd,$sqlr);

                                                                }
                                                            }

                                                        }

                                                    }else{

                                                        switch ($rowc[7]) {
                                                            case 'S':

                                                                if ($valorcred > 0) {

                                                                    $sqlr="INSERT INTO comprobante_det (id_comp,cuenta,tercero,centrocosto,detalle, cheque,valdebito,valcredito,estado,vigencia) VALUES ('5 $concecc','$rowc[4]','".$_POST['tercero']."', '$centroCostosCeros','Reconocimiento de Intereses Sobtretasa Ambiental $vig','','0','".round($valorcred,0)."','1','".$_POST['vigencia']."')";
                                                                    mysqli_query($linkbd, $sqlr);

                                                                }

                                                                break;
                                                            default:

                                                                $valordeb = 0;
                                                                if ($rowc[3] == 'N') {
                                                                    if ($valorcred > 0) {

                                                                        $sqlr="INSERT INTO comprobante_det (id_comp,cuenta,tercero,centrocosto,detalle, cheque,valdebito,valcredito,estado,vigencia) VALUES ('5 $concecc','$rowc[4]','".$_POST['tercero']."', '$centroCostosCeros','Reconocimiento de Intereses Sobtretasa Ambiental $vig','','".round($valorcred,0)."','0','1','".$_POST['vigencia']."')";
                                                                        mysqli_query($linkbd, $sqlr);

                                                                        $sqlr="INSERT INTO comprobante_det (id_comp,cuenta,tercero,centrocosto,detalle, cheque,valdebito,valcredito,estado,vigencia) VALUES ('5 $concecc','$rowc[4]','".$_POST['tercero']."', '$centroCostosCeros','Intereses Sobtretasa Ambiental $vig','','".round($valordeb,0)."','".round($valorcred,0)."','1','".$_POST['vigencia']."')";
                                                                        mysqli_query($linkbd,$sqlr);
                                                                        $valordeb=$valorcred;
                                                                        $sqlr="INSERT INTO comprobante_det (id_comp, cuenta, tercero, centrocosto, detalle, cheque, valdebito, valcredito, estado, vigencia) VALUES ('5 $concecc', '$cuentacb','".$_POST['tercero']."', '$centroCostosCeros','Intereses Sobtretasa Ambiental $vig','','".round($valordeb,0)."','0','1','".$_POST['vigencia']."')";
                                                                        mysqli_query($linkbd,$sqlr);

                                                                    }
                                                                }
                                                                break;
                                                        }
                                                    }

												}
											}break;

											//Intereses Sobretasa Bomberil
											case '30':{
												$sq="SELECT fechainicial FROM conceptoscontables_det WHERE codigo='$rowi[2]' AND modulo='4' AND tipo='PR' AND fechainicial<'$fechaf' AND cuenta!='' ORDER BY fechainicial ASC";
												$re=mysqli_query($linkbd,$sq);
												while($ro=mysqli_fetch_assoc($re)){
													$_POST['fechacausa']=$ro["fechainicial"];
												}
												$sqlrc="SELECT * FROM conceptoscontables_det WHERE estado='S' AND modulo='4' AND codigo='$rowi[2]' AND tipo='PR' AND fechainicial = '".$_POST['fechacausa']."' ORDER BY cuenta DESC";
												$resc=mysqli_query($linkbd, $sqlrc);
												while($rowc=mysqli_fetch_row($resc)){
													$centroCostosCeros = str_pad($rowc[5], 2, "0", STR_PAD_LEFT);
													$porce=$rowi[5];

                                                    $sqlrDescIntBomberil = "SELECT descuentointbomberil FROM tesoliquidapredial_desc WHERE id_predial='".$_POST['idrecaudo']."' AND vigencia = '$vig'";
													$rescDescIntBomberil = mysqli_query($linkbd, $sqlrDescIntBomberil);
													$rowcDescIntBomberil = mysqli_fetch_row($rescDescIntBomberil);

                                                    $valorcred=$row[7]-$rowcDescIntBomberil[0];

                                                    switch($rowc[7]){
                                                        case 'S':
                                                            if ($valorcred > 0) {

                                                                $sqlr="INSERT INTO comprobante_det (id_comp,cuenta,tercero,centrocosto,detalle, cheque,valdebito,valcredito,estado,vigencia) VALUES ('5 $concecc','$rowc[4]','".$_POST['tercero']."', '$centroCostosCeros','Reconocimiento de Intereses Sobretasa Bomberil $vig','','0','".round($valorcred,0)."','1','".$_POST['vigencia']."')";
                                                                mysqli_query($linkbd, $sqlr);

                                                            }
                                                            break;
                                                        default:

                                                            if($valorcred>0){
																$sqlr="INSERT INTO comprobante_det (id_comp, cuenta, tercero, centrocosto,detalle, cheque, valdebito, valcredito, estado, vigencia) VALUES ('5 $concecc', '$rowc[4]', '".$_POST['tercero']."', '$centroCostosCeros', 'Reconocimiento de Intereses Sobretasa Bomberil $vig', '', '".round($valorcred,0)."', '0', '1', '".$_POST['vigencia']."')";
																mysqli_query($linkbd, $sqlr);

																$sqlr="INSERT INTO comprobante_det (id_comp, cuenta, tercero, centrocosto, detalle, cheque, valdebito, valcredito, estado, vigencia) VALUES ('5 $concecc', '$rowc[4]','".$_POST['tercero']."', '$centroCostosCeros','Intereses Sobretasa Bomberil $vig','','0','".round($valorcred,0)."','1','".$_POST['vigencia']."')";
																mysqli_query($linkbd,$sqlr);

                                                                $valordeb=$valorcred;

                                                                $sqlr="INSERT INTO comprobante_det (id_comp, cuenta, tercero, centrocosto, detalle, cheque, valdebito, valcredito, estado, vigencia) VALUES ('5 $concecc', '$cuentacb', '".$_POST['tercero']."', '$centroCostosCeros', 'Intereses Sobretasa Bomberil $vig', '', '".round($valordeb,0)."', '0', '1', '".$_POST['vigencia']."')";
																mysqli_query($linkbd,$sqlr);

															}

                                                            break;
                                                    }
												}
											}break;

											case '34':{

												$sqlca="SELECT valor_inicial,valor_final,tipo FROM dominios WHERE nombre_dominio='COBRO_ALUMBRADO' AND tipo='S'";
												$resca=mysqli_query($linkbd,$sqlca);
												while ($rowca =mysqli_fetch_row($resca)){
													$cobroalumbrado=$rowca[0];
													$vcobroalumbrado=$rowca[1];
													$tcobroalumbrado=$rowca[2];
												}
												if($tcobroalumbrado=='S' && substr($cedulaCatastral,0,2) == '00'){
													$rowcAlumbrado = 0;
													//$valorAlumbrado=round($row[2]*($vcobroalumbrado/1000),0);
													$sqlrAlumbrado = "SELECT val_alumbrado FROM tesoliquidapredial_desc WHERE id_predial='".$_POST['idrecaudo']."' AND vigencia = '$vig'";
													$rescAlumbrado = mysqli_query($linkbd,$sqlrAlumbrado);
													$rowcAlumbrado = mysqli_fetch_row($rescAlumbrado);
													$valorAlumbrado = $rowcAlumbrado[0];
													$sq="SELECT fechainicial FROM conceptoscontables_det WHERE codigo='$rowi[2]' AND modulo='4' AND tipo='PR' AND fechainicial<'$fechaf' AND cuenta!='' ORDER BY fechainicial ASC";
													$re=mysqli_query($linkbd,$sq);
													while($ro=mysqli_fetch_assoc($re)){$_POST['fechacausa']=$ro["fechainicial"];}
													$sqlrc="SELECT * FROM conceptoscontables_det WHERE estado='S' AND modulo='4' AND codigo='$rowi[2]' AND tipo='PR' AND fechainicial='".$_POST['fechacausa']."' ORDER BY cuenta DESC";
													$resc=mysqli_query($linkbd,$sqlrc);
													while($rowc=mysqli_fetch_row($resc)){
														$centroCostosCeros = str_pad($rowc[5], 2, "0", STR_PAD_LEFT);
														$porce=$rowi[5];
                                                        $valorcred=$valorAlumbrado;

                                                        if($rowc[7]=='S'){
                                                            if($rowc[3]=='N'){
                                                                if($valorcred>0){

                                                                    $sqlr="INSERT INTO comprobante_det (id_comp,cuenta,tercero, centrocosto,detalle,cheque,valdebito,valcredito,estado,vigencia) VALUES ('5 $concecc','$rowc[4]','".$_POST['tercero']."','$centroCostosCeros', 'Reconocimiento Impuesto sobre el Servicio de Alumbrado Público $vig','', '0','".round($valorAlumbrado,0)."','1','".$_POST['vigencia']."')";
																	mysqli_query($linkbd,$sqlr);
                                                                }
                                                            }
                                                        }

														if($rowc[6]=='S'){

															$valordeb=0;
															if($rowc[3]=='N'){
																if($valorcred>0){

                                                                    $sqlr="INSERT INTO comprobante_det (id_comp,cuenta,tercero, centrocosto,detalle,cheque,valdebito,valcredito,estado,vigencia) VALUES ('5 $concecc','$rowc[4]','".$_POST['tercero']."','$centroCostosCeros', 'Reconocimiento Impuesto sobre el Servicio de Alumbrado Público $vig','', '".round($valorAlumbrado,0)."','0','1','".$_POST['vigencia']."')";
																	mysqli_query($linkbd,$sqlr);

																	$sqlr="INSERT INTO comprobante_det (id_comp,cuenta,tercero, centrocosto,detalle,cheque,valdebito,valcredito,estado,vigencia) VALUES ('5 $concecc','$rowc[4]','".$_POST['tercero']."','$centroCostosCeros', 'Ingreso Impuesto sobre el Servicio de Alumbrado Público $vig','', '".round($valordeb,0)."','".round($valorcred,0)."','1','".$_POST['vigencia']."')";
																	mysqli_query($linkbd,$sqlr);
																	$valordeb=$valorcred;
																	$sqlr="INSERT INTO comprobante_det (id_comp, cuenta,tercero, centrocosto,detalle,cheque,valdebito,valcredito,estado,vigencia) VALUES ('5 $concecc', '$cuentacb','".$_POST['tercero']."', '$centroCostosCeros', 'Ingreso Impuesto sobre el Servicio de Alumbrado Público $vig','', '".round($valordeb,0)."','0','1','".$_POST['vigencia']."')";
																	mysqli_query($linkbd,$sqlr);

																}
															}
														}
													}
												}
											}break;
										}
									}
									$_POST['dcoding'][]=$row2[0];
									$_POST['dncoding'][]=$row2[1]." ".$vig;
									$_POST['dvalores'][]=$row[11];
								}else{
								///***********OTRAS VIGENCIAS ***********
									$tasadesc=$row[10];
									$idcomp=$_POST['idcomp'];

									$codigoIng = '';
									if(substr($cedulaCatastral,0,2) == '00'){
										$codigoIng = '04';
									}else{
										$codigoIng = '02';
									}

									$sqlr2="SELECT * FROM tesoingresos_predial_det WHERE codigo='$codigoIng' AND vigencia=(SELECT MAX(vigencia) FROM tesoingresos_predial_det WHERE codigo = '$codigoIng') GROUP BY concepto ORDER BY concepto ASC";

									$sqlr="UPDATE tesoreciboscaja SET id_comp=$idcomp WHERE id_recaudo=".$_POST['idrecaudo']." AND tipo='1'";
									mysqli_query($linkbd,$sqlr);

									/* $sqlr2="SELECT * FROM tesoingresos_det WHERE codigo='03' AND MODULO='4' AND vigencia=(SELECT MAX(vigencia) FROM tesoingresos_det WHERE codigo = '03') GROUP BY concepto"; */
									$res2=mysqli_query($linkbd,$sqlr2);
									//****** $cuentacb   ES LA CUENTA CAJA O BANCO
									while($rowi =mysqli_fetch_row($res2)){
										switch($rowi[2]){
											//Ingreso Impuesto Predial Otras Vigencias
											case '22': case '23': {
												$sq="select fechainicial from conceptoscontables_det where codigo='".$rowi[2]."' and modulo='4' and tipo='PR' and fechainicial<'$fechaf' and cuenta!='' order by fechainicial asc";
												$re=mysqli_query($linkbd,$sq);
												while($ro=mysqli_fetch_assoc($re)){$_POST['fechacausa']=$ro["fechainicial"];}
												$sqlrc="Select * from conceptoscontables_det where estado='S' and modulo='4' AND codigo='$rowi[2]' and tipo='PR' and fechainicial='$_POST[fechacausa]'";
												$resc=mysqli_query($linkbd,$sqlrc);
												while($rowc=mysqli_fetch_row($resc)){
													$centroCostosCeros = str_pad($rowc[5], 2, "0", STR_PAD_LEFT);
													if($rowc[6]=='S'){
														$valorcred=$row[4]+$tasadesc;
														$valordeb=0;
														if($rowc[3]=='N'){
															if($valorcred>0){
																$sqlr="insert into comprobante_det (id_comp,cuenta,tercero,centrocosto,detalle, cheque,valdebito,valcredito,estado,vigencia) values ('5 $concecc','$rowc[4]','$_POST[tercero]','$centroCostosCeros','Ingreso Impuesto Predial Otras Vigencias $vig','','".round($valordeb,0)."','".round($valorcred,0)."','1','$_POST[vigencia]')";
																mysqli_query($linkbd,$sqlr);
																$valordeb=$row[4];
																$sqlr="insert into comprobante_det (id_comp,cuenta,tercero,centrocosto,detalle, cheque,valdebito,valcredito,estado,vigencia) values ('5 $concecc','$cuentacb','$_POST[tercero]','$centroCostosCeros','Ingreso Impuesto Predial Otras Vigencias $vig','','".round($valordeb,0)."',0,'1','$_POST[vigencia]')";
																mysqli_query($linkbd,$sqlr);
																/* $sq="select fechainicial from conceptoscontables_det where codigo='P01' and modulo='4' and tipo='PR' and fechainicial<'$fechaf' and cuenta!='' order by fechainicial asc";
																$re=mysqli_query($linkbd,$sq);
																while($ro=mysqli_fetch_assoc($re)){$_POST['fechacausa']=$ro["fechainicial"];}
																$sqlrc="Select * from conceptoscontables_det where estado='S' and modulo='4' AND codigo = 'P01' and tipo='PR' and fechainicial='$_POST[fechacausa]'";
																$resc_1=mysqli_query($linkbd,$sqlrc);
																while($rowc_1=mysqli_fetch_row($resc_1))
																{
																	$sqlrdes="insert into comprobante_det (id_comp,cuenta,tercero,centrocosto, detalle,cheque,valdebito,valcredito,estado,vigencia) values ('5 $concecc','$rowc_1[4]','$_POST[tercero]','$rowc_1[5]','Descuento Pago Predial $vig','', '".round($desc,0)."','0','1','$_POST[vigencia]')";
																	mysqli_query($linkbd,$sqlrdes);
																} */

															}
														}
													}else{
														$cuentaAmb = $rowc[4];
													}
												}
											}break;
											//Ingreso Sobretasa Ambiental
											case '24': case '25': {
												$sq="select fechainicial from conceptoscontables_det where codigo='$rowi[2]' and modulo='4' and tipo='PR' and fechainicial<'$fechaf' and cuenta!='' order by fechainicial asc";
												$re=mysqli_query($linkbd,$sq);
												while($ro=mysqli_fetch_assoc($re)){$_POST['fechacausa']=$ro["fechainicial"];}
												$sqlrc="Select * from conceptoscontables_det where estado='S' and modulo='4' AND codigo='$rowi[2]' and tipo='PR' and fechainicial='".$_POST['fechacausa']."' ORDER BY cuenta DESC";
												$resc=mysqli_query($linkbd,$sqlrc);
												while($rowc=mysqli_fetch_row($resc)){

													$centroCostosCeros = str_pad($rowc[5], 2, "0", STR_PAD_LEFT);
													$porce=$rowi[5];

                                                    $valordeb = 0;
                                                    $valorcred=$row[8];

                                                    $inicioCuenta = substr($rowc[4], 0, 1);

                                                    if($inicioCuenta == '2'){

                                                        if($rowc[7] == 'S'){

                                                            if($rowc[3]=='N'){

                                                                if($valorcred>0){

                                                                    $sqlr="insert into comprobante_det (id_comp,cuenta,tercero,centrocosto,detalle, cheque,valdebito,valcredito,estado,vigencia) values ('5 $concecc','$rowc[4]','$_POST[tercero]','$centroCostosCeros','Ingreso Sobretasa Ambiental $vig','', '0','".round($valorcred,0)."','1','$_POST[vigencia]')";
                                                                    mysqli_query($linkbd, $sqlr);

                                                                    $valordeb=$valorcred;

                                                                    $sqlr="insert into comprobante_det (id_comp,cuenta,tercero,centrocosto,detalle, cheque,valdebito,valcredito,estado,vigencia) values ('5 $concecc','$cuentacb','$_POST[tercero]','$centroCostosCeros','Ingreso Sobretasa Ambiental $vig','', '".round($valordeb,0)."',0,'1','$_POST[vigencia]')";
                                                                    mysqli_query($linkbd, $sqlr);

                                                                }
                                                            }

                                                        }


                                                    }else{

                                                        switch ($rowc[7]){
                                                            case 'S':

                                                                if($valorcred>0){

                                                                    $sqlr="insert into comprobante_det (id_comp,cuenta,tercero,centrocosto,detalle, cheque,valdebito,valcredito,estado,vigencia) values ('5 $concecc','$rowc[4]','$_POST[tercero]','$centroCostosCeros','Reconocimiento de Ingreso Sobretasa Ambiental $vig','', '0','".round($valorcred,0)."','1','$_POST[vigencia]')";
                                                                    mysqli_query($linkbd, $sqlr);

                                                                }

                                                                break;

                                                            default:

                                                                if($valorcred>0){

                                                                    $sqlr="insert into comprobante_det (id_comp,cuenta,tercero,centrocosto,detalle, cheque,valdebito,valcredito,estado,vigencia) values ('5 $concecc','$rowc[4]','$_POST[tercero]','$centroCostosCeros','Reconocimiento de Ingreso Sobretasa Ambiental $vig','', '".round($valorcred,0)."','0','1','$_POST[vigencia]')";
                                                                    mysqli_query($linkbd, $sqlr);

                                                                    $sqlr="insert into comprobante_det (id_comp,cuenta,tercero,centrocosto,detalle, cheque,valdebito,valcredito,estado,vigencia) values ('5 $concecc','$rowc[4]','$_POST[tercero]','$centroCostosCeros','Ingreso Sobretasa Ambiental $vig','', '".round($valordeb,0)."','".round($valorcred,0)."','1','$_POST[vigencia]')";
                                                                    mysqli_query($linkbd, $sqlr);

                                                                    $valordeb=$valorcred;

                                                                    $sqlr="insert into comprobante_det (id_comp,cuenta,tercero,centrocosto,detalle, cheque,valdebito,valcredito,estado,vigencia) values ('5 $concecc','$cuentacb','$_POST[tercero]','$centroCostosCeros','Ingreso Sobretasa Ambiental $vig','', '".round($valordeb,0)."',0,'1','$_POST[vigencia]')";
                                                                    mysqli_query($linkbd, $sqlr);

                                                                }

                                                            break;
                                                        }
                                                    }
												}
											}break;
											//Ingreso Sobretasa Bomberil
											case '26': case '27': {
												$sq="select fechainicial from conceptoscontables_det where codigo='$rowi[2]' and modulo='4' and tipo='PR' and fechainicial<'$fechaf' and cuenta!='' order by fechainicial asc";
												$re=mysqli_query($linkbd,$sq);
												while($ro=mysqli_fetch_assoc($re)){$_POST['fechacausa']=$ro["fechainicial"];}
												$sqlrc="Select * from conceptoscontables_det where estado='S' and modulo='4' AND codigo='$rowi[2]' and tipo='PR' and fechainicial='$_POST[fechacausa]' ORDER BY cuenta DESC";
												$resc=mysqli_query($linkbd,$sqlrc);
												while($rowc=mysqli_fetch_row($resc)){

													$centroCostosCeros = str_pad($rowc[5], 2, "0", STR_PAD_LEFT);
													$porce=$rowi[5];

                                                    $valordeb=0;
                                                    $valorcred=$row[6];

                                                    switch ($rowc[7]){
                                                        case 'S':

                                                            if($valorcred>0){

                                                                $sqlr="insert into comprobante_det (id_comp,cuenta,tercero,centrocosto,detalle, cheque,valdebito,valcredito,estado,vigencia) values ('5 $concecc','$rowc[4]','$_POST[tercero]','$centroCostosCeros','Reconocimiento de Ingreso Sobretasa Bomberil $vig','', '0','".round($valorcred,0)."','1','$_POST[vigencia]')";
																mysqli_query($linkbd, $sqlr);

                                                            }

                                                            break;

                                                        default:

                                                            if($valorcred>0){

                                                                $sqlr="insert into comprobante_det (id_comp,cuenta,tercero,centrocosto,detalle, cheque,valdebito,valcredito,estado,vigencia) values ('5 $concecc','$rowc[4]','$_POST[tercero]','$centroCostosCeros','Reconocimiento de Ingreso Sobretasa Bomberil $vig','', '".round($valorcred,0)."','0','1','$_POST[vigencia]')";
																mysqli_query($linkbd, $sqlr);

                                                                $sqlr="insert into comprobante_det (id_comp,cuenta,tercero,centrocosto,detalle, cheque,valdebito,valcredito,estado,vigencia) values ('5 $concecc','$rowc[4]','$_POST[tercero]','$centroCostosCeros','Ingreso Sobretasa Bomberil $vig','', '".round($valordeb,0)."','".round($valorcred,0)."','1','$_POST[vigencia]')";
																mysqli_query($linkbd, $sqlr);

																$valordeb=$valorcred;

																$sqlr="insert into comprobante_det (id_comp,cuenta,tercero,centrocosto,detalle, cheque,valdebito,valcredito,estado,vigencia) values ('5 $concecc','$cuentacb','$_POST[tercero]','$centroCostosCeros','Ingreso Sobretasa Bomberil $vig','','".round($valordeb,0)."',0,'1','$_POST[vigencia]')";
																mysqli_query($linkbd, $sqlr);

                                                            }

                                                            break;
                                                    }

												}
											}break;
											//Descuento Pronto Pago
											case '31':{

												$sq="select fechainicial from conceptoscontables_det where codigo='$rowi[2]' and modulo='4' and tipo='PR' and fechainicial<'$fechaf' and cuenta!='' order by fechainicial asc";
												$re=mysqli_query($linkbd,$sq);
												while($ro=mysqli_fetch_assoc($re)){$_POST['fechacausa']=$ro["fechainicial"];}
												$sqlrc="Select * from conceptoscontables_det where estado='S' and modulo='4' AND codigo='$rowi[2]' and tipo='PR' and fechainicial='$_POST[fechacausa]'";
												$resc=mysqli_query($linkbd,$sqlrc);
												while($rowc=mysqli_fetch_row($resc)){
													$centroCostosCeros = str_pad($rowc[5], 2, "0", STR_PAD_LEFT);
													$porce=$rowi[5];
													if($rowc[7]=='S'){
														$valordeb=$row[10];
														$valorcred=0;
														if($rowc[3]=='N'){
															if($valordeb>0){
																$sqlr="insert into comprobante_det (id_comp,cuenta,tercero,centrocosto,detalle, cheque,valdebito,valcredito,estado,vigencia) values ('5 $concecc','$rowc[4]','$_POST[tercero]','$centroCostosCeros','Descuento Pronto Pago $vig','', '".round($valordeb,0)."','".round($valorcred,0)."','1','$_POST[vigencia]')";
																mysqli_query($linkbd,$sqlr);
															}
														}
													}
												}

											}break;
											//Intereses Predial
											case '28':{

												$sq="select fechainicial from conceptoscontables_det where codigo='".$rowi[2]."' and modulo='4' and tipo='PR' and fechainicial<'$fechaf' and cuenta!='' order by fechainicial asc";
												$re=mysqli_query($linkbd,$sq);
												while($ro=mysqli_fetch_assoc($re)){$_POST['fechacausa']=$ro["fechainicial"];}
												$sqlrc="Select * from conceptoscontables_det where estado='S' and modulo='4' AND codigo='".$rowi[2]."' and tipo='PR' and fechainicial='".$_POST['fechacausa']."' ORDER BY cuenta DESC";
												$resc=mysqli_query($linkbd,$sqlrc);
												while($rowc=mysqli_fetch_row($resc))
												{
													$centroCostosCeros = str_pad($rowc[5], 2, "0", STR_PAD_LEFT);

                                                    $sqlrDescIntPredial = "SELECT descuentointpredial FROM tesoliquidapredial_desc WHERE id_predial='".$_POST['idrecaudo']."' AND vigencia = '$vig'";
													$rescDescIntPredial = mysqli_query($linkbd, $sqlrDescIntPredial);
													$rowcDescIntPredial = mysqli_fetch_row($rescDescIntPredial);

													$porce=$rowi[5];
													$valdescuento=0;

                                                    $valorcred=$row[5]-$rowcDescIntPredial[0];
                                                    $valdebito = 0;

                                                    switch ($rowc[7]){

                                                        case 'S':

                                                            if($valorcred>0){

                                                                $sqlr="insert into comprobante_det (id_comp, cuenta, tercero, centrocosto, detalle, cheque, valdebito, valcredito, estado, vigencia) values ('5 $concecc', '$rowc[4]','$_POST[tercero]', '$centroCostosCeros','Reconocimiento de Intereses Predial $vig','','0','".round($valorcred,0)."','1','$_POST[vigencia]')";
														        mysqli_query($linkbd,$sqlr);

                                                            }

                                                            break;

                                                        default:

                                                            if($valorcred>0){

                                                                $sqlr = "insert into comprobante_det (id_comp,cuenta,tercero,centrocosto,detalle, cheque,valdebito,valcredito,estado,vigencia) values ('5 $concecc','$rowc[4]','$_POST[tercero]','$centroCostosCeros','Reconocimiento de Intereses Predial $vig','','".round($valorcred,0)."', '0','1','$_POST[vigencia]')";
																mysqli_query($linkbd, $sqlr);


																$sqlr = "insert into comprobante_det (id_comp, cuenta, tercero, centrocosto, detalle, cheque, valdebito, valcredito, estado, vigencia) values ('5 $concecc', '$rowc[4]','$_POST[tercero]', '$centroCostosCeros','Intereses Predial $vig','','0','".round($valorcred,0)."','1','$_POST[vigencia]')";
																mysqli_query($linkbd, $sqlr);

                                                                $valordeb = $valorcred;
																$sqlr = "insert into comprobante_det (id_comp,cuenta,tercero,centrocosto,detalle, cheque,valdebito,valcredito,estado,vigencia) values ('5 $concecc','$cuentacb','$_POST[tercero]','$centroCostosCeros','Intereses Predial $vig','','".round($valordeb,0)."', 0,'1','$_POST[vigencia]')";
																mysqli_query($linkbd, $sqlr);

                                                            }

                                                        break;
                                                    }
												}

											}
                                            break;

											case '29': //Intereses Sobtretasa Ambiental
											{
												$sq="select fechainicial from conceptoscontables_det where codigo='$rowi[2]' and modulo='4' and tipo='PR' and fechainicial<'$fechaf' and cuenta!='' order by fechainicial asc";
												$re = mysqli_query($linkbd,$sq);
												while($ro = mysqli_fetch_assoc($re)){$_POST['fechacausa']=$ro["fechainicial"];}
												$sqlrc="Select * from conceptoscontables_det where estado='S' and modulo='4' AND codigo='".$rowi[2]."' and tipo='PR' and fechainicial='".$_POST['fechacausa']."' ORDER BY cuenta DESC ";
												$resc = mysqli_query($linkbd,$sqlrc);
												while($rowc = mysqli_fetch_row($resc))
												{
													$centroCostosCeros = str_pad($rowc[5], 2, "0", STR_PAD_LEFT);

                                                    $sqlrDescIntAmbiental = "SELECT descuentointambiental FROM tesoliquidapredial_desc WHERE id_predial='".$_POST['idrecaudo']."' AND vigencia = '$vig'";
													$rescDescIntAmbiental = mysqli_query($linkbd, $sqlrDescIntAmbiental);
													$rowcDescIntAmbiental = mysqli_fetch_row($rescDescIntAmbiental);

													$porce=$rowi[5];

                                                    $valorcred=$row[9] - $rowcDescIntAmbiental[0];
													$valordeb=0;

                                                    $inicioCuenta = substr($rowc[4], 0, 1);

                                                    if($inicioCuenta == '2'){

                                                        if($rowc[7] == 'S'){

                                                            if($rowc[3]=='N'){

                                                                if($valorcred>0){

                                                                    $sqlr="insert into comprobante_det (id_comp,cuenta,tercero,centrocosto,detalle, cheque,valdebito,valcredito,estado,vigencia) values ('5 $concecc','$rowc[4]','$_POST[tercero]','$centroCostosCeros','Intereses Sobtretasa Ambiental $vig','', '0','".round($valorcred,0)."','1','$_POST[vigencia]')";
                                                                    mysqli_query($linkbd, $sqlr);

                                                                    $valordeb=$valorcred;

                                                                    $sqlr="insert into comprobante_det (id_comp, cuenta, tercero, centrocosto, detalle, cheque, valdebito, valcredito, estado, vigencia) values ('5 $concecc','$cuentacb','$_POST[tercero]','$centroCostosCeros','Intereses Sobtretasa Ambiental $vig','','".round($valordeb,0)."',0,'1','$_POST[vigencia]')";
                                                                    mysqli_query($linkbd, $sqlr);

                                                                }
                                                            }

                                                        }

                                                    }else{

                                                        switch ($rowc[7]){

                                                            case 'S':

                                                                if($valorcred>0){

                                                                    $sqlr="insert into comprobante_det (id_comp,cuenta,tercero,centrocosto,detalle, cheque,valdebito,valcredito,estado,vigencia) values ('5 $concecc','$rowc[4]','$_POST[tercero]','$centroCostosCeros','Reconocimiento de Intereses Sobtretasa Ambiental $vig','', '0','".round($valorcred,0)."','1','$_POST[vigencia]')";
                                                                    mysqli_query($linkbd,$sqlr);

                                                                }


                                                                break;

                                                            default:

                                                                if($valorcred>0){

                                                                    $sqlr="insert into comprobante_det (id_comp,cuenta,tercero,centrocosto,detalle, cheque,valdebito,valcredito,estado,vigencia) values ('5 $concecc','$rowc[4]','$_POST[tercero]','$centroCostosCeros','Reconocimiento de Intereses Sobtretasa Ambiental $vig','', '".round($valorcred,0)."','0','1','$_POST[vigencia]')";
                                                                    mysqli_query($linkbd, $sqlr);

                                                                    $sqlr="insert into comprobante_det (id_comp,cuenta,tercero,centrocosto,detalle, cheque,valdebito,valcredito,estado,vigencia) values ('5 $concecc','$rowc[4]','$_POST[tercero]','$centroCostosCeros','Intereses Sobtretasa Ambiental $vig','', '".round($valordeb,0)."','".round($valorcred,0)."','1','$_POST[vigencia]')";
                                                                    mysqli_query($linkbd, $sqlr);
                                                                    $valordeb=$valorcred;
                                                                    $sqlr="insert into comprobante_det (id_comp, cuenta, tercero, centrocosto, detalle, cheque, valdebito, valcredito, estado, vigencia) values ('5 $concecc','$cuentacb','$_POST[tercero]','$centroCostosCeros','Intereses Sobtretasa Ambiental $vig','','".round($valordeb,0)."',0,'1','$_POST[vigencia]')";
                                                                    mysqli_query($linkbd, $sqlr);

                                                                }

                                                            break;
                                                        }
                                                    }
				 								}
											}break;

                                            case '30': //Intereses Sobretasa Bomberil
                                            {
                                                $sq="select fechainicial from conceptoscontables_det where codigo='$rowi[2]' and modulo='4' and tipo='PR' and fechainicial<'$fechaf' and cuenta!='' order by fechainicial asc";
                                                $re=mysqli_query($linkbd,$sq);
                                                while($ro=mysqli_fetch_assoc($re)){$_POST['fechacausa']=$ro["fechainicial"];}
                                                $sqlrc="Select * from conceptoscontables_det where estado='S' and modulo='4' AND codigo='$rowi[2]' and tipo='PR' and fechainicial='$_POST[fechacausa]' ORDER BY cuenta DESC ";
                                                    $resc=mysqli_query($linkbd,$sqlrc);
                                                while($rowc=mysqli_fetch_row($resc))
                                                {
                                                    $centroCostosCeros = str_pad($rowc[5], 2, "0", STR_PAD_LEFT);
                                                    $porce=$rowi[5];

                                                    $sqlrDescIntBomberil = "SELECT descuentointbomberil FROM tesoliquidapredial_desc WHERE id_predial='".$_POST['idrecaudo']."' AND vigencia = '$vig'";
													$rescDescIntBomberil = mysqli_query($linkbd, $sqlrDescIntBomberil);
													$rowcDescIntBomberil = mysqli_fetch_row($rescDescIntBomberil);

                                                    $valorcred=$row[7]-$rowcDescIntBomberil[0];
                                                    $valordeb=0;

                                                    switch ($rowc[7]){

                                                        case 'S':

                                                            if($valorcred > 0){

                                                                $sqlr="insert into comprobante_det (id_comp, cuenta, tercero, centrocosto, detalle, cheque, valdebito, valcredito, estado, vigencia) values ('5 $concecc', '$rowc[4]','$_POST[tercero]', '$centroCostosCeros','Reconocimiento de Intereses Sobretasa Bomberil $vig','','0','".round($valorcred,0)."','1','$_POST[vigencia]')";
                                                                mysqli_query($linkbd,$sqlr);

                                                            }

                                                            break;

                                                        default:

                                                            if($valorcred > 0){

                                                                $sqlr="insert into comprobante_det (id_comp,cuenta,tercero,centrocosto,detalle, cheque,valdebito,valcredito,estado,vigencia) values ('5 $concecc','$rowc[4]','$_POST[tercero]','$centroCostosCeros','Reconocimiento de Intereses Sobretasa Bomberil $vig','', '".round($valorcred,0)."','0','1','$_POST[vigencia]')";
                                                                mysqli_query($linkbd, $sqlr);

                                                                $sqlr="insert into comprobante_det (id_comp,cuenta,tercero,centrocosto,detalle, cheque,valdebito,valcredito,estado,vigencia) values ('5 $concecc','$rowc[4]','$_POST[tercero]','$centroCostosCeros','Intereses Sobretasa Bomberil $vig','', '".round($valordeb,0)."','".round($valorcred,0)."','1','$_POST[vigencia]')";
                                                                mysqli_query($linkbd, $sqlr);

                                                                $valordeb = $valorcred;
                                                                $sqlr="insert into comprobante_det (id_comp, cuenta, tercero, centrocosto, detalle, cheque, valdebito, valcredito, estado, vigencia) values ('5 $concecc','$cuentacb','$_POST[tercero]','$centroCostosCeros','Intereses Sobretasa Bomberil $vig','','".round($valordeb,0)."',0,'1','$_POST[vigencia]')";
                                                                mysqli_query($linkbd, $sqlr);

                                                            }

                                                            break;
                                                    }

                                                }
                                            }break;

											case '34': //***
											{
												$sqlca="SELECT valor_inicial,valor_final,tipo FROM dominios WHERE nombre_dominio='COBRO_ALUMBRADO' AND tipo='S'";
												$resca=mysqli_query($linkbd,$sqlca);
												while ($rowca =mysqli_fetch_row($resca))
												{
													$cobroalumbrado=$rowca[0];
													$vcobroalumbrado=$rowca[1];
													$tcobroalumbrado=$rowca[2];
												}
												if($tcobroalumbrado=='S' && substr($cedulaCatastral,0,2) == '00')
												{
													$rowcAlumbrado = 0;
													//$valorAlumbrado=round($row[2]*($vcobroalumbrado/1000),0);
													$sqlrAlumbrado = "SELECT val_alumbrado FROM tesoliquidapredial_desc WHERE id_predial='".$_POST['idrecaudo']."' AND vigencia = '$vig'";
													$rescAlumbrado = mysqli_query($linkbd,$sqlrAlumbrado);
													$rowcAlumbrado = mysqli_fetch_row($rescAlumbrado);
													$valorAlumbrado = $rowcAlumbrado[0];

													$sq="select fechainicial from conceptoscontables_det where codigo='$rowi[2]' and modulo='4' and tipo='PR' and fechainicial<'$fechaf' and cuenta!='' order by fechainicial asc";
													$re=mysqli_query($linkbd,$sq);
													while($ro=mysqli_fetch_assoc($re)){$_POST['fechacausa']=$ro["fechainicial"];}
													$sqlrc="Select * from conceptoscontables_det where estado='S' and modulo='4' AND codigo='$rowi[2]' and tipo='PR' and fechainicial='$_POST[fechacausa]' ORDER BY cuenta DESC";
													$resc=mysqli_query($linkbd,$sqlrc);
													while($rowc=mysqli_fetch_row($resc))
													{
														$centroCostosCeros = str_pad($rowc[5], 2, "0", STR_PAD_LEFT);
														$porce = $rowi[5];
                                                        $valorcred=$valorAlumbrado;
														$valordeb=0;

                                                        switch($rowc[7]){

                                                            case 'S':

                                                                if($valorcred>0){

                                                                    $sqlr="insert into comprobante_det (id_comp,cuenta,tercero,centrocosto,detalle, cheque,valdebito,valcredito,estado,vigencia) values ('5 $concecc','$rowc[4]','$_POST[tercero]','$centroCostosCeros','Reconocimiento de Ingreso Impuesto sobre el Servicio de Alumbrado Público $vig','', '0','".round($valorcred,0)."','1','$_POST[vigencia]')";
																	mysqli_query($linkbd, $sqlr);

                                                                }

                                                                break;

                                                            default:

                                                                if($valorcred>0){

                                                                    $sqlr = "insert into comprobante_det (id_comp,cuenta,tercero,centrocosto,detalle, cheque,valdebito,valcredito,estado,vigencia) values ('5 $concecc','$rowc[4]','$_POST[tercero]','$centroCostosCeros','Reconocimiento de Ingreso Impuesto sobre el Servicio de Alumbrado Público $vig','', '".round($valorcred,0)."','0','1','$_POST[vigencia]')";
																	mysqli_query($linkbd, $sqlr);

                                                                    $sqlr = "insert into comprobante_det (id_comp,cuenta,tercero,centrocosto,detalle, cheque,valdebito,valcredito,estado,vigencia) values ('5 $concecc','$rowc[4]','$_POST[tercero]','$centroCostosCeros','Ingreso Impuesto sobre el Servicio de Alumbrado Público $vig','', '".round($valordeb,0)."','".round($valorcred,0)."','1','$_POST[vigencia]')";
																	mysqli_query($linkbd, $sqlr);

																	$valordeb = $valorcred;

																	$sqlr = "insert into comprobante_det (id_comp,cuenta,tercero,centrocosto,detalle, cheque,valdebito,valcredito,estado,vigencia) values ('5 $concecc','$cuentacb','$_POST[tercero]','$centroCostosCeros','Ingreso Impuesto sobre el Servicio de Alumbrado Público $vig','', '".round($valordeb,0)."',0,'1','$_POST[vigencia]')";
																	mysqli_query($linkbd, $sqlr);

                                                                }

                                                                break;

                                                        }

													}
												}
											}
                                            break;
										}
		 							}
		 						}
							}


	 						echo "<table class='inicio'><tr><td class='saludo1'><center>Se Reflejo de Manera Correcta el Recibo de Caja <img src='imagenes/confirm.png'></center></td></tr></table>";
   	 					} //fin de la verificacion
	 					else
	 					{echo"<script>despliegamodalm('visible','2','Ya Existe un Recibo de Caja para esta Liquidacion Predial');</script>";}
			 			//***FIN DE LA VERIFICACION
					}break;
					case 2:{  //********** INDUSTRIA Y COMERCIO
						$valorcuentabanco = 0;
						$concecc = $_POST['idcomp'];
						$sqlr="SELECT tmindustria, desindustria, desavisos, desbomberil, intindustria, intavisos, intbomberil FROM tesoparametros";
						$res = mysqli_query($linkbd,$sqlr);
						while ($row = mysqli_fetch_row($res)){
							$descunidos = "$row[1]$row[2]$row[3]";
							$intecunidos = "$row[4]$row[5]$row[6]";
						}
						if(strpos($_POST['fecha'],"-")===false){
							preg_match("/([0-9]{2})\/([0-9]{2})\/([0-9]{4})/", $_POST['fecha'],$fecha);
							$fechaf = "$fecha[3]-$fecha[2]-$fecha[1]";
						} else {
							$fechaf = $_POST['fecha'];
						}
						$sqlr = "SELECT count(*) FROM tesoreciboscaja WHERE id_recaudo='".$_POST['idrecaudo']."' and tipo='2'";
						$res = mysqli_query($linkbd,$sqlr);
						while($r = mysqli_fetch_row($res)){
							$numerorecaudos = $r[0];
						}
						if($numerorecaudos >= 0 ){
							$sqlr = "DELETE FROM comprobante_cab WHERE numerotipo = '$concecc' AND tipo_comp = '5'";
							mysqli_query($linkbd,$sqlr);
							$sqlr = "DELETE FROM comprobante_det WHERE id_comp='5 $concecc'";
							mysqli_query($linkbd,$sqlr);
							if (!mysqli_query($linkbd,$sqlr)){
								echo "
								<script>
									Swal.fire({
										icon: 'error',
										title: 'Error!',
										text: 'Manejador de Errores de la Clase BD, No se pudo ejecutar la petición: $sqlr',
										confirmButtonText: 'Continuar',
										confirmButtonColor: '#FF121A',
										timer: 2500
									});
								</script>";
							} else {
								//*************COMPROBANTE CONTABLE INDUSTRIA
								$sqlr = "INSERT INTO comprobante_cab (numerotipo, tipo_comp, fecha, concepto, total, total_debito, total_credito, diferencia, estado) values ($concecc, 5, '$fechaf', '$_POST[concepto]', 0, '$_POST[totalc]','$_POST[totalc]', 0, '$_POST[estadoc]')";
								mysqli_query($linkbd,$sqlr);
								$idcomp = $_POST['idcomp'];
								if($_POST['modorec']=='caja'){
									$cuentacb = $_POST['cuentacaja'];
									$cajas = $_POST['cuentacaja'];
									$cbancos = "";
								}
								if($_POST['modorec']=='banco'){
									$cuentacb = $_POST['banco'];
									$cajas = "";
									$cbancos = $_POST['banco'];
								}
								for($x=0;$x<count($_POST['dcoding']);$x++){
									//***** BUSQUEDA INGRESO ********
									$sqlr = "SELECT ncuotas FROM tesoindustria WHERE id_industria='".$_POST['idrecaudo']."'";
									$res = mysqli_query($linkbd,$sqlr);
									$row = mysqli_fetch_row($res);
									$ncuotas = $row[0];
									$sqlr = "SELECT * FROM tesoindustria_det WHERE id_industria='".$_POST['idrecaudo']."'";
									$res = mysqli_query($linkbd,$sqlr);
									$row = mysqli_fetch_row($res);
									$industria = $row[1] + $row[15] + $row[31];
									$avisos = $row[2];
									$bomberil = $row[3];
									$retenciones = $row[4];
									$saldoafavor = $row[20];
									$sanciones = $row[5];
									$intereses = $row[25];
									$interesesind = $row[26];
									$interesesavi = $row[27];
									$interesesbom = $row[28];
									$antivigact = $row[11];
									$antivigant = $row[10];
									$saldopagar = $row[8];
									$exoneracion = $row[18];
									$autoretencion = $row[19];
									if((float)$intereses>0){//intereses
										$intetodos = (float)$interesesind + (float)$interesesavi + (float)$interesesbom;
										if($intetodos > 0){
											$indinteres = (float)$interesesind;
											$aviinteres = (float)$interesesavi;
											$bominteres = (float)$interesesbom;
										} else {
											$indinteres = (float)$intereses;
											$aviinteres = 0;
											$bominteres = 0;
										}
									}
									if(($row[21]>0)|| ($row[13]>0)){//descuentos
										if(($row[22]+$row[23]+$row[24])>0){
											$descuenindus = $row[22];//descuento industria
											$descuenaviso = $row[23];//descuento avisos
											$descuenbombe = $row[24];//descuento bomberil
										} else {
											if(substr($descunidos, -3, 1)=='S'){//descuento industria
												$descuenindus = ($row[1] + $row[31])*($row[13]/100);
											} else {
												$descuenindus = 0;
											}
											if(substr($descunidos, -2, 1)=='S'){//descuento avisos
												$descuenaviso = $row[2]*($row[13]/100);
											} else {
												$descuenaviso = 0;
											}
											if(substr($descunidos, -1, 1)=='S'){//descuento bomberil
												$descuenbombe = $row[3]*($row[13]/100);
											} else {
												$descuenbombe = 0;
											}
										}
									}
									$totalantivigact = $antivigact - $retenciones - $antivigant - $saldoafavor - $exoneracion - $autoretencion;

									if($totalantivigact < 0){
										$totalica = $industria - $descuenindus + $totalantivigact;
									}else {
										$totalica = $industria  - $descuenindus;
									}
									if ($totalica < 0){
										$totalsanciones = $sanciones + $totalica;
										$totalica = 0;
									} else {
										$totalsanciones = $sanciones;
									}
									if ($totalsanciones < 0){
										$totalavisos = $avisos + $interesesavi - $descuenaviso + $totalsanciones;
										$totalsanciones = 0;
									} else {
										$totalavisos = $avisos + $interesesavi - $descuenaviso;
									}
									if($totalavisos < 0){
										$totalbombe = $bomberil + $interesesbom - $descuenbombe + $totalavisos;
										$totalavisos = 0;
									} else {
										$totalbombe = $bomberil + $interesesbom - $descuenbombe;
									}

									if($ncuotas > 1){
										$totalica = $totalica/$ncuotas;
										$totalsanciones = $totalsanciones/$ncuotas;
										$totalbombe = $totalbombe/$ncuotas;
										$totalavisos = $totalavisos/$ncuotas;
									}
									$valorcred = $valordeb = $saldo01 = $auxreten = $auxsaldoafavor = $auxantivigant = 0;
									$numcc = '';
									$restem1 = $restem2 = $restem3 = 0;
									$idcodigo = $_POST['dcoding'][$x];
									$sqlri = "SELECT * FROM tesoingresos_ica_det WHERE codigo = '$idcodigo'  OR codigo = '04' AND vigencia = (SELECT MAX(vigencia) FROM tesoingresos_ica_det WHERE codigo =  '$idcodigo' OR codigo = '04') ORDER BY concepto ASC";
									$res = mysqli_query($linkbd,$sqlri);
									while($row = mysqli_fetch_row($res)){
										switch($row[2]){
											case '00': {//*****Sanciones
												if(($totalsanciones > 0 ) && ($_POST['valorecaudo'] != 0)){
													$sqlr2 = "SELECT * FROM conceptoscontables_det WHERE estado='S' AND modulo='4' AND codigo='00' AND tipo='C' AND tipocuenta='N' AND debito='S' AND fechainicial=(SELECT MAX(fechainicial) FROM conceptoscontables_det WHERE estado='S' AND modulo='4' AND codigo='00' AND tipo='C' AND tipocuenta='N' AND debito='S' AND fechainicial<='$fechaf')";
													$res2 = mysqli_query($linkbd,$sqlr2);
													while($row2 = mysqli_fetch_row($res2)){
														if($numcc==''){
															$numcc = $row2[5];
														}
														if($row2[3]=='N'){
															if($row2[6]=='S'){
																$valordeb = 0;
																$valorcred = $totalsanciones;
																$sqlr = "INSERT INTO comprobante_det (id_comp, cuenta, tercero, centrocosto,detalle, cheque, valdebito, valcredito, estado, vigencia) VALUES ('5 $concecc', '$row2[4]', '".$_POST['tercero']."', '$row2[5]', 'Sanciones ICA ".$_POST['ageliquida']."', '', '0', '$valorcred', '1', '".$_POST['vigencia']."')";
																mysqli_query($linkbd,$sqlr);
																if($_POST['cuentabanco']==$row2[4]){
																	$valorcuentabanco = $valorcuentabanco + (0-$valorcred);
																}
																//********** CAJA O BANCO
																$valordeb = $totalsanciones;
																$cuentacbr = $cuentacb;
																$sqlr = "INSERT INTO comprobante_det (id_comp,cuenta,tercero,centrocosto, detalle,cheque,valdebito,valcredito,estado,vigencia) VALUES ('5 $concecc','$cuentacb','$_POST[tercero]','$row2[5]','Sanciones ICA $_POST[modorec]','', '$valordeb','0','1','$_POST[vigencia]')";
																mysqli_query($linkbd,$sqlr);
																if($_POST['cuentabanco']==$cuentacbr){
																	$valorcuentabanco = $valorcuentabanco + ($valordeb-0);
																}

															}
														}
													}
												}
											}break;
											case '04': {//*****industria
												if(($totalica > 0) || ($_POST['valorecaudo'] == 0)){
													$sqlr2 = "SELECT * FROM conceptoscontables_det WHERE estado='S' AND modulo='4' AND codigo='04' AND tipo='C' AND tipocuenta='N' AND debito='S' AND fechainicial=(SELECT MAX(fechainicial) FROM conceptoscontables_det WHERE estado='S' AND modulo='4' AND codigo='04' AND tipo='C' AND tipocuenta='N' AND debito='S' AND fechainicial<='$fechaf')";
													$res2 = mysqli_query($linkbd,$sqlr2);
													while($row2 = mysqli_fetch_row($res2)){
														if($_POST['valorecaudo'] == 0){
															$totalica = 0;
														}
														if($numcc==''){
															$numcc=$row2[5];
														}
														$valorcred = $totalica;
														$cuentaica = $row2[4];
														$cuentaindustria = $row2[4];
														$sqlr="INSERT INTO comprobante_det (id_comp,cuenta,tercero,centrocosto,detalle, cheque, valdebito,valcredito,estado,vigencia) VALUES ('5 $concecc','$row2[4]','$_POST[tercero]','$row2[5]','Industria y Comercio $_POST[ageliquida]','',0, '$valorcred','1', '$_POST[vigencia]')";
														mysqli_query($linkbd,$sqlr);
														if($_POST['cuentabanco']==$row2[4]){
															$valorcuentabanco = $valorcuentabanco + (0-$valorcred);
														}
														//********** CAJA O BANCO
														$valordeb = $totalica;
														$cuentacbr = $cuentacb;
														$sqlr="INSERT INTO comprobante_det (id_comp,cuenta,tercero,centrocosto,detalle, cheque, valdebito,valcredito,estado,vigencia) VALUES ('5 $concecc','$cuentacbr','$_POST[tercero]','$row2[5]','Industria y Comercio $_POST[modorec]','','$valordeb','0', '1','$_POST[vigencia]')";
														mysqli_query($linkbd,$sqlr);
														if($_POST['cuentabanco']==$cuentacbr){
															$valorcuentabanco = $valorcuentabanco + ($valordeb-0);
														}

													}
												}
											}break;
											case '05':{//************avisos
												if(($totalavisos > 0 ) && ($_POST['valorecaudo'] != 0)){
													$sqlr2 = "SELECT * FROM conceptoscontables_det WHERE estado='S' AND modulo='4' AND codigo='05' AND tipo='C' AND fechainicial=(SELECT MAX(fechainicial) FROM conceptoscontables_det WHERE estado='S' AND modulo='4' AND codigo='05' AND tipo='C' AND fechainicial<='$fechaf')";
													$res2 = mysqli_query($linkbd,$sqlr2);
													while($row2 = mysqli_fetch_row($res2)){
														if($numcc==''){
															$numcc = $row2[5];
														}
														if($row2[3]=='N'){
															if($row2[6]=='S'){
																$valordeb = 0;
																$valorcred = $totalavisos;
																$sqlr = "INSERT INTO comprobante_det (id_comp,cuenta,tercero,centrocosto,detalle, cheque,valdebito,valcredito,estado,vigencia) VALUES ('5 $concecc','$row2[4]','".$_POST['tercero']."','$row2[5]','Avisos y Tableros ".$_POST['ageliquida']."','','0', '$valorcred','1', '".$_POST['vigencia']."')";
																mysqli_query($linkbd,$sqlr);
																if($_POST['cuentabanco']==$row2[4]){
																	$valorcuentabanco = $valorcuentabanco + (0-$valorcred);
																}
																//********** CAJA O BANCO
																$valordeb = $totalavisos;
																$cuentacbr = $cuentacb;
																$sqlr = "INSERT INTO comprobante_det (id_comp,cuenta,tercero,centrocosto, detalle,cheque,valdebito,valcredito,estado,vigencia) VALUES ('5 $concecc','$cuentacb','$_POST[tercero]','$row2[5]','Avisos y Tableros $_POST[modorec]','', '$valordeb','0','1','$_POST[vigencia]')";
																mysqli_query($linkbd,$sqlr);
																if($_POST['cuentabanco']==$cuentacbr){
																	$valorcuentabanco = $valorcuentabanco + ($valordeb-0);
																}

															}
														}
													}
												}
											}break;
											case '06': {//*********bomberil ********
												if(($totalbombe > 0 ) && ($_POST['valorecaudo'] != 0)){
													$sqlr2 = "SELECT * FROM conceptoscontables_det WHERE estado='S' AND modulo='4' AND codigo='06' AND tipo='C' AND fechainicial=(SELECT MAX(fechainicial) FROM conceptoscontables_det WHERE estado='S' AND modulo='4' AND codigo='06' AND tipo='C' AND fechainicial<='$fechaf')";
													$res2 = mysqli_query($linkbd,$sqlr2);
													while($row2 = mysqli_fetch_row($res2)){
														if($numcc==''){$numcc=$row2[5];}
														if($row2[3]=='N'){
															if($row2[6]=='S'){
																$valordeb = 0;
																$valorcred = $totalbombe;
																$sqlr = "INSERT INTO comprobante_det (id_comp,cuenta,tercero,centrocosto,detalle, cheque,valdebito,valcredito,estado,vigencia) VALUES ('5 $concecc','$row2[4]','$_POST[tercero]','$row2[5]','Bomberil $_POST[ageliquida]', '','$valordeb', '$valorcred','1', '$_POST[vigencia]')";
																mysqli_query($linkbd,$sqlr);
																if($_POST['cuentabanco']==$row2[4]){
																	$valorcuentabanco = $valorcuentabanco + (0-$valorcred);
																}
																//********** CAJA O BANCO
																$valordeb = $totalbombe;
																$cuentacbr = $cuentacb;
																$sqlr = "INSERT INTO comprobante_det (id_comp,cuenta,tercero,centrocosto, detalle,cheque,valdebito,valcredito, estado,vigencia) values ('5 $concecc','$cuentacb','$_POST[tercero]','$row2[5]','Bomberil $_POST[modorec]', '','$valordeb','0', '1','$_POST[vigencia]' )";
																mysqli_query($linkbd,$sqlr);
																if($_POST['cuentabanco']==$cuentacbr){
																	$valorcuentabanco = $valorcuentabanco + ($valordeb-0);
																}
															}
														}
													}
												}
											}break;
											case 'P12':{//Anticipo vigencia Actual
												if(($totalantivigact > 0) && ($_POST['valorecaudo'] != 0)){
													$sqlr2 = "SELECT * FROM conceptoscontables_det WHERE estado='S' AND modulo='4' AND codigo='P12' AND tipo='C' AND fechainicial=(SELECT MAX(fechainicial) FROM conceptoscontables_det WHERE estado='S' AND modulo='4' AND codigo='P12' AND tipo='C' AND fechainicial<='$fechaf')";
													$res2 = mysqli_query($linkbd,$sqlr2);
													while($row2 = mysqli_fetch_row($res2)){
														if($row2[3]=='N'){
															if($row2[6]=='N'){
																$valorcred = $totalantivigact;
																$cuentaica = $row2[4];
																$sqlr="INSERT INTO comprobante_det (id_comp,cuenta,tercero,centrocosto,detalle, cheque,valdebito,valcredito,estado,vigencia) VALUES ('5 $concecc','$cuentaica','$_POST[tercero]','$row2[5]','Anticipo vigencia Actual $_POST[ageliquida]','',0, '$valorcred','1', '$_POST[vigencia]')";
																mysqli_query($linkbd,$sqlr);

																if($_POST['cuentabanco']==$cuentaica){
																	$valorcuentabanco = $valorcuentabanco + (0-$valorcred);
																}
																//********** CAJA O BANCO
																$valordeb = $totalantivigact;
																$cuentacbr = $cuentacb;
																$sqlr = "INSERT INTO comprobante_det (id_comp,cuenta,tercero,centrocosto, detalle,cheque,valdebito,valcredito,estado,vigencia) values ('5 $concecc','$cuentacb','$_POST[tercero]','$row2[5]','Anticipo vigencia Actual $_POST[modorec]','', '$valordeb','0','1','$_POST[vigencia]')";
																mysqli_query($linkbd,$sqlr);

																if($_POST['cuentabanco']==$cuentacbr){
																	$valorcuentabanco=$valorcuentabanco+($valordeb-0);
																}

															}
														}
													}
												}
											}break;
											case 'P16':{//*****INTERESES INDUSTRIA
												if(($indinteres > 0) && ($_POST['valorecaudo'] != 0)){
													$sqlr2 = "SELECT * FROM conceptoscontables_det WHERE estado='S' AND modulo='4' AND codigo='P16' AND tipo='C' AND debito='S' AND fechainicial=(SELECT MAX(fechainicial) FROM conceptoscontables_det WHERE estado='S' AND modulo='4' AND codigo='P16' AND tipo='C' AND debito='S' AND fechainicial<='$fechaf')";
													$res2 = mysqli_query($linkbd,$sqlr2);
													while($row2 = mysqli_fetch_row($res2)){
														if($row2[3]=='N'){
															$valordeb = 0;
															$valorcred = $indinteres;
															$cuentacbr = $cuentacb;
															$sqlr = "insert into comprobante_det (id_comp, cuenta, tercero, centrocosto, detalle, cheque, valdebito, valcredito, estado, vigencia) values ('5 $concecc', '$row2[4]','".$_POST['tercero']."', '$row2[5]','Intereses Industria y Comercio ".$_POST['ageliquida']."','','$valordeb','$valorcred','1', '".$_POST['vigencia']."')";
															mysqli_query($linkbd,$sqlr);
															$valorcred = 0;
															$valordeb = $indinteres;
															$sqlr = "INSERT INTO comprobante_det (id_comp, cuenta, tercero, centrocosto, detalle, cheque, valdebito, valcredito, estado, vigencia) VALUES ('5 $concecc','$cuentacbr','".$_POST['tercero']."', '$row2[5]','Intereses Industria y Comercio ".$_POST['ageliquida']."','','$valordeb','$valorcred','1', '".$_POST['vigencia']."')";
															mysqli_query($linkbd,$sqlr);
															if($_POST['cuentabanco']==$cuentacbr){
																$valorcuentabanco = $valorcuentabanco + ($valordeb-0);
															}
														}
													}
												}
											}break;
										}
									}
								}
								//**************Ajuste de redondeo******************
								$diferenciatotal = ($_POST['valorecaudo']/$ncuotas) - $valorcuentabanco;
								if($diferenciatotal!=0){
									$sqlr = "SELECT valor_inicial from dominios where nombre_dominio='CUENTA_MILES'";
									$res = mysqli_query($linkbd,$sqlr);
									while ($row = mysqli_fetch_row($res)){
										$cuentaredondeos = $row[0];
									}
									if($diferenciatotal>0){
										$sqlr = "INSERT INTO comprobante_det (id_comp,cuenta,tercero,centrocosto,detalle, cheque, valdebito,valcredito,estado,vigencia) VALUES ('5 $concecc','".$_POST['cuentabanco']."','".$_POST['tercero']."','$numcc','AJUSTE DE REDONDEO','0',$diferenciatotal,'0','1', '".$_POST['vigencia']."')";
										mysqli_query($linkbd,$sqlr);
										$sqlr = "INSERT INTO comprobante_det (id_comp,cuenta,tercero,centrocosto,detalle, cheque, valdebito,valcredito,estado,vigencia) VALUES ('5 $concecc','$cuentaredondeos','".$_POST['tercero']."','$numcc','AJUSTE DE REDONDEO','0','0',$diferenciatotal,'1', '".$_POST['vigencia']."')";
										mysqli_query($linkbd,$sqlr);
									} else {
										$sqlr = "INSERT INTO comprobante_det (id_comp,cuenta,tercero,centrocosto,detalle, cheque, valdebito,valcredito,estado,vigencia) VALUES ('5 $concecc','".$_POST['cuentabanco']."','".$_POST['tercero']."','$numcc','AJUSTE DE REDONDEO','0','0',".abs($diferenciatotal).",'1', '".$_POST['vigencia']."')";
										mysqli_query($linkbd,$sqlr);
										$sqlr = "INSERT INTO comprobante_det (id_comp,cuenta,tercero,centrocosto,detalle, cheque, valdebito,valcredito,estado,vigencia) VALUES ('5 $concecc','$cuentaredondeos','".$_POST['tercero']."','$numcc','AJUSTE DE REDONDEO','0',".abs($diferenciatotal).",'0','1', '".$_POST['vigencia']."')";
										mysqli_query($linkbd,$sqlr);
									}
								}
								echo "
								<script>
									Swal.fire({
										icon: 'success',
										title: 'Se Reflejo de Manera Correcta el Recibo de Caja',
										confirmButtonText: 'Continuar',
										confirmButtonColor: '#FF121A',
										timer: 3000
									});
								</script>";
							}
						} else {
							echo"
							<script>
								Swal.fire({
									icon: 'success',
									title: 'Ya Existe un Recibo de Caja para esta Liquidacion',
									showConfirmButton: true,
									confirmButtonText: 'Continuar',
									confirmButtonColor: '#01CC42',
									timer: 3500
								});
							</script>";
						}
					}break;
					case 3: {//**************OTROS RECAUDOS
						$sqlr="delete from comprobante_cab where numerotipo=$_POST[idcomp] and tipo_comp='5'";
						mysqli_query($linkbd,$sqlr);
						$sqlr="delete from comprobante_det where  numerotipo=$_POST[idcomp] and tipo_comp='5'";
						mysqli_query($linkbd,$sqlr);
						if(strpos($_POST['fecha'],"-")===false){
							preg_match("/([0-9]{2})\/([0-9]{2})\/([0-9]{4})/", $_POST['fecha'],$fecha);
							$fechaf="$fecha[3]-$fecha[2]-$fecha[1]";
						}
						else{$fechaf=$_POST['fecha'];}
						//*********************CREACION DEL COMPROBANTE CONTABLE ***************************
						//***busca el consecutivo del comprobante contable
						$consec=$_POST['idcomp'];
						//***cabecera comprobante
						$sqlr="insert into comprobante_cab (numerotipo,tipo_comp,fecha,concepto,total,total_debito,total_credito, diferencia,estado) values ($consec,5,'$fechaf','".$_POST['concepto']."',0,".$_POST['totalc'].",".$_POST['totalc'].",0,'".$_POST['estadoc']."')";
						mysqli_query($linkbd,$sqlr);
						$idcomp=mysqli_insert_id($linkbd);
						//******************* DETALLE DEL COMPROBANTE CONTABLE *********************
						for($x=0;$x<count($_POST['dcoding']);$x++)
						{
		 					//***** BUSQUEDA INGRESO ********
							$sqlri="Select * from tesoingresos_det where codigo='".$_POST['dcoding'][$x]."' and vigencia=(SELECT MAX(vigencia) FROM tesoingresos_det WHERE codigo = '".$_POST['dcoding'][$x]."')";
	 						$resi=mysqli_query($linkbd,$sqlri);
							while($rowi=mysqli_fetch_row($resi))
							{
								//**** busqueda cuenta presupuestal*****
								//busqueda concepto contable
								$sq="select fechainicial from conceptoscontables_det where codigo='$rowi[2]' and modulo='4' and tipo='C' and fechainicial<'$fechaf' and cuenta!='' order by fechainicial asc";
								$re=mysqli_query($linkbd,$sq);
								while($ro=mysqli_fetch_assoc($re)){$_POST['fechacausa']=$ro["fechainicial"];}
								$sqlrc="Select * from conceptoscontables_det where estado='S' and modulo='4' AND codigo='$rowi[2]' and tipo='C' and fechainicial='$_POST[fechacausa]'";
	 	 						$resc=mysqli_query($linkbd,$sqlrc);
								while($rowc=mysqli_fetch_row($resc))
								 {
			  						$porce=$rowi[5];
									if($rowc[6]=='S' and $_POST['dcoding'][$x]!=$_POST['cobrorecibo'])
			  						{
			  							$cuenta=$rowc[4];
										$valorcred=$_POST['dvalores'][$x]*($porce/100);
										$valordeb=0;
										if($rowc[3]=='N')
			    						{
											//*****inserta del concepto contable
											//***********MODIFICAR CUENTA PPTAL DE INGRESO AGREGARLE EL RECAUDO *********
											$sqlrpto="Select * from pptocuentas where estado='S' and cuenta='$rowi[6]' and vigencia='$vigusu'";
		 	 								$respto=mysqli_query($linkbd,$sqlrpto);
											$rowpto=mysqli_fetch_row($respto);
											$vi=$_POST['dvalores'][$x]*($porce/100);
			  								//****creacion documento presupuesto ingresos
											$sql="SELECT terceros FROM tesoingresos WHERE codigo=".$_POST['dcoding'][$x] ;
											$res=mysqli_query($linkbd,$sql);
											$row= mysqli_fetch_row($res);
											//************ FIN MODIFICACION PPTAL
											$sqlr="insert into comprobante_det (id_comp,cuenta,tercero,centrocosto,detalle,cheque,valdebito, valcredito,estado,vigencia) values ('5 $consec','$cuenta','$_POST[tercero]','$rowc[5]','Ingreso ".strtoupper($_POST['dncoding'][$x])."','', '$valordeb.','$valorcred','1','$_POST[vigencia]')";
											mysqli_query($linkbd,$sqlr);
											//***cuenta caja o banco
											if($_POST['modorec']=='caja')
											{
												$cuentacb=$_POST['cuentacaja'];
												$cajas=$_POST['cuentacaja'];
												$cbancos="";
			  								}
											if($_POST['modorec']=='banco')
											{
												$cuentacb=$_POST['banco'];
												$cajas="";
												$cbancos=$_POST['banco'];
			    							}
											$sqlr="insert into comprobante_det (id_comp,cuenta,tercero,centrocosto,detalle,cheque,valdebito, valcredito,estado,vigencia) values ('5 $consec','$cuentacb','".$_POST['tercero']."','$rowc[5]','Ingreso ".strtoupper($_POST['dncoding'][$x])."','', '$valorcred',0,'1','".$_POST['vigencia']."')";
											mysqli_query($linkbd,$sqlr);
										}
									}
			 						if($_POST['dcoding'][$x]==$_POST['cobrorecibo'] and $rowc[7]=='S')
			  						{
										$cuenta=$rowc[4];
										$valorcred=$_POST['dvalores'][$x]*($porce/100);
										$valordeb=0;
										if($rowc[3]=='N')
										{
											//*****inserta del concepto contable
											//***********MODIFICAR CUENTA PPTAL DE INGRESO AGREGARLE EL RECAUDO *********
											$sqlrpto="Select * from pptocuentas where estado='S' and cuenta='$rowi[6]' and vigencia=$vigusu";
											$respto=mysqli_query($linkbd,$sqlrpto);
											$rowpto=mysqli_fetch_row($respto);
											$vi=$_POST['dvalores'][$x]*($porce/100);
											$sql="SELECT terceros FROM tesoingresos WHERE codigo='".$_POST['dcoding'][$x]."'";
											$res=mysqli_query($linkbd,$sql);
											$row= mysqli_fetch_row($res);
											//****creacion documento presupuesto ingresos
											//************ FIN MODIFICACION PPTAL
											$sqlr="insert into comprobante_det (id_comp,cuenta,tercero,centrocosto,detalle,cheque,valdebito, valcredito,estado,vigencia) values ('5 $consec','$cuenta','".$_POST['tercero']."','$rowc[5]','Ingreso ".strtoupper($_POST['dncoding'][$x])."','', '$valordeb','$valorcred','1','".$_POST['vigencia']."')";
											mysqli_query($linkbd,$sqlr);
											//***cuenta caja o banco
											if($_POST['modorec']=='caja')
											{
												$cuentacb=$_POST['cuentacaja'];
												$cajas=$_POST['cuentacaja'];
												$cbancos="";
			  								}
											if($_POST['modorec']=='banco')
											{
												$cuentacb=$_POST['banco'];
												$cajas="";
												$cbancos=$_POST['banco'];
											}
											$sqlr="insert into comprobante_det (id_comp,cuenta,tercero,centrocosto,detalle,cheque,valdebito, valcredito,estado,vigencia) values ('5 $consec','$cuentacb','".$_POST['tercero']."','$rowc[5]','Ingreso ".strtoupper($_POST['dncoding'][$x])."','', '$valorcred',0,'1','$_POST[vigencia]')";
											mysqli_query($linkbd,$sqlr);
										}
			 						}
		 						}
		 					}
						}
						echo "<table class='inicio'><tr><td class='saludo1'><center>Se Reflejo de Manera Correcta el Recibo de Caja <img src='imagenes/confirm.png'></center></td></tr></table>";
					}break;
				} //*****fin del switch
			}//***bloqueo
			else
			{
				echo "<div class='inicio'><img src='imagenes\alert.png'> No Tiene los Permisos para Modificar este Documento</div>";
			}
		}//**fin del oculto
		?>
    </form><?php if($_POST['oculto']==""){echo"<script>validar2();</script>";}?>
</body>

</html>
