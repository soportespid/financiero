<?php

    header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=utf8");
	require 'comun.inc';
	require 'funciones.inc';
	session_start();
    if(empty($_SESSION)){
        header("location: index.php");
    }
	$linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");
	cargarcodigopag($_GET['codpag'],$_SESSION["nivel"]);
	date_default_timezone_set("America/Bogota");
?>
<!DOCTYPE >
<html lang=es>
	<head>
    <meta charset="utf-8"/>
		<meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1"/>
		<meta name="viewport" content="user-scalable=no">
		<title>:: IDEAL 10 - Gestión humana</title>
		<link href="favicon.ico" rel="shortcut icon"/>
		<link href="css/css2z.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
		<link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
		<link href="css/css4.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
        <link href="css/tabs.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css"/>
        <link href="css/style.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css"/>
        <link rel="stylesheet" href="sweetalert2/dist/sweetalert2.min.css">
        <script type="text/javascript" src="css/programas.js"></script>
        <script src="sweetalert2/dist/sweetalert2.min.js"></script>
		<script src="vue/vue.min.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script src="css/programas.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script src="css/calendario.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
	</head>
	<body>
		<header>
			<table>
				<tr><script>barra_imagenes("hum");</script><?php cuadro_titulos();?></tr>
			</table>
		</header>

        <main id="myapp" v-cloak>
            <input type="hidden" value = "2" ref="pageType">
            <div class="loading-container" v-show="isLoading" >
                <p class="text-loading"data-loading="IDEAL 10">IDEAL 10</p>
            </div>
            <nav>
                <table>
                    <tr><?php menu_desplegable("hum");?></tr>
                </table>
                <div class="bg-white group-btn p-1" id="newNavStyle">
                    <button type="button" @click="window.location.href='hum-nomina-liquidar-crear'" class="btn btn-white btn-primary-hover d-flex justify-between align-items-center">
                        <span>Nuevo</span>
                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 -960 960 960"><path d="M440-440H200v-80h240v-240h80v240h240v80H520v240h-80v-240Z"></path></svg>
                    </button>
                    <button type="button" @click="window.location.href='hum-nomina-liquidar-buscar'" class="btn btn-white btn-primary-hover d-flex justify-between align-items-center">
                        <span>Buscar</span>
                        <svg class="fill-black group-hover:fill-white w-5 h-5" xmlns="http://www.w3.org/2000/svg" viewBox="0 -960 960 960"><path d="M784-120 532-372q-30 24-69 38t-83 14q-109 0-184.5-75.5T120-580q0-109 75.5-184.5T380-840q109 0 184.5 75.5T640-580q0 44-14 83t-38 69l252 252-56 56ZM380-400q75 0 127.5-52.5T560-580q0-75-52.5-127.5T380-760q-75 0-127.5 52.5T200-580q0 75 52.5 127.5T380-400Z"></path></svg>
                    </button>
                    <button type="button" class="btn btn-white btn-primary-hover d-flex justify-between align-items-center" @click="mypop=window.open('hum-principal.php','',''); mypop.focus();">
                        <span>Nueva ventana</span>
                        <svg class="fill-black group-hover:fill-white w-5 h-5" xmlns="http://www.w3.org/2000/svg"  viewBox="0 -960 960 960" ><path d="M320-120v-80h80v-80H160q-33 0-56.5-23.5T80-360v-400q0-33 23.5-56.5T160-840h640q33 0 56.5 23.5T880-760v400q0 33-23.5 56.5T800-280H560v80h80v80H320ZM160-360h640v-400H160v400Zm0 0v-400 400Z"/></svg>
                    </button>
                    <button type="button" @click="mypop=window.open('hum-nomina-liquidar-editar','','');mypop.focus();" class="btn btn-white btn-primary-hover d-flex justify-between align-items-center">
                        <span class="group-hover:text-white">Duplicar pantalla</span>
                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 -960 960 960"><path d="M640-160v-360H160v360h480Zm80-200v-80h80v-360H320v200h-80v-200q0-33 23.5-56.5T320-880h480q33 0 56.5 23.5T880-800v360q0 33-23.5 56.5T800-360h-80ZM160-80q-33 0-56.5-23.5T80-160v-360q0-33 23.5-56.5T160-600h480q33 0 56.5 23.5T720-520v360q0 33-23.5 56.5T640-80H160Zm400-603ZM400-340Z"></path></svg>
                    </button>
                    <button type="button" @click="exportData();" class="btn btn-success btn-success-hover d-flex justify-between align-items-center">
                        <span>Exportar Excel</span>
                        <svg class="fill-black group-hover:fill-white w-4 h-4" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 384 512"><!--!Font Awesome Free 6.5.1 by @fontawesome - https://fontawesome.com License - https://fontawesome.com/license/free Copyright 2024 Fonticons, Inc. --><path d="M48 448V64c0-8.8 7.2-16 16-16H224v80c0 17.7 14.3 32 32 32h80V448c0 8.8-7.2 16-16 16H64c-8.8 0-16-7.2-16-16zM64 0C28.7 0 0 28.7 0 64V448c0 35.3 28.7 64 64 64H320c35.3 0 64-28.7 64-64V154.5c0-17-6.7-33.3-18.7-45.3L274.7 18.7C262.7 6.7 246.5 0 229.5 0H64zm90.9 233.3c-8.1-10.5-23.2-12.3-33.7-4.2s-12.3 23.2-4.2 33.7L161.6 320l-44.5 57.3c-8.1 10.5-6.3 25.5 4.2 33.7s25.5 6.3 33.7-4.2L192 359.1l37.1 47.6c8.1 10.5 23.2 12.3 33.7 4.2s12.3-23.2 4.2-33.7L222.4 320l44.5-57.3c8.1-10.5 6.3-25.5-4.2-33.7s-25.5-6.3-33.7 4.2L192 280.9l-37.1-47.6z"></path></svg>
                    </button>
                    <button type="button" @click="window.location.href='hum-nomina-liquidar-buscar'" class="btn btn-success btn-success-hover d-flex justify-between align-items-center">
                        <span>Atras</span>
                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 -960 960 960"><path d="M760-200v-160q0-50-35-85t-85-35H273l144 144-57 56-240-240 240-240 57 56-144 144h367q83 0 141.5 58.5T840-360v160h-80Z"></path></svg>
                    </button>
                </div>
            </nav>
            <section class="bg-white">
                <div>
                    <h2 class="titulos m-0">Ver liquidación de nómina</h2>
                    <div class="d-flex">
                        <div class="form-control w-50">
                            <label class="form-label" for="">Código:</label>
                            <div class="d-flex">
                                <button type="button" class="btn btn-primary" @click="nextItem('prev')"><</button>
                                <input type="text"  style="text-align:center;" v-model="txtConsecutivo" @change="nextItem()">
                                <button type="button" class="btn btn-primary" @click="nextItem('next')">></button>
                            </div>
                        </div>
                        <div class="form-control w-25">
                            <label class="form-label">Estado:</label>
                            <span :class="[txtEstado == 'S'  ? 'badge-success' : txtEstado == 'P' ? 'badge-primary' : 'badge-danger']" class="badge">{{ txtEstado == 'S' ? 'Activo'  : txtEstado=='P' ? 'Pagado' : 'Anulado'}}</span>
                        </div>
                        <div class="form-control">
                            <label class="form-label">Fecha:</label>
                            <input type="date" disabled v-model="txtFecha">
                        </div>
                        <div class="form-control">
                            <label class="form-label">Vigencia:</label>
                            <input type="text" disabled v-model="txtVigencia">
                        </div>
                        <div class="form-control">
                            <label class="form-label">Mes:</label>
                            <input type="text" disabled v-model="txtMes">
                        </div>
                    </div>
                    <div class="overflow-auto" style="height:60vh">
                        <table class="table fw-normal">
                            <thead class="text-center">
                                <tr>
                                    <th>Tipo</th>
                                    <th>Empleado</th>
                                    <th>CC/NIT</th>
                                    <th>Básico</th>
                                    <th>Dias liquidados</th>
                                    <th>Valor</th>
                                    <th>IBC</th>
                                    <th>Base Parafiscales</th>
                                    <th>Base ARP</th>
                                    <th>ARP</th>
                                    <th>Salud empleado</th>
                                    <th>Salud empresa</th>
                                    <th>Total salud</th>
                                    <th>Pensión empleado</th>
                                    <th>Pensión empresa</th>
                                    <th>Total pensión</th>
                                    <th>FSP</th>
                                    <th>Retencion en la fuente</th>
                                    <th>Otras deducciones</th>
                                    <th>Total deducciones</th>
                                    <th>Neto a pagar</th>
                                    <th>CCF</th>
                                    <th>SENA</th>
                                    <th>ICBF</th>
                                    <th>Inst. Técnico</th>
                                    <th>ESAP</th>
                                </tr>
                            </thead>
                            <tbody ref="tableData">
                                <tr :class="[data.type=='total' ? 'bg-secondary' : '']" v-for="(data,index) in arrLiquidacion">
                                    <td v-if="data.type!='total'">{{data.nombre}}</td>
                                    <td v-if="data.type!='total'">{{data.nombre_tercero}}</td>
                                    <td v-if="data.type!='total'">{{data.documento}}</td>
                                    <td v-if="data.type!='total'" class="text-right">{{formatMoney(data.salario)}}</td>
                                    <td v-if="data.type!='total'" class="text-center">{{data.dias}}</td>
                                    <td v-if="data.type!='total'" class="text-right">{{formatMoney(data.total)}}</td>
                                    <td v-if="data.type!='total'" class="text-right">{{formatMoney(data.valor_ibc)}}</td>
                                    <td v-if="data.type!='total'" class="text-right">{{formatMoney(data.valor_ibc_para)}}</td>
                                    <td v-if="data.type!='total'" class="text-right">{{formatMoney(data.valor_ibc_arl)}}</td>
                                    <td v-if="data.type!='total'" class="text-right">{{formatMoney(data.valor_arl)}}</td>
                                    <td v-if="data.type!='total'" class="text-right">{{formatMoney(data.valor_salud_empleado)}}</td>
                                    <td v-if="data.type!='total'" class="text-right">{{formatMoney(data.valor_salud_empresa)}}</td>
                                    <td v-if="data.type!='total'" class="text-right">{{formatMoney(data.valor_total_salud)}}</td>
                                    <td v-if="data.type!='total'" class="text-right">{{formatMoney(data.valor_pension_empleado)}}</td>
                                    <td v-if="data.type!='total'" class="text-right">{{formatMoney(data.valor_pension_empresa)}}</td>
                                    <td v-if="data.type!='total'" class="text-right">{{formatMoney(data.valor_total_pension)}}</td>
                                    <td v-if="data.type!='total'" class="text-right">{{formatMoney(data.valor_fondo)}}</td>
                                    <td v-if="data.type!='total'" class="text-right">{{formatMoney(data.valor_retencion)}}</td>
                                    <td v-if="data.type!='total'" class="text-right">{{formatMoney(data.valor_descuento)}}</td>
                                    <td v-if="data.type!='total'" class="text-right">{{formatMoney(data.valor_total_descuentos)}}</td>
                                    <td v-if="data.type!='total'" class="text-right fw-bold">{{formatMoney(data.valor_neto)}}</td>
                                    <td v-if="data.type!='total'" class="text-right">{{formatMoney(data.valor_aporte_ccf)}}</td>
                                    <td v-if="data.type!='total'" class="text-right">{{formatMoney(data.valor_aporte_sena)}}</td>
                                    <td v-if="data.type!='total'" class="text-right">{{formatMoney(data.valor_aporte_icbf)}}</td>
                                    <td v-if="data.type!='total'" class="text-right">{{formatMoney(data.valor_aporte_inst)}}</td>
                                    <td v-if="data.type!='total'" class="text-right">{{formatMoney(data.valor_aporte_esap)}}</td>

                                    <td v-if="data.type=='total'" class="text-right fw-bold" colspan="5">Total</td>
                                    <td v-if="data.type=='total'" class="text-right fw-bold">{{formatMoney(data.total.total_valor)}}</td>
                                    <td v-if="data.type=='total'" class="text-right fw-bold">{{formatMoney(data.total.total_base_ibc)}}</td>
                                    <td v-if="data.type=='total'" class="text-right fw-bold">{{formatMoney(data.total.total_base_para)}}</td>
                                    <td v-if="data.type=='total'" class="text-right fw-bold">{{formatMoney(data.total.total_base_arl)}}</td>
                                    <td v-if="data.type=='total'" class="text-right fw-bold">{{formatMoney(data.total.total_arl)}}</td>
                                    <td v-if="data.type=='total'" class="text-right fw-bold">{{formatMoney(data.total.total_salud_empleado)}}</td>
                                    <td v-if="data.type=='total'" class="text-right fw-bold">{{formatMoney(data.total.total_salud_empresa)}}</td>
                                    <td v-if="data.type=='total'" class="text-right fw-bold">{{formatMoney(data.total.total_salud)}}</td>
                                    <td v-if="data.type=='total'" class="text-right fw-bold">{{formatMoney(data.total.total_pension_empleado)}}</td>
                                    <td v-if="data.type=='total'" class="text-right fw-bold">{{formatMoney(data.total.total_pension_empresa)}}</td>
                                    <td v-if="data.type=='total'" class="text-right fw-bold">{{formatMoney(data.total.total_pension)}}</td>
                                    <td v-if="data.type=='total'" class="text-right fw-bold">{{formatMoney(data.total.total_fondo)}}</td>
                                    <td v-if="data.type=='total'" class="text-right fw-bold">{{formatMoney(data.total.total_retencion)}}</td>
                                    <td v-if="data.type=='total'" class="text-right fw-bold">{{formatMoney(data.total.total_otros_desc)}}</td>
                                    <td v-if="data.type=='total'" class="text-right fw-bold">{{formatMoney(data.total.total_descuentos)}}</td>
                                    <td v-if="data.type=='total'" class="text-right fw-bold">{{formatMoney(data.total.total_neto)}}</td>
                                    <td v-if="data.type=='total'" class="text-right fw-bold">{{formatMoney(data.total.total_aporte_ccf)}}</td>
                                    <td v-if="data.type=='total'" class="text-right fw-bold">{{formatMoney(data.total.total_aporte_sena)}}</td>
                                    <td v-if="data.type=='total'" class="text-right fw-bold">{{formatMoney(data.total.total_aporte_icbf)}}</td>
                                    <td v-if="data.type=='total'" class="text-right fw-bold">{{formatMoney(data.total.total_aporte_inst)}}</td>
                                    <td v-if="data.type=='total'" class="text-right fw-bold">{{formatMoney(data.total.total_aporte_esap)}}</td>
                                </tr>
                            </tbody>
                            <tfoot v-if="arrLiquidacion.length > 0">
                                <tr class="bg-secondary fw-bold">
                                    <td class="text-right" colspan="5">Neto</td>
                                    <td class="text-right">{{formatMoney(arrNeto.neto_devengado)}}</td>
                                    <td class="text-right" colspan="4">{{formatMoney(arrNeto.neto_arl)}}</td>
                                    <td class="text-right">{{formatMoney(arrNeto.neto_salud_empleado)}}</td>
                                    <td class="text-right">{{formatMoney(arrNeto.neto_salud_empresa)}}</td>
                                    <td class="text-right">{{formatMoney(arrNeto.neto_total_salud)}}</td>
                                    <td class="text-right">{{formatMoney(arrNeto.neto_pension_empleado)}}</td>
                                    <td class="text-right">{{formatMoney(arrNeto.neto_pension_empresa)}}</td>
                                    <td class="text-right">{{formatMoney(arrNeto.neto_total_pension)}}</td>
                                    <td class="text-right">{{formatMoney(arrNeto.neto_fsp)}}</td>
                                    <td class="text-right">{{formatMoney(arrNeto.neto_retencion)}}</td>
                                    <td class="text-right">{{formatMoney(arrNeto.neto_otros_descuentos)}}</td>
                                    <td class="text-right">{{formatMoney(arrNeto.neto_total_descuentos)}}</td>
                                    <td class="text-right">{{formatMoney(arrNeto.neto_total_pagado)}}</td>
                                    <td class="text-right">{{formatMoney(arrNeto.neto_ccf)}}</td>
                                    <td class="text-right">{{formatMoney(arrNeto.neto_sena)}}</td>
                                    <td class="text-right">{{formatMoney(arrNeto.neto_icbf)}}</td>
                                    <td class="text-right">{{formatMoney(arrNeto.neto_inst)}}</td>
                                    <td class="text-right">{{formatMoney(arrNeto.neto_esap)}}</td>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </section>
        </main>
		<script src="Librerias/vue/vue.min.js"></script>
		<script type="module" src="gestion_humana/nomina/js/functions_liquidar.js?<?= date('d_m_Y_h_i_s');?>"></script>
	</body>
</html>
