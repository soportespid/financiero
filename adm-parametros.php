<?php
    header("Cache-control: no-cache, no-store, must-revalidate");
    header("Content-Type: text/html;charset=uft8");
    require "comun.inc";
    require "funciones.inc";
    session_start();
    $linkbd = conectar_v7();
    $linkbd -> set_charset("utf8");
    cargarcodigopag($_GET['codpag'],$_SESSION["nivel"]);
    date_default_timezone_set("America/Bogota");
?>

<!DOCTYPE html>
    <html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>:: IDEAL 10 - Administración</title>
		<link href = "css/css2.css? <?php echo date('d_m_Y_h_i_s');?> " rel = "stylesheet" type = "text/css" />
		<link href = "css/css3.css? <?php echo date('d_m_Y_h_i_s');?> " rel = "stylesheet" type = "text/css" />
		<script type = "text/javascript" src = "jquery-1.11.0.min.js"></script>
		<script type = "text/javascript" src = "css/calendario.js"></script>
		<script type = "text/javascript" src = "css/programas.js"></script>
        <?php titlepag();?>
        <script>
            function actualizar()
            {
                document.form2.submit();
            }

            function validacodigo() {

                var codigo = document.getElementById('codigo').value;

                if (codigo.trim() != '') {
                    document.form2.oculto.value='3';
                    document.form2.submit();
                }
            }
            function cambioswitch(id,valor)
            {
                document.getElementById('idestado').value = id;
                if(valor == 1)
                {
                    despliegamodalm('visible','4','Desea Desactivar Variable de Nomina','1');
                }
                else
                {
                    despliegamodalm('visible','4','Desea activar Variable de Nomina','2');
                }
            }
            function respuestaconsulta(estado,pregunta)
            {
                if(estado == 'S')
                {
                    switch(pregunta)
                    {
                        case '1':	document.form2.cambioestado.value = '1';break;
                        case '2':	document.form2.cambioestado.value = '0';break;
                    }
                }
                else
                {
                    switch(pregunta)
                    {
                        case '1':	document.form2.nocambioestado.value = '1';break;
                        case '2':	document.form2.nocambioestado.value = '0';break;
                    }
                }
                document.form2.submit();
            }
        </script>
    </head>
    <body>
        <table>
            <tr>
                <script>
                    barra_imagenes("adm");
                </script>
                <?php
                    cuadro_titulos();
                ?>
            </tr>
            <tr>
                <?php
                    menu_desplegable("adm");
                ?>
            </tr>
            <tr>
                <td colspan="3" class="cinta">
                    <img src='imagenes/add.png' title='Nuevo' onClick="location.href='adm-parametros'" class="mgbt"/>
                    <img src="imagenes/guardad.png" class="mgbt1"/>
                    <img src="imagenes/busca.png" title="Buscar" onClick="location.href='adm-parametros'" class="mgbt"/>
                    <img src="imagenes/nv.png" title="Nueva Ventana" onClick="<?php echo paginasnuevas("adm");?>" class="mgbt">
                </td>
            </tr>
        </table>
        <div id="bgventanamodalm" class="bgventanamodalm">
            <div id="ventanamodalm" class="ventanamodalm">
                <IFRAME src="" name="ventanam" marginWidth=0 marginHeight=0 frameBorder=0 id="ventanam" frameSpacing=0 style=" width:700px; height:130px; top:200; overflow:hidden;"></IFRAME>
            </div>
        </div>
        <form name="form2" method="post" action="ccp-parametros.php">
			<div class="subpantalla" style="height:65.5%; width:99.6%;overflow-x:hidden" id="divdet">
				<?php
					//if($_POST[oculto])
						$crit1 = "";
						$sqlr = "SELECT * FROM admparametros";
						$resp = mysqli_query($linkbd,$sqlr);
						echo "
						<table class='inicio' align='center'>
							<tr>
								<td colspan='6' class='titulos'>.: Parametros PDF:</td>

							</tr>
							<tr>
								<td class='titulos2' style='width:5%;'>C&oacute;digo</td>
								<td class='titulos2' style='width:10%;'>Modulo</td>
								<td class='titulos2'>Nombre PDF</td>
                                <td class='titulos2' style='width:5%;'>Ver/Editar</td>
							</tr>";
						$iter = 'saludo1a';
						$iter2 = 'saludo2';
						$filas = 1;
						while ($row = mysqli_fetch_row($resp))
						{
							$idcta = $row[0];
							$numfil = $filas;

							echo"<tr class='$iter' onDblClick=\"verUltimaPos('$idcta', '$numfil')\" style='text-transform:uppercase'>
									<td class='icoop'>$row[1]</td>
									<td class='icoop'>$row[2]</td>
                                    <td class='icoop'>$row[3]</td>
                                    <td style='text-align:center;'>
                                        <a onClick=\"verUltimaPos($idcta, $numfil, $filtro)\" style='cursor:pointer;'>
                                            <img src='imagenes/b_edit.png' style='width:18px' title='Editar'>
                                        </a>
                                    </td>
								</tr>";
							$con += 1;
							$aux = $iter;
							$iter = $iter2;
							$iter2 = $aux;
							$filas++;
 						}
				?>
			</div>
		</form>
	</body>
</html>

