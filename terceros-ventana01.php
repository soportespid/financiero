<?php 
	require 'comun.inc';
	require 'funciones.inc';
	$linkbd = conectar_v7();
	$linkbd -> set_charset("utf8"); 	
	cargarcodigopag($_GET['codpag'],$_SESSION["nivel"]);
	header("Cache-control: private"); // Arregla IE 6
	date_default_timezone_set("America/Bogota");
	
?>
<!DOCTYPE >
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="es">
	<head>
    	<meta http-equiv="Content-Type" content="text/html" charset="utf8"/>
    	<meta http-equiv="X-UA-Compatible" content="IE=9"/>
		<title>:: Spid - Gestion Humana</title>
        <link href="css/css2.css" rel="stylesheet" type="text/css" />
        <link href="css/css3.css" rel="stylesheet" type="text/css" />
		<script type="text/javascript" src="css/programas.js"></script>
        <script type="text/javascript" src="css/calendario.js"></script>

		<script>
			var anterior;
			function ponprefijo(dato1,dato2,dato3,dato4,dato5,cedulanit)
			{ 
				parent.document.form2.terceros.value = cedulanit;
    			parent.document.form2.idcargoad.value = dato1;
				parent.document.form2.nomcargoad.value = dato2;
				parent.document.form2.cargo.value =dato3;
				parent.document.form2.nivsal.value =dato4;
				parent.document.form2.asigbas.value =dato5;
				parent.document.form2.asigbas2.value =formatNumber.new(dato5, "$ ");	
    			parent.document.form2.terceros.focus();
				parent.despliegamodal2("hidden");
			} 
		</script> 
		<?php titlepag();?>
	</head>
	<body>
  		<form action="" method="post" enctype="multipart/form-data" name="form2">
        	<?php if($_POST['oculto']==""){$_POST['numpos']=0;$_POST['numres']=10;$_POST['nummul']=0;}?>
			<table class="inicio" align="center">
      			<tr>
                    
                    <td class="titulos" colspan="6">:: Buscar Terceros</td>
                     
        			<td class="cerrar"><a onClick="parent.despliegamodal2('hidden');" href="#" >&nbsp;Cerrar</a></td>
      			</tr>
      			<tr>
        			<td class="saludo1">:: Nombre o Cedula:</td>
        			<td>
                        <input name="nombre" type="text" size="40" value="<?php echo @$_POST['nombre'] ?>" onkeydown = "if (event.keyCode == 13){document.getElementById('filtro').click()}";>
                    </td>

        			<td class="saludo1">:: Codigo:</td>
        			<td>
                    	<input name="codigo" type="text" id="codigo" value="<?php echo @$_POST['codigo'] ?>" onkeydown = "if (event.keyCode == 13){document.getElementById('filtro').click()}";>			      
                	</td>

					<td>
						<input type="button" name="bboton" id="filtro" onClick="limbusquedas();" value="&nbsp;&nbsp;Buscar&nbsp;&nbsp;" />
					</td>
       			</tr>                       
    		</table> 
            <input type="hidden" name="oculto" id="oculto" value="1">
            <input type="hidden" name="numres" id="numres" value="<?php echo $_POST['numres'];?>"/>
            <input type="hidden" name="numpos" id="numpos" value="<?php echo $_POST['numpos'];?>"/>
            <input type="hidden" name="nummul" id="nummul" value="<?php echo $_POST['nummul'];?>"/>

      		<?php
				if ($_POST['nombre'] != '') { // Buscar por nombre
                    
					$nombredividido = array();
					$nombredividido = explode(" ", $_POST['nombre']);
					
					for ($i=0; $i < count($nombredividido); $i++) 
					{ 
						$busqueda = '';
						$busqueda = "AND concat_ws(' ', nombre1, nombre2, apellido1, apellido2, razonsocial, cedulanit) LIKE '%$nombredividido[$i]%' ";

						$crit1 = $crit1 . $busqueda;
					}
                }

				if ($_POST['codigo'] != '') { // Buscar por id
                    $crit1 = "AND id_tercero LIKE '%".$_POST['codigo']."%' ";
                }

                // Codigo para paginación

                $queryString = "SELECT id_tercero, nombre1, nombre2, apellido1, apellido2, razonsocial, cedulanit FROM $_GET[table] WHERE estado = 'S' $crit1 ORDER BY id_tercero $cond2";
				
                $resp = mysqli_query($linkbd, $queryString);
                
                $_POST['numtop'] = mysqli_num_rows($resp);

				$nuncilumnas = ceil($_POST['numtop'] / $_POST['numres']);

				$cond2 = "";

				if (@ $_POST['numres'] != "-1")
				{
					$cond2 = " LIMIT ".$_POST['numpos'].", ".$_POST['numres']; 
				}
                
				$queryString = "SELECT id_tercero, nombre1, nombre2, apellido1, apellido2, razonsocial, cedulanit FROM $_GET[table] WHERE estado = 'S' $crit1 ORDER BY id_tercero $cond2";
                
                $resp = mysqli_query($linkbd, $queryString);
                
                $con = 1;
                
                $numcontrol = $_POST['nummul'] + 1;
                
                if($nuncilumnas == $numcontrol)
				{
					$imagenforward = '<img src="imagenes/forward02.png" style="width: 17px;">';
					$imagensforward = '<img src="imagenes/skip_forward02.png" style="width: 16px;">';
				}
				else 
				{
					$imagenforward="<img src='imagenes/forward01.png' style='width:17px' title='Siguiente' onClick='numsiguiente()'>";
					$imagensforward="<img src='imagenes/skip_forward01.png' style='width:16px' title='Fin' onClick='saltocol(\"$nuncilumnas\")'>";
				}
				if($_POST['numpos'] == 0)
				{
					$imagenback="<img src='imagenes/back02.png' style='width:17px'>";
					$imagensback="<img src='imagenes/skip_back02.png' style='width:16px'>";
				}
				else
				{
					$imagenback="<img src='imagenes/back01.png' style='width:17px' title='Anterior' onClick='numanterior();'>";
					$imagensback="<img src='imagenes/skip_back01.png' style='width:16px' title='Inicio' onClick='saltocol(\"1\")'>";
				}
				echo "
				<table class='inicio' style=\"width: 99%;\">
                    <tr>
                        <td colspan='2' class='titulos'>.: Resultados Busqueda:</td>
                        <td class='submenu' style=\"width: 10%;\">
                            <select name='renumres' id='renumres' onChange='cambionum();' style='width:100%'>
                                <option value='10'"; if ($_POST['renumres'] == '10') echo 'selected'; echo ">10</option>
                                <option value='20'"; if ($_POST['renumres'] == '20') echo 'selected'; echo ">20</option>
                                <option value='30'"; if ($_POST['renumres'] == '30') echo 'selected'; echo ">30</option>
                                <option value='50'"; if ($_POST['renumres'] == '50') echo 'selected'; echo ">50</option>
                                <option value='100'"; if ($_POST['renumres'] == '100') echo 'selected'; echo ">100</option>
								<option value='-1'"; if (@$_POST['renumres']=='-1'){echo 'selected';} echo ">Todos</option>
                            </select>
                        </td>
                    </tr>
                </table>

				<div class='subpantalla' style='height:78%;overflow-x:hidden;'>
					<table class='inicio' style='width: 99%; height:10%;'>
						<tr>
							<td class='titulos2' width='5%'>Codigo</td>
							<td class='titulos2' width='20%'>Razon Social</td>
							<td class='titulos2' width='15%'>Primer Nombre</td>
							<td class='titulos2' width='15%'>Segundo Nombre</td>
							<td class='titulos2' width='15%'>Primer Apellido</td>
							<td class='titulos2' width='15%'>Segundo Apellido</td>
							<td class='titulos2'>Cedula/Nit</td>
						</tr>";	
					$iter = 'saludo1a';
					$iter2 = 'saludo2';
					
					while ($row = mysqli_fetch_assoc($resp)) 
					{
						echo'
						<tr class="'.$iter.'" ondblclick="parent.respuestaModalBusqueda2(\''.$row['id_tercero'].'\', \''.$row['razonsocial'].'\',\''.$row['nombre1'].'\',\''.$row['nombre2'].'\',\''.$row['apellido1'].'\', \''.$row['apellido2'].'\',  \''.$row['cedulanit'].'\' );">
							<td>'.$row['id_tercero'].'</td>
							<td>'.$row['razonsocial'].'</td>
							<td>'.$row['nombre1'].'</td>
							<td>'.$row['nombre2'].'</td>
							<td>'.$row['apellido1'].'</td>
							<td>'.$row['apellido2'].'</td>
							<td>'.$row['cedulanit'].'</td>
						</tr>';
						
						$aux = $iter;
						$iter = $iter2;
						$iter2 = $aux;
					}
					
					if ($_POST['numtop'] == 0)
						{
							echo "
							<table class='inicio'>
								<tr>
									<td class='saludo1' style='text-align:center;width:100%'><img src='imagenes\alert.png' style='width:25px'>No hay coincidencias en la b&uacute;squeda $tibusqueda<img src='imagenes\alert.png' style='width:25px'></td>
								</tr>
							</table>";
						}
					echo"
					</table>

					<table class='inicio'>
							<tr>
								<td style='text-align:center;'>
									<a href='#'>$imagensback</a>&nbsp;
									<a href='#'>$imagenback</a>&nbsp;&nbsp;";
									
					if($nuncilumnas <= 9) {
						$numfin = $nuncilumnas;
					}
					else {
						$numfin = 9;
					}

					for($xx = 1; $xx <= $numfin; $xx++) {
						if($numcontrol <= 9) {
							$numx = $xx;
						}
						else {
							$numx = $xx + ($numcontrol - 9);
						}

						if($numcontrol == $numx) { 
							echo"<a href='#' onClick='saltocol(\"$numx\")'; style='color:#24D915'> $numx </a>";
						}
						else {
							echo"<a href='#' onClick='saltocol(\"$numx\")'; style='color:#000000'> $numx </a>";
						}
					}
					echo"			&nbsp;&nbsp;<a href='#'>$imagenforward</a>
									&nbsp;<a href='#'>$imagensforward</a>
								</td>
							</tr>
						</table>
				</div>";
				
			?>
            <input type="hidden" name="numtop" id="numtop" value="<?php echo $_POST['numtop'];?>" />
		</form>
	</body>
</html>
