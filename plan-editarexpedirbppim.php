<?php 
header("Cache-control: no-cache, no-store, must-revalidate");
header("Content-Type: text/html;charset=utf8");
require 'comun.inc';
require 'funciones.inc';
require "validaciones.inc";
require "conversor.php";
session_start();
$linkbd = conectar_v7();
$linkbd -> set_charset("utf8");
cargarcodigopag($_GET['codpag'],$_SESSION["nivel"]);
date_default_timezone_set("America/Bogota");
?>
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
        <meta http-equiv="Content-Type" content="text/html" />
		<!-- ETIQUETA QUE SIRVE PARA PONER ACENTOS A LAS PALABRAS, PARA NO USAR CARACTERES ESPECIALES ::RECICLAR CÓDIGO:: <meta charset="UTF-8"> -->
        <title>IDEAL 10 - Planeaci&oacute;n Estrat&eacute;gica</title>
        <link href="css/css2.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
        <link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
        <link href="css/tabs.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css"/>
		<link href="favicon.ico" rel="shortcut icon"/>
        <script type="text/javascript" src="css/programas.js"></script>
		<script src="JQuery/jquery-2.1.4.min.js"></script>
		<script type="text/javascript" src="css/calendario.js"></script>
        <script src="JQuery/autoNumeric-master/autoNumeric-min.js"></script>

		<script>
		jQuery(function($){  $('#valacti').autoNumeric('init');});
		jQuery(function($){ $('#aporconv').autoNumeric('init');});
		jQuery(function($){ $('#apormuni').autoNumeric('init');});
		
		
			
			function despliegamodalm(_valor,_tip,mensa)
			{
				document.getElementById("bgventanamodalm").style.visibility=_valor;
				if(_valor=="hidden"){document.getElementById('ventanam').src="";}
				else
				{
					var coding=document.getElementById('codigoproy').value;
					switch(_tip)
					{
					
						case "1":
							document.getElementById('ventanam').src="ventana-mensaje1.php?titulos=Se Guardo El Proyecto \""+coding+"\" con Exito";break;
						case "2":
							document.getElementById('ventanam').src="ventana-mensaje2.php?titulos="+mensa;break;
						case "3":
							document.getElementById('ventanam').src="ventana-mensaje2.php?titulos="+mensa;break;
						case "4":
							document.getElementById('ventanam').src="ventana-consulta1.php?titulos="+mensa+"&idresp=1";break;
						case "5":
							document.getElementById('ventanam').src="ventana-mensaje1.php?titulos="+mensa;break;
					}
				}
			}
			
			function despliegamodalm3(_valor,_tipo,_nomb)
			{
				document.getElementById("bgventanamodalm").style.visibility=_valor;
				if(_valor=="hidden"){document.getElementById('ventanam').src="";}
				else
				{
					var coding=document.getElementById('codigoproy').value;
					var vigen=document.getElementById('vigencia').value;
					switch(_tipo)
					{
						case 1:
							document.getElementById('ventanam').src="ventana-mensaje1.php?titulos=Se Guard\xf3; El Proyecto \""+coding+"\" con Exito";break;
						case 2:
							document.getElementById('ventanam').src="ventana-mensaje2.php?titulos=Ya se ingres\xf3 el c\xf3digo \""+coding+"\" de la vigencia "+vigen;break;
						case 3:
							document.getElementById('ventanam').src="ventana-mensaje2.php?titulos=Ya se ingres\xf3 un Archivo con el nombre \""+_nomb+"\"";break;
					}
						
				}
			}
			function respuestaconsulta(pregunta)
			{
				switch(pregunta)
				{
					case "1":	document.form2.oculto.value="2";
								document.form2.submit();
								break;
								
				}
			}
			function funcionmensaje()
			{
				document.location.href = "plan-editarexpedirbppim.php?id="+document.getElementById('solproyecod').value;
			}
			function agregarubro()
			{
				if(document.getElementById('myonoffswitch').value==1)
				{
					if(document.form2.codrubro.value!="" &&  document.form2.fuente.value!="" && parseFloat(document.form2.valor.value) >0 && document.form2.valor.value !="")
					{ 
						if(parseFloat(document.getElementById('saldo').value)>=parseFloat(document.getElementById('valor').value))
						{
							document.form2.agregadet2.value=1;
							document.form2.submit();
						}
						else {despliegamodalm4('visible','2','La Cuenta "'+document.getElementById('codrubro').value+'" no tiene saldo suficiente');}
					}
					else {despliegamodalm4('visible','2','Falta informacion para poder Agregar');}
				}
			}

			function agregafuente(){
				if(document.getElementById('myonoffswitch').value!=1)
				{
					if( document.form2.ffinciacion.value!="" && parseFloat(document.form2.valor.value) >0 && document.form2.valor.value !="")
					{ 
					
						document.form2.agregadet8.value=1;
						document.form2.submit();
						
					
				}else {despliegamodalm4('visible','2','Falta informacion para poder Agregar');}
			}
		}
			
			function despliegamodalm4(_valor,_tip,mensa)
			{
				document.getElementById("bgventanamodalm").style.visibility=_valor;
				if(_valor=="hidden"){document.getElementById('ventanam').src="";}
				else
				{
					switch(_tip)
					{
						case "1":
							document.getElementById('ventanam').src="ventana-mensaje1.php?titulos=Se Guardo la solicitud de Adqusici\xf3n con Exito";break;
						case "2":
							document.getElementById('ventanam').src="ventana-mensaje3.php?titulos="+mensa;break;
						case "3":
							document.getElementById('ventanam').src="ventana-mensaje2.php?titulos="+mensa;break;	
						case "4":
							document.getElementById('ventanam').src="ventana-mensaje7.php?titulos="+mensa;break;
					}
				}
			}
			
			
			function pdf(){
				document.form2.action="pdfcertificabanco.php";
				document.form2.target="_blank";
				document.form2.submit();
				document.form2.action="";
				document.form2.target="";
			}
			function direcciona(){
				var nombre=document.form2.nomarchadj.value;
				window.location.href='informacion/proyectos/temp/'+nombre ;
			}
		</script>
	</head>
	<body>
        <IFRAME src="alertas.php" name="alertas" id="alertas" style="display:none"></IFRAME>
        <span id="todastablas2"></span>
        <div id="bgventanamodalm" class="bgventanamodalm">
            <div id="ventanamodalm" class="ventanamodalm">
                <IFRAME src="" name="ventanam" marginWidth=0 marginHeight=0 frameBorder=0 id="ventanam" frameSpacing=0 style=" width:700px; height:130px; top:200; overflow:hidden;"> 
                </IFRAME>
            </div>
        </div>
		<table>
			<tr><script>barra_imagenes("plan");</script><?php cuadro_titulos();?></tr>	 
			<tr><?php menu_desplegable("plan");?></tr>
        	<tr>
  				<td colspan="3" class="cinta"><a href="plan-expedirbppim.php" class="mgbt"><img src="imagenes/add.png" title="Nuevo" border="0" /></a><a href="#" class="mgbt" onClick="guardar()"><img src="imagenes/guarda.png" title="Guardar"/></a><a href="plan-buscarexpedirbppim.php" class="mgbt"><img src="imagenes/busca.png" title="Buscar" border="0"/></a><a href="#" onClick="mypop=window.open('plan-principal.php','',''); mypop.focus();" class="mgbt"><img src="imagenes/nv.png" title="Nueva Ventana"></a><a href="#" onClick="pdf()" class="mgbt"><img src="imagenes/print.png" title="Imprimir" /></a><a href="plan-buscarexpedirbppim.php"  class="mgbt"><img src="imagenes/iratras.png" title="Atr&aacute;s"></a></td>
			</tr>
     	</table>
 		<form name="form2" method="post" enctype="multipart/form-data" >
		<?php
			
    		$vigusu=vigencia_usuarios($_SESSION['cedulausu']); 
  			$linkbd=conectar_v7(); 

			if(isset($_GET['id'])) {
				$_POST['autoincre']=$_GET['id'];
			}
								
  			if($_POST['oculto']==""){

				$_POST['tabgroup1'] = 1;

				$sql = "SELECT codproyecto, vigencia, val_actividad, apor_convenio, apor_municipio, observaciones, fecha 
				FROM contrasolicitudproyecto 
				WHERE codigo = $_POST[autoincre]";
				$res = mysqli_query($linkbd,$sql);
				$row = mysqli_fetch_row($res);

				$sqlCab = "SELECT nombre, descripcion, id FROM ccpproyectospresupuesto WHERE codigo = '$row[0]' AND vigencia = '$row[1]'";
				$rowCab = mysqli_fetch_row(mysqli_query($linkbd, $sqlCab));

				$fechaComoEntero = strtotime($row[6]);
				$_POST['fecha'] = date("d/m/Y", $fechaComoEntero);
				$_POST['valactiu'] = $_POST['valacti'] = $row[2];
				$_POST['aporconvu'] = $_POST['aporconv'] = $row[3];
				$_POST['apormuniu'] = $_POST['apormuni'] = $row[4];
				$_POST['observa'] = $row[5];
				$_POST['codigoproy'] = $row[0];
				$_POST['vigencia'] = $row[1];
				$_POST['nombre'] = $rowCab[0];
				$_POST['valorproyecto'] = $row[2];
				$_POST['descripcion'] = $rowCab[1];

				$sqlProyectoProductos = "SELECT sector, programa, subprograma, producto, indicador FROM ccpproyectospresupuesto_productos WHERE codproyecto = $rowCab[2]";
				$resProyectoProductos = mysqli_query($linkbd, $sqlProyectoProductos);
				while ($rowProyectosProductos = mysqli_fetch_row($resProyectoProductos)) {
					
					$_POST['fuentes'][] = $rowProyectosProductos[0];
					$_POST['ccs'][] = $rowProyectosProductos[1];

					$sqlSector = "SELECT nombre FROM ccpetsectores WHERE codigo = '$rowProyectosProductos[0]' AND version = (SELECT MAX(version) FROM ccpetsectores)";
					$rowSector = mysqli_fetch_row(mysqli_query($linkbd, $sqlSector));

					$_POST['sectores'][] = $rowProyectosProductos[0] . " - " . $rowSector[0];

					$sqlPrograma = "SELECT nombre FROM ccpetprogramas WHERE codigo = '$rowProyectosProductos[1]' AND version = (SELECT MAX(version) FROM ccpetprogramas)";
					$rowPrograma = mysqli_fetch_row(mysqli_query($linkbd, $sqlPrograma));

					$_POST['programas'][] = $rowProyectosProductos[1] . " - " . $rowPrograma[0];

					$sqlSubPrograma = "SELECT nombre_subprograma FROM ccpetprogramas WHERE codigo_subprograma = '$rowProyectosProductos[2]' AND version = (SELECT MAX(version) FROM ccpetprogramas)";
					$rowSubPrograma = mysqli_fetch_row(mysqli_query($linkbd, $sqlSubPrograma));

					$_POST['subProgramas'][] = $rowProyectosProductos[2] . " - " . $rowSubPrograma[0];

					$sqlProducto = "SELECT producto FROM ccpetproductos WHERE cod_producto = '$rowProyectosProductos[3]' AND version = (SELECT MAX(version) FROM ccpetproductos)";
					$rowProducto = mysqli_fetch_row(mysqli_query($linkbd, $sqlProducto));

					$_POST['productos'][] = $rowProyectosProductos[3] . " - " . $rowProducto[0];

					$sqlIndicadorProducto = "SELECT indicador_producto FROM ccpetproductos WHERE codigo_indicador = '$rowProyectosProductos[4]' AND version = (SELECT MAX(version) FROM ccpetproductos)";
					$rowIndicadorProducto = mysqli_fetch_row(mysqli_query($linkbd, $sqlIndicadorProducto));

					$_POST['indicadorProductos'][] = $rowProyectosProductos[4] . " - " . $rowIndicadorProducto[0];
				}
  			}

			switch($_POST['tabgroup1'])
			{
				case 1:
					$check1='checked';break;
				case 2:
					$check2='checked';break;
				case 3:
					$check3='checked';break;
				case 4:
					$check4='checked';break;
			}
 		?>
        <div class="tabsmeci"  style="height:76.5%; width:99.6%">
                <div class="tab">
                    <input type="radio" id="tab-1" name="tabgroup1" value="1" <?php echo $check1;?> >
                    <label for="tab-1">Proyecto</label>
                    <div class="content" style="overflow:hidden;">
				
					<table class="inicio">
						<tr>
							<td class="titulos" colspan="8">Certificar BPPIM</td>	
						</tr>
						<tr>
							<td class="saludo1" style="width:5%">Fecha:</td>
							<td width="6%">
								<input name="fecha" type="text" id="fc_1198971545" title="DD/MM/YYYY" value="<?php echo $_POST['fecha']; ?>" readonly/>
							</td>

							<td class="saludo1" style="width:5%">Solicitud:</td>
							<td colspan="4" width="50%">
								<select name="solproyecod" id="solproyecod" onChange="cargarproyecto(this)" style="width: 90%" disabled>
									<option value='' >Seleccione...</option>
									<?php
									$sql="SELECT csp.codigo,csp.codsolicitud,csp.codproyecto,csp.descripcion from contrasolicitudproyecto csp,contrasoladquisiciones cs WHERE csp.vigencia='$vigusu' AND csp.codsolicitud=cs.codsolicitud";
									$res=mysqli_query($linkbd,$sql);
									while($row = mysqli_fetch_row($res)){
										if($_POST['solproyecod']==$row[0]){
											echo "<option value='$row[0]' SELECTED>$row[1] - ".substr($row[3],0,90)."...</option>";
											$_POST['codigo']=$row[2];
											$_POST['codsol']=$row[1];
											$_POST['codigot']=$row[1];
										}else{
											echo "<option value='$row[0]'>$row[1] - ".substr($row[3],0,90)."...</option>";
										}
										
									}
									echo "<input type='hidden' name='codsol' id='codsol' value='$_POST[codsol]' />";
									?>
								</select>

								<span style="text-decoration: underline; cursor:pointer"><b><a <?php if(!empty($_POST['codsol'])){echo "href='informacion/proyectos/temp/solicitudbanco$_POST[codsol].pdf' target='_blank' ";}  ?> >VER</a></b></span>
							</td>
							<td rowspan="2" width="10%" style="border: 1px dashed gray">
								<div style="display:inline-block;"><label style="background-color: white !important">Liberar:</label></div>
								<div class="c1" style="display:inline-block"><input type="checkbox" id="finaliza" name="finaliza"  onChange="validafinalizar(this)" <?php if(isset($_POST['finaliza'])){echo "checked";} ?> value="<?php echo $_POST['finaliza']?>"/><label for="finaliza" id="t1" ></label></div>
							</td>	
						</tr>

						<tr>
							<td class="saludo1" style="width:5%">Valor Solicitud:</td>
							<td style="width:6%">
								<input type="hidden" name="valactiu" id="valactiu" value="<?php echo $_POST['valactiu']; ?>"/>
								<input type="text" name="valacti" id="valacti" value="<?php echo $_POST['valacti']; ?>" style="width:100%;text-align:right;" data-a-sign="$" data-a-dec="," data-a-sep="." data-v-min="0" onKeyUp="sinpuntitos('valactiu','valacti');" readonly/>
							</td>

							<td class="saludo1" style="width:5%">Aporte Convenio:</td>
							<td style="width:10%">
								<input type="hidden" name="aporconvu" id="aporconvu" value="<?php if(!isset($_POST['aporconvu'])) {$_POST['aporconvu'] = 0;} echo $_POST['aporconvu']; ?>"/>
								<input type="text" name="aporconv" id="aporconv" value="<?php echo $_POST['aporconv']; ?>" style="width:100%;text-align:right;" data-a-sign="$" data-a-dec="," data-a-sep="." data-v-min="0" onKeyUp="sinpuntitos('aporconvu','aporconv');" readonly/>
							</td>

							<td class="saludo1" style="width:5%">Aporte Municipio:</td>
							<td style="width:10%">
								<input type="hidden" name="apormuniu" id="apormuniu" value="<?php if(!isset($_POST['apormuniu'])) {$_POST['apormuniu'] = 0;} echo $_POST['apormuniu']; ?>"/>
								<input type="text" name="apormuni" id="apormuni" value="<?php echo $_POST['apormuni']; ?>" style="width:100%;text-align:right;" data-a-sign="$" data-a-dec="," data-a-sep="." data-v-min="0" onKeyUp="sinpuntitos('apormuniu','apormuni');" readonly/>
							</td>
						</tr>

						<tr>
							<td class="saludo1" style="width:5%">Observaciones:</td>
							<td colspan="6">
								<input name="observa" type="text" id="observa"  value="<?php echo $_POST['observa']; ?>" style="width: 100%;text-align:left;height: 40px" readonly>
							</td>
						</tr>
					</table>

					<table class="inicio" >
						<tr>
							<td class="titulos" colspan="10" >Asignacion de Proyecto</td>

						</tr>
						<tr>
							<td class="saludo1" style="width:7%">Codigo: </td>
							<td style="width:20%">
								<input type="text" name="codigoproy" id="codigoproy" value="<?php echo $_POST['codigoproy']?>" style="width:92%; text-align:center;" readonly>
							</td>

							<td class="saludo1" style="width:7%">Vigencia:</td>
							<td style="width:7%">
								<input type="text" name="vigencia" id="vigencia" value="<?php echo $_POST['vigencia']?>" style="width:98%; text-align:center;" readonly>
							</td>

							<td class="saludo1" style="width:8%">Archivo Adjunto:</td>
							<td style="width:42.5%" colspan="4">
								<input type="text" name="nomarchadj" id="nomarchadj"  style="width:95%;text-align: right;" value="<?php echo $_POST['nomarchadj']?>" readonly><img <?php if(!empty($_POST['nomarchadj'])){echo "src='imagenes/descargar.png' onClick='redireccion()' ";  }else{echo "src='imagenes/descargard.png' ";}; ?> title="Descargar" style="cursor:pointer !important" />
							</td>

							<td rowspan="3" width="10%" style="background-image: url('imagenes/proyecto.png'); background-repeat: no-repeat; background-position:center center;background-size: 75px 75px"></td>
						</tr>
						
						<tr>
							<td class="saludo1">Nombre:</td>
							<td colspan="3">
								<input type="text" name="nombre" id="nombre" value="<?php echo $_POST['nombre']?>" style="width:100%;text-transform: uppercase;" readonly> 
							</td>
							<td class="saludo1">Valor del proyecto:</td>
							<td>
	
								<script>jQuery(function($){ $('#valorproyecto').autoNumeric('init');});</script>
								<input type="hidden" name="valorproyectosinformato" id="valorproyectosinformato" value="<?php echo $_POST['valorproyectosinformato']?>"   />
								<input type="text" id="valorproyecto" name="valorproyecto"  value="<?php echo $_POST['valorproyecto']?>" data-a-sign="$" data-a-dec="," data-a-sep="." data-v-min='0' onKeyUp="sinpuntitos('valorp','valorproyecto');return tabular(event,this);" style="width:100%; text-align:right;" autocomplete="off" readonly>
								<input type="hidden" name="saldo" id="saldo" onKeyPress="javascript:return solonumeros(event)" onKeyUp="return tabular(event,this)" value="<?php echo $_POST['saldo']?>" > 

							</td>
							<input type="hidden" name="banderin1" id="banderin1" value="<?php echo $_POST['banderin1'];?>" >
							<input type="hidden" name="contador" id="contador" value="<?php echo $_POST['contador'];?>" >
							<input type="hidden" name="contadorsol" id="contadorsol" value="<?php echo $_POST['contadorsol'];?>" >
							<input type="hidden" name="bloqueartodo" id="bloqueartodo" value = "<?php echo $_POST['bloqueartodo'];?>"  >
						</tr>
						<tr>
							<td class="saludo1">Descripci&oacute;n:</td>
							<td colspan="6">
								<input type="text" name="descripcion" id="descripcion" value="<?php echo $_POST['descripcion']?>" style="width:100%;text-transform: uppercase;" readonly> 
							</td>
							
							
						</tr>
						
						
					</table>


							<?php
							 echo"
                                <div class='subpantalla' style='height:50%; width:99.5%; margin-top:0px; overflow-x:hidden'>
                                        <table class='inicio' width='99%'>
                                            <tr>
                                                <td class='titulos' colspan='6'>Detalle Proyecto</td>
                                            </tr>
											<tr>									
												<td class='titulos2' style='width: 18% !important'>Sector</td>
												<td class='titulos2' style='width: 18% !important'>Programa</td>
												<td class='titulos2' style='width: 18% !important'>SubPrograma</td>
												<td class='titulos2' style='width: 18% !important'>Producto</td>
												<td class='titulos2' style='width: 18% !important'>Indicador producto</td>
											</tr>";
                                
											$itern='saludo1a';
											$iter2n='saludo2';

											for($x=0;$x<count($_POST['sectores']); $x++){
									
												$sector = $_POST['sectores'][$x];
												$programa = $_POST['programas'][$x];
												$subPrograma = $_POST['subProgramas'][$x];
												$producto = $_POST['productos'][$x];
												$indicadorProducto = $_POST['indicadorProductos'][$x];
			
												echo "
													<tr class='$itern'>
														<td>
															$sector
															<input type='hidden' name='sectores[]' value='{$_POST['sectores'][$x]}' readonly>
														</td>
														<td>
															$programa
															<input type='hidden' name='programas[]' value='{$_POST['programas'][$x]}' readonly>
														</td>
														<td>
															$subPrograma
															<input type='hidden' name='subProgramas[]' value='{$_POST['subProgramas'][$x]}' readonly>
														</td>
														<td>
															$producto
															<input type='hidden' name='productos[]' value='{$_POST['productos'][$x]}' readonly>
														</td>
														<td>
															$indicadorProducto
															<input type='hidden' name='indicadorProductos[]' value='{$_POST['indicadorProductos'][$x]}' readonly>
														</td>
													<tr>
												";
											
												$auxn=$itern;
												$itern=$itern2;
												$itern2=$auxn;
											}
									
								echo "</tr>";
                                
                                echo "
                                    </table></div>";
							?>
              		</div>
                </div>
               <div class="tab">
                    <input type="radio" id="tab-3" name="tabgroup1" value="3" <?php echo $check3; ?> >
                    <label for="tab-3">Anexos</label>
                    <div class="content" style="overflow:hidden;">
                        <table class="inicio" >
                            <tr>
                                <td class="titulos" colspan="6" >Subir Anexos</td>
                                <td class="cerrar" style="width:7%;"><a onClick="location.href='plan-principal.php'">&nbsp;Cerrar</a></td>
                            </tr>
                            <tr>
                                <td class="saludo1" style="width:8%">Archivo Principal:</td>
                                <td style="width:25%" ><input type="text" name="nomarch" id="nomarch"  style="width:100%;" value="<?php echo $_POST['nomarch']?>" readonly> </td>
                                    <td style="width:3%">
                                    	
                                    </td>
            					<td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>
                           
                        </table>
                        <?php
                        	 echo"
                                <div class='subpantalla' style='height:46.5%; width:99.5%; margin-top:0px; overflow-x:hidden'>
                                        <table class='inicio' width='99%'>
                                            <tr>
                                                <td class='titulos' colspan='5'>Detalle Adjuntos</td>
                                            </tr>
                                            <tr>
                                                <td class='titulos2'>Nombre</td>
                                                <td class='titulos2'>Ruta</td>
                                                <td class='titulos2'>".utf8_decode("Tamaño")."</td>
                                                <td class='titulos2'></td>
                                         
                                            </tr>";
                                if ($_POST['eliminarc']!='')
                                { 
                                    $posi=$_POST['eliminarc'];
                                    unset($_POST['nomarchivos'][$posi]);
                                    unset($_POST['rutarchivos'][$posi]);
                                    unset($_POST['tamarchivos'][$posi]);
                                    unset($_POST['patharchivos'][$posi]);	 		 
                                    $_POST['nomarchivos']= array_values($_POST['nomarchivos']); 
                                    $_POST['rutarchivos']= array_values($_POST['rutarchivos']); 
                                    $_POST['tamarchivos']= array_values($_POST['tamarchivos']); 
                                    $_POST['patharchivos']= array_values($_POST['patharchivos']); 	
                                    $_POST['eliminarc']='';	 		 		 		 
                                }	 
                                if ($_POST['agregadet3']=='1')
                                {
                                    $ch=esta_en_array($_POST['nomarchivos'],$_POST['nomarchivo']);
                                    if($ch!='1')
                                    {			 
                                        $_POST['nomarchivos'][]=$_POST['nomarchivo'];
                                        $_POST['rutarchivos'][]=$_POST['rutarchivo'];
                                        $_POST['tamarchivos'][]=$_POST['tamarchivo'];
                                        $_POST['patharchivos'][]=$_POST['patharchivo'];
                                        $_POST['agregadet3']=0;
                                        echo"
                                        <script>	
                                            document.form2.nomarchivo.value='';
                                            document.form2.rutarchivo.value='';
                                            document.form2.tamarchivo.value='';
                                        </script>";
                                    }
                                    else {echo"<script>parent.despliegamodalm('visible','2','Ya se Ingreso el Archivo  $_POST[nomarchivo]');</script>";}
                                }
                                $itern='saludo1a';
                                $iter2n='saludo2';
                                for ($x=0;$_POST['nomarchivos'] != null && $x<count($_POST['nomarchivos']);$x++)
                                {
                                	$rutaarchivo="informacion/proyectos/temp/".$_POST['patharchivos'][$x];
                                    echo "
                                    <input type='hidden' name='nomarchivos[]' value='".$_POST['nomarchivos'][$x]."'/>
                                    <input type='hidden' name='rutarchivos[]' value='".$_POST['rutarchivos'][$x]."'/>
                                    <input type='hidden' name='tamarchivos[]' value='".$_POST['tamarchivos'][$x]."'/>
                                    <input type='hidden' name='patharchivos[]' value='".$_POST['patharchivos'][$x]."'/>
                                        <tr class='$itern'>
                                            <td>".$_POST['nomarchivos'][$x]."</td>
                                            <td>".$_POST['rutarchivos'][$x]."</td>
                                            <td>".$_POST['tamarchivos'][$x]." Bytes</td>
                                            <td style='text-align:center;width: 30px'><a href='$rutaarchivo' target='_blank' ><img src='imagenes/descargar.png'  title='(Descargar)' ></a></td>
                                        
                                           
                                        </tr>";
                                    $auxn=$itern;
                                    $itern=$itern2;
                                    $itern2=$auxn;
                                }
                                echo "
                                    </table></div>";
                         ?>
              		</div>
                </div>
     	</div>
        
    	    <input type="hidden" name="oculto" id="oculto" value="1">
    		<input type="hidden" name="vigencia" id="vigencia" value="<?php echo $_POST['vigencia'];?>">
        	<input type="hidden" name="oculgen" id="oculgen" value="<?php echo $_POST['oculgen'];?>">
        	<input type="hidden" name="indindex" id="indindex" value="<?php echo $_POST['indindex'];?>">
           	<input type="hidden" name="codid" id="codid" value="<?php echo $_POST['codid'];?>">
            <input type="hidden" name="pesactiva" id="pesactiva" value="<?php echo $_POST['pesactiva'];?>">
            <input type="hidden" name="busadq" id="busadq" value="0">
         	<input type="hidden" name="bctercero" id="bctercero" value="0">
           	<input type="hidden" name="agregadets" id="agregadets" value="0">
            <input type='hidden' name="eliminars" id="eliminars" >
            <input type="hidden" name="bc" value="0">
            <input type="hidden" name="bcproyectos" value="0" >
			<input type="hidden" name="agregadet7" value="0">
            <input type="hidden" name="agregadet2" value="0">
            <input type="hidden" name="agregadet8" value="0">
            <input type="hidden" name="agregadet3" value="0">
            <input type="hidden" name="agregadet" value="0"> 
            <input type="hidden" name="agregadetadq" value="0">
            <input type='hidden' name='eliminar' id='eliminar'>
            <input type='hidden' name='autoincre' id='autoincre' value="<?php echo $_POST['autoincre']; ?>">
			<input type="hidden" name="esliberado" id="esliberado" value="<?php echo $_POST['esliberado']; ?>">
			<input type="hidden" name="contadorcert" id="contadorcert" value="<?php echo $_POST['contadorcert'];?>" >
			<input type="hidden" name="niveles" id="niveles" value="<?php echo $_POST['niveles'];?>" >
			<input type="hidden" name="codigot" id="codigot" value="<?php echo $_POST['codigot'];?>" >
			
 		<?php  
			//********guardar
		 	if($_POST['oculto']=="2")
			{
				$aceptar=$_POST['finaliza'];
				//$rechazar=$_POST[finaliza2];
				if(!empty($_POST['fecha'])){
						$pos=strpos($_POST['fecha'],"-");
						if($pos===false){
							preg_match( "([0-9]{1,2})/([0-9]{1,2})/([0-9]{2,4})", $_POST['fecha'],$fecha);
							$fechaf=$fecha[3]."-".$fecha[2]."-".$fecha[1];
						}else{
							$fechaf=$_POST['fecha'];
						}
					
					}else{
						$fechaf="";
					}
				$sql="SELECT codsolicitud FROM contrasolicitudproyecto WHERE codigo='$_POST[solproyecod]' ";
				$res=mysqli_query($linkbd,$sql);
				$row = mysqli_fetch_row($res);
				$soli=$row[0];
				$aporconvenio=limpiarnum($_POST['aporconv']);
				$apormunicipio=limpiarnum($_POST['apormuni']);
				$valoracti=limpiarnum($_POST['valacti']);
				$metascert="";
				for($x=0;$x<$_POST['contadorsol']; $x++){
					if($_POST['aceptab'][$x]){
						$meta=$_POST["matmetas$x"][$_POST['niveles']-1];
						$metascert.=($meta."-");
						
					  }
				}
				$metascert=substr($metascert,0,-1);	
				
				$sql="UPDATE contrasolicitudproyecto SET fecha_certi='$fechaf', val_actividad=$valoracti,apor_convenio=$aporconvenio,apor_municipio=$apormunicipio,observaciones='$_POST[observa]',codproyecto='$_POST[codigoproy]',metascert='$metascert' WHERE codigo=$_POST[solproyecod]";
				mysqli_query($linkbd,$sql);
				$sql="UPDATE contrasoladquisicionesgastos SET codproyecto=$_POST[codigoproy] WHERE codsolicitud='$soli'  ";
				mysqli_query($linkbd,$sql);
				$sql="UPDATE contrasolicitudcdp_det SET conproyecto=$_POST[codigoproy] WHERE proceso='$soli'  ";
				mysqli_query($linkbd,$sql);
				
				if(isset($aceptar)){
					$sql="UPDATE contrasolicitudproyecto SET estado='CE' WHERE codigo=$_POST[solproyecod]";
					mysqli_query($linkbd,$sql);
					
					$sql="UPDATE planproyectos SET estado='A' WHERE codigo='$_POST[codigoproy]'  ";
					mysqli_query($linkbd,$sql);
					
				}else{
					$sql="UPDATE contrasolicitudproyecto SET estado='A' WHERE codigo=$_POST[solproyecod]";
					mysqli_query($linkbd,$sql);
					
					$sql="UPDATE planproyectos SET estado='S' WHERE codigo='$_POST[codigoproy]'  ";
					mysqli_query($linkbd,$sql);
				}
				
				echo "<script>despliegamodalm('visible','5','Certificado Generado con Exito');</script>";
					

			}
	
			
			
		 ?>
		 </div>
		 <div id="bgventanamodal2">
            <div id="ventanamodal2">
                <IFRAME  src="" name="buster" marginWidth=0 marginHeight=0 frameBorder=0 id="ventana2" frameSpacing=0 style="left:500px; width:900px; height:500px; top:200;"> 
                </IFRAME>
            </div>
        </div>
	 	</form>       
	</body>
</html>