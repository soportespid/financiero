<?php
    header("Content-Type: text/html;charset=utf-8");
    require_once 'PHPExcel/Classes/PHPExcel.php';
	require_once 'comun.inc';
    require_once 'funciones.inc';
    require_once 'funcionesSP.inc.php';
    session_start();
	$linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");

    $out = array('error' => false);

    $maxVersion = ultimaVersionGastosCCPET();


    if($_GET){
        $contratos = array();

        $critFecha = '';
        $critNumContrato = '';

        if($_GET['fechaInicial'] != '' && $_GET['fechaFinal'] != ''){
            $critFecha = " AND fecha BETWEEN '".$_GET['fechaInicial']."' AND '".$_GET['fechaFinal']."'";
        }

        $sqlr = "SELECT TB1.consvigencia, TB1.detalle, TB1.idcdp, TB1.fecha, TB1.tercero, TB1.contrato, TB2.cuenta, TB2.indicador_producto, TB2.bpim, TB2.fuente, TB2.seccion_presupuestal, TB2.codigo_vigenciag, TB2.medio_pago, SUM(TB2.valor), TB1.vigencia FROM ccpetrp AS TB1, ccpetrp_detalle AS TB2 WHERE TB1.vigencia = TB2.vigencia AND TB1.consvigencia = TB2.consvigencia $critFecha AND TB1.tipo_mov = TB2.tipo_mov AND TB1.tipo_mov = '201' GROUP BY TB1.consvigencia, TB2.cuenta, TB2.indicador_producto, TB2.bpim, TB2.fuente, TB2.seccion_presupuestal, TB2.codigo_vigenciag, TB2.medio_pago";
        $res = mysqli_query($linkbd, $sqlr);
        while($row = mysqli_fetch_assoc($res)){
            $valorRp = 0;
            $saldoPorEjecu = 0;
            $rubro = '';
            $nombreRubro = '';
            $tercero = '';

            $sqlrRev = "SELECT SUM(valor) FROM ccpetrp_detalle WHERE cuenta = '$row[cuenta]' AND indicador_producto = '$row[indicador_producto]' AND bpim = '$row[bpim]' AND fuente = '$row[fuente]' AND seccion_presupuestal = '$row[seccion_presupuestal]' AND codigo_vigenciag = '$row[codigo_vigenciag]' AND medio_pago = '$row[medio_pago]' AND consvigencia = '$row[consvigencia]' AND vigencia = '$row[vigencia]' AND SUBSTRING(tipo_mov, 1, 1) LIKE '4%'";
            $resRev = mysqli_query($linkbd, $sqlrRev);
            $rowRev = mysqli_fetch_assoc($resRev);

            $valorRp = $row['SUM(TB2.valor)'] - $rowRev['SUM(valor)'];


            if(substr($row['cuenta'],0,3) != '2.3'){
                $rubro = $row['cuenta'];
                $nombreRubro = buscacuentaccpetgastos($row['cuenta'], $maxVersion);
            }else{
                $rubro = $row['indicador_producto'].' - '.$row['bpim'].' - '.$row['fuente'].' - '.$row['seccion_presupuestal'].' - '.$row['codigo_vigenciag'].' - '.$row['medio_pago'];
                $nombreRubro = buscaindicadorccpet($row['indicador_producto']).' - '.buescarNombreProyecto($row['bpim'], $row['vigencia']).' - '.buscafuenteccpet($row['fuente']).' - '.buescarNombreVigenciaGasto($row['codigo_vigenciag']);
            }

            $tercero = $row['tercero'].' - '.buscatercero($row['tercero']);

            $det = array();
            array_push($det, $rubro);
            array_push($det, $nombreRubro);
            array_push($det, $row['fuente']);
            array_push($det, buscafuenteccpet($row['fuente']));
            array_push($det, $row['consvigencia']);
            array_push($det, $row['idcdp']);
            array_push($det, $row['fecha']);
            array_push($det, $tercero);
            array_push($det, $row['contrato']);
            array_push($det, $row['detalle']);
            array_push($det, $valorRp);

            array_push($contratos, $det);

            /* array_push($contratos, $row); */
        }
        $request = $contratos;
        $objPHPExcel = new PHPExcel();
        $objPHPExcel->getActiveSheet()->getStyle('A:K')->applyFromArray(
            array(
                'alignment' => array(
                    'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
                    'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER
                )
            )
        );
        $objPHPExcel->getProperties()
        ->setCreator("IDEAL 10")
        ->setLastModifiedBy("IDEAL 10")
        ->setTitle("Exportar Excel con PHP")
        ->setSubject("Documento de prueba")
        ->setDescription("Documento generado con PHPExcel")
        ->setKeywords("usuarios phpexcel")
        ->setCategory("reportes");

        //----Cuerpo de Documento----
        $objPHPExcel->setActiveSheetIndex(0)
        ->mergeCells('A1:K1')
        ->mergeCells('A2:K2')
        ->setCellValue('A1', 'PRESUPUESTO')
        ->setCellValue('A2', 'REPORTE DE CONTRATOS SALDOS RP POR RUBRO');
        $objPHPExcel-> getActiveSheet ()
        -> getStyle ("A1")
        -> getFill ()
        -> setFillType (PHPExcel_Style_Fill :: FILL_SOLID)
        -> getStartColor ()
        -> setRGB ('C8C8C8');
        $objPHPExcel-> getActiveSheet ()
        -> getStyle ("A1:A2")
        -> getFont ()
        -> setBold ( true )
        -> setName ( 'Verdana' )
        -> setSize ( 10 )
        -> getColor ()
        -> setRGB ('000000');
        $objPHPExcel-> getActiveSheet ()
        -> getStyle ('A1:A2')
        -> getAlignment ()
        -> applyFromArray (array ( 'horizontal'  =>  PHPExcel_Style_Alignment :: HORIZONTAL_CENTER ,) );
        $objPHPExcel-> getActiveSheet ()
        -> getStyle ('A3:K3')
        -> getAlignment ()
        -> applyFromArray (array ( 'horizontal'  =>  PHPExcel_Style_Alignment :: VERTICAL_JUSTIFY ,) );
        $objPHPExcel-> getActiveSheet ()
        -> getStyle ("A2")
        -> getFill ()
        -> setFillType (PHPExcel_Style_Fill :: FILL_SOLID);

        $borders = array(
            'borders' => array(
                'allborders' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN,
                'color' => array('argb' => 'FF000000'),
                )
            ),
        );
        $objPHPExcel->setActiveSheetIndex(0)
        ->setCellValue('A3', 'Rubro presupuestal')
        ->setCellValue('B3', "Nombre rubro")
        ->setCellValue('C3', "Fuente")
        ->setCellValue('D3', "Nombre fuente")
        ->setCellValue('E3', "RP")
        ->setCellValue('F3', "CDP")
        ->setCellValue('G3', "Fecha RP")
        ->setCellValue('H3', "Tercero")
        ->setCellValue('I3', "Contrato")
        ->setCellValue('J3', "Objeto")
        ->setCellValue('K3', "Valor");
        $objPHPExcel-> getActiveSheet ()
            -> getStyle ("A3:K3")
            -> getFill ()
            -> setFillType (PHPExcel_Style_Fill :: FILL_SOLID)
            -> getStartColor ()
            -> setRGB ('99ddff');
        $objPHPExcel->getActiveSheet()->getStyle("A3:K3")->getFont()->setBold(true);
        $objPHPExcel->getActiveSheet()->getStyle('A1:K1')->applyFromArray($borders);
        $objPHPExcel->getActiveSheet()->getStyle('A2:K2')->applyFromArray($borders);
        $objPHPExcel->getActiveSheet()->getStyle('A3:K3')->applyFromArray($borders);

        $objWorksheet = $objPHPExcel->getActiveSheet();
        $totalData = count($request);
        $row = 4;
        for ($i=0; $i < $totalData ; $i++) {
            $objPHPExcel->setActiveSheetIndex(0)
                ->setCellValueExplicit ("A$row", $request[$i][0], PHPExcel_Cell_DataType :: TYPE_STRING)
                ->setCellValueExplicit ("B$row", $request[$i][1], PHPExcel_Cell_DataType :: TYPE_STRING)
                ->setCellValueExplicit ("C$row", $request[$i][2], PHPExcel_Cell_DataType :: TYPE_STRING)
                ->setCellValueExplicit ("D$row", $request[$i][3], PHPExcel_Cell_DataType :: TYPE_STRING)
                ->setCellValueExplicit ("E$row", $request[$i][4], PHPExcel_Cell_DataType :: TYPE_STRING)
                ->setCellValueExplicit ("F$row", $request[$i][5], PHPExcel_Cell_DataType :: TYPE_STRING)
                ->setCellValueExplicit ("G$row", $request[$i][6], PHPExcel_Cell_DataType :: TYPE_STRING)
                ->setCellValueExplicit ("H$row", $request[$i][7], PHPExcel_Cell_DataType :: TYPE_STRING)
                ->setCellValueExplicit ("I$row", $request[$i][8], PHPExcel_Cell_DataType :: TYPE_STRING)
                ->setCellValueExplicit ("J$row", $request[$i][9], PHPExcel_Cell_DataType :: TYPE_STRING)
                ->setCellValueExplicit ("K$row", $request[$i][10], PHPExcel_Cell_DataType :: TYPE_NUMERIC);
            $objPHPExcel-> getActiveSheet ()
            -> getStyle ("K$row")
            -> getAlignment ()
            -> applyFromArray (array ( 'horizontal'  =>  PHPExcel_Style_Alignment :: HORIZONTAL_RIGHT));
            $row++;
        }
        $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('I')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('J')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('K')->setAutoSize(true);
        //----Guardar documento----
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="reporte-contratos_saldos_rp_rubro.xlsx"');
        header('Cache-Control: max-age=0');
        $objWriter=PHPExcel_IOFactory::createWriter($objPHPExcel,'Excel2007');
        $objWriter->save('php://output');
    }
    die();
