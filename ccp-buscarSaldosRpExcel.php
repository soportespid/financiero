<?php
    header("Content-Type: text/html;charset=utf-8");
    require_once 'PHPExcel/Classes/PHPExcel.php';
	require_once 'comun.inc';
    require_once 'funciones.inc';
    require_once 'funcionesSP.inc.php';
    session_start();
	$linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");

    $out = array('error' => false);

    $maxVersion = ultimaVersionGastosCCPET();


    if($_GET){
        $contratos = array();

        $critFecha = '';
        $critNumContrato = '';

        if($_GET['fechaInicial'] != '' && $_GET['fechaFinal'] != ''){
            $critFecha = " AND fecha BETWEEN '".$_GET['fechaInicial']."' AND '".$_GET['fechaFinal']."'";
        }

        if($_GET['numContrato'] != ''){
            $critNumContrato = " AND contrato LIKE '%".$_GET['numContrato']."%' ";
        }

        $sqlr = "SELECT vigencia, consvigencia, fecha, detalle, tercero, contrato, idcdp FROM ccpetrp WHERE estado!='N' AND tipo_mov='201' $critFecha $critNumContrato ORDER BY consvigencia DESC";
        $res = mysqli_query($linkbd, $sqlr);
        while($row = mysqli_fetch_assoc($res)){
            $valorRp = 0;
            $valorCxp = 0;
            $saldoPorEjecu = 0;

            $sqlr_rev_tot = "SELECT SUM(valor) FROM ccpetrp_detalle WHERE vigencia = '".$row['vigencia']."' AND consvigencia= '".$row['consvigencia']."' AND tipo_mov LIKE '4%'";
            $res_rev_tot = mysqli_query($linkbd, $sqlr_rev_tot);
            $row_rev_tot = mysqli_fetch_assoc($res_rev_tot);

            $sqlr_det = "SELECT cuenta, productoservicio, fuente, valor, tipo_mov, medio_pago, codigo_vigenciag, bpim, indicador_producto, seccion_presupuestal FROM ccpetrp_detalle WHERE vigencia = '".$row['vigencia']."' AND consvigencia= '".$row['consvigencia']."' AND tipo_mov LIKE '201'";
            $res_det = mysqli_query($linkbd, $sqlr_det);
            while($row_det = mysqli_fetch_assoc($res_det)){


                $valorRpInd = 0;
                if($_GET['check'] == ''){

                    $sqlr_det_rev = "SELECT valor FROM ccpetrp_detalle WHERE vigencia = '".$row['vigencia']."' AND consvigencia= '".$row['consvigencia']."' AND cuenta = '".$row_det['cuenta']."' AND productoservicio = '".$row_det['productoservicio']."' AND fuente = '".$row_det['fuente']."' AND tipo_mov LIKE '4%' AND medio_pago = '".$row_det['medio_pago']."' AND codigo_vigenciag = '".$row_det['codigo_vigenciag']."' AND bpim = '".$row_det['bpim']."' AND indicador_producto = '".$row_det['indicador_producto']."' AND seccion_presupuestal = '".$row_det['seccion_presupuestal']."'";
                    $res_det_rev = mysqli_query($linkbd, $sqlr_det_rev);
                    $row_det_rev = mysqli_fetch_row($res_det_rev);

                    $valorRpInd = round($row_det['valor'] - $row_det_rev[0], 2);

                    $rubro = '';

                    if($row_det['bpim'] != ''){
                        $rubro = $row_det['indicador_producto']."-".$row_det['bpim']."-".$row_det['fuente']."-".$row_det['seccion_presupuestal']."-".$row_det['codigo_vigenciag']."-".$row_det['medio_pago'];
                    }else{
                        $rubro = $row_det['cuenta'];
                    }

                    $saldoPorEjecu = 0;
                    $det = array();

                    array_push($det, $row['contrato']);
                    array_push($det, $row['consvigencia']);
                    array_push($det, $row['vigencia']);
                    array_push($det, $row['idcdp']);
                    array_push($det, "'".$row['detalle']."'");
                    array_push($det, $row['fecha']);
                    array_push($det, $row['tercero']);
                    array_push($det, buscatercero($row['tercero']));
                    array_push($det, $row_det['cuenta']);
                    array_push($det, buscacuentaccpetgastos($row_det['cuenta'], $maxVersion));
                    array_push($det, $row_det['fuente']);
                    array_push($det, buscafuenteccpet($row_det['fuente']));
                    array_push($det, $row_det['productoservicio']);
                    array_push($det, $row_det['medio_pago']);
                    array_push($det, $row_det['codigo_vigenciag']);
                    array_push($det, $row_det['bpim']);
                    array_push($det, $row_det['indicador_producto']);
                    array_push($det, $row_det['seccion_presupuestal']);
                    array_push($det, $valorRpInd);
                    array_push($det, $rubro);

                }else{
                    $valorRpInd = $row_det['valor'] - $row_rev_tot['valor'];
                    $valorRp += $valorRpInd;
                    $row_rev_tot['valor'] = 0;
                }

                $valorCxp_det = 0;

                $sqlr_cxp = "SELECT SUM(valor) FROM tesoordenpago AS TB1, tesoordenpago_det AS TB2 WHERE TB1.id_orden = TB2.id_orden AND TB1.vigencia = '".$row['vigencia']."' AND TB1.id_rp = '".$row['consvigencia']."' AND TB1.estado != 'R' AND TB2.cuentap = '".$row_det['cuenta']."' AND TB2.tipo_mov = '201' AND TB2.fuente = '".$row_det['fuente']."' AND TB2.productoservicio = '".$row_det['productoservicio']."' AND TB2.indicador_producto = '".$row_det['indicador_producto']."' AND TB2.medio_pago = '".$row_det['medio_pago']."' AND TB2.codigo_vigenciag = '".$row_det['codigo_vigenciag']."' AND TB2.bpim = '".$row_det['bpim']."' AND TB2.seccion_presupuestal = '".$row_det['seccion_presupuestal']."' GROUP BY TB1.id_rp";

                $res_cxp = mysqli_query($linkbd, $sqlr_cxp);
                $row_cxp = mysqli_fetch_row($res_cxp);
                if($_GET['check'] == ''){

                    $valorCxp_det += $row_cxp[0];

                    //array_push($det, $row_cxp[0]);
                    $saldoPorEjecu = $valorRpInd - $row_cxp[0];
                    //array_push($det, $saldoPorEjecu);

                    /* array_push($contratos, $det); */
                }else{

                    $valorCxp += $row_cxp[0];
                    /* $saldoPorEjecutar = 0;
                    $saldoPorEjecutar = $row_det['valor'] - $row_cxp[0];
                    $saldoPorEjecu += $saldoPorEjecutar; */

                }

                $sqlr_cxp_nom = "SELECT SUM(TB2.valor) FROM hum_nom_cdp_rp AS TB1, humnom_presupuestal AS TB2 WHERE TB1.rp = '".$row['consvigencia']."' AND TB1.vigencia = '".$row['vigencia']."' AND TB1.nomina = TB2.id_nom AND TB2.estado = 'P' AND TB2.cuenta = '".$row_det['cuenta']."' AND TB2.fuente = '".$row_det['fuente']."' AND TB2.producto = '".$row_det['productoservicio']."' AND TB2.indicador = '".$row_det['indicador_producto']."' AND TB2.medio_pago = '".$row_det['medio_pago']."' AND TB2.vigencia_gasto = '".$row_det['codigo_vigenciag']."' AND TB2.bpin = '".$row_det['bpim']."' AND TB2.seccion_presupuestal = '".$row_det['seccion_presupuestal']."' GROUP BY TB1.rp";

                $res_cxp_nom = mysqli_query($linkbd, $sqlr_cxp_nom);
                $row_cxp_nom = mysqli_fetch_row($res_cxp_nom);
                if($_GET['check'] == ''){
                    $valorCxp_det += $row_cxp_nom[0];
                    array_push($det, $valorCxp_det);
                    $saldoPorEjecu -= $row_cxp_nom[0];
                    array_push($det, $saldoPorEjecu);

                    array_push($contratos, $det);
                }else{

                    $valorCxp += $row_cxp_nom[0];
                    /* $saldoPorEjecutar = 0;
                    $saldoPorEjecutar = $row_det['valor'] - $row_cxp_nom[0];
                    $saldoPorEjecu += $saldoPorEjecutar; */

                }


            }
            if($_GET['check'] != ''){
                $det = array();
                array_push($det, $row['contrato']);
                array_push($det, $row['consvigencia']);
                array_push($det, $row['vigencia']);
                array_push($det, $row['idcdp']);
                array_push($det, $row['detalle']);
                array_push($det, $row['fecha']);
                array_push($det, $row['tercero']);
                array_push($det, buscatercero($row['tercero']));
                array_push($det, $valorRp);
                array_push($det, $valorCxp);
                $saldoPorEjecu = 0;
                $saldoPorEjecu = $valorRp - $valorCxp;
                array_push($det, $saldoPorEjecu);

                array_push($contratos, $det);
            }

            /* array_push($contratos, $row); */
        }
        $request = $contratos;
        $objPHPExcel = new PHPExcel();
        $objPHPExcel->getActiveSheet()->getStyle('A:S')->applyFromArray(
            array(
                'alignment' => array(
                    'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
                    'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER
                )
            )
        );
        $objPHPExcel->getProperties()
        ->setCreator("IDEAL 10")
        ->setLastModifiedBy("IDEAL 10")
        ->setTitle("Exportar Excel con PHP")
        ->setSubject("Documento de prueba")
        ->setDescription("Documento generado con PHPExcel")
        ->setKeywords("usuarios phpexcel")
        ->setCategory("reportes");

        //----Cuerpo de Documento----
        $objPHPExcel->setActiveSheetIndex(0)
        ->mergeCells('A1:S1')
        ->mergeCells('A2:S2')
        ->setCellValue('A1', 'PRESUPUESTO')
        ->setCellValue('A2', 'REPORTE DE CONTRATOS SALDOS RP');
        $objPHPExcel-> getActiveSheet ()
        -> getStyle ("A1")
        -> getFill ()
        -> setFillType (PHPExcel_Style_Fill :: FILL_SOLID)
        -> getStartColor ()
        -> setRGB ('C8C8C8');
        $objPHPExcel-> getActiveSheet ()
        -> getStyle ("A1:A2")
        -> getFont ()
        -> setBold ( true )
        -> setName ( 'Verdana' )
        -> setSize ( 10 )
        -> getColor ()
        -> setRGB ('000000');
        $objPHPExcel-> getActiveSheet ()
        -> getStyle ('A1:A2')
        -> getAlignment ()
        -> applyFromArray (array ( 'horizontal'  =>  PHPExcel_Style_Alignment :: HORIZONTAL_CENTER ,) );
        $objPHPExcel-> getActiveSheet ()
        -> getStyle ('A3:S3')
        -> getAlignment ()
        -> applyFromArray (array ( 'horizontal'  =>  PHPExcel_Style_Alignment :: VERTICAL_JUSTIFY ,) );
        $objPHPExcel-> getActiveSheet ()
        -> getStyle ("A2")
        -> getFill ()
        -> setFillType (PHPExcel_Style_Fill :: FILL_SOLID);

        $borders = array(
            'borders' => array(
                'allborders' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN,
                'color' => array('argb' => 'FF000000'),
                )
            ),
        );
        $objPHPExcel->setActiveSheetIndex(0)
        ->setCellValue('A3', 'Contrato')
        ->setCellValue('B3', "RP")
        ->setCellValue('C3', "Vigencia")
        ->setCellValue('D3', "CDP")
        ->setCellValue('E3', "Concepto")
        ->setCellValue('F3', "Fecha RP")
        ->setCellValue('G3', "Tercero")
        ->setCellValue('H3', "Rubro")
        ->setCellValue('I3', "Cuenta CCPET")
        ->setCellValue('J3', "Fuente")
        ->setCellValue('K3', "CPC")
        ->setCellValue('L3', "Medio pago")
        ->setCellValue('M3', "Vigencia gasto")
        ->setCellValue('N3', "BPIM")
        ->setCellValue('O3', "Programático")
        ->setCellValue('P3', "Sección")
        ->setCellValue('Q3', "Valor RP")
        ->setCellValue('R3', "Pagos")
        ->setCellValue('S3', "Saldo por ejecutar");
        $objPHPExcel-> getActiveSheet ()
            -> getStyle ("A3:S3")
            -> getFill ()
            -> setFillType (PHPExcel_Style_Fill :: FILL_SOLID)
            -> getStartColor ()
            -> setRGB ('99ddff');
        $objPHPExcel->getActiveSheet()->getStyle("A3:S3")->getFont()->setBold(true);
        $objPHPExcel->getActiveSheet()->getStyle('A1:S1')->applyFromArray($borders);
        $objPHPExcel->getActiveSheet()->getStyle('A2:S2')->applyFromArray($borders);
        $objPHPExcel->getActiveSheet()->getStyle('A3:S3')->applyFromArray($borders);

        $objWorksheet = $objPHPExcel->getActiveSheet();
        $totalData = count($request);
        $row = 4;
        for ($i=0; $i < $totalData ; $i++) {
            $objPHPExcel->setActiveSheetIndex(0)
                ->setCellValueExplicit ("A$row", $request[$i][0], PHPExcel_Cell_DataType :: TYPE_STRING)
                ->setCellValueExplicit ("B$row", $request[$i][1], PHPExcel_Cell_DataType :: TYPE_STRING)
                ->setCellValueExplicit ("C$row", $request[$i][2], PHPExcel_Cell_DataType :: TYPE_STRING)
                ->setCellValueExplicit ("D$row", $request[$i][3], PHPExcel_Cell_DataType :: TYPE_STRING)
                ->setCellValueExplicit ("E$row", $request[$i][4], PHPExcel_Cell_DataType :: TYPE_STRING)
                ->setCellValueExplicit ("F$row", $request[$i][5], PHPExcel_Cell_DataType :: TYPE_STRING)
                ->setCellValueExplicit ("G$row", $request[$i][6]."-".$request[$i][7], PHPExcel_Cell_DataType :: TYPE_STRING)
                ->setCellValueExplicit ("H$row", $request[$i][19], PHPExcel_Cell_DataType :: TYPE_STRING)
                ->setCellValueExplicit ("I$row", $request[$i][8]."-".$request[$i][9], PHPExcel_Cell_DataType :: TYPE_STRING)
                ->setCellValueExplicit ("J$row", $request[$i][10]."-".$request[$i][11], PHPExcel_Cell_DataType :: TYPE_STRING)
                ->setCellValueExplicit ("K$row", $request[$i][12], PHPExcel_Cell_DataType :: TYPE_STRING)
                ->setCellValueExplicit ("L$row", $request[$i][13], PHPExcel_Cell_DataType :: TYPE_STRING)
                ->setCellValueExplicit ("M$row", $request[$i][14], PHPExcel_Cell_DataType :: TYPE_STRING)
                ->setCellValueExplicit ("N$row", $request[$i][15], PHPExcel_Cell_DataType :: TYPE_STRING)
                ->setCellValueExplicit ("O$row", $request[$i][16], PHPExcel_Cell_DataType :: TYPE_STRING)
                ->setCellValueExplicit ("P$row", $request[$i][17], PHPExcel_Cell_DataType :: TYPE_STRING)
                ->setCellValueExplicit ("Q$row", $request[$i][18], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
                ->setCellValueExplicit ("R$row", $request[$i][20], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
                ->setCellValueExplicit ("S$row", $request[$i][21], PHPExcel_Cell_DataType :: TYPE_NUMERIC);

            $objPHPExcel-> getActiveSheet ()
            -> getStyle ("Q$row")
            -> getAlignment ()
            -> applyFromArray (array ( 'horizontal'  =>  PHPExcel_Style_Alignment :: HORIZONTAL_RIGHT));
            $objPHPExcel-> getActiveSheet ()
            -> getStyle ("R$row")
            -> getAlignment ()
            -> applyFromArray (array ( 'horizontal'  =>  PHPExcel_Style_Alignment :: HORIZONTAL_RIGHT));
            $objPHPExcel-> getActiveSheet ()
            -> getStyle ("S$row")
            -> getAlignment ()
            -> applyFromArray (array ( 'horizontal'  =>  PHPExcel_Style_Alignment :: HORIZONTAL_RIGHT));
            $row++;
        }
        $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('I')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('J')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('K')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('L')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('M')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('N')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('O')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('P')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('Q')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('R')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('S')->setAutoSize(true);
        //----Guardar documento----
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="reporte-contratos_saldos_rp.xlsx"');
        header('Cache-Control: max-age=0');
        $objWriter=PHPExcel_IOFactory::createWriter($objPHPExcel,'Excel2007');
        $objWriter->save('php://output');
    }
    die();
