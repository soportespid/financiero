<?php
	header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=utf8");
	require 'comun.inc';
	require 'funciones.inc';
	$linkbd = conectar_v7();
    $linkbd -> set_charset("utf8"); 
	date_default_timezone_set("America/Bogota");
?>
<!DOCTYPE >
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="es">
	<head>
		<meta http-equiv="Content-Type" content="text/html" charset="utf8"/>
		<meta http-equiv="X-UA-Compatible" content="IE=9"/>
		<title>:: Control de activos</title>
		<link href="css/css2.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<link href="css/css4.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
        <link href="css/cssSP.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<script type="text/javascript" src="css/programas.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script>
			function agregar(codigo, nombre, tipo)
			{
				if (tipo == 'AUXILIAR' || tipo == 'Auxiliar') {

					parent.document.form2.nombreCuenta.value = nombre;
					parent.document.form2.cuenta.value = codigo;
					parent.despliegamodal2("hidden");
				}
				else {
					alert("Seleccione cuentas auxiliares");
				}
			}
		</script> 
		<?php 
            titlepag();
        ?>
	</head>
	<body>
		<form name="form2" method="post">
			<table class="inicio ancho" style="width:99.5%">
				<tr>
					<td class="titulos" colspan="3">Cuentas - Control de activos</td>
					<td class="cerrar" style="width:7%" onClick="parent.despliegamodal2('hidden');">Cerrar</td>
				</tr>
				
				<tr>
					<td class="tamano01" style='width:3cm;'>Cuenta o nombre:</td>
					<td>
						<input type="search" name="codnom" id="codnom" value="<?php echo @$_POST['codnom'];?>" style='width:100%;'/>
					</td>

					<td style="padding-bottom:0px;height:35px;"><em class="botonflecha" onClick="document.form2.submit();">Buscar</em></td>
				</tr>
			</table>

			<div class="subpantalla" style="height:82%; width:99.2%; overflow-x:hidden;">
				<table class='inicio' align='center' width='99%'>
					<tr>
						<td colspan='4' class='titulos'>Resultados Busqueda: </td>
					</tr>

					<tr class='titulos2' style='text-align:center;'>
						<td style="width: 10%;">Cuenta</td>
						<td>Nombre</td>
                        <td>Naturaleza</td>
                        <td>Tipo</td>
					</tr>

					<?php
						$crit1="";
						$crit2="";
						if (@$_POST['codnom']!="") {
							$crit1 = "AND CONCAT_WS(' ', cn1.cuenta, cn1.nombre) LIKE '%$_POST[codnom]%'";
							$crit2 = "AND CONCAT_WS(' ', cuenta, nombre) LIKE '%$_POST[codnom]%'";
						}
                        $fec = $_GET['fecha'];
                        preg_match("/([0-9]{2})\/([0-9]{2})\/([0-9]{4})/", $fec,$fech);
                        $fecha = "$fech[3]-$fech[2]-$fech[1]";

						$sqlDestinoAlmacen = "SELECT cuenta_inicial, cuenta_final FROM almdestinocompra_det_almacen WHERE codigo = '06' AND fecha = (SELECT MAX(fecha) FROM almdestinocompra_det WHERE fecha <= '$fecha') AND estado = 'S'";
						$resDestinoAlmacen = mysqli_query($linkbd,$sqlDestinoAlmacen);
                        $encontrados = mysqli_num_rows($resDestinoAlmacen);
						$iter  = 'saludo1a';
						$iter2 = 'saludo2';
						$conta = 1;

						while ($rowDestinoAlmacen = mysqli_fetch_row($resDestinoAlmacen))
						{
                            $sqlr = "SELECT * FROM (SELECT cn1.cuenta,cn1.nombre,cn1.naturaleza,cn1.centrocosto,cn1.tercero,cn1.tipo,cn1.estado FROM cuentasnicsp AS cn1 INNER JOIN cuentasnicsp AS cn2 ON cn2.tipo='Auxiliar' AND cn2.naturaleza='DEBITO' AND cn2.cuenta LIKE CONCAT( cn1.cuenta, '%' ) WHERE cn1.tipo='Mayor' AND cn1.naturaleza='DEBITO' AND cn1.cuenta BETWEEN '$rowDestinoAlmacen[0]' AND '$rowDestinoAlmacen[1]' $crit1 GROUP BY cn1.cuenta UNION SELECT cuenta,nombre,naturaleza,centrocosto,tercero,tipo,estado FROM cuentasnicsp WHERE tipo='Auxiliar' AND naturaleza='DEBITO' AND cuenta BETWEEN '$rowDestinoAlmacen[0]' AND '$rowDestinoAlmacen[1]' $crit2) AS tabla ORDER BY 1";
                            $res = mysqli_query($linkbd, $sqlr);
                            while($row = mysqli_fetch_row($res))
                            {
                                ?>
                                    <tr class='<?php echo $iter ?>' style='text-align:center; text-transform:uppercase;' ondblclick="agregar('<?php echo $row[0]; ?>', '<?php echo $row[1]; ?>', '<?php echo $row[5]; ?>')">
                                        <td><?php echo $row[0] ?></td>
                                        <td><?php echo $row[1] ?></td>
                                        <td><?php echo $row[2] ?></td>
                                        <td><?php echo $row[5] ?></td>
                                    </tr>
					            <?php

                                $aux=$iter;
                                $iter=$iter2;
                                $iter2=$aux;
                                $conta++;
                            }	
						}

						if ($encontrados == 0)
						{
					?>
							<table class='inicio'>
								<tr>
									<td class='saludo1' style='text-align:center;'>
										<img src='imagenes\alert.png' style='width:25px; height: 30px;'> No se han encontrado registros <img src='imagenes\alert.png' style='width:25px; height: 30px;'>
									</td>
								</tr>
							</table>
					<?php
						}
					?>
				</table>
			</div>
		</form>
	</body>
</html>
