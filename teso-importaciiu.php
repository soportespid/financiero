<?php
	header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=utf8");
	require "comun.inc";
	require "funciones.inc";
	session_start();
	$linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");
	cargarcodigopag($_GET['codpag'],$_SESSION["nivel"]);
	date_default_timezone_set("America/Bogota");
?>
<!DOCTYPE >
<html lang="es">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1">
		<meta name="viewport" content="user-scalable=no">
		<title>:: IDEAL 10 - Tesorer&iacute;a</title>
		<link href="favicon.ico" rel="shortcut icon"/>
		<link href="css/css2z.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet"/>
		<link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet"/>
		<link href="css/css4.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet"/>
		<link href="sweetalert2/dist/sweetalert2.min.css" rel="stylesheet">
		<script src="sweetalert2/dist/sweetalert2.min.js"></script>
		<script type="text/javascript" src="css/programas.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script type="text/javascript" src="JQuery/jquery-2.1.4.min.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script>
			function pdf(){
				document.form2.action="pdfbalance.php";
				document.form2.target="_BLANK";
				document.form2.submit(); 
				document.form2.action="";
				document.form2.target="";
			}
			function guardar(){
				let version = document.getElementById('versionini').value;
				let lininicial = document.getElementById('lineaini').value;
				let nomarch = document.getElementById('nomarch').value;
				if(version.trim() != '' && lininicial.trim() != '' && nomarch.trim() != ''){
					Swal.fire({
						icon: 'question',
						title: '¿Esta seguro de cargar este archivo de codigos CIIU?',
						showDenyButton: true,
						confirmButtonText: 'Guardar',
						confirmButtonColor: '#01CC42',
						denyButtonText: 'Cancelar',
						denyButtonColor: '#FF121A',
					}).then(
						(result) => {
							if (result.isConfirmed){
								document.getElementById('oculto').value = '2';
								document.form2.submit();
							}
							else if (result.isDenied){
								Swal.fire({
									icon: 'info',
									title: 'No se cargo el archivo',
									confirmButtonText: 'Continuar',
									confirmButtonColor: '#FF121A',
									timer: 2500
								});
							}
						}
					)
				}else{
					Swal.fire({
						icon: 'error',
						title: 'Error!',
						text: 'Faltan información para cargar el archivo',
						confirmButtonText: 'Continuar',
						confirmButtonColor: '#FF121A',
						timer: 2500
					});
				}
			}
			function cargararchivo(){
				document.getElementById('oculto').value = '3'
				document.form2.submit();
			}
		</script>
	</head>
	<body>
		<IFRAME src="alertas.php" name="alertas" id="alertas" style="display:none"></IFRAME>
		<span id="todastablas2"></span>
		<table>
			<tr><script>barra_imagenes("teso");</script><?php cuadro_titulos();?></tr>	 
			<tr><?php menu_desplegable("teso");?></tr>
			<tr>
				<td colspan="3" class="cinta">
					<img src="imagenes/add.png" title="Nuevo" onClick="location.href='teso-importaciiu.php'" class="mgbt">
					<img src="imagenes/guarda.png" title="Guardar" onClick="guardar();" class="mgbt">
					<img src="imagenes/busca.png" title="Buscar" onClick="location.href='teso-buscaactividades.php'" class="mgbt">
					<img src="imagenes/nv.png" title="Nueva ventana" onClick="mypop=window.open('teso-principal.php','','');mypop.focus();" class="mgbt">
					<img src="imagenes/duplicar_pantalla.png" title="Duplicar pesta&ntilde;a" onClick="mypop=window.open('<?php echo $url2; ?>','','');mypop.focus();" class="tooltip bottom mgbt">
				</td>
			</tr>
		</table>
		<form action="teso-importaciiu.php" method="post" enctype="multipart/form-data" name="form2">
			<?php
				if(!$_POST['oculto']){
					$_POST['lineaini'] = 0;
					$sql = "SELECT MAX(version) FROM codigosciiu";
					$res = mysqli_query($linkbd,$sql);
					$row = mysqli_fetch_row($res);
					$_POST['versionini'] = $row[0] + 1;
				}
			?>
			<table class="inicio ancho">
				<tr>
					<td class="titulos" colspan="7">.: Importar Códigos CIIU</td>
					<td class="cerrar" style="width:7%" onClick="location.href='teso-principal.php'">Cerrar</td>
				</tr>   
				<tr> 
				<td class="tamano01" style="width:2cm;">Version: </td>
					<td style="width:5%"><input type="text" name="versionini" id="versionini" style="width:100%" value="<?php echo $_POST['versionini'];?>" /></td>
					<td class="tamano01" style="width:2cm;">Linea Inicial: </td>
					<td style="width:5%"><input type="text" name="lineaini" id="lineaini" style="width:100%" value="<?php echo $_POST['lineaini'];?>" /></td>
					<td class="tamano01" style="width:3.5cm;">Seleccione Archivo: </td>
					<td style="width:35%"><input type="text" name="nomarch" id="nomarch" style="width:100%" value="<?php echo $_POST['nomarch'];?>" readonly/></td>
					<td>
						<div class='upload' style="height: 23px">
							<input type="file" name="archivotexto" id="archivotexto" onChange="cargararchivo()" style='cursor:pointer;' title='Cargar Documento' accept=".csv">
							<img src='imagenes/upload01.png' style="width:22px">
						</div> 
					</td>
				</tr>
			</table>
			<input type="hidden" name="oculto" id="oculto" value="1">
			<div class="subpantalla">
				<?php
					$oculto = $_POST['oculto'];
					if($_POST['oculto'] == '3'){
						if(is_uploaded_file($_FILES['archivotexto']['tmp_name'])){
							$archivo = $_FILES['archivotexto']['name'];
							$archivoF = "./archivos/$archivo";
							echo"<script>document.getElementById('nomarch').value='$archivo';</script>";
							if(move_uploaded_file($_FILES['archivotexto']['tmp_name'],$archivoF)){
								//echo "El archivo se subio correctamente";
							}
						}
						$fich = $archivoF;
						$contenido = fopen($fich,"r+");
						$lineas = 0;
						$iter='saludo1a';
						$iter2='saludo2';
						echo "
						<table class='tablamv'>
							<thead>
								<tr>
									<th class='titulosnew02'>.: Resultados Busqueda:</th>
								</tr>
								<tr>
									<th class='titulosnew00' style='width:5%;'>Linea</th>
									<th class='titulosnew00' style='width:5%;'>División</th>
									<th class='titulosnew00' style='width:5%;'>Grupo</th>
									<th class='titulosnew00' style='width:5%;'>Código</th>
									<th class='titulosnew00' >Nombre</th>
								</tr>
							</thead>
							<tbody>";	
						while ($lineas < $_POST['lineaini'] && !feof($contenido)) {
							
							fgets($contenido);
							$lineas++;
						}
						while(!feof($contenido)){ 
							$buffer = fgets($contenido,4096);
							$datos = explode(";",$buffer);
							echo"
								<tr class='$iter'>
									<td  style='text-align:center; width:5%;'>$lineas</td>
									<td  style='text-align:center; width:5%;'>".trim($datos[0])."</td>
									<td  style='text-align:center; width:5%;'>".trim($datos[1])."</td>
									<td  style='text-align:center; width:5%;'>".trim($datos[2])."</td>
									<td>".trim($datos[3])."</td>
								</tr>
							";
							$aux=$iter;
							$iter=$iter2;
							$iter2=$aux;
							$lineas++;
						}
						echo "
							<tbody>
						</table>";
					}
					if($_POST['oculto'] == '2'){
						$fich = "./archivos/".$_POST['nomarch'];
						$contenido = fopen($fich,"r+");
						$lineas = 0;
						$exito = 0;
						$errores = 0;
						$seccion = $division = $grupo = $clase = '';
						$bandera = "S";
						$iter='saludo1a';
						$iter2='saludo2';
						echo "
						<table class='tablamv'>
							<thead>
								<tr>
									<th class='titulosnew02'>.: Resultados Busqueda:</th>
								</tr>
								<tr>
									<th class='titulosnew00' style='width:5%;'>Linea</th>
									<th class='titulosnew00' style='width:5%;'>División</th>
									<th class='titulosnew00' style='width:5%;'>Grupo</th>
									<th class='titulosnew00' style='width:5%;'>Código</th>
									<th class='titulosnew00' >Nombre</th>
								</tr>
							</thead>
							<tbody>";	
						while ($lineas < $_POST['lineaini'] && !feof($contenido)) {
							
							fgets($contenido);
							$lineas++;
						}
						while(!feof($contenido)){ 
							$buffer = fgets($contenido,4096);
							$datos = explode(";",$buffer);
							echo"
								<tr class='$iter'>
									<td style='text-align:center; width:5%;'>$lineas</td>
									<td style='text-align:center; width:5%;'>".trim($datos[0])."</td>
									<td style='text-align:center; width:5%;'>".trim($datos[1])."</td>
									<td style='text-align:center; width:5%;'>".trim($datos[2])."</td>
									<td>".trim($datos[3])."</td>
								</tr>
							";
							if(trim($datos[0])!= '' && trim($datos[1]) == '' && trim($datos[2]) == ''){
								if(strlen($datos[0]) > 2){
									$seccion = trim($datos[0]);
									$division = $grupo = $clase = '';

								}else{
									$division = trim($datos[0]);
									$grupo = $clase = '';
								}
								$bandera = "S";
							}elseif(trim($datos[1]) != '' && trim($datos[2]) == ''){
								$grupo = trim($datos[1]);
								$division = $clase = '';
								$bandera = "S";
							}elseif(trim($datos[2]) != '' && trim($datos[1]) == ''){
								$clase = trim($datos[2]);
								$division = $grupo = '';
								$bandera = "S";
							}elseif(trim($datos[2]) != ''){
								$grupo = trim($datos[1]);
								$clase = trim($datos[2]);
								$division = '';
								$bandera = "S";
							}else{
								$bandera = "N";
							}
							$descrip = trim($datos[3]);
							if($bandera == "S"){
								$consulta = "INSERT INTO codigosciiu (codigo, nombre, porcentaje, division, grupo, version, seccion) VALUES ('$clase', '$descrip', '0', '$division', '$grupo', '".$_POST['versionini']."', '$seccion')";
								if (!mysqli_query($linkbd,$consulta)){
									$errores+=1;
								}else{
									$exito+=1;
								}
							}
							$aux=$iter;
							$iter=$iter2;
							$iter2=$aux;
							$lineas++;
						}
						echo "
							<tbody>
						</table>
						<script>
							Swal.fire({
								icon: 'success',
								title: 'Se cargaron $exito lineas con exito y $errores con errores ',
								confirmButtonText: 'Continuar',
								confirmButtonColor: '#FF121A',
								timer: 3000
							});
						</script>";
					}
				?> 
			</div>
		</form>
	</body>
</html>