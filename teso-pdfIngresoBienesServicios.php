<?php
	require_once("tcpdf/tcpdf_include.php");
	require('comun.inc');
	require('funciones.inc');
	session_start();
	date_default_timezone_set("America/Bogota");
	$val = 0;
	class MYPDF extends TCPDF 
	{
		public function Header() 
		{
			$linkbd=conectar_v7();
			$linkbd -> set_charset("utf8");
			$sqlr="SELECT nit, razonsocial FROM configbasica WHERE estado='S'";
			$resp=mysqli_query($linkbd,$sqlr);
			while($row=mysqli_fetch_row($resp)){$nit=$row[0];$rs=strtoupper($row[1]);}
			$this->Image('imagenes/escudo.jpg',  13, 13, 20, 20, 'JPG', '', 'T', true, 300, '', false, false, 0, false, false, false);// Logo
			$this->SetFont('helvetica','B',8);
			$this->SetY(10);
			$this->RoundedRect(10, 10, 277, 25, 1,''); //Borde del encabezado
			$this->Cell(26,25,'','R',0,'L');  //Linea que separa el encabazado verticalmente
			$this->SetY(8);
			$this->SetX(80);
			$this->SetFont('helvetica','B',9);
			$this->Cell(160,15,"$rs",0,0,'C'); 
			$this->SetY(12);
			$this->SetX(80);
			$this->SetFont('helvetica','B',7);
			$this->Cell(160,15,'NIT: '.$nit,0,0,'C');
			$this->SetY(23);
			$this->SetX(36);
			$this->SetFont('helvetica','B',9);
			$this->Cell(251,12,"INGRESO BIENES Y SERVICIOS",'T',0,'C'); 
			$this->SetFont('helvetica','I',10);
			$this->SetY(27);
			$this->SetX(62);
			$mov='';
			if(isset($_POST['movimiento']))
			{
				if($_POST['movimiento']=='401' || $_POST['movimiento']=='402'){$mov="DOCUMENTO DE REVERSION";}
			}

			$this->SetFont('helvetica','B',6);
			$this->SetY(10);
			$this->SetX(257);
			$this->Cell(30,5," NUMERO: ".$_POST['idcomp'],"L",0,'L');
			$this->SetY(13.5);
			$this->SetX(257);
			$this->Cell(35,6," FECHA: ".$_POST['fecha'],"L",0,'L');
			$this->SetY(18);
			$this->SetX(257);
			$this->Cell(35,5," VIGENCIA: ".$_POST['vigencia'],"L",0,'L');

			$this->SetFont('helvetica','B',8);
			$this->SetY(25);
			
			$this->cell(248,8,'NETO A PAGAR: ',0,0,'R');
			$this->RoundedRect(257.5, 26 ,27, 6, 1,'');
			$this->cell(26,8,'$'.$_POST['total_ingreso'],0,0,'R');
			}
		public function Footer() 
		{
			$linkbd=conectar_v7();
			$linkbd -> set_charset("utf8");
			$sqlr="SELECT direccion,telefono,web,email FROM configbasica WHERE estado='S'";
			$resp=mysqli_query($linkbd,$sqlr);
			while($row=mysqli_fetch_row($resp))
			{
				$direcc=iconv($_SESSION["VERCARPDFINI"], $_SESSION["VERCARPDFFIN"]."//TRANSLIT",strtoupper($row[0]));
				$telefonos=$row[1];
				$dirweb=iconv($_SESSION["VERCARPDFINI"], $_SESSION["VERCARPDFFIN"]."//TRANSLIT",strtoupper($row[3]));
				$coemail=iconv($_SESSION["VERCARPDFINI"], $_SESSION["VERCARPDFFIN"]."//TRANSLIT",strtoupper($row[2]));
			}
			if($direcc!=''){$vardirec="Dirección: $direcc, ";}
			else {$vardirec="";}
			if($telefonos!=''){$vartelef="Telefonos: $telefonos";}
			else{$vartelef="";}
			if($dirweb!=''){$varemail="Email: $dirweb, ";}
			else {$varemail="";}
			if($coemail!=''){$varpagiw="Pagina Web: $coemail";}
			else{$varpagiw="";}
			$resp=mysqli_query($linkbd, $sqlr);
			$user = $_SESSION['nickusu'];	
			$fecha = date("Y-m-d H:i:s");
			$ip = $_SERVER['REMOTE_ADDR'];
			$useri = $_POST['user'];
			//$this->SetY(-16);
			$this->SetFont('helvetica', 'I', 6);
			$txt = <<<EOD
			$vardirec $vartelef
			$varemail $varpagiw
			EOD;
			$this->Cell(277,8,'','T',0,'T');
			$this->ln(2);
			$this->Write(0, $txt , '', 0, 'C', true, 0, false, false, 0);
			$this->Cell(25, 8, 'Hecho por: '.$useri, 0, false, 'C', 0, '', 0, false, 'T', 'M');
			$this->Cell(100, 8, 'Impreso por: '.$user, 0, false, 'C', 0, '', 0, false, 'T', 'M');
			$this->Cell(30, 8, 'IP: '.$ip, 0, false, 'C', 0, '', 0, false, 'T', 'M');
			$this->Cell(102, 8, 'Fecha: '.$fecha, 0, false, 'C', 0, '', 0, false, 'T', 'M');
			$this->Cell(5, 8, 'IDEAL.10 S.A.S    Pagina '.$this->getAliasNumPage().'/'.$this->getAliasNbPages(), 0, false, 'C', 0, '', 0, false, 'T', 'M');
			
			
			
		}
	}
	$pdf = new MYPDF('L','mm','Letter', true, 'iso-8859-1', false);// create new PDF document
	$pdf->SetDocInfoUnicode (true); 
	// set document information
	$pdf->SetCreator(PDF_CREATOR);
	$pdf->SetAuthor('G&CSAS');
	$pdf->SetTitle('Certificados');
	$pdf->SetSubject('Certificado de Disponibilidad');
	$pdf->SetKeywords('TCPDF, PDF, example, test, guide');
	$pdf->SetMargins(10, 38, 10);// set margins
	$pdf->SetHeaderMargin(38);// set margins
	$pdf->SetFooterMargin(17);// set margins
	$pdf->SetAutoPageBreak(TRUE, 20);// set auto page breaks
	// set some language-dependent strings (optional)
	if (@file_exists(dirname(__FILE__).'/lang/spa.php')) 
	{
		require_once(dirname(__FILE__).'/lang/spa.php');
		$pdf->setLanguageArray($l);
	}
	// ---------------------------------------------------------
	$pdf->AddPage();


	$pdf->SetFillColor(245,245,245);
	$pdf->cell(0.2);
	$pdf->SetFont('helvetica','B',7);
	$pdf->cell(23,4,'Beneficiario: ','LT',0,'L',1);
	$pdf->SetFont('helvetica','',7);
	$ntercero = $_POST['ntercero'];
	$pdf->cell(90,4,''.substr(ucwords($ntercero),0,100),'T',0,'L',1);
	 
	$pdf->SetFont('helvetica','B',7);
	$pdf->cell(23,4,'C.C o NIT: ','T',0,'L',1);
	$pdf->SetFont('helvetica','',7);
	$pdf->cell(40,4,''.$_POST['tercero'],'T',0,'L',1);
	$pdf->SetFont('helvetica','B',7);
	$pdf->cell(23,4,'Cta Beneficiario:','T',0,'L',1);
	$pdf->SetFont('helvetica','',7);
	if(mb_strlen ($_POST['nombreCuenta']) < 50){
		$pdf->SetFont('helvetica','',7);
	}else{
		$pdf->SetFont('helvetica','',5);
	}
	$pdf->cell(78,4,$_POST['nombreCuenta'],'TR',1,'L',1);
	$pdf->SetFont('helvetica','B',7);
	


	$pdf->cell(0.2);

	$concepto = ucfirst(strtolower($_POST['concepto']));
	$lineass = $pdf->getNumLines($concepto, 254);
	$alturadt=(4*$lineass);
	$pdf->MultiCell(23,$alturadt,'Concepto: ', 'LB', 'J', 0, 0, '', '', true, 0, false, true, $alturadt, 'T');
	$pdf->SetFont('helvetica','',8);
	$pdf->MultiCell(254,$alturadt,"$concepto",'RB','L',false,1,'','',true,0,false,true,$alturadt,'T',false);

	//	$pdf->Cell(199,$altura,"CONCEPTO:  ".iconv($_SESSION["VERCARPDFINI"], $_SESSION["VERCARPDFFIN"]."//TRANSLIT",strtoupper($_POST['concepto'])),'LR','L',true,1,'','',true,0,false,true,$altura,'M',false);

	
	$pdf->SetFillColor(245,245,245);
	$pdf->SetFont('helvetica','B',7);
	$pdf->cell(0.2);
	$pdf->cell(23,4,'No Liq::','L',0,'L',1);
	$pdf->SetFont('helvetica','',7);
	$pdf->cell(90,4,''.$_POST['idrecaudo'],0,0,'L',1);
	$pdf->SetFont('helvetica','B',7);
	$pdf->cell(23,4,'Forma Pago:',0,0,'L',1);
	$pdf->SetFont('helvetica','',7);
	$pdf->cell(141,4,''.strtoupper($_POST['modorec']),'R',1,'L',1);
	$pdf->SetFillColor(255,255,255);
	$pdf->SetFont('helvetica','B',7);
	$pdf->cell(0.2);
	$pdf->cell(23,4,'Banco: ','L',0,'L',1);
	$pdf->SetFont('helvetica','',7);
	$pdf->cell(90,4,''.substr(strtoupper($_POST['nbanco']),0,80),0,0,'L',1);
	$pdf->SetFont('helvetica','B',7);
	$pdf->cell(23,4,'N Cta.:',0,0,'L',1);
	$pdf->SetFont('helvetica','',7);
	$pdf->cell(141,4,''.$_POST['tcta'].' '.$_POST['cb'],'R',1,'L',1);
	$pdf->SetFillColor(245,245,245);
	$pdf->SetFont('helvetica','B',7);
	$pdf->cell(0.2);
	$pdf->cell(23,4,'Cheque/Tranf.:','L',0,'L',1);
	$pdf->SetFont('helvetica','',7);
	$pdf->cell(254,4,''.$_POST['ntransfe'].$_POST['ncheque'],'R',1,'L',1);
	$pdf->SetFillColor(255,255,255);
	$pdf->SetFont('helvetica','B',7);
	$pdf->cell(0.2);
	$pdf->cell(23,4,'Valor Pago:','L',0,'L',1);
	$pdf->SetFont('helvetica','',7);
	$pdf->cell(90,4,'$'.number_format($_POST['valorecaudo'],2),0,0,'L',1);
	$pdf->SetFont('helvetica','B',7);
	$pdf->cell(23,4,'Retenciones: ',0,0,'L',1);
	$pdf->SetFont('helvetica','',7);
	$pdf->cell(48,4,'$'.$_POST['total_retencion'],0,0,'L',1);
	$pdf->SetFont('helvetica','B',7);
	$pdf->cell(16,4,'Iva: ',0,0,'L',1);
	$pdf->SetFont('helvetica','',7);
	$pdf->cell(77,4,'$'.$_POST['total_iva'],'R',1,'L',1);
	$pdf->SetFillColor(245,245,245);
	$pdf->SetFont('helvetica','B',7);
	$pdf->cell(0.2);
	$pdf->cell(23,4,'Neto a Pagar:','L',0,'L',1);
	$pdf->SetFont('helvetica','',7);
	$pdf->cell(254,4,'$'.$_POST['total_ingreso'],'R',1,'L',1);
	$pdf->SetFillColor(255,255,255);
	$pdf->SetFont('helvetica','B',7);
	$pdf->cell(0.2);
	$pdf->cell(23,4,'Son: ','LB',0,'L',1);
	$pdf->SetFont('helvetica','',7);
	$pdf->cell(254,4,''.strtoupper($_POST['letras']),'BR',0,'L',1);


	$pdf->ln(6);


	$y=$pdf->GetY();
	$pdf->SetY($y);
	/* $pdf->SetFillColor(222,222,222);
	$pdf->SetFont('helvetica','B',7);
	$pdf->Cell(0.1);
	$pdf->Cell(277,4,'RETENCIONES',0,0,'C',1); 
	$pdf->ln(5); */ 
	$y=$pdf->GetY();
	$pdf->SetFillColor(222,222,222);
	$pdf->SetFont('helvetica','B',7);
	$pdf->SetY($y);
	$pdf->Cell(0.1);
	$pdf->Cell(45,4,'Codigo ',0,0,'C',1); 
	$pdf->SetY($y);
	$pdf->Cell(46);
	$pdf->Cell(140,4,'Retencion',0,0,'C',1);
	$pdf->SetY($y);
	$pdf->Cell(187);
	$pdf->Cell(45,4,'Porcentaje',0,0,'C',1);
	$pdf->SetY($y);
	$pdf->Cell(233);
	$pdf->Cell(44,4,'Valor',0,0,'C',1);
	$pdf->SetFont('helvetica','',7);
	$cont=0;
	$pdf->ln(5);
    
    $linkbd=conectar_v7();
    $linkbd -> set_charset("utf8");

    $codigos = $nombres = $porcentajes = $valores = [];

    $sqlDet = "SELECT codigo, nombre, porcentaje, valor FROM teso_ingreso_bienes_servicios_det WHERE id_recibos = '$_POST[idcomp]' AND tipo LIKE 'R%'";
    $resDet = mysqli_query($linkbd, $sqlDet);
    while ($rowDet = mysqli_fetch_row($resDet)) {
        
        $codigos[] = $rowDet[0];
        $nombres[] = $rowDet[1];
        $porcentajes[] = $rowDet[2];
        $valores[] = $rowDet[3];
    }

	for($x=0;$x<count($codigos);$x++)
	{
		if ($con%2==0){$pdf->SetFillColor(255,255,255);}
		else {$pdf->SetFillColor(245,245,245);}
		$pdf->Cell(46,4,''.$codigos[$x],'',0,'C',1);
		$pdf->Cell(141,4,''.$nombres[$x],'',0,'L',1);
		$pdf->Cell(46,4,''.$porcentajes[$x].'%','',0,'R',1);
		$pdf->Cell(44,4,'$'.number_format($valores[$x],2),'',1,'R',1);
		$con=$con+1;
	}

	$pdf->Output();
?>