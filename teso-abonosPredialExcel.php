<?php
	header("Content-Type: text/html;charset=utf-8");
    require_once 'PHPExcel/Classes/PHPExcel.php';
    require "comun.inc";
    require "funciones.inc";
    require 'funcionesSP.inc.php';
    session_start();

    class Plantilla{
        private $linkbd;
        private $arrData;
        public function __construct() {
            $this->linkbd = conectar_v7();
            $this->linkbd->set_charset("utf8");
        }
        public function getData(){
            if(!empty($_SESSION)){
                $request = $this->selectData();
                echo json_encode($request,JSON_UNESCAPED_UNICODE);
            }
            die();
        }
        public function selectData(){
            $sql = "SELECT id_abono as idabono,idacuerdo,concepto,tercero as documento, valortotal as total,
            cierre,recibo,
            DATE_FORMAT(fecha,'%d/%m/%Y') as fecha,tipomovimiento
            FROM tesoabono WHERE estado = 'S' ORDER BY id_abono DESC";
            $arrData = mysqli_fetch_all(mysqli_query($this->linkbd,$sql),MYSQLI_ASSOC);
            $total = count($arrData);
            if($total>0){
                for ($i=0; $i < $total ; $i++) {
                    $sql="SELECT id_tercero,cedulanit as documento,direccion,tipodoc as tipo_documento,telefono,celular,email as correo,persona as tipo, regimen,
                    CASE WHEN razonsocial IS NULL OR razonsocial = ''
                    THEN CONCAT(nombre1,' ',nombre2,' ',apellido1,' ',apellido2)
                    ELSE razonsocial END AS nombre FROM terceros WHERE cedulanit = '{$arrData[$i]['documento']}'";
                    $arrData[$i]['nombre'] = mysqli_query($this->linkbd,$sql)->fetch_assoc()['nombre'];
                    $arrData[$i]['cierre'] = $arrData[$i]['cierre'] > 0 ? $arrData[$i]['cierre'] :"N/A";
                    $arrData[$i]['recibo'] = $arrData[$i]['recibo'] > 0 ? $arrData[$i]['recibo'] :"N/A";
                }
            }
            $arrResponse = array("status"=>true,"data"=>$arrData,"total"=>$total);
            return $arrResponse;
        }
    }
        $obj = new Plantilla();
        $request = $obj->selectData()['data'];
        $objPHPExcel = new PHPExcel();
        $objPHPExcel->getActiveSheet()->getStyle('A:J')->applyFromArray(
            array(
                'alignment' => array(
                    'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
                    'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER
                )
            )
        );
        $objPHPExcel->getProperties()
        ->setCreator("IDEAL 10")
        ->setLastModifiedBy("IDEAL 10")
        ->setTitle("Exportar Excel con PHP")
        ->setSubject("Documento de prueba")
        ->setDescription("Documento generado con PHPExcel")
        ->setKeywords("usuarios phpexcel")
        ->setCategory("reportes");

        //----Cuerpo de Documento----
        $objPHPExcel->setActiveSheetIndex(0)
        ->mergeCells('A1:J1')
        ->mergeCells('A2:J2')
        ->mergeCells('B3:J3')
        ->setCellValue('A1', 'TESORERÍA')
        ->setCellValue('A2', 'REPORTE ABONOS ACUERDO PREDIAL')
        ->setCellValue('A3', 'CONVENCIONES')
        ->setCellValue('B3', 'Liquidaciones sin recibos de caja');
        $objPHPExcel-> getActiveSheet ()
        -> getStyle ("B3")
        -> getFill ()
        -> setFillType (PHPExcel_Style_Fill :: FILL_SOLID)
        -> getStartColor ()
        -> setRGB ('ffc107');
        $objPHPExcel-> getActiveSheet ()
        -> getStyle ("A1")
        -> getFill ()
        -> setFillType (PHPExcel_Style_Fill :: FILL_SOLID)
        -> getStartColor ()
        -> setRGB ('C8C8C8');
        $objPHPExcel-> getActiveSheet ()
        -> getStyle ("A1:A2")
        -> getFont ()
        -> setBold ( true )
        -> setName ( 'Verdana' )
        -> setSize ( 10 )
        -> getColor ()
        -> setRGB ('000000');
        $objPHPExcel-> getActiveSheet ()
        -> getStyle ('A1:A2')
        -> getAlignment ()
        -> applyFromArray (array ( 'horizontal'  =>  PHPExcel_Style_Alignment :: HORIZONTAL_CENTER ,) );
        $objPHPExcel-> getActiveSheet ()
        -> getStyle ('A4:j4')
        -> getAlignment ()
        -> applyFromArray (array ( 'horizontal'  =>  PHPExcel_Style_Alignment :: VERTICAL_JUSTIFY ,) );
        $objPHPExcel-> getActiveSheet ()
        -> getStyle ("A2")
        -> getFill ()
        -> setFillType (PHPExcel_Style_Fill :: FILL_SOLID);

        $borders = array(
            'borders' => array(
                'allborders' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN,
                'color' => array('argb' => 'FF000000'),
                )
            ),
        );
        $objPHPExcel->setActiveSheetIndex(0)
        ->setCellValue('A4', 'No. Abono')
        ->setCellValue('B4', "No. Acuerdo")
        ->setCellValue('C4', "No. Liquidacion")
        ->setCellValue('D4', "No. Recibo de caja")
        ->setCellValue('E4', "Fecha")
        ->setCellValue('F4', "Concepto")
        ->setCellValue('G4', "Documento")
        ->setCellValue('H4', "Nombre")
        ->setCellValue('I4', "Valor")
        ->setCellValue('J4', "estado");
        $objPHPExcel-> getActiveSheet ()
            -> getStyle ("A4:J4")
            -> getFill ()
            -> setFillType (PHPExcel_Style_Fill :: FILL_SOLID)
            -> getStartColor ()
            -> setRGB ('99ddff');
        $objPHPExcel->getActiveSheet()->getStyle("A4:J4")->getFont()->setBold(true);
        $objPHPExcel->getActiveSheet()->getStyle('A1:J1')->applyFromArray($borders);
        $objPHPExcel->getActiveSheet()->getStyle('A2:J2')->applyFromArray($borders);
        $objPHPExcel->getActiveSheet()->getStyle('A3:J3')->applyFromArray($borders);
        $objPHPExcel->getActiveSheet()->getStyle('A4:J4')->applyFromArray($borders);

        $objPHPExcel->getActiveSheet()->getStyle("E3:J3")->getFont()->getColor()->setRGB("ffffff");
        $objPHPExcel->getActiveSheet()->getStyle("A3:J3")->getFont()->setBold(true);


        $objWorksheet = $objPHPExcel->getActiveSheet();
        $totalData = count($request);
        $row = 5;
        for ($i=0; $i < $totalData ; $i++) {
            $objPHPExcel->setActiveSheetIndex(0)
                ->setCellValueExplicit ("A$row", $request[$i]['idabono'], PHPExcel_Cell_DataType :: TYPE_STRING)
                ->setCellValueExplicit ("B$row", $request[$i]['idacuerdo'], PHPExcel_Cell_DataType :: TYPE_STRING)
                ->setCellValueExplicit ("C$row", $request[$i]['cierre'], PHPExcel_Cell_DataType :: TYPE_STRING)
                ->setCellValueExplicit ("D$row", $request[$i]['recibo'], PHPExcel_Cell_DataType :: TYPE_STRING)
                ->setCellValueExplicit ("E$row", $request[$i]['fecha'], PHPExcel_Cell_DataType :: TYPE_STRING)
                ->setCellValueExplicit ("F$row", $request[$i]['concepto'], PHPExcel_Cell_DataType :: TYPE_STRING)
                ->setCellValueExplicit ("G$row", $request[$i]['documento'], PHPExcel_Cell_DataType :: TYPE_STRING)
                ->setCellValueExplicit ("H$row", $request[$i]['nombre'], PHPExcel_Cell_DataType :: TYPE_STRING)
                ->setCellValueExplicit ("I$row", $request[$i]['total'], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
                ->setCellValueExplicit ("J$row", $request[$i]['tipomovimiento']== 201 ? "Activo" : "Reversado", PHPExcel_Cell_DataType :: TYPE_STRING);


            if($request[$i]['cierre'] > 0 && $request[$i]['recibo'] == "N/A"){
                $objPHPExcel-> getActiveSheet ()
                -> getStyle ("A$row:J$row")
                -> getFill ()
                -> setFillType (PHPExcel_Style_Fill :: FILL_SOLID)
                -> getStartColor ()
                -> setRGB ('ffc107');
                $objPHPExcel->getActiveSheet()->getStyle("A$row:J$row")->getFont()->setBold(true);
            }else{
                $objPHPExcel-> getActiveSheet ()
                -> getStyle ("A$row:J$row")
                -> getFill ()
                -> setFillType (PHPExcel_Style_Fill :: FILL_SOLID)
                -> getStartColor ()
                -> setRGB ('FFFFFF');
            }
            if($request[$i]['tipomovimiento'] ==201){
                $objPHPExcel->getActiveSheet()->getStyle("J$row")->getFont()->setBold(true);
                $objPHPExcel->getActiveSheet()->getStyle("J$row")->getFont()->getColor()->setRGB("198754");
            }else{
                $objPHPExcel->getActiveSheet()->getStyle("J$row")->getFont()->setBold(true);
                $objPHPExcel->getActiveSheet()->getStyle("J$row")->getFont()->getColor()->setRGB("ff0000");
            }
            $row++;
        }
        $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('I')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('J')->setAutoSize(true);
        //----Guardar documento----
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="reporte-abonos_predial.xlsx"');
        header('Cache-Control: max-age=0');
        $objWriter=PHPExcel_IOFactory::createWriter($objPHPExcel,'Excel2007');
        $objWriter->save('php://output');
        die();

?>
