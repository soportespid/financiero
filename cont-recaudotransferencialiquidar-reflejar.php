<?php
	header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=utf8");

	require "comun.inc";
	require "funciones.inc";
	require "conversor.php";
	require "validaciones.inc";

    require "./Librerias/core/Helpers.php";

	$linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");

	session_start();
	date_default_timezone_set("America/Bogota");
?>
<!DOCTYPE >
<html lang="es">
	<head>
		<meta charset="utf-8"/>
		<meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1"/>
		<meta name="viewport" content="user-scalable=no">
		<title>:: IDEAL 10 - Contabilidad</title>
		<link href="favicon.ico" rel="shortcut icon"/>

		<script>
			//************* ver reporte ************
			//***************************************
			function verep(idfac)
			{
				document.form1.oculto.value=idfac;
				document.form1.submit();
			}

			//************* genera reporte ************
			//***************************************
			function genrep(idfac)
			{
				document.form2.oculto.value=idfac;
				document.form2.submit();
			}

			function buscacta(e)
			{
				if (document.form2.cuenta.value!="")
				{
					document.form2.bc.value='1';
					document.form2.submit();
				}
			}

			function validar()
			{
				document.form2.submit();
			}

			function buscater(e)
			{
				if (document.form2.tercero.value!="")
				{
					document.form2.bt.value='1';
					document.form2.submit();
				}
			}

			function agregardetalle()
			{
				if(document.form2.codingreso.value!="" &&  document.form2.valor.value>0  )
				{ 
					document.form2.agregadet.value=1;
					//document.form2.chacuerdo.value=2;
					document.form2.submit();
				}
				else {
					alert("Falta informacion para poder Agregar");
				}
			}

			function eliminar(variable)
			{
				if (confirm("Esta Seguro de Eliminar"))
				{
					document.form2.elimina.value=variable;
					//eli=document.getElementById(elimina);
					vvend=document.getElementById('elimina');
					//eli.value=elimina;
					vvend.value=variable;
					document.form2.submit();
				}
			}

			//************* genera reporte ************
			//***************************************
			function guardar()
			{
				ingresos2=document.getElementsByName('dcoding[]');
                let estado = document.form2.estado.value;
                
                if(estado == "Reversado")
                {
                    alert("El recaudo ya fue reversado, no se puede guardar");
                    return;
                }
				if (document.form2.fecha.value!='' && ingresos2.length>0)
				{
					if (confirm("Esta Seguro de Guardar"))
					{
						document.form2.oculto.value=2;
						document.form2.submit();
					}
				}
				else{
					alert('Faltan datos para completar el registro');
					document.form2.fecha.focus();
					document.form2.fecha.select();
				}
			}

			function pdf()
			{
				document.form2.action="teso-pdfrecaudostrans.php";
				document.form2.target="_BLANK";
				document.form2.submit(); 
				document.form2.action="";
				document.form2.target="";
			}

			function buscater(e)
			{
				if (document.form2.tercero.value!="")
				{
					document.form2.bt.value='1';
					document.form2.submit();
				}
			}

			function buscaing(e)
			{
				if (document.form2.codingreso.value!="")
				{
					document.form2.bin.value='1';
					document.form2.submit();
				}
			}

			function adelante()
			{
				//alert("adelante"+document.form2.ncomp.value);
				//document.form2.oculto.value=2;
				if(parseFloat(document.form2.ncomp.value)<parseFloat(document.form2.maximo.value))
				{
					document.form2.oculto.value=1;
					//document.form2.agregadet.value='';
					//document.form2.elimina.value='';
					document.form2.ncomp.value=parseFloat(document.form2.ncomp.value)+1;
					document.form2.idcomp.value=parseFloat(document.form2.idcomp.value)+1;
					document.form2.action="cont-recaudotransferencialiquidar-reflejar.php";
					document.form2.submit();
				}
			}

			function atrasc()
			{
				//alert("atras"+document.form2.ncomp.value);
				//document.form2.oculto.value=2;
				if(document.form2.ncomp.value>1)
				{
					document.form2.oculto.value=1;
					//document.form2.agregadet.value='';
					//document.form2.elimina.value='';
					document.form2.ncomp.value=document.form2.ncomp.value-1;
					document.form2.idcomp.value=document.form2.idcomp.value-1;
					document.form2.action="cont-recaudotransferencialiquidar-reflejar.php";
					document.form2.submit();
				}
			}

			function validar2()
			{
				//   alert("Balance Descuadrado");
				document.form2.oculto.value=1;
				document.form2.ncomp.value=document.form2.idcomp.value;
				//document.form2.agregadet.value='';
				//document.form2.elimina.value='';
				document.form2.action="cont-recaudotransferencialiquidar-reflejar.php";
				document.form2.submit();
			}
			function iratras()
			{
				var idcomp=document.form2.idcomp.value;
				location.href="cont-reflejardocs.php";
			}

			function despliegamodalm(_valor,_tip,mensa,pregunta)
			{
				document.getElementById("bgventanamodalm").style.visibility=_valor;
				if(_valor=="hidden") {document.getElementById('ventanam').src="";}
				else
				{
					switch(_tip)
					{
						case "1":
							document.getElementById('ventanam').src="ventana-mensaje1.php?titulos="+mensa;break;
						case "2":
							document.getElementById('ventanam').src="ventana-mensaje3.php?titulos="+mensa;break;
						case "3":
							document.getElementById('ventanam').src="ventana-mensaje2.php?titulos="+mensa;break;
						case "4":
							document.getElementById('ventanam').src="ventana-consulta1.php?titulos="+mensa+"&idresp="+pregunta;break;	
					}
				}
			}
		</script>
		<script src="css/programas.js"></script>
		<script src="css/calendario.js"></script>
		<link href="css/css2.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<link href="css/tabs.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />

	</head>
	<body>
		<IFRAME src="alertas.php" name="alertas" id="alertas" style="display:none"></IFRAME>
		<span id="todastablas2"></span>
		<table>
			<tr><script>barra_imagenes("cont");</script><?php cuadro_titulos();?></tr>	 
			<tr><?php menu_desplegable("cont");?></tr>
			<tr>
				<td colspan="3" class="cinta">
					<a class="mgbt"><img src="imagenes/add2.png"/></a>
					<a class="mgbt"><img src="imagenes/guardad.png"/></a>
					<a class="mgbt"><img src="imagenes/buscad.png"/></a>
					<a onClick="mypop=window.open('plan-agenda.php','','');mypop.focus()" class="mgbt"><img src="imagenes/agenda1.png" title="Agenda"/></a>
					<a onClick="mypop=window.open('cont-principal.php','','');mypop.focus();" class="mgbt"><img src="imagenes/nv.png" tittle="Nueva ventana"></a>
					<a onClick="mypop=window.open('<?php echo $url2; ?>','','');mypop.focus();" class="tooltip bottom mgbt"><img src="imagenes/duplicar_pantalla.png" title="Duplicar pesta&ntilde;a">
					<a onClick="guardar()" class="mgbt"><img src="imagenes/reflejar1.png" style='width: 25px;' title="Reflejar"/></a>
					<a href="cont-reflejardocs.php" onClick="iratras()" class="mgbt"><img src="imagenes/iratras.png" title="Atr&aacute;s"></>
				</td>
    		</tr>		  
		</table>
		<div id="bgventanamodalm" class="bgventanamodalm">
            <div id="ventanamodalm" class="ventanamodalm">
                <IFRAME src="" name="ventanam" marginWidth=0 marginHeight=0 frameBorder=0 id="ventanam" frameSpacing=0 style=" width:700px; height:130px; top:200; overflow:hidden;"> 
                </IFRAME>
            </div>
        </div>
		<tr>
			<td colspan="3" class="tablaprin" align="center"> 
				<?php
				if(!$_POST['oculto'])
				{
					$sqlr="SELECT * FROM tesorecaudotransferencialiquidar WHERE 1 ORDER BY id_recaudo DESC";
					$res = mysqli_query($linkbd, $sqlr);
					$row = mysqli_fetch_row($res);
					$_POST['idcomp']=$row[0];
					if($_GET['consecutivo']!='')
						$_GET['idrecaudo']=$_GET['consecutivo'];
					else
						$_GET['idrecaudo']=$row[0];
					//echo $_POST[idcomp];
				}

				//$vigusu=vigencia_usuarios($_SESSION[cedulausu]);
				//$vigencia=$vigusu;
				//$_POST[vigencia]=$vigencia;
				$sqlr="select max(id_recaudo) from tesorecaudotransferencialiquidar ";
				$res=mysqli_query($linkbd, $sqlr);
				$consec=0;
				while($r=mysqli_fetch_row($res))
				{
					$_POST['maximo']=$r[0];	  
				}
	  			//*********** 11050501	CAJA PRINCIPAL esta es la cuenta que va a credito en todas las consignacones

				if($_POST['oculto']=="")
				{
					$sqlr="select distinct *, tesorecaudotransferencialiquidar.estado as estadoRec from tesorecaudotransferencialiquidar as tb1, tesorecaudotransferencialiquidar_det as tb2  where	tesorecaudotransferencialiquidar.tipo_mov = '101' AND  tesorecaudotransferencialiquidar.id_recaudo=$_GET[idrecaudo]  AND tesorecaudotransferencialiquidar.ID_recaudo=tesorecaudotransferencialiquidar_det.ID_recaudo and tesorecaudotransferencialiquidar_det.id_recaudo=$_GET[idrecaudo]";	
					$res=mysqli_query($linkbd, $sqlr);
					$cont=0;
					
					$_POST['idcomp']=$_GET['idrecaudo'];	
					$_POST['ncomp']=$_POST['idcomp'];
					$total=0;
					while ($row = mysqli_fetch_assoc($res))
					{
						$p1=substr($row['fecha'],0,4);
						$p2=substr($row['fecha'],5,2);
						$p3=substr($row['fecha'],8,2);
						$_POST['fecha']=$p3."/".$p2."/".$p1;
						/* $_POST['cc']=$row[8]; */
						$_POST['dcoding'][$cont]=$row['ingreso'];		 
						$_POST['banco']=$row['banco'];
						$_POST['dnbanco']=buscatercero($row['banco']);		 
						$_POST['dncoding'][$cont]=buscaingreso($row['ingreso']);
						$_POST['tercero']=$row['tercero'];
						$_POST['ntercero']=buscatercero($row['tercero']);
						$_POST['concepto']=$row['concepto'];
						$total=$total+$row['valor'];
						$_POST['totalc']=$total;
						$_POST['dseccion'][$cont]=$row['cc'];
						$_POST['dvalores'][$cont]=$row['valor'];
						$_POST['medioDePago']=$row['medio_pago'];
                        $_POST['estado']=$row['estadoRec'] == 'S' ? "Activo" : "Reversado";
                        $estiloEstado = $row['estadoRec'] == 'S' ? "color: green;" : "color: red;";
						$cont=$cont+1;	
					}
				}		 
				$_POST['dcoding']= array(); 		 
				$_POST['dncoding']= array(); 		 
				$_POST['dvalores']= array(); 
				$sqlr="select distinct *, tesorecaudotransferencialiquidar.estado as estadoRec from tesorecaudotransferencialiquidar, tesorecaudotransferencialiquidar_det   where	tesorecaudotransferencialiquidar.tipo_mov = '101' AND  tesorecaudotransferencialiquidar.id_recaudo=$_POST[idcomp]  AND tesorecaudotransferencialiquidar.ID_recaudo=tesorecaudotransferencialiquidar_det.ID_recaudo and tesorecaudotransferencialiquidar_det.id_recaudo=$_POST[idcomp]";	
				$res=mysqli_query($linkbd, $sqlr);
				$cont=0;
				//echo $sqlr;
				//$_POST[idcomp]=$_GET[idrecaudo];	
				$total=0;
				while ($row =mysqli_fetch_assoc($res)) 
				{	
					$p1=substr($row['fecha'],0,4);
					$p2=substr($row['fecha'],5,2);
					$p3=substr($row['fecha'],8,2);
					$_POST['vigencia']=$row['vigencia'];
					$_POST['fecha']=$p3."/".$p2."/".$p1;	
					/* $_POST['cc']=$row[8]; */
					$_POST['dcoding'][$cont]=$row['ingreso'];			 
					$_POST['banco']=$row['banco'];		 
					$_POST['dnbanco']=buscatercero($row['banco']);		 
					$_POST['dncoding'][$cont]=buscaingreso($row['ingreso']);
					$_POST['tercero']=$row['tercero'];
					$_POST['ntercero']=buscatercero($row['tercero']);
					$_POST['concepto']=$row['concepto'];
					$total=$total+$row['valor']; 
					$_POST['totalc']=$total;
					$_POST['dseccion'][$cont]=$row['cc'];
					$_POST['dvalores'][$cont]=$row['valor'];
					$_POST['medioDePago']=$row['medio_pago'];
                    $_POST['estado']=$row['estadoRec'] == 'S' ? "Activo" : "Reversado";
                    $estiloEstado = $row['estadoRec'] == 'S' ? "color: green;" : "color: red;";
					$cont=$cont+1;		
				}	
	
				if(!$_POST['oculto'])
				{
					$check1="checked";
					$fec=date("d/m/Y");

					$sqlr = "select *from cuentacaja where estado='S' and vigencia=$_POST[vigencia]";
					$res = mysqli_query($linkbd, $sqlr);
					while ($row = mysqli_fetch_row($res)) 
					{
						$_POST['cuentacaja']=$row[1];
					}
				}	

				switch($_POST['tabgroup1'])
				{
					case 1:
						$check1='checked';
					break;
					case 2:
						$check2='checked';
					break;
					case 3:
						$check3='checked';
				}
				?>
 				<form name="form2" method="post" action=""> 
 					<?php
 					//***** busca tercero
					if($_POST['bt']=='1')
					{
			  			$nresul=buscatercero($_POST['tercero']);
						if($nresul!='')
						{
			  				$_POST['ntercero']=$nresul;
			  			}
						else
						{
			  				$_POST['ntercero']="";
			  			}
					}
					//******** busca ingreso *****
					//***** busca tercero
					if($_POST['bin']=='1')
					{
						$nresul=buscaingreso($_POST['codingreso']);
						if($nresul!='')
						{
			  				$_POST['ningreso']=$nresul;
			  			}
						else
						{
							$_POST['ningreso']="";
						}
			 		}		 
 					?>
					<table class="inicio" align="center" >
						<tr >
							<td class="titulos" colspan="10">Liquidar Recaudos Transferencias</td>
							<td  class="cerrar" ><a href="teso-principal.php">Cerrar</a></td>
						</tr>
						<tr>
							<td  class="saludo1" >Numero Recaudo:</td>
							<td  style="width:10%;">
								<a href="#" onClick="atrasc()">
									<img src="imagenes/back.png" alt="anterior" align="absmiddle">
								</a>
								<input name="idcomp" type="text" value="<?php echo $_POST['idcomp']?>" onKeyUp="return tabular(event,this)" style="width:50%;" onBlur='validar2()' >
								<input name="ncomp" type="hidden" value="<?php echo $_POST['ncomp']?>">
								<a href="#" onClick="adelante()">
									<img src="imagenes/next.png" alt="siguiente" align="absmiddle">
								</a>
								<input type="hidden" value="a" name="atras" >
								<input type="hidden" value="s" name="siguiente" >
								<input type="hidden" value="<?php echo $_POST['maximo']?>" name="maximo">
							</td>
	  						<td  class="saludo1">Fecha:        </td>
        					<td style="width:10%;" >
        						<input name="fecha" type="text" id="fc_1198971545" title="DD/MM/YYYY" style="width:80%;" value="<?php echo $_POST['fecha']; ?>" onKeyUp="return tabular(event,this) " onKeyDown="mascara(this,'/',patron,true)"  maxlength="10" readonly>
        					</td>
							<td  class="saludo1">Vigencia:</td>
							<td style="width:15%;">
		  						<input type="text" id="vigencia" name="vigencia" style="width:30%;" onKeyPress="javascript:return solonumeros(event)" onKeyUp="return tabular(event,this)"  value="<?php echo $_POST['vigencia']?>" onClick="document.getElementById('tipocta').focus(); document.getElementById('tipocta').select();" readonly> 
							</td>
							<td style="width:10%;" class="saludo1">Causacion Contable:</td>
							<td>
								<select name="causacion" id="causacion" onKeyUp="return tabular(event,this)"  >
									<option value="1" <?php if($_POST['causacion']=='1') echo "SELECTED"; ?>>Si</option>
									<option value="2" <?php if($_POST['causacion']=='2') echo "SELECTED"; ?>>No</option>         
								</select>
							</td>
							<td class="saludo1" style="width:3cm;">Medio de pago: </td>
							<td style="width:14%;">
								<select name="medioDePago" id="medioDePago" onKeyUp="return tabular(event,this)" style="width:80%" disabled>
									<option value="1" <?php if(($_POST['medioDePago']=='1')) echo "SELECTED"; ?>>Con SF</option>
									<option value="2" <?php if($_POST['medioDePago']=='2') echo "SELECTED"; ?>>Sin SF</option>         
								</select>
								<input type="hidden" name="vigencia"  value="<?php echo $_POST['vigencia']?>" onKeyUp="return tabular(event,this)" readonly/>
							</td>          
						</tr>
						<tr>
							<td  class="saludo1">Concepto Recaudo:</td>
							<td colspan="7" >
								<input name="concepto" type="text" value="<?php echo $_POST['concepto']?>" style="width:100%;" onKeyUp="return tabular(event,this)" readonly>
							</td>
                            <td class="saludo1">Estado:</td>
                            <td>
                                <input type="text" name="estado" value="<?php echo $_POST['estado']; ?>" <?php echo "style='$estiloEstado'"; ?> readonly>
                            </td>
						</tr>  
						<tr>
							<td  class="saludo1">NIT: </td>
							<td style="width:10%;">
								<input name="tercero" type="text" value="<?php echo $_POST['tercero']?>" onKeyUp="return tabular(event,this)" style="width:70%;" onBlur="buscater(event)" readonly>
							</td>
							<td class="saludo1">Contribuyente:</td>
							<td colspan="5"  >
								<input type="text" id="ntercero" name="ntercero" value="<?php echo $_POST['ntercero']?>" style="width:100%;" onKeyUp="return tabular(event,this) "  readonly>
								<input type="hidden" value="0" name="bt">
								<input type="hidden" id="cb" name="cb" value="<?php echo $_POST['cb']?>" >
								<input type="hidden" id="ct" name="ct" value="<?php echo $_POST['ct']?>" >
								<input type="hidden" value="1" name="oculto">
							</td>
						</tr>
    				</table>
       				<?php
           			//***** busca tercero
					if($_POST['bt']=='1')
					{
						$nresul=buscatercero($_POST['tercero']);
						if($nresul!='')
						{
			  				$_POST['ntercero']=$nresul;
  							?>
							<script>
			  					document.getElementById('codingreso').focus();document.getElementById('codingreso').select();
							</script>
			  				<?php
			  			}
						else
						{
							$_POST['ntercero']="";
							?>
							<script>
								alert("Tercero Incorrecto o no Existe")				   		  	
								document.form2.tercero.focus();	
							</script>
							<?php
			  			}
			 		}
					//*** ingreso
					if($_POST['bin']=='1')
					{
						$nresul=buscaingreso($_POST['codingreso']);
						if($nresul!='')
						{
							$_POST['ningreso']=$nresul;
							?>
							<script>
							document.getElementById('valor').focus();document.getElementById('valor').select();</script>
							<?php
			  			}
						else
						{
							$_POST['codingreso']="";
							?>
							<script>alert("Codigo Ingresos Incorrecto");document.form2.codingreso.focus();</script>
							<?php
			  			}
			 		}
			 		?>
					<div class="subpantalla">
						<table class="inicio">
							<tr>
								<td colspan="5" class="titulos">Detalle  Liquidar Recaudos Transferencia</td>
							</tr>                  
							<tr>
								<td class="titulos2" style='width:5%;'>Codigo</td>
								<td class="titulos2" style='width:80%;'>Ingreso</td>
								<td class="titulos2" style='width:10%;'>Seccion presupuestal</td>
								<td class="titulos2" style='width:10%;'>Valor</td>
							</tr>
							<?php 		
							if ($_POST['elimina']!='')
							{ 
								//echo "<TR><TD>ENTROS :".$_POST[elimina]."</TD></TR>";
								$posi=$_POST['elimina'];
								
								unset($_POST['dcoding'][$posi]);	
								unset($_POST['dncoding'][$posi]);			 
								unset($_POST['dseccion'][$posi]);			  		 
								unset($_POST['dvalores'][$posi]);			  		 
								$_POST['dcoding']= array_values($_POST['dcoding']); 		 
								$_POST['dncoding']= array_values($_POST['dncoding']); 		 		 
								$_POST['dseccion']= array_values($_POST['dseccion']); 		 		 		 		 		 
								$_POST['dvalores']= array_values($_POST['dvalores']); 		 		 		 		 		 
							}	 
							if ($_POST['agregadet']=='1')
							{
								$_POST['dcoding'][]=$_POST['codingreso'];
								$_POST['dncoding'][]=$_POST['ningreso'];			 		
								$_POST['dseccion'][]=$_POST['cc'];
								$_POST['dvalores'][]=$_POST['valor'];
								$_POST['agregadet']=0;
								?>
								<script>
										//document.form2.cuenta.focus();	
										document.form2.codingreso.value="";
										document.form2.valor.value="";	
										document.form2.ningreso.value="";				
										document.form2.codingreso.select();
										document.form2.codingreso.focus();	
								</script>
		  						<?php
		  					}
		  					$_POST['totalc']=0;
							for ($x=0;$x<count($_POST['dcoding']);$x++)
							{		 
								echo "<tr>
										<td class='saludo1'>
											<input name='dcoding[]' value='".$_POST['dcoding'][$x]."' type='text' style='width:100%;' readonly>
										</td>
										<td class='saludo1'>
											<input name='dncoding[]' value='".$_POST['dncoding'][$x]."' type='text' style='width:100%;' readonly>
										</td>
										<td class='saludo1'>
											<input name='dseccion[]' value='".$_POST['dseccion'][$x]."' type='text' style='width:100%;' readonly>
										</td>
										<td class='saludo1'>
											<input name='dvalores[]' value='".$_POST['dvalores'][$x]."' type='text' style='width:100%;' readonly>
										</td>
									</tr>";
								$_POST['totalc']=$_POST['totalc']+$_POST['dvalores'][$x];
								$_POST['totalcf']=number_format($_POST['totalc'],2);
		 					}
							$resultado = convertir($_POST['totalc']);
							$_POST['letras']=$resultado." Pesos";
							echo "<tr>
									<td>
										</td>
										<td class='saludo2' colspan='2'>Total</td>
										<td class='saludo1'>
											<input name='totalcf' type='text' value='$_POST[totalcf]' style='width:100%;' readonly>
											<input name='totalc' type='hidden' value='$_POST[totalc]' style='width:100%;' readonly>
										</td>
									</tr>
									<tr>
										<td class='saludo1'>Son:</td>
										<td colspan='3'>
											<input name='letras' type='text' value='$_POST[letras]' style='width:100%;' readonly>
										</td>
									</tr>";
							?> 
	   					</table>
					</div>
	  				<?php
					if($_POST['oculto']=='2')
					{
						preg_match("/([0-9]{2})\/([0-9]{2})\/([0-9]{4})/", $_POST['fecha'], $fecha);
						$fechaf=$fecha[3]."-".$fecha[2]."-".$fecha[1];
						$bloq=bloqueos($_SESSION['cedulausu'],$fechaf);	
						if($bloq>=1)
						{
							//*********************CREACION DEL COMPROBANTE CONTABLE ***************************
							$sqlr="delete from comprobante_cab where numerotipo='$_POST[idcomp]' and tipo_comp='28'";
							//echo $sqlr.'</br>';
							mysqli_query($linkbd, $sqlr);
							$sqlr="delete from comprobante_det where numerotipo='$_POST[idcomp]' and tipo_comp='28'";
							//echo $sqlr.'</br>';
							mysqli_query($linkbd, $sqlr);
							//***busca el consecutivo del comprobante contable
							$consec=$_POST['idcomp'];	
							//echo  "Ca:".$_POST[causacion];
							if($_POST['causacion']=='2')
							{
								$_POST['concepto']="ESTE DOCUMENTO NO REQUIERE CAUSACION CONTABLE - ".$_POST['concepto'];
							}
							//***cabecera comprobante
							$sqlr="insert into comprobante_cab (numerotipo,tipo_comp,fecha,concepto,total,total_debito,total_credito,diferencia,estado) values ($consec,28,'$fechaf','".strtoupper($_POST['concepto'])."',0,$_POST[totalc],$_POST[totalc],0,'1')";
							//	echo $sqlr.'</br>';
							if(mysqli_query($linkbd, $sqlr))	
							{
								//echo "$sqlr <br>";	
							}
							else
							{echo "error:".mysqli_error($linkbd);}
							$idcomp=mysqli_insert_id($linkbd);
							$sqlr="delete from comprobante_det where id_comp='28 $_POST[idcomp]'";
							//echo $sqlr.'</br>';
							mysqli_query($linkbd, $sqlr);	
							echo "<input type='hidden' name='ncomp' value='$consec'>";
							if($_POST['causacion']!='2')
							{
								//******************* DETALLE DEL COMPROBANTE CONTABLE *********************
								for($x=0;$x<count($_POST['dcoding']);$x++)
								{
									//***** BUSQUEDA INGRESO ********
									$sqlri="Select * from tesoingresos_det where codigo='".$_POST['dcoding'][$x]."' AND id_det= (SELECT MAX(id_det) FROM tesoingresos_det where codigo='".$_POST['dcoding'][$x]."') ";
									//echo $sqlri.'</br>';
									$resi=mysqli_query($linkbd, $sqlri);
									//echo "$sqlri <br>";	    
									while($rowi=mysqli_fetch_row($resi))
									{
                                        $seccionP = getCentroCosto($_POST['dseccion'][$x]);
										//**** busqueda concepto contable*****
										$sq="select fechainicial from conceptoscontables_det where codigo='".$rowi[2]."' and modulo='4' and tipo='C' and fechainicial<'$fechaf' and cuenta!='' and cc='".$seccionP['id_cc']."' order by fechainicial asc";
										$re=mysqli_query($linkbd, $sq);
										while($ro=mysqli_fetch_assoc($re))
										{
											$_POST['fechacausa']=$ro["fechainicial"];
										}
										$sqlrc="Select * from conceptoscontables_det where estado='S' and modulo='4' AND codigo=".$rowi[2]." and tipo='C' and fechainicial='".$_POST['fechacausa']."' and cc='".$seccionP['id_cc']."'";
										//echo $sqlrc.'</br>';
										$resc=mysqli_query($linkbd, $sqlrc);	  
										//echo "con: $sqlrc <br>";	      
										while($rowc=mysqli_fetch_row($resc))
										{
											$porce=$rowi[5];
											if($seccionP['id_cc']==$rowc[5])
											{
												if($rowc[3]=='N')
												{				
													if($rowc[6]=='S')
													{
														$valordeb=$_POST['dvalores'][$x]*($porce/100);
														$valorcred=0;
													}
													if($rowc[7]=='S')
													{
														$valorcred=$_POST['dvalores'][$x]*($porce/100);
														$valordeb=0;
													}
													$sqlr="insert into comprobante_det (id_comp,cuenta,tercero,centrocosto,detalle,cheque,valdebito,valcredito,estado,vigencia) values ('28 $consec','".$rowc[4]."','".$_POST['tercero']."','".$seccionP['id_cc']."','Recaudo Transferencia".strtoupper($_POST['dncoding'][$x])."','',".$valordeb.",".$valorcred.",'1','".$_POST['vigencia']."')";

													//echo $sqlr.'</br>';
													mysqli_query($linkbd, $sqlr);			   
												}			   
											}
											//echo "Conc: $sqlr <br>";
										}
									}
								}
							}
							//************ insercion de cabecera recaudos ************

							/* $sqlr="delete from tesorecaudotransferencialiquidar where id_recaudo='$consec'";
							mysqli_query($linkbd, $sqlr);
		
							$sqlr="delete from tesorecaudotransferencialiquidar_det where id_recaudo='$consec'";
							mysqli_query($linkbd, $sqlr);

							$sqlr="insert into tesorecaudotransferencialiquidar (id_recaudo,idcomp,fecha,vigencia,banco,ncuentaban,concepto,tercero,cc,valortotal,estado,medio_pago) values($consec,$idcomp,'$fechaf','$_POST[vigencia]','$_POST[ter]','$_POST[cb]','".strtoupper($_POST['concepto'])."','$_POST[tercero]','$_POST[cc]','$_POST[totalc]','S','$_POST[medioDePago]')";	  
							mysqli_query($linkbd, $sqlr);

							$idrec=mysqli_insert_id($linkbd);
							for($x=0;$x<count($_POST['dcoding']);$x++)
							{
								$sqlr="insert into tesorecaudotransferencialiquidar_det (id_recaudo,ingreso,valor,estado) values($consec,'".$_POST['dcoding'][$x]."',".$_POST['dvalores'][$x].",'S')";	

								if (!mysqli_query($linkbd, $sqlr))
								{
									echo "<table ><tr><td class='saludo1'><center><font color=blue><img src='imagenes/alert.png'> Manejador de Errores de la Clase BD<br><font size=1></font></font><br><p align=center>No se pudo ejecutar la petici�n: <br><font color=red><b>$sqlr</b></font></p>";
									
									echo "Ocurri� el siguiente problema:<br>";
									
									echo "<pre>";
									echo "</pre></center></td></tr></table>";
								}
								else
								{			 
									echo "<table  class='inicio'><tr><td class='saludo1'><center>Se ha almacenado el Recaudo con Exito <img src='imagenes/confirm.png'></center></td></tr></table>";
								}
							} */
							echo "<table  class='inicio'><tr><td class='saludo1'><center>Se ha almacenado el Recaudo con Exito <img src='imagenes/confirm.png'></center></td></tr></table>";
						}//***bloqueo
						else
						{
							//echo "<div class='inicio'><img src='imagenes\alert.png'> No Tiene los Permisos para Modificar este Documento</div>";
							?>
							<script>
								//alert("¡No se puede reflejar por Bloqueo de Fecha!");
								despliegamodalm('visible','2',"No Tiene los Permisos para Modificar este Documento");
							</script>
							<?php
						}
					}
					?>	
					<div id="bgventanamodal2">
						<div id="ventanamodal2">
							<IFRAME  src="" name="buster" marginWidth=0 marginHeight=0 frameBorder=0 id="ventana2" frameSpacing=0 style="left:500px; width:900px; height:500px; top:200;"> 
							</IFRAME>
						</div>
					</div>	
				</form>
 			</td>
		</tr>
	</table>
	</body>
</html> 		
