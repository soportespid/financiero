<?php
	require_once("tcpdf/tcpdf_include.php");
	require('comun.inc');
	require "funciones.inc";
	date_default_timezone_set("America/Bogota");
	session_start();
    $linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");
	date_default_timezone_set("America/New_York");

    $sqlr="SELECT funcionario, nomcargo FROM firmaspdf_det WHERE idfirmas='6' AND estado ='S'";
	$res=mysqli_query($linkbd,$sqlr);
	$rowCargo=mysqli_fetch_row($res);

    $_POST['ppto'][] = $rowCargo[0];
    $_POST['nomcargo'][] = $rowCargo[1];
	class MYPDF extends TCPDF 
	{
		public function Header() 
		{
			if ($_POST['estado']=='R'){$this->Image('imagenes/reversado02.png',75,41.5,50,15);}
			$linkbd = conectar_v7();
			$linkbd -> set_charset("utf8");
			$sqlr="select *from configbasica where estado='S' ";
			//echo $sqlr;
			$res=mysqli_query($linkbd, $sqlr);
			while($row=mysqli_fetch_row($res))
			{
				$nit=$row[0];
				$rs=$row[1];
				
			}
			
			//Parte Izquierda
			$this->Image('imagenes/escudo.jpg',  13, 13, 20, 20, 'JPG', '', 'T', true, 300, '', false, false, 0, false, false, false);// Logo
			$this->SetFont('helvetica','B',8);
			$this->SetY(10);
			$this->RoundedRect(10, 10, 277, 25, 1,'');
			$this->Cell(0.1);
			$this->Cell(26,25,'','R',0,'L'); 
			$this->SetY(8);
			$this->SetX(80);
			$this->SetFont('helvetica','B',9);
			$this->Cell(160,15,strtoupper("$rs"),0,0,'C'); 
			$this->SetFont('helvetica','B',7);
			$this->SetY(12);
			$this->SetX(80);
			$this->Cell(160,15,'NIT: '.$nit,0,0,'C');
			//*****************************************************************************************************************************
			$this->SetFont('helvetica','B',9);			 
			$this->SetY(23);
			$this->SetX(36);
            $this->Cell(251,12,strtoupper($_POST['movimiento']),'T',0,'C');
            $this->SetFont('helvetica','B',6);
			$this->SetY(10);
			$this->SetX(257);
			$this->Cell(30,5," NUMERO: ".$_POST['consecutivo'],"L",0,'L');
			$this->SetY(13.5);
            $vigencia = explode('/', $_POST['fecha']);     
			$this->SetX(257);
			$this->Cell(35,6," FECHA: ".$_POST['fecha'],"L",0,'L');
			$this->SetY(18);
			$this->SetX(257);
			$this->Cell(35,5," VIGENCIA: ".$vigencia[2],"L",0,'L');

			

			/*
			$this->SetFont('helvetica','B',10);
			$this->SetY(15);
			$this->Cell(50.1);
			$this->Cell(149,20,$mov,0,0,'C'); 
			//************************************
			$this->SetFont('helvetica','I',7);
			$this->SetY(27);
			$this->Cell(35);
			//$this->multiCell(110.7,3,''.strtoupper($detallegreso),'T','L');
			$this->multiCell(242,3,'','T','L');
			$this->SetFont('helvetica','B',10);
			$this->SetY(27);
			$this->Cell(237);
			$this->Cell(37.8,14,'','TL',0,'L');
			$this->SetY(27);
			$this->Cell(238);
			$this->Cell(35,5,'NUMERO : '.$_POST['idcomp'],0,0,'L');
			$this->SetY(31);
			$this->Cell(238);
			$this->Cell(35,5,'FECHA: '.$_POST['fecha'],0,0,'L');
			$this->SetY(35);
			$this->Cell(238);
			$this->Cell(35,5,'VIGENCIA: '.$_POST['vigencia'],0,0,'L');
			$this->SetY(27);
			$this->Cell(50.2);
			$this->MultiCell(105.7,4,'',0,'L');		
			*/
			//**********************************************************
			$this->SetFont('times','B',10);
			$this->ln(12);
			//**********************************************************
		}
		public function Footer() 
		{
			$linkbd = conectar_v7();
			$linkbd -> set_charset("utf8");
			$sqlr="SELECT direccion,telefono,web,email FROM configbasica WHERE estado='S'";
			$resp=mysqli_query($linkbd, $sqlr);
			$user = $_SESSION['nickusu'];	
			$fecha = date("Y-m-d H:i:s");
			$ip = $_SERVER['REMOTE_ADDR'];
			$useri = $_POST['user'];
			while($row=mysqli_fetch_row($resp))
			{
				$direcc=strtoupper($row[0]);
				$telefonos=$row[1];
				$dirweb=strtoupper($row[3]);
				$coemail=strtoupper($row[2]);
			}
			if($direcc!=''){$vardirec="Dirección: $direcc, ";}
			else {$vardirec="";}
			if($telefonos!=''){$vartelef="Telefonos: $telefonos";}
			else{$vartelef="";}
			if($dirweb!=''){$varemail="Email: $dirweb, ";}
			else {$varemail="";}
			if($coemail!=''){$varpagiw="Pagina Web: $coemail";}
			else{$varpagiw="";}
			$this->SetFont('helvetica', 'I', 8);
			$txt = <<<EOD
			$vardirec $vartelef
			$varemail $varpagiw
			EOD;
			$this->SetFont('helvetica', 'I', 6);
			$this->Cell(277,10,'','T',0,'T');
			$this->ln(2);
			$this->Write(0, $txt , '', 0, 'C', true, 0, false, false, 0);
            /*
            $sqlru="SELECT * FROM almginventario WHERE tipomov = '".$_POST['movr']."' AND tiporeg = '".$_POST['entr']."' AND consec = '".$_POST['numero']."'";
            $respo=mysqli_query($linkbd, $sqlru);
            $rowu=mysqli_fetch_row($respo);
            //echo $rowu[6];

            $sqlrcc="SELECT * FROM `usuarios` WHERE `cc_usu` = '$rowu[6]'";
            $respcc=mysqli_query($linkbd, $sqlrcc);
            $rowcc=mysqli_fetch_row($respcc);
            //echo $rowcc[1];
            */
			//$this->Cell(50, 10, 'Hecho por: '.$rowcc[1], 00, false, 'L', 0, '', 0, false, 'T', 'M');
			$this->Cell(70, 10, 'Impreso por: '.$user, 0, false, 'C', 0, '', 0, false, 'T', 'M');
			$this->Cell(30, 10, 'IP: '.$ip, 0, false, 'C',0, '', 0, false, 'T', 'M');
			$this->Cell(80, 10, 'Fecha: '.$fecha, 0, false, 'C', 0, '', 0, false, 'T', 'M');
			$this->Cell(33, 10, 'IDEAL.10 S.A.S', 0, false, 'C', 0, '', 0, false, 'T', 'M');
            $this->Cell(25, 10, 'Pagina '.$this->getAliasNumPage().'/'.$this->getAliasNbPages(), 0, false, 'C', 0, '', 0, false, 'T', 'M');

			
		}
	}
	$pdf = new MYPDF('L','mm','Letter', true, 'iso-8859-1', false);
	$pdf->SetDocInfoUnicode (true); 
	// set document information
	$pdf->SetCreator(PDF_CREATOR);
	$pdf->SetAuthor('IDEALSAS');
	$pdf->SetTitle('ACTO ADMINISTRATIVO POR AJUSTE');
	$pdf->SetSubject('ACTO ADMINISTRATIVO POR AJUSTE');
	$pdf->SetKeywords('TCPDF, PDF, example, test, guide');
	$pdf->SetMargins(10, 38, 10);// set margins
	$pdf->SetHeaderMargin(38);// set margins
	$pdf->SetFooterMargin(17);// set margins
	$pdf->SetAutoPageBreak(TRUE, 20);// set auto page breaks
	// set some language-dependent strings (optional)
	if (@file_exists(dirname(__FILE__).'/lang/spa.php')) 
	{
		require_once(dirname(__FILE__).'/lang/spa.php');
		$pdf->setLanguageArray($l);
	}	
	$pdf->AddPage();
    $pdf->SetFont('helvetica','I',9);
    $fechaen = explode('-', $_POST['fecha']);
	if($fechaen[1]==''){
        $fechaen = explode('/', $_POST['fecha']);
    }
		
    $infoinicio=strtoupper("En la ciudad de $_POST[ciudad] a los $fechaen[0] días del mes de $mesen del $fechaen[2] en el lugar $_POST[lugar], se procede a la entrada por ajuste de los bienes, efectuada por $_POST[nomTercero].");

	$pdf->SetFont('helvetica','B',9);
	$pdf->SetY(40);
	$pdf->MultiCell(277,4,'EL SUSCRITO '.$_POST['nomcargo'][0],'','C');
	$pdf->Cell(277,6,'CERTIFICA:',0,0,'C');
	$pdf->SetY(50);
	$pdf->SetFont('helvetica','',7);
	$pdf->cell(0.1);
	$pdf->MultiCell(277,3,$infoinicio,0,'C',false,1,'','',true,0,false,true,0,'T',false);
	$pdf->ln(2);
	$pdf->SetFont('helvetica','B',7);
	$pdf->MultiCell(277,3,strtoupper($_POST['descripcion']),0,'L',false,1,'','',true,0,false,true,0,'T',false);
	$pdf->SetFont('helvetica','',10);
	//$pdf->Cell(35,10,$_POST['solicita'],1,1,'C',false,0,0,false,'T','C');

	

    
	$pdf->ln(2);	
	$y=$pdf->GetY();	
	$pdf->SetY($y);

	$y=$pdf->GetY();	
	$pdf->SetFillColor(222,222,222);
	$pdf->SetFont('helvetica','B',7);
	//$pdf->SetY($y);
    $pdf->Cell(0.1);
    $pdf->Cell(35,5,'Articulo',0,0,'C',1); 
	$pdf->SetY($y);
	$pdf->Cell(36);
	$pdf->Cell(60,5,'Nombre articulo',0,0,'C',1);
	$pdf->SetY($y);
	$pdf->Cell(97);
	$pdf->Cell(45,5,'Unidad de medida',0,0,'C',1);
	$pdf->SetY($y);
	$pdf->SetFont('helvetica','B',7);
	$pdf->Cell(143);
	$pdf->Cell(35,5,'Estado articulo',0,0,'C',1);
	$pdf->SetY($y);
	$pdf->Cell(179);
	$pdf->MultiCell(32,5,'Cantidad',0,'C',1,1,'','',true,'',false,true,0,'M',true);
	$pdf->SetY($y);
	$pdf->Cell(212);
    $pdf->MultiCell(32,5,'Valor unitario',0,'C',1,1,'','',true,'',false,true,0,'M',true);
	$pdf->SetY($y);
	$pdf->Cell(245);
    $pdf->MultiCell(32,5,'Valor total',0,'C',1,1,'','',true,'',false,true,0,'M',true);
	$pdf->SetFont('helvetica','',6);
	$cont=0;
	$pdf->ln(1);
	
    $con=0;
    $total=0;
   
    while ($con<count($_POST['articulo'])) 
	{
        if($vv>=170)
        { 
            $pdf->AddPage();
            $vv=$pdf->gety();
        }
        if ($con%2==0){
            $pdf->SetFillColor(255,255,255);
        }
        else {
            $pdf->SetFillColor(245,245,245);
        }
        $cant=$row[3];
	    if($cant==0){
            $cant=$row[4];
        }
		
       
        $lineasnombres = $pdf ->getNumLines($_POST['nomArticulo'][$con], 31);
		$lineasnombress = $pdf ->getNumLines($_POST['unidad'][$con], 31);
        $valorMaxs = max($lineasnombres, $lineasnombress);
        $alturas = $valorMaxs * 3;

        $pdf->cell(0.2);
        $pdf->MultiCell(35,$alturas,strtoupper($_POST['articulo'][$con]),0,'C',true,0,'','',true,0,false,true,0,'M',true);
        $pdf->MultiCell(61,$alturas,$_POST['nomArticulo'][$con],0,'C',true,0,'','',true,0,false,true,0,'M',true);
        $pdf->MultiCell(46,$alturas,$_POST['unidad'][$con],0,'C',true,0,'','',true,0,false,true,0,'M',true);
        $pdf->MultiCell(36,$alturas,$_POST['estado'][$con],0,'C',true,0,'','',true,0,false,true,0,'M',true);
        $pdf->MultiCell(33,$alturas,$_POST['cantidad'][$con],0,'C',true,0,'','',true,0,false,true,0,'M',true);
        $pdf->MultiCell(33,$alturas,'$ '.number_format($_POST['valorUnitario'][$con], 2),0,'R',true,0,'','',true,0,false,true,0,'M',true);
        $pdf->MultiCell(33,$alturas,'$ '.number_format($_POST['valorTotal'][$con],2),0,'R',true,1,'','',true,0,false,true,0,'M',true);
        $total=$total+$_POST['valorTotal'][$con];		
		$con++;

	}
	//$pdf->ln(4);
	//echo $pdf->GetY();
    $pdf->ln(4);
    $v=$pdf->gety();
    $x=$pdf->getx();
    $pdf->line(10.1,$v-2,287,$v-2);
    $pdf->Cell(185);
    $pdf->SetFont('helvetica','B',7);
    $pdf->SetX(248);
    $pdf->Cell(20,3,'Total',0,0,'C');
    $pdf->Cell(20,3,'$ '.number_format($total, 2),0,1,'C');
  
	$pdf->ln(5);

    
        $v=$pdf->gety();
		if($v>=155)
		{ 
			$pdf->AddPage();
			$pdf->ln(15);
			$v=$pdf->gety();
		}
		for($x=0;$x<count($_POST['ppto']);$x++)
		{
			$pdf->ln(20);
			$v=$pdf->gety();
			if($v>=180){
				$pdf->AddPage();
				$pdf->ln(20);
				$v=$pdf->gety();
			}
			$pdf->setFont('times','B',8);
			if (($x%2)==0) {
				if(isset($_POST['ppto'][$x+1])){
					$pdf->Line(17,$v,107,$v);
					$pdf->Line(112,$v,202,$v);
					$v2=$pdf->gety();
					$pdf->Cell(104,4,''.$_POST['ppto'][$x],0,1,'C',false,0,0,false,'T','C');
					$pdf->Cell(200,4,''.$_POST['nomcargo'][$x],0,1,'C',false,0,0,false,'T','C');
					$pdf->SetY($v2);
					$pdf->Cell(295,4,''.$_POST['ppto'][$x+1],0,1,'C',false,0,0,false,'T','C');
					$pdf->Cell(200,4,''.$_POST['nomcargo'][$x+1],0,1,'C',false,0,0,false,'T','C');
				}else{
					$pdf->Line(100,$v,200,$v);
					$pdf->Cell(280,4,''.$_POST['ppto'][$x],0,1,'C',false,0,0,false,'T','C');
					$pdf->Cell(280,4,''.$_POST['nomcargo'][$x],0,0,'C',false,0,0,false,'T','C');
				}
				$v3=$pdf->gety();
			}
			$pdf->SetY($v3);
			$pdf->SetFont('helvetica','',7);
		}

	$pdf->Output('actoadmajuste.pdf', 'I');