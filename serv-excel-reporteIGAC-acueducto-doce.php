<?php
	require_once 'PHPExcel/Classes/PHPExcel.php';
    include '/PHPExcel/Classes/PHPExcel/IOFactory.php';
	require "comun.inc";
	require "funciones.inc";
    require "funcionesSP.inc.php";
    ini_set('max_execution_time',99999999);
    header("Content-type: application/json");
	session_start();
	$linkbd=conectar_v7();

	$objPHPExcel = new PHPExcel();
	
	$objPHPExcel->getProperties()
		->setCreator("IDEAL10")
		->setLastModifiedBy("IDEAL10")
		->setTitle("Reporte de recaudos por periodo liquidado")
		->setSubject("SP")
		->setDescription("SP")
		->setKeywords("SP")
		->setCategory("SERVICIOS PUBLICOS");
	$objPHPExcel->getActiveSheet()->mergeCells('A1:AV1');
	$objPHPExcel->setActiveSheetIndex(0)->setCellValue('A1', "Reporte IGAC Acueducto periodo $_POST[corte]");
	$objFont=$objPHPExcel->getActiveSheet()->getStyle('A1')->getFont();
	$objFont->setName('Courier New');
	$objFont->setSize(15);
	$objFont->setBold(true);
	$objFont->setUnderline(PHPExcel_Style_Font::UNDERLINE_SINGLE);
	$objFont->getColor()->setARGB( PHPExcel_Style_Color::COLOR_BLACK);
	$objAlign=$objPHPExcel->getActiveSheet()->getStyle('A1')->getAlignment(); 
	$objAlign->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
	$objAlign->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
	$objPHPExcel-> getActiveSheet ()
		-> getStyle ("A2:AV2")
		-> getFill ()
		-> setFillType (PHPExcel_Style_Fill :: FILL_SOLID)
		-> getStartColor ()
		-> setRGB ('A6E5F3');
	$objPHPExcel-> getActiveSheet ()
		-> getStyle ("A1")	
		-> getFill ()
		-> setFillType (PHPExcel_Style_Fill :: FILL_SOLID)
		-> getStartColor ()
		-> setRGB ('A6E5F3');
	$borders = array(
		'borders' => array(
			'allborders' => array(
				'style' => PHPExcel_Style_Border::BORDER_THIN,
				'color' => array('argb' => 'FF000000'),
			)
		),
	);

    $periodoId = $_POST["corte"];
    $reporte = [];
    
    $sqlCorte = "SELECT fecha_impresion, fecha_inicial, TIMESTAMPDIFF(DAY, fecha_inicial, fecha_final), version_tarifa FROM srvcortes WHERE numero_corte = $periodoId";
    $resCorte = mysqli_query($linkbd,$sqlCorte);
    $rowCorte = mysqli_fetch_row($resCorte);
    
    $fechaExpedicion = date('d-m-Y',strtotime($rowCorte[0]));
    $fechaInicio = date('d-m-Y',strtotime($rowCorte[1]));
    $diasFacturados = $rowCorte[2];
    $version = $rowCorte[3];

    $sqlUsuarios = "SELECT CD.id_cliente AS clienteId, C.cod_usuario AS codUsuario, C.cod_catastral AS codCatastral, DC.direccion AS direccion, B.nombre AS nombreBarrio, CD.numero_facturacion AS numFactura, E.cod_clase_uso AS codClaseUso, CD.estado_pago AS estadoPago, B.centro_poblado AS centroPoblado, C.hogar_comunitario AS hogarComunitario FROM srvcortes_detalle AS CD INNER JOIN srvclientes AS C ON CD.id_cliente = C.id INNER JOIN srvdireccion_cliente AS DC ON CD.id_cliente = DC.id_cliente INNER JOIN srvbarrios AS B ON C.id_barrio = B.id INNER JOIN srvestratos AS E ON C.id_estrato = E.id WHERE CD.id_corte = $periodoId ORDER BY CD.numero_facturacion ASC";
    $resUsuarios = mysqli_query($linkbd, $sqlUsuarios);
    while ($rowUsuarios = mysqli_fetch_assoc($resUsuarios)) {
        
        $datos = [];
    
        $sqlAsigServ = "SELECT estado_medidor, id_estrato FROM srvasignacion_servicio WHERE id_clientes = $rowUsuarios[clienteId] AND id_servicio = 1";
        $rowAsigServ = mysqli_fetch_assoc(mysqli_query($linkbd, $sqlAsigServ));

        $sqlNovedad = "SELECT TN.estado_medidor AS estadoMedidor FROM srv_asigna_novedades AS SN, srv_tipo_observacion AS TN WHERE SN.corte = $periodoId AND SN.id_cliente = $rowUsuarios[clienteId] AND SN.estado = 'S' AND SN.codigo_observacion = TN.codigo_observacion";
        $rowNovedad = mysqli_fetch_assoc(mysqli_query($linkbd, $sqlNovedad));

        if ($rowNovedad["estadoMedidor"] == 1) {
            $determinacionConsumo = 1;
        }
        else {
            $determinacionConsumo = 2;
        }
    
        $sqlLecturaActual = "SELECT COALESCE(lectura_medidor, '0') AS lectura_medidor, COALESCE(consumo, 0) AS consumo FROM srvlectura WHERE corte = $periodoId AND id_cliente = $rowUsuarios[clienteId] AND id_servicio = 1";
        $rowLecturaActual = mysqli_fetch_assoc(mysqli_query($linkbd,$sqlLecturaActual));
    
        $sqlLecturaAnterior = "SELECT COALESCE(lectura_medidor, 0) AS lectura_medidor FROM srvlectura WHERE corte = $periodoId-1 AND id_cliente = $rowUsuarios[clienteId] AND id_servicio = 1";
        $rowLecturaAnterior = mysqli_fetch_assoc(mysqli_query($linkbd, $sqlLecturaAnterior));

        $sqlCargoFijo = "SELECT credito AS cargoFijo FROM srvdetalles_facturacion WHERE numero_facturacion = $rowUsuarios[numFactura] AND id_servicio = 1 AND tipo_movimiento = '101' AND id_tipo_cobro = 1";
        $rowCargoFijo = mysqli_fetch_assoc(mysqli_query($linkbd, $sqlCargoFijo));

        $cargoFijo = $rowCargoFijo["cargoFijo"];

        $sqlConsumo = "SELECT credito AS consumo FROM srvdetalles_facturacion WHERE numero_facturacion = $rowUsuarios[numFactura] AND id_servicio = 1 AND tipo_movimiento = '101' AND id_tipo_cobro = 2";
        $rowConsumo = mysqli_fetch_assoc(mysqli_query($linkbd, $sqlConsumo));

        $valorConsumo = $rowConsumo["consumo"];
        $valorSubsidiado = 0;
        $valorContribucion = 0;
        $valorServicio = consultaValorServicio($rowUsuarios["numFactura"], 1);

        $sqlDeuda = "SELECT SUM(credito) AS deuda FROM srvdetalles_facturacion WHERE numero_facturacion = $rowUsuarios[numFactura] AND id_servicio = 1 AND tipo_movimiento = '101' AND (id_tipo_cobro = 8 OR id_tipo_cobro = 9 OR id_tipo_cobro = 10)";
        $rowDeuda = mysqli_fetch_assoc(mysqli_query($linkbd, $sqlDeuda));

        $deudaAnterior = $rowDeuda["deuda"];
     
        $sqlTarifa = "SELECT costo_unidad, subsidio, contribucion FROM srvcostos_estandar WHERE id_estrato = $rowAsigServ[id_estrato] AND id_servicio = 1 AND version = $version LIMIT 1";
        $rowTarifa = mysqli_fetch_assoc(mysqli_query($linkbd, $sqlTarifa));
    
        $costoUnidadSinCMT = $rowTarifa["costo_unidad"];

        if ($rowTarifa["subsidio"] > 0) {
            $factorSubCont = $rowTarifa["subsidio"] * -1;
        }
        else if ($rowTarifa["contribucion"] > 0) {
            $factorSubCont = $rowTarifa["contribucion"];
        }
        else {
            $factorSubCont = 0;
        }

        $factorSubCont = $factorSubCont / 100;
    
        $sqlUltPago = "SELECT id FROM srvcortes_detalle WHERE id_cliente = $rowUsuarios[clienteId] AND estado_pago = P AND id_corte >= $periodoId ORDER BY id DESC LIMIT 1";
        $resUltPago = mysqli_query($linkbd, $sqlUltPago);
        $rowUltPago = mysqli_fetch_row($resUltPago); 
    
        if ($rowUltPago[0] <> "") {
    
            $sqlMora = "SELECT estado_pago FROM srvcortes_detalle WHERE id_cliente = $rowUsuarios[clienteId] AND estado_pago = 'V' AND id > $rowUltPago[0]";
            $resMora = mysqli_query($linkbd, $sqlMora);
            $mesesVencidos = mysqli_num_rows($resMora);
            $diasMora = 30 * $mesesVencidos;
        }
        else {
            $sqlMora = "SELECT estado_pago FROM srvcortes_detalle WHERE id_cliente = $rowUsuarios[clienteId] AND estado_pago = 'V'";
            $resMora = mysqli_query($linkbd, $sqlMora);
            $mesesVencidos = mysqli_num_rows($resMora);
            $diasMora = 30 * $mesesVencidos;
        }
    
        if ($rowUsuarios["estadoPago"] == "P") {
            $valorPago = $valorServicio;
        }
        else {
            $valorPago = 0;
        }
    
        if ($rowLecturaAnterior["lectura_medidor"] == "") { $lecturaAnterior = 0;} else {$lecturaAnterior = $rowLecturaAnterior["lectura_medidor"];}
        if ($rowLecturaActual["lectura_medidor"] == "") { $lectura = 0;} else {$lectura = $rowLecturaActual["lectura_medidor"];}
        if ($rowLecturaActual["consumo"] == "") { $consumo = 0;} else {$consumo = $rowLecturaActual["consumo"];}
        if ($rowTarifa["costo_unidad"] == "") { $costoUnidad = 0;} else {$costoUnidad = $rowTarifa["costo_unidad"];}

        if ($rowFactura["consumo_b"] > 0) { $consumo_b = $costoUnidadSinCMT; } else { $consumo_b = 0; }
        if ($rowFactura["consumo_c"] > 0) { $consumo_c = $costoUnidadSinCMT; } else { $consumo_c = 0; }
        if ($rowFactura["consumo_s"] > 0) { $consumo_s = $costoUnidadSinCMT; } else { $consumo_s = 0; }
        if ($rowNovedad["estadoMedidor"] != "") {$estadoMedidor = $rowNovedad["estadoMedidor"];} else {$estadoMedidor = 3;}
        
        $datos[1] = $rowUsuarios["codUsuario"]; 
        $datos[2] = $rowUsuarios["codUsuario"]; 
        $datos[3] = "50";
        $datos[4] = "001";
        $datos[5] = str_pad(substr($rowUsuarios["codCatastral"], 0, 2), 2, "0");
        $datos[6] = str_pad(substr($rowUsuarios["codCatastral"], 2, 2), 2, "0");
        $datos[7] = str_pad(substr($rowUsuarios["codCatastral"], 4, 4), 4, "0");
        $datos[8] = str_pad(substr($rowUsuarios["codCatastral"], 8, 4), 4, "0");
        $datos[9] = str_pad(substr($rowUsuarios["codCatastral"], 12, 3), 3, "0");
        $datos[10] = $rowUsuarios["direccion"];
        $datos[11] = $rowUsuarios["numFactura"];
        $datos[12] = $fechaExpedicion;
        $datos[13] = $fechaInicio;
        $datos[14] = $diasFacturados;
        $datos[15] = $rowUsuarios["codClaseUso"];
        $datos[16] = "";
        $datos[17] = "";
        $datos[18] = $rowUsuarios["hogarComunitario"];
        $datos[19] = $estadoMedidor;
        $datos[20] = $determinacionConsumo;
        $datos[21] = $lecturaAnterior;
        $datos[22] = $lectura;
        $datos[23] = $consumo;
        $datos[24] = $cargoFijo;
        $datos[25] = $consumo_b;
        $datos[26] = $consumo_c;
        $datos[27] = $consumo_s;
        $datos[28] = $rowTarifa["cmt"];
        $datos[29] = $costoUnidad;
        $datos[30] = round($valorConsumo, 2);
        $datos[31] = round($valorSubsidiado, 2);
        $datos[32] = round($valorContribucion, 2);    
        $datos[33] = $factorSubCont;
        $datos[34] = $factorSubCont;
        $datos[35] = 0;
        $datos[36] = 0;
        $datos[37] = 0;
        $datos[38] = 0;
        $datos[39] = $rowFactura["venta_medidor"];
        $datos[40] = 0;
        $datos[41] = $diasMora;
        $datos[42] = round($deudaAnterior, 2);
        $datos[43] = round($rowFactura["interes_mora"], 2);
        $datos[44] = 0;
        $datos[45] = 0;
        $datos[46] = 0;
        $datos[47] = round($valorServicio, 2);
        $datos[48] = round($valorPago, 2);
    
        array_push($reporte, $datos);
    }

	$objPHPExcel->getActiveSheet()->getStyle('A2:AV2')->applyFromArray($borders);
	$objPHPExcel->setActiveSheetIndex(0)
        ->setCellValue('A2', '1')
        ->setCellValue('B2', '2')
        ->setCellValue('C2', '3')
        ->setCellValue('D2', '4')
        ->setCellValue('E2', '5')
        ->setCellValue('F2', '6')
        ->setCellValue('G2', '7')
        ->setCellValue('H2', '8')
        ->setCellValue('I2', '9')
        ->setCellValue('J2', '10')
        ->setCellValue('K2', '11')
        ->setCellValue('L2', '12')
        ->setCellValue('M2', '13')
        ->setCellValue('N2', '14')
        ->setCellValue('O2', '15')
        ->setCellValue('P2', '16')
        ->setCellValue('Q2', '17')
        ->setCellValue('R2', '18')
        ->setCellValue('S2', '19')
        ->setCellValue('T2', '20')
        ->setCellValue('U2', '21')
        ->setCellValue('V2', '22')
        ->setCellValue('W2', '23')
        ->setCellValue('X2', '24')
        ->setCellValue('Y2', '25')
        ->setCellValue('Z2', '26')
        ->setCellValue('AA2', '27')
        ->setCellValue('AB2', '28')
        ->setCellValue('AC2', '29')
        ->setCellValue('AD2', '30')
        ->setCellValue('AE2', '31')
        ->setCellValue('AF2', '32')
        ->setCellValue('AG2', '33')
        ->setCellValue('AH2', '34')
        ->setCellValue('AI2', '35')
        ->setCellValue('AJ2', '36')
        ->setCellValue('AK2', '37')
        ->setCellValue('AL2', '38')
        ->setCellValue('AM2', '39')
        ->setCellValue('AN2', '40')
        ->setCellValue('AO2', '41')
        ->setCellValue('AP2', '42')
        ->setCellValue('AQ2', '43')
        ->setCellValue('AR2', '44')
        ->setCellValue('AS2', '45')
        ->setCellValue('AT2', '46')
        ->setCellValue('AU2', '47')
        ->setCellValue('AV2', '48');     
	$i=3;
    
	foreach ($reporte as $key => $report) {

		$objPHPExcel->setActiveSheetIndex(0)
		->setCellValueExplicit ("A$i", $report[1], PHPExcel_Cell_DataType :: TYPE_STRING)
		->setCellValueExplicit ("B$i", $report[2], PHPExcel_Cell_DataType :: TYPE_STRING)
		->setCellValueExplicit ("C$i", $report[3], PHPExcel_Cell_DataType :: TYPE_STRING)
		->setCellValueExplicit ("D$i", $report[4], PHPExcel_Cell_DataType :: TYPE_STRING)
        ->setCellValueExplicit ("E$i", $report[5], PHPExcel_Cell_DataType :: TYPE_STRING)
        ->setCellValueExplicit ("F$i", $report[6], PHPExcel_Cell_DataType :: TYPE_STRING)
        ->setCellValueExplicit ("G$i", $report[7], PHPExcel_Cell_DataType :: TYPE_STRING)
        ->setCellValueExplicit ("H$i", $report[8], PHPExcel_Cell_DataType :: TYPE_STRING)
        ->setCellValueExplicit ("I$i", $report[9], PHPExcel_Cell_DataType :: TYPE_STRING)
        ->setCellValueExplicit ("J$i", $report[10], PHPExcel_Cell_DataType :: TYPE_STRING)
        ->setCellValueExplicit ("K$i", $report[11], PHPExcel_Cell_DataType :: TYPE_STRING)
        ->setCellValueExplicit ("L$i", $report[12], PHPExcel_Cell_DataType :: TYPE_STRING)
        ->setCellValueExplicit ("M$i", $report[13], PHPExcel_Cell_DataType :: TYPE_STRING)
        ->setCellValueExplicit ("N$i", $report[14], PHPExcel_Cell_DataType :: TYPE_STRING)
        ->setCellValueExplicit ("O$i", $report[15], PHPExcel_Cell_DataType :: TYPE_STRING)
        ->setCellValueExplicit ("P$i", $report[16], PHPExcel_Cell_DataType :: TYPE_STRING)
        ->setCellValueExplicit ("Q$i", $report[17], PHPExcel_Cell_DataType :: TYPE_STRING)
        ->setCellValueExplicit ("R$i", $report[18], PHPExcel_Cell_DataType :: TYPE_STRING)
        ->setCellValueExplicit ("S$i", $report[19], PHPExcel_Cell_DataType :: TYPE_STRING)
        ->setCellValueExplicit ("T$i", $report[20], PHPExcel_Cell_DataType :: TYPE_STRING)
        ->setCellValueExplicit ("U$i", $report[21], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
        ->setCellValueExplicit ("V$i", $report[22], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
        ->setCellValueExplicit ("W$i", $report[23], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
        ->setCellValueExplicit ("X$i", $report[24], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
        ->setCellValueExplicit ("Y$i", $report[25], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
        ->setCellValueExplicit ("Z$i", $report[26], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
        ->setCellValueExplicit ("AA$i", $report[27], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
        ->setCellValueExplicit ("AB$i", $report[28], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
        ->setCellValueExplicit ("AC$i", $report[29], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
        ->setCellValueExplicit ("AD$i", $report[30], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
        ->setCellValueExplicit ("AE$i", $report[31], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
        ->setCellValueExplicit ("AF$i", $report[32], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
        ->setCellValueExplicit ("AG$i", $report[33], PHPExcel_Cell_DataType :: TYPE_STRING)
        ->setCellValueExplicit ("AH$i", $report[34], PHPExcel_Cell_DataType :: TYPE_STRING)
        ->setCellValueExplicit ("AI$i", $report[35], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
        ->setCellValueExplicit ("AJ$i", $report[36], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
        ->setCellValueExplicit ("AK$i", $report[37], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
        ->setCellValueExplicit ("AL$i", $report[38], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
        ->setCellValueExplicit ("AM$i", $report[39], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
        ->setCellValueExplicit ("AN$i", $report[40], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
        ->setCellValueExplicit ("AO$i", $report[41], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
        ->setCellValueExplicit ("AP$i", $report[42], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
        ->setCellValueExplicit ("AQ$i", $report[43], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
        ->setCellValueExplicit ("AR$i", $report[44], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
        ->setCellValueExplicit ("AS$i", $report[45], PHPExcel_Cell_DataType :: TYPE_STRING)
        ->setCellValueExplicit ("AT$i", $report[46], PHPExcel_Cell_DataType :: TYPE_STRING)
        ->setCellValueExplicit ("AU$i", $report[47], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
        ->setCellValueExplicit ("AV$i", $report[48], PHPExcel_Cell_DataType :: TYPE_NUMERIC);

        $objPHPExcel->getActiveSheet()->getStyle("A$i:AV$i")->applyFromArray($borders);

        $i++;
	}

	$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
    $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
    $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
    $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);
    $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setAutoSize(true);
    $objPHPExcel->getActiveSheet()->getColumnDimension('I')->setAutoSize(true);
    $objPHPExcel->getActiveSheet()->getColumnDimension('J')->setAutoSize(true);
    $objPHPExcel->getActiveSheet()->getColumnDimension('K')->setAutoSize(true);
    $objPHPExcel->getActiveSheet()->getColumnDimension('L')->setAutoSize(true);
    $objPHPExcel->getActiveSheet()->getColumnDimension('M')->setAutoSize(true);
    $objPHPExcel->getActiveSheet()->getColumnDimension('N')->setAutoSize(true);
    $objPHPExcel->getActiveSheet()->getColumnDimension('O')->setAutoSize(true);
    $objPHPExcel->getActiveSheet()->getColumnDimension('P')->setAutoSize(true);
    $objPHPExcel->getActiveSheet()->getColumnDimension('Q')->setAutoSize(true);
    $objPHPExcel->getActiveSheet()->getColumnDimension('R')->setAutoSize(true);
    $objPHPExcel->getActiveSheet()->getColumnDimension('S')->setAutoSize(true);
    $objPHPExcel->getActiveSheet()->getColumnDimension('T')->setAutoSize(true);
    $objPHPExcel->getActiveSheet()->getColumnDimension('U')->setAutoSize(true);
    $objPHPExcel->getActiveSheet()->getColumnDimension('V')->setAutoSize(true);
    $objPHPExcel->getActiveSheet()->getColumnDimension('W')->setAutoSize(true);
    $objPHPExcel->getActiveSheet()->getColumnDimension('X')->setAutoSize(true);
    $objPHPExcel->getActiveSheet()->getColumnDimension('Y')->setAutoSize(true);
    $objPHPExcel->getActiveSheet()->getColumnDimension('Z')->setAutoSize(true);
    $objPHPExcel->getActiveSheet()->getColumnDimension('AA')->setAutoSize(true);
    $objPHPExcel->getActiveSheet()->getColumnDimension('AB')->setAutoSize(true);
    $objPHPExcel->getActiveSheet()->getColumnDimension('AC')->setAutoSize(true);
    $objPHPExcel->getActiveSheet()->getColumnDimension('AD')->setAutoSize(true);
    $objPHPExcel->getActiveSheet()->getColumnDimension('AE')->setAutoSize(true);
    $objPHPExcel->getActiveSheet()->getColumnDimension('AF')->setAutoSize(true);
    $objPHPExcel->getActiveSheet()->getColumnDimension('AG')->setAutoSize(true);
    $objPHPExcel->getActiveSheet()->getColumnDimension('AH')->setAutoSize(true);
    $objPHPExcel->getActiveSheet()->getColumnDimension('AI')->setAutoSize(true);
    $objPHPExcel->getActiveSheet()->getColumnDimension('AJ')->setAutoSize(true);
    $objPHPExcel->getActiveSheet()->getColumnDimension('AK')->setAutoSize(true);
    $objPHPExcel->getActiveSheet()->getColumnDimension('AL')->setAutoSize(true);
    $objPHPExcel->getActiveSheet()->getColumnDimension('AM')->setAutoSize(true);
    $objPHPExcel->getActiveSheet()->getColumnDimension('AN')->setAutoSize(true);
    $objPHPExcel->getActiveSheet()->getColumnDimension('AO')->setAutoSize(true);
    $objPHPExcel->getActiveSheet()->getColumnDimension('AP')->setAutoSize(true);
    $objPHPExcel->getActiveSheet()->getColumnDimension('AQ')->setAutoSize(true);
    $objPHPExcel->getActiveSheet()->getColumnDimension('AR')->setAutoSize(true);
    $objPHPExcel->getActiveSheet()->getColumnDimension('AS')->setAutoSize(true);
    $objPHPExcel->getActiveSheet()->getColumnDimension('AT')->setAutoSize(true);
    $objPHPExcel->getActiveSheet()->getColumnDimension('AU')->setAutoSize(true);
    $objPHPExcel->getActiveSheet()->getColumnDimension('AV')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->setTitle('SP');
	//header('Content-Type: application/vnd.ms-excel');
    header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
	header('Content-Disposition: attachment;filename="reporteIgacAcueducto.xlsx"');
	header('Cache-Control: max-age=0');
	$objWriter=PHPExcel_IOFactory::createWriter($objPHPExcel,'Excel2007');
	$objWriter->save('php://output');
	exit;
?>