<?php
	header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=utf8");
	require "comun.inc";
	require "funciones.inc";
	require 'funcionesnomima.inc';
	session_start();
	$linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");
	cargarcodigopag($_GET['codpag'],$_SESSION["nivel"]);
	date_default_timezone_set("America/Bogota");
	$scroll = $_GET['scrtop'];
	$totreg = $_GET['totreg'];
	$idcta = $_GET['idcta'];
	$altura = $_GET['altura'];
	$filtro = "'".$_GET['filtro']."'";
?>
<!DOCTYPE >
<html lang="es">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=9">
		<title>:: IDEAL 10 - Parametrización</title>
		<link href="css/css2.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css">
		<link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css">
		<link href="css/css4.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css">
		<link href="css/tabs2.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css">
		<script type="text/javascript" src="css/calendario.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script type="text/javascript" src="css/programas.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script>
			function despliegamodal2(_valor,_num)
			{
				document.getElementById("bgventanamodal2").style.visibility = _valor;
				if(_valor == "hidden"){
					document.getElementById('ventana2').src = "";
				} else {
					switch(_num){
						case '0':	document.getElementById('ventana2').src = "cargosadministrativos-ventana01.php";break;
						case '1':	document.getElementById('ventana2').src = "tercerosgral-ventana04.php?objeto=tercero&nobjeto=ntercero&nfoco=tercero&valsub=SI"; break;
						case '2':	document.getElementById('ventana2').src = "tercerosgral-ventana01.php?objeto=eps&nobjeto=neps&nfoco=arp";break;
						case "3":	document.getElementById('ventana2').src = "tercerosgral-ventana01.php?objeto=arp&nobjeto=narp&nfoco=afp";break;
						case "4":	document.getElementById('ventana2').src = "tercerosgral-ventana01.php?objeto=afp&nobjeto=nafp&nfoco=fondocesa";break;
						case "5":	document.getElementById('ventana2').src = "tercerosgral-ventana01.php?objeto=fondocesa&nobjeto=nfondocesa&nfoco=cargo";break;
						case "6":	document.getElementById('ventana2').src = "nivelsalarial-ventana01.php";break;
						case "7":	document.getElementById('ventana2').src = "proyinversion-ventana01.php";break;
						case "8":
							var idproy = document.getElementById('idproyecto').value;
							document.getElementById('ventana2').src = "proprogramatico-ventana01.php?idproy=" + idproy;break;
						case "9":
							var idproy = document.getElementById('idproyecto').value;
							var idprogr = document.getElementById('idprogramatico').value;
							document.getElementById('ventana2').src = "profuentes-ventana02.php?idproy=" + idproy + "&idprogr=" + idprogr;break;
					}
				}
			}
			function despliegamodalm(_valor,_tip,mensa,pregunta){
				document.getElementById("bgventanamodalm").style.visibility = _valor;
				if(_valor == "hidden"){
					document.getElementById('ventanam').src = "";
				} else {
					switch(_tip){
						case "1":	document.getElementById('ventanam').src = "ventana-mensaje1.php?titulos="+mensa;break;
						case "2":	document.getElementById('ventanam').src = "ventana-mensaje3.php?titulos="+mensa;break;
						case "3":	document.getElementById('ventanam').src = "ventana-mensaje2.php?titulos="+mensa;break;
						case "4":	document.getElementById('ventanam').src = "ventana-consulta1.php?titulos="+mensa+"&idresp="+pregunta;break;
					}
				}
			}
			function funcionmensaje(){
				var _cons = document.getElementById('egreso').value;
				document.location.href = "teso-girarchequesver.php?idegre="+_cons+"&scrtop=0&numpag=1&limreg=10&filtro1=&filtro2=";
			}
			function respuestaconsulta(pregunta){
				switch(pregunta){
					case "1":	document.form2.oculto.value='2';document.form2.submit();break;
				}
			}
			function validar(){document.form2.submit();}
			function buscar(_num){
				switch(_num){
					case "1":
						var validacion01 = document.getElementById('tercero').value;
						if (validacion01.trim() != ''){
							document.getElementById('vbuscar').value = "1";
							document.form2.submit();
							break;
						} else {
							document.getElementById('ntercero').value = ""
							document.getElementById('direccion').value = "";
							document.getElementById('telefono').value = "";
							document.getElementById('celular').value = "";
							document.getElementById('email').value = "";
							break;
						}
				}
			}
			function guardar() {
				if ((document.form2.fechain.value != '' && existeFecha(document.form2.fechain.value))  && (document.form2.fechaeps.value != '' && existeFecha(document.form2.fechaeps.value)) && (document.form2.fechaarl.value != '' && existeFecha(document.form2.fechaarl.value)) && document.form2.nomcargoad.value != '' && document.form2.ntercero.value!=''  && document.form2.neps.value != '' && document.form2.narp.value != '' && document.form2.nivsal.value != '' && document.form2.numcc.value != '')
				{despliegamodalm('visible','4','Esta seguro de modificar el funcionario','1');}
				else {despliegamodalm('visible','2','Faltan datos para completar el registro');}
			}
			function cambiocheck(){
				if(document.getElementById('idswfun').value == 'S'){document.getElementById('idswfun').value = 'N';}
				else{document.getElementById('idswfun').value = 'S';}
				document.form2.submit();
			}
			function iratras(scrtop, numpag, limreg, filtro){
				var idfun = document.form2.idfun.value;
				location.href = "hum-funcionariosbuscar.php?idfun=" + idfun + "&scrtop=" + scrtop + "&numpag=" + numpag+ "&limreg=" + limreg + "&filtro=" + filtro;
			}
			function adelante(scrtop, numpag, limreg, filtro){
				var maximo = document.getElementById('maximo').value;
				var actual = document.getElementById('idfun').value;
				actual = parseFloat(actual)+1;
				if(actual <= parseFloat(maximo)){
					location.href="hum-funcionarioseditar.php?idfun=" +actual+ "&scrtop=" +scrtop+ "&numpag=" +numpag+ "&limreg=" +limreg+ "&filtro=" +filtro;
				}
			}
			function atrasc(scrtop, numpag, limreg, filtro, prev){
				var minimo = document.getElementById('minimo').value;
				var actual = document.getElementById('idfun').value;
				actual = parseFloat(actual)-1;
				if(actual >= parseFloat(minimo)){
					location.href="hum-funcionarioseditar.php?idfun=" +actual+ "&scrtop=" +scrtop+ "&numpag=" +numpag+ "&limreg=" +limreg+ "&filtro=" +filtro;
				}
			}
			function validaproyecto(){
				if(document.getElementById('idproyecto').value != ''){
					despliegamodal2('visible','8');
				} else {
					despliegamodalm('visible','2','Debe seleccionar primero un proyecto');
				}
			}
			function validaprogramatico(){
				if(document.getElementById('idprogramatico').value != ''){
					despliegamodal2('visible','9');
				} else {
					despliegamodalm('visible','2','Debe seleccionar primero un prográmatico');
				}
			}
		</script>
		<?php titlepag();?>
	</head>
	<body>
		<IFRAME src="alertas.php" name="alertas" id="alertas" style="display:none"></IFRAME>
		<span id="todastablas2"></span>
		<?php
			$numpag = $_GET['numpag'];
			$limreg = $_GET['limreg'];
			$scrtop = 26*$totreg;
		?>
		<table>
			<tr><script>barra_imagenes("para");</script><?php cuadro_titulos();?></tr>
			<tr><?php menu_desplegable("para");?></tr>
			<tr>
				<td colspan="3" class="cinta"><img src="imagenes/add.png" title="Nuevo" onClick="location.href='hum-funcionarios.php'" class="mgbt"><img src="imagenes/guarda.png" title="Guardar" onClick="guardar()" class="mgbt"><img src="imagenes/busca.png" title="Buscar" onClick="location.href='hum-funcionariosbuscar.php'" class="mgbt"><img src="imagenes/nv.png" title="Nueva ventana" onClick="mypop=window.open('para-principal.php','','');mypop.focus();" class="mgbt"><img src="imagenes/iratras.png" title="Atr&aacute;s" class="mgbt"  onClick="iratras(<?php echo "$scrtop, $numpag, $limreg, $filtro"; ?>)"></td>
			</tr>
		</table>
		<div id="bgventanamodalm" class="bgventanamodalm">
			<div id="ventanamodalm" class="ventanamodalm">
				<IFRAME src="" name="ventanam" marginWidth=0 marginHeight=0 frameBorder=0 id="ventanam" frameSpacing=0 style=" width:700px; height:130px; top:200; overflow:hidden;"> 
				</IFRAME>
			</div>
		</div>
		<form name="form2" method="post" action="">
			<?php
				$sqlr = "select MIN(codfun), MAX(codfun) from hum_funcionarios";
				$res = mysqli_query($linkbd,$sqlr);
				$r = mysqli_fetch_row($res);
				$_POST['minimo'] = $r[0];
				$_POST['maximo'] = $r[1];
				if ($_POST['oculto'] == ""){
					$_POST['tabgroup1'] = 1;
					$_POST['fechamodi'] = date('Y-m-d');
					$_POST['idfun'] = $_GET['idfun'];
					$_POST['fechain'] = date('d/m/Y',strtotime(fechasfuncionarios($_POST['idfun'], '26')));
					$_POST['nomcargoad'] = itemfuncionarios($_POST['idfun'],'2');
					$_POST['idcargoad'] = itemfuncionarios($_POST['idfun'],'1');
					$_POST['tercero'] = itemfuncionarios($_POST['idfun'],'6');
					$_POST['ntercero'] = itemfuncionarios($_POST['idfun'],'7');
					$_POST['direccion'] = itemfuncionarios($_POST['idfun'],'8');
					$_POST['telefono'] = itemfuncionarios($_POST['idfun'],'9');
					$_POST['celular'] = itemfuncionarios($_POST['idfun'],'10');
					$_POST['email'] = itemfuncionarios($_POST['idfun'],'11');
					$_POST['tercerocta'] = itemfuncionarios($_POST['idfun'],'12');
					$_POST['bancocta'] = itemfuncionarios($_POST['idfun'],'13');
					$_POST['eps'] = itemfuncionarios($_POST['idfun'],'14');
					$_POST['neps'] = itemfuncionarios($_POST['idfun'],'15');
					$_POST['fechaeps']=date('d/m/Y',strtotime(fechasfuncionarios($_POST['idfun'], '14')));
					$_POST['arp'] = itemfuncionarios($_POST['idfun'],'16');
					$_POST['narp'] = itemfuncionarios($_POST['idfun'],'17');
					$_POST['fechaarl']=date('d/m/Y',strtotime(fechasfuncionarios($_POST['idfun'], '16')));
					$_POST['afp'] = itemfuncionarios($_POST['idfun'],'18');
					$_POST['nafp'] = itemfuncionarios($_POST['idfun'],'19');
					$_POST['fechaafp']=date('d/m/Y',strtotime(fechasfuncionarios($_POST['idfun'], '18')));
					$_POST['fondocesa'] = itemfuncionarios($_POST['idfun'],'20');
					$_POST['nfondocesa'] = itemfuncionarios($_POST['idfun'],'21');
					$_POST['fechafdc']=date('d/m/Y',strtotime(fechasfuncionarios($_POST['idfun'], '20')));
					$_POST['cargo'] = itemfuncionarios($_POST['idfun'],'3');
					$_POST['nivsal'] = itemfuncionarios($_POST['idfun'],'4');
					$_POST['asigbas2']="$ ".number_format(itemfuncionarios($_POST['idfun'],'5'), 0, ',', '.');
					$_POST['asigbas'] = itemfuncionarios($_POST['idfun'],'5');
					$_POST['tperiodo'] = itemfuncionarios($_POST['idfun'],'24');;
					$_POST['numcc'] = itemfuncionarios($_POST['idfun'],'22');
					$_POST['nomcc'] = itemfuncionarios($_POST['idfun'],'23');
					$_POST['pagces'] = itemfuncionarios($_POST['idfun'],'25');
					$_POST['swfun'] = itemfuncionarios($_POST['idfun'],'26');
					$_POST['nivelarl'] = itemfuncionarios($_POST['idfun'],'27');
					$_POST['pvinculacion'] = itemfuncionarios($_POST['idfun'],'28');
					$_POST['uniejecutora'] = itemfuncionarios($_POST['idfun'],'29');
					$_POST['tpresupuesto'] = itemfuncionarios($_POST['idfun'],'30');;
					$_POST['idproyecto'] = itemfuncionarios($_POST['idfun'],'31');
					$_POST['nomproyecto'] = nombreproyecto($_POST['idproyecto']);
					if(itemfuncionarios($_POST['idfun'],'32') == ''){$_POST['porsf'] = buscaporcentajeparafiscalglobal('34');}
					else {$_POST['porsf'] = itemfuncionarios($_POST['idfun'],'32');}
					if(itemfuncionarios($_POST['idfun'],'33') == ''){$_POST['porse'] = buscaporcentajeparafiscalglobal('26');}
					else {$_POST['porse'] = itemfuncionarios($_POST['idfun'],'33');;}
					if(itemfuncionarios($_POST['idfun'],'34') == ''){$_POST['porpf'] = buscaporcentajeparafiscalglobal('35');}
					else {$_POST['porpf'] = itemfuncionarios($_POST['idfun'],'34');}
					if(itemfuncionarios($_POST['idfun'],'35') == ''){$_POST['porpe'] = buscaporcentajeparafiscalglobal('25');}
					else {$_POST['porpe'] = itemfuncionarios($_POST['idfun'],'35');;}
					if(itemfuncionarios($_POST['idfun'],'36') == ''){$_POST['porccf'] = buscaporcentajeparafiscalglobal('28');}
					else {$_POST['porccf'] = itemfuncionarios($_POST['idfun'],'36');;}
					if(itemfuncionarios($_POST['idfun'],'36') == ''){$_POST['poricbf'] = buscaporcentajeparafiscalglobal('30');}
					else {$_POST['poricbf'] = itemfuncionarios($_POST['idfun'],'37');}
					if(itemfuncionarios($_POST['idfun'],'38') == ''){$_POST['porsena'] = buscaporcentajeparafiscalglobal('31');}
					else {$_POST['porsena'] = itemfuncionarios($_POST['idfun'],'38');}
					if(itemfuncionarios($_POST['idfun'],'39') == ''){$_POST['porinte'] = buscaporcentajeparafiscalglobal('33');}
					else {$_POST['porinte'] = itemfuncionarios($_POST['idfun'],'39');}
					if(itemfuncionarios($_POST['idfun'],'40') == ''){$_POST['poresap'] = buscaporcentajeparafiscalglobal('32');}
					else {$_POST['poresap'] = itemfuncionarios($_POST['idfun'],'40');}
					$_POST['tpresupuestop'] = itemfuncionarios($_POST['idfun'],'41');
					$_POST['secpresupuestal'] = itemfuncionarios($_POST['idfun'],'42');
					$_POST['idprogramatico'] = itemfuncionarios($_POST['idfun'],'43');
					$_POST['nomprogramatico'] = nombreprogramatico($_POST['idprogramatico']);
					$_POST['idfuente'] = itemfuncionarios($_POST['idfun'],'44');
					$_POST['nomfuente'] = nombrefuentecuipo($_POST['idfuente']);
					$_POST['tpresupuestoOP'] = itemfuncionarios($_POST['idfun'],'45');
					
					$sqlr="SELECT id_tercero FROM terceros where cedulanit = '$datos[5]' AND estado = 'S'";
					$res = mysqli_query($linkbd,$sqlr);
					$r = mysqli_fetch_row($res);
					$_POST['idterc'] = $r[0];
				}
				switch($_POST['tabgroup1']){
					case 1:	$check1 = 'checked';$check2 = $check3 = '';break;
					case 2:	$check2 = 'checked';$check1 = $check3 = '';break;
					case 2:	$check3 = 'checked';$check1 = $check2 = '';break;
				}
				if ($_POST['vbuscar'] == "1"){
					$sqlr = "SELECT nombre1, nombre2, apellido1, apellido2, direccion, telefono, celular, email, id_tercero FROM terceros WHERE cedulanit = '".$_POST['tercero']."' AND estado='S'";
					$res = mysqli_query($linkbd,$sqlr);
					while($r = mysqli_fetch_row($res)){
						if ($r[3] != "" && $r[1] != ""){$_POST['ntercero'] = "$r[2] $r[3] $r[0] $r[1]";}
						elseif($r[3] != ""){$_POST['ntercero'] = "$r[2] $r[3] $r[0]";}
						elseif($r[1] != ""){$_POST['ntercero'] = "$r[2] $r[0] $r[1]";}
						else {$_POST['ntercero'] = "$r[2] $r[0]";}
						if($r[4] != ""){$_POST['direccion'] = $r[4];}
						else {$_POST['direccion'] = "SIN DIRECCION DIGITADA";}
						if($r[5] != ""){$_POST['telefono'] = $r[5];}
						else{$_POST['telefono'] = "SIN NUMERO TELEFONICO";}
						if($r[6] != ""){$_POST['celular'] = $r[6];}
						else{$_POST['celular'] = "SIN NUMERO CELULAR";}
						if($r[7] != ""){$_POST['email'] = $r[7];}
						else{$_POST['email'] = "SIN CORREO ELECTRONICO";}
						$_POST['idterc'] = $r[8];
					}
					$sqlr00 = "SELECT T1.codcargo, T1.nombrecargo, T1.clasificacion FROM planaccargos T1 INNER JOIN planestructura_terceros T2 ON T1.codcargo = T2.codcargo WHERE T1.estado = 'S' AND T2.cedulanit = '".$_POST['tercero']."' AND T1.estado = 'S' AND T2.estado = 'S'";
					$resp00 = mysqli_query($linkbd,$sqlr00);
					$row00 = mysqli_fetch_row($resp00);
					$sqlr01 = "SELECT nombre,valor FROM humnivelsalarial WHERE id_nivel = '$row00[2]'";
					$resp01 = mysqli_query($linkbd,$sqlr01);
					$row01 = mysqli_fetch_row($resp01);
					$_POST['idcargoad'] = $row00[0];
					$_POST['nomcargoad'] = $row00[1];
					$_POST['cargo'] = $row00[2];
					$_POST['nivsal'] = $row01[0];
					$_POST['asigbas'] = $row01[1];
					$_POST['asigbas2'] = "$ ".number_format($row01[1], 0, ',', '.');
				}
			?>
			<div class="tabs" style="height:74.5%; width:99.6%" >
				<div class="tab">
					<input type="radio" id="tab-1" name="tabgroup1" value="1" <?php echo $check1;?> >
					<label id="clabel" for="tab-1">Informaci&oacute;n general</label>
					<div class="content" style="overflow:hidden;">
						<table class="inicio ancho">
							<tr>
								<td class="titulos" colspan="7">.: Ingresar funcionario nuevo</td>
								<td class="cerrar" style="width:7%" onClick="location.href='para-principal.php'">Cerrar</td>
							</tr>
							<tr>
								<td class="saludo1" >.: Id funcionario:</td>
								<td><img src="imagenes/back.png" onClick="atrasc(<?php echo "$scrtop, $numpag, $limreg, $filtro"; ?>)" class="icobut" title="Anterior"/>&nbsp;<input type="text" name="idfun" id="idfun" value="<?php echo $_POST['idfun'];?>" readonly style="width:55%">&nbsp;<img src="imagenes/next.png" onClick="adelante(<?php echo "$scrtop, $numpag, $limreg, $filtro" ?>);" class="icobut" title="Sigiente"/> </td>
								<td class="saludo1" >Funcionario activo:</td>
								<td>
									<div class="swsino">
										<input type="checkbox" name="swfun" class="swsino-checkbox" id="idswfun" value="<?php echo $_POST['swfun'];?>" <?php if($_POST['swfun']=='S'){echo "checked";}?> onChange="cambiocheck();"/>
										<label class="swsino-label" for="idswfun">
											<span class="swsino-inner"></span>
											<span class="swsino-switch"></span>
										</label>
									</div>
								</td>
							</tr>
							<tr>
								<td class='tamano01' style="width:3cm;">.: Fecha ingreso:</td>
								<td><input type="text" name="fechain" id="fc_1198971547" value="<?php echo $_POST['fechain']?>" maxlength="10" onKeyPress="javascript:return solonumeros(event)" onKeyUp="return tabular(event,this)" onKeyDown="mascara(this,'/',patron,true)" title="DD/MM/YYYY" style="width:100%;" onDblClick="displayCalendarFor('fc_1198971547');" title="Fecha Ingreso" class="colordobleclik" autocomplete="off" onChange=""></td>
								<td class='tamano01'>.: Cargo:</td>
								<td colspan="2"><input type="text" name="nomcargoad" id="nomcargoad" onKeyUp="return tabular(event,this)" value="<?php echo $_POST['nomcargoad']?>" style="width:100%" title="Lista de Cargos" onDblClick="despliegamodal2('visible','0');" class="colordobleclik" autocomplete="off" readonly></td>
								<td><img class="icobut" src="imagenes/ladd.png" title="Agregar Cargo" onClick="mypop=window.open('adm-cargosadmguardar.php','','');mypop.focus();"/></td>
								<td rowspan="11" style="border:double;"></td>
							</tr>
							<input type="hidden" name="idcargoad" id="idcargoad" value="<?php echo $_POST['idcargoad']?>"/>
							<tr>
								<td class="tamano01">.: Escala:</td>
								<td><input type="text" name="cargo" id="cargo" value="<?php echo $_POST['cargo']?>" style="width:100%;" readonly/></td>
								<td colspan="2"><input type="text" name="nivsal" id="nivsal" value="<?php echo $_POST['nivsal']?>" style="width:100%;" readonly></td>
								<td style="width:10%;"><input type="text" name="asigbas2" id="asigbas2" value="<?php echo $_POST['asigbas2']?>" style="width:100%;" readonly/></td>
								<input type="hidden" name="asigbas" id="asigbas" value="<?php echo $_POST['asigbas']?>"/>
							</tr>
							<tr>
								<td class="tamano01">.: Tercero:</td>
								<td style="width:15%;"><input type="text" name="tercero" id="tercero" onKeyUp="return tabular(event,this)" onChange="buscar('1')" value="<?php echo $_POST['tercero']?>" style="width:100%" title="Listado Terceros" class="colordobleclik" onDblClick="despliegamodal2('visible','1');" autocomplete="off"></td>
								<td style="width:50%;" colspan="3"><input type="text" name="ntercero" id="ntercero" value="<?php echo $_POST['ntercero']?>" style="width:100%" readonly></td>
								<?php
									if($_POST['ntercero'] == ""){$editer = " class='icobut1' src='imagenes/usereditd.png'";}
									else{$editer = " class='icobut' src='imagenes/useredit.png' onClick=\"mypop=window.open('hum-terceroseditar01.php?idter=".$_POST['idterc']."','','');mypop.focus();\"";}
								?>
								<td style="width:1.5cm;">&nbsp;<img class="icobut" src="imagenes/usuarion.png" title="Crear Tercero" onClick="mypop=window.open('hum-terceros01.php','','');mypop.focus();"/>&nbsp;<img <?php echo $editer; ?> title="Editar Tercero" /></td>
							</tr>
							<tr>
								<td class="tamano01">.: Direcci&oacute;n:</td>
								<td colspan="5"><input type="text" name="direccion" id="direccion" value="<?php echo $_POST['direccion']?>" style="width:100%;" readonly/></td>
							</tr>
							<tr>
								<td class="tamano01">.: Telefono:</td>
								<td><input type="text" name="telefono" id="telefono" value="<?php echo $_POST['telefono']?>" style="width:100%;" readonly/></td>
								<td class="tamano01" style="width:10%;">.: Celular:</td>
								<td colspan="3"><input type="text" name="celular" id="celular" value="<?php echo $_POST['celular']?>" style="width:100%;" readonly/></td>
							</tr>
							<tr>
								<td class="tamano01">.: E-mail:</td>
								<td colspan="5"><input type="text" name="email" id="email" value="<?php echo $_POST['email']?>" style="width:100%;" readonly/></td>
							</tr>
							<tr>
								<td class="tamano01">.: Cuenta:</td>
								<td><input type="text" name="tercerocta" id="tercerocta" value="<?php echo $_POST['tercerocta']?>" style="width:100%"/></td>
								<td class="tamano01">.: Banco:</td>
								<td colspan="3">
									<select name="bancocta" id="bancocta" style='text-transform:uppercase; width:70%; height:22px;'>
										<option value="">....</option>
										<?php
											$sqlr="SELECT codigo, nombre FROM hum_bancosfun WHERE estado='S' ORDER BY CONVERT(codigo, SIGNED INTEGER)";
											$resp = mysqli_query($linkbd,$sqlr);
											while ($row = mysqli_fetch_row($resp)){
												if($row[0] == $_POST['bancocta']){echo "<option value='$row[0]' SELECTED>$row[0] - $row[1]</option>";}
												else {echo "<option value='$row[0]'>$row[0] - $row[1]</option>";}
											}
										?>
									</select>
									&nbsp;<img class="icobut" src="imagenes/ladd.png" title="Crear Banco" onClick="mypop=window.open('hum-bancos.php','','');mypop.focus();"/>&nbsp;<img class="icorot" src="imagenes/reload.png" title="Actualizar Lista Bancos" onClick="document.form2.submit();"/>
								</td>
							</tr>
							<tr>
								<td class="tamano01">.: Periodo:</td>
								<td>
									<select name="tperiodo" id="tperiodo" style="width:100%;">
										<option value="-1">Seleccione ....</option>
										<option value="30"<?php if($_POST['tperiodo'] == 30){echo"SELECTED";}?>>MENSUAL</option>
										<option value="15"<?php if($_POST['tperiodo'] == 15){echo"SELECTED";}?>>QUINCENAL</option>
									</select>
								</td>
								<td class="tamano01">.: Unidad ejecutora</td>
								<td colspan="3">
									<select name="uniejecutora" id="uniejecutora" style='text-transform:uppercase; width:70%; height:22px;' onChange="document.form2.submit();">
										<option value="">....</option>
										<?php
											$sql = "SELECT id_cc, nombre FROM pptouniejecu WHERE estado = 'S' ORDER BY id_cc ASC";
											$res = mysqli_query($linkbd,$sql);
											while($row = mysqli_fetch_row($res))
											{
												if($row[0] == $_POST['uniejecutora']){echo "<option value='$row[0]' SELECTED>$row[0] - $row[1]</option>";}
												else {echo "<option value='$row[0]'>$row[0] - $row[1]</option>";}
											}
										?>
									</select>
								</td>
							</tr>
							<tr>
								<td class="tamano01" >.: Vinculaci&oacute;n:</td>
								<td>
									<select name="pvinculacion" id="pvinculacion" style="width:100%;">
										<option value="">Seleccione ....</option>
										<option value="T"<?php if($_POST['pvinculacion'] == 'T'){echo" SELECTED";}?>>TEMPORAL</option>
										<option value="P"<?php if($_POST['pvinculacion'] == 'P'){echo" SELECTED";}?>>PERMANENTE</option>
									</select>
								</td>
								<td class="tamano01">.: Secci&oacute;n Presupuestal</td>
								<td colspan="3">
									<select name="secpresupuestal" id="secpresupuestal" style='text-transform:uppercase; width:70%; height:22px;' onChange="document.form2.submit();">
										<option value=''>....</option>
										<?php
											$sql = "SELECT id_seccion_presupuestal, nombre FROM pptoseccion_presupuestal WHERE id_unidad_ejecutora = '".$_POST['uniejecutora']."' ORDER BY id_seccion_presupuestal ASC";
											$res = mysqli_query($linkbd,$sql);
											while($row = mysqli_fetch_row($res))
											{
												if($row[0] == $_POST['secpresupuestal']){echo "<option value='$row[0]' SELECTED>$row[0] - $row[1]</option>";}
												else {echo "<option value='$row[0]'>$row[0] - $row[1]</option>";}
											}
										?>
									</select>
								</td>
							</tr>
							<tr>
								<td class="tamano01">.: Tipo presupuesto Salario:</td>
								<td>
									<select name="tpresupuesto" id="tpresupuesto" style="width:100%;" onChange="document.form2.submit();" >
										<option value="">Seleccione ....</option>
										<option value="F"<?php if($_POST['tpresupuesto'] == 'F'){echo" SELECTED"; $_POST['tpresupuestop'] = 'F';}?>>FUNCIONAMIENTO</option>
										<option value="I"<?php if($_POST['tpresupuesto'] == 'I'){echo" SELECTED"; if($_POST['tpresupuestop'] == ''){$_POST['tpresupuestop'] = 'I';}}?>>INVERSION</option>
										<option value="G"<?php if(@$_POST['tpresupuesto'] == 'G'){echo" SELECTED";if($_POST['tpresupuestop'] <> 'G'){$_POST['tpresupuestop'] = 'G';}}?>>GASTOS COMERCIALIZACI&Oacute;N</option>
									</select>
								</td>
								<td class="tamano01">.: Centro de costo:</td>
								<td colspan="3">
									<select name="numcc" id="numcc" style='text-transform:uppercase; width:70%; height:22px;'>
										<option value="">....</option>
										<?php
											$sqlr = "
											SELECT T1.id_cc, T1.nombre FROM centrocosto AS T1 
											INNER JOIN centrocostos_seccionpresupuestal AS T2 ON T1.id_cc = T2.id_cc
											INNER JOIN pptoseccion_presupuestal AS T3 ON T3.id_seccion_presupuestal = T2.id_sp
											WHERE T1.estado = 'S' AND T3.id_seccion_presupuestal = '".$_POST['secpresupuestal']."' ORDER BY CONVERT(T1.id_cc, SIGNED INTEGER)";
											$resp = mysqli_query($linkbd,$sqlr);
											while ($row =mysqli_fetch_row($resp))
											{
												if($row[0] == $_POST['numcc'])
												{
													echo "<option value='$row[0]' SELECTED>$row[0] - $row[1]</option>";
													$_POST['nomcc'] = $row[1];
												}
												else {echo "<option value='$row[0]'>$row[0] - $row[1]</option>";}
											}
										?>
									</select>
									&nbsp;<img class="icobut" src="imagenes/ladd.png" title="Crear Centro de Costo" onClick="mypop=window.open('cont-buscacentrocosto.php','','');mypop.focus();"/>&nbsp;<img class="icorot" src="imagenes/reload.png" title="Actualizar Lista Centro de Costo" onClick="document.form2.submit();"/>
									<input type="hidden" name="nomcc" id="nomcc" value="<?php echo $_POST['nomcc'];?>"/>
								</td>
							</tr>
							<tr>
								<td class="tamano01">.: Tipo presupuesto Otros Pagos:</td>
								<td>
									<select name="tpresupuestoOP" id="tpresupuestoOP" style="width:100%;" onChange="document.form2.submit();" >
										<option value="">Seleccione ....</option>
										<option value="F"<?php if($_POST['tpresupuestoOP'] == 'F'){echo" SELECTED";}?>>FUNCIONAMIENTO</option>
										<option value="I"<?php if($_POST['tpresupuestoOP'] == 'I'){echo" SELECTED";}?>>INVERSION</option>
										<option value="G"<?php if($_POST['tpresupuestoOP'] == 'G'){echo" SELECTED";}?>>GASTOS COMERCIALIZACI&Oacute;N</option>
									</select>
								</td>
							</tr>
						</table>
						<input type="hidden" name="maximo" id="maximo" value="<?php echo $_POST['maximo']?>"/>
						<input type="hidden" name="minimo" id="minimo" value="<?php echo $_POST['minimo']?>"/>
						<input type="hidden" name="oculto" id="oculto" value="1"/>
						<input type="hidden" name="vbuscar" id="vbuscar" value="0"/>
						<input type="hidden" name="fechamodi" id="fechamodi" value="<?php echo $_POST['fechamodi'];?>"/>
						<input type="hidden" name="idterc" id="idterc" value="<?php echo $_POST['idterc'];?>"/>
					</div>
				</div>
				<div class="tab" >
					<input type="radio" id="tab-2" name="tabgroup1" value="2" <?php echo $check2;?> >
					<label id="clabel" for="tab-2">Parafiscales</label>
					<div class="content" style="overflow:hidden;">
						<table class="inicio ancho">
							<tr>
								<td class="titulos" colspan="6">.: Seguridad social</td>
								<td class="cerrar" style="width:7%" onClick="location.href='para-principal.php'">Cerrar</td>
							</tr>
							<tr>
								<td class="tamano01" style="width:3cm;">.: EPS:</td>
								<td style="width:20%;"><input type="text" name="eps" id="eps" value="<?php echo $_POST['eps']?>" onKeyUp="return tabular(event,this)" style="width:80%;" onBlur="buscar('2')"/>&nbsp;<img class="icobut" src="imagenes/find02.png" onClick="despliegamodal2('visible','2');" title="Lista"/></td>
								<td colspan="2"><input type="text" id="neps" name="neps" value="<?php echo $_POST['neps']?>" onKeyUp="return tabular(event,this)"  style="width:100%;" readonly/></td>
								<td style="width:10%;"><input type="text" name="fechaeps" id="fc_1198971548" value="<?php echo $_POST['fechaeps']?>" maxlength="10" onKeyPress="javascript:return solonumeros(event)" onKeyUp="return tabular(event,this)" onKeyDown="mascara(this,'/',patron,true)" title="DD/MM/YYYY" style="width:100%;"/></td>
								<td ><img src="imagenes/calendario04.png" onClick="displayCalendarFor('fc_1198971548');" class="icobut" title="Fecha Ingreso EPS"/></td>
							</tr>
							<tr>
								<td class="tamano01">.: ARL: </td>
								<td><input type="text" name="arp" id="arp" value="<?php echo $_POST['arp']?>" onKeyUp="return tabular(event,this)" style="width:80%;" onBlur="buscar('3')"/>&nbsp;<img class="icobut" src="imagenes/find02.png" onClick="despliegamodal2('visible','3');" title="Lista"/></td>
								<td colspan="2"><input type="text" id="narp" name="narp" value="<?php echo $_POST['narp']?>" onKeyUp="return tabular(event,this)" style="width:100%;" readonly></td>
								<td><input type="text" name="fechaarl" id="fc_1198971549" value="<?php echo $_POST['fechaarl']?>" maxlength="10" onKeyPress="javascript:return solonumeros(event)" onKeyUp="return tabular(event,this)" onKeyDown="mascara(this,'/',patron,true)" title="DD/MM/YYYY" style="width:100%;"/></td>
								<td><img src="imagenes/calendario04.png" onClick="displayCalendarFor('fc_1198971549');" class="icobut" title="Fecha Ingreso ARL"/></td>
							</tr>
							<tr>
								<td class="tamano01">.: AFP:</td>
								<td><input type="text" id="afp" name="afp" value="<?php echo $_POST['afp']?>" onKeyUp="return tabular(event,this)" style="width:80%;" onBlur="buscar('4')"/>&nbsp;<img class="icobut" src="imagenes/find02.png" onClick="despliegamodal2('visible','4');" /></td>
								<td colspan="2"><input type="text" name="nafp" id="nafp"  value="<?php echo $_POST['nafp']?>" onKeyUp="return tabular(event,this)" style="width:100%;" readonly></td>
								<td><input type="text" name="fechaafp" id="fc_1198971550" value="<?php echo $_POST['fechaafp']?>" maxlength="10" onKeyPress="javascript:return solonumeros(event)" onKeyUp="return tabular(event,this)" onKeyDown="mascara(this,'/',patron,true)" title="DD/MM/YYYY" style="width:100%;"/></td>
								<td><img src="imagenes/calendario04.png" onClick="displayCalendarFor('fc_1198971550');" class="icobut" title="Fecha Ingreso AFP"/></td>
							</tr>
							<tr>
								<td class="tamano01">.: Fondo cesant&iacute;as:</td>
								<td ><input type="text" id="fondocesa" name="fondocesa" value="<?php echo $_POST['fondocesa']?>" onKeyUp="return tabular(event,this)" style="width:80%;" onBlur="buscar('5')"/>&nbsp;<img class="icobut" src="imagenes/find02.png" title="Lista" onClick="despliegamodal2('visible','5');"></td>
								<td colspan="2"><input id="nfondocesa" name="nfondocesa" type="text" value="<?php echo $_POST['nfondocesa']?>" onKeyUp="return tabular(event,this)" style="width:100%;" readonly></td>
								<td><input type="text" name="fechafdc" id="fc_1198971551" value="<?php echo $_POST['fechafdc']?>" maxlength="10" onKeyPress="javascript:return solonumeros(event)" onKeyUp="return tabular(event,this)" onKeyDown="mascara(this,'/',patron,true)" title="DD/MM/YYYY" style="width:100%;"/></td>
								<td><img src="imagenes/calendario04.png" onClick="displayCalendarFor('fc_1198971551');" class="icobut" title="Fecha Ingreso Fondo Cesantias"/></td>
							</tr>
							<tr>
								<td class="tamano01">.: Pago Cesant&iacute;as:</td>
								<td>
									<select name="pagces" id="pagces" style="width:80%;">
										<option value="" <?php if($_POST['pagces']==""){echo "SELECTED";} ?>> ...</option>
										<option value="A" <?php if($_POST['pagces']=="A"){echo "SELECTED";} ?>> Anual</option>
										<option value="M" <?php if($_POST['pagces']=="M"){echo "SELECTED";} ?>> Mensual</option>
									</select>
								</td>
								<td class="tamano01" style="width:3cm;">.: Nivel ARL:</td>
								<td>
									<select name="nivelarl" id="nivelarl" style="width:100%;">
										<?php
											$sqlr = "SELECT id,codigo,tarifa,detalle FROM hum_nivelesarl WHERE estado='S' ORDER BY id";
											$resp = mysqli_query($linkbd,$sqlr);
											while ($row = mysqli_fetch_row($resp)) 
											{
												if($row[0] == $_POST['nivelarl'])
												{
													echo "<option value='$row[0]' SELECTED>Nivel $row[1] ($row[2]) - $row[3]</option>";
												}
												else {echo "<option value='$row[0]'>Nivel $row[1] ($row[2]) - $row[3]</option>";}
											}
										?>
									</select>
								</td>
							</tr>
							<tr>
								<td class="tamano01">.: Tipo presupuesto:</td>
								<td>
									<select name="tpresupuestop" id="tpresupuestop" style="width:80%;" onChange="document.form2.submit();" >
										<option value="">Seleccione ....</option>
										<?php
											if ($_POST['tpresupuesto'] == 'F')
											{
												if($_POST['tpresupuestop']== 'F'){echo"<option value='F' SELECTED>FUNCIONAMIENTO</option>";}
											}
											elseif ($_POST['tpresupuesto'] == 'I')
											{
												if($_POST['tpresupuestop'] == 'F'){echo"<option value='F' SELECTED>FUNCIONAMIENTO</option>";}
												else {echo"<option value='F'>FUNCIONAMIENTO</option>";}
												if($_POST['tpresupuestop'] == 'I'){echo"<option value='I' SELECTED>INVERSION</option>";}
												else {echo"<option value='I'>INVERSION</option>";}
											}
											else
											{
												if($_POST['tpresupuestop'] == 'G'){echo"<option value='G' SELECTED>GASTOS COMERCIALIZACIÓN</option>";}
											}
										?>
									</select>
								</td>
							</tr>
						</table>
						<table class="inicio ancho">
							<tr><td class="titulos" colspan="19">.: Porcentajes</td></tr>
							<tr>
								<td class="tamano01" style="width:1.5cm;" title="Salud Funcionario">.: SF:</td>
								<td><input type="number" id="porsf" name="porsf" value="<?php echo $_POST['porsf']?>" onKeyUp="return tabular(event,this)" style="width:100%;text-align:center;"/></td>
								<td class="tamano01" style="width:1.5cm;" title="Salud Empresa">.: SE:</td>
								<td><input type="number" id="porse" name="porse" value="<?php echo $_POST['porse']?>" onKeyUp="return tabular(event,this)" style="width:100%;text-align:center;"/></td>
								<td class="tamano01" style="width:1.5cm;" title="Pension Funcionario">.: PF:</td>
								<td><input type="number" id="porpf" name="porpf" value="<?php echo $_POST['porpf']?>" onKeyUp="return tabular(event,this)" style="width:100%;text-align:center;"/></td>
								<td class="tamano01" style="width:1.5cm;" title="Pension Empresa">.: PE:</td>
								<td><input type="number" id="porpe" name="porpe" value="<?php echo $_POST['porpe']?>" onKeyUp="return tabular(event,this)" style="width:100%;text-align:center;"/></td>
								<td class="tamano01" style="width:1.5cm;" title="Caja de compensaci&oacute;n familiar">.: CCF:</td>
								<td><input type="number" id="porccf" name="porccf" value="<?php echo $_POST['porccf']?>" onKeyUp="return tabular(event,this)" style="width:100%;text-align:center;"/></td>
								<td class="tamano01" style="width:1.5cm;" title="ICBF">.: ICBF:</td>
								<td><input type="number" id="poricbf" name="poricbf" value="<?php echo $_POST['poricbf']?>" onKeyUp="return tabular(event,this)" style="width:100%;text-align:center;"/></td>
								<td class="tamano01" style="width:1.5cm;" title="SENA">.: SENA:</td>
								<td><input type="number" id="porsena" name="porsena" value="<?php echo $_POST['porsena']?>" onKeyUp="return tabular(event,this)" style="width:100%;text-align:center;"/></td>
								<td class="tamano01" style="width:1.5cm;" title="Institutos Tecnicos">.: INTE:</td>
								<td><input type="number" id="porinte" name="porinte" value="<?php echo $_POST['porinte']?>" onKeyUp="return tabular(event,this)" style="width:100%;text-align:center;"/></td>
								<td class="tamano01" style="width:1.5cm;" title="Institutos Tecnicos">.: ESAP:</td>
								<td><input type="number" id="poresap" name="poresap" value="<?php echo $_POST['poresap']?>" onKeyUp="return tabular(event,this)" style="width:100%;text-align:center;"/></td>
								<td></td>
							</tr>
						</table>
					</div>
				</div>
				<?php
					if($_POST['tpresupuesto'] == 'I')
					{
						echo"
						<div class='tab'>
							<input type='radio' id='tab-3' name='tabgroup1' value='3' $check3 >
							<label id='clabel' for='tab-3'>Proyecto</label>
							<div class='content' style='overflow:hidden;'>
								<table class='inicio ancho'>
									<tr>
										<td class='titulos' colspan='7'>.: Ingresar Información Proyecto</td>
										<td class='cerrar' style='width:7%' onClick=\"location.href='para-principal.php'\">Cerrar</td>
									</tr>
									<tr>
										<td class='tamano01' style='width:3.5cm;'>.: Proyecto:</td>
										<td><input type='text' name='nomproyecto' id='nomproyecto' value='".$_POST['nomproyecto']."' style='width:100%;text-transform:uppercase' class='colordobleclik' onDblClick=\"despliegamodal2('visible','7');\" readonly /></td>
										<input type='hidden' name='idproyecto' id='idproyecto' value='".$_POST['idproyecto']."'/>
									</tr>
									<tr>
										<td class='tamano01' style='width:3.5cm;'>.: Prográmatico:</td>
										<td><input type='text' name='nomprogramatico' id='nomprogramatico' value='".$_POST['nomprogramatico']."' style='width:100%;text-transform:uppercase' class='colordobleclik' onDblClick='validaproyecto();' readonly /></td>
										<input type='hidden' name='idprogramatico' id='idprogramatico' value='".$_POST['idprogramatico']."'/>
									</tr>
									<tr>
										<td class='tamano01' style='width:3.5cm;'>.: Fuente:</td>
										<td><input type='text' name='nomfuente' id='nomfuente' value='".$_POST['nomfuente']."' style='width:100%;text-transform:uppercase' class='colordobleclik' onDblClick='validaprogramatico()' readonly /></td>
										<input type='hidden' name='idfuente' id='idfuente' value='".$_POST['idfuente']."'/>
									</tr>
								</table>
							</div>
						</div>
						";
					}
				?>
				
			</div>
			<?php 
				if($_POST['oculto'] == 2)
				{
					unset($bdatos,$bfechas,$bids);
					for($xx = 1; $xx <= 45; $xx++)
					{
						$bdatos[$xx] = itemfuncionarios($_POST['idfun'],"$xx");
						$bfechas[$xx] = fechasfuncionarios($_POST['idfun'],"$xx");
						$bids[$xx] = idsfuncionarios($_POST['idfun'],"$xx");
					}
					
					$bsave = 0;
					preg_match("/([0-9]{2})\/([0-9]{2})\/([0-9]{4})/", $_POST['fechain'],$fecha);
					$fechaini = "$fecha[3]-$fecha[2]-$fecha[1]";
					preg_match("/([0-9]{2})\/([0-9]{2})\/([0-9]{4})/", $_POST['fechaeps'],$fecha);
					$fechainieps = "$fecha[3]-$fecha[2]-$fecha[1]";
					preg_match("/([0-9]{2})\/([0-9]{2})\/([0-9]{4})/", $_POST['fechaarl'],$fecha);
					$fechainiarl = "$fecha[3]-$fecha[2]-$fecha[1]";
					preg_match("/([0-9]{2})\/([0-9]{2})\/([0-9]{4})/", $_POST['fechafdc'],$fecha);
					$fechainifdc = "$fecha[3]-$fecha[2]-$fecha[1]";
					if($_POST['fechaafp'] != "")
					{
						preg_match("/([0-9]{2})\/([0-9]{2})\/([0-9]{4})/", $_POST['fechaafp'],$fecha);
						$fechainiafp = "$fecha[3]-$fecha[2]-$fecha[1]";
					}
					else{$fechainiafp = "0000-00-00";}
					if($_POST['swfun'] == 'S'){$actfun = 'S';}
					else {$actfun = 'N';}
					if($fechaini != $bfechas[26] || $actfun != $bdatos[26])
					{
						$sqlr = "UPDATE hum_funcionarios SET fechasal = '".$_POST['fechamodi']."', estado = 'N' WHERE codrad = '$bids[26]'";
						mysqli_query($linkbd,$sqlr);
						$sqlr = "INSERT INTO hum_funcionarios (codfun, item, descripcion, valor, fechain, fechasal, estado) VALUES ('".$_POST['idfun']."', 'ESTGEN', '$actfun', '26', '$fechaini', '0000-00-00', 'S')";
						mysqli_query($linkbd,$sqlr);
						$bsave = 1;
					}
					if( $_POST['idcargoad'] != $bdatos[1])
					{
						$sqlr = "UPDATE hum_funcionarios SET fechasal = '".$_POST['fechamodi']."', estado = 'N' WHERE codrad = '$bids[1]'";
						mysqli_query($linkbd,$sqlr);
						$sqlr = "INSERT INTO hum_funcionarios (codfun, item, descripcion, valor,fechain, fechasal, estado) VALUES ('".$_POST['idfun']."', 'IDCARGO', '".$_POST['idcargoad']."', '1', '".$_POST['fechamodi']."', '0000-00-00', 'S')";
						mysqli_query($linkbd,$sqlr);
						$bsave = 1;
					}
					if( $_POST['nomcargoad'] != $bdatos[2])
					{
						$sqlr = "UPDATE hum_funcionarios SET fechasal = '".$_POST['fechamodi']."', estado = 'N' WHERE codrad = '$bids[2]'";
						mysqli_query($linkbd,$sqlr);
						$sqlr = "INSERT INTO hum_funcionarios (codfun, item, descripcion, valor, fechain, fechasal, estado) VALUES ('".$_POST['idfun']."', 'NOMCARGO', '".$_POST['nomcargoad']."', '2', '".$_POST['fechamodi']."', '0000-00-00', 'S')";
						mysqli_query($linkbd,$sqlr);
						$bsave = 1;
					}
					if($_POST['cargo'] != $bdatos[3])
					{
						$sqlr = "UPDATE hum_funcionarios SET fechasal = '".$_POST['fechamodi']."', estado = 'N' WHERE codrad = '$bids[3]'";
						mysqli_query($linkbd,$sqlr);
						$sqlr = "INSERT INTO hum_funcionarios (codfun, item, descripcion, valor, fechain, fechasal, estado) VALUES ('".$_POST['idfun']."', 'IDESCALA', '".$_POST['cargo']."', '3', '".$_POST['fechamodi']."', '0000-00-00', 'S')";
						mysqli_query($linkbd,$sqlr);
						$bsave = 1;
					}
					if( $_POST['nivsal'] != $bdatos[4])
					{
						$sqlr = "UPDATE hum_funcionarios SET fechasal = '".$_POST['fechamodi']."', estado = 'N' WHERE codrad = '$bids[4]'";
						mysqli_query($linkbd,$sqlr);
						$sqlr = "INSERT INTO hum_funcionarios (codfun, item, descripcion, valor, fechain, fechasal, estado) VALUES ('".$_POST['idfun']."', 'INOMESCALA', '".$_POST['nivsal']."', '4', '".$_POST['fechamodi']."', '0000-00-00', 'S')";
						mysqli_query($linkbd,$sqlr);
						$bsave = 1;
					}
					if( $_POST['asigbas'] != $bdatos[5])
					{
						$sqlr = "UPDATE hum_funcionarios SET fechasal = '".$_POST['fechamodi']."', estado = 'N' WHERE codrad = '$bids[5]'";
						mysqli_query($linkbd,$sqlr);
						$sqlr = "INSERT INTO hum_funcionarios (codfun, item, descripcion, valor, fechain, fechasal,estado) VALUES ('".$_POST['idfun']."', 'VALESCALA', '".$_POST['asigbas']."', '5', '".$_POST['fechamodi']."', '0000-00-00', 'S')";
						mysqli_query($linkbd,$sqlr);
						$bsave = 1;
					}
					if( $_POST['tercero'] != $bdatos[6])
					{
						$sqlr = "UPDATE hum_funcionarios SET fechasal = '".$_POST['fechamodi']."', estado = 'N' WHERE codrad = '$bids[6]'";
						mysqli_query($linkbd,$sqlr);
						$sqlr = "INSERT INTO hum_funcionarios (codfun, item, descripcion, valor, fechain, fechasal, estado) VALUES ('".$_POST['idfun']."', 'DOCTERCERO', '".$_POST['tercero']."', '6', '".$_POST['fechamodi']."', '0000-00-00', 'S')";
						mysqli_query($linkbd,$sqlr);
						$bsave = 1;
					}
					if( $_POST['ntercero'] != $bdatos[7])
					{
						$sqlr = "UPDATE hum_funcionarios SET fechasal = '".$_POST['fechamodi']."', estado = 'N' WHERE codrad = '$bids[7]'";
						mysqli_query($linkbd,$sqlr);
						$sqlr = "INSERT INTO hum_funcionarios (codfun, item, descripcion, valor, fechain, fechasal, estado) VALUES ('".$_POST['idfun']."', 'NOMTERCERO', '".$_POST['ntercero']."', '7', '".$_POST['fechamodi']."', '0000-00-00', 'S')";
						mysqli_query($linkbd,$sqlr);
						$bsave =  1;
					}
					if( $_POST['direccion'] != $bdatos[8])
					{
						$sqlr = "UPDATE hum_funcionarios SET fechasal = '".$_POST['fechamodi']."', estado = 'N' WHERE codrad=  '$bids[8]'";
						mysqli_query($linkbd,$sqlr);
						$sqlr = "INSERT INTO hum_funcionarios (codfun, item, descripcion, valor, fechain, fechasal, estado) VALUES ('".$_POST['idfun']."', 'DIRTERCERO', '".$_POST['direccion']."', '8', '".$_POST['fechamodi']."', '0000-00-00', 'S')";
						mysqli_query($linkbd,$sqlr);
						$bsave = 1;
					}
					if( $_POST['telefono'] != $bdatos[9])
					{
						$sqlr = "UPDATE hum_funcionarios SET fechasal = '".$_POST['fechamodi']."', estado = 'N' WHERE codrad = '$bids[9]'";
						mysqli_query($linkbd,$sqlr);
						$sqlr = "INSERT INTO hum_funcionarios (codfun, item, descripcion, valor, fechain, fechasal, estado) VALUES ('".$_POST['idfun']."', 'TELTERCERO', '".$_POST['telefono']."', '9', '".$_POST['fechamodi']."', '0000-00-00', 'S')";
						mysqli_query($linkbd,$sqlr);
						$bsave = 1;
					}
					if( $_POST['celular'] != $bdatos[10])
					{
						$sqlr = "UPDATE hum_funcionarios SET fechasal = '".$_POST['fechamodi']."', estado = 'N' WHERE codrad ='$bids[10]'";
						mysqli_query($linkbd,$sqlr);
						$sqlr = "INSERT INTO hum_funcionarios (codfun, item, descripcion, valor, fechain, fechasal, estado) VALUES ('".$_POST['idfun']."', 'CELTERCERO', '".$_POST['celular']."', '10', '".$_POST['fechamodi']."', '0000-00-00', 'S')";
						mysqli_query($linkbd,$sqlr);
						$bsave = 1;
					}
					if( $_POST['email'] != $bdatos[11])
					{
						$sqlr = "UPDATE hum_funcionarios SET fechasal = '".$_POST['fechamodi']."', estado = 'N' WHERE codrad = '$bids[11]'";
						mysqli_query($linkbd,$sqlr);
						$sqlr = "INSERT INTO hum_funcionarios (codfun, item, descripcion, valor, fechain, fechasal, estado) VALUES ('".$_POST['idfun']."', 'EMATERCERO', '".$_POST['email']."', '11', '".$_POST['fechamodi']."', '0000-00-00', 'S')";
						mysqli_query($linkbd,$sqlr);
						$bsave = 1;
					}
					if( $_POST['tercerocta'] != $bdatos[12])
					{
						$sqlr = "UPDATE hum_funcionarios SET fechasal = '".$_POST['fechamodi']."', estado = 'N' WHERE codrad = '$bids[12]'";
						mysqli_query($linkbd,$sqlr);
						$sqlr = "INSERT INTO hum_funcionarios (codfun, item, descripcion, valor, fechain, fechasal, estado) VALUES ('".$_POST['idfun']."', 'NUMCUENTA', '".$_POST['tercerocta']."', '12', '".$_POST['fechamodi']."', '0000-00-00', 'S')";
						mysqli_query($linkbd,$sqlr);
						$bsave = 1;
					}
					if( $_POST['bancocta'] != $bdatos[13])
					{
						$sqlr = "UPDATE hum_funcionarios SET fechasal = '".$_POST['fechamodi']."', estado = 'N' WHERE codrad = '$bids[13]'";
						mysqli_query($linkbd,$sqlr);
						$sqlr = "INSERT INTO hum_funcionarios (codfun, item, descripcion, valor, fechain, fechasal, estado) VALUES ('".$_POST['idfun']."', 'NOMCUENTA', '".$_POST['bancocta']."', '13', '".$_POST['fechamodi']."', '0000-00-00', 'S')";
						mysqli_query($linkbd,$sqlr);
						$bsave = 1;
					}
					if( $_POST['eps'] != $bdatos[14])
					{
						$sqlr = "UPDATE hum_funcionarios SET fechasal = '".$_POST['fechamodi']."', estado = 'N' WHERE codrad = '$bids[14]'";
						mysqli_query($linkbd,$sqlr);
						$sqlr = "INSERT INTO hum_funcionarios (codfun, item, descripcion, valor, fechain, fechasal, estado) VALUES ('".$_POST['idfun']."', 'NUMEPS', '".$_POST['eps']."', '14', '$fechainieps', '0000-00-00','S')";
						mysqli_query($linkbd,$sqlr);
						$bsave = 1;
					}
					if( $_POST['neps'] != $bdatos[15])
					{
						$sqlr = "UPDATE hum_funcionarios SET fechasal = '".$_POST['fechamodi']."', estado = 'N' WHERE codrad = '$bids[15]'";
						mysqli_query($linkbd,$sqlr);
						$sqlr = "INSERT INTO hum_funcionarios (codfun, item, descripcion, valor, fechain, fechasal, estado) VALUES ('".$_POST['idfun']."', 'NOMEPS', '".$_POST['neps']."', '15', '$fechainieps', '0000-00-00', 'S')";
						mysqli_query($linkbd,$sqlr);
						$bsave = 1;
					}
					if( $_POST['arp'] != $bdatos[16])
					{
						$sqlr = "UPDATE hum_funcionarios SET fechasal = '".$_POST['fechamodi']."', estado = 'N' WHERE codrad = '$bids[16]'";
						mysqli_query($linkbd,$sqlr);
						$sqlr = "INSERT INTO hum_funcionarios (codfun, item, descripcion, valor, fechain, fechasal, estado) VALUES ('".$_POST['idfun']."', 'NUMARL', '".$_POST['arp']."', '16', '$fechainiarl', '0000-00-00', 'S')";
						mysqli_query($linkbd,$sqlr);
						$bsave = 1;
					}
					if( $_POST['narp'] != $bdatos[17])
					{
						$sqlr = "UPDATE hum_funcionarios SET fechasal = '".$_POST['fechamodi']."', estado = 'N' WHERE codrad = '$bids[17]'";
						mysqli_query($linkbd,$sqlr);
						$sqlr = "INSERT INTO hum_funcionarios (codfun, item, descripcion, valor, fechain, fechasal, estado) VALUES ('".$_POST['idfun']."', 'NOMARL', '".$_POST['narp']."', '17', '$fechainiarl', '0000-00-00', 'S')";
						mysqli_query($linkbd,$sqlr);
						$bsave = 1;
					}
					if( $_POST['afp'] != $bdatos[18])
					{
						$sqlr = "UPDATE hum_funcionarios SET fechasal = '".$_POST['fechamodi']."', estado = 'N' WHERE codrad = '$bids[18]'";
						mysqli_query($linkbd,$sqlr);
						$sqlr = "INSERT INTO hum_funcionarios (codfun, item, descripcion, valor, fechain, fechasal, estado) VALUES ('".$_POST['idfun']."', 'NUMAFP', '".$_POST['afp']."', '18', '$fechainiafp', '0000-00-00', 'S')";
						mysqli_query($linkbd,$sqlr);
						$bsave = 1;
					}
					if( $_POST['nafp'] != $bdatos[19])
					{
						$sqlr = "UPDATE hum_funcionarios SET fechasal = '".$_POST['fechamodi']."', estado = 'N' WHERE codrad = '$bids[19]'";
						mysqli_query($linkbd,$sqlr);
						$sqlr = "INSERT INTO hum_funcionarios (codfun, item, descripcion, valor, fechain, fechasal, estado) VALUES ('".$_POST['idfun']."', 'NOMAFP', '".$_POST['nafp']."', '19', '$fechainiafp', '0000-00-00', 'S')";
						mysqli_query($linkbd,$sqlr);
						$bsave = 1;
					}
					if( $_POST['fondocesa'] != $bdatos[20])
					{
						$sqlr ="UPDATE hum_funcionarios SET fechasal = '".$_POST['fechamodi']."', estado = 'N' WHERE codrad = '$bids[20]'";
						mysqli_query($linkbd,$sqlr);
						$sqlr = "INSERT INTO hum_funcionarios (codfun, item, descripcion, valor, fechain, fechasal, estado) VALUES ('".$_POST['idfun']."', 'NUMFDC', '".$_POST['fondocesa']."', '20', '$fechainiafp', '0000-00-00', 'S')";
						mysqli_query($linkbd,$sqlr);
						$bsave = 1;
					}
					if( $_POST['nfondocesa'] != $bdatos[21])
					{
						$sqlr = "UPDATE hum_funcionarios SET fechasal = '".$_POST['fechamodi']."', estado = 'N' WHERE codrad = '$bids[21]'";
						mysqli_query($linkbd,$sqlr);
						$sqlr = "INSERT INTO hum_funcionarios (codfun, item, descripcion, valor, fechain, fechasal, estado) VALUES ('".$_POST['idfun']."', 'NOMFDC', '".$_POST['nfondocesa']."', '21', '$fechainiafp', '0000-00-00', 'S')";
						mysqli_query($linkbd,$sqlr);
						$bsave = 1;
					}
					if( $_POST['numcc'] != $bdatos[22])
					{
						$sqlr = "UPDATE hum_funcionarios SET fechasal = '".$_POST['fechamodi']."', estado = 'N' WHERE codrad = '$bids[22]'";
						mysqli_query($linkbd,$sqlr);
						$sqlr = "INSERT INTO hum_funcionarios (codfun, item, descripcion, valor, fechain, fechasal, estado) VALUES ('".$_POST['idfun']."', 'NUMCC', '".$_POST['numcc']."', '22', '".$_POST['fechamodi']."', '0000-00-00', 'S')";
						mysqli_query($linkbd,$sqlr);
						$bsave = 1;
					}
					if( $_POST['nomcc'] != $bdatos[23])
					{
						$sqlr = "UPDATE hum_funcionarios SET fechasal = '".$_POST['fechamodi']."', estado = 'N' WHERE codrad = '$bids[23]'";
						mysqli_query($linkbd,$sqlr);
						$sqlr = "INSERT INTO hum_funcionarios (codfun, item, descripcion, valor, fechain, fechasal, estado) VALUES ('".$_POST['idfun']."', 'NOMCC', '".$_POST['nomcc']."', '23', '".$_POST['fechamodi']."', '0000-00-00', 'S')";
						mysqli_query($linkbd,$sqlr);
						$bsave = 1;
					}
					if( $_POST['tperiodo'] != $bdatos[24])
					{
						$sqlr = "UPDATE hum_funcionarios SET fechasal = '".$_POST['fechamodi']."', estado = 'N' WHERE codrad = '$bids[24]'";
						mysqli_query($linkbd,$sqlr);
						$sqlr = "INSERT INTO hum_funcionarios (codfun, item, descripcion, valor, fechain, fechasal, estado) VALUES ('".$_POST['idfun']."', 'PERLIQ', '".$_POST['tperiodo']."', '24', '".$_POST['fechamodi']."', '0000-00-00', 'S')";
						mysqli_query($linkbd,$sqlr);
						$bsave = 1;
					}
					if( $_POST['pagces'] != $bdatos[25])
					{
						$sqlr = "UPDATE hum_funcionarios SET fechasal = '".$_POST['fechamodi']."', estado = 'N' WHERE codrad = '$bids[25]'";
						mysqli_query($linkbd,$sqlr);
						$sqlr = "INSERT INTO hum_funcionarios (codfun, item, descripcion, valor, fechain, fechasal, estado) VALUES ('".$_POST['idfun']."', 'TPCESANTIAS', '".$_POST['pagces']."', '25', '".$_POST['fechamodi']."', '0000-00-00', 'S')";
						mysqli_query($linkbd,$sqlr);
						$bsave = 1;
					}
					if($_POST['nivelarl'] != $bdatos[27])
					{
						$sqlr = "UPDATE hum_funcionarios SET fechasal = '".$_POST['fechamodi']."', estado = 'N' WHERE codrad = '$bids[27]'";
						mysqli_query($linkbd,$sqlr);
						$sqlr = "INSERT INTO hum_funcionarios (codfun, item, descripcion, valor, fechain, fechasal, estado) VALUES ('".$_POST['idfun']."', 'NIVELARL', '".$_POST['nivelarl']."', '27', '".$_POST['fechamodi']."', '0000-00-00', 'S')";
						mysqli_query($linkbd,$sqlr);
						$bsave = 1;
					}
					if($_POST['pvinculacion'] != $bdatos[28])
					{
						$sqlr = "UPDATE hum_funcionarios SET fechasal = '".$_POST['fechamodi']."', estado = 'N' WHERE codrad = '$bids[28]'";
						mysqli_query($linkbd,$sqlr);
						$sqlr = "INSERT INTO hum_funcionarios (codfun, item, descripcion, valor, fechain, fechasal, estado) VALUES ('".$_POST['idfun']."', 'TVINCULACION', '".$_POST['pvinculacion']."', '28', '".$_POST['fechamodi']."', '0000-00-00', 'S')";
						mysqli_query($linkbd,$sqlr);
						$bsave = 1;
					}
					if($_POST['uniejecutora'] != $bdatos[29])
					{
						$sqlr = "UPDATE hum_funcionarios SET fechasal = '".$_POST['fechamodi']."', estado = 'N' WHERE codrad = '$bids[29]'";
						mysqli_query($linkbd,$sqlr);
						$sqlr = "INSERT INTO hum_funcionarios (codfun, item, descripcion, valor, fechain, fechasal, estado) VALUES ('".$_POST['idfun']."', 'UNIEJECUTORA', '".$_POST['uniejecutora']."', '29','".$_POST['fechamodi']."', '0000-00-00', 'S')";
						mysqli_query($linkbd,$sqlr);
						$bsave = 1;
					}
					if($_POST['tpresupuesto'] != $bdatos[30])
					{
						$sqlr = "UPDATE hum_funcionarios SET fechasal = '".$_POST['fechamodi']."', estado = 'N' WHERE codrad = '$bids[30]'";
						mysqli_query($linkbd,$sqlr);
						$sqlr = "INSERT INTO hum_funcionarios (codfun, item, descripcion, valor, fechain, fechasal, estado) VALUES ('".$_POST['idfun']."', 'TPRESUPUESTO', '".$_POST['tpresupuesto']."', '30', '".$_POST['fechamodi']."', '0000-00-00', 'S')";
						mysqli_query($linkbd,$sqlr);
						$bsave = 1;
					}
					if($_POST['idproyecto'] != $bdatos[31])
					{
						$sqlr = "UPDATE hum_funcionarios SET fechasal = '".$_POST['fechamodi']."', estado = 'N' WHERE codrad = '$bids[31]'";
						mysqli_query($linkbd,$sqlr);
						$sqlr = "INSERT INTO hum_funcionarios (codfun, item, descripcion, valor, fechain, fechasal, estado) VALUES ('".$_POST['idfun']."', 'PROYECTOINV', '".$_POST['idproyecto']."', '31', '".$_POST['fechamodi']."', '0000-00-00', 'S')";
						mysqli_query($linkbd,$sqlr);
						$bsave = 1;
					}
					if($_POST['porsf'] != $bdatos[32])
					{
						$sqlr = "UPDATE hum_funcionarios SET fechasal = '".$_POST['fechamodi']."', estado = 'N' WHERE codrad = '$bids[32]'";
						mysqli_query($linkbd,$sqlr);
						$sqlr = "INSERT INTO hum_funcionarios (codfun, item, descripcion, valor, fechain, fechasal, estado) VALUES ('".$_POST['idfun']."', 'PORSF', '".$_POST['porsf']."', '32', '".$_POST['fechamodi']."', '0000-00-00', 'S')";
						mysqli_query($linkbd,$sqlr);
						$bsave = 1;
					}
					if($_POST['porse'] != $bdatos[33])
					{
						$sqlr = "UPDATE hum_funcionarios SET fechasal = '".$_POST['fechamodi']."', estado = 'N' WHERE codrad = '$bids[33]'";
						mysqli_query($linkbd,$sqlr);
						$sqlr = "INSERT INTO hum_funcionarios (codfun, item, descripcion, valor, fechain, fechasal, estado) VALUES ('".$_POST['idfun']."', 'PORSE', '".$_POST['porse']."', '33', '".$_POST['fechamodi']."', '0000-00-00', 'S')";
						mysqli_query($linkbd,$sqlr);
						$bsave = 1;
					}
					if($_POST['porpf'] != $bdatos[34])
					{
						$sqlr = "UPDATE hum_funcionarios SET fechasal = '".$_POST['fechamodi']."', estado = 'N' WHERE codrad = '$bids[34]'";
						mysqli_query($linkbd,$sqlr);
						$sqlr="INSERT INTO hum_funcionarios (codfun, item, descripcion, valor, fechain, fechasal, estado) VALUES ('".$_POST['idfun']."', 'PORPF', '".$_POST['porpf']."', '34', '".$_POST['fechamodi']."', '0000-00-00', 'S')";
						mysqli_query($linkbd,$sqlr);
						$bsave = 1;
					}
					if($_POST['porpe'] != $bdatos[35])
					{
						$sqlr = "UPDATE hum_funcionarios SET fechasal = '".$_POST['fechamodi']."', estado = 'N' WHERE codrad = '$bids[35]'";
						mysqli_query($linkbd,$sqlr);
						$sqlr = "INSERT INTO hum_funcionarios (codfun, item, descripcion, valor, fechain, fechasal, estado) VALUES ('".$_POST['idfun']."', 'PORPE', '".$_POST['porpe']."', '35', '".$_POST['fechamodi']."', '0000-00-00', 'S')";
						mysqli_query($linkbd,$sqlr);
						$bsave = 1;
					}
					if($_POST['porccf'] != $bdatos[36])
					{
						$sqlr = "UPDATE hum_funcionarios SET fechasal = '".$_POST['fechamodi']."', estado = 'N' WHERE codrad = '$bids[36]'";
						mysqli_query($linkbd,$sqlr);
						$sqlr="INSERT INTO hum_funcionarios (codfun, item, descripcion, valor, fechain, fechasal, estado) VALUES ('".$_POST['idfun']."', 'PORCCF', '".$_POST['porccf']."', '36', '".$_POST['fechamodi']."', '0000-00-00', 'S')";
						mysqli_query($linkbd,$sqlr);
						$bsave = 1;
					}
					if($_POST['poricbf'] != $bdatos[37])
					{
						$sqlr = "UPDATE hum_funcionarios SET fechasal = '".$_POST['fechamodi']."', estado = 'N' WHERE codrad = '$bids[37]'";
						mysqli_query($linkbd,$sqlr);
						$sqlr="INSERT INTO hum_funcionarios (codfun, item, descripcion, valor, fechain, fechasal, estado) VALUES ('".$_POST['idfun']."', 'PORICBF', '".$_POST['poricbf']."', '37', '".$_POST['fechamodi']."', '0000-00-00', 'S')";
						mysqli_query($linkbd,$sqlr);
						$bsave = 1;
					}
					if($_POST['porsena'] != $bdatos[38])
					{
						$sqlr = "UPDATE hum_funcionarios SET fechasal = '".$_POST['fechamodi']."', estado = 'N' WHERE codrad = '$bids[38]'";
						mysqli_query($linkbd,$sqlr);
						$sqlr = "INSERT INTO hum_funcionarios (codfun, item, descripcion, valor, fechain, fechasal, estado) VALUES ('".$_POST['idfun']."', 'PORSENA', '".$_POST['porsena']."', '38', '".$_POST['fechamodi']."', '0000-00-00', 'S')";
						mysqli_query($linkbd,$sqlr);
						$bsave = 1;
					}
					if($_POST['porinte'] != $bdatos[39])
					{
						$sqlr = "UPDATE hum_funcionarios SET fechasal = '".$_POST['fechamodi']."', estado = 'N' WHERE codrad = '$bids[39]'";
						mysqli_query($linkbd,$sqlr);
						$sqlr = "INSERT INTO hum_funcionarios (codfun, item, descripcion, valor, fechain, fechasal, estado) VALUES ('".$_POST['idfun']."', 'PORINTE', '".$_POST['porinte']."', '39', '".$_POST['fechamodi']."', '0000-00-00', 'S')";
						mysqli_query($linkbd,$sqlr);
						$bsave = 1;
					}
					if($_POST['poresap'] != $bdatos[40])
					{
						$sqlr ="UPDATE hum_funcionarios SET fechasal = '".$_POST['fechamodi']."', estado = 'N' WHERE codrad = '$bids[40]'";
						mysqli_query($linkbd,$sqlr);
						$sqlr = "INSERT INTO hum_funcionarios (codfun, item, descripcion, valor, fechain, fechasal, estado) VALUES ('".$_POST['idfun']."', 'PORESAP', '".$_POST['poresap']."', '40', '".$_POST['fechamodi']."', '0000-00-00', 'S')";
						mysqli_query($linkbd,$sqlr);
						$bsave = 1;
					}
					if($_POST['tpresupuestop'] != $bdatos[41])
					{
						$sqlr = "UPDATE hum_funcionarios SET fechasal = '".$_POST['fechamodi']."', estado = 'N' WHERE codrad = '$bids[41]'";
						mysqli_query($linkbd,$sqlr);
						$sqlr="INSERT INTO hum_funcionarios (codfun, item, descripcion, valor, fechain, fechasal, estado) VALUES ('".$_POST['idfun']."', 'TPRESUPUESTOPARA', '".$_POST['tpresupuestop']."', '41', '".$_POST['fechamodi']."', '0000-00-00', 'S')";
						mysqli_query($linkbd,$sqlr);
						$bsave = 1;
					}
					if($_POST['secpresupuestal'] != $bdatos[42])
					{
						$sqlr = "UPDATE hum_funcionarios SET fechasal='".$_POST['fechamodi']."', estado = 'N' WHERE codrad = '$bids[42]'";
						mysqli_query($linkbd,$sqlr);
						$sqlr="INSERT INTO hum_funcionarios (codfun, item, descripcion, valor, fechain, fechasal, estado) VALUES ('".$_POST['idfun']."', 'SECCIONPRESUP', '".$_POST['secpresupuestal']."', '42', '".$_POST['fechamodi']."', '0000-00-00', 'S')";
						mysqli_query($linkbd,$sqlr);
						$bsave = 1;
					}
					if($_POST['idprogramatico'] != $bdatos[43])
					{
						$sqlr = "UPDATE hum_funcionarios SET fechasal = '".$_POST['fechamodi']."', estado = 'N' WHERE codrad = '$bids[43]'";
						mysqli_query($linkbd,$sqlr);
						$sqlr = "INSERT INTO hum_funcionarios (codfun, item, descripcion, valor, fechain, fechasal, estado) VALUES ('".$_POST['idfun']."', 'PROGRAMATICO', '".$_POST['idprogramatico']."', '43', '".$_POST['fechamodi']."', '0000-00-00', 'S')";
						mysqli_query($linkbd,$sqlr);
						$bsave = 1;
					}
					if($_POST['idfuente'] != $bdatos[44])
					{
						$sqlr = "UPDATE hum_funcionarios SET fechasal = '".$_POST['fechamodi']."', estado = 'N' WHERE codrad = '$bids[44]'";
						mysqli_query($linkbd,$sqlr);
						$sqlr = "INSERT INTO hum_funcionarios (codfun, item, descripcion, valor, fechain, fechasal, estado) VALUES ('".$_POST['idfun']."', 'FUENTEINV', '".$_POST['idfuente']."', '44', '".$_POST['fechamodi']."', '0000-00-00', 'S')";
						mysqli_query($linkbd,$sqlr);
						$bsave = 1;
					}
					if($_POST['tpresupuestoOP'] != $bdatos[45])
					{
						$sqlr = "UPDATE hum_funcionarios SET fechasal = '".$_POST['fechamodi']."', estado = 'N' WHERE codrad = '$bids[45]'";
						mysqli_query($linkbd,$sqlr);
						$sqlr = "INSERT INTO hum_funcionarios (codfun, item, descripcion, valor, fechain, fechasal, estado) VALUES ('".$_POST['idfun']."', 'TPRESUPUESTOOTROS', '".$_POST['tpresupuestoOP']."', '45', '".$_POST['fechamodi']."', '0000-00-00', 'S')";
						mysqli_query($linkbd,$sqlr);
						$bsave = 1;
					}
					if ($bsave == 1){echo "<script>despliegamodalm('visible','3','Se ha modificado con Exito el funcionario');</script>";}
				}
			?>
		</form>
		<div id="bgventanamodal2">
			<div id="ventanamodal2">
				<IFRAME  src="" name="buster" marginWidth=0 marginHeight=0 frameBorder=0 id="ventana2" frameSpacing=0 style="left:500px; width:900px; height:500px; top:200;"></IFRAME>
			</div>
		</div>
	</body>
</html>
