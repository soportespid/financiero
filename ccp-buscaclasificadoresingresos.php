<?php
    header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=utf8");

	require 'comun.inc';
	require 'funciones.inc';

	session_start();
    date_default_timezone_set("America/Bogota");

?>
<!DOCTYPE >
<html lang="es">
	<head>
		<meta charset="utf-8"/>
		<meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1"/>
		<meta name="viewport" content="user-scalable=no">
		<title>:: Ideal - Presupuesto</title>
		<link href="favicon.ico" rel="shortcut icon"/>
        
		<link href="css/css2.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<link href="css/css2z.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
        <link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
        <link href="css/css4.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
        <script type="text/javascript" src="css/programas.js"></script>
        <script type="text/javascript" src="css/calendario.js"></script>
        <script type="text/javascript" src="JQuery/jquery-2.1.4.min.js"></script>

		<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">

		<style>
            .background_active{
				/* font: 115% sans-serif !important; */
    			font-weight: 700 !important;
			}
			.inicio--no-shadow{
				box-shadow: none;
			}
			.btn-delete{
				background: red; 	
				color: white;
				border-radius: 5px;
				border: none;
				font-size: 13px;
			}
			.btn-delete:hover, .btn-delete:focus{
				background: white; 	
				color: red;
			}

			.btn-edit{
				background: #ffc107; 	
				color: white;
				border-radius: 5px;
				border-color: #ffc107;
				border: none;
				font-size: 13px;
				margin-right: 5px;
			}
			.btn-edit:hover, .btn-delete:focus{
				background: white; 	
				color: #ffc107;
			}

			.btn-ver{
				background: #049432;
				color: white;
				border-radius: 5px;
				border-color: #05a642;
				border: none;
				font-size: 13px;
				margin-right: 5px;
			}
			.btn-ver:hover, .btn-delete:focus{
				background: white; 	
				color: #017011;
			}

			[v-cloak]{
				display : none;
			}

		</style>
    </head>
    <body>
        <IFRAME src="alertas.php" name="alertas" id="alertas" style="display:none"></IFRAME>
        <span id="todastablas2"></span>
        <table>
            <tr><script>barra_imagenes("ccpet");</script><?php cuadro_titulos();?></tr>	 
            <tr><?php menu_desplegable("ccpet");?></tr>
        	<tr>
          		<td colspan="3" class="cinta">
					<a><img src="imagenes/add.png" title="Nuevo" onClick="location.href='ccp-generarclasificadoringresos.php'" class="mgbt"/></a>
					<a class="mgbt"><img src="imagenes/guardad.png"/></a>
					<a><img src="imagenes/busca.png" title="Buscar"  onClick="location.href='ccp-buscaclasificadoresingresos.php'" class="mgbt"/></a>
					<a href="#" onClick="mypop=window.open('ccp-principal.php','','');mypop.focus();" class="mgbt"><img src="imagenes/nv.png" title="Nueva Ventana"></a>
					<img src="imagenes/iratras.png" title="Atr&aacute;s" onClick="location.href='ccp-generarclasificadoringresos.php'" class="mgbt"/>
				</td>
        	</tr>
		</table>
		<div class="subpantalla" style="height:80.5%; width:99.6%; overflow-x:hidden; resize: vertical;">
			<div id="myapp" style="height:inherit;" v-cloak>
				<div class="row">
					<div class="col-12">
						<h4 style="padding-left:50px; padding-top:5px; padding-bottom:5px; background-color: #0FB0D4">Buscar clasificadores de ingresos:</h4>
					</div>
				</div>
				<div class="row" style="margin: 1px 50px 0px">
					<div class="col-12">
						<div class="row" style="border-radius:4px; background-color: #E1E2E2; ">
							<div class="col-md-2" style="display: grid; align-content:center;">
								<label for="" style="margin-bottom: 0; font-weight: bold;">Buscar clasificador:</label>
							</div>
							<div class="col-md-8" style="padding: 4px">
								<input v-on:keyup="searchMonitor" v-model="search.keyword" type="text" class="form-control" style="height: auto; border-radius:0;" placeholder="Nombre de clasificador">
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-12" style="height: fit-content;">
						<div style="margin: 0px 50px 0">
							<table>
								<thead>
									<tr>
										<td class='titulos'  style="padding-left: 10px; font: 160% sans-serif;">Nombre del clasificador</td>
										<td class='titulos' width="10%" style="font: 160% sans-serif; text-align: center;">Estado</td>
										<td class='titulos' width="20%" style="font: 160% sans-serif; text-align: center;">Fecha creado</td>
										<td class='titulos' width="20%" style="font: 160% sans-serif; text-align: center;">Acci&oacute;n</td>
                                        <td width="15px" class='titulos' style="font: 160% sans-serif; border-radius: 0px 5px 0px 0px;"></td>
									</tr>
								</thead>
							</table>
						</div>
						<div style="margin: 0px 50px 20px; border-radius: 0 0 0 5px; height: 50%; overflow: scroll; overflow-x: hidden; background: white; ">
							<table class='inicio inicio--no-shadow'>
								<tbody v-if="show_resultados">
									<!-- v-on:click="seleccionaCodigos(result)" -->
									<tr v-for="(result, index) in results" v-bind:class="index % 2 ? 'contenidonew00' : 'contenidonew01'" class=' style=\"cursor: hand\"' :style="estaEnArray(result) == true ? myStyleSelect : ''" style="font: 130% sans-serif; text-rendering: optimizeLegibility; cursor: pointer;">
										<td style="padding-left: 10px;" v-on:click="showClasificador(result)" >{{ result[1] }}</td>
										<td width="10%"  v-on:click="showClasificador(result)" style="text-align: center">{{ estadoClasificador(result[2]) }}</td>
										<td width="20%" v-on:click="showClasificador(result)" style="text-align: center">{{ result[4].split('-').reverse().join('/') }}</td>
										<td width="20%" style="text-align: center"> 
											<button class="btn btn-ver" v-on:click="showClasificador(result)">Ver</button>
											<button class="btn btn-edit" v-on:click="editarClasificador(result[0], result[1])">Editar</button>
											<button class="btn btn-delete" v-on:click="deleteClasificador(result[0])">Eliminar</button>
										</td>
									</tr>
								</tbody>
								<tbody v-else>
									<tr>
										<td width="20%"style="font: 120% sans-serif; padding-left:10px; text-align:center;" colspan="3">No hay clasificadores</td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-12" v-show="show_results_det">
						<div style="margin: 0px 50px 0">
							<table>
								<thead>
									<tr>
										<td class='titulos' width="30%"  style="padding-left: 10px; font: 160% sans-serif; border-radius: 5px 0px 0px 0px;">Rubro presupuestal</td>
										<td class='titulos' style="font: 160% sans-serif;">Nombre</td>
										<td class='titulos' width="10%" style="font: 160% sans-serif; text-align: center">Tipo</td>
                                        <td width="15px" class='titulos' style="font: 160% sans-serif; border-radius: 0px 5px 0px 0px;"></td>
									</tr>
								</thead>
							</table>
						</div>
						<div style="margin: 0px 50px 30px; border-radius: 0 0 0 5px; max-height: 450px; overflow: scroll; overflow-x: hidden; background: white; ">
							<table class='inicio inicio--no-shadow'>
								<tbody v-if="show_resultados">
									
									<tr v-for="(result, index) in results_det" :class="[result[2] == 'A' ? 'background_active' : '',  index % 2 ? 'contenidonew00' : 'contenidonew01']" style="font: 130% sans-serif; text-rendering: optimizeLegibility; cursor: pointer important;">
										<td width="30%" style="padding-left: 10px;">{{ result[0] }}</td>
										<td>{{ result[1] }}</td>
										<td width="10%" style="text-align: center">{{ result[2] }}</td>
										
									</tr>
								</tbody>
								<tbody v-else>
									<tr>
										<td width="20%"style="font: 120% sans-serif; padding-left:10px; text-align:center;" colspan="3">Sin resultados</td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
				</div>
				<span id="end_page"> </span>
                <div id="cargando" v-if="loading" class="loading">
                    <span>Cargando...</span>
                </div>
			</div>	
		</div>

        <script type="module" src="./presupuesto_ccpet/clasificadores/ccp-buscaclasificadoresingresos.js"></script>
        <script src="Librerias/vue/axios.min.js"></script>
        <script src="sweetalert2/dist/sweetalert2.min.js"></script>
        <link rel="stylesheet" href="sweetalert2/dist/sweetalert2.min.css">
        <script src="https://unpkg.com/vue@3/dist/vue.global.js"></script>
	</body>
</html>