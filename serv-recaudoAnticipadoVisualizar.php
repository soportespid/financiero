<?php
	header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=iso-8859-1");
	require "comun.inc";
	require "funciones.inc";
	require "funcionesSP.inc.php";
	require "conversor.php";
	session_start();
	$linkbd = conectar_v7();
	cargarcodigopag($_GET['codpag'],$_SESSION["nivel"]);
	date_default_timezone_set("America/Bogota");

    $scroll=$_GET['scrtop'];
	$totreg=$_GET['totreg'];
	$idcta=$_GET['idcta'];
	$altura=$_GET['altura'];
	$filtro="'".$_GET['filtro']."'";
?>
<!DOCTYPE >
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="es">
	<head>
		<meta http-equiv="Content-type" content="text/html; charset=iso-8859-1"/>
		<meta http-equiv="X-UA-Compatible" content="IE=9"/>
		<title>:: IDEAL 10 - Servicios P&uacute;blicos</title>
		<link href="css/css2.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<link href="css/css4.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<link href="css/cssSP.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<link href="css/style.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css"/>
		<script type="text/javascript" src="css/programas.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
        <script type="text/javascript" src="css/calendario.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<style>
		</style>

		<script>
			function despliegamodalm(_valor,_tip,mensa,pregunta)
			{
				document.getElementById("bgventanamodalm").style.visibility=_valor;

				if(_valor=="hidden")
				{
					document.getElementById('ventanam').src="";
				}
				else
				{
					switch(_tip)
					{
						case "1":	
							document.getElementById('ventanam').src="ventana-mensaje1.php?titulos="+mensa;
						break;

						case "2":	
							document.getElementById('ventanam').src="ventana-mensaje3.php?titulos="+mensa;
						break;

						case "3":	
							document.getElementById('ventanam').src="ventana-mensaje2.php?titulos="+mensa;
						break;

						case "4":	
							document.getElementById('ventanam').src="ventana-consulta1.php?titulos="+mensa+"&idresp="+pregunta;
						break;	
					}
				}
			}

			function funcionmensaje()
			{
				var idban=document.getElementById('codban').value;

				document.location.href = "serv-recaudoAnticipadoBuscar.php";
			}

			function respuestaconsulta(pregunta)
			{
				switch(pregunta)
				{
					case "1":	
						document.form2.oculto.value='2';
						document.form2.submit();
						break;
				}
			}

			function guardar()
			{
				var codigo = document.getElementById('codban').value;
                var cliente = document.getElementById('cliente').value;
                var mesesAbonados = document.getElementById('meses_abonados').value;
				var recaudo = document.getElementById('modoRecaudo').value;
				var fecha = document.getElementById('fecha_pago').value;

				if (codigo != '' && cliente != '' && mesesAbonados != '' && recaudo != '-1' && fecha != '') 
                {
					document.form2.calcularValorMeses.value = '1';
					document.form2.buscaValorFactura.value = '1';
					despliegamodalm('visible','4','Esta Seguro de Guardar','1');   
                }
				else 
				{

                    despliegamodalm('visible','2','Falta información para crear el acuerdo.');
                }
			}

			function actualizar()
			{
				document.form2.calcularValorMeses.value = '1';
				document.form2.buscaValorFactura.value = '1';
				document.form2.submit();
			}

            function iratras(scrtop, numpag, limreg, filtro)
			{
				var idcta=document.getElementById('codban').value;

				location.href="serv-recaudoAnticipadoBuscar.php?idcta="+idcta+"&scrtop="+scrtop+"&numpag="+numpag+"&limreg="+limreg+ "&filtro="+filtro;
			}

			function adelante(scrtop, numpag, limreg, filtro)
			{
				var maximo=document.getElementById('maximo').value;
				var actual=document.getElementById('codban').value;
				actual=parseFloat(actual)+1;
				if(actual<=parseFloat(maximo))
				{
					if(actual<10){actual="0"+actual;}
					location.href="serv-recaudoAnticipadoVisualizar.php?idban=" +actual+ "&scrtop=" +scrtop+ "&numpag=" +numpag+ "&limreg=" +limreg+ "&filtro=" +filtro;
				}
			}

			function atrasc(scrtop, numpag, limreg, filtro, prev)
			{
				var minimo=document.getElementById('minimo').value;
				var actual=document.getElementById('codban').value;
				actual=parseFloat(actual)-1;
				if(actual>=parseFloat(minimo))
				{
					if(actual<10){actual="0"+actual;}
					location.href="serv-recaudoAnticipadoVisualizar.php?idban=" +actual+ "&scrtop=" +scrtop+ "&numpag=" +numpag+ "&limreg=" +limreg+ "&filtro=" +filtro;
				}
			}

			function pdf()
			{
				document.form2.action="serv-pdfReciboRecaudoAnticipado.php";
				document.form2.target="_BLANK";
				document.form2.submit(); 
				document.form2.action="";
				document.form2.target="";
			}

			function reversar()
			{
				var estado = document.getElementById('estado').value;
				var anticipoFacturado = document.getElementById('anticipo_facturado').value;

				if(estado == 'S' && anticipoFacturado == '0')
				{
					despliegamodalm('visible','4','Esta Seguro que quiere reversar?','1');   
				}
				else
				{
					despliegamodalm('visible','2','No se puede reversar ya que tiene dinero ya facturado');
				}
			}
		</script>

		<?php titlepag();?>

	</head>

	<body>
		<IFRAME src="alertas.php" name="alertas" id="alertas" style="display:none"></IFRAME>
		<span id="todastablas2"></span>

        <?php 
            $numpag = @$_GET['numpag'];
            $limreg = @$_GET['limreg'];
            $scrtop = 26 * $totreg;
        ?>

		<table>
			<tr><script>barra_imagenes("serv");</script><?php cuadro_titulos();?></tr>

			<tr><?php menu_desplegable("serv");?></tr>

			<tr>
				<td colspan="3" class="cinta">
					<a href="serv-recaudoAnticipado.php" class="mgbt"><img src="imagenes/add.png"/></a>

					<a onclick="guardar()" class="mgbt"><img src="imagenes/guarda.png" title="Guardar"/></a>

					<a href="serv-recaudoAnticipadoBuscar.php" class="mgbt"><img src="imagenes/busca.png" title="Buscar"/></a>

					<a onclick="mypop=window.open('plan-agenda.php','','');mypop.focus()" class="mgbt"><img src="imagenes/agenda1.png" title="Agenda" /></a>

					<a onclick="mypop=window.open('serv-principal.php','','');mypop.focus();" class="mgbt"><img src="imagenes/nv.png" title="Nueva ventana"></a>

					<a onclick="mypop=window.open('<?php echo $url2; ?>','','');mypop.focus();" class="mgbt"><img src="imagenes/duplicar_pantalla.png" title="Duplicar pesta&ntilde;a"></a>

                    <a onClick="pdf()" class="mgbt"><img src="imagenes/print.png" style="width:29px;height:25px;" title="Imprimir" /></a>	

                    <a onClick="iratras(<?php echo "$scrtop, $numpag, $limreg, $filtro"; ?>)" class="mgbt"><img src="imagenes/iratras.png" title="Atr&aacute;s"></a>

					<a onClick="reversar();" class="mgbt"><img src="imagenes/reversado.png" alt="Reversar" title="Reversar"></a>
                </td>
			</tr>
		</table>

		<div id="bgventanamodalm" class="bgventanamodalm">
			<div id="ventanamodalm" class="ventanamodalm">
				<IFRAME src="" name="ventanam" marginWidth=0 marginHeight=0 frameBorder=0 id="ventanam" frameSpacing=0 style=" width:700px; height:130px; top:200; overflow:hidden;"></IFRAME>
			</div>
		</div>

		<form name="form2" method="post" action="">
			<?php 
				if(@$_POST['oculto'] == "")
				{				
                    $_POST['codban'] = $_GET['idban'];

					//consulta de datos cabecera
					$sqlRecaudoAnticipo = "SELECT R.id_cliente, R.valor_mensual, R.meses_abonados, R.valor_total, R.recaudo_en, R.cuenta_banco, R.fecha, R.estado, R.anticipo_facturado, C.cod_usuario FROM srvrecaudo_anticipo AS R INNER JOIN srvclientes AS C ON R.id_cliente = C.id WHERE R.id = '$_POST[codban]'";
					$resRecaudoAnticipo = mysqli_query($linkbd,$sqlRecaudoAnticipo);
					$rowRecaudoAnticipo = mysqli_fetch_assoc($resRecaudoAnticipo);

					$_POST['cliente'] = $rowRecaudoAnticipo['cod_usuario'];
					$_POST['nCliente'] = encuentraNombreTerceroConIdCliente($rowRecaudoAnticipo['id_cliente']);
					$_POST['valorUnitario'] = $rowRecaudoAnticipo['valor_mensual'];
					$_POST['meses_abonados'] = $rowRecaudoAnticipo['meses_abonados'];
					$_POST['valorTotal'] = $rowRecaudoAnticipo['valor_total'];
					$_POST['modoRecaudo'] = $rowRecaudoAnticipo['recaudo_en'];
					$_POST['banco'] = $rowRecaudoAnticipo['cuenta_banco'];
					$_POST['nbanco'] = buscaCuentaEnCuentasnicsp($_POST['banco']);
					$_POST['fecha_pago'] = $rowRecaudoAnticipo['fecha'];
					$_POST['estado'] = $rowRecaudoAnticipo['estado'];
					$_POST['anticipo_facturado'] = $rowRecaudoAnticipo['anticipo_facturado'];
					$_POST['documento'] = encuentraDocumentoTerceroConIdCliente($_POST['cliente']);
					$totalg = number_format($_POST['valorTotal'],2,'.','');
                    $_POST['letras'] = convertirdecimal($totalg,'.');

					//uso del dinero 
					$sqlAnticipoDet = "SELECT * FROM srvrecaudo_anticipo_det WHERE id_anticipo = '$_POST[codban]'";
					$resAnticipoDet = mysqli_query($linkbd,$sqlAnticipoDet);
					while($rowAnticipoDet = mysqli_fetch_assoc($resAnticipoDet))
					{
						$_POST['codigo'][] = $rowAnticipoDet['id'];
						$_POST['fecha'][] = $rowAnticipoDet['fecha'];
						$nombreMovimiento = buscaNombreTipoMovimiento($rowAnticipoDet['tipo_movimiento']);
						$_POST['movimiento'][] = $rowAnticipoDet['tipo_movimiento'].' - '. $nombreMovimiento;
						$_POST['valorAnticipo'][] = $rowAnticipoDet['valor_anticipo'];
					}
				}	
			?>
			<div>
				<table class="inicio ancho">
					<tr>
						<td class="titulos" colspan="6">.: Recaudo por Anticipo</td>

						<td class="cerrar" style="width:7%" onClick="location.href='serv-principal.php'">Cerrar</td>
					</tr>

					<tr>
						<td class="tamano01" style="width:3cm;">C&oacute;digo:</td>

						<td style="width:15%;">
							<input type="text" name="codban" id="codban" onKeyUp="return tabular(event,this)" value="<?php echo @ $_POST['codban'];?>" style="width:100%;height:30px;text-align:center;" readonly/>
						</td>

						<td class="saludo1">Cliente:</td>

						<td style="width: 250px;">
							<input type="text" name='cliente' id='cliente'  value="<?php echo @$_POST['cliente']?>" style="text-align:center; width:100%;" readonly>
						</td>

						<td colspan="2">
							<input type="text" name="nCliente" id="nCliente" value="<?php echo @$_POST['nCliente']?>" style="width:100%;height:30px;" readonly>
						</td>
					</tr>

					<tr>
						<td class="tamano01">Valor Mensual</td>

						<td>
							<input type="text" name="valorUnitario" id="valorUnitario" value="<?php echo $_POST['valorUnitario'] ?>" style="height: 30px; text-align:center; width:100%;" readonly>
						</td>

						<td class="tamano01" style="width: 150px;">Meses a abonar</td>

						<td>
							<input type="text" name="meses_abonados" id="meses_abonados" value="<?php echo $_POST['meses_abonados'] ?>" style="height: 30px; text-align:center; width:100%;" readonly>
						</td>

						<td class="tamano01" style="width: 150px;">Valor Total:</td>

						<td>
							<input type="text" name="valorTotal" id="valorTotal" value="<?php echo $_POST['valorTotal'] ?>" style="height: 30px; text-align:center; width:30%;" readonly>
						</td>
					</tr>

					<tr>
						<td class="tamano01">Recaudo en: </td>

						<td>
							<input type="text" name="modoRecaudo" id="modoRecaudo" value="<?php echo $_POST['modoRecaudo'] ?>" style="width:100%; height:30px; text-align:center;" readonly>
						</td>

						<td class="saludo1">Cuenta Banco: </td> 

						<td>
							<input type="text" id="banco" name="banco" style="width:100%; text-align:center;" onKeyPress="javascript:return solonumeros(event)" onKeyUp="return tabular(event,this)" onBlur="" value="<?php echo $_POST['banco']?>" readonly>

							<input type="hidden" name="cuentaBancaria" id="cuentaBancaria" value="">
						</td>

						<td colspan="2">
							<input type="text" name="nbanco" id="nbanco" style="width:100%;" value="<?php echo $_POST['nbanco']?>"  readonly>
						</td>	
						
					</tr>

					<tr>
						<td class="tamano01" style="width:10%;">Fecha de Pago</td>

						<td style="width:15%;">
							<input type="text" name="fecha_pago" id="fecha_pago" value="<?php echo @ $_POST['fecha_pago']?>" maxlength="10" onKeyUp="return tabular(event,this)"  id="fc_1198971545" onKeyDown="mascara(this,'/',patron,true)" title="DD/MM/YYYY" style="height:30px;text-align:center;width:100%;" readonly/>
						</td>

						<td class="tamano01" style="width: 3cm;">Descripci&oacute;n:</td>

						<td colspan="3">
							<input type="text" name="descripcion" id="descripcion" value="<?php echo $_POST['descripcion'] ?>" style="width: 100%; height: 30px;">
						</td>
					</tr>
				</table>
			</div>

			<div class="subpantalla" style="height:45.5%; width:99.6%; overflow-x:hidden;">
				<table class="inicio">
					<tr>
						<td class="titulos" colspan="12">.: Historial de uso de Anticipo</td>
					</tr>

					<tr class="titulos2">
						<td style="text-align:center;">C&oacute;digo</td>
						<td style="text-align:center;">Fecha</td>
						<td style="text-align:center;">Tipo Movimiento</td>
						<td style="text-align:center;">Valor</td>
					</tr>

					<?php
						for ($x=0;$x< count($_POST['codigo']);$x++)
						{
							echo "
								<tr class='saludo1a'>
									<td style='text-align:center; width:7%;'>".$_POST['codigo'][$x]."</td>

									<td style='text-align:center;'>".$_POST['fecha'][$x]."</td>

									<td style='text-align:center;'>".$_POST['movimiento'][$x]."</td>
								
									<td style='text-align:center;'>".number_format($_POST['valorAnticipo'][$x],2,',','.')."</td>
								</tr>";
						}
					?>
				</table>
			</div>

			<input type="hidden" name="oculto" id="oculto" value="1"/>
			<input type="hidden" name="buscaValorFactura" id="buscaValorFactura" value=""/>
			<input type="hidden" name="calcularValorMeses" id="calcularValorMeses" value=""/>
			<input type="hidden" name="estado" id="estado" value="<?php echo $_POST['estado'] ?>"/>
			<input type="hidden" name="anticipo_facturado" id="anticipo_facturado" value="<?php echo $_POST['anticipo_facturado'] ?>"/>
			<input type="hidden" name="documento" id="documento" value="<?php echo $_POST['documento'] ?>"/>
			<input type="hidden" name="letras" id="letras" value="<?php echo $_POST['letras'] ?>"/>

			<?php 
				if(@$_POST['oculto'] == "2")
				{
                	$codigo_tipo_comprobante = '40';
					$codigo_reversado_tipo_comprobante = '2040';
					$fecha = date("Y/m/d");
					$año = date("Y");
					$consecutivoReversar = siguienteConsecutivoContabilidad($codigo_reversado_tipo_comprobante);

					//cabecera
					$sqlCabecera = "SELECT total_debito FROM comprobante_cab WHERE numerotipo = '$_POST[codban]' AND tipo_comp = '$codigo_tipo_comprobante'";
					$resCabecera = mysqli_query($linkbd,$sqlCabecera);
					$rowCabecera = mysqli_fetch_row($resCabecera);

					$sql = "INSERT INTO comprobante_cab(numerotipo, tipo_comp, fecha, concepto, total_debito, total_credito, estado) VALUES ('$consecutivoReversar', '$codigo_reversado_tipo_comprobante', '$fecha', 'Reversion de pago anticipado del consecutivo $_POST[codban]', '$rowCabecera[0]', '$rowCabecera[0]', '1')";
					mysqli_query($linkbd,$sql);

					//detalles
					$sqlComprobantes = "SELECT * FROM comprobante_det WHERE id_comp = '$codigo_tipo_comprobante $_POST[codban]'";
					$resComprobantes = mysqli_query($linkbd,$sqlComprobantes);
					while($rowComprobantes = mysqli_fetch_assoc($resComprobantes))
					{
						$sql = "INSERT INTO comprobante_det (id_comp, cuenta, tercero, centrocosto, detalle, valdebito, valcredito, estado, vigencia, tipo_comp, numerotipo) VALUES ('$codigo_reversado_tipo_comprobante $consecutivoReversar', '$rowComprobantes[cuenta]', '$rowComprobantes[tercero]', '$rowComprobantes[centrocosto]', 'REVERSION PAGO POR ADELANTO CLIENTE $_POST[tercero]', '$rowComprobantes[valcredito]', '$rowComprobantes[valdebito]', '1', '$año', '$codigo_reversado_tipo_comprobante', '$consecutivoReversar')";
						mysqli_query($linkbd,$sql);
					}

					//tabla comprobantes reversar
					$sqlComprobanteReversar = "INSERT INTO comprobante_rev (tipo_comprobante_origen, num_comprobante_origen, tipo_comprobante_rev, num_comprobante_rev) VALUES ('$codigo_tipo_comprobante', '$_POST[codban]', '$codigo_reversado_tipo_comprobante', '$consecutivoReversar')";
					mysqli_query($linkbd,$sqlComprobanteReversar);

					$sqlRecaudoAnticipo = "UPDATE srvrecaudo_anticipo SET estado = 'N' WHERE id = $_POST[codban]";
					
					if(mysqli_query($linkbd,$sqlRecaudoAnticipo))
					{
						echo"<script>despliegamodalm('visible','1','Se ha realizado la reversion con exito');</script>";
					}
					else
					{
						echo "<script>despliegamodalm('visible','2','No se pudo realizar la reversion');</script>";
					}
				}
			?>
		</form>
		<div id="bgventanamodal2">
			<div id="ventanamodal2">
				<IFRAME  src="" name="buster" marginWidth=0 marginHeight=0 frameBorder=0 id="ventana2" frameSpacing=0 style="left:500px; width:900px; height:500px; top:200;"></IFRAME>
			</div>
		</div>
	</body>
</html>
