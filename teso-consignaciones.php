<?php
require "comun.inc";
require "funciones.inc";
require "conversor.php";
session_start();
$linkbd = conectar_v7();
cargarcodigopag($_GET['codpag'], $_SESSION["nivel"]);
header("Cache-control: private"); // Arregla IE 6
date_default_timezone_set("America/Bogota");
?>
<!DOCTYPE>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="es">

<head>
    <meta http-equiv="Content-Type" content="text/html" charset="iso-8859-1" />
    <meta http-equiv="X-UA-Compatible" content="IE=9" />
    <title>:: IDEAL 10 - Tesorer&iacute;a</title>
    <link href="favicon.ico" rel="shortcut icon" />
    <link href="css/css2.css" rel="stylesheet" type="text/css" />
    <link href="css/css3.css" rel="stylesheet" type="text/css" />
    <link href="css/style.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="css/programas.js"></script>
    <script type='text/javascript' src='JQuery/jquery-2.1.4.min.js'></script>
    <script type="text/javascript" src="JQuery/alphanum/jquery.alphanum.js"></script>
    <script type="text/javascript" src="JQuery/autoNumeric-master/autoNumeric-min.js"></script>
    <script>
        function validar() { document.form2.submit(); }
        function agregardetalle() {
            var validacion00 = document.getElementById('numero').value;
            if (validacion00.trim() != '' && document.form2.valor.value > 0 && document.form2.banco.value != "") {
                document.form2.agregadet.value = 1;
                document.form2.submit();
            }
            else { despliegamodalm('visible', '2', 'Falta informacion para poder agregar'); }
        }
        function eliminar(variable) {
            document.form2.elimina.value = variable;
            despliegamodalm('visible', '4', 'Esta Seguro de eliminar el detalle', '1');
        }
        function guardar() {
            if (document.form2.fecha.value != '') { despliegamodalm('visible', '4', 'Esta Seguro de Guardar', '2'); }
            else {
                despliegamodalm('visible', '2', 'Faltan datos para completar el registro');
                document.form2.fecha.focus();
                document.form2.fecha.select();
            }
        }
        function pdf() {
            document.form2.action = "teso-pdfconsignaciones.php";
            document.form2.target = "_BLANK";
            document.form2.submit();
            document.form2.action = "";
            document.form2.target = "";
        }
        function despliegamodalm(_valor, _tip, mensa, pregunta) {
            document.getElementById("bgventanamodalm").style.visibility = _valor;
            if (_valor == "hidden") { document.getElementById('ventanam').src = ""; }
            else {
                switch (_tip) {
                    case "1":
                        document.getElementById('ventanam').src = "ventana-mensaje1.php?titulos=" + mensa; break;
                    case "2":
                        document.getElementById('ventanam').src = "ventana-mensaje3.php?titulos=" + mensa; break;
                    case "3":
                        document.getElementById('ventanam').src = "ventana-mensaje2.php?titulos=" + mensa; break;
                    case "4":
                        document.getElementById('ventanam').src = "ventana-consulta1.php?titulos=" + mensa + "&idresp=" + pregunta; break;
                }
            }
        }
        function funcionmensaje() { document.location.href = "teso-consignaciones.php"; }
        function respuestaconsulta(pregunta) {
            switch (pregunta) {
                case "1": document.form2.oculto.value = "5";
                    document.form2.submit(); break;
                case "2": document.form2.oculto.value = "2";
                    document.form2.submit(); break;
            }
        }
        jQuery(function ($) { $('#valorvl').autoNumeric('init'); });
    </script>
</head>

<body>
    <IFRAME src="alertas.php" name="alertas" id="alertas" style="display:none"></IFRAME>
    <span id="todastablas2"></span>
    <table>
        <tr>
            <script>barra_imagenes("teso");</script><?php cuadro_titulos(); ?>
        </tr>
        <tr><?php menu_desplegable("teso"); ?></tr>
    </table>
    <div class="bg-white group-btn p-1"><button type="button" onclick="window.location.href='teso-consignaciones.php'"
            class="btn btn-white btn-primary-hover d-flex justify-between align-items-center">
            <span>Nuevo</span>
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 -960 960 960">
                <path d="M440-440H200v-80h240v-240h80v240h240v80H520v240h-80v-240Z"></path>
            </svg>
        </button><button type="button" onclick="guardar();"
            class="btn btn-white btn-primary-hover d-flex justify-between align-items-center">
            <span>Guardar</span>
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 -960 960 960">
                <path
                    d="M840-680v480q0 33-23.5 56.5T760-120H200q-33 0-56.5-23.5T120-200v-560q0-33 23.5-56.5T200-840h480l160 160Zm-80 34L646-760H200v560h560v-446ZM480-240q50 0 85-35t35-85q0-50-35-85t-85-35q-50 0-85 35t-35 85q0 50 35 85t85 35ZM240-560h360v-160H240v160Zm-40-86v446-560 114Z">
                </path>
            </svg>
        </button><button type="button" onclick="window.location.href='teso-buscaconsignaciones.php'"
            class="btn btn-white btn-primary-hover d-flex justify-between align-items-center">
            <span>Buscar</span>
            <svg class="fill-black group-hover:fill-white w-5 h-5" xmlns="http://www.w3.org/2000/svg"
                viewBox="0 -960 960 960">
                <path
                    d="M784-120 532-372q-30 24-69 38t-83 14q-109 0-184.5-75.5T120-580q0-109 75.5-184.5T380-840q109 0 184.5 75.5T640-580q0 44-14 83t-38 69l252 252-56 56ZM380-400q75 0 127.5-52.5T560-580q0-75-52.5-127.5T380-760q-75 0-127.5 52.5T200-580q0 75 52.5 127.5T380-400Z">
                </path>
            </svg>
        </button><button type="button" onclick="mypop=window.open('plan-agenda.php','','');mypop.focus()"
            class="btn btn-white btn-primary-hover d-flex justify-between align-items-center">
            <span class="group-hover:text-white">Agenda</span>
            <svg class="fill-black group-hover:fill-white w-5 h-5" xmlns="http://www.w3.org/2000/svg"
                viewBox="0 -960 960 960">
                <path
                    d="M560-564v-68q33-14 67.5-21t72.5-7q26 0 51 4t49 10v64q-24-9-48.5-13.5T700-600q-38 0-73 9.5T560-564Zm0 220v-68q33-14 67.5-21t72.5-7q26 0 51 4t49 10v64q-24-9-48.5-13.5T700-380q-38 0-73 9t-67 27Zm0-110v-68q33-14 67.5-21t72.5-7q26 0 51 4t49 10v64q-24-9-48.5-13.5T700-490q-38 0-73 9.5T560-454ZM260-320q47 0 91.5 10.5T440-278v-394q-41-24-87-36t-93-12q-36 0-71.5 7T120-692v396q35-12 69.5-18t70.5-6Zm260 42q44-21 88.5-31.5T700-320q36 0 70.5 6t69.5 18v-396q-33-14-68.5-21t-71.5-7q-47 0-93 12t-87 36v394Zm-40 118q-48-38-104-59t-116-21q-42 0-82.5 11T100-198q-21 11-40.5-1T40-234v-482q0-11 5.5-21T62-752q46-24 96-36t102-12q58 0 113.5 15T480-740q51-30 106.5-45T700-800q52 0 102 12t96 36q11 5 16.5 15t5.5 21v482q0 23-19.5 35t-40.5 1q-37-20-77.5-31T700-240q-60 0-116 21t-104 59ZM280-494Z">
                </path>
            </svg>
        </button><button type="button" onclick="mypop=window.open('teso-principal.php','','');mypop.focus();"
            class="btn btn-white btn-primary-hover d-flex justify-between align-items-center">
            <span>Nueva ventana</span>
            <svg class="fill-black group-hover:fill-white w-5 h-5" xmlns="http://www.w3.org/2000/svg"
                viewBox="0 -960 960 960">
                <path
                    d="M320-120v-80h80v-80H160q-33 0-56.5-23.5T80-360v-400q0-33 23.5-56.5T160-840h640q33 0 56.5 23.5T880-760v400q0 33-23.5 56.5T800-280H560v80h80v80H320ZM160-360h640v-400H160v400Zm0 0v-400 400Z">
                </path>
            </svg>
        </button><button type="button"
            onclick="mypop=window.open('/financiero/teso-consignaciones.php','','');mypop.focus();"
            class="btn btn-white btn-primary-hover d-flex justify-between align-items-center">
            <span class="group-hover:text-white">Duplicar pantalla</span>
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 -960 960 960">
                <path
                    d="M640-160v-360H160v360h480Zm80-200v-80h80v-360H320v200h-80v-200q0-33 23.5-56.5T320-880h480q33 0 56.5 23.5T880-800v360q0 33-23.5 56.5T800-360h-80ZM160-80q-33 0-56.5-23.5T80-160v-360q0-33 23.5-56.5T160-600h480q33 0 56.5 23.5T720-520v360q0 33-23.5 56.5T640-80H160Zm400-603ZM400-340Z">
                </path>
            </svg>
        </button><button type="button" onclick="pdf()"
            class="btn btn-danger btn-danger-hover d-flex justify-between align-items-center">
            <span>Exportar PDF</span>
            <svg xmlns="http://www.w3.org/2000/svg"
                viewBox="0 0 512 512"><!-- !Font Awesome Free 6.5.1 by @fontawesome - https://fontawesome.com License - https://fontawesome.com/license/free Copyright 2024 Fonticons, Inc. -->
                <path
                    d="M64 464l48 0 0 48-48 0c-35.3 0-64-28.7-64-64L0 64C0 28.7 28.7 0 64 0L229.5 0c17 0 33.3 6.7 45.3 18.7l90.5 90.5c12 12 18.7 28.3 18.7 45.3L384 304l-48 0 0-144-80 0c-17.7 0-32-14.3-32-32l0-80L64 48c-8.8 0-16 7.2-16 16l0 384c0 8.8 7.2 16 16 16zM176 352l32 0c30.9 0 56 25.1 56 56s-25.1 56-56 56l-16 0 0 32c0 8.8-7.2 16-16 16s-16-7.2-16-16l0-48 0-80c0-8.8 7.2-16 16-16zm32 80c13.3 0 24-10.7 24-24s-10.7-24-24-24l-16 0 0 48 16 0zm96-80l32 0c26.5 0 48 21.5 48 48l0 64c0 26.5-21.5 48-48 48l-32 0c-8.8 0-16-7.2-16-16l0-128c0-8.8 7.2-16 16-16zm32 128c8.8 0 16-7.2 16-16l0-64c0-8.8-7.2-16-16-16l-16 0 0 96 16 0zm80-112c0-8.8 7.2-16 16-16l48 0c8.8 0 16 7.2 16 16s-7.2 16-16 16l-32 0 0 32 32 0c8.8 0 16 7.2 16 16s-7.2 16-16 16l-32 0 0 48c0 8.8-7.2 16-16 16s-16-7.2-16-16l0-64 0-64z">
                </path>
            </svg>
        </button></div>
    <div id="bgventanamodalm" class="bgventanamodalm">
        <div id="ventanamodalm" class="ventanamodalm">
            <IFRAME src="" name="ventanam" marginWidth=0 marginHeight=0 frameBorder=0 id="ventanam" frameSpacing=0
                style=" width:700px; height:130px; top:200; overflow:hidden;">
            </IFRAME>
        </div>
    </div>
    <form name="form2" method="post" action="">
        <?php
        $vigencia = date('Y');
        $vigusu = vigencia_usuarios($_SESSION['cedulausu']);
        //*********** 11050501	CAJA PRINCIPAL esta es la cuenta que va a credito en todas las consignacones
        if (!$_POST['oculto']) {
            $sqlr = "select cuentacaja from tesoparametros";
            $res = mysqli_query($linkbd, $sqlr);
            while ($row = mysqli_fetch_row($res)) {
                $_POST['cuentacaja'] = $row[0];
            }
            $sqlr = "select max(id_consignacion) from tesoconsignaciones_cab";
            $res = mysqli_query($linkbd, $sqlr);
            $consec = 0;
            while ($r = mysqli_fetch_row($res)) {
                $consec = $r[0];
            }
            $consec += 1;
            $_POST['idcomp'] = $consec;
            $fec = date("d/m/Y");
            $_POST['fecha'] = $fec;
            $_POST['valor'] = 0;
        }
        ?>
        <table class="inicio" align="center">
            <tr>
                <td class="titulos" colspan="7">.: Agregar Consignaciones</td>
                <td class="cerrar" style="width:7%;"><a href="teso-principal.php">&nbsp;Cerrar</a></td>
            </tr>
            <tr>
                <td class="saludo1" style="width:4cm;">Numero Comp:</td>
                <td style="width:20%;"><input type="text" name="idcomp" id="idcomp"
                        value="<?php echo $_POST['idcomp'] ?>" readonly style="width:45%;" /></td>
                <td class="saludo1" style="width:3cm;">Fecha:</td>
                <td style="width:30%;"><input type="date" name="fecha" id="fecha" value="<?php echo $_POST['fecha'] ?>"
                        onKeyUp="return tabular(event,this)" title="DD/MM/YYYY" /></td>
                <td rowspan="4"
                    style="background:url(imagenes/invoice.png); background-repeat:no-repeat; background-position:center; background-size: 75% 110%">
                </td>
            </tr>
            <tr>
                <td class="saludo1">Concepto Consignaci&oacute;n:</td>
                <td colspan="3"><input type="text" name="concepto" id="concepto"
                        value="<?php echo $_POST['concepto'] ?>" onKeyUp="return tabular(event,this)"
                        style="width:100%;"></td>

            </tr>
            <tr>
                <td class="saludo1">N� Consignaci&oacute;n:</td>
                <td><input type="text" name="numero" id="numero" value="<?php echo $_POST['numero'] ?>"
                        onKeyUp="return tabular(event,this)" style="width:90%;"></td>
                <td class="saludo1">Cuenta Bancaria:</td>
                <td>
                    <select id="banco" name="banco" onChange="validar()" onKeyUp="return tabular(event,this)"
                        style="width:100%">
                        <option value="">Seleccione....</option>
                        <?php
                        $sqlr = "SELECT TB.estado,TB.cuenta,TB.ncuentaban,TB.tipo,T.razonsocial,TB.tercero FROM tesobancosctas TB,terceros T WHERE TB.tercero=T.cedulanit and TB.estado='S' ";
                        $res = mysqli_query($linkbd, $sqlr);
                        while ($row = mysqli_fetch_row($res)) {
                            echo "";
                            if ($row[1] == $_POST['banco']) {
                                echo "<option value='$row[1]' SELECTED>$row[2] - Cuenta $row[3] - $row[4]</option>";
                                $_POST['nbanco'] = $row[4];
                                $_POST['cb'] = $row[2];
                                $_POST['ct'] = $row[5];
                            } else {
                                echo "<option value='$row[1]'>$row[2] - Cuenta $row[3] - $row[4]</option>";
                            }
                        }
                        ?>
                    </select>
                    <input type="hidden" id="nbanco" name="nbanco" value="<?php echo $_POST['nbanco'] ?>" readonly />
                    <input type="hidden" id="cb" name="cb" value="<?php echo $_POST['cb'] ?>">
                    <input type="hidden" id="ct" name="ct" value="<?php echo $_POST['ct'] ?>">
                </td>
            </tr>
            <tr>
                <td class="saludo1">Centro Costo:</td>
                <td>
                    <select name="cc" onChange="validar()" onKeyUp="return tabular(event,this)" style="width:90%;">
                        <?php
                        $sqlr = "select *from centrocosto where estado='S'";
                        $res = mysqli_query($linkbd, $sqlr);
                        while ($row = mysqli_fetch_row($res)) {
                            if ($row[0] == $_POST['cc']) {
                                echo "<option value='$row[0]' SELECTED>$row[0] - $row[1]</option>";
                            } else {
                                echo "<option value='$row[0]'>$row[0] - $row[1]</option>";
                            }
                        }
                        ?>
                    </select>
                </td>
                <td class="saludo1">Valor:</td>
                <td>
                    <input type="hidden" id="valor" name="valor" value="<?php echo $_POST['valor'] ?>" />
                    <input type="text" id="valorvl" name="valorvl" value="<?php echo $_POST['valorvl'] ?>"
                        data-a-sign="$" data-a-dec="," data-a-sep="." data-v-min='0'
                        onKeyUp="sinpuntitos('valor','valorvl');return tabular(event,this);"
                        style="width:45%;text-align:right;" />&nbsp;
                    <input type="button" name="agregar" id="agregar" value="   Agregar   " onClick="agregardetalle()" />
                </td>
            </tr>
        </table>
        <input type="hidden" name="agregadet" value="0">
        <input type="hidden" name="oculto" id="oculto" value="1">
        <input type="hidden" name="cuentacaja" value="<?php echo $_POST['cuentacaja'] ?>">
        <div class="subpantallac7" style="height:56.7%; width:99.6%; overflow-x:hidden;">
            <table class="inicio">
                <tr>
                    <td colspan="6" class="titulos">Detalle Consignaciones</td>
                </tr>
                <tr>
                    <td class="titulos2" style='width:10%'>CC</td>
                    <td class="titulos2" style='width:15%'>Consignacion</td>
                    <td class="titulos2" style='width:20%'>Cuenta Bancaria</td>
                    <td class="titulos2">Banco</td>
                    <td class="titulos2" style='width:20%'>Valor</td>
                    <td class="titulos2" style='width:5%'><img src="imagenes/del.png"><input type='hidden'
                            name='elimina' id='elimina'></td>
                </tr>
                <?php
                if ($_POST['oculto'] == '5') {
                    $posi = $_POST['elimina'];
                    unset($_POST['dccs'][$posi]);
                    unset($_POST['dconsig'][$posi]);
                    unset($_POST['dbancos'][$posi]);
                    unset($_POST['dnbancos'][$posi]);
                    unset($_POST['dcbs'][$posi]);
                    unset($_POST['dcts'][$posi]);
                    unset($_POST['dvalores'][$posi]);
                    $_POST['dccs'] = array_values($_POST['dccs']);
                    $_POST['dconsig'] = array_values($_POST['dconsig']);
                    $_POST['dbancos'] = array_values($_POST['dbancos']);
                    $_POST['dnbancos'] = array_values($_POST['dnbancos']);
                    $_POST['dcbs'] = array_values($_POST['dcbs']);
                    $_POST['dcts'] = array_values($_POST['dcts']);
                    $_POST['dvalores'] = array_values($_POST['dvalores']);
                    $_POST['oculto'] = '1';
                }
                if ($_POST['agregadet'] == '1') {
                    $_POST['dccs'][] = $_POST['cc'];
                    $_POST['dconsig'][] = $_POST['numero'];
                    $_POST['dbancos'][] = $_POST['banco'];
                    $_POST['dnbancos'][] = $_POST['nbanco'];
                    $_POST['dcbs'][] = $_POST['cb'];
                    $_POST['dcts'][] = $_POST['ct'];
                    $_POST['dvalores'][] = $_POST['valor'];
                    $_POST['agregadet'] = 0;
                    echo "
		 					<script>
								document.form2.banco.value='';
								document.form2.nbanco.value='';
								document.form2.cb.value='';
								document.form2.valor.value='';
								document.form2.valorvl.value='';
								document.form2.numero.value='';
								document.form2.numero.select();
		  						document.form2.numero.focus();
		 					</script>";
                }
                $_POST['totalc'] = 0;
                $iter = 'saludo1a';
                $iter2 = 'saludo2';
                for ($x = 0; $x < count($_POST['dbancos']); $x++) {
                    echo "
							<tr class='$iter'>
								<td><input type='text' name='dccs[]' value='" . $_POST['dccs'][$x] . "' readonly class='inpnovisibles'  style='width:100%'/></td>
								<td><input type='text' name='dconsig[]' value='" . $_POST['dconsig'][$x] . "' class='inpnovisibles' style='width:100%'/></td>
								<td>
									<input type='text' name='dcbs[]' value='" . $_POST['dcbs'][$x] . "'  class='inpnovisibles' style='width:100%'/>
									<input type='hidden' name='dcts[]' value='" . $_POST['dcts'][$x] . "'>
									<input type='hidden' name='dbancos[]' value='" . $_POST['dbancos'][$x] . "'  >
								</td>
								<td><input type='text' name='dnbancos[]' value='" . $_POST['dnbancos'][$x] . "' class='inpnovisibles' style='width:100%'/></td>
								<td style='text-align:right;'><input type='hidden' name='dvalores[]' value='" . $_POST['dvalores'][$x] . "'/>" . number_format($_POST['dvalores'][$x], 2, ',', '.') . "&nbsp;&nbsp;</td>
								<td><a href='#' onclick='eliminar($x)'><img src='imagenes/del.png'></a></td>
							</tr>";
                    $_POST['totalc'] = $_POST['totalc'] + $_POST['dvalores'][$x];
                    $_POST['totalcf'] = number_format($_POST['totalc'], 2, ".", ",");
                    $aux = $iter;
                    $iter = $iter2;
                    $iter2 = $aux;
                }
                $resultado = convertir($_POST['totalc']);
                $_POST['letras'] = $resultado . " PESOS";
                echo "
						<tr class='$iter'>
							<td colspan='3'></td>
							<td style='text-align:right;'>Total:</td>
							<td style='text-align:right;'>" . number_format($_POST['totalc'], 2, ',', '.') . "&nbsp;&nbsp;
								<input type='hidden' name='totalcf' value='$_POST[totalcf]'/>
								<input type='hidden' name='totalc' value='$_POST[totalc]'/>
							</td>
						</tr>
						<tr>
							<td>Son:</td>
							<td colspan='4'><input type='text' name='letras' value='$_POST[letras]' style='width:100%'></td>
						</tr>";
                ?>
            </table>
        </div>
        <?php
        if ($_POST['oculto'] == '2') {
            $fechaf = $_POST['fecha'];
            $bloq = bloqueos($_SESSION['cedulausu'], $fechaf);
            if ($bloq >= 1) {
                //*********************CREACION DEL COMPROBANTE CONTABLE ***************************
                //***busca el consecutivo del comprobante contable
                $consec = 0;
                $sqlr = "select max(id_consignacion) from tesoconsignaciones_cab ";
                $res = mysqli_query($linkbd, $sqlr);
                while ($r = mysqli_fetch_row($res)) {
                    $consec = $r[0];
                }
                $consec += 1;
                //***cabecera comprobante
                $sqlr = "insert into comprobante_cab (numerotipo,tipo_comp,fecha,concepto,total,total_debito,total_credito, diferencia,estado) values ($consec,8,'$fechaf','$_POST[concepto]',0,$_POST[totalc],$_POST[totalc],0,'1')";
                mysqli_query($linkbd, $sqlr);
                $idcomp = mysqli_insert_id($linkbd);
                echo "<input type='hidden' name='ncomp' value='$idcomp'>";
                //******************* DETALLE DEL COMPROBANTE CONTABLE *********************
                for ($x = 0; $x < count($_POST['dbancos']); $x++) {
                    //**** consignacion  BANCARIA*****
                    $sqlr = "insert into comprobante_det (id_comp,cuenta,tercero,centrocosto,detalle,cheque,valdebito,valcredito, estado,vigencia) values ('8 $consec','" . $_POST['dbancos'][$x] . "','" . $_POST['dcts'][$x] . "','" . $_POST['dccs'][$x] . "','Consignacion " . $_POST['dconsig'][$x] . " " . $_POST['dnbancos'][$x] . "',''," . $_POST['dvalores'][$x] . ",0,'1','" . $vigusu . "')";
                    mysqli_query($linkbd, $sqlr);
                    //*** Cuenta CAJA **
                    $sqlr = "insert into comprobante_det (id_comp,cuenta,tercero,centrocosto,detalle,cheque,valdebito,valcredito, estado,vigencia) values ('8 $consec','" . $_POST['cuentacaja'] . "','" . $_POST['dcts'][$x] . "','" . $_POST['dccs'][$x] . "','Consignacion " . $_POST['dconsig'][$x] . " " . $_POST['dnbancos'][$x] . "','',0," . $_POST['dvalores'][$x] . ",'1','" . $vigusu . "')";
                    mysqli_query($linkbd, $sqlr);
                }
                //************ insercion de cabecera consignaciones ************
                $sqlr = "insert into tesoconsignaciones_cab (id_comp,fecha,vigencia,estado,concepto) values($idcomp,'$fechaf'," . $vigusu . ",'S','$_POST[concepto]')";
                mysqli_query($linkbd, $sqlr);
                $idconsig = mysqli_insert_id($linkbd);
                //************** insercion de consignaciones **************
                for ($x = 0; $x < count($_POST['dbancos']); $x++) {
                    $sqlr = "insert into tesoconsignaciones (id_consignacioncab,fecha,ntransaccion,cc,ncuentaban,tercero,tpago, cheque,valor,estado) values($idconsig,'$fechaf','" . $_POST['dconsig'][$x] . "','" . $_POST['dccs'][$x] . "','" . $_POST['dcbs'][$x] . "','" . $_POST['dcts'][$x] . "','E',''," . $_POST['dvalores'][$x] . ",'S')";
                    if (!mysqli_query($linkbd, $sqlr)) {
                        echo "<script>despliegamodalm('visible','2','No se pudo ejecutar la petici�n');</script>";
                    } else {
                        echo "<script>despliegamodalm('visible','1','Se ha almacenado la Consignacion con Exito');</script>";
                    }
                }
            } else {
                echo "<script>despliegamodalm('visible','2',' No Tiene los Permisos para Modificar este Documento');</script>";
            }
            //****fin if bloqueo
        }
        ?>
        <script type="text/javascript">$('#concepto').alphanum({ allow: '_-' });</script>
        <script type="text/javascript">$('#numero').alphanum({ allow: '' });</script>
    </form>
</body>

</html>
