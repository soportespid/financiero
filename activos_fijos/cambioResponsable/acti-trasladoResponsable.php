<?php
    header("Cache-control: no-cache, no-store, must-revalidate");
    header("Content-Type: text/html;charset=utf8");
	require_once '../../comun.inc';
    require '../../funciones.inc';
    session_start();

	$linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");

    $out = array('error' => false);

    $action = "show";

    if(isset($_GET['action'])){
        $action = $_GET['action'];
    }

    if ($action == "datosIniciales") {

        $terceros = [];

        $sqlTercero = "SELECT nombre1, nombre2, apellido1, apellido2, cedulanit, razonsocial, tipodoc FROM terceros WHERE estado = 'S'";
        $resTercero = mysqli_query($linkbd, $sqlTercero);
        while ($rowTerceros = mysqli_fetch_row($resTercero)) {

            $temporal = array();

            if ($rowTerceros[6] == "31") {

                $nombre = $rowTerceros[5];
            }
            else {

                $nombre = $rowTerceros[0] . " " . $rowTerceros[1] . " " . $rowTerceros[2] . " " . $rowTerceros[3];
            }

            array_push($temporal, $rowTerceros[4]);
            array_push($temporal, $nombre);
            array_push($terceros, $temporal);
        }

        $out['terceros'] = $terceros;

        $activos = [];

        $sqlActivos = "SELECT placa, nombre FROM acticrearact_det WHERE estado = 'S'";
        $resActivos = mysqli_query($linkbd, $sqlActivos);
        while ($rowActivos = mysqli_fetch_row($resActivos)) {

            array_push($activos, $rowActivos);
        }

        $out['activos'] = $activos;
    }

    if ($action == "buscaTercero") {

        $placa = $_POST['placa'];
        $historialResponsables = [];

        $sqlTerceroPlaca = "SELECT tercero, estado, fecha_cambio, motivo FROM acticrearact_det_responsable WHERE placa = '$placa' ORDER BY id DESC";
        $resTerceroPlaca = mysqli_query($linkbd, $sqlTerceroPlaca);
        while ($rowTerceroPlaca = mysqli_fetch_row($resTerceroPlaca)) {

            $datosTemp = [];

            if ($rowTerceroPlaca[1] == 'S') {
                $terceroDoc = $rowTerceroPlaca[0];
                $nomTercero = buscatercero($terceroDoc);
            }

            $datosTemp[] = $rowTerceroPlaca[0] . " - " . buscatercero($rowTerceroPlaca[0]);
            $datosTemp[] = $rowTerceroPlaca[2];
            $datosTemp[] = $rowTerceroPlaca[3];
            array_push($historialResponsables, $datosTemp);
        }

        $out['tercero'] = $terceroDoc;
        $out['nomTercero'] = $nomTercero;
        $out['historialResponsables'] = $historialResponsables;
    }

    if ($action == "guardar") {

        $placa = $_POST['placa'];
        $tercero = $_POST['responsable'];
        $motivo = $_POST['motivo'];
        preg_match("/([0-9]{2})\/([0-9]{2})\/([0-9]{4})/", $_POST['fecha'],$f);
		$fecha="$f[3]-$f[2]-$f[1]";
        $usuario = $_SESSION['cedulausu'];

        //paso a estado N
        $sqlUpdate = "UPDATE acticrearact_det_responsable SET estado = 'N' WHERE placa = '$placa'";
        mysqli_query($linkbd, $sqlUpdate);

        $sqlInsert = "INSERT INTO acticrearact_det_responsable (tercero, placa, estado, fecha_cambio, motivo, usuario) VALUES ('$tercero', '$placa', 'S', '$fecha', '$motivo', '$usuario')";
        mysqli_query($linkbd, $sqlInsert);
    }

    header("Content-type: application/json");
    echo json_encode($out);
    die();
