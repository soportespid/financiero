const URL = 'activos_fijos/cambioResponsable/acti-trasladoResponsable.php';
import { filtroEnArrayDeObjetos } from './../../funciones.js';

var app = new Vue({
    el: '#myapp',
	data:{

        error: '',
        loading: false,
        responsable: '',
        nomResponsable: '',
        terceros: [],
        terceroscopy: [],
        searchCedula: '',
        showModalResponsables: false,
        historialResponsables: [],
        showModalActivos: false,
        searchActivo: '',
        activos: [],
        activoscopy: [],
        numeroActivo: '',
        nomActivo: '',
        actualResponsable: '',
        actualNomResponsable: '',
        motivo: '',
    },

    mounted: function(){

        this.datosIniciales();
    },

    methods: 
    {
        //Desglosar array
        toFormData: function(obj){
            var form_data = new FormData();
            for(var key in obj){
                form_data.append(key, obj[key]);
            }
            return form_data;
        },

        formatonumero: function(valor){
            return new Intl.NumberFormat("en-US", {style: "currency", currency: "USD"}).format(valor);
        },

        datosIniciales: async function() {

            this.loading = true;

            await axios.post(URL+'?action=datosIniciales').then((response) => {
                    
                this.terceros = response.data.terceros;
                this.terceroscopy = response.data.terceros;
                this.activos = response.data.activos;
                this.activoscopy = response.data.activos;
            }).finally(() => {
                this.loading =  false;
            });     
        },

        despliegoTerceros: function() {

            this.showModalResponsables = true;
        },

        filtroResponsable: async function() {

            const data = this.terceroscopy;
            var text = this.searchCedula;
            var resultado = [];

            resultado = await filtroEnArrayDeObjetos({'data': data, 'text': text});

            this.terceros = resultado;
        },

        seleccionaResponsable: function(doc, nom) {

            if (doc != "" && nom != "") {

                this.responsable = doc;
                this.nomResponsable = nom;
                this.showModalResponsables = false;
            }
        },

        despliegoModalActivos: function() {

            this.showModalActivos = true;
            this.searchActivo = "";
        },
        
        filtroActivo: async function() {

            const data = this.activoscopy;
            var text = this.searchActivo;
            var resultado = [];

            resultado = await filtroEnArrayDeObjetos({'data': data, 'text': text});

            this.activos = resultado;
        },

        seleccionaActivo: function(activo) {

            if (activo[0] != "" && activo[1] != "") {

                var formData = new FormData();
                formData.append("placa", activo[0]);
                
                axios.post(URL+'?action=buscaTercero', formData).then((response) => {
                    
                    console.log(response.data);
                    this.actualResponsable = response.data.tercero;
                    this.actualNomResponsable = response.data.nomTercero;
                    this.historialResponsables = response.data.historialResponsables;
                });     

                this.numeroActivo = activo[0];
                this.nomActivo = activo[1];
                this.showModalActivos = false;
            }
        },

        generaYguarda: async function () {

            const fecha = document.getElementById('fechaRegistro').value;

            if (this.numeroActivo != "" && fecha != "" && this.responsable != "" && this.motivo != "") {

                var formData = new FormData();
                formData.append("placa", this.numeroActivo);
                formData.append("responsable", this.responsable);
                formData.append("fecha", fecha);
                formData.append("motivo", this.motivo);

                axios.post(URL+'?action=guardar', formData).then((response) => {
                    
                    console.log(response.data);
                });     

                document.form2.action = "acti-pdfTrasladoResponsable.php";
				document.form2.target = "_BLANK";
				document.form2.submit(); 
				document.form2.action = "";
				document.form2.target = "";
                location.reload();
            }
            else {
                Swal.fire({
                    icon: 'warning',
                    title: 'Falta información',
                    text: 'Verificar los datos ingresados que no falte nada.'
                })   
            }
        }
    },
});