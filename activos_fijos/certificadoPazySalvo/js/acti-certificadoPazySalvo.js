const URL = "activos_fijos/certificadoPazySalvo/model/acti-certificadoPazySalvo.php";
const URLPDF = "acti-certificadoPazySalvoPdf.php";

var app = new Vue({
    el:"#myapp",
    data() {
        return {
            isLoading: false,
            consecutivo: 0,
            fecha: '',
            tercero: [],
            dataTerceros: [],
            dataTercerosCopy: [],
            modalTerceros: false,
            txtSearch: '',
            txtResultados: 0,
            dataCertificados: [],
            dataCertificadosCopy: [],
        }
    },
    mounted() {
        this.intPageVal = this.$refs.pageType.value;

        if (this.intPageVal == 1) {
            this.getDataCreate();
        } else if (this.intPageVal == 2) {
            this.getDataSearch();
        } else if (this.intPageVal == 3) {
            
        }
    },
    methods: {
        //Crear
        getDataCreate: async function(){
            const formData = new FormData();
            formData.append("action","getDataCreate");
            this.isLoading = true;
            const response = await fetch(URL,{method:"POST",body:formData});
            const objData = await response.json();
            this.isLoading = false;

            this.consecutivo = objData.consecutivo;
            this.dataTerceros = objData.dataTerceros;
            this.dataTercerosCopy = objData.dataTerceros;
            this.txtResultados = this.dataTercerosCopy.length;
        },

        searchDataTercero: async function(type) {
            
            let search = "";
            if(type == "modal")search = this.txtSearch.toLowerCase();
            if(type == "documento")search = this.tercero.documento;
    
            if (type == "modal") {
                this.dataTercerosCopy = [...this.dataTerceros.filter(e=>e.documento.toLowerCase().includes(search) || e.nombre.toLowerCase().includes(search) )];
                this.txtResultados = this.dataTercerosCopy.length;
            } else if (type == "documento") {
                let data;
                data = this.dataTerceros.filter(e=>e.documento==search)[0];
                this.tercero = data;
            }
        },

        selecTercero: function(tercero) {
            this.tercero = [];
            this.tercero = tercero;
            this.txtSearch = "";
            this.dataTercerosCopy = this.dataTerceros;
            this.txtResultados = this.dataTercerosCopy.length;
            this.modalTerceros = false;
        },

        saveCreate: function() {

            if (this.consecutivo != "" && this.fecha != "" && this.tercero.documento != "" && this.tercero.nombre != "" && this.tercero.terceroId != "") {
                Swal.fire({
                    title:"¿Estás segur@ de guardar?",
                    text:"",
                    icon: 'warning',
                    showCancelButton:true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText:"Sí, guardar",
                    cancelButtonText:"No, cancelar"
                }).then(async function(result){
                    if(result.isConfirmed){
                        const formData = new FormData();
                        formData.append("action","saveCreate");
                        formData.append("consecutivo", app.consecutivo);
                        formData.append("fecha", app.fecha);
                        formData.append("documento", app.tercero.documento);
                        formData.append("terceroId", app.tercero.terceroId);
    
                        const response = await fetch(URL,{method:"POST",body:formData});
                        const objData = await response.json();
    
                        if(objData.status){
                            Swal.fire("Guardado",objData.msg,"success");
                            setTimeout(function(){
                                window.location.href='acti-buscaCertificadosPazySalvo.php';
                            },1000);
                        }else{
                            Swal.fire("Error",objData.msg,"error");
                        }
                    }
                });
            }
            else {
                Swal.fire("Atención!","Todos los campos con (*) son obligatorios","warning");
            }
        },

        //Buscar
        getDataSearch: async function(){
            const formData = new FormData();
            formData.append("action","getDataSearch");
            this.isLoading = true;
            const response = await fetch(URL,{method:"POST",body:formData});
            const objData = await response.json();
            this.isLoading = false;

            this.dataCertificados = objData.data;
            this.dataCertificadosCopy = objData.data;
            this.txtResultados = this.dataCertificadosCopy.length;
        },

        searchData: function() {
            let search = "";
            search = this.txtSearch.toLowerCase();

            this.dataCertificadosCopy = [...this.dataCertificados.filter(e=>e.numCertificado.toLowerCase().includes(search) || e.documento.toLowerCase().includes(search) || e.nombre.toLowerCase().includes(search) )];
            this.txtResultados = this.dataCertificadosCopy.length;
        },

        anulaDocument: function(numCertificado, index) {
            Swal.fire({
                title:"¿Estás segur@ de anular?",
                text:"",
                icon: 'warning',
                showCancelButton:true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText:"Sí, anular",
                cancelButtonText:"No, cancelar"
            }).then(async function(result){
                if(result.isConfirmed){
                    const formData = new FormData();
                    formData.append("action","anulaDocument");
                    formData.append("numCertificado", numCertificado);

                    const response = await fetch(URL,{method:"POST",body:formData});
                    const objData = await response.json();

                    if(objData.status){
                        Swal.fire("Anulado",objData.msg,"success");
                        
                        app.dataCertificados[index].estado = 'N';
                        app.dataCertificadosCopy[index].estado = 'N';
                    }else{
                        Swal.fire("Error",objData.msg,"error");
                    }
                }
            });
        },

        pdf: function (data) {
            const form = document.createElement("form");
            form.method = "post";
            form.target = "_blank";
            form.action = URLPDF;       
            
            function addField(name,value){
                const input = document.createElement("input");
                input.type="hidden";
                input.name=name;
                input.value = value;
                form.appendChild(input);
            }
         
            addField("data", JSON.stringify(data));
            // addField("dataDet", JSON.stringify(this.dataDet));
        
            document.body.appendChild(form);
            form.submit();
            document.body.removeChild(form);
        },
        
        formatFecha: function(fecha) {
            // Separar la fecha en componentes
            const parts = fecha.split('-');
            if (parts.length !== 3) {
                throw new Error('Formato de fecha inválido. Use Y-m-d.');
            }
            const year = parts[0];
            const month = parts[1];
            const day = parts[2];
        
            // Retornar la fecha en el formato deseado
            return `${day}/${month}/${year}`;
        },
    },
    computed:{

    }
})
