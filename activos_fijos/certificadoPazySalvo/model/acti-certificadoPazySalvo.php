<?php
    require_once '../../../Librerias/core/Helpers.php';
    require_once '../../../Librerias/core/Mysql.php';

    header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=utf8");
    session_start();

    class certificado extends Mysql {
        
        public function get_data_create(){
            if(!empty($_SESSION)){       
                $arrResponse = $data = [];
                $consecutivo = searchConsec("acti_certificado_paz", "num_certificado");

                $sql_terceros = "SELECT UPPER(CONCAT(nombre1, ' ', nombre2, ' ', apellido1, ' ', apellido2)) as nombre, UPPER(razonsocial) as razonsocial, cedulanit, id_tercero, estado FROM terceros ORDER BY id_tercero DESC";
                $terceros = $this->select_all($sql_terceros);

                foreach ($terceros as $tercero) {
                    $dataTemp = [];
                    $dataTemp["terceroId"] = $tercero["id_tercero"];
                    $dataTemp["documento"] = $tercero["cedulanit"];
                    
                    if ($tercero["razonsocial"] != "") {
                        $dataTemp["nombre"] = $tercero["razonsocial"];
                    } else {
                        $dataTemp["nombre"] = preg_replace("/\s+/", " ", trim($tercero["nombre"]));
                    }

                    array_push($data, $dataTemp);
                }

                $arrResponse = ["status" => true, "consecutivo" => $consecutivo, "dataTerceros" => $data];
            }
            echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
            die();
        }

        public function save_create() {
            if (!empty($_SESSION)) {
                $arrResponse = [];
                $consecutivo = $_POST["consecutivo"];
                $fecha = $_POST["fecha"];
                $documento = $_POST["documento"];
                $terceroId = $_POST["terceroId"];

                //Valida que no tenga ningún activo a su nombre en estado
                $sql_activos = "SELECT COUNT(*) as activos FROM acticrearact_det_responsable WHERE tercero = '$documento' AND estado = 'S'";
                $activos = $this->select($sql_activos);
    
                if ($activos["activos"] == 0) {

                    $sql_certificado = "INSERT INTO acti_certificado_paz (num_certificado, fecha, tercero_id, estado) VALUES (?, ?, ?, ?)";
                    $value_certificado = [
                        $consecutivo,
                        "$fecha",
                        $terceroId,
                        'S'
                    ];

                    $this->insert($sql_certificado, $value_certificado);

                    $arrResponse = ["status" => true, "msg" => "Certificado guardado con exito"];
                }
                else {
                    $arrResponse = ["status" => false, "msg" => "La persona seleccionada aún tiene activos a su nombre"];
                }
            }

            echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
            die();
        }

        public function get_data_search() {
            if (!empty($_SESSION)) {
                $arrResponse = [];

                $sql_certificados = "SELECT 
                C.num_certificado as numCertificado,
                C.fecha,
                C.estado,
                UPPER(CONCAT(T.nombre1, ' ', T.nombre2, ' ', T.apellido1, ' ', T.apellido2)) as nombre,
                T.cedulanit as documento
                FROM acti_certificado_paz as C INNER JOIN terceros as T ON C.tercero_id = T.id_tercero
                ORDER BY C.num_certificado DESC";
                $certificados = $this->select_all($sql_certificados);

                foreach ($certificados as $index => $certificado) {
                    $certificados[$index]["nombre"] = preg_replace("/\s+/", " ", trim($certificado["nombre"]));
                }

                $arrResponse = ["status" => true, "data" => $certificados];
            }

            echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
            die();
        }

        public function anula_document() {
            if (!empty($_SESSION)) {
                $numCertificado = $_POST["numCertificado"];

                $sql_certificado = "UPDATE acti_certificado_paz SET estado = ? WHERE num_certificado = $numCertificado";
                $this->update($sql_certificado, ["N"]);

                $arrResponse = ["status" => true, "msg" => "Certificado No. $numCertificado, anulado con exito"];
            }

            echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
            die();
        }
    }

    if($_POST){
        $obj = new certificado();

        switch ($_POST["action"]) {
            case 'getDataCreate':
                $obj->get_data_create();
                break;
            
            case 'saveCreate':
                $obj->save_create();
                break;

            case 'getDataSearch':
                $obj->get_data_search();
                break;

            case 'anulaDocument':
                $obj->anula_document();
                break;

            default:
                $arrResponse = ["status" => false, "msg" => "Action no encontrada"];
                echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
                break;
        }
    }

    header("Content-type: application/json");
    die();