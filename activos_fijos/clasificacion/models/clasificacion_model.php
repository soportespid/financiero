<?php
    require_once '../../../Librerias/core/Helpers.php';
    require_once '../../../Librerias/core/Mysql.php';
    session_start();
    
    class clasificacionModel extends Mysql {
        public function clases() {
            $sql = "SELECT codigo, nombre FROM actipo WHERE tipo = 1 AND estado = 'S'";
            $data = $this->select_all($sql);
            return $data;
        }

        public function grupos() {
            $sql = "SELECT codigo, nombre, niveluno FROM actipo WHERE tipo = 2 AND estado = 'S'";
            $data = $this->select_all($sql);
            return $data;
        }

        public function tipos() {
            $sql = "SELECT codigo, nombre, niveluno, niveldos, deprecia, salv_rate FROM actipo WHERE tipo = 3 AND estado = 'S'";
            $data = $this->select_all($sql);
            return $data;
        }

        public function validaCodClase($codigo) {
            $sql = "SELECT COUNT(*) AS valor FROM actipo WHERE codigo = '$codigo' AND tipo = 1 AND estado = 'S'";
            $data = $this->select($sql);
            return $data["valor"];
        }

        public function validaCodGrupo($clase, $codigo) {
            $sql = "SELECT COUNT(*) AS valor FROM actipo WHERE codigo = '$codigo' AND tipo = 2 AND niveluno = '$clase' AND estado = 'S'";
            $data = $this->select($sql);
            return $data["valor"];
        }

        public function validaCodTipo($clase, $grupo, $codigo) {
            $sql = "SELECT COUNT(*) AS valor FROM actipo WHERE codigo = '$codigo' AND tipo = 3 AND niveluno = '$grupo' AND niveldos = '$clase' AND estado = 'S'";
            $data = $this->select($sql);
            return $data["valor"];
        }

        public function insertClase(string $codigo, string $nombre) {
            $sql = "INSERT INTO actipo (codigo, nombre, tipo, estado) VALUES (?, ?, ?, ?)";
            $value = [$codigo, strtoupper(strClean($nombre)), 1, 'S'];
            $respuesta = $this->insert($sql, $value);
            return $respuesta;
        }

        public function insertGrupo(string $codigo, string $nombre, $nivelUno) {
            $sql = "INSERT INTO actipo (codigo, nombre, tipo, niveluno, estado) VALUES (?, ?, ?, ?, ?)";
            $value = [$codigo, strtoupper(strClean($nombre)), 2, $nivelUno, 'S'];
            $respuesta = $this->insert($sql, $value);
            return $respuesta;
        }

        public function insertTipo(array $datos) {
            $sql = "INSERT INTO actipo (codigo, nombre, tipo, niveluno, niveldos, estado, deprecia, salv_rate) VALUES (?, ?, ?, ?, ?, ?, ?, ?)";
            $values = [
                $datos["tipoId"],
                strtoupper(strClean($datos["nameTipo"])),
                3,
                $datos["grupoId"],
                $datos["claseId"],
                'S',
                $datos["meses"],
                $datos["salvamento"]
            ];

            $resp = $this->insert($sql, $values);
            return $resp;
        }

        public function insertValue(array $data) {
            if ($data["type"] == "depreciacion") {
                $sql = "UPDATE actipo SET deprecia = ? WHERE tipo = 3 AND codigo = $data[codigo] AND niveluno = $data[nivelUno] AND niveldos = $data[nivelDos]";
            } else {
                $sql = "UPDATE actipo SET salv_rate = ? WHERE tipo = 3 AND codigo = $data[codigo] AND niveluno = $data[nivelUno] AND niveldos = $data[nivelDos]";
            }

            $resp = $this->update($sql, [$data["valor"]]);
            return $resp;
        }
    }
?>