const URL = "activos_fijos/clasificacion/controllers/clasificacion_controller.php";

var app = new Vue({
    el:"#myapp",
    data() {
        return {
            tipoCreacion: 3,
            isLoading: false,
            tabClases: true,
            tabGrupos: false,
            tabTipos: false,
            txtSearch: '',
            txtResultados: 0,
            clases: [],
            grupos: [],
            tipos: [],
            data: {
                claseId: '0',
                grupoId: '0',
                tipoId: '',
                nameTipo: '',
                meses: 0,
                salvamento: 0,
            },
            codClase: '',
            codGrupo: '',
        }
    },
    mounted() {
        this.intPageVal = this.$refs.pageType.value;

        if (this.intPageVal == 1) {
            this.getData();
        } else {
            this.getSearch();
        }
    },
    methods: {
        async getData() {
            const formData = new FormData();
            formData.append("action","get");
            const response = await fetch(URL,{method:"POST",body:formData});
            const objData = await response.json();
            this.clases = objData.clases;
            this.grupos = objData.grupos;
        },

        async getSearch() {
            const formData = new FormData();
            formData.append("action","get_search");
            const response = await fetch(URL,{method:"POST",body:formData});
            const objData = await response.json();
            this.clases = objData.clases;
            this.grupos = objData.grupos;
            this.tipos = objData.tipos;
        },

        async save() {
            if (this.tipoCreacion == 1) {
                if (this.data.tipoId == '0' || this.data.tipoId == "" || this.nameTipo == ""){
                    Swal.fire("Atención!","Todos los campos con * son obligatorios","warning");
                    return false;
                }
            }

            if (this.tipoCreacion == 2) {
                if (this.data.claseId == '0' || this.data.tipoId == '00' || this.data.tipoId == "" || this.nameTipo == ""){
                    Swal.fire("Atención!","Todos los campos con * son obligatorios","warning");
                    return false;
                }
            }
            if (this.tipoCreacion == 3) {
                if (this.data.claseId == '0' || this.data.grupoId == '0' || this.data.tipoId == '000' || this.data.tipoId == "" || this.nameTipo == ""){
                    Swal.fire("Atención!","Todos los campos con * son obligatorios","warning");
                    return false;
                }
    
                if (this.data.meses < 0 || this.data.salvamento < 0) {
                    Swal.fire("Atención!","Los meses depreciados y el salvamento no pueden ser menor a 0","warning");
                    return false;
                }
            }

            const formData = new FormData();
            formData.append("action","save");
            formData.append("tipoCreate",this.tipoCreacion);
            formData.append("data", JSON.stringify(this.data));

            Swal.fire({
                title:"¿Estás segur@ de guardar?",
                text:"",
                icon: 'warning',
                showCancelButton:true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText:"Sí, guardar",
                cancelButtonText:"No, cancelar"
            }).then(async function(result) {
                if(result.isConfirmed) {
                    const response = await fetch(URL,{method:"POST",body:formData});
                    const objData = await response.json();
                    if (objData.status) {
                        Swal.fire("Guardado",objData.msg,"success");
                        setTimeout(function(){
                            window.location.reload();
                        },1000);
                    }
                    else {
                        Swal.fire("Error!",objData.msg,"warning");
                    }
                }
            });
        },

        selectClase(item) {
            this.codGrupo = "";
            this.codClase = item.codigo;
            this.tabClases = false;
            this.tabGrupos = true;
        },

        selectGrupo(item) {
            if (this.codClase != "") {
                this.codGrupo = item.codigo;
                this.tabGrupos = false;
                this.tabTipos = true;
            }
            else {
                Swal.fire("Atención!","Primero selecciona una clase","warning");
            }
        },

        tabs(type) {
            if (type == "clase"){
                this.tabGrupos = this.tabTipos = false;
                this.tabClases = true;
            } else if (type == "grupo") {
                this.tabClases = this.tabTipos = false;
                this.tabGrupos = true;
            } else {
                this.tabClases = this.tabGrupos = false;
                this.tabTipos = true;
            }
        },

        saveEdit(data, index, type) {
            Swal.fire({
                title: 'Introduce un valor',
                input: 'number',
                inputPlaceholder: 'Escribe algo...',
                showCancelButton: true,
                confirmButtonText: 'Enviar',
                cancelButtonText: 'Cancelar'
            }).then(async function(result) {
                if (result.isConfirmed) {
                    const datoIngresado = result.value;

                    if ((datoIngresado < 0 || datoIngresado > 100) && type == 'salvamento') {
                        Swal.fire("Atención!","El porcentaje del salvamento no puede ser menor a 0 ni mayor a 100","warning");
                        return false;
                    }

                    if (datoIngresado < 0  && type == 'depreciacion') {
                        Swal.fire("Atención!","El valor de la depreciación no puede ser menor a 0","warning");
                        return false;
                    }

                    const formData = new FormData();
                    formData.append("action","save_edit");
                    formData.append("codigo", data.codigo);
                    formData.append("nivelUno", data.niveluno);
                    formData.append("nivelDos", data.niveldos);
                    formData.append("type", type);
                    formData.append("valor", datoIngresado);

                    const response = await fetch(URL,{method:"POST",body:formData});
                    const objData = await response.json();

                    if (objData.status) {
                        Swal.fire("Guardado",objData.msg,"success");
                        if (type == "depreciacion") {
                            app.tipos[index].deprecia = datoIngresado;
                        } else {
                            app.tipos[index].salv_rate = datoIngresado;
                        }
                    } else {
                        Swal.fire("Error!",objData.msg,"warning");
                    }
                } 
            });
        }
    },
    computed:{

    }
})
