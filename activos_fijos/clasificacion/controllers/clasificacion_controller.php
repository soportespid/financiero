<?php
    require_once '../../../Librerias/core/Helpers.php';
    require_once '../models/clasificacion_model.php';
    session_start();
    header('Content-Type: application/json');

    class clasificacionController extends clasificacionModel {

        public function get() {
            if (!empty($_SESSION)) {
                $arrResponse = array("clases" => $this->clases(), "grupos" => $this->grupos());
                echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
            }
        }

        public function get_search() {
            if (!empty($_SESSION)) {
                $arrResponse = array("clases" => $this->clases(), "grupos" => $this->grupos(), "tipos" => $this->tipos());
                echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
            }
        }


        public function save() {
            if (!empty($_SESSION)) {
                if ($_POST["data"]) {
                    $tipoCreate = $_POST["tipoCreate"];
                    $data = json_decode($_POST['data'],true);    

                    if ($tipoCreate == 1) {
                        if (is_numeric("$data[tipoId]") && strlen($data["tipoId"]) == 1) {
                            if ($this->validaCodClase($data["tipoId"]) == 0) {
                                $resp = $this->insertClase($data["tipoId"], $data["nameTipo"]);
                                if ($resp >= 0) {
                                    $arrResponse = array("status" => true, "msg" => "Guardado con exito");    
                                }
                                else {
                                    $arrResponse = array("status" => false, "msg" => "Error en guardado contacte con soporte");    
                                }
                            }
                            else {
                                $arrResponse = array("status" => false, "msg" => "El código de clase de activo ya existe, elige otro código");
                            }
                        }
                        else {
                            $arrResponse = array("status" => false, "msg" => "El código de clase deben ser numérica y debe tener 1 digito");    
                        }
                    }
                    else if ($tipoCreate == 2) {
                        if (is_numeric("$data[tipoId]") && strlen($data["tipoId"]) == 2) {
                            if ($this->validaCodGrupo($data["claseId"], $data["tipoId"]) == 0) {
                                $resp = $this->insertGrupo($data["tipoId"], $data["nameTipo"], $data["claseId"]);
                                if ($resp >= 0) {
                                    $arrResponse = array("status" => true, "msg" => "Guardado con exito");    
                                }
                                else {
                                    $arrResponse = array("status" => false, "msg" => "Error en guardado contacte con soporte");    
                                }
                            }
                            else {
                                $arrResponse = array("status" => false, "msg" => "El código de grupo de activo ya existe, elige otro código");
                            }
                        }
                        else {
                            $arrResponse = array("status" => false, "msg" => "El código de grupo deben ser numérica y debe tener 2 digitos");    
                        }
                    }
                    else {
                        if (is_numeric("$data[tipoId]") && strlen($data["tipoId"]) == 3) {
                            if ($this->validaCodTipo($data["claseId"], $data["grupoId"], $data["tipoId"]) == 0) {
                                $resp = $this->insertTipo($data);
                                if ($resp >= 0) {
                                    $arrResponse = array("status" => true, "msg" => "Guardado con exito");    
                                }
                                else {
                                    $arrResponse = array("status" => false, "msg" => "Error en guardado contacte con soporte");    
                                }
                            }
                            else {
                                $arrResponse = array("status" => false, "msg" => "El código de tipo de activo ya existe, elige otro código");
                            }
                        }
                        else {
                            $arrResponse = array("status" => false, "msg" => "El código de tipo deben ser numérica y debe tener 3 digitos");    
                        }
                    }                    
                }
                
                echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
            }

            die();
        }

        public function save_edit() {
            if (!empty($_SESSION)) {
                if ($_POST) {

                    $data = [
                        "codigo" => $_POST["codigo"],
                        "nivelUno" => $_POST["nivelUno"],
                        "nivelDos" => $_POST["nivelDos"],
                        "type" => $_POST["type"],
                        "valor" => $_POST["valor"]
                    ];

                    $resp = $this->insertValue($data);

                    if ($resp >= 0) {
                        $arrResponse = array("status" => true, "msg" => "Valor editado con exito"); 
                    }
                    else {
                        $arrResponse = array("status" => false, "msg" => "Error en guardado, intente nuevamente");
                    }
                }
                echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
            }

            die();
        }
    }

    if($_POST){
        $obj = new clasificacionController();

        switch ($_POST["action"]) {
            case 'get':
                $obj->get();
                break;

            case 'get_search':
                $obj->get_search();
                break;

            case 'save':
                $obj->save();
                break;
        
            case 'save_edit':
                $obj->save_edit();
                break;
            default:
                $arrResponse = ["status" => false, "msg" => "Acción no encontrada"];
                echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
                break;
        }
    }
?>