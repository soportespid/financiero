const URL = "activos_fijos/ubicacion/model/acti-ubicacion.php";

var app = new Vue({
    el:"#myapp",
    data() {
        return {
            isLoading:false,
            datos: [],
            consecutivo: 0,
            nombre: '',
            intPageVal: 0,
            ubicaciones: [],
            ubicaciones_copy: [],
            txtSearch: '',
            txtResultados: 0,
            id: 0,
        }
    },
    mounted() {
        this.intPageVal = this.$refs.pageType.value;

        if (this.intPageVal == 1) {
            this.getCreateData();
        } else if (this.intPageVal == 2) {
            this.getSearchData();
        } else if (this.intPageVal == 3) {
            this.getDataEdit();
        }
    },
    methods: {
        getCreateData: async function(){
            const formData = new FormData();
            formData.append("action","getCreateData");
            const response = await fetch(URL,{method:"POST",body:formData});
            const objData = await response.json();

            this.consecutivo = objData;
        },

        getSearchData: async function(){
            const formData = new FormData();
            formData.append("action","getSearchData");
            const response = await fetch(URL,{method:"POST",body:formData});
            const objData = await response.json();

            this.ubicaciones = objData.ubicaciones;
            this.ubicaciones_copy = objData.ubicaciones;
            this.txtResultados = this.ubicaciones_copy.length;
        },

        getDataEdit: async function() {
            this.id = new URLSearchParams(window.location.search).get('id');

            const formData = new FormData();
            formData.append("action","getDataEdit");
            formData.append("consecutivo",this.id);
            const response = await fetch(URL,{method:"POST",body:formData});
            const objData = await response.json();

            this.consecutivo = objData.data.id_cc;
            this.nombre = objData.data.nombre;
        },

        searchData: async function() {
            let search = "";
            search = this.txtSearch.toLowerCase();
            
            this.ubicaciones_copy = [...this.ubicaciones.filter(e=>e.consecutivo.toLowerCase().includes(search) || e.nombre.toLowerCase().includes(search))];
            this.txtResultados = this.ubicaciones_copy.length;
        },

        changeStatus: async function(data, index) {

            Swal.fire({
                title:"¿Estás segur@ de actualizar estado?",
                text:"",
                icon: 'warning',
                showCancelButton:true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText:"Sí, actualizar",
                cancelButtonText:"No, cancelar"
            }).then(async function(result){
                if(result.isConfirmed){
                    const formData = new FormData();
                    formData.append("action","updateData");
                    formData.append("consecutivo",data.consecutivo);
                    formData.append("estado",data.estado == 'S' ? 'N' : 'S');
                    const response = await fetch(URL,{method:"POST",body:formData});
                    const objData = await response.json();
                    
                    if (objData.status) {
                        let estado = data.estado == "S" ? "N" : "S";
                        let status = data.estado == "S" ? 0 : 1;
                        app.ubicaciones[index].estado = estado;
                        app.ubicaciones_copy[index].estado = estado;
                        app.ubicaciones[index].is_status = status;
                        app.ubicaciones_copy[index].is_status = status;
                    }
                    else {
                        Swal.fire("Error",objData.msg,"error");
                    }
                }
                else {
                    app.searchData();
                }
            });

        },

        saveCreate: async function() {

            if (this.consecutivo != "" && this.nombre != "") {
                Swal.fire({
                    title:"¿Estás segur@ de guardar?",
                    text:"",
                    icon: 'warning',
                    showCancelButton:true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText:"Sí, guardar",
                    cancelButtonText:"No, cancelar"
                }).then(async function(result){
                    if(result.isConfirmed){
                        const formData = new FormData();
                        formData.append("action","saveCreate");
                        formData.append("consecutivo",app.consecutivo);
                        formData.append("nombre",app.nombre);
    
                        app.isLoading = true;
                        const response = await fetch(URL,{method:"POST",body:formData});
                        const objData = await response.json();
                        app.isLoading = false;
    
                        if(objData.status){
                            Swal.fire("Guardado",objData.msg,"success");
                            setTimeout(function(){
                                location.reload();
                            },1000);
                        }else{
                            Swal.fire("Error",objData.msg,"error");
                        }
                    }
                });
            }
            else {
                Swal.fire("Atención!","Todos los campos con (*) son obligatorios.","warning");
            }
        },

        saveEdit: async function() {
            if (this.consecutivo != "" && this.nombre != "") {
                Swal.fire({
                    title:"¿Estás segur@ de guardar?",
                    text:"",
                    icon: 'warning',
                    showCancelButton:true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText:"Sí, guardar",
                    cancelButtonText:"No, cancelar"
                }).then(async function(result){
                    if(result.isConfirmed){
                        const formData = new FormData();
                        formData.append("action","saveEdit");
                        formData.append("consecutivo",app.consecutivo);
                        formData.append("nombre",app.nombre);
    
                        app.isLoading = true;
                        const response = await fetch(URL,{method:"POST",body:formData});
                        const objData = await response.json();
                        app.isLoading = false;
    
                        if(objData.status){
                            Swal.fire("Guardado",objData.msg,"success");
                        }else{
                            Swal.fire("Error",objData.msg,"error");
                        }
                    }
                });
            }
            else {
                Swal.fire("Atención!","Todos los campos con (*) son obligatorios.","warning");
            }
        },
    },
    computed:{

    }
})
