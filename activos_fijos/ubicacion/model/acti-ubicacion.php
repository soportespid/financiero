<?php
    require_once '../../../Librerias/core/Helpers.php';
    require_once '../../../Librerias/core/Mysql.php';

    header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=utf8");
    session_start();

    class ubicaciones extends Mysql {
        
        public function get_create_data(){
            if(!empty($_SESSION)){       
                
                $consecutivo = searchConsec("actiubicacion", "id_cc");

                if ($consecutivo <= 9) {
                    $consecutivo = "0".$consecutivo;
                }

                echo json_encode($consecutivo,JSON_UNESCAPED_UNICODE);
            }
            die();
        }

        public function save_create() {
            if (!empty($_SESSION)) {
                $consecutivo = $_POST["consecutivo"];
                $name = replaceChar(strClean($_POST["nombre"]));

                $sql = "INSERT INTO actiubicacion (id_cc, nombre, estado) VALUES (?, ?, ?)";
                $this->insert($sql, [$consecutivo, $name, "S"]);

                $arrResponse = array("status"=>true,"msg"=>"Guardado exitoso");
            }
            else {
                $arrResponse = array("status"=>false,"msg"=>"Sesión cerrada");
            }

            echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
            die();
        }

        public function get_search_data() {
            if (!empty($_SESSION)) {
                $sql_ubicaciones = "SELECT id_cc as consecutivo, nombre, estado FROM actiubicacion ORDER BY id_cc DESC";
                $ubicaciones = $this->select_all($sql_ubicaciones);

                foreach ($ubicaciones as $key => $ubicacion) {
                    $ubicaciones[$key]['is_status'] = $ubicacion['estado'] == "S" ? 1 : 0;
                }

                $arrResponse = array("status"=>true,"ubicaciones"=>$ubicaciones);
            }

            echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
            die();
        }

        public function update_data() {
            if (!empty($_SESSION)) {
                $consecutivo = $_POST["consecutivo"];
                $estado = $_POST["estado"];

                $sql_update = "UPDATE actiubicacion SET estado = ? WHERE id_cc = '$consecutivo'";

                $this->update($sql_update, [$estado]);
                $arrResponse = array("status"=>true);
            }
            echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
            die();
        }

        public function get_data_edit() {
            if (!empty($_SESSION)) {
                $consecutivo = $_POST["consecutivo"];
                $sql = "SELECT id_cc, nombre FROM actiubicacion WHERE id_cc = '$consecutivo'";
                $data = $this->select($sql);
                
                $arrResponse = array("status"=>true, "data"=>$data);
            }
            echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
            die();
        }

        public function save_edit() {
            if (!empty($_SESSION)) {
                $consecutivo = $_POST["consecutivo"];
                $name = replaceChar(strClean($_POST["nombre"]));

                $sql = "UPDATE actiubicacion SET nombre = ? WHERE id_cc = '$consecutivo'";
                $this->update($sql, [$name]);

                $arrResponse = array("status"=>true,"msg"=>"Guardado exitoso");
            }
            else {
                $arrResponse = array("status"=>false,"msg"=>"Sesión cerrada");
            }

            echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
            die();
        }
    }

    if($_POST){
        $obj = new ubicaciones();

        if($_POST['action'] == "getCreateData"){
            $obj->get_create_data();
        } else if ($_POST["action"] == "saveCreate") {
            $obj->save_create();
        } else if ($_POST["action"] == "getSearchData") {
            $obj->get_search_data();
        } else if ($_POST["action"] == "updateData") {
            $obj->update_data();
        } else if ($_POST["action"] == "getDataEdit") {
            $obj->get_data_edit();
        } else if ($_POST["action"] == "saveEdit") {
            $obj->save_edit();
        }
    }

    header("Content-type: application/json");
    die();