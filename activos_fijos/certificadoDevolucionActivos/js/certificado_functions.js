const URL = "activos_fijos/certificadoDevolucionActivos/controller/certificado_controller.php";
const URLPDF = "acti-certificadoDevolucionActivosPDF.php";

var app = new Vue({
    el:"#myapp",
    data() {
        return {
            isLoading: false,
            consecutivo: 0,
            fecha: '',
            tercero: [],
            dataTerceros: [],
            dataTercerosCopy: [],
            modalTerceros: false,
            txtSearch: '',
            txtResultados: 0,
            activos: [],
            observaciones: '',
            dataCertificados: [],
            dataCertificadosCopy: [],
        }
    },
    mounted() {
        this.intPageVal = this.$refs.pageType.value;

        if (this.intPageVal == 1) {
            this.getDataCreate();
        } else if (this.intPageVal == 2) {
            this.getDataSearch();
        } else if (this.intPageVal == 3) {
            
        }
    },
    methods: {
        //Carga datos iniciales
        getDataCreate: async function(){
            const formData = new FormData();
            formData.append("action","getDataCreate");
            this.isLoading = true;
            const response = await fetch(URL,{method:"POST",body:formData});
            const objData = await response.json();
            this.isLoading = false;

            this.consecutivo = objData.consecutivo;
            this.dataTerceros = objData.terceros;
            this.dataTercerosCopy = objData.terceros;
            this.txtResultados = this.dataTercerosCopy.length;
        },

        getDataSearch: async function() {
            const formData = new FormData();
            formData.append("action","getDataSearch");
            this.isLoading = true;
            const response = await fetch(URL,{method:"POST",body:formData});
            const objData = await response.json();
            this.isLoading = false;

            this.dataCertificados = objData.certificados;
            this.dataCertificadosCopy = objData.certificados;
            this.txtResultados = this.dataCertificadosCopy.length;
        },

        //Filtros de busqueda
        searchDataTercero: function() {
            let search = "";
            search = this.txtSearch.toLowerCase();
            this.dataTercerosCopy = [...this.dataTerceros.filter(e=>e.documento.toLowerCase().includes(search) || e.nombre.toLowerCase().includes(search) )];
            this.txtResultados = this.dataTercerosCopy.length;
        },

        searchDataCertificados: function() {
            let search = "";
            search = this.txtSearch.toLowerCase();
            this.dataCertificadosCopy = [...this.dataCertificados.filter(e=>e.documento.toLowerCase().includes(search) || e.nombre.toLowerCase().includes(search) )];
            this.txtResultados = this.dataCertificadosCopy.length;
        },

        //Selecciona datos de modal
        selectTercero: async function(dataTercero) {
            const formData = new FormData();
            formData.append("action","searchActives");
            formData.append("documento", dataTercero.documento);
            const response = await fetch(URL,{method:"POST",body:formData});
            const objData = await response.json();

            this.tercero = dataTercero;
            this.activos = objData.activos;
            this.txtSearch = "";
            this.dataTercerosCopy = this.dataTerceros;
            this.txtResultados = this.dataTercerosCopy.length;
            this.modalTerceros = false;
        },

        //Cambia status en array
        cambioCheck: function(index) {
            if (this.activos[index].check == false) {
                this.activos[index].check = true;
            } else if (this.activos[index].check == true) {
                this.activos[index].check = false;
            }
        },

        //anula documento
        anulaCertificado: function(numCertificado, index) {
            Swal.fire({
                title:"¿Estás segur@ de anular?",
                text:"",
                icon: 'warning',
                showCancelButton:true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText:"Sí, anular",
                cancelButtonText:"No, cancelar"
            }).then(async function(result){
                if(result.isConfirmed){
                    const formData = new FormData();
                    formData.append("action","anulaCertificado");
                    formData.append("numCertificado", numCertificado);

                    const response = await fetch(URL,{method:"POST",body:formData});
                    const objData = await response.json();

                    if(objData.status){
                        app.dataCertificados[index].estado = 'N';
                        app.dataCertificadosCopy[index].estado = 'N';
                        Swal.fire("Anulado",objData.msg,"success");
                    }else{
                        Swal.fire("Error",objData.msg,"error");
                    }
                }
            });
        },

        //guardados
        saveCreate: function() {
            if (this.consecutivo > 0 && this.fecha != "" && this.tercero.terceroId != "" && this.observaciones != "" && this.activos.length > 0) {
                Swal.fire({
                    title:"¿Estás segur@ de guardar?",
                    text:"",
                    icon: 'warning',
                    showCancelButton:true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText:"Sí, guardar",
                    cancelButtonText:"No, cancelar"
                }).then(async function(result){
                    if(result.isConfirmed){
                        const formData = new FormData();
                        formData.append("action","saveCreate");
                        formData.append("consecutivo", app.consecutivo);
                        formData.append("fecha", app.fecha);
                        formData.append("terceroId", app.tercero.terceroId);
                        formData.append("observacion", app.observaciones);
                        formData.append("activos", JSON.stringify(app.activos));
    
                        app.isLoading = true;
                        const response = await fetch(URL,{method:"POST",body:formData});
                        const objData = await response.json();
                        app.isLoading = false;
    
                        if(objData.status){
                            Swal.fire("Guardado",objData.msg,"success");
                            setTimeout(function(){
                                window.location.reload();
                            },1000);
                        }else{
                            Swal.fire("Error",objData.msg,"error");
                        }
                    }
                });
            }
            else {
                Swal.fire("Atención!","Todos los campos con (*) son obligatorios","warning");
            }
        },

        //busca activos agregado y genera pdf
        pdf: async function (data) {

            const formData = new FormData();
            formData.append("action","buscaActivos");
            formData.append("numCertificado", data.numCertificado);
            const response = await fetch(URL,{method:"POST",body:formData});
            const objData = await response.json();

            this.activos = objData.activos;
            
            const form = document.createElement("form");
            form.method = "post";
            form.target = "_blank";
            form.action = URLPDF;       
            
            function addField(name,value){
                const input = document.createElement("input");
                input.type="hidden";
                input.name=name;
                input.value = value;
                form.appendChild(input);
            }
         
            addField("numCertificado", data.numCertificado);
            addField("nombreTercero", data.nombre);
            addField("documentoTercero", data.documento);
            addField("fecha", data.fecha);
            addField("observacionesGenerales", data.observaciones);
            addField("data", JSON.stringify(this.activos));
        
            document.body.appendChild(form);
            form.submit();
            document.body.removeChild(form);
        },

        formatFecha: function(fecha) {
            // Separar la fecha en componentes
            const parts = fecha.split('-');
            if (parts.length !== 3) {
                throw new Error('Formato de fecha inválido. Use Y-m-d.');
            }
            const year = parts[0];
            const month = parts[1];
            const day = parts[2];
        
            // Retornar la fecha en el formato deseado
            return `${day}/${month}/${year}`;
        },
    },
    computed:{

    }
})
