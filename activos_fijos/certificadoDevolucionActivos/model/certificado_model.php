<?php
    require_once '../../../Librerias/core/Helpers.php';
    require_once '../../../Librerias/core/Mysql.php';
    session_start();
    
    class certificadoModel extends Mysql {

        private $documento;

        public function searchConsecutive() {
            $consecutivo = searchConsec("acti_certificado_devolucion", "num_certificado");
            return $consecutivo;
        }

        public function searchTerceros() {
            $data = [];
            $sql_terceros = "SELECT UPPER(CONCAT(nombre1, ' ', nombre2, ' ', apellido1, ' ', apellido2)) as nombre, UPPER(razonsocial) as razonsocial, cedulanit, id_tercero, estado FROM terceros ORDER BY id_tercero DESC";
            $terceros = $this->select_all($sql_terceros);

            foreach ($terceros as $tercero) {
                $dataTemp = [];
                $dataTemp["terceroId"] = $tercero["id_tercero"];
                $dataTemp["documento"] = $tercero["cedulanit"];
                
                if ($tercero["razonsocial"] != "") {
                    $dataTemp["nombre"] = $tercero["razonsocial"];
                } else {
                    $dataTemp["nombre"] = preg_replace("/\s+/", " ", trim($tercero["nombre"]));
                }

                array_push($data, $dataTemp);
            }

            return $data;
        }

        public function busca_activos_terceros(int $documento) {
            $this->documento = $documento;

            $sql_activos_responsable = "SELECT D.placa,
            D.nombre,
            P.nombrearea as nameDependencia,
            U.nombre as nameUbicacion
            FROM acticrearact_det_responsable as R 
            RIGHT JOIN acticrearact_det D ON R.placa = D.placa 
            INNER JOIN planacareas as P ON D.area = P.codarea
            INNER JOIN actiubicacion as U ON D.ubicacion = U.id_cc
            WHERE R.tercero = '$this->documento' AND R.estado = 'S'";
            $activos = $this->select_all($sql_activos_responsable);

            foreach ($activos as $index => $activo) {
                $activos[$index]["check"] = false;
                $activos[$index]["estado"] = "";
                $activos[$index]["observacion"] = "";
            }

            return $activos;
        }

        public function inserte_data_cab(int $numCertificado, string $fecha, int $teceroId, string $descripcion) {

            $sql = "INSERT INTO acti_certificado_devolucion (num_certificado, fecha, tercero_id, observaciones, estado) VALUES (?, ?, ?, ?, ?)";
            $value = [
                $numCertificado,
                "$fecha",
                $teceroId,
                "$descripcion",
                'S'
            ];
            $request = $this->insert($sql, $value);

            return $request;
        }

        public function insert_data_det(int $numCertificado, array $data) {
            
            $sql = "INSERT INTO acti_certificado_devolucion_det (num_certificado, placa, estado_activo, observacion) VALUES (?, ?, ?, ?)";

            foreach ($data as $activo) {

                if ($activo["check"] == true) {
                    $values = [
                        $numCertificado, 
                        $activo["placa"],
                        $activo["estado"],
                        strClean(replaceChar($activo["observacion"]))
                    ];
                    
                    $request = $this->insert($sql, $values);
                    
                    if ($request == 0) {
                        $sql_delete = "DELETE FROM acti_certificado_devolucion WHERE num_certificado = $numCertificado";
                        $sql_delete_det = "DELETE FROM acti_certificado_devolucion_det WHERE num_certificado = $numCertificado";
    
                        $this->delete($sql_delete);
                        $this->delete($sql_delete_det);
    
                        $request = 0;
                    }
                }
            }

            return $request;
        }

        public function certificadosCreados() {
            $sql = "SELECT C.num_certificado as numCertificado, 
            C.fecha, 
            C.tercero_id as terceroId, 
            UPPER(CONCAT(T.nombre1, ' ', T.nombre2, ' ', T.apellido1, ' ', T.apellido2)) as nombre,
            T.cedulanit as documento,
            C.observaciones, 
            C.estado 
            FROM acti_certificado_devolucion AS C INNER JOIN terceros AS T ON C.tercero_id = T.id_tercero
            ORDER BY num_certificado DESC";
            $certificados = $this->select_all($sql);

            foreach ($certificados as $index => $certificado) {
                $certificados[$index]["nombre"] = preg_replace("/\s+/", " ", trim($certificado["nombre"]));
            }

            return $certificados;
        }

        public function anular(int $numCertificado) {
            $sql = "UPDATE acti_certificado_devolucion SET estado = ?  WHERE num_certificado = $numCertificado";
            $request = $this->update($sql, ['N']);

            return $request;
        }

        public function activosAgregados(int $numCertificado) {
            $sql = "SELECT 
            C.placa,
            UPPER(A.nombre) as nombreActivo,
            UPPER(C.estado_activo) as estado_activo,
            C.observacion
            FROM acti_certificado_devolucion_det as C INNER JOIN acticrearact_det as A
            ON C.placa = A.placa
            WHERE C.num_certificado = $numCertificado";

            $activosAgregados = $this->select_all($sql);

            return $activosAgregados;
        }
    }
?>