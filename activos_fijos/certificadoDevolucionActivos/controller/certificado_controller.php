<?php
    require_once '../../../Librerias/core/Helpers.php';
    require_once '../model/certificado_model.php';
    session_start();

    class certificadoController extends certificadoModel {

        public function get_data_create() {
            if (!empty($_SESSION)) {
                $arrResponse = array(
                    "consecutivo" => $this->searchConsecutive(),
                    "terceros" => $this->searchTerceros()
                );
                echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
            }

            die();
        }

        public function get_data_search() {
            if (!empty($_SESSION)) {
                $arrResponse = array(
                    "certificados" => $this->certificadosCreados()
                );

                echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
            }
        }

        public function search_actives() {
            if (!empty($_SESSION)) {
                $documento = (int) $_POST["documento"];
                $activos = $this->busca_activos_terceros($documento);

                $arrResponse = [
                    "activos" => $activos
                ];
                echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
            }
            
            die();
        }

        public function busca_activos() {
            if (!empty($_SESSION)) {

                $consecutivo = intval($_POST["numCertificado"]);
                
                $arrResponse = array(
                    "activos" => $this->activosAgregados($consecutivo)
                );

                echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
            }
        }

        public function save_create() {
            if (!empty($_SESSION)) {
                $consecutivo = intval($_POST["consecutivo"]);
                $fecha = strClean($_POST["fecha"]);
                $terceroId = intval($_POST["terceroId"]);
                $observacion = strClean(replaceChar($_POST["observacion"]));
                $arrActivos = json_decode($_POST['activos'],true);
                
                $request = $this->inserte_data_cab($consecutivo, $fecha, $terceroId, $observacion);
                
                if ($request > 0) {

                    $request_det = $this->insert_data_det($consecutivo, $arrActivos);    

                    if ($request_det > 0) {
                        $arrResponse = ["status" => true, "msg" => "Certificado guardado con exito"];
                    }
                    else {
                        $arrResponse = ["status" => false, "msg" => "Ha ocurrido un error en el guardado de activos, intente de nuevo."];
                    }
                }                
                else {
                    $arrResponse = ["status" => false, "msg" => "Ha ocurrido un error en la cabecera, intente de nuevo."];
                }

                echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
            }

            die();
        }

        public function anula_documento() {
            if (!empty($_SESSION)) {
                $numCertificado = intval($_POST["numCertificado"]);
                $request = $this->anular($numCertificado);

                if ($request > 0) {
                    $arrResponse = ["status" => true, "msg" => "Certificado anulado con exito"];
                }
                else {
                    $arrResponse = ["status" => false, "msg" => "Ha ocurrido un error en el anular, vuelva a intentarlo."];
                }

                echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
            }
        }
    }

    if($_POST){
        $obj = new certificadoController();

        switch ($_POST["action"]) {
            case 'getDataCreate':
                $obj->get_data_create();
                break;

            case 'searchActives':
                $obj->search_actives();
                break;

            case 'saveCreate':
                $obj->save_create();
                break;

            case 'getDataSearch':
                $obj->get_data_search();
                break;

            case 'anulaCertificado':
                $obj->anula_documento();
                break;
            
            case 'buscaActivos':
                $obj->busca_activos();
                break;

            default:
                $arrResponse = ["status" => false, "msg" => "Action no encontrada"];
                echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
                break;
        }
    }
?>