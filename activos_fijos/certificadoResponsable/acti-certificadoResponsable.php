<?php
    header("Cache-control: no-cache, no-store, must-revalidate");
    header("Content-Type: text/html;charset=utf8");
	require_once '../../comun.inc';
    require '../../funciones.inc';
    session_start();

	$linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");

    $out = array('error' => false);

    $action = "show";

    if(isset($_GET['action'])){
        $action = $_GET['action'];
    }

    if ($action == "datosIniciales") {

        $terceros = [];

        $sqlTercero = "SELECT nombre1, nombre2, apellido1, apellido2, cedulanit, razonsocial, tipodoc FROM terceros WHERE estado = 'S'";
        $resTercero = mysqli_query($linkbd, $sqlTercero);
        while ($rowTerceros = mysqli_fetch_row($resTercero)) {
            
            $temporal = array();

            if ($rowTerceros[6] == "31") {

                $nombre = $rowTerceros[5];
            }
            else {

                $nombre = $rowTerceros[0] . " " . $rowTerceros[1] . " " . $rowTerceros[2] . " " . $rowTerceros[3];
            }

            array_push($temporal, $rowTerceros[4]);
            array_push($temporal, $nombre);
            array_push($terceros, $temporal);
        }

        $out['terceros'] = $terceros;

        $dependencias = [];
        
        $sqlDependencia = "SELECT codarea, nombrearea FROM planacareas WHERE estado = 'S'";
        $resDependencia = mysqli_query($linkbd, $sqlDependencia);
        while ($rowDependencia = mysqli_fetch_row($resDependencia)) {
            
            array_push($dependencias, $rowDependencia);
        }

        $out['dependencias'] = $dependencias;
    }

    if ($action == "buscaActivos") {

        $listadoActivos = [];
        $placas = [];
        $documento = $codDependencia = "";
        $documento = (isset($_POST['documento'])) ? $_POST['documento'] : "0";
        $codDependencia = (isset($_POST['dependencia'])) ? $_POST['dependencia'] : "";

        if ($codDependencia == "") {
            $filtro = "";
        }
        else {
            $filtro = "AND area = '$codDependencia'";
        }

        $sqlPlacas = "SELECT placa FROM acticrearact_det_responsable WHERE tercero = '$documento' AND estado = 'S' ORDER BY placa ASC";
        $resPlacas = mysqli_query($linkbd, $sqlPlacas);
        $placas = mysqli_fetch_all($resPlacas);
        
        foreach ($placas as $key => $placa) {
            
            $datosTemp = [];
            $nombre = $fechaCompra = $estado = $ubicacion = $dependencia = "";

            $sqlInfo = "SELECT nombre, fechacom, estadoactivo, ubicacion, area FROM acticrearact_det WHERE placa = '$placa[0]' $filtro";
            $resInfo = mysqli_query($linkbd, $sqlInfo);
            $rowInfo = mysqli_fetch_row($resInfo);

            if ($rowInfo != "") {
                $sqlUbicacion = "SELECT nombre FROM actiubicacion WHERE estado = 'S' AND id_cc = '$rowInfo[3]'";
                $resUbicacion = mysqli_query($linkbd, $sqlUbicacion);
                $rowUbicacion = mysqli_fetch_row($resUbicacion);
                
                $sqlDependencia = "SELECT nombrearea FROM planacareas WHERE codarea = '$rowInfo[4]'";
                $resDependencia = mysqli_query($linkbd, $sqlDependencia);
                $rowDependencia = mysqli_fetch_row($resDependencia);
    
                $nombre = $rowInfo[0];
                $fechaCompra = date("d/m/Y", strtotime($rowInfo[1]));
                $estado = $rowInfo[2];
                $ubicacion = $rowInfo[3] . " - " . $rowUbicacion[0];
                $dependencia = $rowInfo[4] . " - " . $rowDependencia[0];
    
                $datosTemp[] = $placa[0];
                $datosTemp[] = $nombre;
                $datosTemp[] = $fechaCompra;
                $datosTemp[] = $estado;
                $datosTemp[] = $ubicacion;
                $datosTemp[] = $dependencia;
                array_push($listadoActivos, $datosTemp);
            }
        }

        $out['listadoActivos'] = $listadoActivos;
    }

    header("Content-type: application/json");
    echo json_encode($out);
    die();