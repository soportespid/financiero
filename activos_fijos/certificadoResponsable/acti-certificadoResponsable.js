const URL = 'activos_fijos/certificadoResponsable/acti-certificadoResponsable.php';
import { filtroEnArrayDeObjetos } from './../../funciones.js';

var app = new Vue({
    el: '#myapp',
	data:{

        error: '',
        loading: false,
        responsable: '',
        nomResponsable: '',
        terceros: [],
        terceroscopy: [],
        searchCedula: '',
        showModalResponsables: false,
        listadoActivos: [],
        dependencias: [],
        dependencia: '',
    },

    mounted: function(){

        this.datosIniciales();
    },

    methods: 
    {
        //Desglosar array
        toFormData: function(obj){
            var form_data = new FormData();
            for(var key in obj){
                form_data.append(key, obj[key]);
            }
            return form_data;
        },

        formatonumero: function(valor){
            return new Intl.NumberFormat("en-US", {style: "currency", currency: "USD"}).format(valor);
        },
        
        datosIniciales: async function() {

            await axios.post(URL+'?action=datosIniciales').then((response) => {
                    
                this.terceros = response.data.terceros;
                this.terceroscopy = response.data.terceros;
                this.dependencias = response.data.dependencias;
            });
        },

        despliegoTerceros: function() {

            this.showModalResponsables = true;
        },

        filtroResponsable: async function() {

            const data = this.terceroscopy;
            var text = this.searchCedula;
            var resultado = [];

            resultado = await filtroEnArrayDeObjetos({'data': data, 'text': text});

            this.terceros = resultado;
        },

        seleccionaResponsable: function(doc, nom) {

            if (doc != "" && nom != "") {

                this.responsable = doc;
                this.nomResponsable = nom;
                this.showModalResponsables = false;
            }
        },

        buscarActivos: async function() {

            if (this.responsable != "" && this.nomResponsable != "") {

                var formData = new FormData();
                formData.append("documento", this.responsable);
                formData.append("dependencia", this.dependencia);

                await axios.post(URL+'?action=buscaActivos', formData).then((response) => {
                    
                    this.listadoActivos = response.data.listadoActivos;
                });
            }
            else {
                Swal.fire({
                    icon: 'warning',
                    title: 'No ha seleccionado responsable',
                })    
            }
        },
    },
});