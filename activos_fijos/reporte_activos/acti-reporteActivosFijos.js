var app = new Vue({
    el: '#myapp',
	data:{

        error: '',
        loading: false,
    
        clase: '',
        clases: [],
        grupo: '',
        grupos: [],
        tipo: '',
        tipos: [],
        placaInicial: '',
        placaFinal: '',
        activos: [],
    },

    mounted: function(){

        this.datosIniciales();
    },

    methods: 
    {
        //Desglosar array
        toFormData: function(obj){
            var form_data = new FormData();
            for(var key in obj){
                form_data.append(key, obj[key]);
            }
            return form_data;
        },

        formatonumero: function(valor){
            return new Intl.NumberFormat("en-US", {style: "currency", currency: "USD"}).format(valor);
        },

        datosIniciales: async function() {
            
            await axios.post('activos_fijos/reporte_activos/acti-reporteActivosFijos.php?action=datosIniciales')
            .then((response) => {
                
                this.clases = response.data.clases;
            }).catch((error) => {
                this.error = true;
                console.log(error)
            }).finally(() => {
                this.loading = false;
                
            });
        },

        buscarGrupo: async function() {

            if (this.clase != "") {

                await axios.post('activos_fijos/reporte_activos/acti-reporteActivosFijos.php?action=buscarGrupos&clase='+this.clase)
                .then((response) => {
                    
                    this.grupos = response.data.grupos;
                });
            }
        },

        buscarTipo: async function() {

            if (this.grupo != "") {
                await axios.post('activos_fijos/reporte_activos/acti-reporteActivosFijos.php?action=buscarTipos&clase='+this.clase+'&grupo='+this.grupo)
                .then((response) => {
                    
                    this.tipos = response.data.tipos;
                });
            }
        },

        buscarActivos: async function() {

            const fechaInicial = document.getElementById('fechaIni').value;
            const fechaFinal = document.getElementById('fechaFin').value;

            //validacion fechas
            if ((fechaInicial == "" && fechaFinal == "") || (fechaInicial != "" && fechaFinal != "")) {

                //validacion placas
                if ((this.placaInicial == "" && this.placaFinal == "") || (this.placaInicial != "" && this.placaFinal != "")) {

                    this.loading = true;

                    var formData = new FormData();

                    formData.append("fechaIni", fechaInicial);
                    formData.append("fechaFin", fechaFinal);
                    formData.append("placaIni", this.placaInicial);
                    formData.append("placaFin", this.placaFinal);
                    formData.append("clase", this.clase);
                    formData.append("grupo", this.grupo);
                    formData.append("tipo", this.tipo);

                    await axios.post('activos_fijos/reporte_activos/acti-reporteActivosFijos.php?action=buscarActivos', formData)
                    .then((response) => {
                    
                        this.activos = response.data.activos;
                    });

                    this.loading = false;
                }
                else {
                    Swal.fire("Mal uso de filtro", "Debes llenar las dos placas o no llenar ninguna placa", "info");    
                }
            }
            else {
                Swal.fire("Mal uso de filtro", "Debes llenar las dos fechas o no llenar ninguna fecha", "info");
            }
        },

        excel: function() {

            if (this.activos != '') {

                document.form2.action="acti-excelReporteActivosFijos.php";
				document.form2.target="_BLANK";
				document.form2.submit(); 
            }
            else {
                Swal.fire("Faltan datos", "warning");
            }
        },

        fichaTecnica: function(aux) {

            if (aux[0] != "") {

                var x = "acti-fichaActivoFijo.php?placa="+aux[0];
                window.open(x, '_blank');
                window.focus();
            }
        },
    }
});