const URL = 'activos_fijos/ordenActivacion/crear/acti-ordenActivacion.php';
const URLSECOND = 'activos_fijos/reporte_activos/acti-fichaActivoFijo.php';
import { filtroEnArrayDeObjetos } from './../../funciones.js';

var app = new Vue({
    el: '#myapp',
	data:{

        error: '',
        loading: false,
        
        placa: '',
        nombre: '',
        fechaRegistro: '',
        fechaCompra: '',
        fechaActivacion: '',
        valorActivo: '',
        valorDepreciado: '',
        valorCorreccion: '',
        referencia: '',
        modelo: '',
        serial: '',
        clase: '',
        grupo: '',
        tipo: '',
        prototipo: '',
        area: '',
        ubicacion: '',
        cc: '',
        disposicion: '',
        estadoActivo: '',
        responsable: '',
        foto: '',
        img: '',
        imagen : [],
        ficha: [],
        terceros: [],
        terceroscopia: [],
        showModalResponsables: false,
        searchCedula: '',
        cedula: '',
        dependencias: [],
        prototipos: [],
        ubicaciones: [],
    },

    mounted: function(){

        this.traeDatosFormulario();
    },

    methods: 
    {
        //Desglosar array
        toFormData: function(obj){
            var form_data = new FormData();
            for(var key in obj){
                form_data.append(key, obj[key]);
            }
            return form_data;
        },

        datosIniciales: async function() {
            
            await axios.post(URL+'?action=datosIniciales')
            .then((response) => {
                
                // this.origenes = response.data.origenes;
                // this.clases = response.data.clases;
                this.prototipos = response.data.prototipos;
                this.dependencias = response.data.dependencias;
                this.ubicaciones = response.data.ubicaciones;
                // this.centroCostos = response.data.centroCostos;
                // this.disposiciones = response.data.disposiciones;
                // this.codigoOrden = response.data.codigoOrden;
            }).catch((error) => {
                this.error = true;
                console.log(error)
            }).finally(() => {
                this.loading = false;
                
            });
        },

        formatonumero: function(valor){
            return new Intl.NumberFormat("en-US", {style: "currency", currency: "USD"}).format(valor);
        },

        traeParametros: function(name){
            //Captura parametros
            name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
            var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
            results = regex.exec(location.search);
            return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
        },

        traeDatosFormulario: async function() {

            this.placa = this.traeParametros('placa');

            if (this.placa != "") {

                var formData = new FormData();

                formData.append("placa", this.placa);
                
                await axios.post('activos_fijos/reporte_activos/acti-fichaActivoFijo.php?action=datosFormulario', formData)
                .then((response) => {
                    
                    this.nombre = response.data.nombre;
                    this.fechaRegistro = response.data.fechaRegistro;
                    document.getElementById('fechaActivacion').value = response.data.fechaCompra;
                    document.getElementById('fechaCompra').value = response.data.fechaActivacion;
                    this.valorActivo = response.data.valorActivo;
                    this.valorDepreciado = response.data.valorDepreciado;
                    this.valorCorreccion = response.data.valorCorreccion;
                    this.referencia = response.data.referencia;
                    this.modelo = response.data.modelo;
                    this.serial = response.data.serial;
                    this.clase = response.data.clase;
                    this.grupo = response.data.grupo;
                    this.tipo = response.data.tipo;
                    this.prototipo = response.data.prototipo;
                    this.area = response.data.area;
                    this.ubicacion = response.data.ubicacion;
                    this.cc = response.data.cc;
                    this.disposicion = response.data.disposicion;
                    this.estadoActivo = response.data.estadoActivo;
                    this.responsable = response.data.responsable;
                    this.foto = response.data.foto;
                    this.cedula = response.data.cedula;
                }).catch((error) => {
                    this.error = true;
                    console.log(error)
                }).finally(() => {
                    
                    if (this.foto != "") {
                        this.img = "informacion/proyectos/temp/"+this.foto;
                    }
                    else {
                        this.img = "imagenes/useradd02.png";
                    }

                    this.datosIniciales();
                });
            }
            else {
                Swal.fire(
                    'Error!',
                    'No hay ninguna placa seleccionada.',
                    'error'
                );
            }
            
        },

        obtenerImagen: async function(e) {

            let file = e.target.files[0];
            this.imagen = file;
          
            let formData = new FormData();

            formData.append("placa", this.placa);
            formData.append("imagen", this.imagen);
            
            await axios.post('activos_fijos/reporte_activos/acti-fichaActivoFijo.php?action=cargaImagen', formData)
            .then((response) => {
               
                if(response.data.insertaBien == true) {
                    Swal.fire({
                        icon: 'success',
                        title: 'Se ha actualizado la foto del activo',
                        showConfirmButton: false,
                        timer: 1500
                        }).then((response) => {
                            app.redireccionar();
                        }
                    );
                }
                else {
                    Swal.fire(
                        'Error!',
                        'No se pudo actualizar la foto.',
                        'error'
                    );
                }
            }).catch((error) => {
                this.error = true;
                console.log(error)
            }).finally(() => {
                
            });
        },

        obtenerFicha: async function(e) {

            let file = e.target.files[0];
            this.ficha = file;
          
            let formData = new FormData();

            formData.append("placa", this.placa);
            formData.append("imagen", this.ficha);
            
            await axios.post('activos_fijos/reporte_activos/acti-fichaActivoFijo.php?action=cargaFicha', formData)
            .then((response) => {
               
                if(response.data.insertaBien == true) {
                    Swal.fire({
                        icon: 'success',
                        title: 'Se ha actualizado la ficha con exito',
                        showConfirmButton: false,
                        timer: 2500
                        }).then((response) => {
                            app.redireccionar();
                        }
                    );
                }
                else {
                    Swal.fire(
                        'Error!',
                        'No se pudo actualizar la foto.',
                        'error'
                    );
                }
            }).catch((error) => {
                this.error = true;
                console.log(error)
            }).finally(() => {
                
            });
        },

        actualizaFechaCompra: async function() {

            const { value: valor } = await Swal.fire({
                title: 'Cambio de valor de activo',
                input: 'number',
                inputLabel: 'Valor de activo',
                inputPlaceholder: 'Ingrese el valor del activo'
            })
            
            if (valor > 0) {
                this.valorActivo = valor;
            }
        },

        despliegaModalResponsable: async function() {

            this.loading = true;
            
            await axios.post(URL+'?action=buscarTerceros')
            .then((response) => {
                
                this.terceros = response.data.terceros;
                this.terceroscopia = response.data.terceros;
            });

            this.loading = false;
            this.showModalResponsables = true;
        },

        filtroCedulaResponsable: async function() {

            if (this.searchCedula != "") {

                const data = this.terceroscopia;
                var text = this.searchCedula;
                var resultado = [];

                resultado = await filtroEnArrayDeObjetos({'data': data, 'text': text});

                this.terceros = resultado;
            }
        },
  
        seleccionaResponsable: function(cedula, nombre) {
        
            if (cedula != "" && nombre != "") {

                this.cedula = cedula;
                this.responsable = cedula + " - " + nombre;
                this.showModalResponsables = false;
            }        
        },
        
        guardar: function (){

            Swal.fire({
                icon: 'question',
                title: 'Esta seguro que quiere guardar?',
                showDenyButton: true,
                confirmButtonText: 'Guardar!',
                denyButtonText: 'Cancelar',
                }).then((result) => {
                if (result.isConfirmed) {

                    var formData = new FormData();

                    formData.append("placa", this.placa);
                    formData.append("nombre", this.nombre);
                    formData.append("fechaCompra", document.getElementById('fechaCompra').value);
                    formData.append("fechaActivacion", document.getElementById('fechaActivacion').value);
                    formData.append("valorActivo", this.valorActivo);
                    formData.append("area", this.area);
                    formData.append("prototipo", this.prototipo);
                    formData.append("responsable", this.cedula);
                    formData.append("referencia", this.referencia);
                    formData.append("modelo", this.modelo);
                    formData.append("serial", this.serial);
                    formData.append("ubicacion", this.ubicacion);

                    axios.post(URLSECOND + '?action=guardar', formData)
                    .then((response) => {
                        
                        if(response.data.insertaBien){
                            Swal.fire({
                                position: 'top-end',
                                icon: 'success',
                                title: 'Se ha actualizado con exito',
                                showConfirmButton: false,
                                timer: 1500
                            }).then((response) => {
                                
                                
                            });
                        }
                        else{
                        
                            Swal.fire(
                                'Error!',
                                'No se pudo guardar.',
                                'error'
                            );
                        }
                        
                    });
                } 
            })  
        
        },

        redireccionar: function() {
            location.reload();
        },
    }
});