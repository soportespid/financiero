e<?php
	require_once '../../comun.inc';
    require '../../funciones.inc';
    require '../../conversor.php';
    session_start();

	$linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");

    $out = array('error' => false);

    $action = "show";

    if(isset($_GET['action'])){
        $action = $_GET['action'];
    }

    function buscaDepreciacion($placa, $array) {

        $valor = 0;

        foreach ($array as $key => $value) {
            
            if ($placa == $value[0])  {
                $valor = $value[1];
                break;
            }
        }
        
        return $valor;
    }

    if ($action == "datosIniciales") {

        $clases = array();

        $sqlClase = "SELECT codigo, nombre FROM actipo WHERE niveluno = '0' AND estado = 'S'";
		$resClase = mysqli_query($linkbd,$sqlClase);
        while ($rowClase = mysqli_fetch_row($resClase)) {
            
            array_push($clases, $rowClase);
        }

        $out['clases'] = $clases;
    }

    if ($action == 'buscarGrupos') {

        $grupos = array();

        $clase = $_GET['clase'];

        $sqlr = "SELECT codigo, nombre FROM actipo WHERE niveluno='$clase' AND estado = 'S' ";
        $resp = mysqli_query($linkbd,$sqlr);
        while ($row = mysqli_fetch_row($resp)) {
            
            array_push($grupos, $row);
        }

        $out['grupos'] = $grupos;
    }

    if ($action == "buscarTipos") {

        $clase = $_GET['clase'];
        $grupo = $_GET['grupo'];
        $tipos = array();
        
        $sqlr = "SELECT codigo, nombre FROM actipo WHERE niveluno = '$grupo' AND niveldos = '$clase' AND estado = 'S' ";
		$resp = mysqli_query($linkbd,$sqlr);
        while ($row = mysqli_fetch_row($resp)) {
            
            array_push($tipos, $row);
        }

        $out['tipos'] = $tipos;
    }

    if ($action == "buscarActivos") {

        //filtros

        //fecha de creacion documento
        if ($_POST['fechaIni'] != "" && $_POST['fechaFin'] != "") {

            preg_match("/([0-9]{2})\/([0-9]{2})\/([0-9]{4})/", $_POST['fechaIni'], $f);
			$fechaIni = $f[3]."-".$f[2]."-".$f[1];

            preg_match("/([0-9]{2})\/([0-9]{2})\/([0-9]{4})/", $_POST['fechaFin'], $f);
			$fechaFin = $f[3]."-".$f[2]."-".$f[1];

            $filtroFechas = "AND CAB.fechareg BETWEEN '$fechaIni' AND '$fechaFin' ";
        }
        else {
            $filtroFechas = "";
        }

        //placas

        if ($_POST['placaIni'] != "" && $_POST['placaFin']) {

            $filtroPlacas = "AND DET.placa BETWEEN '$_POST[placaIni]' AND '$_POST[placaFin]' ";
        }
        else {
            $filtroPlacas = "";
        }

        //clase, grupo y tipo

        if ($_POST['clase'] != "") {

            $filtroClase = "AND DET.clasificacion = '$_POST[clase]' ";
        }
        else {
            $filtroClase = "";
        }

        if ($_POST['grupo'] != "") {

            $filtroGrupo = "AND DET.grupo = '$_POST[grupo]' ";
        }
        else {
            $filtroGrupo = "";
        }

        if ($_POST['tipo'] != "") {

            $filtroTipo = "AND DET.tipo = '$_POST[tipo]' ";
        }
        else {
            $filtroTipo = "";
        }

        $activos = array();
        $depreciaciones = [];

        $sqlDep = "SELECT numacti, SUM(valcredito) FROM comprobante_det WHERE (tipo_comp = '78' OR tipo_comp = '22') AND valcredito != 0 AND cuenta != '' AND estado = '1' GROUP BY numacti";
        $resDep = mysqli_query($linkbd, $sqlDep);
        while ($rowDep = mysqli_fetch_row($resDep)) {

            array_push($depreciaciones, $rowDep);
        }

        $tipoAnt = "";
        $grupoAnt = "";
        $cont = 0;
        $cantidad = 0;

        $sqlActivos = "SELECT DET.placa, DET.nombre, CAB.fechareg, DET.fechacom, DET.fechact, DET.clasificacion, DET.grupo, DET.tipo, DET.valor, DET.area, DET.ubicacion FROM acticrearact_det AS DET INNER JOIN acticrearact AS CAB ON CAB.codigo = DET.codigo WHERE DET.estado='S' $filtroFechas $filtroPlacas $filtroClase $filtroGrupo $filtroTipo ORDER BY DET.placa ASC";
        $resActivos = mysqli_query($linkbd, $sqlActivos);
        $totalRespuesta = mysqli_num_rows($resActivos);
        while ($rowActivos = mysqli_fetch_row($resActivos) ) {
            
            $datosActi = array();

            $fechaRegistro = date("d/m/Y", strtotime($rowActivos[2]));
            $fechaCompra = date("d/m/Y", strtotime($rowActivos[3]));
            $fechaActivacion = date("d/m/Y", strtotime($rowActivos[4]));

            $sqlClase = "SELECT nombre FROM actipo WHERE codigo = '$rowActivos[5]' AND niveluno = '0' AND estado = 'S'";
            $rowClase = mysqli_fetch_row(mysqli_query($linkbd, $sqlClase));

            $clase = $rowActivos[5] . " - " . $rowClase[0];

            $sqlGrupo = "SELECT nombre FROM actipo WHERE codigo = '$rowActivos[6]' AND niveluno='$rowActivos[5]' AND estado = 'S'";
            $rowGrupo = mysqli_fetch_row(mysqli_query($linkbd, $sqlGrupo));

            $grupo = $rowActivos[6] . " - " . $rowGrupo[0];

            $sqlTipo = "SELECT nombre FROM actipo WHERE codigo = '$rowActivos[7]' AND niveluno = '$rowActivos[6]' AND niveldos = '$rowActivos[5]' AND estado = 'S'";
            $rowTipo = mysqli_fetch_row(mysqli_query($linkbd, $sqlTipo));

            $tipo = $rowActivos[7] . " - " . $rowTipo[0];

            $sqlDependencia = "SELECT nombrearea FROM planacareas WHERE estado = 'S' AND codarea = '$rowActivos[9]'";
            $rowDependencia = mysqli_fetch_row(mysqli_query($linkbd, $sqlDependencia));

            $dependencia = $rowActivos[9] . " - " . $rowDependencia[0];

            $sqlUbicaciones = "SELECT nombre FROM actiubicacion WHERE id_cc = '$rowActivos[10]' AND estado = 'S'";
            $resUbicaciones = mysqli_query($linkbd, $sqlUbicaciones);
            $rowUbicaciones = mysqli_fetch_row($resUbicaciones);

            $ubicacion = $rowActivos[10] . " - " . $rowUbicaciones[0];

            $sqlValorActi = "SELECT SUM(valdebito) FROM comprobante_det WHERE tipo_comp = '70' AND numacti LIKE '$rowActivos[0]' AND valdebito != 0 AND cuenta != '' AND estado = '1'";
            $resValorActi = mysqli_query($linkbd, $sqlValorActi);
            $rowValorActi = mysqli_fetch_row($resValorActi);

            $valorActivo = $rowValorActi[0];

            /* DEMASIADO TIEMPO DE CARGA*/
            $valorDepreciado = doubleval(buscaDepreciacion($rowActivos[0], $depreciaciones));

            $valorCorrecion = 0;

            $valorPorDepreciar = $valorActivo - $valorDepreciado + $valorCorrecion;

            $sqlTercero = "SELECT tercero FROM acticrearact_det_responsable WHERE placa = '$rowActivos[0]'";
            $rowTercero = mysqli_fetch_row(mysqli_query($linkbd, $sqlTercero));

            $nombre = buscatercero($rowTercero[0]);

            $responsable = $rowTercero[0] . " - " . $nombre;

            if ($tipoAnt != "" && $grupoAnt != "") {
                if ($tipoAnt == $rowActivos[7] && $grupoAnt == $rowActivos[6]) {

                    $cont += 1;
                    $totalValorActivos += $valorActivo;
                    $totalValorActivosModulo += $rowActivos[8];
                    $totalDepreciado += $valorDepreciado;
                    $totalCorreccion += $valorCorrecion;
                    $totalValorPorDepreciar += $valorPorDepreciar;
                }
                else {

                    array_push($datosActi, ""); //placa
                    array_push($datosActi, ""); //nombre
                    array_push($datosActi, ""); 
                    array_push($datosActi, ""); 
                    array_push($datosActi, ""); 
                    array_push($datosActi, ""); 
                    array_push($datosActi, "TOTALES"); 
                    array_push($datosActi, $cont); 
                    array_push($datosActi, $totalValorActivos); 
                    array_push($datosActi, $totalValorActivosModulo); 
                    array_push($datosActi, $totalDepreciado); 
                    array_push($datosActi, $totalCorreccion); 
                    array_push($datosActi, $totalValorPorDepreciar); 
                    array_push($datosActi, ""); 
                    array_push($datosActi, ""); 
                    array_push($datosActi, ""); 
        
                    array_push($activos, $datosActi);

                    $cont = 1;
                    $totalValorActivos = $valorActivo;
                    $totalValorActivosModulo = $rowActivos[8];
                    $totalDepreciado = $valorDepreciado;
                    $totalCorreccion = $valorCorrecion;
                    $totalValorPorDepreciar = $valorPorDepreciar;
                    $datosActi = array();
                }
            }
            else {

                $cont += 1;
                $totalValorActivos += $valorActivo;
                $totalValorActivosModulo += $rowActivos[8];
                $totalDepreciado += $valorDepreciado;
                $totalCorreccion += $valorCorrecion;
                $totalValorPorDepreciar += $valorPorDepreciar;
            }

            $tipoAnt = $rowActivos[7];
            $grupoAnt = $rowActivos[6];

            array_push($datosActi, $rowActivos[0]); //placa
            array_push($datosActi, $rowActivos[1]); //nombre
            array_push($datosActi, $fechaRegistro); 
            array_push($datosActi, $fechaCompra); 
            array_push($datosActi, $fechaActivacion); 
            array_push($datosActi, $clase); 
            array_push($datosActi, $grupo); 
            array_push($datosActi, $tipo); 
            array_push($datosActi, $valorActivo); 
            array_push($datosActi, $rowActivos[8]); 
            array_push($datosActi, $valorDepreciado); 
            array_push($datosActi, $valorCorrecion); 
            array_push($datosActi, $valorPorDepreciar); 
            array_push($datosActi, $dependencia); 
            array_push($datosActi, $ubicacion); 
            array_push($datosActi, $responsable); 
            array_push($activos, $datosActi);

            $cantidad++;

            if ($cantidad == $totalRespuesta) {

                $datosActi = array();

                array_push($datosActi, ""); //placa
                array_push($datosActi, ""); //nombre
                array_push($datosActi, ""); 
                array_push($datosActi, ""); 
                array_push($datosActi, ""); 
                array_push($datosActi, ""); 
                array_push($datosActi, "TOTALES"); 
                array_push($datosActi, $cont); 
                array_push($datosActi, $totalValorActivos); 
                array_push($datosActi, $totalValorActivosModulo); 
                array_push($datosActi, $totalDepreciado); 
                array_push($datosActi, $totalCorreccion); 
                array_push($datosActi, $totalValorPorDepreciar); 
                array_push($activos, $datosActi);
            }
        }

        $out['activos'] = $activos;

    }

    header("Content-type: application/json");
    echo json_encode($out);
    die();