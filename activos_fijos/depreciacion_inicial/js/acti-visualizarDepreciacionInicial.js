const URL = 'activos_fijos/depreciacion_inicial/models/acti-depreciacionInicial.php';

var app = new Vue({
    el:"#myapp",
    data() {
        return {
            isLoading:false,

            dataCab: [],
            dataDet: [],
        }
    },
    mounted() {
        this.getData();
    },
    methods: {
        getData: async function(){
            this.id = new URLSearchParams(window.location.search).get('id');
            const formData = new FormData();
            formData.append("action","get_data_visualize");
            formData.append("consecutivo",this.id);
            const response = await fetch(URL,{method:"POST",body:formData});
            const objData = await response.json();

            this.dataCab = objData.dataCab;
            this.dataDet = objData.dataDet;
        },

        formatNumero: function(valor){
            return new Intl.NumberFormat("en-US", {style: "currency", currency: "USD"}).format(valor);      
        },

        formatFecha: function(fecha) {
            // Separar la fecha en componentes
            const parts = fecha.split('-');
            if (parts.length !== 3) {
                throw new Error('Formato de fecha inválido. Use Y-m-d.');
            }
            const year = parts[0];
            const month = parts[1];
            const day = parts[2];
        
            // Retornar la fecha en el formato deseado
            return `${day}/${month}/${year}`;
        },
    },
    computed:{

    }
})
