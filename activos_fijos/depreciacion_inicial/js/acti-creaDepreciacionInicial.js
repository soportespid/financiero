const URL = 'activos_fijos/depreciacion_inicial/models/acti-depreciacionInicial.php';

var app = new Vue({
    el:"#myapp",
    data() {
        return {
            isLoading:false,
            consecutivo: 0,
            fecha: '',
            mes: '',
            cuenta: '',
            cuentaName: '',
            cuentas: [],
            cuentas_copy: [],
            activos: [],
            modalCuentas: false,
            txtSearchCuenta: '',
            numResultadosCuentas: 0,
        }
    },
    mounted() {
        this.getData();
    },
    methods: {
        getData: async function(){
            const formData = new FormData();
            formData.append("action","get_data_create");
            this.isLoading = true;
            const response = await fetch(URL,{method:"POST",body:formData});
            const objData = await response.json();
            this.consecutivo = objData.consecutivo;
            this.cuentas = objData.cuentasnicsp;
            this.cuentas_copy = objData.cuentasnicsp;
            this.numResultadosCuentas = this.cuentas_copy.length;
            this.isLoading = false;
        },

        searchData: async function(type) {
            let search = "";
            if(type == "modal")search = this.txtSearchCuenta.toLowerCase();
            if(type == "cod_cuenta")search = this.cuenta;

            if (type == "modal") {
                this.cuentas_copy = [...this.cuentas.filter(e=>e.cuenta.toLowerCase().includes(search) || e.nombre.toLowerCase().includes(search) )];
                this.numResultadosCuentas = this.cuentas_copy.length;
            } else if (type == "cod_cuenta") {
                let name;
                name = this.cuentas_copy.filter(e=>e.cuenta==search)[0].nombre;
                this.cuentaName =  name;
            } 
        },

        selecCuenta: async function(datos) {

            this.cuenta = datos.cuenta;
            this.cuentaName = datos.nombre;
            this.cuentas_copy = [...this.cuentas];
            this.txtSearchCuenta = '';
            this.numResultadosCuentas = this.cuentas_copy.length;
            this.modalCuentas = false;
        },

        searchActivos: async function() {
            if (this.mes != "") {
                const formData = new FormData();
                formData.append("action","data_activos_create");
                formData.append("depreciaHasta",this.mes);
                this.isLoading = true;
                const response = await fetch(URL,{method:"POST",body:formData});
                const objData = await response.json();
                this.isLoading = false;
                this.activos = objData.activos;
            }
            else {
                Swal.fire("Error","Todos los campos con * son obligatorios","error");
            }
        },

        save: async function() {
            if (this.activos.length > 0 && this.consecutivo > 0 && this.fecha != "" && this.cuenta != "" && this.mes != "") {
                Swal.fire({
                    title:"¿Estás segur@ de guardar?",
                    text:"",
                    icon: 'warning',
                    showCancelButton:true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText:"Sí, guardar",
                    cancelButtonText:"No, cancelar"
                }).then(async function(result){
                    if(result.isConfirmed) {
                        const formData = new FormData();
                        formData.append("action","save_create");
                        formData.append("consecutivo",app.consecutivo);
                        formData.append("fecha",app.fecha);
                        formData.append("cuenta",app.cuenta);
                        formData.append("mes",app.mes);
                        formData.append("activos", JSON.stringify(app.activos));
                        
                        app.isLoading = true;
                        const response = await fetch(URL,{method:"POST",body:formData});
                        const objData = await response.json();
                        app.isLoading = false;

                        if (objData.status == true) {
                            Swal.fire("Guardado","Depreciación inicial guardada con exito","success");
                            setTimeout(function(){
                                location.reload();
                            },2500);
                        }
                        else {
                            let msgError = response.data.msgError;
                            Swal.fire("Error",msgError,"warning");
                        }
                    }
                });
            }
            else {
                Swal.fire("Error","Todos los campos con * son obligatorios","error");
            }
        },

        formatNumero: function(valor){
            return new Intl.NumberFormat("en-US", {style: "currency", currency: "USD"}).format(valor);
            
        },

        formatFecha: function(fecha) {
            // Separar la fecha en componentes
            const parts = fecha.split('-');
            if (parts.length !== 3) {
                throw new Error('Formato de fecha inválido. Use Y-m-d.');
            }
            const year = parts[0];
            const month = parts[1];
            const day = parts[2];
        
            // Retornar la fecha en el formato deseado
            return `${day}/${month}/${year}`;
        },
    },
    computed:{

    }
})
