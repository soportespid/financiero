const URL = 'activos_fijos/depreciacion_inicial/models/acti-depreciacionInicial.php';

var app = new Vue({
    el:"#myapp",
    data() {
        return {
            isLoading:false,

            txtSearch: '',
            arrDepreciaciones: [],
            arrDepreciacionesCopy: [],
            txtResultados: 0,
            fechaBloqueo: '',
        }
    },
    mounted() {
        this.getData();
    },
    methods: {
        getData: async function(){

            const formData = new FormData();
            formData.append("action","get_data_search");
            const response = await fetch(URL,{method:"POST",body:formData});
            const objData = await response.json();

            this.arrDepreciaciones = objData.depreciaciones;
            this.arrDepreciacionesCopy = objData.depreciaciones;
            this.txtResultados = this.arrDepreciacionesCopy.length;
            this.fechaBloqueo = objData.fecha_bloqueo;
        },

        searchData: async function() {
            let search = "";
            search = this.txtSearch.toLowerCase();
            
            this.arrDepreciacionesCopy = [...this.arrDepreciaciones.filter(e=>e.codigo.toLowerCase().includes(search))];
            this.txtResultados = this.arrDepreciacionesCopy.length;
        },

        anulaDocument: async function(data, index) {
            if (data.codigo != "" && data.fecha != "") {
                Swal.fire({
                    title:"¿Estás segur@ de anular?",
                    text:"",
                    icon: 'warning',
                    showCancelButton:true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText:"Sí, anular",
                    cancelButtonText:"No, cancelar"
                }).then(async function(result){
                    if(result.isConfirmed) {

                        const formData = new FormData();
                        formData.append("action","anula_depreciacion");
                        formData.append("consecutivo", data.codigo);
                        formData.append("fecha", data.fecha);
                        const response = await fetch(URL,{method:"POST",body:formData});
                        const objData = await response.json();
        
                        if (objData.status == true) {
                            app.arrDepreciaciones[index].estado = 'N';
                            app.arrDepreciacionesCopy[index].estado = 'N';
                            Swal.fire("Anulado","Depreciación anulada con exito","success");
                        }
                        else {
                            Swal.fire("Error",objData.msg,"warning");
                        }
                    }
                });
            }
        },

        formatNumero: function(valor){
            return new Intl.NumberFormat("en-US", {style: "currency", currency: "USD"}).format(valor);      
        },

        formatFecha: function(fecha) {
            // Separar la fecha en componentes
            const parts = fecha.split('-');
            if (parts.length !== 3) {
                throw new Error('Formato de fecha inválido. Use Y-m-d.');
            }
            const year = parts[0];
            const month = parts[1];
            const day = parts[2];
        
            // Retornar la fecha en el formato deseado
            return `${day}/${month}/${year}`;
        },
    },
    computed:{

    }
})
