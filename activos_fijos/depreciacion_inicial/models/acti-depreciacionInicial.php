<?php
    require_once '../../../Librerias/core/Helpers.php';
    require_once '../../../Librerias/core/Mysql.php';

    header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=utf8");
    date_default_timezone_set("America/Bogota");
    session_start();

    class depreInicialModel extends Mysql {
        
        public function data_create() {
            if (!empty($_SESSION)) {

                $consecutivo = searchConsec("acti_deprecia_inicial", "codigo");
                
                $sql_cuentas = "SELECT cuenta, nombre FROM cuentasnicsp WHERE tipo = 'Auxiliar' AND LENGTH(cuenta) = 9 AND estado = 'S' ORDER BY cuenta ASC";
                $cuentas = $this->select_all($sql_cuentas);

                $arrResponse = array("status"=>true, "consecutivo"=>$consecutivo, "cuentasnicsp"=>$cuentas);
            }

            echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
            die();
        }

        public function search_activos() {
            if (!empty($_SESSION)) {
                $depreciaHasta = $_POST["depreciaHasta"]."-01";
                $fechaFinal = new DateTime($depreciaHasta); 
                $data = []; 
    
                //busca los comprobantes realizados tanto de depreciacion inicial como en depreciacion mensual
                $sql_comprobante = "SELECT numacti AS placa, SUM(valdebito) AS valorDepreciado FROM comprobante_det WHERE (tipo_comp = 22 OR tipo_comp = 78) AND valdebito != 0 AND estado = '1' GROUP BY numacti";
                $comprobantes = $this->select_all($sql_comprobante);
    
                //Busca los activos creados y luego valida que ese número de placa no este en la tabla de depreciacion inicial
                $sql_activos = "SELECT acti.placa,
                acti.nombre,
                acti.fechact,
                acti.valor
                FROM acticrearact_det AS acti LEFT JOIN acti_deprecia_inicial_det AS dep ON acti.placa = dep.placa
                WHERE acti.estado = 'S' AND dep.placa IS NULL AND acti.fechact != ''";
                $activos = $this->select_all($sql_activos);
    
                foreach ($activos as $activo) {
                    $arrayTemp = [];
    
                    $indice = array_search($activo["placa"], array_column($comprobantes, 'placa'));
                    if (!$indice) {
                        $clase = substr($activo["placa"], 0, 1);
                        $grupo = substr($activo["placa"], 1, 2);
                        $tipo = substr($activo["placa"], 3, 3);
    
                        $sql_meses = "SELECT deprecia FROM actipo WHERE niveldos = '$clase' AND niveluno = '$grupo' AND codigo = '$tipo'";
                        $meses = $this->select($sql_meses);
                        
                        if ($meses["deprecia"] > 0) {
                            $valueDeprecionMensual = round(($activo["valor"] / $meses["deprecia"]), 2);
    
                            $fechaInicial = new DateTime($activo["fechact"]);
                            $diferencia = $fechaInicial->diff($fechaFinal);
                            $mesesDiferencia = ($diferencia->y * 12) + $diferencia->m;
                            $valorADepreciar = round(($mesesDiferencia * $valueDeprecionMensual), 2);
                            $mesesDiferencia = $mesesDiferencia > $meses["deprecia"] ? $meses["deprecia"] : $mesesDiferencia;
                            $valorADepreciar = $valorADepreciar >= $activo["valor"] ? $activo["valor"] : $valorADepreciar;
                            $saldo = round(($activo["valor"] - $valorADepreciar), 2);
                            
                            $arrayTemp["placa"] = $activo["placa"];
                            $arrayTemp["nombre"] = $activo["nombre"];
                            $arrayTemp["fechaIngreso"] = $activo["fechact"];
                            $arrayTemp["valorActivo"] = $activo["valor"];
                            $arrayTemp["meses"] = $meses["deprecia"];
                            $arrayTemp["valorDepreciarMensual"] = $valueDeprecionMensual;
                            $arrayTemp["mesesADepreciar"] = $mesesDiferencia;
                            $arrayTemp["valorADepreciar"] = $valorADepreciar;
                            $arrayTemp["saldo"] = $saldo;
                            array_push($data, $arrayTemp);
                        }
                    }
                }
    
                $arrResponse = array("status"=>true, "activos"=>$data);
            }

            echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
            die();
        }
        
        public function save_create() {
            if(!empty($_SESSION)) {
                $consecutivo = $_POST["consecutivo"];
                $fecha = $_POST["fecha"];
                $cuentaDebito = $_POST["cuenta"];
                $depreciaHasta = $_POST["depreciaHasta"]."-01";
                $fechaComoEntero = strtotime($depreciaHasta);
                $vigencia = date("Y", $fechaComoEntero);
                $mes = intval(date("m", $fechaComoEntero));
                $activos = json_decode($_POST['activos'], true);
                $sql_entidad = "SELECT nit FROM configbasica";
                $entidad = $this->select($sql_entidad);

                if (checkBlock("$_SESSION[cedulausu]", "$fecha")) {

                    //save cabeceras
                    $sql_deprecia = "INSERT INTO acti_deprecia_inicial (codigo, fecha, nombre, vigencia, estado) VALUES (?, ?, ?, ?, ?)";
                    $value_deprecia = [$consecutivo, "$fecha", "Depreciación inicial hasta el mes $mes del año $vigencia", "$vigencia", "S"];

                    $sql_comprobante = "INSERT INTO comprobante_cab (numerotipo, tipo_comp, fecha, concepto, total, estado) VALUES (?, ?, ?, ?, ?, ?)";
                    $value_comprobante = [$consecutivo, 22, "$fecha", "Depreciación inicial hasta el mes $mes del año $vigencia", 0, 1];

                    $this->insert($sql_deprecia, $value_deprecia);
                    $this->insert($sql_comprobante, $value_comprobante);

                    foreach ($activos as $activo) {
                        $tipo = substr($activo['placa'],0,6);
                        $nombre = strClean(replaceChar($activo["nombre"]));

                        $sql_cuentas = "SELECT cuenta_credito, centro_costos FROM acti_depreciacionactivos_det WHERE tipo = $tipo";
                        $cuentas = $this->select($sql_cuentas);
        
                        $sql_deprecia_det = "INSERT INTO acti_deprecia_inicial_det (codigo, placa, nombre, fechact, valor, mesesdeptot, valdepmes, mesesdepdic, valdepdic, saldo, estado) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
                        $value_deprecia_det = [
                            $consecutivo, 
                            "$activo[placa]",
                            "$nombre",
                            "$activo[fechaIngreso]",
                            $activo["valorActivo"],
                            $activo["meses"],
                            $activo["valorDepreciarMensual"],
                            $activo["mesesADepreciar"],
                            $activo["valorADepreciar"],
                            $activo["saldo"],
                            'S'
                        ];

                        $sql_det_cont = "INSERT INTO comprobante_det (id_comp, cuenta, tercero, centrocosto, detalle, valdebito, valcredito, estado, vigencia, tipo_comp, numerotipo, numacti) 
                        VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
                        
                        $value_debito = [
                            "22 $consecutivo",
                            "$cuentaDebito",
                            "$entidad[nit]",
                            "$cuentas[centro_costos]",
                            "Depreciacion inicial del activo $activo[placa] $nombre",
                            $activo["valorADepreciar"],
                            0,
                            "1",
                            "$vigencia",
                            22,
                            $consecutivo,
                            "$activo[placa]"
                        ];
            
                        $value_credito = [
                            "22 $consecutivo",
                            "$cuentas[cuenta_credito]",
                            "$entidad[nit]",
                            "$cuentas[centro_costos]",
                            "Depreciacion inicial del activo $activo[placa] $nombre",
                            0,
                            $activo["valorADepreciar"],
                            "1",
                            "$vigencia",
                            22,
                            $consecutivo,
                            "$activo[placa]"
                        ];

                        $this->insert($sql_deprecia_det, $value_deprecia_det);
                        $this->insert($sql_det_cont, $value_debito);
                        $this->insert($sql_det_cont, $value_credito);
                    }

                    $arrResponse = array("status"=>true);
                }
                else {
                    $arrResponse = array("status"=>true, "msgError"=>"Fecha bloqueada");
                }
            }
            echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
            die();
        }

        public function get_data_search() {
            if (!empty($_SESSION)) {

                $sql = "SELECT codigo, fecha, nombre, estado FROM acti_deprecia_inicial ORDER BY codigo DESC";
                $depreciaciones = $this->select_all($sql);

                $sql_bloqueo = "SELECT valor_final AS fecha FROM dominios dom WHERE dom.nombre_dominio = 'PERMISO_MODIFICA_DOC' AND dom.valor_inicial = '$_SESSION[cedulausu]' ORDER BY valor_final DESC";
                $request = $this->select($sql_bloqueo);

                $arrResponse = array("status"=>true, "fecha_bloqueo"=>$request["fecha"], "depreciaciones"=>$depreciaciones);
            }
            echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
            die();
        }

        public function anula_depreciacion() {
            if (!empty($_SESSION)) {
                $consecutivo = $_POST["consecutivo"];
                $fecha = $_POST["fecha"];

                if (checkBlock("$_SESSION[cedulausu]", "$fecha")) {

                    $update_depreciacion = "UPDATE acti_deprecia_inicial SET estado = ? WHERE codigo = $consecutivo";
                    $update_contabilidad = "UPDATE comprobante_cab SET estado = ? WHERE tipo_comp = 22 AND numerotipo = $consecutivo";

                    $this->update($update_depreciacion, ["N"]);
                    $this->update($update_contabilidad, ["0"]);

                    $arrResponse = array("status"=>true);
                }
                else {
                    $arrResponse = array("status"=>false, "msgError"=>"Fecha bloqueada");
                }
            }
            echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
            die();
        }

        public function get_data_visualize() {
            if (!empty($_SESSION)) {
                $consecutivo = $_POST["consecutivo"];

                $sql_depreciacion = "SELECT codigo, fecha, nombre, estado FROM acti_deprecia_inicial WHERE codigo = $consecutivo";
                $sql_depreciacion_det = "SELECT placa, nombre, fechact, valor, mesesdeptot, valdepmes, mesesdepdic, valdepdic, saldo FROM acti_deprecia_inicial_det WHERE codigo = $consecutivo";
                $depreciaciones = $this->select($sql_depreciacion);
                $depreciaciones_det = $this->select_all($sql_depreciacion_det);
                $arrResponse = array("status"=>true, "dataCab"=>$depreciaciones, "dataDet"=>$depreciaciones_det);
            }
            echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
            die();
        }
    }

    if($_POST){

        $obj = new depreInicialModel();

        switch ($_POST["action"]) {
            case 'get_data_create':
                $obj->data_create();
            break;

            case 'data_activos_create':
                $obj->search_activos();
            break;

            case 'save_create':
                $obj->save_create();
            break;

            case 'get_data_search':
                $obj->get_data_search();
            break;

            case 'anula_depreciacion':
                $obj->anula_depreciacion();
            break;

            case 'get_data_visualize':
                $obj->get_data_visualize();
            break;
        }
    }

    header("Content-type: application/json");
    die();