<?php
    // require '../../../funcionesSP.inc.php';
    require_once '../../../Librerias/core/Helpers.php';
    require_once '../../../Librerias/core/Mysql.php';

    header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=utf8");
    session_start();

    $out = array('error' => false);

    class visualizar extends Mysql {
        
        public function getData(){
            if(!empty($_SESSION)){
                $consecutivo = $_POST["consecutivo"];

                $sql_data = "SELECT * FROM acti_retiro WHERE consecutivo = $consecutivo";
                $dataCab = $this->select($sql_data);

                $sql_dependencias = "SELECT codarea, nombrearea FROM planacareas WHERE estado = 'S'";
                $dependencias = $this->select_all($sql_dependencias);

                $sql_ubicaciones = "SELECT id_cc, nombre FROM actiubicacion WHERE estado = 'S'";
                $ubicaciones = $this->select_all($sql_ubicaciones);

                $sql_det = "SELECT det.placa, 
                det.nombre, 
                det.fecha_compra, 
                det.fecha_ingreso, 
                det.dependencia, 
                det.ubicacion, 
                det.responsable, 
                det.valor_activo, 
                det.valor_depreciado, 
                det.valor_a_depreciar, 
                det.estado_activo, 
                det.motivo, 
                det.destino,
                CONCAT(t.nombre1, ' ', t.nombre2, ' ', t.apellido1, ' ', t.apellido2) AS nombreTercero,
                t.razonsocial,
                t.tipodoc
                FROM acti_retiro_det AS det INNER JOIN terceros AS t ON det.responsable = t.cedulanit
                WHERE consecutivo = $consecutivo";
                $dataDet = $this->select_all($sql_det);
                
                foreach ($dataDet as $key => $activo) {

                    if ($activo["tipodoc"] == 31) {
                        $dataDet[$key]["name"] = $activo["razonsocial"];
                    }
                    else {
                        $dataDet[$key]["name"] = $activo["nombreTercero"];
                    }
        
                    foreach ($dependencias as $dependencia) {
                        $indice = array_search($activo["dependencia"], $dependencia);

                        if ($indice) {
                            $dataDet[$key]["name_dependencia"] = $dependencia["nombrearea"];
                            break;
                        }
                        else {
                            $dataDet[$key]["name_dependencia"] = "";
                        }
                    }

                    foreach ($ubicaciones as $ubicacion) {
                        $indice = array_search($activo["ubicacion"], $ubicacion);

                        if ($indice) {
                            $dataDet[$key]["name_ubicacion"] = $ubicacion["nombre"];
                            break;
                        }
                        else {
                            $dataDet[$key]["name_ubicacion"] = "";
                        }
                    }
                }
    
                $arrResponse = array("status"=>true, "dataCab"=>$dataCab, "dataDet"=>$dataDet);
            }
            echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
            die();
        }
    }

    if($_POST){
        $obj = new visualizar();
        if($_POST['action'] == "getData"){
            $obj->getData();
        }
    }

    header("Content-type: application/json");
    echo json_encode($out);
    die();