const URL = 'activos_fijos/retirarActivo/visualizar/acti-visualizarRetiro.php';
const URLPDF = 'acti-visualizarRetiroPdf.php';

var app = new Vue({
    el:"#myapp",
    data() {
        return {
            isLoading:false,
            id: 0,
            dataCab: [],
            dataDet: [],
        }
    },
    mounted() {
        this.getData();
    },
    methods: {
        getData: async function(){
            this.id = new URLSearchParams(window.location.search).get('id');

            const formData = new FormData();
            formData.append("action","getData");
            formData.append("consecutivo",this.id);
            const response = await fetch(URL,{method:"POST",body:formData});
            const objData = await response.json();

            this.dataCab = objData.dataCab;
            this.dataDet = objData.dataDet;
        },

        pdf: function() {
            const form = document.createElement("form");
            form.method = "post";
            form.target = "_blank";
            form.action = URLPDF;       
            
            function addField(name,value){
                const input = document.createElement("input");
                input.type="hidden";
                input.name=name;
                input.value = value;
                form.appendChild(input);
            }
         
            addField("dataCab", JSON.stringify(this.dataCab));
            addField("dataDet", JSON.stringify(this.dataDet));
        
            document.body.appendChild(form);
            form.submit();
            document.body.removeChild(form);
        },

        formatNumero: function(valor){
            return new Intl.NumberFormat("en-US", {style: "currency", currency: "USD"}).format(valor);      
        },

        formatFecha: function(fecha) {
            // Separar la fecha en componentes
            const parts = fecha.split('-');
            if (parts.length !== 3) {
                throw new Error('Formato de fecha inválido. Use Y-m-d.');
            }
            const year = parts[0];
            const month = parts[1];
            const day = parts[2];
        
            // Retornar la fecha en el formato deseado
            return `${day}/${month}/${year}`;
        },
    },
    computed:{

    }
})
