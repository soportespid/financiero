<?php
    require_once '../../../Librerias/core/Helpers.php';
    require_once '../../../Librerias/core/Mysql.php';

    header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=utf8");
    session_start();

    class retiraActivo extends Mysql {
        
        public function getData(){
            if(!empty($_SESSION)){
                $data = [];
                $consecutivo = searchConsec("acti_retiro", "consecutivo");
                
                $sql_activos = "SELECT det.placa, 
                det.nombre AS nombre_act, 
                det.fechacom, 
                det.fechact, 
                det.area, 
                det.ubicacion, 
                det.valor, 
                resp.tercero,
                CONCAT(t.nombre1, ' ', t.nombre2, ' ', t.apellido1, ' ', t.apellido2) AS nombre,
                t.razonsocial,
                t.tipodoc
                FROM acticrearact_det AS det INNER JOIN acticrearact_det_responsable AS resp ON det.placa = resp.placa INNER JOIN terceros AS t ON resp.tercero = t.cedulanit
                WHERE resp.estado = 'S' AND det.estado = 'S' ORDER BY CONVERT(codigo, INT) ASC";
                $activos = $this->select_all($sql_activos);

                $sql_dependencias = "SELECT codarea, nombrearea FROM planacareas WHERE estado = 'S'";
                $dependencias = $this->select_all($sql_dependencias);

                $sql_ubicaciones = "SELECT id_cc, nombre FROM actiubicacion WHERE estado = 'S'";
                $ubicaciones = $this->select_all($sql_ubicaciones);

                $sql_comprobante = "SELECT numacti AS placa, SUM(valdebito) AS valorDepreciado FROM comprobante_det WHERE (tipo_comp = 22 OR tipo_comp = 78) AND valdebito != 0 GROUP BY numacti";
                $comprobantes = $this->select_all($sql_comprobante);

                foreach ($activos as $activo) {
                    $arrayTemp = [];

                    $arrayTemp["placa"] = $activo["placa"];
                    $arrayTemp["nombre"] = $activo["nombre_act"];
                    $arrayTemp["fechaCompra"] = $activo["fechacom"];
                    $arrayTemp["fechaIngreso"] = $activo["fechact"];
                    $arrayTemp["documento"] = $activo["tercero"];
                    $arrayTemp["valorActivo"] = round($activo["valor"], 2);
                    
                    if ($activo["tipodoc"] == 31) {
                        $arrayTemp["name"] = $activo["razonsocial"];
                    }
                    else {
                        $arrayTemp["name"] = $activo["nombre"];
                    }
                    
                    foreach ($dependencias as $dependencia) {
                        $indice = array_search($activo["area"], $dependencia);

                        if ($indice) {
                            $arrayTemp["cod_dependencia"] = $dependencia["codarea"];
                            $arrayTemp["dependencia"] = $dependencia["nombrearea"];
                            break;
                        }
                        else {
                            $arrayTemp["cod_dependencia"] = "";
                            $arrayTemp["dependencia"] = "";
                        }
                    }

                    foreach ($ubicaciones as $ubicacion) {
                        $indice = array_search($activo["ubicacion"], $ubicacion);

                        if ($indice) {
                            $arrayTemp["cod_ubicacion"] = $ubicacion["id_cc"];
                            $arrayTemp["ubicacion"] = $ubicacion["nombre"];
                            break;
                        }
                        else {
                            $arrayTemp["cod_ubicacion"] = "";
                            $arrayTemp["ubicacion"] = "";
                        }
                    }

                    foreach ($comprobantes as $comprobante) {
                        $indice = array_search($activo["placa"], $comprobante);

                        if ($indice) {
                            $arrayTemp["valorDepreciado"] = round($comprobante["valorDepreciado"], 2);
                            $arrayTemp["valorADepreciar"] = round($activo["valor"] - $comprobante["valorDepreciado"], 2);
                            break;
                        }
                        else {
                            $arrayTemp["valorDepreciado"] = 0;
                            $arrayTemp["valorADepreciar"] = 0;
                        }
                    }

                    array_push($data, $arrayTemp);
                }

                $arrResponse = array("status"=>true, "consecutivo"=>$consecutivo, "data"=>$data);
            }
            echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
            die();
        }

        public function save() {
            if (!empty($_SESSION)) {
                $consecutivo = $_POST["consecutivo"];
                $fecha = $_POST["fecha"];
                $descripcion = strClean(replaceChar($_POST["descripcion"]));
                $activos = json_decode($_POST['activos'], true);
                $fechaComoEntero = strtotime($fecha);
                $vigencia = date("Y", $fechaComoEntero);

                $sql_entidad = "SELECT nit FROM configbasica";
                $entidad = $this->select($sql_entidad);

                if (checkBlock("$_SESSION[cedulausu]", "$fecha")) {
                    $sql_retiro_cab = "INSERT INTO acti_retiro (consecutivo, fecha, descripcion, estado) VALUES (?, ?, ?, ?)";
                    $sql_comprobante_cab = "INSERT INTO comprobante_cab (numerotipo, tipo_comp, fecha, concepto, estado) VALUES (?, ?, ?, ?, ?)";
    
                    $value_retiro_cab = [
                        $consecutivo,
                        "$fecha",
                        "$descripcion",
                        "S"
                    ];
    
                    $value_comprobante_cab = [
                        $consecutivo,
                        77,
                        "$fecha",
                        "$descripcion",
                        1
                    ];
    
                    $this->insert($sql_retiro_cab, $value_retiro_cab);
                    $this->insert($sql_comprobante_cab, $value_comprobante_cab);
    
                    foreach ($activos as $activo) {
                        $tipo = substr($activo['placa'],0,6);
    
                        $sql_cuentas_depreciacion = "SELECT cuenta_credito FROM acti_depreciacionactivos_det WHERE tipo = '$tipo'";
                        $cuentas_depreciacion = $this->select($sql_cuentas_depreciacion);
    
                        $sql_cuentas_activos = "SELECT cuenta_activo, cuenta_retiro, centro_costos FROM acti_activos_det WHERE tipo = '$tipo'";
                        $cuentas_activo = $this->select($sql_cuentas_activos);
    
                        $sql_retiro_det = "INSERT INTO acti_retiro_det (consecutivo, placa, nombre, fecha_compra, fecha_ingreso, dependencia, ubicacion, responsable, valor_activo, valor_depreciado, valor_a_depreciar, estado_activo, motivo, destino) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
    
                        $values_retiro_det = [
                            $consecutivo,
                            $activo["placa"],
                            strClean(replaceChar($activo["nombre"])),
                            "$activo[fechaCompra]",
                            "$activo[fechaIngreso]",
                            "$activo[cod_dependencia]",
                            "$activo[cod_ubicacion]",
                            "$activo[documento]",
                            $activo["valorActivo"],
                            $activo["valorDepreciado"],
                            $activo["valorADepreciar"],
                            "$activo[estado]",
                            "$activo[motivo]",
                            "$activo[destino]"
                        ];
                        $this->insert($sql_retiro_det, $values_retiro_det);
    
                        $sql_estado = "UPDATE acticrearact_det SET estado = ? WHERE placa = '$activo[placa]'";
                        $value_estado = ["N"];

                        $sql_estado_tercero = "UPDATE acticrearact_det_responsable SET estado = ? WHERE placa = '$activo[placa]'";
                        
                        $this->update($sql_estado, $value_estado);
                        $this->update($sql_estado_tercero, $value_estado);
    
                        $sql_comprobante_det = "INSERT INTO comprobante_det (id_comp, cuenta, tercero, centrocosto, detalle, valdebito, valcredito, estado, vigencia, tipo_comp, numerotipo, numacti) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
                        
                        if ($activo["valorADepreciar"] <> 0) {
                            $value_debito01 = [
                                "77 $consecutivo",
                                "$cuentas_activo[cuenta_activo]",
                                "$entidad[nit]",
                                "$cuentas_activo[centro_costos]",
                                "Retiro del activo $activo[placa] por motivo $activo[motivo]",
                                $activo["valorDepreciado"],
                                0,
                                1,
                                "$vigencia",
                                77,
                                $consecutivo,
                                "$activo[placa]"
                            ];
                            
                            $value_debito02 = [
                                "77 $consecutivo",
                                "$cuentas_activo[cuenta_retiro]",
                                "$entidad[nit]",
                                "$cuentas_activo[centro_costos]",
                                "Retiro del activo $activo[placa] por motivo $activo[motivo]",
                                $activo["valorADepreciar"],
                                0,
                                1,
                                "$vigencia",
                                77,
                                $consecutivo,
                                "$activo[placa]"
                            ];
    
                            $value_credito = [
                                "77 $consecutivo",
                                "$cuentas_depreciacion[cuenta_credito]",
                                "$entidad[nit]",
                                "$cuentas_activo[centro_costos]",
                                "Retiro del activo $activo[placa] por motivo $activo[motivo]",
                                0,
                                $activo["valorActivo"],
                                1,
                                "$vigencia",
                                77,
                                $consecutivo,
                                "$activo[placa]"
                            ];
    
                            $this->insert($sql_comprobante_det, $value_debito01);
                            $this->insert($sql_comprobante_det, $value_debito02);
                            $this->insert($sql_comprobante_det, $value_credito);
                        }
                        else {
                            $value_debito = [
                                "77 $consecutivo",
                                "$cuentas_activo[cuenta_activo]",
                                "$entidad[nit]",
                                "$cuentas_activo[centro_costos]",
                                "Retiro del activo $activo[placa] por motivo $activo[motivo]",
                                $activo["valorDepreciado"],
                                0,
                                1,
                                "$vigencia",
                                77,
                                $consecutivo,
                                "$activo[placa]"
                            ];
    
                            $value_credito = [
                                "77 $consecutivo",
                                "$cuentas_depreciacion[cuenta_credito]",
                                "$entidad[nit]",
                                "$cuentas_activo[centro_costos]",
                                "Retiro del activo $activo[placa] por motivo $activo[motivo]",
                                0,
                                $activo["valorDepreciado"],
                                1,
                                "$vigencia",
                                77,
                                $consecutivo,
                                "$activo[placa]"
                            ];
    
                            $this->insert($sql_comprobante_det, $value_debito);
                            $this->insert($sql_comprobante_det, $value_credito);
                        }
                    }

                    $arrResponse = array("status"=>true);
                }
                else {
                    $arrResponse = array("status"=>false,"");
                }
            }
            echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
            die();
        }
    }

    if($_POST){
        $obj = new retiraActivo();

        if($_POST['action'] == "getData"){
            $obj->getData();
        }
        else if ($_POST["action"] == "save") {
            $obj->save();
        }
    }

    header("Content-type: application/json");
    die();