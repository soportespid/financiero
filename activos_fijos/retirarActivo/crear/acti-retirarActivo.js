const URL = 'activos_fijos/retirarActivo/crear/acti-retirarActivo.php';

var app = new Vue({
    el:"#myapp",
    data() {
        return {
            isLoading:false,
            tipoMov: '101',
            consecutivo: 0,
            fecha: '',
            descripcion: '',
            estado: '',
            motivo: '',
            destino: '',
            activo: [],
            listadoActivos: [],
            listadoActivosCopy: [],
            retiroActivos: [],
            txtSearch: '',
            txtResultados: 0,
            modalActivos: false,
        }
    },
    mounted() {
        this.getData();
    },
    methods: {
        getData: async function(){
            const formData = new FormData();
            formData.append("action","getData");
            this.isLoading = true;
            const response = await fetch(URL,{method:"POST",body:formData});
            const objData = await response.json();
            this.consecutivo = objData.consecutivo;
            this.listadoActivos = objData.data;
            this.listadoActivosCopy = objData.data;
            this.txtResultados = this.listadoActivosCopy.length;
            this.isLoading = false;
        },

        searchData: async function() {
            let search = "";
            search = this.txtSearch.toLowerCase();
            
            this.listadoActivosCopy = [...this.listadoActivos.filter(e=>e.placa.toLowerCase().includes(search) || e.nombre.toLowerCase().includes(search) || e.name.toLowerCase().includes(search) )];
            this.txtResultados = this.listadoActivosCopy.length;
        },

        selecAct: function({...element}) {
            let values;
            values = this.retiroActivos.filter(function(e){
                return e.placa == element.placa}
            ).length>0?true:false;
            console.log(values);

            if (!values) {
                this.activo = element;
                this.txtSearch = "";
                this.listadoActivosCopy = [...this.listadoActivos];
                this.txtResultados = this.listadoActivosCopy.length;
                this.modalActivos = false;
            }
            else {
                Swal.fire("Error","Activo ya seleccionado","warning");
            }
        },

        addActive: function() {
            if (this.estado != "" && this.motivo != "" && this.destino != "" && this.activo != "") {
                let array = [];
                array = this.activo;
                array["estado"] = this.estado;
                array["motivo"] = this.motivo;
                array["destino"] = this.destino;

                this.retiroActivos.push(array);
                array = [];
                this.activo = [];
                this.estado = this.motivo = this.destino = "";
            }
            else {
                Swal.fire("Error","Todos los campos con * son obligatorios","error");
            }
        },

        deleteActive: function(index) {
            this.retiroActivos.splice(index, 1);
        },

        save: async function() {
            if (this.retiroActivos.length > 0 && this.consecutivo > 0 && this.fecha != "" && this.descripcion != "") {
                Swal.fire({
                    title:"¿Estás segur@ de guardar?",
                    text:"",
                    icon: 'warning',
                    showCancelButton:true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText:"Sí, guardar",
                    cancelButtonText:"No, cancelar"
                }).then(async function(result){
                    if(result.isConfirmed) {
                        const formData = new FormData();
                        formData.append("action","save");
                        formData.append("consecutivo",app.consecutivo);
                        formData.append("fecha", app.fecha)
                        formData.append("descripcion", app.descripcion);
                        formData.append("activos", JSON.stringify(app.retiroActivos));
        
                        app.isLoading = true;
                        const response = await fetch(URL,{method:"POST",body:formData});
                        const objData = await response.json();
                        app.isLoading = false;
                        if (objData.status == true) {
                            Swal.fire("Guardado","Retiro realizado con exito","success");
                            setTimeout(function(){
                                location.reload();
                            },2500);
                        }
                        else {
                            let msgError = response.data.msgError;
                            Swal.fire("Error",msgError,"warning");
                        }
                    }
                });
            }
            else {
                Swal.fire("Error","Todos los campos con * son obligatorios","error");
            }
        },

        formatNumero: function(valor){
            return new Intl.NumberFormat("en-US", {style: "currency", currency: "USD"}).format(valor);
            
        },

        formatFecha: function(fecha) {
            // Separar la fecha en componentes
            const parts = fecha.split('-');
            if (parts.length !== 3) {
                throw new Error('Formato de fecha inválido. Use Y-m-d.');
            }
            const year = parts[0];
            const month = parts[1];
            const day = parts[2];
        
            // Retornar la fecha en el formato deseado
            return `${day}/${month}/${year}`;
        },
    },
    computed:{

    }
})
