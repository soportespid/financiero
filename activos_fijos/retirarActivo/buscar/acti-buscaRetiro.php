<?php
    // require '../../../funcionesSP.inc.php';
    require_once '../../../Librerias/core/Helpers.php';
    require_once '../../../Librerias/core/Mysql.php';

    header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=utf8");
    session_start();

    $out = array('error' => false);

    class buscaRetiros extends Mysql {
        
        public function getData(){
            if(!empty($_SESSION)){
                $sql_data = "SELECT * FROM acti_retiro ORDER BY consecutivo DESC";
                $data = $this->select_all($sql_data);

                $sql_bloqueo = "SELECT valor_final AS fecha FROM dominios dom WHERE dom.nombre_dominio = 'PERMISO_MODIFICA_DOC' AND dom.valor_inicial = '$_SESSION[cedulausu]' ORDER BY valor_final DESC";
                $request = $this->select($sql_bloqueo);

                $arrResponse = array("status"=>true, "data"=>$data, "fechaBloqueo"=>$request["fecha"]);
            }
            echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
            die();
        }

        public function anulaDocument() {
            if(!empty($_SESSION)) {
                $consecutivo = $_POST["consecutivo"];
                $fecha = $_POST["fecha"];

                if (checkBlock("$_SESSION[cedulausu]", "$fecha")) {

                    $sql_acti_cab = "UPDATE acti_retiro SET estado = ? WHERE consecutivo = $consecutivo";
                    $this->update($sql_acti_cab, ['N']);
                    $sql_comprobante = "UPDATE comprobante_cab SET estado = ? WHERE tipo_comp = 77 AND numerotipo = $consecutivo";
                    $this->update($sql_comprobante, [0]);

                    $sql_acti_det = "SELECT placa FROM acti_retiro_det WHERE consecutivo = $consecutivo";
                    $activos = $this->select_all($sql_acti_det);

                    foreach ($activos as $activo) {
                        $update_estado = "UPDATE acticrearact_det SET estado = ? WHERE placa = '$activo[placa]'";
                        $this->update($update_estado, ['S']);
                    }

                    $arrResponse = array("status"=>true);
                }
                else {
                    $arrResponse = array("status"=>false,"msg"=>"Fecha bloqueada no puede realizar esta acción");
                }
            }
            echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
            die();
        }
    }

    if($_POST){
        $obj = new buscaRetiros();
        if($_POST['action'] == "getData"){
            $obj->getData();
        }
        else if ($_POST["action"] == "anular") {
            $obj->anulaDocument();
        }
    }
    
    // dep($obj->select_all('SELECT * FROM srv_acuerdo_cab ORDER BY codigo_acuerdo DESC'));

    header("Content-type: application/json");
    echo json_encode($out);
    die();