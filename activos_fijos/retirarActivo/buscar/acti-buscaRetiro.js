const URL = 'activos_fijos/retirarActivo/buscar/acti-buscaRetiro.php';


var app = new Vue({
    el:"#myapp",
    data() {
        return {
            isLoading:false,

            txtSearch: '',
            actaRetiros: [],
            actaRetirosCopy: [],
            txtResultados: 0,
            fechaBloqueo: '',
        }
    },
    mounted() {
        this.getData();
    },
    methods: {
        getData: async function(){

            const formData = new FormData();
            formData.append("action","getData");
            const response = await fetch(URL,{method:"POST",body:formData});
            const objData = await response.json();

            this.actaRetiros = objData.data;
            this.actaRetirosCopy = objData.data;
            this.txtResultados = this.actaRetirosCopy.length;
            this.fechaBloqueo = objData.fechaBloqueo;
        },

        searchData: async function() {
            let search = "";
            search = this.txtSearch.toLowerCase();
            
            this.actaRetirosCopy = [...this.actaRetiros.filter(e=>e.consecutivo.toLowerCase().includes(search))];
            this.txtResultados = this.actaRetirosCopy.length;
        },

        anulaDocument: async function(item, index) {
            Swal.fire({
                title:"¿Estás segur@ de anular?",
                text:"",
                icon: 'warning',
                showCancelButton:true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText:"Sí, anular",
                cancelButtonText:"No, cancelar"
            }).then(async function(result){
                if(result.isConfirmed) {

                    const formData = new FormData();
                    formData.append("action","anular");
                    formData.append("consecutivo", item.consecutivo);
                    formData.append("fecha", item.fecha);
                    const response = await fetch(URL,{method:"POST",body:formData});
                    const objData = await response.json();
    
                    if (objData.status == true) {
                        app.actaRetiros[index].estado = 'N';
                        app.actaRetirosCopy[index].estado = 'N';
                    }
                    else {
                        Swal.fire("Error",objData.msg,"warning");
                    }
                }
            });
        },

        formatNumero: function(valor){
            return new Intl.NumberFormat("en-US", {style: "currency", currency: "USD"}).format(valor);      
        },

        formatFecha: function(fecha) {
            // Separar la fecha en componentes
            const parts = fecha.split('-');
            if (parts.length !== 3) {
                throw new Error('Formato de fecha inválido. Use Y-m-d.');
            }
            const year = parts[0];
            const month = parts[1];
            const day = parts[2];
        
            // Retornar la fecha en el formato deseado
            return `${day}/${month}/${year}`;
        },
    },
    computed:{

    }
})
