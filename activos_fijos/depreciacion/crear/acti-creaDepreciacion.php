<?php
    require_once '../../../Librerias/core/Helpers.php';
    require_once '../../../Librerias/core/Mysql.php';

    header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=utf8");
    session_start();

    class crearDepreciacion extends Mysql {
        
        public function getData(){
            if(!empty($_SESSION)){
                
                $sql_consecutivo = "SELECT MAX(id_dep) AS consecutivo FROM actidepactivo_cab WHERE tipo_mov = '201'";
                $request = $this->select($sql_consecutivo);
                $consecutivo = $request["consecutivo"] + 1;
                echo json_encode($consecutivo,JSON_UNESCAPED_UNICODE);
            }
            die();
        }

        public function searchsActives() {
            if (!empty($_SESSION)) {
                $data = [];
                $fechaDepreciar = $_POST["fechaDepreciar"]."-01";
                $fechaComoEntero = strtotime($fechaDepreciar);
                $vigencia = date("Y", $fechaComoEntero);
                $mes = intval(date("m", $fechaComoEntero));
            
                $sql_valida_mes = "SELECT count(*) AS valor FROM actidepactivo_cab WHERE mes = '$mes' AND vigencia = '$vigencia' AND estado = 'S'";
                $request = $this->select($sql_valida_mes);

                if ($request["valor"] == 0) {

                    $sql_activos = "SELECT placa, nombre, fechact AS fechaCompra, valor AS valorActivo, bloque FROM acticrearact_det WHERE fechact <= '$fechaDepreciar' AND estado = 'S'";
                    $activos = $this->select_all($sql_activos);

                    $sql_comprobante = "SELECT numacti AS placa, SUM(valdebito) AS valorDepreciado FROM comprobante_det WHERE (tipo_comp = 22 OR tipo_comp = 78) AND valdebito != 0 GROUP BY numacti";
                    $comprobantes = $this->select_all($sql_comprobante);

                    foreach ($activos as $i => $activo) {
                        $arrayTemp = [];

                        $arrayTemp["placa"] = $activo["placa"];
                        $arrayTemp["nombre"] = $activo["nombre"];
                        $arrayTemp["fechaCompra"] = $activo["fechaCompra"];
                        $arrayTemp["bloque"] = $activo["bloque"];
                        $arrayTemp["valorActivo"] = round($activo["valorActivo"], 2);
    
                        foreach ($comprobantes as $j => $comprobante) {
                            $indice = array_search($activo["placa"], $comprobante);

                            if ($indice) {
                                $arrayTemp["valorDepreciado"] = round($comprobante["valorDepreciado"], 2);
                                break;
                            }
                            else {
                                $arrayTemp["valorDepreciado"] = 0;
                            }
                        }
                        
                        $arrayTemp["valorADepreciar"] = 0;
                        $arrayTemp["valorDepreciarMensual"] = 0;
                        array_push($data, $arrayTemp);
                    }
                    $arrResponse = array("status"=>true, "activos"=>$data);
                }
                else {
                    $arrResponse = array("status"=>false,"msg"=>"Este mes ya se le realizo una depreciación");
                }
            }
            echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
            die();
        }

        public function calcularValores() {
            if (!empty($_SESSION)) {
                $data = [];
                $activos = json_decode($_POST['activos'], true);

                foreach ($activos as $key => $activo) {
                    $arrayTemp = [];

                    $clase = substr($activo["placa"], 0, 1);
                    $grupo = substr($activo["placa"], 1, 2);
                    $tipo = substr($activo["placa"], 3, 3);

                    if ($activo["bloque"] == 1) {
                        $sql_tipo = "SELECT deprecia FROM actipo WHERE codigo = '$tipo' AND niveluno = '$grupo' AND niveldos = '$clase'";
                        $tipo = $this->select($sql_tipo);
                        $meses = intval($tipo["deprecia"]);
                    }
                    else {
                        $sql_meses = "SELECT nummesesdep FROM acticrearact_det WHERE placa = $activo[placa]";
                        $mesesDep = $this->select($sql_meses);       
                        $meses = intval($mesesDep["nummesesdep"]);
                    }

                    if ($meses > 0) {
                        $valorMensual = round($activo["valorActivo"] / $meses, 2);
                        $valorDepreciado = $activo["valorDepreciado"] + $valorMensual;
                    }
                    else {
                        $valorMensual = 0;
                        $valorDepreciado = $activo["valorDepreciado"] + $valorMensual;
                    }

                    if ($valorDepreciado > $activo["valorActivo"] && $meses > 0) {
                        $valorMensual = $activo["valorActivo"] - $activo["valorDepreciado"];
                    }

                    $arrayTemp["placa"] = $activo["placa"];
                    $arrayTemp["valorADepreciar"] = $activo["valorActivo"] - $activo["valorDepreciado"];
                    $arrayTemp["valorDepreciarMensual"] = $valorMensual;
                    array_push($data, $arrayTemp);
                }

                $arrResponse = array("status"=>true, "data"=>$data);

                echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
            }
            die();
        }

        public function save() {
            if (!empty($_SESSION)) {
                $activos = json_decode($_POST['activos'], true);
                $fechaDocumento = $_POST["fechaDocumento"];
                $fechaDepreciacion = $_POST["fechaDepreciacion"]."-01";
                $consecutivo = $_POST["consecutivo"];
                $fechaComoEntero = strtotime($fechaDepreciacion);
                $vigencia = date("Y", $fechaComoEntero);
                $mes = intval(date("m", $fechaComoEntero));

                $sql_entidad = "SELECT nit FROM configbasica";
                $entidad = $this->select($sql_entidad);

                if (checkBlock("$_SESSION[cedulausu]", "$fechaDocumento")) {
                    //save cabeceras
                    $sql_cab_depreciacion = "INSERT INTO actidepactivo_cab (id_dep, mes, vigencia, fecha, estado, tipo_mov) VALUES (?, ?, ?, ?, ?, ?)";
                    $values_cab_depreciacion = [$consecutivo, $mes, "$vigencia", "$fechaDocumento", "S", "201"];
                    $this->insert($sql_cab_depreciacion, $values_cab_depreciacion);

                    $sql_cab_contabilidad = "INSERT INTO comprobante_cab (numerotipo, tipo_comp, fecha, concepto, total, estado) VALUES (?, ?, ?, ?, ?, ?)";
                    $values_cab_contabilidad = [$consecutivo, 78, "$fechaDocumento", "DEPRECIACION DEL MES $mes del año $vigencia", 0, 1];
                    $this->insert($sql_cab_contabilidad, $values_cab_contabilidad);

                    //save detalles
                    foreach ($activos as $activo) {
                        if ($activo["valorDepreciarMensual"] > 0 && $activo["valorDepreciado"] < $activo["valorActivo"] ) {
                            $nombre = strClean(replaceChar($activo["nombre"]));
                            $tipo = substr($activo['placa'],0,6);
    
                            $sql_det_depreciacion = "INSERT INTO actidepactivo_det (id_dep, vigencia, mes, placa, fechact, nombre, valor, valord, valorad, valdep, estado, tipo_mov) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
                            $values_det_depreciacion = [$consecutivo, "$vigencia", $mes, "$activo[placa]", "$activo[fechaCompra]", "$nombre", $activo["valorActivo"], $activo["valorDepreciado"], $activo["valorADepreciar"], $activo["valorDepreciarMensual"], "S", "201"];
                            $this->insert($sql_det_depreciacion, $values_det_depreciacion);

                            $sql_cuentas = "SELECT cuenta_debito, cuenta_credito, centro_costos FROM acti_depreciacionactivos_det WHERE tipo = $tipo";
                            $cuentas = $this->select($sql_cuentas);
                            
                            $sql_det_cont = "INSERT INTO comprobante_det (id_comp, cuenta, tercero, centrocosto, detalle, valdebito, valcredito, estado, vigencia, tipo_comp, numerotipo, numacti) 
                            VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";

                            $value_debito = ["78 $consecutivo", "$cuentas[cuenta_debito]", "$entidad[nit]", "$cuentas[centro_costos]", "Depreciacion activo $activo[placa] $nombre", $activo["valorDepreciarMensual"], 0, "1", "$vigencia", 78, $consecutivo, "$activo[placa]"];
                            $this->insert($sql_det_cont, $value_debito);

                            $value_credito = ["78 $consecutivo", "$cuentas[cuenta_credito]", "$entidad[nit]", "$cuentas[centro_costos]", "Depreciacion activo $activo[placa] $nombre", 0, $activo["valorDepreciarMensual"], "1", "$vigencia", 78, $consecutivo, "$activo[placa]"];
                            $this->insert($sql_det_cont, $value_credito);

                            $sql_update = "UPDATE acticrearact_det SET fechaultdep = ? WHERE placa = $activo[placa]";
                            $value_update = ["$fechaDepreciacion"];
                            $this->update($sql_update, $value_update);
                        }
                    }
                    $arrResponse = array("status"=>true);
                }
                else {
                    $arrResponse = array("status"=>false,"msg"=>"Fecha de creacion del documento bloqueada");
                }
            }

            echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
            die();
        }
    }

    if($_POST){
        $obj = new crearDepreciacion();

        if($_POST['action'] == "getData"){
            $obj->getData();
        }
        else if ($_POST["action"] == "searchActives") {
            $obj->searchsActives();
        }
        else if ($_POST["action"] == "calculaValores") {
            $obj->calcularValores();
        }
        else if ($_POST["action"] == "save") {
            $obj->save();
        }
    }

    header("Content-type: application/json");
    die();