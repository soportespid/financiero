const URL = 'activos_fijos/depreciacion/crear/acti-creaDepreciacion.php';

var app = new Vue({
    el:"#myapp",
    data() {
        return {
            isLoading:false,
            consecutivo: 0,
            fecha: '',
            mes: '',
            activos: [],
        }
    },
    mounted() {
        this.getData();
    },
    methods: {
        getData: async function(){
            const formData = new FormData();
            formData.append("action","getData");
            const response = await fetch(URL,{method:"POST",body:formData});
            const objData = await response.json();

            this.consecutivo = objData;
        },

        searchActivos: async function() {

            if (this.fecha != "" &&  this.mes) {
                const formData = new FormData();
                formData.append("action","searchActives");
                formData.append("fechaDepreciar", this.mes);
                this.isLoading = true;
                const response = await fetch(URL,{method:"POST",body:formData});
                const objData = await response.json();
                this.isLoading = false;
                
                if (objData.status == true) {
                    this.activos = objData.activos;
                }
                else {
                    this.activos = [];
                    Swal.fire("Error",objData.msg,"error");
                }
            }
            else {
                Swal.fire("Error","Todos los campos con * son obligatorios","error");    
            }
        },

        calculaValores: async function() {
            if (this.activos.length > 0) {
                const formData = new FormData();
                formData.append("action","calculaValores");
                formData.append("activos", JSON.stringify(this.activos));
                this.isLoading = true;
                const response = await fetch(URL,{method:"POST",body:formData});
                const objData = await response.json();

                if (objData.status == true) {
                    this.activos.forEach(activo => {
                        
                        let values;

                        values = objData.data.filter(function(e){
                            return e.placa == activo.placa}
                        )[0];

                        activo.valorADepreciar = values.valorADepreciar;
                        activo.valorDepreciarMensual = values.valorDepreciarMensual;
                    });
                    this.isLoading = false;
                }
            }
            else {
                Swal.fire("Error","Primero debe buscar los activos","error");
            }
        },

        save: async function() {

            if (this.activos.length > 0) {
                Swal.fire({
                    title:"¿Estás segur@ de guardar?",
                    text:"",
                    icon: 'warning',
                    showCancelButton:true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText:"Sí, guardar",
                    cancelButtonText:"No, cancelar"
                }).then(async function(result){
                    if(result.isConfirmed) {
                        const formData = new FormData();
                        formData.append("action","save");
                        formData.append("activos", JSON.stringify(app.activos));
                        formData.append("fechaDocumento",app.fecha);
                        formData.append("fechaDepreciacion",app.mes);
                        formData.append("consecutivo",app.consecutivo);

                        app.isLoading = true;
                        const response = await fetch(URL,{method:"POST",body:formData});
                        const objData = await response.json();
                        app.isLoading = false;

                        if (objData.status == true) {
                            Swal.fire("Guardado","Depreciacion realizada con exito","success");
                            setTimeout(function(){
                                location.reload();
                            },2500);
                        }
                        else {
                            Swal.fire("Error",objData.msg,"warning");
                        }
                    }
                });
            }
            else {
                Swal.fire("Error","Primero debe buscar los activos y calcular los valores","error");
            }
        },

        formatNumero: function(valor){
            return new Intl.NumberFormat("en-US", {style: "currency", currency: "USD"}).format(valor);
            
        },
    },
    computed:{

    }
})
