const URL = "activos_fijos/depreciacion/visualizar/acti-visualizarDepreciacion.php";

var app = new Vue({
    el:"#myapp",
    data() {
        return {
            isLoading: false,
            id: 0,
            data: [],
            activos: [],
        }
    },
    mounted() {
        this.getData();
    },
    methods: {
        getData: async function(){
            this.id = new URLSearchParams(window.location.search).get('id');

            if (this.id > 0) {
                const formData = new FormData();
                formData.append("action","getData");
                formData.append("consecutivo",this.id);
                const response = await fetch(URL,{method:"POST",body:formData});
                const objData = await response.json();
    
                this.data = objData.cabecera;
                this.activos = objData.activos;
            }
        },

        formatNumero: function(valor){
            return new Intl.NumberFormat("en-US", {style: "currency", currency: "USD"}).format(valor);
            
        },
    },
    computed:{

    }
})
