<?php
    require_once '../../../Librerias/core/Helpers.php';
    require_once '../../../Librerias/core/Mysql.php';

    header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=utf8");
    session_start();

    class visualizarDepreciacion extends Mysql {
        
        public function getData(){
            if(!empty($_SESSION)){
                $consecutivo = $_POST["consecutivo"];
                $data = [];
                //actidepactivo_cab actidepactivo_det

                $sql_cab = "SELECT mes, vigencia, fecha FROM actidepactivo_cab WHERE id_dep = $consecutivo";
                $cab = $this->select($sql_cab);
                $data["consecutivo"] = $consecutivo;
                $data["fecha"] = $cab["fecha"];

                if ($data["mes"] < 10) {
                    $data["fechaDep"] = "$cab[vigencia]-0$cab[mes]";
                }
                else {
                    $data["fechaDep"] = "$cab[vigencia]-$cab[mes]";
                }

                $sql_det = "SELECT placa, nombre, fechact AS fechaCompra, valor AS valorActivo, valord AS valorDepreciado, valorad AS valorADepreciar, valdep AS valorDepreciarMensual FROM actidepactivo_det WHERE id_dep = $consecutivo ORDER BY id ASC";
                $activos = $this->select_all($sql_det);

                $arrResponse = array("status"=>true,"cabecera"=>$data,"activos"=>$activos);
            }
            echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
            die();
        }
    }

    if($_POST){
        $obj = new visualizarDepreciacion();

        if($_POST['action'] == "getData"){
            $obj->getData();
        }
    }

    header("Content-type: application/json");
    die();