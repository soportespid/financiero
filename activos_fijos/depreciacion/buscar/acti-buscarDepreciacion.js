const URL = 'activos_fijos/depreciacion/buscar/acti-buscarDepreciacion.php';


var app = new Vue({
    el:"#myapp",
    data() {
        return {
            isLoading:false,

            txtSearch: '',
            arrDepreciaciones: [],
            arrDepreciacionesCopy: [],
            txtResultados: 0,
            fechaBloqueo: '',
        }
    },
    mounted() {
        this.getData();
    },
    methods: {
        getData: async function(){

            const formData = new FormData();
            formData.append("action","getData");
            const response = await fetch(URL,{method:"POST",body:formData});
            const objData = await response.json();

            this.arrDepreciaciones = objData.data;
            this.arrDepreciacionesCopy = objData.data;
            this.txtResultados = this.arrDepreciacionesCopy.length;
            this.fechaBloqueo = objData.fechaBloqueo;
        },

        searchData: async function() {
            let search = "";
            search = this.txtSearch.toLowerCase();
            
            this.arrDepreciacionesCopy = [...this.arrDepreciaciones.filter(e=>e.consecutivo.toLowerCase().includes(search))];
            this.txtResultados = this.arrDepreciacionesCopy.length;
        },

        anulaDocument: async function(consecutivo, fecha, index) {
            if (consecutivo != "" && fecha != "") {
                Swal.fire({
                    title:"¿Estás segur@ de eliminar?",
                    text:"",
                    icon: 'warning',
                    showCancelButton:true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText:"Sí, eliminar",
                    cancelButtonText:"No, cancelar"
                }).then(async function(result){
                    if(result.isConfirmed) {

                        const formData = new FormData();
                        formData.append("action","anular");
                        formData.append("consecutivo", consecutivo);
                        formData.append("fecha", fecha);
                        const response = await fetch(URL,{method:"POST",body:formData});
                        const objData = await response.json();
        
                        if (objData.status == true) {
                            app.arrDepreciaciones.splice(index, 1);
                            app.arrDepreciacionesCopy = [...app.arrDepreciaciones];
                            Swal.fire("Eliminado","Depreciación eliminada con exito","success");
                        }
                        else {
                            Swal.fire("Error",objData.msg,"warning");
                        }
                    }
                });
            }
        },

        formatNumero: function(valor){
            return new Intl.NumberFormat("en-US", {style: "currency", currency: "USD"}).format(valor);      
        },
    },
    computed:{

    }
})
