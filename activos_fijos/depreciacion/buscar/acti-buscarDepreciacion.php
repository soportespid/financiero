<?php
    // require '../../../funcionesSP.inc.php';
    require_once '../../../Librerias/core/Helpers.php';
    require_once '../../../Librerias/core/Mysql.php';

    header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=utf8");
    session_start();

    $out = array('error' => false);

    class buscaDepreciaciones extends Mysql {
        
        public function getData(){
            if(!empty($_SESSION)){
                $data = [];
                $sql = "SELECT id_dep, mes, vigencia, fecha, estado FROM actidepactivo_cab ORDER BY id_dep DESC";
                $depreciaciones = $this->select_all($sql);

                foreach ($depreciaciones as $depreciacion) {
                    $arrayTemp = [];

                    $sql_mes = "SELECT nombre FROM meses WHERE id = $depreciacion[mes]";
                    $mes = $this->select($sql_mes);

                    $arrayTemp["consecutivo"] = $depreciacion["id_dep"];
                    $arrayTemp["fecha"] = $depreciacion["fecha"];
                    $arrayTemp["mes"] = $mes["nombre"];
                    $arrayTemp["vigencia"] = $depreciacion["vigencia"];
                    $arrayTemp["descripcion"] = "Depreciacion del mes $mes[nombre] del año $depreciacion[vigencia]";

                    if ($depreciacion["estado"] == 'S') {
                        $estado = "Activo";
                    }
                    else if ($depreciacion["estado"] == 'R') {
                        $estado = "Reversado";
                    }
                    else if ($depreciacion["estado"] == 'N') {
                        $estado = "Anulado";
                    }

                    $arrayTemp["estado"] = $estado;
                    array_push($data, $arrayTemp);
                }

                $sql_bloqueo = "SELECT valor_final AS fecha FROM dominios dom WHERE dom.nombre_dominio = 'PERMISO_MODIFICA_DOC' AND dom.valor_inicial = '$_SESSION[cedulausu]' ORDER BY valor_final DESC";
                $request = $this->select($sql_bloqueo);

                $arrResponse = array("status"=>true,"data"=>$data,"fechaBloqueo"=>$request["fecha"]);
            }
            echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
            die();
        }

        public function anulaDocument() {
            if(!empty($_SESSION)) {
                $consecutivo = $_POST["consecutivo"];
                $fecha = $_POST["fecha"];

                if (checkBlock("$_SESSION[cedulausu]", "$fecha")) {

                    $sql_acti_cab = "DELETE FROM actidepactivo_cab WHERE id_dep = $consecutivo";
                    $sql_acti_det = "DELETE FROM actidepactivo_det WHERE id_dep = $consecutivo";
                    $sql_cont_cab = "DELETE FROM comprobante_cab WHERE tipo_comp = 78 AND numerotipo = $consecutivo";
                    $sql_cont_det = "DELETE FROM comprobante_det WHERE tipo_comp = 78 AND numerotipo = $consecutivo";

                    $this->delete($sql_acti_cab);
                    $this->delete($sql_acti_det);
                    $this->delete($sql_cont_cab);
                    $this->delete($sql_cont_det);

                    $arrResponse = array("status"=>true);
                }
                else {
                    $arrResponse = array("status"=>false,"msg"=>"Fecha bloqueada no puede realizar esta acción");
                }
            }
            echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
            die();
        }
    }

    if($_POST){
        $obj = new buscaDepreciaciones();
        if($_POST['action'] == "getData"){
            $obj->getData();
        }
        else if ($_POST["action"] == "anular") {
            $obj->anulaDocument();
        }
    }
    
    // dep($obj->select_all('SELECT * FROM srv_acuerdo_cab ORDER BY codigo_acuerdo DESC'));

    header("Content-type: application/json");
    echo json_encode($out);
    die();