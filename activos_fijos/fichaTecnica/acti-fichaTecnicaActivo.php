<?php
	require_once '../../comun.inc';
    require '../../funciones.inc';
    require '../../conversor.php';
    session_start();

	$linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");

    $out = array('error' => false);

    $action = "show";

    if(isset($_GET['action'])){
        $action = $_GET['action'];
    }

    if ($action == "datosFormulario") {

        $placa = $_POST['placa'];

        $sqlActivos = "SELECT DET.placa, DET.nombre, CAB.fechareg, DET.fechacom, DET.fechact, DET.clasificacion, DET.grupo, DET.tipo, DET.referencia, DET.modelo, DET.serial, DET.prototipo, DET.area, DET.ubicacion, DET.cc, DET.dispoact, DET.estadoactivo, DET.foto FROM acticrearact_det AS DET INNER JOIN acticrearact AS CAB ON CAB.codigo = DET.codigo WHERE DET.placa = '$placa' AND DET.estado='S'";
        $rowActivos = mysqli_fetch_assoc(mysqli_query($linkbd, $sqlActivos));

        $fechaRegistro = date("d/m/Y", strtotime($rowActivos['fechareg']));
        $fechaCompra = date("d/m/Y", strtotime($rowActivos['fechacom']));
        $fechaActivacion = date("d/m/Y", strtotime($rowActivos['fechact']));

        $sqlValorActi = "SELECT COALESCE(SUM(valdebito),0) FROM comprobante_det WHERE tipo_comp = '70' AND numacti = '$rowActivos[placa]'";
        $rowValorActi = mysqli_fetch_row(mysqli_query($linkbd, $sqlValorActi));

        $valorActivo = $rowValorActi[0];

        $sqlDep = "SELECT COALESCE(SUM(valcredito),0) FROM comprobante_det WHERE tipo_comp = '78' AND numacti = '$rowActivos[placa]'";
        $rowDep = mysqli_fetch_row(mysqli_query($linkbd, $sqlDep));

        $valorDepreciado = $rowDep[0];

        $valorCorreccion = 0;

        $sqlClase = "SELECT nombre FROM actipo WHERE codigo = '$rowActivos[clasificacion]' AND niveluno = '0' AND estado = 'S'";
        $rowClase = mysqli_fetch_row(mysqli_query($linkbd, $sqlClase));

        $clase = $rowActivos['clasificacion'] . " - " . $rowClase[0];

        $sqlGrupo = "SELECT nombre FROM actipo WHERE codigo = '$rowActivos[grupo]' AND niveluno='$rowActivos[clasificacion]' AND estado = 'S'";
        $rowGrupo = mysqli_fetch_row(mysqli_query($linkbd, $sqlGrupo));

        $grupo = $rowActivos['grupo'] . " - " . $rowGrupo[0];

        $sqlTipo = "SELECT nombre FROM actipo WHERE codigo = '$rowActivos[tipo]' AND niveluno = '$rowActivos[grupo]' AND niveldos = '$rowActivos[clasificacion]' AND estado = 'S'";
        $rowTipo = mysqli_fetch_row(mysqli_query($linkbd, $sqlTipo));

        $tipo = $rowActivos['tipo'] . " - " . $rowTipo[0];

        $sqlPrototipo = "SELECT nombre FROM acti_prototipo WHERE estado = 'S' AND id = '$rowActivos[prototipo]'";
        $rowPrototipo = mysqli_fetch_row(mysqli_query($linkbd, $sqlPrototipo));

        $prototipo = $rowActivos['prototipo'] . " - " . $rowPrototipo[0];

        $sqlArea = "SELECT nombrearea FROM planacareas WHERE estado = 'S' AND codarea = '$rowActivos[area]'";
        $rowArea = mysqli_fetch_row(mysqli_query($linkbd, $sqlArea));

        $area = $rowActivos['area'] . " - " . $rowArea[0];

        $sqlUbicacion = "SELECT nombre FROM actiubicacion WHERE estado = 'S' AND id_cc = '$rowActivos[ubicacion]'";
        $rowUbicacion = mysqli_fetch_row(mysqli_query($linkbd, $sqlUbicacion));

        $ubicacion = $rowActivos['ubicacion'] . " - " . $rowUbicacion[0];

        $sqlCentroCosto = "SELECT nombre FROM centrocosto WHERE estado = 'S' AND id_cc = '$rowActivos[cc]'";
        $rowCentroCosto = mysqli_fetch_row(mysqli_query($linkbd, $sqlCentroCosto));

        $cc = $rowActivos['cc'] . " - " . $rowCentroCosto[0];

        $sqlDisposicion = "SELECT nombre FROM acti_disposicionactivos WHERE estado = 'S' AND id = '$rowActivos[dispoact]'";
        $rowDisposicion = mysqli_fetch_row(mysqli_query($linkbd, $sqlDisposicion));

        $disposicion = $rowActivos['dispoact'] . " - " . $rowDisposicion[0];

        $sqlTercero = "SELECT tercero FROM acticrearact_det_responsable WHERE placa = '$placa'";
        $rowTercero = mysqli_fetch_row(mysqli_query($link,$sqlrTercero));

        $responsable = buscatercero($rowTercero[0]);
        $responsable = $rowTercero[0] . " - " . $responsable;

        $out['nombre'] = $rowActivos['nombre'];
        $out['fechaRegistro'] = $fechaRegistro;
        $out['fechaCompra'] = $fechaCompra;
        $out['fechaActivacion'] = $fechaActivacion;
        $out['valorActivo'] = $valorActivo;
        $out['valorDepreciado'] = $valorDepreciado;
        $out['valorCorreccion'] = $valorCorreccion;
        $out['referencia'] = $rowActivos['referencia'];
        $out['modelo'] = $rowActivos['modelo'];
        $out['serial'] = $rowActivos['serial'];
        $out['clase'] = $clase;
        $out['grupo'] = $grupo;
        $out['tipo'] = $tipo;
        $out['prototipo'] = $prototipo;
        $out['area'] = $area;
        $out['ubicacion'] = $ubicacion;
        $out['cc'] = $cc;
        $out['disposicion'] = $disposicion;
        $out['estadoActivo'] = $rowActivos['estadoactivo'];
        $out['responsable'] = $responsable;
        $out['foto'] = $rowActivos['foto'];
    }

    if ($action == 'cargaImagen') {

        $placa = $_POST['placa'];
        $foto = $_FILES['imagen']['name'];    
        $direccion = "../../informacion/proyectos/temp/";
        $targetFile = $direccion.basename($foto);

        move_uploaded_file($_FILES['imagen']['tmp_name'], $targetFile);

        $query = "UPDATE acticrearact_det SET foto = '$foto' WHERE placa = '$placa'";
        mysqli_query($linkbd, $query);

        $out['insertaBien'] = true;
    }

    header("Content-type: application/json");
    echo json_encode($out);
    die();