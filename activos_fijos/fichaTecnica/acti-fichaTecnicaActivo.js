var app = new Vue({
    el: '#myapp',
	data:{

        error: '',
        loading: false,
        
        placa: '',
        nombre: '',
        fechaRegistro: '',
        fechaCompra: '',
        fechaActivacion: '',
        valorActivo: '',
        valorDepreciado: '',
        valorCorreccion: '',
        referencia: '',
        modelo: '',
        serial: '',
        clase: '',
        grupo: '',
        tipo: '',
        prototipo: '',
        area: '',
        ubicacion: '',
        cc: '',
        disposicion: '',
        estadoActivo: '',
        responsable: '',
        foto: '',
        img: '',
        imagen : [],
    },

    mounted: function(){

    },

    methods: 
    {
        //Desglosar array
        toFormData: function(obj){
            var form_data = new FormData();
            for(var key in obj){
                form_data.append(key, obj[key]);
            }
            return form_data;
        },

        formatonumero: function(valor){
            return new Intl.NumberFormat("en-US", {style: "currency", currency: "USD"}).format(valor);
        },

        traeParametros: function(name){
            //Captura parametros
            name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
            var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
            results = regex.exec(location.search);
            return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
        },

        traeDatosFormulario: async function() {

            if (this.placa != "") {

                var formData = new FormData();

                formData.append("placa", this.placa);
                
                await axios.post('activos_fijos/reporte_activos/acti-fichaActivoFijo.php?action=datosFormulario', formData)
                .then((response) => {
                    
                    // console.log(response.data);
                    this.nombre = response.data.nombre;
                    this.fechaRegistro = response.data.fechaRegistro;
                    this.fechaCompra = response.data.fechaCompra;
                    this.fechaActivacion = response.data.fechaActivacion;
                    this.valorActivo = response.data.valorActivo;
                    this.valorDepreciado = response.data.valorDepreciado;
                    this.valorCorreccion = response.data.valorCorreccion;
                    this.referencia = response.data.referencia;
                    this.modelo = response.data.modelo;
                    this.serial = response.data.serial;
                    this.clase = response.data.clase;
                    this.grupo = response.data.grupo;
                    this.tipo = response.data.tipo;
                    this.prototipo = response.data.prototipo;
                    this.area = response.data.area;
                    this.ubicacion = response.data.ubicacion;
                    this.cc = response.data.cc;
                    this.disposicion = response.data.disposicion;
                    this.estadoActivo = response.data.estadoActivo;
                    this.responsable = response.data.responsable;
                    this.foto = response.data.foto;
                }).catch((error) => {
                    this.error = true;
                    console.log(error)
                }).finally(() => {
                    
                    if (this.foto != "") {
                        this.img = "informacion/proyectos/temp/"+this.foto;
                    }
                    else {
                        this.img = "imagenes/useradd02.png";
                    }
                });
            }
            else {
                Swal.fire(
                    'Error!',
                    'No hay ninguna placa seleccionada.',
                    'error'
                );
            }
            
        },

        obtenerImagen: async function(e) {

            let file = e.target.files[0];
            this.imagen = file;
          
            this.foto = this.imagen['name'];
            let formData = new FormData();

            formData.append("placa", this.placa);
            formData.append("imagen", this.imagen);
            
            await axios.post('activos_fijos/reporte_activos/acti-fichaActivoFijo.php?action=cargaImagen', formData)
            .then((response) => {
               
                if(response.data.insertaBien == true) {
                    Swal.fire({
                        icon: 'success',
                        title: 'Se ha actualizado la foto del activo',
                        showConfirmButton: false,
                        timer: 1500
                        }).then((response) => {
                            
                        }
                    );
                }
                else {
                    Swal.fire(
                        'Error!',
                        'No se pudo actualizar la foto.',
                        'error'
                    );
                }
            }).catch((error) => {
                this.error = true;
                console.log(error)
            }).finally(() => {
                if (this.foto != "") {
                    this.img = "informacion/proyectos/temp/"+this.foto;
                }
                else {
                    this.img = "imagenes/useradd02.png";
                }
            });
        },
    }
});