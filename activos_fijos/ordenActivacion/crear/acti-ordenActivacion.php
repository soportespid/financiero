<?php

	require_once '../../../comun.inc';
    require '../../../funciones.inc';
    require '../../../funcionesSP.inc.php';
    require '../../../conversor.php';
    session_start();

	$linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");

    $out = array('error' => false);

    $action = "show";

    if(isset($_GET['action'])){
        $action = $_GET['action'];
    }

    function buscaValores($array, $documento, $vigencia) {

        $valor = 0;

        foreach($array as $key => $activos) {

            if ($documento == $activos[0] && $vigencia == $activos[1])  {
                $valor = $activos[2];
                break;
            }
        }

        return $valor;
    }

    function buscaValores2($array, $documento) {

        $valor = 0;

        foreach($array as $key => $activos) {

            if ($documento == $activos[0])  {
                $valor = $activos[1];
                break;
            }
        }

        return $valor;
    }

    if ($action == "datosIniciales") {

        $origenes = array();

        $sqlOrigenes = "SELECT * FROM acti_origenes";
        $resOrigenes = mysqli_query($linkbd, $sqlOrigenes);
        while ($rowOrigenes = mysqli_fetch_row($resOrigenes)) {
            
            array_push($origenes, $rowOrigenes);
        }

        $out['origenes'] = $origenes;

        $clases = array();

        $sqlClase = "SELECT codigo, nombre FROM actipo WHERE niveluno = '0' AND estado = 'S'";
		$resClase = mysqli_query($linkbd,$sqlClase);
        while ($rowClase = mysqli_fetch_row($resClase)) {
            
            array_push($clases, $rowClase);
        }

        $out['clases'] = $clases;


        $prototipos = array();

        $sqlPrototipo = "SELECT id, nombre FROM acti_prototipo WHERE estado = 'S'";
        $resPrototipo = mysqli_query($linkbd, $sqlPrototipo);
        while ($rowPrototipo = mysqli_fetch_row($resPrototipo)) {
            
            array_push($prototipos, $rowPrototipo);
        }

        $out['prototipos'] = $prototipos;

        $dependencias = array();

        $sqlDependencia = "SELECT codarea, nombrearea FROM planacareas WHERE estado = 'S'";
        $resDependencia = mysqli_query($linkbd, $sqlDependencia);
        while ($rowDependencia = mysqli_fetch_row($resDependencia)) {
            
            array_push($dependencias, $rowDependencia);
        }

        $out['dependencias'] = $dependencias;

        $ubicaciones = array();
        
        $sqlUbicaciones = "SELECT id_cc, nombre FROM actiubicacion WHERE estado = 'S' ORDER BY CONVERT(id_cc, INT) ASC";
        $resUbicaciones = mysqli_query($linkbd, $sqlUbicaciones);
        while ($rowUbicaciones = mysqli_fetch_row($resUbicaciones)){
            
            array_push($ubicaciones, $rowUbicaciones);
        }

        $out['ubicaciones'] = $ubicaciones;

        $centroCostos = array();

        $sqlCentroCosto = "SELECT id_cc, nombre FROM centrocosto WHERE estado = 'S'";
        $resCentroCosto = mysqli_query($linkbd, $sqlCentroCosto);
        while ($rowCentroCosto = mysqli_fetch_row($resCentroCosto)) {
           
            array_push($centroCostos, $rowCentroCosto);
        }

        $out['centroCostos'] = $centroCostos;

        $disposiciones = array();

        $sqlDisposicion = "SELECT id, nombre FROM acti_disposicionactivos WHERE estado = 'S' ";
        $resDisposicion = mysqli_query($linkbd, $sqlDisposicion);
        while ($rowDisposicion = mysqli_fetch_row($resDisposicion)) {

            array_push($disposiciones, $rowDisposicion);	 	 
        }	 	

        $out['disposiciones'] = $disposiciones;

        $sqlOrden = "SELECT MAX(codigo) FROM acticrearact";
        $rowOrden = mysqli_fetch_row(mysqli_query($linkbd, $sqlOrden));

        $rowOrden[0] += 1;

        $out['codigoOrden'] = $rowOrden[0];
    }

    if ($action == "buscarRegistrosPresupuestales") {
        $registros = array();
        $documentos = array();

        $vigencia = $f[3];
        $destinoCompra = "02";

        $sqlDocumentos = "SELECT documento, YEAR(fechareg), SUM(valor) FROM acticrearact WHERE origen = 1 GROUP BY documento, YEAR(fechareg)";
        $resDocumentos = mysqli_query($linkbd, $sqlDocumentos);
        while ($rowDocumentos = mysqli_fetch_row($resDocumentos)) {
            array_push($documentos, $rowDocumentos);
        }

        $sqlRegistros = "SELECT CAB.consvigencia, CAB.detalle, CAB.fecha, SUM(DET.valor) FROM ccpetdc AS CAB INNER JOIN ccpetdc_detalle AS DET ON CAB.id = DET.consvigencia WHERE CAB.vigencia = '$vigencia' AND DET.vigencia = '$vigencia' AND DET.destino_compra = '$destinoCompra'";
        $resRegistros = mysqli_query($linkbd, $sqlRegistros);
        while ($rowRegistros = mysqli_fetch_row($resRegistros)) {
    
            $datosTemp = [];
            $valorUsado = 0;
            $valorDisponible = 0;

            $valorUsado = buscaValores($documentos, $rowRegistros[0], $vigencia);
            $valorDisponible = $rowRegistros[3];

            $datosTemp[] = $rowRegistros[0];
            $datosTemp[] = $rowRegistros[1];
            $datosTemp[] = $rowRegistros[2];
            $datosTemp[] = $valorDisponible;
            array_push($registros, $datosTemp);

        }

        $out['registros'] = $registros;
    }

    if ($action == "buscarSalidaAlmacen") {

        $registros = array();
        $documentos = array();
        $fecha = $_POST["fecha"];

        $sqlDocumentos = "SELECT documento, SUM(valor) FROM acticrearact WHERE origen = 4 AND estado = 'S' GROUP BY documento";
        $resDocumentos = mysqli_query($linkbd, $sqlDocumentos);
        while ($rowDocumentos = mysqli_fetch_row($resDocumentos)) {
            array_push($documentos, $rowDocumentos);
        }

        $sql = "SELECT consecutivo, descripcion, fecha, valor_total FROM alm_salida_activos_cab WHERE fecha <= '$fecha' AND estado = 'S'";
        $res = mysqli_query($linkbd, $sql);
        while ($row = mysqli_fetch_row($res)) {
            
            $datosTemp = [];
            $valorUsado = 0;
            $valorDisponible = 0;

            $valorUsado = buscaValores2($documentos, $row[0]);
            $valorDisponible = $row[3] - $valorUsado;

            if ($valorDisponible > 0) {
                $datosTemp[] = $row[0];
                $datosTemp[] = $row[1];
                $datosTemp[] = $row[2];
                $datosTemp[] = $valorDisponible;
                array_push($registros, $datosTemp);
            }
        }

        $out['registros'] = $registros;
    }

    if ($action == 'armarPlaca') {

        $clasificacion = $_GET['placa'];

        $sqlPlaca = "SELECT MAX(SUBSTR(placa, 7, 4) * 1) FROM acticrearact_det WHERE SUBSTR(placa, 1, 6) LIKE '$clasificacion%' AND tipo_mov = '101' ORDER BY placa ASC";
        $rowPlaca = mysqli_fetch_row(mysqli_query($linkbd, $sqlPlaca));

        $orden = $rowPlaca[0] + 1;
        $orden = $clasificacion.substr("0000".$orden, -4);

        $out['placa'] = $orden;
    }

    if ($action == "buscaResponsable") {

        $cedula = $_GET['cedula'];

        $sqlTercero = "SELECT nombre1, nombre2, apellido1, apellido2 FROM terceros WHERE cedulanit = '$cedula'";
        $rowTercero = mysqli_fetch_row(mysqli_query($linkbd, $sqlTercero));

        if ($rowTercero[0] != ""){
            $nombre = $rowTercero[0] . " " . $rowTercero[1] . " " . $rowTercero[2] . " " .$rowTercero[3];
        }
        else {
            $nombre = "";
        }

        $out['nombre'] = $nombre;
    }

    if ($action == "buscarTerceros") {

        $terceros = array();

        $sqlTercero = "SELECT nombre1, nombre2, apellido1, apellido2, cedulanit, razonsocial, tipodoc FROM terceros WHERE estado = 'S'";
        $resTercero = mysqli_query($linkbd, $sqlTercero);
        while ($rowTerceros = mysqli_fetch_row($resTercero)) {
            
            $temporal = array();

            if ($rowTerceros[6] == "31") {

                $nombre = $rowTerceros[5];
            }
            else {

                $nombre = $rowTerceros[0] . " " . $rowTerceros[1] . " " . $rowTerceros[2] . " " . $rowTerceros[3];
            }

            array_push($temporal, $rowTerceros[4]);
            array_push($temporal, $nombre);
            array_push($terceros, $temporal);
        }

        $out['terceros'] = $terceros;
    }

    if ($action == "filtraTerceros") {

        $cedula = $_POST['cedula'];
        
        $sqlTercero = "SELECT nombre1, nombre2, apellido1, apellido2, cedulanit, razonsocial, tipodoc FROM terceros WHERE estado = 'S' AND cedulanit LIKE '$cedula%'";
        $resTercero = mysqli_query($linkbd, $sqlTercero);
        while ($rowTerceros = mysqli_fetch_row($resTercero)) {
            
            $temporal = array();

            if ($rowTerceros[6] == "31") {

                $nombre = $rowTerceros[5];
            }
            else {

                $nombre = $rowTerceros[0] . " " . $rowTerceros[1] . " " . $rowTerceros[2] . " " . $rowTerceros[3];
            }

            array_push($temporal, $rowTerceros[4]);
            array_push($temporal, $nombre);
            array_push($terceros, $temporal);
        }

        $out['terceros'] = $terceros;
    }

    if ($action == 'guardar') {

        //guarda primero en modulo y luego hace contabilidad
        $direccion = "../../../informacion/proyectos/temp/";
        $codigoOrden = $_POST['orden'];
        $origen = $_POST['origen'];
        $documento = $_POST['documento'];
        $valorTotal = $_POST['valorTotal'];
        $fechaRegistro = $_POST["fechaRegistro"];
        $fechaComoEntero = strtotime($fechaRegistro);
        $vigencia = date("Y", $fechaComoEntero);

        $sqlr = "SELECT count(*) From dominios dom  where dom.nombre_dominio = 'PERMISO_MODIFICA_DOC'  and dom.valor_final <= '$fechaRegistro'  AND dom.valor_inicial =  '$_SESSION[cedulausu]' ";
        $res = mysqli_query($linkbd,$sqlr);
        $row = mysqli_fetch_row($res);

        if ($row[0] >= 1) {

            $sqlActivoCab = "INSERT INTO acticrearact (codigo, fechareg, origen, documento, valor, estado, tipo_mov) VALUES ('$codigoOrden', '$fechaRegistro', '$origen', '$documento', $valorTotal, 'S', '101')";
            mysqli_query($linkbd, $sqlActivoCab);

            $sqlr="SELECT nit FROM configbasica WHERE estado='S'";
            $row = mysqli_fetch_assoc(mysqli_query($linkbd, $sqlr));

            $nit = $row['nit'];

            //Contabilidad
            $tipoComprobante = "70";

            $sqlComprobanteCab = "INSERT INTO comprobante_cab (numerotipo, tipo_comp, fecha, concepto, total, total_debito, total_credito, diferencia, estado) VALUES ('$codigoOrden', '$tipoComprobante', '$fechaRegistro', 'Orden de activacion numero $codigoOrden', '$valorTotal', '$valorTotal', '$valorTotal', 0, 1)";
            mysqli_query($linkbd, $sqlComprobanteCab);

            if ($_POST['origen'] == 1) {

                $sqlCredito = "SELECT cuenta_debito FROM ccpetdc_detalle WHERE idrp = '$documento' AND vigencia = '$vigencia'";
                $rowCredito = mysqli_fetch_assoc(mysqli_query($linkbd, $sqlCredito));

                $cuentaCredito = $rowCredito['cuenta_debito'];
            }
            else if ($_POST['origen'] == 4) {
                
                $sqlConceptosContables = "SELECT CC.codigo, CC.nombre, CCD.cuenta FROM conceptoscontables AS CC INNER JOIN conceptoscontables_det AS CCD ON CCD.codigo = CC.codigo WHERE CC.modulo = 5 AND CC.tipo = 'CT' AND CCD.tipo = 'CT' AND CCD.cc = '01' ORDER BY CCD.fechainicial DESC";
                $rowConceptosContables = mysqli_fetch_row(mysqli_query($linkbd, $sqlConceptosContables));

                $cuentaCredito = $rowConceptosContables[2];
            }
            else if ($_POST['origen'] >= 5) {

                $codigo = $_POST['origen'] - 4;

                if ($codigo <= 9) {
                    $codigo = '0' . $codigo;
                }

                $sqlCredito = "SELECT cuenta FROM actiorigenes_det WHERE codigo = '$codigo' AND credito = 'S' AND estado = 'S'";
                $rowCredito = mysqli_fetch_assoc(mysqli_query($linkbd,$sqlCredito));

                $cuentaCredito = $rowCredito['cuenta'];
            }

            for ($i=0; $i < count($_POST['activosGuardar']); $i++) { 
                
                $placa = $_POST['activosGuardar'][$i][0];
                $nombre = strClean(replaceChar($_POST['activosGuardar'][$i][1]));
                $clase = $_POST['activosGuardar'][$i][2];
                $grupo = $_POST['activosGuardar'][$i][3];
                $tipo = $_POST['activosGuardar'][$i][4];
                $prototipo = $_POST['activosGuardar'][$i][5];
                $dependencia = $_POST['activosGuardar'][$i][6];
                $ubicacion = $_POST['activosGuardar'][$i][7];
                $centroCosto = $_POST['activosGuardar'][$i][8];
                $disposicion = $_POST['activosGuardar'][$i][9];
                $valor = $_POST['activosGuardar'][$i][10];
                $referencia = strClean(replaceChar($_POST['activosGuardar'][$i][11]));
                $modelo = strClean(replaceChar($_POST['activosGuardar'][$i][12]));
                $serial = strClean(replaceChar($_POST['activosGuardar'][$i][13]));
                $estadoActivo = $_POST['activosGuardar'][$i][14];
                //preg_match("/([0-9]{2})\/([0-9]{2})\/([0-9]{4})/", $_POST["activosGuardar"][$i][15], $f);
                $fechaCompra =  $_POST["activosGuardar"][$i][15];
                //preg_match("/([0-9]{2})\/([0-9]{2})\/([0-9]{4})/", $_POST["activosGuardar"][$i][16], $f);
                $fechaActivacion =  $_POST["activosGuardar"][$i][16];
                $unidadMedida = $_POST['activosGuardar'][$i][17];
                $numeroMeses = $_POST['activosGuardar'][$i][19];
                $valorDesmantelamiento = (double) $_POST["activosGuardar"][$i][23];
                $valorSalvamento = (double) $_POST["activosGuardar"][$i][24];
                if ($_POST['activosGuardar'][$i][18] == true) {
                    $bloque = 1;
                    $valorDepreciarMensual = 0;
                }
                else {
                    $bloque = 0;
                    $valorDepreciarMensual = $valor / $numeroMeses;
                    $valorDepreciarMensual = round($valorDepreciarMensual, 2);
                }
                $cedula = $_POST['activosGuardar'][$i][22];
                $fotoNombre = $_FILES['activosGuardar']['name'][$i][0];
                $fichaNombre = $_FILES['activosGuardar']['name'][$i][1];
                $fotoTmp = $_FILES['activosGuardar']['tmp_name'][0][0];
                $fichaTmp = $_FILES['activosGuardar']['tmp_name'][0][1];
                //subida de foto
                if ($fotoNombre != "") {
                    $targetFile1 = $direccion.basename($fotoNombre);
                    move_uploaded_file($fotoTmp, $targetFile1);
                }
                
                //subida ficha
                if ($fichaNombre != "") {
                    $targetFile2 = $direccion.basename($fichaNombre);
                    move_uploaded_file($fichaTmp, $targetFile2);
                }

                $sqlActivosDet = "INSERT INTO acticrearact_det (codigo, placa, nombre, clasificacion, grupo, tipo, prototipo, area, ubicacion, cc, dispoact, valor, referencia, modelo, serial, estadoactivo, fechacom, fechact, unidadmed, bloque, saldomesesdep, foto, ficha, saldodepact, valdepmen, tipo_mov, vigencia, estado, valor_desman, valor_salv) VALUES ('$codigoOrden', '$placa', '$nombre', '$clase', '$grupo', '$tipo', '$prototipo', '$dependencia', '$ubicacion', '$centroCosto', '$disposicion', '$valor', '$referencia', '$modelo', '$serial', '$estadoActivo', '$fechaCompra', '$fechaActivacion', '$unidadMedida', '$bloque', '$numeroMeses', '$fotoNombre', '$fichaNombre', '$valor', '$valorDepreciarMensual', '101', '$vigencia', 'S', $valorDesmantelamiento, $valorSalvamento)";
                mysqli_query($linkbd, $sqlActivosDet);

                $sqlResponsable = "INSERT INTO acticrearact_det_responsable(tercero, placa, estado) VALUES ('$cedula', '$placa', 'S')";
                mysqli_query($linkbd, $sqlResponsable);

                $filtroCodigo = substr($placa,0,6);

                //Contabilidad
                $sqlDebito = "SELECT cuenta_activo FROM acti_activos_det WHERE tipo LIKE '$filtroCodigo' AND disposicion_activos = '$disposicion' AND centro_costos LIKE '$centroCosto'";
                $rowDebito = mysqli_fetch_assoc(mysqli_query($linkbd, $sqlDebito));

                $cuentaDebito = $rowDebito['cuenta_activo'];

                //Debito
                $queryCompDebito = "INSERT INTO comprobante_det (id_comp,cuenta,tercero,centrocosto,detalle,cheque,valdebito,valcredito,estado,vigencia, numacti) values ('$tipoComprobante $codigoOrden', '$cuentaDebito','$nit','$centroCosto','$nombre','','$valor',0,'1','$vigencia','$placa')";
                mysqli_query($linkbd, $queryCompDebito);

                //Credito
                $queryCompCredito = "INSERT INTO comprobante_det (id_comp,cuenta,tercero,centrocosto,detalle,cheque,valdebito,valcredito,estado,vigencia, numacti) values ('$tipoComprobante $codigoOrden', '$cuentaCredito','$nit','$centroCosto','$nombre','',0,'$valor','1','$vigencia','$placa')";
                mysqli_query($linkbd, $queryCompCredito);
            }

            $out['insertaBien'] = true;
        }
    }

    if ($action == "buscaOrdenes") {
        $fechaReversion = $_POST['fechaReversion'];

        $ordenes = array();

        $sqlCab = "SELECT CAB.codigo, CAB.fechareg, CAB.origen, CAB.documento, CAB.valor FROM acticrearact AS CAB WHERE CAB.fechareg <= '$fechaReversion' AND CAB.estado = 'S' ORDER BY CAB.codigo DESC";
        $resCab = mysqli_query($linkbd, $sqlCab);
        while ($rowCab = mysqli_fetch_row($resCab)) {
            
            array_push($ordenes, $rowCab);
        }

        $out['ordenes'] = $ordenes;
    }

    if ($action == "buscaOrdenesDet") {

        $orden = $_POST['orden'];
        $activos = array();

        $sqlActivos = "SELECT CAB.codigo, CAB.origen, DET.placa, DET.nombre, CAB.fechareg, DET.clasificacion, DET.grupo, DET.tipo, DET.prototipo, DET.area, DET.ubicacion, DET.cc, DET.dispoact, DET.valor, DET.referencia, DET.modelo, DET.serial, DET.estadoactivo, DET.fechacom, DET.fechact, DET.unidadmed, DET.bloque, DET.saldomesesdep, DET.foto, DET.ficha FROM acticrearact_det AS DET INNER JOIN acticrearact AS CAB ON CAB.codigo = DET.codigo WHERE CAB.estado = 'S' AND CAB.codigo = '$orden' AND DET.codigo = '$orden' ORDER BY CAST(DET.codigo AS unsigned) DESC";
        $resActivos = mysqli_query($linkbd, $sqlActivos);
        while ($rowActivos = mysqli_fetch_row($resActivos)) {

            $datos = array();

            $fechaCompra = date("d/m/Y", strtotime($rowActivos[18]));
            $fechaActivacion = date("d/m/Y", strtotime($rowActivos[19]));

            $sqlClase = "SELECT nombre FROM actipo WHERE codigo = '$rowActivos[5]' AND niveluno = '0' AND estado = 'S'";
            $rowClase = mysqli_fetch_row(mysqli_query($linkbd, $sqlClase));

            $clase = $rowActivos[5] . " - " . $rowClase[0];

            $sqlGrupo = "SELECT nombre FROM actipo WHERE codigo = '$rowActivos[6]' AND niveluno='$rowActivos[5]' AND estado = 'S'";
            $rowGrupo = mysqli_fetch_row(mysqli_query($linkbd, $sqlGrupo));

            $grupo = $rowActivos[6] . " - " . $rowGrupo[0];

            $sqlTipo = "SELECT nombre FROM actipo WHERE codigo = '$rowActivos[7]' AND niveluno = '$rowActivos[6]' AND niveldos = '$rowActivos[5]' AND estado = 'S'";
            $rowTipo = mysqli_fetch_row(mysqli_query($linkbd, $sqlTipo));

            $tipo = $rowActivos[7] . " - " . $rowTipo[0];

            $sqlPrototipo = "SELECT nombre FROM acti_prototipo WHERE id = $rowActivos[8] AND estado = 'S'";
            $rowPrototipo = mysqli_fetch_row(mysqli_query($linkbd, $sqlPrototipo));

            $prototipo = $rowActivos[8] . " - " . $rowPrototipo[0];

            $sqlDependencia = "SELECT nombrearea FROM planacareas WHERE codarea = '$rowActivos[9]' AND estado = 'S'";
            $rowDependencia = mysqli_fetch_row(mysqli_query($linkbd, $sqlDependencia));

            $dependencia = $rowActivos[9] . " - " . $rowDependencia[0];

            $sqlUbicaciones = "SELECT id_cc, nombre FROM actiubicacion WHERE id_cc = '$rowActivos[10]' AND estado = 'S'";
            $rowUbicaciones = mysqli_fetch_row(mysqli_query($linkbd, $sqlUbicaciones));

            $ubicacion = $rowActivos[10] . " - " . $rowUbicaciones[1];

            $sqlCentroCosto = "SELECT id_cc, nombre FROM centrocosto WHERE id_cc = '$rowActivos[11]' AND estado = 'S'";
            $rowCentroCosto = mysqli_fetch_row(mysqli_query($linkbd, $sqlCentroCosto));

            $centroCosto = $rowActivos[11] . " - " . $rowCentroCosto[1];

            $sqlDisposicion = "SELECT id, nombre FROM acti_disposicionactivos WHERE id = '$rowActivos[12]' AND estado = 'S' ";
            $rowDisposicion = mysqli_fetch_row(mysqli_query($linkbd, $sqlDisposicion));

            $disposicion = $rowActivos[12] . " - " . $rowDisposicion[1];

            switch ($rowActivos[20]) {
                case 1:
                    $unidadMedida = "Unidad";
                break;

                case 2:
                    $unidadMedida = "Juego";
                break;

                case 3:
                    $unidadMedida = "Caja";
                break;
            }   

            if ($rowActivos[21] == 1) {
                
                $bloque = "Si";
            }
            else {
                $bloque = "No";
            }

            $rutaFoto = "informacion/proyectos/temp/" . $rowActivos[23];
            $rutaFicha = "informacion/proyectos/temp/" . $rowActivos[24];

            if ($rowActivos[23] != "") {

                $foto = "imagenes/foto.png";
            }
            else {
                $foto = "imagenes/nofoto.png";
            }

            if ($rowActivos[24] != "") {

                $ficha = "imagenes/foto.png";
            }
            else {
                $ficha = "imagenes/nofoto.png";
            }

            array_push($datos, $rowActivos[2]);
            array_push($datos, $rowActivos[3]);
            array_push($datos, $clase);
            array_push($datos, $grupo);
            array_push($datos, $tipo);
            array_push($datos, $prototipo);
            array_push($datos, $dependencia);
            array_push($datos, $ubicacion);
            array_push($datos, $centroCosto);
            array_push($datos, $disposicion);
            array_push($datos, $rowActivos[13]);
            array_push($datos, $rowActivos[14]);
            array_push($datos, $rowActivos[15]);
            array_push($datos, $rowActivos[16]);
            array_push($datos, $rowActivos[17]);
            array_push($datos, $fechaCompra);
            array_push($datos, $fechaActivacion);
            array_push($datos, $unidadMedida);
            array_push($datos, $bloque);
            array_push($datos, $rowActivos[22]);
            array_push($datos, $rutaFoto);
            array_push($datos, $rutaFicha);
            array_push($datos, $foto);
            array_push($datos, $ficha);
            array_push($activos, $datos);
        }

        $out['ordenesDet'] = $activos;
    }

    if ($action == "guardarRev") {

        $orden = $_POST['orden'];
        $origen = $_POST['origen'];
        $documento = $_POST['documento'];
        $valor = $_POST['valor'];
        $fecha = $_POST["fechaRev"];
        $tipoComprobante = "70";
        $tipoComprobanteRev = "2070";

        $sqlActivoCab = "UPDATE acticrearact SET estado = 'R' WHERE codigo = '$orden'";
        mysqli_query($linkbd, $sqlActivoCab);

        $sqlActivoCab = "INSERT INTO acticrearact (codigo, fechareg, origen, valor, estado, tipo_mov) VALUES ('$orden', '$fecha', '$origen', '$valor', 'R', '301')";
        mysqli_query($linkbd, $sqlActivoCab);

        $sqlActivosDet = "UPDATE acticrearact_det SET estado = 'R' WHERE codigo = '$orden'";
        mysqli_query($linkbd, $sqlActivosDet);

        $sqlCabacera = "UPDATE comprobante_cab SET estado = '0' WHERE numerotipo = '$orden' AND tipo_comp = '$tipoComprobante'";
        mysqli_query($linkbd, $sqlCabacera);

        $sqlCabecera = "SELECT concepto FROM comprobante_cab WHERE numerotipo = '$orden' AND tipo_comp = '$tipoComprobante'";
        $rowCabecera = mysqli_fetch_row(mysqli_query($linkbd, $sqlCabecera));

        $sqlCabeceraComp = "INSERT INTO comprobante_cab (numerotipo, tipo_comp, fecha, concepto, total, total_debito, total_credito, diferencia, estado) VALUES ('$orden', '$tipoComprobanteRev', '$fechaRev', '$rowCabecera[0]', '$valor', '$valor', '$valor', 0, 1)";
        mysqli_query($linkbd, $sqlCabeceraComp);

        $sqlDetalle = "SELECT cuenta, tercero, centrocosto, detalle, valdebito, valcredito, vigencia, numacti, canarticulo, bodega_ubicacion FROM comprobante_det WHERE tipo_comp = '$tipoComprobante' AND numerotipo = '$orden'";
        $resDetalle = mysqli_query($linkbd, $sqlDetalle);
        while ($rowDetalle = mysqli_fetch_row($resDetalle)) {
            
            if ($rowDetalle[4] > 0) {

                $sqlDebito = "INSERT INTO comprobante_det (id_comp, cuenta, tercero, centrocosto, detalle, valdebito, valcredito, estado, vigencia, tipo_comp, numerotipo, numacti, canarticulo, bodega_ubicacion) VALUES ('$tipoComprobanteRev $orden', '$rowDetalle[0]', '$rowDetalle[1]', '$rowDetalle[2]', '$rowDetalle[3]', 0, $rowDetalle[4], '1', '$rowDetalle[6]', '$tipoComprobanteRev', '$orden', '$rowDetalle[7]', '$rowDetalle[8]', '$rowDetalle[9]')";
                mysqli_query($linkbd, $sqlDebito);
            }
            else if ($rowDetalle[5] > 0) {

                $sqlCredito = "INSERT INTO comprobante_det (id_comp, cuenta, tercero, centrocosto, detalle, valdebito, valcredito, estado, vigencia, tipo_comp, numerotipo, numacti, canarticulo, bodega_ubicacion) VALUES ('$tipoComprobanteRev $orden', '$rowDetalle[0]', '$rowDetalle[1]', '$rowDetalle[2]', '$rowDetalle[3]', $rowDetalle[5], 0, '1', '$rowDetalle[6]', '$tipoComprobanteRev', '$orden', '$rowDetalle[7]', '$rowDetalle[8]', '$rowDetalle[9]')";
                mysqli_query($linkbd, $sqlCredito);
            }
        }

        $out['insertaBien'] = true;
    }

    header("Content-type: application/json");
    echo json_encode($out);
    die();