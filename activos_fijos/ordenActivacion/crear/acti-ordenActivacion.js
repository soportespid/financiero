const URL = 'activos_fijos/ordenActivacion/crear/acti-ordenActivacion.php';
const URLSECOND = "activos_fijos/reporte_activos/acti-reporteActivosFijos.php";
import { filtroEnArrayDeObjetos } from './../../../funciones.js';

var app = new Vue({
    el: '#myapp',
	data:{
        
        error: '',
        isLoading: false,
        movimiento: 1,
        tabgroup1: 1,
        origenes: [],
        origen: '',
        clases: [],
        clase: '',
        prototipos: [],
        prototipos_copy: [],
        prototipo: '',
        dependencias: [],
        dependencias_copy: [],
        dependencia: '',
        ubicaciones: [],
        ubicaciones_copy: [],
        ubicacion: '',
        centroCostos: [],
        centroCosto: '',
        disposiciones: [],
        disposicion: '',
        codigoOrden: '',
        grupos: [],
        grupo: '',
        tipos: [],
        tipo: '',
        estadoActivo: '',
        nombreActivo: '',
        valorActivo: 0,
        valorSalv: 0,
        valorDesman: 0,
        referencia: '',
        modelo: '',
        serial: '',
        depreacionBloque: true,
        mesesDepreciados: 0,
        foto: [],
        fotoFicha: [],
        unidadMedida: '',
        placa: '',
        valor: "",
        documento: "",
        documentos: [],
        showDocumentos: false,
        img: '',
        img2: '',
        activos: [],
        activosGuardar: [],
        unidades: [],
        totalActivos: 0,
        cedula: '',
        responsable: '',
        terceros: [],
        terceroscopia: [],
        showModalResponsables: false,
        searchCedula: '',
        modalPrototipos: false,
        txtSearchPro: '',
        modalDependencias: false,
        txtSearchDep: '',
        modalUbicaciones: false,
        txtSearchUbi: '',
        //Variables reversion
        ordenRev: '',
        fechaRev: '',
        ordenes: [],
        ordenesDet: [],
        origenRev: '',
        documentoRev: '',
        valorRev: '',
        showOrdenes: false,
        fechaRegistro: '',
        fechaCompra: '',
        fechaActivacion: '',
        fechaReversion: '',
    },

    mounted: function(){

        this.datosIniciales();

        this.unidades[0] = { 
            0: "1",
            1: "Unidad"
        }; 
        this.unidades[1] = { 
            0: "2",
            1: "Juego"
        };
        this.unidades[2] = { 
            0: "3",
            1: "Caja"
        }; 
    },

    methods: 
    {
        //Desglosar array
        toFormData: function(obj){
            var form_data = new FormData();
            for(var key in obj){
                form_data.append(key, obj[key]);
            }
            return form_data;
        },

        formatonumero: function(valor){
            return new Intl.NumberFormat("en-US", {style: "currency", currency: "USD"}).format(valor);
        },

        traeParametros: function(name){
            //Captura parametros
            name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
            var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
            results = regex.exec(location.search);
            return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
        },

        datosIniciales: async function() {
            
            await axios.post(URL+'?action=datosIniciales')
            .then((response) => {
                
                this.origenes = response.data.origenes;
                this.clases = response.data.clases;
                this.prototipos = response.data.prototipos;
                this.prototipos_copy = response.data.prototipos;
                this.dependencias = response.data.dependencias;
                this.dependencias_copy = response.data.dependencias;
                this.ubicaciones = response.data.ubicaciones;
                this.ubicaciones_copy = response.data.ubicaciones;
                this.centroCostos = response.data.centroCostos;
                this.disposiciones = response.data.disposiciones;
                this.codigoOrden = response.data.codigoOrden;
            }).catch((error) => {
                this.error = true;
                console.log(error)
            }).finally(() => {
                this.isLoading = false;
                
            });
        },

        cambioOrigen: function() {

            this.documento = "";
            this.valor = "";
        },

        buscarDocumentos: function() {

            const fechaReg = this.fechaRegistro;

            if (fechaReg != "") {

                if (this.origen != "") {

                    switch (this.origen) {
                        case "1":
                            this.isLoading = true;

                            var formData = new FormData();
                            formData.append("fecha", fechaReg);

                            axios.post(URL+'?action=buscarRegistrosPresupuestales', formData)
                            .then((response) => {
                                console.log(response.data);
                                this.documentos = response.data.registros;
                            }).finally(() => {
                            
                                this.isLoading = false;
                                this.showDocumentos = true;
                            });    
                        break;

                        case "4":
                            this.isLoading = true;

                            var formData = new FormData();
                            formData.append("fecha", fechaReg);

                            axios.post(URL+'?action=buscarSalidaAlmacen', formData)
                            .then((response) => {
                                this.documentos = response.data.registros;
                            }).finally(() => {
                            
                                this.isLoading = false;
                                this.showDocumentos = true;
                            });    
                        break;
                    }
                }
                else {

                    Swal.fire({
                        icon: 'warning',
                        title: 'Falta información',
                        text: 'Debe seleccionar primero un origen'
                    })
                }
            }
            else {
                Swal.fire({
                    icon: 'warning',
                    title: 'Falta información',
                    text: 'Debe ingresar la fecha'
                })
            }
        },

        seleccionarDocumento: function(document) {

            if (document != "") {
                this.documento = document[0];
                this.valor = document[3];
                this.showDocumentos = false;
            }
        },

        buscaGrupos: async function(clase) {

            this.grupo = "";
            this.tipo = "";
            this.placa = "";

            let idClase = clase;

            if (idClase != "") {

                await axios.post(URLSECOND+'?action=buscarGrupos&clase='+idClase)
                .then((response) => {
                    
                    this.grupos = response.data.grupos;
                });
            }
        },

        buscarTipos: async function(clase, grupo) {
            
            this.tipo = "";
            this.placa = "";

            let idClase = clase;
            let idGrupo = grupo;

            if (idGrupo != "" && idClase != "") {

                await axios.post(URLSECOND+'?action=buscarTipos&clase='+idClase+'&grupo='+idGrupo)
                .then((response) => {
                    
                    this.tipos = response.data.tipos;
                });
            }
        },

        
        formarPlaca: async function(clase, grupo, tipo) {

            if (clase != '' && grupo != '' && tipo != '') {

                let placa = clase + grupo + tipo;

                await axios.post(URL+'?action=armarPlaca&placa='+placa)
                .then((response) => {
                    
                    this.placa = response.data.placa;
                });

                var resultado;
                var text = this.placa;
                const data = this.activos;

                resultado = text.slice(6, 10);
                resultado++;

                resultado = resultado.toString().padStart(4, '0');

                for(let i=0; i <= this.activos.length-1; i++) {
    
                    resultado = await filtroEnArrayDeObjetos({'data': data, 'text': text});

                    if (resultado.length != 0) {

                        text = text.slice(6, 10);
                        text++;

                        text = placa+text.toString().padStart(4, 0);
                    }
                }

                this.placa = text;
            }
        },

        searchPrototipo: async function() {
            let search = "";
            search = this.txtSearchPro.toLowerCase();
            
            this.prototipos_copy = [...this.prototipos.filter(e=>e[0].toLowerCase().includes(search) || e[1].toLowerCase().includes(search))];
        },

        selectPrototipo: async function(item) {
            this.prototipo = item;
            this.prototipos_copy = this.prototipos;
            this.txtSearchPro = "";
            this.modalPrototipos = false;
        },

        searchDependencia: async function() {
            let search = "";
            search = this.txtSearchDep.toLowerCase();
            
            this.dependencias_copy = [...this.dependencias.filter(e=>e[0].toLowerCase().includes(search) || e[1].toLowerCase().includes(search))];
        },

        selectDepen: async function(item) {
            this.dependencia = item;
            this.dependencias_copy = this.dependencias;
            this.txtSearchDep = "";
            this.modalDependencias = false;
        },

        searchUbicacion: async function() {
            let search = "";
            search = this.txtSearchUbi.toLowerCase();
            
            this.ubicaciones_copy = [...this.ubicaciones.filter(e=>e[0].toLowerCase().includes(search) || e[1].toLowerCase().includes(search))];
        },

        selectUbicacion: async function(item) {
            this.ubicacion = item;
            this.ubicaciones_copy = this.ubicaciones;
            this.txtSearchUbi = "";
            this.modalUbicaciones = false;
        },

        obtenerFoto: async function(e) {

            let file = e.target.files[0];
            this.foto = file;
        },

        obtenerFicha: async function(e) {

            let file = e.target.files[0];
            this.fotoFicha = file;
        },

        buscarCedula: async function(cedula) {

            if (cedula != "") {
                await axios.post(URL+'?action=buscaResponsable&cedula='+cedula)
                .then((response) => {
                    
                    console.log(response.data);
                    if (response.data.nombre == "") {
                        Swal.fire({
                            icon: 'warning',
                            title: 'No se ha encontrado',
                            text: 'Revisa el documento que escribiste'
                        })
                    }
                    else {
                        this.responsable = response.data.nombre;
                    }
                });
            }
        },

        despliegaModalResponsable: async function() {

            this.isLoading = true;
            
            await axios.post(URL+'?action=buscarTerceros')
            .then((response) => {
                
                this.terceros = response.data.terceros;
                this.terceroscopia = response.data.terceros;
            });

            this.isLoading = false;
            this.showModalResponsables = true;
        },

        filtroCedulaResponsable: async function() {
            let search = "";
            search = this.searchCedula.toLowerCase();
            
            this.terceros = [...this.terceroscopia.filter(e=>e[0].toLowerCase().includes(search) || e[1].toLowerCase().includes(search))];
        },

        seleccionaResponsable: function(cedula, nombre) {
        
            if (cedula != "" && nombre != "") {

                this.cedula = cedula;
                this.responsable = nombre;
                this.showModalResponsables = false;
            }        
        },

        agregarActivo: function() {

            if (
                this.clase != "" &&
                this. grupo != "" && 
                this.tipo != "" &&
                this.prototipo != "" &&
                this.dependencia != "" &&
                this.ubicacion != "" && 
                this.centroCosto != "" && 
                this.disposicion != ""
            ) {

                const fechaCompra = this.fechaCompra;
                const fechaActivacion = this.fechaActivacion;

                if (
                    this.placa != "" && 
                    this.nombreActivo != "" &&
                    this.valorActivo != "" &&
                    this.estadoActivo != "" &&
                    fechaCompra != "" &&
                    fechaActivacion != "" &&
                    this.unidadMedida != "" &&
                    this.responsable != "" &&
                    this.cedula != ""
                ) {

                    var temporal = [];
                    var temporalGuardar = [];
                    var bloque;

                    //objeto visible
                    temporal.push(this.placa);
                    temporal.push(this.nombreActivo);
                    temporal.push(this.clase[1]);
                    temporal.push(this.grupo[1]);
                    temporal.push(this.tipo[1]);
                    temporal.push(this.prototipo[1]);
                    temporal.push(this.dependencia[1]);
                    temporal.push(this.ubicacion[1]);
                    temporal.push(this.centroCosto[1]);
                    temporal.push(this.disposicion[1]);
                    temporal.push(this.valorActivo);
                    temporal.push(this.referencia);
                    temporal.push(this.modelo);
                    temporal.push(this.serial);
                    temporal.push(this.estadoActivo);
                    temporal.push(fechaCompra);
                    temporal.push(fechaActivacion);
                    temporal.push(this.unidadMedida[1]);

                    if (this.depreacionBloque == true) {

                        bloque = "Si";
                    }
                    else {
                        bloque = "No";
                    }

                    temporal.push(bloque);
                    temporal.push(this.mesesDepreciados);
                    temporal.push(this.responsable);
                    this.activos.push(temporal);

                    if (this.foto != "") {

                        this.img = "imagenes/foto.png";
                    }
                    else {
                        this.img = "imagenes/nofoto.png";
                    }

                    if (this.fotoFicha != "") {

                        this.img2 = "imagenes/foto.png";
                    }
                    else {
                        this.img2 = "imagenes/nofoto.png";
                    }

                    temporalGuardar.push(this.placa);
                    temporalGuardar.push(this.nombreActivo);
                    temporalGuardar.push(this.clase[0]);
                    temporalGuardar.push(this.grupo[0]);
                    temporalGuardar.push(this.tipo[0]);
                    temporalGuardar.push(this.prototipo[0]);
                    temporalGuardar.push(this.dependencia[0]);
                    temporalGuardar.push(this.ubicacion[0]);
                    temporalGuardar.push(this.centroCosto[0]);
                    temporalGuardar.push(this.disposicion[0]);
                    temporalGuardar.push(this.valorActivo);
                    temporalGuardar.push(this.referencia);
                    temporalGuardar.push(this.modelo);
                    temporalGuardar.push(this.serial);
                    temporalGuardar.push(this.estadoActivo);
                    temporalGuardar.push(fechaCompra);
                    temporalGuardar.push(fechaActivacion);
                    temporalGuardar.push(this.unidadMedida[0]);
                    temporalGuardar.push(this.depreacionBloque);
                    temporalGuardar.push(this.mesesDepreciados);
                    temporalGuardar.push(this.foto);
                    temporalGuardar.push(this.fotoFicha);
                    temporalGuardar.push(this.cedula);
                    temporalGuardar.push(this.valorDesman);
                    temporalGuardar.push(this.valorSalv);
                    this.activosGuardar.push(temporalGuardar);
                    //redirigue a la otra pestaña
                    this.tabgroup1 = 2; 


                    //suma total
                    this.totalActivos += parseFloat(this.valorActivo);

                    //Limpiar variables
                    this.clase = this.grupo = this.tipo = this.placa = this.nombreActivo = this.prototipo = this.dependencia = this.ubicacion = this.centroCosto = this.disposicion = this.valorActivo = this.referencia = this.modelo = this.serial = this.estadoActivo = this.unidadMedida = temporal = temporalGuardar = this.foto = this.fotoFicha = this.cedula = this.responsable = "";
                    this.fechaCompra = "";
                    this.fechaActivacion = "";
                    this.depreacionBloque = true;
                    this.mesesDepreciados = 0;
                }
                else {
                    Swal.fire({
                        icon: 'warning',
                        title: 'Falta información',
                        text: 'Placa, nombre, valor, estado, fecha de compra, fecha de activación y unidad de medida'
                    })
                }
            }
            else {
                Swal.fire({
                    icon: 'warning',
                    title: 'Falta información',
                    text: 'Clase, grupo, tipo, prototipo, dependencia, ubicación, centro de costos y disposición del activo'
                })
            }
        },

        eliminarActivoAgregado: function(activo) {


            Swal.fire({
                icon: 'question',
                title: 'Esta seguro que quiere eliminar?',
                showDenyButton: true,
                confirmButtonText: 'Eliminar!',
                denyButtonText: 'Cancelar',
                }).then((result) => {
                if (result.isConfirmed) {

                    this.totalActivos = parseFloat(this.totalActivos) - parseFloat(activo[10]);

                    var i = this.activos.indexOf( activo );
                    
                    if ( i !== -1 ) {
                        this.activos.splice( i, 1 );
                        this.activosGuardar.splice( i, 1 );
                    }   
                } 
            })  

            console.log(this.activos);
        },

        guardar: function() {

            if (this.movimiento == 1) {

                if (this.activos != "" && this.activosGuardar != "") {

                    const fechaRegistro = this.fechaRegistro
    
                    if (this.codigoOrden != "" && fechaRegistro != "" && this.origen != "") {

                        if ((this.origen == 7) || (this.origen == 1 && this.totalActivos <= this.valor) || (this.origen == 4 && this.totalActivos <= this.valor)) {

                            Swal.fire({
                                icon: 'question',
                                title: 'Esta seguro que quiere guardar?',
                                showDenyButton: true,
                                confirmButtonText: 'Guardar!',
                                denyButtonText: 'Cancelar',
                                }).then((result) => {
                                if (result.isConfirmed) {
                
                                    var formData = new FormData();

                                    formData.append("orden", this.codigoOrden);
                                    formData.append("fechaRegistro", fechaRegistro);
                                    formData.append("origen", this.origen);
                                    formData.append("valorTotal", this.totalActivos);
                                    formData.append("documento", this.documento);

                                    for(let i=0; i <= this.activosGuardar.length-1; i++) {

                                        const val = Object.values(this.activosGuardar[i]).length-1;
                
                                        for(let x = 0; x <= val; x++) {
                                            formData.append("activosGuardar["+i+"][]", Object.values(this.activosGuardar[i])[x]);
                                        }
                                    }

                                    axios.post(URL + '?action=guardar', formData)
                                    .then((response) => {
                                        
                                        if(response.data.insertaBien){
                                            Swal.fire({
                                                position: 'top-end',
                                                icon: 'success',
                                                title: 'La orden de activación ha sido guardada con exito',
                                                showConfirmButton: false,
                                                timer: 1500
                                            }).then((response) => {
                                                app.redireccionar();
                                            });
                                        }
                                        else{
                                        
                                            Swal.fire(
                                                'Error!',
                                                'No se pudo guardar.',
                                                'error'
                                            );
                                        }
                                        
                                    });
                                } 
                            })  
                        }
                        else {
                            Swal.fire({
                                icon: 'info',
                                title: 'No disponible',
                                text: 'Este origen no se encuentra disponible en este momento'
                            })        
                        }
                    }
                    else {
                        Swal.fire({
                            icon: 'warning',
                            title: 'No tiene ningún activo agregado',
                            text: 'Para poder guardar debes agregar algún activo'
                        })    
                    }
                }
                else {
                    Swal.fire({
                        icon: 'warning',
                        title: 'No tiene ningún activo agregado',
                        text: 'Para poder guardar debes agregar algún activo'
                    })
                }
            }
            else if (this.movimiento == 3) {

                const fechaRev = this.fechaReversion;

               if (this.ordenesDet != "" && this.ordenRev != "" && fechaRev != "" && this.origenRev != "") {
 
                    Swal.fire({
                        icon: 'question',
                        title: 'Esta seguro que quiere reversar?',
                        showDenyButton: true,
                        confirmButtonText: 'Reversar!',
                        denyButtonText: 'Cancelar',
                        }).then((result) => {
                        if (result.isConfirmed) {
        
                            var formData = new FormData();

                            formData.append("orden", this.ordenRev); 
                            formData.append("origen", this.origenRev); 
                            formData.append("documento", this.documentoRev); 
                            formData.append("valor", this.valorRev); 
                            formData.append("fechaRev", fechaRev); 

                            axios.post(URL + '?action=guardarRev', formData)
                            .then((response) => {
                                
                                if(response.data.insertaBien){
                                    Swal.fire({
                                        position: 'top-end',
                                        icon: 'success',
                                        title: 'La reversión se ha guardado con exito',
                                        showConfirmButton: false,
                                        timer: 1500
                                    }).then((response) => {
                                        app.redireccionar();
                                    });
                                }
                                else{
                                
                                    Swal.fire(
                                        'Error!',
                                        'No se pudo guardar.',
                                        'error'
                                    );
                                }
                                
                            });
                        } 
                    })  
               }
            }
        },

        redireccionar: function() {

            location.href = "acti-ordenActivacion.php";
        },

        desplegarOrdenes: async function() {

            const fechaReversion = this.fechaReversion;

            if (fechaReversion != "") {

                var formData = new FormData();

                formData.append("fechaReversion", fechaReversion)

                await axios.post(URL + '?action=buscaOrdenes', formData)
                .then((response) => {
                    
                    this.ordenes = response.data.ordenes;
                });

                this.showOrdenes = true;
            }
            else {
                Swal.fire({
                    icon: 'info',
                    title: 'Fecha vacia',
                    text: 'Debes seleccionar primero una fecha'
                })   
            }
        },

        seleccionarOrden: async function(orden) {

            if (orden != "") {

                var formData = new FormData();

                formData.append("orden", orden[0])

                await axios.post(URL + '?action=buscaOrdenesDet', formData)
                .then((response) => {
                    
                    this.ordenesDet = response.data.ordenesDet;
                    console.log(response.data);
                });

                this.ordenRev = orden[0];
                this.fechaRev = orden[1];
                this.origenRev = orden[2];
                this.documentoRev = orden[3];
                this.valorRev = orden[4];
                this.showOrdenes = false;
            }
        },
    },
});