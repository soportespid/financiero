const URL = 'activos_fijos/ordenActivacion/buscar/acti-buscarOrdenesActivacion.php';

var app = new Vue({
    el: '#myapp',
	data:{

        error: '',
        loading: false,

        codigoOrden: '',
        placa: '',
        datos: '',
        fechaIni: '',
        fechaFin: '',
        activos: [],
    },

    mounted: function(){

    },

    methods: 
    {
        //Desglosar array
        toFormData: function(obj){
            var form_data = new FormData();
            for(var key in obj){
                form_data.append(key, obj[key]);
            }
            return form_data;
        },

        formatonumero: function(valor){
            return new Intl.NumberFormat("en-US", {style: "currency", currency: "USD"}).format(valor);
        },

        buscarDatos: async function() {
   
            const fechaIni = document.getElementById('fechaIni').value;
            const fechaFin = document.getElementById('fechaFin').value;

            if ((fechaIni != "" && fechaFin != "") || (fechaIni == "" && fechaFin == "")) {

                var formData = new FormData();

                formData.append("orden", this.codigoOrden);
                formData.append("placa", this.placa);
                formData.append("fechaIni", fechaIni);
                formData.append("fechaFin", fechaFin);

                await axios.post(URL+'?action=datosIniciales', formData)
                .then((response) => {
                    
                    this.activos = response.data.activos;
                    console.log(this.activos[9]);
                });
            }
            else {
                Swal.fire("Mal uso de filtro", "Debes llenar las dos fechas o no llenar ninguna fecha", "info");
            }
            
        },

        visualizar: function(activo) {

            const x = "acti-visualizarOrdenActivacion.php?orden="+activo[0];
            window.open(x, '_blank');
            window.focus();
        },
    },
});