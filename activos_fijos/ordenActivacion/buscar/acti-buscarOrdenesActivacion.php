e<?php
	require_once '../../../comun.inc';
    require '../../../funciones.inc';
    require '../../../conversor.php';
    session_start();

	$linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");

    $out = array('error' => false);

    $action = "show";

    if(isset($_GET['action'])){
        $action = $_GET['action'];
    }

    if ($action == "datosIniciales") {

        $activos = array();

        if ($_POST['orden'] != "") {

            $filtroOrdenes = "AND DET.codigo = '$_POST[orden]'";
        }
        else {
            $filtroOrdenes = "";
        }

        if ($_POST['placa'] != "") {

            $filtroPlacas = "AND DET.placa = '$_POST[placa]'";
        }
        else {
            $filtroPlacas = "";
        }

        if ($_POST['fechaIni'] != "" && $_POST['fechaFin'] != "") {

            preg_match("/([0-9]{2})\/([0-9]{2})\/([0-9]{4})/", $_POST['fechaIni'], $f);
			$fechaIni = $f[3]."-".$f[2]."-".$f[1];

            preg_match("/([0-9]{2})\/([0-9]{2})\/([0-9]{4})/", $_POST['fechaFin'], $f);
			$fechaFin = $f[3]."-".$f[2]."-".$f[1];

            $filtroFechas = "AND CAB.fechareg BETWEEN '$fechaIni' AND '$fechaFin' ";
        }
        else {
            $filtroFechas = "";
        }

        $sqlActivos = "SELECT CAB.codigo, CAB.origen, DET.placa, DET.nombre, CAB.fechareg, DET.clasificacion, DET.grupo, DET.tipo, DET.valor, CAB.estado, DET.area FROM acticrearact_det AS DET INNER JOIN 
        acticrearact AS CAB ON CAB.codigo = DET.codigo WHERE CAB.estado = 'S' $filtroOrdenes $filtroFechas $filtroPlacas ORDER BY CAST(DET.codigo AS unsigned) DESC";
        $resActivos = mysqli_query($linkbd, $sqlActivos);
        while ($rowActivos = mysqli_fetch_row($resActivos)) {
            
            $datos = array();
            
            $fechaRegistro = date("d/m/Y", strtotime($rowActivos[4]));

            $sqlClase = "SELECT nombre FROM actipo WHERE codigo = '$rowActivos[5]' AND niveluno = '0' AND estado = 'S'";
            $rowClase = mysqli_fetch_row(mysqli_query($linkbd, $sqlClase));

            $clase = $rowActivos[5] . " - " . $rowClase[0];

            $sqlGrupo = "SELECT nombre FROM actipo WHERE codigo = '$rowActivos[6]' AND niveluno='$rowActivos[5]' AND estado = 'S'";
            $rowGrupo = mysqli_fetch_row(mysqli_query($linkbd, $sqlGrupo));

            $grupo = $rowActivos[6] . " - " . $rowGrupo[0];

            $sqlTipo = "SELECT nombre FROM actipo WHERE codigo = '$rowActivos[7]' AND niveluno = '$rowActivos[6]' AND niveldos = '$rowActivos[5]' AND estado = 'S'";
            $rowTipo = mysqli_fetch_row(mysqli_query($linkbd, $sqlTipo));

            $tipo = $rowActivos[7] . " - " . $rowTipo[0];

            $sqlArea = "SELECT nombrearea FROM planacareas WHERE estado = 'S' AND codarea = '$rowActivos[10]'";
            $rowArea = mysqli_fetch_row(mysqli_query($linkbd, $sqlArea));

            $area = $rowActivos[10] . " - " . $rowArea[0];

            switch ($rowActivos[9]) {
                case 'S':
                    $estado = "imagenes/aprobado.png";
                    $nEstado = "Activo";
                break;

                case 'N':
                    $estado = "imagenes/rechazar.png";
                    $nEstado = "Reversado";
                break;
            }

            array_push($datos, $rowActivos[0]);
            array_push($datos, $fechaRegistro);
            array_push($datos, $rowActivos[1]);
            array_push($datos, $rowActivos[2]);
            array_push($datos, $rowActivos[3]);
            array_push($datos, $clase);
            array_push($datos, $grupo);
            array_push($datos, $tipo);
            array_push($datos, $rowActivos[8]);
            array_push($datos, $estado);
            array_push($datos, $nEstado);
            array_push($datos, $area);
            array_push($activos, $datos);
        }

        $out['activos'] = $activos;
    }

    header("Content-type: application/json");
    echo json_encode($out);
    die();