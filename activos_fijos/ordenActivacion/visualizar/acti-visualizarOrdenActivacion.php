e<?php
	require_once '../../../comun.inc';
    require '../../../funciones.inc';
    require '../../../conversor.php';
    session_start();

	$linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");

    $out = array('error' => false);

    $action = "show";

    if(isset($_GET['action'])){
        $action = $_GET['action'];
    }

    if ($action == "datosIniciales") {

        $activos = array();

        if ($_POST['orden'] != "") {

            $filtroOrdenes = "AND DET.codigo = '$_POST[orden]'";
        }
        

        $sqlCab = "SELECT CAB.origen, CAB.fechareg, CAB.documento, CAB.valor FROM acticrearact AS CAB WHERE CAB.codigo = '$_POST[orden]'";
        $rowCab = mysqli_fetch_row(mysqli_query($linkbd, $sqlCab));

        $fechaRegistro = date("d/m/Y", strtotime($rowCab[1]));

        $out['origen'] = $rowCab[0];
        $out['fechaReg'] = $fechaRegistro;
        $out['documento'] = $rowCab[2];
        $out['valor'] = $rowCab[3];

        $sqlActivos = "SELECT CAB.codigo, CAB.origen, DET.placa, DET.nombre, CAB.fechareg, DET.clasificacion, DET.grupo, DET.tipo, DET.prototipo, DET.area, DET.ubicacion, DET.cc, DET.dispoact, DET.valor, DET.referencia, DET.modelo, DET.serial, DET.estadoactivo, DET.fechacom, DET.fechact, DET.unidadmed, DET.bloque, DET.saldomesesdep, DET.foto, DET.ficha FROM acticrearact_det AS DET INNER JOIN 
        acticrearact AS CAB ON CAB.codigo = DET.codigo WHERE CAB.estado = 'S' $filtroOrdenes ORDER BY CAST(DET.codigo AS unsigned) DESC";
        $resActivos = mysqli_query($linkbd, $sqlActivos);
        while ($rowActivos = mysqli_fetch_row($resActivos)) {

            $datos = array();

            $fechaCompra = date("d/m/Y", strtotime($rowActivos[18]));
            $fechaActivacion = date("d/m/Y", strtotime($rowActivos[19]));

            $sqlClase = "SELECT nombre FROM actipo WHERE codigo = '$rowActivos[5]' AND niveluno = '0' AND estado = 'S'";
            $rowClase = mysqli_fetch_row(mysqli_query($linkbd, $sqlClase));

            $clase = $rowActivos[5] . " - " . $rowClase[0];

            $sqlGrupo = "SELECT nombre FROM actipo WHERE codigo = '$rowActivos[6]' AND niveluno='$rowActivos[5]' AND estado = 'S'";
            $rowGrupo = mysqli_fetch_row(mysqli_query($linkbd, $sqlGrupo));

            $grupo = $rowActivos[6] . " - " . $rowGrupo[0];

            $sqlTipo = "SELECT nombre FROM actipo WHERE codigo = '$rowActivos[7]' AND niveluno = '$rowActivos[6]' AND niveldos = '$rowActivos[5]' AND estado = 'S'";
            $rowTipo = mysqli_fetch_row(mysqli_query($linkbd, $sqlTipo));

            $tipo = $rowActivos[7] . " - " . $rowTipo[0];

            $sqlPrototipo = "SELECT nombre FROM acti_prototipo WHERE id = $rowActivos[8] AND estado = 'S'";
            $rowPrototipo = mysqli_fetch_row(mysqli_query($linkbd, $sqlPrototipo));

            $prototipo = $rowActivos[8] . " - " . $rowPrototipo[0];

            $sqlDependencia = "SELECT nombrearea FROM planacareas WHERE codarea = '$rowActivos[9]' AND estado = 'S'";
            $rowDependencia = mysqli_fetch_row(mysqli_query($linkbd, $sqlDependencia));

            $dependencia = $rowActivos[9] . " - " . $rowDependencia[0];

            $sqlUbicaciones = "SELECT id_cc, nombre FROM actiubicacion WHERE id_cc = '$rowActivos[10]' AND estado = 'S'";
            $rowUbicaciones = mysqli_fetch_row(mysqli_query($linkbd, $sqlUbicaciones));

            $ubicacion = $rowActivos[10] . " - " . $rowUbicaciones[1];

            $sqlCentroCosto = "SELECT id_cc, nombre FROM centrocosto WHERE id_cc = '$rowActivos[11]' AND estado = 'S'";
            $rowCentroCosto = mysqli_fetch_row(mysqli_query($linkbd, $sqlCentroCosto));

            $centroCosto = $rowActivos[11] . " - " . $rowCentroCosto[1];

            $sqlDisposicion = "SELECT id, nombre FROM acti_disposicionactivos WHERE id = '$rowActivos[12]' AND estado = 'S' ";
            $rowDisposicion = mysqli_fetch_row(mysqli_query($linkbd, $sqlDisposicion));

            $disposicion = $rowActivos[12] . " - " . $rowDisposicion[1];

            switch ($rowActivos[20]) {
                case 1:
                    $unidadMedida = "Unidad";
                break;

                case 2:
                    $unidadMedida = "Juego";
                break;

                case 3:
                    $unidadMedida = "Caja";
                break;
            }

            if ($rowActivos[21] == 1) {
                
                $bloque = "Si";
            }
            else {
                $bloque = "No";
            }

            $rutaFoto = "informacion/proyectos/temp/" . $rowActivos[23];
            $rutaFicha = "informacion/proyectos/temp/" . $rowActivos[24];

            if ($rowActivos[23] != "") {

                $foto = "imagenes/foto.png";
            }
            else {
                $foto = "imagenes/nofoto.png";
            }

            if ($rowActivos[24] != "") {

                $ficha = "imagenes/foto.png";
            }
            else {
                $ficha = "imagenes/nofoto.png";
            }

            array_push($datos, $rowActivos[2]);
            array_push($datos, $rowActivos[3]);
            array_push($datos, $clase);
            array_push($datos, $grupo);
            array_push($datos, $tipo);
            array_push($datos, $prototipo);
            array_push($datos, $dependencia);
            array_push($datos, $ubicacion);
            array_push($datos, $centroCosto);
            array_push($datos, $disposicion);
            array_push($datos, $rowActivos[13]);
            array_push($datos, $rowActivos[14]);
            array_push($datos, $rowActivos[15]);
            array_push($datos, $rowActivos[16]);
            array_push($datos, $rowActivos[17]);
            array_push($datos, $fechaCompra);
            array_push($datos, $fechaActivacion);
            array_push($datos, $unidadMedida);
            array_push($datos, $bloque);
            array_push($datos, $rowActivos[22]);
            array_push($datos, $rutaFoto);
            array_push($datos, $rutaFicha);
            array_push($datos, $foto);
            array_push($datos, $ficha);
            array_push($activos, $datos);
        }

        $out['activos'] = $activos;
    }

    header("Content-type: application/json");
    echo json_encode($out);
    die();