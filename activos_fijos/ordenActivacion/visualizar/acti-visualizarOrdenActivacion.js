const URL = 'activos_fijos/ordenActivacion/visualizar/acti-visualizarOrdenActivacion.php';

var app = new Vue({
    el: '#myapp',
	data:{

        error: '',
        loading: false,

        orden: "",
        origen: "",
        fechaRegistro: "",
        documento: "",
        valor: "",
        activos: [],
    },

    mounted: function(){

        this.buscarDatos();
    },

    methods: 
    {
        //Desglosar array
        toFormData: function(obj){
            var form_data = new FormData();
            for(var key in obj){
                form_data.append(key, obj[key]);
            }
            return form_data;
        },

        formatonumero: function(valor){
            return new Intl.NumberFormat("en-US", {style: "currency", currency: "USD"}).format(valor);
        },

        traeParametros: function(name){
            //Captura parametros
            name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
            var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
            results = regex.exec(location.search);
            return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
        },

        buscarDatos: async function() {

            this.orden = this.traeParametros("orden");
            const orden = this.traeParametros("orden");

            if (orden != "") {

                var formData = new FormData();

                formData.append("orden", orden);

                await axios.post(URL+'?action=datosIniciales', formData)
                .then((response) => {
                    
                    this.origen = response.data.origen;
                    this.fechaRegistro = response.data.fechaReg;
                    this.documento = response.data.documento;
                    this.valor = response.data.valor;
                    this.activos = response.data.activos;
                });
            }
            else {
                Swal.fire("Error", "No se tiene el consecutivo de la orden de activación", "warning");
            }
            
        },
    },
});