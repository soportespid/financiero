<?php
	header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=utf8");
	require 'comun.inc';
	require 'funciones.inc';
	session_start();
	$linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");
	cargarcodigopag($_GET['codpag'],$_SESSION["nivel"]);
	date_default_timezone_set("America/Bogota");
?>
<!DOCTYPE >
<html lang=es>
	<head>
    <meta charset="utf-8"/>
		<meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1"/>
		<meta name="viewport" content="user-scalable=no">
		<title>:: IDEAL 10 - Servicios publicos</title>
		<link href="favicon.ico" rel="shortcut icon"/>
		<link href="css/css2z.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
		<link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
		<link href="css/css4.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
        <link href="css/tabs.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css"/>
        <link rel="stylesheet" href="sweetalert2/dist/sweetalert2.min.css">
        <script type="text/javascript" src="css/programas.js"></script>
        <script src="sweetalert2/dist/sweetalert2.min.js"></script>
		<script src="vue/vue.min.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script src="css/programas.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script src="css/calendario.js?<?php echo date('d_m_Y_h_i_s');?>"></script>

		<style>
			[v-cloak]{display : none;}
		</style>
	</head>
	<body>
		<header>
			<table>
				<tr><script>barra_imagenes("serv");</script><?php cuadro_titulos();?></tr>
			</table>
		</header>

		<form name="form2" method="post" action="">
			<section id="myapp" v-cloak >
				<nav>
					<table>
						<tr><?php menu_desplegable("serv");?></tr>
						<tr>
							<td colspan="3" class="cinta">
								<img src="imagenes/add.png" v-on:click="location.href=''" class="mgbt" title="Nuevo">
								<img src="imagenes/guarda.png" v-on:click="guardar" title="Guardar"  class="mgbt">
								<img src="imagenes/busca.png" v-on:click="location.href=''" class="mgbt" title="Buscar">
								<img src="imagenes/nv.png" onClick="mypop=window.open('serv-principal','',''); mypop.focus();" class="mgbt" title="Nueva Ventana">
							</td>
						</tr>
					</table>
				</nav>

				<article>
                    <div>
                        <table class="inicio">
                            <tr>
                                <td class="titulos" colspan="8">.: Recaudo por archivo plano</td>
                                <td class="cerrar" style="width:4%" onClick="location.href='acti-principal.php'">Cerrar</td>
                            </tr>

							<tr>
								<td class="textonew01" style="width:3.5cm;">.: Archivo plano:</td>
								<td><input type="file" ref="doc" accept=".txt" @change="readFile()" /></td>
								<td><input type="button" @click="mostrar" value="Cargar informacion"></td>
							</tr>

							<tr>
								<td class="textonew01" style="width:3.5cm;">.: Fecha de pago:</td>
								<td><input type="text" v-model="fechaRecaudo" style="text-align: center;" readonly></td>

								<td class="textonew01" style="width:3.5cm;">.: Fecha de archivo:</td>
								<td><input type="text" v-model="fechaArchivo" style="text-align: center;" readonly></td>

								<td class="textonew01" style="width:3.5cm;">.: Cuenta de banco:</td>
								<td><input type="text" v-model="numeroBanco" readonly></td>
							</tr>
                        </table>

						<div class='subpantalla' style='height:40vh; width:100%; margin-top:0px;  overflow-x:hidden'>
                            <table class='inicio'>        
                                <tbody>
                                    <tr>
										<th class="titulosnew00" style="width: 10%;">consecutivo</th>
										<th class="titulosnew00" style="width: 10%;">numero factura</th>
                                        <th class="titulosnew00" style="width: 10%;">codigo de usuario</th>
                                        <th class="titulosnew00">nombre</th>
										<th class="titulosnew00" style="width: 10%;">valor de pago</th>
										<th class="titulosnew00" style="width: 10%;">estado de pago</th>
                                    </tr>

                                    <tr v-for="(dato,index) in datos" v-bind:class="(index % 2 ? 'saludo1a' : 'saludo2')">
										<td style="text-align: center;">{{ dato[0] }}</td>
                                        <td style="text-align: center;">{{ dato[1] }}</td>
                                        <td style="text-align: center;">{{ dato[4] }}</td>
										<td>{{ dato[5] }}</td>
										<td style="text-align: center;">{{ formatonumero(dato[6]) }}</td>
										<td style="text-align: center;">
											<span v-if="dato[7] == 'S'" style="padding:4px; border-radius:5px; background:#FFFF00;color:#000;font-weight:bold">Pendiente de pago</span>
                                            <span v-if="dato[7] == 'A'" style="padding:4px; border-radius:5px; background:#0000ff;color:#fff;font-weight:bold">Acuerdo de pago</span>
											<span v-if="dato[7] == 'P'" style="padding:4px; border-radius:5px; background:#008000;color:#fff;font-weight:bold">Pago realizado</span>
											<span v-if="dato[7] == 'V'" style="padding:4px; border-radius:5px; background:#FF0000;color:#fff;font-weight:bold">Vencida</span>
										</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div> 

					<div id="cargando" v-if="loading" class="loading">
						<span>Cargando...</span>
					</div>

				</article>
			</section>
		</form>

		<script src="node_modules/read-excel-file/bundle/read-excel-file.min.js"></script>
		<script src="node_modules/xlsx/dist/xlsx.full.min.js"></script>
        <script src="Librerias/vue/vue.min.js"></script>
		<script src="Librerias/vue/axios.min.js"></script>
		<script type="module" src="servicios_publicos/recaudoArchivoPlano/serv-pagoArchivoPlano.js"></script>
        
	</body>
</html>