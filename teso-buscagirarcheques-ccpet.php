<?php
header("Cache-control: no-cache, no-store, must-revalidate");
header("Content-Type: text/html;charset=utf8");

require "comun.inc";
require "funciones.inc";
require "conversor.php";
require "validaciones.inc";

session_start();
$linkbd = conectar_v7();
$linkbd->set_charset("utf8");

cargarcodigopag($_GET['codpag'], $_SESSION["nivel"]);
date_default_timezone_set("America/Bogota");
?>
<!DOCTYPE>
<html lang="es">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1">
    <meta name="viewport" content="user-scalable=no">
    <title>:: IDEAL 10 - Tesorer&iacute;a</title>
    <link href="favicon.ico" rel="shortcut icon" />
    <link href="css/css2.css" rel="stylesheet" type="text/css" />
    <link href="css/css3.css" rel="stylesheet" type="text/css" />
    <link href="css/tabs.css" rel="stylesheet" type="text/css" />

    <link href="css/style.css" rel="stylesheet" type="text/css" />
    <script type='text/javascript' src='JQuery/jquery-2.1.4.min.js'></script>
    <script type="text/javascript" src="css/programas.js"></script>
    <script type="text/javascript" src="css/calendario.js"></script>
    <script>
        function eliminar(idr) {
            if (confirm("Esta Seguro de Eliminar El Egreso No " + idr)) {
                document.form2.oculto.value = 2;
                document.form2.var1.value = idr;
                document.form2.submit();
            }
        }
        function verUltimaPos(idcta, filas, filtro1, filtro2) {
            var scrtop = $('#divdet').scrollTop();
            var altura = $('#divdet').height();
            var numpag = $('#nummul').val();
            var limreg = $('#numres').val();
            if ((numpag <= 0) || (numpag == "")) { numpag = 0; }
            if ((limreg == 0) || (limreg == "")) { limreg = 10; }
            numpag++;
            location.href = "teso-girarchequesver-ccpet.php?idegre=" + idcta + "&scrtop=" + scrtop + "&totreg=" + filas + "&altura=" + altura + "&numpag=" + numpag + "&limreg=" + limreg + "&filtro1=" + filtro1 + "&filtro2=" + filtro2;
        }
        function crearexcel() {
            document.form2.action = "teso-buscagirarchequesexcel.php";
            document.form2.target = "_BLANK";
            document.form2.submit();
            document.form2.action = "";
            document.form2.target = "";
        }
    </script>
    <?php
    $scrtop = $_GET['scrtop'];
    if ($scrtop == "")
        $scrtop = 0;
    echo "<script>window.onload=function(){ $('#divdet').scrollTop(" . $scrtop . ")}</script>";
    $gidcta = $_GET['idcta'];
    if (isset($_GET['filtro1']))
        $_POST['numero'] = $_GET['filtro1'];
    if (isset($_GET['filtro2']))
        $_POST['nombre_1'] = $_GET['filtro2'];

    if (isset($_GET['fini']) && isset($_GET['ffin'])) {
        if (!empty($_GET['fini']) && !empty($_GET['ffin'])) {
            $_POST['fecha_1'] = $_GET['fini'];
            $_POST['fecha2'] = $_GET['ffin'];
        }
    }
    $fech1 = explode("/", $_POST['fecha_1']);
    $fech2 = explode("/", $_POST['fecha2']);
    $f1 = $fech1[2] . "-" . $fech1[1] . "-" . $fech1[0];
    $f2 = $fech2[2] . "-" . $fech2[1] . "-" . $fech2[0];
    ?>
</head>

<body>
    <IFRAME src="alertas.php" name="alertas" id="alertas" style="display:none"></IFRAME>
    <span id="todastablas2"></span>
    <table>
        <tr>
            <script>barra_imagenes("teso");</script><?php cuadro_titulos(); ?>
        </tr>
        <tr><?php menu_desplegable("teso"); ?></tr>
    </table>
    <div class="bg-white group-btn p-1"><button type="button"
            onclick="window.location.href='teso-girarcheques-ccpet.php'"
            class="btn btn-white btn-primary-hover d-flex justify-between align-items-center">
            <span>Nuevo</span>
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 -960 960 960">
                <path d="M440-440H200v-80h240v-240h80v240h240v80H520v240h-80v-240Z"></path>
            </svg>
        </button><button type="button" onclick="mypop=window.open('plan-agenda.php','','');mypop.focus()"
            class="btn btn-white btn-primary-hover d-flex justify-between align-items-center">
            <span class="group-hover:text-white">Agenda</span>
            <svg class="fill-black group-hover:fill-white w-5 h-5" xmlns="http://www.w3.org/2000/svg"
                viewBox="0 -960 960 960">
                <path
                    d="M560-564v-68q33-14 67.5-21t72.5-7q26 0 51 4t49 10v64q-24-9-48.5-13.5T700-600q-38 0-73 9.5T560-564Zm0 220v-68q33-14 67.5-21t72.5-7q26 0 51 4t49 10v64q-24-9-48.5-13.5T700-380q-38 0-73 9t-67 27Zm0-110v-68q33-14 67.5-21t72.5-7q26 0 51 4t49 10v64q-24-9-48.5-13.5T700-490q-38 0-73 9.5T560-454ZM260-320q47 0 91.5 10.5T440-278v-394q-41-24-87-36t-93-12q-36 0-71.5 7T120-692v396q35-12 69.5-18t70.5-6Zm260 42q44-21 88.5-31.5T700-320q36 0 70.5 6t69.5 18v-396q-33-14-68.5-21t-71.5-7q-47 0-93 12t-87 36v394Zm-40 118q-48-38-104-59t-116-21q-42 0-82.5 11T100-198q-21 11-40.5-1T40-234v-482q0-11 5.5-21T62-752q46-24 96-36t102-12q58 0 113.5 15T480-740q51-30 106.5-45T700-800q52 0 102 12t96 36q11 5 16.5 15t5.5 21v482q0 23-19.5 35t-40.5 1q-37-20-77.5-31T700-240q-60 0-116 21t-104 59ZM280-494Z">
                </path>
            </svg>
        </button><button type="button" onclick="window.open('teso-principal');"
            class="btn btn-white btn-primary-hover d-flex justify-between align-items-center">
            <span>Nueva ventana</span>
            <svg class="fill-black group-hover:fill-white w-5 h-5" xmlns="http://www.w3.org/2000/svg"
                viewBox="0 -960 960 960">
                <path
                    d="M320-120v-80h80v-80H160q-33 0-56.5-23.5T80-360v-400q0-33 23.5-56.5T160-840h640q33 0 56.5 23.5T880-760v400q0 33-23.5 56.5T800-280H560v80h80v80H320ZM160-360h640v-400H160v400Zm0 0v-400 400Z">
                </path>
            </svg>
        </button><button type="button"
            onclick="mypop=window.open('/financiero/teso-buscagirarcheques-ccpet.php','','');mypop.focus();"
            class="btn btn-white btn-primary-hover d-flex justify-between align-items-center">
            <span class="group-hover:text-white">Duplicar pantalla</span>
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 -960 960 960">
                <path
                    d="M640-160v-360H160v360h480Zm80-200v-80h80v-360H320v200h-80v-200q0-33 23.5-56.5T320-880h480q33 0 56.5 23.5T880-800v360q0 33-23.5 56.5T800-360h-80ZM160-80q-33 0-56.5-23.5T80-160v-360q0-33 23.5-56.5T160-600h480q33 0 56.5 23.5T720-520v360q0 33-23.5 56.5T640-80H160Zm400-603ZM400-340Z">
                </path>
            </svg>
        </button><button type="button" onclick="crearexcel()"
            class="btn btn-success btn-success-hover d-flex justify-between align-items-center">
            <span>Exportar Excel</span>
            <svg class="fill-black group-hover:fill-white w-4 h-4" xmlns="http://www.w3.org/2000/svg"
                viewBox="0 0 384 512"><!--!Font Awesome Free 6.5.1 by @fontawesome - https://fontawesome.com License - https://fontawesome.com/license/free Copyright 2024 Fonticons, Inc. -->
                <path
                    d="M48 448V64c0-8.8 7.2-16 16-16H224v80c0 17.7 14.3 32 32 32h80V448c0 8.8-7.2 16-16 16H64c-8.8 0-16-7.2-16-16zM64 0C28.7 0 0 28.7 0 64V448c0 35.3 28.7 64 64 64H320c35.3 0 64-28.7 64-64V154.5c0-17-6.7-33.3-18.7-45.3L274.7 18.7C262.7 6.7 246.5 0 229.5 0H64zm90.9 233.3c-8.1-10.5-23.2-12.3-33.7-4.2s-12.3 23.2-4.2 33.7L161.6 320l-44.5 57.3c-8.1 10.5-6.3 25.5 4.2 33.7s25.5 6.3 33.7-4.2L192 359.1l37.1 47.6c8.1 10.5 23.2 12.3 33.7 4.2s12.3-23.2 4.2-33.7L222.4 320l44.5-57.3c8.1-10.5 6.3-25.5-4.2-33.7s-25.5-6.3-33.7 4.2L192 280.9l-37.1-47.6z">
                </path>
            </svg>
        </button>
    </div>
    <form name="form2" method="post" action="teso-buscagirarcheques-ccpet.php">
        <?php
        if ($_POST['oculto'] == "") {
            $_POST['iddeshff'] = 0;
            $_POST['tabgroup1'] = 1;
        }
        switch ($_POST['tabgroup1']) {
            case 1:
                $check1 = 'checked';
                break;
            case 2:
                $check2 = 'checked';
                break;
            case 3:
                $check3 = 'checked';
                break;
        }
        ?>
        <?php
        if ($_GET['numpag'] != "") {
            $oculto = $_POST['oculto'];
            if ($oculto != 2) {
                $_POST['numres'] = $_GET['limreg'];
                $_POST['numpos'] = $_GET['limreg'] * ($_GET['numpag'] - 1);
                $_POST['nummul'] = $_GET['numpag'] - 1;
            }
        } else {
            if ($_POST['nummul'] == "") {
                $_POST['numres'] = 10;
                $_POST['numpos'] = 0;
                $_POST['nummul'] = 0;
            }
        }
        ?>
        <table class="inicio" align="center">
            <tr>
                <td class="titulos" colspan="8">:. Buscar Pagos</td>
                <td class="cerrar" style="width:7%;"><a href="teso-principal.php">&nbsp;Cerrar</a></td>
            </tr>
            <tr>
                <td class="saludo1" style="width:2.2cm;">Fecha Inicial:</td>
                <td style="width:9%;"><input name="fecha_1" type="text" value="<?php echo $_POST['fecha_1'] ?>"
                        maxlength="10" onKeyPress="javascript:return solonumeros(event)"
                        onKeyUp="return tabular(event,this)" onchange="" id="fc_1198971545"
                        onKeyDown="mascara(this,'/',patron,true)" style="width:75%;" title="DD/MM/YYYY" />&nbsp;<a
                        href="#" onClick="displayCalendarFor('fc_1198971545');" title="Calendario"><img
                            src="imagenes/calendario04.png" style="width:20px;" /></a></td>
                <td class="saludo1" style="width:2.2cm;">Fecha Final:</td>
                <td style="width:9%;"><input name="fecha2" type="text" value="<?php echo $_POST['fecha2'] ?>"
                        maxlength="10" onKeyPress="javascript:return solonumeros(event)"
                        onKeyUp="return tabular(event,this)" onchange="" id="fc_1198971546"
                        onKeyDown="mascara(this,'/',patron,true)" style="width:75%;" title="DD/MM/YYYY" />&nbsp;<a
                        href="#" onClick="displayCalendarFor('fc_1198971546');" title="Calendario"><img
                            src="imagenes/calendario04.png" style="width:20px;" /></a></td>
                <td class="saludo1" style="width:5cm;">N&deg; Egreso, N&deg; Orden o Concepto:</td>
                <td style="width:20%;"><input type="search" name="numero" id="numero"
                        value="<?php echo $_POST['numero']; ?>" style="width:98%;"></td>
                <td class="saludo1" style="width:2cm;">Beneficiario: </td>
                <td>
                    <input type="search" name="nombre_1" id="nombre_1" value="<?php echo $_POST['nombre_1']; ?>"
                        style="width:60%;">&nbsp;&nbsp;
                    <input type="button" name="bboton" onClick="limbusquedas();"
                        value="&nbsp;&nbsp;Buscar&nbsp;&nbsp;" />
                </td>
            </tr>
        </table>
        <input type="hidden" name="oculto" id="oculto" value="1">
        <input type="hidden" name="var1" value=<?php echo $_POST['var1']; ?>>
        <input type="hidden" name="numres" id="numres" value="<?php echo $_POST['numres']; ?>" />
        <input type="hidden" name="numpos" id="numpos" value="<?php echo $_POST['numpos']; ?>" />
        <input type="hidden" name="nummul" id="nummul" value="<?php echo $_POST['nummul']; ?>" />
        <div class="tabsmeci" style="height:64.5%; width:99.6%;">
            <div class="tab">
                <input type="radio" id="tab-1" name="tabgroup1" value="1" <?php echo $check1; ?>>
                <label for="tab-1">Pagos General</label>
                <div class="content" style="overflow-x:hidden;" id="divdet">
                    <?php
                    $oculto = $_POST['oculto'];
                    $vigusu = vigencia_usuarios($_SESSION['cedulausu']);
                    $_POST['vigencia'] = $vigusu;
                    if ($_POST['oculto'] == 2) {/*
                   $sqlr="select * from tesoegresos where id_egreso=$_POST[var1] and tipo_mov='201' ";
                   $resp = mysqli_query($linkbd, $sqlr);
                   $row=mysqli_fetch_row($resp);
                   $op=$row[2];
                   $vpa=$row[7];
                   //********Comprobante contable en 000000000000
                    $sqlr="update comprobante_cab set total_debito=0,total_credito=0,estado='0' where tipo_comp='6' and numerotipo=$row[0]";
                    mysqli_query($linkbd, $sqlr);
                    $sqlr="update comprobante_det set valdebito=0,valcredito=0 where id_comp='6 $row[0]'";
                    mysqli_query($linkbd, $sqlr);
                    $sqlr="update pptocomprobante_cab set estado='0' where tipo_comp='11' and numerotipo=$row[0]";
                   mysqli_query($linkbd, $sqlr);
                   //********RETENCIONES
                    $sqlr="delete from pptoretencionpago where idrecibo=$row[0] and vigencia='".$_SESSION['vigencia']."'";
                   mysqli_query($linkbd, $sqlr);
                      $sqlr="delete from pptorecibopagoppto where idrecibo=$row[0] and vigencia='".$_SESSION['vigencia']."'";
                   mysqli_query($linkbd, $sqlr);
                     $sqlr="update tesoordenpago  set estado='S' where id_orden=$op and tipo_mov='201' ";
                    mysqli_query($linkbd, $sqlr);
                     $sqlr="update tesoegresos set estado='N' where id_egreso=$_POST[var1] and tipo_mov='201' ";
                    mysqli_query($linkbd, $sqlr);	 */
                    }
                    $crit1 = "";
                    $crit2 = "";
                    if ($_POST['numero'] != "") {
                        $crit1 = " AND concat_ws(' ', id_egreso, id_orden, concepto) LIKE '%$_POST[numero]%'";
                    }
                    if ($_POST['nombre_1'] != "") {
                        $crit2 = "AND EXISTS (SELECT cedulanit FROM terceros WHERE concat_ws(' ', nombre1,nombre2,apellido1,apellido2,razonsocial,cedulanit) LIKE '%$_POST[nombre_1]%' AND terceros.cedulanit = tesoegresos.tercero)";
                    }
                    if ($_POST['numero'] != "" || $_POST['nombre_1'] != "" || $_POST['fecha_1'] != "" || $_POST['fecha2'] != "") {
                        $sqlr = "SELECT * FROM tesoegresos WHERE id_egreso>-1 $crit1 $crit2  and tipo_mov='201'";
                        if (isset($_POST['fecha_1']) && isset($_POST['fecha2'])) {
                            if (!empty($_POST['fecha_1']) && !empty($_POST['fecha2'])) {
                                $sqlr = "SELECT * FROM tesoegresos WHERE id_egreso>-1 $crit1 $crit2 and fecha between '$f1' AND '$f2' and tipo_mov='201'";
                            }
                        }
                        $resp = mysqli_query($linkbd, $sqlr);
                        $_POST['numtop'] = mysqli_num_rows($resp);
                        $nuncilumnas = ceil($_POST['numtop'] / $_POST['numres']);
                        $cond2 = "";
                        if ($_POST['numres'] != "-1") {
                            $cond2 = "LIMIT $_POST[numpos], $_POST[numres]";
                        }
                        $sqlr = "SELECT * FROM tesoegresos WHERE id_egreso>-1 $crit1 $crit2 and tipo_mov='201' ORDER BY id_egreso DESC $cond2";
                        if (isset($_POST['fecha_1']) && isset($_POST['fecha2'])) {
                            if (!empty($_POST['fecha_1']) && !empty($_POST['fecha2'])) {
                                $sqlr = "SELECT * FROM tesoegresos WHERE id_egreso>-1 $crit1 $crit2 and fecha between '$f1' AND '$f2' and tipo_mov='201' ORDER BY id_egreso DESC $cond2";
                            }
                        }
                        $resp = mysqli_query($linkbd, $sqlr);
                    }

                    $con = 1;
                    $numcontrol = $_POST['nummul'] + 1;
                    if ($nuncilumnas == $numcontrol) {
                        $imagenforward = "<img src='imagenes/forward02.png' style='width:17px'>";
                        $imagensforward = "<img src='imagenes/skip_forward02.png' style='width:16px' >";
                    } else {
                        $imagenforward = "<img src='imagenes/forward01.png' style='width:17px' title='Siguiente' onClick='numsiguiente()'>";
                        $imagensforward = "<img src='imagenes/skip_forward01.png' style='width:16px' title='Fin' onClick='saltocol(\"$nuncilumnas\")'>";
                    }
                    if ($_POST['numpos'] == 0) {
                        $imagenback = "<img src='imagenes/back02.png' style='width:17px'>";
                        $imagensback = "<img src='imagenes/skip_back02.png' style='width:16px'>";
                    } else {
                        $imagenback = "<img src='imagenes/back01.png' style='width:17px' title='Anterior' onClick='numanterior();'>";
                        $imagensback = "<img src='imagenes/skip_back01.png' style='width:16px' title='Inicio' onClick='saltocol(\"1\")'>";
                    }
                    $ntips1 = 7;
                    $ntips2 = 9;
                    echo "
					<table class='inicio' align='center' >
						<tr>
							<td colspan='$ntips1' class='titulos'>.: Resultados Busqueda:</td>
							<td class='submenu'>
								<select name='renumres' id='renumres' onChange='cambionum();' style='width:100%'>
									<option value='10'";
                    if ($_POST['renumres'] == '10') {
                        echo 'selected';
                    }
                    echo ">10</option>
									<option value='20'";
                    if ($_POST['renumres'] == '20') {
                        echo 'selected';
                    }
                    echo ">20</option>
									<option value='30'";
                    if ($_POST['renumres'] == '30') {
                        echo 'selected';
                    }
                    echo ">30</option>
									<option value='50'";
                    if ($_POST['renumres'] == '50') {
                        echo 'selected';
                    }
                    echo ">50</option>
									<option value='100'";
                    if ($_POST['renumres'] == '100') {
                        echo 'selected';
                    }
                    echo ">100</option>
									<option value='-1'";
                    if ($_POST['renumres'] == '-1') {
                        echo 'selected';
                    }
                    echo ">Todos</option>
								</select>
							</td>
						</tr>
						<tr><td colspan='$ntips2'>Pagos Encontrados: $_POST[numtop]</td></tr>
						<tr>
							<td class='titulos2'>Egreso</td>
							<td class='titulos2'>Orden Pago</td>
							<td class='titulos2'>Nombre</td>
							<td class='titulos2' width='6%'>Fecha</td>
							<td class='titulos2' style='text-align:center' width='7%'>Valor</td>
							<td class='titulos2' style='text-align:center'>Concepto</td>
							<td class='titulos2' width='4%'><center>Estado</td>
							<td class='titulos2' width='8%'><center>Medio de Pago</td>";
                    echo "</tr>";
                    $iter = 'zebra1';
                    $iter2 = 'zebra2';
                    $filas = 1;

                    if ($_POST['fecha'] == '' && $_POST['fecha2'] == '' && $_POST['nombre_1'] == '' && $_POST['numero'] == '') {
                        echo "
						<table class='inicio'>
							<tr>
								<td class='saludo1' style='text-align:center;width:100%;font-size:25px'>Utilice el filtro de busqueda</td>
							</tr>
						</table>";
                    } elseif (@mysqli_num_rows($resp) == 0 || @mysqli_num_rows($resp) == '0') {
                        echo "
						<table class='inicio'>
							<tr>
								<td class='saludo1' style='text-align:center;width:100%;font-size:25px'>No hay resultados de su busqueda.</td>
							</tr>
						</table>";
                    } else {
                        while ($row = mysqli_fetch_row($resp)) {
                            $ntr = buscatercero($row[11]);
                            if ($gidcta != "") {
                                if ($gidcta == $row[0]) {
                                    $estilo = 'background-color:#FF9';
                                } else {
                                    $estilo = "";
                                }
                            } else {
                                $estilo = "";
                            }
                            $idcta = "'" . $row[0] . "'";
                            $numfil = "'" . $filas . "'";
                            $filtro1 = "'" . $_POST['numero'] . "'";
                            $filtro2 = "'" . $_POST['nombre_1'] . "'";
                            if ($_SESSION["preditar"] == 1) {
                                echo "<tr class='$iter' onMouseOver=\"anterior=this.style.backgroundColor;this.style.backgroundColor='#40b3ff';\"
								onMouseOut=\"this.style.backgroundColor=anterior\" onDblClick=\"verUltimaPos($idcta, $numfil, $filtro1, $filtro2)\" style='text-transform:uppercase; $estilo'>";
                            } else {
                                echo "<tr class='$iter' onMouseOver=\"anterior=this.style.backgroundColor;this.style.backgroundColor='#40b3ff';\"
								onMouseOut=\"this.style.backgroundColor=anterior\" style='text-transform:uppercase; $estilo'>";
                            }
                            echo "
								<td >$row[0]</td>
								<td >$row[2]</td>
								<td >$ntr</td>
								<td >$row[3]</td>
								<td >" . number_format($row[7], 2) . "</td>
								<td >" . strtoupper($row[8]) . "</td>";
                            switch ($row[13]) {
                                case "S":
                                    echo "<td ><center><img src='imagenes/confirm.png'></center></td>";
                                    break;
                                case "P":
                                    echo "<td ><center><img src='imagenes/dinero3.png'></center></td>";
                                    break;
                                case "N":
                                    echo "<td ><center><img src='imagenes/del3.png'></center></td>";
                                    break;
                                case "R":
                                    echo "<td ><center><img src='imagenes/reversado.png' style='width:18px'></center></td>";
                                    break;
                            }
                            $sqlrMedioPago = "SELECT medio_pago FROM tesoordenpago WHERE id_orden=$row[2]";
                            $respMedioPago = mysqli_query($linkbd, $sqlrMedioPago);
                            $rowMedioPago = mysqli_fetch_row($respMedioPago);
                            if ($rowMedioPago[0] == 2)
                                $medioPago = "SSF";
                            else
                                $medioPago = "CSF";

                            echo "<td style='text-align:center;'>" . $medioPago . "</td>";

                            echo "<input type='hidden' name='egreso[]' 		id='egreso[]' 		value='" . $row[0] . "'>";
                            echo "<input type='hidden' name='ordenpago[]'	id='ordenpago[]' 	value='" . $row[2] . "'>";
                            echo "<input type='hidden' name='tercero[]' 	id='tercero[]' 		value='" . $row[11] . "'>";
                            echo "<input type='hidden' name='nombre[]' 		id='nombre[]' 		value='" . $ntr . "'>";
                            echo "<input type='hidden' name='fecha[]' 		id='fecha[]' 		value='" . $row[3] . "'>";
                            echo "<input type='hidden' name='valor[]' 		id='valor[]' 		value='" . $row[7] . "'>";
                            echo "<input type='hidden' name='concepto[]' 	id='concepto[]' 	value='" . $row[8] . "'>";
                            echo "<input type='hidden' name='estado[]' 		id='estado[]' 		value='" . $row[13] . "'>";
                            echo "<input type='hidden' name='mediopago[]' 	id='mediopago[]' 	value='" . $medioPago . "'>";

                            $con += 1;
                            $aux = $iter;
                            $iter = $iter2;
                            $iter2 = $aux;
                            $filas++;
                        }
                    }

                    echo "
						</table>
						<table class='inicio'>
							<tr>
								<td style='text-align:center;'>
									<a href='#'>$imagensback</a>&nbsp;
									<a href='#'>$imagenback</a>&nbsp;&nbsp;";
                    if ($nuncilumnas <= 9) {
                        $numfin = $nuncilumnas;
                    } else {
                        $numfin = 9;
                    }
                    for ($xx = 1; $xx <= $numfin; $xx++) {
                        if ($numcontrol <= 9) {
                            $numx = $xx;
                        } else {
                            $numx = $xx + ($numcontrol - 9);
                        }
                        if ($numcontrol == $numx) {
                            echo "<a href='#' onClick='saltocol(\"$numx\")'; style='color:#24D915'> $numx </a>";
                        } else {
                            echo "<a href='#' onClick='saltocol(\"$numx\")'; style='color:#000000'> $numx </a>";
                        }
                    }
                    echo "		&nbsp;&nbsp;<a href='#'>$imagenforward</a>
									&nbsp;<a href='#'>$imagensforward</a>
								</td>
							</tr>
						</table>";
                    ?>
                </div>
            </div>
            <div class="tab">
                <input type="radio" id="tab-2" name="tabgroup1" value="1" <?php echo $check2; ?>>
                <label for="tab-2">Pagos Reversados</label>
                <div class="content" style="overflow-x:hidden;" id="divdet">
                    <?php
                    $oculto = $_POST['oculto'];

                    $crit1 = "";
                    $crit2 = "";
                    if ($_POST['numero'] != "") {
                        $crit1 = " AND concat_ws(' ', id_egreso, id_orden, concepto) LIKE '%$_POST[numero]%'";
                    }
                    if ($_POST['nombre_1'] != "") {
                        $crit2 = "AND EXISTS (SELECT cedulanit FROM terceros WHERE concat_ws(' ', nombre1,nombre2,apellido1,apellido2,razonsocial,cedulanit) LIKE '%$_POST[nombre_1]%' AND terceros.cedulanit = tesoegresos.tercero)";
                    }
                    $sqlr = "SELECT * FROM tesoegresos WHERE id_egreso>-1 and vigencia=$vigusu and tipo_mov='401' $crit1 $crit2";
                    $resp = mysqli_query($linkbd, $sqlr);
                    $_POST['numtop'] = mysqli_num_rows($resp);
                    $nuncilumnas = ceil($_POST['numtop'] / $_POST['numres']);
                    $cond2 = "";
                    if ($_POST['numres'] != "-1") {
                        $cond2 = "LIMIT $_POST[numpos], $_POST[numres]";
                    }
                    $sqlr = "SELECT * FROM tesoegresos WHERE id_egreso>-1 $crit1 $crit2 and vigencia=$vigusu and tipo_mov='401'ORDER BY id_egreso DESC $cond2";
                    $resp = mysqli_query($linkbd, $sqlr);
                    $con = 1;
                    $numcontrol = $_POST['nummul'] + 1;
                    if ($nuncilumnas == $numcontrol) {
                        $imagenforward = "<img src='imagenes/forward02.png' style='width:17px'>";
                        $imagensforward = "<img src='imagenes/skip_forward02.png' style='width:16px' >";
                    } else {
                        $imagenforward = "<img src='imagenes/forward01.png' style='width:17px' title='Siguiente' onClick='numsiguiente()'>";
                        $imagensforward = "<img src='imagenes/skip_forward01.png' style='width:16px' title='Fin' onClick='saltocol(\"$nuncilumnas\")'>";
                    }
                    if ($_POST['numpos'] == 0) {
                        $imagenback = "<img src='imagenes/back02.png' style='width:17px'>";
                        $imagensback = "<img src='imagenes/skip_back02.png' style='width:16px'>";
                    } else {
                        $imagenback = "<img src='imagenes/back01.png' style='width:17px' title='Anterior' onClick='numanterior();'>";
                        $imagensback = "<img src='imagenes/skip_back01.png' style='width:16px' title='Inicio' onClick='saltocol(\"1\")'>";
                    }
                    $ntips1 = 7;
                    $ntips2 = 9;
                    if ($_SESSION["preditar"] != 1) {
                        $ntips1 = $ntips1 - 1;
                        $ntips2 = $ntips2 - 1;
                    }
                    if ($_SESSION["preliminar"] != 1) {
                        $ntips1 = $ntips1 - 1;
                        $ntips2 = $ntips2 - 1;
                    }
                    echo "
					<table class='inicio' align='center' >
						<tr>
							<td colspan='$ntips1' class='titulos'>.: Resultados Busqueda:</td>
							<td class='submenu'>
								<select name='renumres' id='renumres' onChange='cambionum();' style='width:100%'>
									<option value='10'";
                    if ($_POST['renumres'] == '10') {
                        echo 'selected';
                    }
                    echo ">10</option>
									<option value='20'";
                    if ($_POST['renumres'] == '20') {
                        echo 'selected';
                    }
                    echo ">20</option>
									<option value='30'";
                    if ($_POST['renumres'] == '30') {
                        echo 'selected';
                    }
                    echo ">30</option>
									<option value='50'";
                    if ($_POST['renumres'] == '50') {
                        echo 'selected';
                    }
                    echo ">50</option>
									<option value='100'";
                    if ($_POST['renumres'] == '100') {
                        echo 'selected';
                    }
                    echo ">100</option>
									<option value='-1'";
                    if ($_POST['renumres'] == '-1') {
                        echo 'selected';
                    }
                    echo ">Todos</option>
								</select>
							</td>
						</tr>
						<tr><td colspan='$ntips2'>Pagos Encontrados: $_POST[numtop]</td></tr>
						<tr>
							<td class='titulos2'>Egreso</td>
							<td class='titulos2'>Orden Pago</td>
							<td class='titulos2'>Usuario</td>
							<td class='titulos2' width='6%'>Fecha</td>
							<td class='titulos2' style='text-align:center' width='7%'>Valor</td>
							<td class='titulos2' style='text-align:center'>Concepto</td>
							<td class='titulos2' width='4%'><center>Estado</td>";
                    if ($_SESSION["preditar"] == 1) {
                        echo "<td class='titulos2' width='4%'><center>Ver</td>";
                    }
                    echo "</tr>";
                    $iter = 'zebra1';
                    $iter2 = 'zebra2';
                    $filas = 1;
                    while ($row = mysqli_fetch_row($resp)) {
                        $ntr = buscatercero($row[11]);
                        if ($gidcta != "") {
                            if ($gidcta == $row[0]) {
                                $estilo = 'background-color:#FF9';
                            } else {
                                $estilo = "";
                            }
                        } else {
                            $estilo = "";
                        }
                        $idcta = "'" . $row[0] . "'";
                        $numfil = "'" . $filas . "'";
                        $filtro1 = "'" . $_POST['numero'] . "'";
                        $filtro2 = "'" . $_POST['nombre_1'] . "'";
                        if ($_SESSION["preditar"] == 1) {
                            echo "<tr class='$iter' onMouseOver=\"anterior=this.style.backgroundColor;this.style.backgroundColor='#40b3ff';\"
							onMouseOut=\"this.style.backgroundColor=anterior\" onDblClick=\"verUltimaPos($idcta, $numfil, $filtro1, $filtro2)\" style='text-transform:uppercase; $estilo'>";
                        } else {
                            echo "<tr class='$iter' onMouseOver=\"anterior=this.style.backgroundColor;this.style.backgroundColor='#40b3ff';\"
							onMouseOut=\"this.style.backgroundColor=anterior\" style='text-transform:uppercase; $estilo'>";
                        }
                        $sqlv = "SELECT valorpago FROM tesoegresos WHERE id_egreso=$row[0] AND tipo_mov='201' ";
                        $resul = mysqli_query($linkbd, $sqlv);
                        $fila = mysqli_fetch_row($resul);
                        $valor = $fila[0];
                        echo "
							<td >$row[0]</td>
							<td >$row[2]</td>
							<td >$row[17]</td>
							<td >$row[3]</td>
							<td >$ " . number_format($valor, 2) . "</td>
							<td >" . strtoupper($row[8]) . "</td>";

                        echo "<td ><center><img src='imagenes/reversado.png' style='width:18px'></center></td>";

                        if ($_SESSION["preditar"] == 1) {
                            echo "<td style='text-align:center;'>
									<a onClick=\"verUltimaPos($idcta, $numfil, $filtro1, $filtro2)\" style='cursor:pointer;'>
										<img src='imagenes/lupa02.png' style='width:18px' title='Ver'>
									</a>
								</td>";
                        }

                        $con += 1;
                        $aux = $iter;
                        $iter = $iter2;
                        $iter2 = $aux;
                        $filas++;
                    }
                    if ($_POST['numtop'] == 0) {
                        echo "
						<table class='inicio'>
							<tr>
								<td class='saludo1' style='text-align:center;width:100%'>Utilice el filtro de busqueda. $tibusqueda</td>
							</tr>
						</table>";
                    }
                    echo "
						</table>
						<table class='inicio'>
							<tr>
								<td style='text-align:center;'>
									<a href='#'>$imagensback</a>&nbsp;
									<a href='#'>$imagenback</a>&nbsp;&nbsp;";
                    if ($nuncilumnas <= 9) {
                        $numfin = $nuncilumnas;
                    } else {
                        $numfin = 9;
                    }
                    for ($xx = 1; $xx <= $numfin; $xx++) {
                        if ($numcontrol <= 9) {
                            $numx = $xx;
                        } else {
                            $numx = $xx + ($numcontrol - 9);
                        }
                        if ($numcontrol == $numx) {
                            echo "<a href='#' onClick='saltocol(\"$numx\")'; style='color:#24D915'> $numx </a>";
                        } else {
                            echo "<a href='#' onClick='saltocol(\"$numx\")'; style='color:#000000'> $numx </a>";
                        }
                    }
                    echo "		&nbsp;&nbsp;<a href='#'>$imagenforward</a>
									&nbsp;<a href='#'>$imagensforward</a>
								</td>
							</tr>
						</table>";
                    ?>
                </div>
            </div>

        </div>
        <input type="hidden" name="numtop" id="numtop" value="<?php echo $_POST['numtop']; ?>" />
    </form>
</body>

</html>
