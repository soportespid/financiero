<?php
	header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=utf8");
	require "comun.inc";
	require "funciones.inc";
	require "funcionesSP.inc.php";
	session_start();
	$linkbd = conectar_v7();
    $linkbd -> set_charset("utf8");
	cargarcodigopag($_GET['codpag'],$_SESSION["nivel"]);
	date_default_timezone_set("America/Bogota");
	//error_reporting(E_ALL);
?>
<!DOCTYPE >
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="es">
	<head>
		<meta http-equiv="Content-type" content="text/html; charset=utf8"/>
		<meta http-equiv="X-UA-Compatible" content="IE=9"/>
		<title>:: IDEAL 10 - Servicios P&uacute;blicos</title>
		<link href="css/css2.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<link href="css/css4.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
        <link href="css/cssSP.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<link href="css/style.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css"/>
		<script type="text/javascript" src="css/programas.js"></script>
        <script type="text/javascript" src="css/calendario.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script src="sweetalert2/dist/sweetalert2.min.js"></script>
		<link rel="stylesheet" href="sweetalert2/dist/sweetalert2.min.css">

		<script>
			function despliegamodalm(_valor,_tip,mensa,pregunta)
			{
				document.getElementById("bgventanamodalm").style.visibility=_valor;
				if(_valor=="hidden"){document.getElementById('ventanam').src="";}
				else
				{
					switch(_tip)
					{
						case "1":	document.getElementById('ventanam').src="ventana-mensaje1.php?titulos="+mensa;break;
						case "2":	document.getElementById('ventanam').src="ventana-mensaje3.php?titulos="+mensa;break;
						case "3":	document.getElementById('ventanam').src="ventana-mensaje2.php?titulos="+mensa;break;
						case "4":	document.getElementById('ventanam').src="ventana-consulta1.php?titulos="+mensa+"&idresp="+pregunta;break;	
					}
				}
			}

            function despliegamodal2(_valor, _table)
			{
                document.getElementById('bgventanamodal2').style.visibility = _valor;
                
                
					if(_table == 'srvcuentasbancarias')
				{
					document.getElementById('ventana2').src = 'cuentasBancarias-ventana.php';
				}
            }

			function funcionmensaje()
			{
                
				
			}

			function actualizar() {
                
				document.form2.submit();
			}

			function guardar() {	
               
				var codObservacion = document.getElementById('codObservacion').value;

				if (codObservacion != '') {

					var nombreObservacion = document.getElementById('nombreObservacion').value;

					if (nombreObservacion != '') {

						var estado = document.getElementById('estado').value;

						if (estado != '') {

							Swal.fire({
								icon: 'question',
								title: 'Seguro que quieres guardar?',
								showDenyButton: true,
								confirmButtonText: 'Guardar',
								denyButtonText: 'Cancelar',
								}).then((result) => {
								if (result.isConfirmed) {
									document.form2.oculto.value = '2';
									document.form2.submit();
								} else if (result.isDenied) {
									Swal.fire('Guardar cancelado', '', 'info')
								}
							})							
						}
						else {

							Swal.fire({
								icon: 'warning',
								title: 'Error en estado'
							})
						}
					}
					else {

						Swal.fire({
							icon: 'warning',
							title: 'Nombre vacío'
						})
					}
				}
				else {

					Swal.fire({
						icon: 'warning',
						title: 'Codigó de observación vacío'
					})
				}
			}

            function respuestaconsulta(pregunta)
			{
				switch(pregunta)
				{
					case "1":	
						document.form2.oculto.value = '2';
						document.form2.submit();
					break;
				}
			}

            function cambiocheck(tipo)
			{
				if (tipo == 'estado') {
					
					if(document.getElementById('estado').value=='S') {
						document.getElementById('estado').value='N';
					}
					else {
						document.getElementById('estado').value='S';
					}
				}

                if (tipo == 'facturacion') {
					
					if(document.getElementById('afectaFacturacion').value == 'S') 
					{
						document.getElementById('afectaFacturacion').value = 'N';
					}
					else 
					{
						document.getElementById('afectaFacturacion').value = 'S';
					}

					document.form2.submit();
				}
			}

            function validaCodigo() {

                var codigo = document.getElementById('codObservacion').value;

                if (codigo != '') {

                    document.form2.oculto.value = '3';
                    document.form2.submit();
                }
            }

			function cambiocheck2($tipo,$id)
			{
				var namecheck = $tipo + '[]';
				var estado = document.getElementsByName(namecheck).item($id).value;

				if (estado == 'S')
				{
					document.getElementsByName(namecheck).item($id).value = 'N';
				}
				else 
				{
					document.getElementsByName(namecheck).item($id).value = 'S';
				}
			}

		</script>
		<?php titlepag();?>
	</head>
	<body>
		<IFRAME src="alertas.php" name="alertas" id="alertas" style="display:none"></IFRAME>
		<span id="todastablas2"></span>
		<table>
			<tr><script>barra_imagenes("serv");</script><?php cuadro_titulos();?></tr>

			<tr><?php menu_desplegable("serv");?></tr>

			<tr>
				<td colspan="3" class="cinta">
                    <a href="serv-crear-observacion.php" class="mgbt"><img src="imagenes/add.png"/></a>
					<a onclick="guardar()" class="mgbt"><img src="imagenes/guarda.png" title="Guardar"/></a>
					<a href="serv-buscar-observacion.php" class="mgbt"><img src="imagenes/busca.png" title="Buscar"/></a>
					<a onclick="mypop=window.open('plan-agenda.php','','');mypop.focus()" class="mgbt"><img src="imagenes/agenda1.png" title="Agenda" /></a>
					<a onclick="mypop=window.open('serv-principal.php','','');mypop.focus();" class="mgbt"><img src="imagenes/nv.png" title="Nueva ventana"></a>
					<a onclick="mypop=window.open('<?php echo $url2; ?>','','');mypop.focus();" class="mgbt"><img src="imagenes/duplicar_pantalla.png" title="Duplicar pesta&ntilde;a"></a>
                </td>
			</tr>
		</table>

		<div id="bgventanamodalm" class="bgventanamodalm">
			<div id="ventanamodalm" class="ventanamodalm">
				<IFRAME src="" name="ventanam" marginWidth=0 marginHeight=0 frameBorder=0 id="ventanam" frameSpacing=0 style=" width:700px; height:130px; top:200; overflow:hidden;"></IFRAME>
			</div>
		</div>

		<form name="form2" method="post" action="">
			<?php 
                $vigusu=vigencia_usuarios($_SESSION['cedulausu']);

				if (@$_POST['oculto'] == "") {

                    $_POST['estado'] = 'S';
					$_POST['afectaFacturacion'] = 'N';
				}				

                if (@$_POST['oculto'] == '3') {

                    $sqlObservacion = "SELECT * FROM srv_tipo_observacion WHERE codigo_observacion = '$_POST[codObservacion]' ";
                    $resp = mysqli_query($linkbd, $sqlObservacion);
                    $resultado = mysqli_num_rows($resp);

                    if ($resultado != 0) {
                        
                        echo "
                                <script>
                                    Swal.fire({
                                        icon: 'error',
                                        title: 'Código ya en uso'
                                    })
                                </script>
                            ";

                        $_POST['codObservacion'] = '';
                    }
                }
			?>
            <div>
                <table class="inicio ancho">
                    <tr>
                        <td class="titulos" colspan="6">.: Crear Observación</td>
                        <td class="cerrar" style="width:7%" onClick="location.href='serv-principal.php'">Cerrar</td>
                    </tr>
                </table>

                <div class="subpantalla" style="height:35%; width:99.5%; float:left; overflow: hidden;">
					<table class="inicio grande">
                        <tr>
                            <td class="titulos" colspan="8">Datos Observación: </td>
                        </tr>

                        <tr>
                            <td class="tamano01" style="width: 10%;">Código de observación</td>
                            <td style="width: 10%;">
                                <input type="number" name="codObservacion" id="codObservacion" value="<?php echo $_POST['codObservacion'] ?>" style="text-align: center; height: 30px;" onchange="validaCodigo();">
                                <input type="hidden" name="id" id="id" value="<?php echo $_POST['id'] ?>">
                            </td>

                            <td class="tamano01" style="width: 10%;">Descripción Observación</td>
                            <td>
                                <input type="text" name="nombreObservacion" id="nombreObservacion" value="<?php echo $_POST['nombreObservacion'] ?>" style="width: 100%;">
                            </td>

                            <td class="tamano01" style="width: 10%;">Estado:</td>
                            <td>
                                <div class="onoffswitch">
                                    <input type="checkbox" name="estado" class="onoffswitch-checkbox" id="estado" value="<?php echo @ $_POST['estado'];?>" <?php if(@ $_POST['estado']=='S'){echo "checked";}?> onChange="cambiocheck('estado');"/>
                                    <label class="onoffswitch-label" for="estado">
                                        <span class="onoffswitch-inner"></span>
                                        <span class="onoffswitch-switch"></span>
                                    </label>
                                </div>
                            </td>

                            <td class="tamano01" style="width: 10%;">Afecta Facturación:</td>
                            <td>
                                <div class="onoffswitch">
                                    <input type="checkbox" name="afectaFacturacion" class="onoffswitch-checkbox" id="afectaFacturacion" value="<?php echo @ $_POST['afectaFacturacion'];?>" <?php if(@ $_POST['afectaFacturacion'] == 'S'){echo "checked";}?> onChange="cambiocheck('facturacion');"/>
                                    <label class="onoffswitch-label" for="afectaFacturacion">
                                        <span class="onoffswitch-inner"></span>
                                        <span class="onoffswitch-switch"></span>
                                    </label>
                                </div>
                            </td>
                        </tr>
                    </table class="inicio grande">

					<?php
						if ($_POST['afectaFacturacion'] == 'S') {
					?>
							<table>
								<tr>
									<td class="titulos" colspan="8">Afecta Facturación: </td>
								</tr>

								<tr class="titulos2" style='text-align:center;'>
									<td style="width:10%">Código servicio</td>
									<td>Nombre Servicio</td>
									<td style="width:10%">Cobro Cargo Fio</td>
									<td style="width:10%">Cobro Consumo</td>
									<td style="width:10%">Cambia consumo</td>
									<td style="width:10%">Consumo</td>
								</tr>
					<?php
								$iter = 'saludo1a';
								$iter2 = 'saludo2';
								$filas = 1;
								$x = 0;

								$sqlServicios = "SELECT id, nombre FROM srvservicios WHERE estado = 'S'";
								$resServicios = mysqli_query($linkbd, $sqlServicios);
								while ($rowServicios = mysqli_fetch_assoc($resServicios)) {		

									$_POST['id_servicio'][$x] = $rowServicios['id'];

									echo"
										<input type='hidden' name='cargoFijo[]' value='".$_POST['cargoFijo'][$x]."'>
										<input type='hidden' name='consumo[]' value='".$_POST['consumo'][$x]."'>
										<input type='hidden' name='cambiaConsumo[]' value='".$_POST['cambiaConsumo'][$x]."'>
										<input type='hidden' name='id_servicio[]' value='".$_POST['id_servicio'][$x]."'>
										";

									if($_POST['cargoFijo'][$x] == 'S'){$_POST["cfcheck$x"] = 'checked';}
									else {$_POST["cfcheck$x"] = '';}
			
									if($_POST['consumo'][$x] == 'S'){$_POST["cscheck$x"] = 'checked';}
									else {$_POST["cscheck$x"] = '';}

									if($_POST['cambiaConsumo'][$x] == 'S'){$_POST["cambiacs$x"] = 'checked';}
									else {$_POST["cambiacs$x"] = '';}
					?>
									<tr class='<?php echo $iter ?>' style='text-transform:uppercase; text-align:center;'>
										<td><?php echo $rowServicios['id'] ?></td>
										<td><?php echo $rowServicios['nombre'] ?></td>

					<?php
										echo "
											<td>
												<div class='rcheck2' style='align-items: center;  display: flex; justify-content: center; !important'>
													<input type='checkbox' id='cfcheck$x' name='cfcheck$x' value='".$_POST["cfcheck$x"]."' ".$_POST["cfcheck$x"]." onClick=\"cambiocheck2('cargoFijo','$x');\">
													<label for='cfcheck$x'></label>
												<div>
											</td>

											<td>
												<div class='rcheck2' style='align-items: center;  display: flex; justify-content: center; !important'>
													<input type='checkbox' id='cscheck$x' name='cscheck$x' value='".$_POST["cscheck$x"]."' ".$_POST["cscheck$x"]." onClick=\"cambiocheck2('consumo','$x');\">
													<label for='cscheck$x'></label>
												<div>
											</td>

											<td>
												<div class='rcheck2' style='align-items: center;  display: flex; justify-content: center; !important'>
													<input type='checkbox' id='cambiacs$x' name='cambiacs$x' value='".$_POST["cambiacs$x"]."' ".$_POST["cambiacs$x"]." onClick=\"cambiocheck2('cambiacs','$x');\">
													<label for='cambiacs$x'></label>
												<div>
											</td>

											<td>
												<div style='align-items: center;  display: flex; justify-content: center; !important'>
													<input type='number' name='consumoLectura[]' value='".$_POST['consumoLectura'][$x]."'>
												<div>
											</td>
										"
					?>
									</tr>
					<?php
									$aux = $iter;
									$iter = $iter2;
									$iter2 = $aux;
									$x++;
								}
					?>
							</table>
					<?php
						} 
					?>
                </div>
            </div>

			<input type="hidden" name="oculto" id="oculto" value="1"/>

			<?php 
				if (@$_POST['oculto'] == "2") {
            
					$idCab = selconsecutivo('srv_tipo_observacion', 'id');
					$codigo_observacion = $_POST['codObservacion'];
					$descripcion = strtoupper($_POST['nombreObservacion']);
					if (@$_POST['afectaFacturacion'] != 'S') {
						$afecta_facturacion = 'N';
					} else {
						$afecta_facturacion = 'S';
					}

					if (@$_POST['estado'] != 'S') {
						$estado = 'N';
					} else {
						$estado = 'S';
					}

					if ($afecta_facturacion == 'S') {

						for ($x=0; $x < count($_POST['cargoFijo']); $x++) {

							$idDet = selconsecutivo('srv_observacion_det', 'id');
							$id_servicio = $_POST['id_servicio'][$x];

							if (@$_POST['cargoFijo'][$x] != 'S') {
								$cargo_fijo = 'N';
							} else {
								$cargo_fijo = 'S';
							}

							if (@$_POST['consumo'][$x] != 'S') {
								$consumo = 'N';
							} else {
								$consumo = 'S';
							}
							
							if (@$_POST['cambiaConsumo'][$x] != 'S') {
								$cambiaConsumo = 'N';
							} else {
								$cambiaConsumo = 'S';
							}

							$consumoLectura = $_POST['consumoLectura'][$x];
							
							$sqlObservacionDet = "INSERT INTO srv_observacion_det (id, codigo_observacion, id_servicio, cargo_fijo, consumo, lectura_consumo, estado) VALUES ($idDet, '$codigo_observacion', '$id_servicio', '$cargo_fijo', '$consumo', '$cambiaConsumo', '$consumoLectura', 'S')";
							mysqli_query($linkbd, $sqlObservacionDet);
						} 
					}

					$sqlObservacionCab = "INSERT INTO srv_tipo_observacion (id, codigo_observacion, descripcion, afecta_facturacion, estado) VALUES ($idCab, '$codigo_observacion', '$descripcion', '$afecta_facturacion', '$estado')";
		
					if(mysqli_query($linkbd, $sqlObservacionCab));
					{
						echo "
							<script>
								Swal.fire({
									icon: 'success',
									title: 'Guardado exitoso!',
									}).then((result) => {
									if (result.value) {
										document.location.href = 'serv-crear-observacion.php';
									} 
								})
							</script>";
					}
				} 
			?>
		</form>
		<div id="bgventanamodal2">
			<div id="ventanamodal2">
				<IFRAME  src="" name="buster" marginWidth=0 marginHeight=0 frameBorder=0 id="ventana2" frameSpacing=0 style="left:500px; width:900px; height:500px; top:200;"></IFRAME>
			</div>
		</div>
	</body>
</html>