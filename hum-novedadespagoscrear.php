<?php
	header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=utf8");
	require "comun.inc";
	require "funciones.inc";
	require 'funcionesnomima.inc';
	session_start();
	$linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");
	cargarcodigopag($_GET['codpag'],$_SESSION["nivel"]);
	date_default_timezone_set("America/Bogota");
?>
<!DOCTYPE >

<html lang="es">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="ie=edge">
		<title>:: IDEAL 10 - Gesti&oacute;n humana</title>
		<link href="css/css2.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css">
		<link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css">
		<link href="css/css4.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css">
		<script type="text/javascript" src="css/calendario.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script type="text/javascript" src="css/programas.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script>
			function validanumeros(evt)
			{
				var code = (evt.which) ? evt.which : evt.keyCode;
				if(code==8) {return true;}
				else if(code>=48 && code<=57) {return true;} 
				else {return false;}
			}
			function guardar()
			{
				
				if (document.getElementById('codigo').value != '')
				{despliegamodalm('visible','4','Esta seguro de guardar','1')}
				else{despliegamodalm('visible','2','Faltan datos para completar el registro');}
			}
			function despliegamodal2(_valor,_num)
			{
				document.getElementById("bgventanamodal2").style.visibility = _valor;
				if(_valor == "hidden"){document.getElementById('ventana2').src = "";}
				else 
				{
					switch(_num)
					{
						case '1':	document.getElementById('ventana2').src = "cargafuncionarios-ventana03.php?objeto=tercero&vcodfun=codfun";break;
						case '2':	if(document.form2.variablepago.value != "-1")
									{
										if (document.form2.periodo.value != "-1")
										{document.getElementById('ventana2').src = "cargafuncionarios-ventana01.php?objeto=lfuncionarios";break;}
										else
										{
											document.getElementById("bgventanamodal2").style.visibility = "hidden";
											document.getElementById('ventana2').src = "";
											despliegamodalm('visible','2','Faltan seleccionar el mes');
											break;
										}
									}
									else
									{
										document.getElementById("bgventanamodal2").style.visibility = "hidden";
										document.getElementById('ventana2').src = "";
										despliegamodalm('visible','2','Faltan seleccionar un tipo de pago');
										break;
									}
					}
				}
			}
			function despliegamodalm(_valor,_tip,mensa,pregunta)
			{
				document.getElementById("bgventanamodalm").style.visibility = _valor;
				if(_valor == "hidden")
				{
					document.getElementById('ventanam').src = "";
					if (document.getElementById('valfocus').value == "1")
					{
						document.getElementById('valfocus').value = '0';
						document.getElementById('ntercero').value = '';
						document.getElementById('tercero').focus();
						document.getElementById('tercero').select();
					}
				}
				else
				{
					switch(_tip)
					{
						case "1":	document.getElementById('ventanam').src = "ventana-mensaje1.php?titulos=" + mensa; break;
						case "2":	document.getElementById('ventanam').src = "ventana-mensaje3.php?titulos=" + mensa; break;
						case "3":	document.getElementById('ventanam').src = "ventana-mensaje2.php?titulos=" + mensa; break;
						case "4":	document.getElementById('ventanam').src = "ventana-consulta1.php?titulos=" + mensa + "&idresp=" + pregunta; break;
					}
				}
			}
			function funcionmensaje()
			{
				var codi = document.form2.codigo.value;
				document.location.href = "hum-novedadespagoseditar.php?codig=" + codi;
			}
			function respuestaconsulta(pregunta)
			{
				switch(pregunta)
				{
					case "1":	document.getElementById('oculto').value = "2";
								document.form2.submit(); break;
					case "2":	document.form2.oculto.value = "3";
								document.form2.submit(); break;
				}
			}
			function cambiocheck1(id)
			{
				var totalfun = document.getElementsByName('vtipopago[]').length;
				var tipo = '';
				var estado = ''
				switch(id)
				{
					case '1':
						if(document.getElementById('idswslse').value == 'S')
						{
							document.getElementById('idswslse').value = 'N';
							estado = 'N';
						}
						else 
						{
							document.getElementById('idswslse').value = 'S';
							estado = 'S';
						}
						tipo = 'vpse';
						break;
					case '2':
						if(document.getElementById('idswslsr').value == 'S')
						{
							document.getElementById('idswslsr').value = 'N';
							estado = 'N';
						}
						else
						{
							document.getElementById('idswslsr').value='S';
							estado = 'S';
						}
						tipo = 'vpsr';
						break;
					case '3':
						if(document.getElementById('idswslpe').value == 'S')
						{
							document.getElementById('idswslpe').value = 'N';
							estado = 'N';
						}
						else 
						{
							document.getElementById('idswslpe').value = 'S';
							estado = 'S';
						}
						tipo = 'vppe';
						break;
					case '4':
						if(document.getElementById('idswslpr').value == 'S')
						{
							document.getElementById('idswslpr').value='N';
							estado = 'N';
						}
						else
						{
							document.getElementById('idswslpr').value = 'S';
							estado = 'S';
						}
						tipo = 'vppr';
						break;
					case '5':
						if(document.getElementById('idswarl').value == 'S')
						{
							document.getElementById('idswarl').value = 'N';
							estado = 'N';
						}
						else
						{
							document.getElementById('idswarl').value = 'S';
							estado = 'S';
						}
						tipo = 'vparl';
						break;
					case '6':
						if(document.getElementById('idswccf').value == 'S')
						{
							document.getElementById('idswccf').value = 'N';
							estado = 'N';
						}
						else 
						{
							document.getElementById('idswccf').value = 'S';
							estado = 'S';
						}
						tipo = 'vpccf';
						break;
					case '7':
						if(document.getElementById('idswicbf').value == 'S')
						{
							document.getElementById('idswicbf').value = 'N';
							estado = 'N';
						}
						else
						{
							document.getElementById('idswicbf').value = 'S';
							estado = 'S';
						}
						tipo = 'vpicbf';
						break;
					case '8':
						if(document.getElementById('idswsena').value == 'S')
						{
							document.getElementById('idswsena').value = 'N';
							estado = 'N';
						}
						else 
						{
							document.getElementById('idswsena').value = 'S';
							estado = 'S';
						}
						tipo = 'vpsena';
						break;
					case '9':
						if(document.getElementById('idswintec').value == 'S')
						{
							document.getElementById('idswintec').value = 'N';
							estado = 'N';
						}
						else
						{
							document.getElementById('idswintec').value='S';
							estado = 'S';
						}
						tipo = 'vpitec';
						break;
					case '10':
						if(document.getElementById('idswesap').value == 'S')
						{
							document.getElementById('idswesap').value = 'N';
							estado = 'N';
						}
						else
						{
							document.getElementById('idswesap').value = 'S';
							estado = 'S';
						}
						tipo = 'vpesap';
						break;
				}
				for(var x = 0; x < totalfun; x++)
				{
					cambiocheck2(tipo,x,estado);
				}
				document.form2.submit();
			}
			function cambiotippago()
			{
				document.form2.cambios.value = "2";
				document.form2.submit();
			}
			function fagregar()
			{
				if(document.form2.variablepago.value != "-1")
				{
					if (document.form2.periodo.value != "-1")
					{
						if (document.form2.ntercero.value!="")
						{
							document.form2.oculto.value = "4";
							document.form2.submit();
						}
						else {despliegamodalm('visible','2','Faltan seleccionar un funcionario');}
					}
					else {despliegamodalm('visible','2','Faltan seleccionar el mes');}
				}
				else {despliegamodalm('visible','2','Faltan seleccionar un tipo de pago');}
			}
			function eliminar(variable)
			{
				document.getElementById('elimina').value = variable;
				despliegamodalm('visible','4','Esta seguro de eliminar','2');
			}
			function fmodificar(pos)
			{
				document.form2.codfun.value = document.getElementsByName('vidfun[]').item(pos).value;
				document.form2.tercero.value = document.getElementsByName('vdocum[]').item(pos).value;
				document.form2.ntercero.value = document.getElementsByName('vnombre[]').item(pos).value;
				document.form2.salbasico.value = document.getElementsByName('vvbasico[]').item(pos).value;
				document.form2.diasn.value = document.getElementsByName('vdias[]').item(pos).value;
				document.form2.valdias.value = document.getElementsByName('vvdias[]').item(pos).value;
				document.form2.descripcion.value = document.getElementsByName('vdescrip[]').item(pos).value;
			}
			function cambiocheck($tipo,$id)
			{
				var namecheck = $tipo + '[]';
				var estado = document.getElementsByName(namecheck).item($id).value;
				if (estado == 'S'){document.getElementsByName(namecheck).item($id).value = 'N';}
				else {document.getElementsByName(namecheck).item($id).value = 'S';}
			}
			function cambiocheck2(tipo,id,estado)
			{
				var namecheck = tipo + '[]';
				if (estado == 'S'){document.getElementsByName(namecheck).item(id).value = 'S';}
				else {document.getElementsByName(namecheck).item(id).value = 'N';}
			}
			function calculos1($id)
			{
				var tipopago = document.getElementsByName('vtipopago[]').item($id).value;
				var basico = document.getElementsByName('vvbasico2[]').item($id).value;
				var dias = document.getElementsByName('vdias[]').item($id).value;
				var total = document.getElementsByName('vvdias[]').item($id);
				var total2 = document.getElementsByName('vvdias2[]').item($id);
				basico = basico.replace('$','');
				var basico2 = Intl.NumberFormat("en-US", {style: "currency", currency: "USD",minimumFractionDigits: 0, maximumFractionDigits: 0,}).format(basico);
				document.getElementsByName('vvbasico[]').item($id).value = basico;
				document.getElementsByName('vvbasico2[]').item($id).value = basico2;
				
				if (tipopago == '01')
				{
					total.value = Math.round((basico / 30) * dias);
					total2.value =  Intl.NumberFormat("en-US", {style: "currency", currency: "USD",minimumFractionDigits: 0, maximumFractionDigits: 0,}).format(total.value);
				}
			}
			function calculos2($id)
			{
				var tipopago = document.getElementsByName('vtipopago[]').item($id).value;
				var basico = document.getElementsByName('vvbasico[]').item($id).value;
				var dias = document.getElementsByName('vdias[]').item($id).value;
				var total = document.getElementsByName('vvdias[]').item($id);
				var total2 = document.getElementsByName('vvdias2[]').item($id);
				if (tipopago == '01')
				{
					total.value = Math.round((basico / 30) * dias);
					total2.value = Intl.NumberFormat("en-US", {style: "currency", currency: "USD",minimumFractionDigits: 0, maximumFractionDigits: 0,}).format(total.value);
				}
			}
			function calculos3($id)
			{
				var tipopago = document.getElementsByName('vtipopago[]').item($id).value;var tipopago = document.getElementsByName('vtipopago[]').item($id).value;
				var total = document.getElementsByName('vvdias[]').item($id);
				var total2 = document.getElementsByName('vvdias2[]').item($id);
				total.value = total2.value.replace('$','');
				total2.value = Intl.NumberFormat("en-US", {style: "currency", currency: "USD",minimumFractionDigits: 0, maximumFractionDigits: 0,}).format(total.value);
			}
			function cambiovigencia($fecha)
			{
				var fechat = document.getElementById("fc_1198971545").value.split('/');
				document.getElementById("vigencia").value = fechat[2]
			}
		</script>
		<?php titlepag();?>
	</head>
	<body>
		<IFRAME src="alertas.php" name="alertas" id="alertas" style="display:none"></IFRAME>
		<span id="todastablas2"></span>
		<table>
			<tr><script>barra_imagenes("hum");</script><?php cuadro_titulos();?></tr>
			<tr><?php menu_desplegable("hum");?></tr>
			<tr>
				<td colspan="3" class="cinta"><img src="imagenes/add.png" title="Nuevo" onClick="location.href='hum-novedadespagoscrear.php'" class="mgbt"><img src="imagenes/guarda.png" title="Guardar" onClick="guardar();" class="mgbt"><img src="imagenes/busca.png" title="Buscar" onClick="location.href='hum-novedadespagosbuscar.php'" class="mgbt"><img src="imagenes/agenda1.png" title="Agenda" onClick="mypop=window.open('plan-agenda.php','','');mypop.focus()" class="mgbt"><img src="imagenes/nv.png" title="Nueva ventana" onClick="mypop=window.open('hum-principal.php','','');mypop.focus();" class="mgbt"><img src="imagenes/duplicar_pantalla.png" title="Duplicar pesta&ntilde;a" onClick="mypop=window.open('<?php echo $url2; ?>','','');mypop.focus();" class="tooltip bottom mgbt"><img src='imagenes/iratras.png' title="Atr&aacute;s" onClick="location.href='hum-novedadespagosbuscar.php'" class="mgbt"></td>
			</tr>
		</table>
		<div id="bgventanamodalm" class="bgventanamodalm">
			<div id="ventanamodalm" class="ventanamodalm">
				<IFRAME src="" name="ventanam" marginWidth=0 marginHeight=0 frameBorder=0 id="ventanam" frameSpacing=0 style=" width:700px; height:130px; top:200; overflow:hidden;"> 
				</IFRAME>
			</div>
		</div>
		<form name="form2" method="post" action="">
			<input type="hidden" name="valfocus" id="valfocus" value="0">
			<?php
				if($_POST['oculto'] == "")
				{
					$fec = date("d/m/Y");
					$_POST['fecha'] = $fec;
					$_POST['vigencia'] = date("Y");
					$_POST['tipo' ]= 'S';
					$_POST['codigo'] = selconsecutivo('hum_novedadespagos','codigo');
					$_POST['lfuncionarios'] = "";
				}
				if($_POST['lfuncionarios'] != '')
				{
					$listacod = str_replace(":","",$_POST['lfuncionarios']);
					$codfun = explode('<->', $listacod);
					for ($x= 0; $x < count($codfun); $x++)
					{
						$idfunenter = $_POST['variablepago'].":$codfun[$x]";
						$valarray = in_array($idfunenter,$_POST['vcftp']);
						if($valarray == false)
						{
							switch($_POST['variablepago'])
							{
								case '01':	$valbasico = $valdiasval = itemfuncionarios($codfun[$x],'5');
											$valdias = '30';
											break;
								case '04':	$sqlr6="SELECT alimentacion, balimentacion FROM admfiscales WHERE vigencia = '".$_POST['vigencia']."'";
											$resp6 = mysqli_query($linkbd,$sqlr6);
											$row6 = mysqli_fetch_row($resp6);
											$salariobasico = itemfuncionarios($codfun[$x],'5');
											if($salariobasico <= $row6[1])
											{
												$valbasico = $valdiasval = $row6[0];
												$valdias = '30';
											}
											else {$valbasico = $valdias = $valdiasval = 0;}
											break;
								case '05':	$sqlr6="SELECT transporte, btransporte FROM admfiscales WHERE vigencia = '".$_POST['vigencia']."'";
											$resp6 = mysqli_query($linkbd,$sqlr6);
											$row6 = mysqli_fetch_row($resp6);
											$salariobasico = itemfuncionarios($codfun[$x],'5');
											if($salariobasico <= $row6[1])
											{
												$valbasico = $valdiasval = $row6[0];
												$valdias = '30';
											}
											else {$valbasico = $valdias = $valdiasval = 0;}
											break;
								default:	$valbasico = $valdias = $valdiasval = 0;
							}
							$_POST['vcodid'][] = '';
							$_POST['vcftp'][] = $_POST['variablepago'].":$codfun[$x]";
							$_POST['vidfun'][] = $codfun[$x];
							$_POST['vfecha'][] = $_POST['fecha'];
							$_POST['vvigencia'][] = $_POST['vigencia'];
							$_POST['vmes'][] = $_POST['periodo'];
							$_POST['vdescrip'][] = $_POST['descripcion'];
							$_POST['vdocum'][] = itemfuncionarios($codfun[$x],'6');
							$_POST['vnombre'][] = itemfuncionarios($codfun[$x],'7');
							$_POST['vtipopago'][] = $_POST['variablepago'];
							$_POST['vvbasico'][] = $valbasico;
							$_POST['vvbasico2'][] = "$".number_format($valbasico,0);
							$_POST['vdias'][] = $valdias;
							$_POST['vvdias'][] = $valdiasval;
							$_POST['vvdias2'][] = "$".number_format($valdiasval,0);
							if($_POST['swslse'] == ''){$_POST['vpse'][] = 'N';}
							else {$_POST['vpse'][] = $_POST['swslse'];}
							if($_POST['swslsr'] == ''){$_POST['vpsr'][] = 'N';}
							else {$_POST['vpsr'][] = $_POST['swslsr'];}
							if($_POST['swslpe'] == ''){$_POST['vppe'][] = 'N';}
							else{$_POST['vppe'][] = $_POST['swslpe'];}
							if($_POST['swslpr'] == ''){$_POST['vppr'][] = 'N';}
							else {$_POST['vppr'][] = $_POST['swslpr'];}
							if($_POST['swarl'] == ''){$_POST['vparl'][] = 'N';}
							else{$_POST['vparl'][] = $_POST['swarl'];}
							if($_POST['swccf'] == ''){$_POST['vpccf'][] = 'N';}
							else{$_POST['vpccf'][] = $_POST['swccf'];}
							if($_POST['swicbf'] == ''){$_POST['vpicbf'][] = 'N';}
							else{$_POST['vpicbf'][] = $_POST['swicbf'];}
							if($_POST['swsena'] == ''){$_POST['vpsena'][] = 'N';}
							else{$_POST['vpsena'][] = $_POST['swsena'];}
							if($_POST['swintec'] == ''){$_POST['vpitec'][] = 'N';}
							else{$_POST['vpitec'][] = $_POST['swintec'];}
							if($_POST['swesap'] == ''){$_POST['vpesap'][] = 'N';}
							else{$_POST['vpesap'][] = $_POST['swesap'];}
							$_POST['vestado'][] = 'S';
							$_POST['secpresu'][] = itemfuncionarios($codfun[$x],'42');
							$vincula1 = itemfuncionarios($codfun[$x],'28');
							$tipopres1 = itemfuncionarios($codfun[$x],'30');
							if($vincula1 == 'P')
							{
								if($tipopres1 == 'F'){$_POST['tpresu1'][] = 'F';}
								elseif($tipopres1 == 'I'){$_POST['tpresu1'][] = 'IN';}
								elseif($tipopres1 == 'G'){$_POST['tpresu1'][] = 'CP';}
								else {$_POST['tpresu1'][] = '';}
							}
							elseif($vincula1 == 'T')
							{
								if($tipopres1 == 'F'){$_POST['tpresu1'][] = 'FT';}
								elseif($tipopres1 == 'I'){$_POST['tpresu1'][] = 'IT';}
								elseif($tipopres1 == 'G'){$_POST['tpresu1'][] = 'CT';}
								else {$_POST['tpresu1'][] = '';}
							}
							else {$_POST['tpresu1'][] = '';}
							$tipopres2 = itemfuncionarios($codfun[$x],'41');
							if($vincula1 == 'P')
							{
								if($tipopres2 == 'F'){$_POST['tpresu2'][] = 'F';}
								elseif($tipopres2 == 'I'){$_POST['tpresu2'][] = 'IN';}
								elseif($tipopres2 == 'G'){$_POST['tpresu2'][] = 'CP';}
								else {$_POST['tpresu2'][] = '';}
							}
							elseif($vincula1 == 'T')
							{
								if($tipopres2 == 'F'){$_POST['tpresu2'][] = 'FT';}
								elseif($tipopres2 == 'I'){$_POST['tpresu2'][] = 'IT';}
								elseif($tipopres2 == 'G'){$_POST['tpresu2'][] = 'CT';}
								else {$_POST['tpresu2'][] = '';}
							}
							else {$_POST['tpresu2'][] = '';}
							$tipopres3 = itemfuncionarios($codfun[$x],'45');
							if($vincula1 == 'P')
							{
								if($tipopres3 == 'F'){$_POST['tpresu3'][] = 'F';}
								elseif($tipopres3 == 'I'){$_POST['tpresu3'][] = 'IN';}
								elseif($tipopres3 == 'G'){$_POST['tpresu3'][] = 'CP';}
								else {$_POST['tpresu3'][] = '';}
							}
							elseif($vincula1 == 'T')
							{
								if($tipopres3 == 'F'){$_POST['tpresu3'][] = 'FT';}
								elseif($tipopres3 == 'I'){$_POST['tpresu3'][] = 'IT';}
								elseif($tipopres3 == 'G'){$_POST['tpresu3'][] = 'CT';}
								else {$_POST['tpresu3'][] = '';}
							}
							else {$_POST['tpresu3'][] = '';}
						}
					}
					$_POST['lfuncionarios'] = '';
				}
				if($_POST['cambios'] == 2)
				{
					$sqlr="SELECT pparafiscal, psalud, ppension, parl FROM ccpethumvariables WHERE codigo = '".$_POST['variablepago']."'";
					$resp = mysqli_query($linkbd,$sqlr);
					$row = mysqli_fetch_row($resp);
					$_POST['swslse'] = $row[1];
					$_POST['swslsr'] = $row[1];
					$_POST['swslpe'] = $row[2];
					$_POST['swslpr'] = $row[2];
					$_POST['swarl'] = $row[3];
					$_POST['swccf'] = $row[0];
					$_POST['swicbf'] = $row[0];
					$_POST['swsena'] = $row[0];
					$_POST['swintec'] = $row[0];
					$_POST['swesap'] = $row[0];
				}
				if($_POST['variablepago'] != '-1')
				{
					switch($_POST['variablepago'])
					{
						case '01':
						{
							$sqlr=" SELECT codfun, GROUP_CONCAT(descripcion ORDER BY CONVERT(valor, SIGNED INTEGER) SEPARATOR '<->') FROM hum_funcionarios WHERE (item = 'VALESCALA') AND codfun = '".$_POST['codfun']."' AND estado = 'S' GROUP BY codfun";
							$resp2 = mysqli_query($linkbd,$sqlr);
							$row2 = mysqli_fetch_row($resp2);
							$_POST['salbasico'] = $row2[1];
							$vlectura = "readonly";
						}break;
						case '04':
						{
							$sqlr="SELECT codfun, GROUP_CONCAT(descripcion ORDER BY CONVERT(valor, SIGNED INTEGER) SEPARATOR '<->') FROM hum_funcionarios WHERE (item = 'VALESCALA') AND codfun = '".$_POST['codfun']."' AND estado = 'S' GROUP BY codfun";
								$resp2 = mysqli_query($linkbd,$sqlr);
							$row2 = mysqli_fetch_row($resp2);
							$salbasico = $row2[1];
							$sqlr="SELECT alimentacion, balimentacion FROM admfiscales WHERE vigencia = '".$_POST['vigencia']."'";
							$resp = mysqli_query($linkbd,$sqlr);
							$row = mysqli_fetch_row($resp);
							if($salbasico <= $row[1]){$_POST['salbasico'] = $row[0];}
							else{$_POST['salbasico'] = 0;}
							$vlectura = "readonly";
						}break;
						case '05':
						{
							$sqlr="SELECT codfun, GROUP_CONCAT(descripcion ORDER BY CONVERT(valor, SIGNED INTEGER) SEPARATOR '<->') FROM hum_funcionarios WHERE (item = 'VALESCALA') AND codfun = '".$_POST['codfun']."' AND estado = 'S' GROUP BY codfun";
							$resp2 = mysqli_query($linkbd,$sqlr);
							$row2 = mysqli_fetch_row($resp2);
							$salbasico = $row2[1];
							$sqlr = "SELECT transporte, btransporte FROM admfiscales WHERE vigencia = '".$_POST['vigencia']."'";
							$resp = mysqli_query($linkbd,$sqlr);
							$row = mysqli_fetch_row($resp);
							if($salbasico <= $row[1]){$_POST['salbasico'] = $row[0];}
							else{$_POST['salbasico'] = 0;}
							$vlectura = "readonly";
						}break;
						default:	$_POST['salbasico'] = 0;
					}
				}
				else {$_POST['salbasico'] = 0;}
				if(($_POST['diasn'] != '') && ($_POST['diasn'] != 0) && ($_POST['salbasico'] != 0))
				{
					$_POST['valdias'] = round (($_POST['salbasico']/30)*$_POST['diasn'],0,PHP_ROUND_HALF_DOWN);
				}
				else if(($_POST['variablepago'] == '01') || $_POST['variablepago'] == '04' || $_POST['variablepago'] == '05')
				{$_POST['valdias'] = 0;}
			?>
			<input type="hidden" name="lfuncionarios" id="lfuncionarios" value="<?php echo $_POST['lfuncionarios'];?>"/>
			<table class="inicio ancho">
				<tr>
					<td class="titulos" colspan="11">.: Agregar novedad de pagos funcionario</td>
					<td class="cerrar" style="width:7%" onClick="location.href='hum-principal.php'">Cerrar</td>
				</tr>
				<tr>
					<td class="saludo1" style="width:2.2cm;">C&oacute;digo:</td>
					<td style="width:5%;"><input type="text" name="codigo" id="codigo" value="<?php echo $_POST['codigo']?>" style="width:100%;" readonly></td>
					<td class="saludo1" style="width:2.2cm;">Fecha:</td>
					<td style="width:10%;"><input type="text" name="fecha" id="fc_1198971545" title="DD/MM/YYYY" value="<?php echo $_POST['fecha']; ?>" onKeyUp="return tabular(event,this)" onKeyDown="mascara(this,'/',patron,true)"  maxlength="10" style="width:100%;" onChange="cambiovigencia();" class="colordobleclik" onDblClick="displayCalendarFor('fc_1198971545');" autocomplete="off"></td>
					<td class="saludo1" style="width:2.2m;">Vigencia:</td> 
					<td style="width:10%;"><input name="vigencia" id='vigencia' type="text" value="<?php echo $_POST['vigencia']?>" style="width:100%;"></td>
					<td class="saludo1" style="width:2.2cm;">Mes:</td>
					<td>
						<select name="periodo" id="periodo" >
							<option value="-1">Seleccione ....</option>
							<?php
								$sqlr = "SELECT * FROM meses WHERE estado='S'";
								$resp = mysqli_query($linkbd,$sqlr);
								while ($row = mysqli_fetch_row($resp)) 
								{
									if($row[0] == $_POST['periodo'])
									{
										echo "<option value='$row[0]' SELECTED>$row[1]</option>";
										$_POST['periodonom'] = $row[1];
										$_POST['periodonom'] = $row[2];
									}
									else {echo "<option value='$row[0]'>$row[1]</option>";}
								}
							?>
						</select>
					</td>
					<td class="saludo1" >Tipo de pago:</td>
					<td >
						<select name="variablepago" id="variablepago" style="width:100%;" onChange="cambiotippago();">
							<option value="-1">Seleccione ....</option>
							<?php
								$sqlr="SELECT codigo, nombre FROM ccpethumvariables WHERE estado='S'";
								$resp = mysqli_query($linkbd,$sqlr);
								while ($row = mysqli_fetch_row($resp))
								{
									if(in_array($row[0], $_POST['vtipopago'])){$vartip = "background-color:yellowgreen;";}
									else{$vartip = "";}
									if($row[0] == $_POST['variablepago'])
									{
										echo "<option value='$row[0]' SELECTED style='$vartip'>$row[0] - $row[1]</option>";
									}
									else 
									{
										echo "<option value='$row[0]' style='$vartip'>$row[0] - $row[1]</option>";
									}
								}
							?>
						</select>
					</td>
				</tr>
				<tr>
					<td class="saludo1">Descripci&oacute;n:</td>
					<td colspan="8"><input type="text" name="descripcion" id="descripcion" value="<?php echo $_POST['descripcion']?>" onKeyUp="return tabular(event,this)" style="width:100%;"></td>
					<td style="padding-bottom:0px"><em class="botonflechaverde" onClick="despliegamodal2('visible','2');">Lista funcionarios</em></td>
				</tr>
			</table>
			<table class="inicio">
				<tr><td colspan="11" class="titulos">Detalle seguridad social y parafiscales</td></tr>
				<tr>
					<td class="saludo1" style="width:3.5cm">Pagar salud empleado:</td>
					<td style="width:7%">
						<div class="swsino">
							<input type="checkbox" name="swslse" class="swsino-checkbox" id="idswslse" value="<?php echo $_POST['swslse'];?>" <?php if($_POST['swslse'] == 'S'){echo "checked";}?> onChange="cambiocheck1('1');"/>
							<label class="swsino-label" for="idswslse">
								<span class='swsino-inner'></span>
								<span class='swsino-switch'></span>
							</label>
						</div>
					</td>
					<td class="saludo1" style="width:3.5cm">Pagar salud empresa:</td>
					<td style="width:7%">
						<div class="swsino">
							<input type="checkbox" name="swslsr" class="swsino-checkbox" id="idswslsr" value="<?php echo $_POST['swslsr'];?>" <?php if($_POST['swslsr'] == 'S'){echo "checked";}?> onChange="cambiocheck1('2');"/>
							<label class="swsino-label" for="idswslsr">
								<span class='swsino-inner'></span>
								<span class='swsino-switch'></span>
							</label>
						</div>
					</td>
					<td class="saludo1" style="width:4cm">Pagar pension empleado:</td>
					<td style="width:7%">
						<div class="swsino">
							<input type="checkbox" name="swslpe" class="swsino-checkbox" id="idswslpe" value="<?php echo $_POST['swslpe'];?>" <?php if($_POST['swslpe']=='S'){echo "checked";}?> onChange="cambiocheck1('3');"/>
							<label class="swsino-label" for="idswslpe">
								<span class='swsino-inner'></span>
								<span class='swsino-switch'></span>
							</label>
						</div>
					</td>
					<td class="saludo1" style="width:4cm">Pagar pension empresa:</td>
					<td style="width:7%">
						<div class="swsino">
							<input type="checkbox" name="swslpr" class="swsino-checkbox" id="idswslpr" value="<?php echo $_POST['swslpr'];?>" <?php if($_POST['swslpr'] == 'S'){echo "checked";}?> onChange="cambiocheck1('4');"/>
							<label class="swsino-label" for="idswslpr">
								<span class='swsino-inner'></span>
								<span class='swsino-switch'></span>
							</label>
						</div>
					</td>
					<td class="saludo1" style="width:3.5cm">Pagar ARL:</td>
					<td style="width:7%">
						<div class="swsino">
							<input type="checkbox" name="swarl" class="swsino-checkbox" id="idswarl" value="<?php echo $_POST['swarl'];?>" <?php if($_POST['swarl'] == 'S'){echo "checked";}?> onChange="cambiocheck1('5');"/>
							<label class="swsino-label" for="idswarl">
								<span class='swsino-inner'></span>
								<span class='swsino-switch'></span>
							</label>
						</div>
					</td>
					<td></td>
				</tr>
				<tr>
					<td class="saludo1" title="Caja de Compensacion Familiar">Pagar CCF:</td>
					<td style="width:7%">
						<div class="swsino">
							<input type="checkbox" name="swccf" class="swsino-checkbox" id="idswccf" value="<?php echo $_POST['swccf'];?>" <?php if($_POST['swccf'] == 'S'){echo "checked";}?> onChange="cambiocheck1('6');"/>
							<label class="swsino-label" for="idswccf">
								<span class='swsino-inner'></span>
								<span class='swsino-switch'></span>
							</label>
						</div>
					</td>
					<td class="saludo1" title="Instituto Colombiano de Bienestar Familiar">Pagar ICBF:</td>
					<td style="width:7%">
						<div class="swsino">
							<input type="checkbox" name="swicbf" class="swsino-checkbox" id="idswicbf" value="<?php echo $_POST['swicbf'];?>" <?php if($_POST['swicbf'] == 'S'){echo "checked";}?> onChange="cambiocheck1('7');"/>
							<label class="swsino-label" for="idswicbf">
								<span class='swsino-inner'></span>
								<span class='swsino-switch'></span>
							</label>
						</div>
					</td>
					<td class="saludo1" title="Servicio Nacional de Aprendizaje">Pagar SENA:</td>
					<td style="width:7%">
						<div class="swsino">
							<input type="checkbox" name="swsena" class="swsino-checkbox" id="idswsena" value="<?php echo $_POST['swsena'];?>" <?php if($_POST['swsena'] =='S'){echo "checked";}?> onChange="cambiocheck1('8');"/>
							<label class="swsino-label" for="idswsena">
								<span class='swsino-inner'></span>
								<span class='swsino-switch'></span>
							</label>
						</div>
					</td>
					<td class="saludo1" title="Institutos Tecnicos">Pagar Ins. Tecnicos:</td>
					<td style="width:7%">
						<div class="swsino">
							<input type="checkbox" name="swintec" class="swsino-checkbox" id="idswintec" value="<?php echo $_POST['swintec'];?>" <?php if($_POST['swintec'] == 'S'){echo "checked";}?> onChange="cambiocheck1('9');"/>
							<label class="swsino-label" for="idswintec">
								<span class='swsino-inner'></span>
								<span class='swsino-switch'></span>
							</label>
						</div>
					</td>
					<td class="saludo1" title="Escuela Superior de Administracion Publica">Pagar ESAP:</td>
					<td style="width:7%">
						<div class="swsino">
							<input type="checkbox" name="swesap" class="swsino-checkbox" id="idswesap" value="<?php echo $_POST['swesap'];?>" <?php if($_POST['swesap'] == 'S'){echo "checked";}?> onChange="cambiocheck1('10');"/>
							<label class="swsino-label" for="idswesap">
								<span class='swsino-inner'></span>
								<span class='swsino-switch'></span>
							</label>
						</div>
					</td>
				</tr>
			</table>
			<div class="subpantalla" style="height:45%; width:99.6%; overflow-x:hidden;">
				<table class="inicio" width="99%">
					<tr><td class="titulos" colspan="22">Funcionarios agregados</td></tr>
					<tr>
						<td class="titulos2" style="width:4%">No</td>
						<td class="titulos2" style="width:4%">Tipo</td>
						<td class="titulos2" style="width:8%">Documento</td>
						<td class="titulos2">Nombre</td>
						<td class="titulos2">B&aacute;sico</td>
						<td class="titulos2">D&iacute;as</td>
						<td class="titulos2">Valor d&iacute;as</td>
						<td class="titulos2">SecPre</td>
						<td class="titulos2">Presu1</td>
						<td class="titulos2">Presu2</td>
						<td class="titulos2">Presu3</td>
						<td class="titulos2" style="width:3%">SE</td>
						<td class="titulos2" style="width:3%">SR</td>
						<td class="titulos2" style="width:3%">PE</td>
						<td class="titulos2" style="width:3%">PR</td>
						<td class="titulos2" style="width:3%">ARL</td>
						<td class="titulos2" style="width:3%">CCF</td>
						<td class="titulos2" style="width:3%">ICBF</td>
						<td class="titulos2" style="width:3%">SENA</td>
						<td class="titulos2" style="width:3%">INTEC</td>
						<td class="titulos2" style="width:3%">ESAP</td>
						<td class="titulos2" style="width:3%">Eliminar</td>
					</tr>
					<?php
						if ($_POST['oculto']=='3')
						{
							$posi=$_POST['elimina'];
							unset($_POST['vcodid'][$posi]);
							unset($_POST['vcftp'][$posi]);
							unset($_POST['vidfun'][$posi]);
							unset($_POST['vfecha'][$posi]);
							unset($_POST['vvigencia'][$posi]);
							unset($_POST['vmes'][$posi]);
							unset($_POST['vdescrip'][$posi]);
							unset($_POST['vdocum'][$posi]);
							unset($_POST['vnombre'][$posi]);
							unset($_POST['vtipopago'][$posi]);
							unset($_POST['vvbasico'][$posi]);
							unset($_POST['vvbasico2'][$posi]);
							unset($_POST['vdias'][$posi]);
							unset($_POST['vvdias'][$posi]);
							unset($_POST['vvdias'][$posi]);
							unset($_POST['vvdias2'][$posi]);
							unset($_POST['vpse'][$posi]);
							unset($_POST['vpsr'][$posi]);
							unset($_POST['vppe'][$posi]);
							unset($_POST['vppr'][$posi]);
							unset($_POST['vparl'][$posi]);
							unset($_POST['vpccf'][$posi]);
							unset($_POST['vpicbf'][$posi]);
							unset($_POST['vpsena'][$posi]);
							unset($_POST['vpitec'][$posi]);
							unset($_POST['vpesap'][$posi]);
							unset($_POST['vestado'][$posi]);
							$_POST['vcodid'] = array_values($_POST['vcodid']);
							$_POST['vcftp'] = array_values($_POST['vcftp']);
							$_POST['vidfun'] = array_values($_POST['vidfun']);
							$_POST['vfecha'] = array_values($_POST['vfecha']);
							$_POST['vvigencia'] = array_values($_POST['vvigencia']);
							$_POST['vmes'] = array_values($_POST['vmes']);
							$_POST['vdescrip'] = array_values($_POST['vdescrip']);
							$_POST['vdocum'] = array_values($_POST['vdocum']);
							$_POST['vnombre'] = array_values($_POST['vnombre']);
							$_POST['vtipopago']= array_values($_POST['vtipopago']);
							$_POST['vvbasico'] = array_values($_POST['vvbasico']);
							$_POST['vvbasico2'] = array_values($_POST['vvbasico2']);
							$_POST['vdias'] = array_values($_POST['vdias']);
							$_POST['vvdias'] = array_values($_POST['vvdias']);
							$_POST['vvdias2'] = array_values($_POST['vvdias2']);
							$_POST['vpse'] = array_values($_POST['vpse']);
							$_POST['vpsr'] = array_values($_POST['vpsr']);
							$_POST['vppe'] = array_values($_POST['vppe']);
							$_POST['vppr'] = array_values($_POST['vppr']);
							$_POST['vparl'] = array_values($_POST['vparl']);
							$_POST['vpccf'] = array_values($_POST['vpccf']);
							$_POST['vpicbf'] = array_values($_POST['vpicbf']);
							$_POST['vpsena'] = array_values($_POST['vpsena']);
							$_POST['vpitec'] = array_values($_POST['vpitec']);
							$_POST['vpesap'] = array_values($_POST['vpesap']);
							$_POST['vestado'] = array_values($_POST['vestado']);
							$_POST['elimina'] = '';
						}
						$co = "saludo1a";
						$co2 = "saludo2";
						for ($x=0;$x<count($_POST['vidfun']);$x++)
						{
							echo"
							<input type='hidden' name='vcftp[]' value='".$_POST['vcftp'][$x]."'>
							<input type='hidden' name='vcodid[]' value='".$_POST['vcodid'][$x]."'>
							<input type='hidden' name='vidfun[]' value='".$_POST['vidfun'][$x]."'>
							<input type='hidden' name='vfecha[]' value='".$_POST['vfecha'][$x]."'>
							<input type='hidden' name='vvigencia[]' value='".$_POST['vvigencia'][$x]."'>
							<input type='hidden' name='vmes[]' value='".$_POST['vmes'][$x]."'/>
							<input type='hidden' name='vdescrip[]' value='".$_POST['vdescrip'][$x]."'>
							<input type='hidden' name='vdocum[]' value='".$_POST['vdocum'][$x]."'>
							<input type='hidden' name='vnombre[]' value='".$_POST['vnombre'][$x]."'>
							<input type='hidden' name='vtipopago[]' value='".$_POST['vtipopago'][$x]."'>
							<input type='hidden' name='vpse[]' value='".$_POST['vpse'][$x]."'>
							<input type='hidden' name='vpsr[]' value='".$_POST['vpsr'][$x]."'>
							<input type='hidden' name='vppe[]' value='".$_POST['vppe'][$x]."'>
							<input type='hidden' name='vppr[]' value='".$_POST['vppr'][$x]."'>
							<input type='hidden' name='vparl[]' value='".$_POST['vparl'][$x]."'>
							<input type='hidden' name='vpccf[]' value='".$_POST['vpccf'][$x]."'>
							<input type='hidden' name='vpicbf[]' value='".$_POST['vpicbf'][$x]."'>
							<input type='hidden' name='vpsena[]' value='".$_POST['vpsena'][$x]."'>
							<input type='hidden' name='vpitec[]' value='".$_POST['vpitec'][$x]."'>
							<input type='hidden' name='vpesap[]' value='".$_POST['vpesap'][$x]."'>
							<input type='hidden' name='vestado[]' value='".$_POST['vestado'][$x]."'>
							<input type='hidden' name='vvbasico[]' value='".$_POST['vvbasico'][$x]."'>
							<input type='hidden' name='vvdias[]' value='".$_POST['vvdias'][$x]."'>
							<input type='hidden' name='tpresu1[]' value='".$_POST['tpresu1'][$x]."'>
							<input type='hidden' name='tpresu2[]' value='".$_POST['tpresu2'][$x]."'>
							<input type='hidden' name='tpresu3[]' value='".$_POST['tpresu3'][$x]."'>
							<input type='hidden' name='secpresu[]' value='".$_POST['secpresu'][$x]."'>
							";
							if($_POST['vtipopago'][$x] == $_POST['variablepago'])
							{
								$aux=$co;
								$co=$co2;
								$co2=$aux;
								$columvisual='';
							}
							else
							{$columvisual="style='display:none;'";}
							if($_POST['vpse'][$x] == 'S'){$_POST["vpsecheck$x"] = 'checked';}
							else {$_POST["vpsecheck$x"] = '';}
							if($_POST['vpsr'][$x] == 'S'){$_POST["vpsrcheck$x"] = 'checked';}
							else {$_POST["vpsrcheck$x"] = '';}
							if($_POST['vppe'][$x] == 'S'){$_POST["vppecheck$x"] = 'checked';}
							else {$_POST["vppecheck$x"] = '';}
							if($_POST['vppr'][$x] == 'S'){$_POST["vpprcheck$x"] = 'checked';}
							else {$_POST["vpprcheck$x"] = '';}
							if($_POST['vparl'][$x] == 'S'){$_POST["vparlcheck$x"] = 'checked';}
							else {$_POST["vparlcheck$x"] = '';}
							if($_POST['vpccf'][$x] == 'S'){$_POST["vpccfcheck$x"] = 'checked';}
							else {$_POST["vpccfcheck$x"] = '';}
							if($_POST['vpicbf'][$x] == 'S'){$_POST["vpicbfcheck$x"] = 'checked';}
							else {$_POST["vpicbfcheck$x"] = '';}
							if($_POST['vpsena'][$x] == 'S'){$_POST["vpsenacheck$x"] = 'checked';}
							else {$_POST["vpsenacheck$x"] = '';}
							if($_POST['vpitec'][$x] == 'S'){$_POST["vpiteccheck$x"] = 'checked';}
							else {$_POST["vpiteccheck$x"] = '';}
							if($_POST['vpesap'][$x] == 'S'){$_POST["vpesapcheck$x"] = 'checked';}
							else {$_POST["vpesapcheck$x"] = '';}
							if($_POST['secpresu'][$x] == ''){$estilo = 'background-color:red';}
							else {$estilo = '';}
							if($_POST['tpresu1'][$x] == ''){$estilo1 = 'background-color:red';}
							else {$estilo1 = '';}
							if($_POST['tpresu2'][$x] == ''){$estilo2 = 'background-color:red';}
							else {$estilo2 = '';}
							if($_POST['tpresu3'][$x] == ''){$estilo3 = 'background-color:red';}
							else {$estilo3 = '';}
							echo"
							<tr class='$co' $columvisual>
								<td style='text-align:right;'>".($x+1)."&nbsp;</td>
								<td style='text-align:right;'>".$_POST['vtipopago'][$x]."&nbsp;</td>
								<td style='text-align:right;'>".$_POST['vdocum'][$x]."&nbsp;</td>
								<td >".$_POST['vnombre'][$x]."</td>
								
								<td style='text-align:right;'><input type='text' name='vvbasico2[]' value='".$_POST['vvbasico2'][$x]."' style='text-align:right;width:100%;' onChange=\"calculos1('$x');\" class='inpnovisibles' autocomplete='off' onkeypress=\"return validanumeros(event);\"></td>
								
								<td style='text-align:right;'><input type='text' name='vdias[]' value='".$_POST['vdias'][$x]."' style='text-align:right;width:100%;' onChange=\"calculos2('$x');\" class='inpnovisibles' autocomplete='off' onkeypress=\"return validanumeros(event);\"></td>
								
								<td style='text-align:right;'><input type='text' name='vvdias2[]' value='".$_POST['vvdias2'][$x]."' style='text-align:right;width:100%;'  onChange=\"calculos3('$x');\" class='inpnovisibles' autocomplete='off' onkeypress=\"return validanumeros(event);\"></td>
								
								<td style='text-align:center;$estilo'>".$_POST['secpresu'][$x]."&nbsp;</td>
								<td style='text-align:center;$estilo1'>".$_POST['tpresu1'][$x]."&nbsp;</td>
								<td style='text-align:center;$estilo2'>".$_POST['tpresu2'][$x]."&nbsp;</td>
								<td style='text-align:center;$estilo3'>".$_POST['tpresu3'][$x]."&nbsp;</td>
								
								<td><div class='rcheck2'><input type='checkbox' id='vpsecheck$x' name='vpsecheck$x' value='".$_POST["vpsecheck$x"]."' ".$_POST["vpsecheck$x"]." onClick=\"cambiocheck('vpse','$x');\"><label for='vpsecheck$x'></label><div></td>
								<td><div class='rcheck2'><input type='checkbox' id='vpsrcheck$x' name='vpsrcheck$x' value='".$_POST["vpsrcheck$x"]."' ".$_POST["vpsrcheck$x"]." onClick=\"cambiocheck('vpsr','$x');\"><label for='vpsrcheck$x'></label><div></td>
								<td><div class='rcheck2'><input type='checkbox' id='vppecheck$x' name='vppecheck$x' value='".$_POST["vppecheck$x"]."' ".$_POST["vppecheck$x"]." onClick=\"cambiocheck('vppe','$x');\"><label for='vppecheck$x'></label><div></td>
								<td><div class='rcheck2'><input type='checkbox' id='vpprcheck$x' name='vpprcheck$x' value='".$_POST["vpprcheck$x"]."' ".$_POST["vpprcheck$x"]." onClick=\"cambiocheck('vppr','$x');\"><label for='vpprcheck$x'></label><div></td>
								<td><div class='rcheck2'><input type='checkbox' id='vparlcheck$x' name='vparlcheck$x' value='".$_POST["vparlcheck$x"]."' ".$_POST["vparlcheck$x"]." onClick=\"cambiocheck('vparl','$x');\"><label for='vparlcheck$x'></label><div></td>
								<td><div class='rcheck2'><input type='checkbox' id='vpccfcheck$x' name='vpccfcheck$x' value='".$_POST["vpccfcheck$x"]."' ".$_POST["vpccfcheck$x"]." onClick=\"cambiocheck('vpccf','$x');\"><label for='vpccfcheck$x'></label><div></td>
								<td><div class='rcheck2'><input type='checkbox' id='vpicbfcheck$x' name='vpicbfcheck$x' value='".$_POST["vpicbfcheck$x"]."' ".$_POST["vpicbfcheck$x"]." onClick=\"cambiocheck('vpicbf','$x');\"><label for='vpicbfcheck$x'></label><div></td>
								<td><div class='rcheck2'><input type='checkbox' id='vpsenacheck$x' name='vpsenacheck$x' value='".$_POST["vpsenacheck$x"]."' ".$_POST["vpsenacheck$x"]." onClick=\"cambiocheck('vpsena','$x');\"><label for='vpsenacheck$x'></label><div></td>
								<td><div class='rcheck2'><input type='checkbox' id='vpiteccheck$x' name='vpiteccheck$x' value='".$_POST["vpiteccheck$x"]."' ".$_POST["vpiteccheck$x"]." onClick=\"cambiocheck('vpitec','$x');\"><label for='vpiteccheck$x'></label><div></td>
								<td><div class='rcheck2'><input type='checkbox' id='vpesapcheck$x' name='vpesapcheck$x' value='".$_POST["vpesapcheck$x"]."' ".$_POST["vpesapcheck$x"]." onClick=\"cambiocheck('vpesap','$x');\"><label for='vpesapcheck$x'></label><div></td>
								<td style='text-align:center;'><img src='imagenes/del.png' onclick='eliminar($x)' class='icoop'></td>
							</tr>";
						}
						echo"
						<tr class='$co' $columvisual>
						</tr>";
					?>
					<input type='hidden' name='elimina' id='elimina'/>
				<table>
			</div>
			<input type="hidden" name="oculto" id="oculto" value="1"/>
			<input type="hidden" name="cambios" id="cambios" value="1"/>
			<?php
				if($_POST['oculto'] == '2')
				{
					$_POST['codigo'] = selconsecutivo('hum_novedadespagos','codigo');
					preg_match("/([0-9]{2})\/([0-9]{2})\/([0-9]{4})/", $_POST['fecha'],$fecha);
					$fechag = "$fecha[3]-$fecha[2]-$fecha[1]";
					$sqlr="INSERT INTO hum_novedadespagos_cab (id, fecha, mes, descripcion, vigencia, estado) VALUES ('".$_POST['codigo']."', '$fechag', '".$_POST['periodo']."', '".$_POST['descripcion']."', '".$_POST['vigencia']."','S')";
					mysqli_query($linkbd,$sqlr);
					for ($x = 0; $x < count($_POST['vidfun']); $x++)
					{
						$conid = selconsecutivo('hum_novedadespagos','id');
						preg_match("/([0-9]{2})\/([0-9]{2})\/([0-9]{4})/", $_POST['vfecha'][$x],$fecha);
						$fechaf="$fecha[3]-$fecha[2]-$fecha[1]";
						$sqlr="INSERT INTO hum_novedadespagos (id, codigo, fecha, mes, vigencia, tipo, descripcion, idfun, documento, nombrefun, valorb, dias, valord, pse, psr, ppe, ppr, parl, pccf, picbf, psena, pitec, pesap, codpre, estado, secpresu, tippresu1, tippresu2, tippresu3) VALUES ('$conid', '".$_POST['codigo']."', '$fechaf', '".$_POST['vmes'][$x]."', '".$_POST['vvigencia'][$x]."', '".$_POST['vtipopago'][$x]."', '".$_POST['vdescrip'][$x]."', '".$_POST['vidfun'][$x]."', '".$_POST['vdocum'][$x]."', '".$_POST['vnombre'][$x]."', '".$_POST['vvbasico'][$x]."', '".$_POST['vdias'][$x]."', '".$_POST['vvdias'][$x]."', '".$_POST['vpse'][$x]."', '".$_POST['vpsr'][$x]."', '".$_POST['vppe'][$x]."', '".$_POST['vppr'][$x]."', '".$_POST['vparl'][$x]."', '".$_POST['vpccf'][$x]."', '".$_POST['vpicbf'][$x]."', '".$_POST['vpsena'][$x]."', '".$_POST['vpitec'][$x]."', '".$_POST['vpesap'][$x]."', '0', 'S', '".$_POST['secpresu'][$x]."', '".$_POST['tpresu1'][$x]."', '".$_POST['tpresu2'][$x]."', '".$_POST['tpresu3'][$x]."')";
						mysqli_query($linkbd,$sqlr);
					}
					echo "<script>despliegamodalm('visible','1','Se ha almacenado con Éxito');</script>";
				}
			?> 
		</form>
		<div id="bgventanamodal2">
			<div id="ventanamodal2">
				<IFRAME  src="" name="buster" marginWidth=0 marginHeight=0 frameBorder=0 id="ventana2" frameSpacing=0 style="left:500px; width:900px; height:500px; top:200;"> 
				</IFRAME>
			</div>
		</div>
	</body>
</html>