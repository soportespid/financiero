<?php

    header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=utf8");
	require 'comun.inc';
	require 'funciones.inc';
	session_start();
    if(empty($_SESSION)){
        header("location: index.php");
    }
	$linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");
	cargarcodigopag($_GET['codpag'],$_SESSION["nivel"]);
	date_default_timezone_set("America/Bogota");
?>
<!DOCTYPE >
<html lang=es>
	<head>
    <meta charset="utf-8"/>
		<meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1"/>
		<meta name="viewport" content="user-scalable=no">
		<title></title>
		<link href="favicon.ico" rel="shortcut icon"/>
		<link href="css/css2z.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
		<link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
		<link href="css/css4.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
        <link href="css/style.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css"/>
        <link rel="stylesheet" href="sweetalert2/dist/sweetalert2.min.css">
        <script src="sweetalert2/dist/sweetalert2.min.js"></script>
		<script src="css/programas.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
	</head>
	<body>
		<header>
			<table>
				<tr><script>barra_imagenes("trans");</script><?php cuadro_titulos();?></tr>
			</table>
		</header>

        <main id="myapp" v-cloak>
            <div class="loading-container" v-show="isLoading" >
                <p class="text-loading"data-loading="IDEAL 10">IDEAL 10</p>
            </div>
            <nav>
                <table>
                    <tr><?php menu_desplegable("trans");?></tr>
                </table>
                <div class="bg-white group-btn p-1" id="newNavStyle">
                    <button type="button" @click="isModal=true" class="btn btn-white btn-primary-hover d-flex justify-between align-items-center">
                        <span>Cargar cartera</span>
                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 -960 960 960"><path d="M440-440H200v-80h240v-240h80v240h240v80H520v240h-80v-240Z"></path></svg>
                    </button>
                    <button type="button" class="btn btn-white btn-primary-hover d-flex justify-between align-items-center" @click="mypop=window.open('trans-principal.php','',''); mypop.focus();">
                        <span>Nueva ventana</span>
                        <svg class="fill-black group-hover:fill-white w-5 h-5" xmlns="http://www.w3.org/2000/svg"  viewBox="0 -960 960 960" ><path d="M320-120v-80h80v-80H160q-33 0-56.5-23.5T80-360v-400q0-33 23.5-56.5T160-840h640q33 0 56.5 23.5T880-760v400q0 33-23.5 56.5T800-280H560v80h80v80H320ZM160-360h640v-400H160v400Zm0 0v-400 400Z"/></svg>
                    </button>
                    <button type="button" @click="mypop=window.open('trans-base-gravable','','');mypop.focus();" class="btn btn-white btn-primary-hover d-flex justify-between align-items-center">
                        <span class="group-hover:text-white">Duplicar pantalla</span>
                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 -960 960 960"><path d="M640-160v-360H160v360h480Zm80-200v-80h80v-360H320v200h-80v-200q0-33 23.5-56.5T320-880h480q33 0 56.5 23.5T880-800v360q0 33-23.5 56.5T800-360h-80ZM160-80q-33 0-56.5-23.5T80-160v-360q0-33 23.5-56.5T160-600h480q33 0 56.5 23.5T720-520v360q0 33-23.5 56.5T640-80H160Zm400-603ZM400-340Z"></path></svg>
                    </button>
                </div>
            </nav>
            <section class="bg-white">
                <div>
                    <h2 class="titulos m-0">Carteras</h2>
                    <div class="d-flex">
                        <div class="form-control w-25">
                            <label class="form-label" for="">Por página:</label>
                            <select v-model="selectPorPagina">
                                <option value="25">25</option>
                                <option value="50">50</option>
                                <option value="100">100</option>
                                <option value="250">250</option>
                                <option value="500">500</option>
                                <option value="1000">1000</option>
                                <option value="">Todo</option>
                            </select>
                        </div>
                        <div class="form-control w-25">
                            <label class="form-label" for="">Vigencia:</label>
                            <select v-model="selectVigencia">
                                <option value="">Seleccione</option>
                                <option v-for="(data,index) in arrVigencias" :key="index" :value="data.vigencia">{{data.vigencia}}</option>
                            </select>
                        </div>
                        <div class="form-control w-25">
                            <label class="form-label" for="">Periodo:</label>
                            <select v-model="selectPeriodo">
                                <option value="">Seleccione</option>
                                <option v-for="(data,index) in arrPeriodos" :key="index" :value="data.periodo">{{data.periodo}}</option>
                            </select>
                        </div>
                        <div class="form-control w-25">
                            <label class="form-label" for="">Placa:</label>
                            <input type="text" v-model="strPlaca">
                        </div>
                        <div class="form-control justify-between">
                            <div></div>
                            <div class="d-flex">
                                <input type="search" v-model="strBuscar" placeholder="buscar" >
                                <button type="button" @click="getSearch()" class="btn btn-primary">Buscar</button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="table-responsive overflow-auto" style="height:50vh">
                    <table class="table table-hover fw-normal">
                        <thead>
                            <tr class="text-center">
                                <th>Vigencia</th>
                                <th>Periodo</th>
                                <th>Placa</th>
                                <th>Avaluo</th>
                                <th>Descuento</th>
                                <th>Interes</th>
                                <th>Pago</th>
                                <th>Sin descuento</th>
                                <th>Descuento matricula</th>
                                <th>Sanción</th>
                                <th>Total interés</th>
                                <th>Total sanción</th>
                                <th>Valor anterior</th>
                                <th>Saldo a pagar</th>
                                <th>Saldo a favor</th>
                                <th>Documento</th>
                                <th>Nombre</th>
                                <th>Porcentaje descuento</th>
                                <th>Porcentaje interés</th>
                                <th>Modelo</th>
                                <th>Antiguo</th>
                                <th>Blindado</th>
                                <th>Tarifa</th>
                                <th>Dias de interés</th>
                                <th>Estado</th>
                                <th>Fecha transacción</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr v-for="(data,index) in arrData" :key="index">
                                <td class="text-center">{{data.vigencia}}</td>
                                <td class="text-center">{{data.periodo}}</td>
                                <td class="text-center">{{data.placa}}</td>
                                <td class="text-right">{{formatMoney(data.valor_avaluo)}}</td>
                                <td class="text-right">{{formatMoney(data.valor_descuento)}}</td>
                                <td class="text-right">{{formatMoney(data.valor_interes)}}</td>
                                <td class="text-right">{{formatMoney(data.valor_pago)}}</td>
                                <td class="text-right">{{formatMoney(data.valor_sin_descuento)}}</td>
                                <td class="text-right">{{formatMoney(data.valor_desc_matricula)}}</td>
                                <td class="text-right">{{formatMoney(data.valor_sancion)}}</td>
                                <td class="text-right">{{formatMoney(data.valor_interes_total)}}</td>
                                <td class="text-right">{{formatMoney(data.valor_sancion_total)}}</td>
                                <td class="text-right">{{formatMoney(data.valor_anterior)}}</td>
                                <td class="text-right">{{formatMoney(data.valor_saldo_pagar)}}</td>
                                <td class="text-right">{{formatMoney(data.valor_saldo_favor)}}</td>
                                <td >{{data.documento}}</td>
                                <td >{{data.nombre}}</td>
                                <td class="text-center">{{data.porcentaje_descuento}}%</td>
                                <td class="text-center">{{data.porcentaje_interes}}%</td>
                                <td class="text-center">{{data.modelo}}</td>
                                <td class="text-center">{{data.antiguo == "S" ? "Si" : "No"}}</td>
                                <td class="text-center">{{data.blindado == "S" ? "Si" : "No"}}</td>
                                <td class="text-center">{{data.tarifa*100}}%</td>
                                <td class="text-center">{{data.dias_interes}}%</td>
                                <td class="text-center">
                                    <span :class="[data.estado == 1? 'badge-success' : 'badge-warning']"class="badge">{{ data.estado == 1 ? "Pagado" : "Pendiente"}}</span>
                                </td>
                                <td class="text-center">{{data.fecha_transaccion}}</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div v-if="arrData.length > 0" class="list-pagination-container">
                    <p>Página {{ intPagina }} de {{ intTotalPaginas }}</p>
                    <ul class="list-pagination">
                        <li v-show="intPagina > 1" @click="getSearch(intPagina = 1)"><< </li>
                        <li v-show="intPagina > 1" @click="getSearch(--intPagina)"><</li>
                        <li v-for="(pagina,index) in arrBotones" :class="intPagina == pagina ? 'active' : ''" @click="getSearch(pagina)" :key="index">{{pagina}}</li>
                        <li v-show="intPagina < intTotalPaginas" @click="getSearch(++intPagina)">></li>
                        <li v-show="intPagina < intTotalPaginas" @click="getSearch(intPagina = intTotalPaginas)" >>></li>
                    </ul>
                </div>
            </section>
            <!-- MODALES-->
            <div v-show="isModal" class="modal">
                <div class="modal-dialog modal-md">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title">Cargar cartera</h5>
                            <button type="button" @click="isModal=false;" class="btn btn-close"><div></div><div></div></button>
                        </div>
                        <div class="modal-body">
                            <div class="d-flex">
                                <div class="form-control">
                                    <label class="form-label">Vigencia</label>
                                    <input type="number" v-model="intVigencia">
                                </div>
                            </div>
                            <div class="d-flex">
                                <div class="form-control">
                                    <label class="form-label">Descargue la plantilla y llene los datos correspondientes</label>
                                    <button type="button" class="btn btn-primary" @click="getPlantilla()">Descargar plantilla</button>
                                </div>
                            </div>
                            <div class="d-flex">
                                <div class="form-control">
                                    <label class="form-label">Seleccione el archivo y cargue</label>
                                    <div class="d-flex align-items-center">
                                        <input type="file" @change="getFile(this)" accept="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel">
                                        <button type="button" class="btn btn-primary w-25" @click="upload()">Cargar</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </main>
        <script src="Librerias/vue/vue.min.js"></script>
		<script type="module" src="transporte/js/functions_base_cartera.js?<?= date('d_m_Y_h_i_s');?>"></script>
	</body>
</html>

