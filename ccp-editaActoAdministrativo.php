<?php
	header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=utf8");
	require 'comun.inc';
	require 'funciones.inc';
	session_start();
	$linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");
	cargarcodigopag($_GET['codpag'],$_SESSION["nivel"]);
	date_default_timezone_set("America/Bogota");
	titlepag();
?>
<!DOCTYPE >
<html lang=es>
	<head>
		<meta charset="utf-8"/>
		<meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1"/>
		<meta name="viewport" content="user-scalable=no">
		<title>:: IDEAL 10 - Presupuesto</title>
		<link href="css/css2z.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
		<link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
		<link href="css/css4.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
		<link href="css/tabs.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css"/>
		<script src="vue/vue.min.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script src="css/programas.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script src="css/calendario.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<style>
			input[type=number]::-webkit-inner-spin-button,
			input[type=number]::-webkit-outer-spin-button {
				-webkit-appearance: none;
				margin: 0;
			}
			input[type=number]{
				text-align:right;
				box-sizing: border-box;
				font: 120% sans-serif;
				width: 100% !important;
				height: 30px;
			}
			[v-cloak]{display : none;}


		</style>
		<script src="sweetalert2/dist/sweetalert2.min.js"></script>
		<link rel="stylesheet" href="sweetalert2/dist/sweetalert2.min.css">
	</head>
	<body>
		<header>
			<table>
				<tr><script>barra_imagenes("ccpet");</script><?php cuadro_titulos();?></tr>
			</table>
		</header>

		<form name="form2" method="post" action="">
			<section id="myapp" v-cloak >
				<nav>
					<table>
						<tr><?php menu_desplegable("ccpet");?></tr>
					</table>
                    <div class="bg-white group-btn p-1"><button type="button" v-on:click="location.href='ccp-crearActoAdministrativo.php'"
                        class="btn btn-white btn-primary-hover d-flex justify-between align-items-center">
                        <span>Nuevo</span>
                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 -960 960 960">
                            <path d="M440-440H200v-80h240v-240h80v240h240v80H520v240h-80v-240Z"></path>
                        </svg>
                    </button>
                    <button type="button" v-on:click="guardar" class="btn btn-white btn-primary-hover d-flex justify-between align-items-center">
                        <span>Guardar</span>
                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 -960 960 960">
                            <path d="M840-680v480q0 33-23.5 56.5T760-120H200q-33 0-56.5-23.5T120-200v-560q0-33 23.5-56.5T200-840h480l160 160Zm-80 34L646-760H200v560h560v-446ZM480-240q50 0 85-35t35-85q0-50-35-85t-85-35q-50 0-85 35t-35 85q0 50 35 85t85 35ZM240-560h360v-160H240v160Zm-40-86v446-560 114Z">
                            </path>
                        </svg>
                    </button>
                    <button type="button" v-on:click="location.href='ccp-buscarActoAdministrativo.php'"
                        class="btn btn-white btn-primary-hover d-flex justify-between align-items-center">
                        <span>Buscar</span>
                        <svg class="fill-black group-hover:fill-white w-5 h-5" xmlns="http://www.w3.org/2000/svg"
                            viewBox="0 -960 960 960">
                            <path
                                d="M784-120 532-372q-30 24-69 38t-83 14q-109 0-184.5-75.5T120-580q0-109 75.5-184.5T380-840q109 0 184.5 75.5T640-580q0 44-14 83t-38 69l252 252-56 56ZM380-400q75 0 127.5-52.5T560-580q0-75-52.5-127.5T380-760q-75 0-127.5 52.5T200-580q0 75 52.5 127.5T380-400Z">
                            </path>
                        </svg>
                    </button>
                    <button type="button" onclick="mypop=window.open('ccp-principal.php','','');mypop.focus();" class="btn btn-white btn-primary-hover d-flex justify-between align-items-center">
                        <span>Nueva ventana</span>
                        <svg class="fill-black group-hover:fill-white w-5 h-5" xmlns="http://www.w3.org/2000/svg" viewBox="0 -960 960 960">
                            <path d="M320-120v-80h80v-80H160q-33 0-56.5-23.5T80-360v-400q0-33 23.5-56.5T160-840h640q33 0 56.5 23.5T880-760v400q0 33-23.5 56.5T800-280H560v80h80v80H320ZM160-360h640v-400H160v400Zm0 0v-400 400Z"></path>
                        </svg>
                    </button>
                    <button type="button" v-on:click="location.href='ccp-buscarActoAdministrativo.php'" class="btn btn-success btn-success-hover d-flex justify-between align-items-center">
                        <span>Atras</span>
                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 -960 960 960">
                            <path d="M760-200v-160q0-50-35-85t-85-35H273l144 144-57 56-240-240 240-240 57 56-144 144h367q83 0 141.5 58.5T840-360v160h-80Z"></path>
                        </svg>
                    </button>
                </div>
				</nav>
				<article>
					<table class="inicio ancho">
						<tr>
							<td class="titulos" colspan="6">.: Agregar Acto Administrativo:</td>
							<td class="cerrar" style="width:7%" onClick="location.href='ccp-principal.php'">Cerrar</td>
						</tr>

						<tr>
                            <td class="textoNewR">
                                <label class="labelR">Fecha:</label>
                            </td>

                            <td>
								<input type="text" name="fecha" id="fecha" onchange="" maxlength="10" onKeyUp="return tabular(event,this)" id="fecha" onKeyDown="mascara(this,'/',patron,true)" title="DD/MM/YYYY" style="height:30px;text-align:center;" ondblclick="displayCalendarFor('fecha');" class="colordobleclik" autocomplete="off">
                            </td>

                            <td class="textoNewR">
                                <label class="labelR">Tipo de acto:</label>
                            </td>
                            <td>
                                <select v-model="tipoActo" style="width: 98%;" disabled>
									<option disabled value="">Seleccione</option>
									<option value="1">Por acuerdo</option>
									<option value="2">Por resolución</option>
									<option value="3">Por decreto</option>
								</select>
                            </td>

                            <td class="textoNewR">
                                <label class="labelR">Número:</label>
                            </td>

                            <td>
                                <input type="text" v-model="consecutivo" style="width: 98%;" readonly>
                            </td>
						</tr>

                        <tr>
                            <td class="textoNewR">
                                <label class="labelR">Acto Adtvo:</label>
                            </td>

                            <td colspan="3">
                                <input type="text" v-model="descripcionActo" style="width: 99.5%;">
                            </td>

							<td class="textoNewR">
								<label class="labelR">Tipo de acuerdo:</label>
							</td>

							<td>
								<select style="width: 98%;" v-model="tipoAcuerdo">
									<option value="I">Inicial</option>
									<option value="M">Modificación</option>
								</select>
							</td>
                        </tr>

                        <tr v-if="tipoAcuerdo == 'I'">
                            <td class="textoNewR">
                                <label class="labelR">Valor inicial:</label>
                            </td>

                            <td>
                                <input type="number" v-model="valorInicial">
                            </td>
                        </tr>

						<tr v-else-if="tipoAcuerdo == 'M'">
							<td class="textoNewR">
                                <label class="labelR">Valor adición:</label>
                            </td>

                            <td>
                                <input type="number" v-model="valorAdicion">
                            </td>

							<td class="textoNewR">
                                <label class="labelR">Valor reducción:</label>
                            </td>

                            <td>
                                <input type="number" v-model="valorReduccion">
                            </td>

							<td class="textoNewR">
                                <label class="labelR">Valor traslados:</label>
                            </td>

                            <td>
                                <input type="number" v-model="valorTraslados">
                            </td>
						</tr>
					</table>

					<div id="cargando" v-if="loading" class="loading">
						<span>Cargando...</span>
					</div>

				</article>
			</section>
		</form>

		<script src="Librerias/vue/vue.min.js"></script>
		<script src="Librerias/vue/axios.min.js"></script>

		<script src="presupuesto_ccpet/actoAdministrativo/editar/ccp-editaActoAdministrativo.js"></script>

	</body>
</html>
