<?php
	header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=utf8");
	require 'comun.inc';
	require 'funciones.inc';
	session_start();
	$linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");
	cargarcodigopag($_GET['codpag'],$_SESSION["nivel"]);
	date_default_timezone_set("America/Bogota");
?>
<!DOCTYPE >
<html lang=es>
	<head>
		<meta charset="utf-8"/>
		<meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1"/>
		<meta name="viewport" content="user-scalable=no">
		<title>:: IDEAL 10 - Presupuesto CCPET</title>
		<link href="favicon.ico" rel="shortcut icon"/>
		<link href="css/css2z.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
		<link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
		<link href="css/css4.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
        <link href="css/tabs.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css"/>
        <link href="css/style.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
        <link rel="stylesheet" href="sweetalert2/dist/sweetalert2.min.css">
        <script src="sweetalert2/dist/sweetalert2.min.js"></script>
		<script src="vue/vue.min.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script src="css/programas.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script src="css/calendario.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
        <script>

        </script>
		<style>
			input[type=number]::-webkit-inner-spin-button,
			input[type=number]::-webkit-outer-spin-button {
				-webkit-appearance: none;
				margin: 0;
			}
			input[type=number]{
				text-align:right;
				box-sizing: border-box;
				font: 120% sans-serif;
				/* width: 100% !important; */
			}
			[v-cloak]{display : none;}

			.centrarSelect {

				height: 30px !important;
				text-align-last:center !important;
			}

			.aumentarTamaño {

				font-size:15px;
			}

            .tamano01 {
                text-align: center !important;
            }

			.captura{
				color: black !important;
                font-weight: normal;
                cursor: pointer !important;
            }

            .captura:hover{
                background: linear-gradient(#40f3ff , #40b3ff 70%, #40f3ff );
            }

            .colorFila{
                background-color: #F0FFFD !important;
            }

            .colorFila1{
                background-color: #FFF02A !important;
                font-family: -apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, "Helvetica Neue", Arial, "Noto Sans", sans-serif, "Apple Color Emoji", "Segoe UI Emoji", "Segoe UI Symbol", "Noto Color Emoji";
                font-size:10px;
                font-weight:normal;
                border:#eeeeee 1px solid;
                padding-left:3px;
                padding-right:3px;
                margin-bottom:1px;
                margin-top:1px;
                height:23.5px;
                border-radius: 1px;
            }

            .colorFila1:hover{
                background: linear-gradient(#40f3ff , #40b3ff 70%, #40f3ff );
            }

            .agregado{
				color: black !important;
                font-weight: 700 !important;
            }

			.bienesServicios{
				font-weight: 600 !important;
				font-style: oblique;
				cursor: pointer !important;
			}

		</style>
	</head>
	<body>
		<header>
			<table>
				<tr><script>barra_imagenes("ccpet");</script><?php cuadro_titulos();?></tr>
			</table>
		</header>

		<form name="form2" method="post" action="">
			<section id="myapp" v-cloak >
				<nav>
					<table>
						<tr><?php menu_desplegable("ccpet");?></tr>
					</table>
                    <div class="bg-white group-btn p-1" id="newNavStyle">
                        <button type="button" class="btn btn-white btn-primary-hover d-flex justify-between align-items-center" onclick="mypop=window.open('ccp-principal.php','',''); mypop.focus();">
                            <span>Nueva ventana</span>
                            <svg class="fill-black group-hover:fill-white w-5 h-5" xmlns="http://www.w3.org/2000/svg"  viewBox="0 -960 960 960" ><path d="M320-120v-80h80v-80H160q-33 0-56.5-23.5T80-360v-400q0-33 23.5-56.5T160-840h640q33 0 56.5 23.5T880-760v400q0 33-23.5 56.5T800-280H560v80h80v80H320ZM160-360h640v-400H160v400Zm0 0v-400 400Z"/></svg>
                        </button>
                        <button type="button" @click="excel" class="btn btn-success btn-success-hover d-flex justify-between align-items-center">
                            <span>Exportar Excel</span>
                            <svg class="fill-black group-hover:fill-white w-4 h-4" xmlns="http://www.w3.org/2000/svg"  viewBox="0 0 384 512"><!--!Font Awesome Free 6.5.1 by @fontawesome - https://fontawesome.com License - https://fontawesome.com/license/free Copyright 2024 Fonticons, Inc. --><path d="M48 448V64c0-8.8 7.2-16 16-16H224v80c0 17.7 14.3 32 32 32h80V448c0 8.8-7.2 16-16 16H64c-8.8 0-16-7.2-16-16zM64 0C28.7 0 0 28.7 0 64V448c0 35.3 28.7 64 64 64H320c35.3 0 64-28.7 64-64V154.5c0-17-6.7-33.3-18.7-45.3L274.7 18.7C262.7 6.7 246.5 0 229.5 0H64zm90.9 233.3c-8.1-10.5-23.2-12.3-33.7-4.2s-12.3 23.2-4.2 33.7L161.6 320l-44.5 57.3c-8.1 10.5-6.3 25.5 4.2 33.7s25.5 6.3 33.7-4.2L192 359.1l37.1 47.6c8.1 10.5 23.2 12.3 33.7 4.2s12.3-23.2 4.2-33.7L222.4 320l44.5-57.3c8.1-10.5 6.3-25.5-4.2-33.7s-25.5-6.3-33.7 4.2L192 280.9l-37.1-47.6z"/></svg>
                        </button>
                        <button type="button" onclick="window.location.href='ccp-ejecucionpresupuestal.php'" class="btn btn-success btn-success-hover d-flex justify-between align-items-center">
                            <span>Atras</span>
                            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 -960 960 960"><path d="M760-200v-160q0-50-35-85t-85-35H273l144 144-57 56-240-240 240-240 57 56-144 144h367q83 0 141.5 58.5T840-360v160h-80Z"/></svg>
                        </button>
                    </div>
				</nav>

				<article>
                    <div>
                        <table class="inicio">
                            <tr>
                                <td class="titulos" colspan="12" style="text-align:center; font-weight: 600;">.: Ejecuci&oacute;n presupuestal de ingresos</td>
                            	<td class="cerrar" style="width:4%" onClick="location.href='ccp-principal.php'">Cerrar</td>
                            </tr>

                            <tr>
								<td style="width: 10%;">
									<label class="labelR" for="">Dependencia:</label>
								</td>
                                <td style="width: 10%;">
                                    <select style="width:80%;" v-model="secPresu" @change="cambiaCriteriosBusqueda">
                                        <option value="">Todas</option>
                                        <option v-for="sec in seccionPresupuestal" v-bind:value="sec[0]">
                                            {{ sec[0] }} - {{ sec[1] }}
                                        </option>
                                    </select>
                                </td>

                                <td style="width: 10%;">
									<label class="labelR" for="">Situción/Fondo:</label>
								</td>
                                <td style="width: 10%;">
                                    <select style="width:80%;" v-model="medioPago" @change="cambiaCriteriosBusqueda">
                                        <option value="">Todos</option>
                                        <option value="CSF">CSF</option>
                                        <option value="SSF">SSF</option>
                                    </select>
                                </td>

                                <td style="width: 15%;">
									<label class="labelR" for="">Vig del gasto:</label>
								</td>
                                <td style="width: 10%;">
                                    <select style="width:80%;" v-model="vigGasto" @change="cambiaCriteriosBusqueda">
                                        <option value="">Todas</option>
                                        <option v-for="vg in vigenciaGasto" v-bind:value="vg[0]">
                                            {{ vg[0 ]}} - {{ vg[1] }}
                                        </option>
                                    </select>
                                </td>

								<td style = "width: 8%;">
									<label class="labelR" for="">Fuente:</label>
								</td>
								<td colspan="2">
									<div>
										<multiselect
											v-model="valueFuentes"
											placeholder="Seleccione una o varias fuentes"
											label="fuente" track-by="fuente"
											:custom-label="fuenteConNombre"
											:options="optionsFuentes"
											:multiple="true"
											:taggable="false"
											@input = "cambiaCriteriosBusqueda"
                                            @open="cambiaCriteriosBusqueda"
										></multiselect>
									</div>
								</td>
                            </tr>

                            <tr>
                                <td>
									<label class="labelR" for="">Fecha Inicial:</label>
								</td>
                                <td>
                                    <input style="width:80%;" type="text" name="fechaIni"  value="<?php echo $_POST['fechaIni']?>" onKeyUp="return tabular(event,this)" id="fechaIni" title="DD/MM/YYYY" onDblClick="displayCalendarFor('fechaIni');" class="colordobleclik" autocomplete="off" onChange="" readonly>
                                </td>

                                <td>
									<label class="labelR" for="">Fecha Final:</label>
								</td>
                                <td style="width:10%;">
                                    <input style="width:80%;" type="text" name="fechaFin" value="<?php echo $_POST['fechaFin']?>" onKeyUp="return tabular(event,this)" id="fechaFin" title="DD/MM/YYYY" onDblClick="displayCalendarFor('fechaFin');" class="colordobleclik" autocomplete="off" onChange="" readonly>
                                </td>
								<td>
                                	<label class="labelR">Clasificador de ingresos:</label>
								</td>
								<td>
									<select style="width:80%;" v-model="selectClasificador" @change="cuentasDelClasificador">
										<option value="-1">Ninguno...</option>
										<option v-for="clasificador in clasificadores" v-bind:value="clasificador[0]">
											{{ clasificador[1] }}
										</option>
									</select>
								</td>
                                <td>
                                	<label class="labelR">Niveles:</label>
								</td>
								<td>
									<select style="width:80%;" v-model="nivelRubro" @change="cuentasDelClasificador">
										<option value="-1">Todos...</option>
										<option v-for="nivel in nivelesRubro" v-bind:value="nivel">
											{{ nivel }}
										</option>
									</select>
                                    <td>
                                        <button type="button" class="botonflechaverde" v-on:click="traeDatosFormulario">Generar</button>
                                    </td>
								</td>
								
                            </tr>
                        </table>

						<div class='subpantalla estilos-scroll' v-show="mostrarEjecucion" style='height:54vh; margin-top:0px; resize: vertical;'>
                            <table class = "tabla-ejecucion">
                                <thead>
                                    <tr style="text-align:Center;">
										<th class="titulosnew00" style="width:3%;">Vig. gasto</th>
										<th class="titulosnew00" style="width:3%;">Dependencia</th>
                                        <th class="titulosnew00" style="width:5%;">Codigo</th>
                                        <th class="titulosnew00">Nombre</th>
                                        <th class="titulosnew00">Clasificador</th>
                                        <th class="titulosnew00">Nombre clasificador</th>
										<th class="titulosnew00" style="width:5%;">Fuente</th>
										<th class="titulosnew00" style="width:12%;">Nombre Fuente</th>
										<th class="titulosnew00" style="width:3%;">Medio Pago</th>
										<th class="titulosnew00" style="width:3%;">CPC</th>
										<th class="titulosnew00" style="width:6%;">Nombre CPC</th>
                                        <th class="titulosnew00" style="width:6%;">Presupuesto Inicial</th>
                                        <th class="titulosnew00" style="width:6%;">Adición</th>
                                        <th class="titulosnew00" style="width:6%;">Reducción</th>
										<th class="titulosnew00" style="width:6%;">Definitivo</th>
										<th class="titulosnew00" style="width:6%;">Total recaudos</th>
										<th class="titulosnew00" style="width:6%;">Saldo por recaudar</th>
                                        <th class="titulosnew00" style="width:6%;">En ejecucción</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr v-for="(det,index) in arbolIngresos" :style="esBienesServicios(det[0]) == true ? myStyle : ''" v-bind:class="[det[8] != '' ? 'bienesServicios' : (det[2] === 'C' ? 'captura' : 'agregado'), (index % 2 ? 'contenidonew00' : 'contenidonew01')]" v-on:dblclick="auxiliarDetalle(det)">
										<td style="width:3%; text-align:center; ">{{ det[3] }}</td>
										<td style="width:3%; text-align:center;">{{ det[4] }} {{ nombreSeccion(det[4]) }}</td>
										<td style="width:5%; text-align:left;">{{ det[0] }}</td>
										<td style="text-align:left;">{{ det[1] }}</td>
                                        <td>{{ det[19] }}</td>
                                        <td>{{ det[20] }}</td>
										<td style="width:5%; text-align:left;">{{ det[5] }}</td>
										<td style="width:8%; text-align:left;">{{ det[6] }}</td>
										<td style="width:3%; text-align:left;">{{ det[7] }}</td>
										<td style="width:3%; text-align:left;">{{ det[8] }}</td>
										<td style="width:6%; text-align:left;">{{ det[9] }}</td>
										<td style="width:6%; text-align:right;">{{ formatonumero(det[10]) }}</td>
										<td style="width:6%; text-align:right;">{{ formatonumero(det[11]) }}</td>
										<td style="width:6%; text-align:right;">{{ formatonumero(det[12]) }}</td>
										<td style="width:6%; text-align:right;">{{ formatonumero(det[13]) }}</td>
										<td style="width:6%; text-align:right;">{{ formatonumero(det[16]) }}</td>
										<td style="width:6%; text-align:right;">{{ formatonumero(det[17]) }}</td>
										<td style="width:6%; text-align:center;">{{ det[18] }} %</td>

										<input type='hidden' name='tipo[]' v-model="det[2]">
										<input type='hidden' name='vigGasto[]' v-model="det[3]">
										<input type='hidden' name='secPresupuestal[]' v-model="det[4]">
										<input type='hidden' name='nombreSecPresupuestal[]' v-model="nombreSeccion(det[4])">
										<input type='hidden' name='codigoCuenta[]' v-model="det[0]">
										<input type='hidden' name='nombreCuenta[]' v-model="det[1]">
										<input type='hidden' name='fuente[]' v-model="det[5]">
										<input type='hidden' name='nombreFuente[]' v-model="det[6]">
										<input type='hidden' name='medioPago[]' v-model="det[7]">
										<input type='hidden' name='cpc[]' v-model="det[8]">
										<input type='hidden' name='nombreCpc[]' v-model="det[9]">
										<input type='hidden' name='presuIni[]' v-model="det[10]">
										<input type='hidden' name='adicion[]' v-model="det[11]">
										<input type='hidden' name='reduccion[]' v-model="det[12]">
										<input type='hidden' name='definitivo[]' v-model="det[13]">
										<input type='hidden' name='recaudoConsulta[]' v-model="det[15]">
										<input type='hidden' name='totalRecaudos[]' v-model="det[16]">
										<input type='hidden' name='saldoRecaudar[]' v-model="det[17]">
										<input type='hidden' name='enEjecucion[]' v-model="det[18]">
                                        <input type='hidden' name='clasificador[]' v-model="det[19]">
                                        <input type='hidden' name='nombre_clasificador[]' v-model="det[20]">
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>

					<div id="cargando" v-if="loading" class="loading">
						<span>Cargando...</span>
					</div>
				</article>
			</section>
		</form>

		<script src="node_modules/read-excel-file/bundle/read-excel-file.min.js"></script>
		<script src="node_modules/xlsx/dist/xlsx.full.min.js"></script>
		<script src="https://cdn.jsdelivr.net/npm/vue@2.5.16/dist/vue.js"></script>
        <script src="https://unpkg.com/vue-multiselect@2.1.0"></script>
        <link rel="stylesheet" href="multiselect.css">

		<script src="Librerias/vue/axios.min.js"></script>
		<script type="module" src="presupuesto_ccpet/ejecucionIngresos/ccp-ejecuPresuIngresos.js?<?= date('d_m_Y_h_i_s');?>"></script>

	</body>
</html>
