const URL = 'planeacion/solicitudCertificadoPAA/visualizar/plan-visualizarSolicitudCertificadoPAA.php';
import { filtroEnArrayDeObjetos } from './../../../funciones.js';

var app = new Vue({
    el: '#myapp',
	data:{

        error: '',
        loading: false,
        fecha: '',
        searchPlan: '',
        codPaa: '',
        consecutivo: '',
        descripcionPaa: '',
        observacion: '',
        productos: [],
    },

    mounted: function(){

        this.datosIniciales();
    },

    methods: 
    {
        //Desglosar array
        toFormData: function(obj){
            var form_data = new FormData();
            for(var key in obj){
                form_data.append(key, obj[key]);
            }
            return form_data;
        },

        formatonumero: function(valor){
            return new Intl.NumberFormat("en-US", {style: "currency", currency: "USD"}).format(valor);
        },

        traeParametros: function(name){
            //Captura parametros
            name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
            var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
            results = regex.exec(location.search);
            return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
        },

        datosIniciales: async function() {
            
            this.loading = true;

            const cod = this.traeParametros("cod");
            this.consecutivo = cod;
            await axios.post(URL+'?action=datosIniciales&cod='+cod)
            .then((response) => {
                console.log(response.data);
                this.productos = response.data.datos;
                this.fecha = response.data.fecha;
                this.codPaa = response.data.codsolicitud;
                this.descripcionPaa = response.data.descripcion;
                this.observacion = response.data.observacion;
            }).finally(() => {
                this.loading = false; 
            });
        },

    },
});