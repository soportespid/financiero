<?php

	require_once '../../../comun.inc';
    require '../../../funciones.inc';
    header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=utf8");
    session_start();

	$linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");

    $out = array('error' => false);

    $action = "show";

    if(isset($_GET['action'])){
        $action = $_GET['action'];
    }

    if ($action == "datosIniciales") {

        // var_dump($_GET);
        $codigo = "";
        $codigo = $_GET["cod"];
        $productos = [];
        $datos = [];

        $sqlPlanes = "SELECT codigo, codsolicitud, fecha, observaciones, descripcion, codigossol FROM contrasolicitudpaa WHERE codigo = $codigo";
        $resPlanes = mysqli_query($linkbd, $sqlPlanes);
        $rowPlanes = mysqli_fetch_row($resPlanes);

        $productos = explode("-",$rowPlanes[5]);

        foreach ($productos as $key => $cod) {
            $datosTemp = [];

            $sqlName = "SELECT nombre FROM productospaa WHERE codigo = '$cod' AND estado = 'S'";
            $resName = mysqli_query($linkbd, $sqlName);
            $rowName = mysqli_fetch_row($resName);

            $datosTemp['id'] = $cod;
            $datosTemp['nombre'] = $rowName[0];
            array_push($datos, $datosTemp);
        }
        
        $out['fecha'] = $rowPlanes[2];
        $out['codsolicitud'] = $rowPlanes[1];
        $out['descripcion'] = $rowPlanes[4];
        $out['observacion'] = $rowPlanes[3];
        $out['datos'] = $datos;
    }

    header("Content-type: application/json");
    echo json_encode($out);
    die();