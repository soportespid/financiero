const URL = 'planeacion/solicitudCertificadoPAA/buscar/plan-buscaCertificadoPAA.php';
import { filtroEnArrayDeObjetos } from './../../../funciones.js';

var app = new Vue({
    el: '#myapp',
	data:{

        error: '',
        loading: false, 
        datos: [],
        datosCopy: [],
        searchDato: '',
    },

    mounted: function(){

        this.datosIniciales();
    },

    methods: 
    {
        //Desglosar array
        toFormData: function(obj){
            var form_data = new FormData();
            for(var key in obj){
                form_data.append(key, obj[key]);
            }
            return form_data;
        },

        formatonumero: function(valor){
            return new Intl.NumberFormat("en-US", {style: "currency", currency: "USD"}).format(valor);
        },

        datosIniciales: async function() {
            
            this.loading = true;

            await axios.post(URL+'?action=datosIniciales')
            .then((response) => {
                this.datos = response.data.datos;
                this.datosCopy = response.data.datos;
            }).finally(() => {
                this.loading = false; 
            });
        },

        filtroDatos: async function() {

            const data = this.datosCopy;
            var text = this.searchDato;
            var resultado = [];

            resultado = await filtroEnArrayDeObjetos({'data': data, 'text': text});

            this.datos = resultado;
        },

        visualizar: function(datos) {

            const x = "plan-visualizarSolicitudCertificadoPAA?cod="+datos.codigo;
            window.open(x, '_blank');
            window.focus();
        },
    },
});