<?php

	require_once '../../../comun.inc';
    require '../../../funciones.inc';
    header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=utf8");
    session_start();

	$linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");

    $out = array('error' => false);

    $action = "show";

    if(isset($_GET['action'])){
        $action = $_GET['action'];
    }

    if ($action == "datosIniciales") {

        $datos = [];

        $sqlSearch = "SELECT codigo, codsolicitud, fecha, descripcion FROM contrasolicitudpaa WHERE estado = 'A'";
        $resSearch = mysqli_query($linkbd, $sqlSearch);
        while ($rowSearch = mysqli_fetch_row($resSearch)) {
            
            $datosTemp = [];

            $datosTemp["codigo"] = $rowSearch[0];
            $datosTemp["codPaa"] = $rowSearch[1];
            $datosTemp["descripcion"] = $rowSearch[3];
            $datosTemp["fecha"] = $rowSearch[2];
            array_push($datos, $datosTemp);
        }

        $out['datos'] = $datos;
    }

    header("Content-type: application/json");
    echo json_encode($out);
    die();