<?php

	require_once '../../../comun.inc';
    require '../../../funciones.inc';
    header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=utf8");
    session_start();

	$linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");

    $out = array('error' => false);

    $action = "show";

    if(isset($_GET['action'])){
        $action = $_GET['action'];
    }

    if ($action == "datosIniciales") {

        $planes = [];

        $vigencia = vigencia_usuarios($_SESSION['cedulausu']);
        $sqlPlanes = "SELECT codplan, descripcion, codigosunspsc, clasificador FROM contraplancompras WHERE vigencia = '$vigencia' ORDER BY id ASC";
        $resPlanes = mysqli_query($linkbd, $sqlPlanes);
        while ($rowPlanes = mysqli_fetch_row($resPlanes)) {
            
            array_push($planes, $rowPlanes);
        }

        $out['planes'] = $planes;
    }

    if ($action == "seleccionaPlan") {

        $codigos = $datos = [];
        $codigos = explode("-",$_GET['productos']);
        
        foreach ($codigos as $key => $cod) {
            
            $datosTemp = [];

            $sqlName = "SELECT nombre FROM productospaa WHERE codigo = '$cod' AND estado = 'S'";
            $resName = mysqli_query($linkbd, $sqlName);
            $rowName = mysqli_fetch_row($resName);

            $datosTemp['id'] = $cod;
            $datosTemp['nombre'] = $rowName[0];
            array_push($datos, $datosTemp);
        }

        $out['productos'] = $datos;
    }

    if ($action == "guardar") {
        
        $fechaSolicitud = $codPaa = $descripcion = $observacion = $consecutivo = $vigencia = $productosGroup = "";
        $productos = [];

        preg_match("/([0-9]{2})\/([0-9]{2})\/([0-9]{4})/", $_POST['fecha'],$fecha);
        $fechaSolicitud="$fecha[3]-$fecha[2]-$fecha[1]";
        $vigencia = $fecha[3];
        $codPaa = $_POST['codPaa'];
        $descripcion = $_POST['descripcionPaa'];
        $observacion = $_POST['observacion'];
        $productos = $_POST['productos'];
        $productosGroup = implode("-", $productos);
        $consecutivo = selconsecutivo("contrasolicitudpaa", "codigo");
        
        $sqlInsert = "INSERT INTO contrasolicitudpaa (codigo, codsolicitud, fecha, estado, observaciones, vigencia, descripcion, codigosaprob, fecha_certi, codigossol) VALUES ($consecutivo, $codPaa, '$fechaSolicitud', 'P', '$observacion', '$vigencia', '$descripcion', '$productosGroup', '$fechaSolicitud', '$productosGroup')";
        mysqli_query($linkbd, $sqlInsert);
        $out['cod'] = $consecutivo;
        $out['insertaBien'] = true;
    }

    header("Content-type: application/json");
    echo json_encode($out);
    die();