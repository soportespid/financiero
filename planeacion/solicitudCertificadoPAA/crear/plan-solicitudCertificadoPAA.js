const URL = 'planeacion/solicitudCertificadoPAA/crear/plan-solicitudCertificadoPAA.php';
import { filtroEnArrayDeObjetos } from './../../../funciones.js';

var app = new Vue({
    el: '#myapp',
	data:{

        error: '',
        loading: false, 
        showpaa: false,
        planes: [],
        planescopy: [],
        searchPlan: '',
        codPaa: '',
        descripcionPaa: '',
        observacion: '',
        productos: [],
        selected: [],
        select_all: false,
    },

    mounted: function(){

        this.datosIniciales();
    },

    methods: 
    {
        //Desglosar array
        toFormData: function(obj){
            var form_data = new FormData();
            for(var key in obj){
                form_data.append(key, obj[key]);
            }
            return form_data;
        },

        formatonumero: function(valor){
            return new Intl.NumberFormat("en-US", {style: "currency", currency: "USD"}).format(valor);
        },

        datosIniciales: async function() {
            
            this.loading = true;

            await axios.post(URL+'?action=datosIniciales')
            .then((response) => {
                this.planes = response.data.planes;
                this.planescopy = response.data.planes;
            }).finally(() => {
                this.loading = false; 
            });
        },

        despliegaModalPlan: function() {
            this.showpaa = true;
        },

        filtroPaa: async function() {

            const data = this.planescopy;
            var text = this.searchPlan;
            var resultado = [];

            resultado = await filtroEnArrayDeObjetos({'data': data, 'text': text});

            this.planes = resultado;
        },

        selecPaa: async function(plan) {

            await axios.post(URL+'?action=seleccionaPlan&productos='+plan[2])
            .then((response) => {
                this.productos = response.data.productos;
            });

            this.codPaa = plan[0];
            this.descripcionPaa = plan[1];
            this.showpaa = false;
        },

        seleccionaTodos: function() {

            this.selected = [];
            if (!this.select_all) {
                for (let i in this.productos) {
                    this.selected.push(this.productos[i].id);
                }
            }
        },

        guardar: function() {

            const fecha = document.getElementById("fechaSolicitud").value;

            if (fecha != "" && this.codPaa != "" && this.descripcionPaa != "" && this.observacion != "") {

                if (this.selected.length > 0) {

                    Swal.fire({
                        icon: 'question',
                        title: 'Esta seguro que quiere guardar?',
                        showDenyButton: true,
                        confirmButtonText: 'Guardar!',
                        denyButtonText: 'Cancelar',
                    }).then((result) => {
        
                        if (result.isConfirmed) {
        
                            var formData = new FormData();
        
                            formData.append("fecha", fecha);
                            formData.append("codPaa", this.codPaa);
                            formData.append("descripcionPaa", this.descripcionPaa);
                            formData.append("observacion", this.observacion);
                          
                            for(let i=0; i <= this.selected.length-1; i++) {
        
                                formData.append("productos[]", this.selected[i]);                        
                            }
        
                            axios.post(URL + '?action=guardar', formData)
                            .then((response) => {
                                console.log(response.data);
                                if(response.data.insertaBien){
                                    Swal.fire({
                                        icon: 'success',
                                        title: 'Solicitud de certificado guardado con exito '+ response.data.cod,
                                        showConfirmButton: false,
                                        timer: 3500
                                    });

                                    location.reload();
                                }
                                else{
                                
                                    Swal.fire(
                                        'Error!',
                                        'No se pudo guardar.',
                                        'error'
                                    );
                                }
                                
                            });
                        } 
                    })  
                }
                else {
                    Swal.fire({
                        icon: 'warning',
                        title: 'Faltan datos',
                        text: 'No has seleccionado ningun producto'
                    })    
                }
            }   
            else {
                Swal.fire({
                    icon: 'warning',
                    title: 'Faltan datos',
                    text: 'Faltan datos en la cabecera'
                })    
            }         
        },
    },
});