<?php
    require_once '../../../Librerias/core/Helpers.php';
    require_once '../models/planIndicativo_model.php';
    session_start();
    header('Content-Type: application/json');

    class indicativoController extends indicativoModel {
        /* crear */
        public function get() {
            if (!empty($_SESSION)) {
                $arrResponse = array("status" => true, "planes" => $this->planesDesarrolloTerritorial(), "fuentes" => $this->fuentes());
                echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
            }
            die();
        }

        public function getDataPdt() {
            if (!empty($_SESSION)) {
                $pdtId = $_POST["pdtId"];
                $arrResponse = array("periodoGobierno" => $this->periodoGobierno($pdtId), "indicadorProductos" => $this->searchIndicadorProductos($pdtId));
                echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
            }
            die();
        }

        public function getLinea() {
            if (!empty($_SESSION)) {
                $id = $_POST["id"];
                $arrResponse = array("lineas" => $this->searchLineasEstrategicas($id));
                echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
            }
            die();
        }

        public function checkBpin() {
            if (!empty($_SESSION)) {
                $bpin = (int) $_POST["bpin"];
                $arrResponse = array("cantidad" => $this->searchBpin($bpin));
                echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
            }
            die();
        }

        public function save() {
            if (!empty($_SESSION)) {
                $fuentes = json_decode($_POST['fuentes'],true);
                $bpins = json_decode($_POST['bpins'],true);
                $value = [
                    $_POST["pdtId"],
                    $_POST["indicadorProductoId"],
                    $_POST["tipoAcumulacion"],
                    round(floatval($_POST["valorProdServ01"]), 2),
                    round(floatval($_POST["valorProdServ02"]), 2),
                    round(floatval($_POST["valorProdServ03"]), 2),
                    round(floatval($_POST["valorProdServ04"]), 2),
                    "S"
                ];

                $id = $this->savePlanIndicativo($value);
                if ($id > 0) {
                    $this->saveFuentes($id, $fuentes);
                    $this->saveBpins($id, $bpins);
                    $arrResponse = array("status" => true, "msg" => "Plan indicativo guardado con exito", "id" => $id);
                }
                else {
                    $arrResponse = array("status" => false, "msg" => "Error al guardar, intente de nuevo");
                }
                echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
            }
            die();
        }

        /* buscar */
        public function getSearch() {
            if (!empty($_SESSION)) {
                $arrResponse = array("data" => $this->getSearchData());
                echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
            }
            die();
        }

        /* editar */
        public function getEdit() {
            if (!empty($_SESSION)) {
                $id = (int) $_POST["id"];
                $data = $this->getDataEdit($id);
                $indicadorProductoId = $data["indicador_producto_id"];
                $pdtId = $data["pdt_id"];
                $arrResponse = [
                    "planes" => $this->planesDesarrolloTerritorial(), 
                    "fuentes" => $this->getFuentesEdit($id),
                    "data" => $data,
                    "lineas" => $this->searchLineasEstrategicas($indicadorProductoId),
                    "periodoGobierno" => $this->periodoGobierno($pdtId),
                    "getBpins" => $this->getBpinsEdit($id),
                    "indicadorProductos" => $this->searchIndicadorProductos($pdtId),
                    "consecutivos" => $this->getConsecutivos()
                ];
                echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
            }
            die();
        }

        public function saveEdit() {
            if (!empty($_SESSION)) {
                $id = (int) $_POST["id"];
                $value = [
                    $_POST["pdtId"],
                    $_POST["indicadorProductoId"],
                    $_POST["tipoAcumulacion"],
                    round(floatval($_POST["valorProdServ01"]), 2),
                    round(floatval($_POST["valorProdServ02"]), 2),
                    round(floatval($_POST["valorProdServ03"]), 2),
                    round(floatval($_POST["valorProdServ04"]), 2),
                ];
                $status = $this->updatePlanIndicativo($id, $value);

                if ($status > 0) {
                    $fuentes = json_decode($_POST['fuentes'],true);
                    $bpins = json_decode($_POST['bpins'],true);
                    $this->saveFuentes($id, $fuentes);
                    $this->saveBpins($id, $bpins);
                    $arrResponse = array("status" => true, "msg" => "Plan indicativo actualizado con exito");
                }
                else {
                    $arrResponse = array("status" => false, "msg" => "Error al actualizar, intente de nuevo");
                }

                echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
            }
            die();
        }
    }

    if($_POST){
        $obj = new indicativoController();
        $accion = $_POST["action"];
        
        if ($accion == 'save') {
            $obj->save();
        } elseif($accion == "get") {
            $obj->get();
        } else if ($accion == "getDataPdt") {
            $obj->getDataPdt();
        }else if ($accion == "getEdit") {
            $obj->getEdit();
        } else if ($accion == "saveEdit") {
            $obj->saveEdit();
        } else if ($accion == "getSearch") {
            $obj->getSearch();
        } else if ($accion == "getLinea") {
            $obj->getLinea();
        } else if ($accion == "searchBpin") {
            $obj->checkBpin();
        }
    }
?>