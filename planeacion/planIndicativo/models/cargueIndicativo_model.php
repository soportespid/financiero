<?php
    require_once '../../../Librerias/core/Helpers.php';
    require_once '../../../Librerias/core/Mysql.php';
    session_start();
    
    class indicativoModel extends Mysql{
        private $cedula;
        private $vigencia;

        function __construct(){
            parent::__construct();
            $this->cedula = $_SESSION["cedulausu"];
            $this->vigencia = getVigenciaUsuario($this->cedula);
        }

        public function planesDesarrolloTerritorial() {
            $sql_periodos = "SELECT id AS periodoId FROM para_periodos_gobierno WHERE vigencia_inicial <= '$this->vigencia' AND vigencia_final >= '$this->vigencia' AND estado = 'S'";
            $result = $this->select($sql_periodos);

            $sql = "SELECT id, nombre_pdt FROM plan_pdt WHERE periodo_gobierno_id = $result[periodoId] AND estado = 'S'";
            $data = $this->select_all($sql);
            return $data;
        }

        public function existeIndicadorProducto($indicadorId) {
            $sql = "SELECT COUNT(*) AS cantidad FROM plan_indicador_producto WHERE id = $indicadorId";
            $data = $this->select($sql);
            return $data["cantidad"];
        }

        public function existeBpin($bpin) {
            $sql = "SELECT COUNT(*) AS cantidad FROM ccpproyectospresupuesto WHERE codigo = '$bpin'";
            $value = $this->select($sql);
            return $value["cantidad"];
        }

        public function saveFuentes(array $fuentes) {
            $sql = "INSERT INTO plan_indicativo_det (plan_indicativo_id, fuente_id, valor_vig_01, valor_vig_02, valor_vig_03, valor_vig_04) VALUES (?, ?, ?, ?, ?, ?)";
            $this->insert($sql, $fuentes);
        }

        public function saveBpins(int $indicativoId, array $data) {
            $sql = "INSERT INTO plan_indicativo_bpin (plan_indicativo_id, bpin) VALUES (?, ?)";
            foreach ($data as $bpin) {
                $this->insert($sql, [$indicativoId, $bpin]);
            }
            return true;
        }

        public function savePlanIndicativo($pdtId, $data) {
            $sql = "INSERT INTO plan_indicativo (pdt_id, indicador_producto_id, tipo_acumulativo, valor_meta_vig_01, valor_meta_vig_02, valor_meta_vig_03, valor_meta_vig_04, estado) VALUES (?, ?, ?, ?, ?, ?, ?, ?)";

            foreach ($data as $key => $d) {
                $values = [
                    $pdtId,
                    $d["indicadorProductoId"],
                    $d["tipoAcumulacion"],
                    $d["valor_meta_01"],
                    $d["valor_meta_02"],
                    $d["valor_meta_03"],
                    $d["valor_meta_04"],
                    "S"
                ];
                $id = $this->insert($sql, $values);
                if ($id > 0) {
                    $this->saveBpins($id, $d["bpins"]);

                    for ($i=1; $i <= 15 ; $i++) { 
                        $value_fuente = [
                            $id,
                            $i,
                            $d["fuente_$i"]["valor_01"],
                            $d["fuente_$i"]["valor_02"],
                            $d["fuente_$i"]["valor_03"],
                            $d["fuente_$i"]["valor_04"]
                        ];
                        
                        $this->saveFuentes($value_fuente);                             
                    }
                }
            }
        }

        /* Eliminar datos */
        public function deleteFuentes($indicativoId) {
            $sql_delete = "DELETE FROM plan_indicativo_det WHERE plan_indicativo_id = $indicativoId";
            $this->delete($sql_delete);
        }

        public function deleteBpins($indicativoId) {
            $sql_delete = "DELETE FROM plan_indicativo_bpin WHERE plan_indicativo_id = $indicativoId";
            $this->delete($sql_delete);
        }
    }
?>