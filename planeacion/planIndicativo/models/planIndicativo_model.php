<?php
    require_once '../../../Librerias/core/Helpers.php';
    require_once '../../../Librerias/core/Mysql.php';
    session_start();
    
    class indicativoModel extends Mysql {
        private $cedula;
        private $vigencia;

        function __construct(){
            parent::__construct();
            $this->cedula = $_SESSION["cedulausu"];
            $this->vigencia = getVigenciaUsuario($this->cedula);
        }

        /* Buscar datos */
        public function planesDesarrolloTerritorial() {
            $sql_periodos = "SELECT id AS periodoId FROM para_periodos_gobierno WHERE vigencia_inicial <= '$this->vigencia' AND vigencia_final >= '$this->vigencia' AND estado = 'S'";
            $result = $this->select($sql_periodos);

            $sql = "SELECT id, nombre_pdt FROM plan_pdt WHERE periodo_gobierno_id = $result[periodoId] AND estado = 'S'";
            $data = $this->select_all($sql);
            return $data;
        }

        public function fuentes() {
            $sql = "SELECT id, nombre FROM plan_fuentes_ppi ORDER BY id";
            $fuentes = $this->select_all($sql);

            foreach ($fuentes as $key => $fuente) {
                $fuentes[$key]["valor_01"] = 0;
                $fuentes[$key]["valor_01_format"] = 0;
                $fuentes[$key]["valor_02"] = 0;
                $fuentes[$key]["valor_02_format"] = 0;
                $fuentes[$key]["valor_03"] = 0;
                $fuentes[$key]["valor_03_format"] = 0;
                $fuentes[$key]["valor_04"] = 0;
                $fuentes[$key]["valor_04_format"] = 0;
                $fuentes[$key]["valor_total_fuente"] = 0;
            }            

            return $fuentes;
        }
        
        public function periodoGobierno(int $pdtId) {
            $data = [];
            $sql_pdt = "SELECT PG.vigencia_inicial, PG.vigencia_final FROM plan_pdt AS PDT INNER JOIN para_periodos_gobierno AS PG ON PDT.periodo_gobierno_id = PG.id WHERE PDT.id = $pdtId";
            $periodos = $this->select($sql_pdt);

            $vigenciaIni = $periodos["vigencia_inicial"];
            $vigenciaFin = $periodos["vigencia_final"];

            for ($vigenciaIni; $vigenciaIni <= $vigenciaFin; $vigenciaIni++) { 
                $data[] = (int) $vigenciaIni;
            }
            return $data;
        }

        public function searchIndicadorProductos($pdtId) {
            $sql = "SELECT IP.id, 
            IP.nombre, 
            P.codigo_indicador, 
            P.indicador_producto AS nombre_producto, 
            IP.meta_cuatrienio, 
            CONCAT(T.nombre1, ' ', T.nombre2, ' ', T.apellido1, ' ', T.apellido2) AS nombre_funcionario
            FROM plan_indicador_producto AS IP INNER JOIN ccpetproductos AS P ON IP.codigo_producto_id = P.id 
            INNER JOIN terceros AS T ON IP.tercero_id = T.id_tercero
            WHERE IP.pdt_id = $pdtId AND IP.estado = 'S'
            ORDER BY IP.id";
            $data = $this->select_all($sql);
            foreach ($data as $key => $d) {
                $data[$key]["nombre_funcionario"] = preg_replace("/\s+/", " ", trim($d["nombre_funcionario"]));
            }
            return $data;
        }

        public function searchLineasEstrategicas($id) {
            $sql = "SELECT IPD.eje_estrategico_id AS id,
            LE.nombre
            FROM plan_indicador_producto_det AS IPD 
            INNER JOIN plan_eje_estrategico AS LE ON IPD.eje_estrategico_id = LE.id
            WHERE IPD.indicador_producto_id = $id
            ORDER BY IPD.id";
            $data = $this->select_all($sql);
            $lineas = "";
            foreach ($data as $d) {
                $lineas .= "LE$d[id] - $d[nombre] \n";
            }

            return $lineas;
        }

        public function searchBpin(string $bpin) {
            $sql = "SELECT COUNT(*) AS cantidad FROM ccpproyectospresupuesto WHERE codigo = '$bpin'";
            $data = $this->select($sql);
            return $data["cantidad"];
        }

        public function getConsecutivos() {
            $sql = "SELECT id FROM plan_indicativo WHERE estado = 'S'";
            $data = $this->select_all($sql);
            return $data;
        }

        public function getDataEdit($id) {
            $sql = "SELECT PI.pdt_id,
            PI.indicador_producto_id,
            P.codigo_indicador AS codigo_producto,
            P.indicador_producto AS nombre_producto,
            PIP.nombre AS nombre_personalizado,
            PIP.meta_cuatrienio,
            PI.tipo_acumulativo,
            PI.valor_meta_vig_01,
            PI.valor_meta_vig_02,
            PI.valor_meta_vig_03,
            PI.valor_meta_vig_04 
            FROM plan_indicativo AS PI
            INNER JOIN plan_indicador_producto AS PIP
            ON PI.indicador_producto_id = PIP.id
            INNER JOIN ccpetproductos AS P
            ON PIP.codigo_producto_id = P.id
            WHERE PI.id = $id";
            $data = $this->select($sql);
            return $data;
        }

        public function getFuentesEdit(int $id) {
            $sql = "SELECT PPD.fuente_id AS id, PFP.nombre, PPD.valor_vig_01 AS valor_01, PPD.valor_vig_02 AS valor_02, PPD.valor_vig_03 AS valor_03, PPD.valor_vig_04 AS valor_04 
            FROM plan_indicativo_det AS PPD 
            INNER JOIN plan_fuentes_ppi AS PFP
            ON PPD.fuente_id = PFP.id
            WHERE PPD.plan_indicativo_id = $id
            ORDER BY PPD.fuente_id";
            $fuentes = $this->select_all($sql);
            foreach ($fuentes as $key => $fuente) {
                $fuentes[$key]["valor_01_format"] = $fuente["valor_01"];
                $fuentes[$key]["valor_02_format"] = $fuente["valor_02"];
                $fuentes[$key]["valor_03_format"] = $fuente["valor_03"];
                $fuentes[$key]["valor_04_format"] = $fuente["valor_04"];
                $fuentes[$key]["valor_total_fuente"] = 0;
            }
            return $fuentes;
        }

        public function getBpinsEdit(int $id) {
            $sql = "SELECT bpin FROM plan_indicativo_bpin WHERE plan_indicativo_id = $id";
            $bpins = $this->select_all($sql);
            return $bpins;
        }

        public function getSearchData() {
            $sql = "SELECT PI.id,
            PDT.nombre_pdt,
            PI.indicador_producto_id,
            P.codigo_indicador AS codigo_producto,
            P.indicador_producto AS nombre_producto,
            PIP.nombre AS nombre_personalizado,
            PIP.meta_cuatrienio,
            PI.tipo_acumulativo
            FROM plan_indicativo AS PI
            INNER JOIN plan_pdt AS PDT 
            ON PDT.id = PI.pdt_id
            INNER JOIN plan_indicador_producto AS PIP
            ON PI.indicador_producto_id = PIP.id
            INNER JOIN ccpetproductos AS P
            ON PIP.codigo_producto_id = P.id
            WHERE PI.estado = 'S'
            ORDER BY PI.id";
            $data = $this->select_all($sql);
            return $data;
        }

        /* Crear datos */
        public function savePlanIndicativo($data) {
            $sql = "INSERT INTO plan_indicativo (pdt_id, indicador_producto_id, tipo_acumulativo, valor_meta_vig_01, valor_meta_vig_02, valor_meta_vig_03, valor_meta_vig_04, estado) VALUES (?,?,?,?,?,?,?,?)";
            $id = $this->insert($sql, $data);
            return $id;
        }

        public function saveFuentes($indicativoId, $fuentes) {
            $this->deleteFuentes($indicativoId);
            $sql = "INSERT INTO plan_indicativo_det (plan_indicativo_id, fuente_id, valor_vig_01, valor_vig_02, valor_vig_03, valor_vig_04) VALUES (?, ?, ?, ?, ?, ?)";
            foreach ($fuentes as $fuente) {
                $values = [
                    $indicativoId,
                    $fuente["id"],
                    $fuente["valor_01"],
                    $fuente["valor_02"],
                    $fuente["valor_03"],
                    $fuente["valor_04"]
                ];

                $this->insert($sql, $values);
            }
        }

        public function saveBpins(int $indicativoId, array $data) {
            $this->deleteBpins($indicativoId);

            $sql = "INSERT INTO plan_indicativo_bpin (plan_indicativo_id, bpin) VALUES (?, ?)";
            foreach ($data as $bpin) {
                $this->insert($sql, [$indicativoId, $bpin["bpin"]]);
            }
            return true;
        }
        /* Editar datos */
        public function updatePlanIndicativo($id, $data) {
            $sql = "UPDATE plan_indicativo SET pdt_id = ?, indicador_producto_id = ?, tipo_acumulativo = ?, valor_meta_vig_01 = ?, valor_meta_vig_02 = ?, valor_meta_vig_03 = ?, valor_meta_vig_04 = ? WHERE id = $id";
            $result = $this->update($sql, $data);
            return $result;
        }

        /* Eliminar datos */
        public function deleteFuentes($indicativoId) {
            $sql_delete = "DELETE FROM plan_indicativo_det WHERE plan_indicativo_id = $indicativoId";
            $this->delete($sql_delete);
        }

        public function deleteBpins($indicativoId) {
            $sql_delete = "DELETE FROM plan_indicativo_bpin WHERE plan_indicativo_id = $indicativoId";
            $this->delete($sql_delete);
        }
    }
?>