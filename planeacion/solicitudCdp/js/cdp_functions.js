const URL = "planeacion/solicitudCdp/controllers/cdp_controller.php";
const URLEXCEL = "plan-solicitudCdpExcel.php";
const URLPDF = "solicitudCdpPdf.php";

var app = new Vue({
    el:"#myapp",
    data() {
        return {
            /* general */
            isLoading: false,
            txtSearch: '',
            id: 0,
            /* fechas */
            fechaSolicitud: '',
            fechaVencimiento: '',
            diasASumar: 120,
            fechaMax: '',
            /* area */
            areas: [],
            codArea: '',
            nomArea: '',
            /* datos paa */
            modalPaa: false,
            paa: [], paaCopy: [],
            responsables: [],
            respon: '',
            idPaa: '',
            codPaa: '',
            nomPaa: '',
            modalidad: '',
            objeto: '',
            /* tipo presupuesto */
            tipoPresupuesto: '',
            tipoGastos: [],
            tipoGasto: '',
            modalSectores: false,
            sectores: [], sectoresCopy: [],
            codSector: '',
            nomSector: '',
            /* contrato o acto */
            tipoDocumento: '',
            tipoContrato: '',
            numeroActo: '',
            fechaActo: '',
            /* agregar detalle */
            tipoPresuDet: '',
            secciones: [],
            codDependencia: '',
            nomDependencia: '',
            medioPago: 'CSF',
            vigenciasGastos: [],
            vigGasto: '',
            rubroPresupuestal: '',
            modalRubro: false,
            rubros: [], rubrosCopy: [],
            nomVigGasto: '',
            modalCuentasCcpet: false,
            cuentasccpet: [], cuentasccpetCopy: [],
            cuenta: '',
            modalFuentes: false,
            fuentes: [], fuentesCopy: [],
            codFuente: '',
            /* Funcionamiento */
            modalCpc: false,
            arrayCpc: [], arrayCpcCopy: [],
            cpc: '',
            /* inversion */
            modalPrograma: false,
            programas: [], programasCopy: [],
            codPrograma: '',
            codSubPrograma: '',
            modalProducto: false,
            productos: [], productosCopy: [],
            codProducto: '',
            modalIndicador: false,
            indicadores: [], indicadoresCopy: [],
            codIndicador: '',
            modalBpin: false,
            arrayBpin: [], arrayBpinCopy: [],
            codBpin: '',
            idBpin: '',
            modalDetalleSectorial: false,
            arraySectorial: [], arraySectorialCopy: [],
            tipoCodigoSectorial:"0",
            codigoSectorial:"0",
            codSectorial: '0',
            codGastoSectorial: '0',
            modalPoliticaPublica: false,
            politicasPublicas: [], politicasPublicasCopy: [],
            codPoliticaPublica: '1',
            /* valores */
            saldo: '',
            valor: '',
            valorFormat: '',
            valorTotalDet: 0,

            valorDisponibilidad: '',
            valorDisponibilidadFormat: '',
            /* agregar */
            arrayDetalles: [],

            //buscar
            dataSearch: [], dataSearchCopy: [],

            //visualizar
            arrConsecutivos: [],
            consecutivo: '',
            estado: '',

            //Variables agregadas por Ricardo
            isModalRubro:false,
            selectSector:0,
            selectPrograma:0,
            selectProducto:0,
            selectSectorData:0,
            selectProgramaData:0,
            selectProductoData:0,
            arrBpim:[],
            arrProgramas:[],
            arrSectores:[],
            arrProductos:[],
            arrProgramasData:[],
            arrSectoresData:[],
            arrProductosData:[],
            arrSectorialGasto:[],
            arrSectorialGastoCopy:[],
            arrBpimCopy:[],
            modalDetalleSectorialGasto:false,
            bpimSeleccionado: '',
            nombreBpinSeleccionado: '',
        }
    },
    watch: {
        codFuente(newVal, oldVal) {
            this.calcularSaldo();
        }
    },
    mounted() {
        this.intPageVal = this.$refs.pageType.value;

        if (this.intPageVal == 1) {
            this.get();
            //Funcion agregadas por Ricardo
            this.getData();
        } else if (this.intPageVal == 2) {
            this.getSearch();
        } else if (this.intPageVal == 3) {
            this.getVisualizar();
        }

        
    },
    methods: {

        /* metodos para traer información del Proyecto */

        getData: async function(){
            const formData = new FormData();
            formData.append("action","get");
            this.isLoading = true;
            const response = await fetch('presupuesto_ccpet/procesos_gastos/controllers/RubroController.php',{method:"POST",body:formData});
            const objData = await response.json();
            this.isLoading = false;
            this.arrBpim = objData.bpim;
            this.arrSectores = objData.sectores;
            this.arrSectoresData = objData.sectores;
            this.arrFuentesCopy = objData.fuentes;
            this.arrBpimCopy = objData.bpim;
            this.arrData = objData.proyectos;
            this.arrDataCopy = objData.proyectos
            this.txtResultadosBpim = this.arrBpim.length;
        },

        filter:function(type=""){
            if(type=="sector" && this.selectSector != 0){
                this.arrProductos = [];
                this.arrBpimCopy = [...this.arrBpim.filter((e)=>{return e.sector==app.selectSector})];
                this.arrProgramas = Array.from(new Set(
                    app.arrBpimCopy.map(e=>{return JSON.stringify({codigo:e.programa,nombre:e.nombre_programa})})
                )).map(item => JSON.parse(item));
            }else if(type=="programa" && this.selectPrograma != 0){
                this.arrBpimCopy = [...this.arrBpim.filter((e)=>{return e.sector==app.selectSector && e.programa==app.selectPrograma})];
                this.arrProductos = Array.from(new Set(
                    app.arrBpimCopy.map(e=>{return JSON.stringify({codigo:e.producto,nombre:e.nombre_producto})})
                )).map(item => JSON.parse(item));
            }else if(type=="producto" && this.selectProducto != 0){
                this.arrBpimCopy = [...this.arrBpim.filter((e)=>{
                    return e.sector==app.selectSector && e.programa==app.selectPrograma && e.producto==app.selectProducto}
                )];
            }else{
                this.arrBpimCopy = [...this.arrBpim];
            }
        },

        /* metodos para traer información */
        async get() {
            const formData = new FormData();
            formData.append("action","get");
            this.isLoading = true;
            const response = await fetch(URL,{method:"POST",body:formData});
            const objData = await response.json();
            this.fechaMax = objData.fechaMax;
            this.paa = this.paaCopy = objData.paa;
            this.responsables = objData.responsables;
            this.areas = objData.areas;
            this.tipoGastos = objData.tipoGastos;
            this.sectores = this.sectoresCopy = objData.sectores;
            this.secciones = objData.secciones;
            this.codDependencia = this.secciones[0];
            this.vigenciasGastos = objData.vigGasto;
            this.vigGasto = this.vigenciasGastos[0];
            this.fuentes = this.fuentesCopy = objData.fuentes;
            this.arraySectorial = this.arraySectorialCopy = objData.detalleSectorial;
            this.arrSectorialGasto = objData.sectorial_gasto;
            this.arrSectorialGastoCopy = objData.sectorial_gasto;
            this.politicasPublicas = this.politicasPublicasCopy = objData.politicasPublicas;
            this.isLoading = false;
        },

        async getSearch() {
            const formData = new FormData();
            formData.append("action","getSearch");
            this.isLoading = true;
            const response = await fetch(URL,{method:"POST",body:formData});
            const objData = await response.json();
            this.dataSearch = this.dataSearchCopy = objData.data;
            this.isLoading = false;
        },

        async getVisualizar() {
            this.id = new URLSearchParams(window.location.search).get('id');
            const formData = new FormData();
            formData.append("action","visualizar");
            formData.append("id",this.id);
            const response = await fetch(URL,{method:"POST",body:formData});
            const objData = await response.json();
            this.arrConsecutivos = objData.consecutivos;
            this.consecutivo = objData.cabecera.consecutivo;
            this.fechaSolicitud = objData.cabecera.fecha_solicitud;
            this.fechaVencimiento = objData.cabecera.fecha_vencimiento;
            this.codPaa = objData.cabecera.codigo_plan_compras;
            this.modalidad = objData.cabecera.nombre_modalidad;
            this.nomPaa = objData.cabecera.descripcion_plan_compras;
            this.codArea = objData.cabecera.id_area_solicitante;
            this.nomArea = objData.cabecera.nombrearea;
            this.tipoPresupuesto = objData.cabecera.tipo_presupuesto;
            this.codSector = objData.cabecera.cod_sector;
            this.nomSector = objData.cabecera.nombre_sector;
            this.tipoDocumento = objData.cabecera.tipo_documento;
            this.tipoContrato = objData.cabecera.tipo_contrato;
            this.numeroActo = objData.cabecera.numero_acto;
            this.fechaActo = objData.cabecera.fecha_acto;
            this.objeto = objData.cabecera.objeto;
            this.estado = objData.cabecera.estado;
            this.arrayDetalles = objData.detalle;
            this.valorTotalDet = objData.total;
            this.tipoGasto = objData.cabecera.tipo_gasto;
        },

        /* metodos para procesar información */
        
        sumaDias() {
            const fecha = new Date(this.fechaSolicitud);
            fecha.setDate(fecha.getDate() + this.diasASumar);
            this.fechaVencimiento = fecha.toISOString().split("T")[0];
        },

        clearTipoPresupuesto() {
            this.tipoGasto = this.codSector = this.nomSector = "";
        },

        clearTipoDocumento() {
            this.tipoContrato = this.numeroActo = this.fechaActo = "";
        },

        clearCuentas() {
            this.cuentasccpet = this.cuentasccpetCopy = [];
            this.cuenta = this.cpc = "";
        },

        selectProyecto: function(data) {

            this.codSector = '';
            this.codPrograma = '';
            this.codProducto = '';
            this.codIndicador = '';
            this.codBpin = '';
            this.bpimSeleccionado = '';
            this.nombreBpinSeleccionado = '';
            this.codFuente = '';
            this.idBpin = '';


            this.codSector = data.sector;
            this.codPrograma = data.programa;
            this.codProducto = data.producto;
            this.codIndicador = data.indicador;
            this.codBpin = data.codigo;
            this.bpimSeleccionado = data.codigo;
            this.nombreBpinSeleccionado = data.nombre;
            this.codFuente = data.fuente;
            this.nomSector = data.nombre_sector;
            this.idBpin = data.id;
            this.isModalRubro = false;
            this.calcularSaldo();
        },

        limpiarProyecto: function() {
            /* this.codSector = '';
            this.codPrograma = '';
            this.codProducto = '';
            this.codIndicador = '';
            this.codBpin = '';
            this.bpimSeleccionado = '';
            this.nombreBpinSeleccionado = '';
            this.codFuente = '';
            this.idBpin = ''; */
            this.isModalRubro = false;
        },

        async deployModal(option) {
            const formData = new FormData();

            switch (option) {
                case "cuentaccpet":
                    if (this.tipoPresupuesto == "") {
                        Swal.fire("Atención!","Debe seleccionar primero el tipo de presupuesto","warning");
                        return false;
                    }

                    if (this.tipoPresupuesto == 1 && this.tipoGasto == "") {
                        Swal.fire("Atención!","Debe seleccionar primero el tipo de gasto","warning");
                        return false;
                    }
                    this.isLoading = true;
                    formData.append("action","cuentasccpet");
                    if (this.tipoPresupuesto == 1) {
                        formData.append("tipoPresupuesto",this.tipoPresupuesto);
                        formData.append("tipoGasto",this.tipoGasto);
                    } else if (this.tipoPresupuesto == 2){
                        formData.append("tipoPresupuesto",this.tipoPresupuesto);
                    } else {
                        formData.append("tipoPresupuesto",this.tipoPresuDet);
                    }

                    break;

                case "rubroPresupuestal":
                    if (this.codDependencia == "" || (this.tipoGasto == "" && this.tipoPresupuesto == "1")) {
                        Swal.fire("Atención!","Por favor, selecciona una dependencia y un tipo de gasto para continuar","warning");
                        return false;
                    }
                    if (this.codArea == "" || this.medioPago == "" || this.vigGasto == "") {
                        Swal.fire("Atención!","Por favor, selecciona un area, medio de pago y una vigencia de gasto para continuar","warning");
                        return false;
                    }

                    formData.append("action","rubroPresupuestal");
                    formData.append("dependencia",this.codDependencia.codigo);
                    formData.append("tipoGasto", this.tipoGasto);
                    formData.append("area", this.codArea);
                    formData.append("medioPago", this.medioPago);
                    formData.append("vigGasto", this.vigGasto.codigo);
                    break;
 
                case "cpc":
                    if (this.cuenta == "") {
                        Swal.fire("Atención!","Debe seleccionar primero una cuenta ccpet","warning");
                        return false;
                    }
                    this.isLoading = true;
                    formData.append("action","cpc");
                    formData.append("cuenta",this.cuenta);
                    break; 

                default:
                    console.log("Error, opción no encontrada");
                    break;
            }

            const response = await fetch(URL,{method:"POST",body:formData});
            const objData = await response.json();
            this.isLoading = false;
            if (option == "cuentaccpet") {
                this.cuentasccpet = this.cuentasccpetCopy = objData.cuentas;
                this.modalCuentasCcpet = true;
            } else if (option == "cpc") {
                this.arrayCpc = this.arrayCpcCopy = objData.cpc;
                this.modalCpc = true;
            } else if (option == "rubroPresupuestal") {
                this.rubros = this.rubrosCopy = objData.data;
                this.modalRubro = true;
            }
        },

        async searchData(option) {
            let search = "";

            switch (option) {
                case "paa":
                    if (this.respon == "") {
                        search = this.txtSearch.toLowerCase();
                        this.paa = [...this.paaCopy.filter(e=>e.codigo.toLowerCase().includes(search) || e.descripcion.toLowerCase().includes(search))];
                    } else {
                        search = this.txtSearch.toLowerCase();
                        this.paa = [...this.paa.filter(e=>e.codigo.toLowerCase().includes(search) || e.descripcion.toLowerCase().includes(search))];
                    }
                    
                    break;

                case "modalccpet":
                    search = this.txtSearch.toLowerCase();
                    this.cuentasccpet = [...this.cuentasccpetCopy.filter(e=>e.codigo.toLowerCase().includes(search) || e.nombre.toLowerCase().includes(search))];
                    break;

                case "modalFuente":
                    search = this.txtSearch.toLowerCase();
                    this.fuentes = [...this.fuentesCopy.filter(e=>e.codigo.toLowerCase().includes(search) || e.nombre.toLowerCase().includes(search))];
                    break;

                case "modalCpc":
                    search = this.txtSearch.toLowerCase();
                    this.arrayCpc = [...this.arrayCpcCopy.filter(e=>e.codigo.toLowerCase().includes(search) || e.nombre.toLowerCase().includes(search))];
                    break;

                case "modalDetSectorial":
                    search = this.txtSearch.toLowerCase();
                    this.arraySectorial = [...this.arraySectorialCopy.filter(e=>e.codigo.toLowerCase().includes(search) || e.nombre.toLowerCase().includes(search))];
                    break;
                case "modalDetSectorialGasto":
                    search = this.txtSearch.toLowerCase();
                    this.arrSectorialGasto = [...this.arrSectorialGastoCopy.filter(e=>e.codigo.toLowerCase().includes(search) || e.nombre.toLowerCase().includes(search))];
                    break;
                case "modalPolitica":
                    search = this.txtSearch.toLowerCase();
                    this.politicasPublicas = [...this.politicasPublicasCopy.filter(e=>e.codigo.toLowerCase().includes(search) || e.nombre.toLowerCase().includes(search))];
                    break;

                case "inputSearch":
                    search = this.txtSearch.toLowerCase();
                    this.dataSearch = [...this.dataSearchCopy.filter(e=>e.consecutivo.toLowerCase().includes(search) || e.nombre_area.toLowerCase().includes(search) || e.objeto.toLowerCase().includes(search))];
                    break;
        
                case "modalProyectos":
                    search = this.txtSearch.toLowerCase();
                    this.arrBpimCopy = [...this.arrBpim.filter(e=>e.codigo.toLowerCase().includes(search) || e.nombre.toLowerCase().includes(search) || e.fuente.toLowerCase().includes(search))];
                    break;
                
                case "modalRubro":
                    search = this.txtSearch.toLowerCase();
                    this.rubros = [...this.rubrosCopy.filter(e=>e.cuenta.toLowerCase().includes(search) || e.cpc.toLowerCase().includes(search) || e.fuente.toLowerCase().includes(search) || e.nombre_cuenta.toLowerCase().includes(search) || e.nombre_cpc.toLowerCase().includes(search) || e.nombre_fuente.toLowerCase().includes(search))];
                    break;
                default:
                    console.log("Error, opción no encontrada");
                    break;
            }    
        },

        filtroResponsable() {
            if (this.respon == "") {
                this.paa = this.paaCopy;
                this.isLoading = false;
                return false;
            }
            let search = "";
            search = this.respon.toLowerCase();
            this.paa = [...this.paaCopy.filter(e=>e.responsable.toLowerCase().includes(search))];
        },

        selectItem(option, item) {
            switch (option) {
                case "paa":
                    this.idPaa = item.id;
                    this.codPaa = item.codigo;
                    this.nomPaa = item.descripcion;
                    this.modalidad = item.nombre_modalidad;
                    this.modalPaa = false;
                    this.paa = this.paaCopy;
                    this.objeto =item.descripcion
                    break;

                case "modalRubro":
                    // codFuente
                    // cuenta
                    // cpc
                    this.codFuente = item.fuente;
                    this.cuenta = item.cuenta;
                    this.cpc = item.cpc;
                    this.rubros = this.rubrosCopy;
                    this.txtSearch = "";
                    this.modalRubro = false;
                    break;
                
                case "modalccpet":
                    this.cuenta = item.codigo;
                    this.modalCuentasCcpet = false;
                    this.cuentasccpet = this.cuentasccpetCopy;
                    break;
                
                case "modalFuente":
                    this.codFuente = item.codigo;
                    this.modalFuentes = false;
                    this.fuentes = this.fuentesCopy;
                    break;

                case "modalCpc":
                    this.cpc = item.codigo;
                    this.modalCpc = false;
                    this.arrayCpc = this.arrayCpcCopy;
                    break;

                case "modalDetSectorial":
                    if((app.codSector == "19" || app.codSector == "22") && item.codigo == 0){
                        Swal.fire("Atención", "El detalle sectorial debe ser distinto a 0","warning");
                        return false;
                    }
                    this.codSectorial = item.id;
                    this.codigoSectorial = item.codigo;
                    this.modalDetalleSectorial = false;
                    this.arraySectorial = this.arraySectorialCopy;
                    break;
                case "modalDetSectorialGasto":   
                    if (app.codSector == "19") {
                        if(item.codigo == 0){
                            Swal.fire("Atención", "El sectorial gasto debe ser distinto a 0","warning");
                            return false;
                        }
                    }
                    
                    this.codGastoSectorial = item.codigo;
                    this.modalDetalleSectorialGasto = false;
                    this.arrSectorialGasto = this.arrSectorialGastoCopy;
                    break;
                case "modalPolitica":
                    this.codPoliticaPublica = item.codigo;
                    this.modalPoliticaPublica = false;
                    this.politicasPublicas = this.politicasPublicasCopy;
                    break;

                default:
                    console.log("Error, opción no encontrada");
                    break;
            }
            this.txtSearch = "";
            if(this.tipoPresupuesto != 2)
                this.calcularSaldo();
        },

        async calcularSaldo() {
            const formData = new FormData();

            if (this.tipoPresupuesto == 1 || this.tipoPresuDet == 1) {
                if (this.codDependencia.codigo == "" || this.medioPago == "" || this.vigGasto.codigo == "" || this.cuenta == "" || this.codFuente == "") {
                    return false;
                }
                
                formData.append("action","saldo");
                formData.append("tipoPresupuesto", 1);
                formData.append("codDependencia",this.codDependencia.codigo);
                formData.append("medioPago",this.medioPago);
                formData.append("vigGasto",this.vigGasto.codigo);
                formData.append("cuenta",this.cuenta);
                formData.append("codFuente",this.codFuente);
                formData.append("area",this.codArea);
                formData.append("cpc",this.cpc);

            } 
            else if (this.tipoPresupuesto == 2 || this.tipoPresuDet == 2) {
                if (this.codDependencia.codigo == "" || this.medioPago == "" || this.vigGasto.codigo == "" || this.codFuente == "" || this.codIndicador == "" || this.codBpin == "") {
                    return false;
                }

                formData.append("action","saldo");
                formData.append("tipoPresupuesto", 3);
                formData.append("codDependencia",this.codDependencia.codigo);
                formData.append("medioPago",this.medioPago);
                formData.append("vigGasto",this.vigGasto.codigo);
                formData.append("cuenta",this.cuenta);
                formData.append("codFuente",this.codFuente);
                formData.append("indicador",this.codIndicador);
                formData.append("bpin",this.codBpin);
                formData.append("area",this.codArea);
            }

            const response = await fetch(URL,{method:"POST",body:formData});
            const objData = await response.json();
            this.saldo = objData.saldo;
        },

        addDet() {
            if (this.tipoPresupuesto == "") {
                return false;
            }

            if(this.cuenta == ""){
                Swal.fire("Atención!","Falta incluir valor CCPET. ","warning");
                return false;
            }

            if (this.tipoPresupuesto == 1 || this.tipoPresuDet == 1) {
                /* dependencia, medio de pago, vig de gasto, cuenta, fuente, cpc, saldo y valor */
                if (this.codDependencia.codigo == "" || this.medioPago == "" || this.vigGasto.codigo == "" || this.cuenta == "" || this.codFuente == "" || this.valor == "") {
                    Swal.fire("Atención!","Todos los campos con (*) son obligatorios","warning");
                    return false;
                }
            } 
            else if (this.tipoPresupuesto == 2 || this.tipoPresuDet == 2) {
                if (this.codDependencia.codigo == "" || this.medioPago == "" || this.vigGasto.codigo == "" || this.codFuente == "" || this.valor == "" || this.codIndicador == "" || this.codBpin == "" || this.codSectorial == "" || this.codPoliticaPublica == "" || this.cuenta == "") {
                    Swal.fire("Atención!","Todos los campos con (*) son obligatorios","warning");
                    return false;
                }
                if(app.codSector == "22" || app.codSector == "19"){
                    if(app.codSectorial == ""){
                        Swal.fire("Atención!","Debe seleccionar el detalle sectorial","warning");
                        return false;
                    }
                    if(app.codSectorial == "0"){
                        Swal.fire("Atención!","El detalle sectorial debe ser distinto a 0","warning");
                        return false;
                    }
                    if(app.codSector == "19" && app.codGastoSectorial == ""){
                        Swal.fire("Atención!","Debe seleccionar el sectorial gasto","warning");
                        return false;
                    }
                    if(app.codSector == "19" && app.codGastoSectorial == "0"){
                        Swal.fire("Atención!","El sectorial gasto debe ser distinto a 0","warning");
                        return false;
                    }
                }
            } 
            
            if (parseFloat(this.saldo) < parseFloat(this.valor)) {
                Swal.fire("Atención!","El valor no puede ser mayor al saldo","warning");
                return false;
            }
            
            let dataTemp = [];

            if (this.tipoPresupuesto == 1 || this.tipoPresuDet == 1) {
                this.codBpin = this.codIndicador = "";
            }

            dataTemp = ({
                tipoPresupuesto: this.tipoPresupuesto,
                codDependencia: this.codDependencia.codigo,
                nameDependencia: this.codDependencia.nombre,
                medioPago: this.medioPago,
                vigGasto: this.vigGasto.codigo,
                nameVigGasto: this.vigGasto.nombre,
                cuenta: this.cuenta,
                codFuente: this.codFuente,
                cpc: this.cpc,
                codIndicador: this.codIndicador,
                bpin: this.codBpin,
                idBpin: this.idBpin,
                codSectorial: this.codigoSectorial,
                idSectorial:this.codSectorial,
                codGastoSectorial:this.codGastoSectorial,
                codPoliticaPublica: this.codPoliticaPublica,
                valor: this.valor
            });
            // this.valorTotalDet = parseFloat(this.valorTotalDet) + parseFloat(this.valor); 
            this.arrayDetalles.push(dataTemp);
            this.valorTotalDet = this.arrayDetalles.reduce((acc, item) => acc + parseFloat(item.valor), 0);
            this.cuenta = '';
            this.saldo = this.saldo - this.valor;
            this.valor = "";
            this.valorFormat = "";
            this.codGastoSectorial = "0";
            if(app.codSector == "19" || app.codSector =="22"){
                this.codigoSectorial ="";
                if(app.codSector =="19"){
                    codGastoSectorial="";
                }
            }
            //this.calcularSaldo();
            //this.saldo
            /* this.tipoPresuDet = this.cuenta = this.codFuente = this.cpc = this.codPrograma = this.codSubPrograma = this.codProducto = this.codIndicador = this.codBpin = this.idBpin = this.codSectorial = this.codPoliticaPublica = this.saldo = this.valorFormat = this.valor = ""; */
        },

        delDet(index) {
            // this.valorTotalDet = this.valorTotalDet - this.arrayDetalles[index].valor;
            this.arrayDetalles.splice(index, 1);
            this.valorTotalDet = this.arrayDetalles.reduce((acc, item) => acc + parseFloat(item.valor), 0);
        },

        redondearACeroDecimales(numero) {
            return Math.round(numero);
        },
        
        /* Metodos para guardar o actualizar información */
        async save() {
            /* validacion cabecera */
            if (this.fechaSolicitud < this.fechaMax) {
                Swal.fire("Atención!","La fecha de solicitud debe ser igual o mayor a la ultima solicitud de CDP creada","warning");
                return false;
            }

            let valorCab = this.redondearACeroDecimales(parseFloat(this.valorDisponibilidad));
            let valorDet = this.redondearACeroDecimales(parseFloat(this.valorTotalDet));

            if(valorCab == 0 || valorCab != valorDet){
                Swal.fire("Atención!","El valor total de la solicitud debe ser igual al valor de los detalles ","warning");
                return false;
            }

            if (this.idPaa === "") {
                Swal.fire("Atención!","Por favor selecciona un código de plan anual de adquisiciones","warning");
                return false;
            }

            if (this.idPaa == 0 && this.tipoDocumento != 2) {
                Swal.fire("Atención!","Si el plan de compras no es aplicable para esta solicitud, selecciona el acto administrativo correspondiente.","warning");
                return false;
            }

            if (this.fechaSolicitud == "" || this.fechaVencimiento == "" || this.codArea == "" || this.tipoPresupuesto == "" || this.tipoDocumento == "" || this.objeto == "") {
                Swal.fire("Atención!","Todos los campos con (*) son obligatorios","warning");
                return false;
            }

            if (this.tipoPresupuesto == 1 && this.tipoGasto == "") {
                Swal.fire("Atención!","Todos los campos con (*) son obligatorios","warning");
                return false;
            } else if (this.tipoPresupuesto == 2 && this.codSector == "" ){
                Swal.fire("Atención!","Todos los campos con (*) son obligatorios","warning");
                return false;
            }

            if (this.tipoDocumento == 1 && this.tipoContrato == "") {
                Swal.fire("Atención!","Todos los campos con (*) son obligatorios","warning");
                return false;
            } else if (this.tipoDocumento == 2 && (this.numeroActo == "" || this.fechaActo == "")) {
                Swal.fire("Atención!","Todos los campos con (*) son obligatorios","warning");
                return false;
            }

            if (this.arrayDetalles.length == 0) {
                Swal.fire("Atención!","Debes agregar minimo un detalle para guardar","warning");
                return false;
            }
            const formData = new FormData();
            formData.append("action","save");
            formData.append("fechaSolicitud",this.fechaSolicitud);
            formData.append("fechaVencimiento",this.fechaVencimiento);
            formData.append("idPaa",this.idPaa);
            formData.append("codArea",this.codArea);
            formData.append("tipoPresupuesto",this.tipoPresupuesto);
            formData.append("tipoDocumento",this.tipoDocumento);
            formData.append("objeto",this.objeto);
            formData.append("tipoGasto",this.tipoGasto);
            formData.append("codSector",this.codSector);
            formData.append("tipoContrato",this.tipoContrato);
            formData.append("numeroActo",this.numeroActo);
            formData.append("fechaActo",this.fechaActo);
            formData.append("detalles", JSON.stringify(this.arrayDetalles));

            Swal.fire({
                title:"¿Estás segur@ de guardar?",
                text:"",
                icon: 'warning',
                showCancelButton:true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText:"Sí, guardar",
                cancelButtonText:"No, cancelar"
            }).then(async function(result) {
                if(result.isConfirmed) {
                    const response = await fetch(URL,{method:"POST",body:formData});
                    const objData = await response.json();

                    if (objData.status) {
                        Swal.fire("Guardado",objData.msg,"success");
                        if(objData.id){
                            setTimeout(function(){
                                // window.location.reload();
                                window.location.href="plan-visualizarSolicitudCdp"+'?id='+objData.id;
                            },1500);
                        }
                    }
                    else {
                        Swal.fire("Error!",objData.msg,"warning");
                    }
                }
            });
        },

        async anulaSolicitud(id, index) {
            const formData = new FormData();
            formData.append("action","anula");
            formData.append("id",id);

            Swal.fire({
                title:"¿Estás segur@ de anular?",
                text:"",
                icon: 'warning',
                showCancelButton:true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText:"Sí, anular",
                cancelButtonText:"No, cancelar"
            }).then(async function(result) {
                if(result.isConfirmed) {
                    const response = await fetch(URL,{method:"POST",body:formData});
                    const objData = await response.json();

                    if (objData.status) {
                        app.dataSearchCopy[index].estado = 'N';
                        app.dataSearch[index].estado = 'N';
                        Swal.fire("Guardado",objData.msg,"success");
                    }
                    else {
                        Swal.fire("Error!",objData.msg,"warning");
                    }
                }
            });
        },

        /* Formatos para mostrar información */
        nextItem:function(type){
            let id = this.id;
            let index = this.arrConsecutivos.findIndex(function(e){return e.id == id});
            if(type=="next" && this.arrConsecutivos[++index]){
                id = this.arrConsecutivos[index++].id;
            }else if(type=="prev" && this.arrConsecutivos[--index]){
                id = this.arrConsecutivos[index--].id;
            }
            window.location.href="plan-visualizarSolicitudCdp"+'?id='+id;
        },

        formatInputNumber(conFormato, sinFormato) {
            this[conFormato] = this.formatNumber(this[conFormato]);
            this[sinFormato] = this[conFormato].replace(/[^0-9.]/g, "");
        },

        formatNumber(number) {
            if (!number) return ""; // Si no hay entrada, retorna vacío
    
            // Elimina caracteres no válidos (excepto números y el punto decimal)
            number = number.replace(/[^0-9.]/g, "");
    
            // Divide la parte entera y decimal
            const [integerPart, decimalPart] = number.split(".");
    
            // Formatear la parte entera con separador de miles
            const formattedInteger = integerPart.replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    
            // Si hay parte decimal, la une; si no, devuelve solo la parte entera
            return decimalPart !== undefined
                ? `${formattedInteger}.${decimalPart.slice(0, 2)}` // Limita a 2 decimales si es necesario
                : formattedInteger;
        },


        viewFormatNumber: function(valor){
            return new Intl.NumberFormat("en-US", {style: "currency", currency: "USD"}).format(valor);  
        },

        formatFecha: function(fecha) {
            // Separar la fecha en componentes
            const parts = fecha.split('-');
            if (parts.length !== 3) {
                throw new Error('Formato de fecha inválido. Use Y-m-d.');
            }
            const year = parts[0];
            const month = parts[1];
            const day = parts[2];
        
            // Retornar la fecha en el formato deseado
            return `${day}/${month}/${year}`;
        },

        /* enviar informacion excel o pdf */
        excel: function() {
            const form = document.createElement("form");
            form.method = "post";
            form.target = "_blank";
            form.action = URLEXCEL;       
            
            function addField(name,value){
                const input = document.createElement("input");
                input.type="hidden";
                input.name=name;
                input.value = value;
                form.appendChild(input);
            }

            addField("data", JSON.stringify(this.dataSearch));
        
            document.body.appendChild(form);
            form.submit();
            document.body.removeChild(form);
        },

        pdf: function() {
            const form = document.createElement("form");
            form.method = "post";
            form.target = "_blank";
            form.action = URLPDF;       
            
            function addField(name,value){
                const input = document.createElement("input");
                input.type="hidden";
                input.name=name;
                input.value = value;
                form.appendChild(input);
            }

            addField("consecutivo", this.consecutivo);
            addField("id", this.id);
            addField("tipoPresupuesto", this.tipoPresupuesto);
            addField("tipoGasto", this.tipoGasto);
            addField("areaSolicitante", this.nomArea);
            addField("codArea", this.codArea);
            addField("fechaSolicitud", this.fechaSolicitud);
            addField("fechaVencimiento", this.fechaVencimiento);
            addField("codPaa", this.codPaa);
            addField("modalidad", this.modalidad);
            addField("valorTotal", this.valorTotalDet);
            addField("objeto", this.objeto);
            addField("codSector", this.codSector);
            addField("nombreSector", this.nomSector);
            addField("tipoDocumento", this.tipoDocumento);
            addField("tipoContrato", this.tipoContrato);
            addField("numeroActo", this.numeroActo);
            addField("fechaActo", this.fechaActo);
            addField("estado", this.estado);
            addField("data", JSON.stringify(this.arrayDetalles));
            
            document.body.appendChild(form);
            form.submit();
            document.body.removeChild(form);
        },
    },
    computed:{

    }
})
