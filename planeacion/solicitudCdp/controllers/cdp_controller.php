<?php
    require_once '../../../Librerias/core/Helpers.php';
    require_once '../models/cdp_model.php';
    session_start();
    header('Content-Type: application/json');

    class cdpController extends cdpModel {
        /* crear */
        public function get() {
            if (!empty($_SESSION)) {
                $arrResponse = array(
                    "paa" => $this->get_plan_compras(),
                    "responsables" => $this->get_responsables_paa(),
                    "fechaMax" => $this->get_fecha_max(),
                    "areas" => $this->get_areas(),
                    "tipoGastos" => $this->get_tipos_gastos(),
                    "secciones" => $this->get_secciones_ppto(),
                    "vigGasto" => $this->get_vigencias_gasto(),
                    "fuentes" => $this->get_fuentes_ccpet(),
                    "detalleSectorial" => $this->selectCodigosSectoriales(),
                    "politicasPublicas" => $this->get_politicas_publicas(),
                    "sectorial_gasto"=>$this->get_detalles_sectorial()
                );
                echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);    
            }
            die();
        }

        public function cuentasccpet() {
            if (!empty($_SESSION)) {
                $tipoPresupuesto = $_POST["tipoPresupuesto"];
                if ($tipoPresupuesto == 1) {
                    $tipoGasto = $_POST["tipoGasto"] != "" ? $_POST["tipoGasto"] : 0;
                    $arrResponse = array("cuentas" => $this->get_cuentas_ccpet_funcionamiento($tipoGasto));
                } else {
                    $arrResponse = array("cuentas" => $this->get_cuentas_ccpet_inversion());
                }
                echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
            }
            die();
        }

        public function rubroPresupuestal() {
            if (!empty($_SESSION)) {
                $dependencia = $_POST["dependencia"];
                $tipoGasto = $_POST["tipoGasto"];
                $area = $_POST["area"];
                $medioPago = $_POST["medioPago"];
                $vigGasto = $_POST["vigGasto"];
                $request = $this->get_rubros_presupuestales_funcionamiento($dependencia, $area, $medioPago, $vigGasto, $tipoGasto);
                $arrResponse = array("data" => $request);
                echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
            }
            die();
        }

        public function cpc() {
            if (!empty($_SESSION)) {
                $cuenta = $_POST["cuenta"];
                $arrResponse = array("cpc" => $this->get_cpc($cuenta));
                echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
            }
            die();
        }

        public function saldo() {
            if (!empty($_SESSION))  {
                if($_POST["tipoPresupuesto"] == 1) {
                    $data = [
                        'cuenta' => $_POST['cuenta'],
                        'fuente' => $_POST['codFuente'],
                        'tipo_presupuesto' => $_POST['tipoPresupuesto'],
                        'dependencia' => $_POST['codDependencia'],
                        'medio_pago' => $_POST['medioPago'],
                        'vig_gasto' => $_POST['vigGasto'],
                        'area' => $_POST['area'],
                        'cpc' => $_POST['cpc'],
                        'bpin' => "",
                        'indicador' => "",
                        'vigencia' => ""
                    ];
                } else if ($_POST["tipoPresupuesto"] == 3) {
                    $data = [
                        'cuenta' => $_POST['cuenta'],
                        'fuente' => $_POST['codFuente'],
                        'tipo_presupuesto' => $_POST['tipoPresupuesto'],
                        'dependencia' => $_POST['codDependencia'],
                        'medio_pago' => $_POST['medioPago'],
                        'vig_gasto' => $_POST['vigGasto'],
                        'bpin' => $_POST['bpin'],
                        'indicador' => $_POST['indicador'],
                        'area' => $_POST['area'],
                        'vigencia' => ""
                    ];
                }
                $arrResponse = array("saldo" => $this->search_saldo($data));
                echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
            }
            die();
        }

        public function save() {
            if (!empty($_SESSION)) {
                $dataCab = [
                    "fechaSolicitud" => $_POST["fechaSolicitud"],
                    "fechaVencimiento" => $_POST["fechaVencimiento"],
                    "codAreaSolicitante" => $_POST["codArea"],
                    "idPaa" => $_POST["idPaa"],
                    "tipoPresupuesto" => $_POST["tipoPresupuesto"],
                    "tipoGasto" => $_POST["tipoGasto"],
                    "codSector" => $_POST["codSector"],
                    "tipoDocumento" => $_POST["tipoDocumento"],
                    "tipoContrato" => $_POST["tipoContrato"],
                    "numeroActo" => $_POST["numeroActo"],
                    "fechaActo" => $_POST["fechaActo"],
                    "objeto" => strClean(replaceChar($_POST["objeto"]))
                ];
                $resp = $this->create_cab($dataCab);
                if ($resp > 0) {
                    $detalles = json_decode($_POST['detalles'],true);
                    $this->create_det($resp, $detalles);
                    $arrResponse = array("status" => true, "msg" => "Guardado exitoso", "id" => $resp);
                } else {
                    $arrResponse = array("status" => false, "msg" => "Error en guardado");
                }
                echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
            }
            die();
        }
        /* buscar */
        public function getSearch() {
            if (!empty($_SESSION)) {
                $arrResponse = array(
                    "data" => $this->search_data()
                );
                echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);    
            }
            die();
        }

        public function anula() {
            if (!empty($_SESSION)) {
                $id = $_POST["id"];
                $resp = $this->anula_solicitud($id);
                if ($resp > 0) {
                    $arrResponse = array("status" => true, "msg" => "Anulado con exito");
                } else {
                    $arrResponse = array("status" => false, "msg" => "Error al anular");
                }
                echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);    
            }
            die();
        }

        /* editar */
        public function visualizar() {
            if (!empty($_SESSION))  {
                $id = $_POST["id"];
                $dataCab = $this->get_data_cab($id);
                $dataDet = $this->get_data_det($id);
                $valorTotal = $this->sum_total_det($id);
                $consecutivos = $this->get_consecutivos();
                $arrResponse = array("consecutivos" => $consecutivos, "cabecera" => $dataCab, "detalle" => $dataDet, "total" => $valorTotal);
                echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);    
            }
            die();
        }
    }

    if($_POST){
        $obj = new cdpController();
        $accion = $_POST["action"];
        
        if ($accion == "get") {
            $obj->get();
        } else if ($accion == "cuentasccpet") {
            $obj->cuentasccpet();
        } else if ($accion == "rubroPresupuestal") {
            $obj->rubroPresupuestal();
        } else if ($accion == "cpc") {
            $obj->cpc();
        } else if ($accion == "saldo") {
            $obj->saldo();
        } else if ($accion == "save") {
            $obj->save();
        } else if ($accion == "getSearch") {
            $obj->getSearch();
        } else if ($accion == "anula") {
            $obj->anula();
        } else if ($accion == "visualizar") {
            $obj->visualizar();
        }
    }
?>