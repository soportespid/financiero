 <?php
    require_once '../../../Librerias/core/Helpers.php';
    require_once '../../../Librerias/core/Mysql.php';
    require_once '../../../funciones.inc';
    session_start();
    
    class cdpModel extends Mysql {
        private $cedula;
        private $vigencia;
        private $versionGastos;
        private $versionFuentes;

        function __construct(){
            parent::__construct();
            $this->cedula = $_SESSION["cedulausu"];
            $this->vigencia = getVigenciaUsuario($this->cedula);
            $this->versionGastos = getVersionGastosCcpet();
            $this->versionFuentes = getVersionFuentesCcpet();
        }

        /* Read data */
        public function selectCodigosSectoriales(){
            $sql = "SELECT codigo, concepto as nombre, grupo, sector FROM ppto_codigos_sectoriales";
            $request = $this->select_all($sql);
            return $request;
        }
        public function get_fecha_max() {
            $sql = "SELECT MAX(fecha_solicitud) AS fecha FROM plan_solicitud_cdp WHERE estado = 'S'";
            $data = $this->select($sql);
            return $data["fecha"];
        }

        public function get_responsables_paa() {
            $sql = "SELECT DISTINCT contacto_respon AS responsable FROM contraplancompras";
            $data = $this->select_all($sql);
            return $data;
        }

        public function get_plan_compras() {
            $sql = "SELECT p.id, p.codplan AS codigo, p.descripcion, m.nombre AS nombre_modalidad, f.nombre AS nombre_fuente, valortotalest AS valor_total, valorestvigactual AS valor_vigencia, contacto_respon AS responsable FROM contraplancompras AS p INNER JOIN plan_modalidad_seleccion AS m ON p.modalidad = m.codigo INNER JOIN plan_fuentes_paa AS f ON p.fuente = f.codigo WHERE p.estado = 'S' AND p.vigencia = '$this->vigencia' ORDER BY p.id";
            $data = $this->select_all($sql);

            $manual_row = [
                'id' => 0,
                'codigo' => '0',
                'descripcion' => 'NO APLICA',
                'nombre_modalidad' => 'NO APLICA',
                'nombre_fuente' => "NO APLICA",
                'valor_total' => 0,
                'valor_vigencia' => 0,
                'responsable' => "NO APLICA"
            ];

            array_unshift($data, $manual_row);

            return $data;
        }

        public function get_areas() {
            $sql = "SELECT codarea AS codigo, nombrearea AS nombre FROM planacareas WHERE estado = 'S' ORDER BY codarea";
            $data = $this->select_all($sql);
            return $data;
        }

        public function get_tipos_gastos() {
            $sql = "SELECT codigo, nombre FROM cuentasccpet WHERE codigo LIKE '2.1%' AND nivel = 3 AND version = $this->versionGastos ORDER BY codigo";
            $data = $this->select_all($sql);
            return $data;
        }

        public function get_secciones_ppto() {
            $sql = "SELECT id_seccion_presupuestal AS codigo, nombre FROM pptoseccion_presupuestal WHERE estado = 'S' ORDER BY id_seccion_presupuestal";
            $data = $this->select_all($sql);
            return $data;
        }

        public function get_vigencias_gasto() {
            $sql = "SELECT codigo, nombre FROM ccpet_vigenciadelgasto ORDER BY id";
            $data = $this->select_all($sql);
            return $data;
        }

        public function get_rubros_presupuestales_funcionamiento($dependencia, $area, $medioPago, $vigGasto, $cuenta) {
            $filtro = $cuenta != '' ? "AND cuenta LIKE '$cuenta%'" : "";
            $sql = "SELECT cuenta, fuente, producto_servicio AS cpc FROM ccpetinicialgastosfun WHERE seccion_presupuestal = '$dependencia' AND area = '$area' AND vigencia_gasto = '$vigGasto' AND medio_pago = '$medioPago' AND vigencia = '$this->vigencia' $filtro";
            $data = $this->select_all($sql);
            foreach ($data as $key => $d) {
                $nombreCuenta = $this->get_name_ccpet($d["cuenta"]);
                if ($d["cpc"] == "") {
                    $nombreCpc = "";
                } else {
                    $nombreCpc = substr($d["cpc"], 0, 1) <= 4 ? $this->get_name_cpc_bienes($d["cpc"]) : $this->get_name_cpc_servicios($d["cpc"]);
                }
                $nombreFuente = $this->get_name_fuente($d["fuente"]);

                $data[$key]["nombre_cuenta"] = $nombreCuenta;
                $data[$key]["nombre_cpc"] = $nombreCpc;
                $data[$key]["nombre_fuente"] = $nombreFuente;
            }
            return $data;
        }

        //nombre cuenta ccpet
        private function get_name_ccpet($ccpet) {
            $sql = "SELECT nombre FROM cuentasccpet WHERE codigo = '$ccpet' AND version = $this->versionGastos";
            $data = $this->select($sql);
            return $data["nombre"];
        }

        private function get_name_cpc_bienes($cpc) {
            $sql = "SELECT titulo FROM ccpetbienestransportables WHERE grupo = '$cpc'";
            $data = $this->select($sql);
            return $data["titulo"];
        }

        private function get_name_cpc_servicios($cpc) {
            $sql = "SELECT titulo FROM ccpetservicios WHERE grupo = '$cpc'";
            $data = $this->select($sql);
            return $data["titulo"];
        }

        private function get_name_fuente($codFuente) {
            $sql = "SELECT nombre FROM ccpet_fuentes_cuipo WHERE codigo_fuente = '$codFuente' AND version = $this->versionFuentes";
            $data = $this->select($sql);
            return $data["nombre"];
        }

        public function get_cuentas_ccpet_funcionamiento($tipoGasto) {
            $filtro = "codigo LIKE '2.1%' ";
            if ($tipoGasto != 0) {
                $filtro = "t1.codigo LIKE '$tipoGasto%' ";
            }
            $sql = "SELECT t1.codigo, t1.nombre FROM cuentasccpet t1 INNER JOIN ccpetinicialgastosfun t2 ON t1.codigo = t2.cuenta WHERE t1.version = $this->versionGastos AND $filtro GROUP BY t1.codigo ORDER BY t1.codigo";
            //$sql = "SELECT codigo, nombre FROM cuentasccpet WHERE tipo = 'C' AND $filtro AND version = $this->versionGastos ORDER BY codigo";
            $data = $this->select_all($sql);
            return $data;
        }

        public function get_cuentas_ccpet_inversion() {
            $sql = "SELECT codigo, nombre FROM cuentasccpet WHERE codigo LIKE '2.3%' AND tipo = 'C' AND version = $this->versionGastos ORDER BY codigo";
            $data = $this->select_all($sql);
            return $data;
        }

        public function get_fuentes_ccpet() {
            $sql = "SELECT codigo_fuente AS codigo, nombre FROM ccpet_fuentes_cuipo ORDER BY codigo_fuente";
            $data = $this->select_all($sql);
            return $data;
        }

        private function search_clasificador($cuenta) {
            $sql = "SELECT clasificadores AS clasificador FROM ccpetprogramarclasificadoresgastos WHERE cuenta = '$cuenta'";
            $data = $this->select($sql);
            $clasificador = $data["clasificador"] == "" ? 0 : $data["clasificador"];
            return $clasificador;
        }

        public function get_cpc($cuenta) {
            $clasificador = $this->search_clasificador($cuenta);

            if ($clasificador == 2) {
                $sql = "SELECT grupo AS codigo, titulo AS nombre FROM ccpetbienestransportables WHERE LENGTH(grupo) = 7 ORDER BY grupo";
            } else if ($clasificador == 3) {
                $sql = "SELECT grupo AS codigo, titulo AS nombre FROM ccpetservicios WHERE (LENGTH(grupo) = 5 OR LENGTH(grupo) = 7) ORDER BY grupo";
            }

            if ($clasificador == 0) {
                $data = [];
            } else {
                $data = $this->select_all($sql);
            }
            
            return $data;
        }

        public function get_detalles_sectorial() {
            $sql = "SELECT id, codigo, concepto AS nombre FROM cuipo_sec_prog_gastos ORDER BY id";
            $data = $this->select_all($sql);
            return $data;
        }

        public function get_politicas_publicas() {
            $sql = "SELECT codigo, nombre FROM ccpet_politicapublica ORDER BY codigo";
            $data = $this->select_all($sql);
            return $data;
        }

        public function search_saldo($parametros) {
            $parametros["vigencia"] = $this->vigencia;
            $saldo = saldoRubroGastoCcpet($parametros);
            return $saldo;
        }

        private function search_consec($vigencia) {
            $sql = "SELECT MAX(consecutivo) AS consecutivo FROM plan_solicitud_cdp WHERE vigencia = '$vigencia'";
            $data = $this->select($sql);
            $consecutivo = $data["consecutivo"] == 0 ? 1 : $data["consecutivo"] + 1;
            return $consecutivo;
        }

        public function search_data() {
            $sql = "SELECT ps.id, ps.vigencia, ps.consecutivo, ps.fecha_solicitud, ps.fecha_vencimiento, a.nombrearea AS nombre_area, ps.objeto, ps.estado FROM plan_solicitud_cdp AS ps INNER JOIN planacareas AS a ON ps.id_area_solicitante = a.codarea WHERE ps.vigencia = '$this->vigencia' ORDER BY consecutivo DESC";
            $data = $this->select_all($sql);
            return $data;
        }

        public function get_data_plan_compras($idPaa) {
            $sql = "SELECT codplan, descripcion, modalidad FROM contraplancompras WHERE id = $idPaa";
            $data = $this->select($sql);
            return $data;
        }

        public function get_name_modalidad($codigo) {
            $sql = "SELECT nombre FROM plan_modalidad_seleccion WHERE codigo = '$codigo'";
            $data = $this->select($sql);
            return $data["nombre"];
        }

        public function get_data_cab($id) {
            $sql = "SELECT psc.consecutivo, 
            psc.fecha_solicitud, 
            psc.fecha_vencimiento,
            psc.id_paa,
            pa.nombrearea,
            psc.tipo_presupuesto,
            psc.sector AS cod_sector,
            s.nombre AS nombre_sector,
            psc.tipo_contrato_o_acto AS tipo_documento,
            psc.tipo_contrato,
            psc.numero_acto,
            psc.fecha_acto,
            psc.objeto,
            psc.tipo_gasto,
            psc.id_area_solicitante,
            psc.tipo_cdp,
            psc.estado
            FROM plan_solicitud_cdp AS psc
            INNER JOIN planacareas AS pa
            ON psc.id_area_solicitante = pa.codarea
            LEFT JOIN ccpetsectores AS s
            ON psc.sector = s.codigo
            WHERE psc.id = $id";
            $data = $this->select($sql);

            if ($data["id_paa"] != 0) {
                $planCompras = $this->get_data_plan_compras($data["id_paa"]);
                $data["codigo_plan_compras"] = $planCompras["codplan"];
                $data["descripcion_plan_compras"] = $planCompras["descripcion"];
                $data["nombre_modalidad"] = $this->get_name_modalidad($planCompras["modalidad"]);
            } else {
                $data["codigo_plan_compras"] = "NO APLICA";
                $data["nombre_modalidad"] = "NO APLICA";
                $data["descripcion_plan_compras"] = "NO APLICA";
            }

            return $data;
        }
        
        public function get_data_det($id) {
            $sql = "SELECT scdp.tipo_presupuesto AS tipoPresupuesto,
            dependencia.nombre AS nameDependencia,
            scdp.medio_pago AS medioPago,
            vig.nombre AS nameVigGasto,
            scdp.cuenta,
            scdp.fuente AS codFuente,
            scdp.cpc,
            scdp.indicador AS codIndicador,
            bpin.codigo AS bpin,
            scdp.id_detalle_sectorial AS codSectorial,
            scdp.id_politica_publica AS codPoliticaPublica,
            scdp.id_sectorial_gasto AS codSectorialGasto,
            scdp.valor
            FROM plan_solicitud_cdp_det AS scdp
            INNER JOIN pptoseccion_presupuestal AS dependencia
            ON scdp.dependencia = dependencia.id_seccion_presupuestal
            INNER JOIN ccpet_vigenciadelgasto AS vig
            ON scdp.id_vig_gasto = vig.codigo
            LEFT JOIN ccpproyectospresupuesto AS bpin
            ON scdp.id_bpin = bpin.id
            WHERE scdp.id_solicitud_cdp = $id";
            $data = $this->select_all($sql);
            return $data;
        }

        public function sum_total_det($id) {
            $sql = "SELECT SUM(valor) AS valor_total FROM plan_solicitud_cdp_det WHERE id_solicitud_cdp = $id";
            $data = $this->select($sql);
            return $data["valor_total"];
        }

        public function get_consecutivos() {
            $sql = "SELECT id FROM plan_solicitud_cdp";
            $data = $this->select_all($sql);
            return $data;
        }

        /* Create data */
        public function create_cab($data) {
            $consecutivo = $this->search_consec($this->vigencia);
            $sql = "INSERT INTO plan_solicitud_cdp (vigencia, consecutivo, fecha_solicitud, fecha_vencimiento, id_area_solicitante, id_paa, tipo_presupuesto, tipo_gasto, sector, tipo_contrato_o_acto, tipo_contrato, numero_acto, fecha_acto, objeto, estado) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
        
            $values = [
                $this->vigencia,
                $consecutivo,
                $data["fechaSolicitud"],
                $data["fechaVencimiento"],
                $data["codAreaSolicitante"],
                $data["idPaa"],
                $data["tipoPresupuesto"],
                $data["tipoGasto"],
                $data["codSector"],
                $data["tipoDocumento"],
                $data["tipoContrato"],
                $data["numeroActo"],
                $data["fechaActo"],
                $data["objeto"],
                "S"
            ];

            $resp = $this->insert($sql, $values);

            $this->create_auditoria("Crear", $consecutivo);
            return $resp;
        }

        public function create_det($id, $data){
            $sql = "INSERT INTO plan_solicitud_cdp_det (id_solicitud_cdp, tipo_presupuesto, dependencia, medio_pago, id_vig_gasto, cuenta, fuente, 
            cpc, indicador, id_bpin, id_detalle_sectorial, id_politica_publica, valor,id_sectorial_gasto) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?,?)";
            
            foreach ($data as $d) {
                $values = [
                    $id,
                    $d["tipoPresupuesto"], 
                    $d["codDependencia"],
                    $d["medioPago"],
                    $d["vigGasto"],
                    $d["cuenta"],
                    $d["codFuente"],
                    $d["cpc"],
                    $d["codIndicador"],
                    $d["idBpin"],
                    $d["codSectorial"],
                    $d["codPoliticaPublica"],
                    $d["valor"],
                    $d["codGastoSectorial"]
                ]; 
        
                $this->insert($sql, $values);
            }
        }

        private function create_auditoria(string $accion, int $consecutivo) {
            $sql_funcion = "SELECT id FROM ccpet_funciones WHERE nombre = 'Solicitud Cdp'";
            $funcion = $this->select($sql_funcion);

            insertAuditoria("ccpet_auditoria","ccpet_funciones_id",$funcion["id"],$accion,$consecutivo);
        }

        /* Update data */
        public function anula_solicitud($id) {
            $sql_consecutivo = "SELECT consecutivo FROM plan_solicitud_cdp WHERE id = $id";
            $data = $this->select($sql_consecutivo);
            $consecutivo = $data["consecutivo"];

            $sql = "UPDATE plan_solicitud_cdp SET estado = ? WHERE id = $id";
            $resp = $this->update($sql, ["N"]);
            $this->create_auditoria("Anular", $consecutivo);

            return $resp;
        }

        /* Delate data */
       
    }
?>