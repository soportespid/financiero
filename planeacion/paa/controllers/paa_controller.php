<?php
    require_once '../../../Librerias/core/Helpers.php';
    require_once '../models/paa_model.php';
    session_start();
    header('Content-Type: application/json');

    class paaController extends paaModel {
        /* crear */
        public function get() {
            if (!empty($_SESSION)) {
                $arrResponse = array("modalidades" => $this->get_modalidades(), "fuentes" => $this->get_fuentes(), "nivel_uno" => $this->get_productos_nivel_uno(), "nivel_dos" => $this->get_pruductos_nivel_dos(), "nivel_tres" => $this->get_pruductos_nivel_tres(), "nivel_cuatro" => $this->get_pruductos_nivel_cuatro());
                echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);    
            }
            die();
        }

        public function productos() {
            if (!empty($_SESSION)) {
                $padre = $_POST["padre"];
                $arrResponse = array("nivel_cinco" => $this->get_pruductos_nivel_cinco($padre));
                echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);    
            }
            die();
        }

        public function searchProducto() {
            if (!empty($_SESSION)) {
                $codigo = $_POST["codigo"];
                if ($this->existe_producto($codigo) > 0) {
                    $arrResponse = array("status" => true, "nombre" => $this->get_producto($codigo));
                } else {
                    $arrResponse = array("status" => false, "msg" => "Código UNSPSC no existe o no es un producto, verifique el código ingresado");
                }
                
                echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);    
            }
            die();
        }

        public function save () {
            if (!empty($_SESSION)) {
                $fechaCreacion = $_POST["fechaCreacion"];
                $fechaInicio = $_POST["fechaInicio"];
                $duracion = $_POST["duracion"];
                $modalidad = $_POST["codModalidad"];
                $fuente = $_POST["codFuente"];
                $descripcion = $_POST["descripcion"];
                $valorEstimado = round($_POST["valorEstimado"], 2);
                $valorVigencia = round($_POST["valorVigencia"], 2);
                $vigenciaFuturas = $_POST["vigenciasFuturas"];
                $statusVigFuturas = $_POST["statusVigenciasFuturas"];
                $responsable = $_POST["responsable"];
                $productos = json_decode($_POST['productos'], true);
                $codigos = array_map(function($producto) {
                    return $producto['codigo'];
                }, $productos);
                $codigosString = implode("-", $codigos);

                $data = [
                    "fechaCreacion" => $fechaCreacion,
                    "fechaInicio" => $fechaInicio,
                    "duracion" => $duracion,
                    "modalidad" => $modalidad,
                    "fuente" => $fuente,
                    "descripcion" => strtoupper(strClean(replaceChar($descripcion))),
                    "valorEstimado" => $valorEstimado,
                    "valorVigencia" => $valorVigencia,
                    "vigenciaFuturas" => $vigenciaFuturas,
                    "statusVigFuturas" => $statusVigFuturas,
                    "responsable" => $responsable,
                    "codigos" => $codigosString
                ];
                $resp = $this->insert_paa($data);
                if ($resp > 0) {
                    $arrResponse = array("status" => true, "msg" => "Guardado con exito", "id" => $resp);
                } else {
                    $arrResponse = array("status" => false, "msg" => "Error en guardado, vuelva a intentar");
                }
                echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
            }
            die();
        }

        /* editar */
        public function getEdit() {
            if (!empty($_SESSION)) {
                $id = $_POST["id"];
                $arrResponse = array("modalidades" => $this->get_modalidades(), "fuentes" => $this->get_fuentes(), "nivel_uno" => $this->get_productos_nivel_uno(), "nivel_dos" => $this->get_pruductos_nivel_dos(), "nivel_tres" => $this->get_pruductos_nivel_tres(), "nivel_cuatro" => $this->get_pruductos_nivel_cuatro(), "data" => $this->get_data_edit($id), "ids" => $this->get_ids());
                echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);    
            }
            die();
        }
        
        public function updateEdit() {
            if (!empty($_SESSION)) {
                $id = $_POST["id"];
                $fechaCreacion = $_POST["fechaCreacion"];
                $fechaInicio = $_POST["fechaInicio"];
                $duracion = $_POST["duracion"];
                $modalidad = $_POST["codModalidad"];
                $fuente = $_POST["codFuente"];
                $descripcion = $_POST["descripcion"];
                $valorEstimado = round($_POST["valorEstimado"], 2);
                $valorVigencia = round($_POST["valorVigencia"], 2);
                $vigenciaFuturas = $_POST["vigenciasFuturas"];
                $statusVigFuturas = $_POST["statusVigenciasFuturas"];
                $responsable = $_POST["responsable"];
                $productos = json_decode($_POST['productos'], true);
                $codigos = array_map(function($producto) {
                    return $producto['codigo'];
                }, $productos);
                $codigosString = implode("-", $codigos);

                $data = [
                    "fechaCreacion" => $fechaCreacion,
                    "fechaInicio" => $fechaInicio,
                    "duracion" => $duracion,
                    "modalidad" => $modalidad,
                    "fuente" => $fuente,
                    "descripcion" => strtoupper(strClean(replaceChar($descripcion))),
                    "valorEstimado" => $valorEstimado,
                    "valorVigencia" => $valorVigencia,
                    "vigenciaFuturas" => $vigenciaFuturas,
                    "statusVigFuturas" => $statusVigFuturas,
                    "responsable" => $responsable,
                    "codigos" => $codigosString
                ];
                $resp = $this->update_paa($data, $id);
                if ($resp > 0) {
                    $arrResponse = array("status" => true, "msg" => "Actualizado con exito");
                } else {
                    $arrResponse = array("status" => false, "msg" => "Error en guardado, vuelva a intentar");
                }
                echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
            }
            die();
        }
    }

    if($_POST){
        $obj = new paaController();
        $accion = $_POST["action"];
        
        if ($accion == "get") {
            $obj->get();
        } else if ($accion == "nivelCinco") {
            $obj->productos();
        } else if ($accion == "searchProducto") {
            $obj->searchProducto();
        } else if ($accion == "save") {
            $obj->save();
        } else if ($accion == "getEdit") {
            $obj->getEdit();
        } else if ($accion == "update") {
            $obj->updateEdit();
        }
    }
?>