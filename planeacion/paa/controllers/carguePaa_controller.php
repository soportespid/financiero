<?php
    require_once "../../../PHPExcel/Classes/PHPExcel.php";
    require_once '../../../Librerias/core/Helpers.php';
    require_once '../models/carguePaa_model.php';
    session_start();
    header('Content-Type: application/json');

    class paaController extends paaModel {
        public function get() {
            if (!empty($_SESSION)) {
                $arrResponse = array("vigencias" => $this->getVigencias());
                echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
            }
        }

        private function convertirNumeroAFecha($numero) {
            $fechaBase = new DateTime('1899-12-30'); // Fecha base de Excel
            $fechaBase->modify("+$numero days");
            return $fechaBase->format('Y-m-d');
        }

        public function upload() {
            if (!empty($_SESSION)) {
                $data = [];
                $error = false;
                $vigencia = $_POST["vigencia"];
                $fecha = $_POST["fecha"];
                $tmpfname = $_FILES['file']['tmp_name'];
                $excelReader = PHPExcel_IOFactory::createReaderForFile($tmpfname);
                $excelObj = $excelReader->load($tmpfname);
                $hojaDatos = $excelObj->getSheet(0);
                $lastRowFormatos = $hojaDatos->getHighestRow();

                for ($i=4; $i <= $lastRowFormatos ; $i++) {
                    //pasa de número a fecha Y-m-d
                    $fechaIni = "2024-01-01";
                    $mes = strtoupper(strClean(replaceChar($hojaDatos->getCell("A$i")->getValue())));
                    switch ($mes) {
                        case 'ENERO':
                            $fechaIni = "$vigencia-01-01";
                            break;
                        case 'FEBRERO':
                            $fechaIni = "$vigencia-02-01";
                            break;
                        case 'MARZO':
                            $fechaIni = "$vigencia-03-01";
                            break;
                        case 'ABRIL':
                            $fechaIni = "$vigencia-04-01";
                            break;
                        case 'MAYO':
                            $fechaIni = "$vigencia-05-01";
                            break;
                        case 'JUNIO':
                            $fechaIni = "$vigencia-06-01";
                            break;
                        case 'JULIO':
                            $fechaIni = "$vigencia-07-01";
                            break;
                        case 'AGOSTO':
                            $fechaIni = "$vigencia-08-01";
                            break;
                        case 'SEPTIEMBRE':
                            $fechaIni = "$vigencia-09-01";
                            break;
                        case 'OCTUBRE':
                            $fechaIni = "$vigencia-10-01";
                            break;
                        case 'NOVIEMBRE':
                            $fechaIni = "$vigencia-11-01";
                            break;
                        case 'DICIEMBRE':
                            $fechaIni = "$vigencia-12-01";
                            break;
                        default:
                        $fechaIni = "$vigencia-01-01";
                            break;
                    }
                    
                    //Limpia los códigos de espacios dobles, luego lo vuelve array y valida que todos tengan 8 cifras y valida que existan en el catalogo como producto
                    $codigos = $hojaDatos->getCell("B$i")->getValue();
                    $codigos = preg_replace("/\s+/", " ", trim($codigos));
                    $arrCodigos = explode(" ", $codigos);
        
                    foreach ($arrCodigos as $codigo) {
                        if (!preg_match('/^\d{8}$/', $codigo)) {
                            $error = true;
                            $arrResponse = array("status" => false, "msg" => "Error en el código $codigo en la fila $i, debe tener 8 digitos");
                            goto salir;
                        }
                        
                        // if ($this->existe_producto($codigo) == 0) {
                        //     $error = true;
                        //     $arrResponse = array("status" => false, "msg" => "Error en el código $codigo en la fila $i, no se encuentra en el catalogo como un producto");
                        //     goto salir;
                        // }
                    }

                    $codigosString = implode("-",$arrCodigos);

                    //limpia variable string
                    $descripcion = $hojaDatos->getCell("C$i")->getValue();
                    $descripcion = strtoupper(strClean(replaceChar($descripcion)));
                    
                    //pasa a entero
                    $duracion = (int) $hojaDatos->getCell("D$i")->getValue();

                    $modalidad = strtoupper($hojaDatos->getCell("E$i")->getValue());
                    if ($this->existe_modalidad($modalidad) == 0) {
                        $error = true;
                        $arrResponse = array("status" => false, "msg" => "Error en la modalidad de selección en la fila $i, revisa el formato");
                        goto salir;
                    }
                    $codModalidad = $this->search_cod_modalidad($modalidad);

                    $fuente = strtoupper($hojaDatos->getCell("F$i")->getValue());
                    if ($this->existe_fuente($fuente) == 0) {
                        $error = true;
                        $arrResponse = array("status" => false, "msg" => "Error en la fuente de recursos en la fila $i, revisa el formato");
                        goto salir;
                    }
                    $codFuente = $this->search_cod_fuente($fuente);

                    //pasa valor a float y redondea a dos cifras
                    $valorEstimado = (float) $hojaDatos->getCell("G$i")->getValue();
                    $valorEstimado = round($valorEstimado, 2);

                    //pasa valor a float y redondea a dos cifras
                    $valorVigencia = (float) $hojaDatos->getCell("H$i")->getValue();
                    $valorVigencia = round($valorVigencia, 2);

                    $requiereVigFuturas = strtoupper(strClean(replaceChar($hojaDatos->getCell("I$i")->getValue())));
                    
                    if ($requiereVigFuturas != "SI" && $requiereVigFuturas != "NO") {
                        $error = true;
                        $arrResponse = array("status" => false, "msg" => "Error en requiere vigencias futuras en la fila $i, revisa el formato");
                        goto salir;
                    }

                    $statusVigenciasFuturas = strtoupper(strClean(replaceChar($hojaDatos->getCell("J$i")->getValue())));

                    if ($statusVigenciasFuturas != "NA" && $statusVigenciasFuturas != "NO SOLICITADAS" && $statusVigenciasFuturas != "SOLICITADAS" && $statusVigenciasFuturas != "APROBADAS") {
                        $error = true;
                        $arrResponse = array("status" => false, "msg" => "Error en estado de solicitud de vigencias futuras en la fila $i, revisa el formato");
                        goto salir;
                    }

                    $responsable = $hojaDatos->getCell("K$i")->getValue();
                    $arrResponsable = explode("/", $responsable);
                    $responsable_cargo = strtoupper($arrResponsable[0]."-".$arrResponsable[1]);

                    $dataTemp = [
                        "fechaIni" => $fechaIni,
                        "codigos" => $codigosString,
                        "descripcion" => $descripcion,
                        "duracion" => $duracion,
                        "modalidad" => $codModalidad,
                        "fuente" => $codFuente,
                        "valorEstimado" => $valorEstimado,
                        "valorVigencia" => $valorVigencia,
                        "requiereVigFuturas" => $requiereVigFuturas,
                        "statusVigenciasFuturas" => $statusVigenciasFuturas,
                        "responsable" => $responsable_cargo
                    ];
                    array_push($data, $dataTemp);
                }
                salir:

                if ($error == false) {
                    $this->createPaa($data, $fecha, $vigencia);
                    $arrResponse = array("status" => true, "msg" => "El archivo ha sido cargado correctamente correctamente.");
                }
                
                echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
            }
            die();
        }

    }

    if($_POST){
        $obj = new paaController();
        $accion = $_POST["action"];

        if ($accion == 'upload') {
            $obj->upload();
        } else if ($accion == "get") {
            $obj->get();
        }
    }
?>