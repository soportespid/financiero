const URL = "planeacion/paa/controllers/carguePaa_controller.php";

var app = new Vue({
    el:"#myapp",
    data() {
        return {
            isLoading: false,
            inputFile: "",
            vigencias: [],
            vigencia: '',
            fecha: '',
        }
    },
    mounted() {
        this.get();
    },
    methods: {
        /* metodos para traer información */
       
        async get() {
            const formData = new FormData();
            formData.append("action","get");
            const response = await fetch(URL,{method:"POST",body:formData});
            const objData = await response.json();
            this.vigencias = objData.vigencias;
        },

        /* metodos para procesar información */
        getFile(element){
            this.inputFile = "";
            this.inputFile = element.event.target.files;
        },       
        /* Metodos para guardar o actualizar información */
        async upload() {
            let inputFile = this.inputFile;

            if (this.vigencia == "" || this.fecha == "") {
                Swal.fire("Error","Todos los campos con (*) es obligatorio llenarlos.","error");
                return false;
            }

            if(inputFile.length == 0){
                Swal.fire("Error","Debe subir la plantilla.","error");
                return false;
            }

            let file = inputFile[0];
            let extension = file.name.split(".")[1];

            if(extension != "xlsx"){
                Swal.fire("Error","El archivo es incorrecto; por favor, utiliza nuestra plantilla.","error");
                return false;
            }

            let formData = new FormData();
            formData.append("action","upload");
            formData.append("file",file);
            formData.append("vigencia",this.vigencia);
            formData.append("fecha",this.fecha);
            
            Swal.fire({
                title:"¿Estás segur@ de cargar?",
                text:"",
                icon: 'warning',
                showCancelButton:true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText:"Sí, guardar",
                cancelButtonText:"No, cancelar"
            }).then(async function(result){
                if(result.isConfirmed){
                    app.isLoading = true;
                    const response = await fetch(URL,{method:"POST",body:formData});
                    const objData = await response.json();
                    app.isLoading = false;
                    if(objData.status){
                        Swal.fire("Proceso con exito",objData.msg,"success");
                        setTimeout(function(){
                            window.location.href="plan-plancomprasbuscar";
                        },1500);
                    }else{
                        Swal.fire("Error",objData.msg,"error");
                    }
                }
            });

            this.inputFile = "";
            document.getElementById('inputFileElementId').value = "";
        },
        
        /* Formatos para mostrar información */
        downloadPlantilla() {
            window.open("plan-formatoPaa.php");
        },
    },
    computed:{

    }
})
