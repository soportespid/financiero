const URL = "planeacion/paa/controllers/paa_controller.php";
const URLEXCEL = "cpp-diferenciaFuentesExcel.php";
const URLPDF = "ccp-diferenciaFuentesPdf.php";

var app = new Vue({
    el:"#myapp",
    data() {
        return {
            isLoading: false,
            txtSearch: '',
            id: 0,

            fechaCreacion: '',
            fechaInicio: '',
            duracion: 0,
            descripcion: '',
            valorEstimado: '',
            valorEstimadoFormat: '',
            valorVigencia: '',
            valorVigenciaFormat: '',
            vigenciasFuturas: '',
            statusVigenciasFuturas: '',
            responsableName: '',
            responsableCargo: '',

            //modalidades
            modalidades: [],
            codModalidad: '',

            //fuentes
            fuentes: [],
            codFuente: '',

            //productos
            inicialProducto: '',
            modalProductos: false,
            productosNivelUno: [],
            nivelUno: '',
            productosNivelDos: [],
            nivelDos: '',
            productosNivelTres: [],
            nivelTres: '',
            productosNivelCuatro: [],
            nivelCuatro: '',
            productos: [],
            productosCopy: [],
            codProducto: '',
            nameProducto: '',
            productosSelect: [],

            /* editar */
            consecutivo: '',
            arrConsecutivos: [],

        }
    },
    mounted() {
        this.intPageVal = this.$refs.pageType.value;

        if (this.intPageVal == 1) {
            this.get();
        } else if (this.intPageVal == 2) {
            this.getEdit();
        } 
    },
    methods: {
        /* metodos para traer información */
        async get() {
            const formData = new FormData();
            formData.append("action","get");
            this.isLoading = true;
            const response = await fetch(URL,{method:"POST",body:formData});
            const objData = await response.json();
            this.modalidades = objData.modalidades;
            this.fuentes = objData.fuentes;
            this.productosNivelUno = objData.nivel_uno;
            this.productosNivelDos = objData.nivel_dos;
            this.productosNivelTres = objData.nivel_tres;
            this.productosNivelCuatro = objData.nivel_cuatro;
            this.isLoading = false;
        },

        async getEdit() {
            this.id = new URLSearchParams(window.location.search).get('id');
            if (this.id <= 0) return false;

            const formData = new FormData();
            formData.append("action","getEdit");
            formData.append("id",this.id);
            this.isLoading = true;
            const response = await fetch(URL,{method:"POST",body:formData});
            const objData = await response.json();
            this.modalidades = objData.modalidades;
            this.fuentes = objData.fuentes;
            this.productosNivelUno = objData.nivel_uno;
            this.productosNivelDos = objData.nivel_dos;
            this.productosNivelTres = objData.nivel_tres;
            this.productosNivelCuatro = objData.nivel_cuatro;
            this.fechaCreacion = objData.data.fechaCreacion;
            this.fechaInicio = objData.data.fechaInicio;
            this.duracion = objData.data.duracion;
            this.codModalidad = objData.data.modalidad;
            this.codFuente = objData.data.fuente;
            this.descripcion = objData.data.descripcion;
            this.valorEstimado = objData.data.valorEstimado;
            this.valorEstimadoFormat = this.formatNumber(this.valorEstimado);
            this.valorVigencia = objData.data.valorVigencia;
            this.valorVigenciaFormat = this.formatNumber(this.valorVigencia);
            this.vigenciasFuturas = objData.data.vigenciaFuturas;
            this.statusVigenciasFuturas = objData.data.statusVigFuturas;
            this.responsableName = objData.data.responsableName;
            this.responsableCargo = objData.data.responsableCargo;
            this.productosSelect = objData.data.codigos;
            this.consecutivo = objData.data.consecutivo;
            this.arrConsecutivos = objData.ids;
            this.isLoading = false;
        },

        async getNivelCinco() {
            this.productos = this.productosCopy = [];
            if (this.nivelCuatro == "") {
                return false;
            }

            const formData = new FormData();
            formData.append("action","nivelCinco");
            formData.append("padre",this.nivelCuatro);
            const response = await fetch(URL,{method:"POST",body:formData});
            const objData = await response.json();
            this.productos = this.productosCopy = objData.nivel_cinco;
        },

        async getProducto() {
            if (this.codProducto == "") {
                return false;
            }

            const formData = new FormData();
            formData.append("action","searchProducto");
            formData.append("codigo",this.codProducto);
            const response = await fetch(URL,{method:"POST",body:formData});
            const objData = await response.json();
            if (objData.status) {
                this.nameProducto = objData.nombre;
            } else {
                Swal.fire("Atención!",objData.msg,"warning");
                this.codProducto = this.nameProducto = "";
            }
        },

        /* metodos para procesar información */
        clearNiveles() {
            this.productos = this.productosCopy = [];
        },

        async searchData(option) {
            let search = "";

            switch (option) {
                case "modalProductos":
                    search = this.txtSearch.toLowerCase();
                    this.productos = [...this.productosCopy.filter(e=>e.codigo.toLowerCase().includes(search) || e.nombre.toLowerCase().includes(search))];
                    break;

                default:
                    console.log("Error, opción no encontrada");
                    break;
            }    
        },

        selectItem(option, item) {
            switch (option) {
                case "selectProducto":
                    this.codProducto = item.codigo;
                    this.nameProducto = item.nombre;
                    this.nivelUno = this.nivelDos = this.nivelTres = this.nivelCuatro = this.txtSearch = "";
                    this.productos = this.productosCopy = [];
                    this.modalProductos = false;
                    break;
                default:
                    console.log("Error, opción no encontrada");
                    break;
            }
        },

        addProducto() {
            if (this.codProducto == "" || this.nameProducto == "") {
                Swal.fire("Atención!","Debes seleccionar primero un producto UNSPSC","warning");
                return false;
            }

            this.productosSelect.push({codigo: this.codProducto, nombre: this.nameProducto});
            this.codProducto = this.nameProducto = "";
        },

        delProducto(index) {
            this.productosSelect.splice(index, 1);
        },

        nextItem:function(type){
            let id = this.id;
            let index = this.arrConsecutivos.findIndex(function(e){return e.id == id});
            if(type=="next" && this.arrConsecutivos[++index]){
                id = this.arrConsecutivos[index++].id;
            }else if(type=="prev" && this.arrConsecutivos[--index]){
                id = this.arrConsecutivos[index--].id;
            }
            window.location.href="plan-planAnualComprasEditar"+'?id='+id;
        },
        
        /* Metodos para guardar o actualizar información */
        async save() {
            if (this.fechaCreacion == "" || this.fechaInicio == "" || this.duracion == "" || this.codModalidad == "" || this.codFuente == "" || this.descripcion == "" || this.valorEstimado == "" || this.valorVigencia == "" || this.vigenciasFuturas == "" || this.statusVigenciasFuturas == "" || this.responsableName == "" || this.responsableCargo == "") {
                Swal.fire("Atención!","Todos los campos con (*) son obligatorios","warning");
                return false;
            }

            if (this.duracion <= 0) {
                Swal.fire("Atención!","La duración del contrato debe ser mayor a cero","warning");
                return false;
            }
    
            if (!this.productosSelect || this.productosSelect.length === 0) {
                Swal.fire("Atención!","Debes agregar minimo un código UNSPSC","warning");
                return false;
            }

            const formData = new FormData();
            formData.append("action","save");
            formData.append("fechaCreacion",this.fechaCreacion);
            formData.append("fechaInicio",this.fechaInicio);
            formData.append("duracion",this.duracion);
            formData.append("codModalidad",this.codModalidad);
            formData.append("codFuente",this.codFuente);
            formData.append("descripcion",this.descripcion);
            formData.append("valorEstimado",this.valorEstimado);
            formData.append("valorVigencia",this.valorVigencia);
            formData.append("vigenciasFuturas",this.vigenciasFuturas);
            formData.append("statusVigenciasFuturas",this.statusVigenciasFuturas);
            let responsable = this.responsableName+"-"+this.responsableCargo;
            formData.append("responsable",responsable);
            formData.append("productos", JSON.stringify(this.productosSelect));

            Swal.fire({
                title:"¿Estás segur@ de guardar?",
                text:"",
                icon: 'warning',
                showCancelButton:true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText:"Sí, guardar",
                cancelButtonText:"No, cancelar"
            }).then(async function(result) {
                if(result.isConfirmed) {
                    const response = await fetch(URL,{method:"POST",body:formData});
                    const objData = await response.json();

                    if (objData.status) {
                        Swal.fire("Guardado",objData.msg,"success");
                        if(objData.id){
                            setTimeout(function(){
                                window.location.href="plan-planAnualComprasEditar"+'?id='+objData.id;
                            },1500);
                        }
                    }
                    else {
                        Swal.fire("Error!",objData.msg,"warning");
                    }
                }
            });
        },

        async update() {
            if (this.fechaCreacion == "" || this.fechaInicio == "" || this.duracion == "" || this.codModalidad == "" || this.codFuente == "" || this.descripcion == "" || this.valorEstimado == "" || this.valorVigencia == "" || this.vigenciasFuturas == "" || this.statusVigenciasFuturas == "" || this.responsableName == "" || this.responsableCargo == "") {
                Swal.fire("Atención!","Todos los campos con (*) son obligatorios","warning");
                return false;
            }

            if (this.duracion <= 0) {
                Swal.fire("Atención!","La duración del contrato debe ser mayor a cero","warning");
                return false;
            }
    
            if (!this.productosSelect || this.productosSelect.length === 0) {
                Swal.fire("Atención!","Debes agregar minimo un código UNSPSC","warning");
                return false;
            }

            const formData = new FormData();
            formData.append("action","update");
            formData.append("id",this.id);
            formData.append("fechaCreacion",this.fechaCreacion);
            formData.append("fechaInicio",this.fechaInicio);
            formData.append("duracion",this.duracion);
            formData.append("codModalidad",this.codModalidad);
            formData.append("codFuente",this.codFuente);
            formData.append("descripcion",this.descripcion);
            formData.append("valorEstimado",this.valorEstimado);
            formData.append("valorVigencia",this.valorVigencia);
            formData.append("vigenciasFuturas",this.vigenciasFuturas);
            formData.append("statusVigenciasFuturas",this.statusVigenciasFuturas);
            let responsable = this.responsableName+"-"+this.responsableCargo;
            formData.append("responsable",responsable);
            formData.append("productos", JSON.stringify(this.productosSelect));

            Swal.fire({
                title:"¿Estás segur@ de guardar?",
                text:"",
                icon: 'warning',
                showCancelButton:true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText:"Sí, guardar",
                cancelButtonText:"No, cancelar"
            }).then(async function(result) {
                if(result.isConfirmed) {
                    const response = await fetch(URL,{method:"POST",body:formData});
                    const objData = await response.json();

                    if (objData.status) {
                        Swal.fire("Guardado",objData.msg,"success");
                    }
                    else {
                        Swal.fire("Error!",objData.msg,"warning");
                    }
                }
            });
        },

        /* Formatos para mostrar información */
        formatInputNumber(conFormato, sinFormato) {
            this[conFormato] = this.formatNumber(this[conFormato]);
            this[sinFormato] = this[conFormato].replace(/[^0-9.]/g, "");
        },

        formatNumber(number) {
            if (!number) return ""; // Si no hay entrada, retorna vacío
    
            // Elimina caracteres no válidos (excepto números y el punto decimal)
            number = number.replace(/[^0-9.]/g, "");
    
            // Divide la parte entera y decimal
            const [integerPart, decimalPart] = number.split(".");
    
            // Formatear la parte entera con separador de miles
            const formattedInteger = integerPart.replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    
            // Si hay parte decimal, la une; si no, devuelve solo la parte entera
            return decimalPart !== undefined
                ? `${formattedInteger}.${decimalPart.slice(0, 2)}` // Limita a 2 decimales si es necesario
                : formattedInteger;
        },


        viewFormatNumber: function(valor){
            return new Intl.NumberFormat("en-US", {style: "currency", currency: "USD"}).format(valor);  
        },
        /* enviar informacion excel o pdf */
    },
    computed:{

    }
})
