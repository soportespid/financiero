<?php
    require_once '../../../Librerias/core/Helpers.php';
    require_once '../../../Librerias/core/Mysql.php';
    session_start();
    ini_set('display_errors', '1');
    ini_set('display_startup_errors', '1');
    error_reporting(E_ERROR);
    class paaModel extends Mysql{
        /* search */
        public function getVigencias() {
            $sql = "SELECT anio AS vigencia FROM admbloqueoanio WHERE bloqueado = 'N' ORDER BY anio DESC";
            $vigencias = $this->select_all($sql);
            return $vigencias;
        }

        public function existe_producto(int $codigo) {
            $sql = "SELECT COUNT(*) AS total_row FROM productospaa WHERE codigo = '$codigo' AND tipo = 5";
            $resp = $this->select($sql);
            return $resp["total_row"];
        }

        public function existe_modalidad($dato) {
            $sql = "SELECT COUNT(*) AS total_row FROM plan_modalidad_seleccion WHERE nombre = '$dato' OR codigo = '$dato'";
            $resp = $this->select($sql);
            return $resp["total_row"];
        }

        public function existe_fuente($dato) {
            $sql = "SELECT COUNT(*) AS total_row FROM plan_fuentes_paa WHERE nombre = '$dato' OR codigo = '$dato'";
            $resp = $this->select($sql);
            return $resp["total_row"];
        }

        public function search_cod_modalidad($dato) {
            $sql = "SELECT codigo FROM plan_modalidad_seleccion WHERE nombre = '$dato' OR codigo = '$dato'";
            $resp = $this->select($sql);
            return $resp["codigo"];
        }

        public function search_cod_fuente($dato) {
            $sql = "SELECT codigo FROM plan_fuentes_paa WHERE nombre = '$dato' OR codigo = '$dato'";
            $resp = $this->select($sql);
            return $resp["codigo"];
        }

        public function search_consec_paa($vigencia) {
            $sql = "SELECT MAX(CONVERT(codplan, SIGNED INTEGER)) AS cod FROM contraplancompras WHERE vigencia = '$vigencia'";
            $resp = $this->select($sql);
            $consecutivo = $resp["cod"] == "" ? 1 : $resp["cod"] + 1;
            return $consecutivo;
        }

        /* Create */
        public function createPaa(array $data, $fecha, $vigencia) {
            $sql = "INSERT INTO contraplancompras (codplan, vigencia, fecharegistro, codelaboradopor, codigosunspsc, descripcion, fechaestinicio, duracionest, modalidad, fuente, valortotalest, valorestvigactual, requierevigfut, estadovigfut, estado, contacto_respon) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";

            foreach ($data as $d) {
                $codPlan = $this->search_consec_paa($vigencia);

                $arrayData = [$codPlan, $vigencia, $fecha, "$_SESSION[cedulausu]", "$d[codigos]", "$d[descripcion]", "$d[fechaIni]", $d["duracion"], $d["modalidad"], $d["fuente"], $d["valorEstimado"], $d["valorVigencia"], "$d[requiereVigFuturas]", "$d[statusVigenciasFuturas]", "S", "$d[responsable]"];
                $this->insert($sql, $arrayData);
            }
        }
    }
?>