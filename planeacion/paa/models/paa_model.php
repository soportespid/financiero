<?php
    require_once '../../../Librerias/core/Helpers.php';
    require_once '../../../Librerias/core/Mysql.php';
    session_start();
    
    class paaModel extends Mysql {
        /* Read data */
        public function get_modalidades() {
            $sql = "SELECT codigo, nombre FROM plan_modalidad_seleccion ORDER BY codigo";
            $data = $this->select_all($sql);
            return $data;
        }

        public function get_fuentes() {
            $sql = "SELECT codigo, nombre FROM plan_fuentes_paa ORDER BY codigo";
            $data = $this->select_all($sql);
            return $data;
        }

        public function get_productos_nivel_uno() {
            $sql = "SELECT codigo, nombre FROM productospaa WHERE tipo = 1 AND estado = 'S' ORDER BY codigo";
            $data = $this->select_all($sql);
            return $data;
        }

        public function get_pruductos_nivel_dos() {
            $sql = "SELECT codigo, nombre, padre FROM productospaa WHERE tipo = 2 AND estado = 'S' ORDER BY codigo";
            $data = $this->select_all($sql);
            return $data;
        }

        public function get_pruductos_nivel_tres() {
            $sql = "SELECT codigo, nombre, padre FROM productospaa WHERE tipo = 3 AND estado = 'S' ORDER BY codigo";
            $data = $this->select_all($sql);
            return $data;
        }

        public function get_pruductos_nivel_cuatro() {
            $sql = "SELECT codigo, nombre, padre FROM productospaa WHERE tipo = 4 AND estado = 'S' ORDER BY codigo";
            $data = $this->select_all($sql);
            return $data;
        }

        public function get_pruductos_nivel_cinco($padre) {
            $sql = "SELECT codigo, nombre FROM productospaa WHERE tipo = 5 AND estado = 'S' AND padre = $padre ORDER BY codigo";
            $data = $this->select_all($sql);
            return $data;
        }

        public function get_producto($codigo) {
            $sql = "SELECT nombre FROM productospaa WHERE tipo = 5 AND estado = 'S' AND codigo = $codigo";
            $data = $this->select($sql);
            return $data["nombre"];
        }

        public function existe_producto($codigo) {
            $sql = "SELECT COUNT(*) as total_row FROM productospaa WHERE tipo = 5 AND estado = 'S' AND codigo = $codigo";
            $data = $this->select($sql);
            return $data["total_row"];
        }

        private function search_consec_paa($vigencia) {
            $sql = "SELECT MAX(CONVERT(codplan, SIGNED INTEGER)) AS cod FROM contraplancompras WHERE vigencia = '$vigencia'";
            $resp = $this->select($sql);
            $consecutivo = $resp["cod"] == "" ? 1 : $resp["cod"] + 1;
            return $consecutivo;
        }

        public function get_ids() {
            $sql = "SELECT id FROM contraplancompras ORDER BY id ASC";
            $data = $this->select_all($sql);
            return $data;
        }

        public function get_data_edit($id) {
            $productos = [];
            $sql = "SELECT fecharegistro, codelaboradopor, codigosunspsc, descripcion, fechaestinicio, duracionest, modalidad, fuente, valortotalest, valorestvigactual, requierevigfut, estadovigfut, contacto_respon, codplan FROM contraplancompras WHERE id = $id";
            $data = $this->select($sql);
            $arrProductos = explode("-", $data["codigosunspsc"]);
            foreach ($arrProductos as $product) {
                $temp = ["codigo" => $product, "nombre" => $this->get_producto($product)];
                array_push($productos, $temp);
            }
            $arrResponsable = explode("-", $data["contacto_respon"]);
            $dataEdit = [
                "fechaCreacion" => $data["fecharegistro"],
                "fechaInicio" => $data["fechaestinicio"],
                "duracion" => $data["duracionest"],
                "modalidad" => $data["modalidad"],
                "fuente" => $data["fuente"],
                "descripcion" => $data["descripcion"],
                "valorEstimado" => $data["valortotalest"],
                "valorVigencia" => $data["valorestvigactual"],
                "vigenciaFuturas" => $data["requierevigfut"],
                "statusVigFuturas" => $data["estadovigfut"],
                "responsableName" => $arrResponsable[0],
                "responsableCargo" => $arrResponsable[1],
                "codigos" => $productos,
                "consecutivo" => $data["codplan"]
            ];
            return $dataEdit;
        }

        /* Create data */
        public function insert_paa(array $data) {
            $cedula = $_SESSION["cedulausu"];
            $vigencia = getVigenciaUsuario($cedula);
            $codplan = $this->search_consec_paa($vigencia);

            $sql = "INSERT INTO contraplancompras (codplan, vigencia, fecharegistro, codelaboradopor, codigosunspsc, descripcion, fechaestinicio, duracionest, modalidad, fuente, valortotalest, valorestvigactual, requierevigfut, estadovigfut, estado, contacto_respon) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";

            $data_insert = [
                $codplan, $vigencia, "$data[fechaCreacion]", "$cedula", "$data[codigos]", "$data[descripcion]", "$data[fechaInicio]", $data["duracion"], $data["modalidad"], $data["fuente"], $data["valorEstimado"], $data["valorVigencia"], "$data[vigenciaFuturas]", "$data[statusVigFuturas]", "S", "$data[responsable]"
            ];

            $resp = $this->insert($sql, $data_insert);
            return $resp;
        }
        /* Update data */
        public function update_paa(array $data, int $id) {
            $sql = "UPDATE contraplancompras SET fecharegistro = ?, codigosunspsc = ?, descripcion = ?, fechaestinicio = ?, duracionest = ?, modalidad = ?, fuente = ?, valortotalest = ?, valorestvigactual = ?, requierevigfut = ?, estadovigfut = ?, contacto_respon = ? WHERE id = $id";
            $data_update = [
                "$data[fechaCreacion]", "$data[codigos]", "$data[descripcion]", "$data[fechaInicio]", $data["duracion"], $data["modalidad"], $data["fuente"], $data["valorEstimado"], $data["valorVigencia"], "$data[vigenciaFuturas]", "$data[statusVigFuturas]", "$data[responsable]"
            ];
            $resp = $this->update($sql, $data_update);
            return $resp;
        }
        /* Delate data */
       
    }
?>