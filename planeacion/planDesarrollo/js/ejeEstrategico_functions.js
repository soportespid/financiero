const URL = "planeacion/planDesarrollo/controllers/ejeEstrategico_controller.php";

var app = new Vue({
    el:"#myapp",
    data() {
        return {
            isLoading: false,
            dataSearch: [],
            dataSearchCopy: [],
            txtSearch: '',
            id: 0, 
            arrConsecutivos: [],

            nombreEje: '',
            descripcion: '',
            nivelPrioridad: '',
            color: '',
            planes: [],
            pdtId: '',
        }
    },
    mounted() {
        this.intPageVal = this.$refs.pageType.value;

        if (this.intPageVal == 1) {
            this.get();
        } else if (this.intPageVal == 2) {
            this.getSearch();
        } else if (this.intPageVal == 3) {
            this.getEdit();
        }
    },
    methods: {
        /* metodos para traer información */
        async get() {
            const formData = new FormData();
            formData.append("action","get");
            const response = await fetch(URL,{method:"POST",body:formData});
            const objData = await response.json();
            this.planes = objData.planes;
        },

        async getEdit() {
            this.id = new URLSearchParams(window.location.search).get('id');
            const formData = new FormData();
            formData.append("action","getEdit");
            formData.append("id", this.id);
            const response = await fetch(URL,{method:"POST",body:formData});
            const objData = await response.json();
            this.arrConsecutivos = objData.consecutivos;
            this.nombreEje = objData.data.nombre;
            this.descripcion = objData.data.descripcion;
            this.nivelPrioridad = objData.data.prioridad;
            this.color = objData.data.color_representativo;
            this.pdtId = objData.data.pdt_id;
            this.planes = objData.planes;
        },

        async getSearch() {
            const formData = new FormData();
            formData.append("action","getSearch");
            const response = await fetch(URL,{method:"POST",body:formData});
            const objData = await response.json();
            this.dataSearch = this.dataSearchCopy = objData.data;
        },

        /* metodos para procesar información */
        nextItem:function(type){
            let id = this.id;
            let index = this.arrConsecutivos.findIndex(function(e){return e.id == id});
            if(type=="next" && this.arrConsecutivos[++index]){
                id = this.arrConsecutivos[index++].id;
            }else if(type=="prev" && this.arrConsecutivos[--index]){
                id = this.arrConsecutivos[index--].id;
            }
            window.location.href="plan-editaEjeEstrategico"+'?id='+id;
        },

        /* Metodos para guardar o actualizar información */
        async save() {
            if (this.nombreEje == "" || this.descripcion == "" || this.nivelPrioridad == "" || this.color == "" || this.pdtId == "") {
                Swal.fire("Atención!","Todos los campos con * son obligatorios","warning");
                return false;
            }

            const formData = new FormData();
            formData.append("action","save");
            formData.append("pdtId",this.pdtId);
            formData.append("nombre",this.nombreEje);
            formData.append("descripcion",this.descripcion);
            formData.append("nivel",this.nivelPrioridad);
            formData.append("color",this.color);

            Swal.fire({
                title:"¿Estás segur@ de guardar?",
                text:"",
                icon: 'warning',
                showCancelButton:true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText:"Sí, guardar",
                cancelButtonText:"No, cancelar"
            }).then(async function(result) {
                if(result.isConfirmed) {
                    const response = await fetch(URL,{method:"POST",body:formData});
                    const objData = await response.json();

                    if (objData.status) {
                        Swal.fire("Guardado",objData.msg,"success");
                        setTimeout(function(){
                            location.href = "plan-editaEjeEstrategico?id="+objData.id;
                        },1000);
                    }
                    else {
                        Swal.fire("Error!",objData.msg,"warning");
                    }
                }
            });
        },

        async saveEdit() {
            if (this.nombreEje == "" || this.descripcion == "" || this.nivelPrioridad == "" || this.color == "" || this.pdtId == "") {
                Swal.fire("Atención!","Todos los campos con * son obligatorios","warning");
                return false;
            }

            const formData = new FormData();
            formData.append("action","saveEdit");
            formData.append("id",this.id);
            formData.append("pdtId",this.pdtId);
            formData.append("nombre",this.nombreEje);
            formData.append("descripcion",this.descripcion);
            formData.append("nivel",this.nivelPrioridad);
            formData.append("color",this.color);

            Swal.fire({
                title:"¿Estás segur@ de guardar?",
                text:"",
                icon: 'warning',
                showCancelButton:true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText:"Sí, guardar",
                cancelButtonText:"No, cancelar"
            }).then(async function(result) {
                if(result.isConfirmed) {
                    const response = await fetch(URL,{method:"POST",body:formData});
                    const objData = await response.json();

                    if (objData.status) {
                        Swal.fire("Guardado",objData.msg,"success");
                    }
                    else {
                        Swal.fire("Error!",objData.msg,"warning");
                    }
                }
            });
        },

        async delEje(id) {
            const formData = new FormData();
            formData.append("action","delEje");
            formData.append("id",id);

            Swal.fire({
                title:"¿Estás segur@ de eliminar?",
                text:"",
                icon: 'warning',
                showCancelButton:true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText:"Sí, eliminar",
                cancelButtonText:"No, cancelar"
            }).then(async function(result) {
                if(result.isConfirmed) {
                    const response = await fetch(URL,{method:"POST",body:formData});
                    const objData = await response.json();

                    if (objData.status) {
                        Swal.fire("Eliminado",objData.msg,"success");
                        setTimeout(function(){
                            window.location.reload();
                        },1000);
                    }
                    else {
                        Swal.fire("Error!",objData.msg,"warning");
                    }
                }
            });
        },

        /* Formatos para mostrar información */
    },
    computed:{

    }
})
