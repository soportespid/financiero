const URL = "planeacion/planDesarrollo/controllers/cargueIndicadores_controller.php";

var app = new Vue({
    el:"#myapp",
    data() {
        return {
            isLoading: false,
            dataSearch: [],
            dataSearchCopy: [],
            txtSearch: '',
            id: 0, 
            arrConsecutivos: [],
            inputFile: "",
        }
    },
    mounted() {
        
    },
    methods: {
        /* metodos para traer información */
       
        async getSearch() {
            const formData = new FormData();
            formData.append("action","getSearch");
            const response = await fetch(URL,{method:"POST",body:formData});
            const objData = await response.json();
            this.dataSearch = this.dataSearchCopy = objData.data;
        },

        /* metodos para procesar información */
        getFile(element){
            this.inputFile = element.event.target.files;
        },       
        /* Metodos para guardar o actualizar información */
        async upload() {
            let inputFile = this.inputFile;

            if(inputFile.length == 0){
                Swal.fire("Error","Debe subir la plantilla.","error");
                return false;
            }
            let file = inputFile[0];
            let extension = file.name.split(".")[1];

            if(extension != "xlsx"){
                Swal.fire("Error","El archivo es incorrecto; por favor, utiliza nuestra plantilla.","error");
                return false;
            }

            let formData = new FormData();
            formData.append("action","upload");
            formData.append("file",file);

            Swal.fire({
                title:"¿Estás segur@ de cargar?",
                text:"",
                icon: 'warning',
                showCancelButton:true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText:"Sí, guardar",
                cancelButtonText:"No, cancelar"
            }).then(async function(result){
                if(result.isConfirmed){
                    app.isLoading = true;
                    const response = await fetch(URL,{method:"POST",body:formData});
                    const objData = await response.json();
                    app.isLoading = false;
                    if(objData.status){
                        Swal.fire("Proceso con exito",objData.msg,"success");
                    }else{
                        Swal.fire("Error",objData.msg,"error");
                    }
                }
            });
        },
        
        /* Formatos para mostrar información */
        downloadPlantilla() {
            window.open("plan-plantillaIndicadoresExcel.php");
        },
    },
    computed:{

    }
})
