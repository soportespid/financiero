const URL = "planeacion/planDesarrollo/controllers/isgr_controller.php";
const URLEXCEL = "plan-isgrExcel.php";

var app = new Vue({
    el:"#myapp",
    components: { Multiselect: window.VueMultiselect.default },
    data() {
        return {
            isLoading: false,
            dataSearch: [],
            dataSearchCopy: [],
            txtSearch: '',
            id: 0, 
            arrConsecutivos: [],
            nombre: '',
            tipo: '',
            //funcionarios
            modalFuncionarios: false,
            funcionarios: [], funcionariosCopy: [],
            terceroId: '',
            documento: '',
            funcionario: '',
            //plan desarrollo territorial
            planes: [],
            pdtId: '',
            //Ejes estrategicos
            ejesEstrategicos: [],
            ejesSelected: [],
            //sectores
            modalSectores: false,
            sectores: [],
            sectoresCopy: [],
            sectorId: '',
            codSector: '',
            nomSector: '',
            //valor de recursos
            recurso: '',
            recursoFormat: '',
            bpins: [],
            txtBpin: '',
        }
    },
    mounted() {
        this.intPageVal = this.$refs.pageType.value;

        if (this.intPageVal == 1) {
            this.get();
        } else if (this.intPageVal == 2) {
            this.getSearch();
        } else if (this.intPageVal == 3) {
            this.getEdit();
        }
    },
    methods: {
        /* metodos para traer información */
        async get() {
            const formData = new FormData();
            formData.append("action","get");
            this.isLoading = true;
            const response = await fetch(URL,{method:"POST",body:formData});
            const objData = await response.json();
            this.funcionarios = this.funcionariosCopy = objData.funcionarios;
            this.planes = objData.planes;
            this.sectores = this.sectoresCopy = objData.sectores;
            this.isLoading = false;
        },

        async getSearch() {
            const formData = new FormData();
            formData.append("action","getSearch");
            const response = await fetch(URL,{method:"POST",body:formData});
            const objData = await response.json();
            this.dataSearch = this.dataSearchCopy = objData.data;
        },

        async getEdit() {
            this.id = new URLSearchParams(window.location.search).get('id');
            if (this.id <= 0) return false;

            const formData = new FormData();
            formData.append("action","getEdit");
            formData.append("id", this.id);
            this.isLoading = true;
            const response = await fetch(URL,{method:"POST",body:formData});
            const objData = await response.json();
            this.funcionarios = this.funcionariosCopy = objData.funcionarios;
            this.ejesEstrategicos = objData.ejes;
            this.planes = objData.planes;
            this.sectores = this.sectoresCopy = objData.sectores;
            this.arrConsecutivos = objData.consecutivos;
            this.pdtId = objData.dataEdit.pdt_id;
            this.nombre = objData.dataEdit.nombre;
            this.tipo = objData.dataEdit.tipo;
            this.sectorId = objData.dataEdit.sector_id;
            this.codSector = objData.dataEdit.codigo_sector;
            this.nomSector = objData.dataEdit.nombre_sector;
            this.recurso = objData.dataEdit.recurso_indicativo;
            this.recursoFormat = this.formatNumber(this.recurso);
            this.terceroId = objData.dataEdit.tercero_id;
            this.documento = objData.dataEdit.documento_funcionario;
            this.funcionario = objData.dataEdit.funcionario;
            this.ejesSelected = objData.ejesSelected;
            this.bpins = objData.bpins;
            this.isLoading = false;
        },

        async changeDataPdt() {
            this.ejesEstrategicos = this.ejesSelected = [];
            if (this.pdtId == "") {
                return false;
            }

            const formData = new FormData();
            formData.append("action","changeDataPdt");
            formData.append("pdtId", this.pdtId);
            const response = await fetch(URL,{method:"POST",body:formData});
            const objData = await response.json();
            this.ejesEstrategicos = objData.ejes;
        },

        // /* metodos para procesar información */
        async searchData(option) {
            let search = "";

            switch (option) {
                case "modalSector":
                    search = this.txtSearch.toLowerCase();
                    this.sectores = [...this.sectoresCopy.filter(e=>e.codigo.toLowerCase().includes(search) || e.nombre.toLowerCase().includes(search))];
                    break;

                case "modalFuncionario":
                    search = this.txtSearch.toLowerCase();
                    this.funcionarios = [...this.funcionariosCopy.filter(e=>e.cedulanit.toLowerCase().includes(search) || e.nombre.toLowerCase().includes(search) || e.nombre_cargo.toLowerCase().includes(search) )];
                    break;

                case "inputSearch":
                    search = this.txtSearch.toLowerCase();
                    this.dataSearch = [...this.dataSearchCopy.filter(e=>e.id.toLowerCase().includes(search) || e.nombre.toLowerCase().includes(search) || e.nombre_sector.toLowerCase().includes(search) )];
                    break;
                default:
                    console.log("Error, opción no encontrada");
                    break;
            }    
        },

        selectItem(option, item) {
            switch (option) {
                case "selectSector":
                    this.sectorId = item.id;
                    this.codSector = item.codigo;
                    this.nomSector = item.nombre;
                    this.modalSectores = false;
                    this.sectores = this.sectoresCopy;
                    this.txtSearch = "";
                    break;

                case "selectFuncionario":
                    this.terceroId = item.tercero_id;
                    this.documento = item.cedulanit;
                    this.funcionario = item.nombre;
                    this.modalFuncionarios = false;
                    this.funcionarios = this.funcionariosCopy;
                    this.txtSearch = "";
                    break;
            
                default:
                    console.log("Error, opción no encontrada");
                    break;
            }
        },
        
        nextItem(type){
            let id = this.id;
            let index = this.arrConsecutivos.findIndex(function(e){return e.id == id});
            if(type=="next" && this.arrConsecutivos[++index]){
                id = this.arrConsecutivos[index++].id;
            }else if(type=="prev" && this.arrConsecutivos[--index]){
                id = this.arrConsecutivos[index--].id;
            }
            window.location.href="plan-editaIsgr"+'?id='+id;
        },

        validaBpinAdd(bpin) {
            let status = false;
            this.bpins.forEach(bpins => {
                if (bpins.bpin == bpin) {
                    status = true;
                }
            });
            return status;
        },

        async addBpin() {
            if (this.txtBpin == "") {
                return false;
            }
            
            if (this.validaBpinAdd(this.txtBpin)) {
                Swal.fire("Atención!","El BPIN que intentas agregar ya fue agregado a la lista","warning");
                return false;
            }

            const formData = new FormData();
            formData.append("action","searchBpin");
            formData.append("bpin", this.txtBpin);
            const response = await fetch(URL,{method:"POST",body:formData});
            const objData = await response.json();
            if (objData.cantidad > 0) {
                this.bpins.push({bpin: this.txtBpin});
                this.txtBpin = "";
            }
            else {
                Swal.fire("Atención!","El BPIN que intentas agregar no se ha radicado en el sistema","warning");
            }
        },

        delBpin(index) {
            this.bpins.splice(index, 1);
        },
        
        // /* Metodos para guardar o actualizar información */
        async save() {
            if (this.pdtId == "" || this.nombre == "" || this.tipo == "" || this.sectorId == "" || this.recurso == "" || this.ejesSelected.length == 0 || this.terceroId == "" || this.bpins.length == 0) {
                Swal.fire("Atención!","Todos los campos con (*) son obligatorios!","warning");
                return false;
            }

            const formData = new FormData();
            formData.append("action","save");
            formData.append("pdtId",this.pdtId);
            formData.append("nombre",this.nombre);
            formData.append("tipo",this.tipo);
            formData.append("sectorId", this.sectorId);
            formData.append("recurso",this.recurso);
            formData.append("terceroId",this.terceroId);
            formData.append("ejes", JSON.stringify(this.ejesSelected));
            formData.append("bpins", JSON.stringify(this.bpins));

            Swal.fire({
                title:"¿Estás segur@ de guardar?",
                text:"",
                icon: 'warning',
                showCancelButton:true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText:"Sí, guardar",
                cancelButtonText:"No, cancelar"
            }).then(async function(result) {
                if(result.isConfirmed) {
                    const response = await fetch(URL,{method:"POST",body:formData});
                    const objData = await response.json();

                    if (objData.status) {
                        Swal.fire("Guardado",objData.msg,"success");
                        if(objData.id){
                            setTimeout(function(){
                                window.location.href="plan-editaIsgr"+'?id='+objData.id;
                            },1500);
                        }
                    }
                    else {
                        Swal.fire("Error!",objData.msg,"warning");
                    }
                }
            });
        },

        async saveEdit() {
            if (this.pdtId == "" || this.nombre == "" || this.tipo == "" || this.sectorId == "" || this.recurso == "" || this.ejesSelected.length == 0 || this.terceroId == "" || this.bpins.length == 0) {
                Swal.fire("Atención!","Todos los campos con (*) son obligatorios!","warning");
                return false;
            }

            const formData = new FormData();
            formData.append("action","saveEdit");
            formData.append("id",this.id);
            formData.append("pdtId",this.pdtId);
            formData.append("nombre",this.nombre);
            formData.append("tipo",this.tipo);
            formData.append("sectorId", this.sectorId);
            formData.append("recurso",this.recurso);
            formData.append("terceroId",this.terceroId);
            formData.append("ejes", JSON.stringify(this.ejesSelected));
            formData.append("bpins", JSON.stringify(this.bpins));

            Swal.fire({
                title:"¿Estás segur@ de guardar?",
                text:"",
                icon: 'warning',
                showCancelButton:true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText:"Sí, guardar",
                cancelButtonText:"No, cancelar"
            }).then(async function(result) {
                if(result.isConfirmed) {
                    const response = await fetch(URL,{method:"POST",body:formData});
                    const objData = await response.json();

                    if (objData.status) {
                        Swal.fire("Guardado",objData.msg,"success");
                    }
                    else {
                        Swal.fire("Error!",objData.msg,"warning");
                    }
                }
            });
        },

        /* Formatos para mostrar información */
        formatInputNumber(conFormato, sinFormato) {
            this[conFormato] = this.formatNumber(this[conFormato]);
            this[sinFormato] = this[conFormato].replace(/[^0-9.]/g, "");
        },

        formatNumber(number) {
            if (!number) return ""; // Si no hay entrada, retorna vacío
    
            // Elimina caracteres no válidos (excepto números y el punto decimal)
            number = number.replace(/[^0-9.]/g, "");
    
            // Divide la parte entera y decimal
            const [integerPart, decimalPart] = number.split(".");
    
            // Formatear la parte entera con separador de miles
            const formattedInteger = integerPart.replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    
            // Si hay parte decimal, la une; si no, devuelve solo la parte entera
            return decimalPart !== undefined
                ? `${formattedInteger}.${decimalPart.slice(0, 2)}` // Limita a 2 decimales si es necesario
                : formattedInteger;
        },

        viewFormatNumber: function(valor){
            return new Intl.NumberFormat("en-US", {style: "currency", currency: "USD"}).format(valor);
            
        },

        /* enviar informacion excel o pdf */

        excel() {
            const form = document.createElement("form");
            form.method = "post";
            form.target = "_blank";
            form.action = URLEXCEL;       
            
            document.body.appendChild(form);
            form.submit();
            document.body.removeChild(form);
        }
    },
    computed:{

    }
})
