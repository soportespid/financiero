const URL = "planeacion/planDesarrollo/controllers/planDesarrollo_controller.php";

var app = new Vue({
    el:"#myapp",
    data() {
        return {
            isLoading: false,
            dataSearch: [],
            dataSearchCopy: [],
            txtSearch: '',
            txtResultados: 0,
            id: 0, 
            arrConsecutivos: [],
            typeVista: 1,
            vistaEjes: 1,
            vistaResultados: 1,
            vistaIndicadores: 1,
            vistaIniciativas: 1,

            representante: '',
            periodos: [],
            periodo: '',
            namePdt: '',
            fechaIni: '',
            fechaFin: '',
            vision: '',
            txtObjetivo: '',
            objetivos: [],
            pdts: [],
            pdtsCopy: [],
            presupuestoTotal: 0,

            //datos visualizar
            lineasEstrategicas: [],
            indicadoresResultado: [],
            indicadoresProducto: [],
            iniciativasSGR: [],
        }
    },
    mounted() {
        this.intPageVal = this.$refs.pageType.value;

        if (this.intPageVal == 1) {
            this.get();
        } 
        else if (this.intPageVal == 2) {
            this.getSearch();
        } 
        else if (this.intPageVal == 3) {
            this.getVisualize();
        }
    },
    methods: {
        /* metodos para traer información */
        async get() {
            const formData = new FormData();
            formData.append("action","get");
            const response = await fetch(URL,{method:"POST",body:formData});
            const objData = await response.json();
            this.representante = objData.representante;
            this.periodos = objData.periodos;
        },

        async getSearch() {
            const formData = new FormData();
            formData.append("action","getSearch");
            const response = await fetch(URL,{method:"POST",body:formData});
            const objData = await response.json();
            this.pdts = this.pdtsCopy = objData.pdts;
        },

        async getVisualize() {
            
            this.id = new URLSearchParams(window.location.search).get('id');
            if (this.id > 0) {
                this.isLoading = true;
                const formData = new FormData();
                formData.append("action","getVisualize");
                formData.append("id",this.id);
                const response = await fetch(URL,{method:"POST",body:formData});
                const objData = await response.json();
                this.namePdt = objData.info.nombrePdt;
                this.representante = objData.info.representante;
                this.periodo = objData.info.periodo;
                this.fechaIni = this.formatFecha(objData.info.fechaIni);
                this.fechaFin = this.formatFecha(objData.info.fechaFin);
                this.vision = objData.info.vision;
                this.objetivos = objData.objetivos;
                this.lineasEstrategicas = objData.lineasEstrategicas;
                this.indicadoresResultado = objData.indicadoresResultado;
                this.indicadoresProducto = objData.indicadoresProducto;
                this.iniciativasSGR = objData.iniciativasSGR;
                this.presupuestoTotal = objData.presupuesto;
                this.isLoading = false;
            }
        },

        /* metodos para procesar información */
        addObjetivo() {
            this.objetivos.push(this.txtObjetivo);
            this.txtObjetivo = "";
        },

        delObjetiivo(index) {
            this.objetivos.splice(index, 1);
        },

        async searchData(option) {
            let search = "";

            switch (option) {
                case "inputSearch":
                    search = this.txtSearch.toLowerCase();
                    this.pdts = [...this.pdtsCopy.filter(e=>e.nombre_pdt.toLowerCase().includes(search) || e.nombre_mandatario.toLowerCase().includes(search))];
                    this.txtResultados = this.dataUsuariosCopy.length;
                    break;

                default:
                    alert("Opción no encontrada");
                    break;
            }    
        },

        /* Metodos para guardar o actualizar información */
        async save() {
            if (this.namePdt == "" || this.fechaIni == "" && this.fechaFin == "" && this.representante == "" || this.periodo == "" || this.vision == "" || this.objetivos.length == 0) {
                Swal.fire("Atención!","Todos los campos con * son obligatorios","warning");
                return false;
            }

            const formData = new FormData();
            formData.append("action","save");
            formData.append("namePdt", this.namePdt);
            formData.append("fechaIni", this.fechaIni);
            formData.append("fechaFin", this.fechaFin);
            formData.append("representante", this.representante);
            formData.append("periodo", this.periodo);
            formData.append("vision", this.vision);
            formData.append("objetivos", JSON.stringify(this.objetivos));

            Swal.fire({
                title:"¿Estás segur@ de guardar?",
                text:"",
                icon: 'warning',
                showCancelButton:true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText:"Sí, guardar",
                cancelButtonText:"No, cancelar"
            }).then(async function(result) {
                if(result.isConfirmed) {
                    const response = await fetch(URL,{method:"POST",body:formData});
                    const objData = await response.json();
                    if (objData.status) {
                        Swal.fire("Guardado",objData.msg,"success");
                        setTimeout(function(){
                            window.location.reload();
                        },1000);
                    }
                    else {
                        Swal.fire("Error!",objData.msg,"warning");
                    }
                }
            });
        },

        async delPdt(id) {
            if (id <= 0) {
                Swal.fire("Error!","Id no valido para eliminar","warning");
                return false;
            }

            const formData = new FormData();
            formData.append("action","delPdt");
            formData.append("id", id);

            Swal.fire({
                title:"¿Estás segur@ de eliminar?",
                text:"",
                icon: 'warning',
                showCancelButton:true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText:"Sí, eliminar",
                cancelButtonText:"No, cancelar"
            }).then(async function(result) {
                if(result.isConfirmed) {
                    const response = await fetch(URL,{method:"POST",body:formData});
                    const objData = await response.json();

                    if (objData.status) {
                        Swal.fire("Eliminado",objData.msg,"success");
                        setTimeout(function(){
                            window.location.reload();
                        },1000);
                    }
                    else {
                        Swal.fire("Error!",objData.msg,"warning");
                    }
                }
            });
        },

        /* Formatos para mostrar información */
        formatFecha: function(fecha) {
            // Separar la fecha en componentes
            const parts = fecha.split('-');
            if (parts.length !== 3) {
                throw new Error('Formato de fecha inválido. Use Y-m-d.');
            }
            const year = parts[0];
            const month = parts[1];
            const day = parts[2];
        
            // Retornar la fecha en el formato deseado
            return `${day}/${month}/${year}`;
        },

        viewFormatNumber: function(valor){
            return new Intl.NumberFormat("en-US", {style: "currency", currency: "USD"}).format(valor);
            
        },
    },
    computed:{

    }
})
