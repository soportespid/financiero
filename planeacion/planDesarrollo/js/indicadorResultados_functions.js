const URL = "planeacion/planDesarrollo/controllers/indicadorResultados_controller.php";

var app = new Vue({
    el:"#myapp",
    components: { Multiselect: window.VueMultiselect.default },
    data() {
        return {
            isLoading: false,
            dataSearch: [],
            dataSearchCopy: [],
            txtSearch: '',
            id: 0, 
            arrConsecutivos: [],
            modalIndicadores: false,
            indicadores: [],
            indicadoresCopy: [],
            indicadorId: 0,
            codIndicador: '',
            nombreIndicador: '',
            unidadMedida: '',
            metaCuatrienio: '',
            ejesEstrategicos: [],
            ejesSelected: [],
            planes: [],
            pdtId: '',
            lineaBase: '',
        }
    },
    mounted() {
        this.intPageVal = this.$refs.pageType.value;

        if (this.intPageVal == 1) {
            this.get();
        } else if (this.intPageVal == 2) {
            this.getSearch();
        } else if (this.intPageVal == 3) {
            this.getEdit();
        }
    },
    methods: {
        /* metodos para traer información */
        async get() {
            const formData = new FormData();
            formData.append("action","get");
            this.isLoading = true;
            const response = await fetch(URL,{method:"POST",body:formData});
            const objData = await response.json();
            this.isLoading = false;
            this.indicadores = this.indicadoresCopy = objData.indicadores;
            this.planes = objData.planes;
        },

        async getSearch() {
            const formData = new FormData();
            formData.append("action","getSearch");
            const response = await fetch(URL,{method:"POST",body:formData});
            const objData = await response.json();
            this.dataSearch = this.dataSearchCopy = objData.data;
        },

        async getEdit() {
            this.id = new URLSearchParams(window.location.search).get('id');
            if (this.id <= 0) return false;

            const formData = new FormData();
            formData.append("action","getEdit");
            formData.append("id", this.id);
            this.isLoading = true;
            const response = await fetch(URL,{method:"POST",body:formData});
            const objData = await response.json();
            this.isLoading = false;
            this.indicadores = this.indicadoresCopy = objData.indicadores;
            this.ejesEstrategicos = objData.ejes;
            this.ejesSelected = objData.selectEjes;
            this.planes = objData.planes;
            this.indicadorId = objData.data.codigo_indicador_id;
            this.arrConsecutivos = objData.consecutivos;
            this.codIndicador = objData.data.codigo_indicador;
            this.nombreIndicador = objData.data.nombre_indicador;
            this.unidadMedida = objData.data.unidad_medida;
            this.metaCuatrienio = objData.data.meta_cuatrienio;
            this.pdtId = objData.data.pdt_id;
            this.lineaBase = objData.data.linea_base;
        },
        
        async searchEjes() {
            this.ejesSelected = this.ejesEstrategicos = [];
            if (this.pdtId == "") {
                return false;
            }

            const formData = new FormData();
            formData.append("action","getEjes");
            formData.append("pdtId",this.pdtId);
            const response = await fetch(URL,{method:"POST",body:formData});
            const objData = await response.json();
            this.ejesEstrategicos = objData.ejes;
        },

        /* metodos para procesar información */
        async searchData(option) {
            let search = "";

            switch (option) {
                case "modalIndicador":
                    search = this.txtSearch.toLowerCase();
                    this.indicadores = [...this.indicadoresCopy.filter(e=>e.codigo_indicador.toLowerCase().includes(search) || e.nombre_indicador.toLowerCase().includes(search) )];
                    this.txtResultados = this.indicadores.length;
                    break;
                
                case "inputIndicador":
                    search = this.txtSearch.toLowerCase();
                    this.dataSearch = [...this.dataSearchCopy.filter(e=>e.codigo_indicador.toLowerCase().includes(search) || e.nombre_indicador.toLowerCase().includes(search) || e.id.toLowerCase().includes(search))];
                    break;

                default:
                    console.log("Error, opción no encontrada");
                    break;
            }    
        },

        selectItem(option, item) {
            switch (option) {
                case "selectIndicador":
                        this.codIndicador = item.codigo_indicador;
                        this.nombreIndicador = item.nombre_indicador;
                        this.unidadMedida = item.unidad_medida;
                        this.indicadorId = item.id;
                        this.modalIndicadores = false;
                        this.indicadores = this.indicadoresCopy;
                        this.txtSearch = "";
                    break;
            
                default:
                    console.log("Error, opción no encontrada");
                    break;
            }
        },
        
        nextItem:function(type){
            let id = this.id;
            let index = this.arrConsecutivos.findIndex(function(e){return e.id == id});
            if(type=="next" && this.arrConsecutivos[++index]){
                id = this.arrConsecutivos[index++].id;
            }else if(type=="prev" && this.arrConsecutivos[--index]){
                id = this.arrConsecutivos[index--].id;
            }
            window.location.href="plan-editaIndicadorResultado"+'?id='+id;
        },

        /* Metodos para guardar o actualizar información */
        async save() {
            if (this.codIndicador == "" || this.nombreIndicador == "" || this.unidadMedida == "") {
                Swal.fire("Atención!","Debes seleccionar un código indicador","warning");
                return false;
            }

            if (this.metaCuatrienio == "" || this.metaCuatrienio == 0) {
                Swal.fire("Atención!","Debes colocar una meta cuatrienio","warning");
                return false;
            }

            if (this.pdtId == "" || this.lineaBase == "") {
                Swal.fire("Atención!","Todos los campos con (*) son obligatorios","warning");
                return false;
            }

            if (this.ejesSelected.length == 0) {
                Swal.fire("Atención!","Debes seleccionar al menos un eje estrategico","warning");
                return false;
            }

            const formData = new FormData();
            formData.append("action","save");
            formData.append("indicadorId", this.indicadorId);
            formData.append("metaCuatrienio", this.metaCuatrienio);
            formData.append("pdtId", this.pdtId);
            formData.append("lineaBase", this.lineaBase);
            formData.append("ejes", JSON.stringify(this.ejesSelected));

            Swal.fire({
                title:"¿Estás segur@ de guardar?",
                text:"",
                icon: 'warning',
                showCancelButton:true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText:"Sí, guardar",
                cancelButtonText:"No, cancelar"
            }).then(async function(result) {
                if(result.isConfirmed) {
                    const response = await fetch(URL,{method:"POST",body:formData});
                    const objData = await response.json();

                    if (objData.status) {
                        Swal.fire("Guardado",objData.msg,"success");
                        if(objData.id){
                            setTimeout(function(){
                                window.location.href="plan-editaIndicadorResultado"+'?id='+objData.id;
                            },1500);
                        }
                    }
                    else {
                        Swal.fire("Error!",objData.msg,"warning");
                    }
                }
            });
        },

        async delIndicador(id) {
            if (id <= 0) {
                Swal.fire("Atención!","Indicador no valido para eliminar","warning");
                return false;
            }

            const formData = new FormData();
            formData.append("action","delIndicador");
            formData.append("indResultadoId", id);

            Swal.fire({
                title:"¿Estás segur@ de eliminar?",
                text:"",
                icon: 'warning',
                showCancelButton:true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText:"Sí, eliminar",
                cancelButtonText:"No, cancelar"
            }).then(async function(result) {
                if(result.isConfirmed) {
                    const response = await fetch(URL,{method:"POST",body:formData});
                    const objData = await response.json();

                    if (objData.status) {
                        Swal.fire("Eliminado",objData.msg,"success");
                        setTimeout(function(){
                            window.location.reload();
                        },800);
                    }
                    else {
                        Swal.fire("Error!",objData.msg,"warning");
                    }
                }
            });
        },

        async saveEdit() {
            if (this.codIndicador == "" || this.nombreIndicador == "" || this.unidadMedida == "") {
                Swal.fire("Atención!","Debes seleccionar un código indicador","warning");
                return false;
            }

            if (this.metaCuatrienio == "" || this.metaCuatrienio == 0) {
                Swal.fire("Atención!","Debes colocar una meta cuatrienio","warning");
                return false;
            }

            if (this.ejesSelected.length == 0) {
                Swal.fire("Atención!","Debes seleccionar al menos un eje estrategico","warning");
                return false;
            }

            const formData = new FormData();
            formData.append("action","saveEdit");
            formData.append("id", this.id);
            formData.append("indicadorId", this.indicadorId);
            formData.append("metaCuatrienio", this.metaCuatrienio);
            formData.append("ejes", JSON.stringify(this.ejesSelected));

            Swal.fire({
                title:"¿Estás segur@ de guardar?",
                text:"",
                icon: 'warning',
                showCancelButton:true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText:"Sí, guardar",
                cancelButtonText:"No, cancelar"
            }).then(async function(result) {
                if(result.isConfirmed) {
                    const response = await fetch(URL,{method:"POST",body:formData});
                    const objData = await response.json();

                    if (objData.status) {
                        Swal.fire("Guardado",objData.msg,"success");
                    }
                    else {
                        Swal.fire("Error!",objData.msg,"warning");
                    }
                }
            });
        },

        /* Formatos para mostrar información */
    },
    computed:{

    }
})
