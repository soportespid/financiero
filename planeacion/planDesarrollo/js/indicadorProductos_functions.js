const URL = "planeacion/planDesarrollo/controllers/indicadorProductos_controller.php";
const URLEXCEL = "plan-indicadorProductoExcel.php";
var app = new Vue({
    el:"#myapp",
    components: { Multiselect: window.VueMultiselect.default },
    data() {
        return {
            isLoading: false,
            dataSearch: [],
            dataSearchCopy: [],
            txtSearch: '',
            id: 0, 
            arrConsecutivos: [],
            modalIndicadores: false,
            modalFuncionarios: false,
            planes: [],
            pdtId: '',
            indicadores: [], indicadoresCopy: [],
            funcionarios: [], funcionariosCopy: [],
            indicadorId: 0,
            codIndicador: '',
            nombreIndicador: '',
            unidadMedida: '',
            metaCuatrienio: '',
            valor: '',
            valorFormat: '',
            terceroId: '',
            documento: '',
            funcionario: '',
            nombrePersonalizado: '',
            ejesEstrategicos: [],
            ejesSelected: [],
            lineaBase: '',

            //sectores
            modalSectores: false,
            sectores: [],
            sectoresCopy: [],
            codSector: '',
            nomSector: '',

            //programa
            modalPrograma: false,
            programas: [],
            programasCopy: [],
            codPrograma: '',
            nomPrograma: '',

            //producto
            modalProducto: false,
            productos: [],
            productosCopy: [],
            codProducto: '',
            nomProducto: '',

            //indicadores resultado
            modalResultado: false,
            indicadoresResultados: [],
            indicadoresResultadosCopy: [],
            indicadorResultadoId: '',
            codResultado: '',
            nomResultado: '',
        }
    },
    mounted() {
        this.intPageVal = this.$refs.pageType.value;

        if (this.intPageVal == 1) {
            this.get();
        } else if (this.intPageVal == 2) {
            this.getSearch();
        } else if (this.intPageVal == 3) {
            this.getEdit();
        }
    },
    methods: {
        /* metodos para traer información */
        async get() {
            const formData = new FormData();
            formData.append("action","get");
            this.isLoading = true;
            const response = await fetch(URL,{method:"POST",body:formData});
            const objData = await response.json();
            this.sectores = this.sectoresCopy = objData.sectores;
            this.funcionarios = this.funcionariosCopy = objData.funcionarios;
            // this.ejesEstrategicos = objData.ejes;
            // this.indicadoresResultados = this.indicadoresResultadosCopy = objData.indicadoresResultado;
            this.planes = objData.planes;
            this.isLoading = false;
        },

        async getSearch() {
            const formData = new FormData();
            formData.append("action","getSearch");
            const response = await fetch(URL,{method:"POST",body:formData});
            const objData = await response.json();
            this.dataSearch = this.dataSearchCopy = objData.data;
        },

        async getEdit() {
            this.id = new URLSearchParams(window.location.search).get('id');
            if (this.id <= 0) return false;

            const formData = new FormData();
            formData.append("action","getEdit");
            formData.append("id", this.id);
            this.isLoading = true;
            const response = await fetch(URL,{method:"POST",body:formData});
            const objData = await response.json();
            this.indicadores = this.indicadoresCopy = objData.indicadores;
            this.funcionarios = this.funcionariosCopy = objData.funcionarios;
            this.ejesEstrategicos = objData.ejes;
            this.indicadoresResultados = this.indicadoresResultadosCopy = objData.indicadoresResultado;
            this.planes = objData.planes;
            this.arrConsecutivos = objData.consecutivos;
            this.sectores = this.sectoresCopy = objData.sectores;
            this.ejesSelected = objData.ejesSelected;
            this.pdtId = objData.data.pdt_id;
            this.indicadorId = objData.data.codigo_producto_id;
            this.codIndicador = objData.data.codigo_indicador;
            this.nombreIndicador = objData.data.nombre_indicador;
            this.unidadMedida = objData.data.unidad_medida;
            this.nombrePersonalizado = objData.data.nombre_personalizado;
            this.indicadorResultadoId = objData.data.indicador_resultado_id;
            this.valorFormat = this.formatNumber(objData.data.valor_cuatrienio);
            this.valor = objData.data.valor_cuatrienio;
            this.metaCuatrienio = objData.data.meta_cuatrienio;
            this.lineaBase = objData.data.linea_base;
            this.terceroId = objData.data.tercero_id;
            this.documento = objData.data.cedulanit;
            this.funcionario = objData.data.funcionario;
            this.codResultado = objData.data.cod_ind_resultado;
            this.nomResultado = objData.data.nom_ind_resultado;
            this.indicadorResultadoId = objData.data.indicador_resultado_id;
            this.codSector = objData.sector.codigo;
            this.nomSector = objData.sector.nombre;
            this.codPrograma = objData.programa.codigo;
            this.nomPrograma = objData.programa.nombre;
            this.codProducto = objData.data.cod_producto;
            this.nomProducto = objData.data.nom_producto;
            this.isLoading = false;
        },

        async changeDataPdt() {
            this.ejesSelected = this.ejesEstrategicos = this.indicadoresResultados = this.indicadoresResultadosCopy = [];
            this.indicadorResultadoId = this.codResultado = this.nomResultado = "";
            if (this.pdtId == "") {
                return false;
            }

            const formData = new FormData();
            formData.append("action","getDataPdt");
            formData.append("pdtId", this.pdtId);
            const response = await fetch(URL,{method:"POST",body:formData});
            const objData = await response.json();

            this.ejesEstrategicos = objData.ejes;
            this.indicadoresResultados = this.indicadoresResultadosCopy = objData.indicadoresResultado;
        },

        // /* metodos para procesar información */
        async deployModal(option) {
            const formData = new FormData();

            if (option == "sector") {
                this.modalSectores = true;
            } 
            else if (option == "programa") {
                if (this.codSector == "") {
                    Swal.fire("Atención!","Debes seleccionar primero el sector","warning");
                    return false;
                }
                formData.append("action","programa");
                formData.append("codSector",this.codSector);
            } 
            else if (option == "producto") {
                if (this.codPrograma == "") {
                    Swal.fire("Atención!","Debes seleccionar primero el programa","warning");
                    return false;
                }
                formData.append("action","producto");
                formData.append("codPrograma",this.codPrograma);
            } 
            else if (option == "indicador") {
                if (this.codProducto == "") {
                    Swal.fire("Atención!","Debes seleccionar primero el producto","warning");
                    return false;
                }
                formData.append("action","indicador");
                formData.append("codProducto",this.codProducto);
            }
            else {
                console.log("Error, opción no encontrada");
            }

            const response = await fetch(URL,{method:"POST",body:formData});
            const objData = await response.json();

            this.programas = this.programasCopy = objData.programas != null ? objData.programas : [];
            this.modalPrograma = objData.programas != null ? true : false;
            this.productos = this.productosCopy = objData.productos != null ? objData.productos : [];
            this.modalProducto = objData.productos != null ? true : false;
            this.indicadores = this.indicadoresCopy = objData.indicador != null ? objData.indicador : [];
            this.modalIndicadores = objData.indicador != null ? true : false;
        },

        searchData(option) {
            let search = "";

            switch (option) {
                case "modalSector":
                    search = this.txtSearch.toLowerCase();
                    this.sectores = [...this.sectoresCopy.filter(e=>e.codigo.toLowerCase().includes(search) || e.nombre.toLowerCase().includes(search) )];
                    break;

                case "modalPrograma":
                    search = this.txtSearch.toLowerCase();
                    this.programas = [...this.programasCopy.filter(e=>e.codigo.toLowerCase().includes(search) || e.nombre.toLowerCase().includes(search) )];
                    break;

                case "modalProducto":
                    search = this.txtSearch.toLowerCase();
                    this.productos = [...this.productosCopy.filter(e=>e.codigo.toLowerCase().includes(search) || e.nombre.toLowerCase().includes(search) )];
                    break;

                case "modalIndicador":
                    search = this.txtSearch.toLowerCase();
                    this.indicadores = [...this.indicadoresCopy.filter(e=>e.codigo_indicador.toLowerCase().includes(search) || e.nombre_indicador.toLowerCase().includes(search) )];
                    break;
                
                case "inputIndicador":
                    search = this.txtSearch.toLowerCase();
                    this.dataSearch = [...this.dataSearchCopy.filter(e=>e.codigo_indicador.toLowerCase().includes(search) || e.nombre_indicador.toLowerCase().includes(search) || e.id.toLowerCase().includes(search) || e.nombre_personalizado.toLowerCase().includes(search))];
                    break;

                case "modalFuncionario":
                    search = this.txtSearch.toLowerCase();
                    this.funcionarios = [...this.funcionariosCopy.filter(e=>e.cedulanit.toLowerCase().includes(search) || e.nombre.toLowerCase().includes(search) || e.nombre_cargo.toLowerCase().includes(search) )];
                    break;

                case "modalResultado":
                    search = this.txtSearch.toLowerCase();
                    this.indicadoresResultados = [...this.indicadoresResultadosCopy.filter(e=>e.codigo_indicador.toLowerCase().includes(search) || e.nombre_indicador.toLowerCase().includes(search))];
                    break;

                default:
                    console.log("Error, opción no encontrada");
                    break;
            }    
        },

        selectItem(option, item) {
            switch (option) {
                case "selectSector":
                    this.codPrograma = this.nomPrograma = this.codProducto = this.nomProducto = this.codIndicador = this.nombreIndicador = this.unidadMedida = this.indicadorId = "";
                    this.codSector = item.codigo;
                    this.nomSector = item.nombre;
                    this.modalSectores = false;
                    this.sectores = this.sectoresCopy;
                    this.txtSearch = "";
                    break;

                case "selectPrograma":
                    this.codProducto = this.nomProducto = this.codIndicador = this.nombreIndicador = this.unidadMedida = this.indicadorId = "";
                    this.codPrograma = item.codigo;
                    this.nomPrograma = item.nombre;
                    this.modalPrograma = false;
                    this.programas = this.programasCopy;
                    this.txtSearch = "";
                    break;

                case "selectProducto":
                    this.codIndicador = this.nombreIndicador = this.unidadMedida = this.indicadorId = "";
                    this.codProducto = item.codigo;
                    this.nomProducto = item.nombre;
                    this.modalProducto = false;
                    this.productos = this.productosCopy;
                    this.txtSearch = "";
                    break;

                case "selectIndicador":
                        this.codIndicador = item.codigo_indicador;
                        this.nombreIndicador = item.nombre_indicador;
                        this.unidadMedida = item.unidad_medida;
                        this.indicadorId = item.id;
                        this.modalIndicadores = false;
                        this.indicadores = this.indicadoresCopy;
                        this.txtSearch = "";
                    break;

                case "selectFuncionario":
                    this.terceroId = item.tercero_id;
                    this.documento = item.cedulanit;
                    this.funcionario = item.nombre;
                    this.modalFuncionarios = false;
                    this.funcionarios = this.funcionariosCopy;
                    this.txtSearch = "";
                    break;

                case "selectResultado":
                    this.indicadorResultadoId = item.id;
                    this.codResultado = item.codigo_indicador;
                    this.nomResultado = item.nombre_indicador;
                    this.modalResultado = false;
                    console.log(this.indicadoresResultados);
                    this.indicadoresResultados = this.indicadoresResultadosCopy;
                    this.txtSearch = "";
                    break;
            
                default:
                    console.log("Error, opción no encontrada");
                    break;
            }
        },
        
        nextItem:function(type){
            let id = this.id;
            let index = this.arrConsecutivos.findIndex(function(e){return e.id == id});
            if(type=="next" && this.arrConsecutivos[++index]){
                id = this.arrConsecutivos[index++].id;
            }else if(type=="prev" && this.arrConsecutivos[--index]){
                id = this.arrConsecutivos[index--].id;
            }
            window.location.href="plan-editaIndicadorProducto"+'?id='+id;
        },

        // /* Metodos para guardar o actualizar información */
        async save() {
            if (this.pdtId == "" || this.indicadorId == "" || this.codIndicador == "" || this.nombreIndicador == "" || this.unidadMedida == "" || this.nombrePersonalizado == "") {
                Swal.fire("Atención!","Todos los campos con (*) son obligatorios","warning");
                return false;
            }

            if (this.indicadorResultadoId == "" || this.valor == "" || this.metaCuatrienio == "" || this.ejesSelected.length == 0 || this.documento == "" || this.funcionario == "" || this.terceroId == "" || this.lineaBase == "") {
                Swal.fire("Atención!","Todos los campos con (*) son obligatorios","warning");
                return false;
            }

            const formData = new FormData();
            formData.append("action","save");
            formData.append("pdtId", this.pdtId);
            formData.append("indicadorId", this.indicadorId);
            formData.append("nombrePersonalizado", this.nombrePersonalizado);
            formData.append("valor", this.valor);
            formData.append("metaCuatrienio", this.metaCuatrienio);
            formData.append("indicadorResultadoId", this.indicadorResultadoId);
            formData.append("terceroId", this.terceroId);
            formData.append("lineaBase", this.lineaBase);
            formData.append("ejes", JSON.stringify(this.ejesSelected));

            Swal.fire({
                title:"¿Estás segur@ de guardar?",
                text:"",
                icon: 'warning',
                showCancelButton:true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText:"Sí, guardar",
                cancelButtonText:"No, cancelar"
            }).then(async function(result) {
                if(result.isConfirmed) {
                    const response = await fetch(URL,{method:"POST",body:formData});
                    const objData = await response.json();

                    if (objData.status) {
                        Swal.fire("Guardado",objData.msg,"success");
                        if(objData.id){
                            setTimeout(function(){
                                window.location.href="plan-editaIndicadorProducto"+'?id='+objData.id;
                            },1500);
                        }
                    }
                    else {
                        Swal.fire("Error!",objData.msg,"warning");
                    }
                }
            });
        },

        async saveEdit() {
            if (this.pdtId == "" || this.indicadorId == "" || this.codIndicador == "" || this.nombreIndicador == "" || this.unidadMedida == "" || this.nombrePersonalizado == "" || this.id <= 0) {
                Swal.fire("Atención!","Todos los campos con (*) son obligatorios","warning");
                return false;
            }

            if (this.indicadorResultadoId == "" || this.valor == "" || this.metaCuatrienio == "" || this.ejesSelected.length == 0 || this.documento == "" || this.funcionario == "" || this.terceroId == "" || this.lineaBase == "") {
                Swal.fire("Atención!","Todos los campos con (*) son obligatorios","warning");
                return false;
            }

            const formData = new FormData();
            formData.append("action","saveEdit");
            formData.append("id",this.id);
            formData.append("pdtId", this.pdtId);
            formData.append("indicadorId", this.indicadorId);
            formData.append("nombrePersonalizado", this.nombrePersonalizado);
            formData.append("valor", this.valor);
            formData.append("metaCuatrienio", this.metaCuatrienio);
            formData.append("indicadorResultadoId", this.indicadorResultadoId);
            formData.append("terceroId", this.terceroId);
            formData.append("lineaBase", this.lineaBase);
            formData.append("ejes", JSON.stringify(this.ejesSelected));

            Swal.fire({
                title:"¿Estás segur@ de guardar?",
                text:"",
                icon: 'warning',
                showCancelButton:true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText:"Sí, guardar",
                cancelButtonText:"No, cancelar"
            }).then(async function(result) {
                if(result.isConfirmed) {
                    const response = await fetch(URL,{method:"POST",body:formData});
                    const objData = await response.json();

                    if (objData.status) {
                        Swal.fire("Guardado",objData.msg,"success");
                    }
                    else {
                        Swal.fire("Error!",objData.msg,"warning");
                    }
                }
            });
        },

        /* Formatos para mostrar información */
        formatInputNumber(conFormato, sinFormato) {
            this[conFormato] = this.formatNumber(this[conFormato]);
            this[sinFormato] = this[conFormato].replace(/[^0-9.]/g, "");
        },

        formatNumber(number) {
            if (!number) return ""; // Si no hay entrada, retorna vacío
    
            // Elimina caracteres no válidos (excepto números y el punto decimal)
            number = number.replace(/[^0-9.]/g, "");
    
            // Divide la parte entera y decimal
            const [integerPart, decimalPart] = number.split(".");
    
            // Formatear la parte entera con separador de miles
            const formattedInteger = integerPart.replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    
            // Si hay parte decimal, la une; si no, devuelve solo la parte entera
            return decimalPart !== undefined
                ? `${formattedInteger}.${decimalPart.slice(0, 2)}` // Limita a 2 decimales si es necesario
                : formattedInteger;
        },

        viewFormatNumber: function(valor){
            return new Intl.NumberFormat("en-US", {style: "currency", currency: "USD"}).format(valor);
            
        },

        /* enviar informacion excel o pdf */

        excel() {
            const form = document.createElement("form");
            form.method = "post";
            form.target = "_blank";
            form.action = URLEXCEL;       
            
            document.body.appendChild(form);
            form.submit();
            document.body.removeChild(form);
        }
    },
    computed:{

    }
})
