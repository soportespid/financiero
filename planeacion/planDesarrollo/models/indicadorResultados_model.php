<?php
    require_once '../../../Librerias/core/Helpers.php';
    require_once '../../../Librerias/core/Mysql.php';
    session_start();
    
    class resultadoModel extends Mysql {
        private $cedula;
        private $vigencia;

        function __construct(){
            parent::__construct();
            $this->cedula = $_SESSION["cedulausu"];
            $this->vigencia = getVigenciaUsuario($this->cedula);
        }

        public function planesDesarrolloTerritorial() {
            $sql_periodos = "SELECT id AS periodoId FROM para_periodos_gobierno WHERE vigencia_inicial <= '$this->vigencia' AND vigencia_final >= '$this->vigencia' AND estado = 'S'";
            $result = $this->select($sql_periodos);

            $sql = "SELECT id, nombre_pdt FROM plan_pdt WHERE periodo_gobierno_id = $result[periodoId] AND estado = 'S'";
            $data = $this->select_all($sql);
            return $data;
        }

        public function getIndicadoresResultado() {
            $sql = "SELECT id, codigo_indicador, nombre_indicador, unidad_medida, fuente, dato_numerico, vigencia_mes, pnd, transformacion FROM plan_catalogo_indicadores_resultado ORDER BY id";
            $data = $this->select_all($sql);
            
            return $data;
        }

        public function getEjesEstrategicos(int $pdtId) {
            $sql = "SELECT id, nombre FROM plan_eje_estrategico WHERE pdt_id = $pdtId AND estado = 'S'";
            $data = $this->select_all($sql);
            return $data;
        }

        public function insertIndicador(int $indicadorId, float $cuatrienio, int $pdtId, float $lineaBase) {
            $sql = "INSERT INTO plan_indicador_resultado (pdt_id, codigo_resultado_id, meta_cuatrienio, linea_base, estado) VALUES (?, ?, ?, ?, ?)";
            $data = [$pdtId, $indicadorId, $cuatrienio, $lineaBase, "S"];
            $result = $this->insert($sql, $data);
            return $result;
        }

        public function insertIndicadorEjes(int $indicadorResultadoId, array $ejes) {
            $sql = "INSERT INTO plan_indicador_resultado_det (indicador_resultado_id, eje_estrategico_id) VALUES (?, ?)";
            foreach ($ejes as $eje) {
                $data = [$indicadorResultadoId, $eje["id"]];
                $this->insert($sql, $data);
            }
            return true;
        }

        public function getIndicadores() {
            $sql = "SELECT IR.id, CIR.codigo_indicador, CIR.nombre_indicador, CIR.unidad_medida, IR.meta_cuatrienio, pdt.nombre_pdt
            FROM plan_indicador_resultado AS IR 
            INNER JOIN plan_catalogo_indicadores_resultado AS CIR
            ON IR.codigo_resultado_id = CIR.id
            INNER JOIN plan_pdt AS pdt
            ON IR.pdt_id = pdt.id
            WHERE IR.estado = 'S' ORDER BY IR.id";
            $data = $this->select_all($sql);
            return $data;
        }

        public function changeStatusIndResultado(int $id) {
            $sql = "UPDATE plan_indicador_resultado SET estado = ? WHERE id = $id";
            $resp = $this->update($sql, ["N"]);
            return $resp;
        }

        public function getEditIndicador(int $id) {
            $sql = "SELECT IR.codigo_resultado_id, CIR.codigo_indicador, CIR.nombre_indicador, CIR.unidad_medida, IR.meta_cuatrienio, IR.pdt_id, IR.linea_base 
            FROM plan_indicador_resultado AS IR 
            INNER JOIN plan_catalogo_indicadores_resultado AS CIR
            ON IR.codigo_resultado_id = CIR.id
            WHERE IR.id = $id";
            $data = $this->select($sql);
            return $data;
        }

        public function getEditEjes(int $id) {
            $sql = "SELECT IRD.eje_estrategico_id AS id, PEE.nombre 
            FROM plan_indicador_resultado_det AS IRD 
            INNER JOIN plan_eje_estrategico AS PEE 
            ON IRD.eje_estrategico_id = PEE.id 
            WHERE IRD.indicador_resultado_id = $id";
            $data = $this->select_all($sql);
            return $data;
        }
        
        public function searchConsecs() {
            $sql = "SELECT id FROM plan_indicador_resultado WHERE estado = 'S'";
            $data = $this->select_all($sql);
            return $data;
        }

        public function updateIndicador(int $indicadorId, float $cuatrienio, int $id) {
            $sql = "UPDATE plan_indicador_resultado SET codigo_resultado_id = ?, meta_cuatrienio = ? WHERE id = $id";
            $resp = $this->update($sql, [$indicadorId, $cuatrienio]);
            return $resp;
        }

        public function updateEjes(int $id, array $ejes) {
            $sql = "DELETE FROM plan_indicador_resultado_det WHERE indicador_resultado_id = $id";
            $this->delete($sql);

            $sql = "INSERT INTO plan_indicador_resultado_det (indicador_resultado_id, eje_estrategico_id) VALUES (?, ?)";
            foreach ($ejes as $eje) {
                $data = [$id, $eje["id"]];
                $this->insert($sql, $data);
            }
            return true;
        }
    }
?>