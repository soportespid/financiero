<?php
    require_once '../../../Librerias/core/Helpers.php';
    require_once '../../../Librerias/core/Mysql.php';
    session_start();
    
    class isgrModel extends Mysql {
        private $cedula;
        private $vigencia;

        function __construct(){
            parent::__construct();
            $this->cedula = $_SESSION["cedulausu"];
            $this->vigencia = getVigenciaUsuario($this->cedula);
        }

        public function planesDesarrolloTerritorial() {
            $sql_periodos = "SELECT id AS periodoId FROM para_periodos_gobierno WHERE vigencia_inicial <= '$this->vigencia' AND vigencia_final >= '$this->vigencia' AND estado = 'S'";
            $result = $this->select($sql_periodos);

            $sql = "SELECT id, nombre_pdt FROM plan_pdt WHERE periodo_gobierno_id = $result[periodoId] AND estado = 'S'";
            $data = $this->select_all($sql);
            return $data;
        }

        public function getEjesEstrategicos(int $pdtId) {
            $sql = "SELECT id, nombre FROM plan_eje_estrategico WHERE pdt_id = $pdtId AND estado = 'S'";
            $data = $this->select_all($sql);
            return $data;
        }

        public function funcionarios() {
            $sql = "SELECT
            T.id_tercero AS tercero_id,
            PT.cedulanit,
            CONCAT(T.nombre1, ' ', T.nombre2, ' ', T.apellido1, ' ', T.apellido2) AS nombre,
            UPPER(PC.nombrecargo) AS nombre_cargo
            FROM planestructura_terceros AS PT
            INNER JOIN planaccargos AS PC
            ON PT.codcargo = PC.codcargo
            INNER JOIN terceros AS T
            ON PT.cedulanit = T.cedulanit
            WHERE PT.estado = 'S'";
            $data = $this->select_all($sql);
            foreach ($data as $key => $d) {
                $data[$key]["nombre"] = preg_replace("/\s+/", " ", trim($d["nombre"]));
            }
            return $data;
        }

        public function sectores() {
            $sql = "SELECT id, codigo, nombre FROM ccpetsectores ORDER BY codigo";
            $data = $this->select_all($sql);
            return $data;
        }

        public function searchConsecs() {
            $sql = "SELECT id FROM plan_indicador_producto WHERE estado = 'S'";
            $data = $this->select_all($sql);
            return $data;
        }

        public function searchBpin(string $bpin) {
            $sql = "SELECT COUNT(*) AS cantidad FROM ccpproyectospresupuesto WHERE codigo = '$bpin'";
            $data = $this->select($sql);
            return $data["cantidad"];
        }

        public function insertSgr(array $data) {
            $sql = "INSERT INTO plan_isgr (pdt_id, nombre, tipo, sector_id, recurso_indicativo, tercero_id, estado) VALUES (?, ?, ?, ?, ?, ?, ?)";
            $result = $this->insert($sql, $data);
            return $result;
        }

        public function insertEjes(int $isgrId, array $data) {
            $sql_delete = "DELETE FROM plan_isgr_det WHERE isgr_id = $isgrId";
            $this->delete($sql_delete);

            $sql = "INSERT INTO plan_isgr_det (isgr_id, eje_estrategico_id) VALUES (?, ?)";
            foreach ($data as $eje) {
                $this->insert($sql, [$isgrId, $eje["id"]]);
            }
            return true;
        }

        public function insertBpins(int $isgrId, array $data) {
            $sql_delete = "DELETE FROM plan_isgr_bpin WHERE isgr_id = $isgrId";
            $this->delete($sql_delete);

            $sql = "INSERT INTO plan_isgr_bpin (isgr_id, bpin) VALUES (?, ?)";
            foreach ($data as $bpin) {
                $this->insert($sql, [$isgrId, $bpin["bpin"]]);
            }
            return true;
        }

        public function getDataSearch() {
            $sql = "SELECT
            PI.id,
            PP.nombre_pdt,
            PI.nombre,
            PI.tipo,
            CS.nombre AS nombre_sector,
            PI.recurso_indicativo,
            CONCAT(T.nombre1, ' ', T.nombre2, ' ', T.apellido1, ' ', T.apellido2) AS funcionario
            FROM plan_isgr AS PI
            INNER JOIN ccpetsectores AS CS
            ON PI.sector_id = CS.id
            INNER JOIN plan_pdt AS PP
            ON PI.pdt_id = PP.id
            INNER JOIN terceros AS T
            ON PI.tercero_id = T.id_tercero
            ORDER BY PI.id";
            $data = $this->select_all($sql);
            foreach ($data as $key => $d) {
                $data[$key]["funcionario"] = preg_replace("/\s+/", " ", trim($d["funcionario"]));
            }
            return $data;
        }

        public function getConsecs() {
            $sql = "SELECT id FROM plan_isgr WHERE estado = 'S'";
            $data = $this->select_all($sql);
            return $data;
        }

        public function getDataEdit(int $id) {
            $sql = "SELECT
            PI.id,
            PI.pdt_id,
            PI.nombre,
            PI.tipo,
            PI.sector_id,
            CS.codigo AS codigo_sector,
            CS.nombre AS nombre_sector,
            PI.recurso_indicativo,
            PI.tercero_id,
            CONCAT(T.nombre1, ' ', T.nombre2, ' ', T.apellido1, ' ', T.apellido2) AS funcionario,
            T.cedulanit AS documento_funcionario
            FROM plan_isgr AS PI
            INNER JOIN ccpetsectores AS CS
            ON PI.sector_id = CS.id
            INNER JOIN terceros AS T
            ON PI.tercero_id = T.id_tercero
            WHERE PI.id = $id
            ORDER BY PI.id";
            $data = $this->select($sql);
            foreach ($data as $key => $d) {
                $data[$key]["funcionario"] = preg_replace("/\s+/", " ", trim($d["funcionario"]));
            }
            return $data;
        }

        public function getDatEjes(int $id) {
            $sql = "SELECT
            PID.eje_estrategico_id AS id,
            PEE.nombre
            FROM plan_isgr_det AS PID
            INNER JOIN plan_eje_estrategico AS PEE
            ON PID.eje_estrategico_id = PEE.id
            WHERE PID.isgr_id = $id";
            $data = $this->select_all($sql);
            return $data;
        }

        public function getDataBpin(int $id) {
            $sql = "SELECT bpin FROM plan_isgr_bpin WHERE isgr_id = $id";
            $data = $this->select_all($sql);
            return $data;
        }

        public function updateSgr(int $id, array $data) {
            $sql = "UPDATE plan_isgr SET pdt_id = ?, nombre = ?, tipo = ?, sector_id = ?, recurso_indicativo = ?, tercero_id = ? WHERE id = $id";
            $result = $this->update($sql, $data);
            return $result;
        }
    }
?>