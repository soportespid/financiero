<?php
    require_once '../../../Librerias/core/Helpers.php';
    require_once '../../../Librerias/core/Mysql.php';
    session_start();
    
    class productoModel extends Mysql{
        private $cedula;
        private $vigencia;

        function __construct(){
            parent::__construct();
            $this->cedula = $_SESSION["cedulausu"];
            $this->vigencia = getVigenciaUsuario($this->cedula);
        }

        public function planesDesarrolloTerritorial() {
            $sql_periodos = "SELECT id AS periodoId FROM para_periodos_gobierno WHERE vigencia_inicial <= '$this->vigencia' AND vigencia_final >= '$this->vigencia' AND estado = 'S'";
            $result = $this->select($sql_periodos);

            $sql = "SELECT id, nombre_pdt FROM plan_pdt WHERE periodo_gobierno_id = $result[periodoId] AND estado = 'S'";
            $data = $this->select_all($sql);
            return $data;
        }

        public function validaIndicadorProducto($codIndicador) {
            $sql = "SELECT COUNT(*) AS total_row FROM ccpetproductos WHERE codigo_indicador = $codIndicador";
            $value = $this->select($sql);
            return $value["total_row"];
        }

        public function validaEjeEstrategico($id) {            
            $sql = "SELECT COUNT(*) AS total_row FROM plan_eje_estrategico WHERE id = $id";
            $value = $this->select($sql);
            return $value["total_row"];
        }

        public function validaIndicadorResultado($id) {
            $sql = "SELECT COUNT(*) AS total_row FROM plan_indicador_resultado WHERE id = $id";
            $value = $this->select($sql);
            return $value["total_row"];
        }

        public function validaDocumentoFuncionario(string $documento) {
            $sql = "SELECT COUNT(*) AS total_row FROM planestructura_terceros WHERE cedulanit = '$documento' AND estado = 'S'";
            $value = $this->select($sql);
            return $value["total_row"];
        }

        public function searchIdFuncionario(string $documento) {
            $sql = "SELECT id_tercero FROM terceros WHERE cedulanit = '$documento'";
            $data = $this->select($sql);
            return $data["id_tercero"];
        }

        public function searchIdProducto($codProducto) {
            $sql = "SELECT id FROM ccpetproductos WHERE codigo_indicador = $codProducto";
            $data = $this->select($sql);
            return $data["id"];
        }

        public function insertData(int $pdtId, array $data) {
            $sql_cab = "INSERT INTO plan_indicador_producto (pdt_id, codigo_producto_id, nombre, valor_cuatrienio, meta_cuatrienio, linea_base, indicador_resultado_id, tercero_id, estado) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)";

            $sql_eje = "INSERT INTO plan_indicador_producto_det (indicador_producto_id, eje_estrategico_id) VALUES (?, ?)";
         
            foreach ($data as $key => $d) {
                $values = [
                    $pdtId, 
                    $this->searchIdProducto($d["codProducto"]),
                    $d["nombre"],
                    $d["valorCuatrienio"],
                    $d["metaCuatrienio"],
                    $d["lineaBase"],
                    str_replace("IR","",$d["indicadorResultadoId"]),
                    $this->searchIdFuncionario($d["documento"]),
                    "S"
                ];

                $id = $this->insert($sql_cab, $values);

                foreach ($d["ejes"] as $eje) {
                    $ejeId = str_replace("LE","",$eje);
                    $this->insert($sql_eje, [$id, $ejeId]);
                }
            }

            return true;
        }
    }
?>