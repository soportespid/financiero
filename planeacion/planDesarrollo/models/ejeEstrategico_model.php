<?php
    require_once '../../../Librerias/core/Helpers.php';
    require_once '../../../Librerias/core/Mysql.php';
    session_start();
    
    class ejeModel extends Mysql {
        private $cedula;
        private $vigencia;

        function __construct(){
            parent::__construct();
            $this->cedula = $_SESSION["cedulausu"];
            $this->vigencia = getVigenciaUsuario($this->cedula);
        }

        public function planesDesarrolloTerritorial() {
            $sql_periodos = "SELECT id AS periodoId FROM para_periodos_gobierno WHERE vigencia_inicial <= '$this->vigencia' AND vigencia_final >= '$this->vigencia' AND estado = 'S'";
            $result = $this->select($sql_periodos);

            $sql = "SELECT id, nombre_pdt FROM plan_pdt WHERE periodo_gobierno_id = $result[periodoId] AND estado = 'S'";
            $data = $this->select_all($sql);
            return $data;
        }

        public function insertEje(array $data) {
            $sql = "INSERT INTO plan_eje_estrategico (pdt_id, nombre, descripcion, prioridad, color_representativo, estado) VALUES (?, ?, ?, ?, ?, ?)";
            $resp = $this->insert($sql, $data);
            return $resp;
        }

        public function getConsecutivos() {
            $sql = "SELECT id FROM plan_eje_estrategico";
            $data = $this->select_all($sql);
            return $data;
        }

        public function getEje(int $id) {
            $sql = "SELECT pdt_id, nombre, descripcion, prioridad, color_representativo FROM plan_eje_estrategico WHERE id = $id";
            $data = $this->select($sql);
            return $data;
        }

        public function updateEje(array $data, int $id) {
            $sql = "UPDATE plan_eje_estrategico SET pdt_id = ?, nombre = ?, descripcion = ?, prioridad = ?, color_representativo = ? WHERE id = $id";
            $resp = $this->update($sql, $data);
            return $resp;
        }

        public function getSearchEje() {
            $sql = "SELECT E.id,
            pdt.nombre_pdt, 
            E.nombre, 
            E.descripcion, 
            E.prioridad, 
            E.color_representativo 
            FROM plan_eje_estrategico AS E
            INNER JOIN plan_pdt AS pdt
            ON E.pdt_id = pdt.id
            WHERE E.estado = 'S' ORDER BY E.id";
            $data = $this->select_all($sql);
            return $data;
        }

        public function changeStatus(int $id) {
            $sql = "UPDATE plan_eje_estrategico SET estado = ? WHERE id = $id";
            $resp = $this->update($sql, ["N"]);
            return $resp;
        }
    }
?>