<?php
    require_once '../../../Librerias/core/Helpers.php';
    require_once '../../../Librerias/core/Mysql.php';
    session_start();
    
    class planModel extends Mysql {
        /* Buscar */
        public function getPeriodosGobierno() {
            $sql = "SELECT id, vigencia_inicial, vigencia_final FROM para_periodos_gobierno WHERE estado = 'S' ORDER BY id";
            $data = $this->select_all($sql);
            return $data;
        }

        public function getRepresentante() {
            $sql = "SELECT representante FROM configbasica";
            $data = $this->select($sql);
            return $data["representante"];
        }

        public function getSearchPdt() {
            $sql = "SELECT P.nombre_pdt, P.nombre_mandatario, CONCAT(PG.vigencia_inicial, ' - ', PG.vigencia_final) AS periodo, P.id FROM plan_pdt AS P INNER JOIN para_periodos_gobierno AS PG ON P.periodo_gobierno_id = PG.id WHERE P.estado = 'S' ORDER BY P.id";
            $data = $this->select_all($sql);
            return $data;
        }

        public function getPdt(int $id) {
            $sql = "SELECT P.nombre_pdt, P.nombre_mandatario, CONCAT(PG.vigencia_inicial, ' - ', PG.vigencia_final) AS periodo, P.fecha_inicio, P.fecha_final, P.vision, P.objetivos FROM plan_pdt AS P INNER JOIN para_periodos_gobierno AS PG ON P.periodo_gobierno_id = PG.id WHERE P.id = $id";
            $data = $this->select($sql);
            return $data;
        }

        public function getLineasEstrategicas(int $pdtId) {
            $sql = "SELECT id, nombre, prioridad FROM plan_eje_estrategico WHERE estado = 'S' AND pdt_id = $pdtId ORDER BY id";
            $data = $this->select_all($sql);
            return $data;
        }

        public function getIndicadoresResultado(int $pdtId) {
            $sql = "SELECT PIR.id,
            CIR.nombre_indicador,
            PIR.linea_base,
            PIR.meta_cuatrienio
            FROM plan_indicador_resultado AS PIR
            INNER JOIN plan_catalogo_indicadores_resultado AS CIR
            ON PIR.codigo_resultado_id = CIR.id
            WHERE PIR.estado = 'S' AND PIR.pdt_id = $pdtId ORDER BY PIR.id";
            $data = $this->select_all($sql);
            return $data;
        }

        public function getIndicadoresProducto(int $pdtId) {
            $sql = "SELECT id,
            nombre,
            valor_cuatrienio,
            meta_cuatrienio,
            linea_base
            FROM plan_indicador_producto
            WHERE estado = 'S' AND pdt_id = $pdtId ORDER BY id";
            $data = $this->select_all($sql);
            return $data;
        } 

        public function getIniciativasSGR(int $pdtId) {
            $sql = "SELECT id, nombre, tipo, recurso_indicativo FROM plan_isgr WHERE estado = 'S' AND pdt_id = $pdtId ORDER BY id";
            $data = $this->select_all($sql);
            return $data;
        }

        public function getPresupuestoTotal(int $pdtId) {
            $sql = "SELECT SUM(valor_cuatrienio) AS presupuesto FROM plan_indicador_producto WHERE estado = 'S' AND pdt_id = $pdtId";
            $data = $this->select($sql);
            return $data["presupuesto"];
        }

        /* Crear */
        public function insertPdt($data) {
            $sql = "INSERT INTO plan_pdt (nombre_pdt, periodo_gobierno_id, nombre_mandatario, fecha_inicio, fecha_final, vision, objetivos, estado) VALUES (?, ?, ?, ?, ?, ?, ?, ?)";
            $response = $this->insert($sql, $data);
            return $response;
        }

        /* Editar */
        public function changeStatus(int $id) {
            $sql = "UPDATE plan_pdt SET estado = ? WHERE id = $id";
            $resp = $this->update($sql, ['N']);
            return $resp;
        }

        /* Eliminar */
    }
?>