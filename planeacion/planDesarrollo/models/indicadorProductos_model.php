<?php
    require_once '../../../Librerias/core/Helpers.php';
    require_once '../../../Librerias/core/Mysql.php';
    session_start();
    
    class productoModel extends Mysql {
        private $cedula;
        private $vigencia;

        function __construct(){
            parent::__construct();
            $this->cedula = $_SESSION["cedulausu"];
            $this->vigencia = getVigenciaUsuario($this->cedula);
        }

        public function planesDesarrolloTerritorial() {
            $sql_periodos = "SELECT id AS periodoId FROM para_periodos_gobierno WHERE vigencia_inicial <= '$this->vigencia' AND vigencia_final >= '$this->vigencia' AND estado = 'S'";
            $result = $this->select($sql_periodos);

            $sql = "SELECT id, nombre_pdt FROM plan_pdt WHERE periodo_gobierno_id = $result[periodoId] AND estado = 'S'";
            $data = $this->select_all($sql);
            return $data;
        }

        public function sectores() {
            $sql = "SELECT codigo, nombre FROM ccpetsectores ORDER BY codigo";
            $data = $this->select_all($sql);
            return $data;
        }

        public function programas(string $sector) {
            $sql = "SELECT codigo, nombre FROM ccpetprogramas WHERE codigo LIKE '$sector%'";
            $data = $this->select_all($sql);
            return $data;
        }

        public function productos(string $programa) {
            $sql = "SELECT DISTINCT cod_producto AS codigo, producto AS nombre FROM ccpetproductos WHERE cod_producto LIKE '$programa%'";
            $data = $this->select_all($sql);
            return $data;
        }

        public function indicadores(string $producto) {
            $sql = "SELECT id, 
            codigo_indicador, 
            indicador_producto AS nombre_indicador, 
            unidad_medida, 
            indicador_principal 
            FROM ccpetproductos
            WHERE codigo_indicador LIKE '$producto%'
            ORDER BY id";
            $data = $this->select_all($sql);
            return $data;
        }

        public function getEjesEstrategicos(int $pdtId) {
            $sql = "SELECT id, nombre FROM plan_eje_estrategico WHERE pdt_id = $pdtId AND estado = 'S'";
            $data = $this->select_all($sql);
            return $data;
        }

        public function funcionarios() {
            $sql = "SELECT
            T.id_tercero AS tercero_id,
            PT.cedulanit,
            CONCAT(T.nombre1, ' ', T.nombre2, ' ', T.apellido1, ' ', T.apellido2) AS nombre,
            UPPER(PC.nombrecargo) AS nombre_cargo
            FROM planestructura_terceros AS PT
            INNER JOIN planaccargos AS PC
            ON PT.codcargo = PC.codcargo
            INNER JOIN terceros AS T
            ON PT.cedulanit = T.cedulanit
            WHERE PT.estado = 'S'";
            $data = $this->select_all($sql);
            foreach ($data as $key => $d) {
                $data[$key]["nombre"] = preg_replace("/\s+/", " ", trim($d["nombre"]));
            }
            return $data;
        }

        public function indicadoresResultado(int $pdtId) {
            $sql = "SELECT PIR.id,
            CIR.codigo_indicador,
            CIR.nombre_indicador
            FROM plan_indicador_resultado AS PIR
            INNER JOIN plan_catalogo_indicadores_resultado AS CIR
            ON PIR.codigo_resultado_id = CIR.id
            WHERE PIR.pdt_id = $pdtId AND PIR.estado = 'S'
            ORDER BY PIR.id";
            $data = $this->select_all($sql);
            return $data;
        }

        public function insertIndicador(array $data) {
            $sql = "INSERT INTO plan_indicador_producto (pdt_id, codigo_producto_id, nombre, valor_cuatrienio, meta_cuatrienio, linea_base, indicador_resultado_id, tercero_id, estado) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)";
            $result = $this->insert($sql, $data);

            return $result;
        }

        public function insertIndicadorEjes(int $id, array $ejes) {
            $sql = "INSERT INTO plan_indicador_producto_det (indicador_producto_id, eje_estrategico_id) VALUES (?, ?)";
            foreach ($ejes as $eje) {
                $data = [$id, $eje["id"]];
                $this->insert($sql, $data);
            }
            return true;
        }

        public function searchConsecs() {
            $sql = "SELECT id FROM plan_indicador_producto WHERE estado = 'S'";
            $data = $this->select_all($sql);
            return $data;
        }

        public function getIndicadorProducto(int $id) {
            $sql = "SELECT 
            pip.pdt_id,
            pip.codigo_producto_id,
            p.codigo_indicador, 
            p.indicador_producto AS nombre_indicador, 
            p.unidad_medida,
            pip.nombre AS nombre_personalizado,
            pip.valor_cuatrienio,
            pip.meta_cuatrienio,
            pip.linea_base,
            pip.indicador_resultado_id,
            pcir.codigo_indicador AS cod_ind_resultado,
            pcir.nombre_indicador AS nom_ind_resultado,
            pip.tercero_id,
            t.cedulanit,
            CONCAT(t.nombre1, ' ', t.nombre2, ' ', t.apellido1, ' ', t.apellido2) AS funcionario,
            p.cod_producto,
            p.producto AS nom_producto
            FROM plan_indicador_producto AS pip
            INNER JOIN terceros AS t
            ON pip.tercero_id = t.id_tercero
            INNER JOIN ccpetproductos AS p
            ON pip.codigo_producto_id = p.id
            INNER JOIN plan_indicador_resultado AS pir
            ON pip.indicador_resultado_id = pir.id
            INNER JOIN plan_catalogo_indicadores_resultado AS pcir
            ON pir.codigo_resultado_id = pcir.id
            WHERE pip.id = $id";
            $data = $this->select($sql);
            return $data;
        }

        public function getDataSector(string $cod) {
            $sql = "SELECT codigo, nombre FROM ccpetsectores WHERE codigo = '$cod' ORDER BY codigo";
            $data = $this->select($sql);
            return $data;
        }

        public function getDataPrograma(string $cod){
            $sql = "SELECT codigo, nombre FROM ccpetprogramas WHERE codigo = '$cod'";
            $data = $this->select($sql);
            return $data;
        }

        public function getEjesIndicador(int $id) {
            $sql = "SELECT PIP.eje_estrategico_id AS id, PEE.nombre 
            FROM plan_indicador_producto_det AS PIP 
            INNER JOIN plan_eje_estrategico AS PEE 
            ON PIP.eje_estrategico_id = PEE.id 
            WHERE PIP.indicador_producto_id = $id";
            $data = $this->select_all($sql);
            return $data;
        }

        public function updateIndicadorProducto(int $id, array $data) {
            $sql = "UPDATE plan_indicador_producto SET pdt_id = ?, codigo_producto_id = ?, nombre = ?, valor_cuatrienio = ?, meta_cuatrienio = ?, linea_base = ?, indicador_resultado_id = ?, tercero_id = ? WHERE id = $id";
            $resp = $this->update($sql, $data);
            return $resp;
        }

        public function updateEjes(int $id, array $ejes) {
            $sql = "DELETE FROM plan_indicador_producto_det WHERE indicador_producto_id = $id";
            $this->delete($sql);

            $sql = "INSERT INTO plan_indicador_producto_det (indicador_producto_id, eje_estrategico_id) VALUES (?, ?)";
            foreach ($ejes as $eje) {
                $data = [$id, $eje["id"]];
                $this->insert($sql, $data);
            }
            return true;
        }

        public function getDataSearch() {
            $sql = "SELECT
            PIP.id,
            PDT.nombre_pdt,
            CP.codigo_indicador,
            CP.indicador_producto AS nombre_indicador,
            PIP.nombre AS nombre_personalizado,
            PIP.valor_cuatrienio,
            PIP.meta_cuatrienio,
            CP.unidad_medida
            FROM plan_indicador_producto AS PIP
            INNER JOIN ccpetproductos AS CP
            ON PIP.codigo_producto_id = CP.id
            INNER JOIN plan_pdt AS PDT
            ON PIP.pdt_id = PDT.id
            ORDER BY PIP.id";
            $data = $this->select_all($sql);
            return $data;
        }
    }
?>