<?php
    require_once '../../../Librerias/core/Helpers.php';
    require_once '../../../Librerias/core/Mysql.php';
    session_start();
    
    class isgrModel extends Mysql{
        private $cedula;
        private $vigencia;

        function __construct(){
            parent::__construct();
            $this->cedula = $_SESSION["cedulausu"];
            $this->vigencia = getVigenciaUsuario($this->cedula);
        }

        public function planesDesarrolloTerritorial() {
            $sql_periodos = "SELECT id AS periodoId FROM para_periodos_gobierno WHERE vigencia_inicial <= '$this->vigencia' AND vigencia_final >= '$this->vigencia' AND estado = 'S'";
            $result = $this->select($sql_periodos);

            $sql = "SELECT id, nombre_pdt FROM plan_pdt WHERE periodo_gobierno_id = $result[periodoId] AND estado = 'S'";
            $data = $this->select_all($sql);
            return $data;
        }

        public function validaSector(string $codSector) {
            $sql = "SELECT COUNT(*) AS total_row FROM ccpetsectores WHERE codigo = '$codSector'";
            $value = $this->select($sql);
            return $value["total_row"];
        }

        public function validaEjeEstrategico($id) {            
            $sql = "SELECT COUNT(*) AS total_row FROM plan_eje_estrategico WHERE id = $id";
            $value = $this->select($sql);
            return $value["total_row"];
        }

        public function validaDocumentoFuncionario(string $documento) {
            $sql = "SELECT COUNT(*) AS total_row FROM planestructura_terceros WHERE cedulanit = '$documento' AND estado = 'S'";
            $value = $this->select($sql);
            return $value["total_row"];
        }

        public function validaBpin(string $bpin) {
            $sql = "SELECT COUNT(*) AS total_row FROM ccpproyectospresupuesto WHERE codigo = '$bpin'";
            $value = $this->select($sql);
            return $value["total_row"];
        }

        public function searchIdSector(string $codSector) {
            $sql = "SELECT id FROM ccpetsectores WHERE codigo = '$codSector'";
            $data = $this->select($sql);
            return $data["id"];
        }

        public function searchIdFuncionario(string $documento) {
            $sql = "SELECT id_tercero FROM terceros WHERE cedulanit = '$documento'";
            $data = $this->select($sql);
            return $data["id_tercero"];
        }

        public function insertData(int $pdtId, array $data) {
            $sql = "INSERT INTO plan_isgr (pdt_id, nombre, tipo, sector_id, recurso_indicativo, tercero_id, estado) VALUES (?, ?, ?, ?, ?, ?, ?)";
            $sql_eje = "INSERT INTO plan_isgr_det (isgr_id, eje_estrategico_id) VALUES (?, ?)";
            $sql_bpin = "INSERT INTO plan_isgr_bpin (isgr_id, bpin) VALUES (?, ?)";

            foreach ($data as $d) {
                
                $values = [
                    $pdtId,
                    $d["nombre"],
                    strtolower($d["tipo"]),
                    $this->searchIdSector($d["codSector"]),
                    $d["valor"],
                    $this->searchIdFuncionario($d["documento"]),
                    "S"
                ];
                $isgrId = $this->insert($sql, $values);

                foreach ($d["ejeEstrategico"] as $eje) {
                    $ejeId = str_replace("LE","",$eje);
                    $this->insert($sql_eje, [$isgrId, $ejeId]);
                }

                foreach ($d["bpins"] as $bpin) {
                    $this->insert($sql_bpin, [$isgrId, $bpin]);
                }
            }

            return true;
        }
    }
?>