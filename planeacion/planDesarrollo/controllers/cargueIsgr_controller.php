<?php
    require_once "../../../PHPExcel/Classes/PHPExcel.php";
    require_once '../../../Librerias/core/Helpers.php';
    require_once '../models/cargueIsgr_model.php';
    session_start();
    header('Content-Type: application/json');

    class isgrController extends isgrModel {
        public function get() {
            if (!empty($_SESSION)) {
                $arrResponse = array("planes" => $this->planesDesarrolloTerritorial());
                echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
            }
        }

        public function upload() {
            if (!empty($_SESSION)) {
                $data = [];
                $error = false;
                $pdtId = $_POST["pdtId"];
                $tmpfname = $_FILES['file']['tmp_name'];
                $excelReader = PHPExcel_IOFactory::createReaderForFile($tmpfname);
                $excelObj = $excelReader->load($tmpfname);
                $hojaDatos = $excelObj->getSheet(0);
                $lastRowFormatos = $hojaDatos->getHighestRow();

                for ($i=4; $i <= $lastRowFormatos ; $i++) {
                    $dataTemp = [];
                    $nombre = $hojaDatos->getCell("A$i")->getValue();
                    $tipo = $hojaDatos->getCell("B$i")->getValue();
                    $codSector = $hojaDatos->getCell("C$i")->getValue();
                    $txtEje = $hojaDatos->getCell("D$i")->getValue();
                    $ejesEstrategico = explode(",", $txtEje);
                    $valor = $hojaDatos->getCell("E$i")->getValue();
                    $documento = $hojaDatos->getCell("F$i")->getValue();
                    $txtBpin = $hojaDatos->getCell("G$i")->getValue();
                    $bpins = explode(",", $txtBpin);

                    /* valida tipo */
                    if ($tipo != "iniciativa" && $tipo != "proyecto" && $tipo != "Iniciativa" && $tipo != "Proyecto") {
                        $error = true;
                        $arrResponse = array("status" => false, "msg" => "Error en el campo tipo en fila $i, revise los datos del formato");        
                        goto salir;
                    }

                    /* valida cod sector */
                    if ($this->validaSector($codSector) == 0) {
                        $error = true;
                        $arrResponse = array("status" => false, "msg" => "Error en el campo sector en la fila $i, revise los datos del formato");
                        goto salir;
                    }

                    /* valida lineas estrategicas */
                    foreach ($ejesEstrategico as $eje) {
                        $abreviacion = substr($eje, 0, 2);
                        if ($abreviacion == "LE") {
                            $id = str_replace("LE","",$eje);
                            if ($this->validaEjeEstrategico ($id) == 0) {
                                $error = true;
                                $arrResponse = array("status" => false, "msg" => "Error en la linea estrategica en la fila $i, revise los datos del formato");
                                goto salir;
                            }
                        }
                        else {
                            $error = true;
                            $arrResponse = array("status" => false, "msg" => "Error en la abreviación de la linea estrategica en la fila $i, revise los datos del formato");
                            goto salir;
                        }
                    }
                    
                    /* valida documento funcionario */
                    if ($this->validaDocumentoFuncionario($documento) == 0) {
                        $error = true;
                        $arrResponse = array("status" => false, "msg" => "Error en el campo documento funcionario en la fila $i, revise los datos del formato");
                        goto salir;
                    }
                    
                    /* valida bpins agregados */
                    foreach ($bpins as $bpin) {
                        if ($this->validaBpin($bpin) == 0) {
                            $error = true;
                            $arrResponse = array("status" => false, "msg" => "Error en el BPIN en la fila $i, revise los datos del formato");
                            goto salir;
                        }
                    }

                    $dataTemp = [
                        "nombre" => strClean(replaceChar($nombre)),
                        "tipo" => (string) $tipo,
                        "codSector" => (string) $codSector,
                        "ejeEstrategico" => $ejesEstrategico,
                        "valor" => (float) round($valor, 2),
                        "documento" => (string) $documento, 
                        "bpins" => $bpins
                    ];
                    array_push($data, $dataTemp);
                }

                salir:

                if ($error == false) {
                    $this->insertData($pdtId, $data);
                    $arrResponse = array("status" => true, "msg" => "El archivo ha sido cargado correctamente correctamente.");
                }
                
                echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
            }
            die();
        }

    }

    if($_POST){
        $obj = new isgrController();
        $accion = $_POST["action"];

        if ($accion == 'upload') {
            $obj->upload();
        } else if ($accion == "get") {
            $obj->get();
        }
    }
?>