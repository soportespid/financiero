<?php
    require_once '../../../Librerias/core/Helpers.php';
    require_once '../models/isgr_model.php';
    session_start();
    header('Content-Type: application/json');

    class isgrController extends isgrModel {
        /* crear */
        public function get() {
            if (!empty($_SESSION)) {
                $arrResponse = [
                    "planes" => $this->planesDesarrolloTerritorial(),
                    "funcionarios" => $this->funcionarios(),
                    "sectores" => $this->sectores()
                ];
                echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
            }
            die();
        }

        public function changeDataPdt() {
            if (!empty($_SESSION)) {
                $pdtId = $_POST["pdtId"];
                $arrResponse = ["ejes" => $this->getEjesEstrategicos($pdtId)];
                echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
            }
            die();
        }

        public function save() {
            if (!empty($_SESSION)) {
                $ejes = json_decode($_POST['ejes'],true);
                $bpins = json_decode($_POST['bpins'],true);
                $dataForm = [
                    (int) $_POST["pdtId"],
                    strClean(replaceChar($_POST["nombre"])),
                    $_POST["tipo"],
                    (int) $_POST["sectorId"],
                    (float) $_POST["recurso"],
                    (int) $_POST["terceroId"],
                    "S"
                ];
                $id = $this->insertSgr($dataForm); 
                if ($id > 0) {
                    $this->insertEjes($id, $ejes);
                    $this->insertBpins($id, $bpins);
                    $arrResponse = array("status" => true, "msg" => "Iniciativa SGR guardada con exito", "id" => $id);
                }
                else {
                    $arrResponse = array("status" => false, "msg" => "Error en guardar, vuelva intentar");
                }
                echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
            }
            die();
        }

        public function checkBpin() {
            if (!empty($_SESSION)) {
                $bpin = (int) $_POST["bpin"];
                $arrResponse = array("cantidad" => $this->searchBpin($bpin));
                echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
            }
            die();
        }

        /* buscar */

        public function getSearch() {
            if (!empty($_SESSION)) {
                $arrResponse = array("status" => true, "data" => $this->getDataSearch());    
                echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
            }
            die();
        }

        /* editar */
        public function getEdit() {
            if (!empty($_SESSION)) {
                $id = (int) $_POST["id"];
                $data = $this->getDataEdit($id);
                $pdtId = $data["pdt_id"];
                $arrResponse = [
                    "planes" => $this->planesDesarrolloTerritorial(),
                    "ejes" => $this->getEjesEstrategicos($pdtId),
                    "funcionarios" => $this->funcionarios(),
                    "sectores" => $this->sectores(),
                    "dataEdit" => $data,
                    "ejesSelected" => $this->getDatEjes($id),
                    "bpins" => $this->getDataBpin($id),
                    "consecutivos" => $this->getConsecs()
                ];    
                echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
            }
            die();
        }

        public function saveEdit() {
            if (!empty($_SESSION))  {
                $ejes = json_decode($_POST['ejes'],true);
                $bpins = json_decode($_POST['bpins'],true);
                $id = $_POST["id"];
                $dataForm = [
                    (int) $_POST["pdtId"],
                    strClean(replaceChar($_POST["nombre"])),
                    $_POST["tipo"],
                    (int) $_POST["sectorId"],
                    (float) $_POST["recurso"],
                    (int) $_POST["terceroId"],
                ];

                $result = $this->updateSgr($id, $dataForm);
                if ($result > 0) {
                    $this->insertEjes($id, $ejes);
                    $this->insertBpins($id, $bpins);
                    $arrResponse = array("status" => true, "msg" => "Iniciativa SGR actualizada con exito");
                }
                else {
                    $arrResponse = array("status" => false, "msg" => "Error en guardar, vuelva intentar");
                }
                echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
            }
            die();
        }
    }

    if($_POST){
        $obj = new isgrController();
        $accion = $_POST["action"];
        if ($accion == "get") {
            $obj->get();
        } else if ($accion == "searchBpin") {
            $obj->checkBpin();
        } else if ($accion == "save") {
            $obj->save();
        } else if ($accion == "getSearch") {
            $obj->getSearch();
        } else if ($accion == "getEdit") {
            $obj->getEdit();
        } else if ($accion == "saveEdit") {
            $obj->saveEdit();
        } else if ($accion == "changeDataPdt") {
            $obj->changeDataPdt();
        }
    }
?>