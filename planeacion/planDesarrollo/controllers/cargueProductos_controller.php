<?php
    require_once "../../../PHPExcel/Classes/PHPExcel.php";
    require_once '../../../Librerias/core/Helpers.php';
    require_once '../models/cargueProductos_model.php';
    session_start();
    header('Content-Type: application/json');

    class productoController extends productoModel {
        public function get() {
            if (!empty($_SESSION)) {
                $arrResponse = array("planes" => $this->planesDesarrolloTerritorial());
                echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
            }
        }

        public function upload() {
            if (!empty($_SESSION)) {
                $data = [];
                $error = false;
                $pdtId = $_POST["pdtId"];
                $tmpfname = $_FILES['file']['tmp_name'];
                $excelReader = PHPExcel_IOFactory::createReaderForFile($tmpfname);
                $excelObj = $excelReader->load($tmpfname);
                $hojaDatos = $excelObj->getSheet(0);
                $lastRowFormatos = $hojaDatos->getHighestRow();

                for ($i=4; $i <= $lastRowFormatos ; $i++) {
                    $dataTemp = [];
                    $nombre = $hojaDatos->getCell("A$i")->getValue();
                    $codIndicadorProducto = $hojaDatos->getCell("B$i")->getValue();
                    $txtEje = $hojaDatos->getCell("C$i")->getValue();
                    $ejesEstrategico = explode(",", $txtEje);
                    $indicadorResultadoId = $hojaDatos->getCell("D$i")->getValue();
                    $valorCuatrienio = $hojaDatos->getCell("E$i")->getValue();
                    $metaCuatrienio = $hojaDatos->getCell("F$i")->getValue();
                    $lineaBase = $hojaDatos->getCell("G$i")->getValue();
                    $documento = $hojaDatos->getCell("H$i")->getValue();

                    /* validaciones indicador producto*/
                    if ($this->validaIndicadorProducto($codIndicadorProducto) == 0) {
                        $error = true;
                        $arrResponse = array("status" => false, "msg" => "Error en el campo código indicador producto en la fila $i, revise los datos del formato");
                        goto salir;
                    }

                    /* validacion eje estrategico*/
                    foreach ($ejesEstrategico as $eje) {
                        $abreviacion = substr($eje, 0, 2);
                        if ($abreviacion == "LE") {
                            $id = str_replace("LE","",$eje);
                            if ($this->validaEjeEstrategico ($id) == 0) {
                                $posicion = $i;
                                $error = true;
                                $arrResponse = array("status" => false, "msg" => "Error en la linea estrategica en la fila $posicion, revise los datos del formato");
                                goto salir;
                            }
                        }
                        else {
                            $posicion = $i;
                            $error = true;
                            $arrResponse = array("status" => false, "msg" => "Error en la abreviación de la linea estrategica en la fila $posicion, revise los datos del formato");
                            goto salir;
                        }

                        
                    }

                    /* validacion indicador resultado*/
                    $abreviacionResultado = substr($indicadorResultadoId, 0, 2);
                    if ($abreviacionResultado == "IR") {
                        $id = str_replace("IR","",$indicadorResultadoId);
                        if ($this->validaIndicadorResultado($id) == 0) {
                            $error = true;
                            $arrResponse = array("status" => false, "msg" => "Error en el indicador resultado en la fila $i, revise los datos del formato");
                            goto salir;
                        }
                    }
                    else {
                        $error = true;
                        $arrResponse = array("status" => false, "msg" => "Error en la abreviación del indicador resultado en la fila $i, revise los datos del formato");
                        goto salir;
                    }
                    
                    /* validacion documento funcionario*/
                    if ($this->validaDocumentoFuncionario($documento) == 0) {
                        $error = true;
                        $arrResponse = array("status" => false, "msg" => "Error en el campo documento funcionario en la fila $i, revise los datos del formato");
                        goto salir;
                    }

                    $dataTemp = [
                        "nombre" => strClean(replaceChar($nombre)),
                        "codProducto" => (int) $codIndicadorProducto,
                        "ejes" => $ejesEstrategico,
                        "indicadorResultadoId" => (string) $indicadorResultadoId,
                        "valorCuatrienio" => (float) round($valorCuatrienio,2),
                        "metaCuatrienio" => (float) round($metaCuatrienio, 2),
                        "lineaBase" => (float) round($lineaBase, 2),
                        "documento" => (int) $documento
                    ];
                    array_push($data, $dataTemp);
                }

                salir:

                if ($error == false) {
                    $this->insertData($pdtId, $data);
                    $arrResponse = array("status" => true, "msg" => "El archivo ha sido cargado correctamente correctamente.");
                }
                
                echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
            }
            die();
        }

    }

    if($_POST){
        $obj = new productoController();
        $accion = $_POST["action"];

        if ($accion == 'upload') {
            $obj->upload();
        } else if ($accion == "get") {
            $obj->get();
        }
    }
?>