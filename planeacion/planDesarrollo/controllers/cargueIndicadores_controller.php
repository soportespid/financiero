<?php
    require_once "../../../PHPExcel/Classes/PHPExcel.php";
    require '../../../comun.inc';
    require '../../../funciones.inc';
    require_once "../../../funcionesSP.inc.php";
    session_start();
    header('Content-Type: application/json');

    class cargueIndicadorController {
        private $linkbd;
        public function __construct() {
            $this->linkbd = conectar_v7();
            $this->linkbd->set_charset("utf8");
        }

        /* crear */
        public function upload() {
            if (!empty($_SESSION)) {
                $tmpfname = $_FILES['file']['tmp_name'];
                $excelReader = PHPExcel_IOFactory::createReaderForFile($tmpfname);
                $excelObj = $excelReader->load($tmpfname);
                $hojaDatos = $excelObj->getSheet(0);

                $lastRowFormatos = $hojaDatos->getHighestRow();

                $sql = "TRUNCATE TABLE plan_catalogo_indicadores_resultado";
                mysqli_query($this->linkbd, $sql);

                for ($i=3; $i <= $lastRowFormatos ; $i++) {
                    $data = [];
                    $codigoIndicador = $hojaDatos->getCell("A$i")->getValue();
                    $nombreIndicador = $hojaDatos->getCell("B$i")->getValue();
                    $unidadMedida = $hojaDatos->getCell("C$i")->getValue();
                    $fuente = $hojaDatos->getCell("D$i")->getValue();
                    $datoNumerico = $hojaDatos->getCell("E$i")->getValue();
                    $vigenciaMes = $hojaDatos->getCell("F$i")->getValue();
                    $pnd = $hojaDatos->getCell("G$i")->getValue();
                    $transformacion = $hojaDatos->getCell("H$i")->getValue();
                    $transformacion = $transformacion != "" ? strClean(replaceChar($transformacion)) : $transformacion;
                    
                    $data = [
                        "codigoIndicador" => $codigoIndicador,
                        "nombreIndicador" => strClean(replaceChar($nombreIndicador)),
                        "unidadMedida" => strClean(replaceChar($unidadMedida)),
                        "fuente" => strClean(replaceChar($fuente)),
                        "datoNumerico" => (double) $datoNumerico,
                        "vigenciaMes" => $vigenciaMes,
                        "pnd" => $pnd,
                        "transformacion" => $transformacion
                    ];
                    $this->insertIndicador($data);
                }
                
                $arrResponse = array("status" => true, "msg" => "El archivo ha sido cargado correctamente correctamente.");
                
                echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
            }
            die();
        }

        private function insertIndicador($data) {
            $sql = "INSERT INTO plan_catalogo_indicadores_resultado (codigo_indicador, nombre_indicador, unidad_medida, fuente, dato_numerico, vigencia_mes, pnd, transformacion) VALUES ($data[codigoIndicador], '$data[nombreIndicador]', '$data[unidadMedida]', '$data[fuente]', $data[datoNumerico], '$data[vigenciaMes]', '$data[pnd]', '$data[transformacion]')";
            $request = mysqli_query($this->linkbd,$sql);
            return $request;
        }

    }

    if($_POST){
        $obj = new cargueIndicadorController();
        $accion = $_POST["action"];

        if ($accion == 'upload') {
            $obj->upload();
        } 
    }
?>