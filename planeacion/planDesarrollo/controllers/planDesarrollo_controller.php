<?php
    require_once '../../../Librerias/core/Helpers.php';
    require_once '../models/planDesarrollo_model.php';
    session_start();
    header('Content-Type: application/json');

    class planController extends planModel {
       
        public function get() {
            if (!empty($_SESSION)) {
                $arrResponse = array("status" => true, "representante" => $this->getRepresentante(), "periodos" => $this->getPeriodosGobierno());
                echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
            }
            die();
        }

        public function getSearch() {
            if (!empty($_SESSION)) {
                $arrResponse = array("status" => true, "pdts" => $this->getSearchPdt());
                echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
            }
            die();
        }

        public function getVisualize() {
            if (!empty($_SESSION)) {
                $id = intval($_POST["id"]);
                $data = $this->getPdt($id);
                $objetivos = explode(",", $data["objetivos"]);
                $info = [
                    "nombrePdt" => $data["nombre_pdt"],
                    "representante" => $data["nombre_mandatario"],
                    "periodo" => $data["periodo"],
                    "fechaIni" => $data["fecha_inicio"],
                    "fechaFin" => $data["fecha_final"],
                    "vision" => $data["vision"],
                ];
                $arrResponse = array("status" => true, "info" => $info, "objetivos" => $objetivos, "lineasEstrategicas" => $this->getLineasEstrategicas($id), "indicadoresResultado" => $this->getIndicadoresResultado($id), "indicadoresProducto" => $this->getIndicadoresProducto($id), "iniciativasSGR" => $this->getIniciativasSGR($id), "presupuesto" => $this->getPresupuestoTotal($id));
                echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
            }
            die();
        }

        public function save() {
            if (!empty($_SESSION)) {
                // var_dump($_POST);
                $objetivos = json_decode($_POST['objetivos'],true);
                $txtObjetivos = implode(",", $objetivos);
                
                $data = [
                    strtoupper(strClean(replaceChar($_POST["namePdt"]))),
                    (int) $_POST["periodo"],
                    strClean(replaceChar($_POST["representante"])),
                    "$_POST[fechaIni]",
                    "$_POST[fechaFin]",
                    strtoupper(strClean(replaceChar($_POST["vision"]))),
                    strtoupper(strClean($txtObjetivos)),
                    'S'
                ];

                $response = $this->insertPdt($data);
                
                if ($response > 0) {
                    $arrResponse = array("status" => true, "msg" => "PDT Guardado con exito");
                }
                else {
                    $arrResponse = array("status" => false, "msg" => "Error en guardar, intente de nuevo");
                }

                echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
            }
            die();
        }

        public function delPdt() {
            if (!empty($_SESSION)) {
                $id = (int) $_POST["id"];
                $resp = $this->changeStatus($id);
                if ($resp > 0) {
                    $arrResponse = array("status" => true, "msg" => "PDT elminado con exito");
                }
                else {
                    $arrResponse = array("status" => false, "msg" => "Error en eliminar, intente de nuevo");
                }
                echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
            }
            die();
        }
    }

    if($_POST){
        $obj = new planController();
        $accion = $_POST["action"];
        
        if ($accion == 'get') {
            $obj->get();
        } else if ($accion == 'save') {
            $obj->save();
        } else if ($accion == 'getSearch') {
            $obj->getSearch();
        } else if ($accion == 'getVisualize') {
            $obj->getVisualize();
        } else if ($accion == "delPdt") {
            $obj->delPdt();
        }
    }
?>