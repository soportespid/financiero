<?php
    require_once '../../../Librerias/core/Helpers.php';
    require_once '../models/ejeEstrategico_model.php';
    session_start();
    header('Content-Type: application/json');

    class ejeController extends ejeModel {
        /* crear */
        public function get() {
            if (!empty($_SESSION)) {
                $arrResponse = array("planes" => $this->planesDesarrolloTerritorial());
                echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
            }
        }

        public function save() {
            if (!empty($_SESSION)) {
                $data = [
                    (int) $_POST["pdtId"],
                    strtoupper(strClean(replaceChar($_POST["nombre"]))),
                    strtoupper(strClean(replaceChar($_POST["descripcion"]))),
                    (int) $_POST["nivel"],
                    $_POST["color"],
                    "S"
                ];

                $resp = $this->insertEje($data);
                if ($resp > 0) {
                    $arrResponse = array("status" => true, "msg" => "Guardado con exito", "id" => $resp);
                }
                else {
                    $arrResponse = array("status" => false, "msg" => "Error en guardado, vuelva a intentar");
                }
                
                echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
            }
            die();
        }

        /* buscar */
        public function getSearch() {
            if (!empty($_SESSION)) {
                $arrResponse = array("status" => true, "data" => $this->getSearchEje());
                echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
            }
            die();
        }

        /* editar */
        public function getEdit() {
            if (!empty($_SESSION)) {
                $id = (int) $_POST["id"];
                $arrResponse = array("status" => true, "consecutivos" => $this->getConsecutivos(), "data" => $this->getEje($id), "planes" => $this->planesDesarrolloTerritorial());
                echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
            }
            die();
        }

        public function saveEdit() {
            if (!empty($_SESSION)) {
                $id = (int) $_POST["id"];
                $data = [
                    (int) $_POST["pdtId"],
                    strtoupper(strClean(replaceChar($_POST["nombre"]))),
                    strtoupper(strClean(replaceChar($_POST["descripcion"]))),
                    (int) $_POST["nivel"],
                    $_POST["color"]
                ];

                $resp = $this->updateEje($data, $id);

                if ($resp > 0) {
                    $arrResponse = array("status" => true, "msg" => "Guardado con exito");
                }
                else {
                    $arrResponse = array("status" => false, "msg" => "Error en guardado, vuelva a intentar");
                }

                echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
            }
            die();
        }

        public function delEje() {
            if (!empty($_SESSION)) {
                $id = (int) $_POST["id"];
                $resp = $this->changeStatus($id);
                if ($resp > 0) {
                    $arrResponse = array("status" => true, "msg" => "Eliminado con exito");
                }
                else {
                    $arrResponse = array("status" => false, "msg" => "Error, vuelva a intentarlo");
                }
                echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
            }
            die();
        }
    }

    if($_POST){
        $obj = new ejeController();
        $accion = $_POST["action"];
        
        if ($accion == 'save') {
            $obj->save();
        } else if ($accion == "get") {
            $obj->get();
        } else if ($accion == "getEdit") {
            $obj->getEdit();
        } else if ($accion == "saveEdit") {
            $obj->saveEdit();
        } else if ($accion == "getSearch") {
            $obj->getSearch();
        } else if(($accion == "delEje")) {
            $obj->delEje();
        }
    }
?>