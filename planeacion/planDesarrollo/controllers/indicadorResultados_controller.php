<?php
    require_once '../../../Librerias/core/Helpers.php';
    require_once '../models/indicadorResultados_model.php';
    session_start();
    header('Content-Type: application/json');

    class resultadoController extends resultadoModel {
        /* crear */
        public function get() {
            if (!empty($_SESSION)) {
                
                $arrResponse = array("status" => true, "indicadores" => $this->getIndicadoresResultado(), "planes" => $this->planesDesarrolloTerritorial());
                echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
            }
            die();
        }

        public function getEjes() {
            if (!empty($_SESSION)) {
                $pdtId = $_POST["pdtId"];
                $arrResponse = array("ejes" => $this->getEjesEstrategicos($pdtId));
                echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
            }
            die();
        }

        public function save() {
            if (!empty($_SESSION)) {
                $ejes = json_decode($_POST['ejes'],true);
                $indicadorId = $_POST["indicadorId"];
                $cuatrienio = $_POST["metaCuatrienio"];
                $pdtId = (int) $_POST["pdtId"];
                $lineaBase = (float) $_POST["lineaBase"];

                $result = $this->insertIndicador($indicadorId, $cuatrienio, $pdtId, $lineaBase);
                if ($result > 0) {
                    $this->insertIndicadorEjes($result, $ejes);
                    $arrResponse = array("status" => true, "msg" => "Indicador de resultado fue creado", "id" => $result);    
                }
                else {
                    $arrResponse = array("status" => false, "msg" => "Error en guardado, vuelva a intentar");    
                }
                
                echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
            }
            die();
        }

        /* buscar */

        public function getSearch() {
            if (!empty($_SESSION)) {
                $arrResponse = array("status" => true, "data" => $this->getIndicadores());    
                echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
            }
            die();
        }

        public function delIndicador() {
            if (!empty($_SESSION)) {
                $id = (int) $_POST["indResultadoId"];
                $result = $this->changeStatusIndResultado($id);
                if ($result > 0) {
                    $arrResponse = array("status" => true, "msg" => "Indicador de resultado ha sido eliminado");    
                }
                else {
                    $arrResponse = array("status" => false, "msg" => "Error, vuelva a intentar");    
                }   
                echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
            }
            die();
        }

        /* editar */
        public function getEdit() {
            if (!empty($_SESSION)) {
                $id = (int) $_POST["id"];
                $data = $this->getEditIndicador($id); 
                $pdtId = $data["pdt_id"];
                $arrResponse = [
                    "data" => $this->getEditIndicador($id), 
                    "selectEjes" => $this->getEditEjes($id), 
                    "indicadores" => $this->getIndicadoresResultado(), 
                    "ejes" => $this->getEjesEstrategicos($pdtId),
                    "consecutivos" => $this->searchConsecs(),
                    "planes" => $this->planesDesarrolloTerritorial()
                ];    
                echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
            }
            die();
        }

        public function saveEdit() {
            if (!empty($_SESSION))  {
                $id = (int) $_POST["id"];
                $indicadorId = $_POST["indicadorId"];
                $metaCuatrienio = $_POST["metaCuatrienio"];
                $ejes = json_decode($_POST['ejes'],true);

                $result = $this->updateIndicador($indicadorId, $metaCuatrienio, $id);

                if ($result > 0) {
                    $this->updateEjes($id, $ejes);
                    $arrResponse = array("status" => true, "msg" => "Indicador de resultado ha sido actualizado");    
                }
                else {
                    $arrResponse = array("status" => false, "msg" => "Error, vuelva a intentar");    
                }

                echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
            }
            die();
        }
    }

    if($_POST){
        $obj = new resultadoController();
        $accion = $_POST["action"];
        
        if ($accion == "get") {
            $obj->get();
        } else if ($accion == 'save') { 
            $obj->save();
        } else if ($accion == "getSearch") {
            $obj->getSearch();
        } else if ($accion == "delIndicador") {
            $obj->delIndicador();
        } else if ($accion == "getEdit") {
            $obj->getEdit();
        } else if ($accion == "saveEdit") {
            $obj->saveEdit();
        } else if ($accion == "getEjes") {
            $obj->getEjes();
        }
    }
?>