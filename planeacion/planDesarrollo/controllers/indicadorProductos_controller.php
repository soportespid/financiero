<?php
    require_once '../../../Librerias/core/Helpers.php';
    require_once '../models/indicadorProductos_model.php';
    session_start();
    header('Content-Type: application/json');

    class productoController extends productoModel {
        /* crear */
        public function get() {
            if (!empty($_SESSION)) {
                
                $arrResponse = [
                    "funcionarios" => $this->funcionarios(),
                    "planes" => $this->planesDesarrolloTerritorial(),
                    "sectores" => $this->sectores()
                ];
                echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
            }
            die();
        }

        public function getDataPdt() {
            if (!empty($_SESSION)) {
                $pdtId = $_POST["pdtId"];
                $arrResponse = [
                    "ejes" => $this->getEjesEstrategicos($pdtId),
                    "indicadoresResultado" => $this->indicadoresResultado($pdtId),
                ];
                echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
            }
            die();
        }

        public function getPrograma() {
            if (!empty($_SESSION)) {
                $codSector = (string) $_POST["codSector"];
                $arrResponse = array("programas" => $this->programas($codSector));
                echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
            }
            die();
        }

        public function getProducto() {
            if (!empty($_SESSION)) {
                $codPrograma = (string) $_POST["codPrograma"];
                $arrResponse = array("productos" => $this->productos($codPrograma));
                echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
            }
            die();
        }

        public function getIndicador() {
            if (!empty($_SESSION)) {
                $codProducto = (string) $_POST["codProducto"];
                $arrResponse = array("indicador" => $this->indicadores($codProducto));
                echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
            }
            die();
        }

        public function save() {
            if (!empty($_SESSION)) {
                $pdtId = (int) $_POST["pdtId"];
                $indicadorId = (int) $_POST["indicadorId"];
                $nombrePersonalizado = strClean(replaceChar($_POST["nombrePersonalizado"]));
                $valorCuatrienio = (float) $_POST["valor"];
                $cuatrienio = (float) $_POST["metaCuatrienio"];
                $lineaBase = (float) $_POST["lineaBase"];
                $indicadorResultadoId = (int) $_POST["indicadorResultadoId"];
                $terceroId = (int) $_POST["terceroId"];
                $ejes = json_decode($_POST['ejes'],true);

                $data = [$pdtId, $indicadorId, $nombrePersonalizado, $valorCuatrienio, $cuatrienio, $lineaBase, $indicadorResultadoId, $terceroId, "S"];
                
                $consec = $this->insertIndicador($data);
                if ($consec > 0) {
                    $this->insertIndicadorEjes($consec, $ejes);
                    $arrResponse = array("status" => true, "msg" => "Indicador de resultado fue creado", "id" => $consec);    
                }
                else {
                    $arrResponse = array("status" => false, "msg" => "Error en guardado, vuelva a intentar");    
                }
                
                echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
            }
            die();
        }

        /* buscar */

        public function getSearch() {
            if (!empty($_SESSION)) {
                $arrResponse = array("status" => true, "data" => $this->getDataSearch());    
                echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
            }
            die();
        }

        public function delIndicador() {
            if (!empty($_SESSION)) {
                // $id = (int) $_POST["indResultadoId"];
                // $result = $this->changeStatusIndResultado($id);
                // if ($result > 0) {
                //     $arrResponse = array("status" => true, "msg" => "Indicador de resultado ha sido eliminado");    
                // }
                // else {
                //     $arrResponse = array("status" => false, "msg" => "Error, vuelva a intentar");    
                // }   
                // echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
            }
            die();
        }

        /* editar */
        public function getEdit() {
            if (!empty($_SESSION)) {
                $id = (int) $_POST["id"];
                $data = $this->getIndicadorProducto($id);
                $pdtId = $data["pdt_id"];
                $sector = $this->getDataSector(substr($data["codigo_indicador"], 0, 2));
                $programa = $this->getDataPrograma(substr($data["codigo_indicador"], 0, 4));
                $arrResponse = [
                    "data" => $data, 
                    "ejes" => $this->getEjesEstrategicos($pdtId),
                    "funcionarios" => $this->funcionarios(),
                    "indicadoresResultado" => $this->indicadoresResultado($pdtId),
                    "planes" => $this->planesDesarrolloTerritorial(),
                    "sectores" => $this->sectores(),
                    "ejesSelected" => $this->getEjesIndicador($id),
                    "consecutivos" => $this->searchConsecs(),
                    "sector" => $sector,
                    "programa" => $programa
                ];    
                
                echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
            }
            die();
        }

        public function saveEdit() {
            if (!empty($_SESSION))  {
                $id = (int) $_POST["id"];
                $pdtId = (int) $_POST["pdtId"];
                $indicadorId = (int) $_POST["indicadorId"];
                $nombrePersonalizado = strClean(replaceChar($_POST["nombrePersonalizado"]));
                $valorCuatrienio = (float) $_POST["valor"];
                $cuatrienio = (float) $_POST["metaCuatrienio"];
                $lineaBase = (float) $_POST["lineaBase"];
                $indicadorResultadoId = (int) $_POST["indicadorResultadoId"];
                $terceroId = (int) $_POST["terceroId"];
                $ejes = json_decode($_POST['ejes'],true);
                $data = [$pdtId, $indicadorId, $nombrePersonalizado, $valorCuatrienio, $cuatrienio, $lineaBase, $indicadorResultadoId, $terceroId];
                $result = $this->updateIndicadorProducto($id, $data);
                if ($result > 0) {
                    $this->updateEjes($id, $ejes);
                    $arrResponse = array("status" => true, "msg" => "Indicador de producto fue actualizado");    
                }
                else {
                    $arrResponse = array("status" => false, "msg" => "Error en guardado, vuelva a intentar");    
                }
                echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
            }
            die();
        }
    }

    if($_POST){
        $obj = new productoController();
        $accion = $_POST["action"];
        
        if ($accion == "get") {
            $obj->get();
        } else if ($accion == 'save') { 
            $obj->save();
        } else if ($accion == "getSearch") {
            $obj->getSearch();
        } else if ($accion == "delIndicador") {
            $obj->delIndicador();
        } else if ($accion == "getEdit") {
            $obj->getEdit();
        } else if ($accion == "saveEdit") {
            $obj->saveEdit();
        } else if ($accion == "programa") {
            $obj->getPrograma();
        } else if ($accion == "producto") {
            $obj->getProducto();
        } else if ($accion == "indicador") {
            $obj->getIndicador();
        } else if ($accion == "getDataPdt") {
            $obj->getDataPdt();
        }
    }
?>