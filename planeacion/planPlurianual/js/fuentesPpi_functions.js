const URL = "planeacion/planPlurianual/controllers/fuentesPpi_controller.php";

var app = new Vue({
    el:"#myapp",
    data() {
        return {
            isLoading: false,
            dataSearch: [],
            dataSearchCopy: [],
            txtSearch: '',
            id: 0, 
            arrConsecutivos: [],
            nombre: '',
        }
    },
    mounted() {
        this.intPageVal = this.$refs.pageType.value;

        if (this.intPageVal == 1) {
           
        } else if (this.intPageVal == 2) {
            this.getSearch();
        } else if (this.intPageVal == 3) {
            this.getEdit();
        }
    },
    methods: {
        /* metodos para traer información */
        async getEdit() {
            this.id = new URLSearchParams(window.location.search).get('id');
            const formData = new FormData();
            formData.append("action","getEdit");
            formData.append("id", this.id);
            const response = await fetch(URL,{method:"POST",body:formData});
            const objData = await response.json();
            this.nombre = objData.data.nombre;
            this.arrConsecutivos = objData.consecutivos;
        },

        async getSearch() {
            const formData = new FormData();
            formData.append("action","getSearch");
            const response = await fetch(URL,{method:"POST",body:formData});
            const objData = await response.json();
            this.dataSearch = this.dataSearchCopy = objData.data;
        },

        /* metodos para procesar información */
        async searchData() {
            let search = "";
            search = this.txtSearch.toLowerCase();
            this.dataSearch = [...this.dataSearchCopy.filter(e=>e.id.toLowerCase().includes(search) || e.nombre.toLowerCase().includes(search) )];
            this.txtResultados = this.indicadores.length;
        },
        
        nextItem:function(type){
            let id = this.id;
            let index = this.arrConsecutivos.findIndex(function(e){return e.id == id});
            if(type=="next" && this.arrConsecutivos[++index]){
                id = this.arrConsecutivos[index++].id;
            }else if(type=="prev" && this.arrConsecutivos[--index]){
                id = this.arrConsecutivos[index--].id;
            }
            window.location.href="plan-editaFuentesPpi"+'?id='+id;
        },

        /* Metodos para guardar o actualizar información */
        async save() {
            if (this.nombre == "") {
                Swal.fire("Atención!","Todos los campos con * son obligatorios","warning");
                return false;
            }

            const formData = new FormData();
            formData.append("action","save");
            formData.append("nombre",this.nombre);

            Swal.fire({
                title:"¿Estás segur@ de guardar?",
                text:"",
                icon: 'warning',
                showCancelButton:true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText:"Sí, guardar",
                cancelButtonText:"No, cancelar"
            }).then(async function(result) {
                if(result.isConfirmed) {
                    const response = await fetch(URL,{method:"POST",body:formData});
                    const objData = await response.json();

                    if (objData.status) {
                        Swal.fire("Guardado",objData.msg,"success");
                        setTimeout(function(){
                            location.href = "plan-editaFuentesPpi?id="+objData.id;
                        },1000);
                    }
                    else {
                        Swal.fire("Error!",objData.msg,"warning");
                    }
                }
            });
        },

        async saveEdit() {
            if (this.nombre == "") {
                Swal.fire("Atención!","Todos los campos con * son obligatorios","warning");
                return false;
            }

            const formData = new FormData();
            formData.append("action","saveEdit");
            formData.append("id",this.id);
            formData.append("nombre",this.nombre);

            Swal.fire({
                title:"¿Estás segur@ de guardar?",
                text:"",
                icon: 'warning',
                showCancelButton:true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText:"Sí, guardar",
                cancelButtonText:"No, cancelar"
            }).then(async function(result) {
                if(result.isConfirmed) {
                    const response = await fetch(URL,{method:"POST",body:formData});
                    const objData = await response.json();
                    if (objData.status) {
                        Swal.fire("Guardado",objData.msg,"success");
                    }
                    else {
                        Swal.fire("Error!",objData.msg,"warning");
                    }
                }
            });
        },

        /* Formatos para mostrar información */
    },
    computed:{

    }
})
