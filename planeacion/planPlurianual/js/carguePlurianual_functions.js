const URL = "planeacion/planPlurianual/controllers/carguePlurianual_controller.php";

var app = new Vue({
    el:"#myapp",
    data() {
        return {
            isLoading: false,
            inputFile: "",
            planes: [],
            pdtId: '',
        }
    },
    mounted() {
        this.get();
    },
    methods: {
        /* metodos para traer información */
       
        async get() {
            const formData = new FormData();
            formData.append("action","get");
            const response = await fetch(URL,{method:"POST",body:formData});
            const objData = await response.json();
            this.planes = objData.planes;
        },

        /* metodos para procesar información */
        getFile(element){
            this.inputFile = element.event.target.files;
        },       
        /* Metodos para guardar o actualizar información */
        async upload() {
            if (this.pdtId == "") {
                Swal.fire("Error","Debe seleccionar el PDT.","error");
                return false;
            }

            let inputFile = this.inputFile;

            if (this.pdtId == "") {
                Swal.fire("Error","Debe seleccionar el PDT.","error");
                return false;
            }

            if(inputFile.length == 0){
                Swal.fire("Error","Debe subir la plantilla.","error");
                return false;
            }

            let file = inputFile[0];
            let extension = file.name.split(".")[1];

            if(extension != "xlsx"){
                Swal.fire("Error","El archivo es incorrecto; por favor, utiliza nuestra plantilla.","error");
                return false;
            }

            let formData = new FormData();
            formData.append("action","upload");
            formData.append("file",file);
            formData.append("pdtId",this.pdtId);

            Swal.fire({
                title:"¿Estás segur@ de cargar?",
                text:"",
                icon: 'warning',
                showCancelButton:true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText:"Sí, guardar",
                cancelButtonText:"No, cancelar"
            }).then(async function(result){
                if(result.isConfirmed){
                    // app.isLoading = true;
                    const response = await fetch(URL,{method:"POST",body:formData});
                    const objData = await response.json();
                    app.isLoading = false;
                    if(objData.status){
                        Swal.fire("Proceso con exito",objData.msg,"success");
                        setTimeout(function(){
                            window.location.href="plan-buscaPlurianual.php";
                        },1500);
                    }else{
                        Swal.fire("Error",objData.msg,"error");
                    }
                }
            });
        },
        
        /* Formatos para mostrar información */
        downloadPlantilla() {
            if (this.pdtId == "") {
                Swal.fire("Error","Debe seleccionar el PDT.","error");
                return false;
            }
            
            const form = document.createElement("form");
            form.method = "post";
            form.target = "_blank";
            form.action = "plan-formatoPlurianualCarga.php";       
            
            function addField(name,value){
                const input = document.createElement("input");
                input.type="hidden";
                input.name=name;
                input.value = value;
                form.appendChild(input);
            }

            addField("pdtId", this.pdtId);
        
            document.body.appendChild(form);
            form.submit();
            document.body.removeChild(form);
        },
    },
    computed:{

    }
})
