const URL = "planeacion/planPlurianual/controllers/planPlurianual_controller.php";
const URLEXCEL = "plan-plurianualExcel.php";

var app = new Vue({
    el:"#myapp",
    data() {
        return {
            isLoading: false,
            dataSearch: [],
            dataSearchCopy: [],
            txtSearch: '',
            id: 0, 
            arrConsecutivos: [],
            /* tabs */
            tabInformacion: true,
            tabValores: false,
            /* pdt */
            planes: [],
            pdtId: '',
            /* datosFormulario */
            nombre: '',
            /* linea estrategica */
            lineasEstrategicas: [],
            lineaId: '',
            /* sectores */
            modalSectores: false,
            sectores: [],
            sectoresCopy: [],
            sectorCod: '',
            sectorName: '',
            /* programa */
            modalPrograma: false,
            programas: [],
            programasCopy: [],
            programaId: '',
            programaCod: '',
            programaName: '',
            /* fuentes */
            fuentes: [],
            vig01: '',
            vig02: '',
            vig03: '',
            vig04: '',
            valor_total_01: 0,
            valor_total_02: 0,
            valor_total_03: 0,
            valor_total_04: 0,
            valor_total_fuente_vigencia: 0,
        }
    },
    mounted() {
        this.intPageVal = this.$refs.pageType.value;

        if (this.intPageVal == 1) {
           this.get();
        } else if (this.intPageVal == 2) {
            this.getSearch();
        } else if (this.intPageVal == 3) {
            this.getEdit();
        }
    },
    methods: {
        /* metodos para traer información */
        async get() {
            const formData = new FormData();
            formData.append("action","get");
            const response = await fetch(URL,{method:"POST",body:formData});
            const objData = await response.json();
            this.planes = objData.planes;
            this.sectoresCopy = this.sectores = objData.sectores;
            this.fuentes = objData.fuentes;
        },

        async getData() {
            this.lineasEstrategicas = [];
            this.lineaId = this.vig01 = this.vig02 = this.vig03 = this.vig04 = "";

            if (this.pdtId == "") {
                return false;
            } 

            const formData = new FormData();
            formData.append("action","getDataPdt");
            formData.append("pdtId",this.pdtId);
            const response = await fetch(URL,{method:"POST",body:formData});
            const objData = await response.json();

            this.lineasEstrategicas = objData.ejes;
            this.vig01 = objData.periodoGobierno[0];
            this.vig02 = objData.periodoGobierno[1];
            this.vig03 = objData.periodoGobierno[2];
            this.vig04 = objData.periodoGobierno[3];
            
        },

        async deployModal(opcion) {
            const formData = new FormData();

            if (opcion == "programa") {
                if (this.sectorCod == "") {
                    Swal.fire("Atención!","Debes seleccionar primero el sector","warning");
                    return false;
                }
                formData.append("action","getPrograma");
                formData.append("sectorCod",this.sectorCod);
            }
            
            const response = await fetch(URL,{method:"POST",body:formData});
            const objData = await response.json();

            if (opcion == "programa") {
                this.programas = this.programasCopy = objData.programas;
                this.modalPrograma = true;
            }
        },

        async getEdit() {
            this.id = new URLSearchParams(window.location.search).get('id');
            const formData = new FormData();
            formData.append("action","getEdit");
            formData.append("id", this.id);
            const response = await fetch(URL,{method:"POST",body:formData});
            const objData = await response.json();
            this.arrConsecutivos = objData.consecutivos;
            this.planes = objData.planes;
            this.sectoresCopy = this.sectores = objData.sectores;
            this.pdtId = objData.data.pdt_id;
            this.lineaId = objData.data.eje_estrategico_id;
            this.nombre = objData.data.nombre;
            this.sectorCod = objData.data.sectorCod;
            this.sectorName = objData.data.sectorName;
            this.programaId = objData.data.programa_id;
            this.programaCod = objData.data.programaCod;
            this.programaName = objData.data.programaName;
            this.lineasEstrategicas = objData.ejes;
            this.vig01 = objData.periodoGobierno[0];
            this.vig02 = objData.periodoGobierno[1];
            this.vig03 = objData.periodoGobierno[2];
            this.vig04 = objData.periodoGobierno[3];
            this.fuentes = objData.dataFuentes;
            for (let i = 0; i < this.fuentes.length; i++) {
                this.fuentes[i].valor_01_format = this.formatNumber(this.fuentes[i].valor_01_format);
                this.fuentes[i].valor_02_format = this.formatNumber(this.fuentes[i].valor_02_format);
                this.fuentes[i].valor_03_format = this.formatNumber(this.fuentes[i].valor_03_format);
                this.fuentes[i].valor_04_format = this.formatNumber(this.fuentes[i].valor_04_format);
            }
            this.sumaTotales();
        },

        async getSearch() {
            const formData = new FormData();
            formData.append("action","getSearch");
            const response = await fetch(URL,{method:"POST",body:formData});
            const objData = await response.json();
            this.dataSearch = this.dataSearchCopy = objData.data;
        },

        /* metodos para procesar información */
        searchData(option) {
            let search = "";
            switch (option) {
                case "modalSector":
                    search = this.txtSearch.toLowerCase();
                    this.sectores = [...this.sectoresCopy.filter(e=>e.codigo.toLowerCase().includes(search) || e.nombre.toLowerCase().includes(search) )];
                    break;

                case "modalPrograma":
                    search = this.txtSearch.toLowerCase();
                    this.programas = [...this.programasCopy.filter(e=>e.codigo.toLowerCase().includes(search) || e.nombre.toLowerCase().includes(search) )];
                    break;

                case "inputSearch":
                    search = this.txtSearch.toLowerCase();
                    this.dataSearch = [...this.dataSearchCopy.filter(e=>e.id.toLowerCase().includes(search) || e.nombre.toLowerCase().includes(search) )];
                    break;
                
                default:
                    console.log("Error, opción no encontrada");
                    break;
            }    
        },

        selectItem(option, item) {
            switch (option) {
                case "selectSector":
                    this.programaCod = this.programaName = "";
                    this.sectorCod = item.codigo;
                    this.sectorName = item.nombre;
                    this.modalSectores = false;
                    this.sectores = this.sectoresCopy;
                    this.txtSearch = "";
                    break

                case "selectPrograma":
                    this.programaId = item.id;
                    this.programaCod = item.codigo;
                    this.programaName = item.nombre;
                    this.modalPrograma = false;
                    this.programas = this.programasCopy;
                    this.txtSearch = "";
                    break;
            
                default:
                    console.log("Error, opción no encontrada");
                    break;
            }
        },
        
        nextItem:function(type){
            let id = this.id;
            let index = this.arrConsecutivos.findIndex(function(e){return e.id == id});
            if(type=="next" && this.arrConsecutivos[++index]){
                id = this.arrConsecutivos[index++].id;
            }else if(type=="prev" && this.arrConsecutivos[--index]){
                id = this.arrConsecutivos[index--].id;
            }
            window.location.href="plan-editaPlurianual"+'?id='+id;
        },

        sumaTotales() {
            this.valor_total_01 = this.valor_total_02 = this.valor_total_03 = this.valor_total_04 = 0;
            this.fuentes.forEach((fuente, index) => {
                let valor01 = fuente.valor_01 == "" ? 0 : parseFloat(fuente.valor_01);
                let valor02 = fuente.valor_02 == "" ? 0 : parseFloat(fuente.valor_02);
                let valor03 = fuente.valor_03 == "" ? 0 : parseFloat(fuente.valor_03);
                let valor04 = fuente.valor_04 == "" ? 0 : parseFloat(fuente.valor_04);
                this.valor_total_01 = parseFloat(this.valor_total_01 + valor01);
                this.valor_total_02 = parseFloat(this.valor_total_02 + valor02);
                this.valor_total_03 = parseFloat(this.valor_total_03 + valor03);
                this.valor_total_04 = parseFloat(this.valor_total_04 + valor04);
                fuente.valor_total_fuente = valor01 + valor02 + valor03 + valor04;
                this.valor_total_fuente_vigencia = this.valor_total_01 + this.valor_total_02 + this.valor_total_03 + this.valor_total_04;
            });
        },

        /* Metodos para guardar o actualizar información */
        async save() {
            if (this.pdtId == "") {
                Swal.fire("Atención!","Debes seleccionar el PDT!","warning");
                return false;
            }

            if (this.lineaId == "" || this.nombre == "" || this.programaId == "") {
                Swal.fire("Atención!","Todos los campos con (*) son obligatorios","warning");
                return false;
            }

            const formData = new FormData();
            formData.append("action","save");
            formData.append("pdtId",this.pdtId);
            formData.append("lineaId",this.lineaId);
            formData.append("nombre",this.nombre);
            formData.append("programaId",this.programaId);
            formData.append("fuentes", JSON.stringify(this.fuentes));

            Swal.fire({
                title:"¿Estás segur@ de guardar?",
                text:"",
                icon: 'warning',
                showCancelButton:true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText:"Sí, guardar",
                cancelButtonText:"No, cancelar"
            }).then(async function(result) {
                if(result.isConfirmed) {
                    const response = await fetch(URL,{method:"POST",body:formData});
                    const objData = await response.json();

                    if (objData.status) {
                        Swal.fire("Guardado",objData.msg,"success");
                        setTimeout(function(){
                            location.href = "plan-editaPlurianual?id="+objData.id;
                        },1000);
                    }
                    else {
                        Swal.fire("Error!",objData.msg,"warning");
                    }
                }
            });
        },

        async saveEdit() {
            if (this.pdtId == "") {
                Swal.fire("Atención!","Debes seleccionar el PDT!","warning");
                return false;
            }

            if (this.lineaId == "" || this.nombre == "" || this.programaId == "") {
                Swal.fire("Atención!","Todos los campos con (*) son obligatorios","warning");
                return false;
            }

            const formData = new FormData();
            formData.append("action","saveEdit");
            formData.append("id",this.id);
            formData.append("pdtId",this.pdtId);
            formData.append("lineaId",this.lineaId);
            formData.append("nombre",this.nombre);
            formData.append("programaId",this.programaId);
            formData.append("fuentes", JSON.stringify(this.fuentes));

            Swal.fire({
                title:"¿Estás segur@ de guardar?",
                text:"",
                icon: 'warning',
                showCancelButton:true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText:"Sí, guardar",
                cancelButtonText:"No, cancelar"
            }).then(async function(result) {
                if(result.isConfirmed) {
                    const response = await fetch(URL,{method:"POST",body:formData});
                    const objData = await response.json();
                    if (objData.status) {
                        Swal.fire("Guardado",objData.msg,"success");
                    }
                    else {
                        Swal.fire("Error!",objData.msg,"warning");
                    }
                }
            });
        },

        /* Formatos para mostrar información */
        formatInputNumberArray(conFormato, sinFormato, index) {
            this.fuentes[index][conFormato] = this.formatNumber(this.fuentes[index][conFormato]);
            this.fuentes[index][sinFormato] = this.fuentes[index][conFormato].replace(/[^0-9.]/g, "");
            this.sumaTotales();
        },

        formatNumber(number) {
            if (!number) return ""; // Si no hay entrada, retorna vacío
    
            // Elimina caracteres no válidos (excepto números y el punto decimal)
            number = number.replace(/[^0-9.]/g, "");
    
            // Divide la parte entera y decimal
            const [integerPart, decimalPart] = number.split(".");
    
            // Formatear la parte entera con separador de miles
            const formattedInteger = integerPart.replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    
            // Si hay parte decimal, la une; si no, devuelve solo la parte entera
            return decimalPart !== undefined
                ? `${formattedInteger}.${decimalPart.slice(0, 2)}` // Limita a 2 decimales si es necesario
                : formattedInteger;
        },

        viewFormatNumber: function(valor){
            return new Intl.NumberFormat("en-US", {style: "currency", currency: "USD"}).format(valor);  
        },

        /* enviar informacion excel o pdf */
        excel() {
            const form = document.createElement("form");
            form.method = "post";
            form.target = "_blank";
            form.action = URLEXCEL;       
            
            document.body.appendChild(form);
            form.submit();
            document.body.removeChild(form);
        }
    },
    computed:{

    }
})
