<?php
    require_once '../../../Librerias/core/Helpers.php';
    require_once '../models/fuentesPpi_model.php';
    session_start();
    header('Content-Type: application/json');

    class fuenteController extends fuenteModel {
        /* crear */

        public function save() {
            if (!empty($_SESSION)) {
                $nombre = strClean(replaceChar($_POST["nombre"]));
                $id = $this->insertFuente($nombre);
                if ($id > 0) {
                    $arrResponse = array("status" => true, "msg" => "Fuente guardada con exito", "id" => $id);
                }
                else {
                    $arrResponse = array("status" => false, "msg" => "Error, vuelva a intentar");
                }
                
                echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
            }
            die();
        }

        /* buscar */
        public function getSearch() {
            if (!empty($_SESSION)) {
                $arrResponse = array("status" => true, "data" => $this->dataSearch());
                echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
            }
            die();
        }

        /* editar */
        public function getEdit() {
            if (!empty($_SESSION)) {
                $id = (int) $_POST["id"];
                $arrResponse = array("status" => true, "consecutivos" => $this->getConsecutivos(), "data" => $this->dataEdit($id));
                echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
            }
            die();
        }

        public function saveEdit() {
            if (!empty($_SESSION)) {
                $nombre = strClean(replaceChar($_POST["nombre"]));
                $id = $_POST["id"];
                $request = $this->updateFuente($id, $nombre);
                if ($request > 0) {
                    $arrResponse = array("status" => true, "msg" => "Fuente actualizada con exito");
                }
                else {
                    $arrResponse = array("status" => false, "msg" => "Error, vuelva a intentar");
                }

                echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
            }
            die();
        }
    }

    if($_POST){
        $obj = new fuenteController();
        $accion = $_POST["action"];
        
        if ($accion == 'save') {
            $obj->save();
        } else if ($accion == "getEdit") {
            $obj->getEdit();
        } else if ($accion == "saveEdit") {
            $obj->saveEdit();
        } else if ($accion == "getSearch") {
            $obj->getSearch();
        }
    }
?>