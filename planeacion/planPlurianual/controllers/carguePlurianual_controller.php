<?php
    require_once "../../../PHPExcel/Classes/PHPExcel.php";
    require_once '../../../Librerias/core/Helpers.php';
    require_once '../models/carguePlurianual_model.php';
    session_start();
    header('Content-Type: application/json');

    class plurianualController extends plurianualModel {
        public function get() {
            if (!empty($_SESSION)) {
                $arrResponse = array("planes" => $this->planesDesarrolloTerritorial());
                echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
            }
        }

        public function upload() {
            if (!empty($_SESSION)) {
                $data = [];
                $error = false;
                $pdtId = $_POST["pdtId"];
                $tmpfname = $_FILES['file']['tmp_name'];
                $excelReader = PHPExcel_IOFactory::createReaderForFile($tmpfname);
                $excelObj = $excelReader->load($tmpfname);
                $hojaDatos = $excelObj->getSheet(0);
                $lastRowFormatos = $hojaDatos->getHighestRow();
                
                for ($i=4; $i <= $lastRowFormatos ; $i++) {
                    $fuente_01 = $fuente_02 = $fuente_03  = $fuente_04  = $fuente_05 = $fuente_06  = $fuente_07  = $fuente_08  = $fuente_09  = $fuente_10  = $fuente_11  = $fuente_12  = $fuente_13  = $fuente_14  = $fuente_15 = ["valor_01" => 0, "valor_02" => 0, "valor_03" => 0, "valor_04" => 0];
                    $dataTemp = [];

                    $lineaEstrategica = $hojaDatos->getCell("A$i")->getValue();
                    $nombre = strClean(replaceChar($hojaDatos->getCell("B$i")->getValue()));
                    $abreviacion = substr($lineaEstrategica, 0, 2);
                    if ($abreviacion == "LE") {
                        $id = str_replace("LE","",$lineaEstrategica);
                        if ($this->validaEjeEstrategico ($id) == 0) {
                            $error = true;
                            $arrResponse = array("status" => false, "msg" => "Error en la linea estrategica en la fila $i, revise los datos del formato");
                            goto salir;
                        }
                    } else {
                        $error = true;
                        $arrResponse = array("status" => false, "msg" => "Error en la abreviación en linea estrategica en la fila $i, revise los datos del formato");
                        goto salir;
                    }

                    $programaCod = $hojaDatos->getCell("C$i")->getValue();

                    if ($this->validaPrograma($programaCod) == 0) {
                        $error = true;
                        $arrResponse = array("status" => false, "msg" => "Error en el programa en la fila $i, revise los datos del formato");
                        goto salir;
                    }
                    
                    $fuente_01["valor_01"] = is_numeric($hojaDatos->getCell("D$i")->getValue()) ? doubleval($hojaDatos->getCell("D$i")->getValue()) : 0;
                    $fuente_01["valor_02"] = is_numeric($hojaDatos->getCell("S$i")->getValue()) ? doubleval($hojaDatos->getCell("S$i")->getValue()) : 0;
                    $fuente_01["valor_03"] = is_numeric($hojaDatos->getCell("AH$i")->getValue()) ? doubleval($hojaDatos->getCell("AH$i")->getValue()) : 0;
                    $fuente_01["valor_04"] = is_numeric($hojaDatos->getCell("AW$i")->getValue()) ? doubleval($hojaDatos->getCell("AW$i")->getValue()) : 0;
                    
                    $fuente_02["valor_01"] = is_numeric($hojaDatos->getCell("E$i")->getValue()) ? doubleval($hojaDatos->getCell("E$i")->getValue()) : 0;
                    $fuente_02["valor_02"] = is_numeric($hojaDatos->getCell("T$i")->getValue()) ? doubleval($hojaDatos->getCell("T$i")->getValue()) : 0;
                    $fuente_02["valor_03"] = is_numeric($hojaDatos->getCell("AI$i")->getValue()) ? doubleval($hojaDatos->getCell("AI$i")->getValue()) : 0;
                    $fuente_02["valor_04"] = is_numeric($hojaDatos->getCell("AX$i")->getValue()) ? doubleval($hojaDatos->getCell("AX$i")->getValue()) : 0;

                    $fuente_03["valor_01"] = is_numeric($hojaDatos->getCell("F$i")->getValue()) ? doubleval($hojaDatos->getCell("F$i")->getValue()) : 0;
                    $fuente_03["valor_02"] = is_numeric($hojaDatos->getCell("U$i")->getValue()) ? doubleval($hojaDatos->getCell("U$i")->getValue()) : 0;
                    $fuente_03["valor_03"] = is_numeric($hojaDatos->getCell("AJ$i")->getValue()) ? doubleval($hojaDatos->getCell("AJ$i")->getValue()) : 0;
                    $fuente_03["valor_04"] = is_numeric($hojaDatos->getCell("AY$i")->getValue()) ? doubleval($hojaDatos->getCell("AY$i")->getValue()) : 0;

                    $fuente_04["valor_01"] = is_numeric($hojaDatos->getCell("G$i")->getValue()) ? doubleval($hojaDatos->getCell("G$i")->getValue()) : 0;
                    $fuente_04["valor_02"] = is_numeric($hojaDatos->getCell("V$i")->getValue()) ? doubleval($hojaDatos->getCell("V$i")->getValue()) : 0;
                    $fuente_04["valor_03"] = is_numeric($hojaDatos->getCell("AK$i")->getValue()) ? doubleval($hojaDatos->getCell("AK$i")->getValue()) : 0;
                    $fuente_04["valor_04"] = is_numeric($hojaDatos->getCell("AZ$i")->getValue()) ? doubleval($hojaDatos->getCell("AZ$i")->getValue()) : 0;

                    $fuente_05["valor_01"] = is_numeric($hojaDatos->getCell("H$i")->getValue()) ? doubleval($hojaDatos->getCell("H$i")->getValue()) : 0;
                    $fuente_05["valor_02"] = is_numeric($hojaDatos->getCell("W$i")->getValue()) ? doubleval($hojaDatos->getCell("W$i")->getValue()) : 0;
                    $fuente_05["valor_03"] = is_numeric($hojaDatos->getCell("AL$i")->getValue()) ? doubleval($hojaDatos->getCell("AL$i")->getValue()) : 0;
                    $fuente_05["valor_04"] = is_numeric($hojaDatos->getCell("BA$i")->getValue()) ? doubleval($hojaDatos->getCell("BA$i")->getValue()) : 0;

                    $fuente_06["valor_01"] = is_numeric($hojaDatos->getCell("I$i")->getValue()) ? doubleval($hojaDatos->getCell("I$i")->getValue()) : 0;
                    $fuente_06["valor_02"] = is_numeric($hojaDatos->getCell("X$i")->getValue()) ? doubleval($hojaDatos->getCell("X$i")->getValue()) : 0;
                    $fuente_06["valor_03"] = is_numeric($hojaDatos->getCell("AM$i")->getValue()) ? doubleval($hojaDatos->getCell("AM$i")->getValue()) : 0;
                    $fuente_06["valor_04"] = is_numeric($hojaDatos->getCell("BB$i")->getValue()) ? doubleval($hojaDatos->getCell("BB$i")->getValue()) : 0;

                    $fuente_07["valor_01"] = is_numeric($hojaDatos->getCell("J$i")->getValue()) ? doubleval($hojaDatos->getCell("J$i")->getValue()) : 0;
                    $fuente_07["valor_02"] = is_numeric($hojaDatos->getCell("Y$i")->getValue()) ? doubleval($hojaDatos->getCell("Y$i")->getValue()) : 0;
                    $fuente_07["valor_03"] = is_numeric($hojaDatos->getCell("AN$i")->getValue()) ? doubleval($hojaDatos->getCell("AN$i")->getValue()) : 0;
                    $fuente_07["valor_04"] = is_numeric($hojaDatos->getCell("BC$i")->getValue()) ? doubleval($hojaDatos->getCell("BC$i")->getValue()) : 0;

                    $fuente_08["valor_01"] = is_numeric($hojaDatos->getCell("K$i")->getValue()) ? doubleval($hojaDatos->getCell("K$i")->getValue()) : 0;
                    $fuente_08["valor_02"] = is_numeric($hojaDatos->getCell("Z$i")->getValue()) ? doubleval($hojaDatos->getCell("Z$i")->getValue()) : 0;
                    $fuente_08["valor_03"] = is_numeric($hojaDatos->getCell("AO$i")->getValue()) ? doubleval($hojaDatos->getCell("AO$i")->getValue()) : 0;
                    $fuente_08["valor_04"] = is_numeric($hojaDatos->getCell("BD$i")->getValue()) ? doubleval($hojaDatos->getCell("BD$i")->getValue()) : 0;

                    $fuente_09["valor_01"] = is_numeric($hojaDatos->getCell("L$i")->getValue()) ? doubleval($hojaDatos->getCell("L$i")->getValue()) : 0;
                    $fuente_09["valor_02"] = is_numeric($hojaDatos->getCell("AA$i")->getValue()) ? doubleval($hojaDatos->getCell("AA$i")->getValue()) : 0;
                    $fuente_09["valor_03"] = is_numeric($hojaDatos->getCell("AP$i")->getValue()) ? doubleval($hojaDatos->getCell("AP$i")->getValue()) : 0;
                    $fuente_09["valor_04"] = is_numeric($hojaDatos->getCell("BE$i")->getValue()) ? doubleval($hojaDatos->getCell("BE$i")->getValue()) : 0;

                    $fuente_10["valor_01"] = is_numeric($hojaDatos->getCell("M$i")->getValue()) ? doubleval($hojaDatos->getCell("M$i")->getValue()) : 0;
                    $fuente_10["valor_02"] = is_numeric($hojaDatos->getCell("AB$i")->getValue()) ? doubleval($hojaDatos->getCell("AB$i")->getValue()) : 0;
                    $fuente_10["valor_03"] = is_numeric($hojaDatos->getCell("AQ$i")->getValue()) ? doubleval($hojaDatos->getCell("AQ$i")->getValue()) : 0;
                    $fuente_10["valor_04"] = is_numeric($hojaDatos->getCell("BF$i")->getValue()) ? doubleval($hojaDatos->getCell("BF$i")->getValue()) : 0;

                    $fuente_11["valor_01"] = is_numeric($hojaDatos->getCell("N$i")->getValue()) ? doubleval($hojaDatos->getCell("N$i")->getValue()) : 0;
                    $fuente_11["valor_02"] = is_numeric($hojaDatos->getCell("AC$i")->getValue()) ? doubleval($hojaDatos->getCell("AC$i")->getValue()) : 0;
                    $fuente_11["valor_03"] = is_numeric($hojaDatos->getCell("AR$i")->getValue()) ? doubleval($hojaDatos->getCell("AR$i")->getValue()) : 0;
                    $fuente_11["valor_04"] = is_numeric($hojaDatos->getCell("BG$i")->getValue()) ? doubleval($hojaDatos->getCell("BG$i")->getValue()) : 0;

                    $fuente_12["valor_01"] = is_numeric($hojaDatos->getCell("O$i")->getValue()) ? doubleval($hojaDatos->getCell("O$i")->getValue()) : 0;
                    $fuente_12["valor_02"] = is_numeric($hojaDatos->getCell("AD$i")->getValue()) ? doubleval($hojaDatos->getCell("AD$i")->getValue()) : 0;
                    $fuente_12["valor_03"] = is_numeric($hojaDatos->getCell("AS$i")->getValue()) ? doubleval($hojaDatos->getCell("AS$i")->getValue()) : 0;
                    $fuente_12["valor_04"] = is_numeric($hojaDatos->getCell("BH$i")->getValue()) ? doubleval($hojaDatos->getCell("BH$i")->getValue()) : 0;

                    $fuente_13["valor_01"] = is_numeric($hojaDatos->getCell("P$i")->getValue()) ? doubleval($hojaDatos->getCell("P$i")->getValue()) : 0;
                    $fuente_13["valor_02"] = is_numeric($hojaDatos->getCell("AE$i")->getValue()) ? doubleval($hojaDatos->getCell("AE$i")->getValue()) : 0;
                    $fuente_13["valor_03"] = is_numeric($hojaDatos->getCell("AT$i")->getValue()) ? doubleval($hojaDatos->getCell("AT$i")->getValue()) : 0;
                    $fuente_13["valor_04"] = is_numeric($hojaDatos->getCell("BI$i")->getValue()) ? doubleval($hojaDatos->getCell("BI$i")->getValue()) : 0;

                    $fuente_14["valor_01"] = is_numeric($hojaDatos->getCell("Q$i")->getValue()) ? doubleval($hojaDatos->getCell("Q$i")->getValue()) : 0;
                    $fuente_14["valor_02"] = is_numeric($hojaDatos->getCell("AF$i")->getValue()) ? doubleval($hojaDatos->getCell("AF$i")->getValue()) : 0;
                    $fuente_14["valor_03"] = is_numeric($hojaDatos->getCell("AU$i")->getValue()) ? doubleval($hojaDatos->getCell("AU$i")->getValue()) : 0;
                    $fuente_14["valor_04"] = is_numeric($hojaDatos->getCell("BJ$i")->getValue()) ? doubleval($hojaDatos->getCell("BJ$i")->getValue()) : 0;

                    $fuente_15["valor_01"] = is_numeric($hojaDatos->getCell("R$i")->getValue()) ? doubleval($hojaDatos->getCell("R$i")->getValue()) : 0;
                    $fuente_15["valor_02"] = is_numeric($hojaDatos->getCell("AG$i")->getValue()) ? doubleval($hojaDatos->getCell("AG$i")->getValue()) : 0;
                    $fuente_15["valor_03"] = is_numeric($hojaDatos->getCell("AV$i")->getValue()) ? doubleval($hojaDatos->getCell("AV$i")->getValue()) : 0;
                    $fuente_15["valor_04"] = is_numeric($hojaDatos->getCell("BK$i")->getValue()) ? doubleval($hojaDatos->getCell("BK$i")->getValue()) : 0;

                    $dataTemp = [
                        "lineaEstrategica" => $lineaEstrategica,
                        "programa" => $programaCod,
                        "nombre" => $nombre,
                        "fuente_01" => $fuente_01,
                        "fuente_02" => $fuente_02,
                        "fuente_03" => $fuente_03,
                        "fuente_04" => $fuente_04,
                        "fuente_05" => $fuente_05,
                        "fuente_06" => $fuente_06,
                        "fuente_07" => $fuente_07,
                        "fuente_08" => $fuente_08,
                        "fuente_09" => $fuente_09,
                        "fuente_10" => $fuente_10,
                        "fuente_11" => $fuente_11,
                        "fuente_12" => $fuente_12,
                        "fuente_13" => $fuente_13,
                        "fuente_14" => $fuente_14,
                        "fuente_15" => $fuente_15
                    ];
                    array_push($data, $dataTemp);
                }

                salir:

                if ($error == false) {
                    $this->insertData($pdtId, $data);
                    $arrResponse = array("status" => true, "msg" => "El archivo ha sido cargado correctamente correctamente.");
                }
                
                echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
            }
            die();
        }

    }

    if($_POST){
        $obj = new plurianualController();
        $accion = $_POST["action"];

        if ($accion == 'upload') {
            $obj->upload();
        } else if ($accion == "get") {
            $obj->get();
        }
    }
?>