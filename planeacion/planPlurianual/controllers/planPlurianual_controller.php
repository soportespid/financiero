<?php
    require_once '../../../Librerias/core/Helpers.php';
    require_once '../models/planPlurianual_model.php';
    session_start();
    header('Content-Type: application/json');

    class plurianualController extends plurianualModel {
        /* crear */
        public function get() {
            if (!empty($_SESSION)) {
                $arrResponse = array("status" => true, "planes" => $this->planesDesarrolloTerritorial(), "fuentes" => $this->fuentes(), "sectores" => $this->sectores());
                echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
            }
            die();
        }

        public function getDataPdt() {
            if (!empty($_SESSION)) {
                $pdtId = $_POST["pdtId"];
                $arrResponse = array("status" => true, "ejes" => $this->getEjesEstrategicos($pdtId), "periodoGobierno" => $this->periodoGobierno($pdtId));
                echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
            }
            die();
        }

        public function getPrograma() {
            if (!empty($_SESSION)) {
                $sectorCod = $_POST["sectorCod"];
                $arrResponse = array("status" => true, "programas" => $this->programas($sectorCod));
                echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
            }
            die();
        }

        public function save() {
            if (!empty($_SESSION)) {
                $dataValues = [
                    $_POST["pdtId"],
                    $_POST["lineaId"],
                    $_POST["programaId"],
                    strClean(replaceChar($_POST["nombre"])),
                    "S"
                ];
                $fuentes = json_decode($_POST['fuentes'],true);
                $id = $this->insertPlanPlurianual($dataValues);
                if ($id > 0) {
                    $this->insertPlanPlurianualFuentes($fuentes, $id);
                    $arrResponse = array("status" => true, "msg" => "Plan plurianual creado con exito", "id" => $id);
                }
                else {
                    $arrResponse = array("status" => false, "msg" => "Error en guardado, vuelva a intentar");
                }
                
                echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
            }
            die();
        }

        /* buscar */
        public function getSearch() {
            if (!empty($_SESSION)) {
                $arrResponse = array("status" => true, "data" => $this->getDataSearch());
                echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
            }
            die();
        }

        /* editar */
        public function getEdit() {
            if (!empty($_SESSION)) {
                $id = (int) $_POST["id"];
                $data = $this->getDataEdit($id);
                $arrResponse = [
                    "status" => true, 
                    "consecutivos" => $this->getConsecutivos(), 
                    "data" => $data,
                    "dataFuentes" => $this->getFuentesEdit($id),
                    "planes" => $this->planesDesarrolloTerritorial(),
                    "sectores" => $this->sectores(),
                    "ejes" => $this->getEjesEstrategicos($data["pdt_id"]),
                    "periodoGobierno" => $this->periodoGobierno($data["pdt_id"])
                ];
                  
                echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
            }
            die();
        }

        public function saveEdit() {
            if (!empty($_SESSION)) {
                $id = $_POST["id"];
                $dataValues = [
                    $_POST["pdtId"],
                    $_POST["lineaId"],
                    $_POST["programaId"],
                    strClean(replaceChar($_POST["nombre"]))
                ];
                $fuentes = json_decode($_POST['fuentes'],true);

                $request = $this->updatePlanPlurianual($id, $dataValues);
                if ($request > 0) {
                    $this->insertPlanPlurianualFuentes($fuentes, $id);
                    $arrResponse = array("status" => true, "msg" => "Plan plurianual actualizado con exito", "id" => $id);
                }
                else {
                    $arrResponse = array("status" => false, "msg" => "Error en actualizar, vuelva a intentar");
                }
                echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
            }
            die();
        }
    }

    if($_POST){
        $obj = new plurianualController();
        $accion = $_POST["action"];
        
        if ($accion == 'save') {
            $obj->save();
        } elseif($accion == "get") {
            $obj->get();
        } else if ($accion == "getDataPdt") {
            $obj->getDataPdt();
        }else if ($accion == "getEdit") {
            $obj->getEdit();
        } else if ($accion == "saveEdit") {
            $obj->saveEdit();
        } else if ($accion == "getSearch") {
            $obj->getSearch();
        } else if ($accion == "getPrograma") {
            $obj->getPrograma();
        }
    }
?>