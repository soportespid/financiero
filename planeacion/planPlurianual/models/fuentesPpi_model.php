<?php
    require_once '../../../Librerias/core/Helpers.php';
    require_once '../../../Librerias/core/Mysql.php';
    session_start();
    
    class fuenteModel extends Mysql {
        public function insertFuente(string $nombre) {
            $sql = "INSERT INTO plan_fuentes_ppi (nombre) VALUES (?)";
            $request = $this->insert($sql, [$nombre]);
            return $request;
        }

        public function dataEdit(int $id) {
            $sql = "SELECT nombre FROM plan_fuentes_ppi WHERE id = $id";
            $data = $this->select($sql);
            return $data;
        }

        public function getConsecutivos() {
            $sql = "SELECT id FROM plan_fuentes_ppi";
            $data = $this->select_all($sql);
            return $data;
        }

        public function updateFuente(int $id, string $nombre) {
            $sql = "UPDATE plan_fuentes_ppi SET nombre = ? WHERE id = $id";
            $request = $this->update($sql, ["$nombre"]);
            return $request;
        }

        public function dataSearch() {
            $sql = "SELECT id, nombre FROM plan_fuentes_ppi ORDER BY id";
            $data = $this->select_all($sql);
            return $data;
        }
    }
?>