<?php
    require_once '../../../Librerias/core/Helpers.php';
    require_once '../../../Librerias/core/Mysql.php';
    session_start();
    
    class plurianualModel extends Mysql{
        private $cedula;
        private $vigencia;

        function __construct(){
            parent::__construct();
            $this->cedula = $_SESSION["cedulausu"];
            $this->vigencia = getVigenciaUsuario($this->cedula);
        }

        public function planesDesarrolloTerritorial() {
            $sql_periodos = "SELECT id AS periodoId FROM para_periodos_gobierno WHERE vigencia_inicial <= '$this->vigencia' AND vigencia_final >= '$this->vigencia' AND estado = 'S'";
            $result = $this->select($sql_periodos);

            $sql = "SELECT id, nombre_pdt FROM plan_pdt WHERE periodo_gobierno_id = $result[periodoId] AND estado = 'S'";
            $data = $this->select_all($sql);
            return $data;
        }

        public function validaEjeEstrategico($id) {            
            $sql = "SELECT COUNT(*) AS total_row FROM plan_eje_estrategico WHERE id = $id";
            $value = $this->select($sql);
            return $value["total_row"];
        }

        public function validaPrograma($id) {
            $sql = "SELECT COUNT(*) AS total_row FROM ccpetprogramas WHERE codigo = $id";
            $value = $this->select($sql);
            return $value["total_row"];
        }

        public function searchProgramaId($programaCod) {
            $sql = "SELECT id FROM ccpetprogramas WHERE codigo = $programaCod";
            $value = $this->select($sql);
            return $value["id"];
        }

        public function insertData(int $pdtId, array $data) {
            foreach ($data as $d) {
                $sql_cab = "INSERT INTO plan_plurianual (pdt_id, eje_estrategico_id, programa_id, nombre, estado) VALUES (?, ?, ?, ?, ?)";
                $sql_det = "INSERT INTO plan_plurianual_det (plurianual_id, id_fuente, valor_vig_01, valor_vig_02, valor_vig_03, valor_vig_04) VALUES (?, ?, ?, ?, ?, ?)";

                $lineaEstrategicaId = str_replace("LE","",$d["lineaEstrategica"]);
                $programaId = $this->searchProgramaId($d["programa"]);

                $value_cab = [$pdtId, $lineaEstrategicaId, $programaId, $d["nombre"], "S"];
                $plurianualId = $this->insert($sql_cab, $value_cab);

                $value_fuente_01 = [$plurianualId, 1, $d["fuente_01"]["valor_01"], $d["fuente_01"]["valor_02"], $d["fuente_01"]["valor_03"], $d["fuente_01"]["valor_04"]];
                $this->insert($sql_det, $value_fuente_01);
                $value_fuente_02 = [$plurianualId, 2, $d["fuente_02"]["valor_01"], $d["fuente_02"]["valor_02"], $d["fuente_02"]["valor_03"], $d["fuente_02"]["valor_04"]];
                $this->insert($sql_det, $value_fuente_02);
                $value_fuente_03 = [$plurianualId, 3, $d["fuente_03"]["valor_01"], $d["fuente_03"]["valor_02"], $d["fuente_03"]["valor_03"], $d["fuente_03"]["valor_04"]];
                $this->insert($sql_det, $value_fuente_03);
                $value_fuente_04 = [$plurianualId, 4, $d["fuente_04"]["valor_01"], $d["fuente_04"]["valor_02"], $d["fuente_04"]["valor_03"], $d["fuente_04"]["valor_04"]];
                $this->insert($sql_det, $value_fuente_04);
                $value_fuente_05 = [$plurianualId, 5, $d["fuente_05"]["valor_01"], $d["fuente_05"]["valor_02"], $d["fuente_05"]["valor_03"], $d["fuente_05"]["valor_04"]];
                $this->insert($sql_det, $value_fuente_05);
                $value_fuente_06 = [$plurianualId, 6, $d["fuente_06"]["valor_01"], $d["fuente_06"]["valor_02"], $d["fuente_06"]["valor_03"], $d["fuente_06"]["valor_04"]];
                $this->insert($sql_det, $value_fuente_06);
                $value_fuente_07 = [$plurianualId, 7, $d["fuente_07"]["valor_01"], $d["fuente_07"]["valor_02"], $d["fuente_07"]["valor_03"], $d["fuente_07"]["valor_04"]];
                $this->insert($sql_det, $value_fuente_07);
                $value_fuente_08 = [$plurianualId, 8, $d["fuente_08"]["valor_01"], $d["fuente_08"]["valor_02"], $d["fuente_08"]["valor_03"], $d["fuente_08"]["valor_04"]];
                $this->insert($sql_det, $value_fuente_08);
                $value_fuente_09 = [$plurianualId, 9, $d["fuente_09"]["valor_01"], $d["fuente_09"]["valor_02"], $d["fuente_09"]["valor_03"], $d["fuente_09"]["valor_04"]];
                $this->insert($sql_det, $value_fuente_09);
                $value_fuente_10 = [$plurianualId, 10, $d["fuente_10"]["valor_01"], $d["fuente_10"]["valor_02"], $d["fuente_10"]["valor_03"], $d["fuente_10"]["valor_04"]];
                $this->insert($sql_det, $value_fuente_10);
                $value_fuente_11 = [$plurianualId, 11, $d["fuente_11"]["valor_01"], $d["fuente_11"]["valor_02"], $d["fuente_11"]["valor_03"], $d["fuente_11"]["valor_04"]];
                $this->insert($sql_det, $value_fuente_11);
                $value_fuente_12 = [$plurianualId, 12, $d["fuente_12"]["valor_01"], $d["fuente_12"]["valor_02"], $d["fuente_12"]["valor_03"], $d["fuente_12"]["valor_04"]];
                $this->insert($sql_det, $value_fuente_12);
                $value_fuente_13 = [$plurianualId, 13, $d["fuente_13"]["valor_01"], $d["fuente_13"]["valor_02"], $d["fuente_13"]["valor_03"], $d["fuente_13"]["valor_04"]];
                $this->insert($sql_det, $value_fuente_13);
                $value_fuente_14 = [$plurianualId, 14, $d["fuente_14"]["valor_01"], $d["fuente_14"]["valor_02"], $d["fuente_14"]["valor_03"], $d["fuente_14"]["valor_04"]];
                $this->insert($sql_det, $value_fuente_14);
                $value_fuente_15 = [$plurianualId, 15, $d["fuente_15"]["valor_01"], $d["fuente_15"]["valor_02"], $d["fuente_15"]["valor_03"], $d["fuente_15"]["valor_04"]];
                $this->insert($sql_det, $value_fuente_15);
            }

            return true;
        }
    }
?>