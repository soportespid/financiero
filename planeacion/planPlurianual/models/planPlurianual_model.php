<?php
    require_once '../../../Librerias/core/Helpers.php';
    require_once '../../../Librerias/core/Mysql.php';
    session_start();
    
    class plurianualModel extends Mysql {
        private $cedula;
        private $vigencia;

        function __construct(){
            parent::__construct();
            $this->cedula = $_SESSION["cedulausu"];
            $this->vigencia = getVigenciaUsuario($this->cedula);
        }

        public function planesDesarrolloTerritorial() {
            $sql_periodos = "SELECT id AS periodoId FROM para_periodos_gobierno WHERE vigencia_inicial <= '$this->vigencia' AND vigencia_final >= '$this->vigencia' AND estado = 'S'";
            $result = $this->select($sql_periodos);

            $sql = "SELECT id, nombre_pdt FROM plan_pdt WHERE periodo_gobierno_id = $result[periodoId] AND estado = 'S'";
            $data = $this->select_all($sql);
            return $data;
        }

        public function getEjesEstrategicos(int $pdtId) {
            $sql = "SELECT id, nombre FROM plan_eje_estrategico WHERE pdt_id = $pdtId AND estado = 'S'";
            $data = $this->select_all($sql);
            return $data;
        }

        public function sectores() {
            $sql = "SELECT codigo, nombre FROM ccpetsectores ORDER BY codigo";
            $data = $this->select_all($sql);
            return $data;
        }

        public function programas(string $sector) {
            $sql = "SELECT id, codigo, nombre FROM ccpetprogramas WHERE codigo LIKE '$sector%'";
            $data = $this->select_all($sql);
            return $data;
        }

        public function periodoGobierno(int $pdtId) {
            $data = [];
            $sql_pdt = "SELECT PG.vigencia_inicial, PG.vigencia_final FROM plan_pdt AS PDT INNER JOIN para_periodos_gobierno AS PG ON PDT.periodo_gobierno_id = PG.id WHERE PDT.id = $pdtId";
            $periodos = $this->select($sql_pdt);

            $vigenciaIni = $periodos["vigencia_inicial"];
            $vigenciaFin = $periodos["vigencia_final"];

            for ($vigenciaIni; $vigenciaIni <= $vigenciaFin; $vigenciaIni++) { 
                $data[] = (int) $vigenciaIni;
            }
            return $data;
        }

        public function fuentes() {
            $sql = "SELECT id, nombre FROM plan_fuentes_ppi ORDER BY id";
            $fuentes = $this->select_all($sql);

            foreach ($fuentes as $key => $fuente) {
                $fuentes[$key]["valor_01"] = 0;
                $fuentes[$key]["valor_01_format"] = 0;
                $fuentes[$key]["valor_02"] = 0;
                $fuentes[$key]["valor_02_format"] = 0;
                $fuentes[$key]["valor_03"] = 0;
                $fuentes[$key]["valor_03_format"] = 0;
                $fuentes[$key]["valor_04"] = 0;
                $fuentes[$key]["valor_04_format"] = 0;
                $fuentes[$key]["valor_total_fuente"] = 0;
            }            

            return $fuentes;
        }

        public function insertPlanPlurianual(array $data) {
            $sql = "INSERT INTO plan_plurianual (pdt_id, eje_estrategico_id, programa_id, nombre, estado) VALUES (?, ?, ?, ?, ?)";
            $result = $this->insert($sql, $data);
            return $result;
        }

        public function insertPlanPlurianualFuentes(array $fuentes, int $id) {
            $sql_delete = "DELETE FROM plan_plurianual_det WHERE plurianual_id = $id";
            $this->delete($sql_delete);

            $sql = "INSERT INTO plan_plurianual_det (plurianual_id, id_fuente, valor_vig_01, valor_vig_02, valor_vig_03, valor_vig_04) VALUES (?, ?, ?, ?, ?, ?)";
            foreach ($fuentes as $fuente) {
                $values = [
                    $id,
                    $fuente["id"],
                    $fuente["valor_01"],
                    $fuente["valor_02"],
                    $fuente["valor_03"],
                    $fuente["valor_04"]
                ];

                $this->insert($sql, $values);
            }
        }

        public function getConsecutivos() {
            $sql = "SELECT id FROM plan_plurianual WHERE estado = 'S'";
            $data = $this->select_all($sql);
            return $data;
        }

        public function getDataEdit(int $id) {
            $sql = "SELECT PP.pdt_id, 
            PP.eje_estrategico_id, 
            PP.programa_id, 
            CP.codigo AS programaCod,
            CP.nombre AS programaName,
            PP.nombre 
            FROM plan_plurianual AS PP
            INNER JOIN ccpetprogramas AS CP
            ON PP.programa_id = CP.id
            WHERE PP.id = $id";
            $data = $this->select($sql);
            $sectorCod = substr($data["programaCod"], 0, 2);
            $sql_sector = "SELECT nombre FROM ccpetsectores WHERE codigo = '$sectorCod'";
            $sector = $this->select($sql_sector);
            $data["sectorCod"] = $sectorCod;
            $data["sectorName"] = $sector["nombre"];
            return $data;
        }

        public function getFuentesEdit(int $id) {
            $sql = "SELECT PPD.id_fuente AS id, PFP.nombre, PPD.valor_vig_01 AS valor_01, PPD.valor_vig_02 AS valor_02, PPD.valor_vig_03 AS valor_03, PPD.valor_vig_04 AS valor_04 
            FROM plan_plurianual_det AS PPD 
            INNER JOIN plan_fuentes_ppi AS PFP
            ON PPD.id_fuente = PFP.id
            WHERE PPD.plurianual_id = $id
            ORDER BY PPD.id_fuente";
            $fuentes = $this->select_all($sql);
            foreach ($fuentes as $key => $fuente) {
                $fuentes[$key]["valor_01_format"] = $fuente["valor_01"];
                $fuentes[$key]["valor_02_format"] = $fuente["valor_02"];
                $fuentes[$key]["valor_03_format"] = $fuente["valor_03"];
                $fuentes[$key]["valor_04_format"] = $fuente["valor_04"];
                // $fuentes[$key]["valor_total_fuente"] = $fuente["valor_01"] + $fuente["valor_02"] + $fuente["valor_03"] + $fuente["valor_04"];
                $fuentes[$key]["valor_total_fuente"] = 0;
            }
            return $fuentes;
        }

        public function updatePlanPlurianual(int $id, array $data) {
            $sql = "UPDATE plan_plurianual SET pdt_id = ?, eje_estrategico_id = ?, programa_id = ?, nombre = ? WHERE id = $id";
            $result = $this->update($sql, $data);
            return $result;
        }
    
        public function getDataSearch() {
            $sql = "SELECT PP.id,
            PDT.nombre_pdt,
            PP.eje_estrategico_id, 
            PP.programa_id, 
            CP.codigo AS programaCod,
            CP.nombre AS programaName,
            PP.nombre 
            FROM plan_plurianual AS PP
            INNER JOIN ccpetprogramas AS CP
            ON PP.programa_id = CP.id
            INNER JOIN plan_pdt AS PDT
            ON PP.pdt_id = PDT.id
            WHERE PP.estado = 'S'";
            $data = $this->select_all($sql);
            foreach ($data as $key => $d) {
                $sectorCod = substr($d["programaCod"], 0, 2);
                $sql_sector = "SELECT nombre FROM ccpetsectores WHERE codigo = '$sectorCod'";
                $sector = $this->select($sql_sector);
                $data[$key]["sectorCod"] = $sectorCod;
                $data[$key]["sectorName"] = $sector["nombre"];
            }
            
            return $data;
        }
    }
?>