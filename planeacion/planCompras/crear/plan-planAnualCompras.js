const URL = 'planeacion/planCompras/crear/plan-planAnualCompras.php';
import { filtroEnArrayDeObjetos } from './../../../funciones.js';

var app = new Vue({
    el: '#myapp',
	data:{

        error: '',
        loading: false, 
        modalidades: [],
        productos: [],
        productosCopy: [],
        showProductos: false,
        searchProducto: '',
        clasificadoresBienes: [],
        clasificadoresBienesCopy: [],
        clasificadoresServicios: [],
        clasificadoresServiciosCopy: [],
        fuentes: [],
        fuentesCopy: [],
        product: '',
        productName: '',
        showBienes: false,
        showServicios: false,
        showCpcBienes: false,
        searchBienes: '',
        cpcBien: '',
        cpcBienName: '',
        showCpcServicios: false,
        searchServicios: '',
        cpcServicio: '',
        cpcServicioName: '',
        groupProducts: [],
        showFuentes: false,
        fuente: '',
        fuenteName: '',
        searchFuente: '',
        groupFuentes: [],
        dias: 0,
        meses: 0,
        modalidad: '',
        descripcion: '',
        valorEstimado: 0,
        valorEstimadoVigActual: 0,
        requiereVigFuturas: '',
        estadoVigFuturas: '',
        contactoResposable: '',
        cargoResponsable: '',
    },

    mounted: function(){

        this.datosIniciales();
    },

    methods: 
    {
        //Desglosar array
        toFormData: function(obj){
            var form_data = new FormData();
            for(var key in obj){
                form_data.append(key, obj[key]);
            }
            return form_data;
        },

        formatonumero: function(valor){
            return new Intl.NumberFormat("en-US", {style: "currency", currency: "USD"}).format(valor);
        },

        datosIniciales: async function() {
            
            this.loading = true;

            await axios.post(URL+'?action=datosIniciales')
            .then((response) => {
                this.modalidades = response.data.modalidades;
                this.clasificadoresBienes = response.data.clasificadoresB;
                this.clasificadoresBienesCopy = response.data.clasificadoresB;
                this.clasificadoresServicios = response.data.clasificadoresS;
                this.clasificadoresServiciosCopy = response.data.clasificadoresS;
                this.fuentes = response.data.fuentes;
                this.fuentesCopy = response.data.fuentes;
            }).finally(() => {
                this.loading = false; 
            });
        },

        despliegaModalProduct: function() {
            this.showProductos = true;
        },

        filtroProduct: async function() {

            await axios.post(URL+'?action=filtroProductos&producto='+this.searchProducto)
            .then((response) => {
                
                this.productos = response.data.productos;                         
            });
        },

        selecProduct: function(product) {
          
            this.product = product[0];
            this.productName = product[1];
            this.productos = [];
            this.searchProducto = "";
            this.showProductos = false;
            var tipoProducto = "";
            tipoProducto = parseInt(this.product.substring(0, 2));

            if (tipoProducto >= 70 && tipoProducto <= 94) {
                this.showServicios = true;
            }
            else {
                this.showBienes = true;
            }
        },

        searchCodProduct: function() {

            axios.post(URL+'?action=buscaProducto&producto='+this.product)
            .then((response) => {
                
                if (response.data.error == true) {

                    this.product = "";
                    this.productName = "";
                    Swal.fire({
                        icon: 'warning',
                        title: 'No se ha encontrado este código UNSPSC',
                        text: 'Revisa el código que sea un producto o exista'
                    })
                }
                else {
                    this.productName = response.data.productName;
                    var tipoProducto = "";
                    tipoProducto = this.product.substring(0, 2);
                    console.log(tipoProducto);
                    if (tipoProducto >= 70 && tipoProducto <= 94) {
                        this.showServicios = true;
                    }
                    else if (tipoProducto < 70 && tipoProducto > 94){
                        this.showBienes = true;
                    }
                    else {
                        this.showBienes = false;
                        this.showServicios = false;
                    }
                }
            });
        },

        despliegaModalBienes: function() {
            this.showCpcBienes = true;
        },

        filtraBienesCpc: async function() {
          
            const data = this.clasificadoresBienesCopy;
            var text = this.searchBienes;
            var resultado = [];

            resultado = await filtroEnArrayDeObjetos({'data': data, 'text': text});

            this.clasificadoresBienes = resultado;
        },

        searchCodBienes: function() {

            axios.post(URL+'?action=buscaCpcBienes&cpc='+this.cpcBien)
            .then((response) => {
                
                if (response.data.error == true) {

                    this.cpcBien = "";
                    this.cpcBienName = "";
                    Swal.fire({
                        icon: 'warning',
                        title: 'No se ha encontrado este cpc',
                        text: 'Revisa el código que exista'
                    })
                }
                else {
                   this.cpcBienName = response.data.cpcBienName;
                }
            });
        },

        selecCpcBien: function(cpc) {
            this.cpcBien = cpc[0];
            this.cpcBienName = cpc[1];
            this.showCpcBienes = false;
            this.searchCodBienes = "";
            this.clasificadoresBienes = this.clasificadoresBienesCopy;
        },

        despliegaModalServicios: function() {
            this.showCpcServicios = true;
        },

        filtraServiciosCpc: async function() {

            const data = this.clasificadoresServiciosCopy;
            var text = this.searchServicios;
            var resultado = [];

            resultado = await filtroEnArrayDeObjetos({'data': data, 'text': text});

            this.clasificadoresServicios = resultado;
        },

        selecCpcServicio: function(cpc) {
            this.cpcServicio = cpc[0];
            this.cpcServicioName = cpc[1];
            this.showCpcServicios = false;
            this.searchServicios = "";
            this.clasificadoresServicios = this.clasificadoresServiciosCopy;
        },

        searchCodServicios: function() {

            axios.post(URL+'?action=buscaCpcServicios&cpc='+this.cpcServicio)
            .then((response) => {
                console.log(response.data);
                if (response.data.error == true) {
                    this.cpcServicio = "";
                    this.cpcServicioName = "";
                    Swal.fire({
                        icon: 'warning',
                        title: 'No se ha encontrado este cpc',
                        text: 'Revisa el código que exista'
                    })
                }
                else {
                   this.cpcServicioName = response.data.cpcServicioName;
                }
            });
        },

        agregarProductos: function() {

            if (this.product != "" && this.productName != "" && ((this.cpcBien != "" && this.cpcBienName != "") || (this.showServicios != "" && this.cpcServicioName != ""))) {

                var temporal = [];

                temporal.push(this.product);
                temporal.push(this.productName);

                if (this.cpcBien != "") {
                    temporal.push(this.cpcBien);
                    temporal.push(this.cpcBienName);
                }
                else if (this.cpcServicio != "") {
                    temporal.push(this.cpcServicio);
                    temporal.push(this.cpcServicioName);
                }

                this.groupProducts.push(temporal);

                this.product = this.productName = this.cpcBien = this.cpcBienName = this.cpcServicio = this.cpcServicioName = "";
                this.showBienes = false;
                this.showServicios = false;
            }
        },

        eliminarProducto: function(products) {

            var i = this.groupProducts.indexOf( products );
            
            if ( i !== -1 ) {
                this.groupProducts.splice( i, 1 );
            }   
        },

        despliegaFuentes: function() {
            this.showFuentes = true;
        },

        selecFuente: function(fuente) {

            this.fuente = fuente[0];
            this.fuenteName = fuente[1];
            this.showFuentes = false;
            this.searchFuente = "";
            this.fuentes = this.fuentesCopy;
        },

        filtraFuentes: async function() {
            const data = this.fuentesCopy;
            var text = this.searchFuente;
            var resultado = [];

            resultado = await filtroEnArrayDeObjetos({'data': data, 'text': text});

            this.fuentes = resultado;
        },

        searchCodFuente: async function() {
            axios.post(URL+'?action=buscaFuente&cod='+this.fuente)
            .then((response) => {
                console.log(response.data);
                if (response.data.error == true) {
                    this.fuente = "";
                    this.fuenteName = "";
                    Swal.fire({
                        icon: 'warning',
                        title: 'No se ha encontrado este cpc',
                        text: 'Revisa el código que exista'
                    })
                }
                else {
                   this.fuenteName = response.data.fuenteName;
                }
            });
        },

        agregarFuente: function() {

            if (this.fuente != "" && this.fuenteName != "") {

                var temporal = [];
                temporal.push(this.fuente);
                temporal.push(this.fuenteName);
                this.groupFuentes.push(temporal);
                this.fuente = this.fuenteName = "";
            }
            else {
                Swal.fire({
                    icon: 'warning',
                    title: 'Faltan datos',
                    text: 'No se puede agregar porque faltan datos'
                })
            }
        },

        eliminarFuente: function(fuentes) {

            var i = this.groupFuentes.indexOf( fuentes );
            
            if ( i !== -1 ) {
                this.groupFuentes.splice( i, 1 );
            }   
        },

        guardar: function() {

            //Validacion cabecera
            const fechaRegistro = document.getElementById("fechaReg").value;
            const fechaEstimada = document.getElementById("fechaIni").value;

            if (this.modalidad != "" && this.descripcion != "" && this.valorEstimado > 0 && this.valorEstimadoVigActual > 0 && this.requiereVigFuturas != "" && this.estadoVigFuturas != "" && this.contactoResposable != "" && this.cargoResponsable != "" && fechaRegistro != "" && fechaEstimada != "") {
                
                if (this.groupProducts.length > 0) {

                    if(this.groupFuentes.length > 0) {

                        Swal.fire({
                            icon: 'question',
                            title: 'Esta seguro que quiere guardar?',
                            showDenyButton: true,
                            confirmButtonText: 'Guardar!',
                            denyButtonText: 'Cancelar',
                        }).then((result) => {
            
                            if (result.isConfirmed) {
            
                                var formData = new FormData();
            
                                formData.append("modalidad", this.modalidad);
                                formData.append("descripcion", this.descripcion);
                                formData.append("valorEstimado", this.valorEstimado);
                                formData.append("valorEstimadoVigActual", this.valorEstimadoVigActual);
                                formData.append("requiereVigFuturas", this.requiereVigFuturas);
                                formData.append("estadoVigFuturas", this.estadoVigFuturas);
                                formData.append("contactoResposable", this.contactoResposable);
                                formData.append("cargoResponsable", this.cargoResponsable);
                                formData.append("fechaRegistro", fechaRegistro);
                                formData.append("fechaEstimada", fechaEstimada);
                                formData.append("dias", this.dias);
                                formData.append("meses", this.meses);
            
                                for(let i=0; i <= this.groupProducts.length-1; i++) {
            
                                    const val = Object.values(this.groupProducts[i]).length-1;
            
                                    for(let x = 0; x <= val; x++) {
                                        formData.append("groupProducts["+i+"][]", Object.values(this.groupProducts[i])[x]);
                                    }
                                }
            
                                for(let i=0; i <= this.groupFuentes.length-1; i++) {
            
                                    const val = Object.values(this.groupFuentes[i]).length-1;
            
                                    for(let x = 0; x <= val; x++) {
                                        formData.append("groupFuentes["+i+"][]", Object.values(this.groupFuentes[i])[x]);
                                    }
                                }
            
                                axios.post(URL + '?action=guardar', formData)
                                .then((response) => {
                                    // console.log(response.data);
                                    if(response.data.insertaBien){
                                        Swal.fire({
                                            icon: 'success',
                                            title: 'PAA guardado con exito con código '+ response.data.cod,
                                            showConfirmButton: false,
                                            timer: 3500
                                        });

                                        location.reload();
                                    }
                                    else{
                                    
                                        Swal.fire(
                                            'Error!',
                                            'No se pudo guardar.',
                                            'error'
                                        );
                                    }
                                    
                                });
                            } 
                        })  
                    }
                    else {
                        Swal.fire({
                            icon: 'warning',
                            title: 'Faltan datos',
                            text: 'No ha agregado ninguna fuente'
                        })    
                    }
                }
                else {
                    Swal.fire({
                        icon: 'warning',
                        title: 'Faltan datos',
                        text: 'No ha agregado ningun producto'
                    })    
                }
            }
            else {
                Swal.fire({
                    icon: 'warning',
                    title: 'Faltan datos',
                    text: 'Faltan datos en la cabecera, por favor revisar'
                })
            }
            
        },
    },
});