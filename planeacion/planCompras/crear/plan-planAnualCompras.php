<?php

	require_once '../../../comun.inc';
    require '../../../funciones.inc';
    header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=utf8");
    session_start();

	$linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");

    $out = array('error' => false);

    $action = "show";

    if(isset($_GET['action'])){
        $action = $_GET['action'];
    }

    if ($action == "datosIniciales") {

        $modalidades = [];
        $productos = [];
        $clasificadoresB = [];
        $clasificadoresS = [];
        $fuentes = [];

        //Modalidad de selección
        $sqlModalidad = "SELECT id, modalidad FROM contramodalidad WHERE estado = 'S' ORDER BY id ASC";
        $resModalidad = mysqli_query($linkbd, $sqlModalidad);
        while ($rowModalidad = mysqli_fetch_row($resModalidad)) {
            
            array_push($modalidades, $rowModalidad);
        }

        $sqlProducto = "SELECT codigo, nombre FROM productospaa WHERE tipo = '5' AND estado = 'S' ORDER BY codigo ASC";
        $resProducto = mysqli_query($linkbd, $sqlProducto);
        while ($rowProducto = mysqli_fetch_row($resProducto)) {
            
            array_push($productos, $rowProducto);
        }

        $sqlBienesTransportables = "SELECT grupo, titulo FROM ccpetbienestransportables WHERE version = (SELECT MAX(version) FROM ccpetbienestransportables) AND LENGTH(grupo) = 7 ORDER BY grupo ASC";
        $resBienesTransportables = mysqli_query($linkbd, $sqlBienesTransportables);
        while ($rowBienesTransportables = mysqli_fetch_row($resBienesTransportables)) {
            
            array_push($clasificadoresB, $rowBienesTransportables);
        }

        $sqlServicios = "SELECT grupo, titulo FROM ccpetservicios WHERE version=(SELECT MAX(version) FROM ccpetservicios ) AND LENGTH(grupo) = 5";
        $resServicios = mysqli_query($linkbd, $sqlServicios);
        while ($rowServicios = mysqli_fetch_row($resServicios)) {
            
            array_push($clasificadoresS, $rowServicios);
        }

        $sqlFuente = "SELECT codigo_fuente, nombre FROM ccpet_fuentes_cuipo WHERE version=(SELECT MAX(version) FROM ccpet_fuentes_cuipo )";
        $resFuente = mysqli_query($linkbd, $sqlFuente);
        while ($rowFuente = mysqli_fetch_row($resFuente)) {
            
            array_push($fuentes, $rowFuente);
        }

        $out['modalidades'] = $modalidades;
        $out['productos'] = $productos;
        $out['clasificadoresB'] = $clasificadoresB;
        $out['clasificadoresS'] = $clasificadoresS;
        $out['fuentes'] = $fuentes;
    }

    if ($action == "filtroProductos") {

        $productos = [];
        $producto = "";
        $producto = $_GET['producto'];

        $sqlProducto = "SELECT codigo, nombre FROM productospaa WHERE tipo = '5' AND estado = 'S' AND (codigo LIKE '%$producto%' OR nombre LIKE '%$producto%') ORDER BY codigo ASC";
        $resProducto = mysqli_query($linkbd, $sqlProducto);
        while ($rowProducto = mysqli_fetch_row($resProducto)) {
            
            array_push($productos, $rowProducto);
        }

        $out['productos'] = $productos;
    }

    if ($action == "buscaProducto") {

        $producto = "";
        $producto = $_GET['producto'];

        $sqlProducto = "SELECT codigo, nombre FROM productospaa WHERE tipo = '5' AND estado = 'S' AND codigo = '$producto'";
        $resProducto = mysqli_query($linkbd, $sqlProducto);
        $cantResult = mysqli_num_rows($resProducto);

        if ($cantResult > 0) {
            $rowProducto = mysqli_fetch_row($resProducto);

            $out['productName'] = $rowProducto[1];
        }
        else {
            $out["error"] = true;
        }
    }

    if ($action == "buscaCpcBienes") {

        $cpc = "";
        $cpc = $_GET['cpc'];

        $sqlBienesTransportables = "SELECT grupo, titulo FROM ccpetbienestransportables WHERE version = (SELECT MAX(version) FROM ccpetbienestransportables) AND grupo = '$cpc' AND LENGTH(grupo) = 7";
        $resBienesTransportables = mysqli_query($linkbd, $sqlBienesTransportables);
        $cantResult = mysqli_num_rows($resBienesTransportables);

        if ($cantResult > 0) {
            $rowBienesTransportables = mysqli_fetch_row($resBienesTransportables);
            $out['cpcBienName'] = $rowBienesTransportables[1];
        }
        else {
            $out['error'] = true;
        }
    }

    if ($action == "buscaCpcServicios") {
        $cpc = "";
        $cpc = $_GET['cpc'];

        $sqlServicios = "SELECT grupo, titulo FROM ccpetservicios WHERE version=(SELECT MAX(version) FROM ccpetservicios ) AND grupo = '$cpc'";
        $resServicios = mysqli_query($linkbd, $sqlServicios);
        $cantResult = mysqli_num_rows($resServicios);

        if ($cantResult > 0) {
            $rowServicios = mysqli_fetch_row($resServicios);
            $out['cpcServicioName'] = $rowServicios[1];
        }
        else {
            $out['error'] = true;
        }
    }
    
    if ($action == "buscaFuente") {

        $codFuente = "";
        $codFuente = $_GET['cod'];

        $sqlFuente = "SELECT codigo_fuente, nombre FROM ccpet_fuentes_cuipo WHERE version=(SELECT MAX(version) FROM ccpet_fuentes_cuipo ) AND codigo_fuente";
        $resFuente = mysqli_query($linkbd, $sqlFuente);
        $cantResult = mysqli_num_rows($resFuente);

        if ($cantResult > 0) {

            $rowFuente = mysqli_fetch_row($resFuente);
            $out['fuenteName'] = $rowFuente[1];
        }
        else {
            $out['error'] = true;
        }
    }

    if ($action == "guardar") {
        
        $modalidad = $descripcion = $requiereVigFuturas = $estadoVigFuturas = $contactoResposable = $cargoResponsable = $fechaRegistro = $fechaEstimada = $productos = $fuentes = $clasificadores = $vigencia = $cedulaUsu = $duracion = $contacto ="";
        $valorEstimado = $valorEstimadoVigActual = 0;
        $groupProducts = [];
        $groupFuentes = [];

        $modalidad = $_POST['modalidad'];
        $descripcion = $_POST['descripcion'];
        $valorEstimado = $_POST['valorEstimado'];
        $valorEstimadoVigActual = $_POST['valorEstimadoVigActual'];
        $requiereVigFuturas = $_POST['requiereVigFuturas'];
        $estadoVigFuturas = $_POST['estadoVigFuturas'];
        $contactoResposable = $_POST['contactoResposable'];
        $cargoResponsable = $_POST['cargoResponsable'];
        preg_match("/([0-9]{2})\/([0-9]{2})\/([0-9]{4})/", $_POST['fechaRegistro'],$fecha);
        $fechaRegistro="$fecha[3]-$fecha[2]-$fecha[1]";
        preg_match("/([0-9]{2})\/([0-9]{2})\/([0-9]{4})/", $_POST['fechaRegistro'],$fecha);
        $fechaEstimada="$fecha[3]-$fecha[2]-$fecha[1]";
        $groupProducts = $_POST['groupProducts'];
        $groupFuentes = $_POST['groupFuentes'];
        $vigencia = vigencia_usuarios($_SESSION['cedulausu']);
        $cedulaUsu = $_SESSION['cedulausu'];
        $duracion = $_POST['dias'] ."/". $_POST['meses'];
        $contacto = $contactoResposable . "-" . $cargoResponsable; 

        foreach ($groupProducts as $i => $product) {
            
            $codigosunspsc[] = $product[0];
            $codclasificadores[] = $product[2];
        }

        foreach ($groupFuentes as $key => $fuent) {
            
            $codFuentes[] = $fuent[0];
        }

        $productos = implode("-", $codigosunspsc);
        $clasificadores = implode("-", $codclasificadores);
        $fuentes = implode("-", $codFuentes);

        $sqlConsec = "SELECT MAX(CONVERT(codplan, SIGNED INTEGER)) FROM contraplancompras WHERE vigencia = '$vigencia'";
        $rowConsec = mysqli_fetch_row(mysqli_query($linkbd,$sqlConsec));
        $planCod = $rowConsec[0] + 1;	

        $sqlInsert ="INSERT INTO contraplancompras (codplan, vigencia, fecharegistro, codelaboradopor, codigosunspsc, descripcion, fechaestinicio, duracionest, modalidad, fuente, valortotalest, valorestvigactual, requierevigfut, estadovigfut, estado, contacto_respon, clasificador) VALUES ('$planCod', '$vigencia', '$fechaRegistro', '$cedulaUsu', '$productos', '$descripcion', '$fechaEstimada', '$duracion', '$modalidad', '$fuentes', $valorEstimado, $valorEstimadoVigActual, '$requiereVigFuturas', '$estadoVigFuturas', 'S', '$contacto', '$clasificadores')";
        mysqli_query($linkbd, $sqlInsert);

        $out['insertaBien'] = true;
        $out['cod'] = $planCod;
    }

    header("Content-type: application/json");
    echo json_encode($out);
    die();