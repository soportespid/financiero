const URL = 'planeacion/planCompras/buscar/plan-buscarPlanAnualCompras.php';

var app = new Vue({
    el:'#myapp',
    data:{
        arrData:[],
        arrDataCopy:[],
        results:0,
        searchValue: '',
        tableData: '',
        isLoading: false,
        txtSearch: '',
        responsables: '',
        responsable: '',
    }, 
    async mounted() {
        this.get();
    },
    methods:{
        /* metodos para traer información */
        async get() {
            const formData = new FormData();
            formData.append("action","get");
            this.isLoading = true;
            const response = await fetch(URL,{method:"POST",body:formData});
            const objData = await response.json();
            this.arrData = objData.data;
            this.arrDataCopy = objData.data;
            this.responsables = objData.responsables;
            this.isLoading = false;
        },
        
        async searchData() {
            let search = "";
            search = this.txtSearch.toLowerCase();
            this.arrData = [...this.arrDataCopy.filter(e=>e.codigo.toLowerCase().includes(search) || e.descripcion.toLowerCase().includes(search))];
        },

        filtroResponsable() {
            this.isLoading = true;
            if (this.responsable == "") {
                this.arrData = this.arrDataCopy;
                this.isLoading = false;
                return false;
            }
            let search = "";
            search = this.responsable.toLowerCase();
            this.arrData = [...this.arrDataCopy.filter(e=>e.contacto_respon.toLowerCase().includes(search))];
            this.isLoading = false;
        },

        async elimina(id, index) {
            const formData = new FormData();
            formData.append("action","elimina");
            formData.append("id", parseInt(id));

            Swal.fire({
                title:"¿Estás segur@ de eliminar?",
                text:"",
                icon: 'warning',
                showCancelButton:true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText:"Sí, eliminar",
                cancelButtonText:"No, cancelar"
            }).then(async function(result) {
                if(result.isConfirmed) {
                    const response = await fetch(URL,{method:"POST",body:formData});
                    const objData = await response.json();
                    if (objData.status) {
                        Swal.fire("Eliminado",objData.msg,"success");
                            app.arrDataCopy.splice(index, 1); 
                            app.arrData = app.arrDataCopy;
                    }
                    else {
                        Swal.fire("Error!",objData.msg,"warning");
                    }
                }
            });
        },

        printPDF: function(){
            const form = document.createElement("form");
            form.method = "post";
            form.target = "_blank";
            form.action = "plan-plancompraspdf.php";       
            
            function addField(name,value){
                const input = document.createElement("input");
                input.type="hidden";
                input.name=name;
                input.value = value;
                form.appendChild(input);
            }

            addField("action","pdf");
            addField("consecutivo",this.consecutivo);
            addField("data", JSON.stringify(this.arrData));
            document.body.appendChild(form);
            form.submit();
            document.body.removeChild(form);
        },
        printExcel: function(){
            window.open('plan-plancomprasexcel.php','_blank')
        },
        formatoNumero: function(valor){
            return new Intl.NumberFormat("en-US", {style: "currency", currency: "USD"}).format(valor);
        },
        formatFecha: function(fecha) {
            // Separar la fecha en componentes
            const parts = fecha.split('-');
            if (parts.length !== 3) {
                throw new Error('Formato de fecha inválido. Use Y-m-d.');
            }
            const year = parts[0];
            const month = parts[1];
            const day = parts[2];
        
            // Retornar la fecha en el formato deseado
            return `${day}/${month}/${year}`;
        },
    }
})