<?php
    require_once '../../../comun.inc';
    require '../../../funciones.inc';
    require '../../../funcionesSP.inc.php';
    session_start();
    header('Content-Type: application/json');

    class buscaPaa {
        private $linkbd;
        public function __construct() {
            $this->linkbd = conectar_v7();
            $this->linkbd->set_charset("utf8");
        }

        public function get() {
            if (!empty($_SESSION)) {
                $vigActual = vigencia_usuarios($_SESSION['cedulausu']);
                $sql = "SELECT PAA.codplan AS codigo,
                PAA.descripcion,
                PAA.fechaestinicio AS fecha,
                PAA.duracionest AS duracion_estimada,
                M.nombre AS nombre_modalidad,
                F.nombre AS nombre_fuente,
                PAA.valortotalest AS valor_estimado,
                PAA.valorestvigactual AS valor_vigencia,
                PAA.id,
                PAA.contacto_respon
                FROM contraplancompras AS PAA
                INNER JOIN plan_modalidad_seleccion AS M
                ON PAA.modalidad = M.codigo
                INNER JOIN plan_fuentes_paa AS F
                ON PAA.fuente = F.codigo
                WHERE PAA.vigencia = '$vigActual' AND PAA.estado = 'S'
                ORDER BY PAA.id ASC";
                $data = mysqli_fetch_all(mysqli_query($this->linkbd, $sql), MYSQLI_ASSOC);

                $arrResponse = array(
                    "data" => $data,
                    "responsables" => $this->filtraCargos($vigActual)
                );

                echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
            }
            die();
        }

        private function filtraCargos($vigencia) {
            $sql = "SELECT DISTINCT contacto_respon FROM contraplancompras WHERE vigencia = '$vigencia' AND estado = 'S'";
            $data = mysqli_fetch_all(mysqli_query($this->linkbd, $sql), MYSQLI_ASSOC);
            return $data;
        }

        public function elimina() {
            $id = $_POST["id"];
            $sql = "UPDATE contraplancompras SET estado = 'N' WHERE id = $id";
            mysqli_query($this->linkbd, $sql);
            $arrResponse = array("status" => true, "msg" => "Plan anual de adquisiciones eliminado con exito");
            echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
        }
    }
    
    if($_POST){
        $obj = new buscaPaa();
        $accion = $_POST["action"];

        if ($accion == "get") {
            $obj->get();
        } else if ($accion == "elimina") {
            $obj->elimina();
        }
    }
?>