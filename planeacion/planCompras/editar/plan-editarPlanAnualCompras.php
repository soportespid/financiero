<?php
    require_once '../../../comun.inc';
    require '../../../funciones.inc';

    session_start();
    if($_POST){
        $objCompra = new Compras();
        if($_POST['action']=='get'){
            $objCompra->getData(intval($_POST['id']));
        }else if($_POST['action'] == 'search'){
            $objCompra->search($_POST['option'],$_POST['search'],$_POST['type']);
        }else if($_POST['action'] =='type'){
            $objCompra->showByType($_POST['type'],null);
        }else if($_POST['action'] == 'save'){
            $objCompra->setData(intval($_POST['id']),$_POST['data']);
        }
    }

    class Compras{
        private $linkbd;
        private $strVigencia;
        private $strDescripcion;
        private $strContacto;
        private $strDuracion;
        private $chrVigencia;
        private $isVigenciaFutura;
        private $dateFechaInicio;
        private $dateFechaRegistro;
        private $strProductos;
        private $strFuentes;
        private $strClasificadores;
        private $intId;
        private $intType;
        private $intValorEstimado;
        private $intValorEstimadoActual;
        private $intModalidad;
        
        public function __construct() {
            $this->linkbd = conectar_v7();
            $this->linkbd->set_charset('UTF-8');
            $this->strVigencia = vigencia_usuarios($_SESSION['cedulausu']);
        }
        public function getData(int $id){
            $this->intId = $id;
            $sql = "SELECT *,DATE_FORMAT(fechaestinicio,'%d/%m/%Y') as fechaestinicio,DATE_FORMAT(fecharegistro,'%d/%m/%Y') as fecharegistro FROM contraplancompras WHERE vigencia = '$this->strVigencia' AND id = $this->intId";
            $request = mysqli_query($this->linkbd,$sql)->fetch_assoc();
            if(!empty($request)){
                $duracion = explode('/',$request['duracionest']);
                $arrResponsable = explode('-',$request['contacto_respon']);
                $arrProductos = explode('-',$request['codigosunspsc']);
                $arrClasificadores = explode('-',$request['clasificador']);
                $arrFuentes = strpos($request['fuente'],'-') ? explode('-',$request['fuente']) : [$request['fuente']];
                $request['modalidades'] =mysqli_fetch_all(mysqli_query($this->linkbd,'SELECT * FROM contramodalidad'),MYSQLI_ASSOC);
                $request['contacto_respon'] = array('nombre'=>$arrResponsable[0],"cargo"=>$arrResponsable[1]);
                $request['duracionest'] = array('dias'=>$duracion[0],'meses'=>$duracion[1]);
                $request['fuente_modal'] = mysqli_fetch_all(mysqli_query($this->linkbd,"SELECT codigo_fuente as codigo, nombre 
                FROM ccpet_fuentes_cuipo WHERE version=(SELECT MAX(version) 
                FROM ccpet_fuentes_cuipo )"),MYSQLI_ASSOC);
                foreach ($arrProductos as $i => $codigo) {
                    $producto = mysqli_query($this->linkbd, "SELECT codigo, nombre FROM productospaa WHERE codigo = $codigo")->fetch_assoc();
                    $clasificacion = mysqli_query($this->linkbd, 
                    "SELECT grupo, titulo
                    FROM ccpetbienestransportables
                    WHERE grupo = {$arrClasificadores[$i]}
                    UNION
                    SELECT grupo, titulo
                    FROM ccpetservicios
                    WHERE grupo = {$arrClasificadores[$i]}")->fetch_assoc();
                    
                    $request['productos'][$i] = array(
                        "codigo" => $producto['codigo'],
                        "nombre" => $producto['nombre'],
                        "cpc" => $clasificacion['grupo'],
                        "cpc_titulo" => $clasificacion['titulo']
                    );
                }
                foreach($arrFuentes as $i => $fuente){
                    $data = mysqli_query($this->linkbd, "SELECT codigo_fuente, nombre FROM ccpet_fuentes_cuipo WHERE codigo_fuente = '$fuente'")->fetch_assoc();
                    if(!empty($data)){
                        $request['fuentes'][$i] = array("codigo"=>$data['codigo_fuente'],"nombre"=>$data['nombre']);
                    }
                }
                $arrResponse = array("status"=>true,"msg"=>"Datos adquiridos","data"=>$request);
            }else{
                $arrResponse = array("status"=>false,"msg"=>"Error de datos");
            }
            echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
            die();
        }
        public function search(string $option,string $search, $type ){
            //var_dump("option:".$option." search:".$search." type:".$type);
            if($search!=""){
                if($option == "producto"){
                    $request = mysqli_fetch_all(mysqli_query($this->linkbd,
                        "SELECT codigo, nombre 
                        FROM productospaa 
                        WHERE tipo = '5' AND estado = 'S' AND (nombre like '%$search%' OR codigo like '$search%') ORDER BY codigo ASC"
                    ),MYSQLI_ASSOC);
                }else if($option == "fuente"){
                    $request = mysqli_fetch_all(mysqli_query($this->linkbd,
                        "SELECT codigo_fuente as codigo, nombre 
                        FROM ccpet_fuentes_cuipo WHERE version=(SELECT MAX(version) 
                        FROM ccpet_fuentes_cuipo ) AND (codigo_fuente like '$search%' OR nombre like '%$search%')"
                    ),MYSQLI_ASSOC);
                }else if($option == "clasificacion" && $type != null){
                    $request = $this->showByType($type,$search);
                }else if($option=="cod_producto"){
                    $request['producto'] = mysqli_query($this->linkbd,"SELECT codigo,nombre FROM productospaa WHERE codigo = '$search'")->fetch_assoc();
                    $request['clasificacion'] = $this->showByType($type,null,false);
                }else if($option=="cod_bien"){
                    $request = mysqli_query($this->linkbd,
                    "SELECT grupo, titulo 
                    FROM ccpetbienestransportables 
                    WHERE version = (SELECT MAX(version) FROM ccpetbienestransportables) AND LENGTH(grupo) = 7 AND grupo='$search'
                    UNION
                    SELECT grupo, titulo 
                    FROM ccpetservicios 
                    WHERE version = (SELECT MAX(version) FROM ccpetbienestransportables) AND LENGTH(grupo) = 5 AND grupo='$search'"
                    )->fetch_assoc();
                }else if($option=="cod_fuente"){
                    $request = mysqli_query($this->linkbd,
                    "SELECT codigo_fuente as codigo, nombre 
                    FROM ccpet_fuentes_cuipo WHERE version=(SELECT MAX(version) 
                    FROM ccpet_fuentes_cuipo ) AND codigo_fuente ='$search'")->fetch_assoc();
                }
                if(!empty($request)){
                    $arrResponse = array("status"=>true,"msg"=>"Datos adquiridos","data"=>$request);
                }else{
                    $arrResponse = array("status"=>false,"msg"=>"Datos no encontrados");
                }
            }else{
                $arrResponse = array("status"=>false,"msg"=>"Error de datos");
            }
            echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
            die();
        }
        public function showByType($type,$search,$bypass=true){
            $this->intType = intval($type);
            $sql="";
            $request ="";
            if($search != null){
                if($this->intType >= 70 && $this->intType <=94){
                    $sql = "SELECT grupo, titulo 
                    FROM ccpetservicios 
                    WHERE version = (SELECT MAX(version) FROM ccpetbienestransportables) AND LENGTH(grupo) = 5 AND (grupo LIKE '$search%' OR titulo LIKE '%$search%')
                    ORDER BY grupo ASC";
                }else{
                    $sql = "SELECT grupo, titulo 
                    FROM ccpetbienestransportables 
                    WHERE version = (SELECT MAX(version) FROM ccpetbienestransportables)  AND LENGTH(grupo) = 7 AND (grupo LIKE '$search%' OR titulo LIKE '%$search%') 
                    ORDER BY grupo ASC";
                    
                }
                $request = mysqli_fetch_all(mysqli_query($this->linkbd,$sql),MYSQLI_ASSOC);
                return $request;
            }else{
                if($this->intType >= 70 && $this->intType <=94){
                    $sql = "SELECT grupo, titulo FROM ccpetservicios WHERE version = (SELECT MAX(version) FROM ccpetbienestransportables) AND LENGTH(grupo) = 5 ORDER BY grupo ASC";
                }else{
                    $sql="SELECT grupo,titulo FROM ccpetbienestransportables WHERE version = (SELECT MAX(version) FROM ccpetbienestransportables)  AND LENGTH(grupo) = 7 ORDER BY grupo ASC";
                }
                $request = mysqli_fetch_all(mysqli_query($this->linkbd,$sql),MYSQLI_ASSOC);
                if($bypass){
                    echo json_encode($request,JSON_UNESCAPED_UNICODE);
                }else{
                    return $request;
                }
            }
            die();
        }
        public function setData(int $id, string $json){
            //print_r($arrData = json_decode($json,true));
            $this->intId = $id;
            $request = mysqli_query($this->linkbd,"SELECT * FROM contraplancompras WHERE vigencia = '$this->strVigencia' AND id = $this->intId")->fetch_assoc();
            if(!empty($request)){
                $arrData = json_decode($json,true);
                if(empty($arrData['arr_productos']) 
                || empty($arrData['date_fecha_inicio']) || empty($arrData['date_fecha_registro'])
                || empty($arrData['int_valor_estimado']) || empty($arrData['int_valor_estimado_actual'])
                || empty($arrData['str_contacto_responsable'] || empty($arrData['str_descripcion']))
                || empty($arrData['str_duracion'])){
                    $arrResponse = array("status"=>false,"msg"=>"Error de datos, por favor, revisa los datos faltantes");
                }else{
                    $arrProductos = $arrData['arr_productos'];
                    $arrFuentes = $arrData['arr_fuentes'];
                    $arrFechaInicio = explode("/",$arrData['date_fecha_inicio']);
                    $arrFechaRegistro = explode("/",$arrData['date_fecha_registro']);
                    
                    foreach($arrProductos as $producto){
                        $arrProductos['codigos'][]=$producto['codigo'];
                        $arrProductos['clasificadores'][] = $producto['cpc'];
                    }
                    if(!empty($arrFuentes)){
                        foreach($arrFuentes as $fuente){
                            $arrFuentes['fuentes'][]=$fuente['codigo'];
                        }
                        $this->strFuentes = implode("-",$arrFuentes['fuentes']);
                    }else{
                        $this->strFuentes="";
                    }

                    $this->intValorEstimado = $arrData['int_valor_estimado'];
                    $this->intValorEstimadoActual = $arrData['int_valor_estimado_actual'];
                    $this->intModalidad = $arrData['int_modalidad'];
                    $this->strDescripcion = $arrData['str_descripcion'];
                    $this->strContacto = $arrData['str_contacto_responsable'];
                    $this->strDuracion = $arrData['str_duracion'];
                    $this->chrVigencia = $arrData['sel_requiere_vigencia'];
                    $this->isVigenciaFutura = $arrData['sel_vigencia_futura'];
                    $this->dateFechaInicio = date_format(date_create($arrFechaInicio[2]."-".$arrFechaInicio[1]."-".$arrFechaInicio[0]),"Y-m-d");
                    $this->dateFechaRegistro = date_format(date_create($arrFechaRegistro[2]."-".$arrFechaRegistro[1]."-".$arrFechaRegistro[0]),"Y-m-d");
                    $this->strProductos = implode("-",$arrProductos['codigos']);
                    $this->strClasificadores = implode("-",$arrProductos['clasificadores']);
                    
                    $sql = "UPDATE contraplancompras SET 
                        fecharegistro='$this->dateFechaRegistro',
                        fechaestinicio='$this->dateFechaInicio',
                        codigosunspsc = '$this->strProductos',
                        descripcion='$this->strDescripcion',
                        duracionest='$this->strDuracion',
                        modalidad=$this->intModalidad,
                        fuente='$this->strFuentes',
                        valortotalest=$this->intValorEstimado,
                        valorestvigactual=$this->intValorEstimadoActual,
                        requierevigfut='$this->chrVigencia',
                        estadovigfut=$this->isVigenciaFutura,
                        contacto_respon='$this->strContacto',
                        clasificador='$this->strClasificadores'
                        WHERE id = $this->intId
                    ";
                    $request = mysqli_query($this->linkbd,$sql);
                    if($request > 0){
                        $arrResponse = array("status"=>true,"msg"=>"Datos actualizados");
                    }else{
                        $arrResponse = array("status"=>false,"msg"=>"Error, los datos no se guardaron. Intentelo de nuevo.");
                    }
                }
            }else{
                $arrResponse = array("status"=>false,"msg"=>"Error de datos");
            }
            echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
            die();
        }
    }
?>