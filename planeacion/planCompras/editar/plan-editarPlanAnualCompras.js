const URL = 'planeacion/planCompras/editar/plan-editarPlanAnualCompras.php';
import { filtroEnArrayDeObjetos } from './../../../funciones.js';

var app = new Vue({
    el:"#myapp",
    data: function(){
        return{
            intId:0,
            intDias:0,
            intMeses:0,
            intValorEstimado:0,
            intValorEstimadoActual:0,
            intModalidad:0,
            intCodeType:null,
            strDescripcion:'',
            strContacto:'',
            strCargo:'',
            strBuscarProductoModal:'',
            strBuscarProductoCodigo:'',
            strBuscarBienModal:'',
            strBuscarBienCodigo:'',
            strBuscarFuenteModal:'',
            strBuscarFuenteCodigo:'',
            strProductoCodigo:'',
            strProductoNombre:'',
            dateFechaInicio:'',
            dateFechaRegistro:'',
            selectReqVigenciaFutura:'',
            selectEstadoVigenciaFutura:'',
            isModalProductos: false,
            isModalBienes:false,
            isModalFuentes:false,
            isFieldBienes: false,
            isFieldServicios:false,
            isLoading:false,
            arrModalidades:[],
            arrProductos:[],
            arrFuentes:[],
            arrModalProductos:[],
            arrModalBienes:[],
            arrModalFuentes:[],
            objProducto:{"nombre":"","codigo":""},
            objBien:{"grupo":"","titulo":""},
            objFuente:{"codigo":"","nombre":""}
        }
    },
    async mounted(){
        await this.getData(); 
    },
    methods:{
        getData: async function(){
            this.id = new URLSearchParams(window.location.search).get('id');
            if(this.id > 0){
                this.isLoading = true;
                const formData = new FormData();
                formData.append("id",this.id);
                formData.append("action","get");
                const response = await fetch(URL,{method:'POST',body:formData});
                const objData = await response.json();
                if(objData.status){
                    const data = objData.data;
                    this.intDias = data.duracionest.dias;
                    this.intMeses = data.duracionest.meses;
                    this.intValorEstimado = data.valortotalest;
                    this.intValorEstimadoActual = data.valorestvigactual;
                    this.intModalidad = data.modalidad;
                    this.strDescripcion = data.descripcion;
                    this.strContacto = data.contacto_respon.nombre;
                    this.strCargo = data.contacto_respon.cargo;
                    this.selectReqVigenciaFutura = data.requierevigfut;
                    this.selectEstadoVigenciaFutura = data.estadovigfut;
                    this.dateFechaInicio = data.fechaestinicio;
                    this.dateFechaRegistro = data.fecharegistro;
                    this.arrModalidades = data.modalidades;
                    this.arrProductos = data.productos;
                    this.arrFuentes = data.fuentes;
                    this.arrModalFuentes = data.fuente_modal;
                }
                this.isLoading = false;
            }else{
                window.location.href="plan-plancomprasbuscar"
            }
        },
        setData: async function(){
            this.id = new URLSearchParams(window.location.search).get('id');
            this.dateFechaRegistro = document.querySelector("#fechaReg").value;
            this.dateFechaInicio = document.querySelector("#fechaIni").value;
            
            if(this.id > 0){  
                
                Swal.fire({
                    icon: 'question',
                    title: '¿Está segur@ de actualizar?',
                    showDenyButton: true,
                    confirmButtonText: 'Si, guardar.',
                    denyButtonText: 'No, cancelar',
                }).then(async (result)=>{
                    if(result.isConfirmed){
                        this.isLoading = true;
                        let formData = new FormData();
                        let data = {
                            "int_valor_estimado":this.intValorEstimado,
                            "int_valor_estimado_actual":this.intValorEstimadoActual,
                            "int_modalidad":this.intModalidad,
                            "str_duracion":this.intDias+"/"+this.intMeses,
                            "str_descripcion":this.strDescripcion,
                            "str_contacto_responsable":this.strContacto+"-"+this.strCargo,
                            "sel_requiere_vigencia":this.selectReqVigenciaFutura,
                            "sel_vigencia_futura":this.selectEstadoVigenciaFutura,
                            "date_fecha_inicio":this.dateFechaInicio,
                            "date_fecha_registro":this.dateFechaRegistro,
                            "arr_productos":this.arrProductos,
                            "arr_fuentes":this.arrFuentes
                        };
                        formData.append("id",this.id);
                        formData.append("action","save");
                        formData.append("data",JSON.stringify(data));
        
                        const response = await fetch(URL,{method:'POST',body:formData});
                        const objData = await response.json();
        
                        if(objData.status){
                            Swal.fire("Actualizado",objData.msg,"success");
                        }else{
                            Swal.fire("Error",objData.msg,"error");
                        }
                        this.isLoading = false;
                    }
                })
            }else{
                Swal.fire("Error","Error de datos","error");
            }
        },
        search: async function(option){
            let search = "";
            
            if(option == "cod_producto"){
                search = this.strBuscarProductoCodigo;
                this.intCodeType = parseInt(search.substring(0, 2));
            }
            if(option=="cod_bien") search = this.strBuscarBienCodigo;
            if(option=="cod_fuente")search=this.strBuscarFuenteCodigo;
            if(option=="producto") search = this.strBuscarProductoModal;
            if(option=="clasificacion") search = this.strBuscarBienModal;
            if(option=="fuente")search=this.strBuscarFuenteModal;

            this.isLoading = true;
            let formData = new FormData()
            formData.append("type",this.intCodeType);
            formData.append("search",search);
            formData.append("action","search");
            formData.append("option",option);
            
            const response = await fetch(URL,{method:'POST',body:formData});
            const objData = await response.json();

            if(objData.status){
                if(option=="cod_producto"){
                    this.objProducto = objData.data.producto;
                    this.arrModalBienes = objData.data.clasificacion;
                    this.objBien.titulo="";
                    this.strBuscarBienCodigo = "";
                }
                if(option=="cod_bien") this.objBien = objData.data;
                if(option=="cod_fuente") this.objFuente = objData.data;
                if(option=="producto") this.arrModalProductos = objData.data;
                if(option=="clasificacion") this.arrModalBienes = objData.data;
                if(option=="fuente")this.arrModalFuentes = objData.data;

                if(this.objProducto.nombre!=""){
                    this.intCodeType = parseInt(this.objProducto.codigo.substring(0, 2));
                    this.isFieldBienes = true;
                }
            }else{
                this.arrModalProductos = [];
                this.arrModalBienes = [];
                this.arrModalFuentes = [];
            }
            this.isLoading = false;
        },
        selectItem: async function(item,type){
            if(type=="producto"){
                let formData = new FormData();
                this.intCodeType = parseInt(item.codigo.substring(0, 2));
                
                formData.append("type",this.intCodeType);
                formData.append("action","type");
    
                const response = await fetch(URL,{method:'POST',body:formData });
                this.arrModalBienes = await response.json();
                if(this.arrModalBienes < 0){
                    this.arrModalBienes = [];
                }
                this.objProducto = item;
                this.strBuscarProductoCodigo = this.objProducto.codigo;
                this.isFieldBienes = true;
                this.arrModalProductos =[];
                this.strBuscarProductoModal ="";
                this.objBien.titulo = "";
                this.strBuscarBienCodigo = "";
            }else if(type=="fuente"){
                this.objFuente = item;
                this.strBuscarFuenteCodigo = this.objFuente.codigo;
                this.strBuscarFuenteModal ="";
            }else{
                this.objBien = item;
                this.strBuscarBienCodigo = this.objBien.grupo;
                this.arrModalBienes =[];
                this.strBuscarBienModal ="";
            }
        },
        addProducto: function(){
            if(this.objProducto.nombre == ""){
                Swal.fire("Error","Debe seleccionar un producto, intente de nuevo.","error");
                return false;
            }else if(this.objBien.titulo ==""){
                Swal.fire("Error","Debe seleccionar la clasificación, intente de nuevo.","error");
                return false;
            }
            let obj = {
                "codigo":this.objProducto.codigo,
                "nombre":this.objProducto.nombre,
                "cpc":this.objBien.grupo,
                "cpc_titulo":this.objBien.titulo
            }
            this.arrProductos.unshift(obj);
            this.objProducto.nombre = "";
            this.objBien.titulo ="";
            this.isFieldBienes = false;
            this.strBuscarProductoCodigo=""
            this.strBuscarBienCodigo ="";
        },
        addFuente: function(){
            if(this.strBuscarFuenteCodigo == ""){
                Swal.fire("Error","Debe seleccionar una fuente, intente de nuevo.","error");
                return false;
            }
            if(this.arrFuentes == undefined){
                this.arrFuentes = [];
            }
            this.arrFuentes.unshift({
                "codigo":this.objFuente.codigo,
                "nombre":this.objFuente.nombre
            });

            this.objFuente.nombre ="";
            this.strBuscarFuenteCodigo="";
        },
        delProducto: function(item){
            let index = this.arrProductos.map(el=>el.codigo).indexOf(item.codigo);
            this.arrProductos.splice(index,1);
        },
        delFuente: function(item){
            let index = this.arrFuentes.map(el=>el.codigo).indexOf(item.codigo);
            this.arrFuentes.splice(index,1);
        },
        formatNumero: function(valor){
            return new Intl.NumberFormat("en-US", {style: "currency", currency: "USD"}).format(valor);
        }
    }
})