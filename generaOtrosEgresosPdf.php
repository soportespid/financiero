<?php
    require_once("tcpdf/tcpdf.php");
	require('comun.inc');
	require "funciones.inc";
    require 'funcionesSP.inc.php';
	date_default_timezone_set("America/Bogota");
	session_start();
	class MYPDF extends TCPDF {
        public $estado = "";
        public $consecutivo = "";

        function __construct(){
            parent::__construct();
            $data = json_decode($_POST["data"], true);
            $this->estado = $data[0]["estado"];
            $this->consecutivo = $data[0]["consecutivo"];
        }

		public function Header() {
			$linkbd = conectar_v7();
			$linkbd -> set_charset("utf8");
			$sqlr="SELECT * FROM configbasica WHERE estado='S'";
			$res=mysqli_query($linkbd, $sqlr);
			while($row=mysqli_fetch_row($res)) {
				$nit = $row[0];
				$rs  = $row[1];
			}
			//Parte Izquierda
			$this->Image('imagenes/escudo.jpg',  13, 13, 20, 20, 'JPG', '', 'T', true, 300, '', false, false, 0, false, false, false);// Logo
			$this->SetFont('helvetica','B',8);
			$this->SetY(10);
			$this->RoundedRect(10, 10, 190, 25, 1,'');
			$this->Cell(0.1);
			$this->Cell(26,25,'','R',0,'L');
			$this->SetY(8);
			$this->SetX(40);
			$this->SetFont('helvetica','B',9);
			$this->Cell(140,15,strtoupper("$rs"),0,0,'C');
			$this->SetFont('helvetica','B',7);
			$this->SetY(12);
			$this->SetX(40);
			$this->Cell(140,15,'NIT: '.$nit,0,0,'C');
			//*****************************************************************************************************************************
			$this->SetFont('helvetica','B',9);
			$this->SetY(23);
			$this->SetX(36);
            $this->Cell(164,12,"OTROS EGRESOS",'T',0,'C');

			//**********************************************************
			$this->SetFont('times','B',10);
			$this->ln(12);
			//**********************************************************
            if($this->estado != "S") {
                $img_file = "assets/img/reversado.png";
                $this->SetAlpha(0.35);
                $this->Image($img_file, 0, 20, 250, 280, '', '', '', false, 300, '', false, false, 300);
                $this->SetAlpha(1);
            }
		}
		public function Footer()
		{
			$linkbd = conectar_v7();
			$linkbd -> set_charset("utf8");
			$sqlr="SELECT direccion,telefono,web,email FROM configbasica WHERE estado='S'";
			$resp=mysqli_query($linkbd, $sqlr);
			$user = $_SESSION['nickusu'];
            $cedula = $_SESSION['cedulausu'];
			$fecha = date("Y-m-d H:i:s");
			$ip = $_SERVER['REMOTE_ADDR'];
			while($row=mysqli_fetch_row($resp))
			{
				$direcc=strtoupper($row[0]);
				$telefonos=$row[1];
				$dirweb=strtoupper($row[3]);
				$coemail=strtoupper($row[2]);
			}
			if($direcc!=''){$vardirec="Dirección: $direcc, ";}
			else {$vardirec="";}
			if($telefonos!=''){$vartelef="Telefonos: $telefonos";}
			else{$vartelef="";}
			if($dirweb!=''){$varemail="Email: $dirweb, ";}
			else {$varemail="";}
			if($coemail!=''){$varpagiw="Pagina Web: $coemail";}
			else{$varpagiw="";}

			$sql_teso_funciones = "SELECT id FROM teso_funciones WHERE nombre = 'Otros Egresos'";
			$row_teso_funciones = mysqli_fetch_assoc(mysqli_query($linkbd, $sql_teso_funciones));
			$idFunAuditoria = $row_teso_funciones["id"];
		
			$sql_auditoria = "SELECT cedula FROM teso_auditoria WHERE teso_funciones_id = $idFunAuditoria AND consecutivo = $this->consecutivo";
			$row_auditoria = mysqli_fetch_assoc(mysqli_query($linkbd, $sql_auditoria));
		
			$documento = $row_auditoria["cedula"];
			$sql_nombre = "SELECT nom_usu FROM usuarios WHERE cc_usu = '$documento' LIMIT 1";
			$row_nombre = mysqli_fetch_assoc(mysqli_query($linkbd, $sql_nombre));
			$nombre = strtoupper($row_nombre["nom_usu"]);

			$this->setY(240);
            $this->setX(70);
            $this->SetFont('helvetica','B',6);
            $this->SetFillColor(153,221,255);
            $this->cell(70,4,'DATOS PERSONALES','LRTB',0,'C',1);
			$this->ln();
			$this->setX(70);
            $this->SetFont('helvetica','B',6);
            $this->SetFillColor(255,255,255);
            $this->MultiCell(70,4," NOMBRE: $nombre","LRTB",'L',true,0,'','',true,0,false,true,0,'M',true);
			$this->ln();
            $this->setX(70);
            $this->SetFont('helvetica','B',6);
            $this->SetFillColor(255,255,255);
            $this->MultiCell(70,4," DOCUMENTO: $documento","LRTB",'L',true,0,'','',true,0,false,true,0,'M',true);
			$this->ln();
            $this->setX(70);
            $this->SetFont('helvetica','',6);
            $this->SetFillColor(255,255,255);
            $this->cell(70,15,'Firma','LRTB',0,'C',1);

            $this->setY(280);
			$this->SetFont('helvetica', 'I', 8);
			$txt = <<<EOD
			$vardirec $vartelef
			$varemail $varpagiw
			EOD;
			$this->SetFont('helvetica', 'I', 6);
			$this->Cell(277,10,'','T',0,'T');
			$this->ln(2);
			$this->Write(0, $txt , '', 0, 'C', true, 0, false, false, 0);

			$this->Cell(50, 10, 'Hecho por: '.$nombre, 00, false, 'C', 0, '', 0, false, 'T', 'M');
			$this->Cell(30, 10, 'Impreso por: '.$user, 0, false, 'C', 0, '', 0, false, 'T', 'M');
			$this->Cell(30, 10, 'IP: '.$ip, 0, false, 'C',0, '', 0, false, 'T', 'M');
			$this->Cell(30, 10, 'Fecha: '.$fecha, 0, false, 'C', 0, '', 0, false, 'T', 'M');
			$this->Cell(30, 10, 'IDEAL.10 S.A.S', 0, false, 'C', 0, '', 0, false, 'T', 'M');
            $this->Cell(20, 10, 'Página ' . $this->getPageNumGroupAlias() . ' de ' . $this->getPageGroupAlias(), 0, false, 'C');
		}
	}
	
    $pdf = new MYPDF('P','mm','Letter', true, 'iso-8859-1', false);
    $pdf->SetDocInfoUnicode (true);
    // set document information
    $pdf->SetCreator(PDF_CREATOR);
    $pdf->SetAuthor('IDEALSAS');
    $pdf->SetTitle('OTROS EGRESOS');
    $pdf->SetSubject('OTROS EGRESOS');
    $pdf->SetKeywords('TCPDF, PDF, example, test, guide');
    $pdf->SetMargins(10, 38, 10);// set margins
    $pdf->SetHeaderMargin(38);// set margins
    $pdf->SetFooterMargin(17);// set margins
    $pdf->SetAutoPageBreak(TRUE, 20);// set auto page breaks
    // set some language-dependent strings (optional)
    if (@file_exists(dirname(__FILE__).'/lang/spa.php'))
    {
        require_once(dirname(__FILE__).'/lang/spa.php');
        $pdf->setLanguageArray($l);
    }
    $pdf->SetFillColor(255,255,255);
    $pdf->AddPage();

    $linkbd = conectar_v7();
    $linkbd -> set_charset("utf8");
    $data = json_decode($_POST["data"], true);
	$cantidadDocumentos = count($data);
    $currentIndex = 0;

    foreach ($data as $d) {
        $currentIndex++;
        $consecutivo = $d["consecutivo"];
        $fecha = DateTime::createFromFormat('Y-m-d', $d["fecha"])->format('d-m-Y');
        $estado = $d["estado"];
        $banco = $d["cuenta_bancaria_entidad"];
        $formaPago = $d["transferencia"] != "" ? "TRANSFERENCIA" : (($d["cheque"] != "") ? "CHEQUE" : "EFECTIVO");
        $documentoBeneficiario = $d["documento"];
        $nombreBeneficiario = $d["nombre"];
        $detalle = $d["detalle"];
        $valorPago = $d["valorPago"];
        $retenciones = $d["valorRetenciones"];
        $valorAPagar = $d["valorAPagar"];
        $arrDescuentos = $d["descuentos"];

        $pdf->SetFont('helvetica','B',9);
        $pdf->SetFillColor(153,221,255);
        $pdf->MultiCell(190,10,"OTROS EGRESOS","LRBT",'C',true,0,'','',true,0,false,true,0,'M',true);
        $pdf->ln();
        $pdf->SetFillColor(255,255,255);
        $pdf->SetTextColor(0,0,0);
        $pdf->SetFont('Helvetica','',8);
        $pdf->MultiCell(25,5,"CONSECUTIVO:","LRBT",'L',true,0,'','',true,0,false,true,0,'M',true);
        $pdf->MultiCell(25,5,$consecutivo,"LRBT",'C',true,0,'','',true,0,false,true,0,'M',true);
        $pdf->MultiCell(25,5,"FECHA:","LRBT",'L',true,0,'','',true,0,false,true,0,'M',true);
        $pdf->MultiCell(35,5,$fecha,"LRBT",'C',true,0,'','',true,0,false,true,0,'M',true);
        $pdf->MultiCell(35,5,"VALOR A PAGAR:","LRBT",'L',true,0,'','',true,0,false,true,0,'M',true);
        $pdf->MultiCell(45,5,"$".number_format($valorAPagar, 2),"LRBT",'R',true,0,'','',true,0,false,true,0,'M',true);
        $pdf->ln();
        $pdf->SetFont('helvetica','B',9);
        $pdf->SetFillColor(153,221,255);
        $pdf->MultiCell(190,5,"INFORMACIÓN PAGO","LRBT",'C',true,0,'','',true,0,false,true,0,'M',true);
        $pdf->ln();
        $pdf->SetFillColor(255,255,255);
        $pdf->SetTextColor(0,0,0);
        $pdf->SetFont('Helvetica','',8);
        $pdf->MultiCell(40,5,"CUENTA BANCARIA ENTIDAD:","LRBT",'L',true,0,'','',true,0,false,true,0,'M',true);
        $pdf->MultiCell(80,5,"$banco","LRBT",'L',true,0,'','',true,0,false,true,0,'M',true);
        $pdf->MultiCell(30,5,"FORMA PAGO:","LRBT",'L',true,0,'','',true,0,false,true,0,'M',true);
        $pdf->MultiCell(40,5,$formaPago,"LRBT",'C',true,0,'','',true,0,false,true,0,'M',true);
        $pdf->ln();
        $pdf->SetFont('helvetica','B',9);
        $pdf->SetFillColor(153,221,255);
        $pdf->MultiCell(190,5,"INFORMACIÓN BENEFICIARIO","LRBT",'C',true,0,'','',true,0,false,true,0,'M',true);
        $pdf->ln();
        $pdf->SetFillColor(255,255,255);
        $pdf->SetTextColor(0,0,0);
        $pdf->SetFont('Helvetica','',8);
        $pdf->MultiCell(30,5,"BENEFICIARIO:","LRBT",'L',true,0,'','',true,0,false,true,0,'M',true);
        $pdf->MultiCell(90,5,$nombreBeneficiario,"LRBT",'L',true,0,'','',true,0,false,true,0,'M',true);
        $pdf->MultiCell(30,5,"DOCUMENTO:","LRBT",'L',true,0,'','',true,0,false,true,0,'M',true);
        $pdf->MultiCell(40,5,$documentoBeneficiario,"LRBT",'C',true,0,'','',true,0,false,true,0,'M',true);
        $pdf->ln();
        $pdf->MultiCell(30,16,"DETALLE:","LRBT",'L',true,0,'','',true,0,false,true,0,'M',true);
        $pdf->MultiCell(160,16,$detalle,"LRBT",'L',true,0,'','',true,0,false,true,0,'M',true);
        $pdf->ln();
        $pdf->MultiCell(23,5,"VALOR PAGO:","LRBT",'L',true,0,'','',true,0,false,true,0,'M',true);
        $pdf->MultiCell(40,5,"$".number_format($valorPago, 2),"LRBT",'R',true,0,'','',true,0,false,true,0,'M',true);
        $pdf->MultiCell(24,5,"RETENCIONES:","LRBT",'L',true,0,'','',true,0,false,true,0,'M',true);
        $pdf->MultiCell(40,5,"$".number_format($retenciones, 2),"LRBT",'R',true,0,'','',true,0,false,true,0,'M',true);
        $pdf->MultiCell(23,5,"VALOR A PAGAR:","LRBT",'L',true,0,'','',true,0,false,true,0,'M',true);
        $pdf->MultiCell(40,5,"$".number_format(0, 2),"LRBT",'R',true,0,'','',true,0,false,true,0,'M',true);
        $pdf->ln();
        $pdf->ln();

        if (!empty($arrDescuentos)) {
            $pdf->SetFont('helvetica','B',8);
            $pdf->SetFillColor(153,221,255);
            $pdf->MultiCell(190,5,"RETENCIONES E INGRESOS","LRBT",'C',true,0,'','',true,0,false,true,0,'M',true);
            $pdf->ln();
            $pdf->MultiCell(130,10,"Nombre retención","LRBT",'C',true,0,'','',true,0,false,true,0,'M',true);
            $pdf->MultiCell(60,10,"Valor retención","LRBT",'R',true,0,'','',true,0,false,true,0,'M',true);
            $pdf->ln();
        
            foreach ($arrDescuentos as $desc) {
                $height = $pdf->getNumLines($desc["nombre"])*5;
                $pdf->SetFont('helvetica','',8);
                $pdf->SetFillColor(255,255,255);
                $pdf->MultiCell(130,$height,$desc["nombre"],"LRBT",'L',true,0,'','',true,0,false,true,0,'M',true);
                $pdf->MultiCell(60,$height,"$" . number_format($desc["valor"], 2),"LRBT",'R',true,0,'','',true,0,false,true,0,'M',true);      
        
                $pdf->ln();
        
                $getY = $pdf->getY();
                if ($getY > 220) {
                    $pdf->AddPage();
                }
            }
        }

            $sql_teso_funciones = "SELECT id FROM teso_funciones WHERE nombre = 'Otros Egresos'";
			$row_teso_funciones = mysqli_fetch_assoc(mysqli_query($linkbd, $sql_teso_funciones));
			$idFunAuditoria = $row_teso_funciones["id"];
		
			$sql_auditoria = "SELECT cedula FROM teso_auditoria WHERE teso_funciones_id = $idFunAuditoria AND consecutivo = $consecutivo";
			$row_auditoria = mysqli_fetch_assoc(mysqli_query($linkbd, $sql_auditoria));
		
			$documento = $row_auditoria["cedula"];
			$sql_nombre = "SELECT nom_usu FROM usuarios WHERE cc_usu = '$documento' LIMIT 1";
			$row_nombre = mysqli_fetch_assoc(mysqli_query($linkbd, $sql_nombre));
			$nombre = strtoupper($row_nombre["nom_usu"]);

			$pdf->setY(240);
            $pdf->setX(70);
            $pdf->SetFont('helvetica','B',6);
            $pdf->SetFillColor(153,221,255);
            $pdf->cell(70,4,'DATOS PERSONALES','LRTB',0,'C',1);
			$pdf->ln();
			$pdf->setX(70);
            $pdf->SetFont('helvetica','B',6);
            $pdf->SetFillColor(255,255,255);
            $pdf->MultiCell(70,4," NOMBRE: $nombre","LRTB",'L',true,0,'','',true,0,false,true,0,'M',true);
			$pdf->ln();
            $pdf->setX(70);
            $pdf->SetFont('helvetica','B',6);
            $pdf->SetFillColor(255,255,255);
            $pdf->MultiCell(70,4," DOCUMENTO: $documento","LRTB",'L',true,0,'','',true,0,false,true,0,'M',true);
			$pdf->ln();
            $pdf->setX(70);
            $pdf->SetFont('helvetica','',6);
            $pdf->SetFillColor(255,255,255);
            $pdf->cell(70,15,'Firma','LRTB',0,'C',1);

        if ($currentIndex != $cantidadDocumentos) {
            $pdf->startPageGroup();
            $pdf->AddPage();
        }
    }
	
    $pdf->Output('otro_egreso'.'.pdf', 'I');
?>
