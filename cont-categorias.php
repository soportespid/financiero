<?php
header("Cache-control: no-cache, no-store, must-revalidate");
header("Content-Type: text/html;charset=utf8");

require 'comun.inc';
require 'funciones.inc';

$linkbd = conectar_v7();
$linkbd->set_charset("utf8");

session_start();
date_default_timezone_set("America/Bogota");
?>
<!DOCTYPE>
<html lang="es">

<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1" />
    <meta name="viewport" content="user-scalable=no">
    <title>:: IDEAL 10 - Parametrización</title>
    <link href="favicon.ico" rel="shortcut icon" />
    <link href="css/css2.css?<?php echo date('d_m_Y_h_i_s'); ?>" rel="stylesheet" type="text/css">
    <link href="css/css3.css?<?php echo date('d_m_Y_h_i_s'); ?>" rel="stylesheet" type="text/css">
    <link href="css/style.css?<?php echo date('d_m_Y_h_i_s'); ?>" rel="stylesheet" type="text/css">
    <script type="text/javascript" src="css/programas.js?<?php echo date('d_m_Y_h_i_s'); ?>"></script>
    <script type="text/javascript" src="JQuery/jquery-2.1.4.min.js?<?php echo date('d_m_Y_h_i_s'); ?>"></script>
    <script type="text/javascript" src="JQuery/alphanum/jquery.alphanum.js?<?php echo date('d_m_Y_h_i_s'); ?>"></script>
    <script>
        function guardar() {
            var validacion01 = document.getElementById('codigo').value;
            var validacion02 = document.getElementById('nombre').value;
            if ((validacion01.trim() != '') && (validacion02.trim() != '')) { despliegamodalm('visible', '4', 'Esta Seguro de Guardar', '1'); }
            else { despliegamodalm('visible', '2', 'Falta informacion para Crear el Centro Costo'); }
        }
        function despliegamodalm(_valor, _tip, mensa, pregunta) {
            document.getElementById("bgventanamodalm").style.visibility = _valor;
            if (_valor == "hidden") {
                document.getElementById('ventanam').src = "";
                if (document.getElementById('valfocus').value == "2") {
                    document.getElementById('valfocus').value = '1';
                    document.getElementById('codigo').focus();
                    document.getElementById('codigo').select();
                }
            }
            else {
                switch (_tip) {
                    case "1":
                        document.getElementById('ventanam').src = "ventana-mensaje1.php?titulos=" + mensa; break;
                    case "2":
                        document.getElementById('ventanam').src = "ventana-mensaje3.php?titulos=" + mensa; break;
                    case "3":
                        document.getElementById('ventanam').src = "ventana-mensaje2.php?titulos=" + mensa; break;
                    case "4":
                        document.getElementById('ventanam').src = "ventana-consulta1.php?titulos=" + mensa + "&idresp=" + pregunta; break;
                }
            }
        }
        function funcionmensaje() { document.location.href = "cont-categorias.php"; }
        function respuestaconsulta(pregunta) {
            switch (pregunta) {
                case "1":
                    document.form2.oculto.value = "2";
                    document.form2.submit();
                    break;
            }
        }
        function valcodigo() { document.form2.oculto.value = "8"; document.form2.submit(); }
    </script>
</head>

<body>
    <IFRAME src="alertas.php" name="alertas" id="alertas" style="display:none"></IFRAME>
    <span id="todastablas2"></span>
    <table>
        <tr>
            <script>barra_imagenes("para");</script><?php cuadro_titulos(); ?>
        </tr>
        <tr><?php menu_desplegable("para"); ?></tr>
    </table>
    <div class="bg-white group-btn p-1"><button type="button" onclick="location.href='cont-categorias.php'"
            class="btn btn-white btn-primary-hover d-flex justify-between align-items-center">
            <span>Nuevo</span>
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 -960 960 960">
                <path d="M440-440H200v-80h240v-240h80v240h240v80H520v240h-80v-240Z"></path>
            </svg>
        </button><button type="button" onclick="guardar();"
            class="btn btn-white btn-primary-hover d-flex justify-between align-items-center">
            <span>Guardar</span>
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 -960 960 960">
                <path
                    d="M840-680v480q0 33-23.5 56.5T760-120H200q-33 0-56.5-23.5T120-200v-560q0-33 23.5-56.5T200-840h480l160 160Zm-80 34L646-760H200v560h560v-446ZM480-240q50 0 85-35t35-85q0-50-35-85t-85-35q-50 0-85 35t-35 85q0 50 35 85t85 35ZM240-560h360v-160H240v160Zm-40-86v446-560 114Z">
                </path>
            </svg>
        </button><button type="button" onclick="location.href='cont-buscacategorias.php'"
            class="btn btn-white btn-primary-hover d-flex justify-between align-items-center">
            <span>Buscar</span>
            <svg class="fill-black group-hover:fill-white w-5 h-5" xmlns="http://www.w3.org/2000/svg"
                viewBox="0 -960 960 960">
                <path
                    d="M784-120 532-372q-30 24-69 38t-83 14q-109 0-184.5-75.5T120-580q0-109 75.5-184.5T380-840q109 0 184.5 75.5T640-580q0 44-14 83t-38 69l252 252-56 56ZM380-400q75 0 127.5-52.5T560-580q0-75-52.5-127.5T380-760q-75 0-127.5 52.5T200-580q0 75 52.5 127.5T380-400Z">
                </path>
            </svg>
        </button><button type="button" onclick="mypop=window.open('plan-agenda.php','','');mypop.focus()"
            class="btn btn-white btn-primary-hover d-flex justify-between align-items-center">
            <span class="group-hover:text-white">Agenda</span>
            <svg class="fill-black group-hover:fill-white w-5 h-5" xmlns="http://www.w3.org/2000/svg"
                viewBox="0 -960 960 960">
                <path
                    d="M560-564v-68q33-14 67.5-21t72.5-7q26 0 51 4t49 10v64q-24-9-48.5-13.5T700-600q-38 0-73 9.5T560-564Zm0 220v-68q33-14 67.5-21t72.5-7q26 0 51 4t49 10v64q-24-9-48.5-13.5T700-380q-38 0-73 9t-67 27Zm0-110v-68q33-14 67.5-21t72.5-7q26 0 51 4t49 10v64q-24-9-48.5-13.5T700-490q-38 0-73 9.5T560-454ZM260-320q47 0 91.5 10.5T440-278v-394q-41-24-87-36t-93-12q-36 0-71.5 7T120-692v396q35-12 69.5-18t70.5-6Zm260 42q44-21 88.5-31.5T700-320q36 0 70.5 6t69.5 18v-396q-33-14-68.5-21t-71.5-7q-47 0-93 12t-87 36v394Zm-40 118q-48-38-104-59t-116-21q-42 0-82.5 11T100-198q-21 11-40.5-1T40-234v-482q0-11 5.5-21T62-752q46-24 96-36t102-12q58 0 113.5 15T480-740q51-30 106.5-45T700-800q52 0 102 12t96 36q11 5 16.5 15t5.5 21v482q0 23-19.5 35t-40.5 1q-37-20-77.5-31T700-240q-60 0-116 21t-104 59ZM280-494Z">
                </path>
            </svg>
        </button><button type="button" onclick="window.open('para-principal');"
            class="btn btn-white btn-primary-hover d-flex justify-between align-items-center">
            <span>Nueva ventana</span>
            <svg class="fill-black group-hover:fill-white w-5 h-5" xmlns="http://www.w3.org/2000/svg"
                viewBox="0 -960 960 960">
                <path
                    d="M320-120v-80h80v-80H160q-33 0-56.5-23.5T80-360v-400q0-33 23.5-56.5T160-840h640q33 0 56.5 23.5T880-760v400q0 33-23.5 56.5T800-280H560v80h80v80H320ZM160-360h640v-400H160v400Zm0 0v-400 400Z">
                </path>
            </svg>
        </button><button type="button"
            onclick="mypop=window.open('/financiero/cont-categorias.php','','');mypop.focus();"
            class="btn btn-white btn-primary-hover d-flex justify-between align-items-center">
            <span class="group-hover:text-white">Duplicar pantalla</span>
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 -960 960 960">
                <path
                    d="M640-160v-360H160v360h480Zm80-200v-80h80v-360H320v200h-80v-200q0-33 23.5-56.5T320-880h480q33 0 56.5 23.5T880-800v360q0 33-23.5 56.5T800-360h-80ZM160-80q-33 0-56.5-23.5T80-160v-360q0-33 23.5-56.5T160-600h480q33 0 56.5 23.5T720-520v360q0 33-23.5 56.5T640-80H160Zm400-603ZM400-340Z">
                </path>
            </svg>
        </button></div>
    <div id="bgventanamodalm" class="bgventanamodalm">
        <div id="ventanamodalm" class="ventanamodalm">
            <IFRAME src="" name="ventanam" marginWidth=0 marginHeight=0 frameBorder=0 id="ventanam" frameSpacing=0
                style=" width:700px; height:130px; top:200; overflow:hidden;">
            </IFRAME>
        </div>
    </div>
    <form name="form2" method="post" action="cont-categorias.php">
        <table class="inicio" align="center">
            <tr>
                <td class="titulos" colspan="6">.: Agregar Categoria de comprobante</td>
                <td class="cerrar" style="width:7%;"><a onClick="location.href='para-principal.php'">&nbsp;Cerrar</a>
                </td>
            </tr>
            <tr>
                <?php
                $sql = "select count(*) FROM categoria_compro";
                $res = mysqli_query($linkbd, $sql);
                $row = mysqli_fetch_row($res);
                $codigo = $row[0];
                $_POST['codigo'] = $row[0] + 1;
                ?>
                <td class="saludo1" style="width:8%;">.: Codigo:</td>
                <td style="width:13%;"><input type="text" name="codigo" id="codigo"
                        value="<?php echo $_POST['codigo'] ?>" style="width:98%;text-align:left"
                        onKeyUp="return tabular(event,this)" readonly /></td>
                <td class="saludo1" style="width:15%;">.: Nombre Categoria:</td>
                <td><input type="text" name="nombre" id="nombre" value="<?php echo $_POST['nombre'] ?>"
                        style="width:80%;" onKeyUp="return tabular(event,this)" /></td>
            </tr>
            <tr>
                <td class="saludo1">.: Activo:</td>
                <td>
                    <select name="estado" id="estado" style="width:50%;">
                        <option value="S" <?php if ($_POST['estado'] == "S") {
                            echo "selected";
                        } ?>>SI</option>
                        <option value="N" <?php if ($_POST['estado'] == "N") {
                            echo "selected";
                        } ?>>NO</option>
                    </select>
                </td>
            </tr>
        </table>
        <input type="hidden" name="oculto" id="oculto" value="1" />
        <input type="hidden" name="valfocus" id="valfocus" value="1" />
        <?php


        if ($_POST['oculto'] == "2") {
            $sqlr = "INSERT INTO categoria_compro (nombre,estado)VALUES ('$_POST[nombre]','$_POST[estado]')";
            if (!mysqli_query($linkbd, $sqlr)) {
                echo "<script>despliegamodalm('visible','2','No se pudo ejecutar la petici�n');</script>";
            } else {
                echo "<script>despliegamodalm('visible','1','Se ha almacenado con Exito');</script>";
            }
        }
        ?>
        <script type="text/javascript">$('#nombre').alphanum({ allow: '' });</script>
        <script
            type="text/javascript">$('#codigo').numeric({ allowThouSep: false, allowDecSep: false, allowMinus: false, maxDigits: 2 });</script>
    </form>
</body>

</html>
