<?php 
	header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=utf8");
	require "comun.inc";
	require "funciones.inc";
	date_default_timezone_set("America/Bogota");
?>
<!DOCTYPE >
<html lang="es">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1">
		<meta name="viewport" content="user-scalable=no">
		<link href="favicon.ico" rel="shortcut icon"/>
		<link href="css/css2z.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet"/>
		<link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet"/>
		<script type="text/javascript" src="JQuery/jquery-2.1.4.min.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script>
			function continuar(){parent.funcionmensaje();parent.despliegamodalm("hidden");}
			setTimeout("continuar()",5000);
			var tecla01 = 13; 
			$(document).keydown(function(e){if (e.keyCode == tecla01){continuar();}})
		</script>
	</head>
	<body style="overflow:hidden"></br></br>
		<table id='ventanamensaje1' class='inicio'>
			<tr>
				<td class='saludo1' style="text-align:center;width:100%"><center><img src='imagenes\confirmar2.gif'  style='width:30px'>&nbsp;<?php echo $_GET['titulos'];?>&nbsp;<img src='imagenes\confirmar2.gif'  style='width:30px'></center></td>
			</tr>
		</table>
		<table>
			<tr>
				<td style="padding: 14px;text-align:center">
					<em name="continuar" id="continuar" class="botonflechaverde" onclick="continuar()">Continuar</em>
				</td>
			</tr>
		</table>
	</body>
	<script>document.getElementById('continuar').focus();</script>
</html>
