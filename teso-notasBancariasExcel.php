<?php

	header("Content-Type: text/html;charset=utf-8");
    require_once 'PHPExcel/Classes/PHPExcel.php';
    require "comun.inc";
    require "funciones.inc";
    require 'funcionesSP.inc.php';
    /*ini_set('display_errors', '1');
    ini_set('display_startup_errors', '1');
    error_reporting(E_ALL);*/

    session_start();

    class Plantilla{
        private $linkbd;
        private $arrData;
        public function __construct() {
            $this->linkbd = conectar_v7();
            $this->linkbd->set_charset("utf8");
        }
        public function search(string $search){
            if(!empty($_SESSION)){
                $request = $this->selectData($search);
                return $request;
            }
        }
        public function selectData($search=""){
            $s="";
            if($search !=""){
                $s="WHERE codigo LIKE '$search%' OR nombre LIKE '$search%'";
            }
            $sql = "SELECT codigo,nombre,tipo,estado
            FROM tesogastosbancarios $s
            ORDER BY codigo DESC";
            $arrData = mysqli_fetch_all(mysqli_query($this->linkbd,$sql),MYSQLI_ASSOC);
            return $arrData;
        }
    }

    if($_GET){
        $obj = new Plantilla();
        $request = $obj->search($_GET['search']);
        //dep($request);exit;
        $objPHPExcel = new PHPExcel();
        $objPHPExcel->getActiveSheet()->getStyle('A:J')->applyFromArray(
            array(
                'alignment' => array(
                    'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
                    'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER
                )
            )
        );
        $objPHPExcel->getProperties()
        ->setCreator("IDEAL 10")
        ->setLastModifiedBy("IDEAL 10")
        ->setTitle("Exportar Excel con PHP")
        ->setSubject("Documento de prueba")
        ->setDescription("Documento generado con PHPExcel")
        ->setKeywords("usuarios phpexcel")
        ->setCategory("reportes");

        //----Cuerpo de Documento----
        $objPHPExcel->setActiveSheetIndex(0)
        ->mergeCells('A1:D1')
        ->mergeCells('A2:D2')
        ->setCellValue('A1', 'TESORERÍA')
        ->setCellValue('A2', 'REPORTE NOTAS BANCARIAS');
        $objPHPExcel-> getActiveSheet ()
        -> getStyle ("A1")
        -> getFill ()
        -> setFillType (PHPExcel_Style_Fill :: FILL_SOLID)
        -> getStartColor ()
        -> setRGB ('C8C8C8');
        $objPHPExcel-> getActiveSheet ()
        -> getStyle ("A1:A2")
        -> getFont ()
        -> setBold ( true )
        -> setName ( 'Verdana' )
        -> setSize ( 10 )
        -> getColor ()
        -> setRGB ('000000');
        $objPHPExcel-> getActiveSheet ()
        -> getStyle ('A1:A2')
        -> getAlignment ()
        -> applyFromArray (array ( 'horizontal'  =>  PHPExcel_Style_Alignment :: HORIZONTAL_CENTER ,) );
        $objPHPExcel-> getActiveSheet ()
        -> getStyle ('A3:D3')
        -> getAlignment ()
        -> applyFromArray (array ( 'horizontal'  =>  PHPExcel_Style_Alignment :: VERTICAL_JUSTIFY ,) );
        $objPHPExcel-> getActiveSheet ()
        -> getStyle ("A2")
        -> getFill ()
        -> setFillType (PHPExcel_Style_Fill :: FILL_SOLID);

        $borders = array(
            'borders' => array(
                'allborders' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN,
                'color' => array('argb' => 'FF000000'),
                )
            ),
        );
        $objPHPExcel->setActiveSheetIndex(0)
        ->setCellValue('A3', 'Código')
        ->setCellValue('B3', "Nombre")
        ->setCellValue('C3', "Tipo")
        ->setCellValue('D3', "Estado");
        $objPHPExcel-> getActiveSheet ()
            -> getStyle ("A3:D3")
            -> getFill ()
            -> setFillType (PHPExcel_Style_Fill :: FILL_SOLID)
            -> getStartColor ()
            -> setRGB ('99ddff');
        $objPHPExcel->getActiveSheet()->getStyle("A3:D3")->getFont()->setBold(true);
        $objPHPExcel->getActiveSheet()->getStyle('A1:D1')->applyFromArray($borders);
        $objPHPExcel->getActiveSheet()->getStyle('A2:D2')->applyFromArray($borders);
        $objPHPExcel->getActiveSheet()->getStyle('A3:D3')->applyFromArray($borders);

        $objWorksheet = $objPHPExcel->getActiveSheet();
        $totalData = count($request);
        $row = 4;
        for ($i=0; $i < $totalData ; $i++) {
            $objPHPExcel->getActiveSheet()->getStyle("A$row:D$row")->applyFromArray($borders);
            $objPHPExcel->setActiveSheetIndex(0)
                ->setCellValueExplicit ("A$row", $request[$i]['codigo'], PHPExcel_Cell_DataType :: TYPE_STRING)
                ->setCellValueExplicit ("B$row", $request[$i]['nombre'], PHPExcel_Cell_DataType :: TYPE_STRING)
                ->setCellValueExplicit ("C$row", $request[$i]['tipo'], PHPExcel_Cell_DataType :: TYPE_STRING)
                ->setCellValueExplicit ("D$row", $request[$i]['estado'] =="S" ? "Activo":"Inactivo" , PHPExcel_Cell_DataType :: TYPE_STRING);
            if($request[$i]['estado'] == "S"){
                $objPHPExcel-> getActiveSheet ()
                -> getStyle ("D$row")
                -> getFill ()
                -> setFillType (PHPExcel_Style_Fill :: FILL_SOLID)
                -> getStartColor ()
                -> setRGB ('198754');
                $objPHPExcel->getActiveSheet()->getStyle("D$row")->getFont()->getColor()->setRGB("ffffff");
                $objPHPExcel->getActiveSheet()->getStyle("D$row")->getFont()->setBold(true);
            }else{
                $objPHPExcel-> getActiveSheet ()
                -> getStyle ("D$row")
                -> getFill ()
                -> setFillType (PHPExcel_Style_Fill :: FILL_SOLID)
                -> getStartColor ()
                -> setRGB ('ff0000');
                $objPHPExcel->getActiveSheet()->getStyle("D$row")->getFont()->getColor()->setRGB("ffffff");
                $objPHPExcel->getActiveSheet()->getStyle("D$row")->getFont()->setBold(true);
            }
            $row++;
        }
        $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
        //----Guardar documento----
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="reporte-notas_bancarias.xlsx"');
        header('Cache-Control: max-age=0');
        $objWriter=PHPExcel_IOFactory::createWriter($objPHPExcel,'Excel2007');
        $objWriter->save('php://output');
        die();
    }
?>
