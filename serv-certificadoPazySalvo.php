<?php
	header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=utf8");
	require 'comun.inc';
	require 'funciones.inc';
	session_start();
	$linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");
	cargarcodigopag($_GET['codpag'],$_SESSION["nivel"]);
	date_default_timezone_set("America/Bogota");
	titlepag();
	if(empty($_SESSION)){
        header("location: index.php");
    }
?>
<!DOCTYPE >
<html lang=es>
	<head>
		<meta charset="utf-8"/>
		<meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1"/>
		<meta name="viewport" content="user-scalable=no">
		<title>:: IDEAL 10 - Servicios Públicos</title>
		<link href="css/css2z.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
		<link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
		<link href="css/css4.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
        <link href="css/style.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css"/>
		<link href="css/tabs.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css"/>
		<script src="vue/vue.min.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script src="css/programas.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script src="css/calendario.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<style>
			input[type=number]::-webkit-inner-spin-button,
			input[type=number]::-webkit-outer-spin-button { 
				-webkit-appearance: none; 
				margin: 0; 
			}
			input[type=number]{
				text-align:right;
				box-sizing: border-box;
				font: 120% sans-serif;
				width: 100% !important;
			}
			[v-cloak]{display : none;}
		</style>
		<script src="sweetalert2/dist/sweetalert2.min.js"></script>
		<link rel="stylesheet" href="sweetalert2/dist/sweetalert2.min.css">
	</head>
	<body>
		<header>
			<table>
				<tr><script>barra_imagenes("serv");</script><?php cuadro_titulos();?></tr>
			</table>
		</header>
		<form name="form2" method="post" action="">
			<section id="myapp" v-cloak >
				<nav>
					<table>
						<tr><?php menu_desplegable("serv");?></tr>
						<tr>
							<td colspan="3" class="cinta">
								<img src="imagenes/add2.png"  class="mgbt1" title="Nuevo">
								<img src="imagenes/guardad.png" title="Guardar"  class="mgbt1">
								<img src="imagenes/buscad.png" class="mgbt1" title="Buscar">
								<img src="imagenes/nv.png" onClick="mypop=window.open('serv-principal.php','',''); mypop.focus();" class="mgbt" title="Nueva Ventana">
								<a href="serv-menuCertificados.php"><img src="imagenes/iratras.png" class="mgbt" alt="Atrás"></a>
							</td>
						</tr>
					</table>
				</nav>
				<article>
					<table class="inicio ancho">
						<tr>
							<td class="titulos" colspan="6" >Certificado de paz y salvo</td>
							<td class="cerrar" style="width:7%" onClick="location.href='serv-principal.php'">Cerrar</td>
						</tr>

						<tr>
							<td class="textonew01" style="width: 10%;">Código de usuario*: </td>
							<td style="width: 15%;">
								<input type="text" v-model="usuario" style="width: 100%; height: 30px; text-align:center;" class="colordobleclik" v-on:dblclick="showUsuarios" autocomplete="off">
							</td>

							<td colspan="2" style="padding-bottom:0px">
                            <button type="button" class="btn btn-primary" @click="searchData">Generar certificado</button>
							</td>
						</tr>
					</table>

					<div id="cargando" v-if="loading" class="loading">
						<span>Cargando...</span>
					</div>

					<div v-show="showModalUsuarios">
						<transition name="modal">
							<div class="modal-mask">
								<div class="modal-wrapper">
									<div class="modal-container2">
										<table class="inicio ancho">
											<tr>
												<td class="titulos" colspan="2" >Listado de usuarios activos</td>
												<td class="cerrar" style="width:7%" @click="showModalUsuarios = false">Cerrar</td>
											</tr>
										</table>
										<table class='tablamv'>
											<thead>
												<tr style="text-align:center;">
													<th class="titulosnew00" style="width:10%;">Codigo de usuario</th>
													<th class="titulosnew00">Nombre</th>
													<th class="titulosnew00" style="width:15%;">Documento</th>
													<th class="titulosnew00" style="width:15%;">Cedula catastral</th>
												</tr>
											</thead>
											<tbody>
												<tr v-for="(usu,index) in usuarios" v-on:click="selecUsuario(usu)" v-bind:class="index % 2 ? 'saludo1a' : 'saludo2'" style='text-rendering: optimizeLegibility; cursor: pointer !important;'>
													<td style="font: 120% sans-serif; padding-left:10px; width:10%; text-align:center;">{{ usu.Codigo }}</td>
													<td style="font: 120% sans-serif; padding-left:10px; text-align:center;">{{ usu.Nombre }}</td>
													<td style="font: 120% sans-serif; padding-left:10px; width:15%; text-align:center;">{{ usu.Documento }}</td>
													<td style="font: 120% sans-serif; padding-left:10px; width:15%; text-align:center;">{{ usu.Catastral }}</td>
												</tr>
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</transition>
					</div>

				</article>
			</section>
		</form>

		<script src="Librerias/vue/vue.min.js"></script>
		<script src="Librerias/vue/axios.min.js"></script>
		<script type="module" src="servicios_publicos/certificados/serv-certificadoPazySalvo.js"></script>
        
	</body>
</html>