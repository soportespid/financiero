<?php
header("Cache-control: no-cache, no-store, must-revalidate");
header("Content-Type: text/html;charset=utf8");

require "comun.inc";
require "funciones.inc";
require "conversor.php";
require "validaciones.inc";

$linkbd = conectar_v7();
$linkbd->set_charset("utf8");

session_start();
date_default_timezone_set("America/Bogota");
?>
<!DOCTYPE>
<html lang="es">

<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1" />
    <meta name="viewport" content="user-scalable=no">
    <title>:: IDEAL 10 - Presupuesto</title>
    <link href="favicon.ico" rel="shortcut icon" />
    <link href="css/css2.css?<?php echo date('d_m_Y_h_i_s'); ?>" rel="stylesheet" type="text/css" />
    <link href="css/css3.css?<?php echo date('d_m_Y_h_i_s'); ?>" rel="stylesheet" type="text/css" />
    <link href="css/style.css?<?php echo date('d_m_Y_h_i_s'); ?>" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="css/programas.js?<?php echo date('d_m_Y_h_i_s'); ?>"> </script>
    <script type="text/javascript" src="JQuery/jquery-2.1.4.min.js"></script>
</head>

<body>
    <IFRAME src="alertas.php" name="alertas" id="alertas" style="display:none"></IFRAME>
    <span id="todastablas2"></span>
    <table>
        <tr>
            <script>barra_imagenes("ccpet");</script><?php cuadro_titulos(); ?>
        </tr>
        <tr><?php menu_desplegable("ccpet"); ?></tr>
    </table>
    <div class="bg-white group-btn p-1"><button type="button"
            onclick="mypop=window.open('ccp-principal.php','','');mypop.focus();"
            class="btn btn-white btn-primary-hover d-flex justify-between align-items-center">
            <span>Nueva ventana</span>
            <svg class="fill-black group-hover:fill-white w-5 h-5" xmlns="http://www.w3.org/2000/svg" viewBox="0 -960 960 960">
                <path d="M320-120v-80h80v-80H160q-33 0-56.5-23.5T80-360v-400q0-33 23.5-56.5T160-840h640q33 0 56.5 23.5T880-760v400q0 33-23.5 56.5T800-280H560v80h80v80H320ZM160-360h640v-400H160v400Zm0 0v-400 400Z">
                </path>
            </svg>
        </button>
    </div>
    <form name="form2" method="post" action="">
        <table class="inicio">
            <tr>
                <td class="titulos" colspan="2">.: Clasificadores </td>
                <td class="cerrar" style="width:7%;"><a href="ccp-principal.php">&nbsp;Cerrar</a></td>
            </tr>
            <td style="background-repeat:no-repeat; background-position:center;">
                <ol id="lista2">
                    <li onClick="location.href='ccp-crearpresupuestoingresos.php'" style="cursor:pointer;">Ingresos</li>
                    <!-- <li onClick="location.href='ccp-crearpresupuestogastos.php'" style="cursor:pointer;">Gastos de funcionamiento</li>  -->
                    <li onClick="location.href='ccp-inicialgastosfun.php'" style="cursor:pointer;">Gastos de
                        funcionamiento y Servicio de la deuda</li>
                    <!-- <li onClick="location.href='ccp-bancoproyctos.php'" style="cursor:pointer;">Gastos de inversi&oacute;n</li> -->
                    <li onClick="location.href='ccp-presupuestoproyectos.php?idr=0'" style="cursor:pointer;">Asignar
                        Presupuesto de inversión
                    </li>
                    <li onClick="location.href='cpp-diferenciaFuentes'" style="cursor:pointer;">
                        Diferencia entre fuentes de financiamiento
                    </li>
                    <li onClick="location.href='cpp-informeCapturaIngresos'" style="cursor:pointer;">
                        Liquidación de presupuesto de ingresos
                    </li>
                </ol>
            </td>
            </tr>
        </table>
    </form>
</body>
<script>
    jQuery(function ($) {
        var user = "<?php echo $_SESSION['cedulausu']; ?>";
        var bloque = '';
        $.post('peticionesjquery/seleccionavigencia.php', { usuario: user }, selectresponse);
        $('#cambioVigencia').change(function (event) {
            var valor = $('#cambioVigencia').val();
            var user = "<?php echo $_SESSION['cedulausu']; ?>";
            var confirma = confirm('¿Realmente desea cambiar la vigencia?');
            if (confirma) {
                var anobloqueo = bloqueo.split("-");
                var ano = anobloqueo[0];
                if (valor < ano) {
                    if (confirm("Tenga en cuenta va a entrar a un periodo bloqueado. Desea continuar")) {
                        $.post('peticionesjquery/cambiovigencia.php', { valor: valor, usuario: user }, updateresponse);
                    } else {
                        location.reload();
                    }
                } else {
                    $.post('peticionesjquery/cambiovigencia.php', { valor: valor, usuario: user }, updateresponse);
                }
            } else {
                location.reload();
            }
        });
        function updateresponse(data) {
            json = eval(data);
            if (json[0].respuesta == '2') {
                alert("Vigencia modificada con exito");
            } else if (json[0].respuesta == '3') {
                alert("Error al modificar la vigencia");
            }
            location.reload();
        }
        function selectresponse(data) {
            json = eval(data);
            $('#cambioVigencia').val(json[0].vigencia);
            bloqueo = json[0].bloqueo;
        }
    });
</script>

</html>
