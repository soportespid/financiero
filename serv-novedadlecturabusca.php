<?php
	header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=iso-8859-1");
	require "comun.inc";
	require "funciones.inc";
	session_start();
	$linkbd = conectar_v7();
	date_default_timezone_set("America/Bogota");

//**niveles menu: Administracion (0) - Consultas (1) - Herramientas (2) - Reportes (3)
?>
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
    <meta http-equiv="Content-Type" content="text/html" />
    <meta http-equiv="X-UA-Compatible" content="IE=9" />
    <title>:: Spid - Servicios P&uacute;blicos</title>

    <script>
		function verUltimaPos(idcta, filas, filtro)
		{
			var scrtop = $('#divdet').scrollTop();
			var altura = $('#divdet').height();
			var numpag = $('#nummul').val();
			var limreg = $('#numres').val();

			if((numpag <= 0)||(numpag == ""))
			{
				numpag = 0;
			}
			if((limreg == 0)||(limreg == ""))
			{
				limreg = 10;
			}
			numpag++;
			location.href="serv-novedadlecturaedita.php?idban="+idcta+"&scrtop="+scrtop+"&totreg="+filas+"&altura="+altura+"&numpag="+numpag+"&limreg="+limreg+"&filtro="+filtro;
		}
    </script>
    <script type="text/javascript" src="css/programas.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
    <link href="css/css2.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
    <link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
    <link href="css/css4.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
	<script type="text/javascript" src="jquery-1.11.0.min.js?<?php echo date('d_m_Y_h_i_s');?>"></script> 
    <?php titlepag();?>
</head>

<body>
    <table>
        <tr>
            <script>
            barra_imagenes("serv");
            </script><?php cuadro_titulos();?>
        </tr>
        <tr><?php menu_desplegable("serv");?></tr>
        <tr>
			<td colspan="3" class="cinta">
                <a href="serv-novedadlectura.php" class="tooltip right mgbt">
                    <img src="imagenes/add.png" />
                    <span class="tiptext">Nuevo</span>
                </a>
                <a class="tooltip bottom mgbt">
                    <img src="imagenes/guardad.png" />
                    <span class="tiptext">Guardar</span>
                </a>
                <a href="serv-novedadlecturabusca.php" class="tooltip bottom mgbt">
                    <img src="imagenes/busca.png" />
                    <span class="tiptext">Buscar</span>
                </a>
                <a onClick="mypop=window.open('serv-principal.php','','');mypop.focus();" class="tooltip bottom mgbt">
                    <img src="imagenes/nv.png">
                    <span class="tiptext">Nueva Ventana</span>
                </a>
                <a onClick="mypop=window.open('<?php echo $url2; ?>','','');mypop.focus();" class="tooltip bottom mgbt">
                    <img src="imagenes/duplicar_pantalla.png">
                    <span class="tiptext">Duplicar Pesta&ntilde;a</span>
                </a>
            </td>
        </tr>
    </table>
	<?php
		if(@ $_POST['oculto'] == '')
		{
			$_POST['cambioestado'] = '';
			$_POST['nocambioestado'] = '';
			$_POST['numres'] = 10;
			$_POST['numpos'] = 0;
			$_POST['nummul'] = 0;
		}
		
		if(@ $_GET['numpag'] != "")
		{
			if(@ $_POST['oculto'] != 2)
			{
				$_POST['numres'] = $_GET['limreg'];
				$_POST['numpos'] = $_GET['limreg']*($_GET['numpag']-1);
				$_POST['nummul'] = $_GET['numpag']-1;
			}
		}
		else
		{
			if(@ $_POST['nummul'] == '')
			{
				$_POST['numres'] = 10;
				$_POST['numpos'] = 0;
				$_POST['nummul'] = 0;
			}
		}
	?>
    <tr>
        <td colspan="4" class="tablaprin">
            <form name="form2" method="post" action="">
				<input type="hidden" name="numres" id="numres" value="<?php echo @ $_POST['numres'];?>"/>
				<input type="hidden" name="numpos" id="numpos" value="<?php echo @ $_POST['numpos'];?>"/>
				<input type="hidden" name="nummul" id="nummul" value="<?php echo @ $_POST['nummul'];?>"/>
				<input type="hidden" name="oculto" id="oculto" value="1">

                <table class="inicio ancho">
                    <tr>
                        <td class="titulos" colspan="5">:: Buscar Novedad de Lectura </td>
                        <td width="11%" class="cerrar"><a href="serv-principal.php">Cerrar</a></td>
                    </tr>
                    <tr>
                        <td width="3%" class="saludo1">Matricula:</td>
                        <td>
							<input name="codigo" type="text" value="" maxlength="9">
                        </td>
                        <td width="4%" class="saludo1">Cliente:</td>
                        <td>
                            <input name="cliente" type="text" id="cliente" style="width:100%" value="<?php echo $_POST[cliente]?>">
                        </td>
						<td style="padding-bottom:0px">
							<em class="botonflecha" onClick="limbusquedas();">Buscar</em>
						</td>	
                    </tr>
                </table>
                <div class="subpantallac5" style="height:69%; width:99.6%; margin-top:0px; overflow-x:hidden" id="divdet">
        		<?php 
					$crit1=" ";
					$crit2=" ";
					if ($_POST[codigo]!=""){
						$crit1=" AND c.id like '%".$_POST[codigo]."%'";
					}
					if ($_POST[cliente]!=""){
						$crit2=" AND concat_ws(t.nombre1, t.nombre2, t.apellido1, t.apellido2, t.razonsocial) LIKE '%".$_POST['cliente']."%'";
					}

					$cond2 = '';
			
					if (@ $_POST['numres'] != "-1")
					{
						$cond2 = "LIMIT ".$_POST['numpos'].", ".$_POST['numres']; 
					}

					$sqlr = "SELECT nl.id, nl.fecha, nl.descripcion, c.id, t.nombre1, t.nombre2, t.apellido1, t.apellido2, t.razonsocial, m.serial, m.referencia, a.nombre, v.estandar, d.direccion_digito, d.direccion_letra_digito, d.direccion_cardinal_digito, d.direccion_conecta, d.direccion_letra_conecta, d.direccion_cardinal_conecta, d.direccion_consecutivo, d.direccion_complemento  FROM terceros t INNER JOIN srvclientes c ON t.id_tercero= c.id_tercero INNER JOIN srvnovedad_lectura nl ON nl.id_cliente = c.id INNER JOIN srvdireccion_cliente d ON d.id_cliente = c.id INNER JOIN srvvia_principal v ON d.direccion_tipo = v.id INNER JOIN srvmedidores m ON nl.id_medidor = m.id INNER JOIN srvanomalias a ON nl.id_novedad = a.id WHERE c.estado = 'S' $crit1 $crit2 ORDER BY CONVERT(c.id, SIGNED INTEGER) DESC $cond2";

					$resp = mysqli_query($linkbd,$sqlr);
					$con = 1;

					$_POST['numtop'] = mysqli_num_rows($resp);
					$nuncilumnas = ceil($_POST['numtop']/$_POST['numres']);
					$numcontrol = $_POST['nummul'] + 1;
					
					if(($nuncilumnas == $numcontrol) || (@ $_POST['numres'] == "-1"))
					{
						$imagenforward = " <img src='imagenes/forward02.png' style='width:17px;cursor:default;'> ";
						$imagensforward = " <img src='imagenes/skip_forward02.png' style='width:16px;cursor:default;'> ";
					}
					else 
					{
						$imagenforward = " <img src='imagenes/forward01.png' style='width:17px;cursor:pointer;' title='Siguiente' onClick='numsiguiente()'> ";
						$imagensforward = " <img src='imagenes/skip_forward01.png' style='width:16px;cursor:pointer;' title='Fin' onClick='saltocol(\"$nuncilumnas\")'> ";
					}
					
					if((@ $_POST['numpos'] == 0) || (@ $_POST['numres'] == '-1'))
					{
						$imagenback = " <img src='imagenes/back02.png' style='width:17px;cursor:default;'> ";
						$imagensback = "<img src='imagenes/skip_back02.png' style='width:16px;cursor:default;'> ";
					}
					else
					{
						$imagenback = " <img src='imagenes/back01.png' style='width:17px;cursor:pointer;' title='Anterior' onClick='numanterior();'> ";
						$imagensback = " <img src='imagenes/skip_back01.png' style='width:16px;cursor:pointer;' title='Inicio' onClick='saltocol(\"1\")'> ";
					}
                
					echo "<table class='inicio' align='center' width='80%'>
						<tr>
							<td colspan='8' class='titulos'>.: Resultados Busqueda:</td>
							<td class='submenu'>
                                <select name='renumres' id='renumres' onChange='cambionum();' style='width:100%'>
                                    <option value='10'"; if (@ $_POST['renumres'] == '10'){echo 'selected';} echo ">10</option>
                                    <option value='20'"; if (@ $_POST['renumres'] == '20'){echo 'selected';} echo ">20</option>
                                    <option value='30'"; if (@ $_POST['renumres'] == '30'){echo 'selected';} echo ">30</option>
                                    <option value='50'"; if (@ $_POST['renumres'] == '50'){echo 'selected';} echo ">50</option>
                                    <option value='100'"; if (@ $_POST['renumres'] == '100'){echo 'selected';} echo ">100</option>
                                    <option value='-1'"; if (@ $_POST['renumres'] == '-1'){echo 'selected';} echo ">Todos</option>
                                </select>
						    </td>
						</tr>
						<tr>
							<td colspan='5'>Encontrados: ".$_POST['numtop']."</td>
						</tr>
						<tr>
							<td class='titulos2'>C&oacute;digo</td>
							<td class='titulos2' style=\"width:10%\">Matricula</td>
							<td class='titulos2' style=\"width:15%\">Nombre Cliente</td>
							<td class='titulos2' style=\"width:8%\">Serial Medidor</td>
							<td class='titulos2'>Referencia Medidor</td>
							<td class='titulos2'>Ultimo Estado</td>
							<td class='titulos2'>Direcci&oacute;n</td>
							<td class='titulos2'style=\"width:8.5%\">Fecha Novedad</td>
							<td class='titulos2'style=\"width:12%\">Descripci&oacute;n</td>
						</tr>";	

					$iter='saludo1a';
					$iter2='saludo2';
					$filas = 1;

					while ($row =mysqli_fetch_array($resp)) 
					{
						$con2 = $con + $_POST['numpos'];

						if($gidcta != '')
						{
							if($gidcta == $row[0])
							{
								$estilo = 'background-color:yellow';
							}
							else 
							{
								$estilo = '';
							}
						}
						else 
						{
							$estilo = '';
						}

						$idcta = "'$row[0]'";
						$numfil = "'$filas'";
						$filtro = "'".@ $_POST['codigo']."'";

						$direccionCompleta = $row['estandar'].' '.$row['direccion_digito'].' '.$row['direccion_letra_digito'].' '.$row['direccion_cardinal_digito'].' '.$row['direccion_conecta'].' '.$row['direccion_letra_conecta'].' '.$row['direccion_cardinal_conecta'].' '.$row['direccion_consecutivo'].' '.$row['direccion_complemento'];

						$matricula = str_pad($row['id'], 10, "0", STR_PAD_LEFT);
						$nombreCompleto = $row['nombre1'].' '.$row['nombre2'].' '.$row['apellido1'].' '.$row['apellido2'];
						if ($row['razonsocial']!='') {
							$nombreCompleto = $row['razonsocial'];
						}

						echo "
							<tr class='$iter' onDblClick=\"verUltimaPos($idcta, $numfil, $filtro)\" style='text-transform:uppercase; $estilo'>
								<td>$row[0]</td>
								<td>".str_pad($row[id], 10, "0", STR_PAD_LEFT)."</td>
								<td>$nombreCompleto</td>
								<td>$row[9] </td>
								<td>$row[10]</td>
								<td>$row[nombre]</td>
								<td>$direccionCompleta</td>
								<td>$row[fecha] </td>
								<td>$row[descripcion] </td>
							</tr>";
						$con += 1;
						$aux = $iter;
						$iter = $iter2;
						$iter2 = $aux;
						$filas++;
					}

					if (@ $_POST['numtop'] == 0)
					{
						echo "
						<table class='inicio'>
							<tr>
								<td class='saludo1' style='text-align:center;width:100%'>
									<img src='imagenes\alert.png' style='width:25px'>No hay coincidencias en la b&uacute;squeda $tibusqueda<img src='imagenes\alert.png' style='width:25px'>
								</td>
							</tr>
						</table>";
					}
					
					echo"
                    </table>
                    
					<table class='inicio'>
						<tr>
							<td style='text-align:center;'>
								<a>$imagensback</a>&nbsp;
                                <a>$imagenback</a>&nbsp;&nbsp;";
                                
                                if($nuncilumnas <= 9)
                                {
                                    $numfin = $nuncilumnas;
                                }
                                else
                                {
                                    $numfin = 9;
                                }
                                
                                for($xx = 1; $xx <= $numfin; $xx++)
				                {
                                    if($numcontrol <= 9)
                                    {
                                        $numx = $xx;
                                    }
                                    else
                                    {
                                        $numx = $xx + ($numcontrol - 9);
                                    }

                                    if($numcontrol == $numx)
                                    {
                                        echo"<a onClick='saltocol(\"$numx\")'; style='color:#24D915;cursor:pointer;'> $numx </a>";
                                    }
                                    else 
                                    {
                                        echo"<a onClick='saltocol(\"$numx\")'; style='color:#000000;cursor:pointer;'> $numx </a>";
                                    }
                                }
                                
                            echo"			
                                &nbsp;&nbsp;<a>$imagenforward</a>
                                &nbsp;<a>$imagensforward</a>

                            </td>
                        </tr>
                    </table> ";

            ?>
            
            <input type="hidden" name="ocules" id="ocules" value="<?php echo @ $_POST['ocules'];?>">
            
            <input type="hidden" name="actdes" id="actdes" value="<?php echo @ $_POST['actdes'];?>">
            
            <input type="hidden" name="numtop" id="numtop" value="<?php echo @ $_POST['numtop'];?>">
            
            </form>
        </td>
    </tr>
    </table>
</body>

</html>