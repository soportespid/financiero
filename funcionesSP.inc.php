<?php
    require_once 'comun.inc';

    function buscaClasificadorPresupuestal($cuenta,$tipoCuenta)
    {
        //Funcion que busca el numero clasificador
        $clasificador = '';
        $linkbd = conectar_v7();

        if($tipoCuenta == 1)
        {
            $sqlr = "SELECT clasificadores FROM ccpetprogramarclasificadores WHERE cuenta = '$cuenta' ";
            $resp = mysqli_query($linkbd, $sqlr);
            $row = mysqli_fetch_row($resp);

            $clasificador = $row[0];
        }

        if($tipoCuenta == 2)
        {
            $sqlr2 = "SELECT clasificadores FROM ccpetprogramarclasificadoresgastos WHERE cuenta = '$cuenta' ";
            $resp2 = mysqli_query($linkbd, $sqlr2);
            $row2 = mysqli_fetch_row($resp2);

            $clasificador = $row2[0];
        }

        return $clasificador;
    }

    function buscaNombreCLasificador($codigo,$tipo)
    {
        $linkbd = conectar_v7();

        $nombreCuenta = '';

        switch($tipo)
        {
            case 1:
                $sqlCuin = "SELECT nombre FROM ccpet_cuin WHERE codigo_cuin = '$codigo' ";
                $resCuin = mysqli_query($linkbd, $sqlCuin);
                $rowCuin = mysqli_fetch_row($resCuin);

                $nombreCuenta = $rowCuin[0];
            break;

            case 2:
                $sqlBienesTransportables = "SELECT titulo FROM ccpetbienestransportables WHERE grupo = '$codigo' ";
                $resBienesTransportables = mysqli_query($linkbd, $sqlBienesTransportables);
                $rowBienesTransportables = mysqli_fetch_row($resBienesTransportables);

                $nombreCuenta = $rowBienesTransportables[0];
            break;

            case 3:
                $sqlServicios = "SELECT titulo FROM ccpetservicios WHERE grupo = '$codigo' ";
                $resServicios = mysqli_query($linkbd, $sqlServicios);
                $rowServicios = mysqli_fetch_row($resServicios);

                $nombreCuenta = $rowServicios[0];
            break;
        }

        return $nombreCuenta;
    }

    function buscaNombreTercero($idDocumento)
    {
        $linkbd = conectar_v7();
        $nombreCuenta = '';
        $datosTerceros = array();

        $sqlTercero = "SELECT nombre1, nombre2, apellido1, apellido2, razonsocial, id_tercero FROM terceros WHERE cedulanit = $idDocumento ";
        $resTercero = mysqli_query($linkbd,$sqlTercero);
        $rowTercero = mysqli_fetch_assoc($resTercero);

        if($rowTercero['razonsocial'] == '')
        {
            $nombreTercero = $rowTercero['nombre1']. ' ' .$rowTercero['nombre2']. ' ' .$rowTercero['apellido1']. ' ' .$rowTercero['apellido2'];
        }
        else
        {
            $nombreTercero = $rowTercero['razonsocial'];
        }

        $datosTerceros = [$nombreTercero,$rowTercero['id_tercero']];

        return $datosTerceros;
    }

    function buscaNombreTerceroConId($id)
    {
        $linkbd = conectar_v7();
        $linkbd -> set_charset("utf8");
        $nombre = '';

        $sqlTercero = "SELECT nombre1, nombre2, apellido1, apellido2, razonsocial FROM terceros WHERE id_tercero = $id ";
        $resTercero = mysqli_query($linkbd,$sqlTercero);
        $rowTercero = mysqli_fetch_assoc($resTercero);

        if($rowTercero['razonsocial'] == '')
        {
            $nombre = $rowTercero['nombre1']. ' ' .$rowTercero['nombre2']. ' ' .$rowTercero['apellido1']. ' ' .$rowTercero['apellido2'];
        }
        else
        {
            $nombre = $rowTercero['razonsocial'];
        }

        $nombre = Quitar_Espacios($nombre);

        return $nombre;
    }

    function buscaDocumentoTerceroConId($id)
    {
        $linkbd = conectar_v7();
        $documento = '';

        $sqlTercero = "SELECT cedulanit FROM terceros WHERE id_tercero = $id ";
        $resTercero = mysqli_query($linkbd,$sqlTercero);
        $rowTercero = mysqli_fetch_row($resTercero);

        if($rowTercero[0] != '')
        {
            $documento = $rowTercero[0];
        }

        mysqli_close($linkbd);
        return $documento;
    }

    function encuentraNombreTerceroConIdCliente($id)
    {
        $linkbd = conectar_v7();
        $linkbd -> set_charset("utf8");
        $nombreCompleto = '';

        $sqlCliente = "SELECT id_tercero FROM srvclientes WHERE id = $id";
        $resCliente = mysqli_query($linkbd,$sqlCliente);
        $rowCliente = mysqli_fetch_row($resCliente);

        $sqlTercero = "SELECT nombre1, nombre2, apellido1, apellido2, razonsocial FROM terceros WHERE id_tercero = '$rowCliente[0]' ";
        $resTercero = mysqli_query($linkbd,$sqlTercero);
        $rowTercero = mysqli_fetch_row($resTercero);

        if ($rowTercero != '')
        {
            if($rowTercero[4] == '')
            {
                $nombreCompleto = $rowTercero[0]. ' ' .$rowTercero[1]. ' ' .$rowTercero[2]. ' ' .$rowTercero[3];
            }
            else
            {
                $nombreCompleto = $rowTercero[4];
            }
        }
        else
        {
            $nombreCompleto = "USUARIO NO ENCONTRADO";
        }

        return $nombreCompleto;
    }

    function codUsuarioConIdCliente($id)
    {
        $linkbd = conectar_v7();
        $codUsuario = '';

        $sqlCliente = "SELECT cod_usuario FROM srvclientes WHERE id = $id";
        $resCliente = mysqli_query($linkbd,$sqlCliente);
        $rowCliente = mysqli_fetch_row($resCliente);

        if ($rowCliente[0] != '') {

            $codUsuario = $rowCliente[0];
        } else {
            $codUsuario = "COD USUARIO NO ENCONTRADO";
        }

        return $codUsuario;
    }

    function encuentraDocumentoTerceroConIdCliente($id)
    {
        $linkbd = conectar_v7();
        $cedulanit = '';

        $sqlCliente = "SELECT id_tercero FROM srvclientes WHERE id = $id";
        $resCliente = mysqli_query($linkbd,$sqlCliente);
        $rowCliente = mysqli_fetch_assoc($resCliente);

        $sqlTercero = "SELECT cedulanit FROM terceros WHERE id_tercero = '$rowCliente[id_tercero]' ";
        $resTercero = mysqli_query($linkbd,$sqlTercero);
        $rowTercero = mysqli_fetch_assoc($resTercero);

        $cedulanit = $rowTercero['cedulanit'];

        return $cedulanit;
    }

    function validaCodigoUsuario($codigoUsuario)
    {
        $linkbd = conectar_v7();
        $existe = "N";

        $sqlCliente = "SELECT cod_usuario FROM srvclientes WHERE cod_usuario = $codigoUsuario";
        $resCliente = mysqli_query($linkbd,$sqlCliente);
        $rowCliente = mysqli_fetch_row($resCliente);

        if($rowCliente[0] != '')
        {
            $existe = "S";
        }

        return $existe;
    }

    function encontrarIdClienteCodUsuario($codigoUsuario)
    {
        $linkbd = conectar_v7();
        $id = "";

        $sqlCliente = "SELECT id FROM srvclientes WHERE cod_usuario = $codigoUsuario";
        $resCliente = mysqli_query($linkbd,$sqlCliente);
        $rowCliente = mysqli_fetch_row($resCliente);

        $id = $rowCliente[0];

        return $id;
    }

    function encuentraDireccionConIdCliente($id)
    {
        $linkbd = conectar_v7();
        $linkbd -> set_charset("utf8");
        $direccion = '';

        $sqlDireccion = "SELECT direccion FROM srvdireccion_cliente WHERE id_cliente = '$id'";
        $resDireccion = mysqli_query($linkbd,$sqlDireccion);
        $rowDireccion = mysqli_fetch_assoc($resDireccion);

        $direccion = $rowDireccion['direccion'];

        return $direccion;
    }

    function consultaCortesActivos()
    {
        $linkbd = conectar_v7();
        $bandera = false;

        $sqlCortes = "SELECT COUNT(numero_corte) FROM srvcortes WHERE estado = 'S' ";
        $resCortes = mysqli_query($linkbd,$sqlCortes);
        $rowCortes = mysqli_fetch_row($resCortes);

        if($rowCortes[0] == 0)
        {
            $bandera = false;
        }
        else
        {
            $bandera = true;
        }

        return $bandera;
    }

    function buscaCuentaEnCuentasnicsp($cuenta)
    {
        $linkbd = conectar_v7();
        $nombreCuenta = '';

        $sql = "SELECT nombre FROM cuentasnicsp WHERE cuenta = '$cuenta'";
        $res = mysqli_query($linkbd,$sql);
        $row = mysqli_fetch_row($res);

        $nombreCuenta = $row[0];

        return $nombreCuenta;
    }

    function buscaNombreTipoMovimiento($movimiento)
    {
        $linkbd = conectar_v7();

        $codigo = substr($movimiento,1);
        $tipoMovimiento = substr($movimiento,0,-2);

        $query = "SELECT nombre FROM srvtipo_movimiento WHERE codigo = '$codigo' AND tipo_movimiento = '$tipoMovimiento'";
        $resp = mysqli_query($linkbd,$query);
        $row = mysqli_fetch_row($resp);

        $nombre = $row[0];
        return $nombre;
    }

    function buscaEstratoConClase($id)
    {
        $linkbd = conectar_v7();

        $nombreEstrato = '';
        $sql = "SELECT upper(descripcion), uso FROM srvestratos WHERE id = '$id'";
        $res = mysqli_query($linkbd,$sql);
        $row = mysqli_fetch_row($res);

        $nombreEstrato = $row[0];

        return $nombreEstrato;
    }

    function buscaCorteActivo()
    {
        $linkbd = conectar_v7();

        $query = "SELECT numero_corte FROM srvcortes WHERE estado = 'S'";
        $res = mysqli_query($linkbd,$query);
        $row = mysqli_fetch_assoc($res);

        $corte = $row['numero_corte'];

        return $corte;
    }

    function siguienteCorte()
    {
        $linkbd = conectar_v7();

        $query = "SELECT MAX(numero_corte) FROM srvcortes";
        $res = mysqli_query($linkbd,$query);
        $row = mysqli_fetch_row($res);

        $corte = $row[0] + 1;

        return $corte;
    }

    function ultimoCorteFacturado()
    {
        $linkbd = conectar_v7();

        $query = "SELECT numero_corte FROM srvcortes WHERE estado = 'C' ORDER BY numero_corte DESC LIMIT 1";
        $res = mysqli_query($linkbd,$query);
        $row = mysqli_fetch_assoc($res);

        $corte = $row['numero_corte'];

        return $corte;
    }

    function siguienteConsecutivoContabilidad($tipoCombrobante)
    {
        $linkbd = conectar_v7();

        $query = "SELECT numerotipo FROM comprobante_cab WHERE tipo_comp = '$tipoCombrobante' ORDER BY numerotipo DESC LIMIT 1";
        $resp = mysqli_query($linkbd,$query);
        $row = mysqli_fetch_row($resp);

        $consecutivo = $row[0] + 1;

        return $consecutivo;
    }

    function validaExistenciaId($tabla, $id)
    {
        $linkbd = conectar_v7();
        $resultado = '';

        $query = "SELECT id FROM $tabla WHERE id = $id";
        $resp = mysqli_query($linkbd, $query);
        $row = mysqli_fetch_row($resp);

        if ($row[0] == '')
        {
            $resultado = 1;
        }
        else
        {
            $resultado = 0;
        }

        return $resultado;
    }

    function buscarNombreEstrato($id_estrato)
    {
        $linkbd = conectar_v7();
        $resultado = '';

        $query = "SELECT descripcion FROM srvestratos WHERE id = $id_estrato";
        $resp = mysqli_query($linkbd,$query);
        $row = mysqli_fetch_row($resp);

        if ($row[0] != '' && isset($row[0]))
        {
            $resultado = $row[0];
        }
        else
        {
            $resultado = "ESTRATO NO ENCONTRADO";
        }

        return $resultado;
    }

    function buscaLectura($id_cliente, $corte, $servicio) {

        $linkbd = conectar_v7();
        $lectura = '';

        $sqlLectura = "SELECT lectura_medidor FROM srvlectura WHERE corte = $corte AND id_cliente = $id_cliente AND id_servicio = $servicio";
        $resLectura = mysqli_query($linkbd,$sqlLectura);
        $rowLectura = mysqli_fetch_row($resLectura);

        if ($rowLectura[0] != '') {
            $lectura = $rowLectura[0];
        } else {
            $lectura = "N/E";
        }

        return $lectura;
    }

    function buscaConsumo($id_cliente, $corte, $servicio) {

        $linkbd = conectar_v7();
        $consumo = '';

        $sqlConsumo = "SELECT consumo FROM srvlectura WHERE corte = $corte AND id_cliente = $id_cliente AND id_servicio = $servicio";
        $resConsumo = mysqli_query($linkbd,$sqlConsumo);
        $rowConsumo = mysqli_fetch_row($resConsumo);

        if ($rowConsumo[0] != '') {
            $consumo = $rowConsumo[0];
        } else {
            $consumo = "N/E";
        }

        return $consumo;
    }

    function buscaValorDetalles($numeroFactura,$idCobro,$servicio) {

        $linkbd = conectar_v7();
        $valor = 0;

        $sql = "SELECT credito, debito FROM srvdetalles_facturacion WHERE numero_facturacion = $numeroFactura AND id_servicio = $servicio AND id_tipo_cobro = $idCobro AND tipo_movimiento = '101   '";
        $res = mysqli_query($linkbd,$sql);
        $row = mysqli_fetch_row($res);


        if($row[0] == '' || $row[1] == '') {

            $valor = 0;
        }

        if ($row[0] > 0 || $row[1] == 0) {

            $valor = $row[0];
        }

        if ($row[0] == 0 || $row[1] > 0){
            $valor = $row[1];
        }
        return $valor;
    }

    function buscarAbonos ($numeroFactura, $servicio){

        $linkbd = conectar_v7();
        $valor = 0;

        $sql = "SELECT credito, debito FROM srvdetalles_facturacion WHERE numero_facturacion = $numeroFactura AND id_servicio = $servicio AND id_tipo_cobro = 14";
        $res = mysqli_query($linkbd,$sql);
        $row = mysqli_fetch_row($res);


        if($row[0] == '' || $row[1] == '') {

            $valor = 0;
        }

        if ($row[0] > 0 || $row[1] == 0) {

            $valor = $row[0];
        }

        if ($row[0] == 0 || $row[1] > 0){
            $valor = $row[1];
        }
        return $valor;
    }

    function buscarSaldoInicial ($numeroFactura, $servicio) {

        $linkbd = conectar_v7();
        $valor = 0;

        $sql = "SELECT debito FROM srvdetalles_facturacion WHERE numero_facturacion = $numeroFactura AND id_servicio = $servicio AND id_tipo_cobro = 9";
        $res = mysqli_query($linkbd,$sql);
        $row = mysqli_fetch_row($res);

        if($row[0] == '') {
            $row[0] == 0;
        }

        $sql1 = "SELECT credito FROM srvdetalles_facturacion WHERE numero_facturacion = $numeroFactura AND id_servicio = $servicio AND id_tipo_cobro = 10";
        $res1 = mysqli_query($linkbd,$sql1);
        $row1 = mysqli_fetch_row($res1);

        if ($row1[0] == '') {

            $row1[0] == 0;
        }


        if ($row[0] == 0 && $row1[0] == 0)
        {
            $valor = 0;
        }

        if ($row[0] > 0 && $row1[0] == 0)
        {
            $valor = $row[0] * -1;
        }

        if ($row[0] == 0 && $row1[0] > 0)
        {
            $valor = $row1[0];
        }

        return $valor;
    }

    function dep($array) {
        print "<pre>";
        print_r($array);
        print"</pre>";
    }
    function consultaValorFactura($numeroFactura) {

        $linkbd = conectar_v7();
        $valorFactura = 0;

        $sql = "SELECT SUM(credito), SUM(debito) FROM srvdetalles_facturacion WHERE numero_facturacion = $numeroFactura AND tipo_movimiento = '101'";
        $res = mysqli_query($linkbd,$sql);
        $row = mysqli_fetch_row($res);

        $valorFactura = $row[0] - $row[1];

        return $valorFactura;
    }
    function consultaValorFacturaSinDeuda($numeroFactura) {

        $linkbd = conectar_v7();
        $valorFactura = 0;

        $sql = "SELECT SUM(credito), SUM(debito) FROM srvdetalles_facturacion WHERE numero_facturacion = $numeroFactura AND id_tipo_cobro != 8 AND id_tipo_cobro != 10 AND tipo_movimiento = '101'";
        $res = mysqli_query($linkbd,$sql);
        $row = mysqli_fetch_row($res);

        $valorFactura = $row[0] - $row[1];

        return $valorFactura;
    }

    function consultaValorIntereses($numeroFactura) {

        $linkbd = conectar_v7();
        $intereses = 0;

        $sql = "SELECT SUM(credito), SUM(debito) FROM srvdetalles_facturacion WHERE numero_facturacion = $numeroFactura AND id_tipo_cobro = 10 AND tipo_movimiento = '101'";
        $res = mysqli_query($linkbd,$sql);
        $row = mysqli_fetch_row($res);

        $intereses = $row[0] - $row[1];

        return $intereses;
    }

    function consultaValorTotalSRVFACTURAS($numeroFactura)
    {
        $linkbd = conectar_v7();
        $credito = 0;
        $debito = 0;
        $valor = 0;

        $sql = "SELECT cargo_f, consumo_b, consumo_c, consumo_s, subsidio_cf, subsidio_cb, subsidio_cc, subsidio_cs, contribucion_cf, contribucion_cb, contribucion_cc, contribucion_cs, deuda_anterior, abonos, acuerdo_pago, venta_medidor, interes_mora, alumbrado, desincentivo FROM srvfacturas WHERE num_factura = $numeroFactura";
        $res = mysqli_query($linkbd,$sql);
        while($row = mysqli_fetch_row($res))
        {
            $credito += $row[0] + $row[1] + $row[2] + $row[3] + $row[8] + $row[9] + $row[10] + $row[11] + $row[12] + $row[14] + $row[15] + $row[16] + $row[17] + $row[18];
            $debito += $row[4] + $row[5] + $row[6] + $row[7] + $row[13];
        }

        $valor = $credito - $debito;

        return $valor;
    }

    function valorServicioIGAC($numeroFactura, $servicioId) {
        $linkbd = conectar_v7();
        $credito = 0;
        $debito = 0;
        $valor = 0;

        $sql = "SELECT cargo_f, consumo_b, consumo_c, consumo_s, subsidio_cf, subsidio_cb, subsidio_cc, subsidio_cs, contribucion_cf, contribucion_cb, contribucion_cc, contribucion_cs, deuda_anterior, abonos, acuerdo_pago, venta_medidor, interes_mora, alumbrado FROM srvfacturas WHERE num_factura = $numeroFactura AND id_servicio = $servicioId";
        $res = mysqli_query($linkbd,$sql);
        while($row = mysqli_fetch_row($res))
        {
            $credito += $row[0] + $row[1] + $row[2] + $row[3] + $row[8] + $row[9] + $row[10] + $row[11] + $row[12] + $row[14] + $row[15] + $row[16] + $row[17];
            $debito += $row[4] + $row[5] + $row[6] + $row[7] + $row[13];
        }

        $valor = $credito - $debito;
        return $valor;
    }

    function consultaValorServicio($numeroFactura, $idServicio) {

        $valorFactura = 0;

        $linkbd = conectar_v7();
        $sql = "SELECT SUM(credito), SUM(debito) FROM srvdetalles_facturacion WHERE numero_facturacion = $numeroFactura AND id_servicio = '$idServicio' AND tipo_movimiento = '101' AND id_tipo_cobro != 10";
        $res = mysqli_query($linkbd,$sql);
        $row = mysqli_fetch_row($res);

        if($row[0] != '') {
            $valorFactura = $row[0] - $row[1];
        }
        else {
            $valorFactura = 0;
        }


        return $valorFactura;
    }

    function consultaValorServicioSinDeuda($numeroFactura, $idServicio) {

        $valorFactura = 0;

        $linkbd = conectar_v7();
        $sql = "SELECT COALESCE(SUM(credito),0), COALESCE(SUM(debito),0) FROM srvdetalles_facturacion WHERE numero_facturacion = $numeroFactura AND id_servicio = '$idServicio' AND tipo_movimiento = '101' AND id_tipo_cobro IN (1,2,3,4,5,9)";
        $res = mysqli_query($linkbd,$sql);
        $row = mysqli_fetch_row($res);

        if($row[0] != '') {
            $valorFactura = $row[0] - $row[1];
        }
        else {
            $valorFactura = 0;
        }


        return $valorFactura;
    }

    function consultaValorInteres($numeroFactura, $idServicio) {

        $valorInteres = 0;

        $linkbd = conectar_v7();
        $sql = "SELECT credito FROM srvdetalles_facturacion WHERE numero_facturacion = $numeroFactura AND id_servicio = '$idServicio' AND tipo_movimiento = '101' AND id_tipo_cobro = 10";
        $res = mysqli_query($linkbd,$sql);
        $row = mysqli_fetch_row($res);

        if($row[0] != '') {
            $valorInteres = $row[0];
        }
        else {
            $valorInteres = 0;
        }

        return $valorInteres;
    }

    function buscaNombreBarrio($idBarrio)
    {
        $linkbd = conectar_v7();
        $linkbd -> set_charset("utf8");
        $nombreBarrio = 'Barrio no encontrado';

        $sqlBarrio = "SELECT nombre FROM srvbarrios WHERE id = $idBarrio";
        $rowBarrio = mysqli_fetch_row(mysqli_query($linkbd,$sqlBarrio));

        if($rowBarrio[0] != '')
        {
            $nombreBarrio = $rowBarrio[0];
        }

        return $nombreBarrio;
    }

    function consultaEstadoServicio($campo, $cliente, $servicio)
    {
        $linkbd = conectar_v7();
        $estado = 'No';

        $sqlAsignacionServicio = "SELECT $campo FROM srvasignacion_servicio WHERE id_clientes = $cliente AND id_servicio = $servicio";
        $rowAsignacionServicio = mysqli_fetch_row(mysqli_query($linkbd,$sqlAsignacionServicio));

        if($rowAsignacionServicio[0] == 'S')
        {
            $estado = 'Si';
        }

        return $estado;
    }

    function consultaIdClienteConFactura($numeroFactura)
    {
        $linkbd = conectar_v7();
        $idCliente = 0;

        $sql = "SELECT id_cliente FROM srvcortes_detalle WHERE numero_facturacion = $numeroFactura";
        $res = mysqli_query($linkbd,$sql);
        $row = mysqli_fetch_row($res);

        if($row[0] != '')
        {
            $idCliente = $row[0];
        }

        return $idCliente;
    }

    function consultaMedidor($cliente, $servicio)
    {
        $linkbd = conectar_v7();
        $medidor = 0;

        $sql = "SELECT id_medidor FROM srvasignacion_servicio WHERE id_clientes = $cliente AND id_servicio = $servicio";
        $res = mysqli_query($linkbd,$sql);
        $row = mysqli_fetch_row($res);

        if($row[0] != '')
        {
            $medidor = $row[0];
        }

        return $medidor;
    }

    function Quitar_Espacios($Frase)
    {
        return preg_replace("/\s+/", " ", trim($Frase));
    }

    function buscaZona($id)
    {
        $linkbd = conectar_v7();
        $zona = '';

        $sql = "SELECT nombre FROM srvzonas WHERE id = $id";
        $res = mysqli_query($linkbd,$sql);
        $row = mysqli_fetch_row($res);

        if($row[0] != '')
        {
            $zona = $row[0];
        }

        return $zona;
    }

    function buscaLado($id)
    {
        $linkbd = conectar_v7();
        $lado = '';

        $sql = "SELECT nombre FROM srvlados WHERE id = $id";
        $res = mysqli_query($linkbd,$sql);
        $row = mysqli_fetch_row($res);

        if($row[0] != '')
        {
            $lado = $row[0];
        }

        return $lado;
    }

    function buscaNombreRuta($id)
    {
        $linkbd = conectar_v7();
        $nombreRuta = '';

        $sql = "SELECT nombre FROM srvrutas WHERE id = $id";
        $res = mysqli_query($linkbd,$sql);
        $row = mysqli_fetch_row($res);

        if($row[0] != '')
        {
            $nombreRuta = $row[0];
        }

        return $nombreRuta;
    }

    function consultaNumeroFacturaConId($id, $corte)
    {
        $linkbd = conectar_v7();
        $codigoFactura = 0;

        $sql = "SELECT numero_facturacion FROM srvcortes_detalle WHERE id_cliente = $id AND id_corte = $corte";
        $res = mysqli_query($linkbd, $sql);
        $row = mysqli_fetch_row($res);

        if($row[0] != '')
        {
            $codigoFactura = $row[0];
        }

        return $codigoFactura;
    }
    function uploadFile(array $data, string $name){
        $url_temp = $data['tmp_name'];
        $destino = __DIR__.'/informacion/proyectos/temp/'.$name;
        move_uploaded_file($url_temp, $destino);
    }
    function deleteFile(string $name){
        unlink(__DIR__.'/informacion/proyectos/temp/'.$name);
    }
    function getRealIP(){
        if (isset($_SERVER["HTTP_CLIENT_IP"])){
            return $_SERVER["HTTP_CLIENT_IP"];
        }elseif (isset($_SERVER["HTTP_X_FORWARDED_FOR"])){
            return $_SERVER["HTTP_X_FORWARDED_FOR"];
        }elseif (isset($_SERVER["HTTP_X_FORWARDED"])){
            return $_SERVER["HTTP_X_FORWARDED"];
        }elseif (isset($_SERVER["HTTP_FORWARDED_FOR"])){
            return $_SERVER["HTTP_FORWARDED_FOR"];
        }elseif (isset($_SERVER["HTTP_FORWARDED"])){
            return $_SERVER["HTTP_FORWARDED"];
        }else{
            return $_SERVER["REMOTE_ADDR"];
        }
    }
    function getCuentaConcepto($tipo_concepto,$concepto,$tipo_cuenta,$modulo){
        $linkbd = conectar_v7();
        $cuenta = " debito = 'S' ";
        if($tipo_cuenta=="credito"){
            $cuenta = " credito = 'S' ";
        }
        $sql = "SELECT cuenta
                FROM conceptoscontables_det
                WHERE tipo = '$tipo_concepto' AND codigo = '$concepto' AND $cuenta AND estado = 'S' AND modulo='$modulo'";
        $request = mysqli_query($linkbd,$sql)->fetch_assoc()['cuenta'];
        return $request;
    }

    function strClean($strCadena){
        $string = preg_replace(['/\s+/','/^\s|\s$/'],[' ',''], $strCadena);
        $string = trim($string); //Elimina espacios en blanco al inicio y al final
        $string = stripslashes($string); // Elimina las \ invertidas
        //Elimina sentencias SQL y scripts
        $string = str_ireplace("<script>","",$string);
        $string = str_ireplace("</script>","",$string);
        $string = str_ireplace("<script src>","",$string);
        $string = str_ireplace("<script type=>","",$string);
        $string = str_ireplace("SELECT * FROM","",$string);
        $string = str_ireplace("DELETE FROM","",$string);
        $string = str_ireplace("INSERT INTO","",$string);
        $string = str_ireplace("SELECT COUNT(*) FROM","",$string);
        $string = str_ireplace("DROP TABLE","",$string);
        $string = str_ireplace("OR '1'='1","",$string);
        $string = str_ireplace('OR "1"="1"',"",$string);
        $string = str_ireplace('OR ´1´=´1´',"",$string);
        $string = str_ireplace("is NULL; --","",$string);
        $string = str_ireplace("is NULL; --","",$string);
        $string = str_ireplace("LIKE '","",$string);
        $string = str_ireplace('LIKE "',"",$string);
        $string = str_ireplace("LIKE ´","",$string);
        $string = str_ireplace("OR 'a'='a","",$string);
        $string = str_ireplace('OR "a"="a',"",$string);
        $string = str_ireplace("OR ´a´=´a","",$string);
        $string = str_ireplace("OR ´a´=´a","",$string);
        $string = str_ireplace("--","",$string);
        $string = str_ireplace("^","",$string);
        $string = str_ireplace("[","",$string);
        $string = str_ireplace("]","",$string);
        $string = str_ireplace("==","",$string);
        $string = str_ireplace("#","",$string);
        $string = str_ireplace("°","",$string);
        return $string;
    }
    function replaceChar(string $cadena){
        //Reemplazamos la A y a
        $cadena = str_replace(
        array('Á', 'À', 'Â', 'Ä', 'á', 'à', 'ä', 'â', 'ª'),
        array('A', 'A', 'A', 'A', 'a', 'a', 'a', 'a', 'a'),
        $cadena
        );

        //Reemplazamos la E y e
        $cadena = str_replace(
        array('É', 'È', 'Ê', 'Ë', 'é', 'è', 'ë', 'ê'),
        array('E', 'E', 'E', 'E', 'e', 'e', 'e', 'e'),
        $cadena );

        //Reemplazamos la I y i
        $cadena = str_replace(
        array('Í', 'Ì', 'Ï', 'Î', 'í', 'ì', 'ï', 'î'),
        array('I', 'I', 'I', 'I', 'i', 'i', 'i', 'i'),
        $cadena );

        //Reemplazamos la O y o
        $cadena = str_replace(
        array('Ó', 'Ò', 'Ö', 'Ô', 'ó', 'ò', 'ö', 'ô'),
        array('O', 'O', 'O', 'O', 'o', 'o', 'o', 'o'),
        $cadena );

        //Reemplazamos la U y u
        $cadena = str_replace(
        array('Ú', 'Ù', 'Û', 'Ü', 'ú', 'ù', 'ü', 'û'),
        array('U', 'U', 'U', 'U', 'u', 'u', 'u', 'u'),
        $cadena );

        //Reemplazamos la N, n, C y c
        $cadena = str_replace(
        array('Ñ', 'ñ', 'Ç', 'ç',',','.',';',':'),
        array('N', 'n', 'C', 'c','','','',''),
        $cadena
        );
        return $cadena;
    }
    function getIp(): string
    {
        if (isset($_SERVER['HTTP_CF_CONNECTING_IP'])) { // Soporte de Cloudflare
            $ip = $_SERVER['HTTP_CF_CONNECTING_IP'];
        } elseif (isset($_SERVER['DIRECCIÓN REMOTA']) === true) {
            $ip = $_SERVER['DIRECCIÓN REMOTA'];
            if (preg_match('/^(?:127|10)\.0\.0\.[12]?\d{1,2}$/', $ip)) {
                if (isset($_SERVER['HTTP_X_REAL_IP'])) {
                    $ip = $_SERVER['HTTP_X_REAL_IP'];
                } elseif (isset($_SERVER['HTTP_X_FORWARDED_FOR'])) {
                    $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
                }
            }
        } else {
            $ip = '127.0.0.1';
        }
        if (in_array($ip, ['::1', '0.0.0.0', 'localhost'], true)) {
            $ip = '127.0.0.1';
        }
        $filter = filter_var($ip, FILTER_VALIDATE_IP, FILTER_FLAG_IPV4);
        if ($filter === false) {
            $ip = '127.0.0.1';
        }

        return $ip;
    }
    function insertFuncionesAuditoria($tabla,$funciones){
        $linkbd = conectar_v7();
        $sql = "SELECT nombre FROM $tabla";
        $arrFunciones = mysqli_fetch_all(mysqli_query($linkbd,$sql),MYSQLI_ASSOC);
        $total = count($arrFunciones);
        $totalNew = count($funciones);
        if($totalNew > 0){
            if($total > 0){
                $arrFunciones = array_column($arrFunciones,"nombre");
                for ($j=0; $j < $totalNew ; $j++) {
                    $strFuncion = ucwords(strtolower(replaceChar($funciones[$j])));
                    if(!in_array($strFuncion,$arrFunciones)){
                        $sql = "INSERT INTO $tabla(nombre) VALUES('$strFuncion')";
                         mysqli_query($linkbd,$sql);
                    }
                }
            }else{
                $strFuncion = ucwords(strtolower(replaceChar($funciones[0])));
                $sql = "INSERT INTO $tabla(nombre) VALUES('$strFuncion')";
                mysqli_query($linkbd,$sql);
            }
        }
    }
    function insertAuditoria(string $tabla,string $nombreIdFuncion,int $intFuncion,string $strAccion,string $strConsecutivo){
        $linkbd = conectar_v7();
        $strAccion = strClean(ucwords(strtolower($strAccion)));
        $strUsuario = $_SESSION['usuario'];
        $strCedula = $_SESSION['cedulausu'];
        $strIp = getRealIP();
        $sql = "INSERT INTO $tabla($nombreIdFuncion,accion,cedula,usuario,ip,fecha_hora,consecutivo)
        VALUES('$intFuncion','$strAccion','$strCedula','$strUsuario','$strIp',NOW(),'$strConsecutivo')";
        mysqli_query($linkbd,$sql);
    }
    function searchValueFacturaTotal($numFactura) {
        $linkbd = conectar_v7();
        $valorFactura = 0;

        $sqlValue = "SELECT SUM(credito) AS valueCredito, SUM(debito) AS valueDebito FROM srvdetalles_facturacion WHERE numero_facturacion = $numFactura AND tipo_movimiento = '101'";
        $rowValue = mysqli_fetch_assoc(mysqli_query($linkbd, $sqlValue));

        $valorFactura = $rowValue["valueCredito"] - $rowValue["valueDebito"];
        $valorFactura = ($valorFactura / 100);
        $valorFactura = ceil($valorFactura);
        $valorFactura = $valorFactura * 100;

        return $valorFactura;
    }
    function searchValueFacturaSinAproximacion($numFactura) {
        $linkbd = conectar_v7();
        $valorFactura = 0;

        $sqlValue = "SELECT SUM(credito) AS valueCredito, SUM(debito) AS valueDebito FROM srvdetalles_facturacion WHERE numero_facturacion = $numFactura AND tipo_movimiento = '101'";
        $rowValue = mysqli_fetch_assoc(mysqli_query($linkbd, $sqlValue));
        $valorFactura = $rowValue["valueCredito"] - $rowValue["valueDebito"];
        $valorFactura = bcdiv($valorFactura, '1', 2);
        return $valorFactura;
    }
    function searchValueIntereses($numFactura) {

        $linkbd = conectar_v7();
        $valorIntereses = 0;

        $sqlValue = "SELECT SUM(credito) AS valueCredito FROM srvdetalles_facturacion WHERE numero_facturacion = $numFactura AND tipo_movimiento = '101' AND id_tipo_cobro = 10";
        $rowValue = mysqli_fetch_assoc(mysqli_query($linkbd, $sqlValue));
        $valorIntereses = $rowValue["valueCredito"];

        return $valorIntereses;
    }
    function searchValueInteresServ($numFactura, $serviceId) {

        $linkbd = conectar_v7();
        $valorIntereses = 0;

        $sqlValue = "SELECT SUM(credito) AS valueCredito FROM srvdetalles_facturacion WHERE numero_facturacion = $numFactura AND id_servicio = $serviceId AND tipo_movimiento = '101' AND id_tipo_cobro = 10";
        $rowValue = mysqli_fetch_assoc(mysqli_query($linkbd, $sqlValue));
        $valorIntereses = $rowValue["valueCredito"];

        return $valorIntereses;
    }
?>
