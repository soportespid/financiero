<?php
	header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=utf8");

	require "comun.inc";
	require "funciones.inc";
	require "conversor.php";
	require "validaciones.inc";

	$linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");

	session_start();
	date_default_timezone_set("America/Bogota");
?>
<!DOCTYPE >
<html lang="es">
	<head>
		<meta charset="utf-8"/>
		<meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1"/>
		<meta name="viewport" content="user-scalable=no">
		<title>:: IDEAL 10 - Contabilidad</title>
		<link href="favicon.ico" rel="shortcut icon"/>
		<link href="css/css2z.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<script type="text/javascript" src="jquery-1.11.0.min.js"></script> 
		<script type="text/javascript" src="css/programas.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script>
			function verUltimaPos(idcta, filas, filtro){
				var scrtop=$('#divdet').scrollTop();
				var altura=$('#divdet').height();
				var numpag=$('#nummul').val();
				var limreg=$('#numres').val();
				if((numpag<=0)||(numpag==""))
					numpag=0;
				if((limreg==0)||(limreg==""))
					limreg=10;
				numpag++;
				location.href="cont-concecontablesivaeditar.php?is="+idcta+"&scrtop="+scrtop+"&totreg="+filas+"&altura="+altura+"&numpag="+numpag+"&limreg="+limreg+"&filtro="+filtro;
			}
		</script>
		<?php
		$scrtop=$_GET['scrtop'];
		if($scrtop=="") $scrtop=0;
		echo"<script>
			window.onload=function(){
				$('#divdet').scrollTop(".$scrtop.")
			}
		</script>";
		$gidcta=$_GET['idcta'];
		if(isset($_GET['filtro']))
			$_POST['nombre']=$_GET['filtro'];
		?>
	</head>
	<body>
		<IFRAME src="alertas.php" name="alertas" id="alertas" style="display:none"></IFRAME>
		<span id="todastablas2"></span>
		<table>
			<tr><script>barra_imagenes("cont");</script><?php cuadro_titulos();?></tr>	 
			<tr><?php menu_desplegable("cont");?></tr>
			<tr>
				<td colspan="3" class="cinta">
					<a href="cont-concecontablesiva.php" class="mgbt"><img src="imagenes/add.png" title="Nuevo" /></a>
					<a class="mgbt"><img src="imagenes/guardad.png"/></a>
					<a class="mgbt"><img src="imagenes/buscad.png"/></a>
					<a onClick="mypop=window.open('plan-agenda.php','','');mypop.focus()" class="mgbt"><img src="imagenes/agenda1.png" title="Agenda" /></a>
					<a href="#" onClick="mypop=window.open('cont-principal.php','','');mypop.focus();" class="mgbt"><img src="imagenes/nv.png" title="Nueva ventana"></a>
					<a onClick="mypop=window.open('<?php echo $url2; ?>','','');mypop.focus();" class="tooltip bottom mgbt"><img src="imagenes/duplicar_pantalla.png" title="Duplicar pesta&ntilde;a"></a>
					<a href="teso-concecontables.php" class="mgbt"><img src="imagenes/iratras.png" title="Atr&aacute;s"></a>
				</td>
			</tr>	
		</table>
		<?php
		if($_GET['numpag']!=""){
			$oculto=$_POST['oculto'];
			if($oculto!=2){
				$_POST['numres']=$_GET['limreg'];
				$_POST['numpos']=$_GET['limreg']*($_GET['numpag']-1);
				$_POST['nummul']=$_GET['numpag']-1;
			}
		}
		else{
			if($_POST['nummul']==""){
				$_POST['numres']=10;
				$_POST['numpos']=0;
				$_POST['nummul']=0;
			}
		}
		?>
		<form name="form2" method="post" action="cont-concecontablesivabuscar.php">
			<input type="hidden" name="numres" id="numres" value="<?php echo $_POST['numres']; ?>"/>
			<input type="hidden" name="numpos" id="numpos" value="<?php echo $_POST['numpos']; ?>"/>
			<input type="hidden" name="nummul" id="nummul" value="<?php echo $_POST['nummul']; ?>"/>
			<input type="hidden" name="numtop" id="numtop" value="<?php echo $_POST['numtop'];?>"/>
			<table class="inicio">
				<tr>
					<td class="titulos" colspan="2">:: Buscar Concepto Contable IVA</td>
					<td style="width:7%" class="cerrar" ><a href="cont-principal.php">Cerrar</a></td>
				</tr>
				<tr>
					<td style="width:3.5cm" class="saludo1">:. Codigo o Nombre:</td>
					<td style="width:80%">
						<input name="nombre" type="text" style="width:50%" value="<?php echo $_POST['nombre']; ?>">
						<input name="oculto" id="oculto" type="hidden" value="1"> 
					</td>
					<td style="width:7%"></td>
				</tr>                       
			</table>   
			<div class="subpantalla" style="height:66.5%; width:99.6%; overflow-x:hidden;">
				<?php
				$oculto = $_POST['oculto'];
				$cond2 = "";
				if ($_POST['nombre']!=""){
					$crit1="and concat_ws(' ', codigo, nombre) LIKE '%".$_POST['nombre']."%'";
				}else{
					$crit1 = "";
				}

				if ($_POST['numres']!="-1"){ 
					$cond2 = "LIMIT $_POST[numpos], $_POST[numres]"; 
				}
				//sacar el consecutivo 
				$sqlr = "SELECT * from conceptoscontables WHERE tipo = 'IV' AND modulo = '4' $crit1";
				$resp = mysqli_query($linkbd, $sqlr);
				$ntr = mysqli_num_rows($resp);
				$_POST['numtop'] = $ntr;
				$nuncilumnas = ceil($_POST['numtop']/$_POST['numres']);
				$sqlr = "SELECT * FROM conceptoscontables WHERE tipo = 'IV' AND modulo = '4' $crit1 ORDER BY codigo $cond2";
				$resp = mysqli_query($linkbd, $sqlr);			
				$co = 'saludo1a';
				$co2 = 'saludo2';	
				$numcontrol = $_POST['nummul']+1;
				$i = ($_POST['nummul']*$_POST['numres'])+1;
				if(($nuncilumnas==$numcontrol)||($_POST['numres']=="-1")){
					$imagenforward="<img src='imagenes/forward02.png' style='width:17px'>";
					$imagensforward="<img src='imagenes/skip_forward02.png' style='width:16px' >";
				}
				else{
					$imagenforward="<img src='imagenes/forward01.png' style='width:17px' title='Siguiente' onClick='numsiguiente()'>";
					$imagensforward="<img src='imagenes/skip_forward01.png' style='width:16px' title='Fin' onClick='saltocol(\"$nuncilumnas\")'>";
				}
				if(($_POST['numpos']==0)||($_POST['numres']=="-1")){
					$imagenback="<img src='imagenes/back02.png' style='width:17px'>";
					$imagensback="<img src='imagenes/skip_back02.png' style='width:16px'>";
				}
				else{
					$imagenback="<img src='imagenes/back01.png' style='width:17px' title='Anterior' onClick='numanterior();'>";
					$imagensback="<img src='imagenes/skip_back01.png' style='width:16px' title='Inicio' onClick='saltocol(\"1\")'>";
				}

				echo "
				<table class='inicio' align='center'>
					<tr>
						<td colspan='3' class='titulos'>.: Resultados Busqueda:</td>
						<td class='submenu'>
							<select name='renumres' id='renumres' onChange='cambionum();' style='width:100%'>
								<option value='10'"; if ($_POST['renumres']=='10'){echo "selected";} echo ">10</option>
								<option value='20'"; if ($_POST['renumres']=='20'){echo "selected";} echo ">20</option>
								<option value='30'"; if ($_POST['renumres']=='30'){echo "selected";} echo ">30</option>
								<option value='50'"; if ($_POST['renumres']=='50'){echo "selected";} echo ">50</option>
								<option value='100'"; if ($_POST['renumres']=='100'){echo "selected";} echo ">100</option>
								<option value='-1'"; if ($_POST['renumres']=='-1'){echo "selected";} echo ">Todos</option>
							</select>
						</td>
					</tr>
				<tr>
					<td colspan='3'>Concepto Contable IVA: $ntr</td>
				</tr>
				<tr>
					<td width='5%' class='titulos2'>Fecha</td>
					<td width='5%' class='titulos2'>Codigo</td>
					<td class='titulos2'>Nombre</td>
					<td class='titulos2' width='5%'>Editar</td>
				</tr>";	
				$con = 1;
				$iter = 'saludo1a';
				$iter2 = 'saludo2';
				$filas = 1;
				while ($row =mysqli_fetch_row($resp)){
					$sq = "SELECT fechainicial FROM conceptoscontables_det WHERE codigo = '$row[0]' AND modulo = '$row[2]' AND tipo = '$row[3]'";
					$rs = mysqli_query($linkbd, $sq);
					$rw = mysqli_fetch_row($rs);
					if($gidcta != ""){
						if($gidcta == $row[0]){
							$estilo = 'background-color: yellow;';
						}
						else{
							$estilo = "";
						}
					}
					else{
						$estilo = "";
					}	
					$idcta = "'".$row[0]."'";
					$numfil = "'".$filas."'";
					$filtro = "'".$_POST['nombre']."'";
					echo"<tr class='$iter' onDblClick=\"verUltimaPos($idcta, $numfil, $filtro)\" style='text-transform:uppercase; $estilo' >
					<td>$rw[0]</td>
					<td>".strtoupper($row[0])."</td>
					<td>".strtoupper($row[1])."</td>";
					echo"<td style='text-align:center;'>
							<a onClick=\"verUltimaPos($idcta, $numfil, $filtro)\" style='cursor:pointer;'>
								<img src='imagenes/b_edit.png' style='width:18px' title='Editar'>
							</a>
						</td>
					</tr>";
					$con+=1;
					$aux = $iter;
					$iter = $iter2;
					$iter2 = $aux;
					$filas++;
				}
				echo"</table>";
					echo"<table class='inicio'>
						<tr>
							<td style='text-align:center;'>
								<a href='#'>$imagensback</a>&nbsp;
								<a href='#'>$imagenback</a>&nbsp;&nbsp;";
								if($nuncilumnas<=9){$numfin=$nuncilumnas;}
								else{$numfin=9;}
								for($xx = 1; $xx <= $numfin; $xx++){
									if($numcontrol<=9){$numx=$xx;}
									else{$numx=$xx+($numcontrol-9);}
									if($numcontrol==$numx){echo"<a href='#' onClick='saltocol(\"$numx\")'; style='color:#24D915'> $numx </a>";}
									else {echo"<a href='#' onClick='saltocol(\"$numx\")'; style='color:#000000'> $numx </a>";}
								}
								echo "&nbsp;&nbsp;<a href='#'>$imagenforward</a>
									&nbsp;<a href='#'>$imagensforward</a>
							</td>
						</tr>
					</table>";
				?>
				</div>
		</form>
	</body>
</html>
