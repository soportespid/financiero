<?php
    header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=utf8");

	require 'comun.inc';
	require 'funciones.inc';

	session_start();
    date_default_timezone_set("America/Bogota");

?>
<!DOCTYPE >
<html lang="es">
	<head>
		<meta charset="utf-8"/>
		<meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1"/>
		<meta name="viewport" content="user-scalable=no">
		<title>:: IDEAL 10 - Activos fijos</title>
		<link href="favicon.ico" rel="shortcut icon"/>
        
		<link href="css/css2.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<link href="css/css2z.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
        <link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
        <link href="css/css4.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
        <script type="text/javascript" src="css/programas.js"></script>
        <script type="text/javascript" src="css/calendario.js"></script>
        <script type="text/javascript" src="JQuery/jquery-2.1.4.min.js"></script>

		<style>
			.inicio--no-shadow{
				box-shadow: none;
			}

			[v-cloak]{
				display : none;
			}
		</style>
    </head>
    <body>
        <header>
			<table>
				<tr><script>barra_imagenes("acti");</script><?php cuadro_titulos();?></tr>
			</table>
		</header>

        <section id="myapp" v-cloak>
            <form name="form2" method="post" action="">
                <nav>
					<table>
						<tr><?php menu_desplegable("acti");?></tr>
						<tr>
							<td colspan="3" class="cinta">
								<img src="imagenes/add.png" v-on:click="location.href='acti-ordenActivacion'" class="mgbt" title="Nuevo">
								<img src="imagenes/guarda.png" v-on:click="" title="Guardar"  class="mgbt">
								<img src="imagenes/busca.png" v-on:click="location.href='acti-buscarOrdenesActivacion'" class="mgbt" title="Buscar">
								<img src="imagenes/nv.png" onClick="mypop=window.open('acti-principal','',''); mypop.focus();" class="mgbt" title="Nueva Ventana">
                                <a href="acti-gestiondelosactivos"><img src="imagenes/iratras.png" class="mgbt" alt="Atrás"></a>
							</td>
						</tr>
					</table>
				</nav>

                <article>
                    <table class="inicio">
                        <tr>    
                            <td class="titulos" colspan="10" >Ordenes de activación creadas:</td>
                            <td class="cerrar" style="width:7%" onClick="location.href='acti-principal.php'">Cerrar</td>
                        </tr>

                        <tr>
                            <td class="textonew01" style="width:3.5cm;">.: Código orden:</td>
                            <td>
                                <input type="text" v-model="codigoOrden">
                            </td>

                            <td class="textonew01" style="width:3.5cm;">.: Placa:</td>
                            <td>
                                <input type="text" v-model="placa">
                            </td>

                            <td class="textonew01" style="width:3.5cm;">.: Fecha inicial:</td>
                            <td>
                                <input type="text" name="fechaIni"  value="<?php echo $_POST['fechaIni']?>" onKeyUp="return tabular(event,this)" id="fechaIni" title="DD/MM/YYYY" onDblClick="displayCalendarFor('fechaIni');" class="colordobleclik" autocomplete="off" onChange=""  readonly>
                            </td>

                            <td class="textonew01" style="width:3.5cm;">.: Fecha final:</td>
                            <td>
                                <input type="text" name="fechaFin" value="<?php echo $_POST['fechaFin']?>" onKeyUp="return tabular(event,this)" id="fechaFin" title="DD/MM/YYYY" onDblClick="displayCalendarFor('fechaFin');" class="colordobleclik" autocomplete="off" onChange=""  readonly>
                            </td>

                            <td>
                                <input type="button" class="btn btn-primary" value="Buscar activos" @click="buscarDatos">
                            </td>
                        </tr>
                    </table>

                    <div class='subpantalla' style='height:54vh; width:100%; margin-top:0px;'>
                        <table class=''>        
                            <thead>
                                <tr>
                                    <th class="titulosnew00" width="5%">Código orden</th>
                                    <th class="titulosnew00" width="7%">Fecha registro</th>
                                    <th class="titulosnew00" width="10%">Origen</th>
                                    <th class="titulosnew00" width="10%">Placa</th>
                                    <th class="titulosnew00">Nombre</th>
                                    <th class="titulosnew00" width="10%">Clase</th>
                                    <th class="titulosnew00" width="10%">Grupo</th>
                                    <th class="titulosnew00" width="10%">Tipo</th>
                                    <th class="titulosnew00" width="10%">Dependencia</th>
                                    <th class="titulosnew00" width="10%">Valor</th>
                                    <th class="titulosnew00" width="5%">Estado</th>
                                </tr>
                            </thead>

                            <tbody>
                                <tr v-for="(activo,index) in activos" v-on:dblclick="visualizar(activo)" v-bind:class="(index % 2 ? 'saludo1a' : 'saludo2')" style="font: 100% sans-serif;" >
                                    <td style="text-align:center; width: 5%;">{{ activo[0] }}</td>
                                    <td style="text-align:center; width: 7%;">{{ activo[1] }}</td>
                                    <td style="text-align:center; width: 10%;">{{ activo[2] }}</td>
                                    <td style="text-align:center; width: 10%;">{{ activo[3] }}</td>
                                    <td style="text-align:left;">{{ activo[4] }}</td>
                                    <td style="text-align:left; width: 10%;">{{ activo[5] }}</td>
                                    <td style="text-align:left; width: 10%;">{{ activo[6] }}</td>
                                    <td style="text-align:left; width: 10%;">{{ activo[7] }}</td>
                                    <td style="text-align:center; width: 10%;">{{ activo[11] }}</td>
                                    <td style="text-align:right; width: 10%;">{{ formatonumero(activo[8]) }}</td>
                                    <td style="text-align:center; width: 5%;"><img v-bind:src="activo[9]" v-bind:title="activo[10]" width="40px" height="40px"></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </article>
            </form>		

            <div id="cargando" v-if="loading" class="loading">
                <span>Cargando...</span>
            </div>

        </section>	

        <script src="node_modules/read-excel-file/bundle/read-excel-file.min.js"></script>
		<script src="node_modules/xlsx/dist/xlsx.full.min.js"></script>
        <script src="Librerias/vue/vue.min.js"></script>
		<script src="Librerias/vue/axios.min.js"></script>
		<script type="module" src="activos_fijos/ordenActivacion/buscar/acti-buscarOrdenesActivacion.js"></script>
	</body>
</html>