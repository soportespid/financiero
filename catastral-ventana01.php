<?php
	header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=utf8");
	require "comun.inc";
	require "funciones.inc";
	session_start();
	$linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");
	date_default_timezone_set("America/Bogota");
?>
<!DOCTYPE >
<html lang="es">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1">
		<meta name="viewport" content="user-scalable=no">
		<title>:: IDEAL 10</title>
		<link href="favicon.ico" rel="shortcut icon"/>
		<link href="css/css2z.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet"/>
		<link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet"/>
		<script type="text/javascript" src="css/programas.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script type="text/javascript" src="css/calendario.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script>
			var anterior;
			function ponprefijo(pref,pref2,pref3){ 
				parent.document.form2.codcat.value =pref;
				parent.document.form2.ord.value =pref2;
				parent.document.form2.tot.value =pref3;
				parent.document.form2.codcat.focus();	
				parent.despliegamodal2("hidden");
			}
		</script> 
	</head>
	<body >
		<form action="" method="post" enctype="multipart/form-data" name="form1">
			<table  class="inicio" align="center" >
				<tr>
					<td class="titulos" colspan="4">:: Buscar Codigo Catastral</td>
					<td class="cerrar"><a onClick="parent.despliegamodal2('hidden');">&nbsp;Cerrar</a></td>
				</tr>
				<tr>
					<td class="saludo1">Nombres:</td>
					<td><input name="nombre" type="text" value="" size="20"></td>
					<td class="saludo1">Documento Propietario:</td>
					<td><input name="cedula" type="text" value="" size="20"></td>
				</tr>
				<tr>
					<td class="saludo1">Cod Catastral:</td>
					<td><input name="documento" type="text" id="documento" value="" size="30"></td>
					<td class="saludo1">Direccion:</td>
					<td><input name="direccion" type="text" value="" size="30">&nbsp;<input type="submit" name="Submit" value="Buscar" ></td>
				</tr>  
				<input type="hidden" name="oculto" id="oculto" value="1"/>                     
			</table> 
			<div class="subpantalla" style="height:78.5%; width:99.6%; overflow-x:hidden;">
				<?php
				
					function obtenerNombre($codigo){
						$linkbd = conectar_v7();
						$linkbd -> set_charset("utf8");
						$sql="SELECT nombre FROM teso_clasificapredios WHERE codigo=$codigo LIMIT 0,1";
						$res=mysqli_query($linkbd,$sql);
						$row = mysqli_fetch_row($res);
						return $row[0];
					}
					$oculto=$_POST['oculto'];
					if(true){
						$crit1=" ";
						$crit2=" ";
						if ($_POST['nombre']!=""){$crit1=" and (tesopredios.nombrepropietario  like '%".$_POST['nombre']."%') ";}
						if ($_POST['documento']!=""){$crit2=" and tesopredios.cedulacatastral like '%".$_POST['documento']."%' ";}
						if ($_POST['direccion']!=""){$crit3=" and (tesopredios.direccion  like '%".$_POST['direccion']."%') ";}
						if ($_POST['cedula']!=""){$crit4=" and (tesopredios.documento  like '%".$_POST['cedula']."%') ";}
						if($_POST['nombre']!="" || $_POST['documento']!="" || $_POST['direccion']!="" || $_POST['cedula']!=""){
							$sqlr="select *from tesopredios where tesopredios.estado='S' $crit1 $crit2 $crit3 $crit4 order by tesopredios.cedulacatastral, tesopredios.nombrepropietario, tesopredios.cedulacatastral ";
							$resp = mysqli_query($linkbd,$sqlr);
						
						
							$ntr = mysqli_num_rows($resp);
							$con=1;
							echo "
							<table class='inicio' align='center' width='99%'>
								<tr><td colspan='10' class='titulos'>.: Resultados Busqueda:</td></tr>
								<tr><td colspan='8'>Terceros Encontrados: $ntr</td></tr>
								<tr>
									<td class='titulos2' width='2%'>Item</td>
									<td class='titulos2' width='30%'>CODIGO CATASTRAL</td>
									<td class='titulos2' >ORD</td>
									<td class='titulos2' >TOT</td>
									<td class='titulos2' >PROPIETARIO</td>
									<td class='titulos2' width='20%'>DOCUMENTO</td>
									<td class='titulos2' width='20%'>DIRECCION</td>
									<td class='titulos2' width='20%'>TIPO PREDIO</td>
								</tr>";	
							$iter='zebra1';
							$iter2='zebra2';
							while ($row =mysqli_fetch_row($resp)){
								echo"
								<tr onClick=\"javascript:ponprefijo('$row[0]','$row[1]','$row[2]')\" class='$iter' style='text-transform:uppercase'>
									<td>$con</td>
									<td>$row[0]</td>
									<td>$row[1]</td>
									<td>$row[2]</td>
									<td>$row[6]</td>
									<td>$row[5]</td>
									<td>$row[7]</td>
									<td>".obtenerNombre($row[15])."</td>
								</tr>";
								$con+=1;
								$aux=$iter;
								$iter=$iter2;
								$iter2=$aux;
							}
							echo"</table>"; 
						}
					}
				?>
			</div>
		</form>
	</body>
</html> 
