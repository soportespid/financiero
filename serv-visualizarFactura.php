<?php
	header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=utf8");
	require "comun.inc";
	require "funciones.inc";
    require "funcionesSP.inc.php";
	session_start();
	$linkbd = conectar_v7();
	cargarcodigopag($_GET['codpag'],$_SESSION["nivel"]);
	date_default_timezone_set("America/Bogota");
	$scroll=$_GET['scrtop'];
	$totreg=$_GET['totreg'];
	$idcta=$_GET['idcta'];
	$altura=$_GET['altura'];
	$filtro="'".$_GET['filtro']."'";
?>
<!DOCTYPE >
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="es">
	<head>
		<meta http-equiv="Content-type" content="text/html; charset=utf8"/>
		<meta http-equiv="X-UA-Compatible" content="IE=9"/>
		<title>:: IDEAL 10 - Servicios P&uacute;blicos</title>
		<link href="css/css2.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<link href="css/css4.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
        <link href="css/cssSP.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<script type="text/javascript" src="css/calendario.js"></script>
		<script type="text/javascript" src="css/programas.js"></script>

		<script>	
            <?php
				$sqlfactura = "SELECT nombre FROM srvarchivospdf WHERE id='1' ";
				$resfactura = mysqli_query($linkbd,$sqlfactura);
				$rowfactura = mysqli_fetch_row($resfactura);
				echo "
				function pdf()
				{
					document.form2.action='".$rowfactura[0]."';
					document.form2.target='_BLANK';
					document.form2.submit(); 
					document.form2.action='';
					document.form2.target='';
				}";
			?>                                                                                                                                                                   
		</script>

		<?php titlepag();?>

	</head>

	<body>
		<IFRAME src="alertas.php" name="alertas" id="alertas" style="display:none"></IFRAME>
		<span id="todastablas2"></span>
		<table>
			<tr><script>barra_imagenes("serv");</script><?php cuadro_titulos();?></tr>
			<tr><?php menu_desplegable("serv");?></tr>
			<tr>
				<td colspan="3" class="cinta">
                    <a class="mgbt"><img src="imagenes/add2.png"/></a>
					<a class="mgbt"><img src="imagenes/guardad.png" title="Guardar"/></a>
					<a href="serv-buscarLiquidacion.php" class="mgbt"><img src="imagenes/busca.png" title="Buscar"/></a>
					<a onclick="mypop=window.open('plan-agenda.php','','');mypop.focus()" class="mgbt"><img src="imagenes/agenda1.png" title="Agenda" /></a>
					<a onclick="mypop=window.open('serv-principal.php','','');mypop.focus();" class="mgbt"><img src="imagenes/nv.png" title="Nueva ventana"></a>
					<a onclick="mypop=window.open('<?php echo $url2; ?>','','');mypop.focus();" class="mgbt"><img src="imagenes/duplicar_pantalla.png" title="Duplicar pesta&ntilde;a"></a>
                    <a onClick="pdf()" class="mgbt"><img src="imagenes/print.png" style="width:29px;height:25px;" title="Imprimir" /></a>
					<a onclick="" class="mgbt"><img src="imagenes/iratras.png" title="Atr&aacute;s"></a>
                </td>
			</tr>
		</table>
        
		<div id="bgventanamodalm" class="bgventanamodalm">
			<div id="ventanamodalm" class="ventanamodalm">
				<IFRAME src="" name="ventanam" marginWidth=0 marginHeight=0 frameBorder=0 id="ventanam" frameSpacing=0 style=" width:700px; height:130px; top:200; overflow:hidden;"></IFRAME>
			</div>
		</div>

		<form name="form2" method="post" action="">
			<?php
                $idCliente = $_GET['cliente'];
                $numeroFactura = $_GET['factura'];

				if(@$_POST['oculto'] == "")
				{
					$sqlCliente = "SELECT id_tercero, cod_usuario, cod_catastral, id_estrato, zona_uso, id_ruta, codigo_ruta FROM srvclientes WHERE id = '$idCliente'";
                    $rowCliente = mysqli_fetch_row(mysqli_query($linkbd,$sqlCliente));

                    $sqlEstrato = "SELECT descripcion, tipo FROM srvestratos WHERE id = $rowCliente[3]";
                    $rowEstrato = mysqli_fetch_row(mysqli_query($linkbd,$sqlEstrato));

                    $sqlTipoUso = "SELECT nombre FROM srvtipo_uso WHERE id = $rowEstrato[1]";
                    $rowTipoUso = mysqli_fetch_row(mysqli_query($linkbd,$sqlTipoUso));

                    $sqlZonaUso = "SELECT nombre FROM srvusozona WHERE id = $rowCliente[4]";
                    $rowZonaUso = mysqli_fetch_row(mysqli_query($linkbd,$sqlZonaUso));

                    $sqlDireccion = "SELECT direccion FROM srvdireccion_cliente WHERE id = $idCliente";
                    $rowDireccion = mysqli_fetch_row(mysqli_query($linkbd,$sqlDireccion));

                    $sqlRuta = "SELECT nombre FROM srvrutas WHERE id = $rowCliente[5]";
                    $rowRuta = mysqli_fetch_row(mysqli_query($linkbd,$sqlRuta));

                    $sqlCorteDet = "SELECT id_corte, estado_pago FROM srvcortes_detalle WHERE numero_facturacion = $numeroFactura";
                    $rowCorteDet = mysqli_fetch_row(mysqli_query($linkbd,$sqlCorteDet));

                    $sqlCorte = "SELECT fecha_inicial, fecha_final, fecha_limite_pago, fecha_impresion FROM srvcortes WHERE numero_corte = $rowCorteDet[0]";
                    $rowCorte = mysqli_fetch_row(mysqli_query($linkbd,$sqlCorte));

                    $_POST['fechaImpresion'] = $rowCorte[3];
                    $_POST['documentoSuscriptor'] = buscaDocumentoTerceroConId($rowCliente[0]);
                    $_POST['nombreSuscriptor'] = buscaNombreTerceroConId($rowCliente[0]);
                    $_POST['codUsuario'] = $rowCliente[1];
                    $_POST['codCatastral'] = $rowEstrato[0];
                    $_POST['estrato'] = $rowEstrato[0];
                    $_POST['tipoUso'] = $rowTipoUso[0];
                    $_POST['zonaUso'] = $rowZonaUso[0];
                    $_POST['direccion'] = $rowDireccion[0];
                    $_POST['ruta'] = $rowRuta[0];
                    $_POST['codigoRuta'] = $rowCliente[6];
                    $_POST['numeroFactura'] = $numeroFactura;
                    $_POST['fechaInicial'] = $rowCorte[0];
                    $_POST['fechaFinal'] = $rowCorte[1];
                    $_POST['fechaLimite'] = $rowCorte[2];
                    switch ($rowCorteDet[1]) {
                        case 'S':
                            $_POST['estadoPago'] = "PENDIENTE";
                            break;
                        
                        case 'P':
                            $_POST['estadoPago'] = "PAGO";
                            break;                            

                        case 'V':
                            $_POST['estadoPago'] = "VENCIDO";
                            break;

                        case 'R':
                            $_POST['estadoPago'] = "REVERSADO";
                            break;

                        case 'A':
                            $_POST['estadoPago'] = "ACUERDO PAGO";
                            break;

                        default:
                            $_POST['estadoPago'] = "NO APLICA";
                            break;
                    }

                    $sqlFacturas = "SELECT cargo_f, consumo_b, consumo_c, consumo_s, subsidio_cf, subsidio_cb, subsidio_cc, subsidio_cs, contribucion_cf, contribucion_cb, contribucion_cc, contribucion_cs, deuda_anterior, abonos, acuerdo_pago, venta_medidor, interes_mora, alumbrado, estado, id_servicio FROM srvfacturas WHERE num_factura = $numeroFactura AND estado = 'ACTIVO'";
                    $resFacturas = mysqli_query($linkbd,$sqlFacturas);
                    while($rowFacturas = mysqli_fetch_row($resFacturas))
                    {
                        $cf = number_format($rowFacturas[0], 2, '.', '');
                        $cb = number_format($rowFacturas[1], 2, '.', '');
                        $cc = number_format($rowFacturas[2], 2, '.', '');
                        $cs = number_format($rowFacturas[3], 2, '.', '');
                        $sb_cf = number_format($rowFacturas[4], 2, '.', '');
                        $sb_cb = number_format($rowFacturas[5], 2, '.', '');
                        $sb_cc = number_format($rowFacturas[6], 2, '.', '');
                        $sb_cs = number_format($rowFacturas[7], 2, '.', '');
                        $cb_cf = number_format($rowFacturas[8], 2, '.', '');
                        $cb_cb = $rowFacturas[9] + $rowFacturas[10] + $rowFacturas[11];
                        $cb_cb = number_format($cb_cb, 2, '.', '');
                        $deuda_anterior = number_format($rowFacturas[12], 2, '.', '');
                        $abonos = number_format($rowFacturas[13], 2, '.', '');
                        $acuero_p = number_format($rowFacturas[14], 2, '.', '');
                        $venta_m = number_format($rowFacturas[15], 2, '.', '');
                        $interes_m = number_format($rowFacturas[16], 2, '.', '');
                        $alumbrado = number_format($rowFacturas[17], 2, '.', '');

                        $subTotal = $cf + $cb + $cc + $cs + $cb_cf + $cb_cb + $cb_cc + $cb_cs + $deuda_anterior + $acuero_p + $venta_m + $interes_m + $alumbrado - $sb_cf - $sb_cb - $sb_cc - $sb_cs - $abonos;
                        $subTotal = ($subTotal / 100);
                        $subTotal = ceil($subTotal);
                        $subTotal = $subTotal * 100;	
                        $total += $subTotal;
                        $total = number_format($total, 2, '.', '');
                    }

                    $_POST['valorTotal'] = $total;
				}
			?>
            <div>
                <table class="inicio grande">
                    <tr>
                        <td class="titulos" colspan="10">.: Datos de Usuario</td>
                    </tr>
                    
                    <tr>
                        <td class="tamano01" style="width: 10%;">Documento Usuario</td>
                        <td style="width: 9%;">
                            <input type="text" name="documentoSuscriptor" id="documentoSuscriptor" value="<?php echo @ $_POST['documentoSuscriptor'];?>" style="text-align:center;" readonly/>
                        </td>

                        <td class="tamano01" style="width: 10%;">Nombre Suscriptor</td>
                        <td style="width: 9%;">
                            <input type="text" name="nombreSuscriptor" id="nombreSuscriptor" value="<?php echo @ $_POST['nombreSuscriptor'];?>" style="width: 309%;" readonly/>
                        </td>

                        <td style="width: 28%;" colspan="2"></td>

                        <td rowspan="10">
                            <img src="imagenes/usuario-factura.png" height="100px" alt="Usuario">
                        </td>
                    </tr>

                    <tr>
                        <td class="tamano01" style="width: 10%;">Código Usuario</td>
                        <td>
                            <input type="text" name="codUsuario" id="codUsuario" value="<?php echo @ $_POST['codUsuario'];?>" style="text-align:center;" readonly/>
                        </td>

                        <td class="tamano01" style="width: 10%;">Código Catastral</td>
                        <td>
                            <input type="text" name="codCatastral" id="codCatastral" value="<?php echo @ $_POST['codCatastral'];?>" style="text-align:center;" readonly/>
                        </td>
                    </tr>   

                    <tr>
                        <td class="tamano01" style="width: 10%;">Estrato</td>
                        <td>
                            <input type="text" name="estrato" id="estrato" value="<?php echo @ $_POST['estrato'];?>" style="text-align:center;" readonly/>
                        </td>

                        <td class="tamano01" style="width: 10%;">Tipo de uso</td>
                        <td>
                            <input type="text" name="tipoUso" id="tipoUso" value="<?php echo @ $_POST['tipoUso'];?>" style="text-align:center;" readonly/>
                        </td>

                        <td class="tamano01" style="width: 10%;">Zona de uso</td>
                        <td style="width: 28%;">
                            <input type="text" name="zonaUso" id="zonaUso" value="<?php echo @ $_POST['zonaUso'];?>" style="text-align:center;" readonly/>
                        </td>
                    </tr>

                    <tr>
                        <td class="tamano01" style="width: 10%;">Dirección</td>
                        <td>
                            <input type="text" name="direccion" id="direccion" value="<?php echo @ $_POST['direccion'];?>"  readonly/>
                        </td>

                        <td class="tamano01" style="width: 10%;">Ruta</td>
                        <td>
                            <input type="text" name="ruta" id="ruta" value="<?php echo @ $_POST['ruta'];?>" style="text-align:center;" readonly/>
                        </td>

                        <td class="tamano01" style="width: 10%;">Código Ruta</td>
                        <td style="width: 28%;">
                            <input type="text" name="codigoRuta" id="codigoRuta" value="<?php echo @ $_POST['codigoRuta'];?>" style="text-align:center;" readonly/>
                        </td>
                    </tr>
                </table>

                <table class="inicio grande">
                    <tr>
                        <td class="titulos" colspan="7">.: Información de Factura</td>
                    </tr>
                    
                    <tr>
                        <td class="tamano01" style="width: 10%;">Número Factura</td>
                        <td style="width: 9%;">
                            <input type="text" name="numeroFactura" id="numeroFactura" value="<?php echo @ $_POST['numeroFactura'];?>" style="text-align: center;" readonly/>
                        </td>

                        <td class="tamano01" style="width: 10%;">Valor Total</td>
                        <td style="width: 9%;">
                            <input type="text" name="valorTotal" id="valorTotal" value="<?php echo @ $_POST['valorTotal'];?>" style="text-align: center;" readonly/>
                        </td>

                        <td class="tamano01" style="width: 10%;">Estado Pago</td>
                        <td style="width: 29%;">
                            <input type="text" name="estadoPago" id="estadoPago" value="<?php echo @ $_POST['estadoPago'];?>" style="text-align: center;" readonly/>
                        </td>

                        <td rowspan="10">
                            <img src="imagenes/factura-liquidacion.png" height="60px" alt="Usuario">
                        </td>
                    </tr>

                    <tr>
                        <td class="tamano01" style="width: 10%;">Inicio Periodo Liquidación</td>
                        <td>
                            <input type="text" name="fechaInicial" id="fechaInicial" value="<?php echo @ $_POST['fechaInicial'];?>" style="text-align: center;" readonly/>
                        </td>

                        <td class="tamano01" style="width: 10%;">Final Periodo Liquidación</td>
                        <td>
                            <input type="text" name="fechaFinal" id="fechaFinal" value="<?php echo @ $_POST['fechaFinal'];?>" style="text-align: center;" readonly/>
                        </td>

                        <td class="tamano01" style="width: 10%;">Fecha Limite de Pago</td>
                        <td style="width: 29%;">
                            <input type="text" name="fechaLimite" id="fechaLimite" value="<?php echo @ $_POST['fechaLimite'];?>" style="text-align: center;" readonly/>
                        </td>
                    </tr>
                </table>
            </div>

            <div class="subpantalla" style="overflow-x: hidden;">
                <table class="inicio">
                    <tr><td class="titulos" colspan="21">.: Informaci&oacute;n Servicios</td></tr>

                    <tr class="titulos2" style="text-align: center; height: 30px;">
                        <td>Servicio</td>
                        <td>Lectura Anterior</td>
                        <td>Lectura Actual</td>
                        <td>Consumo</td>
                        <td>Cargo Fijo</td>
                        <td>Consumo Basico</td>
                        <td>Consumo Complementario</td>
                        <td>Consumo Suntuario</td>
                        <td>Subsidio CF</td>
                        <td>Subsidio CB</td>
                        <td>Subsidio CC</td>
                        <td>Subsidio CS</td>
                        <td>Contribución CF</td>
                        <td>Contribución Consumo</td>
                        <td>Acuerdo de Pago</td>
                        <td>Venta Medidor</td>
                        <td>Alumbrado</td>
                        <td>Deuda Anterior</td>
                        <td>Interes mora</td>
                        <td>Abono</td>
                        <td>Total</td>
                    </tr>

                    <?php
                        $iter  = 'saludo1a';
                        $iter2 = 'saludo2';

                        $sqlCorteDet = "SELECT id_corte, estado_pago FROM srvcortes_detalle WHERE numero_facturacion = $numeroFactura";
                        $rowCorteDet = mysqli_fetch_row(mysqli_query($linkbd,$sqlCorteDet));
                        $corteActual = $rowCorteDet[0];
                        $corteAnterior = $corteActual - 1;

                        $sqlFacturas = "SELECT cargo_f, consumo_b, consumo_c, consumo_s, subsidio_cf, subsidio_cb, subsidio_cc, subsidio_cs, contribucion_cf, contribucion_cb, contribucion_cc, contribucion_cs, deuda_anterior, abonos, acuerdo_pago, venta_medidor, interes_mora, alumbrado, estado, id_servicio FROM srvfacturas WHERE num_factura = $numeroFactura AND estado = 'ACTIVO'";
                        $resFacturas = mysqli_query($linkbd,$sqlFacturas);
                        while($rowFacturas = mysqli_fetch_row($resFacturas))
                        {
                            $lecturaAnt = buscaLectura($idCliente, $corteAnterior, $rowFacturas[19]);
                            $lecturaAct = buscaLectura($idCliente, $corteActual, $rowFacturas[19]);
                            $consumo = buscaConsumo($idCliente, $corteActual, $rowFacturas[19]);
                            $cf = number_format($rowFacturas[0], 2, '.', '');
                            $cb = number_format($rowFacturas[1], 2, '.', '');
                            $cc = number_format($rowFacturas[2], 2, '.', '');
                            $cs = number_format($rowFacturas[3], 2, '.', '');
                            $sb_cf = number_format($rowFacturas[4], 2, '.', '');
                            $sb_cb = number_format($rowFacturas[5], 2, '.', '');
                            $sb_cc = number_format($rowFacturas[6], 2, '.', '');
                            $sb_cs = number_format($rowFacturas[7], 2, '.', '');
                            $cb_cf = number_format($rowFacturas[8], 2, '.', '');
                            $cb_cb = $rowFacturas[9] + $rowFacturas[10] + $rowFacturas[11];
                            $cb_cb = number_format($cb_cb, 2, '.', '');
                            $deuda_anterior = number_format($rowFacturas[12], 2, '.', '');
                            $abonos = number_format($rowFacturas[13], 2, '.', '');
                            $acuero_p = number_format($rowFacturas[14], 2, '.', '');
                            $venta_m = number_format($rowFacturas[15], 2, '.', '');
                            $interes_m = number_format($rowFacturas[16], 2, '.', '');
                            $alumbrado = number_format($rowFacturas[17], 2, '.', '');

                            $sqlServicio = "SELECT nombre FROM srvservicios WHERE id = $rowFacturas[19]";
                            $rowServicio = mysqli_fetch_row(mysqli_query($linkbd,$sqlServicio));

                            $servicio = $rowServicio[0];

                            $subTotal = $cf + $cb + $cc + $cs + $cb_cf + $cb_cb + $cb_cc + $cb_cs + $deuda_anterior + $acuero_p + $venta_m + $interes_m + $alumbrado - $sb_cf - $sb_cb - $sb_cc - $sb_cs - $abonos;
                            $subTotal = number_format($subTotal, 2, '.', '');
                    ?>
                        <tr class='<?php echo $iter ?>' style='text-align:center; text-transform:uppercase; height: 30px;'>
                            
                            <td><?php echo $servicio ?></td>
                            <td><?php echo $lecturaAnt ?></td>
                            <td><?php echo $lecturaAct ?></td>
                            <td><?php echo $consumo ?></td>
                            <td><?php echo $cf ?></td>
                            <td><?php echo $cb ?></td>
                            <td><?php echo $cc ?></td>
                            <td><?php echo $cs ?></td>
                            <td><?php echo $sb_cf ?></td>
                            <td><?php echo $sb_cb ?></td>
                            <td><?php echo $sb_cc ?></td>
                            <td><?php echo $sb_cs ?></td>
                            <td><?php echo $cb_cf ?></td>
                            <td><?php echo $cb_cb ?></td>
                            <td><?php echo $acuero_p ?></td>
                            <td><?php echo $venta_m ?></td>
                            <td><?php echo $alumbrado ?></td>
                            <td><?php echo $deuda_anterior ?></td>
                            <td><?php echo $interes_m ?></td>
                            <td><?php echo $abonos ?></td>
                            <td><?php echo $subTotal ?></td>
                        </tr>
                    <?php
                            $total += $subTotal;
                            $aux=$iter;
                            $iter=$iter2;
                            $iter2=$aux;
                        }

                        $_POST['valorTotal'] = $total;
                    ?>
                </table>
            </div>

			<input type="hidden" name="oculto" id="oculto" value="1"/>

			<?php
				if(@ $_POST['oculto']=="")
				{
                
                    $sqlCorteDet = "SELECT id_corte, estado_pago FROM srvcortes_detalle WHERE numero_facturacion = '$_GET[factura]'";
                    $rowCorteDet = mysqli_fetch_row(mysqli_query($linkbd,$sqlCorteDet));

                    $_POST['idCorte'] = $rowCorteDet[0];
                    $_POST['facturaFinal'] = $_GET['factura'];
                    $_POST['facturaInicial'] = $_GET['factura'];

                    $sqlcorte = "SELECT fecha_impresion,fecha_inicial,fecha_final FROM srvcortes WHERE numero_corte = '".$_POST['idCorte']."'";
					$rescorte = mysqli_query($linkbd,$sqlcorte);
					$rowcorte = mysqli_fetch_row($rescorte);
					echo "
					<input type='hidden' name='fechaimpresion' id='fechaimpresion' value='$rowcorte[0]'/>
					<input type='hidden' name='fechainicial' id='fechainicial' value='$rowcorte[1]'/>
					<input type='hidden' name='fechafinal' id='fechafinal' value='$rowcorte[2]'/>
					";
					$c=$totalfact=$bancliente=0;
					$totalcli=$_POST['facturaFinal']-$_POST['facturaInicial']+1;
					for($x = $_POST['facturaInicial']; $x <= $_POST['facturaFinal'];$x++)
					{
						
						$totalfact++;
						$c+=1;
						$sqlser = "SELECT id FROM srvservicios ORDER BY id";
						$respser = mysqli_query($linkbd,$sqlser);
						while ($rowser = mysqli_fetch_row($respser))
						{
							$sqldet = "SELECT id_tipo_cobro,credito,debito,id_cliente,tipo_movimiento,id_servicio FROM srvdetalles_facturacion WHERE numero_facturacion='$x' AND id_servicio='$rowser[0]'";
							$resdet = mysqli_query($linkbd,$sqldet);
							while ($rowdet = mysqli_fetch_row($resdet))
							{
								$dettotal=$rowdet[1]-$rowdet[2];
								echo"<input type='hidden' name='servicio".$x."[".$rowser[0]."][".$rowdet[0]."_".$rowdet[4]."]' value='$dettotal'/>";
								if($bancliente != $rowdet[3])
								{
									$bancliente = $rowdet[3];
									$sqlmedidor = "SELECT id_medidor FROM srvasignacion_servicio WHERE id_clientes = '$rowdet[3]' AND id_servicio = '1' AND estado = 'S'";
									$resmedidor = mysqli_query($linkbd,$sqlmedidor);
									$rowmedidor = mysqli_fetch_row($resmedidor);
									echo "<input type='hidden' name='medidorfactura[$x]' id='medidorfactura[$x]' value='$rowmedidor[0]'/>";
									
									$sqlusoacueducto = "SELECT cargo_fijo, consumo FROM srvasignacion_servicio WHERE id_clientes = '$rowdet[3]' AND id_servicio = '1' AND estado = 'S'";
									$resusoacueducto = mysqli_query($linkbd,$sqlusoacueducto);
									$rowusoacueducto = mysqli_fetch_row($resusoacueducto);
									echo "<input type='hidden' name='usoacueductocf[$x]' id='usoacueductocf[$x]' value='$rowusoacueducto[0]'/>";
									echo "<input type='hidden' name='usoacueductocs[$x]' id='usoacueductocs[$x]' value='$rowusoacueducto[1]'/>";
									
									$sqlusoalcantarillado = "SELECT cargo_fijo, consumo FROM srvasignacion_servicio WHERE id_clientes = '$rowdet[3]' AND id_servicio = '2' AND estado = 'S'";
									$resusoalcantarillado = mysqli_query($linkbd,$sqlusoalcantarillado);
									$rowusoalcantarillado = mysqli_fetch_row($resusoalcantarillado);
									echo "<input type='hidden' name='usoalcantarilladocf[$x]' id='usoalcantarilladocf[$x]' value='$rowusoalcantarillado[0]'/>";
									echo "<input type='hidden' name='usoalcantarilladocs[$x]' id='usoalcantarilladocs[$x]' value='$rowusoalcantarillado[1]'/>";
									
									$sqlusaseo = "SELECT cargo_fijo, consumo FROM srvasignacion_servicio WHERE id_clientes = '$rowdet[3]' AND id_servicio = '3' AND estado = 'S'";
									$resusoaseo = mysqli_query($linkbd,$sqlusaseo);
									$rowusoaseo = mysqli_fetch_row($resusoaseo);
									echo "<input type='hidden' name='usoaseocf[$x]' id='usoaseocf[$x]' value='$rowusoaseo[0]'/>";
									echo "<input type='hidden' name='usoaseocs[$x]' id='usoaseocs[$x]' value='$rowusoaseo[1]'/>";
									
									$sqlcliente = "SELECT cod_usuario, id_estrato, id_tercero, id_barrio, cod_catastral, estado, id_ruta FROM srvclientes WHERE id='$rowdet[3]'";
									$rescliente = mysqli_query($linkbd,$sqlcliente);
									$rowcliente = mysqli_fetch_row($rescliente);
									echo "<input type='hidden' name='codigocliente[$x]' id='codigocliente[$x]' value='$rowcliente[0]'/>";
									
									echo "<input type='hidden' name='codigocatastral[$x]' id='codigocatastral[$x]' value='$rowcliente[4]'/>";
									
									echo "<input type='hidden' name='estadocliente[$x]' id=estadocliente[$x]' value='$rowcliente[5]'/>";
									
									echo "<input type='hidden' name='codruta[$x]' id=codruta[$x]' value='$rowcliente[6]'/>";
									
									$sqlestrato = "SELECT descripcion,uso FROM srvestratos WHERE id='$rowcliente[1]'";
									$resestrato = mysqli_query($linkbd,$sqlestrato);
									$rowestrato = mysqli_fetch_row($resestrato);
									echo "<input type='hidden' name='estratocliente[$x]' id='estratocliente[$x]' value='$rowestrato[0]'/>";
									
									echo "<input type='hidden' name='estratoclientenum[$x]' id='estratoclientenum[$x]' value='$rowcliente[1]'/>";
									
									$sqlusosuelo = "SELECT nombre FROM srvusosdesuelo WHERE id='$rowestrato[1]'";
									$resusosuelo = mysqli_query($linkbd,$sqlusosuelo);
									$rowusosuelo = mysqli_fetch_row($resusosuelo);
									echo "<input type='hidden' name='usosuelocliente[$x]' id='usosuelocliente[$x]' value='$rowusosuelo[0]'/>";
									
									$sqltercero = "SELECT nombre1,nombre2,apellido1,apellido2,razonsocial FROM terceros WHERE id_tercero='$rowcliente[2]'";
									$restercero = mysqli_query($linkbd,$sqltercero);
									$rowtercero = mysqli_fetch_row($restercero);
									if($rowtercero[4] != '') {echo "<input type='hidden' name='tercerocliente[$x]' id='tercerocliente[$x]' value='$rowtercero[4]'/>";}
									else {echo "<input type='hidden' name='tercerocliente[$x]' id='tercerocliente' value='".$rowtercero[0]." ".$rowtercero[1]." ".$rowtercero[2]." ".$rowtercero[3]."'/>";}
									
									$sqldireccion = "SELECT direccion FROM srvdireccion_cliente WHERE id='$rowdet[3]'";
									$resdireccion = mysqli_query($linkbd,$sqldireccion);
									$rowdireccion = mysqli_fetch_row($resdireccion);
									echo "<input type='hidden' name='direccioncliente[$x]' id='direccioncliente[$x]' value='$rowdireccion[0]'/>";
									
									$sqlbarrio = "SELECT nombre FROM srvbarrios WHERE id='$rowcliente[3]'";
									$resbarrio = mysqli_query($linkbd,$sqlbarrio);
									$rowbarrio = mysqli_fetch_row($resbarrio);
									echo "<input type='hidden' name='barriocliente[$x]' id='barriocliente[$x]' value='$rowbarrio[0]'/>";
									
									$sqlconsumo = "SELECT GROUP_CONCAT(consumo ORDER BY corte DESC SEPARATOR '<->') FROM srvlectura WHERE id_servicio = '1' AND id_cliente = '$rowdet[3]' AND corte <= ".$_POST['idCorte'];
									$resconsumo = mysqli_query($linkbd,$sqlconsumo);
									$rowconsumo = mysqli_fetch_row($resconsumo);
									echo "<input type='hidden' name='consumocliente[$x]' id='consumocliente[$x]' value='$rowconsumo[0]'/>";

									$sqlobservacion = "SELECT codigo_observacion FROM srv_asigna_novedades WHERE id_cliente = '$rowdet[3]' AND corte = '$_POST[idCorte]' AND estado = 'S' ";
									$resobservacion = mysqli_query($linkbd,$sqlobservacion);
									$rowobservacion = mysqli_fetch_row($resobservacion);
									
									$sqlNovedadCab = "SELECT descripcion, afecta_facturacion FROM srv_tipo_observacion WHERE codigo_observacion = '$rowobservacion[0]'";
									$rowNovedadCab = mysqli_fetch_row(mysqli_query($linkbd, $sqlNovedadCab));

									echo "<input type='hidden' name='observacion[$x]' id='observacion[$x]' value='$rowNovedadCab[0]'/>";

									if ($rowNovedadCab[1] == 'S') {

										$sqlNovedadDet = "SELECT cargo_fijo, consumo FROM srv_observacion_det WHERE codigo_observacion = '$rowobservacion[0]' AND id_servicio = 1 AND estado = 'S' ";
										$rowNovedadDet = mysqli_fetch_row(mysqli_query($linkbd, $sqlNovedadDet));

										echo "<input type='hidden' name='novedadAcueductoCF[$x]' id='novedadAcueductoCF[$x]' value='$rowNovedadDet[0]'/>";
										echo "<input type='hidden' name='novedadAcueductoCS[$x]' id='novedadAcueductoCS[$x]' value='$rowNovedadDet[1]'/>";

										$sqlNovedadDet = "SELECT cargo_fijo, consumo FROM srv_observacion_det WHERE codigo_observacion = '$rowobservacion[0]' AND id_servicio = 2 AND estado = 'S' ";
										$rowNovedadDet = mysqli_fetch_row(mysqli_query($linkbd, $sqlNovedadDet));

										echo "<input type='hidden' name='novedadAlcantarilladoCF[$x]' id='novedadAlcantarilladoCF[$x]' value='$rowNovedadDet[0]'/>";
										echo "<input type='hidden' name='novedadAlcantarilladoCS[$x]' id='novedadAlcantarilladoCS[$x]' value='$rowNovedadDet[1]'/>";

										$sqlNovedadDet = "SELECT cargo_fijo, consumo FROM srv_observacion_det WHERE codigo_observacion = '$rowobservacion[0]' AND id_servicio = 3 AND estado = 'S' ";
										$rowNovedadDet = mysqli_fetch_row(mysqli_query($linkbd, $sqlNovedadDet));

										echo "<input type='hidden' name='novedadAseoCF[$x]' id='novedadAseoCF[$x]' value='$rowNovedadDet[0]'/>";
										echo "<input type='hidden' name='novedadAseoCS[$x]' id='novedadAseoCS[$x]' value='$rowNovedadDet[1]'/>";
									}
									else {
										echo "<input type='hidden' name='novedadAcueductoCF[$x]' id='novedadAcueductoCF[$x]' value='S'/>";
										echo "<input type='hidden' name='novedadAcueductoCS[$x]' id='novedadAcueductoCS[$x]' value='S'/>";

										echo "<input type='hidden' name='novedadAlcantarilladoCF[$x]' id='novedadAlcantarilladoCF[$x]' value='S'/>";
										echo "<input type='hidden' name='novedadAlcantarilladoCS[$x]' id='novedadAlcantarilladoCS[$x]' value='S'/>";

										echo "<input type='hidden' name='novedadAseoCF[$x]' id='novedadAseoCF[$x]' value='S'/>";
										echo "<input type='hidden' name='novedadAseoCS[$x]' id='novedadAseoCS[$x]' value='S'/>";
									}

									$sqlAcuerdoPago = "SELECT valor_cuota, valor_acuerdo, valor_liquidado FROM srv_acuerdo_cab WHERE id_cliente = '$rowdet[3]' AND estado_acuerdo = 'Activo'";
									$resAcuerdoPago = mysqli_query($linkbd,$sqlAcuerdoPago);
									$rowAcuerdoPago = mysqli_fetch_row($resAcuerdoPago);

									if($rowAcuerdoPago[0] != '')
									{
										$saldoPendiente = $rowAcuerdoPago[1] - $rowAcuerdoPago[2];
									}
									else
									{
										$saldoPendiente = 0;
									}

									echo "<input type='hidden' name='saldoPendiente[$x]' id='saldoPendiente[$x]' value='$saldoPendiente'/>";

									$sqlLectura = "SELECT GROUP_CONCAT(lectura_medidor ORDER BY corte DESC SEPARATOR '<->') FROM srvlectura WHERE id_servicio = '1' AND id_cliente = '$rowdet[3]' AND corte <= ".$_POST['idCorte'];
									$resLectura = mysqli_query($linkbd,$sqlLectura);
									$rowLectura = mysqli_fetch_row($resLectura);
									echo "<input type='hidden' name='lecturaMedidor[$x]' id='lecturaMedidor[$x]' value='$rowLectura[0]'/>";
									
									$sqlpordescf = "SELECT subsidio FROM srvcargo_fijo WHERE id_estrato = '$rowcliente[1]' AND id_servicio = '1'";
									$respordescf = mysqli_query($linkbd,$sqlpordescf);
									$rowpordescf = mysqli_fetch_row($respordescf);
									echo "<input type='hidden' name='porsubacueductofijo[$x]' id='porsubacueductofijo[$x]' value='$rowpordescf[0]'/>";
									
									$sqlpordescc = "SELECT subsidio, costo_unidad FROM srvtarifas WHERE id_estrato = '$rowcliente[1]' AND id_servicio = '1' AND subsidio <> 0";
									$respordescc = mysqli_query($linkbd,$sqlpordescc);
									$rowpordescc = mysqli_fetch_row($respordescc);
									echo "<input type='hidden' name='porsubacueducto[$x]' id='porsubacueducto[$x]' value='$rowpordescc[0]'/>";
									echo "<input type='hidden' name='costosubacueducto[$x]' id='costosubacueducto[$x]' value='$rowpordescc[1]'/>";
									
									$sqlpordescf = "SELECT subsidio FROM srvcargo_fijo WHERE id_estrato = '$rowcliente[1]' AND id_servicio = '2'";
									$respordescf = mysqli_query($linkbd,$sqlpordescf);
									$rowpordescf = mysqli_fetch_row($respordescf);
									echo "<input type='hidden' name='porsubalcantarilladofijo[$x]' id='porsubalcantarilladofijo[$x]' value='$rowpordescf[0]'/>";
									
									$sqlpordescc = "SELECT subsidio, costo_unidad FROM srvtarifas WHERE id_estrato = '$rowcliente[1]' AND id_servicio = '2' AND subsidio <> 0";
									$respordescc = mysqli_query($linkbd,$sqlpordescc);
									$rowpordescc = mysqli_fetch_row($respordescc);
									echo "<input type='hidden' name='porsubalcantarillado[$x]' id='porsubalcantarillado[$x]' value='$rowpordescc[0]'/>";
									echo "<input type='hidden' name='costosubalcantarillado[$x]' id='costosubalcantarillado[$x]' value='$rowpordescc[1]'/>";
									
									$sqlpordesaseo = "SELECT subsidio, costo_unidad FROM srvcostos_estandar WHERE id_estrato = '$rowcliente[1]' AND id_servicio = '3'";
									$respordesaseo = mysqli_query($linkbd,$sqlpordesaseo);
									$rowpordesaseo = mysqli_fetch_row($respordesaseo);
									echo "<input type='hidden' name='porsubaseo[$x]' id='porsubaseo[$x]' value='$rowpordesaseo[0]'/>";
									echo "<input type='hidden' name='costosubaseo[$x]' id='costosubaseo[$x]' value='$rowpordesaseo[1]'/>";

									$contadorFacturasVencidas = 0;
									$sqlFacturasVencidas = "SELECT estado_pago FROM srvcortes_detalle WHERE id_cliente = $rowdet[3] AND id_corte < '$_POST[idCorte]' ORDER BY id_corte DESC ";
									$resFacturasVencidas = mysqli_query($linkbd, $sqlFacturasVencidas);
									while ($rowFacturasVencidas = mysqli_fetch_row($resFacturasVencidas)){

										if ($rowFacturasVencidas[0] == 'V') {
											$contadorFacturasVencidas += 1;
										}
										else {
											break;
										}
									}
									echo "<input type='hidden' name='facVencidas[$x]' id='facVencidas[$x]' value='$contadorFacturasVencidas'/>";
								}
							}
						}
						$porcentaje = $c * 100 / $totalcli; 
						echo"
						<script>
							progres='".round($porcentaje)."';callprogress(progres);
							document.getElementById('totalfacturas').value=$totalfact;
						</script>";
						flush();
						ob_flush();
						usleep(5);
					}
				}
			?>
		</form>
		<div id="bgventanamodal2">
			<div id="ventanamodal2">
				<IFRAME  src="" name="buster" marginWidth=0 marginHeight=0 frameBorder=0 id="ventana2" frameSpacing=0 style="left:500px; width:900px; height:500px; top:200;"></IFRAME>
			</div>
		</div>
	</body>
</html>