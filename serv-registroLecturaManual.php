<?php
header("Cache-control: no-cache, no-store, must-revalidate");
header("Content-Type: text/html;charset=iso-8859-1");
require "comun.inc";
require "funciones.inc";
require "funcionesSP.inc.php";
session_start();
$linkbd = conectar_v7();
cargarcodigopag($_GET['codpag'],$_SESSION["nivel"]);
date_default_timezone_set("America/Bogota");
?>
<!DOCTYPE >
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>:: IDEAL 10 - Servicios P&uacute;blicos</title>
    <link href="css/css2.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css"/>
    <link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css"/>
    <link href="css/css4.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css"/>
    <link href="css/cssSP.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css"/>
    <script type="text/javascript" src="css/calendario.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
    <script type="text/javascript" src="css/programas.js"></script>

    <style>
    </style>

    <script>
        function despliegamodalm(_valor,_tip,mensa,pregunta)
        {
            document.getElementById("bgventanamodalm").style.visibility=_valor;

            if(_valor=="hidden")
            {
                document.getElementById('ventanam').src="";
            }
            else
            {
                switch(_tip)
                {
                    case "1":	document.getElementById('ventanam').src="ventana-mensaje1.php?titulos="+mensa;break;
                    case "2":	document.getElementById('ventanam').src="ventana-mensaje3.php?titulos="+mensa;break;
                    case "3":	document.getElementById('ventanam').src="ventana-mensaje2.php?titulos="+mensa;break;
                    case "4":	document.getElementById('ventanam').src="ventana-consulta1.php?titulos="+mensa+"&idresp="+pregunta;break;	
                }
            }
        }

        function funcionmensaje()
        {
            document.location.href = "serv-registrarLectura.php";
        }

        function respuestaconsulta(pregunta)
        {
            switch(pregunta)
            {
                case "1":
                    document.form2.oculto.value='2';
                    document.form2.submit();
                break;
            }
        }

        function guardar()
        {
            despliegamodalm('visible','4','Esta seguro de guardar?','1');
        }

		function agregarcolumna()
        {
            document.form2.validador.value='2';
            document.form2.rrr.value='2';
            document.form2.submit();
        }
    </script>
</head>

    <?php titlepag();?>
    
<body>
    <IFRAME src="alertas.php" name="alertas" id="alertas" style="display:none"></IFRAME>
    <span id="todastablas2"></span>
    <table>
        <tr><script>barra_imagenes("serv");</script><?php cuadro_titulos();?></tr>
        <tr><?php menu_desplegable("serv");?></tr>
        <tr>
            <td colspan="3" class="cinta">
                <a href="" class="mgbt"><img src="imagenes/add.png"/></a>

                <a onclick="guardar()" class="mgbt"><img src="imagenes/guarda.png" title="Guardar"/></a>

                <a href="" class="mgbt"><img src="imagenes/busca.png" title="Buscar"/></a>

                <a onclick="mypop=window.open('plan-agenda.php','','');mypop.focus()" class="mgbt"><img src="imagenes/agenda1.png" title="Agenda" /></a>

                <a onclick="mypop=window.open('serv-principal.php','','');mypop.focus();" class="mgbt"><img src="imagenes/nv.png" title="Nueva ventana"></a>

                <a onclick="mypop=window.open('<?php echo $url2; ?>','','');mypop.focus();" class="mgbt"><img src="imagenes/duplicar_pantalla.png" title="Duplicar pesta&ntilde;a"></a>
            </td>
        </tr>
    </table>

    <div id="bgventanamodalm" class="bgventanamodalm">
        <div id="ventanamodalm" class="ventanamodalm">
            <IFRAME src="" name="ventanam" marginWidth=0 marginHeight=0 frameBorder=0 id="ventanam" frameSpacing=0 style=" width:700px; height:130px; top:200; overflow:hidden;"></IFRAME>
        </div>
    </div>

    <form name="form2" method="post" action="">
        <?php
            $vigusu=vigencia_usuarios($_SESSION['cedulausu']);

            if(@$_POST['oculto'] == '')
            {
               $_POST['corte'] = siguienteCorte();
			   $_POST['corteLectura'] = siguienteCorte();
               $total_lecturas++; 
            }

            if(@$_POST['validador'] = '1')
            {
                $total_lecturas++; 
            }
        ?>
        
        <div>
            <table class="inicio">
                <tr>
					<td class="titulos" colspan="7">.: Registro de Lecturas</td>

					<td class="cerrar" style="width:7%" onclick="location.href='serv-principal.php'">Cerrar</td>
				</tr>

                <tr>
					<td class="tamano01" style="width: 3cm;">Corte: </td>

					<td style="width: 15%;">
						<input type="text" name="corte" id="corte" value="<?php echo $_POST['corte']?>" style="height: 30px; width: 98%; text-align:center;" readonly>
					</td>

					<td class="tamano01" style="width: 3cm;">Servicio: </td>

					<td>
						<select name="id_servicio" id="id_servicio" value="<?php echo $_POST['id_servicio']?>" class="centrarSelect" style="height: 30px; width: 98%;">
							<option class="aumentarTamaño" value="-1">:: SELECCIONE SERVICIO ::</option>
							<?php
								$sql = "SELECT id, nombre FROM srvservicios";
								$res = mysqli_query($linkbd,$sql);
								while($row = mysqli_fetch_assoc($res))
								{
									if(@$_POST['id_servicio'] == $row['id'])
									{
										echo "<option class='aumentarTamaño' value='$row[id]' selected>$row[nombre]</option>";
									}
									else
									{
										echo "<option class='aumentarTamaño' value='$row[id]'>$row[nombre]</option>";
									}
								}
							?>
						</select>
					</td>

					<td class="tamano01" style="width: 3cm;">Fecha:</td>

					<td style="width: 15%;">
						<input type="text" name="fecha" value="<?php echo @ $_POST['fecha']?>" maxlength="10" onKeyPress="javascript:return solonumeros(event)" onKeyUp="return tabular(event,this)" id="fecha" onKeyDown="mascara(this,'/',patron,true)" title="DD/MM/YYYY" style="width:80%;height: 30px; text-align:center;" readonly>&nbsp;
                        <img src="imagenes/calendario04.png" onClick="displayCalendarFor('fecha');" title="Calendario" class="icobut"/>
					</td>
				</tr>
            </table>

            <table>
				<tr>
                    <td class="titulos" colspan="5" height="25">Captura de Lecturas</td>
                </tr>

				<tr class="titulos2">
                    <td style="text-align: center;">C&oacute;digo Usuario</td>
                    <td style="text-align: center;">Documento</td>
                    <td style="text-align: center;">Nombre Usuario</td>
                    <td style="text-align: center;">Direcci&oacute;n</td>
                    <td style="text-align: center;">Lectura</td>
                </tr>

				<?php
                
                if($_POST['rrr'] = '1')
                {
                    $total_lecturas = 1;
                }
                else
                {
                    $total_lecturas++; 
                }


                $iter = 'saludo1a';
                $iter2 = 'saludo2';

                echo $total_lecturas;
                for ($x=0; $x < $total_lecturas; $x++) 
                { 
                    echo "
                    <tr class='$iter'>
                        <td style='width:10%'>
							<div>
								<input type='text' id='cod_usuario$x' name='cod_usuario$x' value='".$_POST["cod_usuario$x"]."' ".$_POST["cod_usuario$x"]."' style='text-align:center;' onBlur='agregarcolumna();'/>
							</div>
						</td>
                        <td style='text-align:center;'>".$_POST['documento_usuario'][$x]."</td>
						<td style='text-align:center;'>".$_POST['nombre_usuario'][$x]."</td>
                        <td style='text-align:center;'>".$_POST['direccion_usuario'][$x]."</td>
						<td style='width:8%'>
							<div>
								<input type='text' id='lectura$x' name='lectura$x' value='".$_POST["lectura$x"]."' ".$_POST["lectura$x"]."' style='text-align:center;'/>
							</div>
						</td>
                    </tr>
                    ";

                    $aux = $iter;
                    $iter = $iter2;
                    $iter2 = $aux;
                }
                ?>
			</table>

            <input type="hidden" name="oculto" id="oculto" value="1">
            <input type="hidden" name="rrr" id="rrr" value="1">
            <input type="hidden" name="total_lecturas" id="total_lecturas" value="<?php echo $_POST['total_lecturas'] ?>">
            <input type="hidden" name="validador" id="validador" value="1">
            <input type="hidden" name="generarLista" id="generarLista" value="1">
            <input type="hidden" name="corteLectura" id="corteLectura" value="<?php echo $_POST['corteLectura'] ?>">
        </div>

		<?php 
			if(@$_POST['generarLista'] == '2')
			{
                
			}

            if(@$_POST['oculto'] == '2')
            {
                $usuario = $_SESSION['usuario'];
                preg_match("/([0-9]{2})\/([0-9]{2})\/([0-9]{4})/", $_POST['fecha'],$f);
                $fecha="$f[3]-$f[2]-$f[1]";
                
                for ($x=0; $x < count($_POST['cod_usuario']); $x++) 
                {
                    $lectura = $_POST["lectura$x"];

                    if($lectura == '')
                    {
                        $lectura = 0;
                    }
                    
                    
                    $sql = "INSERT INTO srvlectura(corte, id_cliente, id_servicio, lectura, fecha, usuario) VALUES ($_POST[corte], '".$_POST['id_cliente'][$x]."', $_POST[id_servicio], $lectura, '$fecha', '$usuario')";
                    $res = mysqli_query($linkbd,$sql);
                }

                if($res)
                {
                    echo "<script>despliegamodalm('visible','1','Se ha almacenado con Exito');</script>";
                }
                else
                {
                    echo"<script>despliegamodalm('visible','2','No se pudo ejecutar el guardado');</script>";
                }
            }
		?>
    </form>
</body>
</html>