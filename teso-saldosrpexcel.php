<?php 
	ini_set('max_execution_time',3600);
	require_once '/PHPExcel/Classes/PHPExcel.php';// Incluir la libreria PHPExcel
	include '/PHPExcel/Classes/PHPExcel/IOFactory.php';// PHPExcel_IOFactory
	require"comun.inc";
	require"validaciones.inc";
	require"funciones.inc"; //guardar
	session_start();
	$linkbd = conectar_v7();
    $linkbd -> set_charset("utf8");
	$objPHPExcel = new PHPExcel();// Crea un nuevo objeto PHPExcel
	$objPHPExcel->getProperties()->setCreator("Ideal SAS")
	   ->setLastModifiedBy("HAFR")
	   ->setTitle("Lista saldos rp´s")
	   ->setSubject("Saldos")
	   ->setDescription("Lista saldos rp´s")
	   ->setKeywords("Saldos")
	   ->setCategory("Tesoreria");
	$objPHPExcel->setActiveSheetIndex(0)
		->mergeCells('A1:D1')
		->mergeCells('A2:D2')
  		->setCellValue('A1', 'SALDOS')
     	->setCellValue('A2', 'INFORMACION GENERAL');
	$objPHPExcel-> getActiveSheet ()
        -> getStyle ("A1")
        -> getFill ()
        -> setFillType (PHPExcel_Style_Fill :: FILL_SOLID)
        -> getStartColor ()
        -> setRGB ('C8C8C8');
	$objPHPExcel-> getActiveSheet ()
        -> getStyle ("A1:A2")
		-> getFont ()
		-> setBold ( true ) 
      	-> setName ( 'Verdana' ) 
      	-> setSize ( 10 ) 
		-> getColor ()
		-> setRGB ('000000');
	$objPHPExcel-> getActiveSheet ()	
		-> getStyle ('A1:A2')
		-> getAlignment ()
		-> applyFromArray (array ( 'horizontal'  =>  PHPExcel_Style_Alignment :: HORIZONTAL_CENTER ,) ); 
	$objPHPExcel-> getActiveSheet ()	
		-> getStyle ('A3:D3')
		-> getAlignment ()
		-> applyFromArray (array ( 'horizontal'  =>  PHPExcel_Style_Alignment :: VERTICAL_JUSTIFY ,) ); 
	$objPHPExcel-> getActiveSheet ()
		-> getStyle ("A2")
        -> getFill ()
        -> setFillType (PHPExcel_Style_Fill :: FILL_SOLID)
        -> getStartColor ()
        -> setRGB ('A6E5F3');
	$objPHPExcel-> getActiveSheet ()
		-> getStyle ("A3:D3")
        -> getFill ()
        -> setFillType (PHPExcel_Style_Fill :: FILL_SOLID)
        -> getStartColor ()
        -> setRGB ('22C6CB');
	$borders = array(
      'borders' => array(
        'allborders' => array(
          'style' => PHPExcel_Style_Border::BORDER_THIN,
          'color' => array('argb' => 'FF000000'),
        )
      ),
    );
	$objPHPExcel->getActiveSheet()->getStyle('A1:D1')->applyFromArray($borders);
	$objPHPExcel->getActiveSheet()->getStyle('A2:D2')->applyFromArray($borders);
	$objPHPExcel->getActiveSheet()->getStyle('A3:D3')->applyFromArray($borders);
	$objPHPExcel-> getActiveSheet()->getColumnDimension('A')->setWidth(12); 
	$objPHPExcel-> getActiveSheet()->getColumnDimension('B')->setWidth(11);
	$objPHPExcel-> getActiveSheet ()	
		-> getStyle ('B:C')
		-> getAlignment ()
		-> applyFromArray (array ( 'horizontal'  =>  PHPExcel_Style_Alignment :: VERTICAL_JUSTIFY ,) ); 
	$objPHPExcel-> getActiveSheet()->getColumnDimension('C')->setWidth(80);
	$objPHPExcel-> getActiveSheet()->getColumnDimension('D')->setWidth(20);
	$objWorksheet = $objPHPExcel->getActiveSheet();
	$objWorksheet->fromArray(array(utf8_encode('No rp'),utf8_encode('Fecha'),utf8_encode('Detalle'),utf8_encode('Saldo')),NULL,'A3');
	ereg( "([0-9]{1,2})/([0-9]{1,2})/([0-9]{2,4})", $_GET[fecha1],$fecha);
	$fechaf=$fecha[3]."-".$fecha[2]."-".$fecha[1];
    $_POST['vigencia'] = $fecha[3];
	ereg( "([0-9]{1,2})/([0-9]{1,2})/([0-9]{2,4})", $_GET[fecha2],$fecha);
	$fechaf2=$fecha[3]."-".$fecha[2]."-".$fecha[1];	
	$cont=4;
	
    $sqlr = "select distinct(p1.vigencia) vigencia ,p1.consvigencia,p1.fecha, p1.detalle ,p1.estado,cd1.consvigencia,p1.valor,p1.saldo, p1.tercero 
    from ccpetrp p1, ccpetcdp cd1, ccpetrp_detalle pd1 
    where p1.idcdp=cd1.consvigencia
    and pd1.vigencia = p1.vigencia 
    and pd1.consvigencia = p1.consvigencia 
    and cd1.tipo_mov='201' 
    and p1.tipo_mov='201'
    and p1.vigencia=$_POST[vigencia] 
    and cd1.vigencia=$_POST[vigencia]
    and fx_ccpet_saldoregistro (p1.consvigencia ,p1.vigencia,pd1.cuenta ,'$fechaf2')>0
    union all
    select distinct(p.vigencia) vigencia,p.consvigencia, p.fecha, p.detalle ,p.estado,cd.consvigencia,p.valor,p.saldo, p.tercero 
    from ccpetrp p , ccpetcdp cd, pptocuentas p2, ccpetrp_detalle pd 
    where p.idcdp=cd.consvigencia
    and cd.tipo_mov='201' 
    and p.tipo_mov='201' 
    and pd.vigencia = p.vigencia 
    and pd.consvigencia = p.consvigencia 
    and pd.vigencia  = p2.vigencia 
    and pd.cuenta    = p2.cuenta 
    and p2.regalias ='S'
    and fx_ccpet_saldoregistro (p.consvigencia ,p.vigencia,pd.cuenta ,'$fechaf2')>0
    and p2.vigenciarg like '%$_POST[vigencia]'";
	$resp = mysqli_query($linkbd, $sqlr);
	//echo $_POST[conanul]."hola";
	while ($row = mysqli_fetch_row($resp)) 
	{
		$scrit="";
		if($row[7] > 0){
                    

            $objWorksheet->fromArray(array($row[1],$row[2],utf8_encode($row[3]),round($row[7],2)),NULL,"A$cont");

            $objPHPExcel->getActiveSheet()->getStyle("A$cont:D$cont")->applyFromArray($borders);
            $objPHPExcel->getActiveSheet()->getStyle("D$cont:D$cont")->getNumberFormat()
            ->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);
            $objPHPExcel->getActiveSheet()->getStyle('A3:D3')->getAlignment()->applyFromArray(array('vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,));
            $objPHPExcel->getActiveSheet()->getStyle('A4:D4')->getAlignment()->applyFromArray(array('vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,));
            $objPHPExcel->getActiveSheet()->getStyle('A4')->getAlignment()->applyFromArray(array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,));
            $objPHPExcel->getActiveSheet()->getStyle('C3')->getAlignment()->applyFromArray(array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,));
            $objPHPExcel->getActiveSheet()->getStyle('A:D')->getAlignment()->applyFromArray(array('vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,));
            $objPHPExcel->getActiveSheet()->getStyle('A')->getAlignment()->applyFromArray(array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,));
            $objPHPExcel-> getActiveSheet()-> getStyle ("A3:D3")-> getFont ()-> setBold ( true );
                
            
            $cont++;
        }
	}
	$objPHPExcel->getActiveSheet()->setTitle('Listado 1');// Renombrar Hoja
	$objPHPExcel->setActiveSheetIndex(0);// Establecer la hoja activa, para que cuando se abra el documento se muestre primero.
	// --------Cerrar--------
	header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
	header('Content-Disposition: attachment;filename="Listado Saldos RPs.xlsx"');
	header('Cache-Control: max-age=0');
	header ('Expires: Mon, 15 Dic 2015 09:31:00 GMT'); // Date in the past
	header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
	header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
	header ('Pragma: public'); // HTTP/1.0
	$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
	$objWriter->save('php://output');
	exit;
?>
