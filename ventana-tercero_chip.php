<?php
	header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=utf8");
	require 'comun.inc';
	require 'funciones.inc';
	$linkbd=conectar_v7();
	$linkbd -> set_charset("utf8");
	cargarcodigopag($_GET['codpag'],$_SESSION["nivel"]);
	date_default_timezone_set("America/Bogota");
?>
<!DOCTYPE >
<html lang=es>
	<head>
		<meta charset="utf-8"/>
		<meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1"/>
		<meta name="viewport" content="user-scalable=no">
		<title>:: SieS</title>
		<link href="css/css2z.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
		<link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
		<link href="css/css4.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
		<script type="text/javascript" src="css/calendario.js"></script>
		<script type="text/javascript" src="css/programas.js"></script>
		<script>
			function fagregar(documento,idfun,nombre){
				if(document.getElementById('tcodfun').value!='' && document.getElementById('tcodfun').value != null){
					var tcodfun = document.getElementById('tcodfun').value;
					parent.document.getElementById(''+tcodfun).value = idfun;
				}
				parent.despliegamodal2("hidden");
			}
			function bbuscar(){
				document.form2.oculto.value = '2';
				document.form2.submit();
			}
		</script> 
		<?php titlepag();?>
	</head>
	<body>
		<form name="form2" method="post">
			<?php
				if($_POST['oculto']==""){
					$_POST['tcodfun'] = $_GET['codfun'];
				}
			?>
			<table class="inicio ancho" style="width:99.5%">
				<tr>
					<td class="titulos" colspan="6">:: Buscar Tercero Chip</td>
					<td class="cerrar" style="width:7%;"><a onClick="parent.despliegamodal2('hidden');">&nbsp;Cerrar</a></td>
				</tr>
				<tr>
					<td class="saludo1" style='width:4cm;'>:: código o Nombre:</td>
					<td colspan="4"><input type="search" name="nombre" id="nombre" value="<?php echo @$_POST['nombre'];?>" style='width:100%;'/> </td>
					<td colspan="1" style="padding-bottom:0px"><em class="botonflechaverde" onClick="bbuscar()">Buscar</em></td>
				</tr> 
			</table> 
			<input type="hidden" name="oculto" id="oculto" value="1"/>
			<input type="hidden" name="tcodfun" id="tcodfun" value="<?php echo @$_POST['tcodfun']?>"/>
			<?php 
				if (@$_POST['nombre']!=""){
					$crit1 = "(codigo LIKE '%".$_POST['nombre']."%' OR nombre LIKE '%".$_POST['nombre']."%') AND ";
				}else {
					$crit1="";
				}
				$sql = "SELECT codigo, nombre FROM ccpet_chip WHERE $crit1 version = (SELECT MAX(version) FROM ccpet_chip )";
				$res = mysqli_query($linkbd, $sql);
				echo "
				<table class='tablamv' style='height:82%; width:99.6%;'>
					<thead>
						<tr style='text-align:left;'>
							<th class='titulos'>.: Resultados Busqueda:</th>
						</tr>
						<tr style='text-align:Center;'>
							<th class='titulosnew00' style='width:5%;'>No</th>
							<th class='titulosnew00' style='width:20%;'>CÓDIGO</th>
							<th class='titulosnew00'>NOMBRE</th>
						</tr>
					</thead>
					<tbody id='divdet'>";	
				$iter='saludo1a';
				$iter2='saludo2';
				$conta = 1;
				if($_POST['oculto']== '2'){
					while($row = mysqli_fetch_row($res)){
						echo "
						<tr class='$iter' onClick=\"javascript:fagregar('$row[0]')\">
							<td style='font: 120% sans-serif; padding-left:10px; width:5%; text-align:Center;'>$conta</td>
							<td style='font: 120% sans-serif; padding-left:10px; width:20%; text-align:left;'>$row[0]</td>
							<td style='font: 120% sans-serif; padding-left:10px; text-align:left;'>$row[1]</td>
						</tr>
						";
						$aux=$iter;
						$iter=$iter2;
						$iter2=$aux;
						$conta++;
					}
				}
				echo"
					</tbody>
				</table>";
			?>
		</form>
	</body>
</html>
