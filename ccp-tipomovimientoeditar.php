<?php
	require "comun.inc";
	require "funciones.inc";
	require "conversor.php";
	session_start();
	$linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");
	$scroll=$_GET['scrtop'];
	$totreg=$_GET['totreg'];
	$idcta=$_GET['idcta'];
	$altura=$_GET['altura'];
	$filtro="'".$_GET['filtro']."'";
?>
<html lang="es">
	<head>
		<meta charset="UTF-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
    	<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<title>:: IDEAL 10 - Presupuesto</title>
        <link href="css/css2.css" rel="stylesheet" type="text/css" />
		<link href="css/css3.css" rel="stylesheet" type="text/css" />
		<script type="text/javascript" src="css/programas.js"></script>
    	<script>
    		function guardar()
			{
				var validacion01=document.getElementById('nombre').value;
				if (validacion01.trim()!='' && document.getElementById('codigo').value!='')
			  		{despliegamodalm('visible','4','Esta Seguro de Guardar','1');}
			  	else {despliegamodalm('visible','2','Faltan datos para completar el registro');}
			 }
			function despliegamodalm(_valor,_tip,mensa,pregunta)
			{
				document.getElementById("bgventanamodalm").style.visibility=_valor;
				if(_valor=="hidden"){document.getElementById('ventanam').src="";}
				else
				{
					switch(_tip)
					{
						case "1":
							document.getElementById('ventanam').src="ventana-mensaje1.php?titulos="+mensa;break;
						case "2":
							document.getElementById('ventanam').src="ventana-mensaje3.php?titulos="+mensa;break;
						case "3":
							document.getElementById('ventanam').src="ventana-mensaje2.php?titulos="+mensa;break;
						case "4":
							document.getElementById('ventanam').src="ventana-consulta1.php?titulos="+mensa+"&idresp="+pregunta;break;
					}
				}
			}
			function respuestaconsulta(pregunta)
			{
				switch(pregunta)
				{
					case "1":	document.getElementById('oculto').value='2';document.form2.submit();break;
				}
			}
    	</script>
		<script>
			function adelante(scrtop, numpag, limreg, filtro, next){
				var maximo=document.getElementById('maximo').value;
				var actual=document.getElementById('codigo').value;
				if(parseFloat(maximo)>parseFloat(actual)){
					document.getElementById('oculto').value='1';
					document.getElementById('codigo').value=next;
					var idcta=document.getElementById('codigo').value;
					document.form2.action="ccp-tipomovimientoeditar.php?idcta="+idcta+"&scrtop="+scrtop+"&numpag="+numpag+"&limreg="+limreg+"&filtro="+filtro;
					document.form2.submit();
				}
			}

			function atrasc(scrtop, numpag, limreg, filtro, prev){
				var minimo=document.getElementById('minimo').value;
				var actual=document.getElementById('codigo').value;
				if(parseFloat(minimo)<parseFloat(actual)){
					document.getElementById('oculto').value='1';
					document.getElementById('codigo').value=prev;
					var idcta=document.getElementById('codigo').value;
					document.form2.action="ccp-tipomovimientoeditar.php?idcta="+idcta+"&scrtop="+scrtop+"&numpag="+numpag+"&limreg="+limreg+"&filtro="+filtro;
					document.form2.submit();
				}
			}

			function iratras(scrtop, numpag, limreg, filtro){
				var idcta=document.getElementById('codigo').value;
				var idcta1=document.getElementById('id').value;
				location.href="ccp-tipomovdocumentosbuscar.php?idcta="+idcta1+"&idcta1="+idcta+"&scrtop="+scrtop+"&numpag="+numpag+"&limreg="+limreg+"&filtro="+filtro;
			}
		</script>
    	<?php titlepag();?>
	</head>
	<body>
    	<IFRAME src="alertas.php" name="alertas" id="alertas" style="display:none"></IFRAME>
    	<span id="todastablas2"></span>
        <?php
		$numpag=$_GET['numpag'];
		$limreg=$_GET['limreg'];
		$scrtop=26*$totreg;
		?>
		<table>
			<tr><script>barra_imagenes("ccpet");</script><?php cuadro_titulos();?></tr>
			<tr><?php menu_desplegable("ccpet");?></tr>
        	<tr>
  				<td colspan="3" class="cinta">
					<img src="imagenes/add.png"  title="Nuevo" href="ccp-tipomovimiento.php" class="mgbt"/><img src="imagenes/guardad.png" title="Guardar"  href="#"  class="mgbt"/><img src="imagenes/busca.png" title="Buscar" href="ccp-tipomovdocumentosbuscar.php" class="mgbt"/><img src="imagenes/agenda1.png" title="Agenda"  onClick="mypop=window.open('plan-agenda.php','','');mypop.focus()" class="mgbt"/><img src="imagenes/nv.png" title="Nueva Ventana" href="#" onClick="mypop=window.open('ccp-principal.php','','');mypop.focus();" class="mgbt"><img src="imagenes/iratras.png" title="Atr&aacute;s" href="#" onClick="iratras(<?php echo $scrtop; ?>, <?php echo $numpag; ?>, <?php echo $limreg; ?>, <?php echo $filtro; ?>)" class="mgbt">
				</td>
			</tr>
		</table>
        <div id="bgventanamodalm" class="bgventanamodalm">
            <div id="ventanamodalm" class="ventanamodalm">
                <IFRAME src="" name="ventanam" marginWidth=0 marginHeight=0 frameBorder=0 id="ventanam" frameSpacing=0 style=" width:700px; height:130px; top:200; overflow:hidden;">
                </IFRAME>
            </div>
        </div>
 		<form name="form2" method="post">
			<?php
			if ($_GET['idproceso']!=""){echo "<script>document.getElementById('codrec').value=$_GET[idproceso];</script>";}
			if ($_GET['codproceso']!=""){echo "<script>document.getElementById('codrec1').value=$_GET[codproceso];</script>";}
			$sqlr="SELECT MIN(id), MAX(id) FROM tipo_movdocumentos ORDER BY id";

			$res=mysqli_query($linkbd, $sqlr);
			$r=mysqli_fetch_row($res);
			$_POST['minimo']=$r[0];
			$_POST['maximo']=$r[1];
			if($_POST['oculto']==""){
				if ($_POST['codrec']!="" || $_GET['idproceso']!=""){
					if($_POST['codrec']!=""){
						$sqlr="SELECT * FROM tipo_movdocumentos where id='$_POST[codrec]' and codigo='$_GET[codproceso]' ";

					}
					else{
						$sqlr="SELECT * FROM tipo_movdocumentos where id='$_GET[idproceso]' and codigo='$_GET[codproceso]'";

					}
				}
				else{
					$sqlr="SELECT * FROM tipo_movdocumentos  ORDER BY id DESC";

				}
				$res=mysqli_query($linkbd, $sqlr);
				$row=mysqli_fetch_row($res);
			   	$_POST['codigo']=$row[1];
				$_POST['id']=$row[0];
				$_POST['nombre']=$row[2];
				$_POST['estado']=$row[3];

			}

 			if($_POST['oculto']!="2")
        		{
					$sqlr="SELECT id,codigo,descripcion,estado FROM tipo_movdocumentos where id=".$_POST['id']."and codigo =".$_POST['codigo'];
            		$resp = mysqli_query($linkbd,$sqlr);
					while ($row =mysqli_fetch_row($resp))
					{
						$_POST['id']=$row[0];

						$_POST['codigo']=$row[1];

						$_POST['nombre']=$row[2];

						$_POST['estado']=$row[3];

					}
        		}
			//NEXT
			$sqln="SELECT * FROM tipo_movdocumentos where CONVERT(id, SIGNED INTEGER) > CONVERT('$_POST[id]', SIGNED INTEGER) ORDER BY CONVERT(id, SIGNED INTEGER) ASC LIMIT 1";
			$resn=mysqli_query($linkbd, $sqln);
			$row=mysqli_fetch_row($resn);
			$next="'".$row[0]."'";
			//PREV
			$sqlp="SELECT * FROM tipo_movdocumentos WHERE CONVERT(id, SIGNED INTEGER) < CONVERT('$_POST[id]', SIGNED INTEGER) ORDER BY CONVERT(id, SIGNED INTEGER) DESC LIMIT 1";
			$resp=mysqli_query($linkbd, $sqlp);
			$row=mysqli_fetch_row($resp);
			$prev="'".$row[0]."'";
     		?>
   			<table class="inicio" >
				<tr>
       				<td class="titulos" colspan="6">Editar Tipo de Movimientos</td>
         			<td class="cerrar" style="width:7%;"><a href="ccp-principal.php">&nbsp;Cerrar</a></td>
				</tr>
   				<tr>
        			<td class="saludo1" style="width:2cm">C&oacute;digo:</td>
                    <td style="width:10%;">
	        	    	<a href="#" onClick="atrasc(<?php echo $scrtop; ?>, <?php echo $numpag; ?>, <?php echo $limreg; ?>, <?php echo $filtro; ?>, <?php echo $prev; ?>)"><img src="imagenes/back.png" alt="anterior" align="absmiddle"></a>
                    	<input type="text" name="codigo" id="codigo" value="<?php echo $_POST['codigo']?>" style="width:35%;" readonly />
	    	            <a href="#" onClick="adelante(<?php echo $scrtop; ?>, <?php echo $numpag; ?>, <?php echo $limreg; ?>, <?php echo $filtro; ?>, <?php echo $next; ?>)"><img src="imagenes/next.png" alt="siguiente" align="absmiddle"></a>
						<input type="hidden" value="<?php echo $_POST['maximo']?>" name="maximo" id="maximo">
						<input type="hidden" value="<?php echo $_POST['minimo']?>" name="minimo" id="minimo">
						<input type="hidden" value="<?php echo $_POST['codrec']?>" name="codrec" id="codrec">
						<input type="hidden" value="<?php echo $_POST['codrec1']?>" name="codrec1" id="codrec1">
						<input type="hidden" name="id" id="id" value="<?php echo $_POST['id']?>"/>
                   	</td>
					<td class="saludo1" style="width:2cm">Nombre:</td>
            		<td style="width:35%"><input type="text" name="nombre" id="nombre" value="<?php echo $_POST['nombre']?>" onKeyUp="return tabular(event,this)" style="width:100%"/></td>
   					<td class="saludo1" style="width:2cm">Estado</td>
            		<td>
            			<select name="estado" id="estado" onKeyUp="return tabular(event,this)" >
          					<option value="S" <?php if($_POST['estado']=='S') echo "SELECTED"; ?>>Activo</option>
          					<option value="N" <?php if($_POST['estado']=='N') echo "SELECTED"; ?>>Inactivo</option>
        				</select>
        			</td>
   				</tr>
   			</table>
            <input type="hidden" name="oculto" id="oculto" value="1"/>
 			<?php
 				if($_POST['oculto']=="2")//********guardar
				{
					$sqlr="UPDATE tipo_movdocumentos SET descripcion='$_POST[nombre]',estado='$_POST[estado]' WHERE codigo=$_POST[codigo] ";
					if (!mysqli_query($linkbd, $sqlr))
					{echo"<script>despliegamodalm('visible','2',''Error no se almaceno el Tipo de Movimiento');</script>";}
		  			else {echo"<script>despliegamodalm('visible','3','Se Modifico el Tipo de Movimiento con Exito');</script>";}
				}
 			?>
		</form>
	</body>
</html>
