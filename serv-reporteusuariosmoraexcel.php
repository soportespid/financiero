<?php  
	require_once 'PHPExcel/Classes/PHPExcel.php';
	require"comun.inc";
	require"funciones.inc";
	session_start();
	$linkbd=conectar_bd();
	$objPHPExcel = new PHPExcel();
	//----Propiedades---- 
	$objPHPExcel->getProperties()
		->setCreator("SPID")
		->setLastModifiedBy("SPID")
		->setTitle("Exportar Excel con PHP")
		->setSubject("Documento de prueba")
		->setDescription("Documento generado con PHPExcel")
		->setKeywords("usuarios phpexcel")
		->setCategory("reportes");
	//----Cuerpo de Documento----
	if($_POST['select_servicio']!=''){$nom='TODOS LOS SERVICIOS';}
	if($_POST['select_servicio']!='01'){$nom='SERVICIO DE ACUEDUCTO';}
	if($_POST['select_servicio']!='02'){$nom='SERVICIO DE ALCANTARILLADO';}
	if($_POST['select_servicio']!='03'){$nom='SERVICIO DE ASEO';}
	$objPHPExcel->getActiveSheet()->mergeCells('A1:I1');
	$objPHPExcel->setActiveSheetIndex(0)
				->setCellValue('A1', 'LISTADO CLIENTES $nom');
	$objFont=$objPHPExcel->getActiveSheet()->getStyle('A1')->getFont();
	$objFont->setName('Courier New'); 
	$objFont->setSize(15); 
	$objFont->setBold(true); 
	$objFont->setUnderline(PHPExcel_Style_Font::UNDERLINE_SINGLE);
	$objFont->getColor()->setARGB( PHPExcel_Style_Color::COLOR_BLACK);
	$objAlign=$objPHPExcel->getActiveSheet()->getStyle('A1')->getAlignment(); 
	$objAlign->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER); 
	$objAlign->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER); 
	$objPHPExcel->setActiveSheetIndex(0)
			->setCellValue('A2', 'CODIGO CLIENTE')
			->setCellValue('B2', 'CODIGO CATASTRAL')
			->setCellValue('C2', 'DOCUMENTO')
			->setCellValue('D2', 'TERCERO')
			->setCellValue('E2', 'DIRECCION')
			->setCellValue('F2', 'BARRIO')
			->setCellValue('G2', 'ESTRATO')
			->setCellValue('H2', 'TOTAL FACTURAS')
			->setCellValue('I2', 'VALOR MORA');
	$linkbd=conectar_bd();
	$iter='saludo1a';
	$iter2='saludo2';
	if($_POST['select_servicio']!=''){$condi="AND servicio='".$_POST['select_servicio']."'";}
	else {$condi='';}
	$sqlr="SELECT *FROM servtempmorosos WHERE  totalfacturas > 1 $condi ORDER BY totalfacturas DESC";
	$res=mysqli_query($linkbd,$sqlr);
	$resp = mysql_query($sqlr,$linkbd);
	$i=3;
	while ($row =mysql_fetch_row($resp))
	{
		$barrio=$estrato="";
		$sqlr01="SELECT nombre FROM servbarrios WHERE id='$row[6]'";
		$res01=mysql_query($sqlr01,$linkbd);
		$row01 =mysql_fetch_row($res01);
		$barrio=$row01[0];
		$sqlr01="SELECT tipo, descripcion FROM servestratos WHERE id='$row[7]'";
		$res01=mysql_query($sqlr01,$linkbd);
		$row01 =mysql_fetch_row($res01);
		$estrato="$row01[0] - $row01[1]";
		$objPHPExcel->setActiveSheetIndex(0)
		->setCellValueExplicit ("A".$i, str_pad($row[1],10,"0", STR_PAD_LEFT), PHPExcel_Cell_DataType :: TYPE_STRING);
		$objPHPExcel->setActiveSheetIndex(0)
		->setCellValueExplicit ("B".$i, str_pad($row[2],10,"0", STR_PAD_LEFT), PHPExcel_Cell_DataType :: TYPE_STRING);
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(2,$i,$row[3]);
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(3,$i,$row[4]);
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(4,$i,utf8_decode($row[5]));
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(5,$i,utf8_decode($barrio));
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(6,$i,$estrato);
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(7,$i,$row[11]);
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(8,$i,$row[10]);
		$i++;
	}
	//----Propiedades de la hoja
	$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setAutoSize(true); 
	$objPHPExcel->getActiveSheet()->setTitle('Listado Usuarios Morosos');
	//----Guardar documento----
	header('Content-Type: application/vnd.ms-excel');
	header('Content-Disposition: attachment;filename="Listado Usuarios Morosos.xls"');
	header('Cache-Control: max-age=0');
	$objWriter=PHPExcel_IOFactory::createWriter($objPHPExcel,'Excel5');
	$objWriter->save('php://output');
	exit;
?>