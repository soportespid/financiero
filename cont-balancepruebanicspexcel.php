<?php
require_once 'PHPExcel/Classes/PHPExcel.php';
require "comun.inc";
require "funciones.inc";
ini_set('max_execution_time',99999999);
session_start();
$objPHPExcel = new PHPExcel();
$linkbd=conectar_v7();
$sqlr="select *from configbasica where estado='S'";
//echo $sqlr;
$res=mysqli_query($linkbd, $sqlr);
while($row=mysqli_fetch_row($res))
{
    $nit=$row[0];
    $rs=$row[1];
}

//----Propiedades----
$objPHPExcel->getProperties()
        ->setCreator("IDEAL 10")
        ->setLastModifiedBy("IDEAL 10")
        ->setTitle("Exportar Excel con PHP")
        ->setSubject("Documento de prueba")
        ->setDescription("Documento generado con PHPExcel")
        ->setKeywords("usuarios phpexcel")
        ->setCategory("reportes");

//----Cuerpo de Documento----
$objPHPExcel->getActiveSheet()->mergeCells('A1:F1');
$objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue('A1', $rs);
$objFont=$objPHPExcel->getActiveSheet()->getStyle('A1')->getFont();
$objFont->setName('Arial');
$objFont->setSize(16);
$objFont->setBold(true);
$objFont->getColor()->setARGB( PHPExcel_Style_Color::COLOR_WHITE);
$objAlign=$objPHPExcel->getActiveSheet()->getStyle('A1')->getAlignment();
$objAlign->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$objAlign->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);

$objPHPExcel->getActiveSheet()->mergeCells('A2:F2');
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('A2', $nit);
$objFont=$objPHPExcel->getActiveSheet()->getStyle('A2')->getFont();
$objFont->setName('Arial');
$objFont->setSize(12);
$objFont->setBold(true);
$objFont->getColor()->setARGB( PHPExcel_Style_Color::COLOR_WHITE);
$objAlign=$objPHPExcel->getActiveSheet()->getStyle('A2')->getAlignment();
$objAlign->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$objAlign->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);

$objPHPExcel->getActiveSheet()->mergeCells('A3:F3');
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('A3', 'Balance de prueba');
$objFont=$objPHPExcel->getActiveSheet()->getStyle('A3')->getFont();
$objFont->setName('Arial');
$objFont->setSize(12);
$objFont->setBold(true);
$objFont->getColor()->setARGB( PHPExcel_Style_Color::COLOR_WHITE);
$objAlign=$objPHPExcel->getActiveSheet()->getStyle('A3')->getAlignment();
$objAlign->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$objAlign->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);

$objPHPExcel->getActiveSheet()->mergeCells('A4:F4');
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('A4', $_POST['fecha'].' - '.$_POST['fecha2']);
$objFont=$objPHPExcel->getActiveSheet()->getStyle('A4')->getFont();
$objFont->setName('Arial');
$objFont->setSize(12);
$objFont->setBold(true);
$objFont->getColor()->setARGB( PHPExcel_Style_Color::COLOR_WHITE);
$objAlign=$objPHPExcel->getActiveSheet()->getStyle('A4')->getAlignment();
$objAlign->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$objAlign->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);


$objPHPExcel-> getActiveSheet ()
-> getStyle ("A1:F4")
-> getFill ()
-> setFillType (PHPExcel_Style_Fill :: FILL_SOLID)
-> getStartColor ()
-> setRGB ('61CBF3');

$objPHPExcel-> getActiveSheet ()
-> getStyle ("A5:F5")
-> getFill ()
-> setFillType (PHPExcel_Style_Fill :: FILL_SOLID)
-> getStartColor ()
-> setRGB ('4D93D9');

$borders = array(
    'borders' => array(
        'allborders' => array(
            'style' => PHPExcel_Style_Border::BORDER_THIN,
        'color' => array('argb' => 'FF000000'),
        )
    ),
);
$bordersTitle = array(
    'borders' => [
        'top' => [
            'style' => PHPExcel_Style_Border::BORDER_THICK,
            'color' => ['argb' => 'FF000000'],
        ],
        'bottom' => [
            'style' => PHPExcel_Style_Border::BORDER_THICK,
            'color' => ['argb' => 'FF000000'],
        ],
        'left' => [
            'style' => PHPExcel_Style_Border::BORDER_THIN,
            'color' => ['argb' => 'FF000000'],
        ],
        'right' => [
            'style' => PHPExcel_Style_Border::BORDER_THIN,
            'color' => ['argb' => 'FF000000'],
        ],
    ],
    'font' => array(
        'bold' => true,
        'size' => 12,
        'color' => array('argb' => 'ffffffff'),
    ),
);


$objPHPExcel->getActiveSheet()->getStyle('A5:F5')->applyFromArray($bordersTitle);

$objPHPExcel->setActiveSheetIndex(0)
->setCellValue('A5', 'CUENTA')
->setCellValue('B5', 'NOMBRE CUENTA')
->setCellValue('C5', 'SALDO INICIAL')
->setCellValue('D5', 'DEBITOS')
->setCellValue('E5', 'CREDITOS')
->setCellValue('F5', 'SALDO FINAL')
;

$i = 6;


for ($x = 0; $x < count($_POST['dcuentas']); $x++) {

    $saldoant = floatval(str_replace(',', '.', str_replace('.', '', $_POST['dsaldoant'][$x])));
    $debitos = floatval(str_replace(',', '.', str_replace('.', '', $_POST['ddebitos'][$x])));
    $creditos = floatval(str_replace(',', '.', str_replace('.', '', $_POST['dcreditos'][$x])));
    $saldo = floatval(str_replace(',', '.', str_replace('.', '', $_POST['dsaldo'][$x])));

    $objPHPExcel->setActiveSheetIndex(0)
        ->setCellValueExplicit("A$i", $_POST['dcuentas'][$x], PHPExcel_Cell_DataType::TYPE_NUMERIC)
        ->setCellValueExplicit("B$i", $_POST['dncuentas'][$x], PHPExcel_Cell_DataType::TYPE_STRING)
        ->setCellValueExplicit("C$i", $saldoant, PHPExcel_Cell_DataType::TYPE_NUMERIC)
        ->setCellValueExplicit("D$i", $debitos, PHPExcel_Cell_DataType::TYPE_NUMERIC)
        ->setCellValueExplicit("E$i", $creditos, PHPExcel_Cell_DataType::TYPE_NUMERIC)
        ->setCellValueExplicit("F$i", $saldo, PHPExcel_Cell_DataType::TYPE_NUMERIC);

    // Aplica bordes a las celdas
    $objPHPExcel->getActiveSheet()->getStyle("A$i:F$i")->applyFromArray($borders);

    // Verificar si la longitud de dcuentas es menor a 9
    if (strlen($_POST['dcuentas'][$x]) < 9) {
        // Aplica negrita a toda la fila de la columna A a F
        $objPHPExcel->getActiveSheet()->getStyle("A$i:F$i")->applyFromArray([
            'font' => [
                'bold' => true
            ]
        ]);
    }
    $objPHPExcel->getActiveSheet()->getStyle('A5:F' . $i)->getFont()->setSize(10);
    $objPHPExcel->getActiveSheet()->getStyle('C5:F' . $i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
    $objPHPExcel->getActiveSheet()->getStyle('A1:F' . $i )->applyFromArray([
        'borders' => [
            'left' => [
                'style' => PHPExcel_Style_Border::BORDER_THICK,
                'color' => ['argb' => 'FF000000'],
            ],
            'right' => [
                'style' => PHPExcel_Style_Border::BORDER_THICK,
                'color' => ['argb' => 'FF000000'],
            ],
        ],
    ]);
    $objPHPExcel->getActiveSheet()->getStyle('A5:F' . $i)->applyFromArray([
        'font' => [
            'name' => 'Arial',
            'size' => 9,
        ],
    ]);

    $objPHPExcel->getActiveSheet()->getStyle("C$i:F$i")->getNumberFormat()
    ->setFormatCode('#,##0.00');


    $i++;
}


//----Propiedades de la hoja
$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->setTitle('BALANCE DE PRUEBA');
$objPHPExcel->setActiveSheetIndex(0);

//----Guardar documento----
header('Content-Type: application/vnd.ms-excel');
header('Content-Disposition: attachment;filename="Teso-balanceprueba.xls"');
header('Cache-Control: max-age=0');

$objWriter=PHPExcel_IOFactory::createWriter($objPHPExcel,'Excel5');
$objWriter->save('php://output');
exit;

?>
