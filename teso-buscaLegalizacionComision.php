<?php

    header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=utf8");
	require 'comun.inc';
	require 'funciones.inc';
	session_start();
    if(empty($_SESSION)){
        header("location: index.php");
    }
	$linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");
	cargarcodigopag($_GET['codpag'],$_SESSION["nivel"]);
	date_default_timezone_set("America/Bogota");
?>
<!DOCTYPE >
<html lang=es>
	<head>
    <meta charset="utf-8"/>
		<meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1"/>
		<meta name="viewport" content="user-scalable=no">
		<title>:: IDEAL 10 - Tesoreria</title>
		<link href="favicon.ico" rel="shortcut icon"/>
		<link href="css/css2z.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
		<link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
		<link href="css/css4.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
        <link href="css/tabs.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css"/>
        <link href="css/style.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css"/>
        <link rel="stylesheet" href="sweetalert2/dist/sweetalert2.min.css">
        <script type="text/javascript" src="css/programas.js"></script>
        <script src="sweetalert2/dist/sweetalert2.min.js"></script>
		<script src="vue/vue.min.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script src="css/programas.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script src="css/calendario.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
	</head>
	<body>
		<header>
			<table>
				<tr><script>barra_imagenes("teso");</script><?php cuadro_titulos();?></tr>
			</table>
		</header>

        <main id="myapp" v-cloak>
            <input type="hidden" value = "2" ref="pageType">
            <div class="loading-container" v-show="isLoading" >
                <p class="text-loading"data-loading="IDEAL 10">IDEAL 10</p>
            </div>
            <nav>
                <table>
                    <tr><?php menu_desplegable("teso");?></tr>
                </table>
                <div class="bg-white group-btn p-1" id="newNavStyle">
                    <button type="button" class="btn btn-white btn-primary-hover d-flex justify-between align-items-center" @click="location.href='teso-creaLegalizacionComision'">
                        <span>Nuevo</span>
                        <svg viewBox="0 -960 960 960" ><path d="M440-440H200v-80h240v-240h80v240h240v80H520v240h-80v-240Z"/></svg>
                    </button>
                    <button type="button" class="btn btn-white btn-primary-hover d-flex justify-between align-items-center" @click="window.location.reload()">
                        <span>Buscar</span>
                        <svg class="fill-black group-hover:fill-white w-5 h-5" viewBox="0 -960 960 960" ><path d="M784-120 532-372q-30 24-69 38t-83 14q-109 0-184.5-75.5T120-580q0-109 75.5-184.5T380-840q109 0 184.5 75.5T640-580q0 44-14 83t-38 69l252 252-56 56ZM380-400q75 0 127.5-52.5T560-580q0-75-52.5-127.5T380-760q-75 0-127.5 52.5T200-580q0 75 52.5 127.5T380-400Z"/></svg>
                    </button>
                    <button type="button" class="btn btn-white btn-primary-hover d-flex justify-between align-items-center" @click="mypop=window.open('teso-principal.php','',''); mypop.focus();">
                        <span>Nueva ventana</span>
                        <svg class="fill-black group-hover:fill-white w-5 h-5" viewBox="0 -960 960 960" ><path d="M320-120v-80h80v-80H160q-33 0-56.5-23.5T80-360v-400q0-33 23.5-56.5T160-840h640q33 0 56.5 23.5T880-760v400q0 33-23.5 56.5T800-280H560v80h80v80H320ZM160-360h640v-400H160v400Zm0 0v-400 400Z"/></svg>
                    </button>
                </div>
            </nav>
            <section class="bg-white">
                <div>
                    <h2 class="titulos m-0">Busca legalización de comisiones</h2>
                </div>

                <div class="d-flex">
                    <div class="form-control">
                        <label class="form-label m-0" for="">Buscar:</label>
                        <input type="search" v-model="txtSearch" @keyup="modalFilter('search')" placeholder="Buscar por consecutivo, cod obligación o beneficiario">
                    </div>
                </div>

                <div class="table-responsive overflow-auto" style="height:55vh">
                    <table class="table table-hover fw-normal">
                        <thead>
                            <tr class="text-center">
                                <th>Consecutivo</th>
                                <th>Fecha</th>
                                <th>Cosec obligación</th>
                                <th>Valor obligación</th>
                                <th>Valor legalización</th>
                                <th>Detalle</th>
                                <th>Beneficiario Obligación</th>
                                <th>Estado</th>
                                <th>Opción</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr v-for="(data,index) in arrSearch" :key="index" @dblclick="window.location.href='teso-visualizarLegalizacionComision?id='+data.consecutivo">
                                <td class="text-center">{{ data.consecutivo }}</td>                    
                                <td class="text-center">{{ formatFecha(data.fecha) }}</td>
                                <td class="text-center">{{ data.cod_obligacion }}</td>
                                <td class="text-right">{{ viewFormatNumber(data.valor_obligacion) }}</td>
                                <td class="text-right">{{ viewFormatNumber(data.valor_legalizacion) }}</td>
                                <td>{{ data.detalle }}</td>
                                <td>{{ data.tercero }} - {{ data.nomTercero }}</td>
                                <td class="text-center">
                                    <span :class="data.estado == 'S' ? 'badge-success' : 'badge-danger'" class="badge">
                                        {{ (data.estado == 'S') ? "Activo" : ((data.estado == 'N') ? "Anulado" : "Reversado") }}
                                    </span>
                                </td>
                                <td v-if="data.fecha > fechaBloqueo" class="text-center">
                                    <button type="button" class="btn btn-danger btn-sm m-0" @click="anulaDoc(index, data.consecutivo, data.fecha)">Anular</button>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </section>
        </main>
		<script src="Librerias/vue/vue.min.js"></script>
		<script type="module" src="tesoreria/legalizacion_comision/js/legalizacion_comision_functions.js"></script>
	</body>
</html>
