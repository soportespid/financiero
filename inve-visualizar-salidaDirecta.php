<?php
	header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=utf8");
	require "comun.inc";
	require "funciones.inc";
    require "conversor.php";
	session_start();
	$linkbd = conectar_v7();
    $linkbd -> set_charset("utf8");
	cargarcodigopag($_GET['codpag'],$_SESSION["nivel"]);
	date_default_timezone_set("America/Bogota");
	//error_reporting(E_ALL);
?>
<!DOCTYPE >
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="es">
	<head>
		<meta http-equiv="Content-type" content="text/html; charset=iso-8859-1"/>
		<meta http-equiv="X-UA-Compatible" content="IE=9"/>
		<title>:: IDEAL 10 - Almacen</title>
		<link href="css/css2.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<link href="css/css4.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
        <link href="css/cssSP.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<script type="text/javascript" src="css/programas.js"></script>
        <script type="text/javascript" src="css/calendario.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script src="sweetalert2/dist/sweetalert2.min.js"></script>
		<link rel="stylesheet" href="sweetalert2/dist/sweetalert2.min.css">

		<script>
            function pdf(){
				document.form2.action = "inve-pdfSalidaDirecta.php";
				document.form2.target = "_BLANK";
				document.form2.submit(); 
				document.form2.action = "";
				document.form2.target = "";
			}
		</script>

		<?php titlepag();?>
	</head>
	<body>
		<IFRAME src="alertas.php" name="alertas" id="alertas" style="display:none"></IFRAME>
		<span id="todastablas2"></span>
		<table>
			<tr><script>barra_imagenes("inve");</script><?php cuadro_titulos();?></tr>

			<tr><?php menu_desplegable("inve");?></tr>

			<tr>
				<td colspan="3" class="cinta">
                    <a href="inve-salidaDirecta.php" class="mgbt"><img src="imagenes/add.png"/></a>
					<a class="mgbt"><img src="imagenes/guardad.png" title="Guardar"/></a>
					<a href="inve-buscar-salidasDirectas.php" class="mgbt"><img src="imagenes/busca.png" title="Buscar"/></a>
					<a onclick="mypop=window.open('plan-agenda.php','','');mypop.focus()" class="mgbt"><img src="imagenes/agenda1.png" title="Agenda" /></a>
					<a onclick="mypop=window.open('inve-principal.php','','');mypop.focus();" class="mgbt"><img src="imagenes/nv.png" title="Nueva ventana"></a>
					<a onclick="mypop=window.open('<?php echo $url2; ?>','','');mypop.focus();" class="mgbt"><img src="imagenes/duplicar_pantalla.png" title="Duplicar pesta&ntilde;a"></a>
                    <a onclick="pdf()"><img src="imagenes/print.png" title="Imprimir" class="mgbt"></a>
                    <a href="inve-buscar-salidasDirectas.php" class="mgbt"><img src="imagenes/iratras.png" title="Atrás"></a>
                </td>
			</tr>
		</table>

		<div id="bgventanamodalm" class="bgventanamodalm">
			<div id="ventanamodalm" class="ventanamodalm">
				<IFRAME src="" name="ventanam" marginWidth=0 marginHeight=0 frameBorder=0 id="ventanam" frameSpacing=0 style=" width:700px; height:130px; top:200; overflow:hidden;"></IFRAME>
			</div>
		</div>

		<form name="form2" method="post" action="">
			<?php 
                $vigusu=vigencia_usuarios($_SESSION['cedulausu']);

				if(@$_POST['oculto']=="")
				{
                    $consecutivo = $_GET['consec'];
                    $tipomov = $_GET['mov'];
                    
                    $sqlInventario = "SELECT consec, fecha, nombre, usuario, vigenciadoc, tipomov, tiporeg FROM almginventario WHERE consec = $consecutivo AND tipomov = '$tipomov' AND tiporeg = '06' AND estado = 'S' ";
                    $rowInventario = mysqli_fetch_row(mysqli_query($linkbd, $sqlInventario));

                    $sqlUsuarios = "SELECT usu_usu FROM usuarios WHERE cc_usu = '$rowInventario[3]'";
                    $rowUsuarios = mysqli_fetch_row(mysqli_query($linkbd, $sqlUsuarios));

                    $_POST['tipoMov'] = $rowInventario[5].$rowInventario[6];
                    $_POST['consec'] = $rowInventario[0];
                    $_POST['fecha'] = date('d-m-Y',strtotime($rowInventario[1]));
                    $_POST['vigencia'] = $rowInventario[4];
                    $_POST['realiza'] = $rowUsuarios[0];
                    $_POST['descripcion'] = $rowInventario[2]; 


                    $sqlInventarioDet = "SELECT codart, unspsc, cantidad_salida, unidad, bodega, cc, codcuentacre, valorunit, valortotal, LEFT(codart, 4), RIGHT(codart, 5), cantidad_entrada FROM almginventario_det WHERE codigo = $consecutivo AND tipomov = '$tipomov' AND tiporeg = '06'";
                    $resInventarioDet = mysqli_query($linkbd, $sqlInventarioDet);
				}				
			?>
            <div>
                <table class="inicio ancho">
                    <tr>
                        <td class="titulos" colspan="8">.: Datos de salida Directa</td>
                        <td class="cerrar" style="width:7%" onClick="location.href='inve-principal.php'">Cerrar</td>
                    </tr>

                    <tr>
                        <td class="tamano01">Tipo Movimiento: </td>
                        <td>
                            <input type="text" name="tipoMov" id="tipoMov" value="<?php echo $_POST['tipoMov'] ?>" style="text-align: center;" readonly>
                        </td>

                        <td class="tamano01">Consecutivo: </td>
                        <td>
                            <input type="text" name="consec" id="consec" value="<?php echo $_POST['consec'] ?>" style="text-align: center;" readonly>
                        </td>

                        <td class="tamano01">Fecha: </td>
                        <td>
                            <input type="text" name="fecha" id="fecha" value="<?php echo $_POST['fecha'] ?>" style="text-align: center;" readonly>
                        </td>

                        <td class="tamano01">Vigencia: </td>
                        <td>
                            <input type="text" name="vigencia" id="vigencia" value="<?php echo $_POST['vigencia'] ?>" style="text-align: center;" readonly>
                        </td>
                    </tr>

                    <tr>
                        <td class="tamano01">Realiza: </td>
                        <td>
                            <input type="text" name="realiza" id="realiza" value="<?php echo $_POST['realiza'] ?>" style="text-align: center;" readonly>
                        </td>

                        <td class="tamano01">Descripción: </td>
                        <td colspan="5">
                            <input type="text" name="descripcion" id="descripcion" value="<?php echo $_POST['descripcion'] ?>" style="width: 100%;" readonly>
                        </td>
                    </tr>
                </table>

                <div class="subpantalla" style="height:50%; width:99.5%; float:left; overflow-x:hidden;">
					<table class="inicio grande">
                        <tr>
                            <td class="titulos" colspan="20">Detalles salida directa: </td>
                        </tr>

                        <tr class="titulos2" style='height:30px;'>
							<td>Cod articulo </td>							
							<td>Nombre Articulo </td>
                            <td>Cantidad entrada </td>
							<td>Cantidad salida </td>
							<td>Unidad medida </td>
							<td>Bodega </td>
							<td>CC articulo </td>
                            <td>Cod cuenta </td>
                            <td>Nombre cuenta </td>
                            <td>CC salida </td>
                            <td>Valor unitario </td>
                            <td>Valor total </td>
						</tr>

                    <?php
                        $iter = 'saludo1a';
                        $iter2 = 'saludo2';
                        $total = 0;

                        while ($rowInventarioDet = mysqli_fetch_row($resInventarioDet)) {

                            $sqlArticulos = "SELECT nombre FROM almarticulos WHERE codigo = $rowInventarioDet[10] AND grupoinven = $rowInventarioDet[9]";
                            $rowArticulos = mysqli_fetch_row(mysqli_query($linkbd, $sqlArticulos));

                            $sqlBodegas = "SELECT nombre FROM almbodegas WHERE id_cc = $rowInventarioDet[4]";
                            $rowBodegas = mysqli_fetch_row(mysqli_query($linkbd, $sqlBodegas));

                            $sqlCuentaCre = "SELECT nombre FROM cuentasnicsp WHERE cuenta = $rowInventarioDet[6]";
                            $rowCuentaCre = mysqli_fetch_row(mysqli_query($linkbd, $sqlCuentaCre));

                            $total += round($rowInventarioDet[8], 2);

                            echo "
							    <input type='hidden' name='codunsd[]' value='$rowInventarioDet[1]'>
                                <input type='hidden' name='codinard[]' value='$rowInventarioDet[0]'>
                                <input type='hidden' name='nomartd[]' value='$rowArticulos[0]'>
                                <input type='hidden' name='cantidadd[]' value='$rowInventarioDet[2]'>
                                <input type='hidden' name='cantidade[]' value='$rowInventarioDet[11]'>
                                <input type='hidden' name='unidadd[]' value='$rowInventarioDet[3]'>
                                <input type='hidden' name='valunit[]' value='$rowInventarioDet[7]'>
                                <input type='hidden' name='valtotal[]' value='$rowInventarioDet[8]'>
                            ";
                    ?>
                            <tr class='<?php echo $iter ?>' style='text-transform:uppercase;' >
                                <td> <?php echo $rowInventarioDet[0] ?> </td>
                                <td> <?php echo $rowArticulos[0] ?> </td>
                                <td> <?php echo $rowInventarioDet[11] ?> </td>
                                <td> <?php echo $rowInventarioDet[2] ?> </td>
                                <td> <?php echo $rowInventarioDet[3] ?> </td>
                                <td> <?php echo $rowBodegas[0] ?> </td>
                                <td> <?php echo $rowInventarioDet[5] ?> </td>
                                <td> <?php echo $rowInventarioDet[6] ?> </td>
                                <td> <?php echo $rowCuentaCre[0] ?> </td>
                                <td> <?php echo $rowInventarioDet[5] ?> </td>
                                <td> <?php echo "$".number_format($rowInventarioDet[7],2) ?> </td>
                                <td> <?php echo "$".number_format($rowInventarioDet[8],2) ?> </td>
                            </tr>
                    <?php
                        echo "
                            <input type='hidden' name='totalart' value='$total'>
                        ";
                    
                        }
                    ?>
                    </table>
                </div>
            </div>

			<input type="hidden" name="oculto" id="oculto" value="1"/>
            <input type="hidden" name="bc" id="bc" value="1"/>
            <input type="hidden" name="estado" id="estado" value="<?php echo $_POST['estado'] ?>"/>
            <input type="hidden" name="letras" id="letras" value="<?php echo $_POST['letras'] ?>"/>
            <input type="hidden" name="cuentaBanco" id="cuentaBanco" value="<?php echo $_POST['cuentaBanco'] ?>"/>
            <input type="hidden" name="descripcion" id="descripcion" value="<?php echo $_POST['descripcion'] ?>"/>

			<?php 
				if(@$_POST['oculto']=="2")
				{

				}
			?>
		</form>
		<div id="bgventanamodal2">
			<div id="ventanamodal2">
				<IFRAME  src="" name="buster" marginWidth=0 marginHeight=0 frameBorder=0 id="ventana2" frameSpacing=0 style="left:500px; width:900px; height:500px; top:200;"></IFRAME>
			</div>
		</div>
	</body>
</html>