<?php
	header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=iso-8859-1");
	require "comun.inc";
	require "funciones.inc";
	session_start();
	$linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");
	cargarcodigopag($_GET['codpag'],$_SESSION["nivel"]);
	date_default_timezone_set("America/Bogota");
?>

<!DOCTYPE >
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="es">
	<head>
		<meta http-equiv="Content-type" content="text/html; charset=iso-8859-1"/>
		<meta http-equiv="X-UA-Compatible" content="IE=9"/>
		<title>:: IDEAL 10 - Servicios P&uacute;blicos</title>
		<link href="css/css2.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<link href="css/css4.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<link href="css/style.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css"/>
		<script type="text/javascript" src="css/programas.js"></script>

		<script>
			function despliegamodalm(_valor,_tip,mensa,pregunta)
			{
				document.getElementById("bgventanamodalm").style.visibility=_valor;

				if(_valor == "hidden")
				{
					document.getElementById('ventanam').src = "";
				}
				else
				{
					switch(_tip)
					{
						case "1":	document.getElementById('ventanam').src="ventana-mensaje1.php?titulos="+mensa;break;
						case "2":	document.getElementById('ventanam').src="ventana-mensaje3.php?titulos="+mensa;break;
						case "3":	document.getElementById('ventanam').src="ventana-mensaje2.php?titulos="+mensa;break;
						case "4":	document.getElementById('ventanam').src="ventana-consulta1.php?titulos="+mensa+"&idresp="+pregunta;break;	
					}
				}
			}

			function funcionmensaje()
			{
				document.location.href = "serv-estratos.php";
			}

			function respuestaconsulta(pregunta)
			{
				switch(pregunta)
				{
					case "1":	document.form2.oculto.value='2';
								document.form2.submit();
								break;
				}
			}

			function guardar()
			{
				var codigo = document.getElementById('codban').value;
				var nombre = document.getElementById('nomban').value;
				var tipoUso = document.getElementById('tipoUso').value;

				if (codigo.trim() != '' && nombre.trim() && tipoUso != '-1') 
				{
					despliegamodalm('visible','4','Esta Seguro de Guardar','1');
				}
				else 
				{
					despliegamodalm('visible','2','Falta informacion para crear el estrato');
				}
			}

			function cambiocheck()
			{
				if(document.getElementById('myonoffswitch').value == 'S')
				{
					document.getElementById('myonoffswitch').value = 'N';
				}
				else
				{
					document.getElementById('myonoffswitch').value = 'S';
				}
				document.form2.submit();
			}
		</script>

		<?php titlepag();?>

	</head>

	<body>
		<IFRAME src="alertas.php" name="alertas" id="alertas" style="display:none"></IFRAME>
		<span id="todastablas2"></span>
		<table>
			<tr><script>barra_imagenes("serv");</script><?php cuadro_titulos();?></tr>	 

			<tr><?php menu_desplegable("serv");?></tr>

			<tr>
				<td colspan="3" class="cinta">
					<a href="serv-estratos.php" class="mgbt"><img src="imagenes/add.png"/></a>

					<a onclick="guardar()" class="mgbt"><img src="imagenes/guarda.png" title="Guardar"/></a>

					<a href="serv-estratosbuscar.php" class="mgbt"><img src="imagenes/busca.png" title="Buscar"/></a>

					<a onclick="mypop=window.open('plan-agenda.php','','');mypop.focus()" class="mgbt"><img src="imagenes/agenda1.png" title="Agenda" /></a>

					<a onclick="mypop=window.open('serv-principal.php','','');mypop.focus();" class="mgbt"><img src="imagenes/nv.png" title="Nueva ventana"></a>

					<a onclick="mypop=window.open('<?php echo $url2; ?>','','');mypop.focus();" class="mgbt"><img src="imagenes/duplicar_pantalla.png" title="Duplicar pesta&ntilde;a"></a>
				</td>
			</tr>
		</table>

		<div id="bgventanamodalm" class="bgventanamodalm">
			<div id="ventanamodalm" class="ventanamodalm">
				<IFRAME src="" name="ventanam" marginWidth=0 marginHeight=0 frameBorder=0 id="ventanam" frameSpacing=0 style=" width:700px; height:130px; top:200; overflow:hidden;"></IFRAME>
			</div>
		</div>

		<form name="form2" method="post" action="serv-estratos.php">

			<?php 
				if(@$_POST['oculto'] == "")
				{
					$_POST['onoffswitch'] = "S";
				}
			?>

			<table class="inicio ancho">
				<tr>
					<td class="titulos" colspan="6">.: Ingresar Estrato</td>

					<td class="cerrar" style="width:7%" onClick="location.href='serv-principal.php'">Cerrar</td>
				</tr>

				<tr>
					<td class="tamano01" style="width:3cm;">C&oacute;digo:</td>

					<td style="width:15%;">
						<input type="text" name="codban" id="codban" onKeyUp="return tabular(event,this)" value="<?php echo @ $_POST['codban'];?>" style="width:100%;height:30px;text-align:center;"/>
					</td>

					<td class="tamano01" style="width:3cm;">Estrato:</td>

					<td colspan="3">
						<input type="text" name="nomban" id="nomban" value="<?php echo @ $_POST['nomban'];?>" style="width:100%;height:30px;text-transform:uppercase"/>
					</td>
				</tr>

				<tr>
					<td class="tamano01" width="3cm">Tipo de uso: </td>

					<td>
						<select name="tipoUso" id="tipoUso" style="width: 100%;">
							<option value="-1">:: Seleccione tipo de uso ::</option>
							<?php
								$sqlTipoUso = "SELECT id,nombre FROM srvtipo_uso WHERE estado = 'S' ";
								$resTipoUso = mysqli_query($linkbd,$sqlTipoUso);

								while ($rowTipoUso = mysqli_fetch_row($resTipoUso))
								{
									if($_POST['tipoUso'] == $rowTipoUso[0])
									{
										echo "<option value='$rowTipoUso[0]' SELECTED>$rowTipoUso[1]</option>";
									}
									else 
									{
										echo "<option value='$rowTipoUso[0]'>$rowTipoUso[1]</option>no";
									}
								}
							?>
						</select>
					</td>
			</table>

			<input type="hidden" name="oculto" id="oculto" value="1"/>

			<?php 
				if(@$_POST['oculto'] == "2")
				{
					$sqlr = " INSERT INTO srvestratos (id,descripcion,tipo,estado) VALUES ('".$_POST['codban']."','".$_POST['nomban']."','".$_POST['tipoUso']."','S') ";

					if (!mysqli_query($linkbd,$sqlr))
					{
						echo"
						<script>
							despliegamodalm('visible','2','No se pudo ejecutar la petici&oacute;n');
							</script>";
					}
					else 
					{
						echo "
						<script>
							despliegamodalm('visible','1','Se ha almacenado con Exito');
						</script>";
					}
				}
			?>
			
		</form>

		<div id="bgventanamodal2">
			<div id="ventanamodal2">
				<IFRAME  src="" name="buster" marginWidth=0 marginHeight=0 frameBorder=0 id="ventana2" frameSpacing=0 style="left:500px; width:900px; height:500px; top:200;"></IFRAME>
			</div>
		</div>
		
	</body>
</html>