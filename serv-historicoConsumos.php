<?php
	header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=utf8");
	require 'comun.inc';
	require 'funciones.inc';
	session_start();
	$linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");
	cargarcodigopag($_GET['codpag'],$_SESSION["nivel"]);
	date_default_timezone_set("America/Bogota");
?>
<!DOCTYPE >
<html lang=es>
	<head>
    <meta charset="utf-8"/>
		<meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1"/>
		<meta name="viewport" content="user-scalable=no">
		<title>:: IDEAL 10 - Servicios públicos</title>
		<link href="favicon.ico" rel="shortcut icon"/>
		<link href="css/css2z.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
		<link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
		<link href="css/css4.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
        <link href="css/tabs.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css"/>
        <link rel="stylesheet" href="sweetalert2/dist/sweetalert2.min.css">
        <script type="text/javascript" src="css/programas.js"></script>
        <script src="sweetalert2/dist/sweetalert2.min.js"></script>
		<script src="vue/vue.min.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script src="css/programas.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script src="css/calendario.js?<?php echo date('d_m_Y_h_i_s');?>"></script>

        <style>
            .checkbox-wrapper-31:hover .check {
                stroke-dashoffset: 0;
            }
        
            .checkbox-wrapper-31 {
                position: relative;
                display: inline-block;
                width: 40px;
                height: 40px;
            }
            .checkbox-wrapper-31 .background {
                fill: #ccc;
                transition: ease all 0.6s;
                -webkit-transition: ease all 0.6s;
            }
            .checkbox-wrapper-31 .stroke {
                fill: none;
                stroke: #fff;
                stroke-miterlimit: 10;
                stroke-width: 2px;
                stroke-dashoffset: 100;
                stroke-dasharray: 100;
                transition: ease all 0.6s;
                -webkit-transition: ease all 0.6s;
            }
            .checkbox-wrapper-31 .check {
                fill: none;
                stroke: #fff;
                stroke-linecap: round;
                stroke-linejoin: round;
                stroke-width: 2px;
                stroke-dashoffset: 22;
                stroke-dasharray: 22;
                transition: ease all 0.6s;
                -webkit-transition: ease all 0.6s;
            }
            .checkbox-wrapper-31 input[type=checkbox] {
                position: absolute;
                width: 100%;
                height: 100%;
                left: 0;
                top: 0;
                margin: 0;
                opacity: 0;
                -appearance: none;
            }
            .checkbox-wrapper-31 input[type=checkbox]:hover {
                cursor: pointer;
            }
            .checkbox-wrapper-31 input[type=checkbox]:checked + svg .background {
                fill: #6cbe45;
            }
            .checkbox-wrapper-31 input[type=checkbox]:checked + svg .stroke {
                stroke-dashoffset: 0;
            }
            .checkbox-wrapper-31 input[type=checkbox]:checked + svg .check {
                stroke-dashoffset: 0;
            }
        </style>
	</head>
	<body>
		<header>
			<table>
				<tr><script>barra_imagenes("serv");</script><?php cuadro_titulos();?></tr>
			</table>
		</header>

		<form name="form2" method="post" action="">
			<section id="myapp" v-cloak >
				<nav>
					<table>
						<tr><?php menu_desplegable("serv");?></tr>
						<tr>
							<td colspan="3" class="cinta">
								<img src="imagenes/add.png" v-on:click="location.href='serv-historicoConsumos'" class="mgbt" title="Nuevo">
								<img src="imagenes/guardad.png" title="Guardar"  class="mgbt">
								<img src="imagenes/buscad.png" v-on:click="location.href='serv-historicoConsumos'" class="mgbt" title="Buscar">
								<img src="imagenes/nv.png" onClick="mypop=window.open('serv-principal','',''); mypop.focus();" class="mgbt" title="Nueva Ventana">
                                <img src="imagenes/excel.png" title="Excel" class="mgbt" v-on:click="excel">
                                <a href="serv-menuInformes"><img src="imagenes/iratras.png" class="mgbt" alt="Atrás"></a>
							</td>
						</tr>
					</table>
				</nav>

				<article>
                    <div>
                        <table class="inicio">
                            <tr>
                                <td class="titulos" colspan="8">.: Historico de lecturas y consumos por usuario</td>
                            </tr>
                            
                            <tr>
                                <td class="textonew01" style="width:10%;">.: Usuario:</td>
                                <td style="width: 11%;">
                                    <input type="text" v-model="codUsuario" name="usuario" v-on:keyup.enter="buscarUsuario" style="text-align:center;">
                                </td>
                                <td style="width: 31%;">
                                    <textarea v-model="nameUsuario" style="width: 99%;" readonly></textarea>
                                </td>
                                <td>
                                    <input type="button" @click="buscarDatos" value="Buscar" />
                                </td>
                            </tr>
                        </table>
                        <div class='subpantalla' style='height:62vh; width:100%; margin-top:0px;  overflow-x:hidden'>
                            <table class='inicio'>        
                                <tbody>
                                    <tr>
                                    <th class="titulosnew00" style="width: 10%;">Codigo periodo</th>
                                        <th class="titulosnew00">Periodo liquidado</th>
                                        <th class="titulosnew00" style="width: 15%;">Servicio</th>
                                        <th class="titulosnew00" style="width: 10%;">Lectura</th>
                                        <th class="titulosnew00" style="width: 10%;">Consumo</th>
                                        <th class="titulosnew00" style="width: 25%;">Novedad</th>
                                    </tr>

                                    <tr v-for="(dato,index) in datos" v-bind:class="(index % 2 ? 'saludo1a' : 'saludo2')">
                                        <td style="text-align: center;">{{ dato[0] }}</td>
                                        <td style="text-align: center;">{{ dato[1] }}</td>
                                        <td style="text-align: center;">{{ dato[2] }}</td>
                                        <td style="text-align: center;">{{ dato[3] }}</td>
                                        <td style="text-align: center;">{{ dato[4] }}</td>
                                        <td style="text-align: center;">{{ dato[5] }}</td>

                                        <input type='hidden' name='codPeriodo[]' v-model="dato[0]">	
                                        <input type='hidden' name='periodo[]' v-model="dato[1]">	
                                        <input type='hidden' name='servicio[]' v-model="dato[2]">	
                                        <input type='hidden' name='lectura[]' v-model="dato[3]">	
                                        <input type='hidden' name='consumo[]' v-model="dato[4]">	
                                        <input type='hidden' name='novedad[]' v-model="dato[5]">	
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
				</article>
			</section>
		</form>

		<script src="node_modules/read-excel-file/bundle/read-excel-file.min.js"></script>
		<script src="node_modules/xlsx/dist/xlsx.full.min.js"></script>
        <script src="Librerias/vue/vue.min.js"></script>
		<script src="Librerias/vue/axios.min.js"></script>
		<script type="module" src="servicios_publicos/historicoConsumos/serv-historicoConsumos.js"></script>
        
	</body>
</html>