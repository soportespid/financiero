
const URL = './ejemplo-vue-back.php';
import { ordenarArbol, buscaNombreRubros } from './funciones.js'


const app = Vue.createApp({
    
    data() {
        return {
            ordenPresu: 1,
            
            loading: false,
            rubrosInicial: [],
            rubrosAd: [],
            rubrosJoin: [],
            rubrosTrasladosCred: [],
            accounts: [],
            accountsAux: [],
            accountsMayor: [],
            fuentes: [],
            arbol: [],
            arbolInv: [],
            rubrosInv: [],
            rubrosAdInv: [],
            rubrosTrasladosCredInv: [],
            nombreRubrosInv: [],
        }
    },

    mounted() {
        this.cargarParametros();
		this.loading = false;
	},

    computed: {
      
    },

    methods: {
        
        getParametros(name){
            //Captura parametros
            name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
            var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
            results = regex.exec(location.search);
            return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
        },

        async cargarParametros(){

            this.loading = true;
            
            await axios.post(URL)
            .then((response) => {
                /* if(response.data.rubrosInicial != null) */
                this.rubrosInicial = response.data.rubrosInicial;
                this.rubrosAd = response.data.rubrosAd;
                this.rubrosTrasladosCred = response.data.rubrosTrasladosCred;
                this.accounts = response.data.accounts;
                this.fuentes = response.data.fuentes;
                this.rubrosInv = response.data.rubrosInv;
                this.rubrosAdInv = response.data.rubrosAdInv;
                this.rubrosTrasladosCredInv = response.data.rubrosTrasladosCredInv;
                this.nombreRubrosInv = response.data.nombreRubrosInv;

            }).catch((error) => {
                this.error = true;
                console.log(error)
            }).finally(() => {
                this.loading = false;
                this.auxiliarMayor();
                this.orderAccounts();
            });

        },

        async orderAccounts(){

            //concatenar y filtrar los rubros que se utilizaron en el presupuesto inicial, adicion y traslados cred.
            const rubros = this.rubrosInicial.concat(this.rubrosAd, this.rubrosTrasladosCred);
            const dataArr = new Set(rubros);

            const nombresRubros = this.accountsAux.concat(this.fuentes);

            //this.rubrosJoin = buscaNombreRubros([...dataArr], this.accountsAux, this.fuentes);
            this.rubrosJoin = buscaNombreRubros([...dataArr], nombresRubros);

            this.arbol = await ordenarArbol(this.accountsMayor, this.rubrosJoin);

            const rubrosInv = this.rubrosInv.concat(this.rubrosAdInv, this.rubrosTrasladosCredInv);
            const dataArrInv = new Set(rubrosInv);

            const nombresRubrosInv = this.nombreRubrosInv.concat(this.fuentes);

            this.rubrosJoinInv = buscaNombreRubros([...dataArrInv], nombresRubrosInv);

            this.arbolInv = await ordenarArbol(this.nombreRubrosInv, this.rubrosJoinInv);

            console.log(this.arbolInv);

        },

        auxiliarMayor(){
            this.accountsAux = this.accounts.filter(element => element[2] == 'C');
            this.accountsMayor = this.accounts.filter(element => element[2] == 'A');
        },

        //metodos par aeste archivo


        guardarOrdenPresu(){

            Swal.fire({
                title: 'Esta seguro de guardar?',
                text: "Guardar orden presupuestal en la base de datos, confirmar campos!",
                icon: 'question',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Si, guardar!'
            }).then((result) => {
                if (result.isConfirmed) {
                    var formData = new FormData();

                    formData.append("orden", this.ordenPresu);
                
                    axios.post(URL + '?action=guardarOrdenPresu', formData)
                    .then((response) => {
                        
                        if(response.data.insertaBien){
                            Swal.fire({
                                position: 'top-end',
                                icon: 'success',
                                title: 'El orden presupuestal se guard&oacute; con Exito',
                                showConfirmButton: false,
                                timer: 1500
                            }).then((response) => {
                                    this.redireccionar();
                                });
                        }else{
                        
                            Swal.fire(
                                'Error!',
                                'No se pudo guardar.',
                                'error'
                            );
                        }
                        
                    });
                    
                }
            });
            
        },

        redireccionar(){
            location.href ="ccp-ordenpresupuestal.php";
        },

        toFormData(obj){
            var form_data = new FormData();
            for(var key in obj){
                form_data.append(key, obj[key]);
            }
            return form_data;
        },

        formatonumero: function(valor){
            return new Intl.NumberFormat("en-US", {style: "currency", currency: "USD"}).format(valor);
        },
    }
})

app.mount('#myapp')