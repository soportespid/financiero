<?php
header("Cache-control: no-cache, no-store, must-revalidate");
header("Content-Type: text/html;charset=utf8");
require 'comun.inc';
require 'funciones.inc';
session_start();
$linkbd = conectar_v7();
$linkbd->set_charset("utf8");
cargarcodigopag($_GET['codpag'], $_SESSION["nivel"]);
date_default_timezone_set("America/Bogota");
?>
<!DOCTYPE >
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="es">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1">
    <title>:: IDEAL 10 - Almacen</title>
    <link href="favicon.ico" rel="shortcut icon"/>
    <link href="css/css2.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css">
    <link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css">
    <script type="text/javascript" src="css/programas.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
    <link href="css/style.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css"/>
</head>

<body>
    <IFRAME src="alertas.php" name="alertas" id="alertas" style="display:none"></IFRAME>
    <span id="todastablas2"></span>
    <table>
        <tr><script>barra_imagenes("inve");</script><?php cuadro_titulos();?></tr>
        <tr><?php menu_desplegable("inve");?></tr>
    </table>
    <div class="bg-white group-btn p-1">
        <button type="button" onclick="window.open('inve-principal');"
            class="btn btn-white btn-primary-hover d-flex justify-between align-items-center">
            <span>Nueva ventana</span>
            <svg class="fill-black group-hover:fill-white w-5 h-5" xmlns="http://www.w3.org/2000/svg"
                viewBox="0 -960 960 960">
                <path
                    d="M320-120v-80h80v-80H160q-33 0-56.5-23.5T80-360v-400q0-33 23.5-56.5T160-840h640q33 0 56.5 23.5T880-760v400q0 33-23.5 56.5T800-280H560v80h80v80H320ZM160-360h640v-400H160v400Zm0 0v-400 400Z">
                </path>
            </svg>
        </button>
        <button type="button" onclick="mypop=window.open('/financiero/inve-menuactos.php','','');mypop.focus();"
            class="btn btn-white btn-primary-hover d-flex justify-between align-items-center">
            <span class="group-hover:text-white">Duplicar pantalla</span>
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 -960 960 960">
                <path d="M640-160v-360H160v360h480Zm80-200v-80h80v-360H320v200h-80v-200q0-33 23.5-56.5T320-880h480q33 0 56.5 23.5T880-800v360q0 33-23.5 56.5T800-360h-80ZM160-80q-33 0-56.5-23.5T80-160v-360q0-33 23.5-56.5T160-600h480q33 0 56.5 23.5T720-520v360q0 33-23.5 56.5T640-80H160Zm400-603ZM400-340Z">
                </path>
            </svg>
        </button>
    </div>
    <form name="form2" method="post" action="">
        <?php
        $accion = "INGRESO MENU DE ACTOS";
        $origen = getUserIpAddr();
        generaLogs($_SESSION["nickusu"], 'INV', 'V', $accion, $origen);
        ?>
        <table class="inicio ancho">
            <tr>
                <td class="titulos" width='100%'>.: Men&uacute; Actos de Inventario</td>
                <td class="boton02" onClick="location.href='inve-principal.php'">Cerrar</td>
            </tr>
            <tr>
                <td class='saludo1' colspan="2" width='100%'>
                    <ol id="lista2">
                        <li class='icoom' onClick="location.href='inve-actoAdministrativo'">Acto Administrativo General
                        </li>
                    </ol>
                </td>
            </tr>
        </table>
    </form>
</body>

</html>
