<?php
    require_once("tcpdf/tcpdf_include.php");
	require('comun.inc');
	require "funciones.inc";
    require 'funcionesSP.inc.php';
	date_default_timezone_set("America/Bogota");
	session_start();
	class MYPDF extends TCPDF
	{
		public function Header()
		{
			$linkbd = conectar_v7();
			$linkbd -> set_charset("utf8");
			$sqlr="select *from configbasica where estado='S' ";
			//echo $sqlr;
			$res=mysqli_query($linkbd, $sqlr);
			while($row=mysqli_fetch_row($res))
			{
				$nit=$row[0];
				$rs=$row[1];

			}
            define("MUNICIPIO",$rs);
			//Parte Izquierda
			$this->Image('imagenes/escudo.jpg',  13, 13, 20, 20, 'JPG', '', 'T', true, 300, '', false, false, 0, false, false, false);// Logo
			$this->SetFont('helvetica','B',8);
			$this->SetY(10);
			$this->RoundedRect(10, 10, 190, 25, 1,'');
			$this->Cell(0.1);
			$this->Cell(26,25,'','R',0,'L');
			$this->SetY(8);
			$this->SetX(80);
			$this->SetFont('helvetica','B',9);
			$this->Cell(50,15,strtoupper("$rs"),0,0,'C');
			$this->SetFont('helvetica','B',7);
			$this->SetY(12);
			$this->SetX(80);
			$this->Cell(50,15,'NIT: '.$nit,0,0,'C');
			//*****************************************************************************************************************************
			$this->SetFont('helvetica','B',9);
			$this->SetY(23);
			$this->SetX(36);
            $this->Cell(164,12,"CERTIFICADO DE INSCRIPCIÓN DE INDUSTRIA Y COMERCIO",'T',0,'C');


            $this->SetFont('helvetica','B',7);
			$this->SetY(10);
			$this->SetX(170);
			$this->Cell(30,7,"FECHA","L",0,'C');
			$this->SetY(17);
			$this->SetX(170);
            $this->SetFont('helvetica','B',9);
            $this->Cell(30,6,date("d/m/Y"),"L",0,'C');

			//**********************************************************
			$this->SetFont('times','B',10);
			$this->ln(12);
			//**********************************************************
		}
		public function Footer()
		{
			$linkbd = conectar_v7();
			$linkbd -> set_charset("utf8");
			$sqlr="SELECT direccion,telefono,web,email FROM configbasica WHERE estado='S'";
			$resp=mysqli_query($linkbd, $sqlr);
			$user = $_SESSION['nickusu'];
            $cedula = $_SESSION['cedulausu'];
			$fecha = date("Y-m-d H:i:s");
			$ip = $_SERVER['REMOTE_ADDR'];
			while($row=mysqli_fetch_row($resp))
			{
				$direcc=strtoupper($row[0]);
				$telefonos=$row[1];
				$dirweb=strtoupper($row[3]);
				$coemail=strtoupper($row[2]);
			}
			if($direcc!=''){$vardirec="Dirección: $direcc, ";}
			else {$vardirec="";}
			if($telefonos!=''){$vartelef="Telefonos: $telefonos";}
			else{$vartelef="";}
			if($dirweb!=''){$varemail="Email: $dirweb, ";}
			else {$varemail="";}
			if($coemail!=''){$varpagiw="Pagina Web: $coemail";}
			else{$varpagiw="";}
			$this->SetFont('helvetica', 'I', 8);
			$txt = <<<EOD
			$vardirec $vartelef
			$varemail $varpagiw
			EOD;
			$this->SetFont('helvetica', 'I', 6);
			$this->Cell(277,10,'','T',0,'T');
			$this->ln(2);
			$this->Write(0, $txt , '', 0, 'C', true, 0, false, false, 0);


            $sqlrcc="SELECT * FROM `usuarios` WHERE `cc_usu` = '$cedula'";
            $respcc=mysqli_query($linkbd, $sqlrcc);
            $rowcc=mysqli_fetch_row($respcc);
            //echo $rowcc[1];

			$this->Cell(50, 10, 'Hecho por: '.$rowcc[1], 00, false, 'C', 0, '', 0, false, 'T', 'M');
			$this->Cell(30, 10, 'Impreso por: '.$user, 0, false, 'C', 0, '', 0, false, 'T', 'M');
			$this->Cell(30, 10, 'IP: '.$ip, 0, false, 'C',0, '', 0, false, 'T', 'M');
			$this->Cell(30, 10, 'Fecha: '.$fecha, 0, false, 'C', 0, '', 0, false, 'T', 'M');
			$this->Cell(30, 10, 'IDEAL.10 S.A.S', 0, false, 'C', 0, '', 0, false, 'T', 'M');
            $this->Cell(20, 10, 'Pagina '.$this->getAliasNumPage().'/'.$this->getAliasNbPages(), 0, false, 'C', 0, '', 0, false, 'T', 'M');
		}
	}

	if($_GET['id']){
        $id = intval($_GET['id']);
        $linkbd = conectar_v7();
		$linkbd -> set_charset("utf8");
        $sql = "SELECT cedulanit,estado,matricula,DATE_FORMAT(fecha,'%d/%m/%Y') as fecha FROM tesorepresentantelegal WHERE id = $id";
        $request = mysqli_query($linkbd,$sql)->fetch_assoc();

        $cedula = $request['cedulanit'];
        $estado = $request['estado'];
        $arrFecha = explode("/",$request['fecha']);
        $matricula = $request['matricula'];

        $sql = "SELECT tipodoc,
        CASE WHEN razonsocial IS NULL OR razonsocial = ''
        THEN CONCAT(nombre1,' ',nombre2,' ',apellido1,' ',apellido2)
        ELSE razonsocial END AS nombre FROM terceros WHERE cedulanit = $cedula";
        $request = mysqli_query($linkbd,$sql)->fetch_assoc();
        $nombre = $request['nombre'];

        $sql="SELECT funcionario, nomcargo as cargo FROM firmaspdf_det WHERE idfirmas='5' AND estado ='S' AND fecha < '".date("Y-m-d")."'";
        $arrFuncionario = mysqli_query($linkbd,$sql)->fetch_assoc();

        $arrTipoDoc = array(
            12=>"Tarjeta de Identidad",
            21=>"Tarjeta de Extranjeria",
            11=>"Registro Civil de Nacimiento",
            13=>"Cedula de Ciudadania",
            22=>"Cedula Extranjera",
            31=>"NIT",
            41=>"Pasaporte"
        );
        $arrMeses = array(
            "01"=>"Enero","02"=> "Febrero","03"=> "Marzo", "04"=>"Abril", "05"=>"Mayo","06"=> "Junio",
            "07"=> "Julio", "08"=>"Agosto", "09"=>"Septiembre", "10"=>"Octubre", "11"=>"Noviembre", "12"=>"Diciembre"
        );
        $arrEstado = array(
            "S"=>" se encuentra matriculado con número ".$matricula." en la base de datos de industria y comercio de la alcadía desde el día ".$arrFecha[0]." del mes de ".$arrMeses[$arrFecha[1]]." del año ".$arrFecha[2],
            "A"=>" se encuentra matriculado con número ".$matricula." en la base de datos de industria y comercio de la alcadía desde el día ".$arrFecha[0]." del mes de ".$arrMeses[$arrFecha[1]]." del año ".$arrFecha[2],
            "N"=>" ha cancelado la matrícula con número ".$matricula." que se encontraba en la base de datos de industria y comercio de la alcadía desde el día ".$arrFecha[0]." del mes de ".$arrMeses[$arrFecha[1]]." del año ".$arrFecha[2]
        );
        $tipoDoc = $arrTipoDoc[$request['tipodoc']];
        $arrFechaExp = explode("/",date("d/m/Y"));
        $descripcion = "Certifica que el contribuyente ".$nombre." con ".$tipoDoc." ".$cedula." ".$arrEstado[$estado].".";
        $expide ="Se expide, el día ".$arrFechaExp[0]." del mes de ".$arrMeses[$arrFechaExp[1]]." del año ".$arrFechaExp[2].",";



        $pdf = new MYPDF('P','mm','Letter', true, 'iso-8859-1', false);
        $pdf->SetDocInfoUnicode (true);
        // set document information
        $pdf->SetCreator(PDF_CREATOR);
        $pdf->SetAuthor('IDEALSAS');
        $pdf->SetTitle('CERTIFICADO DE INSCRIPCIÓN DE INDUSTRIA Y COMERCIO');
        $pdf->SetSubject('CERTIFICADO DE INSCRIPCIÓN DE INDUSTRIA Y COMERCIO');
        $pdf->SetKeywords('TCPDF, PDF, example, test, guide');
        $pdf->SetMargins(10, 38, 10);// set margins
        $pdf->SetHeaderMargin(38);// set margins
        $pdf->SetFooterMargin(17);// set margins
        $pdf->SetAutoPageBreak(TRUE, 20);// set auto page breaks
        // set some language-dependent strings (optional)
        if (@file_exists(dirname(__FILE__).'/lang/spa.php'))
        {
            require_once(dirname(__FILE__).'/lang/spa.php');
            $pdf->setLanguageArray($l);
        }
        $pdf->AddPage();
        $pdf->setY(50);
        $pdf->ln();
        $pdf->SetFillColor(255,255,255);
        $pdf->SetFont('helvetica','B',12);
        $pdf->cell(190,4,strtoupper('Del '.MUNICIPIO),'',0,'C',1);
        $pdf->ln();
        $pdf->ln();
        $pdf->ln();
        $pdf->SetFont('helvetica','',12);
        $pdf->MultiCell(190,30,$descripcion,"",'L',true,0,'','',true,0,false,true,0,'T',true);
        $pdf->ln();
        $pdf->cell(95,4,$expide,'',0,'L',1);
        $pdf->ln();
        $pdf->ln();
        $pdf->ln();
        $pdf->ln();
        $pdf->ln();
        $pdf->ln();
        $pdf->SetFont('helvetica','B',8);
        $pdf->setX(55);
        $pdf->cell(100,4,"",'B',0,'C',1);
        $pdf->ln();
        $pdf->setX(55);
        $pdf->cell(100,4,$arrFuncionario['funcionario'],'',0,'C',1);
        $pdf->ln();
        $pdf->setX(55);
        $pdf->cell(100,4,$arrFuncionario['cargo'],'',0,'C',1);
        $pdf->ln();
        $pdf->Output('certificado_industria_y_comercio'.'.pdf', 'I');
    }
?>
