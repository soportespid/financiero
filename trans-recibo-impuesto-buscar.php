<?php

    header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=utf8");
	require 'comun.inc';
	require 'funciones.inc';
	session_start();
    if(empty($_SESSION)){
        header("location: index.php");
    }
	$linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");
	cargarcodigopag($_GET['codpag'],$_SESSION["nivel"]);
	date_default_timezone_set("America/Bogota");
?>
<!DOCTYPE >
<html lang=es>
	<head>
        <meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1"/>
		<meta name="viewport" content="user-scalable=no">
		<title>:: IDEAL 10 - Parametrización</title>
		<link href="favicon.ico" rel="shortcut icon"/>
		<link href="css/css2z.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
		<link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
		<link href="css/css4.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
        <link href="css/style.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css"/>
        <link rel="stylesheet" href="sweetalert2/dist/sweetalert2.min.css">
        <script src="sweetalert2/dist/sweetalert2.min.js"></script>
		<script src="css/programas.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
	</head>
	<body>
		<header>
			<table>
				<tr>
                    <script>barra_imagenes("trans");</script><?php cuadro_titulos();?>
                </tr>
			</table>
		</header>

        <section id="myapp" v-cloak>
            <!-- variables -->
            <input type="hidden" value = "2" ref="pageType">

            <!-- loading -->
            <div class="loading-container" v-show="isLoading" >
                <p class="text-loading"data-loading="IDEAL 10">IDEAL 10</p>
            </div>

            <nav>
                <table>
                    <tr><?php menu_desplegable("trans");?></tr>
                </table>
                <div class="bg-white group-btn p-1" id="newNavStyle">
                    <button type="button" class="btn btn-white btn-primary-hover d-flex justify-between align-items-center" @click="location.href='trans-recibo-impuesto-crear'">
                        <span>Nuevo</span>
                        <svg viewBox="0 -960 960 960" ><path d="M440-440H200v-80h240v-240h80v240h240v80H520v240h-80v-240Z"/></svg>
                    </button>
                    <button type="button" class="btn btn-white btn-primary-hover d-flex justify-between align-items-center" @click="mypop=window.open('trans-principal','',''); mypop.focus();">
                        <span>Nueva ventana</span>
                        <svg class="fill-black group-hover:fill-white w-5 h-5" viewBox="0 -960 960 960" ><path d="M320-120v-80h80v-80H160q-33 0-56.5-23.5T80-360v-400q0-33 23.5-56.5T160-840h640q33 0 56.5 23.5T880-760v400q0 33-23.5 56.5T800-280H560v80h80v80H320ZM160-360h640v-400H160v400Zm0 0v-400 400Z"/></svg>
                    </button>
                </div>
            </nav>
            <article class="bg-white">
                <div>
                    <div>
                        <h2 class="titulos m-0">Buscar - Recaudo impuesto vehicular</h2>
                        
                        <div class="d-flex">
                            <div class="form-control">
                                <label class="form-label m-0" for="">Buscar</label>
                                <input type="text" v-model="txtSearch" placeholder="Buscar por codigo placa o propietario">
                            </div>

                            <div class="form-control w-25">
                                <label class="form-label m-0" for="">Por página:</label>
                                <select class="text-center" v-model="selectPorPagina">
                                    <option value="25">25</option>
                                    <option value="50">50</option>
                                    <option value="100">100</option>
                                    <option value="250">250</option>
                                    <option value="500">500</option>
                                    <option value="1000">1000</option>
                                    <option value="">Todo</option>
                                </select>
                            </div>

                            <div class="form-control justify-between text-center w-25">
                                <label class="form-label m-0" for=""></label>
                                
                                    <button type="button" @click="getDataSearch()" class="btn btn-primary">Buscar</button>
                                </div>
                            </div>
                        </div>
                    </div>                       
                </div>

                <div class="table-responsive overflow-auto" style="height:60vh">
                    <table class="table table-hover fw-normal">
                        <thead>
                            <tr class="text-center">
                                <th>Código</th>
                                <th>Fecha tramite</th>
                                <th>Código liquidación</th>
                                <th>Placa</th>
                                <th>Propietario</th>
                                <th>Medio de pago</th>
                                <th>Cuenta bancaria</th>
                                <th>Valor</th>
                                <th>Detalle</th>
                                <th>Estado</th>
                                <th>Opción</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr v-for="(data,index) in arrDataSearch" :key="index" class="text-center" @dblclick="window.location.href='trans-recibo-impuesto-visualizar?id='+data.id">
                                <td>{{ data.id }}</td>
                                <td>{{ formatFecha(data.fecha) }}</td>
                                <td>{{ data.liquidacion_id }}</td>
                                <td>{{ data.placa }}</td>
                                <td>{{ data.propietario }} - {{ data.nombre_propietario }}</td>
                                <td>{{ data.medio_pago }}</td>
                                <td>{{ data.cuenta_bancaria }}</td>
                                <td>{{ viewFormatNumber(data.valor_recaudo) }}</td>
                                <td class="text-left">{{ data.detalle }}</td>
                                <td class="text-center">
                                    <span :class="data.estado == 'S' ? 'badge-success' : 'badge-danger'" class="badge">
                                        {{ (data.estado == 'S') ? "Activo" : ((data.estado == 'N') ? "Anulado" : "Reversado") }}
                                    </span>
                                </td>
                                <td v-if="data.fecha > fechaMin" class="text-center">
                                    <button type="button" class="btn btn-danger btn-sm m-0" @click="anulaRecaudo(index, data.id)">Anular</button>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>

                <div v-if="arrDataSearch.length > 0" class="list-pagination-container">
                    <p>Página {{ paginaActual }} de {{ intTotalPaginas }}</p>
                    <ul class="list-pagination">
                        <li v-show="paginaActual > 1" @click="getDataSearch(paginaActual = 1)"><< </li>
                        <li v-show="paginaActual > 1" @click="getDataSearch(--paginaActual)"><</li>
                        <li v-for="(pagina,index) in arrBotones" :class="paginaActual == pagina ? 'active' : ''" @click="getDataSearch(pagina)" :key="index">{{pagina}}</li>
                        <li v-show="paginaActual < intTotalPaginas" @click="getDataSearch(++paginaActual)">></li>
                        <li v-show="paginaActual < intTotalPaginas" @click="getDataSearch(paginaActual = intTotalPaginas)" >>></li>
                    </ul>
                </div>

            </article>
        </section>

        <script src="Librerias/vue/vue.min.js"></script>
		<script src="Librerias/vue/axios.min.js"></script>
		<script type="module" src="transporte/js/functions_recibo_impuesto.js"></script>

	</body>
</html>
