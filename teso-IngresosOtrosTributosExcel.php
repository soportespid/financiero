<?php
	header("Content-Type: text/html;charset=utf-8");
    require_once 'PHPExcel/Classes/PHPExcel.php';
    require "comun.inc";
    require "funciones.inc";
    require 'funcionesSP.inc.php';
    session_start();
    class Articulo{
        private $linkbd;
        public function __construct() {
            $this->linkbd = conectar_v7();
            $this->linkbd->set_charset("utf8");
        }
        public function search(string $search){

            $sql="SELECT i.codigo, i.nombre,d.concepto,UPPER(i.tipoIngreso) as tipoingreso,i.terceros,d.cuentapres,i.is_tercero,is_cuenta FROM tesoingresos i
            INNER JOIN tesoingresos_det d
            ON i.codigo = d.codigo
            WHERE i.estado<> '' AND d.modulo = '4'
            AND d.vigencia = (SELECT MAX(vigencia) FROM tesoingresos_det WHERE codigo = i.codigo)
            AND (i.nombre LIKE '$search%' OR i.codigo LIKE '$search%')
            ORDER BY CAST(i.codigo AS INT) ASC";

            $request = mysqli_fetch_all(mysqli_query($this->linkbd,$sql),MYSQLI_ASSOC);
            $totalCount = count($request);
            $opcionTercero = [
                '' => 'OTROS',
                'D' => 'DEPARTAMENTAL',
                'M' => 'MUNICIPAL',
                'N' => 'NACIONAL',
            ];

            for ($i=0; $i < $totalCount ; $i++) {
                $isTercero = $request[$i]['is_tercero'] == 1 ? "TERCEROS" : "PROPIO";
                $request[$i]['terceros'] = $opcionTercero[$request[$i]['terceros']]."-".$isTercero;
                $request[$i]['is_presupuesto'] = $request[$i]['is_cuenta'] == 1 ? "No" : "Si";
                $request[$i]['estado_cuenta'] = $this->validCuenta($request[$i]['cuentapres']);
            }
            return $request;
        }

        public function validCuenta($cuenta){
            $sql = "SELECT * FROM cuentasingresosccpetseleccionadas WHERE codigo = '$cuenta'";
            $request = mysqli_fetch_all(mysqli_query($this->linkbd,$sql),MYSQLI_ASSOC);
            $estado = 1;
            if(empty($request)) $estado = 2;
            return $estado;
        }
    }
    if($_GET){
        $obj = new Articulo();
        $request = $obj->search($_GET['search']);
        $objPHPExcel = new PHPExcel();
        $objPHPExcel->getActiveSheet()->getStyle('A:G')->applyFromArray(
            array(
                'alignment' => array(
                    'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
                    'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER
                )
            )
        );
        $objPHPExcel->getProperties()
        ->setCreator("IDEAL 10")
        ->setLastModifiedBy("IDEAL 10")
        ->setTitle("Exportar Excel con PHP")
        ->setSubject("Documento de prueba")
        ->setDescription("Documento generado con PHPExcel")
        ->setKeywords("usuarios phpexcel")
        ->setCategory("reportes");

        //----Cuerpo de Documento----
        $objPHPExcel->setActiveSheetIndex(0)
        ->mergeCells('A1:g1')
        ->mergeCells('A2:g2')
        ->setCellValue('A1', 'TESORERÍA')
        ->setCellValue('A2', 'REPORTE OTROS TRIBUTOS');
        $objPHPExcel-> getActiveSheet ()
        -> getStyle ("A1")
        -> getFill ()
        -> setFillType (PHPExcel_Style_Fill :: FILL_SOLID)
        -> getStartColor ()
        -> setRGB ('C8C8C8');
        $objPHPExcel-> getActiveSheet ()
        -> getStyle ("A1:A2")
        -> getFont ()
        -> setBold ( true )
        -> setName ( 'Verdana' )
        -> setSize ( 10 )
        -> getColor ()
        -> setRGB ('000000');
        $objPHPExcel-> getActiveSheet ()
        -> getStyle ('A1:A2')
        -> getAlignment ()
        -> applyFromArray (array ( 'horizontal'  =>  PHPExcel_Style_Alignment :: HORIZONTAL_CENTER ,) );
        $objPHPExcel-> getActiveSheet ()
        -> getStyle ('A3:g3')
        -> getAlignment ()
        -> applyFromArray (array ( 'horizontal'  =>  PHPExcel_Style_Alignment :: VERTICAL_JUSTIFY ,) );
        $objPHPExcel-> getActiveSheet ()
        -> getStyle ("A2")
        -> getFill ()
        -> setFillType (PHPExcel_Style_Fill :: FILL_SOLID);

        $borders = array(
            'borders' => array(
                'allborders' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN,
                'color' => array('argb' => 'FF000000'),
                )
            ),
        );
        $objPHPExcel->setActiveSheetIndex(0)
        ->setCellValue('A4', 'Código:')
        ->setCellValue('B4', "Nombre")
        ->setCellValue('C4', "Presupuesto")
        ->setCellValue('D4', "Destino/Tercero")
        ->setCellValue('E4', "Afecta presupuesto")
        ->setCellValue('F4', "Concepto")
        ->setCellValue('G4', "Tipo ingreso");
        $objPHPExcel-> getActiveSheet ()
            -> getStyle ("A4:G4")
            -> getFill ()
            -> setFillType (PHPExcel_Style_Fill :: FILL_SOLID)
            -> getStartColor ()
            -> setRGB ('99ddff');
        $objPHPExcel->getActiveSheet()->getStyle("A4:G4")->getFont()->setBold(true);
        $objPHPExcel->getActiveSheet()->getStyle('A1:G1')->applyFromArray($borders);
        $objPHPExcel->getActiveSheet()->getStyle('A2:G2')->applyFromArray($borders);
        $objPHPExcel->getActiveSheet()->getStyle('A3:G3')->applyFromArray($borders);
        $objPHPExcel->getActiveSheet()->getStyle('A4:G4')->applyFromArray($borders);
        $objPHPExcel->setActiveSheetIndex(0)
            ->mergeCells('A3:B3')
            ->mergeCells('C3:D3')
            ->mergeCells('E3:G3')
            ->setCellValue('A3', 'Convenciones:')
            ->setCellValue('C3', "Cuenta sin asignar")
            ->setCellValue('E3', "Cuenta no habilitada");
        $objPHPExcel-> getActiveSheet ()
        -> getStyle ("C3:G3")
        -> getAlignment ()
        -> applyFromArray (array ( 'horizontal'  =>  PHPExcel_Style_Alignment :: HORIZONTAL_CENTER));
        $objPHPExcel-> getActiveSheet ()
        -> getStyle ("C3:D3")
        -> getFill ()
        -> setFillType (PHPExcel_Style_Fill :: FILL_SOLID)
        -> getStartColor ()
        -> setRGB ('ffeb3b');
        $objPHPExcel-> getActiveSheet ()
        -> getStyle ("E3:G3")
        -> getFill ()
        -> setFillType (PHPExcel_Style_Fill :: FILL_SOLID)
        -> getStartColor ()
        -> setRGB ('ff0000');

        $objPHPExcel->getActiveSheet()->getStyle("E3:G3")->getFont()->getColor()->setRGB("ffffff");
        $objPHPExcel->getActiveSheet()->getStyle("A3:G3")->getFont()->setBold(true);


        $objWorksheet = $objPHPExcel->getActiveSheet();
        $totalData = count($request);
        $row = 5;
        for ($i=0; $i < $totalData ; $i++) {
            $objPHPExcel->setActiveSheetIndex(0)
                ->setCellValueExplicit ("A$row", $request[$i]['codigo'], PHPExcel_Cell_DataType :: TYPE_STRING)
                ->setCellValueExplicit ("B$row", $request[$i]['nombre'], PHPExcel_Cell_DataType :: TYPE_STRING)
                ->setCellValueExplicit ("C$row", $request[$i]['cuentapres'], PHPExcel_Cell_DataType :: TYPE_STRING)
                ->setCellValueExplicit ("D$row", $request[$i]['terceros'], PHPExcel_Cell_DataType :: TYPE_STRING)
                ->setCellValueExplicit ("E$row", $request[$i]['is_presupuesto'], PHPExcel_Cell_DataType :: TYPE_STRING)
                ->setCellValueExplicit ("F$row", $request[$i]['concepto'], PHPExcel_Cell_DataType :: TYPE_STRING)
                ->setCellValueExplicit ("G$row", $request[$i]['tipoingreso'], PHPExcel_Cell_DataType :: TYPE_STRING);

            if($request[$i]['cuentapres'] == "" && $request[$i]['is_tercero'] == 2 && $request[$i]['is_cuenta'] == 2){
                $objPHPExcel-> getActiveSheet ()
                -> getStyle ("A$row:F$row")
                -> getFill ()
                -> setFillType (PHPExcel_Style_Fill :: FILL_SOLID)
                -> getStartColor ()
                -> setRGB ('ffeb3b');
                $objPHPExcel->getActiveSheet()->getStyle("A$row:F$row")->getFont()->setBold(true);
            }else if($request[$i]['estado_cuenta'] == 2 && $request[$i]['is_tercero'] == 2 && $request[$i]['is_cuenta'] == 2){
                $objPHPExcel-> getActiveSheet ()
                -> getStyle ("A$row:F$row")
                -> getFill ()
                -> setFillType (PHPExcel_Style_Fill :: FILL_SOLID)
                -> getStartColor ()
                -> setRGB ('ff0000');
                $objPHPExcel->getActiveSheet()->getStyle("A$row:F$row")->getFont()->getColor()->setRGB("ffffff");
                $objPHPExcel->getActiveSheet()->getStyle("A$row:F$row")->getFont()->setBold(true);
            }
            $row++;
        }
        $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);
        //----Guardar documento----
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="reporte-otros_tributos.xlsx"');
        header('Cache-Control: max-age=0');
        $objWriter=PHPExcel_IOFactory::createWriter($objPHPExcel,'Excel2007');
        $objWriter->save('php://output');
        die();
    }
?>
