<?php

    header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=utf8");
	require 'comun.inc';
	require 'funciones.inc';
	session_start();
    if(empty($_SESSION)){
        header("location: index.php");
    }
	$linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");
	cargarcodigopag($_GET['codpag'],$_SESSION["nivel"]);
	date_default_timezone_set("America/Bogota");
?>
<!DOCTYPE >
<html lang=es>
	<head>
    <meta charset="utf-8"/>
		<meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1"/>
		<meta name="viewport" content="user-scalable=no">
		<title>:: IDEAL 10 - Parametrización</title>
		<link href="favicon.ico" rel="shortcut icon"/>
		<link href="css/css2z.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
		<link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
		<link href="css/css4.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
        <link href="css/style.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css"/>
        <link rel="stylesheet" href="sweetalert2/dist/sweetalert2.min.css">
        <script src="sweetalert2/dist/sweetalert2.min.js"></script>
		<script src="css/programas.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
	</head>
	<body>
		<header>
			<table>
				<tr><script>barra_imagenes("trans");</script><?php cuadro_titulos();?></tr>
			</table>
		</header>

        <main id="myapp" v-cloak>
            <input type="hidden" value = "1" ref="pageType">
            <div class="loading-container" v-show="isLoading" >
                <p class="text-loading"data-loading="IDEAL 10">IDEAL 10</p>
            </div>
            <nav>
                <table>
                    <tr><?php menu_desplegable("trans");?></tr>
                </table>
                <div class="bg-white group-btn p-1" id="newNavStyle">
                    <button type="button" @click="save()" class="btn btn-white btn-primary-hover d-flex justify-between align-items-center">
                        <span>Guardar</span>
                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 -960 960 960">
                            <path d="M840-680v480q0 33-23.5 56.5T760-120H200q-33 0-56.5-23.5T120-200v-560q0-33 23.5-56.5T200-840h480l160 160Zm-80 34L646-760H200v560h560v-446ZM480-240q50 0 85-35t35-85q0-50-35-85t-85-35q-50 0-85 35t-35 85q0 50 35 85t85 35ZM240-560h360v-160H240v160Zm-40-86v446-560 114Z">
                            </path>
                        </svg>
                    </button>
                    <button type="button" @click="window.location.href='trans-tarifa-buscar'" class="btn btn-white btn-primary-hover d-flex justify-between align-items-center">
                        <span>Buscar</span>
                        <svg class="fill-black group-hover:fill-white w-5 h-5" xmlns="http://www.w3.org/2000/svg" viewBox="0 -960 960 960"><path d="M784-120 532-372q-30 24-69 38t-83 14q-109 0-184.5-75.5T120-580q0-109 75.5-184.5T380-840q109 0 184.5 75.5T640-580q0 44-14 83t-38 69l252 252-56 56ZM380-400q75 0 127.5-52.5T560-580q0-75-52.5-127.5T380-760q-75 0-127.5 52.5T200-580q0 75 52.5 127.5T380-400Z"></path></svg>
                    </button>
                    <button type="button" class="btn btn-white btn-primary-hover d-flex justify-between align-items-center" @click="mypop=window.open('trans-principal.php','',''); mypop.focus();">
                        <span>Nueva ventana</span>
                        <svg class="fill-black group-hover:fill-white w-5 h-5" xmlns="http://www.w3.org/2000/svg"  viewBox="0 -960 960 960" ><path d="M320-120v-80h80v-80H160q-33 0-56.5-23.5T80-360v-400q0-33 23.5-56.5T160-840h640q33 0 56.5 23.5T880-760v400q0 33-23.5 56.5T800-280H560v80h80v80H320ZM160-360h640v-400H160v400Zm0 0v-400 400Z"/></svg>
                    </button>
                    <button type="button" @click="mypop=window.open('trans-tarifa-crear','','');mypop.focus();" class="btn btn-white btn-primary-hover d-flex justify-between align-items-center">
                        <span class="group-hover:text-white">Duplicar pantalla</span>
                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 -960 960 960"><path d="M640-160v-360H160v360h480Zm80-200v-80h80v-360H320v200h-80v-200q0-33 23.5-56.5T320-880h480q33 0 56.5 23.5T880-800v360q0 33-23.5 56.5T800-360h-80ZM160-80q-33 0-56.5-23.5T80-160v-360q0-33 23.5-56.5T160-600h480q33 0 56.5 23.5T720-520v360q0 33-23.5 56.5T640-80H160Zm400-603ZM400-340Z"></path></svg>
                    </button>
                    <button type="button" @click="window.location.href='trans-tarifa-buscar'" class="btn btn-success btn-success-hover d-flex justify-between align-items-center">
                        <span>Atras</span>
                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 -960 960 960"><path d="M760-200v-160q0-50-35-85t-85-35H273l144 144-57 56-240-240 240-240 57 56-144 144h367q83 0 141.5 58.5T840-360v160h-80Z"></path></svg>
                    </button>
                </div>
            </nav>
            <section class="bg-white">
                <div>
                    <h2 class="titulos m-0">Crear tarifa legal</h2>
                    <p class="m-0 ms-2">Todos los campos con (<span class="text-danger fw-bolder">*</span>) son obligatorios.</p>
                    <div class="d-flex">
                        <div class="form-control">
                            <label class="form-label" for="">Nombre <span class="text-danger fw-bolder">*</span>:</label>
                            <input type="text" v-model="strNombre">
                        </div>
                        <div class="form-control">
                            <label class="form-label" for="">Vigencia <span class="text-danger fw-bolder">*</span>:</label>
                            <input type="number" v-model="intVigencia">
                        </div>
                        <div class="form-control w-50">
                            <label class="form-label" for="">Tipo <span class="text-danger fw-bolder">*</span>:</label>
                            <select v-model="intTipo">
                                <option value="1">Porcentaje</option>
                                <option value="2">Rango y porcentaje</option>
                            </select>
                        </div>
                        <div class="form-control w-50" v-if="intTipo == 1">
                            <label class="form-label" for="">Porcentaje <span class="text-danger fw-bolder">*</span>:</label>
                            <input type="number" v-model="intPorcentaje">
                        </div>
                    </div>
                    <div class="d-flex">
                        <div class="form-control">
                            <label class="form-label" for="">Tipo de vehiculos que aplica:</label>
                            <div class="select-wrapper">
                                <div class="select-multiple" id="selectMultiple1" onclick="showMultipleSelect(this)"> Seleccione</div>
                                <ul class="select-multiple-content d-none">
                                    <li >
                                        <label for="multipleCheckAll1" >
                                            <input type="checkbox"  value="all" onchange="getCheckMultipleSelect(this,true)" id="multipleCheckAll1">
                                            Todos
                                        </label>
                                    </li>
                                    <li  v-for="(data,index) in arrTipos" :key="index">
                                        <label :for="'multipleCheck'+index" >
                                            <input type="checkbox"  onchange="getCheckMultipleSelect(this)" :value="data.id+'-'+data.nombre" :id="'multipleCheck'+index">
                                            {{data.nombre}}
                                        </label>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div v-if="intTipo == 2">
                        <h2 class="titulos m-0">Rangos</h2>
                        <div class="d-flex">
                            <div class="form-control" >
                                <label class="form-label" for="">Desde:</label>
                                <input type="text" class="text-right" :value="valorDesde" @keyup="setValue(event,'desde')">
                            </div>
                            <div class="form-control" >
                                <label class="form-label" for="">Limite:</label>
                                <select v-model="intLimite">
                                    <option value="1">Valor</option>
                                    <option value="2">En adelante</option>
                                </select>
                            </div>
                            <div class="form-control" v-if="intLimite==1">
                                <label class="form-label" for="">Hasta:</label>
                                <input type="text" class="text-right" :value="valorHasta" @keyup="setValue(event,'hasta')">
                            </div>
                            <div class="form-control">
                                <label class="form-label" for="">Porcentaje:</label>
                                <div class="d-flex">
                                    <input type="number" v-model="intRangoPorcentaje">
                                    <button type="button" class="btn btn-primary" @click="addItem()">Agregar</button>
                                </div>
                            </div>
                        </div>
                        <div class="overflow-auto" style="height:40vh">
                            <table class="table fw-normal">
                                <thead>
                                    <tr class="text-center">
                                        <th>Desde</th>
                                        <th>Límite</th>
                                        <th>Hasta</th>
                                        <th>Porcentaje</th>
                                        <th>
                                            <div class="d-flex justify-center">
                                                <button type="button" @click="delItem()" class="btn btn-danger">x</button>
                                            </div>
                                        </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr v-for="(data,index) in arrRangos" :key="index">
                                        <td class="text-right">{{formatMoney(data.desde)}}</td>
                                        <td class="text-center">{{data.limite == 1 ? "Valor" : "En adelante"}}</td>
                                        <td class="text-right">{{data.limite == 1 ? formatMoney(data.hasta) : "N/A"}}</td>
                                        <td class="text-center">{{data.porcentaje}}%</td>
                                        <td>
                                            <div class="d-flex justify-center">
                                                <button type="button" @click="delItem(index)" class="btn btn-danger">x</button>
                                            </div>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </section>
        </main>
        <script src="Librerias/vue/vue.min.js"></script>
		<script type="module" src="transporte/js/functions_tarifas.js?<?= date('d_m_Y_h_i_s');?>"></script>
	</body>
</html>
