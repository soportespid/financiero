<?php
	require_once 'PHPExcel/Classes/PHPExcel.php';
	require "comun.inc";
	require "funciones.inc";
	session_start();
	$objPHPExcel = new PHPExcel();

	//----Propiedades----
	$objPHPExcel->getProperties()
	->setCreator("IDEAL 10")
	->setLastModifiedBy("IDEAL 10")
	->setTitle("Exportar Excel con PHP")
	->setSubject("Documento de prueba")
	->setDescription("Documento generado con PHPExcel")
	->setKeywords("usuarios phpexcel")
	->setCategory("reportes");

	//----Cuerpo de Documento----
	$objPHPExcel->setActiveSheetIndex(0)
	->mergeCells('A1:K1')
	->mergeCells('A2:K2')
	->setCellValue('A1', 'TESORERÍA - ACUERDO PREDIAL')
	->setCellValue('A2', 'ACUERDO DE PAGO PREDIAL');
	$objPHPExcel-> getActiveSheet ()
	-> getStyle ("A1")
	-> getFill ()
	-> setFillType (PHPExcel_Style_Fill :: FILL_SOLID)
	-> getStartColor ()
	-> setRGB ('C8C8C8');
	$objPHPExcel-> getActiveSheet ()
	-> getStyle ("A1:A2")
	-> getFont ()
	-> setBold ( true ) 
	-> setName ( 'Verdana' ) 
	-> setSize ( 10 ) 
	-> getColor ()
	-> setRGB ('000000');
	$objPHPExcel-> getActiveSheet ()
	-> getStyle ('A1:A2')
	-> getAlignment ()
	-> applyFromArray (array ( 'horizontal'  =>  PHPExcel_Style_Alignment :: HORIZONTAL_CENTER ,) );
	$objPHPExcel-> getActiveSheet ()
	-> getStyle ('A3:K3')
	-> getAlignment ()
	-> applyFromArray (array ( 'horizontal'  =>  PHPExcel_Style_Alignment :: VERTICAL_JUSTIFY ,) ); 
	$objPHPExcel-> getActiveSheet ()
	-> getStyle ("A2")
	-> getFill ()
	-> setFillType (PHPExcel_Style_Fill :: FILL_SOLID)
	-> getStartColor ()
	-> setRGB ('A6E5F3');
	$objPHPExcel-> getActiveSheet ()
	-> getStyle ("A3:K3")
	-> getFill ()
	-> setFillType (PHPExcel_Style_Fill :: FILL_SOLID)
	-> getStartColor ()
	-> setRGB ('22C6CB');
	$borders = array(
		'borders' => array(
			'allborders' => array(
				'style' => PHPExcel_Style_Border::BORDER_THIN,
			'color' => array('argb' => 'FF000000'),
			)
		),
	);
	$objPHPExcel->getActiveSheet()->getStyle('A1:K1')->applyFromArray($borders);
	$objPHPExcel->getActiveSheet()->getStyle('A2:K2')->applyFromArray($borders);
	$objPHPExcel->getActiveSheet()->getStyle('A3:K3')->applyFromArray($borders);
	$objWorksheet = $objPHPExcel->getActiveSheet();


	$objPHPExcel->setActiveSheetIndex(0)
	->setCellValue('A3', 'No de acuerdo')
	->setCellValue('B3', 'Cod. Catastral')
	->setCellValue('C3', 'Fecha')
	->setCellValue('D3', 'Valor')
	->setCellValue('E3', 'Tercero')
	->setCellValue('F3', 'Cuotas pactadas')
	->setCellValue('G3', 'Cuotas pagas')
	->setCellValue('H3', 'Cuotas faltantes')
	->setCellValue('I3', 'Valor pagado')
	->setCellValue('J3', 'Valor faltante')
	->setCellValue('K3', 'Estado');

	$i = 4;

	for($x = 0; $x < count($_POST['idacuerdo']); $x++ )
	{
		$objPHPExcel->setActiveSheetIndex(0)
		
		->setCellValueExplicit ("A$i", $_POST['idacuerdo'][$x], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
		->setCellValueExplicit ("B$i", $_POST['codcatastral'][$x], PHPExcel_Cell_DataType :: TYPE_STRING)
		->setCellValueExplicit ("C$i", $_POST['fecha_acuerdo'][$x], PHPExcel_Cell_DataType :: TYPE_STRING)
		->setCellValueExplicit ("D$i", $_POST['valor_pago'][$x], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
		->setCellValueExplicit ("E$i", $_POST['tercero'][$x], PHPExcel_Cell_DataType :: TYPE_STRING)
		->setCellValueExplicit ("F$i", $_POST['cuotas'][$x], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
		->setCellValueExplicit ("G$i", $_POST['cuotasPagas'][$x], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
		->setCellValueExplicit ("H$i", $_POST['cuotasFaltantes'][$x], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
		->setCellValueExplicit ("I$i", $_POST['valorPagado'][$x], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
		->setCellValueExplicit ("J$i", $_POST['valorFaltante'][$x], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
		->setCellValueExplicit ("K$i", $_POST['estado'][$x], PHPExcel_Cell_DataType :: TYPE_STRING);
		
		$objPHPExcel->getActiveSheet()->getStyle("A$i:K$i")->applyFromArray($borders);
		
		$i++;

	}
	
	//----Propiedades de la hoja
	$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true); 
	$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true); 
	$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true); 
	$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true); 
	$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true); 
	$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true); 
	$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setAutoSize(true); 
	$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setAutoSize(true); 
	$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setAutoSize(true); 
	$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setAutoSize(true); 
	$objPHPExcel->getActiveSheet()->getColumnDimension('K')->setAutoSize(true); 
	$objPHPExcel->getActiveSheet()->setTitle('Teso-AcuerdoPredial');
	$objPHPExcel->setActiveSheetIndex(0);

//----Guardar documento----
header('Content-Type: application/vnd.ms-excel');
header('Content-Disposition: attachment;filename="Teso-Liquida-Predial.xlsx"');
header('Cache-Control: max-age=0');
$objWriter=PHPExcel_IOFactory::createWriter($objPHPExcel,'Excel2007');
$objWriter->save('php://output');
exit;
?>