<?php
	header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=utf8");
	require 'comun.inc';
	require 'funciones.inc';
	session_start();
	$linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");
	cargarcodigopag($_GET['codpag'],$_SESSION["nivel"]);
	date_default_timezone_set("America/Bogota");
	titlepag();
?>
<!DOCTYPE >
<html lang=es>
	<head>
		<meta charset="utf-8"/>
		<meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1"/>
		<meta name="viewport" content="user-scalable=no">
		<title>:: IDEAL 10 - Servicios Públicos</title>
		<link href="css/css2z.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
		<link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
		<link href="css/css4.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
		<link href="css/tabs.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css"/>
		<script src="vue/vue.min.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script src="css/programas.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script src="css/calendario.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<style>
			input[type=number]::-webkit-inner-spin-button,
			input[type=number]::-webkit-outer-spin-button { 
				-webkit-appearance: none; 
				margin: 0; 
			}
			input[type=number]{
				text-align:right;
				box-sizing: border-box;
				font: 120% sans-serif;
				width: 100% !important;
			}
			[v-cloak]{display : none;}

			.tablamv tbody {
				display: block;
				max-height: 56vh;
				overflow-y: hidden !important;
			}
		</style>
		<script src="sweetalert2/dist/sweetalert2.min.js"></script>
		<link rel="stylesheet" href="sweetalert2/dist/sweetalert2.min.css">
	</head>
	<body>
		<header>
			<table>
				<tr><script>barra_imagenes("serv");</script><?php cuadro_titulos();?></tr>
			</table>
		</header>
		<form name="form2" method="post" action="">
			<section id="myapp" v-cloak >
				<nav>
					<table>
						<tr><?php menu_desplegable("serv");?></tr>
						<tr>
							<td colspan="3" class="cinta">
								<img src="imagenes/add2.png"  class="mgbt1" title="Nuevo">
								<img src="imagenes/guardad.png" title="Guardar"  class="mgbt1">
								<img src="imagenes/buscad.png" class="mgbt1" title="Buscar">
								<img src="imagenes/nv.png" onClick="mypop=window.open('serv-principal.php','',''); mypop.focus();" class="mgbt" title="Nueva Ventana">
								<img src="imagenes/excel.png" title="Excel" class="mgbt" v-on:click="excel">
								<a href="serv-menuInformes"><img src="imagenes/iratras.png" class="mgbt" alt="Atrás"></a>
							</td>
						</tr>
					</table>
				</nav>
				<article>
					<table class="inicio ancho">
						<tr>
							<td class="titulos" colspan="6" >Informe de facturación</td>
							<td class="cerrar" style="width:7%" onClick="location.href='serv-principal.php'">Cerrar</td>
						</tr>

						<tr>
							<td class="tamano01" style="width: 10%;">Periodo inicial: </td>
							<td style="width: 15%;">
								<input type="text" v-model="descripcionPeriodoInicial" style="width: 100%; height: 30px; text-align:center;" class="colordobleclik" v-on:dblclick="periodosLiquidados(1)" autocomplete="off" readonly>
							</td>

                            <td class="tamano01" style="width: 10%;">Periodo final: </td>
							<td style="width: 15%;">
								<input type="text" v-model="descripcionPeriodoFinal" style="width: 100%; height: 30px; text-align:center;" class="colordobleclik" v-on:dblclick="periodosLiquidados(2)" autocomplete="off" readonly>
							</td>

							<td colspan="2" style="padding-bottom:0px">
								<em class="botonflechaverde" v-on:Click="generarInforme">Generar Informe</em>
							</td>
						</tr>
					</table>

					<div class='subpantalla' style='height:66vh; width:99.2%; margin-top:0px; overflow-x: hidden;'>
						<table v-for="(corte, index) in cortes" class='tablamv' style="overflow-y: hidden">
							<thead>
								<tr style="text-align:Center;">
									<th class="titulosnew00" style="width:6%;">Periodo</th>
									<th class="titulosnew00">Servicio</th>
									<th class="titulosnew00" style="width:10%;">Cargo fijo</th>
									<th class="titulosnew00" style="width:10%;">Sub Cargo fijo</th>
									<th class="titulosnew00" style="width:10%;">Consumo</th>
									<th class="titulosnew00" style="width:10%;">Sub consumo</th>
									<th class="titulosnew00" style="width:10%;">Contribución</th>
									<th class="titulosnew00" style="width:10%;">Total</th>
								</tr>
							</thead>
							
							<tbody>
								<tr v-for="(informacion, index) in informaciones" v-if="corte[0] == informacion[0]"  v-bind:class="index % 2 ? 'saludo1a' : 'saludo2'"  style='text-rendering: optimizeLegibility; cursor: pointer !important;'>
									<td style="width:6%; font: 120% sans-serif; padding-left:5px; text-align:center;">{{ informacion[0] }}</td>
									<td style="font: 120% sans-serif; padding-left:5px; text-align:center;">{{ informacion[1] }}</td>
									<td style="width:10%; font: 120% sans-serif; padding-left:5px; text-align:center;">{{ formatonumero(informacion[2]) }}</td>
									<td style="width:10%; font: 120% sans-serif; padding-left:5px; text-align:center;">{{ formatonumero(informacion[3]) }}</td>
									<td style="width:10%; font: 120% sans-serif; padding-left:5px; text-align:center;">{{ formatonumero(informacion[4]) }}</td>
									<td style="width:10%; font: 120% sans-serif; padding-left:5px; text-align:center;">{{ formatonumero(informacion[5]) }}</td>
									<td style="width:10%; font: 120% sans-serif; padding-left:5px; text-align:center;">{{ formatonumero(informacion[6]) }}</td>
									<td style="width:10%; font: 120% sans-serif; padding-left:5px; text-align:center;">{{ formatonumero(informacion[7]) }}</td>

									<input type='hidden' name='periodo[]' v-model="informacion[0]">	
									<input type='hidden' name='servicio[]' v-model="informacion[1]">	
									<input type='hidden' name='cargoFijo[]' v-model="informacion[2]">	
									<input type='hidden' name='subCargoFijo[]' v-model="informacion[3]">	
									<input type='hidden' name='consumo[]' v-model="informacion[4]">	
									<input type='hidden' name='subConsumo[]' v-model="informacion[5]">	
									<input type='hidden' name='contribucion[]' v-model="informacion[6]">	
									<input type='hidden' name='total[]' v-model="informacion[7]">	
								</tr>
							</tbody>

							<thead>
								<tr v-for="(total, index) in totales" v-if="corte[0] == total[0]" style="text-align:Center;">
									<th class="titulosnew00" style="width:55%; text-align: right;">Total:</th>
									<th class="" style="width:6%;"> {{ formatonumero(total[1]) }}</th>
								</tr>
							</thead>

						</table>
					</div>

					<div id="cargando" v-if="loading" class="loading">
						<span>Cargando...</span>
					</div>

					<div v-show="showModalLiquidaciones">
						<transition name="modal">
							<div class="modal-mask">
								<div class="modal-wrapper">
									<div class="modal-container2">
										<table class="inicio ancho">
											<tr>
												<td class="titulos" colspan="2" >SELECCIONAR PERIODO LIQUIDADO</td>
												<td class="cerrar" style="width:7%" @click="showModalLiquidaciones = false">Cerrar</td>
											</tr>
											<tr>
												<td class="tamano01" style="width:3cm">PERIODO:</td>
												<td>
													<input type="text" class="form-control" placeholder="Buscar por numero de cuenta" v-on:keyup="" style="width:100%" />
												</td>
											</tr>
										</table>
										<table class="subpantalla" style="overflow:scroll;">
											<thead>
												<tr style="text-align:center;">
													<th class="titulosnew00" style="width:8.5%;">Número de corte liquidado</th>
													<th class="titulosnew00">Descripcion</th>
													<th class="titulosnew00" style="width:15%;">Fecha inicial</th>
													<th class="titulosnew00" style="width:15%;">Fecha final</th>
													<th class="titulosnew00" style="width: 1%;"></th>
												</tr>
											</thead>
											<tbody>
												<tr v-for="(periodo,index) in datosPeriodosLiquidados" v-on:click="seleccionaPeriodo(periodo)" v-bind:class="index % 2 ? 'saludo1a' : 'saludo2'" style='text-rendering: optimizeLegibility; cursor: pointer !important;'>
													<td style="font: 120% sans-serif; padding-left:10px; width:8.5%; text-align:center;">{{ periodo[0] }}</td>
													<td style="font: 120% sans-serif; padding-left:10px; text-align:center;">{{ periodo[1] }}</td>
													<td style="font: 120% sans-serif; padding-left:10px; width:15%; text-align:center;">{{ periodo[2] }}</td>
													<td style="font: 120% sans-serif; padding-left:10px; width:15%; text-align:center;">{{ periodo[3] }}</td>
												</tr>
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</transition>
					</div>

				</article>
			</section>
		</form>

		<script src="Librerias/vue/vue.min.js"></script>
		<script src="Librerias/vue/axios.min.js"></script>
		<script src="servicios_publicos/reporte_facturacion/serv-reporte_facturacion.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
        
	</body>
</html>