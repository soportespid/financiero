<?php 
	require "comun.inc";
	require "funciones.inc";
	session_start();
	$linkbd = conectar_v7();	
	cargarcodigopag($_GET['codpag'],$_SESSION["nivel"]);
	header("Cache-control: private"); // Arregla IE 6
	date_default_timezone_set("America/Bogota");
?>
<!DOCTYPE >
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="es">
	<head>
	 	<meta http-equiv="Content-Type" content="text/html" charset="iso-8859-1"/>
        <meta http-equiv="X-UA-Compatible" content="IE=9"/>
		<title>:: IDEAL 10 - Cuentas Bancar&iacute;as</title>
        <link href="css/css2.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="css/calendario.js"></script>
		<script>
			var anterior;
			function ponprefijo(pref,opc,cuenta)
			{
				parent.document.form2.banco.value =pref;
				parent.document.form2.nbanco.value =opc;
				parent.document.form2.cuentaBancaria.value = cuenta;
				parent.document.form2.banco.focus();
				
				parent.despliegamodal2("hidden");
				
			} 
		</script> 
		<?php titlepag();?>
	</head>

	<body>
		<form action="" method="post" enctype="multipart/form-data" name="form1">
			<table class="inicio">
				<tr>
					<td height="25" colspan="4" class="titulos" >Buscar Cuentas Bancar&iacute;s</td>
					<td class="cerrar"><a onClick="parent.despliegamodal2('hidden');">&nbsp;Cerrar</a></td>
				</tr>
				
				<tr>
					<td class="saludo1" >:&middot; Raz&oacute;n Social:</td>

					<td colspan="3">
                        <input name="numero" type="text" size="30"/>

                        <input type="hidden" name="oculto" id="oculto" value="1" >

                        <input type="submit" name="Submit" value="Buscar">
					</td>
				</tr>

				<tr>
					<td colspan="4" align="center">&nbsp;</td>
				</tr>
			</table>
            
			<div class="subpantalla" style="height:73.5%; width:99.6%; overflow-x:hidden;">
				<table class="inicio">
					<tr>
						<td height="25" colspan="5" class="titulos" >Resultados Busqueda </td>
					</tr>

					<tr>
						<td width="" class="titulos2">Nit</td>
						<td width="" class="titulos2">Razo&oacute;n Social</td>
						<td width="" class="titulos2">Cuenta</td>	  
						<td width="" class="titulos2">Cuenta Bancar&iacute;a</td>
                        <td width="" class="titulos2">Tipo Cuenta</td>
					</tr>
					<?php
					$vigusu = vigencia_usuarios($_SESSION['cedulausu']); 
					$oculto = $_POST['oculto'];
					
					//if($oculto!="")
                    $link = conectar_v7();

                    $cond = " AND (codigo LIKE'%".$_POST['numero']."%' OR nombre LIKE '%".($_POST['numero'])."%')";

                    $sqlr = "SELECT B.cuenta,B.tercero,B.ncuentaban,B.tipo,T.razonsocial FROM tesobancosctas B JOIN terceros T ON B.tercero = T.cedulanit";
                    
                    $resp = mysqli_query($link, $sqlr);			
                    
                    $co = 'saludo1a';
                    $co2 = 'saludo2';	
                    $i=1;
                    while ($r = mysqli_fetch_row($resp)) 
                    {
                        echo 
                        "<tr class='$co' onMouseOver=\"anterior=this.style.backgroundColor;this.style.backgroundColor='#40b3ff';\" onMouseOut=\"this.style.backgroundColor=anterior\" "; 
                        
                        echo" onClick=\"javascript:ponprefijo('$r[2]','$r[4]','$r[0]')\"";
                         
                        echo"
                            <td></td>
                            <td>$r[1]</td>
                            <td>$r[4]</td>
                            <td>$r[0]</td>
                            <td>$r[2]</td>
                            <td>$r[3]</td>
                        </tr> ";
                        
                        $aux=$co;
                        $co=$co2;
                        $co2=$aux;
                        $i=1+$i;
                    }
                    $_POST['oculto']="";
					
					?>
				</table>
			</div>
		</form>
	</body>
</html> 
