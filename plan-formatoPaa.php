<?php

	header("Content-Type: text/html;charset=utf-8");
    require_once 'PHPExcel/Classes/PHPExcel.php';
    require "comun.inc";
    require "funciones.inc";
    require 'funcionesSP.inc.php';
    /*ini_set('display_errors', '1');
    ini_set('display_startup_errors', '1');
    error_reporting(E_ALL);*/

    session_start();

    class Plantilla{
        private $linkbd;
        public function __construct() {
            $this->linkbd = conectar_v7();
            $this->linkbd->set_charset("utf8");
        }
        public function modalidades() {
            $sql = "SELECT codigo, nombre FROM plan_modalidad_seleccion ORDER BY codigo";
            $data = mysqli_fetch_all(mysqli_query($this->linkbd,$sql),MYSQLI_ASSOC);
            return $data;
        }

        public function fuentes() {
            $sql = "SELECT codigo, nombre FROM plan_fuentes_paa ORDER BY codigo";
            $data = mysqli_fetch_all(mysqli_query($this->linkbd,$sql),MYSQLI_ASSOC);
            return $data;
        }
    }

    $obj = new Plantilla();
    $modalidades = $obj->modalidades();
    $fuentes = $obj->fuentes();

    $objPHPExcel = new PHPExcel();

    $objPHPExcel->createSheet(1);
    $objPHPExcel->createSheet(2);
    
    $objPHPExcel->getSheet(0)->getStyle('A:K')->applyFromArray(
        array(
            'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
                'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER
            )
        )
    );
    $objPHPExcel->getSheet(0)->setTitle("Formato");
    $objPHPExcel->getSheet(1)->setTitle("Modalidad");
    $objPHPExcel->getSheet(2)->setTitle("Fuentes");
    $objPHPExcel->getProperties()
    ->setCreator("IDEAL 10")
    ->setLastModifiedBy("IDEAL 10")
    ->setTitle("Exportar Excel con PHP")
    ->setSubject("Documento de prueba")
    ->setDescription("Documento generado con PHPExcel")
    ->setKeywords("usuarios phpexcel")
    ->setCategory("reportes");

    //----Cuerpo de Documento----
    $objPHPExcel->getSheet(0)
    ->mergeCells('A1:K1')
    ->mergeCells('A2:K2')
    ->setCellValue('A1', 'PLANEACIÓN ESTRATEGICA')
    ->setCellValue('A2', 'PLANTILLA DE PLAN ANUAL DE ADQUISICIONES');
    $objPHPExcel->getSheet(0)
    -> getStyle ("A1")
    -> getFill ()
    -> setFillType (PHPExcel_Style_Fill :: FILL_SOLID)
    -> getStartColor ()
    -> setRGB ('C8C8C8');
    $objPHPExcel->getSheet(0)
    -> getStyle ("A1:K2")
    -> getFont ()
    -> setBold ( true )
    -> setName ( 'Verdana' )
    -> setSize ( 10 )
    -> getColor ()
    -> setRGB ('000000');
    $objPHPExcel->getSheet(0)
    -> getStyle ('A1:K2')
    -> getAlignment ()
    -> applyFromArray (array ( 'horizontal'  =>  PHPExcel_Style_Alignment :: HORIZONTAL_CENTER ,) );
    $objPHPExcel->getSheet(0)
    -> getStyle ('A3:K3')
    -> getAlignment ()
    -> applyFromArray (array ( 'horizontal'  =>  PHPExcel_Style_Alignment :: VERTICAL_JUSTIFY ,) );
    $objPHPExcel->getSheet(0)
    -> getStyle ("A2")
    -> getFill ()
    -> setFillType (PHPExcel_Style_Fill :: FILL_SOLID);

    $borders = array(
        'borders' => array(
            'allborders' => array(
                'style' => PHPExcel_Style_Border::BORDER_THIN,
            'color' => array('argb' => 'FF000000'),
            )
        ),
    );
    $objPHPExcel->getSheet(0)
    ->setCellValue('A3', "Mes estimado inicio (En letra)")
    ->setCellValue('B3', "Códigos UNSPSC (separados por espacio)")
    ->setCellValue('C3', "Descripcion")
    ->setCellValue('D3', "Duracion estimada de contrato (meses)")
    ->setCellValue('E3', "Modalidad de selección")
    ->setCellValue('F3', "Fuente de los recursos")
    ->setCellValue('G3', "Valor total estimado")
    ->setCellValue('H3', "Valor estimado en la vigencia actual")
    ->setCellValue('I3', "¿Se requieren vigencias futuras? (Si/No)")
    ->setCellValue('J3', "Estado de solicitud de vigencias futuras")
    ->setCellValue('K3', "Datos de contacto del responsable (Nombre/Cargo)");
    $objPHPExcel->getSheet(0)
        -> getStyle ("A3:K3")
        -> getFill ()
        -> setFillType (PHPExcel_Style_Fill :: FILL_SOLID)
        -> getStartColor ()
        -> setRGB ('99ddff');
    $objPHPExcel->getSheet(0)->getStyle("A3:K3")->getFont()->setBold(true);
    $objPHPExcel->getSheet(0)->getStyle('A1:K1')->applyFromArray($borders);
    $objPHPExcel->getSheet(0)->getStyle('A2:K2')->applyFromArray($borders);
    $objPHPExcel->getSheet(0)->getStyle('A3:K3')->applyFromArray($borders);


    $objPHPExcel->getSheet(0)->getColumnDimension('A')->setAutoSize(true);
    $objPHPExcel->getSheet(0)->getColumnDimension('B')->setAutoSize(true);
    $objPHPExcel->getSheet(0)->getColumnDimension('C')->setAutoSize(true);
    $objPHPExcel->getSheet(0)->getColumnDimension('D')->setAutoSize(true);
    $objPHPExcel->getSheet(0)->getColumnDimension('E')->setAutoSize(true);
    $objPHPExcel->getSheet(0)->getColumnDimension('F')->setAutoSize(true);
    $objPHPExcel->getSheet(0)->getColumnDimension('G')->setAutoSize(true);
    $objPHPExcel->getSheet(0)->getColumnDimension('H')->setAutoSize(true); 
    $objPHPExcel->getSheet(0)->getColumnDimension('K')->setAutoSize(true); 

    //Hoja conceptos

    $objPHPExcel->getSheet(1)->getStyle('A:B')->applyFromArray(
        array(
            'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
                'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER
            )
        )
    );

    $objPHPExcel->getSheet(1)
    ->mergeCells('A1:B1')
    ->mergeCells('A2:B2')
    ->setCellValue('A1', 'PLANEACIÓN ESTRATEGICA')
    ->setCellValue('A2', 'MODALIDADES DE SELECCIÓN');
    $objPHPExcel->getSheet(1)
    -> getStyle ("A1")
    -> getFill ()
    -> setFillType (PHPExcel_Style_Fill :: FILL_SOLID)
    -> getStartColor ()
    -> setRGB ('C8C8C8');
    $objPHPExcel->getSheet(1)
    -> getStyle ("A1:A2")
    -> getFont ()
    -> setBold ( true )
    -> setName ( 'Verdana' )
    -> setSize ( 10 )
    -> getColor ()
    -> setRGB ('000000');
    $objPHPExcel->getSheet(1)
    -> getStyle ('A1:A2')
    -> getAlignment ()
    -> applyFromArray (array ( 'horizontal'  =>  PHPExcel_Style_Alignment :: HORIZONTAL_CENTER ,) );
    $objPHPExcel->getSheet(1)
    -> getStyle ('A3:B3')
    -> getAlignment ()
    -> applyFromArray (array ( 'horizontal'  =>  PHPExcel_Style_Alignment :: VERTICAL_JUSTIFY ,) );
    $objPHPExcel->getSheet(1)
    -> getStyle ("A2")
    -> getFill ()
    -> setFillType (PHPExcel_Style_Fill :: FILL_SOLID);

    $borders = array(
        'borders' => array(
            'allborders' => array(
                'style' => PHPExcel_Style_Border::BORDER_THIN,
            'color' => array('argb' => 'FF000000'),
            )
        ),
    );
    $objPHPExcel->getSheet(1)
    ->setCellValue('A3', 'Código')
    ->setCellValue('B3', "Nombre");
    $objPHPExcel->getSheet(1)
        -> getStyle ("A3:B3")
        -> getFill ()
        -> setFillType (PHPExcel_Style_Fill :: FILL_SOLID)
        -> getStartColor ()
        -> setRGB ('99ddff');
    $objPHPExcel->getSheet(1)->getStyle("A3:B3")->getFont()->setBold(true);
    $objPHPExcel->getSheet(1)->getStyle('A1:B1')->applyFromArray($borders);
    $objPHPExcel->getSheet(1)->getStyle('A2:B2')->applyFromArray($borders);
    $objPHPExcel->getSheet(1)->getStyle('A3:B3')->applyFromArray($borders);

    $objWorksheet = $objPHPExcel->getSheet(1);

    $objPHPExcel->getSheet(1)->getColumnDimension('A')->setAutoSize(true);
    $objPHPExcel->getSheet(1)->getColumnDimension('B')->setAutoSize(true);

    //Hoja de columnas
    $objPHPExcel->getSheet(2)
    ->mergeCells('A1:B1')
    ->mergeCells('A2:B2')
    ->setCellValue('A1', 'CONTABILIDAD')
    ->setCellValue('A2', 'FUENTES DE RECURSOS');
    $objPHPExcel-> getSheet(2)
    -> getStyle ("A1")
    -> getFill ()
    -> setFillType (PHPExcel_Style_Fill :: FILL_SOLID)
    -> getStartColor ()
    -> setRGB ('C8C8C8');
    $objPHPExcel-> getSheet(2)
    -> getStyle ("A1:B2")
    -> getFont ()
    -> setBold ( true )
    -> setName ( 'Verdana' )
    -> setSize ( 10 )
    -> getColor ()
    -> setRGB ('000000');
    $objPHPExcel-> getSheet(2)
    -> getStyle ('A1:B2')
    -> getAlignment ()
    -> applyFromArray (array ( 'horizontal'  =>  PHPExcel_Style_Alignment :: HORIZONTAL_CENTER ,) );
    $objPHPExcel-> getSheet(2)
    -> getStyle ('A3:B2')
    -> getAlignment ()
    -> applyFromArray (array ( 'horizontal'  =>  PHPExcel_Style_Alignment :: VERTICAL_JUSTIFY ,) );
    $objPHPExcel-> getSheet(2)
    -> getStyle ("A2")
    -> getFill ()
    -> setFillType (PHPExcel_Style_Fill :: FILL_SOLID);

    $borders = array(
        'borders' => array(
            'allborders' => array(
                'style' => PHPExcel_Style_Border::BORDER_THIN,
            'color' => array('argb' => 'FF000000'),
            )
        ),
    );
    $objPHPExcel->getSheet(2)
    ->setCellValue('A3', 'Código')
    ->setCellValue('B3', "Nombre");
    $objPHPExcel-> getSheet(2)
        -> getStyle ("A3:B3")
        -> getFill ()
        -> setFillType (PHPExcel_Style_Fill :: FILL_SOLID)
        -> getStartColor ()
        -> setRGB ('99ddff');
    $objPHPExcel->getSheet(2)->getStyle("A3:B3")->getFont()->setBold(true);
    $objPHPExcel->getSheet(2)->getStyle('A1:B1')->applyFromArray($borders);
    $objPHPExcel->getSheet(2)->getStyle('A2:B2')->applyFromArray($borders);
    $objPHPExcel->getSheet(2)->getStyle('A3:B3')->applyFromArray($borders);


    $objPHPExcel->getSheet(2)->getColumnDimension('A')->setAutoSize(true);
    $objPHPExcel->getSheet(2)->getColumnDimension('B')->setAutoSize(true);



    //Formatos
    if(!empty($modalidades)){
        $total = count($modalidades);
        $row = 4;
        for ($i=0; $i < $total ; $i++) {
            $objPHPExcel->getSheet(1)
            ->setCellValue("A$row", $modalidades[$i]['codigo'])
            ->setCellValue("B$row", $modalidades[$i]['nombre']);
            $row++;
        }
    }

    if(!empty($fuentes)){
        $total = count($fuentes);
        $row = 4;
        for ($i=0; $i < $total ; $i++) {
            $objPHPExcel->getSheet(2)
            ->setCellValue("A$row", $fuentes[$i]['codigo'])
            ->setCellValue("B$row", $fuentes[$i]['nombre']);
            $row++;
        }
    }
  
    $objPHPExcel->setActiveSheetIndex(0);
    //----Guardar documento----
    header('Content-Type: application/vnd.ms-excel');
    header('Content-Disposition: attachment;filename="formato_cargue_masivo_paa.xlsx"');
    header('Cache-Control: max-age=0');
    $objWriter=PHPExcel_IOFactory::createWriter($objPHPExcel,'Excel2007');
    $objWriter->save('php://output');
    die();

?>
