<?php
    header("Cache-control: no-cache, no-store, must-revalidate");
    header("Content-Type: text/html;charset=utf8");
    require 'comun.inc';
    require 'funciones.inc';
    require 'conversor.php';
    session_start();
    $linkbd = conectar_v7();
    $linkbd->set_charset("utf8");
    cargarcodigopag($_GET['codpag'], $_SESSION["nivel"]);
    date_default_timezone_set("America/Bogota");
?>
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
    <meta http-equiv="Content-Type" content="text/html" />
    <meta http-equiv="X-UA-Compatible" content="IE=9" />
    <title>:: IDEAL 10 - Tesoreria</title>
    <link href="favicon.ico" rel="shortcut icon" />

    <script>
        //************* ver reporte ************
        //***************************************
        function verep(idfac) {
            document.form1.oculto.value = idfac;
            document.form1.submit();
        }
    </script>
    <script>
        //************* genera reporte ************
        //***************************************
        function genrep(idfac) {
            document.form2.oculto.value = idfac;
            document.form2.submit();
        }
    </script>
    <script>
        function buscacta(e) {
            if (document.form2.cuenta.value != "") {
                document.form2.bc.value = '1';
                document.form2.submit();
            }
        }
    </script>
    <script language="JavaScript1.2">
        function validar() {
            document.form2.submit();
        }
    </script>
    <script>
        function buscater(e) {
            if (document.form2.tercero.value != "") {
                document.form2.bt.value = '1';
                document.form2.submit();
            }
        }
    </script>
    <script>
        function agregardetalle() {
            if (document.form2.numero.value != "" && document.form2.valor.value > 0 && document.form2.banco.value != "") {
                document.form2.agregadet.value = 1;
                //			document.form2.chacuerdo.value=2;
                document.form2.submit();
            }
            else {
                alert("Falta informacion para poder Agregar");
            }
        }
    </script>
    <script>
        function eliminar(variable) {
            if (confirm("Esta Seguro de Eliminar")) {
                document.form2.elimina.value = variable;
                //eli=document.getElementById(elimina);
                vvend = document.getElementById('elimina');
                //eli.value=elimina;
                vvend.value = variable;
                document.form2.submit();
            }
        }
    </script>
    <script>
        //************* genera reporte ************
        //***************************************
        function guardar() {
            vvigencias = document.getElementsByName('dselvigencias[]');
            if (!isNaN(document.form2.recibo.value) && parseFloat(vvigencias.length) == 0) {
                if (confirm("Esta Seguro de Guardar")) {
                    document.form2.oculto.value = 2;
                    document.form2.submit();
                }
            }
            else {
                if (vvigencias.length > 0) {
                    alert('El Predio tiene Pagos Pendientes');
                    document.form2.recibo.focus();
                    document.form2.recibo.select();
                }
                else {
                    alert('Faltan datos para completar el registro' + document.form2.recibo.value);
                    document.form2.recibo.focus();
                    document.form2.recibo.select();
                }
            }
        }
    </script>
    <script>
        function pdf() {
            document.form2.action = "pdfpazysalvo.php";
            document.form2.target = "_BLANK";
            document.form2.submit();
            document.form2.action = "";
            document.form2.target = "";
        }
    </script>
    <script>
        function buscar() {
            // alert("dsdd");
            document.form2.buscav.value = '1';
            document.form2.submit();
        }
    </script>
    <script>
        function buscarc() {
            // alert("dsdd");
            document.form2.brc.value = '1';
            document.form2.submit();
        }
    </script>
    <script>
        function buscavigencias(objeto) {

            //document.form2.buscarvig.value='1';
            vvigencias = document.getElementsByName('dselvigencias[]');
            vtotalpred = document.getElementsByName("dpredial[]");
            vtotaliqui = document.getElementsByName("dhavaluos[]");
            vtotalbomb = document.getElementsByName("dimpuesto1[]");
            vtotalmedio = document.getElementsByName("dimpuesto2[]");
            vtotalintp = document.getElementsByName("dipredial[]");
            vtotalintb = document.getElementsByName("dinteres1[]");
            vtotalintma = document.getElementsByName("dinteres2[]");
            vtotaldes = document.getElementsByName("ddescuentos[]");
            sumar = 0;
            sumarp = 0;
            sumarb = 0;
            sumarma = 0;
            sumarint = 0;
            sumarintp = 0;
            sumarintb = 0;
            sumarintma = 0;
            sumardes = 0;
            for (x = 0; x < vvigencias.length; x++) {
                if (vvigencias.item(x).checked) {
                    sumar = sumar + parseFloat(vtotaliqui.item(x).value);
                    sumarp = sumarp + parseFloat(vtotalpred.item(x).value);
                    sumarb = sumarb + parseFloat(vtotalbomb.item(x).value);
                    sumarma = sumarma + parseFloat(vtotalmedio.item(x).value);
                    sumarint = sumarint + parseFloat(vtotalintp.item(x).value) + parseFloat(vtotalintb.item(x).value) + parseFloat(vtotalintma.item(x).value);
                    sumarintp = sumarintp + parseFloat(vtotalintp.item(x).value);
                    sumarintb = sumarintb + parseFloat(vtotalintb.item(x).value);
                    sumarintma = sumarintma + parseFloat(vtotalintma.item(x).value);
                    sumardes = sumardes + parseFloat(vtotaldes.item(x).value);
                }
            }

            document.form2.totliquida.value = sumar;
            document.form2.totliquida2.value = sumar;
            document.form2.totpredial.value = sumarp;
            document.form2.totbomb.value = sumarb;
            document.form2.totamb.value = sumarma;
            document.form2.totint.value = sumarint;
            document.form2.intpredial.value = sumarintp;
            document.form2.intbomb.value = sumarintb;
            document.form2.intamb.value = sumarintma;
            document.form2.totdesc.value = sumardes;
        }

        function adelante() {
            if (parseFloat(document.form2.maximo.value) > parseFloat(document.form2.numpredial.value)) {
                var idpaz = parseFloat(document.form2.numpredial.value);
                document.form2.action = "teso-pazysalvover.php?idpaz=" + (idpaz + 1);
                document.form2.submit();
            }
        }

        function atrasc() {
            if (1 < parseFloat(document.form2.numpredial.value)) {
                var idpaz = document.form2.numpredial.value;
                document.form2.action = "teso-pazysalvover.php?idpaz=" + (idpaz - 1);
                document.form2.submit();
            }

        }
        function iratras() {
            location.href = "teso-buscapazysalvo.php";
        }
    </script>

    <script src="css/programas.js"></script>
    <link href="css/css2.css" rel="stylesheet" type="text/css" />
    <link href="css/css3.css" rel="stylesheet" type="text/css" />
    <link href="css/tabs.css" rel="stylesheet" type="text/css" />
    <link href="css/style.css" rel="stylesheet" type="text/css" />
</head>

<body>
    <IFRAME src="alertas.php" name="alertas" id="alertas" style="display:none"></IFRAME>
    <span id="todastablas2"></span>
    <table>
        <tr>
            <script>barra_imagenes("teso");</script><?php cuadro_titulos(); ?>
        </tr>
        <tr><?php menu_desplegable("teso"); ?></tr>
    </table>
    <div class="bg-white group-btn p-1"><button type="button" onclick="window.location.href='teso-pazysalvo.php'"
            class="btn btn-white btn-primary-hover d-flex justify-between align-items-center">
            <span>Nuevo</span>
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 -960 960 960">
                <path d="M440-440H200v-80h240v-240h80v240h240v80H520v240h-80v-240Z"></path>
            </svg>
        </button>
        <button type="button" onclick="window.location.href='teso-buscapazysalvo.php'"
            class="btn btn-white btn-primary-hover d-flex justify-between align-items-center">
            <span>Buscar</span>
            <svg class="fill-black group-hover:fill-white w-5 h-5" xmlns="http://www.w3.org/2000/svg"
                viewBox="0 -960 960 960">
                <path
                    d="M784-120 532-372q-30 24-69 38t-83 14q-109 0-184.5-75.5T120-580q0-109 75.5-184.5T380-840q109 0 184.5 75.5T640-580q0 44-14 83t-38 69l252 252-56 56ZM380-400q75 0 127.5-52.5T560-580q0-75-52.5-127.5T380-760q-75 0-127.5 52.5T200-580q0 75 52.5 127.5T380-400Z">
                </path>
            </svg>
        </button>
        <button type="button" onclick="mypop=window.open('teso-principal.php','','');mypop.focus();"
            class="btn btn-white btn-primary-hover d-flex justify-between align-items-center">
            <span>Nueva ventana</span>
            <svg class="fill-black group-hover:fill-white w-5 h-5" xmlns="http://www.w3.org/2000/svg"
                viewBox="0 -960 960 960">
                <path
                    d="M320-120v-80h80v-80H160q-33 0-56.5-23.5T80-360v-400q0-33 23.5-56.5T160-840h640q33 0 56.5 23.5T880-760v400q0 33-23.5 56.5T800-280H560v80h80v80H320ZM160-360h640v-400H160v400Zm0 0v-400 400Z">
                </path>
            </svg>
        </button>
        <button type="button" onclick="pdf()"
            class="btn btn-danger btn-danger-hover d-flex justify-between align-items-center">
            <span>Exportar PDF</span>
            <svg xmlns="http://www.w3.org/2000/svg"
                viewBox="0 0 512 512"><!-- !Font Awesome Free 6.5.1 by @fontawesome - https://fontawesome.com License - https://fontawesome.com/license/free Copyright 2024 Fonticons, Inc. -->
                <path
                    d="M64 464l48 0 0 48-48 0c-35.3 0-64-28.7-64-64L0 64C0 28.7 28.7 0 64 0L229.5 0c17 0 33.3 6.7 45.3 18.7l90.5 90.5c12 12 18.7 28.3 18.7 45.3L384 304l-48 0 0-144-80 0c-17.7 0-32-14.3-32-32l0-80L64 48c-8.8 0-16 7.2-16 16l0 384c0 8.8 7.2 16 16 16zM176 352l32 0c30.9 0 56 25.1 56 56s-25.1 56-56 56l-16 0 0 32c0 8.8-7.2 16-16 16s-16-7.2-16-16l0-48 0-80c0-8.8 7.2-16 16-16zm32 80c13.3 0 24-10.7 24-24s-10.7-24-24-24l-16 0 0 48 16 0zm96-80l32 0c26.5 0 48 21.5 48 48l0 64c0 26.5-21.5 48-48 48l-32 0c-8.8 0-16-7.2-16-16l0-128c0-8.8 7.2-16 16-16zm32 128c8.8 0 16-7.2 16-16l0-64c0-8.8-7.2-16-16-16l-16 0 0 96 16 0zm80-112c0-8.8 7.2-16 16-16l48 0c8.8 0 16 7.2 16 16s-7.2 16-16 16l-32 0 0 32 32 0c8.8 0 16 7.2 16 16s-7.2 16-16 16l-32 0 0 48c0 8.8-7.2 16-16 16s-16-7.2-16-16l0-64 0-64z">
                </path>
            </svg>
        </button>
        <button type="button" onclick="iratras()"
            class="btn btn-success btn-success-hover d-flex justify-between align-items-center">
            <span>Atras</span>
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 -960 960 960">
                <path
                    d="M760-200v-160q0-50-35-85t-85-35H273l144 144-57 56-240-240 240-240 57 56-144 144h367q83 0 141.5 58.5T840-360v160h-80Z">
                </path>
            </svg>
        </button>
    </div>
    <tr>
        <td colspan="3" class="tablaprin" align="center">
            <?php
            $vigencia = date('Y');
            ?>
            <?php
            //*********** 11050501	CAJA PRINCIPAL esta es la cuenta que va a credito en todas las consignacones

            //$_POST[vigencia]=$_SESSION[vigencia];
            // $fec=date("d/m/Y");
            $_POST['fecha'] = $fec;
            //*************cuenta caja
            $sqlr = "select * from tesopazysalvo where id='" . $_GET['idpaz'] . "'";
            $res = mysqli_query($linkbd, $sqlr);
            $consec = 0;
            while ($r = mysqli_fetch_row($res)) {
                $consec += 1;
                $_POST['numpredial'] = $r[0];
                //$fec=date("d/m/Y");
//		 $fec=$r[3];
                preg_match("/([0-9]{4})\-([0-9]{2})\-([0-9]{2})/", $r[3], $fec);
                $fechaf = $fec[3] . "/" . $fec[2] . "/" . $fec[1];
                $_POST['vigencia'] = $fec[1];
                $_POST['fecha'] = $fechaf;
                $_POST['recibo'] = $r[2];
                $_POST['codcat'] = $r[1];
                $_POST['destino'] = $r[5];

            }

            //NEXT
            $sqln = "select max(id) from tesopazysalvo";
            $resn = mysqli_query($linkbd, $sqln);
            $row = mysqli_fetch_row($resn);
            $_POST['maximo'] = $row[0];


            switch ($_POST['tabgroup1']) {
                case 1:
                    $check1 = 'checked';
                    break;
                case 2:
                    $check2 = 'checked';
                    break;
                case 3:
                    $check3 = 'checked';
            }

            ?>
            <form name="form2" method="post" action="">
                <?php

                $_POST['dcuentas'] = array();
                $_POST['dncuentas'] = array();
                $_POST['dtcuentas'] = array();
                $_POST['dvalores'] = array();

                $sqlr = "select *from tesopredios where cedulacatastral='" . $_POST['codcat'] . "' ";
                //echo "s:$sqlr";
                $res = mysqli_query($linkbd, $sqlr);
                while ($row = mysqli_fetch_row($res)) {
                    //$_POST[vigencia]=$row[0];
                    $_POST['catastral'] = $row[0];
                    $_POST['ord'] = $row[1];
                    $_POST['tot'] = $row[2];
                    $_POST['ntercero'] = $row[6];
                    $_POST['tercero'] = $row[5];
                    $_POST['direccion'] = $row[7];
                    $_POST['ha'] = $row[8];
                    $_POST['mt2'] = $row[9];
                    $_POST['areac'] = $row[10];
                    $_POST['avaluo'] = number_format($row[11], 2);
                    $_POST['avaluo2'] = number_format($row[11], 2);
                    $_POST['vavaluo'] = $row[11];
                    $_POST['tipop'] = $row[14];
                    if ($_POST['tipop'] == 'urbano') {
                        $_POST['estrato'] = $row[15];
                        $tipopp = $row[15];
                    } else {
                        $_POST['rangos'] = $row[15];
                        $tipopp = $row[15];
                    }
                    // $_POST[dcuentas][]=$_POST[estrato];
                    $_POST['dtcuentas'][] = $row[1];
                    $_POST['dvalores'][] = $row[5];
                    $_POST['buscav'] = "";
                    $sqlr2 = "select *from tesotarifaspredial where vigencia='" . $_SESSION['vigencia'] . "' and tipo='$_POST[tipop]' and estratos=$tipopp";
                    //echo $sqlr2;
                    $res2 = mysqli_query($linkbd, $sqlr2);
                    while ($row2 = mysqli_fetch_row($res2)) {
                        $_POST['tasa'] = $row2[5];
                        $_POST['predial'] = ($row2[5] / 1000) * $_POST['vavaluo'];
                        $_POST['predial'] = number_format($_POST['predial'], 2);
                    }
                }
                // echo "dc:".$_POST[dcuentas];

                ?>
                <?php
                $sqlr = "select tesoreciboscaja.id_recibos,tesorecaudos.concepto from tesoreciboscaja,tesorecaudos where tesoreciboscaja.id_recibos='$_POST[recibo]' and tesoreciboscaja.id_recaudo=tesorecaudos.id_recaudo and tesoreciboscaja.tipo='3' and tesoreciboscaja.estado='S'";
                //echo $sqlr;
                $_POST['detallerc'] = '';
                $_POST['recibo'] = '';
                $res = mysqli_query($linkbd, $sqlr);
                while ($row = mysqli_fetch_row($res)) {
                    $_POST['recibo'] = $row[0];
                    $_POST['detallerc'] = $row[1];
                }
                if ($_POST['detallerc'] == '') {
                    ?>
                    <script>
                        //alert("Recibo de Caja Incorrecto");
                        document.form2.recibo.focus();
                        document.form2.recibo.select();
                    </script>
                    <?php
                }
                ?>
                <table class="inicio" align="center">
                    <tr>
                        <td class="titulos" colspan="6">Consultar Predio</td>
                        <td width="68" class="cerrar"><a href="teso-principal.php">Cerrar</a></td>
                    </tr>
                    <tr>
                        <td style="width:10%;" class="saludo1">No Paz y Salvo:</td>
                        <td style="width:10%;">
                            <a href="#" onClick="atrasc()"><img src="imagenes/back.png" alt="anterior"
                                    align="absmiddle"></a>
                            <input name="numpredial" id="numpredial" type="text"
                                value="<?php echo $_POST['numpredial'] ?>" style="width:50%"
                                onKeyPress="javascript:return solonumeros(event)" onKeyUp="return tabular(event,this)"
                                readonly>
                            <a href="#" onClick="adelante()"><img src="imagenes/next.png" alt="siguiente"
                                    align="absmiddle"></a>
                            <input type="hidden" value="<?php echo $_POST['maximo'] ?>" name="maximo" id="maximo">
                            <input type="hidden" value="<?php echo $_POST['minimo'] ?>" name="minimo" id="minimo">
                            <input type="hidden" value="<?php echo $_POST['codrec'] ?>" name="codrec" id="codrec">

                            <input id="ord" type="hidden" name="ord" value="<?php echo $_POST['ord'] ?>">
                            <input id="tot" type="hidden" name="tot" value="<?php echo $_POST['tot'] ?>">
                        </td>
                        <td class="saludo1">Fecha:</td>
                        <td>
                            <input name="fecha" id="fecha" type="text" value="<?php echo $_POST['fecha'] ?>"
                                maxlength="10" style="width:100%;" onKeyPress="javascript:return solonumeros(event)"
                                onKeyUp="return tabular(event,this)" readonly>
                        </td>
                        <td class="saludo1">Vigencia:</td>
                        <td>
                            <input name="vigencia" type="text" value="<?php echo $_POST['vigencia'] ?>" maxlength="2"
                                onKeyPress="javascript:return solonumeros(event)" onKeyUp="return tabular(event,this)"
                                readonly>
                        </td>
                    </tr>
                    <tr>
                        <td class="saludo1">Recibo Caja:</td>
                        <td>
                            <input id="recibo" type="text" name="recibo" onKeyUp="return tabular(event,this)"
                                onBlur="buscarc(event)" value="<?php echo $_POST['recibo'] ?>">
                            <input type="hidden" value="0" name="brc">
                        </td>
                        <td colspan="2">
                            <input type="text" name="detallerc" value='<?php echo $_POST['detallerc'] ?>'
                                style="width:100%;">
                        </td>
                        <td class="saludo1" style="width:8%;">C&oacute;digo Catastral:</td>
                        <td>
                            <input id="codcat" type="text" name="codcat" onKeyUp="return tabular(event,this)"
                                onBlur="buscar(event)" style="width:25%;" value="<?php echo $_POST['codcat'] ?>">
                            <input type="hidden" value="0" name="bt"> <a href="#"
                                onClick="mypop=window.open('catastral-ventana.php','','menubar=0,scrollbars=yes, toolbar=no, location=no, width=900,height=500px');mypop.focus();"><img
                                    src="imagenes/buscarep.png" align="absmiddle" border="0"></a>
                            <input id="ord" type="text" name="ord" style="width:10%;" value="<?php echo $_POST['ord'] ?>"
                                readonly>
                            <input id="tot" type="text" name="tot" style="width:10%;" value="<?php echo $_POST['tot'] ?>"
                                readonly>
                            <input type="hidden" name="chacuerdo" value="1">
                            <input type="hidden" value="1" name="oculto">
                            <input type="hidden" value="<?php echo $_POST['buscav'] ?>" name="buscav">
                            <input type="button" name="buscarb" id="buscarb" value="Buscar" onClick="buscar()">
                        </td>
                    </tr>
                    <tr>
                        <td class="saludo1">Destino:</td>
                        <td>
                            <input type="text" name="destino" value="<?php echo $_POST['destino'] ?>">
                        </td>
                    </tr>
                </table>
                <table class="inicio">
                    <tr>
                        <td class="titulos" colspan="9">Informacion Predio</td>
                    </tr>
                    <tr>
                        <td class="saludo1" style="width:10%;">C&oacute;digo Catastral:</td>
                        <td style="width:10%;">
                            <input type="hidden" value="<?php echo $_POST['nbanco'] ?>" name="nbanco">
                            <input name="catastral" type="text" id="catastral" onBlur="buscater(event)"
                                style="width:100%;" onKeyUp="return tabular(event,this)"
                                value="<?php echo $_POST['catastral'] ?>" readonly>
                        </td>

                        <td class="saludo1" style="width:8%;">Avaluo:</td>
                        <td colspan="5">
                            <input name="avaluo" type="text" id="avaluo" onKeyUp="return tabular(event,this)"
                                value="<?php echo $_POST['avaluo'] ?>" style="width:24%;" readonly>
                        </td>
                        <td style="width:20%;"></td>
                    </tr>
                    <tr>
                        <td class="saludo1">Documento:</td>
                        <td><input name="tercero" type="text" id="tercero" onKeyUp="return tabular(event,this)"
                                value="<?php echo $_POST['tercero'] ?>" style="width:100%;" readonly>
                        </td>
                        <td class="saludo1">Propietario:</td>
                        <td colspan="5">
                            <input type="hidden" value="<?php echo $_POST['nbanco'] ?>" name="nbanco">
                            <input name="ntercero" type="text" id="propietario" onKeyUp="return tabular(event,this)"
                                value="<?php echo $_POST['ntercero'] ?>" style="width:100%;" readonly>
                        </td>

                    </tr>
                    <tr>
                        <td class="saludo1">Direccion:</td>
                        <td><input type="hidden" value="<?php echo $_POST['nbanco'] ?>" name="nbanco">
                            <input name="direccion" type="text" id="direccion" onKeyUp="return tabular(event,this)"
                                value="<?php echo $_POST['direccion'] ?>" style="width:100%;" readonly>
                        </td>

                        <td class="saludo1" style="width:8%;">Ha:</td>
                        <td style="width:5%;">
                            <input name="ha" type="text" id="ha" onKeyUp="return tabular(event,this)"
                                value="<?php echo $_POST['ha'] ?>" style="width:100%;" readonly>
                        </td>
                        <td class="saludo1" style="width:8%;">Mt2:</td>
                        <td style="width:8%;">
                            <input name="mt2" type="text" id="mt2" onKeyUp="return tabular(event,this)"
                                value="<?php echo $_POST['mt2'] ?>" style="width:100%;" readonly>
                        </td>
                        <td class="saludo1" style="width:8%;">Area Cons:</td>
                        <td style="width:8%;">
                            <input name="areac" type="text" id="areac" onKeyUp="return tabular(event,this)"
                                value="<?php echo $_POST['areac'] ?>" style="width:100%;" readonly>
                        </td>
                    </tr>


                    <tr>
                        <td width="119" class="saludo1">Tipo:</td>
                        <td width="202"><select name="tipop" onChange="validar();" disabled>
                                <option value="">Seleccione ...</option>
                                <option value="urbano" <?php if ($_POST['tipop'] == 'urbano')
                                    echo "SELECTED" ?>>Urbano
                                    </option>
                                    <option value="rural" <?php if ($_POST['tipop'] == 'rural')
                                    echo "SELECTED" ?>>Rural
                                    </option>
                                </select>
                            </td>
                            <?php
                                if ($_POST['tipop'] == 'urbano') {
                                    ?>
                            <td class="saludo1">Estratos:</td>
                            <td><select name="estrato" disabled>
                                    <option value="">Seleccione ...</option>
                                    <?php
                                    $sqlr = "select *from estratos where estado='S'";
                                    $res = mysqli_query($linkbd, $sqlr);
                                    while ($row = mysqli_fetch_row($res)) {
                                        echo "<option value=$row[0] ";
                                        $i = $row[0];

                                        if ($i == $_POST['estrato']) {
                                            echo "SELECTED";
                                            $_POST['nestrato'] = $row[1];
                                        }
                                        echo ">" . $row[1] . "</option>";
                                    }
                                    ?>
                                </select> <input type="hidden" value="<?php echo $_POST['nestrato'] ?>" name="nestrato">
                            </td>
                            <?php
                                } else {
                                    ?>
                            <td class="saludo1">Rango Avaluo:</td>
                            <td>
                                <select name="rangos">
                                    <option value="">Seleccione ...</option>
                                    <?php
                                    $sqlr = "select *from rangoavaluos where estado='S'";
                                    $res = mysqli_query($linkbd, $sqlr);
                                    while ($row = mysqli_fetch_row($res)) {
                                        echo "<option value=$row[0] ";
                                        $i = $row[0];

                                        if ($i == $_POST['rangos']) {
                                            echo "SELECTED";
                                            $_POST['nrango'] = $row[1] . " - " . $row[2] . " SMMLV";
                                        }
                                        echo ">Entre " . $row[1] . " - " . $row[2] . " SMMLV</option>";
                                    }
                                    ?>
                                </select>
                                <input type="hidden" value="<?php echo $_POST['nrango'] ?>" name="nrango"> <input
                                    type="hidden" value="0" name="agregadet">
                            </td>
                            <?php
                                }
                                ?>
                    </tr>
                </table>
                <div class="subpantallac">
                    <table class="inicio">
                        <tr>
                            <td colspan="12" class="titulos">Periodos Sin Pagar </td>
                        </tr>
                        <tr>
                            <td class="titulos2">Vigencia</td>
                            <td class="titulos2">Codigo Catastral</td>
                            <td class="titulos2">Predial</td>
                            <td class="titulos2">Intereses</td>
                            <td class="titulos2">Sobretasa Bombe</td>
                            <td class="titulos2">Intereses</td>
                            <td class="titulos2">Sobretasa Amb</td>
                            <td class="titulos2">Intereses</td>
                            <td class="titulos2">Descuentos</td>
                            <td class="titulos2">Valor Total</td>
                            <td class="titulos2">Dias Mora</td>
                            <td width="3%" class="titulos2">Sel
                                <input type='hidden' name='buscarvig' id='buscarvig'>
                            </td>
                        </tr>
                        <?php
                        preg_match("/([0-9]{2})\/([0-9]{2})\/([0-9]{4})/", $_POST["fecha"], $fecha);
                        $fechaactual = $fecha[3] . "-" . $fecha[2] . "-" . $fecha[1];
                        $tasaintdiaria = (pow((1 + $_POST['tasamora'] / 100), (1 / 365)) - 1);
                        $cuentavigencias = 0;
                        $tdescuentos = 0;
                        $sqlr = "Select *from tesoprediosavaluos,tesopredios where tesoprediosavaluos.codigocatastral='" . $_POST['catastral'] . "' and   tesoprediosavaluos.estado='S' and tesoprediosavaluos.pago='N' and tesoprediosavaluos.codigocatastral=tesopredios.cedulacatastral order by tesoprediosavaluos.vigencia ASC";
                        $res = mysqli_query($linkbd, $sqlr);
                        $cuentavigencias = mysqli_num_rows($res);
                        while ($r = mysqli_fetch_row($res)) {
                            $sqlr2 = "select *from tesotarifaspredial where vigencia='" . $_SESSION['vigencia'] . "' and tipo='$r[19]' and estratos=$r[20]";
                            //echo $sqlr2;
                            $res2 = mysqli_query($linkbd, $sqlr2);
                            $row2 = mysqli_fetch_row($res2);
                            $base = $r[2];
                            $valorperiodo = $base * ($row2[5] / 1000);
                            $tasav = $row2[5];
                            $predial = $base * ($row2[5] / 1000);
                            $valoringresos = 0;
                            $sidescuentos = 0;
                            //****buscar en el concepto del ingreso *******
                            $intereses = array();
                            $valoringreso = array();
                            $in = 0;
                            if ($cuentavigencias > 1) {
                                if ($_SESSION['vigencia'] == $r[0] && $_POST['descuento'] > 0) {
                                    $diasd = 0;
                                    $totalintereses = 0;
                                    $sidescuentos = 0;
                                } else {
                                    $fechaini = mktime(0, 0, 0, 1, 1, $r[0]);
                                    $fechafin = mktime(0, 0, 0, $fecha[2], $fecha[1], $fecha[3]);
                                    $difecha = $fechafin - $fechaini;
                                    $diasd = $difecha / (24 * 60 * 60);
                                    $diasd = floor($diasd);
                                    $totalintereses = 0;
                                }
                            } else { //********* si solo debe la actual vigencia
                                $diasd = 0;
                                $totalintereses = 0;
                                $tdescuentos = 0;
                                $sidescuentos = 1;
                                // echo "Aqui";
                                if ($_SESSION['vigencia'] == $r[0] && $_POST['descuento'] > 0) {
                                    $pdescuento = $_POST['descuento'] / 100;
                                    $tdescuentos += ceil(($valorperiodo) * $pdescuento);
                                }
                            }

                            $sqlr2 = "select *from tesoingresos_det where codigo='01' and modulo='4' and  estado='S' order by concepto";
                            //echo $sqlr2;
                            $res3 = mysqli_query($linkbd, $sqlr2);
                            while ($r3 = mysqli_fetch_row($res3)) {
                                if ($r3[5] > 0 && $r3[5] < 100) {
                                    //	 echo $valoringresos."-";
                                    if ($r3[2] == '03') {
                                        $valoringreso[0] = ceil($valorperiodo * ($r3[5] / 100));
                                        $valoringresos += ceil($valorperiodo * ($r3[5] / 100));
                                        $intereses[0] = ceil($valoringreso[0] * (pow(1 + $tasaintdiaria, $diasd) - 1));
                                        $totalintereses += $intereses[0];
                                    }
                                    if ($r3[2] == '02') {
                                        $valoringreso[1] = ceil($valorperiodo * ($r3[5] / 100));
                                        $valoringresos += ceil($valorperiodo * ($r3[5] / 100));
                                        $intereses[1] = ceil($valoringreso[1] * (pow(1 + $tasaintdiaria, $diasd) - 1));
                                        $totalintereses += $intereses[1];
                                    }
                                    if ($sidescuentos == 1 && '03' == $r3[2]) {
                                        $tdescuentos += ceil($valoringreso[0] * $pdescuento);
                                    }
                                }
                            }
                            $valorperiodo += $valoringresos;
                            $ipredial = ceil($predial * (pow(1 + $tasaintdiaria, $diasd) - 1));
                            $totalpredial = ceil($valorperiodo + $totalintereses + $ipredial);
                            $totalpagar = ceil($totalpredial - ceil($tdescuentos));
                            $ch = esta_en_array($_POST['dselvigencias'], $r[0]);
                            if ($ch == 1) {
                                $chk = "checked";
                            }
                            //*************
                            echo "<tr><td class='saludo1'><input name='dvigencias[]' value='" . $r[0] . "' type='text' size='4' readonly></td><td class='saludo1'><input name='dcodcatas[]' value='" . $r[1] . "' type='text' size='16' readonly><input name='dvaloravaluo[]' value='" . $base . "' type='hidden' ><input name='dtasavig[]' value='" . $tasav . "' type='hidden' ></td><td class='saludo1'><input name='dpredial[]' value='" . $predial . "' type='text' size='12' readonly></td><td class='saludo1'><input name='dipredial[]' value='" . $ipredial . "' type='text' size='7' readonly></td><td class='saludo1'><input name='dimpuesto1[]' value='" . $valoringreso[0] . "' type='text'  size='12' readonly></td><td class='saludo1'><input name='dinteres1[]' value='" . $intereses[0] . "' type='text' size='7' readonly></td><td class='saludo1'><input name='dimpuesto2[]' value='" . $valoringreso[1] . "' type='text'  size='12' readonly></td><td class='saludo1'><input name='dinteres2[]' value='" . $intereses[1] . "' type='text' size='7' readonly></td><td class='saludo1'><input type='text' name='ddescuentos[]' value='$tdescuentos' size='6' readonly></td><td class='saludo1'><input name='davaluos[]' value='" . number_format($totalpagar, 2) . "' type='text' size='10' readonly><input name='dhavaluos[]' value='" . $totalpagar . "' type='hidden' ></td><td class='saludo1'><input type='text' name='dias[]' value='$diasd' size='4' readonly></td><td class='saludo1'><input type='checkbox' name='dselvigencias[]' value='$r[0]' onClick='buscavigencias(this)' $chk></td></tr>";
                            $_POST['totalc'] = $_POST['totalc'] + $_POST['davaluos'][$x];
                            $_POST['totalcf'] = number_format($_POST['totalc'], 2, ".", ",");
                            //$ageliqui=$ageliqui." ".$_POST[dselvigencias][$x];
                        }
                        $resultado = convertir($_POST['totliquida']);
                        $_POST['letras'] = $resultado . " PESOS M/CTE";
                        ?>
                    </table>
                </div>

                <?php
                if ($_POST['oculto'] == '2') {
                    preg_match("/([0-9]{2})\/([0-9]{2})\/([0-9]{4})/", $_POST["fecha"], $fecha);
                    $fechaf = $fecha[3] . "-" . $fecha[2] . "-" . $fecha[1];
                    $sqlr = "insert into tesopazysalvo (codigocatastral,idrecibo,fecha,estado,destino) values ('$_POST[codcat]',$_POST[recibo],$fechaf,'S','$_POST[destino]')";
                    if (!mysqli_query($linkbd, $sqlr)) {
                        echo "<table class='inicio'><tr><td class='saludo1'>No Se ha podido Generar el Paz y Salvo <img src='imagenes\alert.png'></td></tr></table>";
                    } else {
                        echo "<table class='inicio'><tr><td class='saludo1'>Se ha  Generado el Paz y Salvo <img src='imagenes\confirm.png'></td></tr></table>";
                        $idps = mysqli_insert_id($linkbd);
                        $_POST['numpredial'] = $idps;
                    }
                }
                ?>
            </form>
        </td>
    </tr>
    </table>
</body>

</html>
