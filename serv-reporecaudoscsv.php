<?php
header("content-disposition: attachment;filename=reporecaudos.xls");
header("Content-Type: application/vnd.ms-excel");
header("Expires: 0");
header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
require"comun.inc";
require"funciones.inc";
require"serviciospublicos.inc";
session_start();
$linkbd=conectar_bd();
$vigencias=$_GET['vigencias'];
$mes=$_GET['mes'];
$tpserv=$_GET['tpserv'];
$estrato=$_GET['estrato'];
?>
<html>
<body>
	<?php
 	$meses=array('','Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre');
	$vigusu=vigencia_usuarios($_SESSION[cedulausu]);
	$vigencia=$vigusu;
  	$vact=$vigusu;  
		if($tpserv!="")
			$crit2 = " and servliquidaciones_det.servicio = ".$tpserv;
		else
			$crit2="";
		if($estrato!="")
			$crit3 = " and servliquidaciones_det.estrato = ".$estrato;
		else
			$crit3="";

		$crit4 = "WHERE servliquidaciones.vigencia = ".$vigencias." and (servliquidaciones.mes=$mes or  servliquidaciones.mesfin=$mes)";

		$sqlr1="SELECT servliquidaciones_det.servicio, servservicios.nombre, servestratos.tipo, servestratos.descripcion, SUM(servliquidaciones_det.tarifa), SUM(servliquidaciones_det.subsidio), SUM(servliquidaciones_det.valorliquidacion), SUM(servliquidaciones_det.saldo), COUNT(servliquidaciones_det.estrato), servestratos.id FROM servliquidaciones INNER JOIN ((servliquidaciones_det INNER JOIN servservicios ON servliquidaciones_det.servicio=servservicios.codigo) INNER JOIN servestratos ON servliquidaciones_det.estrato=servestratos.id) ON servliquidaciones_det.id_liquidacion=servliquidaciones.id_liquidacion ".$crit4." ".$crit2."  ".$crit3." GROUP BY servliquidaciones_det.servicio, servliquidaciones_det.estrato";		
		$resp1 = mysql_query($sqlr1,$linkbd);
		$ntr = mysql_num_rows($resp1);
//echo  "sq:".$sqlr1;
echo "<table class='inicio' align='center' >
	<tr>
		<td width='150' class='titulos2' style='border-top: 1px solid; border-bottom: 1px solid; border-left: 1px solid;'>Tipo de Servicio</td>
		<td width='150' class='titulos2' style='border-top: 1px solid; border-bottom: 1px solid;'>Estrato</td>
		<td width='150' class='titulos2' style='border-top: 1px solid; border-bottom: 1px solid; border-right: 1px solid;'>Usuarios</td>
		<td width='50' class='titulos2' style='border-top: 1px solid; border-bottom: 1px solid; border-left: 1px solid;'>Tarifa</td>
		<td width='50' class='titulos2' style='border-top: 1px solid; border-bottom: 1px solid;'>Subsidio</td>
		<td width='50' class='titulos2' style='border-top: 1px solid; border-bottom: 1px solid;'>Contribución</td>
		<td width='50' class='titulos2' style='border-top: 1px solid; border-bottom: 1px solid;'>Saldos Anteriores</td>
		<td width='50' class='titulos2' style='border-top: 1px solid; border-bottom: 1px solid; border-right: 1px solid;'>Valor Facturado</td>
		<td width='50' class='titulos2' style='border-top: 1px solid; border-bottom: 1px solid; border-left: 1px solid;'>Tarifa</td>
		<td width='50' class='titulos2' style='border-top: 1px solid; border-bottom: 1px solid;'>Subsidio</td>
		<td width='50' class='titulos2' style='border-top: 1px solid; border-bottom: 1px solid;'>Contribución</td>
		<td width='50' class='titulos2' style='border-top: 1px solid; border-bottom: 1px solid;'>Saldos Anteriores</td>
		<td width='50' class='titulos2' style='border-top: 1px solid; border-bottom: 1px solid; border-right: 1px solid;'>Valor Recaudado</td>
	</tr>";	
	$iter='zebra1';
	$iter2='zebra2';
	$totusu=0;
	$totftar=0;
	$totfsub=0;
	$totfcon=0;
	$totfant=0;
	$totfval=0;
	$totrtar=0;
	$totrsub=0;
	$totrcon=0;
	$totrant=0;
	$totrval=0;
 	while ($row =mysql_fetch_row($resp1)) {	
		//echo $sqlr;
		$contrib=$row[4]-$row[5];
		echo "<tr class='$iter' style='text-transform:uppercase' onMouseOver=\"anterior=this.style.backgroundColor;this.style.backgroundColor='#40b3ff';\" onMouseOut=\"this.style.backgroundColor=anterior\">
			<td >$row[0] $row[1]</td>
			<td >$row[2] - $row[3]</td>
			<td align='right' style='border-right: 1px solid;'>".number_format($row[8],0,",",".")."</td>
			<td align='right'>".number_format($row[4],2,",",".")."</td>
			<td align='right'>".number_format($row[5],2,",",".")."</td>
			<td align='right'>".number_format($contrib,2,",",".")."</td>
			<td align='right'>".number_format($row[7],2,",",".")."</td>
			<td align='right' style='border-right: 1px solid;'>".number_format($row[6],2,",",".")."</td>";
			$sqlr="SELECT SUM(servreciboscaja.valor), SUM(servreciboscaja_det.tarifa), SUM(servreciboscaja_det.subsidio), SUM(servreciboscaja_det.saldoanterior) FROM (servreciboscaja INNER JOIN servreciboscaja_det ON servreciboscaja_det.id_recibos=servreciboscaja.id_recibos) INNER JOIN (servliquidaciones INNER JOIN servliquidaciones_det ON servliquidaciones_det.id_liquidacion=servliquidaciones.id_liquidacion) ON servreciboscaja.id_recaudo=servliquidaciones.id_liquidacion WHERE servliquidaciones.vigencia = ".$vigencias." and (servliquidaciones.mes=$mes or  servliquidaciones.mesfin=$mes) AND servliquidaciones_det.servicio=$row[0] AND servliquidaciones_det.estrato=$row[9]";		
			$rsr = mysql_query($sqlr,$linkbd);
			$wr=mysql_fetch_array($rsr);
			$rcont=$wr[1]-$wr[2];
			echo"<td align='right'>".number_format($wr[1],2,",",".")."</td>
			<td align='right'>".number_format($wr[2],2,",",".")."</td>
			<td align='right'>".number_format($rcont,2,",",".")."</td>
			<td align='right'>".number_format($wr[3],2,",",".")."</td>
			<td align='right' style='border-right: 1px solid;'>".number_format($wr[0],2,",",".")."</td>";

		echo"</tr>";	
	 	$con+=1;
	 	$aux=$iter;
	 	$iter=$iter2;
	 	$iter2=$aux;
	 	$totusu+=$row[8];
	 	$totftar+=$row[4];
	 	$totfsub+=$row[5];
	 	$totfcon+=$contrib;
	 	$totfant+=$row[7];
	 	$totfval+=$row[6];
	 	$totrtar+=$wr[1];
	 	$totrsub+=$wr[2];
	 	$totrcon+=$rcont;
	 	$totrant+=$wr[3];
	 	$totrval+=$wr[0];
 	}
 	echo "<tr>
		<td colspan='2'></td>
		<td align='right' style='border-right: 1px solid;'>".number_format($totusu,0,",",".")."</td>
		<td align='right'>".number_format($totftar,2,",",".")."</td>
		<td align='right'>".number_format($totfsub,2,",",".")."</td>
		<td align='right'>".number_format($totfcon,2,",",".")."</td>
		<td align='right'>".number_format($totfant,2,",",".")."</td>
		<td align='right' style='border-right: 1px solid;'>".number_format($totfval,2,",",".")."</td>
		<td align='right'>".number_format($totrtar,2,",",".")."</td>
		<td align='right'>".number_format($totrsub,2,",",".")."</td>
		<td align='right'>".number_format($totrcon,2,",",".")."</td>
		<td align='right'>".number_format($totrant,2,",",".")."</td>
		<td align='right' style='border-right: 1px solid;'>".number_format($totrval,2,",",".")."</td>
	</tr>";
 	echo"</table>";
 	?>
</body>
</html>