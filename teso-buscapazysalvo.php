<?php
header("Cache-control: no-cache, no-store, must-revalidate");
header("Content-Type: text/html;charset=utf8");
require 'comun.inc';
require 'funciones.inc';
session_start();
$linkbd = conectar_v7();
$linkbd->set_charset("utf8");
cargarcodigopag($_GET['codpag'], $_SESSION["nivel"]);
date_default_timezone_set("America/Bogota");
?>
<!DOCTYPE>
<html lang="es">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1">
    <meta name="viewport" content="user-scalable=no">
    <title>:: IDEAL 10 - Tesoreria</title>
    <link href="favicon.ico" rel="shortcut icon" />
    <link href="css/css2z.css?<?php echo date('d_m_Y_h_i_s'); ?>" rel="stylesheet" />
    <link href="css/css3.css?<?php echo date('d_m_Y_h_i_s'); ?>" rel="stylesheet" />
    <link href="css/style.css?<?php echo date('d_m_Y_h_i_s'); ?>" rel="stylesheet" />
    <script type="text/javascript" src="css/calendario.js?<?php echo date('d_m_Y_h_i_s'); ?>"></script>
    <script type="text/javascript" src="css/programas.js?<?php echo date('d_m_Y_h_i_s'); ?>"></script>
    <script>
        function buscacta(e) {
            if (document.form2.cuenta.value != "") {
                document.form2.bc.value = '1';
                document.form2.submit();
            }
        }
        function validar() {
            document.form2.submit();
        }
        function buscater(e) {
            if (document.form2.tercero.value != "") {
                document.form2.bt.value = '1';
                document.form2.submit();
            }
        }
        function agregardetalle() {
            if (document.form2.numero.value != "" && document.form2.valor.value > 0 && document.form2.banco.value != "") {
                document.form2.agregadet.value = 1;
                document.form2.submit();
            } else {
                alert("Falta informacion para poder Agregar");
            }
        }
        function eliminar(idr) {
            if (confirm("Esta Seguro de Eliminar el Recibo de Caja")) {
                document.form2.oculto.value = 2;
                document.form2.var1.value = idr;
                document.form2.submit();
            }
        }
        function guardar() {
            if (document.form2.fecha.value != '') {
                if (confirm("Esta Seguro de Guardar")) {
                    document.form2.oculto.value = 2;
                    document.form2.submit();
                }
            } else {
                alert('Faltan datos para completar el registro');
                document.form2.fecha.focus();
                document.form2.fecha.select();
            }
        }
        function pdf() {
            document.form2.action = "teso-pdfconsignaciones.php";
            document.form2.target = "_BLANK";
            document.form2.submit();
            document.form2.action = "";
            document.form2.target = "";
        }
    </script>
</head>

<body>
    <IFRAME src="alertas.php" name="alertas" id="alertas" style="display:none"></IFRAME>
    <span id="todastablas2"></span>
    <table>
        <tr>
            <script>barra_imagenes("teso");</script><?php cuadro_titulos(); ?>
        </tr>
        <tr><?php menu_desplegable("teso"); ?></tr>
    </table>
    <div class="bg-white group-btn p-1"><button type="button" onclick="window.location.href='teso-pazysalvo.php'"
            class="btn btn-white btn-primary-hover d-flex justify-between align-items-center">
            <span>Nuevo</span>
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 -960 960 960">
                <path d="M440-440H200v-80h240v-240h80v240h240v80H520v240h-80v-240Z"></path>
            </svg>
        </button>
        <button type="button" onclick="document.form2.submit();"
            class="btn btn-white btn-primary-hover d-flex justify-between align-items-center">
            <span>Buscar</span>
            <svg class="fill-black group-hover:fill-white w-5 h-5" xmlns="http://www.w3.org/2000/svg"
                viewBox="0 -960 960 960">
                <path
                    d="M784-120 532-372q-30 24-69 38t-83 14q-109 0-184.5-75.5T120-580q0-109 75.5-184.5T380-840q109 0 184.5 75.5T640-580q0 44-14 83t-38 69l252 252-56 56ZM380-400q75 0 127.5-52.5T560-580q0-75-52.5-127.5T380-760q-75 0-127.5 52.5T200-580q0 75 52.5 127.5T380-400Z">
                </path>
            </svg>
        </button>
        <button type="button" onclick="mypop=window.open('teso-principal.php','','');mypop.focus();"
            class="btn btn-white btn-primary-hover d-flex justify-between align-items-center">
            <span>Nueva ventana</span>
            <svg class="fill-black group-hover:fill-white w-5 h-5" xmlns="http://www.w3.org/2000/svg"
                viewBox="0 -960 960 960">
                <path
                    d="M320-120v-80h80v-80H160q-33 0-56.5-23.5T80-360v-400q0-33 23.5-56.5T160-840h640q33 0 56.5 23.5T880-760v400q0 33-23.5 56.5T800-280H560v80h80v80H320ZM160-360h640v-400H160v400Zm0 0v-400 400Z">
                </path>
            </svg>
        </button>
    </div>
    <form name="form2" method="post" action="teso-buscapazysalvo.php">
        <table class="inicio" align="center">
            <tr>
                <td class="titulos" colspan="6">:. Buscar Paz y Salvos </td>
                <td width="70" class="cerrar"><a href="teso-principal.php">Cerrar</a></td>
            </tr>
            <tr>
                <td width="168" class="saludo1">N&uacute;mero:</td>
                <td width="154"><input name="numero" type="text" value="">
                </td>
                <td width="144" class="saludo1">C&oacute;digo Catastral: </td>
                <td width="498"><input name="nombre" type="text" value="" size="80"></td>
                <input name="oculto" id="oculto" type="hidden" value="1">
                <input name="var1" type="hidden" value=<?php echo $_POST['var1']; ?>>
            </tr>
        </table>
        <div class="subpantallap" style="height:68.5%; width:99.6%; overflow-x:hidden;">
            <?php
            $sqlr = "SELECT * FROM tesopazysalvo WHERE id_recaudo=$_POST[var1]";
            $resp = mysqli_query($linkbd, $sqlr);
            $row = mysqli_fetch_row($resp);
            $oculto = $_POST['oculto'];
            if (true) {
                $crit1 = " ";
                $crit2 = " ";
                if ($_POST['numero'] != "") {
                    $crit1 = "AND id like '%" . $_POST['numero'] . "%'";
                }
                if ($_POST['nombre'] != "") {
                    $crit2 = "AND codigocatastral like '%" . $_POST['nombre'] . "%'";
                }
                $sqlr = "SELECT * FROM tesopazysalvo WHERE idrecibo >-1 $crit1 $crit2 ORDER BY id DESC";
                $resp = mysqli_query($linkbd, $sqlr);
                $ntr = mysqli_num_rows($resp);
                $con = 1;
                echo "
						<table class='inicio' align='center' >
							<tr>
								<td colspan='6' class='titulos'>.: Resultados Busqueda:</td>
							</tr>
							<tr>
								<td colspan='6'>Recaudos Encontrados: $ntr</td>
							</tr>
							<tr>
								<td width='150' class='titulos2'>No Paz y Salvo</td><td class='titulos2'>Cod Catastral</td>
								<td class='titulos2'>Fecha</td>
								<td class='titulos2'>Recibo Caja</td>
								<td class='titulos2'>Estado</td>
								<td class='titulos2' width='5%'><center>Anular</td>
							</tr>";
                $iter = 'saludo1a';
                $iter2 = 'saludo2';
                while ($row = mysqli_fetch_row($resp)) {
                    if ($row[4] == 'S') {
                        $imgsem = "src='imagenes/sema_verdeON.jpg' title='Activo'";
                    } else {
                        $imgsem = "src='imagenes/sema_rojoON.jpg' title='Inactivo'";
                    }
                    echo "
								<tr class='$iter' onDblClick=\"location.href='teso-pazysalvover.php?idpaz=$row[0]'\">
									<td>$row[0]</td>
									<td>$row[1]</td>
									<td>$row[3]</td>
									<td>$row[2]</td>
									<td><center><img $imgsem style='width:20px'/></center></td>";
                    if ($row[4] == 'S') {
                        echo "<td ><a href='#' onClick=eliminar($row[0])><center><img src='imagenes/anular.png'></center></a></td></tr>";
                    }
                    if ($row[4] == 'N' || $row[4] == 'P') {
                        echo "<td></td></tr>";
                    }
                    $con += 1;
                    $aux = $iter;
                    $iter = $iter2;
                    $iter2 = $aux;
                }
                echo "</table>";
            }
            ?>
        </div>
    </form>
</body>

</html>
