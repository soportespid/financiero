<?php
	require_once("tcpdf2/tcpdf_include.php");
	require('comun.inc');
	require"funciones.inc";
	session_start();
	class MYPDF extends TCPDF 
	{
		public function Header() {}
		public function Footer() 
		{
			
		}
	}
	$pdf = new MYPDF('P','mm','A4', true, 'iso-8859-1', false);
	$pdf->SetDocInfoUnicode (true);
	// set document information
	$pdf->SetCreator(PDF_CREATOR);
	$pdf->SetAuthor('G&CSAS');
	$pdf->SetTitle('Factura');
	$pdf->SetSubject('Factura');
	$pdf->SetKeywords('');
	$pdf->SetMargins(0, 0, 0);
	$pdf->SetHeaderMargin(0);
	$pdf->SetFooterMargin(0);
	$pdf->SetAutoPageBreak(TRUE, 0);
	if (@file_exists(dirname(__FILE__).'/lang/spa.php')) 
	{
		require_once(dirname(__FILE__).'/lang/spa.php');
		$pdf->setLanguageArray($l);
	}
	ini_set('max_execution_time', 7200);
	$yy=-2;
	$xx=0;
	$linkbd=conectar_v7();
	for($numfac=$_POST['facturaInicial'];$numfac<=$_POST['facturaFinal'];$numfac++)
	{
		$pdf->AddPage();
		
		////////////////////////////////////////////////////////////
		
		$pdf->SetFont('helvetica', '', 9);
		$pdf->SetY(30+$yy);
		$pdf->Cell(74+$mxcor);
		$pdf->Cell(30,0,date('d/m/Y',strtotime($_POST['fechaimpresion'])),0,0,'L',FALSE);
		
		$pdf->SetFont('helvetica', '', 10);
		$pdf->SetY(39+$yy);
		$pdf->Cell(52+$mxcor);
		$pdf->Cell(30,0,date('d/m/Y',strtotime($_POST['fechainicial'])),0,0,'L',FALSE);
		
		$pdf->SetY(39+$yy);
		$pdf->Cell(74+$mxcor);
		$pdf->Cell(30,0,date('d/m/Y',strtotime($_POST['fechafinal'])),0,0,'L',FALSE);
		
		$pdf->SetY(39+$yy);
		$pdf->Cell(90+$mxcor);
		$pdf->Cell(30,0,$numfac,0,0,'R',FALSE);
		
		////////////////////////////////////////////////////////////
		
		$pdf->SetFont('helvetica', '', 9);
		$pdf->SetY(51+$yy);
		$pdf->Cell(37+$mxcor);
		$pdf->Cell(30,0,$_POST['codigocliente'][$numfac],0,0,'L',FALSE);
		
		$pdf->SetY(51+$yy);
		$pdf->Cell(102+$mxcor);
		$pdf->Cell(30,0,$_POST['estratocliente'][$numfac],0,0,'L',FALSE);
		
		$pdf->SetY(56+$yy);
		$pdf->Cell(37+$mxcor);
		$pdf->Cell(30,0,$_POST['tercerocliente'][$numfac],0,0,'L',FALSE);
		
		$pdf->SetY(56+$yy);
		$pdf->Cell(102+$mxcor);
		$pdf->Cell(30,0,$_POST['usosuelocliente'][$numfac],0,0,'L',FALSE);
		
		$pdf->SetY(61+$yy);
		$pdf->Cell(37+$mxcor);
		$pdf->Cell(30,0,$_POST['direccioncliente'][$numfac],0,0,'L',FALSE);
		
		$pdf->SetY(61+$yy);
		$pdf->Cell(102+$mxcor);
		$pdf->Cell(30,0,$_POST['barriocliente'][$numfac],0,0,'L',FALSE);
		
		////////////////////////////////////////////////////////////
		if($_POST["servicio$numfac"][1]['1_101'] != ''){$cargofijo = $_POST["servicio$numfac"][1]['1_101'];}
		else {$cargofijo = 0;}
		if($_POST["servicio$numfac"][1]['2_101'] != ''){$consumo = $_POST["servicio$numfac"][1]['2_101'];}
		else {$consumo = 0;}
		if($_POST["servicio$numfac"][1]['9_101'] != ''){$otroscobros = $_POST["servicio$numfac"][1]['9_101'];}
		else {$otroscobros=0;}
		if($_POST["servicio$numfac"][1]['5_101'] != ''){$subsidios = $_POST["servicio$numfac"][1]['5_101'];}
		else {$subsidios = 0;}
		if($_POST["servicio$numfac"][1]['6_101'] != ''){$exoneraciones = $_POST["servicio$numfac"][1]['6_101'];}
		else {$exoneraciones = 0;}
		if($_POST["servicio$numfac"][1]['7_101'] != ''){$contribucion = $_POST["servicio$numfac"][1]['7_101'];}
		else {$contribucion = 0;}

		if($_POST["servicio$numfac"][1]['8_101'] != ''){$saldoanterior = $_POST["servicio$numfac"][1]['8_101'];}
		else {$saldoanterior = 0;}

		$sqlAnticipo = "SELECT SUM(debito) FROM srvdetalles_facturacion WHERE numero_facturacion = $numfac AND tipo_movimiento = '204'";
		$rowAnticipo = mysqli_fetch_row(mysqli_query($linkbd, $sqlAnticipo));
		$exoneraciones = $exoneraciones * -1;
		$saldoafavor = $rowAnticipo[0] + $exoneraciones;
		$otros = $otroscobros + $contribucion;
		$total = $cargofijo + $consumo + $otros + $saldoanterior - $saldoafavor;
			
		$pdf->SetFont('helvetica', '', 9);
		
		$pdf->SetY(76+$yy);
		$pdf->Cell(105+$mxcor);
		$pdf->Cell(30,0,number_format($cargofijo,2,".",","),0,0,'R',FALSE);
		
		$pdf->SetY(80+$yy);
		$pdf->Cell(105+$mxcor);
		$pdf->Cell(30,0,number_format($consumo,2,".",","),0,0,'R',FALSE);
		
		$pdf->SetY(84+$yy);
		$pdf->Cell(105+$mxcor);
		$pdf->Cell(30,0,number_format($saldoanterior,2,".",","),0,0,'R',FALSE);
		
		$pdf->SetY(88+$yy);
		$pdf->Cell(105+$mxcor);
		$pdf->Cell(30,0,number_format($saldoafavor,2,".",","),0,0,'R',FALSE);
		
		$pdf->SetY(92+$yy);
		$pdf->Cell(105+$mxcor);
		$pdf->Cell(30,0,number_format($otros,2,".",","),0,0,'R',FALSE);
		
		$pdf->SetY(98+$yy);
		$pdf->Cell(105+$mxcor);
		$pdf->Cell(30,0,number_format($total,2,".",","),0,0,'R',FALSE);
		
		$pdf->SetFont('helvetica', '', 16);
		$pdf->SetY(108+$yy);
		$pdf->Cell(105+$mxcor);
		$pdf->Cell(30,0,number_format($total,2,".",","),0,0,'R',FALSE);
		
		$fechap = explode('-', $_POST['fechalimite']);
		$nuevafecha = "$fechap[2] de ".mesletras($fechap[1])." de $fechap[0]";
		$pdf->SetFont('helvetica', '', 9);
		$pdf->SetY(121+$yy);
		$pdf->Cell(108+$mxcor);
		$pdf->Cell(30,0,$nuevafecha,0,0,'R',FALSE);
		
		////////////////////////////////////////////////////////////
		
		$pdf->SetFont('helvetica', '', 9);
		$pdf->SetY(26+$yy);
		$pdf->Cell(153+$mxcor);
		$pdf->Cell(30,0,$_POST['codigocliente'][$numfac],0,0,'L',FALSE);
		
		$pdf->SetY(36+$yy);
		$pdf->Cell(153+$mxcor);
		$pdf->MultiCell(40,0,$_POST['tercerocliente'][$numfac],0,'L',false,1,'','',true,0,false,true,19,'T',false);
		
		$pdf->SetY(48+$yy);
		$pdf->Cell(153+$mxcor);
		$pdf->MultiCell(40,0,$_POST['direccioncliente'][$numfac],0,'L',false,1,'','',true,0,false,true,19,'T',false);
		
		$pdf->SetFont('helvetica', '', 8);
		$pdf->SetY(62+$yy);
		$pdf->Cell(175+$mxcor);
		$pdf->Cell(30,0,date('d/m/Y',strtotime($_POST['fechaimpresion'])),0,0,'L',FALSE);
		
		$pdf->SetFont('helvetica', '', 10);
		$pdf->SetY(70+$yy);
		$pdf->Cell(153+$mxcor);
		$pdf->Cell(30,0,date('d/m/Y',strtotime($_POST['fechainicial']))."  ".date('d/m/Y',strtotime($_POST['fechafinal'])),0,0,'L',FALSE);
		
		$pdf->SetY(84+$yy);
		$pdf->Cell(150+$mxcor);
		$pdf->Cell(30,0,$numfac,0,0,'R',FALSE);
		
		$pdf->SetFont('helvetica', '', 16);
		$pdf->SetY(98+$yy);
		$pdf->Cell(150+$mxcor);
		$pdf->Cell(30,0,number_format($total,2,".",","),0,0,'R',FALSE);
		
		$pdf->SetFont('helvetica', '', 9);
		$pdf->SetY(117+$yy);
		$pdf->Cell(156+$mxcor);
		$pdf->Cell(30,0,$nuevafecha,0,0,'R',FALSE);
		
		////////////////////////////////////////////////////////////
		$pdf->SetFont('helvetica', '', 10);
		$pdf->SetY(104+$yy);
		$pdf->Cell(60+$mxcor);
		$pdf->MultiCell(40,0,"SI SE ENCUENTRA A PAZ Y SALVO. HAGA CASO OMISO A ESTA FACTURA",0,'L',false,1,'','',true,0,false,true,19, 'T', false);
	}

	$pdf->Output('Listadofacturas.pdf', 'I');
?>