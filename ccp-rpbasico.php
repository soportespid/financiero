<?php
    header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=utf8");

	require 'comun.inc';
	require 'funciones.inc';

    session_start();
    date_default_timezone_set("America/Bogota");

?>

<html lang="es">
    <head>
        <meta charset="utf-8"/>
		<meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1"/>
		<meta name="viewport" content="user-scalable=no">
        <title>:: IDEAL 10 - Presupuesto</title>
        <link href="favicon.ico" rel="shortcut icon"/>
        <link href="css/css2.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
        <link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
        <link href="css/css4.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
        <script type="text/javascript" src="css/programas.js"></script>
		<script type="text/javascript" src="css/calendario.js"></script>
		<script type="text/javascript" src="JQuery/jquery-2.1.4.min.js"></script>

        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">

        <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
        <script type="text/javascript" src="bootstrap/fontawesome.5.11.2/js/all.js"></script>
        <link href="css/style.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css"/>
        <script>
            function cambiaFecha(){
                var fecha = document.getElementById('fecha').value;
                fecArray = fecha.split('/');
                document.getElementById('vigencia').value = fecArray[2];
            }
        </script>

        <script>
            function despliegamodal2(_valor,_nomve,_vaux, pos = '', sector = '')
			{
				document.getElementById("bgventanamodal2").style.visibility=_valor;
				if(_valor=="hidden")
				{
					document.getElementById('ventana2').src="";
				}
				else
				{
					switch(_nomve)
					{
						case "1":	document.getElementById('ventana2').src="registroccpet-ventana01.php?vigencia="+_vaux;break;
						case "2":	document.getElementById('ventana2').src="tercerosgral-ventana01.php?objeto=tercero&nobjeto=ntercero&tnfoco=detallegreso";break;
						case "3":	document.getElementById('ventana2').src="reversar-cxp.php?vigencia="+_vaux;break;
						case "4":	document.getElementById('ventana2').src="ventana-servicio.php?vigencia="+_vaux;break;
						case "5":	document.getElementById('ventana2').src="parametrizarCuentaccpet-ventana01.php?cuenta="+_vaux+"&posicion="+pos+"&sector="+sector;break;
                        case '6':	document.getElementById('ventana2').src="cuentasbancarias-ventana01.php?tipoc=D";break;
					}
				}
			}
        </script>

        <style>
            .c9 input[type="checkbox"]:not(:checked),
            .c9 input[type="checkbox"]:checked {
                position: absolute !important;
                left: -9999px !important;
            }
            .c9 input[type="checkbox"]:not(:checked) +  #t9,
            .c9 input[type="checkbox"]:checked +  #t9 {
                position: relative !important;
                padding-left: 1.95em !important;
                cursor: pointer !important;
            }

            /* checkbox aspect */
            .c9 input[type="checkbox"]:not(:checked) +  #t9:before,
            .c9 input[type="checkbox"]:checked +  #t9:before {
                content: '' !important;
                position: absolute !important;
                left: 0 !important; top: 5 !important;
                width: 1.55em !important; height: 1.55em !important;
                border: 2px solid #ccc !important;
                background: #fff !important;
                border-radius: 4px !important;
                box-shadow: inset 0 1px 3px rgba(0,0,0,.1) !important;
            }
            /* checked mark aspect */
            .c9 input[type="checkbox"]:not(:checked) +  #t9:after,
            .c9 input[type="checkbox"]:checked + #t9:after {
                content: url(imagenes/tilde.png) !important;
                position: absolute !important;
                top: .4em; left: .3em !important;
                font-size: 1.3em !important;
                line-height: 0.8 !important;
                color: #09ad7e !important;
                transition: all .2s !important;
            }
            /* checked mark aspect changes */
            .c9 input[type="checkbox"]:not(:checked) +  #t9:after {
                opacity: 0 !important;
                transform: scale(0) !important;
            }
            .c9 input[type="checkbox"]:checked +  #t9:after {
                opacity: 1 !important;
                transform: scale(1) !important;
            }
            /* disabled checkbox */
            .c9 input[type="checkbox"]:disabled:not(:checked) +  #t9:before,
            .c9 input[type="checkbox"]:disabled:checked +  #t9:before {
                box-shadow: none !important;
                border-color: #bbb !important;
                background-color: #ddd !important;
            }
            .c9 input[type="checkbox"]:disabled:checked +  #t9:after {
                color: #999 !important;
            }
            .c9 input[type="checkbox"]:disabled +  #t9 {
                color: #aaa !important;
            }
            /* accessibility */
            .c9 input[type="checkbox"]:checked:focus + #t9:before,
            .c9 input[type="checkbox"]:not(:checked):focus + #t9:before {
                border: 2px dotted blue !important;
            }

            /* hover style just for information */
            .c9 #t9:hover:before {
                border: 2px solid #4778d9 !important;
            }
            #t9{
                background-color: white !important;
            }



            .background_active_color{
				background: #16a085;
			}
			.background_active{
				/* font: 115% sans-serif !important; */
    			/*font-weight: 700 !important;*/
				/*font-family: "Constantia", serif !important;*/
				font-family: calibri !important;
				font-weight: bold !important;
				font-size:20px !important;
			}
			.background_active_1{
				/* font: 115% sans-serif !important; */
    			/*font-weight: 700 !important;*/
				/*font-family: "Constantia", serif !important;*/
				font-family: helvética !important;
				font-size:20px !important;
			}
			.inicio--no-shadow{
				box-shadow: none;
			}
			.btn-delete{
				background: red;
				color: white;
				border-radius: 5px;
				border: none;
				font-size: 13px;
			}
			.btn-delete:hover, .btn-delete:focus{
				background: white;
				color: red;
			}
			.modal-mask {
			position: fixed;
			z-index: 9998;
			top: 0;
			left: 0;
			width: 100%;
			height: 100%;
			background-color: rgba(0, 0, 0, .5);
			display: table;
			transition: opacity .3s ease;
			}

			.modal-wrapper {
			display: table-cell;
			vertical-align: middle;
			}
			.modal-body{
				max-height: 450px;
				overflow-y: scroll;
			}
			.modal-intermetio{
				margin: 0 15px;
				font-family: helvética !important;
				font-size: 26px !important;
				padding: 10px 0;
			}
			.modal-intermedio-agregar{
				text-align:right;
				padding: 4px;
				margin-top: 6px;
				margin-right: 20px
			}
			.modal-body_1{
				padding-top: 15px;
				height: 40px;
			}
			.loader-table{
				/* background-color: #dff9fb;
				opacity: .5; */
				display: flex;
				align-items: center;
				justify-content: center;
				height: 75%;
			}
			.spinner{
				border: 4px solid rgba(0, 0, 0, 0.2);
				border-left-color: #39C;
				border-radius: 50%;
				width: 50px;
				height: 50px;
				animation: spin .9s linear infinite;
			}
			@keyframes spin {
				to { transform: rotate(360deg); }
			}
			.modal-container1
			{
				width: 50%;
				margin: 0px auto;
				padding: 20px 30px;
				text-align: left;
				background:linear-gradient(#99bbcc, #B6CEDA);
				border-radius: 10px;
				box-shadow: 0 2px 8px rgba(0, 0, 0, .33);
				transition: all .3s ease;
			}

            [v-cloak]{
                display : none;
            }

            label{
                font-size:13px;
            }

            input{
                height: calc(1em + 0.6rem + 0.5px) !important;
                font-size: 14px !important;
                margin-top: 4px !important;
            }

        </style>
    </head>

    <body>
        <!-- <div class="subpantalla" style="height:640px; width:99.6%; overflow:hidden;"> -->
        <IFRAME src="alertas.php" name="alertas" id="alertas" style="display:none"></IFRAME>
        <span id="todastablas2"></span>

        <div>
            <div id="myapp" style="height:inherit;" v-cloak>
                <div id="cargando" v-if="loading" class="loading">
					<span>Cargando...</span>
				</div>
                <div>
                    <table>
                        <tr><script>barra_imagenes("ccpet");</script><?php cuadro_titulos();?></tr>
                        <tr><?php menu_desplegable("ccpet");?></tr>
                    </table>
                    <div class="bg-white group-btn p-1" id="newNavStyle">
                        <button type="button" class="btn btn-white btn-primary-hover d-flex justify-between align-items-center" @click="window.location.href='ccp-rpbasico.php'">
                            <span>Nuevo</span>
                            <svg  xmlns="http://www.w3.org/2000/svg"  viewBox="0 -960 960 960" ><path d="M440-440H200v-80h240v-240h80v240h240v80H520v240h-80v-240Z"/></svg>
                        </button>
                        <button type="button" class="btn btn-white btn-primary-hover d-flex justify-between align-items-center" v-on:Click="guardarRp">
                            <span>Guardar</span>
                            <svg xmlns="http://www.w3.org/2000/svg"  viewBox="0 -960 960 960" ><path d="M840-680v480q0 33-23.5 56.5T760-120H200q-33 0-56.5-23.5T120-200v-560q0-33 23.5-56.5T200-840h480l160 160Zm-80 34L646-760H200v560h560v-446ZM480-240q50 0 85-35t35-85q0-50-35-85t-85-35q-50 0-85 35t-35 85q0 50 35 85t85 35ZM240-560h360v-160H240v160Zm-40-86v446-560 114Z"/></svg>
                        </button>
                        <button type="button" class="btn btn-white btn-primary-hover d-flex justify-between align-items-center" @click="window.location.href='ccp-buscarrp.php'">
                            <span>Buscar</span>
                            <svg class="fill-black group-hover:fill-white w-5 h-5" xmlns="http://www.w3.org/2000/svg"  viewBox="0 -960 960 960" ><path d="M784-120 532-372q-30 24-69 38t-83 14q-109 0-184.5-75.5T120-580q0-109 75.5-184.5T380-840q109 0 184.5 75.5T640-580q0 44-14 83t-38 69l252 252-56 56ZM380-400q75 0 127.5-52.5T560-580q0-75-52.5-127.5T380-760q-75 0-127.5 52.5T200-580q0 75 52.5 127.5T380-400Z"/></svg>
                        </button>
                        <button type="button" class="btn btn-white btn-primary-hover d-flex justify-between align-items-center" @click="mypop=window.open('ccp-principal.php','',''); mypop.focus();">
                            <span>Nueva ventana</span>
                            <svg class="fill-black group-hover:fill-white w-5 h-5" xmlns="http://www.w3.org/2000/svg"  viewBox="0 -960 960 960" ><path d="M320-120v-80h80v-80H160q-33 0-56.5-23.5T80-360v-400q0-33 23.5-56.5T160-840h640q33 0 56.5 23.5T880-760v400q0 33-23.5 56.5T800-280H560v80h80v80H320ZM160-360h640v-400H160v400Zm0 0v-400 400Z"/></svg>
                        </button>
                        <button type="button" class="btn btn-white btn-primary-hover d-flex justify-between align-items-center" @click="mypop=window.open('ccp-cdpbasico.php','','');mypop.focus();">
                            <span class="group-hover:text-white">Duplicar pantalla</span>
                            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 -960 960 960"><path d="M640-160v-360H160v360h480Zm80-200v-80h80v-360H320v200h-80v-200q0-33 23.5-56.5T320-880h480q33 0 56.5 23.5T880-800v360q0 33-23.5 56.5T800-360h-80ZM160-80q-33 0-56.5-23.5T80-160v-360q0-33 23.5-56.5T160-600h480q33 0 56.5 23.5T720-520v360q0 33-23.5 56.5T640-80H160Zm400-603ZM400-340Z"></path></svg>
                        </button>

                    </div>
                </div>

                <div class="row">
					<div class="col-12">
						<h6 style="padding-left:30px; padding-top:5px; padding-bottom:5px; background-color: #559CFC; color:white">.: Tipo de Movimiento Registro Presupuestal</h6>
					</div>
				</div>



                <div class="row" style="margin: -6px 0px 2px 0px">
                    <div class="col-12">
						<div class="row" style="border-radius:2px; background-color: #F6F6F6; ">
							<div class="col-md-2" style="display: grid; align-content:center;">
								<label for="" style="margin-bottom: 0; ">Tipo de movimiento:</label>
							</div>
							<div class="col-md-2" style="padding: 4px">
                                <select v-model="tipoMovimiento" class="form-control select" v-on:change="seleccionarFormulario()" style = "font-size: 8px !important; margin-top:1px; height: calc(1.5em + 0.8rem + 0.5px);">
                                    <option v-for="tipoDeMovimiento in tiposDeMovimiento" v-bind:value = "tipoDeMovimiento[0]">
                                        {{ tipoDeMovimiento[0] }} - {{ tipoDeMovimiento[1] }}
                                    </option>
                                </select>
                            </div>

                            <!-- <div class="col-md-2" style="display: grid; align-content:center;">
								<label for="" style="margin-bottom: 0; ">Origen:</label>
							</div>
							<div class="col-md-2" style="padding: 4px">
                                <select v-model="origen" class="form-control select" v-on:change="selectOrigen()" style = "font-size: 10px !important; margin-top:1px; height: calc(1.5em + 0.8rem + 0.5px);">
                                    <option v-bind:value = "-1">Seleccione una opción</option>
                                    <option v-for="origen in origenes" v-bind:value = "origen.value">
                                        {{ origen.text }}
                                    </option>
                                </select>
                            </div> -->
						</div>
					</div>
                </div>

                <div v-show="mostrarCrearRp">

                    <div class="row">
                        <div class="col-12">
                            <h6 style=" background-color: #559CFC;"></h6>
                        </div>
                    </div>

                    <div class="row" style="margin: -6px 0px 2px 0px">
                        <div class="col-12">
                            <div class="row" style="border-radius:2px; background-color: #F6F6F6; ">

                                <!-- <div class="col-md-1" style="display: grid; align-content:center;">
                                    <label for="" style="margin-bottom: 0;">Consecutivo:</label>
                                </div>
                                <div class="col-md-1" style="padding: 1px">
                                    <input type="text" id="consecutivo" name="consecutivo" class="form-control" style =  text-align:center;" v-model="consecutivo" readonly>
                                </div> -->

                                <div v-show="show_cdp_basico" class="col-md-1" style="display: grid; align-content:center;">
                                    <label for="" style="margin-bottom: 0; font-size:14px;">N&uacute;mero cdp:</label>
                                </div>

                                <div v-show="show_cdp_basico" class="col-md-1" style="padding: 2px">
                                    <input type="text" id="cdp" name="cdp" class="form-control colordobleclik" style =  text-align:center;"  v-on:dblclick="ventanaCdp" v-on:change = "buscarCdp"  v-model="cdp" autocomplete="off" >
                                </div>

                                <div v-show="show_registro_contrato" class="col-md-1" style="display: grid; align-content:center;">
                                    <label for="" style="margin-bottom: 0;">Reg. contrato:</label>
                                </div>
                                <div v-show="show_registro_contrato" class="col-md-1" style="padding: 1px">
                                    <input type="text" id="reg_contrato" name="reg_contrato" class="form-control colordobleclik" style =  text-align:center;" v-on:dblclick="ventanaRegistroContrato" v-model="reg_contrato" readonly>
                                </div>

                                <div class="col-md-1" style="display: grid; align-content:center;">
                                    <label for="" style="margin-bottom: 0;">Fecha:</label>
                                </div>
                                <div class="col-md-2" style="padding: 1px">
                                    <div class="row">
                                        <div class="col-md-8" style="padding: 1px; margin-left: 15px;">
                                            <input type="text" id="fecha" name="fecha" class="form-control input" title="DD/MM/YYYY" placeholder="DD/MM/YYYY"  onchange="cambiaFecha();">
                                        </div>
                                        <div class="col-md-2" style="padding: 2px; padding-top: 8px">
                                            <a href="#" onClick="displayCalendarFor('fecha');" tabindex="3" title="Calendario"><img src="imagenes/calendario04.png" align="absmiddle" style="width:20px;"></a>
                                        </div>
                                    </div>

                                </div>

                                <div class="col-md-1" style="display: grid; align-content:center;">
                                    <label for="" style="margin-bottom: 0;">Vigencia:</label>
                                </div>
                                <div class="col-md-1" style="padding: 1px">
                                    <input type="text" id="vigencia" name="vigencia" class="form-control" style =  text-align:center;" readonly>
                                </div>

                                <div class="col-md-1" style="display: grid; align-content:center;">
                                    <label for="" style="margin-bottom: 0;">Contrato:</label>
                                </div>
                                <div class="col-md-1" style="padding: 1px">
                                    <input type="text" id="contrato" name="contrato" class="form-control" style =   text-align:center;" v-model="contrato">
                                </div>

                                <div class="col-md-1" style="display: grid; align-content:center;">
                                    <label for="" style="margin-bottom: 0;">N&oacute;mina:</label>
                                </div>
                                <div class="col-md-1 c9" style="padding: 1px">
                                    <input type="checkbox"  id="checkbox" v-model="checked" disabled>
                                    <label for="checkbox" id="t9" >
                                    </label>
                                </div>

                            </div>

                            <div class="row" style="border-radius:2px; background-color: #F6F6F6; ">
                                <div v-show="!show_registro_contrato">
                                    <div class="col-md-1" style="display: grid; align-content:center;">
                                        <label for="" style="margin-bottom: 0;">Fecha Cdp:</label>
                                    </div>
                                    <div class="col-md-1" style="padding: 1px">
                                        <input type="text" id="fechaCdp" name="fechaCdp" class="form-control" style = " text-align:center;" v-model="fechaCdp" readonly>
                                    </div>
                                </div>

                                
                                    <div v-show="show_registro_contrato" class="col-md-1" style="display: grid; align-content:center;">
                                        <label for="" style="margin-bottom: 0; font-size:14px;">N&uacute;mero cdp:</label>
                                    </div>

                                    <div v-show="show_registro_contrato" class="col-md-1" style="padding: 2px">
                                        <input type="text" id="cdp" name="cdp" class="form-control" style =  text-align:center;"  v-model="cdp" autocomplete="off" readonly>
                                    </div>
                                
                                    <div v-show="!show_registro_contrato" class="col-md-1" style="display: grid; align-content:center;">
                                        <label for="" style="margin-bottom: 0;">Solicita:</label>
                                    </div>
                                    <div v-show="!show_registro_contrato" class="col-md-6" style="padding: 1px">
                                        <input type="text" id="solicita" name="solicita" class="form-control" v-model = "solicita">
                                    </div>
                                
                                

                            </div>

                            <div class="row" style="border-radius:2px; background-color: #F6F6F6; ">

                                <div class="col-md-1" style="display: grid; align-content:center;">
                                    <label for="" style="margin-bottom: 0;">Objeto:</label>
                                </div>
                                <div class="col-md-10" style="padding: 1px">
                                    <textarea class="resize-vertical" id="objeto" name="objeto" rows="2" v-model="objeto" style="max-height:100px; width: 100%;"></textarea>
                                    <!-- <input type="text" id="objeto" name="objeto" class="form-control" v-model="objeto"> -->
                                </div>



                            </div>

                            <div class="row" style="border-radius:2px; background-color: #F6F6F6; ">

                                <div class="col-md-1" style="display: grid; align-content:center;">
                                    <label for="" style="margin-bottom: 0;">Tercero:</label>
                                </div>

                                <div class="col-md-1" style="padding: 2px">
                                    <input type="text" id="tercero" name="tercero" v-model="tercero" style="font-size: 11px; padding: 0px 0px 0px 4px;" class="form-control colordobleclik" v-on:change = "buscarTercero" onDblClick="despliegamodal2('visible','2')" autocomplete="off">
                                </div>
                                <div class="col-md-3" style="padding: 2px">
                                    <input type="text" id="ntercero" name="ntercero" class="form-control" readonly>
                                </div>

                                <div class="col-md-1" style="display: grid; align-content:center;">
                                    <label for="" style="margin-bottom: 0;">Banco:</label>
                                </div>

                                <div class="col-md-1" style="padding: 2px">
                                    <input type="text" v-model="codigoCuenta" style="font-size: 11px; padding: 0px 0px 0px 4px;" class="form-control colordobleclik" v-on:dblclick="cuentaBancariaTercero" autocomplete="off" readonly>
                                </div>
                                <div class="col-md-4" style="padding: 2px">
                                    <input type="text" v-model="nameCuenta" class="form-control" readonly>
                                    <input type='hidden' name='banco' id='banco' />
                                    <input type='hidden' name='tcta' id='tcta' />
                                   	<input type='hidden' name='ter' id='ter' />
                                </div>

                            </div>

                        </div>
                    </div>

                    <div v-show="showBancos">
						<transition name="modal">
							<div class="modal-mask">
								<div class="modal-wrapper">
									<div class="modal-container1">
										<table class="inicio ancho">
											<tr>
												<td class="titulos" colspan="2" >Listado Bancos</td>
												<td class="cerrar" style="width:7%" @click="showBancos = false">Cerrar</td>
											</tr>
                                            <td class="tamano01" style="width:3cm">Banco:</td>
												<td>
													<input type="text" class="form-control" placeholder="Buscar por codigo o nombre de clase" style="width:100%" />
												</td>
										</table>
										<table class='tablamv'>
											<thead>
												<tr style="text-align:center;">
													<th class="titulosnew02" >Resultados Busqueda</th>
												</tr>
												<tr style="text-align:center;">
													<th class="titulosnew00" style="width:20%;">Tipo cuenta</th>
													<th class="titulosnew00">Banco</th>
                                                    <th class="titulosnew00" style="width:20%;">Numero cuenta</th>
												</tr>
											</thead>
											<tbody>
												<tr v-for="(banco,index) in listaBancos" v-on:click="seleccionaBanco(banco)" v-bind:class="index % 2 ? 'saludo1a' : 'saludo2'" style='text-rendering: optimizeLegibility; cursor: pointer !important;'>
													<td style="font: 120% sans-serif; padding-left:10px; width:20%; text-align:center;">{{ banco[0] }}</td>
													<td style="font: 120% sans-serif; padding-left:10px; text-align:center;">{{ banco[2] }}</td>
                                                    <td style="font: 120% sans-serif; padding-left:10px; width:20%; text-align:center;">{{ banco[3] }}</td>
												</tr>
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</transition>
					</div>

                    <div v-show="showModal_reg_contrato" class="modal">
                        <div class="modal-dialog modal-xl" >
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title">Contratos Registrados</h5>
                                    <button type="button" @click="showModal_reg_contrato = false" class="btn btn-close"><div></div><div></div></button>
                                </div>
                                <div class="modal-body">
                                    <!-- <p class="m-0 ms-2 fw-bold">Señor usuario, use un filtro a la vez<span class="text-danger fw-bolder">*</span></p> -->
                                    <div class="d-flex flex-row mt-2">
                                        <div class="mr-4">
                                            <label class="form-label" for="">Fecha inicial:</label>
                                            <input class="form-control pb-4" type="date" v-model="fechaIni" @change="filterByDate()">
                                        </div>

                                        <div class="mr-4">
                                            <label class="form-label" for="">Fecha final:</label>
                                            <input class="form-control pb-4" type="date" v-model="fechaFin" @change="filterByDate()">
                                        </div>

                                        <div class="w-100">
                                            <label class="form-label" for="">Buscar:</label>
                                            <input class="form-control w-100" type="search" v-model="txtSearch" @keyup="filter('modalCdpTxt')" placeholder="Busca contrato, cdp, tercero o descripción">
                                        </div>
                                    </div>

                                    <div class="overflow-auto max-vh-50 overflow-x-hidden " >
                                        <table class="table table-hover fw-normal">
                                            <thead>
                                                <tr>
                                                    <th>Contrato</th>
                                                    <th>Cdp</th>
                                                    <th>Tercero</th>
                                                    <th>Fecha Registro</th>
                                                    <th>Objeto</th>
                                                    <th>Valor</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr v-for="reg_contrato in reg_contratos" v-on:click="seleccionarRegContrato(reg_contrato)">
                                                    <td>{{ reg_contrato.contrato }}</td>
                                                    <td>{{ reg_contrato.cdp }}</td>
                                                    <td>{{ reg_contrato.tercero }} - {{ reg_contrato.nombreTercero }}</td>
                                                    <td>{{ reg_contrato.fecha }}</td>
                                                    <td>{{ reg_contrato.objeto }}</td>
                                                    <td>{{ formatonumero(reg_contrato.valor) }}</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div v-show="showModal_cpc" class="modal">
                        <div class="modal-dialog modal-lg" >
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title">Seleccionar CPC</h5>
                                    <button type="button" @click="verificarSaldos" class="btn btn-close"><div></div><div></div></button>
                                </div>
                                <div class="d-flex flex-column">
                                    <div class="form-control m-0 mb-3">
                                        <input type="search" placeholder="Buscar" v-on:keyup="searchGeneralCpc" v-model="searchGeneral.keywordGeneral">
                                    </div>
                                </div>
                                <div class="modal-body">
                                    <p>{{ cuenta_ccpet_cpc }}, Valor del rubro: {{ formatonumero(valorRubroSel) }} </p>
                                    <div class="overflow-auto max-vh-50 overflow-x-hidden " >
                                        <table class="table table-hover fw-normal">
                                            <thead>
                                                <tr>
                                                    <th>Código CPC</th>
                                                    <th>Nombre CPC</th>
                                                    <th>Acciones</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr v-for="cpc in catalogo_cpc" v-on:click="seleccionarCpc(cpc[0], cpc[1])">
                                                    <td>{{ cpc[0] }}</td>
                                                    <td>{{ cpc[1] }}</td>
                                                    <td>{{  }}</td>
                                                </tr>
                                            </tbody>
                                        </table>

                                        

                                    </div>
                                    <div class="modal-footer">
                                        <table class="table table-hover fw-normal">
                                            <thead>
                                                <tr>
                                                    <th>Código CPC</th>
                                                    <th>Nombre CPC</th>
                                                    <th>Agrega Valor</th>
                                                    <th>Acciones</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr v-for="(cpc_seleccionado, index) in codigo_cpc_seleccionados">
                                                    <td>{{ cpc_seleccionado[0] }}</td>
                                                    <td>{{ cpc_seleccionado[1] }}</td>
                                                    <td>
                                                        <input type="number" min="00" v-on:change="calcularTotal(cpc_seleccionado[0], valorGastoCpc[cpc_seleccionado[0]], cpc_seleccionado[1])" v-model="valorGastoCpc[cpc_seleccionado[0]]" style="text-align:center;">
                                                    </td>
                                                    <td>
                                                        <button type="button" class="btn btn-danger" @click="eliminarCpc(cpc_seleccionado[0], valorGastoCpc[cpc_seleccionado[0]])">Eliminar</button>
                                                    </td>
                                                </tr>
                                            </tbody>
                                            <tfoot>
                                                <tr>
                                                    <td colspan="2" style="text-align:right;">Total:</td>
                                                    <td style="text-align:center;">{{ formatonumero(totalCpc) }}</td>
                                                </tr>
                                            </tfoot>
                                        </table>

                                        <button type="button" class="btn btn-secondary" @click="verificarSaldos">Cerrar</button>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div v-show="showModal_cdp">
                        <transition name="modal">
                            <div class="modal-mask">
                                <div class="modal-wrapper">
                                    <div class="modal-dialog modal-lg" style = "max-width: 1200px !important;" role="document">
                                        <div class="modal-content"  style = "width: 100% !important;" scrollable>
                                            <div class="modal-header">
                                                <h5 class="modal-title">Disponibilidades</h5>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true" @click="showModal_cdp = false">&times;</span>
                                                </button>
                                            </div>
                                            <div class="modal-body">
                                                <div style="margin: 2px 0 0 0;">
                                                    <div class="row" style="margin: 4px; border-radius:4px; background-color: #F6F6F6; ">
                                                        <div class="col-md-3" style="padding: 12px 0px 0px 30px; font: 140% sans-serif;">
                                                            <label for="">Consecutivo u objeto:</label>
                                                        </div>

                                                        <div class="col-md-6 col-md-offset-6" style="padding: 4px">
                                                            <input type="text" class="form-control" placeholder="Buscar por nombre u objeto de cdp" style="font: sans-serif; " v-on:keyup="searchMonitorCdp" v-model="searchCdp.keywordCdp">
                                                        </div>
                                                    </div>
                                                    <table>
                                                        <thead>
                                                            <tr>
                                                                <td width="20%" class='titulos' style="font: 120% sans-serif; padding-left:10px; border-radius: 5px 0 0 0; width:10%;">CDP</td>
                                                                <td class='titulos' style="font: 120% sans-serif; ">Nombre</td>
                                                                <td class='titulos' style="font: 120% sans-serif; width:20%;">Saldo</td>

                                                            </tr>
                                                        </thead>
                                                    </table>
                                                </div>
                                                <div style="margin: 2px 0 0 0; border-radius: 0 0 0 6px; height: 200px; overflow: scroll; overflow-x: hidden; background: white; ">
                                                    <table class='inicio inicio--no-shadow'>
                                                        <tbody>
                                                            <?php
                                                                $co ='zebra1';
                                                                $co2='zebra2';
                                                            ?>
                                                            <tr v-for="cdp in cdps" v-on:click="seleccionarCdp(cdp)" class='<?php echo $co; ?>' style='text-rendering: optimizeLegibility; cursor: pointer !important; style=\"cursor: hand\"'>
                                                                <td width="20%" style="font: 120% sans-serif; padding-left:10px; width:10%;">{{ cdp[0] }}</td>
                                                                <td  style="font: 120% sans-serif; padding-left:10px">{{ cdp[2] }}</td>
                                                                <td  style="font: 120% sans-serif; padding-left:10px; width:20%;">{{ cdp[3] }}</td>
                                                                <?php
                                                                $aux=$co;
                                                                $co=$co2;
                                                                $co2=$aux;
                                                                ?>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-secondary" @click="showModal_cdp = false">Cerrar</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </transition>
                    </div>


                    <div class="row" style="margin: 10px -5px 0px; height:350px; width:99.6%; overflow-x:hidden;" >
                        <div class="col-12">
                            <table class="table table-bordered table-hover">
                                <thead>
                                    <tr>
                                        <!-- <td class='titulos' style="font: 110% sans-serif; padding-left:2px; border-radius: 5px 0 0 0;">Tipo de gasto</td> -->
                                        <td class='titulos' style="font: 110% sans-serif; padding-left:2px; border-radius: 5px 0 0 0;">Dependencia</td>
                                        <td class='titulos' style="font: 110% sans-serif;">CSF</td>
                                        <td class='titulos' style="font: 110% sans-serif;">Vig. del gasto</td>
                                        <td class='titulos' style="font: 110% sans-serif;">Cuenta CCPET</td>
                                        <td class='titulos' style="font: 110% sans-serif;">BPIM</td>
                                        <td class='titulos' style="font: 110% sans-serif;">Indicador Producto</td>
                                        <td class='titulos' style="font: 110% sans-serif;">Fuentes</td>
                                        <td class='titulos' style="font: 110% sans-serif;" align="center">CPC</td>
                                        <!-- <td class='titulos' style="font: 110% sans-serif;">Divipola</td>
                                        <td class='titulos' style="font: 110% sans-serif;">Terceros CHIP</td> -->
                                        <td class='titulos' style="font: 110% sans-serif; padding-right:5px;">Valor</td>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                        $co ='zebra1';
                                        $co2='zebra2';
                                    ?>
                                    <tr v-for="(detalle, index) in detallesCdp"  class='<?php echo $co; ?>' style='text-rendering: optimizeLegibility; height: 5px; cursor: pointer !important; style=\"cursor: hand\";'>
                                        <!-- <td style="font: 110% sans-serif; padding-left:10px">{{ detalle[18] }}</td> -->
                                        <td style="font: 110% sans-serif; padding-left:10px">{{ detalle[17] }} {{ buscarDependencia(detalle[17]) }}</td>
                                        <td style="font: 110% sans-serif; padding-left:10px">{{ detalle[12] }}</td>
                                        <td style="font: 110% sans-serif; padding-left:10px">{{ detalle[14] }} {{ buscarVigencia(detalle[14]) }}</td>
                                        <td style="font: 110% sans-serif; padding-left:10px">{{ detalle[3] }} {{ buscarCuentaCcp(detalle[3]) }} </td>
                                        <td style="font: 110% sans-serif; padding-left:10px">{{ detalle[16] }} {{ buscarBpim(detalle[16]) }} </td>
                                        <td style="font: 110% sans-serif; padding-left:10px">{{ detalle[11] }}</td>
                                        <td style="font: 110% sans-serif; padding-left:10px">{{ detalle[5] }} {{ buscarFuente(detalle[5]) }}</td>
                                        <td style="font: 110% sans-serif; padding-left:10px;  background-color:#80C4E9; text-align: left; width: 15%;" v-on:dblclick="verificarCpc(index, detalle[3])">{{ detalle[4] }} {{ buscarCpcAgregados(detalle[3])  }} </td>
                                        <!-- <td style="font: 110% sans-serif; padding-left:10px">{{ detalle[19] }}</td>
                                        <td style="font: 110% sans-serif; padding-left:10px">{{ detalle[20] }}</td> -->
                                        <td style="font: 110% sans-serif; padding-left:10px; width: 10%; background-color:#80C4E9; text-align: right;" v-on:dblclick="cambiarValor(index)" >
                                            {{ formatonumero(valorReg[index]) }}
                                        </td>

                                        <?php
                                        $aux=$co;
                                        $co=$co2;
                                        $co2=$aux;
                                        ?>
                                    </tr>
                                </tbody>
                                <tfoot>
                                    <tr class='saludo1'>
                                        <td colspan = '8' style='font-size:18px; text-align: center; color:gray !important; ' class='saludo1'>TOTAL:</td>
                                        <td align="right" style='font-size:14px;'> {{ formatonumero(totalRp()) }}</td>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>

                </div>

                <div v-show = "mostrarReversionrp">
                    <article>
                        <table class="inicio ancho">
                            <tr>
                                <td class="titulos" colspan="7" >Reversar documento RP</td>
                                <td class="cerrar" style="width:7%" onClick="location.href='ccp-principal.php'">Cerrar</td>
                            </tr>

                            <tr>
                                <td class = "textoNewR"  style = "width: 8%;">
                                    <label class="labelR">
                                        Fecha Rev:
                                    </label>
                                </td>
                                <td  style = "width: 10%;">
                                    <input type="text" style = "width: 80%;" name="fechaRev" value="<?php echo $_POST['fechaRev']?>" onKeyUp="return tabular(event,this)" id="fc_1198971545" title="DD/MM/YYYY" onDblClick="displayCalendarFor('fc_1198971545');" class="colordobleclik" autocomplete="off" onChange="" readonly>
                                </td>

                                <td class="textoNewR" style = "width: 8%;">
                                    <label class="labelR">N° Rp:</label>
                                </td>
                                <td style = "width: 10%;">
                                    <input type="text"  style = "width: 80%;" class="colordobleclik" v-model = "numRpRev" @dblclick="ventanaRpRev" @change="validarRpRev">
                                </td>

                                <td class = "textoNewR"  style = "width: 8%;">
                                    <label class="labelR">
                                        Descripci&oacute;n:
                                    </label>
                                </td>
                                <td>
                                    <input type="text" style = "width: 100%;" v-model = "descripcionRev">
                                </td>
                            </tr>

                            <tr>
                                <td class="textoNewR">
                                    <label class="labelR">
                                        Fecha CDP:
                                    </label>
                                </td>
                                <td>
                                    <input type="text" v-model = "fechaRp" style = "width: 80%" readonly>
                                </td>
                                <td class="textoNewR">
                                    <label class="labelR">
                                        Tercero:
                                    </label>
                                </td>
                                <td>
                                    <input type="text" v-model = "terceroRp" style = "width: 100%;" readonly>
                                </td>
                                <td colspan="2">
                                    <input type="text" v-model = "nombreTerceroRev" style = "width: 100%;" readonly>
                                </td>
                            </tr>

                            <tr>
                                <td class="textoNewR">
                                    <label class="labelR">
                                        Objeto:
                                    </label>
                                </td>
                                <td colspan = "5">
                                    <input type="text" v-model = "objetoRp" style = "width: 100%;" readonly>
                                </td>
                            </tr>
                        </table>

                        <div class='subpantalla' style='height:56vh; margin-top:0px; overflow:hidden'>
                            <table class='tablamvR' id="tableId">
                                <thead>
                                    <tr style="text-align:Center;">
                                        <th class="titulosnew00" style="width:7%;">Tipo de gasto</th>
                                        <th class="titulosnew00" style="width:8%;">Sec. Presupuestal</th>
                                        <th class="titulosnew00" style="width:7%;">Medio pago</th>
                                        <th class="titulosnew00" style="width:7%;">Vig. del gasto</th>
                                        <th class="titulosnew00" style="width:7%;">Proyecto</th>
                                        <th class="titulosnew00" style="width:7%;">Programático</th>
                                        <th class="titulosnew00">Cuenta</th>
                                        <th class="titulosnew00" style="width:15%;">CPC</th>
                                        <th class="titulosnew00" style="width:8%;">Divipola</th>
                                        <th class="titulosnew00" style="width:8%;">CHIP</th>
                                        <th class="titulosnew00" style="width:8%;">Saldo</th>
                                        <th style="width:1%;"></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr v-for="(row,index) in detallesRev" :key="index" v-bind:class="[index % 2 ? 'contenidonew00' : 'contenidonew01']">
                                        <td style="width:7%;">{{ row[18] }}</td>
                                        <td style="width:8%;">{{ row[17] }}</td>
                                        <td style="width:7%;">{{ row[12] }}</td>
                                        <td style="width:7%;">{{ row[14] }}</td>
                                        <td style="width:7%;">{{ row[16] }}</td>
                                        <td style="width:7%;">{{ row[11] }}</td>
                                        <td>{{ row[3] }}</td>
                                        <td style="width:15%;">{{ row[4] }}</td>
                                        <td style="width:8%;">{{ row[19] }}</td>
                                        <td style="width:8%;">{{ row[20] }}</td>
                                        <td style="width:8%; text-align: right;">{{ formatonumero(valorRev[index]) }}</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </article>

                    <div v-show="showModal_rps_rev">
                        <transition name="modal">
                            <div class="modal-mask">
                                <div class="modal-wrapper">
                                    <div class="modal-dialog modal-lg" style = "max-width: 1200px !important;" role="document">
                                        <div class="modal-content"  style = "width: 1200px !important;" scrollable>
                                            <div class="modal-header">
                                                <h5 class="modal-title">Rps con saldo</h5>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true" @click="showModal_rps_rev = false">&times;</span>
                                                </button>
                                            </div>
                                            <div class="modal-body">
                                                <div style="margin: 2px 0 0 0;">
                                                    <div class="row" style="margin: 4px; border-radius:4px; background-color: #F6F6F6; ">
                                                        <!-- <div class="col-md-2" style="padding: 12px 0px 0px 30px; font: 140% sans-serif;">
                                                            <label for="">Cdp:</label>
                                                        </div>

                                                        <div class="col-md-6 col-md-offset-6" style="padding: 4px">
                                                            <input type="text" class="form-control" placeholder="Buscar por objeto o numero de CDP" style="font: sans-serif; " v-on:keyup="searchMonitorCdpRev" v-model="searchCdpRev.keywordCdpRev">
                                                        </div> -->
                                                    </div>
                                                    <table>
                                                        <thead>
                                                            <tr>
                                                                <td width="10%" class='titulos' style="font: 120% sans-serif; padding-left:10px; border-radius: 5px 0 0 0;">RP</td>
                                                                <td class='titulos' style="font: 120% sans-serif; ">Objeto</td>
                                                                <td width="15%" class='titulos' style="font: 120% sans-serif; ">Fecha</td>
                                                                <td width="15%" class='titulos' style="font: 120% sans-serif; ">Saldo</td>
                                                            </tr>
                                                        </thead>
                                                    </table>
                                                </div>
                                                <div style="margin: 2px 0 0 0; border-radius: 0 0 0 6px; height: 200px; overflow: scroll; overflow-x: hidden; background: white; ">
                                                    <table class='inicio inicio--no-shadow'>
                                                        <tbody>
                                                            <?php
                                                                $co ='zebra1';
                                                                $co2='zebra2';
                                                            ?>
                                                            <tr v-for="rp in rps_rev" v-on:click="seleccionarRpRev(rp)" class='<?php echo $co; ?>' style='text-rendering: optimizeLegibility; cursor: pointer !important; style=\"cursor: hand\"'>
                                                                <td width="10%" style="font: 120% sans-serif; padding-left:10px">{{ rp[0] }}</td>
                                                                <td  style="font: 120% sans-serif; padding-left:10px">{{ rp[2] }}</td>
                                                                <td width="15%" style="font: 120% sans-serif; padding-left:10px">{{ rp[3] }}</td>
                                                                <td width="15%" style="font: 120% sans-serif; padding-left:10px">{{ formatonumero(rp[4]) }}</td>
                                                                <?php
                                                                $aux=$co;
                                                                $co=$co2;
                                                                $co2=$aux;
                                                                ?>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-secondary" @click="showModal_rps_rev = false">Cerrar</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </transition>
                    </div>
                </div>
            </div>
        <!-- </div> -->

        <script src="Librerias/vue/vue.min.js"></script>
		<script src="Librerias/vue/axios.min.js"></script>
		<script src="vue/presupuesto_ccp/ccp-rpbasico.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
        <script src="sweetalert2/dist/sweetalert2.min.js"></script>
        <link rel="stylesheet" href="sweetalert2/dist/sweetalert2.min.css">


        <div id="bgventanamodal2">
            <div id="ventanamodal2">
                <IFRAME  src="" name="buster" marginWidth=0 marginHeight=0 frameBorder=0 id="ventana2" frameSpacing=0 style="left:500px; width:900px; height:500px; top:200;">
                </IFRAME>
            </div>
        </div>

    </body>
</html>
