<?php
	header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=utf8");

	require "comun.inc";
	require "funciones.inc";

	$linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");

	session_start();
	date_default_timezone_set("America/Bogota");
	ini_set('max_execution_time', 3600);
?>
<!DOCTYPE >
<html lang="es">
	<head>
		<meta charset="utf-8"/>
		<meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1"/>
		<meta name="viewport" content="user-scalable=no">
		<title>:: IDEAL 10 - Informes</title>
		<link href="favicon.ico" rel="shortcut icon"/>
		<link href="css/css2.css" rel="stylesheet" type="text/css" />
		<link href="css/css3.css" rel="stylesheet" type="text/css" />
		<script type="text/javascript" src="css/programas.js"></script>
		<script type="text/javascript" src="css/calendario.js"></script>
		<script type="text/javascript" src="jquery-1.11.0.min.js"></script>
		<script>
			$(window).load(function () {
					$('#cargando').hide();
				});
			function buscacta(e){
				if (document.form2.cuenta.value!=""){
					document.form2.bc.value='1';
					document.form2.submit();
				}
			}
			function validar(){
				document.form2.submit();
			}
			function buscatercero(e){
				if (document.form2.tercero.value!=""){
					document.form2.bc.value='1';
						document.form2.submit();
				}
			}
			function agregardetalle(){
				if(document.form2.numero.value!="" &&  document.form2.valor.value>0 &&  document.form2.banco.value!=""  ){
					document.form2.agregadet.value=1;
					//document.form2.chacuerdo.value=2;
					document.form2.submit();
				}else {
					alert("Falta informacion para poder Agregar");
				}
			}
			function eliminar(idr){
				if (confirm("Esta Seguro de Eliminar El Egreso No "+idr)){
					document.form2.oculto.value=2;
					document.form2.var1.value=idr;
					document.form2.submit();
				}
			}
			function guardar(){
				var validacion01=document.getElementById('concepto').value
				if (document.form2.fecha.value!='' && validacion01.trim()!='' && document.getElementById('vigencias').value!=''){
					despliegamodalm('visible','4','Esta Seguro de Guardar','2');
				}else{
					despliegamodalm('visible','2','Faltan datos para completar el registro');
					document.form2.fecha.focus();
					document.form2.fecha.select();
				}
			}
			function pdf(){
				document.form2.action="pdfreporegresos.php";
				document.form2.target="_BLANK";
				document.form2.submit();
				document.form2.action="";
				document.form2.target="";
			}
			function despliegamodal2(_valor){
				document.getElementById("bgventanamodal2").style.visibility=_valor;
				if(_valor=="hidden"){
					document.getElementById('ventana2').src="";
				}else {
					document.getElementById('ventana2').src="tercerosgral-ventana01.php?objeto=tercero&nobjeto=ntercero&nfoco=cc";
				}
			}
			function despliegamodal3(_valor,pos){
				document.getElementById("bgventanamodal2").style.visibility=_valor;
				if(_valor=="hidden"){
					document.getElementById('ventana2').src="";
				}else {
					document.getElementById('ventana2').src="cont-codigosinternosexogena-ventana.php?pos="+pos;
				}
			}
			function despliegamodalm(_valor,_tip,mensa,pregunta){
				document.getElementById("bgventanamodalm").style.visibility=_valor;
				if(_valor=="hidden"){
					document.getElementById('ventanam').src="";
					switch(document.getElementById('valfocus').value){
						case "1":	//document.getElementById('valfocus').value='';
									document.getElementById('vigencias').focus();
									//document.getElementById('tercero').select();
									break;
						case "2":	//document.getElementById('banco').value='';
									document.getElementById('vigencias').focus();
									//document.getElementById('banco').select();
					}
				}else{
					switch(_tip){
						case "1":
							document.getElementById('ventanam').src="ventana-mensaje1.php?titulos="+mensa;break;
						case "2":
							document.getElementById('ventanam').src="ventana-mensaje3.php?titulos="+mensa;break;
						case "3":
							document.getElementById('ventanam').src="ventana-mensaje2.php?titulos="+mensa;break;
						case "4":
							document.getElementById('ventanam').src="ventana-consulta1.php?titulos="+mensa+"&idresp="+pregunta;break;
					}
				}
			}
			function funcionmensaje(){
				//var numdocar=document.getElementById('idcomp').value;
				//document.location.href = "teso-exogena1001.php";
				var _cons=document.getElementById('idexo').value;
				document.location.href = "teso-verexogena2276.php?idexo="+_cons;
			}
			function respuestaconsulta(pregunta){
				switch(pregunta){
					case "1":	document.form2.oculto.value='2';
								document.form2.submit();
								break;
					case "2":	document.getElementById('btguardar').value="";
								document.getElementById('btexcell').value="1";
								document.form2.oculto.value='2';
								document.form2.submit();
								break;
				}
			}
			function excell(){
				if (document.getElementById('vigencias').value!=''){
					document.form2.action="teso-exogena1001excel.php";
					document.form2.target="_BLANK";
					document.form2.submit();
					document.form2.action="";
					document.form2.target="";
				}
			}
			function actgenerar(){
				var validacion01=document.getElementById('concepto').value
				if (document.form2.fecha.value!='' && validacion01.trim()!='' && document.getElementById('vigencias').value!=''){
					document.getElementById('btgenerar').value="hidden";
					document.getElementById('btguardar').value="1";
					document.form2.submit();
				}else{
					despliegamodalm('visible','2','Faltan datos para generar el reporte');
					document.form2.fecha.focus();
					document.form2.fecha.select();
				}
			}
		</script>
		<?php
			if ($_POST['btexcell']==""){
				$mtrexcell="<a class='mgbt1'><img src='imagenes/excelb.png' ></a>";
			}else {
				$mtrexcell="<a class='mgbt' onClick='excell();'><img src='imagenes/excel.png' title='Exogena 2276'></a>";
			}
			if($_POST['btguardar']=="1"){
				$mtrguardar="<a onClick='guardar()' class='mgbt'><img src='imagenes/guarda.png' title='Guardar' style='width:24px;height:24px;'/></a>";
			}else {
				$mtrguardar="<a class='mgbt1'><img src='imagenes/guardad.png' style='width:24px;height:24px;'/></a>";
			}
		?>
	</head>
	<body>
		<div id="cargando" style=" position:absolute;left: 46%; bottom: 45%">
			<img src="imagenes/loading.gif" style=" width: 80px; height: 80px"/>
		</div>
		<IFRAME src="alertas.php" name="alertas" id="alertas" style="display:none"></IFRAME>
		<span id="todastablas2"></span>
		<table>
			<tr><script>barra_imagenes("info");</script><?php cuadro_titulos();?></tr>
			<tr><?php menu_desplegable("info");?></tr>
			<tr>
				<td colspan="3" class="cinta">
					<a href='teso-exogena2276.php' class="mgbt"><img src="imagenes/add.png" title="Nuevo"/></a><?php echo $mtrguardar;?>
					<a href='teso-buscaexogena2276.php' class="mgbt"><img src="imagenes/busca.png" title="Buscar"/></a>
					<a onClick="mypop=window.open('plan-agenda.php','','');mypop.focus()" class="mgbt"><img src="imagenes/agenda1.png" title="Agenda"/></a>
					<a class="mgbt" onClick="mypop=window.open('info-principal.php','','');mypop.focus();"><img src="imagenes/nv.png" title="Nueva ventana"></a>
					<a onClick="mypop=window.open('<?php echo $url2; ?>','','');mypop.focus();" class="tooltip bottom mgbt"><img src="imagenes/duplicar_pantalla.png" title="Duplicar pesta&ntilde;a"></a>
					<a class="mgbt" onClick="pdf()"><img src="imagenes/print.png" title="Imprimir"></a>
					<a class="mgbt" onClick="location.href='<?php echo "archivos/".$_SESSION['usuario']."-reporteegresosexogena.csv"; ?>'" ><img src="imagenes/csv.png" title="Csv"/></a><?php echo $mtrexcell;?>
					<a href='teso-formatoexogena.php' class="mgbt"><img src="imagenes/iratras.png" title="Atr&aacute;s"></a>
				</td>
			</tr>
		</table>
		<div id="bgventanamodalm" class="bgventanamodalm">
			<div id="ventanamodalm" class="ventanamodalm">
				<IFRAME src="" name="ventanam" marginWidth=0 marginHeight=0 frameBorder=0 id="ventanam" frameSpacing=0 style=" width:700px; height:130px; top:200; overflow:hidden;">
				</IFRAME>
			</div>
		</div>
		<form name="form2" method="post" action="teso-exogena2276.php">
			<?php
				if ($_POST['oculto']==""){
					$_POST['btgenerar']="visible";
					$_POST['btexcell']="";
					$_POST['btguardar']="";
				}
				$sqlr = "SELECT MAX(id_exo) FROM exogena_cab_2276";
				$resp = mysqli_query($linkbd, $sqlr);
				$row = mysqli_fetch_row($resp);
				$_POST['idexo'] = $row[0]+1;
				$vigusu = vigencia_usuarios($_SESSION['cedulausu']);
				$vigencia = $vigusu;
				$vact = $vigusu;
				//echo "vig:".$_POST[vigencias];
				if($_POST['bc']=='1'){
					$nresul = buscatercero($_POST['tercero']);
					if($nresul!=''){
						$_POST['ntercero']=$nresul;
					} else {
						$_POST['ntercero']="";
					}
				}
			?>
			<table  class="inicio" align="center" >
				<tr >
					<td class="titulos" colspan="9">:. Formato Exogena 2276</td>
					<td class="cerrar" style="width:7%;"><a onClick="location.href='info-principal.php'">&nbsp;Cerrar</a></td>
				</tr>
				<tr>
					<td class="saludo1" style="width:2.5cm;">No Exogena:</td>
					<td style="width:8%"><input type="text" id="idexo" name="idexo" value="<?php echo $_POST['idexo']?>" style="width:100%" readonly></td>
					<td class="saludo1"  style="width:2cm;">Fecha:</td>
					<td style="width:10%"><input type="text" name="fecha"  value="<?php echo $_POST['fecha']?>" maxlength="10"  onKeyPress="javascript:return solonumeros(event)" onchange="" onKeyUp="return tabular(event,this)"  id="fc_1198971545" onKeyDown="mascara(this,'/',patron,true)" title="DD/MM/YYYY" style="width:80%;">&nbsp;<a onClick="displayCalendarFor('fc_1198971545');" title="Calendario"><img src="imagenes/calendario04.png" style="width:20px; cursor:pointer"></a></td>
					<td class="saludo1"  style="width:2cm;">Concepto:</td>
					<td style="width:30%"><input type="text" name="concepto" id="concepto" value="<?php echo $_POST['concepto']?>" style="width:100%"/></td>
					<td class="saludo1"  style="width:3.5cm;">Vigencia Exogena:</td>
					<td style="width:7%">
						<select name="vigencias" id="vigencias" style="width:100%;">
							<option value="">Sel..</option>
								<?php
									$sqlr="SELECT anio FROM admbloqueoanio WHERE bloqueado='N'";
									$res = mysqli_query($linkbd, $sqlr);
									while($row=mysqli_fetch_row($res)){
										if($row[0]==$_POST['vigencias']){
											echo "<option value='$row[0]' SELECTED>$row[0]</option>";
										}else {
											echo "<option value='$row[0]'>$row[0]</option>";
										}

									}
								?>
							</select>
					</td>
					<td> &nbsp;
						<input type="button" name="generar" value="Generar" onClick="actgenerar()"  style="visibility:<?php echo $_POST['btgenerar'];?>"/>
						<input type="hidden" name="oculto" id="oculto" value="1" >
						<input type="hidden" name="btgenerar" id="btgenerar" value="<?php echo $_POST['btgenerar'];?>"/>
						<input type="hidden" name="btexcell" id="btexcell" value="<?php echo $_POST['btexcell'];?>"/>
						<input type="hidden" name="btguardar" id="btguardar" value="<?php echo $_POST['btguardar'];?>"/>
					</td>
				</tr>
				<?php
					if($_POST['bc']=='1')
					{
						$nresul=buscatercero($_POST['tercero']);
						if($nresul!='')
						{
							$_POST['ntercero']=$nresul;
							echo"<script>document.form2.fecha.focus();document.form2.fecha.select();</script>";
						}
						else
						{
							$_POST['ntercero']="";
							echo"<script>alert('Tercero Incorrecta');document.form2.tercero.focus();</script>";
						}
					}
				?>
			</table>
			<div class="subpantallap"  style="height:68%;overflow-x:hidden;">
				<?php
					$oculto = $_POST['oculto'];
					if($_POST['oculto']){
						//ereg( "([0-9]{1,2})/([0-9]{1,2})/([0-9]{2,4})", $_POST[fecha],$fecha);
						preg_match("/([0-9]{2})\/([0-9]{2})\/([0-9]{4})/", $_POST['fecha'],$fecha);
						$fechaf = $fecha[3]."-".$fecha[2]."-".$fecha[1];
						//ereg( "([0-9]{1,2})/([0-9]{1,2})/([0-9]{2,4})", $_POST[fecha2],$fecha);
						preg_match("/([0-9]{2})\/([0-9]{2})\/([0-9]{4})/", $_POST['fecha2'],$fecha);
						$fechaf2 = $fecha[3]."-".$fecha[2]."-".$fecha[1];
						$crit1=" ";
						$crit2=" ";
						if ($_POST['tercero']!=""){
							$crit4 = "AND tesoegresosnomina.tercero LIKE '%$_POST[tercero]%' ";
						}
						if ($_POST['numero']!=""){
							$crit7 = "AND tesoegresosnomina.id_egreso LIKE '%$_POST[numero]%' ";
						}
						if ($_POST['nombre']!=""){
							$crit10 = "AND tesoegresosnomina.concepto LIKE '%$_POST[nombre]%'  ";
						}
						//sacar el consecutivo
						$sqlr2 = "SELECT * FROM tesoegresosnomina,tesoegresosnomina_det WHERE tesoegresosnomina.id_egreso=tesoegresosnomina_det.id_egreso AND tesoegresosnomina.id_egreso>-1 $crit4 $crit7 $crit10  AND YEAR(tesoegresosnomina.fecha) = '$_POST[vigencias]' AND tesoegresosnomina.estado='S' AND tesoegresosnomina_det.tipo NOT IN ('DS','SR', 'PR', 'F', 'SE', 'PE', 'FS') GROUP BY tesoegresosnomina.id_egreso,tesoegresosnomina_det.tipo ORDER BY tesoegresosnomina.tercero DESC";
						//echo "<div><div>sqlr:".$sqlr2."</div></div>"; //sistem@s2014
						$resp2 = mysqli_query($linkbd, $sqlr2);
						$ntr2 = mysqli_num_rows($resp2);
						$con=0;
						$namearch="archivos/".$_SESSION['usuario']."-reporteegresosexogena.csv";
						$namearch2="archivos/fmt1001_".$_POST['vigencias'].".csv";
						$Descriptor1 = fopen($namearch,"w+");
						$Descriptor2 = fopen($namearch2,"w+");
						fputs($Descriptor1,"CONCEPTO;TIPODOC;Doc Tercero;DV;FECHA;CHEQUE;VALOR;RETENCION EN LA FUENTE;RETE IVA;VALOR PAGO;CONCEPTO;ESTADO\r\n");
						//fputs($Descriptor2,"CONCEPTO;TIPODOC;Doc Tercero;DV;FECHA;CHEQUE;VALOR;VALOR PAGO;CONCEPTO;ESTADO\r\n");
						fputs($Descriptor2,"Concepto;Tipo de documento;Número identificación del informado;DV;Primer apellido del informado;Segundo apellido del informado;Primer nombre del informado;Otros nombres del informado;Razón social informado;Dirección;Código dpto;Código mcp;País de residencia o domicilio;Pago o abono en cuenta deducible;Pago o abono en cuenta no deducible;Iva mayor valor del costo o gasto deducible;Iva mayor valor del costo o gasto no deducible;Retención en la fuente practicada en renta;Retención en la fuente asumida en renta;Retención en la fuente practicada IVA régimen común;Retención en la fuente asumida  IVA régimen simplificado;Retención en la fuente practicada IVA no domiciliados;Retención en la fuente practicadas CREE;Retención en la fuente asumidas CREE\r\n");
						echo "
						<table class='inicio' align='center' >
							<tr><td colspan='12' class='titulos'>.: Resultados Busqueda:</td></tr>
							<tr><td colspan='12' class='saludo3'>Pagos Encontrados: ".($ntr+$ntr2+$ntr3+$ntr4)."</td></tr>
							<tr>
								<td  class='titulos2'>CONCEPTO</td>
								<td  class='titulos2'>EGRESO</td>
								<td  class='titulos2'>TIPO</td>
								<td  class='titulos2'>ORDEN</td>
								<td class='titulos2'>DOC TERCERO</td>
								<td class='titulos2'>TERCERO</td>
								<td class='titulos2'>FECHA</td>
								<td class='titulos2'>VALOR</td>
								<td class='titulos2'>Retencion en la Fuente</td>
								<td class='titulos2'>Rete IVA</td>
								<td class='titulos2'>Descripcion</td>
								<td class='titulos2' width='3%'><center>Estado</td>
							</tr>";
						//echo "nr:".$nr;
						$iter='zebra11';
						$iter2='zebra22';
						function calculavalordevengado123($codigo, $tipo){
							//global $linkbd;
							$linkbd = conectar_v7();
							$linkbd -> set_charset("utf8");
							$sql = "SELECT SUM(valordevengado) FROM tesoegresosnomina_det WHERE id_egreso='$codigo' AND tipo='$tipo'";
							$res=mysqli_query($linkbd, $sql);
							$row = mysqli_fetch_row($res);
							return $row[0];
						}
						while ($row2 = mysqli_fetch_row($resp2)){
							$valorExogena = '';
							$sqlrCod = "SELECT C.codigo FROM contcodigosinternos AS C, contcodigosinternos_det AS CT WHERE CT.codigo='$row2[20]' AND CT.idnum = C.idnum ";
							$respCod = mysqli_query($linkbd, $sqlrCod);
							$rowCod = mysqli_fetch_row($respCod);

							$valorExogena = $rowCod[0];

							if($row2[18]=='01' || $row2[18]=='N'){
								$ntr = buscatercero($row2[11]);
								echo "
								<tr class='$iter'>
								<td ><input type='text' name='conexogena[]' value='$valorExogena' size='4' onDblClick='despliegamodal3(\"visible\",$con)'></td>
								<td ><input type='hidden' name='egresos[]' value='$row2[0]' >$row2[0]</td>
								<td ><input type='hidden' name='tipo[]' value='$row2[20]'>$row2[20]</td>
								<td ><input type='hidden' name='ordenes[]' value='$row2[2]'>$row2[2]</td>
								<td ><input type='hidden' name='terceros[]' value='$row2[11]'>$row2[11]</td>
								<td ><input type='hidden' name='nterceros[]' value='$ntr'>$ntr</td>
								<td ><input type='hidden' name='fechas[]' value='$row2[3]'>$row2[3]</td>
								<td ><input type='hidden' name='valoresb[]' value='".calculavalordevengado123($row2[0], $row2[20])."'>".number_format(calculavalordevengado123($row2[0], $row2[20]),2)."</td>
								<td ><input type='hidden' name='valores[]' value='$row2[6]'>".number_format($row2[6],2)."</td>
								<td ><input type='hidden' name='valoresiv[]' value='$valretiva'>".number_format($valretiva,2)."</td>
								<td ><input type='hidden' name='conceptos[]' value='EGRESO NOMINA $row2[8]'>".strtoupper("EGRESO NOMINA ".$row2[8])."</td>
								<td ><input type='hidden' name='estados[]' value='EN'>".strtoupper($row2[13])."</td>
								</tr>";
								$con+=1;
								$aux=$iter;
								$iter=$iter2;
								$iter2=$aux;
								fputs($Descriptor1,$row2[0].";".$row2[2].";".$row2[11].";".$ntr.";".$row2[3].";".$row2[10].";".number_format(calculavalordevengado($row2[0]),2,".","").";".$row2[6].";".$valretiva.";".number_format($row2[7],2,".","").";".strtoupper("EGRESO NOMINA".$row2[8]).";".strtoupper($row2[13])."\r\n");
							} else {
								$ntr = buscatercero($row2[11]);
								echo "
								<tr class='$iter'>
								<td ><input type='text' name='conexogena[]' value='$valorExogena' size='4' onDblClick='despliegamodal3(\"visible\",$con)'></td>
								<td ><input type='hidden' name='egresos[]' value='$row2[0]' >$row2[0]</td>
								<td ><input type='hidden' name='tipo[]' value='$row2[20]'>$row2[20]</td>
								<td ><input type='hidden' name='ordenes[]' value='$row2[2]'>$row2[2]</td>
								<td ><input type='hidden' name='terceros[]' value='$row2[11]'>$row2[11]</td>
								<td ><input type='hidden' name='nterceros[]' value='$ntr'>$ntr</td>
								<td ><input type='hidden' name='fechas[]' value='$row2[3]'>$row2[3]</td>
								<td ><input type='hidden' name='valoresb[]' value='".calculavalordevengado123($row2[0], $row2[20])."'>".number_format(calculavalordevengado123($row2[0], $row2[20]),2)."</td>
								<td ><input type='hidden' name='valores[]' value='$row2[6]'>".number_format($row2[6],2)."</td>
								<td ><input type='hidden' name='valoresiv[]' value='$valretiva'>".number_format($valretiva,2)."</td>
								<td ><input type='hidden' name='conceptos[]' value='EGRESO NOMINA $row2[8]'>".strtoupper("EGRESO NOMINA ".$row2[8])."</td>
								<td ><input type='hidden' name='estados[]' value='EN'>".strtoupper($row2[13])."</td>
								</tr>";
								$con+=1;
								$aux=$iter;
								$iter=$iter2;
								$iter2=$aux;
								fputs($Descriptor1,$row2[0].";".$row2[2].";".$row2[11].";".$ntr.";".$row2[3].";".$row2[10].";".number_format($row2[5],2,".","").";".$row2[6].";".$valretiva.";".number_format($row2[7],2,".","").";".strtoupper("EGRESO NOMINA".$row2[8]).";".strtoupper($row2[13])."\r\n");
							}
						}
						fclose($Descriptor1);
						echo"</table>";
					}
					if($_POST['oculto']==2 || $_POST['oculto']==3){
						//ereg( "([0-9]{1,2})/([0-9]{1,2})/([0-9]{2,4})", $_POST['fecha'],$fecha);
						preg_match("/([0-9]{2})\/([0-9]{2})\/([0-9]{4})/", $_POST['fecha'],$fecha);
						$fechaf = $fecha[3]."-".$fecha[2]."-".$fecha[1];
						$idtabla = selconsecutivo('exogena_cab_2276','id_exo');
						$sqlr = "INSERT INTO exogena_cab_2276 (id_exo,fecha,vigencia,descripcion,estado) VALUES ('$idtabla','$fechaf','$_POST[vigencias]', '$_POST[concepto]','S')";
						if(mysqli_query($linkbd, $sqlr)){
							$id=$idtabla;
							echo"<script>despliegamodalm('visible','3','Se ha almacenado La Exogena con Exito');</script>";
							$conta = count($_POST['conexogena']);
							for($x=0;$x<$conta;$x++){
								$sqlr="INSERT INTO exogena_det_2276 (id_exo, concepto, id_egre, id_cxp, fecha, tercero, detalle, valor, retefte,reteiva, tipoegre, tipo) VALUES ($id,'".$_POST['conexogena'][$x]."','".$_POST['egresos'][$x]."','".$_POST['ordenes'][$x]."','".$_POST['fechas'][$x]."','".$_POST['terceros'][$x]."','".$_POST['conceptos'][$x]."','".$_POST['valoresb'][$x]."','".$_POST['valores'][$x]."','".$_POST['valoresiv'][$x]."','".$_POST['estados'][$x]."', '".$_POST['tipo'][$x]."')";
								mysqli_query($linkbd, $sqlr);
								//  echo "<br>".$sqlr;
							}
							if($_POST['oculto']==3){
								// $sqlr="select distinct * from exogena_det where id_exo=$id group by concepto,tercero order by concepto,tercero";
								$sqlr="SELECT DISTINCT concepto, tercero, sum(valor), sum(retefte), sum(reteiva) FROM exogena_det_2276 WHERE id_exo=$_POST[idexo] GROUP BY concepto,tercero ORDER BY concepto, tercero";
								$resp = mysqli_query($linkbd, $sqlr);
								while($row=mysqli_fetch_row($resp)){
									$sqlrt="SELECT * FROM terceros WHERE cedulanit=$row[1]";
									$rest = mysqli_query($linkbd, $sqlrt);
									$rowt = mysqli_fetch_row($rest);
									fputs($Descriptor2,$row[0].";".$rowt[11].";".$rowt[12].";".$rowt[13].";".$rowt[3].";".$rowt[4].";".$rowt[1].";".$rowt[2].";".$rowt[5].";".$rowt[6].";".$rowt[14].";".$rowt[15].";169;0;".$row[2].";0;0;".$row[3].";0;".$row[4].";0;0;0;0\r\n");
									//fputs($Descriptor2,$row[1].";".$rowt[11].";".$rowt[12].";".$rowt[13].";".$rowt[3].";".$rowt[4].";".$rowt[1].";".$rowt[2].";".$rowt[5]."; ".$rowt[6].";".$rowt[14]."; ".$rowt[15].";169;0;".$row[7].";0;0;".$row[8].";0;".$row[9].";0;0;0;0\r\n");
									//***Concepto	Tipo de documento	N�mero identificaci�n del informado	DV	Primer apellido del informado	Segundo apellido del informado	Primer nombre del informado	Otros nombres del informado	Raz�n social informado	Direcci�n	C�digo dpto.	C�digo mcp	Pa�s de residencia o domicilio	Pago o abono en cuenta deducible	 Pago o abono en cuenta no deducible 	 Iva mayor valor del costo o gasto deducible 	 Iva mayor valor del costo o gasto no deducible 	 Retenci�n en la fuente practicada en renta 	 Retenci�n en la fuente asumida en renta 	 Retenci�n en la fuente practicada IVA r�gimen com�n 	 Retenci�n en la fuente asumida  IVA r�gimen simplificado 	 Retenci�n en la fuente practicada IVA no domiciliados 	" Retenci�n en la fuente practicadas CREE "	" Retenci�n en la fuente asumidas CREE "
								}
							}
						}else {
							echo"<script>despliegamodalm('visible','2','Ya Se ha almacenado un documento con ese consecutivo');</script>";
						}
					}
				?>
				<div id="bgventanamodal2">
					<div id="ventanamodal2">
						<IFRAME  src="" name="buster" marginWidth=0 marginHeight=0 frameBorder=0 id="ventana2" frameSpacing=0 style="left:500px; width:900px; height:500px; top:200;">
						</IFRAME>
					</div>
				</div>
			</div>
		</form>
	</body>
</html>
