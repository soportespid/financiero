<?php
	header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=utf8");
	require 'comun.inc';
	require 'funciones.inc';
	require 'conversor.php';
	session_start();
	$linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");
	cargarcodigopag($_GET['codpag'],$_SESSION["nivel"]);
	date_default_timezone_set("America/Bogota");
	$scrtop = $_GET['scrtop'];
	$totreg = $_GET['totreg'];
	$idcta = $_GET['idcta'];
	$altura = $_GET['altura'];
	$filtro = $_GET['filtro'];
	$numpag = $_GET['numpag'];
	$limreg = $_GET['limreg'];
	$scrtop = 20 * $totreg;
?>
<!DOCTYPE >
<html lang="es">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="ie=edge"/>
		<title>:: IDEAL 10 - Gesti&oacute;n humana</title>
		<link href="css/css2.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css">
		<link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css">
		<link href="css/css4.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css">
		<link href="sweetalert2/dist/sweetalert2.min.css" rel="stylesheet">
		<script src="sweetalert2/dist/sweetalert2.min.js"></script>
		<script type="text/javascript" src="css/calendario.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script type="text/javascript" src="css/programas.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script>
			function guardar(){
				if(document.form2.totalinca.value>0){
					if (document.form2.tercero.value != '' && document.form2.periodo.value != ''){
						Swal.fire({
						icon: 'question',
						title: '¿Esta Seguro de guardar?',
						showDenyButton: true,
						confirmButtonText: 'Guardar',
						confirmButtonColor: '#01CC42',
						denyButtonText: 'Cancelar',
						denyButtonColor: '#FF121A',
						}).then(
							(result) => {
								if (result.isConfirmed){
									document.form2.oculto.value = "2";
									document.form2.submit();
								}else if (result.isDenied){
									Swal.fire({
										icon: 'info',
										title: 'No se guardo',
										confirmButtonText: 'Continuar',
										confirmButtonColor: '#FF121A',
										timer: 2500
									});
								}
							}
						)
					}else{
						Swal.fire({
							icon: 'error',
							title: 'Error!',
							text: 'Faltan datos para completar la incapacidad',
							confirmButtonText: 'Continuar',
							confirmButtonColor: '#FF121A',
							timer: 2500
						});
					}
				}
				else{
					Swal.fire({
						icon: 'error',
						title: 'Error!',
						text: 'Falta asignar detalles a la incapacidad',
						confirmButtonText: 'Continuar',
						confirmButtonColor: '#FF121A',
						timer: 2500
					});
				}
			}
			function validar(formulario){
				document.form2.submit();
			}
			function buscater(e){
				if (document.form2.tercero.value != ""){
					document.form2.bt.value = '1';
					document.form2.submit();
				}
			}
			
			function fagregar(){
				if(document.form2.vigenciat.value != ""){
					if(document.form2.tipoinca.value != ""){
						if(document.form2.periodo.value != "-1"){
							if(document.form2.diasperiodo.value != ""){
								if(document.form2.porcinca.value != ""){
									if(document.form2.salbas.value != ""){
										document.form2.oculto.value = "4";
										document.form2.submit();
									}
									else{
										Swal.fire({
											icon: 'error',
											title: 'Error!',
											text: 'Falta un funcionario con cargo asignado',
											confirmButtonText: 'Continuar',
											confirmButtonColor: '#FF121A',
											timer: 2500
										});
									}
								}
								else{
									Swal.fire({
										icon: 'error',
										title: 'Error!',
										text: 'Falta asignar porcentaje de incapacidad',
										confirmButtonText: 'Continuar',
										confirmButtonColor: '#FF121A',
										timer: 2500
									});
								}
							}
							else{
								Swal.fire({
									icon: 'error',
									title: 'Error!',
									text: 'Falta asignar días de incapacidad',
									confirmButtonText: 'Continuar',
									confirmButtonColor: '#FF121A',
									timer: 2500
								});
							}
						}
						else{
							Swal.fire({
								icon: 'error',
								title: 'Error!',
								text: 'Falta asignar el mes',
								confirmButtonText: 'Continuar',
								confirmButtonColor: '#FF121A',
								timer: 2500
							});
						}
					}
					else{
						Swal.fire({
							icon: 'error',
							title: 'Error!',
							text: 'Falta asignar tipo de incapacidad',
							confirmButtonText: 'Continuar',
							confirmButtonColor: '#FF121A',
							timer: 2500
						});
					}
				}
				else{
					Swal.fire({
						icon: 'error',
						title: 'Error!',
						text: 'Falta activar la vigencia actual',
						confirmButtonText: 'Continuar',
						confirmButtonColor: '#FF121A',
						timer: 2500
					});
				}
			}
			function eliminar(variable){
				Swal.fire({
					icon: 'question',
					title: '¿Esta seguro de eliminar?',
					showDenyButton: true,
					confirmButtonText: 'Guardar',
					confirmButtonColor: '#01CC42',
					denyButtonText: 'Cancelar',
					denyButtonColor: '#FF121A',
				}).then(
					(result) => {
						if (result.isConfirmed){
							document.getElementById('elimina').value = variable;
							document.form2.oculto.value = "3";
							document.form2.submit();
						}else if (result.isDenied){
							Swal.fire({
								icon: 'info',
								title: 'No se Elimino',
								confirmButtonText: 'Continuar',
								confirmButtonColor: '#FF121A',
								timer: 2500
							});
						}
					}
				)
			}
			function cambiocheck(id){
				switch(id){
					case '1':
						if(document.getElementById('idswibc').value == 'S'){
							document.getElementById('idswibc').value = 'N';
						}else{
							document.getElementById('idswibc').value = 'S';
						}
						break;
					case '2':
						if(document.getElementById('idswarl').value == 'S'){
							document.getElementById('idswarl').value = 'N';
						}else {
							document.getElementById('idswarl').value = 'S';
						}
						break;
					case '3':
						if(document.getElementById('idswparafiscal').value == 'S'){
							document.getElementById('idswparafiscal').value =  'N';
						}else {
							document.getElementById('idswparafiscal').value = 'S';
						}
				}
				document.form2.submit();
			}
			function atrasc(scrtop,numpag,limreg,filtro,totreg,altura){
				var codig = document.form2.idcomp.value;
				var minim = document.form2.minimo.value;
				codig = parseFloat(codig)-1;
				if(codig => minim){location.href = "hum-incapacidadeseditar.php?idinca=" + codig + "&scrtop=" + scrtop + "&totreg=" + totreg + "&altura=" + altura + "&numpag=" + numpag + "&limreg=" + limreg + "&filtro=" + filtro;}
			}
			function adelante(scrtop,numpag,limreg,filtro,totreg,altura){
				var codig = document.form2.idcomp.value;
				var maxim = document.form2.maximo.value;
				codig=parseFloat(codig)+1;
				if(codig <= maxim){location.href = "hum-incapacidadeseditar.php?idinca=" + codig + "&scrtop=" + scrtop + "&totreg=" + totreg + "&altura=" + altura + "&numpag=" + numpag + "&limreg=" + limreg + "&filtro=" + filtro;}
			}
			function iratras(scrtop,numpag,limreg,filtro,totreg,altura){
				var codig = document.getElementById('idcomp').value;
				location.href = "hum-incapacidadesbuscar.php?idban=" + codig + "&scrtop=" + scrtop + "&totreg=" + totreg + "&altura=" + altura + "&numpag=" + numpag + "&limreg=" + limreg + "&filtro=" + filtro;
			}
		</script>
		<?php titlepag();?>
	</head>
	<body>
		<IFRAME src="alertas.php" name="alertas" id="alertas" style="display:none"></IFRAME>
		<span id="todastablas2"></span>
		<table>
			<tr><script>barra_imagenes("hum");</script><?php cuadro_titulos();?></tr>
			<tr><?php menu_desplegable("hum");?></tr>
			<tr>
				<td colspan="3" class="cinta"><img src="imagenes/add.png" title="Nuevo" onClick="location.href='hum-incapacidadesagregar.php'" class="mgbt"><img src="imagenes/guarda.png" title="Guardar" onClick="guardar()" class="mgbt"><img src="imagenes/busca.png" title="Buscar" onClick="location.href='hum-incapacidadesbuscar.php'" class="mgbt"><img src="imagenes/nv.png" title="nueva ventana"  class="mgbt" onClick="mypop=window.open('hum-principal.php','','');mypop.focus();"><img src='imagenes/iratras.png' title='Men&uacute; Nomina' class='mgbt' onClick="iratras(<?php echo "'$scrtop','$numpag','$limreg','$filtro','$totreg','$altura'"; ?>)"></td>
			</tr>
		</table>
		<div id="bgventanamodalm" class="bgventanamodalm">
			<div id="ventanamodalm" class="ventanamodalm">
				<IFRAME src="" name="ventanam" marginWidth=0 marginHeight=0 frameBorder=0 id="ventanam" frameSpacing=0 style=" width:700px; height:130px; top:200; overflow:hidden;"> 
				</IFRAME>
			</div>
		</div>
		<form name="form2" method="post" action="">
			<?php
				if(!$_POST['oculto']){
					$_POST['idcomp'] = $_GET['idinca'];
					$sqlr = "SELECT * FROM hum_incapacidades WHERE num_inca = '".$_POST['idcomp']."'";
					$res = mysqli_query($linkbd,$sqlr);
					$row = mysqli_fetch_row($res);
					$_POST['fecha'] = date('d-m-Y',strtotime($row[1]));
					$_POST['vigenciat'] = $row[2];
					$_POST['tercero'] = $row[3];
					$_POST['ntercero'] = $row[4];
					$_POST['incaeps'] = $row[5];
					$_POST['incaepsant'] = $row[6];
					$_POST['salbas'] = $row[7];
					$_POST['fechai'] = date('d/m/Y',strtotime($row[8]));
					$_POST['fechaf'] = date('d/m/Y',strtotime($row[9]));
					$_POST['tipoinca'] = $row[10];
					$_POST['swibc'] = $row[12];
					$_POST['swarl'] = $row[13];
					$_POST['swparafiscal'] = $row[14];
					$sql1 = "SELECT * FROM hum_incapacidades_det WHERE estado = 'S' AND num_inca = '".$_POST['idcomp']."'";
					$res1 = mysqli_query($linkbd,$sql1);
					while ($row1 = mysqli_fetch_row($res1)){
						$_POST['idinca'][] = $row1[0];
						$_POST['viginca'][] = $row1[4];
						$_POST['mesinca'][] = $row1[5];
						$_POST['diasinca'][] = $row1[6];
						$_POST['valordiainca'][] = $row1[8];;
						$_POST['valorinca'][] = $row1[9];;
						$_POST['porvinca'][] = $row1[7];
						$_POST['tipinca'][] = $row1[3];
					}
					$sqlr = "SELECT salario FROM admfiscales WHERE vigencia = '".$_POST['vigenciat']."'";
					$resp = mysqli_query($linkbd,$sqlr);
					while ($row = mysqli_fetch_row($resp)){$_POST['salmin'] = $row[0];}
					$sqlb = "SELECT MIN(num_inca),MAX(num_inca) FROM hum_incapacidades";
					$resb = mysqli_query($linkbd,$sqlb);
					$rowb = mysqli_fetch_array($resb);
					$_POST['maximo'] = $rowb[1];
					$_POST['minimo'] = $rowb[0];
					$_POST['ideliminar'] = "";
				}
			?>
			<table class="inicio ancho">
				<tr>
					<td class="titulos" colspan="7">:: Incapacidades y Licencias</td>
					<td class="cerrar" style="width:7%" onClick="location.href='hum-principal.php'">Cerrar</td>
				</tr>
				<tr> 
					<td class="saludo1" style="width:3.5cm;">No Incapacidad:</td>
					<td style="width:12%"><img src="imagenes/back.png" onClick="atrasc(<?php echo "'$scrtop', '$numpag', '$limreg', '$filtro', '$totreg', '$altura'"; ?>)" class="icobut" title="Anterior">&nbsp;<input type="text" name="idcomp" id="idcomp" value="<?php echo $_POST['idcomp']?>" style="width:60%;" readonly>&nbsp;<img src="imagenes/next.png" onClick="adelante(<?php echo "'$scrtop', '$numpag', '$limreg', '$filtro', '$totreg', '$altura'"; ?>)" class="icobut" title="Sigiente"></td>
					<td class="saludo1"  style="width:4cm;">Fecha Registro:</td>
					<td style="width:12%"><input type="date" name="fecha"  value="<?php echo $_POST['fecha']?>" style="width:98%;" readonly/></td>
					<td class="saludo1" style="width:3.5cm;">Vigencia:</td>
					<td style="width:14%"><input type="text" name="vigenciat" id="vigenciat" value="<?php echo $_POST['vigenciat'];?>" style="width:100%;" readonly/></td>
					<td rowspan="6" colspan="2" style="background:url(imagenes/incapacidad.png); background-repeat:no-repeat; background-position:center; background-size: 60% 100%;" ></td>
				</tr>
				<tr>
					<td class="saludo1">Funcionario:</td>
					<td><input type="text" id="tercero" name="tercero" onKeyUp="return tabular(event,this)" value="<?php echo $_POST['tercero']?>" style="width:100%;" readonly/></td>
					<td colspan="4"><input type="text" name="ntercero" id="ntercero" value="<?php echo $_POST['ntercero']?>" style="width: 100%;"readonly/></td>
					<input type="hidden" name="bt" id="bt" value="0"/>
				</tr>
				<tr>
					<td class="saludo1">Cod Incapacidad EPS:</td>
					<td><input type="text" name="incaeps" id="incaeps" value="<?php echo $_POST['incaeps']?>" style="width:98%;"/></td>
					<td class="saludo1">Cod Incapacidad Ant. EPS:</td>
					<td><input name="incaepsant" type="text" value="<?php echo $_POST['incaepsant']?>" style="width:98%;"/></td>
					<td class="saludo1">Salario Basico:</td>
					<td><input type="text" name="salbas" value="<?php echo $_POST['salbas']?>" style="width:100%;"/></td>
				</tr>
				<tr>
					<td class="saludo1">Fecha Inicial:</td>
					<td><input type="text" name="fechai" value="<?php echo $_POST['fechai']?>" maxlength="10" onKeyPress="javascript:return solonumeros(event)" onKeyUp="return tabular(event,this)"  id="fc_1198971545" onKeyDown="mascara(this,'/',patron,true)" title="DD/MM/YYYY" style="width:100%;" class="colordobleclik" onDblClick="displayCalendarFor('fc_1198971545');" autocomplete="off" onChange=""></td>
					<td class="saludo1">Fecha Final:</td>
					<td><input type="text" name="fechaf" value="<?php echo $_POST['fechaf']?>" maxlength="10" onKeyPress="javascript:return solonumeros(event)" onKeyUp="return tabular(event,this)" id="fc_1198971546" onKeyDown="mascara(this,'/',patron,true)" title="DD/MM/YYYY" style="width:100%;" class="colordobleclik" onDblClick="displayCalendarFor('fc_1198971546');" autocomplete="off" onChange=""></td> 
					<td class="saludo1">Tipo Incapacidad:</td>
					<td >
						<select name="tipoinca" id="tipoinca" style="width:100%;" >
							<option value="">Seleccione ....</option>
							<?php
								$sqlr="Select * from dominios where tipo = 'S' and nombre_dominio LIKE 'LICENCIAS' ";
								$resp = mysqli_query($linkbd,$sqlr);
								while ($row = mysqli_fetch_row($resp)){
									if($row[1] == $_POST['tipoinca']){
										echo "<option value='$row[1]' SELECTED>$row[0]</option>";
									}else{
										echo "<option value='$row[1]'>$row[0]</option>";
									}
								}
							?>
						</select>
					</td>
				</tr>
				<tr>
					<td class="saludo1">Mes:</td>
					<td >
						<select name="periodo" id="periodo" style="width:98%;">
							<option value="-1">Seleccione ....</option>
							<?php
								$sqlr = "SELECT * FROM meses WHERE estado = 'S' ";
								$resp = mysqli_query($linkbd,$sqlr);
								while ($row = mysqli_fetch_row($resp)){
									if($row[0] == $_POST['periodo']){
										echo "<option value='$row[0]' SELECTED>$row[1]</option>";
										$_POST['periodonom'] = $row[1];
										$_POST['periodonom'] = $row[2];
									}
									else {
										echo "<option value='$row[0]'>$row[1]</option>";
									}
								}
							?>
						</select>
					</td>
					<td class="saludo1">Dias Incapacidad:</td>
					<td><input type="text" name="diasperiodo" id="diasperiodo" value="<?php echo $_POST['diasperiodo']?>" style="width:98%;" /></td>
					<td class="saludo1">Incapacidad (%):</td>
					<td >
						<select name="porcinca" id="porcinca" style="width:100%;">
							<option value="">Seleccione ....</option>
							<?php
								$sqlr="SELECT codigo,porcentaje FROM hum_porcentajesinca WHERE estado='S' ";
								$resp = mysqli_query($linkbd,$sqlr);
								while ($row = mysqli_fetch_row($resp)){
									if($row[0]==@$_POST['porcinca']){
										echo "<option value='$row[0]' SELECTED>$row[1]%</option>";
									}else {
										echo "<option value='$row[0]'>$row[1]%</option>";
									}
								}
							?>
						</select>
					</td>
				</tr>
				<tr>
					<td class="saludo1" >Pagar EPS y AFP:</td>
					<td style="width:7%">
						<div class="swsino">
							<input type="checkbox" name="swibc" class="swsino-checkbox" id="idswibc" value="<?php echo $_POST['swibc'];?>" <?php if($_POST['swibc']=='S'){echo "checked";}?> onChange="cambiocheck('1');"/>
							<label class="swsino-label" for="idswibc">
								<span class="swsino-inner"></span>
								<span class="swsino-switch"></span>
							</label>
						</div>
					</td>
					<td class="saludo1" >Pagar ARL:</td>
					<td style="width:7%">
						<div class="swsino">
							<input type="checkbox" name="swarl" class="swsino-checkbox" id="idswarl" value="<?php echo $_POST['swarl'];?>" <?php if($_POST['swarl']=='S'){echo "checked";}?> onChange="cambiocheck('2');"/>
							<label class="swsino-label" for="idswarl">
								<span class="swsino-inner"></span>
								<span class="swsino-switch"></span>
							</label>
						</div>
					</td>
					<td class="saludo1" >Pagar Parafiscales:</td>
					<td style="width:7%">
						<div class="swsino">
							<input type="checkbox" name="swparafiscal" class="swsino-checkbox" id="idswparafiscal" value="<?php echo $_POST['swparafiscal'];?>" <?php if($_POST['swparafiscal']=='S'){echo "checked";}?> onChange="cambiocheck('3');"/>
							<label class="swsino-label" for="idswparafiscal">
								<span class="swsino-inner"></span>
								<span class="swsino-switch"></span>
							</label>
						</div>
					</td>
				</tr>
				<tr>
					<td style="height:35px;"><em class="botonflechaverde" onClick="fagregar();">Agragar</em></td>
				</tr>
			</table> 
			<input type="hidden" name="oculto" id="oculto" value="1"/>
			<input type="hidden" name="salmin" id="salmin"  value="<?php echo @$_POST['salmin']?>"/>
			<div class="subpantalla" style="height:33%; width:99.5%;overflow-x:hidden" > 
				<table class="inicio" width="99%">
					<tr><td class="titulos" colspan="9">Detalles Incapacidad</td></tr>
					<tr>
						<td class="titulos2" style="width:6%">Item</td>
						<td class="titulos2" style="width:6%">Tipo</td>
						<td class="titulos2" style="width:6%">Vigencia</td>
						<td class="titulos2">Mes</td>
						<td class="titulos2">Dias</td>
						<td class="titulos2">%</td>
						<td class="titulos2">Valor Dia</td>
						<td class="titulos2">Valor Total</td>
						<td class="titulos2" style="width:8%">Eliminar</td>
					</tr>
					<?php
						if ($_POST['oculto']=='3'){
							$posi = $_POST['elimina'];
							if($_POST['idinca'][$posi] != ""){
								if($_POST['ideliminar'] == ""){
									$_POST['ideliminar'] = $_POST['idinca'][$posi];
								}else {
									$_POST['ideliminar'] = $_POST['ideliminar']."<->".$_POST['idinca'][$posi];
								}
							}
							unset($_POST['idinca'][$posi]);
							unset($_POST['viginca'][$posi]);
							unset($_POST['mesinca'][$posi]);
							unset($_POST['diasinca'][$posi]);
							unset($_POST['valordiainca'][$posi]);
							unset($_POST['valorinca'][$posi]);
							unset($_POST['porvinca'][$posi]);
							unset($_POST['tipinca'][$posi]);
							$_POST['idinca'] = array_values($_POST['idinca']);
							$_POST['viginca'] = array_values($_POST['viginca']);
							$_POST['mesinca'] = array_values($_POST['mesinca']);
							$_POST['diasinca'] = array_values($_POST['diasinca']);
							$_POST['valordiainca'] = array_values($_POST['valordiainca']);
							$_POST['valorinca'] = array_values($_POST['valorinca']);
							$_POST['porvinca'] = array_values($_POST['porvinca']);
							$_POST['tipinca'] = array_values($_POST['tipinca']);
							$_POST['elimina'] ='';
						}	 
						if ($_POST['oculto'] == '4'){
							switch($_POST['tipoinca']){
								case "EP":
								case "EG":
								case "LR":
									switch($_POST['porcinca']){ 
										case 1:{
											$totaldias = $_POST['diasperiodo'];
											$saldia = $_POST['salbas']/30;
											$diasalmin = ($_POST['salmin']/30);
											if($totaldias > 2){
												$saldo1 = $saldia * 2;
												$saldia2 = ($saldia*66.667)/100;
												if($saldia2 >= $diasalmin){
													$saldia2g = $saldia2;
												}else {
													$saldia2g = $diasalmin;
												}
												$saldo2 = $saldia2g * ($totaldias - 2);
												//100%
												$_POST['idinca'][] = "";
												$_POST['viginca'][] = $_POST['vigenciat'];
												$_POST['mesinca'][] = $_POST['periodo'];
												$_POST['diasinca'][] = 2;
												$_POST['valordiainca'][] = $saldia;
												$_POST['valorinca'][] = round($saldo1);
												$_POST['porvinca'][] = 100;
												$_POST['tipinca'][] = $_POST['tipoinca'];
												//66,667%
												$_POST['idinca'][] = "";
												$_POST['viginca'][] = $_POST['vigenciat'];
												$_POST['mesinca'][] = $_POST['periodo'];
												$_POST['diasinca'][] = $totaldias-2;
												$_POST['valordiainca'][] = $saldia2g;
												$_POST['valorinca'][] = round($saldo2);
												$_POST['porvinca'][] = 66.667;
												$_POST['tipinca'][] = $_POST['tipoinca'];
											}else{
												$saldo1 = round($saldia * $totaldias);
												$_POST['idinca'][] = "";
												$_POST['viginca'][] = $_POST['vigenciat'];
												$_POST['mesinca'][] = $_POST['periodo'];
												$_POST['diasinca'][] = $totaldias;
												$_POST['valordiainca'][] = $saldia;
												$_POST['valorinca'][] = round($saldo1);
												$_POST['porvinca'][] = 100;
												$_POST['tipinca'][] = $_POST['tipoinca'];
											}
										}break;
										case 2:{
											$saldia = $_POST['salbas'] / 30;
											$totaldias = $_POST['diasperiodo'];
											$diasalmin = ($_POST['salmin'] / 30);
											$saldia2 = ($saldia * 66.667) / 100;
											if($saldia2 >= $diasalmin){
												$saldia2g = $saldia2;
											}else {
												$saldia2g = $diasalmin;
											}
											$saldo2 = $saldia2g * ($totaldias);
											//66,667%
											$_POST['idinca'][] = "";
											$_POST['viginca'][] = $_POST['vigenciat'];
											$_POST['mesinca'][] = $_POST['periodo'];
											$_POST['diasinca'][] = $totaldias;
											$_POST['valordiainca'][] = $saldia2g;
											$_POST['valorinca'][] = round($saldo2);
											$_POST['porvinca'][] = 66.667;
											$_POST['tipinca'][] = $_POST['tipoinca'];
										}break;
										case 3:{
											$saldia = $_POST['salbas'] / 30;
											$totaldias = $_POST['diasperiodo'];
											$diasalmin = ($_POST['salmin'] / 30);
											$saldia2 = ($saldia * 50) / 100;
											if($saldia2 >= $diasalmin){
												$saldia2g = $saldia2;
											}else {
												$saldia2g =  $diasalmin;
											}
											$saldo2 = $saldia2g * ($totaldias);
											//50%
											$_POST['idinca'][] = "";
											$_POST['viginca'][] = $_POST['vigenciat'];
											$_POST['mesinca'][] = $_POST['periodo'];
											$_POST['diasinca'][] = $totaldias;
											$_POST['valordiainca'][] = $saldia2g;
											$_POST['valorinca'][] = round($saldo2);
											$_POST['porvinca'][] = 50;
											$_POST['tipinca'][] = $_POST['tipoinca'];
										}break;
										case 4:{
											$_POST['idinca'][] = "";
											$_POST['viginca'][] = $_POST['vigenciat'];
											$_POST['mesinca'][] = $_POST['periodo'];
											$_POST['diasinca'][] = $_POST['diasperiodo'];
											$_POST['valordiainca'][] = 0;
											$_POST['valorinca'][] = 0;
											$_POST['porvinca'][] = 0;
											$_POST['tipinca'][] = $_POST['tipoinca'];
										}break;
									}break;
								case "180":{
									$saldia = $_POST['salbas'] / 30;
									$totaldias = $_POST['diasperiodo'];
									$saldia2 = ($saldia * 50) / 100;
									$saldo2 = $saldia2 * ($totaldias);
									//50%
									$_POST['idinca'][] = "";
									$_POST['viginca'][] = $_POST['vigenciat'];
									$_POST['mesinca'][] = $_POST['periodo'];
									$_POST['diasinca'][] = $totaldias;
									$_POST['valordiainca'][] = $saldia2;
									$_POST['valorinca'][] = round($saldo2);
									$_POST['porvinca'][] = 50;
									$_POST['tipinca'][] = $_POST['tipoinca'];
								}break;
								case "LNR":{
									$_POST['idinca'][] = "";
									$_POST['viginca'][] = $_POST['vigenciat'];
									$_POST['mesinca'][] = $_POST['periodo'];
									$_POST['diasinca'][] = $_POST['diasperiodo'];
									$_POST['valordiainca'][] = 0;
									$_POST['valorinca'][] = 0;
									$_POST['porvinca'][] = 0;
									$_POST['tipinca'][] = $_POST['tipoinca'];
								}break;
								case "LM":
								case "LP":{
									$totaldias = $_POST['diasperiodo'];
									$saldia = $_POST['salbas'] / 30;
									$saldo1 = $saldia * $totaldias;
									//100%
									$_POST['idinca'][] = "";
									$_POST['viginca'][] = $_POST['vigenciat'];
									$_POST['mesinca'][] = $_POST['periodo'];
									$_POST['diasinca'][] = $totaldias;
									$_POST['valordiainca'][] = $saldia;
									$_POST['valorinca'][] = round($saldo1);
									$_POST['porvinca'][] = 100;
									$_POST['tipinca'][] = $_POST['tipoinca'];
								}break;
							}
						}
					?>
					<input type='hidden' name='elimina' id='elimina'/>
					<?php
						$co = "saludo1a";
						$co2 = "saludo2";
						$sumtotal = 0;
						$_POST['totalinca'] = count($_POST['valorinca']);
						$_POST['mestemp'] = "";
						$_POST['mesletras'] = "";
						$_POST['totaldiasinca'] = 0;
						for ($x = 0; $x < $_POST['totalinca']; $x++){
							echo "
							<input type='hidden' name='idinca[]' value='".$_POST['idinca'][$x]."'/>
							<input type='hidden' name='mesinca[]' value='".$_POST['mesinca'][$x]."'/>
							<input type='hidden' name='diasinca[]' value='".$_POST['diasinca'][$x]."'/>
							<input type='hidden' name='valordiainca[]' value='".$_POST['valordiainca'][$x]."'/>
							<input type='hidden' name='valorinca[]' value='".$_POST['valorinca'][$x]."'/>
							<input type='hidden' name='porvinca[]' value='".$_POST['porvinca'][$x]."'/>
							<input type='hidden' name='tipinca[]' value='".$_POST['tipinca'][$x]."'/>
							<tr class='$co'>
								<td style='text-align:right;'>".($x+1)."&nbsp;</td>
								<td style='text-align:center;'>".$_POST['tipinca'][$x]."</td>
								<td style='text-align:right;'><input type='text' name='viginca[]' value='".$_POST['viginca'][$x]."' style='text-align:right; width:100%' class='inpnovisibles'/></td>
								<td style='text-align:right;'>".$_POST['mesinca'][$x]."&nbsp;</td>
								<td style='text-align:right;'>".$_POST['diasinca'][$x]."&nbsp;</td>
								<td style='text-align:right;'>".$_POST['porvinca'][$x]."&nbsp;</td>
								<td style='text-align:right;'>$ ".number_format($_POST['valordiainca'][$x],0)."&nbsp;</td>
								<td style='text-align:right;'>$ ".number_format($_POST['valorinca'][$x],0)."&nbsp;</td>
								<td style='text-align:center;'><img src='imagenes/del.png' onclick='eliminar($x)' class='icoop'></td>
							</tr>";
							$_POST['totaldiasinca'] += $_POST['diasinca'][$x];
							$sumtotal += $_POST['valorinca'][$x];
							$aux = $co;
							$co = $co2;
							$co2 = $aux;
							if($_POST['mestemp'] == ""){
								$_POST['mesletras'] = mesletras($_POST['mesinca'][$x]);
								$_POST['mestemp'] = $_POST['mesinca'][$x];
							}elseif($_POST['mestemp'] != $_POST['mesinca'][$x]){
								$_POST['mesletras'] = $_POST['mesletras']." - ".mesletras($_POST['mesinca'][$x]);
								$_POST['mestemp'] = $_POST['mesinca'][$x];
							}
						}
						$totalred = round($sumtotal, 0, PHP_ROUND_HALF_UP);
						$_POST['valinca']=$totalred;
						$resultado = convertir($totalred);
						echo "
						<tr class='$co' style='text-align:right;'>
							<td colspan='7'>Total:</td>
							<td>$ ".number_format($sumtotal,0)."</td>
						</tr>
						<tr class='titulos2'>
							<td>Son:</td>
							<td colspan='8'>$resultado PESOS</td>
						</tr>";
				?>
					<input type="hidden" name="valinca" value="<?php echo $_POST['valinca']?>">
					<input type="hidden" name="totalinca" id="totalinca" value="<?php echo $_POST['totalinca']?>">
					<input type="hidden" name="mesletras" value="<?php echo $_POST['mesletras']?>">
					<input type="hidden" name="mestemp" value="<?php echo $_POST['mestemp']?>">
					<input type="hidden" name="totaldiasinca" value="<?php echo $_POST['totaldiasinca']?>">
					<input type="hidden" name="ideliminar" value="<?php echo $_POST['ideliminar']?>">
					<input type="hidden" name="maximo" id="maximo" value="<?php echo $_POST['maximo'] ?>">
					<input type="hidden" name="minimo" id="minimo" value="<?php echo $_POST['minimo'] ?>">
				</table>
			</div>
			<?php
				if($_POST['oculto'] == 2){
					preg_match("/([0-9]{2})\/([0-9]{2})\/([0-9]{4})/", $_POST['fechai'],$fecha);
					$fechai = "$fecha[3]-$fecha[2]-$fecha[1]";
					preg_match("/([0-9]{2})\/([0-9]{2})\/([0-9]{4})/", $_POST['fechaf'],$fecha);
					$fechaf = "$fecha[3]-$fecha[2]-$fecha[1]";
					if($_POST['swibc'] == 'S'){$pagoibc = 'S';}
					else {$pagoibc='N';}
					if($_POST['swarl'] == 'S'){$pagoarl = 'S';}
					else {$pagoarl = 'N';}
					if($_POST['swparafiscal'] == 'S'){$pagopara = 'S';}
					else {$pagopara = 'N';}
					$sqlr = "UPDATE hum_incapacidades SET cod_inca_n = '".$_POST['incaeps']."', cod_inca_a = '".$_POST['incaepsant']."', fecha_ini = '$fechai', fecha_fin = '$fechaf', tipo_inca = '".$_POST['tipoinca']."', valor_total = '".$_POST['valinca']."', paga_ibc = '$pagoibc', paga_arl = '$pagoarl', paga_para = '$pagopara', meses = '".$_POST['mesletras']."',dias_total = '".$_POST['totaldiasinca']."' WHERE num_inca = '".$_POST['idcomp']."'";
					if (mysqli_query($linkbd,$sqlr)){
						for($y = 0; $y < $_POST['totalinca']; $y++){
							if($_POST['idinca'][$y] == ""){
								$consdet = selconsecutivo('hum_incapacidades_det','id_det');
								$sqlr1 = "INSERT INTO hum_incapacidades_det (id_det, doc_funcionario, num_inca, tipo_inca, vigencia, mes, dias_inca, porcentaje, valor_dia, valor_total, pagar_ibc, pagar_arl, pagar_para, estado) VALUES ('$consdet', '".$_POST['tercero']."', '".$_POST['idcomp']."', '".$_POST['tipinca'][$y]."', '".$_POST['viginca'][$y]."', '".$_POST['mesinca'][$y]."', '".$_POST['diasinca'][$y]."', '".$_POST['porvinca'][$y]."', '".$_POST['valordiainca'][$y]."', '".$_POST['valorinca'][$y]."', '$pagoibc', '$pagoarl', '$pagopara', 'S')";
								mysqli_query($linkbd,$sqlr1);
							}else{
								$sqlr1 = "UPDATE hum_incapacidades_det SET vigencia = '".$_POST['viginca'][$y]."', pagar_ibc = '$pagoibc', pagar_arl = '$pagoarl', pagar_para = '$pagopara' WHERE id_det = '".$_POST['idinca'][$y]."'";
								mysqli_query($linkbd,$sqlr1);
							}
						}
						$ideli = explode('<->', $_POST['ideliminar']);
						$totaleli = count($ideli);
						for($y = 0; $y < $totaleli; $y++)
						{
							$sqlr1 = "UPDATE hum_incapacidades_det SET estado = 'D' WHERE id_det = '$ideli[$y]'";
							//$sqlr1 = "DELETE FROM `hum_incapacidades_det` WHERE id_det = '$ideli[$y]'";
							mysqli_query($linkbd,$sqlr1);
						}
						echo "
						<script>
							Swal.fire({
								icon: 'success',
								title: 'Se ha modificado con Exito la Incapacidad',
								showConfirmButton: true,
								confirmButtonText: 'Continuar',
								confirmButtonColor: '#01CC42',
								timer: 3500
							});
						</script>";
					}
					else {
						echo"
						<script>
							Swal.fire({
								icon: 'error',
								title: 'Error!',
								text: 'Error al Almacenar Incapacida',
								confirmButtonText: 'Continuar',
								confirmButtonColor: '#FF121A',
								timer: 2500
							});
						</script>";
					}
				}
			?>
		</form>
	</body>
</html>