<?php
	ini_set('max_execution_time',3600);
	require_once 'PHPExcel/Classes/PHPExcel.php';
	include 'PHPExcel/Classes/PHPExcel/IOFactory.php';// PHPExcel_IOFactory
	require "comun.inc";
	require "validaciones.inc";
	require "funciones.inc"; //guardar
	session_start();
	$linkbd = conectar_v7();
    $linkbd -> set_charset("utf8");
	$objPHPExcel = new PHPExcel();// Crea un nuevo objeto PHPExcel
	$objPHPExcel->getProperties()->setCreator("Ideal SAS")
		->setLastModifiedBy("HAFR")
		->setTitle("Lista saldos cxp")
		->setSubject("Saldos")
		->setDescription("Lista saldos cxp")
		->setKeywords("Saldos")
		->setCategory("Tesoreria");
	$objPHPExcel->setActiveSheetIndex(0)
		->mergeCells('A1:F1')
		->mergeCells('A2:F2')
		->setCellValue('A1', 'SALDOS')
		->setCellValue('A2', 'INFORMACION GENERAL');
	$objPHPExcel-> getActiveSheet ()
        -> getStyle ("A1")
        -> getFill ()
        -> setFillType (PHPExcel_Style_Fill :: FILL_SOLID)
        -> getStartColor ()
        -> setRGB ('C8C8C8');
	$objPHPExcel-> getActiveSheet ()
        -> getStyle ("A1:A2")
		-> getFont ()
		-> setBold ( true )
		-> setName ( 'Verdana' )
		-> setSize ( 10 )
		-> getColor ()
		-> setRGB ('000000');
	$objPHPExcel-> getActiveSheet ()
		-> getStyle ('A1:A2')
		-> getAlignment ()
		-> applyFromArray (array ( 'horizontal'  =>  PHPExcel_Style_Alignment :: HORIZONTAL_CENTER ,) );
	$objPHPExcel-> getActiveSheet ()
		-> getStyle ('A3:F3')
		-> getAlignment ()
		-> applyFromArray (array ( 'horizontal'  =>  PHPExcel_Style_Alignment :: VERTICAL_JUSTIFY ,) );
	$objPHPExcel-> getActiveSheet ()
		-> getStyle ("A2")
        -> getFill ()
        -> setFillType (PHPExcel_Style_Fill :: FILL_SOLID)
        -> getStartColor ()
        -> setRGB ('A6E5F3');
	$objPHPExcel-> getActiveSheet ()
		-> getStyle ("A3:F3")
        -> getFill ()
        -> setFillType (PHPExcel_Style_Fill :: FILL_SOLID)
        -> getStartColor ()
        -> setRGB ('22C6CB');
	$borders = array(
		'borders' => array(
			'allborders' => array(
				'style' => PHPExcel_Style_Border::BORDER_THIN,
				'color' => array('argb' => 'FF000000'),
			)
		),
    );
	$objPHPExcel->getActiveSheet()->getStyle('A1:F1')->applyFromArray($borders);
	$objPHPExcel->getActiveSheet()->getStyle('A2:F2')->applyFromArray($borders);
	$objPHPExcel->getActiveSheet()->getStyle('A3:F3')->applyFromArray($borders);
	$objPHPExcel-> getActiveSheet()->getColumnDimension('A')->setWidth(12);
	$objPHPExcel-> getActiveSheet()->getColumnDimension('B')->setWidth(11);
	$objPHPExcel-> getActiveSheet ()
		-> getStyle ('B:C')
		-> getAlignment ()
		-> applyFromArray (array ( 'horizontal'  =>  PHPExcel_Style_Alignment :: VERTICAL_JUSTIFY ,) );
	$objPHPExcel-> getActiveSheet()->getColumnDimension('C')->setWidth(36);
	$objPHPExcel-> getActiveSheet()->getColumnDimension('D')->setWidth(15);
	$objPHPExcel-> getActiveSheet()->getColumnDimension('E')->setWidth(80);
    $objPHPExcel-> getActiveSheet()->getColumnDimension('F')->setWidth(20);
	$objWorksheet = $objPHPExcel->getActiveSheet();
	$objWorksheet->fromArray(array(utf8_encode('No Cxp'),utf8_encode('Vigencia'),utf8_encode('Tercero'),utf8_encode('Documento tercero'),utf8_encode('Detalle'),utf8_encode('Saldo')),NULL,'A3');
	preg_match("/([0-9]{2})\/([0-9]{2})\/([0-9]{4})/", $_GET['fecha1'],$fecha);
	$fechaf = "$fecha[3]-$fecha[2]-$fecha[1]";
    $_POST['vigencia'] = $fecha[3];
	preg_match("/([0-9]{2})\/([0-9]{2})\/([0-9]{4})/", $_GET['fecha2'],$fecha);
	$fechaf2 = $fecha[3]."-".$fecha[2]."-".$fecha[1];
	$cont = 4;


    $sqlr = "SELECT * FROM tesoordenpago WHERE estado='S' AND NOT EXISTS (SELECT 1 FROM tesoegresos WHERE tesoegresos.id_orden = tesoordenpago.id_orden and tesoegresos.estado='S') AND tipo_mov='201' AND tesoordenpago.fecha BETWEEN '$fechaf' AND '$fechaf2'";
	$resp = mysqli_query($linkbd, $sqlr);
	//echo $_POST[conanul]."hola";
	while ($row = mysqli_fetch_row($resp))
	{
        $nomter=buscatercero($row[6]);

		$scrit="";
        $objWorksheet->fromArray(array($row[0],$row[3],utf8_encode($nomter),round($row[6],0),utf8_encode($row[7]),round($row[10],2)),NULL,"A$cont");

        $objPHPExcel->getActiveSheet()->getStyle("A$cont:F$cont")->applyFromArray($borders);
        $objPHPExcel->getActiveSheet()->getStyle("D$cont:F$cont")->getNumberFormat()
        ->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);
        $objPHPExcel->getActiveSheet()->getStyle('A3:F3')->getAlignment()->applyFromArray(array('vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,));
        $objPHPExcel->getActiveSheet()->getStyle('A4:F4')->getAlignment()->applyFromArray(array('vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,));
        $objPHPExcel->getActiveSheet()->getStyle('A4')->getAlignment()->applyFromArray(array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,));
        $objPHPExcel->getActiveSheet()->getStyle('C3')->getAlignment()->applyFromArray(array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,));
        $objPHPExcel->getActiveSheet()->getStyle('A:E')->getAlignment()->applyFromArray(array('vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,));
        $objPHPExcel->getActiveSheet()->getStyle('A')->getAlignment()->applyFromArray(array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,));
        $objPHPExcel-> getActiveSheet()-> getStyle ("A3:F3")-> getFont ()-> setBold ( true );


        $cont++;
	}
	$objPHPExcel->getActiveSheet()->setTitle('Listado 1');// Renombrar Hoja
	$objPHPExcel->setActiveSheetIndex(0);// Establecer la hoja activa, para que cuando se abra el documento se muestre primero.
	// --------Cerrar--------
	header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
	header('Content-Disposition: attachment;filename="Listado cxp.xlsx"');
	header('Cache-Control: max-age=0');
	header ('Expires: Mon, 15 Dic 2015 09:31:00 GMT'); // Date in the past
	header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
	header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
	header ('Pragma: public'); // HTTP/1.0
	$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
	$objWriter->save('php://output');
	exit;
?>
