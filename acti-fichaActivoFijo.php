<?php
	header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=utf8");
	require 'comun.inc';
	require 'funciones.inc';
	session_start();
	$linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");
	cargarcodigopag($_GET['codpag'],$_SESSION["nivel"]);
	date_default_timezone_set("America/Bogota");
?>
<!DOCTYPE >
<html lang=es>
	<head>
		<meta charset="utf-8"/>
		<meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1"/>
		<meta name="viewport" content="user-scalable=no">
		<title>:: IDEAL 10 - Activos Fijos</title>
		<link href="favicon.ico" rel="shortcut icon"/>
		<link href="css/css2z.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
		<link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
		<link href="css/css4.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
		<link href="css/tabs.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css"/>
		<link rel="stylesheet" href="sweetalert2/dist/sweetalert2.min.css">
		<script src="sweetalert2/dist/sweetalert2.min.js"></script>
		<script src="vue/vue.min.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script src="css/programas.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script src="css/calendario.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script>
			function pdf(){
				document.form2.action="ficha_activos_fijos_pdf.php";
				document.form2.target="_BLANK";
				document.form2.submit(); 
				document.form2.action="";
				document.form2.target="";
			}
		</script>
		<style>
			input[type=file]::file-selector-button {
				margin-right: 20px;
				border: none;
				background: #084cdf;
				/* padding: 5px 5px; */
				border-radius: 10px;
				color: #fff;
				cursor: pointer;
				transition: background .2s ease-in-out;
			}

			input[type=file]::file-selector-button:hover {
				background: #0d45a5;
			}
			input.text, select.text, textarea.text {
				background: silver;
				border: 1px solid #393939;
				border-radius: 5px 5px 5px 5px;
				color: #393939;
				font-size: 12px;
				padding: 5px;
			}

			[v-cloak]{display : none;}

			.centrarSelect {

				height: 30px !important;
				text-align-last:center !important;
			}

			.aumentarTamaño {

				font-size:15px;
			}

			.tamano01 {
				text-align: center !important;
			}


		</style>
	</head>
	<body>
		<header>
			<table>
				<tr><script>barra_imagenes("acti");</script><?php cuadro_titulos();?></tr>
			</table>
		</header>

		<form name="form2" method="post" action="" enctype="multipart/form-data">
			<section id="myapp" v-cloak >
				<nav>
					<table>
						<tr><?php menu_desplegable("acti");?></tr>
						<tr>
							<td colspan="3" class="cinta">
								<img src="imagenes/add2.png" v-on:click="location.href=''" class="mgbt1" title="Nuevo">
								<img src="imagenes/guarda.png" title="Guardar" v-on:click="guardar" class="mgbt">
								<img src="imagenes/buscad.png" v-on:click="location.href=''" class="mgbt1" title="Buscar">
								<img src="imagenes/nv.png" onClick="mypop=window.open('acti-principal.php','',''); mypop.focus();" class="mgbt" title="Nueva Ventana">
								<img src="imagenes/print.png" title="Imprimir" onClick="pdf()" class="mgbt">
							</td>
						</tr>
					</table>
				</nav>

				<article>
					<div>
						<table class="inicio">
							<tr>
								<td class="titulos" colspan="8">.: Ficha de activo fijo</td>
								<td class="cerrar" style='width:7%'><a onClick="location.href='acti-principal.php'">Cerrar</a></td>
							</tr>

							<tr>
								<td class="textonew01" style="width:3.5cm;">.: Placa:</td>
								<td>
									<input type="text" v-model="placa" name="placa" style="text-align:center;" readonly>
								</td>
								
								<td class="textonew01" style="width:3.5cm;">.: Nombre:</td>
								<td colspan="3">
									<input type="text" v-model="nombre" name="nombre" style="width: 98%">
								</td>

								<td rowspan="10" colspan="2" style="text-align: right;"><img v-bind:src="img" alt="Foto" width="250" height="250" name="nombrefoto"> </td>
							</tr> 
							
							<tr> 
								<td class="textonew01" style="width:3.5cm;">.: Fecha de compra:</td>
								<td>
									<input type="text" name="fechaCompra"  value="<?php echo $_POST['fechaCompra']?>" onKeyUp="return tabular(event,this)" id="fechaCompra" title="DD/MM/YYYY" onDblClick="displayCalendarFor('fechaCompra');" class="colordobleclik" autocomplete="off" onchange="" readonly>
								</td>

								<td class="textonew01" style="width:3.5cm;">.: Fecha de activación:</td>
								<td>
									<input type="text" name="fechaActivacion"  value="<?php echo $_POST['fechaActivacion']?>" onKeyUp="return tabular(event,this)" id="fechaActivacion" title="DD/MM/YYYY" onDblClick="displayCalendarFor('fechaActivacion');" class="colordobleclik" autocomplete="off" onChange=""  readonly>
								</td>
							</tr>

							<tr>
								<td class="textonew01" style="width:3.5cm;">.: Fecha registro:</td>
								<td>
									<input type="text" v-model="fechaRegistro" name="fechaRegistro" style="text-align:center;" readonly>
								</td>

								<td class="textonew01" style="width:3.5cm;">.: Valor activo:</td>
								<td>
									<input type="text" v-model="formatonumero(valorActivo)" name="valorActivo" style="text-align:center;" @dblclick="actualizaFechaCompra" readonly>
								</td>
							</tr>

							<tr>
								<td class="textonew01" style="width:3.5cm;">.: Valor depreciado:</td>
								<td>
									<input type="text" v-model="formatonumero(valorDepreciado)" name="valorDepreciado" style="text-align:center;" readonly>
								</td>

								<td class="textonew01" style="width:3.5cm;">.: Valor Corrección:</td>
								<td>
									<input type="text" v-model="formatonumero(valorCorreccion)" name="valorCorreccion" style="text-align:center;" readonly>
								</td>
							</tr>

							<tr>
								<td class="textonew01" style="width:3.5cm;">.: Referencia:</td>
								<td>
									<input type="text" v-model="referencia" name="referencia">
								</td>
								
								<td class="textonew01" style="width:3.5cm;">.: Modelo:</td>
								<td>
									<input type="text" v-model="modelo" name="modelo">
								</td>
							</tr>

							<tr>
								<td class="textonew01" style="width:3.5cm;">.: Serial:</td>
								<td>
									<input type="text" v-model="serial" name="serial">
								</td>

								<td class="textonew01" style="width:3.5cm;">.: Estado:</td>
								<td>
									<input type="text" v-model="estadoActivo" name="estadoActivo" style="width: 100%" readonly>
								</td>
							</tr>
							
							<tr>
								<td class="textonew01" style="width:3.5cm;">.: Clase:</td>
								<td colspan="3">
									<input type="text" v-model="clase" name="clase" style="width: 100%" readonly>
								</td>
							</tr>

							<tr>
								<td class="textonew01" style="width:3.5cm;">.: Grupo:</td>
								<td colspan="3">
									<input type="text" v-model="grupo" name="grupo" style="width: 100%" readonly>
								</td>
							</tr>

							<tr>
								<td class="textonew01" style="width:3.5cm;">.: Tipo:</td>
								<td colspan="3">
									<input type="text" v-model="tipo" name="tipo" style="width: 100%" readonly>
								</td>
							</tr>

							<tr>
								<td class="textonew01" style="width:3.5cm;">.: Prototipo:</td>
								<td>
									<select v-model="prototipo" style="width: 90%; font-size: 10px; margin-top:4px">
										<option value="">Seleccionar prototipo</option>
										<option v-for="proto in prototipos" v-bind:value="proto[0]">
											{{ proto[0] }} - {{ proto[1] }}
										</option>
									</select>
								</td>

								<td class="textonew01" style="width:3.5cm;">.: Dependencia:</td>
								<td>
									<select v-model="area" style="width: 90%; font-size: 10px; margin-top:4px">
										<option value="">Seleccionar dependencia</option>
										<option v-for="depen in dependencias" v-bind:value="depen[0]">
											{{ depen[0] }} - {{ depen[1] }}
										</option>
									</select>
								</td>
							</tr>

							<tr>
								<td class="textonew01" style="width:3.5cm;">.: Ubicación:</td>
								<td style="width: 11%;">
									<select v-model="ubicacion" style="width: 99%; font-size: 10px; margin-top:4px">
										<option value="">Seleccionar ubicación</option>
										<option v-for="ubi in ubicaciones" v-bind:value="ubi[0]">
											{{ ubi[0] }} - {{ ubi[1] }}
										</option>
									</select>
								</td>
							</tr>

							<tr>
								<td class="textonew01" style="width:3.5cm;">.: Centro de costos:</td>
								<td>
									<input type="text" v-model="cc" name="cc" style="width: 100%" readonly>
								</td>

								<td class="textonew01" style="width:3.5cm;">.: Disposición de activo:</td>
								<td>
									<input type="text" v-model="disposicion" name="disposicion" style="width: 100%" readonly>
								</td>
							</tr>

							<tr>
								<td class="textonew01" style="width:3.5cm;">.: Responsable:</td>
								<td colspan="3">
									<input type="text" v-model="responsable" name="responsable" @dblclick="despliegaModalResponsable" style="width: 100%" class="colordobleclik" readonly>
								</td>
							</tr>

							<tr>
								<td class="textonew01">.: Foto:</td>
								<td>							
									<input type="file" id="foto" @change="obtenerImagen">
								</td>

								<td class="textonew01">.: Ficha tecnica:</td>
								<td>							
									<input type="file" id="ficha" @change="obtenerFicha">
								</td>
							</tr>
						</table>
					</div>

					<div id="cargando" v-if="loading" class="loading">
						<span>Cargando...</span>
					</div>

					<div v-show="showModalResponsables">
						<transition name="modal">
							<div class="modal-mask">
								<div class="modal-wrapper">
									<div class="modal-container">
										<table class="inicio ancho">
											<tr>
												<td class="titulos" colspan="2" >Listado terceros</td>
												<td class="cerrar" style="width:7%" @click="showModalResponsables = false">Cerrar</td>
											</tr>

                                            <tr>
                                                <td class="textonew01" style="text-align:center;">Cedula:</td>
                                                <td>
                                                    <input type="text" v-model="searchCedula" v-on:keyup="filtroCedulaResponsable" placeholder="Buscar por número de cedula" style="width: 100%;">
                                                </td>
                                            </tr>
										</table>
										<table class='tablamv'>
											<thead>
												<tr style="text-align:center;">
													<th class="titulosnew00" style="width: 20%;">Documento</th>
													<th class="titulosnew00">Nombre o razón social</th>
                                                    <th style="width: 1%;"></th>
												</tr>
											</thead>
											<tbody>
												<tr v-for="(tercero,index) in terceros" v-bind:class="index % 2 ? 'saludo1a' : 'saludo2'" v-on:click="seleccionaResponsable(tercero[0], tercero[1])">
													<td style="width:20%; text-align:center;"> {{ tercero[0] }} </td>
													<td style="text-align:left;"> {{ tercero[1] }} </td>
												</tr>
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</transition>
					</div>  
				</article>
			</section>
		</form>
		
		<script src="node_modules/read-excel-file/bundle/read-excel-file.min.js"></script>
		<script src="node_modules/xlsx/dist/xlsx.full.min.js"></script>
		<script src="Librerias/vue/vue.min.js"></script>
		<script src="Librerias/vue/axios.min.js"></script>
		<script type="module" src="activos_fijos/reporte_activos/acti-fichaActivoFijo.js?"></script>
		
	</body>
</html>