<?php
require_once ("tcpdf/tcpdf_include.php");
require ('comun.inc');
require "funciones.inc";
require 'funcionesnomima.inc';
session_start();
date_default_timezone_set("America/Bogota");
$linkbd = conectar_v7();
$linkbd->set_charset("utf8");
$sqlr = "SELECT codnovedad FROM hum_prenomina WHERE num_liq ='" . $_POST['idcomp'] . "'";
$res = mysqli_query($linkbd, $sqlr);
$row = mysqli_fetch_row($res);
$sqlrcc = "SELECT T1.descripcion FROM hum_novedadespagos_cab AS T1 INNER JOIN hum_prenomina AS T2 ON T1.prenomina = T2.codigo WHERE num_liq = '" . $_POST['idcomp'] . "'";
$rowcc = mysqli_fetch_row(mysqli_query($linkbd, $sqlrcc));
$descrip = $rowcc[0];
class MYPDF extends TCPDF
{
    public function Header()
    {
        $linkbd = conectar_v7();
        $linkbd->set_charset("utf8");
        $sqlr = "SELECT * FROM configbasica WHERE estado='S'";
        $res = mysqli_query($linkbd, $sqlr);
        while ($row = mysqli_fetch_row($res)) {
            $nit = $row[0];
            $rs = $row[1];
        }

        $this->Image('imagenes/escudo.jpg', 12, 12, 25, 25, 'JPG', '', 'T', true, 300, '', false, false, 0, false, false, false);// Logo
        $this->SetFont('helvetica', 'B', 8);
        $this->SetY(10);
        $this->RoundedRect(10, 10, 190, 29, 1, '1111');
        $this->Cell(30, 29, '', 'R', 0, 'L');
        $this->SetY(10);
        $this->SetX(40);
        $this->SetFont('helvetica', 'B', 12);
        $this->Cell(127, 13, "$rs", 0, 0, 'C');
        $this->SetY(16);
        $this->SetX(40);
        $this->SetFont('helvetica', 'B', 11);
        $this->Cell(127, 10, 'Nit: ' . $nit, 0, 0, 'C');
        //*****************************************************************************************************************************
        $this->SetFont('helvetica', 'B', 14);
        $this->SetY(10);
        $this->Cell(50.1);
        $this->Cell(149, 31, '', 0, 1, 'C');
        $this->SetY(8);
        $this->Cell(50.1);
        //************************************
        $this->SetFont('helvetica', 'B', 8);
        $this->SetY(10);
        $this->Cell(157);
        $this->Cell(33, 15, '', 'LB', 0, 'L');
        $this->SetY(10);
        $this->Cell(157.5);
        $this->Cell(33, 5, 'Número: ' . $_POST['idcomp'], 0, 0, 'L');
        $this->SetY(15);
        $this->Cell(157.5);
        $this->Cell(33, 5, 'Vigencia F: ' . $_POST['fecha'], 0, 0, 'L');
        $this->SetY(20);
        $this->Cell(157.5);
        $this->Cell(33, 5, 'Fecha: ' . $_POST['fecha'], 0, 0, 'L');
        $this->SetY(25);
        $this->SetX(40);
        $this->SetFont('helvetica', 'B', 12);
        $this->Cell(127, 14, 'SOLICITUD DE DISPONIBILIDAD PRESUPUESTAL', 'T', 1, 'C');
    }
    public function Footer()
    {
        $linkbd = conectar_v7();
        $linkbd->set_charset("utf8");
        $sqlr = "SELECT direccion,telefono,web,email FROM configbasica WHERE estado='S'";
        $resp = mysqli_query($linkbd, $sqlr);
        $user = $_SESSION['nickusu'];
        $fecha = date("Y-m-d H:i:s");
        $ip = $_SERVER['REMOTE_ADDR'];
        $useri = $_POST['user'];
        while ($row = mysqli_fetch_row($resp)) {
            $direcc = strtoupper($row[0]);
            $telefonos = $row[1];
            $dirweb = strtoupper($row[3]);
            $coemail = strtoupper($row[2]);
        }
        if ($direcc != '') {
            $vardirec = "Dirección: $direcc, ";
        } else {
            $vardirec = "";
        }
        if ($telefonos != '') {
            $vartelef = "Telefonos: $telefonos";
        } else {
            $vartelef = "";
        }
        if ($dirweb != '') {
            $varemail = "Email: $dirweb, ";
        } else {
            $varemail = "";
        }
        if ($coemail != '') {
            $varpagiw = "Pagina Web: $coemail";
        } else {
            $varpagiw = "";
        }

        $txt = <<<EOD
			$vardirec $vartelef
			$varemail $varpagiw
			EOD;
        $this->SetFont('helvetica', 'I', 5);
        $this->Cell(190, 7, '', 'T', 0, 'T');
        $this->ln(1);
        $this->Write(0, $txt, '', 0, 'C', true, 0, false, false, 0);
        //$this->Cell(12, 7, 'Hecho por: '.$useri, 0, false, 'C', 0, '', 0, false, 'T', 'M');
        $this->Cell(15, 7, 'Impreso por: ' . $user, 0, false, 'C', 0, '', 0, false, 'T', 'M');
        $this->Cell(57, 7, 'IP: ' . $ip, 0, false, 'C', 0, '', 0, false, 'T', 'M');
        $this->Cell(50, 7, 'Fecha: ' . $fecha, 0, false, 'C', 0, '', 0, false, 'T', 'M');
        $this->Cell(54, 7, 'IDEAL.10 S.A.S', 0, false, 'C', 0, '', 0, false, 'T', 'M');
        $this->Cell(25, 7, 'Pagina ' . $this->getAliasNumPage() . '/' . $this->getAliasNbPages(), 0, false, 'C', 0, '', 0, false, 'T', 'M');
    }
}
$pdf = new MYPDF('P', 'mm', 'Letter', true, 'utf8', false);
$pdf->SetDocInfoUnicode(true);
// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('IDEAL10');
$pdf->SetTitle('Certificados');
$pdf->SetSubject('Certificado de Disponibilidad');
$pdf->SetMargins(10, 45, 10);// set margins
$pdf->SetHeaderMargin(45);// set margins
$pdf->SetFooterMargin(17);// set margins
$pdf->SetAutoPageBreak(TRUE, 20);// set auto page breaks
$pdf->AddPage();
$y = $pdf->GetY();
$pdf->SetFillColor(245, 245, 245);
$pdf->SetFont('helvetica', 'B', 8);
$lineas = $pdf->getNumLines($descrip, 140);
$lineas2 = $lineas * 2;
$pdf->Cell(30, $lineas2, "OBJETO:", 'R', 0, 'L', true);
$pdf->SetFont('helvetica', '', 7);
$pdf->MultiCell(160, $lineas2, $descrip, 0, 'L', false, 1, '', '');
$pdf->SetFont('helvetica', 'B', 8);
$ncc = buscacentro($_POST['cc']);

if ($ncc == '') {
    $ncc = 'TODOS';
}

$pdf->Cell(30, 7, "CENTRO DE COSTO:", 'R', 0, 'L', true);
$pdf->SetFont('helvetica', '', 7);
$pdf->MultiCell(160, 7, $_POST['cc'] . $ncc, 'T', 'L', false, 1, '', '', true, 0, false, true, 0, 'M', true);
$pdf->RoundedRect(10, $y, 190, $lineas + 10, 1, '1111');
//titulos tabla
$pdf->ln(5);
$yy = $pdf->GetY();
$pdf->SetY($yy);
$pdf->SetFillColor(222, 222, 222);
$pdf->SetFont('helvetica', 'B', 8);
$pdf->Cell(0.1);
$pdf->Cell(30, 5, 'Código', 0, 0, 'C', 1);
$pdf->SetY($yy);
$pdf->Cell(31);
$pdf->Cell(130, 5, 'Rubro', 0, 0, 'C', 1);
$pdf->SetY($yy);
$pdf->Cell(162);
$pdf->MultiCell(28, 5, 'Valor', 0, 'C', 1, 1, '', '', true, '', false, true, 0, 'M', true);
$pdf->SetFont('helvetica', '', 7);
$pdf->ln(1);
$con = 0;
//datos tabla

while ($con < count($_POST['rubrosp'])) {
    if ($con % 2 == 0) {
        $pdf->SetFillColor(245, 245, 245);
    } else {
        $pdf->SetFillColor(255, 255, 255);
    }
    $desrubro = $_POST['nrubrosp'][$con];
    $pdf->MultiCell(31, 4, '' . $_POST['rubrosp'][$con], 0, 'C', true, 0, '', '', true, 0, false, true, 0, 'M', true);
    $pdf->MultiCell(131, 4, substr('' . $desrubro, 0, 80), 0, 'L', true, 0, '', '', true, 0, false, true, 0, 'M', true);
    $pdf->MultiCell(28, 4, '' . number_format($_POST['vrubrosp'][$con], 2), 0, 'R', true, 1, '', '', true, 0, false, true, 0, 'M', true);
    $con = $con + 1;
}
$pdf->ln(2);
$pdf->Cell(162, 5, 'Total', 'T', 0, 'R');
$pdf->SetX(172);
$pdf->Cell(28, 5, '$ ' . number_format(array_sum($_POST['vrubrosp']), 2), 'T', 0, 'R');
$pdf->ln(10);
$linkbd = conectar_v7();
$linkbd->set_charset("utf8");
$sqlr = "SELECT funcionario,nomcargo FROM firmaspdf_det WHERE idfirmas='1' AND estado ='S' AND fecha < '" . $_POST['fecha'] . "' ORDER BY orden";
$res = mysqli_query($linkbd, $sqlr);
while ($row = mysqli_fetch_row($res)) {
    $_POST['ppto'][] = $row[0];
    $_POST['nomcargo'][] = $row[1];
}
for ($x = 0; $x < count($_POST['ppto']); $x++) {
    $pdf->ln(14);
    $v = $pdf->gety();
    if ($v >= 251) {
        $pdf->AddPage();
        $pdf->ln(20);
        $v = $pdf->gety();
    }
    $pdf->setFont('helvetica', 'B', 8);
    if (($x % 2) == 0) {
        if (isset ($_POST['ppto'][$x + 1])) {
            $pdf->Line(17, $v, 107, $v);
            $pdf->Line(112, $v, 202, $v);
            $v2 = $pdf->gety();
            $pdf->Cell(104, 4, '' . $_POST['ppto'][$x], 0, 1, 'C', false, 0, 0, false, 'T', 'C');
            $pdf->Cell(104, 4, '' . $_POST['nomcargo'][$x], 0, 1, 'C', false, 0, 0, false, 'T', 'C');
            $pdf->SetY($v2);
            $pdf->Cell(295, 4, '' . $_POST['ppto'][$x + 1], 0, 1, 'C', false, 0, 0, false, 'T', 'C');
            $pdf->Cell(295, 4, '' . $_POST['nomcargo'][$x + 1], 0, 1, 'C', false, 0, 0, false, 'T', 'C');
        } else {
            $pdf->Line(50, $v, 160, $v);
            $pdf->Cell(190, 4, '' . $_POST['ppto'][$x], 0, 1, 'C', false, 0, 0, false, 'T', 'C');
            $pdf->Cell(190, 4, '' . $_POST['nomcargo'][$x], 0, 0, 'C', false, 0, 0, false, 'T', 'C');
        }
        $v3 = $pdf->gety();
    }
    $pdf->SetY($v3);
    $pdf->SetFont('helvetica', '', 7);
}

$pdf->Output();
