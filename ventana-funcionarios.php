<?php
	header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=utf8");
	require 'comun.inc';
	require 'funciones.inc';
	session_start();
	$linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");
	cargarcodigopag($_GET['codpag'],$_SESSION["nivel"]);
	date_default_timezone_set("America/Bogota");
?>
<!DOCTYPE >
<html lang=es>
	<head>
		<meta charset="utf-8"/>
		<meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1"/>
		<meta name="viewport" content="user-scalable=no">
		<title>:: SieS</title>
		<link href="favicon.ico" rel="shortcut icon"/>
		<link href="css/css2z.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet"/>
		<link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet"/>
		<link href="css/css4.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet"/>
		<script type="text/javascript" src="css/programas.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script>
			function fagregar(documento,nombre){
				if(document.getElementById('tdocumento').value!='' && document.getElementById('tdocumento').value != null){
					tdocumento=document.getElementById('tdocumento').value;
					parent.document.getElementById(''+tdocumento).value=documento
				}
				if(document.getElementById('tnombre').value!='' && document.getElementById('tnombre').value != null){
					tnombre=document.getElementById('tnombre').value;
					parent.document.getElementById(''+tnombre).value=nombre;
				}
				parent.despliegamodal2("hidden");
			}
		</script> 
		<?php titlepag();?>
	</head>
	<body>
		<form name="form2" method="post">
			<?php
				if($_POST['oculto'] == ""){

					$_POST['tdocumento'] = $_GET['documento'];
					$_POST['tnombre'] = $_GET['nombre'];
				}
			?>
			<table class="inicio" style="width:99.5%">
				<tr>
					<td class="titulos" colspan="6">:: Buscar Funcionario</td>
					<td class="cerrar" style='width:7%' onClick="parent.despliegamodal2('hidden');">Cerrar</td>
				</tr>
				<tr>
					<td class="saludo1" style='width:4cm;'>:: Documento o Nombre:</td>
					<td colspan="4"><input type="search" name="nombre" id="nombre" value="<?php echo @$_POST['nombre'];?>" style='width:100%;'/> </td>
					<td> <input type="button" name="bboton" onClick="document.form2.submit();" value="&nbsp;&nbsp;Buscar&nbsp;&nbsp;" /></td>
				</tr>
			</table> 
			<input type="hidden" name="oculto" id="oculto" value="1"/>
			<input type="hidden" name="tdocumento" id="tdocumento" value="<?php echo @$_POST['tdocumento']?>"/>
			<input type="hidden" name="tnombre" id="tnombre" value="<?php echo @$_POST['tnombre']?>"/>
			<div class="subpantalla" style="height:82%; width:99.2%; overflow-x:hidden;">
				<?php 
					if (@$_POST['nombre']!=""){
						$crit1 = "";
					}else{
						$crit1 = "";
					}
					$sqlr = "SELECT pt.cedulanit, pl.nombrecargo, t.nombre1, t.nombre2, t.apellido1, t.apellido2
					FROM terceros AS t
					INNER jOIN planestructura_terceros AS pt ON pt.cedulanit = t.cedulanit
					INNER JOIN planaccargos AS pl ON pl.codcargo = pt.codcargo
					WHERE pt.estado = 'S'
					$crit1"; 
					$resp = mysqli_query($linkbd,$sqlr);
					$con = mysqli_num_rows($resp);
					echo "
					<table class='inicio' align='center' width='99%'>
						<tr><td colspan='6' class='titulos'>.: Resultados Busqueda:</td></tr>
						<tr><td colspan='7'>funcionarios Encontrados: </td></tr>
						<tr class='titulos2'>
							<td style='width:5%'>No</td>
							<td style='width:8%'>DOCUMENTO</td>
							<td >NOMBRE</td>
							<td >CARGO</td>
						</tr>";	
					$iter='saludo1a';
					$iter2='saludo2';
					$conta=1;
					while ($row = mysqli_fetch_row($resp)) 
					{
						$nombre = '';
						if($row[2] != ''){
							$nombre = $row[2];
							if($row[3] != ''){$nombre = $nombre." $row[3]";}
							if($row[4] != ''){$nombre = $nombre." $row[4]";}
							if($row[5] != ''){$nombre = $nombre." $row[5]";}
						}
						echo "
						<tr class='$iter' onClick=\"javascript:fagregar('$row[0]','$nombre')\">
							<td>$conta</td>
							<td style='text-align:right;'>".number_format($row[0],0,'','.')."</td>
							<td >$nombre</td>
							<td>$row[1]</td>
						</tr>
						";
						$aux=$iter;
						$iter=$iter2;
						$iter2=$aux;
						$conta++;
						
					}
					echo"</table>";
				?>
			</div>
		</form>
	</body>
</html>
