<?php
    require_once("tcpdf/tcpdf_include.php");
    require './funciones.inc';
    require './funcionesSP.inc.php';

    session_start();
    class MYPDF extends TCPDF {

        // Load table data from file

        public function Header() 
        {
            if ($_POST['estado']=='R'){$this->Image('imagenes/reversado02.png',75,41.5,50,15);}
            $linkbd = conectar_v7();
            $linkbd -> set_charset("utf8");
            $sqlr="select *from configbasica where estado='S' ";
            //echo $sqlr;
            $res=mysqli_query($linkbd, $sqlr);
            while($row=mysqli_fetch_row($res))
            {
                $nit=$row[0];
                $rs=$row[1];
                $nalca=$row[6];
            }
            $strCodigo = $_GET['codigo_recaudo'];
            $strFecha = $_GET['fecha'];
            $detallegreso = $_POST['detallegreso'];
            //Parte Izquierda
            $this->Image('imagenes/escudo.jpg',  13, 13, 20, 20, 'JPG', '', 'T', true, 300, '', false, false, 0, false, false, false);// Logo
            $this->SetFont('helvetica','B',8);
            $this->SetY(10);
            $this->RoundedRect(10, 10, 190, 25, 1,'');
            $this->Cell(0.1);
            $this->Cell(26,25,'','R',0,'L'); 
            $this->SetY(11);
            $this->SetX(60);
            $this->SetFont('helvetica','B',8);
            
            //$this->MultiCell(100, 5, "ADMINISTRACIóN PúBLICA COOPERATIVA DE ACUEDUCTO", '', 'C', 0, 0, '', '', true);
            $this->MultiCell(100, 5, strtoupper("$rs"), '', 'C', 0, 0, '', '', true);
            //$this->Cell(40,15,strtoupper("$rs"),0,0,'C'); 
            $this->SetFont('helvetica','B',6.5);
            $this->SetY(12.5);
            $this->SetX(85);
            $this->Cell(40,15,'NIT: '.$nit,0,0,'C');
            //*****************************************************************************************************************************
            $this->SetFont('helvetica','B',9);		
            $this->SetY(23);
            $this->SetX(36);	
            $this->Cell(164,12,"",'T',0,'C');  
            $this->SetY(23);
            $this->SetX(36);
            $this->Cell(160,12,"ESTADO DE CARTERA - REPORTE DE MOVIMIENTOS",'',0,'C'); 
            $this->SetY(10);
            $this->SetX(135);
            $mov='';
            if(isset($_POST['movimiento']))
            {
                if(!empty($_POST['movimiento']))
                {
                    if($_POST['movimiento']=='401'){$mov="DOCUMENTO DE REVERSION";}
                }
            }

            $this->SetFont('helvetica','B',6);
            
            $this->SetY(10);
            $this->SetX(170);
            $this->Cell(35,6.8," FECHA: ".date("d/m/Y"),"L",0,'L');
            $this->SetY(17);
            $this->SetX(170);
            $this->Cell(35,6," VIGENCIA: ".date("Y"),"L",0,'L');
            //**********************************************************
            $this->SetFont('times','B',10);
            $this->ln(12);
            //**********************************************************
        }
        public function Footer() {
        
            $linkbd = conectar_v7();
            $linkbd -> set_charset("utf8");
            $sqlr="SELECT direccion,telefono,web,email FROM configbasica WHERE estado='S'";
            $resp=mysqli_query($linkbd, $sqlr);
            $user = $_SESSION['nickusu'];	
            $fecha = date("Y-m-d H:i:s");
            $ip = $_SERVER['REMOTE_ADDR'];
            $useri = $_POST['user'];
            while($row=mysqli_fetch_row($resp))
            {
                $direcc=strtoupper($row[0]);
                $telefonos=$row[1];
                $dirweb=strtoupper($row[3]);
                $coemail=strtoupper($row[2]);
            }
            if($direcc!=''){$vardirec="Dirección: $direcc, ";}
            else {$vardirec="";}
            if($telefonos!=''){$vartelef="Telefonos: $telefonos";}
            else{$vartelef="";}
            if($dirweb!=''){$varemail="Email: $dirweb, ";}
            else {$varemail="";}
            if($coemail!=''){$varpagiw="Pagina Web: $coemail";}
            else{$varpagiw="";}
            $this->SetFont('helvetica', 'I', 8);
            $txt = <<<EOD
            $vardirec $vartelef
            $varemail $varpagiw
            EOD;
            $this->SetFont('helvetica', 'I', 6);
            $this->Cell(277,10,'','T',0,'T');
            $this->ln(2);
            $this->Write(0, $txt , '', 0, 'C', true, 0, false, false, 0);
            //$this->Cell(25, 10, 'Hecho por: '.$useri, 0, false, 'C', 0, '', 0, false, 'T', 'M');
            $this->Cell(25, 10, 'Impreso por: '.$user, 0, false, 'C', 0, '', 0, false, 'T', 'M');
            $this->SetX(25);
            $this->Cell(107, 10, 'IP: '.$ip, 0, false, 'C', 0, '', 0, false, 'T', 'M');
            $this->Cell(35, 10, 'Fecha: '.$fecha, 0, false, 'C', 0, '', 0, false, 'T', 'M');
            $this->Cell(107, 10, 'IDEAL.10 S.A.S', 0, false, 'C', 0, '', 0, false, 'T', 'M');
            $this->Cell(20, 10, 'Pagina '.$this->getAliasNumPage().'/'.$this->getAliasNbPages(), 0, false, 'C', 0, '', 0, false, 'T','M');

            
        }
        // Colored table
        public function ColoredTable($data) {
            // Colors, line width and bold font
            $this->SetFillColor(222, 222, 222);
            $this->SetTextColor(000);
            $this->SetDrawColor(128, 0, 0);
            $this->SetLineWidth(0.3);
            $this->SetFont('helvetica','',6);
            // Header
            $w = array(30, 30, 30,36,33,31);
            $header =array(
                "Tipo de movimiento",
                "Consecutivo",
                "Fecha",
                "Cobro",
                "Pago",
                "Saldo"
            );
            $num_headers = count($header);
            for ($i=0; $i < $num_headers; $i++) { 
                $this->Cell($w[$i], 7, $header[$i], 0, 0, 'C', 1);
            }
            $this->Ln();
            // Color and font restoration
            $this->SetFillColor(245,245,245);
            $this->SetTextColor(0);
            $this->SetFont('');
            // Data
            $fill = 0;
            foreach($data as $row) {
                $this->Cell($w[0], 6, $row['tipo'], '', 0, 'C', $fill);
                $this->Cell($w[1], 6, $row['consecutivo'], '', 0, 'C', $fill);
                $this->Cell($w[2], 6, $row['fecha'], '', 0, 'C', $fill);
                $this->Cell($w[3], 6, '$'.number_format($row['cobro'],2), '', 0, 'C', $fill);
                $this->Cell($w[4], 6, '$'.number_format($row['pago'],2), '', 0, 'C', $fill);
                $this->Cell($w[5], 6, '$'.number_format($row['saldo'],2), '', 0, 'C', $fill);
                $this->Ln();
                $fill=!$fill;
            }
            $this->Cell(array_sum($w), 0, '', '');
            $this->Ln();
            $this->SetFont('helvetica','B',8);
        }
    }
    class EstadoCartera{
        private $linkbd;
        private $strCodigoUsuario;
        private $intIdTercero;
        private $intIdBarrio;
        private $intIdEstrato;
        private $intIdCliente;
        public function __construct() {
            $this->linkbd = conectar_v7();
            $this->linkbd->set_charset("utf8");
        }

        public function getMovimientos(string $codigo){ 
            $this->strCodigoUsuario = $codigo;
            $sql="SELECT id,id_tercero,id_barrio,id_estrato,estado,cod_usuario FROM srvclientes WHERE cod_usuario=$this->strCodigoUsuario";
            $requestUser = mysqli_query($this->linkbd,$sql)->fetch_assoc();
            if(!empty($requestUser)){
                if($requestUser['estado']=="S"){
                    $this->intIdTercero = $requestUser['id_tercero'];
                    $this->intIdBarrio = $requestUser['id_barrio'];
                    $this->intIdEstrato = $requestUser['id_estrato'];
                    $this->intIdCliente = $requestUser['id'];

                    //DATOS USUARIO
                    $request['usuario']['codigo'] = $requestUser['cod_usuario'];

                    $request['usuario']['nombre'] = mysqli_query($this->linkbd,"SELECT CASE WHEN razonsocial IS NULL OR razonsocial = '' 
                    THEN CONCAT(nombre1,' ',nombre2,' ',apellido1,' ',apellido2) 
                    ELSE razonsocial END AS nombre FROM terceros WHERE id_tercero = $this->intIdTercero")->fetch_assoc()['nombre'];
                    
                    $request['usuario']['barrio'] = mysqli_query($this->linkbd,
                    "SELECT nombre FROM srvbarrios WHERE id = $this->intIdBarrio")->fetch_assoc()['nombre'];

                    $request['usuario']['estrato']= mysqli_query($this->linkbd,
                    "SELECT descripcion FROM srvestratos WHERE id = $this->intIdEstrato")->fetch_assoc()['descripcion'];

                    $request['usuario']['direccion']= mysqli_query($this->linkbd,
                    "SELECT direccion FROM srvdireccion_cliente  WHERE id = $this->intIdCliente")->fetch_assoc()['direccion'];
                    //DATOS FACTURAS
                    $arrFacturas = mysqli_fetch_all(mysqli_query($this->linkbd,
                    "SELECT id_corte,numero_facturacion FROM srvcortes_detalle WHERE id_cliente = $this->intIdCliente"),MYSQLI_ASSOC);
                    $rows = count($arrFacturas);
                    for ($i=0; $i < $rows; $i++) { 
                        $idCorte = $arrFacturas[$i]['id_corte'];
                        $arrFacturas[$i]['fecha'] = mysqli_query($this->linkbd,
                        "SELECT fecha_impresion as fecha FROM srvcortes WHERE numero_corte = $idCorte")->fetch_assoc()['fecha'];
                        if($idCorte == 1){
                            $valorFactura =  consultaValorFactura($arrFacturas[$i]['numero_facturacion']);
                        }else{
                            $valorFactura = consultaValorFacturaSinDeuda($arrFacturas[$i]['numero_facturacion']);
                        }
                        $arrFacturas[$i]['intereses'] = consultaValorIntereses($arrFacturas[$i]['numero_facturacion']);
                        $arrFacturas[$i]['valor_factura'] = $valorFactura;
                    }
                    $request['movimientos']['facturas'] = $arrFacturas;
                    $request['movimientos']['pagos'] = mysqli_fetch_all(mysqli_query($this->linkbd,
                    "SELECT codigo_recaudo, valor_pago, fecha_recaudo as fecha 
                    FROM srv_recaudo_factura WHERE estado != 'REVERSADO' AND id_cliente = $this->intIdCliente"),MYSQLI_ASSOC);

                    $request['movimientos']['acuerdos_pago'] = mysqli_fetch_all(mysqli_query($this->linkbd,
                    "SELECT codigo_acuerdo, valor_abonado, fecha_acuerdo as fecha 
                    FROM srv_acuerdo_cab WHERE estado_acuerdo != 'Reversado' AND id_cliente = $this->intIdCliente"),MYSQLI_ASSOC);
                    
                    $request['movimientos'] = $this->calcMovimientos($request['movimientos']);
                    return $request;
                }
            }
        }

        public function calcMovimientos(array $data){
            $facturas = $data['facturas'];
            $pagos = $data['pagos'];
            $acuerdosPago = $data['acuerdos_pago'];
            $arrMovimientos = array_merge($facturas,$pagos,$acuerdosPago);
            $saldo=0;
            usort($arrMovimientos,function($a,$b){
                $fechaA = new DateTime($a['fecha']);
                $fechaB = new DateTime($b['fecha']);
                if($fechaA == $fechaB){
                    return 0;
                }
                return ($fechaA < $fechaB) ? -1 : 1;
            });
            $arrLength = count($arrMovimientos);
            for ($i=0; $i < $arrLength; $i++) { 
                $arrDate = explode("-",$arrMovimientos[$i]['fecha']);
                $arrMovimientos[$i]['fecha'] = $arrDate[2]."/".$arrDate[1]."/".$arrDate[0];
                $arrMovimientos[$i]['cobro'] = 0;
                $arrMovimientos[$i]['pago'] = 0;
                
                if(isset($arrMovimientos[$i]['numero_facturacion'])){
                    $arrMovimientos[$i]['tipo'] = "FACTURACION";
                    $arrMovimientos[$i]['consecutivo'] = $arrMovimientos[$i]['numero_facturacion'];
                    $valorAnterior = $arrMovimientos[$i-1]['valor_factura'];
                    $valorActual = $arrMovimientos[$i]['valor_factura'];
                    $interes = $arrMovimientos[$i]['intereses'];
                    $interesReal = abs($arrMovimientos[$i-1]['intereses']-$interes);
                    $valorActualTotal = $valorActual+$interesReal;
                    $arrMovimientos[$i]['valor_factura']=$valorActualTotal+$valorAnterior;
                    $arrMovimientos[$i]['cobro'] =$valorActualTotal;
                    $arrMovimientos[$i]['saldo'] =$arrMovimientos[$i]['valor_factura'];
                }elseif(isset($arrMovimientos[$i]['codigo_recaudo'])){
                    $arrMovimientos[$i]['tipo'] = "PAGO DE FACTURA";
                    $arrMovimientos[$i]['consecutivo'] = $arrMovimientos[$i]['codigo_recaudo'];
                    $arrMovimientos[$i]['pago'] =$arrMovimientos[$i]['valor_pago'];
                }elseif(isset($arrMovimientos[$i]['codigo_acuerdo'])){
                    $arrMovimientos[$i]['tipo'] = "ACUERDO DE PAGO";
                    $arrMovimientos[$i]['consecutivo'] = $arrMovimientos[$i]['codigo_acuerdo'];
                    $arrMovimientos[$i]['pago'] =$arrMovimientos[$i]['valor_abonado'];
                }
                $saldo = $arrMovimientos[$i]['saldo'];
                $pago = $arrMovimientos[$i]['pago'];
                if($arrMovimientos['tipo'] == "pago"){  
                    $saldo-=$pago;
                }
                $arrMovimientos[$i]['pago'] = ceil($arrMovimientos[$i]['pago']/100)*100;
                $arrMovimientos[$i]['cobro'] = ceil($arrMovimientos[$i]['cobro']/100)*100;
                $arrMovimientos[$i]['saldo'] = ceil($saldo/100)*100;
            }
            return $arrMovimientos;
        }
    }

    if($_GET['codigo']){
        $obj = new EstadoCartera();
        $arrData = $obj->getMovimientos($_GET['codigo']);
        if(!empty($arrData)){
            $arrMovimientos = $arrData['movimientos'];
            $arrUsuario = $arrData['usuario'];

            $pdf = new MYPDF('P','mm','Letter', true, 'UTF-8', false);
            $pdf->SetCreator(PDF_CREATOR);
            $pdf->SetAuthor('IDEALSAS');
            $pdf->SetTitle('ESTADO DE CARTERA');
            $pdf->SetSubject('ESTADO DE CARTERA');
            $pdf->SetKeywords('TCPDF, PDF, example, test, guide');
            
            // set margins
            $pdf->SetMargins(10, 38, 10);// set margins
            $pdf->SetHeaderMargin(38);// set margins
            $pdf->SetFooterMargin(17);// set margins
            $pdf->SetAutoPageBreak(TRUE, 20);
            
            // set some language-dependent strings (optional)
            if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
                require_once(dirname(__FILE__).'/lang/eng.php');
                $pdf->setLanguageArray($l);
            }
            
            // ---------------------------------------------------------
            // add a page
            $pdf->AddPage();
            $html='
            <table style="font-size:8pt">
                <tbody>
                    <tr>
                        <td style="width:20%;background-color:rgb(222,222,222);text-align:left;">Código de usuario:</td>
                        <td>'.$arrUsuario['codigo'].'</td>
                    </tr>
                    <tr>
                        <td style="width:20%;background-color:rgb(222,222,222);text-align:left;">Nombre:</td>
                        <td>'.$arrUsuario['nombre'].'</td>
                    </tr>
                    <tr>
                        <td style="width:20%;background-color:rgb(222,222,222);text-align:left;">Barrio:</td>
                        <td>'.$arrUsuario['barrio'].'</td>
                    </tr>
                    <tr>
                        <td style="width:20%;background-color:rgb(222,222,222);text-align:left;">Estrato:</td>
                        <td>'.$arrUsuario['estrato'].'</td>
                    </tr>
                    <tr>
                        <td style="width:20%;background-color:rgb(222,222,222);text-align:left;">Dirección:</td>
                        <td>'.$arrUsuario['direccion'].'</td>
                    </tr>
                </tbody>
            </table>';
            $pdf->writeHTML($html);
            $pdf->ColoredTable($arrMovimientos);
            // column titles
            $header = array('Country', 'Capital', 'Area (sq km)', 'Pop. (thousands)');
            $pdf->Output('reporte-estado-cartera.pdf', 'I');
            
        }
    }
?>