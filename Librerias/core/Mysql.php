<?php
	require_once BASE_URL.'/Librerias/core/Conexion.php';
	/**
	 * Ejemplos para usar esta clase
	 *
	 * Para insertar datos:
	 *
	 * $obj = new Mysql();
	 * $sql = "INSERT INTO tabla(nombre,apellido) VALUES(?,?);
	 * $arrData = array("Fulano","Mengano");
	 * $request = $obj->insert($sql,$arrData); Devuelve el id de la fila insertada
	 *
	 * $sql = "SELECT * FROM TABLA";
	 * $obj->select_all($sql) Para llamar varias filas
	 *
	 * $sql = "SELECT * FROM TABLA WHERE id = 1;
	 * $obj->select($sql) Para llamar una fila
	 *
	 * Para actualizar datos:
	 *
	 * $sql = "UPDATE tabla SET nombre = ?,apellido=?;
	 * $arrData = array("Mengano","Méndez");
	 * $request = $obj->update($sql,$arrData);
	 *
	 * Para eliminar datos:
	 *
	 * $sql = "DELETE FROM tabla WHERE nombre = 'Mengano';
	 * $request = $obj->delete($sql); // devuelve 'ok'
	 * 
	 * Para truncar una tabla:
	 * $sql "TRUNCATE TABLE nombre_tabla";
	 * $request = $obj->truncate($sql); //devuelve true
	 * 
	 * Para seleccionar registros con parametros para evitar inyección de código
	 * $sql = "SELECT campos FROM nombre_tabla WHERE cuenta = :cuenta AND fecha = :fechaf";
	 * params = array(
	 * 	':cuenta' => $cuenta,
	 * 	':fechaf' => $fechaf
	 * );
	 * $request = $this->select_all_conParametros($sql, $params);
	 * 
	 * para eliminar un registros con parametros para evitar inyección de código
	 * public function delete_conParametros(string $query, $params = []) {
	 * 	$this->strquery = $query;
	 *	$result = $this->conexion->prepare($this->strquery);
	 * 	$del = $result->execute($params);
	 * 	return $del;
	 * }
	 * $this->delete_conParametros($sql, $params);
	 */
	class Mysql extends Conexion
	{
		private $conexion;
		private $strquery;
		private $arrValues;

		function __construct($base="",$user="")
		{
			$this->conexion = new Conexion($base,$user);
			$this->conexion = $this->conexion->conect();
		}

		//Insertar un registro
		public function insert(string $query, array $values)
		{
			$this->strquery = $query;
			$this->arrValues = $values;
			$insert = $this->conexion->prepare($this->strquery);
			$resInsert = $insert->execute($this->arrValues);
			if($resInsert)
			{
				$lastInsert = $this->conexion->lastInsertId();
			}else{
				$lastInsert = 0;
			}
			return $lastInsert;
		}
		//Busca un registro
		public function select(string $query)
		{
			$this->strquery = $query;
			$result = $this->conexion->prepare($this->strquery);
			$result->execute();
			$data = $result->fetch(PDO::FETCH_ASSOC);
			return $data;
		}
		//Devuelve todos los registros
		public function select_all(string $query)
		{
			$this->strquery = $query;
			$result = $this->conexion->prepare($this->strquery);
			$result->execute();
			$data = $result->fetchall(PDO::FETCH_ASSOC);
			return $data;
		}
		//Actualiza registros
		public function update(string $query, array $values)
		{
			$this->strquery = $query;
			$this->arrValues = $values;
			$update = $this->conexion->prepare($this->strquery);
			$resExecute = $update->execute($this->arrValues);
			return $resExecute;
		}
		//Eliminar un registros
		public function delete(string $query)
		{
			$this->strquery = $query;
			$result = $this->conexion->prepare($this->strquery);
			$del = $result->execute();
			return $del;
		}

		//Truncar una tabla
		public function truncate(string $query) {
			$this->strquery = $query;
			$truncate = $this->conexion->prepare($this->strquery);
			$respTruncate = $truncate->execute();	
			return $respTruncate;
		}

		//Seleccionar registros con parametros para evitar inyección de código
		public function select_all_conParametros(string $query, $params = array()) {
			$this->strquery = $query;
			$stmt = $this->conexion->prepare($this->strquery);
			if (!empty($params)) {
				foreach ($params as $key => $value) {
					$stmt->bindValue($key, $value);
				}
			}
			$stmt->execute();
			return $stmt->fetchAll(PDO::FETCH_ASSOC);
		}

		//Eliminar un registros con parametros para evitar inyección de código
		public function delete_conParametros(string $query, $params = []) {
			$this->strquery = $query;
			$result = $this->conexion->prepare($this->strquery);
			$del = $result->execute($params);
			return $del;
		}
	}
?>
