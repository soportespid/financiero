<?php
    define("BASE_URL",dirname(__DIR__,2));
    define("CANT_BTNS",8);
    require_once BASE_URL."/vendor/autoload.php";

    use Laravel\Route;
    use Laravel\DB;

    require_once BASE_URL."/Librerias/core/Mysql.php";
    require_once BASE_URL."/zxy.inc.php";
    require_once BASE_URL."/conversor.php";
    session_start();
    /*ini_set('display_errors', '1');
    ini_set('display_startup_errors', '1');
    error_reporting(E_ALL);*/
    $flagSession = !empty($_SESSION);
    if($flagSession){
        if($_POST['shortcut']){
            $request = validRoutePermits($_POST['shortcut'],$_SESSION['idusuario']);
            if(!empty($request)){
                $arrResponse = array("login"=>$flagSession,"status"=>true,"route"=>$request);
            }else{
                $arrResponse = array("login"=>$flagSession,"status"=>false,"msg"=>"Ruta no encontrada");
            }
            echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
        }
        if($_POST['action'] =="valid"){
            $arrMainRoutes = getRouteModules();
            $arrOptionsRoutes = getRouteOptions();
            $currentUrl = explode("/",$_SERVER['HTTP_REFERER']);
            $currentPage = $currentUrl[count($currentUrl)-1];
            if(!in_array($currentPage,$arrMainRoutes) && in_array($currentPage,$arrOptionsRoutes)){
                $request = validRoutePermits($currentPage,$_SESSION['idusuario'],true);
                if(!empty($request)){
                    $arrResponse = array("login"=>$flagSession,"status"=>true,"route"=>$request);
                }else{
                    $arrResponse = array("login"=>$flagSession,"status"=>false,"msg"=>"Ruta no encontrada");
                }
            }
        }
    }else{
        echo json_encode(array("login"=>$flagSession,"status"=>false,"msg"=>"no ha iniciado sesion"),JSON_UNESCAPED_UNICODE);
    }
    function baseDatos(){
        $zxy = funcion_empalme();
        $arrData['base'] = base64_decode($zxy[0]);
        $arrData['host'] = base64_decode($zxy[1]);
        $arrData['user'] = base64_decode($zxy[2]);
        $arrData['password'] = base64_decode($zxy[3]);
        return $arrData;
    }
    function dep($array) {
        print "<pre>";
        print_r($array);
        print"</pre>";
    }
    function uploadFile(array $data, string $name){
        $url_temp = $data['tmp_name'];
        $destino = BASE_URL.'/informacion/proyectos/temp/'.$name;
        move_uploaded_file($url_temp, $destino);
    }
    function deleteFile(string $name){
        unlink(BASE_URL.'/informacion/proyectos/temp/'.$name);
    }
    function getRealIP(){
        if (isset($_SERVER["HTTP_CLIENT_IP"])){
            return $_SERVER["HTTP_CLIENT_IP"];
        }elseif (isset($_SERVER["HTTP_X_FORWARDED_FOR"])){
            return $_SERVER["HTTP_X_FORWARDED_FOR"];
        }elseif (isset($_SERVER["HTTP_X_FORWARDED"])){
            return $_SERVER["HTTP_X_FORWARDED"];
        }elseif (isset($_SERVER["HTTP_FORWARDED_FOR"])){
            return $_SERVER["HTTP_FORWARDED_FOR"];
        }elseif (isset($_SERVER["HTTP_FORWARDED"])){
            return $_SERVER["HTTP_FORWARDED"];
        }else{
            return $_SERVER["REMOTE_ADDR"];
        }
    }
    function strClean($strCadena){
        $string = preg_replace(['/\s+/','/^\s|\s$/'],[' ',''], $strCadena);
        $string = trim($string); //Elimina espacios en blanco al inicio y al final
        $string = stripslashes($string); // Elimina las \ invertidas
        //Elimina sentencias SQL y scripts
        $string = str_ireplace("<script>","",$string);
        $string = str_ireplace("</script>","",$string);
        $string = str_ireplace("<script src>","",$string);
        $string = str_ireplace("<script type=>","",$string);
        $string = str_ireplace("SELECT * FROM","",$string);
        $string = str_ireplace("DELETE FROM","",$string);
        $string = str_ireplace("INSERT INTO","",$string);
        $string = str_ireplace("SELECT COUNT(*) FROM","",$string);
        $string = str_ireplace("DROP TABLE","",$string);
        $string = str_ireplace("OR '1'='1","",$string);
        $string = str_ireplace('OR "1"="1"',"",$string);
        $string = str_ireplace('OR ´1´=´1´',"",$string);
        $string = str_ireplace("is NULL; --","",$string);
        $string = str_ireplace("is NULL; --","",$string);
        $string = str_ireplace("LIKE '","",$string);
        $string = str_ireplace('LIKE "',"",$string);
        $string = str_ireplace("LIKE ´","",$string);
        $string = str_ireplace("OR 'a'='a","",$string);
        $string = str_ireplace('OR "a"="a',"",$string);
        $string = str_ireplace("OR ´a´=´a","",$string);
        $string = str_ireplace("OR ´a´=´a","",$string);
        $string = str_ireplace("--","",$string);
        $string = str_ireplace("´","",$string);
        $string = str_ireplace("'","",$string);
        $string = str_ireplace("^","",$string);
        $string = str_ireplace("[","",$string);
        $string = str_ireplace("]","",$string);
        $string = str_ireplace("==","",$string);
        $string = str_ireplace("#","",$string);
        $string = str_ireplace("°","",$string);
        return $string;
    }
    function replaceChar(string $cadena){
        //Reemplazamos la A y a
        $cadena = str_replace(
        array('Á', 'À', 'Â', 'Ä', 'á', 'à', 'ä', 'â', 'ª'),
        array('A', 'A', 'A', 'A', 'a', 'a', 'a', 'a', 'a'),
        $cadena
        );

        //Reemplazamos la E y e
        $cadena = str_replace(
        array('É', 'È', 'Ê', 'Ë', 'é', 'è', 'ë', 'ê'),
        array('E', 'E', 'E', 'E', 'e', 'e', 'e', 'e'),
        $cadena );

        //Reemplazamos la I y i
        $cadena = str_replace(
        array('Í', 'Ì', 'Ï', 'Î', 'í', 'ì', 'ï', 'î'),
        array('I', 'I', 'I', 'I', 'i', 'i', 'i', 'i'),
        $cadena );

        //Reemplazamos la O y o
        $cadena = str_replace(
        array('Ó', 'Ò', 'Ö', 'Ô', 'ó', 'ò', 'ö', 'ô'),
        array('O', 'O', 'O', 'O', 'o', 'o', 'o', 'o'),
        $cadena );

        //Reemplazamos la U y u
        $cadena = str_replace(
        array('Ú', 'Ù', 'Û', 'Ü', 'ú', 'ù', 'ü', 'û'),
        array('U', 'U', 'U', 'U', 'u', 'u', 'u', 'u'),
        $cadena );

        //Reemplazamos la N, n, C y c
        $cadena = str_replace(
        array('Ñ', 'ñ', 'Ç', 'ç',',',';'),
        array('N', 'n', 'C', 'c','',''),
        $cadena
        );
        return $cadena;
    }
    function insertFuncionesAuditoria($tabla,$funciones){
        $con = new Mysql();
        $sql = "SELECT nombre FROM $tabla";
        $arrFunciones = $con->select_all($sql);
        $total = count($arrFunciones);
        $totalNew = count($funciones);
        if($totalNew > 0){
            if($total > 0){
                $arrFunciones = array_column($arrFunciones,"nombre");
                for ($j=0; $j < $totalNew ; $j++) {
                    $strFuncion = ucwords(strtolower($funciones[$j]));
                    if(!in_array($strFuncion,$arrFunciones)){
                        $sql = "INSERT INTO $tabla(nombre) VALUES(?)";
                        $con->insert($sql,array($strFuncion));
                    }
                }
            }else{
                $strFuncion = ucwords(strtolower($funciones[0]));
                $sql = "INSERT INTO $tabla(nombre) VALUES(?)";
                $con->insert($sql,array($strFuncion));
            }
        }
    }
    function insertAuditoria(string $tabla,string $nombreIdFuncion,int $intFuncion,string $strAccion,string $strConsecutivo, string $nombreFuncion = '',string $tablaFuncion=""){
        $con = new Mysql();
        $strAccion = strClean(ucwords(strtolower($strAccion)));
        $strUsuario = $_SESSION['usuario'];
        $strCedula = $_SESSION['cedulausu'];
        $nombreFuncion = ucwords(strtolower($nombreFuncion));
        if($nombreFuncion != "" && $tablaFuncion != ""){
            verificaNombreFuncion($intFuncion, $nombreFuncion,$tablaFuncion);
        }
        $strIp = getRealIP();
        $sql = "INSERT INTO $tabla($nombreIdFuncion,accion,cedula,usuario,ip,consecutivo)VALUES(?,?,?,?,?,?)";
        $arrData = array($intFuncion,$strAccion,$strCedula,$strUsuario,$strIp,$strConsecutivo);
        $con->insert($sql,$arrData);
    }

    function verificaNombreFuncion($intFuncion, $nombreFuncion,$tablaFuncion){
        $con = new Mysql();
        $sql = "SELECT * FROM $tablaFuncion WHERE id = $intFuncion";
        $request = $con->select($sql);
        if(empty($request)){
            $sqlrInsert = "INSERT INTO $tablaFuncion(id, nombre) VALUES(?,?)";
            $con->insert($sqlrInsert,array($intFuncion,$nombreFuncion));
        }
    }

    function formatNum($num,$decimales=0,$separador_decimal=",",$separador_millar=".",$divisa=false){
        $num = "$".number_format($num,$decimales,$separador_decimal,$separador_millar);
        return $num;
    }
    function configBasica(){
        $con = new Mysql();
        $sql = "SELECT * FROM configbasica WHERE estado='S'";
        $request = $con->select($sql);
        return $request;
    }
    function searchUser(string $cedula){
        $con = new Mysql();
        $sql = "SELECT * FROM usuarios WHERE cc_usu = '$cedula'";
        $request = $con->select($sql);
        return $request;
    }
    function getNombreTercero(string $data){
        $con = new Mysql();
        $sql = "SELECT *,CASE WHEN razonsocial IS NULL OR razonsocial = ''
        THEN CONCAT(nombre1,' ',nombre2,' ',apellido1,' ',apellido2)
        ELSE razonsocial END AS nombre
        FROM terceros WHERE cedulanit LIKE '$data%'";
        $request = $con->select($sql);
        $strNombre ="";
        if(!empty($request)){
            $strNombre = preg_replace("/\s+/", " ", trim($request['nombre']));
        }
        return $strNombre;
    }
    function getTerceros(){
        $con = new Mysql();
        $sql = "SELECT *,cedulanit as codigo,CASE WHEN razonsocial IS NULL OR razonsocial = ''
        THEN CONCAT(nombre1,' ',nombre2,' ',apellido1,' ',apellido2)
        ELSE razonsocial END AS nombre
        FROM terceros";
        $request = $con->select_all($sql);
        if(!empty($request)){
            $total = count($request);
            for ($i=0; $i < $total ; $i++) {
                $sql = "SELECT ban.codigo, ban.nombre,te.numero_cuenta as cuenta
                FROM teso_cuentas_terceros te
                LEFT JOIN hum_bancosfun ban ON ban.codigo =te.codigo_banco
                WHERE id_tercero = {$request[$i]['id_tercero']}";
                $request[$i]['cuentas']= $con->select_all($sql);
                $request[$i]['nombre'] = preg_replace("/\s+/", " ", trim($request[$i]['nombre']));
            }
        }
        return $request;
    }
    // La funcion getAllTerceros
    function getAllTerceros(string $txt_search) {
        $con = new Mysql();
        $sql_terceros = "SELECT *,CASE WHEN razonsocial IS NULL OR razonsocial = ''
        THEN CONCAT(nombre1,' ',nombre2,' ',apellido1,' ',apellido2)
        ELSE razonsocial END AS nombre
        FROM terceros WHERE cedulanit LIKE '$txt_search%'
        OR CONCAT_WS(' ', nombre1, nombre2, apellido1, apellido2, razonsocial) LIKE '%$txt_search%'";
        $row_terceros = $con->select_all($sql_terceros);
        return $row_terceros;
    }
    function getNombreIngreso(string $id){
        $con = new Mysql();
        $sql = "SELECT * FROM tesoingresos WHERE codigo='$id' AND estado = 'S'";
        $request = $con->select($sql);
        $strNombre ="";
        if(!empty($request)){
            $strNombre = $request['nombre'];
        }
        return $strNombre;
    }
    function getNombreRetencion(string $id){
        $con = new Mysql();
        $sql = "SELECT * FROM tesoretenciones WHERE id='$id' AND estado = 'S'";
        $request = $con->select($sql);
        $strNombre ="";
        if(!empty($request)){
            $strNombre = $request['nombre'];
        }
        return $strNombre;
    }
    function checkBlock (string $cedula, string $fecha) {
        $con = new Mysql();
        $sql_bloqueo = "SELECT COUNT(*) AS valor FROM dominios dom WHERE dom.nombre_dominio = 'PERMISO_MODIFICA_DOC' AND dom.valor_final <= '$fecha' AND dom.valor_inicial = '$cedula'";
        $request = $con->select($sql_bloqueo);
        if ($request["valor"] >= 1) {$resp = true;} else {$resp = false;}
        return $resp;
    }
    function searchConsec(string $tabla, string $campo) {
        $con = new Mysql();
        $sql_consecutivo = "SELECT MAX(CONVERT($campo, INT)) AS consecutivo FROM $tabla";
        $request = $con->select($sql_consecutivo);
        $consecutivo = $request["consecutivo"] + 1;
        return $consecutivo;
    }
    /* function searchConsecVps(string $tabla, string $campo) {
        $intIdUser = $_SESSION['idusuario'] ?? null; // Verificamos que la sesión esté definida

        if (!$intIdUser) {
            throw new \Exception("Usuario no autenticado.");
        }

        $tenant_db = DB::connectWithDomain(Route::currentDomain());

        if (!$tenant_db) {
            throw new \Exception("Error de conexión con la base de datos.");
        }

        $sql_consecutivo = $tenant_db->prepare('SELECT MAX(CONVERT(?, INT)) AS consecutivo FROM ?');

        if (!$sql_consecutivo) {
            throw new \Exception("Error en la preparación de la consulta SELECT.");
        }

        $sql_consecutivo->bind_param('ss', $campo, $tabla);

        if (!$sql_consecutivo->execute()) {
            $sql_consecutivo->close();
            $tenant_db->close();
            return false;
        }

        $request = $sql_consecutivo->get_result();
        $consecutivo = $request["consecutivo"] + 1;
        $sql_consecutivo->close();
        $tenant_db->close();
        return $consecutivo;
    } */
    /**
     * Busca el siguiente número consecutivo para un campo específico en una tabla
     *
     * @param string $tabla Nombre de la tabla
     * @param string $campo Nombre del campo
     * @return int Siguiente número consecutivo
     * @throws \Exception Si hay errores de autenticación o base de datos
     */
    function searchConsecVps(string $tabla, string $campo): int
    {
        // Validar nombres de tabla y campo permitidos
        //$tablasPermitidas = ['tabla1', 'tabla2']; // Definir tablas permitidas
        //$camposPermitidos = ['campo1', 'campo2']; // Definir campos permitidos

        /* if (!in_array($tabla, $tablasPermitidas) || !in_array($campo, $camposPermitidos)) {
            throw new \Exception("Tabla o campo no válidos");
        } */

        $intIdUser = $_SESSION['idusuario'] ?? null;

        if (!$intIdUser) {
            throw new \Exception("Usuario no autenticado.");
        }

        $tenant_db = null;
        $sql_consecutivo = null;

        try {
            $tenant_db = DB::connectWithDomain(Route::currentDomain());

            if (!$tenant_db) {
                throw new \Exception("Error de conexión con la base de datos.");
            }

            // Usar consulta más segura con nombre de tabla y campo como identificadores
            $query = sprintf(
                'SELECT MAX(CONVERT(%s, INT)) AS consecutivo FROM %s',
                $tenant_db->real_escape_string($campo),
                $tenant_db->real_escape_string($tabla)
            );

            $sql_consecutivo = $tenant_db->prepare($query);

            if (!$sql_consecutivo) {
                throw new \Exception("Error en la preparación de la consulta SELECT.");
            }

            if (!$sql_consecutivo->execute()) {
                throw new \Exception("Error al ejecutar la consulta.");
            }

            $result = $sql_consecutivo->get_result();

            if (!$result) {
                throw new \Exception("Error al obtener el resultado.");
            }

            $row = $result->fetch_assoc();

            if (!$row) {
                return 1; // Si no hay registros, comenzar en 1
            }

            return (int)$row['consecutivo'] + 1;

        } finally {
            // Cerrar recursos
            if ($sql_consecutivo) {
                $sql_consecutivo->close();
            }
            if ($tenant_db) {
                $tenant_db->close();
            }
        }
    }
    function searchConsecVigencia(string $tabla, string $campo,string $campoVigencia) {
        $con = new Mysql();
        $vigencia = date_format(date_create("now"),"Y");
        $sql_consecutivo = "SELECT MAX(CONVERT($campo, INT)) AS consecutivo FROM $tabla WHERE $campoVigencia = $vigencia";
        $request = $con->select($sql_consecutivo);
        $consecutivo = $request["consecutivo"] + 1;
        return $consecutivo;
    }
    function getConsecutivos(string $tabla, string $campo,string $campoMov="",string $mov = ""){
        $mov= $campoMov !="" && $mov !="" ? " WHERE $campoMov = '$mov'" : "";
        $con = new Mysql();
        $sql = "SELECT $campo AS id FROM $tabla $mov";
        $request = $con->select_all($sql);
        sort($request);
        return $request;
    }
    function validRoutePermits(string $shortcut,int $idUser,bool $flag = false){
        $strShortcut = !$flag ? strtoupper(strClean(replaceChar($shortcut))) : strClean($shortcut);
        $condition = !$flag ? "comando = '$strShortcut'" : "ruta_opcion = '$strShortcut'";
        $con = new Mysql();
        $sql = "SELECT ruta_opcion, modulo FROM opciones WHERE $condition";
        $request = $con->select($sql);
        $route = $request['ruta_opcion'];

        $sqlRoutes = "SELECT op.ruta_opcion,rol.id_rol as rol
        FROM usuarios usu
        INNER JOIN rol_priv rol ON usu.id_rol = rol.id_rol
        INNER JOIN opciones op ON rol.id_opcion = op.id_opcion
        WHERE usu.id_usu = $idUser";
        $requestRoutes = $con->select_all($sqlRoutes);
        if(!empty($requestRoutes) && $route != ""){
            $arrRoutes = array_column($requestRoutes,"ruta_opcion");
            if(!in_array($route,$arrRoutes)){
                $route ="";
            }else{
                $intModule = $request['modulo'];
                $intRol = $requestRoutes[0]['rol'];
                $sqlOptions = "SELECT DISTINCT T2.nom_opcion,T2.ruta_opcion,T2.niv_opcion,T2.comando
                FROM rol_priv AS T1
                INNER JOIN opciones AS T2 ON T2.id_opcion = T1.id_opcion
                WHERE T1.id_rol = $intRol AND T2.modulo = $intModule
                GROUP BY (T2.nom_opcion),T2.ruta_opcion,T2.niv_opcion,T2.comando ORDER BY T2.orden;";
                $requestOptions = $con->select_all($sqlOptions);
                if(!empty($requestOptions)){
                    if($intModule == 0){
                        $strNameSession = "linksetad";
                    }else if($intModule == 1){
                        $strNameSession = "linksetco";
                    }else if($intModule == 2){
                        $strNameSession = "linksethu";
                    }else if($intModule == 3){
                        $strNameSession = "linksetpr";
                    }else if($intModule == 4){
                        $strNameSession = "linksette";
                    }else if($intModule == 5){
                        $strNameSession = "linksetin";
                    }else if($intModule == 6){
                        $strNameSession = "linksetac";
                    }else if($intModule == 7){
                        $strNameSession = "linkset";
                    }else if($intModule == 8){
                        $strNameSession = "linksetct";
                    }else if($intModule == 9){
                        $strNameSession = "linksetpl";
                    }else if($intModule == 10){
                        $strNameSession = "linksetser";
                    }else if($intModule == 11){
                        $strNameSession = "linksetccp";
                    }
                    unset($_SESSION[$strNameSession]);
                    foreach ($requestOptions as $e) {
                        $_SESSION[$strNameSession][$e['niv_opcion']] .='
                        <li>
                            <a onClick="location.href=\''.$e['ruta_opcion'].'\'" style="cursor:pointer;">
                                '.$e['nom_opcion'].'   <span style="float:right">'.$e['comando'].'</span>
                            </a>
                        </li>';
                    }
                }
            }
        }else{
            $route ="";
        }
        return $route;
    }
    function getRouteModules(){
        $con = new Mysql();
        $sql = "SELECT libre FROM modulos";
        $request = $con->select_all($sql);
        $request = array_column($request,"libre");
        array_push($request,"principal.php");
        return $request;
    }
    function getRouteOptions(){
        $con = new Mysql();
        $sql = "SELECT ruta_opcion FROM opciones";
        $request = $con->select_all($sql);
        $request = array_column($request,"ruta_opcion");
        return $request;
    }
    function getIngresosDet($code){
        $con = new Mysql();
        $sql="SELECT * FROM tesoingresos_det where codigo='$code'AND estado='S'";
        $request = $con->select_all($sql);
        return $request;
    }

    function getCentroCosto($code){
        $con = new Mysql();
        $sql= "SELECT id_cc FROM centrocostos_seccionpresupuestal WHERE id_sp = $code";
        $request = $con->select($sql);
        if(empty($request)){
            $request['id_cc'] = "01";
        }
        return $request;
    }
    function getConceptoDet($code,$module,$type,$center,$date="",$cuentaSector=""){
        $cuentaSector = $cuentaSector != "" ? "AND cuenta LIKE '$cuentaSector%'" : "";
        $date = $date == "" ? date("Y-m-d") : $date;
        $type = strtoupper($type);
        $con = new Mysql();
        $sql="(SELECT * FROM conceptoscontables_det
        WHERE estado='S' AND modulo='$module' AND codigo='$code' AND tipo='$type' AND cc='$center'
        AND fechainicial = fechainicial <= '$date'
        AND debito = 'S' $cuentaSector
        ORDER BY fechainicial DESC
        LIMIT 1)
        UNION ALL
        (SELECT * FROM conceptoscontables_det
        WHERE estado='S' AND modulo='$module' AND codigo='$code' AND tipo='$type' AND cc='$center'
        AND fechainicial = fechainicial <= '$date'
        AND credito = 'S' $cuentaSector
        ORDER BY fechainicial DESC
        LIMIT 1)
        ";
        $request = $con->select_all($sql);
        return $request;
    }
    function getCuentasBancos(){
        $con = new Mysql();
        $sql = "SELECT TB2.razonsocial as nombre,TB1.cuenta,TB1.ncuentaban as cuenta_banco,TB1.tipo, TB1.tercero
        FROM tesobancosctas TB1
        INNER JOIN terceros TB2
        ON TB1.tercero = TB2.cedulanit
        INNER JOIN cuentasnicsp TB3
        ON TB1.cuenta=TB3.cuenta
        WHERE TB1.estado='S'";
        $request = $con->select_all($sql);
        return $request;
    }
    function getMediosDePago(){
        $con = new Mysql();
        $sql = "SELECT nombre, cuentacontable as cuenta, tercero as cuenta_banco, id as medio_pago FROM tesomediodepago WHERE estado='S'";
        $request = $con->select_all($sql);
        return $request;
    }
    function getTesoParametros(){
        $con = new Mysql();
        $sql = "SELECT * FROM tesoparametros";
        $request = $con->select($sql);
        return $request;
    }
    function getSeccionPresupuestoCentro($id){
        $con = new Mysql();
        $sql = "SELECT id_sp FROM centrocostos_seccionpresupuestal WHERE id_cc = $id";
        $request = $con->select($sql);
        if(empty($request)){
            $request['id_sp'] = 16;
        }
        return $request;
    }
    function getMeses(){
        return array(
            "01"=>"Enero",
            "02"=> "Febrero",
            "03"=> "Marzo",
            "04"=>"Abril",
            "05"=>"Mayo",
            "06"=> "Junio",
            "07"=> "Julio",
            "08"=>"Agosto",
            "09"=>"Septiembre",
            "10"=>"Octubre",
            "11"=>"Noviembre",
            "12"=>"Diciembre"
        );
    }
    function getValorLetras($valor,$extra="",$upper=true){
        $f = new NumberFormatter("es",NumberFormatter::SPELLOUT);
        $result = $f->format($valor);
        $result = replaceChar($result);
        $result = $upper ? strtoupper($result." ".$extra) : $result;
        return $result;
    }
    function getDuracionTiempo($fechaInicial,$fechaFinal,$mostrarValor = false){
        $fechaInicio = new DateTime($fechaInicial);
        $fechaFinal = new DateTime($fechaFinal);
        $intervalo = $fechaInicio->diff($fechaFinal);
        $duracion = '';
        if ($intervalo->y > 0) {
            $duracion .= $intervalo->y . ' año' . ($intervalo->y > 1 ? 's ' : ' ');
        }
        if ($intervalo->m > 0) {
            $duracion .= $intervalo->m . ' mes' . ($intervalo->m > 1 ? 'es ' : ' ');
        }
        if ($intervalo->d > 0) {
            $duracion .= $intervalo->d . ' dia' . ($intervalo->d > 1 ? 's' : '');
        }
        if($mostrarValor){

            return array("y"=>$intervalo->y,"m"=>$intervalo->m,"d"=>$intervalo->d);
        }
        return $duracion;
    }
    function connectBase($base,$usuario){
        $connect = new Mysql($base,$usuario);
        return $connect;
    }
    function getBases(){
        $con = new Mysql();
        $sql = "SELECT codigo,base, usuario FROM redglobal";
        $request = $con->select_all($sql);
        return $request;
    }
    function getNombreCuentaNicsp($cuenta){
        $con = new Mysql();
        $sql = "SELECT nombre FROM cuentasnicsp WHERE cuenta='$cuenta'";
        $request = $con->select($sql);
        return $request;
    }
    function getNombresCuentaNicsp(){
        $con = new Mysql();
        $sql = "SELECT nombre,cuenta FROM cuentasnicsp";
        $request = $con->select_all($sql);
        return $request;
    }
    function getVigenciaUsuario($cedula) {
        $con = new Mysql();
        $sql = "SELECT dom.tipo AS vigencia FROM dominios dom WHERE dom.nombre_dominio = 'PERMISO_MODIFICA_DOC' AND dom.valor_inicial = '$cedula' ORDER BY dom.valor_final DESC";
        $request = $con->select($sql);
        return $request["vigencia"];
    }
    function getVersionGastosCcpet() {
        $con = new Mysql();
        $sql = "SELECT MAX(version) AS version FROM cuentasccpet";
        $request = $con->select($sql);
        return $request["version"];
    }
    function getVersionFuentesCcpet() {
        $con = new Mysql();
        $sql = "SELECT MAX(version) AS version FROM ccpet_fuentes_cuipo";
        $request = $con->select($sql);
        return $request["version"];
    }
    function search_id_bpin($bpin, $vigencia) {
        $con = new Mysql();
        $sql = "SELECT id FROM ccpproyectospresupuesto WHERE codigo = '$bpin' AND vigencia = '$vigencia'";
        $request = $con->select($sql);
        return $request["id"];
    }
    function saldoRubroGastoCcpet($parametros) {
        /* Parametros para esta función sera los siguientes:
        cuenta => cuentas ccpet
        fuente => fuente presupuestal
        tipo_presupuesto => 1.Funcionamiento, 2.Servicio a la deuda, 3.Inversión
        dependencia => antigua sección presupuestal,
        medio_pago => medio de pago CSF o SSF
        vig_gasto => vigencia del gasto
        bpin => bpin o codigo de proyecto
        indicador => indicador que se obtiene de sector, programa y producto
        vigencia => año de consulta
        area => area, nuevo parametro.
        */

        $con = new Mysql();
        $inicial = 0;
        $adicion = 0;
        $reduccion = 0;
        $credito = 0;
        $contracredito = 0;
        $saldoCdp = 0;
        $saldoFinal = 0;
        $bpinId = search_id_bpin($parametros["bpin"], $parametros["vigencia"]); /* se busca el id para realizar busqueda en tabla de solicitud de cdp */
        $critAreaInv = $parametros["area"] == "" ? "" : " AND TB2.area LIKE '%$parametros[area]%'";

        $critAreaFun = $parametros["area"] == "" ? "" : " AND area LIKE '%$parametros[area]%'";

        $critCpc = '';
        $critCpc_cdp = '';
        $critCpc_sol_cdp = '';

        /* $critCpc = $parametros["cpc"] == "" ? "" : " AND producto_servicio = '$parametros[cpc]'";
        $critCpc_cdp = $parametros["cpc"] == "" ? "" : " AND productoservicio = '$parametros[cpc]'";
        $critCpc_sol_cdp = $parametros["cpc"] == "" ? "" : " AND det.cpc = '$parametros[cpc]'"; */

        /* saldo inicial */
        if ($parametros["tipo_presupuesto"] == 3) {
            $sql_proyectos = "SELECT SUM(TB2.valorcsf) AS valorCSF, SUM(TB2.valorssf) AS valorSSF FROM ccpproyectospresupuesto AS TB1, ccpproyectospresupuesto_presupuesto AS TB2 WHERE TB1.codigo = '$parametros[bpin]' AND TB2.id_fuente = '$parametros[fuente]' AND TB2.indicador_producto = '$parametros[indicador]' AND TB1.id = TB2.codproyecto AND TB1.vigencia = '$parametros[vigencia]' AND TB2.vigencia_gasto = '$parametros[vig_gasto]' AND TB1.idunidadej = '$parametros[dependencia]' $critAreaInv";
            $data_proyectos = $con->select($sql_proyectos);

            if ($parametros['medio_pago'] == 'CSF') {
                $inicial += $data_proyectos["valorCSF"];
            } else {
                $inicial += $data_proyectos["valorSSF"];
            }

            /* saldo del cdp de inversion */
            $sql_cdp = "SELECT SUM(valor) AS valor_total_cdp FROM ccpetcdp_detalle WHERE bpim = '$parametros[bpin]' AND fuente = '$parametros[fuente]' AND indicador_producto = '$parametros[indicador]' AND vigencia = '$parametros[vigencia]' AND codigo_vigenciag = '$parametros[vig_gasto]' AND medio_pago = '$parametros[medio_pago]' AND tipo_mov = '201' $critAreaFun AND seccion_presupuestal = '$parametros[dependencia]'";

            $sql_cdp_rev = "SELECT SUM(valor) AS valor_total_cdp_rev FROM ccpetcdp_detalle WHERE bpim = '$parametros[bpin]' AND fuente = '$parametros[fuente]' AND indicador_producto = '$parametros[indicador]' AND vigencia = '$parametros[vigencia]' AND codigo_vigenciag = '$parametros[vig_gasto]' AND medio_pago = '$parametros[medio_pago]' AND tipo_mov LIKE '4%' $critAreaFun AND seccion_presupuestal = '$parametros[dependencia]'";

            $sql_solicitud_cdp = "SELECT SUM(valor) AS valor_total_solicitud FROM plan_solicitud_cdp_det AS det INNER JOIN plan_solicitud_cdp AS cab ON det.id_solicitud_cdp = cab.id WHERE det.id_bpin = $bpinId AND det.fuente = '$parametros[fuente]' AND det.indicador = '$parametros[indicador]' AND cab.vigencia = '$parametros[vigencia]' AND det.id_vig_gasto = '$parametros[vig_gasto]' AND det.medio_pago = '$parametros[medio_pago]' AND det.dependencia = '$parametros[dependencia]' AND cab.id_area_solicitante = '$parametros[area]' AND cab.estado = 'S'";

            $data_cdp = $con->select($sql_cdp);
            $data_cdp_rev = $con->select($sql_cdp_rev);
            $data_solicitud_cdp = $con->select($sql_solicitud_cdp);
            $saldoCdp = $data_cdp["valor_total_cdp"] - $data_cdp_rev["valor_total_cdp_rev"] + $data_solicitud_cdp["valor_total_solicitud"];
        }
        else {
            $sql_inicial = "SELECT SUM(valor) AS valor_total_inicial FROM ccpetinicialgastosfun WHERE cuenta = '$parametros[cuenta]' AND fuente = '$parametros[fuente]' AND vigencia = '$parametros[vigencia]' AND seccion_presupuestal = '$parametros[dependencia]' AND medio_pago = '$parametros[medio_pago]' AND vigencia_gasto = '$parametros[vig_gasto]' $critCpc $critAreaFun";
            $data_inicial = $con->select($sql_inicial);

            $inicial += $data_inicial["valor_total_inicial"];

            $sql_cdp = "SELECT SUM(valor) AS valor_total_cdp FROM ccpetcdp_detalle WHERE cuenta = '$parametros[cuenta]' AND fuente = '$parametros[fuente]' AND vigencia = '$parametros[vigencia]' AND seccion_presupuestal = '$parametros[dependencia]' AND medio_pago = '$parametros[medio_pago]' AND codigo_vigenciag = '$parametros[vig_gasto]' AND tipo_mov = '201' $critCpc_cdp $critAreaFun";

            $sql_cdp_rev = "SELECT SUM(valor) AS valor_total_cdp_rev FROM ccpetcdp_detalle WHERE cuenta = '$parametros[cuenta]' AND fuente = '$parametros[fuente]' AND vigencia = '$parametros[vigencia]' AND seccion_presupuestal = '$parametros[dependencia]' AND medio_pago = '$parametros[medio_pago]' AND codigo_vigenciag = '$parametros[vig_gasto]' $critCpc_cdp AND tipo_mov LIKE '4%' $critAreaFun";

            $sql_solicitud_cdp = "SELECT SUM(valor) AS valor_total_solicitud FROM plan_solicitud_cdp_det AS det INNER JOIN plan_solicitud_cdp AS cab ON det.id_solicitud_cdp = cab.id WHERE det.cuenta = '$parametros[cuenta]' AND det.fuente = '$parametros[fuente]' AND cab.vigencia = '$parametros[vigencia]' AND det.dependencia = '$parametros[dependencia]' AND det.medio_pago = '$parametros[medio_pago]' AND det.id_vig_gasto = '$parametros[vig_gasto]' AND cab.id_area_solicitante = '$parametros[area]' $critCpc_sol_cdp AND cab.estado = 'S'";
            $data_cdp = $con->select($sql_cdp);
            $data_cdp_rev = $con->select($sql_cdp_rev);
            $data_solicitud_cdp = $con->select($sql_solicitud_cdp);
            $saldoCdp = $data_cdp["valor_total_cdp"] - $data_cdp_rev["valor_total_cdp_rev"] + $data_solicitud_cdp["valor_total_solicitud"];
        }

        /* adición a presupuesto */
        $sql_adicion = "SELECT SUM(valor) AS valot_total_adicion FROM ccpet_adiciones WHERE cuenta = '$parametros[cuenta]' AND fuente = '$parametros[fuente]' AND vigencia = '$parametros[vigencia]' AND seccion_presupuestal = '$parametros[dependencia]' AND codigo_vigenciag = '$parametros[vig_gasto]' AND medio_pago = '$parametros[medio_pago]' AND programatico = '$parametros[indicador]' AND bpim = '$parametros[bpin]' AND clasificador = '$parametros[cpc]' $critAreaFun";
        $data_adicion = $con->select($sql_adicion);
        $adicion = $data_adicion["valot_total_adicion"];

        /* reduccion a presupuesto */
        $sql_reduccion = "SELECT SUM(valor) AS valor_total_reduccion FROM ccpet_reducciones WHERE cuenta = '$parametros[cuenta]' AND fuente = '$parametros[fuente]' AND vigencia = '$parametros[vigencia]' AND seccion_presupuestal = '$parametros[dependencia]' AND codigo_vigenciag = '$parametros[vig_gasto]' AND medio_pago = '$parametros[medio_pago]' AND programatico = '$parametros[indicador]' AND bpim = '$parametros[bpin]' $critCpc $critAreaFun";
        $data_reduccion = $con->select($sql_reduccion);
        $reduccion = $data_reduccion["valor_total_reduccion"];

        /* traslado credito */
        $sql_credito = "SELECT SUM(valor) AS valor_total_credito FROM ccpet_traslados WHERE cuenta = '$parametros[cuenta]' AND fuente = '$parametros[fuente]' AND vigencia = '$parametros[vigencia]' AND seccion_presupuestal = '$parametros[dependencia]' AND codigo_vigenciag = '$parametros[vig_gasto]' AND medio_pago = '$parametros[medio_pago]' AND programatico = '$parametros[indicador]' AND bpim = '$parametros[bpin]' AND tipo = 'C' $critCpc $critAreaFun";
        $data_credito = $con->select($sql_credito);
        $credito = $data_credito["valor_total_credito"];

        /* traslado contracredito */
        $sql_contracredito = "SELECT SUM(valor) AS valor_total_contracredito FROM ccpet_traslados WHERE cuenta = '$parametros[cuenta]' AND fuente = '$parametros[fuente]' AND vigencia = '$parametros[vigencia]' AND seccion_presupuestal = '$parametros[dependencia]' AND codigo_vigenciag = '$parametros[vig_gasto]' AND medio_pago = '$parametros[medio_pago]' AND programatico = '$parametros[indicador]' AND bpim = '$parametros[bpin]' AND tipo = 'R' $critCpc $critAreaFun";
        $data_contracredito = $con->select($sql_contracredito);
        $contracredito = $data_contracredito["valor_total_contracredito"];

        /* Calculo del saldo final sumando y restando la progrmacion presupuestal menos saldo del cdp. */
        $saldoFinal = round($inicial, 2) +  round($adicion, 2) - round($reduccion, 2) + round($credito, 2) - round($contracredito, 2) - round($saldoCdp, 2);
        return $saldoFinal;
    }

    function getFechaBlock (string $cedula) {
        $con = new Mysql();
        $sql_bloqueo = "SELECT valor_final AS fecha_bloqueo FROM dominios WHERE nombre_dominio = 'PERMISO_MODIFICA_DOC' AND valor_inicial = '$cedula'";
        $request = $con->select($sql_bloqueo);
        return $request["fecha_bloqueo"];
    }

    function selectNotas_all($cuenta, $fechai, $fechaf){
        $con = new Mysql();
        $sql = "SELECT fecha, num_comprobante, nom_comprobante, nota, tiponota, debito, credito, fechaaux FROM conciliacion_registros WHERE cuenta = '$cuenta' AND fecha BETWEEN '$fechai' AND '$fechaf' ORDER BY tiponota DESC";
        $request = $con->select_all($sql);
        return $request;
    }
?>
