<?php
require_once BASE_URL.'/Librerias/core/Helpers.php';
class Conexion{
	private $conect;

	public function __construct($base,$user){
        $arrData = baseDatos();
        $strBase = $base=="" ? $arrData['base'] : $base;
        $strUserName = $base=="" ? $arrData['user'] : $user;
        $strHostName = $arrData['host'];
        $strPassword = $arrData['password'];
		$connectionString = "mysql:host=". $strHostName.";port=3306;dbname=". $strBase.";charset=utf8";
		$options = array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET lc_time_names='es_CO'");
		try{
			$this->conect = new PDO($connectionString,  $strUserName,  $strPassword,$options);
			$this->conect->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		}catch(PDOException $e){
			$this->conect = 'Error de conexión';
		    echo "ERROR: " . $e->getMessage();
		}
	}

	public function conect(){
		return $this->conect;
	}
}

?>
