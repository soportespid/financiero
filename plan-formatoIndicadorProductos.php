<?php

	header("Content-Type: text/html;charset=utf-8");
    require_once 'PHPExcel/Classes/PHPExcel.php';
    require "comun.inc";
    require "funciones.inc";
    require 'funcionesSP.inc.php';
    /*ini_set('display_errors', '1');
    ini_set('display_startup_errors', '1');
    error_reporting(E_ALL);*/

    session_start();

    class Plantilla{
        private $linkbd;
        public function __construct() {
            $this->linkbd = conectar_v7();
            $this->linkbd->set_charset("utf8");
        }
        public function indicadoresProducto() {
            $sql = "SELECT codigo_indicador, indicador_producto AS nombre_indicador FROM ccpetproductos ORDER BY codigo_indicador";
            $data = mysqli_fetch_all(mysqli_query($this->linkbd,$sql),MYSQLI_ASSOC);
            return $data;
        }

        public function indicadoresResultado() {
            $sql = "SELECT PIR.id, PCIR.nombre_indicador FROM plan_indicador_resultado AS PIR INNER JOIN plan_catalogo_indicadores_resultado AS PCIR ON PIR.codigo_resultado_id = PCIR.id ORDER BY PIR.id";
            $data = mysqli_fetch_all(mysqli_query($this->linkbd,$sql),MYSQLI_ASSOC);
            return $data;
        }

        public function ejesEstrategicos() {
            $sql = "SELECT id, nombre FROM plan_eje_estrategico WHERE estado = 'S' ORDER BY id";
            $data = mysqli_fetch_all(mysqli_query($this->linkbd,$sql),MYSQLI_ASSOC);
            return $data;
        }

        public function funcionarios() {
            $sql = "SELECT
            PT.cedulanit AS documento,
            CONCAT(T.nombre1, ' ', T.nombre2, ' ', T.apellido1, ' ', T.apellido2) AS nombre,
            UPPER(PC.nombrecargo) AS nombre_cargo
            FROM planestructura_terceros AS PT
            INNER JOIN planaccargos AS PC
            ON PT.codcargo = PC.codcargo
            INNER JOIN terceros AS T
            ON PT.cedulanit = T.cedulanit
            WHERE PT.estado = 'S'";
            $data = mysqli_fetch_all(mysqli_query($this->linkbd,$sql),MYSQLI_ASSOC);
            foreach ($data as $key => $d) {
                $data[$key]["nombre"] = preg_replace("/\s+/", " ", trim($d["nombre"]));
            }
            return $data;
        }
    }

    $obj = new Plantilla();
    $indicadoresProducto = $obj->indicadoresProducto();
    $indicadoresResultado = $obj->indicadoresResultado();
    $ejesEstrategicos = $obj->ejesEstrategicos();
    $funcionarios = $obj->funcionarios();
    $objPHPExcel = new PHPExcel();

    $objPHPExcel->createSheet(1);
    $objPHPExcel->createSheet(2);
    $objPHPExcel->createSheet(3);
    $objPHPExcel->createSheet(4);
    $objPHPExcel->getSheet(0)->getStyle('A:H')->applyFromArray(
        array(
            'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
                'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER
            )
        )
    );
    $objPHPExcel->getSheet(0)->setTitle("Formato");
    $objPHPExcel->getSheet(1)->setTitle("Indicadores producto");
    $objPHPExcel->getSheet(2)->setTitle("Lineas estrategicas");
    $objPHPExcel->getSheet(3)->setTitle("Indicadores resultado");
    $objPHPExcel->getSheet(4)->setTitle("Funcionarios");
    $objPHPExcel->getProperties()
    ->setCreator("IDEAL 10")
    ->setLastModifiedBy("IDEAL 10")
    ->setTitle("Exportar Excel con PHP")
    ->setSubject("Documento de prueba")
    ->setDescription("Documento generado con PHPExcel")
    ->setKeywords("usuarios phpexcel")
    ->setCategory("reportes");

    //----Cuerpo de Documento----
    $objPHPExcel->getSheet(0)
    ->mergeCells('A1:H1')
    ->mergeCells('A2:H2')
    ->setCellValue('A1', 'PLANEACIÓN ESTRATEGICA')
    ->setCellValue('A2', 'PLANTILLA INDICADORES DE PRODUCTO');
    $objPHPExcel->getSheet(0)
    -> getStyle ("A1")
    -> getFill ()
    -> setFillType (PHPExcel_Style_Fill :: FILL_SOLID)
    -> getStartColor ()
    -> setRGB ('C8C8C8');
    $objPHPExcel->getSheet(0)
    -> getStyle ("A1:H2")
    -> getFont ()
    -> setBold ( true )
    -> setName ( 'Verdana' )
    -> setSize ( 10 )
    -> getColor ()
    -> setRGB ('000000');
    $objPHPExcel->getSheet(0)
    -> getStyle ('A1:H2')
    -> getAlignment ()
    -> applyFromArray (array ( 'horizontal'  =>  PHPExcel_Style_Alignment :: HORIZONTAL_CENTER ,) );
    $objPHPExcel->getSheet(0)
    -> getStyle ('A3:H3')
    -> getAlignment ()
    -> applyFromArray (array ( 'horizontal'  =>  PHPExcel_Style_Alignment :: VERTICAL_JUSTIFY ,) );
    $objPHPExcel->getSheet(0)
    -> getStyle ("A2")
    -> getFill ()
    -> setFillType (PHPExcel_Style_Fill :: FILL_SOLID);

    $borders = array(
        'borders' => array(
            'allborders' => array(
                'style' => PHPExcel_Style_Border::BORDER_THIN,
            'color' => array('argb' => 'FF000000'),
            )
        ),
    );
    $objPHPExcel->getSheet(0)
    ->setCellValue('A3', "Nombre")
    ->setCellValue('B3', "Código indicador producto")
    ->setCellValue('C3', "Lineas estrategicas (separados por coma)")
    ->setCellValue('D3', "Indicadores resultado")
    ->setCellValue('E3', "Valor a cuatrienio")
    ->setCellValue('F3', "Meta cuatrienio")
    ->setCellValue('G3', "Linea base")
    ->setCellValue('H3', "Documento funcionario");
    $objPHPExcel->getSheet(0)
        -> getStyle ("A3:H3")
        -> getFill ()
        -> setFillType (PHPExcel_Style_Fill :: FILL_SOLID)
        -> getStartColor ()
        -> setRGB ('99ddff');
    $objPHPExcel->getSheet(0)->getStyle("A3:H3")->getFont()->setBold(true);
    $objPHPExcel->getSheet(0)->getStyle('A1:H1')->applyFromArray($borders);
    $objPHPExcel->getSheet(0)->getStyle('A2:H2')->applyFromArray($borders);
    $objPHPExcel->getSheet(0)->getStyle('A3:H3')->applyFromArray($borders);

    $objPHPExcel->getSheet(0)->getColumnDimension('A')->setAutoSize(true);
    $objPHPExcel->getSheet(0)->getColumnDimension('B')->setAutoSize(true);
    $objPHPExcel->getSheet(0)->getColumnDimension('C')->setAutoSize(true);
    $objPHPExcel->getSheet(0)->getColumnDimension('D')->setAutoSize(true);
    $objPHPExcel->getSheet(0)->getColumnDimension('E')->setAutoSize(true);
    $objPHPExcel->getSheet(0)->getColumnDimension('F')->setAutoSize(true);
    $objPHPExcel->getSheet(0)->getColumnDimension('G')->setAutoSize(true);
    $objPHPExcel->getSheet(0)->getColumnDimension('H')->setAutoSize(true);

    //Indicadores producto

    $objPHPExcel->getSheet(1)->getStyle('A:B')->applyFromArray(
        array(
            'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
                'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER
            )
        )
    );

    $objPHPExcel->getSheet(1)
    ->mergeCells('A1:B1')
    ->mergeCells('A2:B2')
    ->setCellValue('A1', 'PLANEACIÓN ESTRATEGICA')
    ->setCellValue('A2', 'FORMATO DE INDICADORES PRODUCTO');
    $objPHPExcel->getSheet(1)
    -> getStyle ("A1")
    -> getFill ()
    -> setFillType (PHPExcel_Style_Fill :: FILL_SOLID)
    -> getStartColor ()
    -> setRGB ('C8C8C8');
    $objPHPExcel->getSheet(1)
    -> getStyle ("A1:A2")
    -> getFont ()
    -> setBold ( true )
    -> setName ( 'Verdana' )
    -> setSize ( 10 )
    -> getColor ()
    -> setRGB ('000000');
    $objPHPExcel->getSheet(1)
    -> getStyle ('A1:A2')
    -> getAlignment ()
    -> applyFromArray (array ( 'horizontal'  =>  PHPExcel_Style_Alignment :: HORIZONTAL_CENTER ,) );
    $objPHPExcel->getSheet(1)
    -> getStyle ('A3:B3')
    -> getAlignment ()
    -> applyFromArray (array ( 'horizontal'  =>  PHPExcel_Style_Alignment :: VERTICAL_JUSTIFY ,) );
    $objPHPExcel->getSheet(1)
    -> getStyle ("A2")
    -> getFill ()
    -> setFillType (PHPExcel_Style_Fill :: FILL_SOLID);

    $borders = array(
        'borders' => array(
            'allborders' => array(
                'style' => PHPExcel_Style_Border::BORDER_THIN,
            'color' => array('argb' => 'FF000000'),
            )
        ),
    );
    $objPHPExcel->getSheet(1)
    ->setCellValue('A3', 'Código')
    ->setCellValue('B3', "Nombre");
    $objPHPExcel->getSheet(1)
        -> getStyle ("A3:B3")
        -> getFill ()
        -> setFillType (PHPExcel_Style_Fill :: FILL_SOLID)
        -> getStartColor ()
        -> setRGB ('99ddff');
    $objPHPExcel->getSheet(1)->getStyle("A3:B3")->getFont()->setBold(true);
    $objPHPExcel->getSheet(1)->getStyle('A1:B1')->applyFromArray($borders);
    $objPHPExcel->getSheet(1)->getStyle('A2:B2')->applyFromArray($borders);
    $objPHPExcel->getSheet(1)->getStyle('A3:B3')->applyFromArray($borders);

    $objWorksheet = $objPHPExcel->getSheet(1);

    $objPHPExcel->getSheet(1)->getColumnDimension('A')->setAutoSize(true);
    $objPHPExcel->getSheet(1)->getColumnDimension('B')->setWidth(80);

    //Lineas estrategicas
    $objPHPExcel->getSheet(2)
    ->mergeCells('A1:B1')
    ->mergeCells('A2:B2')
    ->setCellValue('A1', 'CONTABILIDAD')
    ->setCellValue('A2', 'FORMATO LINEAS ESTRATEGICAS');
    $objPHPExcel-> getSheet(2)
    -> getStyle ("A1")
    -> getFill ()
    -> setFillType (PHPExcel_Style_Fill :: FILL_SOLID)
    -> getStartColor ()
    -> setRGB ('C8C8C8');
    $objPHPExcel-> getSheet(2)
    -> getStyle ("A1:B2")
    -> getFont ()
    -> setBold ( true )
    -> setName ( 'Verdana' )
    -> setSize ( 10 )
    -> getColor ()
    -> setRGB ('000000');
    $objPHPExcel-> getSheet(2)
    -> getStyle ('A1:B2')
    -> getAlignment ()
    -> applyFromArray (array ( 'horizontal'  =>  PHPExcel_Style_Alignment :: HORIZONTAL_CENTER ,) );
    $objPHPExcel-> getSheet(2)
    -> getStyle ('A3:B2')
    -> getAlignment ()
    -> applyFromArray (array ( 'horizontal'  =>  PHPExcel_Style_Alignment :: VERTICAL_JUSTIFY ,) );
    $objPHPExcel-> getSheet(2)
    -> getStyle ("A2")
    -> getFill ()
    -> setFillType (PHPExcel_Style_Fill :: FILL_SOLID);

    $borders = array(
        'borders' => array(
            'allborders' => array(
                'style' => PHPExcel_Style_Border::BORDER_THIN,
            'color' => array('argb' => 'FF000000'),
            )
        ),
    );
    $objPHPExcel->getSheet(2)
    ->setCellValue('A3', 'Código')
    ->setCellValue('B3', "Nombre");
    $objPHPExcel-> getSheet(2)
        -> getStyle ("A3:B3")
        -> getFill ()
        -> setFillType (PHPExcel_Style_Fill :: FILL_SOLID)
        -> getStartColor ()
        -> setRGB ('99ddff');
    $objPHPExcel->getSheet(2)->getStyle("A3:B3")->getFont()->setBold(true);
    $objPHPExcel->getSheet(2)->getStyle('A1:B1')->applyFromArray($borders);
    $objPHPExcel->getSheet(2)->getStyle('A2:B2')->applyFromArray($borders);
    $objPHPExcel->getSheet(2)->getStyle('A3:B3')->applyFromArray($borders);


    $objPHPExcel->getSheet(2)->getColumnDimension('A')->setAutoSize(true);
    $objPHPExcel->getSheet(2)->getColumnDimension('B')->setAutoSize(true);

    //Indicadores de resultado
    $objPHPExcel->getSheet(3)
    ->mergeCells('A1:B1')
    ->mergeCells('A2:B2')
    ->setCellValue('A1', 'CONTABILIDAD')
    ->setCellValue('A2', 'FORMATO INDICADORES DE RESULTADO');
    $objPHPExcel-> getSheet(3)
    -> getStyle ("A1")
    -> getFill ()
    -> setFillType (PHPExcel_Style_Fill :: FILL_SOLID)
    -> getStartColor ()
    -> setRGB ('C8C8C8');
    $objPHPExcel-> getSheet(3)
    -> getStyle ("A1:B2")
    -> getFont ()
    -> setBold ( true )
    -> setName ( 'Verdana' )
    -> setSize ( 10 )
    -> getColor ()
    -> setRGB ('000000');
    $objPHPExcel-> getSheet(3)
    -> getStyle ('A1:B2')
    -> getAlignment ()
    -> applyFromArray (array ( 'horizontal'  =>  PHPExcel_Style_Alignment :: HORIZONTAL_CENTER ,) );
    $objPHPExcel-> getSheet(3)
    -> getStyle ('A3:B2')
    -> getAlignment ()
    -> applyFromArray (array ( 'horizontal'  =>  PHPExcel_Style_Alignment :: VERTICAL_JUSTIFY ,) );
    $objPHPExcel-> getSheet(3)
    -> getStyle ("A2")
    -> getFill ()
    -> setFillType (PHPExcel_Style_Fill :: FILL_SOLID);

    $borders = array(
        'borders' => array(
            'allborders' => array(
                'style' => PHPExcel_Style_Border::BORDER_THIN,
            'color' => array('argb' => 'FF000000'),
            )
        ),
    );
    $objPHPExcel->getSheet(3)
    ->setCellValue('A3', 'Código')
    ->setCellValue('B3', "Nombre");
    $objPHPExcel-> getSheet(3)
        -> getStyle ("A3:B3")
        -> getFill ()
        -> setFillType (PHPExcel_Style_Fill :: FILL_SOLID)
        -> getStartColor ()
        -> setRGB ('99ddff');
    $objPHPExcel->getSheet(3)->getStyle("A3:B3")->getFont()->setBold(true);
    $objPHPExcel->getSheet(3)->getStyle('A1:B1')->applyFromArray($borders);
    $objPHPExcel->getSheet(3)->getStyle('A2:B2')->applyFromArray($borders);
    $objPHPExcel->getSheet(3)->getStyle('A3:B3')->applyFromArray($borders);


    $objPHPExcel->getSheet(3)->getColumnDimension('A')->setAutoSize(true);
    $objPHPExcel->getSheet(3)->getColumnDimension('B')->setAutoSize(true);

    //Funcionarios
    $objPHPExcel->getSheet(4)
    ->mergeCells('A1:C1')
    ->mergeCells('A2:C2')
    ->setCellValue('A1', 'CONTABILIDAD')
    ->setCellValue('A2', 'LISTADO DE FUNCIONARIOS');
    $objPHPExcel-> getSheet(4)
    -> getStyle ("A1")
    -> getFill ()
    -> setFillType (PHPExcel_Style_Fill :: FILL_SOLID)
    -> getStartColor ()
    -> setRGB ('C8C8C8');
    $objPHPExcel-> getSheet(4)
    -> getStyle ("A1:C2")
    -> getFont ()
    -> setBold ( true )
    -> setName ( 'Verdana' )
    -> setSize ( 10 )
    -> getColor ()
    -> setRGB ('000000');
    $objPHPExcel-> getSheet(4)
    -> getStyle ('A1:C2')
    -> getAlignment ()
    -> applyFromArray (array ( 'horizontal'  =>  PHPExcel_Style_Alignment :: HORIZONTAL_CENTER ,) );
    $objPHPExcel-> getSheet(4)
    -> getStyle ('A3:C2')
    -> getAlignment ()
    -> applyFromArray (array ( 'horizontal'  =>  PHPExcel_Style_Alignment :: VERTICAL_JUSTIFY ,) );
    $objPHPExcel-> getSheet(4)
    -> getStyle ("A2")
    -> getFill ()
    -> setFillType (PHPExcel_Style_Fill :: FILL_SOLID);

    $borders = array(
        'borders' => array(
            'allborders' => array(
                'style' => PHPExcel_Style_Border::BORDER_THIN,
            'color' => array('argb' => 'FF000000'),
            )
        ),
    );
    $objPHPExcel->getSheet(4)
    ->setCellValue('A3', 'Documento')
    ->setCellValue('B3', "Nombre")
    ->setCellValue('C3', "Cargo");
    $objPHPExcel-> getSheet(4)
        -> getStyle ("A3:C3")
        -> getFill ()
        -> setFillType (PHPExcel_Style_Fill :: FILL_SOLID)
        -> getStartColor ()
        -> setRGB ('99ddff');
    $objPHPExcel->getSheet(4)->getStyle("A3:C3")->getFont()->setBold(true);
    $objPHPExcel->getSheet(4)->getStyle('A1:C1')->applyFromArray($borders);
    $objPHPExcel->getSheet(4)->getStyle('A2:C2')->applyFromArray($borders);
    $objPHPExcel->getSheet(4)->getStyle('A3:C3')->applyFromArray($borders);


    $objPHPExcel->getSheet(4)->getColumnDimension('A')->setAutoSize(true);
    $objPHPExcel->getSheet(4)->getColumnDimension('B')->setAutoSize(true);
    $objPHPExcel->getSheet(4)->getColumnDimension('C')->setAutoSize(true);


    //data indicadores producto
    if(!empty($indicadoresProducto)){
        $total = count($indicadoresProducto);
        $row = 4;
        for ($i=0; $i < $total ; $i++) {
            $objPHPExcel->getSheet(1)
            ->setCellValue("A$row", $indicadoresProducto[$i]['codigo_indicador'])
            ->setCellValue("B$row", $indicadoresProducto[$i]['nombre_indicador']);
            $row++;
        }
    }
    
    //Lineas estrategicas
    if(!empty($ejesEstrategicos)){
        $total = count($ejesEstrategicos);
        $row = 4;
        for ($i=0; $i < $total ; $i++) {
            $objPHPExcel->getSheet(2)
            ->setCellValue("A$row", "LE".$ejesEstrategicos[$i]['id'])
            ->setCellValue("B$row", $ejesEstrategicos[$i]['nombre']);
            $row++;
        }
    }

    //Indicadores resultado
    if(!empty($indicadoresResultado)){
        $total = count($indicadoresResultado);
        $row = 4;
        for ($i=0; $i < $total ; $i++) {
            $objPHPExcel->getSheet(3)
            ->setCellValue("A$row", "IR".$indicadoresResultado[$i]['id'])
            ->setCellValue("B$row", $indicadoresResultado[$i]['nombre_indicador']);
            $row++;
        }
    }

    if(!empty($funcionarios)){
        $total = count($funcionarios);
        $row = 4;
        for ($i=0; $i < $total ; $i++) {
            $objPHPExcel->getSheet(4)
            ->setCellValue("A$row", $funcionarios[$i]['documento'])
            ->setCellValue("B$row", $funcionarios[$i]['nombre'])
            ->setCellValue("C$row", $funcionarios[$i]['nombre_cargo']);
            $row++;
        }
    }
  
    $objPHPExcel->setActiveSheetIndex(0);
    //----Guardar documento----
    header('Content-Type: application/vnd.ms-excel');
    header('Content-Disposition: attachment;filename="formato_cargue_masivo_ind_producto.xlsx"');
    header('Cache-Control: max-age=0');
    $objWriter=PHPExcel_IOFactory::createWriter($objPHPExcel,'Excel2007');
    $objWriter->save('php://output');
    die();

?>
