<?php
	require "comun.inc";
	require "funciones.inc";
	require "conversor.php";
	session_start();
	$linkbd=conectar_v7();
	cargarcodigopag($_GET['codpag'],$_SESSION["nivel"]);
	header("Cache-control: private"); // Arregla IE 6
	date_default_timezone_set("America/Bogota");
?>
<!DOCTYPE >
<html lang="es">
	<head>
		<meta charset="utf-8"/>
		<meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1"/>
		<meta name="viewport" content="user-scalable=no">
		<title>:: IDEAL 10 - Tesoreria</title>
		<link href="favicon.ico" rel="shortcut icon"/>
		<script>
			//************* ver reporte *************
			//***************************************
			function verep(idfac)
			{
				document.form1.oculto.value=idfac;
				document.form1.submit();
			}
		</script>
		<script>
			//************* genera reporte ************
			//***************************************
			function genrep(idfac)
			{
				document.form2.oculto.value=idfac;
				document.form2.submit();
			}
		</script>
		<script>
			function generar()
			{
				document.form2.oculto.value='1';
				document.form2.submit();
			}
		</script>
		<script language="JavaScript1.2">
			function validar()
			{
				document.form2.submit();
			}
		</script>
		<script>
			function buscarp(e)
			{
				if (document.form2.rp.value!="")
				{
					document.form2.brp.value='1';
					document.form2.submit();
				}
			}
		</script>
		<script>
			function agregardetalle()
			{
				if(document.form2.numero.value!="" &&  document.form2.valor.value>0 &&  document.form2.banco.value!=""  )
				{
					document.form2.agregadet.value=1;
					//document.form2.chacuerdo.value=2;
					document.form2.submit();
				}
				else
				{
					alert("Falta informacion para poder Agregar");
				}
			}
		</script>
		<script>
			function agregardetalled()
			{
				if(document.form2.retencion.value!="" &&  document.form2.vporcentaje.value!=""  )
				{
					document.form2.agregadetdes.value=1;
					//document.form2.chacuerdo.value=2;
					document.form2.submit();
				}
				else {
				alert("Falta informacion para poder Agregar");
				}
			}
			</script>
		<script>
			function eliminar(variable)
			{
				document.form2.elimina.value=variable;
				//eli=document.getElementById(elimina);
				vvend=document.getElementById('elimina');
				//eli.value=elimina;
				vvend.value=variable;
				document.form2.submit();
			}
		</script>
		<script>
			function eliminard(variable)
			{
				document.form2.eliminad.value=variable;
				//eli=document.getElementById(elimina);
				vvend=document.getElementById('eliminad');
				//eli.value=elimina;
				vvend.value=variable;
				document.form2.submit();
			}
		</script>
		<script>
			//************* genera reporte ************
			//***************************************
			function guardar()
			{
				if (document.form2.fecha.value!='')
				{
					if (confirm("Esta Seguro de Guardar"))
					{
						document.form2.oculto.value=2;
						document.form2.submit();
					}
				}
				else{
					alert('Faltan datos para completar el registro');
					document.form2.fecha.focus();
					document.form2.fecha.select();
				}
			}
		</script>
		<script>
			function calcularpago()
			{
				//alert("dddadadad");
				valorp=document.form2.valor.value;
				descuentos=document.form2.totaldes.value;
				valorc=valorp-descuentos;
				document.form2.valorcheque.value=valorc;
				document.form2.valoregreso.value=valorp;
				document.form2.valorretencion.value=descuentos;
			}
		</script>
		<script>
			function agregardetalled()
			{
				if(document.form2.retencion.value!="" )
				{
					var tipoRetencion = document.form2.tipoRetencion.value;
					if(tipoRetencion == 'C')
					{
						var tipoRete = document.form2.tiporet.value;
						if(tipoRete != '')
						{
							document.form2.agregadetdes.value=1;
							//document.form2.chacuerdo.value=2;
							document.form2.submit();
						}
						else
						{
							alert("Falta seleccionar el tipo de retencion");
						}
					}
					else
					{
						document.form2.agregadetdes.value=1;
						//document.form2.chacuerdo.value=2;
						document.form2.submit();
					}
				}
				else
				{
					alert("Seleccione una retencion");
				}
			}
		</script>
		<script>
			function eliminard(variable)
			{
				if (confirm("Esta Seguro de Eliminar"))
				{
					document.form2.eliminad.value=variable;
					//eli=document.getElementById(elimina);
					vvend=document.getElementById('eliminad');
					//eli.value=elimina;
					vvend.value=variable;
					document.form2.submit();
				}
			}
		</script>
		<script>
			function pdf()
			{
				document.form2.action="pdfpagotercerosreportefechas.php";
				document.form2.target="_BLANK";
				document.form2.submit();
				document.form2.action="";
				document.form2.target="";
			}
		</script>
		<script>
			function buscater(e)
			{
				if (document.form2.tercero.value!="")
				{
					document.form2.bt.value='1';
					document.form2.submit();
				}
			}
		</script>
		<script>
			function excell()
			{
				document.form2.action="teso-pagotercerosbancosexcel.php";
				document.form2.target="_BLANK";
				document.form2.submit();
				document.form2.action="";
				document.form2.target="";
			}
		</script>
		<script src="css/calendario.js"></script>
		<script src="css/programas.js"></script>
		<link href="css/css2.css" rel="stylesheet" type="text/css" />
		<link href="css/css3.css" rel="stylesheet" type="text/css" />
		<link href="css/tabs.css" rel="stylesheet" type="text/css" />
	</head>
	<body>
		<IFRAME src="alertas.php" name="alertas" id="alertas" style="display:none"></IFRAME>
		<span id="todastablas2"></span>
		<table>
			<tr><script>barra_imagenes("teso");</script><?php cuadro_titulos();?></tr>
			<tr><?php menu_desplegable("teso");?></tr>
			<tr>
				<td colspan="3" class="cinta">
					<a href="teso-pagotercerosbancosfechas.php" class="mgbt"><img src="imagenes/add.png" title="Nuevo"/></a>
					<a href="#" class="mgbt" ><img src="imagenes/guardad.png"/></a>
					<a class="mgbt"><img src="imagenes/buscad.png"  alt="Buscar" border="0" /></a>
					<a onClick="mypop=window.open('plan-agenda.php','','');mypop.focus()" class="mgbt"><img src="imagenes/agenda1.png" title="Agenda" /></a>
					<a href="#" class="mgbt" onClick="mypop=window.open('teso-principal.php','','');mypop.focus();"><img src="imagenes/nv.png" title="Nueva Ventana"></a>
					<a href="#" class="mgbt" onClick="pdf()"><img src="imagenes/print.png"  title="PDF" /></a>
					<a href="#" class="mgbt" onClick="excell()"><img src="imagenes/excel.png"  title="Excel"></a>
					<a href="teso-declaracionretenciones.php" class="mgbt"><img src="imagenes/iratras.png" title="Atr&aacute;s"></a>
				</td>
			</tr>
		</table>
			<tr><td colspan="3" class="tablaprin" align="center">
			<?php
			$vigusu=vigencia_usuarios($_SESSION['cedulausu']);
			$_POST['vigencia']=$vigusu;
			$vigencia=$vigusu;
			$vact=$vigusu;
			//*********** cuenta origen va al credito y la destino al debito
			if(!$_POST['oculto'])
			{
				$sqlr="select *from cuentapagar where estado='S' ";
				$res=mysqli_query($linkbd,$sqlr);
				while ($row =mysqli_fetch_row($res))
				{
					$_POST['cuentapagar']=$row[1];
				}
				$check1="checked";
				$fec=date("d/m/Y");
				$_POST['fecha']=$fec;
				//$_POST[valor]=0;
				//$_POST[valorcheque]=0;
				//$_POST[valorretencion]=0;
				//$_POST[valoregreso]=0;
				//$_POST[totaldes]=0;
				$_POST['vigencia']=$vigusu;
				$sqlr="select max(id_pago) from tesopagoterceros";
				$res=mysqli_query($linkbd,$sqlr);
				$consec=0;
				$r=mysqli_fetch_row($res);
				$consec=$r[0];
				$consec+=1;
				$_POST['idcomp']=$consec;
			}
			switch($_POST['tabgroup1'])
			{
				case 1:
					$check1='checked';
				break;
				case 2:
					$check2='checked';
				break;
				case 3:
					$check3='checked';
			}
			?>
 			<form name="form2" method="post" action="">
				<?php
				$meses=array('','Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre');
				if($_POST['bt']=='1')
				{
					$nresul=buscatercero($_POST['tercero']);
					if($nresul!='')
					{
						$_POST['ntercero']=$nresul;
					}
					else
					{
						$_POST['ntercero']="";
					}
				}
 				?>
	   			<table class="inicio" align="center" >
	   				<tr>
	     				<td colspan="8" class="titulos">Declaraci&oacute;n Retenciones - Bancos - Por fechas</td>
                        <td width="5%" class="cerrar" ><a href="teso-principal.php">Cerrar</a></td>
					</tr>
      				<tr>
	  					<td  class="saludo1" style="width:10%">Fecha: </td>
        				<td style="width:15%">
                            <input name="fecha" type="text" value="<?php echo $_POST['fecha']?>" maxlength="10" style="width:80%" onKeyPress="javascript:return solonumeros(event)" onKeyUp="return tabular(event,this)"  id="fc_1198971545" onKeyDown="mascara(this,'/',patron,true)" title="DD/MM/YYYY">   <a href="#" onClick="displayCalendarFor('fc_1198971545');"><img src="imagenes/calendario04.png" style="width:20px" align="absmiddle" border="0"></a>
                        </td>
	  					<td class="saludo1" style="width:10%">Vigencia: </td>
        				<td style="width:10%">
                            <input name="vigencia" type="text" value="<?php echo $_POST['vigencia']?>" style="width:100%" onKeyPress="javascript:return solonumeros(event)" onKeyUp="return tabular(event,this)" readonly>
                        </td>
                        <td  class="saludo1" style="width:10%;">Fecha Inicial: </td>
                        <td class="saludo1" style="width:10%;">
                            <input name="fechaini"  type="text" value="<?php echo $_POST['fechaini']?>" maxlength="10" onchange="" onKeyPress="javascript:return solonumeros(event)" onKeyUp="return tabular(event,this)"  id="fc_1198971546" onKeyDown="mascara(this,'/',patron,true)" style="width:80%;" title="DD/MM/YYYY"/>&nbsp;<a href="#" onClick="displayCalendarFor('fc_1198971546');" title="Calendario"><img src="imagenes/calendario04.png" style="width:20px;"/></a>
                        </td>
                        <td class="saludo1" style="width:10%;">Fecha Fin: </td>
                        <td style="width:10%;">
                            <input name="fechafin" type="text" value="<?php echo $_POST['fechafin']?>" maxlength="10" onchange="" onKeyPress="javascript:return solonumeros(event)" onKeyUp="return tabular(event,this)"  id="fc_1198971547" onKeyDown="mascara(this,'/',patron,true)" style="width:80%;" title="DD/MM/YYYY"/>&nbsp;<a href="#" onClick="displayCalendarFor('fc_1198971547');" title="Calendario"><img src="imagenes/calendario04.png" style="width:20px;"/></a>
                        </td>
                        <td></td>
					</tr>
       				<tr>
         				<td  class="saludo1">Tercero:</td>
          				<td>
                            <input id="tercero" type="text" name="tercero" onKeyUp="return tabular(event,this)" onBlur="buscater(event)" value="<?php echo $_POST['tercero']?>" ><a href="#" onClick="mypop=window.open('terceros-ventana.php','','menubar=0,scrollbars=yes, toolbar=no, location=no, width=900,height=500px');mypop.focus();"><img src="imagenes/buscarep.png" align="absmiddle" border="0"></a>

                        </td>
                        <td colspan = "6">
                            <input name="ntercero" type="text" value="<?php echo $_POST['ntercero']?>" style="width:100%" readonly>
                            <input type="hidden" value="0" name="bt">
                        </td>
                    </tr>
          			<tr>
						<td class="saludo1">Valor a Pagar:</td>
						<td>
							<input name="valorpagar" type="text" id="valorpagar" onKeyUp="return tabular(event,this)" value="<?php echo $_POST['valorpagar']?>" style="width:80%" readonly>
						</td>
						<td class="saludo1">Retenciones e Ingresos:</td>
						<td colspan="6">
						<?php
						$reten=array();
						$nreten=array();
						?>
						<select name="retencion" style="width:50%" onChange="validar()" onKeyUp="return tabular(event,this)">
							<option value="">Seleccione ...</option>
							<option value="t"  <?php if($_POST['retencion']=='t') echo "  SELECTED"?>>Todos</option>
							<?php
							//PARA LA PARTE CONTABLE SE TOMA DEL DETALLE DE LA PARAMETRIZACION LAS CUENTAS QUE INICIAN EN 2**********************

							$sqlr="select *from tesoretenciones where estado='S' order by codigo";
							$res=mysqli_query($linkbd,$sqlr);
							while ($row =mysqli_fetch_row($res))
							{
								echo "<option value='R-$row[0]' ";
								$i=$row[0];

								if('R-'.$i==$_POST['retencion'])
								{
									echo "SELECTED";
									$_POST['nretencion']='R - '.$row[1]." - ".$row[2];
								}
								echo ">R - ".$row[1]." - ".$row[2]."</option>";
								$reten[]= 'R-'.$row[0];
								$nreten[]= 'R - '.$row[1]." - ".$row[2];
							}
							$sqlr="select *from tesoingresos where estado='S' order by codigo";
							$res=mysqli_query($linkbd,$sqlr);
							while ($row =mysqli_fetch_row($res))
							{
								echo "<option value='I-$row[0]' ";
								$i=$row[0];

								if('I-'.$i==$_POST['retencion'])
								{
									echo "SELECTED";
									$_POST['nretencion']='I - '.$row[1]." - ".$row[2];
								}
								echo ">I - $row[0] - ".$row[1]." - ".$row[2]."</option>";
								$reten[]= 'I-'.$row[0];
								$nreten[]= 'I - '.$row[1]." - ".$row[2];
							}
							?>
   						</select>
						<input type="hidden" value="<?php echo $_POST['nretencion']?>" name="nretencion"><input type="hidden" value="<?php echo $_POST['oculto']?>" name="oculto">
						<?php
						$tipoRetencion ='';
						$tipoRetencion = buscaTipoDeRetencion(substr($_POST['retencion'],-2));
						if($tipoRetencion == 'C')
						{
							?>
							<select name="tiporet" id="tiporet">
								<option value="" >Seleccione...</option>
								<option value="N" <?php if($_POST['tiporet']=='N') echo "SELECTED"?>>Nacional</option>
								<option value="D" <?php if($_POST['tiporet']=='D') echo "SELECTED"?>>Departamental</option>
								<option value="M" <?php if($_POST['tiporet']=='M') echo "SELECTED"?>>Municipal</option>
							</select>
							<?php
						}
						?>
						<input type='hidden' name='tipoRetencion' id='tipoRetencion' value='<?php echo $tipoRetencion ?>'>
						<input type="button" name="agregard" id="agregard" value="   Agregar   " onClick="agregardetalled()" ><input type="hidden" value="0" name="agregadetdes"></td>
					</tr>
      			</table>
	  			<div class="subpantallac4" style="height:180px;">
       				<table class="inicio" style="overflow:scroll">
       					<?php

                        $fech1=explode("/",$_POST['fechaini']);
                        $fech2=explode("/",$_POST['fechafin']);
                        $f1=$fech1[2]."-".$fech1[1]."-".$fech1[0];
                        $f2=$fech2[2]."-".$fech2[1]."-".$fech2[0];

						if ($_POST['eliminad']!='')
		 				{
							//echo "<TR><TD>ENTROS :".$_POST[elimina]."</TD></TR>";
							$posi=$_POST['eliminad'];
							unset($_POST['ddescuentos'][$posi]);
							unset($_POST['dndescuentos'][$posi]);
							unset($_POST['dntipoRetencion'][$posi]);
							unset($_POST['dntipoRet'][$posi]);
							$_POST['ddescuentos']= array_values($_POST['ddescuentos']);
							$_POST['dndescuentos']= array_values($_POST['dndescuentos']);
							$_POST['dntipoRetencion']= array_values($_POST['dntipoRetencion']);
							$_POST['dntipoRet']= array_values($_POST['dntipoRet']);
						}
						if ($_POST['agregadetdes']=='1')
						{
							if($_POST['retencion']=='t')
							{
								//echo "DDDDD".count($_POST[retencion]);
								for($x=0;$x<count($reten);$x++)
								{
									// echo "f:".$k;
									$_POST['ddescuentos'][]=$reten[$x];
									$_POST['dndescuentos'][]=$nreten[$x];
								}
								$_POST['agregadetdes']='0';
							}
							else
							{
		 						$_POST['ddescuentos'][]=$_POST['retencion'];
								$_POST['dndescuentos'][]=$_POST['nretencion'];
								$_POST['dntipoRetencion'][]=$_POST['tiporet'];
								$_POST['dntipoRet'][]=$_POST['tipoRetencion'];
								$_POST['agregadetdes']='0';
								?>
								<script>
									document.form2.porcentaje.value=0;
									document.form2.vporcentaje.value=0;
									document.form2.retencion.value='';
									document.form2.tiporet.value='';
									document.form2.tipoRetencion.value='';
								</script>

								<?php
							}
		 				}
		  				?>
        				<tr>
							<td class="titulos">Retenciones e Ingresos</td>
							<td class="titulos2">
								<img src="imagenes/del.png" >
								<input type='hidden' name='eliminad' id='eliminad'>
							</td>
						</tr>
						<?php
						$totaldes=0;
						//echo "v:".$_POST[valor];
						for ($x=0;$x<count($_POST['ddescuentos']);$x++)
		 				{
							 echo "<tr>
								<td class='saludo2'>
									<input name='dndescuentos[]' value='".$_POST['dndescuentos'][$x]."' type='text' size='100' readonly>
									<input name='ddescuentos[]' value='".$_POST['ddescuentos'][$x]."' type='hidden'>
									<input name='dntipoRetencion[]' value='".$_POST['dntipoRetencion'][$x]."' type='hidden'>
									<input name='dntipoRet[]' value='".$_POST['dntipoRet'][$x]."' type='hidden'>
								</td>";
		 					echo "		<td class='saludo2'><a href='#' onclick='eliminard($x)'><img src='imagenes/del.png'></a></td>
							</tr>";
		 				}
						$_POST['valorretencion']=$totaldes;

						?>
        				<script>
        					document.form2.totaldes.value=<?php echo $totaldes;?>;
							//calcularpago();
       						document.form2.valorretencion.value=<?php echo $totaldes;?>;
        				</script>
						<?php
						$_POST['valorcheque']=$_POST['valoregreso']-$_POST['valorretencion'];
						?>
					</table>
					<?php
					//***** busca tercero
					if($_POST['bt']=='1')
			 		{
						$nresul=buscatercero($_POST['tercero']);
						if($nresul!='')
						{
							$_POST['ntercero']=$nresul;
							?>
							<script>
							document.getElementById('retencion').focus();document.getElementById('retencion').select();</script>
							<?php
						}
						else
						{
							$_POST['ntercero']="";
							?>
							<script>
							alert("Tercero Incorrecto o no Existe")
							document.form2.tercero.focus();
							</script>
							<?php
			  			}
			 		}
			 		?>
				</div>
				<div class="subpantallac6" style="width:99.6%; height:165px; overflow-x:hidden;">
       				<table class="inicio" style="overflow:scroll">
						<tr>
                            <td class="titulos">Banco</td>
                            <td class="titulos">Cuenta Bancaria</td>
                            <td class="titulos">Valor</td>
                            <td></td>
                        </tr>
						<?php

						$_POST['mddescuentos']=array();
						$_POST['mnbancos']=array();
						$_POST['mctanbancos']=array();
						$_POST['mtdescuentos']=array();
						$_POST['mddesvalores']=array();
						$_POST['mddesvalores2']=array();
						$_POST['mdndescuentos']=array();
						$_POST['mdctas']=array();
						$totalpagar=0;

						preg_match("/([0-9]{2})\/([0-9]{2})\/([0-9]{4})/", $_POST["fecha"], $fecha);
						$fechaf=$fecha[3]."-".$fecha[2]."-".$fecha[1];
						//**** buscar movimientos
						for ($x=0;$x<count($_POST['ddescuentos']);$x++)
						{
							$tm=strlen($_POST['ddescuentos'][$x]);
							//********** RETENCIONES *********
							if(substr($_POST['ddescuentos'][$x],0,1)=='R')
							{
								$query="SELECT conta_pago FROM tesoparametros";
								$resultado=mysqli_query($linkbd,$query);
								$arreglo=mysqli_fetch_row($resultado);
								$opcion=$arreglo[0];
								if($opcion=='1')
								{
									$sqlr="SELECT tesoordenpago_retenciones.id_retencion, SUM(tesoordenpago_retenciones.valor),tesoordenpago.base,tesoordenpago.iva,tesoordenpago.id_orden, tesoegresos.banco, tesoegresos.cuentabanco FROM tesoordenpago, tesoordenpago_retenciones,tesoegresos WHERE tesoegresos.id_orden=tesoordenpago.id_orden AND tesoordenpago.fecha BETWEEN '$f1' AND '$f2' AND tesoordenpago_retenciones.id_retencion='".substr($_POST['ddescuentos'][$x],2,$tm-2)."' and tesoordenpago.id_orden=tesoordenpago_retenciones.id_orden AND tesoordenpago.estado!='R' AND tesoordenpago.tipo_mov='201' AND tesoegresos.estado = 'S' GROUP BY tesoegresos.banco, tesoegresos.cuentabanco order BY tesoordenpago.id_orden ";

									$res=mysqli_query($linkbd,$sqlr);
									//	 echo "<br> ".$sqlr;
									while ($row =mysqli_fetch_row($res))
									{
										/* $sqlrBanco = "SELECT banco, cuentabanco FROM tesoegresos WHERE id_orden = '$row[4]'";
										$resBanco = mysqli_query($linkbd, $sqlrBanco);
										$rowBanco = mysqli_fetch_row($resBanco); */
										$crit ='';
										if($_POST['dntipoRet'][$x] == 'C')
										{
											$crit = "AND destino='".$_POST['dntipoRetencion'][$x]."' AND conceptoingreso!= '-1'";
										}
										$sqlr="select *from tesoretenciones_det where codigo='$row[0]' $crit ORDER BY porcentaje DESC";
										$rowRete = view($sqlr);
										//var_dump(count($rowRete));
										$valorProcentaje = 0;
										$idRetencion = '';
										for($xx = 0; $xx<count($rowRete); $xx++)
										{
											if($idRetencion == $rowRete[$xx]['codigo']){
												$valorProcentaje = $val2 + $rowRete[$xx]['porcentaje'];
											}else{
												$valorProcentaje = $rowRete[$xx]['porcentaje'];
												$idRetencion = $rowRete[$xx]['codigo'];
											}

											$val2 = 0;
											$val2 = $rowRete[$xx]['porcentaje'];

											$rest = substr($rowRete[$xx]['tipoconce'],-2);
											$sq="select fechainicial from conceptoscontables_det where codigo='".$rowRete[$xx]['conceptoingreso']."' and modulo='".$rowRete[$xx]['modulo']."' and tipo='$rest' and fechainicial<'$fechaf' and cuenta!='' order by fechainicial asc";
											$re=mysqli_query($linkbd,$sq);
											while($ro=mysqli_fetch_assoc($re))
											{
												$_POST['fechacausa']=$ro["fechainicial"];
											}
											$sqlr="select * from conceptoscontables_det where codigo='".$rowRete[$xx]['conceptoingreso']."' and modulo='".$rowRete[$xx]['modulo']."' and tipo='$rest' and fechainicial='".$_POST['fechacausa']."'";
											$rst=mysqli_query($linkbd,$sqlr);
											$row1=mysqli_fetch_assoc($rst);
											if($_POST['dntipoRet'][$x] == 'C' || $_POST['retencion']=='t')
											{
												if($valorProcentaje <= 100){
													$_POST['mtdescuentos'][]='R';
													$_POST['mddesvalores'][]=$row[1]*($rowRete[$xx]['porcentaje']/100);
													$_POST['mddesvalores2'][]=$row[1]*($rowRete[$xx]['porcentaje']/100);
													$_POST['mddescuentos'][]=$row[0];
													$_POST['mdctas'][]=$row1['cuenta'];
													$_POST['mnbancos'][]=buscatercero(buscabanco($row[5]));
													$_POST['mctanbancos'][]=$row[6];
													$_POST['mdndescuentos'][]=buscaretencion($row[0]);
													$totalpagar+=$row[1]*($rowRete[$xx]['porcentaje']/100);
												}
											}
											else
											{
												$_POST['mtdescuentos'][]='R';
												$_POST['mddesvalores'][]=$row[1];
												$_POST['mddesvalores2'][]=$row[1];
												$_POST['mddescuentos'][]=$row[0];
												$_POST['mdctas'][]=$row1['cuenta'];
												$_POST['mnbancos'][]=buscatercero(buscabanco($row[5]));
												$_POST['mctanbancos'][]=$row[6];
												$_POST['mdndescuentos'][]=buscaretencion($row[0]);
												$totalpagar+=$row[1];
											}
										}
									}

									$sqlr="SELECT tesoordenpago_retenciones.id_retencion, SUM(tesoordenpago_retenciones.valor),tesoordenpago.base,tesoordenpago.iva,tesoordenpago.id_orden FROM tesoordenpago, tesoordenpago_retenciones WHERE NOT EXISTS (SELECT * FROM tesoegresos WHERE tesoegresos.id_orden = tesoordenpago.id_orden) AND tesoordenpago.fecha BETWEEN '$f1' AND '$f2' AND tesoordenpago_retenciones.id_retencion='".substr($_POST['ddescuentos'][$x],2,$tm-2)."' and tesoordenpago.id_orden=tesoordenpago_retenciones.id_orden AND tesoordenpago.estado!='R' AND tesoordenpago.tipo_mov='201' order BY tesoordenpago.id_orden ";


									$res=mysqli_query($linkbd,$sqlr);
									//	 echo "<br> ".$sqlr;
									while ($row =mysqli_fetch_row($res))
									{
										/* $sqlrBanco = "SELECT banco, cuentabanco FROM tesoegresos WHERE id_orden = '$row[4]'";
										$resBanco = mysqli_query($linkbd, $sqlrBanco);
										$rowBanco = mysqli_fetch_row($resBanco); */
										$crit ='';
										if($_POST['dntipoRet'][$x] == 'C')
										{
											$crit = "AND destino='".$_POST['dntipoRetencion'][$x]."' AND conceptoingreso!= '-1'";
										}
										$sqlr="select *from tesoretenciones_det where codigo='$row[0]' $crit ORDER BY porcentaje DESC";
										$rowRete = view($sqlr);
										//var_dump(count($rowRete));
										$valorProcentaje = 0;
										$idRetencion = '';
										for($xx = 0; $xx<count($rowRete); $xx++)
										{
											if($idRetencion == $rowRete[$xx]['codigo']){
												$valorProcentaje = $val2 + $rowRete[$xx]['porcentaje'];
											}else{
												$valorProcentaje = $rowRete[$xx]['porcentaje'];
												$idRetencion = $rowRete[$xx]['codigo'];
											}

											$val2 = 0;
											$val2 = $rowRete[$xx]['porcentaje'];

											$rest = substr($rowRete[$xx]['tipoconce'],-2);
											$sq="select fechainicial from conceptoscontables_det where codigo='".$rowRete[$xx]['conceptoingreso']."' and modulo='".$rowRete[$xx]['modulo']."' and tipo='$rest' and fechainicial<'$fechaf' and cuenta!='' order by fechainicial asc";
											$re=mysqli_query($linkbd,$sq);
											while($ro=mysqli_fetch_assoc($re))
											{
												$_POST['fechacausa']=$ro["fechainicial"];
											}
											$sqlr="select * from conceptoscontables_det where codigo='".$rowRete[$xx]['conceptoingreso']."' and modulo='".$rowRete[$xx]['modulo']."' and tipo='$rest' and fechainicial='".$_POST['fechacausa']."'";
											$rst=mysqli_query($linkbd,$sqlr);
											$row1=mysqli_fetch_assoc($rst);
											if($_POST['dntipoRet'][$x] == 'C' || $_POST['retencion']=='t')
											{
												if($valorProcentaje <= 100){
													$_POST['mtdescuentos'][]='R';
													$_POST['mddesvalores'][]=$row[1]*($rowRete[$xx]['porcentaje']/100);
													$_POST['mddesvalores2'][]=$row[1]*($rowRete[$xx]['porcentaje']/100);
													$_POST['mddescuentos'][]=$row[0];
													$_POST['mdctas'][]=$row1['cuenta'];
													$_POST['mnbancos'][]="Sin Egreso";
													$_POST['mctanbancos'][]="Sin Egreso";
													$_POST['mdndescuentos'][]=buscaretencion($row[0]);
													$totalpagar+=$row[1]*($rowRete[$xx]['porcentaje']/100);
												}
											}
											else
											{
												$_POST['mtdescuentos'][]='R';
												$_POST['mddesvalores'][]=$row[1];
												$_POST['mddesvalores2'][]=$row[1];
												$_POST['mddescuentos'][]=$row[0];
												$_POST['mdctas'][]=$row1['cuenta'];
												$_POST['mnbancos'][]="Sin Egreso";
												$_POST['mctanbancos'][]="Sin Egreso";
												$_POST['mdndescuentos'][]=buscaretencion($row[0]);
												$totalpagar+=$row[1];
											}
										}
									}

								}
								else
								{
									$sqlr="SELECT DISTINCT tesoordenpago_retenciones.id_retencion, SUM(tesoordenpago_retenciones.valor), tesoegresos.banco, tesoegresos.cuentabanco FROM tesoordenpago, tesoordenpago_retenciones,tesoegresos where tesoegresos.id_orden=tesoordenpago.id_orden AND tesoegresos.estado='S' AND tesoegresos.fecha BETWEEN '$f1' AND '$f2' AND tesoordenpago_retenciones.id_retencion='".substr($_POST['ddescuentos'][$x],2,$tm-2)."' AND tesoordenpago.id_orden=tesoordenpago_retenciones.id_orden AND tesoegresos.estado='S' AND tesoegresos.tipo_mov='201' AND tesoordenpago.tipo_mov='201' GROUP BY tesoegresos.banco, tesoegresos.cuentabanco";
									$res=mysqli_query($linkbd,$sqlr);
									//echo "<br> ".$sqlr;
									while($row = mysqli_fetch_row($res))
									{
										$crit ='';
										if($_POST['dntipoRet'][$x] == 'C')
										{
											$crit = "AND destino='".$_POST['dntipoRetencion'][$x]."' AND conceptoingreso!= '-1'";
										}
										$sqlr="select *from tesoretenciones_det where codigo='$row[0]' $crit ORDER BY porcentaje DESC";
										$rowRete = view($sqlr);
										//var_dump($sqlr);die;
										$valorProcentaje = 0;
										$idRetencion = '';
										for($xx = 0; $xx<count($rowRete); $xx++)
										{
											if($idRetencion == $rowRete[$xx]['codigo']){
												$valorProcentaje = $val2 + $rowRete[$xx]['porcentaje'];
											}else{
												$valorProcentaje = $rowRete[$xx]['porcentaje'];
												$idRetencion = $rowRete[$xx]['codigo'];
											}

											$val2 = 0;
											$val2 = $rowRete[$xx]['porcentaje'];

											$rest = substr($rowRete[$xx]['tipoconce'],-2);
											$sq="select fechainicial from conceptoscontables_det where codigo='".$rowRete[$xx]['conceptoingreso']."' and modulo='".$rowRete[$xx]['modulo']."' and tipo='$rest' and fechainicial<'$fechaf' and cuenta!='' order by fechainicial asc";
											$re=mysqli_query($linkbd,$sq);
											while($ro=mysqli_fetch_assoc($re))
											{
												$_POST['fechacausa']=$ro["fechainicial"];
											}
											$sqlr="select * from conceptoscontables_det where codigo='".$rowRete[$xx]['conceptoingreso']."' and modulo='".$rowRete[$xx]['modulo']."' and tipo='$rest' and fechainicial='".$_POST['fechacausa']."'";
											$rst=mysqli_query($linkbd,$sqlr);
											$row1=mysqli_fetch_assoc($rst);
											if($_POST['dntipoRet'][$x] == 'C' || $_POST['retencion']=='t')
											{
												if($valorProcentaje <= 100){
													$_POST['mtdescuentos'][]='R';
													$_POST['mddesvalores'][]=$row[1]*($rowRete[$xx]['porcentaje']/100);
													$_POST['mddesvalores2'][]=$row[1]*($rowRete[$xx]['porcentaje']/100);
													$_POST['mddescuentos'][]=$row[0];
													$_POST['mdctas'][]=$row1['cuenta'];
													$_POST['mnbancos'][]=buscatercero(buscabanco($row[2]));
													$_POST['mctanbancos'][]=$row[3];
													$_POST['mdndescuentos'][]=buscaretencion($row[0]);
													$totalpagar+=$row[1]*($rowRete[$xx]['porcentaje']/100);
												}
											}
											else
											{
												$_POST['mtdescuentos'][]='R';
												$_POST['mddesvalores'][]=$row[1];
												$_POST['mddesvalores2'][]=$row[1];
												$_POST['mddescuentos'][]=$row[0];
												$_POST['mdctas'][]=$row1['cuenta'];
												$_POST['mnbancos'][]=buscatercero(buscabanco($row[2]));
												$_POST['mctanbancos'][]=$row[3];
												$_POST['mdndescuentos'][]=buscaretencion($row[0]);
												$totalpagar+=$row[1];
											}
										}
									}
									/* while ($row =mysqli_fetch_row($res))
									{
										$sqlr="select *from tesoretenciones_det where codigo='$row[0]' ORDER BY porcentaje DESC";
										$res2=mysqli_query($linkbd,$sqlr);
										$valorProcentaje = 0;
										$idRetencion = '';
										while($row2=mysqli_fetch_row($res2))
										{
											if($idRetencion == $rowRete[$xx]['codigo']){
												$valorProcentaje = $val2 + $rowRete[$xx]['porcentaje'];
											}else{
												$valorProcentaje = $rowRete[$xx]['porcentaje'];
												$idRetencion = $rowRete[$xx]['codigo'];
											}

											$val2 = 0;
											$val2 = $rowRete[$xx]['porcentaje'];

											$rest=substr($row2[6],-2);
											$sq="select fechainicial from conceptoscontables_det where codigo='$row2[4]' and modulo='$row2[5]' and tipo='$rest' and fechainicial<'$fechaf' and cuenta!='' order by fechainicial asc";
											$re=mysqli_query($linkbd,$sq);
											while($ro=mysqli_fetch_assoc($re))
											{
												$_POST['fechacausa']=$ro["fechainicial"];
											}
											$sqlr="select * from conceptoscontables_det where codigo='$row2[4]' and modulo='$row2[5]' and tipo='$rest' and fechainicial='".$_POST['fechacausa']."'";
											$rst=mysqli_query($linkbd,$sqlr);
											$row1=mysqli_fetch_assoc($rst);
											if(substr($row1['cuenta'],0,1)==2)
											{
												if($valorProcentaje <= 100){
													$vpor=$row2[7];
													$_POST['mtdescuentos'][]='R';
													$_POST['mddesvalores'][]=$row[1]*($vpor/100);
													$_POST['mddesvalores2'][]=$row[1]*($vpor/100);
													$_POST['mddescuentos'][]=$row[0];
													$_POST['mdctas'][]=$row1['cuenta'];
													$_POST['mnbancos'][]=buscatercero(buscabanco($row[2]));
													$_POST['mctanbancos'][]=$row[3];
													$_POST['mdndescuentos'][]=buscaretencion($row[0]);
													$totalpagar+=$row[1]*($vpor/100);
												}
											}
										}
									} */
								}

								$sqlr="SELECT DISTINCT tesopagotercerosvigant_retenciones.id_retencion, sum(tesopagotercerosvigant_retenciones.valor), tesopagotercerosvigant.banco FROM tesopagotercerosvigant_retenciones,tesopagotercerosvigant where tesopagotercerosvigant.id_pago = tesopagotercerosvigant_retenciones.id_egreso AND tesopagotercerosvigant.estado='S' AND tesopagotercerosvigant.fecha BETWEEN '$f1' AND '$f2' AND tesopagotercerosvigant_retenciones.id_retencion='".substr($_POST['ddescuentos'][$x],2,$tm-2)."'  GROUP BY tesopagotercerosvigant.banco";
								$res=mysqli_query($linkbd,$sqlr);
								while($row = mysqli_fetch_row($res))
								{
									$crit ='';
									if($_POST['dntipoRet'][$x] == 'C')
									{
										$crit = "AND destino='".$_POST['dntipoRetencion'][$x]."' AND conceptoingreso!= '-1'";
									}
									$sqlr="select *from tesoretenciones_det where codigo='$row[0]' $crit ORDER BY porcentaje DESC";
									$rowRete = view($sqlr);
									//var_dump($sqlr);die;
									$valorProcentaje = 0;
									$idRetencion = '';
									for($xx = 0; $xx<count($rowRete); $xx++)
									{
										if($idRetencion == $rowRete[$xx]['codigo']){
											$valorProcentaje = $val2 + $rowRete[$xx]['porcentaje'];
										}else{
											$valorProcentaje = $rowRete[$xx]['porcentaje'];
											$idRetencion = $rowRete[$xx]['codigo'];
										}

										$val2 = 0;
										$val2 = $rowRete[$xx]['porcentaje'];

										$rest = substr($rowRete[$xx]['tipoconce'],-2);
										$sq="select fechainicial from conceptoscontables_det where codigo='".$rowRete[$xx]['conceptoingreso']."' and modulo='".$rowRete[$xx]['modulo']."' and tipo='$rest' and fechainicial<'$fechaf' and cuenta!='' order by fechainicial asc";
										$re=mysqli_query($linkbd,$sq);
										while($ro=mysqli_fetch_assoc($re))
										{
											$_POST['fechacausa']=$ro["fechainicial"];
										}
										$sqlr="select * from conceptoscontables_det where codigo='".$rowRete[$xx]['conceptoingreso']."' and modulo='".$rowRete[$xx]['modulo']."' and tipo='$rest' and fechainicial='".$_POST['fechacausa']."'";
										$rst=mysqli_query($linkbd,$sqlr);
										$row1=mysqli_fetch_assoc($rst);
										if($_POST['dntipoRet'][$x] == 'C' || $_POST['retencion']=='t')
										{
											if($valorProcentaje <= 100){
												$_POST['mtdescuentos'][]='R';
												$_POST['mddesvalores'][]=$row[1]*($rowRete[$xx]['porcentaje']/100);
												$_POST['mddesvalores2'][]=$row[1]*($rowRete[$xx]['porcentaje']/100);
												$_POST['mddescuentos'][]=$row[0];
												$_POST['mdctas'][]=$row1['cuenta'];
												$_POST['mnbancos'][]=buscatercero(buscabanco($row[2]));
												$_POST['mctanbancos'][]=buscaCuentaBancaria($row[2]);
												$_POST['mdndescuentos'][]=buscaretencion($row[0]);
												$totalpagar+=$row[1]*($rowRete[$xx]['porcentaje']/100);
											}
										}
										else
										{
											$_POST['mtdescuentos'][]='R';
											$_POST['mddesvalores'][]=$row[1];
											$_POST['mddesvalores2'][]=$row[1];
											$_POST['mddescuentos'][]=$row[0];
											$_POST['mdctas'][]=$row1['cuenta'];
											$_POST['mnbancos'][]=buscatercero(buscabanco($row[2]));
											$_POST['mctanbancos'][]=buscaCuentaBancaria($row[2]);
											$_POST['mdndescuentos'][]=buscaretencion($row[0]);
											$totalpagar+=$row[1];
										}
									}
								}

							}
							// echo "<br>cccc".count($_POST[mddescuentos]);
							//****** INGRESOS *******
							if(substr($_POST['ddescuentos'][$x],0,1)=='I')
							{
								//$sqlr="select distinct tesorecaudos_det.ingreso, sum(tesorecaudos_det.valor), tesoreciboscaja.cuentabanco,tesoreciboscaja.cuentacaja from tesorecaudos, tesorecaudos_det,tesoreciboscaja where tesorecaudos.estado='P' and MONTH(tesoreciboscaja.fecha)='$_POST[mes]' and YEAR(tesoreciboscaja.fecha)='".$_POST[vigencias]."' and tesorecaudos_det.ingreso='".substr($_POST[ddescuentos][$x],2,$tm-2)."' and tesorecaudos.id_recaudo=tesorecaudos_det.id_recaudo and tesorecaudos.id_recaudo=tesoreciboscaja.id_recaudo and tesoreciboscaja.tipo=3";
								$sqlr="SELECT DISTINCT tesoreciboscaja_det.ingreso, SUM(tesoreciboscaja_det.valor), tesoreciboscaja.cuentabanco,tesoreciboscaja.cuentacaja FROM  tesoreciboscaja_det,tesoreciboscaja where tesoreciboscaja.estado='S' AND tesoreciboscaja.fecha BETWEEN '$f1' AND '$f2' AND tesoreciboscaja_det.ingreso='".substr($_POST['ddescuentos'][$x],2,$tm-2)."' and tesoreciboscaja_det.id_recibos=tesoreciboscaja.id_recibos AND tesoreciboscaja.ESTADO='S' GROUP BY tesoreciboscaja_det.ingreso, tesoreciboscaja.cuentabanco";
								$res=mysqli_query($linkbd,$sqlr);
								// echo "<br> - ".$sqlr;
								while ($row =mysqli_fetch_row($res))
								{
									/* $sqlr="select *from  tesoingresos_det where codigo='$row[0]' and vigencia='$vigusu'"; */
                                    $sqlr="select *from  tesoingresos_det where codigo='$row[0]' and vigencia=(SELECT MAX(vigencia) FROM tesoingresos_det WHERE codigo = '$row[0]')";
									$res2=mysqli_query($linkbd,$sqlr);
									//echo "$row[0] - ".$sqlr;
									while($row2 =mysqli_fetch_row($res2))
									{
										$sqlr="select *from  conceptoscontables_det where codigo='$row2[2]' and modulo='4' and tipo='C' ";
										$res3=mysqli_query($linkbd,$sqlr);
										// echo "$row2[1] - ".$sqlr;
										while($row3 =mysqli_fetch_row($res3))
										{
											if(substr($row3[4],0,1)=='2')
											{
												$vpor=$row2[5];
												$_POST['mtdescuentos'][]='I';
												$_POST['mddesvalores'][]=$row[1]*($vpor/100);
												$_POST['mddesvalores2'][]=$row[1]*($vpor/100);
												$_POST['mddescuentos'][]=$row[0];
												$_POST['mdctas'][]=$row3[4];
												$_POST['mdndescuentos'][]=buscaingreso($row[0]);
												$_POST['mnbancos'][]=buscatercero(buscabanco($row[2]));
												$_POST['mctanbancos'][]= buscabancocn2($row[2],buscabanco($row[2]));
												$totalpagar+=$row[1]*($vpor/100);
												//$nv=buscaingreso($row[0]);
												//echo "ing:$nv";
											}
										}
									}
								}
								//*****INGRESOS PROPIOS
								$sqlr="SELECT DISTINCT tesosinreciboscaja_det.ingreso, SUM(tesosinreciboscaja_det.valor), tesosinreciboscaja.cuentabanco,tesosinreciboscaja.cuentacaja from  tesosinreciboscaja_det,tesosinreciboscaja WHERE tesosinreciboscaja.estado='S' AND tesosinreciboscaja.fecha BETWEEN '$f1' AND '$f2' AND tesosinreciboscaja_det.ingreso='".substr($_POST['ddescuentos'][$x],2,$tm-2)."' AND tesosinreciboscaja_det.id_recibos=tesosinreciboscaja.id_recibos GROUP BY tesosinreciboscaja_det.ingreso, tesosinreciboscaja.cuentabanco";
								$res=mysqli_query($linkbd,$sqlr);
								//echo "<br> - ".$sqlr;
								while ($row =mysqli_fetch_row($res))
								{
									$sqlr="select *from  tesoingresos_det where codigo='$row[0]' and vigencia=(SELECT MAX(vigencia) FROM tesoingresos_det WHERE codigo = '$row[0]')";
									$res2=mysqli_query($linkbd,$sqlr);
									//echo "$row[0] - ".$sqlr;
									while($row2 =mysqli_fetch_row($res2))
									{
										$sqlr="select *from  conceptoscontables_det where codigo='$row2[2]' and modulo='4' and tipo='C' ";
										$res3=mysqli_query($linkbd,$sqlr);
										// echo "$row2[1] - ".$sqlr;
										while($row3 =mysqli_fetch_row($res3))
										{
											if(substr($row3[4],0,1)=='2' || substr($row3[4],0,1)=='4' || substr($row3[4],0,1) == 3 || substr($row3[4],0,1) == 1)
											{
												$vpor=$row2[5];
												$_POST['mtdescuentos'][]='I';
												$_POST['mddesvalores'][]=$row[1]*($vpor/100);
												$_POST['mddesvalores2'][]=$row[1]*($vpor/100);
												$_POST['mddescuentos'][]=$row[0];
												$_POST['mdctas'][]=$row3[4];
												$_POST['mdndescuentos'][]=buscaingreso($row[0]);
												$_POST['mnbancos'][]=buscatercero(buscabanco($row[2]));
												$_POST['mctanbancos'][]= buscabancocn2($row[2],buscabanco($row[2]));
												$totalpagar+=$row[1]*($vpor/100);
                                                break;
												//$nv=buscaingreso($row[0]);
												//echo "ing:$nv";
											}
										}
									}
								}
								//****** RECAUDO TRANSFERENCIAS *******
								$sqlr="SELECT DISTINCT tesorecaudotransferencia_det.ingreso, SUM(tesorecaudotransferencia_det.valor), tesorecaudotransferencia.banco,tesorecaudotransferencia.ncuentaban FROM  tesorecaudotransferencia_det,tesorecaudotransferencia WHERE tesorecaudotransferencia.estado='S' AND tesorecaudotransferencia.fecha BETWEEN '$f1' AND '$f2' AND tesorecaudotransferencia_det.ingreso='".substr($_POST['ddescuentos'][$x],2,$tm-2)."' AND tesorecaudotransferencia_det.id_recaudo=tesorecaudotransferencia.id_recaudo GROUP BY tesorecaudotransferencia_det.ingreso";
								$res=mysqli_query($linkbd,$sqlr);
								// echo "<br> - ".$sqlr;
								while ($row =mysqli_fetch_row($res))
								{
									$sqlr="select *from  tesoingresos_det where codigo='$row[0]' and vigencia=(SELECT MAX(vigencia) FROM tesoingresos_det WHERE codigo = '$row[0]')";
									$res2=mysqli_query($linkbd,$sqlr);
									//echo "$row[0] - ".$sqlr;
									while($row2 =mysqli_fetch_row($res2))
									{
										$sqlr="select *from  conceptoscontables_det where codigo='$row2[2]' and modulo='4' and tipo='C' ";
										$res3=mysqli_query($linkbd,$sqlr);
										// echo "$row2[1] - ".$sqlr;
										while($row3 =mysqli_fetch_row($res3))
										{
											if(substr($row3[4],0,1)=='2')
											{
												$vpor=$row2[5];
												$_POST['mtdescuentos'][]='I';
												$_POST['mddesvalores'][]=$row[1]*($vpor/100);
												$_POST['mddesvalores2'][]=$row[1]*($vpor/100);
												$_POST['mddescuentos'][]=$row[0];
												$_POST['mdctas'][]=$row3[4];
												$_POST['mdndescuentos'][]=buscaingreso($row[0]);
												$totalpagar+=$row[1]*($vpor/100);
												//$nv=buscaingreso($row[0]);
												//echo "ing:$nv";
											}
										}
									}
								}
							}
							//********************************
						}
						// echo "<br>c...".count($_POST[mddescuentos]);
                        /* var_dump($_POST['mctanbancos']); */

                        $bancos = [];
                        $bacosUnicos = [];
                        $bacosUnicosX = array_unique($_POST['mctanbancos']);
						foreach($bacosUnicosX as $key=>$value) {
							$bacosUnicos[] = $value;
						}
						unset($banco);
                        for ($x=0;$x<count($bacosUnicos);$x++)
						{
							unset($array);
                            $array = $_POST['mctanbancos'];
							/* echo "<br>";
							echo gettype($array); */
                            $obj = [];
                            $sum = 0;
							/* $banco = 'inicio'; */
							foreach($array as $clave => $valor){
								if($valor == $bacosUnicos[$x]){
									$sum += round($_POST['mddesvalores'][$clave],0);
								}
							}
                            /* while ($banco = current($array)) {
								echo $banco." --> ".current($array)."<br>";
                                if ($banco == $bacosUnicos[$x]) {
                                    $sum += round($_POST['mddesvalores'][key($array)],0);
                                }
                                next($array);
                                next($array);
								//var_dump(next($array));
                            } */
                            $bancos[$bacosUnicos[$x]] =  $sum;
                        }

                        $contabilidad = array_unique($_POST['mdctas']);
                        $totalpagar = 0;
                        for($x = 0; $x < count($bacosUnicos); $x++){
                            if($bancos[$bacosUnicos[$x]] > 0){
                                echo "<tr>
                                    <td class='saludo2'>
                                        <input name='mnbancos[]' value='".buscatercero(buscabanco_ter($bacosUnicos[$x]))."' type='text' style='width:100%' readonly>
                                    </td>
                                    <td class='saludo2'>
                                        <input name='mctanbancos[]' value='".$bacosUnicos[$x]."' type='text' style='width:100%' readonly>
                                    </td>
                                    <td class='saludo2' style='width:30px;'>
                                        <input name='mddesvalores[]' value='".$bancos[$bacosUnicos[$x]]."'  type='hidden'>
                                        <input name='mddesvalores2[]' value='".number_format($bancos[$bacosUnicos[$x]],0)."' type='text' style='width:100%' readonly>
                                    </td>
                                    <td></td>
                                </tr>";

                                $totalpagar += $bancos[$bacosUnicos[$x]];
                            }
                        }


						/* for ($x=0;$x<count($_POST['mddescuentos']);$x++)
						{
							// echo "<br>".count($_POST[mddescuentos]);
							$ncheck='check'.$x;
							echo "<tr>
								<td class='saludo2'>
									<input name='mdndescuentos[]' value='".$_POST['mdndescuentos'][$x]."' type='text' style='width:100%' readonly>
									<input name='mddescuentos[]' value='".$_POST['mddescuentos'][$x]."' type='hidden'>
									<input name='mtdescuentos[]' value='".$_POST['mtdescuentos'][$x]."' type='hidden'>
								</td>
								<td class='saludo2'>
									<input name='mnbancos[]' value='".$_POST['mnbancos'][$x]."' type='text' style='width:100%' readonly>
								</td>
								<td class='saludo2'>
									<input name='mctanbancos[]' value='".$_POST['mctanbancos'][$x]."' type='text' style='width:100%' readonly>
								</td>
								<td class='saludo2'>
									<input name='mdctas[]' value='".$_POST['mdctas'][$x]."' type='text' style='width:100%' readonly>
								</td>
								<td class='saludo2'>
									<input name='mddesvalores[]' value='".round($_POST['mddesvalores'][$x],0)."'  type='hidden'>
									<input name='mddesvalores2[]' value='".number_format($_POST['mddesvalores2'][$x],0)."' type='text' style='width:100%' readonly>
								</td>
								<td><input type='checkbox' name='$ncheck' id='$ncheck' value='$x'></td>
							</tr>";
						} */
						$resultado = convertir($totalpagar);
						$_POST['letras']=$resultado." PESOS M/CTE";
						echo "<tr><td></td><td>Total:</td><td><input type='hidden' name='totalpago2' value='".round($totalpagar,0)."' ><input type='text' name='totalpago' value='".number_format($totalpagar,0)."' size='15' readonly></td></tr>";
						echo "<tr><td colspan='3'><input name='letras' type='text' value='$_POST[letras]' size='150' ></td>";
						?>
						<script>
							document.form2.valorpagar.value=<?php echo round($totalpagar,0);?>;
							//calcularpago();
						</script>
        			</table>
	   			</div>
				<?php
				//echo "oculto".$_POST[oculto];
				if($_POST['oculto']=='2')
				{
					/* //**verificacion de guardado anteriormente *****
					$sqlr="select count(*) from tesopagoterceros where id_pago=$_POST[idcomp] ";
					$res=mysqli_query($linkbd,$sqlr);
					//echo $sqlr;
					$r=mysqli_fetch_row($res);
					$numerorecaudos=$r[0];
					if($numerorecaudos==0)
					{
						preg_match("/([0-9]{2})\/([0-9]{2})\/([0-9]{4})/", $_POST["fecha"], $fecha);
						$fechaf=$fecha[3]."-".$fecha[2]."-".$fecha[1];
						//************CREACION DEL COMPROBANTE CONTABLE ************************
						$sqlr="insert into tesopagoterceros (`id_pago`, `tercero`, `banco`, `cheque`, `transferencia`, `valor`, `mes`, `concepto`, `cc`, `estado`,fecha) values ($_POST[idcomp],'$_POST[tercero]','$_POST[banco]', '$_POST[ncheque]','$_POST[ntransfe]',$totalpagar,'$_POST[mes]' , '$_POST[concepto]','$_POST[cc]','S','$fechaf')";
						mysqli_query($linkbd,$sqlr);
						//***busca el consecutivo del comprobante contable
						$sqlr="insert into comprobante_cab (numerotipo,tipo_comp,fecha,concepto,total,total_debito,total_credito,diferencia,estado) values ($_POST[idcomp] ,12,'$fechaf','$_POST[concepto]',0,$totalpagar,$totalpagar,0,'1')";
						mysqli_query($linkbd,$sqlr);
						//echo "<br>C:".count($_POST[mddescuentos]);
						for ($x=0;$x<count($_POST['mddescuentos']);$x++)
						{
							$ncheck='check'.$x;
							if(isset($_POST[$ncheck]))
							{
								echo "<script>document.form2.".$ncheck.".cheched=true;	</script>";
								echo "Seleccionado ".$ncheck."<br>";
								$sqlr="insert into comprobante_det (id_comp,cuenta,tercero,centrocosto,detalle,cheque,valdebito,valcredito,estado,vigencia) values ('12 $_POST[idcomp]','".$_POST['mdctas'][$x]."','".$_POST['tercero']."','".$_POST['cc']."','PAGO RECAUDO A TERCERO MES ".$meses[$_POST['mes']]."','$_POST[ncheque]$_POST[ntransfe]',".$_POST['mddesvalores'][$x].",0,'1','".$vigusu."')";
								echo "$sqlr <br>";
								mysqli_query($linkbd,$sqlr);

								//*** Cuenta BANCO **
								$sqlr="insert into comprobante_det (id_comp,cuenta,tercero,centrocosto,detalle,cheque,valdebito,valcredito,estado,vigencia) values ('12 $_POST[idcomp]','".$_POST['banco']."','".$_POST['tercero']."','".$_POST['cc']."','PAGO RECAUDO A TERCERO MES ".$meses[$_POST['mes']]."','$_POST[ncheque]$_POST[ntransfe]',0,".$_POST['mddesvalores'][$x].",'1','".$vigusu."')";
								mysqli_query($linkbd,$sqlr);
								echo "$sqlr <br>";

								$sqlr="insert into tesopagoterceros_det(`id_pago`, `movimiento`, `tipo`, `valor`, `cuenta`, `estado`) values ($_POST[idcomp],'".$_POST['mddescuentos'][$x]."','".$_POST['mtdescuentos'][$x]."',".$_POST['mddesvalores'][$x].",'".$_POST['mdctas'][$x]."','S')";
								mysqli_query($linkbd,$sqlr);
							}
						}
						echo "<table class='inicio'><tr><td class='saludo1'><center>Se ha almacenado el Recaudo a Terceros con Exito <img src='imagenes/confirm.png'><script>pdf()</script></center></td></tr></table>";
					}//*** if de guardado
					else
					{
						echo "<table class='inicio'><tr><td class='saludo1'><center><img src='imagenes/alert.png'>Ya Se ha almacenado un documento con ese consecutivo</center></td></tr></table>";
					} */

				}
				?>
			</form>
 		</td>
		</tr>
	</table>
</body>
</html>
