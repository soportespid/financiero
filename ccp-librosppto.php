<?php
	header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=utf8");
	require "comun.inc";
	require "funciones.inc";
	sesion();
	$linkbd=conectar_v7();
	$linkbd -> set_charset("utf8");
	cargarcodigopag($_GET['codpag'],$_SESSION["nivel"]);
	date_default_timezone_set("America/Bogota");
?>
<!DOCTYPE >
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="es">
	<head>
		<meta http-equiv="Content-type" content="text/html; charset=utf8"/>
		<meta http-equiv="X-UA-Compatible" content="IE=9"/>
		<title>:: Ideal - Presupuesto</title>
		<link href="css/css2.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css"/>
		<link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css"/>
		<script type="text/javascript" src="css/programas.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<?php titlepag();?>
	</head>
	<body>
		<IFRAME src="alertas.php" name="alertas" id="alertas" style="display:none"></IFRAME>
		<span id="todastablas2"></span>
		<table>
			<tr><script>barra_imagenes("ccpet");</script><?php cuadro_titulos();?></tr>
			<tr><?php menu_desplegable("ccpet");?></tr>
			<tr>
				<td colspan="3" class="cinta"><img src="imagenes/add2.png" class="mgbt1"/><img src="imagenes/guardad.png" class="mgbt1" style="width:24px;"/><img src="imagenes/buscad.png" class="mgbt1"/><img src="imagenes/nv.png" title="Nueva Ventana" onClick="mypop=window.open('ccp-principal.php','','');mypop.focus();" class="mgbt"/></td>
			</tr>
		</table>
		<form name="form2" method="post" action="">
			<table class="inicio">
				<tr>
					<td class="titulos" colspan="2">.: Libros Presupuestales </td>
					<td class="cerrar" style="width:7%" onClick="location.href='ccp-principal.php'">Cerrar</td>
				</tr>
					<td style="background-repeat:no-repeat; background-position:center;">
						<ol id="lista2">
							<li onClick="location.href='ccp-informeCDP.php'" style="cursor:pointer;">Auxiliar Certificados de Disponibilidades Presupuestal (CDP)</li>
							<li onClick="location.href='ccp-informeRP.php'" style="cursor:pointer;">Auxiliar Registros Presupuestales (RP)</li>
							<!-- <li onClick="location.href='ccp-rptcp.php'" style="cursor:pointer;">Auxiliar de obligaciones</li>
							<li onClick="location.href='ccp-rptegresos.php'" style="cursor:pointer;">Auxiliar Egresos</li>
							<li onClick="location.href='ccp-rptegresosnomina.php'" style="cursor:pointer;">Auxiliar Egresos de Nomina</li>
							<li onClick="location.href='ccp-rptegresosterceros.php'" style="cursor:pointer;">Auxiliar Pago recaudo terceros</li>
							<li onClick="location.href='ccp-rptotrosegresos.php'" style="cursor:pointer;">Otros egresos</li>
							<li onClick="location.href='ccp-auxiliaringresos.php'" style="cursor:pointer;">Auxiliar Ingresos</li> -->
							<!-- <li onClick="location.href='ayuda.html'" style="cursor:pointer;">Auxiliar Resoluciones Presupuestales</li>
							<li onClick="location.href='ayuda.html'" style="cursor:pointer;">Auxiliar Vigencias Futuras</li>
							<li onClick="location.href='ayuda.html'" style="cursor:pointer;">Auxiliar Cuentas por Cobrar</li> -->
						</ol>
				</td>
				</tr>
			</table>
		</form>
	</body>
</html>
