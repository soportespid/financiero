<?php
	header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=utf8");
	require 'comun.inc';
	require 'funciones.inc';
	require 'conversor.php';
	sesion();
	$linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");
	cargarcodigopag($_GET['codpag'],$_SESSION["nivel"]);
	date_default_timezone_set("America/Bogota");
	$scrtop = $_GET['scrtop'];
	$totreg = $_GET['totreg'];
	$idcta = $_GET['idcta'];
	$altura = $_GET['altura'];
	$filtro = $_GET['filtro'];
	$numpag = $_GET['numpag'];
	$limreg = $_GET['limreg'];
	$scrtop = 20 * $totreg;
?>
<!DOCTYPE >
<html lang=es>
	<head>
		<meta charset="utf-8"/>
		<meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1"/>
		<meta name="viewport" content="user-scalable=no">
		<title>:: IDEAL 10 - Gesti&oacute;n humana</title>
		<link href="css/css2z.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
		<link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
		<link href="css/css4.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
		<link href="favicon.ico" rel="shortcut icon" />
		<link rel="stylesheet" href="sweetalert2/dist/sweetalert2.min.css">
		<script src="sweetalert2/dist/sweetalert2.min.js"></script>
		<script type="text/javascript" src="css/programas.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script type="text/javascript" src="css/calendario.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script>
			function guardar(){
				var tret = document.form2.tiporete.value;
				var terc = document.form2.tercero.value;
				var tern = document.form2.ntercero.value;
				var salb = document.form2.salbas.value;
				var valr = document.form2.valrete.value;
				var mesr = document.form2.periodo.value;
				var vigr = document.form2.vigencia.value;
				var espago = document.form2.estadopago.value;
				if( espago == 'N'){
					if((tret.trim() != '') && (terc.trim() != '') && (tern.trim() != '') && (salb.trim() != '') && (valr.trim() != '') && (mesr.trim() != '') && (vigr.trim() != '')){
						Swal.fire({
							icon: 'question',
							title: '¿Seguro que quieres guardar los cambios a la retención?',
							showDenyButton: true,
							confirmButtonText: 'Guardar',
							confirmButtonColor: '#01CC42',
							denyButtonText: 'Cancelar',
							denyButtonColor: '#FF121A',
						}).then(
							(result) => {
								if (result.isConfirmed){
									document.form2.oculto.value = "2";
									document.form2.submit();
								}
								else if (result.isDenied){
									Swal.fire({
										icon: 'info',
										title: 'No se guardo la retención',
										confirmButtonText: 'Continuar',
										confirmButtonColor: '#FF121A',
										timer: 2500
									});
								}
							}
						)
					}else{
						Swal.fire({
							icon: 'error',
							title: 'Error!',
							text: 'Falta asignar información a la retención', 
							confirmButtonText: 'Continuar',
							confirmButtonColor: '#FF121A',
							timer: 3500
						});
					}
				}else{
					Swal.fire({
						icon: 'error',
						title: 'Error!',
						text: 'No se puede modificar la retencion, se encuentra paga o aprobada',
						confirmButtonText: 'Continuar',
						confirmButtonColor: '#FF121A',
						timer: 3500
					});
				}
			}
			function validar(formulario){document.form2.submit();}
			function buscater(e){
				if (document.form2.tercero.value != ""){
					document.form2.bt.value = '1';
					document.form2.submit();
				}
			}
			function despliegamodalm(_valor,_tip,mensa,pregunta){
				document.getElementById("bgventanamodalm").style.visibility = _valor;
				if(_valor == "hidden"){
					document.getElementById('ventanam').src = "";
				}else{
					switch(_tip){
						case "1":
							document.getElementById('ventanam').src = "ventana-mensaje1.php?titulos=" + mensa;break;
						case "2":
							document.getElementById('ventanam').src = "ventana-mensaje3.php?titulos=" + mensa;break;
						case "3":
							document.getElementById('ventanam').src = "ventana-mensaje2.php?titulos=" + mensa;break;
						case "4":
							document.getElementById('ventanam').src = "ventana-consulta1.php?titulos=" + mensa + "&idresp=" + pregunta;break;
					}
				}
			}
			function funcionmensaje(){document.location.href = "hum-retencionesbuscar.php";}
			function respuestaconsulta(pregunta){
				switch(pregunta){
					case "1":
						document.form2.oculto.value = "2";
						document.form2.submit();
						break;
					case "2": 
						document.form2.oculto.value = "3";
						document.form2.submit();
						break;
				}
			}
			function despliegamodal2(_valor,_num){
				document.getElementById("bgventanamodal2").style.visibility = _valor;
				if(_valor == "hidden"){
					document.getElementById('ventana2').src = "";
				}else{
					document.getElementById('ventana2').src = "ventanacargafuncionariosretenciones.php?objeto=tercero";
				}
			}
			function atrasc(scrtop,numpag,limreg,filtro,totreg,altura){
				var codig = document.form2.codigo.value;
				var minim = document.form2.minimo.value;
				codig = parseFloat(codig) - 1;
				if(minim <= codig){location.href = "hum-retencioneseditar.php?idban=" + codig + "&scrtop=" + scrtop + "&totreg=" + totreg +  "&altura=" + altura + "&numpag=" + numpag + "&limreg=" + limreg + "&filtro=" + filtro;}
			}
			function adelante(scrtop,numpag,limreg,filtro,totreg,altura){
				var codig = document.form2.codigo.value;
				var maxim = document.form2.maximo.value;
				codig = parseFloat(codig) + 1;
				if(codig <= maxim){location.href = "hum-retencioneseditar.php?idban=" + codig + "&scrtop=" + scrtop + "&totreg=" + totreg +  "&altura=" + altura + "&numpag=" + numpag + "&limreg=" + limreg + "&filtro=" + filtro;}
			}
			function iratras(scrtop,numpag,limreg,filtro,totreg,altura){
				var codig = document.getElementById('codigo').value;
				location.href = "hum-retencionesbuscar.php?idban=" + codig + "&scrtop=" + scrtop + "&totreg=" + totreg + "&altura=" + altura + "&numpag=" + numpag + "&limreg=" + limreg + "&filtro=" + filtro;
			}
		</script>
		<?php titlepag();?>
	</head>
	<body>
		<IFRAME src="alertas.php" name="alertas" id="alertas" style="display:none"></IFRAME>
		<span id="todastablas2"></span>
		<table>
			<tr><script>barra_imagenes("hum");</script><?php cuadro_titulos();?></tr> 
			<tr><?php menu_desplegable("hum");?></tr>
			<tr>
				<td colspan="3" class="cinta">
					<img src="imagenes/add.png" title="Nuevo" onClick="location.href='hum-retencionesagregar.php'" class="mgbt">
					<img src="imagenes/guarda.png" title="Guardar" onClick="guardar()" class="mgbt">
					<img src="imagenes/busca.png" title="Buscar" onClick="location.href='hum-retencionesbuscar.php'" class="mgbt">
					<img src="imagenes/nv.png" title="nueva ventana"  class="mgbt" onClick="mypop=window.open('hum-principal.php','','');mypop.focus();">
					<img src='imagenes/iratras.png' title='Men&uacute; Nomina' class='mgbt' onClick="iratras(<?php echo "'$scrtop','$numpag','$limreg','$filtro','$totreg','$altura'"; ?>)">
				</td>
			</tr>
		</table>
		<div id="bgventanamodalm" class="bgventanamodalm">
			<div id="ventanamodalm" class="ventanamodalm">
				<IFRAME src="" name="ventanam" marginWidth=0 marginHeight=0 frameBorder=0 id="ventanam" frameSpacing=0 style=" width:700px; height:130px; top:200; overflow:hidden;"> 
				</IFRAME>
			</div>
		</div>
		<form name="form2" method="post" action="">
			<?php 
				if($_POST['oculto'] == ''){
					$sqlr = "SELECT * FROM hum_retencionesfun WHERE id='".$_GET['idban']."'";
					$res = mysqli_query($linkbd,$sqlr);
					$row = mysqli_fetch_row($res);
					$_POST['fecha'] = date('d-m-Y',strtotime($row[1]));
					$_POST['periodo'] = $row[7];
					$_POST['vigencia'] = $row[8];
					$_POST['tiporete'] = $row[2];
					$_POST['tercero'] = $row[3];
					$_POST['ntercero'] = $row[4];
					$_POST['salbas'] = $row[5];
					$_POST['valrete'] = $row[6];
					$_POST['variablepago'] = $row[11];
					$_POST['estadopago'] = $row[9];
					$_POST['codigo'] = $_GET['idban'];
					$sqlb = "SELECT MIN(id),MAX(id) FROM hum_retencionesfun";
					$resb = mysqli_query($linkbd,$sqlb);
					$rowb = mysqli_fetch_array($resb);
					$_POST['maximo'] = $rowb[1];
					$_POST['minimo'] = $rowb[0];
					
				}
				if($_POST['bt'] == '1'){
					$nresul = buscatercero($_POST['tercero']);
					if($nresul != ''){
						$_POST['ntercero'] = $nresul;
						$sqlr = "
						SELECT codfun, GROUP_CONCAT(descripcion ORDER BY CONVERT(valor, SIGNED INTEGER) SEPARATOR '<->')
						FROM hum_funcionarios
						WHERE (item = 'VALESCALA') AND codfun=(SELECT codfun FROM hum_funcionarios WHERE descripcion='".$_POST['tercero']."' AND estado='S') AND estado='S'
						GROUP BY codfun";
						$resp2 = mysqli_query($linkbd,$sqlr);
						$row2 = mysqli_fetch_row($resp2);
						$_POST['salbas'] = $row2[1];
					}else{
						$_POST['ntercero'] = "";
					}
				}
			?>
			<input type="hidden" name="estadopago" id="estadopago" value="<?php echo $_POST['estadopago'];?>" />
			<table  class="inicio ancho" style="width:99.7%;">
				<tr>
					<td class="titulos" colspan="10">:: Retenciones Funcionarios</td>
					<td class="cerrar" style="width:7%" onClick="location.href='hum-principal.php'">Cerrar</td>
				</tr>
				<tr>
					<td class="tamano01" style="width: 2.5cm;">C&oacute;digo:</td>
					<td style="width:12%;"><img src="imagenes/back.png" onClick="atrasc(<?php echo "'$scrtop', '$numpag', '$limreg', '$filtro', '$totreg', '$altura'";?>)" class="icobut" title="Anterior"/>&nbsp;<input name="codigo" id="codigo" type="text" value="<?php echo $_POST['codigo']?>" maxlength="2" style="width:50%"  onKeyUp="return tabular(event,this)" readonly>&nbsp;<img src="imagenes/next.png" onClick="adelante(<?php echo "'$scrtop', '$numpag', '$limreg', '$filtro', '$totreg', '$altura'"; ?>)" class="icobut" title="Sigiente"/></td>
					<td class="tamano01"  style="width:4cm;">Fecha Registro:</td>
					<td style="width:12%"><input type="text" name="fecha" value="<?php echo $_POST['fecha']?>" style="width:98%;" readonly/></td>
					<td class="tamano01" style="width:3.5cm;">Tipo Retenci&oacute;n:</td>
					<td colspan="5">
						<select name="tiporete" id="tiporete" style="width:100%;" >
							<option value="-1">Seleccione ....</option>
								<?php 
								$sqlr="SELECT id, codigo, nombre FROM tesoretenciones WHERE estado = 'S' AND nomina = '1' ORDER BY codigo";
								$resp = mysqli_query($linkbd,$sqlr);
								while ($row = mysqli_fetch_row($resp)){
									if($_POST['tiporete'] == $row[0]){
										echo "<option value='$row[0]' SELECTED>$row[1] - $row[2]</option>";
										$_POST['ntiporete'] = $row[1];
									}else{
										echo "<option value='$row[0]'>$row[1] - $row[2]</option>";
									}
								}
							?>
						</select>
						<input type="hidden" name="ntiporete" id="ntiporete" value="<?php echo $_POST['ntiporete']?>" />
					</td>
				</tr>
				<tr>
					<td class="tamano01" >Vigencia:</td>
					<td><input type="text" name="vigencia" id="vigencia" value="<?php echo $_POST['vigencia']?>" onKeyUp="return tabular(event,this)" readonly></td>
					<td class="tamano01">Tipo de Pago:</td>
					<td colspan="4">
						<select name="variablepago" id="variablepago" class="tamano02" style="width:100%;">
							<option value="-1">Seleccione ....</option>
							<?php
								$sqlr="SELECT codigo,nombre FROM ccpethumvariables WHERE estado='S'";
								$resp = mysqli_query($linkbd,$sqlr);
								while ($row =mysqli_fetch_row($resp)){
									if(in_array($row[0], $vtiponum)){$vartip = "S";}
									else{$vartip = "N";}
									if($_POST['variablepago'] == $row[0]){
										if($vartip == "N"){echo "<option value='$row[0]' SELECTED>$row[0] - $row[1]</option>";}
									}else{
										if($vartip == "N"){echo "<option value='$row[0]' >$row[0] - $row[1]</option>";}
									}
								}
							?>
						</select>
					</td>
				</tr>
				<tr>
					<td class="saludo1">Funcionario:</td>
					<td><input type="text" id="tercero" name="tercero" onKeyPress="javascript:return solonumeros(event)" onKeyUp="return tabular(event,this)" onChange="buscater(event)" value="<?php echo $_POST['tercero'];?>" style="width:100%;" class="colordobleclik" autocomplete="off" onDblClick="despliegamodal2('visible');"></td>
					<td colspan="8"><input type="text" name="ntercero" id="ntercero" value="<?php echo $_POST['ntercero'];?>" style="width: 100%;" readonly/></td>
				</tr>
				<tr>
					<td class="saludo1">Salario B&aacute;sico:</td>
					<td><input type="text" name="salbas" id="salbas" value="<?php echo $_POST['salbas']?>" style="width:100%;" readonly/></td>
					<td class="saludo1">Valor Retenci&oacute;n:</td>
					<td style="width:12%;"><input type="text" name="valrete" id="valrete" value="<?php echo $_POST['valrete'];?>" style="width:100%;" onKeyPress="javascript:return solonumeros(event)" onKeyUp="return tabular(event,this)"/></td>
					<td class="saludo1" style="width:2.5cm;">Mes:</td>
					<td style="width:12%;">
						<select name="periodo" id="periodo" style="width:100%">
							<option value="-1">Seleccione ....</option>
							<?php
								$sqlr="SELECT id, nombre FROM meses ORDER BY id";
								$resp = mysqli_query($linkbd,$sqlr);
								while ($row = mysqli_fetch_row($resp))
								{
									if($_POST['periodo'] == $row[0])
									{
										echo "<option value='$row[0]' SELECTED>$row[1]</option>";
										$_POST['nmes'] = $tablameses[$x]['nombre'];
									}
									else{echo "<option value='$row[0]'>$row[1]</option>";}
								}
							?>
						</select>
						<input type="hidden" name="nmes" id="nmes" value="<?php echo $_POST['nmes'];?>" />
						
					</td>
					<td></td>
				</tr>
			</table>
			<?php
				if($_POST['oculto'] == "2")
				{
					$sqlr = "UPDATE hum_retencionesfun SET tiporetencion = '".$_POST['tiporete']."', docfuncionario = '".$_POST['tercero']."', nomfuncionario = '".$_POST['ntercero']."', salbasico = '".$_POST['salbas']."', valorretencion = '".$_POST['valrete']."', mes = '".$_POST['periodo']."', vigencia = '".$_POST['vigencia']."', tipopago = '".$_POST['variablepago']."' WHERE id = '".$_POST['codigo']."'";
					if(!mysqli_query($linkbd,$sqlr)){
						echo "
						<script>
							Swal.fire({
								icon: 'error',
								title: 'Error!',
								text: 'Error al Editar Retenciones', 
								confirmButtonText: 'Continuar',
								confirmButtonColor: '#FF121A',
								timer: 3500
							});
						</script>";
					}else{
						echo "
						<script>
							Swal.fire({
								icon: 'success',
								title: 'Se Edito la Retenci\\xf3n Exitosamente',
								showConfirmButton: true,
								confirmButtonText: 'Continuar',
								confirmButtonColor: '#01CC42',
								timer: 3500
							});
						</script>";
					}
				}
			?>
			<input type="hidden" name="maximo" id="maximo" value="<?php echo $_POST['maximo'] ?>"/>
			<input type="hidden" name="minimo" id="minimo" value="<?php echo $_POST['minimo'] ?>"/>
			<input type="hidden" name="bt" id="bt" value="0"/>
			<input type="hidden" name="oculto" id="oculto" value="1"/>
		</form>
		<div id="bgventanamodal2">
			<div id="ventanamodal2">
				<IFRAME  src="" name="buster" marginWidth=0 marginHeight=0 frameBorder=0 id="ventana2" frameSpacing=0 style="left:500px; width:900px; height:500px; top:200;"></IFRAME>
			</div>
		</div>
	</body>
</html>