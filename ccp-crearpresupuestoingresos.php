<?php
	require "comun.inc";
	require "funciones.inc";
	session_start();
	cargarcodigopag(@$_GET['codpag'], @$_SESSION['nivel']);
	header("Cache-control: private"); // Arregla IE 6
	date_default_timezone_set("America/Bogota");
?>
<!DOCTYPE >
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="es">
	<head>
	 	<meta http-equiv="Content-Type" content="text/html" charset="iso-8859-1"/>
        <meta http-equiv="X-UA-Compatible" content="IE=9"/>
		<title>:: IDEAL 10 - Presupuesto</title>
        <link href="css/css2.css" rel="stylesheet" type="text/css" />
        <link href="css/css3.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="css/programas.js"></script>
        <script src="vue/vue.min.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<link rel="stylesheet" href="bootstrap/css/bootstrap.css">
        <link rel="stylesheet" href="bootstrap/css/estilos.css">
		<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">

        <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
        <script type="text/javascript" src="bootstrap/fontawesome.5.11.2/js/all.js"></script>
		<link href="css/style.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css"/>
		<?php titlepag();?>

		<style>
			.background_active_color{
				background: #16a085;
			}
            .background_active_clasificador{
                font-family: calibri !important;
				font-weight: bold !important;
				font-size:14px !important;
            }
			.background_active{
				/* font: 115% sans-serif !important; */
    			/*font-weight: 700 !important;*/
				/*font-family: "Constantia", serif !important;*/
				font-family: calibri !important;
				font-weight: bold !important;
				font-size:20px !important;
			}
			.background_active_1{
				/* font: 115% sans-serif !important; */
    			/*font-weight: 700 !important;*/
				/*font-family: "Constantia", serif !important;*/
				font-family: helvética !important;
				font-size:20px !important;
			}
			.inicio--no-shadow{
				box-shadow: none;
			}
			.btn-delete{
				background: red;
				color: white;
				border-radius: 5px;
				border: none;
				font-size: 13px;
			}
			.btn-delete:hover, .btn-delete:focus{
				background: white;
				color: red;
			}
			.modal-mask {
			position: fixed;
			z-index: 9998;
			top: 0;
			left: 0;
			width: 100%;
			height: 100%;
			background-color: rgba(0, 0, 0, .5);
			display: table;
			transition: opacity .3s ease;
			}

			.modal-wrapper {
			display: table-cell;
			vertical-align: middle;
			}
			.modal-body{
				max-height: 400px;
				overflow-y: scroll;
			}
			.modal-intermetio{
				margin: 0 15px;
				font-family: helvética !important;
				font-size: 26px !important;
				padding: 10px 0;
			}
			.modal-intermedio-agregar{
				text-align:right;
				padding: 4px;
				margin-top: 6px;
				margin-right: 20px
			}
			.modal-body_1{
				padding-top: 15px;
				height: 40px;
			}

			.loader-table{
				/* background-color: #dff9fb;
				opacity: .5; */
				display: flex;
				align-items: center;
				justify-content: center;
				height: 75%;
			}
			.spinner{
				border: 4px solid rgba(0, 0, 0, 0.2);
				border-left-color: #39C;
				border-radius: 50%;
				width: 50px;
				height: 50px;
				animation: spin .9s linear infinite;
			}
			@keyframes spin {
				to { transform: rotate(360deg); }
			}

			[v-cloak]{
				display : none;
			}
		</style>
    </head>
    <body>
        <IFRAME src="alertas.php" name="alertas" id="alertas" style="display:none"></IFRAME>
        <span id="todastablas2"></span>
        <table>
            <tr><script>barra_imagenes("ccpet");</script><?php cuadro_titulos();?></tr>
            <tr><?php menu_desplegable("ccpet");?></tr>
		</table>
        <div class="bg-white group-btn p-1">
            <button type="button"  onClick="location.href='#'"
                    class="btn btn-white btn-primary-hover d-flex justify-between align-items-center">
                    <span>Nuevo</span>
                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 -960 960 960">
                        <path d="M440-440H200v-80h240v-240h80v240h240v80H520v240h-80v-240Z"></path>
                    </svg>
                </button>
                <button type="button" onClick="location.href='#'" class="btn btn-white btn-primary-hover d-flex justify-between align-items-center">
                    <span>Guardar</span>
                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 -960 960 960">
                        <path d="M840-680v480q0 33-23.5 56.5T760-120H200q-33 0-56.5-23.5T120-200v-560q0-33 23.5-56.5T200-840h480l160 160Zm-80 34L646-760H200v560h560v-446ZM480-240q50 0 85-35t35-85q0-50-35-85t-85-35q-50 0-85 35t-35 85q0 50 35 85t85 35ZM240-560h360v-160H240v160Zm-40-86v446-560 114Z">
                        </path>
                    </svg>
                </button>
                <button type="button" v-on:click="location.href='ccp-buscarActoAdministrativo.php'"
                    class="btn btn-white btn-primary-hover d-flex justify-between align-items-center">
                    <span>Buscar</span>
                    <svg class="fill-black group-hover:fill-white w-5 h-5" xmlns="http://www.w3.org/2000/svg"
                        viewBox="0 -960 960 960">
                        <path
                            d="M784-120 532-372q-30 24-69 38t-83 14q-109 0-184.5-75.5T120-580q0-109 75.5-184.5T380-840q109 0 184.5 75.5T640-580q0 44-14 83t-38 69l252 252-56 56ZM380-400q75 0 127.5-52.5T560-580q0-75-52.5-127.5T380-760q-75 0-127.5 52.5T200-580q0 75 52.5 127.5T380-400Z">
                        </path>
                    </svg>
                </button>
                <button type="button" onclick="mypop=window.open('ccp-principal.php','','');mypop.focus();" class="btn btn-white btn-primary-hover d-flex justify-between align-items-center">
                    <span>Nueva ventana</span>
                    <svg class="fill-black group-hover:fill-white w-5 h-5" xmlns="http://www.w3.org/2000/svg" viewBox="0 -960 960 960">
                        <path d="M320-120v-80h80v-80H160q-33 0-56.5-23.5T80-360v-400q0-33 23.5-56.5T160-840h640q33 0 56.5 23.5T880-760v400q0 33-23.5 56.5T800-280H560v80h80v80H320ZM160-360h640v-400H160v400Zm0 0v-400 400Z"></path>
                    </svg>
                </button>
                <button type="button" onClick="location.href='ccp-capturapresupuestoinicial.php'" class="btn btn-success btn-success-hover d-flex justify-between align-items-center">
                    <span>Atras</span>
                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 -960 960 960">
                        <path d="M760-200v-160q0-50-35-85t-85-35H273l144 144-57 56-240-240 240-240 57 56-144 144h367q83 0 141.5 58.5T840-360v160h-80Z"></path>
                    </svg>
                </button>
            </div>
			<div id="myapp" style="height:inherit;" v-cloak>
                <div class="row">
					<div class="col-12">
						<h5 style="padding-left:30px; padding-top:5px; padding-bottom:5px; background-color: #0FB0D4">Agregar presupuesto ingresos:</h5>
					</div>
				</div>
				<div class="row" style="margin: 1px 10px 0px">
					<div class="col-12">
						<div class="row" style="border-radius:2px; background-color: #E1E2E2; ">
						<div class="col-md-2" style="display: grid; align-content:center;">
								<label for="" style="margin-bottom: 0; font-size:15px;">Dependencia: </label>
							</div>
							<div class="col-md-2" style="padding: 4px">
                                <select v-model="selectUnidadEjecutora" v-on:change="buscarIngresos" class="form-control select" style = "font-size: 10px; margin-top:4px">
                                    <option v-for="unidad in unidadesejecutoras" v-bind:value="unidad[0]">
                                        {{ unidad[0] }} - {{ unidad[1] }}
                                    </option>
                                </select>
							</div>
                            <div class="col-md-1" style="display: grid; align-content:center;">
								<label for="" style="margin-bottom: 0; font-size:14px;">Situción/Fondo: </label>
							</div>
							<div class="col-md-1" style="padding: 4px">

                                <select v-model="selectMedioPago" v-on:change="buscarIngresos" class="form-control select" style = "font-size: 10px; margin-top:4px">
                                    <option v-for="option in optionsMediosPagos" v-bind:value="option.value">
                                        {{ option.text }}
                                    </option>
                                </select>
							</div>

                            <div class="col-md-2" style="display: grid; align-content:center;">
								<label for="" style="margin-bottom: 0; font-size:15px;">Vigencia del gasto: </label>
							</div>
							<div class="col-md-2" style="padding: 4px">
                                <select v-model="selectVigenciaGasto" v-on:change="buscarIngresos" class="form-control select" style = "font-size: 10px; margin-top:4px">
                                    <option v-for="vigenciaDeGasto in vigenciasdelgasto" v-bind:value="vigenciaDeGasto[0]">
                                        {{ vigenciaDeGasto[1] }} - {{ vigenciaDeGasto[2] }}
                                    </option>
                                </select>
							</div>

                            <div class="col-md-1" style="display: grid; align-content:center;">
								<label for="" style="margin-bottom: 0; font-size:15px;">Vigencia: </label>
							</div>
							<div class="col-md-1" style="display: grid; align-content:center;">
								<select v-model="vigencia" style="width:100%" v-on:Change="buscarIngresos" class="form-control" style = "font-size: 10px; margin-top:4px">
									<option v-for="year in years" :value="year[0]">{{ year[0] }}</option>
								</select>
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-12" v-show="mostrar_resultados_ingresos">
						<div style="margin: 5px 10px 0">
							<table>
								<thead>
									<tr>
										<td class='titulos' width="20%"  style="padding-left: 10px; font: 160% sans-serif; border-radius: 5px 0px 0px 0px;">CCPET</td>
										<td class='titulos' style="font: 160% sans-serif;">Nombre</td>
										<td class='titulos' width="10%"  style="font: 160% sans-serif;">Tipo</td>
										<td class='titulos' width="30%" style="font: 160% sans-serif; border-radius: 0px 5px 0px 0px; text-align: center">Asignar presupuesto</td>
									</tr>
								</thead>
							</table>
						</div>
						<div style="margin: 0px 10px 10px; border-radius: 0 0 0 5px; height: 390px; overflow: scroll; overflow-x: hidden; background: white; resize: vertical;">
							<table class='inicio inicio--no-shadow' style='margin: 10px; border-radius: 4px;'>
								<tbody v-if="mostrar_resultados_ingresos">
									<?php
										$co ='zebra1';
										$co2='zebra2';
									?>
									<tr v-for="ingreso in ingresos" :class=" ingreso[2] == 'A' ? 'background_active' : 'background_active_1' " v-on:dblclick=" ingreso[2] == 'A' ? '' : agregarPresupuesto(ingreso[0], ingreso[1])" :style = "ingreso[2] == 'A' ? '' : 'text-rendering: optimizeLegibility; cursor: pointer !important; background-color: dbeafe; border-radius: 2px;'" class='<?php echo $co; ?>' style="font: 130% sans-serif;">
										<td width="20%" style="padding-left: 10px; ">{{ ingreso[0] }}</td>
										<td>{{ ingreso[1] }}</td>
										<td width="9%">{{ ingreso[2] }}</td>
										<td width="30%" v-if="ingreso[2] == 'C' "  style='text-rendering: optimizeLegibility; text-align:center; '>
											{{ formatonumero(valorIngresoCuenta[ingreso[0]]) }}
										</td>
										<td width="30%" v-else style='text-rendering: optimizeLegibility; text-align:center; '> {{ formatonumero(valorIngresoCuenta[ingreso[0]])	 }} </td>

										<?php
										$aux=$co;
										$co=$co2;
										$co2=$aux;
										?>
									</tr>
								</tbody>
								<tbody v-else>
									<tr>
										<td width="20%"style="font: 120% sans-serif; padding-left:10px; text-align:center;" colspan="3">Sin resultados</td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
				</div>

				<div v-show="showModal">
					<transition name="modal">
						<div class="modal-mask">
							<div class="modal-wrapper">
								<div class="modal-dialog modal-lg" style = "max-width: 1200px !important;" role="document">
									<div class="modal-content" style = "width: 100% !important; max-height: 700px;" scrollable>
										<div class="modal-header">
											<h5 class="modal-title">Clasificador CUIN</h5>
											<button type="button" class="close" data-dismiss="modal" aria-label="Close">
												<span aria-hidden="true" @click="showModal = false">&times;</span>
											</button>
										</div>
										<div class="modal-body">
											<!-- Default unchecked -->

											<!-- <span>Checked names: {{ checkedClasificadores | json }}</span> -->
											<div class="row" style="margin: 2px 0 0 0; border-radius:4px; background-color: #E1E2E2; ">
												<div class="col-md-4" style="padding: 12px 0px 0px 30px; font: 120% sans-serif;">
													<label for="">Buscar por nombre o nit</label>
												</div>

												<div class="col-md-6" style="padding: 4px">
													<input type="text" class="form-control" style="height: auto; border-radius:4px;" placeholder="Ej: Instituto de Casas Fiscales del Ejercito" v-on:keyup.enter="searchMonitorCuin"  v-model="searchCuin.keywordCuin">
												</div>
												<div class="col-md-2 col-sm-4 col-md-offset-1" style="padding: 4px">
													<button type="submit" class="btn btn-dark" value="Buscar" style="height: auto; border-radius:4px;" v-on:click="searchMonitorCuin">Buscar</button>
												</div>
											</div>
											<div v-if="!show_table_search" class="loader-table">
												<div class="spinner"></div>
											</div>
											<table v-if="show_table_search" class="inicio inicio--no-shadow">
												<thead>
													<tr>
														<th>C&oacute;digo</th>
														<th>Nit</th>
														<th>Nombre</th>
														<th>C&oacute;didgo CUIN</th>
													</tr>
												</thead>
												<tbody>
                                                    <tr v-for="cuin in resultsCuin" v-on:click="seleccionarCuin(cuin)"  v-on:dblclick= "continuar(1)" v-bind:class="cuin[2] === cuin_p ? 'background_active_color' : ''" class='<?php echo $co; ?>' style='text-rendering: optimizeLegibility; cursor: pointer !important; style=\"cursor: hand\"'>

													<!-- <tr v-for="cuin in resultsCuin" v-on:click="seleccionarCuin(cuin)" v-bind:class="cuin[2] == cuin_p ? 'background_active_color' : ''" style='font-weight: normal; text-rendering: optimizeLegibility; cursor: pointer !important; style=\"cursor: hand\"'> -->
														<td>{{ cuin[2] }}</td>
														<td>{{ cuin[3] }}</td>
														<td>{{ cuin[4] }}</td>
														<td>{{ cuin[13] }}</td>
													</tr>
												</tbody>
											</table>
										</div>
										
										<div class="modal-footer">
											<button type="button" class="btn btn-primary" v-on:click="continuar(1)">Continuar</button>
											<button type="button" class="btn btn-secondary" @click="showModal = false">Cerrar</button>
										</div>
									</div>
								</div>
							</div>
						</div>
					</transition>
				</div>


                <div v-show="showModal_fuentes">
					<transition name="modal">
						<div class="modal-mask">
							<div class="modal-wrapper">
								<div class="modal-dialog modal-lg" style = "max-width: 100% !important;" role="document">
									<div class="modal-content"  style = "width: 100% !important;" scrollable>
										<div class="modal-header">
                                            <h5 class="modal-title">Atributos</h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true" @click="showModal_fuentes = false">&times;</span>
                                            </button>
										</div>
                                        <div>
                                            <h6 style="margin: 0px 12px">{{ cuentaActual }} - {{ nombreCuentaActual }}</h6>
                                            <!-- <h6 style="margin: 0px 12px">{{ selectMedioPago == 'CSF' ? 'Con situacion de fondos' : 'Sin situacion de fondos' }}</h6> -->
                                        </div>
                                        <div class="modal-body">
                                            <div style="margin: 2px 0 0 0;">
                                                <div class="row" style="margin: 4px; border-radius:4px; background-color: #E1E2E2; ">
                                                    <div class="col-md-2" style="padding: 12px 0px 0px 30px; font: 140% sans-serif;">
                                                        <label for="">Fuente:</label>
                                                    </div>

                                                    <div class="col-md-6 col-md-offset-6" style="padding: 4px">
                                                        <input type="text" class="form-control" placeholder="Buscar por nombre o c&oacute;digo de la fuente" v-on:keyup="searchMonitorFuente" v-model="searchFuente.keywordFuente">
                                                    </div>
                                                </div>
                                                <table>
                                                    <thead>
                                                        <tr>
                                                            <td width="20%" class='titulos' style="font: 120% sans-serif; padding-left:10px; border-radius: 5px 0 0 0;">C&oacute;digo</td>
                                                            <td class='titulos' style="font: 120% sans-serif; ">Nombre</td>
                                                            <td width="20%" class='titulos' style="font: 120% sans-serif; ">Valor</td>

                                                        </tr>
                                                    </thead>
                                                </table>
                                            </div>
                                            <div style="margin: 2px 0 0 0; border-radius: 0 0 0 6px; height: 200px; overflow: scroll; overflow-x: hidden; background: white; ">
                                                <table class='inicio inicio--no-shadow'>
                                                    <tbody>
                                                        <?php
                                                            $co ='zebra1';
                                                            $co2='zebra2';
                                                        ?>
                                                        <tr v-for="fuente in fuentes" v-on:click="seleccionarFuente(fuente)"  v-on:dblclick= "continuar('')" v-bind:class="fuente[1] === fuente_p ? 'background_active_color' : ''" class='<?php echo $co; ?>' style='text-rendering: optimizeLegibility; cursor: pointer !important; style=\"cursor: hand\"'>
                                                            <td width="20%" style="font: 120% sans-serif; padding-left:10px">{{ fuente[1] }}</td>
                                                            <td style="font: 120% sans-serif; padding-left:10px">{{ fuente[2] }}</td>
                                                            <td width="20%" style="font: 120% sans-serif; padding-left:10px">{{ formatonumero(valorIngreso[fuente[1]]) }}</td>

                                                            <?php
                                                            $aux=$co;
                                                            $co=$co2;
                                                            $co2=$aux;
                                                            ?>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
										<div class="modal-footer">
											<button type="button" class="btn btn-primary" v-on:click="continuar('')">Continuar</button>
											<button type="button" class="btn btn-secondary" @click="showModal_fuentes = false">Cerrar</button>
										</div>
									</div>
								</div>
							</div>
						</div>
					</transition>
				</div>

                <div v-show="showModal_bienes_transportables">
					<transition name="modal">
						<div class="modal-mask">
							<div class="modal-wrapper">
								<div class="modal-dialog modal-lg" style = "max-width: 100% !important;" role="document">
									<div class="modal-content"  style = "width: 100% !important;" scrollable>
										<div class="modal-header">
											<h5 class="modal-title">CPC - Bientes transportables</h5>
											<button type="button" class="close" data-dismiss="modal" aria-label="Close">
												<span aria-hidden="true" @click="showModal_bienes_transportables = false">&times;</span>
											</button>
										</div>
                                        <div class="modal-body">
                                            <div style="margin: 2px 0 0 0;">
                                                <div class="row" style="margin: 4px; border-radius:4px; background-color: #E1E2E2; ">
                                                    <div class="col-md-3" style="padding: 12px 0px 0px 30px; font: 140% sans-serif;">
                                                        <label for="">Bien Transportable:</label>
                                                    </div>

                                                    <div class="col-md-6 col-md-offset-6" style="padding: 4px">
                                                        <input type="text" class="form-control" placeholder="Buscar por nombre o c&oacute;digo del bien transportable" v-on:keyup="searchBienesTransportables" v-model="searchGeneral.keywordGeneral">
                                                    </div>
                                                </div>
                                                <table>
                                                    <thead>
                                                        <tr>
                                                            <td width="20%" class='titulos' style="font: 120% sans-serif; padding-left:10px; border-radius: 5px 0 0 0;">C&oacute;digo</td>
                                                            <td class='titulos' style="font: 120% sans-serif; ">Nombre</td>
                                                            <td width="20%" class='titulos' style="font: 120% sans-serif; ">Valor</td>

                                                        </tr>
                                                    </thead>
                                                </table>
                                            </div>
                                            <div style="margin: 2px 0 0 0; border-radius: 0 0 0 6px; height: 200px; overflow: scroll; overflow-x: hidden; background: white; ">
                                                <table class='inicio inicio--no-shadow'>
                                                    <tbody>
                                                        <?php
                                                            $co ='zebra1';
                                                            $co2='zebra2';
                                                        ?>
                                                        <tr v-for="bienTransportable in bienesTrasportables" v-on:click="seleccionarBienTransportable(bienTransportable)"  v-on:dblclick= "continuar('2')" v-bind:class="bienTransportable[0] === bienTransportable_p ? 'background_active_color' : ''" class='<?php echo $co; ?>' style='text-rendering: optimizeLegibility; cursor: pointer !important; style=\"cursor: hand\"'>
                                                            <td width="20%" style="font: 120% sans-serif; padding-left:10px">{{ bienTransportable[0] }}</td>
                                                            <td style="font: 120% sans-serif; padding-left:10px">{{ bienTransportable[1] }}</td>
                                                            <td width="20%" style="font: 120% sans-serif; padding-left:10px">
                                                                
                                                            {{ formatonumero(bienTransportableServicio[bienTransportable[0]+''+fuente_p]) }}

                                                            </td>

                                                            <?php
                                                            $aux=$co;
                                                            $co=$co2;
                                                            $co2=$aux;
                                                            ?>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
										<div class="modal-footer">
											<button type="button" class="btn btn-primary" v-on:click="continuar('2')">Continuar</button>
											<button type="button" class="btn btn-secondary" @click="showModal_bienes_transportables = false">Cerrar</button>
										</div>
									</div>
								</div>
							</div>
						</div>
					</transition>
				</div>

                <div v-show="showModal_servicios">
					<transition name="modal">
						<div class="modal-mask">
							<div class="modal-wrapper">
								<div class="modal-dialog modal-lg" style = "max-width: 100% !important;" role="document">
									<div class="modal-content"  style = "width: 100% !important;" scrollable>
										<div class="modal-header">
											<h5 class="modal-title">CPC - Servicios</h5>
											<button type="button" class="close" data-dismiss="modal" aria-label="Close">
												<span aria-hidden="true" @click="showModal_servicios = false">&times;</span>
											</button>
										</div>
                                        <div class="modal-body">
                                            <div style="margin: 2px 0 0 0;">
                                                <div class="row" style="margin: 4px; border-radius:4px; background-color: #E1E2E2; ">
                                                    <div class="col-md-3" style="padding: 12px 0px 0px 30px; font: 140% sans-serif;">
                                                        <label for="">Servicio:</label>
                                                    </div>

                                                    <div class="col-md-6 col-md-offset-6" style="padding: 4px">
                                                        <input type="text" class="form-control" placeholder="Buscar por nombre o c&oacute;digo del bien transportable" v-on:keyup="searchServicios" v-model="searchGeneralServicio.keywordGeneralServicio">
                                                    </div>
                                                </div>
                                                <table>
                                                    <thead>
                                                        <tr>
                                                            <td width="20%" class='titulos' style="font: 120% sans-serif; padding-left:10px; border-radius: 5px 0 0 0;">C&oacute;digo</td>
                                                            <td class='titulos' style="font: 120% sans-serif; ">Nombre</td>
                                                            <td width="20%" class='titulos' style="font: 120% sans-serif; ">Valor</td>

                                                        </tr>
                                                    </thead>
                                                </table>
                                            </div>
                                            <div style="margin: 2px 0 0 0; border-radius: 0 0 0 6px; height: 200px; overflow: scroll; overflow-x: hidden; background: white; ">
                                                <table class='inicio inicio--no-shadow'>
                                                    <tbody>
                                                        <?php
                                                            $co ='zebra1';
                                                            $co2='zebra2';
                                                        ?>
                                                        <tr v-for="servicio in servicios" v-on:click="seleccionarServicio(servicio)"  v-on:dblclick= "continuar('3')" v-bind:class="servicio[0] === servicio_p ? 'background_active_color' : ''" class='<?php echo $co; ?>' style='text-rendering: optimizeLegibility; cursor: pointer !important; style=\"cursor: hand\"'>
                                                            <td width="20%" style="font: 120% sans-serif; padding-left:10px">{{ servicio[0] }}</td>
                                                            <td style="font: 120% sans-serif; padding-left:10px">{{ servicio[1] }}</td>
                                                            <td width="20%" style="font: 120% sans-serif; padding-left:10px">
                                                                {{ formatonumero(bienTransportableServicio[servicio[0]+''+fuente_p]) }}
                                                            </td>

                                                            <?php
                                                            $aux=$co;
                                                            $co=$co2;
                                                            $co2=$aux;
                                                            ?>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
										<div class="modal-footer">
											<button type="button" class="btn btn-primary" v-on:click="continuar('3')">Continuar</button>
											<button type="button" class="btn btn-secondary" @click="showModal_servicios = false">Cerrar</button>
										</div>
									</div>
								</div>
							</div>
						</div>
					</transition>
				</div>

                <div v-show="showModal_Solo_Presupuesto">
                    <transition name="modal">
                        <div class="modal-mask">
                            <div class="modal-wrapper">
                                <div class="modal-dialog modal-lg" style = "max-width: 1200px !important; height: 600px !important;" role="document">
                                    <div class="modal-content"  style = "width: 100% !important; height: 600px !important;" scrollable>
                                        <div class="modal-header" style="background-color: #E1E2E2; ">

                                            <h5 class="modal-title">Atributos</h5>

                                            <div class="close" style = "opacity: 1; display: flex; gap: 10px;">
                                                <h6>{{  }} </h6>
                                            </div>

                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true" @click="showModal_Solo_Presupuesto = false">&times;</span>
                                            </button>
                                        </div>

                                        <div>
                                            <h6 style="margin: 0px 12px">{{ cuentaActual }} - {{ nombreCuentaActual }}</h6>
                                            <!-- <h6 style="margin: 0px 12px">{{ selectMedioPago == 'CSF' ? 'Con situacion de fondos' : 'Sin situacion de fondos' }}</h6> -->
                                        </div>

                                        <div v-show="bienTransportable_p != ''">
                                            <h6 style="margin: 0px 12px">{{ bienTransportable_p }} - {{ nombreBienTransportable_p }}</h6>
                                        </div>

                                        <div v-show="servicio_p != ''">
                                            <h6 style="margin: 0px 12px">{{ servicio_p }} - {{ nombreServicio_p }}</h6>
                                        </div>
                                        <div v-show="cuin_p != ''">
                                            <h6 style="margin: 0px 12px">{{ cuin_p }} - {{ nombreCuin_p }}</h6>
                                        </div>
                                        <div>
                                            <h6 style="margin: 0px 12px">{{ selectUnidadEjecutora }} - {{ buscaSeccion(selectUnidadEjecutora) }}</h6>
                                        </div>

                                        <div class="modal-body">
                                            <div class="row" style="margin-left: 15px; margin-top: 15px;">
                                                <form class="form-inline">
                                                    <div class="row">
                                                        <div class="col-4">
                                                            <div class="form-group">
                                                                <label for="valorSolo">Situción/Fondo:</label>
                                                                <select v-model="selectMedioPago" v-on:change="buscarIngresos" class="form-control select" style = "font-size: 10px; margin-top:4px">
                                                                    <option v-for="option in optionsMediosPagos" v-bind:value="option.value">
                                                                        {{ option.text }}
                                                                    </option>
                                                                </select>
                                                            </div>

                                                        </div>
                                                        <div class="col-6">
                                                            <div class="form-group w-100">
                                                                <label for="valorSolo">Valor (Presupuesto inicial):</label>
                                                                <input type="number" v-model="valorSolo" v-on:keydown.enter.prevent="guardarValorSolo" class="form-control form-control-sm mx-sm-3">
                                                                {{ formatonumero(valorSolo) }}
                                                            </div>
                                                        </div>
                                                    </div>
                                                    

                                                    
                                                </form>

                                            </div>

                                            <div class="row d-flex" style="width: 100%; margin: 10px 0 0 0;">
                                                <div class="col-6">
                                                    <label class="form-label">Destinación Especifica:</label>
                                                    <select v-model="destEspecifica" class="form-control form-control-sm" v-on:change="valida_destEspecifica">
                                                        <option value='SI'>SI</option>
                                                        <option value='NO'>NO</option>
                                                    </select>
                                                </div>
                                                <div class="col-6">
                                                    <div v-show="show_destEspecifica">
                                                        <label class="form-label">Tipo de Norma:</label>
                                                        <select class="form-control form-control-sm"  v-model="tiponorma">
                                                            <option v-for="SeltNorma in tNorma" :value="SeltNorma[0]">{{ SeltNorma[0] }} - {{ SeltNorma[1] }}</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                
                                                
                                            </div>
                                            <div v-show="show_destEspecifica">
                                                <!-- <div class="row d-flex" style=" margin: 10px 0 0 0;">
                                                    <div class="col-10">
                                                        <multiselect
                                                            v-model="valueFuentes"
                                                            placeholder="Seleccione una fuente"
                                                            label="fuente" track-by="fuente"
                                                            :custom-label="fuenteConNombre"
                                                            :options="optionsFuentes"
                                                            :multiple="false"
                                                            :taggable="false"
                                                            @open="cambiaEstadoDetalleDestinacion"
                                                            @close="cambiaEstadoDetalleDestinacion"
                                                            class="w-100"
                                                        ></multiselect>
                                                    </div>
                                                </div> -->
                                            

                                                <div class="row d-flex" style="width: 100%; margin: 10px 0 0 0;">
                                                    <div class="col-6">
                                                        
                                                        <label class="form-label">Número de norma:</label>
                                                        <input class="form-control" v-model="numeroNorma" type="text"  placeholder="Número de la norma">
                                                        
                                                    </div>

                                                    <div class="col-6">
                                                        <label class="form-label">Fecha de norma:</label>
                                                        <input class="form-control" type="date" v-model="txtFecha">
                                                    </div>
                                                    
                                                </div>

                                                

                                                <!-- <div class="row d-flex" style="width: 100%; margin: 10px 0 0 0;">
                                                    <div class="col-4">
                                                            <label class="form-label">Porcentaje:</label>
                                                            <input class="form-control" v-model="valorPorcentaje" type="text" placeholder="Porcentaje">
                                                    </div>

                                                    <div class="col-4">
                                                        <label class="form-label">Valor:</label>
                                                        <select class="form-control" v-model="tipoValor">
                                                            <option value='0'>Porcentaje</option>
                                                            <option value='1'>Valor</option>
                                                        </select>
                                                    </div>

                                                    <div class="col-4">
                                                        <label class="form-label">Acción:</label>
                                                        <div class="form-control">
                                                            <button 
                                                                type="button" 
                                                                class="btn btn-primary btn-sm" 
                                                                @click="agregarDetalleSectorial" 
                                                                >
                                                                Agregar Auxiliar
                                                            </button>
                                                        </div>
                                                        
                                                    </div>
                                                    
                                                </div> -->
                                            </div>
                                            <div class="row d-flex" style="width: 100%; margin: 10px 0 0 0;">
                                                <div class="col-12">
                                                    <label class="form-label">Detalle Sectorial:</label>
                                                    <select class="form-control form-control-sm" v-model="detSectorial" >
                                                        <option v-for="SeldetSecto in detSecto" :value="SeldetSecto[0]">{{ SeldetSecto[0] }} - {{ SeldetSecto[1] }}</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="row d-flex" style="width: 100%; margin: 10px 0 0 0;">
                                                <div class="col-12">
                                                    <label class="form-label">Tercero:</label>
                                                </div>
                                                <div class="col-3">
                                                    <input class="form-control" v-on:dblclick="buscaVentanaModal('1')" v-model="cuin_p" type="text"  style='text-rendering: optimizeLegibility; cursor: pointer !important; text-align:center; background-color: dbeafe; border-radius: 2px;'>
                                                </div>
                                                <div class="col-9">
                                                    <input class="form-control" v-model="nombreCuin_p" type="text"  readonly>
                                                    <!-- {{ this.nombreCuin_p == '' ? 'No Plica' : this.nombreCuin_p }} -->
                                                </div>
                                            </div>
                                            <!-- <div style="margin: 2px 0 0 0;" v-show="mostrarDetalleDestinacion">
                                                <div class="row d-flex justify-content-end" 
                                                    style="margin: 4px; border-radius: 4px; background-color: #E1E2E2;">
                                                    
                                                </div>
                                                <table>
                                                    <thead>
                                                        <tr>
                                                            <td width="20%" class='titulos' style=" width:20%; font: 120% sans-serif; padding-left:10px; border-radius: 5px 0 0 0;">Rubro</td>
                                                            <td class='titulos' style="font: 120% sans-serif; ">Fuente</td>
                                                            <td class='titulos' style="font: 120% sans-serif; ">Tipo </td>
                                                            <td class='titulos' style="font: 120% sans-serif; ">Número </td>
                                                            <td class='titulos' style="font: 120% sans-serif; ">Fecha </td>
                                                            <td class='titulos' style="font: 120% sans-serif; ">Porcentaje </td>
                                                            <td class="titulos" style=" width:10%; font: 120% sans-serif; ">Quitar</td>
                                                        </tr>
                                                    </thead>
                                                    <tbody>

                                                        <?php
                                                            $co ='zebra1';
                                                            $co2='zebra2';
                                                        ?>

                                                        <tr v-for="(destinacion, index) in destinacionesEspecificas" style="font-size: large;" class='<?php echo $co; ?>'>
                                                            <td style="width:20%; padding-left: 10px; border-right: 1px solid black">{{ destinacion[6] }}</td>
                                                            <td  style="border-right: 1px solid black">
                                                                {{ destinacion[4] }}
                                                            </td>
                                                            <td style="border-right: 1px solid black"> {{ destinacion[1]}} </td>
                                                            <td style="border-right: 1px solid black"> {{ destinacion[2]}} </td>
                                                            <td style="border-right: 1px solid black"> {{ destinacion[3]}} </td>
                                                            <td style="border-right: 1px solid black"> {{ destinacion[5]}} </td>
                                                            <td style="width:10%; text-align:center;">
                                                                <button type="button" class="btn btn-danger btn-sm" @click="quitarAuxiliar(rubroSel)">Quitar</button>
                                                            </td>
                                                            <?php
                                                            $aux=$co;
                                                            $co=$co2;
                                                            $co2=$aux;
                                                            ?>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div> -->
                                            
                                            <!-- <div style="margin: 2px 0 0 0; border-radius: 0 0 0 6px; height: 400px; overflow: scroll; overflow-x: hidden; background: white; " v-show="mostrarDetalleDestinacion">
                                                <table class='inicio inicio--no-shadow'>
                                                    
                                                </table>
                                            </div> -->
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-primary" v-on:click="guardarValorSolo">Guardar y Continuar</button>
                                            <button type="button" class="btn btn-secondary" @click="showModal_Solo_Presupuesto = false">Cerrar</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </transition>
                </div>

				<!-- <div v-show="showModal_Solo_Presupuesto">
					<transition name="modal">
						<div class="modal-mask">
							<div class="modal-wrapper">
								<div class="modal-dialog modal-lg" role="document">
									<div class="modal-content"  style = "width: 700px !important;" scrollable>
										<div class="modal-header">
                                            <div class="row">
                                                <div class="col-10">
                                                    <h5 class="modal-title">Asignar presupuesto</h5>
                                                </div>
                                                <div class="col-2">
                                                    <button type="button" class="btn btn-secondary" v-on:click="buscarFuente()">Volver</button>
                                                </div>
                                            </div>
											<button type="button" class="close" data-dismiss="modal" aria-label="Close">
												<span aria-hidden="true" @click="showModal_Solo_Presupuesto = false">&times;</span>
											</button>
										</div>
                                        <div class="modal-body">
                                            <div class="modal-intermedio-agregar">
                                                <div class="row" style="margin-left: 15px;">
                                                    <form class="form-inline">
                                                        <div class="form-group">
                                                            <label for="valorSolo">Valor:</label>
                                                            <input type="number" v-model="valorSolo" v-on:keydown.enter.prevent="guardarValorSolo" class="form-control mx-sm-3">
                                                            {{ formatonumero(valorSolo) }}
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
										<div class="modal-footer">
											<button type="button" class="btn btn-primary" v-on:click="guardarValorSolo">Guardar y Continuar</button>
											<button type="button" class="btn btn-secondary" @click="showModal_Solo_Presupuesto = false">Cerrar</button>
										</div>
									</div>
								</div>
							</div>
						</div>
					</transition>
				</div> -->
                <!-- <div v-show="showModal_clasificador">
					<transition name="modal">
						<div class="modal-mask">
							<div class="modal-wrapper">
								<div class="modal-dialog modal-lg" style = "max-width: 1200px !important;" role="document">
									<div class="modal-content" style = "width: 1200px !important;" scrollable>
										<div class="modal-header">
											<h5 class="modal-title">Clasificador </h5>
											<button type="button" class="close" data-dismiss="modal" aria-label="Close">
												<span aria-hidden="true" @click="showModal_clasificador = false">&times;</span>
											</button>
										</div>
										<div class="modal-body">

											<div v-if="!show_table_search" class="loader-table">
												<div class="spinner"></div>
											</div>
											<table v-if="show_table_search" class="table table-hover">
												<thead>
													<tr>
														<th>C&oacute;digo</th>
														<th>Nombre</th>
														<th>tipo</th>
													</tr>
												</thead>
												<tbody>
													<tr v-for="clasificador in resultsClasificador" v-on:click="seleccionarClasificador(clasificador)" :class=" clasificador[2] == 'A' ? 'background_active_clasificador' : '' " v-bind:class="clasificador[0] === clasificador_p ? 'background_active_color' : ''" style='font-weight: normal; text-rendering: optimizeLegibility; cursor: pointer !important; style=\"cursor: hand\"'>
														<td>{{ clasificador[0] }}</td>
														<td>{{ clasificador[1] }}</td>
														<td>{{ clasificador[2] }}</td>
													</tr>
												</tbody>
											</table>
										</div>
										<div class="modal-intermedio-agregar">
											<div class="row" style="margin-left: 15px;">
												<form class="form-inline">
													<div class="form-group">
														<label for="valorClasificador">Valor:</label>
														<input type="number" v-model="valorClasificador" class="form-control mx-sm-3">

														<button type="button" class="btn btn-primary" v-on:click="agregaClasificador">Agregar cuenta</button>

													</div>
												</form>

												<form class="form-inline" style='padding-left:10px'>
													<div class="form-group">
															<label for="valorTotalClasificador">Valor Total:</label>
															<input type="number" v-model="valorTotalClasificador" class="form-control mx-sm-3" readonly>
													</div>
												</form>

											</div>

										</div>
										<div class="modal-intermetio">
											<div class="row">
												<div class="col-12" style="text-align:center; background: #E1E2E2;">
													<h5 class="modal-title">Cuentas seleccionadas</h5>
												</div>
											</div>
										</div>
										<div class="modal-body" style='height:140px; paddind: 0px'>
											<table v-if="show_table_search" class="table table-hover" >
												<thead>
													<tr>
														<th>C&oacute;digo</th>
														<th>Nombre</th>
														<th>Valor</th>
														<th>Eliminar</th>
													</tr>
												</thead>
												<tbody>
													<tr v-for="cuenta in cuentasClasificadorAgr" style="font-weight: normal">
														<td>{{ cuenta[0] }}</td>
														<td>{{ cuenta[1] }}</td>
														<td>{{ cuenta[3] }}</td>
														<td>
															<button type="button" class="btn btn-danger" v-on:click="eliminarClasificador(cuenta)">Eliminar</button>
														</td>
													</tr>
												</tbody>
											</table>
										</div>
										<div class="modal-footer">
											<button type="button" class="btn btn-primary" v-on:click="guardarClasificador">Guardar y continuar</button>
											<button type="button" class="btn btn-secondary" @click="showModal_clasificador = false">Cerrar</button>
										</div>
									</div>
								</div>
							</div>
						</div>
					</transition>
				</div> -->
				<span id="end_page"> </span>
			</div>

		<!-- <script src="Librerias/vue/vue.min.js"></script> -->
        <script src="https://cdn.jsdelivr.net/npm/vue@2.5.16/dist/vue.js"></script>
		<script src="Librerias/vue/axios.min.js"></script>
		<script type="module" src="vue/ccp-crearpresupuestoingresos.js"></script>
        <script src="https://unpkg.com/vue-multiselect@2.1.0"></script>
        <link rel="stylesheet" href="multiselect.css">
	</body>
</html>
