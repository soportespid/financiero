<?php
	header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=utf8");
	require "comun.inc";
	require "funciones.inc";
    require "conversor.php";
	require "funcionesSP.inc.php";
	session_start();
	$linkbd = conectar_v7();
	$linkbd -> set_charset("utf8"); 
	cargarcodigopag($_GET['codpag'],$_SESSION['nivel']);
    date_default_timezone_set("America/Bogota");
	titlepag();
			
?>

<!DOCTYPE >

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="es">
	<head>
		<meta http-equiv="Content-type" content="text/html; charset=utf8"/>
		<meta http-equiv="X-UA-Compatible" content="IE=9"/>
		<title>:: IDEAL 10 - Servicios P&uacute;blicos</title>
		<link href="css/css2.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<link href="css/css4.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<link href="css/cssSP.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<link href="css/tabs.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<script type="text/javascript" src="css/calendario.js"></script>
        <script type="text/javascript" src="css/programas.js"></script>
		<script type="text/javascript" src="jquery-1.11.0.min.js"></script> 
		<script type="text/javascript" src="JQuery/autoNumeric-master/autoNumeric-min.js"></script>
		<script src="sweetalert2/dist/sweetalert2.min.js"></script>
		<link rel="stylesheet" href="sweetalert2/dist/sweetalert2.min.css">
        
		<script>
			function pdf()
			{
				document.form2.action="serv-pdf-acuerdoPago.php";
				document.form2.target="_BLANK";
				document.form2.submit(); 
				document.form2.action="";
				document.form2.target="";
			}

			function funcionmensaje()
			{
				var x = window.location.search;
				location.href = "serv-visualizar-acuerdoPago.php"+x;
            }

			function pagarAcuerdo()
			{
				var estadoFacturaActual = document.getElementById('estadoFacturaActual').value;

				if(estadoFacturaActual != 'S') {
					
					Swal.fire({
						icon: 'question',
						title: 'Seguro que quieres pagar todo el acuerdo?',
						showDenyButton: true,
						confirmButtonText: 'Pagar',
						denyButtonText: 'Cancelar',
						}).then((result) => {
						if (result.isConfirmed) {
							document.form2.oculto.value='2';
							document.form2.submit();
						} else if (result.isDenied) {
							Swal.fire('pago cancelado', '', 'info')
						}
					})
				}
				else {
					Swal.fire("Error", "Debe tener la factura vigente con pago", "error");
				}
			}
        </script>
                
	</head>
	<body>
		<IFRAME src="alertas.php" name="alertas" id="alertas" style="display:none"></IFRAME>
		<span id="todastablas2"></span>
		<table>
			<tr>
                <script>barra_imagenes("serv");</script><?php cuadro_titulos();?>
            </tr>

			<tr><?php menu_desplegable("serv");?></tr>
			<tr>
				<td colspan="3" class="cinta">
					<a href="serv-crear-acuerdos-pago.php" class="mgbt"><img src="imagenes/add.png"/></a>
					<a onclick="" class="mgbt"><img src="imagenes/guarda.png" title="Guardar"/></a>
					<a href="serv-buscar-acuerdosPago.php" class="mgbt"><img src="imagenes/busca.png" title="Buscar"/></a>
					<a onclick="mypop=window.open('plan-agenda.php','','');mypop.focus()" class="mgbt"><img src="imagenes/agenda1.png" title="Agenda" /></a>
					<a onclick="mypop=window.open('serv-principal.php','','');mypop.focus();" class="mgbt"><img src="imagenes/nv.png" title="Nueva ventana"></a>
					<a onclick="mypop=window.open('<?php echo $url2; ?>','','');mypop.focus();" class="mgbt"><img src="imagenes/duplicar_pantalla.png" title="Duplicar pesta&ntilde;a"></a>
                    <a onClick="pdf()" class="mgbt"><img src="imagenes/print.png" style="width:29px;height:25px;" title="Imprimir" /></a>
                    <a href="serv-buscar-acuerdosPago.php" class="mgbt"><img src="imagenes/iratras.png" title="Atrás"></a>
                    <a onclick="pagarAcuerdo();" class="mgbt"><img src="imagenes/abonos.png" title="Pagar acuerdo"></a>
                </td>
			</tr>
        </table>
        
		<div id="bgventanamodalm" class="bgventanamodalm">
			<div id="ventanamodalm" class="ventanamodalm">
				<IFRAME src="" name="ventanam" marginWidth=0 marginHeight=0 frameBorder=0 id="ventanam" frameSpacing=0 style=" width:700px; height:130px; top:200; overflow:hidden;"></IFRAME>
			</div>
		</div>

		<form name="form2" method="post" action="">
			<?php
				if(@ $_POST['oculto'] == '')
				{
					$codigoAcuerdo = $_GET['codAcu'];
                    
                    $sqlAcuerdo = "SELECT AC.codigo_acuerdo, AC.id_cliente, AC.cod_usuario, AC.documento, AC.nombre_suscriptor, AC.fecha_acuerdo, AC.codigo_factura, AC.valor_factura, AC.valor_abonado, AC.valor_acuerdo, AC.valor_cuota, AC.numero_cuotas, AC.cuotas_liquidadas, AC.estado_acuerdo, AC.numero_cuenta, AC.medio_pago, AC.valor_liquidado FROM srv_acuerdo_cab AS AC WHERE AC.codigo_acuerdo = $codigoAcuerdo";
                    $rowAcuerdo = mysqli_fetch_assoc(mysqli_query($linkbd, $sqlAcuerdo));

                    $_POST['numeroAcuerdo'] = $rowAcuerdo['codigo_acuerdo'];
                    $_POST['codUsuario'] = $rowAcuerdo['cod_usuario'];
                    $_POST['nombreSuscriptor'] = $rowAcuerdo['nombre_suscriptor'];
                    $_POST['documento'] = $rowAcuerdo['documento'];
                    $_POST['fecha'] = date('d-m-Y',strtotime($rowAcuerdo['fecha_acuerdo']));
                    $_POST['modoRecaudo'] = $rowAcuerdo['medio_pago'];
                    $_POST['cuentaRecaudo'] = $rowAcuerdo['numero_cuenta'];
                    $_POST['numeroFactura'] = $rowAcuerdo['codigo_factura'];
                    $_POST['valorFacturaVisible'] = "$".number_format($rowAcuerdo['valor_factura'],2);
                    $_POST['valorFactura'] = $rowAcuerdo['valor_factura'];
                    $_POST['valorAbono'] = "$".number_format($rowAcuerdo['valor_abonado'],2);
                    $_POST['valorAcuerdoVisible'] = "$".number_format($rowAcuerdo['valor_acuerdo'],2);
					$_POST['valorAcuerdo'] = $rowAcuerdo['valor_acuerdo'];
                    $_POST['numeroCuotas'] = $rowAcuerdo['numero_cuotas'];
                    $_POST['valorCuota'] = "$".number_format($rowAcuerdo['valor_cuota'],2);
                    $_POST['cuotasFacturadas'] = $rowAcuerdo['cuotas_liquidadas'];
                    $_POST['valorFacturado'] = "$".number_format($rowAcuerdo['valor_liquidado'],2);
                    $_POST['estadoAcuerdo'] = $rowAcuerdo['estado_acuerdo'];
                    $_POST['concepto'] = "Acuerdo de pago de la factura numero ".$_POST['numeroFactura']. " del codigo de usuario ".$_POST['codUsuario'];
                    $_POST['letras'] = convertirdecimal($_POST['valorFactura'],'.');

                    $sqlCorte = "SELECT id_corte FROM srvcortes_detalle WHERE numero_facturacion = $_POST[numeroFactura]";
                    $rowCorte = mysqli_fetch_row(mysqli_query($linkbd,$sqlCorte));

                    $sql = "SET lc_time_names = 'es_ES'";
                    mysqli_query($linkbd,$sql);

                    $sqlr = "SELECT numero_corte, UPPER(MONTHNAME(fecha_inicial)), UPPER(MONTHNAME(fecha_final)), YEAR(fecha_inicial), YEAR(fecha_final) FROM srvcortes WHERE numero_corte = $rowCorte[0]";
                    $resp = mysqli_query($linkbd,$sqlr);
                    $row = mysqli_fetch_row($resp);
                    
                    $corte = $row[0]." - " .$row[1]. " " .$row[3]. " - " .$row[2]. " " .$row[4];
                    
                    $_POST['descripcion'] = "Valor de la factura numero $_POST[numeroFactura] del corte liquidado $corte";

					$corteUltimo = selconsecutivo('srvcortes', 'numero_corte');
					$corteUltimo = $corteUltimo - 1;

					$sqlEstadoFactura = "SELECT estado_pago FROM srvcortes_detalle WHERE id_cliente = $rowAcuerdo[id_cliente] AND id_corte = $corteUltimo";
                    $rowEstadoFactura = mysqli_fetch_row(mysqli_query($linkbd,$sqlEstadoFactura));

					$_POST['estadoFacturaActual'] = $rowEstadoFactura[0];
				}
            ?>

			<div>
				<table class="inicio">
					<tr>
						<td class="titulos" colspan="7">Acuerdo de Pago</td>
						<td class="cerrar" style="width:7%" onClick="location.href='serv-principal.php'">Cerrar</td>
					</tr>
				</table>

				<div class="subpantalla" style="height:40%; width:99.5%; float:left; overflow: hidden;">
					<table class="inicio grande">
                        <tr>
                            <td class="titulos" colspan="9">Datos Acuerdo: </td>
                        </tr>
						<tr>
                            <td class="tamano01" style="width: 10%;">Código de Acuerdo: </td>
                            <td style="width: 15%">
                                <input type="text" name="numeroAcuerdo" id="numeroAcuerdo" value="<?php echo $_POST['numeroAcuerdo'] ?>" style="text-align:center; width: 100%;" readonly/>
								<input type="hidden" name="porcentaje_abono" id="porcentaje_abono" value="<?php echo $_POST['porcentaje_abono'] ?>">
                            </td>

							<td class="tamano01" style="width: 10%;">Código Usuario:</td> 
							<td style="width: 15%">
								<input type="text" name='codUsuario' id='codUsuario' value="<?php echo @$_POST['codUsuario'];?>" style="text-align:center; width: 100%;" readonly/>

								<input type="hidden" name="id_cliente" id="id_cliente" value="<?php echo $_POST['id_cliente'] ?>">
							</td>

							<td class="tamano01" style="width: 10%;">Nombre del Suscriptor: </td>
							<td style="width: 19%">
								<input type="text" name="nombreSuscriptor" id="nombreSuscriptor" value="<?php echo @ $_POST['nombreSuscriptor'];?>" style="text-align: center; width: 100%;" readonly/>
							</td>

							<td class="tamano01" style="width: 10%;">Documento Suscriptor: </td>
							<td>
								<input type="text" name="documento" id="documento" value="<?php echo @ $_POST['documento'];?>" style="text-align: center;" readonly/>
							</td>
						</tr>

						<tr>
							<td class="tamano01" style="width:10%;">Fecha:</td>
							<td style="width:15%;">
								<input type="text" name="fecha" id="fecha" value="<?php echo @ $_POST['fecha']?>" style="height:30px;text-align:center;width:100%;" readonly/>
							</td>

							<td class="tamano01">Recaudo En:</td>
							<td>
								<input type="text" name="modoRecaudo" id="modoRecaudo" value="<?php echo @ $_POST['modoRecaudo']?>" style="text-align:center;width:100%;" readonly>
							</td>
							
							<td class="tamano01">Número de cuenta:</td>
                            <td>
                                <input  type="text" name="cuentaRecaudo" id="cuentaRecaudo" value="<?php echo @ $_POST['cuentaRecaudo'];?>" style="width:100%;height:30px;text-align:center;" readonly/>
                            </td>

                            <td class="tamano01">Estado de acuerdo:</td>
                            <td>
                                <input  type="text" name="estadoAcuerdo" id="estadoAcuerdo" value="<?php echo @ $_POST['estadoAcuerdo'];?>" style="width:100%;height:30px;text-align:center;" readonly/>
                            </td>
						</tr>

						<tr>
							<td class="tamano01">Numero Factura: </td>
							<td>
								<input type="text" name="numeroFactura" id="numeroFactura" value="<?php echo $_POST['numeroFactura'] ?>" style="text-align:center; width: 100%;" readonly>	
							</td>

							<td class="tamano01">Valor Factura: </td>
							<td>
								<input type="text" name="valorFacturaVisible" id="valorFacturaVisible" value="<?php echo $_POST['valorFacturaVisible'] ?>" style="text-align:center; width: 100%;" readonly>
								<input type="hidden" name="valorFactura" id="valorFactura" value="<?php echo $_POST['valorFactura'] ?>" >	
							</td>
						</tr>

						<tr>
							<td class="tamano01">Valor Abono: </td>
							<td>
								<input type="text" id="valorAbono" name="valorAbono" value="<?php echo $_POST['valorAbono']?>" style="text-align:center;width:100%;" readonly>
							</td>

							<td class="tamano01">Valor Acuerdo: </td>
							<td>
								<input type="text" name="valorAcuerdoVisible" id="valorAcuerdoVisible" value="<?php echo $_POST['valorAcuerdoVisible'] ?>" style="text-align:center; width: 100%;" readonly>
								<input type="hidden" name="valorAcuerdo" id="valorAcuerdo" value="<?php echo $_POST['valorAcuerdo'] ?>" >
							</td>
						</tr>

						<tr>
							<td class="tamano01">Numero de cuotas: </td>
							<td>
								<input type="text" name="numeroCuotas" id="numeroCuotas" value="<?php echo $_POST['numeroCuotas'] ?>" style="text-align:center; width: 100%;" readonly>
							</td>

							<td class="tamano01">Valor de cuota: </td>
							<td>
								<input type="text" name="valorCuota" id="valorCuota" value="<?php echo $_POST['valorCuota'] ?>" style="text-align:center;width:100%;" readonly>
							</td>

                            <td class="tamano01">Cuotas facturadas: </td>
							<td>
								<input type="text" name="cuotasFacturadas" id="cuotasFacturadas" value="<?php echo $_POST['cuotasFacturadas'] ?>" style="text-align:center;width:100%;" readonly>
							</td>

                            <td class="tamano01">Valor facturado: </td>
							<td>
								<input type="text" name="valorFacturado" id="valorFacturado" value="<?php echo $_POST['valorFacturado'] ?>" style="text-align:center;width:100%;" readonly>
							</td>
						</tr>
					</table>
				</div>	
			</div>	
			<?php

				if($_POST['oculto'] == '2')
				{
					$sqlAcuerdo = "UPDATE srv_acuerdo_cab SET valor_liquidado = $_POST[valorAcuerdo], estado_acuerdo = 'Finalizado' WHERE codigo_acuerdo = $_POST[numeroAcuerdo]";
					if(mysqli_query($linkbd, $sqlAcuerdo));
					{
						echo "
							<script>
								Swal.fire({
									icon: 'success',
									title: 'Guardado exitoso!',
									}).then((result) => {
									if (result.value) {
										funcionmensaje();
									} 
								})
							</script>";
					}
				}
			?>

			<input type="hidden" name="cambio" id="cambio" value="1"/>
			<input type="hidden" name="oculto" id="oculto" value="1"/>
            <input type="hidden" name="concepto" id="concepto" value="<?php echo $_POST['concepto'] ?>"/>
            <input type="hidden" name="letras" id="letras" value="<?php echo $_POST['letras'] ?>"/>
            <input type="hidden" name="descripcion" id="descripcion" value="<?php echo $_POST['descripcion'] ?>"/>
			<input type="hidden" name="estadoFacturaActual" id="estadoFacturaActual" value="<?php echo $_POST['estadoFacturaActual'] ?>"/>
			
			<div id="bgventanamodal2">
				<div id="ventanamodal2">
					<IFRAME  src="" name="buster" marginWidth=0 marginHeight=0 frameBorder=0 id="ventana2" frameSpacing=0 style="left:500px; width:900px; height:500px; top:200;"></IFRAME>
				</div>
			</div>
			
			
        </form>                          
    </body>   
</html>
