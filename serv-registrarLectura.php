<?php
header("Cache-control: no-cache, no-store, must-revalidate");
header("Content-Type: text/html;charset=iso-8859-1");
require "comun.inc";
require "funciones.inc";
require "funcionesSP.inc.php";
session_start();
$linkbd = conectar_v7();
cargarcodigopag($_GET['codpag'],$_SESSION["nivel"]);
date_default_timezone_set("America/Bogota");
?>
<!DOCTYPE >
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>:: IDEAL 10 - Servicios P&uacute;blicos</title>
    <link href="css/css2.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css"/>
    <link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css"/>
    <link href="css/css4.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css"/>
    <link href="css/cssSP.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css"/>
    <script type="text/javascript" src="css/calendario.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
    <script type="text/javascript" src="css/programas.js"></script>

    <style>
    </style>

    <script>
        function despliegamodalm(_valor,_tip,mensa,pregunta)
        {
            document.getElementById("bgventanamodalm").style.visibility=_valor;

            if(_valor=="hidden")
            {
                document.getElementById('ventanam').src="";
            }
            else
            {
                switch(_tip)
                {
                    case "1":	document.getElementById('ventanam').src="ventana-mensaje1.php?titulos="+mensa;break;
                    case "2":	document.getElementById('ventanam').src="ventana-mensaje3.php?titulos="+mensa;break;
                    case "3":	document.getElementById('ventanam').src="ventana-mensaje2.php?titulos="+mensa;break;
                    case "4":	document.getElementById('ventanam').src="ventana-consulta1.php?titulos="+mensa+"&idresp="+pregunta;break;	
                }
            }
        }

        function funcionmensaje()
        {
            document.location.href = "serv-registrarLectura.php";
        }

        function respuestaconsulta(pregunta)
        {
            switch(pregunta)
            {
                case "1":	
                    document.form2.generarLista.value = '2';
                    document.form2.oculto.value='2';
                    document.form2.submit();
                break;
            }
        }

        function guardar()
        {
            despliegamodalm('visible','4','Esta seguro de guardar?','1');
        }

		function generarListaClientes()
		{
			var corte = document.getElementById('corte').value;
			var servicio = document.getElementById('id_servicio').value;
			var fecha = document.getElementById('fecha').value;
			var corteLectura = document.getElementById('corteLectura').value;

			if(corte == corteLectura)
			{
				if(servicio != '-1')
				{
					if(fecha != '')
					{
						document.form2.generarLista.value = '2';
						document.form2.submit();
					}
					else
					{
						despliegamodalm('visible', '2', '¡Error! Seleccione una fecha');
					}
				}
				else
				{
					despliegamodalm('visible', '2', '¡Error! Seleccione un servicio');
				}
			}
			else
			{
				despliegamodalm('visible', '2', 'El corte no es el correcto, comunicarse con soporte');
			}
		}
    </script>
</head>

    <?php titlepag();?>
    
<body>
    <IFRAME src="alertas.php" name="alertas" id="alertas" style="display:none"></IFRAME>
    <span id="todastablas2"></span>
    <table>
        <tr><script>barra_imagenes("serv");</script><?php cuadro_titulos();?></tr>
        <tr><?php menu_desplegable("serv");?></tr>
        <tr>
            <td colspan="3" class="cinta">
                <a href="" class="mgbt"><img src="imagenes/add.png"/></a>

                <a onclick="guardar()" class="mgbt"><img src="imagenes/guarda.png" title="Guardar"/></a>

                <a href="" class="mgbt"><img src="imagenes/busca.png" title="Buscar"/></a>

                <a onclick="mypop=window.open('plan-agenda.php','','');mypop.focus()" class="mgbt"><img src="imagenes/agenda1.png" title="Agenda" /></a>

                <a onclick="mypop=window.open('serv-principal.php','','');mypop.focus();" class="mgbt"><img src="imagenes/nv.png" title="Nueva ventana"></a>

                <a onclick="mypop=window.open('<?php echo $url2; ?>','','');mypop.focus();" class="mgbt"><img src="imagenes/duplicar_pantalla.png" title="Duplicar pesta&ntilde;a"></a>
            </td>
        </tr>
    </table>

    <div id="bgventanamodalm" class="bgventanamodalm">
        <div id="ventanamodalm" class="ventanamodalm">
            <IFRAME src="" name="ventanam" marginWidth=0 marginHeight=0 frameBorder=0 id="ventanam" frameSpacing=0 style=" width:700px; height:130px; top:200; overflow:hidden;"></IFRAME>
        </div>
    </div>

    <form name="form2" method="post" action="">
        <?php
            $vigusu=vigencia_usuarios($_SESSION['cedulausu']);

            if(@$_POST['oculto'] == '')
            {
               $_POST['corte'] = siguienteCorte();
			   $_POST['corteLectura'] = siguienteCorte();
            }
        ?>
        
        <div>
            <table class="inicio">
                <tr>
					<td class="titulos" colspan="7">.: Registro de Lecturas</td>

					<td class="cerrar" style="width:7%" onclick="location.href='serv-principal.php'">Cerrar</td>
				</tr>

                <tr>
					<td class="tamano01" style="width: 3cm;">Corte: </td>

					<td style="width: 15%;">
						<input type="text" name="corte" id="corte" value="<?php echo $_POST['corte']?>" style="height: 30px; width: 98%; text-align:center;" readonly>
					</td>

					<td class="tamano01" style="width: 3cm;">Servicio: </td>

					<td>
						<select name="id_servicio" id="id_servicio" value="<?php echo $_POST['id_servicio']?>" class="centrarSelect" style="height: 30px; width: 98%;">
							<option class="aumentarTamaño" value="-1">:: SELECCIONE SERVICIO ::</option>
							<?php
								$sql = "SELECT id, nombre FROM srvservicios";
								$res = mysqli_query($linkbd,$sql);
								while($row = mysqli_fetch_assoc($res))
								{
									if(@$_POST['id_servicio'] == $row['id'])
									{
										echo "<option class='aumentarTamaño' value='$row[id]' selected>$row[nombre]</option>";
									}
									else
									{
										echo "<option class='aumentarTamaño' value='$row[id]'>$row[nombre]</option>";
									}
								}
							?>
						</select>
					</td>

					<td class="tamano01" style="width: 3cm;">Fecha:</td>

					<td style="width: 15%;">
						<input type="text" name="fecha" value="<?php echo @ $_POST['fecha']?>" maxlength="10" onKeyPress="javascript:return solonumeros(event)" onKeyUp="return tabular(event,this)" id="fecha" onKeyDown="mascara(this,'/',patron,true)" title="DD/MM/YYYY" style="width:80%;height: 30px; text-align:center;" readonly>&nbsp;
                        <img src="imagenes/calendario04.png" onClick="displayCalendarFor('fecha');" title="Calendario" class="icobut"/>
					</td>

					<td>
						<input type="button" name="generar" id="generar" style="height:30px;" value="Generar" onclick="generarListaClientes();">
					</td>
				</tr>
                <input type="hidden" name="oculto" id="oculto" value="1">
                <input type="hidden" name="generarLista" id="generarLista" value="1">
                <input type="hidden" name="corteLectura" id="corteLectura" value="<?php echo $_POST['corteLectura'] ?>">
            </table>
        </div>

		<?php 
			if(@$_POST['generarLista'] == '2')
			{
                $sqlLecturas = "SELECT * FROM srvlectura WHERE corte = $_POST[corte] LIMIT 1";
                $resLecturas = mysqli_query($linkbd,$sqlLecturas);
                $rowLecturas = mysqli_fetch_row($resLecturas);

                $crear_editar = 0;

                if(isset($rowLecturas[0]))
                {
                    $crear_editar = 2;
                    $x = 0;

                    $sqlLecturasGuardadas = "SELECT * FROM srvlectura WHERE corte = $_POST[corte]";
                    $resLecturasGuardadas = mysqli_query($linkbd,$sqlLecturasGuardadas);
                    while($rowLecturasGuardadas = mysqli_fetch_row($resLecturasGuardadas))
                    {
                        $sqlClientes = "SELECT cod_usuario FROM srvclientes WHERE id = $rowLecturasGuardadas[2]";
                        $resClientes = mysqli_query($linkbd,$sqlClientes);
                        $rowClientes = mysqli_fetch_row($resClientes);

                        $_POST["lectura$x"] = $rowLecturasGuardadas[5];
                        $_POST['id_cliente'][] = $rowLecturasGuardadas[2];
                        $_POST['cod_usuario'][] = $rowClientes[0];
                        $_POST['nombre_usuario'][] = encuentraNombreTerceroConIdCliente($rowLecturasGuardadas[2]);
                        $_POST['documento_usuario'][] = encuentraDocumentoTerceroConIdCliente($rowLecturasGuardadas[2]);
                        $_POST['direccion_usuario'][] = encuentraDireccionConIdCliente($rowLecturasGuardadas[2]);

                        $x++;
                    }
                }
                else
                {
                    $crear_editar = 1;
                    
                    $sqlServicio = "SELECT id_clientes FROM srvasignacion_servicio WHERE id_servicio = '$_POST[id_servicio]' AND tarifa_medidor = 'N'";
                    $resServicio = mysqli_query($linkbd,$sqlServicio);
                    while($rowServicio = mysqli_fetch_row($resServicio))
                    {
                        $sqlClientes = "SELECT cod_usuario FROM srvclientes WHERE id = $rowServicio[0]";
                        $resClientes = mysqli_query($linkbd,$sqlClientes);
                        $rowClientes = mysqli_fetch_row($resClientes);

                        $_POST['id_cliente'][] = $rowServicio[0];
                        $_POST['cod_usuario'][] = $rowClientes[0];
                        $_POST['nombre_usuario'][] = encuentraNombreTerceroConIdCliente($rowServicio[0]);
                        $_POST['documento_usuario'][] = encuentraDocumentoTerceroConIdCliente($rowServicio[0]);
                        $_POST['direccion_usuario'][] = encuentraDireccionConIdCliente($rowServicio[0]);
				    }    
                }

				

                if(@$_POST['oculto'] == '2')
                {
                    $usuario = $_SESSION['usuario'];
                    preg_match("/([0-9]{2})\/([0-9]{2})\/([0-9]{4})/", $_POST['fecha'],$f);
                    $fecha="$f[3]-$f[2]-$f[1]";
                    
                    for ($x=0; $x < count($_POST['cod_usuario']); $x++) 
                    {
                        $lectura = $_POST["lectura$x"];

                        if($lectura == '')
                        {
                            $lectura = 0;
                        }
                        
                        
                        $sql = "INSERT INTO srvlectura(corte, id_cliente, id_servicio, lectura, fecha, usuario) VALUES ($_POST[corte], '".$_POST['id_cliente'][$x]."', $_POST[id_servicio], $lectura, '$fecha', '$usuario')";
                        $res = mysqli_query($linkbd,$sql);
                    }

                    if($res)
                    {
                        echo "<script>despliegamodalm('visible','1','Se ha almacenado con Exito');</script>";
                    }
                    else
                    {
                        echo"<script>despliegamodalm('visible','2','No se pudo ejecutar el guardado');</script>";
                    }
                }
			}

		?>

		<div class="subpantalla" style="height: 65%;">
			<table>
				<tr>
                    <td class="titulos" colspan="5" height="25">Captura de Lecturas</td>
                </tr>

				<tr class="titulos2">
                    <td style="text-align: center;">C&oacute;digo Usuario</td>
                    <td style="text-align: center;">Documento</td>
                    <td style="text-align: center;">Nombre Usuario</td>
                    <td style="text-align: center;">Direcci&oacute;n</td>
                    <td style="text-align: center;">Lectura</td>
                </tr>

				<?php

                $iter = 'saludo1a';
                $iter2 = 'saludo2';

                for ($x=0; $x < count($_POST['cod_usuario']); $x++) 
                { 
                    echo "
                    <tr class='$iter'>
                        <td style='text-align:center;'>".$_POST['cod_usuario'][$x]."</td>
                        <td style='text-align:center;'>".$_POST['documento_usuario'][$x]."</td>
						<td style='text-align:center;'>".$_POST['nombre_usuario'][$x]."</td>
                        <td style='text-align:center;'>".$_POST['direccion_usuario'][$x]."</td>
						<td style='width:8%'>
							<div>
								<input type='text' id='lectura$x' name='lectura$x' value='".$_POST["lectura$x"]."' ".$_POST["lectura$x"]."' style='text-align:center;'/>
							</div>
						</td>
                    </tr>
                    ";

                    $aux = $iter;
                    $iter = $iter2;
                    $iter2 = $aux;
                }
                ?>
                
                
                <?php
                    
                ?>
			</table>
		</div>
    </form>
</body>
</html>