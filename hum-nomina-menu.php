<?php
	header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=utf8");
	require "comun.inc";
	require "funciones.inc";
	session_start();
	cargarcodigopag($_GET['codpag'],$_SESSION["nivel"]);
	date_default_timezone_set("America/Bogota");
?>
<!DOCTYPE >
<html lang="es">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1">
		<title>:: IDEAL 10 - Gesti&oacute;n humana</title>
		<link href="css/css2.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<script type="text/javascript" src="css/programas.js"></script>
		<?php titlepag();?>
	</head>
	<body>
		<IFRAME src="alertas.php" name="alertas" id="alertas" style="display:none"></IFRAME>
		<span id="todastablas2"></span>
		<table>
			<tr><script>barra_imagenes("hum");</script><?php cuadro_titulos();?></tr>
			<tr><?php menu_desplegable("hum");?></tr>
			<tr>
				<td colspan="3" class="cinta">
					<img src="imagenes/add2.png" class="mgbt1">
					<img src="imagenes/guardad.png" class="mgbt1">
					<img src="imagenes/buscad.png" class="mgbt1">
					<img src="imagenes/agenda1.png" title="Agenda" onClick="mypop=window.open('plan-agenda.php','','');mypop.focus()" class="mgbt">
					<img src="imagenes/nv.png" title="Nueva ventana" onClick="mypop=window.open('hum-principal.php','','');mypop.focus();" class="mgbt">
					<img src="imagenes/duplicar_pantalla.png" title="Duplicar pesta&ntilde;a" onClick="mypop=window.open('<?php echo $url2; ?>','','');mypop.focus();" class="tooltip bottom mgbt">
				</td>
			</tr>
		</table>
		<form name="form2" method="post" action="">
			<table class="inicio">
				<tr>
					<td class="titulos" colspan="2">.: Menú tramites Nomina</td>
					<td class="cerrar" style="width:7%" onClick="location.href='hum-principal.php'">Cerrar</td>
				</tr>
				<tr>
					<td class="titulos2">Gestión de nómina</td>
                    <td class="titulos2">Validar liquidación</td>
				</tr>
				<tr>
					<td class='saludo1' >
						<ol id="lista2">
                            <li class='mgbt3' onClick="location.href='hum-nomina-descuentos-buscar'">Descuentos nómina</li>
                            <li class='mgbt3' onClick="location.href='hum-nomina-vacaciones-buscar'">Novedad de Vacaciones</li>
                            <li class='mgbt3' onClick="location.href='hum-nomina-incapacidad-buscar'">Novedad de Incapacidades</li>
							<li class='mgbt3' onClick="location.href='hum-nomina-novedades-buscar'">Preliquidación de nómina</li>
							<li class='mgbt3' onClick="location.href='hum-nomina-retenciones-buscar.php'">Aplicar retenciones</li>
							<li class='mgbt3' onClick="location.href='hum-nomina-liquidar-buscar'">Crear liquidación</li>
							<li class='mgbt3' onClick="location.href='hum-nomina-aprobar-buscar.php'">Aprobar liquidación</li>
						</ol>
					</td>
                    <td class='saludo1'>
						<ol id="lista2">
                            <li class='mgbt3' onClick="location.href='hum-ams-raw_planillas'">Carga de planilla</li>
                            <li class='mgbt3' onClick="location.href='hum-nomina-validar'">Validar liquidación</li>
						</ol>
					</td>
				</tr>
			</table>
		</form>
	</body>
</html>
