<?php
	require "comun.inc";
	require "funciones.inc"; 
	session_start();
	cargarcodigopag(@$_GET['codpag'], @$_SESSION['nivel']);
	header("Cache-control: private"); // Arregla IE 6 
	date_default_timezone_set("America/Bogota");
?>
<!DOCTYPE >
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="es"> 
	<head>
	 	<meta http-equiv="Content-Type" content="text/html" charset="iso-8859-1"/>
        <meta http-equiv="X-UA-Compatible" content="IE=9"/>
		<title>:: Ideal - Presupuesto</title>
        <link href="css/css2.css" rel="stylesheet" type="text/css" />
        <link href="css/css3.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="css/programas.js"></script>
		<link rel="stylesheet" href="bootstrap/css/bootstrap.css">
        <link rel="stylesheet" href="bootstrap/css/estilos.css">
		<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">

        <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
        <script type="text/javascript" src="bootstrap/fontawesome.5.11.2/js/all.js"></script>
		
		<?php titlepag();?> 

		<style>
			.background_active_color{
				background: #16a085;
			}
            .background_active_clasificador{
                font-family: calibri !important;
				font-weight: bold !important;
				font-size:14px !important;
            }
			.background_active{
				/* font: 115% sans-serif !important; */
    			/*font-weight: 700 !important;*/
				/*font-family: "Constantia", serif !important;*/
				font-family: calibri !important;
				font-weight: bold !important;
				font-size:20px !important;
			}
			.background_active_1{
				/* font: 115% sans-serif !important; */
    			/*font-weight: 700 !important;*/
				/*font-family: "Constantia", serif !important;*/
				font-family: helvética !important;
				font-size:20px !important;
			}
			.inicio--no-shadow{
				box-shadow: none;
			}
			.btn-delete{
				background: red; 	
				color: white;
				border-radius: 5px;
				border: none;
				font-size: 13px;
			}
			.btn-delete:hover, .btn-delete:focus{
				background: white; 	
				color: red;
			}
			.modal-mask {
			position: fixed;
			z-index: 9998;
			top: 0;
			left: 0;
			width: 100%;
			height: 100%;
			background-color: rgba(0, 0, 0, .5);
			display: table;
			transition: opacity .3s ease;
			}

			.modal-wrapper {
			display: table-cell;
			vertical-align: middle;
			}
			.modal-body{
				max-height: 250px;
				overflow-y: scroll;
			}
			.modal-intermetio{
				margin: 0 15px;
				font-family: helvética !important;
				font-size: 26px !important;
				padding: 10px 0;
			}
			.modal-intermedio-agregar{
				text-align:right;
				padding: 4px;
				margin-top: 6px;
				margin-right: 20px
			}
			.modal-body_1{
				padding-top: 15px;
				height: 40px;
			}

			.loader-table{
				/* background-color: #dff9fb;
				opacity: .5; */
				display: flex;
				align-items: center;
				justify-content: center;
				height: 75%;
			}
			.spinner{
				border: 4px solid rgba(0, 0, 0, 0.2);
				border-left-color: #39C;
				border-radius: 50%;
				width: 50px;
				height: 50px;
				animation: spin .9s linear infinite;
			}
			@keyframes spin {
				to { transform: rotate(360deg); }
			}
		</style>
		<script>
			function excell()
            {
                document.form2.action="ccp-reportegastosinversionprogramaticoexcel.php";
                document.form2.target="_BLANK";
                document.form2.submit(); 
                document.form2.action="";
                document.form2.target="";
            }
		</script>
			
    </head>
    <body>
        <IFRAME src="alertas.php" name="alertas" id="alertas" style="display:none"></IFRAME>
        <span id="todastablas2"></span>
		<div id="myapp" style="height:inherit;">
			<table>
				<tr><script>barra_imagenes("ccpet");</script><?php cuadro_titulos();?></tr>	 
				<tr><?php menu_desplegable("ccpet");?></tr>
				<tr>
					<td colspan="3" class="cinta">
						<a><img src="imagenes/add.png" title="Nuevo" onClick="location.href='#'" class="mgbt"/></a>
						<a class="mgbt"><img src="imagenes/guardad.png"/></a>
						<a><img src="imagenes/busca.png" title="Buscar"  onClick="location.href='#'" class="mgbt"/></a>
						<a href="#" onClick="mypop=window.open('ccp-principal.php','','');mypop.focus();" class="mgbt"><img src="imagenes/nv.png" title="Nueva Ventana"></a>
						<img src="imagenes/excel.png" title="Excel" v-on:click="tableToExcel('table', 'CCPET')" class="mgbt"/>
						<img src="imagenes/iratras.png" title="Atr&aacute;s" onClick="location.href='ccp-reportegastosinversion.php'" class="mgbt"/>
					</td>
				</tr>
			</table>
			<div class="subpantalla" style="height:520px; width:99.6%; overflow:hidden;">
			
                <div class="row">
					<div class="col-12">
						<h5 style="padding-left:30px; padding-top:5px; padding-bottom:5px; background-color: #0FB0D4">Reporte indicador producto:</h5>
					</div>
				</div>
				<div class="row">
					<div class="col-12" v-show="mostrar_resultados_proyectos">
						<div style="margin: 0px 10px 10px; border-radius: 0 0 0 5px; max-height: 476px; overflow: scroll; background: white; ">
							
							<table ref="table" id="loremTable" class='inicio inicio--no-shadow' style='margin: 10px; border-radius: 4px;'>
								<thead>
									<tr>
										<td width = "10%">COD. PRESUPUESTAL</td>
										<td width = "5%">COD. SERVICIOS</td>
										<td width = "10%">COD. PRODUCTO</td>
										<td width = "30%">INDICADOR PRODUCTO</td>
										<td width = "20%">Meta PDM</td>
										<td width = "5%">FUENTE</td>
										<td width = "10%" style="background-color:gainsboro">VALOR CSF</td>
										<td width = "10%" style="background-color:lemonchiffon">VALOR SSF</td>
										<td width = "10%" style="background-color:gainsboro">VALOR CDP</td>
										<td width = "10%" style="background-color:lemonchiffon">COMRPOMISOS</td>
										<td width = "10%" style="background-color:gainsboro">OBLIGACIONES</td>
										<td width = "10%" style="background-color:lemonchiffon">RESERVA</td>
										<td width = "10%" style="background-color:gainsboro">PAGOS</td>
										<td width = "10%" style="background-color:lemonchiffon">CXP</td>
										<td width = "10%" style="background-color:gainsboro">SALDOS</td>
									</tr>
								</thead>
								<tbody v-if="mostrar_resultados_proyectos" v-for="proyecto in proyectos">
									<?php
										$co ='zebra1';
										$co2='zebra2';
									?>
                                    <tr class='<?php echo $co; ?>' style="font: 100% sans-serif; font-weight: bold;">
                                        <td width="100%" colspan="15">SECTOR: {{ proyecto[2] }}</td>
                                    </tr>

                                    <tr class='<?php echo $co; ?>' style="font: 100% sans-serif; font-weight: bold;">
                                        <td width="100%" colspan="15">PROGRAMA: {{ proyecto[3] }}</td>
                                    </tr>

									<tr class='<?php echo $co; ?>' style="font: 100% sans-serif; font-weight: bold;">
                                        <td width="100%" colspan="15">SUBPROGRAMA: {{ proyecto[4] }}</td>
                                    </tr>

                                    <tr class='<?php echo $co; ?>' style="font: 100% sans-serif; font-weight: bold;">
                                        <td width="100%" colspan="15">PROYECTO: {{ proyecto[1] }}</td>
                                    </tr>
									

									<tr v-for="proyectos_det in proyectos_det_indicador[proyecto[0]-1]" class='<?php echo $co; ?>' style="font: 100% sans-serif;">
										<td width = "10%" style="padding-left: 10px; ">{{ proyectos_det[3] }}</td>
										<td width = "5%" v-if="proyectos_det[5] != ''">{{ proyectos_det[5] }} </td>
										<td width = "5%" v-else>{{ proyectos_det[4] }} </td>
										<td width = "10%">{{ proyectos_det[10] }} </td>
										<td width = "30%">{{ proyectos_det[8]  }} - {{ proyectos_det[11] }}</td>
										<td width = "20%">{{ proyectos_det[9]  }} - {{ proyectos_det[12] }}</td>
										<td width = "5%"> {{ proyectos_det[1]  }} </td>
										<td width = "10%" style="background-color:gainsboro"> {{ proyectos_det[6]  }} </td>
										<td width = "10%" style="background-color:lemonchiffon"> {{ proyectos_det[7]  }} </td>
										<td width = "10%" style="background-color:gainsboro"> {{ proyectos_det[13]  }} </td>
										<td width = "10%" style="background-color:lemonchiffon"> {{ proyectos_det[14]  }} </td>
										<td width = "10%" style="background-color:gainsboro"> {{ proyectos_det[15]  }} </td>
										<td width = "10%" style="background-color:lemonchiffon"> {{ proyectos_det[16]  }} </td>
										<td width = "10%" style="background-color:gainsboro"> {{ proyectos_det[17]  }} </td>
										<td width = "10%" style="background-color:lemonchiffon"> {{ proyectos_det[18]  }} </td>
										<td width = "10%" style="background-color:gainsboro"> {{ proyectos_det[19]  }} </td>
										
										<?php
										$aux=$co;
										$co=$co2;
										$co2=$aux;
										?>
									</tr>

								</tbody>
								<tbody v-else>
									<tr>
										<td width="20%"style="font: 120% sans-serif; padding-left:10px; text-align:center;" colspan="3">Sin resultados</td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
				</div>
				<span id="end_page"> </span>
			</div>	
		</div>
		
		<script src="Librerias/vue/vue.min.js"></script>
		<script src="Librerias/vue/axios.min.js"></script>
		<script src="vue/ccp-reportegastosinversionprogramatico.js"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/vue/2.6.10/vue.js"></script>
	</body>
</html>