<?php

    header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=utf8");
	require 'comun.inc';
	require 'funciones.inc';
	session_start();
    if(empty($_SESSION)){
        header("location: index.php");
    }
	$linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");
	cargarcodigopag($_GET['codpag'],$_SESSION["nivel"]);
	date_default_timezone_set("America/Bogota");
?>
<!DOCTYPE >
<html lang=es>
	<head>
    <meta charset="utf-8"/>
		<meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1"/>
		<meta name="viewport" content="user-scalable=no">
		<title></title>
		<link href="favicon.ico" rel="shortcut icon"/>
		<link href="css/css2z.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
		<link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
		<link href="css/css4.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
        <link href="css/style.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css"/>
        <link rel="stylesheet" href="sweetalert2/dist/sweetalert2.min.css">
        <script src="sweetalert2/dist/sweetalert2.min.js"></script>
		<script src="vue/vue.min.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script src="css/programas.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
	</head>
	<body>
		<header>
			<table>
				<tr><script>barra_imagenes("trans");</script><?php cuadro_titulos();?></tr>
			</table>
		</header>

        <main id="myapp" v-cloak>
            <input type="hidden" value = "2" ref="pageType">
            <div class="loading-container" v-show="isLoading" >
                <p class="text-loading"data-loading="IDEAL 10">IDEAL 10</p>
            </div>
            <nav>
                <table>
                    <tr><?php menu_desplegable("trans");?></tr>
                </table>
                <div class="bg-white group-btn p-1" id="newNavStyle">
                    <button type="button" @click="window.location.href='trans-liquidacion-ingreso-crear'" class="btn btn-white btn-primary-hover d-flex justify-between align-items-center">
                        <span>Nuevo</span>
                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 -960 960 960"><path d="M440-440H200v-80h240v-240h80v240h240v80H520v240h-80v-240Z"></path></svg>
                    </button>
                    <button type="button" @click="window.location.href='trans-liquidacion-ingreso-buscar'" class="btn btn-white btn-primary-hover d-flex justify-between align-items-center">
                        <span>Buscar</span>
                        <svg class="fill-black group-hover:fill-white w-5 h-5" xmlns="http://www.w3.org/2000/svg" viewBox="0 -960 960 960"><path d="M784-120 532-372q-30 24-69 38t-83 14q-109 0-184.5-75.5T120-580q0-109 75.5-184.5T380-840q109 0 184.5 75.5T640-580q0 44-14 83t-38 69l252 252-56 56ZM380-400q75 0 127.5-52.5T560-580q0-75-52.5-127.5T380-760q-75 0-127.5 52.5T200-580q0 75 52.5 127.5T380-400Z"></path></svg>
                    </button>
                    <button type="button" class="btn btn-white btn-primary-hover d-flex justify-between align-items-center" @click="mypop=window.open('teso-principal.php','',''); mypop.focus();">
                        <span>Nueva ventana</span>
                        <svg class="fill-black group-hover:fill-white w-5 h-5" xmlns="http://www.w3.org/2000/svg"  viewBox="0 -960 960 960" ><path d="M320-120v-80h80v-80H160q-33 0-56.5-23.5T80-360v-400q0-33 23.5-56.5T160-840h640q33 0 56.5 23.5T880-760v400q0 33-23.5 56.5T800-280H560v80h80v80H320ZM160-360h640v-400H160v400Zm0 0v-400 400Z"/></svg>
                    </button>
                    <button type="button" @click="mypop=window.open('trans-liquidacion-ingreso-editar','','');mypop.focus();" class="btn btn-white btn-primary-hover d-flex justify-between align-items-center">
                        <span class="group-hover:text-white">Duplicar pantalla</span>
                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 -960 960 960"><path d="M640-160v-360H160v360h480Zm80-200v-80h80v-360H320v200h-80v-200q0-33 23.5-56.5T320-880h480q33 0 56.5 23.5T880-800v360q0 33-23.5 56.5T800-360h-80ZM160-80q-33 0-56.5-23.5T80-160v-360q0-33 23.5-56.5T160-600h480q33 0 56.5 23.5T720-520v360q0 33-23.5 56.5T640-80H160Zm400-603ZM400-340Z"></path></svg>
                    </button>
                    <button type="button" @click="exportData()" class="btn btn-danger btn-danger-hover d-flex justify-between align-items-center">
                        <span>Exportar PDF</span>
                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><!-- !Font Awesome Free 6.5.1 by @fontawesome - https://fontawesome.com License - https://fontawesome.com/license/free Copyright 2024 Fonticons, Inc. --><path d="M64 464l48 0 0 48-48 0c-35.3 0-64-28.7-64-64L0 64C0 28.7 28.7 0 64 0L229.5 0c17 0 33.3 6.7 45.3 18.7l90.5 90.5c12 12 18.7 28.3 18.7 45.3L384 304l-48 0 0-144-80 0c-17.7 0-32-14.3-32-32l0-80L64 48c-8.8 0-16 7.2-16 16l0 384c0 8.8 7.2 16 16 16zM176 352l32 0c30.9 0 56 25.1 56 56s-25.1 56-56 56l-16 0 0 32c0 8.8-7.2 16-16 16s-16-7.2-16-16l0-48 0-80c0-8.8 7.2-16 16-16zm32 80c13.3 0 24-10.7 24-24s-10.7-24-24-24l-16 0 0 48 16 0zm96-80l32 0c26.5 0 48 21.5 48 48l0 64c0 26.5-21.5 48-48 48l-32 0c-8.8 0-16-7.2-16-16l0-128c0-8.8 7.2-16 16-16zm32 128c8.8 0 16-7.2 16-16l0-64c0-8.8-7.2-16-16-16l-16 0 0 96 16 0zm80-112c0-8.8 7.2-16 16-16l48 0c8.8 0 16 7.2 16 16s-7.2 16-16 16l-32 0 0 32 32 0c8.8 0 16 7.2 16 16s-7.2 16-16 16l-32 0 0 48c0 8.8-7.2 16-16 16s-16-7.2-16-16l0-64 0-64z"></path></svg>
                    </button>
                    <button type="button" @click="window.location.href='trans-liquidacion-ingreso-buscar'" class="btn btn-success btn-success-hover d-flex justify-between align-items-center">
                        <span>Atras</span>
                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 -960 960 960"><path d="M760-200v-160q0-50-35-85t-85-35H273l144 144-57 56-240-240 240-240 57 56-144 144h367q83 0 141.5 58.5T840-360v160h-80Z"></path></svg>
                    </button>
                </div>
            </nav>
            <section class="bg-white">
                <div>
                    <div>
                        <div class="d-flex w-50">
                            <div class="form-control">
                                <label class="form-label" for="">Número reconocimiento:</label>
                                <div class="d-flex">
                                    <button type="button" class="btn btn-primary" @click="nextItem('prev')"><</button>
                                    <input type="text"  style="text-align:center;" v-model="txtConsecutivo" @change="nextItem()">
                                    <button type="button" class="btn btn-primary" @click="nextItem('next')">></button>
                                </div>
                            </div>
                            <div class="form-control" v-if="txtStatus =='R'">
                                <label class="form-label">Tipo de movimiento:</label>
                                <select v-model="selectMovimiento">
                                    <option value="101">Documento creado</option>
                                    <option value="301">Documento reversado</option>
                                </select>
                            </div>
                            <div class="form-control" v-if="txtStatus == 'P'">
                                <label class="form-label">Nro. Comprobante de Ingreso:</label>
                                <div class="d-flex">
                                    <input type="number" class="text-center" :value="txtIngresoInterno" disabled>
                                    <button type="button" class="btn btn-primary" @click="mypop=window.open('trans-liquidacion-ingreso-editar?id='+txtIngresoInterno,'',''); mypop.focus();">Ver</button>
                                </div>
                            </div>
                            <div class="form-control">
                                <label class="form-label">Estado:</label>
                                <div>
                                    <span class="badge" :class="[txtStatus == 'S' ? 'badge-success' : txtStatus == 'P' ? 'badge-primary' :'badge-danger']">
                                        {{ txtStatus == 'S' ? 'Activo' : txtStatus == 'P' ? 'Pagado' :'Reversado' }}
                                    </span>
                                </div>
                            </div>
                        </div>
                        <div v-if="selectMovimiento == 101">
                            <h2 class="titulos m-0">Reconocimiento de ingresos nro. {{ txtConsecutivo }}</h2>
                            <div class="d-flex w-100">
                                <div class="form-control w-25">
                                    <label class="form-label">Fecha:</label>
                                    <input type="date" :value="txtFecha" disabled>
                                </div>
                                <div class="form-control w-25">
                                    <label class="form-label">Medio de pago <span class="text-danger fw-bolder">*</span>:</label>
                                    <select v-model="selectMedioPago" disabled>
                                        <option value="CSF">Con SF</option>
                                        <option value="SSF">Sin SF</option>
                                    </select>
                                </div>
                                <div class="form-control">
                                    <label class="form-label">Tercero:</label>
                                    <div class="d-flex">
                                        <input type="text" class="w-25" :value="objTercero.codigo" disabled >
                                        <input type="text" :value="objTercero.nombre" disabled>
                                    </div>
                                </div>
                            </div>
                            <div class="d-flex w-100">
                                <div class="form-control">
                                    <label class="form-label">Concepto:</label>
                                    <textarea rows="3" v-model="txtConcepto" disabled></textarea>
                                </div>
                            </div>
                            <h2 class="titulos m-0">Detalle</h2>
                            <div class="table-responsive" style="height:30vh">
                                <table class="table table-hover">
                                    <thead class="text-center">
                                        <tr>
                                            <th>Ingreso</th>
                                            <th>Nombre Ingreso</th>
                                            <th>Valor</th>
                                        </tr>
                                    </thead>
                                    <tbody class="fw-normal ">
                                        <tr v-for="(data,index) in arrData" :key="index">
                                            <td>{{data.ingreso.codigo}}</td>
                                            <td>{{data.ingreso.nombre}}</td>
                                            <td class="text-right">{{formatNum(data.valor)}}</td>
                                        </tr>
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <td colspan="2" class="fw-bold text-right">Total</td>
                                            <td class="text-right">{{formatNum(txtTotal)}}</td>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                        <div v-if="selectMovimiento == 301">
                            <h2 class="titulos m-0">Reconocimiento de ingreso reversado</h2>
                            <div class="w-100">
                                <div class="d-flex">
                                    <div class="form-control">
                                        <label class="form-label">Fecha:</label>
                                        <input type="date" :value="txtFechaRev">
                                    </div>
                                    <div class="form-control pb-3">
                                        <label class="form-label">No. de liquidación:</label>
                                        <input type="text" :value="objRev.codigo" disabled >
                                    </div>
                                </div>
                                <div class="d-flex">
                                    <div class="form-control">
                                        <label class="form-label">Descripción:</label>
                                        <textarea rows="3" :value="txtConceptoRev" disabled></textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </main>
        <script src="Librerias/vue/vue.min.js"></script>
		<script type="module" src="transporte/js/functions_liquidacion_ingreso.js?<?= date('d_m_Y_h_i_s');?>"></script>
	</body>
</html>
