<?php
header("Cache-control: no-cache, no-store, must-revalidate");
header("Content-Type: text/html;charset=iso-8859-1");
require "comun.inc";
require "funciones.inc";
require "funcionesSP.inc.php";
session_start();
$linkbd = conectar_v7();
cargarcodigopag($_GET['codpag'],$_SESSION["nivel"]);
date_default_timezone_set("America/Bogota");
?>
<!DOCTYPE >
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>:: IDEAL 10 - Servicios P&uacute;blicos</title>
    <link href="css/css2.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css"/>
    <link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css"/>
    <link href="css/css4.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css"/>
    <link href="css/cssSP.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css"/>
    <script type="text/javascript" src="css/calendario.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
    <script type="text/javascript" src="css/programas.js"></script>

    <style>
    </style>

    <script>
        function despliegamodal2(_valor, _table)
        {
            document.getElementById('bgventanamodal2').style.visibility = _valor;

            if(_table == 'srvmedidores')
            {
                document.getElementById('ventana2').src = 'medidores-ventana.php?table=' + _table;
            }
            else if(_table == 'srvclientes')
            {
                document.getElementById('ventana2').src = 'ventana-clienteservicio.php?table=' + _table;
            }
        }

        function despliegamodalm(_valor,_tip,mensa,pregunta)
        {
            document.getElementById("bgventanamodalm").style.visibility=_valor;

            if(_valor=="hidden")
            {
                document.getElementById('ventanam').src="";
            }
            else
            {
                switch(_tip)
                {
                    case "1":	document.getElementById('ventanam').src="ventana-mensaje1.php?titulos="+mensa;break;
                    case "2":	document.getElementById('ventanam').src="ventana-mensaje3.php?titulos="+mensa;break;
                    case "3":	document.getElementById('ventanam').src="ventana-mensaje2.php?titulos="+mensa;break;
                    case "4":	document.getElementById('ventanam').src="ventana-consulta1.php?titulos="+mensa+"&idresp="+pregunta;break;	
                }
            }
        }

        function funcionmensaje()
        {
            var idban=document.getElementById('consecutivo').value;
            var corte=document.getElementById('corte').value;

            document.location.href = "serv-recaudoCompletoVisualizar.php?idban="+idban+'&corte='+corte;
        }

        function respuestaconsulta(pregunta)
        {
            switch(pregunta)
            {
                case "1":	
                    document.form2.oculto.value = '2';
                    document.form2.submit();
                break;
            }
        }

        function guardar()
        {
            var codigo = document.getElementById('codigo').value;
            var consecutivo = document.getElementById('consecutivo').value;
            var fecha = document.getElementById('fecha').value;
            var concepto = document.getElementById('concepto').value;

            if(codigo.trim() != '' && consecutivo.trim() != '' && fecha.trim() != '' && concepto.trim() != '') 
            {
                despliegamodalm('visible','4','Esta seguro de reversar este recibo de caja?','1');
            }
            else 
            {
                despliegamodalm('visible','2','Falta información para realizar la reversion');
            }
        }

        function atras()
        {
            var idban=document.getElementById('consecutivo').value;
            var corte=document.getElementById('corte').value;

            document.location.href = "serv-recaudoCompletoVisualizar.php?idban="+idban+'&corte='+corte;
        }
    </script>
</head>

    <?php titlepag();?>
    
<body>
    <IFRAME src="alertas.php" name="alertas" id="alertas" style="display:none"></IFRAME>
    <span id="todastablas2"></span>
    <table>
        <tr><script>barra_imagenes("serv");</script><?php cuadro_titulos();?></tr>
        <tr><?php menu_desplegable("serv");?></tr>
        <tr>
            <td colspan="3" class="cinta">
                <a href="" class="mgbt"><img src="imagenes/add.png"/></a>

                <a onclick="guardar()" class="mgbt"><img src="imagenes/guarda.png" title="Guardar"/></a>

                <a href="" class="mgbt"><img src="imagenes/busca.png" title="Buscar"/></a>

                <a onclick="mypop=window.open('plan-agenda.php','','');mypop.focus()" class="mgbt"><img src="imagenes/agenda1.png" title="Agenda" /></a>

                <a onclick="mypop=window.open('serv-principal.php','','');mypop.focus();" class="mgbt"><img src="imagenes/nv.png" title="Nueva ventana"></a>

                <a onclick="mypop=window.open('<?php echo $url2; ?>','','');mypop.focus();" class="mgbt"><img src="imagenes/duplicar_pantalla.png" title="Duplicar pesta&ntilde;a"></a>

                <a onclick="atras()" class="mgbt"><img src="imagenes/iratras.png" alt="Atras" title="Atras"></a>
            </td>
        </tr>
    </table>

    <div id="bgventanamodalm" class="bgventanamodalm">
        <div id="ventanamodalm" class="ventanamodalm">
            <IFRAME src="" name="ventanam" marginWidth=0 marginHeight=0 frameBorder=0 id="ventanam" frameSpacing=0 style=" width:700px; height:130px; top:200; overflow:hidden;"></IFRAME>
        </div>
    </div>

    <form name="form2" method="post" action="">
        <?php
            if(@$_POST['oculto'] == '')
            {
               $_POST['consecutivo'] = $_GET['cs'];
               $_POST['codigo'] = selconsecutivo('srvreversion_recibo_caja', 'id');
               $_POST['corte'] = $_GET['corte'];
            }
        ?>
        
        <div>
            <table class="inicio">
                    <tr>
						<td class="titulos" colspan="6">.: Reversar Recibo de Caja</td>
						<td class="cerrar" style="width:7%" onClick="location.href='serv-principal.php'">Cerrar</td>
					</tr>

                <tr>
                    <td class="tamano01" style="width: 3cm;">Codigo Reversi&oacute;n:</td>
                    <td style="width: 15%;">
                        <input type="text" name="codigo" id="codigo" value="<?php echo $_POST['codigo'] ?>" style="height: 30px; width:98%; text-align:center;" readonly>
                    </td>

                    <td class="tamano01" style="width: 3cm;">Consecutuvo Recibo de Caja:</td>
                    <td style="width: 15%;">
                        <input type="text" name="consecutivo" id="consecutivo" value="<?php echo $_POST['consecutivo'] ?>" style="height: 30px; width:98%; text-align:center;" readonly>
                    </td>

                    <td class="tamano01" style="text-align: center;">Fecha Reversi&oacute;n</td>
                    <td>
                        <input type="text" name="fecha" value="<?php echo @ $_POST['fecha']?>" maxlength="10" onKeyPress="javascript:return solonumeros(event)" onKeyUp="return tabular(event,this)" id="fecha" onKeyDown="mascara(this,'/',patron,true)" title="DD/MM/YYYY" style="width:80%;height: 30px; text-align:center;" readonly>&nbsp;
                        <img src="imagenes/calendario04.png" onClick="displayCalendarFor('fecha');" title="Calendario" class="icobut"/>
                    </td>
                </tr>

                <tr>
                    <td class="tamano01" style="width: 3cm;">Concepto de Reversi&oacute;n</td>
                    <td colspan="5">
                        <input type="text" name="concepto" id="concepto" value="<?php echo $_POST['concepto'] ?>" style="height: 30px; width:98%;">
                    </td>
                </tr>
            </table>
        </div>

        <input type="hidden" name="oculto" id="oculto" value="1">
        <input type="hidden" name="corte" id="corte" value="<?php echo $_POST['corte'] ?>">

    </form>

    <?php
        if(@$_POST['oculto'] == '2')
        {
            preg_match("/([0-9]{2})\/([0-9]{2})\/([0-9]{4})/", $_POST['fecha'],$f);
			$fecha="$f[3]-$f[2]-$f[1]";
            $vigencia = $f[3];

            $sqlReciboCaja = "SELECT numero_factura FROM srvrecibo_caja WHERE consecutivo = '$_POST[consecutivo]'";
            $resReciboCaja = mysqli_query($linkbd,$sqlReciboCaja);
            $rowReciboCaja = mysqli_fetch_row($resReciboCaja);

            $numero_factura = $rowReciboCaja[0];
            $movimiento_recaudo = '201';
            $movimiento_reversion_recaudo = '401';
            $comprobante_recaudo = '30';
            $comprobante_recaudo_reversion = 2000 + $comprobante_recaudo;

            //reversa el kardex
            $sqlDetalleFacturacion = "SELECT * FROM srvdetalles_facturacion WHERE numero_facturacion = '$numero_factura' AND tipo_movimiento = '$movimiento_recaudo' ORDER BY id_tipo_cobro"; 
            $resDetalleFacturacion = mysqli_query($linkbd,$sqlDetalleFacturacion);
            while($rowDetalleFacturacion = mysqli_fetch_assoc($resDetalleFacturacion))
            {
                if($rowDetalleFacturacion['credito'] > 0)
                {
                    $sql = "INSERT INTO srvdetalles_facturacion (corte, id_cliente, numero_facturacion, tipo_movimiento, fecha_movimiento, id_servicio, id_tipo_cobro, credito, debito, saldo, estado) VALUES ('$_POST[corte]', '$rowDetalleFacturacion[id_cliente]', '$numero_factura', '$movimiento_reversion_recaudo', '$fecha', '$rowDetalleFacturacion[id_servicio]', '$rowDetalleFacturacion[id_tipo_cobro]', 0, '$rowDetalleFacturacion[credito]', 0, 'S')";
                    mysqli_query($linkbd,$sql);
                }
                elseif($rowDetalleFacturacion['debito'] > 0)
                {
                    $sql = "INSERT INTO srvdetalles_facturacion (corte, id_cliente, numero_facturacion, tipo_movimiento, fecha_movimiento, id_servicio, id_tipo_cobro, credito, debito, saldo, estado) VALUES ('$_POST[corte]', '$rowDetalleFacturacion[id_cliente]', '$numero_factura', '$movimiento_reversion_recaudo', '$fecha', '$rowDetalleFacturacion[id_servicio]', '$rowDetalleFacturacion[id_tipo_cobro]', '$rowDetalleFacturacion[debito]', 0, 0, 'S')";
                    mysqli_query($linkbd,$sql);
                }           
            }

            //reversion contabilidad
            $sql = "SELECT * FROM comprobante_cab WHERE numerotipo = '$_POST[consecutivo]' AND tipo_comp = '$comprobante_recaudo'";
            $res = mysqli_query($linkbd,$sql);
            $row = mysqli_fetch_assoc($res);

            $total = $row['total_debito'];

            //guarda comprobante cabecera
            $sqlComprobanteCabecera = "INSERT INTO comprobante_cab (numerotipo, tipo_comp, fecha, concepto, total_debito, total_credito, estado) VALUES ('$_POST[codigo]', '$comprobante_recaudo_reversion', '$fecha', 'Reversado Recaudo del recibo de caja $_POST[consecutivo]', $total, $total, '1') ";
            mysqli_query($linkbd,$sqlComprobanteCabecera);

            //comprobante detalles
            $sql = "SELECT * FROM comprobante_det WHERE id_comp = '$comprobante_recaudo $_POST[consecutivo]'";
            $res = mysqli_query($linkbd,$sql);
            while($row = mysqli_fetch_assoc($res))
            {
                $tercero = $row['tercero'];
                $cuenta[] = $row['cuenta'];
                $cc[] = $row['centrocosto'];
                $valdebito[] = $row['valdebito'];
                $valcredito[] = $row['valcredito'];
            }

            for($i=0; $i <count($cuenta) ; $i++) 
            { 
                if($valdebito[$i] != 0)
                {
                    $query = "INSERT INTO comprobante_det(id_comp,cuenta,tercero,centrocosto,detalle,cheque,valdebito, valcredito,estado,vigencia) VALUES('$comprobante_recaudo_reversion $_POST[codigo]','$cuenta[$i]','$tercero','$cc[$i]','Reversado del recaudo $_POST[consecutivo]','',0,$valdebito[$i],'1' ,'$vigencia')";
                    mysqli_query($linkbd,$query);
                }
                elseif($valcredito[$i] != 0)
                {
                    $query = "INSERT INTO comprobante_det(id_comp,cuenta,tercero,centrocosto,detalle,cheque,valdebito, valcredito,estado,vigencia) VALUES('$comprobante_recaudo_reversion $_POST[codigo]','$cuenta[$i]','$tercero','$cc[$i]','Reversado del recaudo $_POST7[consecutivo]','','$valcredito[$i]',0,'1' ,'$vigencia')";
                    mysqli_query($linkbd,$query);
                }
            }

            $sqlReversar = "INSERT INTO comprobante_rev (tipo_comprobante_origen, num_comprobante_origen, tipo_comprobante_rev, num_comprobante_rev) VALUES ('30', '$_POST[consecutivo]', '$comprobante_recaudo_reversion', '$_POST[codigo]')";
            mysqli_query($linkbd,$sqlReversar);

            //cambia estado a pendiente de pago
            $sqlCorte = "UPDATE srvcortes_detalle SET estado_pago = 'S' WHERE numero_facturacion = $numero_factura";
            mysqli_query($linkbd,$sqlCorte);

            $sqlRecibo = "UPDATE srvrecibo_caja SET estado = 'N' WHERE consecutivo = '$_POST[consecutivo]'";
            mysqli_query($linkbd,$sqlRecibo);

            $sqlReversarFactura = "INSERT INTO srvreversion_recibo_caja (consecutivo_recibo_caja, fecha, concepto) VALUES ('$_POST[consecutivo]', '$fecha', '$_POST[concepto]')";
            
            if(mysqli_query($linkbd,$sqlReversarFactura))
            {
                echo "<script>despliegamodalm('visible','1','Se ha almacenado con Exito');</script>";
            }
            else
            {
                echo"<script>despliegamodalm('visible','2','No se pudo ejecutar la peticion: $e');</script>";
            }
        }
    ?>
</body>
</html>