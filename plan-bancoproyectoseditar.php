<?php
	header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=utf8");
	require 'comun.inc';
	require 'funciones.inc';
	require 'validaciones.inc';
	require 'conversor.php';
	session_start();
	$linkbd=conectar_v7();
	cargarcodigopag($_GET['codpag'],$_SESSION["nivel"]);
	date_default_timezone_set("America/Bogota");
?>
<!DOCTYPE >
<html lang="es">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=9"/>
		<title>IDEAL 10 - Planeaci&oacute;n Estrat&eacute;gica</title>
		<link href="css/css2.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<link href="css/css4.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<link href="css/tabs.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css"/>
		<script src="vue/vue.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script type="text/javascript" src="css/programas.js?<?php echo date('d_m_Y_h_i_s');?>"> </script>
		<style>
			.modal-mask
			{
				position: fixed;
				z-index: 9998;
				top: 0;
				left: 0;
				width: 100%;
				height: 100%;
				background-color: rgba(0, 0, 0, .5);
				display: table;
				transition: opacity .3s ease;
			}
			.modal-wrapper 
			{
				display: table-cell;
				vertical-align: middle;
			}
			.modal-container
			{
				width: 60%;
				margin: 0px auto;
				padding: 20px 30px;
				text-align: left;
				background:linear-gradient(#99bbcc, #B6CEDA);
				border-radius: 2px;
				box-shadow: 0 2px 8px rgba(0, 0, 0, .33);
				transition: all .3s ease;
			}
			.modal-container1
			{
				width: 50%;
				margin: 0px auto;
				padding: 20px 30px;
				text-align: left;
				background:linear-gradient(#99bbcc, #B6CEDA);
				border-radius: 10px;
				box-shadow: 0 2px 8px rgba(0, 0, 0, .33);
				transition: all .3s ease;
			}
			.modal-container2
			{
				width: 80%;
				margin: 0px auto;
				padding: 20px 30px;
				text-align: left;
				background:linear-gradient(#99bbcc, #B6CEDA);
				border-radius: 2px;
				box-shadow: 0 2px 8px rgba(0, 0, 0, .33);
				transition: all .3s ease;
			}
			.modal-container3
			{
				width: 90%;
				margin: 0px auto;
				padding: 20px 30px;
				text-align: left;
				background:linear-gradient(#99bbcc, #B6CEDA);
				border-radius: 2px;
				box-shadow: 0 2px 8px rgba(0, 0, 0, .33);
				transition: all .3s ease;
			}
			[v-cloak]{display : none;}
			footer
			{
				text-align: right;
			}
		</style>
		<?php titlepag();?>
	</head>
	<body>
		<div id="myapp" v-cloak>
			<table>
				<tr><script>barra_imagenes("plan");</script><?php cuadro_titulos();?></tr>
				<tr><?php menu_desplegable("plan");?></tr>
				<tr>
					<td colspan="3" class="cinta">
						<img src="imagenes/add.png" onClick="location.href='plan-bancoproyctos.php'" class="mgbt" title="Nuevo" />
						<img src="imagenes/guarda.png" title="Guardar" v-on:click="preguntaguardar('1')" class="mgbt"/>
						<img src="imagenes/busca.png" onClick="location.href='plan-buscabancoproyectos.php'" class="mgbt" title="Buscar"/>
						<img src="imagenes/nv.png" onClick="mypop=window.open('plan-principal.php','',''); mypop.focus();" class="mgbt" title="Nueva Ventana">
						<img src="imagenes/iratras.png" title="Atr&aacute;s" onClick="location.href='plan-buscabancoproyectos.php'" class="mgbt"/>
					</td>
				</tr>
			</table>
			<table class="inicio ancho">
				<tr>
					<td class="titulos" colspan="8" >Ingresar Proyecto</td>
					<td class="cerrar" style="width:7%" onClick="location.href='plan-principal.php'">Cerrar</td>
				</tr>
				<tr>
					<td class="tamano01" style="width:4cm">Secci&oacute;n Presupuestal:</td>
					<td>
						<input type="text" v-model="unidadejecutora" v-on:dblclick='toggleModalUnidadEje' style="width:100%;height:30px;" v-bind:class="unidadejecutoradobleclick"  autocomplete="off" readonly/>
						<input type="hidden" v-model="cunidadejecutora"/>
					</td>

					<td class="tamano01" style="width:7%">Vigencia:</td>
					<td style="width:7%">
						<select v-model="vigencia" style="width:100%">
							<option v-for="year in years" :value="year[0]">{{ year[0] }}</option>
						</select>
					</td>

				</tr>
				<tr>
					<td class="tamano01" style="width:3cm">C&oacute;digo BPIM:</td>
					<td style="width:20%"><input type="text" v-model="codigo" style="width:100%;height:30px;" ref="codigo" maxlength="13" @keypress="onlyNumber" @paste="onPaste" readonly></td>
					
					<td class="tamano01" style="width:3cm">Nombre:</td>
					<td colspan="3"><input type="text" v-model="nombre" style="width:100%;height:30px;" ref="nombre"/></td>
				</tr>
				<tr>
					<td class="tamano01">Descripci&oacute;n:</td>
					<td colspan="5"><input type="text" v-model="descripcion" style="width:100%;height:30px;" ref="descripcion"/></td>
				</tr>
				<tr>
					<td class="tamano01">Sector:</td>
					<td colspan="3"><input type="text" v-model="sector" v-on:dblclick='toggleModal' style="width:100%;height:30px;" v-bind:class="sectordobleclick" autocomplete="off" readonly/><input type="hidden" v-model="csector"/></td>
					<td class="tamano01">Programa:</td>
					<td colspan="3"><input type="text"  v-model="programa" v-on:dblclick='toggleModal2' style="width:100%;height:30px;" v-bind:class="programadobleclick" readonly/><input type="hidden" v-model="cprograma"/></td>
				</tr>
				<tr>
					<td class="tamano01">Subprograma:</td>
					<td colspan="3"><input type="text"  v-model="subprograma" style="width:100%;height:30px;" readonly/><input type="hidden" v-model="csubprograma"/></td>
					<td class="tamano01">Indicador Producto:</td>
					<td colspan="3"><input type="text" v-model="indicadorpro" v-on:dblclick='toggleModal3' style="width:100%;height:30px;" v-bind:class=" indicadordobleclick" autocomplete="off" v-on:keyup="validaindicadorproducto(indicadorpro)"/><input type="hidden" v-model="cindicadorpro"/></td>
				</tr>
				<tr>
					<td class="tamano01">Producto:</td>
					<td colspan="4"><input type="text"  v-model="producto" style="width:100%;height:30px;"  autocomplete="off" readonly/><input type="hidden" v-model="cproducto"/></td>
					<td style=" height: 30px;"><em class="botonflecha" v-on:click="agregarproducto()">Agregar</em></td>
				</tr>
			</table>
			<div class='subpantalla' style='height:39%; width:99.5%; margin-top:0px; overflow-x:hidden'>
				<table class='inicio inicio--no-shadow'>
					<tbody>
						<tr class="titulos">
							<td>Producto</td>
							<td>Indicador</td>
							<td>Eliminar</td>
							<td></td>
						</tr>
						<tr v-for="(vcproducto, index) in selecproductosa" v-bind:class="index % 2 ? 'saludo1a' : 'saludo2'" style='text-rendering: optimizeLegibility; cursor: pointer !important; style=\"cursor: hand\"' >

							<td width="50%" style="font: 120% sans-serif; padding-left:10px">{{ vcproducto[1] }}</td>
							<td style="font: 120% sans-serif; padding-left:10px">{{ vcproducto[3] }}</td>
							<td  v-on:click="eliminaproducto(index)"><img src='imagenes/del.png'></td>

						</tr>
					</tbody>
				</table>
			</div>
				
			
				<div v-show="showMensaje">
					<transition name="modal">
						<div class="modal-mask">
							<div class="modal-wrapper">
								<div class="modal-container1">
									<table id='ventanamensaje1' class='inicio' style="border-radius: 10px;">
										<tr >
											<td class="titulosmensajes1" v-bind:style="{color:colortitulosmensaje,}" style=" text-shadow: 7px 4px 5px grey;font-style: italic;border-radius: 50px;">{{titulomensaje}}</td>
										</tr>
										<tr>
											<td class='.cuerpomensajes1' style="text-align:center;"><h3 style="font-size: 20px;font-family:Gotham, 'Helvetica Neue', Helvetica, Arial, 'sans-serif';font-style: italic;">{{ contenidomensaje }}</h3></td>
										</tr>
										<tr>
											<td class='.cuerpomensajes1' style="padding: 14px;text-align:center">
												<em name="continuar" id="continuar" class="botonflecha" @click="toggleMensaje()">Continuar</em>
											</td>
										</tr>
									</table>
								</div>
							</div>
						</div>
					</transition>
				</div>
				<div v-show="showMensajeSN">
					<transition name="modal">
						<div class="modal-mask">
							<div class="modal-wrapper">
								<div class="modal-container1">
									<table id='ventanamensaje1' class='inicio' style="border-radius: 10px;">
										<tr >
											<td class="titulosmensajes1" v-bind:style="{color:colortitulosmensaje,}" style=" text-shadow: 7px 4px 5px grey;font-style: italic;border-radius: 50px;">{{titulomensaje}}</td>
										</tr>
										<tr>
											<td class='.cuerpomensajes1' style="text-align:center;"><h3 style="font-size: 20px;font-family:Gotham, 'Helvetica Neue', Helvetica, Arial, 'sans-serif';font-style: italic;">{{ contenidomensaje }}</h3></td>
										</tr>
										<tr>
											<td class='.cuerpomensajes1' style="padding: 14px;text-align:center">
												<em name="continuar" id="continuar" class="botonflechaverde" @click="toggleMensajeSN('1','S')">Aceptar</em> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
												<em name="continuar" id="continuar" class="botonflecharoja" @click="toggleMensajeSN('1','N')">Cancelar</em>
											</td>
										</tr>
									</table>
								</div>
							</div>
						</div>
					</transition>
				</div>
				<div v-show="showModalUnidadEj">
					<transition name="modal">
						<div class="modal-mask">
							<div class="modal-wrapper">
								<div class="modal-container">
									<table class="inicio ancho">
										<tr>
											<td class="titulos" colspan="2" >SELECCIONAR UNIDAD EJECUTORA</td>
											<td class="cerrar" style="width:7%" @click="showModalUnidadEj = false">Cerrar</td>
										</tr>
										<!-- <tr>
											<td class="tamano01" style="width:3cm">Unidad Ejecutora:</td>
											<td><input type="text" class="form-control" placeholder="Buscar por nombre de unidad ejecutora" v-on:keyup="searchMonitorUnidadEj" v-model="search.keyword" style="width:100%" /></td>
										</tr> -->
									</table>
									<table>
										<thead>
											<tr>
												<td class='titulos' style="font: 160% sans-serif; border-radius: 5px 0 0 0; width:20%;">C&oacute;digo</td>
												<td class='titulos' style="font: 160% sans-serif; width:60%;">Nombre</td>
												<td class='titulos' style="font: 160% sans-serif; border-radius: 0 5px 0 0;">Aplicaci&oacute;n</td>
											</tr>
										</thead>
									</table>
									<div style='margin: 0px 5px 5px; border-radius: 0 0 5px 5px; height: 200px; overflow: scroll; overflow-x: hidden; background: white;'>
										<table class='inicio inicio--no-shadow'>
											<tbody>
												<tr v-for="(unidadeejecutora,index) in unidadesejecutoras" v-on:click="cargaunidadejecutora(unidadeejecutora[0],unidadeejecutora[1])" v-bind:class="index % 2 ? 'saludo1a' : 'saludo2'" style='text-rendering: optimizeLegibility; cursor: pointer !important; style=\"cursor: hand\"'>
													<td style="font: 120% sans-serif; padding-left:10px; width:20%;">{{ unidadeejecutora[0] }}</td>
													<td style="font: 120% sans-serif; padding-left:10px; width:60%;">{{ unidadeejecutora[1] }}</td>
													<td style="font: 120% sans-serif; padding-left:10px">{{ unidadeejecutora[2] }}</td>
												</tr>
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
					</transition>
				</div>
				<div v-show="showModal">
					<transition name="modal">
						<div class="modal-mask">
							<div class="modal-wrapper">
								<div class="modal-container">
									<table class="inicio ancho">
										<tr>
											<td class="titulos" colspan="2" >SELECCIONAR SECTOR</td>
											<td class="cerrar" style="width:7%" @click="showModal = false">Cerrar</td>
										</tr>
										<tr>
											<td class="tamano01" style="width:4cm">C&oacute;digo o Sector:</td>
											<td><input type="text" class="form-control" placeholder="Buscar por c&oacute;digo o nombre de sector" v-on:keyup="searchMonitor" v-model="search.keyword" style="width:100%" /></td>
										</tr>
									</table>
									<table>
										<thead>
											<tr>
												<td class='titulos' style="font: 160% sans-serif; border-radius: 5px 0 0 0; width:20%;">C&oacute;digo</td>
												<td class='titulos' style="font: 160% sans-serif; width:60%;">Nombre</td>
												<td class='titulos' style="font: 160% sans-serif; border-radius: 0 5px 0 0;">Aplicaci&oacute;n</td>
											</tr>
										</thead>
									</table>
									<div style='margin: 0px 5px 5px; border-radius: 0 0 5px 5px; height: 400px; overflow: scroll; overflow-x: hidden; background: white;'>
										<table class='inicio inicio--no-shadow'>
											<tbody>
												<tr v-for="(sector,index) in sectores" v-on:click="cargasector(sector[0],sector[1])" v-bind:class="index % 2 ? 'saludo1a' : 'saludo2'" style='text-rendering: optimizeLegibility; cursor: pointer !important; style=\"cursor: hand\"'>
													<td style="font: 120% sans-serif; padding-left:10px; width:20%;">{{ sector[0] }}</td>
													<td style="font: 120% sans-serif; padding-left:10px; width:60%;">{{ sector[1] }}</td>
													<td style="font: 120% sans-serif; padding-left:10px">{{ sector[2] }}</td>
												</tr>
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
					</transition>
				</div>
				<div v-show="showModal2">
					<transition name="modal">
						<div class="modal-mask">
							<div class="modal-wrapper">
								<div class="modal-container2">
									<table class="inicio ancho">
										<tr>
											<td class="titulos" colspan="2" >SELECCIONAR PROGRAMA</td>
											<td class="cerrar" style="width:7%" @click="showModal2 = false">Cerrar</td>
										</tr>
										<tr>
											<td class="tamano01" style="width:5cm">C&oacute;digo o nombre de Programa:</td>
											<td><input type="text" class="form-control" placeholder="Buscar por c&oacute;digo o nombre del programa" v-on:keyup="searchMonitorPrograms" v-model="searchProgram.keywordProgram" style="width:100%" /></td>
										</tr>
									</table>
									<table>
										<thead>
											<tr>
												<td width="10%" class='titulos' style="font: 160% sans-serif; border-radius: 5px 0 0 0;">C&oacute;digo</td>
												<td width="30%" class='titulos' style="font: 160% sans-serif;">Nombre programa</td>
												<td width="20%" class='titulos' style="font: 160% sans-serif;">C&oacute;digo subprograma</td>
												<td width="20%" class='titulos' style="font: 160% sans-serif;">Nombre subprograma</td>
												<td width="20%" class='titulos' style="font: 160% sans-serif; border-radius: 0 5px 0 0;">Aplicaci&oacute;n</td>
											</tr>
										</thead>
									</table>
									<div style='margin: 0px 5px 5px; border-radius: 0 0 5px 5px; height: 200px; overflow: scroll; overflow-x: hidden; background: white;'>
										<table class='inicio inicio--no-shadow'>
											<tbody>
												<tr v-for="(programa,index) in programas_subprogramas" v-on:click="cargaprograma( programa[0],programa[1], programa[2], programa[3])" v-bind:class="index % 2 ? 'saludo1a' : 'saludo2'" style='text-rendering: optimizeLegibility; cursor: pointer !important; style=\"cursor: hand\"'>
													<td width="10%" style="font: 120% sans-serif; padding-left:10px">{{ programa[0] }}</td>
													<td width="30%" style="font: 120% sans-serif; padding-left:10px">{{ programa[1] }}</td>
													<td width="20%" style="font: 120% sans-serif; padding-left:10px">{{ programa[2] }}</td>
													<td width="20%" style="font: 120% sans-serif; padding-left:10px">{{ programa[3] }}</td>
													<td width="20%" style="font: 120% sans-serif; padding-left:10px">{{ programa[4] }}</td>
												</tr>
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
					</transition>
				</div>
				<div v-show="showModal3">
					<transition name="modal">
						<div class="modal-mask">
							<div class="modal-wrapper">
								<div class="modal-container3">
									<table class="inicio ancho">
										<tr>
											<td class="titulos" colspan="2" >SELECCIONAR PRODUCTOS</td>
											<td class="cerrar" style="width:7%" @click="showModal3 = false">Cerrar</td>
										</tr>
										<tr>
											<td class="tamano01" style="width:8cm">C&oacute;digo, producto o indicador producto:</td>
											<td><input type="text" class="form-control" placeholder="Buscar por c&oacute;digo, nombre de producto o indicador producto" v-on:keyup="searchMonitorProducts" v-model="searchProduct.keywordProduct" style="width:100%" /></td>
										</tr>
									</table>
									<table>
										<thead>
											<tr>
												<td width="10%" class='titulos' style="font: 160% sans-serif; padding-left:10px; border-radius: 5px 0 0 0;">C&oacute;digo</td>
												<td width="20%" class='titulos' style="font: 160% sans-serif; ">Producto</td>
												<td width="30%" class='titulos' style="font: 160% sans-serif; ">Descripci&oacute;n</td>
												<td width="10%" class='titulos' style="font: 160% sans-serif; ">Medio a traves</td>
												<td width="5%" class='titulos' style="font: 160% sans-serif; ">C&oacute;digo indicador</td>
												<td width="10%" class='titulos' style="font: 160% sans-serif;">Indicador producto</td>
												<td width="10%" class='titulos' style="font: 160% sans-serif;">Unidad medida</td>
												<td width="5%" class='titulos' style="font: 160% sans-serif; padding-right:10px; border-radius: 0 5px 0 0;">Indicador principal</td>
											</tr>
										</thead>
									</table>
									<div style='margin: 0px 5px 5px; border-radius: 0 0 5px 5px; height: 400px; overflow: scroll; overflow-x: hidden; background: white;'>
										<table class='inicio inicio--no-shadow'>
											<tbody>
												<tr v-for="(producto,index) in productos" v-on:click="cargaproducto(producto[0],producto[1],producto[4],producto[5])" v-bind:class="index % 2 ? 'saludo1a' : 'saludo2'" style='text-rendering: optimizeLegibility; cursor: pointer !important; style=\"cursor: hand\"'>
													<td width="10%" style="font: 120% sans-serif; padding-left:10px">{{ producto[0] }}</td>
													<td width="20%" style="font: 120% sans-serif; padding-left:10px">{{ producto[1] }}</td>
													<td width="30%" style="font: 120% sans-serif; padding-left:10px">{{ producto[2] }}</td>
													<td width="10%" style="font: 120% sans-serif; padding-left:10px">{{ producto[3] }}</td>
													<td width="5%" style="font: 120% sans-serif; padding-left:10px">{{ producto[4] }}</td>
													<td width="10%" style="font: 120% sans-serif; padding-left:10px">{{ producto[5] }}</td>
													<td width="10%" style="font: 120% sans-serif; padding-left:10px">{{ producto[6] }}</td>
													<td width="5%" style="font: 120% sans-serif; padding-left:10px">{{ producto[7] }}</td>
												</tr>
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
					</transition>
				</div>
				<div v-show="showModal4">
					<transition name="modal">
						<div class="modal-mask">
							<div class="modal-wrapper">
								<div class="modal-container">
									<table class="inicio ancho">
										<tr>
											<td class="titulos" colspan="2" >SELECCIONAR CUENTAS PRESUPUESTALES</td>
											<td class="cerrar" style="width:7%" @click="showModal4 = false">Cerrar</td>
										</tr>
										<tr>
											<td class="tamano01" style="width:3cm">Descripci&oacute;n:</td>
											<td><input type="text" class="form-control" placeholder="Buscar por descripcion cuenta" v-on:keyup="searchMonitorCuentasPresupuestales" v-model="searchCuentaPresupuestal.keywordCuentaPresupuestal" style="width:100%" /></td>
										</tr>
									</table>
									<table>
										<thead>
											<tr>

												<td class='titulos' style="width:30%; font: 160% sans-serif; ">C&oacute;digo</td>
												<td class='titulos' style="font: 160% sans-serif; ">Nombre</td>
												<td class='titulos' style="width:10%; font: 160% sans-serif; ">Tipo</td>

											</tr>
										</thead>
									</table>
									<div style='margin: 0px 5px 5px; border-radius: 0 0 5px 5px; height: 200px; overflow: scroll; overflow-x: hidden; background: white;'>
										<table class='inicio inicio--no-shadow'>
											<tbody>
												<tr v-for="(cuentapre,index) in cuentaspres" v-on:click="cargacuenta(cuentapre[0],cuentapre[1],cuentapre[2])"  v-bind:class="index % 2 ? 'saludo1a' : 'saludo2'" style='text-rendering: optimizeLegibility; cursor: pointer !important; style=\"cursor: hand\"'>
													<td style="font: 120% sans-serif; padding-left:10px; width: 30%;">{{ cuentapre[0] }}</td>
													<td style="font: 120% sans-serif; padding-left:10px">{{ cuentapre[1] }}</td>
													<td  style="width: 8%; font: 120% sans-serif; padding-left:10px">{{ cuentapre[2] }}</td>
												</tr>
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
					</transition>
				</div>
				<div v-show="showModal5">
					<transition name="modal">
						<div class="modal-mask">
							<div class="modal-wrapper">
								<div class="modal-container">
									<table class="inicio ancho">
										<tr>
											<td class="titulos" colspan="2" >SELECCIONAR SECCION</td>
											<td class="cerrar" style="width:7%" @click="showModal5 = false">Cerrar</td>
										</tr>
										<tr>
											<td class="tamano01" style="width:3cm">NOMBRE:</td>
											<td><input type="text" class="form-control" placeholder="Buscar por nombre seccion" v-on:keyup="searchMonitorProducts" v-model="searchProduct.keywordProduct" style="width:100%" /></td>
										</tr>
									</table>
									<table>
										<thead>
											<tr>
												<td class='titulos' style="width:20%; font: 160% sans-serif; ">C&oacute;digo</td>
												<td class='titulos' style="font: 160% sans-serif; ">Nombre</td>
											</tr>
										</thead>
									</table>
									<div style='margin: 0px 5px 5px; border-radius: 0 0 5px 5px; height: 200px; overflow: scroll; overflow-x: hidden; background: white;'>
										<table class='inicio inicio--no-shadow'>
											<tbody>
												<tr v-for="(seccion,index) in secciones" v-on:click="cargaseccion(seccion[0],seccion[1])"  v-bind:class="index % 2 ? 'saludo1a' : 'saludo2'" style='text-rendering: optimizeLegibility; cursor: pointer !important; style=\"cursor: hand\"'>
													<td style="font: 120% sans-serif; padding-left:10px;width:20%;">{{ seccion[0] }}</td>
													<td style="font: 120% sans-serif; padding-left:10px">{{ seccion[1] }}</td>
												</tr>
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
					</transition>
				</div>
				<div v-show="showModal6">
					<transition name="modal">
						<div class="modal-mask">
							<div class="modal-wrapper">
								<div class="modal-container">
									<table class="inicio ancho">
										<tr>
											<td class="titulos" colspan="2" >SELECCIONAR DIVISION</td>
											<td class="cerrar" style="width:7%" @click="showModal6 = false">Cerrar</td>
										</tr>
										<tr>
											<td class="tamano01" style="width:3cm">NOMBRE:</td>
											<td><input type="text" class="form-control" placeholder="Buscar por nombre de divisiï¿½n" v-on:keyup="searchMonitorProducts" v-model="searchProduct.keywordProduct" style="width:100%" /></td>
										</tr>
									</table>
									<table>
										<thead>
											<tr>
												<td class='titulos' style="width:20%; font: 160% sans-serif; ">C&oacute;digo</td>
												<td class='titulos' style="font: 160% sans-serif; ">Nombre</td>
											</tr>
										</thead>
									</table>
									<div style='margin: 0px 5px 5px; border-radius: 0 0 5px 5px; height: 200px; overflow: scroll; overflow-x: hidden; background: white;'>
										<table class='inicio inicio--no-shadow'>
											<tbody>
												<tr v-for="(division,index) in divisiones" v-on:click="cargadivision(division[0],division[1])"  v-bind:class="index % 2 ? 'saludo1a' : 'saludo2'" style='text-rendering: optimizeLegibility; cursor: pointer !important; style=\"cursor: hand\"'>
													<td style="font: 120% sans-serif; padding-left:10px; width:20%;">{{ division[0] }}</td>
													<td style="font: 120% sans-serif; padding-left:10px">{{ division[1] }}</td>
												</tr>
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
					</transition>
				</div>
				<div v-show="showModal7">
					<transition name="modal">
						<div class="modal-mask">
							<div class="modal-wrapper">
								<div class="modal-container">
									<table class="inicio ancho">
										<tr>
											<td class="titulos" colspan="2" >SELECCIONAR GRUPO</td>
											<td class="cerrar" style="width:7%" @click="showModal7 = false">Cerrar</td>
										</tr>
										<tr>
											<td class="tamano01" style="width:3cm">NOMBRE:</td>
											<td><input type="text" class="form-control" placeholder="Buscar por nombre del grupo" v-on:keyup="searchMonitorProducts" v-model="searchProduct.keywordProduct" style="width:100%" /></td>
										</tr>
									</table>
									<table>
										<thead>
											<tr>
												<td class='titulos' style="width:20%; font: 160% sans-serif; ">C&oacute;digo</td>
												<td class='titulos' style="font: 160% sans-serif; ">Nombre</td>
											</tr>
										</thead>
									</table>
									<div style='margin: 0px 5px 5px; border-radius: 0 0 5px 5px; height: 200px; overflow: scroll; overflow-x: hidden; background: white;'>
										<table class='inicio inicio--no-shadow'>
											<tbody>
												<tr v-for="(grupo,index) in grupos" v-on:click="cargagrupo(grupo[0],grupo[1])"  v-bind:class="index % 2 ? 'saludo1a' : 'saludo2'" style='text-rendering: optimizeLegibility; cursor: pointer !important; style=\"cursor: hand\"'>
													<td style="font: 120% sans-serif; padding-left:10px;width:20%;">{{ grupo[0] }}</td>
													<td style="font: 120% sans-serif; padding-left:10px">{{ grupo[1] }}</td>
												</tr>
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
					</transition>
				</div>
				<div v-show="showModal8">
					<transition name="modal">
						<div class="modal-mask">
							<div class="modal-wrapper">
								<div class="modal-container">
									<table class="inicio ancho">
										<tr>
											<td class="titulos" colspan="2" >SELECCIONAR CLASE</td>
											<td class="cerrar" style="width:7%" @click="showModal8 = false">Cerrar</td>
										</tr>
										<tr>
											<td class="tamano01" style="width:3cm">NOMBRE:</td>
											<td><input type="text" class="form-control" placeholder="Buscar por nombre de la clase" v-on:keyup="searchMonitorProducts" v-model="searchProduct.keywordProduct" style="width:100%" /></td>
										</tr>
									</table>
									<table>
										<thead>
											<tr>
												<td class='titulos' style="width:20%; font: 160% sans-serif; ">C&oacute;digo</td>
												<td class='titulos' style="font: 160% sans-serif; ">Nombre</td>
											</tr>
										</thead>
									</table>
									<div style='margin: 0px 5px 5px; border-radius: 0 0 5px 5px; height: 200px; overflow: scroll; overflow-x: hidden; background: white;'>
										<table class='inicio inicio--no-shadow'>
											<tbody>
												<tr v-for="(clase,index) in clases" v-on:click="cargaclase(clase[0],clase[1])"  v-bind:class="index % 2 ? 'saludo1a' : 'saludo2'" style='text-rendering: optimizeLegibility; cursor: pointer !important; style=\"cursor: hand\"'>
													<td style="font: 120% sans-serif; padding-left:10px; width:20%;">{{ clase[0] }}</td>
													<td style="font: 120% sans-serif; padding-left:10px">{{ clase[1] }}</td>
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
					</transition>
				</div>
				<div v-show="showModal9">
					<transition name="modal">
						<div class="modal-mask">
							<div class="modal-wrapper">
								<div class="modal-container">
									<table class="inicio ancho">
										<tr>
											<td class="titulos" colspan="2" >SELECCIONAR SUBCLASE</td>
											<td class="cerrar" style="width:7%" @click="showModal9 = false">Cerrar</td>
										</tr>
										<tr>
											<td class="tamano01" style="width:3cm">NOMBRE:</td>
											<td><input type="text" class="form-control" placeholder="Buscar por nombre de la subclase" v-on:keyup="buscarsubclases" v-model="searchsubclase.keywordsubclase" style="width:100%" /></td>
										</tr>
									</table>
									<table>
										<thead>
											<tr>
												<td class='titulos' style="font: 160% sans-serif; padding-left:10px; border-radius: 5px 0 0 0; width:20%;">C&oacute;digo</td>
												<td class='titulos' style="font: 160% sans-serif; ">Nombre</td>
												<td class='titulos' style="font: 160% sans-serif; width:15%;">CIIU Rev. 4 A.C. </td>
												<td class='titulos' style="font: 160% sans-serif; width:25%;">Sistema Armonizado 2012</td>
												<td class='titulos' style="font: 160% sans-serif; width:10%;">CPC 2 A.C.</td>
											</tr>
										</thead>
									</table>
									<div style='margin: 0px 5px 5px; border-radius: 0 0 5px 5px; height: 200px; overflow: scroll; overflow-x: hidden; background: white;'>
										<table class='inicio inicio--no-shadow'>
											<tbody>
												<tr v-for="(subclase,index) in subClases" v-bind:class="index % 2 ? 'saludo1a' : 'saludo2'" v-on:click="cargasubclase(subclase[0],subclase[1])"  style='text-rendering: optimizeLegibility; cursor: pointer !important; style=\"cursor: hand\"'>
													<td width="20%" style="font: 120% sans-serif; padding-left:10px">{{ subclase[0] }}</td>
													<td width="30%" style="font: 120% sans-serif; padding-left:10px">{{ subclase[1] }}</td>
													<td width="15%" style="font: 120% sans-serif; padding-left:10px">{{ subclase[2] }}</td>
													<td width="25%" style="font: 120% sans-serif; padding-left:10px">{{ subclase[3] }}</td>
													<td width="10%" style="font: 120% sans-serif; padding-left:10px">{{ subclase[4] }}</td>
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
					</transition>
				</div>
				<div v-show="showModal10">
					<transition name="modal">
						<div class="modal-mask">
							<div class="modal-wrapper">
								<div class="modal-container3">
									<table class="inicio ancho">
										<tr>
											<td class="titulos" colspan="2" >SELECCIONAR FUENTE</td>
											<td class="cerrar" style="width:7%" @click="showModal10 = false">Cerrar</td>
										</tr>
										<tr>
											<td class="tamano01" style="width:3cm">NOMBRE:</td>
											<td><input type="text" class="form-control" placeholder="Buscar por nombre de la Fuente" v-on:keyup="searchMonitorfuentes" v-model="searchfuentes.keyword" style="width:100%" /></td>
										</tr>
									</table>
									<table>
										<thead>
											<tr>
												<td class='titulos' style="font: 160% sans-serif; border-radius: 5px 0px 0px 0px; width:6.6%;">Id fuente</td>
												<td class='titulos' style="font: 160% sans-serif; width:10%;">Entidad financiadora</td>
											</tr>
										</thead>
									</table>
									<div style='margin: 0px 5px 5px; border-radius: 0 0 5px 5px; height: 200px; overflow: scroll; overflow-x: hidden; background: white;'>
										<table class='inicio inicio--no-shadow'>
											<tbody>
												<tr v-for="(result,index) in results" v-on:click="cargafuente(result[1],result[2])"  v-bind:class="index % 2 ? 'saludo1a' : 'saludo2'" style='text-rendering: optimizeLegibility; cursor: pointer !important; style=\"cursor: hand\"'>
													<td width="40%" style="font: 160% sans-serif;">{{ result[1] }}</td>
													<td width="60%" style="font: 160% sans-serif;">{{ result[2] }}</td>
												</tr>
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
					</transition>
				</div>
				<div v-show="showModal11">
					<transition name="modal">
						<div class="modal-mask">
							<div class="modal-wrapper">
								<div class="modal-container3">
									<table class="inicio ancho">
										<tr>
											<td class="titulos" colspan="2" >SELECCIONAR FUENTE</td>
											<td class="cerrar" style="width:7%" @click="showModal11 = false">Cerrar</td>
										</tr>
										<tr>
											<td class="tamano01" style="width:3cm">NOMBRE:</td>
											<td><input type="text" class="form-control" placeholder="Buscar por nombre de la Fuente" v-on:keyup="searchMonitorProducts" v-model="searchProduct.keywordProduct" style="width:100%" /></td>
										</tr>
									</table>
									<table>
										<thead>
											<tr>
												<td width="1%" class='titulos' style="font: 100% sans-serif; border-radius: 5px 0px 0px;">No</td>
												<td width="8%" class='titulos' style="font: 100% sans-serif;">Id entidad</td>
												<td width="8%" class='titulos' style="font: 100% sans-serif;">Nit</td>
												<td width="34%" class='titulos' style="font: 100% sans-serif;">Nombre</td>
												<td width="4%" class='titulos' style="font: 100% sans-serif;">Sector</td>
												<td width="5%" class='titulos' style="font: 100% sans-serif;">Subsector</td>
												<td width="4%" class='titulos' style="font: 100% sans-serif;">Tipo</td>
												<td width="5%" class='titulos' style="font: 100% sans-serif;">Supra regional</td>
												<td width="5%" class='titulos' style="font: 100% sans-serif;">Nivel territorial</td>
												<td width="5%" class='titulos' style="font: 100% sans-serif;">Depto</td>
												<td width="5%" class='titulos' style="font: 100% sans-serif;">Municipio</td>
												<td width="5%" class='titulos' style="font: 100% sans-serif;">Consecutivo</td>
												<td width="15%" class='titulos' style="font: 100% sans-serif; text-align:center; border-radius: 0px 5px 0px 0px;">C&oacute;digo CUIN</td>
											</tr>
										</thead>
									</table>
									<div style='margin: 0px 5px 5px; border-radius: 0 0 5px 5px; height: 200px; overflow: scroll; overflow-x: hidden; background: white;'>
										<table class='inicio inicio--no-shadow'>
											<tbody>
												<tr v-for="(clacuin,index) in codigoscuin" v-on:click="cargacodigocuin(clacuin[2],clacuin[3],clacuin[4],clacuin[13])" v-bind:class="index % 2 ? 'saludo1a' : 'saludo2'" style='text-rendering: optimizeLegibility; cursor: pointer !important; style=\"cursor: hand\"'>
													<td width="1%" style="font: 120% sans-serif;">{{ clacuin[1] }}</td>
													<td width="8%" style="font: 120% sans-serif;">{{ clacuin[2] }}</td>
													<td width="8%" style="font: 120% sans-serif;">{{ clacuin[3] }}</td>
													<td width="34%" style="font: 120% sans-serif;">{{ clacuin[4] }}</td>
													<td width="4%" style="font: 120% sans-serif;">{{ clacuin[5] }}</td>
													<td width="5%" style="font: 120% sans-serif;">{{ clacuin[6] }}</td>
													<td width="4%" style="font: 120% sans-serif;">{{ clacuin[7] }}</td>
													<td width="5%" style="font: 120% sans-serif;">{{ clacuin[8] }}</td>
													<td width="5%" style="font: 120% sans-serif;">{{ clacuin[9] }}</td>
													<td width="5%" style="font: 120% sans-serif;">{{ clacuin[10] }}</td>
													<td width="5%" style="font: 120% sans-serif;">{{ clacuin[11] }}</td>
													<td width="5%" style="font: 120% sans-serif;">{{ clacuin[12] }}</td>
													<td width="10%" style="font: 120% sans-serif;">{{ clacuin[13] }}</td>
												</tr>
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
					</transition>
				</div>
				<div v-show="showModal12">
					<transition name="modal">
						<div class="modal-mask">
							<div class="modal-wrapper">
								<div class="modal-container2">
									<table class="inicio ancho">
										<tr>
											<td class="titulos" colspan="2" >SELECCIONAR PRODUCTO</td>
											<td class="cerrar" style="width:7%" @click="showModal12 = false">Cerrar</td>
										</tr>
										<tr>
											<td class="tamano01" style="width:3cm">Producto:</td>
											<td><input type="text" class="form-control" placeholder="Buscar por nombre del producto" v-on:keyup="buscarsubproductos" v-model="searchsubproductos.keywordsubproductos" style="width:100%" /></td>
										</tr>
									</table>
									<table>
										<thead>
											<tr>
												<td width="10%" class='titulos' style="font: 160% sans-serif; border-radius: 5px 0 0 0;">C&oacute;digo</td>
												<td width="30%" class='titulos' style="font: 160% sans-serif;">Nombre programa</td>
												<td width="20%" class='titulos' style="font: 160% sans-serif;">Ciiu</td>
												<td width="20%" class='titulos' style="font: 160% sans-serif;">Sistema Armonizado</td>
												<td width="20%" class='titulos' style="font: 160% sans-serif; border-radius: 0 5px 0 0;">CPC</td>
											</tr>
										</thead>
									</table>
									<div style='margin: 0px 5px 5px; border-radius: 0 0 5px 5px; height: 200px; overflow: scroll; overflow-x: hidden; background: white;'>
										<table class='inicio inicio--no-shadow'>
											<tbody>
												<tr v-for="(subprod,index) in subproductos" v-on:click="cargasubproducto(subprod[0],subprod[1])" v-bind:class="index % 2 ? 'saludo1a' : 'saludo2'" style='text-rendering: optimizeLegibility; cursor: pointer !important; style=\"cursor: hand\"'>
													<td width="10%" style="font: 120% sans-serif; padding-left:10px">{{ subprod[0] }}</td>
													<td width="30%" style="font: 120% sans-serif; padding-left:10px">{{ subprod[1] }}</td>
													<td width="20%" style="font: 120% sans-serif; padding-left:10px">{{ subprod[2] }}</td>
													<td width="20%" style="font: 120% sans-serif; padding-left:10px">{{ subprod[3] }}</td>
													<td width="20%" style="font: 120% sans-serif; padding-left:10px">{{ subprod[4] }}</td>
												</tr>
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
					</transition>
				</div>
				<div v-show="showModal13">
					<transition name="modal">
						<div class="modal-mask">
							<div class="modal-wrapper">
								<div class="modal-container">
									<table class="inicio ancho">
										<tr>
											<td class="titulos" colspan="2" >SELECCIONAR META</td>
											<td class="cerrar" style="width:7%" @click="showModal13 = false">Cerrar</td>
										</tr>
										<tr>
											<td class="tamano01" style="width:3cm">Meta:</td>
											<td><input type="text" class="form-control" placeholder="Buscar por nombre del producto" v-on:keyup="searchMonitorMetas" v-model="searchmetas.keywordmetas" style="width:100%" /></td>
										</tr>
									</table>
									<table>
										<thead>
											<tr>
												<td width="10%" class='titulos' style="font: 160% sans-serif; border-radius: 5px 0 0 0;">Id</td>
												<td width="20%" class='titulos' style="font: 160% sans-serif;">Cod. Meta</td>
												<td class='titulos' style="font: 160% sans-serif; border-radius: 0 5px 0 0;">Descripci&oacute;n</td>
											</tr>
										</thead>
									</table>
									<div style='margin: 0px 5px 5px; border-radius: 0 0 5px 5px; height: 200px; overflow: scroll; overflow-x: hidden; background: white;'>
										<table class='inicio inicio--no-shadow'>
											<tbody>
												<tr v-for="(metasvs,index) in infometas" v-on:click="cargametas(metasvs[0],metasvs[1],metasvs[2])" v-bind:class="index % 2 ? 'saludo1a' : 'saludo2'" style='text-rendering: optimizeLegibility; cursor: pointer !important; style=\"cursor: hand\"'>
													<td width="10%" style="font: 120% sans-serif; padding-left:10px">{{ metasvs[0] }}</td>
													<td width="20%" style="font: 120% sans-serif; padding-left:10px">{{ metasvs[1] }}</td>
													<td style="font: 120% sans-serif; padding-left:10px">{{ metasvs[2] }}</td>
												</tr>
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
					</transition>
				</div>
				<div v-show="showModal14">
					<transition name="modal">
						<div class="modal-mask">
							<div class="modal-wrapper">
								<div class="modal-container3">
									<table class="inicio ancho">
										<tr>
											<td class="titulos" colspan="2" >SELECCIONAR POL&Iacute;TICA P&Uacute;BLICA</td>
											<td class="cerrar" style="width:7%" @click="showModal14 = false">Cerrar</td>
										</tr>
										<tr>
											<td class="tamano01" style="width:3cm">NOMBRE:</td>
											<td><input type="text" class="form-control" placeholder="Buscar por nombre de la Fuente" v-on:keyup="searchMonitorpp" v-model="searchpoliticaspublicas.keywordpp" style="width:100%" /></td>
										</tr>
									</table>
									<table>
										<thead>
											<tr>
												<td class='titulos' style="font: 160% sans-serif; border-radius: 5px 0px 0px 0px; width:4.9%;">Codigo</td>
												<td class='titulos' style="font: 160% sans-serif; width:10%;">Nombre</td>
											</tr>
										</thead>
									</table>
									<div style='margin: 0px 5px 5px; border-radius: 0 0 5px 5px; height: 200px; overflow: scroll; overflow-x: hidden; background: white;'>
										<table class='inicio inicio--no-shadow'>
											<tbody>
												<tr v-for="(resultpp,index) in resultspp" v-on:click="cargapp(resultpp[1],resultpp[2])"  v-bind:class="index % 2 ? 'saludo1a' : 'saludo2'" style='text-rendering: optimizeLegibility; cursor: pointer !important; style=\"cursor: hand\"'>
													<td width="30%" style="font: 160% sans-serif;">{{ resultpp[1] }}</td>
													<td width="60%" style="font: 160% sans-serif;">{{ resultpp[2] }}</td>
												</tr>
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
					</transition>
				</div>
			<input type="hidden" v-model="idproyecto" :value="idproyecto=<?php echo $_GET['idr']; ?>"/>
			<input type="hidden" name="oculto" value="1">
		</div>
		<script src="Librerias/vue/vue.min.js"></script>
		<script src="Librerias/vue/axios.min.js"></script>
		<script src="vue/presupuesto_ccp/plan-bancoproyectoseditar.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
	</body>
</html>