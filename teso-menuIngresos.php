<?php
	header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=utf8");
	require "comun.inc";
	require "funciones.inc";
	require 'funcionesSP.inc.php';
	session_start();
	$linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");
	cargarcodigopag($_GET['codpag'],$_SESSION["nivel"]);
	date_default_timezone_set("America/Bogota");
?>
<!DOCTYPE >
<html lang="es">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1">
		<meta name="viewport" content="user-scalable=no">
		<title>:: IDEAL 10 - Tesorería</title>
		<link href="favicon.ico" rel="shortcut icon"/>
		<link href="css/css2z.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet"/>
		<link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet"/>
		<script type="text/javascript" src="css/programas.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
	</head>
	<body>
		<IFRAME src="alertas.php" name="alertas" id="alertas" style="display:none"></IFRAME>
		<span id="todastablas2"></span>
		<table>
			<tr><script>barra_imagenes("teso");</script><?php cuadro_titulos();?></tr>
			<tr><?php menu_desplegable("teso");?></tr>
			<tr>
				<td colspan="3" class="cinta">
					<a class="mgbt"><img src="imagenes/add2.png"/></a>
					<a class="mgbt"><img src="imagenes/guardad.png" title="Guardar" /></a>
					<a class="mgbt"><img src="imagenes/buscad.png"/></a><a href="#" class="mgbt" onClick="mypop=window.open('teso-principal.php','','');mypop.focus();"><img src="imagenes/nv.png" title="Nueva Ventana"></a>
				</td>
			</tr>
		</table>
		<form name="form2" method="post" action="">
			<?php
				/* $sqlOpciones = "SELECT nom_opcion, ruta_opcion FROM opciones WHERE modulo = '4' AND niv_opcion = '1' AND vinculado_menu = '1'";
				$resp = view($sqlOpciones);
				$filas = count($resp); */
			?>
			<table class="inicio">
				<tr>
					<td class="titulos" colspan="2">.: Men&uacute; Ingresos</td>
					<td class="cerrar" style="width:7%" onClick="location.href='teso-principal.php'">Cerrar</td>
				</tr>
				<tr>
					<td>
						<ol id="lista2">
							<?php
								/* $sqlOpciones = "SELECT nom_opcion, ruta_opcion FROM opciones WHERE modulo = '4' AND niv_opcion = '1' AND vinculado_menu = '1' ORDER BY orden_sub_menu ASC";
								$resOpciones = mysqli_query($linkbd, $sqlOpciones);
								while($rowOpciones = mysqli_fetch_row($resOpciones))
								{
									echo "<li onclick=\"location.href='$rowOpciones[1]'\" style='cursor:pointer'>".$rowOpciones[0]."</li>";
								}*/
							?>

                            <li onClick="location.href='teso-buscaingresosPredial.php'" style="cursor:pointer">Predial</li>
                            <li onClick="location.href='teso-buscaIngresosIca.php'" style="cursor:pointer">ICA</li>
                            <li onClick="location.href='teso-ingresosOtrosTributosBuscar.php'" style="cursor:pointer">Recaudo Impuestos</li>
                            <li onClick="location.href='teso-buscaingresosVentas.php'" style="cursor:pointer">Venta de bienes y servicios</li>
						</ol>
					</td>
				</tr>
			</table>
		</form>
	</body>
</html>
