<?php
	header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=utf8");
	require "comun.inc";
	require "funciones.inc";
	require_once 'funcionesSP.inc.php';
	session_start();
	$linkbd=conectar_v7();
	$linkbd -> set_charset("utf8"); 
	cargarcodigopag($_GET['codpag'],$_SESSION["nivel"]);
	date_default_timezone_set("America/Bogota");
	$scroll=$_GET['scrtop'];
	$totreg=$_GET['totreg'];
	$idcta=$_GET['idcta'];
	$altura=$_GET['altura'];
	$filtro="'".$_GET['filtro']."'";
?>
<!DOCTYPE >
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="es">
	<head>
		<meta http-equiv="Content-type" content="text/html; charset=utf8"/>
		<meta http-equiv="X-UA-Compatible" content="IE=9"/>
		<title>:: IDEAL 10 - Servicios P&uacute;blicos</title>
		<link href="css/css2.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<link href="css/css4.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<link href="css/cssSP.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<link href="css/style.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css"/>
		<script src="sweetalert2/dist/sweetalert2.min.js"></script>
		<link rel="stylesheet" href="sweetalert2/dist/sweetalert2.min.css">
		<script type="text/javascript" src="css/programas.js"></script>

		<script>
			function despliegamodalm(_valor,_tip,mensa,pregunta)
			{
				document.getElementById("bgventanamodalm").style.visibility=_valor;
				if(_valor=="hidden"){document.getElementById('ventanam').src="";}
				else
				{
					switch(_tip)
					{
						case "1":	document.getElementById('ventanam').src="ventana-mensaje1.php?titulos="+mensa;break;
						case "2":	document.getElementById('ventanam').src="ventana-mensaje3.php?titulos="+mensa;break;
						case "3":	document.getElementById('ventanam').src="ventana-mensaje2.php?titulos="+mensa;break;
						case "4":	document.getElementById('ventanam').src="ventana-consulta1.php?titulos="+mensa+"&idresp="+pregunta;break;	
					}
				}
			}
			function funcionmensaje(){}
			function respuestaconsulta(pregunta)
			{
				switch(pregunta)
				{
					case "1":	document.form2.oculto.value='2';
								document.form2.submit();
								break;
				}
			}
			function guardar()
			{
				var validacion01=document.getElementById('codban').value;
				var validacion02=document.getElementById('nomban').value;

				if (validacion01.trim()!='' && validacion02.trim()) 
				{
					Swal.fire({
						icon: 'question',
						title: 'Seguro que quieres guardar los cambios?',
						showDenyButton: true,
						confirmButtonText: 'Guardar',
						denyButtonText: 'Cancelar',
						}).then((result) => {

						if (result.isConfirmed) {
							document.form2.oculto.value='2';
							document.form2.submit();
						} else if (result.isDenied) {
							Swal.fire('Guardar cancelado', '', 'info')
						}
					})
				}
				else 
				{
					Swal.fire({
						icon: 'warning',
						title: 'Falta información',
						text: 'Digite el nombre del servicio'
					})
				}
			}

			function cambiocheck(tipo)
			{
				if (tipo == 'estado') {
					
					if(document.getElementById('myonoffswitch').value=='S') {
						document.getElementById('myonoffswitch').value='N';
					}
					else {
						document.getElementById('myonoffswitch').value='S';
					}
				}

				if (tipo == 'medidor') {

					if(document.getElementById('tarifaMedidor').value=='S') {
						document.getElementById('tarifaMedidor').value='N';
					}
					else {
						document.getElementById('tarifaMedidor').value='S';
					}
				}
				
			}
			
			function iratras(scrtop, numpag, limreg, filtro)
			{
				var idcta=document.getElementById('codban').value;
				location.href="serv-serviciosbuscar.php?idcta="+idcta+"&scrtop="+scrtop+"&numpag="+numpag+"&limreg="+limreg+ "&filtro="+filtro;
			}

			function adelante(scrtop, numpag, limreg, filtro, next)
			{
				var maximo = document.getElementById('maximo').value;
				var actual = parseFloat(document.getElementById('codban').value);

				if(parseFloat(maximo)>parseFloat(actual))
				{
					actual = actual + 1;

					if(actual < 10)
					{
						actual = "0" + actual;
					}

					document.getElementById('oculto').value = '1';
					location.href="serv-servicioseditar.php?idban="+actual+"&scrtop="+scrtop+"&numpag="+numpag+"&limreg="+limreg+"&filtro="+filtro;
				}
			}

			function atrasc(scrtop, numpag, limreg, filtro, prev)
			{
				var minimo = document.getElementById('minimo').value;
				var actual = parseFloat(document.getElementById('codban').value);

				if(parseFloat(minimo)<parseFloat(actual))
				{
					actual = actual - 1;

					if(actual < 10)
					{
						actual = "0" + actual;
					}

					document.getElementById('oculto').value = '1';
					location.href="serv-servicioseditar.php?idban="+actual+"&scrtop="+scrtop+"&numpag="+numpag+"&limreg="+limreg+"&filtro="+filtro;
				}
			}
			
			function despliegamodal2(_valor, tipo)
			{
				document.getElementById("bgventanamodal2").style.visibility=_valor;
				if(_valor=="hidden")
				{
					document.getElementById('ventana2').src="";
				}
				else 
				{
					if (tipo == 'cuenta') {
						
						document.getElementById('ventana2').src="serv-cuentasppto.php?ti=1";
					}

					if (tipo == 'fuente') {

						var cuentaPres = document.getElementById('cuenta').value;

						if (cuentaPres != '') {
							document.getElementById('ventana2').src="fuentes-ventana1.php?cuenta=" + cuentaPres;
						}
						else {
							parent.despliegamodal2("hidden");
							Swal.fire("Error", "Debes seleccionar primero la cuenta", "error");
						}
					}
				}
			}

			function buscacta(e)
			{
				if (document.form2.cuenta.value!="")
				{
					document.form2.bc.value='1';
					document.form2.submit();
				}
			}

			function despliegamodal4(_valor)
			{
				document.getElementById("bgventanamodal2").style.visibility=_valor;
				if(_valor=="hidden")
				{
					document.getElementById('ventana2').src="";
				}
				else 
				{
					document.getElementById('ventana2').src="servicios-ventana.php";
				}
			}
		</script>
		<?php titlepag();?>
	</head>
	<body>
		<IFRAME src="alertas.php" name="alertas" id="alertas" style="display:none"></IFRAME>
		<span id="todastablas2"></span>
		<?php
			$numpag=@ $_GET['numpag'];
			$limreg=@ $_GET['limreg'];
			$scrtop=26*$totreg;
		?>
		<table>
			<tr><script>barra_imagenes("serv");</script><?php cuadro_titulos();?></tr>
			<tr><?php menu_desplegable("serv");?></tr>
			<tr>
				<td colspan="3" class="cinta">
					<a href="serv-servicios.php" class="mgbt"><img src="imagenes/add.png"/></a>

					<a onclick="guardar()" class="mgbt"><img src="imagenes/guarda.png" title="Guardar"/></a>

					<a href="serv-serviciosbuscar.php" class="mgbt"><img src="imagenes/busca.png" title="Buscar"/></a>

					<a onclick="mypop=window.open('plan-agenda.php','','');mypop.focus()" class="mgbt"><img src="imagenes/agenda1.png" title="Agenda" /></a>

					<a onclick="mypop=window.open('serv-principal.php','','');mypop.focus();" class="mgbt"><img src="imagenes/nv.png" title="Nueva ventana"></a>

					<a onclick="mypop=window.open('<?php echo $url2; ?>','','');mypop.focus();" class="mgbt"><img src="imagenes/duplicar_pantalla.png" title="Duplicar pesta&ntilde;a"></a>

					<a onClick="iratras(<?php echo "$scrtop, $numpag, $limreg, $filtro"; ?>)" class="mgbt"><img src="imagenes/iratras.png" title="Atr&aacute;s"></a>
				</td>
			</tr>
		</table>
		
		<div id="bgventanamodalm" class="bgventanamodalm">
			<div id="ventanamodalm" class="ventanamodalm">
				<IFRAME src="" name="ventanam" marginWidth=0 marginHeight=0 frameBorder=0 id="ventanam" frameSpacing=0 style=" width:700px; height:130px; top:200; overflow:hidden;"></IFRAME>
			</div>
		</div>

		<form name="form2" method="post" action="">
			<?php
				if(@$_POST['oculto']=="")
				{
					$sqlr = "SELECT MIN(id), MAX(id) FROM srvservicios";
					$res = mysqli_query($linkbd,$sqlr);
					$r = mysqli_fetch_row($res);
					$_POST['minimo']=$r[0];
					$_POST['maximo']=$r[1];

					$sqlr = "SELECT * FROM srvservicios WHERE id = '$_GET[idban]'";
					$resp = mysqli_query($linkbd,$sqlr);
					$row = mysqli_fetch_row($resp); 

					$_POST['codban'] = $row[0];
					$_POST['nomban'] = $row[1];
					$_POST['tipoUnidad'] = $row[2];
					$_POST['cc'] = $row[3];
					$_POST['cargoFijoU'] = $row[4];
					$_POST['cargoFijoR'] = $row[9];
					$_POST['consumoU'] = $row[5];
					$_POST['consumoR'] = $row[10];
					$_POST['subsidioCFU'] = $row[6];
					$_POST['subsidioCFR'] = $row[11];
					$_POST['subsidioCSU'] = $row[7];
					$_POST['subsidioCSR'] = $row[12];
					$_POST['contribucionU'] = $row[8];
					$_POST['contribucionR'] = $row[13];
					$_POST['interesMoratorio'] = $row[14];
					$_POST['acuerdoPago'] = $row[15];
					$_POST['onoffswitch'] = $row[16];
					$_POST['tarifaMedidor'] = $row[17];
					$_POST['cuenta'] = $row[18];
					$_POST['fuente'] = $row[19];
					$_POST['clasificadorServicios'] = $row[20];

					if ($row[17] != '') {

						$sqlCuenta = "SELECT nombre FROM cuentasingresosccpet WHERE codigo = '$row[17]'";
						$rowCuenta = mysqli_fetch_row(mysqli_query($linkbd, $sqlCuenta));

						$_POST['nombreCuenta'] = $rowCuenta[0];
					}

					if ($row[18] != '') {

						$sqlFuente = "SELECT nombre FROM ccpet_fuentes_cuipo WHERE codigo_fuente = '$row[18]'";
						$rowFuente = mysqli_fetch_row(mysqli_query($linkbd, $sqlFuente));

						$_POST['nfuente'] = $rowFuente[0];
					}

					if ($row[19] != '') {

						$sqlProductoServicio = "SELECT titulo FROM ccpetservicios WHERE grupo = '$row[19]' ";
						$rowProductoServicio = mysqli_fetch_row(mysqli_query($linkbd, $sqlProductoServicio));

						$_POST['nclasificadorServicios'] = $rowProductoServicio[0];

					}
				}
			?>
			
			<table class="inicio">
				<tr>
					<td class="titulos" colspan="7">.: Crear Servicios</td>
					<td class="cerrar" style="width:7%" onClick="location.href='serv-principal.php'">Cerrar</td>
				</tr>

				<tr>
					<td class="tamano01" style="width:3.5cm;">C&oacute;digo:</td>
					<td style="width:15%; text-align:center;">
						<a href="#" onClick="atrasc(<?php echo "$scrtop, $numpag, $limreg, $filtro, $prev"; ?>)"><img src="imagenes/back.png" alt="anterior" align="absmiddle"></a> 
						<input type="text" name="codban" id="codban" onKeyUp="return tabular(event,this)" value="<?php echo @ $_POST['codban'];?>" style="text-align:center;width:60%;" readonly/>&nbsp;
						<a href="#" onClick="adelante(<?php echo "$scrtop, $numpag, $limreg, $filtro, $next" ?>);"><img src="imagenes/next.png" alt="siguiente" align="absmiddle"></a>
					</td>

					<td class="tamano01" style="width:3cm;">Nombre:</td>
					<td>
						<input type="text" name="nomban" id="nomban" value="<?php echo @ $_POST['nomban'];?>" style="width:100%;text-transform:uppercase"/>
					</td>

					<td class="tamano01" style="width: 3cm;">Estado:</td>
					<td style="width: 15%;">
						<div class="onoffswitch">
							<input type="checkbox" name="onoffswitch" class="onoffswitch-checkbox" id="myonoffswitch" value="<?php echo @ $_POST['onoffswitch'];?>" <?php if(@ $_POST['onoffswitch']=='S'){echo "checked";}?> onChange="cambiocheck('estado');"/>
							<label class="onoffswitch-label" for="myonoffswitch">
								<span class="onoffswitch-inner"></span>
								<span class="onoffswitch-switch"></span>
							</label>
						</div>
					</td>
				</tr>

				<tr>
					<td class="tamano01" style="width:3cm;">Unidad medida:</td>
					<td>
						<select name="tipoUnidad" id="tipoUnidad" style="width:100%" class="centrarOption">
							<option value="-1">:: Seleccione unidad de medida ::</option>  
							<?php
								$sqlr="SELECT id,nombre FROM srvtipo_unidad";
								$resp = mysqli_query($linkbd,$sqlr);

								while ($row = mysqli_fetch_row($resp))
								{
									if($_POST['tipoUnidad'] == $row[1])
									{
										echo "<option value='$row[1]' SELECTED>$row[1]</option>";
									}
									else
									{
										echo "<option value='$row[1]'>$row[1]</option>no";
									}
								}
							?>
						</select>
					</td>
								
					<td class="tamano01" style="width:3cm;">Centro de Costos:</td>
					<td style="width: 3cm;">
						<select name="cc" id="cc" class="centrarOption">
							<option value="-1">:: Seleccione Cento de Costos ::</option>
							<?php
								$sqlCentroCosto = "SELECT * FROM centrocosto WHERE estado = 'S' AND entidad = 'S' ORDER BY id_cc";
								$resCentroCosto = mysqli_query($linkbd,$sqlCentroCosto);
								while ($rowCentroCosto = mysqli_fetch_row($resCentroCosto))
								{
									if(@ $_POST['cc'] == $rowCentroCosto[0])
									{
										echo "<option value='$rowCentroCosto[0]' SELECTED>$rowCentroCosto[0] - $rowCentroCosto[1]</option>";
									}
									else{echo "<option value='$rowCentroCosto[0]'>$rowCentroCosto[0] - $rowCentroCosto[1]</option>";}
								}
							?>
						</select>
					</td>		
					
					<td class="tamano01" style="width: 3cm;">Uso de medidor:</td>
					<td style="width: 15%;">
						<div class="onoffswitch">
							<input type="checkbox" name="tarifaMedidor" class="onoffswitch-checkbox" id="tarifaMedidor" value="<?php echo @ $_POST['tarifaMedidor'];?>" <?php if(@ $_POST['tarifaMedidor']=='S'){echo "checked";}?> onChange="cambiocheck('medidor');"/>
							<label class="onoffswitch-label" for="tarifaMedidor">
								<span class="onoffswitch-inner"></span>
								<span class="onoffswitch-switch"></span>
							</label>
						</div>
					</td>
				</tr>
			</table>
			
			<table class="inicio">
				<tr style="text-align: center;">
					<td class="titulos" colspan="2">.: Conceptos Contables Urbano</td>
					<td class="titulos" colspan="2">.: Conceptos Contables Rural</td>
				</tr>

				<tr>
					<td class="tamano01" style="width:5cm;">Cargo Fijo:</td>
					<td>
						<select name="cargoFijoU" id="cargoFijoU" style="width:100%;" class="centrarOption">
							<option value="-1">:: Seleccione Concepto ::</option>
							<?php
								$sqlr="SELECT * FROM conceptoscontables WHERE modulo='10' AND tipo='SS' ORDER BY codigo";
								$resp = mysqli_query($linkbd,$sqlr);
								while ($row =mysqli_fetch_row($resp))
								{
									if(@$_POST['cargoFijoU']==$row[0])
									{
										echo "<option value='$row[0]' SELECTED>$row[0] - $row[1]</option>";
									}
									else
									{
										echo "<option value='$row[0]'>$row[0] - $row[1]</option>";
									}
								}
							?>
						</select>
					</td>		
					
					<td class="tamano01" style="width:5cm;">Cargo Fijo:</td>
					<td>
						<select name="cargoFijoR" id="cargoFijoR" style="width:100%;" class="centrarOption">
							<option value="-1">:: Seleccione Concepto ::</option>
							<?php
								$sqlr="SELECT * FROM conceptoscontables WHERE modulo='10' AND tipo='SS' ORDER BY codigo";
								$resp = mysqli_query($linkbd,$sqlr);
								while ($row =mysqli_fetch_row($resp))
								{
									if(@$_POST['cargoFijoR']==$row[0])
									{
										echo "<option value='$row[0]' SELECTED>$row[0] - $row[1]</option>";
									}
									else
									{
										echo "<option value='$row[0]'>$row[0] - $row[1]</option>";
									}
								}
							?>
						</select>
					</td>		
				</tr>

				<tr>
					<td class="tamano01" style="width:5cm;">Consumo:</td>
					<td>
						<select name="consumoU" id="consumoU" style="width:100%;" class="centrarOption">
							<option value="-1">:: Seleccione Concepto ::</option>
							<?php
								$sqlr="SELECT * FROM conceptoscontables WHERE modulo='10' AND tipo='CL' ORDER BY codigo";
								$resp = mysqli_query($linkbd,$sqlr);
								while ($row =mysqli_fetch_row($resp))
								{
									if(@$_POST['consumoU']==$row[0])
									{
										echo "<option value='$row[0]' SELECTED>$row[0] - $row[1]</option>";
									}
									else
									{
										echo "<option value='$row[0]'>$row[0] - $row[1]</option>";
									}
								}
							?>
						</select>
					</td>		

					<td class="tamano01" style="width:5cm;">Consumo:</td>
					<td>
						<select name="consumoR" id="consumoR" style="width:100%;" class="centrarOption">
							<option value="-1">:: Seleccione Concepto ::</option>
							<?php
								$sqlr="SELECT * FROM conceptoscontables WHERE modulo='10' AND tipo='CL'ORDER BY codigo";
								$resp = mysqli_query($linkbd,$sqlr);
								while ($row =mysqli_fetch_row($resp))
								{
									if(@$_POST['consumoR']==$row[0])
									{
										echo "<option value='$row[0]' SELECTED>$row[0] - $row[1]</option>";
									}
									else
									{
										echo "<option value='$row[0]'>$row[0] - $row[1]</option>";
									}
								}
							?>
						</select>
					</td>		
				</tr>

				<tr>
					<td class="tamano01" style="width:5cm;">Subsidio Cargo Fijo:</td>
					<td>
						<select name="subsidioCFU" id="subsidioCFU" style="width:100%;" class="centrarOption">
							<option value="-1">:: Seleccione Concepto ::</option>
							<?php
								$sqlr="SELECT * FROM conceptoscontables WHERE modulo='10' AND tipo='SB' ORDER BY codigo";
								$resp = mysqli_query($linkbd,$sqlr);
								while ($row =mysqli_fetch_row($resp))
								{
									if(@$_POST['subsidioCFU']==$row[0])
									{
										echo "<option value='$row[0]' SELECTED>$row[0] - $row[1]</option>";
									}
									else
									{
										echo "<option value='$row[0]'>$row[0] - $row[1]</option>";
									}
								}
							?>
						</select>
					</td>	

					<td class="tamano01" style="width:5cm;">Subsidio Cargo Fijo:</td>
					<td>
						<select name="subsidioCFR" id="subsidioCFR" style="width:100%;" class="centrarOption">
							<option value="-1">:: Seleccione Concepto ::</option>
							<?php
								$sqlr="SELECT * FROM conceptoscontables WHERE modulo='10' AND tipo='SB' ORDER BY codigo";
								$resp = mysqli_query($linkbd,$sqlr);
								while ($row =mysqli_fetch_row($resp))
								{
									if(@$_POST['subsidioCFR']==$row[0])
									{
										echo "<option value='$row[0]' SELECTED>$row[0] - $row[1]</option>";
									}
									else
									{
										echo "<option value='$row[0]'>$row[0] - $row[1]</option>";
									}
								}
							?>
						</select>
					</td>
				</tr>

				<tr>
					<td class="tamano01" style="width:5cm;">Subsidio Consumo:</td>
					<td>
						<select name="subsidioCSU" id="subsidioCSU" style="width:100%;" class="centrarOption">
							<option value="-1">:: Seleccione Concepto ::</option>
							<?php
								$sqlr="SELECT * FROM conceptoscontables WHERE modulo='10' AND tipo='SB' ORDER BY codigo";
								$resp = mysqli_query($linkbd,$sqlr);
								while ($row =mysqli_fetch_row($resp))
								{
									if(@$_POST['subsidioCSU']==$row[0])
									{
										echo "<option value='$row[0]' SELECTED>$row[0] - $row[1]</option>";
									}
									else
									{
										echo "<option value='$row[0]'>$row[0] - $row[1]</option>";
									}
								}
							?>
						</select>
					</td>

					<td class="tamano01" style="width:5cm;">Subsidio Consumo:</td>
					<td>
						<select name="subsidioCSR" id="subsidioCSR" style="width:100%;" class="centrarOption">
							<option value="-1">:: Seleccione Concepto ::</option>
							<?php
								$sqlr="SELECT * FROM conceptoscontables WHERE modulo='10' AND tipo='SB' ORDER BY codigo";
								$resp = mysqli_query($linkbd,$sqlr);
								while ($row =mysqli_fetch_row($resp))
								{
									if(@$_POST['subsidioCSR']==$row[0])
									{
										echo "<option value='$row[0]' SELECTED>$row[0] - $row[1]</option>";
									}
									else
									{
										echo "<option value='$row[0]'>$row[0] - $row[1]</option>";
									}
								}
							?>
						</select>
					</td>
				</tr>

				<tr>
					<td class="tamano01" style="width:5cm;">Contribución:</td>
					<td>
						<select name="contribucionU" id="contribucionU" style="width:100%;" class="centrarOption">
							<option value="-1">:: Seleccione Concepto ::</option>
							<?php
								$sqlr="SELECT * FROM conceptoscontables WHERE modulo='10' AND tipo='SC' ORDER BY codigo";
								$resp = mysqli_query($linkbd,$sqlr);
								while ($row =mysqli_fetch_row($resp))
								{
									if(@$_POST['contribucionU']==$row[0])
									{
										echo "<option value='$row[0]' SELECTED>$row[0] - $row[1]</option>";
									}
									else
									{
										echo "<option value='$row[0]'>$row[0] - $row[1]</option>";
									}
								}
							?>
						</select>
					</td>

					<td class="tamano01" style="width:5cm;">Contribución:</td>
					<td>
						<select name="contribucionR" id="contribucionR" style="width:100%;" class="centrarOption">
							<option value="-1">:: Seleccione Concepto ::</option>
							<?php
								$sqlr="SELECT * FROM conceptoscontables WHERE modulo='10' AND tipo='SC' ORDER BY codigo";
								$resp = mysqli_query($linkbd,$sqlr);
								while ($row =mysqli_fetch_row($resp))
								{
									if(@$_POST['contribucionR']==$row[0])
									{
										echo "<option value='$row[0]' SELECTED>$row[0] - $row[1]</option>";
									}
									else
									{
										echo "<option value='$row[0]'>$row[0] - $row[1]</option>";
									}
								}
							?>
						</select>
					</td>
				</tr>
			</table>

			<table class="inicio">
				<tr style="text-align: center;">
					<td class="titulos" colspan="4">.: Concepto Generales</td>
				</tr>

				<tr>
					<td class="tamano01" style="width:5cm;">Interes Moratorio:</td>
					<td style="width: 35.2%;">
						<select name="interesMoratorio" id="interesMoratorio" style="width:100%;" class="centrarOption">
							<option value="-1">:: Seleccione Concepto ::</option>
							<?php
								$sqlr="SELECT * FROM conceptoscontables WHERE modulo='10' AND tipo='SM' ORDER BY codigo";
								$resp = mysqli_query($linkbd,$sqlr);
								while ($row =mysqli_fetch_row($resp))
								{
									if(@$_POST['interesMoratorio']==$row[0])
									{
										echo "<option value='$row[0]' SELECTED>$row[0] - $row[1]</option>";
									}
									else
									{
										echo "<option value='$row[0]'>$row[0] - $row[1]</option>";
									}
								}
							?>
						</select>
					</td>

					<td class="tamano01" style="width:5cm;">Acuerdos de pago:</td>
					<td>
						<select name="acuerdoPago" id="acuerdoPago" style="width:100%;" class="centrarOption">
							<option value="-1">:: Seleccione Concepto ::</option>
							<?php
								$sqlr="SELECT * FROM conceptoscontables WHERE modulo='10' AND tipo='AP' ORDER BY codigo";
								$resp = mysqli_query($linkbd,$sqlr);
								while ($row =mysqli_fetch_row($resp))
								{
									if(@$_POST['acuerdoPago']==$row[0])
									{
										echo "<option value='$row[0]' SELECTED>$row[0] - $row[1]</option>";
									}
									else
									{
										echo "<option value='$row[0]'>$row[0] - $row[1]</option>";
									}
								}
							?>
						</select>
					</td>
				</tr>
			</table>

			<table class="inicio">
				<tr style="text-align: center;">
					<td class="titulos" colspan="4">.: Presupuesto</td>
				</tr>
				
				<tr>
					<td class="tamano01" style="width:10%;">Cuenta:</td>

					<td style="width:15%;">
						<input type="text" name="cuenta" id="cuenta" value="<?php echo @ $_POST['cuenta'];?>" style="text-align:center; width:100%;" class="colordobleclik" ondblclick="despliegamodal2('visible', 'cuenta')" onBlur="buscacta(event)"/>
					</td>

					<td colspan="2">
						<input type="text" name="nombreCuenta" id="nombreCuenta" value="<?php echo @ $_POST['nombreCuenta'];?>" style="width:100%;" readonly>
					</td>
				</tr>

				<tr>
					<td class="tamano01" style="width:10%;">Fuente:</td>

					<td style="width:15%;">
						<input type="text" name="fuente" id="fuente" value="<?php echo @ $_POST['fuente'];?>" style="text-align:center; width:100%;" class="colordobleclik" ondblclick="despliegamodal2('visible', 'fuente')" />
					</td>

					<td colspan="2">
						<input type="text" name="nfuente" id="nfuente" value="<?php echo @ $_POST['nfuente'];?>" style="width:100%;" readonly>
					</td>
				</tr>

				<?php
					$buscarClasificador = '';
					$sqlr = "SELECT clasificadores FROM ccpetprogramarclasificadores WHERE cuenta = '".$_POST['cuenta']."' ";
					$resp = mysqli_query($linkbd, $sqlr);
					$row = mysqli_fetch_row($resp);

					if($row[0] == '3')
					{		
						$buscarClasificador = '3';
					}

					if($buscarClasificador == '3')
					{
				?>
						<tr>
							<td class="tamano01" style="width:10%;">Servicios: </td> 

							<td style="width:15%;">
								<input type="text" id="clasificadorServicios" name="clasificadorServicios" style="text-align:center; width:100%;" value="<?php echo $_POST['clasificadorServicios']?>" class="colordobleclik" ondblclick="despliegamodal4('visible');">
							</td>

							<td>
								<input type="text" name="nclasificadorServicios" id="nclasificadorServicios" style="width:100%;" value="<?php echo $_POST['nclasificadorServicios']?>"  readonly>
							</td>
						</tr>
				<?php
					}
				?>
			</table>

			<input type="hidden" name="maximo" id="maximo" value="<?php echo @ $_POST['maximo']?>"/>
			<input type="hidden" name="minimo" id="minimo" value="<?php echo @ $_POST['minimo']?>"/>
			<input type="hidden" name="bc" id="bc" value=""/>
			<input type="hidden" name="oculto" id="oculto" value="1"/>

			<?php
				if(@$_POST['oculto']=="2")
				{				
					
					if (@$_POST['onoffswitch']!='S') {
						$valest='N';
					} else {
						$valest='S';
					}

					if (@$_POST['tarifaMedidor'] != 'S') {

						$tarifaMedidor = 'N';
					}
					else {

						$tarifaMedidor = 'S';
					}

					$queryServicio = "UPDATE srvservicios SET nombre = '$_POST[nomban]', cc = '$_POST[cc]', unidad_medida = '$_POST[tipoUnidad]', estado = '$valest', cargo_fijo_u = '$_POST[cargoFijoU]', cargo_fijo_r = '$_POST[cargoFijoR]', consumo_u = '$_POST[consumoU]', consumo_r = '$_POST[consumoR]', subsidio_cf_u = '$_POST[subsidioCFU]', subsidio_cf_r = '$_POST[subsidioCFR]', subsidio_cs_u = '$_POST[subsidioCSU]', subsidio_cs_r = '$_POST[subsidioCSR]', contribucion_u = '$_POST[contribucionU]', contribucion_r = '$_POST[contribucionR]', interes_moratorio = '$_POST[interesMoratorio]', tarifa_medidor = '$tarifaMedidor', cuenta = '$_POST[cuenta]', fuente = '$_POST[fuente]', productoservicio = '$_POST[clasificadorServicios]', acuerdo_pago = '$_POST[acuerdoPago]' WHERE id = '$_GET[idban]'";
					if (mysqli_query($linkbd,$queryServicio))
					{
						echo "
							<script>
								var x = window.location.href;

								Swal.fire({
									icon: 'success',
									title: 'Guardado exitoso!',
									}).then((result) => {
									if (result.value) {
										document.location.href = x;
									} 
								})
							</script>";
					}
					else
					{
						echo"
							<script>
								Swal.fire('Error en el guardar', '', 'error');
							</script>";
					}
				}
			?>
		</form>

		<div id="bgventanamodal2">
			<div id="ventanamodal2">
				<IFRAME  src="" name="buster" marginWidth=0 marginHeight=0 frameBorder=0 id="ventana2" frameSpacing=0 style="left:500px; width:900px; height:500px; top:200;"></IFRAME>
			</div>
		</div>

	</body>
</html>	