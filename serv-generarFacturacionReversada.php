<?php
header("Cache-control: no-cache, no-store, must-revalidate");
header("Content-Type: text/html;charset=iso-8859-1");
require "comun.inc";
require "funciones.inc";
require "funcionesSP.inc.php";
session_start();
$linkbd = conectar_v7();
cargarcodigopag($_GET['codpag'],$_SESSION["nivel"]);
date_default_timezone_set("America/Bogota");
?>
<!DOCTYPE >
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>:: IDEAL 10 - Servicios P&uacute;blicos</title>
    <link href="css/css2.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css"/>
    <link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css"/>
    <link href="css/css4.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css"/>
    <link href="css/cssSP.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css"/>
    <script type="text/javascript" src="css/calendario.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
    <script type="text/javascript" src="css/programas.js"></script>

    <style>
    </style>

    <script>
        function despliegamodalm(_valor,_tip,mensa,pregunta)
        {
            document.getElementById("bgventanamodalm").style.visibility=_valor;

            if(_valor=="hidden")
            {
                document.getElementById('ventanam').src="";
            }
            else
            {
                switch(_tip)
                {
                    case "1":	document.getElementById('ventanam').src="ventana-mensaje1.php?titulos="+mensa;break;
                    case "2":	document.getElementById('ventanam').src="ventana-mensaje3.php?titulos="+mensa;break;
                    case "3":	document.getElementById('ventanam').src="ventana-mensaje2.php?titulos="+mensa;break;
                    case "4":	document.getElementById('ventanam').src="ventana-consulta1.php?titulos="+mensa+"&idresp="+pregunta;break;	
                }
            }
        }

        function funcionmensaje()
        {
            document.location.href = "serv-generarFacturacionReversada.php";
        }

        function respuestaconsulta(pregunta)
        {
            switch(pregunta)
            {
                case "1":	
                    document.form2.oculto.value='2';
                    document.form2.submit();
                break;
            }
        }

        function guardar()
        {
            var factura = document.getElementById('facturaReversada').value;
            if(factura.trim() != '') 
            {
                despliegamodalm('visible','4','Esta seguro de guardar?','1');
            }
            else 
            {
                despliegamodalm('visible','2','Falta información para realizar el guardado');
            }
        }
        
        function despliega_modales(mostrar, modal)
        {
            document.getElementById('bgventanamodal2').style.visibility = mostrar;

            if(modal == 'factura_reversadas')
            {
                document.getElementById('ventana2').src = "serv-ventanaFacturasReversadas.php";
            }
        }

        function cambiocheck(id)
        {
            var nomid='valcheck'+id;
            if(document.getElementById(nomid).value != 'checked')
            {
                document.getElementById(nomid).value='S';
            }
            else
            {
                document.getElementById(nomid).value='N';
            }

            document.form2.reversado.value = '2';
        }
    </script>
</head>

    <?php titlepag();?>
    
<body>
    <IFRAME src="alertas.php" name="alertas" id="alertas" style="display:none"></IFRAME>
    <span id="todastablas2"></span>
    <table>
        <tr><script>barra_imagenes("serv");</script><?php cuadro_titulos();?></tr>
        <tr><?php menu_desplegable("serv");?></tr>
        <tr>
            <td colspan="3" class="cinta">
                <a href="" class="mgbt"><img src="imagenes/add.png"/></a>

                <a onclick="guardar()" class="mgbt"><img src="imagenes/guarda.png" title="Guardar"/></a>

                <a href="" class="mgbt"><img src="imagenes/buscad.png" title="Buscar"/></a>

                <a onclick="mypop=window.open('plan-agenda.php','','');mypop.focus()" class="mgbt"><img src="imagenes/agenda1.png" title="Agenda" /></a>

                <a onclick="mypop=window.open('serv-principal.php','','');mypop.focus();" class="mgbt"><img src="imagenes/nv.png" title="Nueva ventana"></a>

                <a onclick="mypop=window.open('<?php echo $url2; ?>','','');mypop.focus();" class="mgbt"><img src="imagenes/duplicar_pantalla.png" title="Duplicar pesta&ntilde;a"></a>

                <a href="serv-menuFacturacion01.php" class="mgbt"><img src="imagenes/iratras.png" alt="Atras" title="Atras"></a>
            </td>
        </tr>
    </table>

    <div id="bgventanamodalm" class="bgventanamodalm">
        <div id="ventanamodalm" class="ventanamodalm">
            <IFRAME src="" name="ventanam" marginWidth=0 marginHeight=0 frameBorder=0 id="ventanam" frameSpacing=0 style=" width:700px; height:130px; top:200; overflow:hidden;"></IFRAME>
        </div>
    </div>

    <form name="form2" method="post" action="">
        <?php
            if(@$_POST['oculto'] == '')
            {
                $_POST['nueva_factura'] = selconsecutivo('srvcortes_detalle', 'numero_facturacion');
            }  
        ?>
        
        <div>
            <table class="inicio">
                <tr>
					<td class="titulos" colspan="6">.: Generar Factura Reversada</td>

					<td class="cerrar" style="width:7%" onClick="location.href='serv-principal.php'">Cerrar</td>
				</tr>

                <tr>
                    <td class="tamano01" style="width:3cm;">Fecha de creaci&oacute;n</td>

					<td style="width:15%;">
                        <input type="text" name="fecha" id="fecha" value="<?php echo @ $_POST['fecha']?>" maxlength="10" onKeyUp="return tabular(event,this)"  id="fecha" onKeyDown="mascara(this,'/',patron,true)" title="DD/MM/YYYY" style="height:30px;text-align:center;width:85%;" readonly/>
                        
                        <a href="#" onClick="displayCalendarFor('fecha');" title="Calendario">
                            <img src="imagenes/calendario04.png" style="width:25px;">
                        </a>
                    </td>

                    <td class="tamano01" style="width:3cm;">Factura Reversada</td>

                    <td style="width: 15%;">
                        <input type="text" name="facturaReversada" id="facturaReversada" value="<?php echo $_POST['facturaReversada'] ?>" style="width: 98%; height: 30px; text-align:center;" class="colordobleclik" onclick="despliega_modales('visible','factura_reversadas')">
                    </td>

                    <td class="tamano01" style="width: 3cm;">Nueva factura:</td>

                    <td style="width: 15%;">
                        <input type="text" name="nueva_factura" id="nueva_factura" value="<?php echo $_POST['nueva_factura'] ?>" style="width: 98%; height: 30px; text-align:center;" readonly>
                    </td>
                </tr>
            </table>
        </div>

        <?php
            if(@$_POST['reversado'] == '2')
            {
                $sqlFacturacion = "SELECT * FROM srvdetalles_facturacion WHERE numero_facturacion = '$_POST[facturaReversada]' AND tipo_movimiento = '101'";
                $resFacturacion = mysqli_query($linkbd,$sqlFacturacion);
                while($rowFacturacion = mysqli_fetch_assoc($resFacturacion))
                { 
                    $_POST['servicio'][] = $rowFacturacion['id_servicio'];
                    $_POST['tipo_cobro'][] = $rowFacturacion['id_tipo_cobro'];
                    $_POST['valor'][] = $rowFacturacion['credito'] + $rowFacturacion['debito'];

                    $sqlServicio = "SELECT nombre FROM srvservicios WHERE id = '$rowFacturacion[id_servicio]'";
                    $resServicio = mysqli_query($linkbd,$sqlServicio);
                    $rowServicio = mysqli_fetch_assoc($resServicio);
                    
                    $_POST['nombre_servicio'][] = $rowServicio['nombre'];

                    $sqlTipoCobro = "SELECT nombre FROM srvtipo_cobro WHERE id = '$rowFacturacion[id_tipo_cobro]'";
                    $resTipoCobro = mysqli_query($linkbd,$sqlTipoCobro);
                    $rowTipoCobro = mysqli_fetch_assoc($resTipoCobro);

                    $_POST['nombre_tipo_cobro'][] = $rowTipoCobro['nombre'];
                }
            }
        ?>

        <div>
            <table class="inicio">
                <tr>
                    <td class="titulos" colspan="4" height="25">Informacion de Cobros</td>
                </tr>

                <tr class="titulos2">
                    <td style="text-align:center;">Servicio</td>
                    <td style="text-align:center;">Tipo de Cobro</td>
                    <td style="text-align:center;">Valor</td>
                    <td style="text-align:center;">Agregar</td>
                </tr>

                <?php

                $iter = 'saludo1a';
                $iter2 = 'saludo2';

                for ($x=0; $x < count($_POST['servicio']); $x++) 
                { 
                    echo "
                    <tr class='$iter'>
                        <td style='text-align:center;'>".$_POST['servicio'][$x]." - ".$_POST['nombre_servicio'][$x]."</td>
                        <td style='text-align:center;'>".$_POST['tipo_cobro'][$x]." - ".$_POST['nombre_tipo_cobro'][$x]."</td>
                        <td style='text-align:center;'>".number_format($_POST['valor'][$x],2,',','.')."</td>
                        <td style='width:7%;'>
                            <div class='rcheck'>
                                <input type='checkbox' id='valcheck$x' name='valcheck$x' value='".$_POST["valcheck$x"]."' ".$_POST["valcheck$x"]." onclick='cambiocheck($x)'/>
                                <label for='valcheck$x'>Facturar</label>
                            <div>
                        </td>
                    </tr>
                    ";

                    $aux = $iter;
                    $iter = $iter2;
                    $iter2 = $aux;
                }
                ?>
            </table>
        </div>

        <input type="hidden" name="oculto" id="oculto" value="1">
        <input type="hidden" name="id_reversion" id="id_reversion" value="<?php echo $_POST['id_reversion'] ?>">
        <input type="hidden" name="reversado" id="reversado" value="<?php echo $_POST['reversado'] = '1' ?>">
        <input type="hidden" name="comprueba_agregrar" id="comprueba_agregar" value="<?php echo $_POST['comprueba_agregar'] = '1'?>"/>
    </form>

    <div id="bgventanamodal2">
        <div id="ventanamodal2">
            <IFRAME  src="" name="buster" marginWidth=0 marginHeight=0 frameBorder=0 id="ventana2" frameSpacing=0 style="left:500px; width:900px; height:500px; top:200;"></IFRAME>
        </div>
    </div>

    <?php
        if(@$_POST['oculto'] == '2')
        {
            $año = date('Y');
            preg_match("/([0-9]{2})\/([0-9]{2})\/([0-9]{4})/", $_POST['fecha'],$f);
			$fecha="$f[3]-$f[2]-$f[1]";
            $vigencia = $f[3];
            $codigoComprobante = '29';

            $sql = "UPDATE srvreversion_factura SET factura_nueva_reversada = '$_POST[nueva_factura]', fecha_nueva_reversada = '$fecha' WHERE id = '$_POST[id_reversion]' ";
            mysqli_query($linkbd,$sql);

            $sqlConsulta = "SELECT id_cliente, id_corte FROM srvcortes_detalle WHERE numero_facturacion = '$_POST[facturaReversada]' ";
            $resConsulta = mysqli_query($linkbd,$sqlConsulta);
            $rowConsulta = mysqli_fetch_row($resConsulta);

            $id_cliente = $rowConsulta[0];
            $id_corte = $rowConsulta[1];

            $sqlCorteDetalle = "INSERT INTO srvcortes_detalle (id_cliente, id_corte, numero_facturacion, estado_pago) VALUES ('$id_cliente', '$id_corte', '$_POST[nueva_factura]', 'S')";
            mysqli_query($linkbd,$sqlCorteDetalle);            
            $movimiento = '101';

            for ($x=0; $x < count($_POST['servicio']); $x++) 
            { 
                if($_POST["valcheck$x"]=='S')
                {
                    $vlsino='S';
                    $valorTotal = $valorTotal + $_POST['valor'][$x];
                }
				else 
                {
                    $vlsino='N';
                }

                if($vlsino == 'S')
                {
                    //kardex
                    if($_POST['tipo_cobro'][$x] == '1' || $_POST['tipo_cobro'][$x] == '2' || $_POST['tipo_cobro'][$x] == '3' || $_POST['tipo_cobro'][$x] == '4' || $_POST['tipo_cobro'][$x] == '7' || $_POST['tipo_cobro'][$x] == '8' || $_POST['tipo_cobro'][$x] == '9')
                    {
                        $queryInsert = "INSERT INTO srvdetalles_facturacion (corte, id_cliente, numero_facturacion, tipo_movimiento, fecha_movimiento, id_servicio, id_tipo_cobro, credito, debito, saldo, estado) VALUES ('$id_corte', '$id_cliente', '$_POST[nueva_factura]', '$movimiento', '$fecha', '$_POST[servicio][$x]', '$_POST[tipo_cobro][$x]', '$_POST[valor][$x]', 0, 0, 'S')";
                        mysqli_query($linkbd,$queryInsert);
                    }
                    else
                    {
                        $queryInsert = "INSERT INTO srvdetalles_facturacion (corte, id_cliente, numero_facturacion, tipo_movimiento, fecha_movimiento, id_servicio, id_tipo_cobro, credito, debito, saldo, estado) VALUES ('$id_corte', '$id_cliente', '$_POST[nueva_factura]', '$movimiento', '$fecha', '$_POST[servicio][$x]', '$_POST[tipo_cobro][$x]', 0, '$_POST[valor][$x]', 0, 'S')";
                        mysqli_query($linkbd,$queryInsert);
                    }

                    switch($_POST['tipo_cobro'][$x])
                    {
                        case 1:
                            $sqlServicio = "SELECT concepto,cc FROM srvservicios WHERE id = ".$_POST['servicio'][$x]."";
                            $resServicio = mysqli_query($linkbd,$sqlServicio);
                            $rowServicio = mysqli_fetch_row($resServicio);

                            $sqlCliente = "SELECT id_tercero FROM srvclientes WHERE id = '$id_cliente' ";
                            $resCliente = mysqli_query($linkbd,$sqlCliente);
                            $rowCliente = mysqli_fetch_row($resCliente);

                            $sqlTerceros = "SELECT cedulanit FROM terceros WHERE id_tercero = '$rowCliente[0]' ";
                            $resTerceros = mysqli_query($linkbd,$sqlTerceros);
                            $rowTerceros = mysqli_fetch_row($resTerceros);

                            $concepto = concepto_cuentasn2($rowServicio[0],'SS',10,$rowServicio[1],$fecha);
                
                            for($z=0; $z<count($concepto); $z++)
                            {
                                if($concepto[$z][3] == 'S' and $concepto[$z][2] == 'N')
                                {
                                    $sqlr="INSERT INTO comprobante_det (id_comp,cuenta,tercero,centrocosto,detalle,cheque,valdebito, valcredito,estado,vigencia) VALUES ('$codigoComprobante $_POST[nueva_factura]','".$concepto[$z][0]."','$rowTerceros[0]','$rowServicio[1]','Facturacion Corte $id_corte de la factura $_POST[nueva_factura] Cargo Fijo','',0,".$_POST['valor'][$x].",'1' ,'$vigencia')";
                                    mysqli_query($linkbd,$sqlr);
                                }
                                if($concepto[$z][3] == 'N' and $concepto[$z][2] == 'S') 
                                {
                                    $sqlr="INSERT INTO comprobante_det (id_comp,cuenta,tercero,centrocosto,detalle,cheque,valdebito, valcredito,estado,vigencia) VALUES ('$codigoComprobante $_POST[nueva_factura]','".$concepto[$z][0]."','$rowTerceros[0]','$rowServicio[1]','Facturacion Corte $id_corte de la factura $_POST[nueva_factura] Cargo Fijo','',0,".$_POST['valor'][$x].",'1' ,'$vigencia')";
                                    mysqli_query($linkbd,$sqlr);
                                }
                            }
                        break;

                        case 2:
                            $sqlCliente = "SELECT id_tercero, id_estrato FROM srvclientes WHERE id = '$id_cliente' ";
                            $resCliente = mysqli_query($linkbd,$sqlCliente);
                            $rowCliente = mysqli_fetch_row($resCliente);

                            $sqlTerceros = "SELECT cedulanit FROM terceros WHERE id_tercero = '$rowCliente[0]' ";
                            $resTerceros = mysqli_query($linkbd,$sqlTerceros);
                            $rowTerceros = mysqli_fetch_row($resTerceros);

                            $sqlServicio = "SELECT concepto_contable,centro_costo FROM srvcostos_estandar WHERE id_servicio = ".$_POST['servicio'][$x]." AND id_estrato = '$rowCliente[1]' AND vigencia = '$año'";
                            $resServicio = mysqli_query($linkbd,$sqlServicio);
                            $rowServicio = mysqli_fetch_row($resServicio);

                            $concepto = concepto_cuentasn2($rowServicio[0],'CB',10,$rowServicio[1],$fecha);

                            for($z=0; $z<count($concepto); $z++)
                            {
                                if($concepto[$z][3] == 'S' and $concepto[$z][2] == 'N')
                                {
                                    $sqlr="INSERT INTO comprobante_det (id_comp,cuenta,tercero,centrocosto,detalle,cheque,valdebito, valcredito,estado,vigencia) VALUES ('$codigoComprobante $_POST[nueva_factura]','".$concepto[$z][0]."','$rowTerceros[0]','$rowServicio[1]','Facturacion Corte $id_corte de la factura $_POST[nueva_factura] Consumo Basico','',0,".$_POST['valor'][$x].",'1' ,'$vigencia')";
                                    mysqli_query($linkbd,$sqlr);
                                }
                                if ($concepto[$z][3] == 'N' and $concepto[$z][2] == 'S') 
                                {
                                    $sqlr="INSERT INTO comprobante_det (id_comp,cuenta,tercero,centrocosto,detalle,cheque,valdebito, valcredito,estado,vigencia) VALUES ('$codigoComprobante $_POST[nueva_factura]','".$concepto[$z][0]."','$rowTerceros[0]','$rowServicio[1]','Facturacion Corte $id_corte de la factura $_POST[nueva_factura] Consumo Basico','',0,".$_POST['valor'][$x].",'1' ,'$vigencia')";
                                    mysqli_query($linkbd,$sqlr);
                                }
                            }
                        break;

                        case 3:

                            $sqlCliente = "SELECT id_tercero, id_estrato FROM srvclientes WHERE id = '$id_cliente' ";
                            $resCliente = mysqli_query($linkbd,$sqlCliente);
                            $rowCliente = mysqli_fetch_row($resCliente);

                            $sqlTerceros = "SELECT cedulanit FROM terceros WHERE id_tercero = '$rowCliente[0]' ";
                            $resTerceros = mysqli_query($linkbd,$sqlTerceros);
                            $rowTerceros = mysqli_fetch_row($resTerceros);

                            $sqlServicio = "SELECT concepto_contable,centro_costo FROM srvtarifas WHERE id_servicio = ".$_POST['servicio'][$x]." AND id_estrato = '$rowCliente[1]' ";
                            $resServicio = mysqli_query($linkbd,$sqlServicio);
                            $rowServicio = mysqli_fetch_row($resServicio);

                            $concepto = concepto_cuentasn2($rowServicio[0],'CL',10,$rowServicio[1],$fecha);

                            for($z=0; $z<count($concepto); $z++)
                            {
                                if($concepto[$z][3] == 'S' and $concepto[$z][2] == 'N')
                                {
                                    $sqlr="INSERT INTO comprobante_det (id_comp,cuenta,tercero,centrocosto,detalle,cheque,valdebito, valcredito,estado,vigencia) VALUES ('$codigoComprobante $_POST[nueva_factura]','".$concepto[$z][0]."','$rowTerceros[0]','$rowServicio[1]','Facturacion Corte $id_corte de la factura $_POST[nueva_factura] Consumo Basico','',0,".$_POST['valor'][$x].",'1' ,'$vigencia')";
                                    mysqli_query($linkbd,$sqlr);
                                }
                                if ($concepto[$z][3] == 'N' and $concepto[$z][2] == 'S') 
                                {
                                    $sqlr="INSERT INTO comprobante_det (id_comp,cuenta,tercero,centrocosto,detalle,cheque,valdebito, valcredito,estado,vigencia) VALUES ('$codigoComprobante $_POST[nueva_factura]','".$concepto[$z][0]."','$rowTerceros[0]','$rowServicio[1]','Facturacion Corte $id_corte de la factura $_POST[nueva_factura] Consumo Basico','',0,".$_POST['valor'][$x].",'1' ,'$vigencia')";
                                    mysqli_query($linkbd,$sqlr);
                                }
                            }
                        break;

                        case 4:
                            $sqlAsignacionOtroCobro = "SELECT id_otro_cobro FROM srvasignacion_otroscobros WHERE id_cliente = '$id_cliente' ";
                            $resAsignacionOtroCobro = mysqli_query($linkbd,$sqlAsignacionOtroCobro);
                            $rowAsignacionOtroCobro = mysqli_fetch_row($resAsignacionOtroCobro);

                            $sqlOtroCobro = "SELECT concepto FROM srvotroscobros WHERE id = '$rowAsignacionOtroCobro[0]' ";
                            $resOtroCobro = mysqli_query($linkbd,$sqlOtroCobro);
                            $rowOtroCobro = mysqli_fetch_row($resOtroCobro);

                            $sqlServicio = "SELECT cc FROM srvservicios WHERE id = ".$_POST['servicio'][$x]." ";
                            $resServicio = mysqli_query($linkbd,$sqlServicio);
                            $rowServicio = mysqli_fetch_row($resServicio);

                            $sqlCliente = "SELECT id_tercero FROM srvclientes WHERE id = '$id_cliente' ";
                            $resCliente = mysqli_query($linkbd,$sqlCliente);
                            $rowCliente = mysqli_fetch_row($resCliente);

                            $sqlTerceros = "SELECT cedulanit FROM terceros WHERE id_tercero = '$rowCliente[0]' ";
                            $resTerceros = mysqli_query($linkbd,$sqlTerceros);
                            $rowTerceros = mysqli_fetch_row($resTerceros);

                            $concepto = concepto_cuentasn2($rowOtroCobro[0],'SO',10,$rowServicio[0],$fecha);

                            for($z=0; $z<count($concepto); $z++)
                            {
                                if($concepto[$z][3] == 'S' and $concepto[$z][2] == 'N')
                                {
                                    $sqlr="INSERT INTO comprobante_det (id_comp,cuenta,tercero,centrocosto,detalle,cheque,valdebito, valcredito,estado,vigencia) VALUES ('$codigoComprobante $_POST[nueva_factura]','".$concepto[$z][0]."','$rowTerceros[0]','$rowServicio[1]','Facturacion Corte $id_corte de la factura $_POST[nueva_factura] Consumo Basico','',0,".$_POST['valor'][$x].",'1' ,'$vigencia')";
                                    mysqli_query($linkbd,$sqlr);
                                }
                                if ($concepto[$z][3] == 'N' and $concepto[$z][2] == 'S') 
                                {
                                    $sqlr="INSERT INTO comprobante_det (id_comp,cuenta,tercero,centrocosto,detalle,cheque,valdebito, valcredito,estado,vigencia) VALUES ('$codigoComprobante $_POST[nueva_factura]','".$concepto[$z][0]."','$rowTerceros[0]','$rowServicio[1]','Facturacion Corte $id_corte de la factura $_POST[nueva_factura] Consumo Basico','',0,".$_POST['valor'][$x].",'1' ,'$vigencia')";
                                    mysqli_query($linkbd,$sqlr);
                                }
                            }
                        break;

                        case 5:
                            $sqlCliente = "SELECT id_tercero,id_estrato FROM srvclientes WHERE id = '$id_cliente' ";
                            $resCliente = mysqli_query($linkbd,$sqlCliente);
                            $rowCliente = mysqli_fetch_row($resCliente);

                            $sqlTerceros = "SELECT cedulanit FROM terceros WHERE id_tercero = '$rowCliente[0]' ";
                            $resTerceros = mysqli_query($linkbd,$sqlTerceros);
                            $rowTerceros = mysqli_fetch_row($resTerceros);

                            $sqlServicio = "SELECT cc FROM srvservicios WHERE id = ".$_POST['servicio'][$x]." ";
                            $resServicio = mysqli_query($linkbd,$sqlServicio);
                            $rowServicio = mysqli_fetch_row($resServicio);
                            
                            $sqlSubsidio = "SELECT concepto FROM srvsubsidios WHERE id_estrato = '$rowCliente[1]' AND vigencia = '$año'";
                            $resSubsidio = mysqli_query($linkbd,$sqlSubsidio);
                            $rowSubsidio = mysqli_fetch_row($resSubsidio);

                            $concepto = concepto_cuentasn2($rowSubsidio[0],'SB',10,$rowServicio[0],$fecha);

                            for($z=0; $z<count($concepto); $z++)
                            {
                                if($concepto[$z][3] == 'S' and $concepto[$z][2] == 'N')
                                {
                                    $sqlr="INSERT INTO comprobante_det (id_comp,cuenta,tercero,centrocosto,detalle,cheque,valdebito, valcredito,estado,vigencia) VALUES ('$codigoComprobante $_POST[nueva_factura]','".$concepto[$z][0]."','$rowTerceros[0]','$rowServicio[0]','Facturacion Corte $id_corte de la factura $_POST[nueva_factura] Subsidio','',0,".$_POST['valor'][$x].",'1' ,'$vigencia')";
                                    mysqli_query($linkbd,$sqlr);
                                }
                                if ($concepto[$z][3] == 'N' and $concepto[$z][2] == 'S') 
                                {
                                    $sqlr="INSERT INTO comprobante_det (id_comp,cuenta,tercero,centrocosto,detalle,cheque,valdebito, valcredito,estado,vigencia) VALUES ('$codigoComprobante $_POST[nueva_factura]','".$concepto[$z][0]."','$rowTerceros[0]','$rowServicio[0]','Facturacion Corte $id_corte de la factura $_POST[nueva_factura] Subsidio','',0,".$_POST['valor'][$x].",'1' ,'$vigencia')";
                                    mysqli_query($linkbd,$sqlr);
                                }
                            }
                        break;

                        case 6:
                            $sqlAsignacionExoneracion = "SELECT id_exoneracion FROM srvasignacion_exoneracion WHERE id_cliente = '$id_cliente' ";
                            $resAsignacionExoneracion = mysqli_query($linkbd,$sqlAsignacionExoneracion);
                            $rowAsignacionExoneracion = mysqli_fetch_row($resAsignacionExoneracion);

                            $sqlExoneraciones = "SELECT concepto FROM srvexoneraciones WHERE id = '$rowAsignacionExoneracion[0]' ";
                            $resExoneraciones = mysqli_query($linkbd,$sqlExoneraciones);
                            $rowExoneraciones = mysqli_fetch_row($resExoneraciones);

                            $sqlServicio = "SELECT cc FROM srvservicios WHERE id = ".$_POST['servicio'][$x]." ";
                            $resServicio = mysqli_query($linkbd,$sqlServicio);
                            $rowServicio = mysqli_fetch_row($resServicio);

                            $sqlCliente = "SELECT id_tercero FROM srvclientes WHERE id = '$id_cliente' ";
                            $resCliente = mysqli_query($linkbd,$sqlCliente);
                            $rowCliente = mysqli_fetch_row($resCliente);

                            $sqlTerceros = "SELECT cedulanit FROM terceros WHERE id_tercero = '$rowCliente[0]' ";
                            $resTerceros = mysqli_query($linkbd,$sqlTerceros);
                            $rowTerceros = mysqli_fetch_row($resTerceros);

                            $concepto = concepto_cuentasn2($rowExoneraciones[0],'SE',10,$rowServicio[0],$fecha);

                            for($z=0; $z<count($concepto); $z++)
                            {
                                if($concepto[$z][3] == 'S' and $concepto[$z][2] == 'N')
                                {
                                    $sqlr="INSERT INTO comprobante_det (id_comp,cuenta,tercero,centrocosto,detalle,cheque,valdebito, valcredito,estado,vigencia) VALUES ('$codigoComprobante $_POST[nueva_factura]','".$concepto[$z][0]."','$rowTerceros[0]','$rowServicio[0]','Facturacion Corte $id_corte de la factura $_POST[nueva_factura] Exoneracion','',0,".$_POST['valor'][$x].",'1' ,'$vigencia')";
                                    mysqli_query($linkbd,$sqlr);
                                }
                                if ($concepto[$z][3] == 'N' and $concepto[$z][2] == 'S') 
                                {
                                    $sqlr="INSERT INTO comprobante_det (id_comp,cuenta,tercero,centrocosto,detalle,cheque,valdebito, valcredito,estado,vigencia) VALUES ('$codigoComprobante $_POST[nueva_factura]','".$concepto[$z][0]."','$rowTerceros[0]','$rowServicio[0]','Facturacion Corte $id_corte de la factura $_POST[nueva_factura] Exoneracion','',0,".$_POST['valor'][$x].",'1' ,'$vigencia')";
                                    mysqli_query($linkbd,$sqlr);
                                }
                            }
                        break;								

                        case 7:
                            $sqlCliente = "SELECT id_tercero,id_estrato FROM srvclientes WHERE id = '$id_cliente' ";
                            $resCliente = mysqli_query($linkbd,$sqlCliente);
                            $rowCliente = mysqli_fetch_row($resCliente);

                            $sqlTerceros = "SELECT cedulanit FROM terceros WHERE id_tercero = '$rowCliente[0]' ";
                            $resTerceros = mysqli_query($linkbd,$sqlTerceros);
                            $rowTerceros = mysqli_fetch_row($resTerceros);

                            $sqlServicio = "SELECT cc FROM srvservicios WHERE id = ".$_POST['servicio'][$x]." ";
                            $resServicio = mysqli_query($linkbd,$sqlServicio);
                            $rowServicio = mysqli_fetch_row($resServicio);

                            $sqlContribuciones = "SELECT concepto FROM srvcontribuciones WHERE id_estrato = '$rowCliente[1]' ";
                            $resContribuciones = mysqli_query($linkbd,$sqlContribuciones);
                            $rowContribuciones = mysqli_fetch_row($resContribuciones);

                            $concepto = concepto_cuentasn2($rowContribuciones[0],'SC',10,$rowServicio[0],$fecha);

                            for($z=0; $z<count($concepto); $z++)
                            {
                                if($concepto[$z][3] == 'S' and $concepto[$z][2] == 'N')
                                {
                                    $sqlr="INSERT INTO comprobante_det (id_comp,cuenta,tercero,centrocosto,detalle,cheque,valdebito, valcredito,estado,vigencia) VALUES ('$codigoComprobante $_POST[nueva_factura]','".$concepto[$z][0]."','$rowTerceros[0]','$rowServicio[0]','Facturacion Corte $idcorte de la factura $_POST[nueva_factura] Contribucion','',0,".$_POST['valor'][$x].",'1' ,'$vigencia')";
                                    mysqli_query($linkbd,$sqlr);
                                }
                                if ($concepto[$z][3] == 'N' and $concepto[$z][2] == 'S') 
                                {
                                    $sqlr="INSERT INTO comprobante_det (id_comp,cuenta,tercero,centrocosto,detalle,cheque,valdebito, valcredito,estado,vigencia) VALUES ('$codigoComprobante $_POST[nueva_factura]','".$concepto[$z][0]."','$rowTerceros[0]','$rowServicio[0]','Facturacion Corte $idcorte de la factura $_POST[nueva_factura] Contribucion','',0,".$_POST['valor'][$x].",'1' ,'$vigencia')";
                                    mysqli_query($linkbd,$sqlr);
                                }
                            }
                        break;								
                    }
                } 
            }
            //cabecera comprobante
            $sqlComprobanteCabecera = "INSERT INTO comprobante_cab (numerotipo, tipo_comp, fecha, concepto, total_debito, total_credito, estado) VALUES ('$_POST[nueva_factura]', '$codigoComprobante', '$fecha', 'Facturacion corte $id_corte de la factura numero $_POST[nueva_factura]', $valorTotal, $valorTotal, '1') ";
            $resp = mysqli_query($linkbd,$sqlComprobanteCabecera);
            if($resp)
            {
                echo "<script>despliegamodalm('visible','1','Se ha almacenado con Exito');</script>";
            }
            else
            {
                echo"<script>despliegamodalm('visible','2','No se pudo ejecutar el guardado');</script>";
            }
        }
    ?>
</body>
</html>