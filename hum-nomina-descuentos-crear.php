<?php

    header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=utf8");
	require 'comun.inc';
	require 'funciones.inc';
	session_start();
    if(empty($_SESSION)){
        header("location: index.php");
    }
	$linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");
	cargarcodigopag($_GET['codpag'],$_SESSION["nivel"]);
	date_default_timezone_set("America/Bogota");
?>
<!DOCTYPE >
<html lang=es>
	<head>
    <meta charset="utf-8"/>
		<meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1"/>
		<meta name="viewport" content="user-scalable=no">
		<title>:: IDEAL 10 - Gestión humana</title>
		<link href="favicon.ico" rel="shortcut icon"/>
		<link href="css/css2z.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
		<link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
		<link href="css/css4.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
        <link href="css/style.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css"/>
        <link rel="stylesheet" href="sweetalert2/dist/sweetalert2.min.css">
        <script src="sweetalert2/dist/sweetalert2.min.js"></script>
		<script src="vue/vue.min.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script src="css/programas.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
	</head>
	<body>
		<header>
			<table>
				<tr><script>barra_imagenes("hum");</script><?php cuadro_titulos();?></tr>
			</table>
		</header>

        <main id="myapp" v-cloak>
            <input type="hidden" value = "1" ref="pageType">
            <div class="loading-container" v-show="isLoading" >
                <p class="text-loading"data-loading="IDEAL 10">IDEAL 10</p>
            </div>
            <nav>
                <table>
                    <tr><?php menu_desplegable("hum");?></tr>
                </table>
                <div class="bg-white group-btn p-1" id="newNavStyle">
                    <button type="button" @click="save()" class="btn btn-white btn-primary-hover d-flex justify-between align-items-center">
                        <span>Guardar</span>
                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 -960 960 960">
                            <path d="M840-680v480q0 33-23.5 56.5T760-120H200q-33 0-56.5-23.5T120-200v-560q0-33 23.5-56.5T200-840h480l160 160Zm-80 34L646-760H200v560h560v-446ZM480-240q50 0 85-35t35-85q0-50-35-85t-85-35q-50 0-85 35t-35 85q0 50 35 85t85 35ZM240-560h360v-160H240v160Zm-40-86v446-560 114Z">
                            </path>
                        </svg>
                    </button>
                    <button type="button" @click="window.location.href='hum-nomina-descuentos-buscar'" class="btn btn-white btn-primary-hover d-flex justify-between align-items-center">
                        <span>Buscar</span>
                        <svg class="fill-black group-hover:fill-white w-5 h-5" xmlns="http://www.w3.org/2000/svg" viewBox="0 -960 960 960"><path d="M784-120 532-372q-30 24-69 38t-83 14q-109 0-184.5-75.5T120-580q0-109 75.5-184.5T380-840q109 0 184.5 75.5T640-580q0 44-14 83t-38 69l252 252-56 56ZM380-400q75 0 127.5-52.5T560-580q0-75-52.5-127.5T380-760q-75 0-127.5 52.5T200-580q0 75 52.5 127.5T380-400Z"></path></svg>
                    </button>
                    <button type="button" class="btn btn-white btn-primary-hover d-flex justify-between align-items-center" @click="mypop=window.open('hum-principal.php','',''); mypop.focus();">
                        <span>Nueva ventana</span>
                        <svg class="fill-black group-hover:fill-white w-5 h-5" xmlns="http://www.w3.org/2000/svg"  viewBox="0 -960 960 960" ><path d="M320-120v-80h80v-80H160q-33 0-56.5-23.5T80-360v-400q0-33 23.5-56.5T160-840h640q33 0 56.5 23.5T880-760v400q0 33-23.5 56.5T800-280H560v80h80v80H320ZM160-360h640v-400H160v400Zm0 0v-400 400Z"/></svg>
                    </button>
                    <button type="button" @click="mypop=window.open('hum-nomina-descuentos-crear','','');mypop.focus();" class="btn btn-white btn-primary-hover d-flex justify-between align-items-center">
                        <span class="group-hover:text-white">Duplicar pantalla</span>
                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 -960 960 960"><path d="M640-160v-360H160v360h480Zm80-200v-80h80v-360H320v200h-80v-200q0-33 23.5-56.5T320-880h480q33 0 56.5 23.5T880-800v360q0 33-23.5 56.5T800-360h-80ZM160-80q-33 0-56.5-23.5T80-160v-360q0-33 23.5-56.5T160-600h480q33 0 56.5 23.5T720-520v360q0 33-23.5 56.5T640-80H160Zm400-603ZM400-340Z"></path></svg>
                    </button>
                    <button type="button" @click="window.location.href='hum-nomina-descuentos-buscar'" class="btn btn-success btn-success-hover d-flex justify-between align-items-center">
                        <span>Atras</span>
                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 -960 960 960"><path d="M760-200v-160q0-50-35-85t-85-35H273l144 144-57 56-240-240 240-240 57 56-144 144h367q83 0 141.5 58.5T840-360v160h-80Z"></path></svg>
                    </button>
                </div>
            </nav>
            <section class="bg-white w-100">
                <h2 class="titulos m-0">Crear autorizacion descuentos de nómina</h2>
                <p class="m-0 ms-2">Todos los campos con (<span class="text-danger fw-bolder">*</span>) son obligatorios.</p>
                <div class="d-flex">
                    <div class="form-control w-25">
                        <label class="form-label">Fecha <span class="text-danger fw-bolder">*</span>:</label>
                        <input type="date" v-model="txtFecha">
                    </div>
                    <div class="form-control">
                        <label class="form-label">Contrato laboral <span class="text-danger fw-bolder">*</span>:</label>
                        <div class="d-flex">
                            <input type="text" v-model="objContrato.codigo" @dblclick="isModalContrato=true" @change="search('cod_contrato');setDescription();" class="colordobleclik w-25">
                            <input type="text" v-model="objContrato.nombre" disabled>
                        </div>
                    </div>
                </div>
                <div class="d-flex">
                    <div class="form-control">
                        <label class="form-label">Valor <span class="text-danger fw-bolder">*</span>:</label>
                        <input type="text" :value="valorCuota" @keyup="setValue(event);getCuotas()">
                    </div>
                    <div class="form-control">
                        <label class="form-label">Cuotas <span class="text-danger fw-bolder">*</span>:</label>
                        <input type="number" v-model="txtCuotas" @keyup="getCuotas()">
                    </div>
                    <div class="form-control">
                        <label class="form-label">Modo de pago <span class="text-danger fw-bolder">*</span>:</label>
                        <select v-model="selectModoPago">
                            <option value="CSF">CSF</option>
                            <option value="SSF">SSF</option>
                        </select>
                    </div>
                    <div class="form-control">
                        <label class="form-label">Variable de retención <span class="text-danger fw-bolder">*</span>:</label>
                        <select v-if="selectModoPago == 'CSF'" v-model="selectRetencion" @change="setDescription">
                            <option value="">Seleccione</option>
                            <option v-for="(data,index) in arrVariablesRet" :key="index" :value="data.codigo">{{data.codigo+"-"+data.nombre}}</option>
                        </select>
                        <select v-if="selectModoPago == 'SSF'" v-model="selectSsf" @change="setDescription">
                            <option value="">Seleccione</option>
                            <option v-for="(data,index) in arrVariablesSsf" :key="index" :value="data.codigo">{{data.codigo+"-"+data.nombre}}</option>
                        </select>
                    </div>
                </div>
                <div class="d-flex">
                    <div class="form-control">
                        <label class="form-label">Descripción <span class="text-danger fw-bolder">*</span>:</label>
                        <textarea style="max-height:4rem;" v-model="txtDescripcion"></textarea>
                    </div>
                </div>
                <h2 class="titulos m-0">Detalle</h2>
                <div class="overflow-auto" style="height:40vh">
                    <table class="table fw-normal">
                        <thead>
                            <tr class="text-center">
                                <th>No. Cuota</th>
                                <th>Mes</th>
                                <th>Valor</th>
                                <th>Tipo de pago</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr v-for="(data,index) in arrCuotas" :key="index" >
                                <td class="text-center">{{data.cuota}}</td>
                                <td ><div class="d-flex"><div class="form-control"><input type="month" :value="data.mes"></div></div></td>
                                <td v-if="index < arrCuotas.length-1"><div class="d-flex"><div class="form-control"><input type="text" v-model="data.valor_formateado" @keyup="updateCuotas"></div></div></td>
                                <td v-else><div class="d-flex"><div class="form-control"><input type="text" :value="data.valor_formateado" disabled></div></div></td>
                                <td>
                                    <div class="d-flex">
                                        <div class="form-control">
                                            <select v-model="data.tipo_pago">
                                                <option v-for="(arrData,i) in arrVariablesPago" :value="arrData.codigo" :key="i">{{arrData.codigo+"-"+arrData.nombre}}</option>
                                            </select>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </section>
            <!-- MODALES-->
            <div v-show="isModalContrato" class="modal">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title">Buscar contratos</h5>
                            <button type="button" @click="isModalContrato=false;" class="btn btn-close"><div></div><div></div></button>
                        </div>
                        <div class="modal-body">
                            <div class="d-flex flex-column">
                                <div class="form-control m-0 mb-3">
                                    <input type="search" placeholder="Buscar" v-model="txtSearchContrato" @keyup="search('modal_contrato')" id="labelInputName">
                                </div>
                                <div class="form-control m-0 mb-3">
                                    <label class="form-label" for="labelInputName">Resultados: <span class="fw-bold">{{txtResultadosContrato}}</span></label>
                                </div>
                            </div>
                            <div class="overflow-auto max-vh-50 overflow-x-hidden " >
                                <table class="table table-hover fw-normal">
                                    <thead>
                                        <tr>
                                            <th>Código</th>
                                            <th>Nombre</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr v-for="(data,index) in arrContratosCopy" :key="index" @dblclick="selectItem(data)">
                                            <td>{{data.codigo}}</td>
                                            <td>{{data.nombre}}</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </main>
		<script src="Librerias/vue/vue.min.js"></script>
		<script type="module" src="gestion_humana/nomina/js/functions_autorizacion_descuentos.js?<?= date('d_m_Y_h_i_s');?>"></script>
	</body>
</html>
