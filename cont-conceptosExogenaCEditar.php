<?php

    header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=utf8");
	require 'comun.inc';
	require 'funciones.inc';
	session_start();
    if(empty($_SESSION)){
        header("location: index.php");
    }
	$linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");
	cargarcodigopag($_GET['codpag'],$_SESSION["nivel"]);
	date_default_timezone_set("America/Bogota");
?>
<!DOCTYPE >
<html lang=es>
	<head>
    <meta charset="utf-8"/>
		<meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1"/>
		<meta name="viewport" content="user-scalable=no">
		<title>:: IDEAL 10 - Tesorería</title>
		<link href="favicon.ico" rel="shortcut icon"/>
		<link href="css/css2z.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
		<link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
		<link href="css/css4.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
        <link href="css/tabs.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css"/>
        <link href="css/style.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css"/>
        <link rel="stylesheet" href="sweetalert2/dist/sweetalert2.min.css">
        <script type="text/javascript" src="css/programas.js"></script>
        <script src="sweetalert2/dist/sweetalert2.min.js"></script>
		<script src="vue/vue.min.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script src="css/programas.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script src="css/calendario.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
	</head>
	<body>
		<header>
			<table>
				<tr><script>barra_imagenes("cont");</script><?php cuadro_titulos();?></tr>
			</table>
		</header>

		<form name="form2" method="post" action="">
			<section id="myapp" v-cloak >
                <div id="cargando" v-show="isLoading" class="loading" style="z-index: 10000;">
                    <span>Cargando...</span>
                </div>
				<nav>
					<table>
						<tr><?php menu_desplegable("cont");?></tr>
                        <tr>
							<td colspan="3" class="cinta">
                                <img src="imagenes/add.png"  @click="window.location.href='cont-conceptosExogenaCCrear.php'" class="mgbt" title="Nuevo">
								<img src="imagenes/guarda.png"   @click="save()" title="Guardar"  class="mgbt">
								<img src="imagenes/busca.png" @click="window.location.href='cont-conceptosExogenaCBuscar.php'"   class="mgbt" title="Buscar">
								<img src="imagenes/nv.png" @click="mypop=window.open('cont-principal.php','',''); mypop.focus();" class="mgbt" title="Nueva Ventana">
                                <a href="cont-conceptosExogenaCBuscar.php"  class="mgbt"><img src="imagenes/iratras.png" title="Atr&aacute;s"></a>
							</td>
						</tr>
					</table>
				</nav>
				<article>
                    <div class="inicio">
                        <div>
                            <h2 class="titulos m-0">.: Editar conceptos exógena</h2>
                            <div class="d-flex w-75">
                                <div class="form-control w-25">
                                    <label class="form-label">.: Consecutivo:</label>
                                    <div class="d-flex">
                                        <button v-show="txtConsecutivo > 1" type="button" class="btn btn-primary" @click="editItem(--txtConsecutivo)"><</button>
                                        <input type="text"  style="text-align:center;" v-model="txtConsecutivo" @change="editItem(txtConsecutivo)">
                                        <button v-show="txtConsecutivo < txtMax" type="button" class="btn btn-primary" @click="editItem(++txtConsecutivo)">></button>
                                    </div>
                                </div>
                                <div class="form-control ">
                                    <label class="form-label">.: Formato <span class="text-danger fw-bolder">*</span></label>
                                    <select v-model="selectFormato">
                                        <option value="0">Seleccione</option>
                                        <option v-for="(data,index) in arrFormatos" :key="index" :value="data.formato">
                                            {{data.formato+" - "+data.nombre}}
                                        </option>
                                    </select>
                                </div>
                            </div>
                            <div class="d-flex w-75">
                                <div class="form-control w-25">
                                    <label class="form-label">.: Código <span class="text-danger fw-bolder">*</span></label>
                                    <input type="text" v-model="txtCodigo">
                                </div>
                                <div class="form-control">
                                    <label class="form-label">.: Nombre <span class="text-danger fw-bolder">*</span></label>
                                    <input type="text" v-model="txtNombre">
                                </div>
                            </div>
                        </div>
                    </div>
				</article>
			</section>
		</form>

		<script src="node_modules/read-excel-file/bundle/read-excel-file.min.js"></script>
		<script src="node_modules/xlsx/dist/xlsx.full.min.js"></script>
        <script src="Librerias/vue/vue.min.js"></script>
		<script src="Librerias/vue/axios.min.js"></script>
		<script type="module" src="contabilidad_vue/exogena_conceptos_crear/editar/cont-conceptosEditar.js?<?= date('d_m_Y_h_i_s');?>"></script>

	</body>
</html>
