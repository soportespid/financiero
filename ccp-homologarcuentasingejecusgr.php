<?php
    header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=utf8");

	require 'comun.inc';
	require 'funciones.inc';

	session_start();
    date_default_timezone_set("America/Bogota");

?>
<!DOCTYPE >
<html lang="es">
	<head>
		<meta charset="utf-8"/>
		<meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1"/>
		<meta name="viewport" content="user-scalable=no">
		<title>:: Ideal - Informes</title>
		<link href="favicon.ico" rel="shortcut icon"/>

		<link href="css/css2.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<link href="css/css2z.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
        <link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
        <link href="css/css4.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
        <script type="text/javascript" src="css/programas.js"></script>
        <script type="text/javascript" src="css/calendario.js"></script>
        <script type="text/javascript" src="JQuery/jquery-2.1.4.min.js"></script>
		<link href="css/style.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css"/>

		<style>
			.inicio--no-shadow{
				box-shadow: none;
			}

			[v-cloak]{
				display : none;
			}

			.seleccionarTodos {
				padding: 4px 0px 4px 10px;
				background: linear-gradient(#337CCF, #1450A3);
				border: 1px solid;
				border-top-color: #0A6EBD;
				border-right-color: #0A6EBD;
				border-bottom-color: #337CCF;
				border-left-color: #337CCF;
				box-shadow: 4px 4px 2px #368eb880;
				border-radius: 5px;
				display: flex;
				align-items: center;
				flex-direction: row;
				text-align: center;
				gap: 5px;
				color: #fff;
				cursor: pointer;
				font-size: 0.8rem;
			}

			.span-check{
				margin-right: 0.5em;
				text-align: center;
				width: 1.5rem;
				height: 1.5rem;
				background-color: rgb(12, 176, 252);
				border: 2px solid;
				border-color: rgb(46, 136, 238);
				border-radius: 5px;
				cursor: pointer;
			}

			.span-check-completed {
				margin-right: 0.5em;
				text-align: center;
				width: 1.5rem;
				height: 1.5rem;
				background-color: rgb(12, 176, 252);
				border: 2px solid;
				border-color: rgb(46, 136, 238);
				border-radius: 5px;
				cursor: pointer;
				background-image: url("data:image/svg+xml,%3csvg viewBox='0 0 16 16' fill='white' xmlns='http://www.w3.org/2000/svg'%3e%3cpath d='M12.207 4.793a1 1 0 010 1.414l-5 5a1 1 0 01-1.414 0l-2-2a1 1 0 011.414-1.414L6.5 9.086l4.293-4.293a1 1 0 011.414 0z'/%3e%3c/svg%3e");
				background-size:auto;
			}
		</style>
    </head>
    <body>
        <IFRAME src="alertas.php" name="alertas" id="alertas" style="display:none"></IFRAME>
        <span id="todastablas2"></span>
        <table>
            <tr><script>barra_imagenes("info");</script><?php cuadro_titulos();?></tr>
            <tr><?php menu_desplegable("info");?></tr>
        	<tr>
          		<td colspan="3" class="cinta">
					<a><img src="imagenes/add.png" title="Nuevo" onClick="location.href='ccp-homologarcuentasingsgr.php'" class="mgbt"/></a>
					<a class="mgbt"><img src="imagenes/guardad.png"/></a>
					<a><img src="imagenes/buscad.png" title="Buscar" class="mgbt"/></a>
					<a href="#" onClick="mypop=window.open('info-principal.php','','');mypop.focus();" class="mgbt"><img src="imagenes/nv.png" title="Nueva Ventana"></a>
					<img src="imagenes/iratras.png" title="Atr&aacute;s" onClick="location.href='ccp-menucuipo.php'" class="mgbt"/>
				</td>
        	</tr>
		</table>
		<div class="h-100" >
			<div id="myapp" v-cloak>
                <h2 class="titulos m-0">Ejecución de Ingresos, sistema general de regalias</h2>
                <div class="d-flex">
                    <div class="form-control">
                        <label class="form-label">Buscar Rubro:</label>
                        <input v-on:keyup="buscarRubro" v-model="buscar_rubro" type="text" placeholder="Buscar Rubro.">
                    </div>
                    <div class="form-control">
                        <label class="form-label">Vigencia:</label>
                        <select v-model="vigencia"  v-on:Change="cargarParametros">
                            <option v-for="year in years" :value="year[0]">{{ year[0] }}</option>
                        </select>
                    </div>
                </div>
                <button v-show="puedeGuardar" class="btn btn-success ms-2 mt-1 mb-1" @click = "guardarRubroSGR">Guardar Cambios</button>
				<div class="overflow-y" style="height:60%">
                    <table>
                        <thead>
                            <tr>
                                <td class='titulos' width="20%"  style="font: 160% sans-serif; border-radius: 5px 0px 0px 0px;">Rubro</td>
                                <td class='titulos' style="font: 160% sans-serif;">
                                    Nombre
                                </td>
                                <td class='titulos' width = "5%" style="text-align: center;">
                                    Tipo
                                </td>
                                <td class='titulos' width = "10%" style="text-align: center;">
                                    Tipo de Recurso
                                </td>
                                <td class='titulos' width = "10%" style="text-align: center;">
                                    BPIM
                                </td>

                                <td class='titulos' width = "10%" style="text-align: center;">
                                    Tercero
                                </td>

                                <td class='titulos' width = "10%" style="text-align: center;">
                                    Politica publica
                                </td>

                                <td class='titulos' width = "1%"></td>
                            </tr>
                        </thead>
                        <tbody v-if="show_resultados">

                            <tr v-for="(result, index) in results" :style="estaEnArray(result) == true ? myStyle : ''" v-bind:class="index % 2 ? 'contenidonew00' : 'contenidonew01'" >
                                <td width="20%" style="font: 160% sans-serif; padding: 5px;">{{ result[0].split('-')[0] }}</td>

                                <td colspan="2" style="font: 160% sans-serif;">{{ result[1] }}</td>

                                <td width="5%" style="font: 160% sans-serif; padding: 5px;">{{ result[0].split('-')[2] }}</td>

                                <td width="10%" style="text-align: center;">
                                    <div v-on:dblclick="agregarTipoRecursoSGR(result[0],result[1])" title="Doble click para desplegar ventana emergente" style='text-rendering: optimizeLegibility; text-align:center; background-color: #dbeafe; cursor: pointer !important; border-radius: 5px; height: 5vh; display: flex; flex-direction: column; justify-content: center'>
                                        {{ cuentaSelSGR && cuentaSelSGR[result[0]+'-2'] }}
                                    </div>
                                </td>

                                <td width="10%" style="text-align: center;">
                                    <div v-on:dblclick="agregarBpimSGR(result[0], result[1])" title="Doble click para desplegar ventana emergente" style='text-rendering: optimizeLegibility; text-align:center; background-color: #dbeafe; cursor: pointer !important; border-radius: 5px; height: 5vh; display: flex; flex-direction: column; justify-content: center'>
                                        {{ cuentaSelSGR && cuentaSelSGR[result[0]+'-1']  }}
                                    </div>
                                </td>


                                <td width="10%" style="text-align: center;">
                                    <div v-on:dblclick="agregarTerceroSGR(result[0],result[1])" title="Doble click para desplegar ventana emergente" style='text-rendering: optimizeLegibility; text-align:center; background-color: #dbeafe; cursor: pointer !important; border-radius: 5px; height: 5vh; display: flex; flex-direction: column; justify-content: center'>
                                        {{ cuentaSelSGR && cuentaSelSGR[result[0]+'-3'] }}
                                    </div>
                                </td>

                                <td width="10%" style="text-align: center;">
                                    <div v-on:dblclick="agregarPoliticaPublicaSGR(result[0],result[1])" title="Doble click para desplegar ventana emergente" style='text-rendering: optimizeLegibility; text-align:center; background-color: #dbeafe; cursor: pointer !important; border-radius: 5px; height: 5vh; display: flex; flex-direction: column; justify-content: center'>
                                        {{ cuentaSelSGR && cuentaSelSGR[result[0]+'-4'] }}
                                    </div>
                                </td>
                            </tr>
                        </tbody>
                        <tbody v-else>
                            <tr>
                                <td width="20%"style="font: 120% sans-serif; padding-left:10px; text-align:center;" colspan="3">Sin resultados</td>
                            </tr>
                        </tbody>
                    </table>
				</div>
				<div v-show="showModal_bpim">
                    <transition name="modal">
                        <div class="modal-mask">
                            <div class="modal-wrapper">
                                <div class="modal-dialog modal-lg" style = "max-width: 1200px !important;" role="document">
                                    <div class="modal-content"  style = "width: 100% !important;" scrollable>
                                        <div class="modal-header">
                                            <h5 class="modal-title">{{ rubroVentanaEmergente }}</h5>
                                            <button type="button" @click="showModal_bpim = false" class="btn btn-close p-2"><div></div><div></div></button>
                                        </div>
                                        <div class="modal-body">
                                            <div style="margin: 2px 0 0 0;">
                                                <div class="row" style="margin: 4px; border-radius:4px; background-color: #F6F6F6; ">
                                                    <div class="col-md-2" style="padding: 12px 0px 0px 30px; font: 140% sans-serif;">
                                                        <label for="">Proyecto:</label>
                                                    </div>

                                                    <div class="col-md-6 col-md-offset-6" style="padding: 4px">
                                                        <input type="text" class="form-control" placeholder="Buscar por nombre o bpim" style="font: sans-serif; " v-on:keyup="searchMonitorBpim" v-model="searchBpim.keywordBpim">
                                                    </div>
                                                </div>
                                                <table>
                                                    <thead>
                                                        <tr>
                                                            <td width="20%" class='titulos' style="font: 120% sans-serif; padding-left:10px; border-radius: 5px 0 0 0;">Bpim</td>
                                                            <td class='titulos' style="font: 120% sans-serif; ">Nombre</td>

                                                        </tr>
                                                    </thead>
                                                </table>
                                            </div>
                                            <div style="margin: 2px 0 0 0; border-radius: 0 0 0 6px; height: 200px; overflow: scroll; overflow-x: hidden; background: white; ">
                                                <table class='inicio inicio--no-shadow'>
                                                    <tbody>
                                                        <?php
                                                            $co ='zebra1';
                                                            $co2='zebra2';
                                                        ?>
                                                        <tr v-for="bpim in bpimSGR" v-on:click="seleccionarBpim(bpim)" class='<?php echo $co; ?>' style='text-rendering: optimizeLegibility; cursor: pointer !important; style=\"cursor: hand\"'>
                                                            <td width="20%" style="font: 120% sans-serif; padding-left:10px">{{ bpim[0] }}</td>
                                                            <td  style="font: 120% sans-serif; padding-left:10px">{{ bpim[1] }}</td>
                                                            <?php
                                                            $aux=$co;
                                                            $co=$co2;
                                                            $co2=$aux;
                                                            ?>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                        <div class="modal-footer d-flex justify-end p-2">
                                            <button type="button" class="btn btn-secondary" @click="showModal_bpim = false">Cerrar</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </transition>
                </div>

				<div v-show="showModal_tiposRecursos">
                    <transition name="modal">
                        <div class="modal-mask">
                            <div class="modal-wrapper">
                                <div class="modal-dialog modal-lg" style = "max-width: 1200px !important;" role="document">
                                    <div class="modal-content"  style = "width: 100% !important;" scrollable>
                                        <div class="modal-header">
                                            <h5 class="modal-title">{{ rubroVentanaEmergente }}</h5>
                                            <button type="button" @click="showModal_tiposRecursos = false" class="btn btn-close p-2"><div></div><div></div></button>
                                        </div>
                                        <div class="modal-body">
                                            <div style="margin: 2px 0 0 0;">
                                                <div class="row" style="margin: 4px; border-radius:4px; background-color: #F6F6F6; ">
                                                    <div class="col-md-2" style="padding: 12px 0px 0px 30px; font: 140% sans-serif;">
                                                        <label for="">Tipo de Recurso:</label>
                                                    </div>

                                                    <div class="col-md-6 col-md-offset-6" style="padding: 4px">
                                                        <input type="text" class="form-control" placeholder="Buscar por nombre o c&oacute;digo del tipo de recurso" style="font: sans-serif; " v-on:keyup="searchMonitorTipoRecurso" v-model="searchTipoRecurso.keywordTipoRecurso">
                                                    </div>
                                                </div>
                                                <table>
                                                    <thead>
                                                        <tr>
                                                            <td width="20%" class='titulos' style="font: 120% sans-serif; padding-left:10px; border-radius: 5px 0 0 0;">Codigo</td>
                                                            <td class='titulos' style="font: 120% sans-serif; ">Nombre</td>

                                                        </tr>
                                                    </thead>
                                                </table>
                                            </div>
                                            <div style="margin: 2px 0 0 0; border-radius: 0 0 0 6px; height: 200px; overflow: scroll; overflow-x: hidden; background: white; ">
                                                <table class='inicio inicio--no-shadow'>
                                                    <tbody>
                                                        <?php
                                                            $co ='zebra1';
                                                            $co2='zebra2';
                                                        ?>
                                                        <tr v-for="tipoRecurso in tiposRecursosSGR" v-on:click="seleccionarTipoRecurso(tipoRecurso)" class='<?php echo $co; ?>' style='text-rendering: optimizeLegibility; cursor: pointer !important; style=\"cursor: hand\"'>
                                                            <td width="20%" style="font: 120% sans-serif; padding-left:10px">{{ tipoRecurso[1] }}</td>
                                                            <td  style="font: 120% sans-serif; padding-left:10px">{{ tipoRecurso[2] }}</td>
                                                            <?php
                                                            $aux=$co;
                                                            $co=$co2;
                                                            $co2=$aux;
                                                            ?>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                        <div class="modal-footer d-flex justify-end p-2">
                                            <button type="button" class="btn btn-secondary" @click="showModal_tiposRecursos = false">Cerrar</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </transition>
                </div>

				<div v-show="showModal_terceros">
                    <transition name="modal">
                        <div class="modal-mask">
                            <div class="modal-wrapper">
                                <div class="modal-dialog modal-lg" style = "max-width: 1200px !important;" role="document">
                                    <div class="modal-content"  style = "width: 100% !important;" scrollable>
                                        <div class="modal-header">
                                            <h5 class="modal-title">{{ rubroVentanaEmergente }}</h5>
                                            <button type="button" @click="showModal_terceros = false" class="btn btn-close p-2"><div></div><div></div></button>
                                        </div>
                                        <div class="modal-body">
                                            <div style="margin: 2px 0 0 0;">
                                                <div class="row" style="margin: 4px; border-radius:4px; background-color: #F6F6F6; ">
                                                    <div class="col-md-2" style="padding: 12px 0px 0px 30px; font: 140% sans-serif;">
                                                        <label for="">Tercero:</label>
                                                    </div>

                                                    <div class="col-md-6 col-md-offset-6" style="padding: 4px">
                                                        <input type="text" class="form-control" placeholder="Buscar por nombre o c&oacute;digo del tercero" style="font: sans-serif; " v-on:keyup="searchMonitorTercero" v-model="searchTercero.keywordTercero">
                                                    </div>
                                                </div>
                                                <table>
                                                    <thead>
                                                        <tr>
                                                            <td width="20%" class='titulos' style="font: 120% sans-serif; padding-left:10px; border-radius: 5px 0 0 0;">Tercero</td>
                                                            <td class='titulos' style="font: 120% sans-serif; ">Nombre</td>

                                                        </tr>
                                                    </thead>
                                                </table>
                                            </div>
                                            <div style="margin: 2px 0 0 0; border-radius: 0 0 0 6px; height: 200px; overflow: scroll; overflow-x: hidden; background: white; ">
                                                <table class='inicio inicio--no-shadow'>
                                                    <tbody>
                                                        <?php
                                                            $co ='zebra1';
                                                            $co2='zebra2';
                                                        ?>
                                                        <tr v-for="tercero in tercerosSGR" v-on:click="seleccionarTercero(tercero)" class='<?php echo $co; ?>' style='text-rendering: optimizeLegibility; cursor: pointer !important; style=\"cursor: hand\"'>
                                                            <td width="20%" style="font: 120% sans-serif; padding-left:10px">{{ tercero[1] }}</td>
                                                            <td  style="font: 120% sans-serif; padding-left:10px">{{ tercero[2] }}</td>
                                                            <?php
                                                            $aux=$co;
                                                            $co=$co2;
                                                            $co2=$aux;
                                                            ?>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                        <div class="modal-footer d-flex justify-end p-2">
                                            <button type="button" class="btn btn-secondary" @click="showModal_terceros = false">Cerrar</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </transition>
                </div>

                <div v-show="showModal_politicas_publicas">
                    <transition name="modal">
                        <div class="modal-mask">
                            <div class="modal-wrapper">
                                <div class="modal-dialog modal-lg" style = "max-width: 1200px !important;" role="document">
                                    <div class="modal-content"  style = "width: 100% !important;" scrollable>
                                        <div class="modal-header">
                                            <h5 class="modal-title">{{ rubroVentanaEmergente }}</h5>
                                            <button type="button" @click="showModal_politicas_publicas = false" class="btn btn-close p-2"><div></div><div></div></button>
                                        </div>
                                        <div class="modal-body">
                                            <div style="margin: 2px 0 0 0;">
                                                <div class="row" style="margin: 4px; border-radius:4px; background-color: #F6F6F6; ">
                                                    <div class="col-md-2" style="padding: 12px 0px 0px 30px; font: 140% sans-serif;">
                                                        <label for="">Politica publica:</label>
                                                    </div>

                                                    <div class="col-md-6 col-md-offset-6" style="padding: 4px">
                                                        <input type="text" class="form-control" placeholder="Buscar por nombre o c&oacute;digo" style="font: sans-serif; " v-on:keyup="searchMonitorPoliticaPublica" v-model="searchPoliticaPublica.keywordPoliticaPublica">
                                                    </div>
                                                </div>
                                                <table>
                                                    <thead>
                                                        <tr>
                                                            <td width="20%" class='titulos' style="font: 120% sans-serif; padding-left:10px; border-radius: 5px 0 0 0;">Politica</td>
                                                            <td class='titulos' style="font: 120% sans-serif; ">Nombre</td>

                                                        </tr>
                                                    </thead>
                                                </table>
                                            </div>
                                            <div style="margin: 2px 0 0 0; border-radius: 0 0 0 6px; height: 200px; overflow: scroll; overflow-x: hidden; background: white; ">
                                                <table class='inicio inicio--no-shadow'>
                                                    <tbody>
                                                        <?php
                                                            $co ='zebra1';
                                                            $co2='zebra2';
                                                        ?>
                                                        <tr v-for="politica in politicasPublicasSGR" v-on:click="seleccionarPoliticaPublica(politica)" class='<?php echo $co; ?>' style='text-rendering: optimizeLegibility; cursor: pointer !important; style=\"cursor: hand\"'>
                                                            <td width="20%" style="font: 120% sans-serif; padding-left:10px">{{ politica[1] }}</td>
                                                            <td  style="font: 120% sans-serif; padding-left:10px">{{ politica[2] }}</td>
                                                            <?php
                                                            $aux=$co;
                                                            $co=$co2;
                                                            $co2=$aux;
                                                            ?>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                        <div class="modal-footer d-flex justify-end p-2">
                                            <button type="button" class="btn btn-secondary" @click="showModal_politicas_publicas = false">Cerrar</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </transition>
                </div>

                <div id="cargando" v-show="loading" class="loading">
                    <span>Cargando...</span>
                </div>

			</div>
		</div>

        <script type="module" src="./presupuesto_ccpet/reportes/ccp-homologarcuentasingejecusgr.js"></script>
        <script src="Librerias/vue/axios.min.js"></script>
        <script src="sweetalert2/dist/sweetalert2.min.js"></script>
        <link rel="stylesheet" href="sweetalert2/dist/sweetalert2.min.css">
        <script src="https://unpkg.com/vue@3/dist/vue.global.js"></script>
	</body>
</html>
