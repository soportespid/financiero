<?php
require "comun.inc";
require "funciones.inc";
session_start();
cargarcodigopag(@$_GET['codpag'], @$_SESSION['nivel']);
header("Cache-control: private"); // Arregla IE 6
date_default_timezone_set("America/Bogota");
?>
<!DOCTYPE>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="es">

<head>
    <meta http-equiv="Content-Type" content="text/html" charset="iso-8859-1" />
    <meta http-equiv="X-UA-Compatible" content="IE=9" />
    <title>:: IDEAL 10 - Parametrización</title>
    <link href="css/css2.css" rel="stylesheet" type="text/css" />
    <link href="css/css3.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="css/programas.js"></script>
    <link rel="stylesheet" href="bootstrap/css/bootstrap.css">
    <link rel="stylesheet" href="bootstrap/css/estilos.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css"
        integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">

    <?php titlepag(); ?>

    <style>
        .background_active {
            color: white;
            background: gray;
        }

        .inicio--no-shadow {
            box-shadow: none;
        }

        .btn-delete {
            background: red;
            color: white;
            border-radius: 5px;
            border: none;
            font-size: 13px;
        }

        .btn-delete:hover,
        .btn-delete:focus {
            background: white;
            color: red;
        }

        .zebra1 {
            cursor: pointer;
        }

        .msm-error {
            font-size: 13px;
            margin-bottom: 0;
        }

        .frame_cuentasector {
            background: #E1E2E2;
            padding: 10px;
            border-radius: 10px;
        }

        .btn-primary {
            background: #39C;
        }
    </style>
</head>

<body>
    <IFRAME src="alertas.php" name="alertas" id="alertas" style="display:none"></IFRAME>
    <span id="todastablas2"></span>
    <table>
        <tr>
            <script>barra_imagenes("para");</script><?php cuadro_titulos(); ?>
        </tr>
        <tr><?php menu_desplegable("para"); ?></tr>
    </table>
    <div class="bg-white group-btn p-1"><button type="button" class="btn btn-white btn-primary-hover d-flex justify-between align-items-center">
            <span>Nuevo</span>
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 -960 960 960"><path d="M440-440H200v-80h240v-240h80v240h240v80H520v240h-80v-240Z"></path></svg>
        </button><button type="button" onclick="mypop=window.open('plan-agenda.php','','');mypop.focus()" class="btn btn-white btn-primary-hover d-flex justify-between align-items-center">
            <span class="group-hover:text-white">Agenda</span>
            <svg class="fill-black group-hover:fill-white w-5 h-5" xmlns="http://www.w3.org/2000/svg" viewBox="0 -960 960 960"><path d="M560-564v-68q33-14 67.5-21t72.5-7q26 0 51 4t49 10v64q-24-9-48.5-13.5T700-600q-38 0-73 9.5T560-564Zm0 220v-68q33-14 67.5-21t72.5-7q26 0 51 4t49 10v64q-24-9-48.5-13.5T700-380q-38 0-73 9t-67 27Zm0-110v-68q33-14 67.5-21t72.5-7q26 0 51 4t49 10v64q-24-9-48.5-13.5T700-490q-38 0-73 9.5T560-454ZM260-320q47 0 91.5 10.5T440-278v-394q-41-24-87-36t-93-12q-36 0-71.5 7T120-692v396q35-12 69.5-18t70.5-6Zm260 42q44-21 88.5-31.5T700-320q36 0 70.5 6t69.5 18v-396q-33-14-68.5-21t-71.5-7q-47 0-93 12t-87 36v394Zm-40 118q-48-38-104-59t-116-21q-42 0-82.5 11T100-198q-21 11-40.5-1T40-234v-482q0-11 5.5-21T62-752q46-24 96-36t102-12q58 0 113.5 15T480-740q51-30 106.5-45T700-800q52 0 102 12t96 36q11 5 16.5 15t5.5 21v482q0 23-19.5 35t-40.5 1q-37-20-77.5-31T700-240q-60 0-116 21t-104 59ZM280-494Z"></path></svg>
        </button><button type="button" onclick="mypop=window.open('para-principal.php','','');mypop.focus();" class="btn btn-white btn-primary-hover d-flex justify-between align-items-center">
            <span>Nueva ventana</span>
            <svg class="fill-black group-hover:fill-white w-5 h-5" xmlns="http://www.w3.org/2000/svg" viewBox="0 -960 960 960"><path d="M320-120v-80h80v-80H160q-33 0-56.5-23.5T80-360v-400q0-33 23.5-56.5T160-840h640q33 0 56.5 23.5T880-760v400q0 33-23.5 56.5T800-280H560v80h80v80H320ZM160-360h640v-400H160v400Zm0 0v-400 400Z"></path></svg>
        </button><button type="button" onclick="mypop=window.open('<?php echo $url2; ?>','','');mypop.focus();" class="btn btn-white btn-primary-hover d-flex justify-between align-items-center">
            <span class="group-hover:text-white">Duplicar pantalla</span>
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 -960 960 960"><path d="M640-160v-360H160v360h480Zm80-200v-80h80v-360H320v200h-80v-200q0-33 23.5-56.5T320-880h480q33 0 56.5 23.5T880-800v360q0 33-23.5 56.5T800-360h-80ZM160-80q-33 0-56.5-23.5T80-160v-360q0-33 23.5-56.5T160-600h480q33 0 56.5 23.5T720-520v360q0 33-23.5 56.5T640-80H160Zm400-603ZM400-340Z"></path></svg>
        </button>
        <button type="button" onclick="location.href='cont-menupresupuesto.php'" class="btn btn-success btn-success-hover d-flex justify-between align-items-center">
            <span>Atras</span>
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 -960 960 960"><path d="M760-200v-160q0-50-35-85t-85-35H273l144 144-57 56-240-240 240-240 57 56-144 144h367q83 0 141.5 58.5T840-360v160h-80Z"></path></svg>
        </button></div>
    <div class="subpantalla" style="height:80.5%; width:99.6%; padding: 20px 50px 0; overflow-x:hidden;">
        <div id="myapp" class="frame_cuentasector">
            <div class="row">
                <div class="col">
                    <div class="row" style="margin: 0 0 0 0; border-radius:4px; background-color: #fff; ">
                        <div class="col-md-3" style="padding: 12px 0px 0px 30px; font: 140% sans-serif;">
                            <label for="">Sectores</label>
                        </div>
                        <div class="col-md-6" style="padding: 4px">
                            <input type="text" class="form-control" style="height: auto; border-radius:0;"
                                placeholder="Ej: Ciencia, tecnolog&iacute;a e innovaci&oacute;n"
                                v-on:keyup="searchMonitor" v-model="search.keyword">
                        </div>
                    </div>
                    <div style="margin: 4px 0 0">
                        <table>
                            <thead>
                                <tr>
                                    <td width="1%" class='titulos'
                                        style="font: 120% sans-serif; border-radius: 5px 0px 0px;">C&oacute;digo</td>
                                    <td width="8%" class='titulos' style="font: 120% sans-serif;">Nombre</td>
                                </tr>
                            </thead>
                        </table>
                    </div>
                    <div
                        style="margin: 0px 0 20px; border-radius: 0 0 0 15px; height: 200px; overflow: scroll; overflow-x: hidden; background: white; ">
                        <table class='inicio inicio--no-shadow'>
                            <tbody v-if="show_resultados_sectores">
                                <?php
                                $co = 'zebra1';
                                $co2 = 'zebra2';
                                ?>
                                <tr v-for="result in results_sectores" v-on:click="chosenCuentaSector(result, 'sector')"
                                    v-bind:class="result[1] === app.codigo_sector  ? 'background_active' : ''"
                                    class='<?php echo $co; ?>  text-rendering: optimizeLegibility; cursor: pointer important; style=\"cursor: hand\"'>
                                    <td width="1%" style="font: 120% sans-serif;">{{ result[1] }}</td>
                                    <td width="8%" style="font: 120% sans-serif;">{{ result[2] }}</td>
                                    <?php
                                    $aux = $co;
                                    $co = $co2;
                                    $co2 = $aux;
                                    ?>
                                </tr>
                            </tbody>
                            <tbody v-else>
                                <tr>
                                    <td width="20%" style="font: 120% sans-serif; padding-left:10px; text-align:center;"
                                        colspan="3">Sin resultados</td>
                                </tr>
                            </tbody>
                            <tbody v-if="show_sectores_parametri">
                                <tr>
                                    <td width="20%" style="font: 120% sans-serif; padding-left:10px; text-align:center;"
                                        colspan="3">Todos los sectores parametrizados</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>

                <div class="col">
                    <div class="row" style="margin: 0 0 0 0; border-radius:4px; background-color: #fff; ">
                        <div class="col-md-3" style="padding: 12px 0px 0px 30px; font: 140% sans-serif;">
                            <label for="">Cuentas</label>
                        </div>
                    </div>
                    <div style="margin: 4px 0 0">
                        <table>
                            <thead>
                                <tr>
                                    <td width="1%" class='titulos'
                                        style="font: 120% sans-serif; border-radius: 5px 0px 0px;">Cuenta</td>
                                    <td width="8%" class='titulos' style="font: 120% sans-serif;">Nombre</td>
                                </tr>
                            </thead>
                        </table>
                    </div>
                    <div
                        style="margin: 0px 0 20px; border-radius: 0 0 0 15px; height: 200px; overflow: scroll; overflow-x: hidden; background: white; ">
                        <table class='inicio inicio--no-shadow'>
                            <tbody v-if="show_resultados_cuenta">
                                <?php
                                $co = 'zebra1';
                                $co2 = 'zebra2';
                                ?>
                                <tr v-for="result in results_cuentas" v-on:click="chosenCuentaSector(result, 'cuenta')"
                                    v-bind:class="result[0] === app.codigo_cuenta  ? 'background_active' : ''"
                                    class='<?php echo $co; ?> text-rendering: optimizeLegibility; cursor: pointer important; style=\"cursor: hand\"'>
                                    <td width="1%" style="font: 120% sans-serif;">{{ result[0] }}</td>
                                    <td width="8%" style="font: 120% sans-serif;">{{ result[1] }}</td>
                                    <?php
                                    $aux = $co;
                                    $co = $co2;
                                    $co2 = $aux;
                                    ?>
                                </tr>
                            </tbody>
                            <tbody v-if="show_selecciona_sector">
                                <tr>
                                    <td width="20%" style="font: 120% sans-serif; padding-left:10px; text-align:center;"
                                        colspan="3"><- Selecciona un Sector</td>
                                </tr>
                            </tbody>
                            <tbody v-if="show_sectores_parametri">
                                <tr>
                                    <td width="20%" style="font: 120% sans-serif; padding-left:10px; text-align:center;"
                                        colspan="3">Todos los sectores parametrizados</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="col-2" style="display: grid;">
                    <div style="align-self: center;">
                        <button type="submit" class="col btn btn-primary" v-on:click="addCuentaSector" value="Buscar"
                            style="border-radius:5px;">Agregar</button>
                        <div v-if="seleccione_sector" class="alert alert-danger">
                            <p class="msm-error">Seleccione un sector</p>
                        </div>
                        <div v-if="seleccione_cuenta" class="alert alert-danger">
                            <p class="msm-error">Seleccione una cuenta</p>
                        </div>
                        <div v-if="sector_paramatri" class="alert alert-danger">
                            <p class="msm-error">Este sector ya esta parametrizado, recargue la pagina!</p>
                        </div>
                    </div>
                </div>
            </div>
            <table>
                <thead>
                    <tr>
                        <td width="10%" class='titulos' style="font: 120% sans-serif; border-radius: 5px 0px 0px;">Id
                            sector</td>
                        <td width="35%" class='titulos' style="font: 120% sans-serif;">Nombre sector</td>
                        <td width="10%" class='titulos' style="font: 120% sans-serif;">Id cuenta</td>
                        <td width="35%" class='titulos' style="font: 120% sans-serif;">Cuenta</td>
                        <td width="10%" class='titulos' style="font: 120% sans-serif; border-radius: 0px 5px 0px;">
                            Eliminar</td>
                    </tr>
                </thead>
            </table>
            <div
                style="margin: 0px 0 20px; border-radius: 0 0 0 15px; height: 75%; overflow: scroll; overflow-x: hidden; background: white; ">
                <table class='inicio inicio--no-shadow'>
                    <tbody v-if="show_resultados_cuentasectores">
                        <?php
                        $co = 'zebra1';
                        $co2 = 'zebra2';
                        ?>
                        <tr v-for="result in results_cuentasectores"
                            class='<?php echo $co; ?> text-rendering: optimizeLegibility; cursor: pointer important; style=\"cursor: hand\"'>
                            <td width="10%" style="font: 120% sans-serif;">{{ result[1] }}</td>
                            <td width="35%" style="font: 120% sans-serif;">{{ result[2] }}</td>
                            <td width="10%" style="font: 120% sans-serif;">{{ result[3] }}</td>
                            <td width="35%" style="font: 120% sans-serif;">{{ result[4] }}</td>
                            <td width="10%" style="font: 120% sans-serif;"><button class="btn-delete"
                                    v-on:click="deleteCuentaSector(result[0])">Eliminar</button></td>
                            <?php
                            $aux = $co;
                            $co = $co2;
                            $co2 = $aux;
                            ?>
                        </tr>
                    </tbody>
                    <tbody v-else>
                        <tr>
                            <td width="20%" style="font: 120% sans-serif; padding-left:10px; text-align:center;"
                                colspan="3">No hay sectores parametrizados</td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>

    <script src="Librerias/vue/vue.min.js"></script>
    <script src="Librerias/vue/axios.min.js"></script>
    <script src="vue/ccp-cuentasectores.js?<?php echo date('d_m_Y_h_i_s'); ?>"></script>
</body>

</html>
