<?php 
	require "comun.inc";
	require "funciones.inc";
	session_start();
	$linkbd = conectar_v7();	
	cargarcodigopag($_GET['codpag'],$_SESSION["nivel"]);
	header("Cache-control: private");
	date_default_timezone_set("America/Bogota");
?>
<!DOCTYPE >
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="es">
	<head>
	 	<meta http-equiv="Content-Type" content="text/html" charset="iso-8859-1"/>
        <meta http-equiv="X-UA-Compatible" content="IE=9"/>
		<title>:: Bancos</title>
        <link href="css/css2.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="css/calendario.js"></script>
        
		<script>
			function ponprefijo(codigo, nombre)
			{
				parent.document.form2.nombreBanco.value = nombre;
				parent.document.form2.codigoBanco.value = codigo;
				parent.despliegaModalBancos("hidden");
			} 
		</script> 

		<?php titlepag();?>

	</head>

	<body>
		<?php
			
		?>
	<form action="" method="post" enctype="multipart/form-data" name="form1">
		<table class="inicio">
			<tr>
				<td height="25" colspan="4" class="titulos">.: Busca bancos</td>

				<td class="cerrar">
                    <a onClick="parent.despliegaModalBancos('hidden');">Cerrar</a>
                </td>
			</tr>

			<tr>
				<td class="saludo1" style="width: 10%;">.: Codigo o nombre banco:</td>

				<td colspan="3">
                    <input type="text" name="filtro" id="filtro" value="<?php echo $_POST['filtro'] ?>" style="width: 80%;"> 
                    <input type="submit" name="Submit" value="Buscar">
					<input type="hidden" name="oculto" id="oculto" value="1">
				</td>
			</tr>
		</table>

		<div class="subpantalla" style="height:85%; width:99.6%; overflow-x:hidden;">
			<table class="inicio">
				<tr>
					<td height="25" colspan="4" class="titulos">:. Resultados Busqueda</td>
				</tr>

				<tr>
					<td class="titulos2" width="10%" style="text-align: center;">Codigo banco</td>
					<td class="titulos2">Nombre</td>
					<td class="titulos2" width="10%" style="text-align: center;">Cuenta nacional</td>	  
				</tr>
				<?php
				$vigusu=vigencia_usuarios($_SESSION['cedulausu']); 
				$oculto=$_POST['oculto'];
				//echo $oculto;
				//if($oculto!="")
				{
					$linkbd = conectar_v7();

                    $crit = "";

                    if ($_POST['filtro'] != "") {

                        $crit = "WHERE concat_ws(' ', codigo,nombre) LIKE '%$_POST[filtro]%'";
                    }
				
					$sqlr = "SELECT * FROM hum_bancosfun $crit ORDER BY CONVERT(codigo, SIGNED INTEGER)";
					$resp = mysqli_query($linkbd, $sqlr);			

					$co = 'saludo1a';
					$co2 = 'saludo2';	

					while($row = mysqli_fetch_row($resp)) 
					{
						echo" 
						<tr class='$co' style='text-transform:uppercase;' onClick=\"javascript:ponprefijo('$row[0]','$row[1]')\">
                            <td style='text-align: center;'>$row[0]</td>
                            <td>$row[1]</td>
                            <td style='text-align: center;'>$row[2]</td>
						</tr> ";
						
						$aux=$co;
						$co=$co2;
						$co2=$aux;
					}
					$_POST['oculto']="";
				}
				?>
			</table>
		</div>
	</form>
</body>
</html> 
