<?php  
	require_once 'PHPExcel/Classes/PHPExcel.php';
	require"comun.inc";
	require"funciones.inc";
	session_start();
	$linkbd=conectar_bd();  
	$objPHPExcel = new PHPExcel();
	//----Propiedades----
	$objPHPExcel->getProperties()
			->setCreator("SPID")
			->setLastModifiedBy("SPID")
			->setTitle("Exportar Excel con PHP")
			->setSubject("Reporte Subsidos Terceros")
			->setDescription("Reporte Subsidios por servicios y estrato")
			->setKeywords("usuarios phpexcel")
			->setCategory("reportes");
	//----Cuerpo de Documento----
	$objPHPExcel->getActiveSheet()->mergeCells('A1:H1');
	$objPHPExcel->getActiveSheet()->mergeCells('A2:H2');
	$objPHPExcel->setActiveSheetIndex(0)
				->setCellValue('A1', "Subsidos Asignados a Terceros $_POST[mesletra] $_POST[vigencias]")
				->setCellValue('A2', "$_POST[nomservi] Estrato: $_POST[estralet]");
	$objFont=$objPHPExcel->getActiveSheet()->getStyle('A1:A2')->getFont();
	$objFont->setName('Courier New'); 
	$objFont->setSize(15); 
	$objFont->setBold(true); 
	$objFont->setUnderline(PHPExcel_Style_Font::UNDERLINE_SINGLE);
	$objFont->getColor()->setARGB( PHPExcel_Style_Color::COLOR_BLACK);
	$objAlign=$objPHPExcel->getActiveSheet()->getStyle('A1:A2')->getAlignment(); 
	$objAlign->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER); 
	$objAlign->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER); 
	$objPHPExcel-> getActiveSheet ()
		-> getStyle ("A2:H2")	
        -> getFill ()
        -> setFillType (PHPExcel_Style_Fill :: FILL_SOLID)
        -> getStartColor ()
        -> setRGB ('A6E5F3');
	$objPHPExcel-> getActiveSheet ()
			-> getStyle ("A1")	
			-> getFill ()
			-> setFillType (PHPExcel_Style_Fill :: FILL_SOLID)
			-> getStartColor ()
			-> setRGB ('A6E5F3');
	$borders = array(
		  'borders' => array(
			'allborders' => array(
			  'style' => PHPExcel_Style_Border::BORDER_THIN,
			  'color' => array('argb' => 'FF000000'),
			)
		  ),
		);
	$objPHPExcel->getActiveSheet()->getStyle('A1:H1')->applyFromArray($borders);
	$objPHPExcel->getActiveSheet()->getStyle('A2:H2')->applyFromArray($borders);
	$objPHPExcel->getActiveSheet()->getStyle('A3:H3')->applyFromArray($borders);
	$objPHPExcel->setActiveSheetIndex(0)
				->setCellValue('A3', 'Num.')
				->setCellValue('B3', 'Cod. Cliente')
				->setCellValue('C3', 'Cod. Catastral')
				->setCellValue('D3', 'Nombre')
				->setCellValue('E3', utf8_encode ('Direcci�n'))
				->setCellValue('F3', 'Tarifa')
				->setCellValue('G3', 'Subsidio')
				->setCellValue('H3', 'Factura');
	$i=4;
	$cont=1;
	$sqlr="SELECT cliente,codcatastral,nomcliente,direccion,tarifa,subsidio,factura FROM servlistasubsidiost ORDER BY CAST(cliente AS UNSIGNED)";
	$resp = mysql_query($sqlr,$linkbd);
	while ($row =mysql_fetch_row($resp))
	{
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0,$i,$cont);
		$objPHPExcel->getActiveSheet()->setCellValueExplicit ("B".$i,$row[0], PHPExcel_Cell_DataType :: TYPE_STRING);
		$objPHPExcel->getActiveSheet()->setCellValueExplicit ("C".$i,$row[1], PHPExcel_Cell_DataType :: TYPE_STRING);
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(3,$i,utf8_encode ($row[2]));
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(4,$i,utf8_encode ($row[3]));
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(5,$i,$row[4]);
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(6,$i,$row[5]);
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(7,$i,$row[6]);
		$objPHPExcel->getActiveSheet()->getStyle("A$i:H$i")->applyFromArray($borders);
		$i++;
		$cont++;
	}
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(4,$i,'Totales:');
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(5,$i,'=SUM(F4:F'.($i-1).')');
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(6,$i,'=SUM(G4:G'.($i-1).')');
	$objPHPExcel->getActiveSheet()->getStyle("F$i:G$i")->applyFromArray($borders);
	//----Propiedades de la hoja
	$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true); 
	$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true); 
	$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true); 
	$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true); 
	$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true); 
	$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true); 
	$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setAutoSize(true); 
	$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setAutoSize(true); 
	$objPHPExcel->getActiveSheet()->setTitle('Subsidios');
	$objPHPExcel->setActiveSheetIndex(0);
	//----Guardar documento----
	header('Content-Type: application/vnd.ms-excel');
	header("Content-Disposition: attachment;filename='Terceros Subsidios $_POST[mesletra] $_POST[vigencias].xls'");
	header('Cache-Control: max-age=0'); 
	$objWriter=PHPExcel_IOFactory::createWriter($objPHPExcel,'Excel5');
	$objWriter->save('php://output');
	exit;
?>