<?php
	header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=uft8");
	require "comun.inc";
	require "funciones.inc";
    require "funcionesSP.inc.php";
	session_start();
	$linkbd=conectar_v7();
	$linkbd -> set_charset("utf8");
	cargarcodigopag($_GET['codpag'],$_SESSION["nivel"]);
	date_default_timezone_set("America/Bogota");
?>
<!DOCTYPE >
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="es">
	<head>
		<meta http-equiv="Content-type" content="text/html;" charset="utf8"/>
		<meta http-equiv="X-UA-Compatible" content="IE=9"/>
		<title>:: IDEAL 10 - Servicios P&uacute;blicos</title>
		<link href="css/css2.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
        <link href="css/cssSP.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<script type="text/javascript" src="jquery-1.11.0.min.js"></script> 
		<script type="text/javascript" src="css/programas.js"></script>
        <script src="sweetalert2/dist/sweetalert2.min.js"></script>
		<link rel="stylesheet" href="sweetalert2/dist/sweetalert2.min.css">
		<script type="text/javascript" src="css/programas.js"></script>

		<script>
			$(window).load(function () {
				$('#cargando').hide();
			});
			
			function verUltimaPos(cliente, factura)
			{
				location.href="serv-visualizarFactura.php?cliente="+cliente+"&factura="+factura;
			}

            function limbusquedas1()
            {
				var numeroFacturas = document.getElementById('numtop').value;
				
				if (numeroFacturas == '') {
						
					document.form2.bandera.value=1;
					document.form2.submit();
				
				}
				else {
					document.getElementById('numtop').value = '';
					document.form2.bandera.value=0;
					document.form2.submit();
				}
					
            }
		</script>
		<?php 
			titlepag();
			$scrtop= @ $_GET['scrtop'];
			if($scrtop=="") $scrtop=0;
			echo"<script>
				window.onload=function(){
					$('#divdet').scrollTop(".$scrtop.")
				}
			</script>";
			$gidcta=@ $_GET['idcta'];
			if(isset($_GET['filtro']))
			$_POST['nombre']=$_GET['filtro'];
		?>
	</head>
	<body>
		<IFRAME src="alertas.php" name="alertas" id="alertas" style="display:none"></IFRAME>
		<span id="todastablas2"></span>
		<table>
			<tr><script>barra_imagenes("serv");</script><?php cuadro_titulos();?></tr>	 
			<tr><?php menu_desplegable("serv");?></tr>
			<tr>
				<td colspan="3" class="cinta">
					<a class="mgbt"><img src="imagenes/add2.png"/></a>
					<a class="mgbt"><img src="imagenes/guardad.png" title="Guardar"/></a>
					<a href="serv-buscarLiquidacion.php" class="mgbt"><img src="imagenes/busca.png" title="Buscar"/></a>
					<a onclick="mypop=window.open('plan-agenda.php','','');mypop.focus()" class="mgbt"><img src="imagenes/agenda1.png" title="Agenda" /></a>
					<a onclick="mypop=window.open('serv-principal.php','','');mypop.focus();" class="mgbt"><img src="imagenes/nv.png" title="Nueva ventana"></a>
					<a onclick="mypop=window.open('<?php echo $url2; ?>','','');mypop.focus();" class="mgbt"><img src="imagenes/duplicar_pantalla.png" title="Duplicar pesta&ntilde;a"></a>
					<a href="serv-menuLiquidacion.php" class="mgbt"><img src="imagenes/iratras.png" title="Atr&aacute;s"></a>
                </td>
			</tr>
		</table>
		<?php
			if(@ $_POST['oculto']=="")
			{
				$_POST['numres']=10;$_POST['numpos']=0;$_POST['nummul']=0;
			}
			if(@ $_GET['numpag']!="")
			{
				if(@ $_POST['oculto']!=2)
				{
					$_POST['numres']=$_GET['limreg'];
					$_POST['numpos']=$_GET['limreg']*($_GET['numpag']-1);
					$_POST['nummul']=$_GET['numpag']-1;
				}
			}
			else
			{
				if(@ $_POST['nummul']=="")
				{
					$_POST['numres']=10;
					$_POST['numpos']=0;
					$_POST['nummul']=0;
				}
			}
		?>
		<div id="bgventanamodalm" class="bgventanamodalm">
			<div id="ventanamodalm" class="ventanamodalm">
				<IFRAME src="" name="ventanam" marginWidth=0 marginHeight=0 frameBorder=0 id="ventanam" frameSpacing=0 style=" width:700px; height:130px; top:200; overflow:hidden;"></IFRAME>
			</div>
		</div>
		<form name="form2" method="post" action="">
			<input type="hidden" name="numres" id="numres" value="<?php echo @ $_POST['numres'];?>"/>
			<input type="hidden" name="numpos" id="numpos" value="<?php echo @ $_POST['numpos'];?>"/>
			<input type="hidden" name="nummul" id="nummul" value="<?php echo @ $_POST['nummul'];?>"/>
			<input type="hidden" name="oculto" id="oculto" value="1">
            <input type="hidden" name="bandera" id="bandera" value="<?php echo @ $_POST['bandera'];?>">
			
			<table class="inicio">
				<tr>
					<td class="titulos" colspan="9">:: Buscar Factura</td>
					<td class="cerrar" style="width:7%" onClick="location.href='serv-principal.php'">Cerrar</td>
				</tr>
				<tr>
					<td class="tamano01" style='width:3cm;'>:: C&oacute;digo Factura:</td>
					<td>
                        <input type="search" name="codigoFactura" id="codigoFactura" value="<?php echo @ $_POST['codigoFactura'];?>" style='width:100%;height:30px;' onkeydown = "if (event.keyCode == 13){document.getElementById('filtro').click()}"; autocomplete="off"/>
                    </td>

                    <td class="tamano01" style='width:3cm;'>:: C&oacute;digo Usuario:</td>

                    <td>
                        <input type="search" name="codigoUsuario" id="codigoUsuario" value="<?php echo @ $_POST['codigoUsuario'];?>" style='width:100%;height:30px;' onkeydown = "if (event.keyCode == 13){document.getElementById('filtro').click()}"; autocomplete="off"/>
                    </td>

					<td class="tamano01" style='width:3cm;'>:: Nombre Usuario:</td>

                    <td>
                        <input type="search" name="nombreUsuario" id="nombreUsuario" value="<?php echo @ $_POST['nombreUsuario'];?>" style='width:100%;height:30px;' onkeydown = "if (event.keyCode == 13){document.getElementById('filtro').click()}"; autocomplete="off"/>
                    </td>
                    
					<td style="padding-bottom:0px"><em class="botonflecha" id="filtro" onClick="limbusquedas1();">Buscar</em></td>
					
				</tr>

			</table>

			<div id="cargando" style=" position:absolute;left: 46%; bottom: 45%">
				<img src="imagenes/loading.gif" style=" width: 80px; height: 80px"/>
			</div>

			<div class="subpantallac5" style="height:66%; width:99.6%; margin-top:0px; overflow-x:hidden" id="divdet">
			<?php
				if($_POST['bandera'] == 1)
                {
                    if(@$_POST['codigoUsuario'] != '')
                    {
                        $crit1 = "AND concat_ws('', C.cod_usuario) = '".$_POST['codigoUsuario']."' ";
                    }
                    

                    if(@$_POST['codigoFactura'] != '')
                    {
                        $crit1 = "AND D.numero_facturacion = '".$_POST['codigoFactura']."' ";   
                    }
                    
                    if (@$_POST['nombreUsuario'] != '') {

                        $nombredividido = array();
                        $nombredividido = explode(" ", $_POST['nombreUsuario']);
                        
                        for ($i=0; $i < count($nombredividido); $i++) 
                        { 
                            $busqueda = '';
                            $busqueda = "AND concat_ws(' ', T.nombre1, T.nombre2, T.apellido1, T.apellido2, T.razonsocial) LIKE '%$nombredividido[$i]%' ";

                            $crit1 = $crit1 . $busqueda;
                        }
                    }

                    $sqlr = "SELECT D.id, D.id_cliente, D.id_corte, D.numero_facturacion, D.estado_pago, C.id_tercero, C.cod_usuario, T.nombre1, T.nombre2, T.apellido1, T.apellido2, T.razonsocial, T.cedulanit FROM srvcortes_detalle AS D INNER JOIN srvclientes AS C ON D.id_cliente = C.id INNER JOIN terceros AS T ON C.id_tercero = T.id_tercero WHERE D.id_corte != '' AND D.estado_pago <> 'N' $crit1 ORDER BY numero_facturacion DESC";
                    $resp = mysqli_query($linkbd,$sqlr);
                    $_POST['numtop'] = mysqli_num_rows($resp);
                    $nuncilumnas = ceil($_POST['numtop']/$_POST['numres']);
                    $cond2="";

                    if (@ $_POST['numres']!="-1")
                    {
                        $cond2="LIMIT ".$_POST['numpos'].", ".$_POST['numres']; 
                    }

                    $sqlr = "SELECT D.id, D.id_cliente, D.id_corte, D.numero_facturacion, D.estado_pago, C.id_tercero, C.cod_usuario, T.nombre1, T.nombre2, T.apellido1, T.apellido2, T.razonsocial, T.cedulanit FROM srvcortes_detalle AS D INNER JOIN srvclientes AS C ON D.id_cliente = C.id INNER JOIN terceros AS T ON C.id_tercero = T.id_tercero WHERE D.id_corte != '' AND D.estado_pago <> 'N' $crit1 ORDER BY numero_facturacion DESC $cond2";
                    $resp = mysqli_query($linkbd,$sqlr);
                    $con = 1;
                    $numcontrol=$_POST['nummul']+1;
                }
                    

				if(($nuncilumnas==$numcontrol)||(@ $_POST['numres']=="-1"))
				{
					$imagenforward="<img src='imagenes/forward02.png' style='width:17px;cursor:default;'>";
					$imagensforward="<img src='imagenes/skip_forward02.png' style='width:16px;cursor:default;' >";
				}
				else 
				{
					$imagenforward="<img src='imagenes/forward01.png' style='width:17px;cursor:pointer;' title='Siguiente' onClick='numsiguiente()'>";
					$imagensforward="<img src='imagenes/skip_forward01.png' style='width:16px;cursor:pointer;' title='Fin' onClick='saltocol(\"$nuncilumnas\")'>";
				}
				if((@ $_POST['numpos']==0)||(@ $_POST['numres']=="-1"))
				{
					$imagenback="<img src='imagenes/back02.png' style='width:17px;cursor:default;'>";
					$imagensback="<img src='imagenes/skip_back02.png' style='width:16px;cursor:default;'>";
				}
				else
				{
					$imagenback="<img src='imagenes/back01.png' style='width:17px;cursor:pointer;' title='Anterior' onClick='numanterior();'>";
					$imagensback="<img src='imagenes/skip_back01.png' style='width:16px;cursor:pointer;' title='Inicio' onClick='saltocol(\"1\")'>";
				}
				$con=1;
				echo "
					<table class='inicio'>
						<tr>
							<td colspan='8' class='titulos'>.: Resultados Busqueda:</td>
							<td class='submenu'>
								<select name='renumres' id='renumres' onChange='cambionum();' style='width:100%'>
									<option value='10'"; if (@$_POST['renumres']=='10'){echo 'selected';} echo ">10</option>
									<option value='20'"; if (@$_POST['renumres']=='20'){echo 'selected';} echo ">20</option>
									<option value='30'"; if (@$_POST['renumres']=='30'){echo 'selected';} echo ">30</option>
									<option value='50'"; if (@$_POST['renumres']=='50'){echo 'selected';} echo ">50</option>
									<option value='100'"; if (@$_POST['renumres']=='100'){echo 'selected';} echo ">100</option>
									<option value='-1'"; if (@$_POST['renumres']=='-1'){echo 'selected';} echo ">Todos</option>
								</select>
							</td>
						</tr>

						<tr>
							<td colspan='9'>Facturas Encontradas: ".$_POST['numtop']."</td>
						</tr>

						<tr style='text-align:center;'>
                            <td class='titulos2' style='width:5%;height: 30px;'>N&deg; Corte</td>
							<td class='titulos2' style='width:5%;'>Numero de Factura</td>
							<td class='titulos2' style='width:5%;'>ID Cliente</td>
							<td class='titulos2' style='width:10%;'>Cod usuario</td>
                            <td class='titulos2'>Nombre Usuario</td>
                            <td class='titulos2' style='width:10%;'>N&deg; Documento</td>
                            <td class='titulos2' style='width:15%;'>Dirrecci&oacute;n</td>
                            <td class='titulos2' style='width:5%;'>Total Pago</td>
							<td class='titulos2' style='width:7%;'>Estado</td>
						</tr>";
						$iter='saludo1a';
						$iter2='saludo2';
						$filas=1;
						while ($row = mysqli_fetch_row($resp))
						{
                            if($row[11] != '')
                            {
                                $nombre = $row[11];
                            }
                            else
                            {
                                $nombre = $row[7].' '.$row[8].' '.$row[9].' '.$row[10];
                            }

                            $sqlDireccion = "SELECT direccion FROM srvdireccion_cliente WHERE id_cliente = '$row[1]' ";
                            $respDireccion = mysqli_query($linkbd,$sqlDireccion);
                            $rowDireccion = mysqli_fetch_row($respDireccion);

                            $sqlDetallesFacturacion = "SELECT SUM(credito), SUM(debito) FROM srvdetalles_facturacion WHERE id_cliente = '$row[1]' AND corte = '$row[2]' AND tipo_movimiento = '101'";
                            $respDetallesFacturacion = mysqli_query($linkbd,$sqlDetallesFacturacion);
                            $rowDetallesFacturacion = mysqli_fetch_row($respDetallesFacturacion);
                            
							$totalFactura = $rowDetallesFacturacion[0] - $rowDetallesFacturacion[1];
							$totalFactura = ($totalFactura / 100);
							$totalFactura = ceil($totalFactura);
							$totalFactura = $totalFactura * 100;

							switch ($row[4]) 
						    {
                                case 'S': 	$imgsem="<img src='imagenes/sema_amarilloON.jpg' title='Pago Pendiente' style='width:20px;'/>";
                                            break;
                                case 'P':	$imgsem="<img src='imagenes/sema_verdeON.jpg' title='Pago Realizado' style='width:20px;'/>";
                                            break;
                                case 'V':	$imgsem="<img src='imagenes/sema_rojoON.jpg' title='Pago Vencido' style='width:20px;'/>";
                                            break;
                                case 'R':	$imgsem="<img src='imagenes/sema_amarilloOFF.jpg' title='Factura reversada' style='width:20px;'/>";
                                            break;
                                case 'A':	$imgsem="<img src='imagenes/sema_azulON.jpg' title='Acuerdo de Pago' style='width:20px;'/>"
                                            ;break;
								case 'AB':	$imgsem="<img src='imagenes/sema_verdeR.jpg' title='Abono' style='width:20px;'/>"
                                            ;break;			
                                default:	$imgsem="";
						    }

							if($gidcta!="")
							{
								if($gidcta==$row[3])
								{
									$estilo='background-color:yellow';
								}
								else
								{
									$estilo="";
								}
							}
							else
							{
								$estilo="";
							}

							$cliente = "'$row[1]'";
							$factura = "'$row[3]'";
							$numfil = "'$filas'";
							$filtro = "'".@ $_POST['nombre']."'";

							echo"
							<tr class='$iter' onDblClick=\"verUltimaPos($cliente,$factura)\" style='text-transform:uppercase; $estilo'>
								<td class='icoop' style='height: 35px;text-align:center;'>$row[2]</td>
								<td class='icoop' style='text-align:center;'>$row[3]</td>
								<td class='icoop' style='text-align:center;'>$row[1]</td>
								<td class='icoop' style='text-align:center;'>$row[6]</td>
                                <td class='icoop'>$nombre</td>
                                <td class='icoop' style='text-align:center;'>$row[12]</td>
                                <td class='icoop' style='text-align:center;'>$rowDireccion[0]</td>
                                <td class='icoop' style='text-align:center;'>".number_format($totalFactura)."</td>
								<td style='text-align:center;'><a href='serv-visualizarFactura.php?cliente=$row[1]&factura=$row[3]'>  $imgsem </a></td>
							</tr>";

							$aux = $iter;
							$iter = $iter2;
							$iter2 = $aux;
							$filas++;
						}

                        if($_POST['bandera'] == 0)
                        {
                            echo "
                            <table class='inicio'>
                                <tr>
                                    <td class='saludo1' style='text-align:center;width:100%;font-size:25px'>Utilice el filtro de busqueda</td>
                                </tr>
                            </table>";
                            $nuncilumnas = 0;
                        }

						if (@ $_POST['numtop']==0 && $_POST['bandera'] != 0)
						{
							echo "
							<table class='inicio'>
								<tr>
									<td class='saludo1' style='text-align:center;width:100%'><img src='imagenes\alert.png' style='width:25px'>No hay coincidencias en la b&uacute;squeda<img src='imagenes\alert.png' style='width:25px'></td>
								</tr>
							</table>";
						}
						echo"
					</table>
					<table class='inicio'>
						<tr>
							<td style='text-align:center;'>
								<a>$imagensback</a>&nbsp;
								<a>$imagenback</a>&nbsp;&nbsp;";
				if($nuncilumnas<=9){$numfin=$nuncilumnas;}
				else{$numfin=9;}
				for($xx = 1; $xx <= $numfin; $xx++)
				{
					if($numcontrol<=9){$numx=$xx;}
					else{$numx=$xx+($numcontrol-9);}
					if($numcontrol==$numx){echo"<a onClick='saltocol(\"$numx\")'; style='color:#24D915;cursor:pointer;'> $numx </a>";}
					else {echo"<a onClick='saltocol(\"$numx\")'; style='color:#000000;cursor:pointer;'> $numx </a>";}
				}
				echo"			&nbsp;&nbsp;<a>$imagenforward</a>
								&nbsp;<a>$imagensforward</a>
							</td>
						</tr>
					</table>";	
			?>
			<input type="hidden" name="ocules" id="ocules" value="<?php echo @ $_POST['ocules'];?>">
			<input type="hidden" name="actdes" id="actdes" value="<?php echo @ $_POST['actdes'];?>">
			<input type="hidden" name="numtop" id="numtop" value="<?php echo @ $_POST['numtop'];?>" />
		</form>
		<div id="bgventanamodal2">
			<div id="ventanamodal2">
				<IFRAME  src="" name="buster" marginWidth=0 marginHeight=0 frameBorder=0 id="ventana2" frameSpacing=0 style="left:500px; width:900px; height:500px; top:200;"></IFRAME>
			</div>
		</div>
	</body>
</html>