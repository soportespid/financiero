<?php 
	header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=utf8");
	require "comun.inc";
	require "funciones.inc";
	$linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");
	date_default_timezone_set("America/Bogota");
?>
<!DOCTYPE >
<html lang="es">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1">
		<meta name="viewport" content="user-scalable=no">
		<title>:: Spid - Gestion Humana</title>
		<link href="css/css2.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<link href="css/css4.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<script type="text/javascript" src="css/programas.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script>
			var anterior;
			function ponprefijo(idproy,codproy,nomproy){
				parent.document.form2.idproyecto.value = idproy;
				parent.document.form2.nomproyecto.value = codproy+" - "+nomproy;
				parent.document.form2.idprogramatico.value = '';
				parent.document.form2.nomprogramatico.value = '';
				parent.document.form2.idfuente.value = ''
				parent.document.form2.nomfuente.value = '';
				parent.despliegamodal2("hidden");
			}
		</script> 
		<?php titlepag();?>
	</head>
	<body>
		<form action="" method="post" enctype="multipart/form-data" name="form2">
			<?php 
				if($_POST['oculto']==""){
					$_POST['numpos'] = 0;
					$_POST['numres'] = 10;
					$_POST['nummul'] = 0;
					$_POST['vigencia'] = date('Y');
				}
			?>
			<table class="inicio ancho">
				<tr>
					<td class="titulos" colspan="5">:: Buscar Proyecto Inversion</td>
					<td class="cerrar" style="width:7%" onClick="parent.despliegamodal2('hidden');">Cerrar</td>
				</tr>
				<tr>
					<td class='tamano01' style="width:3cm;">:: C&oacute;digo:</td>
					<td><input type="text" name="documento" id="documento" value="" style="width:100%;height: 22px;"></td>
					<td class='tamano01' style="width:3cm;">:: Vigencia:</td>
					<td >
					<select name="vigencia" id="vigencia" style='text-transform:uppercase; width:70%; height:22px;' onChange="document.form2.submit();">
						<option value="">..Todos..</option>
						<?php
							$sql = "SELECT anio FROM admbloqueoanio WHERE bloqueado = 'N' ORDER BY anio DESC";
							$res = mysqli_query($linkbd,$sql);
							while($row = mysqli_fetch_row($res)){
								if($row[0] == $_POST['vigencia']){
									echo "<option value='$row[0]' SELECTED>$row[0]</option>";
								} else {
									echo "<option value='$row[0]'>$row[0]</option>";
								}
							}
						?>
					</select>
				</td>
					
					
					<td><em name="continuar" id="continuar" class="botonflecha" onClick="limbusquedas();">Buscar</em></td>
				</tr>
			</table>
			<input type="hidden" name="oculto" id="oculto" value="1">
			<input type="hidden" name="numres" id="numres" value="<?php echo @ $_POST['numres'];?>"/>
			<input type="hidden" name="numpos" id="numpos" value="<?php echo @ $_POST['numpos'];?>"/>
			<input type="hidden" name="nummul" id="nummul" value="<?php echo @ $_POST['nummul'];?>"/>
			<div class="subpantalla" style="height:84%; width:99.6%; overflow-x:hidden;">
				<?php
					

					if($_POST['documento']!="")
					{
						$crit1 = "WHERE concat_ws(' ',codigo, nombre) LIKE '%".$_POST['documento']."%'";
						if($_POST['vigencia']!=""){$crit2 = " AND vigencia = '".$_POST['vigencia']."'";}
						else {$crit2="";}
					}
					else
					{
						$crit1="";
						if($_POST['vigencia']!=""){$crit2 = "WHERE vigencia = '".$_POST['vigencia']."'";}
						else {$crit2="";}
					}
					$sqlr = "SELECT id FROM ccpproyectospresupuesto $crit1 $crit2";
					$resp = mysqli_query($linkbd,$sqlr);
					$_POST['numtop'] = mysqli_num_rows($resp);
					$nuncilumnas=ceil($_POST['numtop']/$_POST['numres']);
					$sqlr="SELECT id,codigo,nombre FROM ccpproyectospresupuesto $crit1 $crit2 ORDER BY id DESC LIMIT ".$_POST['numpos'].",".$_POST['numres'];
					$resp = mysqli_query($linkbd,$sqlr);
					$con=1;
					$numcontrol=$_POST['nummul']+1;
					if($nuncilumnas==$numcontrol)
					{
						$imagenforward="<img src='imagenes/forward02.png' style='width:17px'>";
						$imagensforward="<img src='imagenes/skip_forward02.png' style='width:16px' >";
					}
					else 
					{
						$imagenforward="<img src='imagenes/forward01.png' style='width:17px' title='Siguiente' onClick='numsiguiente()'>";
						$imagensforward="<img src='imagenes/skip_forward01.png' style='width:16px' title='Fin' onClick='saltocol(\"$nuncilumnas\")'>";
					}
					if($_POST['numpos']==0)
					{
						$imagenback="<img src='imagenes/back02.png' style='width:17px'>";
						$imagensback="<img src='imagenes/skip_back02.png' style='width:16px'>";
					}
					else
					{
						$imagenback="<img src='imagenes/back01.png' style='width:17px' title='Anterior' onClick='numanterior();'>";
						$imagensback="<img src='imagenes/skip_back01.png' style='width:16px' title='Inicio' onClick='saltocol(\"1\")'>";
					}
					echo "
					<table class='inicio' align='center' width='99%'>
					<tr>
						<td colspan='3' class='titulos'>.: Resultados Busqueda:</td>
						<td class='submenu' style='width:7%'>
							<select name='renumres' id='renumres' onChange='cambionum();' style='width:100%'>
								<option value='10'"; if ($_POST['renumres']=='10'){echo 'selected';} echo ">10</option>
								<option value='20'"; if ($_POST['renumres']=='20'){echo 'selected';} echo ">20</option>
								<option value='30'"; if ($_POST['renumres']=='30'){echo 'selected';} echo ">30</option>
								<option value='50'"; if ($_POST['renumres']=='50'){echo 'selected';} echo ">50</option>
								<option value='100'"; if ($_POST['renumres']=='100'){echo 'selected';} echo ">100</option>
							</select>
						</td>
					</tr>
					<tr><td colspan='4'>Centro Costo Encontrados: ".$_POST['numtop']."</td></tr>
					<tr>
						<td class='titulos2' width='5%'>Item</td>
						<td class='titulos2' width='5%'>id</td>
						<td class='titulos2' colspan='2'>Nombre</td>
					</tr>";	
					$iter='saludo1a';
					$iter2='saludo2';
					while ($row = mysqli_fetch_row($resp)){
						$ncc=$row[1];
						$con2=$con+ $_POST['numpos'];
						echo" 
						<tr class='$iter' onClick=\"javascript:ponprefijo('$row[0]','$row[1]','$row[2]')\" >
							<td>$con2</td>
							<td >$row[0]</td>
							<td colspan='2' style='text-transform:uppercase'>$row[1] - $row[2]</td>
						</tr>";
						$con+=1;
						$aux=$iter;
						$iter=$iter2;
						$iter2=$aux;
					}
					if ($_POST['numtop']==0)
						{
							echo "
							<table class='inicio'>
								<tr>
									<td class='saludo1' style='text-align:center;width:100%'><img src='imagenes\alert.png' style='width:25px'>No hay coincidencias en la b&uacute;squeda $tibusqueda<img src='imagenes\alert.png' style='width:25px'></td>
								</tr>
							</table>";
						}
					echo"
						</table>
						<table class='inicio'>
							<tr>
								<td style='text-align:center;'>
									<a href='#'>$imagensback</a>&nbsp;
									<a href='#'>$imagenback</a>&nbsp;&nbsp;";
					if($nuncilumnas<=9){$numfin=$nuncilumnas;}
					else{$numfin=9;}
					for($xx = 1; $xx <= $numfin; $xx++)
					{
						if($numcontrol<=9){$numx=$xx;}
						else{$numx=$xx+($numcontrol-9);}
						if($numcontrol==$numx){echo"<a href='#' onClick='saltocol(\"$numx\")'; style='color:#24D915'> $numx </a>";}
						else {echo"<a href='#' onClick='saltocol(\"$numx\")'; style='color:#000000'> $numx </a>";}
					}
					echo"			&nbsp;&nbsp;<a href='#'>$imagenforward</a>
									&nbsp;<a href='#'>$imagensforward</a>
								</td>
							</tr>
						</table>";
				?>
			</div>
			<input type="hidden" name="numtop" id="numtop" value="<?php echo @ $_POST['numtop'];?>" />
		</form>
	</body>
</html>
