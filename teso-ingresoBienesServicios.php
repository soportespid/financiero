<?php
	header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=utf8");
	require "comun.inc";
	require "funciones.inc";
	require "conversor.php";
	session_start();
	$linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");
	cargarcodigopag($_GET['codpag'],$_SESSION["nivel"]);
	date_default_timezone_set("America/Bogota");
?>
<!DOCTYPE >
<html lang="es">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1">
		<meta name="viewport" content="user-scalable=no">
		<title>:: IDEAL 10 - Tesorer&iacute;a</title>
		<link href="favicon.ico" rel="shortcut icon"/>
		<link href="css/css2z.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet"/>
		<link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet"/>
		<link href="css/css4.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet"/>
		<link href="css/tabs.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet"/>
		<link href="sweetalert2/dist/sweetalert2.min.css" rel="stylesheet">
		<script src="sweetalert2/dist/sweetalert2.min.js"></script>
		<script type="text/javascript" src="css/programas.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script type="text/javascript" src="css/calendario.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script>
			function formatonumero(valor){
				//return new Intl.NumberFormat("en-US", {style: "currency", currency: "USD"}).format(valor);
				return new Intl.NumberFormat("es-CO", {style: "currency", currency: "COP"}).format(valor);
			}
			function validar() {
				document.form2.submit();
			}
			function guardar(){
				if(document.form2.fecha.value != '' && document.form2.modorec.value != '') {
					if(document.form2.modorec.value == "banco"){
						if(document.form2.nbanco.value != ''){
							Swal.fire({
								icon: 'question',
								title: '¿Esta Seguro de guardar?',
								showDenyButton: true,
								confirmButtonText: 'Guardar',
								confirmButtonColor: '#01CC42',
								denyButtonText: 'Cancelar',
								denyButtonColor: '#FF121A',
							}).then(
								(result) => {
									if (result.isConfirmed){
										document.getElementById('oculto').value = "2";
										document.form2.submit();
									}else if (result.isDenied){
										Swal.fire({
											icon: 'info',
											title: 'No se guardo',
											confirmButtonText: 'Continuar',
											confirmButtonColor: '#FF121A',
											timer: 2500
										});
									}
								}
							)
						}else{
							Swal.fire({
								icon: 'error',
								title: 'Error!',
								text: 'Faltan cuenta bancaria',
								confirmButtonText: 'Continuar',
								confirmButtonColor: '#FF121A',
								timer: 2500
							});
						}

					}else{
						Swal.fire({
							icon: 'question',
							title: '¿Esta Seguro de guardar?',
							showDenyButton: true,
							confirmButtonText: 'Guardar',
							confirmButtonColor: '#01CC42',
							denyButtonText: 'Cancelar',
							denyButtonColor: '#FF121A',
						}).then(
							(result) => {
								if (result.isConfirmed){
									document.getElementById('oculto').value = "2";
									document.form2.submit();
								}else if (result.isDenied){
									Swal.fire({
										icon: 'info',
										title: 'No se guardo',
										confirmButtonText: 'Continuar',
										confirmButtonColor: '#FF121A',
										timer: 2500
									});
								}
							}
						)
					}
					
				}else {
					Swal.fire({
						icon: 'error',
						title: 'Error!',
						text: 'Faltan datos para completar el registro',
						confirmButtonText: 'Continuar',
						confirmButtonColor: '#FF121A',
						timer: 2500
					});
					document.form2.fecha.focus();
					document.form2.fecha.select();
				}
			}
			function despliegamodalm(_valor,_tip,mensa,pregunta,variable) {
				document.getElementById("bgventanamodalm").style.visibility=_valor;
				if(_valor=="hidden") {
					document.getElementById('ventanam').src = "";
				}else {
					switch(_tip) {
						case "1":	document.getElementById('ventanam').src="ventana-mensaje1.php?titulos="+mensa;break;
						case "2":	document.getElementById('ventanam').src="ventana-mensaje3.php?titulos="+mensa;break;
						case "3":	document.getElementById('ventanam').src="ventana-mensaje2.php?titulos="+mensa;break;
						case "4":	document.getElementById('ventanam').src="ventana-consulta1.php?titulos="+mensa+"&idresp="+pregunta;break;	
						case "5":	document.getElementById('ventanam').src="ventana-elimina1.php?titulos="+mensa+"&idresp="+pregunta+"&variable="+variable;break;	
					}
				}
			}
			function respuestaconsulta(pregunta, variable) {
				switch(pregunta) {
					
					case "2":
						document.form2.elimina.value=variable;
						vvend=document.getElementById('elimina');
						vvend.value=variable;
						document.form2.submit();
					break;
				}
			}
			function funcionmensaje() {
				document.location.href = "teso-verIngresosBienesServicios.php?idrecibo="+document.getElementById('idcomp').value+"&scrtop=0&totreg=1&altura=461&numpag=1&limreg=10&filtro=";
			}
			function pdf() {
				document.form2.action = "teso-pdfsinrecaja.php";
				document.form2.target = "_BLANK";
				document.form2.submit(); 
				document.form2.action = "";
				document.form2.target = "";
			}
			function despliegamodal2(_valor) {
				document.getElementById("bgventanamodal2").style.visibility = _valor;
				if(_valor=="hidden") {
					document.getElementById('ventana2').src = "";
				}else {
					document.getElementById('ventana2').src = "cuentasbancarias-ventana02.php?tipoc=D&obj01=banco&obj02=nbanco&obj03=&obj04=cb&obj05=ter";
				}
			}
			function agregardetalled(){
				if(document.form2.retencion.value != "" &&  document.form2.vporcentaje.value != ""){ 
					document.form2.agregadetdes.value = 1;
					document.form2.calculaRetencion.value = 1;
					document.form2.submit();
				}else {
					Swal.fire({
						icon: 'error',
						title: 'Error!',
						text: 'Falta informacion para poder Agregar',
						confirmButtonText: 'Continuar',
						confirmButtonColor: '#FF121A',
						timer: 2500
					});
				}
			}
			function eliminard(variable){
				Swal.fire({
					icon: 'question',
					title: '¿Esta Seguro de Eliminar?',
					showDenyButton: true,
					confirmButtonText: 'Eliminar',
					confirmButtonColor: '#01CC42',
					denyButtonText: 'Cancelar',
					denyButtonColor: '#FF121A',
				}).then(
					(result) => {
						if (result.isConfirmed){
							document.form2.eliminad.value = variable;
							document.form2.calculaRetencion.value = 1;
							vvend = document.getElementById('eliminad');
							vvend.value = variable;
							document.form2.submit();
						}else if (result.isDenied){
							Swal.fire({
								icon: 'info',
								title: 'No se elimino',
								confirmButtonText: 'Continuar',
								confirmButtonColor: '#FF121A',
								timer: 2500
							});
						}
					}
				)
			}
			function cambiovigencia(){
				let anno = document.getElementById('fc_1198971545').value.split("/");
				document.getElementById('vigencia').value = anno[2];
			}
			function cambioliquidacion(){
				document.form2.vcambios.value = '1';
				document.form2.submit();
			}
		</script>
	</head>
	<body>
		<IFRAME src="alertas.php" name="alertas" id="alertas" style="display:none"></IFRAME>
		<span id="todastablas2"></span>
		<table>
			<tr><script>barra_imagenes("teso");</script><?php cuadro_titulos();?></tr>	 
			<tr><?php menu_desplegable("teso");?></tr>
			<tr>
				<td colspan="3" class="cinta">
					<img src="imagenes/add.png" title="Nuevo" onClick="location.href='teso-ingresoBienesServicios.php'" class="mgbt"/>
					<img src="imagenes/guarda.png" title="Guardar" onClick="guardar()" class="mgbt"/>
					<img src="imagenes/busca.png" title="Buscar" class="mgbt" onClick="location.href='teso-buscarIngresosBienesServicios.php'"/>
					<img src="imagenes/agenda1.png" title="Agenda" onClick="mypop=window.open('plan-agenda.php','','');mypop.focus()" class="mgbt"/>
					<img src="imagenes/nv.png" title="Nueva ventana" onClick="mypop=window.open('teso-principal.php','',''); mypop.focus();" class="mgbt"/>
					<img src="imagenes/duplicar_pantalla.png" title="Duplicar pesta&ntilde;a" onClick="mypop=window.open('<?php echo $url2; ?>','','');mypop.focus();" class="tooltip bottom mgbt"/>
					<img src="imagenes/print.png" title="inprimir" class="mgbt" <?php if($_POST['oculto']==2) { ?>onClick="pdf()" <?php } ?>/>
				</td>
			</tr>		  
		</table>
		<div id="bgventanamodalm" class="bgventanamodalm">
			<div id="ventanamodalm" class="ventanamodalm">
				<IFRAME src="" name="ventanam" marginWidth=0 marginHeight=0 frameBorder=0 id="ventanam" frameSpacing=0 style=" width:700px; height:130px; top:200; overflow:hidden;"> 
				</IFRAME>
			</div>
		</div>
		<form name="form2" method="post" action=""> 
			<?php
				if(!$_POST['oculto']){ 
					$check1 = "checked";
					$_POST['fecha'] = $fec = date("d/m/Y");
					$_POST['vigencia'] = date('Y');
					$_POST['vcambios'] = '0';
					$sqlr = "SELECT cuentacaja FROM tesoparametros";
					$res = mysqli_query($linkbd,$sqlr);
					while ($row = mysqli_fetch_row($res)){
						$_POST['cuentacaja'] = $row[0];
					}
					$sqlr = "SELECT valor_inicial, valor_final, tipo FROM dominios WHERE nombre_dominio = 'COBRO_RECIBOS' AND descripcion_valor = '".$_POST['vigencia']."' AND tipo='S'";
					$res = mysqli_query($linkbd,$sqlr);
					while ($row = mysqli_fetch_row($res)){
						$_POST['cobrorecibo'] = $row[0];
						$_POST['vcobrorecibo'] = $row[1];
						$_POST['tcobrorecibo'] = $row[2];	 
					}
					$_POST['idcomp'] = selconsecutivo('teso_ingreso_bienes_servicios','id_recibos'); 		  			 
					$_POST['valor'] = 0;		 
				}
				switch($_POST['tabgroup1']){
					case 1:	$check1 = 'checked';break;
					case 2:	$check2 = 'checked';break;
					case 3:	$check3 = 'checked';
				}
			?>
			<input type="hidden" name="vcambios" id="vcambios" value="<?php echo $_POST['vcambios']?>"/>
			<input type="hidden" name="encontro" value="<?php echo $_POST['encontro']?>"/>
			<input type="hidden" name="cobrorecibo" value="<?php echo $_POST['cobrorecibo']?>"/>
			<input type="hidden" name="vcobrorecibo" value="<?php echo $_POST['vcobrorecibo']?>"/>
			<input type="hidden" name="tcobrorecibo" value="<?php echo $_POST['tcobrorecibo']?>"/> 
			<input type="hidden" name="codcatastral" value="<?php echo $_POST['codcatastral']?>"/>
			<?php 
				if($_POST['vcambios'] == '1'){
					$sqlr="SELECT concepto, fecha, vigencia, tercero, valortotal FROM tesobienesservicios WHERE id_recaudo = '".$_POST['idrecaudo']."'";
					$resp = mysqli_query($linkbd,$sqlr);
					$row = mysqli_fetch_row($resp);
					$_POST['concepto'] = $row[0];
					$_POST['fecha'] = date('d/m/Y',strtotime($row[1]));
					$_POST['vigencia'] = $row[2];
					$_POST['valorecaudo'] = number_format($row[4],2,".",",");
					$_POST['valorecaudo1'] = $row[4];
					$_POST['tercero'] = $row[3];
					echo "<script>document.form2.vcambios.value = '0';</script>";
					$_POST['ntercero'] = buscatercero($row[3]);
					unset ($_POST['dcoding'], $_POST['dncoding'], $_POST['dncc'], $_POST['dfuente'], $_POST['dvalores'], $_POST['ddescuentos'], $_POST['dporcentajes'], $_POST['ddesvalores'], $_POST['dndescuentos'], $_POST['ddestipo'], $_POST['dvalorestt'], $_POST['dvaliva19']);
					$sqld = "SELECT ingreso, valor, cc, fuente, iva FROM tesobienesservicios_det WHERE id_recaudo = '".$_POST['idrecaudo']."'";
					$resd = mysqli_query($linkbd,$sqld);
					while ($rowd = mysqli_fetch_row($resd)){
						$_POST['dcoding'][] = $rowd[0];
						$_POST['dncc'][] = $rowd[2];
						$_POST['dfuente'][] = $rowd[3];
						$_POST['dvalores'][] = $rowd[1];
						$_POST['dvalorestt'][] = $rowd[1] + $rowd[4];
						$_POST['dvaliva'][] = $rowd[4];
						$sqling = "SELECT * FROM tesoingresos_ventas WHERE codigo = '$rowd[0]' AND estado='S' ORDER BY codigo";
						$rowing = mysqli_fetch_row(mysqli_query($linkbd,$sqling));
						$_POST['dncoding'][] = $rowing[1];
					}
					$_POST['retegeneral'] = $_POST['reteiva'] = 0;
					$sqlrt = "SELECT id_retencion, porcentaje, valor FROM tesobienesservicios_retenciones WHERE id_orden = '".$_POST['idrecaudo']."'";
					$resrt = mysqli_query($linkbd,$sqlrt);
					while ($rowrt = mysqli_fetch_row($resrt)){
						$codrete = '';
						if(strlen($rowrt[0])>1){
							$codrete = $rowrt[0];
						}else{
							$codrete = '0'.$rowrt[0];
						}
						$_POST['ddescuentos'][] = $codrete;
						$_POST['dporcentajes'][] = $rowrt[1];
						$_POST['ddesvalores'][] = $rowrt[2];
						$_POST['ddestipo'][] = 'L';
						$sqlnr = "SELECT nombre, iva FROM tesoretenciones WHERE codigo = '$codrete'";
						$resnr = mysqli_query($linkbd, $sqlnr);
						$rownr = mysqli_fetch_row($resnr);
						$_POST['dndescuentos'][] = $rownr[0];
						$_POST['dndesiva'][] = $rownr[1];
						if($rownr[1] == '1'){
							$_POST['reteiva'] = $_POST['reteiva'] + $rowrt[2];
						}else{
							$_POST['retegeneral'] = $_POST['retegeneral'] + $rowrt[2];
						}
					}
				}
			?>
			<input type="hidden" name="retegeneral" id="retegeneral" value="<?php echo $_POST['retegeneral']?>">
			<input type="hidden" name="reteiva" id="reteiva" value="<?php echo $_POST['reteiva']?>">
			<table class="inicio ancho">
				<tr>
					<td class="titulos" colspan="2">Ingresos Bienes y Servicios</td>
					<td class="cerrar" style="width:7%" onClick="location.href='teso-principal.php'">Cerrar</td>
				</tr>
				<tr>
					<td style="width:80%;">
						<table>
							<tr>
								<td style="width:2.5cm" class="saludo1" >No Recibo:</td>
								<td style="width:15%">
									<input type="hidden" name="cuentacaja" value="<?php echo $_POST['cuentacaja']?>"/>
									<input name="idcomp" id="idcomp" style="width:100%;" value="<?php echo $_POST['idcomp']?>" onKeyUp="return tabular(event,this) " readonly/>
								</td>
								<td style="width:2.5cm" class="saludo1">Fecha:</td>
								<td style="width:15%"><input name="fecha" type="text" value="<?php echo $_POST['fecha']?>" maxlength="10" style="width:100%" onKeyPress="javascript:return solonumeros(event)" onKeyUp="return tabular(event,this)" onchange="" id="fc_1198971545" onKeyDown="mascara(this,'/',patron,true)" title="DD/MM/YYYY" onDblClick="displayCalendarFor('fc_1198971545');" class="colordobleclik" autocomplete="off" onchange="cambiovigencia()"/></td>
								<td style="width:2.5cm" class="saludo1">Vigencia:</td>
								<td style="width:10%">
									<input type="text" id="vigencia" name="vigencia" style="width:30%" onKeyPress="javascript:return solonumeros(event)" onKeyUp="return tabular(event,this)"  value="<?php echo $_POST['vigencia']?>" onClick="document.getElementById('tipocta').focus();document.getElementById('tipocta').select();" readonly>     
								</td> 
							</tr>
							<tr>
								<td class="saludo1"> Recaudo:</td>
								<td><input type="text" id="tiporec" name="tiporec" value="Bienes y Servicios" style="width:100%" onKeyUp="return tabular(event,this)" onChange="validar()"></td>
								<td class="saludo1">Recaudado en:</td>
								
								<td>
									<select name="modorec" id="modorec" onKeyUp="return tabular(event,this)" style="width:100%;" onChange="validar()" >
										<option value="" >Seleccione...</option>
										<option value="caja" <?php if($_POST['modorec']=='caja') echo "SELECTED"; ?>>Caja</option>
										<option value="banco" <?php if($_POST['modorec']=='banco') echo "SELECTED"; ?>>Banco</option>
									</select>
								</td>
								<td class="saludo1">No Liquid:</td>
								<td style="width:22%;">
									<select name="idrecaudo" id="idrecaudo" onKeyUp="return tabular(event,this)" style="width:100%;" onChange="cambioliquidacion()">
										<option value="" >Seleccione...</option>
										<?php
											$sqlr = "SELECT id_recaudo, concepto FROM tesobienesservicios WHERE estado = 'S' AND id_recaudo NOT IN (SELECT id_recaudo FROM teso_ingreso_bienes_servicios) ORDER BY id_recaudo ASC";
											$resp = mysqli_query($linkbd,$sqlr);
											while ($row = mysqli_fetch_row($resp)){
												if($row[0]==$_POST['idrecaudo']){
													echo "<option value='$row[0]' SELECTED>$row[0] - $row[1]</option>";
												}else{
													echo "<option value='$row[0]'>$row[0] - $row[1]</option>";
												}
											}
										?>
									</select>
								</td>
							</tr>
							<?php
								if ($_POST['modorec']=='banco'){
									echo "<tr>
										<td class='saludo1'>Cuenta :</td>
										<td>
											<input type='text' name='cb' id='cb' value='".$_POST['cb']."' style='width:80%;'/>&nbsp;
											<a onClick=\"despliegamodal2('visible');\"  style='cursor:pointer;' title='Listado Cuentas Bancarias'>	
												<img src='imagenes/find02.png' style='width:20px;'/>
											</a>
										</td>
										<td colspan='4'>
												<input type='text' id='nbanco' name='nbanco' style='width:100%;' value='".$_POST['nbanco']."'  readonly>
										</td>
										<input type='hidden' name='banco' id='banco' value='".$_POST['banco']."'/>
										<input type='hidden' id='ter' name='ter' value='".$_POST['ter']."'/></td>
									</tr>";
								}
							?> 
							<tr>
								<td class="saludo1">Concepto:</td>
								<td colspan="5"><input name="concepto" style="width:100%" type="text" value="<?php echo $_POST['concepto'] ?>" onKeyUp="return tabular(event,this)"></td>
							</tr>
							<tr>
								<td class="saludo1">Valor:</td>
								<td><input type="text" id="valorecaudo" name="valorecaudo" value="<?php echo $_POST['valorecaudo']?>" style="width:100%" onKeyUp="return tabular(event,this)" readonly ></td>
								<td  class="saludo1">Documento: </td>
								<td ><input name="tercero" type="text" value="<?php echo $_POST['tercero']?>" style="width:100%" onKeyUp="return tabular(event,this)" readonly></td>
								<td class="saludo1">Contribuyente:</td>
								<td><input type="text" id="ntercero" name="ntercero" value="<?php echo $_POST['ntercero']?>" style="width:100%" onKeyUp="return tabular(event,this) "  readonly></td>
							</tr>
							<tr>
								<td class="saludo1">Retenciones:</td>
								<td><input type="text" id="total_retencion" name="total_retencion" value="<?php echo $_POST['total_retencion']?>" style="width:100%" onKeyUp="return tabular(event,this)" readonly ></td>
								<td  class="saludo1">IVA: </td>
								<td ><input type="text" id="total_iva" name="total_iva" value="<?php echo $_POST['total_iva']?>" style="width:100%" onKeyUp="return tabular(event,this)" readonly></td>
								<td class="saludo1">Total:</td>
								<td><input type="text" id="total_ingreso" name="total_ingreso" value="<?php echo $_POST['total_ingreso']?>" style="width:100%" onKeyUp="return tabular(event,this) "  readonly></td>
							</tr>
						</table>
						<input type="hidden" id="valorecaudo1" name="valorecaudo1" value="<?php echo $_POST['valorecaudo1']?>">
						<input type="hidden" id="total_retencion1" name="total_retencion1" value="<?php echo $_POST['total_retencion1']?>">
						<input type="hidden" id="total_iva1" name="total_iva1" value="<?php echo $_POST['total_iva1']?>">
						<input type="hidden" id="total_ingreso1" name="total_ingreso1" value="<?php echo $_POST['total_ingreso1']?>">
						<input type="hidden" id="ct" name="ct" value="<?php echo $_POST['ct']?>" >
						<input type="hidden" name="oculto" id="oculto" value="1">
						<input type="hidden" value="<?php echo $_POST['trec']?>"  name="trec">
						<input type="hidden" value="0" name="agregadet">
					</td>
					<td colspan="2" style="width:20%; background:url(imagenes/siglasideal.png); background-repeat:no-repeat; background-position:left; background-size: 70% 100%;" >
					</td>  
				</tr>
			</table>
			<div class="tabs" style="height:38%; width:99.7%;" id="myDiv">
				<div class="tab" >
					<input type="radio" id="tab-1" name="tabgroup1" value="1" <?php echo $check1;?> >
					<label id="clabel" for="tab-1">Orden de pago</label>
					<div class="content" style="overflow:hidden;height:93%;">
						<table class='tablamv3' style="height:99%;">
							<thead>
								<tr>
									<th class="titulos" style="text-align:left;">Detalle Recaudo</th>
								</tr>
								<tr >
									<th class="titulosnew02" style="width: 5%;">C&oacute;digo</th>
									<th class="titulosnew02">Ingreso</th>
									<th class="titulosnew02" style="width: 10%;">Centro Costo</th>
									<th class="titulosnew02" style="width: 15%;">Fuente</th>
									<th class="titulosnew02" style="width: 15%;">Valor</th>
								</tr>
							</thead>
							<tbody>
								<?php
									$_POST['totalc'] = 0;
									$iter='saludo1a';
									$iter2='saludo2';
									for ($x=0;$x<count($_POST['dcoding']);$x++){	
										echo "
										<input type='hidden' name='dcoding[]' value='".$_POST['dcoding'][$x]."'>
										<input type='hidden' name='dncoding[]' value='".$_POST['dncoding'][$x]."'>
										<input type='hidden' name='dncc[]' value='".$_POST['dncc'][$x]."'>
										<input type='hidden' name='dfuente[]' value='".$_POST['dfuente'][$x]."'>
										<input type='hidden' name='dvalores[]' value='".$_POST['dvalores'][$x]."'>
										<input type='hidden' name='dvalorestt[]' value='".$_POST['dvalorestt'][$x]."'>
										<input type='hidden' name='dvaliva[]' value='".$_POST['dvaliva'][$x]."'>
										<tr class='$iter'>
											<td style='width: 5%;text-align:center;''>".$_POST['dcoding'][$x]."</td>
											<td>".$_POST['dncoding'][$x]."</td>
											<td style='width: 10%;text-align:center;''>".$_POST['dncc'][$x]."</td>
											<td style='width: 15%;text-align:center;''>".$_POST['dfuente'][$x]."</td>
											<td style='width: 15%;text-align:right;'>".number_format($_POST['dvalorestt'][$x],2,".",",")."</td>
										</tr>";
										$_POST['totalc'] = $_POST['totalc'] + $_POST['dvalorestt'][$x];
										$_POST['totalcf'] = number_format($_POST['totalc'],2);
									}
									$resultado = convertir($_POST['totalc'],"PESOS");
									$_POST['letras'] = $resultado." PESOS M/CTE";
									echo "
									<input type='hidden' name='letras' value='".$_POST['letras']."'>
									<input type='hidden' name='totalcf' value='$_POST[totalcf]'>
									<input type='hidden' name='totalc' value='$_POST[totalc]'>
									<tr>
										<td></td>
										<td style='width: 15%;text-align:right;'>Total</td>
										<td style='width: 15%;text-align:right;'>".$_POST['totalcf']."</td>
									</tr>
									<tr class='$iter'>
										<td class='titulosnew02' style='width:10%'>Son:</td>
										<td class='titulosnew04' colspan='4' >".$_POST['letras']."</td>
									</tr>";
								?>
							</tbody>
						</table>
					</div>
				</div>
				<div class="tab">
					<input type="radio" id="tab-2" name="tabgroup1" value="2" <?php echo $check2;?> >
					<label id="clabel" for="tab-2">Retenciones</label>
					<div class="content" style="overflow:hidden;height:93%;">
						<table class='tablamv3 ancho' style="height:99%;">
							<thead>
								<tr><th class="titulos" style="text-align:left;">Retenciones</th></tr>
								<tr>
									<th class="saludo1" style="width:12%;">Retencion y Descuento:</th>
									<th style="width:15%;">
										<select name="retencion" onChange="validar()" onKeyUp="return tabular(event,this)">
											<option value="">Seleccione ...</option>
											<?php
												$sqlr="SELECT * FROM tesoretenciones WHERE estado='S'";
												$res = mysqli_query($linkbd, $sqlr);
												while ($row = mysqli_fetch_row($res)){
													if("$row[0]" == $_POST['retencion']){
														echo "<option value='$row[0]' SELECTED>$row[1] - $row[2]</option>";
														$_POST['nretencion'] = $row[1]." - ".$row[2];
														if($_POST['porcentaje'] == ''){
															$_POST['porcentaje'] = $row[5];
														}
													}else {
														echo "<option value='$row[0]'>$row[1] - $row[2]</option>";
													} 	 
												}
											?>
										</select>
										<input type="hidden" value="<?php echo $_POST['nretencion']?>" name="nretencion">
									</th>
									<input type="hidden" id="contador" name="contador"  value="<?php echo $_POST['contador']; ?>" >
									<input type="hidden" id="oculto12" name="oculto12"  value="<?php echo $_POST['oculto12']; ?>">
									<input type="hidden" name="estado"  value="<?php echo $_POST['estado']; ?>" >
									<th style="width:8%;" class="saludo1">Porcentaje:</th>
									<th style="width:6%;"><input type="text" id="porcentaje" name="porcentaje" value="<?php echo $_POST['porcentaje']?>"></th>
									<th class="saludo1" style="width:4%;">Valor:</th>
									<th style="width:10%;">
										<input type="text" id="vporcentaje" name="vporcentaje" value="<?php echo $_POST['vporcentaje']?>" >
										<input type="hidden" name="calculaRetencion" id="calculaRetencion" value="<?php echo $_POST['calculaRetencion'] ?>">
									</th>
									<th style="width:10%;"><em class="botonflechaverde" onClick="agregardetalled();">Agregar</em></th>
									<th></th>
								</tr>
								<input type="hidden" name="agregadetdes" value="0" >
								<?php
									if ($_POST['eliminad'] != ''){ 
										$posi = $_POST['eliminad'];
										unset($_POST['ddescuentos'][$posi]);
										unset($_POST['dndescuentos'][$posi]);
										unset($_POST['dporcentajes'][$posi]);
										unset($_POST['ddesvalores'][$posi]);
										unset($_POST['ddestipo'][$posi]);
										unset($_POST['dndesiva'][$posi]);
										$_POST['ddescuentos'] = array_values($_POST['ddescuentos']); 
										$_POST['dndescuentos'] = array_values($_POST['dndescuentos']); 
										$_POST['dporcentajes'] = array_values($_POST['dporcentajes']); 
										$_POST['ddesvalores'] = array_values($_POST['ddesvalores']);
										$_POST['ddestipo'] = array_values($_POST['ddestipo']); 	
										$_POST['dndesiva'] = array_values($_POST['dndesiva']);
									}	 
									if ($_POST['agregadetdes']=='1'){
										$codrete = '';
										if(strlen($_POST['retencion'])>1){
											$codrete = $_POST['retencion'];
										}else{
											$codrete = '0'.$_POST['retencion'];
										}
										$_POST['ddescuentos'][] = $codrete;
										$_POST['dndescuentos'][] = $_POST['nretencion'];
										$_POST['dporcentajes'][] = $_POST['porcentaje'];
										$_POST['ddesvalores'][] = $_POST['vporcentaje'];
										$_POST['ddestipo'][] = 'N';
										$_POST['agregadetdes'] = '0';
										$_POST['dndesiva'][] = '';
										
										echo"
										<script>
											document.form2.porcentaje.value='';
											document.form2.vporcentaje.value=0;	
											document.form2.retencion.value='';	
										</script>";
									}
								?>
								<tr>
									<th class="titulosnew02">Descuento</th>
									<th class="titulosnew02" style="width:10%;" >%</th>
									<th class="titulosnew02" style="width:20%;">Valor</th>
									<th class="titulosnew02" style="width:5%;"><img src="imagenes/del.png" ></td>
								</tr>
							</thead>
							<tbody style="height:60%;">
								<input type='hidden' name='eliminad' id='eliminad'>
								<?php
									$totaldes = 0;
									$iter='saludo1a';
									$iter2='saludo2';
									$_POST['tdescuentosn'] = 0;
									if(isset($_POST['ddescuentos'])){
										for ($x = 0; $x<count($_POST['ddescuentos']); $x++){
											echo "
											<input type='hidden' name='dndescuentos[]' value='".$_POST['dndescuentos'][$x]."'>
											<input type='hidden' name='ddescuentos[]' value='".$_POST['ddescuentos'][$x]."' >
											<input type='hidden' name='dporcentajes[]' value='".$_POST['dporcentajes'][$x]."'>
											<input type='hidden' name='ddesvalores[]' value='".$_POST['ddesvalores'][$x]."'>
											<input type='hidden' name='ddestipo[]' value='".$_POST['ddestipo'][$x]."'>
											<input type='hidden' name='dndesiva[]' value='".$_POST['dndesiva'][$x]."'>
											<tr class='$iter'>
												<td>".$_POST['dndescuentos'][$x]."</td>
												<td style='text-align:center;width:10%;'>".$_POST['dporcentajes'][$x]."</td>
												<td style='text-align:right;width:20%;'>".number_format($_POST['ddesvalores'][$x],2,".",",")."</td>";
											if($_POST['ddestipo'][$x]== 'N'){
												echo"
													<td style='width:5%;text-align:center;'><a href='#' onclick='eliminard($x)'><img src='imagenes/del.png'></a></td>
												</tr>";
											}else{
												echo"
													<td style='width:5%;'></td>
												</tr>";
												$_POST['tdescuentosn'] += $_POST['ddesvalores'][$x];
											}
											$totaldes += $_POST['ddesvalores'][$x];
											$aux = $iter;
											$iter = $iter2;
											$iter2 = $aux;
										}
									}
									if(isset($totaldes)){
										
										echo"
										<script>
											document.form2.total_retencion.value = '$".number_format($totaldes,2,".",",")."';
											document.form2.total_retencion1.value ='$totaldes';
											document.form2.total_ingreso1.value = Number(document.form2.valorecaudo1.value) - Number(document.form2.total_retencion1.value) + Number(document.form2.total_iva1.value);
											document.form2.total_ingreso.value = formatonumero(document.form2.total_ingreso1.value);
										</script>
										<tr>
											<td colspan = '2' style='font-size:18px; text-align: center; color:gray !important; ' class='saludo1'>TOTAL:</td>
											<td class='saludo1'>
												$totaldes
											</td>
											<td></td>
										</tr>
										";	
									}
								?>
							</tbody>
						</table>
						
					</div>
				</div>
				<input type="hidden" name="tdescuentosn" id="tdescuentosn" value="<?php echo $_POST['tdescuentosn']?>">
				<div class="tab">
					<input type="radio" id="tab-3" name="tabgroup1" value="3" <?php echo $check3;?> >
					<label id="clabel" for="tab-3">IVA</label>
					<div class="content" style="overflow:hidden;">
						<table class='tablamv'>
							<thead>
								<tr><th class="titulos" style="text-align:left;">IVA</th></tr>
								<tr>
									<th class="titulosnew02" >CONCEPTO</th>
									<th class="titulosnew02" style="width:20%" >VALOR</th>
								</tr>
							</thead>
							<tbody style="resize: vertical;">
								<?php
									$iter='saludo1a';
									$iter2='saludo2';
									$granvalortotal = 0;
									for ($x=0;$x<count($_POST['dcoding']);$x++){
										//IVA
										$sqlr = "SELECT concepto_iva FROM tesoingresos_ventas WHERE codigo = '".$_POST['dcoding'][$x]."'";
										$resp = mysqli_query($linkbd,$sqlr);
										$row = mysqli_fetch_row($resp);
										$codiva = $row[0];
										if($codiva != ''){
											$sqliva = "SELECT porcentaje, nombre FROM conceptoscontables WHERE modulo = '4' AND tipo = 'IV' AND codigo = '$codiva'";
											$resiva = mysqli_query($linkbd,$sqliva);
											$rowiva = mysqli_fetch_row($resiva);
											$porceniva = $rowiva[0];
											$valtotaliva = $_POST['dvaliva'][$x];
											$granvalortotal += $valtotaliva;
											if($valtotaliva > 0){
												echo "
												<input type='hidden' name='dcodiva[]' value='$codiva'>
												<input type='hidden' name='ddesiva[]' value='$rowiva[1]' >
												<input type='hidden' name='dporiva[]' value='$porceniva'>
												
												<tr class='$iter'>
													<td>$rowiva[1]</td>
													<td style='text-align:right;width:20%'>$".number_format($valtotaliva,2,".",",")."</td>
												</tr>";
												$aux = $iter;
												$iter = $iter2;
												$iter2 = $aux;
											}
										}
									}
									echo "
									<script>
										document.form2.total_iva1.value ='$granvalortotal';
										document.form2.total_iva.value ='$".number_format($granvalortotal,2,".",",")."';
										document.form2.total_ingreso1.value = Number(document.form2.valorecaudo1.value) - Number(document.form2.total_retencion1.value) + Number(document.form2.total_iva1.value);
										document.form2.total_ingreso.value = formatonumero(document.form2.total_ingreso1.value);
									</script>
									<tr class='$iter'>
										<td style='text-align:right;font-weight:bold;'>TOTAL:</td>
										<td style='text-align:right;font-weight:bold;width:20%'>$".number_format($granvalortotal,2,".",",")."</td>
									</tr>";
								?>
							</tbody>
						</table>
					</div>
				</div>
				<?php
					if($_POST['oculto']=='2'){
						preg_match("/([0-9]{2})\/([0-9]{2})\/([0-9]{4})/", $_POST['fecha'],$fecha);
						$fechaf = $fecha[3]."-".$fecha[2]."-".$fecha[1];
						$vigencia = $fecha[3];
						$numrecaudo = $_POST['idrecaudo'];
						if($_POST['modorec'] == 'banco'){
							$sqlban = "SELECT cuenta FROM tesobancosctas WHERE ncuentaban = '".$_POST['cb']."'";
							$resban = mysqli_query($linkbd,$sqlban);
							$rowban = mysqli_fetch_row($resban);
							$cuentabanco = $rowban[0];
							$cuentacaja = '';
						}else{
							$cuentabanco = '';
							$cuentacaja = $_POST['cuentacaja'];
						}
						$numtercero = $_POST['tercero'];
						$nomconcepto = $_POST['concepto'];
						$valordet = $_POST['valorecaudo1'];
						$valrete = $_POST['total_retencion1'];
						$valiva = $_POST['total_iva1'];
						$neto_pagar = $_POST['total_ingreso1'];
						if($cuentabanco != ''){
							$cuentadeb = $cuentabanco;
						}else{
							$cuentadeb = $cuentacaja;
						}
						$codingreso = selconsecutivo('teso_ingreso_bienes_servicios','id_recibos');

						$sql = "INSERT INTO teso_ingreso_bienes_servicios (id_recibos, fecha, vigencia, id_recaudo, cuentacaja, cuentabanco,concepto, tercero, valor, retencion, iva, neto_pagar, estado, tipo_mov) VALUES ('$codingreso', '$fechaf', '$vigencia','$numrecaudo', '$cuentacaja', '$cuentabanco', '$nomconcepto', '$numtercero', '$valordet', '$valrete', '$valiva', '$neto_pagar', 'S', '201')";
						mysqli_query($linkbd, $sql);

						$valorconta = 0;
						$sqlr="INSERT INTO comprobante_cab (numerotipo, tipo_comp, fecha, concepto, total, total_debito, total_credito, diferencia, estado) VALUES ($codingreso, 42, '$fechaf', '".strtoupper($nomconcepto)."', 0, 0, 0, 0, '1')";
						mysqli_query($linkbd,$sqlr);
						
						for ($x=0;$x<count($_POST['dcoding']);$x++){
							$coddetalle = selconsecutivo('teso_ingreso_bienes_servicios_det','id_det');
							$sql = "INSERT INTO teso_ingreso_bienes_servicios_det (id_det, id_recibos, codigo, nombre, tipo, porcentaje, valor, cc, fuente, estado) VALUES ('$coddetalle', '$codingreso', '".$_POST['dcoding'][$x]."', '".$_POST['dncoding'][$x]."', 'D', '0', '".$_POST['dvalorestt'][$x]."', '".$_POST['dncc'][$x]."', '".$_POST['dfuente'][$x]."', 'S')";
							mysqli_query($linkbd, $sql);

							$valdescuento = $_POST['total_retencion1'] - $_POST['tdescuentosn'];
							$valorconta = $_POST['dvalores'][$x] - abs($valdescuento) - $_POST['retegeneral'];
							

							$sqlr="INSERT INTO comprobante_det (id_comp,cuenta, tercero, centrocosto, detalle, cheque, valdebito, valcredito, estado, vigencia) VALUES ('42 $codingreso', '$cuentadeb', '$numtercero', '".$_POST['dncc'][$x]."', 'Causación ".strtoupper($_POST['dncoding'][$x])."', '', '$valorconta', '0', '1', '$vigencia')";
							mysqli_query($linkbd,$sqlr);

							$sqlDetalle = "SELECT cuentapres, concepto FROM tesoingresos_ventas_det WHERE codigo = '".$_POST['dcoding'][$x]."'";
							$resDetalle = mysqli_query($linkbd, $sqlDetalle);
							$rowDetalle = mysqli_fetch_row($resDetalle);
							$vrconcepto = $rowDetalle[1];

							$sql1 = "SELECT cuenta FROM conceptoscontables_det WHERE modulo = '4' AND tipo = 'C' AND fechainicial < '$fechaf' AND cc='".$_POST['dncc'][$x]."' AND codigo = '$vrconcepto' AND cuenta != '' AND debito = 'S' ORDER BY fechainicial asc";
							$res1 = mysqli_query($linkbd,$sql1);
							$row1 = mysqli_fetch_row($res1);
							$cuentaiva = $row1[0];
									
							$sqlr="INSERT INTO comprobante_det (id_comp,cuenta, tercero, centrocosto, detalle, cheque, valdebito, valcredito, estado, vigencia) VALUES ('42 $codingreso', '$cuentaiva', '$numtercero', '".$_POST['dncc'][$x]."', 'Causación ".strtoupper($_POST['dncoding'][$x])."', '', '0', '$valorconta', '1', '$vigencia')";
							mysqli_query($linkbd,$sqlr);
						}

						for ($x=0;$x<count($_POST['dcodiva']);$x++){
							$coddetalle = selconsecutivo('teso_ingreso_bienes_servicios_det','id_det');
							$sql = "INSERT INTO teso_ingreso_bienes_servicios_det (id_det, id_recibos, codigo, nombre, tipo, porcentaje, valor, cc, fuente, estado) VALUES ('$coddetalle', '$codingreso', '".$_POST['dcodiva'][$x]."', '".$_POST['ddesiva'][$x]."', 'I', '".$_POST['dporiva'][$x]."', '".$_POST['dvaliva'][$x]."', '".$_POST['dncc'][0]."', '".$_POST['dfuente'][0]."', 'S')";
							mysqli_query($linkbd, $sql);
							$valorcesiva = $_POST['dvaliva'][$x] - $_POST['reteiva'];
							$sqlr="INSERT INTO comprobante_det (id_comp,cuenta, tercero, centrocosto, detalle, cheque, valdebito, valcredito, estado, vigencia) VALUES ('42 $codingreso', '$cuentadeb', '$numtercero', '".$_POST['dncc'][0]."', 'Iva ".strtoupper($_POST['ddesiva'][$x])."', '', '$valorcesiva', '0', '1', '$vigencia')";
							mysqli_query($linkbd,$sqlr);

							$sqlr="INSERT INTO comprobante_det (id_comp,cuenta, tercero, centrocosto, detalle, cheque, valdebito, valcredito, estado, vigencia) VALUES ('42 $codingreso', '$cuentaiva', '$numtercero', '".$_POST['dncc'][0]."', 'IV ".strtoupper($_POST['dncoding'][$x])."', '', '0', '$valorcesiva', '1', '$vigencia')";
							mysqli_query($linkbd,$sqlr);
							
						}
						
						for ($x = 0; $x<count($_POST['ddescuentos']); $x++){
							if($_POST['ddestipo'][$x]== 'N'){
								$coddetalle = selconsecutivo('teso_ingreso_bienes_servicios_det','id_det');
								$sql = "INSERT INTO teso_ingreso_bienes_servicios_det (id_det, id_recibos, codigo, nombre, tipo, porcentaje, valor, cc, fuente, estado) VALUES ('$coddetalle', '$codingreso', '".$_POST['ddescuentos'][$x]."', '".$_POST['dndescuentos'][$x]."', 'RN', '".$_POST['dporcentajes'][$x]."', '".$_POST['ddesvalores'][$x]."', '".$_POST['dncc'][0]."', '".$_POST['dfuente'][0]."', 'S')";
								mysqli_query($linkbd, $sql);
								
								$sql1 = "SELECT cuenta FROM conceptoscontables_det WHERE modulo = '4' AND tipo = 'RI' AND fechainicial < '$fechaf' AND cc='".$_POST['dncc'][0]."' AND codigo = '".$_POST['ddescuentos'][$x]."' AND cuenta != '' AND debito = 'S' ORDER BY fechainicial asc";
								$res1 = mysqli_query($linkbd,$sql1);
								$row1 = mysqli_fetch_row($res1);
								$cuentaretencion = $row1[0];

								$sqlr="INSERT INTO comprobante_det (id_comp,cuenta, tercero, centrocosto, detalle, cheque, valdebito, valcredito, estado, vigencia) VALUES ('42 $codingreso', '$cuentaiva', '$numtercero', '".$_POST['dncc'][0]."', 'Retención ".strtoupper($_POST['dndescuentos'][$x])."', '', '0', '".$_POST['ddesvalores'][$x]."', '1', '$vigencia')";
								mysqli_query($linkbd,$sqlr);

								$sqlr="INSERT INTO comprobante_det (id_comp,cuenta, tercero, centrocosto, detalle, cheque, valdebito, valcredito, estado, vigencia) VALUES ('42 $codingreso', '$cuentaretencion', '$numtercero', '".$_POST['dncc'][0]."', 'Retención ".strtoupper($_POST['dncoding'][$x])."', '', '".$_POST['ddesvalores'][$x]."', '0', '1', '$vigencia')";
								mysqli_query($linkbd,$sqlr);
							}else{
								$coddetalle = selconsecutivo('teso_ingreso_bienes_servicios_det','id_det');
								$sql = "INSERT INTO teso_ingreso_bienes_servicios_det (id_det, id_recibos, codigo, nombre, tipo, porcentaje, valor, cc, fuente, estado) VALUES ('$coddetalle', '$codingreso', '".$_POST['ddescuentos'][$x]."', '".$_POST['dndescuentos'][$x]."', 'RL', '".$_POST['dporcentajes'][$x]."', '".$_POST['ddesvalores'][$x]."', '".$_POST['dncc'][0]."', '".$_POST['dfuente'][0]."', 'S')";
								mysqli_query($linkbd, $sql);
							}
						}
						echo"
						<script>
							Swal.fire({
								icon: 'success',
								title: 'Se ha guardo con Exito',
								confirmButtonText: 'Continuar',
								confirmButtonColor: '#FF121A',
								timer: 3000
							}).then((response) => {
								location.href = 'teso-ingresoBienesServicios.php';
							});
						</script>";
					} 
				?>	
				<script>
					/*var myDiv = document.getElementById("myDiv");
					var isResizing = false;
					myDiv.addEventListener("mousedown", function(event) {
						isResizing = true;
					});
					myDiv.addEventListener("mouseup", function(event) {
						isResizing = false;
					});
					myDiv.addEventListener("mousemove", function(event) {
						if (isResizing) {
							var y = event.clientY;
							myDiv.style.height = y + "px";
						}
					});*/
				</script>
				<div id="bgventanamodal2">
					<div id="ventanamodal2">
						<IFRAME  src="" name="buster" marginWidth=0 marginHeight=0 frameBorder=0 id="ventana2" frameSpacing=0 style="left:500px; width:900px; height:500px; top:200;"> 
						</IFRAME>
					</div>
				</div>
			</div>
		</form>
	</body>
</html>