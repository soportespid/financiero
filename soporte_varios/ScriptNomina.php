<?php
    require_once '../Librerias/core/Helpers.php';
    require_once '../Librerias/core/Mysql.php';
    /*ini_set('display_errors', '1');
    ini_set('display_startup_errors', '1');
    error_reporting(E_ALL);*/

    class ScriptNomina extends Mysql{
        private $intConsecutivo;
        private $strTercero;
        private $intPeriodoId;
        private $strFechaInicio;
        private $strFechaFinal;
        private $strBancoId;
        private $strCuentaBanco;
        private $intCargoId;
        private $strJornada;
        private $strContrato;
        private $intTipoContrato;
        private $intAutomatico;
        private $strVinculacion;
        private $strPttoPagos;
        private $strPttoSalarios;
        private $intUnidad;
        private $intSeccion;
        private $strCentro;
        private $strTerceroEps;
        private $strTerceroArl;
        private $strTerceroAfp;
        private $strTerceroFondo;
        private $strFechaEps;
        private $strFechaArl;
        private $strFechaAfp;
        private $strFechaFondo;
        private $intCesantias;
        private $intRiesgo;
        private $floatSalud;
        private $floatPension;
        private $strPttoPara;
        private $strFuente;
        private $strIndicador;
        private $intBpin;
        function __construct(){
            parent::__construct();
        }
        public function insertContrato(string $strTercero, int $intTipoContrato, int $intPeriodoId, string $strFechaInicio, string $strFechaFinal,
            string $strBancoId, string $strCuentaBanco, int $intCargoId, string $strJornada, string $strContrato, int $intAutomatico,array $requisitos){

            $this->strTercero = $strTercero;
            $this->intTipoContrato = $intTipoContrato;
            $this->intPeriodoId = $intPeriodoId;
            $this->strFechaInicio = $strFechaInicio;
            $this->strFechaFinal = $strFechaFinal;
            $this->strBancoId = $strBancoId;
            $this->strCuentaBanco = $strCuentaBanco;
            $this->intCargoId = $intCargoId;
            $this->strJornada = $strJornada;
            $this->strContrato = $strContrato;
            $this->intAutomatico = $intAutomatico;
            $sql = "SELECT * FROM hum_contratos WHERE tercero = '$this->strTercero'";
            $request = $this->select($sql);
            $return ="";
            if(empty($request)){
                $sql = "INSERT INTO hum_contratos(tercero,tipo,periodo,fecha_inicial,fecha_final,banco,cuenta,cargo_id,jornada,contrato,automatico)
                VALUES(?,?,?,?,?,?,?,?,?,?,?)";
                $request = $this->insert($sql,[
                    $this->strTercero,
                    $this->intTipoContrato,
                    $this->intPeriodoId,
                    $this->strFechaInicio,
                    $this->strFechaFinal,
                    $this->strBancoId,
                    $this->strCuentaBanco,
                    $this->intCargoId,
                    $this->strJornada,
                    $this->strContrato,
                    $this->intAutomatico,
                ]);
                $return = $request;
                if(!empty($requisitos)){
                    foreach ($requisitos as $e) {
                        $tipos = $e['tipos'];
                        foreach ($tipos as $f) {
                            $req = $f['requisitos'];
                            foreach ($req as $g) {
                                if($g['is_checked'] && $f['is_checked'] && $e['is_checked']){
                                    $sqlReq = "INSERT INTO hum_contratos_requisitos(cargo_id,requisito_id,documento_id,tipo,estado) VALUES(?,?,?,?,?)";
                                    $this->insert($sqlReq,[$this->intCargoId,$g['id'],$return,"CONTRATO",$g['checked']]);
                                }
                            }
                        }
                    }
                }
            }else{
                $return = "existe";
            }
            return $return;
        }
        public function insertPtto(int $intConsecutivo,string $strVinculacion,string $strPttoSalarios,string $strPttoPagos,int $intUnidad,int $intSeccion,string $strCentro,
        string $strPttoPara, string $intBpin,string $strIndicador,string $strFuente){
            $this->intConsecutivo = $intConsecutivo;
            $this->strVinculacion = $strVinculacion;
            $this->strPttoSalarios = $strPttoSalarios;
            $this->strPttoPagos = $strPttoPagos;
            $this->intUnidad = $intUnidad;
            $this->intSeccion = $intSeccion;
            $this->strCentro = $strCentro;
            $this->strPttoPara = $strPttoPara;
            $this->strIndicador = $strIndicador;
            $this->strFuente = $strFuente;
            $this->intBpin = $intBpin;
            $sql = "INSERT INTO hum_contratos_ptto(contrato_id,vinculacion,ptto_salario,ptto_pago,unidad_id,seccion_id,cc_id,ptto_para,fuente,id_bpin,indicador)
            VALUES(?,?,?,?,?,?,?,?,?,?,?)";
            $arrData = [
                $this->intConsecutivo,
                $this->strVinculacion,
                $this->strPttoSalarios,
                $this->strPttoPagos,
                $this->intUnidad,
                $this->intSeccion,
                $this->strCentro,
                $this->strPttoPara,
                $this->strFuente,
                $this->intBpin,
                $this->strIndicador
            ];
            $request = $this->insert($sql,$arrData);
            return $request;
        }
        public function insertSeguridad(int $intConsecutivo,string $strTerceroEps,string $strTerceroArl,string $strTerceroAfp,string $strTerceroFondo,string $strFechaEps,string $strFechaArl,string $strFechaAfp,
        string $strFechaFondo,string $intCesantias,int $intRiesgo,float $floatSalud,float $floatPension){
            $this->intConsecutivo = $intConsecutivo;
            $this->strTerceroEps = $strTerceroEps;
            $this->strTerceroArl = $strTerceroArl;
            $this->strTerceroAfp = $strTerceroAfp;
            $this->strTerceroFondo = $strTerceroFondo;
            $this->strFechaEps = $strFechaEps;
            $this->strFechaArl = $strFechaArl;
            $this->strFechaAfp = $strFechaAfp;
            $this->strFechaFondo = $strFechaFondo;
            $this->intCesantias = $intCesantias;
            $this->intRiesgo = $intRiesgo;
            $this->floatSalud = $floatSalud;
            $this->floatPension = $floatPension;
            $sql = "INSERT INTO hum_contratos_seguridad(contrato_id,tercero_eps,tercero_arl,tercero_afp,tercero_fondo,
            fecha_eps,fecha_arl,fecha_afp,fecha_fondo,cesantias,riesgo_id,salud,pension) VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?)";
            $request = $this->insert($sql,[
                $this->intConsecutivo,
                $this->strTerceroEps,
                $this->strTerceroArl,
                $this->strTerceroAfp,
                $this->strTerceroFondo,
                $this->strFechaEps,
                $this->strFechaArl,
                $this->strFechaAfp,
                $this->strFechaFondo,
                $this->intCesantias,
                $this->intRiesgo,
                $this->floatSalud,
                $this->floatPension,
            ]);
            return $request;
        }
        public function insertDet(int $intConsecutivo,array $data){
            $this->intConsecutivo = $intConsecutivo;
            $this->delete("DELETE FROM humretenempleados_det WHERE cab_id = $this->intConsecutivo");
            foreach ($data as $d) {
                $sql="INSERT INTO humretenempleados_det(cab_id, tipo_pago, cuota, valor,mes, estado) VALUES(?,?,?,?,?,?)";
                $request = $this->insert($sql,[
                    $this->intConsecutivo,
                    $d['tipo_pago'],
                    $d['cuota'],
                    $d['valor'],
                    $d['mes'],
                    $d['estado'],
                ]);
            }
            return $request;
        }
        public function selectTempCampoFuncionario($campo,$id){
            $sql = "SELECT COALESCE(h.descripcion, '') AS descripcion
                    FROM hum_funcionarios AS h
                    WHERE h.item = '$campo'
                    AND h.estado = 'S'
                    AND h.codfun = $id
                    AND h.fechain = (
                        SELECT MAX(f.fechain)
                        FROM hum_funcionarios f
                        WHERE f.codfun = $id
                            AND f.item = '$campo'
                            AND f.estado = 'S'
                    )";
            $request = $this->select($sql);
            return $request['descripcion'];
        }
        public function selectRequisitos(){
            $sql = "SELECT * FROM hum_caracterizacion_funciones ORDER BY orden";
            $request = $this->select_all($sql);
            $arrData = [];
            if(!empty($request)){
                $total = count($request);
                for ($i=0; $i < $total ; $i++) {
                    $e = $request[$i];
                    $idFuncion = $e['id'];
                    $sqlTipos = "SELECT * FROM hum_caracterizacion_tipo WHERE funcion_id = $idFuncion ORDER BY orden ";
                    $arrTipos = $this->select_all($sqlTipos);
                    if(!empty($arrTipos)){
                        $totalTipos = count($arrTipos);
                        for ($j=0; $j < $totalTipos ; $j++) {
                            $idTipo = $arrTipos[$j]['id'];
                            $sqlRequisitos = "SELECT * FROM hum_caracterizacion_requisito WHERE tipo_id = $idTipo";
                            $arrRequisitos = $this->select_all($sqlRequisitos);
                            $totalRequisitos = count($arrRequisitos);
                            for ($k=0; $k < $totalRequisitos ; $k++) {
                                $arrRequisitos[$k]['is_checked'] = 0;
                            }
                            $arrTipos[$j]['requisitos'] = $arrRequisitos;
                            $arrTipos[$j]['is_checked'] = 1;
                        }
                        $arrTipos = array_values(array_filter($arrTipos,function($e){return !empty($e['requisitos']);}));
                        $request[$i]['tipos'] = $arrTipos;
                        $request[$i]['is_checked'] = 0;
                        array_push($arrData,$request[$i]);
                    }
                }
            }
            return $arrData;
        }
        public function selectCargos(){
            $sql = "SELECT
            c.codcargo as codigo,
            c.nombrecargo as nombre,
            c.clasificacion,
            s.valor,
            s.id_nivel as escala,
            s.nombre as nivel_salarial
            FROM planaccargos c
            INNER JOIN humnivelsalarial s ON s.id_nivel=c.clasificacion
            WHERE c.estado = 'S' AND s.estado = 'S' ORDER BY c.codcargo DESC";
            $request = $this->select_all($sql);
            $dataRequisitos = $this->selectRequisitos();
            if(!empty($request)){
                $totalCargos = count($request);
                for ($i=0; $i < $totalCargos ; $i++) {
                    $id = $request[$i]['codigo'];
                    $sql = "SELECT hf.id as funciones_id, ht.id as tipo_id, hr.id as requisito_id
                    FROM planaccargos_requisitos pr
                    INNER JOIN hum_caracterizacion_requisito hr ON hr.id = pr.requisito_id
                    INNER JOIN hum_caracterizacion_tipo ht ON ht.id = hr.tipo_id
                    INNER JOIN hum_caracterizacion_funciones hf ON hf.id = ht.funcion_id
                    WHERE hr.estado = 'S' AND ht.estado = 'S' AND hf.estado = 'S' AND pr.cargo_id = $id";
                    $arrCargoRequisitos = $this->select_all($sql);
                    if(!empty($arrCargoRequisitos)){
                        for ($j=0; $j < count($dataRequisitos) ; $j++) {
                            $areas = $dataRequisitos[$j];
                            $tipos = $areas['tipos'];
                            for ($k=0; $k < count($tipos) ; $k++) {
                                $tipo = $tipos[$k];
                                $requisitos = $tipo['requisitos'];
                                for ($l=0; $l < count($requisitos)  ; $l++) {
                                    $requisito = $requisitos[$l];
                                    $cargoReq = array_filter($arrCargoRequisitos,function($e)use($requisito,$areas,$tipo){
                                        return $requisito['id'] == $e['requisito_id'] && $tipo['id'] == $e['tipo_id']
                                        && $areas['id'] == $e['funciones_id'];
                                    });
                                    $requisito['is_checked'] = !empty($cargoReq) ? 1 : 0;
                                    $requisito['checked'] =1;
                                    $requisitos[$l] = $requisito;
                                }
                                $tipos[$k]['requisitos'] = $requisitos;
                                $tipos[$k]['is_checked'] = !empty(array_filter($requisitos,function($e){return $e['is_checked'] == 1;})) ? 1 : 0;
                            }
                            $dataRequisitos[$j]['tipos'] = $tipos;
                            $dataRequisitos[$j]['is_checked'] = !empty(array_filter($tipos,function($e){return $e['is_checked'] == 1;})) ? 1 : 0;
                        }
                    }
                    $request[$i]['requisitos'] = $dataRequisitos;
                }
            }
            return $request;
        }
        public function selectPeriodos(){
            $sql = "SELECT id_periodo as codigo,nombre,dias FROM humperiodos ORDER BY id_periodo";
            $request = $this->select_all($sql);
            return $request;
        }
        public function selectTempFuncionarios(){

            $arrCargos = $this->selectCargos();
            $arrPeriodos = $this->selectPeriodos();

            $sql="SELECT T1.codfun,T1.fechain as fecha
            FROM hum_funcionarios T1
            WHERE (T1.item = 'IDCARGO' OR T1.item = 'DOCTERCERO' OR T1.item = 'NUMCUENTA' OR T1.item = 'NOMCUENTA' OR T1.item = 'NUMEPS' OR T1.item = 'NUMARL'
                    OR T1.item = 'NUMAFP' OR T1.item = 'NUMFDC' OR T1.item = 'NUMCC' OR T1.item = 'PERLIQ' OR T1.item = 'TPCESANTIAS'
                    OR T1.item = 'NIVELARL' OR T1.item = 'UNIEJECUTORA' OR T1.item = 'TPRESUPUESTO' OR T1.item = 'PORSF' OR T1.item = 'PORPF'
                    OR T1.item = 'TPRESUPUESTOPARA' OR T1.item = 'SECCIONPRESUP' OR T1.item = 'TPRESUPUESTOOTROS' OR T1.item = 'TVINCULACION') AND T1.estado = 'S'
            GROUP BY T1.codfun
            ORDER BY CONVERT(T1.codfun, SIGNED INTEGER)";
            $request = $this->select_all($sql);

            $this->delete("TRUNCATE TABLE hum_contratos");
            $this->delete("TRUNCATE TABLE hum_contratos_novedades");
            $this->delete("TRUNCATE TABLE hum_contratos_renovacion");
            $this->delete("TRUNCATE TABLE hum_contratos_requisitos");
            $this->delete("TRUNCATE TABLE hum_contratos_ptto");
            $this->delete("TRUNCATE TABLE hum_contratos_seguridad");
            foreach ($request as $data) {
                $fecha = $data['fecha'];
                $idFuncionario = $data['codfun'];

                $tipoContrato = 1;
                $idCargo = $this->selectTempCampoFuncionario("IDCARGO",$idFuncionario);
                $arrCargo = array_values(array_filter($arrCargos,function($e)use($idCargo){return $idCargo == $e['codigo'];}));
                $cargo = 0;
                $cargoRequisitos = [];
                if(!empty($arrCargo)){
                    $cargo = $arrCargo[0]['codigo'];
                    $cargoRequisitos = $arrCargo[0]['requisitos'];
                }
                $tercero = $this->selectTempCampoFuncionario("DOCTERCERO",$idFuncionario);
                $sql = "SELECT T1.codfun,
					GROUP_CONCAT(T1.descripcion ORDER BY CONVERT(T1.valor, SIGNED INTEGER) SEPARATOR '<->') as dato
					FROM hum_funcionarios T1
					WHERE (T1.item = 'NOMCARGO' OR T1.item = 'VALESCALA' OR T1.item = 'DOCTERCERO' OR T1.item = 'NOMTERCERO' OR T1.item = 'ESTGEN' OR T1.item = 'NOMCC') AND T1.estado='S' $crit1 $crit2
					GROUP BY T1.codfun
					ORDER BY CONVERT(T1.codfun, SIGNED INTEGER) DESC";
                $funcionarios = $this->select_all($sql);
                $flag = true;
                foreach ($funcionarios as $fun) {
                    $datoFun = explode('<->',$fun['dato']);
                    if($datoFun[5] == "N" && $datoFun[2] == $tercero){
                        $flag = false;
                        break;
                    }
                }
                if($flag){
                    $cuenta = $this->selectTempCampoFuncionario("NUMCUENTA",$idFuncionario);
                    $banco = $this->selectTempCampoFuncionario("NOMCUENTA",$idFuncionario);
                    $eps = strval($this->selectTempCampoFuncionario("NUMEPS",$idFuncionario));
                    $arl = strval($this->selectTempCampoFuncionario("NUMARL",$idFuncionario));
                    $afp = strval($this->selectTempCampoFuncionario("NUMAFP",$idFuncionario));
                    $cesantias = strval($this->selectTempCampoFuncionario("NUMFDC",$idFuncionario));
                    $centro = strval($this->selectTempCampoFuncionario("NUMCC",$idFuncionario));
                    $idPeriodo = $this->selectTempCampoFuncionario("PERLIQ",$idFuncionario);
                    $arrPeriodo = array_values(array_filter($arrPeriodos,function($e)use($idPeriodo){return $idPeriodo == $e['dias'];}));
                    if(!empty($arrPeriodo)){
                        $periodo = $arrPeriodo[0]['codigo'];
                    }
                    $tipoCesantias = $this->selectTempCampoFuncionario("TPCESANTIAS",$idFuncionario);
                    $nivelArl = $this->selectTempCampoFuncionario("NIVELARL",$idFuncionario);
                    $unidadEjecutora = intval($this->selectTempCampoFuncionario("UNIEJECUTORA",$idFuncionario));
                    $saludFuncionario = floatval($this->selectTempCampoFuncionario("PORSF",$idFuncionario));
                    $pensionFuncionario = floatval($this->selectTempCampoFuncionario("PORPF",$idFuncionario));
                    $seccionPresupuestal = intval($this->selectTempCampoFuncionario("SECCIONPRESUP",$idFuncionario));
                    $pttoOtros = strval($this->selectTempCampoFuncionario("TPRESUPUESTOOTROS",$idFuncionario));
                    $pttoSalario = strval($this->selectTempCampoFuncionario("TPRESUPUESTO",$idFuncionario));
                    $pttoPara = strval($this->selectTempCampoFuncionario("TPRESUPUESTOPARA",$idFuncionario));
                    $bpin = strval($this->selectTempCampoFuncionario("PROYECTOINV",$idFuncionario));
                    $programatico = strval($this->selectTempCampoFuncionario("PROGRAMATICO",$idFuncionario));
                    $fuente = strval($this->selectTempCampoFuncionario("FUENTEINV",$idFuncionario));
                    $vinculacion = strval($this->selectTempCampoFuncionario("TVINCULACION",$idFuncionario));
                    $jornada = '{"jornada":"1","horario":{"inicio":"08:00","final":"17:30","inicio_sabado":"08:00","final_sabado":"13:00","inicio_domingo":"","final_domingo":""}}';
                    $contrato = '{"obligaciones":[{"type":1,"value":"Pagar en la forma pactada el monto equivalente a la remuneración."},{"type":1,"value":"Realizar la afiliación y correspondiente aporte a parafiscales."},{"type":1,"value":"Dotar al TRABAJADOR de los elementos de trabajo necesarios para el correcto desempeño de la gestión contratada."},{"type":1,"value":"Las obligaciones especiales enunciadas en los artículos 56 y 57 del Código Sustantivo del Trabajo."},{"type":2,"value":"Cumplir a cabalidad con el objeto del contrato, en la forma convenida."},{"type":2,"value":"Las obligaciones especiales enunciadas en los artículos 56 y 58 del Código Sustantivo del Trabajo."}],"clausulas":[{"clausula":"Terminación unilateral del contrato. El presente contrato se podrá terminar unilateralmente y sin indemnización alguna, por cualquiera de las partes, siempre y cuando se configure algunas de las situaciones previstas en el artículo 62 del Código Sustantivo del Trabajo o haya incumplimiento grave de alguna cláusula prevista en el contrato de trabajo. Se considera incumplimiento grave el desconocimiento de las obligaciones o prohibiciones previstas en el contrato","paragrafo":""},{"clausula":"Este contrato ha sido redactado estrictamente de acuerdo con la ley y la jurisprudencia y será interpretado de buena fe y en consonancia con el Código Sustantivo del Trabajo  cuyo objeto, definido en su artículo 1º, es lograr la justicia en las relaciones entre empleadores y trabajadores dentro de un espíritu de coordinación económica y equilibrio social.","paragrafo":""},{"clausula":"El presente contrato reemplaza en su integridad y deja sin efecto alguno cualquiera otro contrato verbal o escrito celebrado por las partes con anterioridad. Las modificaciones que se acuerden al presente contrato se anotarán a continuación de su texto.","paragrafo":""}]}';
                    $requestContrato = $this->insertContrato($tercero,$tipoContrato,$periodo,$fecha,"0000-00-00 00:00:00",$banco,$cuenta,$cargo,$jornada,$contrato,0,$cargoRequisitos);

                    if($requestContrato=="existe"){
                        $idContrato = $this->select("SELECT id FROM hum_contratos WHERE tercero = '$tercero'")['id'];
                        $sqlNovedad = "INSERT INTO hum_contratos_novedades(contrato_id, tipo,fecha) VALUES(?,?,?)";
                        $requestNovedad = $this->insert($sqlNovedad,[$idContrato,1,$fecha]);
                        $this->update("UPDATE hum_contratos SET estado =? WHERE id = $idContrato",["RENOVADO"]);
                        $sql = "INSERT INTO hum_contratos_renovacion(id,tercero,tipo,periodo,fecha_inicial,fecha_final,banco,cuenta,cargo_id,jornada,contrato,novedad_id)
                        VALUES(?,?,?,?,?,?,?,?,?,?,?,?)";
                        $this->insert($sql,[
                            $idFuncionario,
                            $tercero,
                            $tipoContrato,
                            $periodo,
                            $fecha,
                            "0000-00-00 00:00:00",
                            $cuenta,
                            $banco,
                            $cargo,
                            $jornada,
                            $contrato,
                            $requestNovedad
                        ]);
                        if(!empty($requisitos)){
                            foreach ($requisitos as $e) {
                                $tipos = $e['tipos'];
                                foreach ($tipos as $f) {
                                    $req = $f['requisitos'];
                                    foreach ($req as $g) {
                                        if($g['is_checked'] && $f['is_checked'] && $e['is_checked']){
                                            $sqlReq = "INSERT INTO hum_contratos_requisitos(cargo_id,requisito_id,documento_id,tipo,estado) VALUES(?,?,?,?,?)";
                                            $this->insert($sqlReq,[$cargo,$g['id'],$requestNovedad,"RENOVADO",1]);
                                        }
                                    }
                                }
                            }
                        }
                    }else{
                        $this->insertPtto($requestContrato,$vinculacion,$pttoSalario,$pttoOtros,$unidadEjecutora,$seccionPresupuestal,$centro,$pttoPara,$bpin,$programatico,$fuente);
                        $this->insertSeguridad($requestContrato,$eps,$arl,$afp,$cesantias,$fecha,$fecha,$fecha,$fecha,$tipoCesantias,$nivelArl,$saludFuncionario,$pensionFuncionario);
                    }
                }
            }
        }
        public function selectCuotasUpdate($strFechaInicial="",$strFechaFinal="",$strDocumento="",$intContrato="",$strDescripcion=""){
            $sql = "SELECT *,DATE_FORMAT(fecha,'%d/%m/%Y') as fecha
            FROM humretenempleados WHERE estado != '' AND fecha BETWEEN '$strFechaInicial' AND '$strFechaFinal'
            AND empleado LIKE '$strDocumento%' AND idfuncionario LIKE '$intContrato%' AND descripcion LIKE '$strDescripcion%'
            ORDER BY id DESC";
            $request = $this->select_all($sql);
            $total = count($request);
            for ($i=0; $i < $total; $i++) {
                $id = $request[$i]['id'];
                $sqlDet = "SELECT COUNT(*) as cuotas_pagas,COALESCE(SUM(valor)) AS total_pagado FROM humretenempleados_det WHERE estado = 'P' AND cab_id = $id";
                $sqlCuotas = "SELECT COUNT(1) as total FROM humnominaretenemp WHERE id = '$id' AND estado = 'P' AND tipo_des = 'DS'";
                $cuotasPagas = $this->select($sqlCuotas)['total'];
                $cuotasFaltantes = $request[$i]['ncuotas'] - $cuotasPagas;
                if($cuotasFaltantes == 0 && $request[$i]['estado'] == "S"){
                    $sqlUpdate = "UPDATE humretenempleados SET estado = ? WHERE id='$id'";
                    $this->update($sqlUpdate,['P']);
                    $request[$i]['estado'] = "P";
                }
                $arrDet = $this->select($sqlDet);
                $request[$i]['cuotas_pagas'] = 0;
                $request[$i]['total_pagado'] = 0;
                $request[$i]['is_status'] = $request[$i]['estado'] =="S"? 1 : 0;
                if(!empty($arrDet)){
                    $request[$i]['cuotas_pagas'] = $arrDet['cuotas_pagas'];
                    $request[$i]['total_pagado'] = $arrDet['total_pagado'];
                }
            }
            return $request;
        }
        public function selectTempCuotas(){
            $this->delete("TRUNCATE TABLE humretenempleados_det");
            $sql = "SELECT * FROM humretenempleados";
            $request = $this->select_all($sql);
            $arrContratos = $this->select_all("SELECT id,tercero FROM hum_contratos");
            foreach ($request as $data) {
                $idContrato = array_values(array_filter($arrContratos,function($e) use($data){return $e['tercero'] == $data['empleado'];}));
                if(!empty($idContrato)){
                    $idContrato = $idContrato[0]['id'];
                    $estadoCuota =$data['estado'];
                    $estadoDescuento =$data['estado'];
                    $totalCuotas = $data['ncuotas'];
                    $totalDeuda = $data['deuda'];
                    $valorCuota = $totalDeuda/$totalCuotas;
                    $arrCuotas = [];
                    $arrFecha = explode("-",$data['fecha']);
                    $mes = $arrFecha[0]."-".$arrFecha[1];
                    if($estadoDescuento !="P" && $data['habilitado'] == "D"){
                        $estadoDescuento = "N";
                    }else if($estadoDescuento =="P"){
                        $estadoCuota ="P";
                    }
                    for ($i=0; $i < $totalCuotas; $i++) {
                        array_push($arrCuotas,[
                            "tipo_pago"=>$data['tipopago'],
                            "cuota"=>$i+1,
                            "valor"=>$valorCuota,
                            "mes"=>$mes,
                            "estado"=>$estadoCuota,
                        ]);
                    }
                    $sqlUpdate = "UPDATE humretenempleados SET estado=?,idfuncionario=? WHERE id = {$data['id']}";
                    $this->update($sqlUpdate,[$estadoDescuento,$idContrato]);
                    $this->insertDet($data['id'],$arrCuotas);
                }
            }
            $this->selectCuotasUpdate();
        }
    }
    $obj = new ScriptNomina();
    $obj->selectTempFuncionarios();
    $obj->selectTempCuotas();
    $obj->selectTempCuotas();
?>
