const URL = "tesoreria_funciones/teso_funciones.php";
const form = document.querySelector("#form");
const btnAdd = document.querySelector("#btnAdd");
window.addEventListener("load",function(){
    getData();
});

form.addEventListener("submit",function(e){
    e.preventDefault();
    save();
});
async function save(){
    let formData = new FormData(form);
    formData.append("action","set");
    btnAdd.innerHTML="Espere...";
    btnAdd.setAttribute("disabled","");
    const response = await fetch(URL,{method:"POST",body:formData});
    const objData = await response.json();
    if(!objData.status){
        alert(objData.msg);
    }else{
        form.reset();
        getData();
    }
    btnAdd.innerHTML="Guardar";
    btnAdd.removeAttribute("disabled");
}
async function getData(){
    let formData = new FormData();
    formData.append("action","get_all");
    const response = await fetch(URL,{method:"POST",body:formData});
    const objData = await response.json();
    let html ="";
    if(objData.status){
        const data = objData.data;
        data.forEach(e => {
            html+=`
                <tr>
                    <td>${e.id}</td>
                    <td>${e.nombre}</td>
                    <td>${e.opciones}</td>
                </tr>
            `;
        });
    }
    document.querySelector("#tableData").innerHTML = html;
};
async function editItem(element,id){
    let formData = new FormData(form);
    formData.append("action","get");
    formData.append("id",id);
    element.innerHTML="Espere...";
    element.setAttribute("disabled","");
    const response = await fetch(URL,{method:"POST",body:formData});
    const objData = await response.json();
    if(!objData.status){
        alert(objData.msg);
    }else{
        const data = objData.data;
        document.querySelector("#id").value = data.id;
        document.querySelector("#strName").value = data.nombre;
    }
    element.innerHTML="Editar";
    element.removeAttribute("disabled");
}
async function deleteItem(element,id){
    let formData = new FormData(form);
    formData.append("action","del");
    formData.append("id",id);
    element.innerHTML="Espere...";
    element.setAttribute("disabled","");
    const response = await fetch(URL,{method:"POST",body:formData});
    const objData = await response.json();
    if(!objData.status){
        alert(objData.msg);
    }else{
        getData();
    }
    element.innerHTML="Eliminar";
    element.removeAttribute("disabled");
}

