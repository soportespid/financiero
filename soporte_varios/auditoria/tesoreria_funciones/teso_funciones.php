<?php
    ini_set('display_errors', '1');
    ini_set('display_startup_errors', '1');
    error_reporting(E_ALL);
    require_once '../../../comun.inc';
    require '../../../funciones.inc';
    require '../../../funcionesSP.inc.php';

    if($_POST){
        $obj = new Plantilla();
        if($_POST['action']=="get_all"){
            $obj->getData();
        }else if($_POST['action']=="set"){
            $obj->setData();
        }else if($_POST['action']=="get"){
            $obj->getInfo();
        }else if($_POST['action']=="del"){
            $obj->delData();
        }
    }

    class Plantilla{
        private $linkbd;
        private $strName;
        private $intId;
        public function __construct() {
            $this->linkbd = conectar_v7();
            $this->linkbd->set_charset("utf8");
        }
        public function getData(){
            if(!empty($_SESSION)){
                $request = $this->selectData();
                if(!empty($request)){
                    $total = count($request);
                    for ($i=0; $i < $total ; $i++) {
                        $editar = '<button type="button" onclick="editItem(this,'.$request[$i]['id'].')">Editar</button>';
                        $eliminar = '<button type="button" onclick="deleteItem(this,'.$request[$i]['id'].')">Borrar</button>';
                        $request[$i]['opciones'] = $editar.$eliminar;
                    }
                    $arrResponse = array("status"=>true,"data"=>$request);
                }else{
                    $arrResponse = array("status"=>false,"data"=>$request);
                }
                echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
            }
            die();
        }
        public function getInfo(){
            if(!empty($_SESSION)){
                $id = intval($_POST['id']);
                $request = $this->selectInfo($id);
                if(!empty($request)){
                    $arrResponse = array("status"=>true,"data"=>$request);
                }else{
                    $arrResponse = array("status"=>false,"data"=>$request);
                }
                echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
            }
            die();
        }
        public function setData(){
            if(!empty($_SESSION)){
                if(empty($_POST['strName'])){
                    $arrResponse = array("status"=>false,"msg"=>"Error de datos");
                }else{
                    $intId = intval($_POST['id']);
                    $strName = ucwords(strtolower(strClean($_POST['strName'])));
                    if($intId == 0){
                        $option = 1;
                        $request = $this->insert($strName);
                    }else{
                        $option = 2;
                        $request = $this->update($intId,$strName);
                    }
                    if(is_numeric($request) && $request > 0){
                        if($option == 1){
                            $arrResponse = array("status"=>true,"msg"=>"Datos guardados.");
                        }else{
                            $arrResponse = array("status"=>true,"msg"=>"Datos actualizados.");
                        }
                    }else if($request =="existe"){
                        $arrResponse = array("status"=>false,"msg"=>"Este nombre ya existe, pruebe con otro.");
                    }else{
                        $arrResponse = array("status"=>false,"msg"=>"Ha ocurrido un error.");
                    }
                }
                echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
            }
            die();
        }
        public function delData(){
            if(!empty($_SESSION)){
                $intId = intval($_POST['id']);
                $request = $this->delete($intId);
                if(is_numeric($request)){
                    $arrResponse = array("status"=>true,"msg"=>"Se ha eliminado.");
                }else{
                    $arrResponse = array("status"=>false,"msg"=>"Esta función ya tiene registros en auditoría, no puede ser eliminado.");
                }
                echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
            }
            die();
        }
        public function insert(string $strName){
            $this->strName = $strName;
            $sql = "SELECT * FROM teso_funciones WHERE nombre = '$this->strName'";
            $request = mysqli_fetch_all(mysqli_query($this->linkbd,$sql),MYSQLI_ASSOC);
            $return ="";
            if(empty($request)){
                $sql = "INSERT INTO teso_funciones(nombre) VALUES('$this->strName')";
                $return = intval(mysqli_query($this->linkbd,$sql));
            }else{
                $return ="existe";
            }
            return $return;
        }
        public function update(int $intId,string $strName){
            $this->strName = $strName;
            $this->intId = $intId;
            $sql = "SELECT * FROM teso_funciones WHERE nombre = '$this->strName' AND id != $this->intId ";
            $request = mysqli_fetch_all(mysqli_query($this->linkbd,$sql),MYSQLI_ASSOC);
            $return ="";
            if(empty($request)){
                $sql = "UPDATE teso_funciones SET nombre = '$this->strName' WHERE id = $this->intId";
                $return = intval(mysqli_query($this->linkbd,$sql));
            }else{
                $return ="existe";
            }
            return $return;
        }
        public function delete(int $intId){
            $this->intId = $intId;
            $sql = "SELECT * FROM teso_auditoria WHERE teso_funciones_id = $this->intId ";
            $request = mysqli_fetch_all(mysqli_query($this->linkbd,$sql),MYSQLI_ASSOC);
            $return ="";
            if(empty($request)){
                $sql = "DELETE FROM teso_funciones WHERE id = $this->intId";
                $return = intval(mysqli_query($this->linkbd,$sql));
            }else{
                $return ="existe";
            }
            return $return;
        }
        public function selectData(){
            $sql = "SELECT * FROM teso_funciones";
            $request = mysqli_fetch_all(mysqli_query($this->linkbd,$sql),MYSQLI_ASSOC);
            return $request;
        }
        public function selectInfo(int $intId){
            $this->intId = $intId;
            $sql = "SELECT * FROM teso_funciones WHERE id = $this->intId";
            $request = mysqli_query($this->linkbd,$sql)->fetch_assoc();
            return $request;
        }

    }

?>
