<?php

    header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=utf8");
	date_default_timezone_set("America/Bogota");
?>
<!DOCTYPE >
<html lang=es>
	<head>
    <meta charset="utf-8"/>
		<meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1"/>
		<meta name="viewport" content="user-scalable=no">
		<title>:: IDEAL 10 - Auditoría funciones tesorería</title>
		<link href="favicon.ico" rel="shortcut icon"/>
	</head>
	<body>
		<form id="form">
            <h2 class="titulos m-0">Auditoria - funciones de auditoria para tesoreria</h2>
            <input type="hidden" id="id" name="id">
            <label for="">Nombre</label>
            <input type="text" id="strName" name="strName">
            <button type="submit" id="btnAdd">Guardar</button>
		</form>
        <div style="overflow-y:auto;max-height:50vh">
            <table style="width:100%;text-align:left">
                <thead>
                    <th>ID</th>
                    <th>Nombre</th>
                    <th>Opciones</th>
                </thead>
                <tbody id="tableData"></tbody>
            </table>
        </div>
		<script src="tesoreria_funciones/teso_funciones.js?<?= date('d_m_Y_h_i_s');?>"></script>

	</body>
</html>
