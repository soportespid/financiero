<?php
    require_once '../../../PHPExcel/Classes/PHPExcel.php';
    require_once '../../../comun.inc';
    require '../../../funciones.inc';
    require '../../../funcionesSP.inc.php';
    /*ini_set('display_errors', '1');
    ini_set('display_startup_errors', '1');
    error_reporting(E_ALL);*/
    session_start();
    if($_POST){
        $obj = new Plantilla();
        if($_POST['action']=="upload"){
            $obj->upload();
        }
    }

    class Plantilla{
        private $linkbd;
        public function __construct() {
            $this->linkbd = conectar_v7();
            $this->linkbd->set_charset("utf8");
        }
        public function upload(){
            if(!empty($_SESSION)){
                $arrPredios = $this->selectPredios();
                if(!empty($arrPredios)){
                    $total = count($arrPredios);
                    $request = 0;
                    for ($i=0; $i < $total ; $i++) {
                        $request = $this->insertPropietarios($arrPredios[$i]);
                    }
                    if($request > 0){
                        $arrResponse = array("status"=>true,"msg"=>"Se han asignado los propietarios faltantes a los predios.");
                    }else{
                        $arrResponse = array("status"=>false,"msg"=>"Ha ocurrido un error");
                    }
                }else{
                    $arrResponse = array("status"=>false,"msg"=>"No hay nada que corregir");
                }
                echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
            }
            die();
        }
        public function insertPropietarios($data){
            $prop = $data['propietario'];
            $sql = "INSERT INTO predio_propietarios(predio_id,orden,tipo_documento,documento,nombre_propietario,created_at,updated_at,cancelado)
            VALUES(
                '$data[id]',
                1,
                '$prop[tipo_documento]',
                '$prop[documento]',
                '$prop[nombre_propietario]',
                NOW(),
                NOW(),
                0
            )";
            mysqli_query($this->linkbd,$sql);
            $sql = "UPDATE predio_informacions SET direccion = '$prop[direccion]' WHERE predio_id = '$data[id]'";
            $request = intval(mysqli_query($this->linkbd,$sql));
            return $request;
        }
        public function selectPredios(){
            $arrPredios = [];
            $sql = "SELECT id,codigo_catastro FROM predios";
            $request = mysqli_fetch_all(mysqli_query($this->linkbd,$sql),MYSQLI_ASSOC);
            $total = count($request);
            for ($i=0; $i < $total ; $i++) {
                $sql_prop = "SELECT * FROM predio_propietarios WHERE orden = 1 AND predio_id = {$request[$i]['id']}";
                $arrPropietario = mysqli_query($this->linkbd,$sql_prop)->fetch_assoc();
                if(empty($arrPropietario)){
                    array_push($arrPredios,$request[$i]);
                }
            }
            $arrPredios = $this->selectInfoPredio($arrPredios);
            return $arrPredios;
        }
        public function selectInfoPredio($data){
            $total = count($data);
            $arrData = [];
            for ($i=0; $i < $total ; $i++) {
                $sql = "SELECT documento,d as tipo_documento,direccion,nombrepropietario as nombre_propietario
                FROM tesopredios WHERE cedulacatastral = '{$data[$i]['codigo_catastro']}' AND ord = '001'";
                $arrPropietario = mysqli_query($this->linkbd,$sql)->fetch_assoc();
                if(!empty($arrPropietario)){
                    $data[$i]['propietario'] = $arrPropietario;
                    array_push($arrData,$data[$i]);
                }
            }
            return $arrData;
        }
    }
?>
