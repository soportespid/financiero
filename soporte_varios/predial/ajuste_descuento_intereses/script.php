<?php
    /*ini_set('display_errors', '1');
    ini_set('display_startup_errors', '1');
    error_reporting(E_ALL);*/
    require_once '../../../PHPExcel/Classes/PHPExcel.php';
    require_once '../../../comun.inc';
    require '../../../funciones.inc';
    require '../../../funcionesSP.inc.php';

    session_start();
    if($_POST){
        $obj = new Plantilla();
        if($_POST['action']=="upload"){
            $obj->upload();
        }
    }

    class Plantilla{
        private $linkbd;
        public function __construct() {
            $this->linkbd = conectar_v7();
            $this->linkbd->set_charset("utf8");
        }
        public function upload(){
            if(!empty($_SESSION)){
                $arrData = $this->selectFacturas();
                if(!empty($arrData)){
                    $data = $arrData;
                    $total = count($data);
                    $request = 0;
                    for ($i=0; $i < $total ; $i++) {
                        $request = $this->insertData($data[$i]);
                    }
                    if($request > 0){
                        $arrResponse = array("status"=>true,"msg"=>"Se ha corregido correctamente.");
                    }else{
                        $arrResponse = array("status"=>false,"msg"=>"Ha ocurrido un error");
                    }
                }else{
                    $arrResponse = array("status"=>false,"msg"=>"No hay nada que corregir");
                }
                echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
            }
            die();
        }
        public function selectFacturas(){
            $sql = "TRUNCATE TABLE tesoliquidapredial_desc";
            mysqli_query($this->linkbd, $sql);

            $sql="SELECT id,data FROM factura_predials ORDER BY id ASC";
            $request = mysqli_fetch_all(mysqli_query($this->linkbd,$sql),MYSQLI_ASSOC);
            $total = count($request);
            $arrData = [];

            for ($i=0; $i < $total ; $i++) {
                $idLiquidacion = $request[$i]['id'];
                $factura = json_decode($request[$i]['data'],true);
                $arrVigencias = $factura['liquidacion']['vigencias'];
                $totalVigencias = count($arrVigencias);
                for ($j=0; $j < $totalVigencias ; $j++) {
                    $vigencia = $arrVigencias[$j];
                    $predialDescuentoInt = isset($vigencia['predial_descuento_intereses']) ? $vigencia['predial_descuento_intereses'] : 0;
                    $bomberilDescuentoInt = isset($vigencia['bomberil_descuento_intereses']) ? $vigencia['bomberil_descuento_intereses'] : 0;
                    $ambientalDescuentoInt = isset($vigencia['ambiental_descuento_intereses']) ? $vigencia['ambiental_descuento_intereses'] : 0;
                    array_push($arrData,array(
                        "id_predial"=>$idLiquidacion,
                        "cod_catastral"=>$factura['codigo_catastro'],
                        "user"=>$factura['nombre_propietario'],
                        "vigencia"=>$vigencia['vigencia'],
                        "descuentointpredial"=>$predialDescuentoInt,
                        "descuentointbomberil"=>$bomberilDescuentoInt,
                        "descuentointambiental"=>$ambientalDescuentoInt,
                        "val_alumbrado"=>$vigencia['alumbrado'],
                        "estado"=>"S"
                    ));
                }
            }
            return $arrData;
        }
        public function insertData($data){
            $sql = "INSERT INTO tesoliquidapredial_desc(id_predial,cod_catastral,vigencia,descuentointpredial,descuentointbomberil,descuentointambiental,
            val_alumbrado,estado,user) VALUES(
                '$data[id_predial]',
                '$data[cod_catastral]',
                '$data[vigencia]',
                '$data[descuentointpredial]',
                '$data[descuentointbomberil]',
                '$data[descuentointambiental]',
                '$data[val_alumbrado]',
                '$data[estado]',
                '$_SESSION[cedulausu]'
            )";
            $request = intval(mysqli_query($this->linkbd,$sql));
            return $request;
        }
    }
?>
