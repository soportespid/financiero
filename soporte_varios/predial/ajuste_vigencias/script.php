<?php
    require_once '../../../PHPExcel/Classes/PHPExcel.php';
    require_once '../../../comun.inc';
    require '../../../funciones.inc';
    require '../../../funcionesSP.inc.php';
    /*ini_set('display_errors', '1');
    ini_set('display_startup_errors', '1');
    error_reporting(E_ALL);*/
    session_start();
    if($_POST){
        $obj = new Plantilla();
        if($_POST['action']=="upload"){
            $obj->upload();
        }
    }

    class Plantilla{
        private $linkbd;
        public function __construct() {
            $this->linkbd = conectar_v7();
            $this->linkbd->set_charset("utf8");
        }
        public function upload(){
            if(!empty($_SESSION)){
                $arrPredios = $this->selectVigencias();
                if(!empty($arrPredios)){
                    $total = count($arrPredios);
                    $request =0;
                    for ($i=0; $i < $total ; $i++) {
                        $request = $this->insertVigencias($arrPredios[$i]['id'],$arrPredios[$i]['vigencias']);
                    }
                    if($request > 0){
                        $arrResponse = array("status"=>true,"msg"=>"Las vigencias de los predios han sido corregidas correctamente.");
                    }else{
                        $arrResponse = array("status"=>false,"msg"=>"Ha ocurrido un error");
                    }
                }else{
                    $arrResponse = array("status"=>false,"msg"=>"No hay nada que corregir");
                }
                echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
            }
            die();
        }
        public function selectVigencias(){
            $sql_det = "SELECT YEAR(created_at) as vigencia,predio_id as id,codigo_destino_economico_id,direccion,hectareas,
            metros_cuadrados,area_construida,predio_estrato_id,predio_tipo_id,user_id,cancelado
            FROM predio_informacions";
            $vigencias = mysqli_fetch_all(mysqli_query($this->linkbd,$sql_det),MYSQLI_ASSOC);
            $arrPredios = [];
            $totalVigencias = count($vigencias);
            for ($j=0; $j < $totalVigencias; $j++) {
                if($totalVigencias > 1){
                    array_push($arrPredios,$vigencias[$j]);
                }
            }
            $arrPredios = $this->orderVigencias($arrPredios);
            return $arrPredios;
        }
        public function orderVigencias($data){
            $arrPredios = [];
            $total = count($data);
            for ($i=0; $i < $total; $i++) {
                $predio = $data[$i];
                $totalPredios = count($arrPredios);
                if($totalPredios>0){
                    $flag = false;
                    for ($j=0; $j < $totalPredios; $j++) {
                        if($arrPredios[$j]['id'] == $predio['id']){
                            array_push($arrPredios[$j]['vigencias'],array(
                                "vigencia"=>$predio['vigencia'],
                                "codigo_destino_economico_id"=>$predio['codigo_destino_economico_id'],
                                "direccion"=>$predio['direccion'],
                                "hectareas"=>$predio['hectareas'],
                                "metros_cuadrados"=>$predio['metros_cuadrados'],
                                "area_construida"=>$predio['area_construida'],
                                "predio_estrato_id"=>$predio['predio_estrato_id'],
                                "predio_tipo_id"=>$predio['predio_tipo_id'],
                                "user_id"=>$predio['user_id'],
                                "cancelado"=>$predio['cancelado']
                            ));
                            $flag = true;
                            break;
                        }
                    }
                    if(!$flag){
                        array_push($arrPredios,
                            array("id"=>$predio['id'],"vigencias"=>array(array(
                                "vigencia"=>$predio['vigencia'],
                                "codigo_destino_economico_id"=>$predio['codigo_destino_economico_id'],
                                "direccion"=>$predio['direccion'],
                                "hectareas"=>$predio['hectareas'],
                                "metros_cuadrados"=>$predio['metros_cuadrados'],
                                "area_construida"=>$predio['area_construida'],
                                "predio_estrato_id"=>$predio['predio_estrato_id'],
                                "predio_tipo_id"=>$predio['predio_tipo_id'],
                                "user_id"=>$predio['user_id'],
                                "cancelado"=>$predio['cancelado']
                            ))
                        ));
                    }
                }else{
                    array_push($arrPredios,
                        array("id"=>$predio['id'],"vigencias"=>array(array(
                            "vigencia"=>$predio['vigencia'],
                            "codigo_destino_economico_id"=>$predio['codigo_destino_economico_id'],
                            "direccion"=>$predio['direccion'],
                            "hectareas"=>$predio['hectareas'],
                            "metros_cuadrados"=>$predio['metros_cuadrados'],
                            "area_construida"=>$predio['area_construida'],
                            "predio_estrato_id"=>$predio['predio_estrato_id'],
                            "predio_tipo_id"=>$predio['predio_tipo_id'],
                            "user_id"=>$predio['user_id'],
                            "cancelado"=>$predio['cancelado']
                        ))
                    ));
                }
            }
            $arrPredios = $this->resetVigencias($arrPredios);
            return $arrPredios;
        }
        public function resetVigencias($data){
            $total = count($data);
            $arrPredios = [];
            for ($i=0; $i < $total; $i++) {
                $vigencias = $data[$i]['vigencias'];
                usort($vigencias,function($a,$b){return $b<$a;});
                $arrSacoVigencias = array_column($vigencias,'vigencia');
                $contador = 0;
                $antiguaVigencia = $vigencias[0]['vigencia'];
                $actualVigencia = $vigencias[count($vigencias)-1]['vigencia'];
                $arrFechas = [];

                for ($j=$antiguaVigencia; $j < $actualVigencia ; $j++) {
                    if(!in_array($j,$arrSacoVigencias)){
                        array_push($arrFechas,array(
                            "vigencia"=>$j,
                            "codigo_destino_economico_id"=>$vigencias[$contador-1]['codigo_destino_economico_id'],
                            "direccion"=>$vigencias[$contador-1]['direccion'],
                            "hectareas"=>$vigencias[$contador-1]['hectareas'],
                            "metros_cuadrados"=>$vigencias[$contador-1]['metros_cuadrados'],
                            "area_construida"=>$vigencias[$contador-1]['area_construida'],
                            "predio_estrato_id"=>$vigencias[$contador-1]['predio_estrato_id'],
                            "predio_tipo_id"=>$vigencias[$contador-1]['predio_tipo_id'],
                            "user_id"=>$vigencias[$contador-1]['user_id'],
                            "cancelado"=>$vigencias[$contador-1]['cancelado']
                        ));
                    }
                    $contador++;
                }
                if(!empty($arrFechas)){
                    $data[$i]['vigencias'] = $arrFechas;
                    array_push($arrPredios,$data[$i]);
                }
            }
            return $arrPredios;
        }
        public function insertVigencias($id,$vigencias){
            $request = 0;
            foreach ($vigencias as $data) {
                $fecha = $data['vigencia']."-12-31";
                $sql = "INSERT INTO predio_informacions(predio_id,codigo_destino_economico_id,direccion,hectareas,
                metros_cuadrados,area_construida,predio_estrato_id,predio_tipo_id,user_id,cancelado,created_at,updated_at)
                VALUES(
                    $id,
                    '$data[codigo_destino_economico_id]',
                    '$data[direccion]',
                    '$data[hectareas]',
                    '$data[metros_cuadrados]',
                    '$data[area_construida]',
                    '$data[predio_estrato_id]',
                    '$data[predio_tipo_id]',
                    '$data[user_id]',
                    '$data[cancelado]',
                    '$fecha',
                    NOW()
                )";
                $request = intval(mysqli_query($this->linkbd,$sql));
            }
            return $request;
        }
    }
?>
