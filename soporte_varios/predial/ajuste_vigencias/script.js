const URL ='ajuste_vigencias/script.php';
async function upload(){
    const btn = document.querySelector(".btn");
    btn.innerHTML ="Espere...";
    btn.setAttribute("disabled","");
    let formData = new FormData();
    formData.append("action","upload");
    const response = await fetch(URL,{method:"POST",body:formData});
    const objData = await response.json();
    alert(objData.msg);
    btn.innerHTML ="Corregir";
    btn.removeAttribute("disabled");
}
