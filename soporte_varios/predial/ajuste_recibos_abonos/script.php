<?php
    /*ini_set('display_errors', '1');
    ini_set('display_startup_errors', '1');
    error_reporting(E_ALL);*/
    require_once '../../../PHPExcel/Classes/PHPExcel.php';
    require_once '../../../comun.inc';
    require '../../../funciones.inc';
    require '../../../funcionesSP.inc.php';

    session_start();
    if($_POST){
        $obj = new Plantilla();
        if($_POST['action']=="upload"){
            $obj->upload();
        }
    }

    class Plantilla{
        private $linkbd;
        public function __construct() {
            $this->linkbd = conectar_v7();
            $this->linkbd->set_charset("utf8");
        }
        public function upload(){
            if(!empty($_SESSION)){
                $arrData = $this->selectData();
                if(!empty($arrData)){
                    $arrResponse = array("status"=>true,"msg"=>"Se ha corregido correctamente.");
                }else{
                    $arrResponse = array("status"=>false,"msg"=>"No hay nada que corregir");
                }
                echo json_encode($arrResponse,JSON_UNESCAPED_UNICODE);
            }
            die();
        }
        public function selectData(){
            $sql = "SELECT * FROM tesoabono WHERE cierre > 0 AND recibo = 0";
            $request = mysqli_fetch_all(mysqli_query($this->linkbd,$sql),MYSQLI_ASSOC);
            $total = count($request);
            for ($i=0; $i < $total ; $i++) {
                $idLiquidacion = $request[$i]['cierre'];
                $idAbono = $request[$i]['id_abono'];
                $sql_recibo = "SELECT id_recibos FROM tesoreciboscaja WHERE id_recaudo=$idLiquidacion";
                $idRecibo = mysqli_query($this->linkbd,$sql_recibo)->fetch_assoc()['id_recibos'];
                $sql_update = "UPDATE tesoabono SET recibo = $idRecibo WHERE id_abono = $idAbono";
                mysqli_query($this->linkbd,$sql_update);
            }
            return $request;
        }
    }
?>
