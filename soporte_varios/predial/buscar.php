<?php
    require_once '../../../comun.inc';
    require '../../../funciones.inc';
    require '../../../funcionesSP.inc.php';

    if($_GET['s']){
        $busqueda = $_GET['s'];
        $obj = new Plantilla();
        dep($obj->buscar($busqueda));
    }

    class Plantilla{
        private $linkbd;
        public function __construct() {
            $this->linkbd = conectar_v7();
            $this->linkbd->set_charset("utf8");
        }
        public function buscar($busqueda){
            $sql = "SELECT data,id FROM estado_cuentas";
            $request = mysqli_fetch_all(mysqli_query($this->linkbd,$sql),MYSQLI_ASSOC);
            $arrEstados = array();
            $total = count($request);
            for ($i=0; $i < $total; $i++) {
                $estado = json_decode($request[$i]['data'],true);
                if($estado['codigo_catastro'] == $busqueda || $estado['direccion'] == $busqueda || $estado['documento'] == $busqueda){
                    array_push($arrEstados,array("id"=>$request[$i]['id'],"data"=>$estado));
                }
            }
            return $arrEstados;
        }
    }
?>
