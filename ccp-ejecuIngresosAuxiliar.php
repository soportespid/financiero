<?php
	header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=utf8");
	require 'comun.inc';
	require 'funciones.inc';
	session_start();
	$linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");
	cargarcodigopag($_GET['codpag'],$_SESSION["nivel"]);
	date_default_timezone_set("America/Bogota");
?>
<!DOCTYPE >
<html lang=es>
	<head>
		<meta charset="utf-8"/>
		<meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1"/>
		<meta name="viewport" content="user-scalable=no">
		<title>:: IDEAL 10 - Presupuesto CCPET</title>
		<link href="favicon.ico" rel="shortcut icon"/>
		<link href="css/css2z.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
		<link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
		<link href="css/css4.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
        <link href="css/tabs.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css"/>
        <link rel="stylesheet" href="sweetalert2/dist/sweetalert2.min.css">
        <script src="sweetalert2/dist/sweetalert2.min.js"></script>
		<script src="vue/vue.min.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script src="css/programas.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script src="css/calendario.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
        <script>
            
        </script>
		<style>
			input[type=number]::-webkit-inner-spin-button,
			input[type=number]::-webkit-outer-spin-button { 
				-webkit-appearance: none; 
				margin: 0; 
			}
			input[type=number]{
				text-align:right;
				box-sizing: border-box;
				font: 120% sans-serif;
				/* width: 100% !important; */
			}
			[v-cloak]{display : none;}

			.centrarSelect {

				height: 30px !important;
				text-align-last:center !important;
			}

			.aumentarTamaño {

				font-size:15px;
			}

            .tamano01 {
                text-align: center !important;
            }

			.button-parametros {
				padding: 5px 10px;
				background-color: #007bff;
				color: white;
				border: none;
				border-radius: 5px;
				cursor: pointer;
			}
			.button-parametros:hover {
				background-color: #0b7dda;
			}
			.button-parametros:active {
				background-color: #3e8e41;
			}

			.button-reflejar {
				padding: 5px 10px;
				background-color: #04AA6D;
				color: white;
				border: none;
				border-radius: 5px;
				cursor: pointer;
			}
			.button-reflejar:hover {
				background-color: #46a049;
			}
			.button-reflejar:active {
				background-color: #3e8e41;
			}
		</style>
	</head>
	<body>
		<header>
			<table>
				<tr><script>barra_imagenes("ccpet");</script><?php cuadro_titulos();?></tr>
			</table>
		</header>

		<form name="form2" method="post" action="">
			<section id="myapp" v-cloak >
				<nav>
					<table>
						<tr><?php menu_desplegable("ccpet");?></tr>
						<tr>
							<td colspan="3" class="cinta">
								<img src="imagenes/add.png" v-on:click="location.href=''" class="mgbt" title="Nuevo">
								<img src="imagenes/guarda.png" title="Guardar" v-on:click="" class="mgbt">
								<img src="imagenes/busca.png" v-on:click="location.href=''" class="mgbt" title="Buscar">
								<img src="imagenes/nv.png" onClick="mypop=window.open('ccp-principal.php','',''); mypop.focus();" class="mgbt" title="Nueva Ventana">
								<img src="imagenes/excel.png" title="Excel" class="mgbt" v-on:click="excel">
							</td>
						</tr>
					</table>
				</nav>

				<article>
                    <div>
                        <table class="inicio grande">
                            <tr>
                                <td class="titulos" colspan="12">.: Auxiliar por cuenta de ingreso</td>
                            	<td class="cerrar" style="width:4%" onClick="location.href='ccp-principal.php'">Cerrar</td>
                            </tr>

                            <tr>
                                <td style="width: 3cm;">Cuenta:</td>
                                <td style="width: 10%;">
                                    <input type="text" v-model="cuenta" readonly>
                                </td>

                                <td style="width: 3cm;">Vigencia de gasto:</td>
                                <td style="width: 10%;">
                                    <input type="text" v-model="vigGasto" readonly>
                                </td>

                                <td style="width: 3cm;">Sec. presupuestal:</td>
                                <td style="width: 10%;">
                                    <input type="text" v-model="secPresupuestal" readonly>
                                </td>

                                <td style="width: 3cm;">Fuente:</td>
                                <td style="width: 10%;">
                                    <input type="text" v-model="fuente" readonly>
                                </td>

								<td style="width: 3cm;">Medio pago:</td>
                                <td style="width: 10%;">
                                    <input type="text" v-model="medioPago" readonly>
                                </td>

                                <td style="width: 3cm;">CPC:</td>
                                <td style="width: 10%;">
                                    <input type="text" v-model="cpc" readonly>
                                </td>
                            <tr>
                                <td style="width:3cm;">Fecha Inicial:</td>
                                <td style="width:10%;">
                                    <input type="text" v-model="fechaIni" readonly>
                                </td>
                                <td style="width:3cm;">Fecha Final:</td>
                                <td style="width:10%;">
                                    <input type="text" v-model="fechaFin" readonly>
                                </td>
                            </tr>

                          
                        </table>

						<table>
							<tr>
								<td class="titulos" colspan="20">.: Detalles informe</td>
							</tr>
						</table>
						<div class='subpantalla estilos-scroll' style='height:56vh; margin-top:0px; '>
                            <table>
                                <thead>
                                    <tr style="text-align:Center;">
										<th class="titulosnew00">Tipo comprobante</th>
                                        <th class="titulosnew00" style="width:20%;">Consecutivo</th>
                                        <th class="titulosnew00" style="width:20%;">Fecha</th>
										<th class="titulosnew00" style="width:20%;">Valor</th>
										<th class="titulosnew00" style="width:10%;">Reflejar</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr v-for="(det,index) in detalles" v-bind:class="det[0] === 'T' ? 'saludo2a' : 'saludo1a'" v-on:dblclick="seleccionaDetalle(det)">
										<td style="text-align:center; ">{{ det[1] }}</td>
										<td style="width:20%; text-align:center; ">{{ det[2] }}</td>
										<td style="width:20%; text-align:center; ">{{ det[3] }}</td>
										<td style="width:20%; text-align:center; ">{{ formatonumero(det[4]) }}</td>
										
										<td style="width:10%; text-align:center; ">
											<button v-show = "(det[0] != 'T' && det[1] != 'Adición' && det[1] != 'Reducción')" type="button" class="button-reflejar" v-on:click="reflejar(det)">Reflejar</button>
										</td>

										<input type='hidden' name='tipo[]' v-model="det[0]">
										<input type='hidden' name='tipoComprobante[]' v-model="det[1]">
										<input type='hidden' name='consecutivo[]' v-model="det[2]">
										<input type='hidden' name='fecha[]' v-model="det[3]">
										<input type='hidden' name='valor[]' v-model="det[4]">
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>

					<div id="cargando" v-if="loading" class="loading">
						<span>Cargando...</span>
					</div>
				</article>
			</section>
		</form>

		<script src="node_modules/read-excel-file/bundle/read-excel-file.min.js"></script>
		<script src="node_modules/xlsx/dist/xlsx.full.min.js"></script>
		<script src="Librerias/vue/vue.min.js"></script>
		<script src="Librerias/vue/axios.min.js"></script>
		<script src="presupuesto_ccpet/ejecucionIngresos/ccp-ejecuIngresosAuxiliar.js?"></script>
        
	</body>
</html>