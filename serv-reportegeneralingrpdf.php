<?php
	require_once("tcpdf2/tcpdf_include.php");
	require('comun.inc');
	require"funciones.inc";
	session_start();
	class MYPDF extends TCPDF 
	{
		public function Header() 
		{
			$linkbd=conectar_bd();
			$sqlr="SELECT nit, razonsocial FROM configbasica WHERE estado='S'";
			$resp=mysql_query($sqlr,$linkbd);
			while($row=mysql_fetch_row($resp)){$nit=$row[0];$rs=utf8_encode(strtoupper($row[1]));}
			$this->Image('imagenes/eng.jpg', 25, 10, 25, 23.9, 'JPG', '', 'T', true, 300, '', false, false, 0, false, false, false);// Logo
			$this->SetFont('helvetica','B',8);
			$this->SetY(10);
			$this->RoundedRect(10, 10, 200, 31, 2.5,''); //Borde del encabezado
			$this->Cell(52,31,'','R',0,'L'); //Linea que separa el encabazado verticalmente
			$this->SetY(32.5);
			$this->Cell(52,5,''.$rs,0,0,'C',false,0,1,false,'T','B'); //Nombre Municipio
			$this->SetFont('helvetica','B',8);
			$this->SetY(36.5);
			$this->Cell(52,5,''.$nit,0,0,'C',false,0,1,false,'T','C'); //Nit
			$this->SetFont('helvetica','B',14);
			$this->SetY(10);
			$this->SetX(62);
			$this->Cell(116,17,'REPORTE GENERAL INGRESOS',1,0,'C'); 
			$this->SetFont('helvetica','I',10);
			$this->SetY(27);
			$this->SetX(62);
			$this->Cell(116,7,"",'T',0,'L',false,0,1); 
			$this->SetY(31.2);
			$this->SetX(62);
			$this->Cell(116,7,"",0,0,'L',false,0,1);
			$this->SetFont('helvetica','B',9);
			$this->SetY(10);
			$this->SetX(178);
			$this->Cell(37.8,30.7,'','L',0,'L');
			$this->SetY(29);
			$this->SetX(178.5);
			$this->Cell(35,5," FECHA: ".date("d-m-Y"),0,0,'L');
			$this->SetY(34);
			$this->SetX(178.5);
			$this->Cell(35,5," HORA: ".date('h:i:s a'),0,0,'L');
			//-----------------------------------------------------
			$this->SetY(44);
			$this->Cell(15,5,'Vigencia',1,0,'C',false,0,0,false,'T','C');
			$this->Cell(20,5,'Mes',1,0,'C',false,0,0,false,'T','C');
			$this->Cell(60,5,'Servicio',1,0,'C',false,0,0,false,'T','C');
			$this->Cell(35,5,'Subsidio',1,0,'C',false,0,0,false,'T','C');
			$this->Cell(35,5,'Recaudo',1,0,'C',false,0,0,false,'T','C');
			$this->Cell(35,5,'Ingresos',1,0,'C',false,0,0,false,'T','C');
		}
		public function Footer() 
		{
			$linkbd=conectar_bd();
			$sqlr="SELECT direccion,telefono,web,email FROM configbasica WHERE estado='S'";
			$resp=mysql_query($sqlr,$linkbd);
			while($row=mysql_fetch_row($resp))
			{
				$direcc=utf8_encode(strtoupper($row[0]));
				$telefonos=$row[1];
				$dirweb=utf8_encode(strtoupper($row[3]));
				$coemail=utf8_encode(strtoupper($row[2]));
			}
			if($direcc!=''){$vardirec="Dirección: $direcc, ";}
			else {$vardirec="";}
			if($telefonos!=''){$vartelef="Telefonos: $telefonos";}
			else{$vartelef="";}
			if($dirweb!=''){$varemail="Email: $dirweb, ";}
			else {$varemail="";}
			if($coemail!=''){$varpagiw="Pagina Web: $coemail";}
			else{$varpagiw="";}
			$this->SetY(-16);
			$this->SetFont('helvetica', 'BI', 8);
			$txt = <<<EOD
$vardirec $vartelef
$varemail $varpagiw
EOD;
			$this->Write(0, $txt, '', 0, 'C', true, 0, false, false, 0);
			$this->SetY(-13);
			$this->Cell(0, 10, 'Page '.$this->getAliasNumPage().'/'.$this->getAliasNbPages(), 0, false, 'C', 0, '', 0, false, 'T', 'M');
			$this->Line(10, 260, 210, 260,$styleline);
		}
	}
	$pdf = new MYPDF('P','mm','Letter', true, 'iso-8859-1', false);// create new PDF document
	$pdf->SetDocInfoUnicode (true); 
	// set document information
	$pdf->SetCreator(PDF_CREATOR);
	$pdf->SetAuthor('G&CSAS');
	$pdf->SetTitle('Reporte General Recaudos');
	$pdf->SetSubject('RP General');
	$pdf->SetKeywords('TCPDF, PDF, example, test, guide');
	$pdf->SetMargins(10, 49, 10);// set margins
	$pdf->SetHeaderMargin(49);// set margins
	$pdf->SetFooterMargin(20);// set margins
	$pdf->SetAutoPageBreak(TRUE, 20);// set auto page breaks
	// set some language-dependent strings (optional)
	if (@file_exists(dirname(__FILE__).'/lang/spa.php')) 
	{
		require_once(dirname(__FILE__).'/lang/spa.php');
		$pdf->setLanguageArray($l);
	}
	// ---------------------------------------------------------
	$pdf->AddPage();
	$pdf->SetFont('helvetica','I',9);
	$linkbd=conectar_bd();
	$crips01="";
	if($_POST[cdc]!=""){$crips01="AND T2.servicio='$_POST[cdc]'";}
	$sqlr="SELECT T1.mes,T2.servicio,T1.fecha,T1.mesfin,SUM(T2.subsidio) FROM servliquidaciones T1, servliquidaciones_det T2 WHERE  T1.id_liquidacion=T2.id_liquidacion AND T1.vigencia='$_POST[vigencia]' AND (CAST(T1.mes as UNSIGNED) BETWEEN $_POST[mesini] AND $_POST[mesfin]) $crips01 GROUP BY T1.mes,T2.servicio ORDER BY CAST(T1.mes as UNSIGNED)";
	$resp = mysql_query($sqlr,$linkbd);
	$row=mysql_num_rows($resp);
	$totalfac1=0;
	while ($row =mysql_fetch_row($resp)) 
	{
		$totalfac1+=round($row[4], 0, PHP_ROUND_HALF_UP);
		$vsubsi[(integer)$row[1]][(integer)$row[0]]=round($row[4], 0, PHP_ROUND_HALF_UP);
	}
	$crips01="";
	if($_POST[cdc]!=""){$crips01="AND T2.ingreso='$_POST[cdc]'";}
	$sqlr="SELECT MONTH(T1.fecha),T2.ingreso, SUM(T2.valor) FROM servreciboscaja T1, servreciboscaja_det T2 WHERE T1.id_recibos=T2.id_recibos AND T1.estado != 'N' AND (T1.tipo='4' OR T1.tipo='5') AND YEAR(T1.fecha)='$_POST[vigencia]' AND (CAST(MONTH(T1.fecha) as UNSIGNED) BETWEEN $_POST[mesini] AND $_POST[mesfin]) $crips01 GROUP BY MONTH(T1.fecha),T2.ingreso ORDER BY CAST(MONTH(T1.fecha) as UNSIGNED)";
	$resp = mysql_query($sqlr,$linkbd);
	$row=mysql_num_rows($resp);
	$totalrec=0;
	while ($row =mysql_fetch_row($resp)) 
	{
		$totalrec+=$row[2];
		$vrecau[(integer)$row[1]][(integer)$row[0]]=$row[2];
	}
	$totalingreso=$totalfac1+$totalrec;
	if($_POST[cdc]=='')
	{
		$sqlr="SELECT MAX(CONVERT(codigo, SIGNED INTEGER)) FROM servservicios";
		$resp = mysql_query($sqlr,$linkbd);
		while ($row =mysql_fetch_row($resp)){$maximus=$row[0];}
		$sqlr="SELECT MIN(CONVERT(codigo, SIGNED INTEGER)) FROM servservicios";
		$resp = mysql_query($sqlr,$linkbd);
		while ($row =mysql_fetch_row($resp)){$minimus=$row[0];}
	}
	else{$minimus=$maximus=(integer)$_POST[cdc];}
	for($xm=(integer)$_POST[mesini];$xm<=(integer)$_POST[mesfin];$xm++)
	{
		for($xs=$minimus;$xs<=$maximus;$xs++)
		{
			$totalmes=$vsubsi[1][$xm]+$vrecau[1][$xm];
			$sqlrsv="SELECT nombre FROM servservicios WHERE codigo='0$xs'";
			$rowsv =mysql_fetch_row(mysql_query($sqlrsv,$linkbd));
			if ($concolor==0){$pdf->SetFillColor(200,200,200);$concolor=1;}
			else {$pdf->SetFillColor(255,255,255);$concolor=0;}
			$pdf->Cell(15,$altura,"$_POST[vigencia]",1,0,'C',true,0,0,false,'T','C');
			$pdf->Cell(20,$altura,mesletras($xm),1,0,'L',true,0,0,false,'T','C');
			$pdf->Cell(60,$altura," $rowsv[0]",1,0,'L',true,0,0,false,'T','C');
			$pdf->Cell(35,$altura,"$ ".number_format(($vsubsi[$xs][$xm]),0,",",".")." ",1,0,'R',true,0,0,false,'T','C');
			$pdf->Cell(35,$altura,"$ ".number_format(($vrecau[$xs][$xm]),0,",",".")." ",1,0,'R',true,0,0,false,'T','C');
			$pdf->Cell(35,$altura,"$ ".number_format($totalmes,0,',','.')." ",1,1,'R',true,0,0,false,'T','C');
		}
	}
		if ($concolor==0){$pdf->SetFillColor(200,200,200);$concolor=1;}
		else {$pdf->SetFillColor(255,255,255);$concolor=0;}
		$pdf->Cell(95,$altura,"Total:",1,0,'R',true,0,0,false,'T','C');
		$pdf->Cell(35,$altura,"$ ".number_format((float)$totalfac1,0,",",".")." ",1,0,'R',true,0,0,false,'T','C');
		$pdf->Cell(35,$altura,"$ ".number_format((float)$totalrec,0,",",".")." ",1,0,'R',true,0,0,false,'T','C');
		$pdf->Cell(35,$altura,"$ ".number_format((float)$totalingreso,0,",",".")." ",1,1,'R',true,0,0,false,'T','C');
		
	// ---------------------------------------------------------
	$pdf->Output('reportergeneralsubsidios.pdf', 'I');//Close and output PDF document
?>