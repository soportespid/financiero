<?php
    header("Cache-control: no-cache, no-store, must-revalidate");
    header("Content-Type: text/html;charset=utf8");
    require "comun.inc";
    require "funciones.inc";
    require 'funcionesSP.inc.php';
    session_start();
    $linkbd = conectar_v7();
    $linkbd -> set_charset("utf8");
    cargarcodigopag($_GET['codpag'],$_SESSION["nivel"]);
    date_default_timezone_set("America/Bogota");
    $scroll = $_GET['scrtop'];
	$totreg = $_GET['totreg'];
	$idcta  = $_GET['idcta'];
	$altura = $_GET['altura'];
	$filtro = "'".$_GET['filtro']."'";
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>:: IDEAL 10 - Parametrización</title>
    <link href="css/css2n.css" rel="stylesheet" type="text/css"/>
    <link href="css/css3n.css" rel="stylesheet" type="text/css"/>
    <link href="css/css4.css" rel="stylesheet" type="text/css"/>
    <link href="css/cssSP.css" rel="stylesheet" type="text/css"/>
    <script type="text/javascript" src="css/programas.js"></script>
    <script type="text/javascript" src="css/calendario.js"></script>
    <?php titlepag();?>

    <script>
        function despliegamodal2(_valor)
        {
            document.getElementById("bgventanamodal2").style.visibility=_valor;
            if(_valor=="hidden"){document.getElementById('ventana2').src="";}
            else
            {document.getElementById('ventana2').src="aesgpriet-ventana.php";}
        }

			function despliegamodalm(_valor,_tip,mensa,pregunta)
			{
				document.getElementById("bgventanamodalm").style.visibility=_valor;

				if(_valor=="hidden")
				{
					document.getElementById('ventanam').src="";
				}
				else
				{
					switch(_tip)
					{
						case "1":
							document.getElementById('ventanam').src="ventana-mensaje1.php?titulos="+mensa;
						    break;

						case "2":
							document.getElementById('ventanam').src="ventana-mensaje3.php?titulos="+mensa;
						    break;

						case "3":
							document.getElementById('ventanam').src="ventana-mensaje2.php?titulos="+mensa;
						    break;

						case "4":
							document.getElementById('ventanam').src="ventana-consulta1.php?titulos="+mensa+"&idresp="+pregunta;
						    break;
					}
				}
			}

			//ventanas servicios publicos
            function respuestaModalBusqueda2()
			{

            }

			function funcionmensaje() {
                document.form2.submit();
			}

			function respuestaconsulta(pregunta)
			{
				switch(pregunta)
				{
					case "1":
						document.form2.oculto.value='2';
						document.form2.submit();
					break;
				}
			}

			function guardar() {
                var nombre = document.getElementById('nombre').value;
                var unidadEjecutora = document.getElementById('unidadEjecutora').value;

				if (nombre.trim() != '' && unidadEjecutora != '-1') {
					despliegamodalm('visible','4','Esta Seguro de Guardar','1');
                }
				else {

                    despliegamodalm('visible','2','Falta información por completar');
                }
			}

			function actualizar()
			{
				document.form2.submit();
			}

            function iratras(scrtop, numpag, limreg, filtro)
			{
				var idcta = document.getElementById('codigo').value;
				location.href="ccpet-buscaSeccionPresupuestal.php?idcta="+idcta+"&scrtop="+scrtop+"&numpag="+numpag+"&limreg="+limreg+ "&filtro="+filtro;
			}

            function adelante(scrtop, numpag, limreg, filtro)
			{
				var maximo = document.getElementById('maximo').value;
				var actual = document.getElementById('codigo').value;
				actual = parseFloat(actual) + 1;
				if(actual <= parseFloat(maximo))
				{
					if(actual < 10)
					{
						actual = "0" + actual;
					}

					location.href="ccpet-editaSeccionPresupuestal.php?idban=" +actual+ "&scrtop=" +scrtop+ "&numpag=" +numpag+ "&limreg=" +limreg+ "&filtro=" +filtro;
				}
			}

			function atrasc(scrtop, numpag, limreg, filtro, prev)
			{
				var minimo = document.getElementById('minimo').value;
				var actual = document.getElementById('codigo').value;
				actual = parseFloat(actual)-1;
				if(actual >= parseFloat(minimo))
				{
					if(actual < 10)
					{
						actual = "0" + actual;
					}

					location.href="ccpet-editaSeccionPresupuestal.php?idban=" +actual+ "&scrtop=" +scrtop+ "&numpag=" +numpag+ "&limreg=" +limreg+ "&filtro=" +filtro;
				}
			}

            function validacodigo() {

                var codigo = document.getElementById('codigo').value;

                if (codigo.trim() != '') {
                    document.form2.oculto.value='3';
				    document.form2.submit();
                }
            }

            function cambiocheck()
			{
				if(document.getElementById('myonoffswitch').value=='S'){document.getElementById('myonoffswitch').value='N';}
				else{document.getElementById('myonoffswitch').value='S';}
				document.form2.submit();
			}
    </script>
</head>
<body>
    <IFRAME src="alertas.php" name="alertas" id="alertas" style="display:none"></IFRAME>
    <span id="todastablas2"></span>
    <?php
        $numpag = @$_GET['numpag'];
        $limreg = @$_GET['limreg'];
        $scrtop = 26 * $totreg;
    ?>
    <table>
        <tr><script>barra_imagenes("para");</script><?php cuadro_titulos();?></tr>

        <tr><?php menu_desplegable("para");?></tr>

        <tr>
            <td colspan="3" class="cinta">
                <a href="ccpet-crearSeccionPresupuestal.php" class="mgbt"><img src="imagenes/add.png"/></a>

                <a onclick="guardar()" class="mgbt"><img src="imagenes/guarda.png" title="Guardar"/></a>

                <a href="ccpet-buscaSeccionPresupuestal.php" class="mgbt"><img src="imagenes/busca.png" title="Buscar"/></a>

                <a onclick="mypop=window.open('plan-agenda.php','','');mypop.focus()" class="mgbt"><img src="imagenes/agenda1.png" title="Agenda" /></a>

                <a onclick="mypop=window.open('para-principal.php','','');mypop.focus();" class="mgbt"><img src="imagenes/nv.png" title="Nueva ventana"></a>

                <a onclick="mypop=window.open('<?php echo $url2; ?>','','');mypop.focus();" class="mgbt"><img src="imagenes/duplicar_pantalla.png" title="Duplicar pesta&ntilde;a"></a>

                <a onClick="iratras(<?php echo "$scrtop, $numpag, $limreg, $filtro"; ?>)" class="mgbt"><img src="imagenes/iratras.png" title="Atr&aacute;s"></a>
            </td>
        </tr>
    </table>

    <div id="bgventanamodalm" class="bgventanamodalm">
        <div id="ventanamodalm" class="ventanamodalm">
            <IFRAME src="" name="ventanam" marginWidth=0 marginHeight=0 frameBorder=0 id="ventanam" frameSpacing=0 style=" width:700px; height:130px; top:200; overflow:hidden;"></IFRAME>
        </div>
    </div>

    <form name="form2" method="post" action="">
        <?php
            if(@$_POST['oculto'] == "")
            {
                $sqlr = "SELECT MIN(id_seccion_presupuestal), MAX(id_seccion_presupuestal) FROM pptoseccion_presupuestal";
                $res = mysqli_query($linkbd,$sqlr);
                $r = mysqli_fetch_row($res);

                $_POST['minimo'] = $r[0];
                $_POST['maximo'] = $r[1];

                $sqlr = "SELECT id_seccion_presupuestal, id_unidad_ejecutora, nombre, estado, aesgpriet FROM pptoseccion_presupuestal WHERE id_seccion_presupuestal = '$_GET[idban]' ";
                $resp = mysqli_query($linkbd,$sqlr);
                $row = mysqli_fetch_row($resp);

                $_POST['codigo'] = $row[0];
                $_POST['unidadEjecutora'] = $row[1];
                $_POST['nombre'] = $row[2];
                $_POST['onoffswitch'] = $row[3];
                $_POST['aesgpriet'] = $row[4];

                $sqlrEt = "SELECT nombre_entidad FROM aesgpriet WHERE entidad = '$row[4]'";
                $respEt = mysqli_query($linkbd,$sqlrEt);
                $rowEt = mysqli_fetch_row($respEt);
                $_POST['nAesgpriet'] = $rowEt[0];
            }

            if (@$_POST['oculto'] == 3) {

                $query = "SELECT id_seccion_presupuestal FROM pptoseccion_presupuestal WHERE id_seccion_presupuestal = '$_POST[codigo]'";
                $resp = mysqli_query($linkbd,$query);
                $row = mysqli_fetch_row($resp);

                if ($row[0] != '') {
                    echo"<script>despliegamodalm('visible','2','Codigo duplicado');</script>";
                    $_POST['codigo'] = '';
                }
            }
        ?>
        <div>
            <table class="inicio">
                <tr>
                    <td class="titulos" colspan="6">.: Crear Secci&oacute;n Presupuestal</td>

                    <td class="cerrar" style="width:7%" onclick="location.href='ccpet-principal.php'">Cerrar</td>
                </tr>

                <tr>
                    <td class="tamano01" style="width:3cm; text-align:center;">Codigo:</td>
                    <td style="width: 15%;">
                        <a onClick="atrasc(<?php echo "$scrtop, $numpag, $limreg, $filtro"; ?>)" class="icobut" title="Anterior"><img src="imagenes/back.png"/></a>
                        <input type="text" name="codigo" id="codigo" value="<?php echo $_POST['codigo'] ?>" style="width:65%; text-align:center;" onblur="validacodigo();">
                        <a onClick="adelante(<?php echo "$scrtop, $numpag, $limreg, $filtro" ?>);" class="icobut" title="Sigiente"><img src="imagenes/next.png"/></a>
                    </td>

                    <td class="tamano01" style="width:3cm; text-align:center;">Nombre:</td>
                    <td colspan="3">
                        <input type="text" name="nombre" id="nombre" value="<?php echo $_POST['nombre'] ?>" style="width:98%;">
                    </td>
                </tr>

                <tr>
                    <td class="tamano01" style="width:3cm; text-align:center;">Unidad Ejecutora:</td>
                    <td style="width: 15%">
						<select name="unidadEjecutora" id="unidadEjecutora" class="centrarSelect" style="width:100%;">
                            <option value="-1">SELECCIONE UNIDAD EJECUTORA</option>
							<?php
                                $query = "SELECT id_cc, nombre FROM pptouniejecu WHERE estado = 'S'";
                                $resp = mysqli_query($linkbd,$query);
                                while ($row = mysqli_fetch_row($resp)) {

                                    if(@$_POST['unidadEjecutora'] == $row[0]) {
                                        echo "<option value='$row[0]' SELECTED>$row[0] - $row[1]</option>";
                                    }
                                    else {
                                        echo "<option value='$row[0]'>$row[0] - $row[1]</option>";
                                    }
                                }
							?>
						</select>
					</td>

                    <td class="tamano01" style="width:15%;">AESGPRI ET: </td>
					<td  style="width: 15%">
                        <input type="text" id="aesgpriet" name="aesgpriet" class="tamano02" onKeyUp="return tabular(event,this)" value="<?php echo @ $_POST['aesgpriet']?>" style="background-color:#40f3ff;width:98%;height:35px!important;" title="Doble Click Listado Cuentas" onDblClick="despliegamodal2('visible');"/>

                    </td>
					<td>
                        <input type="text" name="nAesgpriet" id="nAesgpriet" value="<?php echo @ $_POST['nAesgpriet'];?>" style="width:98%;height:35px!important;" class="tamano02" readonly/>
                    </td>

                    <td class="tamano01">Estado:</td>

					<td>
						<div class="onoffswitch">
							<input type="checkbox" name="onoffswitch" class="onoffswitch-checkbox" id="myonoffswitch" value="<?php echo @ $_POST['onoffswitch'];?>" <?php if(@ $_POST['onoffswitch']=='S'){echo "checked";}?> onChange="cambiocheck();"/>
							<label class="onoffswitch-label" for="myonoffswitch">
								<span class="onoffswitch-inner"></span>
								<span class="onoffswitch-switch"></span>
							</label>
						</div>
					</td>
                </tr>
            </table>

            <input type="hidden" name="maximo" id="maximo" value="<?php echo @$_POST['maximo']?>"/>
			<input type="hidden" name="minimo" id="minimo" value="<?php echo @$_POST['minimo']?>"/>
            <input type="hidden" name="oculto" id="oculto" value="1"/>
        </div>
        <?php
            if(@$_POST['oculto'] == "2")
            {
                if (@ $_POST['onoffswitch']!='S'){$valest='N';}
				else {$valest='S';}

                $query = "UPDATE pptoseccion_presupuestal SET id_seccion_presupuestal = '$_POST[codigo]', id_unidad_ejecutora = '$_POST[unidadEjecutora]', nombre = '$_POST[nombre]', estado = '$valest', aesgpriet = '$_POST[aesgpriet]' WHERE id_seccion_presupuestal = '$_GET[idban]'";
                if (mysqli_query($linkbd,$query)) {
                    echo "<script>despliegamodalm('visible','1','Se ha editado con exito');</script>";
                }
                else {
                    echo"<script>despliegamodalm('visible','2','No se pudo editar con exito');</script>";
                }
            }
        ?>
    </form>

    <div id="bgventanamodal2">
        <div id="ventanamodal2">
            <IFRAME  src="" name="buster" marginWidth=0 marginHeight=0 frameBorder=0 id="ventana2" frameSpacing=0 style="left:500px; width:900px; height:500px; top:200;"></IFRAME>
        </div>
    </div>
</body>
</html>
