<?php
	header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=utf8");

	require "comun.inc";
	require "funciones.inc";
	require "conversor.php";
	require "validaciones.inc";

	$linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");

	session_start();
	date_default_timezone_set("America/Bogota");
   if(!isset($_SESSION["usuario"]))
   {
      //*** verificar el username y el pass
      $users=$_POST['user'];
      $pass=$_POST['pass'];
      $link=conectar_v7();
      $link -> set_charset("utf8");
      $sqlr="Select usuarios.nom_usu,roles.nom_rol, usuarios.id_rol, usuarios.id_usu, usuarios.foto_usu, usuarios.usu_usu from usuarios, roles where usuarios.usu_usu='$users' and usuarios.pass_usu='$pass' and usuarios.id_rol=roles.id_rol and usuarios.est_usu='1'";
      //echo $sqlr;
      $res=mysqli_query($link,$sqlr);
      while($r=mysqli_fetch_row($res))
      {
         $user=$r[0];
         $perf=$r[1];
         $niv=$r[2];
         $idusu=$r[3];
         $nick=$r[5];
         $dirfoto=$r[4];
 	   }
      if($user == "")
      {
        header("location: index2.php");
      }
      else
      {
         //login correcto
         //session_start();
         $_SESSION["usuario"]=array();
         //session_register("usuario");
         //$usuario=array();
         $_SESSION["usuario"]=$user;
         $_SESSION["perfil"]=$perf;
         $_SESSION["idusuario"]=$idusu;
         $_SESSION["nickusu"]=$nick;
         //session_register("perfil");
         //$perfil="";
         //$perfil=$perf;
         $_SESSION["nivel"]=$niv;
         //session_register("nivel");
         //$nivel="";
         //$nivel=$niv;
         //$niv=$_SESSION["nivel"];
         $_SESSION["linksetad"]=array();
         if($dirfoto=="")
         {
            $dirfoto="blanco.png";
         }
	      $_SESSION["fotousuario"]="fotos/".$dirfoto;
         //******************* menuss ************************
  	      $sqlr="";
         $sqlr="Select DISTINCT (opciones.nom_opcion),opciones.ruta_opcion, opciones.niv_opcion,opciones.comando  from rol_priv, opciones where rol_priv.id_rol=$niv and opciones.id_opcion=rol_priv.id_opcion group by (opciones.nom_opcion), opciones.ruta_opcion, opciones.niv_opcion,opciones.comando  order by opciones.orden";

         $linksetad[$x]="";
         $res=mysqli_query($link,$sqlr);
	      while($roww=mysqli_fetch_row($res))
         {
	         $_SESSION['linksetad'][$roww[2]].='<li> <a onClick="location.href=\''.$roww[1].'\'" style="cursor:pointer;">'.$roww[0].' <span style="float:right">'.$roww[3].'</span></a></li>';
	      }
      }
   }
   else
   {
      $link=conectar_v7();
      $link -> set_charset("utf8");
      $_SESSION["linksetad"]=array();
      $niv=$_SESSION["nivel"];
      $sqlr="";
      $sqlr="Select DISTINCT (opciones.nom_opcion),opciones.ruta_opcion, opciones.niv_opcion,opciones.comando  from rol_priv, opciones where rol_priv.id_rol=$niv and opciones.id_opcion=rol_priv.id_opcion and opciones.modulo=0 group by (opciones.nom_opcion), opciones.ruta_opcion, opciones.niv_opcion,opciones.comando  order by opciones.orden";
      //  echo "sql=".$sqlr;
      $linksetad[$x]="";
      $res=mysqli_query($link,$sqlr);
      while($roww=mysqli_fetch_row($res))
      {
	      $_SESSION['linksetad'][$roww[2]].='<li> <a onClick="location.href=\''.$roww[1].'\'" style="cursor:pointer;">'.$roww[0].' <span style="float:right">'.$roww[3].'</span></a></li>';
         //echo "<br>".$_SESSION[linkset][$roww[2]];
	   }
   }
   //**niveles menu: Administracion (0) - Consultas (1) - Herramientas (2) - Reportes (3)
   ?>
   <!DOCTYPE >
   <html lang="es">
      <head>
         <meta charset="utf-8"/>
         <meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1"/>
         <meta name="viewport" content="user-scalable=no">
         <title>:: IDEAL 10 - Administraci&oacute;n</title>
		   <link href="favicon.ico" rel="shortcut icon"/>
         <link href="css/css2.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		   <link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
         <script type="text/javascript" src="css/programas.js"></script>
         <link href="css/style.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css"/>
      </head>
      <body>
	      <IFRAME src="alertas.php" name="alertas" id="alertas" style="display:none"></IFRAME>
	      <span id="todastablas2"></span>
         <table>
               <tr><script>barra_imagenes("adm");</script><?php cuadro_titulos();?></tr>
            <tr><?php menu_desplegable("adm");?></tr>
         </table>
      </body>
</html>
