<?php //V 1000 12/12/16 ?> 
<?php
	ini_set('max_execution_time',3600);
	require "comun.inc";
	require "funciones.inc";
	require "validaciones.inc";
	session_start();
	$linkbd=conectar_bd();	
	cargarcodigopag($_GET[codpag],$_SESSION["nivel"]);
	header("Cache-control: private"); // Arregla IE 6
	date_default_timezone_set("America/Bogota");
?>
<!DOCTYPE >
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="es">
	<head>
	 	<meta http-equiv="Content-Type" content="text/html" charset="iso-8859-1"/>
        <meta http-equiv="X-UA-Compatible" content="IE=9"/>
		<title>:: SPID - Presupuesto</title>
        <link href="css/css2.css" rel="stylesheet" type="text/css" />
        <link href="css/css3.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="css/calendario.js"></script>
		<script type="text/javascript" src="css/programas.js"></script>
<script>
function buscacta(e)
 {
if (document.form2.cuenta.value!="")
{
 document.form2.bc.value=2;
 document.form2.submit();
 }
 }
//************* ver reporte ************
//***************************************
function verep(idfac)
{
  document.form1.oculto.value=idfac;
  document.form1.submit();
  }
//************* genera reporte ************
//***************************************
function genrep(idfac)
{
  document.form2.oculto.value=idfac;
  document.form2.submit();
  }
function pdf()
{
document.form2.action="pdfejecuciongastos.php";
document.form2.target="_BLANK";
document.form2.submit(); 
document.form2.action="";
document.form2.target="";
}
function excell()
{
document.form2.action="presu-ejecuciongastosexcel.php";
document.form2.target="_BLANK";
document.form2.submit(); 
document.form2.action="";
document.form2.target="";
}
function agregardetalled()
{
if(document.form2.retencion.value!="" )
{ 
				document.form2.agregadetdes.value=1;
	//			document.form2.chacuerdo.value=2;
				document.form2.submit();
 }
 else {
 alert("Seleccione una retencion");
 }
}
function eliminard(variable)
{
if (confirm("Esta Seguro de Eliminar"))
  {
document.form2.eliminad.value=variable;
//eli=document.getElementById(elimina);
vvend=document.getElementById('eliminad');
//eli.value=elimina;
vvend.value=variable;
document.form2.submit();
}
}
function validar(){document.form2.submit();}
function crear(){document.form2.genbal.value=1;document.form2.gbalance.value=0;document.form2.submit();}
function guardarbalance(){document.form2.gbalance.value=1;document.form2.submit();}
function guardarchip(){document.form2.gchip.value=1; document.form2.submit();}
function cargarotro(){document.form2.cargabal.value=1; document.form2.submit();}
function checktodos()
			{
				cali=document.getElementsByName('ctes[]');
				for (var i=0;i < cali.length;i++) 
				{ 
					if (document.getElementById("todoscte").checked == true)
					{cali.item(i).checked = true;document.getElementById("todoscte").value=1;}
					else{cali.item(i).checked = false;document.getElementById("todoscte").value=0;}
				}	
			}
			function checktodosn()
			{
				cali=document.getElementsByName('nctes[]');
				for (var i=0;i < cali.length;i++) 
				{ 
					if (document.getElementById("todoscten").checked == true) 
					{cali.item(i).checked = true;document.getElementById("todoscten").value=1;	 }
					else{cali.item(i).checked = false;document.getElementById("todoscten").value=0;}
				}	
			}
</script>
		<?php titlepag();?>
    </head>
    <body>
        <IFRAME src="alertas.php" name="alertas" id="alertas" style="display:none"></IFRAME>
        <span id="todastablas2"></span>
        <table>
            <tr><script>barra_imagenes("presu");</script><?php cuadro_titulos();?></tr>	 
            <tr><?php menu_desplegable("presu");?></tr>
        	<tr>
			<?php
                $informes=array();
                $informes[1]="F05-Registros_PresupuestalesCDM";
                $informes[2]="F06-IngresosCDM";
                $informes[3]="F07-FuentedeRecursosCDM";
                $informes[4]="F08-ContratoPrincipalCDM";
                $informes[5]="F17A-MesaRendirCDM(ESTAMPILLAS PROCULTURA)";
                $informes[6]="F17B-MesaRendirCDM(ESTAMPILLAS PROTURISMO)";
                $informes[7]="F17C-MesaRendirCDM(ESTAMPILLAS PRODESARROLLO)";
                $informes[8]="F17D-MesaRendirCDM(ESTAMPILLAS PROUNILLANOS)";
                $informes[9]="F18-EntidadCDM";
                $informes[10]="F20-1A-NITCDM";
                $informes[11]="F20-1B-NITCDM";
                $informes[12]="F20-1C-NITConsorcioCDM";
                $informes[13]="F20-2AGR-EntidadFiducianteCDM";
                $informes[14]="F25-VigenciasCDM";
                $informes[15]="SIA";
            ?>
  			<td colspan="3" class="cinta"><a href="#" class="mgbt"><img src="imagenes/add.png" title="Nuevo" /></a>
            <a href="#" class="mgbt" onClick="document.form2.submit();"><img src="imagenes/guarda.png" title="Guardar"/></a>
            <a href="#" onClick="document.form2.submit()" class="mgbt"><img src="imagenes/busca.png" title="Buscar" /></a>
            <a href="#" onClick="mypop=window.open('presu-principal.php','','');mypop.focus();" class="mgbt"><img src="imagenes/nv.png" title="Nueva Ventana"></a>
            <a href="#" onClick="pdf()" class="mgbt"><img src="imagenes/print.png" title="imprimir"></a>
            <a href="<?php echo "archivos/FORMATO_".$informes[$_POST[reporte]].".csv"; ?>" target="_blank" class="mgbt"><img src="imagenes/csv.png" title="csv"></a><a href="descargartxt.php?id=<?php echo $informes[$_POST[reporte]].".txt"; ?>&dire=archivos" target="_blank" class="mgbt"><img src="imagenes/contraloria.png"  title="contraloria"></a></td>
			</tr>
		</table>
 <form name="form2" method="post" action="presu-informescdm.php">
 <?php
 $vigusu=vigencia_usuarios($_SESSION[cedulausu]);
 
 if($_POST[bc]!='')
			 {
			  $nresul=buscacuentapres($_POST[cuenta],2);

			  if($nresul!='')
			   {
			  $_POST[ncuenta]=$nresul;
   			  $linkbd=conectar_bd();
			  $sqlr="select *from pptocuentaspptoinicial where cuenta=$_POST[cuenta] and vigencia=".$vigusu;
			  $res=mysql_query($sqlr,$linkbd);
			  $row=mysql_fetch_row($res);
			  $_POST[valor]=$row[5];		  
			  $_POST[valor2]=$row[5];		  			  

			   }
			  else
			  {
			   $_POST[ncuenta]="";	
			   }
			 }
			 
 ?>
    <table  align="center" class="inicio" >
      <tr >
        <td class="titulos" colspan="6">.: Reportes CDM</td>
        <td width="74" class="cerrar"><a href="presu-principal.php">Cerrar</a></td>
      </tr>
      <tr  ><td class="saludo1">Reporte</td>
      <td>
       <select name="reporte" id="reporte">
       <option value="-1">Seleccione ....</option>
          <option value="1" <?php if($_POST[reporte]=='1') echo "selected" ?>>F05 - Registros Presupuestales CDM</option>
          <option value="2" <?php if($_POST[reporte]=='2') echo "selected" ?>>F06 - Ingresos CDM</option>
          <option value="3" <?php if($_POST[reporte]=='3') echo "selected" ?>>F07 - Fuente de Recursos CDM</option>
          <option value="4" <?php if($_POST[reporte]=='4') echo "selected" ?>>F08 - Contrato Principal CDM</option>
          <option value="5" <?php if($_POST[reporte]=='5') echo "selected" ?>>F17A - Mes a Rendir CDM (ESTAMPILLAS PROCULTURA)</option>
          <option value="6" <?php if($_POST[reporte]=='6') echo "selected" ?>>F17B - Mes a Rendir CDM (ESTAMPILLAS PROTURISMO)</option>
          <option value="7" <?php if($_POST[reporte]=='7') echo "selected" ?>>F17C - Mes a Rendir CDM (ESTAMPILLAS PRODESARROLLO)</option>
          <option value="8" <?php if($_POST[reporte]=='8') echo "selected" ?>>F17D - Mes a Rendir CDM (ESTAMPILLAS PROUNILLANOS)</option>
          <option value="9" <?php if($_POST[reporte]=='9') echo "selected" ?>>F18 - Entidad CDM</option>
          <option value="10" <?php if($_POST[reporte]=='10') echo "selected" ?>>F20-1A - NIT CDM</option>
          <option value="11" <?php if($_POST[reporte]=='11') echo "selected" ?>>F20-1B - NIT CDM</option>
          <option value="12" <?php if($_POST[reporte]=='12') echo "selected" ?>>F20-1C - NIT Consorcio CDM</option>
          <option value="13" <?php if($_POST[reporte]=='13') echo "selected" ?>>F20-2AGR - Entidad Fiduciante CDM</option>
          <option value="14" <?php if($_POST[reporte]=='14') echo "selected" ?>>F25 - Vigencias CDM</option>
          <option value="15" <?php if($_POST[reporte]=='15') echo "selected" ?>>SIA</option>
        </select>
      </td> 
        <td width="88" class="saludo1">Fecha Inicial:</td>
        <td width="193"><input type="hidden" value="<?php echo $vigusu ?>" name="vigencias"><input name="fecha" type="text" id="fc_1198971545" title="DD/MM/YYYY" size="10" value="<?php echo $_POST[fecha]; ?>" onKeyUp="return tabular(event,this) " onKeyDown="mascara(this,'/',patron,true)"  maxlength="10">         <a href="#" onClick="displayCalendarFor('fc_1198971545');"><img src="imagenes/buscarep.png" align="absmiddle" border="0"></a>        </td>
        <td width="79" class="saludo1">Fecha Final: </td>
        <td width="613"><input name="fecha2" type="text" id="fc_1198971546" title="DD/MM/YYYY" size="10" value="<?php echo $_POST[fecha2]; ?>" onKeyUp="return tabular(event,this) " onKeyDown="mascara(this,'/',patron,true)"  maxlength="10"> <a href="#" onClick="displayCalendarFor('fc_1198971546');"><img src="imagenes/buscarep.png" align="absmiddle" border="0"></a>  <input type="button" name="generar" value="Generar" onClick="document.form2.submit()"> <input type="hidden" value="1" name="oculto"></td></tr>      
    </table>
     <?php
			//**** busca cuenta
			if($_POST[bc]!='')
			 {
			  $nresul=buscacuentapres($_POST[cuenta],2);
			  if($nresul!='')
			   {
			  $_POST[ncuenta]=$nresul;
			  $linkbd=conectar_bd();
			  $sqlr="select *from pptocuentaspptoinicial where cuenta=$_POST[cuenta] and vigencia=".$vigusu;
			  $res=mysql_query($sqlr,$linkbd);
			  $row=mysql_fetch_row($res);
			  $_POST[valor]=$row[5];		  
			  $_POST[valor2]=$row[5];		  			  
  			  ?>
			<script>
			  document.form2.fecha.focus();
			  document.form2.fecha.select();
			  </script>
			  <?php
			  }
			 else
			 {
			  $_POST[ncuenta]="";
			  ?>
	  <script>alert("Cuenta Incorrecta");document.form2.cuenta.focus();</script>
			  <?php
			  }
			 }
			 ?>
	<div class="subpantallap" style="height:65.5%; width:99.6%; overflow-x:hidden;">
  <?php
  //**** para sacar la consulta del balance se necesitan estos datos ********
  //**** nivel, mes inicial, mes final, cuenta inicial, cuenta final, cc inicial, cc final  
$oculto=$_POST['oculto'];
if($_POST[oculto])
{$linkbd=conectar_bd();
$iter='zebra1';
$iter2='zebra2';
  switch($_POST[reporte])
   {	
	case 1: //F05 - Registros Presupuestales CDM
			
		$crit1=" ";
		$crit2=" ";
		$crit3=" ";
		$crit4=" ";
		$crit5=" ";
		$_POST[nominforme]="F05-Registros_PresupuestalesCDM";
 		$namearch="archivos/FORMATO_F05-Registros_PresupuestalesCDM.csv";
		$_POST[nombrearchivo]=$namearch;
		$namearch2="archivos/".$informes[$_POST[reporte]].".txt";
		$Descriptor2 = fopen($namearch2,"w+"); 	
		$Descriptor1 = fopen($namearch,"w+"); 
		fputs($Descriptor1,"(N) RP;(C) Rubro;(F)Fecha;(D)Valor;(N)Numero De Cdp;(C)N� Contrato;(C)Objeto;(C) Contratista;(C) Nit Tercero\r\n");
		fputs($Descriptor2,"S\t".$_POST[codent]."\t".$_POST[periodo]."\t".$vigusu."\tREGISTROS_PRESUPUESTALES\r\n");
		if ($vigusu!="")
		$crit1=" and pptorp.vigencia ='$vigusu' ";
		if ($_POST[numero]!="")
		$crit2=" and pptorp.consvigencia like '%$_POST[numero]%' ";
		if ($_POST[fecha]!="" and $_POST[fecha2]!="" )
		{	ereg( "([0-9]{1,2})/([0-9]{1,2})/([0-9]{2,4})", $_POST[fecha],$fecha);
		$fechai=$fecha[3]."-".$fecha[2]."-".$fecha[1];
		ereg( "([0-9]{1,2})/([0-9]{1,2})/([0-9]{2,4})", $_POST[fecha2],$fecha);
		$fechaf=$fecha[3]."-".$fecha[2]."-".$fecha[1];
		$crit3=" and pptorp.fecha between '$fechai' and '$fechaf'  ";
		}
		$sqlr="select *from pptorp inner join pptorp_detalle 
		where pptorp.estado<>'N' and pptorp.estado!='R' and pptorp_detalle.estado!='R' and  pptorp.estado!=''".$crit1.$crit2.$crit3." 
		and pptorp.consvigencia=pptorp_detalle.consvigencia and pptorp.vigencia=".$vigusu." and pptorp_detalle.vigencia=".$vigusu." order by pptorp.consvigencia";
		echo $sqlr;
		$resp = mysql_query($sqlr,$linkbd);
		$ntr = mysql_num_rows($resp);
		$con=1;
		echo "<table class='inicio' align='center' width='80%'>
		<tr><td colspan='10' class='titulos'>.: F05 - REGISTROS PRESUPUESTALES CDM:</td></tr>
		<tr><td colspan='5'>Registro Presupuestal Encontrados: $ntr</td></tr>
		<tr><td class='titulos2'>RP</td><td class='titulos2' >Rubro</td>
		<td class='titulos2' >Nombre Rubro</td><td class='titulos2' >Fecha</td>
		<td class='titulos2' >Valor</td><td class='titulos2'>CDP</td>
		<td class='titulos2' >Contrato</td><td class='titulos2'>Objeto</td>
		<td class='titulos2'>Nit Tercero</td><td class='titulos2'>Tercero</td></tr>";	
		
		while ($row =mysql_fetch_row($resp)) 
		 {
		 $sqlr2="select pptocdp.objeto from pptocdp where pptocdp.consvigencia=$row[2] and pptocdp.vigencia=$row[0]";
		//echo $sqlr2."<br>";
		 $resp2 = mysql_query($sqlr2,$linkbd);
		 $r2 =mysql_fetch_row($resp2);
		 $nrub= buscacuentapres($row[16],2);
		 //echo $nrub."hola";
		 $tercero=buscatercero($row[5]);
		 if($nrub!="")
	 	 {
			ereg( "([0-9]{2,4})-([0-9]{1,2})-([0-9]{1,2})", $row[4],$fecha);
	$fechaf=$fecha[1]."/".$fecha[2]."/".$fecha[3]; 
			 echo "<tr class='$iter'>
			 <td >$row[1]</td>
			 <td >$row[16]</td>
		 	 <td >".str_replace(","," ",$nrub)."</td><td>".$fechaf."</td>
		 	<td >".number_format($row[18],0)."</td>
			<td >$row[2]</td>
		 	<td >".$row[8]."</td>
			<td >".str_replace(","," ",$r2[0])."</td>
		 	<td >$row[5]</td><td >".str_replace(","," ",$tercero)."</td></tr>";
		 	fputs($Descriptor1,$row[1].";".$row[16].";".$fechaf.";".number_format($row[18],2,".","").";".$row[2].";".$row[8].";".str_replace(","," ",$r2[0]).";".str_replace(","," ",$tercero).";".$row[5]."\r\n");
			fputs($Descriptor2,"D\t".$row[1]."\t".$row[16]."\t".$fechaf."\t".number_format($row[18],2,".","")."\t".$row[2]."\t1\t".$row[8]."\t".str_replace(","," ",$r2[0])."\t".str_replace(","," ",$tercero)."\t".$row[5]."\r\n");	
		 	$con+=1;
		 	$aux=$iter;
		 	$iter=$iter2;
		 	$iter2=$aux;
	 	 }
 	}
	echo"</table>";
	fclose($Descriptor1);
	break;
	
	//------------------------------------------------------------------------------------------------
	
	case 2:	//F06 - Ingresos CDM
	$crit3=" ";
	$crit4=" ";	
	$namearch="archivos/FORMATO_F06-IngresosCDM.csv";
	$_POST[nombrearchivo]=$namearch;
	$Descriptor1 = fopen($namearch,"w+");
	
	if ($_POST[fecha]!="" and $_POST[fecha2]!="" )
	{	ereg( "([0-9]{1,2})/([0-9]{1,2})/([0-9]{2,4})", $_POST[fecha],$fecha);
	$fechaf=$fecha[3]."-".$fecha[2]."-".$fecha[1];
	ereg( "([0-9]{1,2})/([0-9]{1,2})/([0-9]{2,4})", $_POST[fecha2],$fecha);
	$fechaf2=$fecha[3]."-".$fecha[2]."-".$fecha[1];	
	}
		
	 $sumareca=0;
			$sumarp=0;	
			$sumaop=0;	
			$sumap=0;			
			$sumai=0;
			$sumapi=0;				
			$sumapad=0;	
			$sumapred=0;	
			$sumapcr=0;	
			$sumapccr=0;						
			ereg( "([0-9]{1,2})/([0-9]{1,2})/([0-9]{2,4})", $_POST[fecha],$fecha);
			$fechaf=$fecha[3]."-".$fecha[2]."-".$fecha[1];
			ereg( "([0-9]{1,2})/([0-9]{1,2})/([0-9]{2,4})", $_POST[fecha2],$fecha);
			$fechaf2=$fecha[3]."-".$fecha[2]."-".$fecha[1];	
			$vigenciaTr = $fecha[3];
			$iter="zebra1";
			$iter2="zebra2";
			echo "<table class='inicio' ><tr><td colspan='9' class='titulos'>Ejecucion Cuentas $_POST[fecha] - $_POST[fecha2]</td></tr>";
			echo "<tr><td class='titulos2'>Cuenta</td><td class='titulos2'>Nombre</td><td class='titulos2'>Fuente</td><td class='titulos2'>PRES INI</td><td class='titulos2'>ADICIONES</td><td class='titulos2'>REDUCCIONES</td><td class='titulos2'>PRES DEF</td><td class='titulos2'>INGRESOS</td><td class='titulos2'>%</td></tr>";
			$linkbd=conectar_bd();
			if($_POST[vereg]==2)
				$cond=" AND pptocuentas.regalias='S'";
			else
				$cond="";
			$sqlr2="Select distinct pptocuentas.cuenta, pptocuentas.tipo, pptocuentas.vigencia, pptocuentas.vigenciaf from pptocuentas where (pptocuentas.vigencia='".$vigenciaTr."' or  pptocuentas.vigenciaf='".$vigenciaTr."') AND (pptocuentas.clasificacion='ingresos' or pptocuentas.clasificacion='reservas-ingresos') $cond ORDER BY pptocuentas.cuenta ASC ";
			$cuentaPadre=Array();
			$rescta=mysql_query($sqlr2,$linkbd);
			while ($row =mysql_fetch_row($rescta)) 
			{
				//$nc=buscacuentap($_POST[cuenta]);
				$pdef=0;
				$vitot=0;
				$todos=0;	 	 
				
				// echo "<br>".$ctaniv; 
				$nt=existecuentain($row[0]);	
				//$vitot=$vi+$vit+$viret+$vissf+$notas+$visinrec;
				//$vporcentaje=round(($vitot/$pdef)*100,2);
				$negrilla="style='font-weight:bold'";	  
				$tcta="0"; 
				if($row[1]=='Auxiliar' || $row[1]=='auxiliar')
				{
					$arregloCuenta=generaReporteIngresos($row[0],$row[2],$fechaf,$fechaf2);
					$cuentas[$row[0]]["numCuenta"]=$row[0];
					$cuentas[$row[0]]["nomCuenta"]=$nt;
					$cuentas[$row[0]]["presuInicial"]=$arregloCuenta[0];
					$cuentas[$row[0]]["adicion"]=$arregloCuenta[1];
					$cuentas[$row[0]]["reduccion"]=$arregloCuenta[2];
					$cuentas[$row[0]]["presuDefinitivo"]=$arregloCuenta[3];
					$cuentas[$row[0]]["ingreso_tot"]=$arregloCuenta[6];
					$cuentas[$row[0]]["porcentaje"]=round(($arregloCuenta[6]/$arregloCuenta[3])*100,2);
					$cuentas[$row[0]]["tipo"]="Auxiliar";

					/*$cuentas[$row[0]]["numCuenta"]=$row[0];
					$cuentas[$row[0]]["nomCuenta"]=$row[1];
					$cuentas[$row[0]]["disponibilidad"]=$arregloCuenta[8];
					$cuentas[$row[0]]["presuInicial"]=$arregloCuenta[0];
					$cuentas[$row[0]]["fuenCuenta"]=obtenerFuente($row[3],$row[4]);
					$cuentas[$row[0]]["adicion"]=$arregloCuenta[1];
					$cuentas[$row[0]]["reduccion"]=$arregloCuenta[2];
					$cuentas[$row[0]]["presuDefinitivo"]=$arregloCuenta[3];
					$cuentas[$row[0]]["ingreso_ant"]=$arregloCuenta[4];
					$cuentas[$row[0]]["ingreso_mes"]=$arregloCuenta[5];
					$cuentas[$row[0]]["ingreso_tot"]=$arregloCuenta[6];
					$cuentas[$row[0]]["superavit"]=$arregloCuenta[7];
					$cuentas[$row[0]]["saldo_ing"]=$arregloCuenta[3]-$arregloCuenta[6];
					$cuentas[$row[0]]["porcentaje"]=round(($arregloCuenta[6]/$arregloCuenta[3])*100,2);
					$cuentas[$row[0]]["tipo"]="Auxiliar";*/
	
				}
				else
				{
					$cuentas[$row[0]]["numCuenta"]=$row[0];
					$cuentas[$row[0]]["nomCuenta"]=$nt;
					$cuentas[$row[0]]["presuInicial"]=0;
					$cuentas[$row[0]]["adicion"]=0;
					$cuentas[$row[0]]["reduccion"]=0;
					$cuentas[$row[0]]["presuDefinitivo"]=0;
					$cuentas[$row[0]]["ingreso_tot"]=0;
					$cuentas[$row[0]]["porcentaje"]=0;
					$cuentas[$row[0]]["tipo"]="Mayor";
					$cuentaPadre[]=$row[0];
					/*
					$cuentas[$row[0]]["numCuenta"]=$row[0];
					$cuentas[$row[0]]["nomCuenta"]=$row[1];
					$cuentas[$row[0]]["presuInicial"]=0;
					$cuentas[$row[0]]["disponibilidad"]=0;
					$cuentas[$row[0]]["fuenCuenta"]=obtenerFuente($row[3],$row[4]);
					$cuentas[$row[0]]["adicion"]=0;
					$cuentas[$row[0]]["reduccion"]=0;
					$cuentas[$row[0]]["presuDefinitivo"]=0;
					$cuentas[$row[0]]["ingreso_ant"]=0;
					$cuentas[$row[0]]["ingreso_mes"]=0;
					$cuentas[$row[0]]["ingreso_tot"]=0;
					$cuentas[$row[0]]["superavit"]=0;
					$cuentas[$row[0]]["saldo_ing"]=0;
					$cuentas[$row[0]]["porcentaje"]=0;
					$cuentas[$row[0]]["tipo"]="Mayor";
					$cuentaPadre[]=$row[0];*/
				}
				//****ppto inicial
				//$cuentas = generaReporteIngresos($pctas[$x],$pctasvig1[$pctas[$x]],$fechaf,$fechaf2);
				//var_dump($cuentas);
				
			}
			
			function buscaCuentasHijo($cuenta)
			{
				global $cuentas,$linkbd,$vigenciaTr,$f1,$f2,$cuentaPadre;
				$arreglo=Array('0','1','2','3','4','5','6','7','8','9');
				$numcuenta=strlen($cuenta);
				$cuentaPunto = strpos($cuenta,'.');
				$cuentaGuion = strpos($cuenta,'-');
	
				if($cuenta=="01-TI.A.1.2.1"){$cuenta=$cuenta."-";}
				if(($numcuenta==1 || $numcuenta==2) && !is_numeric($cuenta)){
					$sql="SELECT cuenta FROM pptocuentas WHERE cuenta LIKE '%$cuenta%' AND estado='S' AND clasificacion  LIKE '%ingresos%' AND vigencia=$vigenciaTr AND (tipo='Auxiliar' OR tipo='auxiliar')";
				}else{
					if($cuentaPunto==1 && $cuentaGuion==false)
					{
						$sql="SELECT cuenta FROM pptocuentas WHERE cuenta LIKE '$cuenta.%' AND estado='S' AND clasificacion  LIKE '%ingresos%' AND vigencia=$vigenciaTr AND (tipo='Auxiliar' OR tipo='auxiliar')";
					}
					else
					{
						$sql="SELECT cuenta FROM pptocuentas WHERE cuenta LIKE '$cuenta%' AND estado='S' AND clasificacion  LIKE '%ingresos%' AND vigencia=$vigenciaTr AND (tipo='Auxiliar' OR tipo='auxiliar')";
					}
				}
				$result=mysql_query($sql,$linkbd);
				$acumpptoini=0.0;
				$acumadic=0.0;
				$acumredu=0.0;
				$acumpptodef=0.0;
				$acumingreso_tot=0.0;
				$acumporcentaje=0.0;
				while($row = mysql_fetch_array($result)){
					$acumpptoini+=$cuentas[$row[0]]["presuInicial"];
					$acumadic+=$cuentas[$row[0]]["adicion"];
					$acumredu+=$cuentas[$row[0]]["reduccion"];
					$acumpptodef+=$cuentas[$row[0]]["presuDefinitivo"];
					$acumingreso_tot+=$cuentas[$row[0]]["ingreso_tot"];
					$acumporcentaje+=$cuentas[$row[0]]["porcentaje"];
				}
				$cuentas[$cuenta]["presuInicial"]=$acumpptoini;
				$cuentas[$cuenta]["adicion"]=$acumadic;
				$cuentas[$cuenta]["reduccion"]=$acumredu;
				$cuentas[$cuenta]["presuDefinitivo"]=$acumpptodef;
				$cuentas[$cuenta]["ingreso_tot"]=$acumingreso_tot;
				$cuentas[$cuenta]["porcentaje"]=$acumporcentaje;
			}
			$contCuentaPadre = sizeof($cuentaPadre);
			for ($i=0; $i < $contCuentaPadre; $i++) 
			{
				buscaCuentasHijo($cuentaPadre[$i]);
			}
			
			$totPresuInicial=0;
	   		$totAdiciones=0;
	   		$totReducciones=0;
	   		$totPresuDefinitivo=0;
	   		$totIngresos_tot=0;
	   		$totSaldo=0;
			$totPorcentaje=0;
			foreach ($cuentas as $key => $value) 
			{
				$numeroCuenta=$cuentas[$key]['numCuenta'];
				$nombreCuenta=$cuentas[$key]['nomCuenta'];
				$presupuestoInicial=$cuentas[$key]['presuInicial'];
				$adicion=$cuentas[$key]['adicion'];
				$reduccion=$cuentas[$key]['reduccion'];
				$presupuestoDefinitivo=$cuentas[$key]['presuDefinitivo'];
				$ingresos_tot=$cuentas[$key]['ingreso_tot'];
				$porcentaje=$cuentas[$key]['porcentaje'];
				$tipo=$cuentas[$key]['tipo'];
				$saldo=0;
				$style='';
				if($saldo<0){
					$style='background: yellow';
				}
				//fputs($Descriptor2,"D\t".$numeroCuenta."\t".$_POST[periodo]."\t".$vigusu."\tPROGRAMACIONDEINGRESOS\r\n");
				$nombreCuentaff=iconv($_SESSION["VERCARPHPINI"], $_SESSION["VERCARPHPFIN"]."//TRANSLIT",$nombreCuenta);
				if(!empty($numeroCuenta))//----bloque nuevo 17/01/2016
				{  
					if($tipo=='Auxiliar' || $tipo=='auxiliar')
					{
						$totPresuInicial+=$cuentas[$key]['presuInicial'];
						$totAdiciones+=$cuentas[$key]['adicion'];
						$totReducciones+=$cuentas[$key]['reduccion'];
						$totPresuDefinitivo+=$cuentas[$key]['presuDefinitivo'];
						$totIngresos_tot+=$cuentas[$key]['ingreso_tot'];
						$totPorcentaje+=$cuentas[$key]['porcentaje'];
						echo "<tr style='font-size:9px; text-rendering: optimizeLegibility;$style' class='$iter' ondblclick='direccionaCuentaGastos(this)'>";
						
						echo "
						<td id='1' style='width: 5%'>$numeroCuenta</td>
						<td id='2' style='width: 15%'>$nombreCuentaff</td>
						<td id='2' style='width: 15%'></td>
						<td id='3' style='width: 5.5%'>".number_format($presupuestoInicial,2,",",".")."</td>
						<td id='4' style='width: 4.5%'>".number_format($adicion,2,",",".")."</td>
						<td id='5' style='width: 4.5%'>".number_format($reduccion,2,",",".")."</td>
						<td id='6' style='width: 5%'>".number_format($presupuestoDefinitivo,2,",",".")."</td>
						<td id='10' style='width: 4.5%'>".number_format($ingresos_tot,2,",",".")."</td>
						<td id='12' style='width: 4.5%'>".$porcentaje."%</td>";
						$vporcentaje = $porcentaje;
						echo "</tr>";
						$sumapi = $sumapi + $presupuestoInicial;
						$sumapad = $sumapad + $adicion;
						$sumapred = $sumapred + $reduccion;
						$sumai = $sumai + $presupuestoDefinitivo;
						$sumareca = $sumareca + $ingresos_tot;
					}
					else
					{
						echo "<tr style='font-weight:bold; font-size:9px; text-rendering: optimizeLegibility' class='$iter'>";
						
						echo "
						<td id='1' style='width: 5%'>$numeroCuenta</td>
						<td id='2' style='width: 15%'>$nombreCuentaff</td>
						<td id='2' style='width: 15%'></td>
						<td id='3' style='width: 5.5%'>".number_format($presupuestoInicial,2,",",".")."</td>
						<td id='4' style='width: 4.5%'>".number_format($adicion,2,",",".")."</td>
						<td id='5' style='width: 4.5%'>".number_format($reduccion,2,",",".")."</td>
						<td id='6' style='width: 5%'>".number_format($presupuestoDefinitivo,2,",",".")."</td>
						<td id='10' style='width: 4.5%'>".number_format($ingresos_tot,2,",",".")."</td>
						<td id='12' style='width: 4.5%'>".ROUND(($ingresos_tot/$presupuestoDefinitivo)*100,2)."%</td>";
						$vporcentaje = ROUND(($ingresos_tot/$presupuestoDefinitivo)*100,2);
						echo "</tr>";
					}
					$nombreCuentaffcsv = str_replace(',',' ',$nombreCuentaff);
					fputs($Descriptor1,$numeroCuenta.",".strtoupper($nombreCuentaffcsv).",".number_format($presupuestoInicial,0,'.','').",".number_format($adicion,0,'.','').",".number_format($reduccion,0,'.','').",".number_format($presupuestoDefinitivo,0,'.','').",".number_format($ingresos_tot,0,'.','').",".number_format($vporcentaje,2,".","")."\r\n");
					//fputs($Descriptor2,"D\t".$numeroCuenta."\t".$nombreCuenta."\t".$fuenteCuenta."\t".number_format($presupuestoInicial,2,",",".")."\t".number_format($adicion,2,",",".")."\t".number_format($reduccion,2,",",".")."\t".number_format($presupuestoDefinitivo,2,",",".")."\t".number_format($superavit,2,",",".")."\t".number_format($ingresos_ant,2,",",".")."\t".number_format($ingresos_mes,2,",",".")."\t".number_format($ingresos_tot,2,",",".")."\t".number_format($saldo_ing,2,",",".")."\t".ROUND(($ingresos_tot/$presupuestoDefinitivo)*100,2)."\r\n");
					
					$aux=$iter;
					$iter=$iter2;
					$iter2=$aux;
				}  //----bloque nuevo 17/01/2016
		
			}
			

			/*
				echo "<tr class='$iter' style='font-size:9px;'>
					<td  $negrilla><input type='hidden' name='tcodcuenta[]' value='$tcta'><input type='hidden' name='codcuenta[]' value='$ctaniv'>".$ctaniv."</td>
					<td  $negrilla><input type='hidden' name='nomcuenta[]' value='$nt'>".strtoupper($nt)."</td>
					<td  $negrilla><input type='hidden' name='fuente[]' value='$fte'>".strtoupper($fte)."</td>
					<td align='right' $negrilla><input type='hidden' name='picuenta[]' value='$pi'>".number_format($pi,2)."</td>
					<td  align='right' $negrilla><input type='hidden' name='padcuenta[]' value='".number_format($pad,2,",","")."'>".number_format($pad,2)."</td>
					<td  align='right' $negrilla><input type='hidden' name='predcuenta[]' value='$pred'>".number_format($pred,2)."</td>
					<td  align='right' $negrilla><input type='hidden' name='pdefcuenta[]' value='$pdef'>".number_format($pdef,2)."</td>
					<td  align='right' $negrilla><input type='hidden' name='vicuenta[]' value='".number_format($vitot,2,",","")."'>".number_format($vitot,2)."</td>
					<td  align='right' $negrilla><input type='hidden' name='vipcuenta[]' value='$vporcentaje'>".number_format($vporcentaje,2)."%</td>
				</tr>";
				$aux=$iter;
				$iter=$iter2;
				$iter2=$aux;
			*/	
				
			//fin for
   
			$vportot=round(($sumareca/$sumai)*100,2);
		echo "<tr><td ></td><td ></td><td  align='right'>Totales:</td><td class='saludo3' align='right'>".number_format($sumapi,2)."</td><td class='saludo3' align='right'>".number_format($sumapad,2)."</td><td class='saludo3' align='right'>".number_format($sumapred,2)."</td><td class='saludo3' align='right'>".number_format($sumai,2)."</td><td class='saludo3' align='right'>".number_format($sumareca,2)."</td><td class='saludo3' align='right'>".number_format($vportot,2)."%</td></tr>";
   
   fclose($Descriptor1);
	break;
	//----------------------------------------------------------------------------------------------
	case 3: //F07 - Fuente de Recursos CDM
	$crit3=" ";
	$crit4=" ";	
	$namearch="archivos/FORMATO_".$informes[$_POST[reporte]].".csv";
	$_POST[nombrearchivo]=$namearch;
	$Descriptor1 = fopen($namearch,"w+"); 
	fputs($Descriptor1,"(C) Codigo;(C) Descripcion;(C) Fuente De Recursos;(D) Apropiacion Inicial;(D) Adiciones;(D) Reducciones;(D) Creditos;(D) Contracreditos;(D) Apropiacion Definitiva;(D) Compromisos;(D) Saldo Por Comprometer;(D) Obligaciones;(D) Compromisos Por Ejecutar;(D) Pagos;(D) Obligaciones Por Pagar\r\n");
	
	if ($_POST[fecha]!="" and $_POST[fecha2]!="" )
	{	ereg( "([0-9]{1,2})/([0-9]{1,2})/([0-9]{2,4})", $_POST[fecha],$fecha);
	$fechaf=$fecha[3]."-".$fecha[2]."-".$fecha[1];
	ereg( "([0-9]{1,2})/([0-9]{1,2})/([0-9]{2,4})", $_POST[fecha2],$fecha);
	$fechaf2=$fecha[3]."-".$fecha[2]."-".$fecha[1];	
	}
	$sqlr2="Select distinct cuenta, tipo, vigencia, vigenciaf from pptocuentas where  (left(cuenta,1)>=2 OR (left(cuenta,1)='R' and substring(cuenta,2,1)>=2)) and  (vigencia='".$vigusu."' or vigenciaf='".$vigusu."') and tipo='Auxiliar' order by cuenta";	
	//echo "$sqlr2";
	$pctas=array();
	$tpctas=array();
	$pctasvig1=array();
	$pctasvig2=array();	
	$rescta=mysql_query($sqlr2,$linkbd);
	while ($row =mysql_fetch_row($rescta)) 
	 {
	  $pctas[]=$row[0];
	  $tpctas[$row[0]]=$row[1];
	  $pctasvig1[$row[0]]=$row[2];
	  $pctasvig2[$row[0]]=$row[3];
	 }
	mysql_free_result($rescta);
	echo "<table class='inicio' align='center' width='80%'>
	<tr><td colspan='15' class='titulos'>.: F07 - Fuente de Recursos CDM:</td></tr>
	<tr class='zebra1'><td colspan='15'>Registros Encontrados: $ntr</td></tr>
	<tr><td class='titulos2'>Codigo Rubro</td><td class='titulos2'>Descripcion Rubro</td>
	<td class='titulos2'>Fuente de Recursos</td><td class='titulos2' >Aforo Inicial</td>
	<td class='titulos2' >Adiciones</td><td class='titulos2' >Reducciones</td>
	<td class='titulos2' >Creditos</td><td class='titulos2' >Contracreditos</td>	
	<td class='titulos2'>Aforo Definitivo</td><td class='titulos2' >Compromisos</td>
	<td class='titulos2'>Saldo por Comprometer</td><td class='titulos2' >Obligaciones</td>	
	<td class='titulos2'>Compromisos por Ejecutar</td><td class='titulos2' >Pagos</td>	
	<td class='titulos2'>Obligaciones por Pagar</td></tr>";
	for ($x=0;$x<count($pctas);$x++) 
	 {
	  $pdef=0;
	  $pred=0;
	  $pad=0;
	  $vitot=0;
	  $pdef=0;
	  $rps=0;	 
	  $pagos=0;
	  $pccr=0;
	  $pcr=0;	 
	  $ops=0;
	 
	  $sqlr3="SELECT DISTINCT
          pptocomprobante_det.cuenta,
          sum(pptocomprobante_det.valdebito),
          sum(pptocomprobante_det.valcredito)
	      FROM pptocomprobante_det, pptocomprobante_cab
    	  WHERE pptocomprobante_cab.tipo_comp = pptocomprobante_det.tipo_comp
          AND pptocomprobante_det.numerotipo = pptocomprobante_cab.numerotipo
          AND pptocomprobante_cab.estado = 1
          AND ( pptocomprobante_det.valdebito > 0
          OR pptocomprobante_det.valcredito > 0)			   
	  AND (pptocomprobante_cab.VIGENCIA=".$pctasvig1[$pctas[$x]]." or pptocomprobante_cab.VIGENCIA=".$pctasvig2[$pctas[$x]].")
	  AND (pptocomprobante_det.VIGENCIA=".$pctasvig1[$pctas[$x]]." or pptocomprobante_det.VIGENCIA=".$pctasvig2[$pctas[$x]].")
		  AND pptocomprobante_cab.fecha BETWEEN '$fechaf' AND '$fechaf2'
          AND pptocomprobante_det.tipo_comp = 1 
          AND pptocomprobante_cab.tipo_comp = 1 		  
          AND pptocomprobante_det.cuenta = '".$pctas[$x]."' 
		  GROUP BY pptocomprobante_det.cuenta
		  ORDER BY pptocomprobante_det.cuenta";
	   $res=mysql_query($sqlr3,$linkbd);
	   $row =mysql_fetch_row($res);
	   $pi=$row[1];
	   $pdef+=$pi;
	   
		//*** adiciones ***
	   $sqlr3="SELECT DISTINCT
          pptocomprobante_det.cuenta,
          sum(pptocomprobante_det.valdebito),
          sum(pptocomprobante_det.valcredito)
     FROM pptocomprobante_det, pptocomprobante_cab
    WHERE     pptocomprobante_cab.tipo_comp = pptocomprobante_det.tipo_comp
          AND pptocomprobante_det.numerotipo = pptocomprobante_cab.numerotipo
          AND pptocomprobante_cab.estado = 1
          AND (   pptocomprobante_det.valdebito > 0
               OR pptocomprobante_det.valcredito > 0)			   
		AND (pptocomprobante_cab.VIGENCIA=".$pctasvig1[$pctas[$x]]." or pptocomprobante_cab.VIGENCIA=".$pctasvig2[$pctas[$x]].")
	  	AND (pptocomprobante_det.VIGENCIA=".$pctasvig1[$pctas[$x]]." or pptocomprobante_det.VIGENCIA=".$pctasvig2[$pctas[$x]].")		    AND pptocomprobante_cab.fecha BETWEEN '$fechaf' AND '$fechaf2'
          AND pptocomprobante_cab.tipo_comp = 2 
		  AND pptocomprobante_cab.tipo_comp = 2 
          AND pptocomprobante_det.cuenta = '".$pctas[$x]."' 
		  GROUP BY pptocomprobante_det.cuenta
   ORDER BY pptocomprobante_det.cuenta";
	   $res=mysql_query($sqlr3,$linkbd);
	   $row =mysql_fetch_row($res);
	   $pad+=$row[1];	 
	   $pdef+=$pad;	
	  
	   //*** reducciones ***
	   $sqlr3="SELECT DISTINCT
          pptocomprobante_det.cuenta,
          sum(pptocomprobante_det.valdebito),
          sum(pptocomprobante_det.valcredito)
     FROM pptocomprobante_det, pptocomprobante_cab
    WHERE     pptocomprobante_cab.tipo_comp = pptocomprobante_det.tipo_comp
          AND pptocomprobante_det.numerotipo = pptocomprobante_cab.numerotipo
          AND pptocomprobante_cab.estado = 1
          AND (   pptocomprobante_det.valdebito > 0
               OR pptocomprobante_det.valcredito > 0)			   		
  	  AND (pptocomprobante_cab.VIGENCIA=".$pctasvig1[$pctas[$x]]." or pptocomprobante_cab.VIGENCIA=".$pctasvig2[$pctas[$x]].")
	  AND (pptocomprobante_det.VIGENCIA=".$pctasvig1[$pctas[$x]]." or pptocomprobante_det.VIGENCIA=".$pctasvig2[$pctas[$x]].")
		  AND pptocomprobante_cab.fecha BETWEEN '$fechaf' AND '$fechaf2'
          AND pptocomprobante_cab.tipo_comp = 3 
		  AND pptocomprobante_cab.tipo_comp = 3 
          AND pptocomprobante_det.cuenta = '".$pctas[$x]."' 
		  GROUP BY pptocomprobante_det.cuenta
   ORDER BY pptocomprobante_det.cuenta";
	   $res=mysql_query($sqlr3,$linkbd);
	   $row =mysql_fetch_row($res);
	   $pred+=$row[2];	   
	   $pdef-=$pred;	 
	   //*** traslados ***
	   $sqlr3="SELECT DISTINCT
          pptocomprobante_det.cuenta,
          sum(pptocomprobante_det.valdebito),
          sum(pptocomprobante_det.valcredito)
     FROM pptocomprobante_det, pptocomprobante_cab
    WHERE     pptocomprobante_cab.tipo_comp = pptocomprobante_det.tipo_comp
          AND pptocomprobante_det.numerotipo = pptocomprobante_cab.numerotipo
          AND pptocomprobante_cab.estado = 1
          AND (   pptocomprobante_det.valdebito > 0
               OR pptocomprobante_det.valcredito > 0)			   
			   AND
			   pptocomprobante_cab.VIGENCIA=$vigusu
			   and  pptocomprobante_det.VIGENCIA=$vigusu
		    AND pptocomprobante_cab.fecha BETWEEN '$fechaf' AND '$fechaf2'
          AND pptocomprobante_cab.tipo_comp = 5 
		  AND pptocomprobante_cab.tipo_comp = 5 
          AND pptocomprobante_det.cuenta = '".$pctas[$x]."' 
		  GROUP BY pptocomprobante_det.cuenta
   ORDER BY pptocomprobante_det.cuenta";
	   $res=mysql_query($sqlr3,$linkbd);
	   $row =mysql_fetch_row($res);
	   $pcr+=$row[1];	   
	   $pccr+=$row[2];	   	   
	   $pdef=$pdef+$pcr-$pccr;	 
	   	   	  
	     //*** rps ***
	   $sqlr3="SELECT DISTINCT
          pptocomprobante_det.cuenta,
          sum(pptocomprobante_det.valdebito),
          sum(pptocomprobante_det.valcredito)
     FROM pptocomprobante_det, pptocomprobante_cab
    WHERE     pptocomprobante_cab.tipo_comp = pptocomprobante_det.tipo_comp
          AND pptocomprobante_det.numerotipo = pptocomprobante_cab.numerotipo
          AND pptocomprobante_cab.estado = 1
          AND (   pptocomprobante_det.valdebito > 0
               OR pptocomprobante_det.valcredito > 0)			   
			   AND
		AND (pptocomprobante_cab.VIGENCIA=".$pctasvig1[$pctas[$x]]." or pptocomprobante_cab.VIGENCIA=".$pctasvig2[$pctas[$x]].")
	    AND (pptocomprobante_det.VIGENCIA=".$pctasvig1[$pctas[$x]]." or pptocomprobante_det.VIGENCIA=".$pctasvig2[$pctas[$x]].")
		    AND pptocomprobante_cab.fecha BETWEEN '$fechaf' AND '$fechaf2'
          AND pptocomprobante_cab.tipo_comp = 7 
		  AND pptocomprobante_cab.tipo_comp = 7 
          AND pptocomprobante_det.cuenta = '".$pctas[$x]."' 
		  GROUP BY pptocomprobante_det.cuenta
   ORDER BY pptocomprobante_det.cuenta";
	   $res=mysql_query($sqlr3,$linkbd);
	   $row =mysql_fetch_row($res);
	   $rps+=$row[1];	   
	   
//***CXP TODOS  ****
	  $sqlr3="SELECT DISTINCT
          pptocomprobante_det.cuenta,
          sum(pptocomprobante_det.valdebito),
          sum(pptocomprobante_det.valcredito)
     FROM pptocomprobante_det, pptocomprobante_cab, pptotipo_comprobante
    WHERE     pptocomprobante_cab.tipo_comp = pptocomprobante_det.tipo_comp
          AND pptocomprobante_det.numerotipo = pptocomprobante_cab.numerotipo
          AND pptocomprobante_cab.estado = 1
          AND (   pptocomprobante_det.valdebito > 0
               OR pptocomprobante_det.valcredito > 0)			   
			   AND
		AND (pptocomprobante_cab.VIGENCIA=".$pctasvig1[$pctas[$x]]." or pptocomprobante_cab.VIGENCIA=".$pctasvig2[$pctas[$x]].")
	    AND (pptocomprobante_det.VIGENCIA=".$pctasvig1[$pctas[$x]]." or pptocomprobante_det.VIGENCIA=".$pctasvig2[$pctas[$x]].")
		    AND pptocomprobante_cab.fecha BETWEEN '$fechaf' AND '$fechaf2'
          AND pptocomprobante_cab.tipo_comp = pptotipo_comprobante.codigo 
		  AND (pptotipo_comprobante.tipo = 'C')		   
          AND pptocomprobante_det.cuenta = '".$ctaniv."' 
		  GROUP BY pptocomprobante_det.cuenta
   ORDER BY pptocomprobante_det.cuenta";
	   $res=mysql_query($sqlr3,$linkbd);
	   $row =mysql_fetch_row($res);
	   $ops+=$row[1];	
	   	   
	    //*** todos los PAGOS ***
		
	 $sqlr3="SELECT DISTINCT
          pptocomprobante_det.cuenta,
          sum(pptocomprobante_det.valdebito),
          sum(pptocomprobante_det.valcredito)
     FROM pptocomprobante_det, pptocomprobante_cab, pptotipo_comprobante
    WHERE     pptocomprobante_cab.tipo_comp = pptocomprobante_det.tipo_comp
          AND pptocomprobante_det.numerotipo = pptocomprobante_cab.numerotipo
          AND pptocomprobante_cab.estado = 1
          AND (   pptocomprobante_det.valdebito > 0
               OR pptocomprobante_det.valcredito > 0)			   			   
		AND (pptocomprobante_cab.VIGENCIA=".$pctasvig1[$pctas[$x]]." or pptocomprobante_cab.VIGENCIA=".$pctasvig2[$pctas[$x]].")
	    AND (pptocomprobante_det.VIGENCIA=".$pctasvig1[$pctas[$x]]." or pptocomprobante_det.VIGENCIA=".$pctasvig2[$pctas[$x]].")
		  AND pptocomprobante_cab.fecha BETWEEN '$fechaf' AND '$fechaf2'
          AND pptocomprobante_cab.tipo_comp = pptotipo_comprobante.codigo 
		  AND (pptotipo_comprobante.tipo = 'G' or pptotipo_comprobante.tipo = 'D')		   
          AND pptocomprobante_det.cuenta = '".$pctas[$x]."' 
		  GROUP BY pptocomprobante_det.cuenta
   ORDER BY pptocomprobante_det.cuenta";
	   $res=mysql_query($sqlr3,$linkbd);
	   $row =mysql_fetch_row($res);
	   $vitot+=$row[1];
	   $fuente=explode("_",buscafuenteppto($pctas[$x],$vigusu));
   	   $nrub= buscacuentapres($pctas[$x],1);
	   $saldocomp=$pdef-$rps;	
	   $compejec=$rps-$ops;
	   $oblpagar=$ops-$vitot;   	 
//	(C) Codigo,(C) Descripcion,(C) Fuente De Recursos,(D) Apropiacion Inicial,(D) Adiciones,(D) Reducciones,(D) Creditos,(D) Contracreditos,(D) Apropiacion Definitiva,(D) Compromisos,(D) Saldo Por Comprometer,(D) Obligaciones,(D) Compromisos Por Ejecutar,(D) Pagos,(D) Obligaciones Por Pagar	   
	 echo "<tr class='$iter'><td>".$pctas[$x]."</td><td>$nrub</td><td >$fuente[0] $fuente[1]</td><td >$pi</td>
	 <td >$pad</td><td>$pred</td><td>$pcr</td><td >$pccr</td><td>$pdef</td>
	 <td >$rps</td><td>$saldocomp</td><td>$ops</td><td>$compejec</td>	 	 
	 <td>$vitot</td><td>$oblpagar</td></tr>";
	 fputs($Descriptor1,$pctas[$x].";".$nrub.";".$fuente[0].";".$pi.";".$pad.";".$pred.";".$pcr.";".$pccr.";".$pdef.";".$rps.";".$saldocomp.";".$ops.";".$compejec.";".$vitot.";".$oblpagar."\r\n");
	 $con+=1;
	 $aux=$iter;
	 $iter=$iter2;
	 $iter2=$aux;	 
   }
   fclose($Descriptor1);
	
	break;
	case 5:  //****** F17B - Mes a Rendir CDM (ESTAMPILLAS PRO CULTURA)
		if ($_POST[fecha]!="" and $_POST[fecha2]!="")
		{
			ereg( "([0-9]{1,2})/([0-9]{1,2})/([0-9]{2,4})", $_POST[fecha],$fecha);
			$fechaf=$fecha[3]."-".$fecha[2]."-".$fecha[1];
			ereg( "([0-9]{1,2})/([0-9]{1,2})/([0-9]{2,4})", $_POST[fecha2],$fecha);
			$fechaf2=$fecha[3]."-".$fecha[2]."-".$fecha[1];	
		}

		$namearch="archivos/".$_SESSION[usuario]."informecdm_".$informes[$_POST[reporte]].".csv";
		$_POST[nombrearchivo]=$namearch;
		$Descriptor1 = fopen($namearch,"w+"); 

		?>
   	 	<table class="inicio">
			<tr>
				<td class="saludo1">Retenciones e Ingresos:</td>
			</tr>
		</table>
		<table class="inicio" >
        	<tr>
				<td class="titulos">Mes</td>
				<td class="titulos">Pago</td>
				<td class="titulos">Fecha Pago</td>
				<td class="titulos">Retenciones / Ingresos</td>
				<td class="titulos">Contabilidad</td>
				<td class="titulos">Valor</td>
			</tr>   
		<?php
	break;	
	case 6://****** F17B - Mes a Rendir CDM (ESTAMPILLAS PROTURISMO)
		if ($_POST[fecha]!="" and $_POST[fecha2]!="")
		{
			ereg( "([0-9]{1,2})/([0-9]{1,2})/([0-9]{2,4})", $_POST[fecha],$fecha);
			$fechaf=$fecha[3]."-".$fecha[2]."-".$fecha[1];
			ereg( "([0-9]{1,2})/([0-9]{1,2})/([0-9]{2,4})", $_POST[fecha2],$fecha);
			$fechaf2=$fecha[3]."-".$fecha[2]."-".$fecha[1];	
		}
		$namearch="archivos/".$_SESSION[usuario]."informecdm_".$informes[$_POST[reporte]].".csv";
		$_POST[nombrearchivo]=$namearch;
		$Descriptor1 = fopen($namearch,"w+"); 
		fputs($Descriptor1,"(C) Mes A Rendir;(D) Saldo Anterior;(D) Valor Retenido;(F) Fecha De Pago;(C) N� Recibo;(D) Valor Pagado;(D) Saldo Siguiente\r\n");
		?>
   	 	<table class="inicio">
	 		<tr> 
				<td class="saludo1">Retenciones e Ingresos:</td>
				<td colspan="2">
					<select name="retencion"  onChange="validar()" onKeyUp="return tabular(event,this)">
						<option value="">Seleccione ...</option>
							<?php
							//PARA LA PARTE CONTABLE SE TOMA DEL DETALLE DE LA PARAMETRIZACION LAS CUENTAS QUE INICIAN EN 2**********************	
							$linkbd=conectar_bd();
							$sqlr="select *from tesoretenciones where estado='S' and terceros='1'";
							$res=mysql_query($sqlr,$linkbd);
							while ($row =mysql_fetch_row($res)) 
				    		{
								echo "<option value='R-$row[0]' ";
								$i=$row[0];
		
					 			if('R-'.$i==$_POST[retencion])
			 					{
						 			echo "SELECTED";
						  			$_POST[nretencion]='R - '.$row[1]." - ".$row[2];
						 		}
					  			echo ">R - ".$row[1]." - ".$row[2]."</option>";	 	 
							}	 	
							$sqlr="select *from tesoingresos where estado='S' and terceros='1'";
							$res=mysql_query($sqlr,$linkbd);
							while ($row =mysql_fetch_row($res)) 
				    		{
								echo "<option value='I-$row[0]' ";
								$i=$row[0];
		
					 			if('I-'.$i==$_POST[retencion])
			 					{
						 			echo "SELECTED";
						  			$_POST[nretencion]='I - '.$row[1]." - ".$row[2];
						 		}
					  			echo ">I - ".$row[1]." - ".$row[2]."</option>";	 	 
							}	 	
	?>
   </select>
		</td> <td><input type="hidden" value="<?php echo $_POST[nretencion]?>" name="nretencion"><input type="hidden" value="1" name="oculto">
		<input type="button" name="agregard" id="agregard" value="   Agregar   " onClick="agregardetalled()" ><input type="hidden" value="0" name="agregadetdes"></td></tr></table>
        <table class="inicio" style="overflow:scroll">
	   <?php 	
	   echo $_POST[retencion]."hola <br>";
		if ($_POST[eliminad]!='')
		 { 
		 //echo "<TR><TD>ENTROS :".$_POST[elimina]."</TD></TR>";
		 $posi=$_POST[eliminad];
		 unset($_POST[ddescuentos][$posi]);
		 unset($_POST[dndescuentos][$posi]);
		 $_POST[ddescuentos]= array_values($_POST[ddescuentos]); 
		 $_POST[dndescuentos]= array_values($_POST[dndescuentos]); 
		 }	 
		 if ($_POST[agregadetdes]=='1')
		 {
		 if(!esta_en_array($_POST[ddescuentos], $_POST[retencion]))
		  	 {
		 $_POST[ddescuentos][]=$_POST[retencion];
		 $_POST[dndescuentos][]=$_POST[nretencion];
		 $_POST[agregadetdes]='0';
			 }
			 else
			 {
			?>
			 <script>
				alert('La Retencion o Ingreso ya esta en la Lista');
	        </script>
			<?php
			 }
		 ?>
		 <script>
        document.form2.porcentaje.value=0;
        document.form2.vporcentaje.value=0;	
		document.form2.retencion.value='';	
        </script>
		<?php
		 }
		  ?>
        <tr><td class="titulos">Retenciones e Ingresos</td><td class="titulos2"><img src="imagenes/del.png" ><input type='hidden' name='eliminad' id='eliminad'></td></tr>
      	<?php
		$totaldes=0;
//		echo "v:".$_POST[valor];
		for ($x=0;$x<count($_POST[ddescuentos]);$x++)
		 {		 
		 echo "<tr><td class='saludo2'><input name='dndescuentos[]' value='".$_POST[dndescuentos][$x]."' type='text' size='100' readonly><input name='ddescuentos[]' value='".$_POST[ddescuentos][$x]."' type='hidden'></td>";		
		 echo "<td class='saludo2'><a href='#' onclick='eliminard($x)'><img src='imagenes/del.png'></a></td></tr>";	
		 }		 
		$_POST[valorretencion]=$totaldes;

		?>
        <script>
        document.form2.totaldes.value=<?php echo $totaldes;?>;		
//	calcularpago();
       document.form2.valorretencion.value=<?php echo $totaldes;?>;
        </script>
        <?php
		$_POST[valorcheque]=$_POST[valoregreso]-$_POST[valorretencion];
		?>
        </table>
        
        <table class="inicio" >
        <tr><td class="titulos">Mes</td><td class="titulos">Pago</td><td class="titulos">Fecha Pago</td><td class="titulos">Retenciones / Ingresos</td><td class="titulos">Contabilidad</td><td class="titulos">Valor</td></tr>        
      	<?php
		$_POST[mddescuentos]=array();
		$_POST[mtdescuentos]=array();		
		$_POST[mddesvalores]=array();
		$_POST[mddesvalores2]=array();		
		$_POST[mdndescuentos]=array();
		$_POST[mdctas]=array();	
		$_POST[mdmes]=array();				
		$totalpagar=0;
		//**** buscar movimientos
		for ($x=0;$x<count($_POST[ddescuentos]);$x++)
		 {	
		 $tm=strlen($_POST[ddescuentos][$x]);
		//********** RETENCIONES *********
		if(substr($_POST[ddescuentos][$x],0,1)=='R')
		  {
		 $sqlr="select distinct tesoordenpago_retenciones.id_retencion, sum(tesoordenpago_retenciones.valor),MONTH(tesoegresos.fecha) from tesoordenpago, tesoordenpago_retenciones,tesoegresos where tesoegresos.id_orden=tesoordenpago.id_orden  and tesoegresos.estado='S' and tesoegresos.fecha BETWEEN '$fechaf' AND '$fechaf2' and tesoordenpago_retenciones.id_retencion='".substr($_POST[ddescuentos][$x],2,$tm-2)."' and tesoordenpago.id_orden=tesoordenpago_retenciones.id_orden group by MONTH(tesoegresos.fecha),tesoordenpago_retenciones.id_retencion";				 	
		 $res=mysql_query($sqlr,$linkbd);		 
		// echo "$row[0] - ".$sqlr;
		while ($row =mysql_fetch_row($res)) 
	    {
		 $sqlr="select *from tesoretenciones_det where codigo='$row[0]' and vigencia='$vigusu' ";
		  $res2=mysql_query($sqlr,$linkbd);	
		 // echo "$row[0] - ".$sqlr;
		  while($row2 =mysql_fetch_row($res2))
		   {
		   if(substr($row2[2],0,1)=='2')
		    {
		   $vpor=$row2[4];
	   	   $_POST[mtdescuentos][]='R';
		   $_POST[mddesvalores][]=$row[1]*($vpor/100);
	   	   $_POST[mddesvalores2][]=$row[1]*($vpor/100);		   
		   $_POST[mddescuentos][]=$row[0];
		   $_POST[mdctas][]=$row2[2];		   
		   $_POST[mdmes][]=$row[2];		   		   
		   $_POST[mdndescuentos][]=buscaretencion($row[0]);
		   $totalpagar+=$row[1]*($vpor/100);
			}
		   }
		 }
		}
		//****** INGRESOS *******
		if(substr($_POST[ddescuentos][$x],0,1)=='I')
		  {
	$sqlr="select distinct tesoreciboscaja_det.ingreso, sum(tesoreciboscaja_det.valor), tesoreciboscaja.cuentabanco,tesoreciboscaja.cuentacaja,MONTH(tesoreciboscaja.fecha) from  tesoreciboscaja_det,tesoreciboscaja where tesoreciboscaja.estado='S' and tesoreciboscaja.fecha BETWEEN '$fechaf' AND '$fechaf2' and tesoreciboscaja_det.ingreso='".substr($_POST[ddescuentos][$x],2,$tm-2)."' and tesoreciboscaja_det.id_recibos=tesoreciboscaja.id_recibos group by MONTH(tesoreciboscaja.fecha), tesoreciboscaja_det.ingreso";
		 $res=mysql_query($sqlr,$linkbd);		 
	// echo "<br> - ".$sqlr; 
		while ($row =mysql_fetch_row($res)) 
	    {
		 $sqlr="select *from  tesoingresos_det where codigo='$row[0]' and vigencia='$vigusu'";
		  $res2=mysql_query($sqlr,$linkbd);	
		  //echo "$row[0] - ".$sqlr;
		  while($row2 =mysql_fetch_row($res2))
		   {
		   $sqlr="select *from  conceptoscontables_det where codigo='$row2[2]' and modulo='4' and tipo='C' ";
		  $res3=mysql_query($sqlr,$linkbd);	
		 // echo "$row2[1] - ".$sqlr;
		   while($row3 =mysql_fetch_row($res3))
		   {
		   if(substr($row3[4],0,1)=='2')
		    {
		   $vpor=$row2[5];
		   $_POST[mtdescuentos][]='I';
		   $_POST[mdmes][]=$row[4];		   		   		   
	   	   $_POST[mddesvalores][]=$row[1]*($vpor/100);
		   $_POST[mddesvalores2][]=$row[1]*($vpor/100);		   
		   $_POST[mddescuentos][]=$row[0];
		   $_POST[mdctas][]=$row3[4];
		   $_POST[mdndescuentos][]=buscaingreso($row[0]);
		   $totalpagar+=$row[1]*($vpor/100);		   
		   //$nv=buscaingreso($row[0]);
		   //echo "ing:$nv";
			}
		   }
		  }
		 }
		}
		
		
		//*****INGRESOS PROPIOS
		 $sqlr="select distinct tesosinreciboscaja_det.ingreso, sum(tesosinreciboscaja_det.valor), tesosinreciboscaja.cuentabanco,tesosinreciboscaja.cuentacaja,MONTH(tesosinreciboscaja.fecha) from  tesosinreciboscaja_det,tesosinreciboscaja where tesosinreciboscaja.estado='S' and MONTH(tesosinreciboscaja.fecha)='$_POST[mes]' and YEAR(tesosinreciboscaja.fecha)='".$_POST[vigencias]."' and tesosinreciboscaja_det.ingreso='".substr($_POST[ddescuentos][$x],2,$tm-2)."' and tesosinreciboscaja_det.id_recibos=tesosinreciboscaja.id_recibos group by  MONTH(tesosinreciboscaja.fecha),tesosinreciboscaja_det.ingreso ";
		 $res=mysql_query($sqlr,$linkbd);		 
	//echo "<br> - ".$sqlr;
		while ($row =mysql_fetch_row($res)) 
	    {
		 $sqlr="select *from  tesoingresos_det where codigo='$row[0]' and vigencia='$vigusu'";
		  $res2=mysql_query($sqlr,$linkbd);	
		  //echo "$row[0] - ".$sqlr;
		  while($row2 =mysql_fetch_row($res2))
		   {
		   $sqlr="select *from  conceptoscontables_det where codigo='$row2[2]' and modulo='4' and tipo='C' ";
		  $res3=mysql_query($sqlr,$linkbd);	
		 // echo "$row2[1] - ".$sqlr;
		   while($row3 =mysql_fetch_row($res3))
		   {
		   if(substr($row3[4],0,1)=='2')
		    {
		   $vpor=$row2[5];
		   $_POST[mdmes][]=$row[4];
		   $_POST[mtdescuentos][]='I';
	   	   $_POST[mddesvalores][]=$row[1]*($vpor/100);
		   $_POST[mddesvalores2][]=$row[1]*($vpor/100);		   
		   $_POST[mddescuentos][]=$row[0];
		   $_POST[mdctas][]=$row3[4];
		   $_POST[mdndescuentos][]=buscaingreso($row[0]);
		   $totalpagar+=$row[1]*($vpor/100);		   
		   //$nv=buscaingreso($row[0]);
		   //echo "ing:$nv";
			}
		   }
		  }
		 }
		 
		//********************************
		}
		for ($x=0;$x<count($_POST[mddescuentos]);$x++)
		 {		 
		 $valorpago=buscapagotercero_detalle($_POST[mddescuentos][$x],$_POST[mdmes][$x],$vigusu);
		 
		 echo "<tr  class='$iter'><td><input name='mdmes[]' value='".$_POST[mdmes][$x]."' type='text' size='15' readonly></td><td>".$valorpago[0]."</td><td>".$valorpago[1]."</td><td><input name='mdndescuentos[]' value='".$_POST[mdndescuentos][$x]."' type='text' size='100' readonly><input name='mddescuentos[]' value='".$_POST[mddescuentos][$x]."' type='hidden'><input name='mtdescuentos[]' value='".$_POST[mtdescuentos][$x]."' type='hidden'></td><td><input name='mdctas[]' value='".$_POST[mdctas][$x]."' type='text' size='15' readonly></td>";
		echo "<td><input name='mddesvalores[]' value='".round($_POST[mddesvalores][$x],0)."' type='hidden'><input name='mddesvalores2[]' value='".number_format($_POST[mddesvalores2][$x],0)."' type='text' size='15' readonly></td></tr>";
		 $aux=$iter;
		 $iter=$iter2;
		 $iter2=$aux;
		 }		 
		 $vmil=0;
		if($_POST[ajuste]=='1')
		 {
		$vmil=round($totalpagar,-3);	 
		  }
		  else
		  {
		$vmil=$totalpagar;			  
		  }
		$resultado = convertir(round($vmil,0));
		$_POST[letras]=$resultado." PESOS M/CTE";
		 echo "<tr><td></td><td>Total:</td><td><input type='hidden' name='totalpago2' value='".round($totalpagar,0)."' ><input type='text' name='totalpago' value='".number_format($totalpagar,0)."' size='15' readonly></td></tr>";
		 echo "<tr><td colspan='3'><input name='letras' type='text' value='$_POST[letras]' size='150' ></td>";
		$dif=$vmil-$totalpagar;
		?>
        <script>
        document.form2.valorpagar.value=<?php echo round($totalpagar,0);?>;	
        document.form2.valorpagarmil.value=<?php echo $vmil;?>;	
		document.form2.diferencia.value=<?php echo round($dif,0);?>;			//calcularpago();
        </script>
        </table>
        
        
	<?php

	break;
	case 15: //SIA
	
		if($_POST[reglas]==1){$chkchip=" checked";}
 				else {$chkchip=" ";}
 				if(!$_POST[oculto]){$_POST[oculto]=1;}
   				$vact=$vigusu;    
	  			//*** PASO 2		  
				$sqlr="select *from configbasica where estado='S'";
				$res=mysql_query($sqlr,$linkbd);
				while($row=mysql_fetch_row($res))
	 			{
  					$_POST[nitentidad]=$row[0];
  					$_POST[entidad]=$row[1];
					$_POST[codent]=$row[8];
 				 }
		?>
		<input type="hidden" name="oculto" id="oculto" value="<?php echo $_POST[oculto] ?>">
        <input type="hidden" name="periodo" id="periodo" value="<?php echo $_POST[periodo] ?>"> 
		<table class="inicio" align="center"> 

			<tr>
        		<td class="titulos" colspan="10">Consolidacion CHIP</td>
     		</tr> 
     		<tr>
     			<td class="saludo3" style="width:6%;">Vigencia:</td> 
     			<td style="width:11%;">
     					  
     				<select name="vigencias" id="vigencias" style="width:98%;">
     					<option value="">Sel..</option>
     					<?php	  
                            for($x=$vact;$x>=$vact-2;$x--)
                            {
                                if($x==$_POST[vigencias]){echo "<option  value=$x SELECTED>$x</option>";}
                                else {echo "<option value=$x>$x</option>";}
                            }
                        ?>
     				</select>
     				
     			</td>
     			<td class="saludo3" style="width:6%;">Periodo:</td>
     			<td  style="width:14%;">
     				<select name="periodos" id="periodos" onChange="validar()"  style="width:45%;" >
      					<option value="">Sel..</option>
	  					<?php	  
  	  						$sqlr="Select * from chip_periodos  where estado='S' order by id";		
							$resp = mysql_query($sqlr,$linkbd);
							while ($row =mysql_fetch_row($resp)) 
							{
		 						if($row[0]==$_POST[periodos])
		 						{
			 						echo "<option value=$row[0] SELECTED>$row[2]</option>";
			 						$_POST[periodo]=$row[1]; 
			 						$_POST[cperiodo]=$row[2]; 	
								}
								else {echo "<option value=$row[0]>$row[2]</option>";}
							}
	 					?>
      				</select>
      				<input type="hidden" name="periodo" value="<?php echo $_POST[periodo]?>" >
                    <input type="text" name="cperiodo" value="<?php echo $_POST[cperiodo]?>"  style="width:45%;" readonly>
     			</td>
     			<td class="saludo1" style="width:15%;">
                	<span style=" vertical-align:middle; width:10px"> Reglas Chip (Signos)&nbsp;
      					<input type="checkbox" name="reglas" id="reglas" class="defaultcheckbox" value="1" <?php echo $chkchip?>>
                  	</span>
               	</td>
               	<td></td>
               	<td class="saludo3" style="width:11%;">Codigo Entidad</td>
                <td><input name="codent" type="text" value="<?php echo $_POST[codent]?>"></td>
     		</tr>
     		<tr>
            	<td class="saludo3">Nit:</td>
                <td>
                	<input type="hidden" name="nivel" value="4">
                    <input type="hidden" name="genbal"  value=" <?php echo $_POST[genbal]?>">
                    <input type="text" name="nitentidad" value="<?php echo $_POST[nitentidad]?>" style="width:98%;"readonly>
               	</td>
                <td class="saludo3">Entidad:</td>
                <td colspan="3">
                	<input type="text" name="entidad" value="<?php echo $_POST[entidad]?>" style="width:100%;"  readonly></td>
                <td colspan="2"> 
                	
                	<input type="button" name="genera" value=" Generar " onClick="crear()">  
                    <input type="hidden" name="gbalance" value="<?php echo $_POST[gbalance]?>"  readonly>    
        			<input type="button" name="guardabal" value="Clasificar Cuentas" onClick="guardarbalance()"> 
                    <input name="finalizar" type="button" value="Generar Archivo CSV" onClick="guardarchip()" >       
      			</td>
       		</tr> 
     		</table>
     		<div class="subpantallap" style="height:72%; width:99.6%; overflow-x:hidden;">
     			
     		
		<?php
				if($_POST[genbal]==1 && $_POST[gbalance]==0 )
				{
					$oculto=$_POST['oculto'];
					if($_POST[oculto])
					{
						$_POST[cuenta1]='1';
						$_POST[cuenta2]='9999999999999';
						$horaini=date('h:i:s');		
  						$niveles=array();
  						$sqlr="Select * from nivelesctas  where estado='S' order by id_nivel";
						$resp = mysql_query($sqlr,$linkbd);
						while ($row =mysql_fetch_row($resp)){$niveles[]=$row[4];}
						$mes1=substr($_POST[periodo],1,2);
						$mes2=substr($_POST[periodo],3,2);
						$_POST[fecha]='01'.'/'.$mes1.'/'.$_POST[vigencias];	
						$_POST[fecha2]=intval(date("t",$mes2)).'/'.$mes2.'/'.$_POST[vigencias];
						ereg( "([0-9]{1,2})/([0-9]{1,2})/([0-9]{2,4})", $_POST[fecha],$fecha);
						$fechaf1=$fecha[3]."-".$fecha[2]."-".$fecha[1];
						ereg( "([0-9]{1,2})/([0-9]{1,2})/([0-9]{2,4})", $_POST[fecha2],$fecha);
						$fechaf2=$fecha[3]."-".$fecha[2]."-".$fecha[1];
						ereg( "([0-9]{1,2})/([0-9]{1,2})/([0-9]{2,4})", $_POST[fecha],$fecha);
						$fechaf=$fecha[3]."-".$fecha[2]."-".$fecha[1];
						$fechafa2=mktime(0,0,0,$fecha[2],$fecha[1],$fecha[3]);
						$f1=$fechafa2;	
						$f2=mktime(0,0,0,$fecha[2],$fecha[1],$fecha[3]);	
						$fechafa=$_POST[vigencias]."-01-01";
						$fechafa2=date('Y-m-d',$fechafa2-((24*60*60)));
						$sqlr2="select distinct digitos, posiciones from nivelesctas where estado='S' ORDER BY id_nivel DESC ";
						$resn=mysql_query($sqlr2,$linkbd);
						$rown=mysql_fetch_row($resn);
						$nivmax=$rown[0];
						$dignivmax=$rown[1];
						echo "<table class='inicio' ><tr><td colspan='6' class='titulos'>Balance de Prueba</td></tr>";
  						echo "<tr><td class='titulos2'>Codigo</td><td class='titulos2'>Cuenta</td><td class='titulos2'>Saldo Anterior</td><td class='titulos2'>Debito</td><td class='titulos2'>Credito</td><td class='titulos2'>Saldo Final</td></tr>";
						$tam=$niveles[$_POST[nivel]-1];
						$crit1=" and left(cuenta,$tam)>='$_POST[cuenta1]' and left(cuenta,$tam)<='$_POST[cuenta2]' ";
						$sqlr2="select distinct cuenta,tipo from cuentas where estado ='S' and length(cuenta)=$tam ".$crit1." group by cuenta,tipo order by cuenta ";
						$rescta=mysql_query($sqlr2,$linkbd);
						$i=0;
						//echo $sqlr2;
						$pctas=array();
						$pctasb[]=array();
						while ($row =mysql_fetch_row($rescta)) 
						{
							$pctas[]=$row[0];
							$pctasb["$row[0]"][0]=$row[0];
						  	$pctasb["$row[0]"][1]=0;
						  	$pctasb["$row[0]"][2]=0;
						  	$pctasb["$row[0]"][3]=0;
						}
						mysql_free_result($rescta);
						$tam=$niveles[$_POST[nivel]-1];
						//echo "tc:".count($pctas);
						//******MOVIMIENTOS PERIODO
						$sqlr3="SELECT DISTINCT
          SUBSTR(comprobante_det.cuenta,1,$tam),
          sum(comprobante_det.valdebito),
          sum(comprobante_det.valcredito)
     FROM comprobante_det, comprobante_cab
    WHERE     comprobante_cab.tipo_comp = comprobante_det.tipo_comp
          AND comprobante_det.numerotipo = comprobante_cab.numerotipo
          AND comprobante_cab.estado = 1
          AND (   comprobante_det.valdebito > 0
               OR comprobante_det.valcredito > 0)
		    AND comprobante_cab.fecha BETWEEN '$fechaf1' AND '$fechaf2'
          AND comprobante_det.tipo_comp <> 7 
          AND SUBSTR(comprobante_det.cuenta,1,$tam) >= '$_POST[cuenta1]' AND SUBSTR(comprobante_det.cuenta,1,$tam) <='$_POST[cuenta2]'		 
		  		  AND comprobante_det.centrocosto like '%$_POST[cc]%'
		  GROUP BY SUBSTR(comprobante_det.cuenta,1,$tam)
   ORDER BY comprobante_det.cuenta";
   $res=mysql_query($sqlr3,$linkbd);
   while ($row =mysql_fetch_row($res)) 
 {
  $pctasb["$row[0]"][0]=$row[0];
  $pctasb["$row[0]"][2]=$row[1];
  $pctasb["$row[0]"][3]=$row[2];
 }
 $sqlr3="SELECT DISTINCT
          SUBSTR(comprobante_det.cuenta,1,$tam),
          sum(comprobante_det.valdebito)-
          sum(comprobante_det.valcredito)
     FROM comprobante_det, comprobante_cab
    WHERE     comprobante_cab.tipo_comp = comprobante_det.tipo_comp
          AND comprobante_det.numerotipo = comprobante_cab.numerotipo
          AND comprobante_cab.estado = 1
          AND (   comprobante_det.valdebito > 0
               OR comprobante_det.valcredito > 0)         
          AND comprobante_det.tipo_comp = 7 
          AND SUBSTR(comprobante_det.cuenta,1,$tam) >= '$_POST[cuenta1]' AND SUBSTR(comprobante_det.cuenta,1,$tam) <='$_POST[cuenta2]'      
		  		  AND comprobante_det.centrocosto like '%$_POST[cc]%'
		  GROUP BY SUBSTR(comprobante_det.cuenta,1,$tam)
   ORDER BY comprobante_det.cuenta";
   $res=mysql_query($sqlr3,$linkbd);
   while ($row =mysql_fetch_row($res)) 
 {
  $pctasb["$row[0]"][0]=$row[0];
   $pctasb["$row[0]"][1]=$row[1];
 }
 $sqlr3="SELECT DISTINCT
          SUBSTR(comprobante_det.cuenta,1,$tam),
          sum(comprobante_det.valdebito)-
          sum(comprobante_det.valcredito)
     FROM comprobante_det, comprobante_cab
    WHERE     comprobante_cab.tipo_comp = comprobante_det.tipo_comp
          AND comprobante_det.numerotipo = comprobante_cab.numerotipo
          AND comprobante_cab.estado = 1
          AND (   comprobante_det.valdebito > 0
               OR comprobante_det.valcredito > 0)
          AND comprobante_det.tipo_comp <> 7 
		   AND comprobante_cab.fecha BETWEEN '' AND '$fechafa2'
          AND SUBSTR(comprobante_det.cuenta,1,$tam) >= '$_POST[cuenta1]' AND SUBSTR(comprobante_det.cuenta,1,$tam) <='$_POST[cuenta2]'		
		  AND comprobante_det.centrocosto like '%$_POST[cc]%'
		  GROUP BY SUBSTR(comprobante_det.cuenta,1,$tam)
   ORDER BY comprobante_det.cuenta";
   $res=mysql_query($sqlr3,$linkbd);
   while ($row =mysql_fetch_row($res)) 
 {
  $pctasb["$row[0]"][0]=$row[0];
  $pctasb["$row[0]"][1]+=$row[1]; 
 } 
 for ($y=0;$y<$_POST[nivel];$y++)
 { 
   $lonc=count($pctasb);
   //foreach($pctasb as $k => $valores )
   $k=0;
  // echo "lonc:".$lonc;
//   while($k<$lonc)
	foreach($pctasb as $k => $valores )
    {
	if (strlen($pctasb[$k][0])>=$niveles[$y-1])
	 {		 
	 $ncuenta=substr($pctasb[$k][0],0,$niveles[$y-1]);
	 if($ncuenta!='')
	  {
	 $pctasb["$ncuenta"][0]=$ncuenta;
	 $pctasb["$ncuenta"][1]+=$pctasb[$k][1];
	 $pctasb["$ncuenta"][2]+=$pctasb[$k][2];
 	 $pctasb["$ncuenta"][3]+=$pctasb[$k][3];
	 //echo "<br>N:".$niveles[$y-1]." : cuenta:".$k." NC:".$ncuenta."  ".$pctasb["$ncuenta"][1]."  ".$pctasb["$ncuenta"][2]."  ".$pctasb["$ncuenta"][3];	
	  }
	 }
	   $k++;
	}
 }
 $sqlr="create  temporary table usr_session (id int(11),cuenta varchar(20),nombrecuenta varchar(100),saldoinicial double,debito double,credito double,saldofinal double)";
 mysql_query($sqlr,$linkbd);
 $i=1;
    foreach($pctasb as $k => $valores )
    {	 
	if(($pctasb[$k][1]<0 || $pctasb[$k][1]>0) || ($pctasb[$k][2]<0 || $pctasb[$k][2]>0) || ($pctasb[$k][3]<0 || $pctasb[$k][3]>0))
 	{
	$saldofinal=$pctasb[$k][1]+$pctasb[$k][2]-$pctasb[$k][3];
	 $nomc=existecuenta($pctasb[$k][0]);	 
	 $sqlr="insert into usr_session (id,cuenta,nombrecuenta,saldoinicial,debito,credito,saldofinal) values($i,'".$pctasb[$k][0]."','".$nomc."',".$pctasb[$k][1].",".$pctasb[$k][2].",".$pctasb[$k][3].",".$saldofinal.")";
	// $sqlr="insert into usr_session (id,cuenta,nombrecuenta,saldoinicial,debito,credito,saldofinal) values($i,'".$pctasb[$k][0]."','".$nomc."',".$viva.",".$pctasb[$k][2].",".$pctasb[$k][3].",".$saldofinal.")";
	 mysql_query($sqlr,$linkbd);
	 //echo "<br>".$sqlr;
	 $i+=1;
	}
	 //echo "<br>cuenta:".$k."  ".$pctasb[$k][1]."  ".$pctasb[$k][2]."  ".$pctasb[$k][3];	
	}
	$sqlr="select *from usr_session order by cuenta";
	$res=mysql_query($sqlr,$linkbd);
 $_POST[tsaldoant]=0;
	 $_POST[tdebito]=0;
	 $_POST[tcredito]=0;
	 $_POST[tsaldofinal]=0;
	  $cuentachipno=array();
	 $namearch="archivos/".$_SESSION[usuario]."balanceprueba-nivel$_POST[nivel].csv";
$Descriptor1 = fopen($namearch,"w+"); 
fputs($Descriptor1,"CODIGO;CUENTA;SALDO ANTERIOR;DEBITO;CREDITO;SALDO FINAL\r\n");
	 $co='saludo1a';
	 $co2='saludo2';
	  while($row=mysql_fetch_row($res))
  {	  
	  if(strlen($row[1])==6)
	  {
	   $sqlrchip="select count(*) from chipcuentas where cuenta=$row[1]";
	   $reschip=mysql_query($sqlrchip,$linkbd);
	   $rowchip=mysql_fetch_row($reschip);
	   if($rowchip[0]==0)
	    {
		 $cuentachipno[]=$row[1];
		// $ncuentachipno[]=buscacuenta($row[1]);		 
		}
	  }
	   $negrilla="style='font-weight:bold'";
	  if (strlen($row[1])==($dignivmax))
		{
		// $negrilla=" "; 
		 //$_POST[tsaldoant]+=$row[3];
		 //$_POST[tdebito]+=$row[4];
		 //$_POST[tcredito]+=$row[5];
		 }
		 if($niveles[$_POST[nivel]-1]==strlen($row[1]))
		  {
			$negrilla=" ";  
			$_POST[tsaldoant]+=$row[3];
			 $_POST[tdebito]+=$row[4];
	 		$_POST[tcredito]+=$row[5];			  
	 		$_POST[tsaldofinal]+=$row[6];			  	
		  }
	echo "<tr class='$co' style='text-transform:uppercase' onMouseOver=\"anterior=this.style.backgroundColor;this.style.backgroundColor='#40b3ff';\"
onMouseOut=\"this.style.backgroundColor=anterior\" ><td $negrilla>$row[1]</td><td $negrilla>$row[2]</td><td $negrilla align='right'>".number_format($row[3],2,".",",")."</td><td $negrilla align='right'>".number_format($row[4],2,".",",")."</td><td $negrilla align='right'>".number_format($row[5],2,".",",")."</td><td $negrilla align='right'>".number_format($row[6],2,".",",")."";
	 echo "<input type='hidden' name='dcuentas[]' value= '".$row[1]."'> <input type='hidden' name='dncuentas[]' value= '".$row[2]."'><input type='hidden' name='dsaldoant[]' value= '".round($row[3],2)."'> <input type='hidden' name='ddebitos[]' value= '".round($row[4],2)."'> <input type='hidden' name='dcreditos[]' value= '".round($row[5],2)."'><input type='hidden' name='dsaldo[]' value= '".round($row[6],2)."'></td></tr>" ;	 
	  fputs($Descriptor1,$row[1].";".$row[2].";".number_format($row[3],3,",","").";".number_format($row[4],3,",","").";".number_format($row[5],3,",","").";".number_format($row[6],3,",","")."\r\n");
	  $aux=$co;
         $co=$co2;
         $co2=$aux;
		 $i=1+$i;
		 //$nomvuenta=$row[2];
  }
  fclose($Descriptor1);
  echo "<tr class='$co'><td colspan='2'></td><td class='$co' align='right'>".number_format($_POST[tsaldoant],2,".",",")."<input type='hidden' name='tsaldoant' value= '$_POST[tsaldoant]'></td><td class='$co' align='right'>".number_format($_POST[tdebito],2,".",",")."<input type='hidden' name='tdebito' value= '$_POST[tdebito]'></td><td class='$co' align='right'>".number_format($_POST[tcredito],2,".",",")."<input type='hidden' name='tcredito' value= '$_POST[tcredito]'></td><td class='$co' align='right'>".number_format($_POST[tsaldofinal],2,".",",")."<input type='hidden' name='tsaldofinal' value= '$_POST[tsaldofinal]'></td></tr>";  
  $horafin=date('h:i:s');	
  echo "<DIV class='ejemplo'>INICIO:$horaini FINALIZO: $horafin</DIV>";
}

	foreach($cuentachipno as $cch)
	 {		
	  $ncta=existecuenta($cch);
	  echo "<div class='saludo3'>".strtoupper($cch)."  -  ".strtoupper($ncta)."</div>";		
	 }
?>
	</div>
 <?php
     }
	 if($_POST[gbalance]==1 && $_POST[gchip]!='1' )
	  {
		 $fechab=date('Y-m-d');
		 $linkbd=conectar_bd();
		 $sqlr="Delete from chipcarga_cab where vigencia=$_POST[vigencias] and periodo=$_POST[periodo]";
		 mysql_query($sqlr,$linkbd);
		 $sqlr="Delete from chipcarga_det where vigencia=$_POST[vigencias] and periodo=$_POST[periodo]";
		 mysql_query($sqlr,$linkbd);	
 		 $sqlr="insert into chipcarga_cab (entidad, nombre_entidad, vigencia, fecha, periodo, estado) values ('$_POST[nitentidad]','$_POST[entidad]','$_POST[vigencias]','$fechab','$_POST[periodo]','S') ";
		 mysql_query($sqlr,$linkbd);	
		 //echo "Error".mysql_error($linkbd); 
		 $cerror=0;
		 $cexit=0;
		 for($x=0;$x<count($_POST[dcuentas]);$x++)
		  {
			$sqlr="insert into chipcarga_det (entidad, vigencia, periodo, cuenta, saldoinicial, debitos, creditos, saldofinal, saldofincte, saldofincteno) values ('$_POST[nitentidad]','$_POST[vigencias]','$_POST[periodo]',".$_POST[dcuentas][$x].",".$_POST[dsaldoant][$x].",".$_POST[ddebitos][$x].",".$_POST[dcreditos][$x].",".$_POST[dsaldo][$x].",'','') ";
			if(mysql_query($sqlr,$linkbd))	
		   {
			$cexit+=1;
		   }
		   else
		    {
			$cerror+=1;
			//echo "$sqlr<br>Error".mysql_error($linkbd); 
			}
		  }	
		  echo "<div class='inicio'>CUENTAS INSERTADAS:".$cexit." <img src='imagenes\confirm.png'></div>";
  		  echo "<div class='inicio'>CUENTAS NO INSERTADAS:".$cerror." <img src='imagenes\alert.png'></div>";		 
	   } 
	//**FIN PASO 2
	//***** PASO 3 ****			 
	 //**** FIN PASO 3 ****
	//***** PASO 3 ****		 
	  if($_POST[gbalance]==1)
	 {
	  ?>      	
       	
      <table class="inicio">
      <tr><td class="titulos" colspan="8">CLASIFICAR CUENTAS</td></tr>
      <tr><td class="titulos2">CUENTA</td><td class="titulos2">NOMBRE</td><td class="titulos2">SALDO INICIAL</td><td class="titulos2">DEBITOS</td><td class="titulos2">CREDITOS</td><td class="titulos2">SALDO FINAL</td><td class="titulos2">CTE 
     <input id="todoscte" name="todoscte" type="checkbox" value="1" onClick="checktodos()"></td><td class="titulos2">NOCTE <input id="todoscten"  name="todoscten" type="checkbox" value="1" onClick="checktodosn()"></td></tr>
     <?php
	  $totalsalini=0;
	  $totaldeb=0;
	  $totalcred=0;
	  $totalsalfin=0;	  	  	  
	  $linkbd=conectar_bd();
	  $sqlrc="select distinct cuenta, sum(saldoinicial),sum(debitos), sum(creditos), sum(saldofinal) from chipcarga_det where vigencia=$_POST[vigencias] and periodo=$_POST[periodo] group by cuenta";
	  $resc=mysql_query($sqlrc,$linkbd);
	  
	  while($rowc=mysql_fetch_row($resc))
	   {
		   $ncta=existecuenta($rowc[0]);		   
	  echo "<tr class='saludo3'><td ><input type='hidden' name='cuentac[]' value='$rowc[0]'>$rowc[0]</td><td><input type='hidden' name='nomcuenta[]' value='$ncta'> $ncta</td><td><input type='hidden' name='saldoinic[]' value='$rowc[1]'>$rowc[1]</td><td ><input type='hidden' name='debitosc[]' value='$rowc[2]'>$rowc[2]</td><td ><input type='hidden' name='creditosc[]' value='$rowc[3]'>$rowc[3]</td><td ><input type='hidden' name='saldofinc[]' value='$rowc[4]'>$rowc[4]</td>";
	  $chk='';
	  $chk2='';
	  if(strlen($rowc[0])==6)
	  {
		  $ch=esta_en_array($_POST[ctes], $rowc[0]);
			if($ch==1)
			 {
			 $chk="checked";
			 }
		  $ch2=esta_en_array($_POST[nctes], $rowc[0]);
			if($ch2==1)
			 {
			 $chk2="checked";
			 }	  
			 $totalsalini+=$rowc[1]; 
	  $totaldeb+=$rowc[2]; 	  
	  $totalcred+=$rowc[3]; 
	  $totalsalfin+=$rowc[4]; 
		echo "<td ><center><input type='checkbox' name='ctes[]' value='$rowc[0]' onClick='actdes($rowc[0],1)' $chk></center></td><td><center><input type='checkbox' name='nctes[]' value='$rowc[0]' onClick='actdes($rowc[0],2)' $chk2></center></td></tr>";  
	  }
	  else 
	   {
	  	echo "<td ></td><td></td></tr>";
	   }
	   }
	   
	  ?>
      <tr class="saludo3"><td></td><td></td><td><?php echo $totalsalini ?></td><td><?php echo $totaldeb ?></td><td><?php echo $totalcred ?></td><td><?php echo $totalsalfin ?>  <input type="hidden" name="gchip" value="<?php //echo $_POST[gchip] ?>"></td></tr>
      </table>
                
    <!-- <div class='inicio'>Guardar y Finalizar Consolidado <input name="finalizar" type="button" value="Finalizar" onClick="guardarchip()" ></div>      --> 
        <?php
		if($_POST[gchip]=='1')
		 {
		 $namearch="archivos/".$_SESSION[usuario]."chip-$_POST[periodo].csv";
		 $Descriptor1 = fopen($namearch,"w+"); 
		 fputs($Descriptor1,"(S) C�digo Contable; (C) Nombre De La Cuenta; (D) Saldo Anterior; (D) D�bito; (D) Cr�dito;(D) Saldo Corriente; (D) Saldo No Corriente \r\n");
		for($x=0;$x<count($_POST[cuentac]);$x++)
		 {	 
		  $signo=cuenta_colocar_signo($_POST[cuentac][$x]);
		  if(strlen($_POST[cuentac][$x])==6)
		  {
			$nombrecuenta=$_POST[nomcuenta][$x];
			$chk="";  
		    $chk2="";
			$salfinal=0;
		   $ch=esta_en_array($_POST[ctes], $_POST[cuentac][$x]);
		   $saldoinicial=round($_POST[saldoinic][$x]/1000,0);
		   $debitos=round($_POST[debitosc][$x]/1000,0);
		   $creditos=round($_POST[creditosc][$x]/1000,0);
		   $salfinal=($saldoinicial+$debitos-$creditos); 
				 if(substr($_POST[cuentac][$x],0,1)>1)
				   $saldoinicial=abs($saldoinicial);
				   else
					 $saldoinicial=$saldoinicial;
					$salfinal=abs($salfinal); 
					$salfinal=$salfinal*$signo;		
					if($ch==1)
				 {				
			  $ncuentas=substr($_POST[cuentac][$x],0,1)."".substr($_POST[cuentac][$x],1,1)."".substr($_POST[cuentac][$x],2,2)."".substr($_POST[cuentac][$x],4,2); 
			   fputs($Descriptor1,"".$ncuentas.";".$nombrecuenta.";".abs($saldoinicial).";".$debitos.";". $creditos.";".$salfinal.";0\r\n");
			   $chk="checked";
			   //echo "<br>D;".$ncuentas.";".abs($saldoinicial).";".round($_POST[debitosc][$x]/1000,0).";".round($_POST[creditosc][$x]/1000,0).";".$salfinal.";".$salfinal.";0";
			   }
		  $ch2=esta_en_array($_POST[nctes], $_POST[cuentac][$x]);
			if($ch2==1)
			 {
			//$salfinal=($_POST[saldoinic][$x]+$_POST[debitosc][$x]-$_POST[creditosc][$x])/1000; 
			$ncuentas=substr($_POST[cuentac][$x],0,1)."".substr($_POST[cuentac][$x],1,1)."".substr($_POST[cuentac][$x],2,2)."".substr($_POST[cuentac][$x],4,2); 
			  fputs($Descriptor1,"".$ncuentas.";".$nombrecuenta.";".abs($saldoinicial).";". $debitos.";". $creditos.";0;".$salfinal.";\r\n");
			 $chk2="checked";
			//  echo "<br>D;".$ncuentas.";".abs($saldoinicial).";".($_POST[debitosc][$x]/1000).";".($_POST[creditosc][$x]/1000).";".$salfinal.";".$salfinal.";0";
			 }	  			 
		  }
		  fclose($namearch);
		 }
		?> 
        </div>
	<div class='inicio'>Formato CHIP da Click <a href="<?php echo "archivos/".$_SESSION[usuario]."chip-$_POST[periodo].csv"; ?>" target="_blank"><img src="imagenes/csv.png"  alt="csv"></a></div>
     <?php
					}
				}
				?>
				
				<?php
	break;
	
   }
 $sqlr="select *from pptocuentaspptoinicial where cuenta=$_POST[cuenta] and vigencia=".$vigusu;	
}
?>
</div></form></td></tr>
</table>
</body>
</html