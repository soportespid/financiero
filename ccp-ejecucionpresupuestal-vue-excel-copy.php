<?php
	require_once 'PHPExcel/Classes/PHPExcel.php';
    include '/PHPExcel/Classes/PHPExcel/IOFactory.php';
	require "comun.inc";
	require "funciones.inc";
    ini_set('max_execution_time',99999999);
    header("Content-type: application/json");
	session_start();
	$linkbd=conectar_v7();
    $linkbd -> set_charset("utf8");

    $sqlr="select *from configbasica where estado='S' ";
    $res=mysqli_query($linkbd, $sqlr);
    while($row=mysqli_fetch_row($res))
    {
        $nit=$row[0];
        $rs=$row[1];
    }

	$objPHPExcel = new PHPExcel();
	
	$objPHPExcel->getProperties()
		->setCreator("IDEAL10")
		->setLastModifiedBy("IDEAL10")
		->setTitle("EJECUCION PRESUPUESTAL DE GASTOS")
		->setSubject("CCP")
		->setDescription("CCP")
		->setKeywords("CCP")
		->setCategory("PRESUPUESTO CCP");
	$objPHPExcel->getActiveSheet()->mergeCells('A1:X1');
	$objPHPExcel->setActiveSheetIndex(0)->setCellValue('A1', 'Informe de ejecución presupuestal de gastos');
	$objFont=$objPHPExcel->getActiveSheet()->getStyle('A1')->getFont();
	$objFont->setName('Courier New');
	$objFont->setSize(15);
	$objFont->setBold(true);
	$objFont->setUnderline(PHPExcel_Style_Font::UNDERLINE_SINGLE);
	$objFont->getColor()->setARGB( PHPExcel_Style_Color::COLOR_WHITE);
	$objAlign=$objPHPExcel->getActiveSheet()->getStyle('A1')->getAlignment(); 
	$objAlign->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
	$objAlign->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);

    $objPHPExcel->getActiveSheet()->mergeCells('A2:X2');
	$objPHPExcel->setActiveSheetIndex(0)->setCellValue('A2', $nit.' - '.$rs);
	$objFont=$objPHPExcel->getActiveSheet()->getStyle('A2')->getFont();
	$objFont->setName('Courier New');
	$objFont->setSize(15);
	$objFont->setBold(true);
	$objFont->setUnderline(PHPExcel_Style_Font::UNDERLINE_SINGLE);
	$objFont->getColor()->setARGB( PHPExcel_Style_Color::COLOR_WHITE);
	$objAlign=$objPHPExcel->getActiveSheet()->getStyle('A2')->getAlignment(); 
	$objAlign->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
	$objAlign->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);

    $objPHPExcel->getActiveSheet()->mergeCells('A3:X3');
	$objPHPExcel->setActiveSheetIndex(0)->setCellValue('A3', 'Fecha inicial: '.$_POST['fechaini'].' - Fecha final: '.$_POST['fechafin']);
	$objFont=$objPHPExcel->getActiveSheet()->getStyle('A3')->getFont();
	$objFont->setName('Courier New');
	$objFont->setSize(15);
	$objFont->setBold(true);
	$objFont->setUnderline(PHPExcel_Style_Font::UNDERLINE_SINGLE);
	$objFont->getColor()->setARGB( PHPExcel_Style_Color::COLOR_WHITE);
	$objAlign=$objPHPExcel->getActiveSheet()->getStyle('A3')->getAlignment(); 
	$objAlign->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
	$objAlign->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);


	$objPHPExcel-> getActiveSheet ()
		-> getStyle ("A4:X4")
		-> getFill ()
		-> setFillType (PHPExcel_Style_Fill :: FILL_SOLID)
		-> getStartColor ()
		-> setRGB ('368986');

	$objPHPExcel-> getActiveSheet ()
		-> getStyle ("A1")	
		-> getFill ()
		-> setFillType (PHPExcel_Style_Fill :: FILL_SOLID)
		-> getStartColor ()
		-> setRGB ('e79a32');

    $objPHPExcel-> getActiveSheet ()
		-> getStyle ("A2")	
		-> getFill ()
		-> setFillType (PHPExcel_Style_Fill :: FILL_SOLID)
		-> getStartColor ()
		-> setRGB ('e79a32');
    
    $objPHPExcel-> getActiveSheet ()
		-> getStyle ("A3")	
		-> getFill ()
		-> setFillType (PHPExcel_Style_Fill :: FILL_SOLID)
		-> getStartColor ()
		-> setRGB ('e79a32');
    
    $bordersTitle = array(
        'borders' => array(
            'allborders' => array(
                'style' => PHPExcel_Style_Border::BORDER_THIN,
                'color' => array('argb' => 'ffffffff'),
            )
        ),
        'font' => array(
            'bold' => true,
            'size' => 12,
            'color' => array('argb' => 'ffffffff'),
        ),
    );

	$borders = array(
		'borders' => array(
			'allborders' => array(
				'style' => PHPExcel_Style_Border::BORDER_THIN,
				'color' => array('argb' => 'FF000000'),
			)
		),
	);

    $borders2 = array(
		'borders' => array(
			'allborders' => array(
				'style' => PHPExcel_Style_Border::BORDER_THIN,
				'color' => array('argb' => 'FF000000'),
			)
		),
        'font' => array(
            'bold' => true
        ),
	);
 

	$objPHPExcel->getActiveSheet()->getStyle('A4:X4')->applyFromArray($bordersTitle);
	$objPHPExcel->setActiveSheetIndex(0)
        ->setCellValue('A4', 'Sección')
        ->setCellValue('B4', 'Dependencia')
        ->setCellValue('C4', 'Area')
        ->setCellValue('D4', 'Vig gasto')
        ->setCellValue('E4', 'Rubro')
        ->setCellValue('F4', 'Nombre')
        ->setCellValue('G4', 'Tipo')
        ->setCellValue('H4', 'Fuente')
        ->setCellValue('I4', 'Nombre fuente')
        ->setCellValue('J4', 'Medio de pago')
        ->setCellValue('K4', 'Presupuesto Inicial')
        ->setCellValue('L4', 'Adiciones')
        ->setCellValue('M4', 'Reducciones')
        ->setCellValue('N4', 'Creditos')
        ->setCellValue('O4', 'Contracreditos')
        ->setCellValue('P4', 'Definitivo')
        ->setCellValue('Q4', 'Disponibilidades')
        ->setCellValue('R4', 'Compromisos')
        ->setCellValue('S4', 'Saldos por comprometer')
        ->setCellValue('T4', 'Obligaciones')
        ->setCellValue('U4', 'Compromisos en ejecucion')
        ->setCellValue('V4', 'Pagos')
        ->setCellValue('W4', 'Cuentas por pagar')
        ->setCellValue('X4', 'Saldos por disponer');

	$i=5;

    $tamTotal = count($_POST['rubroTot']);
    if($tamTotal > 0){
        $objPHPExcel-> getActiveSheet ()
		-> getStyle ("A$i:X$i")
		-> getFill ()
		-> setFillType (PHPExcel_Style_Fill :: FILL_SOLID)
		-> getStartColor ()
		-> setRGB ('31ada1');
    }

    for($ii=0;$ii<$tamTotal;$ii++)
	{
        $saldosPorComprometer = round(($_POST['definitivoTot'][$ii]-$_POST['compromisoTot'][$ii]), 2);
        $compromisosEnEjecucion = round(($_POST['compromisoTot'][$ii]-$_POST['obligacionTot'][$ii]), 2);
        $cuentasPorPagar = round(($_POST['obligacionTot'][$ii]-$_POST['egresoTot'][$ii]), 2);
		$objPHPExcel->setActiveSheetIndex(0)
		->setCellValueExplicit ("A$i", '', PHPExcel_Cell_DataType :: TYPE_STRING)
        ->setCellValueExplicit ("B$i", $_POST['sec_presupuestalTot'][$ii], PHPExcel_Cell_DataType :: TYPE_STRING)
        ->setCellValueExplicit ("C$i", '', PHPExcel_Cell_DataType :: TYPE_STRING)
		->setCellValueExplicit ("D$i", $_POST['vigencia_gastoTot'][$ii], PHPExcel_Cell_DataType :: TYPE_STRING)
		->setCellValueExplicit ("E$i", $_POST['rubroTot'][$ii], PHPExcel_Cell_DataType :: TYPE_STRING)
		->setCellValueExplicit ("F$i", $_POST['nombreRubroTot'][$ii], PHPExcel_Cell_DataType :: TYPE_STRING)
        ->setCellValueExplicit ("G$i", $_POST['tipoTot'][$ii], PHPExcel_Cell_DataType :: TYPE_STRING)
        ->setCellValueExplicit ("H$i", $_POST['fuenteTot'][$ii], PHPExcel_Cell_DataType :: TYPE_STRING)
        ->setCellValueExplicit ("I$i", '', PHPExcel_Cell_DataType :: TYPE_STRING)
        ->setCellValueExplicit ("J$i", $_POST['csfTot'][$ii], PHPExcel_Cell_DataType :: TYPE_STRING)
        ->setCellValueExplicit ("K$i", $_POST['inicialTot'][$ii], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
        ->setCellValueExplicit ("L$i", $_POST['adicionTot'][$ii], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
        ->setCellValueExplicit ("M$i", $_POST['reduccionTot'][$ii], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
        ->setCellValueExplicit ("N$i", $_POST['creditoTot'][$ii], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
        ->setCellValueExplicit ("O$i", $_POST['contraCreditoTot'][$ii], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
        ->setCellValueExplicit ("P$i", $_POST['definitivoTot'][$ii], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
        ->setCellValueExplicit ("Q$i", $_POST['disponibilidadTot'][$ii], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
        ->setCellValueExplicit ("R$i", $_POST['compromisoTot'][$ii], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
        ->setCellValueExplicit ("S$i", $saldosPorComprometer, PHPExcel_Cell_DataType :: TYPE_NUMERIC)
        ->setCellValueExplicit ("T$i", $_POST['obligacionTot'][$ii], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
        ->setCellValueExplicit ("U$i", $compromisosEnEjecucion, PHPExcel_Cell_DataType :: TYPE_NUMERIC)
        ->setCellValueExplicit ("V$i", $_POST['egresoTot'][$ii], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
        ->setCellValueExplicit ("W$i", $cuentasPorPagar, PHPExcel_Cell_DataType :: TYPE_NUMERIC)
        ->setCellValueExplicit ("X$i", $_POST['saldoTot'][$ii], PHPExcel_Cell_DataType :: TYPE_NUMERIC);

        
		
        if ($_POST['tipo'][$ii] == "A") {
            $objPHPExcel->getActiveSheet()->getStyle("A$i:X$i")->applyFromArray($borders2);
        }
        else {
            $objPHPExcel->getActiveSheet()->getStyle("A$i:X$i")->applyFromArray($borders);
        }

        $objPHPExcel->getActiveSheet()->getStyle("I$i:X$i")->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);
		
        $i++;
	}

    $tamTotalFun = count($_POST['rubroFun']);
    if($tamTotalFun > 0){
        $objPHPExcel-> getActiveSheet ()
		-> getStyle ("A$i:X$i")
		-> getFill ()
		-> setFillType (PHPExcel_Style_Fill :: FILL_SOLID)
		-> getStartColor ()
		-> setRGB ('8bd1a9');
    }

    for($ii=0;$ii<$tamTotalFun;$ii++)
	{
        $saldosPorComprometer = round(($_POST['definitivoFun'][$ii]-$_POST['compromisoFun'][$ii]), 2);
        $compromisosEnEjecucion = round(($_POST['compromisoFun'][$ii]-$_POST['obligacionFun'][$ii]), 2);
        $cuentasPorPagar = round(($_POST['obligacionFun'][$ii]-$_POST['egresoFun'][$ii]), 2);
		$objPHPExcel->setActiveSheetIndex(0)
		->setCellValueExplicit ("A$i", '', PHPExcel_Cell_DataType :: TYPE_STRING)
        ->setCellValueExplicit ("B$i", $_POST['sec_presupuestalFun'][$ii], PHPExcel_Cell_DataType :: TYPE_STRING)
        ->setCellValueExplicit ("C$i", '', PHPExcel_Cell_DataType :: TYPE_STRING)
		->setCellValueExplicit ("D$i", $_POST['vigencia_gastoFun'][$ii], PHPExcel_Cell_DataType :: TYPE_STRING)
		->setCellValueExplicit ("E$i", $_POST['rubroFun'][$ii], PHPExcel_Cell_DataType :: TYPE_STRING)
		->setCellValueExplicit ("F$i", $_POST['nombreRubroFun'][$ii], PHPExcel_Cell_DataType :: TYPE_STRING)
        ->setCellValueExplicit ("G$i", $_POST['tipoFun'][$ii], PHPExcel_Cell_DataType :: TYPE_STRING)
        ->setCellValueExplicit ("H$i", $_POST['fuenteFun'][$ii], PHPExcel_Cell_DataType :: TYPE_STRING)
        ->setCellValueExplicit ("I$i", '', PHPExcel_Cell_DataType :: TYPE_STRING)
        ->setCellValueExplicit ("J$i", $_POST['csfFun'][$ii], PHPExcel_Cell_DataType :: TYPE_STRING)
        ->setCellValueExplicit ("K$i", $_POST['inicialFun'][$ii], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
        ->setCellValueExplicit ("L$i", $_POST['adicionFun'][$ii], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
        ->setCellValueExplicit ("M$i", $_POST['reduccionFun'][$ii], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
        ->setCellValueExplicit ("N$i", $_POST['creditoFun'][$ii], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
        ->setCellValueExplicit ("O$i", $_POST['contraCreditoFun'][$ii], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
        ->setCellValueExplicit ("P$i", $_POST['definitivoFun'][$ii], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
        ->setCellValueExplicit ("Q$i", $_POST['disponibilidadFun'][$ii], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
        ->setCellValueExplicit ("R$i", $_POST['compromisoFun'][$ii], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
        ->setCellValueExplicit ("S$i", $saldosPorComprometer, PHPExcel_Cell_DataType :: TYPE_NUMERIC)
        ->setCellValueExplicit ("T$i", $_POST['obligacionFun'][$ii], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
        ->setCellValueExplicit ("U$i", $compromisosEnEjecucion, PHPExcel_Cell_DataType :: TYPE_NUMERIC)
        ->setCellValueExplicit ("V$i", $_POST['egresoFun'][$ii], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
        ->setCellValueExplicit ("W$i", $cuentasPorPagar, PHPExcel_Cell_DataType :: TYPE_NUMERIC)
        ->setCellValueExplicit ("X$i", $_POST['saldoFun'][$ii], PHPExcel_Cell_DataType :: TYPE_NUMERIC);

        
		
        if ($_POST['tipo'][$ii] == "A") {
            $objPHPExcel->getActiveSheet()->getStyle("A$i:X$i")->applyFromArray($borders2);
        }
        else {
            $objPHPExcel->getActiveSheet()->getStyle("A$i:X$i")->applyFromArray($borders);
        }

        $objPHPExcel->getActiveSheet()->getStyle("I$i:X$i")->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);
		
        $i++;
	}

    $tamTotalInv = count($_POST['rubroInv']);
    if($tamTotalInv > 0){
        $objPHPExcel-> getActiveSheet ()
		-> getStyle ("A$i:X$i")
		-> getFill ()
		-> setFillType (PHPExcel_Style_Fill :: FILL_SOLID)
		-> getStartColor ()
		-> setRGB ('8bd1a9'); 
    }

    for($ii=0;$ii<$tamTotalInv;$ii++)
	{
        $saldosPorComprometer = round(($_POST['definitivoInv'][$ii]-$_POST['compromisoInv'][$ii]), 2);
        $compromisosEnEjecucion = round(($_POST['compromisoInv'][$ii]-$_POST['obligacionInv'][$ii]), 2);
        $cuentasPorPagar = round(($_POST['obligacionInv'][$ii]-$_POST['egresoInv'][$ii]), 2);
		$objPHPExcel->setActiveSheetIndex(0)
		->setCellValueExplicit ("A$i", '', PHPExcel_Cell_DataType :: TYPE_STRING)
        ->setCellValueExplicit ("B$i", $_POST['sec_presupuestalInv'][$ii], PHPExcel_Cell_DataType :: TYPE_STRING)
        ->setCellValueExplicit ("C$i", '', PHPExcel_Cell_DataType :: TYPE_STRING)
		->setCellValueExplicit ("D$i", $_POST['vigencia_gastoInv'][$ii], PHPExcel_Cell_DataType :: TYPE_STRING)
		->setCellValueExplicit ("E$i", $_POST['rubroInv'][$ii], PHPExcel_Cell_DataType :: TYPE_STRING)
		->setCellValueExplicit ("F$i", $_POST['nombreRubroInv'][$ii], PHPExcel_Cell_DataType :: TYPE_STRING)
        ->setCellValueExplicit ("G$i", $_POST['tipoInv'][$ii], PHPExcel_Cell_DataType :: TYPE_STRING)
        ->setCellValueExplicit ("H$i", $_POST['fuenteInv'][$ii], PHPExcel_Cell_DataType :: TYPE_STRING)
        ->setCellValueExplicit ("I$i", '', PHPExcel_Cell_DataType :: TYPE_STRING)
        ->setCellValueExplicit ("J$i", $_POST['csfInv'][$ii], PHPExcel_Cell_DataType :: TYPE_STRING)
        ->setCellValueExplicit ("K$i", $_POST['inicialInv'][$ii], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
        ->setCellValueExplicit ("L$i", $_POST['adicionInv'][$ii], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
        ->setCellValueExplicit ("M$i", $_POST['reduccionInv'][$ii], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
        ->setCellValueExplicit ("N$i", $_POST['creditoInv'][$ii], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
        ->setCellValueExplicit ("O$i", $_POST['contraCreditoInv'][$ii], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
        ->setCellValueExplicit ("P$i", $_POST['definitivoInv'][$ii], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
        ->setCellValueExplicit ("Q$i", $_POST['disponibilidadInv'][$ii], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
        ->setCellValueExplicit ("R$i", $_POST['compromisoInv'][$ii], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
        ->setCellValueExplicit ("S$i", $saldosPorComprometer, PHPExcel_Cell_DataType :: TYPE_NUMERIC)
        ->setCellValueExplicit ("T$i", $_POST['obligacionInv'][$ii], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
        ->setCellValueExplicit ("U$i", $compromisosEnEjecucion, PHPExcel_Cell_DataType :: TYPE_NUMERIC)
        ->setCellValueExplicit ("V$i", $_POST['egresoInv'][$ii], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
        ->setCellValueExplicit ("W$i", $cuentasPorPagar, PHPExcel_Cell_DataType :: TYPE_NUMERIC)
        ->setCellValueExplicit ("X$i", $_POST['saldoInv'][$ii], PHPExcel_Cell_DataType :: TYPE_NUMERIC);

        
		
        if ($_POST['tipo'][$ii] == "A") {
            $objPHPExcel->getActiveSheet()->getStyle("A$i:X$i")->applyFromArray($borders2);
        }
        else {
            $objPHPExcel->getActiveSheet()->getStyle("A$i:X$i")->applyFromArray($borders);
        }

        $objPHPExcel->getActiveSheet()->getStyle("I$i:X$i")->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);
		
        $i++;
	}
    
	for($ii=0;$ii<count ($_POST['rubro']);$ii++)
	{
        $saldosPorComprometer = round(($_POST['definitivo'][$ii]-$_POST['compromiso'][$ii]), 2);
        $compromisosEnEjecucion = round(($_POST['compromiso'][$ii]-$_POST['obligacion'][$ii]), 2);
        $cuentasPorPagar = round(($_POST['obligacion'][$ii]-$_POST['egreso'][$ii]), 2);
		$objPHPExcel->setActiveSheetIndex(0)
		->setCellValueExplicit ("A$i", $_POST['unida_ejecutora'][$ii].' - '.$_POST['unida_ejecutora_nombre'][$ii], PHPExcel_Cell_DataType :: TYPE_STRING)
        ->setCellValueExplicit ("B$i", $_POST['sec_presupuestal'][$ii], PHPExcel_Cell_DataType :: TYPE_STRING)
        ->setCellValueExplicit ("C$i", $_POST['area'][$ii]."-".$_POST['nombre_area'][$ii], PHPExcel_Cell_DataType :: TYPE_STRING)
		->setCellValueExplicit ("D$i", $_POST['vigencia_gasto'][$ii], PHPExcel_Cell_DataType :: TYPE_STRING)
		->setCellValueExplicit ("E$i", $_POST['rubro'][$ii], PHPExcel_Cell_DataType :: TYPE_STRING)
		->setCellValueExplicit ("F$i", $_POST['nombreRubro'][$ii], PHPExcel_Cell_DataType :: TYPE_STRING)
        ->setCellValueExplicit ("G$i", $_POST['tipo'][$ii], PHPExcel_Cell_DataType :: TYPE_STRING)
        ->setCellValueExplicit ("H$i", $_POST['fuente'][$ii], PHPExcel_Cell_DataType :: TYPE_STRING)
        ->setCellValueExplicit ("I$i", $_POST['nombreFuente'][$ii], PHPExcel_Cell_DataType :: TYPE_STRING)
        ->setCellValueExplicit ("J$i", $_POST['csf'][$ii], PHPExcel_Cell_DataType :: TYPE_STRING)
        ->setCellValueExplicit ("K$i", $_POST['inicial'][$ii], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
        ->setCellValueExplicit ("L$i", $_POST['adicion'][$ii], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
        ->setCellValueExplicit ("M$i", $_POST['reduccion'][$ii], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
        ->setCellValueExplicit ("N$i", $_POST['credito'][$ii], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
        ->setCellValueExplicit ("O$i", $_POST['contraCredito'][$ii], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
        ->setCellValueExplicit ("P$i", $_POST['definitivo'][$ii], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
        ->setCellValueExplicit ("Q$i", $_POST['disponibilidad'][$ii], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
        ->setCellValueExplicit ("R$i", $_POST['compromiso'][$ii], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
        ->setCellValueExplicit ("S$i", $saldosPorComprometer, PHPExcel_Cell_DataType :: TYPE_NUMERIC)
        ->setCellValueExplicit ("T$i", $_POST['obligacion'][$ii], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
        ->setCellValueExplicit ("U$i", $compromisosEnEjecucion, PHPExcel_Cell_DataType :: TYPE_NUMERIC)
        ->setCellValueExplicit ("V$i", $_POST['egreso'][$ii], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
        ->setCellValueExplicit ("W$i", $cuentasPorPagar, PHPExcel_Cell_DataType :: TYPE_NUMERIC)
        ->setCellValueExplicit ("X$i", $_POST['saldo'][$ii], PHPExcel_Cell_DataType :: TYPE_NUMERIC);

        
		
        if ($_POST['tipo'][$ii] == "A") {
            $objPHPExcel->getActiveSheet()->getStyle("A$i:X$i")->applyFromArray($borders2);
        }
        else {
            $objPHPExcel->getActiveSheet()->getStyle("A$i:X$i")->applyFromArray($borders);
        }

        $objPHPExcel->getActiveSheet()->getStyle("I$i:X$i")->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);
		
        $i++;
	}

	$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(10);
	$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(8);
	$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(8);
	$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(8);
	$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(80);
    $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);
    $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(10);
    $objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(50);
    $objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(8);
    $objPHPExcel->getActiveSheet()->getColumnDimension('K')->setAutoSize(true);
    $objPHPExcel->getActiveSheet()->getColumnDimension('L')->setAutoSize(true);
    $objPHPExcel->getActiveSheet()->getColumnDimension('M')->setAutoSize(true);
    $objPHPExcel->getActiveSheet()->getColumnDimension('N')->setAutoSize(true);
    $objPHPExcel->getActiveSheet()->getColumnDimension('O')->setAutoSize(true);
    $objPHPExcel->getActiveSheet()->getColumnDimension('P')->setAutoSize(true);
    $objPHPExcel->getActiveSheet()->getColumnDimension('Q')->setAutoSize(true);
    $objPHPExcel->getActiveSheet()->getColumnDimension('R')->setAutoSize(true);
    $objPHPExcel->getActiveSheet()->getColumnDimension('S')->setAutoSize(true);
    $objPHPExcel->getActiveSheet()->getColumnDimension('T')->setAutoSize(true);
    $objPHPExcel->getActiveSheet()->getColumnDimension('U')->setAutoSize(true);
    $objPHPExcel->getActiveSheet()->getColumnDimension('V')->setAutoSize(true);
    $objPHPExcel->getActiveSheet()->getColumnDimension('W')->setAutoSize(true);
    $objPHPExcel->getActiveSheet()->getColumnDimension('X')->setAutoSize(true);

	$objPHPExcel->getActiveSheet()->setTitle('Ejecucion Gastos');
	//header('Content-Type: application/vnd.ms-excel');
    header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
	header('Content-Disposition: attachment;filename="informeEjecucionGastos.xlsx"');
	header('Cache-Control: max-age=0');
	$objWriter=PHPExcel_IOFactory::createWriter($objPHPExcel,'Excel2007');
	$objWriter->save('php://output');
	exit;
?>