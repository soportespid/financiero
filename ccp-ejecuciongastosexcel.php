<?php  
	require_once 'PHPExcel/Classes/PHPExcel.php';
	require "comun.inc";
	require "funciones.inc";
	session_start();
	$linkbd=conectar_v7(); 
	$linkbd_V7=conectar_v7(); 

	$objPHPExcel = new PHPExcel();
	//----Propiedades----
	$objPHPExcel->getProperties()
        ->setCreator("IDEAL")
        ->setLastModifiedBy("IDEAL")
        ->setTitle("Reporte Ejecucion Presupuestal Gastos")
        ->setSubject("Presupuesto")
        ->setDescription("Presupuesto")
        ->setKeywords("Presupuesto")
        ->setCategory("Presupuesto");
	//----Cuerpo de Documento----
	$objPHPExcel->getActiveSheet()->mergeCells('A1:V1');
	$objPHPExcel->setActiveSheetIndex(0)->setCellValue('A1', 'Ejecucion presupuestal gastos');

	$objFont=$objPHPExcel->getActiveSheet()->getStyle('A1')->getFont();
	$objFont->setName('Courier New'); 
	$objFont->setSize(15); 
	$objFont->setBold(true); 
	$objFont->setUnderline(PHPExcel_Style_Font::UNDERLINE_SINGLE);
	$objFont->getColor()->setARGB( PHPExcel_Style_Color::COLOR_BLACK);

	$objAlign=$objPHPExcel->getActiveSheet()->getStyle('A1')->getAlignment(); 
	$objAlign->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER); 
	$objAlign->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER); 
	$objPHPExcel-> getActiveSheet ()
		-> getStyle ("A2:V2")	
        -> getFill ()
        -> setFillType (PHPExcel_Style_Fill :: FILL_SOLID)
        -> getStartColor ()
        -> setRGB ('A6E5F3');
	$objPHPExcel-> getActiveSheet ()
		-> getStyle ("A1")	
        -> getFill ()
        -> setFillType (PHPExcel_Style_Fill :: FILL_SOLID)
        -> getStartColor ()
        -> setRGB ('A6E5F3');
	$borders = array(
      'borders' => array(
        'allborders' => array(
          'style' => PHPExcel_Style_Border::BORDER_THIN,
          'color' => array('argb' => 'FF000000'),
        )
      ),
	);
	
	$styleArray = array(
		"font"  => array(
		"bold" => false,
		"color" => array("rgb" => "FF0000"),
		"size"  => 12,
		"name" => "Verdana"
		));
	$objPHPExcel->getActiveSheet()->getStyle('A2:V2')->applyFromArray($borders);
	$objPHPExcel->setActiveSheetIndex(0)
	->setCellValue('A2', 'MEDIO PAGO')
	->setCellValue('B2', 'UNIDAD')
	->setCellValue('C2', 'SECTOR')
	->setCellValue('D2', 'CODIGO')
	->setCellValue('E2', 'FUENTE')
	->setCellValue('F2', 'PROGRAMATICO')
	->setCellValue('G2', 'BIENES/SERVICIOS')
	->setCellValue('H2', 'NOMBRE')
	->setCellValue('I2', 'META PDM')
	->setCellValue('J2', 'PRESUPUESTO INICIAL')
	->setCellValue('K2', 'ADICION')
	->setCellValue('L2', 'REDUCCION')
	->setCellValue('M2', 'CREDITO')
	->setCellValue('N2', 'CONTRACREDITO')
	->setCellValue('O2', 'DEFINITIVO')
	->setCellValue('P2', 'DISPONIBILIDAD')
	->setCellValue('Q2', 'COMPROMISOS')
	->setCellValue('R2', 'OBLIGACION')
	->setCellValue('S2', 'COMPROMISOS EN EJECUCION')
	->setCellValue('T2', 'PAGOS')
	->setCellValue('U2', 'CUENTAS POR PAGAR')
	->setCellValue('V2', 'SALDO');
	
	$i=3;
	//echo count($_POST[codigo])."holaaa";
	$crit = '';
	$critBienesTransportables = '';
    if($_POST['unidadEjecutora'] != ''){
        $crit = "AND unidad = '$_POST[unidadEjecutora]'";
		$critBienesTransportables = "AND CB.unidad = '$_POST[unidadEjecutora]'";
    }

    $crit1 = '';
    
    if($_POST['medioDePago'] != ''){
        $crit1 = "AND medioPago = '$_POST[medioDePago]'";
        

	}

	$critinv1 = '';
	$crit1BienesTransportables = '';
    if($_POST['medioDePago'] != ''){
        $critinv1 = "AND medio_pago = '$_POST[medioDePago]'";
		$crit1BienesTransportables = " AND CB.medioPago = '$_POST[medioDePago]'";
	}

	preg_match("/([0-9]{2})\/([0-9]{2})\/([0-9]{4})/", $_POST['fecha'],$fecha);
	$fechaIni=''.$fecha[3].'-'.$fecha[2].'-'.$fecha[1].'';

	preg_match("/([0-9]{2})\/([0-9]{2})\/([0-9]{4})/", $_POST['fechafin'],$fechaf);
	$fechaFin=''.$fechaf[3].'-'.$fechaf[2].'-'.$fechaf[1].'';

	$vigencia = $fecha[3];

    for($xx=0; $xx<count($_POST['codigo']); $xx++)
    {
        $objPHPExcel->setActiveSheetIndex(0)
		->setCellValueExplicit ("D$i", $_POST['codigo'][$xx], PHPExcel_Cell_DataType :: TYPE_STRING)
		->setCellValueExplicit ("H$i", $_POST['nombre'][$xx], PHPExcel_Cell_DataType :: TYPE_STRING)
		->setCellValueExplicit ("J$i", $_POST['presupuestoInicial'][$xx], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
		->setCellValueExplicit ("K$i", $_POST['adicion'][$xx], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
		->setCellValueExplicit ("L$i", $_POST['reduccion'][$xx], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
		->setCellValueExplicit ("M$i", $_POST['credito'][$xx], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
		->setCellValueExplicit ("N$i", $_POST['contracredito'][$xx], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
		->setCellValueExplicit ("O$i", $_POST['presupuestoDefinitivo'][$xx], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
		->setCellValueExplicit ("P$i", $_POST['cdp'][$xx], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
		->setCellValueExplicit ("Q$i", $_POST['rp'][$xx], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
		->setCellValueExplicit ("R$i", $_POST['cxp'][$xx], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
		->setCellValueExplicit ("S$i", $_POST['compromisoEnEjecucion'][$xx], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
		->setCellValueExplicit ("T$i", $_POST['egreso'][$xx], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
		->setCellValueExplicit ("U$i", $_POST['cuentasPorPagar'][$xx], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
		->setCellValueExplicit ("V$i", $_POST['saldo'][$xx], PHPExcel_Cell_DataType :: TYPE_NUMERIC);
		if($_POST['tipo'][$xx] == 'A'){
			$objPHPExcel->getActiveSheet()->getStyle("A$i:V$i")->getFont()->setBold(true);
		}else{
			$objPHPExcel->getActiveSheet()->getStyle("A$i:V$i");
		}

		//$sqlr = "SELECT fuente, medioPago, subclase, unidad, valor FROM ccpetinicialigastosbienestransportables WHERE cuenta = '".$_POST['codigo'][$xx]."' AND vigencia='$vigencia' $crit $crit1";

		$sqlr = "SELECT CB.fuente, CB.medioPago, CB.subclase, CB.unidad, CB.valor FROM ccpetinicialigastosbienestransportables AS CB WHERE CB.cuenta = '".$_POST['codigo'][$xx]."' AND CB.vigencia='$vigencia' $critBienesTransportables $crit1BienesTransportables UNION SELECT CA.fuente, CA.mediopago, CA.cuenta_clasificadora, CA.tipo, CA.valor FROM ccpetadiciones AS CA WHERE CA.cuenta = '".$_POST['codigo'][$xx]."' AND CA.vigencia='$vigencia' AND LEFT(CA.cuenta, 3)!='2.3' AND CA.clasificador='2' AND CA.cuenta_clasificadora!='' AND LEFT(CA.cuenta, 3)!='2.3' AND NOT EXISTS(SELECT 1 FROM ccpetinicialigastosbienestransportables AS CBT WHERE CBT.cuenta = '".$_POST['codigo'][$xx]."' AND CBT.vigencia='$vigencia' AND CA.cuenta_clasificadora = CBT.subclase AND CA.fuente = CBT.fuente) UNION SELECT CT.fuente, CT.mediopago, CT.productoservicio, CT.tipo, CT.valor FROM ccpettraslados AS CT WHERE CT.cuenta = '".$_POST['codigo'][$xx]."' AND CT.vigencia='$vigencia' AND CT.productoservicio!='' AND LENGTH(CT.productoservicio)=7 AND LEFT(CT.cuenta, 3)!='2.3' AND NOT EXISTS(SELECT 1 FROM ccpetinicialigastosbienestransportables AS CTT WHERE CTT.cuenta = '".$_POST['codigo'][$xx]."' AND CT.productoservicio = CTT.subclase AND CTT.vigencia='$vigencia' AND CT.fuente = CTT.fuente) AND NOT EXISTS(SELECT 1 FROM ccpetadiciones AS CAT WHERE CAT.cuenta = '".$_POST['codigo'][$xx]."' AND CT.productoservicio = CAT.cuenta_clasificadora AND CAT.vigencia='$vigencia' AND CT.fuente = CAT.fuente) GROUP BY CT.fuente, CT.mediopago, CT.productoservicio";
		//echo $sqlr."<br>";

		$result = mysqli_query($linkbd, $sqlr);
		if(mysqli_num_rows($result) > 0){
			while($row = mysqli_fetch_array($result)){

				$definitivo = 0;
				$compromisoEnEjecucion = 0;
				$saldo = 0;
				$cuentasPorPagar = 0;

				$inicial = 0;
				$adicion = 0;
				$reduccion = 0;
				$credito = 0;
				$contracredito = 0;
				$presupuestoDefinitivo = 0;
				$totalCDPEnt = 0;
				$totalRPEnt = 0;
				$totalCxPEnt = 0;
				$totalEgresoEnt = 0;
				$arregloRP=[];


				$sqlr_bienes = "SELECT titulo FROM ccpetbienestransportables WHERE grupo = '$row[2]'";
				$result_bienes = mysqli_query($linkbd, $sqlr_bienes);
				$row_bienes = mysqli_fetch_array($result_bienes);

				//============= AQUI INICIA EL PROCESO PARA CALCULAR LA ADICION =========//


				$sqlrAdicion = "SELECT SUM(valor) FROM ccpetadiciones WHERE cuenta = '".$_POST['codigo'][$xx]."' AND fecha BETWEEN '$fechaIni' AND '$fechaFin' AND vigencia='$vigencia' AND estado!='N' AND mediopago = '".$row[1]."' AND fuente = '".$row[0]."' AND cuenta_clasificadora = '".$row[2]."'";
				$resAdicion = mysqli_query($linkbd_V7, $sqlrAdicion);
				$rowAdicion = mysqli_fetch_row($resAdicion);
	
				$adicion = $rowAdicion[0];
	
				//============= AQUI FINALIZA EL PROCESO PARA CALCULAR LA ADICION =========//
	
				//============= AQUI INICIA EL PROCESO PARA CALCULAR LA REDUCCION =========//
				
				$sqlrReduccion = "SELECT SUM(valor) FROM ccpetreducciones WHERE cuenta = '".$_POST['codigo'][$xx]."' AND fecha BETWEEN '$fechaIni' AND '$fechaFin' AND vigencia='$vigencia' AND estado!='N' AND mediopago = '".$row[1]."' AND fuente = '".$row[0]."' AND cuenta_clasificadora = '".$row[2]."'";
				$resReduccion = mysqli_query($linkbd_V7, $sqlrReduccion);
				$rowReduccion = mysqli_fetch_row($resReduccion);
	
				$reduccion = $rowReduccion[0];

				$sqlrReduccion_n = "SELECT SUM(CR.valor) FROM ccpetreduccion AS CR, ccpetacuerdos AS CAC WHERE CR.rubro = '".$_POST['codigo'][$xx]."' AND CR.acuerdo = CAC.id_acuerdo AND CAC.fecha BETWEEN '$fechaIni' AND '$fechaFin' AND CR.vigencia='$vigencia' AND CR.medio_pago = '".$row[1]."' AND CR.fuente = '".$row[0]."' AND CR.clasificador = '".$row[2]."'";
				$resReduccion_n = mysqli_query($linkbd_V7, $sqlrReduccion_n);
				$rowReduccion_n = mysqli_fetch_row($resReduccion_n);

				$reduccion += $rowReduccion_n[0];
	
				//============= AQUI FINALIZA EL PROCESO PARA CALCULAR LA REDUCCION =========//
	
	
				//============= AQUI INICIA EL PROCESO PARA CALCULAR EL CREDITO =========//
				
				$sqlrCredito = "SELECT SUM(valor) FROM ccpettraslados WHERE cuenta = '".$_POST['codigo'][$xx]."' AND fecha BETWEEN '$fechaIni' AND '$fechaFin' AND vigencia='$vigencia' AND estado!='N' AND mediopago = '".$row[1]."' AND tipo='C' AND productoservicio = '".$row[2]."' AND fuente='".$row[0]."'";
				$resCredito = mysqli_query($linkbd_V7, $sqlrCredito);
				$rowCredito = mysqli_fetch_row($resCredito);
	
				$credito = $rowCredito[0];
	
				//============= AQUI FINALIZA EL PROCESO PARA CALCULAR EL CREDITO =========//
	
				//============= AQUI INICIA EL PROCESO PARA CALCULAR EL CONTRACREDITO =========//
	
				$sqlrContracreditoCredito = "SELECT SUM(valor) FROM ccpettraslados WHERE cuenta = '".$_POST['codigo'][$xx]."' AND fecha BETWEEN '$fechaIni' AND '$fechaFin' AND vigencia='$vigencia' AND estado!='N' AND mediopago = '".$row[1]."' AND tipo='R' AND productoservicio = '".$row[2]."' AND fuente='".$row[0]."'";
				$resContracreditoCredito = mysqli_query($linkbd_V7, $sqlrContracreditoCredito);
				$rowContracreditoCredito = mysqli_fetch_row($resContracreditoCredito);
	
				$contracredito = $rowContracreditoCredito[0];
	
				//============= AQUI FINALIZA EL PROCESO PARA CALCULAR EL CONTRACREDITO =========//
	
				//============= AQUI INICIA EL PROCESO PARA CALCULAR EL VALOR DE LOS CDPS =========//
	
	
				$sqlrCdp = "SELECT C.consvigencia, C.tipo_mov,D.valor,D.tipo_mov FROM ccpetcdp C, ccpetcdp_detalle D WHERE D.cuenta = '".$_POST['codigo'][$xx]."' AND D.vigencia='$vigencia' AND D.consvigencia=C.consvigencia AND C.vigencia='$vigencia' AND NOT(D.estado='N') AND D.valor>0 AND D.tipo_mov=C.tipo_mov AND D.tipo_mov='201' AND C.fecha BETWEEN '$fechaIni' AND '$fechaFin' AND D.fuente = '".$row[0]."' AND D.productoservicio = '".$row[2]."' AND D.medio_pago = '".$row[1]."' UNION SELECT C.consvigencia, C.tipo_mov,SUM(D.valor),D.tipo_mov FROM ccpetcdp C, ccpetcdp_detalle D WHERE D.cuenta = '".$_POST['codigo'][$xx]."' AND D.vigencia='$vigencia' AND D.consvigencia=C.consvigencia AND C.vigencia='$vigencia' AND NOT(D.estado='N') AND D.valor>0 AND D.tipo_mov=C.tipo_mov AND (D.tipo_mov='401' OR D.tipo_mov='402') AND D.fuente = '".$row[0]."' AND D.productoservicio = '".$row[2]."' AND D.medio_pago = '".$row[1]."' AND EXISTS(SELECT 1 FROM ccpetcdp DAUX WHERE DAUX.consvigencia=D.consvigencia AND DAUX.tipo_mov='201' AND DAUX.fecha BETWEEN '$fechaIni' AND '$fechaFin')";
				
				$resCdp = mysqli_query($linkbd_V7, $sqlrCdp);
				if(mysqli_num_rows($resCdp)!=0)
				{
					while($rowCdp = mysqli_fetch_row($resCdp))
					{
						if($rowCdp[3]=='201')
						{
							$totalCDPEnt+=round($rowCdp[2],2);
						}
						else if( substr($rowCdp[3],0,1) == '4' )
						{
							$totalCDPEnt-=round($rowCdp[2],2);
						}
					}
				}
	
				//============= AQUI FINALIZA EL PROCESO PARA CALCULAR EL VALOR DE LOS CDPS =========//
	
				//============= AQUI INICIA EL PROCESO PARA CALCULAR EL VALOR DE LOS RPS =========//
	
				
				$sqlrRp = "SELECT R.consvigencia,R.fecha,R.tipo_mov,RD.valor,RD.tipo_mov FROM ccpetrp R,ccpetrp_detalle RD where  R.vigencia='$vigencia' AND RD.cuenta = '".$_POST['codigo'][$xx]."' AND RD.consvigencia=R.consvigencia AND RD.vigencia='$vigencia'  AND NOT(R.estado='N') AND R.tipo_mov=RD.tipo_mov AND R.tipo_mov='201' AND RD.valor>0 AND R.fecha BETWEEN '$fechaIni' AND '$fechaFin' AND RD.fuente = '".$row[0]."' AND RD.productoservicio = '".$row[2]."' AND RD.medio_pago = '".$row[1]."' UNION SELECT R.consvigencia,R.fecha,R.tipo_mov,SUM(RD.valor),RD.tipo_mov FROM ccpetrp R,ccpetrp_detalle RD where  R.vigencia='$vigencia' AND RD.cuenta = '".$_POST['codigo'][$xx]."' AND RD.consvigencia=R.consvigencia AND RD.vigencia='$vigencia'  AND NOT(R.estado='N') AND R.tipo_mov=RD.tipo_mov AND (R.tipo_mov='401' OR R.tipo_mov='402') AND RD.valor>0 AND EXISTS(SELECT 1 FROM ccpetrp RAUX WHERE RAUX.consvigencia=R.consvigencia AND RAUX.tipo_mov='201' AND RAUX.fecha BETWEEN '$fechaIni' AND '$fechaFin') AND RD.fuente = '".$row[0]."' AND RD.productoservicio = '".$row[2]."' AND RD.medio_pago = '".$row[1]."'";
				//echo $sqlrRp."<br>";
				$resRp = mysqli_query($linkbd_V7, $sqlrRp);
				if(mysqli_num_rows($resRp)!=0)
				{
					while($rowRp = mysqli_fetch_row($resRp))
					{
						if( $rowRp[4]=='201')
						{
							$totalRPEnt+=$rowRp[3];
							$arregloRP[]=$rowRp[0];
						}
						else if( substr($rowRp[4],0,1) == '4')
						{
							$totalRPEnt-=$rowRp[3];
						}
					}
				}
	
				//============= AQUI FINALIZA EL PROCESO PARA CALCULAR EL VALOR DE LOS RPS =========//
	
				//============= AQUI INICIA EL PROCESO PARA CALCULAR EL VALOR DE LAS CXP =========//
	
	
				$sqlrCxp = "SELECT T.id_orden,T.fecha,T.tipo_mov,TD.valor,T.estado,TD.tipo_mov FROM tesoordenpago T,tesoordenpago_det TD WHERE T.vigencia=$vigencia  AND T.id_orden=TD.id_orden AND NOT(T.estado='N') AND TD.valor>0 AND TD.cuentap = '".$_POST['codigo'][$xx]."' AND T.tipo_mov=TD.tipo_mov AND T.tipo_mov='201' AND T.fecha BETWEEN '$fechaIni' AND '$fechaFin'  AND TD.productoservicio = '".$row[2]."' AND TD.fuente = '".$row[0]."' AND TD.medio_pago = '".$row[1]."' UNION SELECT T.id_orden,T.fecha,T.tipo_mov,TD.valor,T.estado,TD.tipo_mov FROM tesoordenpago T,tesoordenpago_det TD WHERE T.vigencia=$vigencia  AND T.id_orden=TD.id_orden AND NOT(T.estado='N') AND TD.valor>0 AND TD.cuentap = '".$_POST['codigo'][$xx]."' AND T.tipo_mov=TD.tipo_mov AND (T.tipo_mov='401' OR T.tipo_mov='402') AND EXISTS(SELECT 1 FROM tesoordenpago TAUX WHERE TAUX.id_orden=T.id_orden AND TAUX.tipo_mov='201' AND TAUX.fecha BETWEEN '$fechaIni' AND '$fechaFin')  AND TD.productoservicio = '".$row[2]."' AND TD.fuente = '".$row[0]."' AND TD.medio_pago = '".$row[1]."'";
	
				$resCxp = mysqli_query($linkbd_V7, $sqlrCxp);
				if(mysqli_num_rows($resCxp)!=0)
				{
					while($rowCxp = mysqli_fetch_row($resCxp))
					{
						if($rowCxp[5]=='201')
						{
							$totalCxPEnt+=$rowCxp[3];
						}
						else if($rowCxp[5]=='401')
						{
							$totalCxPEnt-=$rowCxp[3];
						}
					}
				}
	
				for ($xi=0; $xi <sizeof($arregloRP); $xi++) 
				{
					$sqlrCxpNomina = "SELECT HN.id_nom,HN.periodo,HNP.valor,HN.fecha FROM hum_nom_cdp_rp HNR,humnom_presupuestal HNP,humnomina HN WHERE HNR.rp=$arregloRP[$xi] AND HNR.nomina=HNP.id_nom AND HNP.cuenta = '".$_POST['codigo'][$xx]."' AND HNR.vigencia='$vigencia' AND  NOT(HNR.estado='N' OR HNR.estado='R') AND HNP.valor>0 AND HN.id_nom=HNR.nomina AND HN.fecha BETWEEN '$fechaIni' AND '$fechaFin' AND HNP.medio_pago = '".$row[1]."' AND HNP.fuente = '".$row[0]."'";
					
					$resCxpNomina = mysqli_query($linkbd_V7, $sqlrCxpNomina);
	
					if(mysqli_num_rows($resCxpNomina)==0)
					{
					}	
					else
					{
						while($rowCxpNomina = mysqli_fetch_row($resCxpNomina))
						{
							$sqlrEgresoNomina = "SELECT TEND.valordevengado FROM tesoegresosnomina TEN,tesoegresosnomina_det TEND WHERE  TEN.id_orden=".$rowCxpNomina[0]." AND TEN.vigencia='$vigencia' AND NOT(TEN.estado='N' OR TEN.estado='R')  AND TEN.fecha BETWEEN '$fechaIni' AND '$fechaFin' AND TEN.id_egreso=TEND.id_egreso AND NOT(TEND.tipo='SE' OR TEND.tipo='PE' OR TEND.tipo='DS' OR TEND.tipo='RE' OR TEND.tipo='FS') AND TEND.cuentap LIKE '".$_POST['codigo'][$xx]."%' AND TEND.fuente = '".$row[0]."' ";
							
							$resEgresoNomina = mysqli_query($linkbd_V7, $sqlrEgresoNomina);
							if(mysqli_num_rows($resEgresoNomina)!=0)
							{
								while($rowEgresoNomina = mysqli_fetch_row($resEgresoNomina))
								{
									$totalEgresoEnt+=$rowEgresoNomina[0];
								}
							}
							$totalCxPEnt+=$rowCxpNomina[2];
							//$totalEgresoEnt+=$rowEgresoNomina[0];
						}
					}
	
				}
	
				//============= AQUI FINALIZA EL PROCESO PARA CALCULAR EL VALOR DE LAS CXP =========//
	
				//============= AQUI INICIA EL PROCESO PARA CALCULAR EL VALOR DE LOS EGRESOS =========//
	
	
				$sqlrEgreso = "SELECT TE.id_egreso,TE.fecha,TE.tipo_mov,TD.valor FROM tesoegresos TE,tesoordenpago_det TD, tesoordenpago TIA where TIA.vigencia='$vigencia' AND TIA.id_orden=TD.id_orden AND TD.cuentap = '".$_POST['codigo'][$xx]."' AND TE.id_orden=TD.id_orden AND  NOT(TE.estado='N') AND TD.valor >0 AND TE.tipo_mov='201' AND TE.fecha BETWEEN '$fechaIni' AND '$fechaFin' AND TD.productoservicio = '".$row[2]."' AND TD.fuente = '".$row[0]."' AND TD.medio_pago = '".$row[1]."' UNION SELECT TE.id_egreso,TE.fecha,TE.tipo_mov,TD.valor FROM tesoegresos TE,tesoordenpago_det TD,tesoordenpago TIO where TIO.vigencia='$vigencia' AND TIO.id_orden=TD.id_orden AND TD.cuentap = '".$_POST['codigo'][$xx]."' AND TE.id_orden=TD.id_orden AND  NOT(TE.estado='N') AND TD.valor >0 AND TE.tipo_mov='401'  AND EXISTS(SELECT 1 FROM tesoegresos TEAUX WHERE TEAUX.id_egreso=TE.id_egreso AND TEAUX.tipo_mov='201' AND TEAUX.fecha BETWEEN '$fechaIni' AND '$fechaFin') AND TD.productoservicio = '".$row[2]."' AND TD.fuente = '".$row[0]."' AND TD.medio_pago = '".$row[1]."'";
	
				$resEgreso = mysqli_query($linkbd_V7, $sqlrEgreso);
				if(mysqli_num_rows($resEgreso)!=0)
				{
					while($rowEgreso = mysqli_fetch_row($resEgreso))
					{
						if($rowEgreso[2]=='201')
						{
							$totalEgresoEnt+=$rowEgreso[3];
						}
						else if($rowEgreso[2]=='401')
						{
							$totalEgresoEnt-=$rowEgreso[3];
						}
					}
				}
	
				//============= AQUI FINALIZA EL PROCESO PARA CALCULAR EL VALOR DE LOS EGRESOS =========//




				/* $sqlr_cdp = "SELECT sum(D.valor) FROM ccpetcdp AS C, ccpetcdp_detalle AS D WHERE C.consvigencia = D.consvigencia AND C.vigencia = D.vigencia AND D.cuenta='".$_POST['codigo'][$xx]."' AND C.fecha BETWEEN '$fechaIni' AND '$fechaFin' AND D.tipo_mov = '201' AND D.fuente = '".$row[0]."' AND D.productoservicio = ".$row[2]." AND D.medio_pago='".$row[1]."'";

				//$sqlr_cdp = "SELECT sum(valor) FROM ccpetcdp_detalle WHERE cuenta='".$_POST['codigo'][$xx]."' AND productoservicio = ".$row[2]." AND fuente = '".$row[0]."' AND medio_pago='".$row[1]."'";
				$res_cdp = mysqli_query($linkbd, $sqlr_cdp);
				$row_cdp = mysqli_fetch_array($res_cdp);

				$sqlr_rp = "SELECT sum(D.valor) FROM ccpetrp AS C, ccpetrp_detalle AS D WHERE C.consvigencia = D.consvigencia AND C.vigencia = D.vigencia AND D.cuenta='".$_POST['codigo'][$xx]."' AND C.fecha BETWEEN '$fechaIni' AND '$fechaFin' AND D.tipo_mov = '201' AND D.fuente = '".$row[0]."' AND D.productoservicio = '".$row[2]."' AND D.medio_pago='".$row[1]."'";

				//$sqlr_rp = "SELECT sum(valor) FROM ccpetrp_detalle WHERE cuenta='".$_POST['codigo'][$xx]."' AND productoservicio = ".$row[2]." AND fuente = '".$row[0]."' AND medio_pago='".$row[1]."'";
				$res_rp = mysqli_query($linkbd, $sqlr_rp);
				$row_rp = mysqli_fetch_array($res_rp);

				$sqlr_cxp = "SELECT sum(D.valor) FROM tesoordenpago AS C, tesoordenpago_det AS D WHERE C.id_orden =  D.id_orden AND D.cuentap = '".$_POST['codigo'][$xx]."' AND C.fecha BETWEEN '$fechaIni' AND '$fechaFin' AND  C.tipo_mov = '201' AND D.productoservicio = '".$row[2]."' AND  NOT(C.estado='N' OR C.estado='R') AND D.fuente = '".$row[0]."' AND D.medio_pago='".$row[1]."'";

				//$sqlr_cxp = "SELECT sum(valor) FROM tesoordenpago_det WHERE cuentap='".$_POST['codigo'][$xx]."' AND productoservicio = ".$row[2]." AND fuente = '".$row[0]."' AND medio_pago='".$row[1]."'";
				$res_cxp = mysqli_query($linkbd, $sqlr_cxp);
				$row_cxp = mysqli_fetch_array($res_cxp);

				$sqlr_cxp_nom = "SELECT sum(D.valor) FROM humnomina AS C, humnom_presupuestal AS D WHERE C.id_nom = D.id_nom AND C.fecha BETWEEN '$fechaIni' AND '$fechaFin' AND D.cuenta = '".$_POST['codigo'][$xx]."' AND D.fuente = '".$row[0]."' AND C.estado!='N'";//echo $sqlr_cxp_nom."<br>";
				$res_cxp_nom = mysqli_query($linkbd, $sqlr_cxp_nom);
				$row_cxp_nom = mysqli_fetch_row($res_cxp_nom);

				$valorCxp = $row_cxp[0] + $row_cxp_nom[0];


				$sqlr_egreso = "SELECT sum(TD.valor) FROM tesoordenpago T, tesoordenpago_det TD, tesoegresos TE WHERE TE.id_orden=T.id_orden AND T.id_orden=TD.id_orden AND TD.cuentap = '".$_POST['codigo'][$xx]."' AND T.fecha BETWEEN '$fechaIni' AND '$fechaFin' AND  NOT(TE.estado='N' OR TE.estado='R') AND TD.valor >0 AND TE.tipo_mov='201' AND TD.productoservicio = '".$row[2]."' AND TD.fuente = '".$row[0]."' AND TD.medio_pago='".$row[1]."'";

				//$sqlr_egreso = "SELECT sum(valor) FROM tesoordenpago T, tesoordenpago_det TD, tesoegresos TE WHERE TE.id_orden=T.id_orden AND T.id_orden=TD.id_orden AND TD.cuentap = '".$_POST['codigo'][$xx]."' AND TE.vigencia='$_POST[vigencia]' AND TD.vigencia='$_POST[vigencia]' AND  NOT(TE.estado='N') AND TD.valor >0 AND TE.tipo_mov='201' AND TD.productoservicio = ".$row[2]." AND TD.fuente = '".$row[0]."' AND TD.medio_pago='".$row[1]."'";
				$res_egreso = mysqli_query($linkbd, $sqlr_egreso);
				$row_egreso = mysqli_fetch_row($res_egreso);

				$sqlr_egreso_nom = "SELECT sum(D.valordevengado) FROM tesoegresosnomina AS C, tesoegresosnomina_det AS D WHERE C.id_egreso = D.id_egreso AND C.fecha BETWEEN '$fechaIni' AND '$fechaFin' AND NOT(D.tipo='SE' OR D.tipo='PE' OR D.tipo='DS' OR D.tipo='RE') AND D.cuentap = '".$_POST['codigo'][$xx]."'";
				$res_egreso_nom = mysqli_query($linkbd, $sqlr_egreso_nom);
				$row_egreso_nom = mysqli_fetch_row($res_egreso_nom);
	
				$valorEgreso = $row_egreso[0] + $row_egreso_nom[0];
 */

				if($row[3] == 'I' || $row[3] == 'G' || $row[3] == 'C' || $row[3] == 'R')
				{
					$row[4] = 0;
				}

				$compromisoEnEjecucion = $totalRPEnt - $totalCxPEnt;

				$cuentasPorPagar = $totalCxPEnt - $totalEgresoEnt;

				$presupuestoDefinitivo = $row[4] + $adicion - $reduccion + $credito - $contracredito;

				$saldo = $presupuestoDefinitivo - $totalCDPEnt;

				$i++;
				$objPHPExcel->setActiveSheetIndex(0)
				->setCellValueExplicit ("A$i", $row[1], PHPExcel_Cell_DataType :: TYPE_STRING)
				->setCellValueExplicit ("B$i", $row[3], PHPExcel_Cell_DataType :: TYPE_STRING)
				->setCellValueExplicit ("C$i", '', PHPExcel_Cell_DataType :: TYPE_STRING)
				->setCellValueExplicit ("D$i", $_POST['codigo'][$xx], PHPExcel_Cell_DataType :: TYPE_STRING)
				->setCellValueExplicit ("E$i", $row[0], PHPExcel_Cell_DataType :: TYPE_STRING)
				->setCellValueExplicit ("F$i", '', PHPExcel_Cell_DataType :: TYPE_STRING)
				->setCellValueExplicit ("G$i", $row[2], PHPExcel_Cell_DataType :: TYPE_STRING)
				->setCellValueExplicit ("H$i", $row_bienes[0], PHPExcel_Cell_DataType :: TYPE_STRING)
				->setCellValueExplicit ("I$i", '', PHPExcel_Cell_DataType :: TYPE_STRING)
				->setCellValueExplicit ("J$i", $row[4], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
				->setCellValueExplicit ("K$i", $adicion, PHPExcel_Cell_DataType :: TYPE_NUMERIC)
				->setCellValueExplicit ("L$i", $reduccion, PHPExcel_Cell_DataType :: TYPE_NUMERIC)
				->setCellValueExplicit ("M$i", $credito, PHPExcel_Cell_DataType :: TYPE_NUMERIC)
				->setCellValueExplicit ("N$i", $contracredito, PHPExcel_Cell_DataType :: TYPE_NUMERIC)
				->setCellValueExplicit ("O$i", $presupuestoDefinitivo, PHPExcel_Cell_DataType :: TYPE_NUMERIC)
				->setCellValueExplicit ("P$i", $totalCDPEnt, PHPExcel_Cell_DataType :: TYPE_NUMERIC)
				->setCellValueExplicit ("Q$i", $totalRPEnt, PHPExcel_Cell_DataType :: TYPE_NUMERIC)
				->setCellValueExplicit ("R$i", $totalCxPEnt, PHPExcel_Cell_DataType :: TYPE_NUMERIC)
				->setCellValueExplicit ("S$i", round($compromisoEnEjecucion,2), PHPExcel_Cell_DataType :: TYPE_NUMERIC)
				->setCellValueExplicit ("T$i", $totalEgresoEnt, PHPExcel_Cell_DataType :: TYPE_NUMERIC)
				->setCellValueExplicit ("U$i", $cuentasPorPagar, PHPExcel_Cell_DataType :: TYPE_NUMERIC)
				->setCellValueExplicit ("V$i", $saldo, PHPExcel_Cell_DataType :: TYPE_NUMERIC);
				$objPHPExcel->getActiveSheet()->getStyle("A$i:V$i")->applyFromArray($styleArray);

				//$objPHPExcel-> getActiveSheet()->getStyle ("A$i:H$i")-> getStartColor ()-> setRGB ('A6E5F3');
			}

		}

		//$sqlr = "SELECT fuente, medioPago, subclase, unidad, valor FROM ccpetinicialservicios WHERE cuenta = '".$_POST['codigo'][$xx]."' AND vigencia='$vigencia' $crit $crit1";

		$sqlr = "SELECT CB.fuente, CB.medioPago, CB.subclase, CB.unidad, CB.valor FROM ccpetinicialservicios AS CB WHERE CB.cuenta = '".$_POST['codigo'][$xx]."' AND CB.vigencia='$vigencia' $critBienesTransportables $crit1BienesTransportables UNION SELECT CA.fuente, CA.mediopago, CA.cuenta_clasificadora, CA.tipo, CA.valor FROM ccpetadiciones AS CA WHERE CA.cuenta = '".$_POST['codigo'][$xx]."' AND CA.vigencia='$vigencia' AND LEFT(CA.cuenta, 3)!='2.3' AND CA.clasificador='3' AND NOT EXISTS(SELECT 1 FROM ccpetinicialservicios AS CBT WHERE CBT.cuenta = '".$_POST['codigo'][$xx]."' AND CBT.vigencia='$vigencia' AND CA.cuenta_clasificadora = CBT.subclase AND CA.fuente = CBT.fuente) UNION SELECT CT.fuente, CT.mediopago, CT.productoservicio, CT.tipo, CT.valor FROM ccpettraslados AS CT WHERE CT.cuenta = '".$_POST['codigo'][$xx]."' AND CT.vigencia='$vigencia' AND CT.productoservicio!='' AND LENGTH(CT.productoservicio)=5 AND LEFT(CT.cuenta, 3)!='2.3' AND NOT EXISTS(SELECT 1 FROM ccpetinicialservicios AS CTT WHERE CTT.cuenta = '".$_POST['codigo'][$xx]."' AND CT.productoservicio = CTT.subclase AND CTT.vigencia='$vigencia' AND CT.fuente = CTT.fuente) AND NOT EXISTS(SELECT 1 FROM ccpetadiciones AS CAT WHERE CAT.cuenta = '".$_POST['codigo'][$xx]."' AND CT.productoservicio = CAT.cuenta_clasificadora AND CAT.vigencia='$vigencia' AND CT.fuente = CAT.fuente) GROUP BY CT.fuente, CT.mediopago, CT.productoservicio";
		

		$result = mysqli_query($linkbd, $sqlr);
		if(mysqli_num_rows($result) > 0){
			while($row = mysqli_fetch_array($result)){

				$definitivo = 0;
				$compromisoEnEjecucion = 0;
				$saldo = 0;
				$cuentasPorPagar = 0;

				$inicial = 0;
				$adicion = 0;
				$reduccion = 0;
				$credito = 0;
				$contracredito = 0;
				$presupuestoDefinitivo = 0;
				$totalCDPEnt = 0;
				$totalRPEnt = 0;
				$totalCxPEnt = 0;
				$totalEgresoEnt = 0;
				$arregloRP=[];

				$sqlr_bienes = "SELECT titulo FROM ccpetservicios WHERE grupo = '$row[2]'";
				$result_bienes = mysqli_query($linkbd, $sqlr_bienes);
				$row_bienes = mysqli_fetch_array($result_bienes);

				//============= AQUI INICIA EL PROCESO PARA CALCULAR LA ADICION =========//


				$sqlrAdicion = "SELECT SUM(valor) FROM ccpetadiciones WHERE cuenta = '".$_POST['codigo'][$xx]."' AND fecha BETWEEN '$fechaIni' AND '$fechaFin' AND vigencia='$vigencia' AND estado!='N' AND mediopago = '".$row[1]."' AND fuente = '".$row[0]."' AND cuenta_clasificadora = '".$row[2]."'";
				$resAdicion = mysqli_query($linkbd_V7, $sqlrAdicion);
				$rowAdicion = mysqli_fetch_row($resAdicion);
	
				$adicion = $rowAdicion[0];
	
				//============= AQUI FINALIZA EL PROCESO PARA CALCULAR LA ADICION =========//
	
				//============= AQUI INICIA EL PROCESO PARA CALCULAR LA REDUCCION =========//
				
				$sqlrReduccion = "SELECT SUM(valor) FROM ccpetreducciones WHERE cuenta = '".$_POST['codigo'][$xx]."' AND fecha BETWEEN '$fechaIni' AND '$fechaFin' AND vigencia='$vigencia' AND estado!='N' AND mediopago = '".$row[1]."' AND fuente = '".$row[0]."' AND cuenta_clasificadora = '".$row[2]."'";
				$resReduccion = mysqli_query($linkbd_V7, $sqlrReduccion);
				$rowReduccion = mysqli_fetch_row($resReduccion);
	
				$reduccion = $rowReduccion[0];

				$sqlrReduccion_n = "SELECT SUM(CR.valor) FROM ccpetreduccion AS CR, ccpetacuerdos AS CAC WHERE CR.rubro = '".$_POST['codigo'][$xx]."' AND CR.acuerdo = CAC.id_acuerdo AND CAC.fecha BETWEEN '$fechaIni' AND '$fechaFin' AND CR.vigencia='$vigencia' AND CR.medio_pago = '".$row[1]."' AND CR.fuente = '".$row[0]."' AND CR.clasificador = '".$row[2]."'";
				$resReduccion_n = mysqli_query($linkbd_V7, $sqlrReduccion_n);
				$rowReduccion_n = mysqli_fetch_row($resReduccion_n);

				$reduccion += $rowReduccion_n[0];
	
				//============= AQUI FINALIZA EL PROCESO PARA CALCULAR LA REDUCCION =========//
	
	
				//============= AQUI INICIA EL PROCESO PARA CALCULAR EL CREDITO =========//
				
				$sqlrCredito = "SELECT SUM(valor) FROM ccpettraslados WHERE cuenta = '".$_POST['codigo'][$xx]."' AND fecha BETWEEN '$fechaIni' AND '$fechaFin' AND vigencia='$vigencia' AND estado!='N' AND mediopago = '".$row[1]."' AND tipo='C' AND productoservicio = '".$row[2]."' AND fuente='".$row[0]."'";
				$resCredito = mysqli_query($linkbd_V7, $sqlrCredito);
				$rowCredito = mysqli_fetch_row($resCredito);
	
				$credito = $rowCredito[0];
				
				
	
				//============= AQUI FINALIZA EL PROCESO PARA CALCULAR EL CREDITO =========//
	
				//============= AQUI INICIA EL PROCESO PARA CALCULAR EL CONTRACREDITO =========//
	
				
				$sqlrContracreditoCredito = "SELECT SUM(valor) FROM ccpettraslados WHERE cuenta = '".$_POST['codigo'][$xx]."' AND fecha BETWEEN '$fechaIni' AND '$fechaFin' AND vigencia='$vigencia' AND estado!='N' AND mediopago = '".$row[1]."' AND tipo='R' AND productoservicio = '".$row[2]."' AND fuente='".$row[0]."'";
				$resContracreditoCredito = mysqli_query($linkbd_V7, $sqlrContracreditoCredito);
				$rowContracreditoCredito = mysqli_fetch_row($resContracreditoCredito);
	
				$contracredito = $rowContracreditoCredito[0];
				
				//============= AQUI FINALIZA EL PROCESO PARA CALCULAR EL CONTRACREDITO =========//
	
				//============= AQUI INICIA EL PROCESO PARA CALCULAR EL VALOR DE LOS CDPS =========//
	
				$sqlrCdp = "SELECT C.consvigencia, C.tipo_mov,D.valor,D.tipo_mov FROM ccpetcdp C, ccpetcdp_detalle D WHERE D.cuenta = '".$_POST['codigo'][$xx]."' AND D.vigencia='$vigencia' AND D.consvigencia=C.consvigencia AND C.vigencia='$vigencia' AND NOT(D.estado='N') AND D.valor>0 AND D.tipo_mov=C.tipo_mov AND D.tipo_mov='201' AND C.fecha BETWEEN '$fechaIni' AND '$fechaFin' AND D.fuente = '".$row[0]."' AND D.productoservicio = '".$row[2]."' AND D.medio_pago = '".$row[1]."' UNION SELECT C.consvigencia, C.tipo_mov,SUM(D.valor),D.tipo_mov FROM ccpetcdp C, ccpetcdp_detalle D WHERE D.cuenta = '".$_POST['codigo'][$xx]."' AND D.vigencia='$vigencia' AND D.consvigencia=C.consvigencia AND C.vigencia='$vigencia' AND NOT(D.estado='N') AND D.valor>0 AND D.tipo_mov=C.tipo_mov AND (D.tipo_mov='401' OR D.tipo_mov='402') AND D.fuente = '".$row[0]."' AND D.productoservicio = '".$row[2]."' AND D.medio_pago = '".$row[1]."' AND EXISTS(SELECT 1 FROM ccpetcdp DAUX WHERE DAUX.consvigencia=D.consvigencia AND DAUX.tipo_mov='201' AND DAUX.fecha BETWEEN '$fechaIni' AND '$fechaFin')";
				
				$resCdp = mysqli_query($linkbd_V7, $sqlrCdp);
				if(mysqli_num_rows($resCdp)!=0)
				{
					while($rowCdp = mysqli_fetch_row($resCdp))
					{
						if($rowCdp[3]=='201')
						{
							$totalCDPEnt+=round($rowCdp[2],2);
						}
						else if( substr($rowCdp[3],0,1) == '4' )
						{
							$totalCDPEnt-=round($rowCdp[2],2);
						}
					}
				}
	
				//============= AQUI FINALIZA EL PROCESO PARA CALCULAR EL VALOR DE LOS CDPS =========//
	
				//============= AQUI INICIA EL PROCESO PARA CALCULAR EL VALOR DE LOS RPS =========//
	
				
				$sqlrRp = "SELECT R.consvigencia,R.fecha,R.tipo_mov,RD.valor,RD.tipo_mov FROM ccpetrp R,ccpetrp_detalle RD where  R.vigencia='$vigencia' AND RD.cuenta = '".$_POST['codigo'][$xx]."' AND RD.consvigencia=R.consvigencia AND RD.vigencia='$vigencia'  AND NOT(R.estado='N') AND R.tipo_mov=RD.tipo_mov AND R.tipo_mov='201' AND RD.valor>0 AND R.fecha BETWEEN '$fechaIni' AND '$fechaFin' AND RD.fuente = '".$row[0]."' AND RD.productoservicio = '".$row[2]."' AND RD.medio_pago = '".$row[1]."' UNION SELECT R.consvigencia,R.fecha,R.tipo_mov,SUM(RD.valor),RD.tipo_mov FROM ccpetrp R,ccpetrp_detalle RD where  R.vigencia='$vigencia' AND RD.cuenta = '".$_POST['codigo'][$xx]."' AND RD.consvigencia=R.consvigencia AND RD.vigencia='$vigencia'  AND NOT(R.estado='N') AND R.tipo_mov=RD.tipo_mov AND (R.tipo_mov='401' OR R.tipo_mov='402') AND RD.valor>0 AND EXISTS(SELECT 1 FROM ccpetrp RAUX WHERE RAUX.consvigencia=R.consvigencia AND RAUX.tipo_mov='201' AND RAUX.fecha BETWEEN '$fechaIni' AND '$fechaFin') AND RD.fuente = '".$row[0]."' AND RD.productoservicio = '".$row[2]."' AND RD.medio_pago = '".$row[1]."'";
				//echo $sqlrRp."<br>";
				$resRp = mysqli_query($linkbd_V7, $sqlrRp);
				if(mysqli_num_rows($resRp)!=0)
				{
					while($rowRp = mysqli_fetch_row($resRp))
					{
						if( $rowRp[4]=='201')
						{
							$totalRPEnt+=$rowRp[3];
							$arregloRP[]=$rowRp[0];
						}
						else if( substr($rowRp[4],0,1) == '4')
						{
							$totalRPEnt-=$rowRp[3];
						}
					}
				}
	
				//============= AQUI FINALIZA EL PROCESO PARA CALCULAR EL VALOR DE LOS RPS =========//
	
				//============= AQUI INICIA EL PROCESO PARA CALCULAR EL VALOR DE LAS CXP =========//
	
	
				$sqlrCxp = "SELECT T.id_orden,T.fecha,T.tipo_mov,TD.valor,T.estado,TD.tipo_mov FROM tesoordenpago T,tesoordenpago_det TD WHERE T.vigencia=$vigencia  AND T.id_orden=TD.id_orden AND NOT(T.estado='N') AND TD.valor>0 AND TD.cuentap = '".$_POST['codigo'][$xx]."' AND T.tipo_mov=TD.tipo_mov AND T.tipo_mov='201' AND T.fecha BETWEEN '$fechaIni' AND '$fechaFin'  AND TD.productoservicio = '".$row[2]."' AND TD.fuente = '".$row[0]."' AND TD.medio_pago = '".$row[1]."' UNION SELECT T.id_orden,T.fecha,T.tipo_mov,TD.valor,T.estado,TD.tipo_mov FROM tesoordenpago T,tesoordenpago_det TD WHERE T.vigencia=$vigencia  AND T.id_orden=TD.id_orden AND NOT(T.estado='N') AND TD.valor>0 AND TD.cuentap = '".$_POST['codigo'][$xx]."' AND T.tipo_mov=TD.tipo_mov AND (T.tipo_mov='401' OR T.tipo_mov='402') AND EXISTS(SELECT 1 FROM tesoordenpago TAUX WHERE TAUX.id_orden=T.id_orden AND TAUX.tipo_mov='201' AND TAUX.fecha BETWEEN '$fechaIni' AND '$fechaFin')  AND TD.productoservicio = '".$row[2]."' AND TD.fuente = '".$row[0]."' AND TD.medio_pago = '".$row[1]."'";
	
				$resCxp = mysqli_query($linkbd_V7, $sqlrCxp);
				if(mysqli_num_rows($resCxp)!=0)
				{
					while($rowCxp = mysqli_fetch_row($resCxp))
					{
						if($rowCxp[5]=='201')
						{
							$totalCxPEnt+=$rowCxp[3];
						}
						else if($rowCxp[5]=='401')
						{
							$totalCxPEnt-=$rowCxp[3];
						}
					}
				}
	
				for ($xi=0; $xi <sizeof($arregloRP); $xi++) 
				{
					$sqlrCxpNomina = "SELECT HN.id_nom,HN.periodo,HNP.valor,HN.fecha FROM hum_nom_cdp_rp HNR,humnom_presupuestal HNP,humnomina HN WHERE HNR.rp=$arregloRP[$xi] AND HNR.nomina=HNP.id_nom AND HNP.cuenta = '".$_POST['codigo'][$xx]."' AND HNR.vigencia='$vigencia' AND  NOT(HNR.estado='N' OR HNR.estado='R') AND HNP.valor>0 AND HN.id_nom=HNR.nomina AND HN.fecha BETWEEN '$fechaIni' AND '$fechaFin' AND HNP.medio_pago = '".$row[1]."' AND HNP.fuente = '".$row[0]."'";
					
					$resCxpNomina = mysqli_query($linkbd_V7, $sqlrCxpNomina);
	
					if(mysqli_num_rows($resCxpNomina)==0)
						echo "";
					else
					{
						while($rowCxpNomina = mysqli_fetch_row($resCxpNomina))
						{
							$sqlrEgresoNomina = "SELECT TEND.valordevengado FROM tesoegresosnomina TEN,tesoegresosnomina_det TEND WHERE  TEN.id_orden=".$rowCxpNomina[0]." AND TEN.vigencia='$vigencia' AND NOT(TEN.estado='N' OR TEN.estado='R')  AND TEN.fecha BETWEEN '$fechaIni' AND '$fechaFin' AND TEN.id_egreso=TEND.id_egreso AND NOT(TEND.tipo='SE' OR TEND.tipo='PE' OR TEND.tipo='DS' OR TEND.tipo='RE' OR TEND.tipo='FS') AND TEND.cuentap LIKE '".$_POST['codigo'][$xx]."%' AND TEND.fuente = '".$row[0]."' ";
							
							$resEgresoNomina = mysqli_query($linkbd_V7, $sqlrEgresoNomina);
							if(mysqli_num_rows($resEgresoNomina)!=0)
							{
								while($rowEgresoNomina = mysqli_fetch_row($resEgresoNomina))
								{
									$totalEgresoEnt+=$rowEgresoNomina[0];
								}
							}
							$totalCxPEnt+=$rowCxpNomina[2];
							//$totalEgresoEnt+=$rowEgresoNomina[0];
						}
					}
	
				}
	
				//============= AQUI FINALIZA EL PROCESO PARA CALCULAR EL VALOR DE LAS CXP =========//
	
				//============= AQUI INICIA EL PROCESO PARA CALCULAR EL VALOR DE LOS EGRESOS =========//
	
	
				$sqlrEgreso = "SELECT TE.id_egreso,TE.fecha,TE.tipo_mov,TD.valor FROM tesoegresos TE,tesoordenpago_det TD, tesoordenpago TIA where TIA.vigencia='$vigencia' AND TIA.id_orden=TD.id_orden AND TD.cuentap = '".$_POST['codigo'][$xx]."' AND TE.id_orden=TD.id_orden AND  NOT(TE.estado='N') AND TD.valor >0 AND TE.tipo_mov='201' AND TE.fecha BETWEEN '$fechaIni' AND '$fechaFin' AND TD.productoservicio = '".$row[2]."' AND TD.fuente = '".$row[0]."' AND TD.medio_pago = '".$row[1]."' UNION SELECT TE.id_egreso,TE.fecha,TE.tipo_mov,TD.valor FROM tesoegresos TE,tesoordenpago_det TD,tesoordenpago TIO where TIO.vigencia='$vigencia' AND TIO.id_orden=TD.id_orden AND TD.cuentap = '".$_POST['codigo'][$xx]."' AND TE.id_orden=TD.id_orden AND  NOT(TE.estado='N') AND TD.valor >0 AND TE.tipo_mov='401'  AND EXISTS(SELECT 1 FROM tesoegresos TEAUX WHERE TEAUX.id_egreso=TE.id_egreso AND TEAUX.tipo_mov='201' AND TEAUX.fecha BETWEEN '$fechaIni' AND '$fechaFin') AND TD.productoservicio = '".$row[2]."' AND TD.fuente = '".$row[0]."' AND TD.medio_pago = '".$row[1]."'";
	
				$resEgreso = mysqli_query($linkbd_V7, $sqlrEgreso);
				if(mysqli_num_rows($resEgreso)!=0)
				{
					while($rowEgreso = mysqli_fetch_row($resEgreso))
					{
						if($rowEgreso[2]=='201')
						{
							$totalEgresoEnt+=$rowEgreso[3];
						}
						else if($rowEgreso[2]=='401')
						{
							$totalEgresoEnt-=$rowEgreso[3];
						}
					}
				}
	
				//============= AQUI FINALIZA EL PROCESO PARA CALCULAR EL VALOR DE LOS EGRESOS =========//



				/* $sqlr_cdp = "SELECT sum(D.valor) FROM ccpetcdp AS C, ccpetcdp_detalle AS D WHERE C.consvigencia = D.consvigencia AND C.vigencia = D.vigencia AND D.cuenta='".$_POST['codigo'][$xx]."' AND C.fecha BETWEEN '$fechaIni' AND '$fechaFin' AND D.tipo_mov = '201' AND D.fuente = '".$row[0]."' AND D.productoservicio = ".$row[2]." AND D.medio_pago='".$row[1]."'";

				//$sqlr_cdp = "SELECT sum(valor) FROM ccpetcdp_detalle WHERE cuenta='".$_POST['codigo'][$xx]."' AND productoservicio = ".$row[2]." AND fuente = '".$row[0]."' AND medio_pago='".$row[1]."'";
				$res_cdp = mysqli_query($linkbd, $sqlr_cdp);
				$row_cdp = mysqli_fetch_array($res_cdp);

				$sqlr_rp = "SELECT sum(D.valor) FROM ccpetrp AS C, ccpetrp_detalle AS D WHERE C.consvigencia = D.consvigencia AND C.vigencia = D.vigencia AND D.cuenta='".$_POST['codigo'][$xx]."' AND C.fecha BETWEEN '$fechaIni' AND '$fechaFin' AND D.tipo_mov = '201' AND D.fuente = '".$row[0]."' AND D.productoservicio = ".$row[2]." AND D.medio_pago='".$row[1]."'";

				//$sqlr_rp = "SELECT sum(valor) FROM ccpetrp_detalle WHERE cuenta='".$_POST['codigo'][$xx]."' AND productoservicio = ".$row[2]." AND fuente = '".$row[0]."' AND medio_pago='".$row[1]."'";
				$res_rp = mysqli_query($linkbd, $sqlr_rp);
				$row_rp = mysqli_fetch_array($res_rp);

				$sqlr_cxp = "SELECT sum(D.valor) FROM tesoordenpago AS C, tesoordenpago_det AS D WHERE C.id_orden =  D.id_orden AND D.cuentap = '".$_POST['codigo'][$xx]."' AND C.fecha BETWEEN '$fechaIni' AND '$fechaFin' AND  NOT(C.estado='N' OR C.estado='R') AND  C.tipo_mov = '201' AND D.productoservicio = ".$row[2]." AND D.fuente = '".$row[0]."' AND D.medio_pago='".$row[1]."'";

				//$sqlr_cxp = "SELECT sum(valor) FROM tesoordenpago_det WHERE cuentap='".$_POST['codigo'][$xx]."' AND productoservicio = ".$row[2]." AND fuente = '".$row[0]."' AND medio_pago='".$row[1]."'";

				$res_cxp = mysqli_query($linkbd, $sqlr_cxp);
				$row_cxp = mysqli_fetch_array($res_cxp);

				$sqlr_cxp_nom = "SELECT sum(D.valor) FROM humnomina AS C, humnom_presupuestal AS D WHERE C.id_nom = D.id_nom AND C.fecha BETWEEN '$fechaIni' AND '$fechaFin' AND D.cuenta = '".$_POST['codigo'][$xx]."' AND D.fuente = '".$row[0]."' AND C.estado!='N'";//echo $sqlr_cxp_nom."<br>";
				$res_cxp_nom = mysqli_query($linkbd, $sqlr_cxp_nom);
				$row_cxp_nom = mysqli_fetch_row($res_cxp_nom);

				$valorCxp = $row_cxp[0] + $row_cxp_nom[0];

				$sqlr_egreso = "SELECT sum(TD.valor) FROM tesoordenpago T, tesoordenpago_det TD, tesoegresos TE WHERE TE.id_orden=T.id_orden AND T.id_orden=TD.id_orden AND TD.cuentap = '".$_POST['codigo'][$xx]."' AND T.fecha BETWEEN '$fechaIni' AND '$fechaFin' AND  NOT(TE.estado='N' OR TE.estado='R') AND TD.valor >0 AND TE.tipo_mov='201' AND TD.productoservicio = ".$row[2]." AND TD.fuente = '".$row[0]."' AND TD.medio_pago='".$row[1]."'";

				//$sqlr_egreso = "SELECT sum(valor) FROM tesoordenpago T, tesoordenpago_det TD, tesoegresos TE WHERE TE.id_orden=T.id_orden AND T.id_orden=TD.id_orden AND TD.cuentap = '".$_POST['codigo'][$xx]."' AND TE.vigencia='$_POST[vigencia]' AND TD.vigencia='$_POST[vigencia]' AND  NOT(TE.estado='N') AND TD.valor >0 AND TE.tipo_mov='201' AND TD.productoservicio = ".$row[2]." AND TD.fuente = '".$row[0]."' AND TD.medio_pago='".$row[1]."'";
				$res_egreso = mysqli_query($linkbd, $sqlr_egreso);
				$row_egreso = mysqli_fetch_row($res_egreso);

				$sqlr_egreso_nom = "SELECT sum(D.valordevengado) FROM tesoegresosnomina AS C, tesoegresosnomina_det AS D WHERE C.id_egreso = D.id_egreso AND C.fecha BETWEEN '$fechaIni' AND '$fechaFin' AND NOT(D.tipo='SE' OR D.tipo='PE' OR D.tipo='DS' OR D.tipo='RE') AND D.cuentap = '$cuenta'";
				$res_egreso_nom = mysqli_query($linkbd, $sqlr_egreso_nom);
				$row_egreso_nom = mysqli_fetch_row($res_egreso_nom);

				$valorEgreso = $row_egreso[0] + $row_egreso_nom[0]; */

				if($row[3] == 'I' || $row[3] == 'G' || $row[3] == 'C' || $row[3] == 'R')
				{
					$row[4] = 0;
				}


				$compromisoEnEjecucion = $totalRPEnt - $totalCxPEnt;

				$cuentasPorPagar = $totalCxPEnt - $totalEgresoEnt;

				$presupuestoDefinitivo = $row[4] + $adicion - $reduccion + $credito - $contracredito;

				$saldo = $presupuestoDefinitivo - $totalCDPEnt;

				$i++;
				$objPHPExcel->setActiveSheetIndex(0)
				->setCellValueExplicit ("A$i", $row[1], PHPExcel_Cell_DataType :: TYPE_STRING)
				->setCellValueExplicit ("B$i", $row[3], PHPExcel_Cell_DataType :: TYPE_STRING)
				->setCellValueExplicit ("C$i", '', PHPExcel_Cell_DataType :: TYPE_STRING)
				->setCellValueExplicit ("D$i", $_POST['codigo'][$xx], PHPExcel_Cell_DataType :: TYPE_STRING)
				->setCellValueExplicit ("E$i", $row[0], PHPExcel_Cell_DataType :: TYPE_STRING)
				->setCellValueExplicit ("F$i", '', PHPExcel_Cell_DataType :: TYPE_STRING)
				->setCellValueExplicit ("G$i", $row[2], PHPExcel_Cell_DataType :: TYPE_STRING)
				->setCellValueExplicit ("H$i", $row_bienes[0], PHPExcel_Cell_DataType :: TYPE_STRING)
				->setCellValueExplicit ("I$i", '', PHPExcel_Cell_DataType :: TYPE_STRING)
				->setCellValueExplicit ("J$i", $row[4], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
				->setCellValueExplicit ("K$i", $adicion, PHPExcel_Cell_DataType :: TYPE_NUMERIC)
				->setCellValueExplicit ("L$i", $reduccion, PHPExcel_Cell_DataType :: TYPE_NUMERIC)
				->setCellValueExplicit ("M$i", $credito, PHPExcel_Cell_DataType :: TYPE_NUMERIC)
				->setCellValueExplicit ("N$i", $contracredito, PHPExcel_Cell_DataType :: TYPE_NUMERIC)
				->setCellValueExplicit ("O$i", $presupuestoDefinitivo, PHPExcel_Cell_DataType :: TYPE_NUMERIC)
				->setCellValueExplicit ("P$i", $totalCDPEnt, PHPExcel_Cell_DataType :: TYPE_NUMERIC)
				->setCellValueExplicit ("Q$i", $totalRPEnt, PHPExcel_Cell_DataType :: TYPE_NUMERIC)
				->setCellValueExplicit ("R$i", $totalCxPEnt, PHPExcel_Cell_DataType :: TYPE_NUMERIC)
				->setCellValueExplicit ("S$i", round($compromisoEnEjecucion,2), PHPExcel_Cell_DataType :: TYPE_NUMERIC)
				->setCellValueExplicit ("T$i", $totalEgresoEnt, PHPExcel_Cell_DataType :: TYPE_NUMERIC)
				->setCellValueExplicit ("U$i", $cuentasPorPagar, PHPExcel_Cell_DataType :: TYPE_NUMERIC)
				->setCellValueExplicit ("V$i", $saldo, PHPExcel_Cell_DataType :: TYPE_NUMERIC);
				$objPHPExcel->getActiveSheet()->getStyle("A$i:V$i")->applyFromArray($styleArray);

				//$objPHPExcel-> getActiveSheet()->getStyle ("A$i:H$i")-> getStartColor ()-> setRGB ('A6E5F3');
			}
		}

		//$sqlr = "SELECT fuente, medioPago, unidad, valor FROM ccpetinicialvalorgastos WHERE cuenta = '".$_POST['codigo'][$xx]."' AND vigencia='$vigencia' $crit $crit1";

		$sqlr = "SELECT CB.fuente, CB.medioPago, CB.unidad, CB.valor FROM ccpetinicialvalorgastos AS CB WHERE CB.cuenta = '".$_POST['codigo'][$xx]."' AND CB.vigencia='$vigencia' $critBienesTransportables $crit1BienesTransportables UNION SELECT CA.fuente, CA.mediopago, CA.tipo, CA.valor FROM ccpetadiciones AS CA WHERE CA.cuenta = '".$_POST['codigo'][$xx]."' AND CA.vigencia='$vigencia' AND LEFT(CA.cuenta, 3)!='2.3' AND CA.cuenta_clasificadora='' AND LEFT(CA.cuenta, 3)!='2.3' AND NOT EXISTS(SELECT 1 FROM ccpetinicialvalorgastos AS CBT WHERE CBT.cuenta = '".$_POST['codigo'][$xx]."' AND CBT.vigencia='$vigencia' AND CA.fuente = CBT.fuente) UNION SELECT CT.fuente, CT.mediopago, CT.tipo, CT.valor FROM ccpettraslados AS CT WHERE CT.cuenta = '".$_POST['codigo'][$xx]."' AND CT.productoservicio = '' AND CT.vigencia='$vigencia' AND LEFT(CT.cuenta, 3)!='2.3' AND NOT EXISTS(SELECT 1 FROM ccpetinicialvalorgastos AS CTT WHERE CTT.cuenta = '".$_POST['codigo'][$xx]."' AND CTT.vigencia='$vigencia' AND CT.fuente = CTT.fuente) AND NOT EXISTS(SELECT 1 FROM ccpetadiciones AS CAT WHERE CAT.cuenta = '".$_POST['codigo'][$xx]."' AND CAT.vigencia='$vigencia' AND CT.fuente = CAT.fuente)";
		
		$result = mysqli_query($linkbd, $sqlr);
		if(mysqli_num_rows($result) > 0){
			while($row = mysqli_fetch_array($result)){

				$definitivo = 0;
				$compromisoEnEjecucion = 0;
				$saldo = 0;
				$cuentasPorPagar = 0;

				$inicial = 0;
				$adicion = 0;
				$reduccion = 0;
				$credito = 0;
				$contracredito = 0;
				$presupuestoDefinitivo = 0;
				$totalCDPEnt = 0;
				$totalRPEnt = 0;
				$totalCxPEnt = 0;
				$totalEgresoEnt = 0;
				$arregloRP=[];

				//============= AQUI INICIA EL PROCESO PARA CALCULAR LA ADICION =========//


				$sqlrAdicion = "SELECT SUM(valor) FROM ccpetadiciones WHERE cuenta = '".$_POST['codigo'][$xx]."' AND fecha BETWEEN '$fechaIni' AND '$fechaFin' AND vigencia='$vigencia' AND estado!='N' AND mediopago = '".$row[1]."' AND fuente = '".$row[0]."' AND cuenta_clasificadora = ''";
				$resAdicion = mysqli_query($linkbd_V7, $sqlrAdicion);
				$rowAdicion = mysqli_fetch_row($resAdicion);
	
				$adicion = $rowAdicion[0];
	
				//============= AQUI FINALIZA EL PROCESO PARA CALCULAR LA ADICION =========//
	
				//============= AQUI INICIA EL PROCESO PARA CALCULAR LA REDUCCION =========//
				
				$sqlrReduccion = "SELECT SUM(valor) FROM ccpetreducciones WHERE cuenta = '".$_POST['codigo'][$xx]."' AND fecha BETWEEN '$fechaIni' AND '$fechaFin' AND vigencia='$vigencia' AND estado!='N' AND mediopago = '".$row[1]."' AND fuente = '".$row[0]."' AND cuenta_clasificadora = ''";
				$resReduccion = mysqli_query($linkbd_V7, $sqlrReduccion);
				$rowReduccion = mysqli_fetch_row($resReduccion);
	
				$reduccion = $rowReduccion[0];

				$sqlrReduccion_n = "SELECT SUM(CR.valor) FROM ccpetreduccion AS CR, ccpetacuerdos AS CAC WHERE CR.rubro = '".$_POST['codigo'][$xx]."' AND CR.acuerdo = CAC.id_acuerdo AND CAC.fecha BETWEEN '$fechaIni' AND '$fechaFin' AND CR.vigencia='$vigencia' AND CR.medio_pago = '".$row[1]."' AND CR.fuente = '".$row[0]."' ";
				$resReduccion_n = mysqli_query($linkbd_V7, $sqlrReduccion_n);
				$rowReduccion_n = mysqli_fetch_row($resReduccion_n);

				$reduccion += $rowReduccion_n[0];

	
				//============= AQUI FINALIZA EL PROCESO PARA CALCULAR LA REDUCCION =========//
	
	
				//============= AQUI INICIA EL PROCESO PARA CALCULAR EL CREDITO =========//
				
				$sqlrCredito = "SELECT SUM(valor) FROM ccpettraslados WHERE cuenta = '".$_POST['codigo'][$xx]."' AND fecha BETWEEN '$fechaIni' AND '$fechaFin' AND vigencia='$vigencia' AND estado!='N' AND mediopago = '".$row[1]."' AND tipo='C' AND productoservicio = '' AND fuente='".$row[0]."'";
				$resCredito = mysqli_query($linkbd_V7, $sqlrCredito);
				$rowCredito = mysqli_fetch_row($resCredito);
	
				$credito = $rowCredito[0];
	
				//============= AQUI FINALIZA EL PROCESO PARA CALCULAR EL CREDITO =========//
	
				//============= AQUI INICIA EL PROCESO PARA CALCULAR EL CONTRACREDITO =========//
	
				$sqlrContracreditoCredito = "SELECT SUM(valor) FROM ccpettraslados WHERE cuenta = '".$_POST['codigo'][$xx]."' AND fecha BETWEEN '$fechaIni' AND '$fechaFin' AND vigencia='$vigencia' AND estado!='N' AND mediopago = '".$row[1]."' AND tipo='R' AND productoservicio = '' AND fuente='".$row[0]."'";
				$resContracreditoCredito = mysqli_query($linkbd_V7, $sqlrContracreditoCredito);
				$rowContracreditoCredito = mysqli_fetch_row($resContracreditoCredito);
	
				$contracredito = $rowContracreditoCredito[0];
	
				//============= AQUI FINALIZA EL PROCESO PARA CALCULAR EL CONTRACREDITO =========//
	
				//============= AQUI INICIA EL PROCESO PARA CALCULAR EL VALOR DE LOS CDPS =========//
	
	
				$sqlrCdp = "SELECT C.consvigencia, C.tipo_mov,D.valor,D.tipo_mov FROM ccpetcdp C, ccpetcdp_detalle D WHERE D.cuenta = '".$_POST['codigo'][$xx]."' AND D.vigencia='$vigencia' AND D.consvigencia=C.consvigencia AND C.vigencia='$vigencia' AND NOT(D.estado='N') AND D.valor>0 AND D.tipo_mov=C.tipo_mov AND D.tipo_mov='201' AND C.fecha BETWEEN '$fechaIni' AND '$fechaFin' AND D.fuente = '".$row[0]."' AND D.productoservicio = '' AND D.medio_pago = '".$row[1]."' UNION SELECT C.consvigencia, C.tipo_mov,SUM(D.valor),D.tipo_mov FROM ccpetcdp C, ccpetcdp_detalle D WHERE D.cuenta = '".$_POST['codigo'][$xx]."' AND D.vigencia='$vigencia' AND D.consvigencia=C.consvigencia AND C.vigencia='$vigencia' AND NOT(D.estado='N') AND D.valor>0 AND D.tipo_mov=C.tipo_mov AND (D.tipo_mov='401' OR D.tipo_mov='402') AND D.fuente = '".$row[0]."' AND D.productoservicio = '' AND D.medio_pago = '".$row[1]."' AND EXISTS(SELECT 1 FROM ccpetcdp DAUX WHERE DAUX.consvigencia=D.consvigencia AND DAUX.tipo_mov='201' AND DAUX.fecha BETWEEN '$fechaIni' AND '$fechaFin')";
				
				$resCdp = mysqli_query($linkbd_V7, $sqlrCdp);
				if(mysqli_num_rows($resCdp)!=0)
				{
					while($rowCdp = mysqli_fetch_row($resCdp))
					{
						if($rowCdp[3]=='201')
						{
							$totalCDPEnt+=round($rowCdp[2],2);
						}
						else if( substr($rowCdp[3],0,1) == '4' )
						{
							$totalCDPEnt-=round($rowCdp[2],2);
						}
					}
				}
	
				//============= AQUI FINALIZA EL PROCESO PARA CALCULAR EL VALOR DE LOS CDPS =========//
	
				//============= AQUI INICIA EL PROCESO PARA CALCULAR EL VALOR DE LOS RPS =========//
	
	
				$sqlrRp = "SELECT R.consvigencia,R.fecha,R.tipo_mov,RD.valor,RD.tipo_mov FROM ccpetrp R,ccpetrp_detalle RD where  R.vigencia='$vigencia' AND RD.cuenta = '".$_POST['codigo'][$xx]."' AND RD.consvigencia=R.consvigencia AND RD.vigencia='$vigencia'  AND NOT(R.estado='N') AND R.tipo_mov=RD.tipo_mov AND R.tipo_mov='201' AND RD.valor>0 AND R.fecha BETWEEN '$fechaIni' AND '$fechaFin' AND RD.fuente = '".$row[0]."' AND RD.productoservicio = '' AND RD.medio_pago = '".$row[1]."' UNION SELECT R.consvigencia,R.fecha,R.tipo_mov,SUM(RD.valor),RD.tipo_mov FROM ccpetrp R,ccpetrp_detalle RD where  R.vigencia='$vigencia' AND RD.cuenta = '".$_POST['codigo'][$xx]."' AND RD.consvigencia=R.consvigencia AND RD.vigencia='$vigencia'  AND NOT(R.estado='N') AND R.tipo_mov=RD.tipo_mov AND (R.tipo_mov='401' OR R.tipo_mov='402') AND RD.valor>0 AND EXISTS(SELECT 1 FROM ccpetrp RAUX WHERE RAUX.consvigencia=R.consvigencia AND RAUX.tipo_mov='201' AND RAUX.fecha BETWEEN '$fechaIni' AND '$fechaFin') AND RD.fuente = '".$row[0]."' AND RD.productoservicio = '' AND RD.medio_pago = '".$row[1]."'";
				//echo $sqlrRp."<br>";
				$resRp = mysqli_query($linkbd_V7, $sqlrRp);
				if(mysqli_num_rows($resRp)!=0)
				{
					while($rowRp = mysqli_fetch_row($resRp))
					{
						if( $rowRp[4]=='201')
						{

							$totalRPEnt+=$rowRp[3];
							$arregloRP[]=$rowRp[0];
						}
						else if( substr($rowRp[4],0,1) == '4')
						{
							$totalRPEnt-=$rowRp[3];
						}
					}
				}
	
				//============= AQUI FINALIZA EL PROCESO PARA CALCULAR EL VALOR DE LOS RPS =========//
	
				//============= AQUI INICIA EL PROCESO PARA CALCULAR EL VALOR DE LAS CXP =========//
	
	
				$sqlrCxp = "SELECT T.id_orden,T.fecha,T.tipo_mov,TD.valor,T.estado,TD.tipo_mov FROM tesoordenpago T,tesoordenpago_det TD WHERE T.vigencia=$vigencia  AND T.id_orden=TD.id_orden AND NOT(T.estado='N') AND TD.valor>0 AND TD.cuentap = '".$_POST['codigo'][$xx]."' AND T.tipo_mov=TD.tipo_mov AND T.tipo_mov='201' AND T.fecha BETWEEN '$fechaIni' AND '$fechaFin'  AND TD.productoservicio = '' AND TD.fuente = '".$row[0]."' AND TD.medio_pago = '".$row[1]."' UNION SELECT T.id_orden,T.fecha,T.tipo_mov,TD.valor,T.estado,TD.tipo_mov FROM tesoordenpago T,tesoordenpago_det TD WHERE T.vigencia=$vigencia  AND T.id_orden=TD.id_orden AND NOT(T.estado='N') AND TD.valor>0 AND TD.cuentap = '".$_POST['codigo'][$xx]."' AND T.tipo_mov=TD.tipo_mov AND (T.tipo_mov='401' OR T.tipo_mov='402') AND EXISTS(SELECT 1 FROM tesoordenpago TAUX WHERE TAUX.id_orden=T.id_orden AND TAUX.tipo_mov='201' AND TAUX.fecha BETWEEN '$fechaIni' AND '$fechaFin')  AND TD.productoservicio = '' AND TD.fuente = '".$row[0]."' AND TD.medio_pago = '".$row[1]."'";
	
				$resCxp = mysqli_query($linkbd_V7, $sqlrCxp);
				if(mysqli_num_rows($resCxp)!=0)
				{
					while($rowCxp = mysqli_fetch_row($resCxp))
					{
						if($rowCxp[5]=='201')
						{
							$totalCxPEnt+=$rowCxp[3];
						}
						else if($rowCxp[5]=='401')
						{
							$totalCxPEnt-=$rowCxp[3];
						}
					}
				}
	
				for ($xi=0; $xi <sizeof($arregloRP); $xi++) 
				{
					$sqlrCxpNomina = "SELECT HN.id_nom,HN.periodo,HNP.valor,HN.fecha FROM hum_nom_cdp_rp HNR,humnom_presupuestal HNP,humnomina HN WHERE HNR.rp=$arregloRP[$xi] AND HNR.nomina=HNP.id_nom AND HNP.cuenta = '".$_POST['codigo'][$xx]."' AND HNR.vigencia='$vigencia' AND  NOT(HNR.estado='N' OR HNR.estado='R') AND HNP.valor>0 AND HN.id_nom=HNR.nomina AND HN.fecha BETWEEN '$fechaIni' AND '$fechaFin' AND HNP.medio_pago = '".$row[1]."' AND HNP.fuente = '".$row[0]."'";
					//echo $sqlrCxpNomina."<br>";
					$resCxpNomina = mysqli_query($linkbd_V7, $sqlrCxpNomina);
	
					if(mysqli_num_rows($resCxpNomina)==0)
						echo "";
					else
					{
						while($rowCxpNomina = mysqli_fetch_row($resCxpNomina))
						{
							$sqlrEgresoNomina = "SELECT TEND.valordevengado FROM tesoegresosnomina TEN,tesoegresosnomina_det TEND WHERE  TEN.id_orden=".$rowCxpNomina[0]." AND TEN.vigencia='$vigencia' AND NOT(TEN.estado='N' OR TEN.estado='R')  AND TEN.fecha BETWEEN '$fechaIni' AND '$fechaFin' AND TEN.id_egreso=TEND.id_egreso AND NOT(TEND.tipo='SE' OR TEND.tipo='PE' OR TEND.tipo='DS' OR TEND.tipo='RE' OR TEND.tipo='FS') AND TEND.cuentap LIKE '".$_POST['codigo'][$xx]."%' AND TEND.fuente = '".$row[0]."' ";
							//echo $sqlrEgresoNomina."<br>";
							$resEgresoNomina = mysqli_query($linkbd_V7, $sqlrEgresoNomina);
							//$rowEgresoNomina = mysqli_fetch_row($resEgresoNomina);

							if(mysqli_num_rows($resEgresoNomina)!=0)
							{
								while($rowEgresoNomina = mysqli_fetch_row($resEgresoNomina))
								{
									$totalEgresoEnt+=$rowEgresoNomina[0];
								}
							}

							$totalCxPEnt+=$rowCxpNomina[2];
							
						}
					}
	
				}
	
				//============= AQUI FINALIZA EL PROCESO PARA CALCULAR EL VALOR DE LAS CXP =========//
	
				//============= AQUI INICIA EL PROCESO PARA CALCULAR EL VALOR DE LOS EGRESOS =========//
	
	
				$sqlrEgreso = "SELECT TE.id_egreso,TE.fecha,TE.tipo_mov,TD.valor FROM tesoegresos TE,tesoordenpago_det TD, tesoordenpago TIA where TIA.vigencia='$vigencia' AND TIA.id_orden=TD.id_orden AND TD.cuentap = '".$_POST['codigo'][$xx]."' AND TE.id_orden=TD.id_orden AND  NOT(TE.estado='N') AND TD.valor >0 AND TE.tipo_mov='201' AND TE.fecha BETWEEN '$fechaIni' AND '$fechaFin' AND TD.productoservicio = '' AND TD.fuente = '".$row[0]."' AND TD.medio_pago = '".$row[1]."' UNION SELECT TE.id_egreso,TE.fecha,TE.tipo_mov,TD.valor FROM tesoegresos TE,tesoordenpago_det TD,tesoordenpago TIO where TIO.vigencia='$vigencia' AND TIO.id_orden=TD.id_orden AND TD.cuentap = '".$_POST['codigo'][$xx]."' AND TE.id_orden=TD.id_orden AND  NOT(TE.estado='N') AND TD.valor >0 AND TE.tipo_mov='401'  AND EXISTS(SELECT 1 FROM tesoegresos TEAUX WHERE TEAUX.id_egreso=TE.id_egreso AND TEAUX.tipo_mov='201' AND TEAUX.fecha BETWEEN '$fechaIni' AND '$fechaFin') AND TD.productoservicio = '' AND TD.fuente = '".$row[0]."' AND TD.medio_pago = '".$row[1]."'";
	
				$resEgreso = mysqli_query($linkbd_V7, $sqlrEgreso);
				if(mysqli_num_rows($resEgreso)!=0)
				{
					while($rowEgreso = mysqli_fetch_row($resEgreso))
					{
						if($rowEgreso[2]=='201')
						{
							$totalEgresoEnt+=$rowEgreso[3];
						}
						else if($rowEgreso[2]=='401')
						{
							$totalEgresoEnt-=$rowEgreso[3];
						}
					}
				}
	
				//============= AQUI FINALIZA EL PROCESO PARA CALCULAR EL VALOR DE LOS EGRESOS =========//
	

				/* $sqlr_cdp = "SELECT sum(D.valor) FROM ccpetcdp AS C, ccpetcdp_detalle AS D WHERE C.consvigencia = D.consvigencia AND C.vigencia = D.vigencia AND D.cuenta='".$_POST['codigo'][$xx]."' AND C.fecha BETWEEN '$fechaIni' AND '$fechaFin' AND D.tipo_mov = '201' AND D.fuente = '".$row[0]."' AND D.productoservicio = '' AND D.medio_pago='".$row[1]."'";

				//$sqlr_cdp = "SELECT sum(valor) FROM ccpetcdp_detalle WHERE cuenta='".$_POST['codigo'][$xx]."' AND fuente = '".$row[0]."' AND productoservicio = '' AND medio_pago='".$row[1]."'";
				$res_cdp = mysqli_query($linkbd, $sqlr_cdp);
				$row_cdp = mysqli_fetch_array($res_cdp);

				$sqlr_rp = "SELECT sum(D.valor) FROM ccpetrp AS C, ccpetrp_detalle AS D WHERE C.consvigencia = D.consvigencia AND C.vigencia = D.vigencia AND D.cuenta='".$_POST['codigo'][$xx]."' AND C.fecha BETWEEN '$fechaIni' AND '$fechaFin' AND D.tipo_mov = '201' AND D.fuente = '".$row[0]."' AND D.productoservicio = '' AND D.medio_pago='".$row[1]."'";

				//$sqlr_rp = "SELECT sum(valor) FROM ccpetrp_detalle WHERE cuenta='".$_POST['codigo'][$xx]."' AND fuente = '".$row[0]."' AND productoservicio = '' AND medio_pago='".$row[1]."'";
				$res_rp = mysqli_query($linkbd, $sqlr_rp);
				$row_rp = mysqli_fetch_array($res_rp);

				$sqlr_cxp = "SELECT sum(D.valor) FROM tesoordenpago AS C, tesoordenpago_det AS D WHERE C.id_orden =  D.id_orden AND D.cuentap = '".$_POST['codigo'][$xx]."' AND C.fecha BETWEEN '$fechaIni' AND '$fechaFin' AND  NOT(C.estado='N' OR C.estado='R') AND  C.tipo_mov = '201' AND D.productoservicio = '' AND D.fuente = '".$row[0]."' AND D.medio_pago='".$row[1]."'";

				//$sqlr_cxp = "SELECT sum(valor) FROM tesoordenpago_det WHERE cuentap='".$_POST['codigo'][$xx]."' AND productoservicio = '' AND fuente = '".$row[0]."' AND medio_pago='".$row[1]."'";
				$res_cxp = mysqli_query($linkbd, $sqlr_cxp);
				$row_cxp = mysqli_fetch_array($res_cxp);

				$sqlr_cxp_nom = "SELECT sum(D.valor) FROM humnomina AS C, humnom_presupuestal AS D WHERE C.id_nom = D.id_nom AND C.fecha BETWEEN '$fechaIni' AND '$fechaFin' AND D.cuenta = '".$_POST['codigo'][$xx]."' AND D.fuente = '".$row[0]."' AND C.estado!='N'";//echo $sqlr_cxp_nom."<br>";
				$res_cxp_nom = mysqli_query($linkbd, $sqlr_cxp_nom);
				$row_cxp_nom = mysqli_fetch_row($res_cxp_nom);

				$valorCxp = $row_cxp[0] + $row_cxp_nom[0];

				$sqlr_egreso = "SELECT sum(TD.valor) FROM tesoordenpago T, tesoordenpago_det TD, tesoegresos TE WHERE TE.id_orden=T.id_orden AND T.id_orden=TD.id_orden AND TD.cuentap = '".$_POST['codigo'][$xx]."' AND T.fecha BETWEEN '$fechaIni' AND '$fechaFin' AND  NOT(TE.estado='N' OR TE.estado='R') AND TD.valor >0 AND TE.tipo_mov='201' AND TD.productoservicio = '' AND TD.fuente = '".$row[0]."' AND TD.medio_pago='".$row[1]."'";

				//$sqlr_egreso = "SELECT sum(valor) FROM tesoordenpago T, tesoordenpago_det TD, tesoegresos TE WHERE TE.id_orden=T.id_orden AND T.id_orden=TD.id_orden AND TD.cuentap = '".$_POST['codigo'][$xx]."' AND TE.vigencia='$_POST[vigencia]' AND TD.vigencia='$_POST[vigencia]' AND  NOT(TE.estado='N') AND TD.valor >0 AND TE.tipo_mov='201' AND TD.productoservicio = '' AND TD.fuente = '".$row[0]."' AND TD.medio_pago='".$row[1]."'";
				$res_egreso = mysqli_query($linkbd, $sqlr_egreso);
				$row_egreso = mysqli_fetch_row($res_egreso);

				$sqlr_egreso_nom = "SELECT sum(D.valordevengado) FROM tesoegresosnomina AS C, tesoegresosnomina_det AS D WHERE C.id_egreso = D.id_egreso AND C.fecha BETWEEN '$fechaIni' AND '$fechaFin' AND NOT(D.tipo='SE' OR D.tipo='PE' OR D.tipo='DS' OR D.tipo='RE') AND D.cuentap = '$cuenta'";
				$res_egreso_nom = mysqli_query($linkbd, $sqlr_egreso_nom);
				$row_egreso_nom = mysqli_fetch_row($res_egreso_nom);

				$valorEgreso = $row_egreso[0] + $row_egreso_nom[0]; 

				$definitivo = $row[3];
				$compromisoEnEjecucion = $row_rp[0] - $valorCxp;
				$saldo = $definitivo - $row_cdp[0];
				$cuentasPorPagar = $valorCxp - $valorEgreso;*/

				if($row[2] == 'I' || $row[2] == 'G' || $row[2] == 'C' || $row[2] == 'R')
				{
					$row[3] = 0;
				}

				$compromisoEnEjecucion = $totalRPEnt - $totalCxPEnt;

				$cuentasPorPagar = $totalCxPEnt - $totalEgresoEnt;

				$presupuestoDefinitivo = $row[3] + $adicion - $reduccion + $credito - $contracredito;

				$saldo = $presupuestoDefinitivo - $totalCDPEnt;

				$i++;
				$objPHPExcel->setActiveSheetIndex(0)
				->setCellValueExplicit ("A$i", $row[1], PHPExcel_Cell_DataType :: TYPE_STRING)
				->setCellValueExplicit ("B$i", $row[2], PHPExcel_Cell_DataType :: TYPE_STRING)
				->setCellValueExplicit ("C$i", '', PHPExcel_Cell_DataType :: TYPE_STRING)
				->setCellValueExplicit ("D$i", $_POST['codigo'][$xx], PHPExcel_Cell_DataType :: TYPE_STRING)
				->setCellValueExplicit ("E$i", $row[0], PHPExcel_Cell_DataType :: TYPE_STRING)
				->setCellValueExplicit ("F$i", '', PHPExcel_Cell_DataType :: TYPE_STRING)
				->setCellValueExplicit ("G$i", '', PHPExcel_Cell_DataType :: TYPE_STRING)
				->setCellValueExplicit ("H$i", $_POST['nombre'][$xx], PHPExcel_Cell_DataType :: TYPE_STRING)
				->setCellValueExplicit ("I$i", '', PHPExcel_Cell_DataType :: TYPE_NUMERIC)
				->setCellValueExplicit ("J$i", $row[3], PHPExcel_Cell_DataType :: TYPE_NUMERIC)
				->setCellValueExplicit ("K$i", $adicion, PHPExcel_Cell_DataType :: TYPE_NUMERIC)
				->setCellValueExplicit ("L$i", $reduccion, PHPExcel_Cell_DataType :: TYPE_NUMERIC)
				->setCellValueExplicit ("M$i", $credito, PHPExcel_Cell_DataType :: TYPE_NUMERIC)
				->setCellValueExplicit ("N$i", $contracredito, PHPExcel_Cell_DataType :: TYPE_NUMERIC)
				->setCellValueExplicit ("O$i", $presupuestoDefinitivo, PHPExcel_Cell_DataType :: TYPE_NUMERIC)
				->setCellValueExplicit ("P$i", $totalCDPEnt, PHPExcel_Cell_DataType :: TYPE_NUMERIC)
				->setCellValueExplicit ("Q$i", $totalRPEnt, PHPExcel_Cell_DataType :: TYPE_NUMERIC)
				->setCellValueExplicit ("R$i", $totalCxPEnt, PHPExcel_Cell_DataType :: TYPE_NUMERIC)
				->setCellValueExplicit ("S$i", round($compromisoEnEjecucion,2), PHPExcel_Cell_DataType :: TYPE_NUMERIC)
				->setCellValueExplicit ("T$i", $totalEgresoEnt, PHPExcel_Cell_DataType :: TYPE_NUMERIC)
				->setCellValueExplicit ("U$i", $cuentasPorPagar, PHPExcel_Cell_DataType :: TYPE_NUMERIC)
				->setCellValueExplicit ("V$i", $saldo, PHPExcel_Cell_DataType :: TYPE_NUMERIC);
				$objPHPExcel->getActiveSheet()->getStyle("A$i:V$i")->applyFromArray($styleArray);

				//$objPHPExcel-> getActiveSheet()->getStyle ("A$i:H$i")-> getStartColor ()-> setRGB ('A6E5F3');
			}
		}

		//$sqlr = "SELECT fuente, medioPago, unidad, valor FROM ccpetinicialvalorgastos WHERE cuenta = '".$_POST[codigo][$xx]."' AND vigencia='$_POST[vigencia]' $crit $crit1";

		//$sqlr = "SELECT id_fuente, medio_pago, subproducto, codigocuin, valorcsf, valorssf, subclase, codproyecto, indicador_producto, metas FROM ccpproyectospresupuesto_presupuesto WHERE rubro = '".$_POST['codigo'][$xx]."' $critinv1";

		$sqlr = "SELECT CP.id_fuente, CP.medio_pago, CP.subproducto, CP.codigocuin, SUM(CP.valorcsf), SUM(CP.valorssf), CP.subclase, CP.codproyecto, CP.indicador_producto, CP.metas FROM ccpproyectospresupuesto_presupuesto AS CP WHERE CP.rubro = '".$_POST['codigo'][$xx]."' $critinv1 GROUP BY CP.id_fuente, CP.medio_pago, CP.subproducto, CP.codigocuin, CP.subclase, CP.indicador_producto UNION SELECT CA.fuente, CA.mediopago, CA.cuenta_clasificadora, CA.clasificador, 0, 0, CA.cuenta_clasificadora, CA.tipo, CA.indicadorproducto, CA.tipo FROM ccpetadiciones AS CA WHERE CA.cuenta = '".$_POST['codigo'][$xx]."' AND CA.vigencia='$vigencia' AND LEFT(CA.cuenta, 3)='2.3' AND NOT EXISTS(SELECT 1 FROM ccpproyectospresupuesto_presupuesto AS CBT WHERE CBT.rubro = '".$_POST['codigo'][$xx]."' AND (CA.cuenta_clasificadora = CBT.subproducto OR CA.cuenta_clasificadora = CBT.subclase) AND CA.fuente = CBT.id_fuente AND CA.indicadorproducto = CBT.indicador_producto) GROUP BY CA.fuente, CA.mediopago, CA.cuenta_clasificadora, CA.indicadorproducto UNION SELECT CPA.id_fuente, CPA.medio_pago, CPA.subproducto, CPA.codigo_cuin, 0, 0, CPA.subclase, CPA.codproyecto, CPA.indicador_producto, CPA.metas FROM ccpetadicion_inversion_detalles AS CPA WHERE CPA.rubro = '".$_POST['codigo'][$xx]."' AND NOT EXISTS(SELECT 1 FROM ccpproyectospresupuesto_presupuesto AS CBTA WHERE CBTA.rubro = '".$_POST['codigo'][$xx]."' AND (CPA.subclase = CBTA.subproducto OR CPA.subclase = CBTA.subclase) AND CPA.id_fuente = CBTA.id_fuente AND CPA.indicador_producto = CBTA.indicador_producto) GROUP BY CPA.id_fuente, CPA.medio_pago, CPA.subproducto, CPA.codigo_cuin, CPA.subclase, CPA.indicador_producto";
		//echo $sqlr."<br> <br>";

		$result = mysqli_query($linkbd, $sqlr);
		if(mysqli_num_rows($result) > 0){
			while($row = mysqli_fetch_array($result)){
				$i++;
				if($row[1] == 'CSF'){
					$valor = $row[4];
				}else{
					$valor = $row[5];
				}

				$inicial = 0;
				$adicion = 0;
				$reduccion = 0;
				$credito = 0;
				$contracredito = 0;
				$presupuestoDefinitivo = 0;
				$totalCDPEnt = 0;
				$totalRPEnt = 0;
				$totalCxPEnt = 0;
				$totalEgresoEnt = 0;
				$saldo = 0;
				$arregloRP=[];

				$definitivo = 0;
				$compromisoEnEjecucion = 0;
				$cuentasPorPagar = 0;
				$cuinBienesServicios = '';

				$sqlr_cuin = "SELECT nombre FROM ccpet_cuin WHERE codigo_cuin = '$row[3]'";
				$result_cuin = mysqli_query($linkbd, $sqlr_cuin);
				$row_cuin = mysqli_fetch_array($result_cuin);
				$nombreCuinProducto = $row_cuin[0];
				$cuinBienesServicios = $row[3];

				$sqlr_bienes = "SELECT titulo FROM ccpetbienestransportables WHERE grupo = '$row[2]'";
				$result_bienes = mysqli_query($linkbd, $sqlr_bienes);
				$row_bienes = mysqli_fetch_array($result_bienes);

				$codigoProducto = $row[2];

				if($row_bienes[0] == ''){
					$sqlr_bienes = "SELECT titulo FROM ccpetservicios WHERE grupo = '$row[6]'";
					$result_bienes = mysqli_query($linkbd, $sqlr_bienes);
					$row_bienes = mysqli_fetch_array($result_bienes);
					$codigoProducto = $row[6];
				}

				if($nombreCuinProducto == ''){
					$nombreCuinProducto = $row_bienes[0];
					$cuinBienesServicios = $row[6];
				}

				//============= AQUI INICIA EL PROCESO PARA CALCULAR LA ADICION =========//


				$sqlrAdicion = "SELECT SUM(valor) FROM ccpetadiciones WHERE cuenta = '".$_POST['codigo'][$xx]."' AND fecha BETWEEN '$fechaIni' AND '$fechaFin' AND vigencia='$vigencia' AND estado!='N' AND mediopago = '".$row[1]."' AND fuente = '".$row[0]."' AND (cuenta_clasificadora = '".$row[2]."' OR cuenta_clasificadora = '".$row[6]."') AND indicadorproducto = '".$row[8]."'";
				$resAdicion = mysqli_query($linkbd_V7, $sqlrAdicion);
				$rowAdicion = mysqli_fetch_row($resAdicion);
	
				$adicion = $rowAdicion[0];

				$sqlrAdicionInversion = "SELECT SUM(PD.valorcsf) FROM ccpetadicion_inversion AS P, ccpetadicion_inversion_detalles AS PD, ccpetacuerdos AS CA WHERE P.id = PD.codproyecto AND P.id_acuerdo = CA.id_acuerdo AND CA.fecha BETWEEN '$fechaIni' AND '$fechaFin' AND PD.rubro='".$_POST['codigo'][$xx]."' AND P.vigencia='$vigencia' AND medio_pago = '".$row[1]."' AND id_fuente = '".$row[0]."'  AND (subclase = '".$row[2]."' OR subclase = '".$row[6]."') AND indicador_producto = '".$row[8]."'";
            
				$resAdicionInversion = mysqli_query($linkbd_V7, $sqlrAdicionInversion);
				$rowAdicionInversion = mysqli_fetch_row($resAdicionInversion);
				$adicion += $rowAdicionInversion[0];
	
				//============= AQUI FINALIZA EL PROCESO PARA CALCULAR LA ADICION =========//
	
				//============= AQUI INICIA EL PROCESO PARA CALCULAR LA REDUCCION =========//
				
				$sqlrReduccion = "SELECT SUM(valor) FROM ccpetreducciones WHERE cuenta = '".$_POST['codigo'][$xx]."' AND fecha BETWEEN '$fechaIni' AND '$fechaFin' AND vigencia='$vigencia' AND estado!='N' AND mediopago = '".$row[1]."' AND fuente = '".$row[0]."' AND (cuenta_clasificadora = '".$row[2]."' OR cuenta_clasificadora = '".$row[6]."') AND indicadorproducto = '".$row[8]."'";
				$resReduccion = mysqli_query($linkbd_V7, $sqlrReduccion);
				$rowReduccion = mysqli_fetch_row($resReduccion);
	
				$reduccion = $rowReduccion[0];

				$sqlrReduccion_n = "SELECT SUM(CR.valor) FROM ccpetreduccion AS CR, ccpetacuerdos AS CAC WHERE CR.rubro = '".$_POST['codigo'][$xx]."' AND CR.acuerdo = CAC.id_acuerdo AND CAC.fecha BETWEEN '$fechaIni' AND '$fechaFin' AND CR.vigencia='$vigencia' AND CR.medio_pago = '".$row[1]."' AND CR.fuente = '".$row[0]."' AND (CR.clasificador = '".$row[2]."' OR CR.clasificador = '".$row[6]."') AND CR.indicador_producto = '".$row[8]."'";
				$resReduccion_n = mysqli_query($linkbd_V7, $sqlrReduccion_n);
				$rowReduccion_n = mysqli_fetch_row($resReduccion_n);

				$reduccion += $rowReduccion_n[0];
	
				//============= AQUI FINALIZA EL PROCESO PARA CALCULAR LA REDUCCION =========//
	
	
				//============= AQUI INICIA EL PROCESO PARA CALCULAR EL CREDITO =========//
				
				$sqlrCredito = "SELECT SUM(valor) FROM ccpettraslados WHERE cuenta = '".$_POST['codigo'][$xx]."' AND fecha BETWEEN '$fechaIni' AND '$fechaFin' AND vigencia='$vigencia' AND estado!='N' AND mediopago = '".$row[1]."' AND tipo='C' AND (productoservicio = '".$row[2]."' OR productoservicio = '".$row[6]."') AND fuente='".$row[0]."' AND indicador_producto = '".$row[8]."'";
				$resCredito = mysqli_query($linkbd_V7, $sqlrCredito);
				$rowCredito = mysqli_fetch_row($resCredito);
	
				$credito = $rowCredito[0];
	
				//============= AQUI FINALIZA EL PROCESO PARA CALCULAR EL CREDITO =========//
	
				//============= AQUI INICIA EL PROCESO PARA CALCULAR EL CONTRACREDITO =========//
	
				$sqlrContracreditoCredito = "SELECT SUM(valor) FROM ccpettraslados WHERE cuenta = '".$_POST['codigo'][$xx]."' AND fecha BETWEEN '$fechaIni' AND '$fechaFin' AND vigencia='$vigencia' AND estado!='N' AND mediopago = '".$row[1]."' AND tipo='R' AND (productoservicio = '".$row[2]."' OR productoservicio = '".$row[6]."') AND fuente='".$row[0]."' AND indicador_producto = '".$row[8]."'";
				$resContracreditoCredito = mysqli_query($linkbd_V7, $sqlrContracreditoCredito);
				$rowContracreditoCredito = mysqli_fetch_row($resContracreditoCredito);
	
				$contracredito = $rowContracreditoCredito[0];
	
				//============= AQUI FINALIZA EL PROCESO PARA CALCULAR EL CONTRACREDITO =========//
	
				//============= AQUI INICIA EL PROCESO PARA CALCULAR EL VALOR DE LOS CDPS =========//
	
				$sqlrCdp = "SELECT C.consvigencia, C.tipo_mov,D.valor,D.tipo_mov FROM ccpetcdp C, ccpetcdp_detalle D WHERE D.cuenta = '".$_POST['codigo'][$xx]."' AND D.vigencia='$vigencia' AND D.consvigencia=C.consvigencia AND C.vigencia='$vigencia' AND NOT(D.estado='N') AND D.valor>0 AND D.tipo_mov=C.tipo_mov AND D.tipo_mov='201' AND C.fecha BETWEEN '$fechaIni' AND '$fechaFin' AND D.fuente = '".$row[0]."' AND (D.productoservicio = '".$row[2]."' OR D.productoservicio = '".$row[6]."') AND D.medio_pago = '".$row[1]."' AND D.indicador_producto = '".$row[8]."' UNION SELECT C.consvigencia, C.tipo_mov,D.valor,D.tipo_mov FROM ccpetcdp C, ccpetcdp_detalle D WHERE D.cuenta = '".$_POST['codigo'][$xx]."' AND D.vigencia='$vigencia' AND D.consvigencia=C.consvigencia AND C.vigencia='$vigencia' AND NOT(D.estado='N') AND D.valor>0 AND D.tipo_mov=C.tipo_mov AND (D.tipo_mov='401' OR D.tipo_mov='402') AND D.fuente = '".$row[0]."' AND (D.productoservicio = '".$row[2]."' OR D.productoservicio = '".$row[6]."') AND D.medio_pago = '".$row[1]."' AND D.indicador_producto = '".$row[8]."' AND EXISTS(SELECT 1 FROM ccpetcdp DAUX WHERE DAUX.consvigencia=D.consvigencia AND DAUX.tipo_mov='201' AND DAUX.fecha BETWEEN '$fechaIni' AND '$fechaFin')";
				
				$resCdp = mysqli_query($linkbd_V7, $sqlrCdp);
				if(mysqli_num_rows($resCdp)!=0)
				{
					while($rowCdp = mysqli_fetch_row($resCdp))
					{
						if($rowCdp[3]=='201')
						{
							$totalCDPEnt+=round($rowCdp[2],2);
						}
						else if( substr($rowCdp[3],0,1) == '4' )
						{
							$totalCDPEnt-=round($rowCdp[2],2);
						}
					}
				}
	
				//============= AQUI FINALIZA EL PROCESO PARA CALCULAR EL VALOR DE LOS CDPS =========//
	
				//============= AQUI INICIA EL PROCESO PARA CALCULAR EL VALOR DE LOS RPS =========//
	
				
				$sqlrRp = "SELECT R.consvigencia,R.fecha,R.tipo_mov,RD.valor,RD.tipo_mov FROM ccpetrp R,ccpetrp_detalle RD where  R.vigencia='$vigencia' AND RD.cuenta = '".$_POST['codigo'][$xx]."' AND RD.consvigencia=R.consvigencia AND RD.vigencia='$vigencia'  AND NOT(R.estado='N') AND R.tipo_mov=RD.tipo_mov AND R.tipo_mov='201' AND RD.valor>0 AND R.fecha BETWEEN '$fechaIni' AND '$fechaFin' AND RD.fuente = '".$row[0]."' AND (RD.productoservicio = '".$row[2]."' OR RD.productoservicio = '".$row[6]."') AND RD.medio_pago = '".$row[1]."' AND RD.indicador_producto = '".$row[8]."' UNION SELECT R.consvigencia,R.fecha,R.tipo_mov,RD.valor,RD.tipo_mov FROM ccpetrp R,ccpetrp_detalle RD where  R.vigencia='$vigencia' AND RD.cuenta = '".$_POST['codigo'][$xx]."' AND RD.consvigencia=R.consvigencia AND RD.vigencia='$vigencia'  AND NOT(R.estado='N') AND R.tipo_mov=RD.tipo_mov AND (R.tipo_mov='401' OR R.tipo_mov='402') AND RD.valor>0 AND EXISTS(SELECT 1 FROM ccpetrp RAUX WHERE RAUX.consvigencia=R.consvigencia AND RAUX.tipo_mov='201' AND RAUX.fecha BETWEEN '$fechaIni' AND '$fechaFin') AND RD.fuente = '".$row[0]."' AND (RD.productoservicio = '".$row[2]."' OR RD.productoservicio = '".$row[6]."') AND RD.medio_pago = '".$row[1]."' AND RD.indicador_producto = '".$row[8]."'";
				//echo $sqlrRp."<br>";
				$resRp = mysqli_query($linkbd_V7, $sqlrRp);
				if(mysqli_num_rows($resRp)!=0)
				{
					while($rowRp = mysqli_fetch_row($resRp))
					{
						if( $rowRp[4]=='201')
						{
							$totalRPEnt+=$rowRp[3];
							$arregloRP[]=$rowRp[0];
						}
						else if( substr($rowRp[4],0,1) == '4')
						{
							$totalRPEnt-=$rowRp[3];
						}
					}
				}
	
				//============= AQUI FINALIZA EL PROCESO PARA CALCULAR EL VALOR DE LOS RPS =========//
	
				//============= AQUI INICIA EL PROCESO PARA CALCULAR EL VALOR DE LAS CXP =========//
	
	
				$sqlrCxp = "SELECT T.id_orden,T.fecha,T.tipo_mov,TD.valor,T.estado,TD.tipo_mov FROM tesoordenpago T,tesoordenpago_det TD WHERE T.vigencia=$vigencia  AND T.id_orden=TD.id_orden AND NOT(T.estado='N') AND TD.valor>0 AND TD.cuentap = '".$_POST['codigo'][$xx]."' AND T.tipo_mov=TD.tipo_mov AND T.tipo_mov='201' AND T.fecha BETWEEN '$fechaIni' AND '$fechaFin'  AND (TD.productoservicio = '".$row[2]."' OR TD.productoservicio = '".$row[6]."') AND TD.fuente = '".$row[0]."' AND TD.medio_pago = '".$row[1]."' AND TD.indicador_producto = '".$row[8]."' UNION SELECT T.id_orden,T.fecha,T.tipo_mov,TD.valor,T.estado,TD.tipo_mov FROM tesoordenpago T,tesoordenpago_det TD WHERE T.vigencia=$vigencia  AND T.id_orden=TD.id_orden AND NOT(T.estado='N') AND TD.valor>0 AND TD.cuentap = '".$_POST['codigo'][$xx]."' AND T.tipo_mov=TD.tipo_mov AND (T.tipo_mov='401' OR T.tipo_mov='402') AND EXISTS(SELECT 1 FROM tesoordenpago TAUX WHERE TAUX.id_orden=T.id_orden AND TAUX.tipo_mov='201' AND TAUX.fecha BETWEEN '$fechaIni' AND '$fechaFin')  AND (TD.productoservicio = '".$row[2]."' OR TD.productoservicio = '".$row[6]."') AND TD.fuente = '".$row[0]."' AND TD.medio_pago = '".$row[1]."' AND TD.indicador_producto = '".$row[8]."'";
	
				$resCxp = mysqli_query($linkbd_V7, $sqlrCxp);
				if(mysqli_num_rows($resCxp)!=0)
				{
					while($rowCxp = mysqli_fetch_row($resCxp))
					{
						if($rowCxp[5]=='201')
						{
							$totalCxPEnt+=$rowCxp[3];
						}
						else if($rowCxp[5]=='401')
						{
							$totalCxPEnt-=$rowCxp[3];
						}
					}
				}
	
				for ($xi=0; $xi <sizeof($arregloRP); $xi++) 
				{
					$sqlrCxpNomina = "SELECT HN.id_nom,HN.periodo,HNP.valor,HN.fecha FROM hum_nom_cdp_rp HNR,humnom_presupuestal HNP,humnomina HN WHERE HNR.rp=$arregloRP[$xi] AND HNR.nomina=HNP.id_nom AND HNP.cuenta = '".$_POST['codigo'][$xx]."' AND HNR.vigencia='$vigencia' AND  NOT(HNR.estado='N' OR HNR.estado='R') AND HNP.valor>0 AND HN.id_nom=HNR.nomina AND HN.fecha BETWEEN '$fechaIni' AND '$fechaFin' AND HNP.medio_pago = '".$row[1]."' AND HNP.fuente = '".$row[0]."'";
					
					$resCxpNomina = mysqli_query($linkbd_V7, $sqlrCxpNomina);
	
					if(mysqli_num_rows($resCxpNomina)==0)
						echo "";
					else
					{
						while($rowCxpNomina = mysqli_fetch_row($resCxpNomina))
						{
							$sqlrEgresoNomina = "SELECT TEND.valordevengado FROM tesoegresosnomina TEN,tesoegresosnomina_det TEND WHERE  TEN.id_orden=".$rowCxpNomina[0]." AND TEN.vigencia='$vigencia' AND NOT(TEN.estado='N' OR TEN.estado='R')  AND TEN.fecha BETWEEN '$fechaIni' AND '$fechaFin' AND TEN.id_egreso=TEND.id_egreso AND NOT(TEND.tipo='SE' OR TEND.tipo='PE' OR TEND.tipo='DS' OR TEND.tipo='RE' OR TEND.tipo='FS') AND TEND.cuentap LIKE '".$_POST['codigo'][$xx]."%' AND TEND.fuente = '".$row[0]."' ";
							
							$resEgresoNomina = mysqli_query($linkbd_V7, $sqlrEgresoNomina);
							if(mysqli_num_rows($resEgresoNomina)!=0)
							{
								while($rowEgresoNomina = mysqli_fetch_row($resEgresoNomina))
								{
									$totalEgresoEnt+=$rowEgresoNomina[0];
								}
							}
							$totalCxPEnt+=$rowCxpNomina[2];
							//$totalEgresoEnt+=$rowEgresoNomina[0];
						}
					}
	
				}
	
				//============= AQUI FINALIZA EL PROCESO PARA CALCULAR EL VALOR DE LAS CXP =========//
	
				//============= AQUI INICIA EL PROCESO PARA CALCULAR EL VALOR DE LOS EGRESOS =========//
	
	
				$sqlrEgreso = "SELECT TE.id_egreso,TE.fecha,TE.tipo_mov,TD.valor FROM tesoegresos TE,tesoordenpago_det TD, tesoordenpago TIA where TIA.vigencia='$vigencia' AND TIA.id_orden=TD.id_orden AND TD.cuentap = '".$_POST['codigo'][$xx]."' AND TE.id_orden=TD.id_orden AND  NOT(TE.estado='N') AND TD.valor >0 AND TE.tipo_mov='201' AND TE.fecha BETWEEN '$fechaIni' AND '$fechaFin' AND (TD.productoservicio = '".$row[2]."' OR TD.productoservicio = '".$row[6]."') AND TD.fuente = '".$row[0]."' AND TD.medio_pago = '".$row[1]."' AND TD.indicador_producto = '".$row[8]."' UNION SELECT TE.id_egreso,TE.fecha,TE.tipo_mov,TD.valor FROM tesoegresos TE,tesoordenpago_det TD,tesoordenpago TIO where TIO.vigencia='$vigencia' AND TIO.id_orden=TD.id_orden AND TD.cuentap = '".$_POST['codigo'][$xx]."' AND TE.id_orden=TD.id_orden AND  NOT(TE.estado='N') AND TD.valor >0 AND TE.tipo_mov='401'  AND EXISTS(SELECT 1 FROM tesoegresos TEAUX WHERE TEAUX.id_egreso=TE.id_egreso AND TEAUX.tipo_mov='201' AND TEAUX.fecha BETWEEN '$fechaIni' AND '$fechaFin') AND (TD.productoservicio = '".$row[2]."' OR TD.productoservicio = '".$row[6]."') AND TD.fuente = '".$row[0]."' AND TD.medio_pago = '".$row[1]."' AND TD.indicador_producto = '".$row[8]."'";
	
				$resEgreso = mysqli_query($linkbd_V7, $sqlrEgreso);
				if(mysqli_num_rows($resEgreso)!=0)
				{
					while($rowEgreso = mysqli_fetch_row($resEgreso))
					{
						if($rowEgreso[2]=='201')
						{
							$totalEgresoEnt+=$rowEgreso[3];
						}
						else if($rowEgreso[2]=='401')
						{
							$totalEgresoEnt-=$rowEgreso[3];
						}
					}
				}
	
				//============= AQUI FINALIZA EL PROCESO PARA CALCULAR EL VALOR DE LOS EGRESOS =========//
	

				/* $sqlr_cdp = "SELECT sum(D.valor) FROM ccpetcdp AS C, ccpetcdp_detalle AS D WHERE C.consvigencia = D.consvigencia AND C.vigencia = D.vigencia AND D.cuenta='".$_POST['codigo'][$xx]."' AND C.fecha BETWEEN '$fechaIni' AND '$fechaFin' AND D.tipo_mov = '201' AND D.fuente = '".$row[0]."' AND indicador_producto = '".$row[8]."' AND D.productoservicio = ".$codigoProducto." AND D.medio_pago='".$row[1]."'";

				//$sqlr_cdp = "SELECT sum(valor) FROM ccpetcdp_detalle WHERE cuenta='".$_POST['codigo'][$xx]."' AND productoservicio = ".$codigoProducto." AND fuente = '".$row[0]."' AND indicador_producto = '".$row[8]."' AND medio_pago='".$row[1]."'";
				$res_cdp = mysqli_query($linkbd, $sqlr_cdp);
				
				$row_cdp = mysqli_fetch_array($res_cdp);

				$sqlr_rp = "SELECT sum(D.valor) FROM ccpetrp AS C, ccpetrp_detalle AS D WHERE C.consvigencia = D.consvigencia AND C.vigencia = D.vigencia AND D.cuenta='".$_POST['codigo'][$xx]."' AND C.fecha BETWEEN '$fechaIni' AND '$fechaFin' AND D.tipo_mov = '201' AND D.fuente = '".$row[0]."' AND indicador_producto = '".$row[8]."' AND D.productoservicio = ".$codigoProducto." AND D.medio_pago='".$row[1]."'";

				//$sqlr_rp = "SELECT sum(valor) FROM ccpetrp_detalle WHERE cuenta='".$_POST['codigo'][$xx]."' AND productoservicio = ".$codigoProducto." AND fuente = '".$row[0]."' AND indicador_producto = '".$row[8]."' AND medio_pago='".$row[1]."'";
				$res_rp = mysqli_query($linkbd, $sqlr_rp);
				$row_rp = mysqli_fetch_array($res_rp);

				$sqlr_cxp = "SELECT sum(D.valor) FROM tesoordenpago AS C, tesoordenpago_det AS D WHERE C.id_orden =  D.id_orden AND D.cuentap = '".$_POST['codigo'][$xx]."' AND C.fecha BETWEEN '$fechaIni' AND '$fechaFin' AND  NOT(C.estado='N' OR C.estado='R') AND   C.tipo_mov = '201' AND D.productoservicio = ".$codigoProducto." AND indicador_producto = '".$row[8]."' AND D.fuente = '".$row[0]."' AND D.medio_pago='".$row[1]."'";

				//$sqlr_cxp = "SELECT sum(valor) FROM tesoordenpago_det WHERE cuentap='".$_POST['codigo'][$xx]."' AND productoservicio = ".$codigoProducto." AND fuente = '".$row[0]."' AND indicador_producto = '".$row[8]."' AND medio_pago='".$row[1]."'";
				$res_cxp = mysqli_query($linkbd, $sqlr_cxp);
				$row_cxp = mysqli_fetch_array($res_cxp);

				$sqlr_cxp_nom = "SELECT sum(D.valor) FROM humnomina AS C, humnom_presupuestal AS D WHERE C.id_nom = D.id_nom AND C.fecha BETWEEN '$fechaIni' AND '$fechaFin' AND D.cuenta = '".$_POST['codigo'][$xx]."' AND D.fuente = '".$row[0]."' AND C.estado!='N'";//echo $sqlr_cxp_nom."<br>";
				$res_cxp_nom = mysqli_query($linkbd, $sqlr_cxp_nom);
				$row_cxp_nom = mysqli_fetch_row($res_cxp_nom);

				$valorCxp = $row_cxp[0] + $row_cxp_nom[0];

				$sqlr_egreso = "SELECT sum(TD.valor) FROM tesoordenpago T, tesoordenpago_det TD, tesoegresos TE WHERE TE.id_orden=T.id_orden AND T.id_orden=TD.id_orden AND TD.cuentap = '".$_POST['codigo'][$xx]."' AND T.fecha BETWEEN '$fechaIni' AND '$fechaFin' AND  NOT(TE.estado='N' OR TE.estado='R') AND TD.valor >0 AND TE.tipo_mov='201' AND TD.productoservicio = ".$codigoProducto." AND TD.fuente = '".$row[0]."' AND TD.indicador_producto = '".$row[8]."' AND TD.medio_pago='".$row[1]."'";

				//$sqlr_egreso = "SELECT sum(valor) FROM tesoordenpago T, tesoordenpago_det TD, tesoegresos TE WHERE TE.id_orden=T.id_orden AND T.id_orden=TD.id_orden AND TD.cuentap = '".$_POST['codigo'][$xx]."' AND TE.vigencia='$_POST[vigencia]' AND TD.vigencia='$_POST[vigencia]' AND  NOT(TE.estado='N') AND TD.valor >0 AND TE.tipo_mov='201' AND TD.productoservicio = ".$codigoProducto." AND TD.fuente = '".$row[0]."' AND TD.indicador_producto = '".$row[8]."' AND TD.medio_pago='".$row[1]."'";
				$res_egreso = mysqli_query($linkbd, $sqlr_egreso);
				$row_egreso = mysqli_fetch_row($res_egreso);

				$sqlr_egreso_nom = "SELECT sum(D.valordevengado) FROM tesoegresosnomina AS C, tesoegresosnomina_det AS D WHERE C.id_egreso = D.id_egreso AND C.fecha BETWEEN '$fechaIni' AND '$fechaFin' AND NOT(D.tipo='SE' OR D.tipo='PE' OR D.tipo='DS' OR D.tipo='RE') AND D.cuentap = '$cuenta'";
				$res_egreso_nom = mysqli_query($linkbd, $sqlr_egreso_nom);
				$row_egreso_nom = mysqli_fetch_row($res_egreso_nom);

				$valorEgreso = $row_egreso[0] + $row_egreso_nom[0]; */

				$sqlr_producto = "SELECT indicador_producto FROM ccpetproductos WHERE codigo_indicador='".$row[8]."'";
				$res_producto = mysqli_query($linkbd, $sqlr_producto);
				$row_producto = mysqli_fetch_array($res_producto);

				$sector = substr($row[8], 0, 2);
				$sqlr_sector = "SELECT nombre FROM ccpetsectores WHERE codigo=".$sector;
				$res_sector = mysqli_query($linkbd, $sqlr_sector);
				$row_sector = mysqli_fetch_array($res_sector);

				$sqlr_metapdm = "SELECT meta_pdm FROM ccpet_metapdm WHERE id_meta='".$row[9]."'";
				$res_metapdm = mysqli_query($linkbd, $sqlr_metapdm);
				$row_metapdm = mysqli_fetch_array($res_metapdm);

				if($row[6] == 'I' || $row[6] == 'G')
				{
					$valor = 0;
				}
				

				$compromisoEnEjecucion = $totalRPEnt - $totalCxPEnt;

				$cuentasPorPagar = $totalCxPEnt - $totalEgresoEnt;

				$presupuestoDefinitivo = $valor + $adicion - $reduccion + $credito - $contracredito;

				$saldo = $presupuestoDefinitivo - $totalCDPEnt;

				$objPHPExcel->setActiveSheetIndex(0)
				->setCellValueExplicit ("A$i", $row[1], PHPExcel_Cell_DataType :: TYPE_STRING)
				->setCellValueExplicit ("B$i", $_POST['unidad'][$xx], PHPExcel_Cell_DataType :: TYPE_STRING)
				->setCellValueExplicit ("C$i", $sector.' - '.$row_sector[0], PHPExcel_Cell_DataType :: TYPE_STRING)
				->setCellValueExplicit ("D$i", $_POST['codigo'][$xx], PHPExcel_Cell_DataType :: TYPE_STRING)
				->setCellValueExplicit ("E$i", $row[0], PHPExcel_Cell_DataType :: TYPE_STRING)
				->setCellValueExplicit ("F$i", $row[8].' - '.$row_producto[0], PHPExcel_Cell_DataType :: TYPE_STRING)
				->setCellValueExplicit ("G$i", $cuinBienesServicios, PHPExcel_Cell_DataType :: TYPE_STRING)
				->setCellValueExplicit ("H$i", $nombreCuinProducto, PHPExcel_Cell_DataType :: TYPE_STRING)
				->setCellValueExplicit ("I$i", $row[9].' - '.$row_metapdm[0], PHPExcel_Cell_DataType :: TYPE_STRING)
				->setCellValueExplicit ("J$i", $valor, PHPExcel_Cell_DataType :: TYPE_NUMERIC)
				->setCellValueExplicit ("K$i", $adicion, PHPExcel_Cell_DataType :: TYPE_NUMERIC)
				->setCellValueExplicit ("L$i", $reduccion, PHPExcel_Cell_DataType :: TYPE_NUMERIC)
				->setCellValueExplicit ("M$i", $credito, PHPExcel_Cell_DataType :: TYPE_NUMERIC)
				->setCellValueExplicit ("N$i", $contracredito, PHPExcel_Cell_DataType :: TYPE_NUMERIC)
				->setCellValueExplicit ("O$i", $presupuestoDefinitivo, PHPExcel_Cell_DataType :: TYPE_NUMERIC)
				->setCellValueExplicit ("P$i", $totalCDPEnt, PHPExcel_Cell_DataType :: TYPE_NUMERIC)
				->setCellValueExplicit ("Q$i", $totalRPEnt, PHPExcel_Cell_DataType :: TYPE_NUMERIC)
				->setCellValueExplicit ("R$i", $totalCxPEnt, PHPExcel_Cell_DataType :: TYPE_NUMERIC)
				->setCellValueExplicit ("S$i", round($compromisoEnEjecucion,2), PHPExcel_Cell_DataType :: TYPE_NUMERIC)
				->setCellValueExplicit ("T$i", $totalEgresoEnt, PHPExcel_Cell_DataType :: TYPE_NUMERIC)
				->setCellValueExplicit ("U$i", $cuentasPorPagar, PHPExcel_Cell_DataType :: TYPE_NUMERIC)
				->setCellValueExplicit ("V$i", $saldo, PHPExcel_Cell_DataType :: TYPE_NUMERIC);
				$objPHPExcel->getActiveSheet()->getStyle("A$i:V$i")->applyFromArray($styleArray);
			}
		}
		$i++;
    }
		
	//----Propiedades de la hoja 1
	$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth('50');
	$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth('50');
	$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->getColumnDimension('K')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->getColumnDimension('L')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->getColumnDimension('M')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->getColumnDimension('N')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->getColumnDimension('O')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->getColumnDimension('P')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->getColumnDimension('Q')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->getColumnDimension('R')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->getColumnDimension('S')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->getColumnDimension('T')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->getColumnDimension('U')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->getColumnDimension('V')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->setTitle('EJECUCION');
	$objPHPExcel->setActiveSheetIndex(0);

//----Guardar documento----
header('Content-Type: application/vnd.ms-excel');
header('Content-Disposition: attachment;filename="REPORTE EJECUCION GASTOS.xls"');
header('Cache-Control: max-age=0');
 
$objWriter=PHPExcel_IOFactory::createWriter($objPHPExcel,'Excel5');
$objWriter->save('php://output');
exit;

?>