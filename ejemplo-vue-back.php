<?php

    require_once './comun.inc';
    require './funciones.inc';
 
    session_start();

	$linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");

    $maxVersion = ultimaVersionGastosCCPET();

    $out = array('error' => false);

    $action = "show";

    if(isset($_GET['action'])){
        $action = $_GET['action'];
    }
    
    if($action == 'show'){

        $rubrosInicial = [];
        $rubrosAd = [];
        $rubrosTrasladosCred = [];
        $accounts = [];
        $fuentes = [];
        $rubrosInv = [];
        $rubrosAdInv = [];
        $rubrosTrasladosCredInv = [];
        $nombreRubrosInv = [];

        $vigencia = '2023';

        $sqlrIni = "SELECT DISTINCT cuenta, fuente FROM ccpetinicialgastosfun WHERE vigencia = '".$vigencia."'";
        $resIni = mysqli_query($linkbd, $sqlrIni);
        while($rowIni = mysqli_fetch_row($resIni)){
            $rubro = '';
            $rubroNom = [];
            $rubro = $rowIni[0]."-".$rowIni[1];
            /* $nombre = buscacuentaccpetgastos($rowIni[0], $maxVersion)." - ".buscaNombreFuenteCCPET($rowIni[1]); */
            array_push($rubroNom, $rubro);
            array_push($rubrosInicial, $rubroNom);
        }

        $sqlrAd = "SELECT DISTINCT cuenta, fuente FROM ccpet_adiciones WHERE vigencia = '".$vigencia."' AND cuenta != '' AND tipo_cuenta = 'G'";
        $respAd = mysqli_query($linkbd, $sqlrAd);
        while($rowAd = mysqli_fetch_row($respAd)){
            $rubro = '';
            $rubroNom = [];
            $rubro = $rowAd[0]."-".$rowAd[1];
            /* $nombre = buscacuentaccpetgastos($rowAd[0], $maxVersion)." - ".buscaNombreFuenteCCPET($rowAd[1]); */
            array_push($rubroNom, $rubro);
            array_push($rubrosAd, $rubroNom);
        }

        $sqlrTrasladosCred = "SELECT DISTINCT cuenta, fuente FROM ccpet_traslados WHERE vigencia = '".$vigencia."' AND cuenta != '' AND tipo = 'C'";
        $respTrasladosCred = mysqli_query($linkbd, $sqlrTrasladosCred);
        while($rowTrasladosCred = mysqli_fetch_row($respTrasladosCred)){
            $rubro = '';
            $rubroNom = [];
            $rubro = $rowTrasladosCred[0]."-".$rowTrasladosCred[1];
            /* $nombre = buscacuentaccpetgastos($rowTrasladosCred[0], $maxVersion)." - ".buscaNombreFuenteCCPET($rowTrasladosCred[1]); */
            array_push($rubroNom, $rubro);
            array_push($rubrosTrasladosCred, $rubroNom);
        }

        $sqlrAccounts = "SELECT codigo, nombre, tipo FROM cuentasccpet WHERE version = $maxVersion AND SUBSTRING(codigo, 1, 3) != '2.3'";
        $respAccounts = mysqli_query($linkbd, $sqlrAccounts);
        while($rowAccounts = mysqli_fetch_row($respAccounts)){
            array_push($accounts, $rowAccounts);
        }

        $sqlrFuentes = "SELECT codigo_fuente, nombre FROM ccpet_fuentes_cuipo";
        $respFuentes = mysqli_query($linkbd, $sqlrFuentes);
        while($rowFuentes = mysqli_fetch_row($respFuentes)){
            array_push($fuentes, $rowFuentes);
        }

        $sqlrInv = "SELECT TB2.indicador_producto, TB1.codigo, TB2.id_fuente FROM ccpproyectospresupuesto AS TB1, ccpproyectospresupuesto_presupuesto AS TB2 WHERE TB1.id = TB2.codproyecto AND TB1.vigencia = '$vigencia'";
        $respInv = mysqli_query($linkbd, $sqlrInv);
        while($rowInv = mysqli_fetch_row($respInv)){
            $rubros = [];
            $nombreRubro = '';
            $nombreRubro = $rowInv[0].'-'.$rowInv[1].'-'.$rowInv[2];
            array_push($rubros, $nombreRubro);
            array_push($rubrosInv, $rubros);
        }

        $sqlrAdInv = "SELECT programatico, bpim, fuente FROM ccpet_adiciones WHERE vigencia = '$vigencia' AND bpim != ''";
        $respAdInv = mysqli_query($linkbd, $sqlrAdInv);
        while($rowAdInv = mysqli_fetch_row($respAdInv)){
            $rubros = [];
            $nombreRubro = '';

            $nombreRubro = $rowAdInv[0].'-'.$rowAdInv[1].'-'.$rowAdInv[2];
            array_push($rubros, $nombreRubro);
            array_push($rubrosAdInv, $rubros);
        }

        $sqlrTrasladosCredInv = "SELECT programatico, bpim, fuente FROM ccpet_traslados WHERE vigencia = '$vigencia' AND bpim != '' AND tipo = 'C'";
        $respTrasladosCredInv = mysqli_query($linkbd, $sqlrTrasladosCredInv);
        while($rowTrasladosInv = mysqli_fetch_row($respTrasladosCredInv)){
            $rubros = [];
            $nombreRubro = '';

            $nombreRubro = $rowTrasladosInv[0].'-'.$rowTrasladosInv[1].'-'.$rowTrasladosInv[2];
            array_push($rubros, $nombreRubro);
            array_push($rubrosTrasladosCredInv, $rubros);
        }

        $sqlrProyInv = "SELECT codigo, nombre FROM ccpproyectospresupuesto WHERE vigencia = '$vigencia'";
        $respProyInv = mysqli_query($linkbd, $sqlrProyInv);
        while($rowProyInv = mysqli_fetch_row($respProyInv)){
            $nomProy = [];
            array_push($nomProy, $rowProyInv[0]);
            array_push($nomProy, $rowProyInv[1]);
            array_push($nomProy, 'A');
            array_push($nombreRubrosInv, $nomProy);
        }

        $sqlrSectores = "SELECT codigo, nombre FROM ccpetsectores WHERE version = (SELECT MAX(version) FROM ccpetsectores)";
        $respSectores = mysqli_query($linkbd, $sqlrSectores);
        while($rowSectores = mysqli_fetch_row($respSectores)){
            $nomSec = [];
            array_push($nomSec, $rowSectores[0]);
            array_push($nomSec, $rowSectores[1]);
            array_push($nomSec, 'A');
            array_push($nombreRubrosInv, $nomSec);
        }

        $sqlrProgramas = "SELECT codigo, nombre FROM ccpetprogramas WHERE version = (SELECT MAX(version) FROM ccpetprogramas)";
        $respProgramas = mysqli_query($linkbd, $sqlrProgramas);
        while($rowProgramas = mysqli_fetch_row($respProgramas)){
            $nomProg = [];
            array_push($nomProg, $rowProgramas[0]);
            array_push($nomProg, $rowProgramas[1]);
            array_push($nomProg, 'A');
            array_push($nombreRubrosInv, $nomProg);
        }

        $sqlrProgramatico = "SELECT codigo_indicador, indicador_producto FROM ccpetproductos WHERE version = (SELECT MAX(version) FROM ccpetproductos)";
        $respProgramatico = mysqli_query($linkbd, $sqlrProgramatico);
        while($rowProgramatico = mysqli_fetch_row($respProgramatico)){
            $nomInd = [];
            array_push($nomInd, $rowProgramatico[0]);
            array_push($nomInd, $rowProgramatico[1]);
            array_push($nomInd, 'A');
            array_push($nombreRubrosInv, $nomInd);
        }


        $out['rubrosInicial'] = $rubrosInicial;
        $out['rubrosAd'] = $rubrosAd;
        $out['rubrosTrasladosCred'] = $rubrosTrasladosCred;
        $out['accounts'] = $accounts;
        $out['fuentes'] = $fuentes;
        $out['rubrosInv'] = $rubrosInv;
        $out['rubrosAdInv'] = $rubrosAdInv;
        $out['rubrosTrasladosCredInv'] = $rubrosTrasladosCredInv;
        $out['nombreRubrosInv'] = $nombreRubrosInv;

    }

    if($action == 'guardarOrdenPresu'){
       
        $user = $_SESSION['nickusu'];
        $ordenPresu = $_POST['orden'];
        $consecutivo = 0;
        $sqlr = "DELETE FROM ccpet_parametros";
        mysqli_query($linkbd, $sqlr);

        $sqlrI = "INSERT INTO ccpet_parametros (orden, user) VALUES ('".$ordenPresu."', '".$user."')";
        mysqli_query($linkbd, $sqlrI);
        
        $out['insertaBien'] = true;
        

    }

    header("Content-type: application/json");
    echo json_encode($out);
    die();