<?php
	header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=utf8");
	require 'comun.inc';
	require "funciones.inc";
    require 'funcionesSP.inc.php';
	session_start();
	$linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");

	date_default_timezone_set("America/Bogota");
	$fec = date("d/m/Y");
	software();
    $sqlr="select *from configbasica where estado='S' ";
	$razonsocial=strtoupper(replaceChar((mysqli_query($linkbd, $sqlr)->fetch_assoc()['razonsocial'])));
    $vigencia = date("Y");
    $arrAgenda = [];
    $arrPublicacion = [];
    $totalAgenda = 0;
    $totalPublicacion = 0;

    $sqlr="SELECT DATE_FORMAT(tb1.fechasig,'%d/%m/%Y') as fecha,tb2.descripcionr as tarea,
    tb1.codigo,tb1.codradicacion,tb1.proceso,tb1.tipot,tb1.estado
    FROM planacresponsables tb1
    INNER JOIN planacradicacion tb2 ON tb2.numeror = tb1.codradicacion
    WHERE tb1.estado='AN' AND tb1.usuariocon = '$_SESSION[cedulausu]' AND YEAR(tb1.fechasig) = '$vigencia'
    AND tb2.usuarior = '$_SESSION[cedulausu]'
    ORDER BY tb1.codigo ASC;";
    $arrTareas = mysqli_fetch_all(mysqli_query($linkbd,$sqlr),MYSQLI_ASSOC);
    $totalTareas = count($arrTareas);
	if (!isset($_SESSION['usuario'])){
		$sqlr = "SELECT * FROM parametros WHERE estado='S'";
		$res = mysqli_query($linkbd,$sqlr);
		while($r = mysqli_fetch_row($res)){$vigencia = $r[1];}
		//*** verificar el username y el pass
		$users = $_POST['user'];
		$pass = $_POST['pass'];
		$sqlr = "SELECT U.nom_usu, R.nom_rol, U.id_rol, U.id_usu, U.foto_usu, U.usu_usu, U.cc_usu FROM usuarios U, roles R WHERE U.usu_usu = '$users' AND U.pass_usu = '$pass' AND U.id_rol = R.id_rol AND U.est_usu='1'";
		$res = mysqli_query($linkbd,$sqlr);
		while($r = mysqli_fetch_row($res)){
			$user = $r[0];
			$perf = $r[1];
			$niv = $r[2];
			$idusu = $r[3];
			$nick = $r[5];
			$dirfoto = $r[4];
			$cedusu = $r[6];
		}
		if ($user == ""){
            header("location: index2.php");
		}else{

			//login correcto
			$_SESSION['vigencia'] = $vigencia;
			$_SESSION['usuario'] = array();
			$_SESSION['usuario'] = $user;
			$_SESSION['perfil'] = $perf;
			$_SESSION['idusuario'] = $idusu;
			$_SESSION['nickusu'] = $nick;
			$_SESSION['cedulausu'] = $cedusu;
			$_SESSION['nivel'] = $niv;
			$_SESSION['linkmod'] = array();
			if($dirfoto == ""){
				$_SESSION['fotousuario'] = "imagenes/usuario_on.png";
			}else{
				$_SESSION["fotousuario"] = "informacion/fotos_usuarios/$dirfoto";
			}
			$corresmensaje = "1";
			$sqlr = "SELECT * FROM usuarios_privilegios WHERE id_usu='$idusu'";
			$row = mysqli_fetch_row(mysqli_query($linkbd,$sqlr));
			//$_SESSION["prcrear"]=$row[1];
			//$_SESSION["preditar"]=$row[2];
			//$_SESSION["prdesactivar"]=$row[3];
			//$_SESSION["preliminar"]=$row[4];
			$_SESSION['prcrear'] = 1;
			$_SESSION['preditar'] = 1;
			$_SESSION['prdesactivar'] = 1;
			$_SESSION['preliminar'] = 1;
			$sqlr = "SELECT valor_inicial, valor_final, descripcion_valor FROM dominios WHERE nombre_dominio='SEPARADOR_NUMERICO'";
			$row = mysqli_fetch_row(mysqli_query($linkbd,$sqlr));
			if($row[0]!=''){
				$_SESSION['spdecimal'] = $row[0];
			}else{
				$_SESSION['spdecimal']= '.';
			}
			if($row[1]!=''){
				$_SESSION['spmillares'] = $row[1];
			}else{
				$_SESSION['spmillares'] = ',';
			}
			if($row[2]!=''){
				$_SESSION['ndecimales'] = $row[2];
			}
			else{
				$_SESSION['ndecimales'] = 2;
			}
			//******************* menuss ************************
			//***** NUEVO REVISION DE LOS MODULOS ***************
			$sqlr = "SELECT DISTINCT (modulos.nombre),modulos.id_modulo,modulos.libre FROM modulos, modulo_rol WHERE modulo_rol.id_rol = $niv AND modulos.id_modulo = modulo_rol.id_modulo GROUP BY (modulos.nombre),modulos.id_modulo,modulos.libre ORDER BY modulos.id_modulo";
			$res = mysqli_query($linkbd,$sqlr);
			while($roww = mysqli_fetch_row($res)){
				$_SESSION['linkmod'][$roww[1]].=$roww[2];
			}
			//verificació de tipo de caracteres
			$sqlr="SELECT valor_inicial, valor_final FROM dominios WHERE nombre_dominio='TIPO_CARACTER_VERPHP'";
			$row = mysqli_fetch_row(mysqli_query($linkbd,$sqlr));
			$_SESSION['VERCARPHPINI'] = $row[0];
			$_SESSION['VERCARPHPFIN'] = $row[1];
			$sqlr = "SELECT valor_inicial, valor_final FROM dominios WHERE nombre_dominio='TIPO_CARACTER_VERPDF'";
			$row = mysqli_fetch_row(mysqli_query($linkbd,$sqlr));
			$_SESSION['VERCARPDFINI'] = $row[0];
			$_SESSION['VERCARPDFFIN'] = $row[1];
			//crear y borrar tabla """temporal"""
			$datin = datosiniciales();
			$hoy = getdate();
			$auxtablatemporal1 = "usr_session_".$hoy['year'];
			$auxtablatemporal2 = "usr_session_".$hoy['year'].$hoy['mon'].$hoy['mday'];
			$_SESSION['tablatemporal'] = $auxtablatemporal2."_".$hoy['hours'].$hoy['minutes'].$hoy['seconds'];
			if($auxtablatemporal1 != '' && $auxtablatemporal2 != ''){
				$sqlr = "SELECT CONCAT('DROP TABLE ' , GROUP_CONCAT(table_name),';') FROM information_schema.tables WHERE table_schema = '$datin[0]' AND table_name LIKE '$auxtablatemporal1%' AND NOT table_name LIKE '$auxtablatemporal2%'";
				$row = mysqli_fetch_row(mysqli_query($linkbd,$sqlr));
				$sqlrdel = $row[0];
				mysqli_query($linkbd,$sqlr);
			}
			//registro log
			$accion = "USUARIO INICIA SESION EN EL SISTEMA";
			$origen = getUserIpAddr();
			generaLogs($_SESSION['nickusu'],'PRI','V',$accion,$origen);


        }
	}
?>
<!DOCTYPE >
<html lang="es">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1">
		<meta name="viewport" content="user-scalable=no">
		<title>FINANCIERO</title>
		<link href="css/css2.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<link href="css/style.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<script type="text/javascript" src="css/programas.js"></script>
		<script type="text/javascript" src="funcioneshf.js"></script>
		<script type='text/javascript' src='JQuery/jquery-2.1.4.min.js'></script>
		<script>$(document).ready(function(){$('body').ripples({resolution: 512,dropRadius: 10,perturbance: 0.04});});</script>
		<?php titlepag();?>
	</head>
	<body>
		<div class="main-container">
			<div class="main-content flex-column">
                <div class="d-flex h-100">
                    <div class="h-100 main-card d-flex flex-column align-items-center justify-between w-50 me-2">
                        <div class="text-center m">
                            <img class="mt-3 mb-2" src="./assets/img/logo_ideal.jpeg" alt="" height="150">
                            <h3 class="fs-3 fw-bold text-black"><?=$razonsocial?></h3>
                            <p class="fs-3 text-black">Hola! <?=ucwords(strtolower($_SESSION['usuario']))?></p>
                        </div>
                        <div class="bg-white group-btn p-1">
                            <button type="button" onclick="window.open('https://ideal-10.com/web/index.php/enlaces-de-interes', '_blank')" class="btn btn-white btn-primary-hover d-flex justify-between align-items-center">
                                <span class="group-hover:text-white">Enlaces de interés</span>
                                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 -960 960 960" fill="#000"><path d="M838-65 720-183v89h-80v-226h226v80h-90l118 118-56 57ZM480-80q-83 0-156-31.5T197-197q-54-54-85.5-127T80-480q0-83 31.5-156T197-763q54-54 127-85.5T480-880q83 0 156 31.5T763-763q54 54 85.5 127T880-480q0 20-2 40t-6 40h-82q5-20 7.5-40t2.5-40q0-20-2.5-40t-7.5-40H654q3 20 4.5 40t1.5 40q0 20-1.5 40t-4.5 40h-80q3-20 4.5-40t1.5-40q0-20-1.5-40t-4.5-40H386q-3 20-4.5 40t-1.5 40q0 20 1.5 40t4.5 40h134v80H404q12 43 31 82.5t45 75.5q20 0 40-2.5t40-4.5v82q-20 2-40 4.5T480-80ZM170-400h136q-3-20-4.5-40t-1.5-40q0-20 1.5-40t4.5-40H170q-5 20-7.5 40t-2.5 40q0 20 2.5 40t7.5 40Zm34-240h118q9-37 22.5-72.5T376-782q-55 18-99 54.5T204-640Zm172 462q-18-34-31.5-69.5T322-320H204q29 51 73 87.5t99 54.5Zm28-462h152q-12-43-31-82.5T480-798q-26 36-45 75.5T404-640Zm234 0h118q-29-51-73-87.5T584-782q18 34 31.5 69.5T638-640Z"/></svg>
                            </button>
                        </div>
                    </div>
                    <div class="card-container bg-transparent d-flex justify-between flex-wrap">
                        <div class="d-flex w-100">
                            <div class="cursor-pointer card d-flex flex-column align-items-center justify-center mb-1 me-1" onclick="window.location.href='<?php echo $_SESSION['linkmod'][1];?>'">
                                <svg xmlns="http://www.w3.org/2000/svg" height="35px" viewBox="0 -960 960 960" fill="#000"><path d="M400-400h160v-80H400v80Zm0-120h320v-80H400v80Zm0-120h320v-80H400v80Zm-80 400q-33 0-56.5-23.5T240-320v-480q0-33 23.5-56.5T320-880h480q33 0 56.5 23.5T880-800v480q0 33-23.5 56.5T800-240H320Zm0-80h480v-480H320v480ZM160-80q-33 0-56.5-23.5T80-160v-560h80v560h560v80H160Zm160-720v480-480Z"/></svg>
                                <span class="fs-3 fw-bold">Contabilidad</span>
                            </div>
                            <div class="cursor-pointer card d-flex flex-column align-items-center justify-center mb-1 me-1" onclick="window.location.href='<?php echo $_SESSION['linkmod'][8];?>'">
                                <svg xmlns="http://www.w3.org/2000/svg" height="35px" viewBox="0 -960 960 960" fill="#000"><path d="M240-80q-50 0-85-35t-35-85v-120h120v-560h600v680q0 50-35 85t-85 35H240Zm480-80q17 0 28.5-11.5T760-200v-600H320v480h360v120q0 17 11.5 28.5T720-160ZM360-600v-80h360v80H360Zm0 120v-80h360v80H360ZM240-160h360v-80H200v40q0 17 11.5 28.5T240-160Zm0 0h-40 400-360Z"/></svg>
                                <span class="fs-3 fw-bold">Contratación</span>
                            </div>
                            <div class="cursor-pointer card d-flex flex-column align-items-center justify-center mb-1" onclick="window.location.href='<?php echo $_SESSION['linkmod'][2];?>'">
                            <svg xmlns="http://www.w3.org/2000/svg" height="35px" viewBox="0 -960 960 960" fill="#000"><path d="M400-480q-66 0-113-47t-47-113q0-66 47-113t113-47q66 0 113 47t47 113q0 66-47 113t-113 47ZM80-160v-112q0-33 17-62t47-44q51-26 115-44t141-18h14q6 0 12 2-8 18-13.5 37.5T404-360h-4q-71 0-127.5 18T180-306q-9 5-14.5 14t-5.5 20v32h252q6 21 16 41.5t22 38.5H80Zm560 40-12-60q-12-5-22.5-10.5T584-204l-58 18-40-68 46-40q-2-14-2-26t2-26l-46-40 40-68 58 18q11-8 21.5-13.5T628-460l12-60h80l12 60q12 5 22.5 11t21.5 15l58-20 40 70-46 40q2 12 2 25t-2 25l46 40-40 68-58-18q-11 8-21.5 13.5T732-180l-12 60h-80Zm40-120q33 0 56.5-23.5T760-320q0-33-23.5-56.5T680-400q-33 0-56.5 23.5T600-320q0 33 23.5 56.5T680-240ZM400-560q33 0 56.5-23.5T480-640q0-33-23.5-56.5T400-720q-33 0-56.5 23.5T320-640q0 33 23.5 56.5T400-560Zm0-80Zm12 400Z"/></svg>
                                <span class="fs-3 fw-bold">Gestión humana</span>
                            </div>
                        </div>
                        <div class="d-flex w-100">
                            <div class="cursor-pointer card d-flex flex-column align-items-center justify-center mb-1 me-1" onclick="window.location.href='<?php echo $_SESSION['linkmod'][9];?>'">
                                <svg xmlns="http://www.w3.org/2000/svg" height="35px" viewBox="0 -960 960 960"  fill="#000"><path d="M220-520 80-600v-160l140-80 140 80v160l-140 80Zm0-92 60-34v-68l-60-34-60 34v68l60 34Zm440 123v-93l140 82v280L560-80 320-220v-280l140-82v93l-60 35v188l160 93 160-93v-188l-60-35Zm-140 89v-480h360l-80 120 80 120H600v240h-80Zm40 69ZM220-680Z"/></svg>
                                <span class="fs-3 fw-bold">Planeación estratégica</span>
                            </div>
                            <div onclick="window.location.href='ccp-principal.php'" class="cursor-pointer card d-flex flex-column align-items-center justify-center mb-1 me-1">
                                <svg xmlns="http://www.w3.org/2000/svg" height="35px" viewBox="0 -960 960 960" fill="#000"><path d="M440-200h80v-40h40q17 0 28.5-11.5T600-280v-120q0-17-11.5-28.5T560-440H440v-40h160v-80h-80v-40h-80v40h-40q-17 0-28.5 11.5T360-520v120q0 17 11.5 28.5T400-360h120v40H360v80h80v40ZM240-80q-33 0-56.5-23.5T160-160v-640q0-33 23.5-56.5T240-880h320l240 240v480q0 33-23.5 56.5T720-80H240Zm280-560v-160H240v640h480v-480H520ZM240-800v160-160 640-640Z"/></svg>
                                <span class="fs-3 fw-bold">Presupuesto</span>
                            </div>
                            <div class="cursor-pointer card d-flex flex-column align-items-center justify-center mb-1" onclick="window.location.href='<?php echo $_SESSION['linkmod'][7];?>'">
                                <svg xmlns="http://www.w3.org/2000/svg" height="35px" viewBox="0 -960 960 960" fill="#000"><path d="M756-120 537-339l84-84 219 219-84 84Zm-552 0-84-84 276-276-68-68-28 28-51-51v82l-28 28-121-121 28-28h82l-50-50 142-142q20-20 43-29t47-9q24 0 47 9t43 29l-92 92 50 50-28 28 68 68 90-90q-4-11-6.5-23t-2.5-24q0-59 40.5-99.5T701-841q15 0 28.5 3t27.5 9l-99 99 72 72 99-99q7 14 9.5 27.5T841-701q0 59-40.5 99.5T701-561q-12 0-24-2t-23-7L204-120Z"/></svg>
                                <span class="fs-3 fw-bold">Herramientas MIPG</span>
                            </div>
                        </div>
                        <div class="d-flex w-100">
                            <div class="cursor-pointer card d-flex flex-column align-items-center justify-center mb-1 me-1" onclick="window.location.href='<?php echo $_SESSION['linkmod'][4];?>'">
                                <svg xmlns="http://www.w3.org/2000/svg" height="35px" viewBox="0 -960 960 960" fill="#000"><path d="M336-120q-91 0-153.5-62.5T120-336q0-38 13-74t37-65l142-171-97-194h530l-97 194 142 171q24 29 37 65t13 74q0 91-63 153.5T624-120H336Zm144-200q-33 0-56.5-23.5T400-400q0-33 23.5-56.5T480-480q33 0 56.5 23.5T560-400q0 33-23.5 56.5T480-320Zm-95-360h190l40-80H345l40 80Zm-49 480h288q57 0 96.5-39.5T760-336q0-24-8.5-46.5T728-423L581-600H380L232-424q-15 18-23.5 41t-8.5 47q0 57 39.5 96.5T336-200Z"/></svg>
                                <span class="fs-3 fw-bold">Tesorería</span>
                            </div>
                            <div class="cursor-pointer card d-flex flex-column align-items-center justify-center mb-1 me-1" onclick="window.location.href='<?php echo $_SESSION['linkmod'][6];?>'">
                                <svg xmlns="http://www.w3.org/2000/svg" height="35px" viewBox="0 -960 960 960"  fill="#000"><path d="M760-400v-260L560-800 360-660v60h-80v-100l280-200 280 200v300h-80ZM560-800Zm20 160h40v-40h-40v40Zm-80 0h40v-40h-40v40Zm80 80h40v-40h-40v40Zm-80 0h40v-40h-40v40ZM280-220l278 76 238-74q-5-9-14.5-15.5T760-240H558q-27 0-43-2t-33-8l-93-31 22-78 81 27q17 5 40 8t68 4q0-11-6.5-21T578-354l-234-86h-64v220ZM40-80v-440h304q7 0 14 1.5t13 3.5l235 87q33 12 53.5 42t20.5 66h80q50 0 85 33t35 87v40L560-60l-280-78v58H40Zm80-80h80v-280h-80v280Z"/></svg>
                                <span class="fs-3 fw-bold">Activos fijos</span>
                            </div>
                            <div class="cursor-pointer card d-flex flex-column align-items-center justify-center mb-1" onclick="window.location.href='<?php echo $_SESSION['linkmod'][5];?>'">
                                <svg xmlns="http://www.w3.org/2000/svg" height="35px" viewBox="0 -960 960 960"  fill="#000"><path d="M160-200h80v-320h480v320h80v-426L480-754 160-626v426Zm-80 80v-560l400-160 400 160v560H640v-320H320v320H80Zm280 0v-80h80v80h-80Zm80-120v-80h80v80h-80Zm80 120v-80h80v80h-80ZM240-520h480-480Z"/></svg>
                                <span class="fs-3 fw-bold">Almacén</span>
                            </div>
                        </div>
                        <div class="d-flex w-100">
                            <div class="cursor-pointer card d-flex flex-column align-items-center justify-center me-1" onclick="window.location.href='<?php echo $_SESSION['linkmod'][10];?>'">
                                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" height="35px" fill="#000"><!--!Font Awesome Free 6.6.0 by @fontawesome - https://fontawesome.com License - https://fontawesome.com/license/free Copyright 2024 Fonticons, Inc.--><path d="M352 256H313.4c-15.7-13.4-35.5-23.1-57.4-28V180.4l-32-3.4-32 3.4V228c-21.9 5-41.7 14.6-57.4 28H16A16 16 0 0 0 0 272v96a16 16 0 0 0 16 16h92.8C129.4 421.7 173 448 224 448s94.6-26.3 115.2-64H352a32 32 0 0 1 32 32 32 32 0 0 0 32 32h64a32 32 0 0 0 32-32A160 160 0 0 0 352 256zM81.6 159.9l142.4-15 142.4 15c9.4 1 17.6-6.8 17.6-16.8V112.9c0-10-8.2-17.8-17.6-16.8L256 107.7V80a16 16 0 0 0 -16-16H208a16 16 0 0 0 -16 16v27.7L81.6 96.1C72.2 95.1 64 102.9 64 112.9v30.2C64 153.1 72.2 160.9 81.6 159.9z"/></svg>
                                <span class="fs-3 fw-bold">Servicios públicos</span>
                            </div>
                            <div class="cursor-pointer card d-flex flex-column align-items-center justify-center  me-1" onclick="window.location.href='<?php echo $_SESSION['linkmod'][0];?>'">
                                <svg xmlns="http://www.w3.org/2000/svg" height="35px" viewBox="0 -960 960 960" fill="#000"><path d="M680-280q25 0 42.5-17.5T740-340q0-25-17.5-42.5T680-400q-25 0-42.5 17.5T620-340q0 25 17.5 42.5T680-280Zm0 120q31 0 57-14.5t42-38.5q-22-13-47-20t-52-7q-27 0-52 7t-47 20q16 24 42 38.5t57 14.5ZM480-80q-139-35-229.5-159.5T160-516v-244l320-120 320 120v227q-19-8-39-14.5t-41-9.5v-147l-240-90-240 90v188q0 47 12.5 94t35 89.5Q310-290 342-254t71 60q11 32 29 61t41 52q-1 0-1.5.5t-1.5.5Zm200 0q-83 0-141.5-58.5T480-280q0-83 58.5-141.5T680-480q83 0 141.5 58.5T880-280q0 83-58.5 141.5T680-80ZM480-494Z"/></svg>
                                <span class="fs-3 fw-bold">Administración</span>
                            </div>
                            <div class="cursor-pointer card d-flex flex-column align-items-center justify-center " onclick="window.location.href='index2.php'">
                                <svg xmlns="http://www.w3.org/2000/svg" height="35px" viewBox="0 -960 960 960"     fill="#000"><path d="M200-120q-33 0-56.5-23.5T120-200v-560q0-33 23.5-56.5T200-840h280v80H200v560h280v80H200Zm440-160-55-58 102-102H360v-80h327L585-622l55-58 200 200-200 200Z"/></svg>
                                <span class="fs-3 fw-bold">Cerrar sesión</span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="d-flex justify-between border-radius shadow-b mt-3 p-3 bg-white">
                    <div class="w-100">
                        <div class="d-flex align-items-center cursor-pointer" onclick="mypop=window.open('plan-actareasbusca.php','','');mypop.focus()">
                            <span class="me-1 fs-3">Tareas</span>
                            <svg xmlns="http://www.w3.org/2000/svg" height="22px" viewBox="0 -960 960 960" fill="#000"><path d="M655-200 513-342l56-56 85 85 170-170 56 57-225 226Zm0-320L513-662l56-56 85 85 170-170 56 57-225 226ZM80-280v-80h360v80H80Zm0-320v-80h360v80H80Z"/></svg>
                            <span class="ms-1 fs-3">(<?=$totalTareas?>)</span>
                        </div>
                        <div class="border border-radius overflow-hidden relative" style="height:4rem;">
                            <ul class="marquee" >
                                <?php for ($i=0; $i < $totalTareas ; $i++) {
                                    $url="plan-actareasresponder.php?idradicado=".$arrTareas[$i]['codradicacion']."&idresponsable=".
                                    $arrTareas[$i]['codigo']."&tipoe=".$arrTareas[$i]['estado']."&tiporad=".$arrTareas[$i]['tipot']
                                ?>
                                <li><a href="<?=$url?>" class="fs-3 fw-normal"><?=$arrTareas[$i]['fecha']." - ". $arrTareas[$i]['tarea']?></a></li>
                                <?php } ?>
                            </ul>
                        </div>
                    </div>
                    <div class="w-100 me-3 ms-3">
                        <div class="d-flex align-items-center cursor-pointer" onclick="mypop=window.open('plan-agenda.php','','');mypop.focus()">
                            <span class="me-1 fs-3">Agenda</span>
                            <svg class="fill-black group-hover:fill-white w-5 h-5" xmlns="http://www.w3.org/2000/svg" height="22px" viewBox="0 -960 960 960">
                                <path d="M560-564v-68q33-14 67.5-21t72.5-7q26 0 51 4t49 10v64q-24-9-48.5-13.5T700-600q-38 0-73 9.5T560-564Zm0 220v-68q33-14 67.5-21t72.5-7q26 0 51 4t49 10v64q-24-9-48.5-13.5T700-380q-38 0-73 9t-67 27Zm0-110v-68q33-14 67.5-21t72.5-7q26 0 51 4t49 10v64q-24-9-48.5-13.5T700-490q-38 0-73 9.5T560-454ZM260-320q47 0 91.5 10.5T440-278v-394q-41-24-87-36t-93-12q-36 0-71.5 7T120-692v396q35-12 69.5-18t70.5-6Zm260 42q44-21 88.5-31.5T700-320q36 0 70.5 6t69.5 18v-396q-33-14-68.5-21t-71.5-7q-47 0-93 12t-87 36v394Zm-40 118q-48-38-104-59t-116-21q-42 0-82.5 11T100-198q-21 11-40.5-1T40-234v-482q0-11 5.5-21T62-752q46-24 96-36t102-12q58 0 113.5 15T480-740q51-30 106.5-45T700-800q52 0 102 12t96 36q11 5 16.5 15t5.5 21v482q0 23-19.5 35t-40.5 1q-37-20-77.5-31T700-240q-60 0-116 21t-104 59ZM280-494Z">
                                </path>
                            </svg>
                            <span class="ms-1 fs-3">(0)</span>
                        </div>
                        <div class="border border-radius overflow-hidden relative" style="height:4rem;">
                            <ul class="marquee" >

                            </ul>
                        </div>
                    </div>
                    <div class="w-100">
                        <div class="d-flex align-items-center cursor-pointer" >
                            <span class="me-1 fs-3">Publicaciones</span>
                                <svg xmlns="http://www.w3.org/2000/svg" height="22px" viewBox="0 -960 960 960" fill="#000"><path d="M120-120v-720h720v720H120Zm600-160H240v60h480v-60Zm-480-60h480v-60H240v60Zm0-140h480v-240H240v240Zm0 200v60-60Zm0-60v-60 60Zm0-140v-240 240Zm0 80v-80 80Zm0 120v-60 60Z"/></svg>
                            <span class="ms-1 fs-3">(0)</span>
                        </div>
                        <div class="border border-radius overflow-hidden relative" style="height:4rem;">
                            <ul class="marquee" >
                            </ul>
                        </div>
                    </div>
                </div>
			</div>
		</div>
	</body>
</html>
