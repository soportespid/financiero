<?php //V 1000 22/02/2021 ?>
<?php
	header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=utf8");
	require "comun.inc";
	require "funciones.inc";
	require "funcionesSP.inc.php";
	session_start();
	$linkbd = conectar_v7();
	$linkbd -> set_charset("utf8"); 
	cargarcodigopag($_GET['codpag'],$_SESSION["nivel"]);
	date_default_timezone_set("America/Bogota");

	$scroll=$_GET['scrtop'];
	$totreg=$_GET['totreg'];
	$idcta=$_GET['idcta'];
	$altura=$_GET['altura'];
	$filtro="'".$_GET['filtro']."'";
?>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta charset="utf-8"/>
	<meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1"/>
	<meta name="viewport" content="user-scalable=no">
	<title>:: IDEAL 10 - Tesoreria</title>
	<link href="css/css2z.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
	<link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
	<link href="css/css4.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
	<link href="css/tabs2.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css"/>
	<script src="sweetalert2/dist/sweetalert2.min.js"></script>
	<link rel="stylesheet" href="sweetalert2/dist/sweetalert2.min.css">
	<script type="text/javascript" src="css/programas.js"></script>
<script>
	function buscacta(e)
		{
			if (document.form2.cuenta.value!="")
			{		
				document.form2.bc.value='1';
				document.getElementById('oculto').value='7';
				document.form2.submit();
			}
		}

	function buscarFuente(e)
	{
		if (document.form2.fuente.value!="")
		{		
			document.form2.bf.value='1';
			document.form2.submit();
		}
	}
	function buscacta2(e,tipo)
	{
		if (tipo == 1)
		{
			document.form2.bc.value='2';
			document.getElementById('oculto').value='7';
			document.form2.submit();
		}
		if(tipo == 2)
		{
			document.form2.bc.value='3';
			document.getElementById('oculto').value='7';
			document.form2.submit();
		}
		if(tipo == 3)
		{
			document.form2.bc.value='4';
			document.getElementById('oculto').value='7';
			document.form2.submit();
			
		}
	}

	function validar(){
		document.getElementById('oculto').value='7';
		document.form2.submit();
	}

	function guardar() {

		var codigo = document.getElementById('codigo').value;
		var nombreIngreso = document.getElementById('nombre').value;
		var precioVenta = document.getElementById('precio').value;
		var tipo = document.getElementById('tipo').value;
		var tipoIngreso = document.getElementById('regalias').value;

		if(document.getElementById('destinoIng')){
			terceros = true;
		}else{
			terceros = false;
		}

		if (codigo != '' && nombreIngreso != '' && precioVenta != '' && tipoIngreso != '') {
			
			if (terceros == true) {
				var destino = document.getElementById('destinoIng').value;
				
				if (destino != "") {
					Swal.fire({
						icon: 'question',
						title: 'Seguro que quieres guardar?',
						showDenyButton: true,
						confirmButtonText: 'Guardar',
						denyButtonText: 'Cancelar',
						}).then((result) => {
							if (result.isConfirmed) {
								document.form2.oculto.value='2';
								document.form2.submit();
							} else if (result.isDenied) {
								Swal.fire('Guardar cancelado', '', 'info')
							}
						}
					)
				}
				else {
					Swal.fire('Falta información', 'Debes seleccionar un destino', 'info');
				}
			}	
			else {

				if (tipo == 'S') {

					var cuenta = document.getElementById('cuenta').value;
					var ingresos2=document.getElementsByName('codigoFuentes[]');

					if (cuenta != "" && ingresos2.length > 0) {
						Swal.fire({
							icon: 'question',
							title: 'Seguro que quieres guardar?',
							showDenyButton: true,
							confirmButtonText: 'Guardar',
							denyButtonText: 'Cancelar',
							}).then((result) => {
								if (result.isConfirmed) {
									document.form2.oculto.value='2';
									document.form2.submit();
								} else if (result.isDenied) {
									Swal.fire('Guardar cancelado', '', 'info')
								}
							}
						)
					}
					else {
						Swal.fire('Falta información', 'Debes tener cuenta presupuestal y fuente', 'info');
					}
				}
				else if (tipo == 'C') {

					ingresos2=document.getElementsByName('codigoFuentes[]');
					dcuentas=document.getElementsByName('dcuentas[]');


					if (ingresos2.length > 0 && dcuentas.length > 0) {

						Swal.fire({
							icon: 'question',
							title: 'Seguro que quieres guardar?',
							showDenyButton: true,
							confirmButtonText: 'Guardar',
							denyButtonText: 'Cancelar',
							}).then((result) => {
								if (result.isConfirmed) {
									document.form2.oculto.value='2';
									document.form2.submit();
								} else if (result.isDenied) {
									Swal.fire('Guardar cancelado', '', 'info')
								}
							}
						)
					}
					else {
						Swal.fire('Falta información', 'Debes tener cuenta presupuestal y fuente', 'info');
					}
				}
			}
		}
		else {
			Swal.fire('Falta información', 'Información faltante en cabecera', 'info');
		}
	}

	function agregardetalle(){

		if(document.form2.valor.value!="" && document.form2.concecont.value!=-1 ){
			document.form2.agregadet.value=1;
			document.getElementById('oculto').value='7';
			document.form2.submit();
		 }
		 else {
			 despliegamodalm('visible','2','Faltan datos para Agregar el Registro');
		}
	}

	function eliminar(variable){
		document.getElementById('elimina').value=variable;
		despliegamodalm('visible','4','Esta Seguro de Eliminar el Registro','2');
	}
	function despliegamodal2(_valor)
		{
			document.getElementById("bgventanamodal2").style.visibility=_valor;
			if(_valor=="hidden")
			{
				document.getElementById('ventana2').src="";
			}
			else 
			{
				document.getElementById('ventana2').src="cuentasppto-ventana1.php?";
			}
		}

		function despliegamodal3(_valor)
		{
			document.getElementById("bgventanamodal2").style.visibility=_valor;
			if(_valor=="hidden")
			{
				document.getElementById('ventana2').src="";
			}
			else 
			{
				document.getElementById('ventana2').src="bienesTransportables-ventana.php";
			}
		}
		
		function despliegamodal4(_valor)
		{
			document.getElementById("bgventanamodal2").style.visibility=_valor;
			if(_valor=="hidden")
			{
				document.getElementById('ventana2').src="";
			}
			else 
			{
				document.getElementById('ventana2').src="servicios-ventana.php";
			}
		}

		function despliegamodal5(_valor)
		{
			document.getElementById("bgventanamodal2").style.visibility=_valor;
			if(_valor=="hidden")
			{
				document.getElementById('ventana2').src="";
			}
			else 
			{
				document.getElementById('ventana2').src="cuin-ventana.php";
			}
		}

		function despliegamodal6(_valor,pos)
		{
			document.getElementById("bgventanamodal2").style.visibility=_valor;
			if(_valor=="hidden")
			{
				document.getElementById('ventana2').src="";
			}
			else 
			{
				document.getElementById('ventana2').src="cuentasppto-ventanaingresos.php?pos="+pos;
			}
		}

		function despliegamodal7(_valor)
		{	
			document.getElementById("bgventanamodal2").style.visibility=_valor;
			if(_valor=="hidden")
			{
				document.getElementById('ventana2').src="";
			}
			else 
			{
				document.getElementById('ventana2').src="fuentes-ventana1.php"
			}
		}

		function despliegamodalClasificadores(_valor, pos, clasi)
		{	
			
			var _tip =  document.getElementById("numeroClasificador").value;
			
			if(clasi != 0 || _tip != '')
			document.getElementById("bgventanamodal2").style.visibility=_valor;
			
			//console.log(clasi);
			if(_valor == "hidden")
			{
				document.getElementById('ventana2').src="";
			}
			
			else
			{
				if(_tip != '1' && _tip != '2' && _tip != '3' && _tip == '' && clasi != 1 && clasi != 2 && clasi != 3 && clasi == '')
				{
					alert('Esta cuenta no tiene clasficador');
				}
				else
				{	
					if(clasi != 0)
					{
						switch(clasi)
						{
							case 1:
								document.getElementById('ventana2').src="cuin-ventana2.php?pos="+pos;
								break;
							case 2:
								document.getElementById('ventana2').src="bienesTransportables-ventana2.php?pos="+pos;
								break;
							case 3:
								document.getElementById('ventana2').src="servicios-ventana2.php?pos="+pos;
								break;
						}	
					}

					if(_tip != '')
					{
						switch(_tip)
						{
							case "1":
								document.getElementById('ventana2').src="cuin-ventana2.php?pos="+pos;
								break;
							case "2":
								document.getElementById('ventana2').src="bienesTransportables-ventana2.php?pos="+pos;
								break;
							case "3":
								document.getElementById('ventana2').src="servicios-ventana2.php?pos="+pos;
								break;
		
						}
					}
					
				}
			}
		} 

		function modalCuentaClasificadora(_valor, pos, clasi)
		{
			console.log(_valor);
			console.log(pos);
			console.log(clasi);
			document.getElementById("bgventanamodal2").style.visibility=_valor;
			if(_valor=="hidden")
			{
				document.getElementById('ventana2').src="";
			}
			else 
			{
				switch(clasi)
					{
						case "1":
							document.getElementById('ventana2').src="cuin-ventana2.php?pos="+pos;
							break;
						case "2":
							document.getElementById('ventana2').src="bienesTransportables-ventana2.php?pos="+pos;
							break;
						case "3":
							document.getElementById('ventana2').src="servicios-ventana2.php?pos="+pos;
							break;
					}
			}
		}

	function despliegamodalm(_valor,_tip,mensa,pregunta){
		document.getElementById("bgventanamodalm").style.visibility=_valor;
		if(_valor=="hidden"){document.getElementById('ventanam').src="";}
		else
		{
			switch(_tip)
			{
				case "1":
					document.getElementById('ventanam').src="ventana-mensaje1.php?titulos="+mensa;break;
				case "2":
					document.getElementById('ventanam').src="ventana-mensaje3.php?titulos="+mensa;break;
				case "3":
					document.getElementById('ventanam').src="ventana-mensaje2.php?titulos="+mensa;break;
				case "4":
					document.getElementById('ventanam').src="ventana-consulta1.php?titulos="+mensa+"&idresp="+pregunta;break;	
			}
		}
	}

	function funcionmensaje()
	{
		document.form2.submit();
	}

	function respuestaconsulta(pregunta){
		switch(pregunta){
			case "1":	document.getElementById('oculto').value='2';document.form2.submit();break;
			case "2":	document.getElementById('oculto').value='6';
						document.form2.submit();break;
		}
	}
	 function validafinalizar(e)
	 {
		 var id=e.id;
		 var check=e.checked;
		 if(id=='terceros'){
			document.getElementById('terceros').value=1;
		 }else{
			 document.form2.terceros.checked=false;
		 }
		 document.getElementById('oculto').value='6';
		 document.form2.submit();
	 }
	function buscacuentap()
	{
		document.form2.buscap.value='1';
		document.form2.oculto.value='7';
		document.form2.submit();
	}

	function agregaFuente() {
		
		var fuente = document.getElementById('fuente').value;
		var nombreFuente = document.getElementById('nfuente').value;

		if (fuente != "" && nombreFuente != "") {

			document.form2.agregaFuente.value = 1;
			document.form2.submit();
		}
		else {
			Swal.fire('Falta información', 'Seleccióna la fuente', 'info');
		}
	}

	function eliminarFuente(pos) {

		Swal.fire({
			icon: 'question',
			title: 'Seguro que quieres eliminar?',
			showDenyButton: true,
			confirmButtonText: 'Eliminar',
			denyButtonText: 'Cancelar',
			}).then((result) => {
				if (result.isConfirmed) {
					document.form2.eliminaFuente.value=pos;
					document.form2.submit();
				}
				else {
					Swal.fire('Eliminar cancelado', '', 'info');
				}
			}
		)
	}


</script>

		<script>
			function adelante(scrtop, numpag, limreg, filtro, next){
				var maximo=document.getElementById('maximo').value;
				var actual=document.getElementById('codigo').value;
				if(parseFloat(maximo)>parseFloat(actual)){
					document.getElementById('oculto').value='1';
					document.getElementById('codigo').value=next;
					var idcta=document.getElementById('codigo').value;
					document.form2.action="teso-editaingresos.php?idcta="+idcta+"&scrtop="+scrtop+"&numpag="+numpag+"&limreg="+limreg+"&filtro="+filtro;
					document.form2.submit();
				}
			}
		
			
			function atrasc(scrtop, numpag, limreg, filtro, prev){
				var minimo=document.getElementById('minimo').value;
				var actual=document.getElementById('codigo').value;
				if(parseFloat(minimo)<parseFloat(actual)){
					document.getElementById('oculto').value='1';
					document.getElementById('codigo').value=prev;
					var idcta=document.getElementById('codigo').value;
					document.form2.action="teso-editaingresos.php?idcta="+idcta+"&scrtop="+scrtop+"&numpag="+numpag+"&limreg="+limreg+"&filtro="+filtro;
					document.form2.submit();
				}
			}
		
			function iratras(scrtop, numpag, limreg, filtro){
				var idrecaudo=document.getElementById('codigo').value;
				location.href="teso-buscaingresos.php?idcta="+idrecaudo;
			}
			function cambiavalor(pos,element){
				document.form2.posicion.value=pos;
				document.form2.valorpos.value=element.value;
				document.form2.submit();
			}
		</script>
<?php titlepag();?>
<?php

	function enocontrarCuentaPresupuestal($cuenta)
	{
		$cuentaDevolver = 0;
		$linkbd=conectar_v7();

		if(isset($cuenta))
		{
			$sqlr = "SELECT clasificadores FROM ccpetprogramarclasificadores WHERE cuenta = '".$cuenta."' ";
			$res = mysqli_query($linkbd, $sqlr);
			$row = mysqli_fetch_row($res);
			$cuentaDevolver = $row[0];
		}
		return $cuentaDevolver;
	}
?>

</head>
<body>
<IFRAME src="alertas.php" name="alertas" id="alertas" style="display:none"></IFRAME>
<span id="todastablas2"></span>
        <?php
		$numpag=$_GET['numpag'];
		$limreg=$_GET['limreg'];
		$scrtop=20*$totreg;
		?>
<table>
	<tr>
		<script>barra_imagenes("teso");</script>
		<?php cuadro_titulos();?>
	</tr>	 
	<tr>
		<?php menu_desplegable("teso");?>
	</tr>
	<tr>
  		<td colspan="3" class="cinta">
			<a href="teso-ingresos.php" class="mgbt"><img src="imagenes/add.png" title="Nuevo"/></a> 
			<a onClick="guardar();" class="mgbt"><img src="imagenes/guarda.png"  title="Guardar"/></a>
			<a href="teso-buscaingresos.php" class="mgbt"> <img src="imagenes/busca.png"  title="Buscar"/></a> 
			<a onClick="mypop=window.open('plan-agenda.php','','');mypop.focus()" class="mgbt"><img src="imagenes/agenda1.png" title="Agenda" /></a>
			<a onClick="mypop=window.open('teso-principal.php','','');mypop.focus();" class="mgbt"><img src="imagenes/nv.png"  title="Nueva ventana"></a> 
			<a onClick="mypop=window.open('<?php echo $url2; ?>','','');mypop.focus();" class="tooltip bottom mgbt"><img src="imagenes/duplicar_pantalla.png" title="Duplicar pesta&ntilde;a"></a>
			<a onClick="iratras(<?php echo $scrtop; ?>, <?php echo $numpag; ?>, <?php echo $limreg; ?>, <?php echo $filtro; ?>)" class="mgbt"><img src="imagenes/iratras.png" title="Atr&aacute;s"></a>
		</td>
	</tr>		  
</table>
	<tr>
		<td colspan="3" class="tablaprin" align="center"> 
		<?php
 			$vigusu=vigencia_usuarios($_SESSION['cedulausu']); 

			if ($_GET['idr']!=""){
				echo "<script>document.getElementById('codrec').value=$_GET[idr];</script>";
			}

			$sqlr="SELECT MIN(CONVERT(codigo, SIGNED INTEGER)), MAX(CONVERT(codigo, SIGNED INTEGER)) FROM tesoingresos ORDER BY CONVERT(codigo, SIGNED INTEGER)";
			$res=mysqli_query($linkbd, $sqlr);
			$r=mysqli_fetch_row($res);
			$_POST['minimo']=$r[0];
			$_POST['maximo']=$r[1];

			if($_POST['oculto']==""){
				if ($_POST['codrec']!="" || $_GET['idr']!=""){
					if($_POST['codrec']!=""){
						$sqlr="SELECT * FROM tesoingresos WHERE codigo='$_POST[codrec]'";
					}
					else{
						$sqlr="SELECT * FROM tesoingresos WHERE codigo ='$_GET[idr]'";
					}
				}
				else{
					$sqlr="SELECT * FROM  tesoingresos ORDER BY CONVERT(codigo, SIGNED INTEGER) DESC";
				}
				$res=mysqli_query($linkbd, $sqlr);
				$row=mysqli_fetch_row($res);
			   	$_POST['codigo']=$row[0];

				$_POST['tabgroup1']=1;
			}

			switch($_POST['tabgroup1'])
			{
				case 1:
					$check1='checked';break;
				case 2:
					$check2='checked';break;
				case 3:
					$check3='checked';break;
				case 4:
					$check4='checked';break;
				case 5:
					$check5='checked';break;
			}

 			if(($_POST['oculto']!="2")&&($_POST['oculto']!="6")&&($_POST['oculto']!="7")&&($_POST['agregaFuente']!="1")){	
				
 		 		$linkbd=conectar_V7();
				$sqlr="SELECT * FROM tesoingresos WHERE tesoingresos.codigo='$_POST[codigo]' ";
 				$cont=0;
 				$resp = mysqli_query($linkbd, $sqlr);
				while ($row = mysqli_fetch_row($resp)){	 
					$_POST['codigo']=$row[0];
				 	$_POST['nombre']=$row[1]; 	
					 $_POST['tipo']=$row[2];
					if($row[4]!='')
					{
						$_POST['terceros']="1";
					}
					
					$_POST['destinoIng']=$row[4];
					$_POST['regalias'] = $row[5];
                    $_POST['contLiquidacion'] = $row[6];						
				}	 
			}
			if($_POST['oculto']=="7" && $_POST['terceros']=="on")
			{
				$_POST['terceros']=1;
			}
 			if(($_POST['oculto']!="2")&&($_POST['oculto']!="6")&&($_POST['oculto']!="7")&&($_POST['agregaFuente']!="1"))
 			{
				unset($_POST['dcuentas']);
				unset($_POST['dncuentas']);
				unset($_POST['dconceptos']);	 		 		 		 		 
				unset($_POST['dnconceptos']);	
				unset($_POST['dvalores']);
				unset($_POST['cuin']); 		 		 		 		 		 	
				unset($_POST['ncuin']);
				unset($_POST['clasificador']);
				unset($_POST['nclasificador']);
				$_POST['dcuentas']=array();	
				$_POST['dncuentas']=array();	
				$_POST['dconceptos']=array();		 		 		 		 		 
				$_POST['dnconceptos']=array();		 		 		 		 		 		 
				$_POST['dvalores']=array();
				$_POST['cuin'] = array();
				$_POST['ncuin'] = array();
				$_POST['clasificador'] = array();
				$_POST['nclasificador'] = array();							
				$_POST['concecont']="";
				$_POST['cuenta']="";
				$_POST['ncuenta']="";
				$_POST['precio']=0;
				$sqlr="SELECT precio FROM tesoingresos_precios WHERE tesoingresos_precios.ingreso='$_POST[codigo]' AND estado='S' ";
				$resp = mysqli_query($linkbd, $sqlr);	
				while ($row =mysqli_fetch_row($resp))
				{
					$_POST['precio'] = $row[0];
				}
				
				$sqlr="SELECT tesoingresos.tipo, tesoingresos.codigo, tesoingresos_det.cuentapres, tesoingresos_det.porcentaje, tesoingresos_det.concepto, tesoingresos_det.cuenta_clasificadora, tesoingresos_det.fuente  FROM tesoingresos INNER JOIN tesoingresos_det ON tesoingresos.codigo=tesoingresos_det.codigo WHERE   tesoingresos_det.modulo=4 AND tesoingresos.codigo='$_POST[codigo]'  AND vigencia = (SELECT MAX(vigencia) FROM tesoingresos_det WHERE codigo = '$_POST[codigo]') ORDER BY  tesoingresos_det.cuentapres";
				$cont=0;
				$resp = mysqli_query($linkbd, $sqlr);
				while ($row =mysqli_fetch_row($resp)){	
					$sql="SELECT tipo FROM tesoingresos_tipo WHERE cod_ingreso='$row[1]' AND concepto='$row[4]' AND cuentapres='$row[2]' AND vigencia='$vigusu' ORDER BY cuentapres";
					$res=mysqli_query($linkbd, $sql);
					$fila=mysqli_fetch_row($res);
					$_POST['tipos'][]=$fila[0];
					$sqlr1="SELECT * FROM conceptoscontables WHERE modulo='4' AND tipo='C' AND codigo ='$row[4]' ";
					$resp1 = mysqli_query($linkbd, $sqlr1);
					$row1 =mysqli_fetch_row($resp1);

					if ($row[0]=='C'){ 
						$_POST['dcuentas'][$cont]=$row[2];
						$_POST['dncuentas'][$cont]=buscaNombreCuentaCCPETIngresosSel($row[2]);
						$_POST['dvalores'][$cont]=$row[3];	
						$_POST['dnconceptos'][$cont]=$row1[0]." ".$row1[1];
						$_POST['dconceptos'][$cont]=$row1[0];
						$_POST['cuentaClasificadora'][$cont] = $row[5];
					}
			
					if($row[0]=='S')
					{
						$_POST['cuenta']=$row[2];
						$_POST['ncuenta']=buscaNombreCuentaCCPETIngresosSel($row[2]);
						$_POST['valor']=$row[3];			 
						$_POST['concecont']=$row[4];
						$clasificadorEncontrado = $row[5];

						if($row[2] != '')
						{
							$sqlr = "SELECT clasificadores FROM ccpetprogramarclasificadores WHERE cuenta = '".$row[2]."' ";
							$res = mysqli_query($linkbd, $sqlr);
							$row = mysqli_fetch_row($res);

							if($row[0] == '1')
							{
								$sqlr1 = "SELECT nombre, codigo_cuin FROM ccpet_cuin WHERE codigo_cuin = '".$clasificadorEncontrado."' ";
								$res1 = mysqli_query($linkbd, $sqlr1);
								$row1 = mysqli_fetch_row($res1);	
								$_POST['ncuin'] = $row1[0];
								$_POST['cuin']= $row1[1];
							}

							elseif($row[0] == '2')
							{
								$sqlr2 = "SELECT titulo, grupo FROM ccpetbienestransportables WHERE grupo = '".$clasificadorEncontrado."' ";
								$res2 = mysqli_query($linkbd, $sqlr2);
								$row2 = mysqli_fetch_row($res2);

								$_POST['nclasificador'] = $row2[0];
								$_POST['clasificador'] = $row2[1];
							}
							elseif($row[0] == '3')
							{
								$sqlr3 = "SELECT titulo, grupo FROM ccpetservicios WHERE grupo = '".$clasificadorEncontrado."' ";
								$res3 = mysqli_query($linkbd, $sqlr3);
								$row3 = mysqli_fetch_row($res3);

								$_POST['nclasificadorServicios'] = $row3[0];
								$_POST['clasificadorServicios'] = $row3[1];
							}
							else
							{
								
							}
							
						}
					}
				$cont=$cont+1; 
				}
				
				unset($_POST['codigoFuentes']);
				unset($_POST['nombreFuentes']);
				$_POST['codigoFuentes']=array();
				$_POST['nombreFuentes']=array();	
				$cont = 0;

				$sqlFuentes = "SELECT TF.fuente, FC.nombre FROM tesoingresos_fuentes AS TF INNER JOIN ccpet_fuentes_cuipo AS FC ON TF.fuente = FC.codigo_fuente WHERE TF.codigo = '$_POST[codigo]' AND TF.estado = 'S'";
				$resFuentes = mysqli_query($linkbd, $sqlFuentes);
				while ($rowFuentes = mysqli_fetch_row($resFuentes)) {

					$_POST['codigoFuentes'][$cont] = $rowFuentes[0];
					$_POST['nombreFuentes'][$cont] = $rowFuentes[1];

					$cont++;
				}
			}
			
			$sqln="SELECT * FROM tesoingresos WHERE codigo > '$_POST[codigo]' ORDER BY codigo ASC LIMIT 1";
			$resn=mysqli_query($linkbd, $sqln);
			$row=mysqli_fetch_row($resn);
			$next="'".$row[0]."'";
			
			$sqlp="SELECT * FROM tesoingresos WHERE codigo < '$_POST[codigo]' ORDER BY codigo DESC LIMIT 1";
			$resp=mysqli_query($linkbd, $sqlp);
			$row=mysqli_fetch_row($resp);
			$prev="'".$row[0]."'";
		?>

			<div id="bgventanamodalm" class="bgventanamodalm">
	            <div id="ventanamodalm" class="ventanamodalm">
	                <IFRAME src="" name="ventanam" marginWidth=0 marginHeight=0 frameBorder=0 id="ventanam" frameSpacing=0 style=" width:700px; height:130px; top:200; overflow:hidden;"> 
	                </IFRAME>
	            </div>
	        </div>

 			<form name="form2" method="post" action="">
 				<?php //**** busca cuenta
					if($_POST['bc']!='')
					{
						//var_dump($_POST['cuenta']);
						$nresul=buscaNombreCuentaCCPETIngresosSel($_POST['cuenta']);			

						if($nresul!='')
						{
							$_POST['ncuenta']=$nresul;
						}
						else
						{
                            echo "  <script>
                                        swal.fire({
                                            icon: 'error',
                                            title: 'Error',
                                            text: 'No esta habilitada esta cuenta: " . $_POST['cuenta'] . "'
                                        })
                                    </script>";

                            $_POST['ncuenta']="";
                            $_POST['cuenta']="";
						}
					}

					if($_POST['bc'] == '2')
					{
						$nresul = buscaNombreClasificadorCCPET($_POST['cuin'], '1');

						if($nresul!='')
						{
							$_POST['ncuin']=$nresul;
						}
						else
						{
							$_POST['ncuin']="";	
						}
					}

					if($_POST['bc'] == '3')
					{
						$nresul = buscaNombreClasificadorCCPET($_POST['clasificador'], '2');

						if($nresul!='')
						{
							$_POST['nclasificador']=$nresul;
						}
						else
						{
							$_POST['nclasificador']="";	
						}
					}

					if($_POST['bc'] == '4')
					{
						$nresul = buscaNombreClasificadorCCPET($_POST['clasificadorServicios'], '3');

						if($nresul!='')
						{
							$_POST['nclasificadorServicios']=$nresul;
						}
						else
						{
							$_POST['nclasificadorServicios']="";	
						}
					} 

					if ($_POST['bf'] == '1') {

						$sqlFuente = "SELECT codigo_fuente, nombre FROM ccpet_fuentes_cuipo WHERE codigo_fuente = '$_POST[fuente]' AND version = '1'";
						$resFuente = mysqli_query($linkbd, $sqlFuente);
						$rowFuente = mysqli_fetch_row($resFuente);

						if ($rowFuente[0] != '') {

							$_POST['nfuente'] = $rowFuente[1];
						}
						else {
							$_POST['fuente'] = '';
							$_POST['nfuente'] = '';
							echo"
								<script>
									Swal.fire({
										icon: 'error',
										title: 'Fuente no encontrada'
									})
									document.getElementById('fuente').value='';
									document.getElementById('nfuente').value='';
								</script>";
						}
					}
				?>
	
			<div class="tabs" style="height:77%; width:99.6%">
				<div class="tab">
					<input type="radio" id="tab-1" name="tabgroup1" value="1" <?php echo $check1;?>/>
					<label id="clabel" for="tab-1">Ingresos</label>
						<div class="content" style="overflow:hidden;">
							<table class="inicio ancho">
								<tr>
									<td class="titulos" colspan="12">.: Edita ingresos</td>
									<td class="cerrar" style="width:7%"><a href="teso-principal.php">Cerrar</a></td>
								</tr>

								<tr>
									<td class="textoNewR" style="width:7%">
										<label class="labelR">Código:</label>
									</td>

									<td style="width:10%">
										<a href="#" onClick="atrasc(<?php echo $scrtop; ?>, <?php echo $numpag; ?>, <?php echo $limreg; ?>, <?php echo $filtro; ?>, <?php echo $prev; ?>)">
											<img src="imagenes/back.png" alt="anterior" align="absmiddle">
										</a> 

										<input name="codigo" id="codigo" type="text" value="<?php echo $_POST['codigo']?>" maxlength="2" style="width:30%" onKeyPress="javascript:return solonumeros(event)" onKeyUp="return tabular(event,this)" readonly>       

										<a href="#" onClick="adelante(<?php echo $scrtop; ?>, <?php echo $numpag; ?>, <?php echo $limreg; ?>, <?php echo $filtro; ?>, <?php echo $next; ?>)">
											<img src="imagenes/next.png" alt="siguiente" align="absmiddle">
										</a> 

										<input type="hidden" value="<?php echo $_POST['maximo']?>" name="maximo" id="maximo">
										<input type="hidden" value="<?php echo $_POST['minimo']?>" name="minimo" id="minimo">
										<input type="hidden" value="<?php echo $_POST['codrec']?>" name="codrec" id="codrec">
									</td>

									<td class="textoNewR" style="width:10%">
										<label class="labelR">Nombre Ingreso:</label>
									</td>
									<td style="width:40%" colspan="5">
										<input name="nombre" id="nombre" type="text" style="width:100%" value="<?php echo $_POST['nombre']?>" onKeyUp="return tabular(event,this)">        
									</td>

									<td class="textoNewR">
										<label class="labelR">Precio Venta:</label>
									</td>
									<td>
										<input id="precio" name="precio" type="text" value="<?php echo $_POST['precio']?>" onKeyUp="return tabular(event,this)" size="8" onKeyPress="javascript:return solonumeros(event)" >
									</td>
								</tr> 

								<tr>
									<td class="textoNewR">
										<label class="labelR">Tipo:</label>
									</td>
									<td>
										<select name="tipo" id="tipo" onChange="validar()" >
											<option value="S" <?php if($_POST['tipo']=='S') echo "SELECTED"?>>Simple</option>
											<option value="C" <?php if($_POST['tipo']=='C') echo "SELECTED"?>>Compuesto</option>
										</select>
										<input name="oculto" id="oculto" type="hidden" value="1">		  
									</td>

									<td class="textoNewR">
										<label class="labelR">Terceros:</label>
									</td>
									<td> 
										<div class="c1">
											<input type="checkbox" id="terceros" name="terceros"  onChange="validafinalizar(this)" <?php if($_POST['terceros']!=''){echo "checked"; } ?> />	
												<label for="terceros" id="t1" ></label>
										</div>
									</td>

									<?php
										if($_POST['terceros']!="")
										{
									?>
											<td class="textoNewR" style="width:10%">
												<label class="labelR">Destino:</label>
											</td>
											<td>
												<select name="destinoIng" id="destinoIng" onChange="validar()">
													<option value="" >Seleccione...</option>        
													<option value="N" <?php if($_POST['destinoIng']=='N') echo "SELECTED"?>>Nacional</option>
													<option value="D" <?php if($_POST['destinoIng']=='D') echo "SELECTED"?>>Departamental</option>
													<option value="M" <?php if($_POST['destinoIng']=='M') echo "SELECTED"?>>Municipal</option>            
												</select>
											</td>	
											<?php 
										}
									?>

									<td class="textoNewR" style="width:10%">
										<label class="labelR">Tipo ingreso:</label>
									</td>
									<td>
										<select name="regalias" id="regalias" onChange="validar()">
											<option value="normal" <?php if($_POST['regalias']=='normal') echo "SELECTED"?>>Normal</option>
											<option value="sgr" <?php if($_POST['regalias']=='sgr') echo "SELECTED"?>>SGR</option>
											<option value="sgp" <?php if($_POST['regalias']=='sgp') echo "SELECTED"?>>SGP</option>
											<option value="salud" <?php if($_POST['regalias']=='salud') echo "SELECTED"?>>Salud ssf</option>
											<option value="educacion" <?php if($_POST['regalias']=='educacion') echo "SELECTED"?>>Educacion ssf</option>
										</select>
									</td>

									<td class="textoNewR" style="width:15%">
										<label class="labelR">Contabiliza liquidación:</label>
									</td>
									<td>
										<select name="contLiquidacion" id="contLiquidacion">
											<option value="S" <?php if($_POST['contLiquidacion']=='S') echo "SELECTED"?>>Si</option>
											<option value="N" <?php if($_POST['contLiquidacion']=='N') echo "SELECTED"?>>No</option>
										</select>
									</td>
								</tr>
							</table>

							<?php
								if($_POST['tipo']=='S') //***** SIMPLE
								{
							?>
									<table class="inicio ancho">
										<tr>
											<td colspan="6" class="titulos">Agregar Detalle Ingreso</td>
										</tr>   

										<tr>
											<td class="textoNewR" style="width:10%">
												<label class="labelR">Concepto contable:</label>
											</td>
											<td style="width:50%;">
												<select name="concecont" id="concecont" style="width:100%;">
													<option value="-1">Seleccione ....</option>
														<?php
															$sqlr="Select * from conceptoscontables where modulo='4' and tipo='C' order by codigo";
															$resp = mysqli_query($linkbd, $sqlr);
															while ($row =mysqli_fetch_row($resp))
															{
																$i=$row[0];
																echo "<option value=$row[0] ";
																if($i==$_POST['concecont'])
																{
																	echo "SELECTED";
																	$_POST['concecontnom']=$row[0]." - ".$row[3]." - ".$row[1];
																}
																echo " >".$row[0]." - ".$row[3]." - ".$row[1]."</option>";	  
															}
														?>
												</select>
												<input id="concecontnom" name="concecontnom" type="hidden" value="<?php echo $_POST['concecontnom']?>" >
											</td>
										</tr>
								
										<tr>
											<?php
												if($_POST['terceros']=="")
												{
											?>
													<td class="textoNewR" style="width:10%">
														<label class="labelR">Cuenta presupuestal:</label>
													</td>
													<td style="width:50%;" valign="middle" >
														<input type="text" id="cuenta" name="cuenta" style="width:20%" onKeyPress="javascript:return solonumeros(event)" onKeyUp="return tabular(event,this)" onBlur="buscacta(event)" value="<?php echo $_POST['cuenta']?>">
								
														<input type="hidden" value="0" name="bc" id="bc">

														<a title="Cuentas presupuestales" onClick="despliegamodal2('visible');" style="cursor:pointer;"><img src="imagenes/find02.png" style="width:20px;"/></a>
														<input type="text" name="ncuenta" id="ncuenta" style="width:33%;" value="<?php echo $_POST['ncuenta']?>"  readonly>
													</td>
											<?php
												}
											?>
										</tr>

										<?php
											$buscarClasificador = '';

											$sqlr = "SELECT clasificadores FROM ccpetprogramarclasificadores WHERE cuenta = '".$_POST['cuenta']."' ";
											
											$resp = mysqli_query($linkbd, $sqlr);
										
											$row = mysqli_fetch_row($resp);

											if($row[0] == '1')
											{
												$buscarClasificador = '1';
											}
										
											if($row[0] == '2')
											{
												$buscarClasificador = '2';	
											}
											elseif($row[0] == '3')
											{	
												$buscarClasificador = '3';
											}
										?>

										<?php
											if($buscarClasificador == '1') {
										?>
												<tr>
													<td class="textoNewR" style="width:10%">
														<label class="labelR">CUIN:</label>
													</td>
													<td style="width:50%;" valign="middle">
														<input type="text" id="cuin" name="cuin" style="width:20%" onKeyPress="javascript:return solonumeros(event)" onKeyUp="return tabular(event,this)"  value="<?php echo $_POST['cuin']?>" onBlur="buscacta2(event,1)" onClick="document.getElementById('cuin').focus();document.getElementById('cuin').select();">
														<a title="CUIN" onClick="despliegamodal5('visible');" style="cursor:pointer;"><img src="imagenes/find02.png" style="width:20px;"/></a>
														<input type="text" name="ncuin" id="ncuin" style="width:33%;" value="<?php echo $_POST['ncuin']?>"  readonly>
													</td>
												</tr>
								
										<?php
											}

											if($buscarClasificador == '2') {
										?>
												<tr>
													<td class="textoNewR" style="width:10%">
														<label class="labelR">Bienes Transportables:</label>
													</td>
													<td style="width:50%;" valign="middle">
														<input type="text" id="clasificador" name="clasificador" style="width:20%" onKeyPress="javascript:return solonumeros(event)" onKeyUp="return tabular(event,this)"  value="<?php echo $_POST['clasificador']?>" onClick="document.getElementById('clasificador').focus();document.getElementById('clasificador').select();" onBlur="buscacta2(event,2)">
														<a title="Cuentas presupuestales" onClick="despliegamodal3('visible');" style="cursor:pointer;"><img src="imagenes/find02.png" style="width:20px;"/></a>
														<input type="text" name="nclasificador" id="nclasificador" style="width:33%;" value="<?php echo $_POST['nclasificador']?>"  readonly>
													</td>
												</tr>
										<?php
											}

											if($buscarClasificador == '3') {
										?>
												<tr>
													<td class="textoNewR" style="width:10%">
														<label class="labelR">Servicios:</label>
													</td>
													<td style="width:50%;" valign="middle">
														<input type="text" id="clasificadorServicios" name="clasificadorServicios" style="width:20%" value="<?php echo $_POST['clasificadorServicios']?>" onBlur="buscacta2(event,3)">
														<a title="Clasificador Servicios" onClick="despliegamodal4('visible');" style="cursor:pointer;"><img src="imagenes/find02.png" style="width:20px;"/></a>
														<input type="text" name="nclasificadorServicios" id="nclasificadorServicios" style="width:33%;" value="<?php echo $_POST['nclasificadorServicios']?>"  readonly>
													</td>
												</tr>
										<?php
											}

											$cuentaClasificadora;

											if(isset($_POST['cuin']))
											{
												$cuentaClasificadora = $_POST['cuin'];
											}
											elseif(isset($_POST['clasificador']))
											{
												$cuentaClasificadora = $_POST['clasificador'];
											}
											elseif(isset($_POST['clasificadorServicios']))
											{
												$cuentaClasificadora = $_POST['clasificadorServicios'];
											}
											else
											{
												$cuentaClasificadora = '';
											}

											if($_POST['cuenta'] != '' && $_POST['fuente'] != '') {
												$sqlrFuente = "SELECT fuente FROM ccpetinicialing WHERE cuenta = '".$_POST['cuenta']."' "; 
												$respFuente = mysqli_query($linkbd, $sqlrFuente);
												$cantFuente = mysqli_num_rows($respFuente);
												if($cantFuente == 1){
													$rowFuente = mysqli_fetch_row($respFuente);
													$_POST['fuente'] = $rowFuente[0];	
													$_POST['nfuente'] = buscaNombreFuenteCCPET($rowFuente[0]);	
												}
											}
										?>

										<tr>		  
											<td class="textoNewR" style="width:10%">
												<label class="labelR">Porcentaje:</label>
											</td>
											<td>
												<input id="valor" name="valor" type="text" value="<?php echo $_POST['valor']?>" style="width:10%;" onKeyUp="return tabular(event,this)" onKeyPress="javascript:return solonumeros(event)" > %
											</td>
										</tr>
									</table>
							<?php
								}
								if($_POST['tipo']=='C') //**** COMPUESTO
								{
									$linkbd=conectar_v7();
							?>
									<table class="inicio ancho">
										<tr>
											<td colspan="7" class="titulos">Agregar Detalle Ingreso</td>
										</tr>      

										<tr>
											<td class="textoNewR" style="width:10%">
												<label class="labelR">Concepto contable:</label>
											</td>
											<td style="width:100%;" colspan="4">
												<select name="concecont" id="concecont" style="width:62%;">
													<option value="-1">Seleccione ....</option>
														<?php
															$sqlr="SELECT * FROM conceptoscontables WHERE modulo='4' AND tipo='C' ORDER BY codigo";
															$resp = mysqli_query($linkbd, $sqlr);
															while ($row =mysqli_fetch_row($resp)) 
															{
																$i=$row[0];
																echo "<option value=$row[0] ";
																if($i==$_POST['concecont'])
																{
																	echo "SELECTED";
																	$_POST['concecontnom']=$row[0]." - ".$row[3]." - ".$row[1];
																}
																echo " >".$row[0]." - ".$row[3]." - ".$row[1]."</option>";	  
															}   
														?>
												</select>
												<input id="concecontnom" name="concecontnom" type="hidden" value="<?php echo $_POST['concecontnom']?>" >
											</td>
										</tr>

										<tr>
											<?php
												if($_POST['terceros']=="")
												{
											?>
													<td class="textoNewR" style="width:10%">
														<label class="labelR">Cuenta Presupuestal:</label>
													</td>
													<td style="width:50%;" valign="middle" >
														<input type="text" id="cuenta" name="cuenta" style="width:20%" onKeyPress="javascript:return solonumeros(event)" onKeyUp="return tabular(event,this)" onBlur="buscacta(event)" value="<?php echo $_POST['cuenta']?>" onClick="document.getElementById('cuenta').focus();document.getElementById('cuenta').select();">
														<input type="hidden" value="0" name="bc" id="bc">
														<a title="Cuentas presupuestales" onClick="despliegamodal2('visible');" style="cursor:pointer;"><img src="imagenes/find02.png" style="width:20px;"/></a>
														<input type="text" name="ncuenta" id="ncuenta" style="width:33%;" value="<?php echo $_POST['ncuenta']?>"  readonly>
													</td>
											<?php
												}
											?>
										</tr>
										<?php
										
											$buscarClasificador = '';

											$sqlr = "SELECT clasificadores FROM ccpetprogramarclasificadores WHERE cuenta = '".$_POST['cuenta']."' ";
											
											$resp = mysqli_query($linkbd, $sqlr);
										
											$row = mysqli_fetch_row($resp);

											if($row[0] == '1')
											{
												$buscarClasificador = '1';
											}
										
											if($row[0] == '2')
											{
												$buscarClasificador = '2';	
											}
											elseif($row[0] == '3')
											{	
												$buscarClasificador = '3';
											}
										?>

										<?php
											if($buscarClasificador == '1') {
										?>

											<tr>
												<td class="textoNewR" style="width:10%">
													<label class="labelR">CUIN:</label>
												</td>
												<td style="width:50%;" valign="middle">
													<input type="text" id="cuin" name="cuin" style="width:20%" onKeyPress="javascript:return solonumeros(event)" onKeyUp="return tabular(event,this)"  value="<?php echo $_POST['cuin']?>" onBlur="buscacta2(event,1)" onClick="document.getElementById('cuin').focus();document.getElementById('cuin').select();">
													<a title="CUIN" onClick="despliegamodal5('visible');" style="cursor:pointer;"><img src="imagenes/find02.png" style="width:20px;"/></a>
													<input type="text" name="ncuin" id="ncuin" style="width:33%;" value="<?php echo $_POST['ncuin']?>"  readonly>
												</td>
											</tr>
							
										<?php
											}

											if($buscarClasificador == '2') {
										?>
												<tr>
													<td class="textoNewR" style="width:10%">
														<label class="labelR">Bienes Transportables:</label>
													</td>
													<td style="width:50%;" valign="middle">
														<input type="text" id="clasificador" name="clasificador" style="width:20%" onKeyPress="javascript:return solonumeros(event)" onKeyUp="return tabular(event,this)"  value="<?php echo $_POST['clasificador']?>" onClick="document.getElementById('clasificador').focus();document.getElementById('clasificador').select();" onBlur="buscacta2(event,2)">
														<a title="Cuentas presupuestales" onClick="despliegamodal3('visible');" style="cursor:pointer;"><img src="imagenes/find02.png" style="width:20px;"/></a>
														<input type="text" name="nclasificador" id="nclasificador" style="width:33%;" value="<?php echo $_POST['nclasificador']?>"  readonly>
													</td>
												</tr>
										<?php
											}

											if($buscarClasificador == '3') {
										?>
												<tr>
													<td class="textoNewR" style="width:10%">
														<label class="labelR">Servicios:</label>
													</td>
													<td style="width:50%;" valign="middle">
														<input type="text" id="clasificadorServicios" name="clasificadorServicios" style="width:20%" onKeyPress="javascript:return solonumeros(event)" onKeyUp="return tabular(event,this)"  value="<?php echo $_POST['clasificadorServicios']?>" onClick="document.getElementById('clasificadorServicios').focus();document.getElementById('clasificadorServicios').select();" onBlur="buscacta2(event,3)">
														<a title="Clasificador Servicios" onClick="despliegamodal4('visible');" style="cursor:pointer;"><img src="imagenes/find02.png" style="width:20px;"/></a>
														<input type="text" name="nclasificadorServicios" id="nclasificadorServicios" style="width:33%;" value="<?php echo $_POST['nclasificadorServicios']?>"  readonly>
													</td>
												</tr>
										<?php
											}

											//Mirar cual tiene datos 
											$cuentaClasificadora;
											if(isset($_POST['cuin']))
											{
												$cuentaClasificadora = $_POST['cuin'];
											}
											elseif(isset($_POST['clasificador']))
											{
												$cuentaClasificadora = $_POST['clasificador'];
											}
											elseif(isset($_POST['clasificadorServicios']))
											{
												$cuentaClasificadora = $_POST['clasificadorServicios'];
											}
											else
											{
												$cuentaClasificadora = '';
											}
										?>
										<tr>
											<td class="textoNewR" style="width:10%">
												<label class="labelR">Porcentaje:</label>
											</td>
											<td style="width:20%;">
												<input id="valor" name="valor" type="text" value="<?php echo $_POST['valor']?>" style="width:20%;" onKeyUp="return tabular(event,this)" onKeyPress="javascript:return solonumeros(event)" >% 
												<input type="button" name="agregar" id="agregar" value="   Agregar   " onClick="agregardetalle()" style="margin-left: 4.5%">
												<input type="hidden" value="0" name="agregadet"><input type="hidden" value="0" name="buscap">
												<input type='hidden' name='numeroClasificador' id='numeroClasificador' >	
											</td>
										</tr> 
									</table>
				
									<?php
										//**** busca cuenta
										if($_POST['bc']!='')
										{
											$nresul=buscaNombreCuentaCCPETIngresosSel($_POST['cuenta']);
											if($nresul != '')
											{
												$_POST['ncuenta'] = $nresul;
									?>
									<script>
										document.getElementById('agregar').focus();
										document.getElementById('agregar').select();
									</script>
									<?php
											}
											else
											{
												
                                                echo "  <script>
                                                            swal.fire({
                                                                icon: 'error',
                                                                title: 'Error',
                                                                text: 'No esta habilitada esta cuenta: " . $_POST['cuenta'] . "'
                                                            })
                                                        </script>";
                                                $_POST['ncuenta'] = "";
                                                $_POST['cuenta']="";
											}
										}
									?>

									<div class="subpantalla" style="height:48%; width:99.5%;" id="divdet">
										<table class="inicio" style="height:300px;">
											<tr>
												<td class="titulos" colspan="7">Detalle Ingreso</td>
											</tr>
											<tr>

												<td class="titulos2" style="width:8%">Cuenta</td>
												<td class="titulos2" style="width:25%">Nombre Cuenta</td>
												<td class="titulos2" style="width:30%">Concepto</td>
												<td class="titulos2" style="width:20%">Cuenta Clasificadora</td>
												<td class="titulos2" style="width:6%">Porcentaje</td>
												<td class="titulos2" style="width:5%"><center>
													<img src="imagenes/del.png" >
													<input type='hidden' name='elimina' id='elimina'></center>
													<input type='hidden' name='posicion' id='posicion'  >
													<input type='hidden' name='valorpos' id='valorpos'  >
													
													
												</td>
												
											</tr>
											<?php
												if($_POST['posicion']!=''){
												$pos=$_POST['posicion'];
												$val=$_POST['valorpos'];
												
												
											}
												if ($_POST['elimina']!='')
												{ 
													$posi=$_POST['elimina'];
													unset($_POST['dcuentas'][$posi]);
													unset($_POST['dncuentas'][$posi]);
													unset($_POST['dconceptos'][$posi]);	 		 		 		 		 
													unset($_POST['dnconceptos'][$posi]);	 		 		 		 		 		 
													unset($_POST['dvalores'][$posi]);
													unset($_POST['cuentaClasificadora'][$posi]); 		 		 		 		 		 	
													
													$_POST['dcuentas'] = array_values($_POST['dcuentas']); 
													$_POST['dncuentas'] = array_values($_POST['dncuentas']); 		 		 
													$_POST['dconceptos'] = array_values($_POST['dconceptos']); 
													$_POST['dnconceptos'] = array_values($_POST['dnconceptos']); 		 
													$_POST['dvalores'] = array_values($_POST['dvalores']);
													$_POST['cuentaClasificadora'] = array_values($_POST['cuentaClasificadora']);
												}
										
												if ($_POST['agregadet']=='1')
												{
													$cuentacred=0;
													$cuentadeb=0;
													$diferencia=0;
													$_POST['dcuentas'][]=$_POST['cuenta'];
													$_POST['dncuentas'][]=$_POST['ncuenta'];
													$_POST['cuentaClasificadora'][] = $cuentaClasificadora;
													$_POST['dconceptos'][]=$_POST['concecont'];		 
													$_POST['dnconceptos'][]=$_POST['concecontnom'];		 		 
													$_POST['dvalores'][]=$_POST['valor'];	

													$_POST['dvalores'][]=$_POST['valor'];	
													$_POST['agregadet']=0;
											?>
											<script>
												document.form2.concecont.select();
											</script>
											
											
											<?php
												}
												if($_POST['buscap']=='1')
												{
													for($x=0;$x<count($_POST['dcuentas']);$x++)
													{
														$sqlr = "SELECT nombre FROM cuentasingresosccpetseleccionadas WHERE codigo='".$_POST['dcuentas'][$x]."'";
														$res = mysqli_query($linkbd, $sqlr);
														$row = mysqli_fetch_row($res);
                                                        if(!empty($row[0])){
                                                            $_POST['dncuentas'][$x]=$row[0];
                                                        }else{
                                                            $_POST['dncuentas'][$x]="";
                                                            echo "  <script>
                                                                        swal.fire({
                                                                            icon: 'error',
                                                                            title: 'Error',
                                                                            text: 'No esta habilitada esta cuenta: " . $_POST['dcuentas'][$x] . "'
                                                                        })
                                                                    </script>";
                                                        }
													}
												}
												for ($x=0;$x<count($_POST['dcuentas']);$x++)
												{	
													$sqlr = "SELECT clasificadores FROM ccpetprogramarclasificadores WHERE cuenta = '".$_POST['dcuentas'][$x]."'";
													$res = mysqli_query($linkbd, $sqlr);
													$row = mysqli_fetch_row($res);
													
													$numeroclasificador2;

													if($row[0] == '1')
													{
														$numeroclasificador2 = '1';
													}
													if($row[0] == '2')
													{
														$numeroclasificador2 = '2';
													}
													if($row[0] == '3')
													{
														$numeroclasificador2 = '3';
													}
													if(empty($row[0]))
													{
														$numeroclasificador2 = '0';
														
													}

													echo 
														"<tr>

															

															<td class='saludo2'>
																<input name='dcuentas[]' id='dcuentas[]' value='".$_POST['dcuentas'][$x]."' type='text' style='width:100%' onDblClick='despliegamodal6(\"visible\",$x)'>
															</td>

															<td class='saludo2' style='width:10%'>
																<input name='dncuentas[]' value='".$_POST['dncuentas'][$x]."' type='text'style='width:100%' readonly>
															</td>

															<td class='saludo2'>
																<input name='dnconceptos[]' value='".$_POST['dnconceptos'][$x]."' type='text' style='width:100%' onDblClick='llamarventanadeb(this,$x)' readonly>
																<input name='dconceptos[]' value='".$_POST['dconceptos'][$x]."' type='hidden'>
															</td>
												
															<td class='saludo2' style='width:10%'>
																<input name='cuentaClasificadora[]' value='".$_POST['cuentaClasificadora'][$x]."' type='text' style='width:100%' onDblClick='despliegamodalClasificadores(\"visible\",$x,$numeroclasificador2)' readonly>
															</td>

															<td class='saludo2'>
																<input name='dvalores[]' value='".$_POST['dvalores'][$x]."' type='text' style='width:70%' onDblClick='llamarventanacred(this,$x)' readonly>%
															</td>

															<td class='saludo2'>
																<center>
																	<a href='#' onclick='eliminar($x)'>
																		<img src='imagenes/del.png'>
																	</a>
																</center>
															</td>

														</tr>";
												}	 
											?>
											<tr></tr>
										</table>	
									</div>
							<?php
								}
							?>
						</div>
				</div>

				<div class="tab">
					<input type="radio" id="tab-2" name="tabgroup1" value="2" <?php echo $check2;?>/>
					<label id="clabel" for="tab-2">Fuentes</label>
					<div class="content" style="overflow:hidden;">
						<table class="inicio ancho">
							<tr>
								<td class="titulos" colspan="12">.: Agregar Fuentes</td>
								<td class="cerrar" style="width:7%"><a href="teso-principal.php">Cerrar</a></td>
							</tr>
							
							<tr>
								<td class="textoNewR" style="width:10%">
									<label class="labelR">Fuente:</label>
								</td>
								<td style="width: 15%;">
									<input type="text" id="fuente" name="fuente" style="width:80%" onKeyPress="javascript:return solonumeros(event)" onKeyUp="return tabular(event,this)" onBlur="buscarFuente(event)" value="<?php echo $_POST['fuente']?>" autocomplete="off">

									<input type="hidden" name="bf" id="bf" value="0">
									
									<a title="Fuentes presupuestales" onClick="despliegamodal7('visible');" style="cursor:pointer;"><img src="imagenes/find02.png" style="width:20px;"/></a>
								</td>

								<td colspan="4">
									<input type="text" name="nfuente" id="nfuente" style="width:100%;" value="<?php echo $_POST['nfuente']?>"  readonly>
									<input type="hidden" name="agregaFuente" id="agregaFuente" value="0">
								</td>

								<td>
									<em class="botonflechaverde" onclick="agregaFuente()">Agregar fuente</em>
								</td>
							</tr>
						</table>

						<table class="inicio ancho">
							<tr>
								<td class="titulos" colspan="6">Fuentes: </td>
							</tr>
							<tr>
								<td class="titulos2" style="text-align: center; width: 20%;">Código fuente</td>
								<td class="titulos2" style="text-align: center;">Nombre fuente</td>	
								<td class="titulos2" style="text-align: center;">
									Eliminar
									<input type='hidden' name='eliminaFuente' id='eliminaFuente'>
								</td>
							</tr>

							<?php 
								if ($_POST['agregaFuente'] == 1) {
			
									$validacion = 0;
						
									for ($i=0; $i < count($_POST['codigoFuentes']); $i++) { 
										
										if ($_POST['codigoFuentes'][$i] == $_POST['fuente']) {
											$validacion++;
										}			
									}
						
									if ($validacion == 0) {
										$_POST['codigoFuentes'][]=$_POST['fuente'];
										$_POST['nombreFuentes'][]=$_POST['nfuente'];
										$_POST['fuente'] = "";
										$_POST['nfuente'] = "";
										echo"
											<script>
												document.getElementById('fuente').value='';
												document.getElementById('nfuente').value='';
											</script>";
									}
									else {
										echo "
											<script>
												Swal.fire({
													icon: 'error',
													title: 'Código repetido'
												})
											</script>
										";
									}
								}

								if ($_POST['eliminaFuente'] != '') {

									$posi=$_POST['eliminaFuente'];
									unset($_POST['codigoFuentes'][$posi]);
									unset($_POST['nombreFuentes'][$posi]);
									$_POST['codigoFuentes']= array_values($_POST['codigoFuentes']); 
									$_POST['nombreFuentes']= array_values($_POST['nombreFuentes']); 
								}

								for ($x=0;$x< count($_POST['codigoFuentes']);$x++)
								{
									echo "
									<tr>
										<td class='saludo2'>
											<input name='codigoFuentes[]' value='".$_POST['codigoFuentes'][$x]."' type='text' readonly style='width: 100%; text-align: center;'>
										</td>
										<td class='saludo2'>
											<input name='nombreFuentes[]' value='".$_POST['nombreFuentes'][$x]."' type='text' readonly style='width: 100%;'>
										</td>
										<td class='saludo2' style='text-align: center;'>
											<a onclick='eliminarFuente($x)'>
											<img src='imagenes/del.png'></a>
										</td>
									</tr>";
								}
							?>
						</table>
					</div>
				</div>
			</div>
							
		
		<div id="bgventanamodal2">
			<div id="ventanamodal2">
				<IFRAME  src="" name="buster" marginWidth=0 marginHeight=0 frameBorder=0 id="ventana2" frameSpacing=0 style="left:500px; width:900px; height:500px; top:200;"> 
				</IFRAME>
			</div>
       	</div>

    </form>
  			<?php
				$oculto=$_POST['oculto'];

				if($_POST['oculto']=='2')
				{
					if($_POST['terceros']!="")
					{
						$sqlr = "UPDATE tesoingresos SET nombre = '$_POST[nombre]', tipo = '$_POST[tipo]', terceros = '$_POST[destinoIng]', tipoIngreso = '$_POST[regalias]', gravado = '$_POST[contLiquidacion]' WHERE codigo = '$_POST[codigo]'";
					}
					else{
						$sqlr="INSERT INTO tesoingresos (codigo,nombre,tipo,estado,terceros,tipoIngreso,gravado)VALUES ('$_POST[codigo]', '$_POST[nombre]', '$_POST[tipo]', 'S', '', '$_POST[regalias]', '$_POST[contLiquidacion]')";

						$sqlr = "UPDATE tesoingresos SET nombre = '$_POST[nombre]', tipo = '$_POST[tipo]', terceros = '', tipoIngreso = '$_POST[regalias]', gravado = '$_POST[contLiquidacion]' WHERE codigo = '$_POST[codigo]'";
					}
					
					if (!mysqli_query($linkbd, $sqlr))
					{
						echo "
							<script>
								Swal.fire({
									icon: 'error',
									title: 'Error en guardado de cabecera'
								})
							";
					}
					else
					{
						$fecha=date('Y-m-d h:i:s');
						$sqlr="UPDATE tesoingresos_precios SET estado='N' WHERE ingreso='$_POST[codigo]'";
						mysqli_query($linkbd, $sqlr);

						$sqlr="INSERT INTO tesoingresos_precios (ingreso,precio,fecha,estado) VALUES ('$_POST[codigo]','$_POST[precio]','$fecha','S')";
						mysqli_query($linkbd, $sqlr);

						if($_POST['tipo']=='S') {

							$sql="DELETE FROM tesoingresos_tipo WHERE cod_ingreso='$_POST[codigo]' AND vigencia='$vigusu'";
							mysqli_query($linkbd, $sql);

							$sqlr="DELETE FROM tesoingresos_det WHERE codigo ='$_POST[codigo]'";		
							mysqli_query($linkbd, $sqlr);

							$_POST['valor']=100;

							$sqlr="INSERT INTO tesoingresos_det (codigo,concepto,modulo,tipoconce,porcentaje,cuentapres,cuenta_clasificadora,estado)VALUES ('$_POST[codigo]','".$_POST['concecont']."','4', 'S', '".$_POST['valor']."', '".$_POST['cuenta']."','$cuentaClasificadora','S')";
							
							if (!mysqli_query($linkbd, $sqlr))
							{
								echo "
										<script>
											Swal.fire({
												icon: 'error',
												title: 'Error en guardado de detalle'
											})
										</script>
									";
							}
							else
							{
								$sql = "UPDATE tesoingresos_fuentes SET estado='N' WHERE codigo = '$_POST[codigo]'";
								mysqli_query($linkbd, $sql);

								for ($x=0;$x< count($_POST['codigoFuentes']);$x++) {

									$sqlr = "INSERT INTO tesoingresos_fuentes (codigo, fuente, estado) VALUES ('$_POST[codigo]', '".$_POST['codigoFuentes'][$x]."', 'S')";
									mysqli_query($linkbd, $sqlr);
								}	
								
								echo "
									<script>
										despliegamodalm('visible','1','Se ha actualizado con exito');
									</script>";
							}
						}
						
						//Tipo compuesto
						if($_POST['tipo']=='C') {

							$sql="DELETE FROM tesoingresos_tipo WHERE cod_ingreso = '$_POST[codigo]' AND vigencia = '$vigusu'";
							mysqli_query($linkbd, $sql);
							
							$sqlr="DELETE FROM tesoingresos_det WHERE tipoconce='C' AND codigo = '$_POST[codigo]' AND vigencia = '$vigusu' ";
						
							if (!mysqli_query($linkbd, $sqlr))
							{
								echo "
									<script>
										Swal.fire({
											icon: 'error',
											title: 'Error en guardado'
										})
									</script>
								";
							}
							else
							{
								for($x=0;$x<count($_POST['dcuentas']);$x++)
								{
									
									$sqlr="INSERT INTO tesoingresos_det (codigo,concepto,modulo,tipoconce,porcentaje,cuentapres,estado,vigencia, cuenta_clasificadora)VALUES ('$_POST[codigo]','".$_POST['dconceptos'][$x]."','4', 'C', '".$_POST['dvalores'][$x]."', '".$_POST['dcuentas'][$x]."','S', '$vigusu', '".$_POST['cuentaClasificadora'][$x]."' )";
									echo $sqlr;
									if (!mysqli_query($linkbd, $sqlr))
									{
										echo "
											<script>
												Swal.fire({
													icon: 'error',
													title: 'Error en guardado de detalle'
												})
											</script>
										";
									}	
										
									
								}

								$sql = "UPDATE tesoingresos_fuentes SET estado='N' WHERE codigo = '$_POST[codigo]'";
								mysqli_query($linkbd, $sql);

								for ($x=0;$x< count($_POST['codigoFuentes']);$x++) {

									$sqlr = "INSERT INTO tesoingresos_fuentes (codigo, fuente, estado) VALUES ('$_POST[codigo]', '".$_POST['codigoFuentes'][$x]."', 'S')";
									mysqli_query($linkbd, $sqlr);
								}	
								
								echo "
									<script>
											despliegamodalm('visible','1','Se ha actualizado con exito');
									</script>";
							}
						}
					}
					
				}
			?> 
		</td>
	</tr>
     
</table>
</body>
</html>