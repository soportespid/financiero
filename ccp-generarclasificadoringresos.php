<?php
    header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=utf8");

	require 'comun.inc';
	require 'funciones.inc';

	session_start();
    date_default_timezone_set("America/Bogota");

?>
<!DOCTYPE >
<html lang="es">
	<head>
		<meta charset="utf-8"/>
		<meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1"/>
		<meta name="viewport" content="user-scalable=no">
		<title>:: Ideal - Presupuesto</title>
		<link href="favicon.ico" rel="shortcut icon"/>

		<link href="css/css2.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<link href="css/css2z.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
        <link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
        <link href="css/css4.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
        <script type="text/javascript" src="css/programas.js"></script>
        <script type="text/javascript" src="css/calendario.js"></script>
        <script type="text/javascript" src="JQuery/jquery-2.1.4.min.js"></script>

		<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
        <link href="css/style.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css"/>
		<style>
			.inicio--no-shadow{
				box-shadow: none;
			}

			[v-cloak]{
				display : none;
			}

			.seleccionarTodos {
				padding: 4px 0px 4px 10px;
				background: linear-gradient(#337CCF, #1450A3);
				border: 1px solid;
				border-top-color: #0A6EBD;
				border-right-color: #0A6EBD;
				border-bottom-color: #337CCF;
				border-left-color: #337CCF;
				box-shadow: 4px 4px 2px #368eb880;
				border-radius: 5px;
				display: flex;
				align-items: center;
				flex-direction: row;
				text-align: center;
				gap: 5px;
				color: #fff;
				cursor: pointer;
				font-size: 0.8rem;
			}

			.span-check{
				margin-right: 0.5em;
				text-align: center;
				width: 1.5rem;
				height: 1.5rem;
				background-color: rgb(12, 176, 252);
				border: 2px solid;
				border-color: rgb(46, 136, 238);
				border-radius: 5px;
				cursor: pointer;
			}

			.span-check-completed {
				margin-right: 0.5em;
				text-align: center;
				width: 1.5rem;
				height: 1.5rem;
				background-color: rgb(12, 176, 252);
				border: 2px solid;
				border-color: rgb(46, 136, 238);
				border-radius: 5px;
				cursor: pointer;
				background-image: url("data:image/svg+xml,%3csvg viewBox='0 0 16 16' fill='white' xmlns='http://www.w3.org/2000/svg'%3e%3cpath d='M12.207 4.793a1 1 0 010 1.414l-5 5a1 1 0 01-1.414 0l-2-2a1 1 0 011.414-1.414L6.5 9.086l4.293-4.293a1 1 0 011.414 0z'/%3e%3c/svg%3e");
				background-size:auto;
			}
		</style>
    </head>
    <body>
        <IFRAME src="alertas.php" name="alertas" id="alertas" style="display:none"></IFRAME>
        <span id="todastablas2"></span>
        <table>
            <tr><script>barra_imagenes("ccpet");</script><?php cuadro_titulos();?></tr>
            <tr><?php menu_desplegable("ccpet");?></tr>
        	<tr>
          		<td colspan="3" class="cinta">
					<a><img src="imagenes/add.png" title="Nuevo" onClick="location.href='ccp-generarclasificadoringresos.php'" class="mgbt"/></a>
					<a class="mgbt"><img src="imagenes/guardad.png"/></a>
					<a><img src="imagenes/busca.png" title="Buscar"  onClick="location.href='ccp-buscaclasificadoresingresos.php'" class="mgbt"/></a>
					<a href="#" onClick="mypop=window.open('ccp-principal.php','','');mypop.focus();" class="mgbt"><img src="imagenes/nv.png" title="Nueva Ventana"></a>
					<img src="imagenes/iratras.png" title="Atr&aacute;s" onClick="location.href='ccp-visualizarclasificadorpresupuestal.php'" class="mgbt"/>
				</td>
        	</tr>
		</table>
		<div class="subpantalla" style="height:78%; width:99.6%; overflow:hidden; resize: vertical;">
			<div id="myapp" v-cloak>
				<div class="row">
					<div class="col-12">
						<h4 style="padding-left:50px; padding-top:5px; padding-bottom:5px; background-color: #0FB0D4">Crear clasificadores de ingresos:</h4>
					</div>
				</div>
				<div class="row" style="height:100%;">
					<div class="col-7">
						<div class="row" style="margin:20px 50px 0; border-radius: 2px; background-color: #E1E2E2; ">
							<div class="col" style="padding: 12px 0px 0px 30px; font: 140% sans-serif;">
								<label for="">Buscar Rubro. <span style="font-size: 12px; font-weight: bold;"><i>(filtre por cuenta ccpet, fuente, nombre cuenta ccpet o nombre fuente.)</i></span></label>
							</div>
						</div>
						<div class="row" style="margin:0px 50px 0; border-radius: 2px; background-color: #E1E2E2;">
							<div class="col-md-10" style="padding: 4px">
								<input v-on:keyup="buscarRubro" v-model="buscar_rubro" type="text" class="form-control" style="height: auto; border-radius:4px;" placeholder="Buscar Rubro.">
							</div>
							<div class="col-md-2" style="padding: 4px">
								<select v-model="vigencia" style="width:100%" v-on:Change="cargarParametros" class="form-control" style = "font-size: 10px; margin-top:4px">
									<option v-for="year in years" :value="year[0]">{{ year[0] }}</option>
								</select>
								<!-- <button v-on:click="buscarRubro" type="submit" class="col btn btn-primary" value="Buscar"  style="height: auto; border-radius:5px;">Buscar</button> -->
							</div>
						</div>

						<div style="margin: 10px 50px 0">
							<table>
								<thead>
									<tr>
										<td class='titulos' width="30%"  style="font: 160% sans-serif; border-radius: 5px 0px 0px 0px;">Rubro</td>
										<td class='titulos'  style="font: 160% sans-serif;">Nombre</td>
										<td v-show="mostrarCheck" width = "20%" v-on:click = "selTodo">
											<div class = "seleccionarTodos" >
												Seleccionar todos <span v-bind:class="checkTodo ? 'span-check-completed' : 'span-check'"> </span>
											</div>
										</td>
									</tr>
								</thead>
							</table>
						</div>
						<div style="margin: 0px 50px 20px; border-radius: 0 0 0 5px; height: 62%; overflow: scroll; overflow-x: hidden; background: white; ">
							<table class='inicio inicio--no-shadow'>
								<tbody v-if="show_resultados">

									<tr v-for="(result, index) in results" v-on:click="seleccionaCodigos(result)" :style="estaEnArray(result) == true ? myStyle : ''" v-bind:class="index % 2 ? 'contenidonew00' : 'contenidonew01'" style="cursor: pointer;">
										<td width="30%" style="font: 160% sans-serif; padding: 5px;">{{ result[0] }}</td>
										<td style="font: 160% sans-serif;">{{ result[1] }}</td>

									</tr>
								</tbody>
								<tbody v-else>
									<tr>
										<td width="20%"style="font: 120% sans-serif; padding-left:10px; text-align:center;" colspan="3">Sin resultados</td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
					<div class="col-5">
						<div class="row" style="margin:20px 50px 0 0; border-radius: 2px; background-color: #E1E2E2; ">
							<div class="col" style="padding: 12px 0px 0px 30px; font: 140% sans-serif;">
								<label for="">Crear clasificador</label>
							</div>
						</div>
						<div class="row" style="margin:0 50px 0 0; border-radius: 2px; background-color: #E1E2E2;">
							<div class="col-md-10" style="padding: 4px">
								<input v-on:keyup.enter="addClasificador" v-model="name_clasificador" type="text" class="form-control" style="height: auto; border-radius:4px;" placeholder="Nombre de clasificador">
							</div>
							<div class="col-md-2" style="padding: 4px">
								<button v-on:click="addClasificador" type="submit" class="col btn btn-success" value="Buscar"  style="height: auto; border-radius:5px;">Guardar</button>
							</div>
						</div>

						<div style="margin: 0 50px 0 0">
							<table>
								<thead>
									<tr>
										<td colspan="2" class='titulos' style="font: 130% sans-serif; font-weight: bold; text-align: center;">Rubros seleccionados</td>
									</tr>
									<tr>
										<td class='titulos' width="30%"  style="font: 160% sans-serif; border-radius: 5px 0px 0px 0px;">Rubro</td>
										<td class='titulos'  style="font: 160% sans-serif;">Nombre </td>
									</tr>
								</thead>
							</table>
						</div>
						<div style="margin: 0 50px 0 0; border-radius: 0 0 0 5px; height: 62%; overflow: scroll; overflow-x: hidden; background: white; ">
							<table class='inicio inicio--no-shadow'>
								<tbody v-if="show_resultados_seleccionados">

									<tr v-for="(result, index) in results_seleccionadosOrd" v-on:click="seleccionaCodigos(result)" v-bind:class="index % 2 ? 'contenidonew00' : 'contenidonew01'" style="cursor: pointer; font: 100% sans-serif;">
										<td width="30%" :style="result[2] == 'A' ? myStyleMayor : ''" style=" padding: 5px;">{{ result[0] }}</td>
										<td :style="result[2] == 'A' ? myStyleMayor : ''">{{ result[1] }}</td>
									</tr>
								</tbody>
								<tbody v-else>
									<tr>
										<td width="20%"style="font: 120% sans-serif; padding-left:10px; text-align:center;" colspan="3">Seleccione los rubros para crear clasificador</td>
									</tr>
								</tbody>
							</table>
						</div>

					</div>
				</div>

                <div id="cargando" v-if="loading" class="loading">
                    <span>Cargando...</span>
                </div>

			</div>
		</div>

        <script type="module" src="./presupuesto_ccpet/clasificadores/ccp-generarclasificadoringresos.js"></script>
        <script src="Librerias/vue/axios.min.js"></script>
        <script src="sweetalert2/dist/sweetalert2.min.js"></script>
        <link rel="stylesheet" href="sweetalert2/dist/sweetalert2.min.css">
        <script src="https://unpkg.com/vue@3/dist/vue.global.js"></script>
	</body>
</html>
