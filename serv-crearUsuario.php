<?php
	header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=utf8");
	require 'comun.inc';
	require 'funciones.inc';
	session_start();
	$linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");
	cargarcodigopag($_GET['codpag'],$_SESSION["nivel"]);
	date_default_timezone_set("America/Bogota");
	titlepag();
?>
<!DOCTYPE >
<html lang=es>
	<head>
		<meta charset="utf-8"/>
		<meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1"/>
		<meta name="viewport" content="user-scalable=no">
		<title>:: IDEAL 10 - Servicios Públicos</title>
		<link href="css/css2z.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
		<link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
		<link href="css/css4.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
		<link href="css/tabs.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css"/>
		<script src="vue/vue.min.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script src="css/programas.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script src="css/calendario.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<style>
			input[type=number]::-webkit-inner-spin-button,
			input[type=number]::-webkit-outer-spin-button { 
				-webkit-appearance: none; 
				margin: 0; 
			}
			input[type=number]{
				text-align:right;
				box-sizing: border-box;
				font: 120% sans-serif;
				width: 100% !important;
			}
			[v-cloak]{display : none;}

			.centrarSelect {

				height: 30px !important;
				text-align-last:center !important;
			}

			.aumentarTamaño {

				font-size:15px;
			}

            .tamano01 {
                text-align: center !important;
            }
		</style>
		<script src="sweetalert2/dist/sweetalert2.min.js"></script>
		<link rel="stylesheet" href="sweetalert2/dist/sweetalert2.min.css">
	</head>
	<body>
		<header>
			<table>
				<tr><script>barra_imagenes("serv");</script><?php cuadro_titulos();?></tr>
			</table>
		</header>

		<form name="form2" method="post" action="">
			<section id="myapp" v-cloak >
				<nav>
					<table>
						<tr><?php menu_desplegable("serv");?></tr>
						<tr>
							<td colspan="3" class="cinta">
								<img src="imagenes/add.png" v-on:click="location.href='serv-crearUsuario.php'" class="mgbt" title="Nuevo">
								<img src="imagenes/guarda.png" v-on:click="guardar" title="Guardar"  class="mgbt">
								<img src="imagenes/busca.png" v-on:click="location.href='serv-buscaUsuarios.php'" class="mgbt" class="mgbt" title="Buscar">
								<img src="imagenes/nv.png" onClick="mypop=window.open('serv-principal.php','',''); mypop.focus();" class="mgbt" title="Nueva Ventana">
							</td>
						</tr>
					</table>
				</nav>
				<article>
					<table class="inicio ancho">
						<tr>
							<td class="titulos" colspan="6">.: Crear usuario</td>
							<td class="cerrar" style="width:7%" onClick="location.href='serv-principal.php'">Cerrar</td>
						</tr>

						<tr>
                            <td class="textoNewR">
                                <label class="labelR">Código de usuario:</label>
                            </td>

                            <td>
                                <input type="text" v-model="codUsuario" @change="validaCodUsuario" style="text-align: center; width: 80%">
                            </td>

                            <td class="textoNewR">
                                <label class="labelR">Código catastral:</label>
                            </td>
                            <td>
                                <input type="text" v-model="codCatastral">
                            </td>

                            <td class="textoNewR">
                                <label class="labelR">Fecha de creación:</label>
                            </td>

                            <td>
                                <input type="text" name="fecha" id="fecha" onchange="" maxlength="10" onKeyUp="return tabular(event,this)" id="fecha" onKeyDown="mascara(this,'/',patron,true)" title="DD/MM/YYYY" style="height:30px;text-align:center;" ondblclick="displayCalendarFor('fecha');" class="colordobleclik" autocomplete="off">
                            </td>
						</tr>

                        <tr>
                            <td class="textoNewR">
                                <label class="labelR">Suscriptor:</label>
                            </td>
                            
                            <td>
                                <input type="text" v-model="cedula" v-on:dblclick="despliegaSuscriptores" placeholder="Cedula suscriptor" style="width: 80%" class="colordobleclik" readonly>
                            </td>

                            <td colspan="10">
                                <input type="text" v-model="nombre" placeholder="Nombre suscriptor" style="width: 90%;" readonly>
                            </td>
                        </tr>

                        <tr>
                            <td class="textoNewR">
                                <label class="labelR">Barrio:</label>
                            </td>
                            
                            <td>
                                <input type="text" v-model="nombreBarrio" v-on:dblclick="despliegaBarrios" style="width: 80%" class="colordobleclik" placeholder="Nombre barrio" readonly>
                            </td>

                            <td class="textoNewR">
                                <label class="labelR">Estrato:</label>
                            </td>

                            <td>
                                <input type="text" v-model="estrato" v-on:dblclick="despliegaEstratos" class="colordobleclik" placeholder="Nombre estrato" readonly>
                            </td>
                        </tr>

                        <tr>
                            <td class="textoNewR">
                                <label class="labelR">Ruta:</label>
                            </td>

                            <td>
                                <input type="text" v-model="ruta" v-on:dblclick="despliegaRutas" class="colordobleclik" style="width: 80%">
                            </td>

                            <td class="textoNewR">
                                <label class="labelR">Orden de ruta:</label>
                            </td>

                            <td>
                                <input type="text" v-model="ordenRuta">
                            </td>
                        </tr>

						<tr>
							<td class="textoNewR">
								<label class="labelR">Dirección:</label>
							</td>

							<td colspan="6">
								<input type="text" v-model="direccion" style="width: 92%;">
							</td>
						</tr>
					</table>

					<div id="cargando" v-if="loading" class="loading">
						<span>Cargando...</span>
					</div>

                    <div v-show="showTerceros">
						<transition name="modal">
							<div class="modal-mask">
								<div class="modal-wrapper">
									<div class="modal-container">
										<table class="inicio ancho">
											<tr>
												<td class="titulos" colspan="2" >Suscriptores</td>
												<td class="cerrar" style="width:7%" @click="showTerceros = false">Cerrar</td>
											</tr>
											<tr>
												<td class="tamano01" style="width:3cm">Suscriptor:</td>
												<td>
													<input type="text" v-model="searchTercero.keywordTercero" v-on:keyup="searchMonitorTercero" placeholder="Buscar por cedula o nombre" style="width:100%" />
												</td>
											</tr>
										</table>
										<table class='tablamv'>
											<thead>
												<tr style="text-align:center;">
													<th class="titulosnew02" >Resultados Busqueda</th>
												</tr>
												<tr style="text-align:center;">
													<th class="titulosnew00" style="width:25%;">Cedula</th>
													<th class="titulosnew00">Nombre</th> 
												</tr>
											</thead>
											<tbody>
												<tr v-for="(suscriptor,index) in suscriptores" v-on:click="seleccionaTercero(suscriptor)" v-bind:class="index % 2 ? 'saludo1a' : 'saludo2'" style='text-rendering: optimizeLegibility; cursor: pointer !important;'>
                                                    <td style="font: 120% sans-serif; padding-left:10px; width:25%; text-align:left;">{{ suscriptor[1] }}</td>
                                                    <td style="font: 120% sans-serif; padding-left:10px; text-align:left; text-transform:uppercase;">{{ suscriptor[2] }}</td>
												</tr>
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</transition>
					</div>

					<div v-show="showBarrios">
						<transition name="modal">
							<div class="modal-mask">
								<div class="modal-wrapper">
									<div class="modal-container">
										<table class="inicio ancho">
											<tr>
												<td class="titulos" colspan="2" >Barrios</td>
												<td class="cerrar" style="width:7%" @click="showBarrios = false">Cerrar</td>
											</tr>
											<tr>
												<td class="tamano01" style="width:3cm">Barrio:</td>
												<td>
													<input type="text" v-model="searchBarrio.keywordBarrio" v-on:keyup="searchMonitorBarrio" placeholder="Buscar por nombre o código" style="width:100%" />
												</td>
											</tr>
										</table>
										<table class='tablamv'>
											<thead>
												<tr style="text-align:center;">
													<th class="titulosnew02" >Resultados Busqueda</th>
												</tr>
												<tr style="text-align:center;">
													<th class="titulosnew00" style="width: 15%;">Código barrio</th>
													<th class="titulosnew00">Nombre barrio</th>
												</tr>
											</thead>
											<tbody>
												<tr v-for="(barrio,index) in barrios" v-on:click="seleccionaBarrio(barrio)" v-bind:class="index % 2 ? 'saludo1a' : 'saludo2'" style='text-rendering: optimizeLegibility; cursor: pointer !important;'>
													<td style="font: 120% sans-serif; padding-left:10px; width:15%; text-align:center;">{{ barrio[0] }}</td>
													<td style="font: 120% sans-serif; padding-left:10px; text-align:left;">{{ barrio[1] }}</td>
												</tr>
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</transition>
					</div>

					<div v-show="showEstratos">
						<transition name="modal">
							<div class="modal-mask">
								<div class="modal-wrapper">
									<div class="modal-container">
										<table class="inicio ancho">
											<tr>
												<td class="titulos" colspan="2" >Estratos</td>
												<td class="cerrar" style="width:7%" @click="showEstratos = false">Cerrar</td>
											</tr>
											<tr>
												<td class="tamano01" style="width:3cm">Estratos:</td>
												<td>
													<input type="text" v-model="searchEstrato.keywordEstrato" v-on:keyup="searchMonitorEstrato" placeholder="Buscar por nombre o código" style="width:100%" />
												</td>
											</tr>
										</table>
										<table class='tablamv'>
											<thead>
												<tr style="text-align:center;">
													<th class="titulosnew02" >Resultados Busqueda</th>
												</tr>
												<tr style="text-align:center;">
													<th class="titulosnew00" style="width: 15%;">Código barrio</th>
													<th class="titulosnew00">Nombre barrio</th>
												</tr>
											</thead>
											<tbody>
												<tr v-for="(estrato,index) in estratos" v-on:click="seleccionaEstrato(estrato)" v-bind:class="index % 2 ? 'saludo1a' : 'saludo2'" style='text-rendering: optimizeLegibility; cursor: pointer !important;'>
													<td style="font: 120% sans-serif; padding-left:10px; width:15%; text-align:center;">{{ estrato[0] }}</td>
													<td style="font: 120% sans-serif; padding-left:10px; text-align:left;">{{ estrato[1] }}</td>
												</tr>
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</transition>
					</div>

					<div v-show="showRutas">
						<transition name="modal">
							<div class="modal-mask">
								<div class="modal-wrapper">
									<div class="modal-container">
										<table class="inicio ancho">
											<tr>
												<td class="titulos" colspan="2" >Rutas</td>
												<td class="cerrar" style="width:7%" @click="showRutas = false">Cerrar</td>
											</tr>
											<tr>
												<td class="tamano01" style="width:3cm">Ruta:</td>
												<td>
													<input type="text" v-model="searchRuta.keywordRuta" v-on:keyup="searchMonitorRuta" placeholder="Buscar por nombre o código" style="width:100%" />
												</td>
											</tr>
										</table>
										<table class='tablamv'>
											<thead>
												<tr style="text-align:center;">
													<th class="titulosnew02" >Resultados Busqueda</th>
												</tr>
												<tr style="text-align:center;">
													<th class="titulosnew00" style="width: 15%;">Código barrio</th>
													<th class="titulosnew00">Nombre barrio</th>
												</tr>
											</thead>
											<tbody>
												<tr v-for="(ruta,index) in rutas" v-on:click="seleccionaRuta(ruta)" v-bind:class="index % 2 ? 'saludo1a' : 'saludo2'" style='text-rendering: optimizeLegibility; cursor: pointer !important;'>
													<td style="font: 120% sans-serif; padding-left:10px; width:15%; text-align:center;">{{ ruta[0] }}</td>
													<td style="font: 120% sans-serif; padding-left:10px; text-align:center;">{{ ruta[1] }}</td>
												</tr>
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</transition>
					</div>
				</article>
			</section>
		</form>
        
		<script src="Librerias/vue/vue.min.js"></script>
		<script src="Librerias/vue/axios.min.js"></script>
		<script src="servicios_publicos/usuarios/crear/serv-crearUsuario.js"></script>
        
	</body>
</html>