<?php //V 1000 12/12/16 ?> 
<?php  
require_once 'PHPExcel/Classes/PHPExcel.php';
require"comun.inc";
require"funciones.inc";
session_start();
$linkbd=conectar_bd();  
$objPHPExcel = new PHPExcel();

//----Propiedades----
$objPHPExcel->getProperties()
        ->setCreator("SPID")
        ->setLastModifiedBy("SPID")
        ->setTitle("Exportar Excel con PHP")
        ->setSubject("Documento de prueba")
        ->setDescription("Documento generado con PHPExcel")
        ->setKeywords("usuarios phpexcel")
        ->setCategory("reportes");

//----Cuerpo de Documento----
$objPHPExcel->getActiveSheet()->mergeCells('A1:G1');
$objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue('A1', 'Archivos Maestros -  Ingresos Sin Situacion de Fondos SSF');

$objFont=$objPHPExcel->getActiveSheet()->getStyle('A1')->getFont();
$objFont->setName('Courier New'); 
$objFont->setSize(15); 
$objFont->setBold(true); 
$objFont->setUnderline(PHPExcel_Style_Font::UNDERLINE_SINGLE);
$objFont->getColor()->setARGB( PHPExcel_Style_Color::COLOR_BLACK);

$objAlign=$objPHPExcel->getActiveSheet()->getStyle('A1')->getAlignment(); 
$objAlign->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER); 
$objAlign->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER); 

$objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue('A2', 'Codigo')
            ->setCellValue('B2', 'Nombre')
            ->setCellValue('C2', 'Cuenta Presupuesto Ingreso')
            ->setCellValue('D2', 'Cuenta Presupuesto Gasto')
            ->setCellValue('E2','Concepto Ingreso')
            ->setCellValue('F2','Concepto Gasto')
            ->setCellValue('G2','Estado');

$i=3;
$estado="";
$sqlr="SELECT * FROM tesoingresossf_cab order by tesoingresossf_cab.codigo ";
$resp = mysql_query($sqlr,$linkbd);
while ($row =mysql_fetch_row($resp)) 
 {
    $sqlr2="select * from tesoingresossf_det where tesoingresossf_det.estado='S' and tesoingresossf_det.codigo='$row[0]'";
    $resp2 = mysql_query($sqlr2,$linkbd);
    $row2 =mysql_fetch_row($resp2);
    if($row[2]=='S')
    {
        $estado="ACTIVO";
    }else
    {
        $estado="INACTIVO";
    }
    
    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0,$i,$row[0]);
    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(1,$i,utf8_encode($row[1]));
    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(2,$i,$row2[5]);
    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(3,$i,$row2[6]);
    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(4,$i,$row2[2]);
    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(5,$i,$row2[3]);
    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(6,$i,$estado);
    $i+=1;
 }

//----Propiedades de la hoja
$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true); 
$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true); 
$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true); 
$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true); 
$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true); 
$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);  
$objPHPExcel->getActiveSheet()->setTitle('IngresoSSF');
$objPHPExcel->setActiveSheetIndex(0);

//----Guardar documento----
header('Content-Type: application/vnd.ms-excel');
header('Content-Disposition: attachment;filename="Teso-Arch-M-ingresossf.xls"');
header('Cache-Control: max-age=0');
 
$objWriter=PHPExcel_IOFactory::createWriter($objPHPExcel,'Excel5');
$objWriter->save('php://output');
exit;

?>