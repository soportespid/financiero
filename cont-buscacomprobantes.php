<?php
header("Cache-control: no-cache, no-store, must-revalidate");
header("Content-Type: text/html;charset=utf8");

require 'comun.inc';
require 'funciones.inc';
require 'funcionesSP.inc.php';

$linkbd = conectar_v7();
$linkbd->set_charset("utf8");

session_start();
date_default_timezone_set("America/Bogota");

?>
<!DOCTYPE>
<html lang="es">

<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1" />
    <meta name="viewport" content="user-scalable=no">
    <title>:: IDEAL 10 - Contabilidad</title>
    <link href="favicon.ico" rel="shortcut icon" />
    <link href="css/css2.css?<?php echo date('d_m_Y_h_i_s'); ?>" rel="stylesheet" type="text/css">
    <link href="css/css3.css?<?php echo date('d_m_Y_h_i_s'); ?>" rel="stylesheet" type="text/css">
    <link href="css/style.css?<?php echo date('d_m_Y_h_i_s'); ?>" rel="stylesheet" type="text/css">
    <script type="text/javascript" src="css/calendario.js?<?php echo date('d_m_Y_h_i_s'); ?>"></script>
    <script type="text/javascript" src="css/programas.js?<?php echo date('d_m_Y_h_i_s'); ?>"></script>
    <script type='text/javascript' src='JQuery/jquery-2.1.4.min.js'></script>
    <script>
        function pdf() {
            document.form2.action = "cmovpdf";
            document.form2.target = "_BLANK";
            document.form2.submit();
            document.form2.action = "";
            document.form2.target = "";
        }
        function redirect() {
            let estadRev = document.form2.estadoRev.value;
            let partsRev = estadRev.split('-');
            window.open("cont-buscacomprobantes.php?idCat=1&tipo_compro=" + partsRev[0] + "&num_compro=" + partsRev[1]);
        }
        function agregardetalle() {
            document.form2.tipcomp.value = document.form2.tipocomprobante.value;
            document.form2.catcompro.value = document.form2.categoria.value;
            valordeb = quitarpuntos(document.form2.vlrdeb.value)
            valorcred = quitarpuntos(document.form2.vlrcre.value)
            if (document.form2.cuenta.value != "" && document.form2.tercero.value != "" && document.form2.cc.value != "" && (valordeb > 0 || valorcred > 0)) { document.form2.agregadet.value = 1; document.form2.submit(); }
            else { despliegamodalm('visible', '2', 'Falta informacion para poder Agregar'); }
        }
        function eliminar(variable) {
            document.form2.elimina.value = variable;
            document.form2.tipcomp.value = document.form2.tipocomprobante.value;
            document.form2.catcompro.value = document.form2.categoria.value;
            despliegamodalm('visible', '4', 'Esta Seguro de Eliminar', '4');
        }
        function adelante() {
            var validarFijo = document.getElementById('estadoFijo').value;
            if (validarFijo == 'N') {
                if (parseFloat(document.form2.ncomp.value) < parseFloat(document.form2.maximo.value)) {
                    document.form2.fecha.value = '';
                    document.form2.oculto.value = 2;
                    document.form2.agregadet.value = '';
                    document.form2.elimina.value = '';
                    document.form2.ncomp.value = parseFloat(document.form2.ncomp.value) + 1;
                    document.form2.action = "cont-buscacomprobantes.php";
                    document.form2.tipcomp.value = document.form2.tipocomprobante.value;
                    document.form2.catcompro.value = document.form2.categoria.value;
                    document.form2.submit();
                }
            }
            else {
                if (parseFloat(document.form2.ncomp.value) < parseFloat(document.form2.maximo.value)) {
                    document.form2.fecha.value = '';
                    document.form2.oculto.value = 2;
                    // document.form2.agregadet.value='';
                    document.form2.elimina.value = '';
                    document.form2.ncomp.value = parseFloat(document.form2.ncomp.value) + 1;
                    document.form2.action = "cont-buscacomprobantes.php";
                    document.form2.tipcomp.value = document.form2.tipocomprobante.value;
                    document.form2.catcompro.value = document.form2.categoria.value;
                    document.form2.submit();
                }
            }

        }
        function atrasc() {
            var validarFijo = document.getElementById('estadoFijo').value;

            if (validarFijo == 'N') {
                if (document.form2.ncomp.value > 1) {
                    document.form2.fecha.value = '';
                    document.form2.oculto.value = 2;
                    document.form2.agregadet.value = '';
                    document.form2.elimina.value = '';
                    document.form2.ncomp.value = document.form2.ncomp.value - 1;
                    document.form2.tipcomp.value = document.form2.tipocomprobante.value;
                    document.form2.catcompro.value = document.form2.categoria.value;
                    document.form2.action = "cont-buscacomprobantes.php";
                    document.form2.submit();
                }
            }
            else {
                if (document.form2.ncomp.value > 1) {
                    document.form2.fecha.value = '';
                    document.form2.oculto.value = 2;
                    //document.form2.agregadet.value='';
                    //document.form2.elimina.value='';
                    document.form2.ncomp.value = document.form2.ncomp.value - 1;
                    document.form2.tipcomp.value = document.form2.tipocomprobante.value;
                    document.form2.catcompro.value = document.form2.categoria.value;
                    document.form2.action = "cont-buscacomprobantes.php";
                    document.form2.submit();
                }
            }

        }
        function validarMovimiento() {
            var x = document.getElementById("tipomov").value;
            document.form2.movimiento.value = x;
            document.form2.oculto.value = 2;
            document.form2.tipcomp.value = document.form2.tipocomprobante.value;
            document.form2.catcompro.value = document.form2.categoria.value;
            document.form2.agregadet.value = '';
            document.form2.elimina.value = '';
            document.form2.action = "cont-buscacomprobantes.php";
            document.form2.submit();
        }
        function validar() {
            document.form2.oculto.value = 1;
            document.form2.action = "cont-buscacomprobantes.php";
            document.form2.tipcomp.value = document.form2.tipocomprobante.value;
            document.form2.catcompro.value = document.form2.categoria.value;
            document.form2.submit();
        }
        function validarcat() {
            document.form2.action = "cont-buscacomprobantes.php";
            document.form2.catcompro.value = document.form2.categoria.value;
            document.form2.submit();
        }
        function validar3() { document.form2.submit(); }
        function validar2() {
            var validarFijo = document.getElementById('estadoFijo').value;
            if (validarFijo == 'N') {
                document.form2.oculto.value = 2;
                document.form2.tipcomp.value = document.form2.tipocomprobante.value;
                document.form2.catcompro.value = document.form2.categoria.value;
                document.form2.agregadet.value = '';
                document.form2.elimina.value = '';
                document.form2.action = "cont-buscacomprobantes.php";
                document.form2.submit();
            }
            else {
                document.form2.oculto.value = 2;
                document.form2.tipcomp.value = document.form2.tipocomprobante.value;
                document.form2.catcompro.value = document.form2.categoria.value;
                // document.form2.agregadet.value='';
                // document.form2.elimina.value='';
                document.form2.action = "cont-buscacomprobantes.php";
                document.form2.submit();
            }

        }
        function guardar() {
            document.form2.tipcomp.value = document.form2.tipocomprobante.value;
            document.form2.catcompro.value = document.form2.categoria.value;
            valor = Math.round(Math.abs(parseFloat(document.form2.diferencia.value)));
            if (valor == 0) {
                var validacion01 = document.getElementById('concepto').value;
                if ((document.form2.fecha.value != '') && (document.form2.tipocomprobante.value != "") && (validacion01.trim() != '')) { despliegamodalm('visible', '4', 'Esta Seguro de Modificar', '2'); }
                else { despliegamodalm('visible', '2', "Falta información para poder guardar") }
            }
            else {
                var nomtitul = 'Comprobante descuadrado Diferencia: \"' + valor + '\"';
                despliegamodalm('visible', '2', nomtitul)
            }
        }
        function duplicarcomp() {
            valor = Math.round(parseFloat(document.form2.diferencia.value));
            if (valor == 0 && document.form2.fecha.value != '') { despliegamodalm('visible', '4', 'Esta Seguro de Duplicar el Comprobante', '3'); }
            else {
                var titumod = "Comprobante descuadrado o faltan informacion: " + valor;
                despliegamodalm('visible', '2', titumod);
            }
        }
        function buscacta(e) {
            if (document.form2.cuenta.value != "") {
                document.form2.bc.value = '1';
                document.form2.tipcomp.value = document.form2.tipocomprobante.value;
                document.form2.catcompro.value = document.form2.categoria.value;
                document.form2.submit();
            }
        }
        function buscacc(e) {
            if (document.form2.cc.value != "") {
                document.form2.bcc.value = '1';
                document.form2.tipcomp.value = document.form2.tipocomprobante.value;
                document.form2.catcompro.value = document.form2.categoria.value;
                document.form2.submit();
            }
        }
        function buscater(e) {
            if (document.form2.tercero.value != "") {
                document.form2.bt.value = '1';
                document.form2.tipcomp.value = document.form2.tipocomprobante.value;
                document.form2.catcompro.value = document.form2.categoria.value;
                document.form2.submit();
            }
        }
        function excell() {
            document.form2.action = "cont-buscacomprobantesexcel.php";
            document.form2.target = "_BLANK";
            document.form2.submit();
            document.form2.action = "";
            document.form2.target = "";
        }
        function despliegamodal2(_valor, _nomve) {
            document.getElementById("bgventanamodal2").style.visibility = _valor;
            if (_valor == "hidden") { document.getElementById('ventana2').src = ""; }
            else {
                switch (_nomve) {
                    case "1": document.getElementById('ventana2').src = "terceros-ventana1.php"; break;
                    case "2": document.getElementById('ventana2').src = "cuentas-ventana01.php"; break;
                    case "3": document.getElementById('ventana2').src = "cc-ventana01.php"; break;
                }

            }
        }
        function despliegamodalm(_valor, _tip, mensa, pregunta) {
            document.getElementById("bgventanamodalm").style.visibility = _valor;
            if (_valor == "hidden") {
                document.getElementById('ventanam').src = "";
                switch (document.getElementById('valfocus').value) {
                    case "2": document.getElementById('valfocus').value = '1';
                        document.getElementById('cuenta').focus();
                        document.getElementById('cuenta').select();
                        break;
                    case "3": document.getElementById('valfocus').value = '1';
                        document.getElementById('cc').focus();
                        document.getElementById('cc').select();
                        break;
                }
            }
            else {
                switch (_tip) {
                    case "1":
                        document.getElementById('ventanam').src = "ventana-mensaje1.php?titulos=" + mensa; break;
                    case "2":
                        document.getElementById('ventanam').src = "ventana-mensaje3.php?titulos=" + mensa; break;
                    case "3":
                        document.getElementById('ventanam').src = "ventana-mensaje2.php?titulos=" + mensa; break;
                    case "4":
                        document.getElementById('ventanam').src = "ventana-consulta2.php?titulos=" + mensa + "&idresp=" + pregunta; break;
                }
            }
        }
        function funcionmensaje() { document.location.href = "cont-buscacomprobantes.php"; }
        function respuestaconsulta(estado, pregunta) {
            if (estado == "S") {
                switch (pregunta) {
                    case "1": document.getElementById('bt').value = "0";
                        mypop = window.open('cont-terceros.php', '', ''); break;
                    case "2": document.form2.oculto.value = '3';
                        document.form2.action = "cont-buscacomprobantes.php";
                        document.form2.submit(); break;
                    case "3": document.form2.oculto.value = '4';
                        document.form2.duplicar.value = '2';
                        document.form2.action = "cont-buscacomprobantes.php";
                        document.form2.submit(); break;
                    case "4": document.form2.elidet.value = "1";
                        document.form2.submit(); break;
                }
            }
            else {
                switch (pregunta) {
                    case "1": document.getElementById('bt').value = "0";
                        document.getElementById('tercero').focus();
                        document.getElementById('tercero').select();
                        break;
                    case "2":
                    case "3": break;
                    case "4": document.form2.elimina.value = ""; break;
                }
            }
        }
        function llamadoesc(e, _opc) {
            tecla = (document.all) ? e.keyCode : e.which;
            if (tecla == 27) {
                switch (_opc) {
                    case "1": document.getElementById("bgventanamodal2").style.visibility = "visible";
                        document.getElementById('ventana2').src = "cuentas-ventana01.php"; break;
                    case "2": document.getElementById("bgventanamodal2").style.visibility = "visible";
                        document.getElementById('ventana2').src = "terceros-ventana1.php"; break;
                    case "3": document.getElementById("bgventanamodal2").style.visibility = "visible";
                        document.getElementById('ventana2').src = "cc-ventana01.php"; break;
                }
            }
        }
        function calcular() {
            document.form2.tipcomp.value = document.form2.tipocomprobante.value;
            document.form2.catcompro.value = document.form2.categoria.value;
            document.form2.submit();
        }
        var ctrlPressed = false;
        var tecla01 = 17, tecla02 = 37, tecla03 = 39;
        $(document).keydown(function (e) {
            if (e.keyCode == tecla01) { ctrlPressed = true; }
            if (ctrlPressed && (e.keyCode == tecla02)) {
                atrasc();
            }
        });
        $(document).keydown(function (e) {
            if (ctrlPressed && (e.keyCode == tecla03)) {
                adelante();
            }
        });
        $(document).keyup(function (e) {
            if (e.keyCode == tecla01) { ctrlPressed = false; }
        });
    </script>
</head>

<body>
    <IFRAME src="alertas.php" name="alertas" id="alertas" style="display:none"></IFRAME>
    <span id="todastablas2"></span>
    <table>
        <tr>
            <script>barra_imagenes("cont");</script><?php cuadro_titulos(); ?>
        </tr>
        <tr><?php menu_desplegable("cont"); ?></tr>
    </table>
    <div class="bg-white group-btn p-1"><button type="button" onclick="location.href='cont-comprobantes.php'" class="btn btn-white btn-primary-hover d-flex justify-between align-items-center">
            <span>Nuevo</span>
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 -960 960 960"><path d="M440-440H200v-80h240v-240h80v240h240v80H520v240h-80v-240Z"></path></svg>
        </button><button type="button" onclick="guardar()" class="btn btn-white btn-primary-hover d-flex justify-between align-items-center">
            <span>Guardar</span>
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 -960 960 960"><path d="M840-680v480q0 33-23.5 56.5T760-120H200q-33 0-56.5-23.5T120-200v-560q0-33 23.5-56.5T200-840h480l160 160Zm-80 34L646-760H200v560h560v-446ZM480-240q50 0 85-35t35-85q0-50-35-85t-85-35q-50 0-85 35t-35 85q0 50 35 85t85 35ZM240-560h360v-160H240v160Zm-40-86v446-560 114Z"></path></svg>
        </button><button type="button" onclick="location.href='cont-buscacomprobantes.php'" class="btn btn-white btn-primary-hover d-flex justify-between align-items-center">
            <span>Buscar</span>
            <svg class="fill-black group-hover:fill-white w-5 h-5" xmlns="http://www.w3.org/2000/svg" viewBox="0 -960 960 960"><path d="M784-120 532-372q-30 24-69 38t-83 14q-109 0-184.5-75.5T120-580q0-109 75.5-184.5T380-840q109 0 184.5 75.5T640-580q0 44-14 83t-38 69l252 252-56 56ZM380-400q75 0 127.5-52.5T560-580q0-75-52.5-127.5T380-760q-75 0-127.5 52.5T200-580q0 75 52.5 127.5T380-400Z"></path></svg>
        </button><button type="button" onclick="mypop=window.open('plan-agenda.php','','');mypop.focus()" class="btn btn-white btn-primary-hover d-flex justify-between align-items-center">
            <span class="group-hover:text-white">Agenda</span>
            <svg class="fill-black group-hover:fill-white w-5 h-5" xmlns="http://www.w3.org/2000/svg" viewBox="0 -960 960 960"><path d="M560-564v-68q33-14 67.5-21t72.5-7q26 0 51 4t49 10v64q-24-9-48.5-13.5T700-600q-38 0-73 9.5T560-564Zm0 220v-68q33-14 67.5-21t72.5-7q26 0 51 4t49 10v64q-24-9-48.5-13.5T700-380q-38 0-73 9t-67 27Zm0-110v-68q33-14 67.5-21t72.5-7q26 0 51 4t49 10v64q-24-9-48.5-13.5T700-490q-38 0-73 9.5T560-454ZM260-320q47 0 91.5 10.5T440-278v-394q-41-24-87-36t-93-12q-36 0-71.5 7T120-692v396q35-12 69.5-18t70.5-6Zm260 42q44-21 88.5-31.5T700-320q36 0 70.5 6t69.5 18v-396q-33-14-68.5-21t-71.5-7q-47 0-93 12t-87 36v394Zm-40 118q-48-38-104-59t-116-21q-42 0-82.5 11T100-198q-21 11-40.5-1T40-234v-482q0-11 5.5-21T62-752q46-24 96-36t102-12q58 0 113.5 15T480-740q51-30 106.5-45T700-800q52 0 102 12t96 36q11 5 16.5 15t5.5 21v482q0 23-19.5 35t-40.5 1q-37-20-77.5-31T700-240q-60 0-116 21t-104 59ZM280-494Z"></path></svg>
        </button><button type="button" onclick="window.open('cont-principal');" class="btn btn-white btn-primary-hover d-flex justify-between align-items-center">
            <span>Nueva ventana</span>
            <svg class="fill-black group-hover:fill-white w-5 h-5" xmlns="http://www.w3.org/2000/svg" viewBox="0 -960 960 960"><path d="M320-120v-80h80v-80H160q-33 0-56.5-23.5T80-360v-400q0-33 23.5-56.5T160-840h640q33 0 56.5 23.5T880-760v400q0 33-23.5 56.5T800-280H560v80h80v80H320ZM160-360h640v-400H160v400Zm0 0v-400 400Z"></path></svg>
        </button><button type="button" onclick="mypop=window.open('/financiero/cont-buscacomprobantes.php','','');mypop.focus();" class="btn btn-white btn-primary-hover d-flex justify-between align-items-center">
            <span class="group-hover:text-white">Duplicar pantalla</span>
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 -960 960 960"><path d="M640-160v-360H160v360h480Zm80-200v-80h80v-360H320v200h-80v-200q0-33 23.5-56.5T320-880h480q33 0 56.5 23.5T880-800v360q0 33-23.5 56.5T800-360h-80ZM160-80q-33 0-56.5-23.5T80-160v-360q0-33 23.5-56.5T160-600h480q33 0 56.5 23.5T720-520v360q0 33-23.5 56.5T640-80H160Zm400-603ZM400-340Z"></path></svg>
        </button><button type="button" onclick="pdf()" class="btn btn-danger btn-danger-hover d-flex justify-between align-items-center">
            <span>Exportar PDF</span>
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><!-- !Font Awesome Free 6.5.1 by @fontawesome - https://fontawesome.com License - https://fontawesome.com/license/free Copyright 2024 Fonticons, Inc. --><path d="M64 464l48 0 0 48-48 0c-35.3 0-64-28.7-64-64L0 64C0 28.7 28.7 0 64 0L229.5 0c17 0 33.3 6.7 45.3 18.7l90.5 90.5c12 12 18.7 28.3 18.7 45.3L384 304l-48 0 0-144-80 0c-17.7 0-32-14.3-32-32l0-80L64 48c-8.8 0-16 7.2-16 16l0 384c0 8.8 7.2 16 16 16zM176 352l32 0c30.9 0 56 25.1 56 56s-25.1 56-56 56l-16 0 0 32c0 8.8-7.2 16-16 16s-16-7.2-16-16l0-48 0-80c0-8.8 7.2-16 16-16zm32 80c13.3 0 24-10.7 24-24s-10.7-24-24-24l-16 0 0 48 16 0zm96-80l32 0c26.5 0 48 21.5 48 48l0 64c0 26.5-21.5 48-48 48l-32 0c-8.8 0-16-7.2-16-16l0-128c0-8.8 7.2-16 16-16zm32 128c8.8 0 16-7.2 16-16l0-64c0-8.8-7.2-16-16-16l-16 0 0 96 16 0zm80-112c0-8.8 7.2-16 16-16l48 0c8.8 0 16 7.2 16 16s-7.2 16-16 16l-32 0 0 32 32 0c8.8 0 16 7.2 16 16s-7.2 16-16 16l-32 0 0 48c0 8.8-7.2 16-16 16s-16-7.2-16-16l0-64 0-64z"></path></svg>
        </button><button type="button" onclick="duplicarcomp()" class="btn btn-white btn-primary-hover d-flex justify-between align-items-center">
            <span>Duplicar Comprobante</span>
            <svg class="fill-black group-hover:fill-white w-4 h-4" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 384 512"><!--!Font Awesome Free 6.6.0 by @fontawesome - https://fontawesome.com License - https://fontawesome.com/license/free Copyright 2024 Fonticons, Inc.--><path d="M384 336l-192 0c-8.8 0-16-7.2-16-16l0-256c0-8.8 7.2-16 16-16l140.1 0L400 115.9 400 320c0 8.8-7.2 16-16 16zM192 384l192 0c35.3 0 64-28.7 64-64l0-204.1c0-12.7-5.1-24.9-14.1-33.9L366.1 14.1c-9-9-21.2-14.1-33.9-14.1L192 0c-35.3 0-64 28.7-64 64l0 256c0 35.3 28.7 64 64 64zM64 128c-35.3 0-64 28.7-64 64L0 448c0 35.3 28.7 64 64 64l192 0c35.3 0 64-28.7 64-64l0-32-48 0 0 32c0 8.8-7.2 16-16 16L64 464c-8.8 0-16-7.2-16-16l0-256c0-8.8 7.2-16 16-16l32 0 0-48-32 0z"/></path></svg>
        </button><button type="button" onclick="excell()" class="btn btn-success btn-success-hover d-flex justify-between align-items-center">
            <span>Exportar Excel</span>
            <svg class="fill-black group-hover:fill-white w-4 h-4" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 384 512"><!--!Font Awesome Free 6.5.1 by @fontawesome - https://fontawesome.com License - https://fontawesome.com/license/free Copyright 2024 Fonticons, Inc. --><path d="M48 448V64c0-8.8 7.2-16 16-16H224v80c0 17.7 14.3 32 32 32h80V448c0 8.8-7.2 16-16 16H64c-8.8 0-16-7.2-16-16zM64 0C28.7 0 0 28.7 0 64V448c0 35.3 28.7 64 64 64H320c35.3 0 64-28.7 64-64V154.5c0-17-6.7-33.3-18.7-45.3L274.7 18.7C262.7 6.7 246.5 0 229.5 0H64zm90.9 233.3c-8.1-10.5-23.2-12.3-33.7-4.2s-12.3 23.2-4.2 33.7L161.6 320l-44.5 57.3c-8.1 10.5-6.3 25.5 4.2 33.7s25.5 6.3 33.7-4.2L192 359.1l37.1 47.6c8.1 10.5 23.2 12.3 33.7 4.2s12.3-23.2 4.2-33.7L222.4 320l44.5-57.3c8.1-10.5 6.3-25.5-4.2-33.7s-25.5-6.3-33.7 4.2L192 280.9l-37.1-47.6z"></path></svg>
        </button></div>
        <div id="bgventanamodalm" class="bgventanamodalm">
            <div id="ventanamodalm" class="ventanamodalm">
                <IFRAME src="" name="ventanam" marginWidth=0 marginHeight=0 frameBorder=0 id="ventanam" frameSpacing=0 style=" width:700px; height:130px; top:200; overflow:hidden;">
                </IFRAME>
            </div>
        </div>
        <?php
        $vigusu = vigencia_usuarios($_SESSION['cedulausu']);
        $_POST['estadoc'] = '';
        $_POST['estadoRev'] = '';
        $consulta = "";
        $estadoFijo = 'S';

        if (isset($_GET['idCat'])) {
            $_POST['catcompro'] = $_GET['idCat'];
            $_POST['categoria'] = $_GET['idCat'];
            $_POST['tipocomprobante'] = $_GET['tipo_compro'];
            $_POST['tipcomp'] = $_GET['tipo_compro'];
            $consulta = " AND numerotipo='" . $_GET['num_compro'] . "'";
            $_POST['oculto'] = '1';
        }
        if ($_POST['oculto'] == '1') {
            $_POST['concepto'] = "";
            $_POST['total'] = "";
            $_POST['cuenta'] = "";
            $_POST['ncuenta'] = "";
            $_POST['tercero'] = "";
            $_POST['ntercero'] = "";
            $_POST['cc'] = "";
            $_POST['ncc'] = "";
            $_POST['detalle'] = "";
            $_POST['cuentadeb'] = "";
            $_POST['cuentacred'] = "";
            $_POST['diferencia'] = "";
            $_POST['estado'] = "";
            $sqlr = "SELECT * FROM comprobante_cab WHERE tipo_comp = '" . $_POST['tipocomprobante'] . "' $consulta ORDER BY numerotipo DESC";
            $res = mysqli_query($linkbd, $sqlr);
            $r = mysqli_fetch_row($res);
            $_POST['maximo'] = $r[1];
            if (isset($_GET['num_compro'])) {
                $_POST['ncomp'] = $_GET['num_compro'];
            } else {
                $_POST['ncomp'] = $r[1];
            }
            $_POST['fecha'] = $r[3];
            $_POST['concepto'] = $r[4];
            $_POST['total'] = $r[5];
            $_POST['cuentadeb'] = $r[6];
            $_POST['cuentacred'] = $r[7];
            $_POST['diferencia'] = $r[8];
            if ($r[9] == '1') {
                $_POST['estadoc'] = 'ACTIVO';
            }
            if ($r[9] == '0') {
                $_POST['estadoc'] = 'ANULADO';
            }
            $_POST['estado'] = $r[9];
            $_POST['dcuentas'] = array();
            $_POST['dncuentas'] = array();
            $_POST['dterceros'] = array();
            $_POST['dnterceros'] = array();
            $_POST['dccs'] = array();
            $_POST['ddetalles'] = array();
            $_POST['dcreditos'] = array();
            $_POST['ddebitos'] = array();
            $idet = $_POST['tipocomprobante'];
            $ndet = $_POST['ncomp'];
            $sqlr = "SELECT * FROM comprobante_det WHERE tipo_comp = '$idet' AND numerotipo = '$ndet' ORDER BY id_det";
            $res2 = mysqli_query($linkbd, $sqlr);
            while ($r2 = mysqli_fetch_row($res2)) {
                $_POST['dcuentas'][] = $r2[2];
                $nresul = buscacuenta($r2[2]);
                $_POST['dncuentas'][] = $nresul;
                $_POST['dterceros'][] = $r2[3];
                $_POST['dccs'][] = $r2[4];
                $_POST['ddetalles'][] = $r2[5];
                $_POST['dcreditos'][] = $r2[8];
                $_POST['ddebitos'][] = $r2[7];
            }
            $sqlConsultaManual = "SELECT fijo FROM tipo_comprobante WHERE codigo = '" . $_POST['tipocomprobante'] . "'";
            $resConsultaManual = mysqli_query($linkbd, $sqlConsultaManual);
            $rowConsultaManual = mysqli_fetch_row($resConsultaManual);
            $_POST['estadoFijo'] = $rowConsultaManual[0];
        } else {
            $sqlConsultaManual = "SELECT fijo FROM tipo_comprobante WHERE codigo = '" . $_POST['tipocomprobante'] . "'";
            $resConsultaManual = mysqli_query($linkbd, $sqlConsultaManual);
            $rowConsultaManual = mysqli_fetch_row($resConsultaManual);
            $_POST['estadoFijo'] = $rowConsultaManual[0];

            if ($_POST['elimina'] == '' && $_POST['agregadet'] == '') {
                $sqlConsultaManual = "SELECT fijo FROM tipo_comprobante WHERE codigo = '" . $_POST['tipocomprobante'] . "'";
                $resConsultaManual = mysqli_query($linkbd, $sqlConsultaManual);
                $rowConsultaManual = mysqli_fetch_row($resConsultaManual);
                $_POST['estadoFijo'] = $rowConsultaManual[0];
                $_POST['concepto'] = "";
                $_POST['total'] = "";
                $_POST['cuentadeb'] = "";
                $_POST['cuenta'] = "";
                $_POST['ncuenta'] = "";
                $_POST['tercero'] = "";
                $_POST['ntercero'] = "";
                $_POST['cc'] = "";
                $_POST['ncc'] = "";
                $_POST['detalle'] = "";
                $_POST['cuentacred'] = "";
                $_POST['diferencia'] = "";
                $_POST['estado'] = "";
                $sqlr = "SELECT * FROM comprobante_cab WHERE tipo_comp = '" . $_POST['tipocomprobante'] . "' AND numerotipo = '" . $_POST['ncomp'] . "'";
                $res = mysqli_query($linkbd, $sqlr);
                $color = '';
                $colorRev = '';
                while ($r = mysqli_fetch_row($res)) {
                    if ($_POST['oculto'] == 2) {
                        $_POST['fecha'] = $r[3];
                    }
                    $_POST['concepto'] = $r[4];
                    $_POST['total'] = $r[5];
                    $_POST['cuentadeb'] = $r[6];
                    $_POST['cuentacred'] = $r[7];
                    $_POST['diferencia'] = $r[8];
                    $_POST['estado'] = $r[9];
                    if ($r[9] == '1') {
                        $_POST['estadoc'] = 'ACTIVO';
                        $color = " style='background-color:#009900 ;color:#fff'";
                    }
                    if ($r[9] == '0') {
                        $_POST['estadoc'] = 'ANULADO';
                        $color = " style='background-color:#aa0000 ; color:#fff'";
                    }

                    $tipo_compRev = intval($_POST['tipocomprobante']) + 2000;

                    $sqlrRev = "SELECT tipo_comp, numerotipo FROM comprobante_cab WHERE numerotipo = '" . $_POST['ncomp'] . "' AND tipo_comp = '$tipo_compRev'";
                    $resRev = mysqli_query($linkbd, $sqlrRev);
                    $numRows = mysqli_num_rows($resRev);

                    if ($numRows > 0) {
                        $rowRev = mysqli_fetch_row($resRev);
                        $_POST['estadoRev'] = $rowRev[0] . '-' . $rowRev[1];
                        $colorRev = " style='background-color:#aa0000 ; color:#fff'";
                    }

                }
            }
            $idet = $_POST['tipocomprobante'] . " " . $_POST['ncomp'];
            if ($_POST['elimina'] == '' && $_POST['agregadet'] == '') {
                $_POST['dcuentas'] = array();
                $_POST['dncuentas'] = array();
                $_POST['dterceros'] = array();
                $_POST['dnterceros'] = array();
                $_POST['dccs'] = array();
                $_POST['ddetalles'] = array();
                $_POST['dcreditos'] = array();
                $_POST['ddebitos'] = array();
                $sqlr = "SELECT * FROM comprobante_det WHERE tipo_comp = '" . $_POST['tipocomprobante'] . "' AND numerotipo = '" . $_POST['ncomp'] . "' ORDER BY id_det";
                $res2 = mysqli_query($linkbd, $sqlr);
                while ($r2 = mysqli_fetch_row($res2)) {
                    $_POST['dcuentas'][] = $r2[2];
                    $nresul = buscacuenta($r2[2]);
                    $_POST['dncuentas'][] = $nresul;
                    $_POST['dterceros'][] = $r2[3];
                    $_POST['dccs'][] = $r2[4];
                    $_POST['ddetalles'][] = $r2[5];
                    $_POST['dcreditos'][] = round($r2[8], 2);
                    $_POST['ddebitos'][] = round($r2[7], 2);
                }
            }
        }
        ?>
        <form name="form2" method="post" action="">
            <input type="hidden" name="estadoFijo" id="estadoFijo" value="<?php echo $_POST['estadoFijo'] ?>">
            <input type="hidden" name="valfocus" id="valfocus" value="1"/>
            <table class="inicio">
                <tr>
                    <td class="titulos" colspan="9">Comprobantes</td>
                    <td class="cerrar" style="width:7%;"><a onClick="location.href='cont-principal.php'">&nbsp;Cerrar</a></td>
                </tr>
                <tr>
                    <td class="saludo1" style="width:12.5%;">Categoria comprobante:</td>
                    <td style="width:20%;">
                        <select name="categoria" id="categoria" style="width:100%;" onChange="validarcat()">
                            <option value="">.: Seleccione la categoria</option>
                            <?php
                            $sql = "SELECT * FROM categoria_compro WHERE estado='S' ORDER BY id";
                            $result = mysqli_query($linkbd, $sql);
                            while ($row = mysqli_fetch_array($result)) {
                                if ($_POST['catcompro'] == $row[0]) {
                                    echo "<option value='$row[0]' SELECTED>$row[1]</option>";
                                } else {
                                    echo "<option value='$row[0]'>$row[1]</option>";
                                }

                            }
                            ?>
                        </select>
                        <input type="hidden" name="catcompro" id="catcompro" />
                    </td>
                    <td class="saludo1" style="width:4%;" >No:</td>
                    <td style="width:12%;">
                        <input type="hidden" name="ntipocomp" value="<?php echo $_POST['ntipocomp'] ?>"><a onClick="atrasc()" style='cursor:pointer;'><img src="imagenes/back.png" alt="anterior" align="absmiddle"></a>&nbsp;<input type="text" name="ncomp" onKeyPress="javascript:return solonumeros(event)" value="<?php echo $_POST['ncomp'] ?>" onKeyUp="return tabular(event,this)" onBlur="validar2()"  style="width:60%;text-align:center"><input type="hidden" value="a" name="atras"><input type="hidden" value="s" name="siguiente" ><input type="hidden" value="<?php echo $_POST['maximo'] ?>" name="maximo">&nbsp;<a onClick="adelante()" style='cursor:pointer;'><img src="imagenes/next.png" alt="siguiente" align="absmiddle"></a>
                    </td>
                    <td class="saludo1" >Fecha: </td>
                    <td  style="width:20%;"><input type="text" name="fecha" id="fecha" title="YYYY-MM-DD" value="<?php echo $_POST['fecha']; ?>" onKeyUp="return tabular(event,this)"  style="width:50%;" onKeyDown="mascara(this,'/',patron,true)"  maxlength="10" tabindex="2" >&nbsp;<a href="#" onClick="displayCalendarFor('fecha');" tabindex="3" title="Calendario"><img src="imagenes/calendario04.png" align="absmiddle" style="width:20px;"></a></td>
                    <td>
                        <select name="tipomov" id="tipomov" onKeyUp="return tabular(event,this)" onChange="validarMovimiento()" style="float:right">
                            <?php
                            $codMovimiento = '1';
                            if (isset($_POST['movimiento'])) {
                                if (!empty($_POST['movimiento'])) {
                                    $codMovimiento = $_POST['movimiento'];
                                }
                            }
                            $sql = "SELECT estado FROM comprobante_cab where numerotipo = '" . $_POST['ncomp'] . "' AND tipo_comp = '" . $_POST['tipocomprobante'] . "' ORDER BY estado";
                            $resultMov = mysqli_query($linkbd, $sql);
                            $movimientos = array();
                            $movimientos["1"]["nombre"] = "1-Documento de Creacion";
                            $movimientos["1"]["estado"] = "";
                            $movimientos["2"]["nombre"] = "2-Reversion";
                            $movimientos["2"]["estado"] = "";
                            while ($row = mysqli_fetch_row($resultMov)) {
                                $mov = $movimientos[$row[0]]["nombre"];
                                $movimientos[$codMovimiento]["estado"] = "selected";
                                $state = $movimientos[$row[0]]["estado"];
                                echo "<option value='$row[0]' $state>$mov</option>";
                            }
                            $movimientos[$codMovimiento]["estado"] = "";
                            echo "<input type='hidden' id='movimiento' name='movimiento' value='" . $_POST['movimiento'] . "'>";
                            ?>
                        </select>
                    </td>
                    <td class="saludo1">Estado:</td>
                    <td colspan="2">
                        <input type="hidden" id="duplicar" name="duplicar"  value="<?php echo $_POST['duplicar']; ?>" readonly>
                        <input type="hidden" name="estado"  value="<?php echo $_POST['estado']; ?>" readonly>
                        <input type="text" name="estadoc" id="estadoc"  value="<?php echo $_POST['estadoc']; ?>"  readonly>
                    </td>
                </tr>
                <tr>
                    <td class="saludo1" style="width:12.5%;">Tipo Comprobante:</td>
                    <td style="width:20%;">
                        <select name="tipocomprobante" id="tipocomprobante" onKeyUp='return tabular(event,this)' onChange="validar()" style="width:100%;">
                            <option value="-1">.: Seleccione Tipo Comprobante</option>
                            <?php
                            $sqlr = "SELECT * FROM tipo_comprobante WHERE estado = 'S' AND id_cat = '" . $_POST['categoria'] . "' ORDER BY id_tipo";
                            $resp = mysqli_query($linkbd, $sqlr);
                            while ($row = mysqli_fetch_row($resp)) {
                                if ($_POST['tipcomp'] == $row[3]) {
                                    $_POST['ntipocomp'] = $row[1];
                                    $estadoFijo = $row[4];
                                    echo "<option value=$row[3] SELECTED>$row[1]</option>";
                                } else {
                                    echo "<option value=$row[3]>$row[1]</option>";
                                }
                            }
                            ?>
                        </select>
                        <input type="hidden" name="tipcomp" id="tipcomp" />
                    </td>
                    <td class="saludo1" colspan="3">Concepto:</td>
                    <td colspan="2"><input type="text" name="concepto" id="concepto"  style="width:100%;" onKeyUp="return tabular(event,this)" value="<?php echo $_POST['concepto']; ?>"></td>
                    <td class="saludo1">Reversi&oacute;n:</td>
                    <td>
                        <input type="text" name="estadoRev" id="estadoRev"  value="<?php echo $_POST['estadoRev']; ?>" ondblclick="redirect()" readonly>
                    </td>
                </tr>
            </table>
            <?php
            if ($estadoFijo != 'S') {
                ?>
                    <table class="inicio">
                        <tr>
                            <td class="titulos2" colspan="30">Agregar Detalle</td>
                        </tr>
                        <tr>
                            <td class="saludo1" style="width:10%;">Cuenta:</td>
                            <td valign="middle" style="width:12%;">
                                <input type="text" id="cuenta" name="cuenta" style="width:80%;" onKeyPress="javascript:return solonumeros(event)" onKeyUp="return tabular(event,this)" onBlur="buscacta(event)" value="<?php echo $_POST['cuenta'] ?>"  onKeyDown="llamadoesc(event,'1')"/><input type="hidden" value="0" name="bc"/>&nbsp;<a onClick="despliegamodal2('visible','2');" style='cursor:pointer;'><img src="imagenes/find02.png" style="width:20px;"/></a>
                            </td>
                            <td style="width:30%;">
                                <input type="text" name="ncuenta" id="ncuenta" value="<?php echo $_POST['ncuenta'] ?>" style="width:100%;" readonly>
                            </td>
                            <td class="saludo1" style="width:8%;">Tercero:</td>
                            <td style="width:12%;">
                                <input type="text" id="tercero" name="tercero" style="width:80%;" onKeyUp="return tabular(event,this)" onBlur="buscater(event)" value="<?php echo $_POST['tercero'] ?>"onKeyDown="llamadoesc(event,'2')">
                                <input type="hidden" value="0" name="bt" id="bt">
                                <a onClick="despliegamodal2('visible','1');" style='cursor:pointer;'><img src="imagenes/find02.png" style="width:20px;"></a>
                            </td>
                            <td>
                                <input type="text" name="ntercero" id="ntercero" value="<?php echo $_POST['ntercero'] ?>" style="width:100%" readonly/>
                            </td>
                        </tr>
                        <tr>
                            <td width="101" class="saludo1">Centro Costo:</td>
                            <td>
                                <input type="text" id="cc" name="cc" onKeyUp="return tabular(event,this)" onBlur="buscacc(event)" value="<?php echo $_POST['cc'] ?>" onKeyDown="llamadoesc(event,'3')" style="width:80%"/><input type="hidden" value="0" name="bcc">&nbsp;<a onClick="despliegamodal2('visible','3');" style='cursor:pointer;'><img src="imagenes/find02.png" style="width:20px;"></a>
                            </td>
                            <td>
                                <input name="ncc" type="text" id="ncc" value="<?php echo $_POST['ncc'] ?>" style="width:100%" readonly>
                            </td>
                            <td width="115" class="saludo1">Detalle:</td>
                            <td colspan="2">
                                <input type="text" name="detalle" id="detalle" style="width:100%" onKeyUp="return tabular(event,this)" value="<?php echo $_POST['detalle'] ?>" o|nClick="document.getElementById('detalle').focus();document.getElementById('detalle').select();">
                            </td>
                        </tr>
                        <tr>
                            <td class="saludo1">Vlr Debito:</td>
                            <td colspan="2">
                                <input type="text" name="vlrdeb" onKeyPress="javascript:return solonumeros(event)" onKeyDown="return tabular(event,this)"  value="0" onClick="document.getElementById('vlrdeb').focus();document.getElementById('vlrdeb').select();">
                            </td>

                            <td width="115" class="saludo1">Vlr Credito:</td>
                            <td width="140">
                                <input type="text" name="vlrcre"  onKeyPress="javascript:return solonumeros(event)" onKeyDown="return tabular(event,this)" value="0" onClick="document.getElementById('vlrcre').focus();document.getElementById('vlrcre').select();">
                            </td>

                            <td colspan="2">
                                <input type="button" name="agrega" value="  Agregar  " onClick="agregardetalle()" ><input type="hidden" value="0" name="agregadet">
                            </td>
                        </tr>
                    </table>
                <?php
            }
            ?>

            <?php if (!$_POST['oculto']) {
                echo "<script>document.form2.tipocomprobante.focus();</script>";
            } ?>
            <input type="hidden" value="0" name="oculto" >
            <?php
            if ($_POST['estadoc'] == 'ACTIVO') {
                echo "
					<script>
						document.getElementById('estadoc').style.backgroundColor='green';
						document.getElementById('estadoc').style.color='#fff';
					</script>";
            }
            if ($_POST['estadoc'] == 'ANULADO') {
                echo "
						<script>
							document.getElementById('estadoc').style.backgroundColor='red';
							document.getElementById('estadoc').style.color='#fff';
						</script>";
            }
            if ($_POST['estadoRev'] != '') {
                echo "
						<script>
							document.getElementById('estadoRev').style.backgroundColor='red';
							document.getElementById('estadoRev').style.color='#fff';
							document.getElementById('estadoRev').style.cursor='pointer';
						</script>";
            }
            if ($_POST['estadoc'] == '') {
                echo "
						<script>
							document.getElementById('estadoc').style.backgroundColor='';
							document.getElementById('estadoc').style.color='#fff';
						</script>";
            }
            if ($_POST['bc'] == '1') {
                $nresul = buscacuenta($_POST['cuenta']);
                if ($nresul != '') {
                    echo "<script>document.getElementById('ncuenta').value='$nresul';document.getElementById('tercero').focus(); document.getElementById('tercero').select();</script>";
                } else {
                    $_POST['ncuenta'] = "";
                    echo "<script>document.getElementById('valfocus').value='2';despliegamodalm('visible','2','Cuenta Incorrecta');</script>";
                }
            }
            //***** busca tercero
            if ($_POST['bt'] == '1') {
                $nresul = buscatercero($_POST['tercero']);
                if ($nresul != '') {
                    echo "<script>document.getElementById('ntercero').value='$nresul';document.getElementById('cc').focus(); document.getElementById('cc').select();</script>";
                } else {
                    echo "<script>despliegamodalm('visible','4','Tercero Incorrecto o no Existe, ï¿½Desea Agregar un Tercero?','1');</script>";
                }
            }
            //*** centro  costo
            if ($_POST['bcc'] == '1') {
                $nresul = buscacentro($_POST['cc']);
                if ($nresul != '') {
                    echo "<script>document.getElementById('ncc').value='$nresul';document.getElementById('detalle').focus(); document.getElementById('detalle').select();</script>";
                } else {
                    echo "<script>document.getElementById('valfocus').value='3';despliegamodalm('visible','2','Centro de Costos Incorrecto');</script>";
                }
            }
            ?>
            <div class="subpantallac2" style="height:47.2%; width:99.6%; overflow-x:hidden;resize: vertical;">
                <table class="inicio" width="99%">
                    <tr><td class="titulos" colspan="20">Detalle Comprobantes </td></tr>
                    <tr>
                        <td class="titulos2" style='width:8%;'>Cuenta</td>
                        <td class="titulos2" style='width:17%;'>Nombre Cuenta</td>
                        <td class="titulos2" style='width:8%;'>Tercero</td>
                        <td class="titulos2" style='width:20%;'>Nom Tercero</td>
                        <td class="titulos2" style='width:3%;'>CC</td>
                        <td class="titulos2">Detalle</td>
                        <td class="titulos2" style='width:10%;'>Vlr Debito</td>
                        <td class="titulos2" style='width:10%;'>Vlr Credito</td>
                        <td class="titulos2" style='width:3%;'><img src="imagenes/del.png"></td>
                    </tr>
                        <input type='hidden' name='elimina' id='elimina'>
                        <input type='hidden' name='elidet' id='elidet' value="0">
                    <?php
                    if ($_POST['elidet'] == '1') {
                        $posi = $_POST['elimina'];
                        $cuentacred = 0;
                        $cuentadeb = 0;
                        $diferencia = 0;
                        unset($_POST['dcuentas'][$posi]);
                        unset($_POST['dncuentas'][$posi]);
                        unset($_POST['dterceros'][$posi]);
                        unset($_POST['dnterceros'][$posi]);
                        unset($_POST['dccs'][$posi]);
                        unset($_POST['ddetalles'][$posi]);
                        unset($_POST['dcheques'][$posi]);
                        unset($_POST['dcreditos'][$posi]);
                        unset($_POST['ddebitos'][$posi]);
                        $_POST['dcuentas'] = array_values($_POST['dcuentas']);
                        $_POST['dncuentas'] = array_values($_POST['dncuentas']);
                        $_POST['dterceros'] = array_values($_POST['dterceros']);
                        $_POST['dnterceros'] = array_values($_POST['dnterceros']);
                        $_POST['dccs'] = array_values($_POST['dccs']);
                        $_POST['ddetalles'] = array_values($_POST['ddetalles']);
                        $_POST['dcheques'] = array_values($_POST['dcheques']);
                        $_POST['dcreditos'] = array_values($_POST['dcreditos']);
                        $_POST['ddebitos'] = array_values($_POST['ddebitos']);
                    }
                    if ($_POST['agregadet'] == '1') {
                        $cuentacred = 0;
                        $cuentadeb = 0;
                        $diferencia = 0;
                        $_POST['dcuentas'][] = $_POST['cuenta'];
                        $_POST['dncuentas'][] = $_POST['ncuenta'];
                        $_POST['dterceros'][] = $_POST['tercero'];
                        $_POST['dnterceros'][] = $_POST['ntercero'];
                        $_POST['dccs'][] = $_POST['cc'];
                        $_POST['ddetalles'][] = $_POST['detalle'];
                        $_POST['dcheques'][] = $_POST['cheque'];
                        $_POST['vlrcre'] = str_replace(",", ".", $_POST['vlrcre']);
                        $_POST['vlrdeb'] = str_replace(",", ".", $_POST['vlrdeb']);
                        $_POST['dcreditos'][] = round($_POST['vlrcre'], 2);
                        $_POST['ddebitos'][] = round($_POST['vlrdeb'], 2);
                        $_POST['agregadet'] = 0;
                        echo "
								<script>
									document.form2.cuenta.value='';
									document.form2.ncuenta.value='';
									document.form2.tercero.value='';
									document.form2.ntercero.value='';
									document.form2.cc.value='';
									document.form2.ncc.value='';
									document.form2.detalle.value='';
									document.form2.vlrcre.value=0;
									document.form2.vlrdeb.value=0;
									document.form2.cuenta.focus();
									document.form2.cuenta.select();
								</script>";
                    }
                    $co = "saludo1a";
                    $co2 = "saludo2";
                    $_POST['cuentadeb'] = 0;
                    $_POST['cuentacred'] = 0;
                    $_POST['diferencia'] = 0;
                    $_POST['diferencia2'] = 0;
                    $cuentacred = 0;
                    $cuentadeb = 0;
                    $diferencia = 0;
                    for ($x = 0; $x < count($_POST['dcuentas']); $x++) {
                        $lectura = "readonly ";
                        if ($_POST['tipocomprobante'] == "16" || $_POST['tipocomprobante'] == "102") {
                            $lectura = "'";
                        }
                        echo "
							<tr class='$co'>
								<input type='hidden' name='dcuentas[]' value='" . $_POST['dcuentas'][$x] . "'/>
								<input type='hidden' name='dncuentas[]' value='" . ucfirst(strtolower($_POST['dncuentas'][$x])) . "'/>
								<input type='hidden' name='dterceros[]' value='" . $_POST['dterceros'][$x] . "'/>
								<input type='hidden' name='dnterceros[]' value='" . buscatercero($_POST['dterceros'][$x]) . "'/>
								<input type='hidden' name='dccs[]' value='" . $_POST['dccs'][$x] . "'/>
								<input type='hidden' name='ddetalles[]' value='" . ucfirst(strtolower($_POST['ddetalles'][$x])) . "'/>

								<td>" . $_POST['dcuentas'][$x] . "</td>
								<td>" . ucfirst(strtolower($_POST['dncuentas'][$x])) . "</td>
								<td>" . $_POST['dterceros'][$x] . "</td>
								<td>" . buscatercero($_POST['dterceros'][$x]) . "</td>
								<td>" . $_POST['dccs'][$x] . "</td>
								<td>" . ucfirst(strtolower($_POST['ddetalles'][$x])) . "</td>
								<td><input type='text' name='ddebitos[]' value='" . $_POST['ddebitos'][$x] . "' class='inpnovisibles' onKeyUp='return tabular(event,this)' onKeyPress='javascript:return solonumeros(event)' onChange='calcular();' $lectura style='width:100%;text-align:right;'/></td>
								<td><input type='text' name='dcreditos[]' value='" . $_POST['dcreditos'][$x] . "' class='inpnovisibles'  onKeyUp='return tabular(event,this)' onKeyPress='javascript:return solonumeros(event)' onChange='calcular();' $lectura style='width:100%; text-align:right;'/></td>
								<td ><a href='#' onclick='eliminar($x)'><img src='imagenes/del.png'></a></td>
							</tr>";
                        $cred = $_POST['dcreditos'][$x];
                        $deb = $_POST['ddebitos'][$x];
                        $cred = $cred;
                        $deb = $deb;
                        $cuentacred = doubleVal($cuentacred) + doubleVal($cred);
                        $cuentadeb = doubleVal($cuentadeb) + doubleVal($deb);
                        $diferencia = doubleVal($cuentadeb) - doubleVal($cuentacred);
                        $total = number_format(doubleVal($total), 2, ",", "");
                        $_POST['diferencia'] = $diferencia;
                        $_POST['diferencia2'] = number_format(doubleVal($diferencia), 2, ".", ",");
                        $_POST['cuentadeb'] = number_format(doubleVal($cuentadeb), 2, ".", ",");
                        $_POST['cuentadeb2'] = $cuentadeb;
                        $_POST['cuentacred'] = number_format(doubleVal($cuentacred), 2, ".", ",");
                        $_POST['cuentacred2'] = $cuentacred;
                        $aux = $co;
                        $co = $co2;
                        $co2 = $aux;
                    }
                    echo "
						<tr>
							<td></td>
							<td></td>
							<td >Diferencia:</td>
							<td colspan='2'><input type='hidden' id='diferencia' name='diferencia' value='" . $_POST['diferencia'] . "'><input id='diferencia2' name='diferencia2' value='" . $_POST['diferencia2'] . "' type='text' readonly></td>
							<td>Totales:</td>
							<td class='saludo2'><input name='cuentadeb2' type='hidden' id='cuentadeb2' value='" . $_POST['cuentadeb2'] . "'><input name='cuentadeb' id='cuentadeb' value='" . $_POST['cuentadeb'] . "' readonly style='width:100%;text-align:right;'/></td>
							<td class='saludo2'><input id='cuentacred' name='cuentacred' value='" . $_POST['cuentacred'] . "' readonly style='width:100%;text-align:right;'/><input id='cuentacred2' type='hidden' name='cuentacred2' value='" . $_POST['cuentacred2'] . "' ></td>
						</tr>";
                    ?>
                </table>
            </div>
            <?php
            //********** GUARDAR EL COMPROBANTE ***********
            if ($_POST['oculto'] == '3') {
                $fechaf = $_POST['fecha'];
                $bloq = bloqueos($_SESSION['cedulausu'], $fechaf);
                if ($bloq >= 1) {
                    $sqlr = "SELECT fijo FROM tipo_comprobante WHERE codigo='" . $_POST['tipocomprobante'] . "'";
                    $res2 = mysqli_query($linkbd, $sqlr);
                    $rt = mysqli_fetch_row($res2);
                    if ($rt[0] == 'N') //**** validacion tipo comprobante
                    {
                        $fechaf = $_POST['fecha'];
                        $sqlr = "update comprobante_cab set numerotipo = '" . $_POST['ncomp'] . "', tipo_comp = '" . $_POST['tipocomprobante'] . "', fecha = '$fechaf', concepto = '" . $_POST['concepto'] . "',total = 0, total_debito = '" . $_POST['cuentadeb2'] . "', total_credito = '" . $_POST['cuentadeb2'] . "', diferencia = '" . $_POST['diferencia'] . "', estado = '" . $_POST['estado'] . "' WHERE tipo_comp = '" . $_POST['tipocomprobante'] . "' AND numerotipo = '" . $_POST['ncomp'] . "'";
                        if (!mysqli_query($linkbd, $sqlr)) {
                            echo "<table><tr><td class='saludo1'><center><font color=blue>Manejador de Errores de la Clase BD<br><font size=1></font></font><br><p align=center>No se pudo ejecutar la peticiï¿½n: <br><font color=red><b>$sqlr</b></font></p>Ocurri? el siguiente problema:<br><pre></pre></center></td></tr></table>";
                        } else {
                            echo "<script>despliegamodalm('visible','3','Se ha modificado el encabezado con Exito');</script>";
                            $sqlr = "delete from comprobante_det where id_comp='" . $_POST['tipocomprobante'] . " " . $_POST['ncomp'] . "'";
                            mysqli_query($linkbd, $sqlr);
                            for ($x = 0; $x < count($_POST['dcuentas']); $x++) {
                                $sqlr = "insert into comprobante_det (id_comp, cuenta, tercero, centrocosto, detalle, cheque, valdebito, valcredito, estado, vigencia) values ('" . $_POST['tipocomprobante'] . " " . $_POST['ncomp'] . "', '" . $_POST['dcuentas'][$x] . "', '" . $_POST['dterceros'][$x] . "', '" . $_POST['dccs'][$x] . "', '" . $_POST['ddetalles'][$x] . "',''," . $_POST['ddebitos'][$x] . ", " . $_POST['dcreditos'][$x] . ", '1', '$vigusu')";
                                if (!mysqli_query($linkbd, $sqlr)) {
                                    echo "<table ><tr><td class='saludo1'><center><font color=blue>Manejador de Errores de la Clase BD<br><font size=1></font></font><br><p align=center>No se pudo ejecutar la peticiï¿½n: <br><font color=red><b>$sqlr</b></font></p>";
                                    $e = mysqli_error($respquery);
                                    echo "Ocurrio el siguiente problema:<br><pre></pre></center></td></tr></table>";
                                } else {
                                    echo "<script>despliegamodalm('visible','3','Se ha modificado el comprobante con Exito ');</script>";
                                }
                            }
                        }
                    }//**** fin validacion tipo comprobante
                } else {
                    echo "<script>despliegamodalm('visible','2','No Tiene los Permisos para Modificar este Documento');</script>";
                }
            }
            //DUPLICAR LOS COMPROBANTES MANUALES
            if ($_POST['oculto'] == '4' && $_POST['duplicar'] == '2') {
                $fechaf = $_POST['fecha'];
                $bloq = bloqueos($_SESSION['cedulausu'], $fechaf);
                if ($bloq >= 1) {
                    $sqlr = "select fijo from tipo_comprobante where codigo = '" . $_POST['tipocomprobante'] . "'";
                    $res2 = mysqli_query($linkbd, $sqlr);
                    $rt = mysqli_fetch_row($res2);
                    if ($rt[0] == 'N') //**** validacion tipo comprobante
                    {
                        $sqlr = "select * from comprobante_cab where tipo_comp = '" . $_POST['tipocomprobante'] . "'  ORDER BY numerotipo DESC";
                        $res = mysqli_query($linkbd, $sqlr);
                        $r = mysqli_fetch_row($res);
                        $nuevocomp = $r[1] + 1;
                        $fechaf = $_POST['fecha'];
                        $sqlr = "insert into comprobante_cab (numerotipo, tipo_comp, fecha, concepto, total, total_debito, total_credito, diferencia, estado) values ('$nuevocomp', '" . $_POST['tipocomprobante'] . "', '$fechaf', '" . $_POST['concepto'] . "', 0, '" . $_POST['cuentadeb2'] . "', '" . $_POST['cuentacred2'] . "', '" . $_POST['diferencia'] . "', '" . $_POST['estado'] . "')";
                        if (!mysqli_query($linkbd, $sqlr)) {
                            echo "<table><tr><td class='saludo1'><center><font color=blue>Manejador de Errores de la Clase BD<br><font size=1></font></font><br><p align=center>No se pudo ejecutar la petici?n: <br><font color=red><b>$sqlr</b></font></p>Ocurri? el siguiente problema:<br><pre></pre></center></td></tr></table>";
                        } else {
                            echo "<script>despliegamodalm('visible','3','Se ha Duplicado el Comprobante con Exito');</script>";
                            for ($x = 0; $x < count($_POST['dcuentas']); $x++) {
                                $sqlr = "insert into comprobante_det (id_comp, cuenta, tercero, centrocosto, detalle, cheque, valdebito, valcredito, estado, vigencia) values ('" . $_POST['tipocomprobante'] . " $nuevocomp', '" . $_POST['dcuentas'][$x] . "', '" . $_POST['dterceros'][$x] . "', '" . $_POST['dccs'][$x] . "', '" . $_POST['ddetalles'][$x] . "', '', " . $_POST['ddebitos'][$x] . ", " . $_POST['dcreditos'][$x] . ", '1', '" . $vigusu . "')";
                                if (!mysqli_query($linkbd, $sqlr)) {
                                    echo "<table ><tr><td class='saludo1'><center><font color=blue>Manejador de Errores de la Clase BD<br><font size=1></font></font><br><p align=center>No se pudo ejecutar la petici?n: <br><font color=red><b>$sqlr</b></font></p>";
                                    $e = mysqli_error($respquery);
                                    echo "Ocurrio el siguiente problema:<br>pre></pre></center></td></tr></table>";
                                } else {
                                    $_POST['oculto'] = '1';
                                    $_POST['duplicar'] = '';
                                    echo "
											<script>
												document.form2.oculto.value=1;
												setTimeout (\"alert ('Duplicando');\", 8000);
												document.form2.submit();
											</script>";
                                }
                            }
                        }
                    }//**** fin validacion tipo comprobante
                } else {
                    echo "<script>despliegamodalm('visible','2','No Tiene los Permisos para Modificar este Documento');</script>";
                }
            }
            ?>
            <div id="bgventanamodal2">
                <div id="ventanamodal2">
                    <IFRAME  src="" name="buster" marginWidth=0 marginHeight=0 frameBorder=0 id="ventana2" frameSpacing=0 style="left:500px; width:900px; height:500px; top:200;">
                    </IFRAME>
                </div>
            </div>
        </form>
    </body>
</html>
