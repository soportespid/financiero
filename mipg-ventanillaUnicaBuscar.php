<?php
	header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=utf8");
	require 'comun.inc';
	require 'funciones.inc';
	session_start();
	if(empty($_SESSION)){
		header("location: index.php");
	}
	$linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");
	cargarcodigopag($_GET['codpag'],$_SESSION["nivel"]);
	date_default_timezone_set("America/Bogota");
?>
<!DOCTYPE >
<html lang=es>
	<head>
		<meta charset="utf-8"/>
		<meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1"/>
		<meta name="viewport" content="user-scalable=no">
		<title>:: IDEAL 10 - Herramientas MIPG</title>
		<link href="favicon.ico" rel="shortcut icon"/>
		<link href="css/css2z.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
		<link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
		<link href="css/css4.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
		<link href="css/style.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css"/>
		<link rel="stylesheet" href="sweetalert2/dist/sweetalert2.min.css">
		<script src="sweetalert2/dist/sweetalert2.min.js"></script>
		<script type="text/javascript" src="css/programas.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script src="css/calendario.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script>
			function updateFecha2() { 
				const fecha1 = document.getElementById('fc_1198971545').value
				if (document.getElementById('fc_1198971545').value) { 
					const [day, month, year] = fecha1.split('/').map(Number); 
					if (!isNaN(day) && !isNaN(month) && !isNaN(year)) { 
						const firstDay = new Date(year, month - 1, 1); 
						const lastDay = new Date(year, month, 0);  
						document.getElementById('fc_1198971546').value = lastDay.toLocaleDateString('es-ES', { day: '2-digit', month: '2-digit', year: 'numeric' });
					} 
				} 
			}
		</script>
	</head>
	<body>
		<section id="myapp" v-cloak>
			<div class="loading-container" v-show="isLoading" >
				<p class="text-loading"data-loading="IDEAL 10">IDEAL 10</p>
			</div>
			<div class="main-container">
				<header>
					<table>
						<tr><script>barra_imagenes("meci");</script><?php cuadro_titulos();?></tr>
					</table>
				</header>
				<nav>
					<?php menu_desplegable("meci");?>
					<div class="bg-white group-btn p-1">
						<button type="button" onclick="location.href='mipg-ventanillaUnicaCrear.php'" class="btn btn-white btn-primary-hover d-flex justify-between align-items-center">
							<span>Nuevo</span>
							<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 -960 960 960"><path d="M440-440H200v-80h240v-240h80v240h240v80H520v240h-80v-240Z"></path></svg>
						</button>
						<button type="button" onclick="location.href='mipg-ventanillaUnicaBuscar.php'" class="btn btn-white btn-primary-hover d-flex justify-between align-items-center">
							<span>Buscar</span>
							<svg class="fill-black group-hover:fill-white w-5 h-5" xmlns="http://www.w3.org/2000/svg" viewBox="0 -960 960 960"><path d="M784-120 532-372q-30 24-69 38t-83 14q-109 0-184.5-75.5T120-580q0-109 75.5-184.5T380-840q109 0 184.5 75.5T640-580q0 44-14 83t-38 69l252 252-56 56ZM380-400q75 0 127.5-52.5T560-580q0-75-52.5-127.5T380-760q-75 0-127.5 52.5T200-580q0 75 52.5 127.5T380-400Z"></path></svg>
						</button>
						<button type="button" @click="mypop=window.open('meci-principal.php','',''); mypop.focus();" class="btn btn-white btn-primary-hover d-flex justify-between align-items-center">
							<span>Nueva ventana</span>
							<svg class="fill-black group-hover:fill-white w-5 h-5" xmlns="http://www.w3.org/2000/svg" viewBox="0 -960 960 960"><path d="M320-120v-80h80v-80H160q-33 0-56.5-23.5T80-360v-400q0-33 23.5-56.5T160-840h640q33 0 56.5 23.5T880-760v400q0 33-23.5 56.5T800-280H560v80h80v80H320ZM160-360h640v-400H160v400Zm0 0v-400 400Z"></path></svg>
						</button>
						<button type="button" @click="mypop=window.open('mipg-ventanillaUnicaBuscar.php','','');mypop.focus();" class="btn btn-white btn-primary-hover d-flex justify-between align-items-center">
							<span class="group-hover:text-white">Duplicar pantalla</span>
							<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 -960 960 960"><path d="M640-160v-360H160v360h480Zm80-200v-80h80v-360H320v200h-80v-200q0-33 23.5-56.5T320-880h480q33 0 56.5 23.5T880-800v360q0 33-23.5 56.5T800-360h-80ZM160-80q-33 0-56.5-23.5T80-160v-360q0-33 23.5-56.5T160-600h480q33 0 56.5 23.5T720-520v360q0 33-23.5 56.5T640-80H160Zm400-603ZM400-340Z"></path></svg>
						</button>
					</div>
				</nav>
				<article>
					<div class="bg-white">
						<h2 class="titulos m-0">.: Buscar documentos radicados</h2>
						<div class="w-100">
							<div class="d-flex w-100">
								<div class="form-control w-25">
									<label class="form-label" for="">Fecha Inicial:</label>
									<input type="text" name="fecha" value="<?php echo $_POST['fecha']?>" onKeyUp="return tabular(event,this)" id="fc_1198971545" title="DD/MM/YYYY" onDblClick="displayCalendarFor('fc_1198971545');" class="colordobleclik" autocomplete="off" onchange="updateFecha2()" readonly>
								</div>
								<div class="form-control w-25">
									<label class="form-label" for="">Fecha Final:</label>
									<input type="text" name="fecha2" value="<?php echo $_POST['fecha2']?>" onKeyUp="return tabular(event,this)" id="fc_1198971546" title="DD/MM/YYYY" onDblClick="displayCalendarFor('fc_1198971546');" class="colordobleclik" autocomplete="off" onChange="" readonly>
								</div>
								<div class="form-control w-25">
									<label class="form-label" for="">Estado:</label>
									<select id="labelselectTipopqr" v-model="selectEstado" @change="searchx">
										<option value="0" selected>Todos</option>
										<option value="1" selected>Pendientes</option>
										<option value="2" selected>Contestados</option>
										<option value="3" selected>Anulados</option>
										<option value="4" selected>Vencidos</option>
										<option value="5" selected>Solo Lectura</option>
										
									</select>
								</div>
								<div class="form-control justify-between w-25">
									<label for=""></label>
									<div class="d-flex">
										<button type="button" class="btn btn-primary" @click="cargarInfo()">Buscar</button>
									</div>
								</div>
							</div>
							<div class="d-flex w-100">
								<div class="form-control ">
									<label class="form-label" for="">Tercero - Descripción:</label>
									<input type="search" placeholder="buscar" v-model="txtSearch" @keyup="searchx">
								</div>
								<div class="form-control" >
									<label class="form-label" for="">Dependencia:</label>
									<input type="text" class="colordobleclik" @dblclick="isModal=true" v-model="objDependencia.nombre" readonly>
									
								</div>
								<div class="form-control" style="width: 5%;">
								<label class="form-label" for="">&nbsp;</label>
									<button type="button" class="btn btn-primary"  @click="clearDependencia">Limpiar</button>
								</div>
							</div>
						</div>
						<h2 class="titulos m-0">.: Resultados: {{ txtResults }} </h2>
						<div class="overflow-auto max-vh-50 overflow-x-hidden p-2" ref="tableContainer">
							<table class="table table-hover fw-normal">
								<thead>
									<tr>
										<th class="text-center">N° Radicado</th>
										<th class="text-center">Fecha Raricado</th>
										<th class="text-center">Fecha Vencimiento</th>
										<th class="text-center">Fecha Respuesta</th>
										<th class="text-center">Tercero</th>
										<th class="text-center">Descripción</th>
										<th class="text-center">Proceso</th>
										<th class="text-center">Estado</th>
										<th class="text-center">Acción</th>
									</tr>
								</thead>
								<tbody>
									<tr v-for="(data, index) in arrDataCopy" :key="index" v-bind:class="{ 'bg-yellow': data.is_yellow == 1 }" ref="highlightedRow" @dblclick="editItem(data.numeror)">
										<td class="text-center">{{ data.numeror }}</td>
										<td class="text-center">{{ data.fRadicado }}</td>
										<td class="text-center">{{ data.fLimite}}</td>
										<td class="text-center">{{ data.fRespuesta }}</td>
										<td>{{ data.nomtercero }}</td>
										<td>{{ data.descripcionr }}</td>
										<td class="text-center">
											<span :class="[data.estado2 === '0' ? 'badge-warning' : data.estado2 === '2' ? 'badge-success' : data.estado2 === '3' ? 'badge-danger' : '']" class="badge">
												{{ data.estado2 === '0' ? 'En proceso' : data.estado2 === '2' ? 'Concluida' : data.estado2 === '3' ? 'Anulada' : '' }}
											</span>
										</td>
										<td class="text-center">
											<span :class="[data.estadoTiempo === '0' ? 'badge-primary' : data.estadoTiempo === '1' ? 'badge-success' : data.estadoTiempo === '2' ? 'badge-danger' : '']" class="badge">
												{{ data.estadoTiempo === '0' ? 'Sin tiempo Limite' : data.estadoTiempo === '1' ? 'Denetro de terminos' : data.estadoTiempo === '2' ? 'Fecha vencida' : '' }}
											</span>
										</td>
										<td class="text-center">
											<button type="button" class="btn btn-danger btn-sm m-1" @click="anularRadicado(data.numeror)">
												Anular
											</button>
										</td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
				</article>
				<!-- MODALES -->
				<div v-show="isModal" class="modal">
					<div class="modal-dialog modal-lg" >
						<div class="modal-content">
							<div class="modal-header">
								<h5 class="modal-title">Buscar Dependencias</h5>
								<button type="button" @click="isModal=false;" class="btn btn-close"><div></div><div></div></button>
							</div>
							<div class="modal-body">
								<div class="d-flex flex-column">
									<div class="form-control m-0 mb-3">
										<input type="search" placeholder="Buscar" v-model="txtBuscar" @keyup="search()" id="labelInputName">
									</div>
									<div class="form-control m-0 mb-3">
										<label class="form-label" for="labelInputName">Resultados: <span class="fw-bold">{{txtResultados}}</span></label>
									</div>
								</div>
								<div class="overflow-auto max-vh-50 overflow-x-hidden" >
									<table class="table table-hover fw-normal">
										<thead>
											<tr>
												<th>Item</th>
												<th>Código</th>
												<th>Nombre</th>
											</tr>
										</thead>
										<tbody>
											<tr v-for="(data,index) in arrDependenciasCopy" @dblclick="selectItem(data)" :key="index">
												<td>{{index+1}}</td>
												<td>{{data.codigo}}</td>
												<td>{{data.nombre}}</td>
											</tr>
										</tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
		<script src="node_modules/read-excel-file/bundle/read-excel-file.min.js"></script>
		<script src="node_modules/xlsx/dist/xlsx.full.min.js"></script>
		<script src="Librerias/vue/vue.min.js"></script>
		<script src="Librerias/vue/axios.min.js"></script>
		<script type="module" src="vue/herramientas_mipg/ventanilla_unica/buscar/mipg-ventanillaUnicaBuscar.js?<?= date('d_m_Y_h_i_s');?>"></script>
	</body>
</html>