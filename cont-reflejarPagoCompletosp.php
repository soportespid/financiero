<?php
	header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=utf8");
	require 'comun.inc';
	require 'funciones.inc';
	session_start();
	$linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");
	cargarcodigopag($_GET['codpag'],$_SESSION["nivel"]);
	date_default_timezone_set("America/Bogota");
    ini_set('max_execution_time',99999999);
	titlepag();
?>
<!DOCTYPE >
<html lang=es>
	<head>
		<meta charset="utf-8"/>
		<meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1"/>
		<meta name="viewport" content="user-scalable=no">
		<title>:: IDEAL 10 - Servicios Públicos</title>
		<link href="css/css2z.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
		<link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
		<link href="css/css4.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
		<link href="css/tabs.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css"/>
		<script src="vue/vue.min.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script src="css/programas.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script src="css/calendario.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<style>
			input[type=number]::-webkit-inner-spin-button,
			input[type=number]::-webkit-outer-spin-button { 
				-webkit-appearance: none; 
				margin: 0; 
			}
			input[type=number]{
				text-align:right;
				box-sizing: border-box;
				font: 120% sans-serif;
				width: 100% !important;
			}
			[v-cloak]{display : none;}
		</style>
		<script src="sweetalert2/dist/sweetalert2.min.js"></script>
		<link rel="stylesheet" href="sweetalert2/dist/sweetalert2.min.css">
	</head>
	<body>
		<header>
			<table>
				<tr><script>barra_imagenes("serv");</script><?php cuadro_titulos();?></tr>
			</table>
		</header>
		<form name="form2" method="post" action="">
			<section id="myapp" v-cloak >
				<nav>
					<table>
						<tr><?php menu_desplegable("serv");?></tr>
						<tr>
							<td colspan="3" class="cinta">
								<img src="imagenes/add2.png"  class="mgbt1" title="Nuevo">
								<img src="imagenes/guardad.png" title="Guardar"  class="mgbt1">
								<img src="imagenes/buscad.png" class="mgbt1" title="Buscar">
								<img src="imagenes/nv.png" onClick="mypop=window.open('serv-principal.php','',''); mypop.focus();" class="mgbt" title="Nueva Ventana">
								<img src="imagenes/facturasp.png" class="mgbt1" title="Reflejar" v-on:click="">
							</td>
						</tr>
					</table>
				</nav>
				<article>
					<table class="inicio ancho">
						<tr>
							<td class="titulos" colspan="6" >Reflejar masivo recaudo completo</td>
							<td class="cerrar" style="width:7%" onClick="location.href='serv-principal.php'">Cerrar</td>
						</tr>

						<tr>
							<td class="tamano01" style="width: 10%;">Fecha inicio: </td>
							<td style="width: 15%;">
                                <input type="fechaIni" name="fechaIni" id="fechaIni" onchange="" value="<?php echo @$_POST['fechaIni'] ?>" maxlength="10" onKeyUp="return tabular(event,this)"     id="fechaIni" onKeyDown="mascara(this,'/',patron,true)" title="DD/MM/YYYY" style="height:30px;text-align:center;width:100%;" onClick="displayCalendarFor('fechaIni');" class="colordobleclik" readonly />
							</td>

                            <td class="tamano01" style="width: 10%;">Fecha final: </td>
							<td style="width: 15%;">
                                <input type="text" name="fechaFin" id="fechaFin" onchange="" value="<?php echo @$_POST['fechaFin'] ?>" maxlength="10" onKeyUp="return tabular(event,this)" id="fechaFin" onKeyDown="mascara(this,'/',patron,true)" title="DD/MM/YYYY" style="height:30px;text-align:center;width:100%;" onClick="displayCalendarFor('fechaFin');" class="colordobleclik" readonly />
							</td>

							<td colspan="2" style="padding-bottom:0px">
								<em class="botonflechaverde" v-on:Click="buscarDatos">Mostrar recaudos</em>
							</td>
						</tr>   
					</table>

                    <div class='subpantalla' style='height:66vh; width:99.2%; margin-top:0px; overflow:hidden'>
						<table class='tablamv'>
							<thead>
								<tr style="text-align:Center;">
									<th class="titulosnew00" style="width:10%;">Código recaudo</th>
									<th class="titulosnew00" style="width:10%;">Código de usuario</th>
									<th class="titulosnew00" >Suscriptor</th>
									<th class="titulosnew00" style="width:10%;">Número de factura</th>
									<th class="titulosnew00" style="width:10%;">Fecha de recaudo</th>
									<th class="titulosnew00" style="width:10%;">Medio de pago</th>
									<th class="titulosnew00" style="width:10%;">Cuenta contable</th>
									<th class="titulosnew00" style="width:1%;"></th>
								</tr>
							</thead>

							<tbody>
								<tr v-for="(recaudo,index) in recaudos" v-bind:class="index % 2 ? 'saludo1a' : 'saludo2'">
									<td style="padding-left:10px; text-align:center; width: 10%;">{{ recaudo[0] }}</td>
									<td style="padding-left:10px; text-align:center; width: 10%;">{{ recaudo[1] }}</td>
									<td style="padding-left:10px;">{{ recaudo[2] }}</td>
									<td style="padding-left:10px; text-align:center; width: 10%;">{{ recaudo[3] }}</td>
									<td style="padding-left:10px; text-align:center; width: 10%;">{{ recaudo[4] }}</td>
									<td style="padding-left:10px; text-align:center; width: 10%;">{{ recaudo[5] }}</td>
									<td style="padding-left:10px; text-align:center; width: 10%;">{{ recaudo[6] }}</td>
								</tr>
							</tbody>
						</table>
					</div>

					<div id="cargando" v-if="loading" class="loading">
						<span>Cargando...</span>
					</div>

				</article>
			</section>
		</form>

		<script src="Librerias/vue/vue.min.js"></script>
		<script src="Librerias/vue/axios.min.js"></script>
		<script src="servicios_publicos/refacturacion/cont-reflejarPagoMasivos.js"></script>
        
	</body>
</html>