<?php
	header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=utf8");
	require "comun.inc";
	require "funciones.inc";
	require "funcionesSP.inc.php";
	session_start();
	$linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");
	cargarcodigopag($_GET['codpag'],$_SESSION["nivel"]);
	date_default_timezone_set("America/Bogota");
	$scroll = $_GET['scrtop'];
	$totreg = $_GET['totreg'];
	$idcta = $_GET['idcta'];
	$altura = $_GET['altura'];
	$filtro = "'".$_GET['filtro']."'";
?>
<!DOCTYPE >
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="es">
	<head>
		<meta http-equiv="Content-type" content="text/html; charset=iso-8859-1"/>
		<meta http-equiv="X-UA-Compatible" content="IE=9"/>
		<title>:: IDEAL 10 - Servicios P&uacute;blicos</title>
		<link href="css/css2.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<link href="css/css4.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<link href="css/cssSP.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<link href="css/sweetalert.css" rel="stylesheet" type="text/css"/>
		<script type="text/javascript" src="css/sweetalert.js"></script>
    	<script type="text/javascript" src="css/sweetalert.min.js"></script>
		<script type="text/javascript" src="css/programas.js"></script>
		<style>
			
		</style>

		<script>
			function despliegamodalm(_valor,_tip,mensa,pregunta)
			{
				document.getElementById("bgventanamodalm").style.visibility=_valor;

				if(_valor == "hidden")
				{
					document.getElementById('ventanam').src="";
				}
				else
				{
					switch(_tip)
					{
						case "1":	document.getElementById('ventanam').src="ventana-mensaje1.php?titulos="+mensa;break;
						case "2":	document.getElementById('ventanam').src="ventana-mensaje3.php?titulos="+mensa;break;
						case "3":	document.getElementById('ventanam').src="ventana-mensaje2.php?titulos="+mensa;break;
						case "4":	document.getElementById('ventanam').src="ventana-consulta1.php?titulos="+mensa+"&idresp="+pregunta;break;	
					}
				}
			}
			
			function funcionmensaje(){}

			function respuestaconsulta(pregunta)
			{
				switch(pregunta)
				{
					case "1":	
						document.form2.oculto.value='2';
						document.form2.submit();
					break;
				}
			}

			function guardar()
			{
				var codigo = document.getElementById('codban').value;
				var nombre = document.getElementById('nomban').value;
				if (codigo.trim() && nombre.trim()) 
				{
					despliegamodalm('visible','4','Esta Seguro de Modificar','1');
				}
				else 
				{
					despliegamodalm('visible','2','Falta informacion para modificar la zona');
				}
			}

			function cambiocheck()
			{
				if(document.getElementById('myonoffswitch').value == 'S')
				{
					document.getElementById('myonoffswitch').value = 'N';
				}
				else
				{
					document.getElementById('myonoffswitch').value = 'S';
				}

				document.form2.submit();
			}

			function iratras(scrtop, numpag, limreg, filtro)
			{
				var idcta = document.getElementById('codban').value;

				location.href="serv-zonasbuscar.php?idcta="+idcta+"&scrtop="+scrtop+"&numpag="+numpag+"&limreg="+limreg+ "&filtro="+filtro;
			}

			function validateCode() 
			{
				var validacion01 = document.getElementById('codban').value;

				if (validacion01.trim() != '') 
				{
					document.form2.oculto.value = '3';
					document.form2.submit();
				}
				else 
				{
					swal("Error", "Código vacio, digite un código para continuar.", "error");
				}
			}

			function errorCodeRepeated(resultadoBusqueda)
			{	
				swal("Error", "Código en uso, digite otro códigos que no este en uso.", "error");
			}

			function adelante(scrtop, numpag, limreg, filtro, next)
			{
				var maximo = document.getElementById('maximo').value;
				var actual = document.getElementById('codban').value;

				if(parseFloat(maximo)>parseFloat(actual))
				{
					if(actual < 10)
					{
						actual = "0" + actual;
					}

					document.getElementById('oculto').value = '1';
					document.getElementById('codban').value = next;
					var actual = document.getElementById('codban').value;
					location.href="serv-zonaseditar.php?idban="+actual+"&scrtop="+scrtop+"&numpag="+numpag+"&limreg="+limreg+"&filtro="+filtro;
				}
			}

			function atrasc(scrtop, numpag, limreg, filtro, prev)
			{
				var minimo=document.getElementById('minimo').value;
				var actual=document.getElementById('codban').value;

				if(parseFloat(minimo)<parseFloat(actual))
				{
					if(actual < 10)
					{
						actual = "0" + actual;
					}

					document.getElementById('oculto').value = '1';
					document.getElementById('codban').value = prev;
					var actual = document.getElementById('codban').value;
					location.href="serv-zonaseditar.php?idban="+actual+"&scrtop="+scrtop+"&numpag="+numpag+"&limreg="+limreg+"&filtro="+filtro;
				}
			}
		</script>

		<?php titlepag();?>

	</head>
	
	<body>
		<IFRAME src="alertas.php" name="alertas" id="alertas" style="display:none"></IFRAME>
		<span id="todastablas2"></span>

		<?php
			$numpag = @ $_GET['numpag'];
			$limreg = @ $_GET['limreg'];
			$scrtop = 26 * $totreg;
		?>

		<table>
			<tr><script>barra_imagenes("serv");</script><?php cuadro_titulos();?></tr>	

			<tr><?php menu_desplegable("serv");?></tr>

			<tr>
				<td colspan="3" class="cinta">

					<a href="serv-zonas.php" class="mgbt"><img src="imagenes/add.png"/></a>

					<a onclick="guardar()" class="mgbt"><img src="imagenes/guarda.png" title="Guardar"/></a>

					<a href="serv-zonasbuscar.php" class="mgbt"><img src="imagenes/busca.png" title="Buscar"/></a>

					<a onclick="mypop=window.open('plan-agenda.php','','');mypop.focus()" class="mgbt"><img src="imagenes/agenda1.png" title="Agenda" /></a>

					<a onclick="mypop=window.open('serv-principal.php','','');mypop.focus();" class="mgbt"><img src="imagenes/nv.png" title="Nueva ventana"></a>

					<a onclick="mypop=window.open('<?php echo $url2; ?>','','');mypop.focus();" class="mgbt"><img src="imagenes/duplicar_pantalla.png" title="Duplicar pesta&ntilde;a"></a>

					<a onClick="iratras(<?php echo "$scrtop, $numpag, $limreg, $filtro"; ?>)" class="mgbt"><img src="imagenes/iratras.png" title="Atr&aacute;s"></a>
				</td>
			</tr>
		</table>

		<div id="bgventanamodalm" class="bgventanamodalm">
			<div id="ventanamodalm" class="ventanamodalm">
				<IFRAME src="" name="ventanam" marginWidth=0 marginHeight=0 frameBorder=0 id="ventanam" frameSpacing=0 style=" width:700px; height:130px; top:200; overflow:hidden;"></IFRAME>
			</div>
		</div>

		<form name="form2" method="post" action="">

			<?php
				if(@$_POST['oculto'] == "")
				{
					//Minimo y maximo
					$sqlr = "SELECT MIN(id), MAX(id) FROM srvzonas ";
					$res = mysqli_query($linkbd,$sqlr);
					$r = mysqli_fetch_row($res);

					$_POST['minimo'] = $r[0];
					$_POST['maximo'] = $r[1];

					//Siguiente 	
					$sqln = "SELECT id FROM srvzonas WHERE id > '$_GET[idban]' ORDER BY id ASC LIMIT 1";
					$resn = mysqli_query($linkbd,$sqln);
					$row = mysqli_fetch_row($resn);
					$next = $row[0];

					//Anterior
					$sqlp = "SELECT id FROM srvzonas WHERE id < '$_GET[idban]' ORDER BY id DESC LIMIT 1";
					$resp = mysqli_query($linkbd,$sqlp);
					$row = mysqli_fetch_row($resp);
					$prev = $row[0];

					$sqlr = " SELECT * FROM srvzonas WHERE id='".$_GET['idban']."' ";
					$resp = mysqli_query($linkbd,$sqlr);
					$row = mysqli_fetch_row($resp); 

					$_POST['codban'] = $row[0];
					$_POST['nomban'] = $row[1];
					$_POST['onoffswitch'] = $row[2];
				}

				if (@$_POST['oculto'] == '3') 
				{
					$codigo = $_POST['codban'];
					$resultado = validaExistenciaId('srvzonas', $codigo);

					if ($resultado == 0)
					{
						echo "<script>errorCodeRepeated();</script>";
						$_POST['codban'] = '';
					}
					
				}
			?>

			<table class="inicio">
				<tr>
					<td class="titulos" colspan="6">.: Editar Zona</td>

					<td class="cerrar" style="width:7%" onClick="location.href='serv-principal.php'">Cerrar</td>
				</tr>

				<tr>
					<td class="tamano01" style="width:3cm;">C&oacute;digo:</td>

					<td style="width:10%;">
						<a href="#" onClick="atrasc(<?php echo "$scrtop, $numpag, $limreg, $filtro, $prev"; ?>)"><img src="imagenes/back.png" alt="anterior" align="absmiddle"></a> 
						<input type="text" name="codban" id="codban" onKeyUp="return tabular(event,this)" value="<?php echo @$_POST['codban'];?>" style="width:70%;height:30px;text-align:center;" onblur="validateCode();"/>
						<a href="#" onClick="adelante(<?php echo "$scrtop, $numpag, $limreg, $filtro, $next" ?>);"><img src="imagenes/next.png" alt="siguiente" align="absmiddle"></a> 
					</td>

					<td class="tamano01" style="width:3cm;">Nombre:</td>

					<td>
						<input type="text" name="nomban" id="nomban" value="<?php echo $_POST['nomban'];?>" style="width:100%; height:30px;"/>
					</td>

					<td class="tamano01" style="width:3cm;">Estado:</td>

					<td>
						<div class="onoffswitch">
							<input type="checkbox" name="onoffswitch" class="onoffswitch-checkbox" id="myonoffswitch" value="<?php echo @$_POST['onoffswitch'];?>" <?php if(@$_POST['onoffswitch']=='S'){echo "checked";}?> onChange="cambiocheck();"/>

							<label class="onoffswitch-label" for="myonoffswitch">
								<span class="onoffswitch-inner"></span>
								<span class="onoffswitch-switch"></span>
							</label>
						</div>
					</td>
				</tr>
			</table>

			<input type="hidden" name="maximo" id="maximo" value="<?php echo @ $_POST['maximo']?>"/>
			<input type="hidden" name="minimo" id="minimo" value="<?php echo @ $_POST['minimo']?>"/>
			<input type="hidden" name="oculto" id="oculto" value="1"/>

			<?php
				if(@$_POST['oculto'] == "2")
				{
					if(@$_POST['onoffswitch'] != 'S')
					{
						$valest = 'N';
					}
					else 
					{
						$valest = 'S';
					}

					$sqlr = "UPDATE srvzonas SET nombre='".$_POST['nomban']."',estado='$valest' WHERE id='".$_GET['idban']."' ";

					if (!mysqli_query($linkbd,$sqlr))
					{
						echo"
							<script>
								despliegamodalm('visible','2','No se pudo ejecutar la peticion');
							</script>";
					}
					else 
					{
						echo "
							<script>
								despliegamodalm('visible','3','Se ha Edito con Exito');
							</script>";
					}
				}
			?>

		</form>

		<div id="bgventanamodal2">
			<div id="ventanamodal2">
				<IFRAME  src="" name="buster" marginWidth=0 marginHeight=0 frameBorder=0 id="ventana2" frameSpacing=0 style="left:500px; width:900px; height:500px; top:200;"></IFRAME>
			</div>
		</div>

	</body>
</html>