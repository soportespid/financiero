<?php
	header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=utf8");
	require 'comun.inc';
	require 'funciones.inc';
	require 'funcionesnomima.inc.php';
	require 'validaciones.inc';
	sesion();
	$linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");
	cargarcodigopag($_GET['codpag'],$_SESSION['nivel']);
	date_default_timezone_set("America/Bogota");
?>
<!DOCTYPE >
<html lang="es">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=9">
		<meta name="viewport" content="user-scalable=no">
		<title>:: IDEAL 10 - Gesti&oacute;n humana</title>
		<link href="css/css2.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css">
		<link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css">
		<link href="css/css4.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css">
		<link href="css/tabs.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css">
		<link rel="stylesheet" href="sweetalert2/dist/sweetalert2.min.css">
		<script src="sweetalert2/dist/sweetalert2.min.js"></script>
		<script type="text/javascript" src="css/calendario.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script type="text/javascript" src="css/programas.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script type='text/javascript' src='JQuery/jquery-2.1.4.min.js'></script>
		<script>
			function excell(){
				document.form2.action = "hum-liquidarnominaexcel.php";
				document.form2.target = "_BLANK";
				document.form2.submit(); 
				document.form2.action = "";
				document.form2.target = "";
			}
			function pdf(){
				document.form2.action = "pdfpeticionrp.php";
				document.form2.target = "_BLANK";
				document.form2.submit(); 
				document.form2.action = "";
				document.form2.target = "";
			}
			function funcionmensaje(){
				//document.location.href = "hum-liquidarnominamirar.php?idnomi="+document.form2.idcomp.value;
			}
			function guardar(){
				if (document.form2.idpreli.value != '-1'){
					Swal.fire({
						icon: 'question',
						title: 'Seguro que quieres guardar?',
						showDenyButton: true,
						confirmButtonText: 'Guardar',
						denyButtonText: 'Cancelar',
					}).then(
						(result) => {
							if (result.isConfirmed){
								document.getElementById('oculto').value = 2;
								document.form2.submit();
							}
							else if (result.isDenied){
								Swal.fire('No se guardo la información', '', 'info');
							}
						}
					)
				}else{
					Swal.fire(
						'Error!',
						'Faltan datos para completar el registro',
						'error'
					);
				}
			}
			function cambiovigencia($fecha){
				var fechat = document.getElementById("fc_1198971545").value.split('/');
				document.getElementById("vigencia").value = fechat[2];
				document.form2.submit();
			}
		</script>
		<?php 
			titlepag();
			function buscavalorotrospagos($prenom,$tipo,$codfunc){
				$linkbd = conectar_v7();
				$linkbd -> set_charset("utf8");
				$sqlr = "SELECT valpago FROM hum_otrospagos WHERE codpre = '$prenom' AND codpag = '$tipo' AND codigofun = '$codfunc'";
				$res = mysqli_query($linkbd,$sqlr);
				$r = mysqli_fetch_row($res);
				return $r[0];
			}
			function totalvalorotrospagossinaux($prenom,$codfunc){
				$linkbd = conectar_v7();
				$linkbd -> set_charset("utf8");
				$sqlr = "SELECT SUM(valpago) FROM hum_otrospagos WHERE codpre = '$prenom' AND (codpag<>'01' AND codpag<>'04' AND codpag<>'05') AND codigofun = '$codfunc'";
				$res = mysqli_query($linkbd,$sqlr);
				$r = mysqli_fetch_row($res);
				return $r[0];
			}
			function calcularibc($prenom,$codfunc,$tipo){
				switch ($tipo){
					case '01':	$condi = "peps='S'";break;
					case '02':	$condi = "ppen='S'";break;
					case '03':	$condi = "parl='S'";break;
					case '04':	$condi = "ppar='S'";break;
				}
				$linkbd = conectar_v7();
				$linkbd -> set_charset("utf8");
				$sqlr = "SELECT SUM(valpago) FROM hum_otrospagos WHERE codpre='$prenom' AND codigofun='$codfunc' AND $condi";
				$res = mysqli_query($linkbd,$sqlr);
				$r = mysqli_fetch_row($res);
				return 0;
			}
			function calcularibcfs($prenom,$codfunc){
				$linkbd = conectar_v7();
				$linkbd -> set_charset("utf8");
				$sqlr="SELECT salarifun, diast, diasi, diasv, ppen FROM hum_prenomina_det WHERE codigo = '$prenom' AND codigofun = '$codfunc'";
				$res = mysqli_query($linkbd,$sqlr);
				$r = mysqli_fetch_row($res);
				if($r[4] == 'S'){
					$sumdias = $r[1]+$r[2]+$r[3];
					$totaldias = ($r[0]/30) * $sumdias;
				}else {
					$totaldias = 0;
				}
				$sqlr = "SELECT SUM(valpago) FROM hum_otrospagos WHERE codpre = '$prenom' AND codigofun = '$codfunc' AND ppen = 'S'";
				$res = mysqli_query($linkbd,$sqlr);
				$r = mysqli_fetch_row($res);
				$totalotros = $r[0];
				$totalpago = $totaldias + $totalotros;
				return $totalpago;
			}
			function aplicaredondeo($valor,$resino,$renivel,$retipo){
				$ndecimal = -1*substr_count($retipo, '0');
				if($resino == 1){
					if($renivel == 0){
						$revalor = ceil($valor/$retipo)*$retipo;
					}else{
						$revalor = round($valor, $ndecimal);
					}
				}else{
					$revalor = $valor;
				}
				return $revalor;
			}
			function presservsocial($codfun, $tipo){
				$linkbd = conectar_v7();
				$linkbd -> set_charset("utf8");
				switch ($tipo){
					case '1': 	$nf = 'NUMEPS';break;
					case '2':	$nf = 'NUMAFP';break;
					case '3':	$nf = 'NUMARL';break;
					case '4':	$nf = 'NUMFDC';break;
				}
				$sqlr="
				SELECT  GROUP_CONCAT(descripcion ORDER BY CONVERT(codrad, SIGNED INTEGER) SEPARATOR '<->')
				FROM hum_funcionarios 
				WHERE (item = '$nf') AND estado='S'  AND codfun='$codfun'
				GROUP BY codfun
				ORDER BY CONVERT(codfun, SIGNED INTEGER)";
				$res = mysqli_query($linkbd,$sqlr);
				$r = mysqli_fetch_row($res);
				return $r[0];
			}
			function para_admfiscales2($vigencia){ //carga parametros admfiscales
				$linkbd = conectar_v7();
				$linkbd -> set_charset("utf8");
				if($vigencia != ''){
					$sqlr = "SELECT * FROM admfiscales WHERE vigencia = '$vigencia'";
				}else{
					$sqlr = "SELECT * FROM admfiscales WHERE vigencia = (SELECT MAX(vigencia) FROM admfiscales)";
				}
				$resp = mysqli_query($linkbd,$sqlr);
				$row = mysqli_fetch_row($resp);
				$_POST['salmin'] = $row[3];
				$_POST['bfsol'] = $row[6];
				$_POST['icbf'] = $row[10];
				$_POST['sena'] = $row[11];
				$_POST['iti'] = $row[12];
				$_POST['cajacomp'] = $row[13];
				$_POST['esap'] = $row[14];
				$_POST['indiceinca'] = $row[15];
			}
		?>
	</head>
	<body>
		<IFRAME src="alertas.php" name="alertas" id="alertas" style="display:none"></IFRAME>
		<span id="todastablas2"></span>
		<table>
			<tr><script>barra_imagenes("hum");</script><?php cuadro_titulos();?></tr>
			<tr><?php menu_desplegable("hum");?></tr>
			<tr>
				<td colspan="3" class="cinta">
					<img src="imagenes/add.png" title="Nuevo" onClick="location.href='hum-nominacrear.php'" class="mgbt">
					<img src="imagenes/guarda.png" title="Guardar" onClick="guardar()" class="mgbt">
					<img src="imagenes/busca.png" title="Buscar" onClick="location.href='hum-liquidarnominabuscar.php'" class="mgbt">
					<img src="imagenes/nv.png" title="Nueva ventana" onClick="mypop=window.open('hum-principal.php','','');mypop.focus();" class="mgbt">
					<img src='imagenes/iratras.png' title="Atr&aacute;s" onClick="location.href='hum-menunomina.php'" class="mgbt">
				</td>
			</tr>
		</table>
		<div id="bgventanamodalm" class="bgventanamodalm">
			<div id="ventanamodalm" class="ventanamodalm">
			<IFRAME src="" name="ventanam" marginWidth=0 marginHeight=0 frameBorder=0 id="ventanam" frameSpacing=0 style=" width:700px; height:130px; top:200; overflow:hidden;"> 
			</IFRAME>
		</div>
		</div>
		<form name="form2" method="post" action=""> 
			<?php
				if($_POST['oculto'] == ""){
					$_POST['fecha'] = date('d/m/Y');
					$_POST['vigencia'] = date('Y');
					$_POST['idcomp'] = selconsecutivo('humnomina','id_nom');
					$_POST['tabgroup1'] = 1;
					$_POST['valtipos'] = "0";
					$_POST['tipo02'] = "";
					//**** carga parametros de nomina
					$sqlr="SELECT * FROM humparametrosliquida";
					$resp = mysqli_query($linkbd,$sqlr);
					$row = mysqli_fetch_row($resp);
					$_POST['aprueba'] = $row[1];
					$_POST['naprueba'] = buscatercero($row[1]);
					$_POST['tsueldo'] = $row[2];
					$_POST['tprimnav'] = $row[3];
					$_POST['tsubalim'] = $row[8];
					$_POST['tauxtrans'] = $row[9];
					$_POST['trecnoct'] = $row[11];
					$_POST['thorextdiu'] = $row[12];
					$_POST['thorextnoct'] = $row[13];
					$_POST['thororddom'] = $row[14];
					$_POST['thorextdiudom'] = $row[15];
					$_POST['thorextnoctdom'] = $row[16];
					$_POST['tcajacomp'] = $row[17];
					$_POST['ticbf'] = $row[18];
					$_POST['tsena'] = $row[19];
					$_POST['titi'] = $row[20];
					$_POST['tesap'] = $row[21];
					$_POST['tarp'] = $row[22];
					$_POST['tsaludemr'] = $row[23];
					$_POST['tsaludemp'] = $row[24];
					$_POST['tpensionemr'] = $row[25];
					$_POST['tpensionemp'] = $row[26];
					//carga parametros admfiscales
					para_admfiscales2($_POST['vigencia']);
					//carga parametros nomina redondeos
					$sqlr = "SELECT * FROM hum_parametros_nom ";
					$resp = mysqli_query($linkbd,$sqlr);
					$row = mysqli_fetch_row($resp);
					$_POST['salrioaux'] = $row[0];
					$_POST['redondeoibc'] = $row[1];
					$_POST['redonpension'] = $row[2];
					$_POST['redondeoibcarp'] = $row[3];
					$_POST['redondeoibcpara'] = $row[4];
					$_POST['tiporedondeoibc1'] = $row[5];
					$_POST['tiporedondeoibc2'] = $row[6];
					$_POST['tiporedondeoibcp1'] = $row[7];
					$_POST['tiporedondeoibcp2'] = $row[8];
					$_POST['tiporedondeoibca1'] = $row[9];
					$_POST['tiporedondeoibca2'] = $row[10];
					$_POST['tipoliquidacion'] = $row[11];
					$_POST['nivelredondeo'] = $row[12];
					$_POST['tipofondosol'] = $row[13];
					$_POST['tipoincapasidad'] = $row[14];
				}
				$listatipomov = array();
				$listatipopago = array();
				$listatcodigofuncionario = array();
				$listacentrocosto = array();
				$listatipopension = array();
				$listaempleados = array();
				$listadocumentos = array();
				$listasalariobasico = array();
				$listadiasliquidados = array();
				$listadevengados = array();
				$listaauxalimentacion = array();
				$listaauxtrasporte = array();
				$listaotrospagos = array();
				$listatotaldevengados = array();
				$listaibc = array();
				$listaibceps = array();
				$listaibcfdp = array();
				$listabaseparafiscales = array();
				$listabasearp = array();
				$listaarp = array();
				$listasaludempleado = array();
				$listasaludempresa = array();
				$listasaludtotal = array();
				$listapensionempleado = array();
				$listapensionempresa = array();
				$listapensiontotal = array();
				$listafondosolidaridad = array();
				$listaretenciones = array();
				$listaotrasdeducciones = array();
				$listatotaldeducciones = array();
				$listanetoapagar = array();
				$listaccf = array();
				$listasena = array();
				$listaicbf = array();
				$listainstecnicos = array();
				$listaesap = array();
				$listadoceps = array();
				$listadocarl = array();
				$listadocafp = array();
				$listadocfdc = array();
				$listaretiro = array();
				$datosnovedad = array();
				$datosnovedadotros = array();
			
				$listafuentes = array();
				$listaprogramatico = array();
				$listabpin = array();
				$listaproyecto = array();
				$listasecpresu = array();
				$listacuentapresu = array();
			
				$listafuentesarl = array();
				$listaprogramaticoarl = array();
				$listabpinarl = array();
				$listaproyectoarl = array();
				$listacuentapresuarl = array();
				$listaporcentajearl = array();
			
				$listafuentesccf = array();
				$listaprogramaticoccf = array();
				$listabpinccf = array();
				$listaproyectoccf = array();
				$listacuentapresuccf = array();
				$listaporcentajeccf = array();
			
				$listafuentessena = array();
				$listaprogramaticosena = array();
				$listabpinsena = array();
				$listaproyectosena = array();
				$listacuentapresusena = array();
				$listaporcentajesena = array();
			
				$listafuentesicbf = array();
				$listaprogramaticoicbf = array();
				$listabpinicbf = array();
				$listaproyectoicbf = array();
				$listacuentapresuicbf = array();
				$listaporcentajeicbf = array();
			
				$listafuentestiti = array();
				$listaprogramaticotiti = array();
				$listabpintiti = array();
				$listaproyectotiti = array();
				$listacuentapresutiti = array();
				$listaporcentajetiti = array();
			
				$listafuentesesap = array();
				$listaprogramaticoesap = array();
				$listabpinesap = array();
				$listaproyectoesap= array();
				$listacuentapresuesap = array();
				$listaporcentajeesap = array();
				
				$listafuentesSE = array();
				$listaprogramaticoSE = array();
				$listabpinSE = array();
				$listaproyectoSE= array();
				$listacuentapresuSE = array();
				
				$listafuentesSR = array();
				$listaprogramaticoSR = array();
				$listabpinSR = array();
				$listaproyectoSR = array();
				$listacuentapresuSR = array();
			
				$listafuentesPE = array();
				$listaprogramaticoPE = array();
				$listabpinPE = array();
				$listaproyectoPE= array();
				$listacuentapresuPE = array();
			
				$listafuentesPR = array();
				$listaprogramaticoPR = array();
				$listabpinPR = array();
				$listaproyectoPR = array();
				$listacuentapresuPR = array();
			
				$listafuentesFS = array();
				$listaprogramaticoFS = array();
				$listabpinFS = array();
				$listaproyectoFS= array();
				$listacuentapresuFS = array();
			
				$pfcp = array();
				$pf[] = array();
				$pfn[] = array();
				switch($_POST['tabgroup1']){
					case 1:	$check1 = 'checked';break;
					case 2:	$check2 = 'checked';break;
					case 3:	$check3 = 'checked';break;
					case 4:	$check4 = 'checked';break;
					case 5:	$check5 = 'checked';break;
					case 6:	$check6 = 'checked';break;
					case 7:	$check6 = 'checked';break;
				}
			?>
			<table class="inicio ancho">
				<tr>
					<td class="titulos" colspan="8">:: Liquidar Nomina</td>
					<td class="cerrar" style="width:7%" onClick="location.href='hum-principal.php'">Cerrar</td>
				</tr>
				<tr>
					<td class="tamano01" style="width:3cm;">No Liquidaci&oacute;n:</td>
					<td style="width:10%"><input type="text" name="idcomp" id="idcomp" value="<?php echo $_POST['idcomp'];?>" style="width:98%;" readonly></td>
					<td class="tamano01" style="width:3cm;">Fecha:</td>
					<td style="width:15%"><input type="text" name="fecha" value="<?php echo $_POST['fecha'];?>" maxlength="10" onKeyPress="javascript:return solonumeros(event)" onKeyUp="return tabular(event,this)" id="fc_1198971545" onKeyDown="mascara(this,'/',patron,true)" title="DD/MM/YYYY" style="width:100%;" onChange="cambiovigencia();" class="colordobleclik" onDblClick="displayCalendarFor('fc_1198971545');" autocomplete="off"></td>
					<td class="tamano01" style="width:3cm;">Vigencia:</td> 
					<td style="width:10%"><input type="text" name="vigencia" id="vigencia" value="<?php echo $_POST['vigencia']?>" style="width:98%;" readonly></td>
					<td class="tamano01" style="width:3cm;">No Preliquidaci&oacute;n:</td> 
					<td>
						<select name="idpreli" id="idpreli" onChange="document.form2.submit();" >
							<option value="-1">Sel ...</option>
							<?php
								$sqlr="SELECT codigo,mes,vigencia FROM hum_prenomina T1 WHERE estado='S' AND num_liq='0' ORDER BY codigo DESC";
								$resp = mysqli_query($linkbd,$sqlr);
								while ($row =mysqli_fetch_row($resp)){
									if($row[0]==$_POST['idpreli']){
										echo "<option value='$row[0]' SELECTED>$row[0] - ".mesletras($row[1])." $row[2]</option>";
									}else {
										echo "<option value='$row[0]'>$row[0] - ".mesletras($row[1])." $row[2]</option>";
									}
								}
							?>
						</select>
						<input type="hidden" id="numnovedad" name="numnovedad" value="<?php echo $_POST['numnovedad'];?>">
					</td>
				</tr>
			</table>
			<!-- Variables parametros de nomina --> 
			<input type="hidden" id="tprimnav" name="tprimnav" value="<?php echo $_POST['tprimnav'];?>">
			<input type="hidden" id="aprueba" name="aprueba" value="<?php echo $_POST['aprueba'];?>">
			<input type="hidden" id="naprueba" name="naprueba" value="<?php echo $_POST['naprueba'];?>">
			<input type="hidden" id="tsueldo" name="tsueldo" value="<?php echo $_POST['tsueldo'];?>">
			<input type="hidden" id="tsubalim" name="tsubalim" value="<?php echo $_POST['tsubalim'];?>">
			<input type="hidden" id="tauxtrans" name="tauxtrans" value="<?php echo $_POST['tauxtrans'];?>">
			<input type="hidden" id="trecnoct" name="trecnoct" value="<?php echo $_POST['trecnoct'];?>">
			<input type="hidden" id="thorextdiu" name="thorextdiu" value="<?php echo $_POST['thorextdiu'];?>">
			<input type="hidden" id="thorextnoct" name="thorextnoct" value="<?php echo $_POST['thorextnoct'];?>">
			<input type="hidden" id="thororddom" name="thororddom" value="<?php echo $_POST['thororddom'];?>">
			<input type="hidden" id="thorextdiudom" name="thorextdiudom" value="<?php echo $_POST['thorextdiudom'];?>">
			<input type="hidden" id="thorextnoctdom" name="thorextnoctdom" value="<?php echo $_POST['thorextnoctdom'];?>">
			<input type="hidden" id="tcajacomp" name="tcajacomp" value="<?php echo $_POST['tcajacomp'];?>">
			<input type="hidden" id="ticbf" name="ticbf" value="<?php echo $_POST['ticbf'];?>">
			<input type="hidden" id="tsena" name="tsena" value="<?php echo $_POST['tsena'];?>">
			<input type="hidden" id="titi" name="titi" value="<?php echo $_POST['titi'];?>">
			<input type="hidden" id="tesap" name="tesap" value="<?php echo $_POST['tesap'];?>">
			<input type="hidden" id="tarp" name="tarp" value="<?php echo $_POST['tarp'];?>">
			<input type="hidden" id="tsaludemr" name="tsaludemr" value="<?php echo $_POST['tsaludemr'];?>">
			<input type="hidden" id="tsaludemp" name="tsaludemp" value="<?php echo $_POST['tsaludemp'];?>">
			<input type="hidden" id="tpensionemr" name="tpensionemr" value="<?php echo $_POST['tpensionemr'];?>">
			<input type="hidden" id="tpensionemp" name="tpensionemp" value="<?php echo $_POST['tpensionemp'];?>">
			<input type="hidden" id="mesnomina" name="mesnomina" value="<?php echo $_POST['mesnomina'];?>">
			<!-- Variables parametros admfiscales --> 
			<input type="hidden" id="cajacomp" name="cajacomp" value="<?php echo $_POST['cajacomp'];?>">
			<input type="hidden" id="icbf" name="icbf" value="<?php echo $_POST['icbf'];?>">
			<input type="hidden" id="sena" name="sena" value="<?php echo $_POST['sena'];?>">
			<input type="hidden" id="esap" name="esap" value="<?php echo $_POST['esap'];?>">
			<input type="hidden" id="iti" name="iti" value="<?php echo $_POST['iti'];?>">
			<input type="hidden" id="indiceinca" name="indiceinca" value="<?php echo $_POST['indiceinca'];?>">
			<input type="hidden" id="btrans" name="btrans" value="<?php echo $_POST['btrans'];?>">
			<input type="hidden" id="balim" name="balim"  value="<?php echo $_POST['balim'];?>">
			<input type="hidden" id="bfsol" name="bfsol" value="<?php echo $_POST['bfsol'];?>">
			<input type="hidden" id="transp" name="transp"  value="<?php echo $_POST['transp'];?>">
			<input type="hidden" id="alim" name="alim" value="<?php echo $_POST['alim'];?>">
			<input type="hidden" id="salmin" name="salmin" value="<?php echo $_POST['salmin'];?>"> 
			<!-- Variables parametros nomina redondeos --> 
			<input type="hidden" id="salrioaux" name="salrioaux" value="<?php echo $_POST['salrioaux'];?>">
			<input type="hidden" id="redondeoibc" name="redondeoibc" value="<?php echo $_POST['redondeoibc'];?>">
			<input type="hidden" id="redonpension" name="redonpension" value="<?php echo $_POST['redonpension'];?>">
			<input type="hidden" id="redondeoibcarp" name="redondeoibcarp" value="<?php echo $_POST['redondeoibcarp'];?>">
			<input type="hidden" id="redondeoibcpara" name="redondeoibcpara" value="<?php echo $_POST['redondeoibcpara'];?>">
			<input type="hidden" id="tiporedondeoibc1" name="tiporedondeoibc1" value="<?php echo $_POST['tiporedondeoibc1'];?>">
			<input type="hidden" id="tiporedondeoibc2" name="tiporedondeoibc2" value="<?php echo $_POST['tiporedondeoibc2'];?>">
			<input type="hidden" id="tiporedondeoibcp1" name="tiporedondeoibcp1" value="<?php echo $_POST['tiporedondeoibcp1'];?>">
			<input type="hidden" id="tiporedondeoibcp2" name="tiporedondeoibcp2" value="<?php echo $_POST['tiporedondeoibcp2'];?>">
			<input type="hidden" id="tiporedondeoibca1" name="tiporedondeoibca1" value="<?php echo $_POST['tiporedondeoibca1'];?>">
			<input type="hidden" id="tiporedondeoibca2" name="tiporedondeoibca2" value="<?php echo $_POST['tiporedondeoibca2'];?>">
			<input type="hidden" id="tipoliquidacion" name="tipoliquidacion" value="<?php echo $_POST['tipoliquidacion'];?>">
			<input type="hidden" id="nivelredondeo" name="nivelredondeo" value="<?php echo $_POST['nivelredondeo'];?>">
			<input type="hidden" id="tipofondosol" name="tipofondosol" value="<?php echo $_POST['tipofondosol'];?>">
			<input type="hidden" id="tipoincapasidad" name="tipoincapasidad" value="<?php echo $_POST['tipoincapasidad'];?>">
			<!-- Variables tipos procesos adicionales --> 
			<?php
				if($_POST['idpreli']!="-1"){//calcula el listado de pago nomina
					$_POST['saldocuentas'] = "0";
					$numnovedad = numero_novedad($_POST['idpreli']);
					$sqlr="SELECT * FROM hum_prenomina_det WHERE codigo='".$_POST['idpreli']."' ORDER BY id_det";
					$resp = mysqli_query($linkbd,$sqlr);
					while ($row =mysqli_fetch_row($resp)){
						if($_POST['mesnomina'] == ''){$_POST['mesnomina'] = $row[2];}
						$totaldevengado = 0;
						$veribc01 = 0;
						$veribc02 = 0;
						$listatipomov[] = "nomina";
						$listatipopago[] = $_POST['tsueldo'];
						$listatcodigofuncionario[] = $row[4];
						$listacentrocosto[] = $row[13];
						$listatipopension[] = $row[19];
						$listaempleados[] = $row[8];
						$listadocumentos[] = $row[7];
						$listasalariobasico[] = $row[6];
						$listadiasliquidados[] = $row[15];
						$listadoceps[] = $row[9];
						$listadocarl[] = $row[10];
						$listadocafp[] = $row[11];
						$listadocfdc[] = $row[12];
						$listaretiro[] = $row[29];
						$devengadocal = $row[18];
						
						$listasecpresu[] = itemfuncionarios($row[4],'42');
						$datosnovedad = tipopresufuncionario($numnovedad,$row[4],$_POST['tsueldo']);
						$listacuentapresuFS[] = $listacuentapresuPE[] = $listacuentapresuSE[] = $listacuentapresu[] = $cuantapresu = buscarcuentanuevocatalogo1($_POST['tsueldo'], $datosnovedad[1]);
						$fuentepre = buscarfuentespres($cuantapresu, $datosnovedad[0], $_POST['vigencia'], $datosnovedad[1], $row[4],$devengadocal, $pfcp);
						$datosagrupados = explode('<_>', $fuentepre);
						$listafuentesFS[] = $listafuentesPE[] = $listafuentesSE[] = $listafuentes[] = $datosagrupados[0];
						if($datosagrupados[1] != ''){
							$listaprogramaticoFS[] = $listaprogramaticoPE[] = $listaprogramaticoSE[] = $listaprogramatico[] = $datosagrupados[1];
							$listaproyectoFS[] = $listaproyectoPE[] = $listaproyectoSE[] = $listaproyecto[] = $datosagrupados[2];
							$listabpinFS[] = $listabpinPE[] = $listabpinSE[] = $listabpin[] = nombrebpim($datosagrupados[2]);
						}else{
							$listaprogramaticoFS[] = $listaprogramaticoPE[] = $listaprogramaticoSE[] = $listaprogramatico[] = '';
							$listaproyectoFS[] = $listaproyectoPE[] = $listaproyectoSE[] = $listaproyecto[] = '';
							$listabpinFS[] = $listabpinPE[] = $listabpinSE[] = $listabpin[] = '';
						}
						// calcular ibc salud ($veribc01) y pension ($veribc02)
						if($row[24] == 'S'){ 	
							$totaldevengado = $listadevengados[] = $devengadocal;
							$cuentafuente = $datosnovedad[0].'<_>'.$cuantapresu.'<_>'.$fuentepre;
							$pfcp[$cuentafuente] += $totaldevengado;
							if(($datosnovedad[3] == 'S' || $datosnovedad[4] == 'S') && ($datosnovedad[5] == 'S' || $datosnovedad[6] == 'S')){
								$listaibceps[] = $veribc01 = aplicaredondeo($devengadocal + calcularibc($_POST['idpreli'],$row[4],'01'), $_POST['redondeoibc'], $_POST['nivelredondeo'], $_POST['tiporedondeoibc2']);
								$listaibcfdp[] = $veribc02 = aplicaredondeo($devengadocal + calcularibc($_POST['idpreli'],$row[4],'02'), $_POST['redondeoibc'], $_POST['nivelredondeo'], $_POST['tiporedondeoibc2']);
							}elseif(($datosnovedad[3] == 'S' || $datosnovedad[4] == 'S')){
								$listaibceps[] = $veribc01 = aplicaredondeo($devengadocal + calcularibc($_POST['idpreli'],$row[4],'01'), $_POST['redondeoibc'], $_POST['nivelredondeo'], $_POST['tiporedondeoibc2']);
								$listaibcfdp[]  = $veribc02 = aplicaredondeo(0 + calcularibc($_POST['idpreli'],$row[4],'02'), $_POST['redondeoibc'], $_POST['nivelredondeo'], $_POST['tiporedondeoibc2']);
							}elseif(($datosnovedad[5] == 'S' || $datosnovedad[6] == 'S')){
								$listaibceps[] = $veribc01 = aplicaredondeo(0 + calcularibc($_POST['idpreli'],$row[4],'01'), $_POST['redondeoibc'], $_POST['nivelredondeo'], $_POST['tiporedondeoibc2']);
								$listaibcfdp[] = $veribc02 = aplicaredondeo($devengadocal+calcularibc($_POST['idpreli'],$row[4],'02'), $_POST['redondeoibc'], $_POST['nivelredondeo'], $_POST['tiporedondeoibc2']);
							}else{
								$listaibceps[] = $veribc01 = aplicaredondeo(0 + calcularibc($_POST['idpreli'],$row[4],'01'), $_POST['redondeoibc'], $_POST['nivelredondeo'], $_POST['tiporedondeoibc2']);
								$listaibcfdp[] = $veribc02 = aplicaredondeo(0 + calcularibc($_POST['idpreli'],$row[4],'02'), $_POST['redondeoibc'], $_POST['nivelredondeo'], $_POST['tiporedondeoibc2']);
							}
						}else{
							$totaldevengado = $listadevengados[]=0;
							if(($datosnovedad[3] == 'S' || $datosnovedad[4] == 'S') && ($datosnovedad[5] == 'S' || $datosnovedad[6] == 'S') ){
								$listaibceps[] = $veribc01 = aplicaredondeo((($row[6]/30)*$row[15]) + calcularibc($_POST['idpreli'],$row[4], '01'), $_POST['redondeoibc'], $_POST['nivelredondeo'], $_POST['tiporedondeoibc2']);
								$listaibcfdp[] = $veribc02 = aplicaredondeo((($row[6]/30)*$row[15]) + calcularibc($_POST['idpreli'],$row[4], '02'),$_POST['redondeoibc'], $_POST['nivelredondeo'], $_POST['tiporedondeoibc2']);
							}elseif(($datosnovedad[3] == 'S' || $datosnovedad[4] == 'S')){
								$listaibceps[] = $veribc01 = aplicaredondeo((($row[6]/30)*$row[15]) + calcularibc($_POST['idpreli'],$row[4], '01'), $_POST['redondeoibc'], $_POST['nivelredondeo'], $_POST['tiporedondeoibc2']);
								$listaibcfdp[] = $veribc02=0;
							}elseif(($datosnovedad[5] == 'S' || $datosnovedad[6] == 'S')){
								$listaibceps[] = $veribc01 = 0;
								$listaibcfdp[] = $veribc02 = aplicaredondeo((($row[6]/30)*$row[15]) + calcularibc($_POST['idpreli'],$row[4], '02'),$_POST['redondeoibc'], $_POST['nivelredondeo'], $_POST['tiporedondeoibc2']);
							}else{
								$listaibceps[] = $veribc01 = 0;
								$listaibcfdp[] = $veribc02 = 0;
							}
						}
						if ($veribc01 > $veribc02){
							$listaibc[] = $veribc01;
						}else{
							$listaibc[] = $veribc01;
						}
						$totaldevengado += $listaotrospagos[] = $otrospag = 0;
						$listatotaldevengados[] = $totaldevengado;
						if($otrospag > 0){ //verificar que hace luego
							$sqlrotos = "SELECT valpago, codpag FROM hum_otrospagos WHERE codpre='".$_POST['idpreli']."' AND (codpag <> '01' AND codpag <> '04' AND codpag <> '05') AND codigofun = '$row[4]'";
							$resotos = mysqli_query($linkbd,$sqlrotos);
							while ($rotos = mysqli_fetch_row($resotos)){
								if($rotos[0]>0){
									$cuantapresu = buscacuentapresupuestalccp($rotos[1],$row[4],'1','');
									$pfcp[$cuantapresu] += $rotos[0];
								}
							}
						}
						if ($datosnovedad[8] == 'S' || $datosnovedad[9] == 'S' || $datosnovedad[10] == 'S' || $datosnovedad[11] == 'S' || $datosnovedad[12] == 'S'){
							$listabaseparafiscales[] = $ibcpara = aplicaredondeo($devengadocal + calcularibc($_POST['idpreli'], $row[4], '04'), $_POST['redondeoibcpara'], $_POST['nivelredondeo'], $_POST['tiporedondeoibc2']);
						}else {
							$listabaseparafiscales[] = $ibcpara = 0;
						}
						if ($datosnovedad[7] == 'S'){
							$listabasearp[] = $ibcarp=aplicaredondeo($devengadocal + calcularibc($_POST['idpreli'],$row[4],'03'), $_POST['redondeoibcarp'], $_POST['nivelredondeo'], $_POST['tiporedondeoibc2']);
						}
						else {
							$listabasearp[] = $ibcarp = 0;
						}
						//calcular SALUD
						{
							$porcentaje1 = itemfuncionarios($row[4],'33');
							$porcentaje2 = itemfuncionarios($row[4],'32');
							$porcentajet = $porcentaje1 + $porcentaje2;
							$valsaludtot = ceil(($veribc01 * $porcentajet) / 10000) * 100;
							if($datosnovedad[3] == 'S' && $datosnovedad[4] == 'S' && $porcentaje1 > 0 && $porcentaje2 > 0){
								$listasaludempleado[] = $rsalud = round(($veribc01*$porcentaje2)/100);
								$listasaludempresa[] = $rsaludemp = $valsaludtot - $rsalud;
								$listasaludtotal[] = $valsaludtot;
							}elseif($datosnovedad[3] == 'S' && $porcentaje2 > 0){
								$listasaludempleado[] = $rsalud = $valsaludtot;
								$listasaludempresa[] = $rsaludemp = 0;
								$listasaludtotal[] = $valsaludtot;
							}elseif($datosnovedad[4] == 'S' && $porcentaje1 > 0){
								$listasaludempleado[] = $rsalud = 0;
								$listasaludempresa[] = $rsaludemp = $valsaludtot;
								$listasaludtotal[] = $valsaludtot;
							}else{
								$listasaludempleado[] = $rsalud = 0;
								$listasaludempresa[] = $rsaludemp = 0;
								$listasaludtotal[] = $valsaludtot = 0;
							}
							$listacuentapresuSR[] = $cuantapresu = buscarcuentanuevocatalogo2($_POST['tsaludemr'],$datosnovedad[2], 'N/A');
							$fuentepre = buscarfuentespres($cuantapresu, $datosnovedad[0], $_POST['vigencia'], $datosnovedad[2], $row[4], $rsaludemp, $pfcp);
							$datosagrupados = explode('<_>', $fuentepre);
							$listafuentesSR[] = $datosagrupados[0];
							if($datosagrupados[1] != ''){
								$listaprogramaticoSR[] = $datosagrupados[1];
								$listaproyectoSR[] = $datosagrupados[2];
								$listabpinSR[] = nombrebpim($datosagrupados[2]);
							}else {
								$listaprogramaticoSR[] = $listaproyectoSR[] = $listabpinSR[] = '';
							}
							$cuentafuente = $datosnovedad[0].'<_>'.$cuantapresu.'<_>'.$fuentepre;
							$pfcp[$cuentafuente] += $rsaludemp;
						}
						//calular PENSION
						{
							$porcentaje1 = itemfuncionarios($row[4],'35');
							$porcentaje2 = itemfuncionarios($row[4],'34');
							$porcentajet = $porcentaje1 + $porcentaje2;
							$valpensiontot = ceil (($veribc02 * $porcentajet) / 10000) * 100;
							if($datosnovedad[5] == 'S' && $datosnovedad[6] == 'S'  && $porcentaje1 > 0 && $porcentaje2 > 0){
								$listapensionempleado[] = $rpension = round(($veribc02 * $porcentaje2) / 100);
								$listapensionempresa[] = $rpensionemp = $valpensiontot - $rpension;
								$listapensiontotal[] = $valpensiontot;
							}elseif($datosnovedad[5] == 'S' && $porcentaje2 > 0){
								$listapensionempleado[] = $rpension = $valpensiontot;
								$listapensionempresa[] = $rpensionemp = 0;
								$listapensiontotal[] = $valpensiontot;
							}elseif($datosnovedad[6] == 'S' && $porcentaje1 > 0){
								$listapensionempleado[] = $rpension = 0;
								$listapensionempresa[] = $rpensionemp = $valpensiontot;
								$listapensiontotal[] = $valpensiontot;
							}else{
								$listapensionempleado[] = $rpension = 0;
								$listapensionempresa[] = $rpensionemp = 0;
								$listapensiontotal[] = $valpensiontot = 0;
							}
							$listacuentapresuPR[] = $cuantapresu = buscarcuentanuevocatalogo2($_POST['tpensionemr'], $datosnovedad[2], $row[19]);
							$fuentepre = buscarfuentespres($cuantapresu, $datosnovedad[0], $_POST['vigencia'], $datosnovedad[2], $row[4], $rpensionemp, $pfcp);
							$datosagrupados = explode('<_>', $fuentepre);
							$listafuentesPR[] = $datosagrupados[0];
							if($datosagrupados[1] != ''){
								$listaprogramaticoPR[] = $datosagrupados[1];
								$listaproyectoPR[] = $datosagrupados[2];
								$listabpinPR[] = nombrebpim($datosagrupados[2]);
							}else {
								$listaprogramaticoPR[] = $listaproyectoPR[] = $listabpinPR[] = '';
							}
							$cuentafuente = $datosnovedad[0].'<_>'.$cuantapresu.'<_>'.$fuentepre;
							$pfcp[$cuentafuente] += $rpensionemp;
						}
						//calcular FONDO SOLIDARIDAD
						{
							if($datosnovedad[5] == 'S' || $datosnovedad[6] == 'S'){
								$devendadofondo = calcularibcfs($_POST['idpreli'],$row[4]);
								$sqlrfs = "SELECT * FROM humfondosoli WHERE estado='S' AND $devendadofondo BETWEEN (rangoinicial * ".$_POST['salmin'].") AND (rangofinal * ".$_POST['salmin'].")";
								$respfs = mysqli_query($linkbd,$sqlrfs);
								$rowfs = mysqli_fetch_row($respfs);
								$fondosol = 0;
								if($_POST['tipofondosol'] == 0){
									$fondosol = round((($rowfs[3]/2)/100) * (round($veribc02,-3,PHP_ROUND_HALF_DOWN)),-2)*2;
								}else {
									$fondosol = ceil(((($rowfs[3]/2)/100) * round($veribc02,-3,PHP_ROUND_HALF_DOWN))/100)*200;
								}
								$listafondosolidaridad[] = $fondosol;
							}else {
								$listafondosolidaridad[] = 0;
							}
						}
						//calcular ARL
						{
							if($datosnovedad[7] == 'S'){
								$listaporcentajearl[] = $porcentaje = porcentajearl($row[4]);
								$valdeci = (int) substr(number_format(($ibcarp*$porcentaje)/100,0),-2);
								if($valdeci > 5){
									$listaarp[] = $valorarl = ceil(($ibcarp*$porcentaje)/10000)*100;
								}else {
									$listaarp[] = $valorarl = round (($ibcarp*$porcentaje)/100,-2,PHP_ROUND_HALF_DOWN);
								}
							}
							else {$listaporcentajearl[] = $listaarp[] = $valorarl = 0;}
							$listacuentapresuarl[] = $cuantapresu = buscarcuentanuevocatalogo2($_POST['tarp'],$datosnovedad[2], 'N/A');
							$fuentepre = buscarfuentespres($cuantapresu, $datosnovedad[0], $_POST['vigencia'], $datosnovedad[2], $row[4], $valorarl, $pfcp);
							$datosagrupados = explode('<_>', $fuentepre);
							$listafuentesarl[] = $datosagrupados[0];
							if($datosagrupados[1] != ''){
								$listaprogramaticoarl[] = $datosagrupados[1];
								$listaproyectoarl[] = $datosagrupados[2];
								$listabpinarl[] = nombrebpim($datosagrupados[2]);
							}else {
								$listaprogramaticoarl[] = $listaproyectoarl[] = $listabpinarl[] = '';
							}
							$cuentafuente = $datosnovedad[0].'<_>'.$cuantapresu.'<_>'.$fuentepre;
							$pfcp[$cuentafuente] += $valorarl;
							$pf[$_POST['tarp']][$cuantapresu] += $valorarl;
							$verifica = esta_en_array($pfn[$_POST['tarp']], $cuantapresu);
							if($verifica == 0){
								$pfn[$_POST['tarp']][] = $cuantapresu;
							}
						}
						//calcular Retenciones
						{
							$sqlrete="SELECT SUM(valorretencion) FROM hum_retencionesfun WHERE estado = 'S' AND estadopago = 'N' AND mes = '$row[2]' AND docfuncionario = '$row[7]' AND vigencia = '$row[3]' AND tipopago = '01'";
							$resrete = mysqli_query($linkbd,$sqlrete);
							$rowrete = mysqli_fetch_row($resrete);
							$listaretenciones[] = $valorretencion = $rowrete[0];
						}
						// calcular Descuentos
						{
							$sqlrds="SELECT SUM(T1.valorcuota) FROM humretenempleados T1 WHERE T1.empleado = '$row[7]' AND T1.habilitado = 'H' AND T1.estado = 'S' AND T1.tipopago = '01' AND ncuotas > (SELECT COUNT(T2.id) FROM humnominaretenemp T2 WHERE T2.cedulanit = '$row[7]' AND T2.id = T1.id AND estado = 'P')";
							$respds = mysqli_query($linkbd,$sqlrds);
							$rowds = mysqli_fetch_row($respds);
							$listaotrasdeducciones[] = $otrasrete = round($rowds[0]);
						}
						//calcular total descuentos
						$listatotaldeducciones[] = $totalretenciones = $rsalud + $rpension + $otrasrete + $fondosol + $valorretencion;
						//calcular Neto a Pagar
						$listanetoapagar[] = $totalneto = $totaldevengado - $totalretenciones;
						//calcular CCF
						{
							if($datosnovedad[8] == 'S'){
								$listaporcentajeccf[] = $porcentaje = itemfuncionarios($row[4],'36');
								$listaccf[] = $valccf = ceil(($ibcpara * $porcentaje) / 10000) * 100;
							}else {
								$listaporcentajeccf[] = $listaccf[] = $valccf = 0;
							}
							$listacuentapresuccf[] = $cuantapresu = buscarcuentanuevocatalogo2($_POST['tcajacomp'], $datosnovedad[2], 'N/A');
							$fuentepre = buscarfuentespres($cuantapresu, $datosnovedad[0], $_POST['vigencia'], $datosnovedad[2], $row[4], $valccf, $pfcp);
							$datosagrupados = explode('<_>', $fuentepre);
							$listafuentesccf[] = $datosagrupados[0];
							if($datosagrupados[1] != ''){
								$listaprogramaticoccf[] = $datosagrupados[1];
								$listaproyectoccf[] = $datosagrupados[2];
								$listabpinccf[] = nombrebpim($datosagrupados[2]);
							}else {
								$listaprogramaticoccf[] = $listaproyectoccf[] = $listabpinccf[] = '';
							}
							$cuentafuente = $datosnovedad[0].'<_>'.$cuantapresu.'<_>'.$fuentepre;
							$pfcp[$cuentafuente] += $valccf;
							$pf[$_POST['tcajacomp']][$cuantapresu] += $valccf;
							$verifica = esta_en_array($pfn[$_POST['tcajacomp']], $cuantapresu);
							if($verifica == 0){
								$pfn[$_POST['tcajacomp']][] = $cuantapresu;
							}
						}
						//calcular SENA
						{
							if($datosnovedad[10] == 'S'){
								$listaporcentajesena[] = $porcentaje = itemfuncionarios($row[4],'38');
								$listasena[] = $valsena = ceil(($ibcpara * $porcentaje) / 10000) * 100;
							}
							else{$listaporcentajesena[] = $listasena[] = $valsena = 0;}
							$listacuentapresusena[] = $cuantapresu = buscarcuentanuevocatalogo2($_POST['tsena'],$datosnovedad[2], 'N/A');
							$fuentepre = buscarfuentespres($cuantapresu, $datosnovedad[0], $_POST['vigencia'], $datosnovedad[2], $row[4], $valsena, $pfcp);
							$datosagrupados = explode('<_>', $fuentepre);
							$listafuentessena[] = $datosagrupados[0];
							if($datosagrupados[1] != ''){
								$listaprogramaticosena[] = $datosagrupados[1];
								$listaproyectosena[] = $datosagrupados[2];
								$listabpinsena[] = nombrebpim($datosagrupados[2]);
							}else {
								$listaprogramaticosena[] = $listaproyectosena[] = $listabpinsena[] = '';
							}
							$cuentafuente = $datosnovedad[0].'<_>'.$cuantapresu.'<_>'.$fuentepre;
							$pfcp[$cuentafuente] += $valsena;
							$pf[$_POST['tsena']][$cuantapresu] += $valsena;
							$verifica = esta_en_array($pfn[$_POST['tsena']], $cuantapresu);
							if($verifica == 0){
								$pfn[$_POST['tsena']][] = $cuantapresu;
							}
						}
						//calcular ICBF
						{
							if($datosnovedad[9] == 'S'){
								$listaporcentajeicbf[] = $porcentaje = itemfuncionarios($row[4],'37');
								$listaicbf[] = $valicbf = ceil(($ibcpara * $porcentaje) / 10000) * 100;
							}else {
								$listaporcentajeicbf[] = $listaicbf[] = $valicbf = 0;
							}
							$listacuentapresuicbf[] = $cuantapresu = buscarcuentanuevocatalogo2($_POST['ticbf'],$datosnovedad[2], 'N/A');
							$fuentepre = buscarfuentespres($cuantapresu, $datosnovedad[0], $_POST['vigencia'], $datosnovedad[2], $row[4],$valicb, $pfcp);
							$datosagrupados = explode('<_>', $fuentepre);
							$listafuentesicbf[] = $datosagrupados[0];
							if($datosagrupados[1] != ''){
								$listaprogramaticoicbf[] = $datosagrupados[1];
								$listaproyectoicbf[] = $datosagrupados[2];
								$listabpinicbf[] = nombrebpim($datosagrupados[2]);
							}else {
								$listaprogramaticoicbf[] = $listaproyectoicbf[] = $listabpinicbf[] = '';
							}
							$cuentafuente = $datosnovedad[0].'<_>'.$cuantapresu.'<_>'.$fuentepre;
							$pfcp[$cuentafuente] += $valicbf;
							$pf[$_POST['ticbf']][$cuantapresu] += $valicbf;
							$verifica = esta_en_array($pfn[$_POST['ticbf']], $cuantapresu);
							if($verifica == 0){$pfn[$_POST['ticbf']][] = $cuantapresu;}
						}
						//calcular INSTITUTOS TECNICOS
						{
							if($datosnovedad[11] == 'S'){
								$listaporcentajetiti[] = $porcentaje = itemfuncionarios($row[4],'39');
								$listainstecnicos[] = $valinstec = ceil(($ibcpara * $porcentaje) / 10000) * 100;
							}else {
								$listaporcentajetiti[] = $listainstecnicos[] = $valinstec = 0;
							}
							$listacuentapresutiti[] = $cuantapresu = buscarcuentanuevocatalogo2($_POST['titi'],$datosnovedad[2], 'N/A');
							$fuentepre = buscarfuentespres($cuantapresu, $datosnovedad[0], $_POST['vigencia'], $datosnovedad[2], $row[4], $valinstec, $pfcp);
							$datosagrupados = explode('<_>', $fuentepre);
							$listafuentestiti[] = $datosagrupados[0];
							if($datosagrupados[1] != ''){
								$listaprogramaticotiti[] = $datosagrupados[1];
								$listaproyectotiti[] = $datosagrupados[2];
								$listabpintiti[] = nombrebpim($datosagrupados[2]);
							}else {
								$listaprogramaticotiti[] = $listaproyectotiti[] = $listabpintiti[] = '';
							}
							$cuentafuente = $datosnovedad[0].'<_>'.$cuantapresu.'<_>'.$fuentepre;
							$pfcp[$cuentafuente] += $valinstec;
							$pf[$_POST['titi']][$cuantapresu] += $valinstec;
							$verifica = esta_en_array($pfn[$_POST['titi']], $cuantapresu);
							if($verifica == 0){$pfn[$_POST['titi']][] = $cuantapresu;}
						}
						//calcular ESAP
						{
							if($datosnovedad[11] == 'S'){
								$listaporcentajeesap[] = $porcentaje = itemfuncionarios($row[4],'40');
								$listaesap[] = $valesap = ceil(($ibcpara*$porcentaje)/10000)*100;
							}else {
								$listaporcentajeesap[] = $listaesap[] = $valesap = 0;
							}
							$listacuentapresuesap[] = $cuantapresu = buscarcuentanuevocatalogo2($_POST['tesap'],$datosnovedad[2], 'N/A');
							$fuentepre = buscarfuentespres($cuantapresu, $datosnovedad[0], $_POST['vigencia'], $datosnovedad[2], $row[4], $valesap, $pfcp);
							$datosagrupados = explode('<_>', $fuentepre);
							$listafuentesesap[] = $datosagrupados[0];
							if($datosagrupados[1] != ''){
								$listaprogramaticoesap[] = $datosagrupados[1];
								$listaproyectoesap[] = $datosagrupados[2];
								$listabpinesap[] = nombrebpim($datosagrupados[2]);
							}else {
								$listaprogramaticoesap[] = $listaproyectoesap[] = $listabpinesap[] = '';
							}
							$cuentafuente = $datosnovedad[0].'<_>'.$cuantapresu.'<_>'.$fuentepre;
							$pfcp[$cuentafuente] += $valesap;
							$pf[$_POST['tesap']][$cuantapresu] += $valesap;
							$verifica = esta_en_array($pfn[$_POST['tesap']], $cuantapresu);
							if($verifica == 0){
								$pfn[$_POST['tesap']][] = $cuantapresu;
							}
						}
						//calcular otros tipos de pago 
						if($row[16]>0){//calcular Incapacidades
							$sqlric="
							SELECT T1.pagar_ibc, T1.pagar_arl, T1.pagar_para, SUM(T1.valor_total), SUM(T1.dias_inca), T1.tipo_inca FROM hum_incapacidades_det AS T1
							INNER JOIN hum_incapacidades AS T2
							ON T1.num_inca = T2.num_inca
							WHERE T2.estado <> 'N' AND T1.mes = '$row[2]' AND T1.vigencia = '$row[3]' AND T1.estado = 'S' AND T1.tipo_inca <> 'LNR' AND T2.cod_fun = '$row[4]'";
							$respic = mysqli_query($linkbd,$sqlric);
							while ($rowic = mysqli_fetch_row($respic)){
								if($rowic[4] != NULL){
									$veribc01 = 0;
									$veribc02 = 0;
									$listatipomov[] = "Incapacidad";
									$listatipopago[] = $_POST['tsueldo'];
									$listatcodigofuncionario[] = $row[4];
									$listadoceps[] = $row[9];
									$listadocarl[] = $row[10];
									$listadocafp[] = $row[11];
									$listadocfdc[] = $row[12];
									$listacentrocosto[] = $row[13];
									$listatipopension[] = $row[19];
									$listaempleados[] = $row[8];
									$listadocumentos[] = $row[7];
									$listasalariobasico[] = $row[6];
									$listadiasliquidados[] = $rowic[4];
									$totaldevengado = $listadevengados[] = $rowic[3];
									$cuantapresu = buscarcuentanuevocatalogo1($_POST['tsueldo'],$datosnovedad[1]);
									$fuentepre = buscarfuentespres($cuantapresu, $datosnovedad[0], $_POST['vigencia'], $datosnovedad[1], $row[4], $totaldevengado, $pfcp);
									$cuentafuente = $datosnovedad[0].'<_>'.$cuantapresu.'<_>'.$fuentepre;
									$pfcp[$cuentafuente] += $totaldevengado;
									if(($datosnovedad[3] == 'S' || $datosnovedad[4] == 'S') && ($datosnovedad[5] == 'S' || $datosnovedad[6] == 'S') && ($rowic[0] == 'S')){
										if($_POST['tipoincapasidad'] == '1'){
											$listaibceps[] = $veribc01 = aplicaredondeo($totaldevengado, $_POST['redondeoibc'], $_POST['nivelredondeo'], $_POST['tiporedondeoibc2']);
											$listaibcfdp[] = $veribc02 = aplicaredondeo($totaldevengado, $_POST['redondeoibc'], $_POST['nivelredondeo'], $_POST['tiporedondeoibc2']);
										}else{
											$listaibceps[] = $veribc01 = aplicaredondeo(($row[6]/30)*$rowic[4], $_POST['redondeoibc'], $_POST['nivelredondeo'], $_POST['tiporedondeoibc2']);
											$listaibcfdp[] = $veribc02 = aplicaredondeo(($row[6]/30)*$rowic[4], $_POST['redondeoibc'], $_POST['nivelredondeo'], $_POST['tiporedondeoibc2']);
										}
									}elseif(($datosnovedad[3] == 'S' || $datosnovedad[4] == 'S') && $rowic[0] == 'S'){
										if($_POST['tipoincapasidad'] == '1'){
											$listaibceps[] = $veribc01 = aplicaredondeo($totaldevengado, $_POST['redondeoibc'], $_POST['nivelredondeo'], $_POST['tiporedondeoibc2']);
										}else{
											$listaibceps[] = $veribc01 = aplicaredondeo(($row[6]/30)*$rowic[4], $_POST['redondeoibc'], $_POST['nivelredondeo'], $_POST['tiporedondeoibc2']);
										}
										$listaibcfdp[] = $veribc02 = 0;
									}elseif(($datosnovedad[5] == 'S' || $datosnovedad[6] == 'S') && $rowic[0] == 'S'){
										$listaibceps[] = $veribc01 = 0;
										if($_POST['tipoincapasidad'] == '1'){
											$listaibcfdp[] = $veribc02 = aplicaredondeo($totaldevengado, $_POST['redondeoibc'], $_POST['nivelredondeo'], $_POST['tiporedondeoibc2']);
										}else{
											$listaibcfdp[] = $veribc02 = aplicaredondeo(($row[6]/30)*$rowic[4], $_POST['redondeoibc'], $_POST['nivelredondeo'], $_POST['tiporedondeoibc2']);
										}
									}else{
										$listaibceps[] = $veribc01 = 0;
										$listaibcfdp[] = $veribc02 = 0;
									}
									if ($veribc01 > $veribc02){$listaibc[] = $veribc01;}
									else{$listaibc[] = $veribc01;}
									$totaldevengado += $listaauxalimentacion[] = 0;
									$totaldevengado += $listaauxtrasporte[] = 0;
									$totaldevengado += $listaotrospagos[] = 0;
									$listatotaldevengados[] = $totaldevengado;
									if(($datosnovedad[8] == 'S' || $datosnovedad[9] == 'S' || $datosnovedad[10] == 'S' || $datosnovedad[11] == 'S' || $datosnovedad[12] == 'S') && $rowic[2] == 'S'){
										if($_POST['tipoincapasidad'] == '1'){
											$listabaseparafiscales[] = $ibcpara = aplicaredondeo($totaldevengado, $_POST['redondeoibcpara'], $_POST['nivelredondeo'], $_POST['tiporedondeoibc2']);
										}else{
											$listabaseparafiscales[] = $ibcpara = aplicaredondeo(($row[6]/30)*$rowic[4], $_POST['redondeoibcpara'], $_POST['nivelredondeo'], $_POST['tiporedondeoibc2']);
										}
									}else {
										$listabaseparafiscales[] = $ibcpara = 0;
									}
									if($datosnovedad[7] == 'S' && $rowic[1] =='S'){
										if($_POST['tipoincapasidad'] == '1'){
											$listabasearp[] = $ibcarp = aplicaredondeo($totaldevengado, $_POST['redondeoibcarp'], $_POST['nivelredondeo'], $_POST['tiporedondeoibc2']);
										}else{
											$listabasearp[] = $ibcarp = aplicaredondeo(($row[6]/30)*$rowic[4], $_POST['redondeoibcarp'], $_POST['nivelredondeo'], $_POST['tiporedondeoibc2']);
										}
									}else {
										$listabasearp[] = $ibcarp = 0;
									}
									//calcular SALUD
									$porcentaje1 = itemfuncionarios($row[4],'33');
									$porcentaje2 = itemfuncionarios($row[4],'32');

									if($rowic[5] == '180'){
										$porcentajet = $porcentaje1;
										$listasaludtotal[] = $valsaludtot = ceil(((($row[6]/30)*$rowic[4])*$porcentajet)/10000)*100;
										$listasaludempleado[] = $rsalud = 0;
										$listasaludempresa[] = $rsaludemp = $valsaludtot;
									}elseif($rowic[5] == 'LNR'){
										$porcentajet = $porcentaje1 + $porcentaje2;
										$listasaludtotal[] = $valsaludtot = ceil(((($row[6]/30)*$rowic[4])*$porcentajet)/10000)*100;
										$listasaludempleado[] = $rsalud = 0;
										$listasaludempresa[] = $rsaludemp = $valsaludtot;
									}else{
										$porcentajet = $porcentaje1 + $porcentaje2;
										$valsaludtot = ceil(($veribc01*$porcentajet)/10000)*100;
										if($datosnovedad[3] == 'S' && $datosnovedad[4] == 'S'){
											$listasaludempleado[] = $rsalud = round(($veribc01*$porcentaje2)/100);
											$listasaludempresa[] = $rsaludemp = $valsaludtot-$rsalud;
											$listasaludtotal[] = $valsaludtot;
										}elseif($datosnovedad[3] == 'S'){
											$listasaludempleado[] = $rsalud = $valsaludtot;
											$listasaludempresa[] = $rsaludemp = 0;
											$listasaludtotal[] = $valsaludtot;
										}elseif($datosnovedad[4] == 'S'){
											$listasaludempleado[] = $rsalud = 0;
											$listasaludempresa[] = $rsaludemp = $valsaludtot;
											$listasaludtotal[] = $valsaludtot;
										}else{
											$listasaludempleado[] = $rsalud = 0;
											$listasaludempresa[] = $rsaludemp = 0;
											$listasaludtotal[] = $valsaludtot = 0;
										}
									}
									$cuantapresu =  buscarcuentanuevocatalogo2($_POST['tsaludemr'],$datosnovedad[2],'N/A');
									$fuentepre = buscarfuentespres($cuantapresu, $datosnovedad[0], $_POST['vigencia'], $datosnovedad[2], $row[4], $rsaludemp, $pfcp);
									$cuentafuente = $datosnovedad[0].'<_>'.$cuantapresu.'<_>'.$fuentepre;
									$pfcp[$cuentafuente] += $rsaludemp;
									//calular PENSION
									$porcentaje1 = itemfuncionarios($row[4],'35');
									$porcentaje2 = itemfuncionarios($row[4],'34');
									if($rowic[5] == '180'){
										$porcentajet=$porcentaje1;
										$listapensiontotal[] = $valpensiontot = ceil(((($row[6]/30)*$rowic[4])*$porcentajet)/10000)*100;
										$listapensionempleado[] =  $rpension = 0;
										$listapensionempresa[] = $rpensionemp = $valpensiontot;
									}elseif($rowic[5] == 'LNR'){
										$porcentajet = $porcentaje1 + $porcentaje2;
										$listapensiontotal[] = $valpensiontot = ceil(((($row[6]/30)*$rowic[4])*$porcentajet)/10000)*100;
										$listapensionempleado[] =  $rpension = 0;
										$listapensionempresa[] = $rpensionemp = $valpensiontot;
									}else{
										$porcentajet = $porcentaje1 + $porcentaje2;
										$valpensiontot = ceil (($veribc02 * $porcentajet)/10000)*100;
										if($datosnovedad[5] == 'S' && $datosnovedad[6] == 'S'){
											$listapensionempleado[] = $rpension = round(($veribc02 * $porcentaje2)/100);
											$listapensionempresa[] = $rpensionemp = $valpensiontot - $rpension;
											$listapensiontotal[] = $valpensiontot;
										}elseif($datosnovedad[5] == 'S'){
											$listapensionempleado[] = $rpension = $valpensiontot;
											$listapensionempresa[] = $rpensionemp = 0;
											$listapensiontotal[] = $valpensiontot;
										}elseif($datosnovedad[6] == 'S'){
											$listapensionempleado[] = $rpension = 0;
											$listapensionempresa[] = $rpensionemp = $valpensiontot;
											$listapensiontotal[] = $valpensiontot;
										}else{
											$listapensionempleado[] = $rpension = 0;
											$listapensionempresa[] = $rpensionemp = 0;
											$listapensiontotal[] = $valpensiontot = 0;
										}
									}
									$cuantapresu = buscarcuentanuevocatalogo2($_POST['tpensionemr'],$datosnovedad[2],$row[19]);
									$fuentepre = buscarfuentespres($cuantapresu, $datosnovedad[0], $_POST['vigencia'], $datosnovedad[2], $row[4], $rpensionemp, $pfcp);
									$cuentafuente = $datosnovedad[0].'<_>'.$cuantapresu.'<_>'.$fuentepre;
									$pfcp[$cuentafuente] += $rpensionemp;
									//calcular FONDO SOLIDARIDAD
									$devendadofondo = calcularibcfs($_POST['idpreli'],$row[4]);
									$sqlrfs="SELECT * FROM humfondosoli WHERE estado='S' AND $devendadofondo BETWEEN (rangoinicial * ".$_POST['salmin'].") AND (rangofinal * ".$_POST['salmin'].")"; 
									$respfs = mysqli_query($linkbd,$sqlrfs);
									$rowfs = mysqli_fetch_row($respfs);
									$fondosol = 0;
									if($rowic[5] != '180'){
										if($_POST['tipofondosol'] == 0){
											$fondosol = round((($rowfs[3]/2)/100)*(round($veribc02,-3,PHP_ROUND_HALF_DOWN)),-2)*2;
										}else{
											$fondosol = ceil(((($rowfs[3]/2)/100)*round($veribc02,-3,PHP_ROUND_HALF_DOWN))/100)*200;
										}
									}
									$listafondosolidaridad[] = $fondosol;
									//calcular ARL
									if($datosnovedad[7] == 'S'){
										$porcentaje = 
										porcentajearl($row[4]);
										$valdeci = (int) substr(number_format(($ibcarp*$porcentaje)/100,0),-2);
										if($valdeci > 5){
											$listaarp[] = $valorarl = ceil(($ibcarp*$porcentaje)/10000)*100;
										}else {
											$listaarp[] = $valorarl = round (($ibcarp*$porcentaje)/100,-2,PHP_ROUND_HALF_DOWN);
										}
									}else{
										$listaarp[] = $valorarl = 0;
									}
									$cuantapresu = buscarcuentanuevocatalogo2($_POST['tarp'],$datosnovedad[2],'N/A'); 
									$fuentepre = buscarfuentespres($cuantapresu, $datosnovedad[0], $_POST['vigencia'], $datosnovedad[2], $row[4], $valorarl, $pfcp);
									$cuentafuente = $datosnovedad[0].'<_>'.$cuantapresu.'<_>'.$fuentepre;
									$pfcp[$cuentafuente] += $valorarl;
									$pf[$_POST['tarp']][$cuantapresu] += $valorarl;
									$verifica = esta_en_array($pfn[$_POST['tarp']], $cuantapresu);
									if($verifica == 0){
										$pfn[$_POST['tarp']][] = $cuantapresu;
									}
									//calcular Retenciones
									$listaretenciones[] = $valorretencion=0;
									// calcular Descuentos
									$listaotrasdeducciones[] = $otrasrete=0;
									//calcular total descuentos
									$listatotaldeducciones[] = $totalretenciones = $rsalud + $rpension + $otrasrete + $fondosol + $valorretencion;
									//calcular Neto a Pagar
									$listanetoapagar[] = $totalneto = $totaldevengado - $totalretenciones;
									//calcular CCF
									if($datosnovedad[8] == 'S'){
										$porcentaje = itemfuncionarios($row[4],'36');
										$listaccf[] = $valccf = ceil(($ibcpara*$porcentaje)/10000)*100;
									}
									$cuantapresu = buscarcuentanuevocatalogo2($_POST['tcajacomp'],$datosnovedad[2],'N/A');
									$fuentepre = buscarfuentespres($cuantapresu, $datosnovedad[0], $_POST['vigencia'], $datosnovedad[2], $row[4], $valccf, $pfcp);
									$cuentafuente = $datosnovedad[0].'<_>'.$cuantapresu.'<_>'.$fuentepre;
									$pfcp[$cuentafuente] += $valccf;
									$pf[$_POST['tcajacomp']][$cuantapresu] += $valccf;
									$verifica = esta_en_array($pfn[$_POST['tcajacomp']], $cuantapresu);
									if($verifica == 0){$pfn[$_POST['tcajacomp']][] = $cuantapresu;}
									//calcular SENA
									if($datosnovedad[10] == 'S'){
										$porcentaje = itemfuncionarios($row[4],'38');
										$listasena[] = $valsena = ceil(($ibcpara*$porcentaje)/10000)*100;
									}
									$cuantapresu = buscarcuentanuevocatalogo2($_POST['tsena'],$datosnovedad[2],'N/A');
									$fuentepre = buscarfuentespres($cuantapresu, $datosnovedad[0], $_POST['vigencia'], $datosnovedad[2], $row[4], $valsena, $pfcp);
									$cuentafuente = $datosnovedad[0].'<_>'.$cuantapresu.'<_>'.$fuentepre;
									$pfcp[$cuentafuente] += $valsena;
									$pf[$_POST['tsena']][$cuantapresu] += $valsena;
									$verifica = esta_en_array($pfn[$_POST['tsena']], $cuantapresu);
									if($verifica == 0){$pfn[$_POST['tsena']][] = $cuantapresu;}
									//calcular ICBF
									if($datosnovedad[9] == 'S'){
										$porcentaje =  itemfuncionarios($row[4],'37');
										$listaicbf[] = $valicbf = ceil(($ibcpara*$porcentaje)/10000)*100;
									}
									$cuantapresu = buscarcuentanuevocatalogo2($_POST['ticbf'],$datosnovedad[2],'N/A');
									$fuentepre = buscarfuentespres($cuantapresu, $datosnovedad[0], $_POST['vigencia'], $datosnovedad[2], $row[4], $valicbf, $pfcp);
									$cuentafuente = $datosnovedad[0].'<_>'.$cuantapresu.'<_>'.$fuentepre;
									$pfcp[$cuentafuente] += $valicbf;
									$pf[$_POST['ticbf']][$cuantapresu] += $valicbf;
									$verifica = esta_en_array($pfn[$_POST['ticbf']], $cuantapresu);
									if($verifica == 0){$pfn[$_POST['ticbf']][] = $cuantapresu;}
									//calcular INSTITUTOS TECNICOS
									if($rowic[5] == 'LM'){
										$listainstecnicos[] = $valinstec = 0;
										$cuantapresu =  buscarcuentanuevocatalogo2($_POST['titi'],$datosnovedad[2],'N/A');
										$fuentepre = buscarfuentespres($cuantapresu, $datosnovedad[0], $_POST['vigencia'], $datosnovedad[2], $row[4], $valinstec, $pfcp);
										$cuentafuente = $datosnovedad[0].'<_>'.$cuantapresu.'<_>'.$fuentepre;
										$pfcp[$cuentafuente] += $valinstec;
										$pf[$_POST['titi']][$cuantapresu] += $valinstec;
										$verifica = esta_en_array($pfn[$_POST['titi']], $cuantapresu);
										if($verifica == 0){$pfn[$_POST['titi']][] = $cuantapresu;}
									}else{
										if($datosnovedad[11] == 'S'){
											$porcentaje =  itemfuncionarios($row[4],'39');
											$listainstecnicos[] = $valinstec = ceil(($ibcpara*$porcentaje)/10000)*100;
										}
										$cuantapresu = buscarcuentanuevocatalogo2($_POST['titi'],$datosnovedad[2],'N/A');
										$fuentepre = buscarfuentespres($cuantapresu, $datosnovedad[0], $_POST['vigencia'], $datosnovedad[2], $row[4], $valinstec, $pfcp);
										$cuentafuente = $datosnovedad[0].'<_>'.$cuantapresu.'<_>'.$fuentepre;
										$pfcp[$cuentafuente] += $valinstec;
										$pf[$_POST['titi']][$cuantapresu] += $valinstec;
										$verifica = esta_en_array($pfn[$_POST['titi']], $cuantapresu);
										if($verifica == 0){
											$pfn[$_POST['titi']][] = $cuantapresu;
										}
									}
									//calcular ESAP
									if($rowic[5] == 'LM'){
										$listaesap[] = $valesap = 0;
										$cuantapresu = buscarcuentanuevocatalogo2($_POST['tesap'],$datosnovedad[2],'N/A');
										$fuentepre = buscarfuentespres($cuantapresu, $datosnovedad[0], $_POST['vigencia'], $datosnovedad[2], $row[4], $valesap, $pfcp);
										$cuentafuente = $datosnovedad[0].'<_>'.$cuantapresu.'<_>'.$fuentepre;
										$pfcp[$cuentafuente] += $valesap;
										$pf[$_POST['tesap']][$cuantapresu] += $valesap;
										$verifica = esta_en_array($pfn[$_POST['tesap']], $cuantapresu);
										if($verifica == 0){
											$pfn[$_POST['tesap']][] = $cuantapresu;
										}
									}else{
										if($datosnovedad[12] == 'S'){
											$porcentaje = itemfuncionarios($row[4],'40');
											$listaesap[] = $valesap = ceil(($ibcpara*$porcentaje)/10000)*100;
										}
										$cuantapresu = buscarcuentanuevocatalogo2($_POST['tesap'],$datosnovedad[2],'N/A');
										$fuentepre = buscarfuentespres($cuantapresu, $datosnovedad[0], $_POST['vigencia'], $datosnovedad[2], $row[4], $valesap, $pfcp);
										$cuentafuente = $datosnovedad[0].'<_>'.$cuantapresu.'<_>'.$fuentepre;
										$pfcp[$cuentafuente] += $valesap;
										$pf[$_POST['tesap']][$cuantapresu] += $valesap;
										$verifica = esta_en_array($pfn[$_POST['tesap']], $cuantapresu);
										if($verifica == 0){
											$pfn[$_POST['tesap']][] = $cuantapresu;
										}
									}
								}
							}
							$sqlric="
							SELECT T1.pagar_ibc, T1.pagar_arl, T1.pagar_para, SUM(T1.valor_total), SUM(T1.dias_inca), T1.tipo_inca FROM hum_incapacidades_det AS T1
							INNER JOIN hum_incapacidades AS T2
							ON T1.num_inca = T2.num_inca
							WHERE T2.estado <> 'N' AND T1.mes = '$row[2]' AND T1.vigencia = '$row[3]' AND T1.estado = 'S' AND T1.tipo_inca = 'LNR' AND T2.cod_fun = '$row[4]'";
							$respic = mysqli_query($linkbd,$sqlric);
							while ($rowic =mysqli_fetch_row($respic)){
								if($rowic[4] != NULL){
									$veribc01 = 0;
									$veribc02 = 0;
									$listatipomov[] = "Incapacidad";
									$listatipopago[] = $_POST['tsueldo'];
									$listatcodigofuncionario[] = $row[4];
									$listadoceps[] = $row[9];
									$listadocarl[] = $row[10];
									$listadocafp[] = $row[11];
									$listadocfdc[] = $row[12];
									$listacentrocosto[] = $row[13];
									$listatipopension[] = $row[19];
									$listaempleados[] = $row[8];
									$listadocumentos[] = $row[7];
									$listasalariobasico[] = $row[6];
									$listadiasliquidados[] = $rowic[4];
									$totaldevengado = $listadevengados[] = $rowic[3];
									$cuantapresu = buscarcuentanuevocatalogo1($_POST['tsueldo'],$datosnovedad[1]);
									$fuentepre = buscarfuentespres($cuantapresu, $datosnovedad[0], $_POST['vigencia'], $datosnovedad[1], $row[4], $totaldevengado, $pfcp);
									$cuentafuente = $datosnovedad[0].'<_>'.$cuantapresu.'<_>'.$fuentepre;
									$pfcp[$cuentafuente] += $totaldevengado;
									if(($datosnovedad[3] == 'S' || $datosnovedad[4] == 'S') && ($datosnovedad[5] == 'S' || $datosnovedad[6] == 'S') && ($rowic[0] == 'S')){
										if($_POST['tipoincapasidad'] == '1'){
											$listaibceps[] = $veribc01 = aplicaredondeo($totaldevengado, $_POST['redondeoibc'], $_POST['nivelredondeo'], $_POST['tiporedondeoibc2']);
											$listaibcfdp[] = $veribc02 = aplicaredondeo($totaldevengado, $_POST['redondeoibc'], $_POST['nivelredondeo'], $_POST['tiporedondeoibc2']);
										}else{
											$listaibceps[] = $veribc01 = aplicaredondeo(($row[6]/30)*$rowic[4], $_POST['redondeoibc'], $_POST['nivelredondeo'], $_POST['tiporedondeoibc2']);
											$listaibcfdp[] = $veribc02 = aplicaredondeo(($row[6]/30)*$rowic[4], $_POST['redondeoibc'],$_POST['nivelredondeo'], $_POST['tiporedondeoibc2']);
										}
									}elseif(($datosnovedad[3] == 'S' || $datosnovedad[4] == 'S') && $rowic[0] == 'S'){
										if($_POST['tipoincapasidad'] == '1'){
											$listaibceps[] = $veribc01 = aplicaredondeo($totaldevengado, $_POST['redondeoibc'], $_POST['nivelredondeo'], $_POST['tiporedondeoibc2']);
										}else{
											$listaibceps[] = $veribc01 = aplicaredondeo(($row[6]/30)*$rowic[4], $_POST['redondeoibc'], $_POST['nivelredondeo'], $_POST['tiporedondeoibc2']);
										}
										$listaibcfdp[] = $veribc02 = 0;
									}
									elseif(($datosnovedad[5] == 'S' || $datosnovedad[6] == 'S') && $rowic[0] == 'S'){
										$listaibceps[] = $veribc01 = 0;
										if($_POST['tipoincapasidad'] == '1'){
											$listaibcfdp[] = $veribc02 = aplicaredondeo($totaldevengado, $_POST['redondeoibc'], $_POST['nivelredondeo'], $_POST['tiporedondeoibc2']);
										}else{
											$listaibcfdp[] = $veribc02 = aplicaredondeo(($row[6]/30)*$rowic[4], $_POST['redondeoibc'], $_POST['nivelredondeo'], $_POST['tiporedondeoibc2']);
										}
									}else{
										$listaibceps[] = $veribc01 = 0;
										$listaibcfdp[] = $veribc02 = 0;
									}
									if ($veribc01 > $veribc02){
										$listaibc[] = $veribc01;
									}else{
										$listaibc[] = $veribc01;
									}
									$totaldevengado += $listaauxalimentacion[] = 0;
									$totaldevengado += $listaauxtrasporte[] = 0;
									$totaldevengado += $listaotrospagos[]= 0;
									$listatotaldevengados[] = $totaldevengado;
									if(($datosnovedad[8] == 'S' || $datosnovedad[9] == 'S' || $datosnovedad[10] == 'S' || $datosnovedad[11] == 'S' || $datosnovedad[12] == 'S') && $rowic[2] == 'S'){
										if($_POST['tipoincapasidad'] == '1'){
											$listabaseparafiscales[] = $ibcpara = aplicaredondeo($totaldevengado, $_POST['redondeoibcpara'], $_POST['nivelredondeo'], $_POST['tiporedondeoibc2']);
										}else{
											$listabaseparafiscales[] = $ibcpara = aplicaredondeo(($row[6]/30)*$rowic[4], $_POST['redondeoibcpara'], $_POST['nivelredondeo'], $_POST['tiporedondeoibc2']);
										}
									}else {
										$listabaseparafiscales[] = $ibcpara = 0;
									}
									if($datosnovedad[7] == 'S' && $rowic[1] == 'S'){
										if($_POST['tipoincapasidad'] == '1'){
											$listabasearp[] = $ibcarp = aplicaredondeo($totaldevengado, $_POST['redondeoibcarp'], $_POST['nivelredondeo'], $_POST['tiporedondeoibc2']);
										}else{
											$listabasearp[] = $ibcarp = aplicaredondeo(($row[6]/30)*$rowic[4], $_POST['redondeoibcarp'], $_POST['nivelredondeo'], $_POST['tiporedondeoibc2']);
										}
									}else {
										$listabasearp[] = $ibcarp = 0;
									}
									//calcular SALUD
									$porcentaje1 = itemfuncionarios($row[4],'33');
									$porcentaje2 = itemfuncionarios($row[4],'32');
									if($rowic[5] == 'LNR'){
										$porcentajet = $porcentaje1;
										$listasaludtotal[] = $valsaludtot = ceil(((($row[6]/30)*$rowic[4])*$porcentajet)/10000)*100;
										$listasaludempleado[] = $rsalud = 0;
										$listasaludempresa[] = $rsaludemp = $valsaludtot;
									}else{
										$porcentajet = $porcentaje1 + $porcentaje2;
										$valsaludtot = ceil(($veribc01*$porcentajet)/10000)*100;
										if($datosnovedad[3] == 'S' && $datosnovedad[4] == 'S'){
											$listasaludempleado[] = $rsalud = round(($veribc01*$porcentaje2)/100);
											$listasaludempresa[] = $rsaludemp = $valsaludtot - $rsalud;
											$listasaludtotal[] = $valsaludtot;
										}elseif($datosnovedad[3] == 'S'){
											$listasaludempleado[] = $rsalud = $valsaludtot;
											$listasaludempresa[] = $rsaludemp = 0;
											$listasaludtotal[] = $valsaludtot;
										}elseif($datosnovedad[4] == 'S'){
											$listasaludempleado[] = $rsalud = 0;
											$listasaludempresa[] = $rsaludemp = $valsaludtot;
											$listasaludtotal[] = $valsaludtot;
										}else{
											$listasaludempleado[] = $rsalud = 0;
											$listasaludempresa[] = $rsaludemp = 0;
											$listasaludtotal[] = $valsaludtot = 0;
										}
									}
									$cuantapresu = buscarcuentanuevocatalogo2($_POST['tsaludemr'],$datosnovedad[2],'N/A');
									$fuentepre = buscarfuentespres($cuantapresu, $datosnovedad[0], $_POST['vigencia'], $datosnovedad[2], $row[4], $rsaludemp, $pfcp);
									$cuentafuente = $datosnovedad[0].'<_>'.$cuantapresu.'<_>'.$fuentepre;
									$pfcp[$cuentafuente] += $rsaludemp;
									//calular PENSION
									$porcentaje1 = itemfuncionarios($row[4],'35');
									$porcentaje2 = itemfuncionarios($row[4],'34');
									if($rowic[5] == 'LNR'){
										$porcentajet = $porcentaje1;
										$listapensiontotal[] = $valpensiontot = ceil(((($row[6]/30)*$rowic[4])*$porcentajet)/10000)*100;
										$listapensionempleado[] = 0;
										$listapensionempresa[] = $rpensionemp = $valpensiontot;
									}else{
										$porcentajet = $porcentaje1 + $porcentaje2;
										$valpensiontot = ceil (($veribc02*$porcentajet)/10000)*100;
										if($datosnovedad[5] == 'S' && $datosnovedad[6] == 'S'){
											$listapensionempleado[] = $rpension = round(($veribc02*$porcentaje2)/100);
											$listapensionempresa[] = $rpensionemp = $valpensiontot - $rpension;
											$listapensiontotal[] = $valpensiontot;
										}
										if($datosnovedad[5] == 'S'){
											$listapensionempleado[] = $rpension = $valpensiontot;
											$listapensionempresa[] = $rpensionemp = 0;
											$listapensiontotal[] = $valpensiontot;
										}
										if($datosnovedad[6] == 'S'){
											$listapensionempleado[] = 0;
											$listapensionempresa[] = $rpensionemp = $valpensiontot;
											$listapensiontotal[] = $valpensiontot;
										}else{
											$listapensionempleado[] = 0;
											$listapensionempresa[] = $rpensionemp = 0;
											$listapensiontotal[] = $valpensiontot = 0;
										}
									}
									$cuantapresu = buscarcuentanuevocatalogo2($_POST['tpensionemr'],$datosnovedad[2],$row[19]);
									$fuentepre = buscarfuentespres($cuantapresu, $datosnovedad[0], $_POST['vigencia'], $datosnovedad[2], $row[4], $rpensionemp, $pfcp);
									$cuentafuente = $datosnovedad[0].'<_>'.$cuantapresu.'<_>'.$fuentepre;
									$pfcp[$cuentafuente] += $rpensionemp;
									//calcular FONDO SOLIDARIDAD
									$devendadofondo = calcularibcfs($_POST['idpreli'],$row[4]);
									$sqlrfs="SELECT * FROM humfondosoli WHERE estado='S' AND $devendadofondo between (rangoinicial * ".$_POST['salmin'].") AND (rangofinal * ".$_POST['salmin'].")"; 
									$respfs = mysqli_query($linkbd,$sqlrfs);
									$rowfs = mysqli_fetch_row($respfs);
									$fondosol = 0;
									if($_POST['tipofondosol'] == 0){
										$fondosol = round((($rowfs[3]/2)/100)*(round($veribc02,-3,PHP_ROUND_HALF_DOWN)),-2)*2;
									}else {
										$fondosol=ceil(((($rowfs[3]/2)/100)*round($veribc02,-3,PHP_ROUND_HALF_DOWN))/100)*200;
									}
									$listafondosolidaridad[] = $fondosol;
									//calcular ARL
									if($datosnovedad[7] == 'S'){
										$porcentaje = porcentajearl($row[4]);
										$valdeci = (int) substr(number_format(($ibcarp*$porcentaje)/100,0),-2);
										if($valdeci > 5){
											$listaarp[] = $valorarl = ceil(($ibcarp*$porcentaje)/10000)*100;
										}else{
											$listaarp[] = $valorarl = round (($ibcarp*$porcentaje)/100,-2,PHP_ROUND_HALF_DOWN);
										}
									}
									else {
										$listaarp[] = $valorarl = 0;
									}
									$cuantapresu = buscarcuentanuevocatalogo2($_POST['tarp'],$datosnovedad[2],'N/A');
									$fuentepre = buscarfuentespres($cuantapresu, $datosnovedad[0], $_POST['vigencia'], $datosnovedad[2], $row[4], $valorarl, $pfcp);
									$cuentafuente = $datosnovedad[0].'<_>'.$cuantapresu.'<_>'.$fuentepre;
									$pfcp[$cuentafuente] += $valorarl;
									$pf[$_POST['tarp']][$cuantapresu] += $valorarl;
									$verifica = esta_en_array($pfn[$_POST['tarp']], $cuantapresu);
									if($verifica == 0){$pfn[$_POST['tarp']][] = $cuantapresu;}
									//calcular Retenciones
									$listaretenciones[] = $valorretencion = 0;
									// calcular Descuentos
									$listaotrasdeducciones[] = $otrasrete = 0;
									//calcular total descuentos
									$listatotaldeducciones[] = $totalretenciones = $rsalud + $rpension + $otrasrete + $fondosol + $valorretencion;
									//calcular Neto a Pagar
									$listanetoapagar[] = $totalneto = $totaldevengado - $totalretenciones;
									//calcular CCF
									if($datosnovedad[8] == 'S'){
										$porcentaje = itemfuncionarios($row[4],'36');
										$listaccf[] = $valccf = ceil(($ibcpara*$porcentaje)/10000)*100;
									}else {
										$listaccf[] = $valccf = 0;
									}
									$cuantapresu = buscarcuentanuevocatalogo2($_POST['tcajacomp'],$datosnovedad[2],'N/A');
									$fuentepre = buscarfuentespres($cuantapresu, $datosnovedad[0], $_POST['vigencia'], $datosnovedad[2], $row[4], $valccf, $pfcp);
									$cuentafuente = $datosnovedad[0].'<_>'.$cuantapresu.'<_>'.$fuentepre;
									$pfcp[$cuentafuente] += $valccf;
									$pf[$_POST['tcajacomp']][$cuantapresu] += $valccf;
									$verifica = esta_en_array($pfn[$_POST['tcajacomp']], $cuantapresu);
									if($verifica == 0){$pfn[$_POST['tcajacomp']][] = $cuantapresu;}
									//calcular SENA
									if($datosnovedad[10] == 'S'){
										$porcentaje = itemfuncionarios($row[4],'38');
										$listasena[] = $valsena = ceil(($ibcpara*$porcentaje)/10000)*100;
									}else {
										$listasena[] = $valsena = 0;
									}
									$cuantapresu = buscarcuentanuevocatalogo2($_POST['tsena'],$datosnovedad[2],'N/A');
									$fuentepre = buscarfuentespres($cuantapresu, $datosnovedad[0], $_POST['vigencia'], $datosnovedad[2], $row[4], $valsena, $pfcp);
									$cuentafuente = $datosnovedad[0].'<_>'.$cuantapresu.'<_>'.$fuentepre;
									$pfcp[$cuentafuente] += $valsena;
									$pf[$_POST['tsena']][$cuantapresu] += $valsena;
									$verifica = esta_en_array($pfn[$_POST['tsena']], $cuantapresu);
									if($verifica == 0){$pfn[$_POST['tsena']][] = $cuantapresu;}
									//calcular ICBF
									if($datosnovedad[9] == 'S'){
										$porcentaje = itemfuncionarios($row[4],'36');
										$listaicbf[] = $valicbf=ceil(($ibcpara*$porcentaje)/10000)*100;
									}else {
										$listaicbf[] = $valicbf = 0;
									}
									$cuantapresu = buscarcuentanuevocatalogo2($_POST['ticbf'],$datosnovedad[2],'N/A');
									$fuentepre = buscarfuentespres($cuantapresu, $datosnovedad[0], $_POST['vigencia'], $datosnovedad[2], $row[4], $valicbf, $pfcp);
									$cuentafuente = $datosnovedad[0].'<_>'.$cuantapresu.'<_>'.$fuentepre;
									$pfcp[$cuentafuente] += $valicbf;
									$pf[$_POST['ticbf']][$cuantapresu] += $valicbf;
									$verifica = esta_en_array($pfn[$_POST['ticbf']], $cuantapresu);
									if($verifica == 0){$pfn[$_POST['ticbf']][] = $cuantapresu;}
									//calcular INSTITUTOS TECNICOS
									if($rowic[5] == 'LM'){
										$listainstecnicos[] = $valinstec = 0;
										$cuantapresu = buscarcuentanuevocatalogo2($_POST['titi'],$datosnovedad[2],'N/A');
										$fuentepre = buscarfuentespres($cuantapresu, $datosnovedad[0], $_POST['vigencia'], $datosnovedad[2], $row[4], $valinste, $pfcp);
										$cuentafuente = $datosnovedad[0].'<_>'.$cuantapresu.'<_>'.$fuentepre;
										$pfcp[$cuentafuente] += $valinstec;
										
										$pf[$_POST['titi']][$row[13]] += $valinstec;
										$verifica = esta_en_array($pfn[$_POST['titi']], $cuantapresu);
										if($verifica == 0){$pfn[$_POST['titi']][] = $cuantapresu;}
									}else{
										if($datosnovedad[11] == 'S'){
											$porcentaje = itemfuncionarios($row[4],'39');
											$listainstecnicos[] = $valinstec = ceil(($ibcpara*$porcentaje)/10000)*100;
										}else {
											$listainstecnicos[] = $valinstec = 0;
										}
										$cuantapresu = buscarcuentanuevocatalogo2($_POST['titi'],$datosnovedad[2],'N/A');
										$fuentepre = buscarfuentespres($cuantapresu, $datosnovedad[0], $_POST['vigencia'], $datosnovedad[2], $row[4], $valinstec, $pfcp);
										$cuentafuente = $datosnovedad[0].'<_>'.$cuantapresu.'<_>'.$fuentepre;
										$pfcp[$cuentafuente] += $valinstec;
										$pf[$_POST['titi']][$cuantapresu] += $valinstec;
										$verifica = esta_en_array($pfn[$_POST['titi']], $cuantapresu);
										if($verifica == 0){$pfn[$_POST['titi']][] = $cuantapresu;}
									}
									//calcular ESAP
									if($rowic[5]=='LM'){
										$listaesap[] = $valesap=0;
										$cuantapresu = buscacuentapresupuestalccp($_POST['tesap'],$row[4],'2','N/A');
										$fuentepre = buscarfuentespres($cuantapresu, $datosnovedad[0], $_POST['vigencia'], $datosnovedad[2], $row[4], $valesap, $pfcp);
										$cuentafuente = $datosnovedad[0].'<_>'.$cuantapresu.'<_>'.$fuentepre;
										$pfcp[$cuentafuente] += $valesap;
										$pf[$_POST['tesap']][$cuantapresu] += $valesap;
										$verifica = esta_en_array($pfn[$_POST['tesap']], $cuantapresu);
										if($verifica == 0){$pfn[$_POST['tesap']][] = $cuantapresu;}
									}else{
										if($datosnovedad[12] == 'S'){
											$porcentaje = itemfuncionarios($row[4],'40');
											$listaesap[] = $valesap = ceil(($ibcpara*$porcentaje)/10000)*100;
										}
										$cuantapresu = buscacuentapresupuestalccp($_POST['tesap'],$row[4],'2','N/A');
										$fuentepre = buscarfuentespres($cuantapresu, $datosnovedad[0], $_POST['vigencia'], $datosnovedad[2], $row[4], $valesap, $pfcp);
										$cuentafuente = $datosnovedad[0].'<_>'.$cuantapresu.'<_>'.$fuentepre;
										$pfcp[$cuentafuente] += $valesap;
										$pf[$_POST['tesap']][$cuantapresu] += $valesap;
										$verifica = esta_en_array($pfn[$_POST['tesap']], $cuantapresu);
										if($verifica == 0){$pfn[$_POST['tesap']][] = $cuantapresu;}
									}
								}
							}
						} //fin incapacidad
						if($row[17]>0){ //calcular Vacaciones
							$sqlrva = "SELECT T1.valor_total, T1.dias_vaca, T2.valor_total, T2.estado, T1.pagar_ibc, T1.pagar_arl, T1.pagar_para FROM hum_vacaciones_det T1, hum_vacaciones T2 WHERE T1.num_vaca = T2.num_vaca AND T2.estado <> 'N' AND T1.doc_funcionario = '$row[7]' AND T1.mes = '$row[2]' AND T1.vigencia = '$row[3]' AND T1.estado = 'S' ";
							$respva = mysqli_query($linkbd,$sqlrva);
							while ($rowva = mysqli_fetch_row($respva)){
								$veribc01 = 0;
								$veribc02 = 0;
								$listatipomov[] = "Vacaciones";
								$listatipopago[] = $_POST['tsueldo'];
								$listatcodigofuncionario[] = $row[4];
								$listadoceps[] = $row[9];
								$listadocarl[]= $row[10];
								$listadocafp[] = $row[11];
								$listadocfdc[] = $row[12];
								$listacentrocosto[] = $row[13];
								$listatipopension[] = $row[19];
								$listaempleados[] = $row[8];
								$listadocumentos[] = $row[7];
								$listasalariobasico[] = $row[6];
								$listadiasliquidados[]=$rowva[1];
								/*if($rowva[3] == 'S')
								{
									$totaldevengado = $listadevengados[] = $rowva[2];
									$pfcp[traercuentapresu2($_POST[tsueldo],$row[13],$row[3])] += $totaldevengado;
								}
								else*/
								{$totaldevengado = $listadevengados[] = 0;}
								if(($datosnovedad[3] == 'S' || $datosnovedad[4] == 'S') && ($datosnovedad[5] == 'S' || $datosnovedad[6] == 'S') && $rowva[4] == 'S'){
									$listaibceps[] = $veribc01 = aplicaredondeo($rowva[0], $_POST['redondeoibc'], $_POST['nivelredondeo'], $_POST['tiporedondeoibc2']);
									$listaibcfdp[] = $veribc02 = aplicaredondeo($rowva[0], $_POST['redondeoibc'], $_POST['nivelredondeo'], $_POST['tiporedondeoibc2']);
								}elseif(($datosnovedad[3] == 'S' || $datosnovedad[4] == 'S') && $rowva[4] == 'S'){
									$listaibceps[] = $veribc01 = aplicaredondeo($rowva[0], $_POST['redondeoibc'], $_POST['nivelredondeo'] ,$_POST['tiporedondeoibc2']);
									$listaibcfdp[] = $veribc02 = 0;
								}elseif(($datosnovedad[5] == 'S' || $datosnovedad[6] == 'S') && $rowva[4] == 'S'){
									$listaibceps[] = $veribc01 = 0;
									$listaibcfdp[] = $veribc02 = aplicaredondeo($rowva[0], $_POST['redondeoibc'], $_POST['nivelredondeo'], $_POST['tiporedondeoibc2']);
								}else{
									$listaibceps[] = $veribc01 = 0;
									$listaibcfdp[] = $veribc02 = 0;
								}
								if ($veribc01 > $veribc02){
									$listaibc[] = $veribc01;
								}else{
									$listaibc[] = $veribc01;
								}
								$totaldevengado += $listaauxalimentacion[] = 0;
								$totaldevengado += $listaauxtrasporte[] = 0;
								$totaldevengado += $listaotrospagos[] = 0;
								$listatotaldevengados[] = $totaldevengado;
								if(($datosnovedad[8] == 'S' || $datosnovedad[9] == 'S' || $datosnovedad[10] == 'S' || $datosnovedad[11] == 'S' || $datosnovedad[12] == 'S') && $rowva[6] == 'S'){
									$listabaseparafiscales[] = $ibcpara = aplicaredondeo($rowva[0], $_POST['redondeoibcpara'], $_POST['nivelredondeo'], $_POST['tiporedondeoibc2']);
								}else {
									$listabaseparafiscales[] = $ibcpara = 0;
								}
								if($datosnovedad[7] == 'S' && $rowva[5] == 'S'){
									$listabasearp[] = $ibcarp = aplicaredondeo($rowva[0], $_POST['redondeoibcarp'], $_POST['nivelredondeo'], $_POST['tiporedondeoibc2']);
								}else {
									$listabasearp[]=$ibcarp=0;
								}
								//calcular SALUD
								{
									$porcentaje1 = itemfuncionarios($row[4],'33');
									$porcentaje2 = itemfuncionarios($row[4],'32');
									$porcentajet = $porcentaje1 + $porcentaje2;
									$valsaludtot = ceil(($veribc01*$porcentajet)/10000)*100;
									if($datosnovedad[3] == 'S' && $datosnovedad[4] == 'S'){
										$listasaludempleado[] = $rsalud = round(($veribc01*$porcentaje2)/100);
										$listasaludempresa[] = $rsaludemp = $valsaludtot-$rsalud;
										$listasaludtotal[] = $valsaludtot;
									}elseif($datosnovedad[3] == 'S'){
										$listasaludempleado[] = $rsalud = $valsaludtot;
										$listasaludempresa[] = $rsaludemp = 0;
										$listasaludtotal[] = $valsaludtot;
									}elseif($datosnovedad[5] == 'S'){
										$listasaludempleado[] = $rsalud = 0;
										$listasaludempresa[] = $rsaludemp = $valsaludtot;
										$listasaludtotal[] = $valsaludtot;
									}else{
										$listasaludempleado[] = $rsalud = 0;
										$listasaludempresa[] = $rsaludemp = 0;
										$listasaludtotal[] = $valsaludtot = 0;
									}
									$listacuentapresuSR[] = $cuantapresu = buscarcuentanuevocatalogo2($_POST['tsaludemr'], $datosnovedad[2], 'N/A');
									$fuentepre = buscarfuentespres($cuantapresu, $datosnovedad[0], $_POST['vigencia'], $datosnovedad[2], $row[4], $rsaludemp, $pfcp);
									$datosagrupados = explode('<_>', $fuentepre);
									$listafuentesSR[] = $datosagrupados[0];
									if($datosagrupados[1] != ''){
										$listaprogramaticoSR[] = $datosagrupados[1];
										$listaproyectoSR[] = $datosagrupados[2];
										$listabpinSR[] = nombrebpim($datosagrupados[2]);
									}else {
										$listaprogramaticoSR[] = $listaproyectoSR[] = $listabpinSR[] = '';
									}
									$cuentafuente = $datosnovedad[0].'<_>'.$cuantapresu.'<_>'.$fuentepre;
									$pfcp[$cuentafuente] += $rsaludemp;
								}
								//calular PENSION
								{
									$porcentaje1 = itemfuncionarios($row[4],'35');
									$porcentaje2 = itemfuncionarios($row[4],'34');
									$porcentajet = $porcentaje1+$porcentaje2;
									$valpensiontot = ceil (($veribc02*$porcentajet)/10000)*100;
									if($datosnovedad[5] == 'S' && $datosnovedad[6] == 'S'){
										$listapensionempleado[] = $rpension = round(($veribc02*$porcentaje2)/100);
										$listapensionempresa[] = $rpensionemp = $valpensiontot - $rpension;
										$listapensiontotal[] = $valpensiontot;
									}elseif($datosnovedad[5] == 'S'){
										$listapensionempleado[] = $rpension = $valpensiontot;
										$listapensionempresa[] = $rpensionemp = 0;
										$listapensiontotal[] = $valpensiontot;
									}elseif($datosnovedad[6] == 'S'){
										$listapensionempleado[] = $rpension = 0;
										$listapensionempresa[] = $rpensionemp = $valpensiontot;
										$listapensiontotal[] = $valpensiontot;
									}else{
										$listapensionempleado[] = $rpension = 0;
										$listapensionempresa[] = $rpensionemp = 0;
										$listapensiontotal[] = $valpensiontot = 0;
									}
									$listacuentapresuPR[] = $cuantapresu = buscarcuentanuevocatalogo2($_POST['tpensionemr'], $datosnovedad[2], $row[19]);
									$fuentepre = buscarfuentespres($cuantapresu, $datosnovedad[0], $_POST['vigencia'], $datosnovedad[2], $row[4], $rpensionemp, $pfcp);
									$datosagrupados = explode('<_>', $fuentepre);
									$listafuentesPR[] = $datosagrupados[0];
									if($datosagrupados[1] != ''){
										$listaprogramaticoPR[] = $datosagrupados[1];
										$listaproyectoPR[] = $datosagrupados[2];
										$listabpinPR[] = nombrebpim($datosagrupados[2]);
									}else {
										$listaprogramaticoPR[] = $listaproyectoPR[] = $listabpinPR[] = '';
									}
									$cuentafuente = $datosnovedad[0].'<_>'.$cuantapresu.'<_>'.$fuentepre;
									$pfcp[$cuentafuente] += $rpensionemp;
								}
								//calcular FONDO SOLIDARIDAD
								{
									$devendadofondo = calcularibcfs($_POST['idpreli'],$row[4]);
									$sqlrfs = "SELECT * FROM humfondosoli WHERE estado='S' AND $devendadofondo between (rangoinicial * ".$_POST['salmin'].") AND (rangofinal * ".$_POST['salmin'].")"; 
									$respfs = mysqli_query($linkbd,$sqlrfs);
									$rowfs = mysqli_fetch_row($respfs);
									$fondosol = 0;
									if($_POST['tipofondosol'] == 0){
										$fondosol = round((($rowfs[3]/2)/100)*(round($veribc02,-3,PHP_ROUND_HALF_DOWN)),-2)*2;
									}else {
										$fondosol = ceil(((($rowfs[3]/2)/100)*round($veribc02,-3,PHP_ROUND_HALF_DOWN))/100)*200;
									}
									$listafondosolidaridad[] = $fondosol;
								}
								//calcular ARL
								{
									if($datosnovedad[7] == 'S'){
										$porcentaje = porcentajearl($row[4]);
										$valdeci = (int) substr(number_format(($ibcarp*$porcentaje)/100,0),-2);
										if($valdeci > 5){
											$listaarp[] = $valorarl = ceil(($ibcarp*$porcentaje)/10000)*100;
										}else {
											$listaarp[] = $valorarl = round (($ibcarp*$porcentaje)/100,-2,PHP_ROUND_HALF_DOWN);
										}
									}else {
										$listaarp[] = $valorarl = 0;
									}
									$listacuentapresuarl[] = $cuantapresu = buscarcuentanuevocatalogo2($_POST['tarp'],$datosnovedad[2], 'N/A');
									$fuentepre = buscarfuentespres($cuantapresu, $datosnovedad[0], $_POST['vigencia'], $datosnovedad[2], $row[4], $valorarl, $pfcp);
									$datosagrupados = explode('<_>', $fuentepre);
									$listafuentesarl[] = $datosagrupados[0];
									if($datosagrupados[1] != ''){
										$listaprogramaticoarl[] = $datosagrupados[1];
										$listaproyectoarl[] = $datosagrupados[2];
										$listabpinarl[] = nombrebpim($datosagrupados[2]);
									}else {
										$listaprogramaticoarl[] = $listaproyectoarl[] = $listabpinarl[] = '';
									}
									$cuentafuente = $datosnovedad[0].'<_>'.$cuantapresu.'<_>'.$fuentepre;
									$pfcp[$cuentafuente] += $valorarl;
									$pf[$_POST['tarp']][$cuantapresu] += $valorarl;
									$verifica = esta_en_array($pfn[$_POST['tarp']], $cuantapresu);
									if($verifica == 0){
										$pfn[$_POST['tarp']][] = $cuantapresu;
									}
								}
								//calcular Retenciones
								$listaretenciones[] = $valorretencion = 0;
								// calcular Descuentos
								$listaotrasdeducciones[] = $otrasrete = 0;
								//calcular total descuentos
								$listatotaldeducciones[] = $totalretenciones = $rsalud + $rpension + $otrasrete + $fondosol + $valorretencion;
								//calcular Neto a Pagar
								$listanetoapagar[] = $totalneto = $totaldevengado - $totalretenciones;
								//calcular CCF
								{
									if($datosnovedad[8] == 'S'){
										$porcentaje = itemfuncionarios($row[4],'36');
										$listaccf[] = $valccf = ceil(($ibcpara*$porcentaje)/10000)*100;
									}else {
										$listaccf[] = $valccf = 0;
									}
									$listacuentapresuccf[] = $cuantapresu = buscarcuentanuevocatalogo2($_POST['tcajacomp'], $datosnovedad[2], 'N/A');
									$fuentepre = buscarfuentespres($cuantapresu, $datosnovedad[0], $_POST['vigencia'], $datosnovedad[2], $row[4], $valccf, $pfcp);
									$datosagrupados = explode('<_>', $fuentepre);
									$listafuentesccf[] = $datosagrupados[0];
									if($datosagrupados[1] != ''){
										$listaprogramaticoccf[] = $datosagrupados[1];
										$listaproyectoccf[] = $datosagrupados[2];
										$listabpinccf[] = nombrebpim($datosagrupados[2]);
									}else {
										$listaprogramaticoccf[] = $listaproyectoccf[] = $listabpinccf[] = '';
									}
									$cuentafuente = $datosnovedad[0].'<_>'.$cuantapresu.'<_>'.$fuentepre;
									$pfcp[$cuentafuente] += $valccf;
									$pf[$_POST['tcajacomp']][$cuantapresu] += $valccf;
									$verifica = esta_en_array($pfn[$_POST['tcajacomp']], $cuantapresu);
									if($verifica == 0){$pfn[$_POST['tcajacomp']][] = $cuantapresu;}
								}
								//calcular SENA
								{
									if($datosnovedad[10] == 'S'){
										$porcentaje = itemfuncionarios($row[4],'38');
										$listasena[] = $valsena = ceil(($ibcpara*$porcentaje)/10000)*100;
									} else {
										$listasena[] = $valsena = 0;
									}
									$listacuentapresusena[] = $cuantapresu = buscarcuentanuevocatalogo2($_POST['tsena'], $datosnovedad[2], 'N/A');
									$fuentepre = buscarfuentespres($cuantapresu, $datosnovedad[0], $_POST['vigencia'], $datosnovedad[2], $row[4], $valsena, $pfcp);
									$datosagrupados = explode('<_>', $fuentepre);
									$listafuentessena[] = $datosagrupados[0];
									if($datosagrupados[1] != ''){
										$listaprogramaticosena[] = $datosagrupados[1];
										$listaproyectosena[] = $datosagrupados[2];
										$listabpinsena[] = nombrebpim($datosagrupados[2]);
									}else {
										$listaprogramaticosena[] = $listaproyectosena[] = $listabpinsena[] = '';
									}
									$cuentafuente = $datosnovedad[0].'<_>'.$cuantapresu.'<_>'.$fuentepre;
									$pfcp[$cuentafuente] += $valsena;
									$pf[$_POST['tsena']][$cuantapresu] += $valsena;
									$verifica = esta_en_array($pfn[$_POST['tsena']], $cuantapresu);
									if($verifica == 0){
										$pfn[$_POST['tsena']][] = $cuantapresu;
									}
								}
								//calcular ICBF
								{
									if($datosnovedad[9] == 'S'){
										$porcentaje = itemfuncionarios($row[4],'37');
										$listaicbf[] = $valicbf = ceil(($ibcpara*$porcentaje)/10000)*100;
									} else {
										$listaicbf[] = $valicbf = 0;
									}
									$listacuentapresuicbf[] = $cuantapresu = buscarcuentanuevocatalogo2($_POST['ticbf'],$datosnovedad[2], 'N/A');
									$fuentepre = buscarfuentespres($cuantapresu, $datosnovedad[0], $_POST['vigencia'], $datosnovedad[2], $row[4], $valicbf, $pfcp);
									$datosagrupados = explode('<_>', $fuentepre);
									$listafuentesicbf[] = $datosagrupados[0];
									if($datosagrupados[1] != ''){
										$listaprogramaticoicbf[] = $datosagrupados[1];
										$listaproyectoicbf[] = $datosagrupados[2];
										$listabpinicbf[] = nombrebpim($datosagrupados[2]);
									}else {
										$listaprogramaticoicbf[] = $listaproyectoicbf[] = $listabpinicbf[] = '';
									}
									$cuentafuente = $datosnovedad[0].'<_>'.$cuantapresu.'<_>'.$fuentepre;
									$pfcp[$cuentafuente] += $valicbf;
									$pf[$_POST['ticbf']][$cuantapresu] += $valicbf;
									$verifica = esta_en_array($pfn[$_POST['ticbf']], $cuantapresu);
									if($verifica == 0){$pfn[$_POST['ticbf']][] = $cuantapresu;}
								}
								//calcular INSTITUTOS TECNICOS
								{
									if($datosnovedad[11] == 'S'){
										$porcentaje = itemfuncionarios($row[4],'39');
										$listainstecnicos[] = $valinstec = ceil(($ibcpara*$porcentaje)/10000)*100;
									}else {
										$listainstecnicos[] = $valinstec = 0;
									}
									$listacuentapresutiti[] = $cuantapresu = buscarcuentanuevocatalogo2($_POST['titi'], $datosnovedad[2], 'N/A');
									$fuentepre = buscarfuentespres($cuantapresu, $datosnovedad[0], $_POST['vigencia'], $datosnovedad[2], $row[4], $valinstec, $pfcp);
									$datosagrupados = explode('<_>', $fuentepre);
									$listafuentestiti[] = $datosagrupados[0];
									if($datosagrupados[1] != ''){
										$listaprogramaticotiti[] = $datosagrupados[1];
										$listaproyectotiti[] = $datosagrupados[2];
										$listabpintiti[] = nombrebpim($datosagrupados[2]);
									}else {
										$listaprogramaticotiti[] = $listaproyectotiti[] = $listabpintiti[] = '';
									}
									$cuentafuente = $datosnovedad[0].'<_>'.$cuantapresu.'<_>'.$fuentepre;
									$pfcp[$cuentafuente] += $valinstec;
									$pf[$_POST['titi']][$cuantapresu] += $valinstec;
									$verifica = esta_en_array($pfn[$_POST['titi']], $cuantapresu);
									if($verifica == 0){
										$pfn[$_POST['titi']][] = $cuantapresu;
									}
								}
								//calcular ESAP
								{
									if($datosnovedad[12] == 'S'){
										$porcentaje =  itemfuncionarios($row[4],'40');
										$listaesap[] = $valesap = ceil(($ibcpara*$porcentaje)/10000)*100;
									}else {
										$listaesap[] = $valesap = 0;
									}
									$listacuentapresuesap[] = $cuantapresu = buscarcuentanuevocatalogo2($_POST['tesap'],$datosnovedad[2],'N/A');
									$fuentepre = buscarfuentespres($cuantapresu, $datosnovedad[0], $_POST['vigencia'], $datosnovedad[2], $row[4], $valesap, $pfcp);
									$datosagrupados = explode('<_>', $fuentepre);
									$listafuentesesap[] = $datosagrupados[0];
									if($datosagrupados[1] != ''){
										$listaprogramaticoesap[] = $datosagrupados[1];
										$listaproyectoesap[] = $datosagrupados[2];
										$listabpinesap[] = nombrebpim($datosagrupados[2]);
									}else {
										$listaprogramaticoesap[] = $listaproyectoesap[] = $listabpinesap[] = '';
									}
									$cuentafuente = $datosnovedad[0].'<_>'.$cuantapresu.'<_>'.$fuentepre;
									$pfcp[$cuentafuente] += $valesap;
									$pf[$_POST['tesap']][$cuantapresu] += $valesap;
									$verifica = esta_en_array($pfn[$_POST['tesap']], $cuantapresu);
									if($verifica == 0){
										$pfn[$_POST['tesap']][] = $cuantapresu;
									}
								}
							}
						}
					}
					//calcula el listado de otros pagos
					$sqlr = "SELECT T1.* FROM hum_otrospagos T1 WHERE T1.codpre = '".$_POST['idpreli']."' ORDER BY T1.id_det";
					$resp = mysqli_query($linkbd,$sqlr);
					while ($row = mysqli_fetch_row($resp)){
						if($row[10] > 0){
							$veribc01 = 0;
							$veribc02 = 0;
							$listatipomov[] = "Otros Pagos";
							$listatipopago[] = $row[2];
							$listatcodigofuncionario[] = $row[5];
							$listacentrocosto[] = $row[9];
							$listadoceps[] = presservsocial($row[5],'1');
							$listadocarl[] = presservsocial($row[5],'3');
							$listadocafp[] = $numafp = presservsocial($row[5],'2');
							$listadocfdc[] = presservsocial($row[5],'4');
							$sqlrtp = "SELECT tipoemprse FROM hum_terceros_emprse WHERE numdocumento = '$numafp' AND estado = 'S'";
							$resptp = mysqli_query($linkbd,$sqlrtp);
							$rowtp = mysqli_fetch_row($resptp);
							$listatipopension[] = $rowtp[0];
							$listaempleados[] = $row[8];
							$listadocumentos[] = $row[7];
							$listasalariobasico[] = $row[6];
							$listadiasliquidados[] = $row[11];
							$valdevotr = $row[10];
							if($valdevotr == 0){$valpagoibc = $row[6];}
							else {$valpagoibc = $valdevotr;}
							$listasecpresu[] = itemfuncionarios($row[5],'42');
							$datosnovedad = tipopresufuncionario($numnovedad,$row[5],$row[2]);
							$listacuentapresuFS[] = $listacuentapresuPE[] = $listacuentapresuSE[] = $listacuentapresu[] = $cuantapresu = buscarcuentanuevocatalogo1($row[2],$datosnovedad[1]); 
							$fuentepre = buscarfuentespres($cuantapresu, $datosnovedad[0], $_POST['vigencia'], $datosnovedad[1], $row[5],$valdevotr, $pfcp);
							$datosagrupados = explode('<_>', $fuentepre);
							$listafuentesFS[] = $listafuentesPE[] = $listafuentesSE[] = $listafuentes[] = $datosagrupados[0];
							if($datosagrupados[1] != ''){
								$listaprogramaticoFS[] = $listaprogramaticoPE[] = $listaprogramaticoSE[] = $listaprogramatico[] = $datosagrupados[1];
								$listaproyectoFS[] = $listaproyectoPE[] = $listaproyectoSE[] = $listaproyecto[] = $datosagrupados[2];
								$listabpinFS[] = $listabpinPE[] = $listabpinSE[] = $listabpin[] = nombrebpim($datosagrupados[2]);
							}else{
								$listaprogramaticoFS[] = $listaprogramaticoPE[] = $listaprogramaticoSE[] = $listaprogramatico[] = '';
								$listaproyectoFS[] = $listaproyectoPE[] = $listaproyectoSE[] = $listaproyecto[] = '';
								$listabpinFS[] = $listabpinPE[] = $listabpinSE[] = $listabpin[] = '';
							}
							if($row[15] == 'S'){
								$totaldevengado = $listadevengados[] = $valdevotr;
								if($totaldevengado > 0){
									$cuentafuente = $datosnovedad[0].'<_>'.$cuantapresu.'<_>'.$fuentepre;
									$pfcp[$cuentafuente] += $totaldevengado;
								}
							}else{
								$totaldevengado = $listadevengados[] = 0;
							}
							if(($datosnovedad[3] == 'S' || $datosnovedad[4] == 'S') && ($datosnovedad[5] == 'S' || $datosnovedad[6] == 'S')){
								$listaibceps[] = $veribc01 = aplicaredondeo($valpagoibc, $_POST['redondeoibc'], $_POST['nivelredondeo'], $_POST['tiporedondeoibc2']);
								$listaibcfdp[] = $veribc02 = aplicaredondeo($valpagoibc, $_POST['redondeoibc'], $_POST['nivelredondeo'], $_POST['tiporedondeoibc2']);
							}elseif($datosnovedad[3] == 'S' || $datosnovedad[4] == 'S'){
								$listaibceps[] = $veribc01 = aplicaredondeo($valpagoibc, $_POST['redondeoibc'], $_POST['nivelredondeo'], $_POST['tiporedondeoibc2']);
								$listaibcfdp[] = $veribc02=0;
							}elseif($datosnovedad[5] == 'S' || $datosnovedad[6] == 'S'){
								$listaibceps[] = $veribc01=0;
								$listaibcfdp[] = $veribc02 = aplicaredondeo($valpagoibc, $_POST['redondeoibc'], $_POST['nivelredondeo'], $_POST['tiporedondeoibc2']);
							}else{
								$listaibceps[] = $veribc01 = 0;
								$listaibcfdp[] = $veribc02 = 0;
							}
							if ($veribc01 > $veribc02){
								$listaibc[] = $veribc01;
							}else{
								$listaibc[] = $veribc01;
							}
							$totaldevengado += $listaauxalimentacion[] = 0;
							$totaldevengado += $listaauxtrasporte[] = 0;
							$totaldevengado += $listaotrospagos[] = 0;
							$listatotaldevengados[] = $totaldevengado;
							if($datosnovedad[8] == 'S' || $datosnovedad[9] == 'S' || $datosnovedad[10] == 'S' || $datosnovedad[11] == 'S' || $datosnovedad[12] == 'S'){
								$listabaseparafiscales[] = $ibcpara = aplicaredondeo($valpagoibc, $_POST['redondeoibcpara'], $_POST['nivelredondeo'], $_POST['tiporedondeoibc2']);
							}else {
								$listabaseparafiscales[] = $ibcpara = 0;
							}
							if($datosnovedad[7] == 'S'){
								$listabasearp[] = $ibcarp = aplicaredondeo($valpagoibc, $_POST['redondeoibcarp'], $_POST['nivelredondeo'], $_POST['tiporedondeoibc2']);
							}else {$listabasearp[] = $ibcarp = 0;}
							//calcular SALUD
							//if($row[16] == 'S')
							{
								$porcentaje1 = itemfuncionarios($row[5],'33');
								$porcentaje2 = itemfuncionarios($row[5],'32');
								$porcentajet = $porcentaje1 + $porcentaje2;
								$valsaludtot = ceil(($veribc01*$porcentajet)/10000)*100;
								if($datosnovedad[3] == 'S' && $datosnovedad[4] == 'S'){
									$listasaludempleado[] = $rsalud = round(($veribc01*$porcentaje2)/100);
									$listasaludempresa[] = $rsaludemp = $valsaludtot-$rsalud;
									$listasaludtotal[] = $valsaludtot;
								}elseif($datosnovedad[3] == 'S'){
									$listasaludempleado[] = $rsalud = $valsaludtot;
									$listasaludempresa[] = $rsaludemp = 0;
									$listasaludtotal[] = $valsaludtot;
								}elseif($datosnovedad[4] == 'S')
								{
									$listasaludempleado[] = $rsalud = 0;
									$listasaludempresa[] = $rsaludemp = $valsaludtot;
									$listasaludtotal[] = $valsaludtot;
								}else{
									$listasaludempleado[] = $rsalud = 0;
									$listasaludempresa[] = $rsaludemp = 0;
									$listasaludtotal[] = $valsaludtot = 0;
								}
								$listacuentapresuSR[] = $cuantapresu =  buscarcuentanuevocatalogo2($_POST['tsaludemr'], $datosnovedad[2], 'N/A');
								$fuentepre = buscarfuentespres($cuantapresu, $datosnovedad[0], $_POST['vigencia'], $datosnovedad[2], $row[5], $rsaludemp, $pfcp);
								$datosagrupados = explode('<_>', $fuentepre);
								$listafuentesSR[] = $datosagrupados[0];
								if($datosagrupados[1] != ''){
									$listaprogramaticoSR[] = $datosagrupados[1];
									$listaproyectoSR[] = $datosagrupados[2];
									$listabpinSR[] = nombrebpim($datosagrupados[2]);
								}else {
									$listaprogramaticoSR[] = $listaproyectoSR[] = $listabpinSR[] = '';
								}
								$cuentafuente = $datosnovedad[0].'<_>'.$cuantapresu.'<_>'.$fuentepre;
								$pfcp[$cuentafuente] += $rsaludemp;
							}
							//calular PENSION
							//if($row[17] == 'S')
							{
								$porcentaje1 = itemfuncionarios($row[5],'35');
								$porcentaje2 = itemfuncionarios($row[5],'34');
								$porcentajet = $porcentaje1 + $porcentaje2;
								$valpensiontot = ceil (($veribc02*$porcentajet)/10000)*100;
								if($datosnovedad[5] == 'S' && $datosnovedad[6] == 'S'){
									$listapensionempleado[] = $rpension = round(($veribc02*$porcentaje2)/100);
									$listapensionempresa[] = $rpensionemp = $valpensiontot - $rpension;
									$listapensiontotal[] = $valpensiontot;
								}elseif($datosnovedad[5] == 'S'){
									$listapensionempleado[] = $rpension = $valpensiontot;
									$listapensionempresa[] = $rpensionemp = 0;
									$listapensiontotal[] = $valpensiontot;
								}elseif($datosnovedad[6] == 'S'){
									$listapensionempleado[] = $rpension = 0;
									$listapensionempresa[] = $rpensionemp = $valpensiontot;
									$listapensiontotal[] = $valpensiontot;
								}else{
									$listapensionempleado[] = $rpension = 0;
									$listapensionempresa[] = $rpensionemp = 0;
									$listapensiontotal[] = $valpensiontot = 0;
								}
								$listacuentapresuPR[] = $cuantapresu = buscarcuentanuevocatalogo2($_POST['tpensionemr'], $datosnovedad[2], 'PR');
								$fuentepre = buscarfuentespres($cuantapresu, $datosnovedad[0], $_POST['vigencia'], $datosnovedad[2], $row[5], $rpensionemp, $pfcp);
								$listafuentesPR[] = $datosagrupados[0];
								if($datosagrupados[1] != ''){
									$listaprogramaticoPR[] = $datosagrupados[1];
									$listaproyectoPR[] = $datosagrupados[2];
									$listabpinPR[] = nombrebpim($datosagrupados[2]);
								}else {$listaprogramaticoPR[] = $listaproyectoPR[] = $listabpinPR[] = '';}
								$cuentafuente = $datosnovedad[0].'<_>'.$cuantapresu.'<_>'.$fuentepre;
								$pfcp[$cuentafuente] += $rpensionemp;
							}
							//calcular FONDO SOLIDARIDAD
							//if($row[17] == 'S')
							{
								$devendadofondo = calcularibcfs($_POST['idpreli'],$row[5]);
								$sqlrfs = "SELECT * FROM humfondosoli WHERE estado='S' AND $devendadofondo between (rangoinicial * ".$_POST['salmin'].") AND (rangofinal * ".$_POST['salmin'].")"; 
								$respfs = mysqli_query($linkbd,$sqlrfs);
								$rowfs = mysqli_fetch_row($respfs);
								$fondosol = 0;
								if($_POST['tipofondosol'] == 0){
									$fondosol = round((($rowfs[3]/2)/100)*(round($veribc02,-3,PHP_ROUND_HALF_DOWN)),-2)*2;
								}else {
									$fondosol = ceil(((($rowfs[3]/2)/100)*round($veribc02,-3,PHP_ROUND_HALF_DOWN))/100)*200;
								}
								$listafondosolidaridad[] = $fondosol;
							}
							//calcular ARL
							//if($row[18] == 'S')
							{
								if($datosnovedad[7] == 'S'){
									$porcentaje = porcentajearl($row[5]);
									$valdeci = (int) substr(number_format(($ibcarp*$porcentaje)/100,0),-2);
									if($valdeci > 5){
										$listaarp[] = $valorarl = ceil(($ibcarp*$porcentaje)/10000)*100;
									}else {
										$listaarp[] = $valorarl = round (($ibcarp*$porcentaje)/100,-2,PHP_ROUND_HALF_DOWN);
									}
								}
								else{
									$listaarp[] = $valorarl = 0;
								}
								$listacuentapresuarl[] = $cuantapresu = buscarcuentanuevocatalogo2($_POST['tarp'],$datosnovedad[2], 'N/A');
								$fuentepre = buscarfuentespres($cuantapresu, $datosnovedad[0], $_POST['vigencia'], $datosnovedad[2], $row[5], $valorarl, $pfcp);
								$datosagrupados = explode('<_>', $fuentepre);
								$listafuentesarl[] = $datosagrupados[0];
								if($datosagrupados[1] != ''){
									$listaprogramaticoarl[] = $datosagrupados[1];
									$listaproyectoarl[] = $datosagrupados[2];
									$listabpinarl[] = nombrebpim($datosagrupados[2]);
								}else {
									$listaprogramaticoarl[] = $listaproyectoarl[] = $listabpinarl[] = '';
								}
								$cuentafuente = $datosnovedad[0].'<_>'.$cuantapresu.'<_>'.$fuentepre;
								$pfcp[$cuentafuente] += $valorarl;
								$pf[$_POST['tarp']][$cuantapresu] += $valorarl;
								$verifica = esta_en_array($pfn[$_POST['tarp']], $cuantapresu);
								if($verifica == 0){$pfn[$_POST['tarp']][] = $cuantapresu;}
							}
							//calcular Retenciones
							{
								$sqlrete = "SELECT SUM(valorretencion) FROM hum_retencionesfun WHERE estado = 'S' AND estadopago = 'N' AND mes = '$row[3]' AND docfuncionario = '$row[7]' AND vigencia = '$row[4]' AND tipopago = '$row[2]'";
								$resrete = mysqli_query($linkbd,$sqlrete);
								$rowrete = mysqli_fetch_row($resrete);
								$listaretenciones[] = $valorretencion = $rowrete[0];

							}
							// calcular Descuentos
							{
								$sqlrds = "SELECT SUM(T1.valorcuota) FROM humretenempleados T1 WHERE T1.empleado = '$row[7]' AND T1.habilitado = 'H' AND T1.estado = 'S' AND T1.tipopago = '$row[2]' AND ncuotas > (SELECT COUNT(T2.id) FROM humnominaretenemp T2 WHERE T2.cedulanit = '$row[7]' AND T2.id = T1.id AND estado = 'P')";
								$respds = mysqli_query($linkbd,$sqlrds);
								$rowds = mysqli_fetch_row($respds);
								$listaotrasdeducciones[] = $otrasrete = round($rowds[0]);
							}
							//calcular total descuentos
							$listatotaldeducciones[] = $totalretenciones = $rsalud + $rpension + $otrasrete + $fondosol + $valorretencion;
							//calcular Neto a Pagar
							$listanetoapagar[] = $totalneto = $totaldevengado - $totalretenciones;
							//calcular CCF
							//if($row[19] == 'S')
							{
								if($datosnovedad[8] == 'S'){
									$porcentaje = itemfuncionarios($row[5],'36');
									$listaccf[] = $valccf = ceil(($ibcpara*$porcentaje)/10000)*100;
								}else {
									$listaccf[] = $valccf = 0;
								}
								$listacuentapresuccf[] = $cuantapresu = buscarcuentanuevocatalogo2($_POST['tcajacomp'], $datosnovedad[2], 'N/A');
								$fuentepre = buscarfuentespres($cuantapresu, $datosnovedad[0], $_POST['vigencia'], $datosnovedad[2], $row[5], $valccf, $pfcp);
								$datosagrupados = explode('<_>', $fuentepre);
								$listafuentesccf[] = $datosagrupados[0];
								if($datosagrupados[1] != ''){
									$listaprogramaticoccf[] = $datosagrupados[1];
									$listaproyectoccf[] = $datosagrupados[2];
									$listabpinccf[] = nombrebpim($datosagrupados[2]);
								}else {
									$listaprogramaticoccf[] = $listaproyectoccf[] = $listabpinccf[] = '';
								}
								$cuentafuente = $datosnovedad[0].'<_>'.$cuantapresu.'<_>'.$fuentepre;
								$pfcp[$cuentafuente] += $valccf;
								$pf[$_POST['tcajacomp']][$cuantapresu] += $valccf;
								$verifica = esta_en_array($pfn[$_POST['tcajacomp']], $cuantapresu);
								if($verifica == 0){
									$pfn[$_POST['tcajacomp']][] = $cuantapresu;
								}
							}
							//calcular SENA
							//if($row[19] == 'S')
							{
								if($datosnovedad[10] == 'S'){
									$porcentaje = itemfuncionarios($row[5],'38');
									$listasena[] = $valsena = ceil(($ibcpara*$porcentaje)/10000)*100;
								}else {
									$listasena[] = $valsena = 0;
								}
								$listacuentapresusena[] = $cuantapresu = buscarcuentanuevocatalogo2($_POST['tsena'], $datosnovedad[2], 'N/A');
								$fuentepre = buscarfuentespres($cuantapresu, $datosnovedad[0], $_POST['vigencia'], $datosnovedad[2], $row[5], $valsena, $pfcp);
								$datosagrupados = explode('<_>', $fuentepre);
								$listafuentessena[] = $datosagrupados[0];
								if($datosagrupados[1] != ''){
									$listaprogramaticosena[] = $datosagrupados[1];
									$listaproyectosena[] = $datosagrupados[2];
									$listabpinsena[] = nombrebpim($datosagrupados[2]);
								}else {
									$listaprogramaticosena[] = $listaproyectosena[] = $listabpinsena[] = '';
								}
								$cuentafuente = $datosnovedad[0].'<_>'.$cuantapresu.'<_>'.$fuentepre;
								$pfcp[$cuentafuente] += $valsena;
								$pf[$_POST['tsena']][$cuantapresu] += $valsena;
								$verifica = esta_en_array($pfn[$_POST['tsena']], $cuantapresu);
								if($verifica == 0){
									$pfn[$_POST['tsena']][] = $cuantapresu;
								}
							}
							//calcular ICBF
							//if($row[19] == 'S')
							{
								if($datosnovedad[9] == 'S'){
									$porcentaje = itemfuncionarios($row[5],'37');
									$listaicbf[] = $valicbf = ceil(($ibcpara*$porcentaje)/10000)*100;
								}else{
									$listaicbf[] = $valicbf = 0;
								}
								$listacuentapresuicbf[] = $cuantapresu = buscarcuentanuevocatalogo2($_POST['ticbf'], $datosnovedad[2], 'N/A');
								$fuentepre = buscarfuentespres($cuantapresu, $datosnovedad[0], $_POST['vigencia'], $datosnovedad[2], $row[5], $valicbf, $pfcp);
								$datosagrupados = explode('<_>', $fuentepre);
								$listafuentesicbf[] = $datosagrupados[0];
								if($datosagrupados[1] != ''){
									$listaprogramaticoicbf[] = $datosagrupados[1];
									$listaproyectoicbf[] = $datosagrupados[2];
									$listabpinicbf[] = nombrebpim($datosagrupados[2]);
								}else {
									$listaprogramaticoicbf[] = $listaproyectoicbf[] = $listabpinicbf[] = '';
								}
								$cuentafuente = $datosnovedad[0].'<_>'.$cuantapresu.'<_>'.$fuentepre;
								$pfcp[$cuentafuente] += $valicbf;
								$pf[$_POST['ticbf']][$cuantapresu] += $valicbf;
								$verifica = esta_en_array($pfn[$_POST['ticbf']], $cuantapresu);
								if($verifica == 0){
									$pfn[$_POST['ticbf']][] = $cuantapresu;
								}
							}
							//calcular INSTITUTOS TECNICOS
							//if($row[19] == 'S')
							{
								if($datosnovedad[11] == 'S'){
									$porcentaje = itemfuncionarios($row[5],'39');
									$listainstecnicos[] = $valinstec = ceil(($ibcpara*$porcentaje)/10000)*100;
								}else {
									$listainstecnicos[] = $valinstec = 0;
								}
								$listacuentapresutiti[] = $cuantapresu = buscarcuentanuevocatalogo2($_POST['titi'], $datosnovedad[2], 'N/A');
								$fuentepre = buscarfuentespres($cuantapresu, $datosnovedad[0], $_POST['vigencia'], $datosnovedad[2], $row[5], $valinstec, $pfcp);
								$datosagrupados = explode('<_>', $fuentepre);
								$listafuentestiti[] = $datosagrupados[0];
								if($datosagrupados[1] != ''){
									$listaprogramaticotiti[] = $datosagrupados[1];
									$listaproyectotiti[] = $datosagrupados[2];
									$listabpintiti[] = nombrebpim($datosagrupados[2]);
								}else {
									$listaprogramaticotiti[] = $listaproyectotiti[] = $listabpintiti[] = '';
								}
								$cuentafuente = $datosnovedad[0].'<_>'.$cuantapresu.'<_>'.$fuentepre;
								$pfcp[$cuentafuente] += $valinstec;
								$pf[$_POST['titi']][$row[9]] += $valinstec;
								$verifica = esta_en_array($pfn[$_POST['titi']],$cuantapresu);
								if($verifica == 0){
									$pfn[$_POST['titi']][] = $cuantapresu;
								}
							}
							//calcular ESAP
							//if($row[19] == 'S')
							{
								if($datosnovedad[11] == 'S'){
									$porcentaje = itemfuncionarios($row[5],'40');
									$listaesap[] = $valesap = ceil(($ibcpara*$porcentaje)/10000)*100;
								}else{
									$listaesap[] = $valesap = 0;
								}
								$listacuentapresuesap[] = $cuantapresu = buscarcuentanuevocatalogo2($_POST['tesap'], $datosnovedad[2], 'N/A'); 
								$fuentepre = buscarfuentespres($cuantapresu, $datosnovedad[0], $_POST['vigencia'], $datosnovedad[2], $row[5], $valesap, $pfcp);
								$datosagrupados = explode('<_>', $fuentepre);
								$listafuentesesap[] = $datosagrupados[0];
								if($datosagrupados[1] != ''){
									$listaprogramaticoesap[] = $datosagrupados[1];
									$listaproyectoesap[] = $datosagrupados[2];
									$listabpinesap[] = nombrebpim($datosagrupados[2]);
								}else {
									$listaprogramaticoesap[] = $listaproyectoesap[] = $listabpinesap[] = '';
								}
								$cuentafuente = $datosnovedad[0].'<_>'.$cuantapresu.'<_>'.$fuentepre;
								$pfcp[$cuentafuente] += $valesap;
								$pf[$_POST['tesap']][$cuantapresu] += $valesap;
								$verifica = esta_en_array($pfn[$_POST['tesap']], $cuantapresu);
								if($verifica == 0){
									$pfn[$_POST['tesap']][] = $cuantapresu;
								}
							}
						}
					}
					$listemp1 = array_filter($listatipopension, 'array_null');
					$listemp2 = array_filter($listapensiontotal, 'array_zero');
					if(count($listemp1) == count($listemp2) && count($listemp1) != 0){
						$vartemp = "S";
					}else{
						$vartemp = "N";
					}
					$vartemp = "S";
					$_POST['ttippen'] = $vartemp;
				}else{
					$_POST['saldocuentas'] = "0";
				}
			?>
			<div class="tabscontra" style="height:64%; width:99.6%">
				<!--Pestaña liquidación de Empleados-->
				<div class="tab">
					<input type="radio" id="tab-1" name="tabgroup1" value="1" <?php echo $check1;?>>
					<label for="tab-1">Liquidaci&oacute;n Individual</label>
					<div class="content">
						<table class='inicio' align='center' width='99%' id="tabla1">
							<tr>
								<th class='titulos2'>Id</th>
								<th class='titulos2'>TIPO</th>
								<th class='titulos2'>SECTOR</th>
								<th class='titulos2'>EMPLEADO</th>
								<th class='titulos2'>Doc Id</th>
								<th class='titulos2'>SAL BAS</th>
								<th class='titulos2'>DIAS LIQ</th>
								<th class='titulos2'>DEVENGADO</th>
								<th class='titulos2'>AUX ALIM</th>
								<th class='titulos2'>AUX TRAN</th>
								<th class='titulos2'>OTROS</th>
								<th class='titulos2'>TOT DEV</th>
								<th class='titulos2'>IBC</th>
								<th class='titulos2'>BASE PARAFISCALES</th>
								<th class='titulos2'>BASE ARP</th>
								<th class='titulos2'>ARP</th>
								<th class='titulos2'>SALUD EMPLEADO</th>
								<th class='titulos2'>SALUD EMPRESA</th>
								<th class='titulos2'>SALUD TOTAL</th>
								<th class='titulos2'>PENSION EMPLEADO</th>
								<th class='titulos2'>PENSION EMPRESA</th>
								<th class='titulos2'>PENSION TOTAL</th>
								<th class='titulos2'>FONDO SOLIDARIDAD</th>
								<th class='titulos2'>RETE FTE</th>
								<th class='titulos2'>OTRAS DEDUC</th>
								<th class='titulos2'>TOT DEDUC</th>
								<th class='titulos2'>NETO PAG</th>
								<th class='titulos2'>CCF</th>
								<th class='titulos2'>SENA</th>
								<th class='titulos2'>ICBF</th>
								<th class='titulos2'>INS. TEC.</th>
								<th class='titulos2'>ESAP</th>
							</tr>
							<?php
								if($_POST['idpreli']!="-1"){
									$iter="zebra1";
									$iter2="zebra2";
									for ($x=0;$x < count($listaempleados);$x++){
										if($listatipomov[$x] == 'Vacaciones'){
											$valornetor=0;
										}else {
											$valornetor = $listanetoapagar[$x];
										}
										if($listatipopago[$x] == '01'){
											$tipodepago = "$listatipomov[$x] -".nombrevariblespagonomina($listatipopago[$x]);
										}else {
											$tipodepago = nombrevariblespagonomina($listatipopago[$x]);
										}
										echo"
										<tr class='$iter'>
										<td style='text-align:right;'>".($x+1)."&nbsp;</td>
										<td>$tipodepago</td>
										<td>$listatipopension[$x]</td>
										<td>$listaempleados[$x]</td>
										<td style='text-align:right;'>$listadocumentos[$x]&nbsp;</td>
										<td style='text-align:right;' title='Salario Basico'>$".number_format($listasalariobasico[$x],0,',','.')."</td>
										<td style='text-align:right;'>$listadiasliquidados[$x]&nbsp;</td>
										<td style='text-align:right;' title='Salario Devengado'>$".number_format($listadevengados[$x],0,',','.')."</td>
										<td style='text-align:right;' title='Auxilio Alimentacion'>$".number_format($listaauxalimentacion[$x],0,',','.')."</td>
										<td style='text-align:right;' title='Auxilio Transporte'>$".number_format($listaauxtrasporte[$x],0,',','.')."</td>
										<td style='text-align:right;' title='Otros Pagos'>$".number_format($listaotrospagos[$x],0,',','.')."</td>
										<td style='text-align:right;' title='Total Devengado'>$".number_format($listatotaldevengados[$x],0,',','.')."</td>
										<td style='text-align:right;' title='IBC'>$".number_format($listaibc[$x],0,',','.')."</td>
										<td style='text-align:right;' title='Base Parafiscales'>$".number_format($listabaseparafiscales[$x],0,',','.')."</td>
										<td style='text-align:right;' title='Base ARP'>$".number_format($listabasearp[$x],0,',','.')."</td>
										<td style='text-align:right;' title='Valor ARP'>$".number_format($listaarp[$x],0,',','.')."</td>
										<td style='text-align:right;' title='Salud Empleado'>$".number_format($listasaludempleado[$x],0,',','.')."</td>
										<td style='text-align:right;' title='Salud Empresa'>$".number_format($listasaludempresa[$x],0,',','.')."</td>
										<td style='text-align:right;' title='Salud Total'>$".number_format($listasaludtotal[$x],0,',','.')."</td>
										<td style='text-align:right;' title='Pension Empleado'>$".number_format($listapensionempleado[$x],0,',','.')."</td>
										<td style='text-align:right;' title='Pension Empresa'>$".number_format($listapensionempresa[$x],0,',','.')."</td>
										<td style='text-align:right;' title='Pension Total'>$".number_format($listapensiontotal[$x],0,',','.')."</td>
										<td style='text-align:right;'>$".number_format($listafondosolidaridad[$x],0,',','.')."</td>
										<td style='text-align:right;' title='Retefunte'>$".number_format($listaretenciones[$x],0,',','.')."</td>
										<td style='text-align:right;' title='Otras Deducciones'>$".number_format($listaotrasdeducciones[$x],0,',','.')."</td>
										<td style='text-align:right;' title='Total Deducciones'>$".number_format($listatotaldeducciones[$x],0,',','.')."</td>
										<td style='text-align:right;' title='Neto a Pagar'>$".number_format($valornetor,0,',','.')."</td>
										<td style='text-align:right;' title='CCF'>$".number_format($listaccf[$x],0,',','.')."</td>
										<td style='text-align:right;' title='SENA'>$".number_format($listasena[$x],0,',','.')."</td>
										<td style='text-align:right;' title='ICBF'>$".number_format($listaicbf[$x],0,',','.')."</td>
										<td style='text-align:right;' title='Institutos Tecnicos'>$".number_format($listainstecnicos[$x],0,',','.')."</td>
										<td style='text-align:right;' title='ESAP'>$".number_format($listaesap[$x],0,',','.')."</td>
										</tr>";
										$aux=$iter;
										$iter=$iter2;
										$iter2=$aux;
									}
									//Calcula totales generales
									echo"
									<tr class='titulos2'>
										<td colspan='7'></td>
										<td style='text-align:right;'>$".number_format(array_sum($listadevengados),0,',','.')."</td>
										<td style='text-align:right;'>$".number_format(array_sum($listaauxalimentacion),0,',','.')."</td>
										<td style='text-align:right;'>$".number_format(array_sum($listaauxtrasporte),0,',','.')."</td>
										<td style='text-align:right;'>$".number_format(array_sum($listaotrospagos),0,',','.')."</td>
										<td style='text-align:right;'>$".number_format(array_sum($listatotaldevengados),0,',','.')."</td>
										<td style='text-align:right;'>$".number_format(array_sum($listaibc),0,',','.')."</td>
										<td style='text-align:right;'>$".number_format(array_sum($listabaseparafiscales),0,',','.')."</td>
										<td style='text-align:right;'>$".number_format(array_sum($listabasearp),0,',','.')."</td>
										<td style='text-align:right;'>$".number_format(array_sum($listaarp),0,',','.')."</td>
										<td style='text-align:right;'>$".number_format(array_sum($listasaludempleado),0,',','.')."</td>
										<td style='text-align:right;'>$".number_format(array_sum($listasaludempresa),0,',','.')."</td>
										<td style='text-align:right;'>$".number_format(array_sum($listasaludtotal),0,',','.')."</td>
										<td style='text-align:right;'>$".number_format(array_sum($listapensionempleado),0)."</td>
										<td style='text-align:right;'>$".number_format(array_sum($listapensionempresa),0,',','.')."</td>
										<td style='text-align:right;'>$".number_format(array_sum($listapensiontotal),0,',','.')."</td>
										<td style='text-align:right;'>$".number_format(array_sum($listafondosolidaridad),0,',','.')."</td>
										<td style='text-align:right;'>$".number_format(array_sum($listaretenciones),0,',','.')."</td>
										<td style='text-align:right;'>$".number_format(array_sum($listaotrasdeducciones),0,',','.')."</td>
										<td style='text-align:right;'>$".number_format(array_sum($listatotaldeducciones),0,',','.')."</td>
										<td style='text-align:right;'>$".number_format(array_sum($listanetoapagar),0,',','.')."</td>
										<td style='text-align:right;'>$".number_format(array_sum($listaccf),0,',','.')."</td>
										<td style='text-align:right;'>$".number_format(array_sum($listasena),0,',','.')."</td>
										<td style='text-align:right;'>$".number_format(array_sum($listaicbf),0,',','.')."</td>
										<td style='text-align:right;'>$".number_format(array_sum($listainstecnicos),0,',','.')."</td>
										<td style='text-align:right;'>$".number_format(array_sum($listaesap),0,',','.')."</td>
									</tr>";	
									$listatotalparafiscales[$_POST['tcajacomp']]=array_sum($listaccf);
									$listatotalparafiscales[$_POST['ticbf']]=array_sum($listaicbf);
									$listatotalparafiscales[$_POST['tsena']]=array_sum($listasena);
									$listatotalparafiscales[$_POST['titi']]=array_sum($listainstecnicos);
									$listatotalparafiscales[$_POST['tesap']]=array_sum($listaesap);
									$listatotalparafiscales[$_POST['tarp']]=array_sum($listaarp);
									$listatotalparafiscales[$_POST['tsaludemr']]=array_sum($listasaludempresa);
									$listatotalparafiscales[$_POST['tsaludemp']]=array_sum($listasaludempleado);
									$listatotalparafiscales[$_POST['tpensionemr']]=array_sum($listapensionempresa);
									$listatotalparafiscales[$_POST['tpensionemp']]=array_sum($listapensionempleado);
								}
							?>
						</table>
					</div>
				</div>
				<!--Pestaña liquidación de Empleados totales-->
				<div class="tab">
					<input type="radio" id="tab-8" name="tabgroup1" value="8" <?php echo $check8;?> >
					<label for="tab-8">Liquidaci&oacute;n Global</label>
					<div class="content" >
						<table class='inicio' align='center' width='99%' id="tabla1">
							<tr>
								<th class='titulos2'>Id</th>
								<th class='titulos2'>TIPO</th>
								<th class='titulos2'>SECTOR</th>
								<th class='titulos2'>EMPLEADO</th>
								<th class='titulos2'>Doc Id</th>
								<th class='titulos2'>SAL BAS</th>
								<th class='titulos2'>DIAS LIQ</th>
								<th class='titulos2'>DEVENGADO</th>
								<th class='titulos2'>AUX ALIM</th>
								<th class='titulos2'>AUX TRAN</th>
								<th class='titulos2'>OTROS</th>
								<th class='titulos2'>TOT DEV</th>
								<th class='titulos2'>IBC</th>
								<th class='titulos2'>BASE PARAFISCALES</th>
								<th class='titulos2'>BASE ARP</th>
								<th class='titulos2'>ARP</th>
								<th class='titulos2'>SALUD EMPLEADO</th>
								<th class='titulos2'>SALUD EMPRESA</th>
								<th class='titulos2'>SALUD TOTAL</th>
								<th class='titulos2'>PENSION EMPLEADO</th>
								<th class='titulos2'>PENSION EMPRESA</th>
								<th class='titulos2'>PENSION TOTAL</th>
								<th class='titulos2'>FONDO SOLIDARIDAD</th>
								<th class='titulos2'>RETE FTE</th>
								<th class='titulos2'>OTRAS DEDUC</th>
								<th class='titulos2'>TOT DEDUC</th>
								<th class='titulos2'>NETO PAG</th>
								<th class='titulos2'>CCF</th>
								<th class='titulos2'>SENA</th>
								<th class='titulos2'>ICBF</th>
								<th class='titulos2'>INS. TEC.</th>
								<th class='titulos2'>ESAP</th>
							</tr>
							<?php
								if($_POST['idpreli']!="-1"){
									$iter="zebra1";
									$iter2="zebra2";
									$vecmarca=array();
									$x=0;
									$y=1;
									foreach ($listatcodigofuncionario as &$codfunci){	
										$totaldias = $totaldeven = $totalauxalim = $totalauxtrans = $totalotrospag = $totalrdevenga = $totalsumibc = 0;
										$totalibcpara = $totalibcarl = $totalsumarl = $totalsaludempl = $totalsaludempr = $totalsalud = $totalpensionempl = 0;
										$totalpensionempr = $totalpension = $totalfondosol = $totalretefuente = $totalotrasdedu = $totaldeducciones = 0; 
										$totalnetoapagar = $totalccf = $totalsena = $totalicbf = $totalinstecnicos = $totalesap = 0;
										if (!in_array($listatcodigofuncionario[$x], $vecmarca)){
											$vecmarca[] = $listatcodigofuncionario[$x];
											for ($xy=0;$xy < count($listatcodigofuncionario);$xy++){
												if($listatcodigofuncionario[$x] == $listatcodigofuncionario[$xy]){
													$totaldias += $listadiasliquidados[$xy];
													$totaldeven += $listadevengados[$xy];
													$totalauxalim += $listaauxalimentacion[$xy];
													$totalauxtrans += $listaauxtrasporte[$xy];
													$totalotrospag += $listaotrospagos[$xy];
													$totalrdevenga += $listatotaldevengados[$xy];
													$totalsumibc += $listaibc[$xy];
													$totalibcpara += $listabaseparafiscales[$xy];
													$totalibcarl += $listabasearp[$xy];
													$totalsumarl += $listaarp[$xy];
													$totalsaludempl += $listasaludempleado[$xy];
													$totalsaludempr += $listasaludempresa[$xy];
													$totalsalud += $listasaludtotal[$xy];
													$totalpensionempl += $listapensionempleado[$xy];
													$totalpensionempr += $listapensionempresa[$xy];
													$totalpension += $listapensiontotal[$xy];
													$totalfondosol += $listafondosolidaridad[$xy];
													$totalretefuente += $listaretenciones[$xy];
													$totalotrasdedu += $listaotrasdeducciones[$xy];
													$totaldeducciones += $listatotaldeducciones[$xy];
													$totalnetoapagar += $listanetoapagar[$xy];
													$totalccf += $listaccf[$xy];
													$totalsena += $listasena[$xy];
													$totalicbf += $listaicbf[$xy];
													$totalinstecnicos += $listainstecnicos[$xy];
													$totalesap += $listaesap[$xy];
												}
											}
											echo"
											<tr class='$iter'>
											<td style='text-align:right;'>$y&nbsp;</td>
											<td>$listatipopago[$x]</td>
											<td>$listatipopension[$x]</td>
											<td>$listaempleados[$x]</td>
											<td style='text-align:right;'>$listadocumentos[$x]&nbsp;</td>
											<td style='text-align:right;' title='Salario Basico'>$".number_format($listasalariobasico[$x],0,',','.')."</td>
											<td style='text-align:right;'>$totaldias&nbsp;</td>
											<td style='text-align:right;' title='Salario Devengado'>$".number_format($totaldeven,0,',','.')."</td>
											<td style='text-align:right;' title='Auxilio Alimentacion'>$".number_format($totalauxalim,0,',','.')."</td>
											<td style='text-align:right;' title='Auxilio Transporte'>$".number_format($totalauxtrans,0,',','.')."</td>
											<td style='text-align:right;' title='Otros Pagos'>$".number_format($totalotrospag,0,',','.')."</td>
											<td style='text-align:right;' title='Total Devengado'>$".number_format($totalrdevenga,0,',','.')."</td>
											<td style='text-align:right;' title='IBC'>$".number_format($totalsumibc,0,',','.')."</td>
											<td style='text-align:right;' title='Base Parafiscales'>$".number_format($totalibcpara,0,',','.')."</td>
											<td style='text-align:right;' title='Base ARP'>$".number_format($totalibcarl,0,',','.')."</td>
											<td style='text-align:right;' title='Valor ARP'>$".number_format($totalsumarl,0,',','.')."</td>
											<td style='text-align:right;' title='Salud Empleado'>$".number_format($totalsaludempl,0,',','.')."</td>
											<td style='text-align:right;' title='Salud Empresa'>$".number_format($totalsaludempr,0,',','.')."</td>
											<td style='text-align:right;' title='Salud Total'>$".number_format($totalsalud,0,',','.')."</td>
											<td style='text-align:right;' title='Pension Empleado'>$".number_format($totalpensionempl,0,',','.')."</td>
											<td style='text-align:right;' title='Pension Empresa'>$".number_format($totalpensionempr,0,',','.')."</td>
											<td style='text-align:right;' title='Pension Total'>$".number_format($totalpension,0,',','.')."</td>
											<td style='text-align:right;' title='Fondo Solidaridad'>$".number_format($totalfondosol,0,',','.')."</td>
											<td style='text-align:right;' title='Retefunte'>$".number_format($totalretefuente,0,',','.')."</td>
											<td style='text-align:right;' title='Otras Deducciones'>$".number_format($totalotrasdedu,0,',','.')."</td>
											<td style='text-align:right;' title='Total Deducciones'>$".number_format($totaldeducciones,0,',','.')."</td>
											<td style='text-align:right;' title='Neto a Pagar'>$".number_format($totalnetoapagar,0,',','.')."</td>
											<td style='text-align:right;' title='CCF'>$".number_format($totalccf,0,',','.')."</td>
											<td style='text-align:right;' title='SENA'>$".number_format($totalsena,0,',','.')."</td>
											<td style='text-align:right;' title='ICBF'>$".number_format($totalicbf,0,',','.')."</td>
											<td style='text-align:right;' title='Institutos Tecnicos'>$".number_format($totalinstecnicos,0,',','.')."</td>
											<td style='text-align:right;' title='ESAP'>$".number_format($totalesap,0,',','.')."</td>
											</tr>";
											$y++;
											$aux=$iter;
											$iter=$iter2;
											$iter2=$aux;
										}
										$x++;
									}
									//Calcula totales generales
									echo"
									<tr class='titulos2'>
										<td colspan='7'></td>
										<td style='text-align:right;'>$".number_format(array_sum($listadevengados),0,',','.')."</td>
										<td style='text-align:right;'>$".number_format(array_sum($listaauxalimentacion),0,',','.')."</td>
										<td style='text-align:right;'>$".number_format(array_sum($listaauxtrasporte),0,',','.')."</td>
										<td style='text-align:right;'>$".number_format(array_sum($listaotrospagos),0,',','.')."</td>
										<td style='text-align:right;'>$".number_format(array_sum($listatotaldevengados),0,',','.')."</td>
										<td style='text-align:right;'>$".number_format(array_sum($listaibc),0,',','.')."</td>
										<td style='text-align:right;'>$".number_format(array_sum($listabaseparafiscales),0,',','.')."</td>
										<td style='text-align:right;'>$".number_format(array_sum($listabasearp),0,',','.')."</td>
										<td style='text-align:right;'>$".number_format(array_sum($listaarp),0,',','.')."</td>
										<td style='text-align:right;'>$".number_format(array_sum($listasaludempleado),0,',','.')."</td>
										<td style='text-align:right;'>$".number_format(array_sum($listasaludempresa),0,',','.')."</td>
										<td style='text-align:right;'>$".number_format(array_sum($listasaludtotal),0,',','.')."</td>
										<td style='text-align:right;'>$".number_format(array_sum($listapensionempleado),0)."</td>
										<td style='text-align:right;'>$".number_format(array_sum($listapensionempresa),0,',','.')."</td>
										<td style='text-align:right;'>$".number_format(array_sum($listapensiontotal),0,',','.')."</td>
										<td style='text-align:right;'>$".number_format(array_sum($listafondosolidaridad),0,',','.')."</td>
										<td style='text-align:right;'>$".number_format(array_sum($listaretenciones),0,',','.')."</td>
										<td style='text-align:right;'>$".number_format(array_sum($listaotrasdeducciones),0,',','.')."</td>
										<td style='text-align:right;'>$".number_format(array_sum($listatotaldeducciones),0,',','.')."</td>
										<td style='text-align:right;'>$".number_format(array_sum($listanetoapagar),0,',','.')."</td>
										<td style='text-align:right;'>$".number_format(array_sum($listaccf),0,',','.')."</td>
										<td style='text-align:right;'>$".number_format(array_sum($listasena),0,',','.')."</td>
										<td style='text-align:right;'>$".number_format(array_sum($listaicbf),0,',','.')."</td>
										<td style='text-align:right;'>$".number_format(array_sum($listainstecnicos),0,',','.')."</td>
										<td style='text-align:right;'>$".number_format(array_sum($listaesap),0,',','.')."</td>
									</tr>";	
									$listatotalparafiscales[$_POST['tcajacomp']]=array_sum($listaccf);
									$listatotalparafiscales[$_POST['ticbf']]=array_sum($listaicbf);
									$listatotalparafiscales[$_POST['tsena']]=array_sum($listasena);
									$listatotalparafiscales[$_POST['titi']]=array_sum($listainstecnicos);
									$listatotalparafiscales[$_POST['tesap']]=array_sum($listaesap);
									$listatotalparafiscales[$_POST['tarp']]=array_sum($listaarp);
									$listatotalparafiscales[$_POST['tsaludemr']]=array_sum($listasaludempresa);
									$listatotalparafiscales[$_POST['tsaludemp']]=array_sum($listasaludempleado);
									$listatotalparafiscales[$_POST['tpensionemr']]=array_sum($listapensionempresa);
									$listatotalparafiscales[$_POST['tpensionemp']]=array_sum($listapensionempleado);
								}
							?>
						</table>
					</div>
				</div>
				<!--Pestaña Aporte Parafiscales-->
				<div class="tab">
					<input type="radio" id="tab-2" name="tabgroup1" value="2" <?php echo $check2;?> >
					<label for="tab-2">Aportes Parafiscales</label>
					<div class="content" style="overflow:hidden">
						<table class="inicio">
							<tr>
								<td class="titulos" style="width:8%">Codigo</td>
								<td class="titulos" style="width:20%">Aportes Parafiscales</td>
								<td class="titulos" style="width:8%">Porcentaje</td>
								<td class="titulos" style="width:10%">Valor</td>
								<td class="titulos" >descripci&oacute;n</td>
							</tr>
							<?php
								if($_POST['idpreli']!="-1"){
									$iter="zebra1";
									$iter2="zebra2";
									$sqlr="SELECT * from humparafiscalesccpet WHERE estado = 'S' ORDER BY codigo";
									$resp2 = mysqli_query($linkbd,$sqlr);
									while($row2 =mysqli_fetch_row($resp2)){
										if($listatotalparafiscales[$row2[1]] > 0){
											echo "
											<tr class='$iter'>
												<input type='hidden' name='codpara[]' value='$row2[1]'/>
												<input type='hidden' name='codnpara[]' value='$row2[2]'/>
												<input type='hidden' name='porpara[]' value='$row2[4]'/>
												<input type='hidden' name='valpara[]' value='".$listatotalparafiscales[$row2[1]]."'/>
												<input type='hidden' name='tipopara[]' value='$row2[3]'/>
												<td>$row2[1]</td>
												<td>$row2[2]</td>
												<td style='text-align:right;'>$row2[4] %</td>
												<td style='text-align:right;'>$ ".number_format($listatotalparafiscales[$row2[1]],0)."&nbsp;</td>";
											if ($row2[3]=="A"){
												echo"<td>&nbsp;APORTES EMPRESA</td>";
											}else{
												echo"<td>&nbsp;APORTE EMPLEADOS</td>";
											}
											echo"	</tr>";
											$aux=$iter;
											$iter=$iter2;
											$iter2=$aux;
										}
									}
									echo "
									<tr class='$iter'>
										<td></td>
										<td>TOTAL SALUD </td>
										<td style='text-align:right;'>12.5 %</td>
										<td style='text-align:right;'>$ ".number_format(array_sum($listasaludtotal),2)."</td>
										<td></td>
									</tr>";
									$aux=$iter;
									$iter=$iter2;
									$iter2=$aux;
									echo"
									<tr class='$iter'>
										<td></td>
										<td>TOTAL PENSION: </td>
										<td style='text-align:right;'>16 %</td>
										<td style='text-align:right;'>".number_format(array_sum($listapensiontotal),2)."</td>
										<td></td>
									</tr>";
								}
							?>
						</table>
					</div>
				</div>
				<!--Pestaña Presupuesto-->
				<div class="tab">
					<input type="radio" id="tab-3" name="tabgroup1" value="3" <?php echo $check3;?> >
					<label for="tab-3">Presupuesto</label>
					<div class="content" style="overflow-x:hidden">
						<table class="inicio">
							<tr>
								<td class="titulos">Tipo Gasto</td>
								<td class="titulos">Sec. Presupuestal</td>
								<td class="titulos">Medio de Pago</td>
								<td class="titulos">Vig. de Gasto</td>
								<td class="titulos">Proyecto</td>
								<td class="titulos">Programatico</td>
								<td class="titulos">Cuenta Presupuestal</td>
								<td class="titulos">Nombre Cuenta Presupuestal</td>
								<td class="titulos">Fuente</td>
								<td class="titulos">BPIM</td>
								<td class="titulos">Valor</td>
								<td class="titulos" style='width:5%'>Saldo</td>
							</tr>
							<?php
								$totalrubro=0;
								$iter="zebra1";
								$iter2="zebra2";
								foreach($pfcp as $k => $valrubros){
									$datoscuenta = explode('<_>', $k);
									$ncta = nombrecuentapresu($datoscuenta[1]);
									if($valrubros>0){
										if($datoscuenta[4] != ''){
											$nomproyec = nombreproyecto($datoscuenta[4]);
											$numbpim = nombrebpim($datoscuenta[4]);
											$numindicador = $datoscuenta[3];
										}else {
											$nomproyec = '';
											$numbpim = '';
											$numindicador = '';
										}
										$valfuente = $valrubros;
										$tipogasto = substr($datoscuenta[1],0,3);
										$mediopago = 'CSF';
										$viggasto = '1';
										$parametros = array(
											'rubro' => $datoscuenta[1],
											'fuente' => $datoscuenta[2],
											'vigencia' => $_POST['vigencia'],
											'tipo_gasto' => substr($datoscuenta[1],2,1),
											'seccion_presupuestal' => $datoscuenta[0],
											'medio_pago' => $mediopago,
											'vigencia_gasto' => '1',
											'codProyecto' => $numbpim,
											'programatico' => $numindicador
										);
										$valsaldo = saldoPorRubro($parametros);
										if($valfuente > $valsaldo){
											$_POST['saldocuentas'] = "1";
											$saldo="SIN SALDO";
											$color=" style='text-align:center;background-color :#901; color:#fff' ";

										}else{
											$saldo = "OK";
											$color = " style='text-align:center;background-color :#092; color:#fff' ";

										}
										
										echo "
										<input type='hidden' name='valindicadorc[]' value='$numindicador'/>
										<input type='hidden' name='valbpinc[]' value='$numbpim'/>
										<input type='hidden' name='secpresu[]' value='$datoscuenta[0]'/>
										<input type='hidden' name='rubrosp[]' value='$datoscuenta[1]'/>
										<input type='hidden' name='nrubrosp[]' value='".strtoupper($ncta)."'/>
										<input type='hidden' name='fuente[]' value='$datoscuenta[2]'/>
										<input type='hidden' name='nomproyecto[]' value='$nomproyec'/>
										<input type='hidden' name='numproyecto[]' value='$datoscuenta[4]'/>
										<input type='hidden' name='vrubrosp[]' value='$valfuente'/>
										<input type='hidden' name='vsaldo[]' value='$saldo'/>
										<tr class='$iter'>
											<td>$tipogasto</td>
											<td>$datoscuenta[0]</td>
											<td>CSF</td>
											<td>1 - Vigencia Actual</td>
											<td>$nomproyec</td>
											<td>$numindicador</td>
											<td>$datoscuenta[1]</td>
											<td>".strtoupper($ncta)."</td>
											<td>$datoscuenta[2]</td>
											<td>$numbpim</td>
											<td style='text-align:right;'>".number_format($valfuente,0)."</td>
											<td $color>".$saldo."</td>
										</tr>";
										$totalrubro += $valfuente;
										$aux = $iter;
										$iter = $iter2;
										$iter2 = $aux;
									}
								}
								echo"
								<tr class='$iter'>
									<td></td>
									<td style='text-align:right;'>Total:</td>
									<td style='text-align:right;'>".number_format($totalrubro,2)."</td>
								</tr>";
							?> 
						</table>
					</div>
				</div>
				<!--Pestaña Vacaciones-->
				<div class="tab">
					<input type="radio" id="tab-4" name="tabgroup1" value="4" <?php echo $check4;?> >
					<label for="tab-4">Vacaciones</label>
					<div class="content" >
						<table class="inicio">
							<tr>
								<td class="titulos">No</td>
								<td class="titulos">Documento</td>
								<td class="titulos">Nombre</td>
								<td class="titulos">Salario Basico</td>
								<td class="titulos">Salario dia</td>
								<td class="titulos">Valor</td>
								<td class="titulos">Fecha Inicio</td>
								<td class="titulos">Fecha Final</td>
								<td class="titulos" style="width:5%;">Dias Vacaciones</td>
								<td class="titulos" style="width:5%;">Paga SyP</td>
								<td class="titulos" style="width:5%;">Paga ARL</td>
								<td class="titulos" style="width:5%;">Paga Parafiscales</td>
							</tr>
							<?php
								if($_POST['idpreli']!="-1"){
									$iter = "zebra1";
									$iter2 = "zebra2";
									$sqlr = "SELECT mes,vigencia,salarifun,documefun,nombrefun FROM hum_prenomina_det WHERE codigo = '".$_POST['idpreli']."' AND diasv > 0 ORDER BY id_det"; 
									$resp = mysqli_query($linkbd,$sqlr);
									while ($row = mysqli_fetch_row($resp)){
										$sqlr2 = "SELECT * FROM hum_vacaciones_det WHERE mes = '$row[0]' AND vigencia = '$row[1]' AND doc_funcionario = '$row[3]' AND estado = 'S'";
										$resp2 = mysqli_query($linkbd,$sqlr2);
										while($row2 = mysqli_fetch_row($resp2)){
											$sqlr3 = "SELECT fecha_ini,fecha_fin FROM hum_vacaciones WHERE num_vaca = '$row2[0]'";
											$resp3 = mysqli_query($linkbd,$sqlr3);
											$row3 = mysqli_fetch_row($resp3);
											if($row2[8] == "S"){
												$imasem1 = "<img src='imagenes/sema_verdeON.jpg' title='Si Paga' style='width:20px'/>";
											}else{
												$imasem1 = "<img src='imagenes/sema_rojoON.jpg' title='No Paga' style='width:20px'/>";
											}
											if($row2[9] == "S"){
												$imasem2 = "<img src='imagenes/sema_verdeON.jpg' title='Si Paga' style='width:20px'/>";
											}else {
												$imasem2 = "<img src='imagenes/sema_rojoON.jpg' title='No Paga' style='width:20px'/>";
											}
											if($row2[10] == "S"){
												$imasem3 = "<img src='imagenes/sema_verdeON.jpg' title='Si Paga' style='width:20px'/>";
											}else {
												$imasem3 = "<img src='imagenes/sema_rojoON.jpg' title='No Paga' style='width:20px'/>";
											}
											echo"
											<tr class='$iter'>
												<td>$row2[0]</td>
												<td>$row[3]</td>
												<td>$row[4]</td>
												<td style='text-align:right;'>$".number_format($row[2],0,',','.')."</td>
												<td style='text-align:right;'>$".number_format($row2[6],0,',','.')."</td>
												<td style='text-align:right;'>$".number_format($row2[7],0,',','.')."</td>
												<td style='text-align:center;'>".date('d-m-Y',strtotime($row3[0]))."</td>
												<td style='text-align:center;'>".date('d-m-Y',strtotime($row3[1]))."</td>
												<td style='text-align:right;'>$row2[5]</td>
												<td style='text-align:center;'>$imasem1</td>
												<td style='text-align:center;'>$imasem2</td>
												<td style='text-align:center;'>$imasem3</td>
											</tr>";	
											$aux=$iter;
											$iter=$iter2;
											$iter2=$aux;
										}
									}
								}
							?>
						</table>
					</div>
				</div>
				<!--Pestaña Incapacidades-->
				<div class="tab">
					<input type="radio" id="tab-5" name="tabgroup1" value="5" <?php echo $check5;?> >
					<label for="tab-5">Incapacidades</label>
					<div class="content" >
						<table class="inicio">
							<tr>
								<td class="titulos">No</td>
								<td class="titulos">Tipo</td>
								<td class="titulos">Documento</td>
								<td class="titulos">Nombre</td>
								<td class="titulos">Dias</td>
								<td class="titulos">%</td>
								<td class="titulos">Salario Basico</td>
								<td class="titulos">Valor Dia</td>
								<td class="titulos">Valor total</td>
								<td class="titulos" style="width:5%;">Paga SyP</td>
								<td class="titulos" style="width:5%;">Paga ARL</td>
								<td class="titulos" style="width:5%;">Paga Parafiscales</td>
							</tr>
							<?php
								if($_POST['idpreli'] != "-1"){
									$iter = "zebra1";
									$iter2 = "zebra2";
									$sqlr = "SELECT mes, vigencia, salarifun, documefun, nombrefun FROM hum_prenomina_det WHERE codigo = '".$_POST['idpreli']."' AND diasi > 0 ORDER BY id_det";
									$resp = mysqli_query($linkbd,$sqlr);
									while ($row = mysqli_fetch_row($resp)){
										$sqlr2 = "SELECT * FROM hum_incapacidades_det WHERE mes = '$row[0]' AND vigencia = '$row[1]' AND doc_funcionario = '$row[3]' AND estado = 'S'";
										$resp2 = mysqli_query($linkbd,$sqlr2);
										while($row2 = mysqli_fetch_row($resp2)){
											if($row2[10] == "S"){
												$imasem4 = "<img src='imagenes/sema_verdeON.jpg' title='Si Paga' style='width:20px'>";
											}else{
												$imasem4 = "<img src='imagenes/sema_rojoON.jpg' title='No Paga' style='width:20px'>";
											}
											if($row2[11] == "S"){
												$imasem5 = "<img src='imagenes/sema_verdeON.jpg' title='Si Paga' style='width:20px'>";
											}else{
												$imasem5 = "<img src='imagenes/sema_rojoON.jpg' title='No Paga' style='width:20px'>";
											}
											if($row2[12] == "S"){
												$imasem6 = "<img src='imagenes/sema_verdeON.jpg' title='Si Paga' style='width:20px'>";
											}else{
												$imasem6 = "<img src='imagenes/sema_rojoON.jpg' title='No Paga' style='width:20px'/>";
											}
											echo"
											<tr class='$iter'>
												<td>$row2[2]</td>
												<td>$row2[3]</td>
												<td>$row[3]</td>
												<td>$row[4]</td>
												<td style='text-align:right;'>$row2[6]</td>
												<td style='text-align:right;'>$row2[7]%</td>
												<td style='text-align:right;'>$".number_format($row[2],0,',','.')."</td>
												<td style='text-align:right;'>$".number_format($row2[8],0,',','.')."</td>
												<td style='text-align:right;'>$".number_format($row2[9],0,',','.')."</td>
												<td style='text-align:center;'>$imasem4</td>
												<td style='text-align:center;'>$imasem5</td>
												<td style='text-align:center;'>$imasem6</td>
											</tr>";
											$aux=$iter;
											$iter=$iter2;
											$iter2=$aux;
										}
									}
								}
							?>
						</table>
					</div>
				</div>
				<!--Pestaña Descuentos-->
				<div class="tab">
					<input type="radio" id="tab-6" name="tabgroup1" value="6" <?php echo $check6;?>>
					<label for="tab-6">Descuentos</label>
					<div class="content" >
						<table class="inicio">
							<tr>
								<td class="titulos">No</td>
								<td class="titulos">Fecha Registro</td>
								<td class="titulos">Documento</td>
								<td class="titulos">Nombre</td>
								<td class="titulos">Descripcion</td>
								<td class="titulos">Valor</td>
								<td class="titulos">No Cuota</td>
							</tr>
							<?php
								if($_POST['idpreli'] != "-1"){
									$iter="zebra1";
									$iter2="zebra2";
									$con=1;
									$sqlr = "SELECT mes, vigencia, salarifun, documefun, nombrefun FROM hum_prenomina_det WHERE codigo = '".$_POST['idpreli']."' ORDER BY id_det";
									$resp = mysqli_query($linkbd,$sqlr);
									while ($row = mysqli_fetch_row($resp))
									{
										$sqlr2 = "SELECT T1.* FROM humretenempleados T1 WHERE T1.empleado = '$row[3]' AND T1.habilitado = 'H' AND T1.estado = 'S' AND tipopago = '01' AND ncuotas > (SELECT COUNT(T2.id) FROM humnominaretenemp T2 WHERE T2.cedulanit = '$row[3]' AND T2.id = T1.id AND estado = 'P') ORDER BY T1.fecha, T1.descripcion";
										$resp2 = mysqli_query($linkbd,$sqlr2);
										while($row2 = mysqli_fetch_row($resp2)){
											$sqlrdes = "SELECT COUNT(*) FROM humnominaretenemp WHERE cedulanit = '$row[3]' AND id = '$row2[0]' AND estado = 'P'";
											$respdes = mysqli_query($linkbd,$sqlrdes);
											$rowdes = mysqli_fetch_row($respdes);
											$numcot = $rowdes[0] + 1;
											echo "
											<tr class='$iter'>
												<td>$con</td>
												<td>$row2[3]</td>
												<td>$row[3]</td>
												<td>$row[4]</td>
												<td>$row2[1]</td>
												<td>$row2[8]</td>
												<td style='text-align:center;'>$numcot / $row2[6]</td>
											</tr>";
											$aux = $iter;
											$iter = $iter2;
											$iter2 = $aux;
											$con++;
										}
									}
								}
							?>
						</table>
					</div>
				</div>
				<!--Pestaña Retenciones-->
				<div class="tab">
					<input type="radio" id="tab-7" name="tabgroup1" value="7" <?php echo $check7;?> >
					<label for="tab-7">Retenciones</label>
					<div class="content" >
						<table class="inicio">
							<tr>
								<td class="titulos">No</td>
								<td class="titulos">Fecha Registro</td>
								<td class="titulos">Documento</td>
								<td class="titulos">Nombre</td>
								<td class="titulos">Valor</td>
							</tr>
							<?php
								if($_POST['idpreli'] != "-1"){
									$iter = "zebra1";
									$iter2 = "zebra2";
									$con=1;
									$sqlrxre = "SELECT * FROM hum_retencionesfun WHERE estadopago = 'N' AND estado = 'S' AND mes = '".$_POST['mesnomina']."'";
									$respli = mysqli_query($linkbd,$sqlrxre);
									while ($rowr = mysqli_fetch_row($respli)){
										echo "<tr class='$iter'>
											<td>$con</td>
											<td>$rowr[1]</td>
											<td>$rowr[3]</td>
											<td>$rowr[4]</td>
											<td>$rowr[6]</td>
											</tr>";
										$aux=$iter;
										$iter=$iter2;
										$iter2=$aux;
										$con++;
										
									}
								}
							?>
						</table>
					</div>
				</div>
			</div> 
			<?php
				//Listados para exportar informacion
				echo"
				<input type='hidden' name='lista_codigofuncionario' value='".serialize($listatcodigofuncionario)."'/>
				<input type='hidden' name='lista_empleados' value='".serialize($listaempleados)."'/>
				<input type='hidden' name='lista_documento' value='".serialize($listadocumentos)."'/>
				<input type='hidden' name='lista_salariobasico' value='".serialize($listasalariobasico)."'/>
				<input type='hidden' name='lista_devengados' value='".serialize($listadevengados)."'/>
				<input type='hidden' name='lista_auxalimentacion' value='".serialize($listaauxalimentacion)."'/>
				<input type='hidden' name='lista_auxtrasporte' value='".serialize($listaauxtrasporte)."'/>
				<input type='hidden' name='lista_otrospagos' value='".serialize($listaotrospagos)."'/>
				<input type='hidden' name='lista_totaldevengados' value='".serialize($listatotaldevengados)."'/>
				<input type='hidden' name='lista_ibc' value='".serialize($listaibc)."'/>
				<input type='hidden' name='lista_baseparafiscales' value='".serialize($listabaseparafiscales)."'/>
				<input type='hidden' name='lista_basearp' value='".serialize($listabasearp)."'/>
				<input type='hidden' name='lista_arp' value='".serialize($listaarp)."'/>
				<input type='hidden' name='lista_saludempleado' value='".serialize($listasaludempleado)."'/>
				<input type='hidden' name='lista_saludempresa' value='".serialize($listasaludempresa)."'/>
				<input type='hidden' name='lista_saludtotal' value='".serialize($listasaludtotal)."'/>
				<input type='hidden' name='lista_pensionempleado' value='".serialize($listapensionempleado)."'/>
				<input type='hidden' name='lista_pensionempresa' value='".serialize($listapensionempresa)."'/>
				<input type='hidden' name='lista_pensiontotal' value='".serialize($listapensiontotal)."'/>
				<input type='hidden' name='lista_fondosolidaridad' value='".serialize($listafondosolidaridad)."'/>
				<input type='hidden' name='lista_retenciones' value='".serialize($listaretenciones)."'/>
				<input type='hidden' name='lista_otrasdeducciones' value='".serialize($listaotrasdeducciones)."'/>
				<input type='hidden' name='lista_totaldeducciones' value='".serialize($listatotaldeducciones)."'/>
				<input type='hidden' name='lista_netoapagar' value='".serialize($listanetoapagar)."'/>
				<input type='hidden' name='lista_ccf' value='".serialize($listaccf)."'/>
				<input type='hidden' name='lista_sena' value='".serialize($listasena)."'/>
				<input type='hidden' name='lista_icbf' value='".serialize($listaicbf)."'/>
				<input type='hidden' name='lista_instecnicos' value='".serialize($listainstecnicos)."'/>
				<input type='hidden' name='lista_esap' value='".serialize($listaesap)."'/>
				<input type='hidden' name='lista_diasincapacidad' value='".serialize($listadiasincapacidad)."'/>
				<input type='hidden' name='lista_diaslaborados' value='".serialize($listadiasliquidados)."'/>";
			?>
			<input type="hidden" name="ttippen" id="ttippen" value="<?php echo $_POST['ttippen'];?>">
			<input type="hidden" name="saldocuentas" id="saldocuentas" value="<?php echo $_POST['saldocuentas'];?>">
			<input type="hidden" name="oculto" id="oculto" value="10">
			
			<?php
				$sql = "SELECT num_liq FROM hum_prenomina WHERE codigo = '".$_POST['idpreli']."'";
				$res = mysqli_query($linkbd,$sql);
				$row = mysqli_fetch_row($res);
				if(($_POST['oculto'] == 2) && ($row[0] == "0")){
					if(($_POST['oculto'] == 2)){
						$_POST['idcomp'] = selconsecutivo('humnomina','id_nom');
						preg_match("/([0-9]{2})\/([0-9]{2})\/([0-9]{4})/", $_POST['fecha'],$fecha);
						$fechaf = "$fecha[3]-$fecha[2]-$fecha[1]";
						$sqlrcn = "SELECT mes,vigencia FROM hum_prenomina WHERE codigo='".$_POST['idpreli']."' "; 
						$respcn = mysqli_query($linkbd,$sqlrcn);
						$rowcn = mysqli_fetch_row($respcn);
						$sqlr = "INSERT INTO humnomina (id_nom, fecha, periodo, mes, diasp, mesnum, cc, vigencia, estado) VALUES ('".$_POST['idcomp']."', '$fechaf', '1', '$rowcn[0]', '0', '1', '00', '$rowcn[1]', 'S')";
						if (!mysqli_query($linkbd,$sqlr)){
							echo "
							<script>
								Swal.fire(
									'Error!',
									'No se pudo almacenar la Nomina',
									'error'
								);
							</script>
							";
						}else{
							$id = $_POST['idcomp'];
							$idconec = selconsecutivo('hum_nom_cdp_rp','id');
							$sqlrco = "INSERT INTO hum_nom_cdp_rp (id, nomina, cdp, rp, codaprobado, vigencia, estado) VALUES ('$idconec', '$id', '0', '0', '0', '$rowcn[1]', 'S')";
							mysqli_query($linkbd,$sqlrco);
							$sqlrpre = "UPDATE hum_prenomina SET num_liq = '".$_POST['idcomp']."' WHERE codigo = '".$_POST['idpreli']."'";
							mysqli_query($linkbd,$sqlrpre);
							$lastday = mktime (0,0,0,$rowcn[0],1,$rowcn[1]);
							$fecha_dt = new DateTime();
							$fecha_dt->setTimestamp($lastday);
							$nombre_mes = $fecha_dt->format('F');
							echo "<table class='inicio'><tr><td class='saludo1'><center>Se ha almacenado la Liquidacion de la Nomina $id - Centro Costo: $_POST[cc] - Mes: " . strtoupper($nombre_mes) . " <img src='imagenes\confirm.png'></center></td></tr></table>";
							$cex = 0;
							$cerr = 0;
							$eps = "";
							$arp = "";
							$afp = "";
							$tipoafp = "";
							$x = 0;
							for ($x=0; $x < count($listasalariobasico); $x++){
								$perpagoi = "$rowcn[1]-$rowcn[0]-01";
								$fechar = new DateTime($perpagoi);
								$fechar->modify('last day of this month');
								$fechat = $fechar->format('Y-m-d');
								$perpagof = $fechat;
								//$idlist = selconsecutivo('hum_salarios_nom','id_list');
								//$cupres = buscacuentapresupuestalccp($listatipopago[$x],$listatcodigofuncionario[$x],'1');
								//$sqlry1 = "INSERT INTO hum_salarios_nom (id_list, id_nom, empleado, tercero, cc, cuentap, valorneto, valordevengado, fecha, periodopagoi, periodopagof, tipo_item, estado, tipo_mov) VALUES ('$idlist', '$id', '$listatcodigofuncionario[$x]', '$listadocumentos[$x]', '$listacentrocosto[$x]', '$cupres', '$listanetoapagar[$x]', '$listadevengados[$x]', '', '$perpagoi', '$perpagof', '$listatipopago[$x]', 'S', '201')";
								$consid = selconsecutivo('humnomina_det','id');
								$sqlr = "INSERT INTO humnomina_det (id_nom, cedulanit, salbas, diaslab, devendias, ibc, auxalim, auxtran, valhorex, totaldev, salud, saludemp, pension, pensionemp, fondosolid, retefte, otrasdeduc, totaldeduc, netopagar, estado, vac, diasarl, cajacf, sena, icbf, instecnicos, esap, tipofondopension, basepara, basearp, arp, totalsalud, totalpension, prima_navi, cc, id, tipopago, detalle, idfuncionario, fuente, indicador_producto, bpin, productoservicio, medio_pago, codigo_politicap, tipo_mov, proyecto, seccion_presupuestal, cuenta_presu) VALUES ('$id', '$listadocumentos[$x]', '$listasalariobasico[$x]', '$listadiasliquidados[$x]', '$listadevengados[$x]', '$listaibc[$x]', '$listaauxalimentacion[$x]', '$listaauxtrasporte[$x]', '0', '$listatotaldevengados[$x]', '$listasaludempleado[$x]', '$listasaludempresa[$x]', '$listapensionempleado[$x]', '$listapensionempresa[$x]', '$listafondosolidaridad[$x]', '$listaretenciones[$x]', '$listaotrasdeducciones[$x]', '$listatotaldeducciones[$x]','$listanetoapagar[$x]','S','1', '$listadiasincapacidad[$x]', '$listaccf[$x]', '$listasena[$x]', '$listaicbf[$x]', '$listainstecnicos[$x]', '$listaesap[$x]', '$listatipopension[$x]', '$listabaseparafiscales[$x]', '$listabasearp[$x]', '$listaarp[$x]', '$listasaludtotal[$x]', '$listapensiontotal[$x]', '$listaprimasnavidad[$x]', '$listacentrocosto[$x]', '$consid', '$listatipopago[$x]', '$listatipomov[$x]', '$listatcodigofuncionario[$x]', '$listafuentes[$x]', '$listaprogramatico[$x]', '$listabpin[$x]', '', 'CSF', '1', '201', '$listaproyecto[$x]', '$listasecpresu[$x]', '$listacuentapresu[$x]')";
								if (!mysqli_query($linkbd,$sqlr)){$cerr+=1;}
								else{$cex+=1;}
								if($listasaludempleado[$x] > 0){//********SALUD EMPLEADO *****
								
									$idsalud = selconsecutivo('humnomina_saludpension','id');
									$sqlrins = "INSERT INTO humnomina_saludpension (id_nom, tipo, empleado, tercero, cc, valor, estado, sector, id, cod_fun, cuenta_presu, tipopago, fuente, indicador_producto, bpin, productoservicio, medio_pago, codigo_politicap, tipo_mov, proyecto, seccion_presupuestal) VALUES ($id, 'SE', '$listadocumentos[$x]', '$listadoceps[$x]', '$listacentrocosto[$x]', '$listasaludempleado[$x]', 'S', '', '$idsalud', '$listatcodigofuncionario[$x]', '$listacuentapresuSE[$x]', '$listatipopago[$x]', '$listafuentesSE[$x]', '$listaprogramaticoSE[$x]', '$listabpinSE[$x]', '', 'CSF', '1', '201', '$listaproyectoSE[$x]', '$listasecpresu[$x]')";
									mysqli_query($linkbd,$sqlrins);
								}
								if($listapensionempleado[$x] > 0){//********PENSION EMPLEADO *****
									$idsalud = selconsecutivo('humnomina_saludpension','id');
									$sqlrins = "INSERT INTO  humnomina_saludpension (id_nom, tipo, empleado, tercero, cc, valor, estado, sector, id, cod_fun, cuenta_presu, tipopago, fuente, indicador_producto, bpin, productoservicio, medio_pago, codigo_politicap, tipo_mov, proyecto, seccion_presupuestal) VALUES ($id, 'PE', '$listadocumentos[$x]', '$listadocafp[$x]', '$listacentrocosto[$x]', '$listapensionempleado[$x]', 'S', '$listatipopension[$x]', '$idsalud', '$listatcodigofuncionario[$x]', '$listacuentapresuPE[$x]', '$listatipopago[$x]', '$listafuentesPE[$x]', '$listaprogramaticoPE[$x]', '$listabpinPE[$x]', '', 'CSF', '1', '201', '$listaproyectoPE[$x]', '$listasecpresu[$x]')";
									mysqli_query($linkbd,$sqlrins);
								}
								if($listafondosolidaridad[$x]>0){//********FONDO SOLIDARIDAD EMPLEADO *****
									$idsalud = selconsecutivo('humnomina_saludpension','id');
									$sqlrins = "INSERT INTO  humnomina_saludpension (id_nom, tipo, empleado, tercero, cc,valor, estado, sector, id, cod_fun, cuenta_presu, tipopago, fuente, indicador_producto, bpin, productoservicio, medio_pago, codigo_politicap, tipo_mov, proyecto, seccion_presupuestal) VALUES ($id, 'FS', '$listadocumentos[$x]', '$listadocafp[$x]', '$listacentrocosto[$x]', '$listafondosolidaridad[$x]', 'S','$listatipopension[$x]', '$idsalud', '$listatcodigofuncionario[$x]', '$listacuentapresuFS[$x]', '$listatipopago[$x]', '$listafuentesFS[$x]', '$listaprogramaticoFS[$x]', '$listabpinFS[$x]', '', 'CSF', '1', '201', '$listaproyectoFS[$x]', '$listasecpresu[$x]')";
									mysqli_query($linkbd,$sqlrins);
								}
								if($listaretenciones[$x]>0){//********RETENCIONES *****
									$sqlrxre = "SELECT * FROM hum_retencionesfun WHERE docfuncionario = '$listadocumentos[$x]' AND estadopago = 'N' AND estado = 'S' AND mes = '".$rowcn[0]."'";
									$respli = mysqli_query($linkbd,$sqlrxre);
									while ($rowh = mysqli_fetch_row($respli)){
										$iddescu = selconsecutivo('humnominaretenemp','id_des');
										$numcot = 1;
										$sqlret = "INSERT INTO humnominaretenemp (id_nom, id, cedulanit, fecha, descripcion, valor, ncta, estado, id_des, tipo_des) VALUES ('$id', '$rowh[0]', '$rowh[3]', '$fechaf', 'RETENCIÓN $rowh[4]', '$rowh[6]', '$numcot', 'S', '$iddescu', 'RE')";
										mysqli_query($linkbd,$sqlret);
										$sqlret2 ="UPDATE hum_retencionesfun SET id_nom = '$id' WHERE id = '$rowh[0]'";
										mysqli_query($linkbd,$sqlret2);
									}
								}
								if($listaotrasdeducciones[$x]>0){//********OTROS DESCUENTOS EMPLEADO *****
									$sqlrxde = "SELECT T1.* FROM humretenempleados T1 WHERE T1.empleado = '$listadocumentos[$x]' AND T1.habilitado = 'H' AND T1.estado = 'S' AND ncuotas > (SELECT COUNT(T2.id) FROM humnominaretenemp T2 WHERE T2.cedulanit = '$listaempleados[$x]' AND T2.id = T1.id )"; 
									$respli = mysqli_query($linkbd,$sqlrxde);
									while ($rowh = mysqli_fetch_row($respli)){
										$iddescu = selconsecutivo('humnominaretenemp','id_des');
										$sqlrdes = "SELECT COUNT(*) FROM humnominaretenemp WHERE cedulanit = '$listadocumentos[$x]' AND id = '$rowh[0]'";
										$respdes = mysqli_query($linkbd,$sqlrdes);
										$rowdes = mysqli_fetch_row($respdes);
										$numcot = $rowdes[0]+1;
										$sqlret = "INSERT INTO humnominaretenemp (id_nom, id, cedulanit, fecha, descripcion, valor, ncta, estado, id_des, tipo_des, tipopago, cod_fun, fuente, indicador_producto, bpin, productoservicio, medio_pago, codigo_politicap, tipo_mov, proyecto, seccion_presupuestal, cuenta_presu, idtercero) VALUES ($id, $rowh[0], '$rowh[4]', '$fechaf', '$rowh[1]', '$rowh[8]', '$numcot', 'S', '$iddescu', 'DS', '$rowh[11]', '$rowh[13]', '$listafuentes[$x]', '$listaprogramatico[$x]', '$listabpin[$x]', '', 'CSF', '1', '201', '$listaproyecto[$x]','$listasecpresu[$x]','$listacuentapresu[$x]','$rowh[2]')";
										mysqli_query($linkbd,$sqlret);
									}
								}
								if($listasaludempresa[$x] > 0){//******** SALUD EMPLEADOR *******
									$idsalud = selconsecutivo('humnomina_saludpension','id');
									$sqlrins = "INSERT INTO humnomina_saludpension (id_nom, tipo, empleado, tercero, cc, valor, estado, id, cod_fun, cuenta_presu, tipopago, fuente, indicador_producto, bpin, productoservicio, medio_pago, codigo_politicap, tipo_mov, proyecto, seccion_presupuestal) VALUES ($id, 'SR', '$listadocumentos[$x]', '$listadoceps[$x]', '$listacentrocosto[$x]', '$listasaludempresa[$x]', 'S', '$idsalud', '$listatcodigofuncionario[$x]', '$listacuentapresuSR[$x]', '$listatipopago[$x]', '$listafuentesSR[$x]', '$listaprogramaticoSR[$x]', '$listabpinSR[$x]', '', 'CSF', '1', '201', '$listaproyectoSR[$x]', '$listasecpresu[$x]')";
									mysqli_query($linkbd,$sqlrins);
								}
								if($listapensionempresa[$x] > 0){//******** PENSIONES EMPLEADOR *******
									$idsalud = selconsecutivo('humnomina_saludpension','id');
									$sqlrins = "INSERT INTO humnomina_saludpension (id_nom, tipo, empleado, tercero, cc, valor, estado, sector, id, cod_fun, cuenta_presu, tipopago, fuente, indicador_producto, bpin, productoservicio, medio_pago, codigo_politicap, tipo_mov, proyecto, seccion_presupuestal) VALUES ($id, 'PR', '$listadocumentos[$x]', '$listadocafp[$x]', '$listacentrocosto[$x]', '$listapensionempresa[$x]', 'S', '$listatipopension[$x]', '$idsalud', '$listatcodigofuncionario[$x]', '$listacuentapresuPR[$x]', '$listatipopago[$x]', '$listafuentesPR[$x]', '$listaprogramaticoPR[$x]', '$listabpinPR[$x]', '', 'CSF', '1', '201', '$listaproyectoPR[$x]', '$listasecpresu[$x]')";
									mysqli_query($linkbd,$sqlrins);
								}
								if($listaccf[$x] > 0){//******** COFREM *******
									$idparadiscal = selconsecutivo('humnomina_parafiscales','id');
									$sqlrins = "INSERT INTO humnomina_parafiscales (id_nom, id_parafiscal, porcentaje, valor, cc, estado, cuentapresu, id, codfun, tipopago, fuente, indicador_producto, bpin, productoservicio, medio_pago, codigo_politicap, tipo_mov, proyecto, seccion_presupuestal) VALUES ('$id', '".$_POST['tcajacomp']."', '$listaporcentajeccf[$x]', '$listaccf[$x]', '$listacentrocosto[$x]', 'S', '$listacuentapresuccf[$x]', '$idparadiscal', '$listatcodigofuncionario[$x]', '$listatipopago[$x]', '$listafuentesccf[$x]', '$listaprogramaticoccf[$x]', '$listabpinccf[$x]', '', 'CSF', '1', '201', '$listaproyectoccf[$x]', '$listasecpresu[$x]')";
									mysqli_query($linkbd,$sqlrins);
								}
								if($listasena[$x] > 0){//******** SENA *******
									$idparadiscal = selconsecutivo('humnomina_parafiscales','id');
									$sqlrins = "INSERT INTO humnomina_parafiscales (id_nom, id_parafiscal, porcentaje, valor, cc, estado, cuentapresu, id, codfun, tipopago, fuente, indicador_producto, bpin, productoservicio, medio_pago, codigo_politicap, tipo_mov, proyecto, seccion_presupuestal) VALUES ('$id', '".$_POST['tsena']."', '$listaporcentajesena[$x]', '$listasena[$x]', '$listacentrocosto[$x]', 'S', '$listacuentapresusena[$x]', '$idparadiscal', '$listatcodigofuncionario[$x]', '$listatipopago[$x]', '$listafuentessena[$x]', '$listaprogramaticosena[$x]', '$listabpinsena[$x]', '', 'CSF', '1', '201', '$listaproyectosena[$x]', '$listasecpresu[$x]')";
									mysqli_query($linkbd,$sqlrins);
								}
								if($listaicbf[$x] > 0){//******** ICBF *******
									$idparadiscal = selconsecutivo('humnomina_parafiscales','id');
									$sqlrins = "INSERT INTO humnomina_parafiscales (id_nom, id_parafiscal, porcentaje, valor, cc, estado, cuentapresu, id, codfun, tipopago, fuente, indicador_producto, bpin, productoservicio, medio_pago, codigo_politicap, tipo_mov, proyecto, seccion_presupuestal) VALUES ('$id', '".$_POST['ticbf']."', '$listaporcentajeicbf[$x]','$listaicbf[$x]', '$listacentrocosto[$x]', 'S','$listacuentapresuicbf[$x]', '$idparadiscal', '$listatcodigofuncionario[$x]', '$listatipopago[$x]', '$listafuentesicbf[$x]', '$listaprogramaticoicbf[$x]', '$listabpinicbf[$x]', '', 'CSF', '1', '201', '$listaproyectoicbf[$x]', '$listasecpresu[$x]')";
									mysqli_query($linkbd,$sqlrins);
								}
								if($listainstecnicos[$x] > 0){//******** INSTITUTOS TECNICOS *******
									$idparadiscal = selconsecutivo('humnomina_parafiscales','id');
									$sqlrins = "INSERT INTO humnomina_parafiscales (id_nom, id_parafiscal, porcentaje, valor, cc, estado, cuentapresu, id, codfun, tipopago, fuente, indicador_producto, bpin, productoservicio, medio_pago, codigo_politicap, tipo_mov, proyecto, seccion_presupuestal) VALUES ('$id', '".$_POST['titi']."', '$listaporcentajetiti[$x]', '$listainstecnicos[$x]', '$listacentrocosto[$x]', 'S','$listacuentapresutiti[$x]', '$idparadiscal', '$listatcodigofuncionario[$x]', '$listatipopago[$x]', '$listafuentestiti[$x]', '$listaprogramaticotiti[$x]', '$listabpintiti[$x]', '', 'CSF', '1', '201', '$listaproyectotiti[$x]', '$listasecpresu[$x]')";
									mysqli_query($linkbd,$sqlrins);
								}
								if($listaesap[$x] > 0){//******** ESAP *******
									$idparadiscal = selconsecutivo('humnomina_parafiscales','id');
									$sqlrins = "INSERT INTO humnomina_parafiscales (id_nom, id_parafiscal, porcentaje, valor, cc, estado, cuentapresu, id, codfun, tipopago, fuente, indicador_producto, bpin, productoservicio, medio_pago, codigo_politicap, tipo_mov, proyecto, seccion_presupuestal) VALUES ('$id', '".$_POST['tesap']."', '$listaporcentajeesap[$x]', '$listaesap[$x]', '$listacentrocosto[$x]', 'S', '$listacuentapresuesap[$x]', '$idparadiscal', '$listatcodigofuncionario[$x]', '$listatipopago[$x]', '$listafuentesesap[$x]', '$listaprogramaticoesap[$x]', '$listabpinesap[$x]', '', 'CSF', '1', '201', '$listaproyectoesap[$x]', '$listasecpresu[$x]')";
									mysqli_query($linkbd,$sqlrins);
								}
								if($listaarp[$x] > 0){//******** ARL *******
									$idparadiscal = selconsecutivo('humnomina_parafiscales','id');
									$sqlrins = "INSERT INTO humnomina_parafiscales (id_nom, id_parafiscal, porcentaje, valor, cc, estado, cuentapresu, id, codfun, tipopago, fuente, indicador_producto, bpin, productoservicio, medio_pago, codigo_politicap, tipo_mov, proyecto, seccion_presupuestal) VALUES ('$id', '".$_POST['tarp']."', '$listaporcentajearl[$x]', '$listaarp[$x]', '$listacentrocosto[$x]', 'S', '$listacuentapresuarl[$x]', '$idparadiscal','$listatcodigofuncionario[$x]', '$listatipopago[$x]', '$listafuentesarl[$x]', '$listaprogramaticoarl[$x]', '$listabpinarl[$x]', '', 'CSF', '1', '201', '$listaproyectoarl[$x]', '$listasecpresu[$x]')";
									mysqli_query($linkbd,$sqlrins);
								}
							}
							//CARGAR PRESUPUESTO
							for($rb=0; $rb < count($_POST['rubrosp']); $rb++){
								$incuentap = $_POST['rubrosp'][$rb];
								$valorrubros = $_POST['vrubrosp'][$rb];
								$infuentes = $_POST['fuente'][$rb];
								$numproy = $_POST['nproyecto'][$rb];
								$valbpin = $_POST['valbpinc'][$rb];
								$valindicaro = $_POST['valindicadorc'][$rb];
								$valtipogasto = substr($_POST['rubrosp'][$rb],0,3);
								$valsecpresu = $_POST['secpresu'][$rb];
								$sqlrp = "INSERT INTO humnom_presupuestal (id_nom, cuenta, valor, estado, fuente, proyecto, medio_pago, producto, indicador, bpin, tipo_gasto, seccion_presupuestal, vigencia_gasto, divipola, chip) VALUES ($id,'$incuentap', $valorrubros, 'S', '$infuentes', '$numproy', 'CSF', '', '$valindicaro', '$valbpin', '$valtipogasto', '$valsecpresu','1','','')";
								mysqli_query($linkbd,$sqlrp);
							}
							echo "<script>funcionmensaje();</script>";
						}
					}//fin guardar
					echo "<script>document.getElementById('idpreli']).value = '-1'</script>";
				}
				elseif( $row[0] != 0){
					echo "
					<script>
						Swal.fire(
							'Error!',
							Ya se almaceno la preliquidación Nº: ".$_POST['idpreli']."',
							'error'
						);
					</script>";
				}
			?>
		</form>
	</body>
</html>
