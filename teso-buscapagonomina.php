<?php
	header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=utf8");
	require "comun.inc";
	require "funciones.inc";
	session_start();
	$linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");
	cargarcodigopag($_GET['codpag'],$_SESSION["nivel"]);
	date_default_timezone_set("America/Bogota");
?>
<!DOCTYPE >
<html lang="es">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1">
		<meta name="viewport" content="user-scalable=no">
		<title>:: IDEAL 10 - Tesorer&iacute;a</title>
		<link href="favicon.ico" rel="shortcut icon"/>
		<link href="css/css2.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css">
		<link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css">
		<link href="css/css4.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css">
		<script type="text/javascript" src="css/calendario.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script type="text/javascript" src="css/programas.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script type="text/javascript" src="JQuery/jquery-2.1.4.min.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script>
			function verUltimaPos(idcta, filas, filtro){
				var scrtop=$('#divdet').scrollTop();
				var altura=$('#divdet').height();
				var numpag=$('#nummul').val();
				var limreg=$('#numres').val();
				if((numpag<=0)||(numpag=="")){numpag=0;}
				if((limreg==0)||(limreg=="")){limreg=10;}
				numpag++;
				location.href="teso-pagonominaver.php?idegre="+idcta+"&scrtop="+scrtop+"&totreg="+filas+"&altura="+altura+"&numpag="+numpag+"&limreg="+limreg+"&filtro="+filtro;
			}
			function eliminar(idr){
				if (confirm("Esta Seguro de Anular El Egreso No "+idr))
				{
					document.form2.oculto.value=2;
					document.form2.var1.value=idr;
					document.form2.submit();
				}
			}
			function pdf(){
				document.form2.action="teso-pdfconsignaciones.php";
				document.form2.target="_BLANK";
				document.form2.submit(); 
				document.form2.action="";
				document.form2.target="";
			}
			function buscarbotonfiltro(){
				if((document.form2.fechaini.value != "" && document.form2.fechafin.value == "") || (document.form2.fechaini.value == "" && document.form2.fechafin.value != "")){
					alert("Falta digitar fecha");
				}else{
					document.getElementById('numpos').value=0;
					document.getElementById('nummul').value=0;
					document.form2.submit();
				}
			}
			function crearexcel(){
				document.form2.action="teso-pagonominaexcel.php";
				document.form2.target="_BLANK";
				document.form2.submit();
				document.form2.action="";
				document.form2.target="";
			}
		</script>
		<?php 
			if(isset($_GET['fini']) && isset($_GET['ffin'])){
				if(!empty($_GET['fini']) && !empty($_GET['ffin'])){
					$_POST['fecha']=$_GET['fini'];
					$_POST['fecha2']=$_GET['ffin'];
				}
			}
			$fech1=preg_split("/",$_POST['fechaini']);
			$fech2=preg_split("/",$_POST['fechafin']);
			$f1=$fech1[2]."-".$fech1[1]."-".$fech1[0];
			$f2=$fech2[2]."-".$fech2[1]."-".$fech2[0];
			$scrtop=$_GET['scrtop'];
			if($scrtop=="") $scrtop=0;
			echo"<script>
				window.onload=function(){
					$('#divdet').scrollTop(".$scrtop.")
				}
			</script>";
			$gidcta=$_GET['idcta'];
			if(isset($_GET['filtro']))
				$_POST['nombre']=$_GET['filtro'];
		?>
	</head>
	<body>
		<IFRAME src="alertas.php" name="alertas" id="alertas" style="display:none"></IFRAME>
		<span id="todastablas2"></span>
		<table>
			<tr><script>barra_imagenes("teso");</script><?php cuadro_titulos();?></tr>	 
			<tr><?php menu_desplegable("teso");?></tr>
			<tr>
				<td colspan="3" class="cinta">
					<a onClick="location.href='teso-pagonomina.php'" class="mgbt"><img src="imagenes/add.png" title="Nuevo"/></a>
					<a class="mgbt"><img src="imagenes/guardad.png"/></a>
					<a class="mgbt"><img src="imagenes/buscad.png"/></a>
					<a onClick="mypop=window.open('plan-agenda.php','','');mypop.focus()" class="mgbt"><img src="imagenes/agenda1.png" title="Agenda"/></a>
					<a onClick="mypop=window.open('teso-principal.php','','');mypop.focus();" class="mgbt"><img src="imagenes/nv.png" title="Nueva ventana"/></a>
					<a onClick="mypop=window.open('<?php echo $url2; ?>','','');mypop.focus();" class="mgbt"><img src="imagenes/duplicar_pantalla.png" title="Duplicar pesta&ntilde;a"></a>
					<a onclick="crearexcel()" class="mgbt"><img src="imagenes/excel.png" title="Excel"></a>
				</td>
			</tr>	
		</table>
		<form name="form2" method="post" action="teso-buscapagonomina.php">
			<?php
				if($_GET['numpag']!=""){
					$oculto=$_POST['oculto'];
					if($oculto!=2){
						$_POST['numres']=$_GET['limreg'];
						$_POST['numpos']=$_GET['limreg']*($_GET['numpag']-1);
						$_POST['nummul']=$_GET['numpag']-1;
					}
				}else{
					if($_POST['nummul']==""){
						$_POST['numres']=10;
						$_POST['numpos']=0;
						$_POST['nummul']=0;
					}
				}
			?>
			<input type="hidden" name="numres" id="numres" value="<?php echo $_POST['numres'];?>"/>
			<input type="hidden" name="numpos" id="numpos" value="<?php echo $_POST['numpos'];?>"/>
			<input type="hidden" name="nummul" id="nummul" value="<?php echo $_POST['nummul'];?>"/>
			<input type="hidden" name="fecham1"  id="fecham1" value="<?php echo $_POST['fecham1']; ?>"/>
			<input type="hidden" name="fecham2" id="fecham2" value="<?php echo $_POST['fecham2']; ?>"/>
			<table  class="inicio" align="center" >
				<tr >
					<td class="titulos" colspan="16" >:. Buscar Pagos Nomina</td>
					<td class="cerrar" style="width:7%" onClick="location.href='teso-principal.php'">Cerrar</td>
				</tr>
				<tr>
					<td class="saludo1" style="width:4cm;">N&uacute;mero de egreso: </td>
					<td style="width:15%;"><input type="search" name="numero" id="numero"  value="<?php echo $_POST['numero'];?>" style="width:98%;"/></td> 
					<td class="saludo1" style="width:4cm;">Concepto de egreso: </td>
					<td style="width:15%;"><input type="search" name="nombre" id="nombre"  value="<?php echo $_POST['nombre'];?>" style="width:98%;"/></td> 
					<td class="tamano01" style="width:2cm;">Fecha inicial: </td>
					<td><input type="search" name="fechaini" id="fc_1198971545" title="DD/MM/YYYY" placeholder="DD/MM/YYYY" value="<?php echo @$_POST['fechaini'];?>" onKeyUp="return tabular(event,this)" onKeyDown="mascara(this,'/',patron,true)" maxlength="10" style="width:75%;height:30px;">&nbsp;<img src="imagenes/calendario04.png" onClick="displayCalendarFor('fc_1198971545');" class="icobut" title="Calendario"></td>
					<td  class="tamano01" style="width:2cm;">Fecha final: </td>
					<td><input type="search" name="fechafin"  id="fc_1198971546" title="DD/MM/YYYY" placeholder="DD/MM/YYYY" value="<?php echo @$_POST['fechafin'];?>" onKeyUp="return tabular(event,this) " onKeyDown="mascara(this,'/',patron,true)" maxlength="10" style="width:75%;height:30px;"/>&nbsp;<img src="imagenes/calendario04.png" onClick="displayCalendarFor('fc_1198971546');"  class="icobut" title="Calendario"></td>
					<td><input type="button" name="bboton" onClick="buscarbotonfiltro();" value="&nbsp;&nbsp;Buscar&nbsp;&nbsp;" /></td>
				</tr>
			</table>
			<input type="hidden" name="oculto" id="oculto"  value="1">
			<input type="hidden" name="var1"  value=<?php echo $_POST['var1'];?>>
			<div class="subpantallap" style="height:68.5%; width:99.6%; overflow-x:hidden;" id="divdet">
			<?php
				$oculto=$_POST['oculto'];
				if($_POST['oculto']==2){
					$sqlr="select * from tesoegresos where id_egreso=$_POST[var1]";
					$resp = mysqli_query($linkbd,$sqlr);
					$row=mysqli_fetch_row($resp);
					$op=$row[2];
					$vpa=$row[7];
					//********Comprobante contable en 000000000000
					$sqlr="update comprobante_cab set total_debito=0,total_credito=0,estado='0' where tipo_comp='17' and numerotipo='$row[0]'";
					mysqli_query($linkbd,$sqlr);
					$sqlr="update comprobante_det set valdebito=0,valcredito=0 where id_comp='17 $row[0]'";
					mysqli_query($linkbd,$sqlr);
					$sqlr="update pptocomprobante_cab set estado='0' where tipo_comp='10' and numerotipo='$row[0]'";
					mysqli_query($linkbd,$sqlr);	 
					$sqlr="update tesoegresosnomina set estado='N' where id_egreso='$_POST[var1]'";
					mysqli_query($linkbd,$sqlr);	 
					$sqlr="select * from tesoegresosnomina_det where id_egreso='$_POST[var1]'";
					$res=mysqli_query($linkbd,$sqlr);	 
					while($row=mysqli_fetch_row($res)){
						if($row[3]=='N'){
							$sqlru="update humnomina_det set estado='S' where id_nom=$_POST[var1] and cedulanit=$row[4]";
							mysqli_query($linkbd,$sqlru);
						}else if($row[3]=='SE' || $row[3]=='PR' || $row[3]=='PE' || $row[3]=='SR'){
							$sqlru="update humnomina_saludpension set estado='S' where id_nom=$_POST[var1] and tercero=$row[4]";
							mysqli_query($linkbd,$sqlru);
						}else if($row[3]=='F'){
							$sqlru="update humnomina_parafiscales set estado='S' where id_nom=$_POST[var1] and valor=$row[8]";
							mysqli_query($linkbd,$sqlru);
						}
					}
				}
				$crit1="";
				$crit2="";
				if ($_POST['numero']!=""){
					$crit1="AND tesoegresosnomina.id_egreso LIKE '$_POST[numero]'";
				}
				if ($_POST['nombre']!=""){
					$crit2="AND tesoegresosnomina.concepto LIKE '$_POST[nombre]'";
				}
				if($_POST['fechaini']!=''){
					preg_match("/([0-9]{2})\/([0-9]{2})\/([0-9]{4})/",$_POST['fechaini'],$fecha);
					$fechai="$fecha[3]-$fecha[2]-$fecha[1]";
					if($_POST['fechafin']!=''){
						preg_match("/([0-9]{2})\/([0-9]{2})\/([0-9]{4})/",$_POST['fechafin'],$fecha);
						$fechaf="$fecha[3]-$fecha[2]-$fecha[1]";
						$crit3 = "AND fecha BETWEEN '$fechai' AND '$fechaf'";
					}else{
						$fechaf=date("Y-m-d");
						$crit3 = "AND fecha BETWEEN '$fechai' AND '$fechaf'";
					}
				}
				$sqlr="SELECT * FROM tesoegresosnomina WHERE tesoegresosnomina.id_egreso>-1 $crit1 $crit2 $crit3 ORDER BY tesoegresosnomina.id_egreso DESC";
				$resp = mysqli_query($linkbd,$sqlr);
				$ntr1 = mysqli_num_rows($resp);
				$_POST['numtop']=$ntr1;
				$nuncilumnas=ceil($_POST['numtop']/$_POST['numres']);
				$cond2="";
				if ($_POST['numres']!="-1"){ $cond2="LIMIT $_POST[numpos], $_POST[numres]";}
				$sqlr="SELECT * FROM tesoegresosnomina WHERE tesoegresosnomina.id_egreso>-1 $crit1 $crit2 $crit3 ORDER BY tesoegresosnomina.id_egreso DESC $cond2";
				$resp = mysqli_query($linkbd,$sqlr);
				
				$con=1;

				$numcontrol=$_POST['nummul']+1;
				if(($nuncilumnas==$numcontrol)||($_POST['numres']=="-1"))
				{
					$imagenforward="<img src='imagenes/forward02.png' class='icomen2n'/>";
					$imagensforward="<img src='imagenes/skip_forward02.png' class='icomen1n'/>";
				}
				else
				{
					$imagenforward="<img src='imagenes/forward01.png' class='icomen2' title='Siguiente' onClick='numsiguiente()'>";
					$imagensforward="<img src='imagenes/skip_forward01.png' class='icomen1' title='Fin' onClick='saltocol(\"$nuncilumnas\")'/>";
				}
				if(($_POST['numpos']==0)||($_POST['numres']=="-1"))
				{
					$imagenback="<img src='imagenes/back02.png' class='icomen2n'>";
					$imagensback="<img src='imagenes/skip_back02.png' class='icomen1n'/>";
				}
				else
				{
					$imagenback="<img src='imagenes/back01.png' class='icomen2' title='Anterior' onClick='numanterior();'>";
					$imagensback="<img src='imagenes/skip_back01.png' class='icomen1' title='Inicio' onClick='saltocol(\"1\")'/>";
				}
				$con=1;
				echo "
				<table class='inicio' align='center' >
					<tr>
						<td colspan='10' class='titulos'>.: Resultados Busqueda:</td>
						<td class='submenu'>
							<select name='renumres' id='renumres' onChange='cambionum();' style='width:100%'>
								<option value='10'"; if ($_POST['renumres']=='10'){echo 'selected';} echo ">10</option>
								<option value='20'"; if ($_POST['renumres']=='20'){echo 'selected';} echo ">20</option>
								<option value='30'"; if ($_POST['renumres']=='30'){echo 'selected';} echo ">30</option>
								<option value='50'"; if ($_POST['renumres']=='50'){echo 'selected';} echo ">50</option>
								<option value='100'"; if ($_POST['renumres']=='100'){echo 'selected';} echo ">100</option>
								<option value='-1'"; if ($_POST['renumres']=='-1'){echo 'selected';} echo ">Todos</option>
							</select>
						</td>
					</tr>
					<tr>
						<td colspan='11' id='RecEnc'>Pagos encontrados: $ntr2</td>
					</tr>
					<tr>
						<td  class='titulos2'>Egreso</td>
						<td  class='titulos2'>Nomina</td>
						<td class='titulos2'>Nombre</td>
						<td class='titulos2'>Fecha</td>
						<td class='titulos2'>Banco</td>
						<td class='titulos2'>Cuenta</td>
						<td class='titulos2'>Valor</td>
						<td class='titulos2'>Concepto</td>
						<td class='titulos2' width='5%'><center>Estado</td>
						<td class='titulos2' width='5%'><center>Anular</td>
						<td class='titulos2' width='5%'><center>Ver</td>
					</tr>";	
				$iter='zebra1';
				$iter2='zebra2';
				$filas=1;

				if($_POST['fechaini'] == '' && $_POST['fechafin'] == '' && $_POST['numero'] == '' && $_POST['nombre'] == '')
				{
					echo "
					<table class='inicio'>
						<tr>
							<td class='saludo1' style='text-align:center;width:100%;font-size:25px'>Utilice el filtro de busqueda</td>
						</tr>
					</table>";
					$nuncilumnas = 0;
				}
				elseif(mysqli_num_rows($resp) == 0 || mysqli_num_rows($resp) == '0')
				{
					echo "
					<table class='inicio'>
						<tr>
							<td class='saludo1' style='text-align:center;width:100%;font-size:25px'>No hay resultados de su busqueda.</td>
						</tr>
					</table>";
				}
				else
				{
					while ($row =mysqli_fetch_row($resp)) 
					{
						$ntr2 = $ntr1;

						echo "<script>document.getElementById('RecEnc').innerHTML = 'Pagos encontrados: $ntr2'</script>";

						$ntr=buscatercero($row[11]);
						$banco=buscatercero(buscabanco($row[9]));
						if($gidcta!="")
						{
							if($gidcta==$row[0]){$estilo='background-color:#FF9';}
							else{$estilo="";}
						}
						else{$estilo="";}	
						$idcta="'$row[0]'";
						$numfil="'$filas'";
						$filtro="'$_POST[nombre]'";
						if($row[13]=='S')
						{
							$imgsem="src='imagenes/sema_verdeON.jpg' title='Activo'";$coloracti="#0F0";$_POST['lswitch1'][$row[0]]=0;
							$iconanu="<img src='imagenes/anular.png' onClick=\"eliminar($row[0])\" class='icoop' title='Anular'/>";
						}
						else {$imgsem="src='imagenes/sema_rojoON.jpg' title='Inactivo'";$coloracti="#C00";$_POST['lswitch1'][$row[0]]=1;}

						echo"

							<input type='hidden' name='egresoE[]' value='".$row[0]."'>
							<input type='hidden' name='nominaE[]' value='".$row[2]."'>
							<input type='hidden' name='numeroE[]' value='".$row[11]."'>
							<input type='hidden' name='nombreE[]' value='".$ntr."'>
							<input type='hidden' name='fechaE[]' value='".$row[3]."'>
							<input type='hidden' name='bancoE[]' value='".$banco."'>
							<input type='hidden' name='cuentaE[]' value='".$row[12]."'>
							<input type='hidden' name='valorE[]' value='".number_format($row[7],2)."'>
							<input type='hidden' name='conceptoE[]' value='".strtoupper($row[8])."'>
							";

							if ($row[13]=='S')
							{
								echo"
								<input type='hidden' name='estadoE[]' value='ACTIVO'>";
							}
							if ($row[13]=='N')
							{
								echo"
								<input type='hidden' name='estadoE[]' value='INACTIVO'>";
							}
							if ($row[13]=='R')
							{
								echo"
								<input type='hidden' name='estadoE[]' value='INACTIVO'>";
							}

						echo"
						<tr class='$iter' onDblClick=\"verUltimaPos($idcta, $numfil, $filtro)\" style='text-transform:uppercase; $estilo' >
							<td>$row[0]</td>
							<td>$row[2]</td>
							<td>$row[11] - $ntr</td>
							<td>$row[3]</td>
							<td>$banco</td>
							<td>$row[12]</td>
							<td>".number_format($row[7],2)."</td>
							<td>".strtoupper($row[8])."</td>
							<td style='text-align:center;'><img $imgsem style='width:20px'/></td>
							<td style='text-align:center;'>$iconanu</td>
							<td style='text-align:center;'><img src='imagenes/lupa02.png' class='icoop' title='Ver' onClick=\"verUltimaPos($idcta, $numfil, $filtro)\"/></td>
						</tr>";
						$con+=1;
						$filas++;
						$aux=$iter;
						$iter=$iter2;
						$iter2=$aux;
					}	
				}
				echo"
				</table>
				<table class='inicio'>
					<tr style='height:20px'>
						<td style='text-align:center;'>
							$imagensback&nbsp;$imagenback&nbsp;&nbsp;";
				if($nuncilumnas<=9){$numfin=$nuncilumnas;}
				else{$numfin=9;}
				for($xx = 1; $xx <= $numfin; $xx++)
				{
					if($numcontrol<=9){$numx=$xx;}
					else{$numx=$xx+($numcontrol-9);}
					if($numcontrol==$numx){echo"<label onClick='saltocol(\"$numx\")'; style='color:#24D915' class='icomen3'> $numx </label>";}
					else {echo"<label onClick='saltocol(\"$numx\")'; style='color:#000000' class='icomen3'> $numx </label>";}
				}
					echo "		&nbsp;&nbsp;$imagenforward&nbsp;$imagensforward
							</td>
						</tr>
					</table>";

			?>
            </div>
		</form> 
	</body>
</html>