
<!DOCTYPE html>
<html class="bg-blue-1000" lang="en" data-theme="ideal">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="./assets/plugins/daisyUI/daisyui.css">
    <script src="./assets/plugins/tailwindcss/tailwind.js"></script>
    <link rel="stylesheet" href="./assets/css/style.css">
    <script>
        tailwind.config = {
        theme: {
          extend: {
            colors: {
              "cyan":{
                1000:"#26ABD7",
                1050:"#1092bd"
              },
              "blue":{
                1000:"#99bbcc"
              }
            }
          }
        }
      }
    </script>
</head>
<body>
    <div class="container mx-auto">
        <!--BOTONES Y ENCABEZADO-->
        <header>
            <div class="relative shadow-lg bg-base-100 ">
                <div class="flex pt-2 mb-2 justify-center text-center hidden lg:block">
                  <div>
                      <p class="text-cyan-1050 font-bold">Administración Pública Cooperativa de Acueducto, Alcantarillado y Aseo de Rosalía</p>
                      <p class="text-blue-1000 font-bold">Trabajamos por un mejor servicio</p>
                  </div>
                </div>
                <div class="bg-base-100 fixed top-0 z-30 w-full md:relative">
                  <div class="flex justify-center sm:hidden">
                    <img class="object-fill h-14" src="assets/img/logoserviciosprv2.png" alt="">
                  </div>
                  <nav class=" navbar flex justify-between xl:flex-row xl:justify-between lg:flex-col lg:space-y-3 xl:space-y-0">
                    <div class="hidden sm:inline-flex	">
                      <img class="object-fill h-14" src="assets/img/logoserviciosprv2.png" alt="">
                      <img class="object-fill h-14" src="assets/img/escudo.jpg" alt="">
                    </div>
                    <div class="inline-flex sm:hidden">
                      <img class="object-fill h-14" src="assets/img/escudo.jpg" alt="">
                    </div>
                    <div>
                      <ul class="px-1 flex justify-center hidden lg:block space-x-3">
                        <li class="shadow-md btn bg-cyan-1000 border-transparent text-white hover:bg-cyan-1050"><a>Inicio</a></li>
                        <li class="dropdown dropdown-hover ">
                            <ul tabindex="0" role="button" class="shadow-md btn bg-cyan-1000 border-transparent text-white hover:bg-cyan-1050"><li><a>Archivos maestros</a></li></ul>
                            <ul tabindex="0" class="dropdown-content z-[1] menu p-2 shadow bg-cyan-1000 text-white rounded-box w-52">
                                <li class="hover:bg-cyan-1050 hover:rounded-lg"><a>Parámetros SP</a></li>
                                <li class="hover:bg-cyan-1050 hover:rounded-lg"><a>Servicios</a></li>
                                <li class="hover:bg-cyan-1050 hover:rounded-lg"><a>Suscriptores</a></li>
                                <li class="hover:bg-cyan-1050 hover:rounded-lg"><a>Barrios</a></li>
                                <li class="hover:bg-cyan-1050 hover:rounded-lg"><a>Estratos</a></li>
                                <li class="hover:bg-cyan-1050 hover:rounded-lg"><a>Uso de suelo</a></li>
                                <li class="hover:bg-cyan-1050 hover:rounded-lg"><a>Cargo fijo</a></li>
                                <li class="hover:bg-cyan-1050 hover:rounded-lg"><a>Tarifas de consumo</a></li>
                                <li class="hover:bg-cyan-1050 hover:rounded-lg"><a>Tipo de movimiento</a></li>
                                <li class="hover:bg-cyan-1050 hover:rounded-lg"><a>Exonercación</a></li>
                                <li class="hover:bg-cyan-1050 hover:rounded-lg"><a>Novedades</a></li>
                            </ul>
                        </li>
                        <li class="dropdown dropdown-hover">
                            <ul tabindex="0" role="button" class="shadow-md btn bg-cyan-1000 border-transparent text-white hover:bg-cyan-1050"><li><a>Procesos</a></li></ul>
                            <ul tabindex="0" class="dropdown-content z-[1] menu p-2 shadow bg-cyan-1000 text-white rounded-box w-52">
                                <li class="hover:bg-cyan-1050 hover:rounded-lg"><a>Usuarios</a></li>
                                <li class="hover:bg-cyan-1050 hover:rounded-lg"><a>Lecturas AC y ALC</a></li>
                                <li class="hover:bg-cyan-1050 hover:rounded-lg"><a>Novedades</a></li>
                                <li class="hover:bg-cyan-1050 hover:rounded-lg"><a>Menu facturación</a></li>
                                <li class="hover:bg-cyan-1050 hover:rounded-lg"><a>Acuerdos de pago</a></li>
                                <li class="hover:bg-cyan-1050 hover:rounded-lg"><a>Recaudo completo</a></li>
                            </ul>
                        </li>
                        <li class="shadow-md btn bg-cyan-1000 border-transparent text-white hover:bg-cyan-1050"><a>Herramientas</a></li>
                        <li class="dropdown dropdown-hover">
                            <ul tabindex="0" role="button" class="shadow-md btn bg-cyan-1000 border-transparent text-white hover:bg-cyan-1050"><li><a>Informes</a></li></ul>
                            <ul tabindex="0" class="dropdown-content z-[1] menu p-2 shadow bg-cyan-1000 text-white rounded-box w-52">
                                <li class="hover:bg-cyan-1050 hover:rounded-lg"><a>Usuarios</a></li>
                                <li class="hover:bg-cyan-1050 hover:rounded-lg"><a>Lecturas AC y ALC</a></li>
                                <li class="hover:bg-cyan-1050 hover:rounded-lg"><a>Novedades</a></li>
                                <li class="hover:bg-cyan-1050 hover:rounded-lg"><a>Menu facturación</a></li>
                                <li class="hover:bg-cyan-1050 hover:rounded-lg"><a>Acuerdos de pago</a></li>
                                <li class="hover:bg-cyan-1050 hover:rounded-lg"><a>Recaudo completo</a></li>
                            </ul>
                        </li>
                        <li class="shadow-md btn bg-cyan-1000 border-transparent text-white hover:bg-cyan-1050"><a>Ayuda</a></li>
                      </ul>
                      <div class="dropdown dropdown-end">
                          <div tabindex="0" role="button" class="btn btn-ghost btn-circle avatar online">
                            <div class="w-10 rounded-full">
                              <img alt="Tailwind CSS Navbar component" src="https://daisyui.com/images/stock/photo-1534528741775-53994a69daeb.jpg" />
                            </div>
                          </div>
                          <ul tabindex="0" class="mt-1 z-[1] p-2 shadow menu menu-sm dropdown-content bg-base-100 rounded-box w-52">
                            <li class="">
                              <a class="justify-between">
                                Usuario
                                <span class="badge">ideal10</span>
                              </a>
                            </li>
                            <li class="">
                              <a class="justify-between">
                                Perfil
                                <span class="badge">Superman</span>
                              </a>
                            </li>
                            <li class="">
                              <a class="justify-between">
                                Fecha de ingreso
                                <span class="badge">16/02/2024</span>
                              </a>
                            </li>
                            <li class="">
                              <a class="justify-between">
                                Hora de ingreso
                                <span class="badge">05:54:09</span>
                              </a>
                            </li>
                          </ul>
                      </div>
                      <div >
                        <input id="my-drawer" type="checkbox" class="drawer-toggle" />
                        <div class="drawer-content">
                          <!-- Page content here -->
                          <label for="my-drawer" class="btn btn-circle bg-cyan-1000 hover:bg-cyan-1050 lg:hidden ms-4">
                            <svg class="swap-off fill-white" xmlns="http://www.w3.org/2000/svg" width="32" height="32" viewBox="0 0 512 512"><path d="M64,384H448V341.33H64Zm0-106.67H448V234.67H64ZM64,128v42.67H448V128Z"/></svg>
                          </label>
                        </div> 
                        <div class="drawer-side z-40">
                          <label for="my-drawer" aria-label="close sidebar" class="drawer-overlay"></label>
                          <ul class="menu p-4 w-80 min-h-full bg-base-200 text-base-content">
                            <li class="mb-4 shadow-md btn bg-cyan-1000 border-transparent text-white hover:bg-cyan-1050"><a>Inicio</a></li>
                            <li class="mb-4 dropdown dropdown-hover ">
                                <ul tabindex="0" role="button" class="m-0 w-full shadow-md btn bg-cyan-1000 border-transparent text-white hover:bg-cyan-1050"><li><a>Archivos maestros</a></li></ul>
                                <ul tabindex="0" class="dropdown-content z-[1] menu p-2 shadow bg-cyan-1000 text-white rounded-box w-52">
                                    <li class="hover:bg-cyan-1050 hover:rounded-lg"><a>Parámetros SP</a></li>
                                    <li class="hover:bg-cyan-1050 hover:rounded-lg"><a>Servicios</a></li>
                                    <li class="hover:bg-cyan-1050 hover:rounded-lg"><a>Suscriptores</a></li>
                                    <li class="hover:bg-cyan-1050 hover:rounded-lg"><a>Barrios</a></li>
                                    <li class="hover:bg-cyan-1050 hover:rounded-lg"><a>Estratos</a></li>
                                    <li class="hover:bg-cyan-1050 hover:rounded-lg"><a>Uso de suelo</a></li>
                                    <li class="hover:bg-cyan-1050 hover:rounded-lg"><a>Cargo fijo</a></li>
                                    <li class="hover:bg-cyan-1050 hover:rounded-lg"><a>Tarifas de consumo</a></li>
                                    <li class="hover:bg-cyan-1050 hover:rounded-lg"><a>Tipo de movimiento</a></li>
                                    <li class="hover:bg-cyan-1050 hover:rounded-lg"><a>Exonercación</a></li>
                                    <li class="hover:bg-cyan-1050 hover:rounded-lg"><a>Novedades</a></li>
                                </ul>
                            </li>
                            <li class="mb-4 dropdown dropdown-hover">
                                <ul tabindex="0" role="button" class="m-0 w-full shadow-md btn bg-cyan-1000 border-transparent text-white hover:bg-cyan-1050"><li><a>Procesos</a></li></ul>
                                <ul tabindex="0" class="dropdown-content z-[1] menu p-2 shadow bg-cyan-1000 text-white rounded-box w-52">
                                    <li class="hover:bg-cyan-1050 hover:rounded-lg"><a>Usuarios</a></li>
                                    <li class="hover:bg-cyan-1050 hover:rounded-lg"><a>Lecturas AC y ALC</a></li>
                                    <li class="hover:bg-cyan-1050 hover:rounded-lg"><a>Novedades</a></li>
                                    <li class="hover:bg-cyan-1050 hover:rounded-lg"><a>Menu facturación</a></li>
                                    <li class="hover:bg-cyan-1050 hover:rounded-lg"><a>Acuerdos de pago</a></li>
                                    <li class="hover:bg-cyan-1050 hover:rounded-lg"><a>Recaudo completo</a></li>
                                </ul>
                            </li>
                            <li class="mb-4 shadow-md btn bg-cyan-1000 border-transparent text-white hover:bg-cyan-1050"><a>Herramientas</a></li>
                            <li class="mb-4 dropdown dropdown-hover">
                                <ul tabindex="0" role="button" class="m-0 w-full shadow-md btn bg-cyan-1000 border-transparent text-white hover:bg-cyan-1050"><li><a>Informes</a></li></ul>
                                <ul tabindex="0" class="dropdown-content z-[1] menu p-2 shadow bg-cyan-1000 text-white rounded-box w-52">
                                    <li class="hover:bg-cyan-1050 hover:rounded-lg"><a>Usuarios</a></li>
                                    <li class="hover:bg-cyan-1050 hover:rounded-lg"><a>Lecturas AC y ALC</a></li>
                                    <li class="hover:bg-cyan-1050 hover:rounded-lg"><a>Novedades</a></li>
                                    <li class="hover:bg-cyan-1050 hover:rounded-lg"><a>Menu facturación</a></li>
                                    <li class="hover:bg-cyan-1050 hover:rounded-lg"><a>Acuerdos de pago</a></li>
                                    <li class="hover:bg-cyan-1050 hover:rounded-lg"><a>Recaudo completo</a></li>
                                </ul>
                            </li>
                            <li class="mb-4 shadow-md btn bg-cyan-1000 border-transparent text-white hover:bg-cyan-1050"><a>Ayuda</a></li>
                          </ul>
                        </div>
                      </div>
                    </div>
                  </nav>
                </div>
              </div>
            <div class="navbar block lg:flex shadow-lg justify-start space-x-3 bg-base-100 mt-36 md:mt-[2rem] lg:mt-3 rounded-box">
                <div class="px-1 grid grid-cols-2 xl:grid-cols-7 lg:grid-cols-6 md:grid-cols-4 sm:grid-cols-3 gap-2"> 
                    <button type="button" class="group shadow-md btn bg-white border-transparent text-black hover:bg-cyan-1050">
                        <span class="group-hover:text-white">Nuevo</span>
                        <svg class="fill-black group-hover:fill-white w-5 h-5" xmlns="http://www.w3.org/2000/svg"  viewBox="0 -960 960 960" ><path d="M440-440H200v-80h240v-240h80v240h240v80H520v240h-80v-240Z"/></svg>
                    </button>
                    <button type="button" class="group shadow-md btn bg-white border-transparent text-black hover:bg-cyan-1050">
                        <span class="group-hover:text-white">Guardar</span>
                        <svg class="fill-black group-hover:fill-white w-5 h-5" xmlns="http://www.w3.org/2000/svg"  viewBox="0 -960 960 960" ><path d="M840-680v480q0 33-23.5 56.5T760-120H200q-33 0-56.5-23.5T120-200v-560q0-33 23.5-56.5T200-840h480l160 160Zm-80 34L646-760H200v560h560v-446ZM480-240q50 0 85-35t35-85q0-50-35-85t-85-35q-50 0-85 35t-35 85q0 50 35 85t85 35ZM240-560h360v-160H240v160Zm-40-86v446-560 114Z"/></svg>
                    </button>
                    <button type="button" class="group shadow-md btn bg-white border-transparent text-black hover:bg-cyan-1050">
                        <span class="group-hover:text-white">Buscar</span>
                        <svg class="fill-black group-hover:fill-white w-5 h-5" xmlns="http://www.w3.org/2000/svg"  viewBox="0 -960 960 960" ><path d="M784-120 532-372q-30 24-69 38t-83 14q-109 0-184.5-75.5T120-580q0-109 75.5-184.5T380-840q109 0 184.5 75.5T640-580q0 44-14 83t-38 69l252 252-56 56ZM380-400q75 0 127.5-52.5T560-580q0-75-52.5-127.5T380-760q-75 0-127.5 52.5T200-580q0 75 52.5 127.5T380-400Z"/></svg>
                    </button>
                    <button type="button" class="group shadow-md btn bg-white border-transparent text-black hover:bg-cyan-1050">
                        <span class="group-hover:text-white">Agenda</span>
                        <svg class="fill-black group-hover:fill-white w-5 h-5" xmlns="http://www.w3.org/2000/svg"  viewBox="0 -960 960 960" ><path d="M560-564v-68q33-14 67.5-21t72.5-7q26 0 51 4t49 10v64q-24-9-48.5-13.5T700-600q-38 0-73 9.5T560-564Zm0 220v-68q33-14 67.5-21t72.5-7q26 0 51 4t49 10v64q-24-9-48.5-13.5T700-380q-38 0-73 9t-67 27Zm0-110v-68q33-14 67.5-21t72.5-7q26 0 51 4t49 10v64q-24-9-48.5-13.5T700-490q-38 0-73 9.5T560-454ZM260-320q47 0 91.5 10.5T440-278v-394q-41-24-87-36t-93-12q-36 0-71.5 7T120-692v396q35-12 69.5-18t70.5-6Zm260 42q44-21 88.5-31.5T700-320q36 0 70.5 6t69.5 18v-396q-33-14-68.5-21t-71.5-7q-47 0-93 12t-87 36v394Zm-40 118q-48-38-104-59t-116-21q-42 0-82.5 11T100-198q-21 11-40.5-1T40-234v-482q0-11 5.5-21T62-752q46-24 96-36t102-12q58 0 113.5 15T480-740q51-30 106.5-45T700-800q52 0 102 12t96 36q11 5 16.5 15t5.5 21v482q0 23-19.5 35t-40.5 1q-37-20-77.5-31T700-240q-60 0-116 21t-104 59ZM280-494Z"/></svg>
                    </button>
                    <button type="button" class="group shadow-md btn bg-white border-transparent text-black hover:bg-cyan-1050">
                        <span class="group-hover:text-white">Nueva ventana</span>
                        <svg class="fill-black group-hover:fill-white w-5 h-5" xmlns="http://www.w3.org/2000/svg"  viewBox="0 -960 960 960" ><path d="M320-120v-80h80v-80H160q-33 0-56.5-23.5T80-360v-400q0-33 23.5-56.5T160-840h640q33 0 56.5 23.5T880-760v400q0 33-23.5 56.5T800-280H560v80h80v80H320ZM160-360h640v-400H160v400Zm0 0v-400 400Z"/></svg>
                    </button>
                    <button type="button" class="group shadow-md btn bg-white border-transparent text-black hover:bg-cyan-1050">
                        <span class="group-hover:text-white">Exportar PDF</span>
                        <svg class="fill-black group-hover:fill-white w-4 h-4" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><!-- !Font Awesome Free 6.5.1 by @fontawesome - https://fontawesome.com License - https://fontawesome.com/license/free Copyright 2024 Fonticons, Inc. --><path d="M64 464l48 0 0 48-48 0c-35.3 0-64-28.7-64-64L0 64C0 28.7 28.7 0 64 0L229.5 0c17 0 33.3 6.7 45.3 18.7l90.5 90.5c12 12 18.7 28.3 18.7 45.3L384 304l-48 0 0-144-80 0c-17.7 0-32-14.3-32-32l0-80L64 48c-8.8 0-16 7.2-16 16l0 384c0 8.8 7.2 16 16 16zM176 352l32 0c30.9 0 56 25.1 56 56s-25.1 56-56 56l-16 0 0 32c0 8.8-7.2 16-16 16s-16-7.2-16-16l0-48 0-80c0-8.8 7.2-16 16-16zm32 80c13.3 0 24-10.7 24-24s-10.7-24-24-24l-16 0 0 48 16 0zm96-80l32 0c26.5 0 48 21.5 48 48l0 64c0 26.5-21.5 48-48 48l-32 0c-8.8 0-16-7.2-16-16l0-128c0-8.8 7.2-16 16-16zm32 128c8.8 0 16-7.2 16-16l0-64c0-8.8-7.2-16-16-16l-16 0 0 96 16 0zm80-112c0-8.8 7.2-16 16-16l48 0c8.8 0 16 7.2 16 16s-7.2 16-16 16l-32 0 0 32 32 0c8.8 0 16 7.2 16 16s-7.2 16-16 16l-32 0 0 48c0 8.8-7.2 16-16 16s-16-7.2-16-16l0-64 0-64z"/></svg>
                    </button>
                    <button type="button" class="group shadow-md btn bg-white border-transparent text-black hover:bg-cyan-1050">
                        <span class="group-hover:text-white">Exportar Excel</span>
                        <svg class="fill-black group-hover:fill-white w-4 h-4" xmlns="http://www.w3.org/2000/svg"  viewBox="0 0 384 512"><!--!Font Awesome Free 6.5.1 by @fontawesome - https://fontawesome.com License - https://fontawesome.com/license/free Copyright 2024 Fonticons, Inc. --><path d="M48 448V64c0-8.8 7.2-16 16-16H224v80c0 17.7 14.3 32 32 32h80V448c0 8.8-7.2 16-16 16H64c-8.8 0-16-7.2-16-16zM64 0C28.7 0 0 28.7 0 64V448c0 35.3 28.7 64 64 64H320c35.3 0 64-28.7 64-64V154.5c0-17-6.7-33.3-18.7-45.3L274.7 18.7C262.7 6.7 246.5 0 229.5 0H64zm90.9 233.3c-8.1-10.5-23.2-12.3-33.7-4.2s-12.3 23.2-4.2 33.7L161.6 320l-44.5 57.3c-8.1 10.5-6.3 25.5 4.2 33.7s25.5 6.3 33.7-4.2L192 359.1l37.1 47.6c8.1 10.5 23.2 12.3 33.7 4.2s12.3-23.2 4.2-33.7L222.4 320l44.5-57.3c8.1-10.5 6.3-25.5-4.2-33.7s-25.5-6.3-33.7 4.2L192 280.9l-37.1-47.6z"/></svg>
                    </button>
                </div>
            </div>
        </header>
        <main>
            <!--FORMULARIOS-->
            <div class="mt-5">
                <form class="card w-full bg-base-100 shadow-xl">
                    <div class="card-body">
                        <h2 class="card-title">Formulario</h2>
                        <div class="grid grid-cols-1 md:grid-cols-2 gap-4">
                          <label class="form-control w-full">
                            <div class="label">
                              <span class="label-text">Nombre:</span>
                            </div>
                            <input type="text" placeholder="Type here" class="input input-bordered w-full" />
                          </label>
                          <label class="form-control w-full">
                            <div class="label">
                              <span class="label-text">Correo:</span>
                            </div>
                            <input type="email" placeholder="Type here" class="input input-bordered w-full" />
                          </label>
                          <label class="form-control w-full">
                            <div class="label">
                              <span class="label-text">Buscar:</span>
                            </div>
                            <input type="search" placeholder="Type here" class="input input-bordered w-full" />
                          </label>
                          <label class="form-control w-full">
                            <div class="label">
                              <span class="label-text">Contraseña:</span>
                            </div>
                            <input type="password" placeholder="Type here" class="input input-bordered w-full" />
                          </label>
                          <label class="form-control w-full">
                            <div class="label">
                              <span class="label-text">Fecha:</span>
                            </div>
                            <input type="date" placeholder="Type here" class="input input-bordered w-full" />
                          </label>
                          <label class="form-control w-full">
                            <div class="label">
                              <span class="label-text">Seleccione:</span>
                            </div>
                            <select class="select select-bordered w-full">
                              <option disabled selected>Who shot first?</option>
                              <option>Han Solo</option>
                              <option>Greedo</option>
                            </select>
                          </label>
                          <label class="form-control w-full">
                            <div class="label">
                              <span class="label-text">Texto area:</span>
                            </div>
                            <textarea class="textarea textarea-bordered" placeholder="Bio"></textarea>
                          </label>
                          <div class="aform-control">
                            <label class="alabel cursor-pointer flex justify-start space-x-2">
                              <span class="alabel-text">checkbox</span> 
                              <input type="checkbox" checked="checked" class="checkbox checkbox-primary" />
                            </label>
                          </div>
                          <label class="form-control w-full">
                            <div class="label">
                              <span class="label-text">Subir archivo:</span>
                            </div>
                            <input type="file" class="file-input file-input-bordered w-full" />
                          </label>
                          <div class="form-control">
                            <label class="label cursor-pointer">
                              <span class="label-text">Radio boton</span> 
                              <input type="radio" name="radio-10" class="radio checked:bg-cyan-1050" checked />
                            </label>
                            <label class="label cursor-pointer">
                              <span class="label-text">Radio boton</span> 
                              <input type="radio" name="radio-10" class="radio checked:bg-cyan-1050" checked />
                            </label>
                          </div>
                          <label class="form-control w-full">
                            <div class="label">
                              <span class="label-text">Rango:</span>
                            </div>
                            <input type="range" min="0" max="100" value="40" class="range" />
                          </label>
                          <label class="form-control w-full">
                            <div class="label">
                              <span class="label-text">Toggle:</span>
                            </div>
                            <input type="checkbox" class="toggle" checked />
                          </label>
                        </div>
                    </div>
                </form>
                <div class="mt-5 mb-5 card w-full bg-base-100 shadow-xl">
                  <div class="card-body">
                    <h2 class="card-title">Modales</h2>
                    <div>
                      <button class="btn bg-cyan-1000 hover:bg-cyan-1050 text-white" onclick="my_modal_1.showModal()">Modal base</button>
                      <button class="btn bg-cyan-1000 hover:bg-cyan-1050 text-white" onclick="my_modal_2.showModal()">Modal ancho</button>
                      <button class="btn bg-cyan-1000 hover:bg-cyan-1050 text-white" onclick="my_modal_3.showModal()">Modal tabla</button>
                    </div>
                    <dialog id="my_modal_1" class="modal w-full">
                      <div class="modal-box">
                        <form method="dialog" >
                          <button class="group btn btn-sm btn-circle btn-ghost absolute right-2 top-2 hover:bg-cyan-1050">
                            <svg class="group-hover:fill-white w-5 h-5" xmlns="http://www.w3.org/2000/svg" viewBox="0 -960 960 960"><path d="m256-200-56-56 224-224-224-224 56-56 224 224 224-224 56 56-224 224 224 224-56 56-224-224-224 224Z"/></svg>
                          </button>
                        </form>
                        <h3 class="font-bold text-lg">Titulo</h3>
                        <p class="py-4">Press ESC key or click on X button to close</p>
                      </div>
                      <form method="dialog" class="modal-backdrop">
                        <button>close</button>
                      </form>
                    </dialog>
                    <dialog id="my_modal_2" class="modal w-full">
                      <div class="modal-box max-w-full">
                        <form method="dialog" >
                          <button class="group btn btn-sm btn-circle btn-ghost absolute right-2 top-2 hover:bg-cyan-1050">
                            <svg class="group-hover:fill-white w-5 h-5" xmlns="http://www.w3.org/2000/svg" viewBox="0 -960 960 960"><path d="m256-200-56-56 224-224-224-224 56-56 224 224 224-224 56 56-224 224 224 224-56 56-224-224-224 224Z"/></svg>
                          </button>
                        </form>
                        <h3 class="font-bold text-lg">Titulo</h3>
                        <p class="py-4">Press ESC key or click on X button to close</p>
                      </div>
                      <form method="dialog" class="modal-backdrop">
                        <button>close</button>
                      </form>
                    </dialog>
                    <dialog id="my_modal_3" class="modal w-full">
                      <div class="modal-box max-w-full">
                        <form method="dialog" >
                          <button class="group btn btn-sm btn-circle btn-ghost absolute right-2 top-2 hover:bg-cyan-1050">
                            <svg class="group-hover:fill-white w-5 h-5" xmlns="http://www.w3.org/2000/svg" viewBox="0 -960 960 960"><path d="m256-200-56-56 224-224-224-224 56-56 224 224 224-224 56 56-224 224 224 224-56 56-224-224-224 224Z"/></svg>
                          </button>
                        </form>
                        <label class="form-control w-full">
                            <div class="label">
                              <span class="label-text font-bold text-lg">Buscar:</span>
                            </div>
                            <input type="search" placeholder="Nombre, codigo, etc..." class="input input-bordered w-full" />
                        </label>
                        <div class="overflow-auto h-[50vh]">
                          <table class="table text-center">
                            <!-- head -->
                            <thead>
                              <tr>
                                <th>Código</th>
                                <th>Datos</th>
                                <th>Estado</th>
                                <th>Opciones</th>
                              </tr>
                            </thead>
                            <tbody>
                              <tr>
                                <th>1</th>
                                <td>Dato</td>
                                <td><div class="badge bg-green-500 text-white">Activo</div></td>
                                <td>
                                  <button type="button" title="eliminar" class="group shadow-md btn bg-red-500 border-transparent text-white hover:bg-red-500">
                                    <svg class="fill-white w-5 h-5" xmlns="http://www.w3.org/2000/svg"  viewBox="0 -960 960 960"><path d="M280-120q-33 0-56.5-23.5T200-200v-520h-40v-80h200v-40h240v40h200v80h-40v520q0 33-23.5 56.5T680-120H280Zm400-600H280v520h400v-520ZM360-280h80v-360h-80v360Zm160 0h80v-360h-80v360ZM280-720v520-520Z"/></svg>
                                  </button>
                                </td>
                              </tr>
                              <tr class="bg-base-200">
                                <th>2</th>
                                <td>Hart Hagerty</td>
                                <td><div class="badge bg-red-500 text-white">Reversado</div></td>
                                <td>
                                  <button type="button" title="eliminar" class="group shadow-md btn bg-red-500 border-transparent text-white hover:bg-red-500">
                                    <svg class="fill-white w-5 h-5" xmlns="http://www.w3.org/2000/svg"  viewBox="0 -960 960 960"><path d="M280-120q-33 0-56.5-23.5T200-200v-520h-40v-80h200v-40h240v40h200v80h-40v520q0 33-23.5 56.5T680-120H280Zm400-600H280v520h400v-520ZM360-280h80v-360h-80v360Zm160 0h80v-360h-80v360ZM280-720v520-520Z"/></svg>
                                  </button>
                                </td>
                              </tr>
                              <tr>
                                <th>1</th>
                                <td>Dato</td>
                                <td><div class="badge bg-green-500 text-white">Activo</div></td>
                                <td>
                                  <button type="button" title="eliminar" class="group shadow-md btn bg-red-500 border-transparent text-white hover:bg-red-500">
                                    <svg class="fill-white w-5 h-5" xmlns="http://www.w3.org/2000/svg"  viewBox="0 -960 960 960"><path d="M280-120q-33 0-56.5-23.5T200-200v-520h-40v-80h200v-40h240v40h200v80h-40v520q0 33-23.5 56.5T680-120H280Zm400-600H280v520h400v-520ZM360-280h80v-360h-80v360Zm160 0h80v-360h-80v360ZM280-720v520-520Z"/></svg>
                                  </button>
                                </td>
                              </tr>
                              <tr class="bg-base-200">
                                <th>2</th>
                                <td>Hart Hagerty</td>
                                <td><div class="badge bg-red-500 text-white">Reversado</div></td>
                                <td>
                                  <button type="button" title="eliminar" class="group shadow-md btn bg-red-500 border-transparent text-white hover:bg-red-500">
                                    <svg class="fill-white w-5 h-5" xmlns="http://www.w3.org/2000/svg"  viewBox="0 -960 960 960"><path d="M280-120q-33 0-56.5-23.5T200-200v-520h-40v-80h200v-40h240v40h200v80h-40v520q0 33-23.5 56.5T680-120H280Zm400-600H280v520h400v-520ZM360-280h80v-360h-80v360Zm160 0h80v-360h-80v360ZM280-720v520-520Z"/></svg>
                                  </button>
                                </td>
                              </tr>
                              <tr>
                                <th>1</th>
                                <td>Dato</td>
                                <td><div class="badge bg-green-500 text-white">Activo</div></td>
                                <td>
                                  <button type="button" title="eliminar" class="group shadow-md btn bg-red-500 border-transparent text-white hover:bg-red-500">
                                    <svg class="fill-white w-5 h-5" xmlns="http://www.w3.org/2000/svg"  viewBox="0 -960 960 960"><path d="M280-120q-33 0-56.5-23.5T200-200v-520h-40v-80h200v-40h240v40h200v80h-40v520q0 33-23.5 56.5T680-120H280Zm400-600H280v520h400v-520ZM360-280h80v-360h-80v360Zm160 0h80v-360h-80v360ZM280-720v520-520Z"/></svg>
                                  </button>
                                </td>
                              </tr>
                              <tr class="bg-base-200">
                                <th>2</th>
                                <td>Hart Hagerty</td>
                                <td><div class="badge bg-red-500 text-white">Reversado</div></td>
                                <td>
                                  <button type="button" title="eliminar" class="group shadow-md btn bg-red-500 border-transparent text-white hover:bg-red-500">
                                    <svg class="fill-white w-5 h-5" xmlns="http://www.w3.org/2000/svg"  viewBox="0 -960 960 960"><path d="M280-120q-33 0-56.5-23.5T200-200v-520h-40v-80h200v-40h240v40h200v80h-40v520q0 33-23.5 56.5T680-120H280Zm400-600H280v520h400v-520ZM360-280h80v-360h-80v360Zm160 0h80v-360h-80v360ZM280-720v520-520Z"/></svg>
                                  </button>
                                </td>
                              </tr>
                              <tr>
                                <th>1</th>
                                <td>Dato</td>
                                <td><div class="badge bg-green-500 text-white">Activo</div></td>
                                <td>
                                  <button type="button" title="eliminar" class="group shadow-md btn bg-red-500 border-transparent text-white hover:bg-red-500">
                                    <svg class="fill-white w-5 h-5" xmlns="http://www.w3.org/2000/svg"  viewBox="0 -960 960 960"><path d="M280-120q-33 0-56.5-23.5T200-200v-520h-40v-80h200v-40h240v40h200v80h-40v520q0 33-23.5 56.5T680-120H280Zm400-600H280v520h400v-520ZM360-280h80v-360h-80v360Zm160 0h80v-360h-80v360ZM280-720v520-520Z"/></svg>
                                  </button>
                                </td>
                              </tr>
                              <tr class="bg-base-200">
                                <th>2</th>
                                <td>Hart Hagerty</td>
                                <td><div class="badge bg-red-500 text-white">Reversado</div></td>
                                <td>
                                  <button type="button" title="eliminar" class="group shadow-md btn bg-red-500 border-transparent text-white hover:bg-red-500">
                                    <svg class="fill-white w-5 h-5" xmlns="http://www.w3.org/2000/svg"  viewBox="0 -960 960 960"><path d="M280-120q-33 0-56.5-23.5T200-200v-520h-40v-80h200v-40h240v40h200v80h-40v520q0 33-23.5 56.5T680-120H280Zm400-600H280v520h400v-520ZM360-280h80v-360h-80v360Zm160 0h80v-360h-80v360ZM280-720v520-520Z"/></svg>
                                  </button>
                                </td>
                              </tr>
                            </tbody>
                          </table>
                        </div>
                        <div class="flex justify-center join">
                          <button class="join-item btn bg-cyan-1000 text-white hover:bg-cyan-1050">
                            <svg class="fill-white w-5 h-5" xmlns="http://www.w3.org/2000/svg" viewBox="0 -960 960 960"><path d="M440-240 200-480l240-240 56 56-183 184 183 184-56 56Zm264 0L464-480l240-240 56 56-183 184 183 184-56 56Z"/></svg>
                          </button>
                          <button class="join-item btn bg-cyan-1000 text-white hover:bg-cyan-1050">
                            <svg class="fill-white w-5 h-5" xmlns="http://www.w3.org/2000/svg" viewBox="0 -960 960 960"><path d="M560-240 320-480l240-240 56 56-184 184 184 184-56 56Z"/></svg>
                          </button>
                          <button class="join-item btn bg-cyan-1000 text-white hover:bg-cyan-1050">Pagina 1 de 22</button>
                          <button class="join-item btn bg-cyan-1000 text-white hover:bg-cyan-1050">
                            <svg class="fill-white w-5 h-5" xmlns="http://www.w3.org/2000/svg" viewBox="0 -960 960 960"><path d="M504-480 320-664l56-56 240 240-240 240-56-56 184-184Z"/></svg>
                          </button>
                          <button class="join-item btn bg-cyan-1000 text-white hover:bg-cyan-1050">
                            <svg class="fill-white w-5 h-5" xmlns="http://www.w3.org/2000/svg" viewBox="0 -960 960 960"><path d="M383-480 200-664l56-56 240 240-240 240-56-56 183-184Zm264 0L464-664l56-56 240 240-240 240-56-56 183-184Z"/></svg>
                          </button>
                        </div>
                      </div>
                      <form method="dialog" class="modal-backdrop">
                        <button>close</button>
                      </form>
                    </dialog>
                  </div>
                </div>
                <div class="mt-5 mb-5 card w-full bg-base-100 shadow-xl">
                  <div class="card-body">
                    <h2 class="card-title">Tablas</h2>
                    <div class="overflow-auto h-[50vh]">
                      <table class="table text-center">
                        <!-- head -->
                        <thead>
                          <tr>
                            <th>Código</th>
                            <th>Datos</th>
                            <th>Estado</th>
                            <th>Opciones</th>
                          </tr>
                        </thead>
                        <tbody>
                          <tr>
                            <th>1</th>
                            <td>Dato</td>
                            <td><div class="badge bg-green-500 text-white">Activo</div></td>
                            <td>
                              <button type="button" title="eliminar" class="group shadow-md btn bg-red-500 border-transparent text-white hover:bg-red-500">
                                <svg class="fill-white w-5 h-5" xmlns="http://www.w3.org/2000/svg"  viewBox="0 -960 960 960"><path d="M280-120q-33 0-56.5-23.5T200-200v-520h-40v-80h200v-40h240v40h200v80h-40v520q0 33-23.5 56.5T680-120H280Zm400-600H280v520h400v-520ZM360-280h80v-360h-80v360Zm160 0h80v-360h-80v360ZM280-720v520-520Z"/></svg>
                              </button>
                            </td>
                          </tr>
                          <tr class="bg-base-200">
                            <th>2</th>
                            <td>Hart Hagerty</td>
                            <td><div class="badge bg-red-500 text-white">Reversado</div></td>
                            <td>
                              <button type="button" title="eliminar" class="group shadow-md btn bg-red-500 border-transparent text-white hover:bg-red-500">
                                <svg class="fill-white w-5 h-5" xmlns="http://www.w3.org/2000/svg"  viewBox="0 -960 960 960"><path d="M280-120q-33 0-56.5-23.5T200-200v-520h-40v-80h200v-40h240v40h200v80h-40v520q0 33-23.5 56.5T680-120H280Zm400-600H280v520h400v-520ZM360-280h80v-360h-80v360Zm160 0h80v-360h-80v360ZM280-720v520-520Z"/></svg>
                              </button>
                            </td>
                          </tr>
                          <tr>
                            <th>1</th>
                            <td>Dato</td>
                            <td><div class="badge bg-green-500 text-white">Activo</div></td>
                            <td>
                              <button type="button" title="eliminar" class="group shadow-md btn bg-red-500 border-transparent text-white hover:bg-red-500">
                                <svg class="fill-white w-5 h-5" xmlns="http://www.w3.org/2000/svg"  viewBox="0 -960 960 960"><path d="M280-120q-33 0-56.5-23.5T200-200v-520h-40v-80h200v-40h240v40h200v80h-40v520q0 33-23.5 56.5T680-120H280Zm400-600H280v520h400v-520ZM360-280h80v-360h-80v360Zm160 0h80v-360h-80v360ZM280-720v520-520Z"/></svg>
                              </button>
                            </td>
                          </tr>
                          <tr class="bg-base-200">
                            <th>2</th>
                            <td>Hart Hagerty</td>
                            <td><div class="badge bg-red-500 text-white">Reversado</div></td>
                            <td>
                              <button type="button" title="eliminar" class="group shadow-md btn bg-red-500 border-transparent text-white hover:bg-red-500">
                                <svg class="fill-white w-5 h-5" xmlns="http://www.w3.org/2000/svg"  viewBox="0 -960 960 960"><path d="M280-120q-33 0-56.5-23.5T200-200v-520h-40v-80h200v-40h240v40h200v80h-40v520q0 33-23.5 56.5T680-120H280Zm400-600H280v520h400v-520ZM360-280h80v-360h-80v360Zm160 0h80v-360h-80v360ZM280-720v520-520Z"/></svg>
                              </button>
                            </td>
                          </tr>
                          <tr>
                            <th>1</th>
                            <td>Dato</td>
                            <td><div class="badge bg-green-500 text-white">Activo</div></td>
                            <td>
                              <button type="button" title="eliminar" class="group shadow-md btn bg-red-500 border-transparent text-white hover:bg-red-500">
                                <svg class="fill-white w-5 h-5" xmlns="http://www.w3.org/2000/svg"  viewBox="0 -960 960 960"><path d="M280-120q-33 0-56.5-23.5T200-200v-520h-40v-80h200v-40h240v40h200v80h-40v520q0 33-23.5 56.5T680-120H280Zm400-600H280v520h400v-520ZM360-280h80v-360h-80v360Zm160 0h80v-360h-80v360ZM280-720v520-520Z"/></svg>
                              </button>
                            </td>
                          </tr>
                          <tr class="bg-base-200">
                            <th>2</th>
                            <td>Hart Hagerty</td>
                            <td><div class="badge bg-red-500 text-white">Reversado</div></td>
                            <td>
                              <button type="button" title="eliminar" class="group shadow-md btn bg-red-500 border-transparent text-white hover:bg-red-500">
                                <svg class="fill-white w-5 h-5" xmlns="http://www.w3.org/2000/svg"  viewBox="0 -960 960 960"><path d="M280-120q-33 0-56.5-23.5T200-200v-520h-40v-80h200v-40h240v40h200v80h-40v520q0 33-23.5 56.5T680-120H280Zm400-600H280v520h400v-520ZM360-280h80v-360h-80v360Zm160 0h80v-360h-80v360ZM280-720v520-520Z"/></svg>
                              </button>
                            </td>
                          </tr>
                          <tr>
                            <th>1</th>
                            <td>Dato</td>
                            <td><div class="badge bg-green-500 text-white">Activo</div></td>
                            <td>
                              <button type="button" title="eliminar" class="group shadow-md btn bg-red-500 border-transparent text-white hover:bg-red-500">
                                <svg class="fill-white w-5 h-5" xmlns="http://www.w3.org/2000/svg"  viewBox="0 -960 960 960"><path d="M280-120q-33 0-56.5-23.5T200-200v-520h-40v-80h200v-40h240v40h200v80h-40v520q0 33-23.5 56.5T680-120H280Zm400-600H280v520h400v-520ZM360-280h80v-360h-80v360Zm160 0h80v-360h-80v360ZM280-720v520-520Z"/></svg>
                              </button>
                            </td>
                          </tr>
                          <tr class="bg-base-200">
                            <th>2</th>
                            <td>Hart Hagerty</td>
                            <td><div class="badge bg-red-500 text-white">Reversado</div></td>
                            <td>
                              <button type="button" title="eliminar" class="group shadow-md btn bg-red-500 border-transparent text-white hover:bg-red-500">
                                <svg class="fill-white w-5 h-5" xmlns="http://www.w3.org/2000/svg"  viewBox="0 -960 960 960"><path d="M280-120q-33 0-56.5-23.5T200-200v-520h-40v-80h200v-40h240v40h200v80h-40v520q0 33-23.5 56.5T680-120H280Zm400-600H280v520h400v-520ZM360-280h80v-360h-80v360Zm160 0h80v-360h-80v360ZM280-720v520-520Z"/></svg>
                              </button>
                            </td>
                          </tr>
                        </tbody>
                      </table>
                    </div>
                    <div class="flex justify-center join">
                      <button class="join-item btn bg-cyan-1000 text-white hover:bg-cyan-1050">
                        <svg class="fill-white w-5 h-5" xmlns="http://www.w3.org/2000/svg" viewBox="0 -960 960 960"><path d="M440-240 200-480l240-240 56 56-183 184 183 184-56 56Zm264 0L464-480l240-240 56 56-183 184 183 184-56 56Z"/></svg>
                      </button>
                      <button class="join-item btn bg-cyan-1000 text-white hover:bg-cyan-1050">
                        <svg class="fill-white w-5 h-5" xmlns="http://www.w3.org/2000/svg" viewBox="0 -960 960 960"><path d="M560-240 320-480l240-240 56 56-184 184 184 184-56 56Z"/></svg>
                      </button>
                      <button class="join-item btn bg-cyan-1000 text-white hover:bg-cyan-1050">Pagina 1 de 22</button>
                      <button class="join-item btn bg-cyan-1000 text-white hover:bg-cyan-1050">
                        <svg class="fill-white w-5 h-5" xmlns="http://www.w3.org/2000/svg" viewBox="0 -960 960 960"><path d="M504-480 320-664l56-56 240 240-240 240-56-56 184-184Z"/></svg>
                      </button>
                      <button class="join-item btn bg-cyan-1000 text-white hover:bg-cyan-1050">
                        <svg class="fill-white w-5 h-5" xmlns="http://www.w3.org/2000/svg" viewBox="0 -960 960 960"><path d="M383-480 200-664l56-56 240 240-240 240-56-56 183-184Zm264 0L464-664l56-56 240 240-240 240-56-56 183-184Z"/></svg>
                      </button>
                    </div>
                  </div>
                </div>
            </div>
        </main>
        <footer>
            
        </footer>
    </div>
</body>
</html>