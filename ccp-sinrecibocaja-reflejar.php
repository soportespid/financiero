<?php
	header("Cache-control: no-cache, no-store, must-revalidate");
    header("Content-Type: text/html;charset=utf8");
    require 'comun.inc';
    require 'funciones.inc';
    require 'conversor.php';
    session_start();
    $linkbd = conectar_v7();
    $linkbd -> set_charset("utf8");
    cargarcodigopag($_GET['codpag'],$_SESSION["nivel"]);
    date_default_timezone_set("America/Bogota");
	date_default_timezone_set("America/Bogota");
?>
<!DOCTYPE >
<html lang=es>
	<head>
		<meta charset="utf-8"/>
		<meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1"/>
		<meta name="viewport" content="user-scalable=no">
		<title>:: IDEAL 10 - Presupuesto CCPET</title>
		<link href="favicon.ico" rel="shortcut icon"/>
		<link href="css/css2.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<link href="css/tabs.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<script src="css/programas.js?<?php echo date('d_m_Y_h_i_s');?>"></script>

		<script>

			function validar()
			{
				document.form2.submit();
			}

			function guardar()
			{

				if(document.form2.fecha.value!='' && ((document.form2.modorec.value=='banco' && document.form2.banco.value!='') || (document.form2.modorec.value=='caja')))
				{
					if (confirm("Esta Seguro de Guardar"))
					{
						document.form2.oculto.value=2;
						document.form2.submit();
					}
				}
				else{
					alert('Faltan datos para completar el registro');
					document.form2.fecha.focus();
					document.form2.fecha.select();
				}
			}

			function adelante()
			{
                
				if(parseFloat(document.form2.ncomp.value)<parseFloat(document.form2.maximo.value))
				{
					document.form2.oculto.value=1;
					document.form2.ncomp.value=parseFloat(document.form2.ncomp.value)+1;
					document.form2.idcomp.value=parseFloat(document.form2.idcomp.value)+1;
					document.form2.action="ccp-sinrecibocaja-reflejar.php";
					document.form2.submit();
				}
			}

			function atrasc()
			{
				if(document.form2.ncomp.value>1)
				{
					document.form2.oculto.value=1;
					document.form2.ncomp.value=document.form2.ncomp.value-1;
					document.form2.idcomp.value=document.form2.idcomp.value-1;
					document.form2.action="ccp-sinrecibocaja-reflejar.php";
					document.form2.submit();
				}
			}

			function validar2()
			{
				document.form2.oculto.value=1;
				document.form2.ncomp.value=document.form2.idcomp.value;
				document.form2.action="ccp-sinrecibocaja-reflejar.php";
				document.form2.submit();
			}
		</script>

		<script src="css/programas.js"></script>
		<link href="css/css2.css" rel="stylesheet" type="text/css" />
		<link href="css/css3.css" rel="stylesheet" type="text/css" />
		<link href="css/tabs.css" rel="stylesheet" type="text/css" />
	</head>
	<body>
		<IFRAME src="alertas.php" name="alertas" id="alertas" style="display:none"></IFRAME>
		<span id="todastablas2"></span>
		<table>
			<tr><script>barra_imagenes("ccpet");</script><?php cuadro_titulos();?></tr>	 
			<tr><?php menu_desplegable("ccpet");?></tr>
			<tr>
			<td colspan="3" class="cinta">
				<a class="mgbt" href="#" ><img src="imagenes/add2.png" alt="Nuevo"  border="0" /></a> 
				<a class="mgbt" href="#"><img src="imagenes/guardad.png"  alt="Guardar" /></a> 
				<a class="mgbt" href="ccp-buscasinrecibocaja-reflejar.php"> <img src="imagenes/busca.png"  alt="Buscar" /></a> 
				<a class="mgbt" href="#" onClick="mypop=window.open('ccp-principal.php','','');mypop.focus();"><img src="imagenes/nv.png" alt="nueva ventana"></a> 
				<a class="mgbt" href="#" onClick="guardar()"><img src="imagenes/reflejar1.png"  alt="Reflejar" style="width:24px;" /></a>
				<a class="mgbt" href="ccp-reflejar.php"><img src="imagenes/iratras.png" alt="nueva ventana"></a></td>
			</tr>		  
		</table>
		<tr>
			<td colspan="3" class="tablaprin" align="center"> 
                <form name="form2" method="post" action=""> 
                    <?php
                    
                    
                    if ($_GET['idrecibo']!=""){echo "<script>document.getElementById('codrec').value=$_GET[idrecibo];</script>";}
                    $sqlr="SELECT id_recibos, id_recaudo, vigencia FROM tesosinreciboscaja ORDER BY id_recibos DESC";
                    $res=mysqli_query($linkbd, $sqlr);
                    $r=mysqli_fetch_row($res);
                    $_POST['maximo']=$r[0];
                    $vigusu=$r[2];
                    $vigencia=$vigusu;
                    if(!$_POST['oculto']){
                        $check1="checked";
                        $fec=date("d/m/Y");
                        $_POST['vigencia']=$vigencia;
                        if ($_POST['codrec']!="" || $_GET['idrecibo']!=""){
                            if($_POST['codrec']!=""){
                                $sqlr="SELECT id_recibos,id_recaudo FROM  tesoreciboscaja WHERE id_recibos='$_POST[codrec]'";
                            }
                            else{
                                $sqlr="SELECT id_recibos,id_recaudo FROM  tesoreciboscaja WHERE id_recibos='$_GET[idrecibo]'";
                            }
                        }
                        else{
                            $sqlr="SELECT id_recibos,id_recaudo FROM  tesosinreciboscaja ORDER BY id_recibos DESC";
                        }
                        $res=mysqli_query($linkbd, $sqlr);
                        $r=mysqli_fetch_row($res);
                        $_POST['ncomp']=$r[0];
                        $_POST['idcomp']=$r[0];
                        $_POST['idrecaudo']=$r[1];
                        if($_GET['idrecibo']!=""){
                            $_POST['idcomp']=$_GET['idrecibo'];
                            $_POST['ncomp']=$_GET['idrecibo'];
                        }
                    }
                    $sqlr="SELECT * FROM tesosinreciboscaja WHERE id_recibos=$_POST[idcomp]";
                    $res=mysqli_query($linkbd, $sqlr);
                    while($r=mysqli_fetch_row($res)){
                        $_POST['tiporec']=$r[10];
                        $_POST['idrecaudo']=$r[4];
                        $_POST['ncomp']=$r[0];
                        $_POST['modorec']=$r[5];	
                    }
                    switch($_POST['tabgroup1']){
                        case 1:
                            $check1='checked';
                            break;
                        case 2:
                            $check2='checked';
                            break;
                        case 3:
                            $check3='checked';
                    }
                    ?>
                    <input name="encontro" type="hidden" value="<?php echo $_POST['encontro']?>" >
                    <?php 
                    switch($_POST['tiporec']){
                        case 3:
                            $sqlr="SELECT * FROM tesosinrecaudos WHERE tesosinrecaudos.id_recaudo=$_POST[idrecaudo] AND 3=$_POST[tiporec]";
                            $_POST['encontro']="";
                            $res=mysqli_query($linkbd, $sqlr);
                            while ($row =mysqli_fetch_row($res))
                            {
                                $_POST['concepto']=$row[6];
                                $_POST['valorecaudo']=$row[5];
                                $_POST['totalc']=$row[5];
                                $_POST['tercero']=$row[4];
                                $_POST['ntercero']=buscatercero($row[4]);
                                $_POST['encontro']=1;
                            }
                            $sqlr="SELECT * FROM tesosinreciboscaja WHERE id_recibos=$_POST[idcomp] ";
                            $res=mysqli_query($linkbd, $sqlr);
                            $row =mysqli_fetch_row($res);
                            $_POST['fecha']=$row[2];
                            $_POST['estadoc']=$row[9];
                            if ($row[9]=='N')
                            {
                                $_POST['estado']="ANULADO";
                                $_POST['estadoc']='0';
                            }
                            else
                            {
                                $_POST['estadoc']='1';
                                $_POST['estado']="ACTIVO";
                            }

                            $_POST['modorec']=$row[5];
                            $_POST['banco']=$row[7];
                            preg_match("/([0-9]{4})\-([0-9]{2})\-([0-9]{2})/", $_POST['fecha'],$fecha);
                            $_POST['fecha']=$fecha[3]."/".$fecha[2]."/".$fecha[1];
                            $vigusu=$fecha[1];
                        break;	
                    }

                    ?>
                    <table class="inicio">
                        <tr>
                            <td class="titulos" colspan="6">Interfaz Ingresos Internos CCPET </td>
                            <td style="width:7%;" class="cerrar" ><a href="ccpet-principal.php">Cerrar</a></td>
                        </tr>
                        <tr>
                            <td class="saludo1" style="width:2.5cm;">No Recibo:</td>
                            <td style="width:10%;"> 
                                <a href="#" onClick="atrasc()"><img src="imagenes/back.png" alt="anterior" align="absmiddle"></a> 
                                <input name="cuentacaja" type="hidden" value="<?php echo $_POST['cuentacaja']?>" >
                                <input name="idcomp" type="text"  style="width:50%;" value="<?php echo $_POST['idcomp']?>" onKeyUp="return tabular(event,this) "  onBlur="validar2()"  >
                                <input name="ncomp" type="hidden" value="<?php echo $_POST['ncomp']?>">
                                <a href="#" onClick="adelante()"><img src="imagenes/next.png" alt="siguiente" align="absmiddle"></a> 
                                <input type="hidden" value="a" name="atras" ><input type="hidden" value="s" name="siguiente" >
                                <input type="hidden" value="<?php echo $_POST['maximo']?>" name="maximo">
                            </td>
                            <td class="saludo1" style="width:2.5cm;">Fecha:</td>
                            <td style="width:10%;">
                                <input name="fecha" type="text" value="<?php echo $_POST['fecha']?>" style="width:100%;" onKeyUp="return tabular(event,this)" readonly>        
                            </td>
                            <td class="saludo1" style="width:2.5cm;">Vigencia:</td>
                            <td style="width:38%;">
                                <input type="text" id="vigencia" name="vigencia" size="8" onKeyPress="javascript:return solonumeros(event)" onKeyUp="return tabular(event,this)"  value="<?php echo $_POST['vigencia']?>" onClick="document.getElementById('tipocta').focus();document.getElementById('tipocta').select();" readonly>   
                                <input type="text" name="estado" value="<?php echo $_POST['estado'] ?>" readonly>  
                                <input type="hidden" name="estadoc" value="<?php echo $_POST['estadoc'] ?>" readonly>      
                            </td>
                            <td></td>
                        </tr>
                        <tr>
                            <td class="saludo1" style="width:2.5cm;"> Recaudo:</td>
                            <td style="width:10%;"> 
                                <select name="tiporec" id="tiporec" onKeyUp="return tabular(event,this)" onChange="validar()" >
                                    <?php
                                    switch($_POST['tiporec']){
                                        case "3":	echo"<option value='3' SELECTED>Ingresos internos</option>";break;
                                    }
                                    ?>
                                </select>
                            </td>
                            <td class="saludo1">No Liquid:</td>
                            <td>
                                <input type="text" id="idrecaudo" name="idrecaudo" value="<?php echo $_POST['idrecaudo']?>" size="30" onKeyUp="return tabular(event,this)" onChange="validar()" readonly> 
                            </td>
                            <td class="saludo1">Recaudado en:</td>
                            <td style="width:38%;"> 
                                <select name="modorec" id="modorec" onKeyUp="return tabular(event,this)" onChange="validar()" style="width:16%;" >
                                    <?php
                                    if($_POST['modorec']=='banco'){
                                        echo"<option value='banco' SELECTED>Banco</option>";
                                    }
                                    else{
                                        echo"<option value='caja' SELECTED>Caja</option>";
                                    }
                                    ?>
                                </select>
                                <?php
                                if ($_POST['modorec']=='banco'){
                                ?>
                                    <select id="banco" name="banco"  onChange="validar()" onKeyUp="return tabular(event,this)" style="width:83%;">
                                    <?php
                                    $sqlr="SELECT tesobancosctas.estado,tesobancosctas.cuenta,tesobancosctas.ncuentaban,tesobancosctas.tipo,terceros.razonsocial,tesobancosctas.tercero FROM tesobancosctas,terceros WHERE tesobancosctas.tercero=terceros.cedulanit AND tesobancosctas.estado='S' ";
                                    $res=mysqli_query($linkbd, $sqlr);
                                    while ($row =mysqli_fetch_row($res)){
                                        echo "<option value=$row[1] ";
                                        if($row[1]==$_POST['banco']){
                                            echo "<option value='$row[1]' SELECTED>$row[2] - Cuenta $row[3] - $row[4]</option>";
                                            $_POST['nbanco']=$row[4];
                                            $_POST['ter']=$row[5];
                                            $_POST['cb']=$row[2];
                                        }
                                    }	 	
                                    ?>
                                    </select>
                                    <input name="cb" type="hidden" value="<?php echo $_POST['cb']?>" >
                                    <input type="hidden" id="ter" name="ter" value="<?php echo $_POST['ter']?>" >           
                                    <input type="hidden" id="nbanco" name="nbanco" value="<?php echo $_POST['nbanco']?>" size="40" readonly>
                                <?php
                                }
                                ?> 
                            </td>
                
                        </tr>
                        <tr>
                            <td class="saludo1" style="width:2.5cm;">Concepto:</td>
                            <td colspan="5">
                                <input name="concepto" style="width:100%;" type="text" value="<?php echo $_POST['concepto'] ?>" onKeyUp="return tabular(event,this)" readonly>
                            </td>
                        </tr>
                        <tr>
                            <td class="saludo1" style="width:2.5cm;">Valor:</td>
                            <td>
                                <input type="text" id="valorecaudo" name="valorecaudo" value="<?php echo number_format($_POST['valorecaudo'],2,',','.')?>" style="width:100%; text-align:right;" onKeyUp="return tabular(event,this)" readonly >
                            </td>
                            <td class="saludo1">Documento: </td>
                            <td>
                                <input name="tercero" type="text" value="<?php echo $_POST['tercero']?>" style="width:100%;" onKeyUp="return tabular(event,this)" readonly>
                            </td>
                            <td class="saludo1">Contribuyente:</td>
                            <td>
                                <input type="text" id="ntercero" name="ntercero" value="<?php echo $_POST['ntercero']?>" style="width:100%;" onKeyUp="return tabular(event,this)" readonly>
                                <input type="hidden" id="cb" name="cb" value="<?php echo $_POST['cb']?>" ><input type="hidden" id="ct" name="ct" value="<?php echo $_POST['ct']?>" >
                            </td>
                        </tr>
                    </table>
                    <input type="hidden" value="1" name="oculto">
                    <input type="hidden" value="<?php echo $_POST['trec']?>"  name="trec">
                    <input type="hidden" value="0" name="agregadet">
                    <div class="subpantallac7">
                        <?php
                        if($_POST['encontro']=='1'){
                            switch($_POST['tiporec']){	 	 
                                case 3: ///*****************otros recaudos *******************
                                    $_POST['trec']='OTROS RECAUDOS';	 
                                    $sqlr="SELECT * FROM tesosinrecaudos_det WHERE tesosinrecaudos_det.id_recaudo=$_POST[idrecaudo] AND 3=$_POST[tiporec]";
                                    $_POST['dcoding']= array(); 		 
                                    $_POST['dncoding']= array(); 		 
                                    $_POST['dvalores']= array(); 	
                                    $res=mysqli_query($linkbd, $sqlr);
                                    while($row =mysqli_fetch_row($res)){	
                                        $_POST['dcoding'][]=$row[2];
                                        $sqlr2="SELECT nombre FROM tesoingresos WHERE codigo='".$row[2]."'";
                                        $res2=mysqli_query($linkbd, $sqlr2);
                                        $row2 =mysqli_fetch_row($res2); 
                                        $_POST['dncoding'][]=$row2[0];			 		
                                        $_POST['dvalores'][]=$row[3];		 	
                                    }
                                break;
                            }
                        }
                        ?>
                        <table class="inicio">
                            <tr>
                                <td colspan="4" class="titulos">Detalle Ingresos Internos</td>
                            </tr>                  
                            <tr>
                                <td class="titulos2">Codigo</td>
                                <td class="titulos2">Ingreso</td>
                                <td class="titulos2">Valor</td>
                            </tr>
                            <?php 		
                            $_POST['totalc']=0;
                            $iter='saludo1a';
                            $iter2='saludo2';
                            for ($x=0;$x<count($_POST['dcoding']);$x++){
                                echo "<tr class='$iter' onMouseOver=\"anterior=this.style.backgroundColor; this.style.backgroundColor='#40b3ff';\" onMouseOut=\"this.style.backgroundColor=anterior\">
                                    <td style='width:10%'>
                                        <input name='dcoding[]' value='".$_POST['dcoding'][$x]."' type='hidden'>".$_POST['dcoding'][$x]."
                                    </td>
                                    <td>
                                        <input name='dncoding[]' value='".$_POST['dncoding'][$x]."' type='hidden'>".$_POST['dncoding'][$x]."
                                    </td>
                                    <td style='width:20%' align='right'>
                                        <input name='dvalores[]' value='".$_POST['dvalores'][$x]."' type='hidden'>$ ".number_format($_POST['dvalores'][$x],2,',','.')."
                                    </td>
                                </tr>";
                                $_POST['totalc']=$_POST['totalc']+$_POST['dvalores'][$x];
                                $_POST['totalcf']=number_format($_POST['totalc'],2);
                                $aux=$iter;
                                $iter=$iter2;
                                $iter2=$aux;
                            }
                            $resultado = convertir($_POST['totalc']);
                            $_POST['letras']=$resultado." PESOS M/CTE";
                            echo "<tr class='$iter'>
                                <td></td>
                                <td align='right'>Total</td>
                                <td align='right'>
                                    <input name='totalcf' type='hidden' value='$_POST[totalcf]'>
                                    <input name='totalc' type='hidden' value='$_POST[totalc]'>
                                    $ ".number_format($_POST['totalc'],2,',','.')."
                                </td>
                            </tr>
                            <tr class='titulos2'>
                                <td>Son:</td>
                                <td colspan='5'>
                                    <input name='letras' type='hidden' value='$_POST[letras]' size='90'>
                                    ".$_POST['letras']."
                                </td>
                            </tr>";
                            ?> 
                        </table>
                    </div>
                    <?php
                    if($_POST['oculto']=='2')
                    {
                        switch($_POST['tiporec']) 
                        {	  
                            case 3: //**************INGRESOS INTERNOS
    
                                $sqlr="DELETE FROM pptosinrecibocajappto WHERE idrecibo=$_POST[idcomp]";
                                mysqli_query($linkbd, $sqlr);
                                $consec=$_POST['idcomp'];
                                $idcomp=$consec;
                                echo "<table class='inicio'><tr><td class='saludo1'><center>Se ha reflejado a presupuesto el Ingreso Propio con Exito <img src='imagenes/confirm.png'><script></script></center></td></tr></table>";
        	
                                for($x=0;$x<count($_POST['dcoding']);$x++)
                                {
                                    //***** BUSQUEDA INGRESO ********
                                    $sqlri="SELECT * FROM tesoingresos_det WHERE codigo='".$_POST['dcoding'][$x]."' AND vigencia=(SELECT MAX(vigencia) FROM tesoingresos_det WHERE codigo='".$_POST['dcoding'][$x]."') AND cuentapres != ''";
                                    $resi=mysqli_query($linkbd, $sqlri);
                                    while($rowi=mysqli_fetch_row($resi))
                                    {
                                        if($rowi[6]!="")
                                        {
                                            $porce = $rowi[5];
                                            $valor = round($_POST['dvalores'][$x]*($porce/100),2);

                                            if($valor > 0){
                                                $sqlrFuente = "SELECT fuente FROM tesoingresos_fuentes WHERE codigo = '".$_POST['dcoding'][$x]."' AND estado = 'S'";
                                                $respFuente = mysqli_query($linkbd, $sqlrFuente);
                                                $rowFuente = mysqli_fetch_row($respFuente);
                                                $sqlr = "INSERT INTO pptosinrecibocajappto (cuenta, idrecibo, valor, vigencia, fuente, productoservicio, seccion_presupuestal, medio_pago, vigencia_gasto) VALUES('$rowi[6]', $_POST[idcomp], $valor, '$_POST[vigencia]', '$rowFuente[0]', '$rowi[7]', '16', 'CSF', '1')";
                                                mysqli_query($linkbd, $sqlr);
                                            }
                                        }
                                    }
                                }
                            break;
                        } //*****fin del switch
                    }//**fin del oculto 
                    ?>	
                </form>
            </td>
        </tr>
    </body>
</html>