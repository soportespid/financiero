<?php
	header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=utf8");
	require "comun.inc";
	require "funciones.inc";
	sesion();
	$linkbd=conectar_v7();
	$linkbd -> set_charset("utf8");
	cargarcodigopag($_GET[codpag],$_SESSION["nivel"]);
	date_default_timezone_set("America/Bogota");

	$cuenta = $_POST['cuenta'];
	$tabla = "<table class='inicio'>
	<tr style='text-align:center;'>
		<td class='titulos2'>Cuenta</td>
		<td class='titulos2'>Descripción</td>
		<td class='titulos2'>Ingresos</td>
		<td class='titulos2'>Adiciones</td>
		<td class='titulos2'>Reducciones</td>
	</tr>";
	$sqlver="
	SELECT id, codigocuenta, nombrecuenta, vinicialing, vadicion, vreduccion
	FROM ".$_SESSION['tablatemporal']."
	WHERE codigocuenta = '$cuenta'
	ORDER BY id ASC";
	$resver = mysqli_query($linkbd,$sqlver);
	while ($rowver = mysqli_fetch_row($resver))
	{
		$tabla.="<tr class='cssdeta' onMouseOver=\"anterior=this.style.backgroundColor;this.style.backgroundColor='#40b3ff';\"
onMouseOut=\"this.style.backgroundColor=anterior\" >
			<td>$rowver[1]</td>
			<td>$rowver[2]</td>
			<td>$".number_format($rowver[3],0,',','.')."</td>
			<td>$".number_format($rowver[4],0,',','.')."</td>
			<td>$".number_format($rowver[5],0,',','.')."</td>
		</tr>";
	}
$tabla.='</table>';
$data_row = array('detalle'=>$tabla);
header('Content-Type: application/json');
echo json_encode($data_row);
?>