<?php
header("Cache-control: no-cache, no-store, must-revalidate");
header("Content-Type: text/html;charset=utf8");

require 'comun.inc';
require 'funciones.inc';

$linkbd = conectar_v7();
$linkbd->set_charset("utf8");

session_start();
date_default_timezone_set("America/Bogota");
?>
<!DOCTYPE>
<html lang="es">

<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1" />
    <meta name="viewport" content="user-scalable=no">
    <title>:: IDEAL 10 - Parametrización</title>
    <link href="favicon.ico" rel="shortcut icon" />
    <link href="css/css2.css?<?php echo date('d_m_Y_h_i_s'); ?>" rel="stylesheet" type="text/css" />
    <link href="css/css3.css?<?php echo date('d_m_Y_h_i_s'); ?>" rel="stylesheet" type="text/css" />
    <link href="css/css4.css?<?php echo date('d_m_Y_h_i_s'); ?>" rel="stylesheet" type="text/css" />
    <link href="css/style.css?<?php echo date('d_m_Y_h_i_s'); ?>" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="css/calendario.js?<?php echo date('d_m_Y_h_i_s'); ?>"></script>
    <script type="text/javascript" src="css/programas.js?<?php echo date('d_m_Y_h_i_s'); ?>"></script>
    <script>
        function despliegamodalm(_valor, _tip, mensa, pregunta, variable) {
            document.getElementById("bgventanamodalm").style.visibility = _valor;
            if (_valor == "hidden") {
                document.getElementById('ventanam').src = "";
                if (document.getElementById('valfocus').value == "2") {
                    document.getElementById('valfocus').value = '1';
                    document.getElementById('documento').focus();
                    document.getElementById('documento').select();
                }
            }
            else {
                switch (_tip) {
                    case "1":
                        document.getElementById('ventanam').src = "ventana-mensaje1.php?titulos=" + mensa; break;
                    case "2":
                        document.getElementById('ventanam').src = "ventana-mensaje3.php?titulos=" + mensa; break;
                    case "3":
                        document.getElementById('ventanam').src = "ventana-mensaje2.php?titulos=" + mensa; break;
                    case "4":
                        document.getElementById('ventanam').src = "ventana-consulta1.php?titulos=" + mensa + "&idresp=" + pregunta; break;
                    case "5":
                        document.getElementById('ventanam').src = "ventana-elimina1.php?titulos=" + mensa + "&idresp=" + pregunta + "&variable=" + variable; break;
                }
            }
        }
        function funcionmensaje() {
            document.location.href = "inve-editadestinocompra.php?is=" + document.getElementById('numero').value + "&scrtop=0&totreg=0&altura=0&numpag=1&limreg=10&filtro=";
        }
        function respuestaconsulta(pregunta) {
            switch (pregunta) {
                case "1":
                    document.form2.oculto.value = "2";
                    document.form2.submit();
                    break;
                case "2":
                    document.getElementById('oculto').value = "6";
                    document.form2.submit(); break;
                    break;
            }
        }
        function guardar() {
            var val1 = document.getElementById('numero').value;
            var val2 = document.getElementById('nombre').value;
            var val3 = document.getElementById('cuenta').value;
            if ((val1.trim() != '') && (val2.trim() != '')) {
                despliegamodalm('visible', '4', 'Esta Seguro de Guardar', '1');
            }
            else {
                document.form2.numero.focus();
                document.form2.numero.select();
                despliegamodalm('visible', '2', 'Faltan Datos para Completar el Registro');
            }
        }
        function agregardetalle() {
            var val1 = document.getElementById('cuenta').value;
            var val2 = document.getElementById('cuentaF').value;

            if ((val1.trim() != '') && (val2.trim() != '')) {
                document.form2.agregadet.value = 1;
                document.form2.submit();
            }
            else {
                despliegamodalm('visible', '2', 'Faltan informacion para el detalle');
            }
        }
        function buscacta(e) {
            if (document.form2.cuenta.value != "") {
                document.form2.bcnt.value = '1';
                document.form2.submit();
            }
        }
        function buscacta2(e) {
            if (document.form2.cuentaF.value != "") {
                document.form2.bcnt2.value = '1';
                document.form2.submit();
            }
        }
        function despliegamodal2(_valor, _nomve) {
            document.getElementById("bgventanamodal2").style.visibility = _valor;
            if (_valor == "hidden") {
                document.getElementById('ventana2').src = "";
            }
            else {
                switch (_nomve) {
                    case "1":
                        document.getElementById('ventana2').src = "cuentas-ventana04.php?cuenta=cuenta&ncuenta=ncuenta";
                        break;
                    case "2":
                        if (document.getElementById('cuenta').value != '') {
                            document.getElementById('ventana2').src = "cuentas-ventana04.php?cuenta=cuentaF&ncuenta=nomCuentaF&limite=" + document.getElementById('cuenta').value;
                        }
                        else {
                            document.getElementById("bgventanamodal2").style.visibility = 'hidden';
                            despliegamodalm('visible', '2', 'Error, Se debe ingresar una cuenta inicial ');
                        }
                        break;
                }
            }
        }
        function agregarcuentas() {
            var val1 = document.getElementById('cuenta').value;
            var val2 = document.getElementById('cuentaF').value;
            if ((val1.trim() != '') && (val2.trim() != '')) {
                document.form2.agregadet.value = 1;
                document.form2.submit();
            }
            else {
                despliegamodalm('visible', '2', 'Error, Se debe ingresar toda la informaci�n de las cuentas para agregar ');
            }
        }
        function eliminar(variable) {
            document.form2.elimina.value = variable;
            despliegamodalm('visible', '4', 'Esta seguro de eliminar detalle', '2');
        }
    </script>
</head>

<body>
    <IFRAME src="alertas.php" name="alertas" id="alertas" style="display:none"></IFRAME>
    <span id="todastablas2"></span>
    <table>
        <tr>
            <script>barra_imagenes("para");</script><?php cuadro_titulos(); ?>
        </tr>
        <tr><?php menu_desplegable("para"); ?></tr>
    </table>
    <div class="bg-white group-btn p-1"><button type="button" onclick="window.location.href='inve-destinocompra.php'"
            class="btn btn-white btn-primary-hover d-flex justify-between align-items-center">
            <span>Nuevo</span>
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 -960 960 960">
                <path d="M440-440H200v-80h240v-240h80v240h240v80H520v240h-80v-240Z"></path>
            </svg>
        </button><button type="button" onclick="guardar()"
            class="btn btn-white btn-primary-hover d-flex justify-between align-items-center">
            <span>Guardar</span>
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 -960 960 960">
                <path
                    d="M840-680v480q0 33-23.5 56.5T760-120H200q-33 0-56.5-23.5T120-200v-560q0-33 23.5-56.5T200-840h480l160 160Zm-80 34L646-760H200v560h560v-446ZM480-240q50 0 85-35t35-85q0-50-35-85t-85-35q-50 0-85 35t-35 85q0 50 35 85t85 35ZM240-560h360v-160H240v160Zm-40-86v446-560 114Z">
                </path>
            </svg>
        </button><button type="button" onclick="location.href='inve-buscadestinocompra.php'"
            class="btn btn-white btn-primary-hover d-flex justify-between align-items-center">
            <span>Buscar</span>
            <svg class="fill-black group-hover:fill-white w-5 h-5" xmlns="http://www.w3.org/2000/svg"
                viewBox="0 -960 960 960">
                <path
                    d="M784-120 532-372q-30 24-69 38t-83 14q-109 0-184.5-75.5T120-580q0-109 75.5-184.5T380-840q109 0 184.5 75.5T640-580q0 44-14 83t-38 69l252 252-56 56ZM380-400q75 0 127.5-52.5T560-580q0-75-52.5-127.5T380-760q-75 0-127.5 52.5T200-580q0 75 52.5 127.5T380-400Z">
                </path>
            </svg>
        </button><button type="button" onclick="mypop=window.open('plan-agenda.php','','');mypop.focus()"
            class="btn btn-white btn-primary-hover d-flex justify-between align-items-center">
            <span class="group-hover:text-white">Agenda</span>
            <svg class="fill-black group-hover:fill-white w-5 h-5" xmlns="http://www.w3.org/2000/svg"
                viewBox="0 -960 960 960">
                <path
                    d="M560-564v-68q33-14 67.5-21t72.5-7q26 0 51 4t49 10v64q-24-9-48.5-13.5T700-600q-38 0-73 9.5T560-564Zm0 220v-68q33-14 67.5-21t72.5-7q26 0 51 4t49 10v64q-24-9-48.5-13.5T700-380q-38 0-73 9t-67 27Zm0-110v-68q33-14 67.5-21t72.5-7q26 0 51 4t49 10v64q-24-9-48.5-13.5T700-490q-38 0-73 9.5T560-454ZM260-320q47 0 91.5 10.5T440-278v-394q-41-24-87-36t-93-12q-36 0-71.5 7T120-692v396q35-12 69.5-18t70.5-6Zm260 42q44-21 88.5-31.5T700-320q36 0 70.5 6t69.5 18v-396q-33-14-68.5-21t-71.5-7q-47 0-93 12t-87 36v394Zm-40 118q-48-38-104-59t-116-21q-42 0-82.5 11T100-198q-21 11-40.5-1T40-234v-482q0-11 5.5-21T62-752q46-24 96-36t102-12q58 0 113.5 15T480-740q51-30 106.5-45T700-800q52 0 102 12t96 36q11 5 16.5 15t5.5 21v482q0 23-19.5 35t-40.5 1q-37-20-77.5-31T700-240q-60 0-116 21t-104 59ZM280-494Z">
                </path>
            </svg>
        </button><button type="button" onclick="mypop=window.open('para-principal.php','','');mypop.focus();"
            class="btn btn-white btn-primary-hover d-flex justify-between align-items-center">
            <span>Nueva ventana</span>
            <svg class="fill-black group-hover:fill-white w-5 h-5" xmlns="http://www.w3.org/2000/svg"
                viewBox="0 -960 960 960">
                <path
                    d="M320-120v-80h80v-80H160q-33 0-56.5-23.5T80-360v-400q0-33 23.5-56.5T160-840h640q33 0 56.5 23.5T880-760v400q0 33-23.5 56.5T800-280H560v80h80v80H320ZM160-360h640v-400H160v400Zm0 0v-400 400Z">
                </path>
            </svg>
        </button><button type="button"
            onclick="mypop=window.open('/financiero/inve-destinocompra.php','','');mypop.focus();"
            class="btn btn-white btn-primary-hover d-flex justify-between align-items-center">
            <span class="group-hover:text-white">Duplicar pantalla</span>
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 -960 960 960">
                <path
                    d="M640-160v-360H160v360h480Zm80-200v-80h80v-360H320v200h-80v-200q0-33 23.5-56.5T320-880h480q33 0 56.5 23.5T880-800v360q0 33-23.5 56.5T800-360h-80ZM160-80q-33 0-56.5-23.5T80-160v-360q0-33 23.5-56.5T160-600h480q33 0 56.5 23.5T720-520v360q0 33-23.5 56.5T640-80H160Zm400-603ZM400-340Z">
                </path>
            </svg>
        </button></div>
    <tr>
        <td colspan="3" class="tablaprin">
            <?php
            if ($_POST['oculto'] == "") {
                $_POST['vareliminadas'] = '';
                $_POST['cuenta1'] = array();
                $_POST['nombre1'] = array();
                $_POST['cuenta2'] = array();
                $_POST['nombre2'] = array();
                $_POST['fechadet'] = array();
                $sqlr = "SELECT MAX(RIGHT(codigo,2)) FROM almdestinocompra ORDER BY codigo Desc";
                $res = mysqli_query($linkbd, $sqlr);
                $row = mysqli_fetch_row($res);
                $_POST['numero'] = $row[0] + 1;
                if (strlen($_POST['numero']) == 1) {
                    $_POST['numero'] = '0' . $_POST['numero'];
                }
            }
            ?>
            <div id="bgventanamodalm" class="bgventanamodalm">
                <div id="ventanamodalm" class="ventanamodalm">
                    <IFRAME src="" name="ventanam" marginWidth=0 marginHeight=0 frameBorder=0 id="ventanam"
                        frameSpacing=0 style=" width:700px; height:130px; top:200; overflow:hidden;"></IFRAME>
                </div>
            </div>

            <div id="bgventanamodal2" class="bgventanamodalm2">
                <div id="ventanamodal2" class="bgventanamodalm2">
                    <IFRAME src="" name="buster" marginWidth=0 marginHeight=0 frameBorder=0 id="ventana2" frameSpacing=0
                        style="left:500px; width:900px; height:500px; top:200;"></IFRAME>
                </div>
            </div>

            <form name="form2" method="post" action="">
                <?php
                if ($_POST['bcnt'] == '1') {
                    $sqlNCA = "SELECT nombre, tipo, estado FROM cuentasnicsp WHERE cuenta = '" . $_POST['cuenta'] . "'";
                    $resNCA = mysqli_query($linkbd, $sqlNCA);
                    $nca = mysqli_fetch_row($resNCA);
                    if ($nca[0] != '') {
                        $_POST['ncuenta'] = $nca[0];
                    } else {
                        echo "<script>despliegamodalm('visible','2','Error, cuenta No " . $_POST['cuenta'] . " no existe ');</script>";
                        $_POST['cuenta'] = '';
                    }
                }
                if ($_POST['bcnt2'] == '1') {
                    if ($_POST['cuenta'] != '') {
                        $sqlNCA = "SELECT nombre, tipo, estado FROM cuentasnicsp WHERE cuenta = '" . $_POST['cuentaF'] . "'";
                        $resNCA = mysqli_query($linkbd, $sqlNCA);
                        $nca = mysqli_fetch_row($resNCA);
                        if ($nca[0] != '') {
                            $_POST['nomCuentaF'] = $nca[0];
                        } else {
                            echo "<script>despliegamodalm('visible','2','Error, cuenta No " . $_POST['cuentaF'] . " no existe ');</script>";
                            $_POST['cuentaF'] = '';
                        }
                    } else {
                        echo "<script>despliegamodalm('visible','2','Error, debe ingresar primero la cuenta inicial ');</script>";
                        $_POST['cuentaF'] = '';
                    }
                }
                ?>

                <table class="inicio" align="center">
                    <tr>
                        <td class="titulos" colspan="8">.: Crear Destino Compra</td>
                        <td class="cerrar" style="width:7%" onClick="location.href='para-principal.php'">Cerrar</td>
                    </tr>
                    <tr>
                        <td style="width:2.5cm" class="tamano01">C&oacute;digo:</td>
                        <td style="width:15%" valign="middle"><input type="text" id="numero" name="numero"
                                style="width:98%;height:30px;" onKeyPress="javascript:return solonumeros(event)"
                                onKeyUp="return tabular(event,this)" value="<?php echo $_POST['numero'] ?>" readonly />
                        </td>
                        <td style="width:2.5cm" class="tamano01">Nombre:</td>
                        <td><input type="text" id="nombre" name="nombre" onKeyUp="return tabular(event,this)"
                                value="<?php echo $_POST['nombre'] ?>" style="width:100%;height:30px;"
                                onClick="document.getElementById('acuerdo').focus();document.getElementById('acuerdo').select();"
                                autocomplete="off"></td>
                    </tr>
                </table>
                <input type="hidden" id="oculto" value="0" name="oculto">
                <table class="inicio">
                    <tr>
                        <td colspan="8" class="titulos2">Crear Detalle Destino Compra</td>
                    </tr>
                    <tr>
                        <td class="tamano01" style="width:2.5cm;">Cuenta de:</td>
                        <td style="width:10%;"><input class="colordobleclik" type="text" id="cuenta" name="cuenta"
                                style="width:100%; height: 30px;" onKeyPress="javascript:return solonumeros(event)"
                                onKeyUp="return tabular(event,this)" onChange="buscacta(event)"
                                onDblClick="despliegamodal2('visible','1');" value="<?php echo $_POST['cuenta'] ?>"
                                autocomplete="off" /></td>
                        <input type="hidden" name="bcnt" id="bcnt" value="0">
                        <td><input type="text" name="ncuenta" id="ncuenta" value="<?php echo $_POST['ncuenta'] ?>"
                                style="width:98%; height:30px;" readonly></td>
                        <td class="tamano01" style="width:2.5cm;">Cuenta hasta:</td>
                        <td style="width:10%;"><input class="colordobleclik" type="text" name="cuentaF" id="cuentaF"
                                value="<?php echo $_POST['cuentaF'] ?>" onKeyPress="javascript:return solonumeros(event)"
                                onKeyUp="return tabular(event,this)" onChange="buscacta2(event)"
                                onDblClick="despliegamodal2('visible','2');" style="width:100%; height:30px;"
                                autocomplete="off" /></td>
                        <input type="hidden" name="bcnt2" id="bcnt2" value="0">
                        <td><input type="text" name="nomCuentaF" id="nomCuentaF"
                                value="<?php echo $_POST['nomCuentaF'] ?>" style="width:98%; height:30px;" readonly />
                        </td>


                        <td style="padding-bottom:0px"><em class="botonflecha" onClick="agregarcuentas();">Agregar</em>
                        </td>
                    </tr>
                </table>
                <div class="subpantalla" style="height:48.5%; width:99.6%; overflow-x:hidden;">
                    <table class="inicio ancho">
                        <tr>
                            <td class="titulos" colspan="10">Destinos de compra creados</td>
                        </tr>
                        <tr class="titulos2">
                            <td style='width:20%;'>Cuenta de</td>
                            <td style='width:20%;'>Nombre Cuenta</td>
                            <td style='width:20%;'>Cuenta hasta</td>
                            <td style='width:20%;'>Nombre Cuental</td>
                            <td style='width:5%;'>Eliminar</td>
                        </tr>
                        <input type='hidden' name='elimina' id='elimina' />
                        <input type='hidden' name='vareliminadas' id='vareliminadas'
                            value="<?php echo $_POST['vareliminadas']; ?>" />
                        <?php
                        if ($_POST['oculto'] == '6') {
                            $posi = $_POST['elimina'];
                            if ($_POST['vareliminadas'] != '') {
                                $_POST['vareliminadas'] = $_POST['vareliminadas'] + "<->" + $_POST['iddetalle'][$posi];
                                echo "<script>document.getElementById('vareliminadas').value=document.getElementById('vareliminadas').value + '<->' + " . $_POST['iddetalle'][$posi] . ";</script>";
                            } else {
                                echo "<script>document.getElementById('vareliminadas').value=" . $_POST['iddetalle'][$posi] . ";</script>";
                            }

                            unset($_POST['iddetalle'][$posi]);
                            unset($_POST['cuenta1'][$posi]);
                            unset($_POST['nombre1'][$posi]);
                            unset($_POST['cuenta2'][$posi]);
                            unset($_POST['nombre2'][$posi]);
                            $_POST['iddetalle'] = array_values($_POST['iddetalle']);
                            $_POST['cuenta1'] = array_values($_POST['cuenta1']);
                            $_POST['nombre1'] = array_values($_POST['nombre1']);
                            $_POST['cuenta2'] = array_values($_POST['cuenta2']);
                            $_POST['nombre2'] = array_values($_POST['nombre2']);
                        }
                        if ($_POST['agregadet'] == '1') {
                            $_POST['iddetalle'][] = '';
                            $_POST['cuenta1'][] = $_POST['cuenta'];
                            $_POST['nombre1'][] = $_POST['ncuenta'];
                            $_POST['cuenta2'][] = $_POST['cuentaF'];
                            $_POST['nombre2'][] = $_POST['nomCuentaF'];
                            echo "
								<script>
									document.getElementById('cuenta').value = '';
									document.getElementById('cuentaF').value  = '';
									document.getElementById('ncuenta').value = '';
									document.getElementById('nomCuentaF').value  = '';
								</script>";
                        }
                        $iter = 'saludo1a';
                        $iter2 = 'saludo2';
                        $cdtll = count($_POST['cuenta1']);
                        for ($x = 0; $x < $cdtll; $x++) {
                            echo "
								<input type='hidden' name='iddetalle[]' value='" . $_POST['iddetalle'][$x] . "'/>
								<input type='hidden' name='cuenta1[]' value='" . $_POST['cuenta1'][$x] . "'/>
								<input type='hidden' name='nombre1[]' value='" . $_POST['nombre1'][$x] . "'/>
								<input type='hidden' name='cuenta2[]' value='" . $_POST['cuenta2'][$x] . "'/>
								<input type='hidden' name='nombre2[]' value='" . $_POST['nombre2'][$x] . "'/>
								<tr class='$iter'>
									<td>" . $_POST['cuenta1'][$x] . "</td>
									<td>" . $_POST['nombre1'][$x] . "</td>
									<td>" . $_POST['cuenta2'][$x] . "</td>
									<td>" . $_POST['nombre2'][$x] . "</td>
									<td><a href='#' onclick='eliminar($x)'><img src='imagenes/del.png'></a></td>
								</tr>";
                            $aux = $iter;
                            $iter = $iter2;
                            $iter2 = $aux;
                        }
                        ?>
                    </table>
                </div>
                <input type="hidden" name="agregadet" id="agregadet" value="0">
            </form>
            <?php
            if ($_POST['oculto'] == '2') {
                $sql = "INSERT INTO almdestinocompra (codigo,nombre,estado) VALUES ('" . $_POST['numero'] . "','" . $_POST['nombre'] . "','S')";
                mysqli_query($linkbd, $sql);
                $cdtll = count($_POST['cuenta1']);
                for ($x = 0; $x < $cdtll; $x++) {
                    if ($_POST['iddetalle'][$x] == '') {
                        $numid_det = selconsecutivo('almdestinocompra_det', 'id_det');
                        $sqlr = "INSERT INTO almdestinocompra_det (id_det,codigo,cuenta_inicial,cuenta_final,estado) VALUES ('$numid_det','" . $_POST['numero'] . "','" . $_POST['cuenta1'][$x] . "', '" . $_POST['cuenta2'][$x] . "', 'S')";
                        mysqli_query($linkbd, $sqlr);
                    }
                }
                $eliminadas = explode('<->', $_POST['vareliminadas']);
                $cdtll = count($eliminadas);
                for ($x = 0; $x < $cdtll; $x++) {
                    $sqlr = "DELETE FROM almdestinocompra_det WHERE id_det='" . $eliminadas[$x] . "'";
                    mysqli_query($linkbd, $sqlr);
                }
                echo "<script>despliegamodalm('visible','1','Se ha almacenado con exito');</script>";
            }
            ?>
</body>

</html>
