<?php
	header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=utf8");
	require "comun.inc";
	require "funciones.inc";
	require "conversor.php";
	session_start();
	$linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");
	cargarcodigopag($_GET['codpag'],$_SESSION["nivel"]);
	date_default_timezone_set("America/Bogota");
?>
<!DOCTYPE >
<html lang="es">
	<head>
		<meta charset="UTF-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="user-scalable=no">
		<title>:: IDEAL 10 - Tesorer&iacute;a</title>
		<link href="favicon.ico" rel="shortcut icon"/>
		<link href="css/css2.css" rel="stylesheet" type="text/css" />
		<link href="css/css3.css" rel="stylesheet" type="text/css" />
		<link href="css/css4.css" rel="stylesheet" type="text/css" />
		<script type="text/javascript" src="css/programas.js"></script>
		<script type="text/javascript" src="css/calendario.js"></script>
		<script type="text/javascript" src="JQuery/jquery-2.1.4.min.js"></script>
		<script type="text/javascript" src="JQuery/autoNumeric-master/autoNumeric-min.js"></script>
		<script>
			function validar(){document.form2.submit();}
			function buscater(e)
			{
				if (document.form2.tercero.value!="")
				{
					document.form2.bt.value='1';
					document.form2.submit();
				}
			}
			function agregardetalle()
			{
				if(document.form2.codingreso.value!="" &&  document.form2.valor.value>0  )
				{
					document.form2.agregadet.value=1;
					document.form2.submit();
				}
				else {alert("Falta informacion para poder Agregar");}
			}
			function eliminar(variable)
			{
				document.form2.elimina.value=variable;
				despliegamodalm('visible','4','Esta Seguro de Eliminar','1');
			}
			function guardar()
			{
				ingresos2=document.getElementsByName('dcoding[]');
				var validacion01=document.getElementById('concepto').value;
				var validacion02=document.getElementById('ntercero').value;
				if (document.form2.fecha.value!='' && ingresos2.length>0 && (validacion01.trim()!='') && (validacion02.trim()!=''))
				{despliegamodalm('visible','4','Esta Seguro de Guardar','2');}
				else
				{
					despliegamodalm('visible','2',"Falta información para poder guardar")
					document.form2.fecha.focus();
					document.form2.fecha.select();
				}
			}
			function buscater(e)
			{
				if (document.form2.tercero.value!="")
				{
					document.form2.bt.value='1';
					document.form2.submit();
				}
			}
			function buscaing(e)
			{
				if (document.form2.codingreso.value!="")
				{
					document.form2.bin.value='1';
					document.form2.submit();
				}
			}
			function despliegamodal2(_valor,_nvent)
			{
				document.getElementById("bgventanamodal2").style.visibility=_valor;
				if(_valor=="hidden"){document.getElementById('ventana2').src="";}
				else
				{
					if(_nvent=='1')
					{document.getElementById('ventana2').src="tercerosgral-ventana01.php?objeto=tercero&nobjeto=ntercero&nfoco=codingreso";}
					else
					{document.getElementById('ventana2').src="ingresos-ventana01.php?ti=I&modulo=4";}
				}
			}

			function despliegaModalFuentes(_valor) {

				var codigo=document.getElementById('codingreso').value;

				if (codigo != "") {

					document.getElementById("bgventanamodal2").style.visibility=_valor;
					if(_valor=="hidden"){document.getElementById('ventana2').src="";}
					else
					{


						if (codigo != "") {
							document.getElementById('ventana2').src="teso-ventana-fuentes.php?codigo="+codigo;
						}
						else {
							alert("Seleccione primero el código de ingreso");
						}
					}
				}
				else {
					alert("Seleccione primero el código de ingreso");
				}
			}

			function despliegamodalm(_valor,_tip,mensa,pregunta)
			{
				document.getElementById("bgventanamodalm").style.visibility=_valor;
				if(_valor=="hidden")
				{
					document.getElementById('ventanam').src="";
					switch(document.getElementById('valfocus').value)
					{
						case "1":	document.getElementById('valfocus').value='';
									document.getElementById('tercero').focus();
									document.getElementById('tercero').select();
									break;
						case "2":	document.getElementById('valfocus').value='';
									document.getElementById('codingreso').focus();
									document.getElementById('codingreso').select();
									break;
					}
				}
				else
				{
					switch(_tip)
					{
						case "1":
							document.getElementById('ventanam').src="ventana-mensaje1.php?titulos="+mensa;break;
						case "2":
							document.getElementById('ventanam').src="ventana-mensaje3.php?titulos="+mensa;break;
						case "3":
							document.getElementById('ventanam').src="ventana-mensaje2.php?titulos="+mensa;break;
						case "4":
							document.getElementById('ventanam').src="ventana-consulta1.php?titulos="+mensa+"&idresp="+pregunta;break;
					}
				}
			}
			function funcionmensaje()
			{
				var numdocar=document.getElementById('ncomp').value;
				document.location.href = "teso-editarecaudotransferencialiquidar.php?idrecaudo="+numdocar;
			}
			function respuestaconsulta(pregunta)
			{
				switch(pregunta)
				{
					case "1":	document.form2.oculto.value='3';
								document.form2.submit();
								break;
					case "2":	document.form2.oculto.value='2';
								document.form2.submit();
								break;
				}
			}

			function cambiarVigencia()
			{
				let fecha = document.getElementById('fc_1198971545').value;
				let vigencia = fecha.substring(6,10);
				document.getElementById('vigencia').value = vigencia;
			}

			jQuery(function($){ $('#valorvl').autoNumeric('init');});
		</script>
	</head>
	<body>
		<IFRAME src="alertas.php" name="alertas" id="alertas" style="display:none"></IFRAME>
		<span id="todastablas2"></span>
		<table>
			<tr><script>barra_imagenes("teso");</script><?php cuadro_titulos();?></tr>
			<tr><?php menu_desplegable("teso");?></tr>
			<tr>
				<td colspan="3" class="cinta">
					<a onClick="location.href='teso-recaudotransferencialiquidar.php'" class="mgbt"><img src="imagenes/add.png" title="Nuevo"/></a>
					<a onClick="guardar()" class="mgbt"><img src="imagenes/guarda.png" title="Guardar" /></a>
					<a onClick="location.href='teso-buscarecaudotransferencialiquidar.php'" class="mgbt"><img src="imagenes/busca.png" title="Buscar" /></a>
					<a onClick="mypop=window.open('plan-agenda.php','','');mypop.focus()" class="mgbt"><img src="imagenes/agenda1.png" title="Agenda" /></a>
					<a onClick="<?php echo paginasnuevas("teso");?>" class="mgbt"><img src="imagenes/nv.png" title="Nueva ventana"></a>
					<a onClick="mypop=window.open('<?php echo $url2; ?>','','');mypop.focus();" class="tooltip bottom mgbt"><img src="imagenes/duplicar_pantalla.png" title="Duplicar pesta&ntilde;a"></a>
					<a class="mgbt1"><img src="imagenes/printd.png" style="width:29px;height:25px;"/></a>
				</td>
			</tr>
		</table>
		<div id="bgventanamodalm" class="bgventanamodalm">
			<div id="ventanamodalm" class="ventanamodalm">
				<IFRAME src="" name="ventanam" marginWidth=0 marginHeight=0 frameBorder=0 id="ventanam" frameSpacing=0 style=" width:700px; height:130px; top:200; overflow:hidden;">
				</IFRAME>
			</div>
		</div>
		<form name="form2" method="post" action="">
			<input type="hidden" name="valfocus" id="valfocus" value=""/>
			<?php
				//echo "<script>cambiarVigencia();</script>";
				if(!$_POST['oculto'])
				{
					$check1="checked";
					$sqlr = "SELECT cuentacaja FROM tesoparametros";
					$res = mysqli_query($linkbd, $sqlr);
					$row = mysqli_fetch_row($res);

					$sqlr = "select max(id_recaudo) from tesorecaudotransferencialiquidar ";
					$res = mysqli_query($linkbd, $sqlr);
					$consec = 0;
					$r = mysqli_fetch_row($res);
					$consec = $r[0];
					$consec+=1;
					$_POST['idcomp'] = $consec;

					$fec = date("d/m/Y");
					$_POST['fecha'] = $fec;
					$_POST['vigencia'] = date('Y');

					$_POST['valor']=0;
				}

				//*** ingreso
				if($_POST['bin'] == '1')
				{
					$sql = "SELECT * FROM tesoingresos WHERE codigo = '$_POST[codingreso]' AND estado='S' ORDER BY codigo";
					$row = mysqli_fetch_row(mysqli_query($linkbd, $sql));

					if($row[0]!='') {
						$_POST['ningreso']=$row[1];
					if ($row[6] == 'N') {
						$_POST['causacion'] = 2;
					}
					else {
						$_POST['causacion'] = 1;
					}
					}
					else {
						$_POST['ningreso']="";
						$_POST['codingreso']="";
					echo"
						<script>
							document.getElementById('valfocus').value='2';
							despliegamodalm('visible','2','Codigo Ingresos Incorrecto');
						</script>";
					}
					$_POST['conFuente'] = ($row[4] == '') ? true : false;
				}
			?>
			<table class="inicio" style="width:99.6%">
				<tr >
					<td class="titulos" colspan="9">Liquidar Recaudos Transferencias</td>
					<td class="cerrar" style="width:7%;"><a onClick="location.href='teso-principal.php'">&nbsp;Cerrar</a></td>
				</tr>
				<tr>
					<td class="saludo1" style="width:10%;">Numero Liquidacion:</td>
					<td style="width:15%;">
						<input type="text" name="idcomp" id="idcomp" value="<?php echo $_POST['idcomp']?>" onKeyUp="return tabular(event,this)" style="width:80%; text-align:center" readonly/>
					</td>
					<td class="saludo1" style="width:10%;">Fecha:</td>
					<td style="width:10%;">
						<input type="text" name="fecha"  value="<?php echo $_POST['fecha']?>" onKeyUp="return tabular(event,this)" id="fc_1198971545" title="DD/MM/YYYY" onDblClick="displayCalendarFor('fc_1198971545');" class="colordobleclik" autocomplete="off" onChange="" readonly>
					</td>
					<td class="saludo1" style="width:10%;">Medio de pago: </td>
					<td style="width:10%;">
						<select name="medioDePago" id="medioDePago" onKeyUp="return tabular(event,this)" style="width:80%">
							<option value="1" <?php if(($_POST['medioDePago']=='1')) echo "SELECTED"; ?>>Con SF</option>
							<option value="2" <?php if($_POST['medioDePago']=='2') echo "SELECTED"; ?>>Sin SF</option>
						</select>
					</td>
					<td  class="saludo1" style="width:10%;">Vigencia:</td>
					<td style="width:10%;">
						<input type="text" id="vigencia" name="vigencia" value="<?php echo $_POST['vigencia']?>" style="width:80%;" readonly/>
					</td>
					<td class="saludo1" style="width:10%;">Causacion Contable:</td>
					<td style="width:4%;">
						<select name="causacion" id="causacion" onKeyUp="return tabular(event,this)"  >
							<option value="1" <?php if($_POST['causacion']=='1') echo "SELECTED"; ?>>Si</option>
							<option value="2" <?php if($_POST['causacion']=='2') echo "SELECTED"; ?>>No</option>
						</select>
					</td>
					<td></td>
				</tr>
				<tr>
					<td  class="saludo1">Concepto Recaudo:</td>
					<td colspan="9">
						<textarea name="concepto" id="concepto" style="width:100%; form-sizing: content; max-height: 100px; min-height: 20px; resize: vertical; " class="estilos-scroll"><?php echo $_POST['concepto']?></textarea>
					</td>
				</tr>
				<tr>
					<td  class="saludo1">NIT: </td>
					<td >
						<input type="text" name="tercero" id="tercero" value="<?php echo $_POST['tercero']?>" onKeyUp="return tabular(event,this)" onBlur="buscater(event)" style="width:80%;"/>&nbsp;<a onClick="despliegamodal2('visible','1');" title="Listado Contribuyentes"><img src="imagenes/find02.png" style="width:20px;cursor:pointer;"/></a>
					</td>
					<td class="saludo1">Contribuyente:</td>
					<td colspan="7">
						<input type="text" id="ntercero" name="ntercero" value="<?php echo $_POST['ntercero']?>" onKeyUp="return tabular(event,this)" style="width:100%;" readonly>
						<input type="hidden" value="0" name="bt">
						<input type="hidden" id="cb" name="cb" value="<?php echo $_POST['cb']?>" >
						<input type="hidden" id="ct" name="ct" value="<?php echo $_POST['ct']?>" >
						<input type="hidden" name="oculto" id="oculto" value="1" >
					</td>
				</tr>
				<tr>
					<td style="width:95%;" class="titulos" colspan="10">Determinaci&oacute;n de c&oacute;digos de ingresos</td>
				</tr>
				<tr>
					<td class="saludo1">Ingreso:</td>
					<td>
						<input type="text" id="codingreso" name="codingreso" value="<?php echo $_POST['codingreso']?>"  onKeyUp="return tabular(event,this)" onBlur="buscaing(event)" style="width:80%;">&nbsp;<a onClick="despliegamodal2('visible','2');" title="Listado de Ingresos"><img src="imagenes/find02.png" style="width:20px;cursor:pointer;"/></a>
						<input type="hidden" value="0" name="bin">
					</td>
					<td colspan="8">
						<input type="text" name="ningreso" id="ningreso" value="<?php echo $_POST['ningreso']?>" style="width:100%;" readonly>
					</td>
				</tr>
				<tr>
					<?php
					if($_POST['conFuente']){
						?>
						<td class="saludo1">Fuente:</td>
						<td>
							<input type="text" id="fuente" name="fuente" value="<?php echo $_POST['fuente'] ?>" style="width:80%;" readonly>
							<a onClick="despliegaModalFuentes('visible')" title="Listado de fuentes"><img src="imagenes/find02.png" style="width:20px;cursor:pointer;"/></a>
						</td>
						<td colspan="8">
							<input type="text" name="nfuente" id="nfuente" value="<?php echo $_POST['nfuente']?>" style="width:100%;" readonly>
						</td>
						<?php
					}
					?>
					<input type="hidden" name = 'conFuente' value="<?php echo $_POST['conFuente'] ?>">
				</tr>
				<tr>
					<td class="saludo1">Centro Costo:</td>
					<td>
						<select name="cc" id="cc" onChange="validar()" onKeyUp="return tabular(event,this)" style="width:90%">
							<?php
								$sqlr="select *from centrocosto where estado='S'";
								$res=mysqli_query($linkbd, $sqlr);
								while ($row =mysqli_fetch_row($res))
								{
									if("$row[0]"==$_POST['cc']){echo "<option value='$row[0]' SELECTED>$row[0] - $row[1]</option>";}
									else {echo "<option value='$row[0]'>$row[0] - $row[1]</option>";}
								}
							?>
						</select>
					</td>
					<td class="saludo1" >Valor:</td>
					<td>
						<input type="hidden" id="valor" name="valor" value="<?php echo $_POST['valor']?>"/>
						<input type="text" name="valorvl" id="valorvl" data-a-dec="<?php echo $_SESSION["spdecimal"];?>" data-a-sep="<?php echo $_SESSION["spmillares"];?>" data-v-min='0' onKeyUp="sinpuntitos2('valor','valorvl');return tabular(event,this);" value="<?php echo $_POST['valorvl']; ?>" style='text-align:right; width:80%;' style=""/>
						<input type="hidden" name="agregadet"/>
					</td>
					<td style="padding-bottom:6px; padding-top:6px">
						<em class="botonflecha" onClick="agregardetalle();">Agregar</em>
					</td>
				</tr>
			</table>
			<?php
			//***** busca tercero
				if($_POST['bt'] == '1')
				{
					$nresul = buscatercero($_POST['tercero']);
					if($nresul!='')
					{
						echo"
						<script>
							document.getElementById('ntercero').value='$nresul';
						</script>";
					}
					else
					{
						echo"
						<script>
							document.getElementById('ntercero').value='';
							document.getElementById('valfocus').value='1';
							despliegamodalm('visible','2','Tercero Incorrecto o no Existe.');
						</script>";
					}
				}
			?>
			<div class="subpantalla estilos-scroll"  style="height:47.3%; width:99.4%; overflow-x:hidden; resize: vertical;" id="divdet">
				<table class="inicio">
					<tr><td colspan="6" class="titulos">Detalle Recaudos Transferencia</td></tr>
					<tr>
						<td class="titulos2" style="width:10%;">Codigo</td>
						<td class="titulos2">Ingreso</td>
						<td class="titulos2" style="width:10%;">Fuente</td>
						<td class="titulos2" style="width:10%;">Centro costo</td>
						<td class="titulos2" style="width:15%;">Valor</td>
						<td class="titulos2" style="width:5%;">Eliminar</td>
					</tr>
					<input type='hidden' name='elimina' id='elimina'>
					<?php
						if ($_POST['oculto'] == '3')
						{
							$posi=$_POST['elimina'];
							unset($_POST['dcoding'][$posi]);
							unset($_POST['dncoding'][$posi]);
							unset($_POST['dfuente'][$posi]);
							unset($_POST['dvalores'][$posi]);
							unset($_POST['dcc'][$posi]);
							$_POST['dcoding'] = array_values($_POST['dcoding']);
							$_POST['dncoding'] = array_values($_POST['dncoding']);
							$_POST['dfuente'] = array_values($_POST['dfuente']);
							$_POST['dvalores'] = array_values($_POST['dvalores']);
							$_POST['dcc'] = array_values($_POST['dcc']);
						}
						if ($_POST['agregadet'] == '1')
						{
							preg_match("/([0-9]{2})\/([0-9]{2})\/([0-9]{4})/", $_POST['fecha'], $fecha);
							$fechaf = $fecha[3]."-".$fecha[2]."-".$fecha[1];

							$sqlri = "SELECT concepto FROM tesoingresos_det WHERE codigo='".$_POST['codingreso']."' AND vigencia=(SELECT MAX(vigencia) FROM tesoingresos_det WHERE codigo = '".$_POST['codingreso']."' AND estado='S')";
							$resi = mysqli_query($linkbd,$sqlri);
							$rowi = mysqli_fetch_row($resi);

							$sq = "SELECT fechainicial FROM conceptoscontables_det WHERE codigo='".$rowi[0]."' AND modulo='4' and tipo='C' AND fechainicial<'$fechaf' AND cc='".$_POST['cc']."' AND cuenta!='' order by fechainicial asc";
							$re = mysqli_query($linkbd,$sq);
							while($ro = mysqli_fetch_assoc($re))
							{
								$_POST['fechacausa']=$ro["fechainicial"];
							}
							$sqlrc = "SELECT * FROM conceptoscontables_det WHERE estado='S' AND modulo='4' AND codigo='".$rowi[0]."' AND tipo='C' AND cc='".$_POST['cc']."' AND fechainicial='".$_POST['fechacausa']."'";
							$resc = mysqli_query($linkbd,$sqlrc);
							$rowc = mysqli_fetch_row($resc);

							if($rowc[4]!='') {
								$_POST['dcoding'][] = $_POST['codingreso'];
								$_POST['dncoding'][] = $_POST['ningreso'];
								$_POST['dfuente'][] = $_POST['fuente'];
								$_POST['dvalores'][] = $_POST['valor'];
								$_POST['dcc'][] = $_POST['cc'];
								$_POST['agregadet'] = 0;
								echo"
								<script>
									document.form2.codingreso.value = '';
									document.form2.ningreso.value = '';
									document.form2.fuente.value = '';
									document.form2.nfuente.value = '';
									document.form2.valor.value = '';
									document.form2.valorvl.value = '';
									document.form2.codingreso.select();
									document.form2.codingreso.focus();
								</script>";
							}
							else {
								echo "<script>despliegamodalm('visible','2','No hay parametrizacion contable para este codigo de ingreso y centro de costo');</script>";
								$_POST['agregadet']=0;
							}
						}
						$_POST['totalc'] = 0;
						$co="saludo1a";
						$co2="saludo2";
						if(isset($_POST['dcoding'])){
							for ($x = 0; $x<count($_POST['dcoding']); $x++)
							{
								echo "
								<input type='hidden' name='dcoding[]' value='".$_POST['dcoding'][$x]."'/>
								<input type='hidden' name='dncoding[]' value='".$_POST['dncoding'][$x]."'/>
								<input type='hidden' name='dfuente[]' value='".$_POST['dfuente'][$x]."'/>
								<input type='hidden' name='dcc[]' value='".$_POST['dcc'][$x]."'/>
								<input type='hidden' name='dvalores[]' value='".$_POST['dvalores'][$x]."'/>
								<tr class='$co' onMouseOver=\"anterior=this.style.backgroundColor;this.style.backgroundColor='#40b3ff';\" onMouseOut=\"this.style.backgroundColor=anterior\">
									<td>".$_POST['dcoding'][$x]."</td>
									<td>".$_POST['dncoding'][$x]."</td>
									<td>".$_POST['dfuente'][$x]."</td>
									<td>".$_POST['dcc'][$x]."</td>
									<td style='text-align:right;'>$ ".number_format($_POST['dvalores'][$x],2)."</td>
									<td style='text-align:center;'><a onclick='eliminar($x)'><img src='imagenes/del.png' style='cursor:pointer;'></a></td>
								</tr>";
								$_POST['totalc'] = $_POST['totalc']+$_POST['dvalores'][$x];
								$_POST['totalcf']=number_format($_POST['totalc'],2);
								$totalg=number_format($_POST['totalc'],2,'.','');
								$aux = $co;
								$co = $co2;
								$co2 = $aux;
							}
						}

						if ($_POST['totalc'] != '' && $_POST['totalc'] != 0){$_POST['letras'] = convertirdecimal($totalg,'.');}
						else{$_POST['letras'] = ''; $_POST['totalcf'] = 0;}
						echo "
						<input type='hidden' name='totalcf' value='$_POST[totalcf]'>
						<input type='hidden' name='totalc' value='$_POST[totalc]'>
						<input type='hidden' name='letras' value='$_POST[letras]'>
						<tr class='$co' style='text-align:right;'>
							<td colspan='4'>Total:</td>
							<td>$ $_POST[totalcf]</td>
							<td></td>
						</tr>
						<tr class='titulos2'>
							<td >Son:</td>
							<td colspan='5' >$_POST[letras]</td>
						</tr>";
					?>
				</table>
			</div>
			<?php
				if($_POST['oculto'] == '2')
				{
					preg_match("/([0-9]{2})\/([0-9]{2})\/([0-9]{4})/", $_POST['fecha'], $fecha);
					$fechaf = $fecha[3]."-".$fecha[2]."-".$fecha[1];
					$_POST["vigencia"] = $fecha[3];

					$bloq = bloqueos($_SESSION['cedulausu'], $fechaf);
					if($bloq >= 1)
					{
						$consec = 0;

						$sqlr="insert into tesorecaudotransferencialiquidar (idcomp,fecha,vigencia,banco,ncuentaban,concepto,tercero,cc,valortotal, estado,medio_pago) values('','$fechaf','".$_POST["vigencia"]."','$_POST[ter]','$_POST[cb]','".strtoupper($_POST['concepto'])."','$_POST[tercero]','$_POST[cc]','$_POST[totalc]','S','$_POST[medioDePago]')";
						mysqli_query($linkbd, $sqlr);
						$idrec=mysqli_insert_id($linkbd);

						//*********************CREACION DEL COMPROBANTE CONTABLE ***************************
						//***busca el consecutivo del comprobante contable

						$consec = $idrec;
						//***cabecera comprobante
						if($_POST['causacion'] == '2'){$_POST['concepto'] = "ESTE DOCUMENTO NO REQUIERE CAUSACION CONTABLE - ".$_POST['concepto'];}

						$sqlr="insert into comprobante_cab (numerotipo,tipo_comp,fecha,concepto,total,total_debito,total_credito,diferencia,estado) values ($consec,28,'$fechaf','".strtoupper($_POST['concepto'])."',0,$_POST[totalc],$_POST[totalc],0,'1')";
						mysqli_query($linkbd, $sqlr);
						$idcomp=mysqli_insert_id($linkbd);
						echo "<input type='hidden' name='ncomp' id='ncomp' value='$idrec'>";
						if($_POST['causacion']!='2')
						{
							//******************* DETALLE DEL COMPROBANTE CONTABLE *********************
							for($x = 0; $x < count($_POST['dcoding']); $x++)
							{
								//***** BUSQUEDA INGRESO ********
								$sqlri = "SELECT * FROM tesoingresos_det WHERE codigo='".$_POST['dcoding'][$x]."' AND vigencia=(SELECT MAX(vigencia) FROM tesoingresos_det WHERE codigo = '".$_POST['dcoding'][$x]."') ";
								$resi = mysqli_query($linkbd, $sqlri);
								while($rowi = mysqli_fetch_row($resi))
								{
									//**** busqueda concepto contable*****
									$sq="select fechainicial from conceptoscontables_det where codigo='$rowi[2]' and modulo='4' and tipo='C' and fechainicial<'$fechaf' and cuenta!='' AND cc='".$_POST['dcc'][$x]."' order by fechainicial asc";
									$re=mysqli_query($linkbd, $sq);
									while($ro = mysqli_fetch_assoc($re))
									{
										$_POST['fechacausa'] = $ro["fechainicial"];
									}
									$sqlrc = "Select * from conceptoscontables_det where estado='S' and modulo='4' AND codigo='$rowi[2]' and tipo='C' and fechainicial='".$_POST['fechacausa']."' AND cc='".$_POST['dcc'][$x]."'";
									$resc = mysqli_query($linkbd, $sqlrc);
									while($rowc = mysqli_fetch_row($resc))
									{
										$porce = $rowi[5];
										if($_POST['dcc'][$x] == $rowc[5])
										{
											if($rowc[3]=='N')
											{
												if($rowc[6] == 'S')
												{
													$valordeb = $_POST['dvalores'][$x]*($porce/100);
													$valorcred = 0;
												}
												if($rowc[7] == 'S')
												{
													$valorcred = $_POST['dvalores'][$x]*($porce/100);
													$valordeb = 0;
												}
												$sqlr = "insert into comprobante_det (id_comp,cuenta,tercero,centrocosto,detalle,cheque,valdebito, valcredito,estado,vigencia) values ('28 $consec','".$rowc[4]."','".$_POST['tercero']."','".$_POST['dcc'][$x]."','Recaudo Transferencia".strtoupper($_POST['dncoding'][$x])."','',".$valordeb.",".$valorcred.",'1','".$_POST['vigencia']."')";
												mysqli_query($linkbd, $sqlr);
											}
										}
									}
								}
							}
						}
						//************ insercion de cabecera recaudos ************

						//************** insercion de consignaciones **************
						for($x = 0; $x < count($_POST['dcoding']); $x++)
						{
							$sqlr="insert into tesorecaudotransferencialiquidar_det (id_recaudo,ingreso,valor,estado,fuente,cc) values($idrec,'".$_POST['dcoding'][$x]."',".$_POST['dvalores'][$x].",'S','".$_POST['dfuente'][$x]."','".$_POST['dcc'][$x]."')";
							if (!mysqli_query($linkbd, $sqlr))
							{
								//mysqli_query($linkbd, $sqlr);
								$e = mysqli_error($linkbd);
								echo"<script>despliegamodalm('visible','2','No se pudo ejecutar la petición: $e');</script>";
							}
							else
							{
								echo"<script>despliegamodalm('visible','1','Se ha almacenado el Recaudo con Exito');</script>";
							}
						}

					}
					else
					{
						echo "<script>despliegamodalm('visible','2',' La fecha del documento es menor a la fecha de bloqueo.');</script>";
					}
				}
			?>
			<div id="bgventanamodal2">
				<div id="ventanamodal2">
					<IFRAME  src="" name="buster" marginWidth=0 marginHeight=0 frameBorder=0 id="ventana2" frameSpacing=0 style="left:500px; width:900px; height:500px; top:200;">
					</IFRAME>
				</div>
			</div>
			<script>cambiarVigencia();</script>
		</form>
	</body>
</html>
