<?php

    header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=utf8");
	require 'comun.inc';
	require 'funciones.inc';
	session_start();
    if(empty($_SESSION)){
        header("location: index.php");
    }
	$linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");
	cargarcodigopag($_GET['codpag'],$_SESSION["nivel"]);
	date_default_timezone_set("America/Bogota");
?>
<!DOCTYPE >
<html lang=es>
	<head>
    <meta charset="utf-8"/>
		<meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1"/>
		<meta name="viewport" content="user-scalable=no">
		<title>:: IDEAL 10 - Informes</title>
		<link href="favicon.ico" rel="shortcut icon"/>
		<link href="css/css2z.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
		<link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
		<link href="css/css4.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
        <link href="css/tabs.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css"/>
        <link href="css/style.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css"/>
        <link rel="stylesheet" href="sweetalert2/dist/sweetalert2.min.css">
        <script type="text/javascript" src="css/programas.js"></script>
        <script src="sweetalert2/dist/sweetalert2.min.js"></script>
		<script src="vue/vue.min.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script src="css/programas.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script src="css/calendario.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
	</head>
	<body>
		<header>
			<table>
				<tr><script>barra_imagenes("info");</script><?php cuadro_titulos();?></tr>
			</table>
		</header>

		<form name="form2" method="post" action="">
			<section id="myapp" v-cloak >
                <div id="cargando" v-show="isLoading" class="loading" style="z-index: 10000;">
                    <span>Cargando...</span>
                </div>
				<nav>
					<table>
						<tr><?php menu_desplegable("info");?></tr>
                        <tr>
							<td colspan="3" class="cinta">
                                <img src="imagenes/add.png"  class="mgbt" @click="window.location.reload()" title="Nuevo">
								<img src="imagenes/guarda.png"   title="Guardar" @click="save" class="mgbt">
								<img src="imagenes/busca.png" @click="window.location.href='cont-columnasExogena2276Buscar.php'"   class="mgbt" title="Buscar">
                                <a @click="mypop=window.open('plan-agenda.php','','');mypop.focus()" class="mgbt"><img src="imagenes/agenda1.png" title="Agenda" /></a>
								<img src="imagenes/nv.png" @click="mypop=window.open('info-principal.php','',''); mypop.focus();" class="mgbt" title="Nueva Ventana">
                                <a href="cont-columnasExogena2276Buscar.php"  class="mgbt"><img src="imagenes/iratras.png" title="Atr&aacute;s"></a>
                            </td>
						</tr>
					</table>
				</nav>
				<article>
                    <div class="inicio">
                        <div>
                            <h2 class="titulos m-0">.: Crear columnas exogena 2276</h2>
                            <div class="d-flex">
                                <div class="form-control w-25">
                                    <label class="form-label">.: Consecutivo:</label>
                                    <div class="d-flex">
                                        <button v-show="txtConsecutivo > 1" type="button" class="btn btn-primary" @click="editItem(--txtConsecutivo)"><</button>
                                        <input type="text"  style="text-align:center;" v-model="txtConsecutivo" @change="editItem(txtConsecutivo)">
                                        <button v-show="txtConsecutivo < txtMax" type="button" class="btn btn-primary" @click="editItem(++txtConsecutivo)">></button>
                                    </div>
                                </div>
                                <div class="form-control w-25">
                                    <label class="form-label">.: Código <span class="text-danger fw-bolder">*</span></label>
                                    <input type="number" v-model="txtCodigo">
                                </div>
                                <div class="form-control w-50">
                                    <label class="form-label">.: Nombre <span class="text-danger fw-bolder">*</span></label>
                                    <input type="text" v-model="txtNombre">
                                </div>
                            </div>
                        </div>
                        <div>
                            <h2 class="titulos m-0">.: Asignar variables de pago</h2>
                            <div class="d-flex">
                                <div class="form-control w-50">
                                    <label class="form-label" for="">.: Variable de pago:</label>
                                    <div class="d-flex">
                                        <input v-model="objVariable.codigo" type="text" @change="search('cod_cuenta')"  @dblclick="isModal=true" class="w-25 colordobleclik">
                                        <input v-model="objVariable.nombre" type="text" disabled readony>
                                        <button type="button" class="btn btn-primary" @click="addItem">Agregar</button>
                                    </div>
                                </div>
                            </div>
                            <div class="overflow-auto max-vh-25 overflow-x-hidden p-2" >
                                <table class="table fw-normal">
                                    <thead>
                                        <tr>
                                            <th>Código</th>
                                            <th>Nombre</th>
                                            <th>Concepto</th>
                                            <th>Opciones</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr v-for="(data,index) in arrVariablesDetalle" :key="index">
                                            <td>{{ data.codigo}}</td>
                                            <td>{{ data.nombre}}</td>
                                            <td>{{ data.concepto}}</td>
                                            <td>
                                                <button type="button" class="btn btn-danger m-1" @click="delItem(data)">Eliminar</button>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
				</article>
                <!--MODALES-->
                <div v-show="isModal">
                    <transition name="modal">
                        <div class="modal-mask">
                            <div class="modal-wrapper">
                                <div class="modal-container">
                                    <table class="inicio ancho">
                                        <tr>
                                            <td class="titulos" colspan="2" >.: Buscar variables de pago</td>
                                            <td class="cerrar" style="width:7%" @click="isModal = false">Cerrar</td>
                                        </tr>
                                    </table>
                                    <div class="bg-white">
                                        <div class="form-control m-0 p-2 w-inherit">
                                            <input type="search" placeholder="Buscar" @keyup="search('modal')" v-model="txtSearch">
                                        </div>
                                        <p class="fw-bolder m-0 p-2">Resultados de búsqueda: {{txtResults}}</p>
                                        <div class="overflow-auto max-vh-50 overflow-x-hidden p-2">
                                            <table class="table table-hover fw-normal">
                                                <thead>
                                                    <tr>
                                                        <th>Código</th>
                                                        <th>Nombre</th>
                                                        <th>Concepto</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr v-for="(data,index) in arrVariablesCopy" @dblclick="selectItem(data)" :key="index">
                                                        <td>{{data.codigo}}</td>
                                                        <td>{{data.nombre}}</td>
                                                        <td>{{data.concepto}}</td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </transition>
                </div>
			</section>
		</form>

		<script src="node_modules/read-excel-file/bundle/read-excel-file.min.js"></script>
		<script src="node_modules/xlsx/dist/xlsx.full.min.js"></script>
        <script src="Librerias/vue/vue.min.js"></script>
		<script src="Librerias/vue/axios.min.js"></script>
		<script type="module" src="contabilidad_vue/exogena_columnas_2276/editar/cont-columnasEditar.js?<?= date('d_m_Y_h_i_s');?>"></script>

	</body>
</html>
