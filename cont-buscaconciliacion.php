<?php
	header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=utf8");
	ini_set('max_execution_time',3600);

	require "comun.inc";
	require "funciones.inc";
	require "conversor.php";
	require "validaciones.inc";

	$linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");

	session_start();
	date_default_timezone_set("America/Bogota");
?>
<script>
	function direccionaCuentaGastos(row){
		var cell = row.getElementsByTagName("td")[1];
		var id = cell.innerHTML;
		var fec = document.form2.fechai.value.split("/");
		var fec1 = document.form2.fechaf.value.split("/");
		if(fec==''){
			alert("Falta digitar las fechas");
		}else{
			let fechai = fec[2]+"-"+fec[1]+"-"+fec[0];
			let fechaf = fec1[2]+"-"+fec1[1]+"-"+fec1[0];
			window.open("cont-conciliacionNuevo.php?cod="+id+"&fec="+fechai+"&fec1="+fechaf);
		}
	}
</script>
<!DOCTYPE >
<html lang="es">
	<head>
		<meta charset="utf-8"/>
		<meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1"/>
		<meta name="viewport" content="user-scalable=no">
		<title>:: IDEAL 10 - Contabilidad</title>
		<link href="favicon.ico" rel="shortcut icon"/>
		<link href="css/css2.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<link href="css/css4.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
        <link href="css/style.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
		<link href="sweetalert2/dist/sweetalert2.min.css" rel="stylesheet">
		<script src="sweetalert2/dist/sweetalert2.min.js"></script>
		<script type="text/javascript" src="jquery-1.11.0.min.js"></script>
		<script type="text/javascript" src="css/programas.js"></script>
		<script type="text/javascript" src="css/calendario.js"></script>
		<script>
			$(window).load(function () {
				$('#cargando').hide();
			});
			function excell(){
				document.form2.action = "cont-conciliacionexcel.php";
				document.form2.target = "_BLANK";
				document.form2.submit();
				document.form2.action = "";
				document.form2.target = "";
			}
			function pdf(){
				document.form2.action = "pdfreporteconciliacion.php";
				document.form2.target = "_BLANK";
				document.form2.submit();
				document.form2.action = "";
				document.form2.target = "";
			}
			function buscar(){
				document.form2.oculto.value = "2";
				document.form2.submit();
			}
			function despliegamodal2(_valor,_tipo,_saldo,_salcal,_difer){
				document.getElementById("bgventanamodal2").style.visibility=_valor;
				if(_valor=="hidden"){
					document.getElementById('ventana2').src="";
				}else {
					let fecha1 = document.getElementById('fc_1198971545').value;
					let fecha2 = document.getElementById('fc_1198971546').value;
					document.getElementById('ventana2').src="cont-archivos_extractos.php?cuenta=" + _tipo + "&fecha1=" + fecha1 + "&fecha2=" + fecha2 + "&saldoex=" + _saldo + "&saldocal=" + _salcal + "&difer=" + _difer;
				}
			}
			function ultimodiaMes(){
				let fecha1 = document.getElementById('fc_1198971545').value.split("/");
				let ultimodia = new Date(fecha1[2], fecha1[1], 0).getDate();
				document.getElementById('fc_1198971546').value = ultimodia + "/" + fecha1[1] + "/" + fecha1[2];
			}
		</script>
	</head>
	<body>
		<div id="cargando" style="position:absolute;left: 46%; bottom: 45%">
			<img src="imagenes/loading.gif" style=" width: 80px; height: 80px"/>
		</div>
		<IFRAME src="alertas.php" name="alertas" id="alertas" style="display:none"></IFRAME>
		<span id="todastablas2"></span>
		<table>
			<tr>
				<script>barra_imagenes("cont");</script>
				<?php cuadro_titulos();?>
			</tr>
			<tr>
				<?php menu_desplegable("cont");?>
			</tr>
		</table>
        <div class="bg-white group-btn p-1"><button type="button" onclick="location.href='cont-buscaconciliacion.php'" class="btn btn-white btn-primary-hover d-flex justify-between align-items-center">
            <span>Nuevo</span>
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 -960 960 960"><path d="M440-440H200v-80h240v-240h80v240h240v80H520v240h-80v-240Z"></path></svg>
        </button><button type="button" onclick="mypop=window.open('plan-agenda.php','','');mypop.focus()" class="btn btn-white btn-primary-hover d-flex justify-between align-items-center">
            <span class="group-hover:text-white">Agenda</span>
            <svg class="fill-black group-hover:fill-white w-5 h-5" xmlns="http://www.w3.org/2000/svg" viewBox="0 -960 960 960"><path d="M560-564v-68q33-14 67.5-21t72.5-7q26 0 51 4t49 10v64q-24-9-48.5-13.5T700-600q-38 0-73 9.5T560-564Zm0 220v-68q33-14 67.5-21t72.5-7q26 0 51 4t49 10v64q-24-9-48.5-13.5T700-380q-38 0-73 9t-67 27Zm0-110v-68q33-14 67.5-21t72.5-7q26 0 51 4t49 10v64q-24-9-48.5-13.5T700-490q-38 0-73 9.5T560-454ZM260-320q47 0 91.5 10.5T440-278v-394q-41-24-87-36t-93-12q-36 0-71.5 7T120-692v396q35-12 69.5-18t70.5-6Zm260 42q44-21 88.5-31.5T700-320q36 0 70.5 6t69.5 18v-396q-33-14-68.5-21t-71.5-7q-47 0-93 12t-87 36v394Zm-40 118q-48-38-104-59t-116-21q-42 0-82.5 11T100-198q-21 11-40.5-1T40-234v-482q0-11 5.5-21T62-752q46-24 96-36t102-12q58 0 113.5 15T480-740q51-30 106.5-45T700-800q52 0 102 12t96 36q11 5 16.5 15t5.5 21v482q0 23-19.5 35t-40.5 1q-37-20-77.5-31T700-240q-60 0-116 21t-104 59ZM280-494Z"></path></svg>
        </button><button type="button" onclick="mypop=window.open('principal.php','',''); mypop.focus();" class="btn btn-white btn-primary-hover d-flex justify-between align-items-center">
            <span>Nueva ventana</span>
            <svg class="fill-black group-hover:fill-white w-5 h-5" xmlns="http://www.w3.org/2000/svg" viewBox="0 -960 960 960"><path d="M320-120v-80h80v-80H160q-33 0-56.5-23.5T80-360v-400q0-33 23.5-56.5T160-840h640q33 0 56.5 23.5T880-760v400q0 33-23.5 56.5T800-280H560v80h80v80H320ZM160-360h640v-400H160v400Zm0 0v-400 400Z"></path></svg>
        </button><button type="button" onclick="pdf()" class="btn btn-danger btn-danger-hover d-flex justify-between align-items-center">
            <span>Exportar PDF</span>
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><!-- !Font Awesome Free 6.5.1 by @fontawesome - https://fontawesome.com License - https://fontawesome.com/license/free Copyright 2024 Fonticons, Inc. --><path d="M64 464l48 0 0 48-48 0c-35.3 0-64-28.7-64-64L0 64C0 28.7 28.7 0 64 0L229.5 0c17 0 33.3 6.7 45.3 18.7l90.5 90.5c12 12 18.7 28.3 18.7 45.3L384 304l-48 0 0-144-80 0c-17.7 0-32-14.3-32-32l0-80L64 48c-8.8 0-16 7.2-16 16l0 384c0 8.8 7.2 16 16 16zM176 352l32 0c30.9 0 56 25.1 56 56s-25.1 56-56 56l-16 0 0 32c0 8.8-7.2 16-16 16s-16-7.2-16-16l0-48 0-80c0-8.8 7.2-16 16-16zm32 80c13.3 0 24-10.7 24-24s-10.7-24-24-24l-16 0 0 48 16 0zm96-80l32 0c26.5 0 48 21.5 48 48l0 64c0 26.5-21.5 48-48 48l-32 0c-8.8 0-16-7.2-16-16l0-128c0-8.8 7.2-16 16-16zm32 128c8.8 0 16-7.2 16-16l0-64c0-8.8-7.2-16-16-16l-16 0 0 96 16 0zm80-112c0-8.8 7.2-16 16-16l48 0c8.8 0 16 7.2 16 16s-7.2 16-16 16l-32 0 0 32 32 0c8.8 0 16 7.2 16 16s-7.2 16-16 16l-32 0 0 48c0 8.8-7.2 16-16 16s-16-7.2-16-16l0-64 0-64z"></path></svg>
        </button><button type="button" onclick="excell()" class="btn btn-success btn-success-hover d-flex justify-between align-items-center">
            <span>Exportar Excel</span>
            <svg class="fill-black group-hover:fill-white w-4 h-4" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 384 512"><!--!Font Awesome Free 6.5.1 by @fontawesome - https://fontawesome.com License - https://fontawesome.com/license/free Copyright 2024 Fonticons, Inc. --><path d="M48 448V64c0-8.8 7.2-16 16-16H224v80c0 17.7 14.3 32 32 32h80V448c0 8.8-7.2 16-16 16H64c-8.8 0-16-7.2-16-16zM64 0C28.7 0 0 28.7 0 64V448c0 35.3 28.7 64 64 64H320c35.3 0 64-28.7 64-64V154.5c0-17-6.7-33.3-18.7-45.3L274.7 18.7C262.7 6.7 246.5 0 229.5 0H64zm90.9 233.3c-8.1-10.5-23.2-12.3-33.7-4.2s-12.3 23.2-4.2 33.7L161.6 320l-44.5 57.3c-8.1 10.5-6.3 25.5 4.2 33.7s25.5 6.3 33.7-4.2L192 359.1l37.1 47.6c8.1 10.5 23.2 12.3 33.7 4.2s12.3-23.2 4.2-33.7L222.4 320l44.5-57.3c8.1-10.5 6.3-25.5-4.2-33.7s-25.5-6.3-33.7 4.2L192 280.9l-37.1-47.6z"></path></svg>
        </button><button type="button" onclick="location.href='cont-conciliacionNuevo.php'" class="btn btn-success btn-success-hover d-flex justify-between align-items-center">
            <span>Atras</span>
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 -960 960 960"><path d="M760-200v-160q0-50-35-85t-85-35H273l144 144-57 56-240-240 240-240 57 56-144 144h367q83 0 141.5 58.5T840-360v160h-80Z"></path></svg>
        </button></div>
		<form name="form2" id="form2" method="post">
			<?php
				if($_POST['oculto'] == ''){
					$_POST['fechai'] = date("01/m/Y");
					$fechafv = new DateTime();
					$fechafv->modify('last day of this month');
					$_POST['fechaf'] = $fechafv->format('d/m/Y');
				}
			?>
			<table class="inicio ancho">
				<tr>
					<td colspan='8' class='titulos'>.: Buscar Conciliacion</td>
					<td class="cerrar" style="width:7%" onClick="location.href='cont-principal.php'">Cerrar</td>
				</tr>
				<tr>
					<td class="saludo1" style="width:3cm;">Fecha Inicial:</td>
					<td style="width:15%;"><input type="text" name="fechai" id="fc_1198971545" title="DD/MM/YYYY" value="<?php echo $_POST['fechai']; ?>" onKeyUp="return tabular(event,this) " onKeyDown="mascara(this,'/',patron,true)" maxlength="10" class="colordobleclik" autocomplete="off" onChange="ultimodiaMes();" onDblClick="displayCalendarFor('fc_1198971545');"></td>
					<td class="saludo1" style="width:3cm;">Fecha Final:</td>
					<td style="width:15%;"><input type="text" name="fechaf" id="fc_1198971546" title="DD/MM/YYYY" value="<?php echo $_POST['fechaf']; ?>" onKeyUp="return tabular(event,this) " onKeyDown="mascara(this,'/',patron,true)" maxlength="10" readonly  autocomplete="off" onChange=""  style="text-align:center;"></td>
					<td style="padding-bottom:0px"><em class="botonflechaverde" onClick="buscar()">Buscar</em></td>
				</tr>
			</table>
			<input type="hidden" name="oculto" id="oculto" value="1"/>
			<table class='tablamv3'>
				<?php
					$fanio = $_POST['anio'];
					$fecha = $_POST['fechai'];
					$ffecha = $_POST['fechaf'];
					preg_match("/([0-9]{2})\/([0-9]{2})\/([0-9]{4})/", $_POST['fechai'],$fechatt);
					$fechaini = $fechatt[3]."-".$fechatt[2]."-".$fechatt[1];
					preg_match("/([0-9]{2})\/([0-9]{2})\/([0-9]{4})/", $_POST['fechaf'],$fechatt);
					$fechafin = $fechatt[3]."-".$fechatt[2]."-".$fechatt[1];
					echo "
					<thead>
						<tr style='text-align:Center;'>
							<th class='titulosnew00' style='width:3%;'></th>
							<th class='titulosnew00' style='width:10%;'>Cuenta Contable</th>
							<th class='titulosnew00' style='width:20%;'>Banco</th>
							<th class='titulosnew00' style='width:14%;'>Saldo Extracto</th>
							<th class='titulosnew00' style='width:14%;'>Saldo Extracto Calc</th>
							<th class='titulosnew00' style='width:14%;'>Diferencia</th>
							<th class='titulosnew00' style='width:14%;'>Saldo Libros</th>
							<th class='titulosnew00' style='width:11%;'>Extractos</th>
						</tr>
					</thead>
					<tbody style='height:60vh;'>";
					if($fecha != '' && $ffecha != '' && $_POST['oculto'] == '2'){
						$sqlr="SELECT estado, cuenta, tipo, nombre FROM cuentasnicsp WHERE cuenta LIKE '1110%' AND (tipo = 'Auxiliar' OR tipo = 'AUXILIAR') ORDER BY cuenta";
						$resp = mysqli_query($linkbd, $sqlr);
						$iter = 'saludo3';
						$iter2 = 'saludo3';
						$filas = 1;
						$cont = 1;
						while ($row = mysqli_fetch_row($resp)){
							$sql2 = "SELECT archivo FROM cont_extractos_conciliacion WHERE cuenta = '$row[1]' AND fechaini = '$fechaini' AND fechafin = '$fechafin' AND estado = 'S'";
							$res2 = mysqli_query($linkbd, $sql2);
							$row2 = mysqli_fetch_row($res2);
							if($row2[0] != ''){
								$nomarchivo = $row2[0];
							}else{
								$nomarchivo = '';
							}
							$sqlr1 = "SELECT * FROM conciliacion_cab WHERE cuenta = '$row[1]' AND periodo1 = '$fechaini' AND periodo2 = '$fechafin'";
							$resp1 = mysqli_query($linkbd, $sqlr1);
							$row1 = mysqli_fetch_row($resp1);
							$num_cols = mysqli_num_fields($resp1);
							$val = round($row1[1],2);
							$dif = $val - $row1[2];
							$dif = round($dif,2);
							$dif = abs($dif);
							if($dif <= 1){
								$dif = 0;
							}
							$aux = $iter;
							$iter = $iter2;
							$iter2 = $aux;
							$estilo = '';
							$sqlrbook = "SELECT  det.cuenta, (SUM(det.valdebito)-SUM(det.valcredito)) as saldof
							FROM comprobante_cab AS cab
							INNER JOIN comprobante_det AS det ON det.tipo_comp = cab.tipo_comp AND det.numerotipo = cab.numerotipo
							WHERE det.cuenta = '$row[1]' AND cab.fecha BETWEEN '' AND '$fechafin' AND cab.estado = '1'
							GROUP BY det.cuenta
							ORDER BY cab.tipo_comp, cab.numerotipo, det.id_det, cab.fecha";
							$resbook = mysqli_query($linkbd, $sqlrbook);
							$saldoperant = 0;
							while ($rowbook = mysqli_fetch_row($resbook)){
								$saldoperant = $rowbook[1];
							}
							$saldoperant = round($saldoperant,2);
							if($dif < 1 && $val != 0 && $num_cols > 0){
								$estilo = 'background-color:#84b6f4';
								$estado = "Conciliado";
							}
							if($dif>1){
								$estilo = 'background-color:#fdfd96';
								$estado = "En Proceso";
							}
							if(($dif>1) and ($dif<10000) ){
								$estilo = 'background-color:#fdfd96';
								$estado = "En Proceso";
							}
							if($row1[2] == '' || $num_cols == 0){
								$estilo = 'background-color:#ffffff';
								$estado = "Sin Conciliar";
								$row1[2] = 0;
							}
							if($dif<1 && $val==0 && $row1[2]==0 && $saldoperant==0 && $num_cols > 0){
								$estilo = '';
								$estado = "Conciliado";
							}
							if ($val != 0 || $row1[2] != 0 || $saldoperant != 0)
							{
								if($nomarchivo != ''){
									$imgdescarga = "<a href='informacion/extractos/$nomarchivo' target='_blank'><img src='imagenes/descargar_pdf.png' style='height:25px; width:25px;' title='Descargar Extracto'></a>";
								}else{
									$imgdescarga = "<img src='imagenes/descargar_pdf2.png' style='height:25px; width:25px;' title='Sin Extracto'/>";
								}
								echo"<tr class='$iter' ondblclick='direccionaCuentaGastos(this)' style='$estilo' title='$estado'>
										<td style='width:3.2%;'>$cont</td>
										<td style='width:10%;'>$row[1]</td>
										<td style='width:20.3%;'>$row[3]</td>
										<td style='width:14%;text-align:right;'>$".number_format($val,2,',','.')."</td>
										<td style='width:14.2%;text-align:right;'>$".number_format($row1[2],2,',','.')."</td>
										<td style='width:14.2%;text-align:right;'>$".number_format($dif,2,',','.')."</td>
										<td style='width:14.1%;text-align:right;'>$".number_format($saldoperant,2,',','.')."</td>
										<td style='width:5.5%;text-align:center;'><img src='imagenes/subir_archivo.png' class='icoop' title='Subir Extracto Bancario' onClick=\"despliegamodal2('visible','$row[1]','$val','$row1[2]','$dif')\" style='height:25px; width:25px;'/></td>
										<td style='width:5.5%;text-align:center;'>$imgdescarga</td>
									</tr>
									<input type='hidden' name='concepto1[]' value='".$row[1]."'/>
									<input type='hidden' name='concepto2[]' value='".$row[3]."'/>
									<input type='hidden' name='concepto3[]' value='$".number_format($val,2,',','.')."'/>
									<input type='hidden' name='concepto4[]' value='$".number_format($row1[2],2,',','.')."'/>
									<input type='hidden' name='concepto5[]' value='$".number_format($dif,2,',','.')."'/>
									<input type='hidden' name='concepto6[]' value='$".number_format($saldoperant,2,',','.')."'/>
									<input type='hidden' name='concepto7[]' value='".$estado."'/>
									";
								$cont+=1;
								$filas++;
							}
						}
					}elseif($_POST['oculto'] != ''){
						echo"
						<script>
							Swal.fire({
								icon: 'info',
								title: 'Debe ingresar fechas para el informe',
								confirmButtonText: 'Continuar',
								confirmButtonColor: '#FF121A',
								timer: 2500
							});
						</script>
						";

					}
				?>
			</table>
		</form>
		<div id="bgventanamodal2">
			<div id="ventanamodal2">
				<IFRAME  src="" name="buster" marginWidth=0 marginHeight=0 frameBorder=0 id="ventana2" frameSpacing=0 style="left:500px; width:900px; height:500px; top:200;"></IFRAME>
			</div>
		</div>
	</body>
</html>
