<?php
    require_once("tcpdf/tcpdf.php");
	require('comun.inc');
	require "funciones.inc";
    require 'funcionesSP.inc.php';
	date_default_timezone_set("America/Bogota");
	session_start();
    ini_set('display_errors', '1');
    ini_set('display_startup_errors', '1');
    error_reporting(E_ERROR);
	class MYPDF extends TCPDF {
        protected $last_page_flag = false;

        public function Close() {
            $this->last_page_flag = true;
            parent::Close();
        }

		public function Header() {
			$linkbd = conectar_v7();
			$linkbd -> set_charset("utf8");
			$sqlr="SELECT * FROM configbasica WHERE estado='S'";
			$res=mysqli_query($linkbd, $sqlr);
			while($row=mysqli_fetch_row($res)) {
				$nit = $row[0];
				$rs  = $row[1];
			}
			//Parte Izquierda
			$this->Image('imagenes/escudo.jpg',  13, 13, 20, 20, 'JPG', '', 'T', true, 300, '', false, false, 0, false, false, false);// Logo
			$this->SetFont('helvetica','B',8);
			$this->SetY(10);
			$this->RoundedRect(10, 10, 190, 25, 1,'');
			$this->Cell(0.1);
			$this->Cell(26,25,'','R',0,'L');
			$this->SetY(8);
			$this->SetX(40);
			$this->SetFont('helvetica','B',9);
			$this->Cell(140,15,strtoupper("$rs"),0,0,'C');
			$this->SetFont('helvetica','B',7);
			$this->SetY(12);
			$this->SetX(40);
			$this->Cell(140,15,'NIT: '.$nit,0,0,'C');
			
			$this->SetFont('helvetica','B',9);
			$this->SetY(23);
			$this->SetX(36);
            $this->Cell(164,12,"SALIDA DE ALMACÉN",'T',0,'C');

			
			$this->SetFont('times','B',10);
			$this->ln(12);
			
            $consecutivo = $_POST["consec"];
            $sql_cab = "SELECT estado FROM almginventario WHERE consec = $consecutivo AND tipomov = '2' AND tiporeg = '06'";
            $row_cab = mysqli_fetch_assoc(mysqli_query($linkbd, $sql_cab));

            if($row_cab["estado"] != "S"){
                $img_file = "assets/img/reversado.png";
                $this->SetAlpha(0.35);
                $this->Image($img_file, 0, 20, 250, 280, '', '', '', false, 300, '', false, false, 300);
                $this->SetAlpha(1);
            }
		}
		public function Footer()
		{
			$linkbd = conectar_v7();
			$linkbd -> set_charset("utf8");
			$sqlr="SELECT direccion,telefono,web,email FROM configbasica WHERE estado='S'";
			$resp=mysqli_query($linkbd, $sqlr);
			$user = $_SESSION['nickusu'];
            $cedula = $_SESSION['cedulausu'];
			$fecha = date("Y-m-d H:i:s");
			$ip = $_SERVER['REMOTE_ADDR'];
			while($row=mysqli_fetch_row($resp))
			{
				$direcc=strtoupper($row[0]);
				$telefonos=$row[1];
				$dirweb=strtoupper($row[3]);
				$coemail=strtoupper($row[2]);
			}
			if($direcc!=''){$vardirec="Dirección: $direcc, ";}
			else {$vardirec="";}
			if($telefonos!=''){$vartelef="Telefonos: $telefonos";}
			else{$vartelef="";}
			if($dirweb!=''){$varemail="Email: $dirweb, ";}
			else {$varemail="";}
			if($coemail!=''){$varpagiw="Pagina Web: $coemail";}
			else{$varpagiw="";}

            //Firmas
            if ($this->last_page_flag)  {
                $this->setY(240);
                $this->setX(30);
                $this->SetFont('helvetica','B',6);
                $this->SetFillColor(153,221,255);
                $this->cell(70,4,'DATOS DE JEFE DE ALMACEN','LRTB',0,'C',1);
                $this->setX(110);
                $this->cell(70,4,'DATOS DE QUIEN RECIBE','LRTB',0,'C',1);
                $this->ln();
                $this->setX(30);
                $this->SetFont('helvetica','B',6);
                $this->SetFillColor(255,255,255);
                $this->MultiCell(70,4," NOMBRE: ","LRTB",'L',true,0,'','',true,0,false,true,0,'M',true);
                $this->setX(110);
                $this->cell(70,4,' NOMBRE:','LRTB',0,'L',1);
                $this->ln();
                $this->setX(30);
                $this->SetFont('helvetica','B',6);
                $this->SetFillColor(255,255,255);
                $this->MultiCell(70,4," DOCUMENTO: ","LRTB",'L',true,0,'','',true,0,false,true,0,'M',true);
                $this->setX(110);
                $this->MultiCell(70,4," DOCUMENTO: ","LRTB",'L',true,0,'','',true,0,false,true,0,'M',true);
                $this->ln();
                $this->setX(30);
                $this->SetFont('helvetica','',6);
                $this->SetFillColor(255,255,255);
                $this->cell(70,15,'Firma','LRTB',0,'C',1);
                $this->setX(110);
                $this->cell(70,15,'Firma','LRTB',0,'C',1);
            }

            $this->setY(280);
			$this->SetFont('helvetica', 'I', 8);
			$txt = <<<EOD
			$vardirec $vartelef
			$varemail $varpagiw
			EOD;
			$this->SetFont('helvetica', 'I', 6);
			$this->Cell(277,10,'','T',0,'T');
			$this->ln(2);
			$this->Write(0, $txt , '', 0, 'C', true, 0, false, false, 0);

			$this->Cell(50, 10, 'Hecho por: '.$_POST["realiza"], 00, false, 'C', 0, '', 0, false, 'T', 'M');
			$this->Cell(30, 10, 'Impreso por: '.$user, 0, false, 'C', 0, '', 0, false, 'T', 'M');
			$this->Cell(30, 10, 'IP: '.$ip, 0, false, 'C',0, '', 0, false, 'T', 'M');
			$this->Cell(30, 10, 'Fecha: '.$fecha, 0, false, 'C', 0, '', 0, false, 'T', 'M');
			$this->Cell(30, 10, 'IDEAL.10 S.A.S', 0, false, 'C', 0, '', 0, false, 'T', 'M');
            $this->Cell(20, 10, 'Pagina '.$this->getAliasNumPage().'/'.$this->getAliasNbPages(), 0, false, 'C', 0, '', 0, false, 'T', 'M');
		}
	}

	
    $pdf = new MYPDF('P','mm','Letter', true, 'iso-8859-1', false);
    $pdf->SetDocInfoUnicode (true);
    // set document information
    $pdf->SetCreator(PDF_CREATOR);
    $pdf->SetAuthor('IDEALSAS');
    $pdf->SetTitle('SALIDA');
    $pdf->SetSubject('SALIDA');
    $pdf->SetKeywords('TCPDF, PDF, example, test, guide');
    $pdf->SetMargins(10, 38, 10);// set margins
    $pdf->SetHeaderMargin(38);// set margins
    $pdf->SetFooterMargin(17);// set margins
    $pdf->SetAutoPageBreak(TRUE, 20);// set auto page breaks
    // set some language-dependent strings (optional)
    if (@file_exists(dirname(__FILE__).'/lang/spa.php'))
    {
        require_once(dirname(__FILE__).'/lang/spa.php');
        $pdf->setLanguageArray($l);
    }
    $pdf->SetFillColor(255,255,255);
    $pdf->AddPage();

    $linkbd = conectar_v7();
    $linkbd -> set_charset("utf8");

	$consecutivo = $_POST["consec"];
	$fecha = $_POST["fecha"];
	$detalle = $_POST["descripcion"];
	$valor_total = $_POST["totalart"];
	$tipo_mov = "2";
	$tipo_reg = "06";
	$tipo_comprobante = 65;

    $sql_det = "SELECT det.codart AS codigo_articulo, 
    det.cantidad_salida AS cantidad, 
    det.valorunit AS valor_unitario, 
    det.valortotal AS valor_total, 
    bodega.nombre AS nombre_bodega, 
    cc.nombre AS nombre_cc
    FROM almginventario_det AS det
    INNER JOIN almbodegas AS bodega ON det.bodega = bodega.id_cc
    INNER JOIN centrocosto AS cc ON det.cc = cc.id_cc
    WHERE codigo = $consecutivo AND tipomov = '$tipo_mov' AND tiporeg = '$tipo_reg'";
    $row_det = mysqli_fetch_all(mysqli_query($linkbd, $sql_det), MYSQLI_ASSOC);
   
    foreach ($row_det as $key => $det) {
        $grupo = substr($det["codigo_articulo"], 0, 4);
        $codigo = substr($det["codigo_articulo"], 4, 5);
        $sql_articulo = "SELECT nombre FROM almarticulos WHERE codigo = '$codigo' AND grupoinven = '$grupo'";
        $row_articulo = mysqli_fetch_assoc(mysqli_query($linkbd, $sql_articulo));
        $row_det[$key]["nombre_articulo"] = ucfirst(mb_strtolower(replaceChar(strClean($row_articulo["nombre"]))));
    }

    $arr_det = $row_det;

    $sql_comprobantes = "SELECT cuenta, 
    SUM(valdebito) AS valdebito, 
    SUM(valcredito) AS valcredito 
    FROM comprobante_det 
    WHERE tipo_comp = '$tipo_comprobante' 
    AND numerotipo = '$consecutivo' 
    GROUP BY cuenta";

    $row_comprobantes = mysqli_fetch_all(mysqli_query($linkbd, $sql_comprobantes), MYSQLI_ASSOC);

    foreach ($row_comprobantes as $key => $comprobante) {
        $sql_cuenta = "SELECT nombre FROM cuentasnicsp WHERE cuenta = '$comprobante[cuenta]'";
        $row_cuenta = mysqli_fetch_assoc(mysqli_query($linkbd, $sql_cuenta));
        $row_comprobantes[$key]["nombre_cuenta"] = $row_cuenta["nombre"] != "" ? $row_cuenta["nombre"] : "Cuenta no creada";
    }

    $pdf->SetFont('helvetica','B',9);
    $pdf->SetFillColor(153,221,255);
    $pdf->MultiCell(190,6,"SALIDA DIRECTA","LRBT",'C',true,0,'','',true,0,false,true,0,'M',true);
    $pdf->ln();
    $pdf->SetFillColor(255,255,255);
    $pdf->SetTextColor(0,0,0);
    $pdf->SetFont('Helvetica','',8);
    $pdf->MultiCell(25,5,"CONSECUTIVO:","LRBT",'L',true,0,'','',true,0,false,true,0,'M',true);
    $pdf->MultiCell(25,5,$consecutivo,"LRBT",'C',true,0,'','',true,0,false,true,0,'M',true);
    $pdf->MultiCell(30,5,"FECHA:","LRBT",'L',true,0,'','',true,0,false,true,0,'M',true);
    $pdf->MultiCell(30,5,$fecha,"LRBT",'C',true,0,'','',true,0,false,true,0,'M',true);
    $pdf->MultiCell(30,5,"VALOR TOTAL:","LRBT",'L',true,0,'','',true,0,false,true,0,'M',true);
    $pdf->MultiCell(50,5,"$" . number_format($valor_total, 2),"LRBT",'R',true,0,'','',true,0,false,true,0,'M',true);
    $pdf->ln();
    $pdf->MultiCell(30,12,"DETALLE:","LRBT",'L',true,0,'','',true,0,false,true,0,'M',true);
    $pdf->MultiCell(160,12,$detalle,"LRBT",'L',true,0,'','',true,0,false,true,0,'M',true);

    if (!empty($arr_det)) {
        $pdf->ln();
        $pdf->SetFont('helvetica','B',9);
        $pdf->SetFillColor(153,221,255);
        $pdf->MultiCell(190,6,"DISCRIMINACIÓN DE ARTICULOS","LRBT",'C',true,0,'','',true,0,false,true,0,'M',true);
        $pdf->ln();
        $pdf->MultiCell(20,10,"Cod articulo","LRBT",'C',true,0,'','',true,0,false,true,0,'M',true);
        $pdf->MultiCell(70,10,"Nombre articulo","LRBT",'C',true,0,'','',true,0,false,true,0,'M',true);
        $pdf->MultiCell(25,10,"Bodega","LRBT",'C',true,0,'','',true,0,false,true,0,'M',true);
        $pdf->MultiCell(25,10,"Valor unitario","LRBT",'C',true,0,'','',true,0,false,true,0,'M',true);
        $pdf->MultiCell(20,10,"Cantidad","LRBT",'C',true,0,'','',true,0,false,true,0,'M',true);
        $pdf->MultiCell(30,10,"Valor total","LRBT",'C',true,0,'','',true,0,false,true,0,'M',true);

        $pdf->ln();
        $pdf->SetFillColor(255,255,255);
        $pdf->SetTextColor(0,0,0);
        $pdf->SetFont('Helvetica','',8);
        
        foreach ($arr_det as $key => $det) {
			$height = $pdf->getNumLines($det["nombre_articulo"]) * 6;
            
            $pdf->MultiCell(20,$height,"$det[codigo_articulo]","LRBT",'C',true,0,'','',true,0,false,true,0,'M',true);
            $pdf->MultiCell(70,$height,"$det[nombre_articulo]","LRBT",'L',true,0,'','',true,0,false,true,0,'M',true);
            $pdf->MultiCell(25,$height,"$det[nombre_bodega]","LRBT",'J',true,0,'','',true,0,false,true,0,'M',true);
            $pdf->MultiCell(25,$height,"$".number_format($det["valor_unitario"], 2),"LRBT",'R',true,0,'','',true,0,false,true,0,'M',true);
            $pdf->MultiCell(20,$height,"$det[cantidad]","LRBT",'C',true,0,'','',true,0,false,true,0,'M',true);
            $pdf->MultiCell(30,$height,"$".number_format($det["valor_total"], 2),"LRBT",'R',true,0,'','',true,0,false,true,0,'M',true);

            $suma_articulos += $det["valor_total"];

            $pdf->ln();
            $getY = $pdf->getY();
            if ($getY > 210) {
                $pdf->AddPage();
            }
        }

        $pdf->SetFont('helvetica','B',8);
        $pdf->MultiCell(160,5,"TOTAL SALIDA:","LRBT",'C',true,0,'','',true,0,false,true,0,'M',true);
        $pdf->MultiCell(30,5,"$".number_format($suma_articulos, 2),"LRBT",'R',true,0,'','',true,0,false,true,0,'M',true);
    }

    $pdf->ln();
    if (!empty($row_comprobantes)) {
        $pdf->ln();
        $pdf->SetFont('helvetica','B',8);
        $pdf->SetFillColor(153,221,255);
        $pdf->MultiCell(190,5,"DISCRIMINACIÓN CONTABLE","LRBT",'C',true,0,'','',true,0,false,true,0,'M',true);
        $pdf->ln();
        $pdf->MultiCell(40,10,"Cuenta","LRBT",'C',true,0,'','',true,0,false,true,0,'M',true);
        $pdf->MultiCell(70,10,"Nombre","LRBT",'C',true,0,'','',true,0,false,true,0,'M',true);
        $pdf->MultiCell(40,10,"Debito","LRBT",'C',true,0,'','',true,0,false,true,0,'M',true);
        $pdf->MultiCell(40,10,"Credito","LRBT",'C',true,0,'','',true,0,false,true,0,'M',true);
        $pdf->ln();
        $pdf->SetFont('helvetica','',8);
        $pdf->SetFillColor(255,255,255);
        $totalDebito = $totalCredito = 0;
        foreach ($row_comprobantes as $comprobante) {
            $saldoCuenta = round($comprobante["valdebito"],2) - round($comprobante["valcredito"], 2);
            if($saldoCuenta < 0){
                $ValDebito = 0;
                $ValCredito = $comprobante["valcredito"];
            }else{
                $ValDebito = $comprobante["valdebito"];
                $ValCredito = 0;
            }
            $height = $pdf->getNumLines($comprobante['nombre_cuenta'])*5;
            $pdf->MultiCell(40,$height,"$comprobante[cuenta]","LRBT",'C',true,0,'','',true,0,false,true,0,'M',true);
            $pdf->MultiCell(70,$height,"$comprobante[nombre_cuenta]","LRBT",'L',true,0,'','',true,0,false,true,0,'M',true);
            $pdf->MultiCell(40,$height,"$".number_format($ValDebito, 2),"LRBT",'R',true,0,'','',true,0,false,true,0,'M',true);      
            $pdf->MultiCell(40,$height,"$".number_format($ValCredito, 2),"LRBT",'R',true,0,'','',true,0,false,true,0,'M',true);      

            $totalDebito += $ValDebito;
            $totalCredito += $ValCredito;
            $pdf->ln();
            $getY = $pdf->getY();
            if ($getY > 210) {
                $pdf->AddPage();
            }
        }

        $pdf->SetFont('helvetica','B',8);
        $pdf->MultiCell(110,5,"TOTALES:","LRBT",'C',true,0,'','',true,0,false,true,0,'M',true);
        $pdf->MultiCell(40,5,"$".number_format($totalDebito, 2),"LRBT",'R',true,0,'','',true,0,false,true,0,'M',true);
        $pdf->MultiCell(40,5,"$".number_format($totalCredito, 2),"LRBT",'R',true,0,'','',true,0,false,true,0,'M',true);
    }

    $pdf->Output("salida_directa_$consecutivo.pdf", 'I');
?>
