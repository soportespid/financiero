<?php

    header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=utf8");
	require 'comun.inc';
	require 'funciones.inc';
	session_start();
    if(empty($_SESSION)){
        header("location: index.php");
    }
	$linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");
	cargarcodigopag($_GET['codpag'],$_SESSION["nivel"]);
	date_default_timezone_set("America/Bogota");
?>
<!DOCTYPE >
<html lang=es>
	<head>
    <meta charset="utf-8"/>
		<meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1"/>
		<meta name="viewport" content="user-scalable=no">
		<title>:: IDEAL 10 - Presupuesto</title>
		<link href="favicon.ico" rel="shortcut icon"/>
		<link href="css/css2z.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
		<link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
		<link href="css/css4.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
        <link href="css/tabs.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css"/>
        <link href="css/style.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css"/>
        <link rel="stylesheet" href="sweetalert2/dist/sweetalert2.min.css">
        <script type="text/javascript" src="css/programas.js"></script>
        <script src="sweetalert2/dist/sweetalert2.min.js"></script>
		<script src="vue/vue.min.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script src="css/programas.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script src="css/calendario.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
	</head>
	<body>
		<header>
			<table>
				<tr><script>barra_imagenes("ccpet");</script><?php cuadro_titulos();?></tr>
			</table>
		</header>

		<form name="form2" method="post" action="">
			<section id="myapp" v-cloak >
                <div class="loading-container" v-show="isLoading" >
                    <p class="text-loading"data-loading="IDEAL 10">IDEAL 10</p>
                </div>
				<nav>
					<table>
						<tr><?php menu_desplegable("ccpet");?></tr>
					</table>
                    <div class="bg-white group-btn p-1" id="newNavStyle">
                        <button type="button" @click="save()" class="btn btn-white btn-primary-hover d-flex justify-between align-items-center">
                            <span>Guardar</span>
                            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 -960 960 960">
                                <path d="M840-680v480q0 33-23.5 56.5T760-120H200q-33 0-56.5-23.5T120-200v-560q0-33 23.5-56.5T200-840h480l160 160Zm-80 34L646-760H200v560h560v-446ZM480-240q50 0 85-35t35-85q0-50-35-85t-85-35q-50 0-85 35t-35 85q0 50 35 85t85 35ZM240-560h360v-160H240v160Zm-40-86v446-560 114Z">
                                </path>
                            </svg>
                        </button>
                        <button type="button" class="btn btn-white btn-primary-hover d-flex justify-between align-items-center" @click="mypop=window.open('ccp-principal.php','',''); mypop.focus();">
                            <span>Nueva ventana</span>
                            <svg class="fill-black group-hover:fill-white w-5 h-5" xmlns="http://www.w3.org/2000/svg"  viewBox="0 -960 960 960" ><path d="M320-120v-80h80v-80H160q-33 0-56.5-23.5T80-360v-400q0-33 23.5-56.5T160-840h640q33 0 56.5 23.5T880-760v400q0 33-23.5 56.5T800-280H560v80h80v80H320ZM160-360h640v-400H160v400Zm0 0v-400 400Z"/></svg>
                        </button>
                        <button type="button" @click="mypop=window.open('ccp-rubros','','');mypop.focus();" class="btn btn-white btn-primary-hover d-flex justify-between align-items-center">
                            <span class="group-hover:text-white">Duplicar pantalla</span>
                            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 -960 960 960"><path d="M640-160v-360H160v360h480Zm80-200v-80h80v-360H320v200h-80v-200q0-33 23.5-56.5T320-880h480q33 0 56.5 23.5T880-800v360q0 33-23.5 56.5T800-360h-80ZM160-80q-33 0-56.5-23.5T80-160v-360q0-33 23.5-56.5T160-600h480q33 0 56.5 23.5T720-520v360q0 33-23.5 56.5T640-80H160Zm400-603ZM400-340Z"></path></svg>
                        </button>
                    </div>
				</nav>
				<article>
                     <div  class="bg-white">
                        <div>
                            <h2 class="titulos m-0">Creación de rubros</h2>
                            <p class="m-0 ms-2">Todos los campos con (<span class="text-danger fw-bolder">*</span>) son obligatorios</p>
                            <div class="d-flex">
                                <div class="d-flex w-50">
                                    <div class="form-control w-50" >
                                        <label class="form-label m-0" for="">Rubro:</label>
                                        <input type="text" class="bg-warning cursor-pointer" v-model="objRubro.rubro" @dblclick="isModalRubro=true">
                                    </div>
                                    <div class="form-control">
                                        <label class="form-label m-0" for="">Fuente:</label>
                                        <div class="d-flex">
                                            <input type="text" class="bg-warning cursor-pointer w-25" v-model="objFuente.codigo" @change="search('cod_fuente')" @dblclick="isModalFuente=true">
                                            <input type="text" v-model="objFuente.nombre" disabled>
                                        </div>
                                    </div>
                                </div>
                                <div class="d-flex w-50">
                                    <div class="form-control">
                                        <label class="form-label m-0" for="">Vigencia de gasto:</label>
                                        <select  v-model="selectVigencias">
                                            <option v-for="(data,index) in arrVigencias" :key="index" :value="data.codigo">{{data.codigo+" - "+data.nombre}}</option>
                                        </select>
                                    </div>
                                    <div class="form-control">
                                        <label class="form-label m-0" for="">Medio de pago:</label>
                                        <div class="d-flex">
                                            <select  v-model="selectPago">
                                                <option value="CSF">CSF</option>
                                                <option value="SSF">SSF</option>
                                            </select>
                                            <button type="button" class="btn btn-primary" @click="add()">Crear</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="d-flex">
                                <div class="d-flex w-50">
                                    <div class="form-control" >
                                        <label class="form-label m-0" for="">Sector:</label>
                                        <input type="text" :value="objRubro.sector+'-'+objRubro.nombre_sector"  disabled>
                                    </div>
                                    <div class="form-control" >
                                        <label class="form-label m-0" for="">Programa:</label>
                                        <input type="text" :value="objRubro.programa+'-'+objRubro.nombre_programa" disabled>
                                    </div>
                                </div>
                                <div class="d-flex w-50">
                                    <div class="form-control" >
                                        <label class="form-label m-0" for="">Producto:</label>
                                        <input type="text" :value="objRubro.producto+'-'+objRubro.nombre_producto" disabled>
                                    </div>
                                    <div class="form-control" >
                                        <label class="form-label m-0" for="">Proyecto:</label>
                                        <input type="text"  :value="objRubro.codigo+'-'+objRubro.nombre"disabled>
                                    </div>
                                </div>
                            </div>
                            <div class="d-flex">
                                <div class="form-control" >
                                    <label class="form-label m-0" for="">Sector:</label>
                                    <select @change="filterData('sector')" v-model="selectSectorData">
                                        <option value="0" selected>Seleccione</option>
                                        <option v-for="(data,index) in arrSectoresData" :key="index" :value="data.codigo">
                                            {{data.codigo+"-"+data.nombre}}
                                        </option>
                                    </select>
                                </div>
                                <div class="form-control" >
                                    <label class="form-label m-0" for="">Programa:</label>
                                    <select @change="filterData('programa')"  v-model="selectProgramaData">
                                        <option value="0" selected>Seleccione</option>
                                        <option v-for="(data,index) in arrProgramasData" :key="index" :value="data.codigo">
                                            {{data.codigo+"-"+data.nombre}}
                                        </option>
                                    </select>
                                </div>
                                <div class="form-control" >
                                    <label class="form-label m-0" for="">Producto:</label>
                                    <select @change="filterData('producto')"  v-model="selectProductoData">
                                        <option value="0" selected>Seleccione</option>
                                        <option v-for="(data,index) in arrProductosData" :key="index" :value="data.codigo">
                                            {{data.codigo+"-"+data.nombre}}
                                        </option>
                                    </select>
                                </div>
                            </div>
                            <div class="overflow-y" style="max-height:40vh">
                                <table class="table fw-normal">
                                    <thead>
                                        <tr>
                                            <th>Rubro</th>
                                            <th>Nombre</th>
                                            <th>Fuente</th>
                                            <th>Vigencia gasto</th>
                                            <th>Sección presupuestal</th>
                                            <th>Opciones</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr v-for="(data,index) in arrDataCopy" :key="index">
                                            <td>{{data.rubro}}</td>
                                            <td>{{data.nombre}}</td>
                                            <td>{{data.fuente+"-"+data.nombre_fuente}}</td>
                                            <td>{{data.vigencia+"-"+data.nombre_vigencia}}</td>
                                            <td>{{data.seccion+"-"+data.nombre_seccion}}</td>
                                            <td><button class="btn btn-danger btn-sm" v-if="!data.is_saved" type="button" @click="del(index)">Eliminar</button></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <!--MODALES-->
                    <div v-show="isModalRubro" class="modal">
                        <div class="modal-dialog modal-xl" >
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title">Buscar proyectos</h5>
                                    <button type="button" @click="isModalRubro=false;" class="btn btn-close"><div></div><div></div></button>
                                </div>
                                <div class="modal-body">
                                    <div class="d-flex">
                                        <div class="form-control" >
                                            <label class="form-label m-0" for="">Sector:</label>
                                            <select @change="filter('sector')" v-model="selectSector">
                                                <option value="0"  selected>Seleccione</option>
                                                <option v-for="(data,index) in arrSectores" :key="index" :value="data.codigo">
                                                    {{data.codigo+"-"+data.nombre}}
                                                </option>
                                            </select>
                                        </div>
                                        <div class="form-control" >
                                            <label class="form-label m-0" for="">Programa:</label>
                                            <select @change="filter('programa')"  v-model="selectPrograma">
                                                <option value="0"  selected>Seleccione</option>
                                                <option v-for="(data,index) in arrProgramas" :key="index" :value="data.codigo">
                                                    {{data.codigo+"-"+data.nombre}}
                                                </option>
                                            </select>
                                        </div>
                                        <div class="form-control" >
                                            <label class="form-label m-0" for="">Producto:</label>
                                            <select @change="filter('producto')"  v-model="selectProducto">
                                                <option value="0" selected>Seleccione</option>
                                                <option v-for="(data,index) in arrProductos" :key="index" :value="data.codigo">
                                                    {{data.codigo+"-"+data.nombre}}
                                                </option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="overflow-auto max-vh-50 overflow-x-hidden " >
                                        <table class="table table-hover fw-normal p-2">
                                            <thead>
                                                <tr>
                                                    <th>Proyecto</th>
                                                    <th>Nombre</th>
                                                    <th>Sector</th>
                                                    <th>Nombre sector</th>
                                                    <th>Programa</th>
                                                    <th>Nombre programa</th>
                                                    <th>Producto</th>
                                                    <th>Nombre producto</th>
                                                    <th>Indicador producto</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr v-for="(data,index) in arrBpimCopy" :key="index" @dblclick="selectItem('modal_rubro',data)">
                                                    <td>{{data.codigo}}</td>
                                                    <td>{{data.nombre}}</td>
                                                    <td>{{data.sector}}</td>
                                                    <td>{{data.nombre_sector}}</td>
                                                    <td>{{data.programa}}</td>
                                                    <td>{{data.nombre_programa}}</td>
                                                    <td>{{data.producto}}</td>
                                                    <td>{{data.nombre_producto}}</td>
                                                    <td>{{data.indicador}}</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary"  @click="isModalRubro=false;">Cerrar</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div v-show="isModalFuente" class="modal">
                        <div class="modal-dialog modal-lg" >
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title">Buscar fuentes</h5>
                                    <button type="button" @click="isModalFuente=false;" class="btn btn-close"><div></div><div></div></button>
                                </div>
                                <div class="modal-body">
                                    <div class="d-flex flex-column">
                                        <div class="form-control m-0 mb-3">
                                            <input type="search" v-model="txtSearchFuente" @keyup="search('modal_fuente')" placeholder="Buscar por código o nombre">
                                        </div>
                                        <div class="form-control m-0 p-2">
                                            <label class="form-label" for="labelInputName">Resultados: <span class="fw-bold">{{txtResultadosFuentes}}</span></label>
                                        </div>
                                    </div>
                                    <div class="overflow-auto max-vh-50 overflow-x-hidden " >
                                        <table class="table table-hover fw-normal p-2">
                                            <thead>
                                                <tr>
                                                    <th class="text-center">Item</th>
                                                    <th>Código</th>
                                                    <th>Nombre</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr v-for="(data,index) in arrFuentesCopy" :key="index" @dblclick="selectItem('modal_fuente',data)">
                                                    <td>{{index+1}}</td>
                                                    <td>{{data.codigo}}</td>
                                                    <td>{{data.nombre}}</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
				</article>
			</section>
		</form>

		<script src="node_modules/read-excel-file/bundle/read-excel-file.min.js"></script>
		<script src="node_modules/xlsx/dist/xlsx.full.min.js"></script>
        <script src="Librerias/vue/vue.min.js"></script>
		<script src="Librerias/vue/axios.min.js"></script>
		<script type="module" src="presupuesto_ccpet/procesos_gastos/js/functions_rubro.js?<?= date('d_m_Y_h_i_s');?>"></script>

	</body>
</html>
