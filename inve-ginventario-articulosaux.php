<?php
	header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=utf8");
	require"comun.inc";
	require"funciones.inc";
	session_start();
	$linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");
	date_default_timezone_set("America/Bogota");
?>
<!DOCTYPE >
<html lang="es">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1">
		<link href="css/css2.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css">
		<link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css">
		<link href="css/css4.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css">
		<script type="text/javascript" src="css/programas.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script type="text/javascript" src="JQuery/jquery-2.1.4.min.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script>
			function validar(){document.form2.submit();}
			function buscaref(){document.form2.oculto.value=2;document.form2.submit();}
			function ponprefijo(opc1,opc2,opc3,opc4,opc5,opc6)
			{
				parent.document.getElementById('docum').value = opc1;
				parent.document.getElementById('codigoarticulo').value = opc1;
				parent.document.getElementById('ndocum').value = opc2;
				parent.document.getElementById('narticulo').value = opc2;
				parent.document.getElementById('codigounspsc').value = opc3;
				parent.document.getElementById('cbodega').value = opc4;
				parent.document.getElementById('unidadmedidaart').value = opc5;
				parent.document.getElementById('nombodega').value = opc6;
				parent.document.getElementById('bodega').disabled = true;
				parent.despliegamodal2('hidden');
			}
		</script>
	</head>
	<body>
		<form name="form2" method="post">
			<?php
				if($_POST['oculto']=="")
				{
					$_POST['numpos']=0;
					$_POST['numres']=10;
					$_POST['nummul']=0;
				}
				if($_GET['bodega'])
				{
					$_POST['bodega'] = $_GET['bodega'];
				}
				if($_GET['cc'])
				{
					$_POST['cc'] = $_GET['cc'];
				}
			?>
			<table class="inicio ancho" style="width:99.4%;" >
				<tr>
					<td class="titulos" colspan="4">:: Buscar Articulos</td>
					<td class="cerrar" style="width:7%" onClick="parent.despliegamodal2('hidden');">Cerrar</td>
				</tr>
				<tr>
					<td class="saludo1" style="width:2cm;">:: C&oacute;digo:</td>
					<td style="width:15%;"><input type="text" name="codigo" id="codigo" value="<?php echo $_POST['codigo'];?>" style="width:100%;"/></td>
					<td class="saludo1" style="width:2cm;">:: Nombre:</td>
					<td>
						<input type="text" name="nombre" id="nombre" value="<?php echo $_POST['nombre'];?>" style="width:65%"/>&nbsp;
						<em class="botonflecha" name="Submit" onClick="document.form2.submit();">Buscar</em>
					</td>
				</tr>
			</table>
			<input type="hidden" name="oculto" id="oculto" value="1"/>
			<input type="hidden" name="numres" id="numres" value="<?php echo $_POST['numres'];?>">
			<input type="hidden" name="numpos" id="numpos" value="<?php echo $_POST['numpos'];?>">
			<input type="hidden" name="nummul" id="nummul" value="<?php echo $_POST['nummul'];?>">
			<input type="hidden" name="bodega" id="bodega" value="<?php echo $_POST['bodega'];?>">
			<input type="hidden" name="cc" id="cc" value="<?php echo $_POST['cc'];?>">
			<div class="subpantallac" style="height:87%; width:99.1%; overflow-x:hidden;">
				<?php
					$co='saludo1a';
					$co2='saludo2';
					$crit1="";
					$crit2="";
					if ($_POST['codigo']!=""){$crit1="AND concat_ws('', grupoinven, codigo) LIKE '%".$_POST['codigo']."%'";}
					if ($_POST['nombre']!=""){$crit2="AND nombre LIKE '%".$_POST['nombre']."%'";}
					$sqlr = "SELECT * FROM almarticulos WHERE estado='S' $crit1 $crit2 ORDER BY length(grupoinven), grupoinven ASC, length(codigo),codigo ASC";
					$resp = mysqli_query($linkbd, $sqlr);
					if($_POST['numpos']==0)
					{
						$imagenback="<img src='imagenes/back02.png' style='width:17px'>";
						$imagensback="<img src='imagenes/skip_back02.png' style='width:16px'>";
					}
					else
					{
						$imagenback="<img src='imagenes/back01.png' style='width:17px' title='Anterior' onClick='numanterior();'>";
						$imagensback="<img src='imagenes/skip_back01.png' style='width:16px' title='Inicio' onClick='saltocol(\"1\")'>";
					}
					echo"
 					<table class='inicio'>
 						<tr>
							<td class='titulos2' style='width:10%'>Codigo</td>
							<td class='titulos2'>Nombre</td>
							<td class='titulos2' style='width:25%'>Grupo Inventario</td>
							<td class='titulos2' style='width:8%'>Disponible</td>
							<td class='titulos2' style='width:8%'>Unidad</td>
							<td class='titulos2' style='width:8%'>Bodega</td>
							<td class='titulos2' style='width:5%'>CC</td>
						</tr>";
					while($row = mysqli_fetch_row($resp))
					{
						$sqlr2 = "SELECT nombre FROM almgrupoinv WHERE codigo='$row[3]'";
						$row2 = mysqli_fetch_row(mysqli_query($linkbd, $sqlr2));
						$grupinv = "$row2[0]";
						$codart = "$row[3]$row[0]";
						$unprinart = almconculta_um_principal($codart);
						$nombreBodega = '';
						$sqlrBodegas = "SELECT * FROM almbodegas WHERE estado='S' AND id_cc = '".$_POST['bodega']."' ORDER BY id_cc";
						$respBodegas = mysqli_query($linkbd, $sqlrBodegas);
						$rowBodegas = mysqli_fetch_row($respBodegas);
						$nombreBodega = $rowBodegas[0]." - ".$rowBodegas[1];
						$totalreserva = 0;
						$disponible = totalinventarioConRutina($codart,$_POST['bodega'],$_POST['cc']);
						$ponePrefijo = '';
						if($disponible>0)
						{
							$ponePrefijo = "\"ponprefijo('$row[3]$row[0]', '$row[1]', '$row[2]', '$disponible', '$unprinart', '$nombreBodega')\"";
							echo"
							<tr class='$co' onClick=$ponePrefijo style='text-transform:uppercase'>
								<td>$row[3]$row[0]</td>
								<td>$row[1]</td>
								<td>$grupinv</td>
								<td style='text-align:right;'>".number_format($disponible,2,'.',',')."</td>
								<td>$unprinart</td>
								<td>".$_POST['bodega']."</td>
								<td>".$_POST['cc']."</td>
							</tr>";
							$aux = $co;
							$co = $co2;
							$co2 = $aux;
						}
					}
					echo"
						</table>";
			?>
			</div>
			<input type="hidden" name="numtop" id="numtop" value="<?php echo $_POST['numtop'];?>">
 		</form>
	</body>
</html>
