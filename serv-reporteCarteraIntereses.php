<?php
	header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=utf8");
	require 'comun.inc';
	require 'funciones.inc';
	session_start();
	$linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");
	cargarcodigopag($_GET['codpag'],$_SESSION["nivel"]);
	date_default_timezone_set("America/Bogota");
	titlepag();
?>
<!DOCTYPE >
<html lang=es>
	<head>
		<meta charset="utf-8"/>
		<meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1"/>
		<meta name="viewport" content="user-scalable=no">
		<title>:: IDEAL 10 - Servicios Públicos</title>
		<link href="css/css2z.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
		<link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
		<link href="css/css4.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
		<link href="css/tabs.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css"/>
		<script src="vue/vue.min.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script src="css/programas.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script src="css/calendario.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<style>
			input[type=number]::-webkit-inner-spin-button,
			input[type=number]::-webkit-outer-spin-button { 
				-webkit-appearance: none; 
				margin: 0; 
			}
			input[type=number]{
				text-align:right;
				box-sizing: border-box;
				font: 120% sans-serif;
				width: 100% !important;
			}
			[v-cloak]{display : none;}
		</style>
		<script src="sweetalert2/dist/sweetalert2.min.js"></script>
		<link rel="stylesheet" href="sweetalert2/dist/sweetalert2.min.css">
	</head>
	<body>
		<header>
			<table>
				<tr><script>barra_imagenes("serv");</script><?php cuadro_titulos();?></tr>
			</table>
		</header>
		<form name="form2" method="post" action="">
			<section id="myapp" v-cloak >
				<nav>
					<table>
						<tr><?php menu_desplegable("serv");?></tr>
						<tr>
							<td colspan="3" class="cinta">
								<img src="imagenes/add2.png"  class="mgbt1" title="Nuevo">
								<img src="imagenes/guardad.png" title="Guardar"  class="mgbt1">
								<img src="imagenes/buscad.png" class="mgbt1" title="Buscar">
								<img src="imagenes/nv.png" onClick="mypop=window.open('serv-principal.php','',''); mypop.focus();" class="mgbt" title="Nueva Ventana">
								<img src="imagenes/excel.png" title="Excel" class="mgbt" v-on:click="excel">
								<a href="serv-menuInformes"><img src="imagenes/iratras.png" class="mgbt" alt="Atrás"></a>
							</td>
						</tr>
					</table>
				</nav>
				<article>
					<table class="inicio ancho">
						<tr>
							<td class="titulos" colspan="6" >Informe de cartera - capital e intereses por usuario</td>
							<td class="cerrar" style="width:7%" onClick="location.href='serv-principal.php'">Cerrar</td>
						</tr>

						<tr>
							<td class="tamano01" style="width: 10%;">Periodo liquidado: </td>
							<td style="width: 15%;">
								<input type="text" v-model="descripcionPeriodo" style="width: 100%; height: 30px; text-align:center;" class="colordobleclik" v-on:dblclick="periodosLiquidados" autocomplete="off">
							</td>

							<td colspan="2" style="padding-bottom:0px">
								<em class="botonflechaverde" v-on:Click="buscarInformacionUsurios">Generar Informe</em>
							</td>
						</tr>
					</table>

					<div class='subpantalla' style='height:66vh; width:99.2%; margin-top:0px; overflow-x:hidden;'>
						<table>
							<thead>
								<tr style="text-align:Center;">
									<th class="titulosnew00" style="width:8%;">Código usuario</th>
									<th class="titulosnew00" style="width:6%;">Documento</th>
									<th class="titulosnew00" >Nombre completo</th>
									<th class="titulosnew00" style="width:8%;">Estrato</th>
									<th class="titulosnew00" style="width:8%;">Barrio</th>
									<th class="titulosnew00" style="width:8%;">AC Capital</th>
                                    <th class="titulosnew00" style="width:8%;">AC Interes</th>
									<th class="titulosnew00" style="width:8%;">AlC Capital</th>
                                    <th class="titulosnew00" style="width:8%;">AlC Interes</th>
									<th class="titulosnew00" style="width:8%;">Aseo Capital</th>
                                    <th class="titulosnew00" style="width:8%;">Aseo Interes</th>
									<th class="titulosnew00" style="width:6%;">Mora</th>
									<th class="titulosnew00" style="width:8%;">Totalizado</th>
								</tr>
							</thead>
							<tbody>
								<tr v-for="(datos, index) in datosUsuarios" v-bind:class="index % 2 ? 'saludo1a' : 'saludo2'"  style='text-rendering: optimizeLegibility; cursor: pointer !important;'>
									<td style="width:8%; font: 120% sans-serif; padding-left:5px; text-align:center;">{{ datos[0] }}</td>
									<td style="width:6%; font: 120% sans-serif; padding-left:5px; text-align:center;">{{ datos[1] }}</td>
									<td style="font: 120% sans-serif; padding-left:5px; text-align:justify;">{{ datos[2] }}</td>
									<td style="width:8%; font: 120% sans-serif; padding-left:5px; text-align:justify;">{{ datos[3] }}</td>
									<td style="width:8%; font: 120% sans-serif; padding-left:5px; text-align:justify;">{{ datos[12] }}</td>
									<td style="width:8%; font: 120% sans-serif; padding-left:5px; text-align:right;">{{ formatonumero(datos[4]) }}</td>
                                    <td style="width:8%; font: 120% sans-serif; padding-left:5px; text-align:right;">{{ formatonumero(datos[5]) }}</td>
									<td style="width:8%; font: 120% sans-serif; padding-left:5px; text-align:right;">{{ formatonumero(datos[6]) }}</td>
                                    <td style="width:8%; font: 120% sans-serif; padding-left:5px; text-align:right;">{{ formatonumero(datos[7]) }}</td>
									<td style="width:8%; font: 120% sans-serif; padding-left:5px; text-align:right;">{{ formatonumero(datos[8]) }}</td>
                                    <td style="width:8%; font: 120% sans-serif; padding-left:5px; text-align:right;">{{ formatonumero(datos[9]) }}</td>
									<td style="width:6%; font: 120% sans-serif; padding-left:5px; text-align:right;">{{ datos[10] }}</td>
									<td style="width:8%; font: 120% sans-serif; padding-left:5px; text-align:right;">{{ formatonumero(datos[11]) }}</td>

									<input type='hidden' name='codigoUsuario[]' v-model="datos[0]">	
									<input type='hidden' name='documento[]' v-model="datos[1]">	
									<input type='hidden' name='nombreCompleto[]' v-model="datos[2]">	
									<input type='hidden' name='estrato[]' v-model="datos[3]">	
									<input type='hidden' name='direccion[]' v-model="datos[12]">	
									<input type='hidden' name='acueductoC[]' v-model="datos[4]">	
                                    <input type='hidden' name='acueductoI[]' v-model="datos[5]">	
									<input type='hidden' name='alcantarilladoC[]' v-model="datos[6]">	
                                    <input type='hidden' name='alcantarilladoI[]' v-model="datos[7]">	
									<input type='hidden' name='aseoC[]' v-model="datos[8]">	
                                    <input type='hidden' name='aseoI[]' v-model="datos[9]">	
									<input type='hidden' name='mora[]' v-model="datos[10]">	
									<input type='hidden' name='total[]' v-model="datos[11]">	
								</tr>
							</tbody>
						</table>
					</div>

					<div id="cargando" v-if="loading" class="loading">
						<span>Cargando...</span>
					</div>

					<div v-show="showModalLiquidaciones">
						<transition name="modal">
							<div class="modal-mask">
								<div class="modal-wrapper">
									<div class="modal-container2">
										<table class="inicio ancho">
											<tr>
												<td class="titulos" colspan="2" >SELECCIONAR PERIODO LIQUIDADO</td>
												<td class="cerrar" style="width:7%" @click="showModalLiquidaciones = false">Cerrar</td>
											</tr>
											<tr>
												<td class="tamano01" style="width:3cm">PERIODO:</td>
												<td>
													<input type="text" class="form-control" placeholder="Buscar por numero de cuenta" v-on:keyup="" style="width:100%" />
												</td>
											</tr>
										</table>
										<table class='tablamv'>
											<thead>
												<tr style="text-align:center;">
													<th class="titulosnew02" >Resultados Busqueda</th>
												</tr>
												<tr style="text-align:center;">
													<th class="titulosnew00" style="width:8.5%;">Número de corte liquidado</th>
													<th class="titulosnew00">Descripcion</th>
													<th class="titulosnew00" style="width:15%;">Fecha inicial</th>
													<th class="titulosnew00" style="width:15%;">Fecha final</th>
												</tr>
											</thead>
											<tbody>
												<tr v-for="(periodo,index) in datosPeriodosLiquidados" v-on:click="seleccionaPeriodo(periodo)" v-bind:class="index % 2 ? 'saludo1a' : 'saludo2'" style='text-rendering: optimizeLegibility; cursor: pointer !important;'>
													<td style="font: 120% sans-serif; padding-left:10px; width:8.5%; text-align:center;">{{ periodo[0] }}</td>
													<td style="font: 120% sans-serif; padding-left:10px; text-align:center;">{{ periodo[1] }}</td>
													<td style="font: 120% sans-serif; padding-left:10px; width:15%; text-align:center;">{{ periodo[2] }}</td>
													<td style="font: 120% sans-serif; padding-left:10px; width:15%; text-align:center;">{{ periodo[3] }}</td>
												</tr>
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</transition>
					</div>

				</article>
			</section>
		</form>

		<script src="Librerias/vue/vue.min.js"></script>
		<script src="Librerias/vue/axios.min.js"></script>
		<script src="servicios_publicos/reporte_cartera/serv-reporteCarteraIntereses.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
        
	</body>
</html>