<?php
	header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=utf8");

	require "comun.inc";
	require "funciones.inc";

	$linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");

	session_start();
    date_default_timezone_set("America/Bogota");
?>
<!DOCTYPE >
<html lang="es">
	<head>
		<meta charset="utf-8"/>
		<meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1"/>
		<meta name="viewport" content="user-scalable=no">
		<title>:: IDEAL 10 - Informes</title>
		<link href="favicon.ico" rel="shortcut icon"/>
        <link href="css/css2.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
		<link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="css/calendario.js"></script>
		<script type="text/javascript" src="css/programas.js"></script>
	</head>
	<body>
		<IFRAME src="alertas.php" name="alertas" id="alertas" style="display:none"></IFRAME>
		<span id="todastablas2"></span>
		<table>
    		<tr><script>barra_imagenes("info");</script><?php cuadro_titulos();?></tr>
    		<tr><?php menu_desplegable("info");?></tr>
			<tr>
  				<td colspan="3" class="cinta">
					<a class="mgbt"><img src="imagenes/add2.png"/></a>
					<a class="mgbt"><img src="imagenes/guardad.png"/></a>
					<a class="mgbt"><img src="imagenes/buscad.png"/></a>
					<a onClick="mypop=window.open('plan-agenda.php','','');mypop.focus()" class="mgbt"><img src="imagenes/agenda1.png" title="Agenda" /></a>
					<a href="#" onClick="mypop=window.open('info-principal.php','','');mypop.focus();" class="mgbt"><img src="imagenes/nv.png" title="Nueva ventana"></a>
                    <a onClick="mypop=window.open('<?php echo $url2; ?>','','');mypop.focus();" class="tooltip bottom mgbt"><img src="imagenes/duplicar_pantalla.png" title="Duplicar pesta&ntilde;a">
				</td>
          	</tr>
      	</table>
        <form name="form2" method="post" action="">
            <table class="inicio" align="center" width="80%" >
                <tr>
                    <td class="titulos" colspan="2">.: Par&aacute;metros Exogena </td>
                    <td style="width:7%" class="cerrar" ><a href="cont-principal.php">Cerrar</a></td>
                </tr>
                <tr>
					<td class='saludo1' width='70%'>
						<ol id="lista2">
                            <li onClick="location.href='cont-conceptosExogenaCarga.php'" style="cursor:pointer;">Carga de información base</li>
                            <li onClick="location.href='cont-formatosExogenaBuscar.php'" style="cursor:pointer;">Crear Formatos</li>
                            <li onClick="location.href='cont-conceptosExogenaCBuscar.php'" style="cursor:pointer;">Crear Conceptos</li>
                            <li onClick="location.href='cont-columnasExogenaBuscar.php'" style="cursor:pointer;">Crear Columnas calculadas</li>
							<li onClick="location.href='cont-conceptosExogenaBuscar.php'" style="cursor:pointer;">Configurar conceptos creados</li>
							<li onClick="location.href='cont-parametrosExogenaBuscar.php'" style="cursor:pointer;">Configurar formatos creados</li>
                            <li onClick="location.href='cont-generarExogena.php'" style="cursor:pointer;">Generar exógena</li>
						</ol>
					</td>
					<td colspan="2" rowspan="1" style="background:url(imagenes/dian.png); background-repeat:no-repeat; background-position:center; background-size: 100% 100%"></td>
				</tr>

            </table>
        </form>
	</body>
</html>
