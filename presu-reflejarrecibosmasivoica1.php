<?php
	header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=utf8");
	require "comun.inc";
	require "funciones.inc";
	session_start();
	$linkbd_v7 = conectar_v7();
	$linkbd_v7 -> set_charset("utf8");
	date_default_timezone_set("America/Bogota");
?>
<!DOCTYPE >
<html lang="es">
	<head>
		<meta charset="utf-8"/>
		<meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1"/>
		<meta name="viewport" content="user-scalable=no">
		<title>:: IDEAL 10 - Administraci&oacute;n</title>
		<link href="favicon.ico" rel="shortcut icon"/>
		<link href="css/css2z.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" /> 
		<link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
		<link href="css/css4.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
		<link href="sweetalert2/dist/sweetalert2.min.css" rel="stylesheet">
		<script src="sweetalert2/dist/sweetalert2.min.js"></script>
		<script src="css/programas.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script src="css/calendario.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script>
			function buscar(){
				var fechaini = document.getElementById("fc_1198971545").value;
				var fechafin = document.getElementById("fc_1198971546").value;
				if(fechaini != '' && fechafin != ''){
					document.form2.oculto.value = '3';
					startProgressAndSubmit();
				}else{
					Swal.fire({
						icon: 'error',
						title: 'Error!',
						text: 'Debe existir una fecha inicial y una fecha final',
						confirmButtonText: 'Continuar',
						confirmButtonColor: '#FF121A',
						timer: 2500
					});
				}
			}
			function reflejar(){
				let ntl = document.getElementById('tfail').value;
				if(ntl > 0){
					document.form2.oculto.value = '4';
					document.form2.submit();
				}else{
					Swal.fire({
						icon: 'error',
						title: 'Error!',
						text: 'No existen recibos para reflejar',
						confirmButtonText: 'Continuar',
						confirmButtonColor: '#FF121A',
						timer: 2500
					});
				}
			}
			function startProgressAndSubmit() {
				let progressBar = document.getElementById("progress-bar");
				progressBar.style.width = "0%";
				fetch("presu-reflejarrecibosmasivoica.php")
					.then(response => response.text())
					.then(data => {
						progressBar.style.display = "none";
						console.log("Proceso completado.");
						document.form2.submit();
					})
					.catch(error => {
						console.error("Error:", error);
					});
			}
			function updateProgress(progress) {
				let progressBar = document.getElementById("progress-bar");
				let progressValue = document.getElementById("progress-value");
				let roundedProgress = Math.round(progress);
				progressBar.style.width = progress + "%";
				progressValue.textContent = roundedProgress + "%";
			}
			function startProgressAndSubmit1() {
				let progressBar = document.getElementById("progress-bar1");
				progressBar.style.width = "0%";
				fetch("process.php")
					.then(response => response.text())
					.then(data => {
						progressBar.style.display = "none";
						console.log("Proceso completado.");
						document.form2.submit();
					})
					.catch(error => {
						console.error("Error:", error);
					});
			}
			function updateProgress1(progress1) {
				let progressBar = document.getElementById("progress-bar1");
				let progressValue = document.getElementById("progress-value1");
				let roundedProgress = Math.round(progress1);
				progressBar.style.width = progress1 + "%";
				progressValue.textContent = roundedProgress + "%";
			}
			function direcReciboCaja(idCat){
				window.open("teso-recibocajaver.php?idrecibo="+idCat);
			}
		</script>
		<style>
			.progress-container {
				width: 80%;
				background-color: #f0f0f0;
				border-radius: 5px;
				overflow: hidden;
				box-shadow: 0px 1px 3px rgba(0, 0, 255, 0.5);
			}
			.progress-bar1{
				width: 0;
				height: 30px;
				background-image: url('imagenes/damascus-pattern4.png'); 
				background-repeat: repeat-x;
				background-size: auto 100%;
				transition: width 0.5s ease-in-out, transform 0.5s ease-in-out;
				transform-origin: left;
				border-top-right-radius: 5px;
				border-bottom-right-radius: 5px;
				position: relative;
			}
			.progress-bar {
				width: 0;
				height: 30px;
				background-image: url('imagenes/damascus-pattern3.png'); 
				background-repeat: repeat-x;
				background-size: auto 100%;
				transition: width 0.5s ease-in-out, transform 0.5s ease-in-out;
				transform-origin: left;
				border-top-right-radius: 5px;
				border-bottom-right-radius: 5px;
				position: relative;
			}
			.progress-bar::before,
			.progress-bar::after {
				content: '';
				position: absolute;
				top: 0;
				bottom: 0;
				width: 10px;
				background-color: transparent; 
				transform-origin: left;
				transition: transform 0.5s ease-in-out;
			}
			.progress-bar::before {
				left: -5px;
			}
			.progress-bar::after {
				right: -5px;
				transform-origin: right;
			}
			.progress-value {
				position: absolute;
				top: 50%;
				left: 50%;
				transform: translate(-50%, -50%);
				color: #f0f0f0;
				user-select: none;
				pointer-events: none;
			}
		</style>
	</head>
	<body>
		<IFRAME src="alertas.php" name="alertas" id="alertas" style="display:none"></IFRAME>
		<span id="todastablas2"></span>
		<table>
			<tr><script>barra_imagenes("adm");</script><?php cuadro_titulos();?></tr>	 
			<tr><?php menu_desplegable("adm");?></tr>
			<tr>
				<td colspan="3" class="cinta">
					<img src="imagenes/add.png" title="Nuevo" onClick="location.href='presu-reflejarrecibosmasivoica.php'" class="mgbt"/>
					<img src="imagenes/guarda.png" title="Guardar" onClick="" class="mgbt"/>
					<img src="imagenes/busca.png" title="Buscar" onClick="document.form2.submit();" class="mgbt"/>
					<img src="imagenes/nv.png" title="Nueva Ventana" class="mgbt" onClick="mypop=window.open('adm-principal.php','',''); mypop.focus();">
					<img src="imagenes/excel.png" title="Excell" onclick="crearexcel()" class="mgbt">
					<img src="imagenes/iratras.png" title="Atr&aacute;s" onClick="location.href='adm-comparacomprobantes-presu.php'" class="mgbt"/>
				</td>
			</tr>	
		</table>
		<form name="form2" method="post" action="">  
			<table class="inicio ancho" >
				<tr>
					<td class="titulos" colspan="7">:: Buscar .: Recibos de Caja ICA</td>
					<td class="cerrar" style='width:7%' onClick="location.href='cont-principal.php'">Cerrar</td>
					<input type="hidden" name="oculto" id="oculto" value="<?php echo $_POST['oculto']; ?>">
					<input type="hidden" name="iddeshff" id="iddeshff" value="<?php echo $_POST['iddeshff'];?>">	 
				</tr>                       
				<tr>
					<td class="saludo1" style='width:3.5cm'>Fecha Inicial: </td>
					<td style='width:15%'><input type="text" name="fechaini" id="fc_1198971545" title="DD/MM/YYYY" value="<?php echo $_POST['fechaini'];?>" class="colordobleclik" onClick="displayCalendarFor('fc_1198971545');" autocomplete="off" onChange="" readonly></td>
					<td class="saludo1" style='width:3.5cm'>Fecha Final: </td>
					<td style='width:15%'><input type="text" name="fechafin" id="fc_1198971546" title="DD/MM/YYYY" value="<?php echo $_POST['fechafin']; ?>" class="colordobleclik" onClick="displayCalendarFor('fc_1198971546');" autocomplete="off" onChange="" readonly></td>
					<td style='width:15%'><em class="botonflecha" onClick="buscar()">Buscar</em></td>
					<td style='width:15%'>
						<div class="progress-container">
							<div class="progress-bar" id="progress-bar">
								<div class="progress-value" id="progress-value">0%</div>
							</div>
						</div>
					</td>
					<td style='width:15%'>
						<div class="progress-container">
							<div class="progress-bar1" id="progress-bar1">
								<div class="progress-value" id="progress-value1">0%</div>
							</div>
						</div>
					</td>
				</tr>
			</table>
			<div class='subpantalla' style='height:66vh; width:99.2%; margin-top:0px; overflow:hidden'>
				<table class='tablamv'>
					<thead>
						<tr style="text-align:Center;">
							<th class="titulosnew02" style="width:76%;">Tesoreria</th>
							<th class="titulosnew03" style="width:14%;">Presupuesto</th>
							<th class="titulosnew06" onclick="reflejar()">Reflejar</th>
						</tr>
						<tr style="text-align:Center;">
							<th class="titulosnew00" style="width:4%;">Estado</th>
							<th class="titulosnew00" style="width:6%;">Código</th>
							<th class="titulosnew00" style="width:6%;">Fecha</th>
							<th class="titulosnew00" style="width:50%;">Descripción</th>
							<th class="titulosnew00" style="width:10%;">Valor</th>
							<th class="titulosnew01" style="width:4%;">Estado</th>
							<th class="titulosnew01" style="width:10%;">Valor</th>
							<th class="titulosnew04" >Diferencia</th>
						</tr>
					</thead>
					<tbody>
						<?php
							if($_POST['oculto']==3){
								unset($_POST['estado1cc']);
								unset($_POST['estado2cc']);
								unset($_POST['fechacc']);
								unset($_POST['vigenciacc']);
								unset($_POST['recaudocc']);
								unset($_POST['conceptocc']);
								unset($_POST['valtotaltescc']);
								unset($_POST['valtotalcontcc']);
								unset($_POST['diferenciacc']);
								unset($_POST['liquidaicacc']);
								$xy = 0;
								preg_match("/([0-9]{2})\/([0-9]{2})\/([0-9]{4})/", $_POST['fechaini'],$fecha);
								$fechaInicial = "$fecha[3]-$fecha[2]-$fecha[1]";
								preg_match("/([0-9]{2})\/([0-9]{2})\/([0-9]{4})/", $_POST['fechafin'],$fecha);
								$fechaFinal = "$fecha[3]-$fecha[2]-$fecha[1]";
								$sqlr = "SELECT id_recibos, valor, estado, id_comp, descripcion, tipo, vigencia, fecha, id_recaudo FROM tesoreciboscaja WHERE estado != 'N' AND tipo = '2' AND fecha >= '$fechaInicial' AND fecha <= '$fechaFinal' GROUP BY id_recibos";
								$resp = mysqli_query($linkbd_v7, $sqlr);
								$ntr = mysqli_num_rows($resp);
								while ($row = mysqli_fetch_row($resp)){
									$xy++;
									$progress = ($xy / $ntr) * 100;
									echo "<script>updateProgress($progress);</script>";
									flush();
									usleep(10000);
									$sql = "SELECT SUM(valor) FROM pptorecibocajappto WHERE idrecibo = '$row[0]' AND cuenta <> '' AND valor > 0";
									$rs = mysqli_query($linkbd_v7, $sql);
									$rw = mysqli_fetch_row($rs);
									if($rw[0] != null){
										$dif = $row[1] - round($rw[0]);
										$difround = round($dif,-1);
										//if ($difround != 0)
                                        {
											$_POST['estado1cc'][] = $row[2];
											$_POST['estado2cc'][] = 'S';
											$_POST['fechacc'][] = $row[7];
											$_POST['vigenciacc'][] = $row[6];
											$_POST['recaudocc'][] = $row[0];
											$_POST['conceptocc'][] = $row[4];
											$_POST['valtotaltescc'][] = $row[1];
											$_POST['valtotalcontcc'][] = $rw[0];
											$_POST['diferenciacc'][] = $difround;
											$_POST['liquidaicacc'][] = $row[8];
										}
									}else{
										$_POST['estado1cc'][] = $row[2];
										$_POST['estado2cc'][] = 'N';
										$_POST['fechacc'][] = $row[7];
										$_POST['vigenciacc'][] = $row[6];
										$_POST['recaudocc'][] = $row[0];
										$_POST['conceptocc'][] = $row[4];
										$_POST['valtotaltescc'][] = $row[1];
										$_POST['valtotalcontcc'][] = 0;
										$_POST['diferenciacc'][] = $row[1];
										$_POST['liquidaicacc'][] = $row[8];
									}
								} 				
							}
							$iter = 'saludo1b';
							$iter2 = 'saludo2b';
							$ntr2 = count($_POST['recaudocc']);
							for($k = 0; $k < $ntr2; $k++){
								if($_POST['oculto']==4){
									$valtesoreria = $_POST['valtotaltescc'][$k];
									$numliquidacion = $_POST['liquidaicacc'][$k];
									$industria = $avisos = $bomberil = 0;
									$saldo_industria = $saldo_avisos = $saldo_bomberil = 0;
									$interesesind = $interesesavi = $interesesbom = 0;
									$saldo_interesesind = $saldo_interesesavi = $saldo_interesesbom = 0;
									$intereses = $retenciones = $sanciones = $saldoafavor = $antivigac = $antivigant = $saldopagar = $descuentotal = $descuenindus = $descuenavisos = $descuenbomberil = 0;
									$sqlr = "SELECT tipoingreso, vigencia FROM tesoindustria WHERE id_industria = '$numliquidacion'";
									$res = mysqli_query($linkbd_v7, $sqlr);
									$row = mysqli_fetch_row($res);
									$tipoingreso = $row[0];
									$vigencia = $row[1];
									$sqlr="SELECT * FROM tesoindustria_det WHERE id_industria = '$numliquidacion'";
									$res = mysqli_query($linkbd_v7, $sqlr);
									$row = mysqli_fetch_row($res);
									$numrecibo = $_POST['recaudocc'][$k];
									$industria = $row[1];
									$avisos = $row[2];
									$bomberil = $row[3];
									$retenciones = (float)$row[4] + (float)$row[19];
									$sanciones = $row[5];	
									$saldoafavor = $row[20];
									$intereses = $row[25];
									$saldo_interesesind = $interesesind = $row[26];
									$saldo_interesesavi = $interesesavi = $row[27];
									$saldo_interesesbom = $interesesbom = $row[28];	
									$antivigact = $row[11];		
									$antivigant = $row[10];
									$saldopagar = $row[8];
									$descuentotal = $row[21];
									$descuenindus = $row[22];
									$descuenavisos = $row[23];
									$descuenbomberil = $row[24];
									$validadescuentos = (float)$descuenindus + (float)$descuenavisos + (float)$descuenbomberil;
									$saldoadescontar = $retenciones + $antivigact + $saldoafavor;
									if( $descuentotal == $validadescuentos){
										$saldo_industria = (float)$industria - (float)$descuenindus;
										if($saldo_industria < 0){
											$saldo_interesesind = (float)$interesesind + (float)$saldo_industria;
											$saldo_industria = 0;
										}
										$saldo_avisos = (float)$avisos - (float)$descuenavisos;
										if($saldo_avisos < 0){
											$saldo_interesesavi = (float)$interesesavi + (float)$saldo_avisos;
											$saldo_avisos = 0;
										}
										$saldo_bomberil = (float)$bomberil - (float)$descuenbomberil;
										if($saldo_bomberil < 0){
											$saldo_interesesbom = (float)$interesesbo + (float)$saldo_bomberil;
											$saldo_bomberil = 0;
										}
									}else{
										$saldo_industria = (float)$industria - (float)$descuentotal;
										if($saldo_industria < 0){
											$saldo_interesesind = (float)$interesesind + (float)$saldo_industria;
											$saldo_industria = 0;
											if($saldo_interesesind < 0){
												$saldo_avisos = (float)$avisos + (float)$saldo_interesesind;
												$saldo_interesesind = 0;
												if($saldo_avisos < 0){
													$saldo_interesesavi = (float)$interesesavi + (float)$saldo_avisos;
													$saldo_avisos = 0;
													if($saldo_interesesavi < 0){
														$saldo_bomberil = (float)$bomberil + (float)$saldo_interesesavi;
														$saldo_interesesavi = 0;
														if($saldo_bomberil < 0){
															$saldo_interesesbom = (float)$interesesbo + (float)$saldo_bomberil;
															$saldo_bomberil = 0;
														}
													}
												}
											}
										}
									}
									if($saldo_industria > 0){
										$saldo_industria = (float)$saldo_industria - (float)$saldoadescontar;
										if($saldo_industria < 0){
											$saldo_interesesind = (float)$saldo_interesesind + (float)$saldo_industria;
											$saldo_industria = 0;
											if($saldo_interesesind < 0){
												$saldo_avisos = (float)$saldo_avisos + (float)$saldo_interesesind;
												$saldo_interesesind = 0;
												if($saldo_avisos < 0){
													$saldo_interesesavi = (float)$saldo_interesesavi + (float)$saldo_avisos;
													$saldo_avisos = 0;
													if($saldo_interesesavi < 0){
														$saldo_bomberil = (float)$saldo_bomberil + (float)$saldo_interesesavi;
														$saldo_interesesavi = 0;
														if($saldo_bomberil < 0){
															$saldo_interesesbom = (float)$saldo_interesesbom + (float)$saldo_bomberil;
															$saldo_bomberil = 0;
														}
													}
												}
											}
										}
									}else if($saldo_interesesind > 0){
										$saldo_interesesind = (float)$saldo_interesesind - (float)$saldoadescontar;
										if($saldo_interesesind < 0){
											$saldo_avisos = (float)$saldo_avisos + (float)$saldo_interesesind;
											$saldo_interesesind = 0;
											if($saldo_avisos < 0){
												$saldo_interesesavi = (float)$saldo_interesesavi + (float)$saldo_avisos;
												$saldo_avisos = 0;
												if($saldo_interesesavi < 0){
													$saldo_bomberil = (float)$saldo_bomberil + (float)$saldo_interesesavi;
													$saldo_interesesavi = 0;
													if($saldo_bomberil < 0){
														$saldo_interesesbom = (float)$saldo_interesesbom + (float)$saldo_bomberil;
														$saldo_bomberil = 0;
													}
												}
											}
										}
									}else if($saldo_avisos > 0){
										$saldo_avisos = (float)$saldo_avisos - (float)$saldoadescontar;
										if($saldo_avisos < 0){
											$saldo_interesesavi = (float)$saldo_interesesavi + (float)$saldo_avisos;
											$saldo_avisos = 0;
											if($saldo_interesesavi < 0){
												$saldo_bomberil = (float)$saldo_bomberil + (float)$saldo_interesesavi;
												$saldo_interesesavi = 0;
												if($saldo_bomberil < 0){
													$saldo_interesesbom = (float)$saldo_interesesbom + (float)$saldo_bomberil;
													$saldo_bomberil = 0;
												}
											}
										}
									}else if($saldo_interesesavi > 0){
										$saldo_interesesavi = (float)$saldo_interesesavi - (float)$saldoadescontar;
										if($saldo_interesesavi < 0){
											$saldo_bomberil = (float)$saldo_bomberil + (float)$saldo_interesesavi;
											$saldo_interesesavi = 0;
											if($saldo_bomberil < 0){
												$saldo_interesesbom = (float)$saldo_interesesbom + (float)$saldo_bomberil;
												$saldo_bomberil = 0;
											}
										}
									}else if($saldo_bomberil > 0){
										$saldo_bomberil = (float)$saldo_bomberil - (float)$saldoadescontar;
										if($saldo_bomberil < 0){
											$saldo_interesesbom = (float)$saldo_interesesbom + (float)$saldo_bomberil;
											$saldo_bomberil = 0;
										}
									}
									$totalglobal = (float)$saldo_industria + (float)$saldo_avisos + (float)$saldo_bomberil + (float)$saldo_interesesind + (float)$saldo_interesesavi + (float)$saldo_interesesbom + (float)$sanciones;
									$valredondeo = (float)$valtesoreria - (float)$totalglobal;
									if($saldo_industria > 0){
										$saldo_industria = (float)$saldo_industria + (float)$valredondeo;
										
									}else if($saldo_interesesind > 0){
										$saldo_interesesind = (float)$saldo_interesesind  +(float)$valredondeo;;
										
									}else if($saldo_avisos > 0){
										$saldo_avisos = (float)$saldo_avisos + (float)$valredondeo;;
										
									}else if($saldo_interesesavi > 0){
										$saldo_interesesavi = (float)$saldo_interesesavi + (float)$valredondeo;
										
									}else if($saldo_bomberil > 0){
										$saldo_bomberil = (float)$saldo_bomberil + (float)$valredondeo;
									}else if($saldo_bomberil > 0){
										$saldo_interesesbom = (float)$saldo_interesesbom + (float)$valredondeo;
									}

									$sqlr = "DELETE FROM pptorecibocajappto WHERE idrecibo = '$numrecibo'";
									mysqli_query($linkbd_v7, $sqlr);
									$sqlr = "SELECT concepto, cuentapres FROM tesoingresos_ica_det WHERE codigo = '$tipoingreso' AND vigencia = (SELECT MAX(vigencia) FROM tesoingresos_ica_det WHERE codigo = '$tipoingreso') ORDER BY concepto";
									$res = mysqli_query($linkbd_v7,$sqlr);
									while($row = mysqli_fetch_row($res)){
										switch($row[0]){
											case '00': {
												if($sanciones > 0){
													$fuenteIngreso = consularFuenteIngresos($row[1], $vigencia);
													$sqlt = "INSERT INTO pptorecibocajappto (cuenta, idrecibo, valor, vigencia, ingreso, fuente, seccion_presupuestal, medio_pago, vigencia_gasto) VALUES ('$row[1]', '$numrecibo','$sanciones','$vigencia', '00', '$fuenteIngreso', '16', 'CSF', '1')";
													mysqli_query($linkbd_v7, $sqlt);
												}
											}break;
											case '04': {
												if($saldo_industria > 0){
													$fuenteIngreso = consularFuenteIngresos($row[1], $vigencia);
													$sqlt = "INSERT INTO pptorecibocajappto (cuenta, idrecibo, valor, vigencia, ingreso, fuente, seccion_presupuestal, medio_pago, vigencia_gasto) VALUES ('$row[1]', '$numrecibo','$saldo_industria','$vigencia', '04', '$fuenteIngreso', '16', 'CSF', '1')";
													mysqli_query($linkbd_v7, $sqlt);
												}
											}break;
											case '05': {
												if($saldo_avisos > 0){
													$fuenteIngreso = consularFuenteIngresos($row[1], $vigencia);
													$sqlt = "INSERT INTO pptorecibocajappto (cuenta, idrecibo, valor, vigencia, ingreso, fuente, seccion_presupuestal, medio_pago, vigencia_gasto) VALUES ('$row[1]', '$numrecibo','$saldo_avisos','$vigencia', '05', '$fuenteIngreso', '16', 'CSF', '1')";
													mysqli_query($linkbd_v7, $sqlt);
												}
											}break;
											case '06': {
												if($saldo_bomberil > 0){
													$fuenteIngreso = consularFuenteIngresos($row[1], $vigencia);
													$sqlt = "INSERT INTO pptorecibocajappto (cuenta, idrecibo, valor, vigencia, ingreso, fuente, seccion_presupuestal, medio_pago, vigencia_gasto) VALUES ('$row[1]', '$numrecibo','$saldo_bomberil','$vigencia', '06', '$fuenteIngreso', '16', 'CSF', '1')";
													mysqli_query($linkbd_v7, $sqlt);
												}
											}break;
											case 'P04': {
												if($saldo_interesesbom > 0){
													$fuenteIngreso = consularFuenteIngresos($row[1], $vigencia);
													$sqlt = "INSERT INTO pptorecibocajappto (cuenta, idrecibo, valor, vigencia, ingreso, fuente, seccion_presupuestal, medio_pago, vigencia_gasto) VALUES ('$row[1]', '$numrecibo','$saldo_interesesbom','$vigencia', 'P04', '$fuenteIngreso', '16', 'CSF', '1')";
													mysqli_query($linkbd_v7, $sqlt);
												}
											}break;
											case 'P16': {
												if($saldo_interesesind > 0){
													$fuenteIngreso = consularFuenteIngresos($row[1], $vigencia);
													$sqlt = "INSERT INTO pptorecibocajappto (cuenta, idrecibo, valor, vigencia, ingreso, fuente, seccion_presupuestal, medio_pago, vigencia_gasto) VALUES ('$row[1]', '$numrecibo','$saldo_interesesind','$vigencia', 'P16', '$fuenteIngreso', '16', 'CSF', '1')";
													mysqli_query($linkbd_v7, $sqlt);
												}
											}break;
											case 'P17': {
												if($saldo_interesesavi > 0){
													$fuenteIngreso = consularFuenteIngresos($row[1], $vigencia);
													$sqlt = "INSERT INTO pptorecibocajappto (cuenta, idrecibo, valor, vigencia, ingreso, fuente, seccion_presupuestal, medio_pago, vigencia_gasto) VALUES ('$row[1]', '$numrecibo','$saldo_interesesavi','$vigencia', 'P17', '$fuenteIngreso', '16', 'CSF', '1')";
													mysqli_query($linkbd_v7, $sqlt);
												}
											}break;
										}
									}
								}
								echo "
										<input type='hidden' name='estado1cc[]' value='".$_POST['estado1cc'][$k]."'/>
										<input type='hidden' name='estado2cc[]' value='".$_POST['estado2cc'][$k]."'/>
										<input type='hidden' name='fechacc[]' value='".$_POST['fechacc'][$k]."'/>
										<input type='hidden' name='vigenciacc[]' value='".$_POST['vigenciacc'][$k]."'/>
										<input type='hidden' name='recaudocc[]' value='".$_POST['recaudocc'][$k]."'/>
										<input type='hidden' name='conceptocc[]' value='".$_POST['conceptocc'][$k]."'/>
										<input type='hidden' name='valtotaltescc[]' value='".$_POST['valtotaltescc'][$k]."'/>
										<input type='hidden' name='valtotalcontcc[]' value='".$_POST['valtotalcontcc'][$k]."'/>
										<input type='hidden' name='diferenciacc[]' value='".$_POST['diferenciacc'][$k]."'/>
										<input type='hidden' name='liquidaicacc[]' value='".$_POST['liquidaicacc'][$k]."'/>
										<tr class='$iter' style='text-transform:uppercase;' ondblclick='direcReciboCaja(".$_POST['recaudocc'][$k].")'>
											<td style='width:4%;text-align:center;'>".$_POST['estado1cc'][$k]."</td>
											<td style='width:6%;text-align:center;'>".$_POST['recaudocc'][$k]."</td>
											<td style='width:6%;text-align:center;'>".$_POST['fechacc'][$k]."</td>
											<td style='width:51%;'>".$_POST['conceptocc'][$k]."</td>
											<td style='text-align:right;width:10%;'>$".number_format($_POST['valtotaltescc'][$k],2,',','.')."</td>
											<td style='width:4%;text-align:center;'>".$_POST['estado2cc'][$k]."</td>
											<td style='text-align:right;width:10%;'>$".number_format($_POST['valtotalcontcc'][$k],2,',','.')."</td>
											<td  style='text-align:right;' >$".number_format($_POST['diferenciacc'][$k],2,',','.')."</td>
										</tr>";
								$aux = $iter;
								$iter = $iter2;
								$iter2 = $aux;
								$resultadoSuma = 0.0;
								$progress1 = (($k + 1) / $ntr2) * 100;
								echo "<script>updateProgress1($progress1);</script>";
								flush();
								usleep(10000);
							}
						?>
					</tbody>
				</table>
				<input type="hidden" name="tfail" id="tfail" value="<?php echo $ntr2 ?>"/>
			</div>
		</form> 
	</body>
</html>