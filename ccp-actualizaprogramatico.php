<?php
	header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=utf8");
	require "comun.inc";
	require "funciones.inc";
	sesion();
	$linkbd=conectar_v7();
	$linkbd -> set_charset("utf8");
	cargarcodigopag($_GET[codpag],$_SESSION["nivel"]);
	date_default_timezone_set("America/Bogota");
?>
<!DOCTYPE >
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="es">
	<head>
		<meta http-equiv="Content-type" content="text/html; charset=utf8"/>
		<meta http-equiv="X-UA-Compatible" content="IE=9"/>
		<title>:: Ideal - Presupuesto</title>
		<link href="css/css2.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css"/>
		<link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" type="text/css"/>
		<script type="text/javascript" src="css/programas.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
		<script>
		function actualizartabla()
		{
			document.form2.oculto.value='3';
			document.form2.submit();
		}
		function callprogress(vValor)
			{
				document.getElementById("getprogress").innerHTML = vValor;
				document.getElementById("getProgressBarFill").innerHTML = '<div class="ProgressBarFill" style="width: '+vValor+'%;"></div>';
				//document.getElementById("titulog1").style.display='block';
				document.getElementById("progreso").style.display='block';
				document.getElementById("getProgressBarFill").style.display='block';
			}
		</script>
		<?php titlepag();?>
	</head>
	<body>
		<IFRAME src="alertas.php" name="alertas" id="alertas" style="display:none"></IFRAME>
		<span id="todastablas2"></span>
		<table>
			<tr><script>barra_imagenes("ccpet");</script><?php cuadro_titulos();?></tr>	 
			<tr><?php menu_desplegable("ccpet");?></tr>
			<tr>
				<td colspan="3" class="cinta"><img src="imagenes/add2.png" class="mgbt1"/><img src="imagenes/guardad.png" class="mgbt1" style="width:24px;"/><img src="imagenes/buscad.png" class="mgbt1"/><img src="imagenes/nv.png" title="Nueva Ventana" onClick="mypop=window.open('ccp-principal.php','','');mypop.focus();" class="mgbt"/></td>
			</tr>
		</table>
		<form name="form2" id="form2" method="post" enctype="multipart/form-data">
			<?php
				if($_POST['oculto']=='')
				{
					$_SESSION['nombrearchivo']='';
				}
			?>
			<table class="inicio">
				<tr>
					<td class="titulos" colspan="7">.: Actualizar Programatico de Inversi&oacute;n </td>
					<td class="cerrar" style="width:7%" onClick="location.href='ccp-principal.php'">Cerrar</td>
				</tr>
				<tr>
					<td class="tamano01" style="width:2.5cm;">Tipo Doc.:</td>
					<td style="width:15%;">
						<select name="tipodoc" id="tipodoc" style="width:98%;height:30px;">
							<option value="-1">Seleccione ....</option>
							<option value="1" <?php if ($_POST['tipodoc'] == '1') echo "selected" ?>>CUIPO_PROGRAMAT_MGA1.txt </option>
							<option value="2" <?php if ($_POST['tipodoc'] == '2') echo "selected" ?>>CUIPO_PROGRAMAT_MGA2.txt </option>
							<option value="3" <?php if ($_POST['tipodoc'] == '3') echo "selected" ?>>CUIPO_CPC.txt </option>
							<option value="4" <?php if ($_POST['tipodoc'] == '4') echo "selected" ?>>CUIPO_FUENTES_FINANC.txt </option>
						</select>
					</td>
					<td class="tamano01" style="width:10%;height:31px;">:&middot;Documentos:</td>
					<td><input type="text" name="nomarch" id="nomarch" style="width:100%" value="<?php echo @$_POST['nomarch'];?>" readonly class="tamano02"/></td>
					<td >
						<div class='upload' style="height: 23px; width:30%;">
							<input type="file" name="plantillaad" onChange="document.form2.submit();" style='cursor:pointer;' title='Cargar Documento' />
							<img  src='imagenes/upload01.png' style="width:22px"/>
						</div>
					</td>
					<td style="padding-bottom:0px"><em class="botonflecha" onClick="actualizartabla();">Cargar</em></td>
					<td style="width:10%;">
					<?php
						echo"
						<div id='progreso' class='ProgressBar' style='display:none; float:left'>
							<div class='ProgressBarText'><span id='getprogress'></span>&nbsp;% </div>
							<div id='getProgressBarFill'></div>
						</div>
						";
					?>
					</td>
				</tr>
			</table>
			<input type='hidden' name='oculto' id='oculto' value='1'/>
			<?php
				if (@ is_uploaded_file($_FILES['plantillaad']['tmp_name'])) 
				{
					unlink("files/".$_FILES['plantillaad']['name']);
					copy($_FILES['plantillaad']['tmp_name'],"files/".$_FILES['plantillaad']['name']);
					if (file_exists("files/".$_FILES['plantillaad']['name']))
					{
						echo"<script>document.getElementById('nomarch').value='".$_FILES['plantillaad']['name']."';</script>";
					}
					else
					{
						echo"<script>despliegamodalm('visible','2','No Se pudo cargar el archivo, favor intentar de nuevo');</script>";
					}
				}
				if($_POST['oculto'] == '3')
				{
					if($_POST['nomarch']!= '')
					{
						$c=0;
						$archivo = fopen("files/".$_POST['nomarch'], "r");
						unset($contenido);
						while(!feof($archivo))
						{
							$traer = fgets($archivo);
							$linead = explode('	',$traer);
							switch ($_POST['tipodoc'])
							{
								case "1":
								{
									$lineasola=explode('.', $linead[0]);
									$codigo2 = explode(' - ', $linead[1]);
									$codigo3 = explode('.', $codigo2[0]);
									$primeroscuatro=$codigo3[0].$codigo3[1];
									$contenido[$primeroscuatro] = $lineasola[0];
								}break;
								case "2":
								{
									$lineasola=explode('.', $linead[0]);
									$codigo2 = explode(' - ', $linead[1]);
									$codigo3 = explode('.', $codigo2[0]);
									$primeroscuatro=$codigo3[0].$codigo3[1].$codigo3[2];
									$contenido[$primeroscuatro] = $lineasola[0];
								}break;
								case "3":
								case "4":
								{
									$lineasola=explode('.', $linead[0]);
									$codigo2 = explode(' - ', $linead[1]);
									$contenido[$codigo2[0]] = $lineasola[0];
								}break;
							}
						}
						fclose($archivo);
						unlink("files/".$_POST['nomarch']);
						switch ($_POST['tipodoc'])
						{
							case "1":
							{
								$sqlmax = "SELECT MAX(version) FROM ccpetproductos";
								$resmax = mysqli_query($linkbd, $sqlmax);
								$rowmax = mysqli_fetch_row($resmax);
								$maxVersion = $rowmax[0];
								$sql = "SELECT cod_producto,id FROM ccpetproductos WHERE version = '$maxVersion' ORDER BY id";
								$res = mysqli_query($linkbd,$sql);
								$totalcli=mysqli_num_rows($res);
								while ($row = mysqli_fetch_row($res))
								{
									$c++;
									$numcod = substr($row[0],0,4);
									$sqlup = "UPDATE ccpetproductos SET concuipom1 = '".$contenido[$numcod]."' WHERE id = '$row[1]'";
									$resup = mysqli_query($linkbd,$sqlup);
									$porcentaje = $c * 100 / $totalcli;
									echo"
									<script>
										progres='".round($porcentaje)."';callprogress(progres);
									</script>";
									flush();
									ob_flush();
									usleep(5);
								}
							}break;
							case "2":
							{
								$sqlmax = "SELECT MAX(version) FROM ccpetproductos";
								$resmax = mysqli_query($linkbd, $sqlmax);
								$rowmax = mysqli_fetch_row($resmax);
								$maxVersion = $rowmax[0];
								$sql = "SELECT cod_producto,id FROM ccpetproductos WHERE version = '$maxVersion' ORDER BY id";
								$res = mysqli_query($linkbd,$sql);
								$totalcli=mysqli_num_rows($res);
								while ($row = mysqli_fetch_row($res))
								{
									$c++;
									$sqlup = "UPDATE ccpetproductos SET concuipom2 = '".$contenido[$row[0]]."' WHERE id = '$row[1]'";
									$resup = mysqli_query($linkbd,$sqlup);
									$porcentaje = $c * 100 / $totalcli;
									echo"
									<script>
										progres='".round($porcentaje)."';callprogress(progres);
									</script>";
									flush();
									ob_flush();
									usleep(5);
								}
							}break;
							case "3":
							{
								$sqlmax = "SELECT MAX(version) FROM ccpetbienestransportables";
								$resmax = mysqli_query($linkbd, $sqlmax);
								$rowmax = mysqli_fetch_row($resmax);
								$maxVersion = $rowmax[0];
								$sql = "SELECT cpc,id FROM ccpetbienestransportables WHERE version = '$maxVersion' AND cpc <> '' ORDER BY id";
								$res = mysqli_query($linkbd,$sql);
								$totalcli=mysqli_num_rows($res);
								while ($row = mysqli_fetch_row($res))
								{
									$c++;
									$sqlup = "UPDATE ccpetbienestransportables SET concuipocpc = '".$contenido[$row[0]]."' WHERE id = '$row[1]'";
									$resup = mysqli_query($linkbd,$sqlup);
									$porcentaje = $c * 100 / $totalcli;
									echo"
									<script>
										progres='".round($porcentaje)."';callprogress(progres);
									</script>";
									flush();
									ob_flush();
									usleep(5);
								}
								$c=0;
								$sqlmax = "SELECT MAX(version) FROM ccpetservicios";
								$resmax = mysqli_query($linkbd, $sqlmax);
								$rowmax = mysqli_fetch_row($resmax);
								$maxVersion = $rowmax[0];
								$sql = "SELECT cpc,id FROM ccpetservicios WHERE version = '$maxVersion' AND cpc <> '' ORDER BY id";
								$res = mysqli_query($linkbd,$sql);
								$totalcli=mysqli_num_rows($res);
								while ($row = mysqli_fetch_row($res))
								{
									$c++;
									$sqlup = "UPDATE ccpetservicios SET concuipocpc = '".$contenido[$row[0]]."' WHERE id = '$row[1]'";
									$resup = mysqli_query($linkbd,$sqlup);
									$porcentaje = $c * 100 / $totalcli;
									echo"
									<script>
										progres='".round($porcentaje)."';callprogress(progres);
									</script>";
									flush();
									ob_flush();
									usleep(5);
								}
							}break;
							case "4":
							{
								$sqlmax = "SELECT MAX(version) FROM ccpet_fuentes_cuipo";
								$resmax = mysqli_query($linkbd, $sqlmax);
								$rowmax = mysqli_fetch_row($resmax);
								$maxVersion = $rowmax[0];
								$sql = "SELECT codigo_fuente,id FROM ccpet_fuentes_cuipo WHERE version = '$maxVersion' AND  LENGTH(codigo_fuente) > 6 ORDER BY id";
								$res = mysqli_query($linkbd,$sql);
								$totalcli=mysqli_num_rows($res);
								while ($row = mysqli_fetch_row($res))
								{
									$c++;
									$sqlup = "UPDATE ccpet_fuentes_cuipo SET concuipocpc = '".$contenido[$row[0]]."' WHERE id = '$row[1]'";
									$resup = mysqli_query($linkbd,$sqlup);
									$porcentaje = $c * 100 / $totalcli;
									echo"
									<script>
										progres='".round($porcentaje)."';callprogress(progres);
									</script>";
									flush();
									ob_flush();
									usleep(5);
								}
							}
						}
						echo"<script>document.getElementById('nomarch').value='';</script>";
					}
				}
			?>
			
		</form>
	</body>
</html>