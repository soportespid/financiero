<?php
header("Cache-control: no-cache, no-store, must-revalidate");
header("Content-Type: text/html;charset=utf8");

require "comun.inc";
require "funciones.inc";
require "conversor.php";
require "validaciones.inc";

$linkbd = conectar_v7();
$linkbd->set_charset("utf8");

session_start();
date_default_timezone_set("America/Bogota");

$scroll = $_GET['scrtop'];
$totreg = $_GET['totreg'];
$idcta = $_GET['idcta'];
$altura = $_GET['altura'];
$filtro = "'" . $_GET['filtro'] . "'";
?>
<!DOCTYPE>
<html lang="es">

<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1" />
    <meta name="viewport" content="user-scalable=no">
    <title>:: IDEAL 10 - Tesorer&iacute;a</title>
    <link href="favicon.ico" rel="shortcut icon" />
    <link href="css/css2.css?<?php echo date('d_m_Y_h_i_s'); ?>" rel="stylesheet" type="text/css" />
    <link href="css/css3.css?<?php echo date('d_m_Y_h_i_s'); ?>" rel="stylesheet" type="text/css" />
    <link href="css/style.css?<?php echo date('d_m_Y_h_i_s'); ?>" rel="stylesheet" type="text/css" />
    <link href="css/tabs.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="css/programas.js"></script>
    <script type="text/javascript" src="css/calendario.js"></script>
    <script>
        function buscacta(e) { if (document.form2.cuenta.value != "") { document.form2.bc.value = '1'; document.form2.submit(); } }
        function validar() {
            var x = document.getElementById("tipomov").value;
            document.form2.movimiento.value = x;
            document.form2.submit();
        }

        function buscarp(e) { if (document.form2.rp.value != "") { document.form2.brp.value = '1'; document.form2.submit(); } }
        function agregardetalle() {
            if (document.form2.numero.value != "" && document.form2.valor.value > 0 && document.form2.banco.value != "") {
                document.form2.agregadet.value = 1;
                //document.form2.chacuerdo.value=2;
                document.form2.submit();
            }
            else { alert("Falta informacion para poder Agregar"); }
        }
        function agregardetalled() {
            if (document.form2.retencion.value != "" && document.form2.vporcentaje.value != "") {
                document.form2.agregadetdes.value = 1;
                //document.form2.chacuerdo.value=2;
                document.form2.submit();
            }
            else { alert("Falta informacion para poder Agregar"); }
        }
        function eliminar(variable) {
            if (confirm("Esta Seguro de Eliminar")) {
                document.form2.elimina.value = variable;
                //eli=document.getElementById(elimina);
                vvend = document.getElementById('elimina');
                //eli.value=elimina;
                vvend.value = variable;
                document.form2.submit();
            }
        }
        function eliminard(variable) {
            if (confirm("Esta Seguro de Eliminar")) {
                document.form2.eliminad.value = variable;
                //eli=document.getElementById(elimina);
                vvend = document.getElementById('eliminad');
                //eli.value=elimina;
                vvend.value = variable;
                document.form2.submit();
            }
        }
        /*function guardar()
        {
            if (document.form2.fecha.value!='')
                {if (confirm("Esta Seguro de Guardar")){document.form2.oculto.value=2;document.form2.submit();}}
            else
            {
                alert('Faltan datos para completar el registro');
                document.form2.fecha.focus();
                document.form2.fecha.select();
            }
        }*/
        function calcularpago() {
            //alert("dddadadad");
            valorp = document.form2.valor.value;
            descuentos = document.form2.totaldes.value;
            valorc = valorp - descuentos;
            document.form2.valorcheque.value = valorc;
            document.form2.valoregreso.value = valorp;
            document.form2.valorretencion.value = descuentos;
            document.form2.submit();
        }
        function pdfMunicipal() {
            document.form2.action = "pdfcxp.php";
            document.form2.target = "_BLANK";
            document.form2.submit();
            document.form2.action = "";
            document.form2.target = "";
        }
        function pdfDepartamental() {
            document.form2.action = "obligacionPdf.php";
            document.form2.target = "_BLANK";
            document.form2.submit();
            document.form2.action = "";
            document.form2.target = "";
        }
        function adelante(scrtop, numpag, limreg, filtro) {
            //alert("Balance Descuadrado");
            //document.form2.oculto.value=2;
            if (parseFloat(document.form2.ncomp.value) < parseFloat(document.form2.maximo.value)) {
                document.form2.oculto.value = 1;
                //document.form2.agregadet.value='';
                //document.form2.elimina.value='';
                document.form2.ncomp.value = parseFloat(document.form2.ncomp.value) + 1;
                document.form2.idcomp.value = parseFloat(document.form2.idcomp.value) + 1;
                var idcta = document.getElementById('idcomp').value;
                document.form2.action = "teso-egresoverccpet.php?idcta=" + idcta + "&scrtop=" + scrtop + "&numpag=" + numpag + "&limreg=" + limreg + "&filtro=" + filtro;
                document.form2.submit();
            }
        }
        function atrasc(scrtop, numpag, limreg, filtro) {
            //document.form2.oculto.value=2;
            if (document.form2.ncomp.value > 1) {
                document.form2.oculto.value = 1;
                //document.form2.agregadet.value='';
                //document.form2.elimina.value='';
                document.form2.ncomp.value = document.form2.ncomp.value - 1;
                document.form2.idcomp.value = document.form2.idcomp.value - 1;
                var idcta = document.getElementById('idcomp').value;
                document.form2.action = "teso-egresoverccpet.php?idcta=" + idcta + "&scrtop=" + scrtop + "&numpag=" + numpag + "&limreg=" + limreg + "&filtro=" + filtro;
                document.form2.submit();
            }
        }
        function iratras(scrtop, numpag, limreg, filtro) {
            var idcta = document.getElementById('idcomp').value;
            var inicio = document.getElementById('fechaini').value;
            var fin = document.getElementById('fechafin').value;

            location.href = "teso-buscaegresoccpet.php?idcta=" + idcta + "&scrtop=" + scrtop + "&numpag=" + numpag + "&limreg=" + limreg + "&filtro=" + filtro + "&fini=" + inicio + "&ffin=" + fin;
        }
    </script>
</head>

<body>
    <IFRAME src="alertas.php" name="alertas" id="alertas" style="display:none"></IFRAME>
    <span id="todastablas2"></span>
    <?php $numpag = $_GET['numpag'];
    $limreg = $_GET['limreg'];
    $scrtop = 27 * $totreg; ?>
    <table>
        <tr>
            <script>barra_imagenes("teso");</script><?php cuadro_titulos(); ?>
        </tr>
        <tr><?php menu_desplegable("teso"); ?></tr>
    </table>
    <div class="bg-white group-btn p-1">
        <button type="button" onclick="window.location.href='teso-egreso-ccpet.php'" class="btn btn-white btn-primary-hover d-flex justify-between align-items-center">
            <span>Nuevo</span>
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 -960 960 960">
                <path d="M440-440H200v-80h240v-240h80v240h240v80H520v240h-80v-240Z"></path>
            </svg>
        </button>
        <button type="button" onclick="window.location.href='teso-buscaegresoccpet.php'" class="btn btn-white btn-primary-hover d-flex justify-between align-items-center">
            <span>Buscar</span>
            <svg class="fill-black group-hover:fill-white w-5 h-5" xmlns="http://www.w3.org/2000/svg" viewBox="0 -960 960 960">
                <path d="M784-120 532-372q-30 24-69 38t-83 14q-109 0-184.5-75.5T120-580q0-109 75.5-184.5T380-840q109 0 184.5 75.5T640-580q0 44-14 83t-38 69l252 252-56 56ZM380-400q75 0 127.5-52.5T560-580q0-75-52.5-127.5T380-760q-75 0-127.5 52.5T200-580q0 75 52.5 127.5T380-400Z"></path>
            </svg>
        </button>
        <button type="button" onclick="mypop=window.open('plan-agenda.php','','');mypop.focus()"class="btn btn-white btn-primary-hover d-flex justify-between align-items-center">
            <span class="group-hover:text-white">Agenda</span>
            <svg class="fill-black group-hover:fill-white w-5 h-5" xmlns="http://www.w3.org/2000/svg" viewBox="0 -960 960 960">
                <path d="M560-564v-68q33-14 67.5-21t72.5-7q26 0 51 4t49 10v64q-24-9-48.5-13.5T700-600q-38 0-73 9.5T560-564Zm0 220v-68q33-14 67.5-21t72.5-7q26 0 51 4t49 10v64q-24-9-48.5-13.5T700-380q-38 0-73 9t-67 27Zm0-110v-68q33-14 67.5-21t72.5-7q26 0 51 4t49 10v64q-24-9-48.5-13.5T700-490q-38 0-73 9.5T560-454ZM260-320q47 0 91.5 10.5T440-278v-394q-41-24-87-36t-93-12q-36 0-71.5 7T120-692v396q35-12 69.5-18t70.5-6Zm260 42q44-21 88.5-31.5T700-320q36 0 70.5 6t69.5 18v-396q-33-14-68.5-21t-71.5-7q-47 0-93 12t-87 36v394Zm-40 118q-48-38-104-59t-116-21q-42 0-82.5 11T100-198q-21 11-40.5-1T40-234v-482q0-11 5.5-21T62-752q46-24 96-36t102-12q58 0 113.5 15T480-740q51-30 106.5-45T700-800q52 0 102 12t96 36q11 5 16.5 15t5.5 21v482q0 23-19.5 35t-40.5 1q-37-20-77.5-31T700-240q-60 0-116 21t-104 59ZM280-494Z"></path>
            </svg>
        </button>
        <button type="button" onclick="window.open('teso-principal');"
            class="btn btn-white btn-primary-hover d-flex justify-between align-items-center">
            <span>Nueva ventana</span>
            <svg class="fill-black group-hover:fill-white w-5 h-5" xmlns="http://www.w3.org/2000/svg" viewBox="0 -960 960 960">
                <path d="M320-120v-80h80v-80H160q-33 0-56.5-23.5T80-360v-400q0-33 23.5-56.5T160-840h640q33 0 56.5 23.5T880-760v400q0 33-23.5 56.5T800-280H560v80h80v80H320ZM160-360h640v-400H160v400Zm0 0v-400 400Z"></path>
            </svg>
        </button>
        <button type="button"
            onclick="mypop=window.open('/financiero/teso-egresoverccpet.php','','');mypop.focus();"
            class="btn btn-white btn-primary-hover d-flex justify-between align-items-center">
            <span class="group-hover:text-white">Duplicar pantalla</span>
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 -960 960 960">
                <path
                    d="M640-160v-360H160v360h480Zm80-200v-80h80v-360H320v200h-80v-200q0-33 23.5-56.5T320-880h480q33 0 56.5 23.5T880-800v360q0 33-23.5 56.5T800-360h-80ZM160-80q-33 0-56.5-23.5T80-160v-360q0-33 23.5-56.5T160-600h480q33 0 56.5 23.5T720-520v360q0 33-23.5 56.5T640-80H160Zm400-603ZM400-340Z">
                </path>
            </svg>
        </button>
       
        <?php 
            $sqlr = "SELECT orden FROM configbasica";
            $resr = mysqli_query($linkbd, $sqlr);
            $rowr = mysqli_fetch_array($resr);
            $orden = $rowr['orden'];
            if ($orden == 'Dptal') {
                ?>
                    <button type="button" onclick="pdfDepartamental()" class="btn btn-danger btn-danger-hover d-flex justify-between align-items-center">
            
                        <span>Exportar PDF</span>
                        <svg xmlns="http://www.w3.org/2000/svg"
                            viewBox="0 0 512 512">
                            <path d="M64 464l48 0 0 48-48 0c-35.3 0-64-28.7-64-64L0 64C0 28.7 28.7 0 64 0L229.5 0c17 0 33.3 6.7 45.3 18.7l90.5 90.5c12 12 18.7 28.3 18.7 45.3L384 304l-48 0 0-144-80 0c-17.7 0-32-14.3-32-32l0-80L64 48c-8.8 0-16 7.2-16 16l0 384c0 8.8 7.2 16 16 16zM176 352l32 0c30.9 0 56 25.1 56 56s-25.1 56-56 56l-16 0 0 32c0 8.8-7.2 16-16 16s-16-7.2-16-16l0-48 0-80c0-8.8 7.2-16 16-16zm32 80c13.3 0 24-10.7 24-24s-10.7-24-24-24l-16 0 0 48 16 0zm96-80l32 0c26.5 0 48 21.5 48 48l0 64c0 26.5-21.5 48-48 48l-32 0c-8.8 0-16-7.2-16-16l0-128c0-8.8 7.2-16 16-16zm32 128c8.8 0 16-7.2 16-16l0-64c0-8.8-7.2-16-16-16l-16 0 0 96 16 0zm80-112c0-8.8 7.2-16 16-16l48 0c8.8 0 16 7.2 16 16s-7.2 16-16 16l-32 0 0 32 32 0c8.8 0 16 7.2 16 16s-7.2 16-16 16l-32 0 0 48c0 8.8-7.2 16-16 16s-16-7.2-16-16l0-64 0-64z">
                            </path>
                        </svg>
                    </button>
                <?php
            }else{
                ?>
                    <button type="button" onclick="pdfMunicipal()" class="btn btn-danger btn-danger-hover d-flex justify-between align-items-center">
            
                        <span>Exportar PDF</span>
                        <svg xmlns="http://www.w3.org/2000/svg"
                            viewBox="0 0 512 512">
                            <path d="M64 464l48 0 0 48-48 0c-35.3 0-64-28.7-64-64L0 64C0 28.7 28.7 0 64 0L229.5 0c17 0 33.3 6.7 45.3 18.7l90.5 90.5c12 12 18.7 28.3 18.7 45.3L384 304l-48 0 0-144-80 0c-17.7 0-32-14.3-32-32l0-80L64 48c-8.8 0-16 7.2-16 16l0 384c0 8.8 7.2 16 16 16zM176 352l32 0c30.9 0 56 25.1 56 56s-25.1 56-56 56l-16 0 0 32c0 8.8-7.2 16-16 16s-16-7.2-16-16l0-48 0-80c0-8.8 7.2-16 16-16zm32 80c13.3 0 24-10.7 24-24s-10.7-24-24-24l-16 0 0 48 16 0zm96-80l32 0c26.5 0 48 21.5 48 48l0 64c0 26.5-21.5 48-48 48l-32 0c-8.8 0-16-7.2-16-16l0-128c0-8.8 7.2-16 16-16zm32 128c8.8 0 16-7.2 16-16l0-64c0-8.8-7.2-16-16-16l-16 0 0 96 16 0zm80-112c0-8.8 7.2-16 16-16l48 0c8.8 0 16 7.2 16 16s-7.2 16-16 16l-32 0 0 32 32 0c8.8 0 16 7.2 16 16s-7.2 16-16 16l-32 0 0 48c0 8.8-7.2 16-16 16s-16-7.2-16-16l0-64 0-64z">
                            </path>
                        </svg>
                    </button>
                <?php
            }
            ?>

        <button type="button" onclick="iratras(27, 1, 10, '')" class="btn btn-success btn-success-hover d-flex justify-between align-items-center">
            <span>Atras</span>
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 -960 960 960">
                <path d="M760-200v-160q0-50-35-85t-85-35H273l144 144-57 56-240-240 240-240 57 56-144 144h367q83 0 141.5 58.5T840-360v160h-80Z"></path>
            </svg>
        </button>
    </div>
    <form name="form2" method="post" action="">

        <?php

        $vigusu = vigencia_usuarios($_SESSION['cedulausu']);
        $vigencia = $vigusu;
        //*********** cuenta origen va al credito y la destino al debito
        if ($_GET['idopc'] != "") {
            echo "<script>document.getElementById('codrec').value=$_GET[idop];</script>";
        }
        if (!$_POST['oculto']) {
            $fech1 = explode("/", $_GET['fini']);
            $fech2 = explode("/", $_GET['ffin']);
            $_POST['fechaini'] = $fech1[2] . "-" . $fech1[1] . "-" . $fech1[0];
            $_POST['fechafin'] = $fech2[2] . "-" . $fech2[1] . "-" . $fech2[0];
            $sqlr = "select *from cuentapagar where estado='S' ";
            $res = mysqli_query($linkbd, $sqlr);
            while ($row = mysqli_fetch_row($res)) {
                $_POST['cuentapagar'] = $row[1];
            }
            $sqlr = "select * from tesoordenpago ORDER BY id_orden DESC";
            $res = mysqli_query($linkbd, $sqlr);
            $r = mysqli_fetch_row($res);
            $_POST['maximo'] = $r[0];
            if ($_POST['codrec'] != "" || $_GET['idop'] != "") {
                if ($_POST['codrec'] != "") {
                    $sqlr = "select * from tesoordenpago where id_orden='$_POST[codrec]' and tipo_mov='201' ";
                } else {
                    $sqlr = "select * from tesoordenpago where id_orden='$_GET[idop]' and tipo_mov='201' ";
                }
            } else {
                $sqlr = "select * from tesoordenpago ORDER BY id_orden DESC";
            }
            $res = mysqli_query($linkbd, $sqlr);
            $r = mysqli_fetch_row($res);
            $_POST['ncomp'] = $r[0];
            $_POST['idcomp'] = $r[0];
            $_POST['vigencia'] = $r[3];

            $check1 = "checked";
            $fec = date("d/m/Y");
        }
        // $_POST[fecha]=$fec;
        //$_POST[valor]=0;
        //$_POST[valorcheque]=0;
        //$_POST[valorretencion]=0;
        //$_POST[valoregreso]=0;
        //$_POST[totaldes]=0;
        $sqlr = "select * from tesoordenpago where id_orden=" . $_POST['ncomp'] . " and tipo_mov='201' ";
        $res = mysqli_query($linkbd, $sqlr);
        $consec = 0;
        while ($r = mysqli_fetch_row($res)) {
            $_POST['fecha'] = $r[2];
            $_POST['compcont'] = $r[1];
            $consec = $r[0];
            $_POST['rp'] = $r[4];
            $_POST['estado'] = $r[13];
            $_POST['estadoc'] = $r[3];

            $_POST['medioDePago'] = $r[19];
            if ($_POST['medioDePago'] == '')
                $_POST['medioDePago'] = '-1';
        }
        preg_match("/([0-9]{4})\-([0-9]{2})\-([0-9]{2})/", $_POST['fecha'], $fecha);
        $fechaf = $fecha[3] . "/" . $fecha[2] . "/" . $fecha[1];
        $_POST['fecha'] = $fechaf;
        switch ($_POST['tabgroup1']) {
            case 1:
                $check1 = 'checked';
                break;
            case 2:
                $check2 = 'checked';
                break;
            case 3:
                $check3 = 'checked';
                break;
            case 4:
                $check4 = 'checked';
                break;
        }
        if ($_POST['oculto'] != '2') {
            $sqlr = "select * from tesoordenpago where id_orden=$_POST[idcomp] and tipo_mov='201' ";
            $res = mysqli_query($linkbd, $sqlr);
            while ($r = mysqli_fetch_row($res)) {
                $_POST['fecha'] = $r[2];
                $_POST['idcomp'] = $r[0];
                preg_match("/([0-9]{4})\-([0-9]{2})\-([0-9]{2})/", $_POST['fecha'], $fecha);
                $fechaf = $fecha[3] . "/" . $fecha[2] . "/" . $fecha[1];
                $_POST['fecha'] = $fechaf;
                $_POST['rp'] = $r[4];
                $_POST['base'] = $r[14];
                $_POST['iva'] = $r[15];
                $_POST['vigencia'] = $r[3];
                $_POST['estado'] = $r[13];
                $_POST['estadoc'] = $r[13];
                $_POST['medioDePago'] = $r[19];
                if ($_POST['medioDePago'] == '')
                    $_POST['medioDePago'] = '-1';
            }
            $nresul = buscaregistroccpet($_POST['rp'], $_POST['vigencia']);
            $_POST['cdp'] = $nresul;
            //*** busca detalle cdp
            $sqlr = "select ccpetrp.vigencia,ccpetrp.consvigencia,ccpetcdp.objeto,ccpetrp.estado,ccpetcdp.consvigencia,ccpetrp.valor,ccpetrp.saldo, ccpetrp.tercero,ccpetcdp.objeto from ccpetrp, ccpetcdp where ccpetrp.estado='S' and ccpetcdp.consvigencia=$_POST[cdp] and ccpetrp.idcdp=ccpetcdp.consvigencia and ccpetrp.consvigencia=" . $_POST['rp'] . " and ccpetrp.vigencia=$_POST[vigencia] and ccpetrp.idcdp=ccpetcdp.consvigencia and ccpetcdp.vigencia=$_POST[vigencia] and ccpetrp.tipo_mov='201' and ccpetcdp.tipo_mov='201' order by ccpetrp.vigencia,ccpetrp.consvigencia,ccpetcdp.objeto,ccpetrp.estado";



            $resp = mysqli_query($linkbd, $sqlr);
            $row = mysqli_fetch_row($resp);
            $_POST['detallecdp'] = $row[2];
            $sqlr = "Select *from tesoordenpago where id_orden=" . $_POST['idcomp'] . " and tipo_mov='201' ";
            $resp = mysqli_query($linkbd, $sqlr);
            $row = mysqli_fetch_row($resp);
            $_POST['tercero'] = $row[6];
            $_POST['user'] = $row[18];
            $_POST['ntercero'] = buscatercero($_POST['tercero']);
            $_POST['valorrp'] = $row[8];
            $_POST['saldorp'] = generaSaldoRPccpet($_POST['rp'], $_POST['vigencia']);
            //$_POST[cdp]=$row[4];
            $_POST['valor'] = $row[10];
            $_POST['cc'] = $row[5];
            $_POST['detallegreso'] = $row[7];
            $_POST['valoregreso'] = $_POST['valor'];
            $_POST['valorretencion'] = $row[12];
            $_POST['valorcheque'] = $_POST['valoregreso'] - $_POST['valorretencion'];
            $_POST['base'] = $row[14];
            $_POST['iva'] = $row[15];
            if ($_POST['movimiento'] == '401') {
                $sql = "select conceptorden from tesoordenpago where id_orden=$_POST[idcomp] and vigencia=$vigusu and tipo_mov='401' ";
                $resp1 = mysqli_query($linkbd, $sql);
                $row1 = mysqli_fetch_row($resp1);
                $_POST['detallegreso'] = $row1[0];
            }
            //$_POST[valorcheque]=number_format($_POST[valorcheque],2);
        }
        ?>
        <input type="hidden" name="fechaini" id="fechaini" value="<?php echo $_GET['fini']; ?>" />
        <input type="hidden" name="fechafin" id="fechafin" value="<?php echo $_GET['ffin']; ?>" />
        <div class="tabs">
            <div class="tab">
                <input type="radio" id="tab-1" name="tabgroup1" value="1" <?php echo $check1; ?>>
                <label for="tab-1">Liquidacion CxP</label>
                <div class="content" style="overflow-x:hidden;">
                    <table class="inicio" align="center">
                        <tr>
                            <td class="titulos" colspan="7">Liquidacion CxP</td>
                            <td class="cerrar" style='width:7%'><a href="teso-principal.php">&nbsp;Cerrar</a></td>
                        </tr>
                        <tr>
                            <td class="saludo1" style="width:2.6cm;">Numero CxP:</td>
                            <td style="width:20%;">
                                <a href="#"
                                    onClick="atrasc(<?php echo $scrtop; ?>, <?php echo $numpag; ?>, <?php echo $limreg; ?>, <?php echo $filtro; ?>)"><img
                                        src="imagenes/back.png" title="anterior" align="absmiddle"></a>
                                <input type="text" id="idcomp" name="idcomp" value="<?php echo $_POST['idcomp'] ?>"
                                    style="width:40%;" readonly>
                                <input type="hidden" id="ncomp" name="ncomp" value="<?php echo $_POST['ncomp'] ?>">
                                <input type="hidden" name="compcont" value="<?php echo $_POST['compcont'] ?>">
                                <a href="#"
                                    onClick="adelante(<?php echo $scrtop; ?>, <?php echo $numpag; ?>, <?php echo $limreg; ?>, <?php echo $filtro; ?>)"><img
                                        src="imagenes/next.png" title="siguiente" align="absmiddle"></a>
                                <input type="hidden" value="a" name="atras"><input type="hidden" value="s"
                                    name="siguiente">
                                <input type="hidden" value="<?php echo $_POST['maximo'] ?>" name="maximo">
                                <input type="hidden" value="<?php echo $_POST['codrec'] ?>" name="codrec" id="codrec">
                                <input name="user" type="hidden" id="user" value="<?php echo $_POST['user'] ?>"
                                    readonly />
                            </td>
                            <td class="saludo1" style="width:3.1cm;">Fecha: </td>
                            <td style="width:10%;"><input type="text" name="fecha" value="<?php echo $_POST['fecha'] ?>"
                                    style='width:100%' id="fc_1198971545" readonly></td>
                            <td class="saludo1" style="width:3cm;">Medio de pago: </td>
                            <td style="width:17%;">
                                <select name="medioDePago" id="medioDePago" onKeyUp="return tabular(event,this)"
                                    disabled style="width:80%">
                                    <option value="-1" <?php if (($_POST['medioDePago'] == '-1'))
                                        echo "SELECTED"; ?>>
                                        Seleccione...</option>
                                    <option value="1" <?php if (($_POST['medioDePago'] == '1'))
                                        echo "SELECTED"; ?>>Con SF
                                    </option>
                                    <option value="2" <?php if ($_POST['medioDePago'] == '2')
                                        echo "SELECTED"; ?>>Sin SF
                                    </option>
                                </select>
                                <input type="hidden" name="vigencia" value="<?php echo $_POST['vigencia'] ?>"
                                    style='width:50%' readonly>
                                <input type="hidden" name="estadoc" value="<?php echo $_POST['estadoc'] ?>">
                                <input type="hidden" name="estado" value="<?php echo $_POST['estado'] ?>">
                            </td>
                            <td rowspan="7" colspan="2"
                                style="background:url(imagenes/factura03.png); background-repeat:no-repeat; background-position:right; background-size: 90% 95%">
                            </td>
                        </tr>
                        <tr>
                            <td class="saludo1">Registro: </td>
                            <td>
                                <input type="text" name="rp" value="<?php echo $_POST['rp'] ?>" style="width:80%;"
                                    readonly />
                                <input type="hidden" value="0" name="brp" />
                            </td>
                            <td class="saludo1">Estado: </td>
                            <?php
                            if ($_POST['estado'] == "S") {
                                $valuees = "ACTIVO";
                                $stylest = "width:100%; background-color:#0CD02A; color:white; text-align:center;";
                            } else if ($_POST['estado'] == "N") {
                                $valuees = "ANULADO";
                                $stylest = "width:100%; background-color:#FF0000; color:white; text-align:center;";
                            } else if ($_POST['estado'] == "P") {
                                $valuees = "PAGO";
                                $stylest = "width:100%; background-color:#0404B4; color:white; text-align:center;";
                            } else if ($_POST['estado'] == "R") {
                                $valuees = "REVERSADO";
                                $stylest = "width:100%; background-color:#FF0000; color:white; text-align:center;";
                            }

                            echo "<td><input type='text' name='estado1' id='estado1' value='$valuees' style='$stylest' readonly /></td>";
                            ?>

                            <td style="width:3.1cm;" colspan="2">
                                <select name="tipomov" id="tipomov" onKeyUp="return tabular(event,this)"
                                    onChange="validar()">
                                    <?php
                                    $codMovimiento = '201';
                                    if (isset($_POST['movimiento'])) {
                                        if (!empty($_POST['movimiento']))
                                            $codMovimiento = $_POST['movimiento'];
                                    }
                                    $sql = "SELECT tipo_mov FROM tesoordenpago where id_orden=$_POST[idcomp] ORDER BY tipo_mov";

                                    $resultMov = mysqli_query($linkbd, $sql);
                                    $movimientos = array();
                                    $movimientos["201"]["nombre"] = "201-Documento de Creacion";
                                    $movimientos["201"]["estado"] = "";
                                    $movimientos["401"]["nombre"] = "401-Reversion Total";
                                    $movimientos["401"]["estado"] = "";
                                    while ($row = mysqli_fetch_row($resultMov)) {
                                        $mov = $movimientos[$row[0]]["nombre"];
                                        $movimientos[$codMovimiento]["estado"] = "selected";
                                        $state = $movimientos[$row[0]]["estado"];
                                        echo "<option value='$row[0]' $state>$mov</option>";
                                    }
                                    $movimientos[$codMovimiento]["estado"] = "";
                                    echo "<input type='hidden' id='movimiento' name='movimiento' value='$_POST[movimiento]' />";
                                    ?>
                                </select>


                            </td>

                        </tr>
                        <tr>
                            <td class="saludo1">CDP:</td>
                            <td><input type="text" id="cdp" name="cdp" value="<?php echo $_POST['cdp'] ?>"
                                    style="width:80%;" readonly></td>
                            <td class="saludo1">Detalle RP:</td>
                            <td colspan="3"><input type="text" id="detallecdp" name="detallecdp"
                                    value="<?php echo $_POST['detallecdp'] ?>" style='width:100%' readonly></td>
                        </tr>
                        <tr>
                            <td class="saludo1">Centro Costo:</td>
                            <td>
                                <select name="cc" onChange="validar()" onKeyUp="return tabular(event,this)"
                                    style="width:100%;text-transform:uppercase;">
                                    <?php
                                    $sqlr = "select *from centrocosto where estado='S'";
                                    $res = mysqli_query($linkbd, $sqlr);
                                    while ($row = mysqli_fetch_row($res)) {
                                        if ("$row[0]" == $_POST['cc']) {
                                            echo "<option value='$row[0]' SELECTED>$row[0] - $row[1]</option>";
                                        }
                                    }
                                    ?>
                                </select>
                            </td>
                            <td class="saludo1">Tercero:</td>
                            <td><input id="tercero" type="text" name="tercero" style='width:100%'
                                    onKeyUp="return tabular(event,this)" onBlur="buscater(event)"
                                    value="<?php echo $_POST['tercero'] ?>" readonly></td>
                            <td colspan="2"><input id="ntercero" name="ntercero" type="text"
                                    value="<?php echo $_POST['ntercero'] ?>" style='width:100%' readonly></td>
                        </tr>
                        <tr>
                            <td class="saludo1">Detalle CxP:</td>
                            <td colspan="5"><input type="text" id="detallegreso" name="detallegreso"
                                    value="<?php echo $_POST['detallegreso'] ?>" style='width:100%' readonly></td>
                        </tr>
                        <tr>
                            <td class="saludo1">Valor RP:</td>
                            <td><input type="text" id="valorrp" name="valorrp" value="<?php echo $_POST['valorrp'] ?>"
                                    style='width:95%' readonly></td>
                            <td class="saludo1">Saldo RP:</td>
                            <td><input type="text" id="saldorp" name="saldorp" value="<?php echo $_POST['saldorp'] ?>"
                                    style='width:95%' readonly></td>
                        </tr>
                        <tr>
                            <td class="saludo1">Valor a Pagar:</td>
                            <td><input type="text" id="valor" name="valor" value="<?php echo $_POST['valor'] ?>"
                                    style='width:95%' onChange='calcularpago()' readonly> <input type="hidden" value="1"
                                    name="oculto"></td>
                            <td class="saludo1">Base:</td>
                            <td><input type="text" id="base" name="base" value="<?php echo $_POST['base'] ?>"
                                    style='width:95%' onKeyUp="return tabular(event,this)" readonly></td>
                            <td class="saludo1">Iva:</td>
                            <td><input type="text" id="iva" name="iva" value="<?php echo $_POST['iva'] ?>"
                                    style='width:100%' onKeyUp="return tabular(event,this)" onChange='calcularpago()'
                                    readonly> </td>
                        </tr>
                    </table>
                    <?php
                    if (!$_POST['oculto']) {
                        echo "<script>document.form2.fecha.focus();document.form2.fecha.select();</script>";
                    }
                    //***** busca tercero
                    if ($_POST['brp'] == '1') {
                        $nresul = buscaregistroccpet($_POST['rp'], $_POST['vigencia']);
                        if ($nresul != '') {
                            $_POST['cdp'] = $nresul;
                            echo "<script>document.getElementById('cc').focus();document.getElementById('cc').select();</script>";
                        } else {
                            $_POST['cdp'] = "";
                            echo "
			  						<script>
				 						alert('Registro Presupuestal Incorrecto');
				 						document.form2.rp.select();
		  								//document.form2.rp.focus();
			  						</script>";
                        }
                    }
                    ?>
                </div>
            </div>
            <div class="tab">
                <input type="radio" id="tab-2" name="tabgroup1" value="2" <?php echo $check2; ?>>
                <label for="tab-2">Retenciones</label>
                <div class="content" style="overflow-x:hidden;">
                    <table class="inicio" style="overflow:scroll">
                        <tr>
                            <td class="titulos">Descuento</td>
                            <td class="titulos">%</td>
                            <td class="titulos">Valor</td>

                        </tr>
                        <?php
                        $totaldes = 0;
                        $sqlr = "select *from tesoordenpago_retenciones where id_orden=" . $_POST['idcomp'];
                        $res = mysqli_query($linkbd, $sqlr);
                        $iter = 'saludo1a';
                        $iter2 = 'saludo2';
                        while ($row = mysqli_fetch_row($res)) {
                            $sqlr = "select *from tesoretenciones where id='$row[0]'";
                            $res2 = mysqli_query($linkbd, $sqlr);
                            $row2 = mysqli_fetch_row($res2);
                            echo "
									<input type='hidden' name='dndescuentos[]' value='$row2[2]'/ >
									<input type='hidden' name='ddescuentos[]' value='$row[0]'/>
									<input type='hidden' name='dporcentajes[]' value='$row[2]'/>
									<input type='hidden' name='ddesvalores[]' value='$row[3]'/>
									<tr class='$iter' style='text-transform:uppercase' onMouseOver=\"anterior=this.style.backgroundColor;this.style.backgroundColor='#40b3ff';\" onMouseOut=\"this.style.backgroundColor=anterior\">
										<td>$row2[2]</td>
										<td style='text-align:right;'>$row[2] %</td>
										<td style='text-align:right;'>$ $row[3]</td>
									</tr>";
                            //echo "<td class='saludo2'><input name='ddesvalores[]' value='".$_POST[ddesvalores][$x]."' type='text' size='15'></td><td class='saludo2'><a href='#' onclick='eliminard($x)'><img src='imagenes/del.png'></a></td></tr>";
                            $totaldes = $totaldes + $row[3];
                            $aux = $iter;
                            $iter = $iter2;
                            $iter2 = $aux;
                        }
                        echo "
        						<script>
        							document.form2.totaldes.value=<?php echo $totaldes;?>;
									calcularpago();
									//document.form2.valorretencion.value=$totaldes;
        						</script>";
                        ?>
                    </table>
                </div>
            </div>
            <div class="tab">
                <input type="radio" id="tab-3" name="tabgroup1" value="3" <?php echo $check3; ?>>
                <label for="tab-3">Cuenta por Pagar</label>
                <div class="content" style="overflow-x:hidden;">
                    <table class="inicio" align="center">
                        <tr>
                            <td colspan="6" class="titulos">Cheque</td>
                            <td width="108" class="cerrar"><a href="teso-principal.php">Cerrar</a></td>
                        </tr>
                        <tr>
                            <td class="saludo1">Cuenta Contable:</td>
                            <td>
                                <input name="cuentapagar" type="text" value="<?php echo $_POST['cuentapagar'] ?>"
                                    size="25" readonly>
                                <input name="cb" type="hidden" value="<?php echo $_POST['cb'] ?>">
                                <input type="hidden" id="ter" name="ter" value="<?php echo $_POST['ter'] ?>">
                            </td>
                        </tr>
                        <tr>
                            <td class="saludo1">Valor Orden de Pago:</td>
                            <td><input type="text" id="valoregreso" name="valoregreso"
                                    value="<?php echo $_POST['valoregreso'] ?>" size="20"
                                    onKeyUp="return tabular(event,this)" readonly></td>
                            <td class="saludo1">Valor Retenciones:</td>
                            <td><input type="text" id="valorretencion" name="valorretencion"
                                    value="<?php echo $_POST['valorretencion'] ?>" size="20"
                                    onKeyUp="return tabular(event,this)" readonly></td>
                            <td class="saludo1">Valor Cta Pagar:</td>
                            <td><input type="text" id="valorcheque" name="valorcheque"
                                    value="<?php echo $_POST['valorcheque'] ?>" size="20"
                                    onKeyUp="return tabular(event,this)" readonly></td>
                        </tr>
                    </table>
                </div>
            </div>

            <div class="tab">
                <input type="radio" id="tab-4" name="tabgroup1" value="4" <?php echo $check4; ?>>
                <label for="tab-4">Afectacion presupuestal</label>
                <div class="content" style="overflow-x:hidden;">
                    <table class="inicio" style="overflow:scroll">
                        <tr>
                            <td class="titulos" colspan="3">Detalle Comprobantes</td>
                        </tr>
                        <tr>
                            <td class="titulos2">Cuenta</td>
                            <td class="titulos2">Nombre Cuenta</td>
                            <td class="titulos2">Valor</td>
                        </tr>
                        <input type="hidden" id="totaldes" name="totaldes" value="<?php echo $_POST['totaldes'] ?>"
                            readonly>

                        <?php

                        //echo "hola";
                        $totaldes = 0;
                        $_POST['dcuenta'] = array();
                        $_POST['ncuenta'] = array();
                        $_POST['rvalor'] = array();
                        $sqlr = "select *from pptoretencionpago where idrecibo=$_POST[idcomp] and vigencia=$_POST[vigencia] and cuenta!='' and tipo='orden'";
                        //echo $sqlr;
                        $resd = mysqli_query($linkbd, $sqlr);

                        $iter = 'saludo1a';
                        $iter2 = 'saludo2';
                        $cr = 0;
                        while ($rowd = mysqli_fetch_row($resd)) {
                            $nresult = buscaNombreCuentaCCPET($rowd[1], 1);
                            echo "<tr class=$iter>
												<td >
													<input name='dcuenta[]' value='$rowd[1]' type='text' size='20' readonly>
												</td>
												<td >
													<input name='ncuenta[]' value='$nresult' type='text' size='55' readonly>
												</td>
												<td >
													<input name='rvalor[]' value='" . number_format($rowd[3], 2) . "' type='text' size='10' readonly>
												</td>
												</tr>";
                            $var1 = $rowd[3];
                            $var1 = $var1;
                            $cuentavar1 = $cuentavar1 + $var1;
                            $_POST['varto'] = number_format($cuentavar1, 2, ".", ",");
                        }
                        echo "<tr class=$iter><td> </td></tr>";
                        echo "<tr >
											<td ></td>
											<td>Total:</td>
											<td >
												<input name='varto' id='varto' value='$_POST[varto]' size='10' readonly>
											</td>
										 </tr>";

                        ?>
                        <input type='hidden' name='contrete' value="<?php echo $_POST['contrete'] ?>" />
                    </table>

                </div>
            </div>
        </div>
        <div class="subpantallac4" style="height:36.8%; width:99.6%; overflow-x:hidden;">
            <?php
            //*** busca contenido del rp
            $_POST['dseccion'] = [];
            $_POST['dmediopago'] = [];
            $_POST['dvigasto'] = [];
            $_POST['bpim'] = [];
            $_POST['indpro'] = [];
            $_POST['nomCPC'] = [];
            $_POST['dcuentas'] = array();
            $_POST['dncuentas'] = array();
            $_POST['dvalores'] = array();
            $_POST['drecursos'] = array();
            $maxVersion = ultimaVersionGastosCCPET();
            $sqlr = "select * from tesoordenpago_det where id_orden=$_POST[idcomp] and tipo_mov='201' ";
            $res = mysqli_query($linkbd, $sqlr);
            while ($r = mysqli_fetch_row($res)) {
                $consec = $r[0];

                $sqlNomSec = "SELECT nombre FROM pptoseccion_presupuestal WHERE id_seccion_presupuestal = '$r[15]' ";
                $resNomSec = mysqli_query($linkbd, $sqlNomSec);
                $rowNomSec = mysqli_fetch_row($resNomSec);

                $sqlVigencia = "SELECT nombre FROM ccpet_vigenciadelgasto WHERE codigo = '$r[12]'";
                $resVigencia = mysqli_query($linkbd, $sqlVigencia);
                $rowVigencia = mysqli_fetch_row($resVigencia);

                $sqlrNomProy = "SELECT nombre FROM ccpproyectospresupuesto WHERE codigo = '$r[14]'";
                $respNomProy = mysqli_query($linkbd, $sqlrNomProy);
                $rowNomProy = mysqli_fetch_row($respNomProy);

                $sqlrIndPro = "SELECT producto FROM ccpetproductos WHERE codigo_indicador = '$r[10]'";
                $respIndPro = mysqli_query($linkbd, $sqlrIndPro);
                $rowNomIndPro = mysqli_fetch_row($respIndPro);
                $indpro = $rowNomIndPro[0];


                $nomCpc = '';
                $primerDigitoCpc = substr($r[9], 0, 1);
                if ($primerDigitoCpc < 5) {
                    $sqlrBienesT = "SELECT titulo FROM ccpetbienestransportables WHERE grupo = '$r[9]'";
                    $respBienesT = mysqli_query($linkbd, $sqlrBienesT);
                    $rowBienesT = mysqli_fetch_row($respBienesT);
                    $nomCpc = $rowBienesT[0];
                } else {
                    $sqlrServicio = "SELECT titulo FROM ccpetservicios WHERE grupo = '$r[9]'";
                    $respServicio = mysqli_query($linkbd, $sqlrServicio);
                    $rowServicio = mysqli_fetch_row($respServicio);
                    $nomCpc = $rowServicio[0];
                }


                $_POST['dseccion'][] = $r[15] . ' - ' . $rowNomSec[0];
                $_POST['dmediopago'][] = $r[11];
                $_POST['dvigasto'][] = $r[12] . ' - ' . $rowVigencia[0];
                $_POST['dbpim'][] = $r[14] . ' - ' . $rowNomProy[0];
                $_POST['dprogramatico'][] = $r[10] . ' - ' . $indpro;
                $_POST['dcuentas'][] = $r[2] . ' - ' . buscacuentaccpetgastos($r[2], $maxVersion);
                $_POST['dvalores'][] = $r[4];
                $_POST['dCPC'][] = $r[9] . ' - ' . $nomCpc;





                $_POST['drecursos'][] = $r[8] . ' - ' . buscafuenteccpet($r[8]);
                // echo count($_POST[drecursos]);



            }

            ?>
            <table class="inicio">
                <tr>
                    <td colspan="9" class="titulos">Detalle Orden de Pago</td>
                </tr>
                <tr>
                    <td class="titulos2" style='width:8%'>Sec. Presupuestal</td>
                    <td class="titulos2" style='width:5%'>Medio de pago</td>
                    <td class="titulos2" style='width:5%'>Vig. de Gasto</td>
                    <td class="titulos2" style='width:5%'>Proyecto</td>
                    <td class="titulos2" style='width:6%'>Programatico</td>
                    <td class="titulos2" style='width:10%'>CCPET</td>
                    <td class="titulos2" style='width:10%'>Fuente</td>
                    <td class="titulos2" style='width:10%'>CPC</td>
                    <td class="titulos2" style='width:10%'>Valor</td>
                </tr>
                <?php
                $_POST['totalc'] = 0;
                $iter = 'saludo1a';
                $iter2 = 'saludo2';
                for ($x = 0; $x < count($_POST['dseccion']); $x++) {
                    echo "
							<input type='hidden' name='dseccion[]' value='" . $_POST['dseccion'][$x] . "'/>
							<input type='hidden' name='dmediopago[]' value='" . $_POST['dmediopago'][$x] . "'/>
							<input type='hidden' name='dvigasto[]' value='" . $_POST['dvigasto'][$x] . "'/>
							<input type='hidden' name='dbpim[]' value='" . $_POST['dbpim'][$x] . "'/>
							<input type='hidden' name='dprogramatico[]' value='" . $_POST['dprogramatico'][$x] . "'/>
							<input type='hidden' name='dCPC[]' value='" . $_POST['dCPC'][$x] . "'/>
							<input type='hidden' name='dcuentas[]' value='" . $_POST['dcuentas'][$x] . "'/>
							<input type='hidden' name='nomCpc[]' value='" . $_POST['nomCpc'][$x] . "'/>
							<input type='hidden' name='indpro[]' value='" . $_POST['indpro'][$x] . "'/>
							<input type='hidden' name='drecursos[]' value='" . $_POST['drecursos'][$x] . "'/>
							<input type='hidden' name='dvalores[]' value='" . $_POST['dvalores'][$x] . "'/>

							<tr class='$iter'>
								<td>" . $_POST['dseccion'][$x] . "</td>
								<td>" . $_POST['dmediopago'][$x] . "</td>
								<td>" . $_POST['dvigasto'][$x] . "</td>
								<td>" . $_POST['dbpim'][$x] . "</td>
								<td>" . $_POST['dprogramatico'][$x] . "</td>
		 						<td>" . $_POST['dcuentas'][$x] . "</td>

		 						<td >" . $_POST['drecursos'][$x] . "</td>
								 <td>" . $_POST['dCPC'][$x] . "</td>
		 						<td style='text-align:right;'>$ " . number_format($_POST['dvalores'][$x], $_SESSION["ndecimales"], $_SESSION["spdecimal"], $_SESSION["spmillares"]) . "</td>
		 					</tr>";
                    $_POST['totalc'] = $_POST['totalc'] + $_POST['dvalores'][$x];
                    $_POST['totalcf'] = number_format($_POST['totalc'], $_SESSION["ndecimales"], $_SESSION["spdecimal"], $_SESSION["spmillares"]);
                    $aux = $iter;
                    $iter = $iter2;
                    $iter2 = $aux;
                }
                $resultado = convertir($_POST['totalc']);
                $_POST['letras'] = $resultado . " PESOS M/CTE";
                echo "
						<input type='hidden' name='totalcf' value='$_POST[totalcf]'/>
						<input type='hidden' name='totalc' value='$_POST[totalc]'/>
						<input type='hidden' name='letras' value='$_POST[letras]' >
						<tr class='$iter' style='text-align:right;font-weight:bold;'>
							<td colspan='8'>Total:</td>
							<td>$ $_POST[totalcf]</td>
						</tr>
						<tr class='titulos2'>
							<td >Son:</td>
							<td colspan='8'>$_POST[letras]</td>
						</tr>";
                ?>
                <script>
                    document.form2.valor.value = <?php echo $_POST['totalc']; ?>;
                    //calcularpago();
                </script>
            </table>
        </div>
        <?php
        if ($_POST['oculto'] == '2') {
            $scdetalle = eliminar_comillas($_POST['detallegreso']);
            preg_match("/([0-9]{2})\/([0-9]{2})\/([0-9]{4})/", $_POST['fecha'], $fecha);
            $fechaf = $fecha[3] . "-" . $fecha[2] . "-" . $fecha[1];
            $sqlr = "update tesoordenpago set fecha='$fechaf', tercero='$_POST[tercero]', conceptorden='$scdetalle' where id_orden=$_POST[idcomp] and tipo_mov='201' ";
            if (!mysqli_query($linkbd, $sqlr)) {
                echo "<table class='inicio'><tr><td class='saludo1'><center>No Se ha Actualizado la Orden de Pago con Exito <img src='imagenes\alert.png'></center></td></tr></table>";
            } else {
                $sqlr = "update comprobante_cab set fecha='$fechaf',concepto='$scdetalle' where id_comp=$_POST[compcont]";
                mysqli_query($linkbd, $sqlr);
                $sqlr = "update comprobante_det set tercero='$_POST[tercero]' where id_comp='11 $_POST[idcomp]'";
                mysqli_query($linkbd, $sqlr);
                echo "<table class='inicio'><tr><td class='saludo1'><center>Se ha Actualizado la Orden de Pago con Exito <img src='imagenes\confirm.png'></center></td></tr></table>";
            }
            //************CREACION DEL COMPROBANTE CONTABLE ************************
//***busca el consecutivo del comprobante contable
        }//************ FIN DE IF OCULTO************
        ?>
    </form>
    </td>
    </tr>
    </table>
</body>

</html>
