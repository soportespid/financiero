<?php
	header("Cache-control: no-cache, no-store, must-revalidate");
	header("Content-Type: text/html;charset=utf8");

	require "comun.inc";
	require "funciones.inc";

	$linkbd = conectar_v7();
	$linkbd -> set_charset("utf8");

	session_start();
    date_default_timezone_set("America/Bogota");
?>
<!DOCTYPE >
<html lang=es>
	<head>
		<meta charset="utf-8"/>
		<meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1"/>
		<meta name="viewport" content="user-scalable=no">
		<title>:: IDEAL 10 - Contabilidad</title>
		<link href="favicon.ico" rel="shortcut icon"/>
		<link href="css/css2z.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
		<link href="css/css3.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
		<link href="css/css4.css?<?php echo date('d_m_Y_h_i_s');?>" rel="stylesheet" />
		<script src="css/programas.js?<?php echo date('d_m_Y_h_i_s');?>"></script>
	</head>
	<body>
		<header>
			<table>
				<tr><script>barra_imagenes("cont");</script><?php cuadro_titulos();?></tr>	
			</table>
			<nav>
				<table>
					<tr><?php menu_desplegable("cont");?></tr>
					<tr>
						<td colspan="3" class="cinta">
							<img src="imagenes/add2.png" class="mgbt1"/>
							<img src="imagenes/guardad.png" class="mgbt1" style="width:24px;"/>
							<img src="imagenes/buscad.png" class="mgbt1"/>
							<img src="imagenes/nv.png" title="Nueva Ventana" onClick="mypop=window.open('cont-principal.php','','');mypop.focus();" class="mgbt"/>
						</td>
					</tr>
				</table>
			</nav>
			<IFRAME src="alertas.php" name="alertas" id="alertas" style="display:none"></IFRAME>
			<span id="todastablas2"></span>
		</header>
		<section>
			<div style="height:79vh; width:99.9%; overflow-x:hidden;">
				<table class="inicio">
					<tr>
						<td class="titulos" colspan="2">.: Estado comprobantes </td>
						<td class="cerrar" style="width:7%" onClick="location.href='ccp-principal.php'">Cerrar</td>
					</tr>
					<tr id="listamenus" style="vertical-align: top;text-align: left;">
						<td style="background-repeat:no-repeat; background-position:center;">
							<ol >
								<li onClick="location.href='cont-estadoComprobantesEstructura.php'" class='mgbt3'>Estructura</li>
								<li onClick="location.href='cont-estadoComprobantesComparacion.php'" class='mgbt3'>Comparación frente a documento origen</li>	
								<li onClick="location.href='cont-estadoComprobantesTrazabilidad.php'" class='mgbt3'>Trazabilidad</li>
							</ol>
						</td>
					</tr>
				</table>
			</div>
		</form>
	</body>
</html>