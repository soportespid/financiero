<?php
header("Cache-control: no-cache, no-store, must-revalidate");
header("Content-Type: text/html;charset=utf8");
require 'comun.inc';
require 'funciones.inc';
require 'validaciones.inc';
require 'conversor.php';
session_start();
$linkbd = conectar_v7();
$linkbd->set_charset("utf8");
cargarcodigopag($_GET['codpag'], $_SESSION["nivel"]);
date_default_timezone_set("America/Bogota");
?>
<!DOCTYPE>
<html lang=es>

<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1" />
    <meta name="viewport" content="user-scalable=no">
    <title>:: IDEAL 10 - Gesti&oacute;n humana</title>
    <link href="favicon.ico" rel="shortcut icon" />
    <link href="css/css2z.css?<?php echo date('d_m_Y_h_i_s'); ?>" rel="stylesheet" />
    <link href="css/css3.css?<?php echo date('d_m_Y_h_i_s'); ?>" rel="stylesheet" />
    <link href="css/css4.css?<?php echo date('d_m_Y_h_i_s'); ?>" rel="stylesheet" />
    <link href="sweetalert2/dist/sweetalert2.min.css" rel="stylesheet">
    <script src="sweetalert2/dist/sweetalert2.min.js"></script>
    <script src="vue/vue.js?<?php echo date('d_m_Y_h_i_s'); ?>"></script>
    <script src="css/programas.js?<?php echo date('d_m_Y_h_i_s'); ?>"></script>
    <script src="css/calendario.js?<?php echo date('d_m_Y_h_i_s'); ?>"></script>
</head>

<body>
    <header>
        <table>
            <tr>
                <script>barra_imagenes("hum");</script><?php cuadro_titulos(); ?>
            </tr>
        </table>
    </header>
    <section id="myapp" v-cloak>
        <nav>
            <table>
                <tr><?php menu_desplegable("hum"); ?></tr>
            </table>
            <div class="bg-white group-btn p-1"><button type="button" onclick="location.href='hum-aprobarnomina.php'"
                    class="btn btn-white btn-primary-hover d-flex justify-between align-items-center">
                    <span>Nuevo</span>
                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 -960 960 960">
                        <path d="M440-440H200v-80h240v-240h80v240h240v80H520v240h-80v-240Z"></path>
                    </svg>
                </button><button type="button" v-on:click="validarguardar()"
                    class="btn btn-white btn-primary-hover d-flex justify-between align-items-center">
                    <span>Guardar</span>
                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 -960 960 960">
                        <path
                            d="M840-680v480q0 33-23.5 56.5T760-120H200q-33 0-56.5-23.5T120-200v-560q0-33 23.5-56.5T200-840h480l160 160Zm-80 34L646-760H200v560h560v-446ZM480-240q50 0 85-35t35-85q0-50-35-85t-85-35q-50 0-85 35t-35 85q0 50 35 85t85 35ZM240-560h360v-160H240v160Zm-40-86v446-560 114Z">
                        </path>
                    </svg>
                </button><button type="button" onclick="location.href='hum-aprobarnominabuscar.php'"
                    class="btn btn-white btn-primary-hover d-flex justify-between align-items-center">
                    <span>Buscar</span>
                    <svg class="fill-black group-hover:fill-white w-5 h-5" xmlns="http://www.w3.org/2000/svg"
                        viewBox="0 -960 960 960">
                        <path
                            d="M784-120 532-372q-30 24-69 38t-83 14q-109 0-184.5-75.5T120-580q0-109 75.5-184.5T380-840q109 0 184.5 75.5T640-580q0 44-14 83t-38 69l252 252-56 56ZM380-400q75 0 127.5-52.5T560-580q0-75-52.5-127.5T380-760q-75 0-127.5 52.5T200-580q0 75 52.5 127.5T380-400Z">
                        </path>
                    </svg>
                </button><button type="button" onclick="mypop=window.open('hum-principal.php','','');mypop.focus();"
                    class="btn btn-white btn-primary-hover d-flex justify-between align-items-center">
                    <span>Nueva ventana</span>
                    <svg class="fill-black group-hover:fill-white w-5 h-5" xmlns="http://www.w3.org/2000/svg"
                        viewBox="0 -960 960 960">
                        <path
                            d="M320-120v-80h80v-80H160q-33 0-56.5-23.5T80-360v-400q0-33 23.5-56.5T160-840h640q33 0 56.5 23.5T880-760v400q0 33-23.5 56.5T800-280H560v80h80v80H320ZM160-360h640v-400H160v400Zm0 0v-400 400Z">
                        </path>
                    </svg>
                </button><button type="button" onclick="location.href='hum-menunomina.php'"
                    class="btn btn-success btn-success-hover d-flex justify-between align-items-center">
                    <span>Atras</span>
                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 -960 960 960">
                        <path
                            d="M760-200v-160q0-50-35-85t-85-35H273l144 144-57 56-240-240 240-240 57 56-144 144h367q83 0 141.5 58.5T840-360v160h-80Z">
                        </path>
                    </svg>
                </button></div>
        </nav>
        <article>
            <table class="inicio ancho">
                <tr>
                    <td class="titulos" colspan="7">Aprobar Nómina</td>
                    <td class="cerrar" style="width:7%" onClick="location.href='hum-principal.php'">Cerrar</td>
                </tr>
                <tr>
                    <td class="textonew01" style="width:3cm;">N° Aprobación:</td>
                    <td style="width:12%;"><input type="text" v-model="idcomp" style="width: 100%;" readonly></td>
                    <td class="textonew01" style="width:2.2cm;">Fecha:</td>
                    <td style="width:12%;"><input type="text" name="fecha" value="<?php echo $_POST['fecha'] ?>"
                            onKeyUp="return tabular(event,this)" id="fc_1198971545" title="DD/MM/YYYY"
                            onDblClick="displayCalendarFor('fc_1198971545');" class="colordobleclik" autocomplete="off"
                            onChange="" readonly></td>
                    <td class="textonew01" style="width:3cm;">N° Liquidación:</td>
                    <td colspan="2">
                        <select v-model="idliq" style="width:100%;" v-on:change="cargainfo()">
                            <option disabled value="">Seleccione nómina</option>
                            <option v-for="(infonominas, index) in infonomina" :value="index">{{infonominas[0]}} -
                                {{infonominas[3]}}</option>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td class="textonew01">N° CDP:</td>
                    <td><input type="text" v-model="idcdp" style="width: 100%;" readonly></td>
                    <td class="textonew01">N° RP:</td>
                    <td><input type="text" v-model="idrp" style="width: 100%;" readonly></td>
                    <td colspan="3"><input type="text" v-model="desrp" style="width: 100%;" readonly></td>
                </tr>
                <tr>
                    <td class="textonew01">Valor N&oacute;mina:</td>
                    <td><input type="text" v-model="valnomina" style="width: 100%;" class="colordobleclik"
                            autocomplete="off" v-on:DblClick="vernomina();" title='Ver Nómina' readonly></td>
                    <td class="textonew01">Valor CDP:</td>
                    <td><input type="text" v-model="valcdp" style="width: 100%;" class="colordobleclik"
                            autocomplete="off" v-on:DblClick="vercdp();" title='Ver CDP' readonly></td>
                    <td class="textonew01">Valor RP:</td>
                    <td style="width:12%;"><input type="text" v-model="valrp" class="colordobleclik" autocomplete="off"
                            v-on:DblClick="verrp();" style="width: 100%;" title='Ver RP' readonly></td>
                    <td></td>

                </tr>
                <tr>
                    <td class="textonew01">Tercero:</td>
                    <td><input type="text" v-model="vtercero" style="width:100%;" title='Listado Terceros'
                            onKeyPress="javascript:return solonumeros(event)" class="colordobleclik" autocomplete="off"
                            v-on:dblclick='toggleModalTerceros' v-on:change="verificatercero()"></td>
                    <td colspan="4"><input type="text" v-model="vntercero" style="width:100%;" readonly></td>
                    <td></td>
                </tr>
            </table>
            <div v-show="showModalTerceros">
                <transition name="modal">
                    <div class="modal-mask">
                        <div class="modal-wrapper">
                            <div class="modal-container2">
                                <table class="inicio ancho">
                                    <tr>
                                        <td class="titulos" colspan="2">SELECCIONAR TERCERO</td>
                                        <td class="cerrar" style="width:7%" @click="showModalTerceros = false">Cerrar
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="tamano01" style="width:3cm">Documento:</td>
                                        <td><input type="text" class="form-control"
                                                placeholder="Buscar por nombre o documento" v-on:keyup="buscarTerceros"
                                                v-model="searchTercero.keywordTercero" style="width:100%" /></td>
                                    </tr>
                                </table>
                                <table class='tablamv'>
                                    <thead>
                                        <tr style="text-align:Center;">
                                            <th class="titulosnew02">Resultados Busqueda</th>
                                        </tr>
                                        <tr style="text-align:Center;">
                                            <th class="titulosnew00" style="width:6%;">Item</th>
                                            <th class="titulosnew00">Razon Social</th>
                                            <th class="titulosnew00" style="width:15%;">Primer Apellido</th>
                                            <th class="titulosnew00" style="width:15%;">Segundo Apellido</th>
                                            <th class="titulosnew00" style="width:15%;">Primer Nombre</th>
                                            <th class="titulosnew00" style="width:15%;">Segundo Nombre</th>
                                            <th class="titulosnew00" style="width:15%; ">Documento</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr v-for="(infotercero,index) in infoterceros"
                                            v-on:click="ingresarTercero(infotercero[0], infotercero[1], infotercero[2], infotercero[3], infotercero[4], infotercero[5])"
                                            v-bind:class="index % 2 ? 'saludo1a' : 'saludo2'"
                                            style='text-rendering: optimizeLegibility; cursor: pointer !important;'>
                                            <td style="font: 120% sans-serif; padding-left:10px; width:6%;">{{ index + 1
                                                }}</td>
                                            <td style="font: 120% sans-serif; padding-left:10px;">{{ infotercero[0] }}
                                            </td>
                                            <td style="font: 120% sans-serif; padding-left:10px; width:15%;">{{
                                                infotercero[1] }}</td>
                                            <td style="font: 120% sans-serif; padding-left:10px; width:15%;">{{
                                                infotercero[2] }}</td>
                                            <td style="font: 120% sans-serif; padding-left:10px; width:15%;">{{
                                                infotercero[3] }}</td>
                                            <td style="font: 120% sans-serif; padding-left:10px; width:15%;">{{
                                                infotercero[4] }}</td>
                                            <td
                                                style="font: 120% sans-serif; padding-left:10px; width:15%; text-align:right;">
                                                {{ infotercero[5] }}</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </transition>
            </div>
            <div id="cargando" v-if="loading" class="loading" style="z-index: 9999;">
                <span>Cargando...</span>
            </div>
        </article>
        <input type="hidden" v-model="idbuscar" :value="idbuscar=1" />
    </section>
    <script src="Librerias/vue/vue.min.js"></script>
    <script src="Librerias/vue/axios.min.js"></script>
    <script src="vue/gestion_humana/hum-aprobarnomina.js?<?php echo date('d_m_Y_h_i_s'); ?>"></script>
</body>

</html>
