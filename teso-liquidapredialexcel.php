<?php
	require_once 'PHPExcel/Classes/PHPExcel.php';
    include '/PHPExcel/Classes/PHPExcel/IOFactory.php';
	require "comun.inc";
	require "funciones.inc";
    ini_set('max_execution_time',99999999);
    header("Content-type: application/json");
	session_start();
	$linkbd=conectar_v7();
	$linkbd -> set_charset("utf8");

	$objPHPExcel = new PHPExcel();// Crea un nuevo objeto PHPExcel
	$objPHPExcel->getProperties()->setCreator("Ideal 10 SAS")
	   ->setLastModifiedBy("HAFR")
	   ->setTitle("Lista Liquidacion Predial")
	   ->setSubject("Liquidacion")
	   ->setDescription("Liastado de Liquidaciones")
	   ->setKeywords("Liquidacion")
	   ->setCategory("Tesoreria");
	$objPHPExcel->setActiveSheetIndex(0)
		->mergeCells('A1:S1')
		->mergeCells('A2:S2')
  		->setCellValue('A1', 'LIQUIDACION')
     	->setCellValue('A2', 'INFORMACION GENERAL');
	$objPHPExcel-> getActiveSheet ()
        -> getStyle ("A1")
        -> getFill ()
        -> setFillType (PHPExcel_Style_Fill :: FILL_SOLID)
        -> getStartColor ()
        -> setRGB ('C8C8C8');
	$objPHPExcel-> getActiveSheet ()
        -> getStyle ("A1:A2")
		-> getFont ()
		-> setBold ( true )
      	-> setName ( 'Verdana' )
      	-> setSize ( 10 )
		-> getColor ()
		-> setRGB ('000000');
	$objPHPExcel-> getActiveSheet ()
		-> getStyle ('A1:A2')
		-> getAlignment ()
		-> applyFromArray (array ( 'horizontal'  =>  PHPExcel_Style_Alignment :: HORIZONTAL_CENTER ,) );
	$objPHPExcel-> getActiveSheet ()
		-> getStyle ('A3:S3')
		-> getAlignment ()
		-> applyFromArray (array ( 'horizontal'  =>  PHPExcel_Style_Alignment :: VERTICAL_JUSTIFY ,) );
	$objPHPExcel-> getActiveSheet ()
		-> getStyle ("A2")
        -> getFill ()
        -> setFillType (PHPExcel_Style_Fill :: FILL_SOLID)
        -> getStartColor ()
        -> setRGB ('A6E5F3');
	$objPHPExcel-> getActiveSheet ()
		-> getStyle ("A3:S3")
        -> getFill ()
        -> setFillType (PHPExcel_Style_Fill :: FILL_SOLID)
        -> getStartColor ()
        -> setRGB ('22C6CB');
	$borders = array(
      'borders' => array(
        'allborders' => array(
          'style' => PHPExcel_Style_Border::BORDER_THIN,
          'color' => array('argb' => 'FF000000'),
        )
      ),
    );
	$objPHPExcel->getActiveSheet()->getStyle('A1:S1')->applyFromArray($borders);
	$objPHPExcel->getActiveSheet()->getStyle('A2:S2')->applyFromArray($borders);
	$objPHPExcel->getActiveSheet()->getStyle('A3:S3')->applyFromArray($borders);
	$objPHPExcel-> getActiveSheet()->getColumnDimension('A')->setWidth(12);
	$objPHPExcel-> getActiveSheet()->getColumnDimension('B')->setWidth(11);
	$objPHPExcel-> getActiveSheet ()
		-> getStyle ('B:C')
		-> getAlignment ()
		-> applyFromArray (array ( 'horizontal'  =>  PHPExcel_Style_Alignment :: VERTICAL_JUSTIFY ,) );
	$objPHPExcel-> getActiveSheet()->getColumnDimension('C')->setWidth(10);
	$objPHPExcel-> getActiveSheet()->getColumnDimension('D')->setWidth(20);
	$objPHPExcel-> getActiveSheet()->getColumnDimension('E')->setWidth(55);
	$objPHPExcel-> getActiveSheet()->getColumnDimension('F')->setWidth(15);
	$objPHPExcel-> getActiveSheet()->getColumnDimension('G')->setWidth(15);
	$objPHPExcel-> getActiveSheet()->getColumnDimension('H')->setWidth(5);
	$objPHPExcel-> getActiveSheet()->getColumnDimension('I')->setWidth(15);
	$objPHPExcel-> getActiveSheet()->getColumnDimension('J')->setWidth(15);
	$objPHPExcel-> getActiveSheet()->getColumnDimension('K')->setWidth(15);
	$objPHPExcel-> getActiveSheet()->getColumnDimension('L')->setWidth(15);
	$objPHPExcel-> getActiveSheet()->getColumnDimension('M')->setWidth(15);
	$objPHPExcel-> getActiveSheet()->getColumnDimension('N')->setWidth(15);
	$objPHPExcel-> getActiveSheet()->getColumnDimension('O')->setWidth(15);
	$objPHPExcel-> getActiveSheet()->getColumnDimension('P')->setWidth(15);
	$objPHPExcel-> getActiveSheet()->getColumnDimension('Q')->setWidth(15);
	$objPHPExcel-> getActiveSheet()->getColumnDimension('R')->setWidth(15);
	$objPHPExcel-> getActiveSheet()->getColumnDimension('S')->setWidth(15);
	$objWorksheet = $objPHPExcel->getActiveSheet();
	$objWorksheet->fromArray(array(utf8_decode('No liquidacion'),utf8_decode('No Recibo caja'),utf8_decode('Vigencia'),utf8_decode('Codigo catastral'),utf8_decode('Tercero'),utf8_decode('Fecha'),utf8_decode('Avaluo'),utf8_decode('Tasa'),utf8_decode('Predial'),utf8_decode('Interes Predial'),utf8_decode('Bomberil'),utf8_decode('Interes Bomberil'),utf8_encode('Medio Ambiente'),utf8_encode('Interes M. Amb.'),utf8_decode('Descuentos'),utf8_decode('Total'),utf8_decode('Estado'),utf8_decode('Fecha recibo caja'),utf8_decode('Descuento Intereses')),NULL,'A3');


	preg_match("/([0-9]{2})\/([0-9]{2})\/([0-9]{4})/", $_GET['fecha1'],$fecha);
	$fechaf=$fecha[3]."-".$fecha[2]."-".$fecha[1];

	preg_match("/([0-9]{2})\/([0-9]{2})\/([0-9]{4})/", $_GET['fecha2'],$fecha);
	$fechaf2=$fecha[3]."-".$fecha[2]."-".$fecha[1];
	$cont=4;

	$crit3 = '';
	$sqlrBase = "SELECT base FROM redglobal WHERE base = 'u657666394_alcaldiarosa'";
	$respBase = mysqli_query($linkbd, $sqlrBase);
	$rowBase = mysqli_fetch_row($respBase);
	if($rowBase[0]=='u657666394_alcaldiarosa'){
		$crit3=" and tesoliquidapredial.idpredial != 568 ";
	}
	$sqlr="select *from tesoliquidapredial,tesoliquidapredial_det, tesoreciboscaja where tesoliquidapredial.estado<>'' and tesoreciboscaja.fecha BETWEEN '$fechaf' AND '$fechaf2' and tesoreciboscaja.id_recaudo=tesoliquidapredial_det.idpredial and tesoreciboscaja.tipo='1' and tesoreciboscaja.estado!='N' and tesoliquidapredial.idpredial=tesoliquidapredial_det.idpredial $crit3 order by tesoliquidapredial.idpredial ASC";
	$resp = mysqli_query($linkbd, $sqlr);
	//echo $_POST[conanul]."hola";
	while ($row =mysqli_fetch_assoc($resp))
	{
		$scrit="";
		if($_GET['conanulados']!='S')
		{
			$scrit="AND estado='S'";
		}
		$sq="select id_recibos,estado, fecha from tesoreciboscaja where tipo='1' and id_recaudo='".$row['idpredial']."'".$scrit;
		$re = mysqli_query($linkbd, $sq);
		$row1 =mysqli_fetch_array($re);
		$tercero=buscatercero($row['tercero']);

		$sq2="select avaluo from tesoprediosavaluos where codigocatastral='".$row['codigocatastral']."' and vigencia='".$row['vigliquidada']."'";
		$re2 = mysqli_query($linkbd, $sq2);
		$row2 =mysqli_fetch_array($re2);
		if($row1[0]!='')
		{
			$predial = 0;
			$bomberil = 0;
			$ambiental = 0;

			$sqlrDesc = "SELECT descuentointpredial, descuentointbomberil, descuentointambiental FROM tesoliquidapredial_desc WHERE id_predial=".$row['idpredial']." AND vigencia=".$row['vigliquidada']."";
			$respDesc = mysqli_query($linkbd, $sqlrDesc);
			$rowDesc = mysqli_fetch_row($respDesc);

			$intpredial = $row['intpredial']-$rowDesc[0];
			$intbomberil = $row['intbomb']-$rowDesc[1];
			$intambiental = $row['intmedioambiente']-$rowDesc[2];
			$tasa=$predial*1000/$row2[0];
			if(round($tasa)=='')
			{
				$tasa=$row['tasav'];
			}

            $descIntereses = $rowDesc[0] + $rowDesc[1] + $rowDesc[2];

			$objWorksheet->fromArray(array($row['idpredial'],$row1[0],$row['vigliquidada'],"COD:".$row['codigocatastral'],$row['tercero']." - ".$tercero,$row['fecha'],$row2[0],round($tasa,2),$row['predial'],$intpredial,$row['bomberil'],$intbomberil,$row['medioambiente'],$intambiental,$row['descuentos'],$row['totaliquidavig'],$row1[1],$row1[2],$descIntereses),NULL,"A$cont");
			$objPHPExcel->getActiveSheet()->getStyle("A$cont:S$cont")->applyFromArray($borders);
			$objPHPExcel->getActiveSheet()->getStyle("H$cont:I$cont")->getNumberFormat()
			->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);
			$objPHPExcel->getActiveSheet()->getStyle('A3:S3')->getAlignment()->applyFromArray(array('vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,));
			$objPHPExcel->getActiveSheet()->getStyle('A4:S4')->getAlignment()->applyFromArray(array('vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,));
			$objPHPExcel->getActiveSheet()->getStyle('A4')->getAlignment()->applyFromArray(array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,));
			$objPHPExcel->getActiveSheet()->getStyle('J:K')->getAlignment()->applyFromArray(array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,));
			$objPHPExcel->getActiveSheet()->getStyle('F3:H3')->getAlignment()->applyFromArray(array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,));
			$objPHPExcel->getActiveSheet()->getStyle('C3')->getAlignment()->applyFromArray(array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,));
			$objPHPExcel->getActiveSheet()->getStyle('A:S')->getAlignment()->applyFromArray(array('vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,));
			$objPHPExcel->getActiveSheet()->getStyle('A')->getAlignment()->applyFromArray(array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,));
			$objPHPExcel-> getActiveSheet()-> getStyle ("A3:S3")-> getFont ()-> setBold ( true );

			$cont=$cont+1;
		}

	}
	$objPHPExcel->getActiveSheet()->setTitle('Listado 1');// Renombrar Hoja
	$objPHPExcel->setActiveSheetIndex(0);// Establecer la hoja activa, para que cuando se abra el documento se muestre primero.
	// --------Cerrar--------
	header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
	header('Content-Disposition: attachment;filename="Listado Liquidacion predial.xlsx"');
	header('Cache-Control: max-age=0');
	header ('Expires: Mon, 15 Dic 2015 09:31:00 GMT'); // Date in the past
	header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
	header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
	header ('Pragma: public'); // HTTP/1.0
	$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
	$objWriter->save('php://output');
	exit;

